#!/bin/sh

HOME=/home/orchestrator/sophia/codifica-debug

JPACKAGE=com.alkemytech.sophia.recognizer
JCLASS=TermsFrequencyEvaluator
JOPTIONS=-Xmx8G
JVERSION=1.1.0


/opt/java8/bin/java -cp "$HOME/sophia-recognizer-$JVERSION.jar:$HOME/lib/*" $JOPTIONS $JPACKAGE.$JCLASS $HOME/configuration.properties 2> /dev/null

if [ $? -eq 0 ]
then
  echo '0'
  exit 0
else
  echo '1'
  exit 1
fi


