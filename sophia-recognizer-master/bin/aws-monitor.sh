#!/bin/sh

HOME=/home/roberto.cerfogli/sophia-recognizer

JPACKAGE=com.alkemytech.sophia.recognizer
JCLASS=AwsMonitorEx
JOPTIONS=-Xmx8G
JVERSION=1.0.1

java -cp "$HOME/sophia-recognizer-$JVERSION.jar:$HOME/lib/*" $JOPTIONS $JPACKAGE.$JCLASS $HOME/configuration.properties &

