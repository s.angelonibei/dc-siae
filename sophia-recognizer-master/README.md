A collection of tools for musical work recognition.

- ItacaJsonParser: extracts relevant informations from Itaca (CODMUS) JSON file(s)
- SophiaKbExtractor: extracts additional titles and artist names from SOPHIA knowledge base
- MasterDatabaseDump: dumps entire database to JSON file
- MasterDatabaseStatistics: extracts relevanti statistics from master database (e.g. max economic relevance)
- TermsFrequencyEvaluator: extracts the number of occurrences of all terms both for titles and artist names 
- LuceneIndexBuilder: creates a Lucene index
- TrieIndexBuilder: creates a Levenshtein Trie index
- InteractiveDatabaseSearch: command line interface to search master database records
- InteractiveLuceneSearch: command line interface to search Lucene index
- InteractiveTrieSearch: command line interface to search Levenshtein Trie index
- StringMatching: recognizes utilizations and computes KPIs
- KpiCalculator: computes KPIs parsing StringMatching output file
- KpiOptimizer: optimizes configuration parameters using StringMatching output file as a training set
- AwsMonitor: monitor S3 folder, automatically starts StringMatching, uploads results to S3
