package com.alkemytech.sophia.recognizer.model;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public enum BatchStatus {

	QUEUED("queued"), RUNNING("running"), COMPLETED("completed"), FAILED("failed"), KILLED("killed");
	
	public static BatchStatus parse(String string) {
		for (BatchStatus role : values()) {
			if (role.toString().equalsIgnoreCase(string)) {
				return role;
			}
		}
		throw new IllegalArgumentException("unknown status " + string);
	}

	private final String status;
	
	private BatchStatus(String status) {
		this.status = status;
	}
	
	@Override
	public String toString() {
		return status;
	}
	
}
