package com.alkemytech.sophia.recognizer.model;

import com.alkemytech.sophia.recognizer.json.Gsonable;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.sleepycat.persist.model.Persistent;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
@Persistent
public class Artist implements Gsonable {

	public static final String NAME = "name";
	public static final String ROLE = "role";
	public static final String TAG = "tag";
	
	public String name;
	public ArtistRole role;
	public String tag;
	
	public Artist() {
		super();
	}
	
	public Artist(String name, ArtistRole role, String tag) {
		super();
		this.name = name;
		this.role = role;
		this.tag = tag;
	}

	@Override
	public JsonObject toJsonObject() {
		final JsonObject jsonObject = new JsonObject();
		if (null != name) {
			jsonObject.addProperty(NAME, name);
		}
		if (null != role) {
			jsonObject.addProperty(ROLE, role.toString());
		}
		if (null != tag) {
			jsonObject.addProperty(TAG, tag);
		}
		return jsonObject;
	}
	
	@Override
	public Artist initWithJsonObject(JsonObject jsonObject) {
		JsonElement jsonElement = jsonObject.get(NAME);
		name = (null == jsonElement ? null : jsonElement.getAsString());
		jsonElement = jsonObject.get(ROLE);
		role = (null == jsonElement ? null : ArtistRole.parse(jsonElement.getAsString()));
		jsonElement = jsonObject.get(TAG);
		tag = (null == jsonElement ? null : jsonElement.getAsString());
		return this;
	}
	
	@Override
	public String toJson(Gson gson) {
		return gson.toJson(toJsonObject());
	}

	@Override
	public Artist initWithJson(String json, Gson gson) {
		return initWithJsonObject(gson.fromJson(json, JsonObject.class));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((null == name) ? 0 : name.hashCode());
		result = prime * result + ((null == role) ? 0 : role.hashCode());
		result = prime * result + ((null == tag) ? 0 : tag.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		} else if (null == object) {
			return false;
		} else if (getClass() != object.getClass()) {
			return false;
		}
		final Artist that = (Artist) object;
		// name
		if (null == this.name) {
			if (null != that.name) {
				return false;
			}
		} else if (!this.name.equals(that.name)) {
			return false;
		}
		// role
		if (this.role != that.role) {
			return false;
		}
		// tag
		if (null == this.tag) {
			if (null != that.tag) {
				return false;
			}
		} else if (!this.tag.equals(that.tag)) {
			return false;
		}
		return true;
	}

}
