package com.alkemytech.sophia.recognizer.siada;

import java.util.regex.Pattern;

import com.alkemytech.sophia.recognizer.util.Str;
import com.google.common.base.Strings;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class SiadaCode {

	private static final Pattern VALIDATION_REGEX = Pattern.compile("^\\d{11}$");
	
	public static String normalize(String code) {
		if (Strings.isNullOrEmpty(code)) {
			return null;
		}
		return Str.lpad(code, 11, '0');
	}
	
	public static boolean isValid(String code) {
		if (null == code) {
			return false;
		}
		return VALIDATION_REGEX.matcher(code).matches();
	}
	
}
