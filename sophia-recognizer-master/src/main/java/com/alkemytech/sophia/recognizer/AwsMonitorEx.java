package com.alkemytech.sophia.recognizer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.ServerSocket;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.guice.Log4j2;
import com.alkemytech.sophia.recognizer.db.BatchDatabase;
import com.alkemytech.sophia.recognizer.db.MasterDatabase;
import com.alkemytech.sophia.recognizer.lucene.LuceneSearch;
import com.alkemytech.sophia.recognizer.model.Batch;
import com.alkemytech.sophia.recognizer.model.BatchStatus;
import com.alkemytech.sophia.recognizer.score.ScoreCalculator;
import com.alkemytech.sophia.recognizer.trie.TrieSearch;
import com.alkemytech.sophia.recognizer.utilization.UtilizationsReader;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import com.ibm.icu.text.SimpleDateFormat;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class AwsMonitorEx {
	
	private static final Logger logger = LoggerFactory.getLogger(AwsMonitorEx.class);
	static int exitCode = 0;
	private class GuiceModuleForProcessing extends AbstractModule {
		
		protected final Properties configuration;
		
		public GuiceModuleForProcessing(Properties configuration) {
			super();
			this.configuration = configuration;
		}

		@Override
		protected void configure() {
			
			// configuration properties
			Names.bindProperties(binder(), configuration);
			bind(Properties.class)
				.annotatedWith(Names.named("configuration"))
				.toInstance(configuration);

			// charset(s)
			bind(Charset.class)
				.annotatedWith(Names.named("charset"))
				.toInstance(Charset.forName(configuration.getProperty("default.charset", "UTF-8")));
			
			// default time zone
			TimeZone.setDefault(TimeZone.getTimeZone(configuration.getProperty("default.timezone", "UTC")));

			// initialize log4j2
			Log4j2.initialize(configuration);
			
			// gson
			bind(Gson.class)
				.toInstance(new GsonBuilder()
						.disableHtmlEscaping()
						.create());	
			
			// singleton(s)
			bind(MasterDatabase.class).asEagerSingleton();
			bind(LuceneSearch.class).asEagerSingleton();
			bind(TrieSearch.class).asEagerSingleton();
			bind(UtilizationsReader.class).asEagerSingleton();
			bind(ScoreCalculator.class).asEagerSingleton();
			bind(StringMatching.class).asEagerSingleton();
		}

	}
	
	private static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args) {
			super(args);
		}

		@Override
		protected void configure() {
			super.configure();
			// singleton(s)
			bind(S3.class).asEagerSingleton();
			bind(SQS.class).asEagerSingleton();
			bind(BatchDatabase.class).asEagerSingleton();
			bind(AwsMonitorEx.class).asEagerSingleton();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			final AwsMonitorEx instance = Guice
				.createInjector(new GuiceModuleExtension(args))
					.getInstance(AwsMonitorEx.class);
			try {
				instance.startup()
					.process();
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
			exitCode = 1 ;
		} finally {
			System.exit(exitCode);
		}
	}
	
	private final Properties configuration;
	private final S3 s3;
	private final SQS sqs;
	private final BatchDatabase batchDatabase;
	private final Gson gson;
	private final File homeFolder;
	private final String watchedPrefix;
	private final String configSuffix;
	private final String inputSuffix;
	private final String lockSuffix;
	private final String outputSuffix;
	private final String skippedSuffix;
	private final String kpiSuffix;
	private final String errorSuffix;
	private final String zipOutputSuffix;
	private final String zipSkippedSuffix;
	private final boolean archiveOnS3UploadError;

	private ExecutorService executorService;
	private StringMatching stringMatching;
	
	@Inject
	protected AwsMonitorEx(@Named("configuration") Properties configuration,
			S3 s3, SQS sqs, BatchDatabase batchDatabase, Gson gson) {
		super();
		this.configuration = configuration;
		this.s3 = s3;
		this.sqs = sqs;
		this.batchDatabase = batchDatabase;
		this.gson = gson;		
		this.homeFolder = new File(configuration
				.getProperty("awsMonitor.homeFolder"));
		this.watchedPrefix = configuration
				.getProperty("awsMonitor.watchedPrefix");
		this.configSuffix = configuration
				.getProperty("awsMonitor.configSuffix");
		this.inputSuffix = configuration
				.getProperty("awsMonitor.inputSuffix");
		this.lockSuffix = configuration
				.getProperty("awsMonitor.lockSuffix");
		this.outputSuffix = configuration
				.getProperty("awsMonitor.outputSuffix");
		this.skippedSuffix = configuration
				.getProperty("awsMonitor.skippedSuffix");
		this.kpiSuffix = configuration
				.getProperty("awsMonitor.kpiSuffix");
		this.errorSuffix = configuration
				.getProperty("awsMonitor.errorSuffix");
		this.zipOutputSuffix = configuration
				.getProperty("awsMonitor.zipOutputSuffix");
		this.zipSkippedSuffix = configuration
				.getProperty("awsMonitor.zipSkippedSuffix");
		this.archiveOnS3UploadError = "true".equalsIgnoreCase(configuration
				.getProperty("awsMonitor.archiveOnS3UploadError"));
	}
	
	public AwsMonitorEx startup() {
		s3.startup();
		sqs.startup();
		batchDatabase.startup();
		executorService = Executors.newSingleThreadExecutor();
		return this;
	}

	public AwsMonitorEx shutdown() {
		executorService.shutdown();
		batchDatabase.shutdown();
		sqs.shutdown();
		s3.shutdown();
		return this;
	}
	
	private void parseMessage(SQS.Msg message, Set<S3.Url> createdObjects, Set<S3.Url> removedObjects) {
		try {
			final JsonObject rootJsonObject = gson.fromJson(message.text, JsonObject.class);
			final JsonArray jsonArray = rootJsonObject.getAsJsonArray("Records");
			if (null != jsonArray) {
				for (JsonElement element : jsonArray) {
					final JsonObject record = element.getAsJsonObject();
					logger.debug("record {}", record);
					final JsonElement eventName = record.get("eventName");
					final JsonObject s3 = record.getAsJsonObject("s3");
					final JsonElement bucketName = s3.getAsJsonObject("bucket").get("name");
					final JsonElement objectKey = s3.getAsJsonObject("object").get("key");
					if (null != bucketName && !Strings.isNullOrEmpty(bucketName.getAsString()) &&
							null != objectKey && !Strings.isNullOrEmpty(objectKey.getAsString()) &&
							null != eventName && !Strings.isNullOrEmpty(eventName.getAsString())) {
						if (eventName.getAsString().contains("ObjectCreated")) {
							createdObjects.add(new S3.Url(bucketName.getAsString(),
									objectKey.getAsString()));
						} else if (eventName.getAsString().contains("ObjectRemoved")) {
							removedObjects.add(new S3.Url(bucketName.getAsString(),
									objectKey.getAsString()));
						}
					}
				}
			}
		} catch (JsonSyntaxException e) {
			logger.warn("parseMessage", e);
			exitCode = 1;
		}
	}
	
	private void processBatch(final Batch batch) throws IOException {
		
		// touch batch (become last in queue)
		batchDatabase.replaceAlways(batch);

		// urls
		final S3.Url configUrl = new S3.Url(batch.url + configSuffix);
		final S3.Url inputUrl = new S3.Url(batch.url + inputSuffix);
		final S3.Url lockUrl = new S3.Url(batch.url + lockSuffix);
		final S3.Url outputUrl = new S3.Url(batch.url + zipOutputSuffix);
		final S3.Url skippedUrl = new S3.Url(batch.url + zipSkippedSuffix);
		final S3.Url kpiUrl = new S3.Url(batch.url + kpiSuffix);
		final S3.Url errorUrl = new S3.Url(batch.url + errorSuffix);
		
		// check objects existence
		final boolean configExists = s3.doesObjectExist(configUrl);
		final boolean inputExists = s3.doesObjectExist(inputUrl);
		final boolean lockExists = s3.doesObjectExist(lockUrl);
		logger.debug("config exists {}", configExists);
		logger.debug("input exists {}", inputExists);
		logger.debug("lock exists {}", lockExists);
		
		if (configExists && inputExists && !lockExists) {
			boolean s3UploadError = false;
			
			// batch filename
			final String filename = batch.getFileName();
			logger.debug("batch filename {}", filename);

			// local files
			final File lockFile = new File(homeFolder, filename + lockSuffix);
			final File configFile = new File(homeFolder, filename + configSuffix);
			final File inputFile = new File(homeFolder, filename + inputSuffix);
			final File outputFile = new File(homeFolder, filename + outputSuffix);
			final File skippedFile = new File(homeFolder, filename + skippedSuffix);
			final File zipOutputFile = new File(homeFolder, filename + zipOutputSuffix);
			final File zipSkippedFile = new File(homeFolder, filename + zipSkippedSuffix);
			final File kpiFile = new File(homeFolder, filename + kpiSuffix);
			final File errorFile = new File(homeFolder, filename + errorSuffix);
			try {
				
				// create lock file and upload to s3
				try (Writer writer = new FileWriter(lockFile)) {
					writer.write(new Date().toString());
				}
				if (!s3.upload(lockUrl, lockFile)) {
					logger.warn("unable to upload lock file {} to {}", lockFile, lockUrl);
					return;
				}

				// download config from s3
				if (!s3.download(configUrl, configFile)) {
					logger.warn("unable to download config file {} from {}", configFile, configUrl);
					return;
				}

				// download input from s3
				if (!s3.download(inputUrl, inputFile)) {
					logger.warn("unable to download input file {} from {}", inputFile, inputUrl);
					return;
				}

				// update batch status to running
				batch.status = BatchStatus.RUNNING;
				batchDatabase.replaceAlways(batch);

				// process batch
				final Semaphore semaphore = new Semaphore(0);
				Executors.newSingleThreadExecutor().execute(new Runnable() {
					@Override
					public void run() {
						try {
							// load configuration
							final Properties properties = new Properties();
							properties.putAll(configuration);
							try (Reader reader = new FileReader(configFile)) {
								properties.load(reader); // override defaults
							}

							// override configuration parameters
							properties.setProperty("utilizationsReader.homeFolder", inputFile.getParent());
							properties.setProperty("utilizationsReader.filenames", inputFile.getName());
							properties.setProperty("stringMatching.backupConfiguration", "false");
							properties.setProperty("stringMatching.saveSkipped", "false");
									
							// create guice injector using configuration
							final Injector injector = Guice
									.createInjector(new GuiceModuleForProcessing(properties));

							// startup, execute and shutdown string matching
							assert (null == stringMatching);
							stringMatching = injector.getInstance(StringMatching.class);
							try {
								stringMatching.startup()
									.process(outputFile, skippedFile, kpiFile);
							} finally {
								stringMatching.shutdown();
								stringMatching = null;
							}
							// update batch status to completed
							if (BatchStatus.KILLED != batch.status) {
								batch.status = BatchStatus.COMPLETED;
								batchDatabase.replaceAlways(batch);
							}
						} catch (Exception e) {
							logger.error("error processing batch", e);
							exitCode = 1;
							// create error file
							try (PrintWriter writer = new PrintWriter(new FileWriter(errorFile))) {
								e.printStackTrace(writer);
							} catch (IOException ex) {
								logger.error("error creating error file", ex);
							} finally {
								// update batch status to failed
								if (BatchStatus.KILLED != batch.status) {
									batch.status = BatchStatus.FAILED;
									batchDatabase.replaceAlways(batch);
								}
							}
						} finally {
							semaphore.release();
						}						
					}
				});

				// busy wait
				while (!semaphore.tryAcquire(3L, TimeUnit.SECONDS)) {
					// check for kill request
					if (BatchStatus.KILLED == batchDatabase
							.getByUrl(batch.url).status) {
						batch.status = BatchStatus.KILLED;
						if (null != stringMatching) {
							stringMatching.interrupt();
						}
					}
				}

				// upload file(s) to s3
				if (BatchStatus.FAILED == batch.status) {
					// upload error to s3
					if (errorFile.exists()) {
						if (!s3.upload(errorUrl, errorFile)) {
							s3UploadError = true;
							logger.warn("unable to upload error file {} to {}", errorFile, errorUrl);
						}
					} else {
						logger.warn("error file does not exists {}", errorFile);
					}
				} else {
					// zip and upload output to s3
					if (outputFile.exists()) {
						try (ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipOutputFile));
								InputStream in = new FileInputStream(outputFile)) {
							final byte[] buffer = new byte[1024];
							out.putNextEntry(new ZipEntry(outputFile.getName()));
							for (int length; -1 != (length = in.read(buffer)); ) {
								out.write(buffer, 0, length);
							}
							out.closeEntry();
						}
						if (!s3.upload(outputUrl, zipOutputFile)) {
							s3UploadError = true;
							logger.warn("unable to upload output file {} to {}", zipOutputFile, outputUrl);
						}
					} else {
						logger.warn("output file does not exists {}", outputFile);
					}
					// zip and upload skipped to s3
					if (skippedFile.exists()) {
						try (ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipSkippedFile));
								InputStream in = new FileInputStream(skippedFile)) {
							final byte[] buffer = new byte[1024];
							out.putNextEntry(new ZipEntry(skippedFile.getName()));
							for (int length; -1 != (length = in.read(buffer)); ) {
								out.write(buffer, 0, length);
							}
							out.closeEntry();
						}
						if (!s3.upload(skippedUrl, zipSkippedFile)) {
							s3UploadError = true;
							logger.warn("unable to upload skipped file {} to {}", zipSkippedFile, skippedUrl);
						}
					} else {
						logger.warn("skipped file does not exists {}", skippedFile);
					}
					// upload kpi to s3
					if (kpiFile.exists()) {
						if (!s3.upload(kpiUrl, kpiFile)) {
							s3UploadError = true;
							logger.warn("unable to upload kpi file {} to {}", kpiFile, kpiUrl);
						}
					} else {
						logger.warn("kpi file does not exists {}", kpiFile);
					}
				}
				if (BatchStatus.KILLED == batch.status) {
					// upload lock file to s3 again
					if (!s3.upload(lockUrl, lockFile)) {
						s3UploadError = true;
						logger.warn("unable to upload lock file {} to {}", lockFile, lockUrl);
					}
				}
			} catch (Exception e) {
				logger.error("error processing batch", e);
				exitCode = 1;
				// update batch status to failed
				if (BatchStatus.KILLED != batch.status) {
					batch.status = BatchStatus.FAILED;
					batchDatabase.replaceAlways(batch);
				}
				// create error file and upload to s3
				try (PrintWriter writer = new PrintWriter(new FileWriter(errorFile))) {
					e.printStackTrace(writer);
				} catch (IOException ex) {
					logger.error("error creating error file", ex);
				}
				if (!s3.upload(errorUrl, errorFile)) {
					s3UploadError = true;
					logger.warn("unable to upload error file {} to {}", errorFile, errorUrl);
				}
			} finally {
				// create archive
				if (s3UploadError && archiveOnS3UploadError) {
					final File archiveFile = new File(homeFolder, new SimpleDateFormat("yyyy_MM_dd_HHmm")
							.format(new Date()) + "_" + filename + "_archive.zip");
					final File[] files = { configFile, inputFile, outputFile, skippedFile, kpiFile, errorFile };
					final byte[] buffer = new byte[1024];
					try (ZipOutputStream out = new ZipOutputStream(new FileOutputStream(archiveFile))) {
						for (File file : files) {
							if (file.length() > 0L) {
								try (InputStream in = new FileInputStream(file)) {
									out.putNextEntry(new ZipEntry(file.getName()));
									for (int length; -1 != (length = in.read(buffer)); ) {
										out.write(buffer, 0, length);
									}
									out.closeEntry();
								}
							}
						}
					} catch (Exception e) {
						logger.error("error creating backup archive", e);
						exitCode = 1;
					}
				}
				// delete local files
				lockFile.delete();
				configFile.delete();
				inputFile.delete();
				outputFile.delete();									
				skippedFile.delete();									
				zipOutputFile.delete();									
				zipSkippedFile.delete();									
				kpiFile.delete();									
				errorFile.delete();
			}
		} else if (!configExists && !inputExists && !lockExists) {
			// delete job because no file exists
			if (!batchDatabase.deleteByUrl(batch.url)) {
				logger.warn("error deleting batch with no files {}", batch);
			} else {
				logger.warn("batch with no files successfully deleted {}", batch);
			}			
		}
	}
	
	public AwsMonitorEx process() throws Exception {
		
		final String queueUrl = sqs.getUrl(configuration
				.getProperty("awsMonitor.queueName"));
		final long pollingDelayMilllis = Long.parseLong(configuration
				.getProperty("awsMonitor.pollingDelayMilllis", "5000")); // default 5 seconds
		final long maxIdleMillis = Long.parseLong(configuration
				.getProperty("awsMonitor.maxIdleMillis", "60000")); // default 1 minute
		final int batchesPerRun = Math.max(1, Integer.parseInt(configuration
				.getProperty("awsMonitor.batchesPerRun", "1"))); // default 1
		final int bindPort = Integer.parseInt(configuration
				.getProperty("awsMonitor.bindport", configuration.getProperty("default.bindport", "0")));

		// bind to lock tcp port
		try (ServerSocket socket = new ServerSocket(bindPort)) {
			final long startTimeMillis = System.currentTimeMillis();
			final Semaphore semaphore = new Semaphore(1);
			final AtomicInteger processedBatches = new AtomicInteger(batchesPerRun);
			while (processedBatches.get() > 0) {
				
				// poll SQS queue
				logger.debug("polling queue {}", queueUrl);
				final List<SQS.Msg> messages = sqs.receiveMessages(queueUrl);
				if (!messages.isEmpty()) {
					
					// parse messag(es)
					for (SQS.Msg message : messages) {
						final Set<S3.Url> createdObjects = new HashSet<>();
						final Set<S3.Url> removedObjects = new HashSet<>();
						parseMessage(message, createdObjects, removedObjects);
						int nonWatchedObjects = 0;
						final int totalObjects = createdObjects.size() + removedObjects.size();

						// analyze created object(s)
						logger.debug("created objects {}", createdObjects);
						for (S3.Url object : createdObjects) {
//							final String key = URLDecoder
//									.decode(object.key.replace("+", "%20"), "UTF-8");
							final String key = URLDecoder .decode(object.key, "UTF-8");
							if (key.startsWith(watchedPrefix)) {
								// watch for created config and/or input only
								S3.Url url = null;
								if (key.endsWith(configSuffix)) {
									logger.debug("watched config object {}", object);
									url = new S3.Url(object.bucket, key
											.substring(0, key.length() - configSuffix.length()));
									logger.debug("watched config url {}", url);
								} else if (key.endsWith(inputSuffix)) {
									logger.debug("watched input object {}", object);
									url = new S3.Url(object.bucket, key
											.substring(0, key.length() - inputSuffix.length()));
									logger.debug("watched input url {}", url);
								} else {
									logger.debug("unhandled watched object {}", object);
								}
								if (null != url) {
									final boolean inputExists = s3.doesObjectExist(new S3
											.Url(url.bucket, url.key + inputSuffix));
									final boolean configExists = s3.doesObjectExist(new S3
											.Url(url.bucket, url.key + configSuffix));
									final boolean lockExists = s3.doesObjectExist(new S3
											.Url(url.bucket, url.key + lockSuffix));
									logger.debug("input exists {}", inputExists);
									logger.debug("config exists {}", configExists);
									logger.debug("lock exists {}", lockExists);
									if (inputExists && configExists && !lockExists) {
										final Batch batch = batchDatabase.getByUrl(url.toString());
										if (null == batch) {
											if (!batchDatabase.insertNoOverwrite(new Batch(url.toString(), BatchStatus.QUEUED))) {
												logger.warn("error inserting new batch");
											}
										} else {
											// reset to queued if not running
											logger.debug("input/config created when {} {}", batch.status, object);
											if (BatchStatus.RUNNING != batch.status &&
													BatchStatus.QUEUED != batch.status) {
												batch.status = BatchStatus.QUEUED;
												if (!batchDatabase.replaceAlways(batch)) {
													logger.warn("error updating existing batch");
												}
											}
										}
									}									
								}
							} else {
								nonWatchedObjects ++;
								logger.debug("non watched object {}", object);
							}							
						}
						
						// analyze removed object(s)
						logger.debug("removed objects {}", removedObjects);
						for (S3.Url object : removedObjects) {
//							final String key = URLDecoder
//								.decode(object.key.replace("+", "%20"), "UTF-8");
							final String key = URLDecoder .decode(object.key, "UTF-8");
							if (key.startsWith(watchedPrefix)) {
								// watch for removed lock only
								if (key.endsWith(lockSuffix)) {
									logger.debug("watched lock object {}", object);
									final S3.Url url = new S3.Url(object.bucket, key
											.substring(0, key.length() - lockSuffix.length()));
									logger.debug("watched lock url {}", url);
									// lookup batch
									final Batch batch = batchDatabase.getByUrl(url.toString());
									if (null != batch) {
										// kill if running, reset to queued otherwise
										logger.debug("lock removed when {} {}", batch.status, object);
										if (BatchStatus.RUNNING == batch.status) {
											batch.status = BatchStatus.KILLED;
											batchDatabase.replaceAlways(batch);
										} else if (BatchStatus.QUEUED != batch.status) {
											batch.status = BatchStatus.QUEUED;
											batchDatabase.replaceAlways(batch);																					
										}
									}
								} else {
									logger.debug("unhandled watched object {}", object);
								}
							} else {
								nonWatchedObjects ++;
								logger.debug("non watched object {}", object);
							}
						}
						
						// remove message
						if (0 == nonWatchedObjects) {
							if (sqs.deleteMessage(queueUrl, message.receiptHandle)) {
								logger.debug("deleted message {}", message.receiptHandle);
							} else {
								logger.warn("unable to delete message {}", message.receiptHandle);
							}
						} else if (nonWatchedObjects != totalObjects) {
							// TODO (1) delete message
							//      (2) send message without watched object(s)
							logger.warn("message in-flight with {}/{} non watched objects", nonWatchedObjects, totalObjects);
						}
					}
				}
				
				// process batch
				if (semaphore.tryAcquire(pollingDelayMilllis, TimeUnit.MILLISECONDS)) {
					semaphore.release();
					
					// idle for too long
					if (System.currentTimeMillis() - startTimeMillis > maxIdleMillis) {
						logger.debug("idle for too long {}ms", System.currentTimeMillis() - startTimeMillis);
						break;
					}
					
					// query for queued or running batch(es)
					final ArrayList<Batch> queuedBatches = new ArrayList<>();
					final ArrayList<Batch> runningBatches = new ArrayList<>();
					batchDatabase.selectAllBatches(new Batch.Selector() {
						@Override
						public Batch.Selector select(Batch batch) {
							if (BatchStatus.QUEUED == batch.status) {
								queuedBatches.add(batch);
							} else if (BatchStatus.RUNNING == batch.status) {
								runningBatches.add(batch);
							}
							return this;
						}
					});
					logger.debug("queued batches {}", queuedBatches);
					logger.debug("running batches {}", runningBatches);
					
					// reset stale running batches to queued state
					for (Batch batch : runningBatches) {
						batch.status = BatchStatus.QUEUED;
						batchDatabase.replaceAlways(batch);
						queuedBatches.add(batch);
					}
					
					// process batch
					if (!queuedBatches.isEmpty()) {
						
						// sort queued batch(es)
						queuedBatches.sort((Batch a, Batch b)->(Long
								.compare(a.lastUpdate, b.lastUpdate)));
						logger.debug("sorted queued batches {}", queuedBatches);

						// process oldest batch
						if (semaphore.tryAcquire()) {
							try {
								executorService.execute(new Runnable() {
									@Override
									public void run() {
										try {
											processBatch(queuedBatches.remove(0));
										} catch (Exception e) {
											logger.error("error processing batch", e);
											exitCode = 1;
										} finally {
											processedBatches.decrementAndGet();
											semaphore.release();
										}
									}
								});
							} catch (RejectedExecutionException | NullPointerException e) {
								semaphore.release();
								exitCode = 1;
							}
						}
						
					} else {
						// slow down
						Thread.sleep(pollingDelayMilllis);
					}
				}
			}
			
			// wait for last batch to complete
			semaphore.acquire();
			
			logger.debug("completed in {}s", (System.currentTimeMillis() - startTimeMillis) / 1000L);
		}
		
		return this;
	}

}