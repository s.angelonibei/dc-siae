package com.alkemytech.sophia.recognizer.lucene;

import java.util.HashMap;
import java.util.Map;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.core.KeywordAnalyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.analysis.miscellaneous.PerFieldAnalyzerWrapper;
import org.apache.lucene.analysis.standard.StandardAnalyzer;

public abstract class LuceneAnalyzers {

	public static Analyzer get(String name) {
		if ("default".equalsIgnoreCase(name) ||
				"whitespace".equalsIgnoreCase(name)) {
			final Map<String,Analyzer> perFieldAnalyzers = new HashMap<>();
			perFieldAnalyzers.put(LuceneField.TITLE, new WhitespaceAnalyzer());
			perFieldAnalyzers.put(LuceneField.TITLE_NO_SPACES, new KeywordAnalyzer());
			perFieldAnalyzers.put(LuceneField.ARTIST, new WhitespaceAnalyzer());
			perFieldAnalyzers.put(LuceneField.ARTIST_NO_SPACES, new KeywordAnalyzer());
			return new PerFieldAnalyzerWrapper(new StandardAnalyzer(CharArraySet.EMPTY_SET), perFieldAnalyzers);
		} else if ("standard".equalsIgnoreCase(name)) {
			final Map<String,Analyzer> perFieldAnalyzers = new HashMap<>();
			perFieldAnalyzers.put(LuceneField.TITLE, new StandardAnalyzer(CharArraySet.EMPTY_SET));
			perFieldAnalyzers.put(LuceneField.TITLE_NO_SPACES, new KeywordAnalyzer());
			perFieldAnalyzers.put(LuceneField.ARTIST, new StandardAnalyzer(CharArraySet.EMPTY_SET));
			perFieldAnalyzers.put(LuceneField.ARTIST_NO_SPACES, new KeywordAnalyzer());
			return new PerFieldAnalyzerWrapper(new StandardAnalyzer(CharArraySet.EMPTY_SET), perFieldAnalyzers);
		}
		throw new IllegalArgumentException("unknown analyzer \"" + name + "\"");
	}
	
}
