package com.alkemytech.sophia.recognizer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.recognizer.db.MasterDatabase;
import com.alkemytech.sophia.recognizer.model.Work;
import com.google.gson.Gson;
import com.google.gson.stream.JsonWriter;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class MasterDatabaseDump {
	
	private static final Logger logger = LoggerFactory.getLogger(MasterDatabaseDump.class);

	protected static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args) {
			super(args);
		}

		@Override
		protected void configure() {
			super.configure();
			// singleton(s)
			bind(MasterDatabase.class).asEagerSingleton();
			bind(MasterDatabaseDump.class).asEagerSingleton();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			final MasterDatabaseDump instance = Guice.createInjector(new GuiceModuleExtension(args))
				.getInstance(MasterDatabaseDump.class)
					.startup();
			try {
				instance.process(args);
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}
	
	private final Properties configuration;
	private final MasterDatabase masterDatabase;
	private final Gson gson;
	
	@Inject
	protected MasterDatabaseDump(@Named("configuration") Properties configuration,
			MasterDatabase masterDatabase, Gson gson) {
		super();
		this.configuration = configuration;
		this.masterDatabase = masterDatabase;
		this.gson = gson;
	}
	
	public MasterDatabaseDump startup() {
		masterDatabase.startup();
		return this;
	}

	public MasterDatabaseDump shutdown() {
		masterDatabase.shutdown();
		return this;
	}
	
	public MasterDatabaseDump process(String[] args) throws IOException {
		
		final File homeFolder = new File(configuration.getProperty("masterDatabaseDump.homeFolder"));
		final File outputFile = new File(homeFolder, configuration.getProperty("masterDatabaseDump.outputFile"));
		final int heartbeat = Integer.parseInt(configuration.getProperty("default.heartbeat", "1000"));
		
		logger.debug("exporting json to {}", outputFile);
		final AtomicInteger rownum = new AtomicInteger(0);
		final long startTimeMillis = System.currentTimeMillis();
		try (JsonWriter writer = new JsonWriter(new BufferedWriter(new FileWriter(outputFile)))) {
			writer.beginArray();
			masterDatabase.selectAllWorks(new Work.Selector() {

				@Override
				public Work.Selector select(Work work) {
					// serialize object
					gson.toJson(work.toJsonObject(), writer);
					// heartbeat
		        	if (0 == (rownum.incrementAndGet() % heartbeat)) {
						logger.debug("{}k", rownum.get() / 1000);
					}
					return this;
				}
				
			});
			writer.endArray();
		}
		logger.debug("{} records exported", rownum.get());
		logger.debug("export completed in {}s", (System.currentTimeMillis() - startTimeMillis) / 1000L);			

		return this;
	}

}