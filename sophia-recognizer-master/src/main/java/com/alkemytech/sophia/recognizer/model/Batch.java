package com.alkemytech.sophia.recognizer.model;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
@Entity
public class Batch {

	public static interface Selector {
		public Selector select(Batch batch);
	}

	@PrimaryKey
	public String url;
	public BatchStatus status;
	public Long lastUpdate;
	
	public Batch() {
		super();
	}

	public Batch(String url, BatchStatus status) {
		super();
		this.url = url;
		this.status = status;
	}

	public Batch(String url, BatchStatus status, Long lastUpdate) {
		super();
		this.url = url;
		this.status = status;
		this.lastUpdate = lastUpdate;
	}
	
	public String getS3FolderUrl() {
		return url.substring(0, url.lastIndexOf('/'));
	}

	public String getFileName() {
		return url.substring(1 + url.lastIndexOf('/'));
	}
	
	@Override
	public String toString() {
		return new StringBuilder()
				.append("{\"url\":\"")
				.append(url)
				.append("\", \"status\":\"")
				.append(status)
				.append("\", \"lastUpdate\":")
				.append(lastUpdate)
				.append("}")
				.toString();
	}

}
