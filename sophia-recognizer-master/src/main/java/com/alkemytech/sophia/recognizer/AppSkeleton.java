package com.alkemytech.sophia.recognizer;

import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class AppSkeleton {
	
	private static final Logger logger = LoggerFactory.getLogger(AppSkeleton.class);

	protected static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args) {
			super(args);
		}

		@Override
		protected void configure() {
			super.configure();
			// singleton(s)
			bind(AppSkeleton.class).asEagerSingleton();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			final AppSkeleton instance = Guice.createInjector(new GuiceModuleExtension(args))
				.getInstance(AppSkeleton.class)
					.startup();
			try {
				instance.process(args);
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}
	
	private final Properties configuration;
	
	@Inject
	protected AppSkeleton(@Named("configuration") Properties configuration) {
		super();
		this.configuration = configuration;
	}
	
	public AppSkeleton startup() {
		return this;
	}

	public AppSkeleton shutdown() {
		return this;
	}
	
	public AppSkeleton process(String[] args) throws IOException {
		
		final long startTimeMillis = System.currentTimeMillis();

		configuration.getProperty("appSkeleton...");
		
		logger.debug("completed in {}s", (System.currentTimeMillis() - startTimeMillis) / 1000L);			

		return this;
	}

}