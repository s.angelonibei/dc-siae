package com.alkemytech.sophia.recognizer;

import java.io.IOException;
import java.net.ServerSocket;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.recognizer.db.MasterDatabase;
import com.alkemytech.sophia.recognizer.jdbc.DBCP2DataSourceProvider;
import com.alkemytech.sophia.recognizer.model.Artist;
import com.alkemytech.sophia.recognizer.model.ArtistRole;
import com.alkemytech.sophia.recognizer.model.Title;
import com.alkemytech.sophia.recognizer.model.Work;
import com.alkemytech.sophia.recognizer.regex.StringNormalizer;
import com.alkemytech.sophia.recognizer.siada.SiadaCode;
import com.google.common.base.Strings;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Scopes;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class SophiaKbExtractor {
	
	private static final Logger logger = LoggerFactory.getLogger(SophiaKbExtractor.class);
	static int exitCode = 0;
	protected static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args) {
			super(args);
		}

		@Override
		protected void configure() {
			super.configure();
			// mysql datasource(s)
			bind(DataSource.class)
				.annotatedWith(Names.named("sophiaKbDs"))
				.toProvider(new DBCP2DataSourceProvider(configuration, "sophiaKbExtractor.dataSource"))
				.in(Scopes.SINGLETON);
			// singleton(s)
			bind(MasterDatabase.class).asEagerSingleton();
			bind(SophiaKbExtractor.class).asEagerSingleton();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			final SophiaKbExtractor instance = Guice.createInjector(new GuiceModuleExtension(args))
				.getInstance(SophiaKbExtractor.class)
					.startup();
			try {
				instance.process();
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
			exitCode = 1 ;
		} finally {
			System.exit(exitCode);
		}
	}
	
	private final Properties configuration;
	private final DataSource sophiaKbDs;
	private final MasterDatabase masterDatabase;
	
	@Inject
	protected SophiaKbExtractor(@Named("configuration") Properties configuration,
			@Named("sophiaKbDs") DataSource sophiaKbDs,
			MasterDatabase masterDatabase) {
		super();
		this.configuration = configuration;
		this.sophiaKbDs = sophiaKbDs;
		this.masterDatabase = masterDatabase;
	}
	
	public SophiaKbExtractor startup() {
		if ("true".equalsIgnoreCase(configuration.getProperty("sophiaKbExtractor.locked", "true"))) {
			throw new IllegalStateException("application locked");
		}
		masterDatabase.startup();
		return this;
	}

	public SophiaKbExtractor shutdown() {
		masterDatabase.shutdown();
		return this;
	}
	
	public SophiaKbExtractor process() throws Exception {
		try {
			final String[] dsps = configuration.getProperty("sophiaKbExtractor.dsps").split(",");
			final String[] origins = configuration.getProperty("sophiaKbExtractor.origins").split(",");
			final ArtistRole role = ArtistRole.parse(configuration.getProperty("sophiaKbExtractor.role"));
			final StringNormalizer titleNormalizer = new StringNormalizer(exitCode,configuration
					.getProperty("sophiaKbExtractor.normalizer.title").split(","));
			final StringNormalizer artistNormalizer = new StringNormalizer(exitCode,configuration
					.getProperty("sophiaKbExtractor.normalizer.artist").split(","));
			final boolean toUpperCase = "true".equalsIgnoreCase("sophiaKbExtractor.toUpperCase");
			final int heartbeat = Integer.parseInt(configuration
					.getProperty("sophiaKbExtractor.heartbeat", configuration.getProperty("default.heartbeat", "1000")));
			final int bindPort = Integer.parseInt(configuration
					.getProperty("sophiaKbExtractor.bindport", configuration.getProperty("default.bindport", "0")));

			// bind to lock tcp port
			try (ServerSocket socket = new ServerSocket(bindPort)) {
				try (Connection connection = sophiaKbDs.getConnection()) {
					logger.debug("connected to {} {}", connection.getMetaData()
							.getDatabaseProductName(), connection.getMetaData().getURL());

					final long startTimeMillis = System.currentTimeMillis();
					final AtomicInteger rownum = new AtomicInteger(0);
					for (String dsp : dsps) {
						logger.debug("processing dsp {}", dsp);
						final String tag = dsp.toLowerCase();
						for (String origin : origins) {
							logger.debug("processing origin {}", origin);

							try (Statement stmt = connection.createStatement()) {
								final String sql = new StringBuilder()
										.append("select codice_opera, title, artists_names, an_origin_vector, an_idutil_src_matrix")
										.append("  from sophia_ca_opera")
										.append(" where an_origin_vector like '%").append(origin).append("%'")
										.append("   and an_idutil_src_matrix like '%").append(dsp).append("%'")
										.append("   and valid = 1")
										//.append(" limit 100")
										.toString();
								try (ResultSet rset = stmt.executeQuery(sql)) {
									while (rset.next()) {

										final String code = SiadaCode.normalize(rset.getString("codice_opera"));
										if (null == code) {
											logger.warn("bad work code {}", rset.getString("codice_opera"));
											continue;
										}

										// title(s)
										String title = rset.getString("title");
										HashSet<Title> titles = null;
										title = titleNormalizer.normalize(toUpperCase ? title.toUpperCase() : title);
										if (!Strings.isNullOrEmpty(title)) {
											titles = new HashSet<>();
											titles.add(new Title(title, tag));
										}

										// artist(s)
										final String[] artistsNames = rset.getString("artists_names").split("\\|");
										final String[] anOriginVector = rset.getString("an_origin_vector").split("\\|");
										final String[] anIdUtilSrcMatrix = rset.getString("an_idutil_src_matrix").split("\\|\\|");
										HashSet<String> names = new HashSet<>();
										for (int i = 0; i < artistsNames.length; i++) {
											if (origin.equalsIgnoreCase(anOriginVector[i]) &&
													anIdUtilSrcMatrix[i].toLowerCase().contains(dsp)) {
												String name = artistNormalizer.normalize(toUpperCase ?
														artistsNames[i].toUpperCase() : artistsNames[i]);
												if (!Strings.isNullOrEmpty(name)) {
													names.add(name);
												}
											}
										}
										HashSet<Artist> artists = null;
										if (!names.isEmpty()) {
											artists = new HashSet<>();
											for (String name : names) {
												artists.add(new Artist(name, role, tag));
											}
										}

										// update work if exists
										if (null != titles || null != artists) {
											final Work work = masterDatabase.getByCode(code);
											if (null != work) {
												if (null != titles) {
													if (null != work.titles) {
														work.titles.addAll(titles);
													} else {
														work.titles = titles;
													}
												}
												if (null != artists) {
													if (null != work.artists) {
														work.artists.addAll(artists);
													} else {
														work.artists = artists;
													}
												}
												masterDatabase.replaceAlways(work);
											}
										}

										// heartbeat
										if (0 == (rownum.incrementAndGet() % heartbeat)) {
											logger.debug("{}k", rownum.get() / 1000);
										}
									}
								}catch (Exception e){
									exitCode = 1 ;
									logger.info("Error :"+e.getMessage());
								}
							}catch (Exception e){
								exitCode = 1 ;
								logger.info("Error :"+e.getMessage());
							}
						}
					}
					logger.debug("{} records extracted", rownum.get());
					logger.debug("extraction completed in {}s", (System.currentTimeMillis() - startTimeMillis) / 1000L);
				}catch (Exception e){
					exitCode = 1 ;
					logger.info("Error :"+e.getMessage());
				}
			}catch (IOException e){
				exitCode = 1 ;
				logger.info("Error :"+e.getMessage());
			}
		}catch (NullPointerException e){
			exitCode = 1 ;
			logger.info("Error :"+e.getMessage());
		}
		return this;
	}

}