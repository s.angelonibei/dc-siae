package com.alkemytech.sophia.recognizer.norm;

import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
@Singleton
public class Norms {

	/**
	 * Identity norm
	 */
	private class Identity implements Norm {
		
		@Override
		public double normalize(double value) {
			return value;
		}
		
		@Override
		public String getName() {
			return "Identity";
		}
		
	}
	
	/**
	 * Divide value by maxValue
	 * @see masterDatabaseStatistics.properties
	 */
	private class MaxValue implements Norm {
		
		private final double maxValue = Double
				.parseDouble(configuration.getProperty("maxValueNorm.maxValue"));
		
		@Override
		public double normalize(double value) {
			if (value <= 0.0) {
				return 0.0;
			}
			return value / maxValue;
		}
		
		@Override
		public String getName() {
			return "MaxValue";
		}
		
	}

	private final Properties configuration;
	private final Map<String, Norm> instances;

	@Inject
	protected Norms(@Named("configuration") Properties configuration) {
		super();
		this.configuration = configuration;
		this.instances = new ConcurrentHashMap<>();
		add(new Identity());
		add(new MaxValue());
	}
	
	public Norm add(Norm norm) {
		return instances.put(norm.getName(), norm);
	}
	
	public Norm getInstance(String name) {
		final Norm norm = instances.get(name);
		if (null == norm) {
			throw new IllegalArgumentException("unknown norm \"" + name + "\"");
		}
		return norm;
	}
		
}
