package com.alkemytech.sophia.recognizer.score;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.recognizer.metric.Metric;
import com.alkemytech.sophia.recognizer.metric.Metrics;
import com.alkemytech.sophia.recognizer.model.ArtistRole;
import com.alkemytech.sophia.recognizer.model.ScoredArtist;
import com.alkemytech.sophia.recognizer.model.ScoredTitle;
import com.alkemytech.sophia.recognizer.model.Work;
import com.alkemytech.sophia.recognizer.norm.Norm;
import com.alkemytech.sophia.recognizer.norm.Norms;
import com.alkemytech.sophia.recognizer.utilization.Utilization;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class ScoreCalculator {
	
	private static final Logger logger = LoggerFactory.getLogger(ScoreCalculator.class);
		
	private static interface Operator {
		public double evaluate(double lvalue, double rvalue);
	}

	private final Operator operator;
	private final double weightTitle;
	private final double weightAuthor;
	private final double weightComposer;
	private final double weightPerformer;
	private final double weightValue;
	private final double exponentTitle;
	private final double exponentAuthor;
	private final double exponentComposer;
	private final double exponentPerformer;
	private final double exponentValue;
	private final Metric titleMetric;
	private final Metric artistMetric;
	private final Norm valueNorm;
	
	@Inject
	public ScoreCalculator(@Named("configuration") Properties configuration,
			Metrics metrics, Norms norms) {
		super();
		final String operator = configuration
				.getProperty("scoreCalculator.operator", "+");
		if ("+".equals(operator)) {
			this.operator = (double lvalue, double rvalue)->(lvalue + rvalue);
		} else if ("*".equals(operator)) {
			this.operator = (double lvalue, double rvalue)->(lvalue * rvalue);
		} else {
			throw new IllegalArgumentException("invalid operator " + operator);
		}
		this.weightTitle = Double.parseDouble(configuration
				.getProperty("scoreCalculator.weight.title"));
		this.weightAuthor = Double.parseDouble(configuration
				.getProperty("scoreCalculator.weight.artist.author"));
		this.weightComposer = Double.parseDouble(configuration
				.getProperty("scoreCalculator.weight.artist.composer"));
		this.weightPerformer = Double.parseDouble(configuration
				.getProperty("scoreCalculator.weight.artist.performer"));
		this.weightValue = Double.parseDouble(configuration
				.getProperty("scoreCalculator.weight.value"));
		this.exponentTitle = Double.parseDouble(configuration
				.getProperty("scoreCalculator.exponent.title"));
		this.exponentAuthor = Double.parseDouble(configuration
				.getProperty("scoreCalculator.exponent.artist.author"));
		this.exponentComposer = Double.parseDouble(configuration
				.getProperty("scoreCalculator.exponent.artist.composer"));
		this.exponentPerformer = Double.parseDouble(configuration
				.getProperty("scoreCalculator.exponent.artist.performer"));
		this.exponentValue = Double.parseDouble(configuration
				.getProperty("scoreCalculator.exponent.value"));
		this.titleMetric = metrics.getInstance(configuration
				.getProperty("scoreCalculator.metric.title"));
		this.artistMetric = metrics.getInstance(configuration
				.getProperty("scoreCalculator.metric.artist"));
		this.valueNorm = norms.getInstance(configuration
				.getProperty("scoreCalculator.norm.value"));
		final double weightSum = Math.abs(weightTitle + Math.max(Math
				.max(weightAuthor, weightComposer), weightPerformer) + weightValue);
		if (weightSum > 1.0) {
			logger.warn("non unit weights sum {}", weightSum);
		}
	}

	public ScoreCalculator startup() {
		return this;
	}

	public ScoreCalculator shutdown() {
		return this;
	}

	public double computeTitleScore(Utilization utilization, Work work) {
		return work.getTitleSimilarity(utilization, titleMetric).score;
	}
	
	public double computeArtistScore(Utilization utilization, Work work, ArtistRole role) {
		return work.getArtistSimilarity(utilization, titleMetric, role).score;
	}

	public ScoreResult computeScore(Utilization utilization, Work work) {
		// title
		final ScoredTitle scoredTitle = work
				.getTitleSimilarity(utilization, titleMetric);
		final double scoreTitle = weightTitle * Math.pow(scoredTitle.score, exponentTitle);
		// author
		final ScoredArtist scoredAuthor = work
				.getArtistSimilarity(utilization, artistMetric, ArtistRole.AUTHOR);
		final double scoreAuthor = weightAuthor * Math.pow(scoredAuthor.score, exponentAuthor);
		// composer
		final ScoredArtist scoredComposer = work
				.getArtistSimilarity(utilization, artistMetric, ArtistRole.COMPOSER);
		final double scoreComposer = weightComposer * Math.pow(scoredComposer.score, exponentComposer);
		// performer
		final ScoredArtist scoredPerformer = work
				.getArtistSimilarity(utilization, artistMetric, ArtistRole.PERFORMER);
		final double scorePerformer = weightPerformer * Math.pow(scoredPerformer.score, exponentPerformer);
		// artist
		final ScoredArtist scoredArtist;
		final double scoreArtist;
		if (scoreAuthor >= scoreComposer) {
			if (scoreAuthor >= scorePerformer) {
				scoredArtist = scoredAuthor;
				scoreArtist = scoreAuthor;
			} else {
				scoredArtist = scoredPerformer;
				scoreArtist = scorePerformer;
			}
		} else if (scoreComposer >= scorePerformer) {
			scoredArtist = scoredComposer;
			scoreArtist = scoreComposer;
		} else {
			scoredArtist = scoredPerformer;	
			scoreArtist = scorePerformer;
		}
		// value
		final double normalizedValue = work.getNormalizedValue(valueNorm);
		final double scoreValue = weightValue * Math.pow(normalizedValue, exponentValue);
		// result
		return new ScoreResult(utilization, work,
				scoredTitle, scoredArtist, scoredAuthor, scoredComposer, scoredPerformer,
				operator.evaluate(operator.evaluate(scoreTitle, scoreArtist), scoreValue));
	}

}