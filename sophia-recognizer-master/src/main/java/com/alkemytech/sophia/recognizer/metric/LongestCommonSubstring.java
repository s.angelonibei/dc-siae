package com.alkemytech.sophia.recognizer.metric;

/**
 * Longest Common Substring
 * 
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class LongestCommonSubstring implements Meter {
	
	public static int longestCommonSubstring(char[] a, char[] b) {
		int m = a.length;
		int n = b.length;
		int cost = 0;
		int lcs = 0;
		int[] p = new int[n];
		int[] d = new int[n];
		for (int i = 0; i < m; ++i) {
			for (int j = 0; j < n; ++j) {
				if (a[i] != b[j]) {
					cost = 0;
				} else if (0 == i || 0 == j) {
					cost = 1;
				} else {
					cost = p[j-1] + 1;
				}
				d[j] = cost;
				if (cost > lcs) {
					lcs = cost;
				}
			}
			int[] swap = p;
			p = d;
			d = swap;
		}
		return lcs;
	}

	public static int longestCommonSubstring(String a, String b) {
		return longestCommonSubstring(a.toCharArray(), b.toCharArray());
	}
	
	@Override
	public double measure(String a, String b) {
		return longestCommonSubstring(a.toCharArray(), b.toCharArray());
	}

}
