package com.alkemytech.sophia.recognizer.metric;

import java.util.Arrays;

/**
 * Damerau-Levenshtein distance with transposition (also
 * sometimes calls unrestricted Damerau-Levenshtein distance) is the
 * minimum number of operations needed to transform one string into
 * the other, where an operation is defined as an insertion, deletion, or
 * substitution of a single character, or a transposition of two adjacent
 * characters.
 * 
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class DamerauLevenshtein implements Meter {

	private static final char[] ALPHABETH = new char[128]; // 7-bits ASCII chars
	
	static {
		for (int i = 0; i < ALPHABETH.length; i ++) {
			ALPHABETH[i] = (char) i;
		}
	}
	
	public static int damerauLevenshtein(char[] a, char[] b) {
		final int[] da = new int[ALPHABETH.length];
		for (int i = 0; i < da.length; i++) {
	        da[i] = 0;
		}
		final int[][] d = new int[a.length + 2][b.length + 2];
		final int maxdist = a.length + b.length;
		d[0][0] = maxdist;
		for (int i = 0; i <= a.length; i ++) {
	        d[i+1][0] = maxdist;
	        d[i+1][1] = i;
		}
		for (int j = 0; j <= b.length; j ++) {
	        d[0][j+1] = maxdist;
	        d[1][j+1] = j;
		}
		int cost = 0;
		for (int i = 1; i <= a.length; i ++) {
	        int db = 0;
	        for (int j = 1; j <= b.length; j ++) { 
	            final int k = da[Arrays.binarySearch(ALPHABETH, b[j-1])];
	            final int l = db;
	            if (a[i-1] == b[j-1]) {
	                cost = 0;
	                db = j;
	            } else {
	                cost = 1;
	            }
	            d[i+1][j+1] = Math.min(Math.min(Math
            		.min(d[i][j] + cost, // substitution
        				d[i+1][j] + 1), // insertion
        				d[i][j+1] + 1), // deletion
        				d[k][l] + (i-k-1) + 1 + (j-l-1)); // transposition
	            da[Arrays.binarySearch(ALPHABETH, a[i-1])] = i;
	        }
		}
	    return d[a.length+1][b.length+1];
	}

	public static int damerauLevenshtein(String a, String b) {
		return damerauLevenshtein(a.toCharArray(), b.toCharArray());
	}
	
	@Override
	public double measure(String a, String b) {
		return damerauLevenshtein(a.toCharArray(), b.toCharArray());
	}
	
}
