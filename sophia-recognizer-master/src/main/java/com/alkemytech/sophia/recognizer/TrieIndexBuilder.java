package com.alkemytech.sophia.recognizer;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.recognizer.db.MasterDatabase;
import com.alkemytech.sophia.recognizer.model.Artist;
import com.alkemytech.sophia.recognizer.model.Title;
import com.alkemytech.sophia.recognizer.model.Work;
import com.alkemytech.sophia.recognizer.regex.StringNormalizer;
import com.alkemytech.sophia.recognizer.trie.MMapBitTrie;
import com.google.common.base.Strings;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class TrieIndexBuilder {

    private static final Logger logger = LoggerFactory.getLogger(TrieIndexBuilder.class);
    static int exitCode = 0;

    protected static class GuiceModuleExtension extends GuiceModule {

        public GuiceModuleExtension(String[] args) {
            super(args);
        }

        @Override
        protected void configure() {
            super.configure();
            // singleton(s)
            bind(MasterDatabase.class).asEagerSingleton();
            bind(TrieIndexBuilder.class).asEagerSingleton();
        }

    }

    public static void main(String[] args) {
        try {
            final TrieIndexBuilder instance = Guice.createInjector(new GuiceModuleExtension(args))
                    .getInstance(TrieIndexBuilder.class)
                    .startup();
            try {
                instance.process();
            } finally {
                instance.shutdown();
            }
        } catch (Throwable e) {
            logger.error("main", e);
            exitCode = 1;
        } finally {
            System.exit(exitCode);
        }
    }

    private final Properties configuration;
    private final MasterDatabase masterDatabase;
    private final NumberSpeller numberSpeller;

    @Inject
    protected TrieIndexBuilder(@Named("configuration") Properties configuration,
                               MasterDatabase masterDatabase, NumberSpeller numberSpeller) {
        super();
        this.configuration = configuration;
        this.masterDatabase = masterDatabase;
        this.numberSpeller = numberSpeller;
    }

    public TrieIndexBuilder startup() {
        if ("true".equalsIgnoreCase(configuration.getProperty("trieIndexBuilder.locked", "true"))) {
            throw new IllegalStateException("application locked");
        }
        masterDatabase.startup();
        numberSpeller.startup();
        return this;
    }

    public TrieIndexBuilder shutdown() {
        numberSpeller.shutdown();
        masterDatabase.shutdown();
        return this;
    }

    public TrieIndexBuilder process() throws IOException {
        try {
            final File homeFolder = new File(configuration
                    .getProperty("trieIndexBuilder.homeFolder"));
            final File indexFolder = new File(homeFolder, configuration
                    .getProperty("trieIndexBuilder.indexFolder"));
            final HashSet<String> includeTags = new HashSet<>(Arrays.asList(configuration
                    .getProperty("trieIndexBuilder.includeTags").split(",")));
            final StringNormalizer titleNormalizer = new StringNormalizer(exitCode, configuration
                    .getProperty("trieIndexBuilder.normalizer.title").split(","));
            final StringNormalizer artistNormalizer = new StringNormalizer(exitCode, configuration
                    .getProperty("trieIndexBuilder.normalizer.artist").split(","));
            final boolean spellTitleNumbers = "true".equalsIgnoreCase(configuration
                    .getProperty("trieIndexBuilder.spellTitleNumbers"));
            final int heartbeat = Integer.parseInt(configuration
                    .getProperty("trieIndexBuilder.heartbeat", configuration.getProperty("default.heartbeat", "1000")));
            final int bindPort = Integer.parseInt(configuration
                    .getProperty("trieIndexBuilder.bindport", configuration.getProperty("default.bindport", "0")));

            // bind to lock tcp port
            try (ServerSocket socket = new ServerSocket(bindPort)) {

                // title(s) trie
                if ("true".equalsIgnoreCase(configuration.getProperty("trieIndexBuilder.create.title"))) {
                    final File titleFolder = new File(indexFolder, "title.trie");
                    final MMapBitTrie titleTrie = new MMapBitTrie(titleFolder, false);

                    logger.debug("building title index from master database");
                    long startTimeMillis = System.currentTimeMillis();
                    final AtomicInteger rownum = new AtomicInteger(0);
                    masterDatabase.selectAllWorks(new Work.Selector() {

                        @Override
                        public Work.Selector select(Work work) {
                            // title(s)
                            if (includeTags.contains(work.title.tag)) {
                                String text = titleNormalizer.normalize(work.title.title);
                                if (!Strings.isNullOrEmpty(text)) {
                                    titleTrie.insert(text);
                                }
                                if (spellTitleNumbers) { // spell title number(s) if any
                                    for (String spelledTitle : numberSpeller.spell(work.title.title)) {
                                        text = titleNormalizer.normalize(spelledTitle);
                                        if (!Strings.isNullOrEmpty(text)) {
                                            titleTrie.insert(text);
                                        }
                                    }
                                }
                            }
                            for (Title title : work.titles) {
                                if (includeTags.contains(title.tag)) {
                                    String text = titleNormalizer.normalize(title.title);
                                    if (!Strings.isNullOrEmpty(text)) {
                                        titleTrie.insert(text);
                                    }
                                    if (spellTitleNumbers) { // spell title number(s) if any
                                        for (String spelledTitle : numberSpeller.spell(title.title)) {
                                            text = titleNormalizer.normalize(spelledTitle);
                                            if (!Strings.isNullOrEmpty(text)) {
                                                titleTrie.insert(text);
                                            }
                                        }
                                    }
                                }
                            }
                            // heartbeat
                            if (0 == (rownum.incrementAndGet() % heartbeat)) {
                                logger.debug("{}k", rownum.get() / 1000);
                            }
                            return this;
                        }

                    },exitCode);
                    logger.debug("{} parsed records", rownum.get());
                    logger.debug("title index built (in {}s)", (System.currentTimeMillis() - startTimeMillis) / 1000L);

                    titleTrie.clear();
                }

                // artist(s) trie
                if ("true".equalsIgnoreCase(configuration.getProperty("trieIndexBuilder.create.artist"))) {
                    final File artistFolder = new File(indexFolder, "artist.trie");
                    final MMapBitTrie artistTrie = new MMapBitTrie(artistFolder, false);

                    logger.debug("building artist index from master database");
                    long startTimeMillis = System.currentTimeMillis();
                    final AtomicInteger rownum = new AtomicInteger(0);
                    masterDatabase.selectAllWorks(new Work.Selector() {

                        @Override
                        public Work.Selector select(Work work) {
                            // artist(s)
                            for (Artist artist : work.artists) {
                                if (includeTags.contains(artist.tag)) {
                                    final String text = artistNormalizer.normalize(artist.name);
                                    if (!Strings.isNullOrEmpty(text)) {
                                        artistTrie.insert(text);
                                    }
                                }
                            }
                            // heartbeat
                            if (0 == (rownum.incrementAndGet() % heartbeat)) {
                                logger.debug("{}k", rownum.get() / 1000);
                            }
                            return this;
                        }

                    },exitCode);
                    logger.debug("{} parsed records", rownum.get());
                    logger.debug("artist index built (in {}s)", (System.currentTimeMillis() - startTimeMillis) / 1000L);

                    artistTrie.clear();
                }

            }catch (IOException e){
                exitCode = 1;
                logger.info("Error :"+e.getMessage());
            }
        } catch (NullPointerException e) {
            exitCode = 1;
            logger.info("Error :"+e.toString());
            e.printStackTrace();
        }
        return this;
    }

}