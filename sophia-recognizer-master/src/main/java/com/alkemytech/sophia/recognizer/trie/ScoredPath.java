package com.alkemytech.sophia.recognizer.trie;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class ScoredPath {

	public final int cost;
	public final String path;
	
	public ScoredPath(int cost, String path) {
		this.cost = cost;
		this.path = path;
	}

	@Override
	public String toString() {
		return new StringBuilder()
			.append(cost)
			.append(" \"")
			.append(path)
			.append("\"")
			.toString();
	}
	
}
