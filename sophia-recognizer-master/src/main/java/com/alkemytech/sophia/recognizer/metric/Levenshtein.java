package com.alkemytech.sophia.recognizer.metric;

/**
 * The Levenshtein distance between two strings is the minimum number of
 * single-character edits (insertions, deletions or substitutions) required to
 * change one string into the other.
 * 
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class Levenshtein implements Meter {
	
	public static int levenshtein(char[] x, char[] y) {
		final int m = x.length;
		final int n = y.length;
		int[] prev = new int[n+1];
		int[] curr = new int[n+1];
		for (int i = 0; i <= n; i++) {
	        prev[i] = i;
		}
		for (int i = 1; i <= m; i++) {
	        curr[0] = i;
	        for (int j = 1; j <= n; j++) {
	            if (x[i - 1] != y[j - 1]) {
	                int k = Math.min(Math.min(curr[j - 1], prev[j - 1]), prev[j]);
	                curr[j] = k + 1;
	            } else {
	                curr[j] = prev[j - 1];
	            }
	        }
	        int[] swap = prev;
	        prev = curr;
	        curr = swap;
	        for (int k = 0; k <= n; k++) {
	        	curr[k] = 0;
			}
	    }
	    return prev[n];
	}
	
	public static int levenshtein(String a, String b) {
		return levenshtein(a.toCharArray(), b.toCharArray());
	}

	@Override
	public double measure(String a, String b) {
		return levenshtein(a.toCharArray(), b.toCharArray());
	}
	
}
