package com.alkemytech.sophia.recognizer.regex;

import java.util.regex.Pattern;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class RegExReplaceAll {

	private final String replacement;
	private final Pattern pattern;
	
	public RegExReplaceAll(String regex, String replacement) {
		this.replacement = replacement;
		this.pattern = Pattern.compile(regex);
	}
	
	public String replaceAll(String target) {
		return pattern.matcher(target).replaceAll(replacement);
	}
	
//	public static void main(String[] args) {
//		
//		System.out.println(Pattern.matches("^(\\$|\\*)$", "$"));
//		System.out.println(Pattern.matches("\\$|\\*", "$$"));
//		System.out.println(Pattern.matches("^(\\$|\\*)$", "GIMME $"));
//		System.exit(0);
//		
//		System.out.println("\"" + new RegExReplaceAll("[^ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789\\u0020\\.\\:\\&\\#\\@\\$\\€\\%\\?\\!\\*\\'\\-\\+\\(\\)]", " ")
//			.replaceAll(" CHE  BELLA GIORN___@===ATA  ") + "\"");
//		System.out.println("\"" + new RegExReplaceAll("[\\p{javaSpaceChar}]", "_").replaceAll(" CHE    BELLA GIORNATA   ") + "\"");
//		System.out.println("\"" + new RegExReplaceAll("A\\.K\\.A\\.", "AKA").replaceAll(" CHE  A.K.A.  BELLA GIORNATA  A.K.A. ") + "\"");
//		System.out.println("\"" + new RegExReplaceAll("[\\p{javaSpaceChar}]{2,}", " ").replaceAll(" CHE  BELLA GIORNATA  ") + "\"");
//		System.out.println("\"" + new RegExReplaceAll("[\\p{javaSpaceChar}](.*)", "$1").replaceAll(" CHE  BELLA GIORNATA  ") + "\"");
//		System.out.println("\"" + new RegExReplaceAll("(.*)[\\p{javaSpaceChar}]", "$1").replaceAll(" CHE  BELLA GIORNATA  ") + "\"");
//	}

}
