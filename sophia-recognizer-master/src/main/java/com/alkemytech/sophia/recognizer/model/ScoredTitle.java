package com.alkemytech.sophia.recognizer.model;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class ScoredTitle {
	
	public final Title title;
	public final double score;
	
	public ScoredTitle(Title title, double score) {
		super();
		this.title = title;
		this.score = score;
	}

	@Override
	public int hashCode() {
		return 31 + ((null == title) ? 0 : title.hashCode());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (null == obj) {
			return false;
		} else if (getClass() != obj.getClass()) {
			return false;
		}
		final ScoredTitle other = (ScoredTitle) obj;
		if (null == title) {
			if (null != other.title) {
				return false;
			}
		} else if (!title.equals(other.title)) {
			return false;
		}
		return true;
	}
	
	
}
