package com.alkemytech.sophia.recognizer.utilization;

import java.io.Closeable;
import java.io.IOException;
import java.io.Writer;
import java.util.Iterator;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.QuoteMode;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class UtilizationWriter implements Closeable {

	private final Writer writer;
	private final CSVPrinter printer;

	public UtilizationWriter(Writer writer) throws IOException {
		super();
		this.writer = writer;
		this.printer = new CSVPrinter(writer, CSVFormat.DEFAULT
				.withIgnoreEmptyLines()
				.withNullString("")
				.withQuoteMode(QuoteMode.NON_NUMERIC)
				.withQuote('"')
				.withIgnoreSurroundingSpaces()
				.withCommentMarker('#'));
	}

	@Override
	public void close() throws IOException {
		if (null != printer) {
			printer.close();
		}
		if (null != writer) {
			writer.close();
		}
	}
	
	public UtilizationWriter writeHeader() throws IOException {
		printer.print("ID");
		printer.print("TITOLO");
		printer.print("ARTISTA 1");
		printer.print("ARTISTA 2");
		printer.print("ARTISTA 3");
		printer.print("CODICE OPERA");
		printer.println();
		return this;
	}

	public UtilizationWriter write(Utilization utilization) throws IOException {
		printer.print(utilization.id);
		printer.print(utilization.title);
		final Iterator<String> iterator = utilization.artists.iterator();
		for (int i = 0; i < 3; i ++) {
			if (iterator.hasNext()) {
				printer.print(iterator.next());
			} else {
				printer.print(null);
			}
		}
		printer.print(utilization.code);
		printer.println();
		return this;
	}

}
