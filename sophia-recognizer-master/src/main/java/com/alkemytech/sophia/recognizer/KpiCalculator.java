package com.alkemytech.sophia.recognizer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.recognizer.db.MasterDatabase;
import com.alkemytech.sophia.recognizer.kpi.KPIs;
import com.alkemytech.sophia.recognizer.model.Work;
import com.alkemytech.sophia.recognizer.siada.OutputForSiadaReader;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class KpiCalculator {
	
	private static final Logger logger = LoggerFactory.getLogger(KpiCalculator.class);

	protected static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args) {
			super(args);
		}

		@Override
		protected void configure() {
			super.configure();
			// singleton(s)
			bind(MasterDatabase.class).asEagerSingleton();
			bind(KpiCalculator.class).asEagerSingleton();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			final KpiCalculator instance = Guice.createInjector(new GuiceModuleExtension(args))
				.getInstance(KpiCalculator.class)
					.startup();
			try {
				instance.process();
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}
	
	private final Properties configuration;
	private final MasterDatabase masterDatabase;
	private final Charset charset;
//	private final boolean debug;
	
	@Inject
	protected KpiCalculator(@Named("configuration") Properties configuration,
			MasterDatabase masterDatabase, @Named("charset") Charset charset) {
		super();
		this.configuration = configuration;
		this.masterDatabase = masterDatabase;
		this.charset = charset;
//		this.debug = "true".equalsIgnoreCase(configuration
//				.getProperty("kpiCalculator.debug", configuration.getProperty("default.debug")));
	}
	
	public KpiCalculator startup() {
		masterDatabase.startup();
		return this;
	}

	public KpiCalculator shutdown() {
		masterDatabase.shutdown();
		return this;
	}

	public KpiCalculator process() throws IOException {
		
		final File homeFolder = new File(configuration.getProperty("kpiCalculator.homeFolder"));
		final Pattern filenames = Pattern.compile(configuration.getProperty("kpiCalculator.filenames"));
		
		// build sorted file list
		final ArrayList<File> files = new ArrayList<>();
		for (File file : homeFolder.listFiles()) {
			if (file.isFile() &&
					filenames.matcher(file.getName()).matches()) {
				files.add(file);
			}
		}
		files.sort((File a, File b)->a.getName().compareTo(b.getName()));

		// process files
		final long startTimeMillis = System.currentTimeMillis();
		final AtomicInteger rownum = new AtomicInteger(0);
		for (File file : files) {
			logger.debug("parsing file {}", file);

			final int heartbeat = Integer.parseInt(configuration.getProperty("default.heartbeat", "1000"));
			final KPIs scoreKPIs = new KPIs();
			try (OutputForSiadaReader reader = new OutputForSiadaReader(new FileReader(file), masterDatabase)) {
				while (reader.next()) {
					final Work verifiedWork = reader.getVerifiedWork();
					if (null == verifiedWork) {
						continue; // verified only
					}
					try {
						scoreKPIs.update(reader.getScoreResults(), verifiedWork);
					} catch (RuntimeException e) {
						logger.warn("process", e);							
					}				
					// heartbeat
		        	if (0 == (rownum.incrementAndGet() % heartbeat)) {
						logger.debug("{}k", rownum.get() / 1000);
					}
				}
			}
			
			// print to stdout
			System.out.println(scoreKPIs);

			// save KPIs for file
			final Matcher matcher = filenames.matcher(file.getName());
			if (matcher.matches()) {
				if (matcher.groupCount() > 0) {
					final File kpisFile = new File(homeFolder, matcher.group(1) + "_KPIs.txt");
					try (OutputStream out = new FileOutputStream(kpisFile)) {
						out.write(scoreKPIs.toString().getBytes(charset));
					}
				}
			}

		}
		logger.debug("{} parsed utilizations", rownum.get());
		logger.debug("kpi computed in {}s", (System.currentTimeMillis() - startTimeMillis) / 1000L);			
		
		return this;
	}

}