package com.alkemytech.sophia.recognizer.norm;

/**
 * A Norm to normalize generic values
 * 
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public interface Norm {

	/**
	 * Returns a normalized value between 0.0 and 1.0. 
	 * 
	 * @param value a value
	 * @return a normalized value between 0.0 and 1.0
	 */
	public double normalize(double value);
	
	/**
	 * Norm name.
	 * 
	 * @return the norm name
	 */
	public String getName();

}
