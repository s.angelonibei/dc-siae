package com.alkemytech.sophia.recognizer.model;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public enum ArtistRole {

	AUTHOR("author"), COMPOSER("composer"), PERFORMER("performer");
	
	public static ArtistRole parse(String string) {
		for (ArtistRole role : values()) {
			if (role.toString().equalsIgnoreCase(string)) {
				return role;
			}
		}
		throw new IllegalArgumentException("unknown role " + string);
	}

	private final String role;
	
	private ArtistRole(String role) {
		this.role = role;
	}
	
	@Override
	public String toString() {
		return role;
	}
	
}
