package com.alkemytech.sophia.recognizer.lucene;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.QuoteMode;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Collector;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.LeafCollector;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Scorer;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.MMapDirectory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.recognizer.regex.StringNormalizer;
import com.alkemytech.sophia.recognizer.search.WorkCollector;
import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class LuceneSearch {
	
	private static final Logger logger = LoggerFactory.getLogger(LuceneSearch.class);
	
	private class WorkCollectorWrapper implements Collector {

		final WorkCollector workCollector;
				
		public WorkCollectorWrapper(WorkCollector workCollector) {
			super();
			this.workCollector = workCollector;
		}
		
		@Override
		public boolean needsScores() {
			return false;
		}
		
		@Override
		public LeafCollector getLeafCollector(LeafReaderContext context) throws IOException {
			final int docBase = context.docBase;
			return new LeafCollector() {
				
				@Override
				public void collect(int docId) throws IOException {
					if (workCollector.isCollecting()) {
						final Document document = indexSearcher.doc(docBase + docId);
						//logger.debug("collecting {} {}", docBase + docId, document.getField(LuceneField.ID).numericValue());
						workCollector.collect(document.getField(LuceneField.ID)
								.numericValue().longValue());
					}
				}

				@Override
				public void setScorer(Scorer scorer) throws IOException { }
				
			};
		}
		
	}
	
	private AtomicInteger startupCalls;
	private final File homeFolder;
	private final File indexFolder;
	private final String analyzerName;
	private final int maxClauseCount;
	private final StringNormalizer titleNormalizer;
	private final StringNormalizer artistNormalizer;
	private final TermFrequencyClassifier titleTermFrequencyClassifier;
	private final int minTermsForCouples;
	private final int minTermsForTriplets;
	private final boolean singleCharArtistsWildcard;
	private final int artistStartsWithLength;
	private final String artistQueryJoinOperator;
	private final boolean titleOnlyAllFrequent;
	private final boolean debug;

	private Directory directory;
	private Analyzer analyzer;
	private DirectoryReader directoryReader;
	private IndexSearcher indexSearcher;
	private QueryParser queryParser;

	@Inject
	protected LuceneSearch(@Named("configuration") Properties configuration) {
		super();
		this.startupCalls = new AtomicInteger(0);
		this.homeFolder = new File(configuration.getProperty("luceneSearch.homeFolder"));
		this.indexFolder = new File(homeFolder, configuration.getProperty("luceneSearch.indexFolder"));
		this.analyzerName = configuration
				.getProperty("luceneSearch.analyzer", "default");
		this.maxClauseCount = Integer.parseInt(configuration
				.getProperty("luceneSearch.maxClauseCount", "1024"));
		this.titleNormalizer = new StringNormalizer(configuration
				.getProperty("luceneSearch.normalizer.title").split(","));
		this.artistNormalizer = new StringNormalizer(configuration
				.getProperty("luceneSearch.normalizer.artist").split(","));
		this.titleTermFrequencyClassifier = new TermFrequencyClassifier() {
			
			private final Set<String> frequentTitleTerms = loadFrequentTerms(new File(homeFolder, 
						configuration.getProperty("luceneSearch.frequentTitleTerms")),
					Integer.parseInt(configuration.getProperty("luceneSearch.frequentTitleTermsNumber", "-1")));
			
			@Override
			public boolean isFrequent(String term) {
				return frequentTitleTerms.contains(term);
			}

		};
		this.minTermsForCouples = Integer.parseInt(configuration
				.getProperty("luceneSearch.minTermsForCouples", "2"));
		this.minTermsForTriplets = Integer.parseInt(configuration
				.getProperty("luceneSearch.minTermsForTriplets", "5"));
		this.titleOnlyAllFrequent = "true".equalsIgnoreCase(configuration
				.getProperty("luceneSearch.titleOnlyAllFrequent"));
		this.singleCharArtistsWildcard = "true".equalsIgnoreCase(configuration
				.getProperty("luceneSearch.singleCharArtistsWildcard"));
		this.artistStartsWithLength = Integer.parseInt(configuration
				.getProperty("luceneSearch.artistStartsWithLength", "-1"));
		this.artistQueryJoinOperator = configuration
				.getProperty("luceneSearch.artistQueryJoinOperator");
		this.debug = "true".equalsIgnoreCase(configuration
				.getProperty("luceneSearch.debug", configuration.getProperty("default.debug")));
	}
	
	public LuceneSearch startup() throws IOException {
		if (0 == startupCalls.getAndIncrement()) {
			BooleanQuery.setMaxClauseCount(maxClauseCount);
			directory = MMapDirectory.open(indexFolder.toPath());
			analyzer = LuceneAnalyzers.get(analyzerName);
			directoryReader = DirectoryReader.open(directory);
			indexSearcher = new IndexSearcher(directoryReader);
			queryParser = new QueryParser(null, analyzer);
		}
		return this;
	}

	public LuceneSearch shutdown() throws IOException {
		if (0 == startupCalls.decrementAndGet()) {
			if (null != directoryReader) {
				directoryReader.close();
				directoryReader = null;
			}
			analyzer.close();
			directory.close();
		}
		return this;
	}
	
	private Set<String> loadFrequentTerms(File file, int maxRecords) {
		Set<String> frequentTerms = new HashSet<>();
		try (FileReader reader = new FileReader(file)) {
			try (CSVParser parser = new CSVParser(reader, CSVFormat.DEFAULT
					.withIgnoreEmptyLines()
					.withQuoteMode(QuoteMode.MINIMAL)
					.withQuote('"')
					.withIgnoreSurroundingSpaces()
					.withCommentMarker('#'))) {
				for (CSVRecord record : parser) {
					if (frequentTerms.size() >= maxRecords) {
						return frequentTerms;
					}
					frequentTerms.add(record.get(0));
				}
			}
		} catch (IOException e) {
			logger.error("loadFrequentTerms", e);
		}
		return frequentTerms;
	}
	
	private String fuzzyTerm(String string) {
		if (string.matches("AND|OR|NOT")) {
			return new StringBuilder()
					.append('"')
					.append(string)
					.append("\"~")
					.toString();
		}
		switch (string.length()) {
		case 0:
		case 1:
			return string;
		case 2:
			return string + "~1";
		default:
			return string + '~';
		}
	}
	
	private String startsWithTerm(String string) {
		if (string.matches("AND|OR|NOT")) {
			return new StringBuilder()
					.append('"')
					.append(string)
					.append('"')
					.toString();
		} else if (string.length() < artistStartsWithLength) {
			return string;
		} else {
			return string.substring(0, artistStartsWithLength) + "*";
		}
	}
		
	private List<List<String>> couples(Set<String> terms) {
		final List<List<String>> couples = new ArrayList<>();
		if (terms.size() > 1) {
			final String[] array = terms
					.toArray(new String[terms.size()]);
			for (int i = 0; i < array.length - 1; i ++) {
				for (int j = i + 1; j < array.length; j ++) {
					couples.add(Arrays.asList(array[i], array[j]));
				}
			}			
		}
		return couples;
	}
	
	private List<List<String>> triplets(Set<String> terms) {
		final List<List<String>> triplets = new ArrayList<>();
		if (terms.size() > 2) {
			final String[] array = terms
					.toArray(new String[terms.size()]);
			for (int i = 0; i < array.length - 1; i ++) {
				for (int j = i + 1; j < array.length; j ++) {
					for (int k = j + 1; k < array.length; k ++) {
						triplets.add(Arrays.asList(array[i], array[j], array[k]));
					}
				}
			}
		}
		return triplets;
	}
	
	private String titleQuery(String title, TermFrequencyClassifier termFrequencyClassifier) {

		// compute title term(s)
		final String normalizedTitle = titleNormalizer.normalize(title);
		final Set<String> titleTerms = new HashSet<String>();
		final Set<String> frequentTitleTerms = new HashSet<String>();
		for (String titleTerm : normalizedTitle.split("\\s")) {
			titleTerm = titleTerm.trim();
			if (!Strings.isNullOrEmpty(titleTerm)) {
				if (termFrequencyClassifier.isFrequent(titleTerm)) {
					frequentTitleTerms.add(titleTerm);
				} else {
					titleTerms.add(titleTerm);
				}
			}
		}

		// query string
		final StringBuilder query = new StringBuilder();
		
		// non-frequent term(s)
		if (!titleTerms.isEmpty()) {
			Iterator<String> iterator = titleTerms.iterator();
			query.append(LuceneField.TITLE)
					.append(':')
					.append(fuzzyTerm(iterator.next()));
			while (iterator.hasNext()) {
				query.append(" OR ")
					.append(LuceneField.TITLE)
					.append(':')
					.append(fuzzyTerm(iterator.next()));
			}
		}
		
		// frequent term(s)
		if (frequentTitleTerms.size() >= minTermsForCouples) {
			final List<List<String>> frequentTuples = (frequentTitleTerms.size() >= minTermsForTriplets ? 
					triplets(frequentTitleTerms) : couples(frequentTitleTerms));
			if (!frequentTuples.isEmpty()) {
				if (!titleTerms.isEmpty()) {
					query.append(" OR ");
				}
				Iterator<List<String>> tuplesIterator = frequentTuples.iterator();
				Iterator<String> iterator = tuplesIterator.next().iterator();
				query.append('(')
						.append(LuceneField.TITLE)
						.append(':')
						.append(fuzzyTerm(iterator.next()));
				while (iterator.hasNext()) {
					query.append(" AND ")
						.append(LuceneField.TITLE)
						.append(':')
						.append(fuzzyTerm(iterator.next()));
				}
				query.append(')');
				while (tuplesIterator.hasNext()) {
					iterator = tuplesIterator.next().iterator();
					query.append(" OR (")
							.append(LuceneField.TITLE)
							.append(':')
							.append(fuzzyTerm(iterator.next()));
					while (iterator.hasNext()) {
						query.append(" AND ")
							.append(LuceneField.TITLE)
							.append(':')
							.append(fuzzyTerm(iterator.next()));
					}
					query.append(')');
				}
			}
		} else if (titleTerms.isEmpty()) {
			if (!frequentTitleTerms.isEmpty()) {
				Iterator<String> iterator = frequentTitleTerms.iterator();
				query.append(LuceneField.TITLE)
						.append(':')
						.append(fuzzyTerm(iterator.next()));
				while (iterator.hasNext()) {
					query.append(" OR ")
						.append(LuceneField.TITLE)
						.append(':')
						.append(fuzzyTerm(iterator.next()));
				}
			} else return null;
		}
		
		return query.toString();
	}
	
	private String simpleTitleQuery(String title) {
		// title term(s)
		final String normalizedTitle = titleNormalizer.normalize(title);
		final Set<String> titleTerms = new HashSet<String>();
		for (String titleTerm : normalizedTitle.split("\\s")) {
			titleTerm = titleTerm.trim();
			if (!Strings.isNullOrEmpty(titleTerm)) {
				titleTerms.add(titleTerm);
			}
		}
		if (titleTerms.isEmpty()) {
			return null;
		}
		// query string
		final StringBuilder query = new StringBuilder();
		Iterator<String> iterator = titleTerms.iterator();
		query.append(LuceneField.TITLE)
				.append(':')
				.append(fuzzyTerm(iterator.next()));
		while (iterator.hasNext()) {
			query.append(" AND ")
				.append(LuceneField.TITLE)
				.append(':')
				.append(fuzzyTerm(iterator.next()));
		}
		return query.toString();
	}
	
	private String artistQuery(Set<String> artists) {
		
		// artist term(s)
		final Set<String> artistTerms = new HashSet<>();
		for (String artist : artists) {
			final String normalizedAtist = artistNormalizer.normalize(artist);
			for (String artistTerm : normalizedAtist.split("\\s")) {
				artistTerm = artistTerm.trim();
				if (!Strings.isNullOrEmpty(artistTerm)) {
					artistTerms.add(artistTerm);
				}
			}
		}

		// query string
		final StringBuilder query = new StringBuilder();
		
		// all term(s)
		Iterator<String> iterator = artistTerms.iterator();
		String term = iterator.next();
		if (singleCharArtistsWildcard && 1 == term.length()) {
			query.append(LuceneField.ARTIST)
				.append(':')
				.append(term)
				.append('*');
		} else {
			query.append(LuceneField.ARTIST)
				.append(':')
				.append(fuzzyTerm(term));
			if (artistStartsWithLength > 0) {
				final String startsWithTerm = startsWithTerm(term);
				if (!term.equals(startsWithTerm)) {
					query.append(" OR ")
						.append(LuceneField.ARTIST)
						.append(':')
						.append(startsWithTerm);
				}
			}
		}
		
		while (iterator.hasNext()) {
			term = iterator.next();
			if (singleCharArtistsWildcard && 1 == term.length()) {
				query.append(" OR ")
					.append(LuceneField.ARTIST)
					.append(':')
					.append(term)
					.append('*');
			} else {
				query.append(" OR ")
					.append(LuceneField.ARTIST)
					.append(':')
					.append(fuzzyTerm(term));
				if (artistStartsWithLength > 0) {
					final String startsWithTerm = startsWithTerm(term);
					if (!term.equals(startsWithTerm)) {
						query.append(" OR ")
							.append(LuceneField.ARTIST)
							.append(':')
							.append(startsWithTerm);
					}
				}
			}
		}
		
		return query.toString();
	}
	
	private String simpleArtistQuery(Set<String> artists) {
		// artist term(s)
		final Set<String> artistTerms = new HashSet<>();
		for (String artist : artists) {
			final String normalizedAtist = artistNormalizer.normalize(artist);
			for (String artistTerm : normalizedAtist.split("\\s")) {
				artistTerm = artistTerm.trim();
				if (!Strings.isNullOrEmpty(artistTerm)) {
					artistTerms.add(artistTerm);
				}
			}
		}
		if (artistTerms.isEmpty()) {
			return null;
		}
		// query string
		final StringBuilder query = new StringBuilder();
		Iterator<String> iterator = artistTerms.iterator();
		query.append(LuceneField.ARTIST)
				.append(':')
				.append(fuzzyTerm(iterator.next()));
		while (iterator.hasNext()) {
			query.append(" OR ")
				.append(LuceneField.ARTIST)
				.append(':')
				.append(fuzzyTerm(iterator.next()));
		}
		return query.toString();
	}
	
	public LuceneSearch simpleSearchTitleWithArtist(String title, Set<String> artists, final WorkCollector workCollector) throws ParseException, IOException {
		if (!workCollector.isCollecting()) {
			return this;
		}

		String queryString = simpleTitleQuery(title);
		if (null == queryString) {
			return this;
		}
		queryString = new StringBuilder()
			.append('(')
			.append(queryString)
			.append(") AND (")
			.append(simpleArtistQuery(artists))
			.append(')')
			.toString();
		if (debug) logger.debug("query string {}", queryString);

		final Query query;
		synchronized (queryParser) {			
			query = queryParser.parse(queryString.toString());
		}
		indexSearcher.search(query, new WorkCollectorWrapper(workCollector));

		return this;
	}
	
	public LuceneSearch searchTitleWithArtist(String title, Set<String> artists, final WorkCollector workCollector) throws ParseException, IOException {
		if (!workCollector.isCollecting()) {
			return this;
		}

		String queryString = titleQuery(title, titleTermFrequencyClassifier);
		if (null == queryString) {
			return this;
		} else if (!Strings.isNullOrEmpty(artistQueryJoinOperator)) {
			queryString = new StringBuilder()
				.append('(')
				.append(queryString)
				.append(") ")
				.append(artistQueryJoinOperator)
				.append(" (")
				.append(artistQuery(artists))
				.append(')')
				.toString();
		}
		if (debug) logger.debug("query string {}", queryString);

		final Query query;
		synchronized (queryParser) {			
			query = queryParser.parse(queryString.toString());
		}
		indexSearcher.search(query, new WorkCollectorWrapper(workCollector));

		return this;
	}

	public LuceneSearch searchTitle(String title, final WorkCollector workCollector) throws ParseException, IOException {
		if (!workCollector.isCollecting()) {
			return this;
		}

		final String queryString = titleOnlyAllFrequent ?
			titleQuery(title, new TermFrequencyClassifier() {
				@Override
				public boolean isFrequent(String term) {
					return true;
				}
			}) :
			titleQuery(title, titleTermFrequencyClassifier);
		if (null == queryString) {
			return this;
		}		
		if (debug) logger.debug("query string {}", queryString);

		final Query query;
		synchronized (queryParser) {			
			query = queryParser.parse(queryString.toString());
		}
		indexSearcher.search(query, new WorkCollectorWrapper(workCollector));

		return this;
	}
	
	public LuceneSearch searchArtist(Set<String> artists, final WorkCollector workCollector) throws ParseException, IOException {
		if (!workCollector.isCollecting()) {
			return this;
		}

		final String queryString = artistQuery(artists);
		if (debug) logger.debug("query string {}", queryString);

		final Query query;
		synchronized (queryParser) {			
			query = queryParser.parse(queryString.toString());
		}
		indexSearcher.search(query, new WorkCollectorWrapper(workCollector));

		return this;
	}
	
	public LuceneSearch findTitle(String title, final WorkCollector workCollector) throws ParseException, IOException {
		if (!workCollector.isCollecting()) {
			return this;
		}

		final String queryString = new StringBuilder()
				.append(LuceneField.TITLE_NO_SPACES)
				.append(":\"")
				.append(title)
				.append('"')
				.toString();
		if (debug) logger.debug("query string {}", queryString);

		final Query query;
		synchronized (queryParser) {			
			query = queryParser.parse(queryString.toString());
		}
		indexSearcher.search(query, new WorkCollectorWrapper(workCollector));

		return this;
	}
	
	public LuceneSearch findArtist(String artist, final WorkCollector workCollector) throws ParseException, IOException {
		if (!workCollector.isCollecting()) {
			return this;
		}
		
		final String queryString = new StringBuilder()
				.append(LuceneField.ARTIST_NO_SPACES)
				.append(":\"")
				.append(artist)
				.append('"')
				.toString();
		if (debug) logger.debug("query string {}", queryString);

		final Query query;
		synchronized (queryParser) {			
			query = queryParser.parse(queryString.toString());
		}
		indexSearcher.search(query, new WorkCollectorWrapper(workCollector));

		return this;
	}
	
}