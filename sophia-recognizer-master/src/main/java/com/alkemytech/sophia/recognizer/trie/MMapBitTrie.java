package com.alkemytech.sophia.recognizer.trie;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.reflect.Method;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @see http://stevehanov.ca/blog/index.php?id=114
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class MMapBitTrie {

	private static final Logger logger = LoggerFactory.getLogger(MMapBitTrie.class);

	private abstract static class Size {

		public static final int SIZE = 4;
		
	}
	
	private static class Node {

		public static final int SIZE = 9;

		public transient int offset;
		public char ch;
		public boolean leaf;
		public int length;
		public int children;

		public Node(int offset, char ch, boolean leaf, int length, int children) {
			super();
			this.offset = offset;
			this.ch = ch; 
			this.leaf = leaf;
			this.length = length;
			this.children = children;
		}
		
	}
	
	private static class NodeRef {
		
		public static final int SIZE = 8;
		
		public transient int offset;
		public int child;
		public int next;
		
		public NodeRef(int offset, int child, int next) {
			super();
			this.offset = offset;
			this.child = child;
			this.next = next;
		}

	}
	
	private final File folder;
	private final FileChannel.MapMode mapMode;
	private final String fileMode;
	
	private MappedByteBuffer[] mmaps;
	private int size;
	
	public MMapBitTrie(File folder, boolean readOnly) {
		super();
		this.folder = folder; 
		if (readOnly) {
			this.mapMode = MapMode.READ_ONLY;
			this.fileMode = "r";
		} else {
			this.mapMode = MapMode.READ_WRITE;
			this.fileMode = "rw";
		}
		this.mmaps = new MappedByteBuffer[] { null, null, null, null };
		this.size = -1;
	}

	private File getFile(int index) {
		return new File(folder, String.format("%03x", index));
	}
	
	private int getBufferIndex(int offset) {
		return offset / 0x3fffff0; // 64Mb - 16b
	}

	private int getRelativeOffset(int offset) {
		return offset % 0x3fffff0; // 64Mb - 16b
	}
	
	private MappedByteBuffer getMmap(int offset) {
		try {
			final int index = getBufferIndex(offset);
			if (mmaps.length <= index) {
				final MappedByteBuffer[] array = new MappedByteBuffer[index + 1];
				System.arraycopy(mmaps, 0, array, 0, mmaps.length);
				Arrays.fill(array, mmaps.length, array.length, null);
				Arrays.fill(mmaps, null);
				mmaps = array;
			}
			if (null == mmaps[index]) {
				folder.mkdirs();
				try (RandomAccessFile randomAccessFile = new RandomAccessFile(getFile(index), fileMode)) {
					mmaps[index] = randomAccessFile.getChannel().map(mapMode, 0L, 0x4000000); // 64Mb
				}
			}
			return mmaps[index];
		} catch (IOException e) {
			logger.error("buffer", e);
			return null;
		}
	}
	
	private int getSize() {
		if (-1 == size) {
			if (getFile(0).length() >= Size.SIZE) {
				size = getInt(0);
			} else {
				putInt(0, size = Size.SIZE);
			}
		}
		return size;
	}

	private void setSize(int size) {
		putInt(0, this.size = size);
	}
	
	private byte getByte(int offset) {
		return getMmap(offset).get(getRelativeOffset(offset));
	}

	private void putByte(int offset, byte value) {
		getMmap(offset).put(getRelativeOffset(offset), value);
	}

	private char getChar(int offset) {
		return getMmap(offset).getChar(getRelativeOffset(offset));
	}

	private void putChar(int offset, char value) {
		getMmap(offset).putChar(getRelativeOffset(offset), value);
	}

	private short getShort(int offset) {
		return getMmap(offset).getShort(getRelativeOffset(offset));
	}

	private void putShort(int offset, short value) {
		getMmap(offset).putShort(getRelativeOffset(offset), value);
	}

	private int getInt(int offset) {
		return getMmap(offset).getInt(getRelativeOffset(offset));
	}

	private void putInt(int offset, int value) {
		getMmap(offset).putInt(getRelativeOffset(offset), value);
	}

	private Node getNode(int offset) {
		final char ch = getChar(offset);
		final boolean leaf = 0 != getByte(offset + 2);
		final int length = (int) getShort(offset + 3);
		final int children = getInt(offset + 5);
		return new Node(offset, ch, leaf, length, children);
	}
	
	private Node putNode(Node node) {
		final int offset = node.offset;
		putChar(offset, node.ch); // 2 bytes
		putByte(offset + 2, (byte) (node.leaf ? 1 : 0)); // 1 byte
		putShort(offset + 3, (short) node.length); // 2 bytes
		putInt(offset + 5, node.children); // 4 bytes
		return node;
	}

	private NodeRef getNodeRef(int offset) {
		if (offset < 1) {
			return null;
		}
		final int child = getInt(offset); // 4 bytes
		final int next = getInt(offset + 4); // 4 bytes
		return new NodeRef(offset, child, next);
	}
	
	private NodeRef putNodeRef(NodeRef nodeRef) {
		final int offset = nodeRef.offset;
		putInt(offset, nodeRef.child);
		putInt(offset + 4, nodeRef.next);
		return nodeRef;
	}

	private void cleanDirectBuffer(MappedByteBuffer buffer) {
	    if (null == buffer || !buffer.isDirect()) {
	    	return;
	    }
	    // we could use this type cast and call functions without reflection code,
	    // but static import from sun.* package is risky for non-SUN virtual machine.
	    try {
	        final Method cleaner = buffer.getClass().getMethod("cleaner");
	        cleaner.setAccessible(true);
	        final Method clean = Class.forName("sun.misc.Cleaner").getMethod("clean");
	        clean.setAccessible(true);
	        clean.invoke(cleaner.invoke(buffer));
	    } catch(Exception e) {
	    	logger.error("cleanDirectBuffer", e);
	    }
	}
	
	public MMapBitTrie clear() {
		if (null != mmaps) {
			for (int i = 0; i < mmaps.length; i ++) {
				cleanDirectBuffer(mmaps[i]);
				mmaps[i] = null;
			}
			mmaps = null;
		}
		mmaps = new MappedByteBuffer[] { null, null, null, null };
		return this;
	}
	
	public MMapBitTrie insert(String text) {
		return insert(text.toCharArray());
	}
	
	public MMapBitTrie insert(char[] text) {
		Node node;
		if (Size.SIZE == getSize()) {
			node = putNode(new Node(Size.SIZE, (char) 0, false, 0, 0));
			setSize(Size.SIZE + Node.SIZE);
		} else {
			node = getNode(Size.SIZE);
		}
		int length = text.length;
		for (char ch : text) {
			if (node.length < length) {
				node.length = length;
				putNode(node);
			}
			boolean add = true;
			for (NodeRef childRef = getNodeRef(node.children);
					null != childRef; childRef = getNodeRef(childRef.next)) {
				final Node child = getNode(childRef.child);
				if (child.ch == ch) {
					add = false;
					node = child;
					length --;
					break;
				}
			}
			if (add) {
				final int size = getSize();
				final NodeRef children = putNodeRef(new NodeRef(size, size + NodeRef.SIZE, node.children));
				final Node child = putNode(new Node(size + NodeRef.SIZE, ch, false, 0, 0));
				setSize(size + NodeRef.SIZE + Node.SIZE);
				node.children = children.offset;
				putNode(node);
				node = child;
				length --;
			}
		}
		node.leaf = true;
		putNode(node);
		return this;
	}
	
	public boolean search(String text) {
		Node node = getNode(Size.SIZE);
		for (char ch : text.toCharArray()) {
			if (0 == node.children) {
				return false;
			}
			boolean notfound = true;
			for (NodeRef childRef = getNodeRef(node.children);
					null != childRef; childRef = getNodeRef(childRef.next)) {
				final Node child = getNode(childRef.child);
				if (child.ch == ch) {
					node = child;
					notfound = false;
					break;
				}
			}
			if (notfound) {
				return false;
			}
		}
		return true;
	}
	
	public List<ScoredPath> search(String text, int maxCost) {
		return search(text.toCharArray(), maxCost);
	}

	public List<ScoredPath> search(char[] text, int maxCost) {
		final List<ScoredPath> results = new ArrayList<>();
		final Node node = getNode(Size.SIZE);
		if (0 == node.children) {
			return results;
		}
		final int columns = 1 + text.length;
		final int[] currentRow = new int[columns];
		for (int i = 0; i < columns; i++) {
			currentRow[i] = i;
		}
		for (NodeRef childRef = getNodeRef(node.children);
				null != childRef; childRef = getNodeRef(childRef.next)) {
			final Node child = getNode(childRef.child);
			search(child, text, maxCost, currentRow, "", results);
		}
		return results;
	}	
	
	private void search(Node node, char[] text, int maxCost, int[] previousRow, String path, List<ScoredPath> results) {
		path += node.ch;
		final int columns = 1 + text.length;
		final int[] currentRow = new int[columns];
		currentRow[0] = previousRow[0] + 1;
		int minCost = currentRow[0];
		for (int i = 1; i < columns; i++) {
			final int insertCost = currentRow[i-1] + 1;
			final int deleteCost = previousRow[i] + 1;
			final int replaceCost = (node.ch == text[i-1] ? previousRow[i-1] : previousRow[i-1] + 1);
			currentRow[i] = Math.min(Math.min(insertCost, deleteCost), replaceCost);
			minCost = Math.min(minCost, currentRow[i]);
		}
		if (node.leaf && currentRow[columns-1] <= maxCost) {
			results.add(new ScoredPath(currentRow[columns-1], path));
		}
		if (0 == node.children) {
			return;
		} else if (minCost > maxCost + node.length) {
			return;
		} else if (minCost <= maxCost) {
			for (NodeRef childRef = getNodeRef(node.children);
					null != childRef; childRef = getNodeRef(childRef.next)) {
				final Node child = getNode(childRef.child);
				search(child, text, maxCost, currentRow, path, results);
			}
		}
	}
	
	public void visit(PathObserver observer) throws Exception {
		final Node node = getNode(Size.SIZE);
		if (0 != node.children) {
			for (NodeRef childRef = getNodeRef(node.children);
					null != childRef; childRef = getNodeRef(childRef.next)) {
				final Node child = getNode(childRef.child);
				visit(child, observer, "");
			}
		}
	}
	
	private void visit(Node node, PathObserver observer, String path) throws Exception {
		path += node.ch;
		if (node.leaf) {
			observer.observe(path);
		}
		if (0 != node.children) {
			for (NodeRef childRef = getNodeRef(node.children);
					null != childRef; childRef = getNodeRef(childRef.next)) {
				final Node child = getNode(childRef.child);
				visit(child, observer, path);
			}
		}
	}
	
}
