package com.alkemytech.sophia.recognizer;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.recognizer.db.MasterDatabase;
import com.alkemytech.sophia.recognizer.model.Artist;
import com.alkemytech.sophia.recognizer.model.Title;
import com.alkemytech.sophia.recognizer.model.Work;
import com.alkemytech.sophia.recognizer.regex.StringNormalizer;
import com.alkemytech.sophia.recognizer.trie.Trie;
import com.alkemytech.sophia.recognizer.trie.Trie.Leaf;
import com.google.common.base.Strings;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class TermsFrequencyEvaluator {

    private static final Logger logger = LoggerFactory.getLogger(TermsFrequencyEvaluator.class);
    static int exitCode = 0;

    static class CountLeaf implements Trie.Leaf {

        public int count;

        public CountLeaf(int count) {
            this.count = count;
        }

        @Override
        public Trie.Leaf merge(Trie.Leaf leaf) {
            if (null != leaf) {
                final CountLeaf counter = (CountLeaf) leaf;
                count += counter.count;
            }
            return this;
        }

        @Override
        public Leaf read(InputStream in) throws IOException {
            throw new IllegalStateException();
        }

        @Override
        public void write(OutputStream out) throws IOException {
            throw new IllegalStateException();
        }

        @Override
        public String toString() {
            return Integer.toString(count);
        }

    }

    static class CountObserver implements Trie.Observer {

        static class Item {

            public int count;
            public String path;

            public Item(int count, String path) {
                super();
                this.count = count;
                this.path = path;
            }

        }

        final ArrayList<Item> items = new ArrayList<>();

        @Override
        public void observe(Leaf leaf, String path) {
            final CountLeaf counter = (CountLeaf) leaf;
            items.add(new Item(counter.count, path));
        }

        @Override
        public String toString() {
            items.sort((Item a, Item b) -> {
                final int rel = Integer.compare(b.count, a.count);
                return 0 != rel ? rel : a.path.compareTo(b.path);
            });
            final StringBuilder builder = new StringBuilder();
            for (Item item : items) {
                builder.append('\"')
                        .append(item.path)
                        .append("\",")
                        .append(item.count)
                        .append('\n');
            }
            return builder.toString();
        }

    }

    protected static class GuiceModuleExtension extends GuiceModule {

        public GuiceModuleExtension(String[] args) {
            super(args);
        }

        @Override
        protected void configure() {
            super.configure();
            // singleton(s)
            bind(MasterDatabase.class).asEagerSingleton();
            bind(TermsFrequencyEvaluator.class).asEagerSingleton();
        }

    }

    public static void main(String[] args) {
        try {
            final TermsFrequencyEvaluator instance = Guice.createInjector(new GuiceModuleExtension(args))
                    .getInstance(TermsFrequencyEvaluator.class)
                    .startup();
            try {
                instance.process(args);
            } finally {
                instance.shutdown();
            }
        } catch (Throwable e) {
            logger.error("main", e);
            exitCode = 1;
        } finally {
            System.exit(exitCode);
        }
    }

    private final Properties configuration;
    private final Charset charset;
    private final MasterDatabase masterDatabase;

    @Inject
    protected TermsFrequencyEvaluator(@Named("configuration") Properties configuration,
                                      @Named("charset") Charset charset, MasterDatabase masterDatabase) {
        super();
        this.configuration = configuration;
        this.charset = charset;
        this.masterDatabase = masterDatabase;
    }

    public TermsFrequencyEvaluator startup() {
        masterDatabase.startup();
        return this;
    }

    public TermsFrequencyEvaluator shutdown() {
        masterDatabase.shutdown();
        return this;
    }

    public TermsFrequencyEvaluator process(String[] args) throws Exception {
        try {
            final File homeFolder = new File(configuration.getProperty("termsFrequencyEvaluator.homeFolder"));
            final StringNormalizer titleNormalizer = new StringNormalizer(exitCode, configuration
                    .getProperty("termsFrequencyEvaluator.normalizer.title").split(","));
            final StringNormalizer artistNormalizer = new StringNormalizer(exitCode, configuration
                    .getProperty("termsFrequencyEvaluator.normalizer.artist").split(","));
            final int heartbeat = Integer.parseInt(configuration.getProperty("default.heartbeat", "1000"));
            final Trie<CountLeaf> titleTrie = new Trie<>();
            final Trie<CountLeaf> artistTrie = new Trie<>();

            logger.debug("analyzing works in master database");
            final AtomicInteger rownum = new AtomicInteger(0);
            final long startTimeMillis = System.currentTimeMillis();
            masterDatabase.selectAllWorks(new Work.Selector() {

                @Override
                public Work.Selector select(Work work) {
                    // title(s)
                    for (Title title : work.titles) {
                        String[] terms = titleNormalizer.normalize(title.title).split("\\s");
                        for (String term : terms) {
                            if (!Strings.isNullOrEmpty(term)) {
                                titleTrie.insert(term, new CountLeaf(1));
                            }
                        }
                    }
                    // artist(s)
                    for (Artist artist : work.artists) {
                        String[] terms = artistNormalizer.normalize(artist.name).split("\\s");
                        for (String term : terms) {
                            if (!Strings.isNullOrEmpty(term)) {
                                artistTrie.insert(term, new CountLeaf(1));
                            }
                        }
                    }
                    // heartbeat
                    if (0 == (rownum.incrementAndGet() % heartbeat)) {
                        logger.debug("{}k", rownum.get() / 1000);
                    }
                    return this;
                }

            }, exitCode);
            logger.debug("{} records analyzed", rownum.get());
            logger.debug("analysis completed in {}s", (System.currentTimeMillis() - startTimeMillis) / 1000L);

            CountObserver observer = new CountObserver();
            titleTrie.visit(observer);
            try (OutputStream out = new BufferedOutputStream(new FileOutputStream(new File(homeFolder, "titleTermsFrequency.csv")))) {
                out.write(observer.toString().getBytes(charset));
            } catch (Exception e) {
                exitCode = 1;
                logger.info("Error :"+e.getMessage());
            }

            observer = new CountObserver();
            artistTrie.visit(observer);
            try (OutputStream out = new BufferedOutputStream(new FileOutputStream(new File(homeFolder, "artistTermsFrequency.csv")))) {
                out.write(observer.toString().getBytes(charset));
            } catch (Exception e) {
                exitCode = 1;
                logger.info("Error :"+e.getMessage());
            }
        } catch (NullPointerException e) {
            exitCode = 1;
            logger.info("Error :"+e.toString());
            e.printStackTrace();
        }
        return this;
    }

}