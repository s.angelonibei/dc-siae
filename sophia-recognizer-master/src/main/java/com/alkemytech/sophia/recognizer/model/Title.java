package com.alkemytech.sophia.recognizer.model;

import com.alkemytech.sophia.recognizer.json.Gsonable;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.sleepycat.persist.model.Persistent;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
@Persistent
public class Title implements Gsonable {

	public static final String TITLE = "title";
	public static final String TAG = "tag";

	public String title;
	public String tag;

	public Title() {
		super();
	}
	
	public Title(String title, String tag) {
		super();
		this.title = title;
		this.tag = tag;
	}

	@Override
	public JsonObject toJsonObject() {
		final JsonObject jsonObject = new JsonObject();
		if (null != title) {
			jsonObject.addProperty(TITLE, title);
		}
		if (null != tag) {
			jsonObject.addProperty(TAG, tag);
		}
		return jsonObject;
	}
	
	@Override
	public Title initWithJsonObject(JsonObject jsonObject) {
		JsonElement jsonElement = jsonObject.get(TITLE);
		title = (null == jsonElement ? null : jsonElement.getAsString());
		jsonElement = jsonObject.get(TAG);
		tag = (null == jsonElement ? null : jsonElement.getAsString());
		return this;
	}
	
	@Override
	public String toJson(Gson gson) {
		return gson.toJson(toJsonObject());
	}

	@Override
	public Title initWithJson(String json, Gson gson) {
		return initWithJsonObject(gson.fromJson(json, JsonObject.class));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((null == tag) ? 0 : tag.hashCode());
		result = prime * result + ((null == title) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		} else if (null == object) {
			return false;
		} else if (getClass() != object.getClass()) {
			return false;
		}
		final Title that = (Title) object;
		if (null == this.title) {
			if (null != that.title) {
				return false;
			}
		} else if (!this.title.equals(that.title)) {
			return false;
		}
		if (null == this.tag) {
			if (null != that.tag) {
				return false;
			}
		} else if (!this.tag.equals(that.tag)) {
			return false;
		}
		return true;
	}
	
}
