package com.alkemytech.sophia.recognizer.trie;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @see http://stevehanov.ca/blog/index.php?id=114
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class Trie<L extends Trie.Leaf> {
	
	public static interface Leaf {
		public Leaf merge(Leaf leaf);
		public Leaf read(InputStream in) throws IOException;
		public void write(OutputStream out) throws IOException;
	}
	
	public static interface Provider {
		public Leaf provide();
	}

	public static interface Observer {
		public void observe(Leaf leaf, String path) throws Exception;
	}

	public static class Match {
		
		public final int cost;
		public final String path;
		public final Leaf leaf;
		
		public Match(int cost, String path, Leaf leaf) {
			this.cost = cost;
			this.path = path;
			this.leaf = leaf;
		}
	
		@Override
		public String toString() {
			return new StringBuilder()
				.append(cost)
				.append(" \"")
				.append(path)
				.append("\" ")
				.append(leaf)
				.toString();
		}
		
	}
	
	private static class NodeRef<L extends Trie.Leaf> {
		
		public final Trie<L> node;
		public final NodeRef<L> next;
		
		public NodeRef(Trie<L> node, NodeRef<L> prev) {
			this.node = node;
			this.next = prev;
		}
		
	}
		
	private char ch;
	private Leaf leaf;
	private NodeRef<L> children;
	private int length;
	
	public Trie() {
		super();
	}

	private Trie(char ch) {
		super();
		this.ch = ch;
	}
	
	public Trie<L> clear() {
		leaf = null;
		children = null;
		length = 0;
		return this;
	}
	
	public Trie<L> insert(String text, L leaf) {
		return insert(text.toCharArray(), leaf);
	}
	
	public Trie<L> insert(char[] text, L leaf) {
		Trie<L> node = this;
		int length = text.length;
		for (char ch : text) {
			node.length = Math.max(node.length, length);
			boolean add = true;
			for (NodeRef<L> child = node.children; null != child; child = child.next) {
				if (child.node.ch == ch) {
					add = false;
					node = child.node;
					length --;
					break;
				}
			}
			if (add) {
				Trie<L> child = new Trie<L>(ch);
				node.children = new NodeRef<L>(child, node.children);
				node = child;
				length --;
			}
		}
		node.leaf = leaf.merge(node.leaf);
		return node;
	}
	
	public Leaf search(String text) {
		Trie<L> node = this;
		for (char ch : text.toCharArray()) {
			if (null == node.children) {
				return null;
			}
			boolean notfound = true;
			for (NodeRef<L> child = node.children; null != child; child = child.next) {
				if (child.node.ch == ch) {
					node = child.node;
					notfound = false;
					break;
				}
			}
			if (notfound) {
				return null;
			}
		}
		return node.leaf;
	}
	
	public List<Match> search(String text, int maxCost) {
		return search(text.toCharArray(), maxCost);
	}

	public List<Match> search(char[] text, int maxCost) {
		final int columns = 1 + text.length;
		final int[] currentRow = new int[columns];
		for (int i = 0; i < columns; i++) {
			currentRow[i] = i;
		}
		List<Match> results = new ArrayList<>();
		for (NodeRef<L> child = children; null != child; child = child.next) {
			child.node.search(text, maxCost, currentRow, "", results);
		}
		return results;
	}	
	
	private void search(char[] text, int maxCost, int[] previousRow, String path, List<Match> results) {
		path += ch;
		final int columns = 1 + text.length;
		final int[] currentRow = new int[columns];
		currentRow[0] = previousRow[0] + 1;
		int minCost = currentRow[0];
		for (int i = 1; i < columns; i++) {
			final int insertCost = currentRow[i-1] + 1;
			final int deleteCost = previousRow[i] + 1;
			final int replaceCost = (ch == text[i-1] ? previousRow[i-1] : previousRow[i-1] + 1);
			currentRow[i] = Math.min(Math.min(insertCost, deleteCost), replaceCost);
			minCost = Math.min(minCost, currentRow[i]);
		}
		if (null != leaf && currentRow[columns-1] <= maxCost) {
			results.add(new Match(currentRow[columns-1], path, leaf));
		}
		if (null == children) {
			return;
		} else if (minCost > maxCost + (int) length) {
			return;
		} else if (minCost <= maxCost) {
			for (int i = 0; i < columns; i ++) {
				for (NodeRef<L> child = children; null != child; child = child.next) {
					child.node.search(text, maxCost, currentRow, path, results);
				}
			}
		}			
	}
	
	public void visit(Observer observer) throws Exception {
		for (NodeRef<L> child = children; null != child; child = child.next) {
			child.node.visit(observer, "");
		}
	}
	
	private void visit(Observer observer, String path) throws Exception {
		path += ch;
		if (null != leaf) {
			observer.observe(leaf, path);
		}
		for (NodeRef<L> child = children; null != child; child = child.next) {
			child.node.visit(observer, path);
		}
	}
	
	public void read(InputStream in, Provider provider) throws IOException {
		ch = (char) (in.read() & 0xff);
		length = (int) (in.read() & 0xff);
		if (0 == in.read()) {
			leaf = null;
		} else {
			leaf = provider.provide().read(in);
		}
		children = null;
		final int count = (int) (in.read() & 0xff);
		for (int i = 0; i < count; i ++) {
			final Trie<L> trie = new Trie<>();
			trie.read(in, provider);
			children = new NodeRef<>(trie, children);
		}
	}
	
	public void write(OutputStream out) throws IOException {
		int count = 0;
		for (NodeRef<L> child = children; null != child; child = child.next) {
			count ++;
		}
		out.write(ch & 0xff);
		out.write(length & 0xff);
		if (null == leaf) {
			out.write(0);
		} else {			
			out.write(1);
			leaf.write(out);
		}
		out.write(count & 0xff);
		for (NodeRef<L> child = children; null != child; child = child.next) {
			child.node.write(out);
		}
	}
	
}
