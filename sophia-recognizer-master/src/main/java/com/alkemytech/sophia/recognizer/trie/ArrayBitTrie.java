package com.alkemytech.sophia.recognizer.trie;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * @see http://stevehanov.ca/blog/index.php?id=114
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class ArrayBitTrie {
	
	private static class Cache {

		private final List<LinkedList<ArrayBitTrie[]>> cache;
		private final int maxSize;
		
		public Cache(int maxSize) {
			super();
			cache = new ArrayList<>();
			this.maxSize = maxSize;
		}
		
		public ArrayBitTrie[] borrowCachedArray(int length) {
			if (cache.size() > length) {
				final LinkedList<ArrayBitTrie[]> cached = cache.get(length);
				if (!cached.isEmpty()) {
					return cached.removeFirst();
				}
			}
			return new ArrayBitTrie[length];
		}

		public void releaseCachedArray(ArrayBitTrie[] array) {
			Arrays.fill(array, null);
			while (cache.size() <= array.length) {
				cache.add(new LinkedList<>());
			}
			final LinkedList<ArrayBitTrie[]> cached = cache.get(array.length);
			if (cached.size() < maxSize) {
				cached.addFirst(array);
			}
		}
		
	}
	
	private static final Cache cache = new Cache(256);
	
	private char ch;
	private boolean leaf;
	private ArrayBitTrie[] children;
	private short length;
	
	public ArrayBitTrie() {
		super();
	}

	private ArrayBitTrie(char ch) {
		super();
		this.ch = ch;
	}
	
	public ArrayBitTrie clear() {
		leaf = false;
		children = null;
		length = 0;
		return this;
	}
	
	public ArrayBitTrie insert(String text) {
		return insert(text.toCharArray());
	}
	
	public ArrayBitTrie insert(char[] text) {
		ArrayBitTrie node = this;
		short length = (short) text.length;
		for (char ch : text) {
			node.length = (short) Math.max(node.length, length);
			boolean add = true;
			if (null != node.children) {
				for (ArrayBitTrie child : node.children) {
					if (child.ch == ch) {
						add = false;
						node = child;
						length --;
						break;
					}
				}
			}
			if (add) {
				final ArrayBitTrie[] children;
				if (null == node.children) {
					children = cache.borrowCachedArray(1);
				} else {
					children = cache.borrowCachedArray(node.children.length + 1);
					System.arraycopy(node.children, 0, children, 1, node.children.length);
					cache.releaseCachedArray(node.children);
				}
				children[0] = new ArrayBitTrie(ch);
				node.children = children;
				node = children[0];
				length --;
			}
		}
		node.leaf = true;
		return node;
	}
	
	public boolean search(String text) {
		ArrayBitTrie node = this;
		for (char ch : text.toCharArray()) {
			if (null == node.children) {
				return false;
			}
			boolean notfound = true;
			if (null != node.children) {
				for (ArrayBitTrie child : node.children) {
					if (child.ch == ch) {
						node = child;
						notfound = false;
						break;
					}
				}
			}
			if (notfound) {
				return false;
			}
		}
		return true;
	}
	
	public List<ScoredPath> search(String text, int maxCost) {
		return search(text.toCharArray(), maxCost);
	}

	public List<ScoredPath> search(char[] text, int maxCost) {
		final int columns = 1 + text.length;
		final int[] currentRow = new int[columns];
		for (int i = 0; i < columns; i++) {
			currentRow[i] = i;
		}
		List<ScoredPath> results = new ArrayList<>();
		if (null != children) {
			for (ArrayBitTrie child : children) {
				child.search(text, maxCost, currentRow, "", results);
			}
		}
		return results;
	}	
	
	private void search(char[] text, int maxCost, int[] previousRow, String path, List<ScoredPath> results) {
		path += ch;
		final int columns = 1 + text.length;
		final int[] currentRow = new int[columns];
		currentRow[0] = previousRow[0] + 1;
		int minCost = currentRow[0];
		for (int i = 1; i < columns; i++) {
			final int insertCost = currentRow[i-1] + 1;
			final int deleteCost = previousRow[i] + 1;
			final int replaceCost = (ch == text[i-1] ? previousRow[i-1] : previousRow[i-1] + 1);
			currentRow[i] = Math.min(Math.min(insertCost, deleteCost), replaceCost);
			minCost = Math.min(minCost, currentRow[i]);
		}
		if (leaf && currentRow[columns-1] <= maxCost) {
			results.add(new ScoredPath(currentRow[columns-1], path));
		}
		if (null == children) {
			return;
		} else if (minCost > maxCost + (int) length) {
			return;
		} else if (minCost <= maxCost) {
			for (ArrayBitTrie child : children) {
				child.search(text, maxCost, currentRow, path, results);
			}
		}
	}
	
	public void visit(PathObserver observer) throws Exception {
		if (null != children) {
			for (ArrayBitTrie child : children) {
				child.visit(observer, "");
			}
		}
	}
	
	private void visit(PathObserver observer, String path) throws Exception {
		path += ch;
		if (leaf) {
			observer.observe(path);
		}
		if (null != children) {
			for (ArrayBitTrie child : children) {
				child.visit(observer, path);
			}
		}
	}
	
	public void read(InputStream in) throws IOException {
		ch = (char) (in.read() & 0xff);
		length = (short) (in.read() & 0xff);
		leaf = (1 == in.read());
		final int count = (int) (in.read() & 0xff);
		if (0 == count) {
			children = null;
		} else {
			children = new ArrayBitTrie[count];
			for (int i = 0; i < count; i ++) {
				final ArrayBitTrie trie = new ArrayBitTrie();
				trie.read(in);
				children[i] = trie;
			}
		}
	}
	
	public void write(OutputStream out) throws IOException {
		out.write(ch & 0xff);
		out.write(length & 0xff);
		out.write(leaf ? 1 : 0);
		final int count = (null == children ? 0 : children.length);
		out.write(count & 0xff);
		if (null != children) {
			for (ArrayBitTrie child : children) {
				child.write(out);
			}
		}
	}
	
}
