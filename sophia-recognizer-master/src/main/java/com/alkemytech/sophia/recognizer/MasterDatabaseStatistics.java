package com.alkemytech.sophia.recognizer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.recognizer.db.MasterDatabase;
import com.alkemytech.sophia.recognizer.model.Work;
import com.google.common.util.concurrent.AtomicDouble;
import com.google.gson.Gson;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class MasterDatabaseStatistics {

    private static final Logger logger = LoggerFactory.getLogger(MasterDatabaseStatistics.class);
    static int exitCode = 0;
    protected static class GuiceModuleExtension extends GuiceModule {

        public GuiceModuleExtension(String[] args) {
            super(args);
        }

        @Override
        protected void configure() {
            super.configure();
            // singleton(s)
            bind(MasterDatabase.class).asEagerSingleton();
            bind(MasterDatabaseStatistics.class).asEagerSingleton();
        }

    }
    
    

    public static void main(String[] args) {
        try {
            final MasterDatabaseStatistics instance = Guice.createInjector(new GuiceModuleExtension(args))
                    .getInstance(MasterDatabaseStatistics.class)
                    .startup();
            try {
                instance.process(args);
            } finally {
                instance.shutdown();
            }
        } catch (Throwable e) {
            logger.error("main", e);
            exitCode = 1 ;
        } finally {
            System.exit(exitCode);
        }
    }

    private final Properties configuration;
    private final MasterDatabase masterDatabase;

    @Inject
    protected MasterDatabaseStatistics(@Named("configuration") Properties configuration,
                                       MasterDatabase masterDatabase, Gson gson) {
        super();
        this.configuration = configuration;
        this.masterDatabase = masterDatabase;
    }

    public MasterDatabaseStatistics startup() {
        masterDatabase.startup();
        return this;
    }

    public MasterDatabaseStatistics shutdown() {
        masterDatabase.shutdown();
        return this;
    }

    public MasterDatabaseStatistics process(String[] args) throws IOException {
        try {
            final File homeFolder = new File(configuration.getProperty("masterDatabaseStatistics.homeFolder"));
            final File outputFile = new File(homeFolder, configuration.getProperty("masterDatabaseStatistics.outputFile"));
            final int heartbeat = Integer.parseInt(configuration.getProperty("default.heartbeat", "1000"));

            // statistics
            final AtomicDouble valueSum = new AtomicDouble(0.0);
            final AtomicDouble maxValue = new AtomicDouble(0.0);
            final AtomicDouble minValue = new AtomicDouble(Double.POSITIVE_INFINITY);
            final AtomicInteger totalWorks = new AtomicInteger(0);

            logger.debug("computing master database statistics");
            final AtomicInteger rownum = new AtomicInteger(0);
            final long startTimeMillis = System.currentTimeMillis();
            masterDatabase.selectAllWorks(new Work.Selector() {
                @Override
                public Work.Selector select(Work work) {
                    // statistics
                    totalWorks.incrementAndGet();
                    if (null != work.value) {
                        valueSum.addAndGet(work.value);
                        if (minValue.doubleValue() > work.value) {
                            minValue.set(work.value);
                        }
                        if (maxValue.doubleValue() < work.value) {
                            maxValue.set(work.value);
                        }
                    }
                    // heartbeat
                    if (0 == (rownum.incrementAndGet() % heartbeat)) {
                        logger.debug("{}k", rownum.get() / 1000);
                    }
                    return this;
                }

            },exitCode);
            logger.debug("{} records analyzed", rownum.get());
            logger.debug("stats calculated in {}s", (System.currentTimeMillis() - startTimeMillis) / 1000L);

            // statistics to properties
            final Properties statistics = new Properties();
            statistics.setProperty("totalWorks", Integer.toString(totalWorks.get()));
            statistics.setProperty("minValue", String.format("%.6f", minValue.get()));
            statistics.setProperty("maxValue", String.format("%.6f", maxValue.get()));
            statistics.setProperty("valueSum", String.format("%.6f", valueSum.get()));
            statistics.setProperty("avgValue", String.format("%.6f", valueSum.get() / totalWorks.get()));

            // save statistics
            try (OutputStream out = new FileOutputStream(outputFile)) {
                statistics.store(out, "master database statistics");
            } catch (Exception e) {
                exitCode = 1 ;
                logger.info("Error :"+e.getMessage());
            }
        } catch (NumberFormatException | NullPointerException e) {
            e.printStackTrace();
            exitCode = 1 ;
        }
        return this;
    }

}