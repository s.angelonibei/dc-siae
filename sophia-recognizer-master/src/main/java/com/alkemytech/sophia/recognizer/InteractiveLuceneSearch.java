package com.alkemytech.sophia.recognizer;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.MMapDirectory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.recognizer.db.MasterDatabase;
import com.alkemytech.sophia.recognizer.lucene.LuceneAnalyzers;
import com.alkemytech.sophia.recognizer.lucene.LuceneField;
import com.alkemytech.sophia.recognizer.model.Work;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class InteractiveLuceneSearch {
	
	private static final Logger logger = LoggerFactory.getLogger(InteractiveLuceneSearch.class);

	protected static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args) {
			super(args);
		}

		@Override
		protected void configure() {
			super.configure();
			// singleton(s)
			bind(MasterDatabase.class).asEagerSingleton();
			bind(InteractiveLuceneSearch.class).asEagerSingleton();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			final InteractiveLuceneSearch instance = Guice.createInjector(new GuiceModuleExtension(args))
				.getInstance(InteractiveLuceneSearch.class)
					.startup();
			try {
				instance.process();
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}
	
	private final Properties configuration;
	private final MasterDatabase masterDatabase;
	private final Gson gson;
	
	@Inject
	protected InteractiveLuceneSearch(@Named("configuration") Properties configuration,
			MasterDatabase masterDatabase) {
		super();
		this.configuration = configuration;
		this.masterDatabase = masterDatabase;
		this.gson = new GsonBuilder()
				.disableHtmlEscaping()
				//.setPrettyPrinting()
				.create();
	}
	
	public InteractiveLuceneSearch startup() {
		masterDatabase.startup();
		return this;
	}

	public InteractiveLuceneSearch shutdown() {
		masterDatabase.shutdown();
		return this;
	}
	
	public InteractiveLuceneSearch process() throws IOException, ParseException {
		
		final File homeFolder = new File(configuration.getProperty("interactiveLuceneSearch.homeFolder"));
		final File indexFolder = new File(homeFolder, configuration.getProperty("interactiveLuceneSearch.indexFolder"));
		final String analyzerName = configuration.getProperty("interactiveLuceneSearch.analyzer", "default");

		try (Directory directory = MMapDirectory.open(indexFolder.toPath());
				Analyzer analyzer = LuceneAnalyzers.get(analyzerName);
				DirectoryReader reader = DirectoryReader.open(directory)) {
			
			final IndexSearcher indexSearcher = new IndexSearcher(reader);
			final QueryParser queryParser = new QueryParser(null, analyzer);

			System.out.println();
			System.out.println("type \"exit\" to quit");
			try (BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in))) {
				for (String line = stdin.readLine(); !"exit".equals(line); line = stdin.readLine()) {
					if (null == line || "".equals(line)) {
						continue;
					}
					System.out.println("input text: \"" + line + "\"");

					final Query query = queryParser.parse(line);
					final TopDocs topDocs = indexSearcher.search(query, 100);
					int rownum = 0;
					for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
						final Document document = indexSearcher.doc(scoreDoc.doc);
						final Work work = masterDatabase.getById(document.getField(LuceneField.ID).numericValue().longValue());
						System.out.println(work.toJson(gson));
						rownum ++;
					}
					
					System.out.println(rownum + " work(s) found");
					System.out.println();
					System.out.println();				
				}
			}
			
		}
		
		return this;
	}

}