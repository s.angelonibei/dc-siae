package com.alkemytech.sophia.recognizer.siada;

import java.io.Closeable;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.QuoteMode;

import com.alkemytech.sophia.recognizer.db.MasterDatabase;
import com.alkemytech.sophia.recognizer.model.Work;
import com.alkemytech.sophia.recognizer.score.ScoreResult;
import com.alkemytech.sophia.recognizer.utilization.Utilization;
import com.google.common.base.Strings;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class OutputForSiadaReader implements OutputForSiada, Closeable {

	private final Reader reader;
	private final MasterDatabase masterDatabase;
	
	private CSVParser parser;
	private Iterator<CSVRecord> iterator;
	private Utilization utilization;
	private List<ScoreResult> scoreResults;
	private int rownum;
	
	public OutputForSiadaReader(Reader reader, MasterDatabase masterDatabase) {
		super();
		this.reader = reader;
		this.masterDatabase = masterDatabase;
	}
	
	@Override
	public void close() throws IOException {
		if (null != parser) {
			parser.close();
		}
		if (null != reader) {
			reader.close();
		}
	}
	
	private Utilization toUtilization(CSVRecord record) {
		final HashSet<String> artists = new HashSet<>();
		for (int i = COMP1_COD_IPI1; i <= COMP3_COD_IPI3; i ++) {
			final String artist = record.get(i);
			if (!Strings.isNullOrEmpty(artist)) {
				artists.add(artist);
			}
		}
		final String code = record.get(CODICE_OPERA);
		return new Utilization(rownum, record.get(CODICE_COMBINAZIONE_ANAGRAFICA), 
				record.get(TITOLO_UTILIZZAZIONE),
				artists,
				Integer.parseInt(record.get(NUMERO_RISULTATI)),
				null != code,
				code);
	}
	
	private Work toWork(CSVRecord record) {
		return masterDatabase.getByCode(record.get(CODICE_OPERA_ASSEGNATO));
	}
	
	private double toScore(CSVRecord record) {
		return Double.parseDouble(record.get(GRADO_DI_CONFIDENZA)) / 100.0;
	}

	public boolean next() throws IOException {
		if (null == iterator) {
			parser = new CSVParser(reader, CSVFormat.DEFAULT
					.withIgnoreEmptyLines()
					.withQuoteMode(QuoteMode.MINIMAL)
					.withQuote('"')
					.withIgnoreSurroundingSpaces()
					.withCommentMarker('#'));
			iterator = parser.iterator();
		}
		if (!iterator.hasNext()) {
			return false;
		}
		CSVRecord record = iterator.next();
		rownum ++;
		utilization = toUtilization(record);
		scoreResults = new ArrayList<>();
		scoreResults.add(new ScoreResult(utilization, toWork(record), toScore(record)));
		for (int i = 1; i < utilization.results && iterator.hasNext(); i ++) {
			record = iterator.next();
			scoreResults.add(new ScoreResult(utilization, toWork(record), toScore(record)));
		}
		return true;
	}

	public List<ScoreResult> getScoreResults() {
		return scoreResults;
	}
	
	public Work getVerifiedWork() {
		return masterDatabase.getByCode(utilization.code);
	}
	
}
