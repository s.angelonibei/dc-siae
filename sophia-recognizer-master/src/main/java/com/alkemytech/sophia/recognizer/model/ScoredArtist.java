package com.alkemytech.sophia.recognizer.model;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class ScoredArtist {
	
	public final Artist artist;
	public final double score;
	
	public ScoredArtist(Artist artist, double score) {
		super();
		this.artist = artist;
		this.score = score;
	}

	@Override
	public int hashCode() {
		return 31 + ((null == artist) ? 0 : artist.hashCode());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (null == obj) {
			return false;
		} else if (getClass() != obj.getClass()) {
			return false;
		}
		final ScoredArtist other = (ScoredArtist) obj;
		if (null == artist) {
			if (null != other.artist) {
				return false;
			}
		} else if (!artist.equals(other.artist)) {
			return false;
		}
		return true;
	}
	
	
}
