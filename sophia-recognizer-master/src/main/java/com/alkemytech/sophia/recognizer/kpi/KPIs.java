package com.alkemytech.sophia.recognizer.kpi;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.csv.CSVPrinter;

import com.alkemytech.sophia.recognizer.model.Work;
import com.alkemytech.sophia.recognizer.score.ScoreResult;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class KPIs {

	// tutte le proposte
	public final AtomicInteger numeroUtilizzazioni = new AtomicInteger(0);
	public final AtomicInteger numeroVerificate = new AtomicInteger(0);
	public final AtomicInteger numeroSkip = new AtomicInteger(0);
	public final AtomicInteger numeroProposteSingole = new AtomicInteger(0);
	public final AtomicInteger numeroProposteMultiple = new AtomicInteger(0);
	public final VariableObserver numeroCandidate = new NonNegativeVariableObserver();
	public final VariableObserver confidenzaPrimaProposta = new NonNegativeVariableObserver();
	public final VariableObserver confidenzaSecondaProposta = new NonNegativeVariableObserver();
	public final VariableObserver confidenzaUltimaProposta = new NonNegativeVariableObserver();
	public final VariableObserver confidenzaPrimaPropostaConCorretta = new NonNegativeVariableObserver();
	public final VariableObserver confidenzaSecondaPropostaConCorretta = new NonNegativeVariableObserver();
	public final VariableObserver confidenzaUltimaPropostaConCorretta = new NonNegativeVariableObserver();

	// solo proposte con corretta
	public final AtomicInteger numeroProposteConCorretta = new AtomicInteger(0);
	public final AtomicInteger numeroProposteConCorrettaAlTop = new AtomicInteger(0);
	public final VariableObserver posizionePropostaCorretta = new NonNegativeVariableObserver();
	public final VariableObserver posizionePropostaCorrettaNoTop = new NonNegativeVariableObserver();
	public final VariableObserver deltaPrimaSecondaConCorrettaAlTop = new NonNegativeVariableObserver();
	public final VariableObserver deltaPrimaSecondaConCorrettaNonAlTop = new NonNegativeVariableObserver();
	public final VariableObserver deltaCorrettaPrima = new VariableObserver();
	public final VariableObserver deltaPrimaCorrettaConCorrettaNonAlTop = new NonNegativeVariableObserver();

	public KPIs update(List<ScoreResult> scoreResults, Work verifiedWork) {
		numeroUtilizzazioni.incrementAndGet();
		if (null == scoreResults || scoreResults.isEmpty()) { // skip
			numeroSkip.incrementAndGet();
			if (null != verifiedWork) {
				numeroVerificate.incrementAndGet();
			}
			numeroCandidate.update(0);
		} else {
			// cerca proposta corretta (-1 not found)
			int posizioneCorretta = -1;
			if (null != verifiedWork) {
				int posizione = 0;
				for (ScoreResult scoreResult : scoreResults) {
					if (scoreResult.work.code.equals(verifiedWork.code)) {
						posizioneCorretta = posizione;
						break;
					}
					posizione ++;
				}
				numeroVerificate.incrementAndGet();
			}
			// solo proposte con corretta
			if (-1 != posizioneCorretta) { // corretta trovata
				numeroProposteConCorretta.incrementAndGet();
				if (0 == posizioneCorretta) { // corretta al top
					numeroProposteConCorrettaAlTop.incrementAndGet();
					if (scoreResults.size() > 1) { // 2+ altre candidate
						deltaPrimaSecondaConCorrettaAlTop.update(scoreResults.get(0).score - scoreResults.get(1).score);
					}
				} else {
					deltaPrimaSecondaConCorrettaNonAlTop
						.update(scoreResults.get(0).score - scoreResults.get(1).score);							
					deltaPrimaCorrettaConCorrettaNonAlTop
						.update(scoreResults.get(0).score - scoreResults.get(posizioneCorretta).score);
					posizionePropostaCorrettaNoTop.update(posizioneCorretta);
				}
				deltaCorrettaPrima.update(scoreResults.get(posizioneCorretta).score - scoreResults.get(0).score);
				confidenzaPrimaPropostaConCorretta.update(scoreResults.get(0).score);
				if (scoreResults.size() > 1) {
					confidenzaSecondaPropostaConCorretta.update(scoreResults.get(1).score);						
					if (scoreResults.size() > 2) {
						confidenzaUltimaPropostaConCorretta.update(scoreResults.get(scoreResults.size() - 1).score);
					}
				}
				posizionePropostaCorretta.update(posizioneCorretta);
			}
			// tutte le propose
			confidenzaPrimaProposta.update(scoreResults.get(0).score);
			if (scoreResults.size() > 1) {
				numeroProposteMultiple.incrementAndGet();
				confidenzaSecondaProposta.update(scoreResults.get(1).score);						
				if (scoreResults.size() > 2) {
					confidenzaUltimaProposta.update(scoreResults.get(scoreResults.size() - 1).score);
				}
			} else {
				numeroProposteSingole.incrementAndGet();
			}
			numeroCandidate.update(scoreResults.size());
		}
		return this;
	}

	private double average(int value, int total) {
		if (0 == total) {
			return 0.0;
		}
		return (double) value / (double) total;
	}
	
	public static void printHeader(CSVPrinter printer) throws IOException {
		printer.print("numero totale utilizzazioni".toUpperCase());
		printer.print("numero totale utilizzazioni verificate".toUpperCase());
		printer.print("numero totale utilizzazioni verificate %".toUpperCase());
		printer.print("numero totale skip".toUpperCase());
		printer.print("numero totale skip %".toUpperCase());
		printer.print("numero totale proposte singole".toUpperCase());
		printer.print("numero totale proposte singole %".toUpperCase());
		printer.print("numero totale proposte multiple".toUpperCase());
		printer.print("numero totale proposte multiple %".toUpperCase());
		printer.print("numero candidate media".toUpperCase());
		
		printer.print("numero candidate deviazione".toUpperCase());
		printer.print("numero candidate minimo".toUpperCase());
		printer.print("numero candidate massimo".toUpperCase());
		printer.print("utilizzazioni con candidata corretta".toUpperCase()); // utilizzazioni con candidata corretta
		printer.print("utilizzazioni con candidata corretta %".toUpperCase()); // utilizzazioni con candidata corretta %
		printer.print("utilizzazioni con candidata corretta top".toUpperCase()); // utilizzazioni con candidata corretta top
		printer.print("utilizzazioni con candidata corretta top % delle corrette".toUpperCase());  // utilizzazioni con candidata corretta top % delle corrette
		printer.print("utilizzazioni con candidata corretta top % del totale".toUpperCase()); // utilizzazioni con candidata corretta top % del totale
		printer.print("scarto prima-seconda se corretta top media".toUpperCase()); // media scarto prima-seconda se corretta top
		printer.print("scarto prima-seconda se corretta top deviazione".toUpperCase()); // deviazione scarto prima-seconda se corretta top 
		
		printer.print("scarto prima-seconda se corretta top minimo".toUpperCase()); // minimo scarto prima-seconda se corretta top
		printer.print("scarto prima-seconda se corretta top massimo".toUpperCase()); // massimo scarto prima-seconda se corretta top
		printer.print("scarto prima-seconda se corretta no top media".toUpperCase()); // media scarto prima-seconda se corretta no top
		printer.print("scarto prima-seconda se corretta no top deviazione".toUpperCase()); // deviazione scarto prima-seconda se corretta no top
		printer.print("scarto prima-seconda se corretta no top minimo".toUpperCase()); // minimo scarto prima-seconda se corretta no top
		printer.print("scarto prima-seconda se corretta no top massimo".toUpperCase()); // massimo scarto prima-seconda se corretta no top
		printer.print("scarto prima-corretta se corretta no top media".toUpperCase()); // media scarto prima-corretta se corretta no top
		printer.print("scarto prima-corretta se corretta no top deviazione".toUpperCase()); // deviazione scarto prima-corretta se corretta no top
		printer.print("scarto prima-corretta se corretta no top minimo".toUpperCase()); // minimo scarto prima-corretta se corretta no top
		printer.print("scarto prima-corretta se corretta no top massimo".toUpperCase()); // massimo scarto prima-corretta se corretta no top
		
		printer.print("posizione corretta media".toUpperCase()); // media posizione corretta
		printer.print("posizione corretta deviazione".toUpperCase()); // deviazione posizione corretta
		printer.print("posizione corretta minimo".toUpperCase()); // minimo posizione corretta
		printer.print("posizione corretta massimo".toUpperCase()); // massimo posizione corretta
		printer.print("posizione corretta no top media".toUpperCase()); // media posizione corretta no top
		printer.print("posizione corretta no top deviazione".toUpperCase()); // deviazione posizione corretta no top
		printer.print("posizione corretta no top minimo".toUpperCase()); // minimo posizione corretta no top
		printer.print("posizione corretta no top massimo".toUpperCase()); // massimo posizione corretta no top
		printer.print("confidenza prima proposta media".toUpperCase()); // media confidenza prima proposta
		printer.print("confidenza prima proposta deviazione".toUpperCase()); // deviazione confidenza prima proposta
		
		printer.print("confidenza prima proposta minimo".toUpperCase()); // minimo confidenza prima proposta
		printer.print("confidenza prima proposta massimo".toUpperCase()); // massimo confidenza prima proposta
		printer.print("confidenza seconda proposta media".toUpperCase()); // media confidenza seconda proposta
		printer.print("confidenza seconda proposta deviazione".toUpperCase()); // deviazione confidenza seconda proposta
		printer.print("confidenza seconda proposta minimo".toUpperCase()); // minimo confidenza seconda proposta
		printer.print("confidenza seconda proposta massimo".toUpperCase()); // massimo confidenza seconda proposta
		printer.print("confidenza ultima proposta media".toUpperCase()); // media confidenza ultima proposta
		printer.print("confidenza ultima proposta deviazione".toUpperCase()); // deviazione confidenza ultima proposta
		printer.print("confidenza ultima proposta minimo".toUpperCase()); // minimo confidenza ultima proposta
		printer.print("confidenza ultima proposta massimo".toUpperCase()); // massimo confidenza ultima proposta
		
		printer.print("confidenza prima proposta con corretta media".toUpperCase()); // media confidenza prima proposta con corretta
		printer.print("confidenza prima proposta con corretta deviazione".toUpperCase()); // deviazione confidenza prima proposta con corretta
		printer.print("confidenza prima proposta con corretta minimo".toUpperCase()); // minimo confidenza prima proposta con corretta
		printer.print("confidenza prima proposta con corretta massimo".toUpperCase()); // massimo confidenza prima proposta con corretta
		printer.print("confidenza seconda proposta con corretta media".toUpperCase()); // media confidenza seconda proposta con corretta
		printer.print("confidenza seconda proposta con corretta deviazione".toUpperCase()); // deviazione confidenza seconda proposta con corretta
		printer.print("confidenza seconda proposta con corretta minimo".toUpperCase()); // minimo confidenza seconda proposta con corretta
		printer.print("confidenza seconda proposta con corretta massimo".toUpperCase()); // massimo confidenza seconda proposta con corretta
		printer.print("confidenza ultima proposta con corretta media".toUpperCase()); // media confidenza ultima proposta con corretta
		printer.print("confidenza ultima proposta con corretta deviazione".toUpperCase()); // deviazione confidenza ultima proposta con corretta
		
		printer.print("confidenza ultima proposta con corretta minimo".toUpperCase()); // minimo confidenza ultima proposta con corretta
		printer.print("confidenza ultima proposta con corretta massimo".toUpperCase()); // massimo confidenza ultima proposta con corretta
	}
	
	public KPIs print(CSVPrinter printer) throws IOException {
		printer.print(numeroUtilizzazioni.get()); // numero totale utilizzazioni
		printer.print(numeroVerificate.get()); // numero totale utilizzazioni verificate
		printer.print(100.0 * average(numeroVerificate.get(), numeroUtilizzazioni.get())); // numero totale utilizzazioni verificate %
		printer.print(numeroSkip.get()); // numero totale skip
		printer.print(100.0 * average(numeroSkip.get(), numeroUtilizzazioni.get())); // numero totale skip %
		printer.print(numeroProposteSingole.get()); // numero totale proposte singole
		printer.print(100.0 * average(numeroProposteSingole.get(), numeroUtilizzazioni.get())); // numero totale proposte singole %
		printer.print(numeroProposteMultiple.get()); // numero totale proposte multiple
		printer.print(100.0 * average(numeroProposteMultiple.get(), numeroUtilizzazioni.get())); // numero totale proposte multiple %
		printer.print(numeroCandidate.average()); // media numero candidate

		printer.print(numeroCandidate.deviation()); // deviazione numero candidate
		printer.print(numeroCandidate.min()); // minimo numero candidate
		printer.print(numeroCandidate.max()); // massimo numero candidate
		printer.print(numeroProposteConCorretta.get()); // utilizzazioni con candidata corretta
		printer.print(100.0 * average(numeroProposteConCorretta.get(), numeroVerificate.get())); // utilizzazioni con candidata corretta %
		printer.print(numeroProposteConCorrettaAlTop.get()); // utilizzazioni con candidata corretta top
		printer.print(100.0 * average(numeroProposteConCorrettaAlTop.get(), numeroProposteConCorretta.get()));  // utilizzazioni con candidata corretta top % sulle corrette
		printer.print(100.0 * average(numeroProposteConCorrettaAlTop.get(), numeroVerificate.get())); // utilizzazioni con candidata corretta top % sul totale
		printer.print(100.0 * deltaPrimaSecondaConCorrettaAlTop.average()); // media scarto prima-seconda se corretta top
		printer.print(100.0 * deltaPrimaSecondaConCorrettaAlTop.deviation()); // deviazione scarto prima-seconda se corretta top 
		
		printer.print(100.0 * deltaPrimaSecondaConCorrettaAlTop.min()); // minimo scarto prima-seconda se corretta top
		printer.print(100.0 * deltaPrimaSecondaConCorrettaAlTop.max()); // massimo scarto prima-seconda se corretta top
		printer.print(100.0 * deltaPrimaSecondaConCorrettaNonAlTop.average()); // media scarto prima-seconda se corretta no top
		printer.print(100.0 * deltaPrimaSecondaConCorrettaNonAlTop.deviation()); // deviazione scarto prima-seconda se corretta no top
		printer.print(100.0 * deltaPrimaSecondaConCorrettaNonAlTop.min()); // minimo scarto prima-seconda se corretta no top
		printer.print(100.0 * deltaPrimaSecondaConCorrettaNonAlTop.max()); // massimo scarto prima-seconda se corretta no top
		printer.print(100.0 * deltaPrimaCorrettaConCorrettaNonAlTop.average()); // media scarto prima-corretta se corretta no top
		printer.print(100.0 * deltaPrimaCorrettaConCorrettaNonAlTop.deviation()); // deviazione scarto prima-corretta se corretta no top
		printer.print(100.0 * deltaPrimaCorrettaConCorrettaNonAlTop.min()); // minimo scarto prima-corretta se corretta no top
		printer.print(100.0 * deltaPrimaCorrettaConCorrettaNonAlTop.max()); // massimo scarto prima-corretta se corretta no top

		printer.print(posizionePropostaCorretta.average()); // media posizione corretta
		printer.print(posizionePropostaCorretta.deviation()); // deviazione posizione corretta
		printer.print(posizionePropostaCorretta.min()); // minimo posizione corretta
		printer.print(posizionePropostaCorretta.max()); // massimo posizione corretta
		printer.print(posizionePropostaCorrettaNoTop.average()); // media posizione corretta no top
		printer.print(posizionePropostaCorrettaNoTop.deviation()); // deviazione posizione corretta no top
		printer.print(posizionePropostaCorrettaNoTop.min()); // minimo posizione corretta no top
		printer.print(posizionePropostaCorrettaNoTop.max()); // massimo posizione corretta no top
		printer.print(100.0 * confidenzaPrimaProposta.average()); // media confidenza prima proposta
		printer.print(100.0 * confidenzaPrimaProposta.deviation()); // deviazione confidenza prima proposta
		
		printer.print(100.0 * confidenzaPrimaProposta.min()); // minimo confidenza prima proposta
		printer.print(100.0 * confidenzaPrimaProposta.max()); // massimo confidenza prima proposta
		printer.print(100.0 * confidenzaSecondaProposta.average()); // media confidenza seconda proposta
		printer.print(100.0 * confidenzaSecondaProposta.deviation()); // deviazione confidenza seconda proposta
		printer.print(100.0 * confidenzaSecondaProposta.min()); // minimo confidenza seconda proposta
		printer.print(100.0 * confidenzaSecondaProposta.max()); // massimo confidenza seconda proposta
		printer.print(100.0 * confidenzaUltimaProposta.average()); // media confidenza ultima proposta
		printer.print(100.0 * confidenzaUltimaProposta.deviation()); // deviazione confidenza ultima proposta
		printer.print(100.0 * confidenzaUltimaProposta.min()); // minimo confidenza ultima proposta
		printer.print(100.0 * confidenzaUltimaProposta.max()); // massimo confidenza ultima proposta
		
		printer.print(100.0 * confidenzaPrimaPropostaConCorretta.average()); // media confidenza prima proposta con corretta
		printer.print(100.0 * confidenzaPrimaPropostaConCorretta.deviation()); // deviazione confidenza prima proposta con corretta
		printer.print(100.0 * confidenzaPrimaPropostaConCorretta.min()); // minimo confidenza prima proposta con corretta
		printer.print(100.0 * confidenzaPrimaPropostaConCorretta.max()); // massimo confidenza prima proposta con corretta
		printer.print(100.0 * confidenzaSecondaPropostaConCorretta.average()); // media confidenza seconda proposta con corretta
		printer.print(100.0 * confidenzaSecondaPropostaConCorretta.deviation()); // deviazione confidenza seconda proposta con corretta
		printer.print(100.0 * confidenzaSecondaPropostaConCorretta.min()); // minimo confidenza seconda proposta con corretta
		printer.print(100.0 * confidenzaSecondaPropostaConCorretta.max()); // massimo confidenza seconda proposta con corretta
		printer.print(100.0 * confidenzaUltimaPropostaConCorretta.average()); // media confidenza ultima proposta con corretta
		printer.print(100.0 * confidenzaUltimaPropostaConCorretta.deviation()); // deviazione confidenza ultima proposta con corretta
		
		printer.print(100.0 * confidenzaUltimaPropostaConCorretta.min()); // minimo confidenza ultima proposta con corretta
		printer.print(100.0 * confidenzaUltimaPropostaConCorretta.max()); // massimo confidenza ultima proposta con corretta
		return this;
	}

		
	@Override
	public String toString() {
		return new StringBuilder()			
			.append(String.format("numero totale utilizzazioni: %d", numeroUtilizzazioni.get()))
			.append('\n')
			.append(String.format("numero totale utilizzazioni verificate: %d  (%.2f%%)",
					numeroVerificate.get(), 100.0 * average(numeroVerificate.get(), numeroUtilizzazioni.get())))
			.append('\n')
			.append(String.format("numero totale skip: %d  (%.2f%%)",
					numeroSkip.get(), 100.0 * average(numeroSkip.get(), numeroUtilizzazioni.get())))
			.append('\n')
			.append(String.format("numero totale proposte singole: %d  (%.2f%%)",
					numeroProposteSingole.get(), 100.0 * average(numeroProposteSingole.get(), numeroUtilizzazioni.get())))
			.append('\n')
			.append(String.format("numero totale proposte multiple: %d  (%.2f%%)",
					numeroProposteMultiple.get(), 100.0 * average(numeroProposteMultiple.get(), numeroUtilizzazioni.get())))
			.append('\n')
			.append(String.format("numero medio candidate: %.3f  (dev %.3f, min %.0f, max %.0f)",
					numeroCandidate.average(), numeroCandidate.deviation(),
					numeroCandidate.min(), numeroCandidate.max()))
			.append('\n')

			.append(String.format("utilizzazioni con candidata corretta: %d  (%.2f%% del totale)",
					numeroProposteConCorretta.get(), 100.0 * average(numeroProposteConCorretta.get(), numeroVerificate.get())))
			.append('\n')
			.append(String.format("utilizzazioni con candidata corretta top: %d  (%.2f%% delle corrette, %.2f%% del totale)",
					numeroProposteConCorrettaAlTop.get(), 100.0 * average(numeroProposteConCorrettaAlTop.get(), numeroProposteConCorretta.get()),
					100.0 * average(numeroProposteConCorrettaAlTop.get(), numeroVerificate.get())))
			.append('\n')
			
			.append(String.format("scarto medio prima-seconda se corretta top: %.2f%%  (dev %.2f%%, min %.2f%%, max %.2f%%)",
					100.0 * deltaPrimaSecondaConCorrettaAlTop.average(), 100.0 * deltaPrimaSecondaConCorrettaAlTop.deviation(),
					100.0 * deltaPrimaSecondaConCorrettaAlTop.min(), 100.0 * deltaPrimaSecondaConCorrettaAlTop.max()))
			.append('\n')
			.append(String.format("scarto medio prima-seconda se corretta no top: %.2f%%  (dev %.2f%%, min %.2f%%, max %.2f%%)",
					100.0 * deltaPrimaSecondaConCorrettaNonAlTop.average(), 100.0 * deltaPrimaSecondaConCorrettaNonAlTop.deviation(),
					100.0 * deltaPrimaSecondaConCorrettaNonAlTop.min(), 100.0 * deltaPrimaSecondaConCorrettaNonAlTop.max()))
			.append('\n')
			.append(String.format("scarto medio prima-corretta se corretta no top: %.2f%%  (dev %.2f%%, min %.2f%%, max %.2f%%)",
					100.0 * deltaPrimaCorrettaConCorrettaNonAlTop.average(), 100.0 * deltaPrimaCorrettaConCorrettaNonAlTop.deviation(),
					100.0 * deltaPrimaCorrettaConCorrettaNonAlTop.min(), 100.0 * deltaPrimaCorrettaConCorrettaNonAlTop.max()))
			.append('\n')
			.append(String.format("scarto medio corretta-prima: %.2f%%  (dev %.2f%%, min %.2f%%, max %.2f%%)",
					100.0 * deltaCorrettaPrima.average(), 100.0 * deltaCorrettaPrima.deviation(),
					100.0 * deltaCorrettaPrima.min(), 100.0 * deltaCorrettaPrima.max()))
			.append('\n')
			
			.append(String.format("distanza media corretta dal primo posto: %.3f  (dev %.3f, min %.0f, max %.0f)",
					posizionePropostaCorretta.average(), posizionePropostaCorretta.deviation(),
					posizionePropostaCorretta.min(), posizionePropostaCorretta.max()))
			.append('\n')
			.append(String.format("distanza media corretta dal primo posto no top: %.3f  (dev %.3f, min %.0f, max %.0f)",
					posizionePropostaCorrettaNoTop.average(), posizionePropostaCorrettaNoTop.deviation(),
					posizionePropostaCorrettaNoTop.min(), posizionePropostaCorrettaNoTop.max()))
			.append('\n')
			
			.append(String.format("confidenza media prima proposta: %.2f%%  (dev %.2f%%, min %.2f%%, max %.2f%%)",
					100.0 * confidenzaPrimaProposta.average(), 100.0 * confidenzaPrimaProposta.deviation(),
					100.0 * confidenzaPrimaProposta.min(), 100.0 * confidenzaPrimaProposta.max()))
			.append('\n')
			.append(String.format("confidenza media seconda proposta: %.2f%%  (dev %.2f%%, min %.2f%%, max %.2f%%)",
					100.0 * confidenzaSecondaProposta.average(), 100.0 * confidenzaSecondaProposta.deviation(),
					100.0 * confidenzaSecondaProposta.min(), 100.0 * confidenzaSecondaProposta.max()))
			.append('\n')
			.append(String.format("confidenza media ultima proposta: %.2f%%  (dev %.2f%%, min %.2f%%, max %.2f%%)",
					100.0 * confidenzaUltimaProposta.average(), 100.0 * confidenzaUltimaProposta.deviation(),
					100.0 * confidenzaUltimaProposta.min(), 100.0 * confidenzaUltimaProposta.max()))
			.append('\n')
			
			.append(String.format("confidenza media prima proposta con corretta: %.2f%%  (dev %.2f%%, min %.2f%%, max %.2f%%)",
					100.0 * confidenzaPrimaPropostaConCorretta.average(), 100.0 * confidenzaPrimaPropostaConCorretta.deviation(),
					100.0 * confidenzaPrimaPropostaConCorretta.min(), 100.0 * confidenzaPrimaPropostaConCorretta.max()))
			.append('\n')
			.append(String.format("confidenza media seconda proposta con corretta: %.2f%%  (dev %.2f%%, min %.2f%%, max %.2f%%)",
					100.0 * confidenzaSecondaPropostaConCorretta.average(), 100.0 * confidenzaSecondaPropostaConCorretta.deviation(),
					100.0 * confidenzaSecondaPropostaConCorretta.min(), 100.0 * confidenzaSecondaPropostaConCorretta.max()))
			.append('\n')
			.append(String.format("confidenza media ultima proposta con corretta: %.2f%%  (dev %.2f%%, min %.2f%%, max %.2f%%)",
					100.0 * confidenzaUltimaPropostaConCorretta.average(), 100.0 * confidenzaUltimaPropostaConCorretta.deviation(),
					100.0 * confidenzaUltimaPropostaConCorretta.min(), 100.0 * confidenzaUltimaPropostaConCorretta.max()))
			.append('\n')
			
			.toString();
	}
	
}
