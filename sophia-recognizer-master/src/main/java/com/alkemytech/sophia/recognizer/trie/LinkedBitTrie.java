package com.alkemytech.sophia.recognizer.trie;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @see http://stevehanov.ca/blog/index.php?id=114
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class LinkedBitTrie {
	
	private static class NodeRef {
		
		public final LinkedBitTrie node;
		public final NodeRef next;
		
		public NodeRef(LinkedBitTrie node, NodeRef prev) {
			this.node = node;
			this.next = prev;
		}
		
	}
		
	private char ch;
	private boolean leaf;
	private NodeRef children;
	private short length;
	
	public LinkedBitTrie() {
		super();
	}

	private LinkedBitTrie(char ch) {
		super();
		this.ch = ch;
	}
	
	public LinkedBitTrie clear() {
		leaf = false;
		children = null;
		length = 0;
		return this;
	}
	
	public LinkedBitTrie insert(String text) {
		return insert(text.toCharArray());
	}
	
	public LinkedBitTrie insert(char[] text) {
		LinkedBitTrie node = this;
		short length = (short) text.length;
		for (char ch : text) {
			node.length = (short) Math.max(node.length, length);
			boolean add = true;
			for (NodeRef child = node.children; null != child; child = child.next) {
				if (child.node.ch == ch) {
					add = false;
					node = child.node;
					length --;
					break;
				}
			}
			if (add) {
				LinkedBitTrie child = new LinkedBitTrie(ch);
				node.children = new NodeRef(child, node.children);
				node = child;
				length --;
			}
		}
		node.leaf = true;
		return node;
	}
	
	public boolean search(String text) {
		LinkedBitTrie node = this;
		for (char ch : text.toCharArray()) {
			if (null == node.children) {
				return false;
			}
			boolean notfound = true;
			for (NodeRef child = node.children; null != child; child = child.next) {
				if (child.node.ch == ch) {
					node = child.node;
					notfound = false;
					break;
				}
			}
			if (notfound) {
				return false;
			}
		}
		return true;
	}
	
	public List<ScoredPath> search(String text, int maxCost) {
		return search(text.toCharArray(), maxCost);
	}

	public List<ScoredPath> search(char[] text, int maxCost) {
		final int columns = 1 + text.length;
		final int[] currentRow = new int[columns];
		for (int i = 0; i < columns; i++) {
			currentRow[i] = i;
		}
		List<ScoredPath> results = new ArrayList<>();
		for (NodeRef child = children; null != child; child = child.next) {
			child.node.search(text, maxCost, currentRow, "", results);
		}
		return results;
	}	
	
	private void search(char[] text, int maxCost, int[] previousRow, String path, List<ScoredPath> results) {
		path += ch;
		final int columns = 1 + text.length;
		final int[] currentRow = new int[columns];
		currentRow[0] = previousRow[0] + 1;
		int minCost = currentRow[0];
		for (int i = 1; i < columns; i++) {
			final int insertCost = currentRow[i-1] + 1;
			final int deleteCost = previousRow[i] + 1;
			final int replaceCost = (ch == text[i-1] ? previousRow[i-1] : previousRow[i-1] + 1);
			currentRow[i] = Math.min(Math.min(insertCost, deleteCost), replaceCost);
			minCost = Math.min(minCost, currentRow[i]);
		}
		if (leaf && currentRow[columns-1] <= maxCost) {
			results.add(new ScoredPath(currentRow[columns-1], path));
		}
		if (null == children) {
			return;
		} else if (minCost > maxCost + (int) length) {
			return;
		} else if (minCost <= maxCost) {
			for (int i = 0; i < columns; i ++) {
				for (NodeRef child = children; null != child; child = child.next) {
					child.node.search(text, maxCost, currentRow, path, results);
				}
			}
		}			
	}
	
	public void visit(PathObserver observer) throws Exception {
		for (NodeRef child = children; null != child; child = child.next) {
			child.node.visit(observer, "");
		}
	}
	
	private void visit(PathObserver observer, String path) throws Exception {
		path += ch;
		if (leaf) {
			observer.observe(path);
		}
		for (NodeRef child = children; null != child; child = child.next) {
			child.node.visit(observer, path);
		}
	}
	
	public void read(InputStream in) throws IOException {
		ch = (char) (in.read() & 0xff);
		length = (short) (in.read() & 0xff);
		leaf = (1 == in.read());
		children = null;
		final int count = (int) (in.read() & 0xff);
		for (int i = 0; i < count; i ++) {
			final LinkedBitTrie trie = new LinkedBitTrie();
			trie.read(in);
			children = new NodeRef(trie, children);
		}
	}
	
	public void write(OutputStream out) throws IOException {
		int count = 0;
		for (NodeRef child = children; null != child; child = child.next) {
			count ++;
		}
		out.write(ch & 0xff);
		out.write(length & 0xff);
		out.write(leaf ? 1 : 0);
		out.write(count & 0xff);
		for (NodeRef child = children; null != child; child = child.next) {
			child.node.write(out);
		}
	}
	
}
