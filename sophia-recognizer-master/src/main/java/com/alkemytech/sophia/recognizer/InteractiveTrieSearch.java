package com.alkemytech.sophia.recognizer;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Properties;

import org.apache.lucene.queryparser.classic.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.recognizer.trie.MMapBitTrie;
import com.alkemytech.sophia.recognizer.trie.ScoredPath;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class InteractiveTrieSearch {
	
	private static final Logger logger = LoggerFactory.getLogger(InteractiveTrieSearch.class);

	protected static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args) {
			super(args);
		}

		@Override
		protected void configure() {
			super.configure();
			// singleton(s)
			bind(InteractiveTrieSearch.class).asEagerSingleton();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			final InteractiveTrieSearch instance = Guice.createInjector(new GuiceModuleExtension(args))
				.getInstance(InteractiveTrieSearch.class)
					.startup();
			try {
				instance.process(args);
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}
	
	private final Properties configuration;
	
	@Inject
	protected InteractiveTrieSearch(@Named("configuration") Properties configuration) {
		super();
		this.configuration = configuration;
	}
	
	public InteractiveTrieSearch startup() throws IOException {
		return this;
	}

	public InteractiveTrieSearch shutdown() throws IOException {
		return this;
	}
	
	public InteractiveTrieSearch process(String[] args) throws IOException, ParseException {
		
		final File homeFolder = new File(configuration.getProperty("interactiveTrieSearch.homeFolder"));
		final File indexFolder = new File(homeFolder, configuration.getProperty("interactiveTrieSearch.indexFolder"));

		final MMapBitTrie titleTrie = new MMapBitTrie(new File(indexFolder, "title.trie"), true);
		final MMapBitTrie artistTrie = new MMapBitTrie(new File(indexFolder, "artist.trie"), true);

		System.out.println();
		System.out.println("type \"exit\" to quit");
		try (BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in))) {
			for (String line = stdin.readLine(); !"exit".equals(line); line = stdin.readLine()) {
				if (null == line || "".equals(line)) {
					continue;
				}
				System.out.println("input text: \"" + line + "\"");
				final int maxCost = (int) Math.ceil(0.25 * line.length()); 
				System.out.println("max cost: " + maxCost);

				// title(s)
				final List<ScoredPath> titleMatches = titleTrie.search(line, maxCost);
				titleMatches.sort((ScoredPath a, ScoredPath b)->Integer.compare(a.cost, b.cost));
				System.out.println(titleMatches.size() + " trie title(s)");
				for (ScoredPath match : titleMatches) {
					System.out.println("  " + match.cost + " " + match.path);
				}
				System.out.println();

				// artist(s)
				final List<ScoredPath> artistMatches = artistTrie.search(line, maxCost);
				artistMatches.sort((ScoredPath a, ScoredPath b)->Integer.compare(a.cost, b.cost));
				System.out.println(artistMatches.size() + " trie artist(s)");
				for (ScoredPath match : artistMatches) {
					System.out.println("  " + match.cost + " " + match.path);
				}
				System.out.println();
				
				System.out.println();				
			}
		}
		
		return this;
	}

}