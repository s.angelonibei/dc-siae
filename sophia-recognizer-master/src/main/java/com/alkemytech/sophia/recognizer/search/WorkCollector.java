package com.alkemytech.sophia.recognizer.search;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public interface WorkCollector {
	
	public WorkCollector collect(long workId);	
	public boolean isCollecting();

}
