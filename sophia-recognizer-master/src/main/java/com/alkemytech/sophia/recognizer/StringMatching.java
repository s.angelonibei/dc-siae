package com.alkemytech.sophia.recognizer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.lucene.queryparser.classic.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.recognizer.db.MasterDatabase;
import com.alkemytech.sophia.recognizer.kpi.KPIs;
import com.alkemytech.sophia.recognizer.kpi.VariableObserver;
import com.alkemytech.sophia.recognizer.lucene.LuceneSearch;
import com.alkemytech.sophia.recognizer.metric.Metric;
import com.alkemytech.sophia.recognizer.metric.Metrics;
import com.alkemytech.sophia.recognizer.model.ScoredWork;
import com.alkemytech.sophia.recognizer.model.Work;
import com.alkemytech.sophia.recognizer.norm.Norms;
import com.alkemytech.sophia.recognizer.regex.StringNormalizer;
import com.alkemytech.sophia.recognizer.score.ScoreCalculator;
import com.alkemytech.sophia.recognizer.score.ScoreResult;
import com.alkemytech.sophia.recognizer.search.WorkCollector;
import com.alkemytech.sophia.recognizer.siada.OutputForSiadaWriter;
import com.alkemytech.sophia.recognizer.trie.TrieSearch;
import com.alkemytech.sophia.recognizer.utilization.Utilization;
import com.alkemytech.sophia.recognizer.utilization.UtilizationWriter;
import com.alkemytech.sophia.recognizer.utilization.UtilizationsReader;
import com.google.common.base.Strings;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class StringMatching {
	
	private static final Logger logger = LoggerFactory.getLogger(StringMatching.class);
	static int exitCode = 0;
	protected static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args) {
			super(args);
		}

		@Override
		protected void configure() {
			super.configure();
			// singleton(s)
			bind(MasterDatabase.class).asEagerSingleton();
			bind(LuceneSearch.class).asEagerSingleton();
			bind(TrieSearch.class).asEagerSingleton();
			bind(UtilizationsReader.class).asEagerSingleton();
			bind(ScoreCalculator.class).asEagerSingleton();
			bind(NumberSpeller.class).asEagerSingleton();
			bind(Metrics.class).asEagerSingleton();
			bind(Norms.class).asEagerSingleton();
			bind(StringMatching.class).asEagerSingleton();
		}
		
	}

	public static void main(String[] args) {
		try {
			final StringMatching instance = Guice.createInjector(new GuiceModuleExtension(args))
				.getInstance(StringMatching.class)
					.startup();
			try {
				instance.process(null, null, null);
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
			exitCode = 1;
		} finally {
			System.exit(exitCode);
		}
	}
	
	/**
	 * Work collector for Lucene and Trie searches.
	 * Collects top scoring results up to a configured maximum number.
	 */
	private class TopScoringWorkCollector implements WorkCollector, Iterable<ScoredWork> {

		private final Comparator<ScoredWork> comparator = new Comparator<ScoredWork>() {
			@Override
			public int compare(ScoredWork a, ScoredWork b) {
				return Double.compare(b.score, a.score); // WARNING: descending order
			}
		};
					
		private ArrayList<ScoredWork> collected;
		private Utilization utilization;
		private boolean collecting;
		private long startTimeMillis;
		private long elapsedMillis;
		private int analyzed;
		private ScoredWork worstScorer;

		@Override
		public Iterator<ScoredWork> iterator() {
			return collected.iterator();
		}

		@Override
		public boolean isCollecting() {
			return collecting;
		}
		
		@Override
		public TopScoringWorkCollector collect(long workId) {
			if (!collecting) {
				return this;
			}

			// stop collecting after maxSearchDurationMillis expires
			elapsedMillis = System.currentTimeMillis() - startTimeMillis;
			collecting = !interrupted && elapsedMillis < maxSearchDurationMillis;

			// retrieve work
			final Work work = masterDatabase.getById(workId);				
			//logger.debug("collecting \"{}\" \"{}\"", work.title.title, work.artists.iterator().next().name);
			
			// best title similarity
			final double titleSimilarity = work
					.getTitleSimilarity(utilization, titleMetric).score;

			if (collected.size() < maxCollectedResults ||
					titleSimilarity / similarityTitleWeight > worstScorer.score) {

				// best artist(s) similarity
				final double artistSimilarity = work
						.getArtistSimilarity(utilization, artistMetric).score;

				// overall score
				final double score = similarityTitleWeight * titleSimilarity +
						similarityArtistWeight * artistSimilarity;

				if (score >= minCollectedScore &&
						(collected.size() < maxCollectedResults || score > worstScorer.score)) {

					// scored work
					final ScoredWork scoredWork = new ScoredWork(work, score);
					
					// insert into reverse-sorted list
					int position = Collections.binarySearch(collected, scoredWork, comparator);
					if (position < 0) {
						position = -position-1;
					} else {
						position ++; // WARNING: prefer first collected
					}
					if (position < maxCollectedResults) {
						if (!collected.contains(scoredWork)) {
					    	collected.add(position, scoredWork);
					    	while (collected.size() > maxCollectedResults) {
					    		collected.remove(collected.size() - 1);
					    	}
							worstScorer = collected.get(collected.size() - 1);						
						}
				    }

				}
			}
			
			analyzed ++;
			return this;
		}

		public TopScoringWorkCollector startCollecting(Utilization utilization) {
			this.collected = new ArrayList<>();
			this.utilization = utilization;
			this.collecting = true;
			this.startTimeMillis = System.currentTimeMillis();
			this.elapsedMillis = 0L;
			this.analyzed = 0;
			this.worstScorer = null;
			return this;
		}
		
		public TopScoringWorkCollector stopCollecting() {
			collecting = false;
			elapsedMillis = System.currentTimeMillis() - startTimeMillis;
			if (logger.isDebugEnabled()) {
				logger.debug("[{}] collected {} out of {}  best score {}  (in {}ms)",
						utilization.rownum, collected.size(), analyzed,
						String.format("%.3f", getTopScore()), getElapsedMillis());
			}
			return this;
		}
		
		public boolean isEmpty() {
			return collected.isEmpty();
		}
		
		public double getTopScore() {
			return collected.isEmpty() ? 0.0 : collected.get(0).score;
		}
		
		public long getElapsedMillis() {
			return elapsedMillis;
		}

//		public List<ScoredWork> getCollected() {
//			return collected;
//		}
		
	}
	
	private final AtomicInteger startupCalls;
	private final Properties configuration;
	private final MasterDatabase masterDatabase;
	private final LuceneSearch luceneSearch;
	private final TrieSearch trieSearch;
	private final UtilizationsReader utilizationsReader;
	private final ScoreCalculator scoreCalculator;
	private final NumberSpeller numberSpeller;
	private final int threadPoolSize;
	private final StringNormalizer titleBlacklister;
	private final StringNormalizer artistBlacklister;
	private final Metric titleMetric;
	private final Metric artistMetric;
	private final long maxSearchDurationMillis;
	private final int maxCollectedResults;
	private final double minCollectedScore;
	private final int maxOutputResults;
	private final double similarityTitleWeight;
	private final double similarityArtistWeight;
	private final double titleWithArtistSearchThreshold;
	private final double spellTitleNumbersSearchThreshold;
	private final double titleOnlySearchThreshold;
	private final double titleTrieSearchThreshold;
	private final double artistOnlySearchThreshold;
	private final double artistTrieSearchThreshold;
	private final boolean debug;
	
	private ExecutorService executorService;
	private boolean interrupted;
	
	@Inject
	protected StringMatching(@Named("configuration") Properties configuration,
			MasterDatabase masterDatabase, LuceneSearch luceneSearch,
			TrieSearch trieSearch, UtilizationsReader utilizationsReader,
			ScoreCalculator scoreCalculator, NumberSpeller numberSpeller,
			Metrics metrics) {
		super();
		this.startupCalls = new AtomicInteger(0);
		this.configuration = configuration;
		this.masterDatabase = masterDatabase;
		this.luceneSearch = luceneSearch;
		this.trieSearch = trieSearch;
		this.utilizationsReader = utilizationsReader;
		this.scoreCalculator = scoreCalculator;
		this.numberSpeller = numberSpeller;
		this.threadPoolSize = Math.max(0, Integer.parseInt(configuration
				.getProperty("stringMatching.threadPoolSize", "0"))); // default 0
		this.titleBlacklister = new StringNormalizer(configuration
				.getProperty("stringMatching.blacklister.title").split(","));
		this.artistBlacklister = new StringNormalizer(configuration
				.getProperty("stringMatching.blacklister.artist").split(","));
		this.titleMetric = metrics.getInstance(configuration
				.getProperty("stringMatching.similarity.metric.title"));
		this.artistMetric = metrics.getInstance(configuration
				.getProperty("stringMatching.similarity.metric.artist"));
		this.maxSearchDurationMillis = Long.parseLong(configuration
				.getProperty("stringMatching.maxSearchDurationMillis", "30000")); // default 30 seconds
		this.maxCollectedResults = Integer.parseInt(configuration
				.getProperty("stringMatching.maxCollectedResults"));
		this.minCollectedScore = Double.parseDouble(configuration
				.getProperty("stringMatching.minCollectedScore"));
		this.maxOutputResults = Integer.parseInt(configuration
				.getProperty("stringMatching.maxOutputResults", "-1")); // default -1
		this.similarityTitleWeight = Double.parseDouble(configuration
				.getProperty("stringMatching.similarity.weight.title"));
		this.similarityArtistWeight = Double.parseDouble(configuration
				.getProperty("stringMatching.similarity.weight.artist"));
		this.titleWithArtistSearchThreshold = Double.parseDouble(configuration
				.getProperty("stringMatching.searchThreshold.titleWithArtist", "-1")); // default -1
		this.spellTitleNumbersSearchThreshold = Double.parseDouble(configuration
				.getProperty("stringMatching.searchThreshold.spellTitleNumbers", "-1")); // default -1
		this.titleOnlySearchThreshold = Double.parseDouble(configuration
				.getProperty("stringMatching.searchThreshold.titleOnly", "-1")); // default -1
		this.titleTrieSearchThreshold = Double.parseDouble(configuration
				.getProperty("stringMatching.searchThreshold.titleTrie", "-1")); // default -1
		this.artistOnlySearchThreshold = Double.parseDouble(configuration
				.getProperty("stringMatching.searchThreshold.artistOnly", "-1")); // default -1
		this.artistTrieSearchThreshold = Double.parseDouble(configuration
				.getProperty("stringMatching.searchThreshold.artistTrie", "-1")); // default -1
		this.debug = "true".equalsIgnoreCase(configuration
				.getProperty("stringMatching.debug", configuration.getProperty("default.debug")));
	}
	
	public StringMatching startup() throws IOException {
		if (0 == startupCalls.getAndIncrement()) {
			luceneSearch.startup();
			trieSearch.startup();
			masterDatabase.startup();
			utilizationsReader.startup();
			scoreCalculator.startup();
			numberSpeller.startup();
			switch (threadPoolSize) {
			case 0:
				executorService = MoreExecutors.newDirectExecutorService();
				break;
			case 1:
				executorService = Executors.newSingleThreadExecutor();
				break;
			default:
				executorService = Executors.newFixedThreadPool(threadPoolSize);
				break;
			}
		}
		return this;
	}

	public StringMatching shutdown() throws IOException {
		if (0 == startupCalls.decrementAndGet()) {
			executorService.shutdown();
			executorService = null;
			numberSpeller.shutdown();
			scoreCalculator.shutdown();
			utilizationsReader.shutdown();
			masterDatabase.shutdown();
			trieSearch.shutdown();
			luceneSearch.shutdown();
		}
		return this;
	}

	public StringMatching interrupt() {
		// raise interrupted flag
		interrupted = true;
		return this;
	}
	
	public StringMatching process(File outputFile, File skippedFile, File kpiFile) throws IOException, ParseException, InterruptedException, ExecutionException {
		// clear interruped flag
		interrupted = false;

		final String datetime = new SimpleDateFormat("yyyy_MM_dd_HHmm")
				.format(new GregorianCalendar(TimeZone.getTimeZone("GMT+2")).getTime());

		logger.info("titleMetric : {}", titleMetric.getName());
		logger.info("artistMetric : {}", artistMetric.getName());
		logger.info("maxCollectedResults : {}", maxCollectedResults);
		logger.info("minCollectedScore : {}", minCollectedScore);
		logger.info("similarityTitleWeight : {}", similarityTitleWeight);
		logger.info("similarityArtistWeight : {}", similarityArtistWeight);
		logger.info("titleWithArtistSearchThreshold : {}", titleWithArtistSearchThreshold);
		logger.info("spellTitleNumbersSearchThreshold : {}", spellTitleNumbersSearchThreshold);
		logger.info("titleOnlySearchThreshold : {}", titleOnlySearchThreshold);
		logger.info("titleTrieSearchThreshold : {}", titleTrieSearchThreshold);
		logger.info("artistOnlySearchThreshold : {}", artistOnlySearchThreshold);
		logger.info("artistTrieSearchThreshold : {}", artistTrieSearchThreshold);
		
		// save configuration
		if ("true".equalsIgnoreCase(configuration.getProperty("stringMatching.backupConfiguration"))) {
			final File configFile = new File(utilizationsReader.getHomeFolder(), datetime + "_config.properties");
			try (FileOutputStream out = new FileOutputStream(configFile)) {
				configuration.store(out, null);
			}
		}
		
		// string-matching KPIs		
		final AtomicInteger rownum = new AtomicInteger(0);
		final AtomicInteger skipBlacklistTitle = new AtomicInteger(0);
		final AtomicInteger skipBlacklistArtist = new AtomicInteger(0);
		final AtomicInteger unknownVerifiedWork = new AtomicInteger(0);
		final AtomicInteger simpleTitleAndArtist = new AtomicInteger(0);
		final AtomicInteger titleWithArtist = new AtomicInteger(0);
		final AtomicInteger titleWithArtistSpellTitleNumbers = new AtomicInteger(0);
		final AtomicInteger titleOnly = new AtomicInteger(0);
		final AtomicInteger titleOnlySpellTitleNumbers = new AtomicInteger(0);
		final AtomicInteger titleTrie = new AtomicInteger(0);
		final AtomicInteger titleTrieSpellTitleNumbers = new AtomicInteger(0);
		final AtomicInteger artistOnly = new AtomicInteger(0);
		final AtomicInteger artistTrie = new AtomicInteger(0);
		final VariableObserver durationMillis = new VariableObserver();
		// scoring KPIs
		final KPIs scoringKPIs = new KPIs();
		// synchronizetion stuff
		final int numberOfPermits = 2 * Math.max(1, threadPoolSize);
		final Semaphore semaphore = new Semaphore(numberOfPermits);
		final Lock writeLock = new ReentrantLock();
		final Lock kpiLock = new ReentrantLock();

		// process utilizations
		final long startTimeMillis = System.currentTimeMillis();
		if (null == outputFile) {
			outputFile = new File(utilizationsReader.getHomeFolder(), datetime + "_output.csv");
		}
		if (null == skippedFile) {
			skippedFile = new File(utilizationsReader.getHomeFolder(), datetime + "_skipped.csv");
		}
		try (final OutputForSiadaWriter outputWriter = new OutputForSiadaWriter(new BufferedWriter(new FileWriter(outputFile)));
				final UtilizationWriter skippedWriter = new UtilizationWriter(new FileWriter(skippedFile))) {
			
			// write headers
			outputWriter.writeHeader();
			skippedWriter.writeHeader();
			
			// loop on utilization(s)
			for (final Utilization utilization : utilizationsReader) {
				if (interrupted) {
					logger.warn("process interrupted");
					break;
				} else if (null == utilization) {
					logger.warn("utilization is null");
					continue;
				}
				rownum.incrementAndGet();

				// blacklist
				if (Strings.isNullOrEmpty(titleBlacklister.normalize(utilization.title))) {
					logger.debug("[{}] titolo non codificabile: {}", utilization.rownum, utilization);
					skipBlacklistTitle.incrementAndGet();
					continue;
				}
				final Iterator<String> iterator = utilization.artists.iterator();
				while (iterator.hasNext()) {
					if (Strings.isNullOrEmpty(artistBlacklister.normalize(iterator.next()))) {
						iterator.remove();
					}
				}
				if (utilization.artists.isEmpty()) {
					logger.debug("[{}] artista non codificabile: {}", utilization.rownum, utilization);
					skipBlacklistArtist.incrementAndGet();
					continue;
				}
				
				// check if verified code exists
				final Work verifiedWork;
				if (Strings.isNullOrEmpty(utilization.code)) {
					verifiedWork = null;
				} else {					
					verifiedWork = masterDatabase.getByCode(utilization.code);
					if (null == verifiedWork) {
						unknownVerifiedWork.incrementAndGet();
						logger.debug("[{}] codice non esistente: {}", utilization.rownum, utilization);
					}
				}

				// submit string-matching task to thread pool
				semaphore.acquire();
				try {
					executorService.execute(new Runnable() {

						@Override
						public void run() {
							try {
								// start collecting
								logger.debug("[{}] utilizzazione: {}", utilization.rownum, utilization);
								final TopScoringWorkCollector workCollector = new TopScoringWorkCollector()
										.startCollecting(utilization);
								
								// simple search for title and artist on lucene
								if (!interrupted) {
									luceneSearch.simpleSearchTitleWithArtist(utilization.title, utilization.artists, workCollector);
									simpleTitleAndArtist.incrementAndGet();
								}

								// search for title with artist on lucene
								if (!interrupted && workCollector.getTopScore() < titleWithArtistSearchThreshold) {
									if (debug) logger.debug("[{}] search title with artist now scoring {}",
											utilization.rownum, String.format("%.3f", workCollector.getTopScore()));
									luceneSearch.searchTitleWithArtist(utilization.title, utilization.artists, workCollector);
									titleWithArtist.incrementAndGet();
								}

								// search for title with artist on lucene, spelling title numbers
								Set<String> titlesWithSpelledNumbers = null;
								if (!interrupted && workCollector.getTopScore() < spellTitleNumbersSearchThreshold) {
									if (debug) logger.debug("[{}] search title with artist and spelled numbers now scoring {}",
											utilization.rownum, String.format("%.3f", workCollector.getTopScore()));
									titlesWithSpelledNumbers = numberSpeller.spell(utilization.title);
									for (String title : titlesWithSpelledNumbers) {
										luceneSearch.searchTitleWithArtist(title, utilization.artists, workCollector);
										titleWithArtistSpellTitleNumbers.incrementAndGet();
										if (workCollector.getTopScore() >= spellTitleNumbersSearchThreshold) {
											break;
										}
									}
								}
								
								// search title only with lucene
								if (!interrupted && workCollector.getTopScore() < titleOnlySearchThreshold) {
									if (debug) logger.debug("[{}] search title only now scoring {}",
											utilization.rownum, String.format("%.3f", workCollector.getTopScore()));
									luceneSearch.searchTitle(utilization.title, workCollector);
									titleOnly.incrementAndGet();
									// search title only with lucene, spelling title numbers
									if (!interrupted && workCollector.getTopScore() < spellTitleNumbersSearchThreshold) {
										if (debug) logger.debug("[{}] search title with spelled numbers now scoring {}",
												utilization.rownum, String.format("%.3f", workCollector.getTopScore()));
//										if (null == titlesWithSpelledNumbers) {
//											titlesWithSpelledNumbers = numberSpeller.spell(utilization.title);
//										}
										for (String title : titlesWithSpelledNumbers) {
											luceneSearch.searchTitle(title, workCollector);
											titleOnlySpellTitleNumbers.incrementAndGet();
											if (workCollector.getTopScore() >= spellTitleNumbersSearchThreshold) {
												break;
											}
										}				
									}
								}
								
								// search title on trie
								if (!interrupted && workCollector.getTopScore() < titleTrieSearchThreshold) {
									if (debug) logger.debug("[{}] search title on trie now scoring {}",
											utilization.rownum, String.format("%.3f", workCollector.getTopScore()));
									trieSearch.searchTitle(utilization.title, workCollector);
									titleTrie.incrementAndGet();
									// search title on trie, spelling title numbers
									if (!interrupted && workCollector.getTopScore() < spellTitleNumbersSearchThreshold) {
										if (debug) logger.debug("[{}] search title on trie with spelled numbers now scoring {}",
												utilization.rownum, String.format("%.3f", workCollector.getTopScore()));
										for (String title : titlesWithSpelledNumbers) {
											trieSearch.searchTitle(title, workCollector);
											titleTrieSpellTitleNumbers.incrementAndGet();
											if (workCollector.getTopScore() >= spellTitleNumbersSearchThreshold) {
												break;
											}
										}				
									}
								}

								// search artist on trie
								if (!interrupted && workCollector.getTopScore() < artistTrieSearchThreshold) {
									if (debug) logger.debug("[{}] search artist on trie now scoring {}",
											utilization.rownum, String.format("%.3f", workCollector.getTopScore()));
									trieSearch.searchArtist(utilization.artists, workCollector);
									artistTrie.incrementAndGet();
								}

								// search artist only on lucene
								if (!interrupted && workCollector.getTopScore() < artistOnlySearchThreshold) {
									if (debug) logger.debug("[{}] search artist only now scoring {}",
											utilization.rownum, String.format("%.3f", workCollector.getTopScore()));
									luceneSearch.searchArtist(utilization.artists, workCollector);
									artistOnly.incrementAndGet();
								}
								
								// process collected
								workCollector.stopCollecting();
								final ArrayList<ScoreResult> scoreResults = new ArrayList<>();
								if (workCollector.isEmpty()) {
									// append to skipped file
									try {
										writeLock.lock();
										skippedWriter.write(utilization);
									} finally {
										writeLock.unlock();
									}
								} else {
									// score collected works
									for (ScoredWork scoredWork : workCollector) {
										scoreResults.add(scoreCalculator.computeScore(utilization, scoredWork.work));
									}
									// sort in descending order
									scoreResults.sort((ScoreResult a, ScoreResult b)->Double.compare(b.score, a.score));
									// write to output
									try {
										writeLock.lock();
										if (maxOutputResults > 0 && maxOutputResults < scoreResults.size()) {
											outputWriter.write(scoreResults.subList(0, maxOutputResults));
										} else {
											outputWriter.write(scoreResults);
										}
									} finally {
										writeLock.unlock();
									}
								}
								
								// update KPIs
								try {
									kpiLock.lock();
									durationMillis.update(workCollector.getElapsedMillis());
									scoringKPIs.update(scoreResults, verifiedWork);
								} finally {
									kpiLock.unlock();
								}
								
							} catch (Exception e) {
								logger.error("run", e);
								exitCode = 1;
							} finally {
								semaphore.release();
							}
						}
						
					});
				} catch (RejectedExecutionException | NullPointerException e) {
					exitCode = 1;
					logger.info("Error :"+e.getMessage());
					semaphore.release();
				}
			}
			
			// wait for all tasks to complete
			semaphore.acquire(numberOfPermits);
		}
		logger.debug("batch completed in {}s", (System.currentTimeMillis() - startTimeMillis) / 1000L);			

		// save KPIs
		if (null == kpiFile) {
			kpiFile = new File(utilizationsReader.getHomeFolder(), datetime + "_KPIs.txt");
		}
		try (Writer writer = new FileWriter(kpiFile)) {
			// string-matching KPIs
			if (interrupted) {
				writer.write("esecuzione interrotta prematuramente\n");
			}
			writer.write(String.format("totale utilizzazioni: %d\n", rownum.get()));
			writer.write(String.format("utilizzazioni con titolo non codificabile: %d\n", skipBlacklistTitle.get()));
			writer.write(String.format("utilizzazioni con artista non codificabile: %d\n", skipBlacklistArtist.get()));
			writer.write(String.format("utilizzazioni con codice verificato sconosciuto: %d\n", unknownVerifiedWork.get()));
			writer.write(String.format("ricerche lucene semplici titolo e artista: %d\n", simpleTitleAndArtist.get()));
			writer.write(String.format("ricerche lucene titolo + artista: %d\n", titleWithArtist.get()));
			writer.write(String.format("ricerche lucene titolo + artista + spelling numeri: %d\n", titleWithArtistSpellTitleNumbers.get()));
			writer.write(String.format("ricerche lucene solo titolo: %d\n", titleOnly.get()));
			writer.write(String.format("ricerche lucene solo titolo + spelling numeri: %d\n", titleOnlySpellTitleNumbers.get()));
			writer.write(String.format("ricerche trie titolo: %d\n", titleTrie.get()));
			writer.write(String.format("ricerche trie titolo + spelling numeri: %d\n", titleTrieSpellTitleNumbers.get()));
			writer.write(String.format("ricerche lucene solo artista: %d\n", artistOnly.get()));
			writer.write(String.format("ricerche trie solo artista: %d\n", artistTrie.get()));
			writer.write(String.format("durata esecuzione: %dms\n", System.currentTimeMillis() - startTimeMillis));
			writer.write(String.format("durata media singola ricerca: %.0fms  (dev %.0fms, min %.0fms, max %.0fms)\n",
					durationMillis.average(), durationMillis.deviation(), durationMillis.min(), durationMillis.max()));
			// scoring KPIs
			writer.write(scoringKPIs.toString());
		}

		// print string-matching KPIs
		if (interrupted) {
			logger.info("esecuzione interrotta prematuramente\n");
		}
		logger.info(String.format("totale utilizzazioni: %d", rownum.get()));
		logger.info("utilizzazioni con titolo non codificabile: {}", skipBlacklistTitle);
		logger.info("utilizzazioni con artista non codificabile: {}", skipBlacklistArtist);
		logger.info("utilizzazioni con codice verificato sconosciuto: {}", unknownVerifiedWork);
		logger.info("ricerche lucene semplici titolo e artista: {}", simpleTitleAndArtist);
		logger.info("ricerche lucene titolo + artista: {}", titleWithArtist);
		logger.info("ricerche lucene titolo + artista + spelling numeri: {}", titleWithArtistSpellTitleNumbers);
		logger.info("ricerche lucene solo titolo: {}", titleOnly);
		logger.info("ricerche lucene solo titolo + spelling numeri: {}", titleOnlySpellTitleNumbers);
		logger.info("ricerche trie titolo: {}", titleTrie);
		logger.info("ricerche trie titolo + spelling numeri: {}", titleTrieSpellTitleNumbers);
		logger.info("ricerche lucene solo artista: {}", artistOnly);
		logger.info("ricerche trie solo artista: {}", artistTrie);
		logger.info("durata esecuzione: {}ms", System.currentTimeMillis() - startTimeMillis);
		logger.info("durata media ricerca: {}ms  (dev {}ms, min {}ms, max {}ms)",
				durationMillis.average(), durationMillis.deviation(), durationMillis.min(), durationMillis.max());
		// print scoring KPIs
		logger.info("Scoring KPIs\n{}", scoringKPIs);
		
		return this;
	}

}