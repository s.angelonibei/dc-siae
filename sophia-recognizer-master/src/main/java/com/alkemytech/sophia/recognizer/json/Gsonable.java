package com.alkemytech.sophia.recognizer.json;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public interface Gsonable {

	public JsonObject toJsonObject();
	public Gsonable initWithJsonObject(JsonObject jsonObject);
	public String toJson(Gson gson);
	public Gsonable initWithJson(String json, Gson gson);

}
