package com.alkemytech.sophia.recognizer.db;

import java.io.File;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

import com.sleepycat.persist.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.recognizer.model.Artist;
import com.alkemytech.sophia.recognizer.model.Title;
import com.alkemytech.sophia.recognizer.model.Work;
import com.alkemytech.sophia.recognizer.siada.SiadaCode;
import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.je.SequenceConfig;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class MasterDatabase {

    private static final Logger logger = LoggerFactory.getLogger(MasterDatabase.class);
    static int exitCode = 0;
    private final AtomicInteger startupCalls;
    private final Properties configuration;
    private final File homeFolder;

    private Environment environment;
    private EntityStore store;

    @Inject
    protected MasterDatabase(@Named("configuration") Properties configuration) {
        super();
        this.startupCalls = new AtomicInteger(0);
        this.configuration = configuration;
        this.homeFolder = new File(configuration.getProperty("masterDatabase.homeFolder"));
    }

    public MasterDatabase startup() {
        if (0 == startupCalls.getAndIncrement()) {
            try {
                final boolean readonly = isReadonly();
                final File environmentHome = new File(homeFolder,
                        configuration.getProperty("masterDatabase.dbFolder", "masterDatabase.bdb"));
                environmentHome.mkdirs();
                environment = new Environment(environmentHome,
                        EnvironmentConfig.DEFAULT
                                .setAllowCreate(!readonly)
                                .setReadOnly(readonly));
                store = new EntityStore(environment, "work",
                        StoreConfig.DEFAULT
                                .setAllowCreate(!readonly)
                                .setReadOnly(readonly)
                                .setDeferredWrite(true));
                store.setSequenceConfig("id",
                        SequenceConfig.DEFAULT
                                .setAllowCreate(!readonly)
                                .setInitialValue(1));
            } catch (DatabaseException e) {
                exitCode = 1;
                logger.error("startup", e);
            }
        }
        return this;
    }

    public MasterDatabase shutdown() {
        if (0 == startupCalls.decrementAndGet()) {
            try {
                if (null != store) {
                    if (!isReadonly()) {
                        store.sync();
                    }
                    store.close();
                }
            } catch (DatabaseException e) {
                logger.error("shutdown", e);
            }
            try {
                if (null != environment) {
                    if (!isReadonly()) {
                        environment.sync();
                    }
                    environment.close();
                }
            } catch (DatabaseException e) {
                logger.error("shutdown", e);
            }
        }
        return this;
    }

    public boolean isReadonly() {
        return "true".equalsIgnoreCase(configuration
                .getProperty("masterDatabase.readonly", "true"));
    }

    public MasterDatabase sync() {
        store.sync();
        return this;
    }

    public Work getById(Long id) {
        return (null == id ? null :
                store.getPrimaryIndex(Long.class, Work.class).get(id));
    }

    public Work getByCode(String code) {
        if (Strings.isNullOrEmpty(code)) {
            return null;
        }
        final PrimaryIndex<Long, Work> primaryIndex = store
                .getPrimaryIndex(Long.class, Work.class);
        final SecondaryIndex<String, Long, Work> secondaryIndex = store
                .getSecondaryIndex(primaryIndex, String.class, "code");
        return secondaryIndex.get(code);
    }

    private boolean isValid(Work work) {
        if (null == work) {
            return false;
        } else if (!SiadaCode.isValid(work.code)) {
            return false;
        } else if (null == work.title ||
                Strings.isNullOrEmpty(work.title.title)) {
            return false;
        }
        if (null == work.artists || work.artists.isEmpty()) {
            return false;
        } else {
            for (Artist artist : work.artists) {
                if (Strings.isNullOrEmpty(artist.name)) {
                    return false;
                }
            }
        }
        if (null != work.titles) {
            for (Title title : work.titles) {
                if (Strings.isNullOrEmpty(title.title)) {
                    return false;
                }
            }
        }
        return true;
    }

    public Work replaceIfExists(Work work) {
        if (!isValid(work)) {
            return null;
        }
        final PrimaryIndex<Long, Work> primaryIndex = store
                .getPrimaryIndex(Long.class, Work.class);
        final Work existingWork = (null == work.id ?
                getByCode(work.code) : getById(work.id));
        if (null != existingWork) {
            work.id = existingWork.id;
            if (!work.equals(existingWork)) {
                primaryIndex.putNoReturn(work);
            }
        }
        return existingWork;
    }

    public Work replaceAlways(Work work) {
        if (!isValid(work)) {
            return null;
        }
        final PrimaryIndex<Long, Work> primaryIndex = store
                .getPrimaryIndex(Long.class, Work.class);
        return primaryIndex.put(work);
    }

    public Work insertOrReplace(Work work) {
        if (!isValid(work)) {
            return null;
        }
        final PrimaryIndex<Long, Work> primaryIndex = store
                .getPrimaryIndex(Long.class, Work.class);
        final Work existingWork = (null == work.id ?
                getByCode(work.code) : getById(work.id));
        if (null == existingWork) {
            work.id = store.getSequence("id").get(null, 1);
            primaryIndex.putNoReturn(work);
            return work;
        } else {
            work.id = existingWork.id;
            if (!work.equals(existingWork)) {
                primaryIndex.putNoReturn(work);
            }
            return existingWork;
        }
    }

    public MasterDatabase selectAllWorks(Work.Selector selector) {
        final PrimaryIndex<Long, Work> primaryIndex = store.getPrimaryIndex(Long.class, Work.class);
        try (EntityCursor<Work> works = primaryIndex.entities()) {
            for (Work work = works.first(); null != work; work = works.next()) {
                selector.select(work);
            }
        }
        return this;
    }

    public MasterDatabase selectAllWorks(Work.Selector selector, int exitCode) {
        try {
            final PrimaryIndex<Long, Work> primaryIndex = store.getPrimaryIndex(Long.class, Work.class);
            try (EntityCursor<Work> works = primaryIndex.entities()) {
                for (Work work = works.first(); null != work; work = works.next()) {
                    selector.select(work);
                }
            } catch (Exception e) {
                exitCode = 1;
                logger.info("Error :"+e.getMessage());
            }
        } catch (IndexNotAvailableException e) {
            exitCode = 1;
            logger.info("Error :"+e.getMessage());
        }
        return this;
    }

    public MasterDatabase selectAllWorks(Work.Selector selector, int limit, int exitCode) {
        try {
            final PrimaryIndex<Long, Work> primaryIndex = store.getPrimaryIndex(Long.class, Work.class);
            try (EntityCursor<Work> works = primaryIndex.entities()) {
                for (Work work = works.first(); null != work && limit > 0; work = works.next(), limit--) {
                    selector.select(work);
                }
            } catch (Exception e) {
                exitCode = 1;
                logger.info("Error :"+e.getMessage());
            }
        } catch (IndexNotAvailableException e) {
            exitCode = 1;
            logger.info("Error :"+e.getMessage());
        }
        return this;
    }

}
