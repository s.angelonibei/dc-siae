package com.alkemytech.sophia.recognizer.kpi;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class NonNegativeVariableObserver extends VariableObserver {
	
	@Override
	public VariableObserver update(double x) {
		if (x < 0.0) {
			throw new IllegalArgumentException("unespected negative value " + x);
		}
		return super.update(x);
	}

}
