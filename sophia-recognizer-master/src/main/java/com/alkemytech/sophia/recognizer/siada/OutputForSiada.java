package com.alkemytech.sophia.recognizer.siada;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public interface OutputForSiada {

	public static final int FLAG_VALIDAZIONE = 0;
	public static final int UTENTE = 1;
	public static final int CODICE_OPERA_ASSEGNATO = 2;
	public static final int CODICE_COMBINAZIONE_ANAGRAFICA = 3;
	public static final int CODICE_OPERA = 4;
	public static final int FLAG_RISULTATI_MULTIPLI = 5;
	public static final int TITOLO_UTILIZZAZIONE = 6;
	public static final int COMP1_COD_IPI1 = 7;
	public static final int COMP2_COD_IPI2 = 8;
	public static final int COMP3_COD_IPI3 = 9;
	public static final int TITOLO_OPERA = 10;
	public static final int SEQUENZA_COMPOSITORI = 11;
	public static final int SEQUENZA_AUTORI = 12;
	public static final int SEQUENZA_INTERPRETI_ABBINATI = 13;
	public static final int NUMERO_RISULTATI = 14;
	public static final int PROGRESSIVO = 15;
	public static final int TIPO_CODIFICA = 16;
	public static final int IMPORTO = 17;
	public static final int GRADO_DI_CONFIDENZA = 18;
	public static final int INDICE_DI_RILEVANZA_ECONOMICA = 19;
	public static final int FONTE_DATI_UTILIZZATA = 20;
	public static final int NAZIONALITA = 21;
	public static final int FLAG_OPERA_PD = 22;
	public static final int FLAG_OPERA_EL = 23;
	public static final int SOMIGLIANZA_TITOLO = 24;
	public static final int SOMIGLIANZA_COMPOSITORI = 25;
	public static final int SOMIGLIANZA_AUTORI = 26;
	public static final int SOMIGLIANZA_INTERPRETI = 27;
	public static final int NON_SODDISFAZIONE_SOGLIA_MINIMI = 28;

	public static final String[] HEADER = {
			"FLAG VALIDAZIONE",
			"UTENTE",
			"CODICE OPERA ASSEGNATO",
			"CODICE COMBINAZIONE ANAGRAFICA",
			"CODICE OPERA",
			"FLAG RISULTATI MULTIPLI",
			"TITOLO UTILIZZAZIONE",
			"COMP.1/COD_IPI.1",
			"COMP.2/COD_IPI.2",
			"COMP.3/COD_IPI.3",
			"TITOLO OPERA",
			"SEQUENZA COMPOSITORI",
			"SEQUENZA AUTORI",
			"SEQUENZA INTERPRETI ABBINATI",
			"NUMERO RISULTATI",
			"PROGRESSIVO",
			"TIPO CODIFICA",
			"IMPORTO",
			"GRADO DI CONFIDENZA",
			"INDICE DI RILEVANZA ECONOMICA",
			"FONTE DATI UTILIZZATA",
			"NAZIONALITA",
			"FLAG OPERA PD",
			"FLAG OPERA EL",
			"SOMIGLIANZA TITOLO",
			"SOMIGLIANZA COMPOSITORI",
			"SOMIGLIANZA AUTORI",
			"SOMIGLIANZA INTERPRETI",
			"NON SODDISFAZIONE SOGLIA MINIMI"
	};
	
}
