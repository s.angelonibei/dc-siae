package com.alkemytech.sophia.recognizer.siada;

import java.io.Closeable;
import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.QuoteMode;

import com.alkemytech.sophia.recognizer.model.Artist;
import com.alkemytech.sophia.recognizer.model.ArtistRole;
import com.alkemytech.sophia.recognizer.model.Work;
import com.alkemytech.sophia.recognizer.score.ScoreResult;
import com.alkemytech.sophia.recognizer.utilization.Utilization;
import com.google.common.base.Strings;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class OutputForSiadaWriter implements OutputForSiada, Closeable {

	private final Writer writer;
	private final CSVPrinter printer;
	
	public OutputForSiadaWriter(Writer writer) throws IOException {
		super();
		this.writer = writer;
		this.printer = new CSVPrinter(writer, CSVFormat.DEFAULT
				.withIgnoreEmptyLines()
				.withNullString("")
				.withQuoteMode(QuoteMode.NON_NUMERIC)
				.withQuote('"')
				.withIgnoreSurroundingSpaces()
				.withCommentMarker('#'));
	}
	
	@Override
	public void close() throws IOException {
		if (null != printer) {
			printer.close();
		}
		if (null != writer) {
			writer.close();
		}
	}
	
	private String getArtistList(Set<Artist> artists, ArtistRole role, char separator) {
		final StringBuilder builder = new StringBuilder();
		for (Artist artist : artists) {
			if (artist.role == role) {
				if (builder.length() > 0) {
					builder.append(separator);
				}
				builder.append(artist.name);
			}
		}
		return builder.toString();
	}

	public void writeHeader() throws IOException {
		for (String header : HEADER) {
			printer.print(header);
		}
		printer.println();
	}
	
	private String getFonteDatiUtilizzata(ScoreResult scoreResult) {
		String titleTag = null;
		if (null != scoreResult.scoredTitle &&
				null != scoreResult.scoredTitle.title) {
			titleTag = scoreResult.scoredTitle.title.tag.toUpperCase();
		}
		String artistTag = null;
		if (null != scoreResult.scoredArtist &&
				null != scoreResult.scoredArtist.artist) {
			artistTag = scoreResult.scoredArtist.artist.tag.toUpperCase();
		}
		if (null == titleTag) {
			return artistTag;
		} else if (null == artistTag) {
			return titleTag;
		} else {
			return titleTag + '+' + artistTag;
		}
	}

	public void write(List<ScoreResult> scoreResults) throws IOException {
		final AtomicInteger progressivo = new AtomicInteger(0);
		for (ScoreResult scoreResult : scoreResults) {
			final Utilization utilization = scoreResult.utilization;
			final Work work = scoreResult.work;
			printer.print(Strings.isNullOrEmpty(utilization.code) ? "" : "X"); // 0 FLAG VALIDAZIONE
			printer.print("SOPHIA"); // 1 UTENTE
			printer.print(work.code); // 2 CODICE OPERA ASSEGNATO
			printer.print(utilization.id); // 3 CODICE COMBINAZIONE ANAGRAFICA
			printer.print(utilization.code); // 4 CODICE OPERA
			printer.print(scoreResults.size() > 1 ? "X" : null); // 5 FLAG RISULTATI MULTIPLI
			printer.print(utilization.title); // 6 TITOLO UTILIZZAZIONE
			for (String artist : utilization.artists) {
				printer.print(artist); // 7..9 COMP n/COD IPI n
			}
			for (int i = utilization.artists.size(); i < 3; i ++) {
				printer.print(null); // 7..9 COMP n/COD IPI n
			}
			printer.print(work.title.title); // 10 TITOLO OPERA
			printer.print(getArtistList(work.artists, ArtistRole.COMPOSER, '-')); // 11 SEQUENZA COMPOSITORI
			printer.print(getArtistList(work.artists, ArtistRole.AUTHOR, '-')); // 12 SEQUENZA AUTORI
			printer.print(getArtistList(work.artists, ArtistRole.PERFORMER, '-')); // 13 SEQUENZA INTERPRETI ABBINATI
			printer.print(scoreResults.size()); // 14 NUMERO RISULTATI
			printer.print(progressivo.incrementAndGet()); // 15 PROGRESSIVO
			printer.print("FUZZY"); // 16 TIPO CODIFICA
			printer.print(null); // 17 IMPORTO
			printer.print(Math.round(100 * scoreResult.score)); // 18 GRADO DI CONFIDENZA
			printer.print(null == work.value ? "0.0" : String.format("%.15f", work.value)); // 19 INDICE DI RILEVANZA ECONOMICA
			printer.print(getFonteDatiUtilizzata(scoreResult)); // 20 FONTE DATI UTILIZZATA
			printer.print(work.scope); // 21 NAZIONALITA
			printer.print(null != work.pd && work.pd ? "X" : null); // 22 FLAG OPERA PD
			printer.print(null != work.el && work.el ? "X," : null); // 23 FLAG OPERA EL
			printer.print(Math.round(100 * scoreResult.scoredTitle.score)); // 24 SOMIGLIANZA TITOLO
			printer.print(Math.round(100 * scoreResult.scoredComposer.score)); // 25 SOMIGLIANZA COMPOSITORI
			printer.print(Math.round(100 * scoreResult.scoredAuthor.score)); // 26 SOMIGLIANZA AUTORI
			printer.print(Math.round(100 * scoreResult.scoredPerformer.score)); // 27 SOMIGLIANZA INTERPRETI
			printer.print(null); // 28 NON SODDISFAZIONE SOGLIA MINIMI
			printer.println();
		}
	}

}
