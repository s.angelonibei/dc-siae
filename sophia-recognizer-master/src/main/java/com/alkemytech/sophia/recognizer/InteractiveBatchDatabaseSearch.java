package com.alkemytech.sophia.recognizer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.lucene.queryparser.classic.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.recognizer.db.BatchDatabase;
import com.alkemytech.sophia.recognizer.model.Batch;
import com.alkemytech.sophia.recognizer.model.Batch.Selector;
import com.alkemytech.sophia.recognizer.model.BatchStatus;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class InteractiveBatchDatabaseSearch {
	
	private static final Logger logger = LoggerFactory.getLogger(InteractiveBatchDatabaseSearch.class);

	protected static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args) {
			super(args);
		}

		@Override
		protected void configure() {
			super.configure();
			// singleton(s)
			bind(BatchDatabase.class).asEagerSingleton();
			bind(InteractiveBatchDatabaseSearch.class).asEagerSingleton();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			final InteractiveBatchDatabaseSearch instance = Guice
				.createInjector(new GuiceModuleExtension(args))
					.getInstance(InteractiveBatchDatabaseSearch.class);
			try {
				instance.startup()
					.process();
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}
	
	private final BatchDatabase batchDatabase;
	
	@Inject
	protected InteractiveBatchDatabaseSearch(@Named("configuration") Properties configuration,
			BatchDatabase batchDatabase) {
		super();
		this.batchDatabase = batchDatabase;
	}
	
	public InteractiveBatchDatabaseSearch startup() {
		batchDatabase.startup();
		return this;
	}

	public InteractiveBatchDatabaseSearch shutdown() {
		batchDatabase.shutdown();
		return this;
	}
	
	public InteractiveBatchDatabaseSearch process() throws IOException, ParseException {
		
		Pattern selectPattern = Pattern.compile(".*select\\s(\\*|queued|running|killed|completed|failed).*");
		Pattern deletePattern = Pattern.compile(".*delete\\s(s3\\://.*).*");
		
		System.out.println();
		System.out.println("type \"exit\" to quit");
		try (BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in))) {
			for (String line = stdin.readLine(); !"exit".equals(line); line = stdin.readLine()) {
				if (null == line || "".equals(line)) {
					continue;
				}
//				System.out.println("input text: \"" + line + "\"");
				
				Matcher selectMatcher = selectPattern.matcher(line);
				Matcher deleteMatcher = deletePattern.matcher(line);
				
				if (selectMatcher.matches()) {
					final BatchStatus status = "*".equals(selectMatcher.group(1)) ?
							null : BatchStatus.parse(selectMatcher.group(1));
					final AtomicInteger rownum = new AtomicInteger(0);
					batchDatabase.selectAllBatches(new Batch.Selector() {
						@Override
						public Selector select(Batch batch) {
							if (null == status || status == batch.status) {
								System.out.println(String.format("%04d: %s",
										rownum.incrementAndGet(), batch));
							}
							return this;
						}
					});
					if (0 == rownum.get()) {
						System.out.println("no batch found");
					}
				} else if (deleteMatcher.matches()) {
					final String url = deleteMatcher.group(1);
					if (batchDatabase.deleteByUrl(url)) {
						batchDatabase.sync();
						System.out.println("batch deleted");
					} else {
						System.out.println("batch not found");
					}
				} else {
					System.out.println("bad command");
				}
				
				System.out.println();
			}
		}
		
		return this;
	}

}