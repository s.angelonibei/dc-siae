package com.alkemytech.sophia.recognizer.jdbc;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;

import com.google.inject.Provider;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class DBCP2DataSourceProvider implements Provider<DataSource> {

	private final BasicDataSource dataSource;

	public DBCP2DataSourceProvider(Properties configuration, String dataSourceName) {
		super();
		dataSource = new BasicDataSource();
		dataSource.setDriverClassName(com.mysql.jdbc.Driver.class.getName());
		dataSource.setUrl(configuration.getProperty(dataSourceName + ".jdbc.url"));
		dataSource.setUsername(configuration.getProperty(dataSourceName + ".jdbc.user"));
		dataSource.setPassword(configuration.getProperty(dataSourceName + ".jdbc.password"));
		dataSource.setInitialSize(Integer.parseInt(configuration.getProperty(dataSourceName + ".dbcp.initial_size", "0")));
		dataSource.setMaxTotal(Integer.parseInt(configuration.getProperty(dataSourceName + ".dbcp.max_total", "8")));
		dataSource.setMaxIdle(Integer.parseInt(configuration.getProperty(dataSourceName + ".dbcp.max_idle", "8")));
		dataSource.setMinIdle(Integer.parseInt(configuration.getProperty(dataSourceName + ".dbcp.min_idle", "0")));
		dataSource.setMaxWaitMillis(Long.parseLong(configuration.getProperty(dataSourceName + ".dbcp.max_wait_millis", "-1")));
	}

	@Override
	public DataSource get() {
		return dataSource;
	}

}
