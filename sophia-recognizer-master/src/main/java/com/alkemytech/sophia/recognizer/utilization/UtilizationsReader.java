package com.alkemytech.sophia.recognizer.utilization;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.QuoteMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * Read utilizations from filesystem
 * 
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class UtilizationsReader implements Iterator<Utilization>, Iterable<Utilization> {
	
	private static final Logger logger = LoggerFactory.getLogger(UtilizationsReader.class);
	
	private final Properties configuration;
	private final File homeFolder;
	private final boolean toUpperCase;
	private final boolean skipHeader;
	private final String delimiter;
	private final String artistSubDelimiter;
	private final int columnId;
	private final int columnTitle;
	private final int columnArtist1;
	private final int columnArtist2;
	private final int columnArtist3;
	private final int columnResultType;
	private final int columnValidated;
	private final int columnCode;

	private Iterator<File> filesIterator;
	private Reader reader;
	private CSVParser parser;
	private Iterator<CSVRecord> recordsIterator;
	private int limit;
	private int rownum;
	private boolean started;
		
	@Inject
	protected UtilizationsReader(@Named("configuration") Properties configuration) throws IOException {
		super();
		this.configuration = configuration;
		this.homeFolder = new File(configuration.getProperty("utilizationsReader.homeFolder"));
		this.toUpperCase = "true".equalsIgnoreCase(configuration.getProperty("utilizationsReader.toUpperCase", "false"));
		this.skipHeader = "true".equalsIgnoreCase(configuration.getProperty("utilizationsReader.skipHeader", "false"));
		this.delimiter = configuration.getProperty("utilizationsReader.delimiter", ",");
		this.artistSubDelimiter = configuration.getProperty("utilizationsReader.artistSubDelimiter");
		this.columnId = Integer.parseInt(configuration.getProperty("utilizationsReader.column.id", "-1"));
		this.columnTitle = Integer.parseInt(configuration.getProperty("utilizationsReader.column.title", "-1"));
		this.columnArtist1 = Integer.parseInt(configuration.getProperty("utilizationsReader.column.artist_1", "-1"));
		this.columnArtist2 = Integer.parseInt(configuration.getProperty("utilizationsReader.column.artist_2", "-1"));
		this.columnArtist3 = Integer.parseInt(configuration.getProperty("utilizationsReader.column.artist_3", "-1"));
		this.columnResultType = Integer.parseInt(configuration.getProperty("utilizationsReader.column.result_type", "-1"));
		this.columnValidated = Integer.parseInt(configuration.getProperty("utilizationsReader.column.validated", "-1"));
		this.columnCode = Integer.parseInt(configuration.getProperty("utilizationsReader.column.code", "-1"));
	}

	public UtilizationsReader startup() {
		if (!started) {
			final ArrayList<File> files = new ArrayList<>();
			for (String filename : configuration.getProperty("utilizationsReader.filenames").split(",")) {
				files.add(new File(homeFolder, filename));
			}
			filesIterator = files.iterator();
			limit = Integer.parseInt(configuration.getProperty("utilizationsReader.limit", Integer.toString(Integer.MAX_VALUE)));
			rownum = 0;
			started = true;
		}
		return this;
	}
	
	public UtilizationsReader shutdown() {
		if (started) {
			started = false;
			try {
				closeIfNeeded();
			} catch (IOException e) {
				logger.error("shutdown", e);
			}
		}
		return this;
	}

	@Override
	public Iterator<Utilization> iterator() {
		return this;
	}

	@Override
	public boolean hasNext() {
		try {
			if (limit < 1) {
				return false;
			}
			if (null == recordsIterator) {
				if (!filesIterator.hasNext()) {
					return false;
				}
				closeIfNeeded();
				reader = new BufferedReader(new FileReader(filesIterator.next()));
				parser = new CSVParser(reader, CSVFormat.DEFAULT
						.withIgnoreEmptyLines()
						.withQuoteMode(QuoteMode.MINIMAL)
						.withQuote('"')
						.withIgnoreSurroundingSpaces()
						.withDelimiter(delimiter.trim().charAt(0))
						.withCommentMarker('#'));
				recordsIterator = parser.iterator();
				if (skipHeader) {
					if (recordsIterator.hasNext()) {
						recordsIterator.next();
					}
				}
			}
			if (recordsIterator.hasNext()) {
				return true;
			}
			recordsIterator = null;
			return hasNext();
		} catch (IOException e) {
			logger.error("hasNext", e);
		}
		return false;
	}

	@Override
	public Utilization next() {
		if (null == recordsIterator) {
			return null;
		}
		final CSVRecord record = recordsIterator.next();
		rownum ++;
		limit --;
		return new Utilization(rownum, -1 == columnId ? null : record.get(columnId),
			decodeTitle(record),
			decodeArtist(record),
			decodeResultType(record),
			decodeValidated(record),
			decodeCode(record));
	}
	
	public File getHomeFolder() {
		return homeFolder;
	}
	
	private void closeIfNeeded() throws IOException {
		if (null != parser) {
			parser.close();
		}
		if (null != reader) {
			reader.close();
		}
	}
	
	private String decodeTitle(CSVRecord record) {
		if (-1 == columnTitle) {
			return null;
		}
		final String title = record.get(columnTitle);
		return toUpperCase ? title.toUpperCase() : title;
	}
	
	private Set<String> decodeArtist(CSVRecord record) {
		final Set<String> artists = new HashSet<>();
		if (-1 != columnArtist1) {
			final String artist = record.get(columnArtist1);
			if (!Strings.isNullOrEmpty(artist)) {
				if (Strings.isNullOrEmpty(artistSubDelimiter)) {
					artists.add(toUpperCase ? artist.toUpperCase() : artist);
				} else {
					for (String subArtist : artist.split(artistSubDelimiter)) {
						artists.add(toUpperCase ? subArtist.toUpperCase() : subArtist);
					}
				}
			}
		}
		if (-1 != columnArtist2) {
			final String artist = record.get(columnArtist2);
			if (!Strings.isNullOrEmpty(artist)) {
				if (Strings.isNullOrEmpty(artistSubDelimiter)) {
					artists.add(toUpperCase ? artist.toUpperCase() : artist);
				} else {
					for (String subArtist : artist.split(artistSubDelimiter)) {
						artists.add(toUpperCase ? subArtist.toUpperCase() : subArtist);
					}
				}
			}
		}
		if (-1 != columnArtist3) {
			final String artist = record.get(columnArtist3);
			if (!Strings.isNullOrEmpty(artist)) {
				if (Strings.isNullOrEmpty(artistSubDelimiter)) {
					artists.add(toUpperCase ? artist.toUpperCase() : artist);
				} else {
					for (String subArtist : artist.split(artistSubDelimiter)) {
						artists.add(toUpperCase ? subArtist.toUpperCase() : subArtist);
					}
				}
			}
		}
		return artists;
	}

	private int decodeResultType(CSVRecord record) {
		if (-1 == columnResultType) {
			return -1;
		}
		final String resultType = record.get(columnResultType);
		if ("MULTI".equalsIgnoreCase(resultType)) {
			return 2;
		} else if ("UNI".equalsIgnoreCase(resultType)) {
			return 1;
		} else if ("NO RISULTATO".equalsIgnoreCase(resultType)) {
			return 0;
		}
		return -1;
	}
	
	private boolean decodeValidated(CSVRecord record) {
		if (-1 == columnValidated) {
			return false;
		}
		final String validated = record.get(columnValidated);
		if ("SI".equalsIgnoreCase(validated)) {
			return true;
//		} else if ("NON CONTROLLATA".equalsIgnoreCase(validated)) {
//			return false;
//		} else if ("-".equalsIgnoreCase(validated)) {
//			return false;
		}
		return false;
	}
	
	private String decodeCode(CSVRecord record) {
		if (-1 == columnCode) {
			return null;
		}
		final String code = record.get(columnCode);
		if ("".equals(code) || "-".equals(code)) {
			return null;
		}
		return code;
	}

}
