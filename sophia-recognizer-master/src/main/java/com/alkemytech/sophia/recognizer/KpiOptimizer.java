package com.alkemytech.sophia.recognizer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.QuoteMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.recognizer.db.MasterDatabase;
import com.alkemytech.sophia.recognizer.kpi.KPIs;
import com.alkemytech.sophia.recognizer.metric.Metrics;
import com.alkemytech.sophia.recognizer.model.Work;
import com.alkemytech.sophia.recognizer.norm.Norms;
import com.alkemytech.sophia.recognizer.score.ScoreCalculator;
import com.alkemytech.sophia.recognizer.score.ScoreResult;
import com.alkemytech.sophia.recognizer.siada.OutputForSiadaReader;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class KpiOptimizer {
	
	private static final Logger logger = LoggerFactory.getLogger(KpiOptimizer.class);

	private static class ScoredConfiguration {
		
		public final Properties properties;
		public final KPIs scoreKPIs;
		public final double score;
		
		public ScoredConfiguration(Properties properties, KPIs scoreKPIs, double score) {
			this.properties = properties;
			this.scoreKPIs = scoreKPIs;
			this.score = score;
		}
		
	}

	protected static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args) {
			super(args);
		}

		@Override
		protected void configure() {
			super.configure();
			// singleton(s)
			bind(MasterDatabase.class).asEagerSingleton();
			bind(Metrics.class).asEagerSingleton();
			bind(Norms.class).asEagerSingleton();
			bind(KpiOptimizer.class).asEagerSingleton();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			final KpiOptimizer instance = Guice.createInjector(new GuiceModuleExtension(args))
				.getInstance(KpiOptimizer.class)
					.startup();
			try {
				instance.process();
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}

	private final Properties configuration;
	private final MasterDatabase masterDatabase;
	private final Metrics metrics;
	private final Norms norms;
	
	@Inject
	protected KpiOptimizer(@Named("configuration") Properties configuration,
			MasterDatabase masterDatabase, ScoreCalculator scoreCalculator,
			Metrics metrics, Norms norms) {
		super();
		this.configuration = configuration;
		this.masterDatabase = masterDatabase;
		this.metrics = metrics;
		this.norms = norms;
	}

	public KpiOptimizer startup() {
		masterDatabase.startup();
		return this;
	}

	public KpiOptimizer shutdown() {
		masterDatabase.shutdown();
		return this;
	}
	
	public KpiOptimizer process() throws IOException {
				
		final File homeFolder = new File(configuration.getProperty("kpiOptimizer.homeFolder"));
		final Pattern filenames = Pattern.compile(configuration.getProperty("kpiOptimizer.filenames"));
		final String[] weights = configuration.getProperty("kpiOptimizer.weights").split(",");
		final String[] exponents = configuration.getProperty("kpiOptimizer.exponents").split(",");
		final String[] titleMetrics = configuration.getProperty("kpiOptimizer.metrics.title").split(",");
		final String[] artistMetrics = configuration.getProperty("kpiOptimizer.metrics.artist").split(",");
		final String[] valueNorms = configuration.getProperty("kpiOptimizer.norms.value").split(",");
		final int numberOfSeeds = Integer.parseInt(configuration.getProperty("kpiOptimizer.numberOfSeeds"));
		final int iterations = Integer.parseInt(configuration.getProperty("kpiOptimizer.jitter.iterations"));
		final double amplitude = Double.parseDouble(configuration.getProperty("kpiOptimizer.jitter.amplitude"));
		final String datetime = new SimpleDateFormat("yyyy_MM_dd_HHmm")
				.format(new GregorianCalendar(TimeZone.getTimeZone("GMT+2")).getTime());
		
		// build sorted file list
		final ArrayList<File> files = new ArrayList<>();
		for (File file : homeFolder.listFiles()) {
			if (file.isFile() &&
					filenames.matcher(file.getName()).matches()) {
				files.add(file);
			}
		}
		files.sort((File a, File b)->a.getName().compareTo(b.getName()));

		// estimate duration and print estimated remaining time
		final long startTimeMillis = System.currentTimeMillis();
		final int totalSteps = iterations + titleMetrics.length * artistMetrics.length *
				valueNorms.length * weights.length * exponents.length * exponents.length;		
		int currentStep = 0;
		
		// save all scored configurations
		final ArrayList<ScoredConfiguration> topScored = new ArrayList<>(numberOfSeeds + 1);
		try (Writer writer = new FileWriter(new File(homeFolder, datetime + "_kpi_optimizer.csv"));
				CSVPrinter printer = new CSVPrinter(writer, CSVFormat.DEFAULT
						.withNullString("").withQuoteMode(QuoteMode.NON_NUMERIC).withQuote('"'))) {
			printHeader(printer);
			
			// hypercube search
			for (String titleWeight : weights) {
				String artistWeight = String.format("%.6f", 1.0 - Double.parseDouble(titleWeight));
				String valueWeight = "0.0";
				for (String titleExponent : exponents) {
					for (String artistExponent : exponents) {
						String valueExponent = "0.0";
						for (String titleMetric : titleMetrics) {
							for (String artistMetric : artistMetrics) {
								for (String valueNorm : valueNorms) {
									
									// score calculator configuration
									final Properties properties = new Properties();
									// Wt * (Mt(title) ^ Et)  [+|*]  Wa * (Ma(artist) ^ Ea)  [+|*]  Wv * (Nv(value) ^ Ev)
									properties.setProperty("scoreCalculator.weight.title", titleWeight);
									properties.setProperty("scoreCalculator.weight.artist.author", artistWeight);
									properties.setProperty("scoreCalculator.weight.artist.composer", artistWeight);
									properties.setProperty("scoreCalculator.weight.artist.performer", artistWeight);
									properties.setProperty("scoreCalculator.weight.value", valueWeight);
									properties.setProperty("scoreCalculator.exponent.title", titleExponent);
									properties.setProperty("scoreCalculator.exponent.artist.author", artistExponent);
									properties.setProperty("scoreCalculator.exponent.artist.composer", artistExponent);
									properties.setProperty("scoreCalculator.exponent.artist.performer", artistExponent);
									properties.setProperty("scoreCalculator.exponent.value", valueExponent);
									properties.setProperty("scoreCalculator.metric.title", titleMetric);
									properties.setProperty("scoreCalculator.metric.artist", artistMetric);
									properties.setProperty("scoreCalculator.norm.value", valueNorm);
									
									// run optimization step
									currentStep ++;
									final long elapsedTimeMillis = System.currentTimeMillis() - startTimeMillis;
									final long averageStepTime = elapsedTimeMillis / currentStep;
									final int remainingSteps = totalSteps - currentStep;
									final Date estimatedFinishTime = new Date(System.currentTimeMillis() + averageStepTime * remainingSteps);
									logger.debug("*** running hypercube optimization step {} of {} ***", currentStep, totalSteps);
									logger.debug("*** remaining {} steps will be completed on {} ***", remainingSteps,
											DateFormat.getDateTimeInstance().format(estimatedFinishTime));
									logger.debug("average step time {}ms", averageStepTime);
									
									// run optimization step and save results
									final ScoredConfiguration scoredConfiguration = runOptimizationStep(properties, files);
									print(printer, scoredConfiguration);
									
									// update top scored
									if (topScored.isEmpty() ||
											scoredConfiguration.score > topScored.get(topScored.size() - 1).score) {
										topScored.add(scoredConfiguration);
										topScored.sort((ScoredConfiguration a, ScoredConfiguration b)->(Double.compare(b.score, a.score)));
										while (topScored.size() > numberOfSeeds) {
											topScored.remove(topScored.size() - 1);
										}
									}
									logger.debug("current top score {}", topScored.get(0).score);
								}
							}
						}
					}
				}
			}
			
			// jittered search
			for (int i = 0; i < iterations; i ++) {
				
				// peek seed configuration
				final Properties properties = jitter(topScored.get(i % topScored.size()).properties, amplitude);
				
				// run optimization step
				currentStep ++;
				final long elapsedTimeMillis = System.currentTimeMillis() - startTimeMillis;
				final long averageStepTime = elapsedTimeMillis / currentStep;
				final int remainingSteps = totalSteps - currentStep;
				final Date estimatedFinishTime = new Date(System.currentTimeMillis() + averageStepTime * remainingSteps);
				logger.debug("*** running jitter optimization step {} of {} ***", currentStep, totalSteps);
				logger.debug("*** remaining {} steps will be completed on {} ***", remainingSteps,
						DateFormat.getDateTimeInstance().format(estimatedFinishTime));
				logger.debug("average step time {}ms", averageStepTime);
				
				// run optimization step and save results
				final ScoredConfiguration scoredConfiguration = runOptimizationStep(properties, files);
				print(printer, scoredConfiguration);
				
				// update top scored
				if (scoredConfiguration.score > topScored.get(topScored.size() - 1).score) {
					topScored.add(scoredConfiguration);
					topScored.sort((ScoredConfiguration a, ScoredConfiguration b)->(Double.compare(b.score, a.score)));
					while (topScored.size() > numberOfSeeds) {
						topScored.remove(topScored.size() - 1);
					}
				}
				logger.debug("current top score {}", topScored.get(0).score);
				
			}
			
		}

		// save top configurations
		try (Writer writer = new FileWriter(new File(homeFolder, datetime + "_kpi_optimizer_top.csv"));
				CSVPrinter printer = new CSVPrinter(writer, CSVFormat.DEFAULT
						.withNullString("").withQuoteMode(QuoteMode.NON_NUMERIC).withQuote('"'))) {
			printHeader(printer);
			for (ScoredConfiguration scoredConfiguration : topScored) {
				print(printer, scoredConfiguration);
				logger.debug("score {}", scoredConfiguration.score);
				logger.debug("properties\n{}", scoredConfiguration.properties);
				logger.debug("KPIs\n{}", scoredConfiguration.scoreKPIs);
			}	
		}
				
		return this;
	}
	
	private double scoreFromKPIs(KPIs scoreKPIs) {
		double score = (double) scoreKPIs.numeroProposteConCorrettaAlTop.get() /
				(double) scoreKPIs.numeroProposteConCorretta.get();
		score = 0.95 * score + 0.05 * scoreKPIs.deltaPrimaSecondaConCorrettaAlTop.average();
		return score;
	}
	
	private double jitter(Properties properties, String name, double amplitude) {
		double value = Double.parseDouble(properties.getProperty(name));
		value += amplitude * (Math.random() - 0.5);
		properties.put(name, String.format("%.9f", value));
		return value;
	}
	
	private Properties jitter(Properties properties, double amplitude) {
		final Properties config = new Properties();
		config.putAll(properties);
		final double titleWeight = jitter(config, "scoreCalculator.weight.title", amplitude);
		final double artistWeight = 1.0 - titleWeight;
		config.setProperty("scoreCalculator.weight.artist.author", String.format("%.9f", artistWeight));
		config.setProperty("scoreCalculator.weight.artist.composer", String.format("%.9f", artistWeight));
		config.setProperty("scoreCalculator.weight.artist.performer", String.format("%.9f", artistWeight));
		jitter(config, "scoreCalculator.exponent.title", amplitude);
		jitter(config, "scoreCalculator.exponent.artist.author", amplitude);
		jitter(config, "scoreCalculator.exponent.artist.composer", amplitude);
		jitter(config, "scoreCalculator.exponent.artist.performer", amplitude);
		return config;
	}

	private void printHeader(CSVPrinter printer) throws IOException {
		printer.print("SCORE");
		printer.print("PESO TITOLO Wt");
		printer.print("PESO AUTORE Waa");
		printer.print("PESO COMPOSITORE Wac");
		printer.print("PESO PERFORMER Wap");
		printer.print("PESO VALORE Wv");
		printer.print("EXP TITOLO Et");
		printer.print("EXP AUTORE Eaa");
		printer.print("EXP COMPOSITORE Eac");
		printer.print("EXP PERFORMER Eap");
		printer.print("EXP VALORE Ev");
		printer.print("METRICA TITOLO");
		printer.print("METRICA ARTISTA");
		printer.print("NORMA VALORE");
		KPIs.printHeader(printer);
		printer.println();		
	}

	private void print(CSVPrinter printer, ScoredConfiguration scoredConfiguration) throws IOException {
		printer.print(scoredConfiguration.score);
		printer.print(scoredConfiguration.properties.get("scoreCalculator.weight.title"));
		printer.print(scoredConfiguration.properties.get("scoreCalculator.weight.artist.author"));
		printer.print(scoredConfiguration.properties.get("scoreCalculator.weight.artist.composer"));
		printer.print(scoredConfiguration.properties.get("scoreCalculator.weight.artist.performer"));
		printer.print(scoredConfiguration.properties.get("scoreCalculator.weight.value"));
		printer.print(scoredConfiguration.properties.get("scoreCalculator.exponent.title"));
		printer.print(scoredConfiguration.properties.get("scoreCalculator.exponent.artist.author"));
		printer.print(scoredConfiguration.properties.get("scoreCalculator.exponent.artist.composer"));
		printer.print(scoredConfiguration.properties.get("scoreCalculator.exponent.artist.performer"));
		printer.print(scoredConfiguration.properties.get("scoreCalculator.exponent.value"));
		printer.print(scoredConfiguration.properties.get("scoreCalculator.metric.title"));
		printer.print(scoredConfiguration.properties.get("scoreCalculator.metric.artist"));
		printer.print(scoredConfiguration.properties.get("scoreCalculator.norm.value"));
		scoredConfiguration.scoreKPIs.print(printer);
		printer.println();		
	}
	
	private ScoredConfiguration runOptimizationStep(Properties configuration, List<File> files) throws FileNotFoundException, IOException {
		final int heartbeat = Integer.parseInt(configuration
				.getProperty("default.heartbeat", "1000"));
		
		final ScoreCalculator scoreCalculator = new ScoreCalculator(configuration, metrics, norms);
		final KPIs scoreKPIs = new KPIs();

		// process files
		final long startTimeMillis = System.currentTimeMillis();
		final AtomicInteger rownum = new AtomicInteger(0);
		for (File file : files) {
			logger.debug("parsing file {}", file);
			try (OutputForSiadaReader reader = new OutputForSiadaReader(new BufferedReader(new FileReader(file)), masterDatabase)) {
				while (reader.next()) {
					final Work verifiedWork = reader.getVerifiedWork();
					if (null == verifiedWork) {
						continue; // verified only
					}

					// re-score works
					final ArrayList<ScoreResult> scoreResults = new ArrayList<>();
					for (ScoreResult scoreResult : reader.getScoreResults()) {
						scoreResults.add(scoreCalculator.computeScore(scoreResult.utilization, scoreResult.work));
					}
					scoreResults.sort((ScoreResult a, ScoreResult b)->Double.compare(b.score, a.score));

					// update KPIs
					scoreKPIs.update(scoreResults, verifiedWork);
					
					// heartbeat
		        	if (0 == (rownum.incrementAndGet() % heartbeat)) {
						logger.debug("{}k", rownum.get() / 1000);
					}
				}
			}
		}
		logger.debug("{} parsed utilizations", rownum.get());			
		logger.debug("KPIs computed in {}s", (System.currentTimeMillis() - startTimeMillis) / 1000L);		

		// properties score
		final double score = scoreFromKPIs(scoreKPIs);

		// log
		logger.debug("score {}", score);
		logger.debug("properties\n{}", configuration);
		logger.debug("KPIs\n{}", scoreKPIs);

		return new ScoredConfiguration(configuration, scoreKPIs, score);
	}
	
}