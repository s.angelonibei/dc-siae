package com.alkemytech.sophia.recognizer;

import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.recognizer.db.MasterDatabase;
import com.alkemytech.sophia.recognizer.model.Artist;
import com.alkemytech.sophia.recognizer.model.ArtistRole;
import com.alkemytech.sophia.recognizer.model.Title;
import com.alkemytech.sophia.recognizer.model.Work;
import com.alkemytech.sophia.recognizer.regex.StringNormalizer;
import com.alkemytech.sophia.recognizer.siada.SiadaCode;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.zip.GZIPInputStream;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class ItacaJsonParser {

    private static final Logger logger = LoggerFactory.getLogger(ItacaJsonParser.class);
    static int exitCode = 0;

    protected static class GuiceModuleExtension extends GuiceModule {

        public GuiceModuleExtension(String[] args) {
            super(args);
        }

        @Override
        protected void configure() {
            super.configure();
            // singleton(s)
            bind(MasterDatabase.class).asEagerSingleton();
            bind(ItacaJsonParser.class).asEagerSingleton();
        }

    }

    public static void main(String[] args) {
        try {
            final ItacaJsonParser instance = Guice.createInjector(new GuiceModuleExtension(args))
                    .getInstance(ItacaJsonParser.class)
                    .startup();
            try {
                instance.process();
            } finally {
                instance.shutdown();
            }
        } catch (Throwable e) {
            logger.error("main", e);
            exitCode = 1;
        } finally {
            System.exit(exitCode);
        }
    }

    private final Properties configuration;
    private final MasterDatabase masterDatabase;
    private final Gson gson;

    @Inject
    protected ItacaJsonParser(@Named("configuration") Properties configuration,
                              MasterDatabase masterDatabase, Gson gson) {
        super();
        this.configuration = configuration;
        this.masterDatabase = masterDatabase;
        this.gson = gson;
    }

    public ItacaJsonParser startup() {
        if ("true".equalsIgnoreCase(configuration.getProperty("itacaJsonParser.locked", "true"))) {
            throw new IllegalStateException("application locked");
        }
        masterDatabase.startup();
        return this;
    }

    public ItacaJsonParser shutdown() {
        masterDatabase.shutdown();
        return this;
    }

    public ItacaJsonParser process() throws IOException {
        try {
            final long startTimeMillis = System.currentTimeMillis();
            final File inputFolder = new File(configuration.getProperty("itacaJsonParser.inputFolder"));
            final String tag = configuration.getProperty("itacaJsonParser.tag");
            final String codeField = configuration.getProperty("itacaJsonParser.field.code");
            final String[] titleFields = configuration.getProperty("itacaJsonParser.field.title").split(",");
            final String[] titlesFields = configuration.getProperty("itacaJsonParser.field.titles").split(",");
            final String authorField = configuration.getProperty("itacaJsonParser.field.author");
            final String composerField = configuration.getProperty("itacaJsonParser.field.composer");
            final String performerField = configuration.getProperty("itacaJsonParser.field.performer");
            final String elField = configuration.getProperty("itacaJsonParser.field.el");
            final String pdField = configuration.getProperty("itacaJsonParser.field.pd");
            final String scopeField = configuration.getProperty("itacaJsonParser.field.scope");
            final String valueField = configuration.getProperty("itacaJsonParser.field.value");
            final StringNormalizer titleNormalizer = new StringNormalizer(exitCode, configuration
                    .getProperty("itacaJsonParser.normalizer.title").split(","));
            final StringNormalizer artistNormalizer = new StringNormalizer(exitCode, configuration
                    .getProperty("itacaJsonParser.normalizer.artist").split(","));
            final boolean toUpperCase = "true".equalsIgnoreCase("itacaJsonParser.toUpperCase");
            final int heartbeat = Integer.parseInt(configuration
                    .getProperty("itacaJsonParser.heartbeat", configuration.getProperty("default.heartbeat", "1000")));
            final int bindPort = Integer.parseInt(configuration
                    .getProperty("itacaJsonParser.bindport", configuration.getProperty("default.bindport", "0")));

            // bind to lock tcp port
            try (ServerSocket socket = new ServerSocket(bindPort)) {
                // parse input folders
                logger.debug("importing json from {}", inputFolder);
                int rownum = 0;
                final ArrayList<File> folders = new ArrayList<>();
                for (File folder : inputFolder.listFiles()) {
                    if (folder.isDirectory()) {
                        folders.add(folder);
                    }
                }
                folders.sort((File a, File b) -> a.getName().compareTo(b.getName()));
                for (File folder : folders) {
                    logger.debug("processing folder {}", folder);
                    for (File file : folder.listFiles()) {

                        if (file.isFile() && file.exists() && file.getName().endsWith(".gz")) {
                            logger.debug("processing file {}", file);
                            try (JsonReader reader = new JsonReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(file))))) {

                                boolean isDelete = file.getName().toLowerCase().contains("delete");

                                if (!isDelete) {
                                    reader.beginArray();

                                    while (reader.hasNext()) {
                                        rownum = jsonProcess(reader, codeField, titleFields, titleNormalizer, toUpperCase, tag, titlesFields, authorField, artistNormalizer, composerField, performerField, elField, pdField, scopeField, valueField, rownum, heartbeat);
                                    }
                                    reader.endArray();

                                } else {
                                    rownum = jsonProcess(reader, codeField, titleFields, titleNormalizer, toUpperCase, tag, titlesFields, authorField, artistNormalizer, composerField, performerField, elField, pdField, scopeField, valueField, rownum, heartbeat);
                                }
                            }

                        }
                    }
                }
                logger.debug("{} records imported", rownum);
                logger.debug("import completed in {}s", (System.currentTimeMillis() - startTimeMillis) / 1000L);
            } catch (Exception e) {
                logger.error("ItacaJsonParser", e);
                exitCode = 1;
            }
        } catch (NullPointerException e) {
            logger.error("ItacaJsonParser", e);
            exitCode = 1;
        }
        return this;
    }

    public int jsonProcess(JsonReader reader, String codeField, String[] titleFields,
                            StringNormalizer titleNormalizer, boolean toUpperCase,
                            String tag, String[] titlesFields, String authorField,
                            StringNormalizer artistNormalizer, String composerField,
                            String performerField, String elField, String pdField,
                            String scopeField, String valueField, int rownum, int heartbeat) {

        final JsonObject jsonObject = gson.fromJson(reader, JsonObject.class);

        // codice opera
        String code = null;
        JsonElement jsonElement = jsonObject.get(codeField);
        if (null != jsonElement) {
            code = SiadaCode.normalize(jsonElement.getAsString());
        }
        if (null == code) {
            logger.warn("missing code: {}", jsonObject);
            return rownum;
        }

        // title
        Title title = null;
        for (String titleField : titleFields) {
            jsonElement = jsonObject.get(titleField);
            if (null != jsonElement) {
                String string = jsonElement.getAsString();
                string = titleNormalizer.normalize(toUpperCase ? string.toUpperCase() : string);
                if (!Strings.isNullOrEmpty(string)) {
                    title = new Title(string, tag);
                    break;
                }
            }
        }
        if (null == title) {
            logger.warn("missing title: {}", jsonObject);
            return rownum;
        }

        // title(s)
        final HashSet<Title> titles = new HashSet<>();
        for (String titlesField : titlesFields) {
            jsonElement = jsonObject.get(titlesField);
            if (null != jsonElement) {
                final Set<String> strings = new HashSet<>();
                if (jsonElement.isJsonArray()) {
                    for (JsonElement jsonArrayElement : jsonElement.getAsJsonArray()) {
                        strings.add(jsonArrayElement.getAsString());
                    }
                } else if (jsonElement.isJsonPrimitive()) {
                    strings.add(jsonElement.getAsString());
                }
                for (String string : strings) {
                    string = titleNormalizer.normalize(toUpperCase ? string.toUpperCase() : string);
                    if (!Strings.isNullOrEmpty(string)) {
                        titles.add(new Title(string, tag));
                    }
                }
            }
        }
//					        	if (titles.isEmpty()) {
//					        		logger.warn("missing titles: {}", jsonObject);
//					        		continue;
//					        	}

        // artist(s)
        final HashSet<Artist> artists = new HashSet<>();
        // author(s)
        jsonElement = jsonObject.get(authorField);
        if (null != jsonElement) {
            final Set<String> strings = new HashSet<>();
            if (jsonElement.isJsonArray()) {
                for (JsonElement jsonArrayElement : jsonElement.getAsJsonArray()) {
                    strings.add(jsonArrayElement.getAsString());
                }
            } else if (jsonElement.isJsonPrimitive()) {
                strings.add(jsonElement.getAsString());
            }
            for (String string : strings) {
                string = artistNormalizer.normalize(toUpperCase ? string.toUpperCase() : string);
                if (!Strings.isNullOrEmpty(string)) {
                    artists.add(new Artist(string, ArtistRole.AUTHOR, tag));
                }
            }
        }
        // composer(s)
        jsonElement = jsonObject.get(composerField);
        if (null != jsonElement) {
            final Set<String> strings = new HashSet<>();
            if (jsonElement.isJsonArray()) {
                for (JsonElement jsonArrayElement : jsonElement.getAsJsonArray()) {
                    strings.add(jsonArrayElement.getAsString());
                }
            } else if (jsonElement.isJsonPrimitive()) {
                strings.add(jsonElement.getAsString());
            }
            for (String string : strings) {
                string = artistNormalizer.normalize(toUpperCase ? string.toUpperCase() : string);
                if (!Strings.isNullOrEmpty(string)) {
                    artists.add(new Artist(string, ArtistRole.COMPOSER, tag));
                }
            }
        }
        // performer(s)
        jsonElement = jsonObject.get(performerField);
        if (null != jsonElement) {
            final Set<String> strings = new HashSet<>();
            if (jsonElement.isJsonArray()) {
                for (JsonElement jsonArrayElement : jsonElement.getAsJsonArray()) {
                    strings.add(jsonArrayElement.getAsString());
                }
            } else if (jsonElement.isJsonPrimitive()) {
                strings.add(jsonElement.getAsString());
            }
            for (String string : strings) {
                string = artistNormalizer.normalize(toUpperCase ? string.toUpperCase() : string);
                if (!Strings.isNullOrEmpty(string)) {
                    artists.add(new Artist(string, ArtistRole.PERFORMER, tag));
                }
            }

        }
        if (artists.isEmpty()) {
            logger.warn("missing artists: {}", jsonObject);
            return rownum;
        }

        // flag EL
        Boolean el = null;
        jsonElement = jsonObject.get(elField);
        if (null != jsonElement) {
            el = !"0".equals(jsonElement.getAsString());
        }

        // flag PD
        Boolean pd = null;
        jsonElement = jsonObject.get(pdField);
        if (null != jsonElement) {
            pd = !"0".equals(jsonElement.getAsString());
        }

        // scope
        String scope = null;
        jsonElement = jsonObject.get(scopeField);
        if (null != jsonElement) {
            scope = jsonElement.getAsString();
        }

        // scope
        Double value = null;
        jsonElement = jsonObject.get(valueField);
        if (null != jsonElement) {
            value = Double.parseDouble(jsonElement.getAsString());
        }

        // insert or update work
        final Work work = masterDatabase
                .insertOrReplace(new Work(code, title, titles, artists, el, pd, scope, value));
        if (null == work) {
            logger.warn("database error: {}", jsonObject);
        }

        // heartbeat
        if (0 == (++rownum) % heartbeat) {
            logger.debug("{}k", rownum / 1000);
        }
        return rownum;
    }
}