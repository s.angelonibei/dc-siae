package com.alkemytech.sophia.recognizer.kpi;

/**
 * An observer for some variable sampling used to compute statistics.
 * <br/>
 * Variance and standard-deviation approximated values are computed using one-pass online algorithm.
 * 
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class VariableObserver {

	private int n;
	private double max;
	private double min;
	private double sum;
	private double sumsq;
	
	public VariableObserver() {
		reset();
	}

	public VariableObserver reset() {
		n = 0;
		max = Double.NEGATIVE_INFINITY;
		min = Double.POSITIVE_INFINITY;
		sum = 0.0;
		sumsq = 0.0;
		return this;
	}
	
	public VariableObserver update(double x) {
		n ++;
		min = Math.min(min, x);
		max = Math.max(max, x);
		sum += x;
		sumsq += x * x;
		return this;
	}
	
	public double min() {
		if (0 == n) {
			return 0.0;
		}
		return min;
	}
	
	public double max() {
		if (0 == n) {
			return 0.0;
		}
		return max;
	}

	public double sum() {
		return sum;
	}

	public double average() {
		if (0 == n) {
			return 0.0;
		}
		return sum / n;
	}

	public double variance() {
		if (0 == n) {
			return 0.0;
		}
		double mean = sum / n;
		return sumsq / n - mean * mean;
//		double v = sumsq - (sum * sum) / n;
//		return v / n;
	}
	
	public double deviation() {
		return Math.sqrt(variance());
	}
	
	public int samples() {
		return n;
	}

}
