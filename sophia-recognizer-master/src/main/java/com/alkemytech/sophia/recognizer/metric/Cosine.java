package com.alkemytech.sophia.recognizer.metric;

import java.util.Map;

import com.alkemytech.sophia.recognizer.util.Str;

/**
 * The similarity between the two strings is the cosine of the angle between
 * their vector representations. Each input string is converted into a set of
 * n-grams, the Cosine similarity is then computed as V1 . V2 / (|V1| * |V2|).
 * 
 * @see https://blog.nishtahir.com/2015/09/19/fuzzy-string-matching-using-cosine-similarity/
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class Cosine extends NGrams {
	
	private static double dot(Map<String, Integer> a, Map<String, Integer> b) {
		final Map<String, Integer> small;
		final Map<String, Integer> large;
        if (a.size() < b.size()) {
            small = a;
            large = b;
        } else {
            small = b;
            large = a;
        }
        double sum = 0.0;
        for (Map.Entry<String, Integer> entry : small.entrySet()) {
        	final Integer count = large.get(entry.getKey());
            if (null != count) {
                sum += (double) entry.getValue() * (double) count;
            }
        }
        return sum;
	}
	
	private static double norm(Map<String, Integer> ngrams) {
		double sum = 0;
        for (Map.Entry<String, Integer> entry : ngrams.entrySet()) {
            sum += (double) entry.getValue() * (double) entry.getValue();
        }
        return Math.sqrt(sum);
	}

	public static double cosine(String a, String b, int n) {
		if (a.equals(b)) {
            return 1.0;
        } else if (a.length() < n || b.length() < n) {
            return 0.0;
        }
		Map<String, Integer> aa = ngrams(a, n);
        Map<String, Integer> bb = ngrams(b, n);
        return dot(aa, bb) / (norm(aa) * norm(bb));
	}
	
	public static class Unigrams implements Meter {

		@Override
		public double measure(String a, String b) {
			return cosine(a, b, 1);
		}
		
	}
	
	public static class Bigrams implements Meter {
		
		@Override
		public double measure(String a, String b) {
			a = Str.lpad(a, 2, '\n');
			b = Str.lpad(b, 2, '\n');
			return cosine(a, b, 2);
		}

	}
	
	public static class Trigrams implements Meter {
		
		@Override
		public double measure(String a, String b) {
			a = Str.lpad(a, 3, '\n');
			b = Str.lpad(b, 3, '\n');
			return cosine(a, b, 3);
		}
		
	}

}
