package com.alkemytech.sophia.recognizer.metric;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.alkemytech.sophia.recognizer.util.Str;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
@Singleton
public class Metrics {
	
	/**
	 * Inside String
	 */
	private class InStr implements Metric {

		@Override
		public double similarity(String a, String b) {
			return b.contains(a) || a.contains(b) ? 1.0 : 0.0;
		}

		@Override
		public String getName() {
			return "InStr";
		}
		
	}
	
	/**
	 * No-spaces Levenshtein Distance
	 */
	private class NoSpLD implements Metric {

		private final Meter meter = new Levenshtein();

		@Override
		public double similarity(String a, String b) {
			a = Str.az09(a);
			b = Str.az09(b);
			final double length = Math.max(a.length(), b.length());
			return (length - meter.measure(a, b)) / length;
		}

		@Override
		public String getName() {
			return "NoSpLD";
		}
		
	};
	
	/**
	 * No-spaces Sorted-words Levenshtein Distance
	 */
	private class NoSpAzLD implements Metric {

		private final Meter meter = new Levenshtein();

		@Override
		public double similarity(String a, String b) {
			a = new String(Str.sortedAz09Chars(a));
			b = new String(Str.sortedAz09Chars(b));
			final double length = Math.max(a.length(), b.length());
			return (length - meter.measure(a, b)) / length;
		}

		@Override
		public String getName() {
			return "NoSpAzLD";
		}
		
	};
	
	/**
	 * Word-wise Levenshtein Distance (Sum of Minima)
	 */
	private class WwLD implements Metric {

		private final Meter meter = new Levenshtein();
		
		@Override
		public double similarity(String a, String b) {
			final String[] aa = a.split("\\s");
			final String[] bb = b.split("\\s");
			double measure = 0.0;
			for (String aw : aa) {
				double length = Double.NEGATIVE_INFINITY;
				double minimum = Double.POSITIVE_INFINITY;
				for (String bw : bb) {
					final double awbw = meter.measure(aw, bw);
					if (awbw < minimum) {
						minimum = awbw;
						length = bw.length();
					}
				}
				length = Math.max(length, aw.length());
				measure += (length - minimum) / length;
			}
			return measure / (double) aa.length;
		}

		@Override
		public String getName() {
			return "WwLD";
		}
		
	};
	
	/**
	 * Minimum of Word-wise and No-Spaces Levenshtein Distance
	 */
	private class MaxWwNoSpLD implements Metric {
		
		@Override
		public double similarity(String a, String b) {
			return Math.max(getInstance("WwLD").similarity(a, b),
					getInstance("NoSpLD").similarity(a, b));
		}

		@Override
		public String getName() {
			return "MaxWwNoSpLD";
		}

	};
	
	/**
	 * Combined Word-wise and No-Spaces Levenshtein Distance
	 */
	private class CombWwNoSpLD implements Metric {
		
		@Override
		public double similarity(String a, String b) {
			return 0.5 * (getInstance("WwLD").similarity(a, b) +
					getInstance("NoSpLD").similarity(a, b));
		}
		
		@Override
		public String getName() {
			return "CombWwNoSpLD";
		}

	};
	
	/**
	 * All Levenshtein Distances Combined
	 */
	private class CombAllLD implements Metric {
		
		@Override
		public double similarity(String a, String b) {
			return (getInstance("NoSpLD").similarity(a, b) +
					getInstance("NoSpAzLD").similarity(a, b) +
					getInstance("WwLD").similarity(a, b)) / 3;
		}
		
		@Override
		public String getName() {
			return "CombAllLD";
		}

	};
	
	/**
	 * No-Spaces Damerau-Levenshtein Distance
	 */
	private class NoSpDLD implements Metric {

		private final Meter meter = new DamerauLevenshtein();

		@Override
		public double similarity(String a, String b) {
			a = Str.az09(a);
			b = Str.az09(b);
			final double length = Math.max(a.length(), b.length());
			return (length - meter.measure(a, b)) / length;
		}

		@Override
		public String getName() {
			return "NoSpDLD";
		}
		
	};
	
	/**
	 * No-Spaces Sorted-Words Damerau-Levenshtein Distance
	 */
	private class NoSpAzDLD implements Metric {

		private final Meter meter = new DamerauLevenshtein();

		@Override
		public double similarity(String a, String b) {
			a = new String(Str.sortedAz09Chars(a));
			b = new String(Str.sortedAz09Chars(b));
			final double length = Math.max(a.length(), b.length());
			return (length - meter.measure(a, b)) / length;
		}

		@Override
		public String getName() {
			return "NoSpAzDLD";
		}
		
	};
	
	/**
	 * Word-wise Damerau-Levenshtein Distance (Sum of Minima)
	 */
	private class WwDLD implements Metric {

		private final Meter meter = new DamerauLevenshtein();
		
		@Override
		public double similarity(String a, String b) {
			final String[] aa = a.split("\\s");
			final String[] bb = b.split("\\s");
			double measure = 0.0;
			for (String aw : aa) {
				double length = Double.NEGATIVE_INFINITY;
				double minimum = Double.POSITIVE_INFINITY;
				for (String bw : bb) {
					final double awbw = meter.measure(aw, bw);
					if (awbw < minimum) {
						minimum = awbw;
						length = bw.length();
					}
				}
				length = Math.max(length, aw.length());
				measure += (length - minimum) / length;
			}
			return measure / (double) aa.length;
		}

		@Override
		public String getName() {
			return "WwDLD";
		}
		
	};
	
	/**
	 * Minimum of Word-wise and No-Spaces Damerau-Levenshtein Distance
	 */
	private class MaxWwNoSpDLD implements Metric {
		
		@Override
		public double similarity(String a, String b) {
			return Math.max(getInstance("WwDLD").similarity(a, b),
					getInstance("NoSpDLD").similarity(a, b));
		}

		@Override
		public String getName() {
			return "MaxWwNoSpDLD";
		}

	};
	
	/**
	 * Combined Word-wise and No-Spaces Damerau-Levenshtein Distance
	 */
	private class CombWwNoSpDLD implements Metric {
		
		@Override
		public double similarity(String a, String b) {
			return 0.5 * (getInstance("WwDLD").similarity(a, b) + getInstance("NoSpDLD").similarity(a, b));
		}
		
		@Override
		public String getName() {
			return "CombWwNoSpDLD";
		}

	};
	
	/**
	 * All Damerau-Levenshtein Distances Combined
	 */
	private class CombAllDLD implements Metric {
		
		@Override
		public double similarity(String a, String b) {
			return (getInstance("NoSpDLD").similarity(a, b) +
					getInstance("NoSpAzDLD").similarity(a, b) +
					getInstance("WwDLD").similarity(a, b)) / 3;
		}
		
		@Override
		public String getName() {
			return "CombAllDLD";
		}

	};
	
	/**
	 * No-spaces Longest Common Substring
	 */
	private class NoSpLCS implements Metric {

		private final Meter meter = new LongestCommonSubstring();
		
		@Override
		public double similarity(String a, String b) {
			a = Str.az09(a);
			b = Str.az09(b);
			final double length = Math.max(a.length(), b.length());
			return meter.measure(a, b) / length;
		}
		
		@Override
		public String getName() {
			return "NoSpLCS";
		}
		
	};
	
	/**
	 * No-spaces Sorted-Words Longest Common Substring
	 */
	private class NoSpAzLCS implements Metric {

		private final Meter meter = new LongestCommonSubstring();
		
		@Override
		public double similarity(String a, String b) {
			a = new String(Str.sortedAz09Chars(a));
			b = new String(Str.sortedAz09Chars(b));
			final double length = Math.max(a.length(), b.length());
			return meter.measure(a, b) / length;
		}
		
		@Override
		public String getName() {
			return "NoSpAzLCS";
		}
		
	};
	
	/**
	 * Word-wise Longest Common Substring (Sum of Maxima)
	 */
	private class WwLCS implements Metric {

		private final Meter meter = new LongestCommonSubstring();

		@Override
		public double similarity(String a, String b) {
			final String[] aa = a.split("\\s");
			final String[] bb = b.split("\\s");
			double measure = 0.0;
			for (String aw : aa) {
				double length = Double.NEGATIVE_INFINITY;
				double maximum = Double.NEGATIVE_INFINITY;
				for (String bw : bb) {
					final double awbw = meter.measure(aw, bw);
					if (awbw > maximum) {
						maximum = awbw;
						length = bw.length();
					}
				}
				length = Math.max(length, aw.length());
				measure += maximum / length;
			}
			return measure / (double) aa.length;
		}

		@Override
		public String getName() {
			return "WwLCS";
		}

	};
	
	/**
	 * Maximum of Word-wise and No-Spaces Longest Common Substring
	 */
	private class MaxWwNoSpLCS implements Metric {
		
		@Override
		public double similarity(String a, String b) {
			return Math.max(getInstance("WwLCS").similarity(a, b),
					getInstance("NoSpLCS").similarity(a, b));
		}

		@Override
		public String getName() {
			return "MaxWwNoSpLCS";
		}
		
	};
	
	/**
	 * Combined Word-wise and No-Spaces Longest Common Substring
	 */
	private class CombWwNoSpLCS implements Metric {
		
		@Override
		public double similarity(String a, String b) {
			return 0.5 * (getInstance("WwLCS").similarity(a, b) +
					getInstance("NoSpLCS").similarity(a, b));
		}

		@Override
		public String getName() {
			return "CombWwNoSpLCS";
		}

	};
	
	/**
	 * All Longest Common Substring Combined
	 */
	private class CombAllLCS implements Metric {
		
		@Override
		public double similarity(String a, String b) {
			return (getInstance("NoSpLCS").similarity(a, b) +
					getInstance("NoSpAzLCS").similarity(a, b) +
					getInstance("WwLCS").similarity(a, b)) / 3;
		}
		
		@Override
		public String getName() {
			return "CombAllLCS";
		}

	};
	
	/**
	 * No-Spaces Sørensen–Dice Coefficient
	 */
	private class NoSpSDC implements Metric {

		private final Meter meter = new SorensenDiceCoefficient();
		
		@Override
		public double similarity(String a, String b) {
			return meter.measure(Str.az09(a), Str.az09(b));
		}

		@Override
		public String getName() {
			return "NoSpSDC";
		}

	};

	/**
	 * No-Spaces Sørensen–Dice Coefficient
	 */
	private class NoSpAzSDC implements Metric {

		private final Meter meter = new SorensenDiceCoefficient();
		
		@Override
		public double similarity(String a, String b) {
			a = new String(Str.sortedAz09Chars(a));
			b = new String(Str.sortedAz09Chars(b));
			return meter.measure(a, b);
		}

		@Override
		public String getName() {
			return "NoSpAzSDC";
		}

	};

	/**
	 * Average Word-wise Sørensen–Dice Coefficient
	 */
	private class AvgWwSDC implements Metric {

		private final Meter meter = new SorensenDiceCoefficient();
		
		@Override
		public double similarity(String a, String b) {
			double measure = 0.0;
			int words = 0;
			final String[] aa = a.split("\\s");
			final String[] bb = b.split("\\s");
			for (String aw : aa) {
				double maximum = 0.0;
				for (String bw : bb) {
					maximum = Math.max(maximum, meter.measure(aw, bw));
				}	
				measure += maximum;
				words ++;
			}
			return measure / words; 
		}

		@Override
		public String getName() {
			return "AvgWwSDC";
		}

	};
	
	/**
	 * Maximum Word-wise Sørensen–Dice Coefficient
	 */
	private class MaxWwSDC implements Metric {

		private final Meter meter = new SorensenDiceCoefficient();
		
		@Override
		public double similarity(String a, String b) {
			final String[] aa = a.split("\\s");
			final String[] bb = b.split("\\s");
			double measure = 0.0;
			for (String aw : aa) {
				for (String bw : bb) {
					measure = Math.max(measure, meter.measure(aw, bw));
				}	
			}
			return measure; 
		}

		@Override
		public String getName() {
			return "MaxWwSDC";
		}

	};

	/**
	 * Maximum of word-wise and no-spaces Sørensen–Dice Coefficient
	 */
	private class MaxAvgWwNoSpSDC implements Metric {
		
		@Override
		public double similarity(String a, String b) {
			return Math.max(getInstance("AvgWwSDC").similarity(a, b),
					getInstance("NoSpSDC").similarity(a, b));
		}

		@Override
		public String getName() {
			return "MaxAvgWwNoSpSDC";
		}

	};
	
	/**
	 * Combined word-wise and no-spaces Sørensen–Dice Coefficient
	 */
	private class CombAvgWwNoSpSDC implements Metric {

		@Override
		public double similarity(String a, String b) {
			return 0.5 * (getInstance("AvgWwSDC").similarity(a, b) +
					getInstance("NoSpSDC").similarity(a, b));
		}

		@Override
		public String getName() {
			return "CombAvgWwNoSpSDC";
		}

	};
	
	/**
	 * All Sørensen–Dice Coefficient Combined
	 */
	private class CombAllSDC implements Metric {
		
		@Override
		public double similarity(String a, String b) {
			return (getInstance("NoSpSDC").similarity(a, b) +
					getInstance("NoSpAzSDC").similarity(a, b) +
					getInstance("AvgWwSDC").similarity(a, b) +
					getInstance("MaxWwSDC").similarity(a, b)) * 0.25;
		}
		
		@Override
		public String getName() {
			return "CombAllSDC";
		}

	};
	
	/**
	 * No-spaces Unigrams Cosine Similarity
	 */
	private class NoSpCOS1 implements Metric {

		private final Meter meter = new Cosine.Unigrams();

		@Override
		public double similarity(String a, String b) {
			a = Str.az09(a);
			b = Str.az09(b);
			return meter.measure(a, b);
		}

		@Override
		public String getName() {
			return "NoSpCOS1";
		}
		
	};
	
	/**
	 * Average Word-wise Unigrams Cosine Similarity
	 */
	private class AvgWwCOS1 implements Metric {

		private final Meter meter = new Cosine.Unigrams();
		
		@Override
		public double similarity(String a, String b) {
			double measure = 0.0;
			int words = 0;
			final String[] aa = a.split("\\s");
			final String[] bb = b.split("\\s");
			for (String aw : aa) {
				double maximum = 0.0;
				for (String bw : bb) {
					maximum = Math.max(maximum, meter.measure(aw, bw));
				}	
				measure += maximum;
				words ++;
			}
			return measure / words; 
		}

		@Override
		public String getName() {
			return "AvgWwCOS1";
		}

	};
	
	/**
	 * Maximum Word-wise Unigrams Cosine Similarity
	 */
	private class MaxWwCOS1 implements Metric {

		private final Meter meter = new Cosine.Unigrams();
		
		@Override
		public double similarity(String a, String b) {
			final String[] aa = a.split("\\s");
			final String[] bb = b.split("\\s");
			double measure = 0.0;
			for (String aw : aa) {
				for (String bw : bb) {
					measure = Math.max(measure, meter.measure(aw, bw));
				}	
			}
			return measure; 
		}

		@Override
		public String getName() {
			return "MaxWwCOS1";
		}

	};
	
	/**
	 * All Unigrams Cosine Similarities Combined
	 */
	private class CombAllCOS1 implements Metric {
		
		@Override
		public double similarity(String a, String b) {
			return (getInstance("NoSpCOS1").similarity(a, b) +
					getInstance("AvgWwCOS1").similarity(a, b) +
					getInstance("MaxWwCOS1").similarity(a, b)) / 3;
		}
		
		@Override
		public String getName() {
			return "CombAllCOS1";
		}

	};
	
	/**
	 * No-spaces Bigrams Cosine Similarity
	 */
	private class NoSpCOS2 implements Metric {

		private final Meter meter = new Cosine.Bigrams();

		@Override
		public double similarity(String a, String b) {
			a = Str.az09(a);
			b = Str.az09(b);
			return meter.measure(a, b);
		}

		@Override
		public String getName() {
			return "NoSpCOS2";
		}
		
	};
	
	/**
	 * No-Spaces Bigrams Cosine Similarity
	 */
	private class NoSpAzCOS2 implements Metric {

		private final Meter meter = new Cosine.Bigrams();
		
		@Override
		public double similarity(String a, String b) {
			a = new String(Str.sortedAz09Chars(a));
			b = new String(Str.sortedAz09Chars(b));
			return meter.measure(a, b);
		}

		@Override
		public String getName() {
			return "NoSpAzCOS2";
		}

	};

	/**
	 * Average Word-wise Bigrams Cosine Similarity
	 */
	private class AvgWwCOS2 implements Metric {

		private final Meter meter = new Cosine.Bigrams();
		
		@Override
		public double similarity(String a, String b) {
			double measure = 0.0;
			int words = 0;
			final String[] aa = a.split("\\s");
			final String[] bb = b.split("\\s");
			for (String aw : aa) {
				double maximum = 0.0;
				for (String bw : bb) {
					maximum = Math.max(maximum, meter.measure(aw, bw));
				}	
				measure += maximum;
				words ++;
			}
			return measure / words; 
		}

		@Override
		public String getName() {
			return "AvgWwCOS2";
		}

	};
	
	/**
	 * Maximum Word-wise Bigrams Cosine Similarity
	 */
	private class MaxWwCOS2 implements Metric {

		private final Meter meter = new Cosine.Bigrams();
		
		@Override
		public double similarity(String a, String b) {
			final String[] aa = a.split("\\s");
			final String[] bb = b.split("\\s");
			double measure = 0.0;
			for (String aw : aa) {
				for (String bw : bb) {
					measure = Math.max(measure, meter.measure(aw, bw));
				}	
			}
			return measure; 
		}

		@Override
		public String getName() {
			return "MaxWwCOS2";
		}

	};
	
	/**
	 * All Bigrams Cosine Similarities Combined
	 */
	private class CombAllCOS2 implements Metric {
		
		@Override
		public double similarity(String a, String b) {
			return (getInstance("NoSpCOS2").similarity(a, b) +
					getInstance("NoSpAzCOS2").similarity(a, b) +
					getInstance("AvgWwCOS2").similarity(a, b) +
					getInstance("MaxWwCOS2").similarity(a, b)) * 0.25;
		}
		
		@Override
		public String getName() {
			return "CombAllCOS2";
		}

	};
	
	/**
	 * No-spaces Trigrams Cosine Similarity
	 */
	private class NoSpCOS3 implements Metric {

		private final Meter meter = new Cosine.Trigrams();

		@Override
		public double similarity(String a, String b) {
			a = Str.az09(a);
			b = Str.az09(b);
			return meter.measure(a, b);
		}

		@Override
		public String getName() {
			return "NoSpCOS3";
		}
		
	};
	
	/**
	 * No-Spaces Trigrams Cosine Similarity
	 */
	private class NoSpAzCOS3 implements Metric {

		private final Meter meter = new Cosine.Trigrams();
		
		@Override
		public double similarity(String a, String b) {
			a = new String(Str.sortedAz09Chars(a));
			b = new String(Str.sortedAz09Chars(b));
			return meter.measure(a, b);
		}

		@Override
		public String getName() {
			return "NoSpAzCOS3";
		}

	};

	/**
	 * Average Word-wise Trigrams Cosine Similarity
	 */
	private class AvgWwCOS3 implements Metric {

		private final Meter meter = new Cosine.Trigrams();
		
		@Override
		public double similarity(String a, String b) {
			double measure = 0.0;
			int words = 0;
			final String[] aa = a.split("\\s");
			final String[] bb = b.split("\\s");
			for (String aw : aa) {
				double maximum = 0.0;
				for (String bw : bb) {
					maximum = Math.max(maximum, meter.measure(aw, bw));
				}	
				measure += maximum;
				words ++;
			}
			return measure / words; 
		}

		@Override
		public String getName() {
			return "AvgWwCOS3";
		}

	};
	
	/**
	 * Maximum Word-wise Trigrams Cosine Similarity
	 */
	private class MaxWwCOS3 implements Metric {

		private final Meter meter = new Cosine.Trigrams();
		
		@Override
		public double similarity(String a, String b) {
			final String[] aa = a.split("\\s");
			final String[] bb = b.split("\\s");
			double measure = 0.0;
			for (String aw : aa) {
				for (String bw : bb) {
					measure = Math.max(measure, meter.measure(aw, bw));
				}	
			}
			return measure; 
		}

		@Override
		public String getName() {
			return "MaxWwCOS3";
		}

	};
	
	/**
	 * All Trigrams Cosine Similarities Combined
	 */
	private class CombAllCOS3 implements Metric {
		
		@Override
		public double similarity(String a, String b) {
			return (getInstance("NoSpCOS3").similarity(a, b) +
					getInstance("NoSpAzCOS3").similarity(a, b) +
					getInstance("AvgWwCOS3").similarity(a, b) +
					getInstance("MaxWwCOS3").similarity(a, b)) * 0.25;
		}
		
		@Override
		public String getName() {
			return "CombAllCOS3";
		}

	};
	
	/**
	 * Maximum of all no-spaces Cosine Similarities
	 */
	private class MaxNoSpCOS implements Metric {
		
		@Override
		public double similarity(String a, String b) {
			return Math.max(Math.max(getInstance("NoSpCOS1").similarity(a, b),
					getInstance("NoSpCOS2").similarity(a, b)),
					getInstance("NoSpCOS3").similarity(a, b));
		}
		
		@Override
		public String getName() {
			return "MaxNoSpCOS";
		}

	};
	
	/**
	 * No-spaces Unigrams Jaccard Similarity
	 */
	private class NoSpJCD1 implements Metric {

		private final Meter meter = new Jaccard.Unigrams();

		@Override
		public double similarity(String a, String b) {
			a = Str.az09(a);
			b = Str.az09(b);
			return meter.measure(a, b);
		}

		@Override
		public String getName() {
			return "NoSpJCD1";
		}
		
	};
	
	/**
	 * Average Word-wise Unigrams Jaccard Similarity
	 */
	private class AvgWwJCD1 implements Metric {

		private final Meter meter = new Jaccard.Unigrams();
		
		@Override
		public double similarity(String a, String b) {
			double measure = 0.0;
			int words = 0;
			final String[] aa = a.split("\\s");
			final String[] bb = b.split("\\s");
			for (String aw : aa) {
				double maximum = 0.0;
				for (String bw : bb) {
					maximum = Math.max(maximum, meter.measure(aw, bw));
				}	
				measure += maximum;
				words ++;
			}
			return measure / words; 
		}

		@Override
		public String getName() {
			return "AvgWwJCD1";
		}

	};
	
	/**
	 * Maximum Word-wise Unigrams Jaccard Similarity
	 */
	private class MaxWwJCD1 implements Metric {

		private final Meter meter = new Jaccard.Unigrams();
		
		@Override
		public double similarity(String a, String b) {
			final String[] aa = a.split("\\s");
			final String[] bb = b.split("\\s");
			double measure = 0.0;
			for (String aw : aa) {
				for (String bw : bb) {
					measure = Math.max(measure, meter.measure(aw, bw));
				}	
			}
			return measure; 
		}

		@Override
		public String getName() {
			return "MaxWwJCD1";
		}

	};
	
	/**
	 * All Unigrams Jaccard Similarities Combined
	 */
	private class CombAllJCD1 implements Metric {
		
		@Override
		public double similarity(String a, String b) {
			return (getInstance("NoSpJCD1").similarity(a, b) +
					getInstance("AvgWwJCD1").similarity(a, b) +
					getInstance("MaxWwJCD1").similarity(a, b)) / 3;
		}
		
		@Override
		public String getName() {
			return "CombAllJCD1";
		}

	};
	
	/**
	 * No-spaces Bigrams Jaccard Similarity
	 */
	private class NoSpJCD2 implements Metric {

		private final Meter meter = new Jaccard.Bigrams();

		@Override
		public double similarity(String a, String b) {
			a = Str.az09(a);
			b = Str.az09(b);
			return meter.measure(a, b);
		}

		@Override
		public String getName() {
			return "NoSpJCD2";
		}
		
	};
	
	/**
	 * No-Spaces Bigrams Jaccard Similarity
	 */
	private class NoSpAzJCD2 implements Metric {

		private final Meter meter = new Jaccard.Bigrams();
		
		@Override
		public double similarity(String a, String b) {
			a = new String(Str.sortedAz09Chars(a));
			b = new String(Str.sortedAz09Chars(b));
			return meter.measure(a, b);
		}

		@Override
		public String getName() {
			return "NoSpAzJCD2";
		}

	};

	/**
	 * Average Word-wise Bigrams Jaccard Similarity
	 */
	private class AvgWwJCD2 implements Metric {

		private final Meter meter = new Jaccard.Bigrams();
		
		@Override
		public double similarity(String a, String b) {
			double measure = 0.0;
			int words = 0;
			final String[] aa = a.split("\\s");
			final String[] bb = b.split("\\s");
			for (String aw : aa) {
				double maximum = 0.0;
				for (String bw : bb) {
					maximum = Math.max(maximum, meter.measure(aw, bw));
				}	
				measure += maximum;
				words ++;
			}
			return measure / words; 
		}

		@Override
		public String getName() {
			return "AvgWwJCD2";
		}

	};
	
	/**
	 * Maximum Word-wise Bigrams Jaccard Similarity
	 */
	private class MaxWwJCD2 implements Metric {

		private final Meter meter = new Jaccard.Bigrams();
		
		@Override
		public double similarity(String a, String b) {
			final String[] aa = a.split("\\s");
			final String[] bb = b.split("\\s");
			double measure = 0.0;
			for (String aw : aa) {
				for (String bw : bb) {
					measure = Math.max(measure, meter.measure(aw, bw));
				}	
			}
			return measure; 
		}

		@Override
		public String getName() {
			return "MaxWwJCD2";
		}

	};
	
	/**
	 * All Bigrams Jaccard Similarities Combined
	 */
	private class CombAllJCD2 implements Metric {
		
		@Override
		public double similarity(String a, String b) {
			return (getInstance("NoSpJCD2").similarity(a, b) +
					getInstance("NoSpAzJCD2").similarity(a, b) +
					getInstance("AvgWwJCD2").similarity(a, b) +
					getInstance("MaxWwJCD2").similarity(a, b)) * 0.25;
		}
		
		@Override
		public String getName() {
			return "CombAllJCD2";
		}

	};
	
	/**
	 * No-spaces Trigrams Jaccard Similarity
	 */
	private class NoSpJCD3 implements Metric {

		private final Meter meter = new Jaccard.Trigrams();

		@Override
		public double similarity(String a, String b) {
			a = Str.az09(a);
			b = Str.az09(b);
			return meter.measure(a, b);
		}

		@Override
		public String getName() {
			return "NoSpJCD3";
		}
		
	};
	
	/**
	 * No-Spaces Trigrams Jaccard Similarity
	 */
	private class NoSpAzJCD3 implements Metric {

		private final Meter meter = new Jaccard.Trigrams();
		
		@Override
		public double similarity(String a, String b) {
			a = new String(Str.sortedAz09Chars(a));
			b = new String(Str.sortedAz09Chars(b));
			return meter.measure(a, b);
		}

		@Override
		public String getName() {
			return "NoSpAzJCD3";
		}

	};

	/**
	 * Average Word-wise Trigrams Jaccard Similarity
	 */
	private class AvgWwJCD3 implements Metric {

		private final Meter meter = new Jaccard.Trigrams();
		
		@Override
		public double similarity(String a, String b) {
			double measure = 0.0;
			int words = 0;
			final String[] aa = a.split("\\s");
			final String[] bb = b.split("\\s");
			for (String aw : aa) {
				double maximum = 0.0;
				for (String bw : bb) {
					maximum = Math.max(maximum, meter.measure(aw, bw));
				}	
				measure += maximum;
				words ++;
			}
			return measure / words; 
		}

		@Override
		public String getName() {
			return "AvgWwJCD3";
		}

	};
	
	/**
	 * Maximum Word-wise Trigrams Jaccard Similarity
	 */
	private class MaxWwJCD3 implements Metric {

		private final Meter meter = new Jaccard.Trigrams();
		
		@Override
		public double similarity(String a, String b) {
			final String[] aa = a.split("\\s");
			final String[] bb = b.split("\\s");
			double measure = 0.0;
			for (String aw : aa) {
				for (String bw : bb) {
					measure = Math.max(measure, meter.measure(aw, bw));
				}	
			}
			return measure; 
		}

		@Override
		public String getName() {
			return "MaxWwJCD3";
		}

	};
	
	/**
	 * All Trigrams Jaccard Similarities Combined
	 */
	private class CombAllJCD3 implements Metric {
		
		@Override
		public double similarity(String a, String b) {
			return (getInstance("NoSpJCD3").similarity(a, b) +
					getInstance("NoSpAzJCD3").similarity(a, b) +
					getInstance("AvgWwJCD3").similarity(a, b) +
					getInstance("MaxWwJCD3").similarity(a, b)) * 0.25;
		}
		
		@Override
		public String getName() {
			return "CombAllJCD3";
		}

	};
	
	/**
	 * Maximum of all no-spaces Jaccard Similarities
	 */
	private class MaxNoSpJCD implements Metric {
		
		@Override
		public double similarity(String a, String b) {
			return Math.max(Math.max(getInstance("NoSpJCD1").similarity(a, b),
					getInstance("NoSpJCD2").similarity(a, b)),
					getInstance("NoSpJCD3").similarity(a, b));
		}
		
		@Override
		public String getName() {
			return "MaxNoSpJCD";
		}

	};
	
	private class TitleEx implements Metric {
		
		@Override
		public double similarity(String a, String b) {
			return 0.25 * getInstance("WwLD").similarity(a, b) +
					0.75 * Math.max(getInstance("NoSpLD").similarity(a, b), getInstance("NoSpAzLD").similarity(a, b));
		}

		@Override
		public String getName() {
			return "TitleEx";
		}

	};
	
	private class ArtistEx implements Metric {
		
		@Override
		public double similarity(String a, String b) {
			return 0.333 * Math.max(Math.max(getInstance("WwLD").similarity(a, b), getInstance("WwLCS").similarity(a, b)), getInstance("MaxWwSDC").similarity(a, b)) +
				0.667 * Math.max(Math.max(getInstance("NoSpAzLD").similarity(a, b), getInstance("NoSpAzLCS").similarity(a, b)), getInstance("NoSpAzSDC").similarity(a, b));
		}

		@Override
		public String getName() {
			return "ArtistEx";
		}

	};

	private class TitleEx2 implements Metric {
		
		@Override
		public double similarity(String a, String b) {
			return 0.25 * getInstance("WwLD").similarity(a, b) +
					0.75 * Math.max(Math.max(getInstance("NoSpLD").similarity(a, b), getInstance("NoSpAzLD").similarity(a, b)), getInstance("NoSpCOS2").similarity(a, b));
		}

		@Override
		public String getName() {
			return "TitleEx2";
		}

	};
	
	private class ArtistEx2 implements Metric {
		
		@Override
		public double similarity(String a, String b) {
			return 0.333 * Math.max(Math.max(Math.max(getInstance("WwLD").similarity(a, b),
					getInstance("WwLCS").similarity(a, b)),
					getInstance("MaxWwSDC").similarity(a, b)),
					getInstance("MaxWwCOS2").similarity(a, b)) +
				0.667 * Math.max(Math.max(Math.max(getInstance("NoSpAzLD").similarity(a, b),
						getInstance("NoSpAzLCS").similarity(a, b)),
						getInstance("NoSpAzSDC").similarity(a, b)), 
						getInstance("NoSpCOS2").similarity(a, b));
		}

		@Override
		public String getName() {
			return "ArtistEx2";
		}

	};

	private class TitleEx3 implements Metric {
		
		@Override
		public double similarity(String a, String b) {
			return 0.333 * getInstance("WwLD").similarity(a, b) +
					0.667 * getInstance("NoSpLD").similarity(a, b);
		}

		@Override
		public String getName() {
			return "TitleEx3";
		}

	};
	
	private class ArtistEx3 implements Metric {
		
		@Override
		public double similarity(String a, String b) {
			return 0.667 * Math.max(getInstance("WwLD").similarity(a, b), getInstance("MaxWwCOS2").similarity(a, b)) +
				0.333 * Math.max(getInstance("NoSpLD").similarity(a, b), getInstance("NoSpCOS2").similarity(a, b));
		}

		@Override
		public String getName() {
			return "ArtistEx3";
		}

	};
	
	private final Map<String, Metric> instances;

	@Inject
	protected Metrics() {
		super();
		this.instances = new ConcurrentHashMap<>();

		// Inside String
		add(new InStr());
		
		// Levenshtein Distance
		add(new NoSpLD());
		add(new NoSpAzLD());
		add(new WwLD());
		add(new MaxWwNoSpLD());
		add(new CombWwNoSpLD());
		add(new CombAllLD());
		
		// Damerau-Levenshtein Distance
		add(new NoSpDLD());
		add(new NoSpAzDLD());
		add(new WwDLD());
		add(new MaxWwNoSpDLD());
		add(new CombWwNoSpDLD());
		add(new CombAllDLD());

		// Longest Common Substring
		add(new NoSpLCS());
		add(new NoSpAzLCS());
		add(new WwLCS());
		add(new MaxWwNoSpLCS());
		add(new CombWwNoSpLCS());
		add(new CombAllLCS());
		
		// Sørensen–Dice Coefficient
		add(new NoSpSDC());
		add(new NoSpAzSDC());
		add(new AvgWwSDC());
		add(new MaxWwSDC());
		add(new MaxAvgWwNoSpSDC());
		add(new CombAvgWwNoSpSDC());
		add(new CombAllSDC());
		
		// Unigrams Cosine
		add(new NoSpCOS1());
		add(new AvgWwCOS1());
		add(new MaxWwCOS1());
		add(new CombAllCOS1());

		// Bigrams Cosine
		add(new NoSpCOS2());
		add(new NoSpAzCOS2());
		add(new AvgWwCOS2());
		add(new MaxWwCOS2());
		add(new CombAllCOS2());

		// Trigrams Cosine
		add(new NoSpCOS3());
		add(new NoSpAzCOS3());
		add(new AvgWwCOS3());
		add(new MaxWwCOS3());
		add(new CombAllCOS3());

		// Cosine
		add(new MaxNoSpCOS());

		// Unigrams Jaccard
		add(new NoSpJCD1());
		add(new AvgWwJCD1());
		add(new MaxWwJCD1());
		add(new CombAllJCD1());

		// Bigrams Jaccard
		add(new NoSpJCD2());
		add(new NoSpAzJCD2());
		add(new AvgWwJCD2());
		add(new MaxWwJCD2());
		add(new CombAllJCD2());

		// Trigrams Jaccard
		add(new NoSpJCD3());
		add(new NoSpAzJCD3());
		add(new AvgWwJCD3());
		add(new MaxWwJCD3());
		add(new CombAllJCD3());
		
		// Jaccard
		add(new MaxNoSpJCD());

		// Extended metrics
		add(new TitleEx());
		add(new ArtistEx());
		add(new TitleEx2());
		add(new ArtistEx2());
		add(new TitleEx3());
		add(new ArtistEx3());
		
	}
	
	public Metric add(Metric metric) {
		return instances.put(metric.getName(), metric);
	}

	public Collection<Metric> all() {
		return instances.values();
	}

	public Metric getInstance(String name) {
		final Metric metric = instances.get(name);
		if (null == metric) {
			throw new IllegalArgumentException("unknown metric \"" + name + "\"");
		}
		return metric;
	}
	
//	public static void main(String[] args) {
//		ArrayList<String> metrics = new ArrayList<>();
//		for (Metric m : new Metrics().all()) {
//			metrics.add(m.getName());
//		}
//		metrics.sort((String a, String b)->(a.compareTo(b)));
//		for (String s : metrics) {
//			System.out.println(s);
//		}
//	}
	
//	public static void main(String[] args) {
//		Metrics metrics = new Metrics();
//		String a = "FOX SAMANTHA";
//		String b = "SAMANTHA FOX SAMANTHA";
////		a = "ARRIVERA L ESTATE";
////		b = "ARRIVERA'L'ESTATE";
//		System.out.println("a = \"" + a + "\"");
//		System.out.println("b = \"" + b + "\"");
//		for (Metric metric : metrics.all()) {
//			double ab = metric.similarity(a, b);			
//			double ba = metric.similarity(b, a);
//			System.out.println(String.format("%s: ab=%g, ba=%g %s", metric.getName(), ab, ba, ab == ba ? "(symmetric)" : "(non symmetric)"));
//		}
//	}
	
}
