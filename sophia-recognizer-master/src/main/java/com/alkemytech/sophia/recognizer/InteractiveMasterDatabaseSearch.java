package com.alkemytech.sophia.recognizer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

import org.apache.lucene.queryparser.classic.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.recognizer.db.MasterDatabase;
import com.alkemytech.sophia.recognizer.model.Work;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class InteractiveMasterDatabaseSearch {
	
	private static final Logger logger = LoggerFactory.getLogger(InteractiveMasterDatabaseSearch.class);

	protected static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args) {
			super(args);
		}

		@Override
		protected void configure() {
			super.configure();
			// singleton(s)
			bind(MasterDatabase.class).asEagerSingleton();
			bind(InteractiveMasterDatabaseSearch.class).asEagerSingleton();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			final InteractiveMasterDatabaseSearch instance = Guice.createInjector(new GuiceModuleExtension(args))
				.getInstance(InteractiveMasterDatabaseSearch.class)
					.startup();
			try {
				instance.process(args);
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}
	
	private final MasterDatabase masterDatabase;
	private final Gson gson;
	
	@Inject
	protected InteractiveMasterDatabaseSearch(@Named("configuration") Properties configuration,
			MasterDatabase masterDatabase) {
		super();
		this.masterDatabase = masterDatabase;
		this.gson = new GsonBuilder()
				.disableHtmlEscaping()
				.setPrettyPrinting()
				.create();
	}
	
	public InteractiveMasterDatabaseSearch startup() {
		masterDatabase.startup();
		return this;
	}

	public InteractiveMasterDatabaseSearch shutdown() {
		masterDatabase.shutdown();
		return this;
	}
	
	public InteractiveMasterDatabaseSearch process(String[] args) throws IOException, ParseException {
		
		System.out.println();
		System.out.println("type \"exit\" to quit");
		try (BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in))) {
			for (String line = stdin.readLine(); !"exit".equals(line); line = stdin.readLine()) {
				if (null == line || "".equals(line)) {
					continue;
				}
				System.out.println("input text: \"" + line + "\"");

				try {
					final Work work = masterDatabase.getByCode(line);
					if (null != work) {
						System.out.println("getByCode: " + work.toJson(gson));
					}
				} catch (Exception e) { }

				try {
					final Work work = masterDatabase.getById(Long.parseLong(line));
					if (null != work) {
						System.out.println("getById: " + work.toJson(gson));
					}
				} catch (Exception e) { }
				
				System.out.println();
				System.out.println();				
			}
		}
		
		return this;
	}

}