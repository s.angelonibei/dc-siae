package com.alkemytech.sophia.recognizer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.ServerSocket;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.lucene.queryparser.classic.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.guice.Log4j2;
import com.alkemytech.sophia.recognizer.db.MasterDatabase;
import com.alkemytech.sophia.recognizer.lucene.LuceneSearch;
import com.alkemytech.sophia.recognizer.score.ScoreCalculator;
import com.alkemytech.sophia.recognizer.trie.TrieSearch;
import com.alkemytech.sophia.recognizer.utilization.UtilizationsReader;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import com.ibm.icu.text.SimpleDateFormat;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class AwsMonitor {
	
	private static final Logger logger = LoggerFactory.getLogger(AwsMonitor.class);
	private class GuiceModuleForProcessing extends AbstractModule {
		
		protected final Properties configuration;
		
		public GuiceModuleForProcessing(Properties configuration) {
			super();
			this.configuration = configuration;
		}

		@Override
		protected void configure() {
			
			// configuration properties
			Names.bindProperties(binder(), configuration);
			bind(Properties.class)
				.annotatedWith(Names.named("configuration"))
				.toInstance(configuration);

			// charset(s)
			bind(Charset.class)
				.annotatedWith(Names.named("charset"))
				.toInstance(Charset.forName(configuration.getProperty("default.charset", "UTF-8")));
			
			// default time zone
			TimeZone.setDefault(TimeZone.getTimeZone(configuration.getProperty("default.timezone", "UTC")));

			// initialize log4j2
			Log4j2.initialize(configuration);
			
			// gson
			bind(Gson.class)
				.toInstance(new GsonBuilder()
						.disableHtmlEscaping()
						.create());	
			
			// singleton(s)
			bind(MasterDatabase.class).asEagerSingleton();
			bind(LuceneSearch.class).asEagerSingleton();
			bind(TrieSearch.class).asEagerSingleton();
			bind(UtilizationsReader.class).asEagerSingleton();
			bind(ScoreCalculator.class).asEagerSingleton();
			bind(StringMatching.class).asEagerSingleton();
		}

	}
	
	private static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args) {
			super(args);
		}

		@Override
		protected void configure() {
			super.configure();
			// singleton(s)
			bind(S3.class).asEagerSingleton();
			bind(SQS.class).asEagerSingleton();
			bind(AwsMonitor.class).asEagerSingleton();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			final AwsMonitor instance = Guice.createInjector(new GuiceModuleExtension(args))
				.getInstance(AwsMonitor.class)
					.startup();
			try {
				instance.process();
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}
	
	private final Properties configuration;
	private final S3 s3;
	private final SQS sqs;
	private final Gson gson;
	
	@Inject
	protected AwsMonitor(@Named("configuration") Properties configuration,
			S3 s3, SQS sqs, Gson gson) {
		super();
		this.configuration = configuration;
		this.s3 = s3;
		this.sqs = sqs;
		this.gson = gson;
	}
	
	public AwsMonitor startup() {
		s3.startup();
		sqs.startup();
		return this;
	}

	public AwsMonitor shutdown() {
		sqs.shutdown();
		s3.shutdown();
		return this;
	}
	
	private void parseMessage(SQS.Msg message, Set<S3.Url> createdObjects, Set<S3.Url> removedObjects) {
		try {
			final JsonObject rootJsonObject = gson.fromJson(message.text, JsonObject.class);
			final JsonArray jsonArray = rootJsonObject.getAsJsonArray("Records");
			if (null != jsonArray) {
				for (JsonElement element : jsonArray) {
					final JsonObject record = element.getAsJsonObject();
					logger.debug("record {}", record);
					final JsonElement eventName = record.get("eventName");
					final JsonObject s3 = record.getAsJsonObject("s3");
					final JsonElement bucketName = s3.getAsJsonObject("bucket").get("name");
					final JsonElement objectKey = s3.getAsJsonObject("object").get("key");
					if (null != bucketName && null != objectKey && null != eventName) {
						if (!Strings.isNullOrEmpty(bucketName.getAsString()) &&
								!Strings.isNullOrEmpty(objectKey.getAsString()) &&
								!Strings.isNullOrEmpty(eventName.getAsString())) {
							if (eventName.getAsString().contains("ObjectCreated")) {
								createdObjects.add(new S3.Url(bucketName.getAsString(),
										objectKey.getAsString()));
							} else if (eventName.getAsString().contains("ObjectRemoved")) {
								removedObjects.add(new S3.Url(bucketName.getAsString(),
										objectKey.getAsString()));
							}
						}
					}
				}
			}
		} catch (JsonSyntaxException e) {
			logger.warn("parseMessage", e);
		}
	}
	
	private boolean processBatch(File configFile, File inputFile, File outputFile, File kpiFile) throws IOException, ParseException, InterruptedException, ExecutionException {
		
		// load configuration
		final Properties properties = new Properties();
		properties.putAll(configuration);
		try (Reader reader = new FileReader(configFile)) {
			properties.load(reader); // override defaults
		}

		// override configuration parameters
		properties.setProperty("utilizationsReader.homeFolder", inputFile.getParent());
		properties.setProperty("utilizationsReader.filenames", inputFile.getName());
		properties.setProperty("stringMatching.backupConfiguration", "false");
		properties.setProperty("stringMatching.saveSkipped", "false");
				
		// create guice injector using configuration
		final Injector injector = Guice
				.createInjector(new GuiceModuleForProcessing(properties));

		// startup, execute and shutdown string matching
		final StringMatching stringMatching = injector
				.getInstance(StringMatching.class);
		try {
			stringMatching.startup()
				.process(outputFile, null, kpiFile);
		} finally {
			stringMatching.shutdown();
		}
		
		return true;
	}
	
	public AwsMonitor process() throws Exception {
		
		final File homeFolder = new File(configuration
				.getProperty("awsMonitor.homeFolder"));
		final String queueUrl = sqs.getUrl(configuration
				.getProperty("awsMonitor.queueName"));
		final long pollingDelayMilllis = Long.parseLong(configuration
				.getProperty("awsMonitor.pollingDelayMilllis"));
		final long maxIdleMillis = Long.parseLong(configuration
				.getProperty("awsMonitor.maxIdleMillis"));
		final String watchedPrefix = configuration
				.getProperty("awsMonitor.watchedPrefix");
		final String configSuffix = configuration
				.getProperty("awsMonitor.configSuffix").toLowerCase();
		final String inputSuffix = configuration
				.getProperty("awsMonitor.inputSuffix").toLowerCase();
		final String lockSuffix = configuration
				.getProperty("awsMonitor.lockSuffix").toLowerCase();
		final String outputSuffix = configuration
				.getProperty("awsMonitor.outputSuffix").toLowerCase();
		final String zipOutputSuffix = configuration
				.getProperty("awsMonitor.zipOutputSuffix").toLowerCase();
		final String kpiSuffix = configuration
				.getProperty("awsMonitor.kpiSuffix").toLowerCase();
		final String errorSuffix = configuration
				.getProperty("awsMonitor.errorSuffix").toLowerCase();
		final int bindPort = Integer.parseInt(configuration
				.getProperty("awsMonitor.bindport", configuration.getProperty("default.bindport", "0")));
		final boolean archiveOnS3UploadError = "true".equalsIgnoreCase(configuration
				.getProperty("awsMonitor.archiveOnS3UploadError"));

		// bind to lock tcp port
		try (ServerSocket socket = new ServerSocket(bindPort)) {
			final long startTimeMillis = System.currentTimeMillis();
			long lastMessageTimeMillis = startTimeMillis;
			while (System.currentTimeMillis() - lastMessageTimeMillis < maxIdleMillis) {
				
				// poll SQS queue
				logger.debug("polling queue {}", queueUrl);
				final List<SQS.Msg> messages = sqs.receiveMessages(queueUrl);
				if (!messages.isEmpty()) {
					// parse messages and collect all watched url prefix(es)
					final Set<S3.Url> watchedUrlPrefixes = new HashSet<>();
					for (SQS.Msg message : messages) {
						final Set<S3.Url> createdObjects = new HashSet<>();
						final Set<S3.Url> removedObjects = new HashSet<>();
						parseMessage(message, createdObjects, removedObjects);
						int otherWatcherObjects = 0;
						// analyze created object(s)
						if (!createdObjects.isEmpty()) {
							final Iterator<S3.Url> iterator = createdObjects.iterator();
							while (iterator.hasNext()) {
								final S3.Url object = iterator.next();
								if (object.key.startsWith(watchedPrefix)) {
									final String key = object.key.toLowerCase();
									// watch for created config or input only
									if (key.endsWith(configSuffix)) {
										watchedUrlPrefixes.add(new S3.Url(object.bucket,
												object.key.substring(0, key.length() - configSuffix.length())));
									} else if (key.endsWith(inputSuffix)) {
										watchedUrlPrefixes.add(new S3.Url(object.bucket,
												object.key.substring(0, key.length() - inputSuffix.length())));
									} else {
										logger.debug("non watched object {}", object);
									}
								} else {
									otherWatcherObjects ++;
									logger.debug("other watcher object {}", object);
								}							
							}
						}
						// analyze removed object(s)
						if (!removedObjects.isEmpty()) {
							final Iterator<S3.Url> iterator = removedObjects.iterator();
							while (iterator.hasNext()) {
								final S3.Url object = iterator.next();
								if (object.key.startsWith(watchedPrefix)) {
									final String key = object.key.toLowerCase();
									// watch for removed lock only
									if (key.endsWith(lockSuffix)) {
										watchedUrlPrefixes.add(new S3.Url(object.bucket,
												object.key.substring(0, key.length() - lockSuffix.length())));
									} else {
										logger.debug("non watched object {}", object);
									}
								} else {
									otherWatcherObjects ++;
									logger.debug("other watcher object {}", object);
								}							
							}
						}
						logger.debug("other watcher objects {}", otherWatcherObjects);
						// delete sqs message
						if (0 == otherWatcherObjects) {
							if (sqs.deleteMessage(queueUrl, message.receiptHandle)) {
								logger.debug("deleted message {}", message.receiptHandle);
							} else {
								logger.warn("unable to delete message {}", message.receiptHandle);
							}
						} else {
							logger.warn("mixed watcher message {}", message.text);
						}
					}
					logger.debug("watchedUrlPrefixes {}", watchedUrlPrefixes);
					if (!watchedUrlPrefixes.isEmpty()) {
						// search for prefixes having config and input objects, but not lock object
						for (S3.Url prefix : watchedUrlPrefixes) {
							// urls
							final S3.Url configUrl = new S3.Url(prefix.bucket, prefix.key + configSuffix);
							final S3.Url inputUrl = new S3.Url(prefix.bucket, prefix.key + inputSuffix);
							final S3.Url lockUrl = new S3.Url(prefix.bucket, prefix.key + lockSuffix);
							final S3.Url outputUrl = new S3.Url(prefix.bucket, prefix.key + zipOutputSuffix);
							final S3.Url kpiUrl = new S3.Url(prefix.bucket, prefix.key + kpiSuffix);
							final S3.Url errorUrl = new S3.Url(prefix.bucket, prefix.key + errorSuffix);
							// check objects existence
							final boolean configExists = s3.doesObjectExist(configUrl);
							final boolean inputExists = s3.doesObjectExist(inputUrl);
							final boolean lockExists = s3.doesObjectExist(lockUrl);
							logger.debug("config exists {}", configExists);
							logger.debug("input exists {}", inputExists);
							logger.debug("lock exists {}", lockExists);
							if (configExists && inputExists && !lockExists) {
								lastMessageTimeMillis = System.currentTimeMillis();
								boolean s3UploadError = false;
								
								// batch filename
								final String filename = prefix.key.substring(1 + prefix.key.lastIndexOf('/'));
								logger.debug("batch filename {}", filename);
								
								// local files
								final File lockFile = new File(homeFolder, filename + lockSuffix);
								final File configFile = new File(homeFolder, filename + configSuffix);
								final File inputFile = new File(homeFolder, filename + inputSuffix);
								final File outputFile = new File(homeFolder, filename + outputSuffix);
								final File zipOutputFile = new File(homeFolder, filename + zipOutputSuffix);
								final File kpiFile = new File(homeFolder, filename + kpiSuffix);
								final File errorFile = new File(homeFolder, filename + errorSuffix);
								try {
									
									// create lock file and upload to s3
									try (Writer writer = new FileWriter(lockFile)) {
										writer.write(new Date().toString());
									}
									if (!s3.upload(lockUrl, lockFile)) {
										logger.warn("unable to upload lock file {} to {}", lockFile, lockUrl);
										continue;
									}
									
									// download config from s3
									if (!s3.download(configUrl, configFile)) {
										logger.warn("unable to download config file {} from {}", configFile, configUrl);
										continue;
									}
									
									// download input from s3
									if (!s3.download(inputUrl, inputFile)) {
										logger.warn("unable to download input file {} from {}", inputFile, inputUrl);
										continue;
									}
																		
									// process batch
									zipOutputFile.delete();
									if (!processBatch(configFile, inputFile, outputFile, kpiFile)) {
										logger.warn("unable to process batch {}", filename);
									}

									// upload output to s3
									if (outputFile.exists()) {
										try (ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipOutputFile));
												InputStream in = new FileInputStream(outputFile)) {
											final byte[] buffer = new byte[1024];
											out.putNextEntry(new ZipEntry(outputFile.getName()));
											for (int length; -1 != (length = in.read(buffer)); ) {
												out.write(buffer, 0, length);
											}
											out.closeEntry();
										}
										if (!s3.upload(outputUrl, zipOutputFile)) {
											s3UploadError = true;
											logger.warn("unable to upload output file {} to {}", zipOutputFile, outputUrl);
										}
									} else {
										logger.warn("output file does not exists {}", zipOutputFile);
									}

									// upload kpi to s3
									if (kpiFile.exists()) {
										if (!s3.upload(kpiUrl, kpiFile)) {
											s3UploadError = true;
											logger.warn("unable to upload kpi file {} to {}", kpiFile, kpiUrl);
										}
									} else {
										logger.warn("kpi file does not exists {}", kpiFile);
									}

								} catch (Exception e) {
									// create error file and upload to s3
									logger.info("Error :"+e.getMessage());
									try (PrintWriter writer = new PrintWriter(new FileWriter(errorFile))) {
										e.printStackTrace(writer);
									}
									if (!s3.upload(errorUrl, errorFile)) {
										s3UploadError = true;
										logger.warn("unable to upload error file {} to {}", errorFile, errorUrl);
									}
								} finally {
									// create archive
									if (s3UploadError && archiveOnS3UploadError) {
										final File archiveFile = new File(homeFolder, new SimpleDateFormat("yyyy_MM_dd_HHmm")
												.format(new Date(startTimeMillis)) + "_" + filename + "_archive.zip");
										final File[] files = { configFile, inputFile, outputFile, zipOutputFile, kpiFile, errorFile };
										final byte[] buffer = new byte[1024];
										try (ZipOutputStream out = new ZipOutputStream(new FileOutputStream(archiveFile))) {
											for (File file : files) {
												if (file.length() > 0L) {
													try (InputStream in = new FileInputStream(file)) {
														out.putNextEntry(new ZipEntry(file.getName()));
														for (int length; -1 != (length = in.read(buffer)); ) {
															out.write(buffer, 0, length);
														}
														out.closeEntry();
													}
												}
											}
										} catch (Exception e) {
											logger.error("error creating backup archive", e);
										}
									}
									// delete local files
									lockFile.delete();
									configFile.delete();
									inputFile.delete();
									outputFile.delete();									
									zipOutputFile.delete();									
									kpiFile.delete();									
									errorFile.delete();
								}
							}
						}
					}		
				} else {				
					// slow down
					Thread.sleep(pollingDelayMilllis);
				}
			}
			logger.debug("completed in {}s", (System.currentTimeMillis() - startTimeMillis) / 1000L);
		}

		return this;
	}

}