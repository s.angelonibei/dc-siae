package com.alkemytech.sophia.recognizer.metric;

/**
 * A Meter to measure string distance
 * 
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public interface Meter {

	/**
	 * Measure distance between <code>a</code> and <code>b</code>
	 * 
	 * @param a
	 * @param b
	 * @return the evaluated measure
	 */
	public double measure(String a, String b);

}
