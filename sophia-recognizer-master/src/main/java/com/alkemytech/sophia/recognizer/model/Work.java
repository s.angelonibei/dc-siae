package com.alkemytech.sophia.recognizer.model;

import java.util.HashSet;
import java.util.Set;

import com.alkemytech.sophia.recognizer.json.Gsonable;
import com.alkemytech.sophia.recognizer.metric.Metric;
import com.alkemytech.sophia.recognizer.norm.Norm;
import com.alkemytech.sophia.recognizer.utilization.Utilization;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;
import com.sleepycat.persist.model.Relationship;
import com.sleepycat.persist.model.SecondaryKey;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
@Entity
public class Work implements Gsonable {

	public static final String ID = "id";
	public static final String CODE = "code";
	public static final String TITLE = "title";
	public static final String TITLES = "titles";
	public static final String ARTISTS = "artists";
	public static final String EL = "el";
	public static final String PD = "pd";
	public static final String SCOPE = "scope";
	public static final String VALUE = "value";

	public static interface Selector {
		public Selector select(Work work);
	}
	
	@PrimaryKey
	public Long id;
    @SecondaryKey(relate=Relationship.ONE_TO_ONE)
	public String code;
    public Title title;
	public Set<Title> titles;
	public Set<Artist> artists;
	public Boolean el;
	public Boolean pd;
	public String scope;
	public Double value;
	
	public Work() {
		super();
	}

	public Work(String code, Title title, Set<Title> titles, Set<Artist> artists, Boolean el, Boolean pd, String scope, Double value) {
		super();
		this.code = code;
		this.title = title;
		this.titles = titles;
		this.artists = artists;
		this.el = el;
		this.pd = pd;
		this.scope = scope;
		this.value = value;
	}

	public Work(Long id, String code, Title title, Set<Title> titles, Set<Artist> artists, Boolean el, Boolean pd, String scope, Double value) {
		super();
		this.id = id;
		this.code = code;
		this.title = title;
		this.titles = titles;
		this.artists = artists;
		this.el = el;
		this.pd = pd;
		this.scope = scope;
		this.value = value;
	}

	public Work(Work that) {
		super();
		this.id = that.id;
		this.code = that.code;
		this.title = that.title;
		if (null != that.titles) {
			this.titles = new HashSet<>(that.titles);
		}
		if (null != that.artists) {
			this.artists = new HashSet<>(that.artists);
		}
		this.el = that.el;
		this.pd = that.pd;
		this.scope = that.scope;
		this.value = that.value;
	}

	public Work updateWith(Work that) {
		// id
		if (null == this.id) {
			this.id = that.id;
		} else if (null != that.id) {
			if (!this.id.equals(that.id)) {
				throw new IllegalArgumentException("id mismatch");
			}
		}
		// code
		if (null == this.code) {
			this.code = that.code;
		} else if (null != that.code) {
			if (!this.code.equals(that.code)) {
				throw new IllegalArgumentException("code mismatch");
			}
		}
		// title
		if (null != that.title) {
			this.title = that.title;
		}
		// title(s)
		if (null == this.titles) {
			if (null != that.titles) {
				this.titles = new HashSet<>(that.titles);
			}
		} else if (null != that.titles) {
			this.titles.addAll(that.titles);
		}
		// artist(s)
		if (null == this.artists) {
			if (null != that.artists) { 
				this.artists = new HashSet<>(that.artists);
			}
		} else if (null != that.artists) {
			this.artists.addAll(that.artists);
		}
		// el
		if (null != that.el) {
			this.el = that.el;
		}
		// pd
		if (null != that.pd) {
			this.pd = that.pd;
		}
		// scope
		if (null != that.scope) {
			this.scope = that.scope;
		}
		// value
		if (null != that.value) {
			this.value = that.value;
		}
		return this;
	}

	public ScoredTitle getTitleSimilarity(Utilization utilization, Metric metric) {
		Title currentTitle = title;
		double currentSimilarity = metric
				.similarity(utilization.title, title.title);
		if (null != titles) {
			for (Title title : titles) {
				final double similarity = metric
						.similarity(utilization.title, title.title);
				if (similarity > currentSimilarity) {
					currentTitle = title;
					currentSimilarity = similarity;
				}
			}
		}
		return new ScoredTitle(currentTitle, currentSimilarity);
	}

	public ScoredArtist getArtistSimilarity(Utilization utilization, Metric metric) {
		Artist currentArtist = null;
		double currentSimilarity = 0.0;
		if (null != artists) {
			for (Artist artist : artists) {
				for (String name : utilization.artists) {
					final double similarity = metric
							.similarity(name, artist.name);
					if (null == currentArtist ||
							similarity > currentSimilarity) {
						currentArtist = artist;
						currentSimilarity = similarity;
					}
				}
			}
		}
		return new ScoredArtist(currentArtist, currentSimilarity);
	}
	
	public ScoredArtist getArtistSimilarity(Utilization utilization, Metric metric, ArtistRole role) {
		Artist currentArtist = null;
		double currentSimilarity = 0.0;
		if (null != artists) {
			for (Artist artist : artists) {
				if (role == artist.role) {
					for (String name : utilization.artists) {
						final double similarity = metric
								.similarity(name, artist.name);
						if (null == currentArtist ||
								similarity > currentSimilarity) {
							currentArtist = artist;
							currentSimilarity = similarity;
						}
					}
				}
			}
		}
		return new ScoredArtist(currentArtist, currentSimilarity);
	}
	
	public double getNormalizedValue(Norm norm) {
		return (null == value ? 0.0 : norm.normalize(value));
	}

	@Override
	public JsonObject toJsonObject() {
		final JsonObject jsonObject = new JsonObject();
		if (null != id) {
			jsonObject.addProperty(ID, id);
		}
		if (null != code) {
			jsonObject.addProperty(CODE, code);
		}
		if (null != title) {
			jsonObject.add(TITLE, title.toJsonObject());
		}
		if (null != titles && !titles.isEmpty()) {
			JsonArray jsonArray = new JsonArray();
			for (Title title : titles) {
				jsonArray.add(title.toJsonObject());
			}
			jsonObject.add(TITLES, jsonArray);
		}
		if (null != artists && !artists.isEmpty()) {
			JsonArray jsonArray = new JsonArray();
			for (Artist artist : artists) {
				jsonArray.add(artist.toJsonObject());
			}
			jsonObject.add(ARTISTS, jsonArray);
		}
		if (null != el) {
			jsonObject.addProperty(EL, el);
		}
		if (null != pd) {
			jsonObject.addProperty(PD, pd);
		}
		if (null != scope) {
			jsonObject.addProperty(SCOPE, scope);
		}
		if (null != value) {
			jsonObject.addProperty(VALUE, value);
		}
		return jsonObject;
	}
	
	@Override
	public Work initWithJsonObject(JsonObject jsonObject) {
		JsonElement jsonElement = jsonObject.get(ID);
		this.id = (null == jsonElement ? null : jsonElement.getAsLong());
		jsonElement = jsonObject.get(CODE);
		this.code = (null == jsonElement ? null : jsonElement.getAsString());
		jsonElement = jsonObject.get(TITLE);
		if (null != jsonElement) {
			this.title = new Title().initWithJsonObject(jsonElement.getAsJsonObject());
		}
		jsonElement = jsonObject.get(TITLES);
		if (null != jsonElement) {
			this.titles = new HashSet<>();
			for (JsonElement jsonArrayElement : jsonElement.getAsJsonArray()) {
				this.titles.add(new Title().initWithJsonObject(jsonArrayElement.getAsJsonObject()));
			}
		}
		jsonElement = jsonObject.get(ARTISTS);
		if (null != jsonElement) {
			this.artists = new HashSet<>();
			for (JsonElement jsonArrayElement : jsonElement.getAsJsonArray()) {
				this.artists.add(new Artist().initWithJsonObject(jsonArrayElement.getAsJsonObject()));
			}
		}
		jsonElement = jsonObject.get(EL);
		this.el = (null == jsonElement ? null : jsonElement.getAsBoolean());
		jsonElement = jsonObject.get(PD);
		this.pd = (null == jsonElement ? null : jsonElement.getAsBoolean());
		jsonElement = jsonObject.get(SCOPE);
		this.scope = (null == jsonElement ? null : jsonElement.getAsString());
		jsonElement = jsonObject.get(VALUE);
		this.value = (null == jsonElement ? null : jsonElement.getAsDouble());
		return this;
	}

	@Override
	public String toJson(Gson gson) {
		return gson.toJson(toJsonObject());
	}

	@Override
	public Work initWithJson(String json, Gson gson) {
		return initWithJsonObject(gson.fromJson(json, JsonObject.class));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((null == id) ? 0 : id.hashCode());
		result = prime * result + ((null == code) ? 0 : code.hashCode());
		result = prime * result + ((null == title) ? 0 : title.hashCode());
		result = prime * result + ((null == titles) ? 0 : titles.hashCode());
		result = prime * result + ((null == artists) ? 0 : artists.hashCode());
		result = prime * result + ((null == el) ? 0 : el.hashCode());
		result = prime * result + ((null == pd) ? 0 : pd.hashCode());
		result = prime * result + ((null == scope) ? 0 : scope.hashCode());
		result = prime * result + ((null == value) ? 0 : value.hashCode());
		return result;
	}

	//	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		} else if (null == object) {
			return false;
		} else if (getClass() != object.getClass()) {
			return false;
		}
		final Work that = (Work) object;
		// id
		if (null == this.id) {
			if (null != that.id) {
				return false;
			}
		} else if (!this.id.equals(that.id)) {
			return false;
		}
		// code
		if (null == this.code) {
			if (null != that.code) {
				return false;
			}
		} else if (!this.code.equals(that.code)) {
			return false;
		}
		// title
		if (null == this.title) {
			if (null != that.title) {
				return false;
			}
		} else if (!this.title.equals(that.title)) {
			return false;
		}
		// titles
		if (null == this.titles) {
			if (null != that.titles) {
				return false;
			}
		} else if (!this.titles.equals(that.titles)) {
			return false;
		}
		// artists
		if (null == this.artists) {
			if (null != that.artists) {
				return false;
			}
		} else if (!this.artists.equals(that.artists)) {
			return false;
		}
		// el
		if (null == this.el) {
			if (null != that.el) {
				return false;
			}
		} else if (!this.el.equals(that.el)) {
			return false;
		}
		// pd
		if (null == this.pd) {
			if (null != that.pd) {
				return false;
			}
		} else if (!this.pd.equals(that.pd)) {
			return false;
		}
		// scope
		if (null == this.scope) {
			if (null != that.scope) {
				return false;
			}
		} else if (!this.scope.equals(that.scope)) {
			return false;
		}
		// value
		if (null == this.value) {
			if (null != that.value) {
				return false;
			}
		} else if (!this.value.equals(that.value)) {
			return false;
		}
		return true;
	}
	
}
