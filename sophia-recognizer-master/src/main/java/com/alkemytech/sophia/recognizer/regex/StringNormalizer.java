package com.alkemytech.sophia.recognizer.regex;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.QuoteMode;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class StringNormalizer {

	private final ArrayList<RegExReplaceAll> replacers;
	
	public StringNormalizer(int exitCode,String...filenames) {
		try {
			replacers = new ArrayList<>();
			for (String filename : filenames) {
				try (InputStream stream = new FileInputStream(filename)) {
					replacers.addAll(parseCsv(stream));
				} catch (FileNotFoundException e) {
					try (InputStream stream = getClass().getClassLoader()
							.getResourceAsStream(filename)) {
						replacers.addAll(parseCsv(stream));
					}catch (IOException ex){
						exitCode = 1 ;
					}
				}
			}
		} catch (IOException e) {
			exitCode = 1 ;
			throw new IllegalArgumentException(e);
		}
	}

	public StringNormalizer(String...filenames) {
		try {
			replacers = new ArrayList<>();
			for (String filename : filenames) {
				try (InputStream stream = new FileInputStream(filename)) {
					replacers.addAll(parseCsv(stream));
				} catch (FileNotFoundException e) {
					try (InputStream stream = getClass().getClassLoader()
							.getResourceAsStream(filename)) {
						replacers.addAll(parseCsv(stream));
					}
				}
			}
		} catch (IOException e) {
			throw new IllegalArgumentException(e);
		}
	}

	public StringNormalizer(File...files) {
		try {
			replacers = new ArrayList<>();
			for (File file : files) {
				try (InputStream stream = new FileInputStream(file)) {
					replacers.addAll(parseCsv(stream));
				}
			}
		} catch (IOException e) {
			throw new IllegalArgumentException(e);
		}
	}

	public StringNormalizer(InputStream...streams) {
		try {
			replacers = new ArrayList<>();
			for (InputStream stream : streams) {
				replacers.addAll(parseCsv(stream));
			}
		} catch (IOException e) {
			throw new IllegalArgumentException(e);
		}
	}
	
	private ArrayList<RegExReplaceAll> parseCsv(InputStream in) throws IOException {
		final ArrayList<RegExReplaceAll> replacers = new ArrayList<>();
		try (InputStreamReader reader = new InputStreamReader(in)) {
			try (CSVParser parser = new CSVParser(reader, CSVFormat.DEFAULT
					.withIgnoreEmptyLines()
					.withQuoteMode(QuoteMode.MINIMAL)
					.withQuote('"')
					.withIgnoreSurroundingSpaces()
					.withCommentMarker('#'))) {
				for (CSVRecord record : parser) {
					if (record.size() >= 2) {
						replacers.add(new RegExReplaceAll(record.get(0), record.get(1)));						
					}
				}
			}
		}
		return replacers;
	}
	
	public String normalize(String target) {
		if (null != replacers) {
			for (RegExReplaceAll replacer : replacers) {
				target = replacer.replaceAll(target);
			}
		}
		return target;
	}
	
//	public static void main(String[] args) throws Exception {
//		final StringNormalizer normalizer = new StringNormalizer("words-blacklist-artist.csv");
//		String[] strings = {
//				"BALLO POPOLARE CHE MI PIACE TANTO",
//				"QUESTO E' UN BALLO POPOLARE ",
//				"AA VV",
//				"AA VV RICKY",
//		};
//		for (String string : strings) {
//			System.out.println("\"" + string + "\" = \"" + normalizer.normalize(string) + "\"");
//		}
//	}
	
}
