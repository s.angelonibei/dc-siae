package com.alkemytech.sophia.recognizer.util;

import java.util.Arrays;
import java.util.Locale;

import com.ibm.icu.text.RuleBasedNumberFormat;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class Str {

	// concat terms
	public static String concat(String...terms) {
		final StringBuilder concat = new StringBuilder();
		for (String term : terms) {
			concat.append(term);
		}
		return concat.toString();	
	}
	
	// drop all but alphanumeric chars
	public static String az09(String string) {
		return string.replaceAll("[^A-Z0-9]", "");
	}
	
	// drop all but alphanumeric chars and sort characters
	public static char[] sortedAz09Chars(String string) {
		char[] charArray = string
				.replaceAll("[^A-Z0-9]", "").toCharArray();
		Arrays.sort(charArray);
		return charArray;
	}
	
	// left pad string up to length
	public static String lpad(String string, int length, char pad) {
		length -= string.length();
		if (length > 0) {
			final StringBuilder builder = new StringBuilder();
			for (; length > 0; length --) {
				builder.append(pad);
			}
			return builder.append(string)
					.toString();
		}
		return string;
	}

	// right pad string up to length
	public static String rpad(String string, int length, char pad) {
		length -= string.length();
		if (length > 0) {
			final StringBuilder builder = new StringBuilder()
					.append(string);
			for (; length > 0; length --) {
				builder.append(pad);
			}
			return builder.toString();
		}
		return string;
	}
	
	// number to text
	public static String spell(int number, String language) {
		return spell(number, Locale.forLanguageTag(language));
	}
	public static String spell(int number, Locale locale) {
		return new RuleBasedNumberFormat(locale,
				RuleBasedNumberFormat.SPELLOUT).format(number).toUpperCase();
	}
	
//	public static void main(String[] args) {
//		System.out.println(spell(1092378, "it"));
//		System.out.println(spell(1092378, "en"));
//		System.out.println(spell(1092378, "fr"));
//		System.out.println(spell(1092378, "de"));
//		System.out.println(spell(1092378, "es"));
//	}
	
}
