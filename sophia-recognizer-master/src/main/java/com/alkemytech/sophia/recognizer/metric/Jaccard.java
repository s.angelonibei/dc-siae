package com.alkemytech.sophia.recognizer.metric;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.alkemytech.sophia.recognizer.util.Str;

/**
 * The similarity between the two strings is the size of the intersection divided by the size
 * of the union of their vector representations. Each input string is converted into a set of
 * n-grams, the Jaccard index is then computed as |V1 intersection V2| / |V1 union V2|.
 * 
 * @see https://blog.nishtahir.com/2015/09/19/fuzzy-string-matching-using-cosine-similarity/
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class Jaccard extends NGrams {
	
	private static double jaccard(String a, String b, int n) {
		if (a.equals(b)) {
            return 1.0;
        } else if (a.length() < n || b.length() < n) {
            return 0.0;
        }
		Map<String, Integer> aa = ngrams(a, n);
        Map<String, Integer> bb = ngrams(b, n);
        Set<String> union = new HashSet<>();
        union.addAll(aa.keySet());
        union.addAll(bb.keySet());
        return (double) (aa.size() + bb.size() -
        		union.size()) / (double) union.size();
	}
	
	public static class Unigrams implements Meter {

		@Override
		public double measure(String a, String b) {
			return jaccard(a, b, 1);
		}
		
	}
	
	public static class Bigrams implements Meter {
		
		@Override
		public double measure(String a, String b) {
			a = Str.lpad(a, 2, '\n');
			b = Str.lpad(b, 2, '\n');
			return jaccard(a, b, 2);
		}

	}
	
	public static class Trigrams implements Meter {
		
		@Override
		public double measure(String a, String b) {
			a = Str.lpad(a, 3, '\n');
			b = Str.lpad(b, 3, '\n');
			return jaccard(a, b, 3);
		}
		
	}

}
