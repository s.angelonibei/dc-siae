package com.alkemytech.sophia.recognizer.model;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class ScoredWork {
	
	public final Work work;
	public final double score;
	
	public ScoredWork(Work work, double score) {
		super();
		this.work = work;
		this.score = score;
	}

	@Override
	public int hashCode() {
		return 31 + ((null == work) ? 0 : work.hashCode());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (null == obj) {
			return false;
		} else if (getClass() != obj.getClass()) {
			return false;
		}
		final ScoredWork other = (ScoredWork) obj;
		if (null == work) {
			if (null != other.work) {
				return false;
			}
		} else if (!work.equals(other.work)) {
			return false;
		}
		return true;
	}
	
	
}
