package com.alkemytech.sophia.recognizer.utilization;

import java.util.Iterator;
import java.util.Set;

import com.alkemytech.sophia.recognizer.siada.SiadaCode;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class Utilization {

	public final int rownum;
	public final String id;
	public final String title;
	public final Set<String> artists;
	public final int results;
	public final boolean validated;
	public final String code;
	
	public Utilization(int rownum, String id, String title, Set<String> artists, int results, boolean validated, String code) {
		super();
		this.rownum = rownum;
		this.id = id;
		this.title = title;
		this.artists = artists;
		this.results = results;
		this.validated = validated;
		this.code = SiadaCode.normalize(code);
	}
	
	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder()
			.append(id)
			.append(",\"")
			.append(title)
			.append("\",\"");
		if (null != artists && !artists.isEmpty()) {
			Iterator<String> iterator = artists.iterator();
			builder.append(iterator.next());
			while (iterator.hasNext()) {
				builder.append("\",\"")
					.append(iterator.next());
			}			
		}
		builder.append("\",")
			.append(results)
			.append(',')
			.append(validated)
			.append(',')
			.append(code)
			.toString();
		return builder.toString();
	}
	
}
