package com.alkemytech.sophia.recognizer.trie;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.lucene.queryparser.classic.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.recognizer.lucene.LuceneSearch;
import com.alkemytech.sophia.recognizer.regex.StringNormalizer;
import com.alkemytech.sophia.recognizer.search.WorkCollector;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class TrieSearch {
	
	private static final Logger logger = LoggerFactory.getLogger(TrieSearch.class);
	
	private final AtomicInteger startupCalls;
	private final LuceneSearch luceneSearch;
	private final File homeFolder;
	private final File indexFolder;
	private final StringNormalizer titleNormalizer;
	private final StringNormalizer artistNormalizer;
	private final double titleMaxCostProportion;
	private final int titleMaxCostMaximum;
	private final double artistMaxCostProportion;
	private final int artistMaxCostMaximum;
	private final boolean debug;

	private MMapBitTrie titleTrie;
	private MMapBitTrie artistTrie;

	@Inject
	protected TrieSearch(@Named("configuration") Properties configuration,
			LuceneSearch luceneSearch) {
		super();
		this.startupCalls = new AtomicInteger(0);
		this.luceneSearch = luceneSearch;
		this.homeFolder = new File(configuration.getProperty("trieSearch.homeFolder"));
		this.indexFolder = new File(homeFolder, configuration.getProperty("trieSearch.indexFolder"));
		this.titleNormalizer = new StringNormalizer(configuration
				.getProperty("trieSearch.normalizer.title").split(","));
		this.artistNormalizer = new StringNormalizer(configuration
				.getProperty("trieSearch.normalizer.artist").split(","));
		this.titleMaxCostProportion = Double.parseDouble(configuration
				.getProperty("trieSearch.maxCost.title.proportion"));
		this.titleMaxCostMaximum = Integer.parseInt(configuration
				.getProperty("trieSearch.maxCost.title.maximum"));
		this.artistMaxCostProportion = Double.parseDouble(configuration
				.getProperty("trieSearch.maxCost.artist.proportion"));
		this.artistMaxCostMaximum = Integer.parseInt(configuration
				.getProperty("trieSearch.maxCost.artist.maximum"));
		this.debug = "true".equalsIgnoreCase(configuration
				.getProperty("trieSearch.debug", configuration.getProperty("default.debug")));
	}
	
	public TrieSearch startup() throws IOException {
		luceneSearch.startup();
		if (0 == startupCalls.getAndIncrement()) {
			titleTrie = new MMapBitTrie(new File(indexFolder, "title.trie"), true);
			artistTrie = new MMapBitTrie(new File(indexFolder, "artist.trie"), true);
		}
		return this;
	}

	public TrieSearch shutdown() throws IOException {
		if (0 == startupCalls.decrementAndGet()) {
			titleTrie.clear();
			titleTrie = null;
			artistTrie.clear();
			artistTrie = null;
		}
		luceneSearch.shutdown();
		return this;
	}

	public TrieSearch searchTitle(String title, WorkCollector workCollector) throws ParseException, IOException {
		if (!workCollector.isCollecting()) {
			return this;
		}
		title = titleNormalizer.normalize(title);
		final int maxCost = (int) Math.min(Math.ceil(titleMaxCostProportion * title.length()), titleMaxCostMaximum); 
		final List<ScoredPath> matches = titleTrie.search(title, maxCost);
		if (debug) logger.debug("title \"{}\" max cost {} results {}", title, maxCost, matches.size());
		for (ScoredPath match : matches) {
			if (!workCollector.isCollecting()) {
				return this;
			}
			luceneSearch.findTitle(match.path, workCollector);
		}
		return this;
	}

	public TrieSearch searchArtist(Set<String> artists, final WorkCollector workCollector) throws ParseException, IOException {
		for (String artist : artists) {
			if (!workCollector.isCollecting()) {
				return this;
			}
			artist = artistNormalizer.normalize(artist);
			final int maxCost = (int) Math.min(Math.ceil(artistMaxCostProportion * artist.length()), artistMaxCostMaximum); 
			final List<ScoredPath> matches = artistTrie.search(artist, maxCost);
			if (debug) logger.debug("artist \"{}\" max cost {} results {}", artist, maxCost, matches.size());
			for (ScoredPath match : matches) {
				if (!workCollector.isCollecting()) {
					return this;
				}
				luceneSearch.findArtist(match.path, workCollector);
			}
		}
		return this;
	}
	
}