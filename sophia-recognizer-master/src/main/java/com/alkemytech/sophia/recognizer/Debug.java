package com.alkemytech.sophia.recognizer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.Reader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.recognizer.db.MasterDatabase;
import com.alkemytech.sophia.recognizer.score.ScoreResult;
import com.alkemytech.sophia.recognizer.siada.OutputForSiadaReader;
import com.alkemytech.sophia.recognizer.util.Str;
import com.alkemytech.sophia.recognizer.utilization.Utilization;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class Debug {
	
	private static final Logger logger = LoggerFactory.getLogger(Debug.class);

	protected static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args) {
			super(args);
		}

		@Override
		protected void configure() {
			super.configure();
			// singleton(s)
			bind(S3.class).asEagerSingleton();
			bind(MasterDatabase.class).asEagerSingleton();
			bind(NumberSpeller.class).asEagerSingleton();
			bind(Debug.class).asEagerSingleton();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			final Debug instance = Guice.createInjector(new GuiceModuleExtension(args))
				.getInstance(Debug.class)
					.startup();
			try {
				instance.process(args);
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}
	
	@SuppressWarnings("unused")
	private final Properties configuration;
	private final NumberSpeller numberSpeller;
	private final MasterDatabase masterDatabase;
	private final S3 s3;
	
	@Inject
	protected Debug(@Named("configuration") Properties configuration,
			NumberSpeller numberSpeller, MasterDatabase masterDatabase, S3 s3) {
		super();
		this.configuration = configuration;
		this.numberSpeller = numberSpeller;
		this.masterDatabase = masterDatabase;
		this.s3 = s3;
	}
	
	public Debug startup() {
		s3.startup();
		numberSpeller.startup();
		masterDatabase.startup();
		return this;
	}

	public Debug shutdown() {
		masterDatabase.shutdown();
		numberSpeller.shutdown();
		s3.shutdown();
		return this;
	}
	
	public void job1(String[] args) throws Exception {
		
		File folder = new File("/Users/roberto/Development/SIAE-SOPHIA/res/fuzzy/musica/NI 162_Risultati algoritmo/kpi_optimization_round01");
		for (File file : folder.listFiles()) {
			if (file.isFile() && file.getName().endsWith(".txt")) {
				byte[] bin = new byte[(int) file.length()]; 
				try (FileInputStream in = new FileInputStream(file)) {
					in.read(bin);
				}
				String txt = new String(bin, "UTF-8");
				txt = txt.replace(" 6125 ", " 18375 ");
//				txt = txt.replace("numero totale utilizzazioni: 1989", "\nnumero totale utilizzazioni: 1989\n");
				txt = txt.replace("numero totale utilizzazioni: 1989\n\n", "\nnumero totale utilizzazioni: 1989\n");
				txt = txt.replace("max 16))", "max 16)");
				txt = txt.replace("MinWwNoSpLD", "MaxWwNoSpLD");
				try (FileOutputStream out = new FileOutputStream(file)) {
					out.write(txt.getBytes("UTF-8"));
				}
			}
		}
		
	}
	
	public void job2(String[] args) throws Exception {
		final HashSet<String> filenames = new HashSet<>(Arrays.asList(
				"073top10KPIs.txt",
				"081top10KPIs.txt",
				"019top10KPIs.txt",
				"052top10KPIs.txt",
				"092top10KPIs.txt",
				"023top10KPIs.txt",
				"035top10KPIs.txt",
				"003top10KPIs.txt",
				"008top10KPIs.txt",
				"078top10KPIs.txt"
		));
		HashMap<String,HashSet<String>> ranges = new HashMap<>();
		File folder = new File("/Users/roberto/Development/SIAE-SOPHIA/res/fuzzy/musica/NI 162_Risultati algoritmo/kpi_optimization_round01");
		for (File file : folder.listFiles()) {
			if (file.isFile() && filenames.contains(file.getName())) {
				byte[] bin = new byte[(int) file.length()]; 
				try (FileInputStream in = new FileInputStream(file)) {
					in.read(bin);
				}
				String txt = new String(bin, "UTF-8");
				
				StringTokenizer tokenizer = new StringTokenizer(txt, "\n");
				while (tokenizer.hasMoreTokens()) {
					String str = tokenizer.nextToken();
					if (str.startsWith("scoreCalculator.")) {
						int eq = str.indexOf('=');
						if (-1 != eq) {
							String key = str.substring(0, eq);
							String value = str.substring(1 + eq).trim();
							HashSet<String> range = ranges.get(key);
							if (null == range) {
								range = new HashSet<>();
								ranges.put(key, range);
							}
							range.add(value);							
						}
					}					
				}

			}
		}
		
		for (Map.Entry<String,HashSet<String>> entry : ranges.entrySet()) {
			System.out.println(entry.getKey() + "=" + entry.getValue());
		}
	}
	
	public void job3(String[] args) throws Exception {
		
		String s = "TUTTA 1 VITA";
		Pattern p = Pattern.compile("[0-9]+");
		Matcher m = p.matcher(s);
		while (m.find()) {
			String n = m.group();
			s = s.replace(n, Str.spell(Integer.parseInt(n), "it"));
			System.out.println(s);
		}
		
		System.out.println(numberSpeller.spell("TUTTA 1 VITA"));
		System.out.println(numberSpeller.spell("24000 BACI"));
		System.out.println(numberSpeller.spell("24K MAGIC"));
		System.out.println(numberSpeller.spell("I23CAMMELLI"));
		
	}

	public void job4(String[] args) throws Exception {
		
		File file = new File("/Users/roberto/Development/SIAE-SOPHIA/res/recognizer/2017_12_03_0926_output2.csv");
		Reader reader = new BufferedReader(new FileReader(file));

//		CSVParser parser = new CSVParser(reader, CSVFormat.DEFAULT
//				.withIgnoreEmptyLines()
//				.withQuoteMode(QuoteMode.MINIMAL)
//				.withQuote('"')
//				.withIgnoreSurroundingSpaces()
//				.withCommentMarker('#'));
//				
//		String currentCombana = null;
//		int rownum = 0;
//		for (CSVRecord record : parser) {
//			String combana = record.get(3);
//			if (!combana.equals(currentCombana)) {
//				rownum ++;
//				currentCombana = combana;
//			}
//			
//			if (0 == rownum % 100) {
//				System.out.println("rownum " + rownum);
//			}
//		}
//		
//		System.out.println("rownum " + rownum);
			
		OutputForSiadaReader siadaReader = new OutputForSiadaReader(reader, masterDatabase);
		
		int rownum = 0;
		while (siadaReader.next()) {
			Utilization u = null;
			List<ScoreResult> ww = siadaReader.getScoreResults();
			boolean found = false;
			for (ScoreResult w : ww) {
				u = w.utilization;
				if (null != w.work && w.work.code.equals(u.code)) {
					found = true;
					break;
				}
			}
			if (!found) {
				System.out.println(u);
			}
			rownum ++;
//			if (0 == rownum % 100) {
//				System.out.println("rownum " + rownum);
//			}
		}
		
		System.out.println("rownum " + rownum);
		
		siadaReader.close();
		
		reader.close();

	}

	private void job5() throws Exception {
		String url2 = "s3://siae-sophia-codifica/dev/UAT codifica string matching/uat.properties";
		
		String url = url2;
		System.out.println(String.format("\"%s\" %s", new S3.Url(url), s3.doesObjectExist(new S3.Url(url)) ? "exists" : ""));

		url = url2.replace(" ", "+");
		System.out.println(String.format("\"%s\" %s", new S3.Url(url), s3.doesObjectExist(new S3.Url(url)) ? "exists" : ""));

		url = url2.replace(" ", "%20");
		System.out.println(String.format("\"%s\" %s", new S3.Url(url), s3.doesObjectExist(new S3.Url(url)) ? "exists" : ""));
	}
	
	public Debug process(String[] args) throws Exception {
//		job1(args);
//		job2(args);
//		job3(args);
//		job4(args);
		job5();
		return this;
	}

}