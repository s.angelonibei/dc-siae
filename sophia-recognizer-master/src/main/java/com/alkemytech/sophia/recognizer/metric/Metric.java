package com.alkemytech.sophia.recognizer.metric;


/**
 * A Metric to evaluate string similarity
 * 
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public interface Metric {

	/**
	 * Returns a value between 0.0 and 1.0 indicating how much
	 * string <code>a</code> is similar to string <code>b</code>. 
	 * 
	 * @param a a string
	 * @param b another string
	 * @return a similarity value between 0.0 and 1.0
	 */
	public double similarity(String a, String b);
	
	/**
	 * Metric name.
	 * 
	 * @return the metric name
	 */
	public String getName();
	
}
