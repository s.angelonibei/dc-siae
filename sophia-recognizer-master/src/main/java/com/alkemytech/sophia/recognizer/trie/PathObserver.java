package com.alkemytech.sophia.recognizer.trie;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public interface PathObserver {

	public void observe(String path) throws Exception;

}
