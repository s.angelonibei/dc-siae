package com.alkemytech.sophia.recognizer;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StoredField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.recognizer.db.MasterDatabase;
import com.alkemytech.sophia.recognizer.lucene.LuceneAnalyzers;
import com.alkemytech.sophia.recognizer.lucene.LuceneField;
import com.alkemytech.sophia.recognizer.model.Artist;
import com.alkemytech.sophia.recognizer.model.Title;
import com.alkemytech.sophia.recognizer.model.Work;
import com.alkemytech.sophia.recognizer.regex.StringNormalizer;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class LuceneIndexBuilder {

    private static final Logger logger = LoggerFactory.getLogger(LuceneIndexBuilder.class);
    static int exitCode = 0;

    protected static class GuiceModuleExtension extends GuiceModule {

        public GuiceModuleExtension(String[] args) {
            super(args);
        }

        @Override
        protected void configure() {
            super.configure();
            // singleton(s)
            bind(MasterDatabase.class).asEagerSingleton();
            bind(LuceneIndexBuilder.class).asEagerSingleton();
        }

    }

    public static void main(String[] args) {
        try {
            final LuceneIndexBuilder instance = Guice.createInjector(new GuiceModuleExtension(args))
                    .getInstance(LuceneIndexBuilder.class)
                    .startup();
            try {
                instance.process();
            } finally {
                instance.shutdown();
            }
        } catch (Throwable e) {
            logger.error("main", e);
            exitCode = 1;
        } finally {
            System.exit(exitCode);
        }
    }

    private final Properties configuration;
    private final MasterDatabase masterDatabase;
    private final NumberSpeller numberSpeller;
    private final Gson gson;

    @Inject
    protected LuceneIndexBuilder(@Named("configuration") Properties configuration,
                                 Gson gson, MasterDatabase masterDatabase, NumberSpeller numberSpeller) {
        super();
        this.configuration = configuration;
        this.masterDatabase = masterDatabase;
        this.numberSpeller = numberSpeller;
        this.gson = gson;
    }

    public LuceneIndexBuilder startup() {
        if ("true".equalsIgnoreCase(configuration.getProperty("luceneIndexBuilder.locked", "true"))) {
            throw new IllegalStateException("application locked");
        }
        masterDatabase.startup();
        numberSpeller.startup();
        return this;
    }

    public LuceneIndexBuilder shutdown() {
        numberSpeller.shutdown();
        masterDatabase.shutdown();
        return this;
    }

    public LuceneIndexBuilder process() throws IOException {
        try {
            final File homeFolder = new File(configuration
                    .getProperty("luceneIndexBuilder.homeFolder"));
            final File indexFolder = new File(homeFolder, configuration
                    .getProperty("luceneIndexBuilder.indexFolder"));
            final String analyzerName = configuration
                    .getProperty("luceneIndexBuilder.analyzer", "default");
            final boolean append = "true".equalsIgnoreCase(configuration
                    .getProperty("luceneIndexBuilder.append"));
            final boolean storeJson = "true".equalsIgnoreCase(configuration
                    .getProperty("luceneIndexBuilder.storeJson"));
            final HashSet<String> includeTags = new HashSet<>(Arrays.asList(configuration
                    .getProperty("luceneIndexBuilder.includeTags").split(",")));
            final StringNormalizer titleNormalizer = new StringNormalizer(exitCode, configuration
                    .getProperty("luceneIndexBuilder.normalizer.title").split(","));
            final StringNormalizer artistNormalizer = new StringNormalizer(exitCode, configuration
                    .getProperty("luceneIndexBuilder.normalizer.artist").split(","));
            final StringNormalizer nospNormalizer = new StringNormalizer(exitCode, configuration
                    .getProperty("luceneIndexBuilder.normalizer.nosp").split(","));
            final boolean spellTitleNumbers = "true".equalsIgnoreCase(configuration
                    .getProperty("luceneIndexBuilder.spellTitleNumbers"));
            final int heartbeat = Integer.parseInt(configuration
                    .getProperty("luceneIndexBuilder.heartbeat", configuration.getProperty("default.heartbeat", "1000")));
            final int bindPort = Integer.parseInt(configuration
                    .getProperty("luceneIndexBuilder.bindport", configuration.getProperty("default.bindport", "0")));
            final int limit = Integer.parseInt(configuration
                    .getProperty("luceneIndexBuilder.limit", "-1"));

            // bind to lock tcp port
            try (ServerSocket socket = new ServerSocket(bindPort)) {
                try (Directory directory = FSDirectory.open(indexFolder.toPath());
                     Analyzer analyzer = LuceneAnalyzers.get(analyzerName);
                     IndexWriter indexWriter = new IndexWriter(directory, new IndexWriterConfig(analyzer)
                             .setOpenMode(append ? OpenMode.APPEND : OpenMode.CREATE))) {

                    logger.debug("building index from master database");
                    final long startTimeMillis = System.currentTimeMillis();
                    final AtomicInteger rownum = new AtomicInteger(0);
                    final Work.Selector workSelector = new Work.Selector() {

                        @Override
                        public Work.Selector select(Work work) {
                            try {
                                final Document document = new Document();
                                // title(s)
                                int includedTitles = 0;
                                if (includeTags.contains(work.title.tag)) {
                                    String text = titleNormalizer.normalize(work.title.title);
                                    String textNoSp = nospNormalizer.normalize(text);
                                    if (!Strings.isNullOrEmpty(text) && !Strings.isNullOrEmpty(textNoSp)) {
                                        document.add(new TextField(LuceneField.TITLE, text, Field.Store.NO));
                                        document.add(new StringField(LuceneField.TITLE_NO_SPACES, textNoSp, Field.Store.NO));
                                        includedTitles++;
                                    }
                                    if (spellTitleNumbers) { // spell title number(s) if any
                                        for (String spelledTitle : numberSpeller.spell(work.title.title)) {
                                            text = titleNormalizer.normalize(spelledTitle);
                                            textNoSp = nospNormalizer.normalize(text);
                                            if (!Strings.isNullOrEmpty(text) && !Strings.isNullOrEmpty(textNoSp)) {
                                                document.add(new TextField(LuceneField.TITLE, text, Field.Store.NO));
                                                document.add(new StringField(LuceneField.TITLE_NO_SPACES, textNoSp, Field.Store.NO));
                                                includedTitles++;
                                            }
                                        }
                                    }
                                }
                                for (Title title : work.titles) {
                                    if (includeTags.contains(title.tag)) {
                                        String text = titleNormalizer.normalize(title.title);
                                        String textNoSp = nospNormalizer.normalize(text);
                                        if (!Strings.isNullOrEmpty(text) && !Strings.isNullOrEmpty(textNoSp)) {
                                            document.add(new TextField(LuceneField.TITLE, text, Field.Store.NO));
                                            document.add(new StringField(LuceneField.TITLE_NO_SPACES, textNoSp, Field.Store.NO));
                                            includedTitles++;
                                        }
                                        if (spellTitleNumbers) { // spell title number(s) if any
                                            for (String spelledTitle : numberSpeller.spell(title.title)) {
                                                text = titleNormalizer.normalize(spelledTitle);
                                                textNoSp = nospNormalizer.normalize(text);
                                                if (!Strings.isNullOrEmpty(text) && !Strings.isNullOrEmpty(textNoSp)) {
                                                    document.add(new TextField(LuceneField.TITLE, text, Field.Store.NO));
                                                    document.add(new StringField(LuceneField.TITLE_NO_SPACES, textNoSp, Field.Store.NO));
                                                    includedTitles++;
                                                }
                                            }
                                        }
                                    }
                                }
                                // artist(s)
                                int includedArtists = 0;
                                for (Artist artist : work.artists) {
                                    if (includeTags.contains(artist.tag)) {
                                        final String text = artistNormalizer.normalize(artist.name);
                                        final String textNoSp = nospNormalizer.normalize(text);
                                        if (!Strings.isNullOrEmpty(text) && !Strings.isNullOrEmpty(textNoSp)) {
                                            document.add(new TextField(LuceneField.ARTIST, text, Field.Store.NO));
                                            document.add(new StringField(LuceneField.ARTIST_NO_SPACES, textNoSp, Field.Store.NO));
                                            includedArtists++;
                                        }
                                    }
                                }
                                // id
                                document.add(new StoredField(LuceneField.ID, work.id));
                                // json
                                if (storeJson) {
                                    document.add(new StoredField(LuceneField.JSON, work.toJson(gson)));
                                }
                                if (includedTitles > 0 && includedArtists > 0) {
                                    indexWriter.addDocument(document);
                                }
                            } catch (IOException e) {
                                logger.error("error processing work {}", work);
                                logger.error("error processing work", e);
                                exitCode = 1;
                            }
                            // heartbeat
                            if (0 == (rownum.incrementAndGet() % heartbeat)) {
                                logger.debug("{}k", rownum.get() / 1000);
                            }
                            return this;
                        }

                    };
                    if (limit > 0) {
                        masterDatabase.selectAllWorks(workSelector, limit,exitCode);
                    } else {
                        masterDatabase.selectAllWorks(workSelector, exitCode);
                    }
                    logger.debug("{} parsed records", rownum.get());
                    logger.debug("index built in {}s", (System.currentTimeMillis() - startTimeMillis) / 1000L);

                }
            } catch (IOException e) {
                exitCode = 1;
                logger.info("Error :"+e.getMessage());
            }
        } catch (NullPointerException e) {
            exitCode = 1;
            logger.info("Error :"+e.toString());
            e.printStackTrace();
        }
        return this;
    }

}