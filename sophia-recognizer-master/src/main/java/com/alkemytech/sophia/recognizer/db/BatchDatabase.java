package com.alkemytech.sophia.recognizer.db;

import java.io.File;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.recognizer.model.Batch;
import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.persist.EntityCursor;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;
import com.sleepycat.persist.StoreConfig;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class BatchDatabase {

	private static final Logger logger = LoggerFactory.getLogger(BatchDatabase.class);

	private final AtomicInteger startupCalls;
	private final Properties configuration;
	private final File homeFolder;

	private Environment environment;
	private EntityStore store;

	@Inject
	protected BatchDatabase(@Named("configuration") Properties configuration) {
		super();
		this.startupCalls = new AtomicInteger(0);
		this.configuration = configuration;
		this.homeFolder = new File(configuration.getProperty("batchDatabase.homeFolder"));
	}
	
	public BatchDatabase startup() {
		if (0 == startupCalls.getAndIncrement()) {
			try {
				final boolean readonly = isReadonly();
				final File environmentHome = new File(homeFolder,
						configuration.getProperty("batchDatabase.dbFolder", "batchDatabase.bdb"));
				environmentHome.mkdirs();
				environment = new Environment(environmentHome,
						EnvironmentConfig.DEFAULT
							.setAllowCreate(!readonly)
							.setReadOnly(readonly));
				store = new EntityStore(environment, "batch",
					StoreConfig.DEFAULT
						.setAllowCreate(!readonly)
						.setReadOnly(readonly)
						.setDeferredWrite(true)); 
			} catch (DatabaseException e) {
				logger.error("startup", e);
			}
		}
		return this;
	}

	public BatchDatabase shutdown() {
		if (0 == startupCalls.decrementAndGet()) {
			try {
				if (null != store) {
					if (!isReadonly()) {
						store.sync();
					}
					store.close();
				}
			} catch (DatabaseException e) {
				logger.error("shutdown", e);
			}
			try {
				if (null != environment) {
					if (!isReadonly()) {
						environment.sync();
					}
					environment.close();
				}
			} catch (DatabaseException e) {
				logger.error("shutdown", e);
			}
		}
		return this;
	}
	
	public boolean isReadonly() {
		return "true".equalsIgnoreCase(configuration
				.getProperty("batchDatabase.readonly", "true"));
	}

	public BatchDatabase sync() {
		store.sync();
		return this;
	}
	
	public Batch getByUrl(String url) {
		return (null == url ? null :
			store.getPrimaryIndex(String.class, Batch.class).get(url));
	}
	
	private boolean isValid(Batch batch) {
		if (null == batch) {
			return false;
		} else if (Strings.isNullOrEmpty(batch.url)) {
			return false;
		} else if (!batch.url.startsWith("s3://")) {
			return false;
		} else if (null == batch.status) {
			return false;
		}
		return true;
	}

	public Batch replaceIfExists(Batch batch) {
		if (!isValid(batch)) {
			return null;
		}
		final PrimaryIndex<String, Batch> primaryIndex = store
				.getPrimaryIndex(String.class, Batch.class);
		final Batch existingBatch = getByUrl(batch.url);
		if (null != existingBatch) {
			batch.lastUpdate = System.currentTimeMillis();
			primaryIndex.putNoReturn(batch);
		}
		return existingBatch;
	}

	public boolean replaceAlways(Batch batch) {
		if (!isValid(batch)) {
			logger.debug("batch not valid {}", batch);
			return false;
		}
		final PrimaryIndex<String, Batch> primaryIndex = store
				.getPrimaryIndex(String.class, Batch.class);
		batch.lastUpdate = System.currentTimeMillis();
		primaryIndex.putNoReturn(batch);
		return true;
	}
	
//	public Batch insertOrReplace(Batch batch) {
//		if (!isValid(batch)) {
//			return null;
//		}
//		final PrimaryIndex<String, Batch> primaryIndex = store
//				.getPrimaryIndex(String.class, Batch.class);
//		final Batch existingBatch = getByUrl(batch.url);
//		if (null == existingBatch) {
//			batch.lastUpdate = System.currentTimeMillis();
//			primaryIndex.putNoReturn(batch);
//			return batch;
//		} else {
//			batch.lastUpdate = System.currentTimeMillis();
//			primaryIndex.putNoReturn(batch);
//			return existingBatch;
//		}
//	}

	public boolean insertNoOverwrite(Batch batch) {
		if (!isValid(batch)) {
			logger.debug("batch not valid {}", batch);
			return false;
		}
		final PrimaryIndex<String, Batch> primaryIndex = store
				.getPrimaryIndex(String.class, Batch.class);
		batch.lastUpdate = System.currentTimeMillis();
		return primaryIndex.putNoOverwrite(batch);
	}

	public boolean deleteByUrl(String url) {
		final PrimaryIndex<String, Batch> primaryIndex = store
				.getPrimaryIndex(String.class, Batch.class);
		return primaryIndex.delete(url);
	}
	
	public BatchDatabase selectAllBatches(Batch.Selector selector) {
		final PrimaryIndex<String, Batch> primaryIndex = store
				.getPrimaryIndex(String.class, Batch.class);
		try (EntityCursor<Batch> batches = primaryIndex.entities()) {
			for (Batch batch = batches.first(); null != batch; batch = batches.next()) {
				selector.select(batch);
			}
		}
		return this;
	}

	public BatchDatabase selectAllBatches(Batch.Selector selector, int limit) {
		final PrimaryIndex<String, Batch> primaryIndex = store
				.getPrimaryIndex(String.class, Batch.class);
		try (EntityCursor<Batch> batches = primaryIndex.entities()) {
			for (Batch batch = batches.first(); null != batch && limit > 0; batch = batches.next(), limit --) {
				selector.select(batch);
			}
		}
		return this;
	}

}
