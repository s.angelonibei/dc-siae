package com.alkemytech.sophia.recognizer.lucene;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public interface LuceneField {

	public static final String ID = "id";
	public static final String TITLE = "title";
	public static final String TITLE_NO_SPACES = "title_nosp";
	public static final String ARTIST = "artist";
	public static final String ARTIST_NO_SPACES = "artist_nosp";
	public static final String JSON = "json";
	
}
