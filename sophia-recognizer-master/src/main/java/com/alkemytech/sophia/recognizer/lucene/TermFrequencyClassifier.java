package com.alkemytech.sophia.recognizer.lucene;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public interface TermFrequencyClassifier {

	public boolean isFrequent(String term);
	
}
