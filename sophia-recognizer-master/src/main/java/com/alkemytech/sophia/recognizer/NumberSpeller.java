package com.alkemytech.sophia.recognizer;

import java.util.HashSet;
import java.util.Locale;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.alkemytech.sophia.recognizer.util.Str;
import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class NumberSpeller {
	
	private final Pattern pattern;	
	private final Locale[] locales;
	
	@Inject
	protected NumberSpeller(@Named("configuration") Properties configuration) {
		super();
		this.pattern = Pattern.compile(configuration.getProperty("numberSpeller.regex"));
		String[] languages = configuration.getProperty("numberSpeller.languages").split(",");
		this.locales = new Locale[languages.length];
		for (int i = 0; i < languages.length; i ++) {
			this.locales[i] = Locale.forLanguageTag(languages[i]);
		}		
	}
	
	public NumberSpeller startup() {
		return this;
	}

	public NumberSpeller shutdown() {
		return this;
	}
	
	public Set<String> spell(String string) {
		return spell(string, locales);
	}

	public Set<String> spell(String string, Locale...locales) {
		final HashSet<String> numbers = new HashSet<>();
		final Matcher matcher = pattern.matcher(string);
		while (matcher.find()) {
			numbers.add(matcher.group());
		}
		final HashSet<String> results = new HashSet<>();
		if (!numbers.isEmpty()) {
			for (Locale locale : locales) {
				String result = string;
				for (String number : numbers) {
					try {
						final int n = Integer.parseInt(number);
						if (n >= 1800 && n < 2000 &&
								(locale == Locale.US || locale == Locale.UK)) {	
							final String hi = number.substring(0, 2);
							result = result.replace(hi,
									Str.spell(Integer.parseInt(hi), locale));
							final String lo = number.substring(2);
							result = result.replace(lo,
									Str.spell(Integer.parseInt(lo), locale));
						} else {
							result = result.replace(number,
									Str.spell(Integer.parseInt(number), locale));
						}
					} catch (NumberFormatException e) { }

				}
				if (!Strings.isNullOrEmpty(result)) {
					results.add(result);
				}
			}
		}
		return results;
	}

}
