package com.alkemytech.sophia.recognizer.score;

import com.alkemytech.sophia.recognizer.model.ScoredArtist;
import com.alkemytech.sophia.recognizer.model.ScoredTitle;
import com.alkemytech.sophia.recognizer.model.Work;
import com.alkemytech.sophia.recognizer.utilization.Utilization;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class ScoreResult {
	
	public final Utilization utilization;
	public final Work work;
	public final ScoredTitle scoredTitle;
	public final ScoredArtist scoredArtist;
	public final ScoredArtist scoredAuthor;
	public final ScoredArtist scoredComposer;
	public final ScoredArtist scoredPerformer;
	public final double score;
	
	public ScoreResult(Utilization utilization, Work work, ScoredTitle scoredTitle, ScoredArtist scoredArtist, ScoredArtist scoredAuthor, ScoredArtist scoredComposer, ScoredArtist scoredPerformer, double score) {
		super();
		this.utilization = utilization;
		this.work = work;
		this.scoredTitle = scoredTitle;
		this.scoredArtist = scoredArtist;
		this.scoredAuthor = scoredAuthor;
		this.scoredComposer = scoredComposer;
		this.scoredPerformer = scoredPerformer;
		this.score = score;
	}
	
	public ScoreResult(Utilization utilization, Work work, double score) {
		super();
		this.utilization = utilization;
		this.work = work;
		this.scoredTitle = null;
		this.scoredArtist = null;
		this.scoredAuthor = null;
		this.scoredComposer = null;
		this.scoredPerformer = null;
		this.score = score;
	}

}
