#!/bin/sh

HOME=/home/sualkemy/goal-dev

JPACKAGE=com.alkemytech.sophia.royalties
JCLASS=UlisseIpiCsv
JCONFIG=ulisse-ipi-csv.properties
#JSTDOUT=$HOME/ulisse-ipi-csv.out
JSTDOUT=/dev/null
JOPTIONS=-Xmx4G
JVERSION=1.0.1

#cd $HOME

nohup java -cp "$HOME/sophia-royalties-$JVERSION.jar:$HOME/lib/*" $JOPTIONS $JPACKAGE.$JCLASS $HOME/$JCONFIG 2>&1 >> $JSTDOUT &


