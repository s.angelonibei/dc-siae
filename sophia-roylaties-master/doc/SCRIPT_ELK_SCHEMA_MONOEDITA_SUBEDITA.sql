SELECT 
  ID_SCHEMA_RIPARTO, 
  (	CASE 
		WHEN SUM_EO = 2  AND SUM_SE = 0   THEN 1 
		WHEN SUM_EO > 2  AND SUM_SE = 0   THEN 0 
		WHEN SUM_EO >= 2  AND SUM_SE >= 2 THEN 0
		WHEN SUM_EO = 0  AND SUM_SE >= 2  THEN 0 
		WHEN SUM_EO = 0  AND SUM_SE = 0   THEN 0 
	END) AS MONOEDITA,
	(CASE
		WHEN SUM_EO = 2  AND SUM_SE = 0   THEN 'E' 
		WHEN SUM_EO > 2  AND SUM_SE = 0   THEN 'E' 
		WHEN SUM_EO >= 2  AND SUM_SE >= 2 THEN 'T'
		WHEN SUM_EO = 0  AND SUM_SE >= 2  THEN 'S' 
		WHEN SUM_EO = 0  AND SUM_SE = 0   THEN 'N' 
	END) AS EDITA_SUBEDITA
	FROM (
			SELECT ID_SCHEMA_RIPARTO, SUM(EO) AS SUM_EO, SUM(SE) AS SUM_SE
			FROM (
				SELECT 
					qr.ID_SCHEMA_RIPARTO,
					COUNT(CASE WHEN qr.TIPO_QUALIFICA = 'EO' then 1 ELSE NULL END) as "EO",
					COUNT(CASE WHEN qr.TIPO_QUALIFICA = 'SE' then 1 ELSE NULL END) as "SE"
				from quota_riparto qr
				join elk_works_export ewe on ewe.id_schema = qr.id_schema_riparto
				where (qr.tipo_quota = 'DEM' or qr.tipo_quota = 'DRM') 
				and qr.tipo_qualifica is not null
				group by qr.id_schema_riparto, qr.TIPO_QUALIFICA
				order by qr.tipo_qualifica desc
      )
  
  GROUP BY ID_SCHEMA_RIPARTO);
