-- CREATE TABLE
CREATE TABLE ULISSE_ADM.ELK_SCHEMA_TERR_EXPORT
(
		"ID_SCHEMA" NUMBER(19,0),
		"ID_OPERA" NUMBER(19,0),
		"TIPO_UTILIZZO" VARCHAR2(255 CHAR),
		"SIGLA_SCHEMA" VARCHAR2(255 CHAR),
		CONSTRAINT "PK_SCHEMA_TERR" PRIMARY KEY ("ID_SCHEMA")
)
TABLESPACE "DATI_ULIADM" ;

GRANT SELECT ON ULISSE_ADM.ELK_SCHEMA_TERR_EXPORT TO ULISSE_SELECTDATA;
GRANT INSERT ON ULISSE_ADM.ELK_SCHEMA_TERR_EXPORT TO ULISSE_MODIFYDATA;
GRANT UPDATE ON ULISSE_ADM.ELK_SCHEMA_TERR_EXPORT TO ULISSE_MODIFYDATA;
GRANT DELETE ON ULISSE_ADM.ELK_SCHEMA_TERR_EXPORT TO ULISSE_DELETEDATA;

CREATE public SYNONYM ELK_SCHEMA_TERR_EXPORT FOR ULISSE_ADM.ELK_SCHEMA_TERR_EXPORT;
GRANT SELECT ON ELK_SCHEMA_TERR_EXPORT TO ULISSE_SELECTDATA;

-- LOAD TABLE (da lanciare ogni volta che si intende ricreare l'indice)
INSERT INTO ELK_SCHEMA_TERR_EXPORT (ID_SCHEMA, ID_OPERA, TIPO_UTILIZZO, SIGLA_SCHEMA)
select sr.id_schema_riparto as ID_SCHEMA, os.id_anag_opera as ID_OPERA, sr.tipo_utilizzo as TIPO_UTILIZZO,
       LISTAGG( (case when st.operatore_logico = 'I' then '+' else '-' end)||t.tis_a, '' )within group (order by (case when st.operatore_logico = 'I' then '+' else '-' end)||t.tis_a  ) as SIGLA_SCHEMA
from schema_riparto sr
       join opera_schema os  on os.id_schema_riparto = sr.id_schema_riparto
       join schema_territorio st on sr.id_schema_riparto = st.id_schema_riparto
       join territorio t on t.tis_n = st.id_territorio
where os.data_tecnica_a > sysdate
  and os.data_validita_a > sysdate
  and sr.id_repertorio = 1
group by sr.id_schema_riparto, os.id_anag_opera, sr.tipo_utilizzo;
COMMIT;




-- JOIN SCRIPT
select
(co.codice || '_' || este.id_schema) as ID_SCHEMA_FLAT,
co.codice as CODICE_OPERA,
to_char(ao.DATA_DICHIARAZIONE, 'dd-MM-yyyy') as DATA_DICHIARAZIONE,
ao.data_dichiarazione_siada as DATA_DICHIARAZIONE_SIADA,
to_char(ao.DATA_ACQUISIZIONE, 'dd-MM-yyyy') as DATA_ACQUISIZIONE,
to_char(ao.data_pubblico_dominio, 'dd-MM-yyyy') as DATA_PUBBLICO_DOMINIO,
ao.modalita_deposito as TIPOLOGIA_DEPOSITO,
ao.musica as MUSICA,
ao.testo as TESTO,
ao.esemplare as ESEMPLARE,
go.nome as GENERE_OPERA,
ao.durata_effettiva as DURATA,
ao.notizie_particolari as NOTIZIE_PARTICOLARI,
ao.sigla_bollettino as TIPO_BOLLETTINO,
ao.cod_provv_editore as COD_PROVV_EDITORE,
ao.riferimento as RIFERIMENTO,
ao.FLAG_PUBBLICO_DOMINIO as FLAG_PD,
ao.FLAG_ELABORAZIONE_MUSICA as FLAG_EL,
ao.FLAG_MAGGIORAZIONE_CL_I as FLAG_MAGGIORAZIONE,
ao.FLAG_SEMPREVERDE as FLAG_SEMPREVERDI,
ao.FLAG_ACCETTATA_CONFUSORIA as FLAG_ACCETTATA_CONFUSORIA,
nop.descrizione as AMBITO,
so.descrizione as STATO_OPERA,
ao.N_PACCO as NPACCO,
rilevanza_opera.rilevanza AS RILEVANZA ,
maturato_opera.maturato AS MATURATO,
maturato_opera.rischio AS RISCHIO,
pr.periodo as SEMESTRE_SCADENZA,
to_char(pr.data_a, 'dd-MM-YYYY') as DATA_SEM_SCADENZA,
pr1.periodo as SEMESTRE_DICHIARAZIONE,
to_char(pr1.data_a, 'dd-MM-YYYY') as DATA_SEM_DICHIARAZIONE,
pr2.periodo as SEMESTRE_VALIDITA,
to_char(pr2.data_a, 'dd-MM-YYYY') as DATA_SEM_VALIDITA,
este.id_schema as ID_SCHEMA,
ao.id_anag_opera as ID_OPERA,
este.tipo_utilizzo as TIPO_UTILIZZO,
este.sigla_schema as TERRITORIO
from anag_opera ao
inner join elk_schema_terr_export este on este.id_opera=ao.id_anag_opera
inner join codice_opera co on ao.id_anag_opera = co.ID_ANAG_OPERA
left join genere_opera go on ao.id_genere_opera = go.id_genere_opera
left join stato_opera so on ao.id_stato_opera = so.id_stato_opera
left join nazionalita_opera nop on ao.ID_NAZIONALITA_OPERA = nop.ID_NAZIONALITA_OPERA
left join periodo_ripartizione pr on ao.id_semestre_scadenza = pr.id_periodo_ripartizione
left join periodo_ripartizione pr1 on ao.id_semestre_dichiarazione = pr1.id_periodo_ripartizione
left join periodo_ripartizione pr2 on ao.id_semestre_validita = pr2.id_periodo_ripartizione
left join (
            select mat.valore_maturato maturato, mat.id_anag_opera id_opera, mat.rischio rischio
            from dwh_maturato_opera mat
            inner join (select max(mat.id_maturato_opera) id, mat.id_anag_opera id_opera from dwh_maturato_opera mat group by mat.id_anag_opera) mat_interna on mat_interna.id = mat.id_maturato_opera
          ) maturato_opera on ao.id_anag_opera =maturato_opera.id_opera
left join (
            select ril.valore_rilevanza rilevanza, ril.id_anag_opera id_opera
            from dwh_rilevanza_opera ril
            inner join ( select max(ril.id_rilevanza_opera) id, ril.id_anag_opera id_opera from dwh_rilevanza_opera ril group by ril.id_anag_opera ) ril_interna on ril_interna.id = ril.id_rilevanza_opera
          ) rilevanza_opera on ao.id_anag_opera =rilevanza_opera.id_opera
where
ao.ID_ANAG_OPERA_STORICO is null
and ao.DATA_TECNICA_A > sysdate
and co.DATA_TECNICA_A > sysdate
and co.ID_TIPO_CODICE = 1
and ao.FORM_OPERA = 'B';