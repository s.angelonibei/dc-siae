-- LOAD TABLE
INSERT INTO ELK_SCHEMA_TERR_EXPORT (ID_SCHEMA, ID_OPERA, TIPO_UTILIZZO, SIGLA_SCHEMA)
select sr.id_schema_riparto as ID_SCHEMA, os.id_anag_opera as ID_OPERA, sr.tipo_utilizzo as TIPO_UTILIZZO,
       LISTAGG( (case when st.operatore_logico = 'I' then '+' else '-' end)||t.tis_a, '' )within group (order by (case when st.operatore_logico = 'I' then '+' else '-' end)||t.tis_a  ) as SIGLA_SCHEMA
from schema_riparto sr
       join opera_schema os  on os.id_schema_riparto = sr.id_schema_riparto
       join schema_territorio st on sr.id_schema_riparto = st.id_schema_riparto
       join territorio t on t.tis_n = st.id_territorio
where os.data_tecnica_a > sysdate
  and os.data_validita_a > sysdate
  and sr.id_repertorio = 1
group by sr.id_schema_riparto, os.id_anag_opera, sr.tipo_utilizzo;
COMMIT;


insert into MAPPA_QUOTA_IPI (ID, NOME, DESCRIZIONE, NOME_IPI)  VALUES (1 , 'CE', 'COMPOSITORE ELABORATO', 'MC' );
insert into MAPPA_QUOTA_IPI (ID, NOME, DESCRIZIONE, NOME_IPI)  VALUES (2 , 'CO', 'COMPOSITORE ORIGINALE', 'MC' );
insert into MAPPA_QUOTA_IPI (ID, NOME, DESCRIZIONE, NOME_IPI)  VALUES (3 , 'SR', 'SUB ARRANGIATORE', 'MC' );
insert into MAPPA_QUOTA_IPI (ID, NOME, DESCRIZIONE, NOME_IPI)  VALUES (4 , 'CA', 'COMPOSITORE - AUTORE (PER OPERE STRANIERE)', 'MC' );
insert into MAPPA_QUOTA_IPI (ID, NOME, DESCRIZIONE, NOME_IPI)  VALUES (5 , 'EL', 'ELABORATORE OPERE PD', 'MC' );
insert into MAPPA_QUOTA_IPI (ID, NOME, DESCRIZIONE, NOME_IPI)  VALUES (6 , 'AO', 'AUTORE ORIGINALE', 'LY' );
insert into MAPPA_QUOTA_IPI (ID, NOME, DESCRIZIONE, NOME_IPI)  VALUES (7 , 'TR', 'TRADUTTORE', 'LY' );
insert into MAPPA_QUOTA_IPI (ID, NOME, DESCRIZIONE, NOME_IPI)  VALUES (8 , 'AS', 'SUB AUTORE OPERE ITALIANE', 'LY' );
insert into MAPPA_QUOTA_IPI (ID, NOME, DESCRIZIONE, NOME_IPI)  VALUES (9 , 'AD', 'ADATTATORE OPERE', 'LY' );
insert into MAPPA_QUOTA_IPI (ID, NOME, DESCRIZIONE, NOME_IPI)  VALUES (10, 'AR', 'ARRANGIATORE OPERE TUTELATE', 'MC' );
insert into MAPPA_QUOTA_IPI (ID, NOME, DESCRIZIONE, NOME_IPI)  VALUES (11, 'SA', 'SUB AUTORE', 'LY' );
insert into MAPPA_QUOTA_IPI (ID, NOME, DESCRIZIONE, NOME_IPI)  VALUES (12, 'PE', 'ESECUTORE ARTISTICO', 'EM' );
insert into MAPPA_QUOTA_IPI (ID, NOME, DESCRIZIONE, NOME_IPI)  VALUES (13, 'EO', 'EDITORE ORIGINALE', 'EM' );
insert into MAPPA_QUOTA_IPI (ID, NOME, DESCRIZIONE, NOME_IPI)  VALUES (14, 'PA', 'EDITORE PARTECIPANTE', 'EM' );
insert into MAPPA_QUOTA_IPI (ID, NOME, DESCRIZIONE, NOME_IPI)  VALUES (15, 'AQ', 'EDITORE ACQUIRENTE', 'EM' );
insert into MAPPA_QUOTA_IPI (ID, NOME, DESCRIZIONE, NOME_IPI)  VALUES (16, 'AM', 'AMMINISTRATORE', 'EM' );
insert into MAPPA_QUOTA_IPI (ID, NOME, DESCRIZIONE, NOME_IPI)  VALUES (17, 'EC', 'EDITORE CEDENTE (TRAMITE)', 'EM' );
insert into MAPPA_QUOTA_IPI (ID, NOME, DESCRIZIONE, NOME_IPI)  VALUES (18, 'EA', 'EDITORE AGGIUNTO PER OPERE ITALIANE', 'EM' );
insert into MAPPA_QUOTA_IPI (ID, NOME, DESCRIZIONE, NOME_IPI)  VALUES (19, 'AP', 'AGENTE DI PROPAGANDA', 'EM' );
insert into MAPPA_QUOTA_IPI (ID, NOME, DESCRIZIONE, NOME_IPI)  VALUES (20, 'SE', 'SUB EDITORE', 'EM' );
insert into MAPPA_QUOTA_IPI (ID, NOME, DESCRIZIONE, NOME_IPI)  VALUES (21, 'CS', 'CONTO SPECIALE', 'MC' );