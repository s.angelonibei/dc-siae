CREATE TABLE ULISSE_ADM.ELK_SCHEMA_TERR_EXPORT
(
"ID_SCHEMA" NUMBER(19,0),
"TIPO_UTILIZZO" VARCHAR2(255 CHAR),
"SIGLA_SCHEMA" VARCHAR2(255 CHAR),
CONSTRAINT "PK_SCHEMA_TERR" PRIMARY KEY ("ID_SCHEMA")
)TABLESPACE "DATI_ULIADM" ;

--CREAZIONE TABELLE
CREATE TABLE ULISSE_ADM.ELK_WORKS_EXPORT
	(
		"ID_WORKS_EXPORT" NUMBER(19,0) NOT NULL ENABLE, 
		"ID_SCHEMA_FLAT" VARCHAR2(255 CHAR), 
		"CODICE_OPERA" VARCHAR2(20 CHAR), 
		"FLAG_PD" NUMBER(1,0),		
		"FLAG_EL" NUMBER(1,0), 
		"FLAG_MAGGIORAZIONE" NUMBER(1,0), 
		"FLAG_SEMPREVERDI" NUMBER(1,0), 
		"AMBITO" VARCHAR2(20 CHAR),
		"STATOOPERA" VARCHAR2(100 CHAR), 
		"NPACCO" VARCHAR2(20 CHAR), 
		"RILEVANZA" NUMBER(10,10), 
		"MATURATO" NUMBER(6,3), 
		"DATA_DICHIARAZIONE" VARCHAR2(20 CHAR),
		"DATA_SCADENZA" VARCHAR2(20 CHAR),
		"ID_SCHEMA" NUMBER(19,0), 
		"ID_OPERA" NUMBER(19,0), 
		"TITOLO_OT" VARCHAR2(400 CHAR), 
		"TIPO_UTILIZZO" VARCHAR2(255 CHAR),
		"TERRITORIO" VARCHAR2(255 CHAR), 
	 	CONSTRAINT "PK_WORKS_EXPORT" PRIMARY KEY ("ID_WORKS_EXPORT")   
)TABLESPACE "DATI_ULIADM" ;

CREATE SEQUENCE ULISSE_ADM.SEQ_ELK_WORKS_EXPORT INCREMENT BY 1 MAXVALUE 9999999999999999999999999999 MINVALUE 1 CACHE 20;commit;

CREATE INDEX ELK_WORKS_EXPORT_ID_OP_IDX ON ELK_WORKS_EXPORT (ID_OPERA);
CREATE INDEX ELK_WORKS_EXPORT_ID_SC_IDX ON ELK_WORKS_EXPORT (ID_SCHEMA);


-- LOAD TABELLE 
INSERT INTO ULISSE_ADM.ELK_SCHEMA_TERR_EXPORT (ID_SCHEMA, TIPO_UTILIZZO, SIGLA_SCHEMA)
select sr.id_schema_riparto as ID_SCHEMA, sr.tipo_utilizzo as TIPO_UTILIZZO,
		LISTAGG( (case when st.operatore_logico = 'I' then '+' else '-' end)||t.tis_a, '' ) within group (order by st.SEQUENZA) as SIGLA_SCHEMA
    from schema_territorio st
    join territorio t on t.tis_n = st.id_territorio
    join schema_riparto sr on sr.id_schema_riparto = st.id_schema_riparto
    join opera_schema os on os.id_schema_riparto = sr.id_schema_riparto    
    where os.data_tecnica_a > sysdate
    and os.data_validita_a > sysdate
    group by sr.id_schema_riparto, sr.tipo_utilizzo;



INSERT INTO ULISSE_ADM.ELK_WORKS_EXPORT
(
ID_SCHEMA_FLAT, 
CODICE_OPERA,
FLAG_PD,
FLAG_EL,
FLAG_MAGGIORAZIONE,
FLAG_SEMPREVERDI,
AMBITO,
STATOOPERA,
NPACCO,
RILEVANZA,
MATURATO,
DATA_DICHIARAZIONE,
DATA_SCADENZA,
ID_SCHEMA,
ID_OPERA,
TIPO_UTILIZZO,
TERRITORIO
)
select 
(co.codice || '_' || terr.id_schema) as ID_SCHEMA_FLAT,
co.codice as CODICE_OPERA,
ao.FLAG_PUBBLICO_DOMINIO as FLAG_PD,
ao.FLAG_ELABORAZIONE_MUSICA as FLAG_EL,
ao.FLAG_MAGGIORAZIONE_CL_I as FLAG_MAGGIORAZIONE,
ao.FLAG_SEMPREVERDE as FLAG_SEMPREVERDI,
nop.descrizione as AMBITO,
so.descrizione as STATOOPERA,
ao.N_PACCO as NPACCO,
DRO.VALORE_RILEVANZA AS RILEVANZA ,
DMO.VALORE_MATURATO AS MATURATO,
to_char(ao.DATA_DICHIARAZIONE, 'dd-MM-yyyy') as DATA_DICHIARAZIONE,
to_char(pr.data_a, 'dd-MM-YYYY') as DATA_SCADENZA,
terr.id_schema as ID_SCHEMA,
ao.id_anag_opera as ID_OPERA,
terr.tipo_utilizzo as TIPO_UTILIZZO,
terr.sigla_schema as TERRITORIO
from anag_opera ao
join codice_opera co on ao.id_anag_opera = co.ID_ANAG_OPERA
join stato_opera so on so.ID_STATO_OPERA = ao.ID_STATO_OPERA
join opera_schema os on os.id_anag_opera = ao.id_anag_opera
join ULISSE_ADM.ELK_SCHEMA_TERR_EXPORT terr on terr.id_schema = os.id_schema_riparto
join nazionalita_opera nop on nop.ID_NAZIONALITA_OPERA = ao.ID_NAZIONALITA_OPERA
left join DWH_MATURATO_OPERA DMO on DMO.ID_ANAG_OPERA = ao.id_anag_opera
left join DWH_RILEVANZA_OPERA DRO on DRO.ID_ANAG_OPERA = ao.id_anag_opera
left join periodo_ripartizione pr on pr.id_periodo_ripartizione = ao.id_semestre_scadenza
where ao.ID_ANAG_OPERA_STORICO is null
and ao.FORM_OPERA = 'B'
and ao.DATA_TECNICA_A > sysdate
and co.DATA_TECNICA_A > sysdate		
and os.data_validita_a > sysdate
and os.DATA_TECNICA_A > sysdate
and co.ID_TIPO_CODICE = 1;


