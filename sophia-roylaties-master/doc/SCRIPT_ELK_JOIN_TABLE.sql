-- INTERPRETI
SELECT
(CASE
	WHEN ap.nominativo IS NULL
	THEN ''
	ELSE ap.nominativo
END ) AS interprete,
(CASE
   WHEN po.provenienza IS NULL
   THEN ''
   ELSE po.provenienza
END ) AS TIPO
FROM partecipante_opera po
JOIN anag_partecipante ap ON ap.id_anag_partecipante = po.id_anag_partecipante
JOIN ELK_WORKS_EXPORT uev ON uev.ID_OPERA = po.ID_ANAG_OPERA
WHERE po.tipo_partecipante   = 'Performer';

-- TITOLI ORIGINALI
SELECT 
(CASE
	WHEN top.titolo IS NOT NULL
	THEN REPLACE(top.titolo,'','')
	ELSE ''
 END)
FROM titolo_opera top
JOIN ELK_WORKS_EXPORT uev ON uev.ID_OPERA = top.ID_ANAG_OPERA
WHERE top.TIPO_TITOLO     = 'OT'
AND top.DATA_TECNICA_A  > sysdate;

-- TITOLI ALTERNATIVI
SELECT 
(CASE
	WHEN top.titolo IS NOT NULL
	THEN REPLACE(top.titolo,'','')
	ELSE ''
 END)
FROM titolo_opera top
JOIN ELK_WORKS_EXPORT uev ON uev.ID_OPERA = top.ID_ANAG_OPERA
WHERE top.TIPO_TITOLO     = 'AT'
AND top.DATA_TECNICA_A  > sysdate;

-- ISWC
SELECT
(CASE
	WHEN codice IS NOT NULL
    THEN codice
    ELSE ''
END)
FROM codice_opera co3
JOIN ELK_WORKS_EXPORT uev ON uev.ID_OPERA = co3.id_anag_opera
WHERE co3.ID_TIPO_CODICE  = 3
AND co3.DATA_TECNICA_A  > sysdate;

-- CODICE REPERTORIO           
SELECT
(CASE
	WHEN codice IS NOT NULL
    THEN codice
    ELSE ''
END)
FROM codice_opera co2
JOIN ELK_WORKS_EXPORT uev ON uev.ID_OPERA = co2.id_anag_opera
WHERE co2.ID_TIPO_CODICE  = 2
AND co2.DATA_TECNICA_A  > sysdate;

-- ISRC
SELECT
(CASE
	WHEN codice IS NOT NULL
    THEN codice
    ELSE ''
END)
FROM codice_opera co4
JOIN ELK_WORKS_EXPORT uev ON uev.ID_OPERA = co4.id_anag_opera
AND co4.ID_TIPO_CODICE  = 15
AND co4.DATA_TECNICA_A  > sysdate;

-- QUOTE RIPARTO
SELECT 
	concat ( qr.cognome, qr.NOME) AS NOMINATIVO,	
	qr.POSIZIONE_SIAE,
    qr.CODICE_IPI,
    qr.CODICE_CONTO,
    qr.CODICE_CONTO_ESTERO,
    qr.PSEUDONIMO,
    REPLACE(qr.TIPO_QUALIFICA,'',''),
    qr.TIPO_QUOTA,
    (CASE
		WHEN qr.tipo_quota = 'DRM'
        THEN qr.quota_percentuale
        WHEN (qr.QUOTA_DENOMINATORE > 0)
        THEN qr.QUOTA_NUMERATORE / qr.QUOTA_DENOMINATORE * 100
        ELSE qr.quota_percentuale
	END) AS QUOTA_AD
    FROM quota_riparto qr
	JOIN ELK_WORKS_EXPORT uev ON uev.ID_SCHEMA = qr.ID_SCHEMA_RIPARTO;
    