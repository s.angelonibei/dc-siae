package com.alkemytech.sophia.royalties.nosql;

import com.alkemytech.sophia.commons.mmap.MemoryMapException;
import com.alkemytech.sophia.commons.mmap.ObjectCodec;

import java.nio.charset.Charset;

/**
 * Created by Alessandro Russo on 25/01/2019.
 */
public class AnagraficaCodec extends ObjectCodec<Anagrafica> {

    private final Charset charset;

    public AnagraficaCodec(Charset charset) {
        super();
        this.charset = charset;
    }

    @Override
    public Anagrafica bytesToObject(byte[] bytes) throws MemoryMapException {
        beginDecoding(bytes);
        final long idSchema = getPackedLong();
        final Anagrafica anagrafica = new Anagrafica( idSchema );
        anagrafica.idOpera = getString(charset);
        anagrafica.idSchemaFlat = getString(charset);
        anagrafica.codiceOpera = getString(charset);
        anagrafica.dataDichiarazione = getString(charset);
        anagrafica.dataDichiarazioneSiada = getString(charset);
        anagrafica.dataAcquisizione = getString(charset);
        anagrafica.dataPubblicoDominio = getString(charset);
        anagrafica.tipologiaDeposito = getString(charset);
        anagrafica.musica = getString(charset);
        anagrafica.testo = getString(charset);
        anagrafica.esemplare = getString(charset);
        anagrafica.genereOpera = getString(charset);
        anagrafica.durata = getString(charset);
        anagrafica.notizieParticolari = getString(charset);
        anagrafica.tipoBollettino = getString(charset);
        anagrafica.codProvvEditore = getString(charset);
        anagrafica.riferimento = getString(charset);
        anagrafica.flagPD = getString(charset);
        anagrafica.flagEL = getString(charset);
        anagrafica.flagSempreverdi = getString(charset);
        anagrafica.flagMaggiorazione = getString(charset);
        anagrafica.flagAccettataConfusoria = getString(charset);
        anagrafica.ambito = getString(charset);
        anagrafica.statoOpera = getString(charset);
        anagrafica.nPacco = getString(charset);
        anagrafica.inviante = getString(charset);
        anagrafica.rilevanza = getString(charset);
        anagrafica.maturato = getString(charset);
        anagrafica.rischio = getString(charset);
        anagrafica.semestreScadenza = getString(charset);
        anagrafica.dataSemScadenza = getString(charset);
        anagrafica.semestreDichiarazione = getString(charset);
        anagrafica.dataSemDichiarazione = getString(charset);
        anagrafica.semestreValidita = getString(charset);
        anagrafica.dataSemValidita = getString(charset);
        anagrafica.tipoUtilizzo = getString(charset);
        anagrafica.territorio = getString(charset);
        anagrafica.ricercabile = getString(charset);
        anagrafica.idCollectingCompany = getString(charset);

        return anagrafica;
    }

    @Override
    public byte[] objectToBytes(Anagrafica anagrafica) throws MemoryMapException {
        beginEncoding();
        putPackedLong( anagrafica.idSchema );
        putString(anagrafica.idOpera, charset);
        putString(anagrafica.idSchemaFlat, charset);
        putString(anagrafica.codiceOpera, charset);
        putString(anagrafica.dataDichiarazione, charset);
        putString(anagrafica.dataDichiarazioneSiada, charset);
        putString(anagrafica.dataAcquisizione, charset);
        putString(anagrafica.dataPubblicoDominio, charset);
        putString(anagrafica.tipoBollettino, charset);
        putString(anagrafica.musica, charset);
        putString(anagrafica.testo, charset);
        putString(anagrafica.esemplare, charset);
        putString(anagrafica.genereOpera, charset);
        putString(anagrafica.durata, charset);
        putString(anagrafica.notizieParticolari, charset);
        putString(anagrafica.tipoBollettino, charset);
        putString(anagrafica.codProvvEditore, charset);
        putString(anagrafica.riferimento, charset);
        putString(anagrafica.flagPD, charset);
        putString(anagrafica.flagEL, charset);
        putString(anagrafica.flagSempreverdi, charset);
        putString(anagrafica.flagMaggiorazione, charset);
        putString(anagrafica.flagAccettataConfusoria, charset);
        putString(anagrafica.ambito, charset);
        putString(anagrafica.statoOpera, charset);
        putString(anagrafica.nPacco, charset);
        putString(anagrafica.inviante, charset);
        putString(anagrafica.rilevanza, charset);
        putString(anagrafica.maturato, charset);
        putString(anagrafica.rischio, charset);
        putString(anagrafica.semestreScadenza, charset);
        putString(anagrafica.dataSemScadenza, charset);
        putString(anagrafica.semestreDichiarazione, charset);
        putString(anagrafica.dataSemDichiarazione, charset);
        putString(anagrafica.semestreValidita, charset);
        putString(anagrafica.dataSemValidita, charset);
        putString(anagrafica.tipoUtilizzo, charset);
        putString(anagrafica.territorio, charset);
        putString(anagrafica.ricercabile, charset);
        putString(anagrafica.idCollectingCompany, charset);
        return commitEncoding();
    }
}
