package com.alkemytech.sophia.royalties.nosql;

import java.math.BigDecimal;
import java.sql.Date;

import com.google.gson.GsonBuilder;


public class Maturato {

	public BigDecimal valore;
	public String periodoLabel;
	public String dataDa;
	public String dataA;

	
	public Maturato(BigDecimal valore, String periodoLabel, String dataDa, String dataA) {
		super();
		this.valore = valore;
		this.periodoLabel = periodoLabel;
		this.dataDa = dataDa;
		this.dataA = dataA;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataA == null) ? 0 : dataA.hashCode());
		result = prime * result + ((dataDa == null) ? 0 : dataDa.hashCode());
		result = prime * result + ((periodoLabel == null) ? 0 : periodoLabel.hashCode());
		result = prime * result + ((valore == null) ? 0 : valore.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Maturato other = (Maturato) obj;
		if (dataA == null) {
			if (other.dataA != null)
				return false;
		} else if (!dataA.equals(other.dataA))
			return false;
		if (dataDa == null) {
			if (other.dataDa != null)
				return false;
		} else if (!dataDa.equals(other.dataDa))
			return false;
		if (periodoLabel == null) {
			if (other.periodoLabel != null)
				return false;
		} else if (!periodoLabel.equals(other.periodoLabel))
			return false;
		if (valore == null) {
			if (other.valore != null)
				return false;
		} else if (!valore.equals(other.valore))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}
	
}
