package com.alkemytech.sophia.royalties;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.ServerSocket;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;
import java.util.zip.GZIPOutputStream;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.collecting.SiaeCode;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.mmap.UnsafeMemoryMap;
import com.alkemytech.sophia.commons.sqs.MessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.SqsMessageHelper;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.alkemytech.sophia.commons.sqs.TrieMessageDeduplicator;
import com.alkemytech.sophia.commons.trie.BooleanBinding;
import com.alkemytech.sophia.commons.trie.ConcurrentTrie;
import com.alkemytech.sophia.commons.trie.FixedLengthStringBinding;
import com.alkemytech.sophia.commons.trie.LeafBinding;
import com.alkemytech.sophia.commons.trie.Trie;
import com.alkemytech.sophia.commons.util.BigDecimals;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.HeartBeat;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.royalties.country.Country;
import com.alkemytech.sophia.royalties.country.CountryService;
import com.alkemytech.sophia.royalties.ipisoc.IpiSoc;
import com.alkemytech.sophia.royalties.ipisoc.NoSqlIpiSocDatabase;
import com.alkemytech.sophia.royalties.jdbc.IpiDataSource;
import com.alkemytech.sophia.royalties.jdbc.UlisseDataSource;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.google.common.base.Strings;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Scopes;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class UlisseIpiDump {
	
	private static final Logger logger = LoggerFactory.getLogger(UlisseIpiDump.class);

	protected static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// amazon service(s)
			bind(S3.class)
				.in(Scopes.SINGLETON);
			bind(SQS.class)
				.in(Scopes.SINGLETON);
			// data source(s)
			bind(DataSource.class)
				.annotatedWith(Names.named("ULISSE"))
				.to(UlisseDataSource.class)
				.asEagerSingleton();
			bind(DataSource.class)
				.annotatedWith(Names.named("IPI"))
				.to(IpiDataSource.class)
				.asEagerSingleton();
			// other binding(s)
			bind(CountryService.class)
				.asEagerSingleton();
			bind(UlisseIpiDump.class)
				.asEagerSingleton();
		}

	}

	public static void main(String[] args) {
		try {
			final UlisseIpiDump instance = Guice
					.createInjector(new GuiceModuleExtension(args,
							"/ulisse-ipi-dump.properties"))
				.getInstance(UlisseIpiDump.class)
					.startup();
			try {
				instance.process(args);
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}

	private final Properties configuration;
	private final Charset charset;
	private final S3 s3;
	private final SQS sqs;
	private final CountryService countryService;
	private final DataSource ulisseDataSource;
	private final DataSource ipiDataSource;

	@Inject
	protected UlisseIpiDump(@Named("configuration") Properties configuration,
			@Named("charset") Charset charset,
			S3 s3, SQS sqs, CountryService countryService,
			@Named("ULISSE") DataSource ulisseDataSource,
			@Named("IPI") DataSource ipiDataSource) {
		super();
		this.configuration = configuration;
		this.charset = charset;
		this.s3 = s3;
		this.sqs = sqs;
		this.countryService = countryService;
		this.ulisseDataSource = ulisseDataSource;
		this.ipiDataSource = ipiDataSource;
	}

	public UlisseIpiDump startup() throws IOException {
		if ("true".equalsIgnoreCase(configuration.getProperty("ulisse_ipi_dump.locked", "true"))) {
			throw new IllegalStateException("application locked");
		}
		s3.startup();
		sqs.startup();
		return this;
	}

	public UlisseIpiDump shutdown() throws IOException {
		sqs.shutdown();
		s3.shutdown();
		return this;
	}

	public void process(String[] args) throws Exception {
		final int bindPort = Integer.parseInt(configuration.getProperty("ulisse_ipi_dump.bind_port",
				configuration.getProperty("default.bind_port", "0")));
		// bind lock tcp port
		try (ServerSocket socket = new ServerSocket(bindPort)) {
			logger.debug("socket bound to {}", socket.getLocalSocketAddress());
			// sqs message pump
			final SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration, "ulisse_ipi_dump.sqs");
			final MessageDeduplicator deduplicator = new TrieMessageDeduplicator(new File(configuration
					.getProperty("ulisse_ipi_dump.sqs.deduplicator_folder")));
			sqsMessagePump.pollingLoop(deduplicator, new SqsMessagePump.Consumer() {

				private JsonObject output;
				private JsonObject error;

				@Override
				public JsonObject getStartedMessagePayload(JsonObject message) {
					output = null;
					error = null;
					return SqsMessageHelper.formatContext();
				}

				@Override
				public boolean consumeMessage(JsonObject message) {
					try {
						output = new JsonObject();
						processMessage(message, output);
						error = null;
						return true;
					} catch (Exception e) {
						output = null;
						error = SqsMessageHelper.formatError(e);
						return false;
					}
				}

				@Override
				public JsonObject getCompletedMessagePayload(JsonObject message) {
					return output;
				}

				@Override
				public JsonObject getFailedMessagePayload(JsonObject message) {
					return error;
				}

			});
		}
	}
	
	private List<Map<String, String>> loadIpiIrregularities() {
		final List<Map<String, String>> result = new ArrayList<Map<String,String>>();		
		final String[] cols = configuration.getProperty("ulisse_ipi_dump.ipi.irregularity.cols", "").toLowerCase().split(",");
		final int rows = Integer.parseInt(configuration.getProperty("ulisse_ipi_dump.ipi.irregularity.rows", "0"));
		for (int row = 1; row <= rows; row ++) {
			final String[] values = configuration.getProperty("ulisse_ipi_dump.ipi.irregularity.row." + row).split(",");
			Map<String, String> irregolarita = new HashMap<String, String>();
			for (int col = 0; col < values.length; col ++) {
				irregolarita.put(cols[col], values[col]);
			}
			result.add(irregolarita);
		}		
		return result;
	}
	
	private Map<String, Map<String, String>> loadUlisseIrregularities() {
		final Map<String, Map<String, String>> result = new HashMap<String, Map<String,String>>();		
		final String[] cols = configuration.getProperty("ulisse_ipi_dump.ulisse.irregularity.cols", "").toLowerCase().split(",");
		final int rows = Integer.parseInt(configuration.getProperty("ulisse_ipi_dump.ulisse.irregularity.rows", "0"));
		for (int row = 1; row <= rows; row ++) {
			final String[] values = configuration.getProperty("ulisse_ipi_dump.ulisse.irregularity.row." + row).split(",");
			Map<String, String> irregolarita = new HashMap<String, String>();
			for (int col = 0; col < values.length; col ++) {
				irregolarita.put(cols[col], values[col]);
			}
			result.put(irregolarita.get("id_tipo_irregolarita"), irregolarita);
		}
		return result;
	}
	
	private Map<String, String> irregolaritaToQuota(String tipoQuota, Map<String, String> irregolarita) {
		if (null != irregolarita && !irregolarita.isEmpty()) {
			if ("DEM".equalsIgnoreCase(tipoQuota)) {
				final Map<String, String> quota = new HashMap<>();
				quota.put("quota_denominatore", irregolarita.get("denominatore"));
				quota.put("quota_numeratore", irregolarita.get("numeratore"));
				quota.put("codice_ipi", irregolarita.get("codice_ipi"));
				quota.put("posizione_siae", irregolarita.get("posizione_dem"));
				quota.put("tipo_qualifica", irregolarita.get("tipo_qualifica"));
				quota.put("tipo_quota", "DEM");
				quota.put("quota_percentuale", "0");
				quota.put("cognome", "");
				quota.put("codice_conto", irregolarita.get("conto_dem"));
				quota.put("id_conto", "");
				quota.put("id_repertorio", "1");
				quota.put("tipo_irregolarita", irregolarita.get("id_tipo_irregolarita"));
				quota.put("societa", "");
				quota.put("shr", "100");
				return quota;
			}
			if ("DRM".equalsIgnoreCase(tipoQuota)) {
				final Map<String, String> quota = new HashMap<>();
				quota.put("quota_denominatore", "0");
				quota.put("quota_numeratore", "0");
				quota.put("codice_ipi", irregolarita.get("codice_ipi"));
				quota.put("posizione_siae", irregolarita.get("posizione_drm"));
				quota.put("tipo_qualifica", irregolarita.get("tipo_qualifica"));
				quota.put("tipo_quota", "DRM");
				quota.put("quota_percentuale", irregolarita.get("percentuale"));
				quota.put("cognome", "");
				quota.put("codice_conto", irregolarita.get("conto_drm"));
				quota.put("id_conto", "");
				quota.put("id_repertorio", "1");
				quota.put("tipo_irregolarita", irregolarita.get("id_tipo_irregolarita"));
				quota.put("societa", "");
				quota.put("shr", "100");
				return quota;
			}
		}
		return null;
	}
	
	private String formatLine(String codice, List<Map<String, String>> arrayQuote, List<String> arrayColumns, char delimiter, char subDelimiter, String endOfLine) {
		// convert list of maps to map of lists
		final Map<String, List<String>> arrays = new HashMap<>();
		final int arraysLength = arrayQuote.size();
		for (Map<String, String> quota : arrayQuote) {
			for (Map.Entry<String, String> entry : quota.entrySet()) {
				List<String> array = arrays.get(entry.getKey());
				if (null == array)
					arrays.put(entry.getKey(), array = new ArrayList<>());
				array.add(entry.getValue());
			}
		}
		// totale DEM
		BigDecimal totaleDem = BigDecimal.ZERO;
		final Iterator<String> iteratorNumeratori = arrays
				.get("quota_numeratore").iterator();
		final Iterator<String> iteratorDenominatori = arrays
				.get("quota_denominatore").iterator();
		while (iteratorNumeratori.hasNext() && iteratorDenominatori.hasNext()) {
			final String numeratore = iteratorNumeratori.next();
			final String denominatore = iteratorDenominatori.next();
			if (!Strings.isNullOrEmpty(numeratore) &&
					!Strings.isNullOrEmpty(denominatore)) {
				final BigDecimal divisor = new BigDecimal(denominatore);
				if (!BigDecimals.isAlmostZero(divisor))
					totaleDem = totaleDem.add(BigDecimals
							.divide(new BigDecimal(numeratore), divisor));
			}
		}
		// totale DRM
		BigDecimal totaleDrm = BigDecimal.ZERO;
		for (String percentuale : arrays.get("quota_percentuale"))
			if (!Strings.isNullOrEmpty(percentuale))
				totaleDrm = totaleDrm.add(new BigDecimal(percentuale));
		// format line		
		final StringBuilder line = new StringBuilder()
				.append(codice);
		for (String column : arrayColumns) {
			List<String> array = arrays.get(column);
			if (null == array) {
				String[] empties = new String[arraysLength];
				Arrays.fill(empties, "");
				array = Arrays.asList(empties);
			} else if (arraysLength != array.size()) {
				throw new IllegalArgumentException(String
						.format("bad number of elements in %s: %d instead of %d",
								column, array.size(), arraysLength));
			}
			final Iterator<String> iterator = array.iterator();
			line.append(delimiter)
				.append(iterator.next());
			while (iterator.hasNext())
				line.append(subDelimiter)
					.append(iterator.next());
		}
		line.append(delimiter)
			.append(BigDecimals.toPlainString(totaleDem))
			.append(delimiter)
			.append(BigDecimals.toPlainString(totaleDrm))
			.append(endOfLine);
		return line.toString();
	}
	
	private void deleteS3FolderContents(String s3FolderUrl) {
		if (!s3FolderUrl.endsWith("/"))
			s3FolderUrl += '/';
		final List<S3ObjectSummary> objects = s3
				.listObjects(new S3.Url(s3FolderUrl));
		for (S3ObjectSummary object : objects) {
			final String s3FileUrl = new StringBuilder()
					.append("s3://")
					.append(object.getBucketName())
					.append('/')
					.append(object.getKey())
					.toString();
			if (s3.delete(new S3.Url(s3FileUrl)))
				logger.debug("file deleted {}", s3FileUrl);
			else
				logger.error("error deleting file {}", s3FileUrl);
		}
	}
	
	private void processMessage(JsonObject input, JsonObject output) throws Exception {

//		{
//		  "body": {
//		    "year": "2017",
//		    "month": "03",
//	        "day": "01",
//		    "techsysdate": true,
//		    "force": true
//		  },
//		  "header": {
//		    "queue": "dev_to_process_ipi_ulisse_dump_ml",
//		    "timestamp": "2018-05-10T00:00:00.000000+02:00",
//		    "uuid": "7f36a2bb-0637-4d4d-9990-4ed9644adefc",
//		    "sender": "aws-console"
//		  }
//		}

		logger.debug("dumping ipi and ulisse");
		final long startTimeMillis = System.currentTimeMillis();

		// parse input json message
		final JsonObject inputBody = input.getAsJsonObject("body");
		String year = GsonUtils.getAsString(inputBody, "year");
		String month = GsonUtils.getAsString(inputBody, "month");
		String day = GsonUtils.getAsString(inputBody, "day", configuration
				.getProperty("ulisse_ipi_dump.reference_day", "01"));
		final boolean techsysdate = GsonUtils.getAsBoolean(inputBody, "techsysdate",
				"true".equalsIgnoreCase(configuration.getProperty("ulisse_ipi_dump.techsysdate", "true")));
		final boolean force = GsonUtils.getAsBoolean(inputBody, "force", false);
		if (Strings.isNullOrEmpty(year) || Strings.isNullOrEmpty(month))
			throw new IllegalArgumentException("invalid input message");
		year = String.format("%04d", Integer.parseInt(year));
		month = String.format("%02d", Integer.parseInt(month));
		day = String.format("%02d", Integer.parseInt(day));
		logger.debug("year {}", year);
		logger.debug("month {}", month);
		logger.debug("day {}", day);		
		logger.debug("techsysdate {}", techsysdate);		

		// sql reference date
		final String sqldate = String
				.format("to_date('%s-%s-%s', 'YYYY-MM-DD')", year, month, day);
		logger.debug("sqldate {}", sqldate);
		final String techsqldate = techsysdate ? "sysdate" : sqldate;
		logger.debug("techsqldate {}", techsqldate);

		// initialize country service
		countryService.loadCountries(sqldate);			

		// configuration
//		final boolean debug = "true".equalsIgnoreCase(configuration
//				.getProperty("ulisse_ipi_dump.debug", configuration.getProperty("default.debug")));
		final File homeFolder = new File(configuration
				.getProperty("default.home_folder"));
		final String countryCode = configuration
				.getProperty("ulisse_ipi_dump.country_code", "ITA");
		final File latestFile = new File(homeFolder,configuration
				.getProperty("ulisse_ipi_dump.latest_filename", "_LATEST"));
		final String s3FolderUrl = configuration
				.getProperty("ulisse_ipi_dump.output.s3_folder_url")
				.replace("{year}", year)
				.replace("{month}", month);
		final long maxFileSize = TextUtils.parseLongSize(configuration
				.getProperty("ulisse_ipi_dump.output.max_file_size"));
		final char delimiter = configuration
				.getProperty("ulisse_ipi_dump.output.delimiter").trim().charAt(0);
		final char subDelimiter = configuration
				.getProperty("ulisse_ipi_dump.output.sub_delimiter").trim().charAt(0);
		final Map<String, String> ipiTypes = new HashMap<>();
		for (String entry : configuration
				.getProperty("ulisse_ipi_dump.ipi_types").split(",")) {
			final int colon = entry.indexOf(':');
			ipiTypes.put(entry.substring(0, colon),
					entry.substring(1 + colon));
		}
		final Set<String> ipiRoles = new HashSet<>(Arrays.asList(configuration
				.getProperty("ulisse_ipi_dump.ipi_roles").split(",")));		
		final Map<String, String> ulisseToIpiRoles = new HashMap<>();
		for (String entry : configuration
				.getProperty("ulisse_ipi_dump.ulisse_to_ipi_roles").split(",")) {
			final int colon = entry.indexOf(':');
			ulisseToIpiRoles.put(entry.substring(0, colon),
					entry.substring(1 + colon));
		}
		final List<String> arrayColumns = Arrays.asList(configuration
				.getProperty("ulisse_ipi_dump.array_columns").split(","));
		final List<Map<String, String>> ipiIrregularities = loadIpiIrregularities();
		final Map<String, Map<String, String>> ulisseIrregularities = loadUlisseIrregularities();
		
		logger.debug("countryCode {}", countryCode);
		logger.debug("ipiTypes {}", ipiTypes);
		logger.debug("ipiRoles {}", ipiRoles);
		logger.debug("ulisseToIpiRoles {}", ulisseToIpiRoles);
		logger.debug("arrayColumns {}", arrayColumns);
		logger.debug("ipiIrregularities {}", ipiIrregularities);		
		logger.debug("ulisseIrregularities {}", ulisseIrregularities);
		
		if (ipiTypes.isEmpty())
			throw new IllegalArgumentException("empty ipi types (rgt)");
		if (ipiRoles.isEmpty())
			throw new IllegalArgumentException("empty ipi roles (rol)");
		if (ulisseToIpiRoles.isEmpty())
			throw new IllegalArgumentException("empty ulisse to ipi roles (rol to tipo_qualifica)");
		if (arrayColumns.isEmpty())
			throw new IllegalArgumentException("empty output array columns");
		
		// check _LATEST file & retrieve latest version number
		final String latestS3FileUrl = new StringBuilder()
				.append(s3FolderUrl)
				.append('/')
				.append(latestFile.getName())
				.toString();
		final String latestVersion = s3
				.getObjectContents(new S3.Url(latestS3FileUrl), charset);
		logger.debug("latestVersion {}", latestVersion);
		// return immediately if a valid version already exists and no force flag
		if (!force && !Strings.isNullOrEmpty(latestVersion)) {
			// output
			output.addProperty("period", year + month);
			output.addProperty("version", latestVersion);
			output.addProperty("outputS3FolderUrl", configuration
					.getProperty("ulisse_ipi_dump.output.s3_versioned_folder_url")
					.replace("{year}", year)
					.replace("{month}", month)
					.replace("{version}", latestVersion));
			output.addProperty("startTime", DateFormat
					.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM)
						.format(new Date(startTimeMillis)));
			output.addProperty("totalDuration", TextUtils
					.formatDuration(System.currentTimeMillis() - startTimeMillis));
			output.addProperty("additionalInfo", String
					.format("version %s for period %s%s already exists", latestVersion, year, month));
			logger.info("version {} for period {}{} already exists", latestVersion, year, month);
			logger.debug("message processed in {}",
					TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));
			return;
		}
		// increment latest version
		final String version;
		if (Strings.isNullOrEmpty(latestVersion)) {
			version = "1";
		} else {
			try {
				version = Integer.toString(1 +
						Integer.parseInt(latestVersion));
			} catch (NumberFormatException e) {
				throw new IllegalArgumentException(String
						.format("invalid version number %s inside %s file",
								latestVersion, latestFile.getName()));
			}
		}
		logger.debug("version {}", version);
		
		// output s3 folder url
		final String outputS3FolderUrl = configuration
				.getProperty("ulisse_ipi_dump.output.s3_versioned_folder_url")
				.replace("{year}", year)
				.replace("{month}", month)
				.replace("{version}", version);
		logger.debug("outputS3FolderUrl {}", outputS3FolderUrl);
		
		// ipi_tisagm
		final Trie<Boolean> ipiTisAgmTrie;
		if ("true".equalsIgnoreCase(configuration
				.getProperty("ulisse_ipi_dump.ipi_tisagm.read_only"))) {
			ipiTisAgmTrie = ConcurrentTrie.wrap(UnsafeMemoryMap.open(new File(configuration
					.getProperty("ulisse_ipi_dump.ipi_tisagm.home_folder")), -1, true), new BooleanBinding());
		} else {
			ipiTisAgmTrie = ConcurrentTrie.wrap(UnsafeMemoryMap.create(new File(configuration
					.getProperty("ulisse_ipi_dump.ipi_tisagm.home_folder")), TextUtils
					.parseIntSize(configuration.getProperty("ulisse_ipi_dump.ipi_tisagm.page_size"))), new BooleanBinding());
		}
		
		// ipi_soc
		if ("true".equalsIgnoreCase(configuration
				.getProperty("ulisse_ipi_dump.ipi_soc.read_only"))) {
			configuration.setProperty("ulisse_ipi_dump.ipi_soc.page_size", "-1");
			configuration.setProperty("ulisse_ipi_dump.ipi_soc.read_only", "true");
			configuration.setProperty("ulisse_ipi_dump.ipi_soc.create_always", "false");					
		}
		final NoSqlIpiSocDatabase ipiSocNoSql = new NoSqlIpiSocDatabase(configuration, "ulisse_ipi_dump.ipi_soc");
		
		// ulisse_schema_territorio
		final Trie<Boolean> ulisseSchemaTerritorioTrie;
		if ("true".equalsIgnoreCase(configuration
					.getProperty("ulisse_ipi_dump.ulisse_schema_territorio.read_only"))) {
			ulisseSchemaTerritorioTrie = ConcurrentTrie.wrap(UnsafeMemoryMap.open(new File(configuration
					.getProperty("ulisse_ipi_dump.ulisse_schema_territorio.home_folder")), -1, true), new BooleanBinding());
		} else {
			ulisseSchemaTerritorioTrie = ConcurrentTrie.wrap(UnsafeMemoryMap.create(new File(configuration
					.getProperty("ulisse_ipi_dump.ulisse_schema_territorio.home_folder")), TextUtils
					.parseIntSize(configuration.getProperty("ulisse_ipi_dump.ulisse_schema_territorio.page_size"))), new BooleanBinding());
		}
		
		// ulisse_codice_opera
		final Trie<String> ulisseCodiceOperaTrie;
		final LeafBinding<String> codiceOperaSerializer = new FixedLengthStringBinding(11, charset);
		if ("true".equalsIgnoreCase(configuration
				.getProperty("ulisse_ipi_dump.ulisse_codice_opera.read_only"))) {
			ulisseCodiceOperaTrie = new ConcurrentTrie<>(UnsafeMemoryMap
					.open(new File(configuration.getProperty("ulisse_ipi_dump.ulisse_codice_opera.home_folder")), -1, true), codiceOperaSerializer);
		} else {
			ulisseCodiceOperaTrie = new ConcurrentTrie<>(UnsafeMemoryMap
					.create(new File(configuration.getProperty("ulisse_ipi_dump.ulisse_codice_opera.home_folder")),
							TextUtils.parseIntSize(configuration.getProperty("ulisse_ipi_dump.ulisse_codice_opera.page_size"))), codiceOperaSerializer);
		}

		////////
		// IPI
		////////
		try (final Connection connection = ipiDataSource.getConnection()) {
			logger.debug("jdbc connected to {} {}", connection.getMetaData()
					.getDatabaseProductName(), connection.getMetaData().getURL());

			///////////////
			// ipi_tisagm
			///////////////
			if (!"true".equalsIgnoreCase(configuration
					.getProperty("ulisse_ipi_dump.ipi_tisagm.read_only"))) {
				final long lapTimeMillis = System.currentTimeMillis();
				logger.debug("creating ipi_tisagm trie for country {}", countryCode);
				try (final Statement statement = connection.createStatement()) {
					long count = 0L;
					final HeartBeat heartbeat = HeartBeat.constant("ipi_tisagm", Integer
							.parseInt(configuration.getProperty("ulisse_ipi_dump.ipi_tisagm.heartbeat", "10000")));
					final String sql = configuration
							.getProperty("ulisse_ipi_dump.sql.ipi_tisagm")
							.replace("{sqldate}", sqldate)
							.replace("{techsqldate}", techsqldate);
					logger.debug("executing query {}", sql);
					try (final ResultSet resultSet = statement.executeQuery(sql)) {
						logger.debug("fetching query results");
						final Set<Country> countries = new HashSet<Country>();
						String lastAgmid = null;
						// loop on result set
						while (resultSet.next()) {
							heartbeat.pump();
							final String agmid = resultSet.getString("agmid");
							final long tisnr = resultSet.getLong("tisnr");
							final String incexc = resultSet.getString("incexc");
							if (null != lastAgmid && !lastAgmid.equals(agmid)) {
								boolean found = false;
								for (Country country : countries) {
									if (countryCode.equalsIgnoreCase(country.getTisAExt())) {
										found = true;
										break;
									}
								}
								countries.clear();
								if (found) {
									ipiTisAgmTrie.upsert(lastAgmid, true);
									count ++;
								}
							}
							lastAgmid = agmid;
							if ("I".equalsIgnoreCase(incexc)) {
								countries.addAll(countryService.getCountries(tisnr));
							} else if ("E".equalsIgnoreCase(incexc)) {
								countries.removeAll(countryService.getCountries(tisnr));
							}
						}
						// last agmid
						if (null != lastAgmid && !countries.isEmpty()) {
							boolean found = false;
							for (Country country : countries) {
								if (countryCode.equalsIgnoreCase(country.getTisAExt())) {
									found = true;
									break;
								}
							}
							if (found) {
								ipiTisAgmTrie.upsert(lastAgmid, true);
								count ++;
							}
						}
					}
					logger.debug("total parsed rows {}", heartbeat.getTotalPumps());
					logger.debug("total ipi_tisagm trie leaves {}", count);
					// output stats
					final JsonObject queryStats = new JsonObject();
					queryStats.addProperty("sql", sql);
					queryStats.addProperty("rownum", heartbeat.getTotalPumps());
					queryStats.addProperty("count", count);
					queryStats.addProperty("duration", TextUtils
							.formatDuration(System.currentTimeMillis() - lapTimeMillis));
					output.add("ipiQuery1", queryStats);
				}
				logger.debug("ipi_tisagm trie created in {}",
						TextUtils.formatDuration(System.currentTimeMillis() - lapTimeMillis));
			}

			////////////
			// ipi_soc
			////////////
			if (!"true".equalsIgnoreCase(configuration
					.getProperty("ulisse_ipi_dump.ipi_soc.read_only"))) {
				final long lapTimeMillis = System.currentTimeMillis();
				logger.debug("exporting ipi_soc for country {}", countryCode);
				try (final Statement statement = connection.createStatement()) {
					long count = 0L;
					final HeartBeat heartbeat = HeartBeat.constant("ipi_soc", Integer
							.parseInt(configuration.getProperty("ulisse_ipi_dump.ipi_soc.heartbeat", "10000")));
					final String sql = configuration
							.getProperty("ulisse_ipi_dump.sql.ipi_soc")
							.replace("{sqldate}", sqldate)
							.replace("{techsqldate}", techsqldate);
					logger.debug("executing query {}", sql);
					try (final ResultSet resultSet = statement.executeQuery(sql)) {
						logger.debug("fetching query results");
						final Set<String> lastRgtRol = new HashSet<String>();
						String lastIpnamenr = null;
						// loop on result set
						while (resultSet.next()) {
							heartbeat.pump();
							final String agmid = resultSet.getString("agmid");
							final String ipnamenr = resultSet.getString("ipnamenr");
							final String rgt = ipiTypes.get(resultSet.getString("rgt"));
							if (null == rgt)
								continue;
							final String rol = resultSet.getString("rol");
							if (!ipiRoles.contains(rol))
								continue;
							if (null != lastIpnamenr && !lastIpnamenr.equals(ipnamenr))
								lastRgtRol.clear();
							final String currRgtRol = rgt + rol;
							if (lastRgtRol.contains(currRgtRol))
								continue;
							lastRgtRol.add(currRgtRol);
							lastIpnamenr = ipnamenr;			
							final Boolean found = ipiTisAgmTrie.find(agmid);
							if (null != found && found) {
								final String shr = resultSet.getString("shr");
								final String societa = resultSet.getString("societa");
								if (!Strings.isNullOrEmpty(societa)) {
									ipiSocNoSql.put(new IpiSoc(ipnamenr, rgt, rol, shr, societa));
									count ++;
								}
							}
						}
						// irregularities
						if (null != ipiIrregularities && !ipiIrregularities.isEmpty()) {
							for (Map<String, String> irregolarita : ipiIrregularities) {
								final String ipnamenr = irregolarita.get("ipnamenr");
								final String rgt = irregolarita.get("rgt");
								final String rol = irregolarita.get("rol");
								final String shr = irregolarita.get("shr");
								final String societa = irregolarita.get("societa");
								if (!Strings.isNullOrEmpty(societa)) {
									ipiSocNoSql.put(new IpiSoc(ipnamenr, rgt, rol, shr, societa));
									count ++;
								}
							}
						}					
					}
					logger.debug("total parsed rows {}", heartbeat.getTotalPumps());
					logger.debug("total nosql ipi_soc records {}", count);
					// output stats
					final JsonObject queryStats = new JsonObject();
					queryStats.addProperty("sql", sql);
					queryStats.addProperty("rownum", heartbeat.getTotalPumps());
					queryStats.addProperty("count", count);
					queryStats.addProperty("duration", TextUtils
							.formatDuration(System.currentTimeMillis() - lapTimeMillis));
					output.add("ipiQuery2", queryStats);
				}
				logger.debug("ipi_soc exported in {}",
						TextUtils.formatDuration(System.currentTimeMillis() - lapTimeMillis));
			}
		}

		// clear ipi_tisagm trie
		if ("true".equalsIgnoreCase(configuration
				.getProperty("ulisse_ipi_dump.ipi_tisagm.delete_when_done")))
			ipiTisAgmTrie.clear();
		
		///////////
		// ULISSE
		///////////
		try (final Connection connection = ulisseDataSource.getConnection()) {
			logger.debug("jdbc connected to {} {}", connection.getMetaData()
					.getDatabaseProductName(), connection.getMetaData().getURL());

			/////////////////////////////
			// ulisse_schema_territorio
			/////////////////////////////
			if (!"true".equalsIgnoreCase(configuration
					.getProperty("ulisse_ipi_dump.ulisse_schema_territorio.read_only"))) {
				final long lapTimeMillis = System.currentTimeMillis();
				logger.debug("creating ulisse_schema_territorio trie for country {}", countryCode);
				try (final Statement statement = connection.createStatement()) {
					long count = 0L;
					final HeartBeat heartbeat = HeartBeat.constant("ulisse_schema_territorio", Integer
							.parseInt(configuration.getProperty("ulisse_ipi_dump.ulisse_schema_territorio.heartbeat", "10000")));
					final String sql = configuration
							.getProperty("ulisse_ipi_dump.sql.ulisse_schema_territorio")
							.replace("{sqldate}", sqldate)
							.replace("{techsqldate}", techsqldate);
					logger.debug("executing query {}", sql);
					try (final ResultSet resultSet = statement.executeQuery(sql)) {
						logger.debug("fetching query results");
						final Set<Country> countries = new HashSet<Country>();
						String lastIdSchemaRiparto = null;
						// loop on result set
						while (resultSet.next()) {
							heartbeat.pump();
							final String idSchemaRiparto = resultSet.getString("id_schema_riparto");
							final long idTerritorio = resultSet.getLong("id_territorio");
							final String operatoreLogico = resultSet.getString("operatore_logico");
							if (null != lastIdSchemaRiparto && !lastIdSchemaRiparto.equals(idSchemaRiparto)) {
								boolean found = false;
								for (Country country : countries) {
									if (countryCode.equalsIgnoreCase(country.getTisAExt())) {
										found = true;
										break;
									}
								}
								countries.clear();
								if (found) {
									ulisseSchemaTerritorioTrie.upsert(lastIdSchemaRiparto, true);
									count ++;
								}
							}
							lastIdSchemaRiparto = idSchemaRiparto;
							if ("I".equalsIgnoreCase(operatoreLogico)) {
								countries.addAll(countryService.getCountries(idTerritorio));
							} else if ("E".equalsIgnoreCase(operatoreLogico)) {
								countries.removeAll(countryService.getCountries(idTerritorio));
							}
						}
						// last id_schema_riparto
						if (null != lastIdSchemaRiparto && !countries.isEmpty()) {
							boolean found = false;
							for (Country country : countries) {
								if (countryCode.equalsIgnoreCase(country.getTisAExt())) {
									found = true;
									break;
								}
							}
							if (found) {
								ulisseSchemaTerritorioTrie.upsert(lastIdSchemaRiparto, true);
								count ++;
							}
						}
					}
					logger.debug("total parsed rows {}", heartbeat.getTotalPumps());
					logger.debug("total ulisse_schema_territorio trie leaves {}", count);
					// output stats
					final JsonObject queryStats = new JsonObject();
					queryStats.addProperty("sql", sql);
					queryStats.addProperty("rownum", heartbeat.getTotalPumps());
					queryStats.addProperty("count", count);
					queryStats.addProperty("duration", TextUtils
							.formatDuration(System.currentTimeMillis() - lapTimeMillis));
					output.add("ulisseQuery1", queryStats);
				}
				logger.debug("ulisse_schema_territorio trie created in {}",
						TextUtils.formatDuration(System.currentTimeMillis() - lapTimeMillis));
			}
			
			////////////////////////
			// ulisse_codice_opera
			////////////////////////
			if (!"true".equalsIgnoreCase(configuration
					.getProperty("ulisse_ipi_dump.ulisse_codice_opera.read_only"))) {
				final long lapTimeMillis = System.currentTimeMillis();
				logger.debug("creating ulisse_codice_opera trie for country {}", countryCode);
				try (final Statement statement = connection.createStatement()) {
					long count = 0L;
					final HeartBeat heartbeat = HeartBeat.constant("ulisse_codice_opera", Integer
							.parseInt(configuration.getProperty("ulisse_ipi_dump.ulisse_codice_opera.heartbeat", "10000")));
					final String sql = configuration
							.getProperty("ulisse_ipi_dump.sql.ulisse_codice_opera")
							.replace("{sqldate}", sqldate)
							.replace("{techsqldate}", techsqldate);
					logger.debug("executing query {}", sql);
					try (final ResultSet resultSet = statement.executeQuery(sql)) {
						logger.debug("fetching query results");					
						// loop on result set
						while (resultSet.next()) {
							heartbeat.pump();
							final String codice = SiaeCode.normalize(resultSet.getString("codice"));
							final String idSchemaRiparto = resultSet.getString("id_schema_riparto");
							if (!Strings.isNullOrEmpty(codice) &&
									!Strings.isNullOrEmpty(idSchemaRiparto)) {
								final Boolean found = ulisseSchemaTerritorioTrie.find(idSchemaRiparto);
								if (null != found && found) {
									ulisseCodiceOperaTrie.upsert(idSchemaRiparto, codice);
									count ++;
								}
							}
						}
					}
					logger.debug("total parsed rows {}", heartbeat.getTotalPumps());
					logger.debug("total ulisse_codice_opera trie leaves {}", count);
					// output stats
					final JsonObject queryStats = new JsonObject();
					queryStats.addProperty("sql", sql);
					queryStats.addProperty("rownum", heartbeat.getTotalPumps());
					queryStats.addProperty("count", count);
					queryStats.addProperty("duration", TextUtils
							.formatDuration(System.currentTimeMillis() - lapTimeMillis));
					output.add("ulisseQuery2", queryStats);
				}
				logger.debug("ulisse_codice_opera trie created in {}",
						TextUtils.formatDuration(System.currentTimeMillis() - lapTimeMillis));
			}

			// clear ulisse_schema_territorio trie
			if ("true".equalsIgnoreCase(configuration
					.getProperty("ulisse_ipi_dump.ulisse_schema_territorio.delete_when_done")))
				ulisseSchemaTerritorioTrie.clear();
			
			// delete all files already in output s3 folder
			logger.debug("deleting dirty file(s) in {}", outputS3FolderUrl);
			deleteS3FolderContents(outputS3FolderUrl);

			////////////////////////
			// ulisse_quota_riparto
			////////////////////////
			long lapTimeMillis = System.currentTimeMillis();
			logger.debug("exporting ulisse_quota_riparto for country {}", countryCode);
			try (final Statement statement = connection.createStatement()) {
				int part = 0;
				long count = 0L;
				long writtenBytes = 0L;
				File outputFile = null;
				OutputStream outputStream = null;
				final String uuid = UUID.randomUUID().toString();
				final HeartBeat heartbeat = HeartBeat.constant("ulisse_quota_riparto", Integer
						.parseInt(configuration.getProperty("ulisse_ipi_dump.ulisse_quota_riparto.heartbeat", "50000")));
				final String sql = configuration
						.getProperty("ulisse_ipi_dump.sql.ulisse_quota_riparto")
						.replace("{sqldate}", sqldate)
						.replace("{techsqldate}", techsqldate);
				logger.debug("executing query {}", sql);
				try (final ResultSet resultSet = statement.executeQuery(sql)) {
					logger.debug("fetching query results");					
					String lastIdSchemaRiparto = null;
					String lastTipoQuota = null;
					String lastIdTipoIrregolarita = null;
					final List<Map<String, String>> arrayQuote = new ArrayList<>();
					// loop on result set
					while (resultSet.next()) {
						heartbeat.pump();
						final String idSchemaRiparto = resultSet.getString("id_schema_riparto");
						final String quotaDenominatore = resultSet.getString("quota_denominatore");						
						final String quotaNumeratore = resultSet.getString("quota_numeratore");
						final String codiceIpi = resultSet.getString("codice_ipi");
						final String posizioneSiae = resultSet.getString("posizione_siae");
						final String tipoQualifica = resultSet.getString("tipo_qualifica");
						final String tipoQuota = resultSet.getString("tipo_quota");
						final String quotaPercentuale = resultSet.getString("quota_percentuale");
						final String cognome = resultSet.getString("cognome");
						final String codiceConto = resultSet.getString("codice_conto");
						final String codiceContoEstero = resultSet.getString("codice_conto_estero");
						final String idRepertorio = resultSet.getString("id_repertorio");
						final String idTipoIrregolarita = resultSet.getString("id_tipo_irregolarita");
//						logger.debug("rownum {}", resultSet.getRow());
//						logger.debug("  idSchemaRiparto {}", idSchemaRiparto);
//						logger.debug("  quotaDenominatore {}", quotaDenominatore);
//						logger.debug("  quotaNumeratore {}", quotaNumeratore);
//						logger.debug("  codiceIpi {}", codiceIpi);
//						logger.debug("  posizioneSiae {}", posizioneSiae);
//						logger.debug("  tipoQualifica {}", tipoQualifica);
//						logger.debug("  tipoQuota {}", tipoQuota);
//						logger.debug("  quotaPercentuale {}", quotaPercentuale);
//						logger.debug("  cognome {}", cognome);
//						logger.debug("  codiceConto {}", codiceConto);
//						logger.debug("  codiceContoEstero {}", codiceContoEstero);
//						logger.debug("  idRepertorio {}", idRepertorio);
//						logger.debug("  idTipoIrregolarita {}", idTipoIrregolarita);
						if (Strings.isNullOrEmpty(idSchemaRiparto) ||
								Strings.isNullOrEmpty(tipoQuota))
							continue;
						
						// irregularities
						if (!Strings.isNullOrEmpty(lastIdSchemaRiparto) &&
								!Strings.isNullOrEmpty(lastTipoQuota) &&
								!Strings.isNullOrEmpty(lastIdTipoIrregolarita)) {
							if (!lastIdSchemaRiparto.equals(idSchemaRiparto) || !lastTipoQuota.equals(tipoQuota)) {
								final Map<String, String> quota = irregolaritaToQuota(lastTipoQuota,
										ulisseIrregularities.get(lastIdTipoIrregolarita));
								if (null != quota)
									arrayQuote.add(quota);
							}
						}

						// next royalty scheme
						if (null != lastIdSchemaRiparto &&
								!lastIdSchemaRiparto.equals(idSchemaRiparto) &&
								!arrayQuote.isEmpty()) {
							final String codice = ulisseCodiceOperaTrie.find(lastIdSchemaRiparto);
//							logger.debug("id_schema_riparto {} codice {}", lastIdSchemaRiparto, codice);
							if (!Strings.isNullOrEmpty(codice)) {
								if (null == outputStream) {
									outputFile = new File(homeFolder, String
											.format("part-%04d-%s.csv.gz", part ++, uuid));
									outputFile.deleteOnExit();
									outputStream = new GZIPOutputStream(new FileOutputStream(outputFile), 65536);
								}
								final String line = formatLine(codice, arrayQuote,
										arrayColumns, delimiter, subDelimiter, "\n");
								final byte[] bytes = line.getBytes(charset);
								outputStream.write(bytes);
								writtenBytes += bytes.length;
								count ++;

								// upload output file to s3
								if (writtenBytes >= maxFileSize) {
									writtenBytes = 0L;
									outputStream.close();
									outputStream = null;
									final String s3FileUrl = new StringBuilder()
											.append(outputS3FolderUrl)
											.append('/')
											.append(outputFile.getName())
											.toString(); 
									if (!s3.upload(new S3.Url(s3FileUrl), outputFile)) {
										logger.error("error uploading file {} to {}", outputFile.getName(), s3FileUrl);
										throw new IOException(String
												.format("error uploading file %s to %s", outputFile.getName(), s3FileUrl));
									} else {
										logger.debug("file {} uploaded to {}", outputFile.getName(), s3FileUrl);
									}
									outputFile.delete();
									outputFile = null;
								}
							}
							arrayQuote.clear();
						}
						
						// regular royalties
						if (Strings.isNullOrEmpty(idTipoIrregolarita)) {
							final IpiSoc ipiSoc = ipiSocNoSql.get(codiceIpi,
									tipoQuota, ulisseToIpiRoles.get(tipoQualifica));
							final Map<String, String> quota = new HashMap<>();
							quota.put("quota_denominatore", null == quotaDenominatore ? "" : quotaDenominatore);
							quota.put("quota_numeratore", null == quotaNumeratore ? "" : quotaNumeratore);
							quota.put("codice_ipi", null == codiceIpi ? "" : codiceIpi);
							quota.put("posizione_siae", null == posizioneSiae ? "" : posizioneSiae);
							quota.put("tipo_qualifica", null == tipoQualifica ? "" : tipoQualifica);
							quota.put("tipo_quota", null == tipoQuota ? "" : tipoQuota);
							quota.put("quota_percentuale", null == quotaPercentuale ? "" : quotaPercentuale);
							quota.put("cognome", null == cognome ? "" : cognome);
							quota.put("codice_conto", null == codiceConto ? "" : codiceConto);
							quota.put("id_conto", null == codiceContoEstero ? "" : codiceContoEstero);
							quota.put("id_repertorio", null == idRepertorio ? "" : idRepertorio);
							quota.put("tipo_irregolarita", "");
							quota.put("societa", null == ipiSoc ? "" : ipiSoc.societa);
							quota.put("shr", null == ipiSoc ? "" : ipiSoc.shr);
							arrayQuote.add(quota);
						}
						
						// choose highest id_tipo_irregolarita
						if (lastIdSchemaRiparto == idSchemaRiparto &&
								null != lastIdTipoIrregolarita) {
							final Map<String, String> irregolarita = ulisseIrregularities.get(idTipoIrregolarita);
							if (null != irregolarita) {
								final Map<String, String> lastIrregolarita = ulisseIrregularities.get(lastIdTipoIrregolarita);
								if (null != lastIrregolarita) {
									if (irregolarita.get("codice").compareTo(lastIrregolarita.get("codice")) > 0) {
										lastIdTipoIrregolarita = idTipoIrregolarita;
									}
								}
							}
						} else {
							lastIdTipoIrregolarita = idTipoIrregolarita;
						}
						
						lastIdSchemaRiparto = idSchemaRiparto;
						lastTipoQuota = tipoQuota;
					}
					
					// remaining irregularities
					if (!Strings.isNullOrEmpty(lastIdSchemaRiparto) &&
							!Strings.isNullOrEmpty(lastTipoQuota) &&
							!Strings.isNullOrEmpty(lastIdTipoIrregolarita)) {
						final Map<String, String> quota = irregolaritaToQuota(lastTipoQuota, 
								ulisseIrregularities.get(lastIdTipoIrregolarita));
						if (null != quota)
							arrayQuote.add(quota);
					}
					
					// remaining royalties
					if (!arrayQuote.isEmpty()) {
						final String codice = ulisseCodiceOperaTrie.find(lastIdSchemaRiparto);
						if (!Strings.isNullOrEmpty(codice)) {
							if (null == outputStream) {
								outputFile = new File(homeFolder, String
										.format("part-%04d-%s.csv.gz", part ++, uuid));
								outputFile.deleteOnExit();
								outputStream = new GZIPOutputStream(new FileOutputStream(outputFile), 65536);
							}
							final String line = formatLine(codice, arrayQuote,
									arrayColumns, delimiter, subDelimiter, "\n");
							final byte[] bytes = line.getBytes(charset);
							outputStream.write(bytes);
							count ++;
						}
						arrayQuote.clear();
					}
					
					// upload output file to s3
					if (null != outputStream && null != outputFile) {
						outputStream.close();
						outputStream = null;
						final String s3FileUrl = new StringBuilder()
								.append(outputS3FolderUrl)
								.append('/')
								.append(outputFile.getName())
								.toString(); 
						if (!s3.upload(new S3.Url(s3FileUrl), outputFile)) {
							logger.error("error uploading file {} to {}", outputFile.getName(), s3FileUrl);
							throw new IOException(String
									.format("error uploading file %s to %s", outputFile.getName(), s3FileUrl));
						} else {
							logger.debug("file {} uploaded to {}", outputFile.getName(), s3FileUrl);
						}
						outputFile.delete();
						outputFile = null;
					}
					
				}
				logger.debug("total parsed rows {}", heartbeat.getTotalPumps());
				logger.debug("total ulisse_quota_riparto records {}", count);
				// output stats
				final JsonObject queryStats = new JsonObject();
				queryStats.addProperty("sql", sql);
				queryStats.addProperty("rownum", heartbeat.getTotalPumps());
				queryStats.addProperty("count", count);
				queryStats.addProperty("duration", TextUtils
						.formatDuration(System.currentTimeMillis() - lapTimeMillis));
				output.add("ulisseQuery3", queryStats);
			}
			logger.debug("ulisse_quota_riparto exported in {}",
					TextUtils.formatDuration(System.currentTimeMillis() - lapTimeMillis));
			
			// create & upload _LATEST file
			try (final FileWriter writer = new FileWriter(latestFile)) {
				writer.write(version);
			}
			final String s3FileUrl = new StringBuilder()
					.append(s3FolderUrl)
					.append('/')
					.append(latestFile.getName())
					.toString();
			latestFile.deleteOnExit();
			if (!s3.upload(new S3.Url(s3FileUrl), latestFile)) {
				logger.error("error uploading file {} to {}", latestFile.getName(), s3FileUrl);
				throw new IOException(String
						.format("error uploading file %s to %s", latestFile.getName(), s3FileUrl));
			} else {
				logger.debug("file {} uploaded to {}", latestFile.getName(), s3FileUrl);
			}
			latestFile.delete();
			
		}
		
		// truncate ipi_soc nosql
		if ("true".equalsIgnoreCase(configuration
				.getProperty("ulisse_ipi_dump.ipi_soc.delete_when_done")))
			ipiSocNoSql.truncate();

		// clear ulisse_codice_opera trie
		if ("true".equalsIgnoreCase(configuration
				.getProperty("ulisse_ipi_dump.ulisse_codice_opera.delete_when_done")))
			ulisseCodiceOperaTrie.clear();

		// output
		output.addProperty("period", year + month);
		output.addProperty("version", version);
		output.addProperty("outputS3FolderUrl", outputS3FolderUrl);
		output.addProperty("startTime", DateFormat
				.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM)
					.format(new Date(startTimeMillis)));
		output.addProperty("totalDuration", TextUtils
				.formatDuration(System.currentTimeMillis() - startTimeMillis));
		
		logger.debug("message processed in {}",
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));
	}

}
