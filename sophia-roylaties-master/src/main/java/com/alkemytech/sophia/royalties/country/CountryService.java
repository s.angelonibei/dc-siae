package com.alkemytech.sophia.royalties.country;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class CountryService {

	private final Logger logger = LoggerFactory.getLogger(CountryService.class);

	private final Properties configuration;
	private final DataSource dataSource;

	private Map<Long, Set<Country>> countries;
	private Map<String, Long> names;
	
	@Inject
	protected CountryService(@Named("configuration") Properties configuration,
			@Named("ULISSE") DataSource dataSource) {
		super();
		this.configuration = configuration;
		this.dataSource = dataSource;
	}
	
	private Long nullAwareGetLong(String column, ResultSet resultSet) throws SQLException {
		final long value = resultSet.getLong(column);
		return resultSet.wasNull() ? null : value;
	}
	
	private void recursiveExpansion(Set<Country> countrySet, Long tisnr, Map<Long, List<Long>> gerarchie, Map<Long, Country> numberToObjectMap) {
		final List<Long> gerarchia = gerarchie.get(tisnr);
		if (null != gerarchia && !gerarchia.isEmpty()) {
			for (Long childTisnr : gerarchia)
				recursiveExpansion(countrySet, childTisnr, gerarchie, numberToObjectMap);
		} else {
			final Country country = numberToObjectMap.get(tisnr);
			if (null != country &&
					!Strings.isNullOrEmpty(country.getTisA()) &&
					!Strings.isNullOrEmpty(country.getTisAExt())) {
				countrySet.add(country);
			}
		}
	}
	
	public synchronized void loadCountries(String sqldate) throws SQLException {
		
//		TERRITORIO
//		--------------------- -------- ------------------ 
//		Name                  Null     Type               
//		--------------------- -------- ------------------ 
//		TIS_N                 NOT NULL NUMBER(10)         
//		TIS_A                 NOT NULL VARCHAR2(255 CHAR) 
//		TIS_A_EXT                      VARCHAR2(255 CHAR) 
//		NOME                  NOT NULL VARCHAR2(255 CHAR) 
//		NOME_UFFICIALE                 VARCHAR2(255 CHAR) 
//		NOME_ABBREVIATO                VARCHAR2(255 CHAR) 
//		TIPO                           VARCHAR2(255 CHAR) 
//		ID_COLLECTING_COMPANY NOT NULL NUMBER(19)         
//		DATA_VALIDITA_DA               TIMESTAMP(6)       
//		DATA_VALIDITA_A                TIMESTAMP(6)       
//		NOTE                           VARCHAR2(255 CHAR) 
//		--------------------- -------- ------------------ 
		
//		TERRITORIO_GERARCHIA
//		----------------------- -------- ------ 
//		Name                    Null     Type   
//		----------------------- -------- ------ 
//		ID_TERRITORIO_GERARCHIA NOT NULL NUMBER 
//		TIS_N_PARENT            NOT NULL NUMBER 
//		TIS_N_CHILD             NOT NULL NUMBER 
//		APPARTIENE_DA                    DATE   
//		APPARTIENE_A                     DATE 
//		----------------------- -------- ------ 

		// WARNING: aggiungere where condition sulla data validità (che può essere null) rispetto 
		//          alla data di estrazione (e non alla sysdate)
		
		try (final Connection connection = dataSource.getConnection()) {
			logger.debug("jdbc connected to {} {}", connection.getMetaData()
					.getDatabaseProductName(), connection.getMetaData().getURL());

			try (final Statement stmt = connection.createStatement()) {
				
				// build map containing flat lists of country numbers for each country group
				// WARNING: only one depth level supported, no recursion is performed
				final Map<Long, List<Long>> gerarchie = new HashMap<Long, List<Long>>();
				String sql = configuration
						.getProperty("country_service.sql.select_territorio_gerarchia")
						.replace("{sqldate}", sqldate);
				logger.debug("executing query {}", sql);
				try (final ResultSet rset = stmt.executeQuery(sql)) {
					while (rset.next()) {
						final Long parent = nullAwareGetLong("TIS_N_PARENT", rset);
						final Long child = nullAwareGetLong("TIS_N_CHILD", rset);
						if (null != parent && null != child) {
							List<Long> gerarchia = gerarchie.get(parent);
							if (null == gerarchia)
								gerarchie.put(parent, gerarchia = new ArrayList<Long>());
							gerarchia.add(child);
						}
					}
				}
				
				// build map containing names and country number and ...
				names = new HashMap<>();
				// ... build map containing a country object for each country number
				final Map<Long, Country> numberToObjectMap = new HashMap<Long, Country>();
				sql = configuration
						.getProperty("country_service.sql.select_territorio")
						.replace("{sqldate}", sqldate);
				logger.debug("executing query {}", sql);
				try (final ResultSet rset = stmt.executeQuery(sql)) {
					while (rset.next()) {
						final Long tisnr = nullAwareGetLong("TIS_N", rset);
						if (null != tisnr) {
							final Country country = new Country()
									.setTisN(tisnr.longValue())
									.setTisA(rset.getString("TIS_A"))
									.setTisAExt(rset.getString("TIS_A_EXT"))
									.setNome(rset.getString("NOME"));
							numberToObjectMap.put(country.getTisN(), country);
							names.put(country.getNome(), country.getTisN());
						}
					}
				}
				
				// add missing countries from configuration
				final String[] missingCountries = configuration
						.getProperty("country_service.missing_countries", "").split(";");
				for (String missingCountry : missingCountries) {
					try {
						final String[] array = missingCountry.split(",");
						if (!Strings.isNullOrEmpty(array[0])) {
							final Country country = new Country()
									.setTisN(Long.parseLong(array[0]))
									.setTisA(array[1])
									.setTisAExt(array[2])
									.setNome(array[3]);
							if (!numberToObjectMap.containsKey(country.getTisN())) {
								numberToObjectMap.put(country.getTisN(), country);
								names.put(country.getNome(), country.getTisN());
								logger.debug("added missing country: {}", missingCountry);
							}
						}
					} catch (Exception e) {
						logger.error("loadCountries", e);
					}
				}
				
				// build map containing (non empty) flat sets of countries for each country number
				countries = new HashMap<>();
				for (Long tisnr : numberToObjectMap.keySet()) {
					final Set<Country> countrySet = new HashSet<Country>();
					recursiveExpansion(countrySet, tisnr, gerarchie, numberToObjectMap);
					if (!countrySet.isEmpty())
						countries.put(tisnr, countrySet);
				}				
			}

			logger.debug("countries: {}", countries);

		}

	}
	
	public long getIdTerritorio(String nome) {
		Long idTerritorio = names.get(nome);
		return null == idTerritorio ? -1L : idTerritorio.longValue();
	}
	
	public synchronized Set<Country> getCountries(long key) {
		Set<Country> result = countries.get(key);
		if (null == result) {
			result = new HashSet<Country>();
			countries.put(key, result);
			logger.debug("unknown country tis number: {}", key);
		}
		return result;
	}

}
