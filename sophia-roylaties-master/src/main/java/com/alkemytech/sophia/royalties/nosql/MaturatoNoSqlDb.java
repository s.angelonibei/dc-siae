package com.alkemytech.sophia.royalties.nosql;

import java.io.File;
import java.nio.charset.Charset;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.mmap.ObjectCodec;
import com.alkemytech.sophia.commons.nosql.ConcurrentNoSql;
import com.alkemytech.sophia.commons.nosql.NoSql;
import com.alkemytech.sophia.commons.nosql.NoSqlConfig;
import com.alkemytech.sophia.commons.util.FileUtils;
import com.alkemytech.sophia.commons.util.TextUtils;


public class MaturatoNoSqlDb {

	private static class ThreadLocalObjectCodec extends ThreadLocal<ObjectCodec<Maturati>> {

		private final Charset charset;
		
	    public ThreadLocalObjectCodec(Charset charset) {
			super();
			this.charset = charset;
		}

		@Override
	    protected synchronized ObjectCodec<Maturati> initialValue() {
	        return new MaturatiCodec(charset);
	    }
	    
	}
	
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private NoSql<Maturati> database;

	public MaturatoNoSqlDb(Properties configuration, String propertyPrefix) {
		super();
		final File homeFolder = new File(configuration
				.getProperty(propertyPrefix + ".home_folder"));
		final boolean readOnly = "true".equalsIgnoreCase(configuration
				.getProperty(propertyPrefix + ".read_only", "true"));
		final int pageSize = TextUtils.parseIntSize(configuration
				.getProperty(propertyPrefix + ".page_size", "-1"));
		final int cacheSize = TextUtils.parseIntSize(configuration
				.getProperty(propertyPrefix + ".cache_size", "0"));
		final boolean createAlways = "true".equalsIgnoreCase(configuration
				.getProperty(propertyPrefix + ".create_always"));
		if (!readOnly && createAlways && homeFolder.exists())
			FileUtils.deleteFolder(homeFolder, true);
		logger.debug("propertyPrefix {}", propertyPrefix);
		database = new ConcurrentNoSql<>(new NoSqlConfig<Maturati>()
				.setHomeFolder(homeFolder)
				.setReadOnly(readOnly)
				.setPageSize(pageSize)
				.setCacheSize(cacheSize)
				.setCodec(new ThreadLocalObjectCodec(Charset.forName("UTF-8")))
				.setSecondaryIndex(false));
	}

	public void truncate() {
		database.truncate();
	}

	public Maturati get(String pk) {
		return database.getByPrimaryKey(pk);
	}

	public boolean put(Maturati bean) {
		database.upsert(bean);
		return true;
	}

}
