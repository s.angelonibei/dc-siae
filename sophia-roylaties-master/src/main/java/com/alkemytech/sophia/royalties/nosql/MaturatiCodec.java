package com.alkemytech.sophia.royalties.nosql;

import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.Date;

import com.alkemytech.sophia.commons.mmap.ObjectCodec;


public class MaturatiCodec extends ObjectCodec<Maturati> {

	private final Charset charset;
	
	public MaturatiCodec(Charset charset) {
		super();
		this.charset = charset;
	}

	@Override
	public Maturati bytesToObject(byte[] bytes) {
		beginDecoding(bytes);
		final long idAnagOpera = getPackedLong();
		final Maturati maturati = new Maturati(idAnagOpera);
		for (int size = getPackedInt(); size > 0; size --) {
			String periodoLabel = getString(charset);
			BigDecimal valore = getPackedBigDecimal();
			String dataDa = getString(charset);
			String dataA = getString(charset);
			Maturato maturatoObj = new Maturato(valore, periodoLabel, dataDa, dataA);
			maturati.add(maturatoObj);
		}
		return maturati;
	}

	@Override
	public byte[] objectToBytes(Maturati maturati) {
		beginEncoding();
		putPackedLong(maturati.idAnagOpera);
		putPackedInt(maturati.maturati.size());
		for (Maturato maturato: maturati.maturati) {
			putString(maturato.periodoLabel, charset);
			putPackedBigDecimal(maturato.valore);
			putString(maturato.dataDa, charset);
			putString(maturato.dataA, charset);
		}
		return commitEncoding();
	}
	
}
