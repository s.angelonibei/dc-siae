package com.alkemytech.sophia.royalties.nosql;

import java.util.HashSet;
import java.util.Set;

import com.alkemytech.sophia.commons.nosql.NoSqlEntity;
import com.google.common.base.Strings;
import com.google.gson.GsonBuilder;


public class Maturati implements NoSqlEntity {

	public final long idAnagOpera;
	public final Set<Maturato> maturati;



	public Maturati(long idAnagOpera) {
		super();
		this.idAnagOpera = idAnagOpera;
		this.maturati = new HashSet<>();
	}

	public void add(Maturato maturato) {
		if (null == maturato)
			return;
		maturati.add(maturato);
	}

	@Override
	public String getPrimaryKey() {
		return Long.toString(idAnagOpera);
	}

	@Override
	public String getSecondaryKey() {
		return null;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}
	
}
