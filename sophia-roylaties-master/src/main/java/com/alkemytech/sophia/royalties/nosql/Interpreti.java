package com.alkemytech.sophia.royalties.nosql;

import java.util.HashSet;
import java.util.Set;

import com.alkemytech.sophia.commons.nosql.NoSqlEntity;
import com.google.common.base.Strings;
import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class Interpreti implements NoSqlEntity {

	public final long idAnagOpera;
	public final Set<Interprete> interpreti;
	
	public Interpreti(long idAnagOpera) {
		super();
		this.idAnagOpera = idAnagOpera;
		this.interpreti = new HashSet<>();
	}

	public void add(String interprete, String tipo) {
		if (null == interprete)
			return;
		interprete = interprete.trim();
		if (Strings.isNullOrEmpty(interprete))
			return;
		if (Strings.isNullOrEmpty(tipo))
			return;
		interpreti.add(new Interprete(interprete, tipo));
	}
	
	@Override
	public String getPrimaryKey() {
		return Long.toString(idAnagOpera);
	}

	@Override
	public String getSecondaryKey() {
		return null;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}
	
}
