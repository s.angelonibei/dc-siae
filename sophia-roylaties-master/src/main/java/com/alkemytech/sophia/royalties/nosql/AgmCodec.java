package com.alkemytech.sophia.royalties.nosql;

import com.alkemytech.sophia.commons.mmap.MemoryMapException;
import com.alkemytech.sophia.commons.mmap.ObjectCodec;

import java.nio.charset.Charset;

/**
 * Created by Alessandro Russo on 25/01/2019.
 */
public class AgmCodec extends ObjectCodec<Agm> {

    private final Charset charset;

    public AgmCodec(Charset charset) {
        super();
        this.charset = charset;
    }

    @Override
    public Agm bytesToObject(byte[] bytes) throws MemoryMapException {
        beginDecoding(bytes);
        final String ipbasenr = getString(charset);
        final Agm agm = new Agm(ipbasenr);
        for (int size = getPackedInt(); size > 0; size --) {
            final String agmid = getString(charset);
            final String rgt = getString(charset);
            final String rol = getString(charset);
            final String society = getString(charset);
            final String country = getString(charset);
            agm.add(agmid, rgt, rol, society, country);
        }
        return agm;
    }

    @Override
    public byte[] objectToBytes(Agm agm) throws MemoryMapException {
        beginEncoding();
        putString(agm.ipbasenr, charset);
        putPackedInt(agm.agmids.size());
        for (int i = agm.agmids.size() - 1; i >= 0; i --) {
            putString(agm.agmids.get(i), charset);
            putString(agm.rgts.get(i), charset);
            putString(agm.rols.get(i), charset);
            putString(agm.societies.get(i), charset);
            putString(agm.countries.get(i), charset);
        }
        return commitEncoding();
    }
}
