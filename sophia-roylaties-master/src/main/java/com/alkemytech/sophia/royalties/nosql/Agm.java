package com.alkemytech.sophia.royalties.nosql;

import com.alkemytech.sophia.commons.nosql.NoSqlEntity;
import com.google.common.base.Strings;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alessandro Russo on 25/01/2019.
 */
public class Agm implements NoSqlEntity {

    public final String ipbasenr;
    public final List<String> agmids;
    public final List<String> rgts;
    public final List<String> rols;
    public final List<String> societies;
    public final List<String> countries;

    public Agm(String ipbasenr) {
        super();
        this.ipbasenr = ipbasenr;
        this.agmids = new ArrayList<>();
        this.rgts = new ArrayList<>();
        this.rols = new ArrayList<>();
        this.societies = new ArrayList<>();
        this.countries = new ArrayList<>();
    }

    public void add(String agmid, String rgt, String rol, String society, String country) {
        if (Strings.isNullOrEmpty(agmid))
            return;
        if (Strings.isNullOrEmpty(rgt))
            return;
        if (Strings.isNullOrEmpty(rol))
            return;
        agmids.add(agmid);
        rgts.add(rgt);
        rols.add(rol);
        societies.add(Strings.isNullOrEmpty(society) ? "" : society);
        countries.add(Strings.isNullOrEmpty(country) ? "" : country);
    }

    @Override
    public String getPrimaryKey() {
        return ipbasenr;
    }

    @Override
    public String getSecondaryKey() {
        return null;
    }

    @Override
    public String toString() {
        return new GsonBuilder()
                .create().toJson(this);
    }
}
