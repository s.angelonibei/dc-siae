package com.alkemytech.sophia.royalties.nosql;

import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class Interprete {
	
	public final String nominativo;
	public final String tipo;

	public Interprete(String nominativo, String tipo) {
		super();
		this.nominativo = nominativo;
		this.tipo = tipo;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}

}
