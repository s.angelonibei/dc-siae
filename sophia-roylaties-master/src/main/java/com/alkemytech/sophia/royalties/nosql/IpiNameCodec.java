package com.alkemytech.sophia.royalties.nosql;

import java.nio.charset.Charset;

import com.alkemytech.sophia.commons.mmap.ObjectCodec;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class IpiNameCodec extends ObjectCodec<IpiName> {

	private final Charset charset;
	
	public IpiNameCodec(Charset charset) {
		super();
		this.charset = charset;
	}

	@Override
	public IpiName bytesToObject(byte[] bytes) {
		beginDecoding(bytes);
		final String ipbasenr = getString(charset);
		final IpiName ipiName = new IpiName(ipbasenr);
		for (int size = getPackedInt(); size > 0; size --) {
			final String ipnamenr = getString(charset);
			final String name = getString(charset);
			final String fstname = getString(charset);
			final String lstname = getString(charset);
			final String nametyp = getString(charset);
			ipiName.add(ipnamenr, name, fstname, lstname, nametyp);
		}
		return ipiName;
	}

	@Override
	public byte[] objectToBytes(IpiName ipiName) {
		beginEncoding();
		putString(ipiName.ipbasenr, charset);
		putPackedInt(ipiName.ipnamenrs.size());
		for (int i = ipiName.ipnamenrs.size() - 1; i >= 0; i --) {
			putString(ipiName.ipnamenrs.get(i), charset);
			putString(ipiName.names.get(i), charset);
			putString(ipiName.fstnames.get(i), charset);
			putString(ipiName.lstnames.get(i), charset);
			putString(ipiName.nametyps.get(i), charset);
		}
		return commitEncoding();
	}
	
}
