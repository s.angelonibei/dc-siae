package com.alkemytech.sophia.royalties.nosql;

import java.util.ArrayList;
import java.util.List;

import com.alkemytech.sophia.commons.nosql.NoSqlEntity;
import com.google.common.base.Strings;
import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class IpiName implements NoSqlEntity {

	public final String ipbasenr;
	public final List<String> ipnamenrs;
	public final List<String> names;
	public final List<String> fstnames;
	public final List<String> lstnames;
	public final List<String> nametyps;
	
	public IpiName(String ipbasenr) {
		super();
		this.ipbasenr = ipbasenr;
		this.ipnamenrs = new ArrayList<>();
		this.names = new ArrayList<>();
		this.fstnames = new ArrayList<>();
		this.lstnames = new ArrayList<>();
		this.nametyps = new ArrayList<>();
	}
	
	public void add(String ipnamenr, String name, String fstname, String lstname, String nametyp) {
		if (Strings.isNullOrEmpty(ipnamenr))
			return;
		if (Strings.isNullOrEmpty(name))
			return;
		ipnamenrs.add(ipnamenr);
		names.add(name);
		fstnames.add(Strings.isNullOrEmpty(fstname) ? "" : fstname);
		lstnames.add(Strings.isNullOrEmpty(lstname) ? "" : lstname);
		nametyps.add(Strings.isNullOrEmpty(nametyp) ? "" : nametyp);
	}

	@Override
	public String getPrimaryKey() {
		return ipbasenr;
	}

	@Override
	public String getSecondaryKey() {
		return null;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}
	
}
