package com.alkemytech.sophia.royalties.nosql;

import com.alkemytech.sophia.commons.mmap.MemoryMapException;
import com.alkemytech.sophia.commons.mmap.ObjectCodec;

import java.nio.charset.Charset;

/**
 * Created by Alessandro Russo on 25/01/2019.
 */
public class ContrattiCodec extends ObjectCodec<Contratti> {

    private final Charset charset;

    public ContrattiCodec(Charset charset) {
        super();
        this.charset = charset;
    }

    @Override
    public Contratti bytesToObject(byte[] bytes) throws MemoryMapException {
        beginDecoding(bytes);
        final long idSchema = getPackedLong();
        final Contratti contract = new Contratti(idSchema);
        for (int size = getPackedInt(); size > 0; size --) {
            final String codiceContratto = getString(charset);
            final String dataScadenza = getString(charset);
            final String tipoContratto = getString(charset);
            final String statoContratto = getString(charset);
            final String territorio = getString(charset);
            final String codiceSchedula = getString(charset);
            contract.add( codiceContratto, dataScadenza, tipoContratto, statoContratto,territorio, codiceSchedula);
        }
        return contract;
    }

    @Override
    public byte[] objectToBytes(Contratti contract) throws MemoryMapException {
        beginEncoding();
        putPackedLong(contract.idSchema);
        putPackedInt(contract.codiciContratto.size());
        for (int i = contract.codiciContratto.size() - 1; i >= 0; i --) {
            putString(contract.codiciContratto.get(i), charset);
            putString(contract.dateScadenza.get(i), charset);
            putString(contract.tipiContratto.get(i), charset);
            putString(contract.statiContratto.get(i), charset);
            putString(contract.territori.get(i), charset);
            putString(contract.codiciSchedula.get(i), charset);
        }
        return commitEncoding();
    }
}
