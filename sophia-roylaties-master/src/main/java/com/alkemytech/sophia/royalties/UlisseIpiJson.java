package com.alkemytech.sophia.royalties;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.net.ServerSocket;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.sql.DataSource;

import com.alkemytech.sophia.royalties.nosql.*;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.io.CompressionAwareFileOutputStream;
import com.alkemytech.sophia.commons.mmap.UnsafeMemoryMap;
import com.alkemytech.sophia.commons.trie.ConcurrentTrie;
import com.alkemytech.sophia.commons.trie.FixedLengthStringBinding;
import com.alkemytech.sophia.commons.trie.Trie;
import com.alkemytech.sophia.commons.util.BigDecimals;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.HeartBeat;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.royalties.jdbc.IpiDataSource;
import com.alkemytech.sophia.royalties.jdbc.UlisseDataSource;
import com.google.common.base.Strings;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Scopes;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class UlisseIpiJson {
	
	private static final Logger logger = LoggerFactory.getLogger(UlisseIpiJson.class);

	protected static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// amazon service(s)
			bind(S3.class)
				.in(Scopes.SINGLETON);
			bind(SQS.class)
				.in(Scopes.SINGLETON);
			// data source(s)
			bind(DataSource.class)
				.annotatedWith(Names.named("ULISSE"))
				.to(UlisseDataSource.class)
				.asEagerSingleton();
			bind(DataSource.class)
				.annotatedWith(Names.named("IPI"))
				.to(IpiDataSource.class)
				.asEagerSingleton();
			// other binding(s)
			bind(UlisseIpiJson.class)
				.asEagerSingleton();
		}

	}

	public static void main(String[] args) {
		try {
			final UlisseIpiJson instance = Guice
					.createInjector(new GuiceModuleExtension(args,
							"/ulisse-ipi-json.properties"))
				.getInstance(UlisseIpiJson.class)
					.startup();
			try {
				instance.process(args);
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}

	private final Properties configuration;
	private final Charset charset;
	private final S3 s3;
	private final SQS sqs;
	private final DataSource ulisseDataSource;
	private final DataSource ipiDataSource;
	private final Set<Long> missingIpnamenrs;

	@Inject
	protected UlisseIpiJson(@Named("configuration") Properties configuration,
			@Named("charset") Charset charset,
			S3 s3, SQS sqs, 
			@Named("ULISSE") DataSource ulisseDataSource,
			@Named("IPI") DataSource ipiDataSource) {
		super();
		this.configuration = configuration;
		this.charset = charset;
		this.s3 = s3;
		this.sqs = sqs;
		this.ulisseDataSource = ulisseDataSource;
		this.ipiDataSource = ipiDataSource;
		this.missingIpnamenrs = new HashSet<>();
	}

	public UlisseIpiJson startup() throws IOException {
		if ("true".equalsIgnoreCase(configuration.getProperty("ulisse_ipi_json.locked", "true"))) {
			throw new IllegalStateException("application locked");
		}
		s3.startup();
		sqs.startup();
		return this;
	}

	public UlisseIpiJson shutdown() throws IOException {
		sqs.shutdown();
		s3.shutdown();
		return this;
	}

	private void addArrayOfObjects(JsonObject record, String label, Collection<String> texts, String textLabel) {
		if (null != texts && texts.size() > 0) {
			final JsonArray jsonArray = new JsonArray();
			for (String text : texts) {
				if (!Strings.isNullOrEmpty(text)) {
					text = text.trim();
					if (!Strings.isNullOrEmpty(text)) {
						final JsonObject jsonObject = new JsonObject();
						jsonObject.addProperty(textLabel, text);
						jsonArray.add(jsonObject);
					}
				}
			}
			record.add(label, jsonArray);
		}
	}

    private void addAnagrafica(JsonObject recordJsonObject, Anagrafica anagrafica ){

        recordJsonObject.addProperty("idSchema", anagrafica.idSchema);
        recordJsonObject.addProperty("idSchemaFlat", anagrafica.idSchemaFlat);
        recordJsonObject.addProperty("idOpera", anagrafica.idOpera);
        recordJsonObject.addProperty("codiceOpera", anagrafica.codiceOpera);
        recordJsonObject.addProperty("dataDichiarazione", anagrafica.dataDichiarazione);
        recordJsonObject.addProperty("dataDichiarazioneSiada", anagrafica.dataDichiarazioneSiada);
        recordJsonObject.addProperty("dataAcquisizione", anagrafica.dataAcquisizione);
        recordJsonObject.addProperty("dataPubblicoDominio", anagrafica.dataPubblicoDominio);
        recordJsonObject.addProperty("tipologiaDeposito", anagrafica.tipologiaDeposito);
        recordJsonObject.addProperty("musica", anagrafica.musica);
        recordJsonObject.addProperty("testo", anagrafica.testo);
        recordJsonObject.addProperty("esemplare", anagrafica.esemplare);
        recordJsonObject.addProperty("genereOpera", anagrafica.genereOpera);
        recordJsonObject.addProperty("durata", anagrafica.durata);
        recordJsonObject.addProperty("notizieParticolari", anagrafica.notizieParticolari);
        recordJsonObject.addProperty("tipoBollettino", anagrafica.tipoBollettino);
        recordJsonObject.addProperty("codProvvEditore", anagrafica.codProvvEditore);
        recordJsonObject.addProperty("riferimento", anagrafica.riferimento);
        recordJsonObject.addProperty("flagPD", anagrafica.flagPD);
        recordJsonObject.addProperty("flagEL", anagrafica.flagEL);
        recordJsonObject.addProperty("flagSempreverdi", anagrafica.flagSempreverdi);
        recordJsonObject.addProperty("flagMaggiorazione", anagrafica.flagMaggiorazione);
        recordJsonObject.addProperty("flagAccettataConfusoria", anagrafica.flagAccettataConfusoria);
        recordJsonObject.addProperty("ambito", anagrafica.ambito);
        recordJsonObject.addProperty("statoOpera", anagrafica.statoOpera);
        recordJsonObject.addProperty("nPacco", anagrafica.nPacco);
        recordJsonObject.addProperty("inviante", anagrafica.inviante);
        recordJsonObject.addProperty("rilevanza", anagrafica.rilevanza);
        recordJsonObject.addProperty("maturato", anagrafica.maturato);
        recordJsonObject.addProperty("rischio", anagrafica.rischio);
        recordJsonObject.addProperty("semestreScadenza", anagrafica.semestreScadenza);
        recordJsonObject.addProperty("dataSemScadenza", anagrafica.dataSemScadenza);
        recordJsonObject.addProperty("semestreDichiarazione", anagrafica.semestreDichiarazione);
        recordJsonObject.addProperty("dataSemDichiarazione", anagrafica.dataSemDichiarazione);
        recordJsonObject.addProperty("semestreValidita", anagrafica.semestreValidita);
        recordJsonObject.addProperty("dataSemValidita", anagrafica.dataSemValidita);
        recordJsonObject.addProperty("tipoUtilizzo", anagrafica.tipoUtilizzo);
        recordJsonObject.addProperty("territorio", anagrafica.territorio);
        recordJsonObject.addProperty("ricercabile", anagrafica.ricercabile);
        recordJsonObject.addProperty("idCollectingCompany", anagrafica.idCollectingCompany);
    }

    private void addCollegamenti(JsonObject record, Collegamenti collegamenti, TitoliNoSqlDb titoliNoSql) {
        final JsonArray collegamentiJsonArray = new JsonArray();
        for( Collegamento collegamento : collegamenti.collegamenti ){
            final JsonObject collegamentoJsonObject = new JsonObject();
            collegamentoJsonObject.addProperty( "codice", collegamento.codiceOperaCollegata);
            collegamentoJsonObject.addProperty("tipoCollegamento", collegamento.tipoCollegamento);
            Titoli titoli = titoliNoSql.get( Long.toString(collegamento.idOperaCollegata) );
            if( titoli != null ){
                Set<String> titoloOriginale = new HashSet<>();
                Set<String> titoloAlternativo = new HashSet<>();
                for(Titolo titolo : titoli.titoli){
                    if("OT".equalsIgnoreCase(titolo.tipoTitolo)){
                        titoloOriginale.add(titolo.titolo);
                    }
                    else{
                        titoloAlternativo.add(titolo.titolo);
                    }
                }
                if( titoloOriginale.size() > 0 ){
                    collegamentoJsonObject.addProperty("titolo", StringUtils.join(titoloOriginale, ","));
                }
                else{
                    collegamentoJsonObject.addProperty("titolo", StringUtils.join(titoloAlternativo, ","));
                }
                collegamentiJsonArray.add(collegamentoJsonObject);
            }
        }
        record.add("collegamenti", collegamentiJsonArray);
    }

    private void addContratti(JsonObject record, Contratti contratti) {
        final JsonArray contrattiJsonArray = new JsonArray();
        for( int i = 0 ; i< contratti.codiciContratto.size(); i++ ){
            final JsonObject contrattoJsonObject = new JsonObject();
            contrattoJsonObject.addProperty("codiceContratto", contratti.codiciContratto.get(i));
            contrattoJsonObject.addProperty("dataScadenza", contratti.dateScadenza.get(i));
            contrattoJsonObject.addProperty("tipiContratto", contratti.tipiContratto.get(i));
            contrattoJsonObject.addProperty("statiContratto", contratti.statiContratto.get(i));
            contrattoJsonObject.addProperty("territori", contratti.territori.get(i));
            contrattoJsonObject.addProperty("codiciSchedula", contratti.codiciSchedula.get(i));
            contrattiJsonArray.add(contrattoJsonObject);
        }
        record.add("contratti", contrattiJsonArray);
    }

    private void addAudiovisivi(JsonObject record, Audiovisivi audiovisivi) {
        final JsonArray audiovisiviJsonArray = new JsonArray();
        for( int i = 0 ; i< audiovisivi.codici.size(); i++ ){
            final JsonObject audiovisivoJsonObject = new JsonObject();
            audiovisivoJsonObject.addProperty("codice", audiovisivi.codici.get(i));
            audiovisivoJsonObject.addProperty("regista", audiovisivi.registi.get(i));
            audiovisivoJsonObject.addProperty("anno", audiovisivi.anni.get(i));
            audiovisivoJsonObject.addProperty("titoloSerie", audiovisivi.titoliSerie.get(i));
            audiovisivoJsonObject.addProperty("titolo", audiovisivi.titoli.get(i));
            audiovisiviJsonArray.add(audiovisivoJsonObject);
        }
        record.add("audiovisivi", audiovisiviJsonArray);
    }


    private void addTitoli(JsonObject record, Titoli titoli) {
        final JsonArray titoliOriginaliJsonArray = new JsonArray();
        final JsonArray titoliAlternativiJsonArray = new JsonArray();
        for( Titolo titolo : titoli.titoli ){
            final JsonObject titoloJsonObject = new JsonObject();
            titoloJsonObject.addProperty("titolo", titolo.titolo);
            titoloJsonObject.addProperty("lingua", titolo.lingua);
            if( "OT".equalsIgnoreCase(titolo.tipoTitolo)){
                titoliOriginaliJsonArray.add(titoloJsonObject);
            }
            else{
                titoliAlternativiJsonArray.add(titoloJsonObject);
            }
        }
        record.add("titoliOt", titoliOriginaliJsonArray);
        record.add("titoliAt", titoliAlternativiJsonArray);
    }

    private Set<String> getSocietaAppartenenza(String tipoQuota, String qualificaIpi, Agm agm ){
        Set<String> societies = new HashSet<>();
        for (int i = 0 ; i < agm.agmids.size(); i++ ){
            if (agm.rgts.get(i).equalsIgnoreCase(tipoQuota) &&
                    agm.rols.get(i).equalsIgnoreCase(qualificaIpi)){
                societies.add(agm.societies.get(i));
            }
        }
        return societies;
    }

    private Map<String, Set<String>> addQuoteRiparto(JsonObject record, Trie<String> ipiNameReverseTrie, AgmNoSqlDb agmNoSqlDb, Quote quote) {
        final Map<String, Set<String>> codiciIpiConQualifiche = new HashMap<>();
        final JsonArray quoteRipartoJsonArray = new JsonArray();
        for (Quota quota : quote.quote) {
            final JsonObject quotaRipartoJsonObject = new JsonObject();
            quotaRipartoJsonObject.addProperty("nominativo", quota.nominativo);
            quotaRipartoJsonObject.addProperty("nome", quota.nome);
            quotaRipartoJsonObject.addProperty("cognome", quota.cognome);
            quotaRipartoJsonObject.addProperty("posizioneSiae", quota.posizioneSiae);
            quotaRipartoJsonObject.addProperty("codiceIpi", quota.codiceIpi);
            quotaRipartoJsonObject.addProperty("codiceConto", quota.codiceConto);
            quotaRipartoJsonObject.addProperty("codiceContoEstero", quota.codiceContoEstero);
            quotaRipartoJsonObject.addProperty("qualifica", quota.qualifica);
            quotaRipartoJsonObject.addProperty("tipoQuota", quota.tipoQuota);
            quotaRipartoJsonObject.addProperty("qualificaIpi", quota.qualificaIpi);
            quotaRipartoJsonObject.addProperty("quotaNumeratore", quota.quotaNumeratore);
            quotaRipartoJsonObject.addProperty("quotaDenominatore", quota.quotaDenominatore);
            quotaRipartoJsonObject.addProperty("quotaAd", quota.quotaAd);

            quoteRipartoJsonArray.add(quotaRipartoJsonObject);

            if (!Strings.isNullOrEmpty(quota.codiceIpi)) {
                Set<String> qualifiche = codiciIpiConQualifiche.get(quota.codiceIpi);
                if (null == qualifiche) {
                    qualifiche = new HashSet<>();
                    codiciIpiConQualifiche.put(quota.codiceIpi, qualifiche);
                }
                if (!Strings.isNullOrEmpty(quota.qualifica))
                    qualifiche.add(quota.qualifica);

                final String ipbasenr = ipiNameReverseTrie.find(quota.codiceIpi);
                if (!Strings.isNullOrEmpty(ipbasenr)) {
                    Agm agm = agmNoSqlDb.get(ipbasenr);
                    if( null != agm ){
                        Set<String> societies = getSocietaAppartenenza(quota.tipoQuota, quota.qualificaIpi, agm);
                        quotaRipartoJsonObject.addProperty("societa" , StringUtils.join(societies, ","));
                        quotaRipartoJsonObject.addProperty("societaLabel", societies.size() > 1 ? "MULTI" : StringUtils.join(societies, ","));
                    }
                    else{
                        if(null != quota.posizioneSiae){
                            if( quota.codiceConto == "09" || quota.codiceConto == "55" || quota.codiceConto == "56" ){
                                quotaRipartoJsonObject.addProperty("societa" , "NON SOCIO");
                                quotaRipartoJsonObject.addProperty("societaLabel", "NON SOCIO");
                            }
                            else{
                                quotaRipartoJsonObject.addProperty("societa" , "SIAE");
                                quotaRipartoJsonObject.addProperty("societaLabel", "SIAE");
                            }
                        }
                        else{
                            quotaRipartoJsonObject.addProperty("societa" , "NON SOCIO");
                            quotaRipartoJsonObject.addProperty("societaLabel", "NON SOCIO");
                        }

                    }
                }

            }
        }
        record.add("quoteRiparto", quoteRipartoJsonArray);
        return codiciIpiConQualifiche;
    }

    private void addIpiObject(JsonObject record, Trie<String> ipiNameReverseTrie, IpiNameNoSqlDb ipiNameNoSql, Map<String, Set<String>> codiciIpiConQualifiche) {
        final JsonArray ipiJsonArray = new JsonArray();
        for (Map.Entry<String, Set<String>> codiceIpiConQualifiche : codiciIpiConQualifiche.entrySet()) {
            final String ipnamenr = codiceIpiConQualifiche.getKey();
            final long namenr;
            try {
                namenr = Long.parseLong(ipnamenr);
            } catch (NumberFormatException e) {
                logger.error("ipnamenr is not a number {}", ipnamenr);
                continue;
            }
            if (missingIpnamenrs.contains(namenr))
                continue;
            final String ipbasenr = ipiNameReverseTrie.find(ipnamenr);
            if (!Strings.isNullOrEmpty(ipbasenr)) {
                IpiName ipiName = ipiNameNoSql.get(ipbasenr);
                if (null != ipiName) {
                    final JsonObject ipiJsonObject = new JsonObject();
                    ipiJsonObject.addProperty("codiceIpiQuota", ipnamenr);
                    ipiJsonObject.add("qualifiche", GsonUtils
                            .newJsonArray(codiceIpiConQualifiche.getValue()));
                    ipiJsonObject.addProperty("ipiBaseNumber", ipiName.ipbasenr);
                    final JsonArray codiciIpiNn = new JsonArray();
                    for (int i = ipiName.ipnamenrs.size() - 1; i >= 0; i --) {
                        final JsonObject codiceIpiNn = new JsonObject();
                        codiceIpiNn.addProperty("ipiNameNumber", ipiName.ipnamenrs.get(i));
                        codiceIpiNn.addProperty("nominativo", ipiName.names.get(i));
                        codiceIpiNn.addProperty("nome", ipiName.fstnames.get(i));
                        codiceIpiNn.addProperty("cognome", ipiName.lstnames.get(i));
                        codiceIpiNn.addProperty("tipoNominativo", ipiName.nametyps.get(i));
                        codiciIpiNn.add(codiceIpiNn);
                    }
                    ipiJsonObject.add("codiciIpiNameNumber", codiciIpiNn);
                    ipiJsonArray.add(ipiJsonObject);
                } else {
                    logger.warn("ipbasenr not found {}", ipbasenr);
                }
            } else {
                missingIpnamenrs.add(namenr);
//				logger.warn("ipnamenr not found {}", ipnamenr);
            }
        }
        record.add("ipi", ipiJsonArray);
    }
	
	public void process(String[] args) throws Exception {
		final long processStartTimeMillis = System.currentTimeMillis();
		final int bindPort = Integer.parseInt(configuration.getProperty("ulisse_ipi_json.bind_port",
				configuration.getProperty("default.bind_port", "0")));
		// bind lock tcp port
		try (ServerSocket socket = new ServerSocket(bindPort)) {
			logger.debug("socket bound to {}", socket.getLocalSocketAddress());

			final Set<String> steps = new HashSet<>(Arrays.asList(TextUtils
					.split(configuration.getProperty("ulisse_ipi_json.steps"), ",")));
			logger.debug("steps {}", steps);

            /**
             * If you don't want run any step just remove it from properties file.
             */

            ////////////////////////////////////////////////////////////////////////////////
            // operaschematerr
            ////////////////////////////////////////////////////////////////////////////////
            if(steps.contains("operaschematerr")){
                final long startTimeMillis = System.currentTimeMillis();
                // connect to database
                try (final Connection connection = ulisseDataSource.getConnection()) {
                    logger.debug("jdbc connected to {} {}", connection.getMetaData()
                            .getDatabaseProductName(), connection.getMetaData().getURL());

                    final Statement truncateStatement = connection.createStatement();
                    // truncate Table
                    final String truncateSql = configuration.getProperty("ulisse_ipi_json.operaschematerr.truncate");
                    logger.debug("truncate sql {}", truncateSql);
                    truncateStatement.executeUpdate(truncateSql);
                    logger.debug("table truncated in {}", TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));

                    final Statement insertStatement = connection.createStatement();
                    // populate Table
                    final String insertSql = configuration.getProperty("ulisse_ipi_json.operaschematerr.insert");
                    logger.debug("insert sql {}", insertSql);
                    truncateStatement.executeUpdate(insertSql);
                    logger.debug("table populated in {}", TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));

                }
                logger.debug("operaschematerr export completed in {}", TextUtils
                        .formatDuration(System.currentTimeMillis() - startTimeMillis));
            }

			////////////////////////////////////////////////////////////////////////////////
			// ipi
			////////////////////////////////////////////////////////////////////////////////
			if (steps.contains("ipi")) {
				configuration.setProperty("ulisse_ipi_json.ipi.read_only", "false");
				configuration.setProperty("ulisse_ipi_json.ipi.create_always", "true");
				configuration.setProperty("ulisse_ipi_json.ipi_reverse.read_only", "false");
			} else {
				configuration.setProperty("ulisse_ipi_json.ipi.page_size", "-1");
				configuration.setProperty("ulisse_ipi_json.ipi.read_only", "true");
				configuration.setProperty("ulisse_ipi_json.ipi.create_always", "false");
				configuration.setProperty("ulisse_ipi_json.ipi_reverse.read_only", "true");
			}
			
			// ipi nosql
			final IpiNameNoSqlDb ipiNameNoSql = new IpiNameNoSqlDb(configuration, "ulisse_ipi_json.ipi");

			// ipi_reverse trie
			final Trie<String> ipiNameReverseTrie;
			if ("true".equalsIgnoreCase(configuration
						.getProperty("ulisse_ipi_json.ipi_reverse.read_only", "true"))) {
				ipiNameReverseTrie = ConcurrentTrie.wrap(UnsafeMemoryMap.open(new File(configuration
						.getProperty("ulisse_ipi_json.ipi_reverse.home_folder")), -1, true),
							new FixedLengthStringBinding(13, charset)); // "H-NNNNNNNNN-C"
			} else {
				ipiNameReverseTrie = ConcurrentTrie.wrap(UnsafeMemoryMap.create(new File(configuration
						.getProperty("ulisse_ipi_json.ipi_reverse.home_folder")), TextUtils
						.parseIntSize(configuration.getProperty("ulisse_ipi_json.ipi_reverse.page_size"))),
							new FixedLengthStringBinding(13, charset)); // "H-NNNNNNNNN-C"
			}

			// ipi
			if (steps.contains("ipi")) {
				final long startTimeMillis = System.currentTimeMillis();

				// connect to database
				try (final Connection connection = ipiDataSource.getConnection()) {
					logger.debug("jdbc connected to {} {}", connection.getMetaData()
							.getDatabaseProductName(), connection.getMetaData().getURL());

					// setup one-by-one rows retrieval
					final Statement statement;
					final int fetchSize = Integer.parseInt(configuration
							.getProperty("ulisse_ipi_json.ipi.fetch_size", "-1"));
					if (-1 == fetchSize) {
						statement = connection.createStatement();
					} else {
						statement = connection
								.createStatement(ResultSet.TYPE_FORWARD_ONLY,
										ResultSet.CONCUR_READ_ONLY);
						statement.setFetchSize(fetchSize);
					}

					// query
					final String sql = configuration.getProperty("ulisse_ipi_json.ipi.sql");
					logger.debug("sql {}", sql);
					try (final ResultSet resultSet = statement.executeQuery(sql)) {
						logger.debug("query executed in {}", TextUtils
								.formatDuration(System.currentTimeMillis() - startTimeMillis));
						final HeartBeat heartbeat = HeartBeat.constant("ipi", Integer
								.parseInt(configuration.getProperty("ulisse_ipi_json.ipi.heartbeat",
										configuration.getProperty("default.heartbeat", "10000"))));

						// fetch rows one-by-one
						String lastIpbasenr = null;
						IpiName ipiName = null;
						while (resultSet.next()) {
							final String ipbasenr = resultSet.getString("ipbasenr");
							final String ipnamenr = resultSet.getString("ipnamenr");
							final String name = resultSet.getString("name");
							final String fstname = resultSet.getString("fstname");
							final String lstname = resultSet.getString("lstname");
							final String nametyp = resultSet.getString("nametyp");

							if (!ipbasenr.equals(lastIpbasenr)) {
								if (null != ipiName) {
									// upsert bean
									ipiNameNoSql.put(ipiName);
									// upsert reverse reference(s)
									for (String namenr : ipiName.ipnamenrs) {
										ipiNameReverseTrie.upsert(namenr, ipiName.ipbasenr);
									}
								}
								lastIpbasenr = ipbasenr;
								ipiName = new IpiName(ipbasenr);
							}

							ipiName.add(ipnamenr, name, fstname, lstname, nametyp);

							heartbeat.pump();
						}
						if (null != ipiName) {
							// upsert bean
							ipiNameNoSql.put(ipiName);
							// upsert reverse reference(s)
							for (String namenr : ipiName.ipnamenrs) {
								ipiNameReverseTrie.upsert(namenr, ipiName.ipbasenr);
							}
						}
						logger.debug("total ipi(s) {}", heartbeat.getTotalPumps());

					}

				}

				logger.debug("ipi export completed in {}", TextUtils
						.formatDuration(System.currentTimeMillis() - startTimeMillis));
			}

            ////////////////////////////////////////////////////////////////////////////////
            // agm
            ////////////////////////////////////////////////////////////////////////////////
            if (steps.contains("agm")) {
                configuration.setProperty("ulisse_ipi_json.agm.read_only", "false");
                configuration.setProperty("ulisse_ipi_json.agm.create_always", "true");
            } else {
                configuration.setProperty("ulisse_ipi_json.agm.page_size", "-1");
                configuration.setProperty("ulisse_ipi_json.agm.read_only", "true");
                configuration.setProperty("ulisse_ipi_json.agm.create_always", "false");
            }

            // agm nosql
            final AgmNoSqlDb agmNoSql = new AgmNoSqlDb(configuration, "ulisse_ipi_json.agm");

            if (steps.contains("agm")) {
                final long startTimeMillis = System.currentTimeMillis();

                // connect to database
                try (final Connection connection = ipiDataSource.getConnection()) {
                    logger.debug("jdbc connected to {} {}", connection.getMetaData()
                            .getDatabaseProductName(), connection.getMetaData().getURL());

                    // setup one-by-one rows retrieval
                    final Statement statement;
                    final int fetchSize = Integer.parseInt(configuration
                            .getProperty("ulisse_ipi_json.agm.fetch_size", "-1"));
                    if (-1 == fetchSize) {
                        statement = connection.createStatement();
                    } else {
                        statement = connection
                                .createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                        ResultSet.CONCUR_READ_ONLY);
                        statement.setFetchSize(fetchSize);
                    }

                    // query
                    final String sql = configuration.getProperty("ulisse_ipi_json.agm.sql");
                    logger.debug("sql {}", sql);
                    try (final ResultSet resultSet = statement.executeQuery(sql)) {
                        logger.debug("query executed in {}", TextUtils
                                .formatDuration(System.currentTimeMillis() - startTimeMillis));
                        final HeartBeat heartbeat = HeartBeat.constant("agm", Integer
                                .parseInt(configuration.getProperty("ulisse_ipi_json.agm.heartbeat",
                                        configuration.getProperty("default.heartbeat", "10000"))));

                        // fetch rows one-by-one
                        String lastIpbasenr = null;
                        Agm agm = null;
                        while (resultSet.next()) {
                            final String ipbasenr = resultSet.getString("ipbasenr");
                            final String agmid = resultSet.getString("agmid");
                            final String rgt = resultSet.getString("rgt");
                            final String rol = resultSet.getString("rol");
                            final String society = resultSet.getString("society");
                            final String country = resultSet.getString("country");

                            if (!ipbasenr.equals(lastIpbasenr)) {
                                if (null != agm) {
                                    // upsert bean
                                    agmNoSql.put(agm);
                                }
                                lastIpbasenr = ipbasenr;
                                agm = new Agm(ipbasenr);
                            }

                            agm.add(agmid, rgt, rol, society, country);

                            heartbeat.pump();
                        }
                        if (null != agm) {
                            // upsert bean
                            agmNoSql.put(agm);
                        }
                        logger.debug("total agm(s) {}", heartbeat.getTotalPumps());

                    }

                }

                logger.debug("agreements export completed in {}", TextUtils
                        .formatDuration(System.currentTimeMillis() - startTimeMillis));
            }
			
			////////////////////////////////////////////////////////////////////////////////
			// codici
			////////////////////////////////////////////////////////////////////////////////
			if (steps.contains("codici")) {
				configuration.setProperty("ulisse_ipi_json.codici.read_only", "false");
				configuration.setProperty("ulisse_ipi_json.codici.create_always", "true");
			} else {
				configuration.setProperty("ulisse_ipi_json.codici.page_size", "-1");
				configuration.setProperty("ulisse_ipi_json.codici.read_only", "true");
				configuration.setProperty("ulisse_ipi_json.codici.create_always", "false");
			}

			// codici nosql
			final CodiciNoSqlDb codiciNoSql = new CodiciNoSqlDb(configuration, "ulisse_ipi_json.codici");
			
			// codici
			if (steps.contains("codici")) {
				final long startTimeMillis = System.currentTimeMillis();

				// connect to database
				try (final Connection connection = ulisseDataSource.getConnection()) {
					logger.debug("jdbc connected to {} {}", connection.getMetaData()
							.getDatabaseProductName(), connection.getMetaData().getURL());

					// setup one-by-one rows retrieval
					final Statement statement;
					final int fetchSize = Integer.parseInt(configuration
							.getProperty("ulisse_ipi_json.codici.fetch_size", "-1"));
					if (-1 == fetchSize) {
						statement = connection.createStatement();
					} else {
						statement = connection
								.createStatement(ResultSet.TYPE_FORWARD_ONLY,
										ResultSet.CONCUR_READ_ONLY);
						statement.setFetchSize(fetchSize);
					}

					// query
					final String sql = configuration.getProperty("ulisse_ipi_json.codici.sql");
					logger.debug("sql {}", sql);
					try (final ResultSet resultSet = statement.executeQuery(sql)) {
						logger.debug("query executed in {}", TextUtils
								.formatDuration(System.currentTimeMillis() - startTimeMillis));
						final HeartBeat heartbeat = HeartBeat.constant("codici", Integer
								.parseInt(configuration.getProperty("ulisse_ipi_json.codici.heartbeat",
										configuration.getProperty("default.heartbeat", "10000"))));

						// fetch rows one-by-one
						long lastIdOpera = -1L;
						Codici codici = null;
						while (resultSet.next()) {
							final long idOpera = resultSet.getLong("idOpera");
							final String codice = resultSet.getString("codice");
							final String idTipoCodice = resultSet.getString("idTipoCodice");

							if (idOpera != lastIdOpera) {
								if (null != codici) {
									// upsert bean
									codiciNoSql.put(codici);
								}
								lastIdOpera = idOpera;
								codici = new Codici(idOpera);
							}

							codici.add(codice, idTipoCodice);

							heartbeat.pump();
						}
						if (null != codici) {
							// upsert bean
							codiciNoSql.put(codici);
						}
						logger.debug("total codici(s) {}", heartbeat.getTotalPumps());

					}

				}

				logger.debug("codici export completed in {}", TextUtils
						.formatDuration(System.currentTimeMillis() - startTimeMillis));
			}

            ////////////////////////////////////////////////////////////////////////////////
            // titoli
            ////////////////////////////////////////////////////////////////////////////////
            if (steps.contains("titoli")) {
                configuration.setProperty("ulisse_ipi_json.titoli.read_only", "false");
                configuration.setProperty("ulisse_ipi_json.titoli.create_always", "true");
            } else {
                configuration.setProperty("ulisse_ipi_json.titoli.page_size", "-1");
                configuration.setProperty("ulisse_ipi_json.titoli.read_only", "true");
                configuration.setProperty("ulisse_ipi_json.titoli.create_always", "false");
            }

            // titoli nosql
            final TitoliNoSqlDb titoliNoSql = new TitoliNoSqlDb(configuration, "ulisse_ipi_json.titoli");

            // titoli
            if (steps.contains("titoli")) {
                final long startTimeMillis = System.currentTimeMillis();

                // connect to database
                try (final Connection connection = ulisseDataSource.getConnection()) {
                    logger.debug("jdbc connected to {} {}", connection.getMetaData()
                            .getDatabaseProductName(), connection.getMetaData().getURL());

                    // setup one-by-one rows retrieval
                    final Statement statement;
                    final int fetchSize = Integer.parseInt(configuration
                            .getProperty("ulisse_ipi_json.titoli.fetch_size", "-1"));
                    if (-1 == fetchSize) {
                        statement = connection.createStatement();
                    } else {
                        statement = connection
                                .createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                        ResultSet.CONCUR_READ_ONLY);
                        statement.setFetchSize(fetchSize);
                    }

                    // query
                    final String sql = configuration.getProperty("ulisse_ipi_json.titoli.sql");
                    logger.debug("sql {}", sql);
                    try (final ResultSet resultSet = statement.executeQuery(sql)) {
                        logger.debug("query executed in {}", TextUtils
                                .formatDuration(System.currentTimeMillis() - startTimeMillis));
                        final HeartBeat heartbeat = HeartBeat.constant("titoli", Integer
                                .parseInt(configuration.getProperty("ulisse_ipi_json.titoli.heartbeat",
                                        configuration.getProperty("default.heartbeat", "10000"))));

                        // fetch rows one-by-one
                        long lastIdOpera = -1L;
                        Titoli titoli = null;
                        while (resultSet.next()) {
                            final long idOpera = resultSet.getLong("idOpera");
                            final String titolo = resultSet.getString("titolo");
                            final String tipoTitolo = resultSet.getString("tipoTitolo");
                            final String lingua = resultSet.getString("lingua");
                            if (idOpera != lastIdOpera) {
                                if (null != titoli) {
                                    // upsert bean
                                    titoliNoSql.put(titoli);
//                                    logger.debug("add titoli {} for idOpera {}",titoli.toString(), lastIdOpera);
                                }
                                lastIdOpera = idOpera;
                                titoli = new Titoli(idOpera);
                            }

                            if( !Strings.isNullOrEmpty(tipoTitolo) ){
                                Titolo titoloObj = new Titolo(titolo, lingua, tipoTitolo);
                                titoli.add(titoloObj);
                            }

                            heartbeat.pump();
                        }
                        if (null != titoli) {
                            // upsert bean
                            titoliNoSql.put(titoli);
                        }
                        logger.debug("total titoli(s) {}", heartbeat.getTotalPumps());

                    }

                }

                logger.debug("titoli export completed in {}", TextUtils
                        .formatDuration(System.currentTimeMillis() - startTimeMillis));
            }
            
            
			////////////////////////////////////////////////////////////////////////////////
			// maturato
			////////////////////////////////////////////////////////////////////////////////
			if (steps.contains("maturato")) {
			    configuration.setProperty("ulisse_ipi_json.maturato.read_only", "false");
			    configuration.setProperty("ulisse_ipi_json.maturato.create_always", "true");
			} else {
			    configuration.setProperty("ulisse_ipi_json.maturato.page_size", "-1");
			    configuration.setProperty("ulisse_ipi_json.maturato.read_only", "true");
			    configuration.setProperty("ulisse_ipi_json.maturato.create_always", "false");
			}
			
			// maturato nosql
			final MaturatoNoSqlDb maturatoNoSqlDb = new MaturatoNoSqlDb(configuration, "ulisse_ipi_json.maturato");
			
			// maturato
			if (steps.contains("maturato")) {
			final long startTimeMillis = System.currentTimeMillis();
			
			// connect to database
			try (final Connection connection = ulisseDataSource.getConnection()) {
			logger.debug("jdbc connected to {} {}", connection.getMetaData()
			.getDatabaseProductName(), connection.getMetaData().getURL());
			
			// setup one-by-one rows retrieval
			final Statement statement;
			final int fetchSize = Integer.parseInt(configuration
			.getProperty("ulisse_ipi_json.maturato.fetch_size", "-1"));
			if (-1 == fetchSize) {
			statement = connection.createStatement();
			} else {
			statement = connection
			.createStatement(ResultSet.TYPE_FORWARD_ONLY,
			ResultSet.CONCUR_READ_ONLY);
			statement.setFetchSize(fetchSize);
			}
			
			// query
			final String sql = configuration.getProperty("ulisse_ipi_json.maturato.sql");
			logger.debug("sql {}", sql);
			try (final ResultSet resultSet = statement.executeQuery(sql)) {
			logger.debug("query executed in {}", TextUtils
			.formatDuration(System.currentTimeMillis() - startTimeMillis));
			final HeartBeat heartbeat = HeartBeat.constant("maturato", Integer
			.parseInt(configuration.getProperty("ulisse_ipi_json.maturato.heartbeat",
			configuration.getProperty("default.heartbeat", "10000"))));
			
			// fetch rows one-by-one
			long lastIdOpera = -1L;
			Maturati maturato = null;
			while (resultSet.next()) {
			    final long idOpera = resultSet.getLong("idOpera");
			    
			    BigDecimal valore = new BigDecimal(0);
			    if(resultSet.getBigDecimal("valore") != null) {
			    	valore = resultSet.getBigDecimal("valore");
			    }
			    
			    final String periodoLabel = resultSet.getString("periodoLabel");
			    final String dataDa = resultSet.getString("dataDa");
			    final String dataA = resultSet.getString("dataA");
			
			    if (idOpera != lastIdOpera) {
			        if (null != maturato) {
			            maturatoNoSqlDb.put(maturato);
			        }
			        lastIdOpera = idOpera;
			            maturato = new Maturati(idOpera);
			        }
			
			        Maturato maturatoObj = new Maturato(valore, periodoLabel, dataDa, dataA);
			        maturato.add(maturatoObj);
			
			        heartbeat.pump();
			    }
			    if (null != maturato) {
			        // upsert bean
			        maturatoNoSqlDb.put(maturato);
			    }
			    logger.debug("total maturato(s) {}", heartbeat.getTotalPumps());
			}
			
			}
			
			logger.debug("maturato export completed in {}", TextUtils
			.formatDuration(System.currentTimeMillis() - startTimeMillis));
			}
            
			
			////////////////////////////////////////////////////////////////////////////////			
			// interpreti
			////////////////////////////////////////////////////////////////////////////////
			if (steps.contains("interpreti")) {
				configuration.setProperty("ulisse_ipi_json.interpreti.read_only", "false");
				configuration.setProperty("ulisse_ipi_json.interpreti.create_always", "false");
			} else {
				configuration.setProperty("ulisse_ipi_json.interpreti.page_size", "-1");
				configuration.setProperty("ulisse_ipi_json.interpreti.read_only", "true");
				configuration.setProperty("ulisse_ipi_json.interpreti.create_always", "false");
			}

			// interpreti nosql
			final InterpretiNoSqlDb interpretiNoSql = new InterpretiNoSqlDb(configuration, "ulisse_ipi_json.interpreti");

			// interpreti
			if (steps.contains("interpreti")) {
				final long startTimeMillis = System.currentTimeMillis();

				// connect to database
				try (final Connection connection = ulisseDataSource.getConnection()) {
					logger.debug("jdbc connected to {} {}", connection.getMetaData()
							.getDatabaseProductName(), connection.getMetaData().getURL());

					// setup one-by-one rows retrieval
					final Statement statement;
					final int fetchSize = Integer.parseInt(configuration
							.getProperty("ulisse_ipi_json.interpreti.fetch_size", "-1"));
					if (-1 == fetchSize) {
						statement = connection.createStatement();
					} else {
						statement = connection
								.createStatement(ResultSet.TYPE_FORWARD_ONLY,
										ResultSet.CONCUR_READ_ONLY);
						statement.setFetchSize(fetchSize);
					}

					// query
					final String sql = configuration.getProperty("ulisse_ipi_json.interpreti.sql");
					logger.debug("sql {}", sql);
					try (final ResultSet resultSet = statement.executeQuery(sql)) {
						logger.debug("query executed in {}", TextUtils
								.formatDuration(System.currentTimeMillis() - startTimeMillis));
						final HeartBeat heartbeat = HeartBeat.constant("interpreti", Integer
								.parseInt(configuration.getProperty("ulisse_ipi_json.interpreti.heartbeat",
										configuration.getProperty("default.heartbeat", "10000"))));

						// fetch rows one-by-one
						long lastIdOpera = -1L;
						Interpreti interpreti = null;
						while (resultSet.next()) {
							final long idOpera = resultSet.getLong("idOpera");
							final String interprete = resultSet.getString("interprete");
							final String tipo = resultSet.getString("tipo");

							if (idOpera != lastIdOpera) {
								if (null != interpreti) {
									// upsert bean
									interpretiNoSql.put(interpreti);
								}
								lastIdOpera = idOpera;
								interpreti = new Interpreti(idOpera);
							}

							interpreti.add(interprete, tipo);

							heartbeat.pump();
						}
						if (null != interpreti) {
							// upsert bean
							interpretiNoSql.put(interpreti);
						}
						logger.debug("total interpreti(s) {}", heartbeat.getTotalPumps());

					}

				}

				logger.debug("interpreti export completed in {}", TextUtils
						.formatDuration(System.currentTimeMillis() - startTimeMillis));
			}
			
			////////////////////////////////////////////////////////////////////////////////
			// irregolarita
			////////////////////////////////////////////////////////////////////////////////
			if (steps.contains("irregolarita")) {
				configuration.setProperty("ulisse_ipi_json.irregolarita.read_only", "false");
				configuration.setProperty("ulisse_ipi_json.irregolarita.create_always", "true");
			} else {
				configuration.setProperty("ulisse_ipi_json.irregolarita.page_size", "-1");
				configuration.setProperty("ulisse_ipi_json.irregolarita.read_only", "true");
				configuration.setProperty("ulisse_ipi_json.irregolarita.create_always", "false");
			}

			// irregolarita nosql
			final IrregolaritaNoSqlDb irregolaritaNoSql = new IrregolaritaNoSqlDb(configuration, "ulisse_ipi_json.irregolarita");

			// irregolarita
			if (steps.contains("irregolarita")) {
				final long startTimeMillis = System.currentTimeMillis();

				// connect to database
				try (final Connection connection = ulisseDataSource.getConnection()) {
					logger.debug("jdbc connected to {} {}", connection.getMetaData()
							.getDatabaseProductName(), connection.getMetaData().getURL());

					// setup one-by-one rows retrieval
					final Statement statement;
					final int fetchSize = Integer.parseInt(configuration
							.getProperty("ulisse_ipi_json.irregolarita.fetch_size", "-1"));
					if (-1 == fetchSize) {
						statement = connection.createStatement();
					} else {
						statement = connection
								.createStatement(ResultSet.TYPE_FORWARD_ONLY,
										ResultSet.CONCUR_READ_ONLY);
						statement.setFetchSize(fetchSize);
					}

					// query
					final String sql = configuration.getProperty("ulisse_ipi_json.irregolarita.sql");
					logger.debug("sql {}", sql);
					try (final ResultSet resultSet = statement.executeQuery(sql)) {
						logger.debug("query executed in {}", TextUtils
								.formatDuration(System.currentTimeMillis() - startTimeMillis));
						final HeartBeat heartbeat = HeartBeat.constant("irregolarita", Integer
								.parseInt(configuration.getProperty("ulisse_ipi_json.irregolarita.heartbeat",
										configuration.getProperty("default.heartbeat", "10000"))));

						// fetch rows one-by-one
						long lastIdOpera = -1L;
						Irregolarita irregolarita = null;
						while (resultSet.next()) {
							final long idOpera = resultSet.getLong("idAnagOpera");
							final String csv = resultSet.getString("csv");

							if (idOpera != lastIdOpera) {
								if (null != irregolarita) {
									// upsert bean
									irregolaritaNoSql.put(irregolarita);
								}
								lastIdOpera = idOpera;
								irregolarita = new Irregolarita(idOpera);
							}

							irregolarita.add(csv);

							heartbeat.pump();
						}
						if (null != irregolarita) {
							// upsert bean
							irregolaritaNoSql.put(irregolarita);
						}
						logger.debug("total irregolarita(s) {}", heartbeat.getTotalPumps());

					}

				}

				logger.debug("irregolarita export completed in {}", TextUtils
						.formatDuration(System.currentTimeMillis() - startTimeMillis));
			}

            ////////////////////////////////////////////////////////////////////////////////
            // contratti
            ////////////////////////////////////////////////////////////////////////////////
            if (steps.contains("contratti")) {
                configuration.setProperty("ulisse_ipi_json.contratti.read_only", "false");
                configuration.setProperty("ulisse_ipi_json.contratti.create_always", "true");
            } else {
                configuration.setProperty("ulisse_ipi_json.contratti.page_size", "-1");
                configuration.setProperty("ulisse_ipi_json.contratti.read_only", "true");
                configuration.setProperty("ulisse_ipi_json.contratti.create_always", "false");
            }

            // contratti nosql
            final ContrattiNoSqlDb contrattiNoSql = new ContrattiNoSqlDb(configuration, "ulisse_ipi_json.contratti");

            // contratti
            if (steps.contains("contratti")) {
                final long startTimeMillis = System.currentTimeMillis();

                // connect to database
                try (final Connection connection = ulisseDataSource.getConnection()) {
                    logger.debug("jdbc connected to {} {}", connection.getMetaData()
                            .getDatabaseProductName(), connection.getMetaData().getURL());

                    // setup one-by-one rows retrieval
                    final Statement statement;
                    final int fetchSize = Integer.parseInt(configuration
                            .getProperty("ulisse_ipi_json.contratti.fetch_size", "-1"));
                    if (-1 == fetchSize) {
                        statement = connection.createStatement();
                    } else {
                        statement = connection
                                .createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                        ResultSet.CONCUR_READ_ONLY);
                        statement.setFetchSize(fetchSize);
                    }

                    // query
                    final String sql = configuration.getProperty("ulisse_ipi_json.contratti.sql");
                    logger.debug("sql {}", sql);
                    try (final ResultSet resultSet = statement.executeQuery(sql)) {
                        logger.debug("query executed in {}", TextUtils
                                .formatDuration(System.currentTimeMillis() - startTimeMillis));
                        final HeartBeat heartbeat = HeartBeat.constant("contratti", Integer
                                .parseInt(configuration.getProperty("ulisse_ipi_json.contratti.heartbeat",
                                        configuration.getProperty("default.heartbeat", "10000"))));

                        // fetch rows one-by-one
                        long lastIdSchema = -1L;
                        Contratti contratti = null;
                        while (resultSet.next()) {
                            final long idSchema = resultSet.getLong("idSchema");
                            final String codiceContratto = resultSet.getString("codiceContratto");
                            final String dataScadenza = resultSet.getString("dataScadenza");
                            final String tipoContratto = resultSet.getString("tipoContratto");
                            final String statoContratto = resultSet.getString("statoContratto");
                            final String territorio = resultSet.getString("territorio");
                            final String codiceSchedula = resultSet.getString("codiceSchedula");

                            if (idSchema != lastIdSchema) {
                                if (null != contratti) {
                                    // upsert bean
                                    contrattiNoSql.put(contratti);
                                }
                                lastIdSchema = idSchema;
                                contratti = new Contratti(idSchema);
                            }

                            contratti.add(codiceContratto, dataScadenza, tipoContratto, statoContratto, territorio, codiceSchedula);

                            heartbeat.pump();
                        }
                        if (null != contratti) {
                            // upsert bean
                            contrattiNoSql.put(contratti);
                        }
                        logger.debug("total contratti(s) {}", heartbeat.getTotalPumps());

                    }

                }

                logger.debug("contratti export completed in {}", TextUtils
                        .formatDuration(System.currentTimeMillis() - startTimeMillis));
            }

            ////////////////////////////////////////////////////////////////////////////////
            // audiovisivi
            ////////////////////////////////////////////////////////////////////////////////
            if (steps.contains("audiovisivi")) {
                configuration.setProperty("ulisse_ipi_json.audiovisivi.read_only", "false");
                configuration.setProperty("ulisse_ipi_json.audiovisivi.create_always", "true");
            } else {
                configuration.setProperty("ulisse_ipi_json.audiovisivi.page_size", "-1");
                configuration.setProperty("ulisse_ipi_json.audiovisivi.read_only", "true");
                configuration.setProperty("ulisse_ipi_json.audiovisivi.create_always", "false");
            }

            // audiovisivi nosql
            final AudiovisiviNoSqlDb audiovisiviNoSql = new AudiovisiviNoSqlDb(configuration, "ulisse_ipi_json.audiovisivi");

            // audiovisivi
            if (steps.contains("audiovisivi")) {
                final long startTimeMillis = System.currentTimeMillis();

                // connect to database
                try (final Connection connection = ulisseDataSource.getConnection()) {
                    logger.debug("jdbc connected to {} {}", connection.getMetaData()
                            .getDatabaseProductName(), connection.getMetaData().getURL());

                    // setup one-by-one rows retrieval
                    final Statement statement;
                    final int fetchSize = Integer.parseInt(configuration
                            .getProperty("ulisse_ipi_json.audiovisivi.fetch_size", "-1"));
                    if (-1 == fetchSize) {
                        statement = connection.createStatement();
                    } else {
                        statement = connection
                                .createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                        ResultSet.CONCUR_READ_ONLY);
                        statement.setFetchSize(fetchSize);
                    }

                    // query
                    final String sql = configuration.getProperty("ulisse_ipi_json.audiovisivi.sql");
                    logger.debug("sql {}", sql);
                    try (final ResultSet resultSet = statement.executeQuery(sql)) {
                        logger.debug("query executed in {}", TextUtils
                                .formatDuration(System.currentTimeMillis() - startTimeMillis));
                        final HeartBeat heartbeat = HeartBeat.constant("audiovisivi", Integer
                                .parseInt(configuration.getProperty("ulisse_ipi_json.audiovisivi.heartbeat",
                                        configuration.getProperty("default.heartbeat", "10000"))));

                        // fetch rows one-by-one
                        long lastIdOpera = -1L;
                        Audiovisivi audiovisivi = null;
                        while (resultSet.next()) {
                            final long idOpera = resultSet.getLong("idOpera");
                            final String codice = resultSet.getString("codice");
                            final String titolo = resultSet.getString("titolo");
                            final String titoloSerie = resultSet.getString("titoloSerie");
                            final String anno = resultSet.getString("anno");
                            final String regista = resultSet.getString("regista");

                            if (idOpera != lastIdOpera) {
                                if (null != audiovisivi) {
                                    // upsert bean
                                    audiovisiviNoSql.put(audiovisivi);
                                }
                                lastIdOpera = idOpera;
                                audiovisivi = new Audiovisivi(idOpera);
                            }

                            audiovisivi.add(codice, titolo, titoloSerie, anno, regista);

                            heartbeat.pump();
                        }
                        if (null != audiovisivi) {
                            // upsert bean
                            audiovisiviNoSql.put(audiovisivi);
                        }
                        logger.debug("total audiovisivi(s) {}", heartbeat.getTotalPumps());

                    }

                }

                logger.debug("audiovisivi export completed in {}", TextUtils
                        .formatDuration(System.currentTimeMillis() - startTimeMillis));
            }

            ////////////////////////////////////////////////////////////////////////////////
            // flags
            ////////////////////////////////////////////////////////////////////////////////
            if (steps.contains("flags")) {
                configuration.setProperty("ulisse_ipi_json.flags.read_only", "false");
            } else {
                configuration.setProperty("ulisse_ipi_json.flags.read_only", "true");
            }

            // flags trie
            final Trie<String> flagsTrie;
            if ("true".equalsIgnoreCase(configuration
                    .getProperty("ulisse_ipi_json.flags.read_only", "true"))) {
                flagsTrie = ConcurrentTrie.wrap(UnsafeMemoryMap.open(new File(configuration
                                .getProperty("ulisse_ipi_json.flags.home_folder")), -1, true),
                        new FixedLengthStringBinding(4, charset)); // "0X"
            } else {
                flagsTrie = ConcurrentTrie.wrap(UnsafeMemoryMap.create(new File(configuration
                                .getProperty("ulisse_ipi_json.flags.home_folder")), TextUtils
                                .parseIntSize(configuration.getProperty("ulisse_ipi_json.flags.page_size"))),
                        new FixedLengthStringBinding(4, charset)); // "0X"
            }

            // flags
            if (steps.contains("flags")) {
                final long startTimeMillis = System.currentTimeMillis();

                // connect to database
                try (final Connection connection = ulisseDataSource.getConnection()) {
                    logger.debug("jdbc connected to {} {}", connection.getMetaData()
                            .getDatabaseProductName(), connection.getMetaData().getURL());

                    // setup one-by-one rows retrieval
                    final Statement statement;
                    final int fetchSize = Integer.parseInt(configuration
                            .getProperty("ulisse_ipi_json.flags.fetch_size", "-1"));
                    if (-1 == fetchSize) {
                        statement = connection.createStatement();
                    } else {
                        statement = connection
                                .createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                        ResultSet.CONCUR_READ_ONLY);
                        statement.setFetchSize(fetchSize);
                    }

                    // query
                    final String sql = configuration.getProperty("ulisse_ipi_json.flags.sql");
                    logger.debug("sql {}", sql);
                    try (final ResultSet resultSet = statement.executeQuery(sql)) {
                        logger.debug("query executed in {}", TextUtils
                                .formatDuration(System.currentTimeMillis() - startTimeMillis));
                        final HeartBeat heartbeat = HeartBeat.constant("flags", Integer
                                .parseInt(configuration.getProperty("ulisse_ipi_json.flags.heartbeat",
                                        configuration.getProperty("default.heartbeat", "10000"))));

                        // fetch rows one-by-one
                        while (resultSet.next()) {
                            final long idSchemaRiparto = resultSet.getLong("idSchemaRiparto");
                            final String monoedita = resultSet.getString("monoedita");
                            final String editaSubedita = resultSet.getString("editaSubedita");
                            final String flagNonSocio = resultSet.getString("flagNonSocio");
                            final String flagContoSpeciale = resultSet.getString("flagContoSpeciale");

                            flagsTrie.upsert(Long.toString(idSchemaRiparto), monoedita + editaSubedita + flagNonSocio + flagContoSpeciale);

                            heartbeat.pump();
                        }
                        logger.debug("total flags(s) {}", heartbeat.getTotalPumps());

                    }

                }

                logger.debug("flags export completed in {}", TextUtils
                        .formatDuration(System.currentTimeMillis() - startTimeMillis));
            }

            ////////////////////////////////////////////////////////////////////////////////
            // collegamenti
            ////////////////////////////////////////////////////////////////////////////////
            if (steps.contains("collegamenti")) {
                configuration.setProperty("ulisse_ipi_json.collegamenti.read_only", "false");
                configuration.setProperty("ulisse_ipi_json.collegamenti.create_always", "true");
            } else {
                configuration.setProperty("ulisse_ipi_json.collegamenti.page_size", "-1");
                configuration.setProperty("ulisse_ipi_json.collegamenti.read_only", "true");
                configuration.setProperty("ulisse_ipi_json.collegamenti.create_always", "false");
            }

            // quote nosql
            final CollegamentiNoSqlDb collegamentiNoSql = new CollegamentiNoSqlDb(configuration, "ulisse_ipi_json.collegamenti");

            // quote
            if (steps.contains("collegamenti")) {
                final long startTimeMillis = System.currentTimeMillis();

                // connect to database
                try (final Connection connection = ulisseDataSource.getConnection()) {
                    logger.debug("jdbc connected to {} {}", connection.getMetaData()
                            .getDatabaseProductName(), connection.getMetaData().getURL());

                    // setup one-by-one rows retrieval
                    final Statement statement;
                    final int fetchSize = Integer.parseInt(configuration
                            .getProperty("ulisse_ipi_json.collegamenti.fetch_size", "-1"));
                    if (-1 == fetchSize) {
                        statement = connection.createStatement();
                    } else {
                        statement = connection
                                .createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                        ResultSet.CONCUR_READ_ONLY);
                        statement.setFetchSize(fetchSize);
                    }

//					// query
                    final String sql = configuration.getProperty("ulisse_ipi_json.collegamenti.sql");
                    logger.debug("sql {}", sql);
                    try (final ResultSet resultSet = statement.executeQuery(sql)) {
                        logger.debug("query executed in {}", TextUtils
                                .formatDuration(System.currentTimeMillis() - startTimeMillis));
                        final HeartBeat heartbeat = HeartBeat.constant("collegamenti", Integer
                                .parseInt(configuration.getProperty("ulisse_ipi_json.collegamenti.heartbeat",
                                        configuration.getProperty("default.heartbeat", "10000"))));

//						// fetch rows one-by-one
                        String lastIdOpera = null;
                        Collegamenti collegamenti = null;
                        while (resultSet.next()) {
                            final String idOpera = resultSet.getString("idOpera");

                            if (!idOpera.equals(lastIdOpera)) {

                                if (null != collegamenti) {
                                    collegamentiNoSql.put(collegamenti);
                                }
                                collegamenti = new Collegamenti(Long.parseLong(idOpera));

                            }

                            final Collegamento collegamento = new Collegamento(resultSet.getLong("idOperaCollegata"));
                            collegamento.codiceOperaCollegata= resultSet.getString("codiceOperaCollegata");
                            collegamento.tipoCollegamento= resultSet.getString("tipoCollegamento");
                            collegamenti.add(collegamento);

                            heartbeat.pump();
                        }

                        if (null != collegamenti) {
                            collegamentiNoSql.put(collegamenti);
                        }

                        logger.debug("total collegamenti(s) {}", heartbeat.getTotalPumps());

                    }
                }

                logger.debug("collegamenti export completed in {}", TextUtils
                        .formatDuration(System.currentTimeMillis() - startTimeMillis));
            }

            ////////////////////////////////////////////////////////////////////////////////
            // anagrafiche
            ////////////////////////////////////////////////////////////////////////////////
            if (steps.contains("anagrafiche")) {
                configuration.setProperty("ulisse_ipi_json.anagrafiche.read_only", "false");
                configuration.setProperty("ulisse_ipi_json.anagrafiche.create_always", "true");
            } else {
                configuration.setProperty("ulisse_ipi_json.anagrafiche.page_size", "-1");
                configuration.setProperty("ulisse_ipi_json.anagrafiche.read_only", "true");
                configuration.setProperty("ulisse_ipi_json.anagrafiche.create_always", "false");
            }

            final AnagraficaNoSqlDb anagraficaNoSql = new AnagraficaNoSqlDb(configuration, "ulisse_ipi_json.anagrafiche");

            // anagrafiche nosql
            if (steps.contains("anagrafiche")) {
                final long startTimeMillis = System.currentTimeMillis();

                // connect to database
                try (final Connection connection = ulisseDataSource.getConnection()) {
                    logger.debug("jdbc connected to {} {}", connection.getMetaData()
                            .getDatabaseProductName(), connection.getMetaData().getURL());

                    // setup one-by-one rows retrieval
                    final Statement statement;
                    final int fetchSize = Integer.parseInt(configuration
                            .getProperty("ulisse_ipi_json.anagrafiche.fetch_size", "-1"));
                    if (-1 == fetchSize) {
                        statement = connection.createStatement();
                    } else {
                        statement = connection
                                .createStatement(ResultSet.TYPE_FORWARD_ONLY,
                                        ResultSet.CONCUR_READ_ONLY);
                        statement.setFetchSize(fetchSize);
                    }

//					// query
                    final String sql = configuration.getProperty("ulisse_ipi_json.anagrafiche.sql");
                    logger.debug("sql {}", sql);
                    try (final ResultSet resultSet = statement.executeQuery(sql)) {
                        logger.debug("query executed in {}", TextUtils
                                .formatDuration(System.currentTimeMillis() - startTimeMillis));
                        final HeartBeat heartbeat = HeartBeat.constant("anagrafiche", Integer
                                .parseInt(configuration.getProperty("ulisse_ipi_json.anagrafiche.heartbeat",
                                        configuration.getProperty("default.heartbeat", "10000"))));

//						// fetch rows one-by-one
                        String lastIdSchema = null;
                        Anagrafica anagrafica = null;
                        while (resultSet.next()) {

                            final String idSchemaFlat = resultSet.getString("idSchemaFlat");
                            final String idSchema = resultSet.getString("idSchema");
                            final String idOpera = resultSet.getString("idOpera");
                            final String codiceOpera = resultSet.getString("codiceOpera");


                            if (!idSchema.equals(lastIdSchema)) {
                                if (null != anagrafica) {
                                    anagraficaNoSql.put(anagrafica);
                                }

                                lastIdSchema = idSchema;
                                anagrafica = new Anagrafica(Long.parseLong(idSchema));
                            }

                            if( StringUtils.isNotEmpty(idSchemaFlat) &&
                                    StringUtils.isNotEmpty(idOpera) &&
                                    StringUtils.isNotEmpty(codiceOpera) ){

                                anagrafica.idOpera = idOpera;
                                anagrafica.idSchemaFlat = idSchemaFlat;
                                anagrafica.codiceOpera = codiceOpera;

                                anagrafica.dataDichiarazione = resultSet.getString("dataDichiarazione");
                                anagrafica.dataDichiarazioneSiada = resultSet.getString("dataDichiarazioneSiada");
                                anagrafica.dataAcquisizione = resultSet.getString("dataAcquisizione");
                                anagrafica.dataPubblicoDominio = resultSet.getString("dataPubblicoDominio");
                                anagrafica.tipologiaDeposito = resultSet.getString("tipologiaDeposito");
                                anagrafica.musica = resultSet.getString("musica");
                                anagrafica.testo = resultSet.getString("testo");
                                anagrafica.esemplare = resultSet.getString("esemplare");
                                anagrafica.genereOpera = resultSet.getString("genereOpera");
                                anagrafica.durata = resultSet.getString("durata");
                                anagrafica.notizieParticolari = resultSet.getString("notizieParticolari");
                                anagrafica.tipoBollettino = resultSet.getString("tipoBollettino");
                                anagrafica.codProvvEditore = resultSet.getString("codProvvEditore");
                                anagrafica.riferimento = resultSet.getString("riferimento");
                                anagrafica.flagPD = resultSet.getString("flagPD");
                                anagrafica.flagEL = resultSet.getString("flagEL");
                                anagrafica.flagSempreverdi = resultSet.getString("flagSempreverdi");
                                anagrafica.flagMaggiorazione = resultSet.getString("flagMaggiorazione");
                                anagrafica.flagAccettataConfusoria = resultSet.getString("flagAccettataConfusoria");
                                anagrafica.ambito = resultSet.getString("ambito");
                                anagrafica.statoOpera = resultSet.getString("statoOpera");
                                anagrafica.nPacco = resultSet.getString("nPacco");
                                anagrafica.inviante = resultSet.getString("inviante");
                                anagrafica.rilevanza = resultSet.getString("rilevanza");
                                anagrafica.maturato = resultSet.getString("maturato");
                                anagrafica.rischio = resultSet.getString("rischio");
                                anagrafica.semestreScadenza = resultSet.getString("semestreScadenza");
                                anagrafica.dataSemScadenza = resultSet.getString("dataSemScadenza");
                                anagrafica.semestreDichiarazione = resultSet.getString("semestreDichiarazione");
                                anagrafica.dataSemDichiarazione = resultSet.getString("dataSemDichiarazione");
                                anagrafica.semestreValidita = resultSet.getString("semestreValidita");
                                anagrafica.dataSemValidita = resultSet.getString("dataSemValidita");
                                anagrafica.tipoUtilizzo = resultSet.getString("tipoUtilizzo");
                                anagrafica.territorio = resultSet.getString("territorio");
                                anagrafica.ricercabile = resultSet.getString("ricercabile");
                                anagrafica.idCollectingCompany = resultSet.getString("idCollectingCompany");

                                heartbeat.pump();
                            }
                        }

                        if (null != anagrafica) {
                            anagraficaNoSql.put(anagrafica);
                        }

                        logger.debug("total anagrafiche(s) {}", heartbeat.getTotalPumps());
                    }
                }

                logger.debug("anagrafiche export completed in {}", TextUtils
                        .formatDuration(System.currentTimeMillis() - startTimeMillis));
            }

			////////////////////////////////////////////////////////////////////////////////
			// quote
			////////////////////////////////////////////////////////////////////////////////

//			if (steps.contains("quote")) {
//				configuration.setProperty("ulisse_ipi_json.quote.read_only", "false");
//				configuration.setProperty("ulisse_ipi_json.quote.create_always", "true");									
//			} else {
//				configuration.setProperty("ulisse_ipi_json.quote.page_size", "-1");
//				configuration.setProperty("ulisse_ipi_json.quote.read_only", "true");
//				configuration.setProperty("ulisse_ipi_json.quote.create_always", "false");
//			}

//			// quote nosql
//			final QuoteNoSqlDb quoteNoSql = new QuoteNoSqlDb(configuration, "ulisse_ipi_json.quote");
			
			// quote
			if (steps.contains("quote")) {
				final long startTimeMillis = System.currentTimeMillis();
				final Calendar calendar = Calendar.getInstance();
				final String environment = configuration.getProperty("default.environment", "local");

				final String foldername = configuration.getProperty("ulisse_ipi_json.output.foldername")
                        .replace("{year}", String.format("%04d", calendar.get(Calendar.YEAR)))
                        .replace("{month}", String.format("%02d", 1 + calendar.get(Calendar.MONTH)))
                        .replace("{day}", String.format("%02d", calendar.get(Calendar.DATE)))
                        .replace("{environment}", environment);

                // output s3 folder url
                final String outputS3FolderUrl = configuration.getProperty("ulisse_ipi_json.output.s3__folder_url")
                        .replace("{foldername}", foldername);
                logger.debug("outputS3FolderUrl {}", outputS3FolderUrl);


				final String filename = configuration.getProperty("ulisse_ipi_json.output.filename")
							.replace("{year}", String.format("%04d", calendar.get(Calendar.YEAR)))
							.replace("{month}", String.format("%02d", 1 + calendar.get(Calendar.MONTH)))
							.replace("{day}", String.format("%02d", calendar.get(Calendar.DATE)))
				            .replace("{environment}", environment);

				final File homeFolder = new File(configuration.getProperty("default.home_folder"));

				final int rowsPerFile = Integer.parseInt(configuration.getProperty("ulisse_ipi_json.output.rows_per_file", "1000000"));

				missingIpnamenrs.clear();
				try (final Connection connection = ulisseDataSource.getConnection()) {
					logger.debug("jdbc connected to {} {}", connection.getMetaData()
							.getDatabaseProductName(), connection.getMetaData().getURL());

					// setup one-by-one rows retrieval
					final Statement statement;
					final int fetchSize = Integer.parseInt(configuration
							.getProperty("ulisse_ipi_json.quote.fetch_size", "-1"));
					if (-1 == fetchSize) {
						statement = connection.createStatement();						
					} else {
						statement = connection
								.createStatement(ResultSet.TYPE_FORWARD_ONLY,
										ResultSet.CONCUR_READ_ONLY);
						statement.setFetchSize(fetchSize);
					}

					// output writer
                    File outputFile = null;
					OutputStream out = null;
					Writer writer = null;
					int index = 0;
					int accumulator = 0;
					
					// query
					final String sql = configuration.getProperty("ulisse_ipi_json.quote.sql");
					logger.debug("sql {}", sql);
					try (final ResultSet resultSet = statement.executeQuery(sql)) {
						logger.debug("query executed in {}", TextUtils
								.formatDuration(System.currentTimeMillis() - startTimeMillis));
						final HeartBeat heartbeat = HeartBeat.constant("quote", Integer
								.parseInt(configuration.getProperty("ulisse_ipi_json.quote.heartbeat",  
										configuration.getProperty("default.heartbeat", "10000"))));

						// fetch rows one-by-one
                        String lastIdSchema = null;
						Quote quote = null;
						while (resultSet.next()) {

                            final String idSchema = resultSet.getString("idSchemaRiparto");

                            if (!idSchema.equals(lastIdSchema)) {

								if (null != quote) {
                                    // handle file pieces
                                    if (++ accumulator > rowsPerFile) {
                                        index ++;
                                        accumulator = 0;
                                        if (null != writer) {
                                            writer.close();
                                            writer = null;
                                            out.close();
                                            out = null;
                                            String s3FileUrl = new StringBuilder()
                                                    .append(outputS3FolderUrl)
                                                    .append('/')
                                                    .append(outputFile.getName())
                                                    .toString();
                                            if (!s3.upload(new S3.Url(s3FileUrl), outputFile)) {
                                                logger.error("error uploading file {} to {}", outputFile.getName(), s3FileUrl);
                                                throw new IOException(String
                                                        .format("error uploading file %s to %s", outputFile.getName(), s3FileUrl));
                                            } else {
                                                logger.debug("file {} uploaded to {}", outputFile.getName(), s3FileUrl);
                                            }
                                        }
                                    }
                                    if (null == writer) {
                                        outputFile = new File(homeFolder,
                                                filename.replace("{index}", String.format("%03d", index)));
                                        out = new CompressionAwareFileOutputStream(outputFile);
                                        writer = new OutputStreamWriter(out, charset);
                                    }

                                    Anagrafica anagOpera = anagraficaNoSql.get(lastIdSchema);

                                    if( anagOpera != null ){
                                        String idOpera = anagOpera.idOpera;
                						JsonObject recordJsonObject = new JsonObject();
                                        addAnagrafica(recordJsonObject, anagOpera);
                                        final Irregolarita irregolarita = irregolaritaNoSql.get(idOpera);
                                        if (null != irregolarita) {
                                            addArrayOfObjects(recordJsonObject, "irregolarita", irregolarita.irregolarita, "tipo");
                                        }

                                        final String flags = flagsTrie.find(lastIdSchema);
                                        if (null != flags) {
                                            recordJsonObject.addProperty("monoedita", flags.charAt(0));
                                            recordJsonObject.addProperty("editaSubedita", flags.charAt(1));
                                            recordJsonObject.addProperty("flagNonSocio", flags.charAt(2));
                                            recordJsonObject.addProperty("flagContoSpeciale", flags.charAt(3));
                                        }

                                        final Titoli titoli = titoliNoSql.get(idOpera);
                                        if (null != titoli) {
                                            addTitoli(recordJsonObject, titoli);
                                        }

                                        final Codici codici = codiciNoSql.get(idOpera);
                                        if (null != codici) {
                                            addArrayOfObjects(recordJsonObject, "iswc", codici.iswc, "codice");
                                            addArrayOfObjects(recordJsonObject, "isrc", codici.isrc, "codice");
                                            addArrayOfObjects(recordJsonObject, "codiciRepertorio", codici.repertoire, "codice");
                                            addArrayOfObjects(recordJsonObject, "codiciPratica", codici.dossier, "codice");
                                            addArrayOfObjects(recordJsonObject, "codiciEditore", codici.editors, "codice");
                                        }
                                        
                                        final Maturati maturati = maturatoNoSqlDb.get(idOpera);
                                        if (null != maturati) {
                                            final JsonArray jsonArray = new JsonArray();
                                            for (Maturato maturato : maturati.maturati) {
                                                final JsonObject jsonObject = new JsonObject();
                                                jsonObject.addProperty("valore", maturato.valore);
                                                jsonObject.addProperty("periodoLabel", maturato.periodoLabel);
                                                jsonObject.addProperty("dataDa", maturato.dataDa);
                                                jsonObject.addProperty("dataA", maturato.dataA);
                                                jsonArray.add(jsonObject);
                                            }
                                            recordJsonObject.add("maturati", jsonArray);
                                        }

                                        final Interpreti interpreti = interpretiNoSql.get(idOpera);
                                        if (null != interpreti) {
                                            final JsonArray jsonArray = new JsonArray();
                                            for (Interprete interprete : interpreti.interpreti) {
                                                final JsonObject jsonObject = new JsonObject();
                                                jsonObject.addProperty("nome", interprete.nominativo);
                                                jsonObject.addProperty("tipo", interprete.tipo);
                                                jsonArray.add(jsonObject);
                                            }
                                            recordJsonObject.add("interpreti", jsonArray);
                                        }

                                        final Contratti contratti = contrattiNoSql.get(idSchema);
                                        if(null != contratti) {
                                            addContratti(recordJsonObject, contratti);
                                        }

                                        final Audiovisivi audiovisivi = audiovisiviNoSql.get(idOpera);
                                        if(null != audiovisivi) {
                                            addAudiovisivi(recordJsonObject, audiovisivi);
                                        }

                                        final Collegamenti collegamenti = collegamentiNoSql.get(idOpera);
                                        if( collegamenti != null ){
                                            addCollegamenti(recordJsonObject, collegamenti, titoliNoSql);
                                        }

                                        final Map<String, Set<String>> codiciIpiConQualifiche = addQuoteRiparto(recordJsonObject, ipiNameReverseTrie, agmNoSql, quote);
                                        addIpiObject(recordJsonObject, ipiNameReverseTrie, ipiNameNoSql, codiciIpiConQualifiche);

                                        // write to output
//									GsonUtils.toJson(recordJsonObject, jsonWriter);
                                        writer.write(GsonUtils.toJson(recordJsonObject));
                                        writer.write('\n');
                                    }
								}

								lastIdSchema = idSchema;
								quote = new Quote(Long.parseLong(idSchema));
							}

                            final Quota quota = new Quota(resultSet.getLong("idQuotaRiparto"));
                            quota.nominativo = resultSet.getString("nominativo");
                            quota.nome = resultSet.getString("nome");
                            quota.cognome = resultSet.getString("cognome");
                            quota.posizioneSiae = resultSet.getString("posizioneSiae");
                            quota.codiceIpi = resultSet.getString("codiceIpi");
                            quota.codiceConto = resultSet.getString("codiceConto");
                            quota.codiceContoEstero = resultSet.getString("codiceContoEstero");
                            quota.qualifica = resultSet.getString("qualifica");
                            quota.tipoQuota = resultSet.getString("tipoQuota");
                            quota.catenaDiritto = resultSet.getString("catenaDiritto");
                            quota.qualificaIpi = resultSet.getString("qualificaIpi");
                            quota.quotaNumeratore = resultSet.getString("quotaNumeratore");
                            quota.quotaDenominatore = resultSet.getString("quotaDenominatore");
                            quota.quotaAd = resultSet.getString("quotaAd");
                            if (!Strings.isNullOrEmpty(quota.quotaAd)) {
                                try {
                                    quota.quotaAd = BigDecimals.toPlainString(new BigDecimal(quota.quotaAd));
                                } catch (Exception e) {
                                    quota.quotaAd = "0";
                                }
                            }
                            quote.add(quota);

                            heartbeat.pump();
						}

						//write last json on last file
						if (null != quote) {
                            Anagrafica anagOpera = anagraficaNoSql.get(lastIdSchema);

                            if( anagOpera != null ){
                                String idOpera = anagOpera.idOpera;
        						JsonObject recordJsonObject = new JsonObject();
                                addAnagrafica(recordJsonObject, anagOpera);
                                final Irregolarita irregolarita = irregolaritaNoSql.get(idOpera);
                                if (null != irregolarita) {
                                    addArrayOfObjects(recordJsonObject, "irregolarita", irregolarita.irregolarita, "tipo");
                                }

                                final String flags = flagsTrie.find(lastIdSchema);
                                if (null != flags) {
                                    recordJsonObject.addProperty("monoedita", flags.charAt(0));
                                    recordJsonObject.addProperty("editaSubedita", flags.charAt(1));
                                    recordJsonObject.addProperty("flagNonSocio", flags.charAt(2));
                                    recordJsonObject.addProperty("flagContoSpeciale", flags.charAt(3));
                                }

                                final Titoli titoli = titoliNoSql.get(idOpera);
                                if (null != titoli) {
                                    addTitoli(recordJsonObject, titoli);
                                }
                                else{
                                    logger.debug("no titoli found for idOpera {}", idOpera);
                                }

                                final Codici codici = codiciNoSql.get(idOpera);
                                if (null != codici) {
                                    addArrayOfObjects(recordJsonObject, "iswc", codici.iswc, "codice");
                                    addArrayOfObjects(recordJsonObject, "isrc", codici.isrc, "codice");
                                    addArrayOfObjects(recordJsonObject, "codiciRepertorio", codici.repertoire, "codice");
                                    addArrayOfObjects(recordJsonObject, "codiciPratica", codici.dossier, "codice");
                                    addArrayOfObjects(recordJsonObject, "codiciEditore", codici.editors, "codice");
                                }

                                final Interpreti interpreti = interpretiNoSql.get(idOpera);
                                if (null != interpreti) {
                                    final JsonArray jsonArray = new JsonArray();
                                    for (Interprete interprete : interpreti.interpreti) {
                                        final JsonObject jsonObject = new JsonObject();
                                        jsonObject.addProperty("nome", interprete.nominativo);
                                        jsonObject.addProperty("tipo", interprete.tipo);
                                        jsonArray.add(jsonObject);
                                    }
                                    recordJsonObject.add("interpreti", jsonArray);
                                }
                                
                                final Maturati maturati = maturatoNoSqlDb.get(idOpera);
                                if (null != maturati) {
                                    final JsonArray jsonArray = new JsonArray();
                                    for (Maturato maturato : maturati.maturati) {
                                        final JsonObject jsonObject = new JsonObject();
                                        jsonObject.addProperty("valore", maturato.valore);
                                        jsonObject.addProperty("periodoLabel", maturato.periodoLabel);
                                        jsonObject.addProperty("dataDa", maturato.dataDa);
                                        jsonObject.addProperty("dataA", maturato.dataA);
                                        jsonArray.add(jsonObject);
                                    }
                                    recordJsonObject.add("maturati", jsonArray);
                                }
                                
                                final Collegamenti collegamenti = collegamentiNoSql.get(idOpera);
                                if( collegamenti != null ){
                                    addCollegamenti(recordJsonObject, collegamenti, titoliNoSql);
                                }

                                final Map<String, Set<String>> codiciIpiConQualifiche = addQuoteRiparto(recordJsonObject, ipiNameReverseTrie, agmNoSql, quote);
                                addIpiObject(recordJsonObject, ipiNameReverseTrie, ipiNameNoSql, codiciIpiConQualifiche);

                                // write to output
//									GsonUtils.toJson(recordJsonObject, jsonWriter);
                                writer.write(GsonUtils.toJson(recordJsonObject));
                                writer.write('\n');
                            }
						}

						//close and upload last file
						if (null != writer) {
                            writer.close();
                            out.close();
                            String s3FileUrl = new StringBuilder()
                                    .append(outputS3FolderUrl)
                                    .append('/')
                                    .append(outputFile.getName())
                                    .toString();
                            if (!s3.upload(new S3.Url(s3FileUrl), outputFile)) {
                                logger.error("error uploading file {} to {}", outputFile.getName(), s3FileUrl);
                                throw new IOException(String
                                        .format("error uploading file %s to %s", outputFile.getName(), s3FileUrl));
                            } else {
                                logger.debug("file {} uploaded to {}", outputFile.getName(), s3FileUrl);
                            }
                        }
						
						logger.debug("total quote(s) {}", heartbeat.getTotalPumps());
						
					}

				}
				
				logger.debug("quote export completed in {}", TextUtils
						.formatDuration(System.currentTimeMillis() - startTimeMillis));
			}
			
			
			// print missing ipnamenr(s)
			final File homeFolder = new File(configuration.getProperty("default.home_folder"));
			final File file = new File(homeFolder, "missing-ipnamenrs.csv");
			try (final OutputStream out = new CompressionAwareFileOutputStream(file);
					final Writer writer = new OutputStreamWriter(out, charset)) {
				for (Long ipnamenr : missingIpnamenrs) {
					writer.write(String.format("%d\n", ipnamenr));
				}
			}
			
		}

		logger.info("export completed in {}", TextUtils
				.formatDuration(System.currentTimeMillis() - processStartTimeMillis));
	}
}
