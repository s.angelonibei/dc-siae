package com.alkemytech.sophia.royalties.nosql;

import com.alkemytech.sophia.commons.nosql.NoSqlEntity;
import com.google.gson.GsonBuilder;

import java.util.Objects;

/**
 * Created by Alessandro Russo on 06/02/2019.
 */
public class Anagrafica implements NoSqlEntity {

    public Long idSchema;
    public String idOpera;
    public String idSchemaFlat;
    public String codiceOpera;
    public String dataDichiarazione;
    public String dataDichiarazioneSiada;
    public String dataAcquisizione;
    public String dataPubblicoDominio;
    public String tipologiaDeposito;
    public String musica;
    public String testo;
    public String esemplare;
    public String genereOpera;
    public String durata;
    public String notizieParticolari;
    public String tipoBollettino;
    public String codProvvEditore;
    public String riferimento;
    public String flagPD;
    public String flagEL;
    public String flagSempreverdi;
    public String flagMaggiorazione;
    public String flagAccettataConfusoria;
    public String ambito;
    public String statoOpera;
    public String nPacco;
    public String inviante;
    public String rilevanza;
    public String maturato;
    public String rischio;
    public String semestreScadenza;
    public String dataSemScadenza;
    public String semestreDichiarazione;
    public String dataSemDichiarazione;
    public String semestreValidita;
    public String dataSemValidita;
    public String tipoUtilizzo;
    public String territorio;
    public String ricercabile;
    public String idCollectingCompany;

    public Anagrafica(Long idSchema) {
        this.idSchema = idSchema;
    }

    @Override
    public String getPrimaryKey() {
        return Long.toString(idSchema);
    }

    @Override
    public String getSecondaryKey() {
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Anagrafica)) return false;
        Anagrafica that = (Anagrafica) o;
        return Objects.equals(idSchema, that.idSchema) &&
                Objects.equals(idOpera, that.idOpera) &&
                Objects.equals(idSchemaFlat, that.idSchemaFlat) &&
                Objects.equals(codiceOpera, that.codiceOpera) &&
                Objects.equals(tipoUtilizzo, that.tipoUtilizzo) &&
                Objects.equals(territorio, that.territorio);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idSchema, idOpera, idSchemaFlat, codiceOpera, tipoUtilizzo, territorio);
    }

    @Override
    public String toString() {
        return new GsonBuilder()
                .create().toJson(this);
    }
}