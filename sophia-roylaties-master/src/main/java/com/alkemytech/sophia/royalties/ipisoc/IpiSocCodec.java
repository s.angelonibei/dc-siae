package com.alkemytech.sophia.royalties.ipisoc;

import java.nio.charset.Charset;

import com.alkemytech.sophia.commons.mmap.ObjectCodec;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class IpiSocCodec extends ObjectCodec<IpiSoc> {

	private final Charset charset;
	
	public IpiSocCodec(Charset charset) {
		super();
		this.charset = charset;
	}

	@Override
	public IpiSoc bytesToObject(byte[] bytes) {
		beginDecoding(bytes);
		final String ipnamenr = getString(charset);
		final String rgt = getString(charset);
		final String rol = getString(charset);
		final String shr = getString(charset);
		final String societa = getString(charset);
		return new IpiSoc(ipnamenr, rgt, rol, shr, societa);
	}

	@Override
	public byte[] objectToBytes(IpiSoc object) {
		beginEncoding();
		putString(object.ipnamenr, charset);
		putString(object.rgt, charset);
		putString(object.rol, charset);
		putString(object.shr, charset);
		putString(object.societa, charset);
		return commitEncoding();
	}
	
}
