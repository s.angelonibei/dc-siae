package com.alkemytech.sophia.royalties.nosql;

import com.google.gson.GsonBuilder;

import java.util.Objects;
import java.util.Set;

/**
 * Created by Alessandro Russo on 04/02/2019.
 */
public class Collegamento {

    public long idOperaCollegata;
    public String codiceOperaCollegata;
    public String tipoCollegamento;


    public Collegamento(long idOperaCollegata) {
        this.idOperaCollegata = idOperaCollegata;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Collegamento)) return false;
        Collegamento that = (Collegamento) o;
        return idOperaCollegata == that.idOperaCollegata;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idOperaCollegata);
    }

    @Override
    public String toString() {
        return new GsonBuilder()
                .create().toJson(this);
    }
}