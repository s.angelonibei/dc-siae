package com.alkemytech.sophia.royalties.nosql;

import java.nio.charset.Charset;

import com.alkemytech.sophia.commons.mmap.ObjectCodec;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class InterpretiCodec extends ObjectCodec<Interpreti> {

	private final Charset charset;
	
	public InterpretiCodec(Charset charset) {
		super();
		this.charset = charset;
	}

	@Override
	public Interpreti bytesToObject(byte[] bytes) {
		beginDecoding(bytes);
		final long idAnagOpera = getPackedLong();
		final Interpreti interpreti = new Interpreti(idAnagOpera);
		for (int size = getPackedInt(); size > 0; size --) {
			final String nominativo = getString(charset);
			final String tipo = getString(charset);
			interpreti.interpreti.add(new Interprete(nominativo, tipo));
		}
		return interpreti;
	}

	@Override
	public byte[] objectToBytes(Interpreti interpreti) {
		beginEncoding();
		putPackedLong(interpreti.idAnagOpera);
		putPackedInt(interpreti.interpreti.size());
		for (Interprete interprete : interpreti.interpreti) {
			putString(interprete.nominativo, charset);
			putString(interprete.tipo, charset);
		}
		return commitEncoding();
	}
	
}
