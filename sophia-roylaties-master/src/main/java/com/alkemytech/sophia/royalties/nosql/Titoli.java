package com.alkemytech.sophia.royalties.nosql;

import java.util.HashSet;
import java.util.Set;

import com.alkemytech.sophia.commons.nosql.NoSqlEntity;
import com.google.common.base.Strings;
import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class Titoli implements NoSqlEntity {

	public final long idAnagOpera;
	public final Set<Titolo> titoli;



	public Titoli(long idAnagOpera) {
		super();
		this.idAnagOpera = idAnagOpera;
		this.titoli = new HashSet<>();
	}

	public void add(Titolo titolo) {
		if (null == titolo)
			return;
			titoli.add(titolo);
	}

	@Override
	public String getPrimaryKey() {
		return Long.toString(idAnagOpera);
	}

	@Override
	public String getSecondaryKey() {
		return null;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}
	
}
