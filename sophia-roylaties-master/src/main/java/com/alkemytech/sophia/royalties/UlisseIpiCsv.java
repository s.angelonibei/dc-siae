package com.alkemytech.sophia.royalties;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.ServerSocket;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.QuoteMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.io.CompressionAwareFileOutputStream;
import com.alkemytech.sophia.commons.util.HeartBeat;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.royalties.jdbc.IpiDataSource;
import com.alkemytech.sophia.royalties.jdbc.UlisseDataSource;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Scopes;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class UlisseIpiCsv {
	
	private static final Logger logger = LoggerFactory.getLogger(UlisseIpiCsv.class);

	protected static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// amazon service(s)
			bind(S3.class)
				.in(Scopes.SINGLETON);
			bind(SQS.class)
				.in(Scopes.SINGLETON);
			// data source(s)
			bind(DataSource.class)
				.annotatedWith(Names.named("ULISSE"))
				.to(UlisseDataSource.class)
				.asEagerSingleton();
			bind(DataSource.class)
				.annotatedWith(Names.named("IPI"))
				.to(IpiDataSource.class)
				.asEagerSingleton();
			// other binding(s)
			bind(UlisseIpiCsv.class)
				.asEagerSingleton();
		}

	}

	public static void main(String[] args) {
		try {
			final UlisseIpiCsv instance = Guice
					.createInjector(new GuiceModuleExtension(args,
							"/ulisse-ipi-csv.properties"))
				.getInstance(UlisseIpiCsv.class)
					.startup();
			try {
				instance.process(args);
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}

	private final Properties configuration;
	private final Charset charset;
	private final S3 s3;
	private final SQS sqs;
	private final DataSource ulisseDataSource;
	private final DataSource ipiDataSource;

	@Inject
	protected UlisseIpiCsv(@Named("configuration") Properties configuration,
			@Named("charset") Charset charset,
			S3 s3, SQS sqs, 
			@Named("ULISSE") DataSource ulisseDataSource,
			@Named("IPI") DataSource ipiDataSource) {
		super();
		this.configuration = configuration;
		this.charset = charset;
		this.s3 = s3;
		this.sqs = sqs;
		this.ulisseDataSource = ulisseDataSource;
		this.ipiDataSource = ipiDataSource;
	}

	public UlisseIpiCsv startup() throws IOException {
		if ("true".equalsIgnoreCase(configuration.getProperty("ulisse_ipi_csv.locked", "true"))) {
			throw new IllegalStateException("application locked");
		}
		s3.startup();
		sqs.startup();
		return this;
	}

	public UlisseIpiCsv shutdown() throws IOException {
		sqs.shutdown();
		s3.shutdown();
		return this;
	}

	private void dump(DataSource dataSource, String name) throws Exception {
		if ("true".equalsIgnoreCase(configuration
				.getProperty("ulisse_ipi_csv." + name + ".export"))) {
			final long startTimeMillis = System.currentTimeMillis();
			final File homeFolder = new File(configuration
					.getProperty("default.home_folder"));
			final File file = new File(homeFolder, name + ".csv.gz");
			if (file.length() > 0L) {
				logger.debug("file aleady exists {}", file.getAbsolutePath());
				return;
			}
			final int fetchSize = Integer.parseInt(configuration
					.getProperty("ulisse_ipi_csv." + name + ".fetch_size", "-1"));
			logger.debug("fetchSize {}", fetchSize);
			try (final Connection connection = dataSource.getConnection();
					final OutputStream out = new CompressionAwareFileOutputStream(file);
					final Writer writer = new OutputStreamWriter(out, charset);
					final CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT
							.withDelimiter(',')
							.withIgnoreSurroundingSpaces()
							.withQuoteMode(QuoteMode.MINIMAL)
							.withQuote('"')
							.withNullString(""))) {
				logger.debug("jdbc connected to {} {}", connection.getMetaData()
						.getDatabaseProductName(), connection.getMetaData().getURL());
					
				// setup one-by-one rows retrieval
				final Statement statement;
				if (-1 == fetchSize) {
					statement = connection.createStatement();						
				} else {
					statement = connection
							.createStatement(ResultSet.TYPE_FORWARD_ONLY,
									ResultSet.CONCUR_READ_ONLY);
					statement.setFetchSize(fetchSize);
				}
				
				// query
				final String sql = configuration.getProperty("ulisse_ipi_csv." + name + ".sql");
				logger.debug("sql {}", sql);
				try (final ResultSet resultSet = statement.executeQuery(sql)) {
					final HeartBeat heartbeat = HeartBeat.constant(name, Integer
							.parseInt(configuration.getProperty("ulisse_ipi_csv." + name + ".heartbeat", "10000")));
	
					final ResultSetMetaData metadata = resultSet.getMetaData();
					final int columnCount = metadata.getColumnCount();

					// fetch rows one-by-one
					while (resultSet.next()) {
						for (int i = 1; i <= columnCount; i ++) {
							csvPrinter.print(resultSet.getString(i));
						}
						csvPrinter.println();						
						heartbeat.pump();
					}
					logger.debug("total " + name + "(s) {}", heartbeat.getTotalPumps());
					
				}

			}
			logger.debug(name + " export completed in {}", TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));
			logger.debug(name + " file size {}", TextUtils.formatSize(file.length()));
		}
	}
	
	public void process(String[] args) throws Exception {
		final long startTimeMillis = System.currentTimeMillis();
		final int bindPort = Integer.parseInt(configuration.getProperty("ulisse_ipi_csv.bind_port",
				configuration.getProperty("default.bind_port", "0")));
		// bind lock tcp port
		try (ServerSocket socket = new ServerSocket(bindPort)) {
			logger.debug("socket bound to {}", socket.getLocalSocketAddress());

			dump(ipiDataSource, "ipi");
			
			dump(ulisseDataSource, "opere");
			dump(ulisseDataSource, "codici");
			dump(ulisseDataSource, "titoli");
			dump(ulisseDataSource, "interpreti");
			dump(ulisseDataSource, "flags");
			
		}
		logger.debug("export completed in {}", TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));
	}
	
}
