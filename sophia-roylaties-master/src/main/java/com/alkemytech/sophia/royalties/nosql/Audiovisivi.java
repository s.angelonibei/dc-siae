package com.alkemytech.sophia.royalties.nosql;

import com.alkemytech.sophia.commons.nosql.NoSqlEntity;
import com.google.common.base.Strings;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alessandro Russo on 25/01/2019.
 */
public class Audiovisivi implements NoSqlEntity {

    public final long idOpera;
    public final List<String> codici;
    public final List<String> titoli;
    public final List<String> titoliSerie;
    public final List<String> anni;
    public final List<String> registi;

    public Audiovisivi(long idOpera) {
        this.idOpera = idOpera;
        this.codici = new ArrayList<>();
        this.titoli = new ArrayList<>();
        this.titoliSerie = new ArrayList<>();
        this.anni = new ArrayList<>();
        this.registi = new ArrayList<>();
    }

    public void add(String codice, String titolo, String titoloSerie, String anno, String regista) {
        if (Strings.isNullOrEmpty(codice))
            return;
        codici.add(codice);
        titoli.add(Strings.isNullOrEmpty(titolo) ? "" : titolo);
        titoliSerie.add(Strings.isNullOrEmpty(titoloSerie) ? "" : titoloSerie);
        anni.add(Strings.isNullOrEmpty(anno) ? "" : anno);
        registi.add(Strings.isNullOrEmpty(regista) ? "" : regista);
    }

    @Override
    public String getPrimaryKey() {
        return Long.toString(idOpera);
    }

    @Override
    public String getSecondaryKey() {
        return null;
    }

    @Override
    public String toString() {
        return new GsonBuilder()
                .create().toJson(this);
    }
}
