package com.alkemytech.sophia.royalties.nosql;

import com.alkemytech.sophia.commons.nosql.NoSqlEntity;
import com.google.common.base.Strings;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Alessandro Russo on 29/01/2019.
 */
public class Contratti implements NoSqlEntity {

    public final long idSchema;
    public final List<String> codiciContratto;
    public final List<String> dateScadenza;
    public final List<String> tipiContratto;
    public final List<String> statiContratto;
    public final List<String> territori;
    public final List<String> codiciSchedula;

    public Contratti(long idSchema) {
        this.idSchema = idSchema;
        codiciContratto = new ArrayList<>();
        dateScadenza = new ArrayList<>();
        tipiContratto = new ArrayList<>();
        statiContratto = new ArrayList<>();
        territori = new ArrayList<>();
        codiciSchedula = new ArrayList<>();
    }

    public void add(String codiceContratto, String dataScadenza, String tipoContratto, String statoContratto, String territorio, String codiceSchedula) {
        if (null == codiceContratto)
            return;

        codiceContratto = codiceContratto.trim();
        if (Strings.isNullOrEmpty(codiceContratto))
            return;

        codiciContratto.add(Strings.isNullOrEmpty(codiceContratto) ? "" : codiceContratto);
        dateScadenza.add(Strings.isNullOrEmpty(dataScadenza) ? "" : dataScadenza);
        tipiContratto.add(Strings.isNullOrEmpty(tipoContratto) ? "" : tipoContratto);
        statiContratto.add(Strings.isNullOrEmpty(statoContratto) ? "" : statoContratto);
        territori.add(Strings.isNullOrEmpty(territorio) ? "" : territorio);
        codiciSchedula.add(Strings.isNullOrEmpty(codiceSchedula) ? "" : codiceSchedula);
    }

    @Override
    public String getPrimaryKey() {
        return Long.toString(idSchema);
    }

    @Override
    public String getSecondaryKey() {
        return null;
    }

    @Override
    public String toString() {
        return new GsonBuilder()
                .create().toJson(this);
    }
}
