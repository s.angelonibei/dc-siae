package com.alkemytech.sophia.royalties.nosql;

import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class Quota {

	public final long idQuotaRiparto;
	
	public String nominativo;
	public String nome;
	public String cognome;
	public String posizioneSiae;
	public String codiceIpi;
	public String codiceConto;
	public String codiceContoEstero;
	public String qualifica;
	public String tipoQuota;
	public String catenaDiritto;
	public String qualificaIpi;
	public String quotaNumeratore;
	public String quotaDenominatore;
	public String quotaAd;

	public Quota(long idQuotaRiparto) {
		super();
		this.idQuotaRiparto = idQuotaRiparto;
	}

	@Override
	public int hashCode() {
		return 31 + (int) (idQuotaRiparto ^ (idQuotaRiparto >>> 32));
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (null == obj)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Quota other = (Quota) obj;
		if (idQuotaRiparto != other.idQuotaRiparto)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}
	
}
