package com.alkemytech.sophia.royalties.nosql;

import java.util.HashSet;
import java.util.Set;

import com.alkemytech.sophia.commons.nosql.NoSqlEntity;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.google.common.base.Strings;
import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class Irregolarita implements NoSqlEntity {

	public final long idAnagOpera;
	public final Set<String> irregolarita;
	
	public Irregolarita(long idAnagOpera) {
		super();
		this.idAnagOpera = idAnagOpera;
		this.irregolarita = new HashSet<>();
	}

	public void add(String csv) {
		if (null == csv)
			return;
		csv = csv.trim();
		if (Strings.isNullOrEmpty(csv))
			return;
		for (String value : TextUtils.split(csv, ",")) {
			irregolarita.add(value);
		}
	}
	
	@Override
	public String getPrimaryKey() {
		return Long.toString(idAnagOpera);
	}

	@Override
	public String getSecondaryKey() {
		return null;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}
	
}
