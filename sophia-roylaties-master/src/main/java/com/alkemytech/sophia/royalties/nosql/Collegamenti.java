package com.alkemytech.sophia.royalties.nosql;

import com.alkemytech.sophia.commons.nosql.NoSqlEntity;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Alessandro Russo on 04/02/2019.
 */
public class Collegamenti implements NoSqlEntity {

    public final long idOpera;
    public final Set<Collegamento> collegamenti;

    public Collegamenti(long idOpera) {
        this.idOpera = idOpera;
        this.collegamenti = new HashSet<>();
    }

    public void add(Collegamento collegamento) {
        collegamenti.add(collegamento);
    }

    @Override
    public String getPrimaryKey() {
        return Long.toString(idOpera);
    }

    @Override
    public String getSecondaryKey() {
        return null;
    }
}
