package com.alkemytech.sophia.royalties.nosql;

import com.alkemytech.sophia.commons.mmap.ObjectCodec;

import java.nio.charset.Charset;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class TitoliCodec extends ObjectCodec<Titoli> {

	private final Charset charset;
	
	public TitoliCodec(Charset charset) {
		super();
		this.charset = charset;
	}

	@Override
	public Titoli bytesToObject(byte[] bytes) {
		beginDecoding(bytes);
		final long idAnagOpera = getPackedLong();
		final Titoli titoli = new Titoli(idAnagOpera);
		for (int size = getPackedInt(); size > 0; size --) {
			String titolo = getString(charset);
			String lingua = getString(charset);
			String tipoTitolo = getString(charset);
			Titolo titoloObj = new Titolo(titolo, lingua, tipoTitolo);
			titoli.add(titoloObj);
		}
		return titoli;
	}

	@Override
	public byte[] objectToBytes(Titoli titoli) {
		beginEncoding();
		putPackedLong(titoli.idAnagOpera);
		putPackedInt(titoli.titoli.size());
		for (Titolo titolo : titoli.titoli) {
			putString(titolo.titolo, charset);
			putString(titolo.lingua, charset);
			putString(titolo.tipoTitolo, charset);
		}
		return commitEncoding();
	}
	
}
