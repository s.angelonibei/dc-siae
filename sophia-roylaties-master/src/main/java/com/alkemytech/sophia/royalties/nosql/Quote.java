package com.alkemytech.sophia.royalties.nosql;

import java.util.HashSet;
import java.util.Set;

import com.alkemytech.sophia.commons.nosql.NoSqlEntity;
import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class Quote implements NoSqlEntity {

	public final long idSchema;
	public final Set<Quota> quote;
	
	public Quote(long idSchema) {
		super();
		this.idSchema = idSchema;
		this.quote = new HashSet<>();
	}

	public void add(Quota quota) {
		quote.add(quota);
	}
	
	@Override
	public String getPrimaryKey() {
		return Long.toString(idSchema);
	}

	@Override
	public String getSecondaryKey() {
		return null;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}
	
}
