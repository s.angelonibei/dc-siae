package com.alkemytech.sophia.royalties.nosql;

import com.alkemytech.sophia.commons.nosql.NoSqlEntity;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Alessandro Russo on 07/02/2019.
 */
public class Anagrafiche implements NoSqlEntity {

    public Long idSchema;
    public Set<Anagrafica> anagrafiche;

    public Anagrafiche(Long idSchema) {
        this.idSchema = idSchema;
        this.anagrafiche = new HashSet<>();
    }

    public void add ( Anagrafica anagrafica ){
        anagrafiche.add(anagrafica);
    }

    @Override
    public String getPrimaryKey() {
        return Long.toString(idSchema);
    }

    @Override
    public String getSecondaryKey() {
        return null;
    }
}
