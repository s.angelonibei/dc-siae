package com.alkemytech.sophia.royalties.country;

import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class Country {

	private long tisN;
	private String tisA;
	private String tisAExt;
	private String nome;
	
	public long getTisN() {
		return tisN;
	}
	public Country setTisN(long tisN) {
		this.tisN = tisN;
		return this;
	}
		
	public String getTisA() {
		return tisA;
	}
	public Country setTisA(String tisA) {
		this.tisA = tisA;
		return this;
	}
	
	public String getTisAExt() {
		return tisAExt;
	}
	public Country setTisAExt(String tisAExt) {
		this.tisAExt = tisAExt;
		return this;
	}
	
	public String getNome() {
		return nome;
	}
	public Country setNome(String nome) {
		this.nome = nome;
		return this;
	}
	
	@Override
	public int hashCode() {
		return 31 + (int) (tisN ^ (tisN >>> 32));
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Country other = (Country) obj;
		if (tisN != other.tisN)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}
	
}
