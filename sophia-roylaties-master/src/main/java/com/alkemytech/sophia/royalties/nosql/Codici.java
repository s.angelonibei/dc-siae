package com.alkemytech.sophia.royalties.nosql;

import java.util.HashSet;
import java.util.Set;

import com.alkemytech.sophia.commons.nosql.NoSqlEntity;
import com.google.common.base.Strings;
import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class Codici implements NoSqlEntity {

	public final long idAnagOpera;
	public final Set<String> iswc;
	public final Set<String> isrc;
	public final Set<String> repertoire;
	public final Set<String> dossier;
	public final Set<String> editors;

	public Codici(long idAnagOpera) {
		super();
		this.idAnagOpera = idAnagOpera;
		this.iswc = new HashSet<>();
		this.isrc = new HashSet<>();
		this.repertoire = new HashSet<>();
		this.dossier = new HashSet<>();
		this.editors = new HashSet<>();
	}

	public void add(String codice, String type) {
		if (null == codice)
			return;
		codice = codice.trim();
		if (Strings.isNullOrEmpty(codice))
			return;
		if ("2".equals(type)) {
			repertoire.add(codice);
		} else if ("3".equals(type)) {
			iswc.add(codice);
		} else if ("10".equals(type)) {
			dossier.add(codice);
		} else if ("14".equals(type)) {
			editors.add(codice);
		} else if ("15".equals(type)) {
			isrc.add(codice);
		}
		else {
			throw new IllegalArgumentException(String
					.format("unknown type %s", type));
		}
	}
	
	@Override
	public String getPrimaryKey() {
		return Long.toString(idAnagOpera);
	}

	@Override
	public String getSecondaryKey() {
		return null;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}
	
}
