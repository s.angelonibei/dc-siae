package com.alkemytech.sophia.royalties.ipisoc;

import com.alkemytech.sophia.commons.nosql.NoSqlEntity;
import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class IpiSoc implements NoSqlEntity {

	public static String getPrimaryKey(String ipnamenr, String rgt, String rol) {
		return new StringBuilder()
//				.append("LY".equals(rol) ? 'A' : "MC".equals(rol) ? 'B' : "EM".equals(rol) ? 'C' : 'D')
//				.append("DEM".equals(rgt) ? 'E' : 'R')
				.append(rol)
				.append(rgt)
				.append(ipnamenr)
				.toString();
	}
	
	public String ipnamenr;
	public String rgt;
	public String shr;
	public String societa;
	public String rol;
	
	public IpiSoc(String ipnamenr, String rgt, String rol, String shr, String societa) {
		super();
		this.ipnamenr = ipnamenr;
		this.rgt = rgt;
		this.rol = rol;
		this.shr = shr;
		this.societa = societa;
	}

	@Override
	public String getPrimaryKey() {
		return getPrimaryKey(ipnamenr, rgt, rol);
	}

	@Override
	public String getSecondaryKey() {
		return null;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}
	
}
