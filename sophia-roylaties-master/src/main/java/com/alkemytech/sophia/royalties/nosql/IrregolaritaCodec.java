package com.alkemytech.sophia.royalties.nosql;

import java.nio.charset.Charset;

import com.alkemytech.sophia.commons.mmap.ObjectCodec;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class IrregolaritaCodec extends ObjectCodec<Irregolarita> {

	private final Charset charset;
	
	public IrregolaritaCodec(Charset charset) {
		super();
		this.charset = charset;
	}

	@Override
	public Irregolarita bytesToObject(byte[] bytes) {
		beginDecoding(bytes);
		final long idAnagOpera = getPackedLong();
		final Irregolarita irregolarita = new Irregolarita(idAnagOpera);
		for (int size = getPackedInt(); size > 0; size --) {
			irregolarita.add(getString(charset));
		}
		return irregolarita;
	}

	@Override
	public byte[] objectToBytes(Irregolarita irregolarita) {
		beginEncoding();
		putPackedLong(irregolarita.idAnagOpera);
		putPackedInt(irregolarita.irregolarita.size());
		for (String value : irregolarita.irregolarita) {
			putString(value, charset);
		}
		return commitEncoding();
	}
	
}
