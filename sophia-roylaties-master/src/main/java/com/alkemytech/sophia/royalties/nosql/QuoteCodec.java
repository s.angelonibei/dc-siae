package com.alkemytech.sophia.royalties.nosql;

import java.nio.charset.Charset;

import com.alkemytech.sophia.commons.mmap.ObjectCodec;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class QuoteCodec extends ObjectCodec<Quote> {

	private final Charset charset;
	
	public QuoteCodec(Charset charset) {
		super();
		this.charset = charset;
	}

	@Override
	public Quote bytesToObject(byte[] bytes) {
		beginDecoding(bytes);
		final long idSchema = getPackedLong();
		final Quote quote = new Quote(idSchema);
		for (int size = getPackedInt(); size > 0; size --) {
			final long idQuotaRiparto = getPackedLong();
			final Quota quota = new Quota(idQuotaRiparto);
			quota.nominativo = getString(charset);
			quota.nome = getString(charset);
			quota.cognome = getString(charset);
			quota.posizioneSiae = getString(charset);
			quota.codiceIpi = getString(charset);
			quota.codiceConto = getString(charset);
			quota.codiceContoEstero = getString(charset);
			quota.qualifica = getString(charset);
			quota.tipoQuota = getString(charset);
			quota.catenaDiritto = getString(charset);
			quota.qualificaIpi = getString(charset);
			quota.quotaNumeratore = getString(charset);
			quota.quotaDenominatore = getString(charset);
			quota.quotaAd = getString(charset);
			quote.add(quota);
		}
		return quote;
	}

	@Override
	public byte[] objectToBytes(Quote quote) {
		beginEncoding();
		putPackedLong(quote.idSchema);
		putPackedInt(quote.quote.size());
		for (Quota quota : quote.quote) {
			putPackedLong(quota.idQuotaRiparto);
			putString(quota.nominativo, charset);
			putString(quota.nome, charset);
			putString(quota.cognome, charset);
			putString(quota.posizioneSiae, charset);
			putString(quota.codiceIpi, charset);
			putString(quota.codiceConto, charset);
			putString(quota.codiceContoEstero, charset);
			putString(quota.qualifica, charset);
			putString(quota.tipoQuota, charset);
			putString(quota.catenaDiritto, charset);
			putString(quota.qualificaIpi, charset);
			putString(quota.quotaNumeratore, charset);
			putString(quota.quotaDenominatore, charset);
			putString(quota.quotaAd, charset);
		}
		return commitEncoding();
	}
	
}
