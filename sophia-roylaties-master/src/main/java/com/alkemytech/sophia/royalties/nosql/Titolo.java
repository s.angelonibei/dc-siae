package com.alkemytech.sophia.royalties.nosql;

import com.google.gson.GsonBuilder;

import java.util.Objects;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class Titolo {

	public String titolo;
	public String lingua;
	public String tipoTitolo;


	public Titolo(String titolo, String lingua, String tipoTitolo) {
		super();
		this.titolo = titolo;
		this.lingua = lingua;
		this.tipoTitolo = tipoTitolo;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Titolo)) return false;
		Titolo titolo1 = (Titolo) o;
		return Objects.equals(titolo, titolo1.titolo) &&
				Objects.equals(lingua, titolo1.lingua) &&
				Objects.equals(tipoTitolo, titolo1.tipoTitolo);
	}

	@Override
	public int hashCode() {
		return Objects.hash(titolo, lingua);
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}
	
}
