package com.alkemytech.sophia.royalties.nosql;

import java.nio.charset.Charset;

import com.alkemytech.sophia.commons.mmap.ObjectCodec;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class CodiciCodec extends ObjectCodec<Codici> {

	private final Charset charset;
	
	public CodiciCodec(Charset charset) {
		super();
		this.charset = charset;
	}

	@Override
	public Codici bytesToObject(byte[] bytes) {
		beginDecoding(bytes);
		final long idAnagOpera = getPackedLong();
		final Codici codici = new Codici(idAnagOpera);
		for (int size = getPackedInt(); size > 0; size --) {
			codici.repertoire.add(getString(charset));
		}
		for (int size = getPackedInt(); size > 0; size --) {
			codici.iswc.add(getString(charset));
		}
		for (int size = getPackedInt(); size > 0; size --) {
			codici.isrc.add(getString(charset));
		}
		return codici;
	}

	@Override
	public byte[] objectToBytes(Codici codici) {
		beginEncoding();
		putPackedLong(codici.idAnagOpera);
		putPackedInt(codici.repertoire.size());
		for (String codice : codici.repertoire) {
			putString(codice, charset);
		}
		putPackedInt(codici.iswc.size());
		for (String codice : codici.iswc) {
			putString(codice, charset);
		}
		putPackedInt(codici.isrc.size());
		for (String codice : codici.isrc) {
			putString(codice, charset);
		}
		return commitEncoding();
	}
	
}
