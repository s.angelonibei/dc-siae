package com.alkemytech.sophia.royalties.nosql;

import com.alkemytech.sophia.commons.mmap.ObjectCodec;

import java.nio.charset.Charset;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class CollegamentiCodec extends ObjectCodec<Collegamenti> {

	private final Charset charset;

	public CollegamentiCodec(Charset charset) {
		super();
		this.charset = charset;
	}

	@Override
	public Collegamenti bytesToObject(byte[] bytes) {
		beginDecoding(bytes);
		final long idOpera = getPackedLong();
		final Collegamenti collegamenti = new Collegamenti(idOpera);
		for (int size = getPackedInt(); size > 0; size --) {
			final long idOperaCollegata = getPackedLong();
			final Collegamento collegamento = new Collegamento(idOperaCollegata);
			collegamento.codiceOperaCollegata= getString(charset);
			collegamento.tipoCollegamento = getString(charset);
			collegamenti.add(collegamento);
		}
		return collegamenti;
	}

	@Override
	public byte[] objectToBytes(Collegamenti collegamenti) {
		beginEncoding();
		putPackedLong(collegamenti.idOpera);
		putPackedInt(collegamenti.collegamenti.size());
		for (Collegamento collegamento : collegamenti.collegamenti) {
			putPackedLong(collegamento.idOperaCollegata);
			putString(collegamento.codiceOperaCollegata, charset);
			putString(collegamento.tipoCollegamento, charset);
		}
		return commitEncoding();
	}
	
}
