package com.alkemytech.sophia.royalties.nosql;

import com.alkemytech.sophia.commons.mmap.MemoryMapException;
import com.alkemytech.sophia.commons.mmap.ObjectCodec;

import java.nio.charset.Charset;

/**
 * Created by Alessandro Russo on 25/01/2019.
 */
public class AudiovisiviCodec extends ObjectCodec<Audiovisivi> {

    private final Charset charset;

    public AudiovisiviCodec(Charset charset) {
        super();
        this.charset = charset;
    }

    @Override
    public Audiovisivi bytesToObject(byte[] bytes) throws MemoryMapException {
        beginDecoding(bytes);
        final long idOpera = getPackedLong();
        final Audiovisivi audiovisivi = new Audiovisivi(idOpera);
        for (int size = getPackedInt(); size > 0; size --) {
            final String codice = getString(charset);
            final String titolo = getString(charset);
            final String titoloSerie = getString(charset);
            final String anno = getString(charset);
            final String regista = getString(charset);
            audiovisivi.add(codice, titolo, titoloSerie, anno, regista);
        }
        return audiovisivi;
    }

    @Override
    public byte[] objectToBytes(Audiovisivi audiovisivi) throws MemoryMapException {
        beginEncoding();
        putLong(audiovisivi.idOpera);
        putPackedInt(audiovisivi.codici.size());
        for (int i = audiovisivi.codici.size() - 1; i >= 0; i --) {
            putString(audiovisivi.codici.get(i), charset);
            putString(audiovisivi.titoli.get(i), charset);
            putString(audiovisivi.titoliSerie.get(i), charset);
            putString(audiovisivi.anni.get(i), charset);
            putString(audiovisivi.registi.get(i), charset);
        }
        return commitEncoding();
    }
}
