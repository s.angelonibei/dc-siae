-- DROP UTI ERRO TABLE
DROP TABLE IF EXISTS PERF_RS_ERR_SHOW_MUSIC;
DROP TABLE IF EXISTS PERF_RS_ERR_SHOW_SCHEDULE;
-- DROP UTI TABLE
DROP TABLE IF EXISTS PERF_RS_SHOW_MUSIC;
DROP TABLE IF EXISTS PERF_RS_SHOW_SCHEDULE;
-- DROP KPI TABLE


create table PERF_RS_ERR_SHOW_SCHEDULE
(
  ID_RS_ERR_SHOW_SCHEDULE bigint auto_increment
    primary key,
  ID_NORMALIZED_FILE bigint not null,
  ID_PALINSESTO bigint null,
  TITLE text null,
  BEGIN_TIME datetime null,
  END_TIME datetime null,
  DURATION int null,
  REGIONAL_OFFICE varchar(10) null,
  ERROR json null,
  GLOBAL_ERROR varchar(400) null,
  REPORT_PALINSESTO varchar(100) null,
  REPORT_BEGIN_DATE varchar(100) null,
  REPORT_BEGIN_TIME varchar(100) null,
  REPORT_DURATION varchar(100) null,
  SCHEDULE_YEAR int(4) null,
  SCHEDULE_MONTH int(2) null,
  DAYS_FROM_UPLOAD int default 0 null,
  CREATION_DATE datetime default CURRENT_TIMESTAMP not null,
  MODIFY_DATE datetime null,
  constraint FK_ERR_RS_SHOW_NORM_FILE
    foreign key (ID_NORMALIZED_FILE) references PERF_RS_NORMALIZED_FILE (ID_NORMALIZED_FILE),
  constraint FK_ERR_RS_SHOW_PAL
    foreign key (ID_PALINSESTO) references PERF_PALINSESTO (ID_PALINSESTO)
)
  comment 'Utilizzazioni scartate relative alle Trasmissioni in ambito RADIO IN STORE(RS)';


create table PERF_RS_ERR_SHOW_MUSIC
(
  ID_RS_ERR_SHOW_MUSIC bigint auto_increment
    primary key,
  ID_NORMALIZED_FILE bigint not null,
  ID_RS_ERR_SHOW_SCHEDULE bigint null,
  ID_MUSIC_TYPE bigint null,
  TITLE text null,
  COMPOSER text null,
  PERFORMER text null,
  ALBUM text null,
  DURATION int null,
  ISWC_CODE varchar(50) null,
  ISRC_CODE varchar(50) null,
  BEGIN_TIME datetime null,
  REAL_DURATION int null,
  REPORT_DURATION varchar(100) null,
  REPORT_BEGIN_TIME varchar(100) null,
  REPORT_MUSIC_TYPE varchar(100) null,
  DAYS_FROM_UPLOAD int default 0 null,
  CREATION_DATE datetime default CURRENT_TIMESTAMP not null,
  MODIFY_DATE datetime null,
  constraint FK_ERR_RS_MUSIC_NORM_FILE
    foreign key (ID_NORMALIZED_FILE) references PERF_RS_NORMALIZED_FILE (ID_NORMALIZED_FILE),
  constraint FK_ERR_RS_MUSIC_SHOW
    foreign key (ID_RS_ERR_SHOW_SCHEDULE) references PERF_RS_ERR_SHOW_SCHEDULE (ID_RS_ERR_SHOW_SCHEDULE),
  constraint FK_ERR_RS_MUSIC_TYPE
    foreign key (ID_MUSIC_TYPE) references MUSIC_TYPE (ID_MUSIC_TYPE)
)
  comment 'Utilizzazioni scartate relative alle Opere Musicali in ambito RADIO IN STORE(RS)';

create index IDX_ERR_RS_SHOW_BEGIN_DATE
  on PERF_RS_ERR_SHOW_SCHEDULE (BEGIN_TIME);


create table PERF_RS_SHOW_SCHEDULE
(
  ID_RS_SHOW_SCHEDULE bigint auto_increment
    primary key,
  ID_NORMALIZED_FILE bigint not null,
  ID_PALINSESTO bigint not null,
  TITLE varchar(400) null,
  BEGIN_TIME datetime null,
  END_TIME datetime null,
  DURATION int null,
  REGIONAL_OFFICE varchar(10) null,
  NOTE varchar(400) null,
  OVERLAP bit null,
  SCHEDULE_YEAR int(4) not null,
  SCHEDULE_MONTH int(2) not null,
  DAYS_FROM_UPLOAD int default 0 not null,
  CREATION_DATE datetime default CURRENT_TIMESTAMP null,
  MODIFY_DATE datetime null,
  constraint FK_RS_SHOW_CHANNEL
    foreign key (ID_PALINSESTO) references PERF_PALINSESTO (ID_PALINSESTO),
  constraint FK_RS_SHOW_NORM_FILE
    foreign key (ID_NORMALIZED_FILE) references PERF_RS_NORMALIZED_FILE (ID_NORMALIZED_FILE)
)
  comment 'Utilizzazioni accettate relative alle Trasmissioni in ambito RADIO IN STORE(RS)';


create table PERF_RS_SHOW_MUSIC
(
  ID_RS_SHOW_MUSIC bigint auto_increment
    primary key,
  ID_NORMALIZED_FILE bigint not null,
  ID_RS_SHOW_SCHEDULE bigint null,
  ID_MUSIC_TYPE bigint null,
  TITLE text null,
  COMPOSER text null,
  PERFORMER text null,
  ALBUM text null,
  DURATION int null,
  ISWC_CODE varchar(50) null,
  ISRC_CODE varchar(50) null,
  BEGIN_TIME datetime null,
  REAL_DURATION int null,
  NOTE varchar(400) null,
  DAYS_FROM_UPLOAD int default 0 not null,
  CREATION_DATE datetime default CURRENT_TIMESTAMP null,
  MODIFY_DATE datetime null,
  constraint FK_RS_MUSIC_NORM_FILE
    foreign key (ID_NORMALIZED_FILE) references PERF_RS_NORMALIZED_FILE (ID_NORMALIZED_FILE),
  constraint FK_RS_MUSIC_SHOW
    foreign key (ID_RS_SHOW_SCHEDULE) references PERF_RS_SHOW_SCHEDULE (ID_RS_SHOW_SCHEDULE),
  constraint FK_RS_MUSIC_TYPE
    foreign key (ID_MUSIC_TYPE) references MUSIC_TYPE (ID_MUSIC_TYPE)
)
  comment 'Utilizzazioni accettate relative alle Opera Musicali in ambito RADIO IN STORE(RS)';

create index IDX_RS_SHOW_BEGIN_DATE
  on PERF_RS_SHOW_SCHEDULE (BEGIN_TIME);

-- DROP KPI TABLE
DROP TABLE IF EXISTS PERF_RS_KPI;
DROP TABLE IF EXISTS PERF_RS_ERR_BRANO_TITOLO;
DROP TABLE IF EXISTS PERF_RS_ERR_COMPOSITORE_TITOLO;


create table PERF_RS_ERR_BRANO_TITOLO
(
  ID_RS_ERR_BRANO_TITOLO int auto_increment
    primary key,
  RS_ERR_BRANO_TITOLO varchar(400) null
);

create table PERF_RS_ERR_COMPOSITORE_TITOLO
(
  ID_RS_ERR_COMPOSITORE_TITOLO int auto_increment
    primary key,
  RS_ERR_COMPOSITORE_TITOLO varchar(400) null
);

create table PERF_RS_KPI
(
  ID_PERF_RS_KPI bigint auto_increment
    primary key,
  ID_MUSIC_PROVIDER bigint not null,
  ID_PALINSESTO bigint not null,
  ANNO int not null,
  MESE int not null,
  INDICATORE varchar(255) null,
  VALORE int null,
  LAST_UPDATE datetime null,
  constraint FK_PERF_RS_KPI_PAL
    foreign key (ID_PALINSESTO) references PERF_PALINSESTO (ID_PALINSESTO)
);

create index IDX_PERF_RS_KPI_FULL
  on PERF_RS_KPI (ID_MUSIC_PROVIDER, ID_PALINSESTO, ANNO, MESE);

create index IDX_PERF_RS_KPI_PAL
  on PERF_RS_KPI (ID_PALINSESTO);