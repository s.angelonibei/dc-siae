#!/bin/sh

export ENVNAME=test
APPCONTEXT=broadcasting
APPNAME=broadcasting-harmonizer
HOME=/var/local/sophia/$ENVNAME/$APPCONTEXT
OUTPUT=$HOME/logs/$APPNAME.out
VERSION=5.3


nohup  java  -Dlog4j.configurationFile=$HOME/log4j2_harmonizer.xml -Djava.io.tmpdir=$HOME -Dfile.encoding=UTF-8  -jar $HOME/broadcasting-harmonizer-1.0.0-jar-with-dependencies.jar $HOME/configuration.properties 2>&1 >> $OUTPUT &
