package com.alkemytech.sophia.harmonizer.model;

/**
 * Created by Alessandro Russo on 07/12/2018.
 */
public class DecodeMusicType {

    private Long idDecodeMusicType;
    private Long idMusicType;
    private Long idCustomer;
    private String value;
    private String scope;

    public DecodeMusicType(Object[] tuple) {
        this.idDecodeMusicType = (Long) tuple[0];
        this.idMusicType = (Long) tuple[1];
        this.idCustomer = (Long) tuple[2];
        this.value = (String) tuple[3];
        this.scope = (String) tuple[4];
    }

    public Long getIdDecodeMusicType() {
        return idDecodeMusicType;
    }

    public void setIdDecodeMusicType(Long idDecodeMusicType) {
        this.idDecodeMusicType = idDecodeMusicType;
    }

    public Long getIdMusicType() {
        return idMusicType;
    }

    public void setIdMusicType(Long idMusicType) {
        this.idMusicType = idMusicType;
    }

    public Long getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(Long idCustomer) {
        this.idCustomer = idCustomer;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }
}
