package com.alkemytech.sophia.harmonizer.model;

/**
 * Created by Alessandro Russo on 07/12/2018.
 */
public class CustomerUnit {

    private Long    idCustomerUnit;
    private Long    idCustomer;
    private String  name;
    private String  codeBusiness;
    private String  codeId;

    public CustomerUnit(Object[] tuple) {
        this.idCustomerUnit = (Long) tuple[0];
        this.idCustomer = (Long) tuple[1];
        this.name = (String) tuple[2];
        this.codeBusiness = (String) tuple[3];
        this.codeId = (String) tuple[4];
    }

    public Long getIdCustomerUnit() {
        return idCustomerUnit;
    }

    public void setIdCustomerUnit(Long idCustomerUnit) {
        this.idCustomerUnit = idCustomerUnit;
    }

    public Long getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(Long idCustomer) {
        this.idCustomer = idCustomer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCodeBusiness() {
        return codeBusiness;
    }

    public void setCodeBusiness(String codeBusiness) {
        this.codeBusiness = codeBusiness;
    }

    public String getCodeId() {
        return codeId;
    }

    public void setCodeId(String codeId) {
        this.codeId = codeId;
    }
}
