package com.alkemytech.sophia.harmonizer.model;

/**
 * Created by Alessandro Russo on 05/12/2018.
 */
public class HarmonizationConfig {
    String supportedFormat;
    Integer expectedFiles;
    String fileValidator;
    String parseConfigPath;
    String parseStreamName;
    String fileParser;
    String normalizeRulePath;
    String validateRulePath;
    String writeConfigPath;
    String writeConfigStream;
    String writeTemplateBucket;
    String writeUploadBucket;
    String writeFileExtension;

    public String getSupportedFormat() {
        return supportedFormat;
    }

    public void setSupportedFormat(String supportedFormat) {
        this.supportedFormat = supportedFormat;
    }

    public Integer getExpectedFiles() {
        return expectedFiles;
    }

    public void setExpectedFiles(Integer expectedFiles) {
        this.expectedFiles = expectedFiles;
    }

    public String getFileValidator() {
        return fileValidator;
    }

    public void setFileValidator(String fileValidator) {
        this.fileValidator = fileValidator;
    }

    public String getParseConfigPath() {
        return parseConfigPath;
    }

    public void setParseConfigPath(String parseConfigPath) {
        this.parseConfigPath = parseConfigPath;
    }

    public String getParseStreamName() {
        return parseStreamName;
    }

    public void setParseStreamName(String parseStreamName) {
        this.parseStreamName = parseStreamName;
    }

    public String getFileParser() {
        return fileParser;
    }

    public void setFileParser(String fileParser) {
        this.fileParser = fileParser;
    }

    public String getNormalizeRulePath() {
        return normalizeRulePath;
    }

    public void setNormalizeRulePath(String normalizeRulePath) {
        this.normalizeRulePath = normalizeRulePath;
    }

    public String getValidateRulePath() {
        return validateRulePath;
    }

    public void setValidateRulePath(String validateRulePath) {
        this.validateRulePath = validateRulePath;
    }

    public String getWriteConfigPath() {
        return writeConfigPath;
    }

    public void setWriteConfigPath(String writeConfigPath) {
        this.writeConfigPath = writeConfigPath;
    }

    public String getWriteConfigStream() {
        return writeConfigStream;
    }

    public void setWriteConfigStream(String writeConfigStream) {
        this.writeConfigStream = writeConfigStream;
    }

    public String getWriteTemplateBucket() {
        return writeTemplateBucket;
    }

    public void setWriteTemplateBucket(String writeTemplateBucket) {
        this.writeTemplateBucket = writeTemplateBucket;
    }

    public String getWriteUploadBucket() {
        return writeUploadBucket;
    }

    public void setWriteUploadBucket(String writeUploadBucket) {
        this.writeUploadBucket = writeUploadBucket;
    }

    public String getWriteFileExtension() {
        return writeFileExtension;
    }

    public void setWriteFileExtension(String writeFileExtension) {
        this.writeFileExtension = writeFileExtension;
    }

    @Override
    public String toString() {
        return "HarmonizationConfig{" +
                "supportedFormat='" + supportedFormat + '\'' +
                ", expectedFiles=" + expectedFiles +
                ", fileValidator='" + fileValidator + '\'' +
                ", parseConfigPath='" + parseConfigPath + '\'' +
                ", parseStreamName='" + parseStreamName + '\'' +
                ", fileParser='" + fileParser + '\'' +
                ", normalizeRulePath='" + normalizeRulePath + '\'' +
                ", validateRulePath='" + validateRulePath + '\'' +
                ", writeConfigPath='" + writeConfigPath + '\'' +
                ", writeConfigStream='" + writeConfigStream + '\'' +
                ", writeTemplateBucket='" + writeTemplateBucket + '\'' +
                ", writeUploadBucket='" + writeUploadBucket + '\'' +
                ", writeFileExtension='" + writeFileExtension + '\'' +
                '}';
    }
}
