package com.alkemytech.sophia.harmonizer.parser;

/**
 * Created by Alessandro Russo on 08/12/2017.
 */
public interface ParserParameters {

    public static final String CONFIGURATION = "configuration";
    public static final String FILES         = "fileList";


}
