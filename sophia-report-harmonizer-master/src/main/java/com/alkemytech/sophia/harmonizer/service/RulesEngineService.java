package com.alkemytech.sophia.harmonizer.service;

import com.alkemytech.sophia.common.s3.S3Service;
import com.amazonaws.services.s3.AmazonS3URI;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.*;

import java.io.InputStream;

/**
 * Created by Alessandro Russo on 05/12/2017.
 */
@Singleton
public class RulesEngineService {

    private S3Service s3Service;

    @Inject
    public RulesEngineService(S3Service s3Service) {
        this.s3Service = s3Service;
    }

    public KnowledgeBase createKnowledgeBaseFromSpreadsheet( String resourcePath ) throws Exception {
        DecisionTableConfiguration dtconf = KnowledgeBuilderFactory.newDecisionTableConfiguration();
        dtconf.setInputType(DecisionTableInputType.XLS);
        KnowledgeBuilder knowledgeBuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
        AmazonS3URI amazonS3URI = new AmazonS3URI(resourcePath);
        InputStream is = s3Service.download(amazonS3URI.getBucket(),amazonS3URI.getKey());
        knowledgeBuilder.add(org.drools.io.ResourceFactory.newInputStreamResource(is), ResourceType.DTABLE, dtconf);
        if (knowledgeBuilder.hasErrors()) {
            throw new RuntimeException(knowledgeBuilder.getErrors().toString());
        }
        KnowledgeBase knowledgeBase = KnowledgeBaseFactory.newKnowledgeBase();
        knowledgeBase.addKnowledgePackages(knowledgeBuilder.getKnowledgePackages());
        return knowledgeBase;
    }
}
