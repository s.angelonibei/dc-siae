package com.alkemytech.sophia.harmonizer.parser;

import com.alkemytech.sophia.common.Constants;
import com.alkemytech.sophia.common.mapper.TemplateMapper;
import com.alkemytech.sophia.common.s3.S3Service;
import com.alkemytech.sophia.harmonizer.service.DbService;
import com.alkemytech.sophia.harmonizer.service.RulesEngineService;
import com.alkemytech.sophia.harmonizer.utils.Utilities;
import com.amazonaws.util.StringUtils;
import org.drools.runtime.StatefulKnowledgeSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Alessandro Russo on 07/12/2017.
 */
public abstract class GenericParser {

    Logger logger = LoggerFactory.getLogger(getClass());

    public static final String PARSED_RECORDS = "records";
    public static final String ERRORS = "errors";

    protected DbService dbService;
    protected RulesEngineService rulesEngineService;
    protected S3Service s3Service;
    protected TemplateMapper mapper;

    public abstract Map<String, Object> parse(Map<String, Object> parameters);

    protected List<Map<String,Object>> normalize(Long idBroadcaster, String normalizedRule, List<Object> records) throws Exception {
        List<Map<String,Object> > normalizedRecords = new ArrayList<>();
        logger.debug("rule file is -> {}", normalizedRule );
        StatefulKnowledgeSession session = rulesEngineService.createKnowledgeBaseFromSpreadsheet(normalizedRule).newStatefulKnowledgeSession();
        for(Object record : records){
            Map<String, Object> castRecord = (Map<String, Object>) record;
            if( Utilities.isFilleMap(castRecord) ){
                logger.debug("- CAST RECORD -> {}", castRecord.toString());
                Map<String, Object> normalizedRecord = new HashMap<>();
                List<String> errors = new ArrayList<>();
                session.setGlobal("idCustomer", idBroadcaster);
                session.setGlobal("dbService", dbService);
                session.setGlobal(Constants.GLOBAL_RECORD, normalizedRecord);
                session.setGlobal(Constants.GLOBAL_ERROR, errors);
                session.insert(castRecord);
                session.fireAllRules();
                logger.debug("- NORMALIZED RECORD -> {}", normalizedRecord.toString());
                normalizedRecords.add(normalizedRecord);
            }
        }
        return normalizedRecords;
    }

    public void setMapper(TemplateMapper mapper) {
        this.mapper = mapper;
    }

    public void setRulesEngineService(RulesEngineService rulesEngineService) {
        this.rulesEngineService = rulesEngineService;
    }

    public void setS3Service(S3Service s3Service) {
        this.s3Service = s3Service;
    }

    public void setDbService(DbService dbService) {
        this.dbService = dbService;
    }

    protected String getConfigProperty(String configProperty, String type){
        String[] configPropertyArray = getConfigList(configProperty);
        if(configPropertyArray != null){
            for(String currentValue: configPropertyArray){
                if(currentValue.contains(type)){
                    return currentValue;
                }
            }
        }
        return null;
    }

    private String[] getConfigList (String configProperty){
        if(!StringUtils.isNullOrEmpty( configProperty ) ){
            return configProperty.split("\\|");
        }
        return null;
    }
}
