package com.alkemytech.sophia.harmonizer.model;

import javax.persistence.SqlResultSetMapping;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Alessandro Russo on 05/12/2018.
 */
public class UtilizationReport implements Serializable {

    private Long    idFile;
    private Long    idCustomer;
    private Long    idCustomerUnit;
    private String  fileName;
    private String  filePath;
    private Integer year;
    private Integer month;
    private Date    uploadDate;
    private String  uploadType;

    public UtilizationReport() {
    }

    public UtilizationReport(Object[] tuple) {
        this.idFile =           (Long)      tuple[0];
        this.idCustomer =       (Long)      tuple[1];
        this.idCustomerUnit =   (Long)      tuple[2];
        this.fileName =         (String)    tuple[3];
        this.filePath =         (String)    tuple[4];
        this.year =             (Integer)   tuple[5];
        this.month =            (Integer)   tuple[6];
        this.uploadDate =       (Date)      tuple[7];
        this.uploadType =       (String)    tuple[8];
    }

    public Long getIdFile() {
        return idFile;
    }

    public void setIdFile(Long idFile) {
        this.idFile = idFile;
    }

    public Long getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(Long idCustomer) {
        this.idCustomer = idCustomer;
    }

    public Long getIdCustomerUnit() {
        return idCustomerUnit;
    }

    public void setIdCustomerUnit(Long idCustomerUnit) {
        this.idCustomerUnit = idCustomerUnit;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public String getUploadType() {
        return uploadType;
    }

    public void setUploadType(String uploadType) {
        this.uploadType = uploadType;
    }

    @Override
    public String toString() {
        return "UtilizationReport{" +
                "idFile=" + idFile +
                ", idCustomer=" + idCustomer +
                ", idCustomerUnit=" + idCustomerUnit +
                ", fileName='" + fileName + '\'' +
                ", filePath='" + filePath + '\'' +
                ", year=" + year +
                ", month=" + month +
                ", uploadDate=" + uploadDate +
                ", uploadType='" + uploadType + '\'' +
                '}';
    }
}