package com.alkemytech.sophia.harmonizer.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Alessandro Russo on 06/12/2018.
 */
@MappedSuperclass
public class NormalizedReport {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "ID_NORMALIZED_FILE" )
    private Long idNormalizedFile;

    @Column(name = "FILE_NAME")
    private String fileName;

    @Column(name = "FILE_BUCKET")
    private String fileBucket;

    @Column(name = "CREATION_DATE")
    private Date creationDate;

    public Long getIdNormalizedFile() {
        return idNormalizedFile;
    }

    public void setIdNormalizedFile(Long idNormalizedFile) {
        this.idNormalizedFile = idNormalizedFile;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileBucket() {
        return fileBucket;
    }

    public void setFileBucket(String fileBucket) {
        this.fileBucket = fileBucket;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}
