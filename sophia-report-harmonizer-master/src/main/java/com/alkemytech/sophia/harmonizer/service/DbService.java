package com.alkemytech.sophia.harmonizer.service;

import com.alkemytech.sophia.harmonizer.model.*;
import com.alkemytech.sophia.harmonizer.model.radioInStore.PerfRSNormalizedFile;
import com.alkemytech.sophia.harmonizer.model.radioInStore.PerfRSUtilizationFile;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.*;

/**
 * Created by Alessandro Russo on 05/12/2018.
 */
@Singleton
public class DbService {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    Provider<EntityManager> provider;

    private Map<String, String> queriesMap;
    private Map<String, String> fileStatusMap;
    private List<Customer> activeCustomers;
    private List<CustomerUnit> activeCustomerUnits;
    private List<MusicType> musicTypes;
    private List<DecodeMusicType> decodeMusicTypes;


    @Inject
    public DbService(Provider<EntityManager> provider,
                     @Named("harmonization.query.json") String harmonizationQueryJson,
                     @Named("file.status.json") String fileStatusJson) {

        this.provider = provider;
        Gson gson = new Gson();
        queriesMap = StringUtils.isNotEmpty(harmonizationQueryJson) ? gson.fromJson(harmonizationQueryJson, Map.class) : new HashMap<>();
        fileStatusMap = StringUtils.isNotEmpty(fileStatusJson) ? gson.fromJson(fileStatusJson, Map.class) : new HashMap<>();
    }

    public void start() {
        loadActiveCustomers();
        loadActiveCustomerUnits();
        loadMusicType();
        loadDecodeMusicType();
    }

    private void loadActiveCustomers() {
        this.activeCustomers = new ArrayList<>();
        if( queriesMap.containsKey("load.customer.query")){
            EntityManager entityManager = provider.get();
            final Query q = entityManager.createNativeQuery(queriesMap.get("load.customer.query"));
            List<Object[]> queryResult = q.getResultList();
            if(CollectionUtils.isNotEmpty(queryResult)){
                for(Object[] tuple : queryResult){
                    activeCustomers.add(new Customer(tuple));
                }
            }
        }
    }

    private void loadActiveCustomerUnits() {
        this.activeCustomerUnits = new ArrayList<>();
        if( queriesMap.containsKey("load.customer_unit.query")){
            EntityManager entityManager = provider.get();
            final Query q = entityManager.createNativeQuery(queriesMap.get("load.customer_unit.query"));
            List<Object[]> queryResult = q.getResultList();
            if(CollectionUtils.isNotEmpty(queryResult)){
                for(Object[] tuple : queryResult){
                    activeCustomerUnits.add(new CustomerUnit(tuple));
                }
            }
        }
    }

    private void loadMusicType() {
        this.musicTypes = new ArrayList<>();
        if( queriesMap.containsKey("load.music.type.query")){
            EntityManager entityManager = provider.get();
            final Query q = entityManager.createNativeQuery(queriesMap.get("load.music.type.query"));
            List<Object[]> queryResult = q.getResultList();
            if(CollectionUtils.isNotEmpty(queryResult)){
                for(Object[] tuple : queryResult){
                    musicTypes.add(new MusicType(tuple));
                }
            }
        }
    }

    private void loadDecodeMusicType() {
        this.decodeMusicTypes = new ArrayList<>();
        if( queriesMap.containsKey("load.decode.music.type.query")){
            EntityManager entityManager = provider.get();
            final Query q = entityManager.createNativeQuery(queriesMap.get("load.decode.music.type.query"));
            List<Object[]> queryResult = q.getResultList();
            if(CollectionUtils.isNotEmpty(queryResult)){
                for(Object[] tuple : queryResult){
                    decodeMusicTypes.add(new DecodeMusicType(tuple));
                }
            }
        }
    }

    public List<Object[]> retrieveFileToProcess() throws Exception {
        if( fileStatusMap.containsKey("to.process") ){
            return retrieveUtilizationFile(fileStatusMap.get("to.process"));
        }
        throw new Exception("PROPERTY -> to.process NOT FOUND in fileStatusJson");
    }

    public List<Object[]> retrieveValidatedFile() throws Exception {
        if( fileStatusMap.containsKey("validated") ){
            return retrieveUtilizationFile(fileStatusMap.get("validated"));
        }
        throw new Exception("PROPERTY -> validated NOT FOUND in fileStatusJson");
    }

    public List<Object[]> retrieveErrorFile() throws Exception {
        if( fileStatusMap.containsKey("error") ){
            return retrieveUtilizationFile(fileStatusMap.get("error"));
        }
        throw new Exception("PROPERTY -> error NOT FOUND in fileStatusJson");
    }

    private List<Object[]> retrieveUtilizationFile(String fileStatus) throws Exception {
        if( queriesMap.containsKey("retrieve.utilization.file.query")){
            EntityManager entityManager = provider.get();
            final Query q = entityManager.createNativeQuery(queriesMap.get("retrieve.utilization.file.query"));
            List<Object[]> queryResult = q.setParameter(1, fileStatus).getResultList();
            return queryResult;
        }
        throw new Exception("PROPERTY -> retrieve.utilization.file.query NOT FOUND in harmonizationQueryJson");
    }

    public String retrieveHarmonizationConfiguration(Long idCustomer) throws Exception {
        if( queriesMap.containsKey("retrieve.harmonization.config.query")){
            EntityManager entityManager = provider.get();
            final Query q = entityManager.createNativeQuery(queriesMap.get("retrieve.harmonization.config.query"));
            String queryResult = (String) q.setParameter(1, idCustomer).getSingleResult();
            return queryResult;
        }
        throw new Exception("PROPERTY -> retrieve.harmonization.config.query NOT FOUND in harmonizationQueryJson");
    }

    public Long findIdCustomerUnitByCode(Long idCustomer, String code) {
        for( CustomerUnit customerUnit : activeCustomerUnits){
            if( idCustomer.equals(customerUnit.getIdCustomer()) && code.equalsIgnoreCase(customerUnit.getCodeId())){
                return customerUnit.getIdCustomerUnit();
            }
        }
        return null;
    }

    public Long findIdMusicTypeByValueAndScope(Long idCustomer, String value, String scope){
        for( DecodeMusicType decodeMusicType : decodeMusicTypes){
            if( idCustomer.equals(decodeMusicType.getIdCustomer()) &&
                value.equalsIgnoreCase(decodeMusicType.getValue()) &&
                scope.equalsIgnoreCase(decodeMusicType.getScope())
            ){
                return  decodeMusicType.getIdMusicType();
            }
        }
        return null;
    }

    public String findNameMusicTypeByValueAndScope(Long idCustomer, String value, String scope){
        for( DecodeMusicType decodeMusicType : decodeMusicTypes){
            if( idCustomer.equals(decodeMusicType.getIdCustomer()) &&
                    value.equalsIgnoreCase(decodeMusicType.getValue()) &&
                    scope.equalsIgnoreCase(decodeMusicType.getScope())
            ){
                return  getNameMusicTypeById(decodeMusicType.getIdMusicType());
            }
        }
        return "";
    }

    public String getNameMusicTypeById(Long idMusicType) {
        for( MusicType musicType : musicTypes){
            if( idMusicType.equals(musicType.getIdMusicType())){
                return  musicType.getName();
            }
        }
        return "";
    }

    public boolean updateFileStatus(Long idFile, String status) throws Exception {
        if( queriesMap.containsKey("update.utilization.file.status.query")){
            EntityManager entityManager = provider.get();
            openTransaction(entityManager);
            try{
                final Query q = entityManager.createNativeQuery(queriesMap.get("update.utilization.file.status.query"));
                int result = q.setParameter(1, status).setParameter(2, new Date()).setParameter(3, idFile).executeUpdate();
                commitTransaction(entityManager);
                return result > 0;
            }
            catch (Exception e){
                logger.error( "Error updating Utilization File Status", e );
                rollbackTransaction(entityManager);
            }
            return false;
        }
        throw new Exception("PROPERTY -> update.utilization.file.status.query  NOT FOUND in harmonizationQueryJson");
    }

    public Long saveNormalizedReport(String fileName, String bucketFile, List<UtilizationReport> reportToParseList) throws Exception {
        if( !queriesMap.containsKey("insert.normalized.file.query")) {
            throw new Exception("PROPERTY -> insert.normalized.file.query  NOT FOUND in harmonizationQueryJson");
        }
        else if( !queriesMap.containsKey("insert.utilization.normalized.file.query")) {
            throw new Exception("PROPERTY -> insert.utilization.normalized.file.query  NOT FOUND in harmonizationQueryJson");
        }
        else if( !queriesMap.containsKey("update.utilization.file.status.query")) {
            throw new Exception("PROPERTY -> update.utilization.file.status.query  NOT FOUND in harmonizationQueryJson");
        }
        else{
            EntityManager em = provider.get();
            openTransaction(em);
            try{
                //PERSIST NORMALIZED REPORT
                PerfRSNormalizedFile normalizedFile = new PerfRSNormalizedFile();
                normalizedFile.setFileName(fileName);
                normalizedFile.setFileBucket(bucketFile);
                normalizedFile.setCreationDate(new Date());
                for ( UtilizationReport utilizationReport : reportToParseList ) {
                    PerfRSUtilizationFile utilizationFile = em.find(PerfRSUtilizationFile.class, utilizationReport.getIdFile());
                    utilizationFile.setStato(fileStatusMap.get("validated"));
                    utilizationFile.setDataProcessamento(new Date());
                    normalizedFile.getUtilizationFiles().add(utilizationFile);
//                    final Query q = em.createNativeQuery(queriesMap.get("update.utilization.file.status.query"));
//                    int result = q.setParameter(1, fileStatusMap.get("validated")).setParameter(2, new Date()).setParameter(3, utilizationReport.getIdFile()).executeUpdate();
                }
                em.persist(normalizedFile);
//                final Query createNormalizedFileQuery = em.createNativeQuery(queriesMap.get("insert.normalized.file.query"));
//                createNormalizedFileQuery.setParameter(1, fileName).setParameter(2, bucketFile).setParameter(3, new Date() ).executeUpdate();
//                final BigInteger idNormalizedFile = (BigInteger) em.createNativeQuery("SELECT LAST_INSERT_ID()").getSingleResult();
//
//                //PERSIST UTILIZATION NORMALIZED RELATIONSHIP
//                final Query createUtilizationNormalizedFileQuery = em.createNativeQuery(queriesMap.get("insert.utilization.normalized.file.query"));
//                for ( UtilizationReport utilizationReport : reportToParseList ) {
//                    createUtilizationNormalizedFileQuery.setParameter(1, idNormalizedFile.longValue()).setParameter(2, utilizationReport.getIdFile()).executeUpdate();
//                    //UPDATE UTILIZATION FILE STATUS
//                    final Query q = em.createNativeQuery(queriesMap.get("update.utilization.file.status.query"));
//                    int result = q.setParameter(1, fileStatusMap.get("validated")).setParameter(2, new Date()).setParameter(3, utilizationReport.getIdFile()).executeUpdate();
//                }
                em.flush();
                commitTransaction(em);
                return normalizedFile.getIdNormalizedFile();
            }
            catch (Exception e){
                logger.error( "Error inserting Normalized File", e );
                rollbackTransaction(em);
            }
        }
        return null;
    }

    private void openTransaction( EntityManager em ){
        if( !em.getTransaction().isActive() ){
            em.getTransaction().begin();
        }
    }

    private void commitTransaction( EntityManager em ){
        if( em.getTransaction().isActive() ){
            em.getTransaction().commit();
        }
    }

    private void rollbackTransaction( EntityManager em ){
        if( em.getTransaction().isActive() ){
            em.getTransaction().rollback();
        }
    }

    public String getCustomerNameById(Long idCustomer) {
        for( Customer customer : activeCustomers){
            if( idCustomer.equals(customer.getIdCustomer())){
                return customer.getName();
            }
        }
        return "UNKNOWN_CUSTOMER";
    }
}