package com.alkemytech.sophia.harmonizer;

import com.alkemytech.sophia.common.Constants;
import com.alkemytech.sophia.common.s3.S3Service;
import com.alkemytech.sophia.harmonizer.guice.McmdbJpaModule;
import com.alkemytech.sophia.harmonizer.model.NormalizedReport;
import com.alkemytech.sophia.harmonizer.model.UtilizationReport;
import com.alkemytech.sophia.harmonizer.model.HarmonizationConfig;
import com.alkemytech.sophia.harmonizer.service.DbService;
import com.alkemytech.sophia.harmonizer.service.HarmonizationService;
import com.amazonaws.services.s3.AmazonS3URI;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.name.Named;
import com.google.inject.persist.PersistService;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.ServerSocket;
import java.util.List;
import java.util.Map;

/**
 * Created by Alessandro Russo on 01/12/2017.
 */
public class Harmonizer {

    private static final Logger logger = LoggerFactory.getLogger(Harmonizer.class);

    private final Integer listenPort;
    private final String fileStatusToProcess;
    private final String fileStatusError;
    private final HarmonizationService harmonizationService;

    @Inject
    public Harmonizer(@Named("listenPort") Integer listenPort,
                      @Named("utilization.file.status.to.process") String fileStatusToProcess,
                      @Named("utilization.file.status.error") String fileStatusError,
                      HarmonizationService harmonizationService) {
        this.listenPort = listenPort;
        this.harmonizationService = harmonizationService;
        this.fileStatusToProcess = fileStatusToProcess;
        this.fileStatusError = fileStatusError;
    }

    public static void main(String[] args) throws Exception {
        try {
            final Injector jpaInjector = Guice.createInjector( new McmdbJpaModule(args) );
            try{
                //Setup services
                jpaInjector.getInstance(S3Service.class).startup();
                jpaInjector.getInstance(PersistService.class).start();
                jpaInjector.getInstance(DbService.class).start();
                Harmonizer harmonizer = jpaInjector.getInstance(Harmonizer.class);
                //Run business logic
                harmonizer.process();
            }
            finally {
                //Stop services
                jpaInjector.getInstance(PersistService.class).stop();
                jpaInjector.getInstance(S3Service.class).shutdown();
            }
        } catch (Throwable e) {
            logger.error("main", e);
        } finally {
            System.exit(0);
        }
    }

    /**
     * Process Original Report in state fileStatusToProcess
     * @throws Exception
     */
    private void process() throws Exception {

        try(final ServerSocket socket = new ServerSocket( listenPort )){

            logger.info("HARMONIZATION TASK START: listening on {}", socket.getLocalSocketAddress());

            //Retrieve files to process
            List<UtilizationReport> fileToProcessList = harmonizationService.retrieveFileToProcess();
            if( CollectionUtils.isNotEmpty(fileToProcessList) ) {
                logger.debug("Retrieved {} files to process ", fileToProcessList.size());
                for( UtilizationReport currentFile : fileToProcessList ){
                    List<UtilizationReport> reportToParseList = null;
                    try{
                        //Reserve file on Amazon S3 in .prelock MODE
                        logger.info("----------------------------------------");
                        logger.info("START working {}", currentFile.getFilePath());
                        logger.info("----------------------------------------");
                        AmazonS3URI currentFileUri =  harmonizationService.reserveSingleFile(currentFile.getFilePath(), Constants.PRELOCK_SUFFIX);
                        if( currentFileUri != null ){
                            try{
                                //Load valid Configuration
                                HarmonizationConfig harmonizationConfig = harmonizationService.retrieveConfiguration(currentFile.getIdCustomer());
                                if(harmonizationConfig == null){
                                    logger.error("ERROR - No valid configuration for file with id {} and name {}", currentFile.getIdFile(), currentFile.getFileName() );
                                    harmonizationService.releaseSingleFile(currentFileUri.getBucket(), currentFileUri.getKey(), Constants.PRELOCK_SUFFIX);
                                    continue;
                                }
                                //Validate configuration and check files list to parse
                                Map<String, Object> evaluationMap = harmonizationService.evaluateFile(harmonizationConfig, currentFile, fileToProcessList);
                                harmonizationConfig = evaluationMap.containsKey("configuration") ? (HarmonizationConfig) evaluationMap.get("configuration") : null;
                                reportToParseList = evaluationMap.containsKey("fileToParseList") ? (List<UtilizationReport>) evaluationMap.get("fileToParseList") : null;
                                if( CollectionUtils.isNotEmpty(reportToParseList) && harmonizationConfig != null ){
                                    Map<UtilizationReport, AmazonS3URI> mapFile = harmonizationService.reserveMultipleFile(reportToParseList, Constants.LOCK_SUFFIX);
                                    if( CollectionUtils.isNotEmpty(mapFile.keySet()) ){
                                        //Parsing original report via BEANIO and mapping to normalized template via DROOLS
                                        List<Map<String, Object> > normalizedContent  = harmonizationService.parseReport(harmonizationConfig, mapFile);
                                        if( normalizedContent != null ){
                                            //Validating normalized field via DROOLS
                                            List<Map<String, Object> > validatedContent = harmonizationService.validateNormalizedRecords(currentFile, harmonizationConfig, normalizedContent);
                                            //Upload on S3 bucket and save on DB
                                            Long idNormalizedFile = harmonizationService.createNormalizedFile(harmonizationConfig, currentFile.getFilePath(), validatedContent, reportToParseList);
                                            if( idNormalizedFile == null){
                                                logger.error("ERROR - Peristing normalized file error" );
                                                harmonizationService.releaseMultipleFile(reportToParseList, Constants.LOCK_SUFFIX);
                                                harmonizationService.releaseSingleFile(currentFileUri.getBucket(), currentFileUri.getKey(), Constants.PRELOCK_SUFFIX);
                                                continue;
                                            }
                                            logger.info("----------------------------------------");
                                            logger.info("SUCCESS - {}", currentFile.getFilePath());
                                            logger.info("----------------------------------------");
                                        }
                                        else{
                                            logger.error("ERROR - Change file status to {}", fileStatusError);
                                            for(UtilizationReport report : reportToParseList){
                                                harmonizationService.changeUtilizationFileStatus( report.getIdFile(), fileStatusError );
                                            }
                                        }
                                    }
                                    else{
                                        logger.error("ERROR - Locking file error" );
                                        harmonizationService.releaseSingleFile(currentFileUri.getBucket(), currentFileUri.getKey(), Constants.PRELOCK_SUFFIX);
                                    }
                                }
                                else{
                                    logger.error("ERROR - Validation Error for file {} - {}", currentFile.getIdFile(), currentFile.getFileName() );
                                    harmonizationService.releaseSingleFile(currentFileUri.getBucket(), currentFileUri.getKey(), Constants.PRELOCK_SUFFIX);
                                }
                           }
                            finally {
                                harmonizationService.releaseMultipleFile(reportToParseList, Constants.LOCK_SUFFIX);
                                harmonizationService.releaseSingleFile(currentFileUri.getBucket(), currentFileUri.getKey(), Constants.PRELOCK_SUFFIX);
                            }
                        }
                        else{
                            logger.info("File {} already locked by other process!", currentFile.getFileName());
                        }
                    }
                    catch (Exception e){
                        if ( CollectionUtils.isNotEmpty( reportToParseList )) {
                            for(UtilizationReport report : reportToParseList){
                                harmonizationService.changeUtilizationFileStatus( report.getIdFile(), fileStatusError );
                            }
                        }
                        logger.error("-- HARMONIZATION TASK ERROR {}", e);
                    }
                }
            }
            else {
                logger.info("No Report with status {} found!", fileStatusToProcess);
            }
            logger.info("-- HARMONIZATION TASK END --");
        }
    }
}