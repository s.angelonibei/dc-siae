package com.alkemytech.sophia.harmonizer.model;

/**
 * Created by Alessandro Russo on 07/12/2018.
 */
public class MusicType {

    private Long idMusicType;
    private String category;
    private String name;

    public MusicType(Object[] tuple) {
        this.idMusicType = (Long) tuple[0];
        this.category = (String) tuple[1];
        this.name = (String) tuple[2];
    }

    public Long getIdMusicType() {
        return idMusicType;
    }

    public void setIdMusicType(Long idMusicType) {
        this.idMusicType = idMusicType;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
