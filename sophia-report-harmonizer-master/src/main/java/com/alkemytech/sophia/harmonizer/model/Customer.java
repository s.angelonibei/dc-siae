package com.alkemytech.sophia.harmonizer.model;

/**
 * Created by Alessandro Russo on 07/12/2018.
 */
public class Customer {

    private Long    idCustomer;
    private String  name;

    public Customer(Object[] tuple) {
        this.idCustomer = (Long) tuple[0];
        this.name = (String) tuple[1];
    }

    public Long getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(Long idCustomer) {
        this.idCustomer = idCustomer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
