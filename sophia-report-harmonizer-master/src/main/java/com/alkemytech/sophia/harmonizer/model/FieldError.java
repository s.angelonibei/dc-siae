package com.alkemytech.sophia.harmonizer.model;

/**
 * Created by Alessandro Russo on 13/12/2017.
 */
public class FieldError {

    private String fieldName;
    private String inputValue;

    public FieldError() {
    }

    public FieldError(String fieldName, String inputValue) {
        this.fieldName = fieldName;
        this.inputValue = inputValue;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getInputValue() {
        return inputValue;
    }

    public void setInputValue(String inputValue) {
        this.inputValue = inputValue;
    }

    @Override
    public String toString() {
        return "FieldError{" +
                "fieldName='" + fieldName + '\'' +
                ", inputValue='" + inputValue + '\'' +
                '}';
    }
}