package com.alkemytech.sophia.harmonizer.service;

import com.alkemytech.sophia.common.Constants;
import com.alkemytech.sophia.common.mapper.TemplateMapper;
import com.alkemytech.sophia.common.s3.S3Service;
import com.alkemytech.sophia.common.tools.StringTools;
import com.alkemytech.sophia.harmonizer.model.FieldError;
import com.alkemytech.sophia.harmonizer.model.HarmonizationConfig;
import com.alkemytech.sophia.harmonizer.model.NormalizedReport;
import com.alkemytech.sophia.harmonizer.model.UtilizationReport;
import com.alkemytech.sophia.harmonizer.parser.GenericParser;
import com.alkemytech.sophia.harmonizer.parser.ParserParameters;
import com.alkemytech.sophia.harmonizer.utils.Utilities;
import com.alkemytech.sophia.harmonizer.validator.GenericValidator;
import com.amazonaws.services.s3.AmazonS3URI;
import com.google.gson.Gson;
import com.google.inject.Inject;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.drools.runtime.StatefulKnowledgeSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by Alessandro Russo on 05/12/2018.
 */

public class HarmonizationService {

    private static final String ENVIRONMENT     = "ENVNAME";
    private static final String NORMALIZED_PATH = "%s/%s/NORMALIZED/%s/%s";
    private static final String SELECTED_FILE   = "selectedFile";
    private static final String FILE_LIST       = "fileList";

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private S3Service s3Service;
    private RulesEngineService rulesEngineService;
    private DbService dbService;
    private Gson gson;

    @Inject
    public HarmonizationService(S3Service s3Service, RulesEngineService rulesEngineService, DbService dbService) {
        this.s3Service = s3Service;
        this.rulesEngineService = rulesEngineService;
        this.dbService = dbService;
        this.gson = new Gson();
    }

    public boolean changeUtilizationFileStatus(Long idFile, String fileStatus) throws Exception {
        return dbService.updateFileStatus(idFile, fileStatus);
    }

    public List<Map<String, Object>> parseReport(HarmonizationConfig broadcasterConfig, Map<UtilizationReport, AmazonS3URI> mapFile) throws IOException {
        if(org.apache.commons.lang.StringUtils.isNotEmpty( broadcasterConfig.getFileParser())){
            try {
                GenericParser parser = (GenericParser) Class.forName(broadcasterConfig.getFileParser()).newInstance();
                parser.setS3Service(s3Service);
                parser.setDbService(dbService);
                parser.setRulesEngineService(rulesEngineService);
                parser.setMapper(new TemplateMapper());
                Map<String, Object> parameters = new HashMap<>();
                parameters.put(ParserParameters.CONFIGURATION, broadcasterConfig);
                parameters.put(ParserParameters.FILES, mapFile);
                Map<String, Object> ret = parser.parse(parameters);
                if(ret.containsKey(GenericParser.ERRORS)){
                    logger.error("parsing error : {}", ret.get(GenericParser.ERRORS));
                    return null;
                }
                logger.info("file parsed successfully!");
                return (List<Map<String, Object>>) ret.get(GenericParser.PARSED_RECORDS);
            }
            catch (Exception e) {
                logger.error(e.getMessage());
            }
        }
        else{
            logger.debug("Parser assente");
            return null;
        }
        return null;
    }

    public List<UtilizationReport> retrieveFileToProcess() throws Exception {
        List<UtilizationReport> result = new ArrayList<>();
        List<Object[]> queryResult = dbService.retrieveFileToProcess();
        if( CollectionUtils.isNotEmpty(queryResult) ){
            for( Object[] tuple : queryResult ){
                result.add(new UtilizationReport(tuple));
            }
        }
        return result;
    }

    public boolean updateUtilizationFileStatus( Long idFile, String status ) throws Exception {
        return dbService.updateFileStatus( idFile, status);
    }

    public HarmonizationConfig retrieveConfiguration(Long idCustomer) throws Exception {
        HarmonizationConfig result = null;
        String jsonConfig = dbService.retrieveHarmonizationConfiguration(idCustomer);
        if( StringUtils.isNotEmpty(jsonConfig) ){
            result = gson.fromJson(jsonConfig, HarmonizationConfig.class);
        }
        return result;
    }

    public Map<String, Object> evaluateFile (HarmonizationConfig configuration, UtilizationReport selectedFile, List<UtilizationReport> fileList ){
        Map<String, Object> ret = new HashMap<>();
        List<UtilizationReport> fileToParseList = new ArrayList<>();
        if( StringUtils.isNotEmpty(configuration.getFileValidator())){
            try{
                GenericValidator validator = (GenericValidator) Class.forName(configuration.getFileValidator()).newInstance();
                Map<String, Object> parameters = new HashMap<>();
                parameters.put(SELECTED_FILE, selectedFile);
                parameters.put(FILE_LIST, fileList);
                fileToParseList = validator.validateFile(parameters);
                configuration = validator.extractConfiguration(configuration, selectedFile);
            }
            catch (Exception e) {
                logger.error(e.getMessage());
            }
        }
        else{
            fileToParseList.add(selectedFile);
        }

        List<String> configMissingFields = validateFullConfiguration(configuration);
        if( configMissingFields.size() > 0){
            logger.error("Broadcaster configuration missing following mandatory fields: {}", org.apache.commons.lang.StringUtils.join(configMissingFields, ","));
        }
        else{
            ret.put("fileToParseList", fileToParseList);
            ret.put("configuration", configuration);
        }
        return ret;
    }

    public List<Map<String, Object>> validateNormalizedRecords(UtilizationReport currentFile, HarmonizationConfig configDto, List<Map<String, Object>> listNormalizedRecords) throws Exception {
        if(org.apache.commons.lang.StringUtils.isNotEmpty(configDto.getValidateRulePath())){
            StatefulKnowledgeSession session = rulesEngineService.createKnowledgeBaseFromSpreadsheet(configDto.getValidateRulePath()).newStatefulKnowledgeSession();
            for(Map<String, Object> normalizedRecord : listNormalizedRecords){
                setupDroolsSession(session, currentFile, normalizedRecord);
                session.insert(normalizedRecord);
                session.fireAllRules();
                Gson gson = new Gson();
                String errorString = gson.toJson(session.getGlobal(Constants.ERRORS));
                normalizedRecord.put(Constants.ERRORS, errorString);
                logger.debug("- VALIDATION ERRORS -> {}", errorString.toString());
            }
            logger.info("normalized file content validated successfully!");
            return listNormalizedRecords;
        }
        else{
            logger.error("Validation rules file not found in config!");
            return null;
        }
    }

    public Long createNormalizedFile(HarmonizationConfig broadcasterConfig, String bucketFile,
                                                 List<Map<String, Object>> validatedContent,
                                                 List<UtilizationReport> reportToParseList) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        File fileToWrite = null;
        try{
            String writeUploadBucket = org.apache.commons.lang.StringUtils.isNotEmpty(broadcasterConfig.getWriteUploadBucket()) ? broadcasterConfig.getWriteUploadBucket() : Constants.DEFAULT_WRITE_BUCKET;
            String writeUploadExt = org.apache.commons.lang.StringUtils.isNotEmpty(broadcasterConfig.getWriteFileExtension()) ? broadcasterConfig.getWriteFileExtension() : Constants.DEFAULT_WRITE_EXT;
            String emptyTemplateBucket = org.apache.commons.lang.StringUtils.isNotEmpty(broadcasterConfig.getWriteTemplateBucket()) ? broadcasterConfig.getWriteTemplateBucket() : Constants.DEFAULT_WRITE_BUCKET;
            String fileName = bucketFile.substring(bucketFile.lastIndexOf("/")+1, bucketFile.lastIndexOf("."))+writeUploadExt;
            String customerName = dbService.getCustomerNameById(reportToParseList.get(0).getIdCustomer());
            logger.debug("Current Environment {}", System.getenv(ENVIRONMENT));
            String bucketPath = StringTools.buildBucketfromBroadcaster(NORMALIZED_PATH, writeUploadBucket, customerName, fileName);
            AmazonS3URI amazonS3URI = new AmazonS3URI(emptyTemplateBucket);
            s3Service.download(amazonS3URI.getBucket(), amazonS3URI.getKey(), bos);
            //retrieve header from empty template
            TemplateMapper templateMapper = new TemplateMapper();
            Map<String, Object> header = templateMapper.parseEmpyTemplate(broadcasterConfig.getWriteConfigPath(), broadcasterConfig.getWriteConfigStream(), bos, s3Service);
            logger.debug("result " + header);
            fileToWrite = new File( fileName );
            logger.debug("create temp file {}", fileToWrite.getAbsolutePath());
            templateMapper.writeFile(header, validatedContent, broadcasterConfig.getWriteConfigPath(), broadcasterConfig.getWriteConfigStream(), fileToWrite, s3Service);
            amazonS3URI = new AmazonS3URI(bucketPath);
            logger.info("Load normalized file on Amazon S3 at {}", bucketPath);
            if (s3Service.upload(amazonS3URI.getBucket(), amazonS3URI.getKey(), fileToWrite)){
                return dbService.saveNormalizedReport( fileName, bucketPath, reportToParseList );
            }
        }
        catch (Exception e){
            logger.error(e.getMessage());
        }
        finally {
            bos.flush();
            bos.close();
            fileToWrite.deleteOnExit();
        }
        return null;
    }

    private void setupDroolsSession(StatefulKnowledgeSession session, UtilizationReport currentFile, Map<String, Object> normalizedRecord) {
//        boolean isTv = TipoBroadcaster.TELEVISIONE.equals(currentFile.getEmittente().getTipo_broadcaster());
//        if ( isTv ){
//            if(normalizedRecord.containsKey(Constants.DECODE_ID_GENERE_TX) && normalizedRecord.get(Constants.DECODE_ID_GENERE_TX) != null ){
//                ShowType showType = decodeService.findShowTypeById( (Long) normalizedRecord.get(Constants.DECODE_ID_GENERE_TX) );
//                normalizedRecord.put(Constants.DECODE_DESC_GENERE_TX, showType != null ? showType.getCategory() : null);
//            }
//            if(normalizedRecord.containsKey(Constants.DECODE_ID_CATEGORIA_USO_OP) && normalizedRecord.get(Constants.DECODE_ID_CATEGORIA_USO_OP) != null ){
//                MusicType musicType = decodeService.findMusicTypeById( (Long) normalizedRecord.get(Constants.DECODE_ID_CATEGORIA_USO_OP) );
//                normalizedRecord.put(Constants.DECODE_DESC_CATEGORIA_USO_OP, musicType != null ? musicType.getName() : null);
//            }
//        }

        //Setup error fields
        Map<String, List<FieldError>> errors = new HashMap<>();
        errors.put(Constants.REGOLA1, new ArrayList<FieldError>());
        errors.put(Constants.REGOLA2, new ArrayList<FieldError>());
        errors.put(Constants.REGOLA3, new ArrayList<FieldError>());
        session.setGlobal(Constants.ERRORS, errors);

        //Insert compare fields
        session.setGlobal("compareCustomerUnit", currentFile.getIdCustomerUnit() != null ? currentFile.getIdCustomerUnit().toString() : null);
        session.setGlobal(Constants.COMPARE_DATE, currentFile.getUploadDate());
        session.setGlobal(Constants.COMPARE_YEAR, currentFile.getYear());
        session.setGlobal(Constants.CURRENT_DATE, new Date());
        session.setGlobal(Constants.CURRENT_YEAR, Calendar.getInstance().get(Calendar.YEAR));
        Date date = null;
        if(currentFile.getYear() != null && currentFile.getMonth() != null){
            date = Utilities.getDateFromMonthAndYear(currentFile.getMonth(), currentFile.getYear());
        }
        session.setGlobal(Constants.BEGIN_MONTH, date != null ? Utilities.getMonthStart(date) : null);
        session.setGlobal(Constants.END_MONTH, date != null ? Utilities.getMonthEnd(date) : null);
    }

    //-- Start S3 Reserve/Release Service
    public Map<UtilizationReport, AmazonS3URI> reserveMultipleFile(List<UtilizationReport> fileList, String suffix ){
        Map<UtilizationReport, AmazonS3URI> ret = new HashMap<>();
        for(UtilizationReport currentFile : fileList){
            AmazonS3URI currentUri = reserveSingleFile( currentFile.getFilePath(), suffix);
            if(currentUri != null){
                ret.put(currentFile, currentUri);
            }
            else{
                releaseMultipleFile(ret.values(), suffix);
            }
        }
        return ret;
    }

    public AmazonS3URI reserveSingleFile(String filePath, String prelockSuffix) {
        if( StringUtils.isNotEmpty( filePath ) ){
            AmazonS3URI amazonS3URI = new AmazonS3URI(filePath);
            if( !s3Service.lockFile(amazonS3URI.getBucket(), amazonS3URI.getKey(), prelockSuffix) ){
                logger.debug("File {} preaviously reserved by other process", amazonS3URI.getKey());
                return null;
            }
            logger.debug("Reserved file at {}", filePath);
            return amazonS3URI;
        }
        return null;
    }

    public void releaseSingleFile(String bucket, String key, String suffix){
        if(s3Service.doesObjectExist(bucket, key+suffix)){
            s3Service.delete(bucket, key+suffix);
            logger.debug("Released file at {}", bucket+key);
        }
        logger.debug("Doesn't exist file at {}", bucket+key+suffix);
    }

    public void releaseMultipleFile(Collection<AmazonS3URI> amazonS3URIs, String suffix){
        if(amazonS3URIs != null){
            for(AmazonS3URI uri : amazonS3URIs){
                releaseSingleFile(uri.getBucket(), uri.getKey(), suffix);
            }
        }
    }

    public void releaseMultipleFile(List<UtilizationReport> fileEntityList, String suffix ){
        if(fileEntityList != null){
            for(UtilizationReport currentFile : fileEntityList){
                AmazonS3URI amazonS3URI = new AmazonS3URI(currentFile.getFilePath());
                releaseSingleFile(amazonS3URI.getBucket(), amazonS3URI.getKey(), suffix);
            }
        }
    }
    //-- End S3 Reserve/Release Service

    private List<String> validateFullConfiguration( HarmonizationConfig harmonizationConfig ){
        List<String> ret = new ArrayList<>();
        if(harmonizationConfig == null){
            ret.add("Empty Configuration");
        }
        else{
            if( StringUtils.isEmpty( harmonizationConfig.getParseConfigPath()) ){
                ret.add("parseConfigPath");
            }
            if( StringUtils.isEmpty( harmonizationConfig.getParseStreamName()) ){
                ret.add("parseStreamName");
            }
            if( StringUtils.isEmpty( harmonizationConfig.getFileParser()) ){
                ret.add("fileParser");
            }
            if( StringUtils.isEmpty( harmonizationConfig.getNormalizeRulePath()) ){
                ret.add("normalizeRulePath");
            }
            if( StringUtils.isEmpty( harmonizationConfig.getValidateRulePath()) ){
                ret.add("validateRulePath");
            }
            if( StringUtils.isEmpty( harmonizationConfig.getWriteConfigPath()) ){
                ret.add("writeConfigPath");
            }
            if( StringUtils.isEmpty( harmonizationConfig.getWriteConfigStream()) ){
                ret.add("writeConfigStream");
            }
        }
        return ret;
    }
}