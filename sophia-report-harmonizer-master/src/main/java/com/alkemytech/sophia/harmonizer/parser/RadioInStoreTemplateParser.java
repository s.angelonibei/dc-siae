package com.alkemytech.sophia.harmonizer.parser;

import com.alkemytech.sophia.harmonizer.model.HarmonizationConfig;
import com.alkemytech.sophia.harmonizer.model.UtilizationReport;
import com.amazonaws.services.s3.AmazonS3URI;
import com.amazonaws.util.IOUtils;
import com.google.inject.Singleton;
import com.ibm.icu.text.CharsetDetector;
import com.ibm.icu.text.CharsetMatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Alessandro Russo on 07/12/2017.
 */
@Singleton
public class RadioInStoreTemplateParser extends GenericParser {

    Logger logger = LoggerFactory.getLogger(getClass());

    public RadioInStoreTemplateParser() {
    }

    @Override
    public Map<String, Object> parse(Map<String, Object> parameters) {
        //Result map (it may contain RECORDS if OK or ERRORS if KO)
        Map<String, Object> ret = new HashMap<>();
        try{
            if(parameters.containsKey(ParserParameters.CONFIGURATION) && parameters.containsKey(ParserParameters.FILES)){
                HarmonizationConfig configuration = (HarmonizationConfig) parameters.get(ParserParameters.CONFIGURATION);
                Map<UtilizationReport,AmazonS3URI> fileMap = (Map<UtilizationReport, AmazonS3URI>) parameters.get(ParserParameters.FILES);
                if( configuration!= null && fileMap != null ){
                    if( configuration.getExpectedFiles() == null || fileMap.size() == 1 ){
                        for(UtilizationReport currentFile : fileMap.keySet()){
                            AmazonS3URI currentUri = fileMap.get(currentFile);
                            ByteArrayOutputStream bos = new ByteArrayOutputStream();
                            try{
                                logger.debug("default charset: {}", Charset.defaultCharset());
                                //download file from Amanzon S3
                                logger.debug("report to parse -> {}", currentFile.getFileName() );
                                InputStream is = s3Service.download(currentUri.getBucket(), currentUri.getKey());
                                byte[] data = IOUtils.toByteArray(is);
                                CharsetMatch match = new CharsetDetector().setText(data).detect();
                                String charsetName = match.getName();
                                logger.debug("detected charset: {}", charsetName);
                                String encodedData = new String(data , charsetName);
                                byte[] newBytes = encodedData.getBytes();
                                bos = new ByteArrayOutputStream(newBytes.length);
                                bos.write(newBytes, 0, newBytes.length);
                                List<Object> records =  mapper.parseFile(configuration.getParseConfigPath(), configuration.getParseStreamName(), bos, s3Service);
                                if(records != null && records.size() > 0){
                                    ret.put(PARSED_RECORDS, normalize(currentFile.getIdCustomer(), configuration.getNormalizeRulePath(), records));
                                    return ret;
                                }
                                else{
                                    ret.put(ERRORS, "Parsing result is empty!");
                                }
                            }
                            finally {
                                bos.flush();
                                bos.close();
                            }
                        }
                    }
                    else{
                        ret.put(ERRORS, String.format("Unexpected files number: Expected %s -> Found %s", configuration.getExpectedFiles(), fileMap.size() ) );
                    }
                }
                else{
                    ret.put(ERRORS, "Parser parameters empty!");
                }
            }
            else{
                ret.put(ERRORS, "Expected parser parameters not found!");
            }
        }
        catch (Exception e){
            ret.put(ERRORS, e);
        }
        return ret;
    }
}
