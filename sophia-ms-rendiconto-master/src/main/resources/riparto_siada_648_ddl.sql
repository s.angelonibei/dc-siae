CREATE EXTERNAL TABLE IF NOT EXISTS :table
(  `codice_provider` string COMMENT 'from deserializer',   `offerta_commerciale` string COMMENT 'from deserializer',   `tipo_utilizzo` string COMMENT 'from deserializer',
`territorio` string COMMENT 'from deserializer',   `mese_di_incasso_fattura` string COMMENT 'from deserializer',   `anno_di_rif_utilizzazione` string COMMENT 'from deserializer',   `tipo_di_per_di_riferimento` string COMMENT 'from deserializer',   `periodo_di_rif_utilizzazione` string COMMENT 'from deserializer',   `tipo_utilizzazione` string COMMENT 'from deserializer',   `tipo_repertorio` string COMMENT 'from deserializer',   `qta_utilizzazioni` string COMMENT 'from deserializer',   `codice_opera` string COMMENT 'from deserializer',
`flag_opera` string COMMENT 'from deserializer',
`tipo_codifica` string COMMENT 'from deserializer',
`qualifica_avente_diritto` string COMMENT 'from deserializer',
`pos_siae_avente_diritto` string COMMENT 'from deserializer',
`ipi_number_ad` string COMMENT 'from deserializer',   `numeratore_dem` string COMMENT 'from deserializer',
`denominatore_dem` string COMMENT 'from deserializer',   `codice_conto_dem` string COMMENT 'from deserializer',
`numero_conto_dem` string COMMENT 'from deserializer',   `compenso_dem_netta_ad` string COMMENT 'from deserializer',
`numeratore_quota_drm` string COMMENT 'from deserializer',   `denominatore_quota_drm` string COMMENT 'from deserializer',
`codice_conto_drm` string COMMENT 'from deserializer',   `numero_conto_drm` string COMMENT 'from deserializer',
`compenso_drm_netta_ad` string COMMENT 'from deserializer',
`percentuale_di_claim` string COMMENT 'from deserializer',
`titolo_utilizzazione` string COMMENT 'from deserializer',
`nominativo_utilizzazione` string COMMENT 'from deserializer',
`transaction_id` string COMMENT 'from deserializer',
`numeratore_quota_dem` string COMMENT 'from deserializer',
`denominatore_quota_dem` string COMMENT 'from deserializer')ROW FORMAT SERDE
'org.apache.hadoop.hive.serde2.OpenCSVSerde' WITH SERDEPROPERTIES (   'quoteChar'='"',   'separatorChar'='\;')
STORED AS INPUTFORMAT   'org.apache.hadoop.mapred.TextInputFormat' OUTPUTFORMAT
'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'LOCATION  :location