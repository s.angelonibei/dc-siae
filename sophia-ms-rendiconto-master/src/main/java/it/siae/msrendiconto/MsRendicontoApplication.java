/*
 * @Autor A.D.C.
 */

package it.siae.msrendiconto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;

import java.sql.SQLException;

@ComponentScan
@SpringBootApplication
@EnableAutoConfiguration
@EnableEurekaClient
public class MsRendicontoApplication {

    public static void main(String[] args)  {

        SpringApplication.run(MsRendicontoApplication.class, args);
    }

}
