/*
 * @Autor A.D.C.
 */

package it.siae.msrendiconto.Filter;

import lombok.Data;

import java.util.List;

@Data
//Create request rendiconto
public class CreateRequestRendiconto {
    private String posizioneSiae ;
    private List<String> idRipartizioneSiada;
    private String richiestaDa;
}
