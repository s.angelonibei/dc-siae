/*
 * @Autor A.D.C.
 */

package it.siae.msrendiconto.rest;

import it.siae.msrendiconto.Filter.CreateRequestRendiconto;
import it.siae.msrendiconto.dto.RequestList;
import it.siae.msrendiconto.entity.MmRichiesteRendiconto;
import it.siae.msrendiconto.repository.MmRichiesteRendicontoRepository;
import it.siae.msrendiconto.service.RendicontoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Description;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("rendiconto")
public class RendicontoResource {

    private final Logger logger = LoggerFactory.getLogger(RendicontoResource.class);

    @Autowired
    RendicontoService service;

    @Autowired
    MmRichiesteRendicontoRepository rendicontoRepo;

    @PostMapping("/addRequest")
    @Description("Api for create report csv request in MMRichiesteRendiconto")
    public List<MmRichiesteRendiconto> createReportRequest(@Valid @RequestBody CreateRequestRendiconto createRequestRendiconto) {
        logger.info("REST request to create new request into MmRichiesteRendiconto: {} ", createRequestRendiconto);
        return this.service.createReportRequest(createRequestRendiconto);
    }

    @GetMapping("/getRequestList")
    @Description("Api for get all request from MMRichiesteRendiconto orderBy DataCreation Desc")
    public RequestList getAllRequest() {
        logger.info("REST request to find all request into MmRichiesteRendiconto: {} ");
        RequestList r = new RequestList();
        r.setRequestsList(this.rendicontoRepo.findAll()
                .stream()
                .sorted(Comparator.comparing(MmRichiesteRendiconto::getDataCreazione)
                        .reversed())
                .collect(Collectors.toList()));
        return r;
    }

//    @PutMapping("/getAllRequestByfilter")
//    @Description("Api for get all request from MMRichiesteRendiconto")
//    public List<MmRichiesteRendiconto> getAllRequestByfilter(@Valid @RequestBody MmRichiesteRendicontoFilter filter) {
//        logger.info("REST request to find the list of MmRichiesteRendiconto with filter: {} ", filter);
//        return this.service.findAllByFilter(filter);
//    }
}
