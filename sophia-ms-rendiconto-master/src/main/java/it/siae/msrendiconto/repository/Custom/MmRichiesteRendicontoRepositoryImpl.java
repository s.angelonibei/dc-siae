/*
 * @Autor A.D.C.
 */

package it.siae.msrendiconto.repository.Custom;

import it.siae.msrendiconto.query.MmRichiesteRendicontoProperites;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class MmRichiesteRendicontoRepositoryImpl implements MmRichiesteRendicontoRepositoryCustom {
    private final Logger logger = LoggerFactory.getLogger(MmRichiesteRendicontoRepositoryImpl.class);

    @Autowired
    private MmRichiesteRendicontoProperites mmRichiesteRendicontoProperites;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

//    @Override
//    public List<MmRichiesteRendiconto> findRendicontiByFilters(MmRichiesteRendicontoFilter filter) {
//
//        logger.info("Request to findIdMerceAreaVendita : {}");
////        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
//        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
//        StringBuffer sb = new StringBuffer();
//        sb.append(mmRichiesteRendicontoProperites.getFilter().getSelect());
//        List<MmRichiesteRendiconto> list = new ArrayList<>();
//        if (filter.getIdRichiesta() != null)
//            sb.append(" ").append(mmRichiesteRendicontoProperites.getFilter().getIdRichiesta());
//            namedParameters.addValue("idRichiesta", filter.getIdRichiesta());
//        if (filter.getPosizioneSiae() != null)
//            sb.append(" ").append(mmRichiesteRendicontoProperites.getFilter().getPosizioneSiae());
//            namedParameters.addValue("posizioneSiae", filter.getPosizioneSiae());
//        if (filter.getListaRipartizione() != null)
//            sb.append(" ").append(mmRichiesteRendicontoProperites.getFilter().getListaRipartizione());
//            namedParameters.addValue("listaRipartizione", filter.getListaRipartizione());
//        if (filter.getStato() != null)
//            sb.append(" ").append(mmRichiesteRendicontoProperites.getFilter().getStato());
//            namedParameters.addValue("stato", filter.getStato());
//        if (filter.getValoreNettoDem() != null)
//            sb.append(" ").append(mmRichiesteRendicontoProperites.getFilter().getValoreNettoDem());
//            namedParameters.addValue("valoreNettoDem", filter.getValoreNettoDem());
//        if (filter.getValoreNettoDrm() != null)
//            sb.append(" ").append(mmRichiesteRendicontoProperites.getFilter().getValoreNettoDrm());
//            namedParameters.addValue("valoreNettoDrm", filter.getValoreNettoDrm());
//        if (filter.getDataCreazione() != null)
//            sb.append(" ").append(mmRichiesteRendicontoProperites.getFilter().getDataCreazione());
//            namedParameters.addValue("dataCreazione", filter.getDataCreazione());
//        if (filter.getUltimoAggiornamento() != null)
//            sb.append(" ").append(mmRichiesteRendicontoProperites.getFilter().getUltimoAggiornamento());
//            namedParameters.addValue("ultimoAggiornamento", filter.getUltimoAggiornamento());
//        if (filter.getRichiestaDa() != null)
//            sb.append(" ").append(mmRichiesteRendicontoProperites.getFilter().getRichiestaDa());
//            namedParameters.addValue("richiestaDa", filter.getRichiestaDa());
//        if (filter.getDescrizioneErrore() != null)
//            sb.append(" ").append(mmRichiesteRendicontoProperites.getFilter().getDescrizioneErrore());
//            namedParameters.addValue("descrizioneErrore", filter.getDescrizioneErrore());
//        if (filter.getPathS3Rendiconto() != null)
//            sb.append(" ").append(mmRichiesteRendicontoProperites.getFilter().getPathS3Rendiconto());
//            namedParameters.addValue("pathS3Rendiconto", filter.getPathS3Rendiconto());
//        try {
//
//            logger.info("findAllByFilter query {} - namedParameters {}", sb.toString(), namedParameters.getValues());
//            list
//                    = this.namedParameterJdbcTemplate.query(sb.toString(),
//                    namedParameters, (ResultSet rs, int rowNum) -> {
//                        MmRichiesteRendiconto dto = new MmRichiesteRendiconto();
//                        dto.setIdRichiesta(rs.getLong("ID_RICHIESTA"));
//                        dto.setPosizioneSiae(rs.getString("POSIZIONE_SIAE"));
//                        dto.setListaRipartizione(rs.getString("LISTA_RIPARTIZIONE"));
//                        dto.setStato(StatoRendicontoEnum.valueOf(rs.getString("STATO")));
//                        dto.setValoreNettoDem(rs.getBigDecimal("VALORE_NETTO_DEM"));
//                        dto.setValoreNettoDrm(rs.getBigDecimal("VALORE_NETTO_DRM"));
//                        dto.setRichiestaDa(rs.getString("RICHIESTA_DA"));
//                        dto.setDataCreazione(rs.getTimestamp("DATA_CREAZIONE"));
//                        dto.setUltimoAggiornamento(rs.getTimestamp("ULTIMO_AGGIORNAMENTO"));
//                        dto.setDescrizioneErrore(rs.getString("DESCRIZIONE_ERRORE"));
//                        dto.setPathS3Rendiconto("PATH_S3_RENDICONTO");
//                        return dto;
//                    });
//
//        } catch (Exception e) {
//            logger.info("searchAll, Exception " + e.getMessage());
//        }
//
//        return list;
//    }
}
