/*
 * @Autor A.D.C.
 */

/*
 * @Autor A.D.C.
 */

package it.siae.msrendiconto.repository;

import it.siae.msrendiconto.entity.AnagDsp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnagDspRepository extends JpaRepository<AnagDsp, String> {
        AnagDsp findByCode(String codiceProvider);
}