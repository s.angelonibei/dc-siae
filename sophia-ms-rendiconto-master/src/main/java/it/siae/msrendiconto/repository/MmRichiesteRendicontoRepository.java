/*
 * @Autor A.D.C.
 */

package it.siae.msrendiconto.repository;

import it.siae.msrendiconto.entity.MmRichiesteRendiconto;
import it.siae.msrendiconto.repository.Custom.MmRichiesteRendicontoRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MmRichiesteRendicontoRepository extends JpaRepository<MmRichiesteRendiconto, Long>, MmRichiesteRendicontoRepositoryCustom {
        List<MmRichiesteRendiconto> findByPosizioneSiaeAndListaRipartizioneAndStato(String posizioneSiae,String listaRipartizione,String stato);

        List<MmRichiesteRendiconto> findByStatoIgnoreCaseOrderByDataCreazioneAsc(String daElaborare);

        List<MmRichiesteRendiconto> findByPosizioneSiaeAndListaRipartizioneAndStatoNot(String posizioneSiae, String listaRipartizione, String stato);
}