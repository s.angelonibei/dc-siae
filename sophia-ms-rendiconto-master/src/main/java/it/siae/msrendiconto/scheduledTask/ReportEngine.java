/*
 * @Autor A.D.C.
 */

package it.siae.msrendiconto.scheduledTask;

import it.siae.msrendiconto.entity.MmRichiesteRendiconto;
import it.siae.msrendiconto.entity.enumeration.StatoRendicontoEnum;
import it.siae.msrendiconto.report.csv.WriteCsv;
import it.siae.msrendiconto.repository.MmRichiesteRendicontoRepository;
import it.siae.msrendiconto.service.CommonsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.RejectedExecutionException;

@Component
public class ReportEngine {

    private static final Logger logger = LoggerFactory.getLogger(ReportEngine.class);
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
    @Autowired
    MmRichiesteRendicontoRepository rendicontoRepo;

    @Autowired
    private CommonsService commonsService;

    @Autowired
    private Environment env;

    @Autowired
    private WriteCsv writeCsv;

    @Value("${spring.hive.clusterListRestUrlSiaeDev}")
    public String hiveUrl;

    @Autowired
    @Qualifier("CustomTaskExecutor")
    private TaskExecutor taskExecutor;


    // add scheduled methods here
    @Scheduled(cron = "0 * * * * ?")
    public void scheduleTaskWithCronExpression() {

        logger.info("Report Service is running");
        logger.info("Cron Task: Current Time - {}", formatter.format(LocalDateTime.now()));
        List<MmRichiesteRendiconto> reports = rendicontoRepo.findByStatoIgnoreCaseOrderByDataCreazioneAsc(StatoRendicontoEnum.DA_ELABORARE.getName());
        List<MmRichiesteRendiconto> reportsrunning = rendicontoRepo.findByStatoIgnoreCaseOrderByDataCreazioneAsc(StatoRendicontoEnum.IN_CORSO.getName());
        logger.info("Reports da Elaborare: " + reports.size());
        logger.info("Reports in corso: " + reportsrunning.size());
        for (MmRichiesteRendiconto report : reports) {
            RendicontoJob job = new RendicontoJob(report, commonsService, env, writeCsv);
            try {
                taskExecutor.execute(job);
            } catch (RejectedExecutionException r) {
                logger.info("Impossibile prendere in carico il report: tutti gli slot disponibili sono occupati");
            }
        }
        logger.info("Report Engine is running");
    }

}