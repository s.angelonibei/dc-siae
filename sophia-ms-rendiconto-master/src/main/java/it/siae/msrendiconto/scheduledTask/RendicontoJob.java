/*
 * @Autor A.D.C.
 */

package it.siae.msrendiconto.scheduledTask;

import it.siae.msrendiconto.entity.MmRichiesteRendiconto;
import it.siae.msrendiconto.entity.enumeration.StatoRendicontoEnum;
import it.siae.msrendiconto.report.csv.WriteCsv;
import it.siae.msrendiconto.service.CommonsService;
import lombok.Data;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

import java.io.File;
import java.text.SimpleDateFormat;


@Data
public class RendicontoJob implements Runnable {

    private final Logger logger = LoggerFactory.getLogger(RendicontoJob.class);

    private MmRichiesteRendiconto report;

    private CommonsService commonsService;

    private WriteCsv writeCsv;

    private Environment env;


    public RendicontoJob(MmRichiesteRendiconto report, CommonsService commonsService, Environment env, WriteCsv writeCsv) {
        this.report = report;
        this.commonsService = commonsService;
        this.env = env;
        this.writeCsv = writeCsv;
    }

    @Override
    public void run() {

        try {
            logger.info("STARTING RendicontoService for report {}", report.getIdRichiesta());

            report.setStato(StatoRendicontoEnum.IN_CORSO.getName());
            report.setPathS3Rendiconto("");
            commonsService.updateReportStatus(report);

            logger.info("Load data from Hive for report {}", +report.getIdRichiesta());

            // temporary output file
            final File file = new File("rendiconto_" + report.getPosizioneSiae() + "_"
                    + new SimpleDateFormat("yyyyMMddHHmmss").format(report.getDataCreazione()) + ".csv");
            logger.info("processCsvFromHive of report Id  " + report.getIdRichiesta() + " : created temporary file {}", file);

            // create csv file from hive query
            logger.info("processCsvFromHive of report Id  " + report.getIdRichiesta() + " : creating CSV file");
            boolean created = false;
            boolean response = false;
            String path = "C:";
            created = writeCsv.createCsvFile(path, file, report);
            logger.info("processCsvFromHive of report Id  " + report.getIdRichiesta() + " : CSV file size {}", file.length());
            if (created) {

                // upload file to S3
                if (file.length() > 0L) {
                    logger.info("processCsvFromHive of report Id  " + report.getIdRichiesta() + " : uploading to S3");
                    writeCsv.uploadCsvFile(report, file);
                    response = true;
                    // add partition to hive table
//                        logger.info("processCsvFromHive of report Id  "+report.getIdRichiesta()+" : creating hive partition");
//                        writeCsv.addHivePartition(path);
                    logger.info("report Id  " + report.getIdRichiesta() + "COMPLETE");
                }

            }

            // delete temporary output file
            file.delete();
            logger.info("processCsvFromHive of report Id  " + report.getIdRichiesta() + " : deleted temporary file {}", file);

            logger.info("Update report {} status", report.getIdRichiesta());

        } catch (Exception e) {
            report.setStato(StatoRendicontoEnum.ERRORE.getName());
            report.setDescrizioneErrore(ExceptionUtils.getStackTrace(e));
            report.setPathS3Rendiconto("");
            logger.error("Error occurred!", e);
            commonsService.updateReportStatus(report);
        }

    }

}

