/*
 * @Autor A.D.C.
 */

package it.siae.msrendiconto.report.csv;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.event.ProgressEvent;
import com.amazonaws.event.ProgressEventType;
import com.amazonaws.event.ProgressListener;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import it.siae.msrendiconto.aws.S3;
import it.siae.msrendiconto.client.RestClient;
import it.siae.msrendiconto.entity.MmRichiesteRendiconto;
import it.siae.msrendiconto.entity.enumeration.StatoRendicontoEnum;
import it.siae.msrendiconto.repository.AnagDspRepository;
import it.siae.msrendiconto.scheduledTask.HiveTableCreator;
import it.siae.msrendiconto.service.CommonsService;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.QuoteMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

@Service
@Transactional
@EnableConfigurationProperties
public class WriteCsvImpl implements WriteCsv {

    private Logger logger = LoggerFactory.getLogger(WriteCsvImpl.class);

    @Autowired
    private Environment env;

    @Autowired
    private CommonsService commonsService;

    //FIX TIMEOUT
    @Autowired
    private HiveTableCreator hiveTableCreator;

    @Value("${hive.drs648.select}")
    public String hiveSelect;

    @Value("${spring.hive.clusterListRestUrlSiaeDev}")
    public String hiveUrl;

    @Value("${spring.s3uri.path}")
    public String path;

    @Autowired
    private AnagDspRepository anagDspRepo;

    @Value("${s3.retry.put_object}")
    private String put_object;

    @Value("${spring.cloud.aws.credentials.accessKey}")
    private String accessKey;

    @Value("${spring.cloud.aws.credentials.secretKey}")
    private String secretKey;

    @Value("${spring.cloud.aws.region.static}")
    private String region;

    @Value("${hive.engine}")
    private String engine ;

    public boolean createCsvFile(String path, File file, MmRichiesteRendiconto report) throws Exception {

        List<String> htablenames = new ArrayList<>();
        final RestClient.Result result = RestClient
                .get(hiveUrl, report);
        if (200 == result.getStatusCode()) {
            final JsonObject resultJson = result.getJsonElement().getAsJsonObject();
            logger.info("createCsvFile: resultJson {}", resultJson);
            if ("OK".equals(resultJson.get("status").getAsString())) {
                final JsonArray activeEmrClusters = resultJson.getAsJsonArray("activeEmrClusters");
                if (null != activeEmrClusters) for (JsonElement activeEmrCluster : activeEmrClusters) {
                    final JsonObject clusterJson = (JsonObject) activeEmrCluster;
                    logger.info("createCsvFile: clusterJson {}", clusterJson);
                    String hiveJdbcUrl = clusterJson.get("hiveJdbcUrl").getAsString().concat("?hive.execution.engine="+engine); //FIX TIMEOUT add engine for union select
                    String hiveJdbcUser = clusterJson.get("hiveJdbcUser").getAsString();
                    String hiveJdbcPassword = clusterJson.get("hiveJdbcPassword").getAsString();
                    hiveJdbcUrl = env.getProperty("hive.jdbc.url", hiveJdbcUrl);
                    hiveJdbcUser = env.getProperty("hive.jdbc.user", hiveJdbcUser);
                    hiveJdbcPassword = env.getProperty("hive.jdbc.password", hiveJdbcPassword);
                    logger.info("createCsvFile: connecting to {}...", hiveJdbcUrl);
                    DriverManager.setLoginTimeout(0);
                    try (final Connection connection = DriverManager
                            .getConnection(hiveJdbcUrl, hiveJdbcUser, hiveJdbcPassword)) {
                        logger.info("createCsvFile: connected to {}", hiveJdbcUrl);
                        try (final Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                                ResultSet.CONCUR_READ_ONLY)) {
                            //Build qry that give me data to export in CSV

                            // FIX TIMEOUT CREATE TABLES
                            String[] listaRipartizione = report.getListaRipartizione().split("-");
                            // Dallo split degli id ripartizione costruisco le tabelle hive che puntano ai percorsi degli id selezionati
                            String hql = "";
                            if (listaRipartizione.length > 0) {
                                String htablename = null;
                                for (String idr : listaRipartizione) {
                                    logger.info("Create hive table for ID Ripartizione : " + idr);
                                    //Mi salvo i nomi delle tabelle create per ogni id ripartizione per effettuare la DROP TABLE al termine delle operazioni.
                                    htablename = hiveTableCreator.createHiveTable(idr, report.getIdRichiesta().toString());
                                    htablenames.add(htablename);
                                }
                                logger.info("HIVE TABLES CREATED OK ...");
                                logger.info("Building hive query");
                                List<String> hiveSelectslist = new ArrayList<>();
                                // Con i nomi delle tabelle costruite proseguo alla costruzione delle query eventuali da mettere in UNION ALL
                                for (String table : htablenames) {
                                    hiveSelectslist.add(hiveSelect
                                            .replace(":table", table).replace(":posAd", report.getPosizioneSiae())
                                            .replace("id_ripartizione,", report.getListaRipartizione() + " as id_ripartizione,"));
                                }
                                for (String select : hiveSelectslist) {
                                    if (hql.equalsIgnoreCase("")) {
                                        hql = select;
                                    } else {
                                        hql = hql.concat(" UNION ALL ").concat(select);
                                    }
                                }
                            }
                            logger.info("createCsvFile of report Id  " + report.getIdRichiesta() + " : executing hql {}", hql);
                            try (ResultSet resultSet = statement.executeQuery(hql)) {
                                if (resultSet != null && resultSet.next() == true) {
                                    resultSet.beforeFirst();
                                    saveCsvFile(resultSet, path, file, report);
                                } else {
                                    report.setStato(StatoRendicontoEnum.COMPLETATA.getName());
                                    report.setValoreNettoDem(new BigDecimal(0));
                                    report.setValoreNettoDrm(new BigDecimal(0));
                                    report.setDescrizioneErrore("Non sono stati trovati record con i filtri impostati.");
                                    report.setPathS3Rendiconto("");
                                    commonsService.updateReportStatus(report);
                                    logger.info("Non sono stati trovati record con i filtri impostati. Report Id " + report.getIdRichiesta() + "");
                                }
                            }
                            return true;
                        }
                    } catch (Exception e) {
                        report.setStato(StatoRendicontoEnum.ERRORE.getName());
                        report.setDescrizioneErrore(e.getMessage());
                        report.setPathS3Rendiconto("");
                        commonsService.updateReportStatus(report);
                        logger.error("createCsvFile report Id  " + report.getIdRichiesta() + "", e);
                    } finally {
                        // In ogni caso al termine delle operazioni , proseguo alla DROP delle tabelle create
                        for (String t : htablenames) {
                            logger.info("DROP TABLE " + t);
                            final Connection connection = DriverManager
                                    .getConnection(hiveJdbcUrl, hiveJdbcUser, hiveJdbcPassword);
                            final Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                                    ResultSet.CONCUR_READ_ONLY);
                            statement.executeUpdate("DROP TABLE IF EXISTS " + t);
                        }
                    }
                }
            }
        }
        return false;
    }


    public boolean uploadCsvFile(MmRichiesteRendiconto report, File file) throws Exception {
        final S3.Url s3Uri = new S3.Url(path, report);
        logger.info("uploadCsvFile for report Id " + report.getIdRichiesta() + " s3Uri: {}", s3Uri);
        if (this.upload(s3Uri, file, report)) {
            logger.info("uploadCsvFile for report Id " + report.getIdRichiesta() + " s3Uri {}", s3Uri);
            System.out.println(s3Uri);
            report.setUltimoAggiornamento(new Date());
            report.setPathS3Rendiconto(path + file.getName());
            report.setDescrizioneErrore("");
            report.setStato(StatoRendicontoEnum.COMPLETATA.getName());
            commonsService.updateReportStatus(report);
            return true;
        }
        report.setStato(StatoRendicontoEnum.ERRORE.getName());
        report.setPathS3Rendiconto("");
        report.setDescrizioneErrore("Upload failed!");
        commonsService.updateReportStatus(report);
        return false;
    }


    public boolean upload(S3.Url url, final File file, MmRichiesteRendiconto report) {
        try {
            for (int retries = Integer.parseInt(put_object);
                 retries > 0; retries--) {
                try {
                    logger.info("uploading file {} for " + "report " + report.getIdRichiesta() + " to {}", file.getName(), url);
                    final PutObjectRequest putObjectRequest = new PutObjectRequest(url.bucket,
                            url.key.replace("//", "/"), file);
                    putObjectRequest.setGeneralProgressListener(new ProgressListener() {

                        private long accumulator = 0L;
                        private long total = 0L;
                        private long chunk = Math.max(256 * 1024, file.length() / 10L);

                        @Override
                        public void progressChanged(ProgressEvent event) {
                            if (ProgressEventType.REQUEST_BYTE_TRANSFER_EVENT
                                    .equals(event.getEventType())) {
                                accumulator += event.getBytes();
                                total += event.getBytes();
                                if (accumulator >= chunk) {
                                    accumulator -= chunk;
                                    logger.info("transferred {}Kb/{}Kb of file {}",
                                            total / 1024L, file.length() / 1024L, file.getName());
                                }
                            } else if (ProgressEventType.TRANSFER_COMPLETED_EVENT
                                    .equals(event.getEventType())) {
                                logger.info("transfer of file for report Id " + report.getIdRichiesta() + " completed", file.getName());
                            }
                        }

                    });
                    AWSCredentials credentials = new BasicAWSCredentials(
                            accessKey,
                            secretKey
                    );
                    AmazonS3 s3client = AmazonS3ClientBuilder
                            .standard()
                            .withCredentials(new AWSStaticCredentialsProvider(credentials))
                            .withRegion(region)
                            .build();
                    if (!s3client.doesBucketExist(url.bucket)) {
                        s3client.createBucket(url.bucket);
                    }
                    s3client.putObject(url.bucket, url.key + file.getName(), file);
                    logger.info("file {} successfully uploaded for " + "report " + report.getIdRichiesta() + " to {}", file.getName(), url);
                    return true;
                } catch (Exception e) {
                    report.setStato(StatoRendicontoEnum.ERRORE.getName());
                    report.setDescrizioneErrore(e.getMessage());
                    report.setPathS3Rendiconto("");
                    commonsService.updateReportStatus(report);
                    logger.error("upload report Id  " + report.getIdRichiesta() + "", e);
                }
            }
        } catch (NumberFormatException | NullPointerException e) {
            report.setStato(StatoRendicontoEnum.ERRORE.getName());
            report.setDescrizioneErrore(e.getMessage());
            report.setPathS3Rendiconto("");
            commonsService.updateReportStatus(report);
            logger.error("upload report Id  " + report.getIdRichiesta() + "", e);
        }
        logger.info("upload: upload failed, report Id  " + report.getIdRichiesta() + "", file.getName());

        return false;
    }

    private void saveCsvFile(ResultSet resultSet, String path, File file, MmRichiesteRendiconto report) throws Exception {
        try (final FileWriter writer = new FileWriter(file, false)) {
            try (final CSVPrinter printer = new CSVPrinter(new BufferedWriter(writer),
                    CSVFormat.EXCEL.withDelimiter(';').withQuoteMode(QuoteMode.MINIMAL).withHeader(
                            "Posizione SIAE",
                            "ID Ripartizione",
                            "DSP",
                            "Territorio",
                            "Offerta commerciale",
                            "Anno di riferimento utilizzazione",
                            "Tipo di periodo di riferimento",
                            "Periodo di riferimento utilizzazione",
                            "Tipo utilizzo",
                            "Numero utilizzazioni",
                            "Codice opera",
                            "Qualifica Avente Diritto",
                            "Posizione Siae Avente Diritto",
                            "IPI number AD",
                            "Numeratore DEM",
                            "Denominatore DEM",
                            "Codice conto DEM",
                            "Numero conto DEM",
                            "Compenso DEM netta Avente Diritto",
                            "Numeratore quota DRM",
                            "Denominatore quota DRM",
                            "Codice conto DRM",
                            "Numero conto DRM",
                            "Compenso DRM netta Avente Diritto",
                            "Titolo utilizzazione",
                            "Nominativo utilizzazione",
                            "Transaction_ID",
                            "Numeratore quota DEM",
                            "Denominatore quota DEM"
                    ))) {
                final int cols = resultSet.getMetaData().getColumnCount();
                logger.info("Numero cols :" + cols);

                final BigDecimal siadaScale = new BigDecimal(10000);
                final int idxCompensoDemNetto = Integer.parseInt(Objects.requireNonNull(env.getProperty("hive.column.compenso dem netta avente diritto")));
                final int idxCompensoDrmNetto = Integer.parseInt(Objects.requireNonNull(env.getProperty("hive.column.compenso drm netta avente diritto")));
                final int idxNumeratoreQuotaDrm = Integer.parseInt(Objects.requireNonNull(env.getProperty("hive.column.numeratore quota drm")));
                final int idxDenominatoreQuotaDrm = Integer.parseInt(Objects.requireNonNull(env.getProperty("hive.column.denominatore quota drm")));
                final int idxCodiceProvider = Integer.parseInt(Objects.requireNonNull(env.getProperty("hive.column.dsp")));
                logger.info("saveCsvFile of " + "report " + report.getIdRichiesta() + " : output columns number: {}", cols);
                int rows = 0;
                BigDecimal sumDem = new BigDecimal(0);
                BigDecimal sumDrm = new BigDecimal(0);
                while (resultSet.next()) {
                    final String[] values = new String[cols];
                    for (int i = 0; i < cols; i++) {
                        values[i] = i != 1 ? resultSet.getString(i + 1) : report.getListaRipartizione();
                    }
                    //Decode Dsp Name
                    values[idxCodiceProvider] = anagDspRepo.findByCode(values[idxCodiceProvider]) != null ? anagDspRepo.findByCode(values[idxCodiceProvider]).getName() : values[idxCodiceProvider];
                    // compensi netti
                    String v = values[idxCompensoDemNetto];
                    values[idxCompensoDemNetto] = fixNumber(values[idxCompensoDemNetto]);
                    values[idxCompensoDrmNetto] = fixNumber(values[idxCompensoDrmNetto]);
                    //increase sum of values Dem and Drm
                    BigDecimal s = new BigDecimal(values[idxCompensoDemNetto].replace(",", "."));
                    sumDem = sumDem.add(new BigDecimal(values[idxCompensoDemNetto].replace(",", ".")));
                    sumDrm = sumDrm.add(new BigDecimal(values[idxCompensoDrmNetto].replace(",", ".")));
                    // numeratore denominatore drm
                    final BigDecimal numeratoreQuotaDrm = toBigDecimal(values[idxNumeratoreQuotaDrm]);
                    final BigDecimal denominatoreQuotaDrm = toBigDecimal(values[idxDenominatoreQuotaDrm]);
                    final BigDecimal percentualeQuotaDrm = (denominatoreQuotaDrm != null && denominatoreQuotaDrm.compareTo(new BigDecimal(0.00)) != 0) ? numeratoreQuotaDrm
                            .divide(denominatoreQuotaDrm, 20, RoundingMode.HALF_UP) : new BigDecimal(0);

                    //FIX IN:2611
                    //values[idxNumeratoreQuotaDrm] = fixNumber(percentualeQuotaDrm.multiply(siadaScale));
                    //values[idxDenominatoreQuotaDrm] = fixNumber(siadaScale);

                    // print columns
                    for (int i = 0; i < cols; i++) {
                        printer.print(values[i]);
                    }
                    printer.println();
                    rows++;
                    if (0 == (rows % 1000)) {
                        logger.info("saveCsvFile of " + "report " + report.getIdRichiesta() + " : rows {}", rows);
                    }
                }
                logger.info("Increase Dem and Drm completed : Id Report : " + report.getIdRichiesta() + " DEM{" + sumDem + "} , DRM{" + sumDrm + "} ");
                report.setValoreNettoDem(sumDem);
                report.setValoreNettoDrm(sumDrm);
                // footer with number of rows
                if (rows > 0) {
                    printer.print(rows);
                    printer.println();
                    logger.info("saveCsvFile of " + "report " + report.getIdRichiesta() + " : rows {}", rows);
                }
            } catch (Exception e) {
                report.setStato(StatoRendicontoEnum.ERRORE.getName());
                report.setDescrizioneErrore(e.getMessage());
                report.setPathS3Rendiconto("");
                commonsService.updateReportStatus(report);
                logger.error("createCsvFile of " + "report Id  " + report.getIdRichiesta() + "", e);
            }
        }
    }

//    public boolean addHivePartition(String path) throws IOException {
//        final RestClient.Result result = RestClient
//                .get(env.getProperty("hive.clusterListRestUrl"));
//        if (200 == result.getStatusCode()) {
//            final JsonObject resultJson = result.getJsonElement().getAsJsonObject();
//            logger.info("addHivePartition: resultJson {}", resultJson);
//            if ("OK".equals(resultJson.get("status").getAsString())) {
//                final JsonArray activeEmrClusters = resultJson.getAsJsonArray("activeEmrClusters");
//                if (null != activeEmrClusters) for (JsonElement activeEmrCluster : activeEmrClusters) {
//                    final JsonObject clusterJson = (JsonObject) activeEmrCluster;
//                    logger.info("addHivePartition: clusterJson {}", clusterJson);
//                    final String hiveJdbcUrl = clusterJson.get("hiveJdbcUrl").getAsString();
//                    final String hiveJdbcUser = clusterJson.get("hiveJdbcUser").getAsString();
//                    final String hiveJdbcPassword = clusterJson.get("hiveJdbcPassword").getAsString();
//                    logger.info("addHivePartition: connecting to {}...", hiveJdbcUrl);
//                    try (final Connection connection = DriverManager
//                            .getConnection(hiveJdbcUrl, hiveJdbcUser, hiveJdbcPassword)) {
//                        logger.info("addHivePartition: connected to {}", hiveJdbcUrl);
//                        try (final Statement statement = connection.createStatement()) {
//                            final String hql = env.getProperty("hive.alter_table")
//                                    .replace(":dsp", metadata.getFtpSourcePath())
//                                    .replace(":year", metadata.getYear())
//                                    .replace(":month", metadata.getMonth())
//                                    .replace(":dsr", dsrAmount.getIdDsr());
//                            logger.info("addHivePartition: executing hql {}", hql);
//                            final int count = statement.executeUpdate(hql);
//                            logger.info("addHivePartition: hql result {}", count);
//                            return count >= 0;
//                        }
//                    } catch (Exception e) {
//                        logger.error("addHivePartition", e);
//                    }
//                }
//            }
//        }
//        return false;
//    }


    //Private Methods
    private BigDecimal toBigDecimal(String value) {
        try {
            return (null == value) ? null : new BigDecimal(value.replace(',', '.'));
        } catch (NumberFormatException e) {
            logger.info(String.valueOf(e));
            return BigDecimal.ZERO;
        }
    }

    private String fixNumber(String value) {
        try {
            return fixNumber(toBigDecimal(value));
        } catch (NumberFormatException e) {
        }
        return value;
    }

    private String fixNumber(BigDecimal number) {
        if (null == number) {
            return null;
        }
        String value = number.toPlainString();
        value = value.replace('.', ',');
        int idx = value.indexOf(",");
        if (-1 != idx) {
            while (value.endsWith("0")) {
                value = value.substring(0, value.length() - 1);
            }
        }
        if (value.endsWith(",")) {
            value = value.substring(0, value.length() - 1);
        }
        idx = value.indexOf(",0000000000");
        if (-1 != idx) {
            value = value.substring(0, idx);
        }
        return value;
    }
}
