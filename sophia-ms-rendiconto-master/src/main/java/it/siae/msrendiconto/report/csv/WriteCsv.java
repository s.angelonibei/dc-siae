/*
 * @Autor A.D.C.
 */

package it.siae.msrendiconto.report.csv;

import it.siae.msrendiconto.entity.MmRichiesteRendiconto;

import java.io.File;

public interface WriteCsv {
    boolean createCsvFile(String path, File file, MmRichiesteRendiconto report) throws Exception;

    boolean uploadCsvFile(MmRichiesteRendiconto report, File file) throws Exception;

//    boolean addHivePartition(String path) throws IOException;
}
