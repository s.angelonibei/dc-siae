/*
 * @Autor A.D.C.
 */

/*
 * @Autor A.D.C.
 */

package it.siae.msrendiconto.dto;

import it.siae.msrendiconto.entity.MmRichiesteRendiconto;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class RequestList implements Serializable {

    private List<MmRichiesteRendiconto> requestsList;
}
