/*
 * @Autor A.D.C.
 */

package it.siae.msrendiconto.service.impl;


import it.siae.msrendiconto.entity.MmRichiesteRendiconto;
import it.siae.msrendiconto.repository.MmRichiesteRendicontoRepository;
import it.siae.msrendiconto.service.CommonsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;


@Service
@Transactional
public class CommonsServiceImpl implements CommonsService {

    private final Logger logger = LoggerFactory.getLogger(CommonsServiceImpl.class);

    @Autowired
    private MmRichiesteRendicontoRepository rendicontoRepo;

    @Override
    public void updateReportStatus(MmRichiesteRendiconto report) {
        report.setUltimoAggiornamento(new Date());
        rendicontoRepo.save(report);
        rendicontoRepo.flush();
    }

}
