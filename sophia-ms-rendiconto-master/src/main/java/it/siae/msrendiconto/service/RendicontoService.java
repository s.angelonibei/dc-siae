/*
 * @Autor A.D.C.
 */

package it.siae.msrendiconto.service;

import it.siae.msrendiconto.Filter.CreateRequestRendiconto;
import it.siae.msrendiconto.entity.MmRichiesteRendiconto;

import java.util.List;

public interface RendicontoService {
    List<MmRichiesteRendiconto> createReportRequest(CreateRequestRendiconto createRequestRendiconto);

//    List<MmRichiesteRendiconto> findAllByFilter(MmRichiesteRendicontoFilter filter);
}
