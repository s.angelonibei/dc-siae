/*
 * @Autor A.D.C.
 */

package it.siae.msrendiconto.service.impl;

import it.siae.msrendiconto.Filter.CreateRequestRendiconto;
import it.siae.msrendiconto.entity.MmRichiesteRendiconto;
import it.siae.msrendiconto.entity.enumeration.StatoRendicontoEnum;
import it.siae.msrendiconto.errors.BadRequestAlertException;
import it.siae.msrendiconto.repository.MmRichiesteRendicontoRepository;
import it.siae.msrendiconto.service.RendicontoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class RendicontoServiceImpl implements RendicontoService {

    @Autowired
    MmRichiesteRendicontoRepository rendicontoRepo;

    Logger logger = LoggerFactory.getLogger(RendicontoServiceImpl.class);

    @Override
    public List<MmRichiesteRendiconto> createReportRequest(CreateRequestRendiconto createRequestRendiconto) {
        MmRichiesteRendiconto r = new MmRichiesteRendiconto();
        String idsRipSiada = "";
        int i = 0;
        List<String> ids = createRequestRendiconto.getIdRipartizioneSiada().stream().sorted((o1, o2) -> o1.compareTo(o2)).collect(Collectors.toList());

        for (String s : ids) {
            idsRipSiada = idsRipSiada.concat(s);
            i++;
            if (i < createRequestRendiconto.getIdRipartizioneSiada().size()) {
                idsRipSiada = idsRipSiada.concat("-");
            }
        }
        r.setDataCreazione(new Date());
        r.setListaRipartizione(idsRipSiada);
        r.setPosizioneSiae(createRequestRendiconto.getPosizioneSiae());
        r.setStato(StatoRendicontoEnum.DA_ELABORARE.getName());
        r.setUltimoAggiornamento(new Date());
        r.setRichiestaDa(createRequestRendiconto.getRichiestaDa());
        List<MmRichiesteRendiconto> l = rendicontoRepo.findByPosizioneSiaeAndListaRipartizioneAndStatoNot(r.getPosizioneSiae(), r.getListaRipartizione(), StatoRendicontoEnum.ERRORE.getName());
        if (l != null && !(l.size() > 0)) {
            rendicontoRepo.save(r);
        } else {
            logger.info("ERROR !! Duplicate key , insert into MM_RICHIESTE_RENDICONTO FAILED");
            throw new BadRequestAlertException("E' stata già trovata una richiesta con i parametri inseriti", "MmRichiesteRendiconto.java", "errorDuplicateKey");
        }
        return rendicontoRepo.findByPosizioneSiaeAndListaRipartizioneAndStato(r.getPosizioneSiae(), r.getListaRipartizione(),r.getStato());
    }

//    @Override
//    public List<MmRichiesteRendiconto> findAllByFilter(MmRichiesteRendicontoFilter filter) {
//        return rendicontoRepo.findRendicontiByFilters(filter);
//    }
}
