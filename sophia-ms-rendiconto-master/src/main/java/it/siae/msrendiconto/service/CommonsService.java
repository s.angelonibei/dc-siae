/*
 * @Autor A.D.C.
 */

package it.siae.msrendiconto.service;


import it.siae.msrendiconto.entity.MmRichiesteRendiconto;

public interface CommonsService {
    void updateReportStatus(MmRichiesteRendiconto report);
}
