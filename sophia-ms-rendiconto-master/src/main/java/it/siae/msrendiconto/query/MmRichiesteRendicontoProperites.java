/*
 * @Autor A.D.C.
 */

package it.siae.msrendiconto.query;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:queries/rendiconto.properties")
@ConfigurationProperties(prefix = "rendiconto", ignoreUnknownFields = false)
@Data
public class MmRichiesteRendicontoProperites {
    private RendicontoFilter filter;
}
