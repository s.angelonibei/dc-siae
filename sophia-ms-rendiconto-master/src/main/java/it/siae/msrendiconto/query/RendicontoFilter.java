/*
 * @Autor A.D.C.
 */

package it.siae.msrendiconto.query;

import lombok.Data;

@Data
public class RendicontoFilter extends AbstractQuery {
    private String idRichiesta;
    private String posizioneSiae;
    private String listaRipartizione;
    private String stato;
    private String valoreNettoDem;
    private String valoreNettoDrm;
    private String dataCreazione;
    private String ultimoAggiornamento;
    private String richiestaDa;
    private String descrizioneErrore;
    private String pathS3Rendiconto;
}
