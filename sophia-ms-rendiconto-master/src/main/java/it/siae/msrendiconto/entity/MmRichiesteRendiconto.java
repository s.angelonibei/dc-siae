/*
 * @Autor A.D.C.
 */

package it.siae.msrendiconto.entity;

import it.siae.msrendiconto.entity.enumeration.StatoRendicontoEnum;
import lombok.Data;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Table(name ="MM_RICHIESTE_RENDICONTO",schema = "MCMDB_debug")
@Entity
@XmlRootElement
public class MmRichiesteRendiconto implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(insertable = false, name = "ID_RICHIESTA", nullable = false)
    private Long idRichiesta;

    @Column(name = "POSIZIONE_SIAE")
    private String posizioneSiae;

    @Column(name = "LISTA_RIPARTIZIONE")
    private String listaRipartizione;

    @Column(name = "STATO")
    private String stato;

    @Column(name = "VALORE_NETTO_DEM")
    private BigDecimal valoreNettoDem;

    @Column(name = "VALORE_NETTO_DRM")
    private BigDecimal valoreNettoDrm;

    @Column(name = "DATA_CREAZIONE")
    private Date dataCreazione;

    @Column(name = "ULTIMO_AGGIORNAMENTO")
    private Date ultimoAggiornamento;

    @Column(name = "RICHIESTA_DA")
    private String richiestaDa;

    @Column(name = "DESCRIZIONE_ERRORE")
    private String descrizioneErrore;

    @Column(name = "PATH_S3_RENDICONTO")
    private String pathS3Rendiconto;

    public String getDescrizioneErrore() {
        return descrizioneErrore;
    }

    public void setDescrizioneErrore(String descrizioneErrore) {
        if(descrizioneErrore.length()>400){
            descrizioneErrore = descrizioneErrore.substring(0,400);
        }
        this.descrizioneErrore = descrizioneErrore;
    }
}