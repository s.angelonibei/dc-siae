/*
 * @Autor A.D.C.
 */

package it.siae.msrendiconto.entity;

import com.google.gson.GsonBuilder;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class DsrAmount {

	private String idDsr;
	private BigDecimal amountDem;
	private BigDecimal amountDrm;
	private String billDate;
	private String invoiceCode;
	private String outputPath;

	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}

	public String getInvoiceCode() {
		return invoiceCode;
	}
}
