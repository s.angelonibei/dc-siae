/*
 * @Autor A.D.C.
 */

package it.siae.msrendiconto.entity;

import com.google.gson.GsonBuilder;
import lombok.Data;

@Data
public class DebugRipartoSiada648 {

	private String  input__file__name;
	private String  codice_provider;
	private String  offerta_commerciale;
	private String  tipo_utilizzo;
	private String  territorio;
	private String  mese_di_incasso_fattura;
	private String  anno_di_rif_utilizzazione;
	private String  tipo_di_per_di_riferimento;
	private String  periodo_di_rif_utilizzazione;
	private String  tipo_utilizzazione;
	private String  tipo_repertorio;
	private String  qta_utilizzazioni;
	private String  codice_opera;
	private String  flag_opera;
	private String  tipo_codifica;
	private String  qualifica_ad;
	private String  pos_siae_ad;
	private String  ipi_number_ad;
	private String  numeratore_quota_dem;
	private String  denominatore_quota_dem;
	private String  codice_conto_dem;
	private String  numero_conto_dem;
	private String  compenso_dem_netta_ad;
	private String  numeratore_quota_drm;
	private String  denominatore_quota_drm;
	private String  codice_conto_drm;
	private String  numero_conto_drm;
	private String  compenso_drm_netta_ad;
	private String  percentuale_di_claim;
	private String  titolo_utilizzazione;
	private String  nominativo_utilizzazione;

	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}

}
