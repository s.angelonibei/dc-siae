/*
 * @Autor A.D.C.
 */

package it.siae.msrendiconto.entity.enumeration;

public enum StatoRendicontoEnum {
    DA_ELABORARE("DA ELABORARE"),
    IN_CORSO("IN CORSO"),
    COMPLETATA("COMPLETATA"),
    ERRORE("ERRORE");

    private String name;

    StatoRendicontoEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
