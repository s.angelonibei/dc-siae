#!/bin/sh

JENV=debug
JENV_DTP=test

#JHOME=/home/sualkemy/sophia-schemi-riparto
JHOME=/home/roberto.cerfogli/schemi-riparto/$JENV_DTP
JNAME=interactive-search

JPACKAGE=com.alkemytech.sophia.royalties
JCLASS=InteractiveSearch
JCONFIG=$JNAME.properties
JOPTIONS="-Xmx4G -Ddefault.home_folder=$JHOME -Ddefault.environment=$JENV -Ddefault.environment_dtp=$JENV_DTP"
JJAR=sophia-schemi-riparto-1.0.jar

#cd $JHOME

java -cp "$JHOME/$JJAR:$JHOME/lib/*" $JOPTIONS $JPACKAGE.$JCLASS $JHOME/$JCONFIG


