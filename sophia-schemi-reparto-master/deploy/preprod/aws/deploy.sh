#!/bin/sh

sudo cp /home/roberto.cerfogli/sophia-schemi-riparto/codici-nosql* /home/orchestrator/sophia/schemi-riparto/
sudo cp /home/roberto.cerfogli/sophia-schemi-riparto/interactive-search* /home/orchestrator/sophia/schemi-riparto/
sudo cp /home/roberto.cerfogli/sophia-schemi-riparto/ucmr-ada-nosql* /home/orchestrator/sophia/schemi-riparto/
sudo cp /home/roberto.cerfogli/sophia-schemi-riparto/*.jar /home/orchestrator/sophia/schemi-riparto/
sudo cp /home/roberto.cerfogli/sophia-schemi-riparto/lib/*.jar /home/orchestrator/sophia/schemi-riparto/lib/
sudo chown -R orchestrator.orchestrator /home/orchestrator/sophia/schemi-riparto/*

sudo cp /home/roberto.cerfogli/sophia-schemi-riparto/pricing-and-claim* /home/orchestrator/sophia/pricing-claim/
sudo cp /home/roberto.cerfogli/sophia-schemi-riparto/*.jar /home/orchestrator/sophia/pricing-claim/
sudo cp /home/roberto.cerfogli/sophia-schemi-riparto/lib/*.jar /home/orchestrator/sophia/pricing-claim/lib/
sudo chown -R orchestrator.orchestrator /home/orchestrator/sophia/pricing-claim/*

sudo cp /home/roberto.cerfogli/sophia-schemi-riparto/distribution* /home/orchestrator/sophia/distribution/
sudo cp /home/roberto.cerfogli/sophia-schemi-riparto/*.jar /home/orchestrator/sophia/distribution/
sudo cp /home/roberto.cerfogli/sophia-schemi-riparto/lib/*.jar /home/orchestrator/sophia/distribution/lib/
sudo chown -R orchestrator.orchestrator /home/orchestrator/sophia/distribution/*
