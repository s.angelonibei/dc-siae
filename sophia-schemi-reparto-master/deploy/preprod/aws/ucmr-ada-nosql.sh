#!/bin/sh

JENV=debug
JENV_DTP=test

JHOME=/home/orchestrator/sophia/schemi-riparto
JNAME=ucmr-ada-nosql

JPACKAGE=com.alkemytech.sophia.royalties
JCLASS=UcmrAdaNoSql
JCONFIG=$JNAME.properties
#JSTDOUT=$JHOME/$JNAME.out
JSTDOUT=/dev/null
JOPTIONS="-Xmx4G -Ddefault.home_folder=$JHOME -Ddefault.environment=$JENV -Ddefault.environment_dtp=$JENV_DTP"
JJAR=sophia-schemi-riparto-1.0.jar

#cd $JHOME

nohup java -cp "$JHOME/$JJAR:$JHOME/lib/*" $JOPTIONS $JPACKAGE.$JCLASS $JHOME/$JCONFIG $1 $2 $3 $4 2>&1 >> $JSTDOUT &


