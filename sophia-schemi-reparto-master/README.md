Tool per l'estrazione degli schemi di riparto, dal database ULISSE e dai file csv di UCMR-ADA.
- Esporta i dati per un periodo di riferimento (anno e mese) sotto forma di database no-sql.
- Crea un database no-sql contenente gli schemi di riparto per ogni CODICE OPERA, con relativa validità territoriale.

Tool per l'estrazione dei territori TIS, dal database ULISSE.
- Esporta i dati per un periodo di riferimento (anno e mese) sotto forma di file json.

Tool per l'estrazione degli agreement IPI, dal database IPI.
- Esporta i dati per un periodo di riferimento (anno e mese) sotto forma di database no-sql.
- Crea un database no-sql contenente le società di tutela per ogni IPI NAME NUMBER, con relativa validità territoriale e percentuale di tutela.

Carica su Amazon S3 i file organizzandoli secondo periodi di riferimento e la data di creazione (i.e. versione).
