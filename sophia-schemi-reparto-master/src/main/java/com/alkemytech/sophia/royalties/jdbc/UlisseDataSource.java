package com.alkemytech.sophia.royalties.jdbc;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.jdbc.DBCP2DataSource;
import com.alkemytech.sophia.royalties.Configuration;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class UlisseDataSource extends DBCP2DataSource {

	private static final Logger logger = LoggerFactory.getLogger(UlisseDataSource.class);

	@Inject
	public UlisseDataSource(Configuration configuration,
			@Named("ulisse_data_source") String propertyPrefix) {
		super(configuration.getProperties(), propertyPrefix);
		logger.debug("propertyPrefix: {}", propertyPrefix);
	}

}


