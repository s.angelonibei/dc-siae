package com.alkemytech.sophia.royalties.nosql;

import java.nio.charset.Charset;

import com.alkemytech.sophia.commons.mmap.ObjectCodec;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class IpiTisAgmCodec extends ObjectCodec<IpiTisAgm> {

	private final Charset charset;
	
	public IpiTisAgmCodec(Charset charset) {
		super();
		this.charset = charset;
	}

	@Override
	public IpiTisAgm bytesToObject(byte[] bytes) {
		beginDecoding(bytes);
		final long agmid = getPackedLongEx();
		final Territories territories = new Territories(getString(charset));
		return new IpiTisAgm(agmid, territories);
	}

	@Override
	public byte[] objectToBytes(IpiTisAgm object) {
		beginEncoding();
		putPackedLongEx(object.agmid);
		putString(object.territories.toString(), charset);
		return commitEncoding();
	}
	
}
