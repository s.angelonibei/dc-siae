package com.alkemytech.sophia.royalties.nosql;

import java.nio.charset.Charset;

import com.alkemytech.sophia.commons.mmap.ObjectCodec;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class UcmrAdaWorkCodec extends ObjectCodec<UcmrAdaWork> {
	
	private final Charset charset;
	
	public UcmrAdaWorkCodec(Charset charset) {
		super();
		this.charset = charset;
	}

	@Override
	public UcmrAdaWork bytesToObject(byte[] bytes) {
		beginDecoding(bytes);
		final String masterCode = getString(charset);
		final UcmrAdaWork object = new UcmrAdaWork(masterCode);
		for (int rows = getPackedIntEx(); rows > 0; rows --) {
			final UcmrAdaRow row = new UcmrAdaRow();
			row.workNumber = getString(charset);
			row.role = getString(charset);
			row.shareholder = getString(charset);
			row.demShare = getString(charset);
			row.drmShare = getString(charset);
			row.ipnamenr = getString(charset);
			row.territory = getString(charset);
			row.status = getString(charset);
			object.add(row);
		}
		return object;
	}

	@Override
	public byte[] objectToBytes(UcmrAdaWork object) {
		beginEncoding();
		putString(object.masterCode, charset);
		putPackedIntEx(object.rows.size());
		for (UcmrAdaRow row : object.rows) {
			putString(row.workNumber, charset);
			putString(row.role, charset);
			putString(row.shareholder, charset);
			putString(row.demShare, charset);
			putString(row.drmShare, charset);
			putString(row.ipnamenr, charset);
			putString(row.territory, charset);
			putString(row.status, charset);
		}
		return commitEncoding();
	}
	
}
