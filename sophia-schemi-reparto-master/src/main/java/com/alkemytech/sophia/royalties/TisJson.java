package com.alkemytech.sophia.royalties;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.sqs.MessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.SqsMessageHelper;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.alkemytech.sophia.commons.sqs.TrieMessageDeduplicator;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.royalties.country.JdbcCountryService;
import com.alkemytech.sophia.royalties.jdbc.UlisseDataSource;
import com.google.common.base.Strings;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Scopes;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import com.ibm.icu.text.SimpleDateFormat;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class TisJson extends MicroService {
	
	private static final Logger logger = LoggerFactory.getLogger(TisJson.class);

	protected static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// amazon service(s)
			bind(S3.class)
				.in(Scopes.SINGLETON);
			bind(SQS.class)
				.in(Scopes.SINGLETON);
			// data source(s)
			bind(DataSource.class)
				.annotatedWith(Names.named("ULISSE"))
				.to(UlisseDataSource.class)
				.asEagerSingleton();
			// other binding(s)
			bind(JdbcCountryService.class)
				.asEagerSingleton();
			bind(Configuration.class)
				.asEagerSingleton();
			bind(TisJson.class)
				.asEagerSingleton();
		}

	}

	public static void main(String[] args) {
		try {
			final TisJson instance = Guice
					.createInjector(new GuiceModuleExtension(args,
							"/tis-json.properties"))
				.getInstance(TisJson.class)
					.startup();
			try {
				instance.process(args);
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}

	private final SQS sqs;
	private final JdbcCountryService countryService;

	@Inject
	protected TisJson(Configuration configuration,
			@Named("charset") Charset charset,
			S3 s3, SQS sqs, JdbcCountryService countryService) {
		super(configuration, charset, s3);
		this.sqs = sqs;
		this.countryService = countryService;
	}

	public TisJson startup() throws IOException {
		if ("true".equalsIgnoreCase(configuration.getProperty("tis_json.locked", "true"))) {
			throw new IllegalStateException("application locked");
		}
		super.startup();
		sqs.startup();
		return this;
	}

	public TisJson shutdown() throws IOException {
		sqs.shutdown();
		super.shutdown();
		return this;
	}

	public void process(String[] args) throws Exception {
		final int bindPort = Integer.parseInt(configuration.getProperty("tis_json.bind_port",
				configuration.getProperty("default.bind_port", "0")));
		// bind lock tcp port
		try (ServerSocket socket = new ServerSocket(bindPort)) {
			logger.debug("socket bound to {}", socket.getLocalSocketAddress());
			
			// standalone mode
			if (Arrays.asList(args).contains("standalone") ||
					"true".equalsIgnoreCase(configuration
							.getProperty("tis_json.standalone", "false"))) {
				final JsonObject input = GsonUtils.fromJson(configuration
						.getProperty("tis_json.standalone.message_body"), JsonObject.class);
				final JsonObject output = new JsonObject();
				logger.debug("standalone input {}", new GsonBuilder()
						.setPrettyPrinting().create().toJson(input));
				processMessage(input, output);
				logger.debug("standalone output {}", new GsonBuilder()
						.setPrettyPrinting().create().toJson(output));
				return;
			}
			
			// sqs message pump
			final SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration
					.getProperties(), "tis_json.sqs");
			final MessageDeduplicator deduplicator = new TrieMessageDeduplicator(new File(configuration
					.getProperty("tis_json.sqs.deduplicator_folder")));
			sqsMessagePump.pollingLoop(deduplicator, new SqsMessagePump.Consumer() {

				private JsonObject output;
				private JsonObject error;

				@Override
				public JsonObject getStartedMessagePayload(JsonObject message) {
					output = null;
					error = null;
					return SqsMessageHelper.formatContext();
				}

				@Override
				public boolean consumeMessage(JsonObject message) {
					try {
						output = new JsonObject();
						processMessage(GsonUtils
								.getAsJsonObject(message, "body"), output);
						error = null;
						return true;
					} catch (Exception e) {
						output = null;
						error = SqsMessageHelper.formatError(e);
						return false;
					}
				}

				@Override
				public JsonObject getCompletedMessagePayload(JsonObject message) {
					return output;
				}

				@Override
				public JsonObject getFailedMessagePayload(JsonObject message) {
					return error;
				}

			});
		} catch (Exception e) {
			logger.error("process", e);
		}
	}
	
	private void processMessage(JsonObject input, JsonObject output) throws Exception {

//		{
//		  "body": {
//		    "year": "2017",
//		    "month": "03",
//		    "force": true
//		  },
//		  "header": {
//	  	    "queue": "dev_to_process_tis_json_v2",
//		    "timestamp": "2018-05-10T00:00:00.000000+02:00",
//		    "uuid": "7f36a2bb-0637-4d4d-9990-4ed9644adefc",
//		    "sender": "aws-console"
//		  }
//		}

		logger.debug("dumping countries");
		final long startTimeMillis = System.currentTimeMillis();
		final String datetime = new SimpleDateFormat(configuration
				.getProperty("tis_json.datetime_format", configuration
						.getProperty("default.datetime_format", "yyyyMMddHHmmss")))
				.format(new Date());
		logger.debug("datetime {}", datetime);

		// parse input json message
		String year = GsonUtils.getAsString(input, "year");
		String month = GsonUtils.getAsString(input, "month");
		if (Strings.isNullOrEmpty(year) ||
				Strings.isNullOrEmpty(month)) {
			throw new IllegalArgumentException("invalid input message");
		}
		String day = GsonUtils.getAsString(input, "day", configuration
				.getProperty("tis_json.reference_day", "01"));
		final JsonArray stepsJson = GsonUtils.getAsJsonArray(input, "steps");
		final Set<String> steps = new HashSet<>();
		if (null != stepsJson) {
			for (JsonElement stepJson : stepsJson)
				steps.add(stepJson.getAsString());
		} else {
			steps.addAll(Arrays.asList("create", "upload"));
		}
		final String period = fixPeriod(month);
		year = String.format("%04d", Integer.parseInt(year));
		month = fixMonth(month);
		day = String.format("%02d", Integer.parseInt(day));
		logger.debug("year {}", year);
		logger.debug("month {}", month);
		logger.debug("day {}", day);		
		logger.debug("period {}", period);		
		logger.debug("steps {}", steps);

		// sql reference date
		final String sqldate = String
				.format("to_date('%s-%s-%s', 'YYYY-MM-DD')", year, month, day);
		logger.debug("sqldate {}", sqldate);

		// configuration tags
		configuration.setTag("{datetime}", datetime);
		configuration.setTag("{year}", year);
		configuration.setTag("{month}", month);
		configuration.setTag("{day}", day);
		configuration.setTag("{period}", period);
		configuration.setTag("{sqldate}", sqldate);

		// configuration
//		final boolean debug = "true".equalsIgnoreCase(configuration
//				.getProperty("tis_json.debug", configuration.getProperty("default.debug")));
		final File homeFolder = new File(configuration
				.getProperty("default.home_folder"));
		
		// archive file
		final File archiveFile = new File(configuration.getProperty("tis_json.archive_file"));
		logger.debug("archiveFile {}", archiveFile);

		///////////
		// create
		///////////
		
		if (steps.contains("create")) {

			final long lapTimeMillis = System.currentTimeMillis();

			// load countries from database
			countryService.loadCountries();			
			
			// export to json file
			archiveFile.delete();
			countryService.save(archiveFile);
			if ("true".equalsIgnoreCase(configuration
					.getProperty("tis_json.archive_file.delete_when_done", "true"))) {
				archiveFile.deleteOnExit();
			}

			// output stats
			final JsonObject countriesStats = new JsonObject();
			countriesStats.addProperty("sqldate", sqldate);
			countriesStats.addProperty("duration", TextUtils
					.formatDuration(System.currentTimeMillis() - lapTimeMillis));
			output.add("countries", countriesStats);			
		}
		
		///////////
		// upload
		///////////
		
		if (steps.contains("upload")) {
			
			final long lapTimeMillis = System.currentTimeMillis();
			
			final JsonObject uploadStats = new JsonObject();
			
			upload("tis_json", homeFolder, archiveFile, uploadStats);
			
			if ("true".equalsIgnoreCase(configuration
					.getProperty("tis_json.archive_file.delete_when_done", "true"))) {
				archiveFile.delete();
			}

			uploadStats.addProperty("duration", TextUtils
					.formatDuration(System.currentTimeMillis() - lapTimeMillis));
			
			output.add("upload", uploadStats);
			
		}
						
		// output
		output.addProperty("period", year + month);
		output.addProperty("startTime", DateFormat
				.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM)
					.format(new Date(startTimeMillis)));
		output.addProperty("totalDuration", TextUtils
				.formatDuration(System.currentTimeMillis() - startTimeMillis));
		
		logger.debug("message processed in {}",
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));
	}

}
