package com.alkemytech.sophia.royalties.blacklist;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.google.common.base.Strings;
import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class BlackListRule {
	
	// ---------- ---------- ---------- ----------
	// EMI Anglo-American repertoire
	// CI
	// 00009576175:00176544252:00467151940:00035943081:00011618128:00241326402:00039793831:00124217804:00198537711:00465397124:00517428649:00161534391
	// PRS:IMRO:ASCAP:BMI:SESAC:SOCAN:APRA:AMRA:SAMRO:NM
	// DEM:DRM
	// ALL
	// ---------- ---------- ---------- ----------
	// UMPI non BIEM repertoire
	// CI
	// 00547691219:00004678586:00155909645:00009361005:00043277685:00024288294:00084979120:00082774059:00071158490
	// !SIAE
	// DEM:DRM
	// ALL
	// ---------- ---------- ---------- ----------
	// SACEM direct member repertoire
	// ED
	// SACEM
	// DEM:DRM
	// ALL
	// ---------- ---------- ---------- ----------
	// NOT MEMBER
	// ED
	// NM
	// DEM:DRM
	// ALL
	// ---------- ---------- ---------- ----------
	// HDS-ZAMP direct member repertoire
	// ED
	// HDS-ZAMP
	// DEM:DRM
	// YOM-201701:YOU-201701:GOO-201701:GOS-201701:DEE-201701:ITU-201801:SPO-201801:APL-201801
	// ---------- ---------- ---------- ----------
	
	public final String ruleName;				// free form text
	private final String ruleType;				// CI (controllo incrociato) or ED (esclusione diretta)
	private final Set<Long> ciIpiNumbers;		// set of ipinamenr for CI
	private final String edSociety;				// society name for ED
	private final Set<String> societies;		// set of society names
	private final boolean excludeSocieties;		// flag indicating if society names are prefixed by not sign
	private final Set<String> rightTypes;		// set of right types
	private final Map<String, String> dsps;		// map of dsp codes (key) and validity start date (value)
	private final boolean allDsps;				// flag indicating ALL dsps
	
	public BlackListRule(String name, String type, String ipnamenrs, String societies, String rights, String dsps) {
		super();
		
		final Set<String> ipnamenrsSet = Strings.isNullOrEmpty(ipnamenrs) ?
				null : new HashSet<>(Arrays.asList(ipnamenrs.split(":")));
		final Set<String> societiesSet = Strings.isNullOrEmpty(societies) ?
				null : new HashSet<>(Arrays.asList(societies.split(":")));
		final Set<String> rightsSet = Strings.isNullOrEmpty(rights) ?
				null : new HashSet<>(Arrays.asList(rights.split(":")));
		final Set<String> dspsSet = Strings.isNullOrEmpty(dsps) ?
				null : new HashSet<>(Arrays.asList(dsps.split(":")));
		
		this.ruleName = name;
		
		this.ruleType = type.toUpperCase();
		
		this.ciIpiNumbers = new HashSet<>();
		if (null != ipnamenrsSet && "CI".equalsIgnoreCase(type)) {
			for (String ipnamenr : ipnamenrsSet) {
				this.ciIpiNumbers.add(Long.parseLong(ipnamenr));
			}
		}
		this.edSociety = "ED".equalsIgnoreCase(type) ? ipnamenrs.trim() : null;
		
		this.societies = new HashSet<>();
		boolean exclamationPrefix = false;
		if (null != societiesSet) {
			for (String society : societiesSet) {
				society = society.trim();
				if (society.startsWith("!")) {
					exclamationPrefix = true;
					society = society.substring(1);
				} else if (exclamationPrefix) {
					throw new IllegalArgumentException("illegal argument: " + societies);
				}
				this.societies.add(society.trim().toUpperCase());
			}
		}
		this.excludeSocieties = exclamationPrefix;
		
		this.rightTypes = new HashSet<>();
		if (null != rightsSet) {
			for (String right : rightsSet) {
				this.rightTypes.add(right.trim().toUpperCase());
			}
		}

		this.dsps = new HashMap<>();
		boolean allDsps = false;
		if (null != dspsSet) {
			for (String dsp : dspsSet) {
				if ("ALL".equalsIgnoreCase(dsp)) {
					allDsps = true;
					break;
				}
				final int hyphen = dsp.indexOf('-');
				if (-1 == hyphen) {
					this.dsps.put(dsp.trim().toUpperCase(), "");
				} else {
					this.dsps.put(dsp.substring(0, hyphen).trim().toUpperCase(),
							dsp.substring(1 + hyphen).trim());					
				}
			}
		}
		this.allDsps = allDsps;
	}

	public boolean isControlloIncrociato() {
		return "CI".equals(ruleType);
	}
	
	public boolean isEsclusioneDiretta() {
		return "ED".equals(ruleType);
	}

	public boolean isCiIpiNumberIncluded(Long ipnamenr) {
		return null == ipnamenr ?
				false : ciIpiNumbers.contains(ipnamenr);
	}

	public boolean isEdSocietyIncluded(String society) {
		return edSociety.equalsIgnoreCase(society);
	}

	public boolean isSocietyIncluded(String society) {
		final boolean found = societies.contains(society.toUpperCase());
		return excludeSocieties ? !found : found;
	}

	public boolean isAnySocietyIncluded(Collection<String> societies) {
		for (String society : societies) {
			if (isSocietyIncluded(society)) {
				return true;
			}
		}
		return false;
	}

	public boolean isRightIncluded(String right) {
		return rightTypes.contains(right.toUpperCase());
	}

	public boolean isDspIncluded(String dspCode, String period) {
		if (allDsps) {
			return true;
		}
		final String validFrom = dsps.get(dspCode);
		if (null == validFrom) {
			return false;
		} else if ("".equals(validFrom)) {
			return true;
		}
		return period.compareTo(validFrom) >= 0;
	}
	
	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}

}
