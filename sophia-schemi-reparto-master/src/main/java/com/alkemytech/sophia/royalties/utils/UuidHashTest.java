package com.alkemytech.sophia.royalties.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.alkemytech.sophia.commons.util.TextUtils;

public class UuidHashTest {

	private static final char[] HEX_DIGITS = "0123456789abcdef".toCharArray();
	
	private static char[] toHash(String text) {
		final int code = text.hashCode();
		final char[] hash = new char[5];
		hash[0] = HEX_DIGITS[code & 0x0f];
		hash[1] = HEX_DIGITS[(code >> 4) & 0x0f];
		hash[2] = HEX_DIGITS[(code >> 8) & 0x0f];
		hash[3] = HEX_DIGITS[(code >> 12) & 0x0f];
		hash[4] = HEX_DIGITS[(code >> 16) & 0x0f];
		return hash;
	}
	
	public static void main(String[] args) throws Exception {
		
		long millis = System.currentTimeMillis();
		
		Map<String, Integer> sizes = new HashMap<>();
		
//		int keyLength = 4; // 4 least significant chars of hex hashcode, 16M record, 6.5Mb disk, average bucket size 256
//		key length 4
//		buckets 65536 = 16^4
//		min 192
//		avg 256 = 16M / (16^4)
//		max 323
//		elapsed 38s591ms
		
//		int keyLength = 5; // 5 least significant chars of hex hashcode, 16M record, 130Mb disk, average bucket size 16
//		key length 5
//		buckets 1048576 = 16^5
//		min 2
//		avg 16 = 16M / (16^5)
//		max 39
//		elapsed 38s803ms

		for (int i = 0; i < 16 * 1024 * 1024; i ++) {
			
			String uuid = UUID.randomUUID().toString();
//			uuid = UuidUtils.uuidToBase64(uuid);
//			String uuid = Long.toString(1000000000L + i);
//			String uuid = Double.toString(Math.random());
			
			String key = new String(toHash(uuid));
						
			Integer count = sizes.get(key);
			if (null == count) {
				sizes.put(key, 1);
			} else {
				sizes.put(key, 1 + count);
			}
			
		}
		
		int max = Integer.MIN_VALUE;
		int min = Integer.MAX_VALUE;
		long sum = 0;
		for (Integer size : sizes.values()) {
			min = Math.min(min, size);
			max = Math.max(max, size);
			sum += size;
		}
		
//		System.out.println(sizes);
		System.out.println("key length " + toHash("").length);
		System.out.println("buckets " + sizes.size());
		System.out.println("min " + min);
		System.out.println("avg " + (sum / sizes.size()));
		System.out.println("max " + max);
		
		System.out.println("elapsed " + TextUtils.formatDuration(System.currentTimeMillis() - millis));
		
	}
	
	
}
