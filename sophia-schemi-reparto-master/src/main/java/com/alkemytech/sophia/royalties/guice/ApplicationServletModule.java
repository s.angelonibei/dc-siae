package com.alkemytech.sophia.royalties.guice;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.royalties.servlet.AggiornaServlet;
import com.alkemytech.sophia.royalties.servlet.GsonFilter;
import com.alkemytech.sophia.royalties.servlet.HealthServlet;
import com.alkemytech.sophia.royalties.servlet.NetmaskFilter;
import com.alkemytech.sophia.royalties.servlet.SearchServlet;
import com.alkemytech.sophia.royalties.servlet.SettingViewServlet;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.inject.Provides;
import com.google.inject.Scopes;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.google.inject.servlet.ServletModule;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class ApplicationServletModule extends ServletModule {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.inject.servlet.ServletModule#configureServlets()
	 */
	@Override
	protected void configureServlets() {

		// servlet filter(s)

		bind(NetmaskFilter.class).in(Scopes.SINGLETON);
		filter("/*").through(NetmaskFilter.class);

		bind(GsonFilter.class).in(Scopes.SINGLETON);
		filter("/search").through(GsonFilter.class);

		bind(SearchServlet.class).in(Scopes.SINGLETON);
		serve("/search").with(SearchServlet.class);

		bind(HealthServlet.class).in(Scopes.SINGLETON);
		serve("/status").with(HealthServlet.class);
		
		bind(GsonFilter.class).in(Scopes.SINGLETON);
		filter("/aggiorna").through(GsonFilter.class);
		bind(AggiornaServlet.class).in(Scopes.SINGLETON);
		serve("/aggiorna").with(AggiornaServlet.class);
		
		bind(SettingViewServlet.class).in(Scopes.SINGLETON);
		serve("/view").with(SettingViewServlet.class);

	}

	@Provides
	@Singleton
	@Named("configurations_cache")
	public Cache<String, Properties> getConfigCache(@Named("configuration") Properties configuration) {
		final long maximumSize = TextUtils
				.parseLongSize(configuration.getProperty("servlet.cache.config.max_size", "1K"));
		final long expireAfter = TextUtils
				.parseLongDuration(configuration.getProperty("servlet.cache.config.expire_after", "5m"));
		return CacheBuilder.newBuilder().maximumSize(maximumSize).expireAfterWrite(expireAfter, TimeUnit.MILLISECONDS)
				.build();
	}

}
