package com.alkemytech.sophia.royalties.filenaming;

import com.alkemytech.sophia.royalties.Configuration;
import com.alkemytech.sophia.royalties.utils.RestClient;
import com.google.gson.JsonObject;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Properties;

@Data
public class MMSpecialCharCcid implements Serializable {

    private static final Logger logger = LoggerFactory.getLogger(MMSpecialCharCcid.class);

    private String context;
    private String idDsp;
    private String charList;
    private Long titleLength;
    private String enabled;

    public MMSpecialCharCcid(Configuration configuration, String dsp) {
        try {
            final RestClient.Result response = RestClient.get(configuration.getProperty("pricing_and_claim.mmSpecialCharCcid_configuration").replace("{idDsp}", dsp));
            logger.debug("MMSpecialCharCcid: response status  {}", response.getStatusCode());
            logger.debug("MMSpecialCharCcid: response body {}", response.toString());
            if (response.getStatusCode() == 200) {
                final JsonObject responseBody = response.getJsonElement().getAsJsonObject();
                if (responseBody.has("idDsp")) {
                    if (responseBody.has("idDsp"))
                        this.idDsp = responseBody.get("idDsp").getAsString();
                    if (responseBody.has("charList"))
                        this.charList = responseBody.get("charList").getAsString();
                    if (responseBody.has("titleLength"))
                        this.titleLength = responseBody.get("titleLength").getAsLong();
                    if (responseBody.has("enabled"))
                        this.enabled = responseBody.get("enabled").getAsString();
                }
            }
        } catch (Exception e) {
            logger.error("MMSpecialCharCcid", e);
        }
    }

    public boolean checkMSCCcid() {
        return (this.getIdDsp() != null && this.getCharList() != null && this.getEnabled() != null) ? true : false;
    }
}
