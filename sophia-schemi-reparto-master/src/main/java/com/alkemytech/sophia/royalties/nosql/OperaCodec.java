package com.alkemytech.sophia.royalties.nosql;

import com.alkemytech.sophia.commons.mmap.ObjectCodec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.Charset;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class OperaCodec extends ObjectCodec<Opera> {

    private static final Logger logger = LoggerFactory.getLogger(OperaCodec.class);
    private final Charset charset;

    public OperaCodec(Charset charset) {
        super();
        this.charset = charset;
    }

    @Override
    public Opera bytesToObject(byte[] bytes) {
        beginDecoding(bytes);
        final String codiceOpera = getString(charset);
        final Opera object = new Opera(codiceOpera);
        boolean schemaRipartoVersionOld = false;
        int i = 0;
        for (int schemi = getPackedIntEx(); schemi > 0; schemi--) {
            final long idSchemaRiparto = getPackedLongEx();
            final Territories territories = new Territories(getString(charset));
            final SchemaRiparto schema = new SchemaRiparto(idSchemaRiparto, territories);
            for (int quote = getPackedIntEx(); quote > 0; quote--) {
                final QuotaRiparto quota = new QuotaRiparto();
                quota.codiceIpi = getPackedLongEx();
                if (-1L == quota.codiceIpi) {
                    quota.codiceIpi = null;
                }
                quota.posizioneSiae = getString(charset);
                quota.tipoQualifica = getString(charset);
                quota.tipoQuota = getString(charset);
                quota.percentuale = getFloat();
                quota.cognome = getString(charset);
                quota.codiceConto = getString(charset);
                quota.codiceContoEstero = getString(charset);
                quota.idRepertorio = getPackedLongEx();
                quota.idTipoIrregolarita = getString(charset);
                if (!schemaRipartoVersionOld && i == 0) {
                   // logger.debug("entro per capire la versione dello schema ");
                    int pos = getPosition();
//                    logger.debug("Position " + pos);
                    float numeratore = getFloat();
                    float denominatore = getFloat();
//                    logger.debug("numeratore " + numeratore + " position " + getPosition());
                    if (numeratore < 0 || denominatore < 0 || String.valueOf(denominatore).contains("E") || String.valueOf(numeratore).contains("E")) {
                       // logger.debug("riconosciuto schema vecchio:  " + numeratore + " " + denominatore);
                        schemaRipartoVersionOld = true;
                        quota.numeratore = 0;
                        quota.denominatore = 0;
                        setPosition(pos);
                    }
                    setPosition(pos);
                    i = 1;
                }
                if (!schemaRipartoVersionOld) {
                   {
//                    logger.debug("NEW VERS");
                        // logger.debug("idSchemaRiparto " + idSchemaRiparto);

                        quota.numeratore = getFloat();
                        quota.denominatore = getFloat();

                       // logger.debug("quota num " + quota.numeratore);
                       // logger.debug("quota den " + quota.denominatore);
                    }
                }
                schema.add(quota);
            }
            object.add(schema);
        }
        return object;
    }

//    @Override
//    public Opera bytesToObject(byte[] bytes) {
//        beginDecoding(bytes);
//        final String codiceOpera = getString(charset);
//        final Opera object = new Opera(codiceOpera);
//        boolean schemaRipartoVersionOld = false;
//        for (int schemi = getPackedIntEx(); schemi > 0; schemi--) {
//            final long idSchemaRiparto = getPackedLongEx();
//            final Territories territories = new Territories(getString(charset));
//            final SchemaRiparto schema = new SchemaRiparto(idSchemaRiparto, territories);
//            for (int quote = getPackedIntEx(); quote > 0; quote--) {
//                final QuotaRiparto quota = new QuotaRiparto();
//                quota.codiceIpi = getPackedLongEx();
//                if (-1L == quota.codiceIpi) {
//                    quota.codiceIpi = null;
//                }
//                quota.posizioneSiae = getString(charset);
//                quota.tipoQualifica = getString(charset);
//                quota.tipoQuota = getString(charset);
//                quota.percentuale = getFloat();
//                quota.cognome = getString(charset);
//                quota.codiceConto = getString(charset);
//                quota.codiceContoEstero = getString(charset);
//                quota.idRepertorio = getPackedLongEx();
//                quota.idTipoIrregolarita = getString(charset);
//
//                schema.add(quota);
//            }
//            object.add(schema);
//        }
//        return object;
//    }

    @Override
    public byte[] objectToBytes(Opera object) {
        beginEncoding();
        putString(object.codiceOpera, charset);
        putPackedIntEx(object.schemi.size());
        for (SchemaRiparto schema : object.schemi) {
            putPackedLongEx(schema.idSchemaRiparto);
            putString(schema.territories.toString(), charset);
            putPackedIntEx(schema.quote.size());
            for (QuotaRiparto quota : schema.quote) {
                putPackedLongEx(null == quota.codiceIpi ? -1L : quota.codiceIpi);
                putString(quota.posizioneSiae, charset);
                putString(quota.tipoQualifica, charset);
                putString(quota.tipoQuota, charset);
                putFloat(quota.percentuale);
                putString(quota.cognome, charset);
                putString(quota.codiceConto, charset);
                putString(quota.codiceContoEstero, charset);
                putPackedLongEx(quota.idRepertorio);
                putString(quota.idTipoIrregolarita, charset);
                putFloat(quota.numeratore);
                putFloat(quota.denominatore);
            }
        }
        return commitEncoding();
    }

}
