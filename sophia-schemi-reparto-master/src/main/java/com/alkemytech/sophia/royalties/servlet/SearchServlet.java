package com.alkemytech.sophia.royalties.servlet;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemy.siae.sophia.pricing.DsrLine;
import com.alkemy.siae.sophia.pricing.DsrMetadata;
import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.nosql.NoSqlException;
import com.alkemytech.sophia.commons.nosql.NoSql.Selector;
import com.alkemytech.sophia.commons.util.FileUtils;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.HeartBeat;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.royalties.Configuration;
import com.alkemytech.sophia.royalties.LatestVersion;
import com.alkemytech.sophia.royalties.blacklist.BlackList;
import com.alkemytech.sophia.royalties.blacklist.BlackListService;
import com.alkemytech.sophia.royalties.claim.ClaimHelper;
import com.alkemytech.sophia.royalties.claim.SocietyInfo;
import com.alkemytech.sophia.royalties.nosql.IpiSocNoSqlDb;
import com.alkemytech.sophia.royalties.nosql.OperaNoSqlDb;
import com.alkemytech.sophia.royalties.nosql.UuidCodici;
import com.alkemytech.sophia.royalties.nosql.UuidCodiciNoSqlDb;
import com.alkemytech.sophia.royalties.role.RoleCatalog;
import com.alkemytech.sophia.royalties.scheme.IrregularityCatalog;
import com.alkemytech.sophia.royalties.scheme.Royalty;
import com.alkemytech.sophia.royalties.scheme.RoyaltyScheme;
import com.alkemytech.sophia.royalties.scheme.RoyaltySchemeService;
import com.alkemytech.sophia.royalties.scheme.TemporaryWorkCatalog;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.inject.Inject;
import com.google.inject.name.Named;


@SuppressWarnings("serial")
public class SearchServlet extends HttpServlet {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	protected final Configuration configuration;
	protected final Charset charset;
	protected final S3 s3;
	private final RoleCatalog roleCatalog;
	private final TemporaryWorkCatalog temporaryWorkCatalog;
	private final IrregularityCatalog irregularityCatalog;
	private final RoyaltySchemeService royaltySchemeService;
	private final BlackListService blackList;
	private final Gson gson;
	private final LockKbService lockWeb;
	
	@Inject
	protected SearchServlet(Configuration configuration,
			@Named("charset") Charset charset,
			S3 s3, RoleCatalog roleCatalog,
			TemporaryWorkCatalog temporaryWorkCatalog,
			IrregularityCatalog irregularityCatalog,
			RoyaltySchemeService royaltySchemeService,
			BlackListService blackList,
			Gson gson,
			LockKbService lockWeb
			) {
		super();
		this.configuration = configuration;
		this.charset = charset;
		this.s3 = s3;
		this.roleCatalog = roleCatalog;
		this.temporaryWorkCatalog = temporaryWorkCatalog;
		this.irregularityCatalog = irregularityCatalog;
		this.royaltySchemeService = royaltySchemeService;
		this.blackList = blackList;
		this.gson = gson;
		this.lockWeb = lockWeb;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JsonObject processing = new JsonObject();
		try {
			if(!this.lockWeb.isLock()) {
				String year = configuration.getTag("{year}");
				String month = configuration.getTag("{month}");
				String dspCode = null;
				String societa = null;//"SIAE";
				String codiceOpera = null;//"12071055900";
				String uuid = null;//"b41c4c45-dd27-4439-a2aa-8cfe2c13850a";
				String territorio = null;//"IT";
				final JsonObject requestJson = (JsonObject)request.getAttribute(GsonFilter.REQUEST_JSON);
				for (Map.Entry<String, JsonElement> entry : requestJson.entrySet()) {
					final String textType = entry.getKey();
					if(entry.getValue().isJsonArray()) {
						for (JsonElement text : GsonUtils.getAsJsonArray(entry.getValue())) {
							logger.info(text.getAsString()+ ":" + textType);
						}
					}else {
						if("dspCode".equals(textType)) {
							dspCode = entry.getValue().getAsString();
						}else if("uuid".equals(textType)) {
							uuid = entry.getValue().getAsString();						
						}else if("societa".equals(textType)) {
							societa = entry.getValue().getAsString();						
						}else if("codiceOpera".equals(textType)) {
							codiceOpera = entry.getValue().getAsString();						
						}else if("territorio".equals(textType)) {
							territorio = entry.getValue().getAsString();						
						}
					}
				}	
				if(year != null && month != null) {
					processing = this.search(societa,
							codiceOpera,
							uuid,
							territorio,
							dspCode,
							year,
							month);
				}else {
					processing.addProperty("errore", "kb non settata");
				}
			}else {
				processing.addProperty("errore", "kb in aggiornamento");
			}
		}catch(Exception ex) {
			ex.printStackTrace();
			processing.addProperty("errore", "errore interno");
		}
		response.setContentType("application/json");
		response.getWriter().write(gson.toJson(processing));			
	}

	/*
	 * 
	 * b41c4c45-dd27-4439-a2aa-8cfe2c13850a
	 System.out.println("type <societa>(SIAE) <codice opera> <territorio> to search royalty schemes");
	 */
	
	private JsonObject search(
			String societa,
			String codiceOpera,
			String uuid,
			String territorio,
			String dspCode,
			String year,
			String month
			) {
		JsonObject processing = new JsonObject();
		if( (dspCode!= null && territorio != null) && (uuid!= null || (societa!= null && codiceOpera != null))) {
			final IpiSocNoSqlDb ipiNoSqlDb = new IpiSocNoSqlDb(configuration.getProperties(), "webapp_search.ipi_nosql");
			final OperaNoSqlDb siaeNoSqlDb = new OperaNoSqlDb(configuration.getProperties(), "webapp_search.siae_nosql");
			final OperaNoSqlDb ucmrAdaNoSqlDb = new OperaNoSqlDb(configuration.getProperties(), "webapp_search.ucmr_ada_nosql");
			final UuidCodiciNoSqlDb codiciNoSqlDb = new UuidCodiciNoSqlDb(configuration.getProperties(), "webapp_search.codici_nosql");

	
			// initialize black list(s)
			final Map<String, BlackList> blackLists = this.blackList.getBlackList();
			
			// society territory
			final Map<String, String> societyTerritoryMap = GsonUtils
					.decodeJsonMap(configuration.getProperty("webapp_search.society_territory", "{}"));
			logger.debug("societyTerritoryMap {}", societyTerritoryMap);
			
			
			
			if(uuid != null) {
				//////////////////////////PER UUID///			
				processing = this.searchUUID(codiciNoSqlDb,siaeNoSqlDb,ipiNoSqlDb, blackLists, ucmrAdaNoSqlDb, societyTerritoryMap,		
						uuid,
						territorio,
						 dspCode,
						 year,
						 month
						);
				//////////////////////////
			}else {
				////////////////////////// PER SOCIETA
				processing = this.search(siaeNoSqlDb,ipiNoSqlDb, blackLists, ucmrAdaNoSqlDb, societyTerritoryMap,		
						societa,
						codiceOpera,
						territorio,
						 dspCode,
						 year,
						 month
						);
				///////////////////////////
				
			}
		}else {
			processing.addProperty("errore", "richiesta errata");
		}
		return processing;
	}
	
	
	private JsonObject searchUUID(UuidCodiciNoSqlDb codiciNoSqlDb,OperaNoSqlDb siaeNoSqlDb,IpiSocNoSqlDb ipiNoSqlDb,Map<String, BlackList> blackLists,OperaNoSqlDb ucmrAdaNoSqlDb,Map<String, String> societyTerritoryMap,		
			String uuid,
			String territorio,
			String dspCode,
			String year,
			String month
			) {
		
		final JsonObject response = new JsonObject();
//		response.addProperty("societa", societa);
//		response.addProperty("codiceOpera", codiceOpera);
		response.addProperty("uuid", uuid);
		response.addProperty("territorio", territorio);
		response.addProperty("dspCode", dspCode);
		response.addProperty("year", year);
		response.addProperty("month", month);

		
		
		final UuidCodici codici = codiciNoSqlDb.get(uuid);
		final JsonArray royaltySchemaJson = new JsonArray();
		final JsonArray blackListSchemaJson = new JsonArray();
		//System.out.println("codici " + codici);
		if (null != codici) {
			for (Map.Entry<String, String> entry : codici.codici.entrySet()) {
				final String societa = entry.getKey();
				final String codiceOpera = entry.getValue();
				logger.info("--------------------------------------------------------------------------------");
				logger.info("societa: " + societa);
				logger.info("codiceOpera: " + codiceOpera);
				
				RoyaltyScheme royaltyScheme = null;
				if ("SIAE".equalsIgnoreCase(societa)) {
					royaltyScheme = royaltySchemeService.lookup(ipiNoSqlDb, siaeNoSqlDb, societa, codiceOpera, territorio);
				} else if ("UCMR-ADA".equalsIgnoreCase(societa)) {
					royaltyScheme = royaltySchemeService.lookup(ipiNoSqlDb, ucmrAdaNoSqlDb, societa, codiceOpera, territorio);
				}
				
				if (null == royaltyScheme) {
//					JsonObject message = new JsonObject();
//					message.addProperty("message", "royalty scheme not found");
					logger.info("royalty scheme not found");
				} else {
					JsonObject jsonObjectSchema = new JsonParser().parse(
							new GsonBuilder().setPrettyPrinting().create().toJson(royaltyScheme)
							).getAsJsonObject();
					
					
					royaltySchemaJson.add(jsonObjectSchema);
					
					logger.info(new GsonBuilder().setPrettyPrinting().create().toJson(royaltyScheme));					
					if (territorio.equalsIgnoreCase(societyTerritoryMap.get(societa))) {											
						final List<Royalty> royalties = blackLists.get(societa).filter(royaltyScheme.royalties, dspCode, year + month);
						
						JsonArray jsonObject = new JsonParser().parse(
								new GsonBuilder()
								.setPrettyPrinting().create().toJson(royalties)
								).getAsJsonArray();
//						response.add("blackList", jsonObject);;
						blackListSchemaJson.add(jsonObject);
						
						logger.info("black list filtered royalties (ready for claim)");
						logger.info(new GsonBuilder().setPrettyPrinting().create().toJson(royalties));			
					} else {
						final List<Royalty> royalties = new ArrayList<>(royaltyScheme.royalties.size());
						for (Royalty royalty : royaltyScheme.royalties) {
							if (!royalty.doNotClaim && !royalty.sospesoPerEditore) {
								royalties.add(royalty);
							}
						}
						
						JsonArray jsonObject = new JsonParser().parse(
								new GsonBuilder()
								.setPrettyPrinting().create().toJson(royalties)
								).getAsJsonArray();
//						response.add("blackList", jsonObject);;
						blackListSchemaJson.add(jsonObject);
					logger.info("royalties ready for claim");
						logger.info(new GsonBuilder().setPrettyPrinting().create().toJson(royalties));			
					}
				}
			}
			if(!royaltySchemaJson.isJsonNull())
				response.add("royaltyScheme", royaltySchemaJson);			
			if(!blackListSchemaJson.isJsonNull())
				response.add("blackList", blackListSchemaJson);;
		}else {
			//codice non presente
			response.addProperty("message", "uuid not found");
		}
		return response;
	}
	
	private JsonObject search(
			OperaNoSqlDb siaeNoSqlDb,
			IpiSocNoSqlDb ipiNoSqlDb,
			Map<String, BlackList> blackLists,
			OperaNoSqlDb ucmrAdaNoSqlDb,
			Map<String, String> societyTerritoryMap,
			String societa,
			String codiceOpera,
			String territorio,
			String dspCode,
			String year,
			String month
			) {
		final JsonObject response = new JsonObject();
		RoyaltyScheme royaltyScheme = null;
		response.addProperty("societa", societa);
		response.addProperty("codiceOpera", codiceOpera);
		response.addProperty("territorio", territorio);
		response.addProperty("dspCode", dspCode);
		response.addProperty("year", year);
		response.addProperty("month", month);
		
		final JsonArray royaltySchemaJson = new JsonArray();
		final JsonArray blackListSchemaJson = new JsonArray();		
		if ("SIAE".equalsIgnoreCase(societa)) {
			royaltyScheme = royaltySchemeService.lookup(ipiNoSqlDb, siaeNoSqlDb, societa, codiceOpera, territorio);
		} else if ("UCMR-ADA".equalsIgnoreCase(societa)) {
			royaltyScheme = royaltySchemeService.lookup(ipiNoSqlDb, ucmrAdaNoSqlDb, societa, codiceOpera, territorio);
		}
		
		if (null == royaltyScheme) {
			response.addProperty("message", "royalty scheme not found");
			logger.info("royalty scheme not found");
		} else {
			JsonObject jsonObjectSchema = new JsonParser().parse(
					new GsonBuilder().setPrettyPrinting().create().toJson(royaltyScheme)
					).getAsJsonObject();
			//response.add("royaltyScheme", jsonObjectSchema);
			royaltySchemaJson.add(jsonObjectSchema);
			response.add("royaltyScheme", royaltySchemaJson);
			
			logger.info(new GsonBuilder().setPrettyPrinting().create().toJson(royaltyScheme));					
			if (territorio.equalsIgnoreCase(societyTerritoryMap.get(societa))) {											
				final List<Royalty> royalties = blackLists.get(societa).filter(royaltyScheme.royalties, dspCode, year + month);
				logger.info("black list filtered royalties");
				JsonArray jsonObject = new JsonParser().parse(
						new GsonBuilder()
						.setPrettyPrinting().create().toJson(royalties)
						).getAsJsonArray();
				blackListSchemaJson.add(jsonObject);
				response.add("blackList", blackListSchemaJson);;
				logger.info(new GsonBuilder()
						.setPrettyPrinting().create().toJson(royalties));			
			}
		}
		return response;
	}
}
