package com.alkemytech.sophia.royalties.nosql;

import com.google.gson.GsonBuilder;
import lombok.Data;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
@Data
public class QuotaRiparto {

    public Long codiceIpi;
    public String posizioneSiae;
    public String tipoQualifica;
    public String tipoQuota;
    public float percentuale;
    public String cognome;
    public String codiceConto;
    public String codiceContoEstero;
    public long idRepertorio;
    public String idTipoIrregolarita;
    // TO DO ADD NUMERATORE E DENOMINATORE
    public float numeratore;
    public float denominatore;

    public QuotaRiparto() {
        super();
        this.numeratore = 0;
        this.denominatore = 0;
    }

    public QuotaRiparto setNumeratore(float numeratore) {
        this.numeratore = numeratore;
        return this;
    }

    public QuotaRiparto setDenominatore(float denominatore) {
        this.denominatore = denominatore;
        return this;
    }

    public QuotaRiparto setCodiceIpi(Long codiceIpi) {
        this.codiceIpi = codiceIpi;
        return this;
    }

    public QuotaRiparto setPosizioneSiae(String posizioneSiae) {
        this.posizioneSiae = posizioneSiae;
        return this;
    }

    public QuotaRiparto setTipoQualifica(String tipoQualifica) {
        this.tipoQualifica = tipoQualifica;
        return this;
    }

    public QuotaRiparto setTipoQuota(String tipoQuota) {
        this.tipoQuota = tipoQuota;
        return this;
    }

    public QuotaRiparto setPercentuale(float percentuale) {
        this.percentuale = percentuale;
        return this;
    }

    public QuotaRiparto setCognome(String cognome) {
        this.cognome = cognome;
        return this;
    }

    public QuotaRiparto setCodiceConto(String codiceConto) {
        this.codiceConto = codiceConto;
        return this;
    }

    public QuotaRiparto setCodiceContoEstero(String codiceContoEstero) {
        this.codiceContoEstero = codiceContoEstero;
        return this;
    }

    public QuotaRiparto setIdRepertorio(long idRepertorio) {
        this.idRepertorio = idRepertorio;
        return this;
    }

    public QuotaRiparto setIdTipoIrregolarita(String idTipoIrregolarita) {
        this.idTipoIrregolarita = idTipoIrregolarita;
        return this;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((codiceIpi == null) ? 0 : codiceIpi.hashCode());
        result = prime * result + ((posizioneSiae == null) ? 0 : posizioneSiae.hashCode());
        result = prime * result + ((tipoQualifica == null) ? 0 : tipoQualifica.hashCode());
        result = prime * result + ((tipoQuota == null) ? 0 : tipoQuota.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        QuotaRiparto other = (QuotaRiparto) obj;
        if (codiceIpi == null) {
            if (other.codiceIpi != null)
                return false;
        } else if (!codiceIpi.equals(other.codiceIpi))
            return false;
        if (posizioneSiae == null) {
            if (other.posizioneSiae != null)
                return false;
        } else if (!posizioneSiae.equals(other.posizioneSiae))
            return false;
        if (tipoQualifica == null) {
            if (other.tipoQualifica != null)
                return false;
        } else if (!tipoQualifica.equals(other.tipoQualifica))
            return false;
        if (tipoQuota == null) {
            if (other.tipoQuota != null)
                return false;
        } else if (!tipoQuota.equals(other.tipoQuota))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return new GsonBuilder()
                .create().toJson(this);
    }

}
