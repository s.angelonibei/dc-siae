package com.alkemytech.sophia.royalties.claim;

import java.math.BigDecimal;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

import com.google.common.util.concurrent.AtomicDouble;
import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class ClaimStats {

	public final AtomicReference<BigDecimal> identificatoValore;
	public final AtomicReference<BigDecimal> identificatoUtilizzazioni;
	public final AtomicLong identificatoRighe;
	public final AtomicReference<BigDecimal> claimValore;
	public final AtomicReference<BigDecimal> claimUtilizzazioni;
	public final AtomicReference<BigDecimal> nonIdentificatoValore;
	public final AtomicReference<BigDecimal> nonIdentificatoUtilizzazioni;
	public final AtomicLong nonIdentificatoRighe;

	public ClaimStats() {
		this.identificatoValore = new AtomicReference<>(BigDecimal.ZERO);
		this.identificatoUtilizzazioni = new AtomicReference<>(BigDecimal.ZERO);
		this.identificatoRighe = new AtomicLong(0L);
		this.claimValore = new AtomicReference<>(BigDecimal.ZERO);
		this.claimUtilizzazioni = new AtomicReference<>(BigDecimal.ZERO);
		this.nonIdentificatoValore = new AtomicReference<>(BigDecimal.ZERO);
		this.nonIdentificatoUtilizzazioni = new AtomicReference<>(BigDecimal.ZERO);
		this.nonIdentificatoRighe = new AtomicLong(0L);
	}

	public void updateIdentificato(BigDecimal identificatoValore, BigDecimal identificatoUtilizzazioni, BigDecimal claimValore, BigDecimal claimUtilizzazioni) {
		this.identificatoValore.accumulateAndGet(identificatoValore, BigDecimal::add);
		this.identificatoUtilizzazioni.accumulateAndGet(identificatoUtilizzazioni, BigDecimal::add);
		this.identificatoRighe.incrementAndGet();
		this.claimValore.accumulateAndGet(claimValore, BigDecimal::add);
		this.claimUtilizzazioni.accumulateAndGet(claimUtilizzazioni, BigDecimal::add);
	}
	
	public void updateNonIdentificato(BigDecimal nonIdentificatoValore, BigDecimal nonIdentificatoUtilizzazioni) {
		this.nonIdentificatoValore.accumulateAndGet(nonIdentificatoValore, BigDecimal::add);
		this.nonIdentificatoUtilizzazioni.accumulateAndGet(nonIdentificatoUtilizzazioni, BigDecimal::add);
		this.nonIdentificatoRighe.incrementAndGet();
	}

	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}
	
}
