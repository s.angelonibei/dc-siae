package com.alkemytech.sophia.royalties.utils;

/**
 * Utility class to perform title and artist normalization (backward)
 * compatible with python scripts.
 * 
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public abstract class SiadaNormalizer {

	private static String unichr(int codePoint) {
		return new String(Character.toChars(codePoint));
	}
	
	public static String normalizeTitle(String title) {
				
		final String[] change = new String[] {
	            "Œ", "",
	            "?", "",
	            "]", "",
	            "[", "",
	            "*", "",
	            "#", "",
	            " & ", " ",
	            unichr(166), unichr(26),
	            unichr(180), unichr(26),
	            unichr(190), unichr(26),
	            unichr(191), "",
	            unichr(64), "E",
	            unichr(224), "A",
	            unichr(225), "A",
	            unichr(226), "A",
	            unichr(227), "A",
	            unichr(228), "A",
	            unichr(229), "A",
	            unichr(230), "A",
	            unichr(192), "A",
	            unichr(193), "A",
	            unichr(194), "A",
	            unichr(195), "A",
	            unichr(196), "A",
	            unichr(197), "A",
	            unichr(232), "E",
	            unichr(233), "E",
	            unichr(234), "E",
	            unichr(235), "E",
	            unichr(200), "E",
	            unichr(201), "E",
	            unichr(202), "E",
	            unichr(203), "E",
	            unichr(236), "I",
	            unichr(237), "I",
	            unichr(238), "I",
	            unichr(239), "I",
	            unichr(204), "I",
	            unichr(205), "I",
	            unichr(206), "I",
	            unichr(207), "A",
	            unichr(242), "O",
	            unichr(243), "O",
	            unichr(244), "O",
	            unichr(245), "O",
	            unichr(246), "O",
	            unichr(210), "O",
	            unichr(211), "O",
	            unichr(212), "O",
	            unichr(213), "O",
	            unichr(214), "O", // CORRETTO! PRIMA ERA unichr(114) = "r"
	            unichr(249), "U",
	            unichr(250), "U",
	            unichr(251), "U",
	            unichr(252), "U",
	            unichr(217), "U",
	            unichr(218), "U",
	            unichr(219), "U",
	            unichr(220), "U",
	            unichr(241), "N",
	            unichr(209), "N",
	            unichr(208), "N",
	            unichr(231), "C",
	            unichr(199), "C",
	            unichr(169), "C",
	            unichr(221), "Y",
	            unichr(253), "Y",
	            unichr(255), "Y",
	            unichr(159), "Y",
	            unichr(216), "O",
	            unichr(248), "O",
	            ",", " ",
	            ":", " ",
	            "+", "",
	            "-", " ",
	            unichr(34), "",
	            "!", " ",
	            "_", " ",
	            "/", " ",
	            " / ", " ",
	    };
		
		// PRIMA DI TUTTO FACCIO LA SOSTITUZIONE DEI SINGOLI CARATTERI SECONDO LE SPECIFICHE SIAE ALTRIMENTI
		// UNA EVENTUALE MODIFICA FATTA PRIMA POTREBBE ALTERARE IL RISULTATO DELL'UTILIZZO DEL DIZIONARIO change
		for (int i = 0; i < change.length; i += 2) {
			title = title.replace(change[i], change[i+1]);
		}

		final String[] init_str_to_remove = new String[] {
                "L.",
                "DER ",
                "GLI ",
                "LES ",
                "UNA ",
                "LOS ",
                "THE ",
                "UNE ",
                "DIE ",
                "DAS ",
                "CA: ",
                "TF: ",
                "AN ",
                "EL ",
                "IL ",
                "LA ",
                "LE ",
                "LO ",
                "NA ",
                "NU ",
                "OS ",
                "SA ",
                "UN ",
                "UN"+unichr(39),
                unichr(39)+"O ",
                unichr(39)+"U ",
                unichr(39)+"A ",
                unichr(39)+"E ",
                "LI ",
                "LU ",
                "ER ",
                "AN ",
                "F: ",
                "CA:",
                "TF:",				
		};
		for (String init : init_str_to_remove) {
			if (title.startsWith(init))
				title = title.substring(init.length());
		}

        title = title.replace(".", " ");
        title = title.replace(unichr(39), " "); // unichr(39) e' il singolo apice

        // RIMUOVE EVENTUALI PARENTESI TONDE E IL LORO CONTENUTO SOSTITUENDO GLI SPAZI MULTIPLI CON SPAZI SINGOLI
        //title = re.sub(' +',' ', re.sub(r'\([^)]*\)', '', title))

        title = title.replaceAll("\\([^)]*\\)", "");
        title = title.replaceAll("\\s+", " ");
        
        title = title.trim();
        title = title.toUpperCase();

        if (title.length() < 1)
        	return "SOPHIANOTITLE";
        
		final int MAX_LEN_TIT = 40;
		if (title.length() > MAX_LEN_TIT) {
			title = title.substring(0,  MAX_LEN_TIT);
		}
        
		return title;
	}
	
	public static String normalizeArtists(String artists) {
		// AGGIUNGE IL SIMBOLO | ALLA FINE DELLA STRINGA
        artists = artists + "|";
		
        // sostituisce i codici ascii indicati da siae con le lettere appropriate
		final String[] change = new String[] {
                unichr(39), "",
                "?", "",
                ";", "|",
                "-", "|",
                unichr(64), "E",
                unichr(224), "A",
                unichr(225), "A",
                unichr(226), "A",
                unichr(227), "A",
                unichr(228), "A",
                unichr(229), "A",
                unichr(230), "A",
                unichr(192), "A",
                unichr(193), "A",
                unichr(194), "A",
                unichr(195), "A",
                unichr(196), "A",
                unichr(197), "A",
                unichr(232), "E",
                unichr(233), "E",
                unichr(234), "E",
                unichr(235), "E",
                unichr(200), "E",
                unichr(201), "E",
                unichr(202), "E",
                unichr(203), "E",
                unichr(236), "I",
                unichr(237), "I",
                unichr(238), "I",
                unichr(239), "I",
                unichr(204), "I",
                unichr(205), "I",
                unichr(206), "I",
                unichr(207), "A",
                unichr(242), "O",
                unichr(243), "O",
                unichr(244), "O",
                unichr(245), "O",
                unichr(246), "O",
                unichr(210), "O",
                unichr(211), "O",
                unichr(212), "O",
                unichr(213), "O",
                unichr(214), "O", // CORRETTO! PRIMA ERA unichr(114) = "r"
                unichr(249), "U",
                unichr(250), "U",
                unichr(251), "U",
                unichr(252), "U",
                unichr(217), "U",
                unichr(218), "U",
                unichr(219), "U",
                unichr(220), "U",
                unichr(241), "N",
                unichr(209), "N",
                unichr(208), "N",
                unichr(231), "C",
                unichr(199), "C",
                unichr(169), "C",
                unichr(221), "Y",
                unichr(253), "Y",
                unichr(255), "Y",
                unichr(159), "Y",
                unichr(216), "O",
                unichr(248), "O",
                ",", "|",
                ". ", "% ",
                ".|", "%|",
                ".", " ",
                "!", " ",
                "_", " ",
            };
		
		for (int i = 0; i < change.length; i += 2) {
			artists = artists.replace(change[i], change[i+1]);
		}

        artists = artists.toUpperCase();
        artists = artists.replace(" WITH |"," WITH|");

        final char[] special_chars = "?¿«€ªæ¥µ³§£¬@²¹".toCharArray();
        String artists_new = null;
        for (String artist : artists.split("\\||,|;| WITH ")) {
        	for (int i = 0; i < special_chars.length; i ++) {
            	artist = artist.replace(new String(special_chars, i, 1), "");
        	}
        	artist = artist.replaceAll("\\([^)]*\\)", "");
            artist = artist.replaceAll("\\s+", " ");
            artist = artist.trim();
            artists_new = null == artists_new ?
            		artist : artists_new + "|" + artist;
        }

        if (artists_new.length() < 1)
        	return "SOPHIANOARTISTS";
        
		
		return artists_new;
	}
	
	
	public static void main(String[] args) {
		System.out.println(normalizeTitle("Non è bello quel (ciò) che è bello ma e' bello ciò che piace"));
		System.out.println(normalizeArtists("Franck lo-giudice|The pink ponk|a.b|ta-dà"));
	}
	
	
}
