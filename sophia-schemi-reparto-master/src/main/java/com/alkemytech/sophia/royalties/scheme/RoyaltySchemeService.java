package com.alkemytech.sophia.royalties.scheme;

import com.alkemytech.sophia.commons.text.TextNormalizer;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.royalties.Configuration;
import com.alkemytech.sophia.royalties.nosql.*;
import com.alkemytech.sophia.royalties.role.RoleCatalog;
import com.google.common.base.Strings;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class RoyaltySchemeService {

    private static final Logger logger = LoggerFactory.getLogger(RoyaltySchemeService.class);

    private final RoleCatalog roleCatalog;
    private final TemporaryWorkCatalog temporaryWorkCatalog;
    private final IrregularityCatalog irregularityCatalog;
    private final Set<String> codiciContoNoIpnamenrNoSiae;
    private final Set<String> codiciContoSospesoPerEditore;
    private final Set<String> codiciContoPublicDomain;
    private final Set<Long> ipiNumbersPublicDomain;
    private final String notMemberSociety;
    private final boolean noOverlappingTerritories;
    private final boolean noMissingIpnamenr;
    private final Map<String, String> formatiQualifiche;
    private final Map<String, String> ovverideTerritori;
    private final String formatoDefaultQualifiche;
    private final boolean debug;

    @Inject
    protected RoyaltySchemeService(Configuration configuration,
                                   RoleCatalog roleCatalog,
                                   TemporaryWorkCatalog temporaryWorkCatalog,
                                   IrregularityCatalog irregularityCatalog) {
        super();
        this.roleCatalog = roleCatalog;
        this.temporaryWorkCatalog = temporaryWorkCatalog;
        this.irregularityCatalog = irregularityCatalog;
        this.codiciContoNoIpnamenrNoSiae = parseStringCsvProperty(configuration,
                "royalty_service.codice_conto.no_ipnamenr_no_siae", "09,55,56", null);
        this.codiciContoSospesoPerEditore = parseStringCsvProperty(configuration,
                "royalty_service.codice_conto.sospeso_per_editore", "85", null);
        this.codiciContoPublicDomain = parseStringCsvProperty(configuration,
                "royalty_service.codice_conto.public_domain", "10,65,78", null);
        this.ipiNumbersPublicDomain = parseLongCsvProperty(configuration,
                "royalty_service.ipi_number.public_domain", "00039657154");
        this.notMemberSociety = configuration
                .getProperty("royalty_service.not_member_society", "NM");
        this.noOverlappingTerritories = "true".equalsIgnoreCase(configuration
                .getProperty("royalty_service.no_overlapping_territories", "true"));
        this.noMissingIpnamenr = "true".equalsIgnoreCase(configuration
                .getProperty("royalty_service.no_missing_ipnamenr", "true"));
        this.formatiQualifiche = new ConcurrentHashMap<>(GsonUtils
                .decodeJsonMap(configuration.getProperty("royalty_service.role_format.mapping")));
        this.formatoDefaultQualifiche = configuration
                .getProperty("royalty_service.role_format.default", "CISAC");
        this.debug = "true".equals(configuration.getProperty("royalty_service.debug",
                configuration.getProperty("default.debug")));
        this.ovverideTerritori = new ConcurrentHashMap<>(GsonUtils.decodeJsonMap(
                configuration.getProperty("royalty_service.override_territori", "{}")
        ));
    }

    private Set<String> parseStringCsvProperty(Configuration configuration, String propertyName, String propertyDefault, TextNormalizer normalizer) {
        final Set<String> values = Collections
                .newSetFromMap(new ConcurrentHashMap<String, Boolean>());
        for (String value : configuration
                .getProperty(propertyName, propertyDefault).split(",")) {
            values.add(null != normalizer ?
                    normalizer.normalize(value) : value);
        }
        return values;
    }

    private Set<Long> parseLongCsvProperty(Configuration configuration, String propertyName, String propertyDefault) {
        final Set<Long> values = Collections
                .newSetFromMap(new ConcurrentHashMap<Long, Boolean>());
        for (String value : configuration
                .getProperty(propertyName, propertyDefault).split(",")) {
            values.add(Long.parseLong(value));
        }
        return values;
    }

    private String getRoleFormat(String societa) {
        String roleFormat = formatiQualifiche.get(societa);
        return null == roleFormat ? formatoDefaultQualifiche : roleFormat;
    }

    // regole speciali per la ricerca:
    //  1. se non ho "codiceIpi" ma ho "posizioneSiae" e il "codiceConto" NON è [09, 55, 56] allora è tutelato SIAE
    //  2. per tutti i casi in cui non trovo nessuna società di tutela la quota va etichettata come NOT MEMBER (società NM)
    public RoyaltyScheme lookup(IpiSocNoSqlDb ipiSocNoSql, OperaNoSqlDb operaNoSql, String societa, String codiceOpera, String territorio) throws RoyaltySchemeException {

        final Territories territories;
        if (ovverideTerritori.containsKey(territorio)) {
            territories = new Territories(ovverideTerritori.get(territorio), true);
        } else {
            territories = new Territories(territorio, true);
        }

        // opere provvisorie
        if (temporaryWorkCatalog.isTemporaryWork(societa, codiceOpera)) {
            if (debug) {
                logger.debug("codice provvisorio {} {} {}", societa, codiceOpera, territories.toString());
            }
            return temporaryWorkCatalog.getRoyaltyScheme(societa,
                    codiceOpera, territories);
        }

        // lookup opera
        final Opera opera = operaNoSql.get(codiceOpera);
//		logger.debug("opera {}", opera);
        if (null == opera) {
            return null;
        }

        // cerca schema per territorio
        final Set<SchemaRiparto> schemi = opera.schemi;
        if (null == schemi || schemi.isEmpty()) {
            return null;
        }
        SchemaRiparto matchedSchema = null;
        Territories matchedTerritories = null;
        for (SchemaRiparto schema : schemi) {
            final Territories ovelapping = territories
                    .intersection(schema.territories);
            if (null != ovelapping && !ovelapping.isEmpty()) {
                if (null != matchedSchema) {
                    if (noOverlappingTerritories) {
                        throw new RoyaltySchemeException("overlapping territories " + territories + " for work " + codiceOpera);
                    }
                }
                matchedSchema = schema;
                matchedTerritories = ovelapping;
                if (!noOverlappingTerritories) {
                    break;
                }
            }
        }
        if (debug) {
            logger.debug("matchedSchema {}", matchedSchema);
            logger.debug("matchedTerritories {}", matchedTerritories);
        }
        if (null == matchedSchema || null == matchedTerritories) {
            return null;
        }

        // schema irregolare
        if (matchedSchema.isIrregular()) {
            if (debug) {
                logger.debug("schema irregolare {} {} {} {}", societa, codiceOpera, territories.toString(), matchedSchema);
            }
            return irregularityCatalog.convert(societa, matchedSchema, matchedTerritories);
        }

        // role format
        final String roleFormat = getRoleFormat(societa);

        // aggiunge la società alle quote
        float demTotal = 0f;
        float drmTotal = 0f;
        final RoyaltyScheme royaltyScheme = new RoyaltyScheme()
                .setTerritories(matchedTerritories);

        for (QuotaRiparto quota : matchedSchema.quote) {
            if (String.valueOf(matchedSchema.idSchemaRiparto).equalsIgnoreCase("364360693")) {
                logger.debug("IDSCHEMA , QUOTARIPARTO " + matchedSchema.idSchemaRiparto + " , " + quota.toString());
            }
            final String rol = roleCatalog.convert(roleFormat, quota.tipoQualifica, "IPI");
            final float shares = quota.percentuale / 100f; // [0, 1]
            boolean added = false;
            if ((null != quota.codiceIpi &&
                    ipiNumbersPublicDomain.contains(quota.codiceIpi)) ||
                    (!Strings.isNullOrEmpty(quota.codiceConto) &&
                            codiciContoPublicDomain.contains(quota.codiceConto))) {
                // pubblico dominio
                royaltyScheme.addRoyalty(new Royalty()
                        .setCodiceConto(quota.codiceConto)
                        .setCodiceContoEstero(quota.codiceContoEstero)
                        .setCodiceIpi(quota.codiceIpi)
                        .setCognome(quota.cognome)
                        .setIdRepertorio(quota.idRepertorio)
                        .setIdTipoIrregolarita(quota.idTipoIrregolarita)
                        .setPosizioneSiae(quota.posizioneSiae)
                        .setPercentuale(quota.percentuale)
                        .setNumeratore(quota.numeratore)
                        .setDenominatore(quota.denominatore)
                        .setTipoQualifica(quota.tipoQualifica)
                        .setFormatoQualifica(roleFormat)
                        .setTipoQuota(quota.tipoQuota)
                        .setSocshr(100f)
                        .setSocname(notMemberSociety)
                        .setShares(shares)
                        .setDoNotClaim(roleCatalog.isDoNotClaimRole(roleFormat, quota.tipoQualifica))
                        .setPublicDomain(true)); // flag public domain
                added = true;
            } else if (null == quota.codiceIpi) {
                if (!Strings.isNullOrEmpty(quota.codiceConto)) {
                    if (codiciContoSospesoPerEditore.contains(quota.codiceConto)) {
                        // conti speciali: sospeso per editore (conto 85)
                        royaltyScheme.addRoyalty(new Royalty()
                                .setCodiceConto(quota.codiceConto)
                                .setCodiceContoEstero(quota.codiceContoEstero)
                                .setCodiceIpi(quota.codiceIpi)
                                .setCognome(quota.cognome)
                                .setIdRepertorio(quota.idRepertorio)
                                .setIdTipoIrregolarita(quota.idTipoIrregolarita)
                                .setPosizioneSiae(quota.posizioneSiae)
                                .setPercentuale(quota.percentuale)
                                .setNumeratore(quota.numeratore)
                                .setDenominatore(quota.denominatore)
                                .setTipoQualifica(quota.tipoQualifica)
                                .setFormatoQualifica(roleFormat)
                                .setTipoQuota(quota.tipoQuota)
                                .setSocshr(100f)
                                .setSocname(societa)
                                .setShares(shares)
                                .setDoNotClaim(roleCatalog.isDoNotClaimRole(roleFormat, quota.tipoQualifica))
                                .setSospesoPerEditore(true)); // flag sospeso per editore
                        added = true;
                    } else {
                        //if (!Strings.isNullOrEmpty(quota.posizioneSiae)) {
                        // conti speciali: mancanza codice ipi ma posizione siae disponibile
                        // rimosso il controllo dei conti perché prendeva conti errati 06 07...
//                        if (!codiciContoNoIpnamenrNoSiae.contains(quota.codiceConto)) { // WARNING: logica negativa
                            royaltyScheme.addRoyalty(new Royalty()
                                    .setCodiceConto(quota.codiceConto)
                                    .setCodiceContoEstero(quota.codiceContoEstero)
                                    .setCodiceIpi(quota.codiceIpi)
                                    .setCognome(quota.cognome)
                                    .setIdRepertorio(quota.idRepertorio)
                                    .setIdTipoIrregolarita(quota.idTipoIrregolarita)
                                    .setPosizioneSiae(quota.posizioneSiae)
                                    .setPercentuale(quota.percentuale)
                                    .setNumeratore(quota.numeratore)
                                    .setDenominatore(quota.denominatore)
                                    .setTipoQualifica(quota.tipoQualifica)
                                    .setFormatoQualifica(roleFormat)
                                    .setTipoQuota(quota.tipoQuota)
                                    .setSocshr(100f)
                                    .setSocname(notMemberSociety)
                                    .setShares(shares)
                                    .setDoNotClaim(roleCatalog.isDoNotClaimRole(roleFormat, quota.tipoQualifica)));
                            added = true;
                        //}
                    }
                }
            } else if (!Strings.isNullOrEmpty(rol)) {
                final IpiSoc ipiSoc = ipiSocNoSql.get(quota.codiceIpi);
                if (null != ipiSoc) {
                    final List<Society> societies = ipiSoc
                            .get(quota.tipoQuota, rol, territories);
                    float shrTotal = 0f;
                    if (null != societies && !societies.isEmpty()) {
                        for (Society society : societies) {
                            royaltyScheme.addRoyalty(new Royalty()
                                    .setCodiceConto(quota.codiceConto)
                                    .setCodiceContoEstero(quota.codiceContoEstero)
                                    .setCodiceIpi(quota.codiceIpi)
                                    .setCognome(quota.cognome)
                                    .setIdRepertorio(quota.idRepertorio)
                                    .setIdTipoIrregolarita(quota.idTipoIrregolarita)
                                    .setPosizioneSiae(quota.posizioneSiae)
                                    .setPercentuale(quota.percentuale)
                                    .setNumeratore(quota.numeratore)
                                    .setDenominatore(quota.denominatore)
                                    .setTipoQualifica(quota.tipoQualifica)
                                    .setFormatoQualifica(null == quota.tipoQualifica ? null : roleFormat)
                                    .setTipoQuota(quota.tipoQuota)
                                    .setSocshr(society.shr)
                                    .setSocname(society.socname)
                                    .setShares(shares * society.shr / 100f)
                                    .setDoNotClaim(roleCatalog.isDoNotClaimRole(roleFormat, quota.tipoQualifica))
                                    .setSospesoPerEditore(false));
                            added = true;
                            shrTotal += society.shr;
                        }
                    }
                    // se sum(society.shr) < 100 crea quota virtuale non socio con la parte rimanente
                    if (shrTotal <= 99f) {
                        final float socshr = 100f - shrTotal;
                        royaltyScheme.addRoyalty(new Royalty()
                                .setCodiceConto(quota.codiceConto)
                                .setCodiceContoEstero(quota.codiceContoEstero)
                                .setCodiceIpi(quota.codiceIpi)
                                .setCognome(quota.cognome)
                                .setIdRepertorio(quota.idRepertorio)
                                .setIdTipoIrregolarita(quota.idTipoIrregolarita)
                                .setPosizioneSiae(quota.posizioneSiae)
                                .setPercentuale(quota.percentuale)
                                .setNumeratore(quota.numeratore)
                                .setDenominatore(quota.denominatore)
                                .setTipoQualifica(quota.tipoQualifica)
                                .setFormatoQualifica(roleFormat)
                                .setTipoQuota(quota.tipoQuota)
                                .setSocshr(socshr)
                                .setSocname(notMemberSociety)
                                .setShares(shares * socshr / 100f)
                                .setDoNotClaim(roleCatalog.isDoNotClaimRole(roleFormat, quota.tipoQualifica))
                                .setSospesoPerEditore(false));
                        added = true;
                    }
                } else {
                    if (noMissingIpnamenr) {
                        throw new RoyaltySchemeException("ipnamenr not found " + quota.codiceIpi + " for work " + codiceOpera);
                    }
                }
            }
            if (added) {
                if ("DEM".equals(quota.tipoQuota)) {
                    demTotal += shares;
                } else {
                    drmTotal += shares;
                }
            }
        }

        // aggiungi quote virtuali non socio se la somma è minore di 100
//		if (demTotal <= 99f) {
//			final float shares = 1f - demTotal;
//			royalties.add(new WorkRoyalty()
//					.setCodiceConto(null)
//					.setCodiceContoEstero(null)
//					.setCodiceIpi(null)
//					.setCognome(null)
//					.setIdRepertorio(0L)
//					.setIdTipoIrregolarita(0L)
//					.setPosizioneSiae(null)
//					.setPercentuale(shares * 100f)
//					.setTipoQualifica(null)
//					.setFormatoQualifica(roleFormat)
//					.setTipoQuota("DEM")
//					.setSocshr(100f)
//					.setSocname(notMemberSociety)
//					.setShares(shares)
//					.setContoSpeciale(false));
//			demTotal = 1f;
//		}
//		if (drmTotal <= 99f) {
//			final float shares = 1f - drmTotal;
//			royalties.add(new WorkRoyalty()
//					.setCodiceConto(null)
//					.setCodiceContoEstero(null)
//					.setCodiceIpi(null)
//					.setCognome(null)
//					.setIdRepertorio(0L)
//					.setIdTipoIrregolarita(0L)
//					.setPosizioneSiae(null)
//					.setPercentuale(shares * 100f)
//					.setTipoQualifica(null)
//					.setFormatoQualifica(roleFormat)
//					.setTipoQuota("DRM")
//					.setSocshr(100f)
//					.setSocname(notMemberSociety)
//					.setShares(shares)
//					.setContoSpeciale(false));
//			drmTotal = 1f;
//		}

        // totale quote
        royaltyScheme
                .setDemTotal(demTotal)
                .setDrmTotal(drmTotal);

        return royaltyScheme;
    }

}