package com.alkemytech.sophia.royalties.country;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.io.CompressionAwareFileInputStream;
import com.alkemytech.sophia.commons.io.CompressionAwareFileOutputStream;
import com.google.common.base.Strings;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public abstract class CountryService {
		
	private static final Logger logger = LoggerFactory.getLogger(CountryService.class);

	private static final String TIS_N = "tis_n";
	private static final String TIS_A = "tis_a";
	private static final String TIS_A_EXT = "tis_a_ext";
	private static final String NOME = "nome";
	private static final String COUNTRIES = "countries";

	protected final Map<Long, Set<Country>> countries;
	protected final Map<String, Long> names;
	protected final Map<String, String> toAlpha2;
	protected final Map<String, String> toAlpha3;
	protected final Charset charset;

	protected CountryService(Charset charset) {
		super();
		this.countries = new HashMap<>();
		this.names = new ConcurrentHashMap<>();
		this.toAlpha2 = new ConcurrentHashMap<>();
		this.toAlpha3 = new ConcurrentHashMap<>();
		this.charset = charset;
	}
	
	public void load(File file) throws IOException {
		try (final CompressionAwareFileInputStream in = new CompressionAwareFileInputStream(file)) {
			load(in);
		}
	}

	public synchronized CountryService load(InputStream in) throws IOException {

//		[
//			{
//				"tis_n": 273,
//				"nome": "WORLD",
//				"countries": [
//					{
//						"tis_n": 17,
//						"tis_a": "IT",
//						"tis_a_ext": "ITA",
//						"nome": "ITALIA",
//					},
//					...
//				]
//			},
//			...
//		]
		
		countries.clear();
		names.clear();
		toAlpha2.clear();
		toAlpha3.clear();
		
		try (final Reader reader = new InputStreamReader(in, charset);
				final JsonReader jsonReader = new JsonReader(reader)) {
				
			long tisnr;
			String nome;
			Set<Country> countrySet;
			
			jsonReader.beginArray();
			while (jsonReader.hasNext()) {
				jsonReader.beginObject();
				tisnr = 0L;
				nome = null;
				countrySet = null;
				while (jsonReader.hasNext()) {
	                String name = jsonReader.nextName();
	                if (name.equals(TIS_N)) {
	                	tisnr = jsonReader.nextLong();
	                } else if (name.equals(NOME)) {
	                	nome = jsonReader.nextString();
	                } else if (name.equals(COUNTRIES)) {
						jsonReader.beginArray();
						while (jsonReader.hasNext()) {
							jsonReader.beginObject();
							final Country country = new Country();
							while (jsonReader.hasNext()) {
				                name = jsonReader.nextName();
				                if (name.equals(TIS_N)) {
				                	country.setTisN(jsonReader.nextLong());
				                } else if (name.equals(TIS_A)) {
				                	country.setTisA(jsonReader.nextString());
				                } else if (name.equals(TIS_A_EXT)) {
				                	country.setTisAExt(jsonReader.nextString());
				                } else if (name.equals(NOME)) {
				                	country.setNome(jsonReader.nextString());
				                } else {
				                	jsonReader.skipValue();
				                }
							}
							jsonReader.endObject();
							if (country.getTisN() > 0L &&
									!Strings.isNullOrEmpty(country.getTisA()) &&
									!Strings.isNullOrEmpty(country.getTisAExt()) &&
									!Strings.isNullOrEmpty(country.getNome())) {
								if (null == countrySet) {
									countrySet = new HashSet<>();
								}
								countrySet.add(country);
							}
						}
						jsonReader.endArray();
	                } else {
	                	jsonReader.skipValue();
	                }
				}
				jsonReader.endObject();
				if (tisnr > 0L && null != countrySet) {
					countries.put(tisnr, countrySet);
					if (!Strings.isNullOrEmpty(nome)) {
						names.put(nome, tisnr);
					}
					for (Country country : countrySet) {
						toAlpha2.put(country.getTisA(), country.getTisA());
						toAlpha2.put(country.getTisAExt(), country.getTisA());
						toAlpha3.put(country.getTisA(), country.getTisAExt());
						toAlpha3.put(country.getTisAExt(), country.getTisAExt());
					}
				}
			}
			jsonReader.endArray();
			
		}

		logger.debug("countries: {}", countries);
		logger.debug("names: {}", names);
		logger.debug("toAlpha2: {}", toAlpha2);
		logger.debug("toAlpha3: {}", toAlpha3);

		return this;
	}

	public CountryService save(File file) throws IOException {
		try (final CompressionAwareFileOutputStream out = new CompressionAwareFileOutputStream(file)) {
			return save(out);
		}
	}
	
	public synchronized CountryService save(OutputStream out) throws IOException {
		try (final Writer writer = new OutputStreamWriter(out, charset);
				final JsonWriter jsonWriter = new JsonWriter(writer)) {
			
			jsonWriter.beginArray();
			for (Map.Entry<String, Long> entry : names.entrySet()) {
				final Set<Country> countrySet = countries.get(entry.getValue());
				jsonWriter.beginObject();
				jsonWriter.name(NOME).value(entry.getKey());
				jsonWriter.name(TIS_N).value(entry.getValue());
				jsonWriter.name(COUNTRIES);
				jsonWriter.beginArray();
				for (Country country : countrySet) {
					jsonWriter.beginObject();
					jsonWriter.name(TIS_N).value(country.getTisN());
					jsonWriter.name(TIS_A).value(country.getTisA());
					jsonWriter.name(TIS_A_EXT).value(country.getTisAExt());
					jsonWriter.name(NOME).value(country.getNome());
					jsonWriter.endObject();
				}
				jsonWriter.endArray();
				jsonWriter.endObject();
			}
			jsonWriter.endArray();
			
		}
		
		return this;
	}
	
	public long getIdTerritorio(String nome) {
		return getIdTerritorio(nome, -1L);
	}
	
	public long getIdTerritorio(String nome, long ifNull) {
		final Long tisnr = names.get(nome);
		return null == tisnr ? ifNull : tisnr.longValue();
	}

	public synchronized Set<Country> getCountries(long idTerritorio) {
		Set<Country> result = countries.get(idTerritorio);
		if (null == result) {
			result = new HashSet<Country>();
			countries.put(idTerritorio, result);
			logger.debug("unknown country tis number: {}", idTerritorio);
		}
		return result;
	}
	
	public String getAlpha2(String code) {
		return toAlpha2.get(code);
	}

	public String getAlpha3(String code) {
		return toAlpha3.get(code);
	}

}
