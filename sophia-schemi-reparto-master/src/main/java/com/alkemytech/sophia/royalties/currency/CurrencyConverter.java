package com.alkemytech.sophia.royalties.currency;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.sql.DataSource;

import com.alkemy.siae.sophia.pricing.DsrMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.royalties.Configuration;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class CurrencyConverter {

	private static final Logger logger = LoggerFactory.getLogger(CurrencyConverter.class);

	private final Configuration configuration;
	private final DataSource dataSource;
	private final Map<String, BigDecimal> rates;

	@Inject
	protected CurrencyConverter(Configuration configuration,
			@Named("MCMDB") DataSource dataSource) {
		super();
		this.configuration = configuration;
		this.dataSource = dataSource;
		this.rates = new ConcurrentHashMap<>();
	}
	
	public CurrencyConverter reloadRates() throws SQLException {
		rates.clear();
		try (final Connection connection = dataSource.getConnection()) {
			logger.debug("jdbc connected to {} {}", connection.getMetaData()
					.getDatabaseProductName(), connection.getMetaData().getURL());
			try (final Statement statement = connection.createStatement()) {
				final String sql = configuration
						.getProperty("currency_converter.sql.select_currency_rate");
				logger.debug("executing query {}", sql);
				try (final ResultSet resultSet = statement.executeQuery(sql)) {
					while (resultSet.next()) {
						rates.put(resultSet.getString("src_currency") +
								resultSet.getString("dst_currency"), 
								new BigDecimal(resultSet.getString("rate")));				
					}
				}
			}
		}
		logger.debug("rates: {}", rates);
		return this;
	}
	
	public BigDecimal convert(String srcCurrency, BigDecimal srcValue, String dstCurrency) {
		final BigDecimal rate = rates.get(srcCurrency + dstCurrency);
		return null == rate ? BigDecimal.ZERO : srcValue.multiply(rate);
	}


	public String getDspReclami(DsrMetadata dsrMetadata) throws SQLException {
		String result = null;
		try (final Connection connection = dataSource.getConnection()) {
			logger.debug("jdbc connected to {} {}", connection.getMetaData()
					.getDatabaseProductName(), connection.getMetaData().getURL());
			try (final Statement statement = connection.createStatement()) {
				final String sql = "SELECT DSP.CODE dspReclami FROM INVOICE_RECLAMO IR JOIN ANAG_DSP DSP ON replace(replace(DSP.IDDSP,'(',''),')','') = IR.ID_DSP WHERE IR.ID_DSR ='"+dsrMetadata.getIdDsr()+ "'";
				logger.debug("executing query {}", sql);
				try (final ResultSet resultSet = statement.executeQuery(sql)) {
					while (resultSet.next()) {
						dsrMetadata.setDspReclami( resultSet.getString("dspReclami"));
						break;
					}
				}
			}
		}
		logger.debug("dspReclami: ", result);
		return result;
	}

}
