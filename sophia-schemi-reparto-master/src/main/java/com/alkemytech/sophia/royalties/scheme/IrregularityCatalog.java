package com.alkemytech.sophia.royalties.scheme;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.QuoteMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.io.CompressionAwareFileInputStream;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.royalties.nosql.QuotaRiparto;
import com.alkemytech.sophia.royalties.nosql.SchemaRiparto;
import com.alkemytech.sophia.royalties.nosql.Territories;
import com.amazonaws.services.s3.model.S3Object;
import com.google.common.base.Strings;
import com.google.gson.GsonBuilder;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class IrregularityCatalog {
	
	private static final Logger logger = LoggerFactory.getLogger(IrregularityCatalog.class);

	private final Charset charset;
	private final Map<String, Map<String, RoyaltyScheme>> irregularityMap;
	private final Map<String, Map<String, String>> priorityMap;

	@Inject
	public IrregularityCatalog(@Named("charset") Charset charset) {
		super();
		this.charset = charset;
		this.irregularityMap = new ConcurrentHashMap<>();
		this.priorityMap = new ConcurrentHashMap<>();
	}
	
	public IrregularityCatalog load(S3 s3, String url) throws IOException {
		try (final S3Object s3object = s3.getObject(new S3.Url(url));
				final InputStream in = s3object.getObjectContent()) {
			return load(in);
		}
	}

	public IrregularityCatalog load(File file) throws IOException {
		try (CompressionAwareFileInputStream in = new CompressionAwareFileInputStream(file)) {
			return load(in);
		}
	}

	public synchronized IrregularityCatalog load(InputStream in) throws IOException {
		
		irregularityMap.clear();

		// parse csv file
		try (final Reader reader = new InputStreamReader(in, charset);
				final CSVParser csvParser = new CSVParser(reader, CSVFormat.newFormat(';')
						.withIgnoreEmptyLines()
						.withIgnoreSurroundingSpaces()
						.withQuoteMode(QuoteMode.MINIMAL)
						.withQuote('"'))) {
						
			final Iterator<CSVRecord> csvIterator = csvParser.iterator();
			
			// skip headers
			csvIterator.next();
			
			while (csvIterator.hasNext()) {
				final CSVRecord record = csvIterator.next();
				
				// SOCIETA;ID_TIPO_IRREGOLARITA;PRIORITA;DESCRIZIONE;ID_REPERTORIO;POSIZIONE_DEM;POSIZIONE_DRM;CONTO_DEM;CONTO_DRM;PERCENTUALE_DEM;PERCENTUALE_DRM;CODICE_IPI;TIPO_QUALIFICA;CODICE_QUALIFICA;SOCIETA_IPI;SHARE_IPI;TERRITORI;CODICE_ESEMPIO
				final String societa = record.get(0);
				final String idTipoIrregolarita = record.get(1);
				final String priorita = record.get(2);
//				final String descrizione = record.get(3);
				final String idRepertorio = record.get(4);
				final String posizioneDem = record.get(5);
				final String posizioneDrm = record.get(6);
				final String contoDem = record.get(7);
				final String contoDrm = record.get(8);
				final String percentualeDem = record.get(9);
				final String percentualeDrm = record.get(10);
				final String codiceIpi = record.get(11);
				final String tipoQualifica = record.get(12);
				final String formatoQualifica = record.get(13);
				final String societaIpi = record.get(14);
				final String shareIpi = record.get(15);
				final String territori = record.get(16);
//				final String codiceEsempio = record.get(17);

				if (!Strings.isNullOrEmpty(societa) &&
						!Strings.isNullOrEmpty(idTipoIrregolarita)) {
					
					// priority map
					if (!Strings.isNullOrEmpty(priorita)) {
						Map<String, String> idTipoIrregolaritaMap = priorityMap.get(societa);
						if (null == idTipoIrregolaritaMap) {
							idTipoIrregolaritaMap = new ConcurrentHashMap<>();
							priorityMap.put(societa, idTipoIrregolaritaMap);
						}
						idTipoIrregolaritaMap.put(idTipoIrregolarita, priorita);
					}
					
					// scheme map
					if (!Strings.isNullOrEmpty(territori)) {
						Map<String, RoyaltyScheme> idTipoIrregolaritaMap = irregularityMap.get(societa);
						if (null == idTipoIrregolaritaMap) {
							idTipoIrregolaritaMap = new ConcurrentHashMap<>();
							irregularityMap.put(societa, idTipoIrregolaritaMap);
						}
						final RoyaltyScheme royaltyScheme = new RoyaltyScheme()
								.setTerritories(new Territories(TextUtils.remove(territori, '|'), true));	
						final float socshr = Strings.isNullOrEmpty(shareIpi) ?
								0f : Float.parseFloat(shareIpi);
						final float sharesDem = Strings.isNullOrEmpty(percentualeDem) ?
								0f : Float.parseFloat(percentualeDem);
						final float sharesDrm = Strings.isNullOrEmpty(percentualeDrm) ?
								0f : Float.parseFloat(percentualeDrm);
						royaltyScheme.addRoyalty(new Royalty()
								.setCodiceIpi(Strings.isNullOrEmpty(codiceIpi) ?
										null : Long.parseLong(codiceIpi))
								.setPosizioneSiae(posizioneDem)
								.setTipoQualifica(tipoQualifica)
								.setFormatoQualifica(formatoQualifica)
								.setTipoQuota("DEM")
								.setPercentuale(sharesDem)
//								.setCognome(descrizione)
								.setCodiceConto(contoDem)
//								.setCodiceContoEstero(null)
								.setIdRepertorio(Strings.isNullOrEmpty(idRepertorio) ?
										0L : Long.parseLong(idRepertorio))
								.setIdTipoIrregolarita(idTipoIrregolarita)
								.setSocname(societaIpi)
								.setSocshr(socshr)
								.setShares((sharesDem / 100f) * (socshr / 100f))
								.setSospesoPerEditore(false));
						royaltyScheme.addRoyalty(new Royalty()
								.setCodiceIpi(Strings.isNullOrEmpty(codiceIpi) ?
										null : Long.parseLong(codiceIpi))
								.setPosizioneSiae(posizioneDrm)
								.setTipoQualifica(tipoQualifica)
								.setFormatoQualifica(formatoQualifica)
								.setTipoQuota("DRM")
								.setPercentuale(sharesDrm)
//								.setCognome(descrizione)
								.setCodiceConto(contoDrm)
//								.setCodiceContoEstero(null)
								.setIdRepertorio(Strings.isNullOrEmpty(idRepertorio) ?
										0L : Long.parseLong(idRepertorio))
								.setIdTipoIrregolarita(idTipoIrregolarita)
								.setSocname(societaIpi)
								.setSocshr(socshr)
								.setShares((sharesDrm / 100f) * (socshr / 100f))
								.setSospesoPerEditore(false));
						royaltyScheme.setDemTotal(sharesDem / 100f);
						royaltyScheme.setDrmTotal(sharesDrm / 100f);
						idTipoIrregolaritaMap.put(idTipoIrregolarita, royaltyScheme);
					}

				}
			}
			
		}
		logger.debug("irregularities: {}", irregularityMap);
		
		return this;
	}
	
	public RoyaltyScheme convert(String societa, SchemaRiparto schemaRiparto, Territories territories) {
		
		// search id_tipo_irregolarita with largest priority
		final Map<String, String> priorityByType = priorityMap.get(societa);
		if (null == priorityByType) {
			throw new IllegalArgumentException("no priority configured for society " + societa);
		}
		String idTipoIrregolarita = null;
		String priorita = null;
		for (QuotaRiparto quota : schemaRiparto.quote) {
			if (null != quota.idTipoIrregolarita && !"".equals(quota.idTipoIrregolarita)) {
				final String nuovaPriorita = priorityByType.get(quota.idTipoIrregolarita);
				if (null != nuovaPriorita) {
					if (null == priorita || nuovaPriorita.compareTo(priorita) > 0) {
						priorita = nuovaPriorita;
						idTipoIrregolarita = quota.idTipoIrregolarita;
					}
				}
			}
		}		
		if (null == idTipoIrregolarita) {
			throw new IllegalArgumentException("no irregularity found in schema");
		}
//		logger.debug("idTipoIrregolarita {}", idTipoIrregolarita);
//		logger.debug("priorita {}", priorita);
		
		// search royalty scheme for selected id_tipo_irregolarita
		final Map<String, RoyaltyScheme> irregularityByType = irregularityMap.get(societa);
		if (null == irregularityByType) {
			throw new IllegalArgumentException("no irregularity configured for society " + societa);
		}
		final RoyaltyScheme royaltyScheme = irregularityByType.get(idTipoIrregolarita);
		if (null == royaltyScheme) {
			throw new IllegalArgumentException("no scheme found for irregularity " + idTipoIrregolarita);
		}
//		logger.debug("royaltyScheme {}", royaltyScheme);
		territories = territories.intersection(royaltyScheme.territories);
		if (null == territories || territories.isEmpty()) {
			return null;
		}
//		logger.debug("territories {}", territories);
		
		return new RoyaltyScheme()
				.setTerritories(territories)
				.addRoyalties(royaltyScheme.royalties)
				.setDemTotal(royaltyScheme.demTotal)
				.setDrmTotal(royaltyScheme.drmTotal)
				.setIrregularWork(true);
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}
	
}
