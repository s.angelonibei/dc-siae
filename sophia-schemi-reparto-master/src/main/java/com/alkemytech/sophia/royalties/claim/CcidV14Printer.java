package com.alkemytech.sophia.royalties.claim;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.alkemytech.sophia.royalties.filenaming.MMSpecialCharCcid;
import com.google.common.base.Strings;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemy.siae.sophia.pricing.DsrLine;
import com.alkemy.siae.sophia.pricing.DsrMetadata;
import com.alkemytech.sophia.commons.util.BigDecimals;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class CcidV14Printer extends CcidPrinter {

    private static final Logger logger = LoggerFactory.getLogger(CcidV14Printer.class);

    public static final char DELIMITER = '\t';

    public CcidV14Printer(CSVPrinter csvPrinter) {
        super(csvPrinter);
    }

    private String toString(BigDecimal number, int decimalPlaces) {
        return number.setScale(decimalPlaces, BigDecimal.ROUND_HALF_UP)
                .toPlainString();
    }

    private String smartTrim(String decimal) {
        final int dot = 1 + decimal.indexOf('.');
        if (0 == dot) {
            return decimal;
        }
        if (dot == decimal.length()) {
            return decimal;
        }
        final char[] array = decimal.toCharArray();
        int length = array.length - 1;
        for (; length > dot && '0' == array[length]; length--) ;
        return new String(array, 0, length + 1);
    }

    private String getCommercialModel(DsrMetadata dsrMetadata) {
        if ("STRM_AD".equalsIgnoreCase(dsrMetadata.getPricingModel())) {
            return "AdvertisementSupportedModel";
        } else if ("STRM_SUB".equalsIgnoreCase(dsrMetadata.getPricingModel())) {
            return "SubscriptionModel";
        } else if ("DWN".equalsIgnoreCase(dsrMetadata.getPricingModel())) {
            return "PayAsYouGoModel";
        }
        return "Unknown";
    }

    private String getUseType(DsrMetadata dsrMetadata) {
        if ("PI".equalsIgnoreCase(dsrMetadata.getUseType())) {
            return "ContentInfluencedStream";
        } else if ("PL".equalsIgnoreCase(dsrMetadata.getUseType())) {
            return "ConditionalDownload";
        } else if ("PD".equalsIgnoreCase(dsrMetadata.getUseType())) {
            return "PermanentDownload";
        } else if ("LD".equalsIgnoreCase(dsrMetadata.getUseType())) {
            return "Download";
        } else if ("ST".equalsIgnoreCase(dsrMetadata.getUseType())) {
            // return "NonInteractiveStream";
            return "Stream";
        } else if ("SD".equalsIgnoreCase(dsrMetadata.getUseType())) {
            return "OnDemandStream";
        } else if ("TD".equalsIgnoreCase(dsrMetadata.getUseType())) {
            return "TetheredDownload";
        } else if ("TM".equalsIgnoreCase(dsrMetadata.getUseType())) {
            return "TetheredDownload";
        } else if ("PW".equalsIgnoreCase(dsrMetadata.getUseType())) {
            return "Webcast";
        } else if ("IW".equalsIgnoreCase(dsrMetadata.getUseType())) {
            return "Webcast";
        }
        return "Unknown";
    }

    @Override
    public void printHeader(DsrMetadata dsrMetadata) throws IOException {
        if (null == dsrMetadata) {
            return;
        }

        // computed values
        final BigDecimal conversionRate = BigDecimals
                .divide(dsrMetadata.getConversionRate(), CONVERSION_RATE_SCALE);
        final BigDecimal spliDem = new BigDecimal(dsrMetadata.getSplitDEM());
        final BigDecimal spliDrm = new BigDecimal(dsrMetadata.getSplitDRM());

        logger.debug("printHeader: conversionRate {}", conversionRate);
        logger.debug("printHeader: spliDem {}", spliDem);
        logger.debug("printHeader: spliDrm {}", spliDrm);

        // 1 RECORD_TYPE
        csvPrinter.print("HD");
        // 2 VERSION
        csvPrinter.print("14");
        // 3 DATE
        csvPrinter.print(new SimpleDateFormat("yyyyMMdd").format(new Date()));
        // 4 SENDER
        csvPrinter.print(dsrMetadata.getSender());
        // 5 RECEIVER
        csvPrinter.print(dsrMetadata.getReceiver());
        // 6 CCID_ID
        csvPrinter.print(dsrMetadata.getCcidId().toString());
        // 7 TERRITORY
        csvPrinter.print(dsrMetadata.getTerritory());
        // 8 START_DATE
        csvPrinter.print(dsrMetadata.getPeriodStartDate());
        // 9 END_DATE
        csvPrinter.print(dsrMetadata.getPeriodEndDate());
        // 10 ROYALTY_CURRENCY
        csvPrinter.print(dsrMetadata.getRoyaltyCurrency());
        // 11 ORIGINAL_REVENUE_BASIS_CURRENCY
        csvPrinter.print(dsrMetadata.getOriginalCurrency());
        // 12 CONVERSION_RATE
        csvPrinter.print(toString(conversionRate, 5));
        // 13 WORK_CODE_TYPE
        csvPrinter.print(dsrMetadata.getWorkCodeType());
        // 14 CCID_ID_CORRECTION_REFERENCE
        csvPrinter.print("");
        // 15 TYPE_OF_CLAIM
//		csvPrinter.print("IN");
        csvPrinter.print(dsrMetadata.getParamClaimType());
        // 16 COMMERCIAL_MODEL
//		csvPrinter.print(getCommercialModel(dsrMetadata));
        csvPrinter.print(dsrMetadata.getParamCommercialModel());
        // 17 SERVICE_DESCRIPTION
        csvPrinter.print(dsrMetadata.getTradingBrand());
        // 18 USE_TYPE
        csvPrinter.print(getUseType(dsrMetadata));
        // 19 MECH_PERC_SPLIT
        csvPrinter.print(toString(spliDrm, 2));
        // 20 PERF_PERC_SPLIT
        csvPrinter.print(toString(spliDem, 2));
        // 21 AUX
        if (dsrMetadata.getParamAux().equalsIgnoreCase("prorata"))
            csvPrinter.print(dsrMetadata.getProRata());
        else if (dsrMetadata.getParamAux().equalsIgnoreCase("vuoto"))
            csvPrinter.print("");
        // end-of-line
        csvPrinter.println();
    }

    @Override
    public void printRow(DsrMetadata dsrMetadata, DsrLine dsrLine, ClaimResult claimResult, boolean excludeClaim0, MMSpecialCharCcid m) throws IOException {
        if (null != dsrMetadata && dsrMetadata.isNotEncoded() && null == claimResult) {
            claimResult = new ClaimResult()
                    .setClaimLicensorDem(BigDecimal.ZERO)
                    .setClaimLicensorDrm(BigDecimal.ZERO)
                    .setClaimPdDem(BigDecimal.ZERO)
                    .setClaimPdDrm(BigDecimal.ZERO)
                    .setClaimUnmatched(ONE_HUNDRED);
            dsrLine.setUuidSophia(null);
        }

        if (null == claimResult || null == dsrLine || null == dsrMetadata) {
            return;
        }
        printedLines.incrementAndGet();

        // ----------------------------------- --------------------------
        // RECORD_TYPE ("ID")
        // TRANSACTION_TYPE
        // REF_ID
        // CORRECTION_REFERENCE
        // SALES_TRANSACTION_ID                DSR::transaction_id
        // WORK_ID
        // RELEASE_ID                          DSR::album_proprietary_id
        // RESOURCE_ID                         DSR::proprietary_id
        // ISRC                                DSR::isrc
        // ISWC                                DSR::iswc
        // WORKCODE                            DSR::uuid_sophia
        // WORK_TITLE                          DSR::title
        // APPLIED_TARIFF                      SQS::applied_tariff
        // ROYALTY_TYPE
        // REVENUE_BASIS
        // ORIGINAL_RELEASE_REVENUE_BASIS
        // ORIGINAL_RESOURCE_REVENUE_BASIS
        // ROYALTY
        // RESOURCE_SHARE
        // RESTRICTIONS
        // CLAIM_LICENSOR_COMBINED
        // CLAIM_COPCON_COMBINED
        // CLAIM_UNMATCHED_COMBINED
        // CLAIM_PD_COMBINED
        // CLAIM_NOT_COLLECTED_COMBINED
        // AMOUNT_INVOICED_TOTAL
        // CLAIM_LICENSOR_MECH
        // CLAIM_LICENSOR_PERF
        // AMOUNT_LICENSOR_MECH
        // AMOUNT_LICENSOR_PERF
        // AMOUNT_COPCON_MECH
        // AMOUNT_COPCON_PERF
        // AMOUNT_PD_MECH
        // AMOUNT_PD_PERF
        // AMOUNT_NOT_COLLECTED_MECH
        // AMOUNT_NOT_COLLECTED_PERF
        // AMOUNT_UNMATCHED_MECH
        // AMOUNT_UNMATCHED_PERF
        // CLAIM_COPCON_MECH
        // CLAIM_COPCON_PERF
        // CLAIM_PD_MECH
        // CLAIM_PD_PERF
        // CLAIM_NOT_COLLECTED_MECH
        // CLAIM_NOT_COLLECTED_PERF
        // CLAIM_UNMATCHED_MECH
        // CLAIM_UNMATCHED_PERF
        // ----------------------------------- --------------------------

        // computed values
        final BigDecimal exchangeRate = BigDecimals
                .divide(CONVERSION_RATE_SCALE, dsrMetadata.getConversionRate());
        final BigDecimal royalty = exchangeRate.multiply(dsrLine.getRoyalty());
        final BigDecimal salesCount = dsrLine.getSalesCount();
        final BigDecimal transactionTracks = new BigDecimal(dsrLine.getTransactionTracks());
        final BigDecimal resourceShare = BigDecimals.divide(ONE_HUNDRED,
                transactionTracks.max(BigDecimal.ONE));
        final BigDecimal splitDem = new BigDecimal(dsrMetadata.getSplitDEM());
        final BigDecimal splitDrm = new BigDecimal(dsrMetadata.getSplitDRM());
        final BigDecimal claimLicensorCombined = splitDem.multiply(claimResult.getClaimLicensorDem())
                .add(splitDrm.multiply(claimResult.getClaimLicensorDrm()));

        if (excludeClaim0 && claimLicensorCombined.compareTo(BigDecimal.ZERO) == 0) {
            return;
        }

        final BigDecimal claimPdCombined = splitDem.multiply(claimResult.getClaimPdDem())
                .add(splitDrm.multiply(claimResult.getClaimPdDrm()));
        final BigDecimal claimNotCollectedCombined = ONE_HUNDRED
                .subtract(claimLicensorCombined)
                .subtract(claimPdCombined)
                .subtract(claimResult.getClaimUnmatched())
                .max(BigDecimal.ZERO);

        final BigDecimal amountInvoicedTotal = claimLicensorCombined
                .multiply(royalty)
                .multiply(salesCount)
                .multiply(ONE_HUNDREDTH);
        final BigDecimal amountInvoicedUnmatchedTotal = claimResult.getClaimUnmatched()
                .multiply(royalty)
                .multiply(salesCount)
                .multiply(ONE_HUNDREDTH);
        //aggiungo al totale anche l'unmatched e lo printo nel file
        final BigDecimal realAmountInvoicedTotal = amountInvoicedTotal.add(amountInvoicedUnmatchedTotal);

        final BigDecimal amountLicensorDem = splitDem
                .multiply(claimResult.getClaimLicensorDem())
                .multiply(royalty)
                .multiply(salesCount)
                .multiply(ONE_HUNDREDTH);
        final BigDecimal amountLicensorDrm = splitDrm
                .multiply(claimResult.getClaimLicensorDrm())
                .multiply(royalty)
                .multiply(salesCount)
                .multiply(ONE_HUNDREDTH);

        //per l'unmatched non ha senso dividere dem e drm quindi lo
        // sottraggo per il calcolo ad entrambi (o 0 o 100)
        final BigDecimal claimNotCollectedDem = ONE_HUNDRED
                .subtract(claimResult.getClaimLicensorDem())
                .subtract(claimResult.getClaimPdDem())
                .subtract(claimResult.getClaimUnmatched())
                .max(BigDecimal.ZERO);
        final BigDecimal claimNotCollectedDrm = ONE_HUNDRED
                .subtract(claimResult.getClaimLicensorDrm())
                .subtract(claimResult.getClaimPdDrm())
                .subtract(claimResult.getClaimUnmatched())
                .max(BigDecimal.ZERO);
        final BigDecimal amountPdDem = splitDem
                .multiply(claimResult.getClaimPdDem())
                .multiply(royalty)
                .multiply(salesCount)
                .multiply(ONE_HUNDREDTH);
        final BigDecimal amountPdDrm = splitDrm
                .multiply(claimResult.getClaimPdDrm())
                .multiply(royalty)
                .multiply(salesCount)
                .multiply(ONE_HUNDREDTH);
        final BigDecimal amountNotCollectedDem = splitDem
                .multiply(claimNotCollectedDem)
                .multiply(royalty)
                .multiply(salesCount)
                .multiply(ONE_HUNDREDTH);
        final BigDecimal amountNotCollectedDrm = splitDrm
                .multiply(claimNotCollectedDrm)
                .multiply(royalty)
                .multiply(salesCount)
                .multiply(ONE_HUNDREDTH);

        final BigDecimal amountUnmatchedDem = splitDem
                .multiply(claimResult.getClaimUnmatched())
                .multiply(royalty)
                .multiply(salesCount)
                .multiply(ONE_HUNDREDTH);
        final BigDecimal amountUnmatchedDrm = splitDrm
                .multiply(claimResult.getClaimUnmatched())
                .multiply(royalty)
                .multiply(salesCount)
                .multiply(ONE_HUNDREDTH);

//		logger.debug("printRow: exchangeRate {}", exchangeRate);
//		logger.debug("printRow: royalty {} sales {} claim {} amount {}",
//				royalty, salesCount, claimLicensorCombined, amountInvoicedTotal);

        // update dsr metadata accumulators
        dsrMetadata.incrementSumIdRecords();
        dsrMetadata.addSumSalesCount(salesCount);
        dsrMetadata.addAmountInvoicedTotal(amountInvoicedTotal);
        dsrMetadata.addAmountUnmatchedTotal(amountInvoicedUnmatchedTotal);

        // 1 RECORD_TYPE
        csvPrinter.print("ID");
        // 2 TRANSACTION_TYPE
        csvPrinter.print("ORI");
        // 3 REF_ID
        csvPrinter.print(getRefId(dsrMetadata));
        // 4 CORRECTION_REFERENCE
        csvPrinter.print("");
        // 5 SALES_TRANSACTION_ID
        csvPrinter.print(dsrLine.getTransactionId());
        // 6 WORK_ID
        csvPrinter.print("");
        // 7 RELEASE_ID
        csvPrinter.print(dsrLine.getAlbumProprietrayId());
        // 8 RESOURCE_ID
        csvPrinter.print(dsrLine.getProprietrayId());
        // 9 ISRC
        csvPrinter.print(dsrLine.getIsrc());
        // 10 ISWC
        csvPrinter.print(dsrLine.getIswc());
        // 11 WORKCODE
        csvPrinter.print(Strings.isNullOrEmpty(dsrLine.getUuidSophia()) ?
                "U".concat(dsrMetadata.getBaseRefId().add(new BigInteger(Long.valueOf(printedLines.get()).toString())).toString()) : dsrLine.getUuidSophia());
        // 12 WORK_TITLE
        //R2011 applico la configurazione per il DSP corrente
        if (m.checkMSCCcid()) {
            logger.info("SET Special Char configuration ...");
            for (String s : m.getCharList().split(" ")) {
                dsrLine.setTitle(dsrLine.getTitle().replace(s.equalsIgnoreCase("\\t") ? "\t" : s, ""));
            }
            if (dsrLine.getTitle().length() > m.getTitleLength().intValue()) {
                csvPrinter.print(dsrLine.getTitle().substring(0, m.getTitleLength().intValue()));
            }else {
                csvPrinter.print(dsrLine.getTitle());
            }
            logger.info("CcidV14Printer end set work title {}" + dsrLine.getTitle());
        } else {
            csvPrinter.print(dsrLine.getTitle());
        }
        // 13 USE_QUANTITY
        csvPrinter.print(dsrLine.getSalesCount());
        // 14 APPLIED_TARIFF
        if (dsrMetadata.getParamAppliedTariff().equalsIgnoreCase("prorata"))
            csvPrinter.print(dsrLine.getProRata());
        else
            csvPrinter.print(dsrMetadata.getAppliedTariff());
        // 15 ROYALTY_TYPE
        csvPrinter.print(dsrLine.getRoyaltyType());
        // 16 REVENUE_BASIS
        csvPrinter.print(smartTrim(toString(exchangeRate.multiply(dsrLine.getPriceBasis()), 20)));
        // 17 ORIGINAL_RELEASE_REVENUE_BASIS
        csvPrinter.print(smartTrim(toString(dsrLine.getOriginalPriceBasis(), 20)));
        // 18 ORIGINAL_RESOURCE_REVENUE_BASIS
        csvPrinter.print(smartTrim(toString(dsrLine.getOriginalPriceBasis(), 20))); // should we divide by transactionTracks?
        // 19 ROYALTY
        csvPrinter.print(smartTrim(toString(royalty, 20)));
        // 20 RESOURCE_SHARE
        csvPrinter.print(toString(resourceShare, 2));
        // 21 RESTRICTIONS
        csvPrinter.print("NR");
        // 22 CLAIM_LICENSOR_COMBINED
        csvPrinter.print(toString(claimLicensorCombined, 2));
        // 23 CLAIM_COPCON_COMBINED
        csvPrinter.print("0.00");
        // 24 CLAIM_UNMATCHED_COMBINED
        csvPrinter.print(toString(claimResult.getClaimUnmatched(), 2));
        // 25 CLAIM_PD_COMBINED
        csvPrinter.print(toString(claimPdCombined, 2));
        // 26 CLAIM_NOT_COLLECTED_COMBINED
        csvPrinter.print(toString(claimNotCollectedCombined, 2));
        // 27 AMOUNT_INVOICED_TOTAL
        csvPrinter.print(smartTrim(toString(realAmountInvoicedTotal, 20)));
        // 28 CLAIM_LICENSOR_MECH (without right split)
        csvPrinter.print(toString(claimResult.getClaimLicensorDrm(), 2));
        // 29 CLAIM_LICENSOR_PERF (without right split)
        csvPrinter.print(toString(claimResult.getClaimLicensorDem(), 2));
        // 30 AMOUNT_LICENSOR_MECH
        csvPrinter.print(smartTrim(toString(amountLicensorDrm, 20)));
        // 31 AMOUNT_LICENSOR_PERF
        csvPrinter.print(smartTrim(toString(amountLicensorDem, 20)));
        // 32 AMOUNT_COPCON_MECH
        csvPrinter.print("0.0");
        // 33 AMOUNT_COPCON_PERF
        csvPrinter.print("0.0");
        // 34 AMOUNT_PD_MECH
        csvPrinter.print(smartTrim(toString(amountPdDrm, 20)));
        // 35 AMOUNT_PD_PERF
        csvPrinter.print(smartTrim(toString(amountPdDem, 20)));
        // 36 AMOUNT_NOT_COLLECTED_MECH
        csvPrinter.print(smartTrim(toString(amountNotCollectedDrm, 20)));
        // 37 AMOUNT_NOT_COLLECTED_PERF
        csvPrinter.print(smartTrim(toString(amountNotCollectedDem, 20)));
        // 38 AMOUNT_UNMATCHED_MECH
        csvPrinter.print(smartTrim(toString(amountUnmatchedDrm, 20)));
        // 39 AMOUNT_UNMATCHED_PERF
        csvPrinter.print(smartTrim(toString(amountUnmatchedDem, 20)));
        // 40 CLAIM_COPCON_MECH (without right split)
        csvPrinter.print("0.00");
        // 41 CLAIM_COPCON_PERF (without right split)
        csvPrinter.print("0.00");
        // 42 CLAIM_PD_MECH (without right split)
        csvPrinter.print(toString(claimResult.getClaimPdDrm(), 2));
        // 43 CLAIM_PD_PERF (without right split)
        csvPrinter.print(toString(claimResult.getClaimPdDem(), 2));
        // 44 CLAIM_NOT_COLLECTED_MECH (without right split)
        csvPrinter.print(toString(claimNotCollectedDrm, 2));
        // 45 CLAIM_NOT_COLLECTED_PERF (without right split)
        csvPrinter.print(toString(claimNotCollectedDem, 2));
        // 46 CLAIM_UNMATCHED_MECH (without right split)
        csvPrinter.print(toString(claimResult.getClaimUnmatched(), 2));
        // 47 CLAIM_UNMATCHED_PERF (without right split)
        csvPrinter.print(toString(claimResult.getClaimUnmatched(), 2));

        // end-of-line
        csvPrinter.println();

        //FIX CTASK0024973 (CHG0033723) SUM AMOUNT_LICENSOR_MECH+AMOUNT_LICENSOR_PERF
        dsrMetadata.addAmountMech(amountLicensorDem);
        dsrMetadata.addAmountPerf(amountLicensorDrm);
    }

    @Override
    public void printTrailer(DsrMetadata dsrMetadata) throws IOException {
        if (null == dsrMetadata) {
            return;
        }

        logger.debug("printTrailer: sumIdRecords {}", dsrMetadata.getSumIdRecords());
        logger.debug("printTrailer: sumSalesCount {}", dsrMetadata.getSumSalesCount());
        logger.debug("printTrailer: sumAmountInvoicedTotal {}", dsrMetadata.getSumAmountInvoicedTotal());
        logger.debug("printTrailer: DEM E DRM ", dsrMetadata.getSumAmountMech(), dsrMetadata.getSumAmountPerf());

        //FIX CTASK0024973 (CHG0033723) SUM AMOUNT_LICENSOR_MECH+AMOUNT_LICENSOR_PERF
        BigDecimal tot = new BigDecimal(0).add(dsrMetadata.getSumAmountMech().add(dsrMetadata.getSumAmountPerf()));

        // 1 RECORD_TYPE
        csvPrinter.print("TR");
        // 2 ID_RECORDS
        csvPrinter.print(Long.toString(dsrMetadata.getSumIdRecords()));
        // 3 DL_RECORDS
        csvPrinter.print("0");
        // 4 SUM_AMOUNT_LICENSOR
        //FIX CTASK0024973 (CHG0033723) SUM AMOUNT_LICENSOR_MECH+AMOUNT_LICENSOR_PERF
//		csvPrinter.print(smartTrim(toString(dsrMetadata.getSumAmountInvoicedTotal(), 20)));
        csvPrinter.print(smartTrim(toString(tot, 4)));
        // 5 SUM_AMOUNT_COPCON
        csvPrinter.print("0.0");
        // 6 SUM_AMOUNT_UNMATCHED

        if (dsrMetadata.isNotEncoded())
            csvPrinter.print(smartTrim(toString(dsrMetadata.getSumAmountUnmatchedTotal(), 20)));
        else
            csvPrinter.print("0.0");

        // end-of-line
        csvPrinter.println();
    }

}
