package com.alkemytech.sophia.royalties.claim;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.alkemytech.sophia.royalties.filenaming.MMSpecialCharCcid;
import com.google.common.base.Strings;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemy.siae.sophia.pricing.DsrLine;
import com.alkemy.siae.sophia.pricing.DsrMetadata;
import com.alkemytech.sophia.commons.util.BigDecimals;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class CcidV13Printer extends CcidPrinter {

    private static final Logger logger = LoggerFactory.getLogger(CcidV13Printer.class);

    public static final char DELIMITER = '|';

    public CcidV13Printer(CSVPrinter csvPrinter) {
        super(csvPrinter);
    }

    private String toString(BigDecimal number, int decimalPlaces) {
        return toCcid13Value(number, decimalPlaces).toPlainString();
    }

    private BigDecimal toCcid13Value(BigDecimal number, int decimalPlaces) {
        return number.multiply(BigDecimal.TEN.pow(decimalPlaces))
                .setScale(0, BigDecimal.ROUND_HALF_UP);
    }

    @Override
    public void printHeader(DsrMetadata dsrMetadata) throws IOException {
        if (null == dsrMetadata) {
            return;
        }

        // computed values
        final BigDecimal conversionRate = BigDecimals
                .divide(dsrMetadata.getConversionRate(), CONVERSION_RATE_SCALE);

        logger.debug("printHeader: conversionRate {}", conversionRate);

        // 1 RECORD_TYPE
        csvPrinter.print("HD");
        // 2 VERSION
        csvPrinter.print("13");
        // 3 DATE
        csvPrinter.print(new SimpleDateFormat("yyyyMMdd").format(new Date()));
        // 4 SENDER
        csvPrinter.print(dsrMetadata.getSender());
        // 5 RECEIVER
        csvPrinter.print(dsrMetadata.getReceiver());
        // 6 CCID_ID
        csvPrinter.print(dsrMetadata.getCcidId().toString());
        // 7 TERRITORY
        csvPrinter.print(dsrMetadata.getTerritory());
        // 8 START_DATE
        csvPrinter.print(dsrMetadata.getPeriodStartDate());
        // 9 END_DATE
        csvPrinter.print(dsrMetadata.getPeriodEndDate());
        // 10 ROYALTY_CURRENCY
        csvPrinter.print(dsrMetadata.getRoyaltyCurrency());
        // 11 ORIGINAL_PRICE_BASIS_CURRENCY
        csvPrinter.print(dsrMetadata.getOriginalCurrency());
        // 12 CONVERSION_RATE
        csvPrinter.print(toString(conversionRate, 5));
        // 13 WORK_CODE_TYPE
        csvPrinter.print(dsrMetadata.getWorkCodeType());

        // end-of-line
        csvPrinter.println();
    }

    @Override
    public void printRow(DsrMetadata dsrMetadata, DsrLine dsrLine, ClaimResult claimResult, boolean excludeClaim0, MMSpecialCharCcid m) throws IOException {
        if (null != dsrMetadata && dsrMetadata.isNotEncoded() && null == claimResult) {
            claimResult = new ClaimResult()
                    .setClaimLicensorDem(BigDecimal.ZERO)
                    .setClaimLicensorDrm(BigDecimal.ZERO)
                    .setClaimPdDem(BigDecimal.ZERO)
                    .setClaimPdDrm(BigDecimal.ZERO)
                    .setClaimUnmatched(ONE_HUNDRED);
            dsrLine.setUuidSophia(null);

        }

        if (null == claimResult || null == dsrLine || null == dsrMetadata) {
            //logger.debug("claimResult null");
            return;
        }
        printedLines.incrementAndGet();

        // ----------------------------------- --------------------------
        // RECORD_TYPE ("ID")
        // TRANSACTION_TYPE
        // REF_ID
        // CORRECTION_REFERENCE
        // SALES_TRANSACTION_ID                DSR::transaction_id
        // TRADING_BRAND
        // RELEASE_ID                          DSR::album_proprietary_id
        // RESOURCE_ID                         DSR::proprietary_id
        // ISRC                                DSR::isrc
        // ISWC                                DSR::iswc
        // WORKCODE                            DSR::uuid_sophia
        // WORK_TITLE                          DSR::title
        // SERVICE_TYPE                        SQS::service_type
        // USE_TYPE                            SQS::use_type
        // USE_QUANTITY                        DSR::sales_count
        // APPLIED_TARIFF                      SQS::applied_tariff
        // ROYALTY_TYPE Q
        // PRICE_BASIS R
        // ORIGINAL_PRICE_BASIS S
        // ROYALTY T
        // MUSIC_SHARE U
        // RESTRICTIONS V
        // FST_LICENSE W
        // CLAIM_LICENSOR_COMBINED X
        // CLAIM_PAI_COMBINED Y
        // CLAIM_UNMATCHED_COMBINED Z
        // CLAIM_PD_COMBINED AA
        // CLAIM_NOT_COLLECTED_COMBINED AB
        // AMOUNT_INVOICED_TOTAL AC
        // CLAIM_LICENSOR_MECH AD
        // CLAIM_LICENSOR_PERF AE
        // AMOUNT_LICENSOR_MECH AF
        // AMOUNT_LICENSOR_PERF AG
        // AMOUNT_PAI_MECH AH
        // AMOUNT_PAI_PERF AI
        // AMOUNT_PD_MECH AJ
        // AMOUNT_PD_PERF AK
        // AMOUNT_NOT_COLLECTED_MECH AL
        // AMOUNT_NOT_COLLECTED_PERF AM
        // AMOUNT_UNMATCHED_MECH AN
        // AMOUNT_UNMATCHED_PERF AO
        // CLAIM_PAI_MECH AP
        // CLAIM_PAI_PERF AQ
        // CLAIM_PD_MECH AR
        // CLAIM_PD_PERF AS
        // CLAIM_NOT_COLLECTED_MECH AT
        // CLAIM_NOT_COLLECTED_PERF AU
        // CLAIM_UNMATCHED_MECH AV
        // CLAIM_UNMATCHED_PERF AW
        // ----------------------------------- --------------------------

        // computed values
        final BigDecimal exchangeRate = BigDecimals
                .divide(CONVERSION_RATE_SCALE, dsrMetadata.getConversionRate());
        final BigDecimal royalty = exchangeRate.multiply(dsrLine.getRoyalty());
        final BigDecimal salesCount = dsrLine.getSalesCount();
        final BigDecimal transactionTracks = new BigDecimal(dsrLine.getTransactionTracks());
        final BigDecimal resourceShare = BigDecimals.divide(ONE_HUNDRED,
                transactionTracks.max(BigDecimal.ONE));
        final BigDecimal splitDem = BigDecimal.valueOf(dsrMetadata.getSplitDEM());
        final BigDecimal splitDrm = BigDecimal.valueOf(dsrMetadata.getSplitDRM());
        final BigDecimal claimLicensorCombined = splitDem.multiply(claimResult.getClaimLicensorDem())
                .add(splitDrm.multiply(claimResult.getClaimLicensorDrm()));

        if (excludeClaim0 && claimLicensorCombined.compareTo(BigDecimal.ZERO) == 0) {
            return;
        }

        final BigDecimal claimPdCombined = splitDem.multiply(claimResult.getClaimPdDem())
                .add(splitDrm.multiply(claimResult.getClaimPdDrm()));
        final BigDecimal claimNotCollectedCombined = ONE_HUNDRED
                .subtract(claimLicensorCombined)
                .subtract(claimPdCombined)
                .subtract(claimResult.getClaimUnmatched())
                .max(BigDecimal.ZERO);

//		final BigDecimal amountInvoicedTotal = claimLicensorCombined
//				.multiply(royalty)
//				.multiply(salesCount)
//				.multiply(ONE_HUNDREDTH);
//		final BigDecimal amountInvoicedUnmatchedTotal = claimResult.getClaimUnmatched()
//				.multiply(royalty)
//				.multiply(salesCount)
//				.multiply(ONE_HUNDREDTH);
//		//aggiungo al totale anche l'unmatched e lo printo nel file
//		final BigDecimal realAmountInvoicedTotal = amountInvoicedTotal.add(amountInvoicedUnmatchedTotal);

        final BigDecimal amountLicensorDem = splitDem
                .multiply(claimResult.getClaimLicensorDem())
                .multiply(royalty)
                .multiply(salesCount)
                .multiply(ONE_HUNDREDTH);
        BigDecimal amountLicensorDrm = splitDrm
                .multiply(claimResult.getClaimLicensorDrm())
                .multiply(royalty)
                .multiply(salesCount)
                .multiply(ONE_HUNDREDTH);
        amountLicensorDrm = normalizeDrm(amountLicensorDem, amountLicensorDrm);

        final BigDecimal amountInvoicedTotal = amountLicensorDem.setScale(4, BigDecimal.ROUND_HALF_UP).add(amountLicensorDrm.setScale(4, BigDecimal.ROUND_HALF_UP));


        //per l'unmatched non ha senso dividere dem e drm quindi lo
        // sottraggo per il calcolo ad entrambi (o 0 o 100)
        final BigDecimal claimNotCollectedDem = ONE_HUNDRED
                .subtract(claimResult.getClaimLicensorDem())
                .subtract(claimResult.getClaimPdDem())
                .subtract(claimResult.getClaimUnmatched())
                .max(BigDecimal.ZERO);
        BigDecimal claimNotCollectedDrm = ONE_HUNDRED
                .subtract(claimResult.getClaimLicensorDrm())
                .subtract(claimResult.getClaimPdDrm())
                .subtract(claimResult.getClaimUnmatched())
                .max(BigDecimal.ZERO);
        claimNotCollectedDrm = normalizeDrm(claimNotCollectedDem, claimNotCollectedDrm);

        final BigDecimal amountPdDem = splitDem
                .multiply(claimResult.getClaimPdDem())
                .multiply(royalty)
                .multiply(salesCount)
                .multiply(ONE_HUNDREDTH);
        BigDecimal amountPdDrm = splitDrm
                .multiply(claimResult.getClaimPdDrm())
                .multiply(royalty)
                .multiply(salesCount)
                .multiply(ONE_HUNDREDTH);
        amountPdDrm = normalizeDrm(amountPdDem, amountPdDrm);

        final BigDecimal amountNotCollectedDem = splitDem
                .multiply(claimNotCollectedDem)
                .multiply(royalty)
                .multiply(salesCount)
                .multiply(ONE_HUNDREDTH);
        BigDecimal amountNotCollectedDrm = splitDrm
                .multiply(claimNotCollectedDrm)
                .multiply(royalty)
                .multiply(salesCount)
                .multiply(ONE_HUNDREDTH);
        amountNotCollectedDrm = normalizeDrm(amountNotCollectedDem, amountNotCollectedDrm);

        final BigDecimal amountUnmatchedDem = splitDem
                .multiply(claimResult.getClaimUnmatched())
                .multiply(royalty)
                .multiply(salesCount)
                .multiply(ONE_HUNDREDTH);
        BigDecimal amountUnmatchedDrm = splitDrm
                .multiply(claimResult.getClaimUnmatched())
                .multiply(royalty)
                .multiply(salesCount)
                .multiply(ONE_HUNDREDTH);
        amountUnmatchedDrm = normalizeDrm(amountUnmatchedDem, amountUnmatchedDrm);


        //normalize ccid14 drm right


        final BigDecimal amountInvoicedUnmatchedTotal = amountUnmatchedDem.add(amountUnmatchedDrm);
        final BigDecimal realAmountInvoicedTotal = amountInvoicedTotal.add(amountInvoicedUnmatchedTotal);

//		logger.debug("printRow: exchangeRate {}", exchangeRate);
//		logger.debug("printRow: royalty {} sales {} claim {} amount {}",
//				royalty, salesCount, claimLicensorCombined, amountInvoicedTotal);

        // update dsr metadata accumulators
        dsrMetadata.incrementSumIdRecords();
        dsrMetadata.addSumSalesCount(salesCount);
        dsrMetadata.addAmountInvoicedTotal(toCcid13Value(amountInvoicedTotal, 4));
        dsrMetadata.addAmountUnmatchedTotal(toCcid13Value(amountInvoicedUnmatchedTotal, 4));

        // 1 RECORD_TYPE
        csvPrinter.print("ID");
        // 2 TRANSACTION_TYPE
        csvPrinter.print("ORI");
        // 3 REF_ID
        String refid = getRefId(dsrMetadata);
        csvPrinter.print(refid);
        // 4 CORRECTION_REFERENCE
        csvPrinter.print("");
        // 5 SALES_TRANSACTION_ID
        csvPrinter.print(dsrLine.getTransactionId());
        // 6 TRADING_BRAND
        csvPrinter.print(dsrMetadata.getTradingBrand());
        // 7 RELEASE_ID
        csvPrinter.print(dsrLine.getAlbumProprietrayId());
        // 8 RESOURCE_ID
        csvPrinter.print(dsrLine.getProprietrayId());
        // 9 ISRC
        csvPrinter.print(dsrLine.getIsrc());
        // 10 ISWC
        csvPrinter.print(dsrLine.getIswc());
        // 11 WORKCODE
        csvPrinter.print(Strings.isNullOrEmpty(dsrLine.getUuidSophia()) ?
                "U".concat(dsrMetadata.getBaseRefId().add(new BigInteger(Long.valueOf(printedLines.get()).toString())).toString()) : dsrLine.getUuidSophia());
        // 12 WORK_TITLE
        //R2011 applico la configurazione per il DSP corrente
        if (m.checkMSCCcid()) {
            logger.info("SET Special Char configuration ...");
            for (String s : m.getCharList().split(" ")) {
                dsrLine.setTitle(dsrLine.getTitle().replace(s.equalsIgnoreCase("\\t") ? "\t" : s, ""));
            }
            if (dsrLine.getTitle().length() > m.getTitleLength().intValue()) {
                csvPrinter.print(dsrLine.getTitle().substring(0, m.getTitleLength().intValue()));
            }else {
                csvPrinter.print(dsrLine.getTitle());
            }
            logger.info("CcidV13Printer end set work title {}" + dsrLine.getTitle());
        } else {
            csvPrinter.print(dsrLine.getTitle());
        }
        //
        // 13 SERVICE_TYPE
        csvPrinter.print(dsrMetadata.getServiceType());
        // 14 USE_TYPE
        csvPrinter.print(dsrMetadata.getUseType());
        // 15 USE_QUANTITY
        csvPrinter.print(dsrLine.getSalesCount());
        // 16 APPLIED_TARIFF
        csvPrinter.print(dsrMetadata.getAppliedTariff());
        // 17 ROYALTY_TYPE
        csvPrinter.print(dsrLine.getRoyaltyType());
        // 18 PRICE_BASIS
        csvPrinter.print(toString(exchangeRate.multiply(dsrLine.getPriceBasis()), 4));
        // 19 ORIGINAL_PRICE_BASIS
        csvPrinter.print(toString(dsrLine.getOriginalPriceBasis(), 4));
        // 20 ROYALTY
        csvPrinter.print(toString(exchangeRate.multiply(dsrLine.getRoyalty()), 4));
        // 21 MUSIC_SHARE
        csvPrinter.print(toString(resourceShare, 2));
        // 22 RESTRICTIONS
        csvPrinter.print("NR");
        // 23 FST_LICENSE
        csvPrinter.print("A");
        // 24 CLAIM_LICENSOR_COMBINED
        csvPrinter.print(toString(claimLicensorCombined, 2));
        // 25 CLAIM_PAI_COMBINED
        csvPrinter.print("0");
        // 26 CLAIM_UNMATCHED_COMBINED
        csvPrinter.print(toString(claimResult.getClaimUnmatched(), 2));
        // 27 CLAIM_PD_COMBINED
        csvPrinter.print(toString(claimPdCombined, 2));
        // 28 CLAIM_NOT_COLLECTED_COMBINED
        csvPrinter.print(toString(claimNotCollectedCombined, 2));
        // 29 AMOUNT_INVOICED_TOTAL
        csvPrinter.print(toString(realAmountInvoicedTotal, 4));
        // 30 CLAIM_LICENSOR_MECH (without right split)
        csvPrinter.print(toString(claimResult.getClaimLicensorDrm(), 2));
        // 31 CLAIM_LICENSOR_PERF (without right split)
        csvPrinter.print(toString(claimResult.getClaimLicensorDem(), 2));
        // 32 AMOUNT_LICENSOR_MECH
        csvPrinter.print(toString(amountLicensorDrm, 4));
        // 33 AMOUNT_LICENSOR_PERF
        //csvPrinter.print(toString(amountLicensorDem, 4));
        csvPrinter.print(toString(amountLicensorDem, 4));
        // 34 AMOUNT_PAI_MECH
        csvPrinter.print("0");
        // 35 AMOUNT_PAI_PERF
        csvPrinter.print("0");
        // 36 AMOUNT_PD_MECH
        csvPrinter.print(toString(amountPdDrm, 4));
        // 37 AMOUNT_PD_PERF
        csvPrinter.print(toString(amountPdDem, 4));
        // 38 AMOUNT_NOT_COLLECTED_MECH
        csvPrinter.print(toString(amountNotCollectedDrm, 4));
        // 39 AMOUNT_NOT_COLLECTED_PERF
        csvPrinter.print(toString(amountNotCollectedDem, 4));
        // 40 AMOUNT_UNMATCHED_MECH
        csvPrinter.print(toString(amountUnmatchedDrm, 4));
        // 41 AMOUNT_UNMATCHED_PERF
        csvPrinter.print(toString(amountUnmatchedDem, 4));
        // 42 CLAIM_PAI_MECH (without right split)
        csvPrinter.print("0");
        // 43 CLAIM_PAI_PERF (without right split)
        csvPrinter.print("0");
        // 44 CLAIM_PD_MECH (without right split)
        csvPrinter.print(toString(claimResult.getClaimPdDrm(), 2));
        // 45 CLAIM_PD_PERF (without right split)
        csvPrinter.print(toString(claimResult.getClaimPdDem(), 2));
        // 46 CLAIM_NOT_COLLECTED_MECH (without right split)
        csvPrinter.print(toString(claimNotCollectedDrm, 2));
        // 47 CLAIM_NOT_COLLECTED_PERF (without right split)
        csvPrinter.print(toString(claimNotCollectedDem, 2));
        // 48 CLAIM_UNMATCHED_MECH (without right split)
        csvPrinter.print(toString(claimResult.getClaimUnmatched(), 2));
        // 49 CLAIM_UNMATCHED_PERF (without right split)
        csvPrinter.print(toString(claimResult.getClaimUnmatched(), 2));

        // end-of-line
        csvPrinter.println();

        //FIX CTASK0024973 (CHG0033723) SUM AMOUNT_LICENSOR_MECH+AMOUNT_LICENSOR_PERF


        dsrMetadata.addAmountMech(toCcid13Value(amountLicensorDrm, 4));
        dsrMetadata.addAmountPerf(toCcid13Value(amountLicensorDem, 4));

    }

    @Override
    public void printTrailer(DsrMetadata dsrMetadata) throws IOException {
        if (null == dsrMetadata) {
            return;
        }

        logger.debug("printTrailer: sumIdRecords {}", dsrMetadata.getSumIdRecords());
        logger.debug("printTrailer: sumSalesCount {}", dsrMetadata.getSumSalesCount());
        logger.debug("printTrailer: sumAmountInvoicedTotal {}", dsrMetadata.getSumAmountInvoicedTotal());
        logger.debug("printTrailer: DRM E DEM {}", dsrMetadata.getSumAmountMech() + " , " + dsrMetadata.getSumAmountPerf());

        //FIX CTASK0024973 (CHG0033723) SUM AMOUNT_LICENSOR_MECH+AMOUNT_LICENSOR_PERF


        BigDecimal tot = new BigDecimal(0).add(dsrMetadata.getSumAmountMech().add(dsrMetadata.getSumAmountPerf()));
        logger.debug("printTrailer: DRM E DEM SCALED {}", dsrMetadata.getSumAmountMech() + " , " + dsrMetadata.getSumAmountPerf());
        logger.debug("PRINT TRAILER TOT " + tot);

        // 1 RECORD_TYPE
        csvPrinter.print("TR");
        // 2 ID_RECORDS
        csvPrinter.print(Long.toString(dsrMetadata.getSumIdRecords()));
        // 3 CD_RECORDS
        csvPrinter.print("0");
        // 4 SUM_AMOUNT_LICENSOR
        //FIX CTASK0024973 (CHG0033723) SUM AMOUNT_LICENSOR_MECH+AMOUNT_LICENSOR_PERF
        //csvPrinter.print(dsrMetadata.getSumAmountInvoicedTotal());
        csvPrinter.print(tot);
        // 5 SUM_AMOUNT_PAI
        csvPrinter.print("0");
        // 6 SUM_AMOUNT_UNMATCHED


        if (dsrMetadata.isNotEncoded())
            csvPrinter.print(toString(dsrMetadata.getSumAmountUnmatchedTotal(), 4));
        else
            csvPrinter.print("0");

        // end-of-line
        csvPrinter.println();
    }

    BigDecimal normalizeDrm(BigDecimal demAmount, BigDecimal drmAmount) {
        BigDecimal tot = demAmount.add(drmAmount).setScale(4, RoundingMode.HALF_UP);
        return tot.subtract(demAmount.setScale(4, RoundingMode.HALF_UP));
    }


}
