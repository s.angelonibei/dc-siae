package com.alkemytech.sophia.royalties.region;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class DvdRegions {

//	1	United States, Canada, U.S. territories (American Samoa, Guam, Northern Mariana Islands, Puerto Rico, United States Minor Outlying Islands, Virgin Islands U.S.)
//	2	Europe (except Russia, Belarus, Ukraine), Egypt, Middle East, Japan, South Africa, Swaziland, Lesotho, Greenland, British Overseas Territories, British Crown Dependencies, French Overseas departments and territories
//	3	Southeast Asia, South Korea, Taiwan, Hong Kong, Macau
//	4	Latin America (except French Guiana), Bermuda, Suriname, New Zealand, Australia
//	5	South Asia, Afghanistan, Russia, Belarus, Ukraine, Africa (except Egypt, South Africa, Swaziland, Lesotho), Central Asia, Mongolia, North Korea
//	6	Mainland China

	private static final String[] REGIONS = { "1", "2", "3", "4", "5", "6" };
	private static final String[][] SUBREGIONS = { { "1" }, { "2" }, { "3" }, { "4" }, { "5" }, { "6" } };	
	private static final String[][][] COUNTRIES = {
		{ { "US", "CA", "AS", "GU", "MP", "PR", "UM", "VI" } },
		{ { "GF", "LS", "JP", "SZ", "EG", "ZA", "GG", "IM", "JE", "AX", "DK", "EE", "FI", "FO", "GB", "IE", "IM", "IS", "LT", "LV",
			"NO", "SE", "SJ", "AT", "BE", "CH", "DE", "DD", "FR", "FX", "LI", "LU", "MC", "NL", "BG", "CZ", "HU", "MD", "PL", "RO",
			"SU", "SK", "AD", "AL", "BA", "ES", "GI", "GR", "HR", "IT", "ME", "MK", "MT", "CS", "RS", "PT", "SI", "SM", "VA", "YU",
			"GL", "PM", "AE", "AM", "AZ", "BH", "CY", "GE", "IL", "IQ", "JO", "KW", "LB", "OM", "PS", "QA", "SA", "NT", "SY", "TR",
			"YE", "YD", "NF", "FJ", "NC", "PG", "SB", "VU", "FM", "KI", "MH", "NR", "PW", "CK", "NU", "PF", "PN", "TK", "TO", "TV",
			"WF", "WS" } },
		{ { "BN", "ID", "KH", "LA", "MM", "BU", "MY", "PH", "SG", "TH", "TL", "TP", "VN", "KR", "TW", "HK", "MO" } },
		{ { "BM", "SR", "NZ", "AU", "AG", "AI", "AN", "AW", "BB", "BL", "BS", "CU", "DM", "DO", "GD", "GP", "HT", "JM", "KN", "KY",
			"LC", "MF", "MQ", "MS", "PR", "TC", "TT", "VC", "VG", "VI", "BZ", "CR", "GT", "HN", "MX", "NI", "PA", "SV", "AR", "BO",
			"BR", "CL", "CO", "EC", "FK", "GY", "PE", "PY", "SR", "UY", "VE"  } },
		{ { "UA", "KP", "MN", "AF", "BD", "BT", "IN", "IR", "LK", "MV", "NP", "PK", "AF", "RU", "BY", "TM", "TJ", "KG", "KZ", "UZ",
			"DZ", "EH", "LY", "MA", "SD", "TN", "BF", "BJ", "CI", "CV", "GH", "GM", "GN", "GW", "LR", "ML", "MR", "NE", "NG", "SH",
			"SL", "SN", "TG", "AO", "CD", "ZR", "CF", "CG", "CM", "GA", "GQ", "ST", "TD", "BI", "DJ", "ER", "ET", "KE", "KM", "MG",
			"MU", "MW", "MZ", "RE", "RW", "SC", "SO", "TZ", "UG", "YT", "ZM", "ZW", "BW", "NA" } },
		{ { "CN" } },
	};
	
	private static final Map<String, String> countryToRegion = new ConcurrentHashMap<>();

	static {
		for (int i = 0; i < REGIONS.length; i ++) {
			final String region = REGIONS[i];
			for (int j = 0; j < SUBREGIONS[i].length; j ++) {
				for (int k = 0; k < COUNTRIES[i][j].length; k ++) {
					final String country = COUNTRIES[i][j][k];
					countryToRegion.put(country, region);
				}
			}
		}
	}
	
	public static String getRegion(String territory) {
		return countryToRegion.get(territory);
	}
	
}
