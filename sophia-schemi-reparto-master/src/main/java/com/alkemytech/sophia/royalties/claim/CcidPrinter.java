package com.alkemytech.sophia.royalties.claim;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.concurrent.atomic.AtomicLong;

import com.alkemytech.sophia.royalties.filenaming.MMSpecialCharCcid;
import org.apache.commons.csv.CSVPrinter;

import com.alkemy.siae.sophia.pricing.DsrLine;
import com.alkemy.siae.sophia.pricing.DsrMetadata;
import com.alkemytech.sophia.commons.util.BigDecimals;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public abstract class CcidPrinter {
	
	protected static final BigDecimal ONE_HUNDRED = new BigDecimal(100.0);
	protected static final BigDecimal ONE_HUNDREDTH = BigDecimals.divide(BigDecimal.ONE, ONE_HUNDRED);
	protected static final BigDecimal CONVERSION_RATE_SCALE = BigDecimal.TEN.pow(5);
	
	protected final CSVPrinter csvPrinter;
	protected final AtomicLong printedLines;
	private final AtomicLong refId;

	protected CcidPrinter(CSVPrinter csvPrinter) {
		this.csvPrinter = csvPrinter;
		this.printedLines = new AtomicLong(0L);
		this.refId = new AtomicLong(0L);
	}
	
	protected String getRefId(DsrMetadata dsrMetadata) {
		return dsrMetadata.getBaseRefId()
				.add(new BigInteger(Long.toString(refId.incrementAndGet())))
					.toString();
	}
	
	public long getPrintedLines() {
		return printedLines.longValue();
	}

	public abstract void printHeader(DsrMetadata dsrMetadata) throws IOException;
	
	public abstract void printRow(DsrMetadata dsrMetadata, DsrLine dsrLine, ClaimResult claimResult, boolean excludeClaim0, MMSpecialCharCcid m) throws IOException;

	public abstract void printTrailer(DsrMetadata dsrMetadata) throws IOException;

}
