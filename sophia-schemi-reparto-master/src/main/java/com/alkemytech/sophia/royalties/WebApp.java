package com.alkemytech.sophia.royalties;

import java.net.ServerSocket;
import java.nio.charset.Charset;
import java.util.EnumSet;

import javax.servlet.DispatcherType;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.royalties.blacklist.BlackListService;
import com.alkemytech.sophia.royalties.guice.ApplicationServletModule;
import com.alkemytech.sophia.royalties.guice.JettyGuiceBootstrap;
import com.alkemytech.sophia.royalties.role.RoleCatalog;
import com.alkemytech.sophia.royalties.scheme.IrregularityCatalog;
import com.alkemytech.sophia.royalties.scheme.RoyaltySchemeService;
import com.alkemytech.sophia.royalties.scheme.TemporaryWorkCatalog;
import com.alkemytech.sophia.royalties.servlet.LockKbService;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Scopes;
import com.google.inject.name.Named;
import com.google.inject.servlet.GuiceFilter;

public class WebApp {
	
	private static final Logger logger = LoggerFactory.getLogger(WebApp.class);
		
	private static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// amazon service(s)
			bind(S3.class)
				.in(Scopes.SINGLETON);
			// other(s)
			bind(RoleCatalog.class)
				.asEagerSingleton();
			bind(TemporaryWorkCatalog.class)
				.asEagerSingleton();
			bind(IrregularityCatalog.class)
				.asEagerSingleton();
			bind(Configuration.class)
				.asEagerSingleton();
			bind(RoyaltySchemeService.class)
				.asEagerSingleton();
			bind(BlackListService.class)
				.asEagerSingleton();
			bind(WebApp.class)
				.asEagerSingleton();
			bind(LockKbService.class)
			.asEagerSingleton();
		}
	}
	
	public static Injector createInjector(String[] args) {
		return Guice.createInjector(
				new GuiceModuleExtension(args, "/webapp-search.properties"),
				new ApplicationServletModule()
				);
	}
	
	public static void main(String[] args) {
		try {
			// WARNING: same injector used for container guice bootstrap
			final Injector injector = createInjector(args);
			final WebApp instance = injector
					.getInstance(WebApp.class)
					.startup();
			try {
				instance.process(injector);
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}

	private final Configuration configuration;
	private final S3 s3;
	private final Charset charset;
	@Inject
	protected WebApp(
			Configuration configuration,
			S3 s3,
			@Named("charset") Charset charset	
			) {
		super();
		this.configuration = configuration;
		this.s3 = s3;
		this.charset = charset;
	}
	
	public WebApp startup() {
		if ("true".equalsIgnoreCase(configuration
				.getProperty("webapp_search.locked", "true"))) {
			throw new IllegalStateException("application locked");
		}
		return this;
	}

	public WebApp shutdown() {
		return this;
	}

	private WebApp process(Injector injector) throws Exception {
		final int port = Integer.parseInt(configuration
				.getProperty("webapp_search.port", "8080"));
		final int bindPort = Integer.parseInt(configuration
				.getProperty("webapp_search.bind_port", configuration
						.getProperty("webapp_search.bind_port", "0")));
		try (final ServerSocket socket = new ServerSocket(bindPort)) {
			logger.debug("lock socket bound to {}", socket.getLocalSocketAddress());	
			final Server server = new Server(port);
			final ServletContextHandler servletContextHandler = new ServletContextHandler(server,"/", ServletContextHandler.NO_SECURITY | ServletContextHandler.NO_SESSIONS);
			servletContextHandler.addEventListener(new JettyGuiceBootstrap(injector));
			servletContextHandler.addFilter(GuiceFilter.class, "/*", EnumSet.allOf(DispatcherType.class));
			servletContextHandler.addServlet(DefaultServlet.class, "/");
			server.start();
			server.join();
		}
		return this;
	}
}
