package com.alkemytech.sophia.royalties.nosql;

import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;

import com.alkemytech.sophia.commons.mmap.ObjectCodec;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class IpiSocCodec extends ObjectCodec<IpiSoc> {

	private final Charset charset;
	
	public IpiSocCodec(Charset charset) {
		super();
		this.charset = charset;
	}

	@Override
	public IpiSoc bytesToObject(byte[] bytes) {
		beginDecoding(bytes);
		final IpiSoc object = new IpiSoc(getPackedLongEx());
		final int demMapSize = getPackedIntEx();
		for (int irol = 0; irol < demMapSize; irol ++) {
			final String rol = getString(charset);
			final int socListSize = getPackedIntEx();
			for (int isoc = 0; isoc < socListSize; isoc ++) {
				final String socname = getString(charset);
				final float shr = getFloat();
				final Territories territories = new Territories(getString(charset));
				object.add(IpiSoc.DEM, rol, socname, shr, territories);
			}
		}
		final int drmMapSize = getPackedIntEx();
		for (int irol = 0; irol < drmMapSize; irol ++) {
			final String rol = getString(charset);
			final int socListSize = getPackedIntEx();
			for (int isoc = 0; isoc < socListSize; isoc ++) {
				final String socname = getString(charset);
				final float shr = getFloat();
				final Territories territories = new Territories(getString(charset));
				object.add(IpiSoc.DRM, rol, socname, shr, territories);
			}
		}
		return object;
	}

	@Override
	public byte[] objectToBytes(IpiSoc object) {
		beginEncoding();
		putPackedLongEx(object.ipnamenr);
		putPackedIntEx(object.demMap.size());
		for (Map.Entry<String, List<Society>> rolEntry : object.demMap.entrySet()) {
			putString(rolEntry.getKey(), charset);
			final List<Society> societies = rolEntry.getValue();
			putPackedIntEx(societies.size());
			for (Society society : societies) {
				putString(society.socname, charset);
				putFloat(society.shr);						
				putString(society.territories.toString(), charset);
			}
		}
		putPackedIntEx(object.drmMap.size());
		for (Map.Entry<String, List<Society>> rolEntry : object.drmMap.entrySet()) {
			putString(rolEntry.getKey(), charset);
			final List<Society> societies = rolEntry.getValue();
			putPackedIntEx(societies.size());
			for (Society society : societies) {
				putString(society.socname, charset);
				putFloat(society.shr);						
				putString(society.territories.toString(), charset);
			}
		}
		return commitEncoding();
	}
	
}
