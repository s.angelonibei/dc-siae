package com.alkemytech.sophia.royalties.nosql;

import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class Society {
	
	public final String socname;
	public final float shr;
	public final Territories territories;

	public Society(String socname, float shr, Territories territories) {
		super();
		if (null == socname || 0 == socname.length()) {
			throw new IllegalArgumentException("socname is null or empty");
		}
		if (null == territories || territories.isEmpty()) {
			throw new IllegalArgumentException("territories is null or empty");
		}
		this.socname = socname;
		this.shr = shr;
		this.territories = territories;
	}
	
	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}

}

