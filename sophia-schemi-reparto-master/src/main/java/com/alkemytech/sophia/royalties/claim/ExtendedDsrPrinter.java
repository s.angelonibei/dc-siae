package com.alkemytech.sophia.royalties.claim;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.csv.CSVPrinter;

import com.alkemy.siae.sophia.pricing.DsrLine;
import com.alkemytech.sophia.commons.util.BigDecimals;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class ExtendedDsrPrinter {

//	private static final Logger logger = LoggerFactory.getLogger(ExtendedDsrPrinter.class);

	private final CSVPrinter csvPrinter;
	private final AtomicLong printedLines;
	
	public ExtendedDsrPrinter(CSVPrinter csvPrinter) {
		super();
		this.csvPrinter = csvPrinter;
		this.printedLines = new AtomicLong(0L);
	}
	
	public long getPrintedLines() {
		return printedLines.longValue();
	}
	
	public void printRow(DsrLine dsrLine, ClaimResult claimResult) throws IOException {
		if (null == claimResult || !claimResult.isClaimTooMuch()) {
			return;
		}
		printedLines.incrementAndGet();

		// print DSR columns
		for (String column : dsrLine.getCSVRecord()) {
			csvPrinter.print(column);
		}
		
		// print pricing columns
		csvPrinter.print(BigDecimals
				.toPlainString(dsrLine.getPriceBasis()));
		csvPrinter.print(BigDecimals
				.toPlainString(dsrLine.getOriginalPriceBasis()));
		csvPrinter.print(dsrLine.getRoyaltyType());
		csvPrinter.print(BigDecimals
				.toPlainString(dsrLine.getRoyalty()));
		csvPrinter.print(BigDecimals
				.toPlainString(dsrLine.getRoyaltyEur()));
		csvPrinter.print(BigDecimals
				.toPlainString(dsrLine.getPrice()));
		csvPrinter.print(BigDecimals
				.toPlainString(dsrLine.getPriceEur()));
		
		// print claim columns
//		if (null == claimResult) {
//			for (int i = 0; i < 10; i ++) {
//				csvPrinter.print("");
//			}
//		} else {
			csvPrinter.print(claimResult.formatTenantWorkCode());				
			csvPrinter.print(claimResult.formatMandatorWorkCodes());
			csvPrinter.print(claimResult.formatClaimTenantDem());
			csvPrinter.print(claimResult.formatClaimMandatorsDem());
			csvPrinter.print(claimResult.formatClaimTenantDemFraction());
			csvPrinter.print(claimResult.formatClaimMandatorsDemFraction());
			csvPrinter.print(claimResult.formatClaimTenantDrm());
			csvPrinter.print(claimResult.formatClaimMandatorsDrm());
			csvPrinter.print(claimResult.formatClaimTenantDrmFraction());
			csvPrinter.print(claimResult.formatClaimMandatorsDrmFraction());
//		}
			
		// end-of-line
		csvPrinter.println();
	}
	
}
