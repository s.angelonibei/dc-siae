package com.alkemytech.sophia.royalties.claim;

import org.apache.commons.csv.CSVRecord;

import com.alkemy.siae.sophia.pricing.DsrLine;
import com.alkemytech.sophia.royalties.Configuration;
import com.google.common.base.Strings;

import java.math.BigDecimal;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class DsrLineParser {	
	
	private final int idUtil;
	private final int territory;
	private final int currency;
	private final int commercialModel;
	private final int unitaryPrice;
	private final int salesCount;
	private final int amount;
	private final int transactionType;
	private final int releaseType;
	private final int upgradeFlag;
	private final int trackDuration;
	private final int pricingModel;
	private final int businessOffer;
	private final int albumTracks;
	private final int transactionTracks;
	private final int uuidSophia;
	private final int title;
	private final int artists;
	private final int isrc;
	private final int iswc;
	private final int transactionId;
	private final int proprietrayId;
	private final int albumProprietrayId;
	private final int matchType;
	
	public DsrLineParser(Configuration configuration, String propertyPrefix) {
		super();
		this.idUtil = Integer.parseInt(configuration.getProperty(propertyPrefix + ".column.id_util", "-1"));
		this.territory = Integer.parseInt(configuration.getProperty(propertyPrefix + ".column.territory", "-1"));
		this.currency = Integer.parseInt(configuration.getProperty(propertyPrefix + ".column.currency", "-1"));
		this.commercialModel = Integer.parseInt(configuration.getProperty(propertyPrefix + ".column.commercial_model", "-1"));
		this.unitaryPrice = Integer.parseInt(configuration.getProperty(propertyPrefix + ".column.unitary_price", "-1"));
		this.salesCount = Integer.parseInt(configuration.getProperty(propertyPrefix + ".column.sales_count", "-1"));
		this.amount = Integer.parseInt(configuration.getProperty(propertyPrefix + ".column.amount", "-1"));
		this.transactionType = Integer.parseInt(configuration.getProperty(propertyPrefix + ".column.transaction_type", "-1"));
		this.releaseType = Integer.parseInt(configuration.getProperty(propertyPrefix + ".column.release_type", "-1"));
		this.upgradeFlag = Integer.parseInt(configuration.getProperty(propertyPrefix + ".column.upgrade_flag", "-1"));
		this.trackDuration = Integer.parseInt(configuration.getProperty(propertyPrefix + ".column.track_duration", "-1"));
		this.pricingModel = Integer.parseInt(configuration.getProperty(propertyPrefix + ".column.pricing_model", "-1"));
		this.businessOffer = Integer.parseInt(configuration.getProperty(propertyPrefix + ".column.business_offer", "-1"));
		this.albumTracks = Integer.parseInt(configuration.getProperty(propertyPrefix + ".column.album_tracks", "-1"));
		this.transactionTracks = Integer.parseInt(configuration.getProperty(propertyPrefix + ".column.transaction_tracks", "-1"));
		this.uuidSophia = Integer.parseInt(configuration.getProperty(propertyPrefix + ".column.uuid_sophia", "-1"));
		this.title = Integer.parseInt(configuration.getProperty(propertyPrefix + ".column.title", "-1"));
		this.artists = Integer.parseInt(configuration.getProperty(propertyPrefix + ".column.artists", "-1"));
		this.isrc = Integer.parseInt(configuration.getProperty(propertyPrefix + ".column.isrc", "-1"));
		this.iswc = Integer.parseInt(configuration.getProperty(propertyPrefix + ".column.iswc", "-1"));
		this.transactionId = Integer.parseInt(configuration.getProperty(propertyPrefix + ".column.transaction_id", "-1"));
		this.proprietrayId = Integer.parseInt(configuration.getProperty(propertyPrefix + ".column.proprietary_id", "-1"));
		this.albumProprietrayId = Integer.parseInt(configuration.getProperty(propertyPrefix + ".column.album_proprietary_id", "-1"));
		this.matchType = Integer.parseInt(configuration.getProperty(propertyPrefix + ".column.match_type", "-1"));
	}

	public DsrLine parse(CSVRecord record) {
		final DsrLine line = new DsrLine(record);
		line.setIdUtil(safeGet(record, idUtil, null));
		line.setTerritory(safeGet(record, territory, null));
		line.setCurrency(safeGet(record, currency, null));
		line.setCommercialModel(safeGet(record, commercialModel, null));
		line.setUnitaryPrice(safeGet(record, unitaryPrice, null));
		line.setSalesCount(new BigDecimal(safeGet(record, salesCount, "0")));
		line.setAmount(safeGet(record, amount, null));
		line.setTransactionType(safeGet(record, transactionType, null));
		line.setReleaseType(safeGet(record, releaseType, null));
		line.setUpgradeFlag(safeGet(record, upgradeFlag, null));
		line.setTrackDuration(safeGet(record, trackDuration, null));
		line.setPricingModel(safeGet(record, pricingModel, null));
		line.setBusinessOffer(safeGet(record, businessOffer, null));
		line.setAlbumTracks(safeGet(record, albumTracks, "1"));
		line.setTransactionTracks(safeGet(record, transactionTracks, "1"));
		line.setUuidSophia(safeGet(record, uuidSophia, null));
		line.setTitle(safeGet(record, title, null));
		line.setArtists(safeGet(record, artists, null));
		line.setIsrc(safeGet(record, isrc, null));
		line.setIswc(safeGet(record, iswc, null));
		line.setTransactionId(safeGet(record, transactionId, null));
		line.setProprietrayId(safeGet(record, proprietrayId, null));
		line.setAlbumProprietrayId(safeGet(record, albumProprietrayId, null));		
		line.setMatchType(safeGet(record, matchType, null));		
		return line;
	}
	
	private String safeGet(CSVRecord record, int index, String defaultValue) {
		if (-1 == index) {
			return defaultValue;
		}
		try {
			final String value = record.get(index);
			return Strings.isNullOrEmpty(value) ? defaultValue : value;
		} catch (IndexOutOfBoundsException e) {
			return defaultValue;
		}
	}

}
