package com.alkemytech.sophia.royalties.nosql;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alkemytech.sophia.commons.nosql.NoSqlEntity;
import com.alkemytech.sophia.commons.nosql.NoSqlException;
import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class IpiSoc implements NoSqlEntity {

	public static final String DEM = "DEM";
	public static final String DRM = "DRM";
	
	protected static String ipnamenrToPk(long ipnamenr) throws NoSqlException {
		return Long.toHexString(ipnamenr);
	}
	
	private static final boolean isDem(String rgt) {
		return DEM.equals(rgt);
	}
	
	private static final boolean isDrm(String rgt) {
		return DRM.equals(rgt);
	}

	// {
	//   "ipnamenr": "1234567890",
	//   "demMap": {
	//     "LY": [{
	//       "socname": "SIAE",
	//       "shr": 100,
	//       "territories": "IT",
	//     }],
	//     "MC": [{
	//       "socname": "SIAE",
	//       "shr": 100,
	//       "territories": "ITUK",
	//     }],
	//   },
	//   "drmMap": {
	//     "MC": [{
	//       "socname": "SIAE",
	//       "shr": 50,
	//       "territories": "IT",
	//     },{
	//       "socname": "EMI",
	//       "shr": 50,
	//       "territories": "AUCHITUKUS",
	//     }],
	//   },
	// }
	
	public final long ipnamenr;
	public final Map<String, List<Society>> demMap;
	public final Map<String, List<Society>> drmMap;
	
	public IpiSoc(long ipnamenr) {
		super();
		this.ipnamenr = ipnamenr;
		this.demMap = new HashMap<>();
		this.drmMap = new HashMap<>();
	}
	
	public IpiSoc add(String rgt, String rol, String socname, float shr, Territories territories) {
		return add(rgt, rol, new Society(socname, shr, territories));
	}
	
	public IpiSoc add(String rgt, String rol, Society society) {
		final Map<String, List<Society>> rolMap;
		if (isDem(rgt)) {
			rolMap = demMap;
		} else if (isDrm(rgt)) {
			rolMap = drmMap;
		} else {
			throw new IllegalArgumentException("illegal right type " + rgt);
		}
		List<Society> societies = rolMap.get(rol); // MC, LY, EM, ...
		if (null == societies) {
			societies = new ArrayList<>(2);
			rolMap.put(rol, societies);
		}
		societies.add(society);
		return this;
	}

	public List<Society> get(String rgt, String rol, String territory) {
		final Map<String, List<Society>> rolMap;
		if (isDem(rgt)) {
			rolMap = demMap;
		} else if (isDrm(rgt)) {
			rolMap = drmMap;
		} else {
			throw new IllegalArgumentException("illegal right type " + rgt);
		}
		final List<Society> societies = rolMap.get(rol); // MC, LY, EM, ...
		if (null == societies) {
			return null;
		}
		final List<Society> result = new ArrayList<>();
		for (Society society : societies) {
			if (society.territories.contains(territory)) {
				result.add(new Society(society.socname,
						society.shr, new Territories(territory)));
			}
		}
		return result;
	}
	
	public List<Society> get(String rgt, String rol, Territories territories) {
		final Map<String, List<Society>> rolMap;
		if (isDem(rgt)) {
			rolMap = demMap;
		} else if (isDrm(rgt)) {
			rolMap = drmMap;
		} else {
			throw new IllegalArgumentException("illegal right type " + rgt);
		}
		final List<Society> societies = rolMap.get(rol); // MC, LY, EM, ...
		if (null == societies) {
			return null;
		}
		final List<Society> result = new ArrayList<>();
		for (Society society : societies) {
			final Territories intersection = 
					society.territories.intersection(territories);
			if (!intersection.isEmpty()) {
				result.add(new Society(society.socname, society.shr, intersection));
			}
		}
		return result;
	}
	
	@Override
	public String getPrimaryKey() {
		return ipnamenrToPk(ipnamenr);
	}

	@Override
	public String getSecondaryKey() {
		return null;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}
	
}
