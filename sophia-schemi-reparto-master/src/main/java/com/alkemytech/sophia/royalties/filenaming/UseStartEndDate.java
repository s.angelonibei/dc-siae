package com.alkemytech.sophia.royalties.filenaming;

public enum UseStartEndDate {
    yyyy("yyyy"),
    yyyy_mm("yyyy-mm"),
    yyyy_mm_dd("yyyy-mm-dd"),
    yyyy_qq("yyyy-Qq"),
    yyyy_www("yyyy-Www"),
    yyyy_mm_dd__yyyy_mm_dd("yyyy-mm-dd--yyyy-mm-dd");


    private String useStartEndDate;

    UseStartEndDate(String useStartEndDate){
        this.useStartEndDate = useStartEndDate;
    }

    public String getUseStartEndDate() {
        return useStartEndDate;
    }
}
