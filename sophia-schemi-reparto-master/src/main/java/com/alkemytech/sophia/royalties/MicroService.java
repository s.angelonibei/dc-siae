package com.alkemytech.sophia.royalties;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.util.FileUtils;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.royalties.blacklist.BlackList;
import com.alkemytech.sophia.royalties.claim.CaricoRipartizione;
import com.alkemytech.sophia.royalties.claim.SocietyInfo;
import com.alkemytech.sophia.royalties.nosql.QuotaRiparto;
import com.alkemytech.sophia.royalties.country.CountryService;
import com.alkemytech.sophia.royalties.role.RoleCatalog;
import com.google.common.base.Strings;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public abstract class MicroService {

    private static final Logger logger = LoggerFactory.getLogger(MicroService.class);

    protected final Configuration configuration;
    protected final Charset charset;
    protected final S3 s3;

    protected MicroService(Configuration configuration, Charset charset, S3 s3) {
        super();
        this.configuration = configuration;
        this.charset = charset;
        this.s3 = s3;
    }

    protected MicroService startup() throws IOException {
        s3.startup();
        return this;
    }

    protected MicroService shutdown() throws IOException {
        s3.shutdown();
        return this;
    }

    protected long computeSize(File fileOrFolder, Set<String> pathsToSkip) {
        if (null != pathsToSkip) {
            for (String pathToSkip : pathsToSkip) {
                if (fileOrFolder.getAbsolutePath()
                        .startsWith(pathToSkip)) {
                    return 0L;
                }
            }
        }
        if (fileOrFolder.isDirectory()) {
            long size = 0L;
            for (File file : fileOrFolder.listFiles()) {
                size += computeSize(file, pathsToSkip);
            }
            return size;
        }
        return fileOrFolder.length();
    }

    protected Map<String, Map<String, String>> parseJsonArrayAsMapOfObjects(String keyName, String json) {
        final JsonArray array = GsonUtils.fromJson(json, JsonArray.class);
        final Map<String, Map<String, String>> objects = new HashMap<>(array.size());
        for (JsonElement element : array) {
            final Map<String, String> object = GsonUtils.getAsHashMap(element);
            if (null != object && !object.isEmpty()) {
                final String keyValue = object.get(keyName);
                if (null != keyValue) {
                    objects.put(keyValue, object);
                }
            }
        }
        return objects;
    }

    protected String fixPeriod(String month) {
        try {
            month = String.format("%02d", Integer.parseInt(month));
        } catch (NumberFormatException ignore) {
        }
        return month;
    }

    protected String fixMonth(String month) {
        try {
            month = String.format("%02d", Integer.parseInt(month));
        } catch (NumberFormatException e) {
            if (!Strings.isNullOrEmpty(month)) {
                if (-1 != "Q1|Q2|Q3|Q4".indexOf(month.toUpperCase())) {
                    month = String.format("%02d",
                            1 + 3 * (Integer.parseInt(month.substring(1)) - 1));
                } else if (-1 != "S1|S2".indexOf(month.toUpperCase())) {
                    month = String.format("%02d",
                            1 + 6 * (Integer.parseInt(month.substring(1)) - 1));
                }
            }
        }
        return month;
    }

    protected void archiveAndUpload(String propertyPrefix, File homeFolder, File archiveFolder, JsonObject output) throws IOException, InterruptedException {

        // archive file
        final File archiveFile = File.createTempFile("__tmp__", ".tgz", homeFolder);
        logger.debug("archiveAndUpload: archiveFile {}", archiveFile);
        archiveFile.delete();
        if ("true".equalsIgnoreCase(configuration
                .getProperty(propertyPrefix + ".archive_file.delete_when_done", "true"))) {
            archiveFile.deleteOnExit();
        }

        // tar archive file
        final long lapTimeMillis = System.currentTimeMillis();
        final String[] cmdline = new String[]{
                configuration.getProperty(propertyPrefix + ".tar_path",
                        configuration.getProperty("default.tar_path", "/usr/bin/tar")),
                "-C", archiveFolder.getAbsolutePath(),
                "-czf", archiveFile.getAbsolutePath(),
                "."
        };
        logger.debug("archiveAndUpload: cmdline {}", Arrays.asList(cmdline));
        final int exitCode = Runtime
                .getRuntime().exec(cmdline).waitFor();
        logger.debug("archiveAndUpload: exitCode {}", exitCode);

        // output stats
        output.addProperty("tar_duration", TextUtils
                .formatDuration(System.currentTimeMillis() - lapTimeMillis));

        // upload archive file
        upload(propertyPrefix, homeFolder, archiveFile, output);

    }

    protected void upload(String propertyPrefix, File homeFolder, File archiveFile, JsonObject output) throws IOException, InterruptedException {

        // check minimum archive file size
        if (archiveFile.length() < TextUtils.parseLongSize(configuration
                .getProperty(propertyPrefix + ".archive_file.min_size", "0"))) {
            throw new IOException("result archive too small " + archiveFile.length());
        }

        // upload archive file
        final String archiveUrl = configuration.getProperty(propertyPrefix + ".archive_url");
        if (!s3.upload(new S3.Url(archiveUrl), archiveFile)) {
            throw new IOException("error uploading " + archiveFile + " to " + archiveUrl);
        }

        // .latest file
        final File latestFile = File.createTempFile("__tmp__", ".latest", homeFolder);
        latestFile.delete();
        latestFile.deleteOnExit();

        // .latest version
        final LatestVersion latestVersion = new LatestVersion()
                .setLocation(archiveUrl)
                .setVersion(UUID.randomUUID().toString());
        logger.debug("upload: latestVersion {}", latestVersion);

        // save .latest version
        latestVersion.save(latestFile);

        // upload .latest file
        final String latestUrl = configuration.getProperty(propertyPrefix + ".latest_url");
        if (!s3.upload(new S3.Url(latestUrl), latestFile)) {
            throw new IOException("error uploading " + latestFile + " to " + latestUrl);
        }

        // output stats
        output.addProperty("archive_file_url", archiveUrl);
        output.addProperty("archive_file_size", archiveFile.length());
        output.addProperty("latest_file_url", latestUrl);
        output.addProperty("latest_file_size", latestFile.length());

    }

    protected void initializeCountryService(CountryService countryService, File homeFolder) throws IOException {

        // .latest url
        final String latestUrl = configuration
                .getProperty("country_service.json.latest_url");
        logger.warn("initializeCountryService: latestUrl {}", latestUrl);

        // load .latest version
        final LatestVersion latestVersion = new LatestVersion()
                .load(s3, latestUrl);
        logger.warn("initializeCountryService: latestVersion {}", latestVersion);

        // json url
        final String jsonUrl = latestVersion.getLocation();

        // json file
        final File jsonFile = File.createTempFile("__tmp__",
                "__" + jsonUrl.substring(1 + jsonUrl.lastIndexOf('/')), homeFolder);
        jsonFile.deleteOnExit();
        logger.warn("initializeCountryService: jsonFile {}", jsonFile.getAbsolutePath());

        // download json file
        if (!s3.download(new S3.Url(jsonUrl), jsonFile)) {
            throw new IOException("file download error: " + jsonUrl);
        }

        // initialize country service
        countryService.load(jsonFile);

        // delete json file
        jsonFile.delete();

    }

    protected void cleanupFolder(File folder, long maxSize, Set<String> pathsToSkip) {
        if (maxSize <= 0L) {
            return;
        }
        final long size = computeSize(folder, pathsToSkip);
        logger.warn("cleanupFolder: size {} ({})", size, TextUtils.formatSize(size));
        if (size <= maxSize) {
            return;
        }
        for (File file : folder.listFiles()) {
            boolean delete = true;
            if (null != pathsToSkip) {
                for (String pathToSkip : pathsToSkip) {
                    if (file.getAbsolutePath()
                            .startsWith(pathToSkip)) {
                        delete = false;
                        break;
                    }
                }
            }
            if (delete) {
                if (file.isDirectory()) {
                    logger.debug("deleting folder {}", file.getName());
                    FileUtils.deleteFolder(file, true);
                } else {
                    logger.debug("deleting file {} ({})", file.getName(),
                            TextUtils.formatSize(file.length()));
                    file.delete();
                }
            }
        }
    }

    protected void downloadAndExtractLatest(String propertyPrefix, String propertyPrefixArchive) throws IOException, InterruptedException {
        // data folder
        final File dataFolder = new File(configuration
                .getProperty(propertyPrefix + ".data_folder"));
        dataFolder.mkdirs();

        // .latest file
        final File latestFile = new File(configuration
                .getProperty(propertyPrefixArchive + ".latest_file"));
        logger.debug("downloadAndExtractLatest: latestFile {}", latestFile);

        // load local .latest version
        final LatestVersion localLatestVersion = new LatestVersion()
                .load(latestFile);
        logger.debug("downloadAndExtractLatest: localLatestVersion {}", localLatestVersion);

        // .latest url
        final String latestUrl = configuration
                .getProperty(propertyPrefixArchive + ".latest_url");
        logger.debug("downloadAndExtractLatest: latestUrl {}", latestUrl);

        // temporary latest file
        final File temporaryLatestFile = File
                .createTempFile("__tmp__", "__.latest", dataFolder);
        temporaryLatestFile.deleteOnExit();
        logger.debug("downloadAndExtractLatest: temporaryLatestFile {}", temporaryLatestFile);

        // download .latest version
        if (!s3.download(new S3.Url(latestUrl), temporaryLatestFile)) {
            temporaryLatestFile.delete();
            throw new IOException("file download error: " + latestUrl);
        }

        // load downloaded .latest version
        final LatestVersion latestVersion = new LatestVersion()
                .load(temporaryLatestFile);
        logger.debug("downloadAndExtractLatest: latestVersion {}", latestVersion);

        // check version
        if (localLatestVersion.getVersion("")
                .equals(latestVersion.getVersion())) {
            logger.info("downloadAndExtractLatest: version unchanged");
            return;
        }

        // archive url
        final String archiveUrl = latestVersion.getLocation();

        // archive file
        final File archiveFile = File.createTempFile("__tmp__",
                "__" + archiveUrl.substring(1 + archiveUrl.lastIndexOf('/')), dataFolder);
        archiveFile.deleteOnExit();
        logger.debug("downloadAndExtractLatest: archiveFile {}", archiveFile);

        // download archive
        if (!s3.download(new S3.Url(archiveUrl), archiveFile)) {
            throw new IOException("file download error: " + archiveUrl);
        }

        // destination home folder
        final File destinationFolder = new File(configuration
                .getProperty(propertyPrefixArchive + ".home_folder"));

        // delete existing destination folder
        FileUtils.deleteFolder(destinationFolder, true);
        destinationFolder.mkdirs();

        // untar archive file
        final String[] cmdline = new String[]{
                configuration.getProperty(propertyPrefix + ".tar_path",
                        configuration.getProperty("default.tar_path", "/usr/bin/tar")),
                "-C", destinationFolder.getAbsolutePath(),
                "-xzf", archiveFile.getAbsolutePath()
        };
        logger.debug("downloadAndExtractLatest: exec cmdline {}", Arrays.asList(cmdline));
        final int exitCode = Runtime
                .getRuntime().exec(cmdline).waitFor();
        logger.debug("downloadAndExtractLatest: exec exitCode {}", exitCode);

        // delete archive file
        archiveFile.delete();

        // overwrite local latest file
        if (FileUtils.copy(temporaryLatestFile, latestFile) < 0L) {
            throw new IOException("unable to update local latest file: " + latestFile);
        }

        // delete temporary latest file
        temporaryLatestFile.delete();
    }

    protected SocietyInfo parseSocietyInfo(String propertyPrefix, JsonElement societyJson, RoleCatalog roleCatalog) throws IOException {
        final JsonObject info = GsonUtils.getAsJsonObject(societyJson);
        final String society = GsonUtils.getAsString(info,
                configuration.getProperty(propertyPrefix + ".society"));
        return new SocietyInfo()
                .setSociety(society)
                .setTerritory(GsonUtils.getAsString(info,
                        configuration.getProperty(propertyPrefix + ".territory")))
                .setBlackList(new BlackList(charset, roleCatalog, society)
                        .load(s3, GsonUtils.getAsString(info,
                                configuration.getProperty(propertyPrefix + ".black_list_url"))));
    }

    protected CaricoRipartizione parseCaricoRipartizione(String propertyPrefix, JsonElement caricoRipartizioneJson) throws IOException {
        final JsonObject info = GsonUtils.getAsJsonObject(caricoRipartizioneJson);
        return new CaricoRipartizione()
                .setSociety(GsonUtils.getAsString(info,
                        configuration.getProperty(propertyPrefix + ".society")))
                .setDataFattura(GsonUtils.getAsString(info,
                        configuration.getProperty(propertyPrefix + ".data_fattura")))
                .setNumeroFattura(GsonUtils.getAsString(info,
                        configuration.getProperty(propertyPrefix + ".numero_fattura")))
                .setValoreDem(new BigDecimal(GsonUtils.getAsString(info,
                        configuration.getProperty(propertyPrefix + ".valore_dem"))))
                .setValoreDrm(new BigDecimal(GsonUtils.getAsString(info,
                        configuration.getProperty(propertyPrefix + ".valore_drm"))))
                .setOutputUrl(GsonUtils.getAsString(info,
                        configuration.getProperty(propertyPrefix + ".output_url")));
    }



}
