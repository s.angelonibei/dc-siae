package com.alkemytech.sophia.royalties.monitoraggio648;

import com.alkemy.siae.sophia.pricing.DsrMetadata;
import com.alkemytech.sophia.commons.http.HTTP;
import com.alkemytech.sophia.commons.util.BigDecimals;
import com.alkemytech.sophia.royalties.Configuration;
import com.alkemytech.sophia.royalties.claim.CaricoRipartizione;
import com.alkemytech.sophia.royalties.claim.ClaimResult;
import com.google.inject.Inject;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;

@Data
public class Monitoraggio648 {

    private static final Logger logger = LoggerFactory.getLogger(Monitoraggio648.class);

    private final Configuration configuration;
    private final HTTP http;

    private DsrMetadata dsr;

    @Inject
    public Monitoraggio648(Configuration configuration,
                           HTTP http){
        this.configuration = configuration;
        this.http = http;
    }

    public void saveToTable(DsrMetadata dsrMetadata, CaricoRipartizione cr, Long insertTime, Long updateTime, String stato, String errorDescr) throws IOException {
            if (dsrMetadata != null) {
                setDsr(dsrMetadata);
            }
            String urlSaveWorkingDsr = configuration.getProperty("distribution.save_working_dsr");
            HashMap<String, String> hashmapParam = new HashMap<>();
            if (dsr.getDsp() != null)
                hashmapParam.put("idDsp", dsr.getDsp());
            if (insertTime != null)
                hashmapParam.put("dataInserimento", String.valueOf(insertTime));
            if (updateTime != null)
                hashmapParam.put("dataAggiornamento", String.valueOf(updateTime));
            if (stato != null)
                hashmapParam.put("stato", stato);
            if (cr != null) {
                if (!cr.getSociety().isEmpty())
                    hashmapParam.put("society", cr.getSociety());
                if (cr.getValoreDem() != null && cr.getValoreDrm() != null) {
                    hashmapParam.put("valoreDem", BigDecimals
                            .toPlainString(cr.getValoreDem()));
                    hashmapParam.put("valoreDrm", BigDecimals
                            .toPlainString(cr.getValoreDrm()));
                }
            }
            if (errorDescr != null)
                hashmapParam.put("errorDesc", errorDescr);
            HTTP.Response response = http.post(urlSaveWorkingDsr, hashmapParam);
            logger.debug("response status is: " + response.status);
    }

    public void setDsr(DsrMetadata dsrMetadata){
        this.dsr = dsrMetadata;
        configuration.setTag("{dsr}", dsr.getIdDsr());
    }
}
