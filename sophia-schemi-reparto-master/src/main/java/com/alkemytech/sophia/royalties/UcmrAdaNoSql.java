package com.alkemytech.sophia.royalties;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.ServerSocket;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.QuoteMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.io.CompressionAwareFileInputStream;
import com.alkemytech.sophia.commons.nosql.NoSql.Selector;
import com.alkemytech.sophia.commons.nosql.NoSqlException;
import com.alkemytech.sophia.commons.sqs.McmdbMessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.MessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.SqsMessageHelper;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.alkemytech.sophia.commons.util.FileUtils;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.HeartBeat;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.royalties.country.Country;
import com.alkemytech.sophia.royalties.country.CountryService;
import com.alkemytech.sophia.royalties.country.JsonCountryService;
import com.alkemytech.sophia.royalties.jdbc.McmdbDataSource;
import com.alkemytech.sophia.royalties.nosql.Opera;
import com.alkemytech.sophia.royalties.nosql.OperaNoSqlDb;
import com.alkemytech.sophia.royalties.nosql.QuotaRiparto;
import com.alkemytech.sophia.royalties.nosql.SchemaRiparto;
import com.alkemytech.sophia.royalties.nosql.Territories;
import com.alkemytech.sophia.royalties.nosql.UcmrAdaRow;
import com.alkemytech.sophia.royalties.nosql.UcmrAdaWork;
import com.alkemytech.sophia.royalties.nosql.UcmrAdaWorkNoSqlDb;
import com.google.common.base.Strings;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Scopes;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import com.ibm.icu.text.SimpleDateFormat;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class UcmrAdaNoSql extends MicroService {

    private static final Logger logger = LoggerFactory.getLogger(UcmrAdaNoSql.class);

    protected static class GuiceModuleExtension extends GuiceModule {

        public GuiceModuleExtension(String[] args, String resourceName) {
            super(args, resourceName);
        }

        @Override
        protected void configure() {
            super.configure();
            // amazon service(s)
            bind(S3.class)
                    .in(Scopes.SINGLETON);
            bind(SQS.class)
                    .in(Scopes.SINGLETON);
            // data source(s)
            bind(DataSource.class)
                    .annotatedWith(Names.named("MCMDB"))
                    .to(McmdbDataSource.class)
                    .asEagerSingleton();
            // message deduplicator(s)
            bind(MessageDeduplicator.class)
                    .to(McmdbMessageDeduplicator.class)
                    .in(Scopes.SINGLETON);
            // other binding(s)
            bind(Configuration.class)
                    .asEagerSingleton();
            bind(CountryService.class)
                    .to(JsonCountryService.class)
                    .asEagerSingleton();
            bind(UcmrAdaNoSql.class)
                    .asEagerSingleton();
        }

    }

    public static void main(String[] args) {
        try {
            final UcmrAdaNoSql instance = Guice
                    .createInjector(new GuiceModuleExtension(args,
                            "/ucmr-ada-nosql.properties"))
                    .getInstance(UcmrAdaNoSql.class)
                    .startup();
            try {
                instance.process(args);
            } finally {
                instance.shutdown();
            }
        } catch (Throwable e) {
            logger.error("main", e);
        } finally {
            System.exit(0);
        }
    }

    private final SQS sqs;
    private final CountryService countryService;
    private final MessageDeduplicator deduplicator;

    @Inject
    protected UcmrAdaNoSql(Configuration configuration,
                           @Named("charset") Charset charset,
                           S3 s3, SQS sqs, CountryService countryService,
                           MessageDeduplicator deduplicator) {
        super(configuration, charset, s3);
        this.sqs = sqs;
        this.countryService = countryService;
        this.deduplicator = deduplicator;
    }

    public UcmrAdaNoSql startup() throws IOException {
        if ("true".equalsIgnoreCase(configuration.getProperty("ucmr_ada_nosql.locked", "true"))) {
            throw new IllegalStateException("application locked");
        }
        super.startup();
        sqs.startup();
        return this;
    }

    public UcmrAdaNoSql shutdown() throws IOException {
        sqs.shutdown();
        super.shutdown();
        return this;
    }

    public void process(String[] args) throws Exception {
        final int bindPort = Integer.parseInt(configuration.getProperty("ucmr_ada_nosql.bind_port",
                configuration.getProperty("default.bind_port", "0")));
        // bind lock tcp port
        try (ServerSocket socket = new ServerSocket(bindPort)) {
            logger.debug("socket bound to {}", socket.getLocalSocketAddress());

            // standalone mode
            if (Arrays.asList(args).contains("standalone") ||
                    "true".equalsIgnoreCase(configuration
                            .getProperty("ucmr_ada_nosql.standalone", "false"))) {
                final JsonObject input = GsonUtils.fromJson(configuration
                        .getProperty("ucmr_ada_nosql.standalone.message_body"), JsonObject.class);
                final JsonObject output = new JsonObject();
                logger.debug("standalone input {}", new GsonBuilder()
                        .setPrettyPrinting().create().toJson(input));
                processMessage(input, output);
                logger.debug("standalone output {}", new GsonBuilder()
                        .setPrettyPrinting().create().toJson(output));
                return;
            }

            // sqs message pump
            final SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration
                    .getProperties(), "ucmr_ada_nosql.sqs");
//			final MessageDeduplicator deduplicator = new TrieMessageDeduplicator(new File(configuration
//					.getProperty("ucmr_ada_nosql.sqs.deduplicator_folder")));
            sqsMessagePump.pollingLoop(deduplicator, new SqsMessagePump.Consumer() {

                private JsonObject output;
                private JsonObject error;

                @Override
                public JsonObject getStartedMessagePayload(JsonObject message) {
                    output = null;
                    error = null;
                    return SqsMessageHelper.formatContext();
                }

                @Override
                public boolean consumeMessage(JsonObject message) {
                    try {
                        output = new JsonObject();
                        processMessage(GsonUtils
                                .getAsJsonObject(message, "body"), output);
                        error = null;
                        return true;
                    } catch (Exception e) {
                        output = null;
                        error = SqsMessageHelper.formatError(e);
                        return false;
                    }
                }

                @Override
                public JsonObject getCompletedMessagePayload(JsonObject message) {
                    return output;
                }

                @Override
                public JsonObject getFailedMessagePayload(JsonObject message) {
                    return error;
                }

            });
        }
    }

    /**
     * decode territory expression (WORLD, ROMANIA, WL-ROMANIA, etc.)
     */
    private Territories decodeTerritoryExpression(Map<String, String> territoryMapping, String expression) {
        if (null == expression) {
            return null;
        }
        final Set<Country> countries = new HashSet<>();
        char op = '+';
        while (null != expression && expression.length() > 0) {
            final int plus = expression.indexOf('+');
            final int minus = expression.indexOf('-');
            final int min = Math.min(plus, minus);
            final int max = Math.max(plus, minus);
            final char plusOrMinus;
            String term;
            if (-1 != min) {
                term = expression.substring(0, min);
                plusOrMinus = expression.charAt(min);
                expression = expression.substring(1 + min);
            } else if (-1 != max) {
                term = expression.substring(0, max);
                plusOrMinus = expression.charAt(max);
                expression = expression.substring(1 + max);
            } else {
                term = expression;
                plusOrMinus = (char) 0;
                expression = null;
            }
            term = term.trim().toUpperCase();
            final String nome = territoryMapping.get(term);
            final long tisnr = countryService.getIdTerritorio(null == nome ? term : nome, -1L);
            if (-1L == tisnr) {
                throw new IllegalStateException("invalid territory: " + term);
            }
            if ('+' == op) {
                countries.addAll(countryService.getCountries(tisnr));
            } else if ('-' == op) {
                countries.removeAll(countryService.getCountries(tisnr));
            }
            op = plusOrMinus;
        }
        final StringBuilder result = new StringBuilder();
        for (Country country : countries) {
            result.append(country.getTisA());
        }
        return new Territories(result.toString(), true);
    }

    private void processMessage(JsonObject input, JsonObject output) throws Exception {

//		{
//		  "body": {
//			"year": "2018",
//			"month": "11",
//			"society": "UCMR-ADA",\
//		    "force": true
//		  },
//		  "header": {
//		    "queue": "dev_to_process_ucmr_ada_dump_v2",
//		    "timestamp": "2018-05-10T00:00:00.000000+02:00",
//		    "uuid": "7f36a2bb-0637-4d4d-9990-4ed9644adefc",
//		    "sender": "aws-console"
//		  }
//		}

        logger.debug("dumping schemi ucrm-ada");
        final long startTimeMillis = System.currentTimeMillis();
        final String datetime = new SimpleDateFormat(configuration
                .getProperty("ucmr_ada_nosql.datetime_format", configuration
                        .getProperty("default.datetime_format", "yyyyMMddHHmmss")))
                .format(new Date());
        logger.debug("datetime {}", datetime);

        // parse input json message
        String year = GsonUtils.getAsString(input, "year");
        String month = GsonUtils.getAsString(input, "month");
        final String society = GsonUtils.getAsString(input, "society");
        if (Strings.isNullOrEmpty(year) ||
                Strings.isNullOrEmpty(month) ||
                Strings.isNullOrEmpty(society)) {
            throw new IllegalArgumentException("invalid input message");
        }
        String day = GsonUtils.getAsString(input, "day", configuration
                .getProperty("ucmr_ada_nosql.reference_day", "01"));
        final JsonArray stepsJson = GsonUtils.getAsJsonArray(input, "steps");
        final Set<String> steps = new HashSet<>();
        if (null != stepsJson) {
            for (JsonElement stepJson : stepsJson)
                steps.add(stepJson.getAsString());
        } else {
            steps.addAll(Arrays.asList("create", "upload"));
        }
        final String period = fixPeriod(month);
        year = String.format("%04d", Integer.parseInt(year));
        month = fixMonth(month);
        day = String.format("%02d", Integer.parseInt(day));
        logger.debug("year {}", year);
        logger.debug("month {}", month);
        logger.debug("day {}", day);
        logger.debug("period {}", period);
        logger.debug("steps {}", steps);

        // configuration tags
        configuration.setTag("{datetime}", datetime);
        configuration.setTag("{year}", year);
        configuration.setTag("{month}", month);
        configuration.setTag("{day}", day);
        configuration.setTag("{period}", period);
        configuration.setTag("{society}", society);

        // configuration
//		final boolean debug = "true".equalsIgnoreCase(configuration
//				.getProperty("ucmr_ada_nosql.debug", configuration.getProperty("default.debug")));
        final File homeFolder = new File(configuration
                .getProperty("default.home_folder"));
        final long idRepertorio = Long.parseLong(configuration
                .getProperty("ucmr_ada_nosql.id_repertorio", "1"));
        final Set<String> regularStatus = new HashSet<String>(Arrays.asList(configuration
                .getProperty("ucmr_ada_nosql.regular_status", "REGULAR").split(",")));
        final Map<String, String> territoryMapping = GsonUtils.decodeJsonMap(configuration
                .getProperty("ucmr_ada_nosql.territory_mapping", "{}"));

        logger.debug("idRepertorio {}", idRepertorio);
        logger.debug("territoryMapping {}", territoryMapping);

        // initialize country service
        initializeCountryService(countryService, homeFolder);

        // post execution tasks
        final List<Runnable> postExecTasks = new ArrayList<>();

        ///////////
        // create
        ///////////

        if (steps.contains("create")) {

            ////////////
            // csv_row
            ////////////

            if ("true".equalsIgnoreCase(configuration
                    .getProperty("ucmr_ada_nosql.csv_row.read_only"))) {
                configuration.setProperty("ucmr_ada_nosql.csv_row.page_size", "-1");
                configuration.setProperty("ucmr_ada_nosql.csv_row.read_only", "true");
                configuration.setProperty("ucmr_ada_nosql.csv_row.create_always", "false");
            }
            final UcmrAdaWorkNoSqlDb csvRowNoSqlDb = new UcmrAdaWorkNoSqlDb(configuration
                    .getProperties(), "ucmr_ada_nosql.csv_row");

            if (!"true".equalsIgnoreCase(configuration
                    .getProperty("ucmr_ada_nosql.csv_row.read_only"))) {

                final long lapTimeMillis = System.currentTimeMillis();
                logger.debug("creating csv_row nosql");

                int rowsToSkip = Integer.parseInt(configuration
                        .getProperty("ucmr_ada_nosql.csv.rows_to_skip", "0"));
                final char inputDelimiter = configuration
                        .getProperty("ucmr_ada_nosql.csv.delimiter", ",").trim().charAt(0);
                final int columnMasterCode = Integer.parseInt(configuration
                        .getProperty("ucmr_ada_nosql.csv.column.master_code"));
                final int columnWorkNumber = Integer.parseInt(configuration
                        .getProperty("ucmr_ada_nosql.csv.column.work_number"));
//				final int columnTitle = Integer.parseInt(configuration
//						.getProperty("ucmr_ada_nosql.csv.column.title"));
                final int columnRole = Integer.parseInt(configuration
                        .getProperty("ucmr_ada_nosql.csv.column.role"));
                final int columnShareholder = Integer.parseInt(configuration
                        .getProperty("ucmr_ada_nosql.csv.column.shareholder"));
                final int columnDemShare = Integer.parseInt(configuration
                        .getProperty("ucmr_ada_nosql.csv.column.dem_share"));
//				final int columnDemSoc = Integer.parseInt(configuration
//						.getProperty("ucmr_ada_nosql.csv.column.dem_soc"));
                final int columnDrmShare = Integer.parseInt(configuration
                        .getProperty("ucmr_ada_nosql.csv.column.drm_share"));
//				final int columnDrmSoc = Integer.parseInt(configuration
//						.getProperty("ucmr_ada_nosql.csv.column.drm_soc"));
                final int columnIpnamenr = Integer.parseInt(configuration
                        .getProperty("ucmr_ada_nosql.csv.column.ipnamenr"));
//				final int columnIswc = Integer.parseInt(configuration
//						.getProperty("ucmr_ada_nosql.csv.column.iswc"));
//				final int columnPerformer = Integer.parseInt(configuration
//						.getProperty("ucmr_ada_nosql.csv.column.performer"));
//				final int columnAlternativeTitles = Integer.parseInt(configuration
//						.getProperty("ucmr_ada_nosql.csv.column.alternative_titles"));
                final int columnTerritory = Integer.parseInt(configuration
                        .getProperty("ucmr_ada_nosql.csv.column.territory"));
//				final int columnValidFrom = Integer.parseInt(configuration
//						.getProperty("ucmr_ada_nosql.csv.column.valid_from"));
//				final int columnValidTo = Integer.parseInt(configuration
//						.getProperty("ucmr_ada_nosql.csv.column.valid_to"));
                final int columnStatus = Integer.parseInt(configuration
                        .getProperty("ucmr_ada_nosql.csv.column.status"));

                logger.debug("rowsToSkip {}", rowsToSkip);
                logger.debug("inputDelimiter {}", inputDelimiter);

                // download file from s3
                final String latestUrl = configuration
                        .getProperty("ucmr_ada_nosql.csv.latest_url");
                logger.debug("latestUrl {}", latestUrl);

                final LatestVersion latestVersion = new LatestVersion()
                        .load(s3, latestUrl);
                logger.debug("latestVersion {}", latestVersion);

                final String csvUrl = latestVersion.getLocation();
                logger.debug("csvUrl {}", csvUrl);

                final File csvFile = File.createTempFile("__tmp__",
                        "__" + csvUrl.substring(1 + csvUrl.lastIndexOf('/')), homeFolder);
                csvFile.deleteOnExit();

                if (!s3.download(new S3.Url(csvUrl), csvFile)) {
                    throw new IOException("file download error: " + csvUrl);
                }

                // parse csv file
                try (final CompressionAwareFileInputStream in = new CompressionAwareFileInputStream(csvFile);
                     final Reader reader = new InputStreamReader(in, charset);
                     final CSVParser csvParser = new CSVParser(reader, CSVFormat.newFormat(inputDelimiter)
                             .withIgnoreEmptyLines()
                             .withIgnoreSurroundingSpaces()
                             .withQuoteMode(QuoteMode.MINIMAL)
                             .withQuote('"'))) {

                    long count = 0L;
                    final HeartBeat heartbeat = HeartBeat.constant("csv_row", Integer
                            .parseInt(configuration.getProperty("ucmr_ada_nosql.csv_row.heartbeat", "10000")));

                    final Iterator<CSVRecord> csvIterator = csvParser.iterator();

                    // skip initial row(s)
                    for (; rowsToSkip > 0 && csvIterator.hasNext(); rowsToSkip--) {
                        final CSVRecord record = csvIterator.next();
                        logger.debug("record skipped {}", record);
                    }

                    while (csvIterator.hasNext()) {
                        final CSVRecord record = csvIterator.next();

                        final String masterCode = record.get(columnMasterCode);
                        final String workNumber = record.get(columnWorkNumber);
//						final String title = record.get(columnTitle);
                        final String role = record.get(columnRole);
                        final String shareholder = record.get(columnShareholder);
                        final String demShare = record.get(columnDemShare);
//						final String demSoc = record.get(columnDemSoc);
                        final String drmShare = record.get(columnDrmShare);
//						final String drmSoc = record.get(columnDrmSoc);
                        final String ipnamenr = record.get(columnIpnamenr);
//						final String iswc = record.get(columnIswc);
//						final String performer = record.get(columnPerformer);
//						final String alternativeTitles = record.get(columnAlternativeTitles);
                        final String territory = record.get(columnTerritory);
//						final String validFrom = record.get(columnValidFrom);
//						final String validTo = record.get(columnValidTo);
                        final String status = record.get(columnStatus);

                        UcmrAdaWork work = csvRowNoSqlDb.get(masterCode);
                        if (null == work) {
                            work = new UcmrAdaWork(masterCode);
                            count++;
                        }
                        work.add(new UcmrAdaRow(workNumber, role, shareholder,
                                demShare, drmShare, ipnamenr, territory, status));
                        csvRowNoSqlDb.put(work);

                        heartbeat.pump();
                    }

                    logger.debug("total parsed rows {}", heartbeat.getTotalPumps());
                    logger.debug("total csv_row records {}", count);

                    final JsonObject csvRowStats = new JsonObject();
                    csvRowStats.addProperty("inputFileSize", csvFile.length());
                    csvRowStats.addProperty("count", count);
                    csvRowStats.addProperty("rownum", heartbeat.getTotalPumps());
                    csvRowStats.addProperty("duration", TextUtils
                            .formatDuration(System.currentTimeMillis() - lapTimeMillis));
                    output.add("csv_row", csvRowStats);
                }

                logger.debug("csv_row nosql created in {}",
                        TextUtils.formatDuration(System.currentTimeMillis() - lapTimeMillis));
            }

            // delete csv_row nosql
            if ("true".equalsIgnoreCase(configuration
                    .getProperty("ucmr_ada_nosql.csv_row.delete_when_done"))) {
                postExecTasks.add(new Runnable() {
                    @Override
                    public void run() {
                        logger.debug("deleting csv_row nosql {}", csvRowNoSqlDb.getHomeFolder());
                        csvRowNoSqlDb.truncate();
                        FileUtils.deleteFolder(csvRowNoSqlDb.getHomeFolder(), true);
                    }
                });
            }

            //////////////////
            // quota_riparto
            //////////////////

            if ("true".equalsIgnoreCase(configuration
                    .getProperty("ucmr_ada_nosql.quota_riparto.read_only"))) {
                configuration.setProperty("ucmr_ada_nosql.quota_riparto.page_size", "-1");
                configuration.setProperty("ucmr_ada_nosql.quota_riparto.read_only", "true");
                configuration.setProperty("ucmr_ada_nosql.quota_riparto.create_always", "false");
            }
            final OperaNoSqlDb operaNoSqlDb = new OperaNoSqlDb(configuration
                    .getProperties(), "ucmr_ada_nosql.quota_riparto");

            if (!"true".equalsIgnoreCase(configuration
                    .getProperty("ucmr_ada_nosql.quota_riparto.read_only"))) {

                final long lapTimeMillis = System.currentTimeMillis();
                logger.debug("creating quota_riparto nosql");

                final HeartBeat heartbeat = HeartBeat.constant("quota_riparto", Integer
                        .parseInt(configuration.getProperty("ucmr_ada_nosql.quota_riparto.heartbeat", "10000")));

                csvRowNoSqlDb.select(new Selector<UcmrAdaWork>() {

                    @Override
                    public void select(UcmrAdaWork work) throws NoSqlException {

                        // sort rows by workNumber
                        Collections.sort(work.rows, new Comparator<UcmrAdaRow>() {
                            @Override
                            public int compare(UcmrAdaRow row1, UcmrAdaRow row2) {
                                return row1.workNumber.compareTo(row2.workNumber);
                            }
                        });

                        final Opera opera = new Opera(work.masterCode);
                        SchemaRiparto schema = null;
                        String workNumber = null;
                        for (UcmrAdaRow row : work.rows) {
                            if (null == schema || !row.workNumber.equals(workNumber)) {
                                schema = new SchemaRiparto(0L,
                                        decodeTerritoryExpression(territoryMapping, row.territory));
                                opera.add(schema);
                            }
                            final String idTipoIrregolarita;
                            final String shareholder;
                            final Long codiceIpi;
                            try {
                                idTipoIrregolarita = regularStatus
                                        .contains(row.status) ? null : row.status;
                                shareholder = null == row.shareholder ?
                                        null : row.shareholder.trim();
                                codiceIpi = Strings.isNullOrEmpty(row.ipnamenr) ?
                                        null : Long.parseLong(TextUtils.remove(row.ipnamenr, ' '));
                            } catch (Exception e) {
                                logger.error("quota_riparto", e);
                                continue;
                            }
                            schema.add(new QuotaRiparto()
                                    .setCodiceIpi(codiceIpi)
                                    .setTipoQualifica(row.role)
                                    .setTipoQuota("DEM")
                                    .setPercentuale(Float.parseFloat(row.demShare))
                                    .setCognome(shareholder)
                                    .setIdRepertorio(idRepertorio)
                                    .setIdTipoIrregolarita(idTipoIrregolarita)
                                    .setNumeratore(0)
                                    .setDenominatore(0));
                            schema.add(new QuotaRiparto()
                                    .setCodiceIpi(codiceIpi)
                                    .setTipoQualifica(row.role)
                                    .setTipoQuota("DRM")
                                    .setPercentuale(Float.parseFloat(row.drmShare))
                                    .setCognome(shareholder)
                                    .setIdRepertorio(idRepertorio)
                                    .setIdTipoIrregolarita(idTipoIrregolarita)
                                    .setNumeratore(0)
                                    .setDenominatore(0));
                            workNumber = row.workNumber;
                        }
                        operaNoSqlDb.put(opera);

                        heartbeat.pump();
                    }
                });

                logger.debug("quota_riparto nosql created in {}",
                        TextUtils.formatDuration(System.currentTimeMillis() - lapTimeMillis));

                // stats
                final JsonObject quotaRiparto = new JsonObject();
                quotaRiparto.addProperty("rownum", heartbeat.getTotalPumps());
                quotaRiparto.addProperty("duration", TextUtils
                        .formatDuration(System.currentTimeMillis() - lapTimeMillis));
                output.add("quota_riparto", quotaRiparto);

            }

            // delete quota_riparto nosql
            if ("true".equalsIgnoreCase(configuration
                    .getProperty("ucmr_ada_nosql.quota_riparto.delete_when_done"))) {
                postExecTasks.add(new Runnable() {
                    @Override
                    public void run() {
                        logger.debug("deleting quota_riparto nosql {}", operaNoSqlDb.getHomeFolder());
                        operaNoSqlDb.truncate();
                        FileUtils.deleteFolder(operaNoSqlDb.getHomeFolder(), true);
                    }
                });
            }
        }

        ///////////
        // upload
        ///////////

        if (steps.contains("upload")) {

            final long lapTimeMillis = System.currentTimeMillis();

            final File archiveFolder = new File(configuration
                    .getProperty("ucmr_ada_nosql.quota_riparto.home_folder"));
            logger.debug("archiveFolder {}", archiveFolder);

            final JsonObject uploadStats = new JsonObject();

            archiveAndUpload("ucmr_ada_nosql", homeFolder, archiveFolder, uploadStats);

            uploadStats.addProperty("duration", TextUtils
                    .formatDuration(System.currentTimeMillis() - lapTimeMillis));

            output.add("upload", uploadStats);

        }

        // run post exec tasks
        for (Runnable task : postExecTasks) {
            task.run();
        }

        // output
        output.addProperty("period", year + month);
        output.addProperty("startTime", DateFormat
                .getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM)
                .format(new Date(startTimeMillis)));
        output.addProperty("totalDuration", TextUtils
                .formatDuration(System.currentTimeMillis() - startTimeMillis));

        logger.debug("message processed in {}",
                TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));
    }

}
