package com.alkemytech.sophia.royalties.nosql;

import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class UcmrAdaRow {
	
	public String workNumber;
	public String role;
	public String shareholder;
	public String demShare;
	public String drmShare;
	public String ipnamenr;
	public String territory;
	public String status;

	public UcmrAdaRow() {
		super();
	}

	public UcmrAdaRow(String workNumber, String role, String shareholder, String demShare, String drmShare,
			String ipnamenr, String territory, String status) {
		super();
		this.workNumber = workNumber;
		this.role = role;
		this.shareholder = shareholder;
		this.demShare = demShare;
		this.drmShare = drmShare;
		this.ipnamenr = ipnamenr;
		this.territory = territory;
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((workNumber == null) ? 0 : workNumber.hashCode());
		result = prime * result + ((ipnamenr == null) ? 0 : ipnamenr.hashCode());
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UcmrAdaRow other = (UcmrAdaRow) obj;
		if (workNumber == null) {
			if (other.workNumber != null)
				return false;
		} else if (!workNumber.equals(other.workNumber))
			return false;
		if (ipnamenr == null) {
			if (other.ipnamenr != null)
				return false;
		} else if (!ipnamenr.equals(other.ipnamenr))
			return false;
		if (role == null) {
			if (other.role != null)
				return false;
		} else if (!role.equals(other.role))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}
	
}
