package com.alkemytech.sophia.royalties.claim;

import java.math.BigDecimal;

import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class MinimumRange {

	public static final String TYPE_PROPORTIONAL = "P";
	public static final String TYPE_FIXED = "F";
	
	private int fromTracks;
	private int upToTracks;
	private BigDecimal minimum;
	private String type;
	
	public MinimumRange() {
		super();
	}

	public MinimumRange(int fromTracks, int upToTracks, BigDecimal minimum, String type) {
		super();
		this.fromTracks = fromTracks;
		this.upToTracks = upToTracks;
		this.minimum = minimum;
		this.type = type;
	}

	public int getFromTracks() {
		return fromTracks;
	}

	public void setFromTracks(int fromTracks) {
		this.fromTracks = fromTracks;
	}

	public int getUpToTracks() {
		return upToTracks;
	}

	public void setUpToTracks(int upToTracks) {
		this.upToTracks = upToTracks;
	}

	public BigDecimal getMinimum() {
		return minimum;
	}
	
	public void setMinimum(BigDecimal minimum) {
		this.minimum = minimum;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public boolean isFixed() {
		return TYPE_FIXED.equalsIgnoreCase(type);
	}

	public boolean isProportional() {
		return TYPE_PROPORTIONAL.equalsIgnoreCase(type);
	}
	
	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}

}
