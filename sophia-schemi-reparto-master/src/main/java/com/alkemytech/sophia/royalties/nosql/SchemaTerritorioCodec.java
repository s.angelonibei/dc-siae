package com.alkemytech.sophia.royalties.nosql;

import java.nio.charset.Charset;

import com.alkemytech.sophia.commons.mmap.ObjectCodec;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class SchemaTerritorioCodec extends ObjectCodec<SchemaTerritorio> {

	private final Charset charset;
	
	public SchemaTerritorioCodec(Charset charset) {
		super();
		this.charset = charset;
	}

	@Override
	public SchemaTerritorio bytesToObject(byte[] bytes) {
		beginDecoding(bytes);
		final long idSchemaRiparto = getPackedLongEx();
		final Territories territories = new Territories(getString(charset));
		return new SchemaTerritorio(idSchemaRiparto, territories);
	}

	@Override
	public byte[] objectToBytes(SchemaTerritorio object) {
		beginEncoding();
		putPackedLongEx(object.idSchemaRiparto);
		putString(object.territories.toString(), charset);
		return commitEncoding();
	}
	
}
