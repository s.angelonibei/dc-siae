package com.alkemytech.sophia.royalties.blacklist;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.QuoteMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.io.CompressionAwareFileInputStream;
import com.alkemytech.sophia.royalties.role.RoleCatalog;
import com.alkemytech.sophia.royalties.scheme.Royalty;
import com.amazonaws.services.s3.model.S3Object;
import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class BlackList {
	
	private static final Logger logger = LoggerFactory.getLogger(BlackList.class);

	private final Charset charset;
	private final RoleCatalog roleCatalog;
	private final String society;
	private final List<BlackListRule> rules;

	public BlackList(Charset charset, RoleCatalog roleCatalog, String society) {
		super();
		this.charset = charset;
		this.roleCatalog = roleCatalog;
		this.society = society;
		this.rules = new ArrayList<>();
	}

	public BlackList load(S3 s3, String url) throws IOException {
		try (final S3Object s3object = s3.getObject(new S3.Url(url));
				final InputStream in = s3object.getObjectContent()) {
			return load(in);
		}
	}

	public BlackList load(File file) throws IOException {
		try (CompressionAwareFileInputStream in = new CompressionAwareFileInputStream(file)) {
			return load(in);
		}
	}

	public synchronized BlackList load(InputStream in) throws IOException {
		
		rules.clear();

		// parse csv file
		try (final Reader reader = new InputStreamReader(in, charset);
				final CSVParser csvParser = new CSVParser(reader, CSVFormat.newFormat(';')
						.withIgnoreEmptyLines()
						.withIgnoreSurroundingSpaces()
						.withQuoteMode(QuoteMode.MINIMAL)
						.withQuote('"'))) {
			
			final Iterator<CSVRecord> csvIterator = csvParser.iterator();
			
			while (csvIterator.hasNext()) {
				final CSVRecord record = csvIterator.next();
				
				// NAME;TYPE;IPNAMENRS;SOCIETIES;RIGHTS;DSPS
				final String name = record.get(0);
				final String type = record.get(1);
				final String ipnamenrs = record.get(2);
				final String societies = record.get(3);
				final String rights = record.get(4);
				final String dsps = record.get(5);
				
				rules.add(new BlackListRule(name, type, ipnamenrs, societies, rights, dsps));				
			}
			
		}
		
		logger.debug("rules: {}", rules);

		return this;
	}
	
	/**
	 * Filter work royalties using black list rules.
	 * 
	 * @param royalties royalty list to filter
	 * @param dspCode 3-digit dsp code
	 * @param period reference period in yyyyMM format
	 * @return a new list containing only valid royalties
	 */
	public List<Royalty> filter(List<Royalty> royalties, String dspCode, String period) {
		
		final Set<String> authorsSocieties = new HashSet<>();
		final Set<String> originalEditorsSocieties = new HashSet<>();
		final Set<Long> subEditors = new HashSet<>();

		for (Royalty royalty : royalties) {
			final boolean isAuthor = roleCatalog
					.isBlackListAuthor(royalty.formatoQualifica, royalty.tipoQualifica);
			if (isAuthor) {
				authorsSocieties.add(royalty.socname);
			}
			final boolean isOriginalEditor = roleCatalog
					.isBlackListOriginalEditor(royalty.formatoQualifica, royalty.tipoQualifica);
			if (isOriginalEditor) {
				originalEditorsSocieties.add(royalty.socname);
			}

			final boolean isSubEditor = roleCatalog
					.isBlackListSubEditor(royalty.formatoQualifica, royalty.tipoQualifica);
			if(isSubEditor){
				subEditors.add(royalty.codiceIpi);
			}
		}
		
		boolean anyAuthor = false;
		final List<Royalty> sospesoPerEditore = new ArrayList<>();
		final List<Royalty> publicDomain = new ArrayList<>();
		final List<Royalty> result = new ArrayList<>(royalties.size());
		for (Royalty royalty : royalties) {
			
			if (royalty.publicDomain) {
				publicDomain.add(royalty);
				continue;
			}

			if (royalty.sospesoPerEditore) {

				// le quote relative al conto 85 vengono incluse/escluse al termine dell'applicazione delle
				// regole di black list
				
				sospesoPerEditore.add(royalty);
				continue;
			}

			boolean include = true;
			for (BlackListRule rule : rules) {
				if (rule.isDspIncluded(dspCode, period)) { // DSP
					if (rule.isRightIncluded(royalty.tipoQuota)) { // DEM/DRM						
						if (rule.isControlloIncrociato()) { // CI
							
							// controllo incrociato (subeditore - autore/compositore): elimina eventualmente solo subeditori.
							// si elimina un subeditore contenuto nella colonna "IPNAMENRS" se esiste almeno un autore/compositore
							// tutelato da una delle società che compaiono nella colonna "SOCIETIES"

							//BUGFIX:11/2019 -> richiedono la rimoziona anche degli autori/editori che fanno scattare la regola
							
							final boolean isEditor = roleCatalog
									.isBlackListSubEditor(royalty.formatoQualifica, royalty.tipoQualifica);
							if (isEditor) {
								if (rule.isCiIpiNumberIncluded(royalty.codiceIpi)) {
									if (rule.isAnySocietyIncluded(authorsSocieties)) {
//										logger.debug("esclusione CI royalty {} rule {}", royalty, rule);
										include = false;
										break;
									}
								}
							//1. verifico se è un autore con società in blacklist
							}else if ((roleCatalog.isBlackListAuthor(royalty.formatoQualifica, royalty.tipoQualifica) || roleCatalog.isBlackListOriginalEditor(royalty.formatoQualifica, royalty.tipoQualifica))
									&& rule.isSocietyIncluded(royalty.socname)){


							//2. verifico la presenza del subeditore
								for (Long subEditor : subEditors) {
									if(rule.isCiIpiNumberIncluded(subEditor)){
							//3. escludo
										include = false;
										break;
									}
								}

							}
							
						} else if (rule.isEsclusioneDiretta()) { // ED
							
							// esclusioni dirette: elimina qualunque qualifica.
							// si applicano a tutti gli aventi diritto associati alle società riportati nelle regole
							// indipendentemente delle loro qualifiche
							
							if (rule.isEdSocietyIncluded(royalty.socname)) {
//								logger.debug("esclusione ED royalty {} rule {}", royalty, rule);
								include = false;
								break;
							}
							
						}
					}
				}	
			}
			
			if (include ) {
				if (!anyAuthor ) {
					anyAuthor = roleCatalog
							.isBlackListAuthor(royalty.formatoQualifica, royalty.tipoQualifica);
				}
				result.add(royalty);
			}

		}

//		logger.debug("any author {}", anyAuthor);

		//2019-09-09 come richiesto da IT Siae e BU:
		//per il territorio italia va considerata solo l'esclusione della blacklist.
		//non vanno effettuate ulteriori modifiche alle quote se non:
		// se al termine dell’applicazione delle regole di blacklist non rimane alcun avente diritto
		// rappresentato con qualifiche autorali e non sono presenti aventi diritto con qualifica di
		// Editore Originale associati alla SIAE il claiming dell’opera sarà pari a 0% (non si fa
		// claim dell’intera opera).


		if (!anyAuthor) {
			
			// se al termine dell’applicazione delle regole di blacklist non rimane alcun avente diritto
			// con qualifiche di autore/compositore (CA,CO,AO,EL,AR) allora il claiming dell’opera deve
			// considerare le sole quote SIAE (i.e. collecting society) ma anche i conti speciali

			// WARNING: a seguito di una CR, il testo precedente è satto così modificato:
			
			// se al termine dell’applicazione delle regole di blacklist non rimane alcun avente diritto
			// rappresentato con qualifiche autorali e non sono presenti aventi diritto con qualifica di
			// Editore Originale associati alla SIAE il claiming dell’opera sarà pari a 0% (non si fa 
			// claim dell’intera opera).

			if (!originalEditorsSocieties.contains(society)) {
				result.clear();
			}

//			result.clear();
			
//			logger.debug("original editor societies {}", originalEditorsSocieties);

//			if (originalEditorsSocieties.contains(society)) {
//				for (Royalty royalty : royalties) {
//					if (society.equals(royalty.socname) &&
//							!royalty.sospesoPerEditore) {
//						result.add(royalty);
//					}
//				}
//			}

		} else {
		
			// conti speciali: dopo aver verificato le esclusioni dirette e le regole incrociate, se rimane
			// almeno un autore/compositore allora va considerata la quota del conto 85, altrimenti anche
			// questa viene esclusa

			//2019-09-09 come richiesto il conto 85 viene SEMPRE escluso mail del BU del 20/06
			//result.addAll(sospesoPerEditore);

		}

		// le quote di pubblico dominio non sono soggette a esclusioni per black list
		
		result.addAll(publicDomain);

		return result;
	}
	
	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}

}
