package com.alkemytech.sophia.royalties.nosql;

import java.util.HashSet;
import java.util.Set;

import com.alkemytech.sophia.commons.nosql.NoSqlEntity;
import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class Opera implements NoSqlEntity {

	public final String codiceOpera;
	public final Set<SchemaRiparto> schemi;
	
	public Opera(String codiceOpera) {
		super();
		if (null == codiceOpera || 0 == codiceOpera.length())
			throw new IllegalArgumentException("codiceOpera is null or empty");
		this.codiceOpera = codiceOpera;
		this.schemi = new HashSet<>();
	}

	public void add(SchemaRiparto schema) {
		schemi.add(schema);
	}
	
	@Override
	public String getPrimaryKey() {
		return codiceOpera;
	}

	@Override
	public String getSecondaryKey() {
		return null;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}
	
}
