package com.alkemytech.sophia.royalties.claim;

import com.alkemy.siae.sophia.pricing.DsrLine;
import com.alkemy.siae.sophia.pricing.DsrMetadata;
import com.alkemytech.sophia.commons.util.BigDecimals;
import com.alkemytech.sophia.royalties.currency.CurrencyConverter;
import com.google.common.base.Strings;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class DsrStats {

	private static final Logger logger = LoggerFactory.getLogger(DsrStats.class);
	private static final BigDecimal CONVERSION_RATE_SCALE = BigDecimal.TEN.pow(5);
	private static final BigDecimal ONE_HUNDREDTH = BigDecimals.divide(BigDecimal.ONE, BigDecimal.TEN.pow(2));

	public final Map<String, ClaimStats> societyStats;
	public final AtomicReference<BigDecimal> totaleValore;
	public final AtomicReference<BigDecimal> totaleUtilizzazioni;
	public final AtomicReference<BigDecimal> identificatoValore;
	public final AtomicReference<BigDecimal> identificatoUtilizzazioni;
	public final AtomicReference<BigDecimal> claimValore;
	public final AtomicReference<BigDecimal> claimUtilizzazioni;
	public final AtomicReference<BigDecimal> nonIdentificatoValore;
	public final AtomicReference<BigDecimal> nonIdentificatoUtilizzazioni;
	public final AtomicLong totaleRighe;
	public final AtomicLong identificatoRighe;
	public final AtomicLong nonIdentificatoRighe;
	public final Function<BigDecimal, BigDecimal> valueNormalizer;

	public DsrStats(Function<BigDecimal, BigDecimal> valueNormalizer) {
		this.societyStats = new ConcurrentHashMap<>();
		this.totaleValore = new AtomicReference<>(BigDecimal.ZERO);
		this.totaleUtilizzazioni = new AtomicReference<>(BigDecimal.ZERO);
		this.totaleRighe = new AtomicLong(0L);
		this.identificatoValore = new AtomicReference<>(BigDecimal.ZERO);
		this.identificatoUtilizzazioni = new AtomicReference<>(BigDecimal.ZERO);
		this.identificatoRighe = new AtomicLong(0L);
		this.claimValore = new AtomicReference<>(BigDecimal.ZERO);
		this.claimUtilizzazioni = new AtomicReference<>(BigDecimal.ZERO);
		this.nonIdentificatoValore = new AtomicReference<>(BigDecimal.ZERO);
		this.nonIdentificatoUtilizzazioni = new AtomicReference<>(BigDecimal.ZERO);
		this.nonIdentificatoRighe = new AtomicLong(0L);
		this.valueNormalizer = valueNormalizer;
	}

	private ClaimStats getSocietyStats(String society) {
		ClaimStats claimStats = societyStats.get(society);
		if (null == claimStats) {
			claimStats = new ClaimStats();
			societyStats.put(society, claimStats);
		}
		return claimStats;
	}

	public void update(DsrMetadata metadata, DsrLine line, ClaimResult claim) {
		final BigDecimal exchangeRate = BigDecimals
				.divide(CONVERSION_RATE_SCALE, metadata.getConversionRate());

		final BigDecimal utilizzazioni = line.getSalesCount();


		//final BigDecimal valore = utilizzazioni.multiply(line.getRoyalty()).multiply(exchangeRate);

		//Set totale valore del calim che dev'essere uguale al trailer del CCID
		if (metadata.getCcidVersion().equals("13"))
			totaleValore.set((metadata.getSumAmountMech().add(metadata.getSumAmountPerf())).divide(BigDecimal.TEN.pow(4)));

		if (metadata.getCcidVersion().equals("14"))
			totaleValore.set(metadata.getSumAmountMech().add(metadata.getSumAmountPerf()));

		//logger.debug("Totale Valore DSRStats: " + totaleValore);//accumulateAndGet(valore, BigDecimal::add);


		final BigDecimal valore = utilizzazioni.multiply(line.getRoyalty()).multiply(exchangeRate);

		totaleUtilizzazioni.accumulateAndGet(utilizzazioni, BigDecimal::add);
		totaleRighe.incrementAndGet();
		if (Strings.isNullOrEmpty(line.getUuidSophia())) {
			nonIdentificatoValore.accumulateAndGet(valore, BigDecimal::add);
			nonIdentificatoUtilizzazioni.accumulateAndGet(utilizzazioni, BigDecimal::add);
			nonIdentificatoRighe.incrementAndGet();
			getSocietyStats(metadata.getTenant().getSociety())
				.updateNonIdentificato(valore, utilizzazioni);
			for (SocietyInfo societyInfo : metadata.getMandators()) {
				getSocietyStats(societyInfo.getSociety())
					.updateNonIdentificato(valore, utilizzazioni);
			}
			return;
		}
		identificatoValore.accumulateAndGet(valore, BigDecimal::add);
		identificatoUtilizzazioni.accumulateAndGet(utilizzazioni, BigDecimal::add);
		identificatoRighe.incrementAndGet();
		if (null == claim || claim.getClaimingSocieties() <= 0) {
			return;
		}
		final BigDecimal splitDem = BigDecimal.valueOf(metadata.getSplitDEM());
		final BigDecimal splitDrm = BigDecimal.valueOf(metadata.getSplitDRM());
		final BigDecimal claimTotale = ((splitDem.multiply(claim.getClaimLicensorDem()))
				.add(splitDrm.multiply(claim.getClaimLicensorDrm())))
				;
		claimValore.set(totaleValore.get());
		claimUtilizzazioni.accumulateAndGet(claimTotale.multiply(utilizzazioni).multiply(ONE_HUNDREDTH), BigDecimal::add);
		// drill down
		for (SocietyInfo societyInfo : metadata.getMandators()) {
			if (claim.hasMandator(societyInfo.getSociety())) {
				final ClaimInfo claimInfo = claim
						.getMandatorInfo(societyInfo.getSociety());
				if (null != claimInfo.claimLicensorDem ||
						null != claimInfo.claimLicensorDrm) {
					final BigDecimal claimDem = null == claimInfo.claimLicensorDem ?
							BigDecimal.ZERO : claimInfo.claimLicensorDem;
					final BigDecimal claimDrm = null == claimInfo.claimLicensorDrm ?
							BigDecimal.ZERO : claimInfo.claimLicensorDrm;
					final BigDecimal percentualeClaimMandator = (splitDem.multiply(claimDem).add(splitDrm.multiply(claimDrm))).multiply(ONE_HUNDREDTH);
					getSocietyStats(societyInfo.getSociety())
						.updateIdentificato(valore, utilizzazioni,
								valueNormalizer.apply(percentualeClaimMandator.multiply(valore)), percentualeClaimMandator.multiply(utilizzazioni));
				} else {
					getSocietyStats(societyInfo.getSociety())
						.updateIdentificato(valore, utilizzazioni, BigDecimal.ZERO, BigDecimal.ZERO);
				}
			} else {
				getSocietyStats(societyInfo.getSociety())
					.updateNonIdentificato(valore, utilizzazioni);
			}
		}
		if (claim.hasTenant()) {
			final ClaimInfo claimInfo = claim.getTenantInfo();
			if (null != claimInfo.claimLicensorDem ||
					null != claimInfo.claimLicensorDrm) {
				final BigDecimal claimDem = null == claimInfo.claimLicensorDem ?
						BigDecimal.ZERO : claimInfo.claimLicensorDem;
				final BigDecimal claimDrm = null == claimInfo.claimLicensorDrm ?
						BigDecimal.ZERO : claimInfo.claimLicensorDrm;
				final BigDecimal percentualeClaimTenant = (splitDem.multiply(claimDem).add(splitDrm.multiply(claimDrm))).multiply(ONE_HUNDREDTH);
				getSocietyStats(metadata.getTenant().getSociety())
					.updateIdentificato(valore, utilizzazioni,
							valueNormalizer.apply(percentualeClaimTenant.multiply(valore)), percentualeClaimTenant.multiply(utilizzazioni));
			} else {
				getSocietyStats(metadata.getTenant().getSociety())
					.updateIdentificato(valore, utilizzazioni, BigDecimal.ZERO, BigDecimal.ZERO);
			}
		} else {
			getSocietyStats(metadata.getTenant().getSociety())
				.updateNonIdentificato(valore, utilizzazioni);
		}
	}

	public JsonObject toJsonObject(DsrMetadata dsrMetadata, CurrencyConverter currencyConverter) {
		final JsonArray jsonDrillDown = new JsonArray();
		for (Map.Entry<String, ClaimStats> entry : societyStats.entrySet()) {
			final JsonObject jsonStats = new JsonObject();
			jsonStats.addProperty("societa", entry.getKey());
			final ClaimStats societyStats = entry.getValue();
			jsonStats.addProperty("identificatoValore",
					dsrMetadata.getRoyaltyCurrency().equals("EUR") ? societyStats.identificatoValore.get() :
					currencyConverter.convert(dsrMetadata.getRoyaltyCurrency(), societyStats.identificatoValore.get(), "EUR"));

			jsonStats.addProperty("identificatoUtilizzazioni",
					societyStats.identificatoUtilizzazioni.get());
			jsonStats.addProperty("identificatoRighe",
					societyStats.identificatoRighe.get());
			jsonStats.addProperty("percentualeIdentificatoUtilizzazioni",
					(societyStats.identificatoUtilizzazioni.get().multiply(BigDecimal.TEN.pow(2))).divide(totaleUtilizzazioni.get(), 2, BigDecimal.ROUND_HALF_UP));
			jsonStats.addProperty("percentualeIdentificatoRighe",
					BigDecimal.valueOf((100.0 * societyStats.identificatoRighe.get()) / (double) totaleRighe.get()));
			jsonStats.addProperty("claimValore",
					dsrMetadata.getRoyaltyCurrency().equals("EUR") ? societyStats.claimValore.get() :
					currencyConverter.convert(dsrMetadata.getRoyaltyCurrency(), societyStats.claimValore.get(), "EUR"));
			jsonStats.addProperty("claimUtilizzazioni",
					societyStats.claimUtilizzazioni.get());
			jsonStats.addProperty("nonIdentificatoValore",
					dsrMetadata.getRoyaltyCurrency().equals("EUR") ? societyStats.nonIdentificatoValore.get() :
					currencyConverter.convert(dsrMetadata.getRoyaltyCurrency(), societyStats.nonIdentificatoValore.get(), "EUR"));
			jsonStats.addProperty("nonIdentificatoUtilizzazioni",
					societyStats.nonIdentificatoUtilizzazioni.get());
			jsonStats.addProperty("nonIdentificatoRighe",
					societyStats.nonIdentificatoRighe.get());
			jsonDrillDown.add(jsonStats);
		}
		final JsonObject jsonStats = new JsonObject();
		jsonStats.addProperty("totalSales",
				dsrMetadata.getTotalSales());

		logger.debug("TOTALE VALORE JSON "+totaleValore.get());

		jsonStats.addProperty("totaleValore",
				totaleValore.get());
		jsonStats.addProperty("totaleUtilizzazioni",
				totaleUtilizzazioni.get());
		jsonStats.addProperty("totaleRighe",
				totaleRighe.get());
		jsonStats.addProperty("identificatoValore",
				dsrMetadata.getRoyaltyCurrency().equals("EUR") ? identificatoValore.get() :
				currencyConverter.convert(dsrMetadata.getRoyaltyCurrency(), identificatoValore.get(), "EUR"));
		jsonStats.addProperty("identificatoUtilizzazioni",
				identificatoUtilizzazioni.get());
		jsonStats.addProperty("identificatoRighe",
				identificatoRighe.get());
		jsonStats.addProperty("percentualeIdentificatoUtilizzazioni",
				(identificatoUtilizzazioni.get().multiply(BigDecimal.TEN.pow(2))).divide(totaleUtilizzazioni.get(), 2, BigDecimal.ROUND_HALF_UP));
		jsonStats.addProperty("percentualeIdentificatoRighe",
				BigDecimal.valueOf((100.0 * identificatoRighe.get()) / (double) totaleRighe.get()));
		jsonStats.addProperty("claimValore",
				dsrMetadata.getRoyaltyCurrency().equals("EUR") ? claimValore.get() :
				currencyConverter.convert(dsrMetadata.getRoyaltyCurrency(), claimValore.get(), "EUR"));
		jsonStats.addProperty("claimUtilizzazioni",
				claimUtilizzazioni.get());
		jsonStats.addProperty("nonIdentificatoValore",
				dsrMetadata.getRoyaltyCurrency().equals("EUR") ? nonIdentificatoValore.get() :
				currencyConverter.convert(dsrMetadata.getRoyaltyCurrency(), nonIdentificatoValore.get(), "EUR"));
		jsonStats.addProperty("nonIdentificatoUtilizzazioni",
				nonIdentificatoUtilizzazioni.get());
		jsonStats.addProperty("nonIdentificatoRighe",
				nonIdentificatoRighe.get());
		jsonStats.add("societa", jsonDrillDown);
		return jsonStats;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}
}
