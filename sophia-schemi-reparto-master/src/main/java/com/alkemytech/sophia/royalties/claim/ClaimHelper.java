package com.alkemytech.sophia.royalties.claim;

import com.alkemy.siae.sophia.pricing.DsrLine;
import com.alkemy.siae.sophia.pricing.DsrMetadata;
import com.alkemytech.sophia.royalties.Configuration;
import com.alkemytech.sophia.royalties.blacklist.BlackList;
import com.alkemytech.sophia.royalties.nosql.IpiSocNoSqlDb;
import com.alkemytech.sophia.royalties.nosql.OperaNoSqlDb;
import com.alkemytech.sophia.royalties.nosql.UuidCodici;
import com.alkemytech.sophia.royalties.nosql.UuidCodiciNoSqlDb;
import com.alkemytech.sophia.royalties.scheme.Royalty;
import com.alkemytech.sophia.royalties.scheme.RoyaltyScheme;
import com.alkemytech.sophia.royalties.scheme.RoyaltySchemeService;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class ClaimHelper {

	private static final Logger logger = LoggerFactory.getLogger(ClaimHelper.class);

	private final IpiSocNoSqlDb ipiNoSqlDb;
	private final UuidCodiciNoSqlDb codiciNoSqlDb;
	private final Map<String, OperaNoSqlDb> operaNoSqlDbs;
	private final RoyaltySchemeService royaltySchemeService;

	public ClaimHelper(Configuration configuration,
			IpiSocNoSqlDb ipiNoSqlDb,
			UuidCodiciNoSqlDb codiciNoSqlDb,
			Map<String, OperaNoSqlDb> operaNoSqlDbs,
			RoyaltySchemeService royaltySchemeService) {
		super();
		this.ipiNoSqlDb = ipiNoSqlDb;
		this.codiciNoSqlDb = codiciNoSqlDb;
		this.operaNoSqlDbs = operaNoSqlDbs;
		this.royaltySchemeService = royaltySchemeService;
	}
	
	public ClaimResult claim(DsrMetadata dsrMetadata, DsrLine dsrLine) {
		final String uuid = dsrLine.getUuidSophia();
		if (Strings.isNullOrEmpty(uuid)) {
			return null;
		}
		final UuidCodici codici = codiciNoSqlDb.get(uuid);

		if (null == codici) {
			logger.warn("claim: uuid not found " + uuid);
			return null;
		}
		final String dspCode = dsrMetadata.getDspCode();
		final String period = dsrMetadata.getReferencePeriod();
		final String territory = dsrLine.getTerritory();
		final SocietyInfo tenant = dsrMetadata.getTenant();
		
		ClaimInfo tenantInfo = null;
		Map<String, ClaimInfo> mandatorsInfo = null;
		BigDecimal totalClaimLicensorDem = BigDecimal.ZERO;
		BigDecimal totalClaimLicensorDrm = BigDecimal.ZERO;
		BigDecimal totalClaimPdDem = null;
		BigDecimal totalClaimPdDrm = null;
		int claimingSocieties = 0;
		
		for (Map.Entry<String, String> entry : codici.codici.entrySet()) {
			final String societa = entry.getKey();
			if (operaNoSqlDbs.get(societa) == null)
				continue;

			final String codiceOpera = entry.getValue();
			final ClaimInfo claimInfo = new ClaimInfo()
					.setWorkCode(codiceOpera);
			boolean tenantSociety = false;


			// lookup royalty scheme
			final RoyaltyScheme royaltyScheme = royaltySchemeService
					.lookup(ipiNoSqlDb, operaNoSqlDbs.get(societa),
							societa, codiceOpera, territory);
			if (null == royaltyScheme) {
				//logger.warn("claim: royalty scheme not found " + societa + " " + codiceOpera);
				continue;
			}


			// select black list (if necessary)
			final BlackList blackList;
			if (societa.equals(tenant.getSociety())) { // tenant
				tenantSociety = true;
				tenantInfo = claimInfo;
				blackList = territory.equals(tenant.getTerritory()) ?
						tenant.getBlackList() : null;
			} else { // mandator(s)
				final SocietyInfo mandantor = dsrMetadata.getMandator(societa);
				if (null == mandantor) {
					continue; // unmatched
				}
				if (null == mandatorsInfo) {
					mandatorsInfo = new HashMap<>();
				}
				mandatorsInfo.put(societa, claimInfo);
				blackList = territory.equals(mandantor.getTerritory()) ?
						mandantor.getBlackList() : null;					
			}
			claimingSocieties ++;
			
			// apply black list (if necessary)
			final List<Royalty> royalties;
			if (null != blackList) {
				royalties = blackList.filter(royaltyScheme.royalties, dspCode, period);
			} else {
				royalties = new ArrayList<>(royaltyScheme.royalties.size());
				for (Royalty royalty : royaltyScheme.royalties) {
					if (!royalty.doNotClaim && !royalty.sospesoPerEditore) {
						royalties.add(royalty);
					}
				}
			}

			// compute claim percentages DEM/DRM
			BigDecimal claimLicensorDem = BigDecimal.ZERO;
			BigDecimal claimLicensorDrm = BigDecimal.ZERO;
			BigDecimal claimPdDem = BigDecimal.ZERO;
			BigDecimal claimPdDrm = BigDecimal.ZERO;
			for (Royalty royalty : royalties) {
				if (royalty.publicDomain) {
					if (royalty.isPerforming()) {
						claimPdDem = claimPdDem.add(new BigDecimal(royalty.percentuale));
					} else if (royalty.isMechanical()) {
						claimPdDrm = claimPdDrm.add(new BigDecimal(royalty.percentuale));
					}
				}
//				 if (societa.equals(royalty.socname) || royalty.sospesoPerEditore) {
				// modificato in: //estero prendi siae, se italia prendi tutto)
				else if (null != blackList  || societa.equals(royalty.socname))  { //|| royalty.sospesoPerEditore) {
					if (royalty.isPerforming()) {
						claimLicensorDem = claimLicensorDem.add(new BigDecimal(royalty.percentuale));
					} else if (royalty.isMechanical()) {
						claimLicensorDrm = claimLicensorDrm.add(new BigDecimal(royalty.percentuale));
					}
				}
			}
			totalClaimLicensorDem = totalClaimLicensorDem.add(claimLicensorDem);
			totalClaimLicensorDrm = totalClaimLicensorDrm.add(claimLicensorDrm);
			if (null == totalClaimPdDem || tenantSociety) {
				totalClaimPdDem = claimPdDem;
				totalClaimPdDrm = claimPdDrm;
			}
			
			claimInfo
				.setRoyaltyScheme(royaltyScheme)
				.setRoyalties(royalties)
				.setClaimLicensorDem(claimLicensorDem)
				.setClaimLicensorDrm(claimLicensorDrm);
		}
		
		if (0 == claimingSocieties) {
			//logger.debug("claim: zero claiming societies found {}", codici);
			return null;
		}

		return new ClaimResult()
				.setTenantInfo(tenantInfo)
				.setMandatorsInfo(mandatorsInfo)
				.setClaimLicensorDem(totalClaimLicensorDem)
				.setClaimLicensorDrm(totalClaimLicensorDrm)
				.setClaimPdDem(totalClaimPdDem)
				.setClaimPdDrm(totalClaimPdDrm)
				.setClaimingSocieties(claimingSocieties);
	}
	
}
