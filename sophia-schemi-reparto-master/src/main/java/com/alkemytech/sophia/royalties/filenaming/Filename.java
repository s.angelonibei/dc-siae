package com.alkemytech.sophia.royalties.filenaming;

import lombok.Data;

@Data
public class Filename {

    private String invoiceSender;
    private String invoiceReceiver;
    private String useType;
    private String useStartEndDate;
    private String licenseSD;
    private String typeOfClaim;
}
