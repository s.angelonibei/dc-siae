package com.alkemytech.sophia.royalties.claim;

import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class DistributionInfo {

	public Long codiceIpi;
	public String tipoQualifica;
	public String posizioneSiae;
	public float percentuale;
	public float numeratore;
	public float denominatore;
	public String codiceConto;
	public String numeroConto;
	public float shares;


	public DistributionInfo setCodiceIpi(Long codiceIpi) {
		this.codiceIpi = codiceIpi;
		return this;
	}

	public DistributionInfo setTipoQualifica(String tipoQualifica) {
		this.tipoQualifica = tipoQualifica;
		return this;
	}

	public DistributionInfo setPosizioneSiae(String posizioneSiae) {
		this.posizioneSiae = posizioneSiae;
		return this;
	}

	public DistributionInfo setPercentuale(float percentuale) {
		this.percentuale = percentuale;
		return this;
	}

	public DistributionInfo setNumeratore(float numeratore) {
		this.numeratore = numeratore;
		return this;
	}

	public DistributionInfo setDenominatore(float denominatore) {
		this.denominatore = denominatore;
		return this;
	}

	public DistributionInfo setCodiceConto(String codiceConto) {
		this.codiceConto = codiceConto;
		return this;
	}

	public DistributionInfo setNumeroConto(String numeroConto) {
		this.numeroConto = numeroConto;
		return this;
	}

	public DistributionInfo setShares(float shares) {
		this.shares = shares;
		return this;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}
	
}
