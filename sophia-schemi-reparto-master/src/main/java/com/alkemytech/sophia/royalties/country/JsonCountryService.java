package com.alkemytech.sophia.royalties.country;

import java.nio.charset.Charset;

import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class JsonCountryService extends CountryService {
	
	@Inject
	protected JsonCountryService(@Named("charset") Charset charset) {
		super(charset);
	}
	
}
