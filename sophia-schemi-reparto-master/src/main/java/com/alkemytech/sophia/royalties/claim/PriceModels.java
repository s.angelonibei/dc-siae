package com.alkemytech.sophia.royalties.claim;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import com.alkemy.siae.sophia.pricing.DsrLine;
import com.alkemy.siae.sophia.pricing.DsrMetadata;
import com.alkemytech.sophia.royalties.Configuration;
import com.alkemytech.sophia.royalties.currency.CurrencyConverter;
import com.google.common.base.Strings;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class PriceModels {
	
	private static final Logger logger = LoggerFactory.getLogger(PriceModels.class);
	
	private static final RoundingMode ROUNDING_MODE = RoundingMode.HALF_UP;
	
	private static final int MINIMUM_ALGORITHM_SIMPLE = 1;
	private static final int MINIMUM_ALGORITHM_INCREMENTAL = 2;

	
	private final int workingScale;
	private final int resultScale;
	private final int minimumAlgorithm;
	private final CurrencyConverter currencyConverter;
	private final String[] downloadOffers;
	private final String[] streamSubOffers;
	private final String[] streamAdSupOffers;
	private final String[] streamGenOffers;
	private final String[] masterlistOffers;


	@Inject
	protected PriceModels(Configuration configuration, CurrencyConverter currencyConverter) {
		super();
		this.workingScale = Integer.parseInt(configuration
				.getProperty("pricing_and_claim.working_scale", "20"));
		this.resultScale = Integer.parseInt(configuration
				.getProperty("pricing_and_claim.result_scale", "20"));
		this.minimumAlgorithm = parseMinimumAlgorithm(configuration
				.getProperty("pricing_and_claim.minimum_algorithm", "simple"));
		this.currencyConverter = currencyConverter;

		this.downloadOffers = configuration
				.getProperty("pricing_and_claim.downloadOffers", "DWN,Download").split(",");
		this.streamSubOffers = configuration
				.getProperty("pricing_and_claim.streamSubOffers", "STRM_SUB,Streaming in abbonamento").split(",");
		this.streamAdSupOffers = configuration
				.getProperty("pricing_and_claim.streamAdSupOffers", "STRM_AD,Streaming AD,YOU_AUDIO,Youtube AudioTier").split(",");
		this.streamGenOffers = (configuration
				.getProperty("pricing_and_claim.streamGenOffers", "STRM_GEN,Streaming generico,STRM_YOU,Streaming generico Youtube")).split(",");
		this.masterlistOffers = (configuration
				.getProperty("pricing_and_claim.masterlistOffers", "MASTERLIST,Master list")).split(",");
	}

	private boolean equalsIgnoreCase(String left, String right, boolean bothNull) {
		if (null == left) {
			return null == right ? bothNull : false;
		} else {
			return left.equalsIgnoreCase(right);
		}
	}
	
	private int parseMinimumAlgorithm(String minimumAlgorithm) {
		if ("simple".equalsIgnoreCase(minimumAlgorithm)) {
			return MINIMUM_ALGORITHM_SIMPLE;
		}
		if ("incremental".equalsIgnoreCase(minimumAlgorithm)) {
			return MINIMUM_ALGORITHM_INCREMENTAL;
		}
		throw new IllegalArgumentException("invalid minimum algorithm: " + minimumAlgorithm);
	}

	private boolean isDownload(String priceModel){
		for (String offer : this.downloadOffers) {
			if (offer.equalsIgnoreCase(priceModel)) return true;
		}
		return false;
	}

	private boolean isStreaming(String priceModel) {
		for (String offer : this.streamSubOffers) {
			if (offer.equalsIgnoreCase(priceModel)) return true;
		}
		return false;
	}

	private boolean isGeneralStreaming(String priceModel) {
		for (String offer : this.streamGenOffers) {
			if (offer.equalsIgnoreCase(priceModel)) return true;
		}
		return false;
	}

	private boolean isAdSupported(String priceModel) {
		for (String offer : this.streamAdSupOffers) {
			if (offer.equalsIgnoreCase(priceModel)) return true;
		}
		return false;
	}

	private boolean isYoutube(String priceModel) {
		return "YOUTUBE".equalsIgnoreCase(priceModel);
	}

	private boolean isMasterlist(String priceModel) {
		for (String offer : this.masterlistOffers) {
			if (offer.equalsIgnoreCase(priceModel)) return true;
		}
		return false;
	}

	public boolean isSingleLineOutput(DsrMetadata dsrMetadata) {
		return isStreaming(dsrMetadata.getPricingModel()) ||
				isAdSupported(dsrMetadata.getPricingModel()) ||
				isMasterlist(dsrMetadata.getPricingModel()) ||
				isGeneralStreaming((dsrMetadata.getPricingModel()));
	}
	
	public void computePrice(DsrLine line) {
		final String pricingModel = line.getPricingModel();
		if (isDownload(pricingModel)) {
			computeDownloadPrice(line);
		} else if (isYoutube(pricingModel)) {
			computeYoutubePrice(line);
		} else {
			throw new UnsupportedOperationException("unsupported price model: " + pricingModel);
		}
	}

	public void computePrice(DsrMetadata metadata) {
		final String pricingModel = metadata.getPricingModel();
		if (isStreaming(pricingModel)) {
			computeStreamingPrice(metadata);
		} else if (isAdSupported(pricingModel)) {
			computeAdSupportedPrice(metadata);
		} else if (isMasterlist(pricingModel)) {
			computeMasterlistPrice(metadata);
		} else if(isGeneralStreaming(pricingModel)){
			computeGeneralStreaming(metadata);
		}else {
			throw new UnsupportedOperationException("unsupported price model: " + pricingModel);
		}
	}
	
	private void computeStreamingPrice(DsrMetadata metadata) {

		// convert decision table minimum to DSR currency
		final boolean convertMinimum = !equalsIgnoreCase(metadata
				.getCurrency(), metadata.getMinimumCurrency(), true);
		final BigDecimal valoreMinimoAbbonamento = convertMinimum ? currencyConverter
				.convert(metadata.getMinimumCurrency(), metadata.getMinimum(), metadata.getCurrency()) :
					metadata.getMinimum();
		
		// PRICE_BASIS = tatal_value / total_sales
		// ROYALTY = PRICE_BASIS * pro_rata

		final BigDecimal valoreBaseUtilizzo = metadata.getTotalAmount()
    			.divide(metadata.getTotalSales(), workingScale, ROUNDING_MODE);
		final BigDecimal valorePercentualeSiae = metadata.getProRata()
				.divide(new BigDecimal(100.0), workingScale, ROUNDING_MODE);
		final BigDecimal valoreUnitarioUtilizzo = valoreBaseUtilizzo
    			.multiply(valorePercentualeSiae);

		final BigDecimal valoreUnitarioMinimo = valoreMinimoAbbonamento
    			.multiply(new BigDecimal(metadata.getTotalSubscriptions()))
    			.divide(metadata.getTotalSales(), workingScale, ROUNDING_MODE);

		if (valoreUnitarioUtilizzo.compareTo(valoreUnitarioMinimo) > 0) {
			metadata.setRoyaltyType("P");
			metadata.setRoyalty(valoreUnitarioUtilizzo
					.setScale(resultScale, ROUNDING_MODE));
		} else {
			metadata.setRoyaltyType("M");			
			metadata.setRoyalty(valoreUnitarioMinimo
					.setScale(resultScale, ROUNDING_MODE));
		}
		metadata.setPriceBasis(valoreBaseUtilizzo
				.setScale(resultScale, ROUNDING_MODE));
		metadata.setOriginalPriceBasis(metadata.getPriceBasis());
		
		if (!equalsIgnoreCase(metadata.getCurrency(), "EUR", true)) {
			metadata.setRoyaltyEur(currencyConverter
					.convert(metadata.getCurrency(), metadata.getRoyalty(), "EUR")
					.setScale(resultScale, ROUNDING_MODE));
		} else {
			metadata.setRoyaltyEur(metadata.getRoyalty());
		}
		
	}
	private void computeGeneralStreaming(DsrMetadata metadata) {
//		-minimo abbonamento =  minimoDrools * subscription / totalSales
//		-minimo per stream = minimoDrools
//		MAX(prorata, mnimo abbonamento, minimo per stream).

		// convert decision table minimum to DSR currency
		final boolean convertMinimum = !equalsIgnoreCase(metadata
				.getCurrency(), metadata.getMinimumCurrency(), true);


		final BigDecimal minimoAbbonamento = convertMinimum ? currencyConverter
				.convert(metadata.getMinimumCurrency(), metadata.getMinimumAbbonamento(), metadata.getCurrency()) :
				metadata.getMinimumAbbonamento();

		final BigDecimal valoreUnitarioMinimoAbb = minimoAbbonamento
				.multiply(new BigDecimal(metadata.getTotalSubscriptions()))
				.divide(metadata.getTotalSales(), workingScale, ROUNDING_MODE);

		final BigDecimal minimoStream = convertMinimum ? currencyConverter
				.convert(metadata.getMinimumCurrency(), metadata.getMinimumStream(), metadata.getCurrency()) :
				metadata.getMinimumStream();

		final BigDecimal minimoMax = valoreUnitarioMinimoAbb.compareTo(minimoStream) > 0 ?
								valoreUnitarioMinimoAbb :minimoStream;




		final BigDecimal valoreBaseUtilizzo = metadata.getTotalAmount()
				.divide(metadata.getTotalSales(), workingScale, ROUNDING_MODE);
		final BigDecimal valorePercentualeSiae = metadata.getProRata()
				.divide(new BigDecimal(100.0), workingScale, ROUNDING_MODE);
		final BigDecimal valoreUnitarioUtilizzo = valoreBaseUtilizzo
				.multiply(valorePercentualeSiae);

		logger.info(String.format("Valori di valoreUnitario:%.4f minimoAbbonamento:%.4f minimoUnitarioAbb:%.4f minimoStream:%.4f minimoMax:%.4f ",
				valoreUnitarioUtilizzo,minimoAbbonamento,valoreUnitarioMinimoAbb,minimoStream,minimoMax));

		if (valoreUnitarioUtilizzo.compareTo(minimoMax) > 0) {
			metadata.setRoyaltyType("P");
			metadata.setRoyalty(valoreUnitarioUtilizzo
					.setScale(resultScale, ROUNDING_MODE));
		} else {
			metadata.setRoyaltyType("M");
			metadata.setRoyalty(minimoMax
					.setScale(resultScale, ROUNDING_MODE));
		}
		metadata.setPriceBasis(valoreBaseUtilizzo
				.setScale(resultScale, ROUNDING_MODE));
		metadata.setOriginalPriceBasis(metadata.getPriceBasis());

		if (!equalsIgnoreCase(metadata.getCurrency(), "EUR", true)) {
			metadata.setRoyaltyEur(currencyConverter
					.convert(metadata.getCurrency(), metadata.getRoyalty(), "EUR")
					.setScale(resultScale, ROUNDING_MODE));
		} else {
			metadata.setRoyaltyEur(metadata.getRoyalty());
		}

	}

	
	private void computeAdSupportedPrice(DsrMetadata metadata) {

		// convert decision table minimum to DSR currency
		final boolean convertMinimum = !equalsIgnoreCase(metadata
				.getCurrency(), metadata.getMinimumCurrency(), true);
		final BigDecimal minimoContrattuale = convertMinimum ? currencyConverter
				.convert(metadata.getMinimumCurrency(), metadata.getMinimum(), metadata.getCurrency()) :
					metadata.getMinimum();

		// PRICE_BASIS = tatal_value / total_sales
		// ROYALTY = PRICE_BASIS * pro_rata

		final BigDecimal valoreBaseUtilizzo = metadata.getTotalAmount()
    			.divide(metadata.getTotalSales(), workingScale, ROUNDING_MODE);
		final BigDecimal valorePercentualeSiae = metadata.getProRata()
				.divide(new BigDecimal(100.0), workingScale, ROUNDING_MODE);
		final BigDecimal valoreUnitarioUtilizzo = valoreBaseUtilizzo
    			.multiply(valorePercentualeSiae);

		if (valoreUnitarioUtilizzo.compareTo(minimoContrattuale) > 0) {
			metadata.setRoyaltyType("P");
			metadata.setRoyalty(valoreUnitarioUtilizzo
					.setScale(resultScale, ROUNDING_MODE));
		} else {
			metadata.setRoyaltyType("M");			
			metadata.setRoyalty(minimoContrattuale
					.setScale(resultScale, ROUNDING_MODE));
		}
		metadata.setPriceBasis(valoreBaseUtilizzo
				.setScale(resultScale, ROUNDING_MODE));
		metadata.setOriginalPriceBasis(metadata.getPriceBasis());

		if (!equalsIgnoreCase(metadata.getCurrency(), "EUR", true)) {
			metadata.setRoyaltyEur(currencyConverter
					.convert(metadata.getCurrency(), metadata.getRoyalty(), "EUR")
					.setScale(resultScale, ROUNDING_MODE));
		} else {
			metadata.setRoyaltyEur(metadata.getRoyalty());
		}
		
	}

	private void computeMasterlistPrice(DsrMetadata metadata) {

		// convert decision table minimum to DSR currency
		final boolean convertMinimum = !equalsIgnoreCase(metadata
				.getCurrency(), metadata.getMinimumCurrency(), true);
		final BigDecimal convertedMinimum = convertMinimum ? currencyConverter
				.convert(metadata.getMinimumCurrency(), metadata.getMinimum(), metadata.getCurrency()) :
					metadata.getMinimum();
		
		final BigDecimal priceBasis = convertedMinimum
				.setScale(resultScale, ROUNDING_MODE);
				
		metadata.setPriceBasis(priceBasis);
		metadata.setOriginalPriceBasis(priceBasis);

		metadata.setRoyaltyType("M");
		metadata.setRoyalty(priceBasis);
		
		if (!equalsIgnoreCase(metadata.getCurrency(), "EUR", true)) {
			metadata.setRoyaltyEur(currencyConverter
					.convert(metadata.getCurrency(), metadata.getRoyalty(), "EUR")
					.setScale(resultScale, ROUNDING_MODE));
		} else {
			metadata.setRoyaltyEur(metadata.getRoyalty());
		}

	}
	
	// WARNING: smart (incremental) minimum computation
	private BigDecimal computeMinimumIncremental(List<MinimumRange> minima, int albumTracks) {
		int fromTrack = 0;
		BigDecimal minimum = BigDecimal.ZERO;
		for (MinimumRange minimumRange : minima) {
			if (albumTracks > minimumRange.getUpToTracks()) {
				if (minimumRange.isFixed()) {
					minimum = minimum.add(minimumRange.getMinimum());
				} else {
					final int tracksInRange = minimumRange.getUpToTracks() - fromTrack;
					minimum = minimum.add(minimumRange.getMinimum()
							.multiply(new BigDecimal(tracksInRange)));
				}
				fromTrack = minimumRange.getUpToTracks();
			} else {
				if (minimumRange.isFixed()) {
					minimum = minimum.add(minimumRange.getMinimum());
				} else {
					final int tracksInRange = albumTracks - fromTrack;
					minimum = minimum.add(minimumRange.getMinimum()
							.multiply(new BigDecimal(tracksInRange)));
				}
				break;
			}
		}
		return minimum;
	}

	// WARNING: dummy (simple) minimum computation
	private BigDecimal computeMinimumSimple(List<MinimumRange> minima, int albumTracks) {
		for (MinimumRange minimumRange : minima) {
			if (albumTracks >= minimumRange.getFromTracks() &&
					albumTracks <= minimumRange.getUpToTracks()) {
				if (minimumRange.isFixed()) {
					return minimumRange.getMinimum();
				} else {
					return minimumRange.getMinimum()
							.multiply(new BigDecimal(albumTracks));
				}
			}
		}
		return BigDecimal.ZERO;
	}

	private int getAlbumTracks(DsrLine line) {
		if (!Strings.isNullOrEmpty(line.getAlbumTracks())) {
			try {
				return Integer.parseInt(line.getAlbumTracks());
			} catch (NumberFormatException e) {
				// swallow
			}			
		}
		return 0;
	}

	private void computeDownloadPrice(DsrLine line) {
		final BigDecimal percentage = line.getProRata()
				.divide(new BigDecimal(100.0), workingScale, ROUNDING_MODE);
		//logger.debug("percentage: {}", percentage);
		final BigDecimal unitaryPrice = new BigDecimal(line.getUnitaryPrice());
		//logger.debug("unitaryPrice: {}", unitaryPrice);
		final int albumTracks = getAlbumTracks(line);
		//logger.debug("albumTracks: {}", albumTracks);
		final BigDecimal valoreCalcolato;
		BigDecimal minimum;
		if (albumTracks > 1) {
			final BigDecimal tracks = new BigDecimal(albumTracks);
			final BigDecimal priceBasis = unitaryPrice
	    			.divide(tracks, workingScale, ROUNDING_MODE);
			//logger.debug(">1 priceBasis: {}", priceBasis);
			valoreCalcolato = percentage
					.multiply(priceBasis);
			//logger.debug(">1 valoreCalcolato: {}", valoreCalcolato);
			switch (minimumAlgorithm) {
			case MINIMUM_ALGORITHM_INCREMENTAL:
				minimum = computeMinimumIncremental(line.getMinima(true), albumTracks)
					.divide(tracks, workingScale, ROUNDING_MODE);
				break;
			case MINIMUM_ALGORITHM_SIMPLE:
			default:
				minimum = computeMinimumSimple(line.getMinima(true), albumTracks)
					.divide(tracks, workingScale, ROUNDING_MODE);
				break;
			}
			//logger.debug(">1 minimum: {}", minimum);
	    	line.setPriceBasis(priceBasis
	    			.setScale(resultScale, ROUNDING_MODE));
		} else {
			valoreCalcolato = percentage
	    			.multiply(unitaryPrice);
			//logger.debug("=1 valoreCalcolato: {}", valoreCalcolato);
			minimum = line.getMinimum();
			//logger.debug("=1 minimum: {}", minimum);
	    	line.setPriceBasis(unitaryPrice
	    			.setScale(resultScale, ROUNDING_MODE));
		}
		// convert decision table minimum to DSR currency
		if (!equalsIgnoreCase(line.getCurrency(), line.getMinimumCurrency(), true)) {
			minimum = currencyConverter
					.convert(line.getMinimumCurrency(), minimum, line.getCurrency());
		}
		//logger.debug("currency minimum: {}", minimum);

		final BigDecimal valoreReale;
		if (valoreCalcolato.compareTo(minimum) > 0) {
			line.setRoyaltyType("P");
			valoreReale = valoreCalcolato;
		} else {
			line.setRoyaltyType("M");
			valoreReale = minimum;
		}
		//logger.debug("valoreReale: {}", valoreReale);
    	line.setRoyalty(valoreReale
    			.setScale(resultScale, ROUNDING_MODE));
    	line.setPrice(valoreReale
    			.multiply(line.getSalesCount())
    			.setScale(resultScale, ROUNDING_MODE));
		line.setOriginalPriceBasis(unitaryPrice
				.setScale(resultScale, ROUNDING_MODE));
		
    	if (!equalsIgnoreCase(line.getCurrency(), "EUR", true)) {
    		line.setRoyaltyEur(currencyConverter
					.convert(line.getCurrency(), line.getRoyalty(), "EUR")
					.setScale(resultScale, ROUNDING_MODE));
    		line.setPriceEur(currencyConverter
					.convert(line.getCurrency(), line.getPrice(), "EUR")
					.setScale(resultScale, ROUNDING_MODE));
		} else {
    		line.setRoyaltyEur(line.getRoyalty());
    		line.setPriceEur(line.getPrice());
		}
    	//logger.debug("line: {}", line);
//		System.out.println("albumTracks=" + albumTracks + ", minimum=" + minimum + ", line " + line);
	}

	private void computeYoutubePrice(DsrLine line) {

		// WARNING: YouTube price is "amount" / "sales count" DSR column instead of "unitary price"

		final BigDecimal percentage = line.getProRata()
				.divide(new BigDecimal(100.0), workingScale, ROUNDING_MODE);
		final BigDecimal unitaryPrice = new BigDecimal(line.getAmount())
			.divide(line.getSalesCount(), workingScale, ROUNDING_MODE);
		final int albumTracks = getAlbumTracks(line);
		final BigDecimal valoreCalcolato;
		BigDecimal minimum;
		if (albumTracks > 1) {
			final BigDecimal tracks = new BigDecimal(albumTracks);
			final BigDecimal priceBasis = unitaryPrice
	    			.divide(tracks, workingScale, ROUNDING_MODE);
			valoreCalcolato = percentage
					.multiply(priceBasis);
			switch (minimumAlgorithm) {
			case MINIMUM_ALGORITHM_INCREMENTAL:
				minimum = computeMinimumIncremental(line.getMinima(true), albumTracks)
					.divide(tracks, workingScale, ROUNDING_MODE);
				break;
			case MINIMUM_ALGORITHM_SIMPLE:
			default:
				minimum = computeMinimumSimple(line.getMinima(true), albumTracks)
					.divide(tracks, workingScale, ROUNDING_MODE);
				break;
			}
	    	line.setPriceBasis(priceBasis
	    			.setScale(resultScale, ROUNDING_MODE));
		} else {
			valoreCalcolato = percentage
	    			.multiply(unitaryPrice);
			minimum = line.getMinimum();
	    	line.setPriceBasis(unitaryPrice
	    			.setScale(resultScale, ROUNDING_MODE));
		}
		// convert decision table minimum to DSR currency
		if (!equalsIgnoreCase(line.getCurrency(), line.getMinimumCurrency(), true)) {
			minimum = currencyConverter
					.convert(line.getMinimumCurrency(), minimum, line.getCurrency());
		}

		final BigDecimal valoreReale;
		if (valoreCalcolato.compareTo(minimum) > 0) {
			line.setRoyaltyType("P");
			valoreReale = valoreCalcolato;
		} else {
			line.setRoyaltyType("M");
			valoreReale = minimum;
		}
    	line.setRoyalty(valoreReale
    			.setScale(resultScale, ROUNDING_MODE));
    	line.setPrice(valoreReale
    			.multiply(line.getSalesCount())
    			.setScale(resultScale, ROUNDING_MODE));
		line.setOriginalPriceBasis(unitaryPrice
				.setScale(resultScale, ROUNDING_MODE));

    	if (!equalsIgnoreCase(line.getCurrency(), "EUR", true)) {
    		line.setRoyaltyEur(currencyConverter
					.convert(line.getCurrency(), line.getRoyalty(), "EUR")
					.setScale(resultScale, ROUNDING_MODE));
    		line.setPriceEur(currencyConverter
					.convert(line.getCurrency(), line.getPrice(), "EUR")
					.setScale(resultScale, ROUNDING_MODE));
		} else {
    		line.setRoyaltyEur(line.getRoyalty());
    		line.setPriceEur(line.getPrice());
		}
	}
	
	public BigDecimal computeStreamingPrice(DsrMetadata metadata, DsrLine line) {
		return metadata.getRoyalty()
			.multiply(line.getSalesCount())
			.setScale(resultScale, ROUNDING_MODE);
	}
	
	public BigDecimal computeStreamingPriceEur(DsrMetadata metadata, DsrLine line) {
		return metadata.getRoyaltyEur()
				.multiply(line.getSalesCount())
				.setScale(resultScale, ROUNDING_MODE);
	}

}
