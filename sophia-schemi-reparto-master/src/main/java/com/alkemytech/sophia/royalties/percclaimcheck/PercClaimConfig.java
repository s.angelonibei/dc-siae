package com.alkemytech.sophia.royalties.percclaimcheck;

import com.google.gson.GsonBuilder;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class PercClaimConfig {

    private int id;
    private String codiceUuid;
    private String idDsp;
    private String territorio;
    private String tipoUtilizzo;
    private String offertaCommerciale;
    private BigDecimal percDem;
    private BigDecimal percDrm;

    @Override
    public String toString() {
        return new GsonBuilder()
                .create().toJson(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        return true;
    }

}
