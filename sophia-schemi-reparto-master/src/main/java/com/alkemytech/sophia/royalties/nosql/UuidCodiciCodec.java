package com.alkemytech.sophia.royalties.nosql;

import java.nio.charset.Charset;
import java.util.Map;

import com.alkemytech.sophia.commons.mmap.ObjectCodec;
import com.alkemytech.sophia.commons.util.UuidUtils;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class UuidCodiciCodec extends ObjectCodec<UuidCodici> {
	
	private final Charset charset;
	
	public UuidCodiciCodec(Charset charset) {
		super();
		this.charset = charset;
	}

	@Override
	public UuidCodici bytesToObject(byte[] bytes) {
		beginDecoding(bytes);
		final UuidCodici object = new UuidCodici(UuidUtils
				.base64ToUuid(getString(charset)));
		for (int codici = getPackedIntEx(); codici > 0; codici --) {
			final String societa = getString(charset);
			final String codice = getString(charset);
			object.add(societa, codice);
		}
		return object;
	}

	@Override
	public byte[] objectToBytes(UuidCodici object) {
		beginEncoding();
		putString(UuidUtils.uuidToBase64(object.uuid), charset);
		putPackedIntEx(object.codici.size());
		for (Map.Entry<String,String> codice : object.codici.entrySet()) {
			putString(codice.getKey(), charset);
			putString(codice.getValue(), charset);
		}
		return commitEncoding();
	}
	
}
