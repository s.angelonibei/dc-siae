package com.alkemytech.sophia.royalties.region;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class GoogleMapsRegions {

	// World
	//  002: Africa
	//      015: Northern Africa
	//           DZ EG EH LY MA SD TN
	//      011: Western Africa
	//           BF BJ CI CV GH GM GN GW LR ML MR NE NG SH SL SN TG
	//      017: Middle Africa
	//           AO CD ZR CF CG CM GA GQ ST TD
	//      014: Eastern Africa
	//           BI DJ ER ET KE KM MG MU MW MZ RE RW SC SO TZ UG YT ZM ZW
	//      018: Southern Africa
	//           BW LS NA SZ ZA
	//  150: Europe
	//      154: Northern Europe
	//           GG IM JE AX DK EE FI FO GB IE IM IS LT LV NO SE SJ
	//      155: Western Europe
	//           AT BE CH DE DD FR FX LI LU MC NL
	//      151: Eastern Europe
	//           BG BY CZ HU MD PL RO RU SU SK UA
	//      039: Southern Europe
	//           AD AL BA ES GI GR HR IT ME MK MT CS RS PT SI SM VA YU
	//  019: Americas
	//      021: Northern America
	//           BM CA GL PM US
	//      029: Caribbean
	//           AG AI AN AW BB BL BS CU DM DO GD GP HT JM KN KY LC MF MQ MS PR TC TT VC VG VI
	//      013: Central America
	//           BZ CR GT HN MX NI PA SV
	//      005: South America
	//           AR BO BR CL CO EC FK GF GY PE PY SR UY VE
	//  142: Asia
	//      143: Central Asia
	//            TM TJ KG KZ UZ
	//      030: Eastern Asia
	//           CN HK JP KP KR MN MO TW
	//      034: Southern Asia
	//           AF BD BT IN IR LK MV NP PK
	//      035: South-Eastern Asia
	//           BN ID KH LA MM BU MY PH SG TH TL TP VN
	//      145: Western Asia
	//           AE AM AZ BH CY GE IL IQ JO KW LB OM PS QA SA NT SY TR YE YD
	//  009: Oceania
	//      053: Australia and New Zealand
	//           AU NF NZ
	//      054: Melanesia
	//           FJ NC PG SB VU
	//      057: Micronesia
	//           FM GU KI MH MP NR PW
	//      061: Polynesia		
	//           AS CK NU PF PN TK TO TV WF WS

	private static final String[] REGIONS = { "Africa", "Europe", "Americas", "Asia", "Oceania" };
	private static final String[][] SUBREGIONS = {
			{ "Northern Africa", "Western Africa", "Middle Africa", "Eastern Africa", "Southern Africa" },
			{ "Northern Europe", "Western Europe", "Eastern Europe", "Southern Europe" },
			{ "Northern America", "Caribbean", "Central America", "South America" },
			{ "Central Asia", "Eastern Asia", "Southern Asia", "South-Eastern Asia", "Western Asia" },
			{ "Australia and New Zealand", "Melanesia", "Micronesia", "Polynesia" },
	};
	private static final String[][][] COUNTRIES = {
			{ { "DZ", "EG", "EH", "LY", "MA", "SD", "TN" },
				{ "BF", "BJ", "CI", "CV", "GH", "GM", "GN", "GW", "LR", "ML", "MR", "NE", "NG", "SH", "SL", "SN", "TG" },
				{ "AO", "CD", "ZR", "CF", "CG", "CM", "GA", "GQ", "ST", "TD" },
				{ "BI", "DJ", "ER", "ET", "KE", "KM", "MG", "MU", "MW", "MZ", "RE", "RW", "SC", "SO", "TZ", "UG", "YT", "ZM", "ZW" },
				{ "BW", "LS", "NA", "SZ", "ZA" } },
			{ { "GG", "IM", "JE", "AX", "DK", "EE", "FI", "FO", "GB", "IE", "IM", "IS", "LT", "LV", "NO", "SE", "SJ" },
				{ "AT", "BE", "CH", "DE", "DD", "FR", "FX", "LI", "LU", "MC", "NL" },
				{ "BG", "BY", "CZ", "HU", "MD", "PL", "RO", "RU", "SU", "SK", "UA" },
				{ "AD", "AL", "BA", "ES", "GI", "GR", "HR", "IT", "ME", "MK", "MT", "CS", "RS", "PT", "SI", "SM", "VA", "YU" } },
			{ { "BM", "CA", "GL", "PM", "US" },
				{ "AG", "AI", "AN", "AW", "BB", "BL", "BS", "CU", "DM", "DO", "GD", "GP", "HT", "JM", "KN", "KY", "LC", "MF", "MQ", "MS", "PR", "TC", "TT", "VC", "VG", "VI" },
				{ "BZ", "CR", "GT", "HN", "MX", "NI", "PA", "SV" }, 
				{ "AR", "BO", "BR", "CL", "CO", "EC", "FK", "GF", "GY", "PE", "PY", "SR", "UY", "VE" } },
			{ { "TM", "TJ", "KG", "KZ", "UZ" },
				{ "CN", "HK", "JP", "KP", "KR", "MN", "MO", "TW" },
				{ "AF", "BD", "BT", "IN", "IR", "LK", "MV", "NP", "PK" },
				{ "BN", "ID", "KH", "LA", "MM", "BU", "MY", "PH", "SG", "TH", "TL", "TP", "VN" },
				{ "AE", "AM", "AZ", "BH", "CY", "GE", "IL", "IQ", "JO", "KW", "LB", "OM", "PS", "QA", "SA", "NT", "SY", "TR", "YE", "YD" } },
			{ { "AU", "NF", "NZ" },
				{ "FJ", "NC", "PG", "SB", "VU" },
				{ "FM", "GU", "KI", "MH", "MP", "NR", "PW" },
				{ "AS", "CK", "NU", "PF", "PN", "TK", "TO", "TV", "WF", "WS" } },
	};

	private static final Map<String, String> countryToRegion = new ConcurrentHashMap<>();
	private static final Map<String, String> countryToSubregion = new ConcurrentHashMap<>();

	static {
		for (int i = 0; i < REGIONS.length; i ++) {
			final String region = REGIONS[i];
			for (int j = 0; j < SUBREGIONS[i].length; j ++) {
				final String subregion = SUBREGIONS[i][j];
				for (int k = 0; k < COUNTRIES[i][j].length; k ++) {
					final String country = COUNTRIES[i][j][k];
					countryToRegion.put(country, region);
					countryToSubregion.put(country, subregion);
				}
			}
		}
	}
	
	public static String getRegion(String territory) {
		return countryToRegion.get(territory);
	}
	
	public static String getSubregion(String territory) {
		return countryToSubregion.get(territory);
	}

}
