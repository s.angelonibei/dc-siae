package com.alkemytech.sophia.royalties.nosql;

import java.util.HashMap;
import java.util.Map;

import com.alkemytech.sophia.commons.nosql.NoSqlEntity;
import com.alkemytech.sophia.commons.util.UuidUtils;
import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class UuidCodici implements NoSqlEntity {

	public final String uuid;
	public final Map<String, String> codici;
	
	public UuidCodici(String uuid) {
		super();
		if (null == uuid || 0 == uuid.length()) {
			throw new IllegalArgumentException("uuid is null or empty");
		}
		this.uuid = uuid;
		this.codici = new HashMap<>();
	}

	public void add(String societa, String codice) {
		codici.put(societa, codice);
	}
	
	public void addAll(Map<String, String> codici) {
		this.codici.putAll(codici);
	}

	@Override
	public String getPrimaryKey() {
		return UuidUtils.uuidToBase64(uuid);
	}

	@Override
	public String getSecondaryKey() {
		return null;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}
	
}
