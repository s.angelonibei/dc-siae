package com.alkemytech.sophia.royalties.scheme;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.alkemytech.sophia.royalties.nosql.Territories;
import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class RoyaltyScheme {

	public final List<Royalty> royalties;
	public Territories territories;
	public float demTotal;
	public float drmTotal;
	public boolean temporaryWork;
	public boolean irregularWork;

	public RoyaltyScheme() {
		super();
		this.royalties = new ArrayList<>();
	}

	public RoyaltyScheme(RoyaltyScheme that) {
		super();
		this.royalties = new ArrayList<>(that.royalties);
		this.territories = new Territories(that.territories);
		this.demTotal = that.demTotal;
		this.drmTotal = that.drmTotal;
		this.temporaryWork = that.temporaryWork;
	}

	public RoyaltyScheme addRoyalty(Royalty royalty) {
		royalties.add(royalty);
		return this;
	}

	public RoyaltyScheme addRoyalties(Collection<Royalty> royalties) {
		this.royalties.addAll(royalties);
		return this;
	}
	
	public RoyaltyScheme setTerritories(Territories territories) {
		this.territories = territories;
		return this;
	}
	
	public RoyaltyScheme setDemTotal(float demTotal) {
		this.demTotal = demTotal;
		return this;
	}

	public RoyaltyScheme setDrmTotal(float drmTotal) {
		this.drmTotal = drmTotal;
		return this;
	}

	public RoyaltyScheme setTemporaryWork(boolean temporaryWork) {
		this.temporaryWork = temporaryWork;
		return this;
	}

	public RoyaltyScheme setIrregularWork(boolean irregularWork) {
		this.irregularWork = irregularWork;
		return this;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}
	
}
