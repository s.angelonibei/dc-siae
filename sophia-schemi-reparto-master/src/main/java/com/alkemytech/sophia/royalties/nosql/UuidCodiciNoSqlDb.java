package com.alkemytech.sophia.royalties.nosql;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.mmap.ObjectCodec;
import com.alkemytech.sophia.commons.nosql.ConcurrentNoSql;
import com.alkemytech.sophia.commons.nosql.NoSql;
import com.alkemytech.sophia.commons.nosql.NoSql.Selector;
import com.alkemytech.sophia.commons.nosql.NoSqlConfig;
import com.alkemytech.sophia.commons.trie.ConcurrentTrieEx;
import com.alkemytech.sophia.commons.util.FileUtils;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.commons.util.UuidUtils;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class UuidCodiciNoSqlDb {

	private static final Logger logger = LoggerFactory.getLogger(UuidCodiciNoSqlDb.class);

	private static class ThreadLocalObjectCodec extends ThreadLocal<ObjectCodec<UuidCodici>> {

		private final Charset charset;
		
	    public ThreadLocalObjectCodec(Charset charset) {
			super();
			this.charset = charset;
		}

		@Override
	    protected synchronized ObjectCodec<UuidCodici> initialValue() {
	        return new UuidCodiciCodec(charset);
	    }
	    
	}
	
	private NoSql<UuidCodici> database;

	public UuidCodiciNoSqlDb(Properties configuration, String propertyPrefix) {
		super();
		final File homeFolder = new File(configuration
				.getProperty(propertyPrefix + ".home_folder"));
		final boolean readOnly = "true".equalsIgnoreCase(configuration
				.getProperty(propertyPrefix + ".read_only", "true"));
		final int pageSize = TextUtils.parseIntSize(configuration
				.getProperty(propertyPrefix + ".page_size", "-1"));
		final int cacheSize = TextUtils.parseIntSize(configuration
				.getProperty(propertyPrefix + ".cache_size", "0"));
		final boolean createAlways = "true".equalsIgnoreCase(configuration
				.getProperty(propertyPrefix + ".create_always"));
		final String trieClassName = configuration
				.getProperty(propertyPrefix + ".trie_class_name",
						ConcurrentTrieEx.class.getName());
		if (!readOnly && createAlways && homeFolder.exists()) {
			FileUtils.deleteFolder(homeFolder, true);
		}
		logger.debug("propertyPrefix {}", propertyPrefix);
		database = new ConcurrentNoSql<>(new NoSqlConfig<UuidCodici>()
				.setHomeFolder(homeFolder)
				.setReadOnly(readOnly)
				.setPageSize(pageSize)
				.setCacheSize(cacheSize)
				.setCodec(new ThreadLocalObjectCodec(StandardCharsets.UTF_8))
				.setTrieClassName(trieClassName)
				.setSecondaryIndex(false));
	}
	
	public File getHomeFolder() {
		return database.getHomeFolder();
	}
	
	public void truncate() {
		database.truncate();
	}

	public void defrag() {
		database.defrag();
	}

	public UuidCodici get(String pk) {
		return database.getByPrimaryKey(UuidUtils.uuidToBase64(pk));
	}

	public boolean put(UuidCodici bean) {
		database.upsert(bean);
		return true;
	}

	public void select(Selector<UuidCodici> selector) {
		database.select(selector);
	}

}
