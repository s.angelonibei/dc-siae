package com.alkemytech.sophia.royalties.claim;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import com.alkemytech.sophia.commons.util.BigDecimals;
import com.alkemytech.sophia.royalties.scheme.Royalty;
import com.alkemytech.sophia.royalties.scheme.RoyaltyScheme;
import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class ClaimInfo {

	private static final RoundingMode ROUNDING_MODE = RoundingMode.HALF_UP;
	private static final BigDecimal ONE_HUNDRED = new BigDecimal("100");
	
	public String workCode;
	public RoyaltyScheme royaltyScheme;
	public List<Royalty> royalties;
	public BigDecimal claimLicensorDem;
	public BigDecimal claimLicensorDrm;
		
	public String formatWorkCode() {
		return null == workCode ? "" : workCode;
	}

	public String formatClaimDem() {
		return null == claimLicensorDem ? 
				"" : BigDecimals.toPlainString(claimLicensorDem);
	}
	
	public String formatClaimDrm() {
		return null == claimLicensorDrm ? 
				"" : BigDecimals.toPlainString(claimLicensorDrm);
	}
	
	public String formatClaimDemFraction(BigDecimal totalClaimLicensorDem) {
		if (null == claimLicensorDem) {
			return "";
		}
		if (BigDecimals.almostEquals(totalClaimLicensorDem, BigDecimal.ZERO)) {
			return "0";
		}
		return BigDecimals.toPlainString(ONE_HUNDRED
				.multiply(claimLicensorDem)
				.divide(totalClaimLicensorDem, ROUNDING_MODE));
	}
	
	public String formatClaimDrmFraction(BigDecimal totalClaimLicensorDrm) {
		if (null == claimLicensorDrm) {
			return "";
		}
		if (BigDecimals.almostEquals(totalClaimLicensorDrm, BigDecimal.ZERO)) {
			return "0";
		}
		return BigDecimals.toPlainString(ONE_HUNDRED
				.multiply(claimLicensorDrm)
				.divide(totalClaimLicensorDrm, ROUNDING_MODE));
	}
	
	public ClaimInfo setRoyaltyScheme(RoyaltyScheme royaltyScheme) {
		this.royaltyScheme = royaltyScheme;
		return this;
	}

	public ClaimInfo setWorkCode(String workCode) {
		this.workCode = workCode;
		return this;
	}

	public ClaimInfo setRoyalties(List<Royalty> royalties) {
		this.royalties = royalties;
		return this;
	}

	public ClaimInfo setClaimLicensorDem(BigDecimal claimLicensorDem) {
		this.claimLicensorDem = claimLicensorDem;
		return this;
	}

	public ClaimInfo setClaimLicensorDrm(BigDecimal claimLicensorDrm) {
		this.claimLicensorDrm = claimLicensorDrm;
		return this;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}
	
}
