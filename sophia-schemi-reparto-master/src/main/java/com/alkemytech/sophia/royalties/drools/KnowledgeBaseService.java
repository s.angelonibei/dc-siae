package com.alkemytech.sophia.royalties.drools;

import java.io.File;

import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.DecisionTableConfiguration;
import org.drools.builder.DecisionTableInputType;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.io.ResourceFactory;

import com.google.inject.Inject;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class KnowledgeBaseService {

	@Inject
	protected KnowledgeBaseService() {
		super();
	}
	
	public KnowledgeBase getKnowledgeBaseFromXls(File xlsFile) throws KnowledgeBaseException {
		final DecisionTableConfiguration configuration = KnowledgeBuilderFactory
				.newDecisionTableConfiguration();
		configuration.setInputType(DecisionTableInputType.XLS);
		final KnowledgeBuilder knowledgeBuilder = KnowledgeBuilderFactory
				.newKnowledgeBuilder();
		knowledgeBuilder.add(ResourceFactory.newFileResource(xlsFile),
				ResourceType.DTABLE, configuration);
		if (knowledgeBuilder.hasErrors()) {
			throw new KnowledgeBaseException(knowledgeBuilder.getErrors().toString());
		}
		final KnowledgeBase knowledgeBase = KnowledgeBaseFactory.newKnowledgeBase();
		knowledgeBase.addKnowledgePackages(knowledgeBuilder.getKnowledgePackages());
		return knowledgeBase;
	}
	
}


