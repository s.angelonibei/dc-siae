package com.alkemytech.sophia.royalties;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.collecting.SiaeCode;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.mmap.UnsafeMemoryMap;
import com.alkemytech.sophia.commons.sqs.MessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.SqsMessageHelper;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.alkemytech.sophia.commons.sqs.TrieMessageDeduplicator;
import com.alkemytech.sophia.commons.trie.ConcurrentTrieEx;
import com.alkemytech.sophia.commons.trie.ExactTrie;
import com.alkemytech.sophia.commons.trie.FixedLengthStringBinding;
import com.alkemytech.sophia.commons.trie.LeafBinding;
import com.alkemytech.sophia.commons.util.FileUtils;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.HeartBeat;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.royalties.country.Country;
import com.alkemytech.sophia.royalties.country.JdbcCountryService;
import com.alkemytech.sophia.royalties.jdbc.UlisseDataSource;
import com.alkemytech.sophia.royalties.nosql.Opera;
import com.alkemytech.sophia.royalties.nosql.OperaNoSqlDb;
import com.alkemytech.sophia.royalties.nosql.QuotaRiparto;
import com.alkemytech.sophia.royalties.nosql.SchemaRiparto;
import com.alkemytech.sophia.royalties.nosql.SchemaTerritorio;
import com.alkemytech.sophia.royalties.nosql.SchemaTerritorioNoSqlDb;
import com.alkemytech.sophia.royalties.nosql.Territories;
import com.google.common.base.Charsets;
import com.google.common.base.Strings;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Scopes;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import com.ibm.icu.text.SimpleDateFormat;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class UlisseNoSql extends MicroService {

    private static final Logger logger = LoggerFactory.getLogger(UlisseNoSql.class);

    protected static class GuiceModuleExtension extends GuiceModule {

        public GuiceModuleExtension(String[] args, String resourceName) {
            super(args, resourceName);
        }

        @Override
        protected void configure() {
            super.configure();
            // amazon service(s)
            bind(S3.class)
                    .in(Scopes.SINGLETON);
            bind(SQS.class)
                    .in(Scopes.SINGLETON);
            // data source(s)
            bind(DataSource.class)
                    .annotatedWith(Names.named("ULISSE"))
                    .to(UlisseDataSource.class)
                    .asEagerSingleton();
            // other binding(s)
            bind(JdbcCountryService.class)
                    .asEagerSingleton();
            bind(Configuration.class)
                    .asEagerSingleton();
            bind(UlisseNoSql.class)
                    .asEagerSingleton();
        }

    }

    public static void main(String[] args) {
        try {
            final UlisseNoSql instance = Guice
                    .createInjector(new GuiceModuleExtension(args,
                            "/ulisse-nosql.properties"))
                    .getInstance(UlisseNoSql.class)
                    .startup();
            try {
                instance.process(args);
            } finally {
                instance.shutdown();
            }
        } catch (Throwable e) {
            logger.error("main", e);
        } finally {
            System.exit(0);
        }
    }

    private final SQS sqs;
    private final JdbcCountryService countryService;
    private final DataSource ulisseDataSource;

    @Inject
    protected UlisseNoSql(Configuration configuration,
                          @Named("charset") Charset charset,
                          S3 s3, SQS sqs, JdbcCountryService countryService,
                          @Named("ULISSE") DataSource ulisseDataSource) {
        super(configuration, charset, s3);
        this.sqs = sqs;
        this.countryService = countryService;
        this.ulisseDataSource = ulisseDataSource;
    }

    public UlisseNoSql startup() throws IOException {
        if ("true".equalsIgnoreCase(configuration.getProperty("ulisse_nosql.locked", "true"))) {
            throw new IllegalStateException("application locked");
        }
        super.startup();
        sqs.startup();
        return this;
    }

    public UlisseNoSql shutdown() throws IOException {
        sqs.shutdown();
        super.shutdown();
        return this;
    }

    public void process(String[] args) throws Exception {
        final int bindPort = Integer.parseInt(configuration.getProperty("ulisse_nosql.bind_port",
                configuration.getProperty("default.bind_port", "0")));
        // bind lock tcp port
        try (ServerSocket socket = new ServerSocket(bindPort)) {
            logger.debug("socket bound to {}", socket.getLocalSocketAddress());

            // standalone mode
            if (Arrays.asList(args).contains("standalone") ||
                    "true".equalsIgnoreCase(configuration
                            .getProperty("ulisse_nosql.standalone", "false"))) {
                final JsonObject input = GsonUtils.fromJson(configuration
                        .getProperty("ulisse_nosql.standalone.message_body"), JsonObject.class);
                final JsonObject output = new JsonObject();
                logger.debug("standalone input {}", new GsonBuilder()
                        .setPrettyPrinting().create().toJson(input));
                processMessage(input, output);
                logger.debug("standalone output {}", new GsonBuilder()
                        .setPrettyPrinting().create().toJson(output));
                return;
            }

            // sqs message pump
            final SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration
                    .getProperties(), "ulisse_nosql.sqs");
            final MessageDeduplicator deduplicator = new TrieMessageDeduplicator(new File(configuration
                    .getProperty("ulisse_nosql.sqs.deduplicator_folder")));
            sqsMessagePump.pollingLoop(deduplicator, new SqsMessagePump.Consumer() {

                private JsonObject output;
                private JsonObject error;

                @Override
                public JsonObject getStartedMessagePayload(JsonObject message) {
                    output = null;
                    error = null;
                    return SqsMessageHelper.formatContext();
                }

                @Override
                public boolean consumeMessage(JsonObject message) {
                    try {
                        output = new JsonObject();
                        processMessage(GsonUtils
                                .getAsJsonObject(message, "body"), output);
                        error = null;
                        return true;
                    } catch (Exception e) {
                        output = null;
                        error = SqsMessageHelper.formatError(e);
                        return false;
                    }
                }

                @Override
                public JsonObject getCompletedMessagePayload(JsonObject message) {
                    return output;
                }

                @Override
                public JsonObject getFailedMessagePayload(JsonObject message) {
                    return error;
                }

            });
        }
    }

    private void checkSchemiOpera(Map<String, Set<String>> opereInconsistenti, Opera opera) {
        Set<String> territoriDuplicati = opereInconsistenti.get(opera.codiceOpera);
        for (SchemaRiparto schema1 : opera.schemi) {
            for (SchemaRiparto schema2 : opera.schemi) {
                if (schema1.idSchemaRiparto != schema2.idSchemaRiparto) {
                    final Territories intersection = schema1
                            .territories.intersection(schema2.territories);
                    if (!intersection.isEmpty()) {
                        if (null == territoriDuplicati) {
                            territoriDuplicati = new HashSet<>();
                            opereInconsistenti.put(opera.codiceOpera, territoriDuplicati);
                        }
                        final String codes = intersection.toString();
                        for (int i = 0; i < codes.length(); i += 2) {
                            territoriDuplicati.add(codes.substring(i, i + 2));
                        }
                    }
                }
            }
        }
    }

    private void processMessage(JsonObject input, JsonObject output) throws Exception {

//		{
//		  "body": {
//		    "year": "2017",
//		    "month": "03",
//		    "techsysdate": true,
//			"worldFromItaly": true,
//		    "force": true
//		  },
//		  "header": {
//		    "queue": "dev_to_process_ulisse_dump_v2",
//		    "timestamp": "2018-05-10T00:00:00.000000+02:00",
//		    "uuid": "7f36a2bb-0637-4d4d-9990-4ed9644adefc",
//		    "sender": "aws-console"
//		  }
//		}

        logger.debug("dumping ulisse");
        final long startTimeMillis = System.currentTimeMillis();
        final String datetime = new SimpleDateFormat(configuration
                .getProperty("ulisse_nosql.datetime_format", configuration
                        .getProperty("default.datetime_format", "yyyyMMddHHmmss")))
                .format(new Date());
        logger.debug("datetime {}", datetime);

        // parse input json message
        String year = GsonUtils.getAsString(input, "year");
        String month = GsonUtils.getAsString(input, "month");
        if (Strings.isNullOrEmpty(year) ||
                Strings.isNullOrEmpty(month)) {
            throw new IllegalArgumentException("invalid input message");
        }
        String day = GsonUtils.getAsString(input, "day", configuration
                .getProperty("ulisse_nosql.reference_day", "01"));
        final boolean techsysdate = GsonUtils.getAsBoolean(input, "techsysdate",
                "true".equalsIgnoreCase(configuration.getProperty("ulisse_nosql.techsysdate", "true")));
        final boolean worldFromItaly = GsonUtils.getAsBoolean(input, "worldFromItaly",
                "true".equalsIgnoreCase(configuration.getProperty("ulisse_nosql.world_from_italy", "true")));
        final JsonArray stepsJson = GsonUtils.getAsJsonArray(input, "steps");
        final Set<String> steps = new HashSet<>();
        if (null != stepsJson) {
            for (JsonElement stepJson : stepsJson)
                steps.add(stepJson.getAsString());
        } else {
            steps.addAll(Arrays.asList("create", "upload"));
        }
        final String period = fixPeriod(month);
        year = String.format("%04d", Integer.parseInt(year));
        month = fixMonth(month);
        day = String.format("%02d", Integer.parseInt(day));
        logger.debug("year {}", year);
        logger.debug("month {}", month);
        logger.debug("day {}", day);
        logger.debug("period {}", period);
        logger.debug("techsysdate {}", techsysdate);
        logger.debug("worldFromItaly {}", worldFromItaly);
        logger.debug("steps {}", steps);

        // sql reference date
        final String sqldate = String
                .format("to_date('%s-%s-%s', 'YYYY-MM-DD')", year, month, day);
        logger.debug("sqldate {}", sqldate);
        final String techsqldate = techsysdate ? "sysdate" : sqldate;
        logger.debug("techsqldate {}", techsqldate);

        // configuration tags
        configuration.setTag("{datetime}", datetime);
        configuration.setTag("{year}", year);
        configuration.setTag("{month}", month);
        configuration.setTag("{day}", day);
        configuration.setTag("{period}", period);
        configuration.setTag("{sqldate}", sqldate);
        configuration.setTag("{techsqldate}", techsqldate);

        // configuration
        final boolean debug = "true".equalsIgnoreCase(configuration
                .getProperty("ulisse_nosql.debug", configuration.getProperty("default.debug")));
        final File homeFolder = new File(configuration
                .getProperty("default.home_folder"));

        // initialize country service
        countryService.loadCountries();

        // post execution tasks
        final List<Runnable> postExecTasks = new ArrayList<>();

        ///////////
        // create
        ///////////

        if (steps.contains("create")) {

            try (final Connection connection = ulisseDataSource.getConnection()) {
                logger.debug("jdbc connected to {} {}", connection.getMetaData()
                        .getDatabaseProductName(), connection.getMetaData().getURL());

                //////////////////////
                // schema_territorio
                //////////////////////

                if ("true".equalsIgnoreCase(configuration
                        .getProperty("ulisse_nosql.schema_territorio.read_only"))) {
                    configuration.setProperty("ulisse_nosql.schema_territorio.page_size", "-1");
                    configuration.setProperty("ulisse_nosql.schema_territorio.read_only", "true");
                    configuration.setProperty("ulisse_nosql.schema_territorio.create_always", "false");
                }
                final SchemaTerritorioNoSqlDb schemaTerritorioNoSqlDb = new SchemaTerritorioNoSqlDb(configuration
                        .getProperties(), "ulisse_nosql.schema_territorio");

                if (!"true".equalsIgnoreCase(configuration
                        .getProperty("ulisse_nosql.schema_territorio.read_only"))) {
                    final long lapTimeMillis = System.currentTimeMillis();
                    logger.debug("creating schema_territorio nosql");
                    try (final Statement statement = connection
                            .createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);) {
                        statement.setFetchSize(Integer.parseInt(configuration
                                .getProperty("ulisse_nosql.schema_territorio.fetch_size", "5000")));
                        long count = 0L;
                        final Set<String> uniqueTerritories = new HashSet<>();
                        final HeartBeat heartbeat = HeartBeat.constant("schema_territorio", Integer
                                .parseInt(configuration.getProperty("ulisse_nosql.schema_territorio.heartbeat", "10000")));
                        final Territories worldTerritories = new Territories();
                        if (worldFromItaly) {
                            final Set<Country> worldCountries = countryService
                                    .getCountries(countryService.getIdTerritorio("WORLD"));
                            for (Country country : worldCountries) {
                                worldTerritories.add(country.getTisA()); // two digits code
                            }
                        }
                        // prepare sql statement
                        final String sql = configuration
                                .getProperty("ulisse_nosql.schema_territorio.sql");
                        logger.debug("executing query {}", sql);
                        // execute query
                        try (final ResultSet resultSet = statement.executeQuery(sql)) {
                            logger.debug("query executed in {}",
                                    TextUtils.formatDuration(System.currentTimeMillis() - lapTimeMillis));
                            final Set<Country> countries = new HashSet<>();
                            String lastIdSchemaRiparto = null;
                            // loop on result set
                            while (resultSet.next()) {
                                heartbeat.pump();
                                final String idSchemaRiparto = resultSet.getString("id_schema_riparto");
                                final long idTerritorio = resultSet.getLong("id_territorio");
                                final String operatoreLogico = resultSet.getString("operatore_logico");
                                if (null != lastIdSchemaRiparto && !lastIdSchemaRiparto.equals(idSchemaRiparto)) {
                                    if (!countries.isEmpty()) {
                                        final Territories territories = new Territories();
                                        for (Country country : countries) {
                                            territories.add(country.getTisA()); // two digits code
                                        }
                                        if (!territories.isEmpty()) {
                                            if (worldFromItaly) {
                                                if (territories.contains("IT")) {
                                                    final SchemaTerritorio schemaTerritorio = new SchemaTerritorio(Long
                                                            .parseLong(lastIdSchemaRiparto), worldTerritories);
                                                    schemaTerritorioNoSqlDb.put(schemaTerritorio);
                                                    count++;
                                                    uniqueTerritories.add(worldTerritories.toString());
                                                }
                                            } else {
                                                final SchemaTerritorio schemaTerritorio = new SchemaTerritorio(Long
                                                        .parseLong(lastIdSchemaRiparto), territories);
                                                schemaTerritorioNoSqlDb.put(schemaTerritorio);
                                                count++;
                                                uniqueTerritories.add(territories.toString());
                                            }
                                            countries.clear();
                                        }
                                    }
                                }
                                lastIdSchemaRiparto = idSchemaRiparto;
                                if ("I".equalsIgnoreCase(operatoreLogico)) {
                                    countries.addAll(countryService.getCountries(idTerritorio));
                                } else if ("E".equalsIgnoreCase(operatoreLogico)) {
                                    countries.removeAll(countryService.getCountries(idTerritorio));
                                }
                            }
                            // last id_schema_riparto
                            if (null != lastIdSchemaRiparto && !countries.isEmpty()) {
                                final Territories territories = new Territories();
                                for (Country country : countries) {
                                    territories.add(country.getTisA()); // two digits code
                                }
                                if (!territories.isEmpty()) {
                                    final SchemaTerritorio schemaTerritorio = new SchemaTerritorio(Long
                                            .parseLong(lastIdSchemaRiparto), territories);
                                    schemaTerritorioNoSqlDb.put(schemaTerritorio);
                                    count++;
                                    countries.clear();
                                    uniqueTerritories.add(territories.toString());
                                }
                            }
                        }
                        logger.debug("total parsed rows {}", heartbeat.getTotalPumps());
                        logger.debug("total schema_territorio records {}", count);
                        logger.debug("unique territories {}", uniqueTerritories.size());
                        // output stats
                        final JsonObject queryStats = new JsonObject();
                        queryStats.addProperty("sql", sql);
                        queryStats.addProperty("rownum", heartbeat.getTotalPumps());
                        queryStats.addProperty("count", count);
                        queryStats.addProperty("duration", TextUtils
                                .formatDuration(System.currentTimeMillis() - lapTimeMillis));
                        output.add("schema_territorio", queryStats);
                    }
                    logger.debug("schema_territorio nosql created in {}",
                            TextUtils.formatDuration(System.currentTimeMillis() - lapTimeMillis));
                }

                // delete schema_territorio nosql
                if ("true".equalsIgnoreCase(configuration
                        .getProperty("ulisse_nosql.schema_territorio.delete_when_done"))) {
                    postExecTasks.add(new Runnable() {
                        @Override
                        public void run() {
                            logger.debug("deleting schema_territorio nosql {}",
                                    schemaTerritorioNoSqlDb.getHomeFolder());
                            schemaTerritorioNoSqlDb.truncate();
                            FileUtils.deleteFolder(schemaTerritorioNoSqlDb.getHomeFolder(), true);
                        }
                    });
                }

                /////////////////
                // codice_opera
                /////////////////

                final ExactTrie<String> ulisseCodiceOperaTrie;
                final File ulisseCodiceOperaTrieHomeFolder = new File(configuration
                        .getProperty("ulisse_nosql.codice_opera.home_folder"));
                final LeafBinding<String> codiceOperaSerializer = new FixedLengthStringBinding(11, charset);
                if ("true".equalsIgnoreCase(configuration
                        .getProperty("ulisse_nosql.codice_opera.read_only"))) {
                    ulisseCodiceOperaTrie = new ConcurrentTrieEx<>(UnsafeMemoryMap
                            .open(ulisseCodiceOperaTrieHomeFolder, -1, true), codiceOperaSerializer);
                } else {
                    ulisseCodiceOperaTrie = new ConcurrentTrieEx<>(UnsafeMemoryMap
                            .create(ulisseCodiceOperaTrieHomeFolder,
                                    TextUtils.parseIntSize(configuration
                                            .getProperty("ulisse_nosql.codice_opera.page_size"))), codiceOperaSerializer);
                }

                if (!"true".equalsIgnoreCase(configuration
                        .getProperty("ulisse_nosql.codice_opera.read_only"))) {
                    final long lapTimeMillis = System.currentTimeMillis();
                    logger.debug("creating codice_opera trie");
                    try (final Statement statement = connection
                            .createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);) {
                        statement.setFetchSize(Integer.parseInt(configuration
                                .getProperty("ulisse_nosql.codice_opera.fetch_size", "5000")));
                        long count = 0L;
                        final HeartBeat heartbeat = HeartBeat.constant("codice_opera", Integer
                                .parseInt(configuration.getProperty("ulisse_nosql.codice_opera.heartbeat", "10000")));
                        // prepare sql statement
                        final String sql = configuration
                                .getProperty("ulisse_nosql.codice_opera.sql");
                        logger.debug("executing query {}", sql);
                        // execute query
                        try (final ResultSet resultSet = statement.executeQuery(sql)) {
                            logger.debug("query executed in {}",
                                    TextUtils.formatDuration(System.currentTimeMillis() - lapTimeMillis));
                            // loop on result set
                            while (resultSet.next()) {
                                heartbeat.pump();
                                final String codice = SiaeCode.normalize(resultSet.getString("codice"));
                                final String idSchemaRiparto = resultSet.getString("id_schema_riparto");
                                if (!Strings.isNullOrEmpty(codice) &&
                                        !Strings.isNullOrEmpty(idSchemaRiparto)) {
                                    ulisseCodiceOperaTrie.upsert(idSchemaRiparto, codice);
                                    count++;
                                }
                            }
                        }
                        logger.debug("total parsed rows {}", heartbeat.getTotalPumps());
                        logger.debug("total codice_opera trie leaves {}", count);
                        // output stats
                        final JsonObject queryStats = new JsonObject();
                        queryStats.addProperty("sql", sql);
                        queryStats.addProperty("rownum", heartbeat.getTotalPumps());
                        queryStats.addProperty("count", count);
                        queryStats.addProperty("duration", TextUtils
                                .formatDuration(System.currentTimeMillis() - lapTimeMillis));
                        output.add("codice_opera", queryStats);
                    }

                    logger.debug("codice_opera trie created in {}",
                            TextUtils.formatDuration(System.currentTimeMillis() - lapTimeMillis));
                }

                // delete codice_opera nosql
                if ("true".equalsIgnoreCase(configuration
                        .getProperty("ulisse_nosql.codice_opera.delete_when_done"))) {
                    postExecTasks.add(new Runnable() {
                        @Override
                        public void run() {
                            logger.debug("deleting codice_opera nosql {}",
                                    schemaTerritorioNoSqlDb.getHomeFolder());
                            ulisseCodiceOperaTrie.clear();
                            FileUtils.deleteFolder(ulisseCodiceOperaTrieHomeFolder, true);
                        }
                    });
                }


                //////////////////
                // quota_riparto
                //////////////////

                if ("true".equalsIgnoreCase(configuration
                        .getProperty("ulisse_nosql.quota_riparto.read_only"))) {
                    configuration.setProperty("ulisse_nosql.quota_riparto.page_size", "-1");
                    configuration.setProperty("ulisse_nosql.quota_riparto.read_only", "true");
                    configuration.setProperty("ulisse_nosql.quota_riparto.create_always", "false");
                }
                final OperaNoSqlDb operaNoSqlDb = new OperaNoSqlDb(configuration
                        .getProperties(), "ulisse_nosql.quota_riparto");

                // create quota_riparto nosql
                if (!"true".equalsIgnoreCase(configuration
                        .getProperty("ulisse_nosql.quota_riparto.read_only"))) {

                    final Map<String, Set<String>> opereInconsistenti = new HashMap<>();
                    long lapTimeMillis = System.currentTimeMillis();
                    logger.debug("creating quota_riparto nosql");
                    try (final Statement statement = connection
                            .createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);) {
                        statement.setFetchSize(Integer.parseInt(configuration
                                .getProperty("ulisse_nosql.quota_riparto.fetch_size", "5000")));

                        long count = 0L;
                        final Map<Integer, AtomicLong> schemaStats = new HashMap<>();
                        final HeartBeat heartbeat = HeartBeat.constant("quota_riparto", Integer
                                .parseInt(configuration.getProperty("ulisse_nosql.quota_riparto.heartbeat", "50000")));

                        // prepare sql statement
                        final String sql = configuration
                                .getProperty("ulisse_nosql.quota_riparto.sql");
                        logger.debug("executing query {}", sql);

                        // execute query
                        try (final ResultSet resultSet = statement.executeQuery(sql)) {
                            logger.debug("query executed in {}",
                                    TextUtils.formatDuration(System.currentTimeMillis() - lapTimeMillis));

                            String lastIdSchemaRiparto = null;
                            final List<QuotaRiparto> quote = new ArrayList<>();

                            // loop on result set
                            while (resultSet.next()) {
                                heartbeat.pump();

                                final String idSchemaRiparto = resultSet.getString("id_schema_riparto");
                                final String quotaNumeratore = resultSet.getString("quota_numeratore");
                                final String quotaDenominatore = resultSet.getString("quota_denominatore");
                                final String codiceIpi = resultSet.getString("codice_ipi");
                                final String posizioneSiae = resultSet.getString("posizione_siae");
                                final String tipoQualifica = resultSet.getString("tipo_qualifica");
                                final String tipoQuota = resultSet.getString("tipo_quota");
                                final String quotaPercentuale = resultSet.getString("quota_percentuale");
                                final String cognome = resultSet.getString("cognome");
                                final String codiceConto = resultSet.getString("codice_conto");
                                final String codiceContoEstero = resultSet.getString("codice_conto_estero");
                                final String idRepertorio = resultSet.getString("id_repertorio");
                                final String idTipoIrregolarita = resultSet.getString("id_tipo_irregolarita");

                                if (debug) {
                                    logger.debug("{} {} {}", idSchemaRiparto, tipoQuota, idTipoIrregolarita);
                                }



                                if (Strings.isNullOrEmpty(idSchemaRiparto) ||
                                        Strings.isNullOrEmpty(tipoQuota)) {
                                    continue;
                                }

                                // next royalty scheme
                                if (null != lastIdSchemaRiparto &&
                                        !lastIdSchemaRiparto.equals(idSchemaRiparto) &&
                                        !quote.isEmpty()) {
                                    final String codiceOpera = ulisseCodiceOperaTrie.find(lastIdSchemaRiparto);
                                    if (debug) {
                                        logger.debug("codice {}\n", codiceOpera);
                                    }
                                    if (!Strings.isNullOrEmpty(codiceOpera)) {
                                        final SchemaTerritorio schemaTerritorio = schemaTerritorioNoSqlDb.get(lastIdSchemaRiparto);
                                        if (null != schemaTerritorio) {
                                            final SchemaRiparto schema = new SchemaRiparto(Long
                                                    .parseLong(lastIdSchemaRiparto), schemaTerritorio.territories);
                                            for (QuotaRiparto quota : quote) {
                                                schema.add(quota);
                                            }

                                            Opera opera = operaNoSqlDb.get(codiceOpera);
                                            if (null == opera) {
                                                opera = new Opera(codiceOpera);
                                            } else if (debug) {
                                                logger.debug("opera merged {} {}", opera.codiceOpera, opera.schemi.size());
                                            }
                                            opera.add(schema);
                                            operaNoSqlDb.put(opera);
                                            count++;

                                            // update stats
                                            checkSchemiOpera(opereInconsistenti, opera);
                                            final AtomicLong schemiOpera = schemaStats.get(opera.schemi.size());
                                            if (null == schemiOpera) {
                                                schemaStats.put(opera.schemi.size(), new AtomicLong(1L));
                                            } else {
                                                schemiOpera.incrementAndGet();
                                            }
                                        }
                                    }
                                    quote.clear();
                                }

                                // royalty scheme
                                final float percentuale;
                                //R20-11 (Ex R20-06) add numeratore e denominatore
                                final float numeratore = null == quotaNumeratore ? 0f : Float.parseFloat(quotaNumeratore);
                                final float denominatore = null == quotaDenominatore ? 0f : Float.parseFloat(quotaDenominatore);
                                if ("DEM".equalsIgnoreCase(tipoQuota)) {
                                    percentuale = denominatore > 0f ? 100f * numeratore / denominatore : 0f;
                                } else if ("DRM".equalsIgnoreCase(tipoQuota)) {
                                    percentuale = Float.parseFloat(null == quotaPercentuale ? "0" : quotaPercentuale);
                                } else {
                                    throw new IllegalStateException("invalid tipoQuota " + tipoQuota);
                                }
                                //R20-11(Ex R20-06) add numeratore e denominatore
                                QuotaRiparto q = new QuotaRiparto();
                                q.setCodiceIpi(Strings.isNullOrEmpty(codiceIpi) ?
                                        null : Long.parseLong(codiceIpi));
                                q.setPosizioneSiae(posizioneSiae);
                                q.setTipoQualifica(tipoQualifica);
                                q.setTipoQuota(tipoQuota);
                                q.setPercentuale(percentuale);
                                q.setCognome(cognome);
                                q.setCodiceConto(codiceConto);
                                q.setCodiceContoEstero(codiceContoEstero);
                                q.setIdRepertorio(Strings.isNullOrEmpty(idRepertorio) ?
                                        0L : Long.parseLong(idRepertorio));
                                q.setIdTipoIrregolarita(idTipoIrregolarita);
                                q.setNumeratore(numeratore);
                                q.setDenominatore(denominatore);
                                quote.add(q);

                                if (String.valueOf(idSchemaRiparto).equalsIgnoreCase("364360693")) {
                                    logger.debug("IDSCHEMA , QUOTARIPARTO " + idSchemaRiparto + " , " + q.toString());
                                }
                                lastIdSchemaRiparto = idSchemaRiparto;
                            }

                            // remaining royalties
                            if (!Strings.isNullOrEmpty(lastIdSchemaRiparto) && !quote.isEmpty()) {
                                final String codiceOpera = ulisseCodiceOperaTrie.find(lastIdSchemaRiparto);
                                if (debug) {
                                    logger.debug("codice {}\n", codiceOpera);
                                }
                                if (!Strings.isNullOrEmpty(codiceOpera)) {
                                    final SchemaTerritorio schemaTerritorio = schemaTerritorioNoSqlDb.get(lastIdSchemaRiparto);
                                    if (null != schemaTerritorio) {
                                        final SchemaRiparto schema = new SchemaRiparto(Long
                                                .parseLong(lastIdSchemaRiparto), schemaTerritorio.territories);
                                        for (QuotaRiparto quota : quote) {
                                            schema.add(quota);
                                        }

                                        Opera opera = operaNoSqlDb.get(codiceOpera);
                                        if (null == opera) {
                                            opera = new Opera(codiceOpera);
                                        } else if (debug) {
                                            logger.debug("opera merged {} {}", opera.codiceOpera, opera.schemi.size());
                                        }
                                        opera.add(schema);
                                        operaNoSqlDb.put(opera);
                                        count++;
                                        // update stats
                                        checkSchemiOpera(opereInconsistenti, opera);
                                        final AtomicLong schemiOpera = schemaStats.get(opera.schemi.size());
                                        if (null == schemiOpera) {
                                            schemaStats.put(opera.schemi.size(), new AtomicLong(1L));
                                        } else {
                                            schemiOpera.incrementAndGet();
                                        }
                                    }
                                }
                                quote.clear();
                            }
                        }
                        logger.debug("total parsed rows {}", heartbeat.getTotalPumps());
                        logger.debug("total quota_riparto records {}", count);
                        logger.debug("numero schemi per codice opera {}", schemaStats);

                        // defrag nosql
                        logger.debug("quota_riparto defrag started");
                        operaNoSqlDb.defrag();
                        logger.debug("quota_riparto defrag completed");

                        // output stats
                        try (OutputStream out = new FileOutputStream(new File(configuration
                                .getProperty("ulisse_nosql.inconsistent_works.filename")))) {
                            out.write("CODICE_OPERA,TERRITORI_DUPLICATI\n".getBytes(Charsets.UTF_8));
                            for (Map.Entry<String, Set<String>> entry : opereInconsistenti.entrySet()) {
                                final StringBuilder row = new StringBuilder()
                                        .append(entry.getKey())
                                        .append(",\"");
                                String prefix = "";
                                for (String code : entry.getValue()) {
                                    row.append(prefix);
                                    prefix = " ";
                                    row.append(code);
                                }
                                row.append("\"\n");
                                out.write(row.toString().getBytes(Charsets.UTF_8));
                            }
                        }
                        final JsonObject queryStats = new JsonObject();
                        queryStats.addProperty("sql", sql);
                        queryStats.addProperty("rownum", heartbeat.getTotalPumps());
                        queryStats.addProperty("count", count);
                        queryStats.addProperty("duration", TextUtils
                                .formatDuration(System.currentTimeMillis() - lapTimeMillis));
                        output.add("quota_riparto", queryStats);

                    }

                    logger.debug("quota_riparto nosql created in {}",
                            TextUtils.formatDuration(System.currentTimeMillis() - lapTimeMillis));
                }

                // delete quota_riparto nosql
                if ("true".equalsIgnoreCase(configuration
                        .getProperty("ulisse_nosql.quota_riparto.delete_when_done"))) {
                    postExecTasks.add(new Runnable() {
                        @Override
                        public void run() {
                            logger.debug("deleting quota_riparto nosql {}",
                                    schemaTerritorioNoSqlDb.getHomeFolder());
                            operaNoSqlDb.truncate();
                            FileUtils.deleteFolder(operaNoSqlDb.getHomeFolder(), true);
                        }
                    });
                }

            }

        }

        ///////////
        // upload
        ///////////

        if (steps.contains("upload")) {

            final long lapTimeMillis = System.currentTimeMillis();

            final File archiveFolder = new File(configuration
                    .getProperty("ulisse_nosql.quota_riparto.home_folder"));
            logger.debug("archiveFolder {}", archiveFolder);

            final JsonObject uploadStats = new JsonObject();

            archiveAndUpload("ulisse_nosql", homeFolder, archiveFolder, uploadStats);

            uploadStats.addProperty("duration", TextUtils
                    .formatDuration(System.currentTimeMillis() - lapTimeMillis));

            output.add("upload", uploadStats);

        }

        // run post exec tasks
        for (Runnable task : postExecTasks) {
            task.run();
        }

        // output
        output.addProperty("period", year + month);
        output.addProperty("startTime", DateFormat
                .getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM)
                .format(new Date(startTimeMillis)));
        output.addProperty("totalDuration", TextUtils
                .formatDuration(System.currentTimeMillis() - startTimeMillis));

        logger.debug("message processed in {}",
                TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));
    }

}
