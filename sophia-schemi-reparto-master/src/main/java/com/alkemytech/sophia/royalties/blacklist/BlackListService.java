package com.alkemytech.sophia.royalties.blacklist;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.royalties.Configuration;
import com.alkemytech.sophia.royalties.role.RoleCatalog;
import com.google.inject.Inject;
import com.google.inject.name.Named;

public class BlackListService {
	
	private static final Logger logger = LoggerFactory.getLogger(BlackListService.class);

	private final Charset charset;
	protected final Configuration configuration;
	protected final S3 s3;
	private final RoleCatalog roleCatalog;
	final Map<String, BlackList> blackLists = new HashMap<>();
	private String blackListSiae;
	private String blackListAda;
	
	@Inject
	public BlackListService(
			Configuration configuration,
			@Named("charset") Charset charset,
			S3 s3,
			RoleCatalog roleCatalog
			) {
		super();
		this.configuration = configuration;
		this.charset = charset;
		this.s3 = s3;
		this.roleCatalog = roleCatalog;
	}
	public synchronized void loadBlackList(String blackListSiae, String blackListAda) throws IOException {
		this.blackListSiae = blackListSiae;
		this.blackListAda = blackListAda;
		this.s3.startup();
		blackLists.put("SIAE",new BlackList(charset, roleCatalog, "SIAE").load(s3, blackListSiae));
		blackLists.put("UCMR-ADA",new BlackList(charset, roleCatalog,"UCMR-ADA").load(s3, blackListAda));
	}

	public Map<String, BlackList> getBlackList(){
		return this.blackLists;
	}
	public String getBlackListSiae() {
		return blackListSiae;
	}
	public String getBlackListAda() {
		return blackListAda;
	}

	
}
