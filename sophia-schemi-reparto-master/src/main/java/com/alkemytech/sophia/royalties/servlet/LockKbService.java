package com.alkemytech.sophia.royalties.servlet;

import java.nio.charset.Charset;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.royalties.Configuration;
import com.google.inject.Inject;
import com.google.inject.name.Named;

public class LockKbService {
	
	private static final Logger logger = LoggerFactory.getLogger(LockKbService.class);

	protected final Configuration configuration;
	private boolean lock = false;
	
	@Inject
	public LockKbService(
			Configuration configuration,
			@Named("charset") Charset charset
			) {
		super();
		this.configuration = configuration;
	}

	public boolean isLock() {
		return lock;
	}

	public void lock() {
		this.lock = true;
	}
	public void unLock() {
		this.lock = false;
	}
	
}
