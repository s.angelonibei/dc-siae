package com.alkemytech.sophia.royalties.servlet;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.util.FileUtils;
import com.alkemytech.sophia.royalties.Configuration;
import com.alkemytech.sophia.royalties.LatestVersion;
import com.alkemytech.sophia.royalties.blacklist.BlackListService;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.google.inject.name.Named;


@SuppressWarnings("serial")
public class SettingViewServlet extends HttpServlet {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	protected final Configuration configuration;
	protected final Charset charset;
	protected final S3 s3;
	private final BlackListService blackListService;
	private final Gson gson;
	@Inject
	protected SettingViewServlet(Configuration configuration,
			@Named("charset") Charset charset,
			S3 s3,
			Gson gson,
			BlackListService blackListService,
			LockKbService lockWeb
			) {
		super();
		this.configuration = configuration;
		this.charset = charset;
		this.s3 = s3;
		this.gson = gson;
		this.blackListService = blackListService;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final JsonObject processing = new JsonObject();
		try {
			String year = configuration.getTag("{year}");
			String month = configuration.getTag("{month}");
			if(year != null && month != null) {
				processing.addProperty("year", 	year);	
				processing.addProperty("month", month);
				final JsonObject blackList = new JsonObject();
				blackList.addProperty("SIAE", this.blackListService.getBlackListSiae());
				blackList.addProperty("UCMR-ADA", this.blackListService.getBlackListAda());
				processing.add("black_list", blackList);
			}else {
				processing.addProperty("errore", "kb non settata");
			}
		}catch(Exception ex) {
			logger.info("internal error",ex);
			processing.addProperty("errore", "impossibile aggiornare la kb");
		}
		response.setContentType("application/json");
		response.getWriter().write(gson.toJson(processing));
	}

	
	protected void downloadAndExtractLatest(String propertyPrefix, String propertyPrefixArchive) throws IOException, InterruptedException {
		// data folder
		final File dataFolder = new File(configuration
				.getProperty(propertyPrefix + ".data_folder"));
		dataFolder.mkdirs();

		// .latest file
		final File latestFile = new File(configuration
				.getProperty(propertyPrefixArchive + ".latest_file"));
		logger.debug("downloadAndExtractLatest: latestFile {}", latestFile);
		
		// load local .latest version
		final LatestVersion localLatestVersion = new LatestVersion()
				.load(latestFile);
		logger.debug("downloadAndExtractLatest: localLatestVersion {}", localLatestVersion);

		// .latest url
		final String latestUrl = configuration
				.getProperty(propertyPrefixArchive + ".latest_url");
		logger.debug("downloadAndExtractLatest: latestUrl {}", latestUrl);

		// temporary latest file
		final File temporaryLatestFile = File
				.createTempFile("__tmp__", "__.latest", dataFolder);
		temporaryLatestFile.deleteOnExit();
		logger.debug("downloadAndExtractLatest: temporaryLatestFile {}", temporaryLatestFile);
		
		// download .latest version
		if (!s3.download(new S3.Url(latestUrl), temporaryLatestFile)) {
			temporaryLatestFile.delete();
			throw new IOException("file download error: " + latestUrl);
		}

		// load downloaded .latest version
		final LatestVersion latestVersion = new LatestVersion()
				.load(temporaryLatestFile);
		logger.debug("downloadAndExtractLatest: latestVersion {}", latestVersion);
		
		// check version
		if (localLatestVersion.getVersion("")
				.equals(latestVersion.getVersion())) {
			logger.info("downloadAndExtractLatest: version unchanged");
			return;
		}

		// archive url
		final String archiveUrl = latestVersion.getLocation();
		
		// archive file
		final File archiveFile = File.createTempFile("__tmp__",
				"__" + archiveUrl.substring(1 + archiveUrl.lastIndexOf('/')), dataFolder);
		archiveFile.deleteOnExit();
		logger.debug("downloadAndExtractLatest: archiveFile {}", archiveFile);

		// download archive
		if (!s3.download(new S3.Url(archiveUrl), archiveFile)) {
			throw new IOException("file download error: " + archiveUrl);
		}

		// destination home folder
		final File destinationFolder = new File(configuration
				.getProperty(propertyPrefixArchive + ".home_folder"));

		// delete existing destination folder
		FileUtils.deleteFolder(destinationFolder, true);
		destinationFolder.mkdirs();

		// untar archive file
		final String[] cmdline = new String[] {
				configuration.getProperty(propertyPrefix + ".tar_path",
						configuration.getProperty("default.tar_path", "/usr/bin/tar")),
				"-C", destinationFolder.getAbsolutePath(),
				"-xzf", archiveFile.getAbsolutePath()
		};
		logger.debug("downloadAndExtractLatest: exec cmdline {}", Arrays.asList(cmdline));
		final int exitCode = Runtime
				.getRuntime().exec(cmdline).waitFor();
		logger.debug("downloadAndExtractLatest: exec exitCode {}", exitCode);
		
		// delete archive file
		archiveFile.delete();
		
		// overwrite local latest file
		if (FileUtils.copy(temporaryLatestFile, latestFile) < 0L) {
			throw new IOException("unable to update local latest file: " + latestFile);
		}

		// delete temporary latest file
		temporaryLatestFile.delete();
	}
}
