package com.alkemytech.sophia.royalties.claim;

import java.math.BigDecimal;
import java.util.Map;

import com.alkemytech.sophia.commons.util.BigDecimals;
import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class ClaimResult {

	private static final BigDecimal ONE_HUNDRED = new BigDecimal("100");

	private ClaimInfo tenantInfo;
	private Map<String, ClaimInfo> mandatorsInfo;
	private BigDecimal claimLicensorDem;
	private BigDecimal claimLicensorDrm;
	private BigDecimal claimPdDem;
	private BigDecimal claimPdDrm;
	private BigDecimal claimUnmatched;
	private int claimingSocieties;
	
	public ClaimResult() {
		super();
	}
	
	public ClaimInfo getTenantInfo() {
		return tenantInfo;
	}

	public ClaimInfo getMandatorInfo(String society) {
		return null == mandatorsInfo ?
				null : mandatorsInfo.get(society);
	}

	public Map<String, ClaimInfo> getMandatorsInfo() {
		return mandatorsInfo;
	}

	public boolean hasTenant() {
		return null != tenantInfo;
	}
	
	public boolean hasMandator(String society) {
		return null != mandatorsInfo && mandatorsInfo.containsKey(society);
	}
	
	public String formatTenantWorkCode() {		
		if (null == tenantInfo) {
			return "";
		}
		return tenantInfo.formatWorkCode();
	}
	
	public String formatMandatorWorkCodes() {
		if (null == mandatorsInfo) {
			return "";
		}
		String result = null;
		for (Map.Entry<String, ClaimInfo> entry : mandatorsInfo.entrySet()) {
			result = (null == result ? "" : "|") +
					entry.getKey() + ":" + entry.getValue().formatWorkCode();
		}
		return result;
	}


	public String formatClaimTenantDem() {		
		if (null == tenantInfo) {
			return "";
		}
		return tenantInfo.formatClaimDem();
	}

	public String formatClaimTenantDrm() {		
		if (null == tenantInfo) {
			return "";
		}
		return tenantInfo.formatClaimDrm();
	}

	public String formatClaimTenantDemFraction() {		
		if (null == tenantInfo) {
			return "";
		}
		return tenantInfo.formatClaimDemFraction(claimLicensorDem);
	}
	
	public String formatClaimTenantDrmFraction() {
		if (null == tenantInfo) {
			return "";
		}
		return tenantInfo.formatClaimDrmFraction(claimLicensorDrm);
	}

	public String formatClaimMandatorsDem() {
		if (null == mandatorsInfo || mandatorsInfo.isEmpty()) {
			return "";
		}
		String result = null;
		for (Map.Entry<String, ClaimInfo> entry : mandatorsInfo.entrySet()) {
			result = (null == result ? "" : "|") +
					entry.getKey() + ":" + entry.getValue().formatClaimDem();
		}
		return result;
	}

	public String formatClaimMandatorsDrm() {
		if (null == mandatorsInfo || mandatorsInfo.isEmpty()) {
			return "";
		}
		String result = null;
		for (Map.Entry<String, ClaimInfo> entry : mandatorsInfo.entrySet()) {
			result = (null == result ? "" : "|") +
					entry.getKey() + ":" + entry.getValue().formatClaimDrm();
		}
		return result;
	}
	
	public String formatClaimMandatorsDemFraction() {
		if (null == mandatorsInfo || mandatorsInfo.isEmpty()) {
			return "";
		}
		String result = null;
		for (Map.Entry<String, ClaimInfo> entry : mandatorsInfo.entrySet()) {
			result = (null == result ? "" : "|") +
					entry.getKey() + ":" + entry.getValue()
						.formatClaimDemFraction(claimLicensorDem);
		}
		return result;
	}
	
	public String formatClaimMandatorsDrmFraction() {
		if (null == mandatorsInfo || mandatorsInfo.isEmpty()) {
			return "";
		}
		String result = null;
		for (Map.Entry<String, ClaimInfo> entry : mandatorsInfo.entrySet()) {
			result = (null == result ? "" : "|") +
					entry.getKey() + ":" + entry.getValue()
						.formatClaimDrmFraction(claimLicensorDrm);
		}
		return result;
	}
	
	public BigDecimal getClaimLicensorDem() {
		if (null == claimLicensorDem) {
			return BigDecimal.ZERO;
		}
		return claimLicensorDem.min(ONE_HUNDRED);
	}
	
	public BigDecimal getClaimLicensorDrm() {
		if (null == claimLicensorDrm) {
			return BigDecimal.ZERO;
		}
		return claimLicensorDrm.min(ONE_HUNDRED);
	}
	
	public BigDecimal getClaimPdDem() {
		if (null == claimPdDem) {
			return BigDecimal.ZERO;
		}
		return claimPdDem.min(ONE_HUNDRED);
	}
	
	public BigDecimal getClaimPdDrm() {
		if (null == claimPdDrm) {
			return BigDecimal.ZERO;
		}
		return claimPdDrm.min(ONE_HUNDRED);
	}

	public BigDecimal getClaimUnmatched() {
		if (null == claimUnmatched) {
			return BigDecimal.ZERO;
		}
		return claimUnmatched.min(ONE_HUNDRED);
	}


	public boolean isClaimZero() {
		return (null == claimLicensorDem || BigDecimals.isAlmostZero(claimLicensorDem)) &&
				(null == claimLicensorDrm || BigDecimals.isAlmostZero(claimLicensorDrm));
	}

	public boolean isClaimTooMuch() {
		if (null != claimLicensorDem && BigDecimals.isReallyGreaterThan(claimLicensorDem, ONE_HUNDRED)) {
			return true;
		} else if (null != claimLicensorDrm && BigDecimals.isReallyGreaterThan(claimLicensorDrm, ONE_HUNDRED)) {
			return true;
		} else if (null != claimPdDem && BigDecimals.isReallyGreaterThan(claimPdDem, ONE_HUNDRED)) {
			return true;
		} else if (null != claimPdDrm && BigDecimals.isReallyGreaterThan(claimPdDrm, ONE_HUNDRED)) {
			return true;
		}
		return false;
	}

	public int getClaimingSocieties() {
		return claimingSocieties;
	}

	public ClaimResult setTenantInfo(ClaimInfo tenantInfo) {
		this.tenantInfo = tenantInfo;
		return this;
	}

	public ClaimResult setMandatorsInfo(Map<String, ClaimInfo> mandatorsInfo) {
		this.mandatorsInfo = mandatorsInfo;
		return this;
	}

	public ClaimResult setClaimLicensorDem(BigDecimal claimLicensorDem) {
		this.claimLicensorDem = claimLicensorDem;
		return this;
	}

	public ClaimResult setClaimLicensorDrm(BigDecimal claimLicensorDrm) {
		this.claimLicensorDrm = claimLicensorDrm;
		return this;
	}

	public ClaimResult setClaimPdDem(BigDecimal claimPdDem) {
		this.claimPdDem = claimPdDem;
		return this;
	}

	public ClaimResult setClaimPdDrm(BigDecimal claimPdDrm) {
		this.claimPdDrm = claimPdDrm;
		return this;
	}

	public ClaimResult setClaimUnmatched(BigDecimal claimUnmatched) {
		this.claimUnmatched = claimUnmatched;
		return this;
	}

	public ClaimResult setClaimingSocieties(int claimingSocieties) {
		this.claimingSocieties = claimingSocieties;
		return this;
	}
	
	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}
	
}
