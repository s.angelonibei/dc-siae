package com.alkemytech.sophia.royalties;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.io.CompressionAwareFileInputStream;
import com.alkemytech.sophia.commons.sqs.McmdbMessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.MessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.SqsMessageHelper;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.alkemytech.sophia.commons.util.FileUtils;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.HeartBeat;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.royalties.jdbc.McmdbDataSource;
import com.alkemytech.sophia.royalties.nosql.UuidCodici;
import com.alkemytech.sophia.royalties.nosql.UuidCodiciNoSqlDb;
import com.google.common.base.Strings;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Scopes;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.*;
import java.net.ServerSocket;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class CodiciNoSql extends MicroService {

    private static final Logger logger = LoggerFactory.getLogger(CodiciNoSql.class);

    protected static class GuiceModuleExtension extends GuiceModule {

        public GuiceModuleExtension(String[] args, String resourceName) {
            super(args, resourceName);
        }

        @Override
        protected void configure() {
            super.configure();
            // amazon service(s)
            bind(S3.class)
                    .in(Scopes.SINGLETON);
            bind(SQS.class)
                    .in(Scopes.SINGLETON);
            // data source(s)
            bind(DataSource.class)
                    .annotatedWith(Names.named("MCMDB"))
                    .to(McmdbDataSource.class)
                    .asEagerSingleton();
            // message deduplicator(s)
            bind(MessageDeduplicator.class)
                    .to(McmdbMessageDeduplicator.class)
                    .in(Scopes.SINGLETON);
            // other binding(s)
            bind(Configuration.class)
                    .asEagerSingleton();
            bind(CodiciNoSql.class)
                    .asEagerSingleton();
        }

    }

    public static void main(String[] args) {
        try {
            final CodiciNoSql instance = Guice
                    .createInjector(new GuiceModuleExtension(args,
                            "/codici-nosql.properties"))
                    .getInstance(CodiciNoSql.class)
                    .startup();
            try {
                instance.process(args);
            } finally {
                instance.shutdown();
            }
        } catch (Throwable e) {
            logger.error("main", e);
        } finally {
            System.exit(0);
        }
    }

    private final SQS sqs;
    private final MessageDeduplicator deduplicator;

    @Inject
    protected CodiciNoSql(Configuration configuration,
                          @Named("charset") Charset charset,
                          S3 s3, SQS sqs,
                          MessageDeduplicator deduplicator) {
        super(configuration, charset, s3);
        this.sqs = sqs;
        this.deduplicator = deduplicator;
    }

    public CodiciNoSql startup() throws IOException {
        if ("true".equalsIgnoreCase(configuration.getProperty("codici_nosql.locked", "true"))) {
            throw new IllegalStateException("application locked");
        }
        super.startup();
        sqs.startup();
        return this;
    }

    public CodiciNoSql shutdown() throws IOException {
        sqs.shutdown();
        super.shutdown();
        return this;
    }

    public void process(String[] args) throws Exception {
        final int bindPort = Integer.parseInt(configuration.getProperty("codici_nosql.bind_port",
                configuration.getProperty("default.bind_port", "0")));
        // bind lock tcp port
        try (ServerSocket socket = new ServerSocket(bindPort)) {
            logger.debug("socket bound to {}", socket.getLocalSocketAddress());

            // standalone mode
            if (Arrays.asList(args).contains("standalone") ||
                    "true".equalsIgnoreCase(configuration
                            .getProperty("codici_nosql.standalone", "false"))) {
                final JsonObject input = GsonUtils.fromJson(configuration
                        .getProperty("codici_nosql.standalone.message_body"), JsonObject.class);
                final JsonObject output = new JsonObject();
                logger.debug("standalone input {}", new GsonBuilder()
                        .setPrettyPrinting().create().toJson(input));
                processMessage(input, output);
                logger.debug("standalone output {}", new GsonBuilder()
                        .setPrettyPrinting().create().toJson(output));
                return;
            }

            // sqs message pump
            final SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration
                    .getProperties(), "codici_nosql.sqs");
//			final MessageDeduplicator deduplicator = new TrieMessageDeduplicator(new File(configuration
//					.getProperty("codici_nosql.sqs.deduplicator_folder")));
            sqsMessagePump.pollingLoop(deduplicator, new SqsMessagePump.Consumer() {

                private JsonObject output;
                private JsonObject error;

                @Override
                public JsonObject getStartedMessagePayload(JsonObject message) {
                    output = null;
                    error = null;
                    return SqsMessageHelper.formatContext();
                }

                @Override
                public boolean consumeMessage(JsonObject message) {
                    try {
                        output = new JsonObject();
                        processMessage(GsonUtils
                                .getAsJsonObject(message, "body"), output);
                        error = null;
                        return true;
                    } catch (Exception e) {
                        output = null;
                        error = SqsMessageHelper.formatError(e);
                        return false;
                    }
                }

                @Override
                public JsonObject getCompletedMessagePayload(JsonObject message) {
                    return output;
                }

                @Override
                public JsonObject getFailedMessagePayload(JsonObject message) {
                    return error;
                }

            });
        }
    }

    private void processMessage(JsonObject input, JsonObject output) throws Exception {

//		{
//		  "body": {
//		    "year": "2018",
//		    "month": "11",
//		    "force": true
//		  },
//		  "header": {
//		    "queue": "dev_to_process_codici_dump_v2",
//		    "timestamp": "2018-05-10T00:00:00.000000+02:00",
//		    "uuid": "7f36a2bb-0637-4d4d-9990-4ed9644adefc",
//		    "sender": "aws-console"
//		  }
//		}

        logger.debug("dumping uuid codici");
        final long startTimeMillis = System.currentTimeMillis();
        final String datetime = new SimpleDateFormat(configuration
                .getProperty("codici_nosql.datetime_format", configuration
                        .getProperty("default.datetime_format", "yyyyMMddHHmmss")))
                .format(new Date());
        logger.debug("datetime {}", datetime);

        // parse input json message
        final String jsonUrl = GsonUtils.getAsString(input, "jsonUrl");
//		String year = GsonUtils.getAsString(input, "year");
//		String month = GsonUtils.getAsString(input, "month");
        if (/*Strings.isNullOrEmpty(year) ||
				Strings.isNullOrEmpty(month) ||*/
                Strings.isNullOrEmpty(jsonUrl)) {
            throw new IllegalArgumentException("invalid input message");
        }
        String day = GsonUtils.getAsString(input, "day", configuration
                .getProperty("codici_nosql.reference_day", "01"));
        final JsonArray stepsJson = GsonUtils.getAsJsonArray(input, "steps");
        final Set<String> steps = new HashSet<>();
        if (null != stepsJson) {
            for (JsonElement stepJson : stepsJson)
                steps.add(stepJson.getAsString());
        } else {
            steps.addAll(Arrays.asList("create", "upload"));
        }
//		final String period = fixPeriod(month);
////		year = String.format("%04d", Integer.parseInt(year));
////		month = fixMonth(month);
////		day = String.format("%02d", Integer.parseInt(day));
////		logger.debug("year {}", year);
////		logger.debug("month {}", month);
////		logger.debug("day {}", day);
////		logger.debug("period {}", period);
////		logger.debug("steps {}", steps);
////
////		// configuration tags
        configuration.setTag("{datetime}", datetime);
////		configuration.setTag("{year}", year);
////		configuration.setTag("{month}", month);
////		configuration.setTag("{day}", day);
////		configuration.setTag("{period}", period);

        // configuration
//		final boolean debug = "true".equalsIgnoreCase(configuration
//				.getProperty("codici_nosql.debug", configuration.getProperty("default.debug")));
        final File homeFolder = new File(configuration
                .getProperty("default.home_folder"));

        // post execution tasks
        final List<Runnable> postExecTasks = new ArrayList<>();

        ///////////
        // create
        ///////////

        if (steps.contains("create")) {

            ////////////////
            // uuid_lookup
            ////////////////

            if ("true".equalsIgnoreCase(configuration
                    .getProperty("codici_nosql.uuid_codici.read_only"))) {
                configuration.setProperty("codici_nosql.uuid_codici.page_size", "-1");
                configuration.setProperty("codici_nosql.uuid_codici.read_only", "true");
                configuration.setProperty("codici_nosql.uuid_codici.create_always", "false");
            }
            final UuidCodiciNoSqlDb codiciNoSqlDb = new UuidCodiciNoSqlDb(configuration
                    .getProperties(), "codici_nosql.uuid_codici");

            // create uuid_codici nosql
            if (!"true".equalsIgnoreCase(configuration
                    .getProperty("codici_nosql.uuid_codici.read_only"))) {

                final long lapTimeMillis = System.currentTimeMillis();
                logger.debug("creating uuid_codici nosql");

                final String uuidName = configuration
                        .getProperty("codici_nosql.json.property.uuid", "uuid");
                final String codiciName = configuration
                        .getProperty("codici_nosql.json.property.codici", "codiciOpera");
                final String societaName = configuration
                        .getProperty("codici_nosql.json.property.societa", "societa");
                final String codiceName = configuration
                        .getProperty("codici_nosql.json.property.codice", "codice");
                logger.debug("uuidName {}", uuidName);
                logger.debug("codiciName {}", codiciName);
                logger.debug("societaName {}", societaName);
                logger.debug("codiceName {}", codiceName);

                final boolean insertAlways = "true".equalsIgnoreCase(configuration
                        .getProperty("codici_nosql.insert_always"));
                logger.debug("insertAlways {}", insertAlways);

                // stats
                long singleSocietyCodes = 0L;
                long multipleSocietyCodes = 0L;

                // download input file from s3
                logger.debug("jsonUrl {}", jsonUrl);
                final String jsonCompression = configuration.getProperty("codici_nosql.json.compression");
                logger.debug("jsonCompression {}", jsonCompression);

                // local input file
                final File localInputFile = File.createTempFile("__tmp__",
                        "__" + jsonUrl.substring(1 + jsonUrl.lastIndexOf('/')), homeFolder);
                localInputFile.deleteOnExit();
                if (!s3.download(new S3.Url(jsonUrl), localInputFile)) {
                    throw new IOException("file download error: " + jsonUrl);
                }

                try (final CompressionAwareFileInputStream in = new CompressionAwareFileInputStream(localInputFile, jsonCompression);
                     final Reader reader = new InputStreamReader(in, charset);
                     final BufferedReader bufferedReader = new BufferedReader(reader, 65536)) {

                    final HeartBeat heartbeat = HeartBeat.constant("uuid_codici", Integer
                            .parseInt(configuration.getProperty("codici_nosql.uuid_codici.heartbeat", "10000")));

                    logger.debug("parsing json file {}", localInputFile);

                    for (String line; null != (line = bufferedReader.readLine()); ) {
                        final JsonObject recordJson = GsonUtils
                                .fromJson(line, JsonObject.class);

//						{
//							"uuid":"bd5d24c5-a46e-493e-b240-06f101e9e75e",
//							"codiciOpera":[
//								{
//									"societa": "UCMR-ADA",
//									"codice": "1150016035.46115"
//								},
//							...
//							]
//						}

                        // parse record
                        final Map<String, String> codici = new HashMap<>();
                        final String uuid = GsonUtils.getAsString(recordJson, uuidName);
                        final JsonArray codiciJson = GsonUtils.getAsJsonArray(recordJson, codiciName);
                        for (JsonElement elementJson : codiciJson) {
                            final JsonObject codiceJson = elementJson.getAsJsonObject();
                            final String codice = GsonUtils.getAsString(codiceJson, codiceName);
                            final String societa = GsonUtils.getAsString(codiceJson, societaName);
                            codici.put(societa, codice);
                        }

                        // insert into nosql database
                        if (!Strings.isNullOrEmpty(uuid) && !codici.isEmpty()) {
                            UuidCodici uuidCodici;
                            if (insertAlways) {
                                uuidCodici = new UuidCodici(uuid);
                                uuidCodici.addAll(codici);
                            } else {
                                uuidCodici = codiciNoSqlDb.get(uuid);
                                if (null == uuidCodici) {
                                    uuidCodici = new UuidCodici(uuid);
                                }
                                uuidCodici.addAll(codici);
                            }
                            codiciNoSqlDb.put(uuidCodici);

                            heartbeat.pump();
                            if (codici.size() > 1) {
                                multipleSocietyCodes++;
                            } else {
                                singleSocietyCodes++;
                            }
                        }

                    }

                    logger.debug("total parsed rows {}", heartbeat.getTotalPumps());

                    // output stats
                    final JsonObject createStats = new JsonObject();
                    createStats.addProperty("inputFileSize", localInputFile.length());
                    createStats.addProperty("rownum", heartbeat.getTotalPumps());
                    createStats.addProperty("singleSocietyCodes", singleSocietyCodes);
                    createStats.addProperty("multipleSocietyCodes", multipleSocietyCodes);
                    createStats.addProperty("duration", TextUtils
                            .formatDuration(System.currentTimeMillis() - lapTimeMillis));
                    output.add("uuid_codici", createStats);
                }

                logger.debug("uuid_codici nosql created in {}",
                        TextUtils.formatDuration(System.currentTimeMillis() - lapTimeMillis));
            }

            // delete uuid_codici nosql
            if ("true".equalsIgnoreCase(configuration
                    .getProperty("codici_nosql.uuid_codici.delete_when_done"))) {
                postExecTasks.add(new Runnable() {
                    @Override
                    public void run() {
                        logger.debug("deleting uuid_codici nosql {}", codiciNoSqlDb.getHomeFolder());
                        codiciNoSqlDb.truncate();
                        FileUtils.deleteFolder(codiciNoSqlDb.getHomeFolder(), true);
                    }
                });
            }

        }

        ///////////
        // upload
        ///////////

        if (steps.contains("upload")) {

            final long lapTimeMillis = System.currentTimeMillis();

            final File archiveFolder = new File(configuration
                    .getProperty("codici_nosql.uuid_codici.home_folder"));
            logger.debug("archiveFolder {}", archiveFolder);

            final JsonObject uploadStats = new JsonObject();

            archiveAndUpload("codici_nosql", homeFolder, archiveFolder, uploadStats);

            uploadStats.addProperty("duration", TextUtils
                    .formatDuration(System.currentTimeMillis() - lapTimeMillis));

            output.add("upload", uploadStats);

        }

        // run post exec tasks
        for (Runnable task : postExecTasks) {
            task.run();
        }

        // output
		// output.addProperty("period", year + month);
        output.addProperty("startTime", DateFormat
                .getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM)
                .format(new Date(startTimeMillis)));
        output.addProperty("totalDuration", TextUtils
                .formatDuration(System.currentTimeMillis() - startTimeMillis));

        logger.debug("message processed in {}",
                TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));
    }

}
