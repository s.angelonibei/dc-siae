package com.alkemytech.sophia.royalties.nosql;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Territories {
	
	private int[] array;
	private String string;

	public Territories() {
		super();
		this.array = new int[0];
	}
	
	public Territories(Territories territories) {
		super();
		final int length = territories.array.length;
		this.array = new int[length];
		System.arraycopy(territories.array, 0, this.array, 0, length);
	}

	public Territories(String string) {
		this(string, false);
	}

	public Territories(String string, boolean fixInputString) {
		super();
		if (fixInputString) {
			int[] array = toIntArray(string);
			final Set<Integer> unique = new HashSet<>();
			for (int i = 0; i < array.length; i++) {
				unique.add(array[i]);
			}
			array = new int[unique.size()];
			int index = 0;
			for (Integer value : unique) {
				array[index ++] = value;
			}
			Arrays.sort(array);
			this.array = array;
		} else {
			this.array = toIntArray(string);
			this.string = string;
		}
	}

	private Territories(int[] array) {
		super();
		this.array = array;
	}
	
	public boolean isEmpty() {
		return 0 == array.length;
	}
	
	public boolean contains(String territory) {
		return Arrays.binarySearch(array, toInt(territory)) >= 0;
	}
	
	public Territories intersection(Territories other) {
		if (0 == array.length) {
			return new Territories();
		}
		if (0 == other.array.length) {
			return new Territories();
		}
		final int[] resultArray = new int[Math.min(array.length, other.array.length)];
		final int length = intersection(array, other.array, resultArray);
		if (resultArray.length == length) {
			return new Territories(resultArray);
		}
		final int[] shrinkedArray = new int[length];
		System.arraycopy(resultArray, 0, shrinkedArray, 0, length);
		return new Territories(shrinkedArray);
	}
	
	private static int intersection(int[] leftArray, int[] rightArray, int[] resultArray) {
		int length = 0;
		if (leftArray.length > rightArray.length) {
			final int[] swapArray = rightArray;
			rightArray = leftArray;
			leftArray = swapArray;
		}
		int leftIndex = 0, rightIndex = 0;
		while (leftIndex < leftArray.length && rightIndex < rightArray.length) {
			final int leftValue = leftArray[leftIndex];
			final int rightValue = rightArray[rightIndex];
			if (leftValue < rightValue) {
				leftIndex ++;
			} else if (leftValue > rightValue) {
				rightIndex ++;
			} else {
				resultArray[length] = leftValue;
				length ++;
				leftIndex ++;
				rightIndex ++;
			}
		}
		return length;
	}
	
	public boolean add(String territory) {
		final int value = toInt(territory);
		int index = Arrays.binarySearch(array, value);
		if (index >= 0) {
			return false;
		}
		index = (-index) - 1;
		final int[] expandedArray = new int[1 + array.length];
		if (index > 0)
			System.arraycopy(array, 0, expandedArray, 0, index);
		expandedArray[index] = value;
		if (index < array.length) {
			System.arraycopy(array, index, expandedArray, index + 1, array.length - index);
		}
		array = expandedArray;
		string = null;
		return true;
	}
	
	private static int twoCharsToInt(String string, int index) {
		return (string.charAt(index+1) & 0xffff)
				+ ((string.charAt(index) & 0xffff) << 16);
	}
	
	private static int toInt(String string) {
		if (null == string) {
			throw new IllegalArgumentException("string argument is null");
		}
		final int length = string.length();
		if (2 != length) {
			throw new IllegalArgumentException("invalid string length: " + length);
		}
		return twoCharsToInt(string, 0);
	}
	
	private static int[] toIntArray(String string) {
		if (null == string) {
			return new int[0];
		}
		final int length = string.length();
		if (0 != (length % 2)) {
			throw new IllegalArgumentException("non even string length: " + length);
		}
		final int[] result = new int[length / 2];
		for (int i = 0, j = 0; i < length; i += 2, j ++) {
			result[j] = twoCharsToInt(string, i);
		}
		return result;
	}
	
	private static String toString(int[] array, int fromIndex, int toIndex) {
		final char[] chars = new char[(toIndex - fromIndex) * 2];
		for (int i = 0, j = 0; fromIndex < toIndex; fromIndex ++, i ++, j += 2) {
			chars[j] = (char) ((array[i] >> 16) & 0xffff);
			chars[j+1] = (char) (array[i] & 0xffff);
		}
		return new String(chars);
	}
	
	@Override
	public String toString() {
		if (null == string) {
			string = toString(array, 0, array.length);
		}
		return string;
	}
	
//	public static void main(String[] args) {		
//		Territories ts = Territories.getInstance();
//		ts.add("CC");
//		ts.add("BB");
//		ts.add("AA");
//		ts.add("CC");
//		ts.add("BB");
//		ts.add("CC");
//		ts.add("XS");
//		ts.add("SX");
//		ts.add("AB");
//		ts.add("AA");
//		System.out.println(ts);
//		Territories ts2 = new Territories("CCXY");
//		System.out.println(ts.intersection(ts2));
//	}

}
