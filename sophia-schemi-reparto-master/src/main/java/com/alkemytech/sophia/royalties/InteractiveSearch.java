package com.alkemytech.sophia.royalties;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.alkemy.siae.sophia.pricing.DsrLine;
import com.alkemy.siae.sophia.pricing.DsrMetadata;
import com.alkemytech.sophia.royalties.claim.ClaimHelper;
import com.alkemytech.sophia.royalties.claim.SocietyInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.nosql.NoSql.Selector;
import com.alkemytech.sophia.commons.nosql.NoSqlException;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.HeartBeat;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.royalties.blacklist.BlackList;
import com.alkemytech.sophia.royalties.nosql.IpiSocNoSqlDb;
import com.alkemytech.sophia.royalties.nosql.OperaNoSqlDb;
import com.alkemytech.sophia.royalties.nosql.UuidCodici;
import com.alkemytech.sophia.royalties.nosql.UuidCodiciNoSqlDb;
import com.alkemytech.sophia.royalties.role.RoleCatalog;
import com.alkemytech.sophia.royalties.scheme.IrregularityCatalog;
import com.alkemytech.sophia.royalties.scheme.Royalty;
import com.alkemytech.sophia.royalties.scheme.RoyaltyScheme;
import com.alkemytech.sophia.royalties.scheme.RoyaltySchemeService;
import com.alkemytech.sophia.royalties.scheme.TemporaryWorkCatalog;
import com.google.common.base.Strings;
import com.google.gson.GsonBuilder;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Scopes;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class InteractiveSearch extends MicroService {
	
	private static final Logger logger = LoggerFactory.getLogger(InteractiveSearch.class);

	protected static class GuiceModuleExtension extends GuiceModule {

		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// amazon service(s)
			bind(S3.class)
				.in(Scopes.SINGLETON);
			// other(s)
			bind(RoleCatalog.class)
				.asEagerSingleton();
			bind(TemporaryWorkCatalog.class)
				.asEagerSingleton();
			bind(IrregularityCatalog.class)
				.asEagerSingleton();
			bind(Configuration.class)
				.asEagerSingleton();
			bind(RoyaltySchemeService.class)
				.asEagerSingleton();
			bind(InteractiveSearch.class)
				.asEagerSingleton();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			final InteractiveSearch instance = Guice
					.createInjector(new GuiceModuleExtension(args,
							"/interactive-search.properties"))
					.getInstance(InteractiveSearch.class)
					.startup();
			try {
				instance.process(args);
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}
	
	private final RoleCatalog roleCatalog;
	private final TemporaryWorkCatalog temporaryWorkCatalog;
	private final IrregularityCatalog irregularityCatalog;
	private final RoyaltySchemeService royaltySchemeService;

	@Inject
	protected InteractiveSearch(Configuration configuration,
			@Named("charset") Charset charset,
			S3 s3, RoleCatalog roleCatalog,
			TemporaryWorkCatalog temporaryWorkCatalog,
			IrregularityCatalog irregularityCatalog,
			RoyaltySchemeService royaltySchemeService) {
		super(configuration, charset, s3);
		this.roleCatalog = roleCatalog;
		this.temporaryWorkCatalog = temporaryWorkCatalog;
		this.irregularityCatalog = irregularityCatalog;
		this.royaltySchemeService = royaltySchemeService;
	}
	
	public InteractiveSearch startup() throws IOException {
		super.startup();
		return this;
	}

	public InteractiveSearch shutdown() throws IOException {
		super.shutdown();
		return this;
	}
	
	public InteractiveSearch process(String[] args) throws Exception {
		
		// configuration
		final String year = configuration.getProperty("interactive_search.year", "2019");
		final String month = configuration.getProperty("interactive_search.month", "01");
		final String dspCode = configuration.getProperty("interactive_search.dsp_code", "YOU");
				
		// configuration tags
		configuration.setTag("{year}", year);
		configuration.setTag("{month}", month);
		
		// initialize role catalog
		roleCatalog.load(s3, configuration.getProperty("role_catalog.csv_url"));	
		
		// initialize temporary work catalog
		temporaryWorkCatalog.load(s3, configuration.getProperty("temporary_work_catalog.csv_url"));	

		// initialize irregularity catalog
		irregularityCatalog.load(s3, configuration.getProperty("irregularity_catalog.csv_url"));	

		// initialize ipi nosql to latest version
		downloadAndExtractLatest("interactive_search", "interactive_search.ipi_nosql");
		final IpiSocNoSqlDb ipiNoSqlDb = new IpiSocNoSqlDb(configuration
				.getProperties(), "interactive_search.ipi_nosql");

		// initialize siae nosql to latest version
		downloadAndExtractLatest("interactive_search", "interactive_search.siae_nosql");
		final OperaNoSqlDb siaeNoSqlDb = new OperaNoSqlDb(configuration
				.getProperties(), "interactive_search.siae_nosql");

		// initialize ucmr_ada nosql to latest version
		downloadAndExtractLatest("interactive_search", "interactive_search.ucmr_ada_nosql");
		final OperaNoSqlDb ucmrAdaNoSqlDb = new OperaNoSqlDb(configuration
				.getProperties(), "interactive_search.ucmr_ada_nosql");
		
		// initialize codici nosql to latest version
		downloadAndExtractLatest("interactive_search", "interactive_search.codici_nosql");
		final UuidCodiciNoSqlDb codiciNoSqlDb = new UuidCodiciNoSqlDb(configuration
				.getProperties(), "interactive_search.codici_nosql");

		// initialize black list(s)
		final Map<String, String> blackListUrls = GsonUtils
				.decodeJsonMap(configuration.getProperty("black_list.csv_urls"));
		final Map<String, BlackList> blackLists = new HashMap<>();
		for (Map.Entry<String, String> entry : blackListUrls.entrySet()) {
			blackLists.put(entry.getKey(),
					new BlackList(charset, roleCatalog, entry.getKey())
							.load(s3, entry.getValue()));
		}
		
		// society territory
		final Map<String, String> societyTerritoryMap = GsonUtils
				.decodeJsonMap(configuration.getProperty("interactive_search.society_territory", "{}"));
		logger.debug("societyTerritoryMap {}", societyTerritoryMap);



		
		System.out.println();
		System.out.println("type <societa> <codice opera> <territorio> to search royalty schemes");
		System.out.println("type \"uuid\" <uuid opera> <territorio> to search royalty schemes");
		System.out.println("type \"exit\" to quit");
		try (final BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in))) {
			for (String line = stdin.readLine(); !"exit".equalsIgnoreCase(line); line = stdin.readLine()) {
				if (null == line || "".equals(line)) {
					continue;
				}
				System.out.println("input text: \"" + line + "\"");

				final String[] terms = line.split("\\s");
				System.out.println("parsed input: " + Arrays.asList(terms));

				if ("search".equalsIgnoreCase(terms[0])) {
					
					final HeartBeat heartbeat = HeartBeat.constant("codici", 10000);
					final Set<String> uuids = Collections.newSetFromMap(new HashMap<String, Boolean>());
					long startTime = System.currentTimeMillis();
					codiciNoSqlDb.select(new Selector<UuidCodici>() {
						@Override
						public void select(UuidCodici value) throws NoSqlException {
							uuids.add(value.uuid);
							heartbeat.pump();
						}
					});
					System.out.println("total codici " + heartbeat.getTotalPumps());
					System.out.println("elapsed " + TextUtils.formatDuration(System.currentTimeMillis() - startTime));
					System.out.println();
					
					final HeartBeat heartbeat2 = HeartBeat.percentage("uuid", uuids.size(), 100);
					startTime = System.currentTimeMillis();					
					for (String uuid : uuids) {
						
						UuidCodici codici = codiciNoSqlDb.get(uuid);
						if (null == codici) {
							System.err.println("uuid not found " + uuid);
						}
						heartbeat2.pump();
					}
					System.out.println("total uuids " + heartbeat.getTotalPumps());
					System.out.println("elapsed " + TextUtils.formatDuration(System.currentTimeMillis() - startTime));
					System.out.println();

					continue;					
				}
				
				if (3 != terms.length ||
						Strings.isNullOrEmpty(terms[0]) ||
						Strings.isNullOrEmpty(terms[1]) ||
						Strings.isNullOrEmpty(terms[2])) {
					continue;
				}
				
				if ("uuid".equalsIgnoreCase(terms[0])) {

					final String uuid = terms[1];
					final String territorio = terms[2];
					final UuidCodici codici = codiciNoSqlDb.get(uuid);


					//adding test claim
					// claim helper
					Map<String, OperaNoSqlDb> operaNoSqlDbs = new HashMap<>();
					operaNoSqlDbs.put("SIAE",siaeNoSqlDb);
//					operaNoSqlDbs.put("UCMR-ADA",ucmrAdaNoSqlDb);
					final ClaimHelper claimHelper = new ClaimHelper(configuration,
							ipiNoSqlDb, codiciNoSqlDb, operaNoSqlDbs, royaltySchemeService);
					DsrLine dsrLine = new DsrLine(null);
					//
					DsrMetadata dsrMetadata = new DsrMetadata();
					dsrMetadata.setDspCode(dspCode);
					dsrMetadata.setYear(year);
					dsrMetadata.setMonth(month);
					SocietyInfo siaeInfo = new SocietyInfo();
					siaeInfo.setSociety("SIAE");
					siaeInfo.setTerritory("IT");
					siaeInfo.setBlackList(blackLists.get(siaeInfo.getSociety()));
					dsrMetadata.setTenant(siaeInfo);
//					dsrMetadata.addMandator();

					dsrLine.setUuidSophia(uuid);
					dsrLine.setTerritory(territorio);

					claimHelper.claim(dsrMetadata,dsrLine);



					System.out.println("codici " + codici);
					if (null != codici) {
						for (Map.Entry<String, String> entry : codici.codici.entrySet()) {
							final String societa = entry.getKey();
							final String codiceOpera = entry.getValue();
							System.out.println("--------------------------------------------------------------------------------");
							System.out.println("societa: " + societa);
							System.out.println("codiceOpera: " + codiceOpera);
							
							RoyaltyScheme royaltyScheme = null;
							if ("SIAE".equalsIgnoreCase(societa)) {
								royaltyScheme = royaltySchemeService
										.lookup(ipiNoSqlDb, siaeNoSqlDb, societa, codiceOpera, territorio);
							} else if ("UCMR-ADA".equalsIgnoreCase(societa)) {
								royaltyScheme = royaltySchemeService
										.lookup(ipiNoSqlDb, ucmrAdaNoSqlDb, societa, codiceOpera, territorio);
							}
							
							if (null == royaltyScheme) {
								System.out.println("royalty scheme not found");
							} else {
								System.out.println(new GsonBuilder()
										.setPrettyPrinting().create().toJson(royaltyScheme));					
								if (territorio.equalsIgnoreCase(societyTerritoryMap.get(societa))) {											
									final List<Royalty> royalties = blackLists
											.get(societa).filter(royaltyScheme.royalties, dspCode, year + month);
									System.out.println("black list filtered royalties (ready for claim)");
									System.out.println(new GsonBuilder()
											.setPrettyPrinting().create().toJson(royalties));			
								} else {
									final List<Royalty> royalties = new ArrayList<>(royaltyScheme.royalties.size());
									for (Royalty royalty : royaltyScheme.royalties) {
										if (!royalty.doNotClaim && !royalty.sospesoPerEditore) {
											royalties.add(royalty);
										}
									}
									System.out.println("royalties ready for claim");
									System.out.println(new GsonBuilder()
											.setPrettyPrinting().create().toJson(royalties));			
								}
							}
						}
					}
					continue;
				}
				
				
				final String societa = terms[0];
				final String codiceOpera = terms[1];
				final String territorio = terms[2];
				
				RoyaltyScheme royaltyScheme = null;
				if ("SIAE".equalsIgnoreCase(societa)) {
					royaltyScheme = royaltySchemeService
							.lookup(ipiNoSqlDb, siaeNoSqlDb, societa, codiceOpera, territorio);
				} else if ("UCMR-ADA".equalsIgnoreCase(societa)) {
					royaltyScheme = royaltySchemeService
							.lookup(ipiNoSqlDb, ucmrAdaNoSqlDb, societa, codiceOpera, territorio);
				}
				
				if (null == royaltyScheme) {
					System.out.println("royalty scheme not found");
				} else {
					System.out.println(new GsonBuilder()
							.setPrettyPrinting().create().toJson(royaltyScheme));					
					if (territorio.equalsIgnoreCase(societyTerritoryMap.get(societa))) {											
						final List<Royalty> royalties = blackLists
								.get(societa).filter(royaltyScheme.royalties, dspCode, year + month);
						System.out.println("black list filtered royalties");
						System.out.println(new GsonBuilder()
								.setPrettyPrinting().create().toJson(royalties));			
					}
				}
				
				System.out.println();				
				System.out.println();				
			}
		}
		
		return this;
	}
	
}