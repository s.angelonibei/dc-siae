package com.alkemytech.sophia.royalties.nosql;

import com.alkemytech.sophia.commons.mmap.ObjectCodec;
import com.alkemytech.sophia.commons.nosql.ConcurrentNoSql;
import com.alkemytech.sophia.commons.nosql.NoSql;
import com.alkemytech.sophia.commons.nosql.NoSql.Selector;
import com.alkemytech.sophia.commons.nosql.NoSqlConfig;
import com.alkemytech.sophia.commons.trie.ConcurrentTrieEx;
import com.alkemytech.sophia.commons.util.FileUtils;
import com.alkemytech.sophia.commons.util.TextUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class OperaNoSqlDb {

	private static final Logger logger = LoggerFactory.getLogger(OperaNoSqlDb.class);

	private static class ThreadLocalObjectCodec extends ThreadLocal<ObjectCodec<Opera>> {

		private final Charset charset;
		
	    public ThreadLocalObjectCodec(Charset charset) {
			super();
			this.charset = charset;
		}

		@Override
	    protected synchronized ObjectCodec<Opera> initialValue() {
	        return new OperaCodec(charset);
	    }
	    
	}
	
	private NoSql<Opera> database;

	public OperaNoSqlDb(Properties configuration, String propertyPrefix) {
		super();
		final File homeFolder = new File(configuration
				.getProperty(propertyPrefix + ".home_folder"));
		final boolean readOnly = "true".equalsIgnoreCase(configuration
				.getProperty(propertyPrefix + ".read_only", "true"));
		final int pageSize = TextUtils.parseIntSize(configuration
				.getProperty(propertyPrefix + ".page_size", "-1"));
		final int cacheSize = TextUtils.parseIntSize(configuration
				.getProperty(propertyPrefix + ".cache_size", "0"));
		final boolean createAlways = "true".equalsIgnoreCase(configuration
				.getProperty(propertyPrefix + ".create_always"));
		final String trieClassName = configuration
				.getProperty(propertyPrefix + ".trie_class_name",
						ConcurrentTrieEx.class.getName());
		if (!readOnly && createAlways && homeFolder.exists()) {
			FileUtils.deleteFolder(homeFolder, true);
		}
		logger.debug("propertyPrefix {}", propertyPrefix);
		database = new ConcurrentNoSql<>(new NoSqlConfig<Opera>()
				.setHomeFolder(homeFolder)
				.setReadOnly(readOnly)
				.setPageSize(pageSize)
				.setCacheSize(cacheSize)
				.setCodec(new ThreadLocalObjectCodec(StandardCharsets.UTF_8))
				.setTrieClassName(trieClassName)
				.setSecondaryIndex(false));
	}

	public File getHomeFolder() {
		return database.getHomeFolder();
	}
	
	public void truncate() {
		database.truncate();
	}

	public void defrag() {
		database.defrag();
	}

	public Opera get(String pk) {
		return database.getByPrimaryKey(pk);
	}

	public Opera put(Opera bean) {
		return database.upsert(bean);
	}

	public void select(Selector<Opera> selector) {
		database.select(selector);
	}

}
