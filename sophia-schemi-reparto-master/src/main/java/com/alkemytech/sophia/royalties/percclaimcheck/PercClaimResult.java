package com.alkemytech.sophia.royalties.percclaimcheck;

import com.google.gson.GsonBuilder;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class PercClaimResult {

    private int id;
    private String idDsr;
    private int idConfig;
    private BigDecimal percDemRes;
    private BigDecimal percDrmRes;
    private BigDecimal percDemConf;
    private BigDecimal percDrmConf;
    private String esito;
    private String transactionId;

    @Override
    public String toString() {
        return new GsonBuilder()
                .create().toJson(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        return true;
    }

}
