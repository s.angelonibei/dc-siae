package com.alkemytech.sophia.royalties;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.sqs.MessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.SqsMessageHelper;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.alkemytech.sophia.commons.sqs.TrieMessageDeduplicator;
import com.alkemytech.sophia.commons.util.FileUtils;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.HeartBeat;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.royalties.country.Country;
import com.alkemytech.sophia.royalties.country.JdbcCountryService;
import com.alkemytech.sophia.royalties.jdbc.IpiDataSource;
import com.alkemytech.sophia.royalties.jdbc.UlisseDataSource;
import com.alkemytech.sophia.royalties.nosql.IpiSoc;
import com.alkemytech.sophia.royalties.nosql.IpiSocNoSqlDb;
import com.alkemytech.sophia.royalties.nosql.IpiTisAgm;
import com.alkemytech.sophia.royalties.nosql.IpiTisAgmNoSqlDb;
import com.alkemytech.sophia.royalties.nosql.Territories;
import com.google.common.base.Strings;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Scopes;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import com.ibm.icu.text.SimpleDateFormat;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class IpiNoSql extends MicroService {
	
	private static final Logger logger = LoggerFactory.getLogger(IpiNoSql.class);

	protected static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// amazon service(s)
			bind(S3.class)
				.in(Scopes.SINGLETON);
			bind(SQS.class)
				.in(Scopes.SINGLETON);
			// data source(s)
			bind(DataSource.class)
				.annotatedWith(Names.named("ULISSE")) // WARNING: used by country service
				.to(UlisseDataSource.class)
				.asEagerSingleton();
			bind(DataSource.class)
				.annotatedWith(Names.named("IPI"))
				.to(IpiDataSource.class)
				.asEagerSingleton();
			// other binding(s)
			bind(JdbcCountryService.class)
				.asEagerSingleton();
			bind(Configuration.class)
				.asEagerSingleton();
			bind(IpiNoSql.class)
				.asEagerSingleton();
		}

	}

	public static void main(String[] args) {
		try {
			final IpiNoSql instance = Guice
					.createInjector(new GuiceModuleExtension(args,
							"/ipi-nosql.properties"))
				.getInstance(IpiNoSql.class)
					.startup();
			try {
				instance.process(args);
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}

	private final SQS sqs;
	private final JdbcCountryService countryService;
	private final DataSource ipiDataSource;

	@Inject
	protected IpiNoSql(Configuration configuration,
			@Named("charset") Charset charset,
			S3 s3, SQS sqs, JdbcCountryService countryService,
			@Named("IPI") DataSource ipiDataSource) {
		super(configuration, charset, s3);
		this.sqs = sqs;
		this.countryService = countryService;
		this.ipiDataSource = ipiDataSource;
	}

	public IpiNoSql startup() throws IOException {
		if ("true".equalsIgnoreCase(configuration.getProperty("ipi_nosql.locked", "true"))) {
			throw new IllegalStateException("application locked");
		}
		super.startup();
		sqs.startup();
		return this;
	}

	public IpiNoSql shutdown() throws IOException {
		sqs.shutdown();
		super.shutdown();
		return this;
	}

	public void process(String[] args) throws Exception {
		final int bindPort = Integer.parseInt(configuration.getProperty("ipi_nosql.bind_port",
				configuration.getProperty("default.bind_port", "0")));
		// bind lock tcp port
		try (ServerSocket socket = new ServerSocket(bindPort)) {
			logger.debug("socket bound to {}", socket.getLocalSocketAddress());
			
			// standalone mode
			if (Arrays.asList(args).contains("standalone") ||
					"true".equalsIgnoreCase(configuration
							.getProperty("ipi_nosql.standalone", "false"))) {
				final JsonObject input = GsonUtils.fromJson(configuration
						.getProperty("ipi_nosql.standalone.message_body"), JsonObject.class);
				final JsonObject output = new JsonObject();
				logger.debug("standalone input {}", new GsonBuilder()
						.setPrettyPrinting().create().toJson(input));
				processMessage(input, output);
				logger.debug("standalone output {}", new GsonBuilder()
						.setPrettyPrinting().create().toJson(output));
				return;
			}
			
			// sqs message pump
			final SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs,
					configuration.getProperties(), "ipi_nosql.sqs");
			final MessageDeduplicator deduplicator = new TrieMessageDeduplicator(new File(configuration
					.getProperty("ipi_nosql.sqs.deduplicator_folder")));
			sqsMessagePump.pollingLoop(deduplicator, new SqsMessagePump.Consumer() {

				private JsonObject output;
				private JsonObject error;

				@Override
				public JsonObject getStartedMessagePayload(JsonObject message) {
					output = null;
					error = null;
					return SqsMessageHelper.formatContext();
				}

				@Override
				public boolean consumeMessage(JsonObject message) {
					try {
						output = new JsonObject();
						processMessage(GsonUtils
								.getAsJsonObject(message, "body"), output);
						error = null;
						return true;
					} catch (Exception e) {
						output = null;
						error = SqsMessageHelper.formatError(e);
						return false;
					}
				}

				@Override
				public JsonObject getCompletedMessagePayload(JsonObject message) {
					return output;
				}

				@Override
				public JsonObject getFailedMessagePayload(JsonObject message) {
					return error;
				}

			});
		}
	}
	
	private void processMessage(JsonObject input, JsonObject output) throws Exception {

//		{
//		  "body": {
//		    "year": "2017",
//		    "month": "03",
//		    "techsysdate": true,
//		    "force": true
//		  },
//		  "header": {
//	  	    "queue": "dev_to_process_ipi_dump_v2",
//		    "timestamp": "2018-05-10T00:00:00.000000+02:00",
//		    "uuid": "7f36a2bb-0637-4d4d-9990-4ed9644adefc",
//		    "sender": "aws-console"
//		  }
//		}

		logger.debug("dumping ipi");
		final long startTimeMillis = System.currentTimeMillis();
		final String datetime = new SimpleDateFormat(configuration
				.getProperty("ipi_nosql.datetime_format", configuration
						.getProperty("default.datetime_format", "yyyyMMddHHmmss")))
				.format(new Date());
		logger.debug("datetime {}", datetime);		

		// parse input json message
		String year = GsonUtils.getAsString(input, "year");
		String month = GsonUtils.getAsString(input, "month");
		if (Strings.isNullOrEmpty(year) ||
				Strings.isNullOrEmpty(month)) {
			throw new IllegalArgumentException("invalid input message");
		}
		String day = GsonUtils.getAsString(input, "day", configuration
				.getProperty("ipi_nosql.reference_day", "01"));
		final boolean techsysdate = GsonUtils.getAsBoolean(input, "techsysdate",
				"true".equalsIgnoreCase(configuration.getProperty("ipi_nosql.techsysdate", "true")));
		final JsonArray stepsJson = GsonUtils.getAsJsonArray(input, "steps");
		final Set<String> steps = new HashSet<>();
		if (null != stepsJson) {
			for (JsonElement stepJson : stepsJson)
				steps.add(stepJson.getAsString());
		} else {
			steps.addAll(Arrays.asList("create", "upload"));
		}
		final String period = fixPeriod(month);
		year = String.format("%04d", Integer.parseInt(year));
		month = fixMonth(month);
		day = String.format("%02d", Integer.parseInt(day));
		logger.debug("year {}", year);
		logger.debug("month {}", month);
		logger.debug("day {}", day);		
		logger.debug("period {}", period);
		logger.debug("techsysdate {}", techsysdate);		
		logger.debug("steps {}", steps);		

		// sql reference date
		final String sqldate = String
				.format("to_date('%s-%s-%s', 'YYYY-MM-DD')", year, month, day);
		logger.debug("sqldate {}", sqldate);
		final String techsqldate = techsysdate ? "sysdate" : sqldate;
		logger.debug("techsqldate {}", techsqldate);

		// configuration tags
		configuration.setTag("{datetime}", datetime);
		configuration.setTag("{year}", year);
		configuration.setTag("{month}", month);
		configuration.setTag("{day}", day);
		configuration.setTag("{period}", period);
		configuration.setTag("{sqldate}", sqldate);
		configuration.setTag("{techsqldate}", techsqldate);

		// configuration
//		final boolean debug = "true".equalsIgnoreCase(configuration
//				.getProperty("ipi_nosql.debug", configuration.getProperty("default.debug")));
		final File homeFolder = new File(configuration
				.getProperty("default.home_folder"));

		final Map<String, String> ipiTypes = GsonUtils.decodeJsonMap(configuration
				.getProperty("ipi_nosql.ipi_types"));
		final Set<String> ipiRoles = new HashSet<>(Arrays.asList(configuration
				.getProperty("ipi_nosql.ipi_roles").split(",")));	
		
		logger.debug("ipiTypes {}", ipiTypes);
		logger.debug("ipiRoles {}", ipiRoles);
		
		if (ipiTypes.isEmpty()) {
			throw new IllegalArgumentException("empty ipi types (rgt)");
		}
		if (ipiRoles.isEmpty()) {
			throw new IllegalArgumentException("empty ipi roles (rol)");
		}
		
		// initialize country service
		countryService.loadCountries();			

		// post execution tasks
		final List<Runnable> postExecTasks = new ArrayList<>();
		
		///////////
		// create
		///////////
		
		if (steps.contains("create")) {

			try (final Connection connection = ipiDataSource.getConnection()) {
				logger.debug("jdbc connected to {} {}", connection.getMetaData()
						.getDatabaseProductName(), connection.getMetaData().getURL());
				
				///////////////
				// ipi_tisagm
				///////////////

				if ("true".equalsIgnoreCase(configuration
						.getProperty("ipi_nosql.ipi_tisagm.read_only"))) {
					configuration.setProperty("ipi_nosql.ipi_tisagm.page_size", "-1");
					configuration.setProperty("ipi_nosql.ipi_tisagm.read_only", "true");
					configuration.setProperty("ipi_nosql.ipi_tisagm.create_always", "false");					
				}
				final IpiTisAgmNoSqlDb ipiTisAgmNoSqlDb = new IpiTisAgmNoSqlDb(configuration
						.getProperties(), "ipi_nosql.ipi_tisagm");			
				
				// create ipi_tisagm nosql
				if (!"true".equalsIgnoreCase(configuration
						.getProperty("ipi_nosql.ipi_tisagm.read_only"))) {
					final long lapTimeMillis = System.currentTimeMillis();
					logger.debug("creating ipi_tisagm nosql");
					
					try (final Statement statement = connection
							.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);) {
						statement.setFetchSize(Integer.parseInt(configuration
								.getProperty("ipi_nosql.ipi_tisagm.fetch_size", "5000")));					
						long count = 0L;
						final Set<String> uniqueTerritories = new HashSet<>();
						final HeartBeat heartbeat = HeartBeat.constant("ipi_tisagm", Integer
								.parseInt(configuration.getProperty("ipi_nosql.ipi_tisagm.heartbeat", "10000")));
						// prepare sql statement
						final String sql = configuration.getProperty("ipi_nosql.ipi_tisagm.sql");
						logger.debug("executing query {}", sql);
						// execute query
						try (final ResultSet resultSet = statement.executeQuery(sql)) {
							logger.debug("query executed in {}",
									TextUtils.formatDuration(System.currentTimeMillis() - lapTimeMillis));
							final Set<Country> countries = new HashSet<Country>();
							String lastAgmid = null;
							// loop on result set
							while (resultSet.next()) {
								heartbeat.pump();
								final String agmid = resultSet.getString("agmid");
								final long tisnr = resultSet.getLong("tisnr");
								final String incexc = resultSet.getString("incexc");
								if (null != lastAgmid && !lastAgmid.equals(agmid)) {
									if (!countries.isEmpty()) {
										final Territories territories = new Territories();
										for (Country country : countries) {
											territories.add(country.getTisA()); // two digits code
										}
										if (!territories.isEmpty()) {
											final IpiTisAgm ipiTisAgm = new IpiTisAgm(Long
													.parseLong(lastAgmid), territories);
											ipiTisAgmNoSqlDb.put(ipiTisAgm);
											count ++;
											countries.clear();
											uniqueTerritories.add(territories.toString());
										}
									}
								}
								lastAgmid = agmid;
								if ("I".equalsIgnoreCase(incexc)) {
									countries.addAll(countryService.getCountries(tisnr));
								} else if ("E".equalsIgnoreCase(incexc)) {
									countries.removeAll(countryService.getCountries(tisnr));
								}
							}
							// last agmid
							if (null != lastAgmid && !countries.isEmpty()) {
								final Territories territories = new Territories();
								for (Country country : countries) {
									territories.add(country.getTisA()); // two digits code
								}
								if (!territories.isEmpty()) {
									final IpiTisAgm ipiTisAgm = new IpiTisAgm(Long
											.parseLong(lastAgmid), territories);
									ipiTisAgmNoSqlDb.put(ipiTisAgm);
									count ++;
									countries.clear();
									uniqueTerritories.add(territories.toString());
								}
							}
						}
						logger.debug("total parsed rows {}", heartbeat.getTotalPumps());
						logger.debug("total ipi_tisagm records {}", count);
						logger.debug("unique territories {}", uniqueTerritories.size());
						
						// output stats
						final JsonObject queryStats = new JsonObject();
						queryStats.addProperty("sql", sql);
						queryStats.addProperty("rownum", heartbeat.getTotalPumps());
						queryStats.addProperty("count", count);
						queryStats.addProperty("duration", TextUtils
								.formatDuration(System.currentTimeMillis() - lapTimeMillis));
						output.add("ipi_tisagm", queryStats);
					}
					
					logger.debug("ipi_tisagm nosql created in {}",
							TextUtils.formatDuration(System.currentTimeMillis() - lapTimeMillis));
				}
				
				// delete ipi_tisagm nosql
				if ("true".equalsIgnoreCase(configuration
						.getProperty("ipi_nosql.ipi_tisagm.delete_when_done"))) {
					postExecTasks.add(new Runnable() {
						@Override
						public void run() {
							logger.debug("deleting ipi_tisagm nosql {}", ipiTisAgmNoSqlDb.getHomeFolder());
							ipiTisAgmNoSqlDb.truncate();
							FileUtils.deleteFolder(ipiTisAgmNoSqlDb.getHomeFolder(), true);
						}
					});
				}
				
				////////////
				// ipi_soc
				////////////

				if ("true".equalsIgnoreCase(configuration
						.getProperty("ipi_nosql.ipi_soc.read_only"))) {
					configuration.setProperty("ipi_nosql.ipi_soc.page_size", "-1");
					configuration.setProperty("ipi_nosql.ipi_soc.read_only", "true");
					configuration.setProperty("ipi_nosql.ipi_soc.create_always", "false");					
				}
				final IpiSocNoSqlDb ipiSocNoSqlDb = new IpiSocNoSqlDb(configuration
						.getProperties(), "ipi_nosql.ipi_soc");

				// create ipi_soc nosql
				if (!"true".equalsIgnoreCase(configuration
						.getProperty("ipi_nosql.ipi_soc.read_only"))) {
					final long lapTimeMillis = System.currentTimeMillis();
					logger.debug("creating ipi_soc nosql");
					try (final Statement statement = connection
							.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);) {
						statement.setFetchSize(Integer.parseInt(configuration
								.getProperty("ipi_nosql.ipi_soc.fetch_size", "5000")));					
						long count = 0L;
						final HeartBeat heartbeat = HeartBeat.constant("ipi_soc", Integer
								.parseInt(configuration.getProperty("ipi_nosql.ipi_soc.heartbeat", "10000")));
						// prepare sql statement
						final String sql = configuration.getProperty("ipi_nosql.ipi_soc.sql");
						logger.debug("executing query {}", sql);
						// execute query
						try (final ResultSet resultSet = statement.executeQuery(sql)) {
							logger.debug("query executed in {}",
									TextUtils.formatDuration(System.currentTimeMillis() - lapTimeMillis));
							IpiSoc ipiSoc = null;
							String lastIpnamenr = null;
							// loop on result set
							while (resultSet.next()) {
								heartbeat.pump();
								final String rgt = ipiTypes.get(resultSet.getString("rgt")); // OD:DEM, MD:DRM
								if (null == rgt) {
									continue;
								}
								final String rol = resultSet.getString("rol");
								if (!ipiRoles.contains(rol)) { // LY, MC, EM
									continue;
								}
								final String ipnamenr = resultSet.getString("ipnamenr");
								if (Strings.isNullOrEmpty(ipnamenr)) {
									continue;
								}
								final String agmid = resultSet.getString("agmid");
								final float shr = resultSet.getFloat("shr");
								final String socname = resultSet.getString("socname");
								if (null != lastIpnamenr && !lastIpnamenr.equals(ipnamenr)) {
									if (null != ipiSoc) {
										ipiSocNoSqlDb.put(ipiSoc);
										count ++;
										ipiSoc = null;
									}								
								}
								lastIpnamenr = ipnamenr;
								// lookup agreement
								final IpiTisAgm ipiTisAgm = ipiTisAgmNoSqlDb.get(agmid);
								if (null != ipiTisAgm && null != ipiTisAgm.territories) {
									if (null == ipiSoc) {
										ipiSoc = new IpiSoc(Long.parseLong(ipnamenr));
									}
									ipiSoc.add(rgt, rol, socname, shr, ipiTisAgm.territories);
								}
							}
							// last record
							if (null != ipiSoc) {
								ipiSocNoSqlDb.put(ipiSoc);
								count ++;
								ipiSoc = null;
							}
						}
						logger.debug("total parsed rows {}", heartbeat.getTotalPumps());
						logger.debug("total ipi_soc records {}", count);
						// output stats
						final JsonObject queryStats = new JsonObject();
						queryStats.addProperty("sql", sql);
						queryStats.addProperty("rownum", heartbeat.getTotalPumps());
						queryStats.addProperty("count", count);
						queryStats.addProperty("duration", TextUtils
								.formatDuration(System.currentTimeMillis() - lapTimeMillis));
						output.add("ipi_soc", queryStats);
					}
					logger.debug("ipi_soc nosql created in {}",
							TextUtils.formatDuration(System.currentTimeMillis() - lapTimeMillis));
				}
				
				// delete ipi_soc nosql
				if ("true".equalsIgnoreCase(configuration
						.getProperty("ipi_nosql.ipi_soc.delete_when_done"))) {
					postExecTasks.add(new Runnable() {
						@Override
						public void run() {
							logger.debug("deleting ipi_soc nosql {}", ipiSocNoSqlDb.getHomeFolder());
							ipiSocNoSqlDb.truncate();
							FileUtils.deleteFolder(ipiSocNoSqlDb.getHomeFolder(), true);
						}
					});
				}

			}
			
		}
		
		///////////
		// upload
		///////////
		
		if (steps.contains("upload")) {
			
			final long lapTimeMillis = System.currentTimeMillis();

			final File archiveFolder = new File(configuration
					.getProperty("ipi_nosql.ipi_soc.home_folder"));
			logger.debug("archiveFolder {}", archiveFolder);
			
			final JsonObject uploadStats = new JsonObject();
			
			archiveAndUpload("ipi_nosql", homeFolder, archiveFolder, uploadStats);
			
			uploadStats.addProperty("duration", TextUtils
					.formatDuration(System.currentTimeMillis() - lapTimeMillis));
			
			output.add("upload", uploadStats);

		}
		
		// run post exec tasks
		for (Runnable task : postExecTasks) {
			task.run();
		}
		
		// output
		output.addProperty("period", year + month);
		output.addProperty("startTime", DateFormat
				.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM)
					.format(new Date(startTimeMillis)));
		output.addProperty("totalDuration", TextUtils
				.formatDuration(System.currentTimeMillis() - startTimeMillis));
		
		logger.debug("message processed in {}",
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));
	}

}
