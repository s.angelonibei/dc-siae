package com.alkemytech.sophia.royalties;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.nosql.NoSql.Selector;
import com.alkemytech.sophia.commons.nosql.NoSqlException;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.HeartBeat;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.royalties.blacklist.BlackList;
import com.alkemytech.sophia.royalties.nosql.IpiSocNoSqlDb;
import com.alkemytech.sophia.royalties.nosql.OperaNoSqlDb;
import com.alkemytech.sophia.royalties.nosql.UuidCodici;
import com.alkemytech.sophia.royalties.nosql.UuidCodiciNoSqlDb;
import com.alkemytech.sophia.royalties.role.RoleCatalog;
import com.alkemytech.sophia.royalties.scheme.IrregularityCatalog;
import com.alkemytech.sophia.royalties.scheme.Royalty;
import com.alkemytech.sophia.royalties.scheme.RoyaltyScheme;
import com.alkemytech.sophia.royalties.scheme.RoyaltySchemeService;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Scopes;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class StressTest extends MicroService {
	
	private static final Logger logger = LoggerFactory.getLogger(StressTest.class);

	protected static class GuiceModuleExtension extends GuiceModule {

		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// amazon service(s)
			bind(S3.class)
				.in(Scopes.SINGLETON);
			// other(s)
			bind(Configuration.class)
				.asEagerSingleton();
			bind(RoleCatalog.class)
				.asEagerSingleton();
			bind(IrregularityCatalog.class)
				.asEagerSingleton();
			bind(RoyaltySchemeService.class)
				.asEagerSingleton();
			bind(StressTest.class)
				.asEagerSingleton();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			final StressTest instance = Guice
					.createInjector(new GuiceModuleExtension(args,
							"/stress_test.properties"))
					.getInstance(StressTest.class)
					.startup();
			try {
				instance.process(args);
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}
	
	private final RoleCatalog roleCatalog;
	private final IrregularityCatalog irregularityCatalog;
	private final RoyaltySchemeService royaltySchemeService;

	@Inject
	protected StressTest(Configuration configuration,
			@Named("charset") Charset charset,
			S3 s3, RoleCatalog roleCatalog,
			IrregularityCatalog irregularityCatalog,
			RoyaltySchemeService royaltySchemeService) {
		super(configuration, charset, s3);
		this.roleCatalog = roleCatalog;
		this.irregularityCatalog = irregularityCatalog;
		this.royaltySchemeService = royaltySchemeService;
	}
	
	public StressTest startup() throws IOException {
		super.startup();
		return this;
	}

	public StressTest shutdown() throws IOException {
		super.shutdown();
		return this;
	}
	
	public StressTest process(String[] args) throws Exception {
		
		// configuration
		final String year = configuration.getProperty("stress_test.year", "2019");
		final String month = configuration.getProperty("stress_test.month", "01");
				
		// configuration tags
		configuration.setTag("{year}", year);
		configuration.setTag("{month}", month);
		
		// initialize role catalog
		roleCatalog.load(s3, configuration.getProperty("role_catalog.csv_url"));	
		
		// initialize irregularity catalog
		irregularityCatalog.load(s3, configuration.getProperty("irregularity_catalog.csv_url"));	

		// initialize ipi nosql to latest version
		downloadAndExtractLatest("stress_test", "stress_test.ipi_nosql");
		final IpiSocNoSqlDb ipiNoSqlDb = new IpiSocNoSqlDb(configuration
				.getProperties(), "stress_test.ipi_nosql");

		// initialize siae nosql to latest version
		downloadAndExtractLatest("stress_test", "stress_test.siae_nosql");
		final OperaNoSqlDb siaeNoSqlDb = new OperaNoSqlDb(configuration
				.getProperties(), "stress_test.siae_nosql");

		// initialize ucmr_ada nosql to latest version
		downloadAndExtractLatest("stress_test", "stress_test.ucmr_ada_nosql");
		final OperaNoSqlDb ucmrAdaNoSqlDb = new OperaNoSqlDb(configuration
				.getProperties(), "stress_test.ucmr_ada_nosql");
		
		// initialize codici nosql to latest version
		downloadAndExtractLatest("stress_test", "stress_test.codici_nosql");
		final UuidCodiciNoSqlDb codiciNoSqlDb = new UuidCodiciNoSqlDb(configuration
				.getProperties(), "stress_test.codici_nosql");

		// initialize black list(s)
		final Map<String, String> blackListUrls = GsonUtils
				.decodeJsonMap(configuration.getProperty("black_list.csv_urls"));
		final Map<String, BlackList> blackLists = new HashMap<>();
		for (Map.Entry<String, String> entry : blackListUrls.entrySet()) {
			blackLists.put(entry.getKey(),
					new BlackList(charset, roleCatalog, entry.getKey())
							.load(s3, entry.getValue()));
		}
		
		// society territory
		final Map<String, String> societyTerritoryMap = GsonUtils
				.decodeJsonMap(configuration.getProperty("stress_test.society_territory", "{}"));
		logger.debug("societyTerritoryMap {}", societyTerritoryMap);
		

				
		final HeartBeat heartbeat = HeartBeat.constant("lookup", 10000); 
		final AtomicLong foundScheme = new AtomicLong(0L);
		final AtomicLong goodScheme = new AtomicLong(0L);
		final AtomicLong blackListOk = new AtomicLong(0L);
		final AtomicLong noBlackListOk = new AtomicLong(0L);

		final long startTimeMillis = System.currentTimeMillis();
		codiciNoSqlDb.select(new Selector<UuidCodici>() {
				
			@Override
			public void select(UuidCodici codici) throws NoSqlException {
				
				for (Map.Entry<String, String> entry : societyTerritoryMap.entrySet()) {
					final String societa = entry.getKey();
					final String territorio = entry.getValue();
					final OperaNoSqlDb nosql = "SIAE".equals(societa) ? siaeNoSqlDb : ucmrAdaNoSqlDb;
					final String codiceOpera = codici.codici.get(societa);
					if (null == codiceOpera) {
						continue;
					}
					final RoyaltyScheme royaltyScheme = royaltySchemeService
							.lookup(ipiNoSqlDb, nosql, societa, codiceOpera, territorio);
					if (null != royaltyScheme) {
						foundScheme.incrementAndGet();
						if (null != royaltyScheme.royalties &&
								!royaltyScheme.royalties.isEmpty()) {
							goodScheme.incrementAndGet();
						}
						if (territorio.equalsIgnoreCase(societyTerritoryMap.get(societa))) {											
							final List<Royalty> filtered = blackLists.get(societa)
								.filter(royaltyScheme.royalties, "YOU", year + month);
							if (null != filtered && !filtered.isEmpty()) {
								blackListOk.incrementAndGet();
							}
						} else {
							final List<Royalty> filtered = new ArrayList<>(royaltyScheme.royalties.size());
							for (Royalty royalty : royaltyScheme.royalties) {
								if (!royalty.doNotClaim && !royalty.sospesoPerEditore) {
									filtered.add(royalty);
								}
							}
							if (!filtered.isEmpty()) {
								noBlackListOk.incrementAndGet();
							}
						}
					}
					heartbeat.pump();
				}

			}
			
		});
		final long elapsedTimeMillis = System.currentTimeMillis() - startTimeMillis;
		
		logger.debug("total items " + heartbeat.getTotalPumps());
		logger.debug("found schemes " + foundScheme.longValue());
		logger.debug("good schemes " + goodScheme.longValue());
		logger.debug("black list ok " + blackListOk.longValue());
		logger.debug("no black list ok " + noBlackListOk.longValue());
		logger.debug("total elapsed millis " + elapsedTimeMillis);
		logger.debug("millis per item " + ((float) elapsedTimeMillis / (float) heartbeat.getTotalPumps()));
		logger.debug("items per second " + (1000f * heartbeat.getTotalPumps() / elapsedTimeMillis));
		logger.debug("lookup completed in " + TextUtils.formatDuration(elapsedTimeMillis));
		

		return this;
	}

}