package com.alkemytech.sophia.royalties;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import com.alkemytech.sophia.commons.aws.S3;
import com.amazonaws.services.s3.model.S3Object;
import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class LatestVersion {

	private final Properties properties;
	
	public LatestVersion() {
		super();
		this.properties = new Properties();
	}
	
	public LatestVersion load(S3 s3, String url) throws IOException {
		try (final S3Object s3object = s3.getObject(new S3.Url(url));
				final InputStream in = s3object.getObjectContent()) {
			return load(in);
		}
	}
	
	public LatestVersion load(File file) throws IOException {
		if (!file.exists()) {
			properties.clear();
			return this;
		}		
		try (final InputStream in = new FileInputStream(file)) {
			return load(in);
		}
	}
	
	public LatestVersion load(InputStream in) throws IOException {
		properties.clear();
		properties.load(in);
		return this;
	}

	protected LatestVersion save(File file) throws IOException {
		try (final OutputStream out = new FileOutputStream(file)) {
			return save(out);
		}
	}
	
	protected LatestVersion save(OutputStream out) throws IOException {
		properties.store(out, "latest version");
		return this;
	}
	
	public String getLocation() {
		return getLocation(null);
	}
	
	public String getLocation(String defaultValue) {
		return properties.getProperty("location", defaultValue);
	}

	public LatestVersion setLocation(String location) {
		properties.setProperty("location", location);
		return this;
	}

	public String getVersion() {
		return getVersion(null);
	}
	
	public String getVersion(String defaultValue) {
		return properties.getProperty("version", defaultValue);
	}

	public LatestVersion setVersion(String version) {
		properties.setProperty("version", version);
		return this;
	}

	public String getProperty(String key) {
		return properties.getProperty(key);
	}
	
	public String getProperty(String key, String defaultValue) {
		return properties.getProperty(key, defaultValue);
	}

	public LatestVersion setProperty(String key, String value) {
		properties.setProperty(key, value);
		return this;
	}
	
	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}

}
