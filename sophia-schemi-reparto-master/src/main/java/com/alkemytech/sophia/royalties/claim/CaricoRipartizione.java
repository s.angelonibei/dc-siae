package com.alkemytech.sophia.royalties.claim;

import java.io.File;
import java.math.BigDecimal;

import org.apache.commons.csv.CSVPrinter;

import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class CaricoRipartizione {

	private String society;			// UCMR-ADA
	private String numeroFattura;	// 12345
	private String dataFattura;		// 12/01/2019
	private BigDecimal valoreDem;	// 100
	private BigDecimal valoreDrm;	// 80
	private String outputUrl;		// s3://siae-sophia-datalake/dev/riparto-siada/181/UCMR-ADA/Report_YouTube_bmat_GB_201801_20180214062650_masterlist-12345.csv

	private File csvFile;
	private CSVPrinter csvPrinter;

	private BigDecimal totalDem;
	private BigDecimal totalDrm;
	
	public CaricoRipartizione() {
		super();
		this.totalDem = BigDecimal.ZERO;
		this.totalDrm = BigDecimal.ZERO;
	}

	public String getSociety() {
		return society;
	}

	public CaricoRipartizione setSociety(String society) {
		this.society = society;
		return this;
	}
	
	public String getNumeroFattura() {
		return numeroFattura;
	}

	public CaricoRipartizione setNumeroFattura(String numeroFattura) {
		this.numeroFattura = numeroFattura;
		return this;
	}

	public String getDataFattura() {
		return dataFattura;
	}

	public CaricoRipartizione setDataFattura(String dataFattura) {
		this.dataFattura = dataFattura;
		return this;
	}

	public BigDecimal getValoreDem() {
		return valoreDem;
	}

	public CaricoRipartizione setValoreDem(BigDecimal valoreDem) {
		this.valoreDem = valoreDem;
		return this;
	}

	public BigDecimal getValoreDrm() {
		return valoreDrm;
	}

	public CaricoRipartizione setValoreDrm(BigDecimal valoreDrm) {
		this.valoreDrm = valoreDrm;
		return this;
	}

	public String getOutputUrl() {
		return outputUrl;
	}

	public CaricoRipartizione setOutputUrl(String outputUrl) {
		this.outputUrl = outputUrl;
		return this;
	}

	public CSVPrinter getCsvPrinter() {
		return csvPrinter;
	}

	public CaricoRipartizione setCsvPrinter(CSVPrinter csvPrinter) {
		this.csvPrinter = csvPrinter;
		return this;
	}

	public File getCsvFile() {
		return csvFile;
	}

	public CaricoRipartizione setCsvFile(File csvFile) {
		this.csvFile = csvFile;
		return this;
	}

	public CaricoRipartizione addTotalDem(BigDecimal value) {
		totalDem = totalDem.add(value);
		return this;
	}

	public BigDecimal getTotalDem() {
		return totalDem;
	}

	public CaricoRipartizione addTotalDrm(BigDecimal value) {
		totalDrm = totalDrm.add(value);
		return this;
	}

	public BigDecimal getTotalDrm() {
		return totalDrm;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}
	
}
