package com.alkemytech.sophia.royalties.claim;

import com.alkemy.siae.sophia.pricing.DsrLine;
import com.alkemy.siae.sophia.pricing.DsrMetadata;
import com.alkemytech.sophia.commons.util.BigDecimals;
import com.alkemytech.sophia.royalties.monitoraggio648.Monitoraggio648;
import com.alkemytech.sophia.royalties.scheme.Royalty;
import com.google.gson.JsonObject;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;


/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */


public class DistributionPrinter {

    //private static final Logger logger = LoggerFactory.getLogger(DistributionPrinter.class);
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(DistributionPrinter.class);

    public static final int SOCIETY_COLUMN = 0;
    public static final int DEM_AMOUNT_COLUMN = 12;
    public static final int DRM_AMOUNT_COLUMN = 17;
    //public static final int COLUMN_COUNT = 21;
    public static final int COLUMN_COUNT = 22; // R5 stabilizzazione transactionId
    //R20-11 add numeratore e denominatore DEM
    public static final int COLUMN_COUNT_SIAE = 24;

    private static final int DEM = 0;
    private static final int DRM = 1;

    private static final BigDecimal ONE_HUNDRED = new BigDecimal("100");
    private static final BigDecimal ONE_HUNDREDTH = BigDecimals
            .divide(BigDecimal.ONE, ONE_HUNDRED);

    private final CSVPrinter csvPrinter;
    private final AtomicLong totalRows;
    private final Map<String, BigDecimal[]> totalValueDem;
    private final Map<String, BigDecimal[]> totalValueDrm;
    private final Monitoraggio648 monitoraggio648;

    public DistributionPrinter(CSVPrinter csvPrinter, Monitoraggio648 monitoraggio648) {
        super();
        this.csvPrinter = csvPrinter;
        this.totalRows = new AtomicLong(0L);
        this.totalValueDem = new HashMap<>();
        this.totalValueDrm = new HashMap<>();
        this.monitoraggio648 = monitoraggio648;
    }

    public BigDecimal getTotalValueDem(String society) {
        final BigDecimal[] array = totalValueDem.get(society);
        return null == array ? BigDecimal.ZERO : array[0];
    }

    public BigDecimal getTotalValueDrm(String society) {
        final BigDecimal[] array = totalValueDrm.get(society);
        return null == array ? BigDecimal.ZERO : array[0];
    }

    private void add(Map<String, BigDecimal[]> sums, String society, BigDecimal value) {
        BigDecimal[] array = sums.get(society);
        if (null == array) {
            array = new BigDecimal[]{BigDecimal.ZERO};
            sums.put(society, array);
        }
        array[0] = value.add(array[0]);
    }

    private String toPlainString(float number, int decimalPlaces) {
        return toPlainString(new BigDecimal(number), decimalPlaces);
    }

    private String toPlainString(BigDecimal number, int decimalPlaces) {
        return number
                .setScale(decimalPlaces, BigDecimal.ROUND_HALF_UP)
                .stripTrailingZeros()
                .toPlainString();
    }

    private List<DistributionInfo[]> mergeDemDrm(List<Royalty> royalties, String society, String codiceOpera, boolean useBlacklist) {
        final Map<String, List<DistributionInfo[]>> rows = new HashMap<>();
        float demShares = 0f;
        float drmShares = 0f;
        if (null != royalties) {
            for (Royalty royalty : royalties) {
                if (royalty.publicDomain) {
                    continue;
                } else if (!society.equals(royalty.socname) && !useBlacklist) {
//					System.out.println("EXCLUDE: "+ codiceOpera + " -> " + royalty.toString());
                    continue;
                }

                final String concatAA = (royalty.codiceIpi == null) ? royalty.posizioneSiae : (royalty.codiceIpi + "");
                final String key = concatAA + ":" + royalty.tipoQualifica;//+ ":" + royalty.codiceConto;

                DistributionInfo[] row = new DistributionInfo[2];
                if (royalty.isPerforming()) {
                    if (royalty != null && royalty.numeratore > 0)
                        logger.debug("NUMERATORE " + royalty.numeratore);
                    if (royalty != null && royalty.denominatore > 0)
                        logger.debug("DENOMINATORE " + royalty.denominatore);
                    row[DEM] = new DistributionInfo()
                            .setCodiceIpi(royalty.codiceIpi)
                            .setPosizioneSiae(royalty.posizioneSiae)
                            .setTipoQualifica(royalty.tipoQualifica)
                            .setPercentuale(royalty.percentuale)
                            .setNumeratore(royalty.numeratore)
                            .setDenominatore(royalty.denominatore)
                            .setCodiceConto(royalty.codiceConto)
                            .setNumeroConto(royalty.codiceContoEstero)
                            .setShares(royalty.shares);
                    demShares += royalty.shares;
                } else if (royalty.isMechanical()) {
                    row[DRM] = new DistributionInfo()
                            .setCodiceIpi(royalty.codiceIpi)
                            .setPosizioneSiae(royalty.posizioneSiae)
                            .setTipoQualifica(royalty.tipoQualifica)
                            .setPercentuale(royalty.percentuale)
                            .setCodiceConto(royalty.codiceConto)
                            .setNumeroConto(royalty.codiceContoEstero)
                            .setShares(royalty.shares);
                    drmShares += royalty.shares;
                }

                if (rows.get(key) == null) {
                    rows.put(key, new ArrayList<DistributionInfo[]>(Collections.singletonList(row)));
                } else {
                    boolean submitted = false;
                    for (DistributionInfo[] existingRow : rows.get(key)) {
                        if (royalty.isPerforming() && existingRow[DEM] == null) {
                            existingRow[DEM] = new DistributionInfo()
                                    .setCodiceIpi(royalty.codiceIpi)
                                    .setPosizioneSiae(royalty.posizioneSiae)
                                    .setTipoQualifica(royalty.tipoQualifica)
                                    .setPercentuale(royalty.percentuale)
                                    .setNumeratore(royalty.numeratore)
                                    .setDenominatore(royalty.denominatore)
                                    .setCodiceConto(royalty.codiceConto)
                                    .setNumeroConto(royalty.codiceContoEstero)
                                    .setShares(royalty.shares);
                            submitted = true;
                            break;
                        } else if (royalty.isMechanical() && existingRow[DRM] == null) {
                            existingRow[DRM] = new DistributionInfo()
                                    .setCodiceIpi(royalty.codiceIpi)
                                    .setPosizioneSiae(royalty.posizioneSiae)
                                    .setTipoQualifica(royalty.tipoQualifica)
                                    .setPercentuale(royalty.percentuale)
                                    .setCodiceConto(royalty.codiceConto)
                                    .setNumeroConto(royalty.codiceContoEstero)
                                    .setShares(royalty.shares);
                            submitted = true;
                            break;
                        }
                    }
                    if (!submitted) {
                        rows.get(key).add(row);
                    }

                }
            }
        }
        final List<DistributionInfo[]> pairs = new ArrayList<>();
        for (String key : rows.keySet()) {
            pairs.addAll(rows.get(key));
        }
        for (DistributionInfo[] pair : pairs) {
            final DistributionInfo dem = pair[DEM];
            if (null != dem) {
                dem.shares = demShares > 0f ? dem.shares / demShares : 0f;
            }
            final DistributionInfo drm = pair[DRM];
            if (null != drm) {
                drm.shares = drmShares > 0f ? drm.shares / drmShares : 0f;
            }
        }
        return pairs;
    }

    private void print(DsrMetadata dsrMetadata, DsrLine dsrLine, ClaimInfo claimInfo, String society, BigDecimal percentualeClaim, final BigDecimal valueDem, final BigDecimal valueDrm, boolean useBlacklist) throws IOException {
        final List<DistributionInfo[]> pairs = mergeDemDrm(claimInfo.royalties, society, dsrLine.getUuidSophia(), useBlacklist);
        if (null == pairs || pairs.isEmpty()) {
            return;
        }

        for (DistributionInfo[] pair : pairs) {

            final DistributionInfo dem = pair[DEM];
            final DistributionInfo drm = pair[DRM];
            final DistributionInfo any = null == dem ? drm : dem;

            final String matchType = dsrLine.getMatchType();
            final String tipoCodifica = "isrc".equalsIgnoreCase(matchType) ? "R" :
                    "iswc".equalsIgnoreCase(matchType) ? "W" : "A";
            final String flagOpera = claimInfo.royaltyScheme.temporaryWork ? "P" :
                    claimInfo.royaltyScheme.irregularWork ? "I" : "R";

            BigDecimal valueDemShares = null == dem ? BigDecimal.ZERO :
                    new BigDecimal(dem.shares).multiply(valueDem);
            add(totalValueDem, society, valueDemShares);

            BigDecimal valueDrmShares = null == drm ? BigDecimal.ZERO :
                    new BigDecimal(drm.shares).multiply(valueDrm);
            add(totalValueDrm, society, valueDrmShares);

            csvPrinter.print(society); // societa (SOCIETY_COLUMN)
            csvPrinter.print(dsrLine.getSalesCount()); // quantita_utilizzazioni
            csvPrinter.print(claimInfo.formatWorkCode()); // codice_opera
            csvPrinter.print(flagOpera); // flag_opera: N=non riconosciuto, P=provvisorio, I=irregolare, R=regolare
            csvPrinter.print(tipoCodifica); // tipo_codifica: N=non identificato, A=titolo artista, R=isrc, W=iswc
            csvPrinter.print(any.tipoQualifica); // tipo_qualifica
            csvPrinter.print(any.posizioneSiae); // posizione_siae
            csvPrinter.print(any.codiceIpi); // ipi_number_ad

            //R20-11 add numeratore e denominatore DEM
            if(society.equalsIgnoreCase("SIAE")) {
                csvPrinter.print(dem != null ? dem.numeratore : 0);
                csvPrinter.print(dem != null ? dem.denominatore : 0);
            }else{
                csvPrinter.print(toPlainString(null == dem ? 0 : dem.percentuale, 2)); // numeratore_quota_dem
                csvPrinter.print(100); // denominatore_quota_dem
            }
            csvPrinter.print(null == dem ? null : dem.codiceConto); // codice_conto_dem
            csvPrinter.print(null == dem ? null : dem.numeroConto); // numero_conto_dem
            csvPrinter.print(BigDecimals.toPlainString(valueDemShares)); // compenso_dem_puntuale (DEM_AMOUNT_COLUMN)

            csvPrinter.print(toPlainString(null == drm ? 0 : drm.percentuale, 2)); // numeratore_quota_drm
            csvPrinter.print(100); // denominatore_quota_drm
            csvPrinter.print(null == drm ? null : drm.codiceConto); // codice_conto_drm
            csvPrinter.print(null == drm ? null : drm.numeroConto); // numero_conto_drm
            csvPrinter.print(BigDecimals.toPlainString(valueDrmShares)); // compenso_drm_puntuale (DRM_AMOUNT_COLUMN)

            csvPrinter.print(toPlainString(percentualeClaim, 2)); // percentuale_di_claim
            csvPrinter.print(dsrLine.getTitle()); // titolo_utilizzazione
            csvPrinter.print(dsrLine.getArtists()); // nominativo_utilizzazione
            csvPrinter.print(dsrLine.getTransactionId()); //  R5 stabilizzazione transaction id
            //R6
            //logger.debug("TRANSACTION ID: "+dsrLine.getTransactionId());
            //R20-11 add numeratore e denominatore DEM
            if(society.equalsIgnoreCase("SIAE")) {
                csvPrinter.print(toPlainString(null == dem ? 0 : dem.percentuale, 2)); // numeratore_quota_dem
                csvPrinter.print(100); // denominatore_quota_dem
            }
            //logger.debug(("DISTRIBUTION PRINTER **********************"));

            // end-of-line
            csvPrinter.println();

        }
    }

    public void printRow(DsrMetadata dsrMetadata, DsrLine dsrLine, ClaimResult claimResult) throws IOException {

        if (null == claimResult || claimResult.isClaimZero()) {
            return;
        }

        totalRows.incrementAndGet();

        // compute combined claim
        final BigDecimal sales = dsrLine.getSalesCount();
        final BigDecimal price = sales.multiply(dsrLine.getRoyaltyEur());
        final double splitDem = dsrMetadata.getSplitDEM();
        final double splitDrm = dsrMetadata.getSplitDRM();
        final BigDecimal percentualeClaim = new BigDecimal(splitDem)
                .multiply(claimResult.getClaimLicensorDem())
                .add(new BigDecimal(splitDrm)
                        .multiply(claimResult.getClaimLicensorDrm()));

        // tenant
        if (claimResult.hasTenant()) {
            final ClaimInfo claimInfo = claimResult.getTenantInfo();
            final String society = dsrMetadata.getTenant().getSociety();

            // compute dem/drm claim and value
            final BigDecimal claimDem = null == claimInfo.claimLicensorDem ?
                    BigDecimal.ZERO : ONE_HUNDREDTH.multiply(claimInfo.claimLicensorDem);
            final BigDecimal claimDrm = null == claimInfo.claimLicensorDrm ?
                    BigDecimal.ZERO : ONE_HUNDREDTH.multiply(claimInfo.claimLicensorDrm);
            final BigDecimal valueDem = claimDem.multiply(price);
            final BigDecimal valueDrm = claimDrm.multiply(price);

//			logger.debug("{} tenant {} dem {} {} drm {} {}",
//					totalRows.get(), society, claimDem, valueDem, claimDrm, valueDrm);

            // update totals
//			add(totalValueDem, society, valueDem);
//			add(totalValueDrm, society, valueDrm);
            boolean useBlacklist = false;
            if (dsrMetadata.getTerritory().equals(dsrMetadata.getTenant().getTerritory()))
                useBlacklist = true;

            // write line
            print(dsrMetadata, dsrLine, claimInfo, society, percentualeClaim, valueDem, valueDrm, useBlacklist);
        }

        // mandator(s)
        for (SocietyInfo societyInfo : dsrMetadata.getMandators()) {
            final String society = societyInfo.getSociety();
            if (claimResult.hasMandator(society)) {
                final ClaimInfo claimInfo = claimResult.getMandatorInfo(society);

                // compute dem/drm claim and value
                final BigDecimal claimDem = null == claimInfo.claimLicensorDem ?
                        BigDecimal.ZERO : ONE_HUNDREDTH.multiply(claimInfo.claimLicensorDem);
                final BigDecimal claimDrm = null == claimInfo.claimLicensorDrm ?
                        BigDecimal.ZERO : ONE_HUNDREDTH.multiply(claimInfo.claimLicensorDrm);
                final BigDecimal valueDem = claimDem.multiply(price);
                final BigDecimal valueDrm = claimDrm.multiply(price);

//				logger.debug("{} mandator {} dem {} {} drm {} {}",
//						totalRows.get(), society, claimDem, valueDem, claimDrm, valueDrm);

                // update totals
//				add(totalValueDem, society, valueDem);
//				add(totalValueDrm, society, valueDrm);
                boolean useBlacklist = false;
                if (dsrMetadata.getTerritory().equals(societyInfo.getTerritory()))
                    useBlacklist = true;



                // write line
                print(dsrMetadata, dsrLine, claimInfo, society, percentualeClaim, valueDem, valueDrm, useBlacklist);

            }
        }
    }

    public JsonObject getStats() {
        final JsonObject result = new JsonObject();
        result.addProperty("totalRows", totalRows);
        final JsonObject dem = new JsonObject();
        for (String society : totalValueDem.keySet()) {
            dem.addProperty(society, BigDecimals
                    .toPlainString(getTotalValueDem(society)));
        }
        result.add("demSum", dem);
        final JsonObject drm = new JsonObject();
        for (String society : totalValueDrm.keySet()) {
            drm.addProperty(society, BigDecimals
                    .toPlainString(getTotalValueDrm(society)));
        }
        result.add("drmSum", drm);
        return result;
    }
}
