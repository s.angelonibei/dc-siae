package com.alkemytech.sophia.royalties.filenaming;

import com.alkemy.siae.sophia.pricing.DsrMetadata;
import com.alkemytech.sophia.commons.http.HTTP;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.royalties.Configuration;
import com.alkemytech.sophia.royalties.utils.RestClient;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.Charset;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;

public class FilenameConfiguration {

    private static final Logger logger = LoggerFactory.getLogger(FilenameConfiguration.class);
    private static final String DATEPATTERN = "yyyyMMdd";
    private static final String UNDERSEPARATOR = "_";
    private static final String SCORESEPARATOR = "-";
    private static final String DOUBLESEPARATOR = "--";
    private static final String CCID = "CCID_";
    private static final String CCIDV13 = "CCID_v";
    private static final String DEFAULTSENDER = "SIAE";
    private static final String EXT = ".DAT";
    private static final String BACKCLAIM = "BC";
    private static String backClaimIncremental;
    private static String bcType;

    private final HTTP http;
    private final Charset charset;
    private final Configuration configuration;
    private Filename filename;

    @Inject
    public FilenameConfiguration(Configuration configuration,
                                 @Named("charset") Charset charset, HTTP http) {
        super();
        this.http = http;
        this.charset = charset;
        this.configuration = configuration;
    }


    public boolean excludeClaim0() {
        logger.debug("Uri chiamata: " + configuration.getProperty("pricing_and_claim.dsr_configuration"));
        try {
            final RestClient.Result response = RestClient.get(configuration.getProperty("pricing_and_claim.dsr_configuration"));
            logger.debug("excludeClaim0: response status  {}", response.getStatusCode());
            logger.debug("excludeClaim0: response body {}", response.toString());
            if (response.getStatusCode() == 200) {
                final JsonObject responseBody = response.getJsonElement().getAsJsonObject();
                final JsonObject configBody = responseBody.getAsJsonObject("configuration");
                logger.debug("ResponseBody: " + responseBody.toString());
                logger.debug("configBody: " + configBody.toString());
                if (configBody.has("excludeClaim0Column1"))
                if (configBody.get("excludeClaim0Column1").getAsString().equalsIgnoreCase("true"))
                    return true;
            }
        } catch (Exception e) {
            logger.error("dsrConfiguration", e);
        }
        return false;
    }

    public boolean isFileNameConfigured() {
        try {
            final RestClient.Result response = RestClient.get(configuration.getProperty("pricing_and_claim.dsr_configuration"));
            logger.debug("dsrConfiguration: response status  {}", response.getStatusCode());
            logger.debug("dsrConfiguration: response body {}", response.toString());
            if (response.getStatusCode() == 200) {
                final JsonObject responseBody = response.getJsonElement().getAsJsonObject();
                final JsonObject configBody = responseBody.getAsJsonObject("configuration");
                if (configBody.has("filename")) {
                    final JsonObject filenameBody = configBody.getAsJsonObject("filename");
                    logger.debug("filenameBody: " + filenameBody.toString());
                    Filename filename = new Filename();
                    if (filenameBody.has("invoiceSender"))
                        filename.setInvoiceSender(filenameBody.get("invoiceSender").getAsString());
                    if (filenameBody.has("invoiceReceiver"))
                        filename.setInvoiceReceiver(filenameBody.get("invoiceReceiver").getAsString());
                    if (filenameBody.has("useType"))
                        filename.setUseType(filenameBody.get("useType").getAsString());
                    if (filenameBody.has("useStartEndDate"))
                        filename.setUseStartEndDate(filenameBody.get("useStartEndDate").getAsString());
                    if (filenameBody.has("licenseSd"))
                        filename.setLicenseSD(filenameBody.get("licenseSd").getAsString());
                    if (filenameBody.has("typeOfClaim"))
                        filename.setTypeOfClaim(filenameBody.get("typeOfClaim").getAsString());
                    setFilename(filename);

                    return true;
                }
            }
        } catch (Exception e) {
            logger.error("dsrConfiguration", e);
        }
        return false;
    }

    private boolean isDsrBackClaimed() {
        try {
            RestClient.Result response = RestClient.get(configuration.getProperty("pricing_and_claim.isDsrbackclaimed"));
            if (response.getStatusCode() == 200) {
                final JsonObject responseBody = response.getJsonElement().getAsJsonObject();
                if (responseBody.has("backclaim") && responseBody.has("bc_type")) {
                    backClaimIncremental = responseBody.get("backclaim").getAsString();
                    bcType = responseBody.get("bc_type").getAsString();

                    return true;
                }
            }
        } catch (Exception e) {
            logger.error("isBackClaimed ", e);
        }
        return false;
    }


    public String getFilenamingCustom(DsrMetadata metadata) {
        StringBuffer buffer = new StringBuffer();
        Filename filename = getFilename();

        buffer.append(CCID + metadata.getCcidVersion());
        if (filename.getInvoiceSender() != null) {
            buffer.append(UNDERSEPARATOR + filename.getInvoiceSender());
        } else {
            buffer.append(UNDERSEPARATOR + DEFAULTSENDER);
        }
        if (filename.getInvoiceReceiver() != null) {
            buffer.append(UNDERSEPARATOR + filename.getInvoiceReceiver());
        } else {
            buffer.append(UNDERSEPARATOR + metadata.getDsp());
        }
        buffer.append(UNDERSEPARATOR + metadata.getCcidId().intValue());
        buffer.append(UNDERSEPARATOR + metadata.getTerritory());

        if (filename.getUseType() != null) {
            buffer.append(UNDERSEPARATOR + filename.getUseType().replace(" ", UNDERSEPARATOR));
        } else {
            buffer.append(UNDERSEPARATOR + metadata.getUseType().replace(" ", UNDERSEPARATOR));
        }
        if (filename.getUseStartEndDate() != null) {
            UseStartEndDate startEndDate = UseStartEndDate.valueOf(filename.getUseStartEndDate());
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATEPATTERN);
            LocalDate date = LocalDate.parse(metadata.getPeriodStartDate(), formatter);
            String monthFormatted = String.format("%02d", date.getMonthValue());
            String dayFormatted = String.format("%02d", date.getDayOfMonth());
            if (startEndDate == UseStartEndDate.yyyy) {
                buffer.append(UNDERSEPARATOR + date.getYear());

            } else if (startEndDate == UseStartEndDate.yyyy_mm) {
                buffer.append(UNDERSEPARATOR + date.getYear() + SCORESEPARATOR + monthFormatted);

            } else if (startEndDate == UseStartEndDate.yyyy_mm_dd) {
                buffer.append(UNDERSEPARATOR + date.getYear() + SCORESEPARATOR + monthFormatted + SCORESEPARATOR + dayFormatted);

            } else if (startEndDate == UseStartEndDate.yyyy_qq) {
                buffer.append(UNDERSEPARATOR + date.getYear() + SCORESEPARATOR + convertMonthToQuarter(monthFormatted));

            } else if (startEndDate == UseStartEndDate.yyyy_www) {
                buffer.append(UNDERSEPARATOR + date.getYear() + SCORESEPARATOR + convertMonthToWeek(monthFormatted, date.getYear()));

            } else if (startEndDate == UseStartEndDate.yyyy_mm_dd__yyyy_mm_dd) {
                LocalDate endDate = LocalDate.parse(metadata.getPeriodEndDate(), formatter);
                String endMonthFormatted = String.format("%02d", endDate.getMonthValue());
                String endDayFormatted = String.format("%02d", endDate.getDayOfMonth());
                buffer.append(UNDERSEPARATOR + date.getYear() + SCORESEPARATOR + monthFormatted +
                        SCORESEPARATOR + dayFormatted +
                        DOUBLESEPARATOR + endDate.getYear() +
                        SCORESEPARATOR + endMonthFormatted +
                        SCORESEPARATOR + endDayFormatted);
            }
        } else {
            buffer.append(UNDERSEPARATOR + metadata.getPeriodStartDate());
        }

        if (filename.getLicenseSD() != null)
            buffer.append(UNDERSEPARATOR + filename.getLicenseSD());

        if (filename.getTypeOfClaim() != null)
            buffer.append(UNDERSEPARATOR + filename.getTypeOfClaim());

        if (isDsrBackClaimed()) {
            buffer.append(UNDERSEPARATOR + BACKCLAIM +
                    UNDERSEPARATOR + bcType +
                    UNDERSEPARATOR + String.format("%04d", Integer.parseInt(backClaimIncremental)));
        }

        buffer.append(EXT);
        logger.info("ccidFilenameCustom {}" + buffer.toString());
        return buffer.toString();
    }


    public String getFilenamingDefault(DsrMetadata dsrMetadata) {
        String CCIDVersion;
        if (dsrMetadata.getCcidVersion().equals("14")) {
            CCIDVersion = CCID;
        } else {
            CCIDVersion = CCIDV13;
        }
        final String ccidFilename = String.format(CCIDVersion + "%s_%s_%s%06d_%s_%s_%s.DAT",
                dsrMetadata.getCcidVersion(),
                dsrMetadata.getTenant().getSociety(),
                dsrMetadata.getYear(),
                dsrMetadata.getCcidId().intValue(),
                dsrMetadata.getTerritory(),
                dsrMetadata.getPeriodStartDate(),
                dsrMetadata.getIdDsr());
        logger.debug("ccidFilename {}", ccidFilename);

        return ccidFilename;
    }

    private String convertMonthToQuarter(String month) {
        String quarter = "";
        if (Integer.parseInt(month) >= 1 && Integer.parseInt(month) <= 3)
            quarter = "Q1";
        if (Integer.parseInt(month) >= 4 && Integer.parseInt(month) <= 6)
            quarter = "Q2";
        if (Integer.parseInt(month) >= 7 && Integer.parseInt(month) <= 9)
            quarter = "Q3";
        if (Integer.parseInt(month) >= 10 && Integer.parseInt(month) <= 12)
            quarter = "Q4";
        return quarter;
    }

    private String convertMonthToWeek(String month, int year) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        cal.set(Calendar.DAY_OF_WEEK_IN_MONTH, 1);
        cal.set(Calendar.WEEK_OF_MONTH, 1);
        cal.set(Calendar.MONTH, (Integer.parseInt(month) - 1));
        String week = "W" + cal.get(Calendar.WEEK_OF_YEAR);
        logger.info("Converted month " + month + " to " + week);
        return week;
    }

    private Filename getFilename() {
        return filename;
    }

    private void setFilename(Filename filename) {
        this.filename = filename;
    }
}
