package com.alkemytech.sophia.royalties.claim;

import com.alkemytech.sophia.royalties.blacklist.BlackList;
import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class SocietyInfo {

	private String society;			// UCMR-ADA
	private String territory;		// RO
	private BlackList blackList;	

	public SocietyInfo() {
		super();
	}

	public String getSociety() {
		return society;
	}

	public SocietyInfo setSociety(String society) {
		this.society = society;
		return this;
	}

	public String getTerritory() {
		return territory;
	}

	public SocietyInfo setTerritory(String territory) {
		this.territory = territory;
		return this;
	}

	public BlackList getBlackList() {
		return blackList;
	}

	public SocietyInfo setBlackList(BlackList blackList) {
		this.blackList = blackList;
		return this;
	}

	@Override
	public int hashCode() {
		return 31 + ((society == null) ? 0 : society.hashCode());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SocietyInfo other = (SocietyInfo) obj;
		if (society == null) {
			if (other.society != null)
				return false;
		} else if (!society.equals(other.society))
			return false;
		return true;
	}	
	
	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}
	
}
