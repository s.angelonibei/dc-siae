package com.alkemytech.sophia.royalties.servlet;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.util.GsonUtils;
import com.google.gson.JsonObject;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class GsonFilter implements Filter {

	public static final String REQUEST_JSON = "requestJson";

	private final Logger logger = LoggerFactory.getLogger(getClass());

	/*
	 * (non-Javadoc)
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig config) throws ServletException {
		
	}

	/*
	 * (non-Javadoc)
	 * @see javax.servlet.Filter#destroy()
	 */
	@Override
	public void destroy() {
		
	}
	
	/*
	 * (non-Javadoc)
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		// HTTP POST request(s) only
		final HttpServletRequest httpRequest = (HttpServletRequest) request;
		final String requestMethod = httpRequest.getMethod();
		logger.debug("requestMethod {}", requestMethod);
		if (!"POST".equalsIgnoreCase(requestMethod))
			throw new ServletException("invalid request method");

		// application/json content type(s) only
		final String requestContentType = request.getContentType();
		logger.debug("requestContentType {}", requestContentType);
		if (-1 == requestContentType
				.toLowerCase().indexOf("application/json"))
			throw new ServletException("invalid request content type");

		// decode request body
		final JsonObject requestJson = GsonUtils
				.fromJson(request.getReader(), JsonObject.class);
		logger.debug("requestJson {}", GsonUtils.toJson(requestJson));
		
		// pass on request json
		request.setAttribute(REQUEST_JSON, requestJson);
		chain.doFilter(request, response);

	}
	
}
