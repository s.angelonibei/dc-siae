package com.alkemytech.sophia.royalties.percclaimcheck;

import com.alkemy.siae.sophia.pricing.DsrMetadata;
import com.alkemytech.sophia.royalties.Configuration;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class PercClaimService {

    private static final Logger logger = LoggerFactory.getLogger(PercClaimService.class);

    private final Configuration configuration;
    private final DataSource dataSource;

    @Inject
    protected PercClaimService(Configuration configuration,
                                @Named("MCMDB") DataSource dataSource) {
        super();
        this.configuration = configuration;
        this.dataSource = dataSource;
    }

    public Map<String, PercClaimConfig> getPercClaimConfigs(DsrMetadata dsrMetadata) throws SQLException {
        Map<String, PercClaimConfig> result = new ConcurrentHashMap<>();
        try (final Connection connection = dataSource.getConnection()) {
            logger.debug("jdbc connected to {} {}", connection.getMetaData()
                    .getDatabaseProductName(), connection.getMetaData().getURL());
            try (final Statement statement = connection.createStatement()) {
                final String sql = configuration
                        .getProperty("perc_claim_service.sql.select_mm_perc_claim_config");
                logger.debug("executing query {}", sql);
                try (final ResultSet resultSet = statement.executeQuery(sql)) {
                    while (resultSet.next()) {
                        // Riempio la mappa result
                        PercClaimConfig percClaimConfig = new PercClaimConfig();
                        percClaimConfig.setId(resultSet.getInt("ID"));
                        percClaimConfig.setCodiceUuid(resultSet.getString("CODICE_UUID"));
                        percClaimConfig.setIdDsp(resultSet.getString("ID_DSP"));
                        percClaimConfig.setTerritorio(resultSet.getString("TERRITORIO"));
                        percClaimConfig.setTipoUtilizzo(resultSet.getString("TIPO_UTILIZZO"));
                        percClaimConfig.setOffertaCommerciale(resultSet.getString("OFFERTA_COMMERCIALE"));
                        percClaimConfig.setPercDem(resultSet.getBigDecimal("PERC_DEM"));
                        percClaimConfig.setPercDrm(resultSet.getBigDecimal("PERC_DRM"));
                        result.put(percClaimConfig.getCodiceUuid(),percClaimConfig);
                    }
                }
            }
        }
        logger.debug("PercClaimConfig: ", result);
        return result;
    }

    public int insertPercClaimResult(Map<String, PercClaimResult> percClaimResults) throws SQLException {
        int result = 0;
        try (final Connection connection = dataSource.getConnection()) {
            logger.debug("jdbc connected to {} {}", connection.getMetaData()
                    .getDatabaseProductName(), connection.getMetaData().getURL());
            try (final Statement statement = connection.createStatement()) {
                for (PercClaimResult percClaimResult : percClaimResults.values()) {
                    configuration.setTag("{idConfig}", percClaimResult.getIdConfig()!=-1 ? String.valueOf(percClaimResult.getIdConfig()) : "null");
                    configuration.setTag("{percDemRes}", percClaimResult.getPercDemRes()!=null ? "'"+ percClaimResult.getPercDemRes() +"'" : "null");
                    configuration.setTag("{percDrmRes}", percClaimResult.getPercDrmRes()!=null ? "'"+ percClaimResult.getPercDrmRes() +"'" : "null");
                    configuration.setTag("{percDemConf}", percClaimResult.getPercDemConf()!=null ? "'"+ percClaimResult.getPercDemConf() +"'" : "null");
                    configuration.setTag("{percDrmConf}", percClaimResult.getPercDrmConf()!=null ? "'"+ percClaimResult.getPercDrmConf() +"'" : "null");
                    configuration.setTag("{transactionId}", percClaimResult.getTransactionId()!= null ? "'"+ percClaimResult.getTransactionId() +"'" : "null");
                    configuration.setTag("{esito}", percClaimResult.getEsito());
                    final String sql = configuration
                            .getProperty("perc_claim_service.sql.insert_mm_perc_claim_result");
                    logger.debug("executing query {}", sql);
                    result = result + statement.executeUpdate(sql);
                }
                //connection.commit();
            }
        }
        logger.debug("Righe inserite su PercClaimResult: {}", result);
        return result;
    }

    public int deletePercClaimResultByIdDsr() throws SQLException {
        int result = 0;
        try (final Connection connection = dataSource.getConnection()) {
            logger.debug("jdbc connected to {} {}", connection.getMetaData()
                    .getDatabaseProductName(), connection.getMetaData().getURL());
            try (final Statement statement = connection.createStatement()) {
                final String sql = configuration
                        .getProperty("perc_claim_service.sql.delete_mm_perc_claim_result");
                logger.debug("executing query {}", sql);
                result = statement.executeUpdate(sql);
                //connection.commit();
            }
        }
        logger.debug("Righe cancellate su PercClaimResult: {}", result);
        return result;
    }

}
