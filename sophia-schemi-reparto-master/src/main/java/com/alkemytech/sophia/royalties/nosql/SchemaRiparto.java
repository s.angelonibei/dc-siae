package com.alkemytech.sophia.royalties.nosql;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class SchemaRiparto {

	public final long idSchemaRiparto;
	public final Territories territories;
	public final List<QuotaRiparto> quote;	
	private boolean irregular;
	
	public SchemaRiparto(long idSchemaRiparto, Territories territories) {
		super();
		if (null == territories || territories.isEmpty()) {
			throw new IllegalArgumentException("territories is null or empty");
		}
		this.idSchemaRiparto = idSchemaRiparto;
		this.territories = territories;
		this.quote = new ArrayList<>();
		this.irregular = false;
	}

	public boolean add(QuotaRiparto quota) {
		irregular = irregular || (null != quota.idTipoIrregolarita &&
				!"".equals(quota.idTipoIrregolarita));
		return quote.add(quota);
	}

	public boolean isIrregular() {
		return irregular;
	}
	
	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}
	
}
