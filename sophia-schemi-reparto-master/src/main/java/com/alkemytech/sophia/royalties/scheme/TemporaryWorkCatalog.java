package com.alkemytech.sophia.royalties.scheme;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.QuoteMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.io.CompressionAwareFileInputStream;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.royalties.nosql.Territories;
import com.amazonaws.services.s3.model.S3Object;
import com.google.common.base.Strings;
import com.google.gson.GsonBuilder;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class TemporaryWorkCatalog {
	
	private static final Logger logger = LoggerFactory.getLogger(TemporaryWorkCatalog.class);

	private final Charset charset;
	private final Map<String, Map<String, RoyaltyScheme>> temporaryWorkMap;

	@Inject
	public TemporaryWorkCatalog(@Named("charset") Charset charset) {
		super();
		this.charset = charset;
		this.temporaryWorkMap = new ConcurrentHashMap<>();
	}
	
	public TemporaryWorkCatalog load(S3 s3, String url) throws IOException {
		try (final S3Object s3object = s3.getObject(new S3.Url(url));
				final InputStream in = s3object.getObjectContent()) {
			return load(in);
		}
	}

	public TemporaryWorkCatalog load(File file) throws IOException {
		try (CompressionAwareFileInputStream in = new CompressionAwareFileInputStream(file)) {
			return load(in);
		}
	}

	public synchronized TemporaryWorkCatalog load(InputStream in) throws IOException {
		
		temporaryWorkMap.clear();

		// parse csv file
		try (final Reader reader = new InputStreamReader(in, charset);
				final CSVParser csvParser = new CSVParser(reader, CSVFormat.newFormat(';')
						.withIgnoreEmptyLines()
						.withIgnoreSurroundingSpaces()
						.withQuoteMode(QuoteMode.MINIMAL)
						.withQuote('"'))) {
						
			final Iterator<CSVRecord> csvIterator = csvParser.iterator();
			
			// skip headers
			csvIterator.next();
			
			while (csvIterator.hasNext()) {
				final CSVRecord record = csvIterator.next();
				
				// SOCIETA;PREFISSO;DESCRIZIONE;ID_REPERTORIO;POSIZIONE_DEM;POSIZIONE_DRM;CONTO_DEM;CONTO_DRM;CONTO_ESTERO_DEM;CONTO_ESTERO_DRM;PERCENTUALE_DEM;PERCENTUALE_DRM;CODICE_IPI;TIPO_QUALIFICA;FORMATO_QUALIFICA;SOCIETA_IPI;SHARE_IPI;TERRITORI;CODICE_ESEMPIO
				final String societa = record.get(0);
				final String prefisso = record.get(1);
//				final String descrizione = record.get(2);
				final String idRepertorio = record.get(3);
				final String posizioneDem = record.get(4);
				final String posizioneDrm = record.get(5);
				final String contoDem = record.get(6);
				final String contoDrm = record.get(7);
				final String contoEsteroDem = record.get(8);
				final String contoEsteroDrm = record.get(9);
				final String percentualeDem = record.get(10);
				final String percentualeDrm = record.get(11);
				final String codiceIpi = record.get(12);
				final String tipoQualifica = record.get(13);
				final String formatoQualifica = record.get(14);
				final String societaIpi = record.get(15);
				final String shareIpi = record.get(16);
				final String territori = record.get(17);
//				final String codiceEsempio = record.get(18);

				if (!Strings.isNullOrEmpty(societa) &&
						!Strings.isNullOrEmpty(prefisso)) {
					
					// scheme map
					if (!Strings.isNullOrEmpty(territori)) {
						Map<String, RoyaltyScheme> temporaryWork = temporaryWorkMap.get(societa);
						if (null == temporaryWork) {
							temporaryWork = new ConcurrentHashMap<>();
							temporaryWorkMap.put(societa, temporaryWork);
						}
						final RoyaltyScheme royaltyScheme = new RoyaltyScheme()
								.setTerritories(new Territories(TextUtils.remove(territori, '|'), true));	
						final float socshr = Strings.isNullOrEmpty(shareIpi) ?
								0f : Float.parseFloat(shareIpi);
						final float sharesDem = Strings.isNullOrEmpty(percentualeDem) ?
								0f : Float.parseFloat(percentualeDem);
						final float sharesDrm = Strings.isNullOrEmpty(percentualeDrm) ?
								0f : Float.parseFloat(percentualeDrm);
						royaltyScheme.addRoyalty(new Royalty()
								.setCodiceIpi(Strings.isNullOrEmpty(codiceIpi) ?
										null : Long.parseLong(codiceIpi))
								.setPosizioneSiae(posizioneDem)
								.setTipoQualifica(tipoQualifica)
								.setFormatoQualifica(formatoQualifica)
								.setTipoQuota("DEM")
								.setPercentuale(sharesDem)
//								.setCognome(descrizione)
								.setCodiceConto(contoDem)
								.setCodiceContoEstero(contoEsteroDem)
								.setIdRepertorio(Strings.isNullOrEmpty(idRepertorio) ?
										0L : Long.parseLong(idRepertorio))
//								.setIdTipoIrregolarita(null)
								.setSocname(societaIpi)
								.setSocshr(socshr)
								.setShares((sharesDem / 100f) * (socshr / 100f))
								.setSospesoPerEditore(false));
						royaltyScheme.addRoyalty(new Royalty()
								.setCodiceIpi(Strings.isNullOrEmpty(codiceIpi) ?
										null : Long.parseLong(codiceIpi))
								.setPosizioneSiae(posizioneDrm)
								.setTipoQualifica(tipoQualifica)
								.setFormatoQualifica(formatoQualifica)
								.setTipoQuota("DRM")
								.setPercentuale(sharesDrm)
//								.setCognome(descrizione)
								.setCodiceConto(contoDrm)
								.setCodiceContoEstero(contoEsteroDrm)
								.setIdRepertorio(Strings.isNullOrEmpty(idRepertorio) ?
										0L : Long.parseLong(idRepertorio))
//								.setIdTipoIrregolarita(null)
								.setSocname(societaIpi)
								.setSocshr(socshr)
								.setShares((sharesDrm / 100f) * (socshr / 100f))
								.setSospesoPerEditore(false));
						royaltyScheme.setDemTotal(sharesDem / 100f);
						royaltyScheme.setDrmTotal(sharesDrm / 100f);
						temporaryWork.put(prefisso, royaltyScheme);
					}
				}
			}
			
		}
		logger.debug("irregularities: {}", temporaryWorkMap);
		
		return this;
	}
	
	public boolean isTemporaryWork(String societa, String codiceOpera) {
		final Map<String, RoyaltyScheme> temporaryWork = temporaryWorkMap.get(societa);
		if (null == temporaryWork) {
			return false;
		}
		for (String prefix : temporaryWork.keySet()) {
			if (codiceOpera.startsWith(prefix)) {
				return true;
			}
		}
		return false;
	}
	
	public RoyaltyScheme getRoyaltyScheme(String societa, String codiceOpera, Territories territories) {
		// search royalty scheme for selected id_tipo_irregolarita
		final Map<String, RoyaltyScheme> temporaryWork = temporaryWorkMap.get(societa);
		if (null == temporaryWork) {
			return null;
		}
		for (Map.Entry<String, RoyaltyScheme> entry : temporaryWork.entrySet()) {
			if (codiceOpera.startsWith(entry.getKey())) {
				final RoyaltyScheme royaltyScheme = entry.getValue();
//				logger.debug("royaltyScheme {}", royaltyScheme);
				territories = territories.intersection(royaltyScheme.territories);
//				logger.debug("territories {}", territories);
				if (null == territories || territories.isEmpty()) {
					return null;
				}				
				return new RoyaltyScheme()
						.setTerritories(territories)
						.addRoyalties(royaltyScheme.royalties)
						.setDemTotal(royaltyScheme.demTotal)
						.setDrmTotal(royaltyScheme.drmTotal)
						.setTemporaryWork(true);
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}
	
}
