package com.alkemytech.sophia.royalties.drools;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
@SuppressWarnings("serial")
public class KnowledgeBaseException extends RuntimeException {

	public KnowledgeBaseException() {
		super();
	}

	public KnowledgeBaseException(String message, Throwable cause) {
		super(message, cause);
	}

	public KnowledgeBaseException(String message) {
		super(message);
	}

	public KnowledgeBaseException(Throwable cause) {
		super(cause);
	}
		
}
