package com.alkemytech.sophia.royalties.role;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.QuoteMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.io.CompressionAwareFileInputStream;
import com.amazonaws.services.s3.model.S3Object;
import com.google.common.base.Strings;
import com.google.gson.GsonBuilder;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class RoleCatalog {
	
	private static final Logger logger = LoggerFactory.getLogger(RoleCatalog.class);

	private final Charset charset;
	private final Map<String, Map<String, String[]>> mappings;
	private final Map<String, Map<String, Object>> blackListsAuthor;
	private final Map<String, Map<String, Object>> blackListsSubEditor;
	private final Map<String, Map<String, Object>> blackListsOriginalEditor;
	private final Map<String, Map<String, Object>> doNotClaimRoles;

	@Inject
	public RoleCatalog(@Named("charset") Charset charset) {
		super();
		this.charset = charset;
		this.mappings = new ConcurrentHashMap<>();
		this.blackListsAuthor = new ConcurrentHashMap<>();
		this.blackListsSubEditor = new ConcurrentHashMap<>();
		this.blackListsOriginalEditor = new ConcurrentHashMap<>();
		this.doNotClaimRoles = new ConcurrentHashMap<>();
	}
	
	private static void extendMappings(Map<String, Map<String, String[]>> mappings, String fromFormat, String fromCode, String toFormat, String toCode) {
		final String key = fromFormat + '-' + toFormat;
		Map<String, String[]> mapping = mappings.get(key);
		if (null == mapping) {
			mapping = new ConcurrentHashMap<>();
			mappings.put(key, mapping);
		}
		String[] toCodes = mapping.get(fromCode);
		if (null == toCodes) {
			toCodes = new String[1];
			mapping.put(fromCode, toCodes);
		} else {
			final String[] toCodesEx = new String[1 + toCodes.length];
			System.arraycopy(toCodes, 0, toCodesEx, 0, toCodes.length);
			toCodes = toCodesEx;
		}
		toCodes[toCodes.length - 1] = toCode;
	}
	
	private static void extendBlackList(Map<String, Map<String, Object>> blackLists, String type, String code) {
		Map<String, Object> blackList = blackLists.get(type);
		if (null == blackList) {
			blackList = new ConcurrentHashMap<>();
			blackLists.put(type, blackList);
		}
		blackList.put(code, code);
	}
	
	public RoleCatalog load(S3 s3, String url) throws IOException {
		try (final S3Object s3object = s3.getObject(new S3.Url(url));
				final InputStream in = s3object.getObjectContent()) {
			return load(in);
		}
	}

	public RoleCatalog load(File file) throws IOException {
		try (CompressionAwareFileInputStream in = new CompressionAwareFileInputStream(file)) {
			return load(in);
		}
	}
	
	public synchronized RoleCatalog load(InputStream in) throws IOException {
		
		mappings.clear();
		blackListsAuthor.clear();
		blackListsSubEditor.clear();

		// parse csv file
		try (final Reader reader = new InputStreamReader(in, charset);
				final CSVParser csvParser = new CSVParser(reader, CSVFormat.newFormat(';')
						.withIgnoreEmptyLines()
						.withIgnoreSurroundingSpaces()
						.withQuoteMode(QuoteMode.MINIMAL)
						.withQuote('"'))) {
						
			final Iterator<CSVRecord> csvIterator = csvParser.iterator();
			
			// skip headers
			csvIterator.next();
			
			while (csvIterator.hasNext()) {
				final CSVRecord record = csvIterator.next();
				
				// ID;SIAE_CODE;SIAE_DESCRIPTION;CISAC_CODE;CISAC_DESCRIPTION;IPI_CODE;IPI_DESCRIPTION;BLACKLIST_ROLE;DO_NOT_CLAIM_FLAG
//				final String id = record.get(0);
				final String siaeCode = record.get(1);
//				final String siaeDescription = record.get(2);
				final String cisacCode = record.get(3);
//				final String cisacDescription = record.get(4);
				final String ipiCode = record.get(5);
//				final String ipiDescription = record.get(6);
				final String blackListRole = record.get(7).toUpperCase();
				final String doNotClaimFlag = record.get(8);
				
				if (!Strings.isNullOrEmpty(siaeCode)) { // from SIAE
					if (!Strings.isNullOrEmpty(cisacCode)) { // to CISAC
						extendMappings(mappings, "SIAE", siaeCode, "CISAC", cisacCode);
					}
					if (!Strings.isNullOrEmpty(ipiCode)) { // to IPI
						extendMappings(mappings, "SIAE", siaeCode, "IPI", ipiCode);
					}
					if (blackListRole.contains("AUTHOR") ||
							blackListRole.contains("COMPOSER")) { // black list composer author flag
						extendBlackList(blackListsAuthor, "SIAE", siaeCode);
					}
					if (blackListRole.contains("SUBEDITOR")) { // black list sub editor flag
						extendBlackList(blackListsSubEditor, "SIAE", siaeCode);
					}
					if (blackListRole.contains("ORIGINAL EDITOR")) { // black list original editor flag
						extendBlackList(blackListsOriginalEditor, "SIAE", siaeCode);
					}
					if (!Strings.isNullOrEmpty(doNotClaimFlag)) { // do not claim flag
						extendBlackList(doNotClaimRoles, "SIAE", siaeCode);
					}
				}

				if (!Strings.isNullOrEmpty(cisacCode)) { // from CISAC
					if (!Strings.isNullOrEmpty(siaeCode)) { // to SIAE
						extendMappings(mappings, "CISAC", cisacCode, "SIAE", siaeCode);
					}
					if (!Strings.isNullOrEmpty(ipiCode)) { // to IPI
						extendMappings(mappings, "CISAC", cisacCode, "IPI", ipiCode);
					}
					if (blackListRole.contains("AUTHOR") ||
							blackListRole.contains("COMPOSER")) { // black list composer author flag
						extendBlackList(blackListsAuthor, "CISAC", cisacCode);
					}
					if (blackListRole.contains("SUBEDITOR")) { // black list sub editor flag
						extendBlackList(blackListsSubEditor, "CISAC", cisacCode);
					}
					if (blackListRole.contains("ORIGINAL EDITOR")) { // black list original editor flag
						extendBlackList(blackListsOriginalEditor, "CISAC", cisacCode);
					}
					if (!Strings.isNullOrEmpty(doNotClaimFlag)) { // do not claim flag
						extendBlackList(doNotClaimRoles, "CISAC", cisacCode);
					}
				}

				if (!Strings.isNullOrEmpty(ipiCode)) { // from IPI
					if (!Strings.isNullOrEmpty(siaeCode)) { // to SIAE
						extendMappings(mappings, "IPI", ipiCode, "SIAE", siaeCode);
					}
					if (!Strings.isNullOrEmpty(cisacCode)) { // to CISAC
						extendMappings(mappings, "IPI", ipiCode, "CISAC", cisacCode);
					}
					if (blackListRole.contains("AUTHOR") ||
							blackListRole.contains("COMPOSER")) { // black list composer author flag
						extendBlackList(blackListsAuthor, "IPI", ipiCode);
					}
					if (blackListRole.contains("SUBEDITOR")) { // black list sub editor flag
						extendBlackList(blackListsSubEditor, "IPI", ipiCode);
					}
					if (blackListRole.contains("ORIGINAL EDITOR")) { // black list original editor flag
						extendBlackList(blackListsOriginalEditor, "IPI", ipiCode);
					}
					if (!Strings.isNullOrEmpty(doNotClaimFlag)) { // do not claim flag
						extendBlackList(doNotClaimRoles, "IPI", ipiCode);
					}
				}

			}

		}
		
		logger.debug("mappings: {}", mappings);
		logger.debug("blackListsAuthor: {}", blackListsAuthor);
		logger.debug("blackListsSubEditor: {}", blackListsSubEditor);
		
		return this;
	}
	
	public String convert(String fromFormat, String fromRole, String toFormat) {
		return convert(fromFormat, fromRole, toFormat, null);
	}
	
	public String convert(String fromFormat, String fromRole, String toFormat, String ifNotFound) {
		if (null == fromRole) {
			return ifNotFound;
		}
		if (fromFormat.equals(toFormat)) {
			return fromRole;
		}
		final Map<String, String[]> mapping = mappings.get(fromFormat + '-' + toFormat);
		if (null == mapping) {
			return ifNotFound;
		}
		final String[] toRoles = mapping.get(fromRole);
		return null == toRoles ? ifNotFound : toRoles[0];
	}
	
	public boolean isBlackListAuthor(String type, String role) {
		if (null == type) {
			return false;
		}
		if (null == role) {
			return false;
		}
		final Map<String, Object> roles = blackListsAuthor.get(type);
		return null != roles && roles.containsKey(role);
	}

	public boolean isBlackListSubEditor(String type, String role) {
		if (null == type) {
			return false;
		}
		if (null == role) {
			return false;
		}
		final Map<String, Object> roles = blackListsSubEditor.get(type);
		return null != roles && roles.containsKey(role);
	}
	
	public boolean isBlackListOriginalEditor(String type, String role) {
		if (null == type) {
			return false;
		}
		if (null == role) {
			return false;
		}
		final Map<String, Object> roles = blackListsOriginalEditor.get(type);
		return null != roles && roles.containsKey(role);
	}
	
	public boolean isDoNotClaimRole(String type, String role) {
		if (null == type) {
			return false;
		}
		if (null == role) {
			return false;
		}
		final Map<String, Object> roles = doNotClaimRoles.get(type);
		return null != roles && roles.containsKey(role);
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}
	
}
