package com.alkemytech.sophia.royalties.nosql;

import com.alkemytech.sophia.commons.nosql.NoSqlEntity;
import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class IpiTisAgm implements NoSqlEntity {

	public final long agmid;
	public final Territories territories;

	public IpiTisAgm(long agmid, Territories territories) {
		super();
		if (null == territories || territories.isEmpty())
			throw new IllegalArgumentException("territories is null or empty");
		this.agmid = agmid;
		this.territories = territories;
	}
	
	@Override
	public String getPrimaryKey() {
		return Long.toString(agmid);
	}

	@Override
	public String getSecondaryKey() {
		return null;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}
	
}
