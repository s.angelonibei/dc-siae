package com.alkemytech.sophia.royalties;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.ServerSocket;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;
import java.util.regex.Pattern;

import javax.sql.DataSource;

import com.alkemytech.sophia.commons.util.FileUtils;
import com.alkemytech.sophia.royalties.claim.*;
import com.alkemytech.sophia.royalties.filenaming.FilenameConfiguration;
import com.alkemytech.sophia.royalties.filenaming.MMSpecialCharCcid;
import com.alkemytech.sophia.royalties.percclaimcheck.PercClaimConfig;
import com.alkemytech.sophia.royalties.percclaimcheck.PercClaimResult;
import com.alkemytech.sophia.royalties.percclaimcheck.PercClaimService;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.drools.KnowledgeBase;
import org.drools.runtime.StatefulKnowledgeSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemy.siae.sophia.pricing.DsrLine;
import com.alkemy.siae.sophia.pricing.DsrMetadata;
import com.alkemy.siae.sophia.pricing.PricingResult;
import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.http.HTTP;
import com.alkemytech.sophia.commons.io.CompressionAwareFileInputStream;
import com.alkemytech.sophia.commons.io.CompressionAwareFileOutputStream;
import com.alkemytech.sophia.commons.sqs.McmdbMessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.MessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.SqsMessageHelper;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.royalties.country.CountryService;
import com.alkemytech.sophia.royalties.country.JsonCountryService;
import com.alkemytech.sophia.royalties.currency.CurrencyConverter;
import com.alkemytech.sophia.royalties.drools.KnowledgeBaseService;
import com.alkemytech.sophia.royalties.jdbc.McmdbDataSource;
import com.alkemytech.sophia.royalties.nosql.IpiSocNoSqlDb;
import com.alkemytech.sophia.royalties.nosql.OperaNoSqlDb;
import com.alkemytech.sophia.royalties.nosql.UuidCodiciNoSqlDb;
import com.alkemytech.sophia.royalties.region.DvdRegions;
import com.alkemytech.sophia.royalties.region.GoogleMapsRegions;
import com.alkemytech.sophia.royalties.role.RoleCatalog;
import com.alkemytech.sophia.royalties.scheme.IrregularityCatalog;
import com.alkemytech.sophia.royalties.scheme.RoyaltySchemeService;
import com.alkemytech.sophia.royalties.scheme.TemporaryWorkCatalog;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.util.StringUtils;
import com.google.common.base.Strings;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Scopes;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class PricingAndClaim extends MicroService {
	
	private static final Logger logger = LoggerFactory.getLogger(PricingAndClaim.class);

	protected static class GuiceModuleExtension extends GuiceModule {

		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// amazon service(s)
			bind(S3.class)
				.in(Scopes.SINGLETON);
			bind(SQS.class)
				.in(Scopes.SINGLETON);
			// http
			bind(HTTP.class)
				.toInstance(new HTTP(configuration));
			// data source(s)
			bind(DataSource.class)
				.annotatedWith(Names.named("MCMDB"))
				.to(McmdbDataSource.class)
				.asEagerSingleton();
			// message deduplicator(s)
			bind(MessageDeduplicator.class)
				.to(McmdbMessageDeduplicator.class)
				.in(Scopes.SINGLETON);
			// other binding(s)
			bind(Configuration.class)
				.asEagerSingleton();
			bind(CountryService.class)
				.to(JsonCountryService.class)
				.asEagerSingleton();
			bind(RoleCatalog.class)
				.asEagerSingleton();
			bind(TemporaryWorkCatalog.class)
				.asEagerSingleton();
			bind(IrregularityCatalog.class)
				.asEagerSingleton();
			bind(RoyaltySchemeService.class)
				.asEagerSingleton();
			bind(CurrencyConverter.class)
				.asEagerSingleton();
			bind(KnowledgeBaseService.class)
				.asEagerSingleton();
			bind(PriceModels.class)
				.asEagerSingleton();
			bind(PricingAndClaim.class)
				.asEagerSingleton();
			bind(FilenameConfiguration.class)
					.asEagerSingleton();
			bind(PercClaimService.class)
					.asEagerSingleton();
		}
		
	}

	public static void main(String[] args) {
		try {
			final PricingAndClaim instance = Guice
					.createInjector(new GuiceModuleExtension(args,
							"/pricing-and-claim.properties"))
					.getInstance(PricingAndClaim.class)
					.startup();
			try {
				instance.process(args);
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}

	private final SQS sqs;
	private final HTTP http;
	private final CountryService countryService;
	private final RoleCatalog roleCatalog;
	private final TemporaryWorkCatalog temporaryWorkCatalog;
	private final IrregularityCatalog irregularityCatalog;
	private final RoyaltySchemeService royaltySchemeService;
	private final CurrencyConverter currencyConverter;
	private final KnowledgeBaseService knowledgeBaseService;
	private final PriceModels priceModels;
	private final MessageDeduplicator deduplicator;
	private final FilenameConfiguration filenameConfiguration;
	// R9
	private final PercClaimService percClaimService;
	private Map<String, PercClaimConfig> percClaimConfigs;
	private Map<String, PercClaimResult> percClaimResults = new ConcurrentHashMap<>();;

	private IpiSocNoSqlDb ipiNoSqlDb;
	private UuidCodiciNoSqlDb codiciNoSqlDb;
	private final Map<String, OperaNoSqlDb> operaNoSqlDbs;
	private final String ESITO_OK = "OK";
	private final String ESITO_KO = "KO";
	private final String SEPARATOR = "_||_";

	@Inject
	protected PricingAndClaim(Configuration configuration,
			@Named("charset") Charset charset,
			S3 s3, SQS sqs, HTTP http,
			CountryService countryService,
			RoleCatalog roleCatalog,
			TemporaryWorkCatalog temporaryWorkCatalog,
			IrregularityCatalog irregularityCatalog,
			RoyaltySchemeService royaltySchemeService,
			CurrencyConverter currencyConverter,
			PriceModels priceModels,
			KnowledgeBaseService knowledgeBaseService,
			FilenameConfiguration filenameConfiguration,
			// R9
			PercClaimService percClaimService,
			MessageDeduplicator deduplicator) {
		super(configuration, charset, s3);
		this.sqs = sqs;
		this.http = http;
		this.countryService = countryService;
		this.roleCatalog = roleCatalog;
		this.temporaryWorkCatalog = temporaryWorkCatalog;
		this.irregularityCatalog = irregularityCatalog;
		this.royaltySchemeService = royaltySchemeService;
		this.currencyConverter = currencyConverter;
		this.knowledgeBaseService = knowledgeBaseService;
		this.priceModels = priceModels;
		this.deduplicator = deduplicator;
		this.operaNoSqlDbs = new HashMap<>();
		this.filenameConfiguration = filenameConfiguration;
		// R9
		this.percClaimService = percClaimService;
	}
	
	public PricingAndClaim startup() throws IOException {
		if ("true".equalsIgnoreCase(configuration.getProperty("pricing_and_claim.locked", "true"))) {
			throw new IllegalStateException("application locked");
		}
		super.startup();
		sqs.startup();
		http.startup();
		return this;
	}

	public PricingAndClaim shutdown() throws IOException {
		sqs.shutdown();
		http.shutdown();
		super.shutdown();
		return this;
	}
	
	public void process(String[] args) throws Exception {
		final int bindPort = Integer.parseInt(configuration.getProperty("pricing_and_claim.bind_port",
				configuration.getProperty("default.bind_port", "0")));
		// bind lock tcp port
		try (ServerSocket socket = new ServerSocket(bindPort)) {
			logger.debug("socket bound to {}", socket.getLocalSocketAddress());
			
			// standalone mode
			if (Arrays.asList(args).contains("standalone") ||
					"true".equalsIgnoreCase(configuration
							.getProperty("pricing_and_claim.standalone", "false"))) {
				final JsonObject input = GsonUtils.fromJson(configuration
						.getProperty("pricing_and_claim.standalone.message_body"), JsonObject.class);
				final JsonObject output = new JsonObject();
				logger.debug("standalone input {}", new GsonBuilder()
						.setPrettyPrinting().create().toJson(input));
				processMessage(input, output);
				logger.debug("standalone output {}", new GsonBuilder()
						.setPrettyPrinting().create().toJson(output));
				return;
			}
			
			// sqs message pump
			final SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration
					.getProperties(), "pricing_and_claim.sqs");
//			final MessageDeduplicator deduplicator = new TrieMessageDeduplicator(new File(configuration
//					.getProperty("pricing_and_claim.sqs.deduplicator_folder")));
			sqsMessagePump.pollingLoop(deduplicator, new SqsMessagePump.Consumer() {

				private JsonObject output;
				private JsonObject error;

				@Override
				public JsonObject getStartedMessagePayload(JsonObject message) {
					output = null;
					error = null;
					return SqsMessageHelper.formatContext();
				}

				@Override
				public boolean consumeMessage(JsonObject message) {
					try {
						output = new JsonObject();
						processMessage(GsonUtils
								.getAsJsonObject(message, "body"), output);
						error = null;
						return true;
					} catch (Exception e) {
						output = null;
						error = SqsMessageHelper.formatError(e);
						return false;
					}
				}

				@Override
				public JsonObject getCompletedMessagePayload(JsonObject message) {
					return output;
				}

				@Override
				public JsonObject getFailedMessagePayload(JsonObject message) {
					return error;
				}

			});
		}
	}
	
	private DsrMetadata parseSqsMessageBody(JsonObject input) throws IOException {
		final DsrMetadata metadata = new DsrMetadata();
		// common
		metadata.setYear(GsonUtils.getAsString(input, "year"));
		metadata.setMonth(GsonUtils.getAsString(input, "month"));
		metadata.setDay(GsonUtils.getAsString(input, "day", configuration
				.getProperty("pricing_and_claim.reference_day", "01")));
		metadata.setIdDsr(GsonUtils.getAsString(input, "idDsr"));
		metadata.setDsp(GsonUtils.getAsString(input, "dsp"));
		metadata.setDspCode(GsonUtils.getAsString(input, "dspCode"));
		metadata.setCountry(GsonUtils.getAsString(input, "country"));
		metadata.setTerritory(GsonUtils.getAsString(input, "territory"));
		// pricing
		metadata.setTotalAmount(new BigDecimal(GsonUtils.getAsString(input, "totalAmount", "0")));
		metadata.setTotalSubscriptions(Long.parseLong(GsonUtils.getAsString(input, "totalSubscriptions", "0")));
		metadata.setTotalSales(new BigDecimal(GsonUtils.getAsString(input, "totalSales", "0")));
		metadata.setCurrency(GsonUtils.getAsString(input, "currency", "EUR"));
		metadata.setBusinessOffer(GsonUtils.getAsString(input, "businessOffer"));
		metadata.setPricingModel("reclami".equals(metadata.getDsp()) ? "MASTERLIST" : GsonUtils.getAsString(input, "priceModel"));
		metadata.setDecisionTableUrl(GsonUtils.getAsString(input, "decisionTableUrl"));
		// claim
		metadata.setClaimPeriodType(GsonUtils.getAsString(input, "claimPeriodType", "M"));
		metadata.setPeriod(Integer.parseInt(GsonUtils.getAsString(input, "period", 
				GsonUtils.getAsString(input, "day", configuration.getProperty("pricing_and_claim.reference_day", "01")))));
		metadata.setPeriodType(GsonUtils.getAsString(input, "periodType", "month"));
		metadata.setPeriodStartDate(GsonUtils.getAsString(input, "periodStartDate"));
		metadata.setPeriodEndDate(GsonUtils.getAsString(input, "periodEndDate"));
		metadata.setSplitDEM(Float.parseFloat(GsonUtils.getAsString(input, "split_perf")));
		metadata.setSplitDRM(Float.parseFloat(GsonUtils.getAsString(input, "split_mech")));
		metadata.setCcidId(new BigInteger(GsonUtils.getAsString(input, "ccid_id")));
		metadata.setBaseRefId(new BigInteger(GsonUtils.getAsString(input, "baseRefId")));
		metadata.setRoyaltyCurrency(GsonUtils.getAsString(input, "royalty_currency", "EUR"));
		metadata.setTradingBrand(GsonUtils.getAsString(input, "trading_brand"));
		metadata.setSender(GsonUtils.getAsString(input, "sender"));
		metadata.setReceiver(GsonUtils.getAsString(input, "receiver"));
		metadata.setAppliedTariff(GsonUtils.getAsString(input, "applied_tariff"));
		metadata.setParamAppliedTariff(GsonUtils.getAsString(input, "PARAM_APPLIED_TARIFF", "1.0"));
		metadata.setParamAux(GsonUtils.getAsString(input, "PARAM_AUX"));
		metadata.setParamCommercialModel(GsonUtils.getAsString(input, "COMMERCIAL_MODEL", "Unknown")); //valore attuale
		metadata.setParamClaimType(GsonUtils.getAsString(input, "CLAIM_TYPE","NI")); //valore attuale
		metadata.setOriginalCurrency(GsonUtils.getAsString(input, "original_price_basis_currency", "EUR"));
		metadata.setClaimUtilizationType(GsonUtils.getAsString(input, "claimUtilizationType"));
		metadata.setEncoded("true".equalsIgnoreCase(GsonUtils.getAsString(input, "encoded", "true")));
		metadata.setNotEncoded("true".equalsIgnoreCase(GsonUtils.getAsString(input, "notEncoded", "false")));
		metadata.setEncodedProvisional("true".equalsIgnoreCase(GsonUtils.getAsString(input, "encodedProvisional", "true")));
		metadata.setConversionRate(new BigDecimal(GsonUtils.getAsString(input, "conversion_rate", "1.0")));
		metadata.setCcidVersion(GsonUtils.getAsString(input, "ccidVersion"));
		metadata.setServiceType(GsonUtils.getAsString(input, "service_type"));
		metadata.setUseType(GsonUtils.getAsString(input, "use_type"));
		metadata.setWorkCodeType(GsonUtils.getAsString(input, "work_code_type"));
		metadata.setTipoRepertorio(GsonUtils.getAsString(input, "tipo_repertorio", "M"));
		// societies
		metadata.setTenant(parseSocietyInfo("pricing_and_claim.sqs.json.collecting_society",
				GsonUtils.getAsJsonObject(input, "tenant"), roleCatalog));
		final JsonArray mandatorsJson = GsonUtils.getAsJsonArray(input, "mandators");
		if (null != mandatorsJson) {
			for (JsonElement mandatorJson : mandatorsJson) {
				metadata.addMandator(parseSocietyInfo("pricing_and_claim.sqs.json.collecting_society",
						mandatorJson, roleCatalog));
			}
		}
		// R9
		metadata.setIdUtilizationType(GsonUtils.getAsString(input, "idUtilizationType"));
		return metadata;
	}
	
	private void processMessage(JsonObject input, JsonObject output) throws IOException, SQLException, InterruptedException {

//		{
//		    "dsp": "amazon", 
//		    "dspCode": "AMA", 
//		    "year": "2019", 
//		    "month": "01", 
//		    "country": "ESP", 
//		    "territory": "ES", 
//		    "idDsr": "Amazon-Sales-DDEX-ES-2016-11", 
//		    "totalSubscriptions": 0, 
//		    "totalSales": 178294, 
//		    "totalAmount": 97618.55 
//		    "currency": "EUR", 
//		    "businessOffer": "Amazon-Sales-DDEX", 
//		    "priceModel": "DWN", 
//		    "decisionTableUrl": "s3://siae-sophia-datalake/debug/rules/amazon/DWN/20160101/AmazonDownload_201601.xls", 
//		    "periodType": "month", 
//		    "period": 1, 
//		    "split_perf": "0.5", 
//		    "split_mech": "0.5", 
//		    "claimPeriodType": "M", 
//		    "ccid_id": 198, 
//		    "baseRefId": 198000000000000, 
//		    "idUtilizationType": "DWN", 
//		    "royalty_currency": "EUR", 
//		    "periodEndDate": "20190131", 
//		    "sliceDspCode": "ALL", 
//		    "trading_brand": "*", 
//		    "periodStartDate": "20190101", 
//		    "commercialOffer": "Amazon-Sales-DDEX", 
//		    "sender": "SIAE", 
//		    "receiver": "amazon", 
//		    "applied_tariff": "DWIP", 
//		    "work_code_type": "SIAE_WORK_CODE", 
//		    "original_price_basis_currency": "EUR", 
//		    "claimUtilizationType": "O", 
//		    "encoded": true, 
//		    "notEncoded": false, 
//		    "encodedProvisional": true, 
//		    "conversion_rate": "100000", 
//		    "ccidVersion": "14", 
//		    "service_type": "PDS", 
//		    "use_type": "PD", 
//		    "PARAM_APPLIED_TARIFF": "10.0", 
//		    "PARAM_AUX": "", 
//		    "sliceMonth": "1" 
//		    "tenant": { 
//			    "society": "SIAE", 
//			    "territory": "IT", 
//			    "black_list_url": "s3://siae-sophia-datalake/${default.environment}/schemi-riparto-v2/CONFIG/blacklist-IT-201903.csv" 
//		    } 
//		    "mandators": [
//				{ 
//				    "society": "UCRM-ADA", 
//				    "territory": "RO", 
//				    "black_list_url": "s3://siae-sophia-datalake/${default.environment}/schemi-riparto-v2/CONFIG/blacklist-RO-201903.csv" 
//			    } 
//		    ],
//	 	   "forced": false
//		}


		logger.debug("processing princing and claim message");
		final long startTimeMillis = System.currentTimeMillis();

		final boolean dryRun = Boolean.parseBoolean(configuration.getProperty("pricing_and_claim.dry_run", "false"));

		// parse input json message
		final DsrMetadata dsrMetadata = parseSqsMessageBody(input);
		logger.debug("dsrMetadata {}", dsrMetadata);
		if (Strings.isNullOrEmpty(dsrMetadata.getYear()) ||
				Strings.isNullOrEmpty(dsrMetadata.getMonth()) ||
				Strings.isNullOrEmpty(dsrMetadata.getIdDsr()) ||
				Strings.isNullOrEmpty(dsrMetadata.getDsp()) ||
				Strings.isNullOrEmpty(dsrMetadata.getDspCode()) ||
				Strings.isNullOrEmpty(dsrMetadata.getPricingModel()) ||
				Strings.isNullOrEmpty(dsrMetadata.getDecisionTableUrl()) ||
				BigInteger.ZERO.equals(dsrMetadata.getTotalSales()) ||
				BigInteger.ZERO.equals(dsrMetadata.getCcidId()) ||
				BigInteger.ZERO.equals(dsrMetadata.getBaseRefId()) ||
				dsrMetadata.getSplitDEM() + dsrMetadata.getSplitDRM() < 0.999f ||
				Strings.isNullOrEmpty(dsrMetadata.getCcidVersion()) ||
				Strings.isNullOrEmpty(dsrMetadata.getServiceType()) ||
				Strings.isNullOrEmpty(dsrMetadata.getUseType()) ||
				Strings.isNullOrEmpty(dsrMetadata.getAppliedTariff()) ||
				null == dsrMetadata.getTenant()) {
			throw new IllegalArgumentException("invalid input message");
		}
		// fix parsed json values
		final String period = fixPeriod(dsrMetadata.getMonth());
		dsrMetadata.setYear(String.format("%04d", Integer.parseInt(dsrMetadata.getYear())));
		dsrMetadata.setMonth(fixMonth(dsrMetadata.getMonth()));
		dsrMetadata.setDay(String.format("%02d", Integer.parseInt(dsrMetadata.getDay())));
	
		// ccid version
		final int ccidVersion;
		try {
			ccidVersion = Integer.parseInt(dsrMetadata.getCcidVersion());
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("invalid CCID version: " + dsrMetadata.getCcidVersion());
		}

		// ccid delimiter
		final char ccidDelimiter;
		if (13 == ccidVersion) {
			ccidDelimiter = CcidV13Printer.DELIMITER;
		} else if (14 == ccidVersion) {
			ccidDelimiter = CcidV14Printer.DELIMITER;
		} else {
			throw new IllegalArgumentException("invalid CCID version: " + ccidVersion);
		}			
		
		// configuration tags
		configuration.setTag("{year}", dsrMetadata.getYear());
		configuration.setTag("{month}", dsrMetadata.getMonth());
		configuration.setTag("{day}", dsrMetadata.getDay());
		configuration.setTag("{period}", period);
		configuration.setTag("{dsr}", dsrMetadata.getIdDsr());
		configuration.setTag("{dsp}", dsrMetadata.getDsp());
		boolean excludeClaim0 = filenameConfiguration.excludeClaim0();
		// FIX RIC0028169 (SCTASK0010340)
		configuration.setTag("{ccidVersion}", String.valueOf(ccidVersion));
				
		// check DSR invoicing status
		logger.debug("checking DSR invoice status");
		if (isDsrInvoiced()) {
			throw new IllegalStateException("DSR already invoiced: " + dsrMetadata.getIdDsr());
		}

		// configuration
//		final boolean debug = "true".equalsIgnoreCase(configuration
//				.getProperty("pricing_and_claim.debug", configuration.getProperty("default.debug")));
		final File homeFolder = new File(configuration
				.getProperty("default.home_folder"));
		logger.debug("homeFolder {}", homeFolder);

		// initialize country service
		initializeCountryService(countryService, homeFolder);

		// alpha-2 (territory) and alpha-3 (country) ISO 3166-1 codes
		dsrMetadata.setTerritory(countryService.getAlpha2(dsrMetadata.getCountry())); // enforce ISO 3166-1 alpha-2
		dsrMetadata.setCountry(countryService.getAlpha3(dsrMetadata.getCountry())); // enforce ISO 3166-1 alpha-3
		dsrMetadata.setRegion(GoogleMapsRegions.getRegion(dsrMetadata.getTerritory()));
		dsrMetadata.setSubregion(GoogleMapsRegions.getSubregion(dsrMetadata.getTerritory()));
		dsrMetadata.setDvdRegion(DvdRegions.getRegion(dsrMetadata.getTerritory()));
		logger.debug("fixed dsrMetadata {}", dsrMetadata);

		// more configuration tags
		configuration.setTag("{country}", dsrMetadata.getCountry());
		configuration.setTag("{territory}", dsrMetadata.getTerritory());
		
		// initialize role catalog
		roleCatalog.load(s3, configuration.getProperty("role_catalog.csv_url"));	
		
		// initialize temporary work catalog
		temporaryWorkCatalog.load(s3, configuration.getProperty("temporary_work_catalog.csv_url"));	

		// initialize irregularity catalog
		irregularityCatalog.load(s3, configuration.getProperty("irregularity_catalog.csv_url"));	

		// initialize currency converter
		currencyConverter.reloadRates();

		// R9
		configuration.setTag("{idUtilizationType}", dsrMetadata.getIdUtilizationType());
		configuration.setTag("{businessOffer}", dsrMetadata.getBusinessOffer());
		this.percClaimConfigs = percClaimService.getPercClaimConfigs(dsrMetadata);

		// drools knowledge base from decision table
		final File xlsFile = File.createTempFile("__tmp__", ".xls", homeFolder);
		xlsFile.deleteOnExit();
		if (!s3.download(new S3.Url(dsrMetadata.getDecisionTableUrl()), xlsFile)) {
			throw new IOException("file download error: " + dsrMetadata.getDecisionTableUrl());
		}
		final KnowledgeBase knowledgeBase = knowledgeBaseService
				.getKnowledgeBaseFromXls(xlsFile);
		
		// input url
		final String inputFolderUrl = configuration.getProperty("pricing_and_claim.input.folder_url");
		logger.debug("inputFolderUrl {}", inputFolderUrl);

		// list and download files
		final Pattern pattern = Pattern.compile(configuration
				.getProperty("pricing_and_claim.input.pattern", ".*\\.csv"));
		final List<String> inputUrls = new ArrayList<>();
		for (S3ObjectSummary s3summary : s3.listObjects(new S3.Url(inputFolderUrl))) {
			if (pattern.matcher(s3summary.getKey().toLowerCase()).matches()) {
				inputUrls.add(new S3.Url(s3summary.getBucketName(),
						s3summary.getKey()).toString());
			}
		}
		if (inputUrls.isEmpty()) {
			throw new IllegalArgumentException("no file found in folder: " + inputFolderUrl);
		}
		
		// sort input urls by path
		Collections.sort(inputUrls, new Comparator<String>() {
			@Override
			public int compare(String left, String right) {
				return left.compareToIgnoreCase(right);
			}
		});
//		logger.debug("inputUrls {}", inputUrls);
		
		// files and folders to keep
		final Set<String> dataFilesToKeep = new HashSet<>();
		
		// initialize codici nosql to latest version
		dataFilesToKeep.add(configuration.getProperty("pricing_and_claim.codici_nosql.latest_file"));
		dataFilesToKeep.add(configuration.getProperty("pricing_and_claim.codici_nosql.home_folder"));
		downloadAndExtractLatest("pricing_and_claim", "pricing_and_claim.codici_nosql");
		codiciNoSqlDb = new UuidCodiciNoSqlDb(configuration
				.getProperties(), "pricing_and_claim.codici_nosql");

		// initialize ipi nosql to latest version
		dataFilesToKeep.add(configuration.getProperty("pricing_and_claim.ipi_nosql.latest_file"));
		dataFilesToKeep.add(configuration.getProperty("pricing_and_claim.ipi_nosql.home_folder"));
		downloadAndExtractLatest("pricing_and_claim", "pricing_and_claim.ipi_nosql");
		ipiNoSqlDb = new IpiSocNoSqlDb(configuration
				.getProperties(), "pricing_and_claim.ipi_nosql");

		// initialize tenant nosql to latest version
		configuration.setTag("{society}", dsrMetadata.getTenant().getSociety());
		dataFilesToKeep.add(configuration.getProperty("pricing_and_claim.society_nosql.latest_file"));
		dataFilesToKeep.add(configuration.getProperty("pricing_and_claim.society_nosql.home_folder"));
		downloadAndExtractLatest("pricing_and_claim", "pricing_and_claim.society_nosql");
		operaNoSqlDbs.put(dsrMetadata.getTenant().getSociety(), new OperaNoSqlDb(configuration
				.getProperties(), "pricing_and_claim.society_nosql"));
		configuration.removeTag("{society}");

		// initialize mandators nosql to latest version
		for (SocietyInfo mandator : dsrMetadata.getMandators()) {
			configuration.setTag("{society}", mandator.getSociety());
			dataFilesToKeep.add(configuration.getProperty("pricing_and_claim.society_nosql.latest_file"));
			dataFilesToKeep.add(configuration.getProperty("pricing_and_claim.society_nosql.home_folder"));
			downloadAndExtractLatest("pricing_and_claim", "pricing_and_claim.society_nosql");
			operaNoSqlDbs.put(mandator.getSociety(), new OperaNoSqlDb(configuration
					.getProperties(), "pricing_and_claim.society_nosql"));
			configuration.removeTag("{society}");
		}

		// cleanup data folder (if exceeding max size)
		final long dataFolderMaxSize = TextUtils
				.parseLongSize(configuration.getProperty("pricing_and_claim.data_limit", "20Gb"));
		final File dataFolder = new File(configuration
				.getProperty("pricing_and_claim.data_folder"));
		logger.debug("dataFolder {}", dataFolder);
		logger.debug("dataFolderMaxSize {}", dataFolderMaxSize);
		logger.debug("dataFilesToKeep {}", dataFilesToKeep);
		cleanupFolder(dataFolder, dataFolderMaxSize, dataFilesToKeep);

		final ClaimHelper claimHelper = new ClaimHelper(configuration,
				ipiNoSqlDb, codiciNoSqlDb, operaNoSqlDbs, royaltySchemeService);
		
		
		// TODO multithreading...
				
		
		// DSR statistics
		final DsrStats dsrStats;
		
		// temporary ccid file
		final File ccidFile = File.createTempFile("__tmp__", ".csv", homeFolder);
		ccidFile.deleteOnExit();

		// temporary extended dsr file
		final File extendedDsrFile = File.createTempFile("__tmp__", ".csv", homeFolder);
		extendedDsrFile.deleteOnExit();

		//R2011 instanzio la classe che mi permetterà di ottenere la configurazione per il DSP corrente per l'eliminazione dei caratteri
		MMSpecialCharCcid mmscCcid = new MMSpecialCharCcid(configuration,dsrMetadata.getDsp());
		
		try (final OutputStream ccidOut = new CompressionAwareFileOutputStream(ccidFile);
				final Writer ccidWriter = new OutputStreamWriter(ccidOut, charset);
				final CSVPrinter ccidCsvPrinter = new CSVPrinter(ccidWriter, CSVFormat.EXCEL.withDelimiter(ccidDelimiter));
				final OutputStream dsrOut = new CompressionAwareFileOutputStream(extendedDsrFile);
				final Writer dsrWriter = new OutputStreamWriter(dsrOut, charset);
				final CSVPrinter dsrCsvPrinter = new CSVPrinter(dsrWriter, CSVFormat.EXCEL.withDelimiter(';'))) {
			
			// extended dsr printer
			final ExtendedDsrPrinter dsrPrinter = new ExtendedDsrPrinter(dsrCsvPrinter);
			
			// ccid printer
			final CcidPrinter ccidPrinter;
			if (13 == ccidVersion) {
				ccidPrinter = new CcidV13Printer(ccidCsvPrinter);
				dsrStats = new DsrStats(value -> value.setScale(4, BigDecimal.ROUND_HALF_UP));
			} else if (14 == ccidVersion) {
				ccidPrinter = new CcidV14Printer(ccidCsvPrinter);
				dsrStats = new DsrStats(Function.identity());
			} else {
				throw new IllegalArgumentException("invalid CCID version: " + ccidVersion);
			}
			

			
			// print CCID rows
			if (priceModels.isSingleLineOutput(dsrMetadata)) { // streaming (parse only one line)
				
				// dummy result listener

				final PricingResult pricingResult = new PricingResult(priceModels) {
					
					@Override
					public void onPricingResult(DsrMetadata dsrMetadata) throws PricingException {
//						logger.debug("onPricingResult: dsrMetadata {}", dsrMetadata);
					}

					@Override
					public void onPricingResult(DsrLine dsrLine) throws PricingException {
						throw new PricingException("unespected line result");
					}
					
				};
				
				// parse first line of csv file
				if (StringUtils.isNullOrEmpty(dsrMetadata.getBusinessOffer()) ||
						StringUtils.isNullOrEmpty(dsrMetadata.getCurrency()) ||
						StringUtils.isNullOrEmpty(dsrMetadata.getRegion())) {
					boolean parsed = false;
					// search for first non empty file
					for (String inputUrl : inputUrls) {
						logger.debug("inputUrl {}", inputUrl);
						
						// input file
						final File inputFile = File.createTempFile("__tmp__",
								"__" + inputUrl.substring(1 + inputUrl.lastIndexOf('/')), homeFolder);
						inputFile.deleteOnExit();
						logger.debug("inputFile {}", inputFile);
						
						// download input file
						if (!s3.download(new S3.Url(inputUrl), inputFile)) {
							throw new IOException("file download error: " + inputUrl);
						}
						
						// parse first non empty file
						if (inputFile.length() > 0L) {
							try (final InputStream in = new CompressionAwareFileInputStream(inputFile);
									final Reader reader = new InputStreamReader(in, charset)) {
								parseFirstLine(dsrMetadata, pricingResult, knowledgeBase, reader);
								parsed = true;
							}
							inputFile.delete();
							break;
						}
						inputFile.delete();
					}
					if (!parsed) {
						throw new PricingException("empty input (zero dsr rows)");
					}
				} else {
					doNotParse(dsrMetadata, pricingResult, knowledgeBase);
				}

				// print CCID header
				ccidPrinter.printHeader(dsrMetadata);
				// loop on input url(s)
				for (String inputUrl : inputUrls) {
					extendCsvFile(inputUrl, homeFolder, new CsvExtender() {
						
						@Override
						public void extend(final CSVParser dsrParser) throws IOException {
							// parse all lines and extend with pricing and claim info
							parseAllLines(claimHelper, dsrMetadata, dsrStats, dsrParser, dsrPrinter, ccidPrinter,excludeClaim0,mmscCcid);
						}
						
					});
				}
				
			} else { // download (parse all lines)

				final AtomicBoolean firstLine = new AtomicBoolean(true);
				
				// loop on input url(s)
				for (String inputUrl : inputUrls) {
					extendCsvFile(inputUrl, homeFolder, new CsvExtender() {
						
						@Override
						public void extend(final CSVParser dsrParser) throws IOException {
							
							// event driven result listener
							final PricingResult pricingResult = new PricingResult(priceModels) {
								
								@Override
								public void onPricingResult(DsrMetadata dsrMetadata) throws PricingException {
									throw new PricingException("unespected metadata result");
								}

								@Override
								public void onPricingResult(DsrLine dsrLine) throws PricingException {
//									logger.debug("onPricingResult: dsrLine {}", dsrLine);							
									try {

										if(firstLine.get()){
											//print header
											// print CCID header
											dsrMetadata.setProRata(dsrLine.getProRata()!= null? dsrLine.getProRata().toPlainString(): "0");
											ccidPrinter.printHeader(dsrMetadata);
											firstLine.set(false);
										}

										// claim
										final ClaimResult claimResult = claimHelper
												.claim(dsrMetadata, dsrLine);
										// print extended DSR columns
										dsrPrinter.printRow(dsrLine, claimResult);

										// print CCID row

										ccidPrinter.printRow(dsrMetadata, dsrLine, claimResult,excludeClaim0,mmscCcid);
										
										// update dsr stats
										dsrStats.update(dsrMetadata, dsrLine, claimResult);

										// R9
										if (!percClaimConfigs.isEmpty() && claimResult!=null) {
											populatePercClaimResultRecords(dsrMetadata, claimResult,dsrLine);
										}

									} catch (IOException e) {
										throw new PricingException(e);
									}
								}
								
							};

							// feed all lines to drools session
							parseAllLines(dsrMetadata, pricingResult, knowledgeBase, dsrParser);
						}
						
					});
				}
							
			}
			
			// print CCID footer
			ccidPrinter.printTrailer(dsrMetadata);
			
			// output stats
			output.addProperty("extendedDsrRows", dsrPrinter.getPrintedLines());
			output.addProperty("ccidRows", ccidPrinter.getPrintedLines());
		}

		// R9
		if (!percClaimConfigs.isEmpty()) {
			// logger.debug("percClaimResults: {}",percClaimResults);
			if (percClaimResults.isEmpty()) {
				PercClaimResult percClaimResult = new PercClaimResult();
				percClaimResult.setIdConfig(-1);
				percClaimResult.setIdDsr(dsrMetadata.getIdDsr());
				percClaimResult.setEsito(ESITO_OK);
				percClaimResults.put(dsrMetadata.getIdDsr(),percClaimResult);
			}
			percClaimService.deletePercClaimResultByIdDsr();
			percClaimService.insertPercClaimResult(percClaimResults);
		}

		// extended dsr filename
		final String extendedDsrFilename = String.format("DSR_%s_%s_%s_%s_%s.csv",
				dsrMetadata.getIdDsr(),
				dsrMetadata.getYear(),
				dsrMetadata.getMonth(),
				dsrMetadata.getTerritory(),
				dsrMetadata.getPeriodStartDate());
		logger.debug("extendedDsrFilename {}", extendedDsrFilename);
		
		// extended dsr url
		final String extendedDsrUrl = configuration
				.getProperty("pricing_and_claim.output_url")
				.replace("{filename}", extendedDsrFilename);
		logger.debug("extendedDsrUrl {}", extendedDsrUrl);

		String ccidFilename = "";
		if (dsrMetadata.getCcidVersion().equals("14") && filenameConfiguration.isFileNameConfigured()){
			ccidFilename = filenameConfiguration.getFilenamingCustom(dsrMetadata);
		} else {
			ccidFilename = filenameConfiguration.getFilenamingDefault(dsrMetadata);
		}

		// ccid url
		final String ccidUrl = configuration
				.getProperty("pricing_and_claim.ccid_url")
				.replace("{filename}", ccidFilename)
				.replace("{ccidVersion}", dsrMetadata.getCcidVersion());
		logger.debug("ccidUrl {}", ccidUrl);

		if(! dryRun) {

			// upload extended dsr file
			if (!s3.upload(new S3.Url(extendedDsrUrl), extendedDsrFile)) {
				throw new IOException("file upload error: " + extendedDsrUrl);
			}
			// upload ccid file
			if (!s3.upload(new S3.Url(ccidUrl), ccidFile)) {
				throw new IOException("file upload error: " + ccidUrl);
			}

			//TODO: adding hive partition

		}else{
			FileUtils.copy(ccidFile,new File(ccidFile+".ccid"));
			logger.debug("file copied in {}", ccidFile+".ccid");
		}
		
		// output values
		output.addProperty("period", dsrMetadata.getReferencePeriod());
		output.addProperty("startTime", DateFormat
				.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM)
					.format(new Date(startTimeMillis)));
		output.addProperty("totalDuration", TextUtils
				.formatDuration(System.currentTimeMillis() - startTimeMillis));
		output.addProperty("country", dsrMetadata.getCountry());
		output.addProperty("territory", dsrMetadata.getTerritory());
		output.addProperty("ccidUrl", ccidUrl);
		output.addProperty("extendedDsrUrl", extendedDsrUrl);
		logger.debug("AMOUNT MECH "+dsrMetadata.getSumAmountMech());
		logger.debug("AMOUNT PERF "+dsrMetadata.getSumAmountPerf());
		logger.debug("TOTALE VALORE "+new BigDecimal(String.valueOf(dsrMetadata.getSumAmountMech().add(dsrMetadata.getSumAmountPerf()))));
		output.add("statistics", dsrStats.toJsonObject(dsrMetadata,currencyConverter));

		logger.info("message processed in " + TextUtils
				.formatDuration(System.currentTimeMillis() - startTimeMillis));
	}


	private boolean isDsrInvoiced() {
		try {
			final HTTP.Response response = http.get(configuration
					.getProperty("pricing_and_claim.dsr_invoiced_url"));
			logger.debug("isDsrInvoiced: response status  {}", response.status);
			logger.debug("isDsrInvoiced: response body {}", response.getBodyAsString(charset));
			if (response.isOK()) {
				final JsonObject responseBody = GsonUtils
						.fromJson(response.getBodyAsString(charset),
								JsonObject.class);
				if (null != responseBody) {
					return "true".equalsIgnoreCase(GsonUtils.getAsString(responseBody, "invoiced"));
				}
			}
		} catch (Exception e) {
			logger.error("isDsrInvoiced", e);
		}
		return false;
	}
	
	private interface CsvExtender {
		
		public void extend(CSVParser dsrParser) throws IOException;

	}

	private void extendCsvFile(String inputUrl, File homeFolder, CsvExtender csvExtender) throws IOException {
		logger.debug("extendCsvFile: inputUrl {}", inputUrl);

		// filename
		final String filename = inputUrl.substring(1 + inputUrl.lastIndexOf('/'));
//		logger.debug("extendCsvFile: filename {}", filename);

		// input file
		final File inputFile = File.createTempFile("__tmp__", "__" + filename, homeFolder);
		inputFile.deleteOnExit();
//		logger.debug("extendCsvFile: inputFile {}", inputFile);

		// download input file
		if (!s3.download(new S3.Url(inputUrl), inputFile)) {
			throw new IOException("file download error: " + inputUrl);
		}

		// process file
		try (final InputStream in = new CompressionAwareFileInputStream(inputFile);
				final Reader reader = new InputStreamReader(in, charset);
				final CSVParser dsrParser = new CSVParser(reader, CSVFormat.newFormat(';'))) {
			
			csvExtender.extend(dsrParser);
			
		}

		// delete input file
		inputFile.delete();
	}

	private void doNotParse(DsrMetadata dsrMetadata, PricingResult pricingResult, KnowledgeBase knowledgeBase) {
//		logger.debug("doNotParse");

		final long startTimeMillis = System.currentTimeMillis();
		final StatefulKnowledgeSession session = knowledgeBase.newStatefulKnowledgeSession();
		session.setGlobal("result", pricingResult);
		// insert event
		session.insert(dsrMetadata);
		// fire rules
		session.fireAllRules();
		session.dispose();
		
		logger.debug("doNotParse: elapsed {}",
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));
    }
	
	private void parseFirstLine(DsrMetadata dsrMetadata, PricingResult pricingResult, KnowledgeBase knowledgeBase, Reader dsrReader) throws IOException {
//		logger.debug("parseFirstLine");

		final long startTimeMillis = System.currentTimeMillis();
		final DsrLineParser lineParser = new DsrLineParser(configuration, "pricing_and_claim.csv");
		final StatefulKnowledgeSession session = knowledgeBase.newStatefulKnowledgeSession();
		session.setGlobal("result", pricingResult);
		try (final CSVParser parser = new CSVParser(dsrReader, CSVFormat.newFormat(';'))) {
			for (CSVRecord record : parser) {
				// create event
				final DsrLine dsrLine = lineParser.parse(record);
				dsrMetadata.setCurrency(dsrLine.getCurrency());
				dsrMetadata.setTerritory(dsrLine.getTerritory());
				// inject regions
				dsrMetadata.setRegion(GoogleMapsRegions.getRegion(dsrLine.getTerritory()));
				dsrMetadata.setSubregion(GoogleMapsRegions.getSubregion(dsrLine.getTerritory()));
				dsrMetadata.setDvdRegion(DvdRegions.getRegion(dsrLine.getTerritory()));
				logger.info("parseFirstLine: dsrMetadata {}", dsrMetadata);
				// insert event
				session.insert(dsrMetadata);
				// fire rules
				session.fireAllRules();
				session.dispose();
				break;
			}
		}
		
		logger.debug("parseFirstLine: elapsed {}",
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));
    }

	private void parseAllLines(DsrMetadata dsrMetadata, PricingResult pricingResult, KnowledgeBase knowledgeBase, CSVParser dsrParser) throws IOException {
//		logger.debug("parseAllLines");

		final long startTimeMillis = System.currentTimeMillis();

		final int eventsPerSession = Integer.parseInt(configuration
				.getProperty("pricing_and_claim.drools.events_per_session", "1024"));
		final DsrLineParser lineParser = new DsrLineParser(configuration, "pricing_and_claim.csv");
		StatefulKnowledgeSession session = null;
		int events = 0;
		for (CSVRecord record : dsrParser) {
			// create event
			final DsrLine dsrLine = lineParser.parse(record);
			if (!Strings.isNullOrEmpty(dsrMetadata.getBusinessOffer())) {
				dsrLine.setBusinessOffer(dsrMetadata.getBusinessOffer()); // override DSR line
			}
			if (!Strings.isNullOrEmpty(dsrMetadata.getPricingModel())) {
				dsrLine.setPricingModel(dsrMetadata.getPricingModel()); // override DSR line
			}
			dsrLine.setTotalAmount(dsrMetadata.getTotalAmount());
			dsrLine.setTotalSales(dsrMetadata.getTotalSales());
			dsrLine.setTotalSubscriptions(dsrMetadata.getTotalSubscriptions());
			dsrLine.setCountry(dsrMetadata.getCountry());
			// inject regions
			dsrLine.setRegion(GoogleMapsRegions.getRegion(dsrLine.getTerritory()));
			dsrLine.setSubregion(GoogleMapsRegions.getSubregion(dsrLine.getTerritory()));
			dsrLine.setDvdRegion(DvdRegions.getRegion(dsrLine.getTerritory()));
//				logger.debug("parseAllLines: dsrLine {}", dsrLine);
			// create session (if not exists)
			if (null == session) {
				session = knowledgeBase.newStatefulKnowledgeSession();
				session.setGlobal("result", pricingResult);
			}
			// insert event
			session.insert(dsrLine);
			// fire rules
			if (events ++ > eventsPerSession) {
				events = 0;				
				session.fireAllRules();
				session.dispose();
				session = null; // force new session creation
			}
		}
		if (events > 0) {
			session.fireAllRules();
			session.dispose();
		}

		logger.debug("parseAllLines: elapsed {}",
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));
    }

	private void parseAllLines(ClaimHelper claimHelper, DsrMetadata dsrMetadata, DsrStats dsrStats, CSVParser dsrParser, ExtendedDsrPrinter dsrPrinter, CcidPrinter ccidPrinter, boolean excludeClaim0, MMSpecialCharCcid mmscCcid) throws IOException {
//		logger.debug("parseAllLines");
		
		final long startTimeMillis = System.currentTimeMillis();

		final DsrLineParser lineParser = new DsrLineParser(configuration, "pricing_and_claim.csv");
		for (CSVRecord record : dsrParser) {
			final DsrLine dsrLine = lineParser.parse(record);
			
			// inject dsr metadata pricing info into dsr line
			dsrLine.setPriceBasis(dsrMetadata.getPriceBasis());
			dsrLine.setOriginalPriceBasis(dsrMetadata.getOriginalPriceBasis());
			dsrLine.setRoyaltyType(dsrMetadata.getRoyaltyType());
			dsrLine.setRoyalty(dsrMetadata.getRoyalty());
			dsrLine.setRoyaltyEur(dsrMetadata.getRoyaltyEur());
			dsrLine.setProRata(dsrMetadata.getProRata()!= null? dsrMetadata.getProRata().toPlainString(): "0");

			try {
				dsrLine.setPrice(priceModels
						.computeStreamingPrice(dsrMetadata, dsrLine));
				dsrLine.setPriceEur(priceModels
						.computeStreamingPriceEur(dsrMetadata, dsrLine));
			} catch (RuntimeException e) {
				logger.error("parseAllLines: dsrMetadata {}", dsrMetadata);
				logger.error("parseAllLines: dsrLine {}", dsrLine);				
				throw e;
			}
			
			// claim
			final ClaimResult claimResult = claimHelper
					.claim(dsrMetadata, dsrLine);

			// print extended DSR columns
			dsrPrinter.printRow(dsrLine, claimResult);

			// print CCID row
			ccidPrinter.printRow(dsrMetadata, dsrLine, claimResult, excludeClaim0,mmscCcid);

			// update dsr stats
			dsrStats.update(dsrMetadata, dsrLine, claimResult);

			// R9
			if (!percClaimConfigs.isEmpty() && claimResult!=null) {
				populatePercClaimResultRecords(dsrMetadata, claimResult, dsrLine);
			}
		}
		
		logger.debug("parseAllLines: elapsed {}",
				TextUtils.formatDuration(System.currentTimeMillis() - startTimeMillis));
	}

	// R9
	private void populatePercClaimResultRecords(DsrMetadata dsrMetadata, ClaimResult claimResult, DsrLine dsrline) {

		PercClaimConfig percClaimConfig = percClaimConfigs.get(dsrline.getUuidSophia());
		if (percClaimConfig!=null) {
//			 logger.debug("percClaimConfigs {}",percClaimConfigs);
//			 logger.debug("claimResult {}",claimResult);
//			 logger.debug("dsrline {}",dsrline);
			if (claimResult.getClaimLicensorDem().setScale(2,BigDecimal.ROUND_DOWN).compareTo(percClaimConfig.getPercDem()) != 0 || claimResult.getClaimLicensorDrm().setScale(2,BigDecimal.ROUND_DOWN).compareTo(percClaimConfig.getPercDrm()) != 0) {
				PercClaimResult percClaimResult = new PercClaimResult();
				percClaimResult.setIdConfig(percClaimConfig.getId());
				percClaimResult.setIdDsr(dsrMetadata.getIdDsr());
				percClaimResult.setPercDemRes(claimResult.getClaimLicensorDem());
				percClaimResult.setPercDrmRes(claimResult.getClaimLicensorDrm());
				percClaimResult.setPercDemConf(percClaimConfig.getPercDem());
				percClaimResult.setPercDrmConf(percClaimConfig.getPercDrm());
				percClaimResult.setTransactionId(dsrline.getTransactionId()!=null ? dsrline.getTransactionId() : null);
				percClaimResult.setEsito(ESITO_KO);
				percClaimResults.put(dsrMetadata.getIdDsr()+SEPARATOR+percClaimConfig.getId()+SEPARATOR+dsrline.getTransactionId(),percClaimResult);
			}
		}

	}
	
}