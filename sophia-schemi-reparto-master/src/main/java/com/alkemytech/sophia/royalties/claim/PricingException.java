package com.alkemytech.sophia.royalties.claim;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
@SuppressWarnings("serial")
public class PricingException extends RuntimeException {

	public PricingException() {
		super();
	}

	public PricingException(String message, Throwable cause) {
		super(message, cause);
	}

	public PricingException(String message) {
		super(message);
	}

	public PricingException(Throwable cause) {
		super(cause);
	}

}
