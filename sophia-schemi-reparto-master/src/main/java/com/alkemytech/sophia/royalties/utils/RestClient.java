package com.alkemytech.sophia.royalties.utils;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class RestClient {
        public static Logger logger = LoggerFactory.getLogger(RestClient.class);
        public static class Result {

            private int statusCode;
            private JsonElement jsonElement;

            public int getStatusCode() {
                return statusCode;
            }

            public Result setStatusCode(int statusCode) {
                this.statusCode = statusCode;
                return this;
            }

            public JsonElement getJsonElement() {
                return jsonElement;
            }

            public Result setJsonElement(JsonElement jsonElement) {
                this.jsonElement = jsonElement;
                return this;
            }

            @Override
            public String toString() {
                return new GsonBuilder().setLenient()
                        .setPrettyPrinting().create().toJson(this);
            }
        }


        private static Result fromResponse(CloseableHttpResponse response) throws IOException {
            Result result = new Result()
                    .setStatusCode(response.getStatusLine().getStatusCode());
            HttpEntity responseEntity = response.getEntity();
            if (null != responseEntity) {
                try (InputStream in = responseEntity.getContent()) {
                    byte[] buffer = new byte[1024];
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    for (int count; -1 != (count = in.read(buffer)); ) {
                        out.write(buffer, 0, count);
                    }
                    result.setJsonElement(new GsonBuilder().setLenient().create()
                            .fromJson(out.toString("UTF-8"), JsonElement.class));
                }catch (Exception e){
                    logger.error("fromResponse: " + e);
                }
            }
            return result;
        }

        public static Result get(String url) throws IOException {
            try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
                HttpGet httpGet = new HttpGet(url);
                try (CloseableHttpResponse response = httpClient.execute(httpGet)) {
                    return fromResponse(response);
                }
            }
        }
}
