package com.alkemytech.sophia.royalties.scheme;

import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class Royalty {

	public Long codiceIpi;
	public String posizioneSiae;
	public String tipoQualifica;
	public String formatoQualifica;
	public String tipoQuota;
	public float percentuale;
	public float numeratore;
	public float denominatore;
	public String cognome;
	public String codiceConto;
	public String codiceContoEstero;
	public long idRepertorio;
	public String idTipoIrregolarita;
	public String socname;
	public float socshr;
	public float shares;
	public boolean doNotClaim;
	public boolean sospesoPerEditore;
	public boolean publicDomain;


	public boolean isMechanical() {
		return "DRM".equalsIgnoreCase(tipoQuota);
	}

	public boolean isPerforming() {
		return "DEM".equalsIgnoreCase(tipoQuota);
	}

	public Royalty setCodiceIpi(Long codiceIpi) {
		this.codiceIpi = codiceIpi;
		return this;
	}

	public Royalty setPosizioneSiae(String posizioneSiae) {
		this.posizioneSiae = posizioneSiae;
		return this;
	}

	public Royalty setTipoQualifica(String tipoQualifica) {
		this.tipoQualifica = tipoQualifica;
		return this;
	}

	public Royalty setFormatoQualifica(String formatoQualifica) {
		this.formatoQualifica = formatoQualifica;
		return this;
	}

	public Royalty setTipoQuota(String tipoQuota) {
		this.tipoQuota = tipoQuota;
		return this;
	}

	public Royalty setPercentuale(float percentuale) {
		this.percentuale = percentuale;
		return this;
	}
	public Royalty setNumeratore(float numeratore) {
		this.numeratore = numeratore;
		return this;
	}

	public Royalty setDenominatore(float denominatore) {
		this.denominatore = denominatore;
		return this;
	}

	public Royalty setCognome(String cognome) {
		this.cognome = cognome;
		return this;
	}

	public Royalty setCodiceConto(String codiceConto) {
		this.codiceConto = codiceConto;
		return this;
	}

	public Royalty setCodiceContoEstero(String codiceContoEstero) {
		this.codiceContoEstero = codiceContoEstero;
		return this;
	}

	public Royalty setIdRepertorio(long idRepertorio) {
		this.idRepertorio = idRepertorio;
		return this;
	}

	public Royalty setIdTipoIrregolarita(String idTipoIrregolarita) {
		this.idTipoIrregolarita = idTipoIrregolarita;
		return this;
	}

	public Royalty setSocname(String socname) {
		this.socname = socname;
		return this;
	}

	public Royalty setSocshr(float socshr) {
		this.socshr = socshr;
		return this;
	}

	public Royalty setShares(float shares) {
		this.shares = shares;
		return this;
	}

	public Royalty setDoNotClaim(boolean doNotClaim) {
		this.doNotClaim = doNotClaim;
		return this;
	}

	public Royalty setSospesoPerEditore(boolean sospesoPerEditore) {
		this.sospesoPerEditore = sospesoPerEditore;
		return this;
	}

	public Royalty setPublicDomain(boolean publicDomain) {
		this.publicDomain = publicDomain;
		return this;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}

}
