package com.alkemytech.sophia.royalties.scheme;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
@SuppressWarnings("serial")
public class RoyaltySchemeException extends RuntimeException {

	public RoyaltySchemeException() {
		super();
	}

	public RoyaltySchemeException(String message, Throwable cause) {
		super(message, cause);
	}

	public RoyaltySchemeException(String message) {
		super(message);
	}

	public RoyaltySchemeException(Throwable cause) {
		super(cause);
	}
		
}
