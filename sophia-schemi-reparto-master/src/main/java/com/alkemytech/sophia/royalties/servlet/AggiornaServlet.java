package com.alkemytech.sophia.royalties.servlet;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.util.FileUtils;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.royalties.Configuration;
import com.alkemytech.sophia.royalties.LatestVersion;
import com.alkemytech.sophia.royalties.blacklist.BlackListService;
import com.alkemytech.sophia.royalties.role.RoleCatalog;
import com.alkemytech.sophia.royalties.scheme.IrregularityCatalog;
import com.alkemytech.sophia.royalties.scheme.TemporaryWorkCatalog;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.google.inject.name.Named;


@SuppressWarnings("serial")
public class AggiornaServlet extends HttpServlet {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	protected final Configuration configuration;
	protected final Charset charset;
	protected final S3 s3;
	private final RoleCatalog roleCatalog;
	private final TemporaryWorkCatalog temporaryWorkCatalog;
	private final IrregularityCatalog irregularityCatalog;
	private final BlackListService blackList;
	private final Gson gson;
	private final LockKbService lockWeb;
	@Inject
	protected AggiornaServlet(Configuration configuration,
			@Named("charset") Charset charset,
			S3 s3, RoleCatalog roleCatalog,
			TemporaryWorkCatalog temporaryWorkCatalog,
			IrregularityCatalog irregularityCatalog,
			Gson gson,
			BlackListService blackList,
			LockKbService lockWeb
			) {
		super();
		this.configuration = configuration;
		this.charset = charset;
		this.s3 = s3;
		this.roleCatalog = roleCatalog;
		this.temporaryWorkCatalog = temporaryWorkCatalog;
		this.irregularityCatalog = irregularityCatalog;
		this.gson = gson;
		this.blackList = blackList;
		this.lockWeb = lockWeb;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final JsonObject processing = new JsonObject();
		String year = null;
		String month = null;
		String blackListSiae = null;
		String blackListAda = null;
		try {
			
			final JsonObject requestJson = (JsonObject)request.getAttribute(GsonFilter.REQUEST_JSON);
			for (Map.Entry<String, JsonElement> entry : requestJson.entrySet()) {
				final String textType = entry.getKey();
				if(entry.getValue().isJsonArray()) {
					for (JsonElement text : GsonUtils.getAsJsonArray(entry.getValue())) {
						System.out.println(text.getAsString()+ ":" + textType);
					}
				}else if(entry.getValue().isJsonObject()) {
					 JsonObject obj = GsonUtils.getAsJsonObject(entry.getValue());
					 blackListSiae = obj.get("SIAE").getAsString();
					 blackListAda = obj.get("UCMR-ADA").getAsString();
				}else {
					if("year".equals(textType)) {
						year = entry.getValue().getAsString();
					}else if("month".equals(textType)) {
						month = entry.getValue().getAsString();						
					}
				}
			}		
			if(month != null && year!= null && blackListSiae != null && blackListAda != null) {
				this.lockWeb.lock();
				s3.startup();
				final String _oldPeriod = (configuration.getTag("{year}")==null?"":configuration.getTag("{year}")) + (configuration.getTag("{month}")==null?"":configuration.getTag("{month}"));
				
				
				// configuration tags
				configuration.setTag("{year}", year);
				configuration.setTag("{month}", month);
				processing.addProperty("year", year);					
				processing.addProperty("month", month);					
				final String _period = (configuration.getTag("{year}")==null?"":configuration.getTag("{year}")) + (configuration.getTag("{month}")==null?"":configuration.getTag("{month}"));
				
				final String _blackListSiae = blackListSiae;
				final String _blackListAda = blackListAda;
				final ExecutorService executorService = Executors.newCachedThreadPool();
				executorService.execute(new Runnable() {
					@Override
					public void run() {
						
						try {
							
							//cancello i file
							if(_oldPeriod != null && !_oldPeriod.equals(_period)) {
								//FileUtils.deleteFolder(homeFolder, true);	
								final File homeFolder = new File(configuration.getProperty("webapp_search.data_folder"));
								FileUtils.deleteFolder(homeFolder, true);
							}
							
							// initialize role catalog
							roleCatalog.load(s3, configuration.getProperty("role_catalog.csv_url"));	
							
							// initialize temporary work catalog
							temporaryWorkCatalog.load(s3, configuration.getProperty("temporary_work_catalog.csv_url"));	
	
							// initialize irregularity catalog
							irregularityCatalog.load(s3, configuration.getProperty("irregularity_catalog.csv_url"));	
	
							// initialize ipi nosql to latest version
							downloadAndExtractLatest("webapp_search", "webapp_search.ipi_nosql");
							logger.info("aggiornamento ipi eseguito");
							// initialize siae nosql to latest version
							downloadAndExtractLatest("webapp_search", "webapp_search.siae_nosql");
							logger.info("aggiornamento siae eseguito");
							// initialize ucmr_ada nosql to latest version
							downloadAndExtractLatest("webapp_search", "webapp_search.ucmr_ada_nosql");
							logger.info("aggiornamento ada eseguito");
							// initialize codici nosql to latest version
							downloadAndExtractLatest("webapp_search", "webapp_search.codici_nosql");
							logger.info("aggiornamento codici eseguito");
	
							
							//CARICO LA BLACK LIST
							blackList.loadBlackList(_blackListSiae, _blackListAda);
							//
							
							processing.addProperty("message", 	"aggiornamento della kb eseguito");	
						}catch(Throwable e) {
							logger.info("internal error",e);
							processing.addProperty("errore", "impossibile aggiornare la kb");	
							configuration.removeTag("{year}");
							configuration.removeTag("{month}");
						}
						lockWeb.unLock();
						logger.info("output {}", new GsonBuilder().setPrettyPrinting().create().toJson(processing));
					}
				});
				processing.addProperty("message", 	"inizio aggiornamento in corso per il periodo richiesto");
			}else {
				processing.addProperty("errore", "richiesta errata");				
			}
		}catch(Exception ex) {
			logger.info("internal error",ex);
			processing.addProperty("errore", "impossibile aggiornare la kb");
		}
		response.setContentType("application/json");
		response.getWriter().write(gson.toJson(processing));
	}

	
	protected void downloadAndExtractLatest(String propertyPrefix, String propertyPrefixArchive) throws IOException, InterruptedException {
		// data folder
		final File dataFolder = new File(configuration
				.getProperty(propertyPrefix + ".data_folder"));
		dataFolder.mkdirs();

		// .latest file
		final File latestFile = new File(configuration
				.getProperty(propertyPrefixArchive + ".latest_file"));
		logger.debug("downloadAndExtractLatest: latestFile {}", latestFile);
		
		// load local .latest version
		final LatestVersion localLatestVersion = new LatestVersion()
				.load(latestFile);
		logger.debug("downloadAndExtractLatest: localLatestVersion {}", localLatestVersion);

		// .latest url
		final String latestUrl = configuration
				.getProperty(propertyPrefixArchive + ".latest_url");
		logger.debug("downloadAndExtractLatest: latestUrl {}", latestUrl);

		// temporary latest file
		final File temporaryLatestFile = File
				.createTempFile("__tmp__", "__.latest", dataFolder);
		temporaryLatestFile.deleteOnExit();
		logger.debug("downloadAndExtractLatest: temporaryLatestFile {}", temporaryLatestFile);
		
		// download .latest version
		if (!s3.download(new S3.Url(latestUrl), temporaryLatestFile)) {
			temporaryLatestFile.delete();
			throw new IOException("file download error: " + latestUrl);
		}

		// load downloaded .latest version
		final LatestVersion latestVersion = new LatestVersion()
				.load(temporaryLatestFile);
		logger.debug("downloadAndExtractLatest: latestVersion {}", latestVersion);
		
		// check version
		if (localLatestVersion.getVersion("")
				.equals(latestVersion.getVersion())) {
			temporaryLatestFile.delete();
			logger.info("downloadAndExtractLatest: version unchanged");
			return;
		}

		// archive url
		final String archiveUrl = latestVersion.getLocation();
		
		// archive file
		final File archiveFile = File.createTempFile("__tmp__",
				"__" + archiveUrl.substring(1 + archiveUrl.lastIndexOf('/')), dataFolder);
		archiveFile.deleteOnExit();
		logger.debug("downloadAndExtractLatest: archiveFile {}", archiveFile);

		// download archive
		if (!s3.download(new S3.Url(archiveUrl), archiveFile)) {
			throw new IOException("file download error: " + archiveUrl);
		}

		// destination home folder
		final File destinationFolder = new File(configuration
				.getProperty(propertyPrefixArchive + ".home_folder"));

		// delete existing destination folder
		FileUtils.deleteFolder(destinationFolder, true);
		destinationFolder.mkdirs();

		// untar archive file
		final String[] cmdline = new String[] {
				configuration.getProperty(propertyPrefix + ".tar_path",
						configuration.getProperty("default.tar_path", "/usr/bin/tar")),
				"-C", destinationFolder.getAbsolutePath(),
				"-xzf", archiveFile.getAbsolutePath()
		};
		logger.debug("downloadAndExtractLatest: exec cmdline {}", Arrays.asList(cmdline));
		final int exitCode = Runtime
				.getRuntime().exec(cmdline).waitFor();
		logger.debug("downloadAndExtractLatest: exec exitCode {}", exitCode);
		
		// delete archive file
		archiveFile.delete();
		
		// overwrite local latest file
		if (FileUtils.copy(temporaryLatestFile, latestFile) < 0L) {
			throw new IOException("unable to update local latest file: " + latestFile);
		}

		// delete temporary latest file
		temporaryLatestFile.delete();
	}
}
