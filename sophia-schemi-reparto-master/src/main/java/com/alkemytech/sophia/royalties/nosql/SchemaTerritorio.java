package com.alkemytech.sophia.royalties.nosql;

import com.alkemytech.sophia.commons.nosql.NoSqlEntity;
import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class SchemaTerritorio implements NoSqlEntity {

	public final long idSchemaRiparto;
	public final Territories territories;

	public SchemaTerritorio(long idSchemaRiparto, Territories territories) {
		super();
		if (null == territories || territories.isEmpty())
			throw new IllegalArgumentException("territories is null or empty");
		this.idSchemaRiparto = idSchemaRiparto;
		this.territories = territories;
	}
	
	@Override
	public String getPrimaryKey() {
		return Long.toString(idSchemaRiparto);
	}

	@Override
	public String getSecondaryKey() {
		return null;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}
	
}
