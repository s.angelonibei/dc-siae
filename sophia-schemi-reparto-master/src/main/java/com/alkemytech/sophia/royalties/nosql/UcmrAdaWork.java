package com.alkemytech.sophia.royalties.nosql;

import java.util.ArrayList;
import java.util.List;

import com.alkemytech.sophia.commons.nosql.NoSqlEntity;
import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class UcmrAdaWork implements NoSqlEntity {

	public String masterCode;
	
	public final List<UcmrAdaRow> rows;

	public UcmrAdaWork() {
		super();
		this.rows = new ArrayList<>();
	}

	public UcmrAdaWork(String masterCode) {
		this();
		this.masterCode = masterCode;
	}

	public void add(UcmrAdaRow row) {
		rows.add(row);
	}
	
	@Override
	public String getPrimaryKey() {
		return masterCode;
	}

	@Override
	public String getSecondaryKey() {
		return null;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}
	
}
