package com.alkemy.siae.sophia.pricing;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alkemytech.sophia.royalties.claim.CaricoRipartizione;
import com.alkemytech.sophia.royalties.claim.SocietyInfo;
import com.alkemytech.sophia.royalties.nosql.QuotaRiparto;
import com.google.gson.GsonBuilder;
import lombok.Data;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
@Data
public class DsrMetadata extends EventBase {

	protected String year;						// 2019
	protected String month;						// 01
	protected String day;						// 01
	protected String idDsr;						// Amazon-Sales-DDEX
	protected String dsp;						// youtube
	protected String dspReclami;				// youtube
	protected String dspCode;					// YOU
	protected String claimPeriodType;			// M
	protected int period;						// 1
	protected String periodType;				// month
	protected String periodStartDate;			// 20190101
	protected String periodEndDate;				// 20190131
	protected float splitDEM;					// performing
	protected float splitDRM;					// mechanical
	protected BigInteger ccidId;				// 198
	protected BigInteger baseRefId;				// 198000000000000
	protected String royaltyCurrency;			// EUR
	protected String tradingBrand;				// *
	protected String sender;					// SIAE
	protected String receiver;					// amazon
	protected String appliedTariff;				// DWIP
	protected String paramAppliedTariff;		// 10.0
	protected String paramAux;
	protected String paramCommercialModel;
	protected String paramClaimType;
	protected String originalCurrency;			// EUR
	protected String claimUtilizationType;		// O
	protected boolean encoded;					// true
	protected boolean notEncoded;				// false
	protected boolean encodedProvisional;		// true
	protected BigDecimal conversionRate;		// 100000
	protected String ccidVersion;				// 14
	protected String serviceType;				// PDS
	protected String useType;					// PD
	protected String workCodeType;				// SIAE_WORK_CODE
	protected String decisionTableUrl;			// s3://siae-sophia-datalake/debug/rules/amazon/DWN/20160101/AmazonDownload_201601.xls
	protected String tipoRepertorio;			// M
	protected SocietyInfo tenant;
	protected final Map<String, SocietyInfo> mandators;
	protected List<CaricoRipartizione> carichiRipartizione;

	protected List<QuotaRiparto> quotaRipartoList;

	// R9
	protected String idUtilizationType;


	// computed fields
	protected BigDecimal sumAmountInvoicedTotal;
	//FIX CTASK0024973 (CHG0033723) SUM AMOUNT_LICENSOR_MECH+AMOUNT_LICENSOR_PERF
	protected BigDecimal sumAmountMech;
	protected BigDecimal sumAmountPerf;
	//
	protected BigDecimal sumAmountUnmatchedTotal;
	protected long sumIdRecords;
	protected BigDecimal sumSalesCount;

	// Relaese R5 Stabilizzazione
	protected String transactionID;			// transaction Id of DSR

	public DsrMetadata() {
		super();
		this.mandators = new HashMap<>();
		this.sumAmountInvoicedTotal = BigDecimal.ZERO;
		this.sumAmountUnmatchedTotal = BigDecimal.ZERO;
		//FIX CTASK0024973 (CHG0033723) SUM AMOUNT_LICENSOR_MECH+AMOUNT_LICENSOR_PERF
		this.sumAmountMech = BigDecimal.ZERO;
		this.sumAmountPerf = BigDecimal.ZERO;
		this.sumIdRecords = 0L;
		this.sumSalesCount = BigDecimal.ZERO;
	}

	//FIX CTASK0024973 (CHG0033723) SUM AMOUNT_LICENSOR_MECH+AMOUNT_LICENSOR_PERF
	public void addAmountMech(BigDecimal amountLicensorDem){
		sumAmountMech = sumAmountMech.add(amountLicensorDem);
	}

	public void addAmountPerf(BigDecimal amountLicensorDrm){
		sumAmountPerf = sumAmountPerf.add(amountLicensorDrm);
	}

	public void addAmountInvoicedTotal(BigDecimal amountInvoicedTotal) {
		sumAmountInvoicedTotal = sumAmountInvoicedTotal.add(amountInvoicedTotal);
	}

	public BigDecimal getSumAmountInvoicedTotal() {
		return sumAmountInvoicedTotal;
	}

	public void addAmountUnmatchedTotal(BigDecimal amountUnmatchedTotal) {
		sumAmountUnmatchedTotal = sumAmountUnmatchedTotal.add(amountUnmatchedTotal);
	}

	public BigDecimal getSumAmountUnmatchedTotal() {
		return sumAmountUnmatchedTotal;
	}

	public void incrementSumIdRecords() {
		sumIdRecords ++;
	}

	public long getSumIdRecords() {
		return sumIdRecords;
	}

	public void addSumSalesCount(BigDecimal salesCount) {
		sumSalesCount = sumSalesCount.add(salesCount);
	}

	public BigDecimal getSumSalesCount() {
		return sumSalesCount;
	}

	public String getReferencePeriod() {
		return year + month;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getIdDsr() {
		return idDsr;
	}

	public void setIdDsr(String idDsr) {
		this.idDsr = idDsr;
	}

	public String getPeriodStartDate() {
		return periodStartDate;
	}

	public void setPeriodStartDate(String periodStartDate) {
		this.periodStartDate = periodStartDate;
	}

	public String getPeriodEndDate() {
		return periodEndDate;
	}

	public void setPeriodEndDate(String periodEndDate) {
		this.periodEndDate = periodEndDate;
	}

	public String getDsp() {
		return dsp;
	}

	public void setDsp(String dsp) {
		this.dsp = dsp;
	}

	public String getDspReclami() {return dspReclami;}

	public void setDspReclami(String dspReclami) {this.dspReclami = dspReclami;}

	public String getDspCode() {
		return dspCode;
	}

	public void setDspCode(String dspCode) {
		this.dspCode = dspCode;
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

	public String getPeriodType() {
		return periodType;
	}

	public void setPeriodType(String periodType) {
		this.periodType = periodType;
	}

	public String getClaimPeriodType() {
		return claimPeriodType;
	}

	public void setClaimPeriodType(String claimPeriodType) {
		this.claimPeriodType = claimPeriodType;
	}

	public float getSplitDEM() {
		return splitDEM;
	}

	public void setSplitDEM(float splitDEM) {
		this.splitDEM = splitDEM;
	}

	public float getSplitDRM() {
		return splitDRM;
	}

	public void setSplitDRM(float splitDRM) {
		this.splitDRM = splitDRM;
	}

	public BigInteger getCcidId() {
		return ccidId;
	}

	public void setCcidId(BigInteger ccidId) {
		this.ccidId = ccidId;
	}

	public BigInteger getBaseRefId() {
		return baseRefId;
	}

	public void setBaseRefId(BigInteger baseRefId) {
		this.baseRefId = baseRefId;
	}

	public String getRoyaltyCurrency() {
		return royaltyCurrency;
	}

	public void setRoyaltyCurrency(String royaltyCurrency) {
		this.royaltyCurrency = royaltyCurrency;
	}

	public String getTradingBrand() {
		return tradingBrand;
	}

	public void setTradingBrand(String tradingBrand) {
		this.tradingBrand = tradingBrand;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getAppliedTariff() {
		return appliedTariff;
	}

	public void setAppliedTariff(String appliedTariff) {
		this.appliedTariff = appliedTariff;
	}

	public String getParamAppliedTariff() {
		return paramAppliedTariff;
	}

	public void setParamAppliedTariff(String paramAppliedTariff) {
		this.paramAppliedTariff = paramAppliedTariff;
	}

	public String getParamAux() {
		return paramAux;
	}

	public void setParamAux(String paramAux) {
		this.paramAux = paramAux;
	}

	public String getOriginalCurrency() {
		return originalCurrency;
	}

	public void setOriginalCurrency(String originalCurrency) {
		this.originalCurrency = originalCurrency;
	}

	public String getClaimUtilizationType() {
		return claimUtilizationType;
	}

	public void setClaimUtilizationType(String claimUtilizationType) {
		this.claimUtilizationType = claimUtilizationType;
	}

	public boolean isEncoded() {
		return encoded;
	}

	public void setEncoded(boolean encoded) {
		this.encoded = encoded;
	}

	public boolean isNotEncoded() {
		return notEncoded;
	}

	public void setNotEncoded(boolean notEncoded) {
		this.notEncoded = notEncoded;
	}

	public boolean isEncodedProvisional() {
		return encodedProvisional;
	}

	public void setEncodedProvisional(boolean encodedProvisional) {
		this.encodedProvisional = encodedProvisional;
	}

	public BigDecimal getConversionRate() {
		return conversionRate;
	}

	public void setConversionRate(BigDecimal conversionRate) {
		this.conversionRate = conversionRate;
	}

	public String getCcidVersion() {
		return ccidVersion;
	}

	public void setCcidVersion(String ccidVersion) {
		this.ccidVersion = ccidVersion;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getUseType() {
		return useType;
	}

	public void setUseType(String useType) {
		this.useType = useType;
	}

	public String getWorkCodeType() {
		return workCodeType;
	}

	public void setWorkCodeType(String workCodeType) {
		this.workCodeType = workCodeType;
	}

	public String getDecisionTableUrl() {
		return decisionTableUrl;
	}

	public void setDecisionTableUrl(String decisionTableUrl) {
		this.decisionTableUrl = decisionTableUrl;
	}

	public String getTipoRepertorio() {
		return tipoRepertorio;
	}

	public void setTipoRepertorio(String tipoRepertorio) {
		this.tipoRepertorio = tipoRepertorio;
	}

	public SocietyInfo getTenant() {
		return tenant;
	}

	public void setTenant(SocietyInfo tenant) {
		this.tenant = tenant;
	}

	public SocietyInfo getMandator(String society) {
		return mandators.get(society);
	}

	public Collection<SocietyInfo> getMandators() {
		return mandators.values();
	}

	public String getTransactionID() {
				return transactionID;
	}

	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}
	public void addMandator(SocietyInfo mandator) {
		mandators.put(mandator.getSociety(), mandator);
	}

	public void addMandators(Collection<SocietyInfo> mandators) {
		for (SocietyInfo mandator : mandators) {
			addMandator(mandator);
		}
	}

	public void clearMandators() {
		mandators.clear();
	}

	public List<CaricoRipartizione> getCarichiRipartizione() {
		return carichiRipartizione;
	}

	public void setCarichiRipartizione(List<CaricoRipartizione> carichiRipartizione) {
		this.carichiRipartizione = carichiRipartizione;
	}

	public List<QuotaRiparto> getQuotaRipartoList() {
		return quotaRipartoList;
	}

	public void setQuotaRipartoList(List<QuotaRiparto> quotaRipartoList) {
		this.quotaRipartoList = quotaRipartoList;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}

	public String getParamCommercialModel() {
		return paramCommercialModel;
	}

	public void setParamCommercialModel(String paramCommercialModel) {
		this.paramCommercialModel = paramCommercialModel;
	}

	public String getParamClaimType() {
		return paramClaimType;
	}

	public void setParamClaimType(String paramClaimType) {
		this.paramClaimType = paramClaimType;
	}
}
