package com.alkemy.siae.sophia.pricing;

import java.math.BigDecimal;

import org.apache.commons.csv.CSVRecord;

import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class DsrLine extends EventBase {	
	
	// reference to csv record
	private final CSVRecord csvRecord;
	
	// read from dsr sale lines
	private String idUtil;					// id utilizzazione sophia
	private String commercialModel;			// SubscriptionModel, PayAsYouGoModel
	private String unitaryPrice;			// 13.45
	private BigDecimal salesCount;				// 2
	private String amount;
	private String transactionType;			// OnDemandStream, PermanentDownload
	private String releaseType;				// ALBUM, SINGLE
	private String upgradeFlag;				// 0
	private String trackDuration;			// 161
	private String albumTracks;				// 1
	private String transactionTracks;		// 1
	private String uuidSophia;				// cbb0a1a5-0375-441a-baab-74cd0bb24afb
	private String title;
	private String artists;
	private String isrc;
	private String iswc;
	private String transactionId;
	private String proprietrayId;
	private String albumProprietrayId;
	private String matchType;				// iswc, isrc, artist_composer
	
	// computed fields
	private BigDecimal price;				// computed price in the currency of royalties
	private BigDecimal priceEur;			// computed price in EUR
	
	public DsrLine(CSVRecord csvRecord) {
		super();
		this.csvRecord = csvRecord;
	}
	
	public CSVRecord getCSVRecord() {
		return csvRecord;
	}
	
	public String getIdUtil() {
		return idUtil;
	}

	public void setIdUtil(String idUtil) {
		this.idUtil = idUtil;
	}

	public String getCommercialModel() {
		return commercialModel;
	}

	public void setCommercialModel(String commercialModel) {
		this.commercialModel = commercialModel;
	}

	public String getUnitaryPrice() {
		return unitaryPrice;
	}

	public void setUnitaryPrice(String unitaryPrice) {
		this.unitaryPrice = unitaryPrice;
	}

	public BigDecimal getSalesCount() {
		return salesCount;
	}

	public void setSalesCount(BigDecimal salesCount) {
		this.salesCount = salesCount;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getReleaseType() {
		return releaseType;
	}

	public void setReleaseType(String releaseType) {
		this.releaseType = releaseType;
	}

	public String getUpgradeFlag() {
		return upgradeFlag;
	}

	public void setUpgradeFlag(String upgradeFlag) {
		this.upgradeFlag = upgradeFlag;
	}

	public String getTrackDuration() {
		return trackDuration;
	}

	public void setTrackDuration(String trackDuration) {
		this.trackDuration = trackDuration;
	}

	public String getAlbumTracks() {
		return albumTracks;
	}

	public void setAlbumTracks(String albumTracks) {
		this.albumTracks = albumTracks;
	}

	public String getTransactionTracks() {
		return transactionTracks;
	}

	public void setTransactionTracks(String transactionTracks) {
		this.transactionTracks = transactionTracks;
	}
	
	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getPriceEur() {
		return priceEur;
	}

	public void setPriceEur(BigDecimal priceEur) {
		this.priceEur = priceEur;
	}
	
	public String getUuidSophia() {
		return uuidSophia;
	}

	public void setUuidSophia(String uuidSophia) {
		this.uuidSophia = uuidSophia;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getArtists() {
		return artists;
	}

	public void setArtists(String artists) {
		this.artists = artists;
	}

	public String getIsrc() {
		return isrc;
	}

	public void setIsrc(String isrc) {
		this.isrc = isrc;
	}

	public String getIswc() {
		return iswc;
	}

	public void setIswc(String iswc) {
		this.iswc = iswc;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getProprietrayId() {
		return proprietrayId;
	}

	public void setProprietrayId(String proprietrayId) {
		this.proprietrayId = proprietrayId;
	}

	public String getAlbumProprietrayId() {
		return albumProprietrayId;
	}

	public void setAlbumProprietrayId(String albumProprietrayId) {
		this.albumProprietrayId = albumProprietrayId;
	}

	public String getMatchType() {
		return matchType;
	}

	public void setMatchType(String matchType) {
		this.matchType = matchType;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}
	
}
