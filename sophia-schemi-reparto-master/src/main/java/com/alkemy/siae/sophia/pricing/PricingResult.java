package com.alkemy.siae.sophia.pricing;

import com.alkemytech.sophia.royalties.claim.PriceModels;
import com.alkemytech.sophia.royalties.claim.PricingException;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public abstract class PricingResult {

	private final PriceModels priceModels;
	
	protected PricingResult(PriceModels priceModels) {
		super();
		this.priceModels = priceModels;
	}
	
	/**
	 * Callback with metadata pricing results.
	 * 
	 * @param dsrMetadata
	 * @throws PricingException
	 */
	public abstract void onPricingResult(DsrMetadata dsrMetadata) throws PricingException;

	public PricingResult add(DsrMetadata dsrMetadata) {
		priceModels.computePrice(dsrMetadata);
		onPricingResult(dsrMetadata);
		return this;
	}

	/**
	 * Callback with line pricing results.
	 * 
	 * @param dsrLine
	 * @throws PricingException
	 */
	public abstract void onPricingResult(DsrLine dsrLine) throws PricingException;

	public PricingResult add(DsrLine dsrLine) {
		priceModels.computePrice(dsrLine);
		onPricingResult(dsrLine);
		return this;
	}
	
}
