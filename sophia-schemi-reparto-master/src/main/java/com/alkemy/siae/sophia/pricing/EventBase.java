package com.alkemy.siae.sophia.pricing;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.alkemytech.sophia.royalties.claim.MinimumRange;
import com.google.gson.GsonBuilder;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public abstract class EventBase {

	// metadata found in *_V
	protected BigDecimal totalAmount;
	protected long totalSubscriptions;
	protected BigDecimal totalSales;

	// data found in DSR lines
	protected String currency;					// EUR
	protected String territory;					// IT (2 digit)
	protected String pricingModel;				// DOWN
	protected String businessOffer;				// GooglePlay_PAYG

	// injected
	protected String country;					// ITA (3 digits)
	protected String region;					// Africa
	protected String subregion;					// Western Africa
	protected String dvdRegion;					// 1

	// esistono due tipi di minimi:
	//   1. minimo per singola traccia (1 sola utilizzazione)
	//   2. minimi per album a range (minimo 2 utilizzazioni)

	// drools injected info
	protected BigDecimal proRata;				// 10.5
	protected BigDecimal minimum;				// 0.076
	protected BigDecimal minimumStream;			// minimo per Stream da drools (nuovo streaming generico)
	protected BigDecimal minimumAbbonamento;	// minimo per Abbonamento da drools (nuovo streaming generico)
	protected List<MinimumRange> minima;		// [{2,0.073,P},{8,0.062,P},{13,0.06,P},{16,0.052,P},{18,0.047,P}]
	protected String minimumCurrency;			// EUR

	// computed fields
	protected BigDecimal priceBasis;			// net price for a single usage of musical work by end user excluding V.A.T in the currency of the royalties
	protected BigDecimal originalPriceBasis;	// net price for a single usage of musical work by end user excluding V.A.T in the original currency
	protected BigDecimal royalty;				// royalty value of the work for a single usage
	protected String royaltyType;				// M = Minimum, P = Percentage, O = Other
	protected BigDecimal royaltyEur;			// computed royalty in EUR

	protected EventBase() {
		super();
	}
	
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
	
	public long getTotalSubscriptions() {
		return totalSubscriptions;
	}
	public void setTotalSubscriptions(long totalSubscriptions) {
		this.totalSubscriptions = totalSubscriptions;
	}
	
	public BigDecimal getTotalSales() {
		return totalSales;
	}
	public void setTotalSales(BigDecimal totalSales) {
		this.totalSales = totalSales;
	}

	public String getTerritory() {
		return territory;
	}

	public void setTerritory(String territory) {
		this.territory = territory;
	}

	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getRegion() {
		return region;
	}
	
	public void setRegion(String region) {
		this.region = region;
	}
	
	public String getSubregion() {
		return subregion;
	}
	
	public void setSubregion(String subregion) {
		this.subregion = subregion;
	}
	
	public String getDvdRegion() {
		return dvdRegion;
	}
	
	public void setDvdRegion(String dvdRegion) {
		this.dvdRegion = dvdRegion;
	}
	
	public String getPricingModel() {
		return pricingModel;
	}

	public void setPricingModel(String pricingModel) {
		this.pricingModel = pricingModel;
	}

	public String getBusinessOffer() {
		return businessOffer;
	}

	public void setBusinessOffer(String businessOffer) {
		this.businessOffer = businessOffer;
	}
	
	public BigDecimal getProRata() {
		return proRata;
	}

	public void setProRata(String proRata) {
		this.proRata = new BigDecimal(proRata.replace(',', '.'));
	}

	public BigDecimal getMinimum() {
		return minimum;
	}
	
	public void setMinimum(String minimum) {
		try {
			this.minimum = new BigDecimal(minimum.replace(',', '.'));
			if (null == minima) {
				minima = new ArrayList<MinimumRange>();
			}
			if (minima.isEmpty() ||
					(minima.get(0).getUpToTracks() > 1 &&
					minima.get(0).getFromTracks() > 1)) {
				minima.add(0, new MinimumRange(1, 1,
						this.minimum, MinimumRange.TYPE_FIXED)); // use FIXED to avoid multiplication by 1
			}
		} catch (NumberFormatException e) {
			this.minimum = null;
		}
	}

	public BigDecimal getMinimumAbbonamento() {
		return minimumAbbonamento!= null ? minimumAbbonamento : BigDecimal.ZERO;
	}

	public void setMinimumAbbonamento(String minimum) {
		try {
			this.minimumAbbonamento = new BigDecimal(minimum.replace(',', '.'));
//			if (null == minima) {
//				minima = new ArrayList<MinimumRange>();
//			}
//			if (minima.isEmpty() ||
//					(minima.get(0).getUpToTracks() > 1 &&
//							minima.get(0).getFromTracks() > 1)) {
//				minima.add(0, new MinimumRange(1, 1,
//						this.minimum, MinimumRange.TYPE_FIXED)); // use FIXED to avoid multiplication by 1
//			}
		} catch (NumberFormatException e) {
			this.minimumAbbonamento = null;
		}
	}

	public BigDecimal getMinimumStream() {
		return minimumStream!= null? minimumStream : BigDecimal.ZERO;
	}

	public void setMinimumStream(String minimum) {
		try {
			this.minimumStream = new BigDecimal(minimum.replace(',', '.'));
//			if (null == minima) {
//				minima = new ArrayList<MinimumRange>();
//			}
//			if (minima.isEmpty() ||
//					(minima.get(0).getUpToTracks() > 1 &&
//							minima.get(0).getFromTracks() > 1)) {
//				minima.add(0, new MinimumRange(1, 1,
//						this.minimum, MinimumRange.TYPE_FIXED)); // use FIXED to avoid multiplication by 1
//			}
		} catch (NumberFormatException e) {
			this.minimumStream = null;
		}
	}
		
	public List<MinimumRange> getMinima(boolean sort) {
		if (null != minima && sort) {
			Collections.sort(minima, new Comparator<MinimumRange>() {
				@Override
				public int compare(MinimumRange o1, MinimumRange o2) {
					return Integer.compare(o1.getUpToTracks(), o2.getUpToTracks());
				}
			});
		}
		return minima;
	}

	public void addMinProp(String upToTracks, String minimum) {
		addMinimum(upToTracks, minimum, MinimumRange.TYPE_PROPORTIONAL);
	}
	
	public void addMinFixed(String upToTracks, String minimum) {
		addMinimum(upToTracks, minimum, MinimumRange.TYPE_FIXED);
	}

	public void addMinimum(String upToTracks, String minimum, String type) {
		if (MinimumRange.TYPE_PROPORTIONAL.equalsIgnoreCase(type) ||
				MinimumRange.TYPE_FIXED.equalsIgnoreCase(type)) {
			try {
				int ft = -1; // default to -1
				final int minus = upToTracks.indexOf('-');
				if (minus > 0) {
					ft = Integer.parseInt(upToTracks.substring(0, minus).trim()); // "2-7" --> 2
					upToTracks = upToTracks.substring(1 + minus); // "2-7" --> "7"
				}
				final int u2t = Integer.parseInt(upToTracks.trim()); // "7" --> 7
				if (u2t > 0) {
					if (null == minima) {
						minima = new ArrayList<MinimumRange>();
					}
					minima.add(new MinimumRange(ft, u2t, 
							new BigDecimal(minimum.replace(',', '.')), type));
				}
			} catch (NullPointerException | NumberFormatException e) {
				// swallow
			}
		}
	}

	public String getMinimumCurrency() {
		return minimumCurrency;
	}

	public void setMinimumCurrency(String minimumCurrency) {
		this.minimumCurrency = minimumCurrency;
	}

	public BigDecimal getPriceBasis() {
		return priceBasis;
	}

	public void setPriceBasis(BigDecimal priceBasis) {
		this.priceBasis = priceBasis;
	}

	public BigDecimal getOriginalPriceBasis() {
		return originalPriceBasis;
	}

	public void setOriginalPriceBasis(BigDecimal originalPriceBasis) {
		this.originalPriceBasis = originalPriceBasis;
	}

	public BigDecimal getRoyalty() {
		return royalty;
	}

	public void setRoyalty(BigDecimal royalty) {
		this.royalty = royalty;
	}

	public String getRoyaltyType() {
		return royaltyType;
	}

	public void setRoyaltyType(String royaltyType) {
		this.royaltyType = royaltyType;
	}
	
	public BigDecimal getRoyaltyEur() {
		return royaltyEur;
	}
	
	public void setRoyaltyEur(BigDecimal royaltyEur) {
		this.royaltyEur = royaltyEur;
	}



	@Override
	public String toString() {
		return new GsonBuilder()
				.create().toJson(this);
	}

}
