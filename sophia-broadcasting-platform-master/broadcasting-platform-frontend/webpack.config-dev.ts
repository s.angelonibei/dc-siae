
/*
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ProgressPlugin = require('webpack/lib/ProgressPlugin');
const CircularDependencyPlugin = require('circular-dependency-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const { ProvidePlugin } = require('webpack');


const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const extractSass = new MiniCssExtractPlugin ({
  filename: '[name].[contenthash].css',
  disable: process.env.NODE_ENV === 'development',
});
*/
import path = require('path');


module.exports = {
  resolve: {
    extensions: [
      '.ts',
      '.js',
    ],
    modules: [
      './node_modules',
    ],
    symlinks: true,
    mainFields: [
      'browser',
      'module',
      'main',
    ],
  },
  resolveLoader: {
    modules: [
      './node_modules',
    ],
  },
  entry: {
    main: [
      './src/main.ts',
    ],
    styles: [
      './src/styles.scss',
    ],
  },
  output: {
    path: path.join(process.cwd(), 'dist'),
    publicPath: '/' + process.env.npm_package_name + '/',
    filename: '[name].bundle.js',
    chunkFilename: '[id].chunk.js',
    crossOriginLoading: false,
  },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.html$/,
        loader: 'raw-loader',
      },
      {
        test: /\.(eot|svg|cur)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[hash:20].[ext]',
          limit: 10000,
        },
      },
      {
        test: /\.(jpg|png|webp|gif|otf|ttf|woff|woff2|ani)$/,
        loader: 'url-loader',
        options: {
          name: '[name].[hash:20].[ext]',
          limit: 10000,
        },
      },

      {
        test: /\.scss$/,
        use: [
          'css-loader',
          'postcss-loader',
        ],
      },
      /*
      {
        exclude: [
          path.join(process.cwd(), 'src/styles.scss'),
        ],
        test: /\.css$/,
        use: [
          'exports-loader?module.exports.toString()',
          {
            loader: 'css-loader',
          },
          {
            loader: 'postcss-loader',
          },
        ],
      },
      {
        include: [
          path.join(process.cwd(), 'src/styles.scss'),
        ],
        test: /\.css$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
          },
          {
            loader: 'postcss-loader',
          },
        ],
      },

      {
        test: /\.ts$/,
        loader: '@ngtools/webpack',
      },
      */
    ],
  },

  plugins: [
    /*new AngularCompilerPlugin({
      mainPath: 'main.ts',
      platform: 0,
      hostReplacementPaths: {
        'environments/environment.ts': 'environments/environment.ts',
      },
      sourceMap: true,
      tsConfigPath: 'src/tsconfig.json',
      skipCodeGeneration: true,
      compilerOptions: {},
    }),
    extractSass,
    new CleanWebpackPlugin(path.join(process.cwd(), 'dist')),
    new ProvidePlugin({
      jQuery: 'jquery',
      $: 'jquery',
      jquery: 'jquery',
    }),
    new CopyWebpackPlugin([
      {
        context: 'src',
        to: '',
        from: {
          glob: 'assets/!**!/!*',
          dot: true,
        },
      },
      {
        context: 'src',
        to: '',
        from: {
          glob: 'favicon.png',
          dot: true,
        },
      },
      {
        context: 'src',
        to: 'WEB-INF',
        from: {
          glob: '*.xml',
          dot: true,
        },
      },
    ],                    {
      ignore: [
        '.gitkeep',
        '**!/.DS_Store',
        '**!/Thumbs.db',
      ],
      debug: 'warning',
    }),
    new ProgressPlugin(),
    new CircularDependencyPlugin({
      exclude: /(\\|\/)node_modules(\\|\/)/,
      failOnError: false,
      onDetected: false,
      cwd: process.cwd(),
    }),
        // new CommonsChunkPlugin({
        //     name: [
        //         "inline"
        //     ],
        //     minChunks: null
        // }),
        // new CommonsChunkPlugin({
        //     name: [
        //         "vendor"
        //     ],
        //     minChunks: (module: any) => {
        //         return module.resource
        //             && (module.resource.startsWith(nodeModules)
        //                 || module.resource.startsWith(genDirNodeModules)
        //                 || module.resource.startsWith(realNodeModules));
        //     },
        //     chunks: [
        //         "main"
        //     ]
        // }),
        // new CommonsChunkPlugin({
        //     name: [
        //         "main"
        //     ],
        //     minChunks: 2,
        //     async: "common"
        // }),
*/
  ],
  node: {
    fs: 'empty',
    global: true,
    crypto: 'empty',
    tls: 'empty',
    net: 'empty',
    process: true,
    module: false,
    clearImmediate: false,
    setImmediate: false,
  },
  devServer: {
    publicPath: '/' + process.env.npm_package_name + '/',
    historyApiFallback: {
      disableDotRule: true,
      htmlAcceptHeaders: ['text/html', 'application/xhtml+xml'],
      rewrites: [
                { from: /^(.*)$/, to: '/' +   + '/' },
      ],
    },
    proxy: {
      '/broadcasting-platform-backend': 'http://localhost:8080',
      '/cruscottoUtilizzazioni': 'http://localhost:8080',
    },
  },
};
