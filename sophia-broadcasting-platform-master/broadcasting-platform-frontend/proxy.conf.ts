const PROXY_CONFIG = [
  {
    context: [
      '/broadcasting-platform-backend',
      '/cruscottoUtilizzazioni',
    ],
    pathRewrite: {
      '/^(.*)$/': '/' + process.env.npm_package_name + '/',
    },
    target: 'http://localhost:8082',
    secure: false,
  },
];

module.exports = PROXY_CONFIG;
