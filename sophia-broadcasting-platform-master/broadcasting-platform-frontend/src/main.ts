import './polyfills.ts';
import 'hammerjs';

import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';
import { environment } from './environments/environment';
import { AppModule } from './app/app.module';

if (environment.production) {
  enableProdMode();
}

if (document.location.pathname !== '/') {
  document.getElementById('manifest')
    .setAttribute('href', 'assets/manifest.dev.json');
}

platformBrowserDynamic().bootstrapModule(AppModule);

// Select the node that will be observed for mutations
