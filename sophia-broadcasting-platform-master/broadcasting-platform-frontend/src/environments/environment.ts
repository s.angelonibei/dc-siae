export const environment = {
  authenticationApiUrl: '/broadcasting-platform-backend',
  dataApiUrl: '/broadcasting-platform-backend',
  production: false,
};
