// export { environment } from "../environment.json";
export const environment = {
  authenticationApiUrl: '/broadcasting-platform-backend',
  dataApiUrl: '/broadcasting-platform-backend',
  production: true,
};
