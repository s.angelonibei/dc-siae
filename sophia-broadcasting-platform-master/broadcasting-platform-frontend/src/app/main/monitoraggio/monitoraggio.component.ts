import { Component } from '@angular/core';
import * as moment from 'moment';
import { User } from '../../shared/models/user.model';
import { KPI } from '../../shared/models/kpi.model';
import { KpiService } from '../../shared/services/kpi.service';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { FileDaElaborare } from '../../shared/models/response.data.kpi.model';
import { UtilService } from '../../shared/services/util.service';
import { Canale } from '../../shared/models/canale.model';
import { ActivatedRoute } from '@angular/router';
import { ResponseKpi } from '../../shared/models/response.kpi.model';
import { CustomMatDialogComponent } from '../../shared/components/custom-mat-dialog/custom-mat-dialog.component';
import { AuthService } from '../../shared/services/auth/auth.service';

@Component({
  selector: 'bp-monitoraggio',
  templateUrl: './monitoraggio.component.html',
  styleUrls: ['./monitoraggio.component.scss'],
})
export class MonitoraggioComponent {
  kpis: KPI[];

  model: any = {
    canali: Canale,
    mesi: [],
    anno: undefined,
    emittente: {
      id: undefined,
      nuovoTracciato: undefined
    },
    tipoCanali: undefined,
  };

  canale: Canale;
  canali: Canale[];
  mesi: {}[];
  anni: number[];

  appendChar = '%';

  thresholdConfig = {
    0: { color: 'red' },
    33.3: { color: 'orange' },
    66.6: { color: 'green' },
  };

  thresholdConfigAlt = {
    0: { color: 'red' },
    33.3: { color: 'orange' },
    66.6: { color: 'green' },
  };

  displayedColumns = ['nomeFile', 'dataUpload', 'canale', 'tipoUpload'];
  fileDaElaborare: MatTableDataSource<FileDaElaborare>;

  constructor(private user: User,
              private ks: KpiService,
              private as: AuthService,
              private dialog: MatDialog,
              private route: ActivatedRoute) {
    this.as.getUserLoggedIn().subscribe((theUser: User) => {
      moment.locale('it');
      this.user = theUser;

      this.model.emittente = this.user.broadcasters;
      this.model.tipoCanali = this.user.broadcasters.tipoBroadcaster;

      this.kpis = [];

      this.anni = UtilService.years;
      this.mesi = UtilService.months;
      this.route.data
                .subscribe((data: { canali: Canale[], kpi: ResponseKpi }) => {
                  this.canali = data.canali;
                  this.kpis = data.kpi.ResponseData.output.listaKPI;
                  this.fileDaElaborare =
                    new MatTableDataSource<FileDaElaborare>(data.kpi.ResponseData.output.fileDaElaborare);
                });

    });

  }

  selected(event) {
    this.model.canali = event.value.id;
  }

  public onChange(event) {
    this.model.mesi = [];
    this.mesi = UtilService.getMonths(event.value);
  }

  onSubmit(form) {
    if (!form.valid) {
      this.dialog.open(CustomMatDialogComponent, {
        data: { title: 'Errore',
          message: 'Riempire i campi obbligatori',
          popup: true, cancel: 'OK' },
      });
    } else {
      const requestData = { RequestData: this.model };
      this.ks.getKPIList(requestData).subscribe((data) => {
        this.kpis = data.ResponseData.output.listaKPI;
        this.fileDaElaborare =
          new MatTableDataSource<FileDaElaborare>(data.ResponseData.output.fileDaElaborare);
      });
    }
  }
}
