import { Component, NgZone, OnInit } from '@angular/core';
import { UploadService } from '../../shared/services/upload.service';
import { UtilService } from '../../shared/services/util.service';
import { CustomMatDialogComponent } from '../../shared/components/custom-mat-dialog/custom-mat-dialog.component';
import { MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { Canale } from '../../shared/models/canale.model';
import { AuthService } from '../../shared/services/auth/auth.service';
import { forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { UploadMassivoComponent } from '../upload-massivo/upload-massivo.component';

@Component({
  selector: 'bp-upload-singolo',
  templateUrl: './upload-singolo.component.html',
  styleUrls: ['./upload-singolo.component.css'],
})
export class UploadSingoloComponent extends UploadMassivoComponent implements OnInit {
  mesi: {}[];
  anni: number[];
  canali: Canale[];

  constructor(us: UploadService,
              zone: NgZone,
              dialog: MatDialog,
              route: ActivatedRoute,
              as: AuthService) {
    super(us, zone, dialog, route, as);
    this.anni = UtilService.years;
    this.mesi = UtilService.months;
  }

  ngOnInit() {
    jQuery('select').trigger('select');
  }

  public dropped(event: any) {
    if (event.files.length > 1) {
      this.dialog.open(CustomMatDialogComponent, {
        data: { title: 'Errore', message: 'Caricare un solo file', popup: true, cancel: 'OK' },
      });
      return false;
    }
    super.dropped(event);
  }

  public onSubmit() {
    const observableBatch = [];
    this.model.keys = super.getFilesToUpload(observableBatch);
    forkJoin(observableBatch).pipe(
      map((responses) => {
        return [].concat(...responses);
      })).subscribe((events) => {
        const unuploadedFiles = this.collectUnuploadedFiles(events);
        // console.log('File is completely uploaded!');
        // console.log("upload percentage", 1);
        this.us.saveUpload(this.model).subscribe(() => {
          const invalidFileNames = this.getNamesFromUnuploadedFiles(unuploadedFiles);
          this.dialog.open(CustomMatDialogComponent, {
            data: {
              title: 'Conferma',
              message: invalidFileNames.length === 0 ?
                'Tutti i report sono stati correttamente caricati' :
                `Tutti i report sono stati correttamente caricati tranne i seguenti: <ul>${invalidFileNames}</ul>`,
              popup: true,
              cancel: 'OK',
            },
          });
          this.cleanUpModel(unuploadedFiles);
        });
      },
                    (error) => {
                      const unuploadedFiles = this.collectUnuploadedFiles(error);
                      const invalidFileNames = this.getNamesFromUnuploadedFiles(unuploadedFiles);
          // console.log("error", error);
                      this.dialog.open(CustomMatDialogComponent, {
                        data: {
                          title: 'Conferma',
                          message: invalidFileNames.length === 0 ?
                'Tutti i report sono stati correttamente caricati' :
                `Tutti i report sono stati correttamente caricati tranne i seguenti: <ul>${invalidFileNames}</ul>`,
                          popup: true,
                          cancel: 'OK',
                        },
                      });
                      this.loading = false;
                    });
    return true;
  }

  public onChangeCanale(canale: Canale) {
    this.model.anno = undefined;
    this.model.mese = undefined;
    this.model.idCanale= canale.id;
    this.model.files = [];
    this.anni = UtilService.getYears(canale.dataInizioValid, canale.dataFineValid);
    if (this.anni.length === 0) {
      this.dialog.open(CustomMatDialogComponent, {
        data: {
          title: 'Attenzione',
          message: `Il canale selezionato è disattivo da troppo tempo (${UtilService.YEAR_EXP}
          anni),<br> selezionare un altro canale`,
          popup: true, cancel: 'OK',
        },
      }).afterClosed().subscribe(() => {
        this.model.canale = undefined;
      });
    }
  }

  public onChangeAnno(event) {
    this.model.mese = undefined;
    this.mesi =
      UtilService.getMonths(event.value,
                          this.model.canale.dataInizioValid,
                          this.model.canale.dataFineValid);
  }

  public confirm(form: any) {
    if (!form.valid) {
      this.dialog.open(CustomMatDialogComponent, {
        data: {
          title: 'Errore',
          message: 'Riempire i campi obbligatori',
          popup: true, cancel: 'OK',
        },
      });
    } else if (this.model.files.length > 0) {
      const dialogRef = this.dialog.open(CustomMatDialogComponent, {
        data: {
          title: 'Conferma',
          message: 'Attenzione: eventuali dichiarazioni in sovrapposizione con ' +
            'le precedenti verranno scartate. Confermi il caricamento?',
        },
      });

      dialogRef.afterClosed().subscribe((data) => {
        if (data) {
          this.onSubmit();
        }
        return data;
      });
    } else {
      this.dialog.open(CustomMatDialogComponent, {
        data: { title: 'ERRORE', message: 'Allegare un file', popup: true, cancel: 'OK' },
      });
      return false;
    }
  }
}
