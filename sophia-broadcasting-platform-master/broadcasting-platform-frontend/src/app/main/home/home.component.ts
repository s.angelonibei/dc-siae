import { Component } from '@angular/core';
import { News } from '../../shared/models/news.model';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../../shared/services/auth/auth.service';
import { User } from '../../shared/models/user.model';
import { AbstractService } from '../../shared/services/abstract.service';
import { HomeService } from '../../shared/services/home.service';
import { HttpResponse } from '@angular/common/http';
import {
  getFileNameFromResponseContentDisposition,
  saveFile,
} from '../../shared/file-download-helper';

@Component({
  selector: 'bp-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent {
  news: News[];
  servicesRoot: string;
  user: User;

  constructor(private route: ActivatedRoute,
              private as: AuthService,
              private hs: HomeService) {
    this.as.getUserLoggedIn().subscribe((user: User) => {
      this.route.data
        .subscribe((data: { news: News[] }) => {
          this.news = data.news;
        });
      this.user = user;
    });
    this.servicesRoot = AbstractService.SERVICES_ROOT;
  }

  public downloadTemplate(): void {
    console.log(this.user);
    this.hs.downloadTemplate(this.user.broadcasters.tipoBroadcaster,this.user.broadcasters.nuovoTracciato).subscribe((event) => {
      console.log("Sono nel servizio");
      if (event instanceof HttpResponse) {
        const name = getFileNameFromResponseContentDisposition(event);
        saveFile(event.body, name);
      }
    });
  }
}
