import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import {UploadedFilesService} from '../../shared/services/uploaded-files.service';

@Component({
  selector: 'bp-custom-mat-dialog',
  templateUrl: './confirm-report-dialog.component.html',
  styleUrls: ['./custom-mat-dialog.component.scss'],
})
export class ConfirmReportDialogComponent {

  constructor(
        public dialogRef: MatDialogRef<ConfirmReportDialogComponent>,
        private ufs: UploadedFilesService,
        @Inject(MAT_DIALOG_DATA) public data: any) { }

  onConfirm(): void {
    this.ufs.confirmUpload(this.data.idFile)
      .subscribe(data => {
        this.dialogRef.close(data);
      });
  }

}
