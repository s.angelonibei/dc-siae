import {AfterViewInit, ChangeDetectorRef, Component, ViewChild} from '@angular/core';
import { UploadedFilesService } from '../../shared/services/uploaded-files.service';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { Output } from '../../shared/models/uploaded-file.model';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { DatePipe, registerLocaleData } from '@angular/common';
import it from '@angular/common/locales/it';
import { getFileNameFromResponseContentDisposition, saveFile } from '../../shared/file-download-helper';
import { AuthService } from '../../shared/services/auth/auth.service';
import { User } from '../../shared/models/user.model';
import { ConfirmReportDialogComponent } from './confirm-report-dialog.component';
import {CancelReportDialogComponent} from './cancel-report-dialog.component';

@Component({
  selector: 'bp-download-report',
  templateUrl: './download-report.component.html',
  styleUrls: ['./download-report.component.css'],
})
export class DownloadReportComponent implements AfterViewInit {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  loading = false;
  user: User;
  uploadedFilesList: MatTableDataSource<Output>;

  displayedColumns = [
        // 'index',
    'dataUpload',
    'nomeFile',
    'stato',
    'tipoUpload',
    'download',
    'more'
  ];

  constructor(private route: ActivatedRoute,
              private ufs: UploadedFilesService,
              private dialog: MatDialog,
              private as: AuthService,
              private changeDetectorRefs: ChangeDetectorRef) {
    this.route.data.subscribe((data) => {
      this.uploadedFilesList = new MatTableDataSource(data.uploadedFiles);
      registerLocaleData(it);
      const datePipe = new DatePipe('it');
      this.uploadedFilesList.filterPredicate = (data2: Output, filter: string) =>
                datePipe.transform(data2.dataUpload, 'dd/MM/yyyy HH:mm:ss').indexOf(filter) !== -1
                || data2.nomeFile.toLowerCase().indexOf(filter.toLowerCase()) !== -1
                || data2.stato.toLowerCase().indexOf(filter.toLowerCase()) !== -1
                || data2.tipoUpload.toLowerCase().indexOf(filter.toLowerCase()) !== -1;
      this.as.getUserLoggedIn().subscribe((user: User) => {
        this.user = user;
      });
    });

  }

  ngAfterViewInit() {
    this.uploadedFilesList.sort = this.sort;
    this.uploadedFilesList.paginator = this.paginator;
  }

  applyFilter(filterValue: string) {
    this.uploadedFilesList.filter = filterValue;
  }

  downloadUploadedFile(id: number) {
    this.ufs.downloadUploadedFile(id).subscribe((event) => {
      this.loading = true;
      if (event instanceof HttpResponse) {
        this.loading = false;

        const name = getFileNameFromResponseContentDisposition(event);
        saveFile(event.body, name);
      }
    });
  }

  confirmUpload(id: number) {
    const dialogRef = this.dialog.open(ConfirmReportDialogComponent, {
      data: {idFile: id}
    });
    dialogRef.afterClosed().subscribe(data => this.updateItem(id, data));
  }

  cancelUpload(id: number) {
    const dialogRef = this.dialog.open(CancelReportDialogComponent, {
      data: {idFile: id}
    });
    dialogRef.afterClosed().subscribe(data => this.updateItem(id, data));
  }

  updateItem(id: number, data: object) {
    // @ts-ignore
    if (data.stato) {
      // @ts-ignore
      const toUpdate = this.uploadedFilesList.filteredData.find(item => item.id === id);
      // @ts-ignore
      toUpdate.stato = data.stato;
      this.changeDetectorRefs.detectChanges();
    }
  }
}

