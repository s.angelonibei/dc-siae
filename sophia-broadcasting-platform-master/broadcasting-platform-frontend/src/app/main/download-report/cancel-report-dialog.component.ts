import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import {UploadedFilesService} from '../../shared/services/uploaded-files.service';

@Component({
  selector: 'bp-custom-mat-dialog',
  templateUrl: './cancel-report-dialog.component.html',
  styleUrls: ['./custom-mat-dialog.component.scss'],
})
export class CancelReportDialogComponent {

  constructor(
        public dialogRef: MatDialogRef<CancelReportDialogComponent>,
        private ufs: UploadedFilesService,
        @Inject(MAT_DIALOG_DATA) public data: any) { }

  onConfirm(): void {
    this.ufs.cancelUpload(this.data.idFile)
      .subscribe(data => {
        this.dialogRef.close(data);
      });
  }

}
