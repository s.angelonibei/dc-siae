import { Component } from '@angular/core';
import { HomeComponent } from '../home/home.component';

@Component({
  selector: 'bp-home-altri-repertori',
  templateUrl: './home-altri-repertori.component.html',
  styleUrls: ['./home-altri-repertori.component.css'],
})
export class HomeAltriRepertoriComponent extends HomeComponent {
}
