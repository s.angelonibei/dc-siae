import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Broadcasters } from '../../shared/models/broadcasters.model';
import { Canale } from '../../shared/models/canale.model';
import { UtilService } from '../../shared/services/util.service';
import { DownloadHarmonizedFileService } from '../../shared/services/download-harmonized-file.service';
import { AuthService } from '../../shared/services/auth/auth.service';
import { CustomMatDialogComponent } from '../../shared/components/custom-mat-dialog/custom-mat-dialog.component';
import { MatDialog } from '@angular/material';
import { User } from '../../shared/models/user.model';
import { DownloadFileArmonizzato } from '../../shared/models/download.file.armonizzato.model';
import { BroadcasterConfig } from 'app/shared/models/broadcaster-config.model';
// import { TracciatoModel } from 'app/shared/models/tracciato.model';

@Component({
  selector: 'bp-download-file-armonizzato',
  templateUrl: './download-file-armonizzato.component.html',
  styleUrls: ['./download-file-armonizzato.component.css'],
})
export class DownloadFileArmonizzatoComponent implements OnInit {

  loading = false;
  mesi: {}[];
  anni: number[];
  canali: Canale[];
  // tracciati: TracciatoModel[];
  config: BroadcasterConfig;
  model: DownloadFileArmonizzato;

  constructor(private route: ActivatedRoute,
    private dhfs: DownloadHarmonizedFileService,
    private dialog: MatDialog,
    private as: AuthService) {

    this.route.data
      .subscribe((data: { canali: Canale[], config: BroadcasterConfig[] }) => {
        this.canali = data.canali;
        const { config } = data;
        this.config = this.getBroadcasterConfig(config);
      });
    this.mesi = UtilService.months;
    this.anni = UtilService.years;
    this.model = new DownloadFileArmonizzato();
    this.model.broadcasters = new Broadcasters();

  }

  ngOnInit() {
    // this.setUpTracciati();
    this.as.getUserLoggedIn().subscribe((user: User) => {
      console.log('Sono in getUserLoggedIn');
      console.log(this.model.broadcasters.nuovoTracciato);
      this.model.broadcasters = user.broadcasters;
      this.model.broadcasters.tipo_broadcaster = user.broadcasters.tipoBroadcaster;
      this.model.broadcasters.nuovoTracciato = user.broadcasters.nuovoTracciato;
    });
  }

  public onChange(event) {
    this.model.mesi = [];
    this.mesi = UtilService.getMonths(event.value);
  }

  public submit(form) {
    if (!form.valid) {
      this.dialog.open(CustomMatDialogComponent, {
        data: {
          title: 'Errore',
          message: 'Riempire i campi obbligatori',
          popup: true,
          cancel: 'OK'
        },
      });
    } else {
      this.dhfs.downloadHarmonizedFile(this.model).subscribe((event) => {
        this.loading = true;
        if (event instanceof HttpResponse) {
          this.loading = false;
          if (event.headers.get('content-type') === 'application/json' ||
            event.headers.get('content-type') === 'text/plain') {
            this.dialog.open(CustomMatDialogComponent, {
              data: { title: 'Conferma', message: event.body, popup: true, cancel: 'OK' },
            });
          } else {
            const data =
              new Blob([decodeURIComponent(
                encodeURI('\ufeff' + event.body.toString()))],
                { type: event.headers.get('content-type') });
            const a = document.createElement('a');
            const fileURL = URL.createObjectURL(data);
            a.href = fileURL;
            a.download = event.headers.get('content-disposition').split('"')[1];
            window.document.body.appendChild(a);
            a.click();
            window.document.body.removeChild(a);
            URL.revokeObjectURL(fileURL);
          }
        }
      }, () => {
        this.loading = false;
      });
    }
  }

  private getBroadcasterConfig(config: BroadcasterConfig[]) {
    return config && config.length === 1 ? config[0] : config.find(x => x.canali === null);
  }

  // private setUpTracciati() {
  //   this.tracciati = [{
  //     label: "Vecchio",
  //     value: false
  //   }, {
  //     label: "Nuovo",
  //     value: true
  //   }];
  // }

}
