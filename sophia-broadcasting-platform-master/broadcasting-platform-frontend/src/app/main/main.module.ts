import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DownloadFileArmonizzatoComponent} from './download-file-armonizzato/download-file-armonizzato.component';
import {NgxGaugeModule} from 'ngx-gauge';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatPaginatorModule,
  MatSelectModule,
  MatSidenavModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
} from '@angular/material';
import {FlexLayoutModule} from '@angular/flex-layout';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../shared/shared.module';
import {CustomMatDialogComponent} from '../shared/components/custom-mat-dialog/custom-mat-dialog.component';
import {AuthGuard} from '../shared/services/auth/auth-guard.service';
import {NewsResolver} from '../shared/resolver/news-resolver';
import {ConfigResolver} from '../shared/resolver/config-resolver';
import {CanaliResolver} from '../shared/resolver/canali-resolver';
import {KpiResolver} from '../shared/resolver/kpi-resolver';
import {UploadedFilesResolver} from '../shared/resolver/uploaded-files-resolver';
import {DownloadReportComponent} from './download-report/download-report.component';
import {UploadMassivoComponent} from './upload-massivo/upload-massivo.component';
import {AssistenzaComponent} from './assistenza/assistenza.component';
import {HomeComponent} from './home/home.component';
import {UploadSingoloComponent} from './upload-singolo/upload-singolo.component';
import {MonitoraggioComponent} from './monitoraggio/monitoraggio.component';
import {HomeAltriRepertoriComponent} from './home-altri-repertori/home-altri-repertori.component';
import {MainComponent} from './main.component';
import {UserResolver} from '../shared/resolver/user-resolver';
import {CanaleService} from '../shared/services/canale.service';
import {ConfirmReportDialogComponent} from './download-report/confirm-report-dialog.component';
import {CancelReportDialogComponent} from './download-report/cancel-report-dialog.component';

export const children = [

  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'home', component: HomeAltriRepertoriComponent, resolve: {
      news: NewsResolver,
    },
  },
  {
    path: 'upload/massivo', component: UploadMassivoComponent, resolve: {
      canali: CanaliResolver,
      config: ConfigResolver,
    },
  },
  {
    path: 'download/report', component: DownloadReportComponent, resolve: {
      uploadedFiles: UploadedFilesResolver,
    },
  },
];

const mainRoutes: Routes = [
  {
    path: '', component: MainComponent, /*resolve: {user: UserResolver}, */children: [
      { path: 'assistenza', component: AssistenzaComponent },
      {
        path: '', children: [
          { path: '', redirectTo: 'musica', pathMatch: 'full' },
          {
            path: 'musica', canActivateChild: [AuthGuard], children: [
              { path: '', redirectTo: 'home', pathMatch: 'full' },
              {
                path: 'home', component: HomeComponent, resolve: {
                  news: NewsResolver,
                },
              },
              {
                path: 'upload/singolo', component: UploadSingoloComponent, resolve: {
                  canali: CanaliResolver,
                  config: ConfigResolver,
                },
              },
              {
                path: 'upload/massivo', component: UploadMassivoComponent,
                resolve: {
                  canali: CanaliResolver,
                  config: ConfigResolver,
                },
              },
              {
                path: 'monitoraggio', component: MonitoraggioComponent,
                resolve: {
                  canali: CanaliResolver,
                  kpi: KpiResolver,
                },
              },
              {
                path: 'download/armonizzato', component: DownloadFileArmonizzatoComponent,
                resolve: {
                  canali: CanaliResolver,
                  config: ConfigResolver,
                },
              },
              {
                path: 'download/report', component: DownloadReportComponent,
                resolve: {
                  uploadedFiles: UploadedFilesResolver,
                },
              }],
          },
          {
            children,
            path: ':repertorio',
            canActivateChild: [AuthGuard],
          },
        ],
      },
    ],
  },
];

@NgModule({
  imports: [
    FlexLayoutModule,
    CommonModule,
    RouterModule,
    MatButtonModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatDatepickerModule,
    MatCardModule,
    MatMenuModule,
    MatMomentDateModule,
    MatSnackBarModule,
    MatExpansionModule,
    MatGridListModule,
    MatTableModule,
    MatTooltipModule,
    MatSortModule,
    MatPaginatorModule,
    MatTabsModule,
    MatIconModule,
    NgxGaugeModule,
    SharedModule,
    MatSidenavModule,
    RouterModule.forChild(mainRoutes),
  ],
  declarations: [
    HomeComponent,
    UploadSingoloComponent,
    MonitoraggioComponent,
    DownloadFileArmonizzatoComponent,
    UploadMassivoComponent,
    DownloadReportComponent,
    HomeAltriRepertoriComponent,
    MainComponent,
    AssistenzaComponent,
  ],
  providers: [
    CanaleService,
    CanaliResolver,
    NewsResolver,
    KpiResolver,
    ConfigResolver,
    UserResolver,
    UploadedFilesResolver,
  ],
  exports: [
    HomeComponent,
    UploadSingoloComponent,
    MonitoraggioComponent,
    DownloadFileArmonizzatoComponent,
    UploadMassivoComponent,
    DownloadReportComponent,
    HomeAltriRepertoriComponent,
    AssistenzaComponent,
  ],
  entryComponents: [
    CustomMatDialogComponent,
    ConfirmReportDialogComponent,
    CancelReportDialogComponent
  ],
  bootstrap: [
    MainComponent,
  ],
})
export class MainModule {
}
