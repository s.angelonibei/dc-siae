import { Component, OnInit } from '@angular/core';
import { CustomMatDialogComponent } from '../shared/components/custom-mat-dialog/custom-mat-dialog.component';
import { MatDialog } from '@angular/material';
import { RoutingStateService } from '../shared/services/routing-state.service';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'bp-not-found',
  template: '',
})
export class NotFoundComponent implements OnInit {

  constructor(private dialog: MatDialog,
              private routingState: RoutingStateService,
              private router: Router) {

  }

  ngOnInit() {
    this.dialog.open(CustomMatDialogComponent, {
      data: { title: 'Errore', message: 'Pagina non trovata', popup: true, cancel: 'OK' },
    }).afterClosed().subscribe((result) => {
            // Set our navigation extras object
            // that contains our global query params and fragment
      const navigationExtras: NavigationExtras = {
        fragment: 'anchor',
      };

            // Navigate to the login page with extras
      this.router.navigate(
        [this.routingState.getPreviousUrl() ?
          this.routingState.getPreviousUrl() : '/'],
        navigationExtras);
      return false;
    });
  }

}
