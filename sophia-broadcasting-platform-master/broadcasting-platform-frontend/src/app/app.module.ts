import {NgModule} from '@angular/core';
// import {ROUTES} from '@angular/router/src/router_config_loader';
// import {ANALYZE_FOR_ENTRY_COMPONENTS} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {
  ErrorStateMatcher,
  MAT_DATE_LOCALE,
  MatButtonModule,
  MatCardModule,
  MatDialogModule,
  MatIconModule,
  MatMenuModule,
  MatPaginatorIntl,
  MatProgressSpinnerModule,
  ShowOnDirtyErrorStateMatcher,
} from '@angular/material';
import {MatListModule} from '@angular/material/list';

import {AppComponent} from './app.component';
import {AuthService} from './shared/services/auth/auth.service';
import {AuthGuard} from './shared/services/auth/auth-guard.service';
import {UploadService} from './shared/services/upload.service';
import {User} from './shared/models/user.model';
import {EmailService} from './shared/services/email.service';
import {KPI} from './shared/models/kpi.model';
import {ResponseKpi} from './shared/models/response.kpi.model';
import {KpiService} from './shared/services/kpi.service';
import {NewsService} from './shared/services/news.service';
import {UploadedFilesService} from './shared/services/uploaded-files.service';
import {DownloadHarmonizedFileService} from './shared/services/download-harmonized-file.service';
import {CustomMatPaginatorIntl} from './shared/custom-mat-paginator-intl';
import {NotFoundComponent} from './not-found/not-found.component';
import {RoutingStateService} from './shared/services/routing-state.service';
import {RepertorioService} from './shared/services/repertorio.service';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTabsModule} from '@angular/material/tabs';
import {HttpErrorInterceptor} from './shared/interceptors/http.error.interceptor';
import {SharedModule} from './shared/shared.module';
import {ProgressModule} from './progress.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {CustomMatDialogComponent} from './shared/components/custom-mat-dialog/custom-mat-dialog.component';
import {AppRoutingModule} from './app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BrowserModule} from '@angular/platform-browser';
import {AccountComponent} from './shared/account/account.component';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';
import {TokenInterceptor} from './shared/interceptors/token.interceptor';
import {SessionManager} from './shared/models/token.storage.model';
import {LoginAlterEgoGuardService} from './shared/services/auth/login-alter-ego-guard.service';
import {ConfirmReportDialogComponent} from './main/download-report/confirm-report-dialog.component';
import {CancelReportDialogComponent} from './main/download-report/cancel-report-dialog.component';

@NgModule({
  declarations: [
    NotFoundComponent,
    AppComponent,
    AccountComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MatIconModule,
    MatMenuModule,
    MatButtonModule,
    MatToolbarModule,
    MatProgressSpinnerModule,
    MatTabsModule,
    MatListModule,
    SharedModule,
    AppRoutingModule,
    MatDialogModule,
    ProgressModule,
    HttpClientModule,
    MatCardModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production }),
  ],

  providers: [
    AuthService,
    LoginAlterEgoGuardService,
    AuthGuard,
    NewsService,
    UploadService,
    EmailService,
    KpiService,
    User,
    KPI,
    ResponseKpi,
    UploadedFilesService,
    DownloadHarmonizedFileService,
    RoutingStateService,
    RepertorioService,
    SessionManager,
    { provide: MatPaginatorIntl, useClass: CustomMatPaginatorIntl },
    { provide: MAT_DATE_LOCALE, useValue: 'it-IT' },
    { provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher },
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: HttpErrorInterceptor, multi: true },
  ],
  bootstrap: [
    AppComponent,
  ],
  entryComponents: [
    CustomMatDialogComponent,
    ConfirmReportDialogComponent,
    CancelReportDialogComponent
  ],
})
export class AppModule { }
