import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

import { MatProgressSpinnerModule } from '@angular/material';
import { ProgressInterceptor } from './shared/interceptors/progress.interceptor';
import { ProgressComponent } from './shared/progress/progress.component';
import { CommonModule } from '@angular/common';

const interceptor = new ProgressInterceptor();

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    MatProgressSpinnerModule,
  ],
  declarations: [
    ProgressComponent,
  ],
  providers: [
        { provide: ProgressInterceptor, useValue: interceptor },
        { provide: HTTP_INTERCEPTORS, useValue: interceptor, multi: true },
  ],
  exports: [
    ProgressComponent,
  ],
})
export class ProgressModule {}
