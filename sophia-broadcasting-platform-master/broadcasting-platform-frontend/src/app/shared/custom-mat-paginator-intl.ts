import { MatPaginatorIntl } from '@angular/material';

export class CustomMatPaginatorIntl extends MatPaginatorIntl {
  itemsPerPageLabel = 'elementi per pagina';
  nextPageLabel     = 'prossima';
  previousPageLabel = 'precedente';

  getRangeLabel = (page: number, pageSize: number, length: number) => {
    if (length === 0 || pageSize === 0) { return `0 di ${length}`; }

    Math.max(length, 0);

    const startIndex = page * pageSize;

        // If the start index exceeds the list length, do not try and fix the end index to the end.
    const endIndex = startIndex < length ?
            Math.min(startIndex + pageSize, Math.max(length, 0)) :
            startIndex + pageSize;

    return `${startIndex + 1} - ${endIndex} di ${Math.max(length, 0)}`;
  }
}
