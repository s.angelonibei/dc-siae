import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomMatDialogComponent } from './components/custom-mat-dialog/custom-mat-dialog.component';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatCardModule,
  MatDialogModule, MatFormFieldModule,
  MatInputModule,
  MatIconModule, MatListModule,
  MatMenuModule,
} from '@angular/material';
import { SanitizeHtmlPipe } from './pipes/sanitize-html.pipe';
import {CanaleSelectComponent} from './canale-select/canale-select.component';
import {BytesPipe} from './pipes/bytes.pipe';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FileDropModule} from 'ngx-file-drop';
import {ConfirmReportDialogComponent} from '../main/download-report/confirm-report-dialog.component';
import {CancelReportDialogComponent} from '../main/download-report/cancel-report-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    MatDialogModule,
    MatButtonModule,
    MatMenuModule,
    MatIconModule,
    MatCardModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    FileDropModule,
    MatListModule,
  ],
  declarations: [ConfirmReportDialogComponent,
    CancelReportDialogComponent, CustomMatDialogComponent, SanitizeHtmlPipe, CanaleSelectComponent, BytesPipe],
  exports: [ConfirmReportDialogComponent, CancelReportDialogComponent, CustomMatDialogComponent,
    CanaleSelectComponent,
    BytesPipe,
    CommonModule,
    FormsModule,
    MatDialogModule,
    MatButtonModule,
    MatMenuModule,
    MatIconModule,
    MatCardModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    FileDropModule,
    MatListModule,
  ],
})
export class SharedModule {
}
