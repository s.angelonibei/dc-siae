import {Canale} from './canale.model';
import {Broadcasters} from './broadcasters.model';
import {Palinsesto} from './mprs/palinsesto.model';
import {MusicProvider} from './mprs/music-provider.model';

export class KPIRequest {
  canali: Canale[];
  palinsesti: Palinsesto[];
  mesi: number[];
  anno: number;
  emittente: Broadcasters;
  musicProvider: MusicProvider;
  idUtente: number;
  tipoCanali: string;
}
