export class RequestUploadedFile {
  RequestData: RequestData;
}
export class RequestData {
  emittente: Emittente;
  stato: string;
  repertorio: string;
}
export class Emittente {
  id: number;
}

export class Esito {
  codice: string;
  descrizioneEsito: string;
}

export class Output {
  dataUpload: Date;
  id: string;
  nomeFile: string;
  stato: string;
  tipoUpload: string;
}

export class ResponseData {
  output: Output[];
}

export class ResponseUploadedFile {
  ResponseData: ResponseData;
}
