import {Broadcasters} from './broadcasters.model';
import {UtenteRepertorio} from './utente.repertorio.model';
import {MusicProvider} from './mprs/music-provider.model';
import {Ruolo} from './ruolo.model';

export class User {
  id: number;
  exp: number;
  sub: string;
  username: string;
  audience: string;
  broadcasters: Broadcasters;
  musicProvider: MusicProvider;
  email: string;
  image?: any;
  ruolo: Ruolo;
  token: string;
  repertori: UtenteRepertorio[];
  currentRepertorio: string;
}
