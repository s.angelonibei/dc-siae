import { Broadcasters } from './broadcasters.model';
import { Canale } from './canale.model';

export interface BroadcasterConfigTemplate {
  idBroadcasterConfigTemplate?: number;
  configurazione?: string;
  dataCreazione?: string;
  flagAttivo?: number;
  flagDefault?: number;
  flagTracciato: boolean;
  nomeTemplate?: string;
  tipoEmittente?: string;
}

export class BroadcasterConfig {
  configuration: BroadcasterConfigTemplate;
  broadcasters: Broadcasters;
  canali: Canale;
  creationDate?: string;
  creationUser?: string;
  fieldsMapping?: string;
  idBroadcasterConfig?: number;
  modifyDate?: string;
  modifyUser?: string;
  validFrom?: string;
  validTo?: string;
}
