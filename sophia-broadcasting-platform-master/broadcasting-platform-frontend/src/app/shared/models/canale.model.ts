export class Canale {
  id: number;
  idBroadcaster: number;
  nome: string;
  tipoCanale: string;
  generalista: number;
  active: number;
  specialRadio: number;
  dataInizioValid: Date;
  dataCreazione: Date;
  dataUltimaModifica: Date;
  utenteUltimaModifica: string;
  dataFineValid?: Date;

  toString() {
    return JSON.stringify(this);
  }

}
