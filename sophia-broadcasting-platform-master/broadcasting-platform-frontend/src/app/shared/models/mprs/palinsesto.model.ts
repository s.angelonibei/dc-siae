import {MusicProvider} from './music-provider.model';
import {PuntoVendita} from './punto-vendita.model';

export class Palinsesto {
  idPalinsesto: number;
  musicProvider: MusicProvider;
  puntiVendita: PuntoVendita[];
  nome: string;
  codiceDitta: string;
  attivo: boolean;
  dataCreazione: Date;
  dataInizioValidita: Date;
  dataFineValidita: Date;
  dataUltimaModifica: Date;
  utenteUltimaModifica: string;
}
