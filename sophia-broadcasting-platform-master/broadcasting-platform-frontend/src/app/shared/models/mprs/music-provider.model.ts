import {TipoMusicProvider} from './tipo-music-provider.model';
import {Palinsesto} from "./palinsesto.model";

export class MusicProvider {
  idMusicProvider: number;
  tipoMusicProvider: TipoMusicProvider;
  palinsesti: Palinsesto[];
  nome: string;
  dataInizioValidita: Date;
  dataFineValidita: Date;
  dataCreazione: Date;
  attivo: boolean;
}
