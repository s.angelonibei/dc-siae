import {Palinsesto} from './palinsesto.model';

export class PuntoVendita {
  idPuntoVendita: number;
  nome: string;
  palinsesto: Palinsesto;
}
