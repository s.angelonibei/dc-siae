import {MusicProvider} from './music-provider.model';

export class TipoMusicProvider {
  idTipoMusicProvider: number;
  nome: string;
  musicProviderList: MusicProvider[];
}
