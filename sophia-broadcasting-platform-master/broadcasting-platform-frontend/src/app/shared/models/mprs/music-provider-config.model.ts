import {MusicProvider} from './music-provider.model';
import {Palinsesto} from './palinsesto.model';

export class MusicProviderConfig {
  configuration: any;
  musicProvider: MusicProvider;
  palinsesto: Palinsesto;
}
