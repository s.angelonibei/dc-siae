export class News {
  title: string;
  news: string;
  flagForAllBdc: boolean;
  flagForAllBdcUser: boolean;
  pubblicationDate: Date;
}
