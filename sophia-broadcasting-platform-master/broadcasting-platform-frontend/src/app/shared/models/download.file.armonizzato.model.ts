import {Canale} from './canale.model';
import {Broadcasters} from './broadcasters.model';
import {Palinsesto} from './mprs/palinsesto.model';
import {MusicProvider} from "./mprs/music-provider.model";

export class DownloadFileArmonizzato {
  canali: Canale[];
  anno: number;
  mesi: number[];
  tracciato: boolean;
  broadcasters: Broadcasters;
  muiscProvider: MusicProvider;
  palinsesti: Palinsesto[];

}
