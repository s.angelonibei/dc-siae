import { User } from './user.model';
import { Repertorio } from './repertorio.model';

export class UtenteRepertorio {
  idUtenteRepertorio: number;
  defaultRepertorio: boolean;
  repertorio: Repertorio;
  utente: User;
}
