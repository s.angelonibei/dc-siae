import { User } from './user.model';

export class PasswordResetToken {
  id: number;
  utente: User;
}
