
export class ChangePassword {
  idUtente: number;
  ruolo: string;
  password: string;
  password2: string;
}
