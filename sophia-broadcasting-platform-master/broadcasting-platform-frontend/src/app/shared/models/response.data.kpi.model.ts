import { KPI } from './kpi.model';

export class OutputKpi {
  listaKPI: KPI[];
  fileDaElaborare: FileDaElaborare[];
}

export class ResponseDataKpi {
  input: any;
  output: OutputKpi;
}

export class FileDaElaborare {
  nomeFile: string;
  canale: string;
  dataUpload: Date;
  tipoUpload: string;
}
