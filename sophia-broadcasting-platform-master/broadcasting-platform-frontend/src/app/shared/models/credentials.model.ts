export class Credentials {
  accessKey: string;
  secretKey: string;
  instanceProfile: boolean;
}
