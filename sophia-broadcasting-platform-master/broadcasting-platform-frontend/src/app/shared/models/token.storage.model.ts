import { Injectable } from '@angular/core';
import { User } from './user.model';

const TOKEN_KEY = 'AuthToken';
const USER = 'user';

@Injectable()
export class SessionManager {
  constructor() { }

  public signOut() {
    window.sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.removeItem(USER);
    window.sessionStorage.clear();
  }

  public saveToken(token: string) {
    window.sessionStorage.removeItem(TOKEN_KEY);
    // window.sessionStorage.removeItem(USER);
    window.sessionStorage.setItem(TOKEN_KEY, token);
    // window.sessionStorage.setItem(USER, JSON.stringify(user));
  }

  public saveUser(user: User) {
    window.sessionStorage.removeItem(USER);
    window.sessionStorage.setItem(USER, JSON.stringify(user));
  }

  public getToken(): string {
    return window.sessionStorage.getItem(TOKEN_KEY);
  }

  public getUser(): User {
    return JSON.parse(window.sessionStorage.getItem(USER));
  }
}
