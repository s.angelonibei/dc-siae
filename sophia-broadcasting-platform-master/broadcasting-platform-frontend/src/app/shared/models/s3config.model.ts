import { Credentials } from './credentials.model';

export class S3Config {
  keyPrefix: string;
  bucket: string;
  credentials: Credentials;
  region: string;
  accessKey: string;
  amzSignature: string;
  policyBase64: string;
  amzDate: string;
  amzCredential: string;
  stack: boolean;


}
