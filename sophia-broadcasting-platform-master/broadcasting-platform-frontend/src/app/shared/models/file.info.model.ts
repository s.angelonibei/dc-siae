export class FileInfo {
  name: string;
  size: number;
  lastModified: string;
}
