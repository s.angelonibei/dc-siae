import { Canale } from './canale.model';
import {Palinsesto} from './mprs/palinsesto.model';

export class Upload {
  canale: Canale;
  palinsesto: Palinsesto;
  anno: number;
  mese: number;
  idUtente: number;
  idBroadcaster: number;
  idCanale: number;
  idMusicProvider: number;
  idPalinsesto: number;
  nomerepertorio: string;
  files: File[];
  filenames: string[];
  keys: string[];
}
