import { User } from './user.model';

export class Email {
  subject: string;
  body: string;
  files: File[];
  user: User;
}
