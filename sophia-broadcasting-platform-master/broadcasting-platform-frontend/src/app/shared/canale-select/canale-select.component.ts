import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { Canale } from '../models/canale.model';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'bp-canale-select',
  templateUrl: './canale-select.component.html',
  styleUrls: ['./canale-select.component.css'],
})
export class CanaleSelectComponent implements OnInit, OnChanges {
  @Input() value: Canale;
  @Input() required?: boolean;
  @Output() valueChange = new EventEmitter();

  canaleCtrl: FormControl;
  canali: Canale[];

  filteredOptions: Observable<any[]>;

  constructor(private route: ActivatedRoute) {
    this.canaleCtrl = new FormControl();
  }

  ngOnInit() {
    this.route.data
            .subscribe((data: { canali: Canale[] }) => {
              this.canali = data.canali;
            });
    this.filteredOptions = this.canaleCtrl.valueChanges
            .pipe(
                startWith(''),
                map(canale => canale ? this.filterOptions(canale) : this.canali.slice()),
            );
    this.canaleCtrl.setValue(this.value ? this.value : '');
  }

  ngOnChanges() {
    this.canaleCtrl.setValue(this.value ? this.value : '');
    if (!this.value) {
      this.canaleCtrl.reset();
    }
  }

  filterOptions(search: any) {
    if (!(search instanceof Object)) {
      return this.canali.filter(
                canale => canale.nome.toLowerCase().indexOf(search.toLowerCase()) !== -1,
            );
    }
  }

  selected(canale: Canale) {
    this.value = this.canali.find(
            x => x.id === canale.id,
        );

        // send to parent or do whatever you want to do
    this.valueChange.emit(canale);
  }

  getDisplayFn(canale: Canale): string {
    return canale ? canale.nome : '' + canale;
  }

}
