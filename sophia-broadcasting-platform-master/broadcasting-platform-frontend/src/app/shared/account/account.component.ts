import { Component, Input } from '@angular/core';
import { animate, keyframes, state, style, transition, trigger } from '@angular/animations';
import { User } from '../models/user.model';
import {Observable, ObservableInput} from 'rxjs';
import {AuthService} from '../services/auth/auth.service';

@Component({
  selector: 'bp-account',
  templateUrl: './account.component.html',
  animations: [
    trigger('focusPanel', [
      state('inactive', style({
        transform: 'scale(1)',
        backgroundColor: '#eee',
      })),
      state('active', style({
        transform: 'scale(1.1)',
        backgroundColor: '#cfd8dc',
      })),
      transition('inactive => active', animate('100ms ease-in')),
      transition('active => inactive', animate('100ms ease-out')),
    ]),

    trigger('movePanel', [

      transition('void => *', [
        animate(600, keyframes([
          style({ opacity: 0, transform: 'translateY(-200px)', offset: 0 }),
          style({ opacity: 1, transform: 'translateY(25px)', offset: .75 }),
          style({ opacity: 1, transform: 'translateY(0)', offset: 1 }),
        ])),
      ]),
    ]),
  ],
})
export class AccountComponent {
  user: User;
  showAccount = false;

  constructor(private authService: AuthService) {
    this.authService.getUserLoggedIn().subscribe((user) => {
      this.user = user;
    });
  }
}
