import { Injectable } from '@angular/core';
import { MatPaginatorIntl } from '@angular/material';

@Injectable()
export class ItalianMatPaginatorIntl extends MatPaginatorIntl {
  itemsPerPageLabel = 'Elementi per pagina:';
  nextPageLabel = 'Prossima';
  previousPageLabel = 'Precedente';

  getRangeLabel = (page: number, pageSize: number, length: number) => {
    if (length === 0 || pageSize === 0) { return `0 of ${length}`; }

    const startIndex = page * pageSize;

        // If the start index exceeds the list length, do not try and fix the end index to the end.
    const endIndex = startIndex < Math.max(length, 0) ?
            Math.min(startIndex + pageSize, Math.max(length, 0)) :
            startIndex + pageSize;

    return `${startIndex + 1} - ${endIndex} di ${Math.max(length, 0)}`;
  }
}
