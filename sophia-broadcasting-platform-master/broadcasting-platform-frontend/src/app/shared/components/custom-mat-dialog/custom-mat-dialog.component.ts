import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'bp-custom-mat-dialog',
  templateUrl: './custom-mat-dialog.component.html',
  styleUrls: ['./custom-mat-dialog.component.scss'],
})
export class CustomMatDialogComponent {

  constructor(
        public dialogRef: MatDialogRef<CustomMatDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): boolean {
    this.dialogRef.close();
    return false;
  }

}
