import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Observable} from 'rxjs';
import {MatProgressSpinner} from '@angular/material';
import {ProgressInterceptor} from '../interceptors/progress.interceptor';
import {map} from 'rxjs/operators';

export type ProgressBarColor = 'primary' | 'accent' | 'warn';

export type ProgressSpinnerMode = 'determinate' | 'indeterminate';

@Component({
  selector: 'pgs-progress-bar',
  template: `
    <div class="spinner" [style.display]="(progressPercentage$ | async) && (progressPercentage$ | async) === -1 || (progressPercentage$ | async) > 0  ? 'block' : 'none' ">
      <div>
        <mat-progress-spinner [value]="progressPercentage$ | async">
        </mat-progress-spinner>
        <div *ngIf="progressSpinner.mode !== 'indeterminate'">
            {{progressPercentage$ | async }} %
        </div>
      </div>
    </div>
  `,
  styleUrls: ['progress.component.scss'],
})
export class ProgressComponent implements OnInit {
  @Input() color: ProgressBarColor = 'primary';

  @ViewChild(MatProgressSpinner) progressSpinner: MatProgressSpinner;

  progressPercentage$: Observable<number>;

  constructor(private interceptor: ProgressInterceptor) {
  }

  ngOnInit() {
    this.progressPercentage$ = this.interceptor.progress$.pipe(
      map((progress) => {
        if (progress === -1) {
          this.setMode('indeterminate');
        } else if (progress > 0) {
          this.setMode('determinate');
        }
        return progress;
      }));
  }

  private setMode(mode: ProgressSpinnerMode) {
    this.progressSpinner.mode = mode;
  }
}
