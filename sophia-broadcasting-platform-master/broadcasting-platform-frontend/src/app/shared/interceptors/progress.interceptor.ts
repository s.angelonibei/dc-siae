import { Observable, Subject, ReplaySubject, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import {Injectable, InjectionToken} from '@angular/core';
import { HttpEvent, HttpEventType, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';

@Injectable()
export class ProgressInterceptor implements HttpInterceptor {
  // private static instance: ProgressInterceptor;
  public progress$: Observable<number | null>;
  private progressSubject: Subject<number | null>;

  private totalLoaded = 0;
  private requests: HttpRequest<any>[] = [];
  private percentage: number[] = [];

  /*static getInstance() {
    if (!ProgressInterceptor.instance) {
      ProgressInterceptor.instance = new ProgressInterceptor();
      // ... any one time initialization goes here ...
    }
    return ProgressInterceptor.instance;
  }*/
  constructor() {
    this.progressSubject = new ReplaySubject<number | null>(1);
    this.progress$ = this.progressSubject.asObservable();
  }

  intercept<T>(req: HttpRequest<T>, next: HttpHandler): Observable<HttpEvent<T>> {
        // console.log("Progress Interceptor");
    const reportingRequest = req.clone({ reportProgress: true });
    const handle = next.handle(reportingRequest);
    this.requests.push(req);
    this.percentage.push(0);

    return handle.pipe(tap((event: HttpEvent<T>) => {

      switch (event.type) {
        case HttpEventType.Sent:
                    // console.log(1,"sent");
          if (this.requests.length === 1) {
            this.progressSubject.next(null);
                        // console.log(2,"sent",null);
          }
          break;
        case HttpEventType.DownloadProgress:
        case HttpEventType.UploadProgress:
          if (event.total) {
            const index = this.requests.findIndex((element) => {
              return element === req;
            });
            this.percentage[index] = Math.round((event.loaded / event.total) * 100);
            for (const perc of this.percentage) {
              this.totalLoaded += perc;
            }
            this.progressSubject.next(Math.round(this.totalLoaded / this.percentage.length));
            this.totalLoaded = 0;
          } else {
            this.progressSubject.next(-1);
          }
          break;
        case HttpEventType.Response:
        // case HttpEventType.ResponseHeader:
          this.requests.pop();
          if (this.requests.length === 0) {
            this.progressSubject.next(null);
            this.percentage = [];
          }
          break;
      }
    }), catchError((error) => {
      this.requests.pop();
      if (this.requests.length !== 0) {
                // console.log(11, this.requests.length, error);
        this.requests = [];
      }
      this.progressSubject.next(null);
      this.percentage = [];
            // return the error to the method that called it
      return throwError(error);
    })) as any;
  }
}

export const PI = new InjectionToken<ProgressInterceptor>('');
