import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { SessionManager } from '../models/token.storage.model';
import {AbstractService} from '../services/abstract.service';

const TOKEN_HEADER_KEY = 'Authorization';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private token: SessionManager) {
  }

  intercept<T>(req: HttpRequest<T>, next: HttpHandler): Observable<HttpEvent<T>> {
    let authReq = req;
    // console.log('req.url.indexOf(\'s3.amazonaws.com\')', req.url.indexOf('s3.amazonaws.com'));
    if (this.token.getToken() && req.url.indexOf('s3.amazonaws.com') === -1) {
      authReq = req.clone({
        headers: req.headers.set(TOKEN_HEADER_KEY, 'Bearer ' + this.token.getToken()),
      });
    } else if (req.url.indexOf(AbstractService.S3_CONFIG_URL) !== -1) {
      authReq = req.clone({
        params: req.params.append('v', '' + (new Date().getMilliseconds())),
      });
    }
    return next.handle(authReq);
  }
}
