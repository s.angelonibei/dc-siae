import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppErrorHandler } from '../app-error-handler';
import { throwError } from 'rxjs/internal/observable/throwError';
import { catchError } from 'rxjs/operators';

@Injectable()
export class HttpErrorInterceptor extends AppErrorHandler implements HttpInterceptor {
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
            catchError((error: HttpErrorResponse) => {
              if (error.error instanceof Error) {
                    // A client-side or network error occurred. Handle it accordingly.
                console.error('An error occurred:',
                              error.error.message ?
                                error.error.message :
                                error.error ? error.error : error);
                this.handleError(error.error);
              } else {
                    // The backend returned an unsuccessful response code.
                    // The response body may contain clues as to what went wrong,
                console.error(`Backend returned code ${error.status ? error.status : error},
                body was: ${error.message ?
                  error.message :
                  error}`);
                this.handleError(error);
              }

                // ...optionally return a default fallback value so app can continue (pick one)
                // which could be a default value (which has to be a HttpResponse here)
                // return Observable.of(new HttpResponse({body: [{name: "Default value..."}]}));
                // or simply an empty observable
              return throwError(error);
            }));
  }
}
