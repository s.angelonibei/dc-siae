import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'bytes',
})
export class BytesPipe implements PipeTransform {

  transform(bytes: any, precision: number): string {
    if (!isFinite(bytes)) { return '-'; }
    const units = ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'];
    const number = Math.floor(Math.log(bytes) / Math.log(1024));
    return (bytes / Math.pow(1024, Math.floor(number)))
      .toFixed(precision ? precision : 1) +  ' ' + units[number];
  }

}
