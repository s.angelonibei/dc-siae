import { Injectable } from '@angular/core';
import { AbstractService } from './abstract.service';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpRequest } from '@angular/common/http';
import { Email } from '../models/email.model';
import { timeout } from 'rxjs/operators';

@Injectable()
export class EmailService extends AbstractService {

  public static readonly SEND_EMAIL_URL = environment.dataApiUrl + '/rest/mail/send';

  constructor(private http: HttpClient) {
    super();
  }

  sendMail(email: Email) {
    const formData: FormData = new FormData();
    for (const file of email.files) {
      formData.append('file', file, file.name);
    }
    formData.append('oggetto', email.subject);
    formData.append('testoMail', email.body);
    formData.append('idUtente', '' + email.user.id);
    formData.append('email', '' + email.user.email);
    if(email.user.broadcasters != null)
      formData.append('idBroadcaster', '' + email.user.broadcasters.id);

    const req = new HttpRequest('POST', `${EmailService.SEND_EMAIL_URL}`, formData, {
      reportProgress: true,
    });

    return this.http.request(req).pipe(timeout(600000));
  }
}
