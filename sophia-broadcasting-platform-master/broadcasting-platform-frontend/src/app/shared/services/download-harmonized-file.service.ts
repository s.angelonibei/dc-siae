import {Injectable} from '@angular/core';
import {AbstractService} from './abstract.service';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {DownloadFileArmonizzato} from '../models/download.file.armonizzato.model';

@Injectable()
export class DownloadHarmonizedFileService extends AbstractService {

  public static readonly DOWNLOAD_HARMONIZED_FILE_URL =
    environment.dataApiUrl + '/rest/broadcastingS3/fe/utilizationfile/downloadFileArmonizato';

  public static readonly DOWNLOAD_MP_HARMONIZED_FILE_URL =
    environment.dataApiUrl + '/rest/performing/radioInStore/armonizzato';

  constructor(private http: HttpClient) {
    super();
  }

  downloadHarmonizedFile(model: DownloadFileArmonizzato): Observable<any> {
      const req =
        new HttpRequest('POST',
          model.broadcasters ?
            `${DownloadHarmonizedFileService.DOWNLOAD_HARMONIZED_FILE_URL}` :
            `${DownloadHarmonizedFileService.DOWNLOAD_MP_HARMONIZED_FILE_URL}`
          ,
          model, {
            reportProgress: true,
            responseType: 'text',
          });
      return this.http.request(req);
  }
}
