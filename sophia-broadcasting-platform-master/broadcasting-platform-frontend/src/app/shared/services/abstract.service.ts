import { environment } from '../../../environments/environment';
import {HttpHeaders} from '@angular/common/http';

export abstract class AbstractService {

  public static readonly SERVICES_ROOT = environment.dataApiUrl;
  public static readonly S3_CONFIG_URL = AbstractService.SERVICES_ROOT + '/rest/config/s3';

// Generates Headers
  protected generateAuthOptions(): object {
    const headers = new HttpHeaders(
      {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': 'Origin, Authorization, Content-Type'});
    return { headers, observe: 'response' };
  }
}
