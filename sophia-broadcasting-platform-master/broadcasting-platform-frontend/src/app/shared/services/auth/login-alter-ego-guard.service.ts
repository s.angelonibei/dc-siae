import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanActivateChild,
  CanLoad,
  NavigationExtras,
  Route,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { AuthGuard } from './auth-guard.service';
import { AuthService } from './auth.service';
import { concatMap, map } from 'rxjs/operators';
import { Observable } from 'rxjs/internal/Observable';

@Injectable()
export class LoginAlterEgoGuardService implements CanActivate, CanActivateChild, CanLoad {

  constructor(private authGuard: AuthGuard,
              private authService: AuthService,
              private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.validateAndSaveToken('/', route.params.token);
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot,
                   state: RouterStateSnapshot): Observable<boolean> {
    return this.canActivate(childRoute, state);
  }

  canLoad(route: Route): Observable<boolean> {
    return this.validateAndSaveToken('/', route.path);
  }

  private validateAndSaveToken(url: string, token: string): Observable<boolean> {
    return this.authService.refreshToken(token).pipe(map(() => {
      if (this.authService.isAuthorized(url)) {
        const redirect =
          this.authService.redirectUrl ?
            this.authService.redirectUrl :
            '/';
        const navigationExtras: NavigationExtras = {
          preserveFragment: false,
          preserveQueryParams: false,
          replaceUrl: true,
        };
        this.router.navigateByUrl(redirect, navigationExtras);
        return false;
          // .pipe(
          //   map((res: boolean) => {
          //     return res;
          //   }),
          // );
      }
    }));
  }
}
