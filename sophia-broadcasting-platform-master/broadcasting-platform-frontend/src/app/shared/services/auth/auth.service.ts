import { Injectable } from '@angular/core';

import {BehaviorSubject, EMPTY, from, Observable, of} from 'rxjs';
import { flatMap, map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../../../environments/environment';
import { AbstractService } from '../abstract.service';
import { User } from '../../models/user.model';
import { ChangePassword } from '../../models/change-password.model';
import { Subject } from 'rxjs/internal/Subject';
import { SessionManager } from '../../models/token.storage.model';
import { UtenteRepertorio } from '../../models/utente.repertorio.model';
import {Repertorio} from '../../models/repertorio.model';

/**
 * AuthService uses JSON-Web-Token authorization strategy.
 * Fetched token and user details are stored in sessionStorage.
 */
@Injectable()
export class AuthService extends AbstractService {
  static readonly SIGNIN_URL = environment.authenticationApiUrl + '/api/auth/signin';
  static readonly REFRESH_TOKEN_URL = environment.authenticationApiUrl + '/api/auth/token/refresh';
  static readonly RESET_PASSWORD_URL =
    environment.authenticationApiUrl + '/api/auth/user/password/reset';
  static readonly UTENTI_URL = environment.authenticationApiUrl + '/rest/utenti';
  static readonly IS_TOKEN_VALID =
    environment.authenticationApiUrl + '/api/auth/user/password/token/valid';
  static readonly RESET_PASSWORD_CONFIG_URL =
    environment.authenticationApiUrl + '/api/auth/user/password/reset/config';
  static readonly CHANGE_PASSWORD_URL =
    environment.authenticationApiUrl + '/api/auth/user/password/change';
  private userLoggedIn$: Subject<User> = new BehaviorSubject<User>(null);
  private REDIRECT_URL: string;
  private USER: User;

  isLoginSubject = new BehaviorSubject<boolean>(this.hasToken());

  constructor(private http: HttpClient,
              private token: SessionManager) {
    super();
    this.refreshUserData();
  }

  get redirectUrl(): string {
    return this.REDIRECT_URL;
  }

  set redirectUrl(value: string) {
    this.REDIRECT_URL = value;
  }

  get user(): User {
    return this.USER;
  }

  set user(value: User) {
    this.USER = value;
  }

  getUserLoggedIn(): Observable<User> {
    return this.userLoggedIn$.asObservable();
  }

  setUserLoggedIn(user: User) {
    this.userLoggedIn$.next(user);
  }

  /**
   * Refreshes userId, username and token from sessionStorage
   */
  refreshUserData(): void {
    this.USER = this.token.getUser();
    // const token = this.token.getToken();
    // if (token) {
    //   this.saveUserDetails(token);
    // }
  }

  /**
   * Fetches and saves token for given user
   * @param username
   * @param password
   */
  signIn(username: string, password: string): Observable<any> {
    const requestParam = {
      username,
      password,
    };
    return this.http.post(AuthService.SIGNIN_URL, requestParam, this.generateAuthOptions())
      .pipe(
        map((res: any) => {
          this.token.saveToken(res.headers.get('Authorization'));
          this.saveUserDetails(this.token.getToken());
        }), flatMap(() => {
          return this.returnRepertori();
        })
      );
  }

  private returnRepertori() {
    return this.getRepertori(this.USER).pipe(map(
      (repertori: UtenteRepertorio[]) => {
        this.USER.repertori = repertori;
        this.token.saveUser(this.USER);
        this.isLoginSubject.next(true);
      }));
  }

  /**
   * Removes token and user details from sessionStorage and service's variables
   */
  logout() {
    this.setUserLoggedIn(null);
    this.isLoginSubject.next(false);
  }

  /**
   * Refreshes token for the user with given token
   * @param token - which should be refreshed
   */
  refreshToken(token: string): Observable<void> {
    this.token.saveToken(token);
    return this.http.get(AuthService.REFRESH_TOKEN_URL)
      .pipe(
        map((res: { token }) => {
          this.token.saveToken(res.token);
          this.saveUserDetails(this.token.getToken());
        }),
        flatMap(() => {
          return this.returnRepertori();
        }),
        );
  }

  /**
   * Checks if user is authorized
   * @return true is user authorized (there is token in sessionStorage) else false
   */
  isAuthorized(url?: string): boolean {
    let authorized = false;
    if (this.USER) {
      if (url) {
        authorized = this._isAuthorized(url, this.USER);
        switch (this.USER.ruolo.ruolo) {
          case 'MP':
            if (authorized) {
              if (!url.startsWith('/mprs')) {
                this.REDIRECT_URL = '/mprs'  + this.REDIRECT_URL;
              }
            }
            break;
        }
        if (authorized) {
          this.token.saveUser(this.USER);
          this.setUserLoggedIn(this.USER);
          return true;
        }
        return false;
      }
    } else {
      // url.indexOf('');
      return false;
    }
  }

  private _isAuthorized(url: string, user: User): boolean {
    if (url !== '/' && !url.startsWith('/mprs')) {
      const repertorio = url.split('/');
      return user.repertori.some((rep: UtenteRepertorio) => {
        if (rep.repertorio.urlPath === repertorio[1]) {
          user.currentRepertorio = rep.repertorio.nome;
          this.setUserLoggedIn(user);
          return true;
        }
        return false;
      });
    } if (!user.currentRepertorio) {
      const defaultRepertorio: UtenteRepertorio =
        user.repertori.find((repertorio: UtenteRepertorio) => {
          return repertorio.defaultRepertorio;
        });
        this.REDIRECT_URL = '/' + defaultRepertorio.repertorio.urlPath;
        user.currentRepertorio = defaultRepertorio.repertorio.nome;
    } else {
      this.REDIRECT_URL = '/' + user.currentRepertorio.toLowerCase();
    }
    return true;
  }

  // Saves user details with token into sessionStorage as user item
  /*  private saveToken(res): void {
      const response = res && res.token;
      if (response) {
        const token = response;
        const claims = this.getTokenClaims(token);
        claims.token = token;
        // FIXME sessionStorage.setItem('user', JSON.stringify(claims));
      } else {
        throw Error(res);
      }
    }*/

  // Saves user details into service properties
  private saveUserDetails(user: string): void {
    this.USER = this.getTokenClaims(user);
    if (this.USER.broadcasters) {
      this.USER.broadcasters.tipo_broadcaster = this.USER.broadcasters.tipoBroadcaster;
      this.USER.broadcasters.nuovoTracciato = this.USER.broadcasters.nuovoTracciato;
    }
    this.USER.token = this.USER.sub;
    this.setUserLoggedIn(this.USER);
  }

  // Retrieves user details from token
  private getTokenClaims(token: string): any {
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace('-', '+').replace('_', '/');
    return JSON.parse(window.atob(base64));
  }

  public getRepertori(user: User, isAlterEgo?: boolean): Observable<UtenteRepertorio[]> {
      if (user.broadcasters) {
        return this.
          http.
           get<UtenteRepertorio[]>(AuthService.UTENTI_URL +
          `/${user.id}/repertori/?isAlterEgo=${!!isAlterEgo}`);
      } else {
        const currentRepertorio = 'MUSICA';
        const utenteRepertorio = new UtenteRepertorio();
        utenteRepertorio.defaultRepertorio = true;
        utenteRepertorio.repertorio = new Repertorio();
        utenteRepertorio.repertorio.nome = currentRepertorio;
        utenteRepertorio.repertorio.urlPath = currentRepertorio.toLowerCase();
        utenteRepertorio.repertorio.descrizione = currentRepertorio.toLowerCase();
        const repertori: UtenteRepertorio[] = [utenteRepertorio];
        return of(repertori);
      }
  }

  public resetPassword(email: string) {
    return this.http.post<boolean>(AuthService.RESET_PASSWORD_URL, { email });
  }

  public isTokenValid(token: string) {
    return this.http.get<any>(AuthService.IS_TOKEN_VALID + `/${token}`);
  }

  public getResetPasswordConfig() {
    return this.http.get<any>(AuthService.RESET_PASSWORD_CONFIG_URL);
  }

  public changePassword(changePassword: ChangePassword) {
    return this.http.post<any>(AuthService.CHANGE_PASSWORD_URL,
                               changePassword);
  }

  private hasToken() {
    return !!this.token.getUser();
  }

  isLoggedIn(): Observable<boolean> {
    return this.isLoginSubject.asObservable();
  }
}
