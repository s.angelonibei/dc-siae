import {Injectable} from '@angular/core';
import {AbstractService} from './abstract.service';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Palinsesto} from '../models/mprs/palinsesto.model';
import {map} from 'rxjs/operators';

@Injectable()
export class PalinsestoService extends AbstractService {

  public static readonly PALINSESTI_URL = environment.dataApiUrl + '/rest/performing/radioInStore/musicProviders';

  constructor(private http: HttpClient) {
    super();
  }

  getPalinsesti(idMusicProvider: number): Observable<Palinsesto[]> {
    return this.http.get<Palinsesto[]>(`${PalinsestoService.PALINSESTI_URL}/${idMusicProvider}/palinsesti`).pipe(
        map((response: any) => <Palinsesto[]>[].concat(response.perfPalinsesto)));
  }
}
