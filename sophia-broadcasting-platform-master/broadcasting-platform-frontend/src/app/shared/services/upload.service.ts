import { Injectable } from '@angular/core';
import { AbstractService } from './abstract.service';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
// import * as AWS from 'aws-sdk';
import { S3Config } from '../models/s3config.model';
import { UtilService } from './util.service';
import * as CryptoJS from 'crypto-js';
import { Upload } from '../models/upload.model';
import { BroadcasterConfig } from '../models/broadcaster-config.model';
import {concatMap, map, mergeMap} from 'rxjs/operators';

@Injectable()
export class UploadService extends AbstractService {

  constructor(private http: HttpClient) {
    super();

    // this.http.get<S3Config>(AbstractService.S3_CONFIG_URL).subscribe((data) => {
    //   AWS.config.update({
    //     region: data.region,
    //     credentials: {
    //       accessKeyId: data.credentials.accessKey,
    //       secretAccessKey: data.credentials.secretKey,
    //     },
    //   });
    //   this.s3Config = data;
    // });
  }
  public static readonly UPLOAD_URL =
      environment.dataApiUrl + '/rest/broadcastingS3/fe/utilizationfile/upload';
  public static readonly UPLOAD_MPRS_URL =
    // environment.dataApiUrl + '/rest/perfRsUtilizationFiles';
    environment.dataApiUrl + '/rest/performing/radioInStore/utilizationFiles';
  public static readonly CONFIG_FORMATS = environment.dataApiUrl + '/rest/config/formats';

  // private s3: AWS.S3;
  // private s3Config: S3Config;

  // public delete(keys: string[]) {
  //   if (keys && keys.length > 0) {
  //     const params = {
  //       Bucket: this.s3Config.bucket,
  //       Delete: {
  //         Objects: []
  //       }
  //     };
  //     for (const key of keys) {
  //       params.Delete.Objects.push({Key: key});
  //     }
  //     return this.s3.deleteObjects(params);
  //   }
  // }

   public upload(file: any, key: string): Observable<any> {
    if (file) {
      return this.http.get<S3Config>(AbstractService.S3_CONFIG_URL).pipe(concatMap((s3Conf => {
        const formData = new FormData();
        formData.append('acl', 'public-read');
        formData.append('Content-Type', file.type);
        formData.append('X-Amz-Date', s3Conf.amzDate);
        formData.append('X-Amz-Algorithm', 'AWS4-HMAC-SHA256');
        formData.append('X-Amz-Credential', s3Conf.amzCredential);
        formData.append('X-Amz-Signature', s3Conf.amzSignature);
        formData.append('Policy', s3Conf.policyBase64);
        formData.append('key', s3Conf.keyPrefix + '/' + key);
        formData.append('file', file);
        return this.http.post(`https://${s3Conf.bucket}.s3.amazonaws.com/`, formData);
      })));
    }
  }

  public saveUpload(model: Upload): Observable<any> {
    // const body: Object = {};
    // for (const k in model) {
    //   if (k !== 'files') {
    //     if (k === 'canale') {
    //       body['idCanale'] = model[k].id;
    //     } else if (k === 'palinsesto') {
    //       body['idPalinsesto'] = model[k].idPalinsesto;
    //     } else {
    //       body[k] = model[k];
    //     }
    //   } else {
    //     let i = 0;
    //     for (const j in model[k]) {
    //       if (i === 0) {
    //         body['filenames'] = [];
    //         body['keys'] = [];
    //       }
    //       body['filenames'].push(model[k][j].name);
    //       body['keys'].push(model.keys[j]);
    //       i += 1;
    //     }
    //   }
    // }
    if (model.canale) {
      model.idCanale = model.canale.id;
    }
    return model.idBroadcaster ? this.http.post(UploadService.UPLOAD_URL, model) : this.http.post(UploadService.UPLOAD_MPRS_URL, model);
  }

  public getConfig(idBroadcaster: number): Observable<any> {
    return this.http.get<BroadcasterConfig[]>(`${UploadService.CONFIG_FORMATS}/${idBroadcaster}`);
  }
}
