import { Injectable } from '@angular/core';
import { AbstractService } from './abstract.service';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Canale } from '../models/canale.model';

@Injectable()
export class CanaleService extends AbstractService {

  public static readonly CANALI_URL = environment.dataApiUrl + '/rest/broadcasting/canali';

  constructor(private http: HttpClient) {
    super();
  }

  getCanali(id?: number): Observable<Canale> {
    return id && id !== 0 ?
            this.http.get<Canale>(`${CanaleService.CANALI_URL}/${id}`) :
            this.http.get<Canale>(CanaleService.CANALI_URL);
  }
}
