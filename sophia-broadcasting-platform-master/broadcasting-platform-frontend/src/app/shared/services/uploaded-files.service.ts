import {Injectable} from '@angular/core';
import {AbstractService} from './abstract.service';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpParams, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {
  Emittente,
  Output,
  RequestData,
  RequestUploadedFile,
  ResponseUploadedFile
} from '../models/uploaded-file.model';
import {map} from 'rxjs/operators';

@Injectable()
export class UploadedFilesService extends AbstractService {

  public static readonly UPLOADED_BROAADCASTER_FILES_URL =
    environment.dataApiUrl + '/rest/broadcastingS3/fe/utilizationfile/listfilebroadcaster';
  public static readonly UPLOADED_MUSIC_PROVIDER_FILES_URL =
    environment.dataApiUrl + '/rest/perfMusicProviders/';
  public static readonly DOWNLOAD_UPLOADED_FILE_URL =
    environment.dataApiUrl + '/rest/broadcastingS3/fe/utilizationfile/download/';
  public static readonly DOWNLOAD_MUSIC_PROVIDER_UPLOADED_FILE_URL =
    environment.dataApiUrl + '/rest/performing/radioInStore/utilizationFiles/';
  public static readonly CONFIRM_UPLOADED_FILE_URL =
    environment.dataApiUrl + '/rest/broadcasting/confirmupload';
  public static readonly CANCEL_UPLOADED_FILE_URL =
    environment.dataApiUrl + '/rest/broadcasting/cancelupload';

  constructor(private http: HttpClient) {
    super();
  }

  getUploadedFilesForBroadcasters(idBroadcaster: number, stato?: string, repertorio?: string):
    Observable<Output[]> {
    const requestUploadedFile: RequestUploadedFile = new RequestUploadedFile;
    const requestData: RequestData = new RequestData;
    const emittente: Emittente = new Emittente;

    emittente.id = idBroadcaster;
    if (stato) {
      requestData.stato = stato;
    }
    requestData.emittente = emittente;
    requestData.repertorio = repertorio;
    requestUploadedFile.RequestData = requestData;

    return this.http.post<ResponseUploadedFile>(UploadedFilesService.UPLOADED_BROAADCASTER_FILES_URL,
      requestUploadedFile)
      .pipe(
          map((response: ResponseUploadedFile) => {
            return response.ResponseData.output;
          })
       );
  }

  getUploadedFilesForMusicProviders(idMusicProvider: number, stato?: string):
    Observable<Output[]> {
    return this.http.get<ResponseUploadedFile>
    (`${UploadedFilesService.UPLOADED_MUSIC_PROVIDER_FILES_URL}${idMusicProvider}/utilizationFiles`,
      stato ?
        {
          params: new HttpParams().set('stato', stato)
        } :
        {},
    ).pipe(
      map((response: any) => {
        return response._embedded.perfRsUtilizationFiles;
      })
    );
  }

  downloadUploadedFile(idFile: number): Observable<any> {
    const req = new HttpRequest('GET',
      `${UploadedFilesService.DOWNLOAD_UPLOADED_FILE_URL}${idFile}`, {
        reportProgress: true,
        responseType: 'blob',
      });
    return this.http.request(req);
  }

  downloadUploadedMusicProviderFile(idFile: number): Observable<any> {
    const req = new HttpRequest('GET',
      `${UploadedFilesService.DOWNLOAD_MUSIC_PROVIDER_UPLOADED_FILE_URL}${idFile}`, {
        reportProgress: true,
        responseType: 'blob',
      });
    return this.http.request(req);
  }

  confirmUpload(idFile: number): Observable<any> {
    return this.http.post(UploadedFilesService.CONFIRM_UPLOADED_FILE_URL, {id: idFile});
  }

  cancelUpload(idFile: number): Observable<any> {
    return this.http.post(UploadedFilesService.CANCEL_UPLOADED_FILE_URL, {id: idFile});
  }
}
