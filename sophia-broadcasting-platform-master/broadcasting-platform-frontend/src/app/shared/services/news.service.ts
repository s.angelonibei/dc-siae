import { Injectable } from '@angular/core';
import { AbstractService } from './abstract.service';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/user.model';
import { News } from '../models/news.model';

@Injectable()
export class NewsService extends AbstractService {

  public static readonly BROADCASTER_NEWS_URL = environment.dataApiUrl + '/rest/broadcastingNews/listNews';
  public static readonly MUSIC_PROVIDER_NEWS_URL = environment.dataApiUrl + '/rest/musicProviders/';
  constructor(private http: HttpClient) {
    super();
  }

  getActiveNewsForBroadcaster(user: User): Observable<News> {
    const request: {} = {
      idUser: user.id,
      idBroadcaster: user.broadcasters.id,
    };
    return this.http.post<News>(NewsService.BROADCASTER_NEWS_URL, request);
  }

  getActiveNewsForMusicProvider(userId: number): Observable<News> {
    return this.http.get<News>(`${NewsService.MUSIC_PROVIDER_NEWS_URL}${userId}/news`);
  }
}
