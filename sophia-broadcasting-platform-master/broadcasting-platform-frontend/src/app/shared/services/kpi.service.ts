import {Injectable} from '@angular/core';
import {AbstractService} from './abstract.service';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {KPIRequest} from '../models/kpi-request.model';
import {OutputKpi} from '../models/response.data.kpi.model';
import {ResponseKpi} from '../models/response.kpi.model';
import {RequestData} from '../models/uploaded-file.model'

@Injectable()
export class KpiService extends AbstractService {

  public static readonly KPI_URL =
    environment.dataApiUrl + '/rest/broadcastingKPI/fe/monitoraggio/kpi';
  public static readonly KPI_MUSIC_PROVIDER_URL =
    environment.dataApiUrl + '/rest/performing/radioInStore/kpis';

  constructor(private http: HttpClient) {
    super();
  }

  getKPIList(requestData: any): Observable<ResponseKpi> {
    return this.http.post<ResponseKpi>(KpiService.KPI_URL, requestData);
  }
  getKPIListForMusicProvider(requestData: KPIRequest): Observable<OutputKpi> {
    return this.http.post<OutputKpi>(KpiService.KPI_MUSIC_PROVIDER_URL, requestData);
  }

}
