import { Injectable } from '@angular/core';
import { AbstractService } from './abstract.service';
import { HttpClient, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root',
})
export class HomeService extends AbstractService {

  private static readonly DOWNLOAD_TEMPLATE_URL =
    AbstractService.SERVICES_ROOT + '/rest/broadcastingS3/fe/utilizationfile/download/armonizzato/';

  constructor(private http: HttpClient) {
    super();
  }

  public downloadTemplate(tipoBroadcaster: string,nuovoTracciato: string): Observable<any> {
    const req = new HttpRequest('GET',
                                `${HomeService.DOWNLOAD_TEMPLATE_URL}${tipoBroadcaster}/${nuovoTracciato}`, {
                                  reportProgress: true,
                                  responseType: 'blob',
                                });
    return this.http.request(req);
  }
}
