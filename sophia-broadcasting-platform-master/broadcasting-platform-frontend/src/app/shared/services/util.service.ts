import { Injectable } from '@angular/core';
import * as moment from 'moment';
import * as CryptoJS from 'crypto-js';

@Injectable()
export class UtilService {
  public static YEAR_EXP = 7;
  public static get months(): {}[] {
    moment.locale('it');
    const mesi: {}[] = [];
    for (let entry = 1; entry <= 12; entry += 1) {
      mesi.push(
        {
          id: entry,
          label: moment('' + entry, 'M').format('MMMM'),
        },
      );
    }
    return mesi;
  }

  public static getMonths(selectedYear: number, validityStartDate?: Date, validityEndDate?: Date): {}[] {
    moment.locale('it');
    const lastDate = validityEndDate ? moment(validityEndDate) : moment();
    const selectedYearMoment = moment(selectedYear, 'YYYY');
    let months = 12;
    if (lastDate.year() === selectedYearMoment.year()) {
      months = lastDate.month() + 1;
    }
    const mesi: {}[] = [];
    for (let entry = validityStartDate && +moment(validityStartDate).format('YYYY') === selectedYear ?
      +moment(validityStartDate).format('M') :
      1;
         entry <= months;
         entry += 1) {
      mesi.push(
        {
          id: entry,
          label: moment('' + entry, 'M').format('MMMM'),
        },
      );
    }
    return mesi;
  }

  public static get years(): number[] {
    moment.locale('it');
    const anni: number[] = [];
    for (let entry = +moment().format('YYYY');
         entry >= +moment().format('YYYY') - this.YEAR_EXP; entry -= 1) {
      anni.push(entry);
    }
    return anni;
  }

  public static getYears(validityStartDate?: Date, validityEndDate?: Date): number[] {
    moment.locale('it');
    const anni: number[] = [];
    for (let entry = +moment(validityEndDate).format('YYYY');
         entry >= (validityStartDate ?
           +moment(validityStartDate).format('YYYY') :
           +moment().format('YYYY') - this.YEAR_EXP);
         entry -= 1) {
      anni.push(entry);
    }
    return anni;
  }

  /*public static getYearsPalinsesti(validityStartDate?: Date, validityEndDate?: Date): number[] {
    moment.locale('it');
    const anni: number[] = [];
    for (let entry = +moment(validityEndDate).format('YYYY');
         entry >= (validityStartDate ?
           (+moment(validityEndDate).format('YYYY') - this.YEAR_EXP >
             +moment(validityStartDate).format('YYYY') ?  ):
           +moment().format('YYYY') - this.YEAR_EXP);
         entry -= 1) {
      anni.push(entry);
    }
    return anni;
  }*/

  public static generateSignatureKey(key, dateStamp, regionName, serviceName) {
    const kDate = CryptoJS.HmacSHA256(dateStamp, 'AWS4' + key);
    const kRegion = CryptoJS.HmacSHA256(regionName, kDate);
    const kService = CryptoJS.HmacSHA256(serviceName, kRegion);
    return CryptoJS.HmacSHA256('aws4_request', kService);
  }

  public static generateTimestamp() {
    const date = new Date();
    const year = date.getFullYear();
    const month = ('0' + (date.getMonth() + 1)).slice(-2);
    const day = ('0' + date.getDate()).slice(-2);
    return year + month + day;
  }

  public static escapeRegex(string: string) {
    return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
  }
}
