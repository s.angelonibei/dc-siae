import { Injectable } from '@angular/core';
import { AbstractService } from './abstract.service';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UtenteRepertorio } from '../models/utente.repertorio.model';

@Injectable()
export class RepertorioService extends AbstractService {

  static readonly REPERTORI = environment.dataApiUrl + '/rest/utentirepertorio';

  constructor(private http: HttpClient) {
    super();
  }

  getAllRepertori(): Observable<UtenteRepertorio[]> {
    return this.http.get<UtenteRepertorio[]>(RepertorioService.REPERTORI);
  }
}
