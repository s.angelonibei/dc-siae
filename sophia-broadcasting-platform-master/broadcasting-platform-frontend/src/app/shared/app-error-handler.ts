import {ErrorHandler, Injectable, Injector} from '@angular/core';
import {FORBIDDEN, NOT_FOUND, UNAUTHORIZED} from 'http-status-codes';
import {NavigationExtras, Router} from '@angular/router';
import {CustomMatDialogComponent} from './components/custom-mat-dialog/custom-mat-dialog.component';
import {MatDialog} from '@angular/material';
import {RoutingStateService} from './services/routing-state.service';
import {EmailService} from './services/email.service';
import {SessionManager} from './models/token.storage.model';
import {Location} from '@angular/common';

@Injectable()
export class AppErrorHandler implements ErrorHandler {

    // static readonly REFRESH_PAGE_ON_TOAST_CLICK_MESSAGE: string =
    // "An error occurred: Please click this message to refresh";
    // static readonly DEFAULT_ERROR_TITLE: string = "Something went wrong";

  constructor(private injector: Injector,
              private routingState: RoutingStateService,
              private es: EmailService,
              private session: SessionManager,
              private _location: Location) {
  }

  public handleError(error: any) {
    const router = <Router>this.injector.get(Router);

        // console.error(error);

    const navigationExtras: NavigationExtras = {
      fragment: 'anchor',
    };

    const httpErrorCode = error.httpErrorCode ? error.httpErrorCode : error.status;

    switch (httpErrorCode) {
      case UNAUTHORIZED:
      case FORBIDDEN:
        // this.showError(error).afterClosed().subscribe(() => {
        this.showError(error.error ? error.error : error ).afterClosed().subscribe(() => {
          router.navigate(['login'], navigationExtras);
        });
        break;
      case NOT_FOUND:
        this.showError(error.error ? error.error : error ).afterClosed().subscribe(() => {
          this.routingState.getPreviousUrl() ? this._location.back() : router.navigate([ '/'], navigationExtras);
        });
        break;
      default:
        this.showError('C\'è stato un problema, riprovare in un secondo momento');
        // const email: Email = {
        //   body: (error.message ? error.message : error),
        //   subject: 'Automatic Error Catch',
        //   user: this.session.getUser(),
        //   files: undefined,
        // };
        // this.es.sendMail(email).subscribe();
        break;
    }
  }

  private showError(error: any) {
    const dialog = <MatDialog>this.injector.get(MatDialog);
    return dialog.open(CustomMatDialogComponent, {
      data: {
        title: 'Errore',
        message: error && error.message ? error.message : error,
        popup: true,
        cancel: 'OK',
      },
    });

  }
}
