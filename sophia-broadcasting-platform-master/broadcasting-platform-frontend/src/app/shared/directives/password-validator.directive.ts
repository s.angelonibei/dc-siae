import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator, ValidatorFn } from '@angular/forms';
import { Directive, Input } from '@angular/core';

export function matchPassword(password: string, confirmPassword: string): ValidatorFn {
  return (c: AbstractControl): { [key: string]: any } => {
    if (c.get(password).value !== c.get(confirmPassword).value) {
      // console.log('false');
      // c.get(password).setErrors({matchPassword: true})
      return { matchPassword: true };
    }
    // console.log('true');
    return null;
  };
}

@Directive({
  selector: '[matchPassword]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: PasswordValidatorDirective,
      multi: true,
    },
  ],
})
export class PasswordValidatorDirective implements Validator {
  @Input('matchPassword') matchPassword: string;
  @Input('matchPassword2') matchPassword2: string;

  validate(c: AbstractControl): ValidationErrors | null {
    return matchPassword(this.matchPassword, this.matchPassword2)(c);
  }
}
