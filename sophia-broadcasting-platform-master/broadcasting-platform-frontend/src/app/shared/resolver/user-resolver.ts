import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { User } from '../models/user.model';
import { AuthService } from '../services/auth/auth.service';

@Injectable()
export class UserResolver implements Resolve<User> {
  constructor(private as: AuthService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User> {
    return this.as.getUserLoggedIn();
  }
}
