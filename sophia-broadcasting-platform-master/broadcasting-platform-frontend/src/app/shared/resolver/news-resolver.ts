import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { User } from '../models/user.model';
import { News } from '../models/news.model';
import { NewsService } from '../services/news.service';
import { AuthService } from '../services/auth/auth.service';

@Injectable()
export class NewsResolver implements Resolve<News> {
  constructor(private ns: NewsService, private user: User, private as: AuthService) {
    this.as.getUserLoggedIn().subscribe((theUser: User) => {
      this.user = theUser;
    });
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<News> {
    return this.user.broadcasters ?
      this.ns.getActiveNewsForBroadcaster(this.user) :
      this.ns.getActiveNewsForMusicProvider(this.user.id);
  }
}

/*
Copyright 2017 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
