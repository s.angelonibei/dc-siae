import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {User} from '../models/user.model';
import {AuthService} from '../services/auth/auth.service';
import {Palinsesto} from '../models/mprs/palinsesto.model';
import {PalinsestoService} from '../services/palinsesto.service';

@Injectable()
export class PalinsestiResolver implements Resolve<Palinsesto[]> {
  constructor(private as: AuthService, private ps: PalinsestoService, private user: User) {
    this.as.getUserLoggedIn().subscribe((theUser: User) => {
      this.user = theUser;
    });
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Palinsesto[]> {
    return this.ps.getPalinsesti(this.user.musicProvider.idMusicProvider);
  }
}

/*
Copyright 2017 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
