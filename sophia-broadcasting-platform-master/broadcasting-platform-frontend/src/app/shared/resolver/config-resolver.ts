import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { User } from '../models/user.model';
import { AuthService } from '../services/auth/auth.service';
import { UploadService } from '../services/upload.service';
import { BroadcasterConfig } from '../models/broadcaster-config.model';

@Injectable()
export class ConfigResolver implements Resolve<BroadcasterConfig[]> {
  constructor(private as: AuthService, private us: UploadService, private user: User) {
    this.as.getUserLoggedIn().subscribe((theUser: User) => {
      this.user = theUser;
    });

  }

  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<BroadcasterConfig[]> {
    return this.us.getConfig(this.user.broadcasters.id);
  }
}

/*
Copyright 2017 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
