import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Canale } from '../models/canale.model';
import { CanaleService } from '../services/canale.service';
import { User } from '../models/user.model';
import { AuthService } from '../services/auth/auth.service';

@Injectable()
export class CanaliResolver implements Resolve<Canale> {
  constructor(private as: AuthService, private cs: CanaleService, private user: User) {
    this.as.getUserLoggedIn().subscribe((theUser: User) => {
      this.user = theUser;
      console.log(this.user)
    });
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Canale> {
    console.log(this.user.broadcasters.id)
    return this.cs.getCanali(this.user.broadcasters.id);
  }
}

/*
Copyright 2017 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
