import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { KpiService } from '../services/kpi.service';
import { AuthService } from '../services/auth/auth.service';
import { User } from '../models/user.model';
import {OutputKpi} from '../models/response.data.kpi.model';
import {MusicProvider}from '../models/mprs/music-provider.model';
import {Palinsesto}from '../models/mprs/palinsesto.model';

@Injectable()
export class KpiResolver implements Resolve<OutputKpi> {
  model: any = {
    musicProvider: MusicProvider,
    palinsesti: [],
    palinsesto: Palinsesto,
    mesi: [],
    anno: undefined,
  };
  user: User;

  constructor(private kpiService: KpiService, private as: AuthService) {
    this.as.getUserLoggedIn().subscribe((user: User) => {
      this.user = user;
    });
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<OutputKpi> {
    return this.kpiService.getKPIListForMusicProvider(this.model);
  }
}
