import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { KpiService } from '../services/kpi.service';
import { AuthService } from '../services/auth/auth.service';
import { User } from '../models/user.model';
import {KPIRequest} from '../models/kpi-request.model';
import {ResponseKpi} from '../models/response.kpi.model';

@Injectable()
export class KpiResolver implements Resolve<ResponseKpi> {
  model: KPIRequest;
  user: User;

  constructor(private kpiService: KpiService, private as: AuthService) {
    this.as.getUserLoggedIn().subscribe((user: User) => {
      this.user = user;
      this.model = new KPIRequest();
      this.model.tipoCanali = user.broadcasters.tipoBroadcaster;
      this.model.emittente = user.broadcasters;
      this.model.musicProvider = user.musicProvider;
    });
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ResponseKpi> {
    return this.kpiService.getKPIList({ RequestData: this.model })
  }
}
