import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { User } from '../models/user.model';
import { Output } from '../models/uploaded-file.model';
import { UploadedFilesService } from '../services/uploaded-files.service';
import { MatDialog } from '@angular/material';
import { AuthService } from '../services/auth/auth.service';

@Injectable()
export class UploadedFilesResolver implements Resolve<Output[]> {
  constructor(private us: UploadedFilesService,
              private user: User,
              private dialog: MatDialog,
              private as: AuthService) {
    this.as.getUserLoggedIn().subscribe((theUser: User) => {
      this.user = theUser;
    });
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Output[]> {
    return this.user.broadcasters ? this.us.getUploadedFilesForBroadcasters(this.user.broadcasters.id,
                                    undefined,
                                    this.user.currentRepertorio) :
      this.us.getUploadedFilesForMusicProviders(this.user.musicProvider.idMusicProvider, undefined);
  }
}

/*
Copyright 2017 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
