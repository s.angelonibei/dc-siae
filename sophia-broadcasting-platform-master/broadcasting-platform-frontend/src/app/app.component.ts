import { Component } from '@angular/core';
import { RoutingStateService } from './shared/services/routing-state.service';
import { Observable } from 'rxjs/internal/Observable';
import { AuthService } from './shared/services/auth/auth.service';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material';
import {User} from './shared/models/user.model';

const { title: title } = require('../../package.json');

@Component({
  selector: 'bp-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title: string;
  userIsLoggedIn$: Observable<boolean>;
  user$: Observable<User>;

  constructor(private routingState: RoutingStateService,
              private iconRegistry: MatIconRegistry,
              private sanitizer: DomSanitizer,
              private authService: AuthService) {
    routingState.loadRouting();
    this.user$ = this.authService.getUserLoggedIn();
    this.userIsLoggedIn$ = this.authService.isLoggedIn();
    this.title = title;
    iconRegistry.addSvgIcon(
      'full-logo-new',
      sanitizer.bypassSecurityTrustResourceUrl('assets/images/full-logo-new.svg'));
    iconRegistry.addSvgIcon(
      'upload',
      sanitizer.bypassSecurityTrustResourceUrl('assets/images/upload.svg'));
    iconRegistry.addSvgIcon(
      'monitoraggio',
      sanitizer.bypassSecurityTrustResourceUrl('assets/images/monitoraggio.svg'));
    iconRegistry.addSvgIcon(
      'musica',
      sanitizer.bypassSecurityTrustResourceUrl('assets/images/musica.svg'));
    iconRegistry.addSvgIcon(
      'cinema',
      sanitizer.bypassSecurityTrustResourceUrl('assets/images/cinema.svg'));
    iconRegistry.addSvgIcon(
      'olaf',
      sanitizer.bypassSecurityTrustResourceUrl('assets/images/olaf.svg'));
    iconRegistry.addSvgIcon(
      'lirica',
      sanitizer.bypassSecurityTrustResourceUrl('assets/images/lirica.svg'));
    iconRegistry.addSvgIcon(
      'dor',
      sanitizer.bypassSecurityTrustResourceUrl('assets/images/dor.svg'));
  }
}
