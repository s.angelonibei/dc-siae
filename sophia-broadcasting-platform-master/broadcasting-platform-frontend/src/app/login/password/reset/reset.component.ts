import { Component } from '@angular/core';
import { AuthService } from '../../../shared/services/auth/auth.service';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { CustomMatDialogComponent } from '../../../shared/components/custom-mat-dialog/custom-mat-dialog.component';

@Component({
  selector: 'bp-password-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['../../main-login.component.scss'],
})
export class ResetComponent {
  model: any;
  loading = false;

  constructor(private as: AuthService,
              private dialog: MatDialog,
              private router: Router) {
    this.model = {
      email: '',
    };
  }

  reset() {
    this.as.resetPassword(this.model.email).subscribe(() => {
      this.loading = true;
      this.dialog.open(CustomMatDialogComponent, {
        data: {title: 'Reset', message: 'Operazione avvenuta con successo. <br>' +
          'Controllare la propria e-mail', popup: true, cancel: 'OK'},
      }).afterClosed().subscribe(() => {
        this.loading = false;
        this.router.navigate(['login']);
      });
    },                                                () => {
      this.loading = false;
    });
  }

}
