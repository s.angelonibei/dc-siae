import {AfterViewInit, Component, ElementRef, OnChanges, ViewChild} from '@angular/core';
import {ActivatedRoute, NavigationExtras, Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatDialog} from '@angular/material';
import {matchPassword} from '../../../shared/directives/password-validator.directive';
import {ChangePassword} from '../../../shared/models/change-password.model';
import {AuthService} from '../../../shared/services/auth/auth.service';
import {CustomMatDialogComponent} from '../../../shared/components/custom-mat-dialog/custom-mat-dialog.component';
import {PasswordResetToken} from '../../../shared/models/password.reset.token.model';
import {MyErrorStateMatcher} from '../../../shared/models/my.error.state.matcher.model';

@Component({
  selector: 'bp-password-change',
  templateUrl: './change.component.html',
  styleUrls: ['../../main-login.component.scss'],
})
export class ChangeComponent implements OnChanges, AfterViewInit {
  passwordFormGroup: FormGroup;
  loading = false;
  resetPasswordConfig: any;
  changePassword: ChangePassword;
  matcher = new MyErrorStateMatcher();
  @ViewChild('tip') tip: ElementRef;
  @ViewChild('help') help: ElementRef;

  constructor(private route: ActivatedRoute,
              private fb: FormBuilder,
              private as: AuthService,
              private dialog: MatDialog,
              private router: Router) {
    this.route.data.subscribe((data: { user: PasswordResetToken, config: any}) => {
      this.changePassword = {
        idUtente: data.user.utente.id,
        ruolo: data.user.utente.ruolo.ruolo,
        password: undefined,
        password2: undefined,
      };
      this.resetPasswordConfig = data.config;
    });
    this.createForm();
    // console.log(this.passwordFormGroup);
  }

  private createForm() {
    console.log('regex', this.resetPasswordConfig['regex']);
    this.passwordFormGroup = this.fb.group({
      password: new FormControl('', [Validators.required,
        Validators.pattern(this.resetPasswordConfig['regex'])]),
      password2: new FormControl('', [Validators.required]),
    },                                     {
      validator: matchPassword('password', 'password2'), // your
      // validation method
    });
  }

  ngOnChanges() {
    this.rebuildForm();
  }

  private rebuildForm() {
    this.passwordFormGroup.reset({
      password: this.changePassword.password,
      password2: this.changePassword.password2,
    });
  }

  private getErrorMessage() {
    return this.passwordFormGroup.controls['password'].hasError('required') ? 'Password richiesta' :
      this.passwordFormGroup.controls['password'].hasError('pattern') ?
        this.resetPasswordConfig.exceptionPasswordNotValid :
        this.passwordFormGroup.errors.matchPassword ? 'Le password non corrispondono' :
        '';
  }

  change(form) {
    if (form.valid) {
      this.changePassword.password = this.passwordFormGroup.controls['password'].value;
      this.changePassword.password2 = this.passwordFormGroup.controls['password2'].value;
      this.as.changePassword(this.changePassword).subscribe(() => {
        this.dialog.open(CustomMatDialogComponent, {
          data: {
            title: 'Cambio Password', message: 'Operazione avvenuta con successo. <br>' +
            'Effettuare la login', popup: true, cancel: 'OK',
          },
        }).afterClosed().subscribe(() => {
          const navigationExtras: NavigationExtras = {
            fragment: 'anchor',
          };

          // Navigate to the login page with extras
          this.router.navigate(['login'], navigationExtras);
          return false;
        });
      });
    } else {
      this.dialog.open(CustomMatDialogComponent, {
        data: { title: 'Errore',
          message: this.getErrorMessage(),
          popup: true, cancel: 'OK' },
      });
    }
  }

  ngAfterViewInit(): void {
    $(this.tip.nativeElement)[0].style.left =
      $(this.help.nativeElement).width() + 'px';
  }
}
