import { AfterViewInit, Component, OnInit } from '@angular/core';
import { animate, group, keyframes, query, style, transition, trigger } from '@angular/animations';
import { AuthService } from '../shared/services/auth/auth.service';
import { ActivatedRoute } from '@angular/router';
import { User } from '../shared/models/user.model';
import { Observable } from 'rxjs';
import { SessionManager } from '../shared/models/token.storage.model';

@Component({
  selector: 'app-broadcasting-platform-login',
  templateUrl: './main-login.component.html',
  animations: [
    trigger('routeAnimation', [
      transition('* <=> *', [
        query(
          ':enter',
          [style({
            position: 'fixed',
            top: '50%',
            left: '0',
            transform: 'translateX(-150%)',
            opacity: 0 })],
          { optional: true },
        ),
        query(
          ':leave',
          [style({
            position: 'fixed',
            top: '50%',
            right: '0',
            transform: 'translateX(50%)',
            opacity: 1 })],
          { optional: true },
        ),
        group([
          query(':leave', [
            animate('500ms ease-in-out',
                    keyframes([
                      style({ right: '50%', opacity: 1 }),
                      // style({ right: '1%', opacity: 1 }),
                      style({ right: '0', opacity: 0 }),
                    ]),
                    // style({ transform: 'translateX(-50%) translateY(-50%)', opacity: 0 }),
          )],   { optional: true }),
          query(':enter', [
            animate('500ms ease-in-out',
                    keyframes([
                      style({ left: '0', opacity: 0 }),
                      // style({ left: '25%', opacity: 1 }),
                      style({ left: '50%', opacity: 1 }),
                    ]),
            )], { optional: true }),
        ]),
      ]),
    ]),
  ],
})
export class MainLoginComponent implements AfterViewInit, OnInit {
  returnUrl: string;
  message: string;
  user$: Observable<User>;

  constructor(private authenticationService: AuthService,
              private route: ActivatedRoute,
              private token: SessionManager) {
    this.setMessage();
  }

  ngOnInit() {
    this.user$ = this.authenticationService.getUserLoggedIn();
  }

  ngAfterViewInit() {
    this.logout();
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  setMessage() {
    this.message =
      'Logged ' + (this.authenticationService.isAuthorized(this.returnUrl) ? 'in' : 'out');
  }

  logout() {
    this.token.signOut();
    this.authenticationService.logout();
    this.setMessage();
  }
}
