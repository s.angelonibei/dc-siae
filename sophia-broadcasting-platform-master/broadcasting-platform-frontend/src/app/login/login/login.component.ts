import { Component } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { AuthService } from '../../shared/services/auth/auth.service';

@Component({
  selector: 'app-broadcasting-platform-login',
  templateUrl: './login.component.html',
  styleUrls: ['../main-login.component.scss'],
})
export class LoginComponent {
  model: any = {};
  loading = false;
  returnUrl: string;

  constructor(private authenticationService: AuthService,
              private route: ActivatedRoute,
              private router: Router) {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  login() {
    this.loading = true;
    this.authenticationService.signIn(this.model.username, this.model.password)
        .subscribe(() => {
          if (this.authenticationService.isAuthorized(this.returnUrl)) {
            const redirect =
              this.authenticationService.redirectUrl ?
                this.authenticationService.redirectUrl :
                '/';
            const navigationExtras: NavigationExtras = {
              queryParamsHandling: 'preserve',
              preserveFragment: true,
            };
            this.router.navigate([redirect], navigationExtras);
          }
        },         () => {
          // this.alertService.error(error);
          this.loading = false;
        });
  }
}
