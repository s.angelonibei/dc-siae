import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import {
  MatButtonModule,
  MatCardModule,
  MatDividerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatToolbarModule, MatTooltipModule,
} from '@angular/material';
import { CustomMatDialogComponent } from '../shared/components/custom-mat-dialog/custom-mat-dialog.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { ResetPasswordConfigResolver } from '../shared/resolver/reset.password.config-resolver';
import { TokenResolve } from '../shared/resolver/token-resolver.service';
import { PasswordValidatorDirective } from '../shared/directives/password-validator.directive';
import { ResetComponent } from './password/reset/reset.component';
import { ChangeComponent } from './password/change/change.component';
import { MainLoginComponent } from './main-login.component';
import { BlockCopyPasteDirective } from '../shared/directives/block-copy-paste.directive';

const loginRoutes: Routes = [
  { path: '', component: MainLoginComponent, children: [
    { path: '', component: LoginComponent },
    { path: 'password/reset', component: ResetComponent },
    {
      path: 'password/change/:token',
      component: ChangeComponent,
      resolve: {
        user: TokenResolve,
        config: ResetPasswordConfigResolver,
      },
    },
  ],
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FlexLayoutModule,
    MatButtonModule,
    MatToolbarModule,
    MatTooltipModule,
    MatIconModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    MatMenuModule,
    MatDividerModule,
    SharedModule,
    RouterModule.forChild(loginRoutes),
  ],
  declarations: [
    MainLoginComponent,
    PasswordValidatorDirective,
    BlockCopyPasteDirective,
    ChangeComponent,
    ResetComponent,
    LoginComponent,
  ],
  exports: [MainLoginComponent, ChangeComponent, ResetComponent, LoginComponent],
  providers: [TokenResolve, ResetPasswordConfigResolver],
  entryComponents: [CustomMatDialogComponent],
  bootstrap: [MainLoginComponent],
})
export class LoginModule { }
