import {Component} from '@angular/core';
import {News} from '../../shared/models/news.model';
import {ActivatedRoute} from '@angular/router';
import {AuthService} from '../../shared/services/auth/auth.service';
import {User} from '../../shared/models/user.model';
import {AbstractService} from '../../shared/services/abstract.service';

@Component({
  selector: 'mprs-home',
  templateUrl: './home.component.html'
})
export class HomeComponent {
  news: News[];
  servicesRoot: string;
  user: User;

  constructor(private route: ActivatedRoute,
              private as: AuthService,) {
    this.as.getUserLoggedIn().subscribe((user: User) => {
      this.route.data
        .subscribe((data: { news: News[] }) => {
          this.news = data.news;
        });
      this.user = user;
    });
    this.servicesRoot = AbstractService.SERVICES_ROOT;
  }

  /*public downloadTemplate(): void {
    this.hs.downloadTemplate(this.user.broadcasters.tipoBroadcaster).subscribe((event) => {
      if (event instanceof HttpResponse) {
        const name = getFileNameFromResponseContentDisposition(event);
        saveFile(event.body, name);
      }
    });
  }*/
}
