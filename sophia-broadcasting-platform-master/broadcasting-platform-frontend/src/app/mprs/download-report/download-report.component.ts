import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { UploadedFilesService } from '../../shared/services/uploaded-files.service';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Output } from '../../shared/models/uploaded-file.model';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { DatePipe, registerLocaleData } from '@angular/common';
import it from '@angular/common/locales/it';
import { getFileNameFromResponseContentDisposition, saveFile } from '../../shared/file-download-helper';
import { AuthService } from '../../shared/services/auth/auth.service';
import { User } from '../../shared/models/user.model';

@Component({
  selector: 'bp-download-report',
  templateUrl: './download-report.component.html',
  styleUrls: ['./download-report.component.css'],
})
export class DownloadReportComponent implements AfterViewInit {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  loading = false;
  user: User;
  uploadedFilesList: MatTableDataSource<Output>;

  displayedColumns = [
        // 'index',
    'dataUpload',
    'nomeFile',
    'stato',
    'tipoUpload',
    'download',
  ];

  constructor(private route: ActivatedRoute,
              private ufs: UploadedFilesService,
              private as: AuthService) {
    this.route.data.subscribe((data) => {
      this.uploadedFilesList = new MatTableDataSource(data.uploadedFiles);
      registerLocaleData(it);
      const datePipe = new DatePipe('it');
      this.uploadedFilesList.filterPredicate = (data2: Output, filter: string) =>
                datePipe.transform(data2.dataUpload, 'dd/MM/yyyy HH:mm:ss').indexOf(filter) !== -1
                || data2.nomeFile.toLowerCase().indexOf(filter.toLowerCase()) !== -1
                || data2.stato.toLowerCase().indexOf(filter.toLowerCase()) !== -1
                || data2.tipoUpload.toLowerCase().indexOf(filter.toLowerCase()) !== -1;
      this.as.getUserLoggedIn().subscribe((user: User) => {
        this.user = user;
      });
    });

  }

  ngAfterViewInit() {
    this.uploadedFilesList.sort = this.sort;
    this.uploadedFilesList.paginator = this.paginator;
  }

  applyFilter(filterValue: string) {
    this.uploadedFilesList.filter = filterValue;
  }

  downloadUploadedFile(id: number) {
    this.ufs.downloadUploadedMusicProviderFile(id).subscribe((event) => {
      this.loading = true;
      if (event instanceof HttpResponse) {
        this.loading = false;

        const name = getFileNameFromResponseContentDisposition(event);
        saveFile(event.body, name);
      }
    });
  }
}

