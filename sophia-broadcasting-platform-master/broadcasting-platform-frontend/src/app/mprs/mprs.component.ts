import { AfterViewChecked, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { User } from '../shared/models/user.model';
import { AuthService } from '../shared/services/auth/auth.service';
import { Observable } from 'rxjs/internal/Observable';

@Component({
  selector: 'app-mprs',
  templateUrl: './mprs.component.html'
})
export class MprsComponent implements OnInit, OnDestroy, AfterViewChecked {

  constructor(private authService: AuthService,
              private cdRef: ChangeDetectorRef) {
  }
  user$: Observable<User>;
  observer: MutationObserver;
  cdkOverlayContainerObserver: MutationObserver;

  private static changeOffset(value: any, index: number) {
    // console.log("element", value);
    if (value.clientHeight !== 0 &&
      (!($(value.childNodes[0]).hasClass('mat-menu-panel') ||
        $(value).hasClass('mat-tooltip-panel')) ||
        $(value).hasClass('mat-select-panel') ||
        value.childNodes.length === 0)) {
      const selectElmUnderline = $($('.mat-select')[index])
        .parents('.mat-form-field-wrapper')
        .find('.mat-form-field-underline');
      if (selectElmUnderline) {
        const yPos = selectElmUnderline.offset().top;
        value.style.top =
          yPos +
          (+selectElmUnderline.css('line-height')
            .substring(0, selectElmUnderline.css('line-height').length - 2)) + 'px';
      }
      // return value.clientHeight !== 0;
      return;
    }
  }

  ngOnInit() {
    this.user$ = this.authService.getUserLoggedIn();
    const targetNode = document.getElementsByTagName('body')[0];

    // Options for the observer (which mutations to observe)
    const config = { childList: true };

// Callback function to execute when mutations are observed
    const callback = this.observe;

// Create an observer instance linked to the callback function
    this.observer = new MutationObserver(callback);

// Start observing the target node for configured mutations
    this.observer.observe(targetNode, config);

  }

  ngAfterViewChecked() {
    const main =  <HTMLElement>document.getElementById('main');
    main.style.paddingRight = main.offsetWidth - main.clientWidth + 'px';
    this.cdRef.detectChanges();
  }

  ngOnDestroy() {
    // Later, you can stop observing
    this.observer.disconnect();
    // this.cdkOverlayContainerObserver.disconnect();
  }

  private observe(mutationsList: MutationRecord[]) {
    for (const mutation of mutationsList) {
      const cdkOverlayContainer =
        Array.from(mutation.addedNodes).concat(Array.from(mutation.removedNodes))[0];
      if ($('.mat-select').length > 0) {
        Array.from($('.cdk-overlay-connected-position-bounding-box')
          .find('.cdk-overlay-pane'))
          .forEach((value, index) => {
            MprsComponent.changeOffset(value, index);
          });
        this.cdkOverlayContainerObserver =
          new MutationObserver((cdkOverlayContainerMutationList) => {
            for (const cdkOverlayContainerMutation of cdkOverlayContainerMutationList) {
              // console.log('cdkOverlayContainerMutation', cdkOverlayContainerMutation);
              Array.from($('.cdk-overlay-connected-position-bounding-box')
                .find('.cdk-overlay-pane'))
                .forEach((value, index) => {
                  MprsComponent.changeOffset(value, index);
                });
            }
          });
        this.cdkOverlayContainerObserver.observe(cdkOverlayContainer, { childList: true });
      }
      // console.log('1st mutation', mutation);
    }
  }

}
