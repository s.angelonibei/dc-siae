import {Component, ElementRef, NgZone, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material';
import {ActivatedRoute} from '@angular/router';
import {UploadFile} from 'ngx-file-drop';
import {forkJoin} from 'rxjs';
import {map} from 'rxjs/operators';
import {CustomMatDialogComponent} from '../../shared/components/custom-mat-dialog/custom-mat-dialog.component';
import {Upload} from '../../shared/models/upload.model';
import {User} from '../../shared/models/user.model';
import {AuthService} from '../../shared/services/auth/auth.service';
import {UploadService} from '../../shared/services/upload.service';
import {BytesPipe} from '../../shared/pipes/bytes.pipe';
import {Palinsesto} from '../../shared/models/mprs/palinsesto.model';
import {MusicProviderConfig} from '../../shared/models/mprs/music-provider-config.model';

@Component({
  selector: 'mprs-upload-massivo',
  templateUrl: './upload-massivo.component.html',
  styleUrls: ['./upload-massivo.component.scss'],
})
export class UploadMassivoComponent {
  public loading = false;
  @ViewChild('inputFile') public input: ElementRef;
  @ViewChild('f') public form;
  @ViewChild('canaliNonAttiviDiv') public palinsestiNonAttiviDiv: ElementRef;

  public maxSize: number = 500 * 1024 * 1024;

  public allowedExts: string[] = [
    'TXT',
  ];

  protected us: UploadService;
  protected zone: NgZone;
  protected dialog: MatDialog;
  protected route: ActivatedRoute;
  protected as: AuthService;
  protected user: User;

  public model: Upload;
  public musicProviderConfigs: MusicProviderConfig[];
  public palinsesti: Palinsesto[];
  public palinsestiNonAttivi: Palinsesto[];

  constructor(us: UploadService,
              zone: NgZone,
              dialog: MatDialog,
              route: ActivatedRoute,
              as: AuthService) {

    this.us = us;
    this.zone = zone;
    this.dialog = dialog;
    this.route = route;
    this.as = as;

    this.as.getUserLoggedIn().subscribe((theUser: User) => {
      this.user = theUser;
      this.resetModel();
      this.route.data
                .subscribe((data: { config: MusicProviderConfig[], palinsesti: Palinsesto[]}) => {
                  this.palinsesti = data.palinsesti;
                  this.palinsestiNonAttivi = this.palinsesti.filter(palinsesto => palinsesto.dataFineValidita);
                  // this.broadcasterConfigs = this.getBroadcasterConfigs(data);
                  if (this.musicProviderConfigs) {
                    const musicProviderConfig =
                      this.musicProviderConfigs.length === 1 ?
                        this.musicProviderConfigs[0] :
                        this.musicProviderConfigs.find(x =>  x.palinsesto === null );
                    // if (theUser.currentRepertorio.toLowerCase() === 'musica') {
                      this.allowedExts = this.getAllowedExts(musicProviderConfig);
                    // }
                  }
                });
    });
  }

  private getAllowedExts(musicProviderConfig) {
    return ['TXT']; /*JSON.parse(broadcasterConfig.configuration.configurazione).supportedFormat
      .split('|')
      .toString()
      .substr(2,
        JSON.parse(broadcasterConfig.configuration.configurazione)
          .supportedFormat.length - 4)
      .toUpperCase()
      .split(',');*/
  }

  private resetModel() {
    this.model = new Upload();
    this.model.nomerepertorio = this.user.currentRepertorio;
    this.model.idMusicProvider = this.user.musicProvider.idMusicProvider;
    this.model.idUtente = this.user.id;
    this.model.files = [];
  }

  public dropped(event: any) {
    this.zone.run(() => {
      this.model.files = [];
      const validFiles = [];
      let invalidFileNames = '';
      let allFiles = [];
      if (event.files[0] instanceof UploadFile) {
        allFiles = event.files;
        allFiles.forEach((file: UploadFile) => {
          invalidFileNames = this.getInvalidFileNames(file.fileEntry, invalidFileNames, validFiles);
        });

        for (const file of validFiles) {
          file.file((fileData) => {
            this.setFiles(fileData);
          });
        }
      } else {
        allFiles = <File[]>Array.from(event.files);
        allFiles.forEach((file: File) => {
          invalidFileNames = this.getInvalidFileNames(file, invalidFileNames, validFiles);
        });

        for (const file of (<File[]> Array.from(validFiles))) {
          this.setFiles(file);
        }
      }
      this.showInvalidFilesDialog(invalidFileNames);
    });
  }

  private showInvalidFilesDialog(invalidFileNames: string) {
    if (invalidFileNames.length !== 0) {
      this.dialog.open(CustomMatDialogComponent, {
        data: {
          title: 'ERRORE',
          message: `I file:<ul>${invalidFileNames}</ul>non hanno estensione valida`,
          popup: true,
          cancel: 'OK',
        },
      });
    }
  }

  private getInvalidFileNames(file, invalidFileNames: string, validFiles) {
    const allowedExts = this.allowedExts.filter((allowedExt: string) => {
      return this.isFormatSupported(allowedExt, file);
    });

    if (allowedExts.length === 0) {
      invalidFileNames += `<li>${file.name}</li>`;
    } else {
      validFiles.push(file);
    }
    return invalidFileNames;
  }

  private isFormatSupported(allowedExt: string, file) {
    return allowedExt === file.name.substr((file.name.lastIndexOf('.') + 1)).toUpperCase();
  }

  public onSubmit() {
    const observableBatch = [];
    this.model.keys = this.getFilesToUpload(observableBatch);
    forkJoin(observableBatch).pipe(
      map((responses) => {
        return [].concat(...responses);
      })).subscribe((events) => {
        const unuploadedFiles = this.collectUnuploadedFiles(events);
        // console.log('File is completely uploaded!');
        // console.log("upload percentage", 1);
        this.us.saveUpload(this.model).subscribe(() => {
          this.showUploadedFilesDialog(unuploadedFiles);
          this.cleanUpModel(unuploadedFiles);
        });
      },
      (error) => {
        const unuploadedFiles = this.collectUnuploadedFiles(error);
        this.showUploadedFilesDialog(unuploadedFiles);
        this.loading = false;
      });
    return true;
  }

  private showUploadedFilesDialog(unuploadedFiles) {
    const invalidFileNames = this.getNamesFromUnuploadedFiles(unuploadedFiles);
    this.dialog.open(CustomMatDialogComponent, {
      data: {
        title: 'Conferma',
        message: invalidFileNames.length === 0 ?
          'Tutti i report sono stati correttamente caricati' :
          'Tutti i report sono stati correttamente caricati ' +
          'tranne i seguenti: <ul>' +
          invalidFileNames + '</ul>',
        popup: true,
        cancel: 'OK',
      },
    });
  }

  public confirm(form?: any) {
    if (this.model.files.length > 0) {
      let msg = 'Attenzione: eventuali dichiarazioni in sovrapposizione con ' +
        'le precedenti verranno scartate.<br>';

      const palinsestiNonAttivi =
        this.palinsesti.filter(palinsesto => palinsesto.dataFineValidita);

      if (palinsestiNonAttivi && palinsestiNonAttivi.length !== 0) {
        // msg += this.getListDeletedChannels(canaliNonAttivi);
        msg += this.palinsestiNonAttiviDiv.nativeElement.innerHTML;
      }
      const dialogRef = this.dialog.open(CustomMatDialogComponent, {
        data: {
          title: 'Conferma',
          message: msg + ' <h3 class="center">Confermi il caricamento?</h3>',
        },
      });

      dialogRef.afterClosed().subscribe((data) => {
        if (data) {
          this.onSubmit();
        }
        return data;
      });
    } else {
      this.dialog.open(CustomMatDialogComponent, {
        data: { title: 'ERRORE', message: 'Allegare un file', popup: true, cancel: 'OK' },
      });
      return false;
    }
  }

  public inputFileSelection(fileInput: any) {
    this.zone.run(() => {
      if (this.input.nativeElement.files.length !== 0) {
        this.input.nativeElement.value = '';
      }
      fileInput.click();
    });
    return false;
  }

  public deleteFile(i: number) {
    this.zone.run(() => {
      const dialogRef = this.dialog.open(CustomMatDialogComponent, {
        data: { title: 'Eliminare?', message: 'Eliminare il file allegato?' },
      });
      dialogRef.afterClosed().subscribe((data) => {
        if (data) {
          // this.fileInfos.splice(i, 1);
          // this.uploadFiles.splice(i, 1);
          this.model.files.splice(i, 1);
        }
      });
    });
  }

  protected setFiles(file: any) {
    if (file.size > this.maxSize) {
      this.zone.run(() => {
        this.dialog.open(CustomMatDialogComponent, {
          data: {
            title: 'ERRORE',
            message: 'Il file ' + file.name + ' supera il limite massimo di ' +
              new BytesPipe().transform(this.maxSize, 2),
            popup: true,
            cancel: 'OK',
          },
        });
      });
    } else {
      this.zone.run(() => {
        this.model.files.push(file);
      });
    }
  }

  protected getFilesToUpload(observableBatch): string[] {
    let i = 0;
    const keys: string[] = [];
    this.model.filenames = [];
    Array.from(this.model.files).forEach((file) => {
      this.model.filenames.push(file.name);
      keys.push(
        this.model.nomerepertorio + '/' +
        this.user.musicProvider.nome.replace(new RegExp(' ', 'g'), '') + '/' +
        file.name.split(new RegExp('\\.(?=[^.]+$)'))[0] + '_' +
        (new Date()).getTime() + '.' +
        file.name.split(new RegExp('\\.(?=[^.]+$)'))[1]);
      observableBatch.push(this.us.upload(file, keys[i]));
      i += 1;
    });
    return keys;
  }

  protected collectUnuploadedFiles(events) {
    const unuploadedFiles: any[] = [];
    for (const index in events) {
      if (events[index]) {
        unuploadedFiles.push({
          index,
          event: events[index],
          file: this.model.files[index],
        });
        events.slice(+index, 1);
        this.model.files.slice(+index, 1);
      }
    }
    return unuploadedFiles;
  }

  protected getNamesFromUnuploadedFiles(unuploadedFiles) {
    let invalidFileNames = '';
    for (const file of unuploadedFiles) {
      invalidFileNames += '<li>' + file.file.name + '</li>';
    }
    return invalidFileNames;
  }

  protected cleanUpModel(unuploadedFiles) {
    this.loading = false;

    // this.fileInfos = [];
    // this.uploadFiles = [];
    this.resetModel();
    this.model.files = unuploadedFiles.map((file) => {
      return file.file;
    });
    this.form.reset();
    this.form.submitted = false;
  }
}
