import { Component } from '@angular/core';
import * as moment from 'moment';
import { User } from '../../shared/models/user.model';
import { KPI } from '../../shared/models/kpi.model';
import { KpiService } from '../../shared/services/kpi.service';
import { MatDialog, MatTableDataSource } from '@angular/material';
import {FileDaElaborare, OutputKpi} from '../../shared/models/response.data.kpi.model';
import { UtilService } from '../../shared/services/util.service';
import { ActivatedRoute } from '@angular/router';
import { CustomMatDialogComponent } from '../../shared/components/custom-mat-dialog/custom-mat-dialog.component';
import { AuthService } from '../../shared/services/auth/auth.service';
import {Palinsesto} from '../../shared/models/mprs/palinsesto.model';
import {MusicProvider} from "../../shared/models/mprs/music-provider.model";

@Component({
  selector: 'bp-monitoraggio',
  templateUrl: './monitoraggio.component.html',
  styleUrls: ['./monitoraggio.component.scss'],
})
export class MonitoraggioComponent {
  kpis: KPI[];

  model: any = {
    musicProvider: MusicProvider,
    palinsesti: [],
    palinsesto: Palinsesto,
    mesi: [],
    anno: undefined,
  };

  palinsesti: Palinsesto[];
  mesi: {}[];
  anni: number[];

  appendChar = '%';

  thresholdConfig = {
    0: { color: 'red' },
    33.3: { color: 'orange' },
    66.6: { color: 'green' },
  };

  thresholdConfigAlt = {
    0: { color: 'red' },
    33.3: { color: 'orange' },
    66.6: { color: 'green' },
  };

  displayedColumns = ['nomeFile', 'dataUpload', 'canale', 'tipoUpload'];
  fileDaElaborare: MatTableDataSource<FileDaElaborare>;

  constructor(private user: User,
              private ks: KpiService,
              private as: AuthService,
              private dialog: MatDialog,
              private route: ActivatedRoute) {
    this.as.getUserLoggedIn().subscribe((theUser: User) => {
      moment.locale('it');
      this.user = theUser;
      this.model.musicProvider = this.user.musicProvider;
      this.kpis = [];

      this.anni = UtilService.years;
      this.mesi = UtilService.months;
      this.route.data
                .subscribe((data: { palinsesti: Palinsesto[], kpi: OutputKpi }) => {
                  this.palinsesti = data.palinsesti;
                  this.kpis = data.kpi.listaKPI;
                  this.fileDaElaborare =
                    new MatTableDataSource<FileDaElaborare>(data.kpi.fileDaElaborare);
                });
    });

  }

  selected(event) {
    this.model.palinsesti = [];
    this.model.palinsesti.push(event.value);
  }

  public onChange(event) {
    this.model.mesi = [];
    this.mesi = UtilService.getMonths(event.value);
  }

  onSubmit(form) {
    if (!form.valid) {
      this.dialog.open(CustomMatDialogComponent, {
        data: { title: 'Errore',
          message: 'Riempire i campi obbligatori',
          popup: true, cancel: 'OK' },
      });
    } else {
      this.ks.getKPIListForMusicProvider(this.model).subscribe((data: OutputKpi) => {
        this.kpis = data.listaKPI;
        this.fileDaElaborare =
          new MatTableDataSource<FileDaElaborare>(data.fileDaElaborare);
      });
    }
  }
}
