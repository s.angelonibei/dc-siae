import {ChangeDetectorRef, Component, ElementRef, NgZone, ViewChild} from '@angular/core';
import {UploadEvent, UploadFile} from 'ngx-file-drop';
import {Email} from '../../shared/models/email.model';
import {EmailService} from '../../shared/services/email.service';
import {AuthService} from '../../shared/services/auth/auth.service';
import {BytesPipe} from '../../shared/pipes/bytes.pipe';
import {HttpResponse} from '@angular/common/http';
import {CustomMatDialogComponent} from '../../shared/components/custom-mat-dialog/custom-mat-dialog.component';
import {MatDialog} from '@angular/material';
import {User} from '../../shared/models/user.model';
import {FileInfo} from '../../shared/models/file.info.model';

@Component({
  selector: 'bp-assistenza',
  templateUrl: './assistenza.component.html',
  styleUrls: ['./assistenza.component.scss'],
})
export class AssistenzaComponent {
  model: Email;
  loading = false;
  maxSize = 20971520;
  totFileSize = 0;
  @ViewChild('inputFile') input: ElementRef;
  @ViewChild('f') form;

  files: UploadFile[] = [];
  fileInfos: FileInfo[] = [];

  constructor(private es: EmailService,
              private ref: ChangeDetectorRef,
              private zone: NgZone,
              private dialog: MatDialog,
              private as: AuthService) {
    this.model = new Email();
    this.model.files = [];
    this.as.getUserLoggedIn().subscribe((user: User) => {
      this.model.user = user;
    });
  }

  public send() {
    this.model = this.prepareSave();
    this.es.sendMail(this.model).subscribe((event) => {
        // Via this API, you get access to the raw event stream.
        // Look for upload progress events.
        this.loading = true;
        if (event instanceof HttpResponse) {
          this.loading = false;
          this.dialog.open(CustomMatDialogComponent, {
            data: {title: 'ok', message: 'e-mail inviata!', popup: true, cancel: 'OK'},
          });
          this.model = new Email();
          this.model.files = [];
          this.files = [];
          this.fileInfos = [];
          this.totFileSize = 0;
          this.form.reset();
          this.form.submitted = false;
        }
      },
      () => {
        this.loading = false;
      },
    );
  }

  public dropped(event: any) {
    this.zone.run(() => {
      let i = 0;
      this.fileInfos = [];
      this.model.files = [];

      if (event.files[0] instanceof UploadFile) {
        for (const file of event.files) {
          file.fileEntry.file((fileData) => {
            this.setFiles(fileData, event.files, i);
          });
          i += 1;
        }
      } else {
        for (const file of (<File[]> Array.from(event.files))) {
          this.setFiles(file, event.files, i);
          i += 1;
        }
      }
    });
  }

  public inputFileSelection(fileInput: any) {
    this.zone.run(() => {
      if (this.input.nativeElement.files.length !== 0) {
        this.input.nativeElement.value = '';
      }
      fileInput.click();
    });
    return false;
    // this.dropped(event as UploadEvent);
  }

  public deleteFile(i: number) {
    this.zone.run(() => {
      const dialogRef = this.dialog.open(CustomMatDialogComponent, {
        data: {title: 'Eliminare?', message: 'Eliminare il file allegato?'},
      });
      dialogRef.afterClosed().subscribe((data) => {
        if (data) {
          this.files.splice(i, 1);
          this.model.files.splice(i, 1);
          this.fileInfos.splice(i, 1);
        }
      });
    });
  }

  public confirm(form) {
    if (!form.valid) {
      this.dialog.open(CustomMatDialogComponent, {
        data: {
          title: 'Errore',
          message: 'Riempire i campi obbligatori',
          popup: true,
          cancel: 'OK',
        },
      });
    } else {
      const dialogRef = this.dialog.open(CustomMatDialogComponent, {
        data: {title: 'Conferma', message: 'Confermi l\'invio della mail di assistenza?'},
      });
      dialogRef.afterClosed().subscribe((data) => {
        if (data) {
          this.send();
        }
        return data;
      });
    }
  }

  private setFiles(file: any, eventFiles: UploadEvent, i: number) {
    if (file.size > this.maxSize) {
      this.zone.run(() => {
        this.dialog.open(CustomMatDialogComponent, {
          data: {
            title: 'ERRORE',
            message: 'Il file ' + file.name + ' supera il limite massimo di ' +
              new BytesPipe().transform(this.maxSize, 2),
            popup: true,
            cancel: 'OK',
          },
        });
      });
    } else {
      this.model.files.push(file);
      this.files.push(eventFiles[i]);

      const fileInfo = new FileInfo();
      fileInfo.name = file.name;
      fileInfo.size = file.size;
      fileInfo.lastModified = file.lastModified;

      this.zone.run(() => {
        this.fileInfos.push(fileInfo);
      });

    }
  }

  private prepareSave(): Email {
    const formModel = this.model;
    const saveEmail: Email = Object.assign({}, formModel);
    saveEmail.files = this.model.files;
    saveEmail.user = this.model.user;

    return saveEmail;
  }
}
