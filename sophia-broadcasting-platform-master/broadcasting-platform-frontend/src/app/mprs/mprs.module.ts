import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgxGaugeModule} from 'ngx-gauge';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatPaginatorModule,
  MatSelectModule,
  MatSidenavModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
} from '@angular/material';
import {FlexLayoutModule} from '@angular/flex-layout';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../shared/shared.module';
import {CustomMatDialogComponent} from '../shared/components/custom-mat-dialog/custom-mat-dialog.component';
import {AuthGuard} from '../shared/services/auth/auth-guard.service';
import {NewsResolver} from '../shared/resolver/news-resolver';
import {ConfigResolver} from '../shared/resolver/config-resolver';
import {KpiResolver} from '../shared/resolver/mprs-kpi-resolver';
import {UploadedFilesResolver} from '../shared/resolver/uploaded-files-resolver';
import {DownloadReportComponent} from './download-report/download-report.component';
import {HomeComponent} from './home/home.component';
import {UploadSingoloComponent} from './upload-singolo/upload-singolo.component';
import {MonitoraggioComponent} from './monitoraggio/monitoraggio.component';
import {UserResolver} from '../shared/resolver/user-resolver';
import {AssistenzaComponent} from './assistenza/assistenza.component';
import {MprsComponent} from './mprs.component';
import {UploadMassivoComponent} from './upload-massivo/upload-massivo.component';
import {PalinsestiResolver} from '../shared/resolver/palinsesti-resolver';
import {PalinsestoService} from '../shared/services/palinsesto.service';
import {DownloadFileArmonizzatoComponent} from './download-file-armonizzato/download-file-armonizzato.component';

const mainRoutes: Routes = [
  {
    path: '', component: MprsComponent, /*resolve: {user: UserResolver}, */children: [
      { path: 'assistenza', component: AssistenzaComponent },
      {
        path: '', children: [
          { path: '', redirectTo: 'musica', pathMatch: 'full' },
          {
            path: 'musica', canActivateChild: [AuthGuard], children: [
              { path: '', redirectTo: 'home', pathMatch: 'full' },
              {
                path: 'home', component: HomeComponent, resolve: {
                  news: NewsResolver,
                },
              },
              {
                path: 'upload/singolo', component: UploadSingoloComponent, resolve: {
                  palinsesti: PalinsestiResolver,
                },
              },
              {
                path: 'monitoraggio', component: MonitoraggioComponent,
                resolve: {
                  palinsesti: PalinsestiResolver,
                  kpi: KpiResolver
                },
              },
              {
                path: 'download/report', component: DownloadReportComponent,
                resolve: {
                  uploadedFiles: UploadedFilesResolver,
                },
              },
              {
                path: 'download/armonizzato', component: DownloadFileArmonizzatoComponent,
                resolve: {
                  palinsesti: PalinsestiResolver,
                },
              },
              ],
          },
        ],
      },
    ],
  },
];

@NgModule({
  imports: [
    FlexLayoutModule,
    CommonModule,
    RouterModule,
    MatButtonModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatDatepickerModule,
    MatCardModule,
    MatMenuModule,
    MatMomentDateModule,
    MatSnackBarModule,
    MatExpansionModule,
    MatGridListModule,
    MatTableModule,
    MatTooltipModule,
    MatSortModule,
    MatPaginatorModule,
    MatTabsModule,
    MatIconModule,
    NgxGaugeModule,
    SharedModule,
    MatSidenavModule,
    RouterModule.forChild(mainRoutes),
  ],
  declarations: [
    HomeComponent,
    UploadSingoloComponent,
    UploadMassivoComponent,
    MonitoraggioComponent,
    DownloadReportComponent,
    DownloadFileArmonizzatoComponent,
    MprsComponent,
    AssistenzaComponent,
  ],
  providers: [
    PalinsestoService,
    PalinsestiResolver,
    NewsResolver,
    KpiResolver,
    ConfigResolver,
    UserResolver,
    UploadedFilesResolver,
  ],
  exports: [
    HomeComponent,
    UploadSingoloComponent,
    MonitoraggioComponent,
    UploadMassivoComponent,
    DownloadReportComponent,
    DownloadFileArmonizzatoComponent,
    AssistenzaComponent,
  ],
  entryComponents: [
    CustomMatDialogComponent,
  ],
  bootstrap: [
    MprsComponent,
  ],
})
export class MprsModule {
}
