import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from './not-found/not-found.component';
import { AuthGuard } from './shared/services/auth/auth-guard.service';
import { NgModule } from '@angular/core';
import { LoginAlterEgoGuardService } from './shared/services/auth/login-alter-ego-guard.service';

export const appRoutes: Routes = [
  {
    path: 'login',
    loadChildren: './login/login.module#LoginModule',
  },
  {
    path: 'mprs',
    loadChildren: './mprs/mprs.module#MprsModule',
    canLoad: [AuthGuard],
  },
  {
    path: 'lae/:token',
    loadChildren: './main/main.module#MainModule',
    canActivate: [LoginAlterEgoGuardService],
  },
  {
    path: '',
    loadChildren: './main/main.module#MainModule',
    canLoad: [AuthGuard],
  },
  { path: '**', component: NotFoundComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {
        useHash: true,
      },
    ),
  ],
  exports: [
    RouterModule,
  ],
})
export class AppRoutingModule { }
