# Broadcasting Platform

L'applicazione permette alle emittenti radio e TV di:
* Fare l'upload Singolo dei Report fornendo Canale, Anno e mese
* Fare l'upload Massivo dei Report attraverso un file contente dati diversi
* Monitorare i dati relativi ai Report caricati e elaborati (Armonizzati) attraverso degli indicatori grafici di 
un determinato Canale, Anno e una selezione di mesi
* Scaricare l'armonizzazione fornendo Canale, Anno e mesi
* Fare il download dei report inviati
* Richiedere assistenza sull'applicazione attraverso un sistema di ticketing

## Installazione iniziale

A seconda dell'IDE di sviluppo scelto, si potrà utilizzare Gradle o Maven per importare il progetto.
Per quanto riguarda Gradle, è incluso il nuovo **Gradle Wrapper** che permette di eseguire i comandi di **Gradle** 
senza aver per forza installato lo stesso.

Il progetto è strutturato a multi progetto: il progetto principale contiene sia il frontend che il backend.
è possibile lanciare i task di **Gradle** avvalendosi della linea di comando o della GUI dell'IDE scelto

Non è richiesto nessun setup specifico, tutti i percorsi sono relativi al progetto, e lanciando i task attraverso
**Gradle**, se lo avete già installato, o il suo **Wrapper**, è possibile installare e eseguire **Node**, **NPM** e **Yarn**
(package manager alternativo a NPM)

## Esecuzione

per far partire il frontend è sufficente far partire da linea di comando

`gradle yarnStart`

la prima volta ci vorrà un po' perchè Gradle inizierà ad installare **Node**, **NPM**, e **Yarn** e solo 
successivamnete farà partire **Angular-CLI** server.

è possibile poi far partire l'ambiente embedded di tomcat con il task di **Spring Boot** bootRun

`gradle bootRun`

## Produzione pacchetti

per produrre i pacchetti da distribuire sull'application server/servlet container eseguire

`gradle war`

**Gradle** produrrà prima un war e poi l'altro per ambiente di sviluppo, alternativamente aggiungendo 
`--parallel` al comando di prima genererà entrambi i pacchetti in parallelo

è possibile specificare l'ambiente attraverso il comando

`gradle war -Penv=nomeAmbiente`

dove nomaAmbiente può essere _dev_, _prod_ o _test_

## Distribuzione automatica

E' possibile anche distribuire i pacchetti in un ambiente tomcat v7 remoto eseguendo il comando

`gradle cargoRedeployRemote -Penv=nomeAmbiente`

o in caso il pacchetto sia già deployato sul server

`gradle cargoRedeployRemote -Penv=nomeAmbiente`

E' necessario prima di eseguire questi comandi di criptare la username e la password del
manager di tomcat con il comando

`gradle addCredentials --key tomcatDevUsername --value valore`
`gradle addCredentials --key tomcatDevPassword --value valore`

E' oltremodo possibile concatenare i comandi di pacchettizzazione e deploy nel seguente modo

`gradle war cargoRedeployRemote -Penv=nomeAmbiente`
