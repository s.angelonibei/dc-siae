package com.app.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.*;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Configuration
public class RestTemplateFactory {

  @Qualifier("serializingObjectMapper")
  @Autowired
  private ObjectMapper objectMapper;

  @Bean
  public RestTemplate createRestTemplate(RestTemplateBuilder restTemplateBuilder) {
    List<HttpMessageConverter<?>> converters = new ArrayList<>();
    MappingJackson2HttpMessageConverter jsonConverter = new MappingJackson2HttpMessageConverter();
    jsonConverter.setObjectMapper(objectMapper);
    converters.add(jsonConverter);

//    ClientHttpRequestFactory factory = new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory());

//    RestTemplate restTemplate = new RestTemplate(factory);
//    RestTemplate restTemplate = new RestTemplate();
//    HttpComponentsClientHttpRequestFactory rf =
//      (HttpComponentsClientHttpRequestFactory) restTemplate.getRequestFactory();
//    rf.setReadTimeout(-1);
//    rf.setConnectTimeout(-1);

//    restTemplate.setInterceptors(Collections.<ClientHttpRequestInterceptor>singletonList(new RequestResponseLoggingInterceptor()));
//    restTemplate.setMessageConverters(converters);

    return restTemplateBuilder
      .setConnectTimeout(3600000)
      .setReadTimeout(3600000)
      .additionalMessageConverters(converters)
      .build();
  }
}
