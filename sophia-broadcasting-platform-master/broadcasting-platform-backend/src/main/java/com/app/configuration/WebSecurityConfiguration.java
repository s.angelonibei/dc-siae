package com.app.configuration;

import com.app.security.auth.JwtAuthenticationEntryPoint;
import com.app.security.auth.JwtAuthenticationTokenFilter;
import com.app.service.JwtUserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {
  private JwtAuthenticationEntryPoint unauthorizedHandler;
  @Autowired
  private JwtUserDetailsServiceImpl userDetailsService;

  @Autowired
  public void setUnauthorizedHandler(JwtAuthenticationEntryPoint unauthorizedHandler) {
    this.unauthorizedHandler = unauthorizedHandler;
  }

  @Autowired
  public void configureAuthentication(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
    authenticationManagerBuilder
      .userDetailsService(userDetailsService)
      .passwordEncoder(passwordEncoder());
  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Bean
  public JwtAuthenticationTokenFilter authenticationTokenFilterBean() {
    return new JwtAuthenticationTokenFilter();
  }
/*
  @Bean
  public StubLoggingFilter stubLoggingFilter(){
    return new StubLoggingFilter();
  }*/

  @Override
  protected void configure(HttpSecurity httpSecurity) throws Exception {
    httpSecurity
      .csrf().disable()
      .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
      .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
      .cors().and()
      .authorizeRequests()
      .antMatchers(
        HttpMethod.GET,
        "/",
        "/**/*.html",
        "/**/*.{png,jpg,jpeg,svg.ico}",
        "/**/*.css",
        "/**/*.js"
      ).permitAll()
      .antMatchers("/api/auth/**").permitAll()
//      .antMatchers("/rest/**").permitAll()
      .anyRequest().authenticated();

    httpSecurity.addFilterBefore(authenticationTokenFilterBean(), UsernamePasswordAuthenticationFilter.class);
//      .addFilterAfter(stubLoggingFilter(), JwtAuthenticationTokenFilter.class);

    httpSecurity.headers().cacheControl();
  }
}
