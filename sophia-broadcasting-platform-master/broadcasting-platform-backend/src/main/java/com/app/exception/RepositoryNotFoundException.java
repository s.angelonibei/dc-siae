package com.app.exception;

import com.app.service.MessageByLocaleService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.NOT_FOUND)
public class RepositoryNotFoundException extends CustomException {
  public RepositoryNotFoundException(MessageByLocaleService messageByLocaleService) {
    super(messageByLocaleService, "exception.repository.not.found");
  }
}
