package com.app.exception;

import com.app.service.MessageByLocaleService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.NOT_FOUND)
public class ConfigurationNotFoundException extends CustomException {
  public ConfigurationNotFoundException(MessageByLocaleService messageByLocaleService) {
    super(messageByLocaleService, "exception.configuration.not.found");
  }
}
