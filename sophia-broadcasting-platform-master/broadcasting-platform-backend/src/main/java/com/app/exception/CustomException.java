package com.app.exception;

import com.app.service.MessageByLocaleService;

class CustomException extends RuntimeException {
  CustomException(MessageByLocaleService messageByLocaleService, String key) {
    super(messageByLocaleService.getMessage(key));
  }

  CustomException(MessageByLocaleService messageByLocaleService, Throwable throwable) {
    super(messageByLocaleService.getMessage("exception.user.not.authorized"), throwable);
  }
}
