package com.app.exception;

import com.app.service.MessageByLocaleService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.UNAUTHORIZED)
public class AuthenticationException extends CustomException {
  public AuthenticationException(MessageByLocaleService messageByLocaleService) {
    super(messageByLocaleService, "exception.user.not.authorized");
  }
  public AuthenticationException(MessageByLocaleService messageByLocaleService, Throwable throwable) {
    super(messageByLocaleService, throwable);
  }
}
