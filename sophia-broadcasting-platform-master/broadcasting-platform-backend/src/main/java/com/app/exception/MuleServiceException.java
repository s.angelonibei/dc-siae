package com.app.exception;

import com.app.service.MessageByLocaleService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class MuleServiceException extends CustomException {
  public MuleServiceException(MessageByLocaleService messageByLocaleService) {
    super(messageByLocaleService, "exception.mule.service");
  }
}
