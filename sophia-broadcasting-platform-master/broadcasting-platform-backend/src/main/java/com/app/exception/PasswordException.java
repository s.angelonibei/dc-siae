package com.app.exception;

import com.app.service.MessageByLocaleService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class PasswordException extends CustomException {
  public PasswordException(MessageByLocaleService messageByLocaleService) {
    super(messageByLocaleService, "exception.password.not.valid");
  }
}
