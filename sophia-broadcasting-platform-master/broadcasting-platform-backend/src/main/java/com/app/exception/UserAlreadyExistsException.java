package com.app.exception;

import com.app.service.MessageByLocaleService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.CONFLICT)
public class UserAlreadyExistsException extends CustomException {
  public UserAlreadyExistsException(MessageByLocaleService messageByLocaleService) {
    super(messageByLocaleService, "exception.user.already.exists");
  }
}
