package com.app.exception;

import com.app.service.MessageByLocaleService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class PasswordAlreadyExistsException extends CustomException{

  public PasswordAlreadyExistsException(MessageByLocaleService messageByLocaleService) {
    super(messageByLocaleService, "exception.password.already.used");
  }
}
