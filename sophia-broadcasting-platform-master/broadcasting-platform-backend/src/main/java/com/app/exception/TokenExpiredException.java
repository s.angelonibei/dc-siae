package com.app.exception;

import com.app.service.MessageByLocaleService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class TokenExpiredException extends CustomException {

  public TokenExpiredException(MessageByLocaleService messageByLocaleService) {
    super(messageByLocaleService, "exception.token.invalid");
  }
}
