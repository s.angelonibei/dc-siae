package com.app.security.auth;

import com.app.entity.Broadcasters;
import com.app.entity.PerfMusicProvider;
import com.app.response.MusicProviderResponse;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

@Data
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class JwtUser implements UserDetails {
    private final Long id;
    private final String email;
    private final Broadcasters broadcasters;
    private final MusicProviderResponse musicProvider;
    private final String ruolo;
    private final String password;
    private final String username;

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }
}
