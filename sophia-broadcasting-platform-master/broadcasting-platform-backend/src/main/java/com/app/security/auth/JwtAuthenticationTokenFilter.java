package com.app.security.auth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.Collections;

public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {
  private final Logger LOG = LoggerFactory.getLogger(this.getClass());
  @Value("${auth.jwt.header}")
  private String tokenHeader;

  @Autowired
  private JwtUtil jwtTokenUtil;

  @Autowired
  private UserDetailsService userDetailsService;


  @Autowired
  public void setJwtTokenUtil(JwtUtil jwtUtil) {
    this.jwtTokenUtil = jwtUtil;
  }

  @Override
  @Transactional
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                  FilterChain chain) throws ServletException, IOException {

    String authToken = request.getHeader(this.tokenHeader);
    LOG.debug(authToken);
    String username = jwtTokenUtil.getUsernameFromToken(authToken);

    LOG.debug(username);

    if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
      UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);

      // Ricostruisco l userdetails con i dati contenuti nel token
      // controllo integrita' token
      if (jwtTokenUtil.validateToken(authToken, userDetails)) {
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
        if (LOG.isInfoEnabled())
          LOG.info("Authenticated user {}, setting security context", username);
        SecurityContextHolder.getContext().setAuthentication(authentication);
      }
    }

    chain.doFilter(request, response);

  }
}
