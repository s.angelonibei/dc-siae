package com.app.security.auth;

import com.app.entity.User;
import com.app.response.MusicProviderResponse;

public final class JwtUserFactory {

    private JwtUserFactory() {}

    public static JwtUser create(User user) {
        return new JwtUser(
                user.getId(),
                user.getEmail(),
                user.getBroadcasters(),
                user.getMusicProvider() != null ?
                  new MusicProviderResponse(user.getMusicProvider()):
                  null,
                user.getRuolo().getRuolo(),
                user.getPassword(),
                user.getUsername());
    }
}
