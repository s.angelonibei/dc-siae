package com.app.security.auth;

import com.app.entity.Broadcasters;
import com.app.entity.PerfMusicProvider;
import com.app.response.MusicProviderResponse;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mobile.device.Device;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.app.model.JWTConstants.EXPIRES;
import static com.app.model.JWTConstants.SECRET;

@Component
public class JwtUtil implements Serializable {
  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  private static final String CLAIM_KEY_USERNAME = "sub";
  private static final String CLAIM_KEY_AUDIENCE = "audience";
  private static final String CLAIM_KEY_CREATED = "iat";

  private static final String AUDIENCE_UNKNOWN = "unknown";
  private static final String AUDIENCE_WEB = "web";
  private static final String AUDIENCE_MOBILE = "mobile";
  private static final String AUDIENCE_TABLET = "tablet";

  private static final String CLAIM_KEY_EMAIL = "email";
  private static final String CLAIM_KEY_IMAGE = "image";
  private static final String CLAIM_KEY_ID = "id";
  private static final String CLAIM_KEY_BROADCASTERS = "broadcasters";
  private static final String CLAIM_KEY_MUSIC_PROVIDER = "musicProvider";
  private static final String CLAIM_KEY_ROLE = "ruolo";
  @Value("${auth.jwt.secret}")
  private String secret;

  @Value("${auth.jwt.expires}")
  private Long expiration;

  public Long getUserIdFromToken(String token) {
    Long id = null;
    try {
      final Claims claims = getClaimsFromToken(token);
      id = Long.valueOf((Integer) claims.get(CLAIM_KEY_ID));
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
    }
    return id;
  }

  public String getUsernameFromToken(String token) {
    String username;
    try {
      final Claims claims = getClaimsFromToken(token);
      username = claims.getSubject();
    } catch (Exception e) {
      username = null;
    }
    return username;
  }

  public Date getCreationDateFromToken(String token) {
    Date created;
    try {
      final Claims claims = getClaimsFromToken(token);
      created = new Date((Long) claims.get(CLAIM_KEY_CREATED));
    } catch (Exception e) {
      created = null;
    }
    return created;
  }

  private Date getExpirationDateFromToken(String token) {
    Date expiration;
    try {
      final Claims claims = getClaimsFromToken(token);
      expiration = claims.getExpiration();
    } catch (Exception e) {
      expiration = null;
    }
    return expiration;
  }

  public String generateToken(UserDetails userDetails, Device device) {
    Map<String, Object> claims = new HashMap<>();
    JwtUser jwtUser = (JwtUser) userDetails;
    claims.put(CLAIM_KEY_ID, jwtUser.getId());
    claims.put(CLAIM_KEY_USERNAME, userDetails.getUsername());
    claims.put(CLAIM_KEY_BROADCASTERS, jwtUser.getBroadcasters());
    claims.put(CLAIM_KEY_ROLE, jwtUser.getRuolo());
    claims.put(CLAIM_KEY_EMAIL, jwtUser.getEmail());
    if(jwtUser.getBroadcasters()!=null)
      claims.put(CLAIM_KEY_IMAGE, jwtUser.getBroadcasters().getImage());
    claims.put(CLAIM_KEY_MUSIC_PROVIDER, jwtUser.getMusicProvider());
//    claims.put(CLAIM_KEY_NAME_DEFAULT_REPERTORIO, jwtUser.getDefaultRepertorio().getNome());
//    claims.put(CLAIM_KEY_URL_DEFAULT_REPERTORIO, jwtUser.getDefaultRepertorio().getUrlPath());
    claims.put(CLAIM_KEY_AUDIENCE, generateAudience(device));
    return generateToken(claims);
  }

  public String refreshToken(String token) {
    String refreshedToken;
    try {
      final Claims claims = getClaimsFromToken(token);
      claims.put(CLAIM_KEY_CREATED, new Date());
      refreshedToken = generateToken(claims);
    } catch (Exception e) {
      refreshedToken = null;
    }
    return refreshedToken;
  }

  public Boolean validateToken(String token, UserDetails userDetails) {
    JwtUser user = (JwtUser) userDetails;
    final String username = getUsernameFromToken(token);
    if (username == null) {
      return false;
    } else {
      return username.equals(user.getUsername()) && !isTokenExpired(token);
    }
  }

  public JwtUser getUserDetails(String token) {

    if (token == null) {
      return null;
    }
    try {
      final Claims claims = getClaimsFromToken(token);
      return new JwtUser(
        (Long) claims.get(CLAIM_KEY_ID),
        (String) claims.get(CLAIM_KEY_EMAIL),
        (Broadcasters) claims.get(CLAIM_KEY_BROADCASTERS),
        (MusicProviderResponse) claims.get(CLAIM_KEY_MUSIC_PROVIDER),
        (String) claims.get(CLAIM_KEY_ROLE),
        "",
        claims.getSubject()
//        ,(Repertorio) claims.get(CLAIM_KEY_NAME_DEFAULT_REPERTORIO)
      );
    } catch (Exception e) {
      return null;
    }

  }

  private Claims getClaimsFromToken(String token) {
    Claims claims;
    token = token.replace("Bearer ","");
    try {
      claims = Jwts.parser()
        .setSigningKey(SECRET)
        .parseClaimsJws(token)
        .getBody();
    } catch (Exception e) {
      claims = null;
    }
    return claims;
  }

  private Date generateExpirationDate() {
    return new Date(System.currentTimeMillis() + EXPIRES * 1000);
  }

  private Boolean isTokenExpired(String token) {
    final Date expiration = getExpirationDateFromToken(token);
    return expiration.before(new Date());
  }

  private String generateToken(Map<String, Object> claims) {
    return Jwts.builder()
      .setClaims(claims)
      .setExpiration(generateExpirationDate())
      .signWith(SignatureAlgorithm.HS512, SECRET)
      .compact();
  }

  private String generateAudience(Device device) {
    String audience = AUDIENCE_UNKNOWN;
    if (device.isNormal()) {
      audience = AUDIENCE_WEB;
    } else if (device.isTablet()) {
      audience = AUDIENCE_TABLET;
    } else if (device.isMobile()) {
      audience = AUDIENCE_MOBILE;
    }
    return audience;
  }
}
