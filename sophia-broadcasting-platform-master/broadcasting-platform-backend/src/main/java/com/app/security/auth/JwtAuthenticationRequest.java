package com.app.security.auth;

import com.alkemytech.sophia.broadcasting.enums.Repertorio;
import com.app.entity.Broadcasters;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class  JwtAuthenticationRequest implements Serializable {
    private String username;
    private String email;
    private String password;
    private Broadcasters broadcasters;
    private Long idRuolo;
    private Repertorio defaultRepertorio;
    private List<Repertorio> repertori;
}
