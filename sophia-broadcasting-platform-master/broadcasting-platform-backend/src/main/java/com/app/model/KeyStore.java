package com.app.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.inject.Singleton;

@Data
@ConfigurationProperties(prefix = "mule")
@JsonAutoDetect
@Component
@NoArgsConstructor
@Singleton
public class KeyStore {
  private String type;
  private boolean selfSigned;
  private String file;
  private String alias;
  private String password;
}
