package com.app.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.inject.Singleton;

@Data
@ConfigurationProperties(prefix = "mule")
@JsonAutoDetect
@Component
@NoArgsConstructor
@Singleton
public class MuleConfig {
  private String host;
  @Value("${mule.mail.path}")
  private String mailService;
  private KeyStore keyStore;
  private boolean verifyHostnames;
}
