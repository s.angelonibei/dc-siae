package com.app.model;

import com.app.service.MessageByLocaleService;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Data
@ConfigurationProperties(prefix = "auth.reset.password")
@JsonAutoDetect
@Component
public class ResetPasswordConfig implements Serializable {
  private long expiration;
  private String regex;
  private short length;

  @Value("${auth.reset.password.length}")
  private short passwordLength;

  @Value("${auth.reset.password.special_chars}")
  private String specialChars;

  private MessageByLocaleService messageByLocaleService;

  @Autowired
  public void setMessageByLocaleService(final MessageByLocaleService messageByLocaleService) {
    this.messageByLocaleService = messageByLocaleService;
  }

  public String getPasswordConstraints() {
    Object[] paramsArray = {passwordLength, specialChars.replaceAll("\\\\","")};
    return messageByLocaleService.getMessage("password.constraints", paramsArray);
  }
  public String getExceptionPasswordNotValid() {
    Object[] paramsArray = {passwordLength, specialChars.replaceAll("\\\\","")};
    return messageByLocaleService.getMessage("exception.password.not.valid", paramsArray);
  }
}
