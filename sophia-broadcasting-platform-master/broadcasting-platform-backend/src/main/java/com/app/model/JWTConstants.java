package com.app.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@ConfigurationProperties(prefix = "auth.jwt")
@JsonAutoDetect
@Component
public class JWTConstants implements Serializable {
  public static final String TOKEN_PREFIX = "Bearer ";
  public static final String HEADER = "Authorization";
  public static final String CLIENT_ID = "sDx3jkx3bMpSY6VLmJMyVatdDDZ5yQmq";
  public static final String SECRET = "h0dbMbOiUr1XZTKhAlhjoZ1asTiBgVSR";
  public static final long EXPIRES = 3600;
}
