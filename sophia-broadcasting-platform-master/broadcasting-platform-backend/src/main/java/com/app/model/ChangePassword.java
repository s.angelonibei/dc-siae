package com.app.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class ChangePassword {
    private Long idUtente;
    private String ruolo;
    private String password;
    private String password2;
}
