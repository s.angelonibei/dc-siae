package com.app.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Data
@ConfigurationProperties(prefix = "cloud.aws.credentials")
@JsonAutoDetect
@Component
public class Credentials implements Serializable {
    private String accessKey;
    private String secretKey;
    private boolean instanceProfile;
}
