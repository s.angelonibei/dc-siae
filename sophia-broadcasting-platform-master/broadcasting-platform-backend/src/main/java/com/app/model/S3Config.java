package com.app.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Data
@JsonAutoDetect
@Component
public class S3Config implements Serializable{
    @Value("${s3.broadcasting.key_prefix}")
    private String keyPrefix;
    @Value("${s3.broadcasting.bucket}")
    private String bucket;
    private Credentials credentials;

    @Value("${cloud.aws.stack.auto}")
    private boolean stack;
    @Value("${cloud.aws.region.static}")
    private String region;
}
