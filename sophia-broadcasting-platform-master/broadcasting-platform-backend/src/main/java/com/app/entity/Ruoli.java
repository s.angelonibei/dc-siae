package com.app.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;


/**
 * The persistent class for the bdc_ruoli database table.
 * 
 */
@Entity
@Table(name="bdc_ruoli")
@NamedQuery(name="Ruoli.findAll", query="SELECT b FROM Ruoli b")
@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Ruoli implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private Long id;

	@Column(nullable=false)
	private String ruolo;

  public Ruoli(Long id) {
    this.id = id;
  }
}
