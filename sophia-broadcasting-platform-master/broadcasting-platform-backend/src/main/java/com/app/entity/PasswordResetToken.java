package com.app.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "BDC_PASSWORD_RESET_TOKEN")
@Data
@NoArgsConstructor
@ToString(exclude = "utente")
@EqualsAndHashCode
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PasswordResetToken implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="ID", unique=true, nullable=false)
    private Long id;

    @NotNull
    @Column(name="TOKEN")
    @JsonIgnore
    private String token;

    @NotNull
    @JsonManagedReference
    @ManyToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "ID_UTENTE")
    private User utente;

    @NotNull
    @Column(name = "ACTIVE")
    @JsonIgnore
    private Boolean active;

    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    @Column(name="DATA_CREAZIONE")
    @JsonIgnore
    private Date dataCreazione;

    public PasswordResetToken(@NotNull String token, @NotNull User utente) {
        this.token = token;
        this.utente = utente;
    }
}
