package com.app.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the BDC_USER_TOKEN database table.
 *
 */
@Entity
@Table(name="BDC_USER_TOKEN")
@Data
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class UserToken implements Serializable {
	private static final long serialVersionUID = 1L;

  @EmbeddedId
  private UserTokenPK id;

  //bi-directional many-to-one association to UserRole
  @ManyToOne(fetch=FetchType.LAZY)
  @JoinColumn(name="BKO_USER", nullable=false)
  private UserRole userRole;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name="DATA_CREAZIONE", nullable=false)
  private Date dataCreazione;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name="DATA_FINE_VALIDITA", nullable=false)
  private Date dataFineValidita;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name="DATA_LOGIN")
  private Date dataLogin;

  @Column(name="REPERTORI", nullable=false)
  private String repertori;

  @Column(name="REPERTORIO_DEFAULT", nullable=false, length=100)
  private String repertorioDefault;

  @Column(name="TOKEN_NAVIGAZIONE")
  private String tokenNavigazione;

  //bi-directional many-to-one association to BdcUtenti
  @ManyToOne(fetch=FetchType.LAZY)
  @JoinColumn(name="BDC_USER", nullable=false, insertable=false, updatable=false)
  private User user;
}
