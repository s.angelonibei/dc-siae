package com.app.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


/**
 * The persistent class for the BDC_REPERTORIO database table.
 * 
 */
@Entity
@Table(name="BDC_REPERTORIO")
@NamedQueries({
  @NamedQuery(name="Repertorio.findAll", query="SELECT b FROM Repertorio b"),
  @NamedQuery(name="Repertorio.getRepertoryByUtente", query="SELECT new com.app.response.UtentiRepertorioResponse(ur.idUtenteRepertorio, ur.defaultRepertorio,ur.repertorio) FROM UtentiRepertorio ur where ur.utente = ?1"),
  @NamedQuery(name="Repertorio.getRepertoriosByNomeIn", query="SELECT new com.app.response.UtentiRepertorioResponse(ur.idUtenteRepertorio, ur.defaultRepertorio,ur.repertorio) FROM UtentiRepertorio ur where ur.utente = ?1"),
})

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Repertorio implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "nome")
    private String nome;

    @Column(length=100)
    private String descrizione;

    @Column(name="url_path", length=100)
    private String urlPath;

    //bi-directional many-to-one association to UtentiRepertorio
    @JsonBackReference
    @ManyToMany(mappedBy = "repertori")
    private List<User> utenti;

    //bi-directional many-to-one association to UtilizationFile
    @JsonBackReference
    @OneToMany(mappedBy="repertorio")
    private List<UtilizationFile> utilizationFiles;

    public Repertorio(String nome) {
        this.nome = nome;
    }
}
