package com.app.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the BDC_OLD_PASSWORD database table.
 *
 */
@Entity
@Table(name="BDC_OLD_PASSWORD")
@NamedQuery(name="BdcOldPassword.findAll", query="SELECT b FROM OldPassword b")
@Data
@NoArgsConstructor
@EqualsAndHashCode(exclude = "utente")
@ToString(exclude = "utente")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class OldPassword implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_old_password", unique=true, nullable=false)
	private Long idOldPassword;

	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	@Column(name="data_creazione")
	private Date dataCreazione;

	@Temporal(TemporalType.TIMESTAMP)
	@UpdateTimestamp
	@Column(name="data_ultima_modifica")
	private Date dataUltimaModifica;

	@Column(name="old_password", nullable=false)
	private String oldPassword;

	//bi-directional many-to-one association to BdcUtenti
	@ManyToOne(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name="user_id", updatable = false)
	@JsonBackReference
	private User utente;

/*  @ManyToOne(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name="user_id")
  @JsonIgnore
  private BdcUtenti bdcUtenti;

  @ManyToOne(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name="user_id")
  @JsonIgnore
  private PerfRsUtente perfRsUtente;*/

}
