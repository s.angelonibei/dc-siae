package com.app.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigInteger;
import java.util.Date;


/**
 * The persistent class for the PERF_RS_UTILIZATION_FILE database table.
 * 
 */
@Entity
@Table(name="PERF_RS_UTILIZATION_FILE")
@NamedQuery(name="PerfRsUtilizationFile.findAll", query="SELECT p FROM PerfRsUtilizationFile p")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PerfRsUtilizationFile implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID", unique=true, nullable=false)
	private BigInteger id;

	@Column(name="ANNO")
	private Short anno;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_PROCESSAMENTO")
	private Date dataProcessamento;

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_UPLOAD", nullable=false)
	private Date dataUpload;

  //bi-directional many-to-one association to PerfRsUtente
  @JsonBackReference
  @ManyToOne(fetch=FetchType.LAZY)
  @JoinColumn(name="ID_UTENTE")
  private PerfRsUtente utente;

	@Column(name="MESE")
	private Short mese;

	@Column(name="NOME_FILE", nullable=false)
	private String nomeFile;

	@Column(name="PERCORSO", nullable=false)
	private String percorso;

	@Column(name="STATO", nullable=false, length=1)
	private String stato;

	@Column(name="TIPO_UPLOAD", nullable=false)
	private String tipoUpload;

	//bi-directional many-to-one association to PerfMusicProvider
  @JsonBackReference
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MUSIC_PROVIDER", nullable=false)
	private PerfMusicProvider musicProvider;

  //bi-directional many-to-one association to PerfPalinsesto
  @JsonBackReference
  @ManyToOne(fetch=FetchType.LAZY)
  @JoinColumn(name="ID_PALINSESTO")
  private PerfPalinsesto palinsesto;

}
