package com.app.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;


/**
 * The persistent class for the BDC_UTENTI_REPERTORIO database table.
 *
 */
@Entity
@Table(name="BDC_UTENTI_REPERTORIO")
@NamedQuery(name="UtentiRepertorio.findAll", query="SELECT b FROM UtentiRepertorio b")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class UtentiRepertorio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_utente_repertorio", unique=true, nullable=false)
	private Long idUtenteRepertorio;

	@Column(name="default_repertorio", nullable=false)
	private Boolean defaultRepertorio;

	//bi-directional many-to-one association to Repertorio
  @JsonBackReference
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="nome_repertorio", nullable=false)
	private Repertorio repertorio;

	//bi-directional many-to-one association to Utenti
  @JsonBackReference
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_utente", nullable=false)
	private User utente;

}
