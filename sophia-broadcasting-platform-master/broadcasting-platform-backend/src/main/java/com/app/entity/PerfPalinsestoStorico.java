package com.app.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the PERF_PALINSESTO_STORICO database table.
 * 
 */
@Entity
@Table(name="PERF_PALINSESTO_STORICO")
@NamedQuery(name="PerfPalinsestoStorico.findAll", query="SELECT p FROM PerfPalinsestoStorico p")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PerfPalinsestoStorico implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_PALINSESTO_STORICO", unique=true, nullable=false)
	private String idPalinsestoStorico;

	@Column(name="ATTIVO", nullable=false)
	private Boolean attivo;

	@Column(name="CODICE_DITTA", nullable=false, length=255)
	private String codiceDitta;

	@Column(name="CODICE_ID", length=2)
	private String codiceId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_CREAZIONE", nullable=false)
	private Date dataCreazione;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_FINE_VALIDITA")
	private Date dataFineValidita;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_INIZIO_VALIDITA")
	private Date dataInizioValidita;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_ULTIMA_MODIFICA", nullable=false)
	private Date dataUltimaModifica;

	@Column(name="NOME", nullable=false, length=255)
	private String nome;

	@Column(name="UTENTE_ULTIMA_MODIFICA", length=255)
	private String utenteUltimaModifica;

	//bi-directional many-to-one association to PerfMusicProvider
  @JsonBackReference
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MUSIC_PROVIDER")
	private PerfMusicProvider musicProvider;

	//bi-directional many-to-one association to PerfPalinsesto
  @JsonBackReference
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PALINSESTO", nullable=false)
	private PerfPalinsesto palinsesto;

}
