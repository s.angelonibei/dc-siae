package com.app.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.math.BigInteger;


/**
 * The persistent class for the PERF_NEWS database table.
 * 
 */
@Entity
@Table(name="PERF_NEWS")
@NamedQuery(name="PerfNew.findAll", query="SELECT p FROM PerfNews p")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PerfNews implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_PERF_NEWS", unique=true, nullable=false)
	private BigInteger idPerfNews;

	@Column(name="CREATOR", nullable=false, length=100)
	private String creator;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DEACTIVATION_DATE")
	private Date deactivationDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="INSERT_DATE", nullable=false)
	private Date insertDate;

	@Lob
	@Column(name="NEWS", nullable=false)
	private String news;

	@Column(name="TITLE", nullable=false, length=45)
	private String title;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="VALID_FROM", nullable=false)
	private Date validFrom;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="VALID_TO", nullable=false)
	private Date validTo;

	//bi-directional many-to-one association to PerfMusicProvider
  @JsonBackReference
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MUSIC_PROVIDER")
	private PerfMusicProvider musicProvider;

	//bi-directional many-to-one association to PerfRsUtente
  @JsonBackReference
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_UTENTE")
	private PerfRsUtente utente;
}
