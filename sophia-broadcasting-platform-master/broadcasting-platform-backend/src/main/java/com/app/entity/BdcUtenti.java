package com.app.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "bdc_utenti")
@Data
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class BdcUtenti implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(unique=true, nullable=false)
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    @Column(name="data_creazione")
    private Date dataCreazione;

    @Temporal(TemporalType.TIMESTAMP)
    @UpdateTimestamp
    @Column(name="data_ultima_modifica")
    private Date dataUltimaModifica;

    @Column(nullable=false)
    private String email;

    @JsonIgnore
    @Column(nullable=false)
    private String password;

    @Column(nullable=false)
    private String username;

    @Column(name="utente_ultima_modifica")
    private String utenteUltimaModifica;

    @JsonBackReference
    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
      name = "BDC_UTENTI_REPERTORIO",
      joinColumns = { @JoinColumn(name = "id_utente") },
      inverseJoinColumns = { @JoinColumn(name = "nome_repertorio") }
    )
    private List<Repertorio> repertori;

    //bi-directional many-to-one association to Broadcaster
    @JsonBackReference
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="id_broadcaster", nullable=false)
    private Broadcasters broadcasters;

    //bi-directional many-to-one association to Ruoli
    @JsonManagedReference
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="id_ruolo", nullable=false)
    private Ruoli ruolo;

    //bi-directional many-to-one association to UtilizationFile
    @JsonManagedReference
    @OneToMany(mappedBy="utenti")
    private List<UtilizationFile> utilizationFiles;

    //bi-directional many-to-one association to OldPassword
    @JsonBackReference
    @OneToMany(mappedBy= "utente", cascade = CascadeType.ALL)
    private List<OldPassword> oldPasswords;

    @JsonBackReference
    @OneToMany(mappedBy= "utente")
    private List<PasswordResetToken> passwordResetTokens;

    public BdcUtenti(String username, String email, String password, Broadcasters broadcasters, Ruoli ruolo) {
        this.email = email;
        this.password = password;
        this.username = username;
        this.broadcasters = broadcasters;
        this.ruolo = ruolo;
    }
}
