package com.app.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PERF_PALINSESTO database table.
 * 
 */
@Entity
@Table(name="PERF_PALINSESTO")
@NamedQuery(name="PerfPalinsesto.findAll", query="SELECT p FROM PerfPalinsesto p")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PerfPalinsesto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_PALINSESTO", unique=true, nullable=false)
	private BigInteger idPalinsesto;

	@Column(name="ATTIVO", nullable=false)
	private Boolean attivo;

	@Column(name="CODICE_DITTA", nullable=false)
	private String codiceDitta;

	@Column(name="CODICE_ID", length=2)
	private String codiceId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_CREAZIONE", nullable=false)
	private Date dataCreazione;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_FINE_VALIDITA")
	private Date dataFineValidita;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_INIZIO_VALIDITA")
	private Date dataInizioValidita;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_ULTIMA_MODIFICA", nullable=false)
	private Date dataUltimaModifica;

	@Column(name="NOME", nullable=false)
	private String nome;

	@Column(name="UTENTE_ULTIMA_MODIFICA")
	private String utenteUltimaModifica;

	//bi-directional many-to-one association to PerfMusicProvider
  @JsonBackReference
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MUSIC_PROVIDER")
	private PerfMusicProvider musicProvider;

	//bi-directional many-to-one association to PerfPalinsestoStorico
  @JsonManagedReference
	@OneToMany(mappedBy= "palinsesto")
	private List<PerfPalinsestoStorico> storicoPalinsesti;

	//bi-directional many-to-one association to PerfPuntoVendita
  @JsonManagedReference
	@OneToMany(mappedBy= "palinsesto")
	private List<PerfPuntoVendita> puntiVendita;

  //bi-directional many-to-one association to PerfRsUtilizationFile
  @JsonManagedReference
  @OneToMany(mappedBy="palinsesto")
  private List<PerfRsUtilizationFile> utilizationFiles;
}
