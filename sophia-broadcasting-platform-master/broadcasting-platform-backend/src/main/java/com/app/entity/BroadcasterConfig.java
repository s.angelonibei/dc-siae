package com.app.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the BDC_BROADCASTER_CONFIG database table.
 * 
 */
@Entity
@Table(name="BDC_BROADCASTER_CONFIG")
@NamedQueries({
		@NamedQuery(name = "BroadcasterConfig.findActive", query ="SELECT x FROM BroadcasterConfig x WHERE ((CURRENT_TIMESTAMP BETWEEN x.validFrom AND x.validTo) or (CURRENT_TIMESTAMP >= x.validFrom and x.validTo is null))"),
		@NamedQuery(name = "BroadcasterConfig.findActiveByIdBroadcaster", query ="SELECT x FROM BroadcasterConfig x WHERE ((CURRENT_TIMESTAMP BETWEEN x.validFrom AND x.validTo) or (CURRENT_TIMESTAMP >= x.validFrom and x.validTo is null)) AND x.broadcasters.id = ?1"),
		@NamedQuery(name="BroadcasterConfig.findAll", query="SELECT b FROM BroadcasterConfig b")
})
@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class BroadcasterConfig implements Serializable {
  private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_BROADCASTER_CONFIG", unique=true, nullable=false)
	private Long idBroadcasterConfig;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name="CREATION_DATE")
  private Date creationDate;

  @Column(name="CREATION_USER", length=50)
  private String creationUser;

	@Column(name="FIELDS_MAPPING")
	private String fieldsMapping;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name="MODIFY_DATE")
  private Date modifyDate;

  @Column(name="MODIFY_USER", length=50)
  private String modifyUser;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name="VALID_FROM", nullable=false)
  private Date validFrom;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name="VALID_TO")
  private Date validTo;

	//bi-directional many-to-one association to Broadcaster
  @JsonManagedReference
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_BROADCASTER", nullable=false)
	private Broadcasters broadcasters;

  //bi-directional many-to-one association to BdcCanali
  @JsonManagedReference
  @ManyToOne(fetch=FetchType.LAZY)
  @JoinColumn(name="ID_CHANNEL")
  private Canali canali;

  //bi-directional many-to-one association to BroadcasterConfigTemplate
  @JsonManagedReference
  @ManyToOne(fetch=FetchType.LAZY)
  @JoinColumn(name="CONFIGURATION")
  private BroadcasterConfigTemplate configuration;

}
