package com.app.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the bdc_canali database table.
 *
 */
@Entity
@Table(name="bdc_canali")
@NamedQuery(name="Canali.findAll", query="SELECT b FROM Canali b")
@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Canali implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private Long id;

	private boolean active;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_creazione")
	private Date dataCreazione;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_fine_valid")
	private Date dataFineValid;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_inizio_valid")
	private Date dataInizioValid;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_ultima_modifica")
	private Date dataUltimaModifica;

	private boolean generalista;

	@Column(nullable=false)
	private String nome;

	@Column(name="special_radio")
	private boolean specialRadio;

	@Column(name="tipo_canale", nullable=false, length=1)
	private String tipoCanale;

	@Column(name="utente_ultima_modifica")
	private String utenteUltimaModifica;

	//bi-directional many-to-one association to BroadcasterConfig
	@JsonBackReference
	@OneToMany(mappedBy="canali")
	private List<BroadcasterConfig> bdcBroadcasterConfigs;

	//bi-directional many-to-one association to Broadcaster
	@JsonManagedReference
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_broadcaster", nullable=false)
	private Broadcasters broadcasters;

	//bi-directional many-to-one association to UtilizationFile
}
