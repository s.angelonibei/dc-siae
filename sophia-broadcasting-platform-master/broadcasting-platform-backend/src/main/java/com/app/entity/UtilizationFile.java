package com.app.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the bdc_utilization_file database table.
 * 
 */
@Entity
@Table(name="bdc_utilization_file")
@NamedQuery(name="UtilizationFile.findAll", query="SELECT b FROM UtilizationFile b")
@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class UtilizationFile implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private Long id;

	private Short anno;

	private Integer canale;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_processamento")
	private Date dataProcessamento;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_upload", nullable=false)
	private Date dataUpload;

	@Column(nullable=false)
	private Integer emittente;

	private Short mese;

	@Lob
	@Column(name="nome_file", nullable=false)
	private String nomeFile;

	@Lob
	@Column(nullable=false)
	private String percorso;

	@Column(nullable=false, length=1)
	private String stato;

	@Column(name="tipo_upload", nullable=false)
	private String tipoUpload;

	//bi-directional many-to-one association to Repertorio
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="NOME_REPERTORIO")
	@JsonBackReference
	private Repertorio repertorio;

	//bi-directional many-to-one association to Utenti
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_utente")
	@JsonBackReference
	private User utenti;
}
