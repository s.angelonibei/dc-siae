package com.app.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PERF_MUSIC_PROVIDER database table.
 */
@Entity
@Table(name = "PERF_MUSIC_PROVIDER")
@NamedQueries({
  @NamedQuery(name = "PerfMusicProvider.findAll", query = "SELECT p FROM PerfMusicProvider p"),
})
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PerfMusicProvider implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "ID_MUSIC_PROVIDER", unique = true, nullable = false)
  private BigInteger idMusicProvider;

  @Column(name = "ATTIVO", nullable = false)
  private Boolean attivo;

  @CreationTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "DATA_CREAZIONE", nullable = false)
  private Date dataCreazione;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "DATA_INIZIO_VALIDITA")
  private Date dataInizioValidita;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "DATA_FINE_VALIDITA")
  private Date dataFineValidita;

  @Column(name = "NOME", nullable = false)
  private String nome;

  //bi-directional many-to-one association to PerfPalinsesto
  @JsonManagedReference
  @OneToMany(mappedBy = "musicProvider")
  private List<PerfPalinsesto> palinsesti;

  //bi-directional many-to-one association to PerfRsUtente
  @JsonManagedReference
  @OneToMany(mappedBy= "musicProvider")
  private List<PerfRsUtente> utenti;

  //bi-directional many-to-one association to PerfRsUtilizationFile
  @JsonManagedReference
  @OneToMany(mappedBy= "musicProvider")
  private List<PerfRsUtilizationFile> utilizationFiles;

  @JsonManagedReference
  @OneToMany(mappedBy= "musicProvider")
  private List<PerfNews> news;
}
