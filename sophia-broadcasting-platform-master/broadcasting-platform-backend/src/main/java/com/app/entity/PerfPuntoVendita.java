package com.app.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the PERF_PUNTO_VENDITA database table.
 * 
 */
@Entity
@Table(name="PERF_PUNTO_VENDITA")
@NamedQuery(name="PerfPuntoVendita.findAll", query="SELECT p FROM PerfPuntoVendita p")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PerfPuntoVendita implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_PUNTO_VENDITA", unique=true, nullable=false)
	private String idPuntoVendita;

	@Column(name="NOME", nullable=false, length=255)
	private String nome;

	//bi-directional many-to-one association to PerfPalinsesto
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PALINSESTO", nullable=false)
  @JsonBackReference
	private PerfPalinsesto palinsesto;
}
