package com.app.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the BDC_BROADCASTER_CONFIG_TEMPLATE database table.
 * 
 */
@Entity
@Table(name="BDC_BROADCASTER_CONFIG_TEMPLATE")
@NamedQuery(name="BroadcasterConfigTemplate.findAll", query="SELECT b FROM BroadcasterConfigTemplate b")
@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class BroadcasterConfigTemplate implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_BROADCASTER_CONFIG_TEMPLATE", unique=true, nullable=false)
	private int idBroadcasterConfigTemplate;

	@Column(name="CONFIGURAZIONE", nullable=false)
	private String configurazione;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_CREAZIONE", nullable=false)
	private Date dataCreazione;

	@Column(name="FLAG_ATTIVO", nullable=false)
	private byte flagAttivo;

	@Column(name="FLAG_DEFAULT", nullable=false)
	private byte flagDefault;

	@Column(name="NOME_TEMPLATE", nullable=false, length=45)
	private String nomeTemplate;

	@Column(name="TIPO_EMITTENTE", nullable=false, length=45)
	private String tipoEmittente;

	//bi-directional many-to-one association to BroadcasterConfig
  @JsonBackReference
	@OneToMany(mappedBy="configuration")
	private List<BroadcasterConfig> broadcasterConfigs;

	public BroadcasterConfig addBdcBroadcasterConfig(BroadcasterConfig bdcBroadcasterConfig) {
		getBroadcasterConfigs().add(bdcBroadcasterConfig);
		bdcBroadcasterConfig.setConfiguration(this);

		return bdcBroadcasterConfig;
	}

	public BroadcasterConfig removeBdcBroadcasterConfig(BroadcasterConfig bdcBroadcasterConfig) {
		getBroadcasterConfigs().remove(bdcBroadcasterConfig);
    bdcBroadcasterConfig.setConfiguration(null);

		return bdcBroadcasterConfig;
	}
}
