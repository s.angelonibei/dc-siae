package com.app.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;


/**
 * The persistent class for the bdc_broadcasters database table.
 *
 */
@Entity
@Table(name="bdc_broadcasters")
@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Broadcasters implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private Long id;

	@Lob
	private byte[] image;

	private String nome;

	@Enumerated(EnumType.STRING)
	@Column(name="tipo_broadcaster")
	private tipoBroadcaster tipoBroadcaster;

  @Column(name = "nuovo_tracciato")
  private String nuovoTracciato;

  public enum tipoBroadcaster {
      TELEVISIONE,
      RADIO
  }

}
