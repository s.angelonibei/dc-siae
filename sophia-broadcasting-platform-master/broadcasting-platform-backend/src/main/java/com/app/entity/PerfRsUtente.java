package com.app.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PERF_RS_UTENTE database table.
 * 
 */
@Entity
@Table(name="PERF_RS_UTENTE")
@NamedQuery(name="PerfRsUtente.findAll", query="SELECT p FROM PerfRsUtente p")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PerfRsUtente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID", unique=true, nullable=false)
	private BigInteger id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_CREAZIONE")
	private Date dataCreazione;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_ULTIMA_MODIFICA")
	private Date dataUltimaModifica;

	@Column(name="EMAIL", nullable=false)
	private String email;

	@Column(name="ID_RUOLO", nullable=false)
	private Short idRuolo;

	@Column(name="PASSWORD", nullable=false)
	private String password;

	@Column(name="USERNAME", nullable=false)
	private String username;

	@Column(name="UTENTE_ULTIMA_MODIFICA")
	private String utenteUltimaModifica;

	//bi-directional many-to-one association to PerfMusicProvider
  @JsonBackReference
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MUSIC_PROVIDER", nullable=false)
	private PerfMusicProvider musicProvider;

  //bi-directional many-to-one association to PerfNew
  @JsonManagedReference
  @OneToMany(mappedBy="utente")
  private List<PerfNews> news;

  //bi-directional one-to-one association to PerfRsUtilizationFile
  @JsonManagedReference
  @OneToMany(mappedBy="utente")
  private List<PerfRsUtilizationFile> utilizationFiles;
}
