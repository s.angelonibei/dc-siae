package com.app.response;

import lombok.*;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class RepertorioResponse implements Serializable {
  private static final long serialVersionUID = 1L;
  private String nome;
  private String descrizione;
  private String urlPath;
}
