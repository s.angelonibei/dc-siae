package com.app.response;

import com.app.entity.PerfMusicProvider;
import lombok.*;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

@Data
@RequiredArgsConstructor
public class S3UploadResponse {
  private String keyPrefix;
  private String bucket;
  private String accessKey;
  private String amzSignature;
  private String policyBase64;
  private String amzDate;
  private String amzCredential;
  private boolean stack;
  private String region;


}
