package com.app.response;

import com.app.entity.PerfMusicProvider;
import lombok.*;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class MusicProviderResponse implements Serializable {
  private BigInteger idMusicProvider;
  private Boolean attivo;
  private Date dataCreazione;
  private Date dataInizioValidita;
  private Date dataFineValidita;
  private String nome;

  public MusicProviderResponse(PerfMusicProvider musicProvider) {
    this.idMusicProvider = musicProvider.getIdMusicProvider();
    this.attivo = musicProvider.getAttivo();
    this.dataCreazione = musicProvider.getDataCreazione();
    this.dataInizioValidita = musicProvider.getDataInizioValidita();
    this.dataFineValidita = musicProvider.getDataFineValidita();
    this.nome = musicProvider.getNome();
  }
}
