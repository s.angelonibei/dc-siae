package com.app.response;

import com.app.entity.Repertorio;
import lombok.*;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class UtentiRepertorioResponse implements Serializable {
  private static final long serialVersionUID = 1L;
  private Long idUtenteRepertorio;
  private Boolean defaultRepertorio;
  private RepertorioResponse repertorio;

  public UtentiRepertorioResponse(Long idUtenteRepertorio, Boolean defaultRepertorio, Repertorio repertorio) {
    this.idUtenteRepertorio = idUtenteRepertorio;
    this.defaultRepertorio = defaultRepertorio;
    this.repertorio = new RepertorioResponse(repertorio.getNome(),repertorio.getDescrizione(),repertorio.getUrlPath());
  }
}
