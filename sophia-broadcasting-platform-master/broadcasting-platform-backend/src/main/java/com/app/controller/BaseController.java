package com.app.controller;

import com.alkemytech.sophia.broadcasting.dto.*;
import com.alkemytech.sophia.broadcasting.enums.Stato;
import com.alkemytech.sophia.broadcasting.model.BdcBroadcasters;
import com.alkemytech.sophia.broadcasting.model.BdcCanali;
import com.alkemytech.sophia.broadcasting.model.BdcInformationFileEntity;
import com.alkemytech.sophia.broadcasting.model.BdcUtenti;
import com.alkemytech.sophia.broadcasting.dto.IdFileRequestDTO;
import com.app.entity.User;
import com.app.model.Credentials;
import com.app.model.S3Config;
import com.app.model.UploadDTO;
import com.app.response.S3UploadResponse;
import com.app.service.MailService;
import com.app.service.UserService;
import com.app.utility.RestClient;
import lombok.extern.apachecommons.CommonsLog;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.*;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.util.Assert;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.math.BigInteger;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.*;

@RestController
@CommonsLog
public class BaseController {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  @Value("${s3.broadcasting.bucket}")
  private String s3Bucket;
  private JavaMailSender mailSender;
  private S3Config s3Config;
  private Credentials credentials;
  private UserService userService;

  @Value("${s3.broadcasting.key_prefix}")
  private String s3KeyPrefix;

  private String backendServiceRoot;
  @Value("${backend.service.canali}")
  private String backendServiceCanali;
  @Value("${backend.service.file.armonizzato}")
  private String backendServiceFileArmonizzato;
  @Value("${backend.service.send.mail}")
  private String backendServiceSendMail;
  @Value("${backend.service.monitoraggio.kpi}")
  private String backendServiceMonitoraggioKpi;
  @Value("${backend.service.news.list}")
  private String backendServiceNewsLists;
  @Value("${backend.service.upload.file}")
  private String backendServiceUploadFile;
  @Value("${backend.service.broadcaster.file.list}")
  private String backendServiceBroadcasterFileList;
  @Value("${backend.service.download.file}")
  private String backendServiceDownloadFile;
  @Value("${backend.service.download.armonizzato}")
  private String backendServiceDownloadArmonizzato;
  @Value("${backend.service.broadcaster}")
  private String backendServiceBroadcaster;
  @Value("${backend.service.broadcaster.config.active}")
  private String backendServiceConfigFindActive;
  @Value("${backend.service.canale}")
  private String backendServiceCanale;
  @Value("${backend.service.user.id}")
  private String backendServiceUserById;
  @Value("${backend.service.information.file}")
  private String backendServiceInformationFile;

  @Value("${backend.service.cancelutilizationrecords}")
  private String backendServiceCancelUtilizationRecords;

  @Value("${backend.service.confirmutilizationrecords}")
  private String backendServiceConfirmUtilizationRecords;

  @Value("${broadcasting.mail.name.subject.prefix}")
  private String subjecPrefix;
  @Value("${broadcasting.mail.name.to}")
  private String to;
  @Value("${spring.mail.username}")
  private String from;

  private RestTemplate restTemplate;
  private RestClient restClient;
  private HttpHeaders httpHeaders;

  private final MailService mailService;

  @Autowired
  public BaseController(@Autowired RestTemplate restTemplate,
                        @Value("${backend.service.root}") String backendServiceRoot,
                        MailService mailService) {
    this.restTemplate = restTemplate;
    this.backendServiceRoot = backendServiceRoot;
    this.mailService = mailService;
    restClient = new RestClient(restTemplate, backendServiceRoot);
    httpHeaders = restClient.getHttpHeaders();
  }

  @GetMapping("/rest/broadcasting/canali/{id}")
  public ResponseEntity getCanaliByBroadcaster(@PathVariable("id") Integer idBroadcaster) {
//    String serviceUri = backendServiceRoot + backendServiceCanali + idBroadcaster;
//    HttpGet httpGet = new HttpGet(serviceUri);
    return new ResponseEntity<>(restClient.get(backendServiceCanali + idBroadcaster), restClient.getResponseHttpHeaders(), restClient.getStatus());
  }

  @PostMapping("/rest/broadcastingS3/fe/utilizationfile/downloadFileArmonizato")
  public ResponseEntity<?> getFileArmonizzato(@RequestBody FileArmonizzatoRequestDTO requestDTO) throws IOException {
    logger.info("DTO "+requestDTO.getBroadcasters().toString());
//    String serviceUri = backendServiceRoot + backendServiceFileArmonizzato;
    return restClient.postFile(backendServiceFileArmonizzato, requestDTO);

//    final CloseableHttpClient client = HttpClients.createDefault();
//    final CloseableHttpResponse response = client.execute(getHttpPost(requestDTO, serviceUri));
//
//    HttpHeaders headers = new HttpHeaders();
//    for (Header header : response.getAllHeaders())
//      if (!header.getName().equalsIgnoreCase(HttpHeaders.CONTENT_LENGTH))
//        headers.add(header.getName(), header.getValue());
//
//    StreamingResponseBody streamingResponseBody = new StreamingResponseBody() {
//      @Override
//      public void writeTo(OutputStream outputStream) throws IOException {
//        try {
//          org.apache.commons.io.IOUtils.copy(response.getEntity().getContent(),outputStream);
//        }catch (Exception e){
//        }finally {
//          response.close();
//          client.close();
//        }
//      }
//    };
//
//    return new ResponseEntity<>(streamingResponseBody, headers,getStatus(response.getStatusLine().getStatusCode()));
  }

  @PostMapping("/rest/broadcastingKPI/fe/monitoraggio/kpi")
  public ResponseEntity getKpiMonitoring(@RequestBody RequestMonitoraggioKPIDTO requestDTO) throws IOException {
    return new ResponseEntity<>(restClient.post(backendServiceMonitoraggioKpi, requestDTO), restClient.getResponseHttpHeaders(), restClient.getStatus());
  }

  @PostMapping("/rest/broadcastingNews/listNews")
  public ResponseEntity getNews(@RequestBody BdcNewsRequestDTO requestDTO) throws IOException {
//    String serviceUri = backendServiceRoot + backendServiceNewsLists;
    return new ResponseEntity<>(restClient.post(backendServiceNewsLists, requestDTO), restClient.getResponseHttpHeaders(), restClient.getStatus());
  }

  @GetMapping("/rest/musicProviders/{id}/news")
  public ResponseEntity getNewsForMusicProvider(BigInteger id) {
    //FIXME implement getNews
//    return new ResponseEntity<>(restClient.post(backendServiceNewsLists, requestDTO), restClient.getResponseHttpHeaders(), restClient.getStatus());
    return ResponseEntity.ok("");
  }

  @GetMapping("/rest/config/s3")
//  @GetMapping("/provaS3")
  public ResponseEntity<S3UploadResponse> getS3Config() {
    s3Config.setCredentials(credentials);

    DateTime dateTime = DateTime.now(DateTimeZone.UTC);
    DateTime dateTimeExpired = dateTime.plusMinutes(5);

    DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyMMdd");
    DateTimeFormatter isoFormat = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    String credential = s3Config.getCredentials().getAccessKey() + "/"+dateTime.toString(fmt)+"/" + s3Config.getRegion() + "/s3/aws4_request";

    String policy = "{" +
              "\"expiration\":\""+dateTimeExpired.toString(isoFormat)+"\"," +
              "\"conditions\":[" +
                          "{\"bucket\":\""+s3Config.getBucket()+"\"}," +
                          "[\"starts-with\",\"$key\",\"\"]," +
                          "{\"acl\":\"public-read\"}," +
                          "[\"starts-with\",\"$Content-Type\",\"\"]," +
                          "{\"x-amz-credential\":\""+credential+"\"}," +
                          "{\"x-amz-algorithm\":\"AWS4-HMAC-SHA256\"}," +
                          "{\"x-amz-date\":\""+dateTime.toString(fmt)+"T000000Z\"}" +
              "]" +
            "}";

    String policyBase64 = Base64Utils.encodeToString(policy.getBytes());

    String signatureKey ="";
    try {
      byte[] signatureByte = getSignatureKey(s3Config.getCredentials().getSecretKey(), dateTime.toString(fmt), s3Config.getRegion(), "s3");

      byte[] signatureKeyByte = HmacSHA256(policyBase64, signatureByte);
      signatureKey = Hex.encodeHexString(signatureKeyByte);
    }catch(Exception e){
      log.error(ExceptionUtils.getStackTrace(e));
      return ResponseEntity.ok(null);
    }
    S3UploadResponse response = new S3UploadResponse();
    response.setAccessKey(s3Config.getCredentials().getAccessKey());
    response.setBucket(s3Config.getBucket());
    response.setKeyPrefix(s3Config.getKeyPrefix());
    response.setPolicyBase64(policyBase64);
    response.setRegion(s3Config.getRegion());
    response.setAmzSignature(signatureKey);
    response.setAmzCredential(credential);
    response.setAmzDate(dateTime.toString(fmt)+"T000000Z");

    return ResponseEntity.ok(response);
  }


  /*
  non sono belli questi metodi privati poi li mettiamo in common casomai
   */
  private static byte[] HmacSHA256(String data, byte[] key) throws Exception {
    String algorithm="HmacSHA256";
    Mac mac = Mac.getInstance(algorithm);
    mac.init(new SecretKeySpec(key, algorithm));
    return mac.doFinal(data.getBytes("UTF8"));
  }

  private static byte[] getSignatureKey(String key, String dateStamp, String regionName, String serviceName) throws Exception {
    byte[] kSecret = ("AWS4" + key).getBytes("UTF8");
    byte[] kDate = HmacSHA256(dateStamp, kSecret);
    byte[] kRegion = HmacSHA256(regionName, kDate);
    byte[] kService = HmacSHA256(serviceName, kRegion);
    byte[] kSigning = HmacSHA256("aws4_request", kService);
    return kSigning;
  }


  @PostMapping("/rest/broadcasting/confirmupload")
  public Object confirmUpload(@RequestBody Map<String, Object> request) {
    Integer id;
    try {
      id = (Integer) request.get("id");
      Assert.notNull(id, "id cannot be null");
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(e.getMessage());
    }
    String serviceUri = backendServiceRoot + backendServiceConfirmUtilizationRecords;
    HttpEntity<IdFileRequestDTO> requestInformationFileEntity = new HttpEntity<>(new IdFileRequestDTO(id), httpHeaders);
    return restTemplate.postForObject(serviceUri, requestInformationFileEntity, Object.class);
  }

  @PostMapping("/rest/broadcasting/cancelupload")
  public Object cancelUpload(@RequestBody Map<String, Object> request) {
    Integer id;
    try {
      id = (Integer) request.get("id");
      Assert.notNull(id, "id cannot be null");
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(e.getMessage());
    }
    String serviceUri = backendServiceRoot + backendServiceCancelUtilizationRecords;
    HttpEntity<IdFileRequestDTO> requestInformationFileEntity = new HttpEntity<>(new IdFileRequestDTO(id), httpHeaders);
    return restTemplate.postForEntity(serviceUri, requestInformationFileEntity, Object.class);
  }




////  private String getSignature(byte[] key) throws Exception{
//    return base16().lowerCase().encode(HmacSHA256(getStringToSign(), key));
//  }

  @PostMapping("/rest/broadcastingS3/fe/utilizationfile/upload")
  public ResponseEntity<ResponseInformationFileInsertDTO> uploadFile(@RequestBody UploadDTO uploadDTO) {
    ResponseEntity<BdcBroadcasters> bdcBroadcaster = getBroadcasterResponseEntity(restTemplate, uploadDTO.getIdBroadcaster());

    List<String> messages = new ArrayList<>();
    ResponseInformationFileInsertDTO response = new ResponseInformationFileInsertDTO();
    ResponseDataInformationFileDTO responseDataInformationFileDTO = new ResponseDataInformationFileDTO();

    int i = 0;

    for (String filename : uploadDTO.getFilenames()) {
      if (isUploadedDataValid(uploadDTO)) {
        return getUploadedDataNotValidResponse(messages, response, responseDataInformationFileDTO);
      }

      final String percorso = String.format("s3://%s/%s/%s", s3Bucket, s3KeyPrefix, uploadDTO.getKeys().get(i));

      final BdcInformationFileEntity bdcInformationFileEntity = new BdcInformationFileEntity();
      bdcInformationFileEntity.setEmittente(bdcBroadcaster.getBody());

      ResponseEntity<BdcUtenti> bdcUtente = getUtenteResponseEntity(uploadDTO, restTemplate);
      setInformationFileEntity(uploadDTO, filename, percorso, bdcInformationFileEntity, bdcUtente);
      updateInformationFile(httpHeaders, bdcInformationFileEntity);
      messages.add(filename + ": upload del file avvenuto con successo");
      i++;
    }

    setEsito(messages, response, responseDataInformationFileDTO, "00", "OK");
    return ResponseEntity.ok(response);
  }

  @PostMapping("/rest/broadcastingS3/fe/utilizationfile/listfilebroadcaster")
  public ResponseEntity getBroadcasterFiles(@RequestBody BdcRequestDataDTO requestDTO) throws IOException {
//    String serviceUri = backendServiceRoot + backendServiceBroadcasterFileList;
    return new ResponseEntity<>(restClient.post(backendServiceBroadcasterFileList, requestDTO), restClient.getResponseHttpHeaders(), restClient.getStatus());
  }

  @GetMapping("/rest/broadcastingS3/fe/utilizationfile/download/{id}")
  public ResponseEntity downloadFile(@PathVariable Integer id) throws IOException {
//    HttpGet httpGet = new HttpGet(backendServiceRoot + backendServiceDownloadFile + id);
    return restClient.getFile(backendServiceDownloadFile + id);
  }

  @GetMapping("/rest/broadcastingS3/fe/utilizationfile/download/armonizzato/{tipoBroadcaster}/{nuovoTracciato}")
  public ResponseEntity downloadHarmonizedFile(@PathVariable String tipoBroadcaster,@PathVariable String nuovoTracciato) throws IOException {
//    String serviceUri = backendServiceRoot + backendServiceDownloadArmonizzato + tipoBroadcaster;
    logger.info("BROADCASTING ... /download/armonizzato/{}/{}",tipoBroadcaster,nuovoTracciato);
    return restClient.getFile(backendServiceDownloadArmonizzato + tipoBroadcaster + "/" + nuovoTracciato);
  }

  @PostMapping(value = "/rest/mail/send", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
  public ResponseEntity sendMailTicket(@RequestParam("file") List<MultipartFile> files,
                                 @RequestParam("oggetto") String oggetto,
                                 @RequestParam("testoMail") String testoMail,
                                 @RequestParam("idUtente") Integer idUtente,
                                 @RequestParam("email") String email,
                                 @RequestParam(value = "idBroadcaster",required = false) Integer idBroadcaster ) throws IOException, MessagingException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, JSONException {

    BdcBroadcasters bdcBroadcaster = getBroadcasterResponseEntity(restTemplate, idBroadcaster).getBody();
    User user = userService.findByEmail(email);

    String text = prepareMailText(testoMail, user);
    String subject = bdcBroadcaster.getNome() + " - "
      + bdcBroadcaster.getTipo_broadcaster() + " - " + oggetto;
    ResponseEntity responseEntity = mailService.sendMailAssistenza(text, subject, to);

    JSONObject response = new JSONObject(responseEntity.getBody().toString());
    if (responseEntity.getStatusCode().is2xxSuccessful() && !response.getBoolean("KO")) {
      return ResponseEntity.ok().build();
    } else {
      logger.error("errore nella chiamata al servizio:\n {}", responseEntity.getBody().toString());
      return ResponseEntity.status(500).build();
    }
  }

//  @PostMapping(value = "/rest/mail/send/exception", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
//  public ResponseEntity sendMailException(@RequestParam("file") List<MultipartFile> files,
//                                 @RequestParam("oggetto") String oggetto,
//                                 @RequestParam("testoMail") String testoMail,
//                                 @RequestParam("idUtente") Integer idUtente,
//                                 @RequestParam("idBroadcaster") Integer idBroadcaster) throws IOException, MessagingException {
//
//    return sendMail(files, oggetto, testoMail, idUtente, idBroadcaster);
//  }

  private HttpStatus getStatus(int statusCode){
    return
      statusCode == HttpStatus.OK.value() ||
        statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value() ?
        HttpStatus.OK : HttpStatus.valueOf(statusCode);
  }

  private void setEsito(List<String> messages, ResponseInformationFileInsertDTO response, ResponseDataInformationFileDTO responseDataInformationFileDTO, String codice, String descrizioneEsito) {
    EsitoDTO esito = new EsitoDTO();
    esito.setCodice(codice);
    esito.setDescrizioneEsito(descrizioneEsito);
    response.setEsito(esito);
    responseDataInformationFileDTO.setOutput(messages.toString());
    response.setResponseData(responseDataInformationFileDTO);
  }

  private void updateInformationFile(HttpHeaders headers, BdcInformationFileEntity bdcInformationFileEntity) {
    String serviceUri;
    serviceUri = backendServiceRoot + backendServiceInformationFile;
    HttpEntity<BdcInformationFileEntity> requestInformationFileEntity =
      new HttpEntity<>(bdcInformationFileEntity, headers);
    restTemplate.postForEntity(serviceUri, requestInformationFileEntity, BdcInformationFileEntity.class);

  }

  private void setInformationFileEntity(@RequestBody UploadDTO uploadDTO, String filename, String percorso, BdcInformationFileEntity bdcInformationFileEntity, ResponseEntity<BdcUtenti> bdcUtente) {
    RestTemplate restTemplate;
    bdcInformationFileEntity.setIdUtente(bdcUtente.getBody());
    if (uploadDTO.getIdCanale() != null) {
      restTemplate = new RestTemplate();
      setCanale(uploadDTO, restTemplate, bdcInformationFileEntity);
    }
    bdcInformationFileEntity.setNomeFile(filename);
    bdcInformationFileEntity.setAnno(uploadDTO.getAnno());
    bdcInformationFileEntity.setMese(uploadDTO.getMese());
    bdcInformationFileEntity.setDataUpload(GregorianCalendar.getInstance(Locale.ITALY).getTime());
    bdcInformationFileEntity.setsDataUpload(bdcInformationFileEntity.getDataUpload().getTime() + "");
    bdcInformationFileEntity.setPercorso(percorso);
    bdcInformationFileEntity.setRepertorio(uploadDTO.getNomerepertorio());
    if (uploadDTO.getIdCanale() == null && uploadDTO.getAnno() == null && uploadDTO.getMese() == null) {
      bdcInformationFileEntity.setTipoUpload("MASSIVO");
    } else {
      bdcInformationFileEntity.setTipoUpload("SINGOLO");
    }
    // se il repertorio è diverso da musica lo stato deve essere ACQUISITO
    if (uploadDTO.getNomerepertorio().equals(com.alkemytech.sophia.broadcasting.enums.Repertorio.MUSICA.toString())) {
      if(bdcInformationFileEntity.getEmittente().getTipo_broadcaster().name().equals("TELEVISIONE") && bdcInformationFileEntity.getEmittente().getNome().contains("RAI")){
        bdcInformationFileEntity.setStato(Stato.DA_CONVERTIRE);
      } else {
        bdcInformationFileEntity.setStato(Stato.DA_ELABORARE);
      }
    } else {
      bdcInformationFileEntity.setStato(Stato.ACQUISITO);
    }
  }

  private void setCanale(@RequestBody UploadDTO uploadDTO, RestTemplate restTemplate, BdcInformationFileEntity bdcInformationFileEntity) {
    String serviceUri;
    serviceUri = backendServiceRoot + backendServiceCanale + uploadDTO.getIdCanale();
    ResponseEntity<BdcCanali> bdcCanali = restTemplate.getForEntity(serviceUri, BdcCanali.class);
    bdcInformationFileEntity.setCanale(bdcCanali.getBody());
  }

  private ResponseEntity<BdcUtenti> getUtenteResponseEntity(@RequestBody UploadDTO uploadDTO, RestTemplate restTemplate) {
    String serviceUri;
    serviceUri = backendServiceRoot + backendServiceUserById + uploadDTO.getIdUtente();
    return restTemplate.getForEntity(serviceUri, BdcUtenti.class);
  }

  private boolean isFormatSupported(String nameFilePart, ResponseEntity<BdcConfigDTO> bdcConfigDto) {
    return !nameFilePart.matches(bdcConfigDto.getBody().getSupportedFormat());
  }

  private ResponseEntity<BdcBroadcasters> getBroadcasterResponseEntity(RestTemplate restTemplate, Integer idBroadcaster) {
    String serviceUri = backendServiceRoot + backendServiceBroadcaster + idBroadcaster;
    return restTemplate.getForEntity(serviceUri, BdcBroadcasters.class);
  }


  private ResponseEntity<BdcInformationFileEntity> getBdcInformationFileEntity(RestTemplate restTemplate, Integer idUtilizationFile) {
    String serviceUri = backendServiceRoot + backendServiceBroadcaster + idUtilizationFile;
    return restTemplate.getForEntity(serviceUri, BdcInformationFileEntity.class);
  }

  private ResponseEntity<BdcConfigDTO> getBdcConfigResponseEntity(HttpHeaders headers, SearchBroadcasterConfigDTO searchBroadcasterConfigDTO) {
    String serviceUri = backendServiceRoot + backendServiceConfigFindActive;
    HttpEntity<SearchBroadcasterConfigDTO> requestEntity = new HttpEntity<>(searchBroadcasterConfigDTO, headers);
    return restTemplate.postForEntity(serviceUri, requestEntity, BdcConfigDTO.class);
  }

  private ResponseEntity<ResponseInformationFileInsertDTO> getUploadedDataNotValidResponse(List<String> messages, ResponseInformationFileInsertDTO response, ResponseDataInformationFileDTO responseDataInformationFileDTO) {
    messages.add("Il format del caricamento SINGOLO prevede l'inserimento dei valori nei campi canale, anno e mese; mentre "
      + "il format del caricamento MASSIVO prevede che i campi canale, anno e mese vengano lasciati vuoti.");
    setEsito(messages, response, responseDataInformationFileDTO, "10", "KO");
    return ResponseEntity.ok(response);
  }

  private boolean isUploadedDataValid(@RequestBody UploadDTO uploadDTO) {
    return !((uploadDTO.getIdCanale() == null && uploadDTO.getAnno() == null && uploadDTO.getMese() == null) ||
      (uploadDTO.getIdCanale() != null && uploadDTO.getAnno() != null && uploadDTO.getMese() != null));
  }

  private static String prepareMailText(String testoMail, User user) {

    return testoMail + "\n\n" +
      "EMAIL UTENTE CHE HA APERTO IL TICKET: " +
      user.getEmail();
  }

  @Autowired
  public void setMailSender(JavaMailSender mailSender) {
    this.mailSender = mailSender;
  }

  @Autowired
  public void setS3Config(S3Config s3Config) {
    this.s3Config = s3Config;
  }

  @Autowired
  public void setCredentials(Credentials credentials) {
    this.credentials = credentials;
  }

  @Autowired
  public void setUserService(UserService userService) {
    this.userService = userService;
  }


  private ResponseEntity<?> getRightResponse(HttpRequestBase httpEntityEnclosingRequestBase) throws IOException {
    CloseableHttpClient client = HttpClients.createDefault();
    httpEntityEnclosingRequestBase.addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
    final CloseableHttpResponse response = client.execute(httpEntityEnclosingRequestBase);

    InputStreamResource inputStreamResource = new InputStreamResource(response.getEntity().getContent());
    HttpHeaders headers = new HttpHeaders();
    for (Header header : response.getAllHeaders())
      if (!header.getName().equalsIgnoreCase(HttpHeaders.CONTENT_LENGTH))
        headers.add(header.getName(), header.getValue());
    return new ResponseEntity(inputStreamResource, headers, HttpStatus.valueOf(response.getStatusLine().getStatusCode()));
  }

  private ResponseEntity sendMail(List<MultipartFile> files, String oggetto, String testoMail, Integer idUtente, String email, Integer idBroadcaster) throws MessagingException, IOException {
    MimeMessage message = mailSender.createMimeMessage();
    MimeMessageHelper helper = new MimeMessageHelper(message, true);

    User user = userService.findByEmail(email);

    if(idBroadcaster != null) {
      ResponseEntity<BdcBroadcasters> bdcBroadcaster = getBroadcasterResponseEntity(restTemplate, idBroadcaster);
      helper.setSubject(bdcBroadcaster.getBody().getNome() + " - "
        + bdcBroadcaster.getBody().getTipo_broadcaster() + " - " + oggetto);
      helper.setText(prepareMailText(testoMail, user));
      helper.setFrom(from, subjecPrefix);
      helper.setTo(to);
    }else{
      helper.setSubject(user.getEmail() + " - " + oggetto);
      helper.setText(prepareMailText(testoMail, user));
      helper.setFrom(from, subjecPrefix);
      helper.setTo(to);

    }





    for (MultipartFile file : files) {
      helper.addAttachment(file.getOriginalFilename(), new ByteArrayResource(IOUtils.toByteArray(file.getInputStream())));
    }

    mailSender.send(message);

    return ResponseEntity.ok().build();
  }

}
