package com.app.controller;

import com.app.entity.PasswordResetToken;
import com.app.entity.User;
import com.app.entity.UserToken;
import com.app.exception.PasswordAlreadyExistsException;
import com.app.exception.PasswordException;
import com.app.exception.TokenExpiredException;
import com.app.exception.TokenNotFoundException;
import com.app.model.ChangePassword;
import com.app.model.ResetPasswordConfig;
import com.app.security.auth.JwtAuthenticationRequest;
import com.app.security.auth.JwtAuthenticationResponse;
import com.app.security.auth.JwtUtil;
import com.app.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static com.app.model.JWTConstants.HEADER;


@RestController
public class AuthController {
  private static final Logger LOG = LoggerFactory.getLogger(AuthController.class);

  private static final String ALTEREGO_LOGIN_URL = "/api/auth/alterego";
  private static final String SIGNUP_URL = "/api/auth/signup";
  private static final String SIGNIN_URL = "/api/auth/signin";
  private static final String REFRESH_TOKEN_URL = "/api/auth/token/refresh";
  private static final String RESET_PASSWORD_URL = "/api/auth/user/password/reset";
  private static final String IS_TOKEN_VALID = "/api/auth/user/password/token/valid";
  private static final String CHANGE_PASSWORD_URL = "/api/auth/user/password/change";
  private static final String RESET_PASSWORD_CONFIG_URL = "/api/auth/user/password/reset/config";

  @Autowired
  private AuthenticationManager authenticationManager;
  private JwtUtil jwtUtil;
  private UserDetailsService userDetailsService;
  private UserService userService;
  private RepertorioService repertorioService;
  private PasswordResetTokenService passwordResetTokenService;
  private MessageByLocaleService messageByLocaleService;
  private MailService mailService;
  private ResetPasswordConfig resetPasswordConfig;
  private UserTokenService userTokenService;

  @Value("${auth.jwt.header}")
  private String tokenHeader;

  @Value("${frontend.baseUrl}")
  private String frontendBaseUrl;

  @Value("${auth.reset.password.expiration}")
  private Long resetPasswordExpiration;

  @Autowired
  public void setUserTokenService(final UserTokenService userTokenService) {
    this.userTokenService = userTokenService;
  }

  @Autowired
  public void setJwtTokenUtil(final JwtUtil jwtUtil) {
    this.jwtUtil = jwtUtil;
  }

  @Autowired
  public void setUserDetailsService(final UserDetailsService userDetailsService) {
    this.userDetailsService = userDetailsService;
  }

  @Autowired
  public void setUserService(final UserService userService) {
    this.userService = userService;
  }

  @Autowired
  public void setRepertorioService(final RepertorioService repertorioService) {
    this.repertorioService = repertorioService;
  }

  @Autowired
  public void setPasswordResetTokenService(final PasswordResetTokenService passwordResetTokenService) {
    this.passwordResetTokenService = passwordResetTokenService;
  }

  @Autowired
  public void setMessageByLocaleService(final MessageByLocaleService messageByLocaleService) {
    this.messageByLocaleService = messageByLocaleService;
  }

  @Autowired
  public void setMailService(final MailService mailService) {
    this.mailService = mailService;
  }

  @Autowired
  public void setResetPasswordConfig(final ResetPasswordConfig resetPasswordConfig) {
    this.resetPasswordConfig = resetPasswordConfig;
  }

  /*@PostMapping(SIGNUP_URL)
  public ResponseEntity createAuthenticationToken(final @RequestBody JwtAuthenticationRequest authenticationRequest) {

    final String username = authenticationRequest.getUsername();
    LOG.info("[POST] CREATING TOKEN FOR User {}", username);

    if (this.userService.findOneByUsername(username) != null) {
      throw new UserAlreadyExistsException(messageByLocaleService);
    }

    final String email = authenticationRequest.getEmail();
    if (this.userService.findByEmail(email) != null) {
      throw new UserAlreadyExistsException(messageByLocaleService);
    }

    final Repertorio defaultRepertorio = authenticationRequest.getDefaultRepertorio();
    if (!repertorioService.existsBy(defaultRepertorio.toString())) {
      throw new RepositoryNotFoundException(messageByLocaleService);
    }

    final List<Repertorio> repertori = authenticationRequest.getRepertori();
    for (final Repertorio repertorio : repertori) {
      if (!repertorioService.existsBy(repertorio.toString())) {
        throw new RepositoryNotFoundException(messageByLocaleService);
      }
    }

    final String password = authenticationRequest.getPassword();
    final Broadcasters broadcasters = authenticationRequest.getBroadcasters();
    final Long idRuolo = authenticationRequest.getIdRuolo();
    userService.save(new User(username, email, password, broadcasters, new Ruoli(idRuolo)), repertori, defaultRepertorio);

    return ResponseEntity.ok().body(messageByLocaleService.getMessage("auth.user.created"));

  }*/

  @PostMapping(SIGNIN_URL)
  public ResponseEntity getAuthenticationToken(final @RequestBody JwtAuthenticationRequest authenticationRequest, final Device device, final HttpServletResponse response) {

    LOG.info("[POST] GETTING TOKEN FOR User {}", authenticationRequest.getUsername());

    try {
      final Authentication authentication = authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(
          authenticationRequest.getUsername(),
          authenticationRequest.getPassword())
      );

      SecurityContextHolder.getContext().setAuthentication(authentication);

      final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
      final String token = jwtUtil.generateToken(userDetails, device);
      response.setHeader(tokenHeader, token);
      // Ritorno il token
      return ResponseEntity.ok(new JwtAuthenticationResponse(userDetails.getUsername()));
    } catch (AuthenticationException ae) {
      throw new com.app.exception.AuthenticationException(messageByLocaleService, ae);
    }
  }

  @GetMapping(REFRESH_TOKEN_URL)
  public ResponseEntity refreshAuthenticationToken(final HttpServletRequest request) {
    final String token = request.getHeader(HEADER);
    LOG.info("[GET] REFRESHING TOKEN");
    final String refreshedToken = jwtUtil.refreshToken(token);
    return ResponseEntity.ok(new JwtAuthenticationResponse(refreshedToken));
  }

  @PostMapping(RESET_PASSWORD_URL)
  public ResponseEntity resetPassword(final @RequestBody JwtAuthenticationRequest userRequest,
                                         final HttpServletRequest request)
    throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException, InterruptedException {

    // verifica se l'utente esiste tramite l'email inviata dal form del recupero password
    final User user = userService.findByEmail(userRequest.getEmail());
    if (user != null) {
      TimeUnit.SECONDS.sleep((new Random((5 - 1)+1)).nextInt() + 1);
      return mailService.sendMailResetPassword(request.getHeader(HttpHeaders.REFERER), user, passwordResetTokenService.generateResetTokenByUserId(user));
    } /*else {
      throw new EmailNotFoundException(messageByLocaleService); //had to remove this for USER ENUMARATION ATTACK
    }*/
    TimeUnit.SECONDS.sleep((new Random((5 - 1)+1)).nextInt() + 1);
    return ResponseEntity.ok().build();
  }

  @GetMapping(IS_TOKEN_VALID + "/{token}")
  @ResponseBody
  public ResponseEntity<PasswordResetToken> isTokenValid(final @PathVariable String token) {
      final PasswordResetToken passwordResetToken = passwordResetTokenService.findByToken(token);

    // il token utilizzato non esiste
    if (passwordResetToken == null) {
      throw new TokenNotFoundException(messageByLocaleService);
    }

    TimeZone timeZone = TimeZone.getTimeZone("UTC");
    Calendar calendar = Calendar.getInstance(timeZone);
    calendar.setTime(new Date(passwordResetToken.getDataCreazione().getTime()+resetPasswordExpiration));

    // il token non è attivo.
    if (!passwordResetToken.getActive() ||
      passwordResetTokenService.isTokenValid(resetPasswordExpiration/1000, passwordResetToken.getId())) {
        passwordResetTokenService.disableTokenByUserId(passwordResetToken.getUtente().getId());
      throw new TokenExpiredException(messageByLocaleService);
    }

    return ResponseEntity.ok(passwordResetToken);
  }

  @PostMapping(CHANGE_PASSWORD_URL)
  @ResponseBody
  public ResponseEntity changePassword(final @RequestBody ChangePassword changePassword) {

    if (!userService.checkNewPasswrd(changePassword.getPassword(), changePassword.getPassword2())) {
      throw new PasswordException(messageByLocaleService);
    }

    if (!userService.changeUserPasswrd(changePassword)) {
      throw new PasswordAlreadyExistsException(messageByLocaleService);
    }

    // disabilito il token
    passwordResetTokenService.disableTokenByUserId(changePassword.getIdUtente());

    return ResponseEntity.ok().build();

  }

  @PostMapping(ALTEREGO_LOGIN_URL)
  @ResponseBody
  public RedirectView redirectAlterEgo(final @RequestParam String token, final Device device, final HttpServletResponse response) {
    final UserToken userToken = userTokenService.findOneByToken(token);

    LOG.info("[POST] GETTING TOKEN FOR User {}", userToken.getUser().getUsername());

    final UserDetails userDetails = userDetailsService.loadUserByUsername(userToken.getUser().getUsername());
    final String newToken = jwtUtil.generateToken(userDetails, device);
    response.setHeader(tokenHeader,newToken);

    final RedirectView redirectView = new RedirectView(frontendBaseUrl + "#/lae/" + newToken);
    redirectView.setContextRelative(false);

    userToken.setDataLogin(GregorianCalendar.getInstance().getTime());

    userTokenService.save(userToken);

    return redirectView;
  }

  @GetMapping(RESET_PASSWORD_CONFIG_URL)
  public ResetPasswordConfig getResetPasswordConfig() {
    return resetPasswordConfig;
  }
}
