package com.app.controller;

import com.alkemytech.sophia.broadcasting.dto.FileArmonizzatoRequestDTO;
import com.alkemytech.sophia.broadcasting.dto.performing.RSRequestMonitoraggioKPIDTO;
import com.app.request.UploadRequest;
import com.app.utility.RestClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URISyntaxException;

@RestController
public class PalinsestoController {
  private String backendServiceRoot;

  private RestTemplate restTemplate;
  private RestClient restClient;
  private HttpHeaders httpHeaders;

  @Value("${backend.service.radio.in.store}")
  private String backendServiceRadioInStore;

  @Value("${backend.service.radio.in.store.music.providers}")
  private String backendServiceMusicProviders;

  @Value("${backend.service.radio.in.store.palinsesti}")
  private String backendServicePalinsesti;

  @Autowired
  public PalinsestoController(@Autowired RestTemplate restTemplate, @Value("${backend.service.root}") String backendServiceRoot) {
    this.restTemplate = restTemplate;
    this.backendServiceRoot = backendServiceRoot;
    restClient = new RestClient(restTemplate, backendServiceRoot);
    httpHeaders = restClient.getHttpHeaders();
  }

  @GetMapping("/rest/performing/radioInStore/musicProviders/{id}/palinsesti")
  public ResponseEntity getPalinsestiByIdMusicProvider(@PathVariable("id") Integer idMusicProvider) {
    return new ResponseEntity<>(restClient.get(backendServiceMusicProviders + idMusicProvider + backendServicePalinsesti), restClient.getResponseHttpHeaders(), restClient.getStatus());
  }

  @PostMapping("/rest/performing/radioInStore/utilizationFiles")
  public ResponseEntity saveUploadedFiles(@RequestBody UploadRequest uploadRequest) {
    return new ResponseEntity<>(restClient.post("/rest/performing/radioInStore/utilizationFiles", uploadRequest), restClient.getResponseHttpHeaders(), restClient.getStatus());
  }

  @GetMapping("/rest/performing/radioInStore/utilizationFiles/{id}")
  public ResponseEntity getUploadedFilesById(@PathVariable("id") Integer id) throws IOException {
    return restClient.getFile("/rest/performing/radioInStore/utilizationFiles/" + id);
  }

  @PostMapping("/rest/performing/radioInStore/armonizzato")
  public ResponseEntity<?> getFileArmonizzato(@RequestBody FileArmonizzatoRequestDTO requestDTO) throws IOException {
    return restClient.postFile("/rest/performing/radioInStore/armonizzato", requestDTO);
  }

  @PostMapping("/rest/performing/radioInStore/kpis")
  public ResponseEntity<?> getKpis(@RequestBody RSRequestMonitoraggioKPIDTO requestDTO) throws IOException {
    return restClient.postFile("/rest/performing/radioInStore/kpis", requestDTO);
  }
}
