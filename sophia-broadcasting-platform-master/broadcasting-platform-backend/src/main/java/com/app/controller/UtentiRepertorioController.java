package com.app.controller;

import com.app.entity.Repertorio;
import com.app.entity.UtentiRepertorio;
import com.app.response.UtentiRepertorioResponse;
import com.app.service.UserService;
import com.app.service.UtentiRepertorioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UtentiRepertorioController {
  private static final String UTENTI = "/rest/utenti";
  private static final String UTENTI_REPERTORIO = "/rest/utentirepertorio";

  private UserService userService;
  private UtentiRepertorioService utentiRepertorioService;


  @Autowired
  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  @GetMapping(value = UTENTI_REPERTORIO, produces = {MediaType.APPLICATION_JSON_VALUE})
  public @ResponseBody
  ResponseEntity<List<UtentiRepertorio>> findAll() {
    List<UtentiRepertorio> results = utentiRepertorioService.findAll();
    return new ResponseEntity<>(results, HttpStatus.OK);
  }

  @GetMapping(UTENTI + "/{id}/repertori")
  public ResponseEntity<List<UtentiRepertorioResponse>> getRepertoriUtente(@PathVariable Long id,
                                                                           @RequestParam boolean isAlterEgo) {
    return new ResponseEntity<>(userService.findRepertoriUtenteById(id,isAlterEgo), HttpStatus.OK);
  }

  @GetMapping(UTENTI + "/{id}/repertori/default")
  public ResponseEntity<Repertorio> getDefaultRepertorio(@PathVariable Long id) {
    return new ResponseEntity<>(userService.findDefaultRepertorioByUserId(id), HttpStatus.CREATED);
  }

  @Autowired
  public void setUtentiRepertorioService(UtentiRepertorioService utentiRepertorioService) {
    this.utentiRepertorioService = utentiRepertorioService;
  }

}
