package com.app.controller;

import com.app.entity.BroadcasterConfig;
import com.app.exception.ConfigurationNotFoundException;
import com.app.service.BroadcasterConfigService;
import com.app.service.MessageByLocaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BroadcasterConfigController {

  private BroadcasterConfigService broadcasterConfigService;
  private final MessageByLocaleService messageByLocaleService;

  @Autowired
  public BroadcasterConfigController(MessageByLocaleService messageByLocaleService) {
    this.messageByLocaleService = messageByLocaleService;
  }

  @Autowired
  public void setBroadcasterConfigService(BroadcasterConfigService broadcasterConfigService) {
    this.broadcasterConfigService = broadcasterConfigService;
  }

  @RequestMapping(
    path = {"/rest/config/formats", "/rest/config/formats/{idBroadcaster}"},
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity getConfigFormats(@PathVariable(required = false) Long idBroadcaster) {
    if (idBroadcaster != null) {
      List<BroadcasterConfig> broadcasterConfigs = broadcasterConfigService.findActiveByIdBroadcaster(idBroadcaster);
      if (broadcasterConfigs != null) {
        return ResponseEntity.ok(broadcasterConfigs);
      } else {
        throw new ConfigurationNotFoundException(messageByLocaleService);
      }
    } else {
      return ResponseEntity.ok(broadcasterConfigService.findActive());
    }
  }
}
