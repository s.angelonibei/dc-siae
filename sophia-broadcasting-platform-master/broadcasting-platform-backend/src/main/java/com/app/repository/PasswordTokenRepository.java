package com.app.repository;

import com.app.entity.PasswordResetToken;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface PasswordTokenRepository extends CrudRepository<PasswordResetToken, Long> {
  PasswordResetToken findByTokenAndActiveTrue(String token);

  @Override
  <S extends PasswordResetToken> S save(S token);

  /*@Query(value = "SELECT * FROM BDC_PASSWORD_RESET_TOKEN PR, ( SELECT max(DATA_CREAZIONE) AS DATA_CREAZIONE FROM BDC_PASSWORD_RESET_TOKEN\n" +
          "WHERE ID_UTENTE=?1) AS LT WHERE PR.DATA_CREAZIONE = LT.DATA_CREAZIONE;", nativeQuery = true)*/
  PasswordResetToken findTopByUtenteIdAndActiveTrue(Long id);

  @Query(value = "select case when(DATA_CREAZIONE + INTERVAL ?1 SECOND) < CURRENT_TIMESTAMP then true else false end from BDC_PASSWORD_RESET_TOKEN where ID = ?2", nativeQuery = true)
  BigInteger isTokenValid(Long seconds, Long id);
}
