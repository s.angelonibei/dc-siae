package com.app.repository;

import com.app.entity.UtentiRepertorio;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UtentiRepertorioRepository extends CrudRepository<UtentiRepertorio,Long> {
    @Override
    List<UtentiRepertorio> findAll();

//    @Query("SELECT ur FROM UtentiRepertorio ur where ur.utente.id")
//    List<UtentiRepertorio> findAllByUtenteIdAndUtenteRuoloRuolo(Long id, String ruolo);

    @Override
    boolean exists(Long aLong);

    UtentiRepertorio findByIdUtenteRepertorio(Long id);
    UtentiRepertorio findByUtenteIdAndDefaultRepertorioTrue(Long id);

}
