package com.app.repository;

import com.app.entity.OldPassword;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OldPasswrdRepository extends CrudRepository<OldPassword,Long> {

    List<OldPassword> findTop5ByUtenteIdOrderByDataCreazioneDesc(Long id);

    @Override
    long count();
}
