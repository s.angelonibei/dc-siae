package com.app.repository;

import com.app.entity.PerfPalinsesto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface PalinsestoRepository extends CrudRepository<PerfPalinsesto, BigInteger> {}
