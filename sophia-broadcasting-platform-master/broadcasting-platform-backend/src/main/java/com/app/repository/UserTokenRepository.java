package com.app.repository;

import com.app.entity.UserToken;
import com.app.entity.UserTokenPK;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface UserTokenRepository extends CrudRepository<UserToken, UserTokenPK> {
    UserToken findOneByIdToken(String token);
    UserToken findTopByUserIdAndDataFineValiditaAfter(Long id, Date data);
}
