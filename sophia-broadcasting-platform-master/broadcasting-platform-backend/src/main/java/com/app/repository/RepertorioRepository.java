package com.app.repository;

import com.app.entity.Repertorio;
import com.app.entity.User;
import com.app.response.UtentiRepertorioResponse;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepertorioRepository extends CrudRepository<Repertorio, Long> {
    boolean existsByNome(String repertorio);

    List<UtentiRepertorioResponse> getRepertoriosByNomeIn(String... repertori);
    List<UtentiRepertorioResponse> getRepertoryByUtente(User utente);
}
