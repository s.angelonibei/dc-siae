package com.app.repository;

import com.app.entity.PerfRsUtilizationFile;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface RsUtitlizationFileRepository extends CrudRepository<PerfRsUtilizationFile, BigInteger> {}
