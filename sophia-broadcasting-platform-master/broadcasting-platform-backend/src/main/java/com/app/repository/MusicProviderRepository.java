package com.app.repository;

import com.app.entity.PerfMusicProvider;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface MusicProviderRepository extends CrudRepository<PerfMusicProvider, BigInteger> {}
