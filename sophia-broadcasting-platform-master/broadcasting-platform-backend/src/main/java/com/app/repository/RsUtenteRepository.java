package com.app.repository;

import com.app.entity.PerfRsUtente;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface RsUtenteRepository extends CrudRepository<PerfRsUtente, BigInteger> {}
