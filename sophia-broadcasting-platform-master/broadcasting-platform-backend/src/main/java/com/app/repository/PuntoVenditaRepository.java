package com.app.repository;

import com.app.entity.PerfPuntoVendita;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface PuntoVenditaRepository extends CrudRepository<PerfPuntoVendita, BigInteger> {}
