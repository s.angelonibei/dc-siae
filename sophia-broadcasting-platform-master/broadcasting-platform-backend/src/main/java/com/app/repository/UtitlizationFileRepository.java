package com.app.repository;

import com.app.entity.UtilizationFile;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UtitlizationFileRepository extends CrudRepository<UtilizationFile, Long> {}
