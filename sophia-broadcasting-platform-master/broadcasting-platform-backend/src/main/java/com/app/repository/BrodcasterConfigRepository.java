package com.app.repository;

import com.app.entity.BroadcasterConfig;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BrodcasterConfigRepository extends CrudRepository<BroadcasterConfig, Long> {
    
    List<BroadcasterConfig> findActive();
    List<BroadcasterConfig> findActiveByIdBroadcaster(Long idBroadcaster);
}
