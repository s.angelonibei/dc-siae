package com.app.repository;

import com.app.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    User findOneByEmail(String email);
    User findOneByUsername(String username);
    User findOneByIdAndRuolo_Ruolo(Long id, String ruolo);
}
