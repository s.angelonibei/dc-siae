package com.app.repository;

import com.app.entity.PerfNews;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface PerfNewsRepository extends CrudRepository<PerfNews, BigInteger> {}
