package com.app.repository;

import com.app.entity.PerfRsUtente;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PerfRsUtenteRepository extends CrudRepository<PerfRsUtente, Long> {
}
