package com.app.repository;

import com.app.entity.BdcUtenti;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BdcUtentiRepository extends CrudRepository<BdcUtenti, Long> {
}
