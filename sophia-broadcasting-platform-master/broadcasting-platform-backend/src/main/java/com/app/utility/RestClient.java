package com.app.utility;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.Map;

public class RestClient {
  private RestTemplate restTemplate;
  private CloseableHttpClient closeableHttpClient;

  public HttpHeaders getHttpHeaders() {
    return httpHeaders;
  }

  public void setHttpHeaders(HttpHeaders httpHeaders) {
    this.httpHeaders = httpHeaders;
  }

  private HttpHeaders httpHeaders;
  private String server;
  private HttpStatus status;
  private HttpHeaders responseHttpHeaders;

  public RestClient(RestTemplate restTemplate, String server) {
    this.server = server;
    this.restTemplate = restTemplate;
    this.httpHeaders = new HttpHeaders();
    httpHeaders.add("Content-Type", "application/json");
    httpHeaders.add("Accept", "*/*");
    RequestConfig config = RequestConfig.custom()
      .setConnectTimeout(-1)
      .setConnectionRequestTimeout(-1)
      .setSocketTimeout(-1).build();
    closeableHttpClient =
      HttpClientBuilder.create().setDefaultRequestConfig(config).build();

  }

  public Object get(String uri) {
    HttpEntity<String> requestEntity = new HttpEntity<>("", httpHeaders);
    ResponseEntity<?> responseEntity = restTemplate.exchange(server + uri, HttpMethod.GET, requestEntity, Object.class);
    this.setStatus(responseEntity.getStatusCode());
    this.setResponseHttpHeaders(responseEntity.getHeaders());
    return responseEntity.getBody();
  }

  public ResponseEntity<InputStreamResource> getFile(String uri) throws IOException {
    /*RestTemplate restTemplate = this.restTemplate;
    restTemplate.getMessageConverters().add(new ByteArrayHttpMessageConverter());
    HttpEntity<String> requestEntity = new HttpEntity<>("", httpHeaders);
    ResponseEntity<Object> responseEntity = restTemplate.exchange(server + uri, HttpMethod.GET, requestEntity, Object.class);
    this.setStatus(responseEntity.getStatusCode());
    this.setResponseHttpHeaders(responseEntity.getHeaders());
    return responseEntity.getBody();*/
    HttpGet httpGet = new HttpGet(server + uri);

    httpGet.addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
    final CloseableHttpResponse response = closeableHttpClient.execute(httpGet);
    InputStreamResource inputStreamResource = new InputStreamResource(response.getEntity().getContent());
    HttpHeaders headers = new HttpHeaders();
    for (Header header : response.getAllHeaders())
//      if (!header.getName().equalsIgnoreCase(HttpHeaders.CONTENT_LENGTH))
        headers.add(header.getName(), header.getValue());
//      this.setResponseHttpHeaders(headers);
//      this.setStatus(HttpStatus.valueOf(response.getStatusLine().getStatusCode()));
    return new ResponseEntity<>(inputStreamResource, headers, HttpStatus.valueOf(response.getStatusLine().getStatusCode()));

  }

  public ResponseEntity<?> getFile(String uri, Object request) throws IOException, URISyntaxException {
    /*RestTemplate restTemplate = this.restTemplate;
    restTemplate.getMessageConverters().add(new ByteArrayHttpMessageConverter());
    HttpEntity<?> requestEntity = new HttpEntity<>(request, httpHeaders);
    ResponseEntity<Object> responseEntity = restTemplate.exchange(server + uri, HttpMethod.GET, requestEntity, Object.class);
    this.setStatus(responseEntity.getStatusCode());
    this.setResponseHttpHeaders(responseEntity.getHeaders());
    return responseEntity.getBody();*/

    final CloseableHttpResponse response = closeableHttpClient.execute(getHttpGet(request, server + uri));
    return getResponseEntity(response);
  }

  private ResponseEntity<?> getResponseEntity(CloseableHttpResponse response) throws IOException {
    InputStreamResource inputStreamResource = null;
    String altResponse = "";
    HttpHeaders headers = new HttpHeaders();
    if (response.getFirstHeader(HttpHeaders.CONTENT_TYPE).getValue().equalsIgnoreCase("text/csv") && HttpStatus.INTERNAL_SERVER_ERROR.equals(HttpStatus.valueOf(response.getStatusLine().getStatusCode()))) {
      inputStreamResource = new InputStreamResource(response.getEntity().getContent());
      altResponse = EntityUtils.toString(response.getEntity());
      headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
    } else {
      for (Header header : response.getAllHeaders())
        if (!header.getName().equalsIgnoreCase(HttpHeaders.CONTENT_LENGTH))
          headers.add(header.getName(), header.getValue());
      if(response.getEntity()!= null){
        altResponse = EntityUtils.toString(response.getEntity());
      }
    }

    return new ResponseEntity<>(
      StringUtils.isNotEmpty(altResponse) ?
        altResponse :
        inputStreamResource, headers, getStatus(response.getStatusLine().getStatusCode()));
  }

  public ResponseEntity<?> postFile(String uri, Object request) throws IOException {
    final CloseableHttpResponse response = closeableHttpClient.execute(getHttpPost(request, server + uri));
    return getResponseEntity(response);
  }

  public Object post(String uri, Object request) {
    HttpEntity<?> requestEntity = new HttpEntity<>(request, httpHeaders);
    ResponseEntity<?> responseEntity = restTemplate.exchange(server + uri, HttpMethod.POST, requestEntity, Object.class);
    this.setStatus(responseEntity.getStatusCode());
    this.setResponseHttpHeaders(responseEntity.getHeaders());
    return responseEntity.getBody();
  }

  public void put(String uri, Object request) throws JsonProcessingException {
    HttpEntity<?> requestEntity = new HttpEntity<>(request, httpHeaders);
    ResponseEntity<?> responseEntity = restTemplate.exchange(server + uri, HttpMethod.PUT, requestEntity, Object.class);
    this.setResponseHttpHeaders(responseEntity.getHeaders());
    this.setStatus(responseEntity.getStatusCode());
  }

  public void delete(String uri, Object request) throws JsonProcessingException {
    ObjectMapper mapper = new ObjectMapper();
    HttpEntity<?> requestEntity = new HttpEntity<>(request, httpHeaders);
    ResponseEntity<?> responseEntity = restTemplate.exchange(server + uri, HttpMethod.DELETE, requestEntity, Object.class);
    this.setResponseHttpHeaders(responseEntity.getHeaders());
    this.setStatus(responseEntity.getStatusCode());
  }

  public HttpStatus getStatus() {
    return status;
  }

  private HttpStatus getStatus(int statusCode) {
    return
      statusCode == HttpStatus.OK.value() ||
        statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value() ?
        HttpStatus.OK : HttpStatus.valueOf(statusCode);
  }

  public void setStatus(HttpStatus status) {
    this.status = status;
  }

  public HttpHeaders getResponseHttpHeaders() {
    return responseHttpHeaders;
  }

  public void setResponseHttpHeaders(HttpHeaders responseHttpHeaders) {
//    this.responseHttpHeaders = responseHttpHeaders;
    HttpHeaders headers = new HttpHeaders();
    headers.add(HttpHeaders.CONTENT_TYPE, responseHttpHeaders.getFirst(HttpHeaders.CONTENT_TYPE));
  }

  private HttpPost getHttpPost(@RequestBody Object requestDTO, String serviceUri) throws UnsupportedEncodingException, JsonProcessingException {
    ObjectMapper mapper = new ObjectMapper();

    HttpPost request = new HttpPost(serviceUri);
    request.addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
    request.setEntity(new StringEntity(mapper.writeValueAsString(requestDTO)));
    return request;
  }

  private HttpGet getHttpGet(@RequestBody Object requestDTO, String serviceUri) throws UnsupportedEncodingException, JsonProcessingException, URISyntaxException {
    ObjectMapper mapper = new ObjectMapper();

//    URIBuilder builder = new URIBuilder(serviceUri + mapper.writeValueAsString(requestDTO));

    HttpGet request = new HttpGet(serviceUri + mapper.writeValueAsString(requestDTO));
    request.addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
    return request;
  }
}
