package com.app.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class UploadRequest implements Serializable {
  private Short anno;
  private Short mese;
  private BigInteger idUtente;
  private BigInteger idBroadcaster;
  private BigInteger idCanale;
  private BigInteger idMusicProvider;
  private BigInteger idPalinsesto;
  private List<String> filenames;
  private List<String> keys;
}
