package com.app.service;

import com.app.entity.UserToken;
import org.springframework.stereotype.Service;

@Service
public interface UserTokenService {
  UserToken findOneByToken(String token);
  <S extends UserToken> void save(S entity);
}
