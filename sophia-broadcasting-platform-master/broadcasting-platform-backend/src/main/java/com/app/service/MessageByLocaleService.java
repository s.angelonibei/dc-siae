package com.app.service;

import org.springframework.stereotype.Service;

@Service
public interface MessageByLocaleService {
  String getMessage(String id);
  String getMessage(String id, Object... args);
}
