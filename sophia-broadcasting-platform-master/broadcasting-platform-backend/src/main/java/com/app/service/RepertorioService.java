package com.app.service;

import org.springframework.stereotype.Service;

@Service
public interface RepertorioService {
  Boolean existsBy(String repertorio);
}
