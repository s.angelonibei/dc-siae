package com.app.service;

import com.app.repository.RepertorioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RepertorioServiceImpl implements RepertorioService {
    private RepertorioRepository repertorioRepository;

    @Autowired
    public void setRepertorioRepository(RepertorioRepository repertorioRepository) {
        this.repertorioRepository = repertorioRepository;
    }

    @Override
    public Boolean existsBy(String repertorio) {
        return repertorioRepository.existsByNome(repertorio);
    }

}
