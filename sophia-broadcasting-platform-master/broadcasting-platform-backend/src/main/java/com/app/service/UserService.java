package com.app.service;

import com.app.entity.Repertorio;
import com.app.entity.User;
import com.app.model.ChangePassword;
import com.app.response.UtentiRepertorioResponse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {
  void save(User user, List<Repertorio> repertori, Repertorio defaultRepertorio);
  void delete(Long id);
  Iterable<User> findAll();
  User findById(Integer id);
  User findByEmail(String email);
  User findOneByUsername(String name);
  List<UtentiRepertorioResponse> findRepertoriUtenteById(Long id, boolean isAlterEgo);
  Repertorio findDefaultRepertorioByUserId(Long id);
  Boolean checkNewPasswrd(String passwrd, String retypepasswrd );
  Boolean changeUserPasswrd(ChangePassword changePassword);
}
