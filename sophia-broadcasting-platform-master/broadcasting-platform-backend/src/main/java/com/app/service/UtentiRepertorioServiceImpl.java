package com.app.service;

import com.app.entity.UtentiRepertorio;
import com.app.repository.UtentiRepertorioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UtentiRepertorioServiceImpl implements UtentiRepertorioService {

  private UtentiRepertorioRepository utentiRepertorioRepository;

  @Autowired
  public void setUtentiRepertorioRepository(UtentiRepertorioRepository utentiRepertorioRepository) {
    this.utentiRepertorioRepository = utentiRepertorioRepository;
  }

  @Override
  public List<UtentiRepertorio> findAll() {
    return utentiRepertorioRepository.findAll();
  }

  @Override
  public UtentiRepertorio save(UtentiRepertorio entity) {
    return utentiRepertorioRepository.save(entity);
  }

  @Override
  public UtentiRepertorio findByIdUtenteRepertorio(Long id) {
    UtentiRepertorio result = utentiRepertorioRepository.findByIdUtenteRepertorio(id);
    if (result == null) {
      result = new UtentiRepertorio();
    }
    return result;
  }

  @Override
  public boolean exists(Long aLong) {
    return utentiRepertorioRepository.exists(aLong);
  }
}
