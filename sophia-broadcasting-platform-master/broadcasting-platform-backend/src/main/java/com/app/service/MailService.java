package com.app.service;

import com.app.entity.User;
import org.springframework.http.ResponseEntity;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

public interface MailService {
    ResponseEntity sendMailResetPassword(String origin, User user, String token) throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException;
    ResponseEntity sendMailAssistenza(String text, String subject, String to) throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException;
}
