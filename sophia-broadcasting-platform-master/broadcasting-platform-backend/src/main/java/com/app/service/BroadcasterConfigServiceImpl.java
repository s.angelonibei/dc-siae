package com.app.service;

import com.app.entity.BroadcasterConfig;
import com.app.repository.BrodcasterConfigRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BroadcasterConfigServiceImpl implements BroadcasterConfigService {
    private BrodcasterConfigRepository brodcasterConfigRepository;

    @Autowired
    public void setBrodcasterConfigRepository(BrodcasterConfigRepository brodcasterConfigRepository) {
        this.brodcasterConfigRepository = brodcasterConfigRepository;
    }


    @Override
    public List<BroadcasterConfig> findActive() {
        return brodcasterConfigRepository.findActive();
    }

    @Override
    public List<BroadcasterConfig> findActiveByIdBroadcaster(Long idBroadcaster) {
        return brodcasterConfigRepository.findActiveByIdBroadcaster(idBroadcaster);
    }
}
