package com.app.service;

import com.app.entity.User;
import com.app.exception.MuleServiceException;
import com.app.model.MuleConfig;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.TrustStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MailServiceImpl implements MailService {
  private final Logger logger = LoggerFactory.getLogger(this.getClass());
  @Value("${broadcasting.mail.resetpassword.from}")
  private String from;

  @Value("${frontend.change.password}")
  public String frontendChangePasswordUrl = "#/login/password/change/";

  private MessageByLocaleService messageByLocaleService;
  private MuleConfig muleConfig;

  @Autowired
  public void setMessageByLocaleService(MessageByLocaleService messageByLocaleService) {
    this.messageByLocaleService = messageByLocaleService;
  }

  @Override
  public ResponseEntity<?> sendMailResetPassword(String origin, User user, String token) throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

    MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
    map.add("codiceAbbonamento", "12345678");
    map.add("isHtml", "0");
    map.add("mailSender", from);
    map.add("messageBody", prepareMailText(origin, token));
    map.add("subject",
      messageByLocaleService.getMessage("mail.password.reset.subject.prefix")
        + " - " +
      messageByLocaleService.getMessage("sophia.broadcasting.title"));
    List<String> emailList = new ArrayList<>();
    Map<String, String> emailMap = new HashMap<>();
    emailMap.put("email", user.getEmail());
    emailList.add(new JSONObject(emailMap).toString());
    map.add("toList", emailList.toString());

    HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

    if (logger.isInfoEnabled()) {
      logger.info("mule_host {}", muleConfig.getHost());
      logger.info("mule_service {}", muleConfig.getMailService());
      logger.info("parametri {}", map.toSingleValueMap());
    }
    ResponseEntity<String> response =
      getRestTemplate().postForEntity(
        muleConfig.getHost() + muleConfig.getMailService(), request, String.class);
    if (logger.isInfoEnabled()) {
      logger.info("response {}", response);
    }

    if(response.getBody().isEmpty() || response.getBody().contains("\"KO\":true"))
      throw new MuleServiceException(messageByLocaleService);

    return response;
  }

  @Override
  public ResponseEntity sendMailAssistenza(String text, String subject, String to) throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

    MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
    map.add("codiceAbbonamento", "12345678");
    map.add("isHtml", "0");
    map.add("mailSender", from);
    map.add("messageBody", text);
    map.add("subject", subject);
    List<String> emailList = new ArrayList<>();
    Map<String, String> emailMap = new HashMap<>();
    emailMap.put("email", to);
    emailList.add(new JSONObject(emailMap).toString());
    map.add("toList", emailList.toString());

    HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

    if (logger.isInfoEnabled()) {
      logger.info("mule_host {}", muleConfig.getHost());
      logger.info("mule_service {}", muleConfig.getMailService());
      logger.info("parametri {}", map.toSingleValueMap());
    }
    ResponseEntity<String> response =
      getRestTemplate().postForEntity(
        muleConfig.getHost() + muleConfig.getMailService(), request, String.class);
    if (logger.isInfoEnabled()) {
      logger.info("response {}", response);
    }

    if(response.getBody().isEmpty() || response.getBody().contains("\"KO\":true"))
      throw new MuleServiceException(messageByLocaleService);

    return response;
  }

  private String prepareMailText(String origin, String token) {
    return "\n\n" +
      messageByLocaleService.getMessage("mail.password.reset.message") +
      "\n\n" +
      origin + frontendChangePasswordUrl + token;
  }

  @Autowired
  public void setMuleConfig(MuleConfig muleConfig) {
    this.muleConfig = muleConfig;
  }

  private RestTemplate getRestTemplate() throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
    TrustStrategy acceptingTrustStrategy = new TrustStrategy() {
      @Override
      public boolean isTrusted(X509Certificate[] x509Certificates, String s) {
        return true;
      }
    };
    SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
    SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext, new NoopHostnameVerifier());
    CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(csf).build();
    HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
    requestFactory.setHttpClient(httpClient);
    return new RestTemplate(requestFactory);
  }
}
