package com.app.service;

import com.app.entity.UserToken;
import com.app.repository.UserTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserTokenServiceImpl implements UserTokenService {
  private UserTokenRepository userTokenRepository;

  @Autowired
  public void setUserTokenRepository(UserTokenRepository userTokenRepository) {
    this.userTokenRepository = userTokenRepository;
  }

  @Override
  public UserToken findOneByToken(String token) {
    return this.userTokenRepository.findOneByIdToken(token);
  }

  @Override
  public <S extends UserToken> void save(S entity) {
    this.userTokenRepository.save(entity);
  }
}
