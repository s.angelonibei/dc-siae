package com.app.service;

import com.app.entity.UtentiRepertorio;

import java.util.List;

public interface UtentiRepertorioService {
    List<UtentiRepertorio> findAll();
    UtentiRepertorio save(UtentiRepertorio entity);
    UtentiRepertorio findByIdUtenteRepertorio(Long id);
    boolean exists(Long aLong);
}
