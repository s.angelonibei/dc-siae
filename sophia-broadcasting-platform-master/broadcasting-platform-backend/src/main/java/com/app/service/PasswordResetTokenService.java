package com.app.service;

import com.app.entity.PasswordResetToken;
import com.app.entity.User;
import org.springframework.stereotype.Service;

@Service
public interface PasswordResetTokenService {
    PasswordResetToken findByToken(String token);
    String generateResetTokenByUserId(User user);
    boolean disableTokenByUserId(Long id);
    boolean isTokenValid(Long seconds, Long id);
}
