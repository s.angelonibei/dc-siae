package com.app.service;

import com.app.entity.*;
import com.app.exception.UserNotFoundException;
import com.app.model.ChangePassword;
import com.app.repository.*;
import com.app.response.UtentiRepertorioResponse;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.GregorianCalendar;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

  @Value("${auth.reset.password.regex}")
  private String passwordRegex;

  @Value("${auth.reset.password.length}")
  private Integer passwrdLength;

  private UserRepository userRepository;
  private UtentiRepertorioRepository utentiRepertorioRepository;
  private BdcUtentiRepository bdcUtentiRepository;
  private PerfRsUtenteRepository perfRsUtenteRepository;
  private RepertorioRepository repertorioRepository;
  private OldPasswrdRepository oldPasswrdRepository;
  private MessageByLocaleService messageByLocaleService;
  private UserTokenRepository userTokenRepository;

  @Autowired
  public void setBdcUtentiRepository(BdcUtentiRepository bdcUtentiRepository) {
    this.bdcUtentiRepository = bdcUtentiRepository;
  }

  @Autowired
  public void setPerfRsUtente(PerfRsUtenteRepository perfRsUtenteRepository) {
    this.perfRsUtenteRepository = perfRsUtenteRepository;
  }

  @Autowired
  public void setUserTokenRepository(UserTokenRepository userTokenRepository) {
    this.userTokenRepository = userTokenRepository;
  }

  @Autowired
  public void setRepertorioRepository(RepertorioRepository repertorioRepository) {
    this.repertorioRepository = repertorioRepository;
  }

  @Autowired
  public void setUserRepository(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Autowired
  public void setUtentiRepertorioRepository(UtentiRepertorioRepository utentiRepertorioRepository) {
    this.utentiRepertorioRepository = utentiRepertorioRepository;
  }

  @Autowired
  public void setOldPasswrdRepository(OldPasswrdRepository oldPasswrdRepository) {
    this.oldPasswrdRepository = oldPasswrdRepository;
  }

  @Autowired
  public void setMessageByLocaleService(MessageByLocaleService messageByLocaleService) {
    this.messageByLocaleService = messageByLocaleService;
  }

  @Bean
  private PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Override
  public Iterable<User> findAll() {
    return userRepository.findAll();
  }

  @Override
  public User findById(Integer id) {
    if (userRepository.exists(id.longValue())) {
      return userRepository.findOne(id.longValue());
    } else {
      throw new UserNotFoundException(messageByLocaleService);
    }
  }

  @Override
  public User findByEmail(String email) {
    return userRepository.findOneByEmail(email);
  }

  @Override
  public User findOneByUsername(String username) {
    return userRepository.findOneByUsername(username);
  }

  @Override
  public List<UtentiRepertorioResponse> findRepertoriUtenteById(Long id, boolean isAlterEgo) {
    final User user = userRepository.findOneByIdAndRuolo_Ruolo(id, "BROADCASTER");
    if (isAlterEgo) {
      UserToken ut = userTokenRepository.findTopByUserIdAndDataFineValiditaAfter(id, GregorianCalendar.getInstance().getTime());
        return repertorioRepository.getRepertoriosByNomeIn(ut.getRepertori().split(","));
    }
    return repertorioRepository.getRepertoryByUtente(user);
  }

  @Override
  public Repertorio findDefaultRepertorioByUserId(Long id) {
    return utentiRepertorioRepository.findByUtenteIdAndDefaultRepertorioTrue(id).getRepertorio();
  }


  @Override
  public void save(User user, List<Repertorio> repertori, Repertorio defaultRepertorio) {


    user.setPassword(passwordEncoder().encode(user.getPassword()));
    user = userRepository.save(user);

    for (Repertorio repertorio : repertori) {
      Repertorio rep = new Repertorio(repertorio.toString());

      UtentiRepertorio utentiRepertorio = new UtentiRepertorio();
      utentiRepertorio.setRepertorio(rep);
      utentiRepertorio.setUtente(user);
      utentiRepertorio.setDefaultRepertorio(repertorio.equals(defaultRepertorio));
      utentiRepertorioRepository.save(utentiRepertorio);
    }
  }

  @Override
  public void delete(Long id) {
    if (userRepository.exists(id)) {
      userRepository.delete(id);
    } else {
      throw new UserNotFoundException(messageByLocaleService);
    }
  }


  @Override
  public Boolean checkNewPasswrd(String passwrd, String retypepasswrd) {

    Integer lenPasswrd = passwrd.length();

    if (!StringUtils.equalsAny(passwrd, retypepasswrd)) {
      return false;
    }

    if (lenPasswrd < passwrdLength) {
      return false;
    }

    return passwrd.matches(passwordRegex);
  }

  @Override
  @Transactional
  public Boolean changeUserPasswrd(ChangePassword changePassword) {

    List<OldPassword> oldpasswrdList = oldPasswrdRepository.findTop5ByUtenteIdOrderByDataCreazioneDesc(changePassword.getIdUtente());
    if (checkOldPasswrd(oldpasswrdList, changePassword.getPassword())) {
      return false;
    }
    User user = userRepository.findOneByIdAndRuolo_Ruolo(changePassword.getIdUtente(), changePassword.getRuolo());
    OldPassword storeOldpasswrd = new OldPassword();
    storeOldpasswrd.setOldPassword(user.getPassword());
    user.setPassword(passwordEncoder().encode(changePassword.getPassword()));
    storeOldpasswrd.setUtente(user);
    if(user.getMusicProvider() == null) {
      BdcUtenti bdcUtenti = new BdcUtenti();
      BeanUtils.copyProperties(user, bdcUtenti);
      bdcUtentiRepository.save(bdcUtenti);
    } else {
      PerfRsUtente perfRsUtente = new PerfRsUtente();
      BeanUtils.copyProperties(user, perfRsUtente);
      perfRsUtenteRepository.save(perfRsUtente);
    }

    oldPasswrdRepository.save(storeOldpasswrd);

    return true;
  }

  /*
      Il metodo verifica se una delle vecchie password coincide con la nuova paswrd che si vole inserire
      return true se la password è stata già utilizzata altrimenti false
   */
  private Boolean checkOldPasswrd(List<OldPassword> oldpwd, String newPasswrd) {
    boolean oldPasswrdMatch = false;
    if (CollectionUtils.isNotEmpty(oldpwd)) {
      for (OldPassword currentoldpwd : oldpwd) {
        if (passwordEncoder().matches(newPasswrd, currentoldpwd.getOldPassword())) {
          oldPasswrdMatch = true;
          break;
        }
      }
    }
    return oldPasswrdMatch;
  }
}
