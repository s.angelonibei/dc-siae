package com.app.service;

import com.app.entity.BroadcasterConfig;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface BroadcasterConfigService {
    List<BroadcasterConfig> findActive();
    List<BroadcasterConfig> findActiveByIdBroadcaster(Long idBroadcaster);
}
