package com.app.service;

import com.app.entity.PasswordResetToken;
import com.app.entity.User;
import com.app.exception.TokenExpiredException;
import com.app.repository.PasswordTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
public class PasswordResetTokenServiceImpl implements PasswordResetTokenService {
    private PasswordTokenRepository passwordTokenRepository;
    private MessageByLocaleService messageByLocaleService;

    @Autowired
    public void setPasswordTokenRepository(PasswordTokenRepository passwordTokenRepository) {
        this.passwordTokenRepository = passwordTokenRepository;
    }

    public PasswordResetToken findByToken(String token) {
        return  passwordTokenRepository.findByTokenAndActiveTrue(token);
    }

    @Transactional
    @Override
    public String generateResetTokenByUserId(User user) {
        PasswordResetToken passwordResetToken = passwordTokenRepository.findTopByUtenteIdAndActiveTrue(user.getId());

        if ( passwordResetToken != null ){
            passwordResetToken.setActive(false);
            passwordTokenRepository.save(passwordResetToken);
        }

        String token = UUID.randomUUID().toString();
        PasswordResetToken myToken = new PasswordResetToken(token, user);
        myToken.setActive(true);
        passwordTokenRepository.save(myToken);
        return token;
    }

    /**
        Il metodo restituisce TRUE  se e solo se la differenza tra la data corrente e la data di creazione del
        token è inferiore al valore resetPasswordExpiration. FALSE altrimenti.
     */

    @Override
    public boolean disableTokenByUserId(Long id) {
        PasswordResetToken tokenToDsable = passwordTokenRepository.findTopByUtenteIdAndActiveTrue(id);
        if(tokenToDsable==null){
          throw new TokenExpiredException(messageByLocaleService);
        }
        tokenToDsable.setActive(false);
        passwordTokenRepository.save(tokenToDsable);
        return true;
    }

  @Override
  public boolean isTokenValid(Long seconds, Long id) {
    return passwordTokenRepository.isTokenValid(seconds, id).intValue() == 1;
  }

  @Autowired
  public void setMessageByLocaleService(MessageByLocaleService messageByLocaleService) {
    this.messageByLocaleService = messageByLocaleService;
  }
}
