#!/bin/sh

ENVNAME=dev
APPNAME=sqs-ipi-ulisse
VERSION=1.0.1

JARNAME=sophia-service-bus-$VERSION-jar-with-dependencies.jar
CFGNAME=sophia-service-bus.properties
HOME=/var/local/sophia/$ENVNAME
#HOME=/home/orchestrator/sophia/servicebus
#OUTPUT=/dev/null
OUTPUT=$HOME/logs/$APPNAME.out

echo "$(date +%Y%m%d%H%M%S) $ENVNAME $APPNAME" >> $OUTPUT

nohup java -cp $HOME/$JARNAME com.alkemytech.sophia.servicebus.tools.SqsIpiUlisse $HOME/$CFGNAME 2>&1 >> $OUTPUT &

echo "$(date +%Y%m%d%H%M%S) $ENVNAME $APPNAME" >> $HOME/logs/crontab.log
