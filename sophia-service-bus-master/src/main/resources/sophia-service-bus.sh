#!/bin/sh

ENVNAME=dev
APPNAME=sophia-service-bus
VERSION=1.0.1

JARNAME=$APPNAME-$VERSION-jar-with-dependencies.jar
CFGNAME=$APPNAME.properties
HOME=/var/local/sophia/$ENVNAME
#HOME=/home/orchestrator/sophia/servicebus
#OUTPUT=/dev/null
OUTPUT=$HOME/logs/$APPNAME.out

echo "$(date +%Y%m%d%H%M%S) $ENVNAME $APPNAME" >> $OUTPUT

nohup /opt/java8/bin/java -jar $HOME/$JARNAME $HOME/$CFGNAME 2>&1 >> $OUTPUT &

echo "$(date +%Y%m%d%H%M%S) $ENVNAME $APPNAME" >> $HOME/logs/crontab.log
