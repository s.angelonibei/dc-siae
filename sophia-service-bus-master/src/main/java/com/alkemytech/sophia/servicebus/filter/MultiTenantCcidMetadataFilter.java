package com.alkemytech.sophia.servicebus.filter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.sql.DataSource;

import com.alkemytech.sophia.servicebus.tools.SendMail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.google.inject.name.Named;

public class MultiTenantCcidMetadataFilter implements BusFilter {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final boolean enabled;
	private final BigDecimal totalValueScale;
	private final String totalValueDefault;
	private final String muleMailUrl;
	private final int errorRegexGroup;
	private final Pattern errorRegex;
	private final DataSource mcmdbDS;
	private final Properties configuration;

	private static final String CLAIM_ERRORE = "1";
	private static final String CLAIM_TERMINATO = "2";
	private static final String CLAIM_NEGATIVO = "3";


	@Inject
	protected MultiTenantCcidMetadataFilter(@Named("configuration") Properties configuration,
											@Named("MCMDB") DataSource mcmdbDS) {
		super();

		this.configuration = configuration;
		this.muleMailUrl = configuration
				.getProperty("mule.mail.url", "");
		this.enabled = Boolean.parseBoolean(configuration
				.getProperty("filter.ccid.enabled", "true"));
		this.totalValueScale = new BigDecimal(configuration
				.getProperty("filter.ccid.mt.total_value_scale", "1.0"));
		this.totalValueDefault = configuration
				.getProperty("filter.ccid.total_value_default", "0");
		this.errorRegexGroup = Integer.parseInt(configuration
				.getProperty("filter.ccid.error_regex_group", "0"));
		this.errorRegex = 0 == errorRegexGroup ? null :
				Pattern.compile(configuration.getProperty("filter.ccid.error_regex", ""));
		this.mcmdbDS = mcmdbDS;
	}

	@Override
	public Runnable newFilterTask(final Map<String, String> context, final JsonObject message) {
		return new Runnable() {
			@Override
			public void run() {
				try {
					process(context, message);
				} catch (Throwable e) {
					logger.error("newFilterTask", e);
				}
			}
		};
	}

	private String getAsString(JsonObject json, String key) {
		final JsonElement element = null == json ? null : json.get(key);
		return null == element ? null : element.getAsString();
	}

	private void process(Map<String, String> context, JsonObject message) throws Exception {
		// skip all but {env}_completed_claim && {env}_failed_claim
		final String queueType = context.get("queueType");
		if (!enabled || !("completed".equals(queueType) || "failed".equals(queueType)) ||
				!"pricingclaim".equals(context.get("serviceName"))) {
			return;
		}
		// process message
		try (final Connection mcmdb = mcmdbDS.getConnection()) {
			logger.debug("process: connected to {} {}", mcmdb.getMetaData()
					.getDatabaseProductName(), mcmdb.getMetaData().getURL());
			mcmdb.setAutoCommit(false);

			// parse message fields
			final JsonObject input = message.getAsJsonObject("input");
			final JsonObject inputBody = input.getAsJsonObject("body");
			final String idCcid = getAsString(inputBody, "ccid_id");
			final String idDsr = getAsString(inputBody, "idDsr");
			final String currency = getAsString(inputBody, "royalty_currency");
			final String ccidVersion = getAsString(inputBody, "ccidVersion");
			final String ccidEncoded = getAsString(inputBody, "encoded");
			final String ccidEncodedProvisional = getAsString(inputBody, "encodedProvisional");
			final String ccidNotEncoded = getAsString(inputBody, "notEncoded");
			final String month = getAsString(inputBody, "month");
			final String year = getAsString(inputBody, "year");


			// search for total value
			String totalValueOrig = null;
			String totalValue = null;
			String sumAmountLicensor = null;
			String sumAmountPai = null;
			String sumAmountUnmatched = null;
			String sumUseQuantity = null;
			String sumUseQuantityMatched = null;
			String sumUseQuantityUnmatched = null;
			String sumUseQuantitySiae = null;
			String sumIdentificatoValorePricing = null;
			String amountScale = null;
			String identificatoUtilizzazioniTot = null;
			String extendedDsrUrl = null;
			String ccidUrl = null;

			JsonArray jsonArraySocieta = null;

			if ("completed".equals(queueType)) {
				final JsonObject output = message.getAsJsonObject("output");
				final JsonObject statistics = output.getAsJsonObject("statistics");

				boolean ccidNotEncodedBool = "true".equalsIgnoreCase(ccidNotEncoded);

				if (ccidNotEncodedBool) {
					final Double valNotEncoded = new Double(getAsString(statistics, "claimValore")) + new Double(getAsString(statistics, "nonIdentificatoValore"));
					totalValue = totalValueOrig = valNotEncoded.toString();
				} else {
					totalValue = totalValueOrig = getAsString(statistics, "claimValore");
				}

				sumAmountLicensor = getAsString(statistics, "claimValore");
//				sumAmountLicensor = ccidNotEncodedBool ? getAsString(statistics, "identificatoValore") : getAsString(statistics, "claimValore");

				sumIdentificatoValorePricing = getAsString(statistics, "identificatoValore");
				sumUseQuantitySiae = getAsString(statistics, "claimUtilizzazioni");
				sumAmountUnmatched = getAsString(statistics, "nonIdentificatoValore");
				sumUseQuantityUnmatched = getAsString(statistics, "nonIdentificatoUtilizzazioni");

				sumUseQuantity = getAsString(statistics, "totalSales");
				sumUseQuantityMatched = getAsString(statistics, "identificatoUtilizzazioni");

				identificatoUtilizzazioniTot = getAsString(statistics, "identificatoUtilizzazioni");

				extendedDsrUrl = getAsString(output, "extendedDsrUrl");

				ccidUrl = getAsString(output, "ccidUrl");

				amountScale = getAsString(output, "amountScale");
				jsonArraySocieta = statistics.getAsJsonArray("societa");

				//sendMail CLAIM FINISHED OR NEGATIVE
				if (
						(null != totalValueOrig && totalValueOrig.indexOf("-") >= 0) ||
								(null != totalValue && totalValue.indexOf("-") >= 0) ||
								(null != sumAmountLicensor && sumAmountLicensor.indexOf("-") >= 0) ||
								(null != sumAmountPai && sumAmountPai.indexOf("-") >= 0) ||
								(null != sumAmountUnmatched && sumAmountUnmatched.indexOf("-") >= 0) ||
								(null != sumUseQuantity && sumUseQuantity.indexOf("-") >= 0) ||
								(null != sumUseQuantityMatched && sumUseQuantityMatched.indexOf("-") >= 0) ||
								(null != sumUseQuantityUnmatched && sumUseQuantityUnmatched.indexOf("-") >= 0) ||
								(null != sumUseQuantitySiae && sumUseQuantitySiae.indexOf("-") >= 0) ||
								(null != sumIdentificatoValorePricing && sumIdentificatoValorePricing.indexOf("-") >= 0) ||
								(null != amountScale && amountScale.indexOf("-") >= 0) ||
								(null != identificatoUtilizzazioniTot && identificatoUtilizzazioniTot.indexOf("-") >= 0)) {

					final String emailSql = configuration
							.getProperty("notifica_mail.sql")
							.replace("{dsp}", context.get("idDsp"))
							.replace("{tipo}", CLAIM_NEGATIVO);


					String indirizziMail = "";
					try (final Statement statement = mcmdb.createStatement()) {
						try (final ResultSet rset = statement.executeQuery(emailSql)) {
							if (rset.next())
								indirizziMail = rset.getString(1);

						}
					}
					try {
						SendMail.sendMailDsr(Arrays.asList(indirizziMail.split(",")), SendMail.TIPO_MAIL.CLAIM_NEGATIVO, idDsr, muleMailUrl);
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					final String emailSql = configuration
							.getProperty("notifica_mail.sql")
							.replace("{dsp}", context.get("idDsp"))
							.replace("{tipo}", CLAIM_TERMINATO);

					String indirizziMail = "";
					try (final Statement statement = mcmdb.createStatement()) {
						try (final ResultSet rset = statement.executeQuery(emailSql)) {
							if (rset.next())
								indirizziMail = rset.getString(1);

						}
					}
					try {
						SendMail.sendMailDsr(Arrays.asList(indirizziMail.split(",")), SendMail.TIPO_MAIL.CLAIM_FINITO, idDsr, muleMailUrl);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			} else if ("failed".equals(queueType) && null != errorRegex) {


				final JsonObject error = message.getAsJsonObject("error");
				final String description = getAsString(error, "description");
				if (!Strings.isNullOrEmpty(description)) {
					final Matcher matcher = errorRegex.matcher(description);
					if (matcher.matches()) {
						totalValue = totalValueOrig = matcher.group(errorRegexGroup);
					}
				}
//				try {
//					final String emailSql = configuration
//							.getProperty("notifica_mail.sql")
//							.replace("{dsp}", context.get("idDsp"))
//							.replace("{tipo}", CLAIM_ERRORE);
//
//					String indirizziMail = "";
//					try (final Statement statement = mcmdb.createStatement()) {
//						try (final ResultSet rset = statement.executeQuery(emailSql)) {
//							if (rset.next())
//								indirizziMail = rset.getString(1);
//
//						}
//					}
//
//					//sendMail CLAIM ERROR
//					SendMail.sendMailDsr(Arrays.asList(indirizziMail.split(",")), SendMail.TIPO_MAIL.CLAIM_ERRORE, idDsr, muleMailUrl);
//				}catch(Exception e){}
			}

			// scale factor
			BigDecimal scaleFactor = totalValueScale;
			if (!Strings.isNullOrEmpty(amountScale)) {
				try {
					scaleFactor = BigDecimal.ONE
							.divide(new BigDecimal(amountScale), 20, RoundingMode.HALF_UP);
				} catch (Exception e) {
					logger.error("process", e);
				}
			}

			// scale totalValueOrig
			if (!Strings.isNullOrEmpty(totalValueOrig)) {
				try {
					totalValueOrig = new BigDecimal(totalValueOrig)
							.multiply(scaleFactor).toPlainString();
				} catch (Exception e) {
					logger.error("process", e);
					totalValueOrig = null;
				}
			}


			// amount(s) scale factor
			BigDecimal amountScaleFactor = scaleFactor;
			BigDecimal amountScaleFactorInvert = scaleFactor;

			// fix currency
			BigDecimal currencyRate = null;
			if (!"EUR".equalsIgnoreCase(currency)) {
				final String sql = new StringBuilder()
						.append("select rate from currency_rate")
						.append(" where src_currency = '").append(currency)
						.append("' and dst_currency = 'EUR'")
						.append(" and year = '").append(year)
						.append("' and month = '").append(month)
						.append("'")
						.toString();
				try (final Statement statement = mcmdb.createStatement()) {
					try (final ResultSet rset = statement.executeQuery(sql)) {
						if (rset.next())
							amountScaleFactor = currencyRate = rset.getBigDecimal(1);
					}
				} catch (Exception e) {
					logger.error("process", e);
				}
				// WARNING: no currency exchange rate available
				if (null == currencyRate) {
					totalValueOrig = null;
				}
			}

			// scale sumUseQuantitySiae
			if (!Strings.isNullOrEmpty(sumUseQuantitySiae)) {
				try {
					sumUseQuantitySiae = new BigDecimal(sumUseQuantitySiae)
							.multiply(amountScaleFactor).toPlainString();
				} catch (Exception e) {
					logger.error("process", e);
					sumUseQuantitySiae = null;
				}
			}


			logger.debug("Scale Factor "+scaleFactor);
			logger.debug("YEAR  , MONTH"+year+" , "+month);
			logger.debug("Currency Rate "+currencyRate);
			logger.debug("Currency "+currency);
			if (null != currencyRate) {
				amountScaleFactorInvert = scaleFactor.divide(currencyRate, 20, RoundingMode.HALF_UP);
			}
			// scale totalValueOrig
			if (!Strings.isNullOrEmpty(totalValueOrig)) {
				try {
//                  FIX  INC0016168 - CHG0034229 - MANCANZA STATISTICHE RIPARTIZIONE
					totalValueOrig = new BigDecimal(totalValueOrig).toPlainString();
//							.multiply(amountScaleFactor).toPlainString();
//                            .multiply(amountScaleFactorInvert).toPlainString();
				} catch (Exception e) {
					logger.error("process", e);
					totalValueOrig = null;
				}
			}


			// scale totalValue
			logger.debug("Total Value "+totalValue);
			logger.debug("amountScaleFactorInvert  "+amountScaleFactorInvert);
			if (!Strings.isNullOrEmpty(totalValue)) {
				try {
//                  FIX  INC0016168 - CHG0034229 - MANCANZA STATISTICHE RIPARTIZIONE
					totalValue = new BigDecimal(totalValue).multiply(amountScaleFactor).toPlainString();
//                            .multiply(amountScaleFactor).toPlainString();
				} catch (Exception e) {
					logger.error("process", e);
					totalValue = null;
				}
			}
			logger.debug("Total Value "+totalValue);
			// scale sumAmountLicensor
			if (!Strings.isNullOrEmpty(sumAmountLicensor)) {
				try {
					sumAmountLicensor = new BigDecimal(sumAmountLicensor)
							.multiply(amountScaleFactor).toPlainString();
				} catch (Exception e) {
					logger.error("process", e);
					sumAmountLicensor = null;
				}
			}
			// scale sumAmountPai
			if (!Strings.isNullOrEmpty(sumAmountPai)) {
				try {
					sumAmountPai = new BigDecimal(sumAmountPai)
							.multiply(amountScaleFactor).toPlainString();
				} catch (Exception e) {
					logger.error("process", e);
					sumAmountPai = null;
				}
			}
			// scale sumAmountUnmatched
			if (!Strings.isNullOrEmpty(sumAmountUnmatched)) {
				try {
					sumAmountUnmatched = new BigDecimal(sumAmountUnmatched)
							.multiply(amountScaleFactor).toPlainString();
				} catch (Exception e) {
					logger.error("process", e);
					sumAmountUnmatched = null;
				}
			}
			// scale sumIdentificatoValorePricing
			if (!Strings.isNullOrEmpty(sumIdentificatoValorePricing)) {
				try {
					sumIdentificatoValorePricing = new BigDecimal(sumIdentificatoValorePricing)
							.multiply(amountScaleFactor).toPlainString();
				} catch (Exception e) {
					logger.error("process", e);
					sumIdentificatoValorePricing = null;
				}
			}
			// insert into CCID_METADATA
			String sql = new StringBuilder()
					.append("insert into CCID_METADATA")
					.append("(ID_CCID")
					.append(", ID_DSR")
					.append(", TOTAL_VALUE")
					.append(", VALORE_FATTURABILE")
					.append(", VALORE_RESIDUO")
					.append(", CURRENCY")
					.append(", CCID_VERSION")
					.append(", CCID_ENCODED")
					.append(", CCID_ENCODED_PROVISIONAL")
					.append(", CCID_NOT_ENCODED")
					.append(", SUM_USE_QUANTITY")
					.append(", SUM_USE_QUANTITY_MATCHED")
					.append(", SUM_USE_QUANTITY_UNMATCHED")
					.append(", SUM_AMOUNT_LICENSOR")
					.append(", SUM_AMOUNT_PAI")
					.append(", SUM_AMOUNT_UNMATCHED")
					.append(", SUM_USE_QUANTITY_SIAE")
					.append(", IDENTIFICATO_VALORE_PRICING")
					.append(", TOTAL_VALUE_CCID_CURRENCY")
					.append(") values")
					.append("(").append(Strings.isNullOrEmpty(idCcid) ? "null" : "'" + idCcid + "'")
					.append(", ").append(Strings.isNullOrEmpty(idDsr) ? "null" : "'" + idDsr + "'")
					.append(", ").append(Strings.isNullOrEmpty(totalValue) ? totalValueDefault : totalValue)
					.append(", ").append(Strings.isNullOrEmpty(totalValue) ? totalValueDefault : totalValue)
					.append(", ").append(Strings.isNullOrEmpty(totalValue) ? totalValueDefault : totalValue)
					.append(", ").append(Strings.isNullOrEmpty(currency) ? "null" : "'" + currency + "'")
					.append(", ").append(Strings.isNullOrEmpty(ccidVersion) ? "null" : "'" + ccidVersion + "'")
					.append(", ").append(Strings.isNullOrEmpty(ccidEncoded) ? "null" :
							"true".equalsIgnoreCase(ccidEncoded) ? "1" : "0")
					.append(", ").append(Strings.isNullOrEmpty(ccidEncodedProvisional) ? "null" :
							"true".equalsIgnoreCase(ccidEncodedProvisional) ? "1" : "0")
					.append(", ").append(Strings.isNullOrEmpty(ccidNotEncoded) ? "null" :
							"true".equalsIgnoreCase(ccidNotEncoded) ? "1" : "0")
					.append(", ").append(Strings.isNullOrEmpty(sumUseQuantity) ? "null" : sumUseQuantity)
					.append(", ").append(Strings.isNullOrEmpty(sumUseQuantityMatched) ? "null" : sumUseQuantityMatched)
					.append(", ").append(Strings.isNullOrEmpty(sumUseQuantityUnmatched) ? "null" : sumUseQuantityUnmatched)
					.append(", ").append(Strings.isNullOrEmpty(sumAmountLicensor) ? "null" : sumAmountLicensor)
					.append(", ").append(Strings.isNullOrEmpty(sumAmountPai) ? "null" : sumAmountPai)
					.append(", ").append(Strings.isNullOrEmpty(sumAmountUnmatched) ? "null" : sumAmountUnmatched)
					.append(", ").append(Strings.isNullOrEmpty(sumUseQuantitySiae) ? "null" : sumUseQuantitySiae)
					.append(", ").append(Strings.isNullOrEmpty(sumIdentificatoValorePricing) ? "null" : sumIdentificatoValorePricing)
					.append(", ").append(Strings.isNullOrEmpty(totalValueOrig) ? "null" : totalValueOrig)
					.append(")")
					.toString();
			logger.debug("process: sql {}", sql);

			try (final Statement statement = mcmdb.createStatement()) {
				int count = 0;
				try {
					count = statement.executeUpdate(sql);
					logger.debug("process: {} row(s) inserted", count);
				} catch (SQLException e) {
					logger.error("process: statement with error {}", sql);
					logger.error("process", e);
					count = 0;
				}
				if (1 != count) {
					sql = new StringBuilder()
							.append("update CCID_METADATA")
							.append(" set TOTAL_VALUE = ").append(Strings.isNullOrEmpty(totalValue) ? totalValueDefault : totalValue)
							.append(", VALORE_FATTURABILE = ").append(Strings.isNullOrEmpty(totalValue) ? totalValueDefault : totalValue)
							.append(", VALORE_RESIDUO = ").append(Strings.isNullOrEmpty(totalValue) ? totalValueDefault : totalValue)
							.append(", CURRENCY = ").append(Strings.isNullOrEmpty(currency) ? "null" : "'" + currency + "'")
							.append(", CCID_VERSION = ").append(Strings.isNullOrEmpty(ccidVersion) ? "null" : "'" + ccidVersion + "'")
							.append(", CCID_ENCODED = ").append(Strings.isNullOrEmpty(ccidEncoded) ? "null" :
									"true".equalsIgnoreCase(ccidEncoded) ? "1" : "0")
							.append(", CCID_ENCODED_PROVISIONAL = ").append(Strings.isNullOrEmpty(ccidEncodedProvisional) ? "null" :
									"true".equalsIgnoreCase(ccidEncodedProvisional) ? "1" : "0")
							.append(", CCID_NOT_ENCODED = ").append(Strings.isNullOrEmpty(ccidNotEncoded) ? "null" :
									"true".equalsIgnoreCase(ccidNotEncoded) ? "1" : "0")
							.append(", SUM_USE_QUANTITY = ").append(Strings.isNullOrEmpty(sumUseQuantity) ? "null" : sumUseQuantity)
							.append(", SUM_USE_QUANTITY_MATCHED = ").append(Strings.isNullOrEmpty(sumUseQuantityMatched) ? "null" : sumUseQuantityMatched)
							.append(", SUM_USE_QUANTITY_UNMATCHED = ").append(Strings.isNullOrEmpty(sumUseQuantityUnmatched) ? "null" : sumUseQuantityUnmatched)
							.append(", SUM_AMOUNT_LICENSOR = ").append(Strings.isNullOrEmpty(sumAmountLicensor) ? "null" : sumAmountLicensor)
							.append(", SUM_AMOUNT_PAI = ").append(Strings.isNullOrEmpty(sumAmountPai) ? "null" : sumAmountPai)
							.append(", SUM_AMOUNT_UNMATCHED = ").append(Strings.isNullOrEmpty(sumAmountUnmatched) ? "null" : sumAmountUnmatched)
							.append(", SUM_USE_QUANTITY_SIAE = ").append(Strings.isNullOrEmpty(sumUseQuantitySiae) ? "null" : sumUseQuantitySiae)
							.append(", IDENTIFICATO_VALORE_PRICING = ").append(Strings.isNullOrEmpty(sumIdentificatoValorePricing) ? "null" : sumIdentificatoValorePricing)
							.append(", TOTAL_VALUE_CCID_CURRENCY = ").append(Strings.isNullOrEmpty(totalValueOrig) ? "null" : totalValueOrig)
							.append(" where ID_CCID = ").append(Strings.isNullOrEmpty(idCcid) ? "null" : "'" + idCcid + "'")
							.append(" and ID_DSR = ").append(Strings.isNullOrEmpty(idDsr) ? "null" : "'" + idDsr + "'")
							.toString();
					logger.debug("process: sql {}", sql);
					try {
						count = statement.executeUpdate(sql);
						logger.debug("process: {} row(s) updated", count);
					} catch (SQLException e) {
						logger.error("process: statement with error {}", sql);
						logger.error("process", e);
					}
				}
				mcmdb.commit();
			}

			try {
				BigDecimal identificatoUtilizzazioniTotBD = new BigDecimal(identificatoUtilizzazioniTot);
				updateDsrStatistics(mcmdb, idDsr, identificatoUtilizzazioniTotBD);
			} catch (Exception e) {
				logger.error("process", e);
			}

			updateDsrMetadata(mcmdb, idDsr, extendedDsrUrl);

			updateCcidUrl(mcmdb, idDsr, ccidUrl);

			if (jsonArraySocieta != null) {
				for (JsonElement jsonElementSocieta : jsonArraySocieta) {
					StringBuilder insertDetailSql = new StringBuilder();
					String identificatoValore = getAsString(jsonElementSocieta.getAsJsonObject(), "identificatoValore");
					if (!Strings.isNullOrEmpty(identificatoValore)) {
						try {
							identificatoValore = new BigDecimal(identificatoValore)
									.multiply(amountScaleFactor).toPlainString();
						} catch (Exception e) {
							logger.error("process", e);
							identificatoValore = null;
						}
					}
					String identificatoUtilizzazioni = getAsString(jsonElementSocieta.getAsJsonObject(), "identificatoUtilizzazioni");
					String percentualeIdentificatoUtilizzazioni = getAsString(jsonElementSocieta.getAsJsonObject(), "percentualeIdentificatoUtilizzazioni");
					String claimValore = getAsString(jsonElementSocieta.getAsJsonObject(), "claimValore");

					if (!Strings.isNullOrEmpty(claimValore)) {
						try {
							claimValore = new BigDecimal(claimValore)
									.multiply(amountScaleFactor).toPlainString();
						} catch (Exception e) {
							logger.error("process", e);
							claimValore = null;
						}
					}
					String claimUtilizzazioni = getAsString(jsonElementSocieta.getAsJsonObject(), "claimUtilizzazioni");
					if (!Strings.isNullOrEmpty(claimUtilizzazioni)) {
						try {
							claimUtilizzazioni = new BigDecimal(claimUtilizzazioni)
									.multiply(scaleFactor).toPlainString();
						} catch (Exception e) {
							logger.error("process", e);
							claimUtilizzazioni = null;
						}
					}
					String nonIdentificatoValore = getAsString(jsonElementSocieta.getAsJsonObject(), "nonIdentificatoValore");
					if (!Strings.isNullOrEmpty(nonIdentificatoValore)) {
						try {
							nonIdentificatoValore = new BigDecimal(nonIdentificatoValore)
									.multiply(amountScaleFactor).toPlainString();
						} catch (Exception e) {
							logger.error("process", e);
							nonIdentificatoValore = null;
						}
					}
					String nonIdentificatoUtilizzazioni = getAsString(jsonElementSocieta.getAsJsonObject(), "nonIdentificatoUtilizzazioni");
					insertDetailSql.append("INSERT INTO CCID_METADATA_DETAILS")
							.append(" (ID_DSR,")
							.append(" SOCIETA,")
							.append(" IDENTIFICATO_VALORE,")
							.append(" IDENTIFICATO_UTILIZZAZIONI,")
							.append(" PERCENTUALE_IDENTIFICATO_UTILIZZAZIONI,")
							.append(" CLAIM_VALORE,")
							.append(" CLAIM_UTILIZZAZIONI,")
							.append(" NON_IDENTIFICATO_VALORE,")
							.append(" NON_IDENTIFICATO_UTILIZZAZIONI)")
							.append(" VALUES (")
							.append("  ").append(Strings.isNullOrEmpty(idDsr) ? "null" : "'" + idDsr + "'")
							.append(", '").append(getAsString(jsonElementSocieta.getAsJsonObject(), "societa")).append("'")
							.append(", ").append(identificatoValore)
							.append(", ").append(identificatoUtilizzazioni)
							.append(", ").append(percentualeIdentificatoUtilizzazioni)
							.append(", ").append(claimValore)
							.append(", ").append(claimUtilizzazioni)
							.append(", ").append(nonIdentificatoValore)
							.append(", ").append(nonIdentificatoUtilizzazioni)
							.append(")");
					sql = insertDetailSql.toString();
					logger.debug("process: sql {}", sql);
					try (final Statement statement = mcmdb.createStatement()) {
						int count = 0;
						try {
							count = statement.executeUpdate(sql);
							logger.debug("process: {} row(s) inserted", count);
						} catch (SQLException e) {
							logger.error("process: statement with error {}", sql);
							logger.error("process", e);
							count = 0;
						}
						if (1 != count) {
							StringBuilder updateDetailSql = new StringBuilder();
							updateDetailSql.append("UPDATE CCID_METADATA_DETAILS SET ")
									.append(" IDENTIFICATO_VALORE = ").append(identificatoValore)
									.append(", IDENTIFICATO_UTILIZZAZIONI = ").append(identificatoUtilizzazioni)
									.append(", PERCENTUALE_IDENTIFICATO_UTILIZZAZIONI = ").append(percentualeIdentificatoUtilizzazioni)
									.append(", CLAIM_VALORE = ").append(claimValore)
									.append(", CLAIM_UTILIZZAZIONI = ").append(claimUtilizzazioni)
									.append(", NON_IDENTIFICATO_VALORE = ").append(nonIdentificatoValore)
									.append(", NON_IDENTIFICATO_UTILIZZAZIONI = ").append(nonIdentificatoUtilizzazioni)
									.append(" WHERE ID_DSR = ").append(Strings.isNullOrEmpty(idDsr) ? "null" : "'" + idDsr + "'")
									.append(" AND SOCIETA = '").append(getAsString(jsonElementSocieta.getAsJsonObject(), "societa")).append("'");
							sql = updateDetailSql.toString();
							logger.debug("process: sql {}", sql);
							try {
								count = statement.executeUpdate(sql);
								logger.debug("process: {} row(s) updated", count);
							} catch (SQLException e) {
								logger.error("process: statement with error {}", sql);
								logger.error("process", e);
							}
						}
						mcmdb.commit();
					}
				}
			}
		}
	}

	protected void updateDsrStatistics(Connection connection, String dsr, BigDecimal saleLineRecognized) {
		String sql = "UPDATE DSR_STATISTICS " + "  "
				+ "SET SALES_LINE_RECOGNIZED = ? "
				+ "WHERE IDDSR = ? ";
		try (final PreparedStatement statement = connection.prepareStatement(sql)) {
			statement.setBigDecimal(1, saleLineRecognized);
			statement.setString(2, dsr);
			statement.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			logger.error("process: statement with error {}", sql);
			logger.error("process", e);
		}
	}

	protected void updateDsrMetadata(Connection connection, String dsr, String extendedDsrUrl) {
		String sql = "UPDATE DSR_METADATA " +
				"   SET EXTENDED_DSR_URL = ? " +
				"WHERE IDDSR = ? ";
		try (final PreparedStatement statement = connection.prepareStatement(sql)) {
			statement.setString(1, extendedDsrUrl);
			statement.setString(2, dsr);
			statement.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			logger.error("process: statement with error {}", sql);
			logger.error("process", e);
		}
	}

	protected void updateCcidUrl(Connection connection, String dsr, String ccidUrl) {
		String updateSql = "UPDATE CCID_S3_PATH " + "SET S3_PATH = ?" + " WHERE IDDSR = ?";
		int count = 0;
		try (final PreparedStatement statement = connection.prepareStatement(updateSql)) {
			statement.setString(1, ccidUrl);
			statement.setString(2, dsr);
			count = statement.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			logger.error("process: statement with error {}", updateSql);
			logger.error("process", e);
		}
		if (count == 0) {
			String insertSql = "INSERT INTO CCID_S3_PATH (IDDSR,S3_PATH) " + "VALUES (?, ?)";
			try (final PreparedStatement statement = connection.prepareStatement(insertSql)) {
				statement.setString(1, dsr);
				statement.setString(2, ccidUrl);
				count = statement.executeUpdate();
				connection.commit();
			} catch (SQLException e) {
				logger.error("process: statement with error {}", insertSql);
				logger.error("process", e);
			}
		}
	}
}
