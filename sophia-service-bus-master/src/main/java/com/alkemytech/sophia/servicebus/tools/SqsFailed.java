package com.alkemytech.sophia.servicebus.tools;

import java.sql.Connection;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.servicebus.GuiceModuleExtension;
import com.alkemytech.sophia.servicebus.McmdbDAO;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class SqsFailed {
	
	private static final Logger logger = LoggerFactory.getLogger(SqsFailed.class);

	public static void main(String[] args) throws Exception {
		try {	
			// guice injector
			final Injector injector = Guice
					.createInjector(new GuiceModuleExtension(args, "/configuration.properties"));
			// startup service(s)
			injector.getInstance(SQS.class).startup();
			//injector.getInstance(S3.class).startup();
			try {
				injector.getInstance(SqsFailed.class)
					.readFailed();
			} finally {
				// shutdown service(s)
				injector.getInstance(SQS.class).shutdown();
				//injector.getInstance(S3.class).shutdown();
			}
		} catch (Exception e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}

	private final Properties configuration;
	private final SQS sqs;
	private final DataSource mcmdbDS;

	@Inject
	protected SqsFailed(@Named("configuration") Properties configuration,
			SQS sqs,
			@Named("MCMDB") DataSource mcmdbDS) {
		super();
		this.configuration = configuration;
		this.sqs = sqs;
		this.mcmdbDS = mcmdbDS;
	}
	
	public void readFailed() throws Exception {
			
		try (final Connection connection = mcmdbDS.getConnection()) {
			logger.debug("connected to: " + connection.getMetaData()
					.getDatabaseProductName() +  " " + connection.getMetaData().getURL());

			// data access object
			final McmdbDAO mcmdbDAO = new McmdbDAO(connection);
			
			// queue(s)
			final Pattern pattern = Pattern.compile(configuration
					.getProperty("sqsfailed.queues.regex", ""), Pattern.CASE_INSENSITIVE);
			final List<String> queueUrls = sqs.getQueueUrls();
			if (null != queueUrls) for (String queueUrl : queueUrls) {
				final Matcher matcher = pattern.matcher(queueUrl);
				if (matcher.find()) {
					final String queueName = matcher.group(1);
					logger.debug("queueUrl: " + queueUrl);
					logger.debug("queueName: " + queueName);
					while (true) {
						final List<SQS.Msg> sqsMessages = sqs.receiveMessages(queueUrl);
						if (sqsMessages.isEmpty()) {
							logger.debug("empty queue: " + queueName);
							break;
						}
						for (SQS.Msg sqsMessage : sqsMessages) {
							logger.debug("received: " + sqsMessage.text);
							final int count = mcmdbDAO.insertSqsFailed(queueName, sqsMessage.text);
							logger.debug("inserted rows: " + count);
							sqs.deleteMessage(queueUrl, sqsMessage.receiptHandle);
						}
					}
				}
			}
			
		}
	}

}
