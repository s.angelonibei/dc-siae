package com.alkemytech.sophia.servicebus;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class McmdbDAO {

	private final Connection jdbc;
	
	public McmdbDAO(Connection jdbc) {
		super();
		this.jdbc = jdbc;
	}
	
	public boolean checkConnection() {
		try (final Statement stmt = jdbc.createStatement()) {
			try (final ResultSet rset = stmt.executeQuery("select 1 from dual")) {
				if (rset.next()) {
					return 1 == rset.getInt(1);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public int insertServiceBus(String queueName, Date messageTime, String messageJson, 
			String environment, String queueType, String serviceName,
			String sender, String hostname, String applicationId,
			String iddsr, String iddsp, String year, String month, String country) throws Exception {
		
		jdbc.setAutoCommit(true);
		jdbc.prepareCall("SET SESSION sql_mode = ''").execute();
		final String sql = new StringBuffer()
			.append("insert into SQS_SERVICE_BUS ")
			.append("( QUEUE_NAME")
			.append(", MESSAGE_TIME")
			.append(", MESSAGE_JSON")
			.append(", ENVIRONMENT")
			.append(", QUEUE_TYPE")
			.append(", SERVICE_NAME")
			.append(", SENDER")
			.append(", HOSTNAME")
			.append(", APPLICATION_ID")
			.append(", IDDSR")
			.append(", IDDSP")
			.append(", YEAR")
			.append(", MONTH")
			.append(", COUNTRY")
			.append(") values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
			.toString();
		try (final PreparedStatement stmt = jdbc.prepareStatement(sql)) {
			int i = 1;
			stmt.setString(i++, queueName);
			if (null != messageTime) {
				stmt.setTimestamp(i++, new Timestamp(messageTime.getTime()));
			} else {
				stmt.setNull(i++, Types.DATE);
			}
			stmt.setString(i++, messageJson);
			stmt.setString(i++, environment);
			stmt.setString(i++, queueType);
			stmt.setString(i++, serviceName);
			stmt.setString(i++, sender);
			stmt.setString(i++, hostname);
			stmt.setString(i++, applicationId);
			stmt.setString(i++, iddsr);
			stmt.setString(i++, iddsp);
			stmt.setString(i++, year);
			stmt.setString(i++, month);
			stmt.setString(i++, country);
			return stmt.executeUpdate();
//		} catch (SQLException e) {
//			if (1062 == e.getErrorCode()) { // duplicate key
//				return 0;
//			} else if (1406 == e.getErrorCode()) { // data too long for column					
//				return 0;
//			}
//			throw e;
		}
		
	}
	
	public int insertSqsFailed(String queueName, String messageJson) throws SQLException {
		
		jdbc.setAutoCommit(true);
		final String sql = new StringBuffer()
			.append("insert into SQS_FAILED ")
			.append("( QUEUE_NAME")
			.append(", MESSAGE_JSON")
			.append(") values (?, ?)")
			.toString();
		try (final PreparedStatement stmt = jdbc.prepareStatement(sql)) {
			stmt.setString(1, queueName);
			stmt.setString(2, messageJson);
			return stmt.executeUpdate();
//		} catch (SQLException e) {
//			if (1062 == e.getErrorCode()) { // duplicate key
//				return 0;
//			} else if (1406 == e.getErrorCode()) { // data too long for column					
//				return 0;
//			}
//			throw e;
		}
	
	}
	
	public Map<String, String> loadDspCodeMapping() throws SQLException {
		try (final Statement stmt = jdbc.createStatement()) {
			final Map<String, String> result = new HashMap<String, String>();
			try (final ResultSet rset = stmt.executeQuery("select CODE, IDDSP from ANAG_DSP")) {
				while (rset.next()) {
					result.put(rset.getString(1), rset.getString(2));
				}
			}
			return result;
		}
	}

}
