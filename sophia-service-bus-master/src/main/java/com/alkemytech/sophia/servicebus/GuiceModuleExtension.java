package com.alkemytech.sophia.servicebus;

import javax.sql.DataSource;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.aws.SQSExtendedClient;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.servicebus.filter.CcidMetadataFilter;
import com.alkemytech.sophia.servicebus.filter.ClusterMonitoringFilter;
import com.alkemytech.sophia.servicebus.filter.DsrStepsMonitoringFilter;
import com.alkemytech.sophia.servicebus.filter.MultiTenantCcidMetadataFilter;
import com.alkemytech.sophia.servicebus.filter.PmDumpFilter;
import com.alkemytech.sophia.servicebus.tools.SqsFailed;
import com.alkemytech.sophia.servicebus.tools.SqsIpiUlisse;
import com.alkemytech.sophia.servicebus.tools.SqsSubmitAgain;
import com.google.inject.Scopes;
import com.google.inject.name.Names;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class GuiceModuleExtension extends GuiceModule {

	public GuiceModuleExtension(String[] args, String resourceName) {
		super(args, resourceName);
	}

	@Override
	protected void configure() {
		super.configure();
		// amazon service(s)
		bind(S3.class)
			.in(Scopes.SINGLETON);
		bind(SQS.class)
			.in(Scopes.SINGLETON);
		bind(SQSExtendedClient.class)
                .in(Scopes.SINGLETON);

		// data source(s)
		bind(DataSource.class)
			.annotatedWith(Names.named("MCMDB"))
			.to(McmdbDataSource.class)
			.in(Scopes.SINGLETON);
		// service bus
		bind(ServiceBus.class).asEagerSingleton();
		bind(CcidMetadataFilter.class).asEagerSingleton();
		bind(MultiTenantCcidMetadataFilter.class).asEagerSingleton();
		bind(DsrStepsMonitoringFilter.class).asEagerSingleton();
		bind(ClusterMonitoringFilter.class).asEagerSingleton();
		bind(PmDumpFilter.class).asEagerSingleton();

		// tools
		bind(SqsFailed.class).asEagerSingleton();
		bind(SqsIpiUlisse.class).asEagerSingleton();
		bind(SqsSubmitAgain.class).asEagerSingleton();
	}

}

