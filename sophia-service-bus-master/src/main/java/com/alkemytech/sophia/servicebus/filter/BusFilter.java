package com.alkemytech.sophia.servicebus.filter;

import java.util.Map;

import com.google.gson.JsonObject;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public interface BusFilter {

	public Runnable newFilterTask(Map<String, String> context, JsonObject message);
	
}
