package com.alkemytech.sophia.servicebus.filter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.google.inject.name.Named;

public class CcidMetadataFilter implements BusFilter {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final boolean enabled;
	private final BigDecimal totalValueScale;
	private final String totalValueDefault;
	private final int errorRegexGroup;
	private final Pattern errorRegex;
	private final DataSource mcmdbDS;
	
	@Inject
	protected CcidMetadataFilter(@Named("configuration") Properties configuration,
			@Named("MCMDB") DataSource mcmdbDS) {
		super();
		this.enabled = Boolean.parseBoolean(configuration
				.getProperty("filter.ccid.enabled", "true"));
		this.totalValueScale = new BigDecimal(configuration
				.getProperty("filter.ccid.total_value_scale", "1.0"));
		this.totalValueDefault = configuration
				.getProperty("filter.ccid.total_value_default", "0");
		this.errorRegexGroup = Integer.parseInt(configuration
				.getProperty("filter.ccid.error_regex_group", "0"));
		this.errorRegex = 0 == errorRegexGroup ? null :
			Pattern.compile(configuration.getProperty("filter.ccid.error_regex", ""));
		this.mcmdbDS = mcmdbDS;
	}

	@Override
	public Runnable newFilterTask(final Map<String, String> context, final JsonObject message) {
		return new Runnable() {
			@Override
			public void run() {
				try {
					process(context, message);
				} catch (Throwable e) {
					logger.error("newFilterTask", e);
				}
			}
		};
	}

	private String getAsString(JsonObject json, String key) {
		final JsonElement element = null == json ? null : json.get(key);
		return null == element ? null : element.getAsString();
	}
	
	private void process(Map<String, String> context, JsonObject message) throws SQLException {
		// skip all but {env}_completed_claim && {env}_failed_claim
		final String queueType = context.get("queueType");
		if (!enabled || !("completed".equals(queueType) || "failed".equals(queueType)) ||
				!"claim".equals(context.get("serviceName"))) {
			return;
		}
		// process message 
		try (final Connection mcmdb = mcmdbDS.getConnection()) {
			logger.debug("process: connected to {} {}", mcmdb.getMetaData()
					.getDatabaseProductName(), mcmdb.getMetaData().getURL());

			// parse message fields
			final JsonObject input = message.getAsJsonObject("input");
			final JsonObject inputBody = input.getAsJsonObject("body");
			final String idCcid = getAsString(inputBody, "ccid_id");
			final String idDsr = getAsString(inputBody, "idDsr");
			final String currency = getAsString(inputBody, "royalty_currency");
			final String ccidVersion = getAsString(inputBody, "ccidVersion");
			final String ccidEncoded = getAsString(inputBody, "encoded");
			final String ccidEncodedProvisional = getAsString(inputBody, "encodedProvisional");
			final String ccidNotEncoded = getAsString(inputBody, "notEncoded");
			final String month = getAsString(inputBody, "month");
			final String year = getAsString(inputBody, "year");

			// search for total value
			String totalValueOrig = null;
			String totalValue = null;
			String sumAmountLicensor = null;
			String sumAmountPai = null;
			String sumAmountUnmatched = null;
			String sumUseQuantity = null;
			String sumUseQuantityMatched = null;
			String sumUseQuantityUnmatched = null;
			String sumUseQuantitySiae = null;
			String sumIdentificatoValorePricing = null;
			String amountScale = null;
			if ("completed".equals(queueType)) {
				final JsonObject output = message.getAsJsonObject("output");
				totalValue = totalValueOrig = getAsString(output, "totalValue");
				sumAmountLicensor = getAsString(output, "sumAmountLicensor");
				sumAmountPai = getAsString(output, "sumAmountPai");
				sumAmountUnmatched = getAsString(output, "sumAmountUnmatched");
				sumUseQuantity = getAsString(output, "sumUseQuantity");
				sumUseQuantityMatched = getAsString(output, "sumUseQuantityMatched");
				sumUseQuantityUnmatched = getAsString(output, "sumUseQuantityUnmatched");
				sumUseQuantitySiae = getAsString(output, "sumUseQuantitySiae");
				sumIdentificatoValorePricing = getAsString(output, "sumIdentificatoValorePricing");
				amountScale = getAsString(output, "amountScale");
			} else if ("failed".equals(queueType) && null != errorRegex) {
				final JsonObject error = message.getAsJsonObject("error");
				final String description = getAsString(error, "description");
				if (!Strings.isNullOrEmpty(description)) {
					final Matcher matcher = errorRegex.matcher(description);
					if (matcher.matches()) {
						totalValue = totalValueOrig = matcher.group(errorRegexGroup);
					}
				}
			}

			// scale factor
			BigDecimal scaleFactor = totalValueScale;
			if (!Strings.isNullOrEmpty(amountScale)) {
				try {
					scaleFactor = BigDecimal.ONE
							.divide(new BigDecimal(amountScale), 20, RoundingMode.HALF_UP);
				} catch (Exception e) {
					logger.error("process", e);
				}
			}
			
			// scale totalValueOrig
			if (!Strings.isNullOrEmpty(totalValueOrig)) {
				try {
					totalValueOrig = new BigDecimal(totalValueOrig)
						.multiply(scaleFactor).toPlainString();
				} catch (Exception e) {
					logger.error("process", e);
					totalValueOrig = null;
				}
			}
			// scale sumUseQuantitySiae
			if (!Strings.isNullOrEmpty(sumUseQuantitySiae)) {
				try {
					sumUseQuantitySiae = new BigDecimal(sumUseQuantitySiae)
						.multiply(scaleFactor).toPlainString();
				} catch (Exception e) {
					logger.error("process", e);
					sumUseQuantitySiae = null;
				}
			}
			
			// amount(s) scale factor
			BigDecimal amountScaleFactor = scaleFactor;
			
			// fix currency
			BigDecimal currencyRate = null;
			if (!"EUR".equalsIgnoreCase(currency)) {
				final String sql = new StringBuilder()
					.append("select rate from currency_rate")
					.append(" where src_currency = '").append(currency)
					.append("' and dst_currency = 'EUR'")
					.append(" and year = '").append(year)
					.append("' and month = '").append(month)
					.append("'")
					.toString();
				try (final Statement statement = mcmdb.createStatement()) {
					try (final ResultSet rset = statement.executeQuery(sql)) {
						if (rset.next())
							currencyRate = rset.getBigDecimal(1);
					}
				} catch (Exception e) {
					logger.error("process", e);
				}
				// WARNING: no currency exchange rate available
				if (null == currencyRate) {
					totalValue = null;
					sumAmountLicensor = null;
					sumAmountPai = null;
					sumAmountUnmatched = null;
				}
			}
			if (null != currencyRate) {
				amountScaleFactor = amountScaleFactor.multiply(currencyRate);
			}

			// scale totalValue
			if (!Strings.isNullOrEmpty(totalValue)) {
				try {
					totalValue = new BigDecimal(totalValue)
						.multiply(amountScaleFactor).toPlainString();
				} catch (Exception e) {
					logger.error("process", e);
					totalValue = null;
				}
			}
			// scale sumAmountLicensor
			if (!Strings.isNullOrEmpty(sumAmountLicensor)) {
				try {
					sumAmountLicensor = new BigDecimal(sumAmountLicensor)
						.multiply(amountScaleFactor).toPlainString();
				} catch (Exception e) {
					logger.error("process", e);
					sumAmountLicensor = null;
				}
			}
			// scale sumAmountPai
			if (!Strings.isNullOrEmpty(sumAmountPai)) {
				try {
					sumAmountPai = new BigDecimal(sumAmountPai)
						.multiply(amountScaleFactor).toPlainString();
				} catch (Exception e) {
					logger.error("process", e);
					sumAmountPai = null;
				}
			}
			// scale sumAmountUnmatched
			if (!Strings.isNullOrEmpty(sumAmountUnmatched)) {
				try {
					sumAmountUnmatched = new BigDecimal(sumAmountUnmatched)
						.multiply(amountScaleFactor).toPlainString();
				} catch (Exception e) {
					logger.error("process", e);
					sumAmountUnmatched = null;
				}
			}
			// scale sumIdentificatoValorePricing
			if (!Strings.isNullOrEmpty(sumIdentificatoValorePricing)) {
				try {
					sumIdentificatoValorePricing = new BigDecimal(sumIdentificatoValorePricing)
						.multiply(amountScaleFactor).toPlainString();
				} catch (Exception e) {
					logger.error("process", e);
					sumIdentificatoValorePricing = null;
				}
			}
			
//			logger.info("process: totalValueOrig {}", totalValueOrig);
//			logger.info("process: totalValue {}", totalValue);
//			logger.info("process: sumAmountLicensor {}", sumAmountLicensor);
//			logger.info("process: sumAmountPai {}", sumAmountPai);
//			logger.info("process: sumAmountUnmatched {}", sumAmountUnmatched);
//			logger.info("process: sumUseQuantity {}", sumUseQuantity);
//			logger.info("process: sumUseQuantityMatched {}", sumUseQuantityMatched);
//			logger.info("process: sumUseQuantityUnmatched {}", sumUseQuantityUnmatched);
//			logger.info("process: sumUseQuantitySiae {}", sumUseQuantitySiae);

			// insert into CCID_METADATA
			String sql = new StringBuilder()
				.append("insert into CCID_METADATA")
				.append("(ID_CCID")
				.append(", ID_DSR")
				.append(", TOTAL_VALUE")
				.append(", VALORE_FATTURABILE")
				.append(", VALORE_RESIDUO")
				.append(", CURRENCY")
				.append(", CCID_VERSION")
				.append(", CCID_ENCODED")
				.append(", CCID_ENCODED_PROVISIONAL")
				.append(", CCID_NOT_ENCODED")
				.append(", SUM_USE_QUANTITY")
				.append(", SUM_USE_QUANTITY_MATCHED")
				.append(", SUM_USE_QUANTITY_UNMATCHED")
				.append(", SUM_AMOUNT_LICENSOR")
				.append(", SUM_AMOUNT_PAI")
				.append(", SUM_AMOUNT_UNMATCHED")
				.append(", SUM_USE_QUANTITY_SIAE")
				.append(", IDENTIFICATO_VALORE_PRICING")
				.append(", TOTAL_VALUE_CCID_CURRENCY")
				.append(") values")
				.append("(").append(Strings.isNullOrEmpty(idCcid) ? "null" : "'" + idCcid + "'")
				.append(", ").append(Strings.isNullOrEmpty(idDsr) ? "null" : "'" + idDsr + "'")
				.append(", ").append(Strings.isNullOrEmpty(totalValue) ? totalValueDefault : totalValue)
				.append(", ").append(Strings.isNullOrEmpty(totalValue) ? totalValueDefault : totalValue)
				.append(", ").append(Strings.isNullOrEmpty(totalValue) ? totalValueDefault : totalValue)
				.append(", ").append(Strings.isNullOrEmpty(currency) ? "null" : "'" + currency + "'")
				.append(", ").append(Strings.isNullOrEmpty(ccidVersion) ? "null" : "'" + ccidVersion + "'")
				.append(", ").append(Strings.isNullOrEmpty(ccidEncoded) ? "null" : 
					"true".equalsIgnoreCase(ccidEncoded) ? "1" : "0")
				.append(", ").append(Strings.isNullOrEmpty(ccidEncodedProvisional) ? "null" :
					"true".equalsIgnoreCase(ccidEncodedProvisional) ? "1" : "0")
				.append(", ").append(Strings.isNullOrEmpty(ccidNotEncoded) ? "null" :
					"true".equalsIgnoreCase(ccidNotEncoded) ? "1" : "0")
				.append(", ").append(Strings.isNullOrEmpty(sumUseQuantity) ? "null" : sumUseQuantity)
				.append(", ").append(Strings.isNullOrEmpty(sumUseQuantityMatched) ? "null" : sumUseQuantityMatched)
				.append(", ").append(Strings.isNullOrEmpty(sumUseQuantityUnmatched) ? "null" : sumUseQuantityUnmatched)
				.append(", ").append(Strings.isNullOrEmpty(sumAmountLicensor) ? "null" : sumAmountLicensor)
				.append(", ").append(Strings.isNullOrEmpty(sumAmountPai) ? "null" : sumAmountPai)
				.append(", ").append(Strings.isNullOrEmpty(sumAmountUnmatched) ? "null" : sumAmountUnmatched)
				.append(", ").append(Strings.isNullOrEmpty(sumUseQuantitySiae) ? "null" : sumUseQuantitySiae)
				.append(", ").append(Strings.isNullOrEmpty(sumIdentificatoValorePricing) ? "null" : sumIdentificatoValorePricing)
				.append(", ").append(Strings.isNullOrEmpty(totalValueOrig) ? "null" : totalValueOrig)
				.append(")")
				.toString();
			logger.debug("process: sql {}", sql);

			mcmdb.setAutoCommit(true);
			try (final Statement statement = mcmdb.createStatement()) {
				int count = 0;
				try {
					count = statement.executeUpdate(sql);
					logger.debug("process: {} row(s) inserted", count);
				} catch (SQLException e) {
					logger.error("process: statement with error {}", sql);
					logger.error("process", e);
					count = 0;
				}
				if (1 != count) {
					sql = new StringBuilder()
						.append("update CCID_METADATA")
						.append(" set TOTAL_VALUE = ").append(Strings.isNullOrEmpty(totalValue) ? totalValueDefault : totalValue)
						.append(", VALORE_FATTURABILE = ").append(Strings.isNullOrEmpty(totalValue) ? totalValueDefault : totalValue)
						.append(", VALORE_RESIDUO = ").append(Strings.isNullOrEmpty(totalValue) ? totalValueDefault : totalValue)
						.append(", CURRENCY = ").append(Strings.isNullOrEmpty(currency) ? "null" : "'" + currency + "'")
						.append(", CCID_VERSION = ").append(Strings.isNullOrEmpty(ccidVersion) ? "null" : "'" + ccidVersion + "'")
						.append(", CCID_ENCODED = ").append(Strings.isNullOrEmpty(ccidEncoded) ? "null" : 
							"true".equalsIgnoreCase(ccidEncoded) ? "1" : "0")
						.append(", CCID_ENCODED_PROVISIONAL = ").append(Strings.isNullOrEmpty(ccidEncodedProvisional) ? "null" :
							"true".equalsIgnoreCase(ccidEncodedProvisional) ? "1" : "0")
						.append(", CCID_NOT_ENCODED = ").append(Strings.isNullOrEmpty(ccidNotEncoded) ? "null" :
							"true".equalsIgnoreCase(ccidNotEncoded) ? "1" : "0")
						.append(", SUM_USE_QUANTITY = ").append(Strings.isNullOrEmpty(sumUseQuantity) ? "null" : sumUseQuantity)
						.append(", SUM_USE_QUANTITY_MATCHED = ").append(Strings.isNullOrEmpty(sumUseQuantityMatched) ? "null" : sumUseQuantityMatched)
						.append(", SUM_USE_QUANTITY_UNMATCHED = ").append(Strings.isNullOrEmpty(sumUseQuantityUnmatched) ? "null" : sumUseQuantityUnmatched)
						.append(", SUM_AMOUNT_LICENSOR = ").append(Strings.isNullOrEmpty(sumAmountLicensor) ? "null" : sumAmountLicensor)
						.append(", SUM_AMOUNT_PAI = ").append(Strings.isNullOrEmpty(sumAmountPai) ? "null" : sumAmountPai)
						.append(", SUM_AMOUNT_UNMATCHED = ").append(Strings.isNullOrEmpty(sumAmountUnmatched) ? "null" : sumAmountUnmatched)
						.append(", SUM_USE_QUANTITY_SIAE = ").append(Strings.isNullOrEmpty(sumUseQuantitySiae) ? "null" : sumUseQuantitySiae)
						.append(", IDENTIFICATO_VALORE_PRICING = ").append(Strings.isNullOrEmpty(sumIdentificatoValorePricing) ? "null" : sumIdentificatoValorePricing)
						.append(", TOTAL_VALUE_CCID_CURRENCY = ").append(Strings.isNullOrEmpty(totalValueOrig) ? "null" : totalValueOrig)
						.append(" where ID_CCID = ").append(Strings.isNullOrEmpty(idCcid) ? "null" : "'" + idCcid + "'")
						.append(" and ID_DSR = ").append(Strings.isNullOrEmpty(idDsr) ? "null" : "'" + idDsr + "'")
						.toString();
					logger.debug("process: sql {}", sql);
					try {
						count = statement.executeUpdate(sql);
						logger.debug("process: {} row(s) updated", count);
					} catch (SQLException e) {
						logger.error("process: statement with error {}", sql);
						logger.error("process", e);
					}
				}
			}
		}
	}

//	public static void main(String[] args) throws Exception {
//		
//		Pattern p = Pattern.compile("^([^0-9]+)(compenso_dem_punto)([^0-9]+)(\\d+\\.?\\d*)([^0-9]+)(\\d+\\.?\\d*)([^0-9]+)(.*)$");
////		String s = "ERROR WHILE PERFORMING PROCESS: ('Sophia schema riparto', 'ERROR: sum of compenso_dem_punto and compenso_drm_punto are not equal to sum_amount_normalization; value is 281715 but should be 281997.0', 'Riparto data saved on riparto table for debugging')";
//		String s = "ERROR WHILE PERFORMING PROCESS: ('Sophia schema riparto', 'ERROR: sum of compenso_dem_punto and compenso_drm_punto are not equal to sum_amount_normalization; value is 399419755 but should be 399430489.0', '{\"sumAmountUnmatchedMech\": 53031839, \"sumAmountLicensorPerf\": 147077536, \"sumAmountUnmatchedPerf\": 160147615, \"sumAmountPai\": 0, \"sumAmountPaiMech\": 0, \"totalAmountPerf\": 307225151, \"sumAmountUnmatched\": 213179454, \"totalAmountMech\": 92205338, \"sumAmountLicensorMech\": 39173499, \"totalValue\": 399430489, \"sumAmountLicensor\": 186251035}', 'Riparto data saved on riparto table for debugging')";
//		
//		Matcher m = p.matcher(s);
//		System.out.println(m.matches());
//		System.out.println(m.group(1));
//		System.out.println(m.group(2));
//		System.out.println(m.group(3));
//		System.out.println(m.group(4));
//		System.out.println(m.group(5));
//		System.out.println(m.group(6));
//		System.out.println(m.group(7));
//		System.out.println(m.group(8));
//		
//	}
	
}
