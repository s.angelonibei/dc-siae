package com.alkemytech.sophia.servicebus.s3queue;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.http.HTTP;
import com.alkemytech.sophia.commons.sqs.McmdbMessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.MessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.SqsMessageHelper;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.S3Utils;
import com.alkemytech.sophia.servicebus.s3queue.jdbc.McmdbDAO;
import com.alkemytech.sophia.servicebus.s3queue.jdbc.McmdbDataSource;
import com.amazonaws.services.s3.model.S3Object;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Scopes;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.IOException;
import java.net.ServerSocket;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.Statement;
import java.util.List;

public class S3ExtractSourcePump {

	private static final Logger logger = LoggerFactory.getLogger(S3ExtractSourcePump.class);

	private static class GuiceModuleExtension extends GuiceModule {

		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// amazon service(s)
			bind(S3.class)
					.in(Scopes.SINGLETON);
			bind(SQS.class)
					.in(Scopes.SINGLETON);
			// http
			bind(HTTP.class)
					.toInstance(new HTTP(configuration));

			// message deduplicator(s)
			bind(MessageDeduplicator.class)
					.to(McmdbMessageDeduplicator.class)
					.in(Scopes.SINGLETON);
			bind(Configuration.class)
					.toInstance(new Configuration(configuration));
			//datasource
			// data source(s)
			bind(DataSource.class)
					.annotatedWith(Names.named("MCMDB"))
					.to(McmdbDataSource.class)
					.asEagerSingleton();
			// data access object(s)
			bind(McmdbDAO.class)
					.asEagerSingleton();

			bind(S3ExtractSourcePump.class)
					.asEagerSingleton();
		}

	}


	public static void main(String[] args) {
		try {
			final S3ExtractSourcePump instance = Guice
					.createInjector(new GuiceModuleExtension(args,
							"/s3-extract-source-pump.properties"))
					.getInstance(S3ExtractSourcePump.class)
					.startup();
			try {
				instance.process(args);
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}

	private final Configuration configuration;
	private final Charset charset;
	private final Gson gson;
	private final S3 s3;
	private final SQS sqs;
	private final HTTP http;
	private final MessageDeduplicator deduplicator;
	private final McmdbDAO dao;


	@Inject
	protected S3ExtractSourcePump(Configuration configuration,
                                  @Named("charset") Charset charset, Gson gson,
                                  McmdbDAO dao, S3 s3, SQS sqs, HTTP http, MessageDeduplicator deduplicator) {
		super();
		this.configuration = configuration;
		this.charset = charset;
		this.gson = gson;
		this.s3 = s3;
		this.sqs = sqs;
		this.http = http;
		this.dao = dao;
		this.deduplicator = deduplicator;
	}


	public S3ExtractSourcePump startup() throws IOException {
		s3.startup();
		sqs.startup();
		http.startup();
		return this;
	}
	public S3ExtractSourcePump shutdown() throws IOException {
		sqs.shutdown();
		s3.shutdown();
		http.shutdown();
		return this;
	}


	private S3ExtractSourcePump process(String[] args) throws Exception {

		final int bindPort = Integer.parseInt(configuration.getProperty("s3-extract-source-pump.bind_port", configuration.getProperty("default.bind_port", "0")));

		// bind lock tcp port
		try (final ServerSocket socket = new ServerSocket(bindPort)) {
			logger.info("socket bound to {}", socket.getLocalSocketAddress());

			process();
		}

		return this;
	}

	private void process() throws Exception {
		
		String fakeMsg =  configuration.getProperty("fake_msg");
		if(fakeMsg!= null && fakeMsg.length()>0){
			processMessage( GsonUtils.
					fromJson(fakeMsg, JsonObject.class));
		}else {


			final SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration.getProperties(), "s3-extract-source-pump.sqs");
			
			sqsMessagePump.pollingLoop(null, new SqsMessagePump.Consumer() {

				private JsonObject output;
				private JsonObject error;

				@Override
				public JsonObject getStartedMessagePayload(JsonObject message) {
					return null;
				}

				@Override
				public boolean consumeMessage(JsonObject message) {

					try {
						output = processMessage(message);
						return true;
					} catch (Exception e) {
						logger.error("", e);
						error = SqsMessageHelper.formatError(e);
						return false;
					}


				}

				@Override
				public JsonObject getCompletedMessagePayload(JsonObject message) {

					sqsMessagePump.sendToProcessMessage(configuration.getProperty("s3-extract-source-pump.sqs.destination_queue"),output,false);
					
					return output;
				}

				@Override
				public JsonObject getFailedMessagePayload(JsonObject message) {
					return error;
				}

			});
		}

	
	}


	private JsonObject processMessage(JsonObject message){
        final JsonObject output = new JsonObject();
//				final String srcBucket = configuration.getProperty("s3.src_bucket");

		List<S3.Url> filesV = S3Utils.getS3UrlFromS3Message(message);

		if(filesV.size() > 1){
			splitAndSend(message);
			return null;
		}else if (filesV.size() == 1){
			return processLogic(filesV.get(0));
		}else{
			throw new RuntimeException("No V files in Record");
		}

	}

	private void splitAndSend(JsonObject message){

		//for JsonObject get Records ssend message
		throw new RuntimeException("Too many file V in message: not supported");

	}

	private JsonObject processLogic(S3.Url fileV){

		final JsonObject output = new JsonObject();
		S3Object s3VFile = s3.getObject(fileV);

		String bucket = s3VFile.getBucketName();
		String idDsr = s3VFile.getKey().substring(0,s3VFile.getKey().lastIndexOf("_V.csv")).substring(s3VFile.getKey().lastIndexOf("/")+1);
		String path = s3VFile.getKey().substring(0,s3VFile.getKey().lastIndexOf("/")+1);
		
		S3.Url s3Gz = new S3.Url(bucket,path+idDsr+".csv.gz");

	
		
	

		output.addProperty("idDsr",idDsr);
		output.addProperty("file_path",fileV.toString());
		

		

		return output;
	}
	
    

}
