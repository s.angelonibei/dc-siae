package com.alkemytech.sophia.servicebus.s3queue.jdbc;


import com.alkemytech.sophia.commons.jdbc.GenericDAO;
import com.google.inject.Inject;
import com.google.inject.name.Named;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class McmdbDAO extends GenericDAO {

	@Inject
	public McmdbDAO(@Named("configuration") Properties configuration,
			@Named("MCMDB") DataSource dataSource) {
		super(configuration, dataSource);
	}

}


