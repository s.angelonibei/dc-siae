package com.alkemytech.sophia.servicebus.tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.sqs.Iso8601Helper;
import com.alkemytech.sophia.servicebus.GuiceModuleExtension;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class SqsIpiUlisse {
	
	private static final Logger logger = LoggerFactory.getLogger(SqsIpiUlisse.class);

	public static void main(String[] args) throws Exception {
		try {	
			final Injector injector = Guice
					.createInjector(new GuiceModuleExtension(args, "/configuration.properties"));
			injector.getInstance(SQS.class).startup();
			//injector.getInstance(S3.class).startup();
			try {
				injector.getInstance(SqsIpiUlisse.class)
					.aggregateIpiUlisse();
			} finally {
				injector.getInstance(SQS.class).shutdown();
				//injector.getInstance(S3.class).shutdown();
			}
		} catch (Exception e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}

	private final Properties configuration;
	private final SQS sqs;

	@Inject
	protected SqsIpiUlisse(@Named("configuration") Properties configuration,
			SQS sqs) {
		super();
		this.configuration = configuration;
		this.sqs = sqs;
	}
	
	private String getAsString(JsonObject json, String key) {
		final JsonElement element = null == json ? null : json.get(key);
		return null == element ? null : element.getAsString();
	}
	
	private String getHostName() {
		try {
			return InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			return "localhost";
		}
	}
	
	@SuppressWarnings("unchecked")
	private Map<String, String> loadRepository() {
		final File file = new File(configuration.getProperty("ipiulisse.repository", ""));
		try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(file))) {
			return (Map<String, String>) in.readObject();
		} catch (IOException | ClassNotFoundException e) {
			logger.error("", e);
			return new HashMap<>();
		}
	}
	
	private boolean saveRepository(Map<String, String> repository) {
		final File file = new File(configuration.getProperty("ipiulisse.repository", ""));
		try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file))) {
			out.writeObject(repository);
			return true;
		} catch (IOException e) {
			logger.error("", e);
			return false;
		}
	}
	
	private void sendMessage(String queueUrl, JsonObject body) {
		final Gson gson = new GsonBuilder()
			.disableHtmlEscaping()
			.setPrettyPrinting()
			.create();

		final JsonObject header = new JsonObject();
		header.addProperty("uuid", UUID.randomUUID().toString());
		header.addProperty("timestamp", Iso8601Helper.format(new Date()));
		header.addProperty("queue", configuration.getProperty("ipiulisse.queue.ipi_ulisse"));
		header.addProperty("sender", "sophia-dump");
		header.addProperty("hostname", getHostName());
		final JsonObject message = new JsonObject();
		message.add("header", header);
		message.add("body", body);
		
		logger.debug("sending message: " + gson.toJson(message));
		
		sqs.sendMessage(queueUrl, gson.toJson(message));
	}
	
	private void mergeMessages(Gson gson, Map<String, String> repository, String inputQueueUrl, String outputQueueUrl) {
		logger.debug("processing queue: " + inputQueueUrl);
		// read message(s)
		while (true) {
			final List<SQS.Msg> sqsMessages = sqs.receiveMessages(inputQueueUrl);
			if (sqsMessages.isEmpty()) {
				logger.debug("empty queue: " + inputQueueUrl);
				break;
			}
			for (SQS.Msg sqsMessage : sqsMessages) {
				logger.debug("processing message: " + sqsMessage.text);
				JsonObject messageJson = gson.fromJson(sqsMessage.text, JsonObject.class);
				if (null != messageJson) {
					JsonObject bodyJson = messageJson.getAsJsonObject("body");
					if (null != bodyJson) {
						String key = getAsString(bodyJson, "year") + "-" + getAsString(bodyJson, "month");
						logger.debug("key: " + key);
						String total = repository.get(key);
						logger.debug("total: " + total);
						if (null == total || Strings.isNullOrEmpty(total)) {
							repository.put(key, gson.toJson(bodyJson));
							saveRepository(repository);
						} else {
							JsonObject totalBodyJson = gson.fromJson(total, JsonObject.class);
							for (Entry<String, JsonElement> entry : bodyJson.entrySet()) {
								totalBodyJson.add(entry.getKey(), entry.getValue());
							}
							logger.debug("totalBodyJson: " + totalBodyJson);
							sendMessage(outputQueueUrl, totalBodyJson);
							repository.remove(key);
							saveRepository(repository);
						}
					}
				}
				sqs.deleteMessage(inputQueueUrl, sqsMessage.receiptHandle);
			}
		}
	}
	
	public void aggregateIpiUlisse() throws Exception {

		// gson
		final Gson gson = new GsonBuilder()
			.disableHtmlEscaping()
			.setPrettyPrinting()
			.create();
					
		// load repository
		final Map<String, String> repository = loadRepository();
		
		// queue(s)
		final String serviceBusQueueUrl = sqs.getOrCreateUrl(configuration.getProperty("ipiulisse.queue.service_bus"));
		logger.debug("serviceBusQueueUrl: " + serviceBusQueueUrl);
		final String ipiQueueUrl = sqs.getOrCreateUrl(configuration.getProperty("ipiulisse.queue.ipi"));
		logger.debug("ipiQueueUrl: " + ipiQueueUrl);
		final String ulisseQueueUrl = sqs.getOrCreateUrl(configuration.getProperty("ipiulisse.queue.ulisse"));
		logger.debug("ulisseQueueUrl: " + ulisseQueueUrl);
		
		// read ipi message(s)
		mergeMessages(gson, repository, ipiQueueUrl, serviceBusQueueUrl);
		
		// read ulisse message(s)
		mergeMessages(gson, repository, ulisseQueueUrl, serviceBusQueueUrl);
		
	}
	
}
