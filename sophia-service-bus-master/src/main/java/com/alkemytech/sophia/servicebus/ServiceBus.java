package com.alkemytech.sophia.servicebus;

import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.aws.SQSExtendedClient;
import com.alkemytech.sophia.commons.sqs.Iso8601Helper;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.servicebus.filter.*;
import com.google.common.base.Strings;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.name.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class ServiceBus {

	private static final Logger logger = LoggerFactory.getLogger(ServiceBus.class);

	public static void main(String[] args) {
		try {
			final Injector injector = Guice
					.createInjector(new GuiceModuleExtension(args, "/configuration.properties"));
			injector.getInstance(SQSExtendedClient.class).startup();
			//injector.getInstance(S3.class).startup();
			try {
				injector.getInstance(ServiceBus.class).readQueue();
			} finally {
				injector.getInstance(SQS.class).shutdown();
				//injector.getInstance(S3.class).shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}
	
	private final Properties configuration;
	private final SQSExtendedClient sqs;
	private final CcidMetadataFilter ccidMetadataFilter;
	private final MultiTenantCcidMetadataFilter multiTenantCcidMetadataFilter;
	private final DsrStepsMonitoringFilter stepsMonitoringFilter;
	private final ClusterMonitoringFilter clusterMonitoringFilter;
	private final PmDumpFilter pmDumpFilter;
	private final PmBonificaFilter pmBonificaFilter;
	private final UpdateSchemiRipartoFilter updateSchemiRipartoFilter;
	private final ExecutorService executorService;
	private final DataSource mcmdbDS;

	@Inject
	protected ServiceBus(@Named("configuration") Properties configuration,
						 SQSExtendedClient sqs,
						 CcidMetadataFilter ccidMetadataFilter,
						 MultiTenantCcidMetadataFilter multiTenantCcidMetadataFilter,
						 DsrStepsMonitoringFilter stepsMonitoringFilter,
						 ClusterMonitoringFilter clusterMonitoringFilter,
						 PmDumpFilter pmDumpFilter,
						 PmBonificaFilter pmBonificaFilter,
						 UpdateSchemiRipartoFilter updateSchemiRipartoFilter, @Named("MCMDB") DataSource mcmdbDS) {
		super();
		this.configuration = configuration;
		this.sqs = sqs;
		this.ccidMetadataFilter = ccidMetadataFilter;
		this.multiTenantCcidMetadataFilter = multiTenantCcidMetadataFilter;
		this.stepsMonitoringFilter = stepsMonitoringFilter;
		this.clusterMonitoringFilter = clusterMonitoringFilter;
		this.pmDumpFilter = pmDumpFilter;
		this.pmBonificaFilter = pmBonificaFilter;
		this.updateSchemiRipartoFilter = updateSchemiRipartoFilter;
		this.mcmdbDS = mcmdbDS;
		this.executorService = Executors.newCachedThreadPool();
	}
	
	private String getHostName() {
		try {
			return InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			return "localhost";
		}
	}
	
	public void readQueue() throws Exception {

		try (final Connection connection = mcmdbDS.getConnection()) {
			logger.info("readQueue: connected to {} {}", connection.getMetaData()
					.getDatabaseProductName(), connection.getMetaData().getURL());

			// data access object
			final McmdbDAO mcmdbDAO = new McmdbDAO(connection);

			// service bus queue
			final String serviceBusQueue = configuration.getProperty("servicebus.queue");
			logger.info("readQueue: serviceBusQueue {}", serviceBusQueue);
			final String serviceBusQueueUrl = sqs.getOrCreateUrl(serviceBusQueue);
			logger.info("readQueue: serviceBusQueueUrl {}", serviceBusQueueUrl);
			
			// service bus failed queue
			final String serviceBusFailedQueue = configuration.getProperty("servicebus.queue.failed");
			logger.info("readQueue: serviceBusFailedQueue {}", serviceBusFailedQueue);
			final String serviceBusFailedQueueUrl = sqs.getOrCreateUrl(serviceBusFailedQueue);
			logger.info("readQueue: serviceBusFailedQueueUrl {}", serviceBusFailedQueueUrl);

			// maps
			final Map<String, String> dspCodes = mcmdbDAO.loadDspCodeMapping();
			final Map<String, String> queueUrls = new HashMap<String, String>();
			
			// configuration params
			final SimpleDateFormat jsonDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			final Pattern queuePattern = Pattern.compile(configuration
					.getProperty("servicebus.queue_regex", "^([a-zA-Z0-9]+)_(to_process|[a-zA-Z]+)_([a-zA-Z0-9_]+)$"));
			final Pattern swallowPattern = Pattern.compile(configuration
					.getProperty("servicebus.swallow_regex", "^([a-zA-Z0-9]+)_(started)_([a-zA-Z0-9_]+)$"));
			final long runTimeoutMillis = TextUtils.parseLongDuration(configuration
					.getProperty("servicebus.run_timeout", "5m")); // default to 5 minutes
			final long pollingTimeoutMillis = TextUtils.parseLongDuration(configuration
					.getProperty("servicebus.polling_timeout", "3s")); // default to 3 seconds
			final int maxNumberOfMessages = Integer.parseInt(configuration
					.getProperty("servicebus.max_number_of_messages", "10")); // default to 10 messages
			final boolean fixTimestamp = "true".equalsIgnoreCase(configuration
					.getProperty("servicebus.fix_timestamp.enabled"));
			final long fixTimestampToleranceMillis = TextUtils.parseLongDuration(configuration
					.getProperty("servicebus.fix_timestamp.tolerance", "5m")); // default to 5 minutes

			final long startTimeMillis = System.currentTimeMillis();
			while (System.currentTimeMillis() - startTimeMillis < runTimeoutMillis) {
				
				final List<SQS.Msg> sqsMessages = sqs
						.receiveMessages(serviceBusQueueUrl, maxNumberOfMessages);
				logger.debug("readQueue: read {} messages", sqsMessages.size());
				if (sqsMessages.isEmpty()) {
					Thread.sleep(pollingTimeoutMillis);
					continue;
				}
				
				for (SQS.Msg sqsMessage : sqsMessages) {
					logger.debug("readQueue: message found {}", sqsMessage.text);
					// message as json object
					JsonObject messageJson = null;
					try {
						
						// mandatory header fields
						String headerUuid = null;
						String headerQueue = null;
						Date headerTimestamp = null;
						String headerSender = null;
						// context, optional header fields
						String hostname = null;
						String applicationId = null;
						// body, input.body or output
						String iddsr = null;
						String iddsp = null;
						String year = null;
						String month = null;
						String country = null;
						// queue name parts
						String environment = null;
						String queueType = null;
						String serviceName = null;
						
						try {
							
							// parse message
							messageJson = GsonUtils.fromJson(sqsMessage.text, JsonObject.class);
							logger.debug("readQueue: parsed message json {}", GsonUtils.toJson(messageJson));
							
							// parse header
							final JsonObject header = messageJson.getAsJsonObject("header");
							// header.uuid
							headerUuid = GsonUtils.getAsString(header, "uuid");
							// header.queue
							headerQueue = GsonUtils.getAsString(header, "queue");
							// header.timestamp
							final String timestamp = GsonUtils.getAsString(header, "timestamp");
							logger.debug("readQueue: timestamp {}", timestamp);
							if (!Strings.isNullOrEmpty(timestamp)) {
								try {
									headerTimestamp = Iso8601Helper.parse(timestamp);
								} catch (IllegalArgumentException e) {
									logger.warn("readQueue", e);
									try {
										headerTimestamp = jsonDateFormat.parse(timestamp);
									} catch (ParseException e1) {
										try {
											headerTimestamp = dateFormat.parse(timestamp);
										} catch (ParseException e2) {
											headerTimestamp = null;
										}
									}
								}
							}
							if (fixTimestamp && null != headerTimestamp) {
								long timeDiff = Calendar.getInstance()
										.getTimeInMillis() - headerTimestamp.getTime();
								timeDiff = (timeDiff + (timeDiff >= 0 ? fixTimestampToleranceMillis : -fixTimestampToleranceMillis)) / 3600000L;
								if (0L != timeDiff) {
									headerTimestamp.setTime(headerTimestamp.getTime() + timeDiff * 3600000L);
								}
							}
							// header.sender
							headerSender = GsonUtils.getAsString(header, "sender");
							// header.hostname
							hostname = GsonUtils.getAsString(header, "hostname");

							// parse context or input.context
							try {
								JsonObject context = null;
								try {
									context = messageJson.getAsJsonObject("context");
								} catch (Exception e) {
									context = null;
								}
								if (null == context) {
									final JsonObject input = messageJson.getAsJsonObject("input");
									if (null != input) {
										try {
											context = input.getAsJsonObject("context");
										} catch (Exception e) {
											context = null;
										}
									}
								}
								applicationId = GsonUtils.getAsString(context, "applicationId");
								// override header.hostname
								final String emrClusterId = GsonUtils.getAsString(context, "emrClusterId");
								if (!Strings.isNullOrEmpty(emrClusterId)) {
									hostname = emrClusterId;
								}
							} catch (Exception e) {
								logger.error("readQueue", e);
							}
							
							// parse body or input.body
							try {
								JsonObject body = null;
								try {
									body = messageJson.getAsJsonObject("body");
								} catch (Exception e) {
									body = null;
								}
								if (null == body) {
									final JsonObject input = messageJson.getAsJsonObject("input");
									if (null != input) {
										try {
											body = input.getAsJsonObject("body");
										} catch (Exception e) {
											body = null;
										}
									}
								}
								if (null != body) {
									iddsr = GsonUtils.getAsString(body, "idDsr");
									if (null == iddsr) {
										iddsr = GsonUtils.getAsString(body, "dsr");
									}
									iddsp = GsonUtils.getAsString(body, "dspCode");
									if (null == iddsp) {
										iddsp = GsonUtils.getAsString(body, "idDsp");
									}
									year = GsonUtils.getAsString(body, "year");
									month = GsonUtils.getAsString(body, "month");
									country = GsonUtils.getAsString(body, "country");
								}
							} catch (Exception e) {
								logger.error("readQueue", e);
							}
							
							// parse output
							try {
								JsonObject output = messageJson.getAsJsonObject("output");
								if (null != output) {
									if (null == iddsr) {
										iddsr = GsonUtils.getAsString(output, "dsrFileName");
									}
								}
							} catch (Exception e) {
								logger.error("readQueue", e);
							}

							// decode dsp codes
							if (null != iddsp && dspCodes.containsKey(iddsp)) {
								iddsp = dspCodes.get(iddsp);
							}
													
						} catch (Exception e) {
							logger.error("readQueue", e);
						}
						
						// invalid message
						if (null == messageJson ||
								Strings.isNullOrEmpty(headerQueue) ||
								Strings.isNullOrEmpty(headerUuid) ||
								null == headerTimestamp ||
								Strings.isNullOrEmpty(headerSender)) {
							logger.error("readQueue: missing or invalid mandatory header field(s)");
							logger.error("readQueue: messageJson {}", messageJson);
							logger.error("readQueue: headerQueue {}", headerQueue);
							logger.error("readQueue: headerUuid {}", headerUuid);
							logger.error("readQueue: headerTimestamp {}", headerTimestamp);
							logger.error("readQueue: headerSender {}", headerSender);
							throw new BadMessageException("missing or invalid mandatory header field(s)");
						}

						// extract environment, queueType and serviceName from mandatory field header.queue
						try {
							final Matcher matcher = queuePattern.matcher(headerQueue);
							if (matcher.matches()) {
								environment = matcher.group(1);
								queueType = matcher.group(2);
								serviceName = matcher.group(3);
							}
						} catch (Exception e) {
							logger.error("readQueue", e);
						}
						
						// invalid queue name
						if (Strings.isNullOrEmpty(environment) ||
								Strings.isNullOrEmpty(queueType) ||
								Strings.isNullOrEmpty(serviceName)) {
							throw new BadMessageException("invalid destination queue name");
						}

						// filters context
						final Map<String, String> context = new HashMap<String, String>();
						
						// mandatory header fields
						context.put("uuid", headerUuid);							
						context.put("queue", headerQueue);							
						context.put("timestamp", Iso8601Helper.format(headerTimestamp));
						context.put("sender", headerSender);
						// context, optional header fields
						context.put("hostname", hostname);
						context.put("applicationId", applicationId);
						// body, input.body or output
						context.put("idDsr", iddsr);							
						context.put("idDsp", iddsp);							
						context.put("year", year);							
						context.put("month", month);							
						context.put("country", country);	
						// queue name parts
						context.put("environment", environment);							
						context.put("queueType", queueType);							
						context.put("serviceName", serviceName);
						
						logger.debug("readQueue: context {}", context);							

						// insert into database
						boolean inserted = false;
						try {
							final int count = mcmdbDAO
									.insertServiceBus(headerQueue, headerTimestamp, sqsMessage.text,
											environment, queueType, serviceName, 
											headerSender, hostname, applicationId,
											iddsr, iddsp, year, month, country);
							logger.debug("readQueue: inserted rows {}", count);
							inserted = (1 == count);
						} catch (Exception e) {
							logger.error("readQueue", e);
						}
						if (!inserted) {
							if (mcmdbDAO.checkConnection()) {
								logger.debug("readQueue: connection healthy");
								throw new BadMessageException("error inserting message into database");
							}
							throw new ExitApplicationException("database connection error");
						}
						logger.debug("readQueue: message successfully inserted into db");							

						// send message to destination queue
						if (swallowPattern.matcher(headerQueue).matches()) {
							logger.debug("readQueue: message to \"{}\" swallowed", headerQueue);														
						} else {
							String queueUrl = queueUrls.get(headerQueue);
							if (null == queueUrl) {
								queueUrl = sqs.getOrCreateUrl(headerQueue);
								if (null == queueUrl) {
									throw new ExitApplicationException(String
											.format("unable to get or create url for queue \"%s\"", headerQueue));							
								}
								queueUrls.put(headerQueue, queueUrl);
							}
							logger.debug("readQueue: queue url \"{}\"", queueUrl);							
							if (!sqs.sendMessage(queueUrl, sqsMessage.text)) {
								throw new ExitApplicationException(String
										.format("error sending message to \"%s\"", headerQueue));																					
							}
							logger.debug("readQueue: message successfully sent to \"{}\"", headerQueue);														
						}
						
						// filter(s)
						try {
							
							// CCID metadata
							try {
								executorService.execute(ccidMetadataFilter
										.newFilterTask(context, messageJson));
							} catch (Exception e) {
								logger.error("readQueue", e);
							}
							try {
								executorService.execute(multiTenantCcidMetadataFilter
										.newFilterTask(context, messageJson));
							} catch (Exception e) {
								logger.error("readQueue", e);
							}
							// steps monitoring
							try {
								executorService.execute(stepsMonitoringFilter
										.newFilterTask(context, messageJson));
							} catch (Exception e) {
								logger.error("readQueue", e);
							}
							// cluster monitoring
							try {
								executorService.execute(clusterMonitoringFilter
										.newFilterTask(context, messageJson));
							} catch (Exception e) {
								logger.error("readQueue", e);
							}
							// PM dump
							try {
								executorService.execute(pmDumpFilter
										.newFilterTask(context, messageJson));
							} catch (Exception e) {
								logger.error("readQueue", e);
							}
							// PM dump bonifica
							try {
								executorService.execute(pmBonificaFilter
										.newFilterTask(context, messageJson));
							} catch (Exception e) {
								logger.error("readQueue", e);
							}

							try {
								executorService.execute(updateSchemiRipartoFilter
										.newFilterTask(context, messageJson));
							} catch (Exception e) {
								logger.error("readQueue", e);
							}
							
						} catch (Exception e) {
							logger.error("readQueue", e);
						}
						
					} catch (BadMessageException e) {
						logger.error("readQueue", e);
						
						// format failed message
						final JsonObject header = new JsonObject();
						header.addProperty("uuid", UUID.randomUUID().toString());
						header.addProperty("timestamp", Iso8601Helper.format(new Date()));
						header.addProperty("queue", serviceBusFailedQueue);
						header.addProperty("sender", "service-bus");
						header.addProperty("hostname", getHostName());
						final JsonObject error = new JsonObject();
						error.addProperty("code", "0");
						error.addProperty("description", e.getMessage());
						error.addProperty("stackTrace", TextUtils.printStackTrace(e));
						final JsonObject message = new JsonObject();
						message.add("header", header);
						if (null == messageJson) {
							message.addProperty("input", sqsMessage.text);
						} else {
							message.add("input", messageJson);			
						}
						message.add("error", error);
						
						// send failed message
						if (!sqs.sendMessage(serviceBusFailedQueueUrl, GsonUtils.toJson(message))) {
							throw new ExitApplicationException(String
									.format("error sending message to \"%s\"", serviceBusFailedQueue));																					
						}
						logger.debug("readQueue: message successfully sent to \"{}\"", serviceBusFailedQueue);
					}

					// delete message
					if (!sqs.deleteMessage(serviceBusQueueUrl, sqsMessage.receiptHandle)) {
						throw new ExitApplicationException(String
								.format("error deleting message from \"%s\"", serviceBusQueue));																					
					}
					logger.debug("readQueue: message {} deleted from \"{}\"",
							sqsMessage.receiptHandle, serviceBusQueue);
				}
			}
			
			logger.info("readQueue: exiting after {}", TextUtils
					.formatDuration(System.currentTimeMillis() - startTimeMillis));
		} 
	}

}
