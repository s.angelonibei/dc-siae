package com.alkemytech.sophia.servicebus.filter;

import java.sql.SQLException;
import java.util.Date;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.sqs.Iso8601Helper;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.google.inject.name.Named;

public class PmBonificaFilter implements BusFilter {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final SQS sqs;
	private final boolean enabled;
	private final String serviceBusQueueName;
	private final String sourceQueueName;
	private final String destinationQueueName;
	
	@Inject
	protected PmBonificaFilter(@Named("configuration") Properties configuration,
			SQS sqs) {
		super();
		this.sqs = sqs;
		this.enabled = Boolean.parseBoolean(configuration
				.getProperty("filter.pmbonifica.enabled", "true"));
		this.serviceBusQueueName = configuration
				.getProperty("filter.pmbonifica.queue.service_bus");
		this.sourceQueueName = configuration
				.getProperty("filter.pmbonifica.queue.source");
		this.destinationQueueName = configuration
				.getProperty("filter.pmbonifica.queue.destination");
		logger.debug("<init>: sourceQueueName " + sourceQueueName);
		logger.debug("<init>: destinationQueueName " + destinationQueueName);
	}
	
	@Override
	public Runnable newFilterTask(final Map<String, String> context, final JsonObject message) {
		return new Runnable() {
			@Override
			public void run() {
				try {
					process(context, message);
				} catch (Throwable e) {
					logger.error("newFilterTask", e);
				}
			}
		};
	}
	
	private void process(Map<String, String> context, JsonObject message) throws SQLException {
		// skip all uninteresting messages
		final String queueName = context.get("queue");
		if (!enabled || !sourceQueueName.equals(queueName)) {
			return;
		}
		// re-format message
		final JsonObject header = new JsonObject();
		header.addProperty("uuid", UUID.randomUUID().toString());
		header.addProperty("timestamp", Iso8601Helper.format(new Date()));
		header.addProperty("queue", destinationQueueName);
		header.addProperty("sender", "pmbonifica-filter");
		final JsonObject body = message.getAsJsonObject("output");
		message = new JsonObject();
		message.add("header", header);
		message.add("body", body);
		// dispatch message
		final String serviceBusQueueUrl = sqs.getUrl(serviceBusQueueName);
		if (null != serviceBusQueueUrl) {
			sqs.sendMessage(serviceBusQueueUrl, GsonUtils.toJson(message));
		}
	}
	
}
