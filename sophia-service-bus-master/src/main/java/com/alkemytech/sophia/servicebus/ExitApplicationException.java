package com.alkemytech.sophia.servicebus;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
@SuppressWarnings("serial")
public class ExitApplicationException extends Exception {

	public ExitApplicationException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ExitApplicationException(String message, Throwable cause) {
		super(message, cause);
	}

	public ExitApplicationException(String message) {
		super(message);
	}

	public ExitApplicationException(Throwable cause) {
		super(cause);
	}

}
