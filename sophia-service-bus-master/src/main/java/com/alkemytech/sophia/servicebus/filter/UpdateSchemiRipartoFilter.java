package com.alkemytech.sophia.servicebus.filter;

import com.alkemytech.sophia.commons.aws.SQSExtendedClient;
import com.alkemytech.sophia.commons.sqs.SqsMessageHelper;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class UpdateSchemiRipartoFilter implements BusFilter {

    private static final Logger logger = LoggerFactory.getLogger(UpdateSchemiRipartoFilter.class);

    private final Properties configuration;
    private final SQSExtendedClient sqs;
    private final boolean enabled;
    //private final List<String> sourceQueueNames;
    private final String sourceServiceName;
    private final List<String> dumpProcessNames;
    private final DataSource mcmdbDS;

    @Inject
    public UpdateSchemiRipartoFilter(@Named("configuration") Properties configuration, SQSExtendedClient sqs,
                                     @Named("MCMDB") DataSource mcmdbDS) {
        this.configuration = configuration;
        this.sqs = sqs;
        this.enabled = Boolean.parseBoolean(configuration.getProperty("filter.update_schemi_riparto.enabled"));
        //this.sourceQueueNames = Arrays.asList(configuration.getProperty("filter.update_schemi_riparto.queue.sources").split(","));
        this.sourceServiceName = configuration.getProperty("filter.update_schemi_riparto.queue.source.service_name");
        this.dumpProcessNames = Arrays.asList(configuration.getProperty("filter.update_schemi_riparto.dump_process_names").split(","));
        this.mcmdbDS = mcmdbDS;
    }

    @Override
    public Runnable newFilterTask(final Map<String, String> context, final JsonObject message) {
        return new Runnable() {

            @Override
            public void run() {
                final String queueType = context.get("queueType");
                final String serviceName = context.get("serviceName");
                final String queueName = context.get("queue");
                if (!enabled || !(dumpProcessNames.contains(serviceName) || sourceServiceName.equals(serviceName)) ) {
                    return;
                }

                JsonObject messageBody = GsonUtils.getAsJsonObject(message, "body");

                if (sourceServiceName.equals(serviceName) && queueType.equals("to_process")) {
                    String updateId = UUID.randomUUID().toString();

                    SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration,
                            "filter.update_schemi_riparto.sqs.self");
                    JsonObject inputMessage = GsonUtils.deepCopy(message);
                    inputMessage.getAsJsonObject("body")
                            .addProperty("updateId", updateId);
                    sqsMessagePump.sendStartedMessage(inputMessage, SqsMessageHelper.formatContext(), false);

                    for (String dumpProcessName : dumpProcessNames) {
                        SqsMessagePump sqsMessagePumpDumpProcess = new SqsMessagePump(sqs, configuration,
                                "filter.update_schemi_riparto.sqs." + dumpProcessName);

                        String body = configuration.getProperty("filter.update_schemi_riparto.sqs." + dumpProcessName + ".body_template")
                                .replace("{year}", GsonUtils.getAsString(messageBody, "year"))
                                .replace("{month}", GsonUtils.getAsString(messageBody, "month"))
                                .replace("{updateId}", updateId);

                        sqsMessagePumpDumpProcess.sendToProcessMessage(GsonUtils.fromJson(body, JsonObject.class), false);
                    }
                    return;
                }

                if (dumpProcessNames.contains(serviceName) &&
                        (queueType.equals("completed") || queueType.equals("failed"))) {
                    final JsonObject input = GsonUtils.getAsJsonObject(message, "input");
                    final JsonObject inputBody = GsonUtils.getAsJsonObject(input, "body");
                    final String updateId = GsonUtils.getAsString(inputBody, "updateId");

                    try (final Connection mcmdb = mcmdbDS.getConnection()) {

                        String sql = configuration.getProperty("filter.update_schemi_riparto.sql.check_executions")
                                .replace("{services}", quotedStrings(dumpProcessNames))
                                .replace("{updateId}", updateId);
                        final ResultSet resultSet = mcmdb.createStatement().executeQuery(sql);

                        List<Map<String, String>> list = new ArrayList<>();
                        int completed = 0, failed = 0;
                        while (resultSet.next()) {
                            final String service_name = resultSet.getString("SERVICE_NAME");
                            final String queue_type = resultSet.getString("QUEUE_TYPE");

                            switch (queue_type) {
                                case "completed":
                                    completed++;
                                    break;
                                case "failed":
                                    failed++;
                                    break;
                            }

                            list.add(new HashMap<String, String>() {{
                                put("SERVICE_NAME", service_name);
                                put("QUEUE_TYPE", queue_type);
                            }});
                        }

                        if (list.size() == dumpProcessNames.size()) {
                            SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration,
                                    "filter.update_schemi_riparto.sqs.self");

                            ResultSet inputMessageRs = mcmdb.createStatement()
                                    .executeQuery(configuration.getProperty("filter.update_schemi_riparto.sql.find_input_message")
                                            .replace("{updateId}", updateId));
                            inputMessageRs.first();
                            JsonObject inputMessage = GsonUtils.fromJson(inputMessageRs.getString("INPUT_MESSAGE"), JsonObject.class);

                            if (failed > 0) {
                                sqsMessagePump.sendFailedMessage(inputMessage, null, false);
                            } else if (dumpProcessNames.size() == completed) {
                                sqsMessagePump.sendCompletedMessage(inputMessage, null, false);
                            }
                        } else {
                            return;
                        }

                    } catch (SQLException e) {
                        logger.error("UpdateSchemiRipartoFilter: ", e);
                    }

                }


            }
        };
    }

    private String quotedStrings(final List<String> values) {
        List<String> quotedValues = new ArrayList<>();
        for (String value : values) {
            quotedValues.add("'" + value + "'");
        }
        return StringUtils.join(quotedValues, ",");
    }
}
