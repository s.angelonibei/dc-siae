package com.alkemytech.sophia.servicebus.tools;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SendMail {
    
    public enum TIPO_MAIL{
        CLAIM_FINITO,
        CLAIM_ERRORE,
        CLAIM_NEGATIVO
    }
    
    
    public static void sendMailDsr(List<String> destinatari, TIPO_MAIL tipo_mail, String iddsr,String muleUrl) throws Exception{

        JsonArray array = new JsonArray();

        for (String s : destinatari) {
            JsonObject obj = new JsonObject();
            obj.addProperty("email",s);
            array.add(obj);
        }
        
        String messageBody = "";
        String titolo = "";
        
        String emailListJson ="";
        
        switch (tipo_mail){
            case CLAIM_FINITO:
                messageBody =  "Il claim per il dsr " + iddsr + " e' terminato con esito POSITIVO";
                titolo =  "CLAIM completato "+ iddsr;
                break;
            case CLAIM_ERRORE:
                messageBody =  "Il claim per il dsr " + iddsr + " e' in ERRORE";
                titolo =  "CLAIM errato "+ iddsr;
                break;
            case CLAIM_NEGATIVO:
                messageBody =  "Il claim per il dsr " + iddsr + " e' terminato con importi NEGATIVI";
                titolo =  "CLAIM negativo "+ iddsr;
                break;
        }
        
        String curl_da_copiare="curl --insecure -X POST "
                + muleUrl 
                + " -H 'cache-control: no-cache'   -H 'content-type: application/x-www-form-urlencoded'  "
                + " -d 'codiceAbbonamento=12345678&isHtml=0&mailSender=noreply%40siae.it&messageBody="
                + URLEncoder.encode(messageBody,java.nio.charset.StandardCharsets.UTF_8.toString())
                +"&subject="
                + URLEncoder.encode(titolo,java.nio.charset.StandardCharsets.UTF_8.toString())
                +"&toList="
                + URLEncoder.encode(array.toString(),java.nio.charset.StandardCharsets.UTF_8.toString())
                +"'";

        ProcessBuilder processBuilder = new ProcessBuilder(curl_da_copiare.split(" "));
        Process process = processBuilder.start();
        
        
    }
    
    
    
    private static void sendMailBrutto(){
        
    }

    public static void main(String[] args) throws Exception{
        
        sendMailDsr(Arrays.asList("marco.galiffa@alkemy.com","eleonora.rinaldi@alkemy.com"),TIPO_MAIL.CLAIM_FINITO,"IDDSRBELLO_FINitO_OK","https://muletst.ws.siae.it/mdaServices/mule/sendMail");
        sendMailDsr(Arrays.asList("marco.galiffa@alkemy.com","eleonora.rinaldi@alkemy.com"),TIPO_MAIL.CLAIM_ERRORE,"IDDSRBELLO_ERRORE","https://muletst.ws.siae.it/mdaServices/mule/sendMail");
        sendMailDsr(Arrays.asList("marco.galiffa@alkemy.com","eleonora.rinaldi@alkemy.com"),TIPO_MAIL.CLAIM_NEGATIVO,"IDDSRBELLO_NEGATIVO","https://muletst.ws.siae.it/mdaServices/mule/sendMail");
    }
}
