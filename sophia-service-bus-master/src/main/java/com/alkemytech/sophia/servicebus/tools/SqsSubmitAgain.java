package com.alkemytech.sophia.servicebus.tools;

import java.io.BufferedReader;
import java.io.FileReader;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.sqs.Iso8601Helper;
import com.alkemytech.sophia.servicebus.GuiceModuleExtension;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
@SuppressWarnings("unused")
public class SqsSubmitAgain {

	private static final Logger logger = LoggerFactory.getLogger(SqsSubmitAgain.class);

	public static void main(String[] args) throws Exception {
		try {	
			final Injector injector = Guice
					.createInjector(new GuiceModuleExtension(args, "/configuration.properties"));
			injector.getInstance(SQS.class).startup();
			//injector.getInstance(S3.class).startup();
			try {
				injector.getInstance(SqsSubmitAgain.class)
					.sendAgain();
			} finally {
				injector.getInstance(SQS.class).shutdown();
				//injector.getInstance(S3.class).shutdown();
			}
		} catch (Exception e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}

	private final SQS sqs;
	private final DataSource mcmdbDS;

	@Inject
	protected SqsSubmitAgain(SQS sqs,
			@Named("MCMDB") DataSource mcmdbDS) {
		super();
		this.sqs = sqs;
		this.mcmdbDS = mcmdbDS;
	}
	
	private String getHostName() {
		try {
			return InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			return "localhost";
		}
	}
	
	public void sendAgain() throws Exception {
			
		System.out.println("opening connection...");

		try (final Connection connection = mcmdbDS.getConnection()) {
			logger.debug("sendAgain: connected to {} {}", connection.getMetaData()
					.getDatabaseProductName(), connection.getMetaData().getURL());

			System.out.println("opened");

			// gson
			final Gson gson = new GsonBuilder()
				.disableHtmlEscaping()
				.setPrettyPrinting()
				.create();


			ArrayList<String> iddsrs = new ArrayList<String>();
			try (final FileReader reader = new FileReader("/Users/roberto/Downloads/iddsr.txt");
					final BufferedReader in = new BufferedReader(reader)) {
				for (String line; null != (line = in.readLine()); ) {
					line = line.trim();
					if (!Strings.isNullOrEmpty(line)) {
						iddsrs.add(line);
					}
				}
			}
			System.out.println(iddsrs.size());
			System.out.println(iddsrs);
			
			
			
//			if (false) {
//				
//				final Map<String, String> messages = new HashMap<String, String>();
//				
//				final String sql = new StringBuilder()
//				.append("select iddsr, message_json from SQS_SERVICE_BUS")
//				.append(" where service_name = 'unidentified'")
//				.append("   and queue_type = 'completed'")
//				.append("   order by insert_time desc")
//				.append("")
//				.toString();
//				try (final Statement statement = connection.createStatement()) {
//					try (final ResultSet resultSet = statement.executeQuery(sql)) {
//						while (resultSet.next()) {
//							final String idDsr = resultSet.getString(1);
//							if (!messages.containsKey(idDsr)) {
//								final String messageJson = resultSet.getString(2);
//								messages.put(idDsr, messageJson);
//							}
//						}
//					}
//				}
//				logger.debug("sendAgain: messages {}", messages);
//				
//				class DsrIT {
//					String idDsr;
//					long count;
//				}
//				final List<DsrIT> dsrits = new ArrayList<DsrIT>();
//				for (Map.Entry<String, String> entry : messages.entrySet()) {
//					final JsonObject json = gson.fromJson(entry.getValue(), JsonObject.class);
//					final JsonObject output = json.getAsJsonObject("output");
//					final long italianISRCs = Long.parseLong(output.get("italianISRCs").getAsString());
//					final long italianTitles = Long.parseLong(output.get("italianTitles").getAsString());
//					DsrIT dsrit = new DsrIT();
//					dsrit.idDsr = entry.getKey();
//					dsrit.count = italianISRCs + italianTitles;
//					dsrits.add(dsrit);
//				}
//				Collections.sort(dsrits, new Comparator<DsrIT>() {
//					@Override
//					public int compare(DsrIT o1, DsrIT o2) {
//						return -Long.compare(o1.count, o2.count);
//					}
//				});
//				
//				for (DsrIT dsrit : dsrits) {
//					System.out.println(dsrit.idDsr + " " + dsrit.count);
//				}
//				System.exit(0);
//				
//			}

			// data riferimento invio messaggi '2017-06-01'
			// data inizio invio messaggi '2017-06-11 19:00:00'
			// data inizio invio messaggi '2017-06-12 13:00:00' -- fallimentare, data filtro sbagliata
			// data inizio invio messaggi '2017-06-12 13:10:00'
			// data inizio invio messaggi '2017-06-14 10:50:00'
			
			String refDate = "2017-06-11 19:00:00";
			
			// sql
			final String sql0 = new StringBuilder()
				.append("select a.environment, a.service_name, a.message_json, a.iddsr")
				.append("  from SQS_SERVICE_BUS a, DISTRIBUTION_AMOUNTS b")
				.append(" where a.queue_type = 'failed'")
				.append("   and a.insert_time >= '" + refDate + "'")
				.append("   and a.service_name = 'extraction_and_validation'")
				.append("   and a.IDDSR = b.IDDSR")
				.toString();
			
			final String sql1 = new StringBuilder()
				.append("select a.environment, a.service_name, a.message_json, a.iddsr")
				.append("  from SQS_SERVICE_BUS a, DISTRIBUTION_AMOUNTS b")
				.append(" where a.queue_type = 'failed'")
				.append("   and a.insert_time >= '" + refDate + "'")
				.append("   and a.service_name = 'cleaning_and_normalization'")
				.append("   and a.IDDSR = b.IDDSR")
				.toString();
			
			final String sql2 = new StringBuilder()
				.append("select a.environment, a.service_name, a.message_json, a.iddsr")
				.append("  from SQS_SERVICE_BUS a, DISTRIBUTION_AMOUNTS b")
				.append(" where a.queue_type = 'failed'")
				.append("   and a.insert_time >= '" + refDate + "'")
				.append("   and a.service_name = 'identification'")
				.append("   and a.IDDSR = b.IDDSR")
				.toString();
			
			// month 'Q' + period 
			final String sql3 = new StringBuilder()
				.append("select a.environment, a.service_name, a.message_json, a.iddsr")
				.append("  from SQS_SERVICE_BUS a, DISTRIBUTION_AMOUNTS b")
				.append(" where a.queue_type = 'failed'")
				.append("   and a.insert_time >= '" + refDate + "'")
				.append("   and a.service_name = 'claim'")
				.append("   and a.message_json not like '%sum of compenso%'")
				.append("   and a.message_json not like '%le tabelle prod_sophia_raw_source%'")
				.append("   and a.message_json like '%prod_priced_lines is empty%'")
				.append("   and a.IDDSR = b.IDDSR")
				.toString();

			final String sql4 = new StringBuilder()
				.append("select a.environment, a.service_name, a.message_json, a.iddsr")
				.append("  from SQS_SERVICE_BUS a, DISTRIBUTION_AMOUNTS b")
				.append(" where a.queue_type = 'failed'")
				.append("   and a.insert_time >= '" + refDate + "'")
				.append("   and a.service_name = 'claim'")
				.append("   and a.message_json not like '%sum of compenso%'")
				.append("   and a.message_json not like '%le tabelle prod_sophia_raw_source%'")
				.append("   and a.message_json not like '%prod_priced_lines is empty%'")
				.append("   and a.IDDSR = b.IDDSR")
				.toString();
			
			final String sql5 = new StringBuilder()
				.append("select a.environment, a.service_name, a.message_json, a.iddsr")
				.append("  from SQS_SERVICE_BUS a")
				.append(" where a.queue_type = 'to_process'")
				.append("   and a.service_name = 'cleaning_and_normalization'")
				.append("   and a.insert_time >= '2017-06-12 00:00:00'")
				.append("   and not exists (")
				.append("      select x.iddsr from SQS_SERVICE_BUS x ")
				.append("       where x.insert_time > '2017-06-12 00:00:00'")
				.append("         and x.service_name = 'identification'")
				.append("         and x.queue_type = 'to_process'")
				.append("         and x.IDDSR = a.IDDSR)")
				.toString();

			final StringBuilder sql6 = new StringBuilder()
				.append("select a.environment, a.service_name, a.message_json, a.iddsr")
				.append("  from SQS_SERVICE_BUS a")
				.append(" where a.queue_type = 'to_process'")
				.append("   and a.service_name = 'pricing'")
				.append("   and a.iddsr in (");
			sql6.append("'").append(iddsrs.get(0)).append("'");
			for (int i = 1; i < iddsrs.size(); i ++) {
				sql6.append(",'").append(iddsrs.get(i)).append("'");
			}
			sql6.append(")   order by a.insert_time desc");			
			
			Set<String> unique = new HashSet<String>();
			
			String sql = sql6.toString();
			int count = 0;
//			System.out.println(sql);
			
			final String serviceBusQueueUrl = sqs
					.getOrCreateUrl("prod_service_bus");
//					.getOrCreateUrl("prod_to_process_identification");
			
			try (final Statement statement = connection.createStatement()) {
				try (final ResultSet resultSet = statement.executeQuery(sql)) {
					while (resultSet.next()) {
						final String environment = resultSet.getString(1);
						final String serviceName = resultSet.getString(2);
						final String messageJson = resultSet.getString(3);
						final String idDsr = resultSet.getString(4);
						
						// skip duplicates
						if (unique.contains(idDsr)) {
							continue;
						}
						unique.add(idDsr);
//						System.out.println(idDsr);
						
						JsonObject jsonObject = gson.fromJson(messageJson, JsonObject.class);
//						jsonObject = jsonObject.get("input").getAsJsonObject();
						if (jsonObject.has("body")) {
//							final JsonObject input = jsonObject.getAsJsonObject("input");
//							final JsonObject header = input.getAsJsonObject("header");
							final JsonObject header = jsonObject.getAsJsonObject("header");
							header.addProperty("uuid", UUID.randomUUID().toString());
							header.addProperty("timestamp", Iso8601Helper.format(new Date()));
							header.addProperty("hostname", getHostName());
//							header.addProperty("queue", environment + "_to_process_" + serviceName);
//							header.addProperty("queue", "prod_to_process_identification");
							
//							final JsonObject body = jsonObject.getAsJsonObject("body");
//							if (null != body) {
//
//								final JsonObject config = body.getAsJsonObject("config");
//								
//								if (null != config) {
//									String periodType = config.get("dsr_period_type").getAsString();
//									if ("quarter".equalsIgnoreCase(periodType)) {
//										body.addProperty("month", config.get("quarter").getAsString());
//										config.addProperty("month", config.get("quarter").getAsString());
//									}
//								} else {
//									String periodType = body.get("periodType").getAsString();
//									if ("quarter".equalsIgnoreCase(periodType)) {
//										body.addProperty("month", "Q" + body.get("period").getAsString());
//									}									
//								}
//
//								
//							}

//							System.out.println(gson.toJson(input));
							System.out.println(gson.toJson(jsonObject));
//							System.out.println("count " + count);
							System.out.println();
							
							//sqs.sendMessage(serviceBusQueueUrl, gson.toJson(jsonObject));
							count ++;
						}
						
					}
				}
			}
			
			System.out.println("sendAgain: sent " + count + " messages");

			System.out.println("sendAgain: unique size " +unique.size());
			iddsrs.removeAll(unique);
			System.out.println("sendAgain: not found " + iddsrs);

			logger.debug("sendAgain: sent {} messages", count);
			logger.debug("sendAgain: finished");
			
		}
	}
	
}
