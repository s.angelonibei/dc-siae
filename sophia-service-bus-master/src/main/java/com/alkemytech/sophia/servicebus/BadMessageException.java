package com.alkemytech.sophia.servicebus;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
@SuppressWarnings("serial")
public class BadMessageException extends Exception {

	public BadMessageException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public BadMessageException(String message, Throwable cause) {
		super(message, cause);
	}

	public BadMessageException(String message) {
		super(message);
	}

	public BadMessageException(Throwable cause) {
		super(cause);
	}

}
