package com.alkemytech.sophia.servicebus.filter;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.Properties;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.google.inject.name.Named;

public class ClusterMonitoringFilter implements BusFilter {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Properties configuration;
	private final DataSource mcmdbDS;
	private final boolean enabled;
	
	@Inject
	protected ClusterMonitoringFilter(@Named("configuration") Properties configuration,
			@Named("MCMDB") DataSource mcmdbDS) {
		super();
		this.configuration = configuration;
		this.mcmdbDS = mcmdbDS;
		this.enabled = Boolean.parseBoolean(configuration
				.getProperty("filter.cluster_monitoring.enabled", "true"));
		logger.debug("<init>: enabled " + enabled);
	}
	
	@Override
	public Runnable newFilterTask(final Map<String, String> context, final JsonObject message) {
		return new Runnable() {
			@Override
			public void run() {
				try {
					process(context, message);
				} catch (Throwable e) {
					logger.error("newFilterTask", e);
				}
			}
		};
	}
	
	private void process(Map<String, String> context, JsonObject message) throws SQLException {
		// skip all uninteresting messages
		final String idDsr = context.get("idDsr"); 
		final String queueType = context.get("queueType"); 
		final String serviceName = context.get("serviceName");
		final String hostname = context.get("hostname"); 
		final String applicationId = context.get("applicationId"); 
		if (!enabled || Strings.isNullOrEmpty(queueType) ||
				Strings.isNullOrEmpty(serviceName) ||
				Strings.isNullOrEmpty(hostname)) {
			return;
		}

		final String updateSql = configuration
				.getProperty("filter.cluster_monitoring.sql.update")
				.replace("{queueType}", queueType)
				.replace("{serviceName}", serviceName)
				.replace("{hostname}", hostname)
				.replace("{idDsr}", null == idDsr ? "" : idDsr)
				.replace("{applicationId}", null == applicationId ? "" : applicationId);
		
		// process message 
		try (final Connection mcmdb = mcmdbDS.getConnection()) {
			logger.debug("process: connected to {} {}", mcmdb.getMetaData()
					.getDatabaseProductName(), mcmdb.getMetaData().getURL());
			mcmdb.setAutoCommit(true);
			try (final Statement stmt = mcmdb.createStatement()) {
				logger.debug("process: executing sql {}", updateSql);
				int count = stmt.executeUpdate(updateSql);
				logger.debug("process: updated rows {}", count);
				if (1 == count)
					return;
				// zero rows updated, try using insert
				final String insertSql = configuration
						.getProperty("filter.cluster_monitoring.sql.insert")
						.replace("{idDsr}", null == idDsr ? "" : idDsr)
						.replace("{queueType}", queueType)
						.replace("{serviceName}", serviceName)
						.replace("{hostname}", hostname)
						.replace("{applicationId}", null == applicationId ? "" : applicationId);
				if (Strings.isNullOrEmpty(insertSql))
					return;
				logger.debug("process: executing sql {}", insertSql);
				count = stmt.executeUpdate(insertSql);
				logger.debug("process: inserted rows {}", count);
			}
		}
	}
	
}
