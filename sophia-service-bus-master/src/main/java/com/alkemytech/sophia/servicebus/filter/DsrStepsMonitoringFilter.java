package com.alkemytech.sophia.servicebus.filter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.sql.DataSource;

import com.alkemytech.sophia.servicebus.tools.SendMail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.google.inject.name.Named;

public class DsrStepsMonitoringFilter implements BusFilter {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Properties configuration;
	private final DataSource mcmdbDS;
	private final boolean enabled;
	private final Set<String> services;
	private final String muleMailUrl;

	private static final String CLAIM_ERRORE="1";
	
	@Inject
	protected DsrStepsMonitoringFilter(@Named("configuration") Properties configuration,
			@Named("MCMDB") DataSource mcmdbDS) {
		super();
		this.configuration = configuration;
		this.mcmdbDS = mcmdbDS;
		this.enabled = Boolean.parseBoolean(configuration
				.getProperty("filter.dsr_steps_monitoring.enabled", "true"));
		this.services = new HashSet<>(Arrays.asList(configuration
				.getProperty("filter.dsr_steps_monitoring.services", "").split(",")));
		this.muleMailUrl=configuration
				.getProperty("mule.mail.url", "");
		logger.debug("<init>: enabled " + enabled);
		logger.debug("<init>: services " + services);
	}
	
	@Override
	public Runnable newFilterTask(final Map<String, String> context, final JsonObject message) {
		return new Runnable() {
			@Override
			public void run() {
				try {
					process(context, message);
				} catch (Throwable e) {
					logger.error("newFilterTask", e);
				}
			}
		};
	}
	
	private void process(Map<String, String> context, JsonObject message) throws SQLException {
		// skip all uninteresting messages
		final String idDsr = context.get("idDsr"); 
		final String queueType = context.get("queueType"); 
		final String serviceName = context.get("serviceName");
		if (!enabled || Strings.isNullOrEmpty(queueType) ||
				Strings.isNullOrEmpty(serviceName) ||
				Strings.isNullOrEmpty(idDsr)) {
			return;
		}
		if (!services.contains(serviceName))
			return;
		
		
		String kbVersion = "-1";
		
		try{
			if(queueType.equalsIgnoreCase("completed") && serviceName.equalsIgnoreCase("newidentification"))
			kbVersion = message.getAsJsonObject("output")
					.getAsJsonArray("items").get(0).getAsJsonObject().get("version").getAsString();
            logger.error("VERSION:" + kbVersion);
		}catch(Exception e){}

		
		String errorMessage = "";
		try{
			if(queueType.equalsIgnoreCase("failed")) {
				errorMessage = serviceName+ ": ";
				errorMessage += message.getAsJsonObject("error").get("description").getAsString();
				
				try {
					if(serviceName.equalsIgnoreCase("extraction_and_normalization") 
							|| serviceName.equalsIgnoreCase("sophia_tools")
							|| serviceName.equalsIgnoreCase("extraction_and_validation")
							|| serviceName.equalsIgnoreCase("cleaning_and_normalization")
							|| serviceName.equalsIgnoreCase("extract_normalize")
							|| serviceName.equalsIgnoreCase("identification")
							|| serviceName.equalsIgnoreCase("newidentification")
							|| serviceName.equalsIgnoreCase("newidentification_ra")
							|| serviceName.equalsIgnoreCase("pricing")
							|| serviceName.equalsIgnoreCase("claim")
							|| serviceName.equalsIgnoreCase("pricingclaim")) {

						String emailSql = null;
							if(serviceName.equalsIgnoreCase("sophia_tools")){
								emailSql = configuration
										.getProperty("notifica_mail_all.sql")
										.replace("{tipo}", CLAIM_ERRORE);
							}else{
								emailSql = configuration
										.getProperty("notifica_mail.sql")
										.replace("{dsp}", context.get("idDsp"))
										.replace("{tipo}", CLAIM_ERRORE);
							}

						String indirizziMail = "";
						try (final Connection mcmdb = mcmdbDS.getConnection()) {
							logger.debug("process: connected to {} {}", mcmdb.getMetaData()
									.getDatabaseProductName(), mcmdb.getMetaData().getURL());
							mcmdb.setAutoCommit(false);
							try (final Statement statement = mcmdb.createStatement()) {
								try (final ResultSet rset = statement.executeQuery(emailSql)) {
									if (rset.next())
										indirizziMail = rset.getString(1);
								}
							}
						}
						//sendMail CLAIM ERROR
						try {
							SendMail.sendMailDsr(Arrays.asList(indirizziMail.split(",")), SendMail.TIPO_MAIL.CLAIM_ERRORE, idDsr, muleMailUrl);
						}catch (Exception e){e.printStackTrace();}
					}

					
				}catch(Exception e){}
			
			}
		}catch(Exception e){}
		
		
		final String updateSql = configuration
				.getProperty("filter.dsr_steps_monitoring.sql.update." + serviceName, "")
				.replace("{queueType}", queueType)
				.replace("{idDsr}", idDsr)
				.replace("{kbVersion}", kbVersion)
				.replace("{errorMessage}", errorMessage);
		if (Strings.isNullOrEmpty(updateSql))
			return;
		
		// process message 
		try (final Connection mcmdb = mcmdbDS.getConnection()) {
			logger.debug("process: connected to {} {}", mcmdb.getMetaData()
					.getDatabaseProductName(), mcmdb.getMetaData().getURL());
			mcmdb.setAutoCommit(true);
			try (final Statement stmt = mcmdb.createStatement()) {				
				logger.debug("process: executing sql {}", updateSql);
				int count = stmt.executeUpdate(updateSql);
				logger.debug("process: updated rows {}", count);
				if (1 == count)
					return;
				// zero rows updated, try using insert
				final String insertSql = configuration
						.getProperty("filter.dsr_steps_monitoring.sql.insert." + serviceName, "")
						.replace("{queueType}", queueType)
						.replace("{idDsr}", idDsr);
				if (Strings.isNullOrEmpty(insertSql))
					return;
				logger.debug("process: executing sql {}", insertSql);
				count = stmt.executeUpdate(insertSql);
				logger.debug("process: inserted rows {}", count);
			}
		}
	}

//	private void process(Map<String, String> context, JsonObject message) throws SQLException {
//		// skip all uninteresting messages
//		final String idDsr = context.get("idDsr"); 
//		final String queueType = decodeType.get(context.get("queueType")); 
//		final String serviceName = decodeService.get(context.get("serviceName"));
//		if (!enabled || null == queueType || null == serviceName ||
//				Strings.isNullOrEmpty(idDsr)) {
//			return;
//		}
//		// process message 
//		try (final Connection mcmdb = mcmdbDS.getConnection()) {
//			logger.debug("process: connected to {} {}", mcmdb.getMetaData()
//					.getDatabaseProductName(), mcmdb.getMetaData().getURL());
//			mcmdb.setAutoCommit(true);
//
//			final Date timestamp = Iso8601Helper.parse(context.get("timestamp"));
//			final String status = decodeStatus.get(context.get("queueType"));
//
//			// update DSR_STEPS_MONITORING
//			StringBuilder sql = new StringBuilder()
//				.append("update DSR_STEPS_MONITORING")
////				.append(" set LAST_UPDATE = CURRENT_TIMESTAMP(3)") // WARNING: ... ON UPDATE CURRENT_TIMESTAMP(3) ...
//				.append(" set ").append(serviceName).append("_").append(queueType).append(" = ?");
//			if (null != status) {
//				sql.append(", ").append(serviceName).append("_STATUS = ?");
//			}
//			sql.append(" where IDDSR = ?");			
//			logger.debug("process: sql " + sql);
//			try (final PreparedStatement statement = mcmdb.prepareStatement(sql.toString())) {
//				int i = 1;
//				statement.setTimestamp(i ++, new Timestamp(timestamp.getTime()));
//				if (null != status) {
//					statement.setString(i ++, status);
//				}
//				statement.setString(i ++, idDsr);
//				final int count = statement.executeUpdate();
//				logger.debug("process: " + count + " row(s) updated");
//				if (1 == count) {
//					return;
//				}
//			}
//
//			// insert into DSR_STEPS_MONITORING
//			sql = new StringBuilder()
//				.append("insert into DSR_STEPS_MONITORING")
//				.append("(IDDSR")
////				.append(", LAST_UPDATE") // WARNING: ... DEFAULT CURRENT_TIMESTAMP(3) ...
//				.append(", ").append(serviceName).append("_").append(queueType);
//			if (null != status) {
//				sql.append(", ").append(serviceName).append("_STATUS")
//					.append(") values (?, ?, ?)");
//			} else {
//				sql.append(") values (?, ?)");
//			}
//			logger.debug("process: sql " + sql);
//			try (final PreparedStatement statement = mcmdb.prepareStatement(sql.toString())) {
//				int i = 1;
//				statement.setString(i ++, idDsr);
//				statement.setTimestamp(i ++, new Timestamp(timestamp.getTime()));
//				if (null != status) {
//					statement.setString(i ++, status);
//				}
//				final int count = statement.executeUpdate();
//				logger.debug("process: " + count + " row(s) inserted");
//			}
//		}
//	}
	
}
