package com.alkemytech.sophia.servicebus.s3queue.jdbc;


import com.alkemytech.sophia.commons.jdbc.DBCP2DataSource;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class McmdbDataSource extends DBCP2DataSource {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Inject
	public McmdbDataSource(@Named("configuration") Properties configuration,
			@Named("mcmdb_data_source") String propertyPrefix) {
		super(configuration, propertyPrefix);
		logger.debug("propertyPrefix: {}", propertyPrefix);
	}

}


