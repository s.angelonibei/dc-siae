package com.alkemytech.sophia.servicebus.s3queue;

import com.google.inject.Inject;
import com.google.inject.name.Named;

import java.util.Enumeration;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class Configuration {
		
	private final Properties configuration;
	private final Map<String, String> tags;

	@Inject
	protected Configuration(@Named("configuration") Properties configuration) {
		super();
		this.configuration = configuration;
		this.tags = new ConcurrentHashMap<>();
	}

	public Configuration setTag(String key, String value) {
		tags.put(key, value);
		return this;
	}
	
	public Configuration removeTag(String key) {
		tags.remove(key);
		return this;
	}

	public Properties getPropertiesUnchanged() {
		return configuration;
	}
	
	public Properties getProperties() {
		final Properties properties = new Properties();
		final Enumeration<?> paramNames = configuration.propertyNames(); // WARNING: include defaults
		while (paramNames.hasMoreElements()) {
			final String paramName = paramNames.nextElement().toString();
			String paramValue = configuration.getProperty(paramName);
			for (Map.Entry<String, String> tag : tags.entrySet()) {
				paramValue = paramValue.replace(tag.getKey(), tag.getValue());
			}
			properties.setProperty(paramName, paramValue);
		}
		return properties;
	}
	
	public String getProperty(String paramName) {
		return getProperty(paramName, null);
	}

	public String getProperty(String paramName, String ifNull) {
		String paramValue = configuration.getProperty(paramName, ifNull);
		if (null != paramValue) {
			for (Map.Entry<String, String> tag : tags.entrySet()) {
				paramValue = paramValue.replace(tag.getKey(), tag.getValue());
			}
		}
		return paramValue;
	}

	public void setProperty(String paramName, String paramValue) {
		configuration.setProperty(paramName, paramValue);
	}
}