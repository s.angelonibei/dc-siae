package com.alkemytech.sophia.servicebus.filter;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;

import com.alkemytech.sophia.commons.guice.ConfigurationLoader;
import com.alkemytech.sophia.servicebus.McmdbDataSource;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class CcidFilterTest {
	
	private DataSource datasource;

	private Properties configuration;
	
	private MultiTenantCcidMetadataFilter ccidMetadataFilter;
	
	@Before
	public void setUp() throws Exception {
		Locale.setDefault(Locale.US);
		final Properties defaults = new ConfigurationLoader().withResourceName("/defaults.properties").load();
		configuration = new ConfigurationLoader().withResourceName("/configuration.properties").load(defaults);
		ConfigurationLoader.expandVariables(configuration);
		datasource = new McmdbDataSource(configuration, configuration.getProperty("mcmdb_data_source"));
		ccidMetadataFilter = new MultiTenantCcidMetadataFilter(configuration, datasource);
	}

	@Test
	public void test() {
		Map<String, String> context = new HashMap<String, String>();							
		context.put("queueType", "completed");							
		context.put("serviceName", "pricingclaim");
		JsonParser parser = new JsonParser();
		JsonElement jsonTree = parser.parse(new BufferedReader(new InputStreamReader(
                this.getClass().getResourceAsStream("/" + "message-new.json"))));
		JsonObject message = jsonTree.getAsJsonObject();
		ccidMetadataFilter.newFilterTask(context, message).run();
	}

}
