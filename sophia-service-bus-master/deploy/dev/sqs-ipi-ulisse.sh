#!/bin/sh

ENVNAME=dev
APPNAME=sqs-ipi-ulisse
VERSION=1.0.2

JARNAME=sophia-service-bus-$VERSION-jar-with-dependencies.jar
CFGNAME=sophia-service-bus.properties
RUNNAME=logs/crontab.log
HOME=/var/local/sophia/$ENVNAME
#OUTPUT=/dev/null
OUTPUT=$HOME/logs/$APPNAME.out

nohup java -cp $HOME/$JARNAME com.alkemytech.sophia.servicebus.tools.SqsIpiUlisse $HOME/$CFGNAME 2>&1 >> $OUTPUT &

echo "$(date +%Y%m%d%H%M%S) $ENVNAME $APPNAME" >> $HOME/$RUNNAME

