#!/bin/sh

ENVNAME=dev
APPNAME=sophia-service-bus
VERSION=1.0.2

JARNAME=$APPNAME-$VERSION-jar-with-dependencies.jar
CFGNAME=$APPNAME.properties
RUNNAME=logs/crontab.log
HOME=/var/local/sophia/$ENVNAME
#OUTPUT=/dev/null
OUTPUT=$HOME/logs/$APPNAME.out

nohup /opt/java8/bin/java -jar $HOME/$JARNAME $HOME/$CFGNAME 2>&1 >> $OUTPUT &

echo "$(date +%Y%m%d%H%M%S) $ENVNAME $APPNAME" >> $HOME/$RUNNAME

