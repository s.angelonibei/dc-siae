#!/bin/sh

ENVNAME=test
APPNAME=sophia-service-bus
VERSION=1.0.5

JARNAME=$APPNAME-$VERSION-jar-with-dependencies.jar
CFGNAME=$APPNAME.properties
#HOME=/var/local/sophia/$ENVNAME
HOME=/var/local/sophia/test
OUTPUT=/dev/null
#OUTPUT=/home/orchestrator/logs/$APPNAME.out
RUNLOG=/var/local/sophia/test/logs/crontab.log

echo "$(date +%Y%m%d%H%M%S) $ENVNAME $APPNAME" >> $OUTPUT

nohup /opt/java8/bin/java -jar $HOME/$JARNAME $HOME/$CFGNAME 2>&1 >> $OUTPUT &

echo "$(date +%Y%m%d%H%M%S) $ENVNAME $APPNAME" >> $RUNLOG
