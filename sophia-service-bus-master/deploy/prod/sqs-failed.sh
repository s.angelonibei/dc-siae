#!/bin/sh

ENVNAME=prod
APPNAME=sqs-failed
VERSION=1.0.3

JARNAME=sophia-service-bus-$VERSION-jar-with-dependencies.jar
CFGNAME=$APPNAME.properties
#HOME=/var/local/sophia/$ENVNAME
HOME=/home/orchestrator/sophia/servicebus
OUTPUT=/dev/null
#OUTPUT=/home/orchestrator/logs/$APPNAME.out
RUNLOG=/home/orchestrator/logs/crontab.log

echo "$(date +%Y%m%d%H%M%S) $ENVNAME $APPNAME" >> $OUTPUT

nohup /opt/java8/bin/java -cp $HOME/$JARNAME com.alkemytech.sophia.servicebus.tools.SqsFailed $HOME/$CFGNAME 2>&1 >> $OUTPUT &

echo "$(date +%Y%m%d%H%M%S) $ENVNAME $APPNAME" >> $RUNLOG
