# configuration.properties

mule.mail.url=https://muleprd.ws.siae.it/mdaServices/mule/sendMail
default.environment=prod
default.environment_dtp=prod
default.bind_port=12345
default.debug=true
default.home_folder=/home/orchestrator/sophia/servicebus
log4j.configuration=${default.home_folder}/log4j2.xml
aws.credentials=${default.home_folder}/AwsCredentials.properties

# proxy settings
#http.proxy.host=10.255.1.241
#http.proxy.port=8080


# service bus
# WARNING: service bus allows multiple instances running together, please set run_timeout slightly less than crontab period
servicebus.run_timeout=4m45s
servicebus.polling_timeout=3s
servicebus.fix_timestamp.enabled=true
servicebus.fix_timestamp.tolerance=5m
servicebus.queue=${default.environment}_service_bus
servicebus.queue.failed=${default.environment}_failed_service_bus
servicebus.max_number_of_messages=10
servicebus.queue_regex=^(${default.environment})_(to_process|[a-zA-Z]+)_([a-zA-Z0-9_]+)$
servicebus.swallow_regex=^([a-zA-Z0-9]+)_(started)_([a-zA-Z0-9_]+)$


# dsr steps monitoring filter
filter.steps.enabled=true
filter.steps.decode_service={\
	extraction_and_normalization=EXTRACT,\
	sophia_tools=EXTRACT,\
	extraction_and_validation:EXTRACT,\
	cleaning_and_normalization:CLEAN,\
    extract_normalize=CLEAN,\
	hypercube_slice:HYPERCUBE,\
	identification:IDENTIFY,\
	newidentification:IDENTIFY,\
	newidentification_ra:IDENTIFY,\
	pricing:PRICE,\
	claim:CLAIM,\
	pricingclaim:CLAIM,\
	unidentified:UNILOAD,\
	newunidentified:UNILOAD,\
	pre_learning:PRELEARN,\
	learning:LEARN\
}
filter.steps.decode_type={\
	to_process:QUEUED,\
	started:STARTED,\
	completed:FINISHED,\
	failed:FINISHED\
}
filter.steps.decode_status={completed:OK,failed:KO}


# ccid metadata filter
filter.ccid.enabled=true
filter.ccid.total_value_scale=0.0001
filter.ccid.mt.total_value_scale=1.0
#filter.ccid.total_value_default=null
filter.ccid.error_regex=^([^0-9]+)(compenso_dem_punto)([^0-9]+)(\\d+\\.?\\d*)([^0-9]+)(\\d+\\.?\\d*)([^0-9]+)(.*)$
filter.ccid.error_regex_group=6


# pmdump metadata filter
filter.pmdump.enabled=true
filter.pmdump.queue.service_bus=${default.environment}_service_bus
filter.pmdump.queue.source=${default.environment}_completed_pmdump
filter.pmdump.queue.destination=${default.environment}_to_process_pmload


# pmbonifica metadata filter
filter.pmbonifica.enabled=true
filter.pmbonifica.queue.service_bus=${default.environment}_service_bus
filter.pmbonifica.queue.source=${default.environment}_completed_pmdump_bonifica
filter.pmbonifica.queue.destination=${default.environment}_to_process_pmload_bonifica


# cluster_monitoring filter
filter.cluster_monitoring.enabled=true
filter.cluster_monitoring.sql.insert=\
	insert into CLUSTER_MONITORING\
	( CLUSTER_ID \
	, QUEUE_TYPE \
	, SERVICE_NAME \
	, APPLICATION_ID \
	, IDDSR \
	) values \
	( '{hostname}' \
	, '{queueType}' \
	, '{serviceName}' \
	, IF ('' = '{applicationId}', NULL, '{applicationId}') \
	, IF ('' = '{idDsr}', NULL, '{idDsr}') \
	)
	
filter.cluster_monitoring.sql.update=\
	update CLUSTER_MONITORING \
	set QUEUE_TYPE = '{queueType}' \
	  , SERVICE_NAME = '{serviceName}' \
	  , APPLICATION_ID = IF ('' = '{applicationId}', NULL, '{applicationId}') \
	  , IDDSR = IF ('' = '{idDsr}', NULL, '{idDsr}') \
	where CLUSTER_ID = '{hostname}'


# dsr_steps_monitoring filter
filter.dsr_steps_monitoring.enabled=true
filter.dsr_steps_monitoring.services=\
    sophia_tools,\
	extraction_and_validation,\
	cleaning_and_normalization,\
    extract_normalize,\
	identification,\
	newidentification,\
	pricing,\
	claim,\
	pricingclaim,\
	pre_learning,\
	learning,\
	unidentified,\
    newunidentified

filter.dsr_steps_monitoring.sql.insert.extraction_and_validation=\
insert into DSR_STEPS_MONITORING\
	( IDDSR \
	, EXTRACT_QUEUED \
	) values \
	( '{idDsr}' \
	, CURRENT_TIMESTAMP() \
	)

filter.dsr_steps_monitoring.sql.insert.sophia_tools=\
insert into DSR_STEPS_MONITORING\
	( IDDSR \
	, EXTRACT_QUEUED \
	) values \
	( '{idDsr}' \
	, CURRENT_TIMESTAMP() \
	)

filter.dsr_steps_monitoring.sql.update.extraction_and_validation=\
	update DSR_STEPS_MONITORING \
	set EXTRACT_QUEUED = IF ('{queueType}' = 'to_process', CURRENT_TIMESTAMP(), EXTRACT_QUEUED) \
	  , EXTRACT_STARTED = (CASE WHEN '{queueType}' = 'to_process' THEN NULL \
	                            WHEN '{queueType}' = 'started' THEN CURRENT_TIMESTAMP() \
	                            ELSE EXTRACT_STARTED END) \
	  , EXTRACT_FINISHED = (CASE WHEN '{queueType}' IN ('failed', 'completed') THEN CURRENT_TIMESTAMP() \
	                             WHEN '{queueType}' = 'started' THEN EXTRACT_FINISHED \
	                             ELSE NULL END) \
	  , EXTRACT_STATUS = (CASE WHEN '{queueType}' = 'failed' THEN 'KO' \
	                           WHEN '{queueType}' = 'completed' THEN 'OK' \
	                           WHEN '{queueType}' = 'started' THEN EXTRACT_STATUS \
	                           ELSE NULL END) \
	  , ERROR_MESSAGE = (CASE WHEN '{queueType}' = 'failed' THEN '{errorMessage}' ELSE null END) \
	where IDDSR = '{idDsr}'

filter.dsr_steps_monitoring.sql.update.sophia_tools=\
	update DSR_STEPS_MONITORING \
	set EXTRACT_QUEUED = IF ('{queueType}' = 'to_process', CURRENT_TIMESTAMP(), EXTRACT_QUEUED) \
	  , EXTRACT_STARTED = (CASE WHEN '{queueType}' = 'to_process' THEN NULL \
	                            WHEN '{queueType}' = 'started' THEN CURRENT_TIMESTAMP() \
	                            ELSE EXTRACT_STARTED END) \
	  , EXTRACT_FINISHED = (CASE WHEN '{queueType}' IN ('failed', 'completed') THEN CURRENT_TIMESTAMP() \
	                             WHEN '{queueType}' = 'started' THEN EXTRACT_FINISHED \
	                             ELSE NULL END) \
	  , EXTRACT_STATUS = (CASE WHEN '{queueType}' = 'failed' THEN 'KO' \
	                           WHEN '{queueType}' = 'completed' THEN 'OK' \
	                           WHEN '{queueType}' = 'started' THEN EXTRACT_STATUS \
	                           ELSE NULL END) \
	  , CLEAN_FINISHED = NULL \
	  , CLEAN_QUEUED = NULL \
	  , CLEAN_STARTED = NULL \
	  , CLEAN_STATUS = NULL \
	  , IDENTIFY_FINISHED = NULL \
	  , IDENTIFY_QUEUED = NULL \
	  , IDENTIFY_STARTED = NULL \
	  , IDENTIFY_STATUS = NULL \
	  , PRICE_FINISHED = NULL \
	  , PRICE_QUEUED = NULL \
	  , PRICE_STARTED = NULL \
	  , PRICE_STATUS = NULL \
	  , CLAIM_FINISHED = NULL \
	  , CLAIM_QUEUED = NULL \
	  , CLAIM_STARTED = NULL \
	  , CLAIM_STATUS = NULL \
	  , PRELEARN_FINISHED = NULL \
	  , PRELEARN_QUEUED = NULL \
	  , PRELEARN_STARTED = NULL \
	  , PRELEARN_STATUS = NULL \
	  , LEARN_FINISHED = NULL \
	  , LEARN_QUEUED = NULL \
	  , LEARN_STARTED = NULL \
	  , LEARN_STATUS = NULL \
	  , UNILOAD_FINISHED = NULL \
	  , UNILOAD_QUEUED = NULL \
	  , UNILOAD_STARTED = NULL \
	  , UNILOAD_STATUS = NULL \
	  , IDENTIFY_KB_VERSION = NULL \
	  , ERROR_MESSAGE = (CASE WHEN '{queueType}' = 'failed' THEN '{errorMessage}' WHEN '{queueType}' = 'to_process' THEN NULL ELSE ERROR_MESSAGE END) \
	where IDDSR = '{idDsr}'



filter.dsr_steps_monitoring.sql.update.cleaning_and_normalization=\
	update DSR_STEPS_MONITORING \
	set CLEAN_QUEUED = IF ('{queueType}' = 'to_process', CURRENT_TIMESTAMP(), CLEAN_QUEUED) \
	  , CLEAN_STARTED = (CASE WHEN '{queueType}' = 'to_process' THEN NULL \
	                          WHEN '{queueType}' = 'started' THEN CURRENT_TIMESTAMP() \
	                          ELSE CLEAN_STARTED END) \
	  , CLEAN_FINISHED = (CASE WHEN '{queueType}' IN ('failed', 'completed') THEN CURRENT_TIMESTAMP() \
	                           WHEN '{queueType}' = 'started' THEN CLEAN_FINISHED \
	                           ELSE NULL END) \
	  , CLEAN_STATUS = (CASE WHEN '{queueType}' = 'failed' THEN 'KO' \
	                         WHEN '{queueType}' = 'completed' THEN 'OK' \
	                         WHEN '{queueType}' = 'started' THEN CLEAN_STATUS \
	                         ELSE NULL END) \
	  , ERROR_MESSAGE = (CASE WHEN '{queueType}' = 'failed' THEN '{errorMessage}' ELSE ERROR_MESSAGE END) \
	where IDDSR = '{idDsr}'

filter.dsr_steps_monitoring.sql.insert.extract_normalize=\
insert into DSR_STEPS_MONITORING\
	( IDDSR \
	, CLEAN_QUEUED \
	) values \
	( '{idDsr}' \
	, CURRENT_TIMESTAMP() \
	)

filter.dsr_steps_monitoring.sql.update.extract_normalize=\
	update DSR_STEPS_MONITORING \
	set CLEAN_QUEUED = IF ('{queueType}' = 'to_process', CURRENT_TIMESTAMP(), CLEAN_QUEUED) \
	  , CLEAN_STARTED = (CASE WHEN '{queueType}' = 'to_process' THEN NULL \
	                          WHEN '{queueType}' = 'started' THEN CURRENT_TIMESTAMP() \
	                          ELSE CLEAN_STARTED END) \
	  , CLEAN_FINISHED = (CASE WHEN '{queueType}' IN ('failed', 'completed') THEN CURRENT_TIMESTAMP() \
	                           WHEN '{queueType}' = 'started' THEN CLEAN_FINISHED \
	                           ELSE NULL END) \
	  , CLEAN_STATUS = (CASE WHEN '{queueType}' = 'failed' THEN 'KO' \
	                         WHEN '{queueType}' = 'completed' THEN 'OK' \
	                         WHEN '{queueType}' = 'started' THEN CLEAN_STATUS \
	                         ELSE NULL END) \
	  , ERROR_MESSAGE = (CASE WHEN '{queueType}' = 'failed' THEN '{errorMessage}' ELSE ERROR_MESSAGE END) \
	where IDDSR = '{idDsr}'
        

filter.dsr_steps_monitoring.sql.update.identification=\
	update DSR_STEPS_MONITORING \
	set IDENTIFY_QUEUED = IF ('{queueType}' = 'to_process', CURRENT_TIMESTAMP(), IDENTIFY_QUEUED) \
	  , IDENTIFY_STARTED = (CASE WHEN '{queueType}' = 'to_process' THEN NULL \
	                             WHEN '{queueType}' = 'started' THEN CURRENT_TIMESTAMP() \
	                             ELSE IDENTIFY_STARTED END) \
	  , IDENTIFY_FINISHED = (CASE WHEN '{queueType}' IN ('failed', 'completed') THEN CURRENT_TIMESTAMP() \
	                              WHEN '{queueType}' = 'started' THEN IDENTIFY_FINISHED \
	                              ELSE NULL END) \
	  , IDENTIFY_STATUS = (CASE WHEN '{queueType}' = 'failed' THEN 'KO' \
	                            WHEN '{queueType}' = 'completed' THEN 'OK' \
	                            WHEN '{queueType}' = 'started' THEN IDENTIFY_STATUS \
	                            ELSE NULL END) \
	where IDDSR = '{idDsr}'

filter.dsr_steps_monitoring.sql.update.newidentification=\
	update DSR_STEPS_MONITORING \
	set IDENTIFY_QUEUED = IF ('{queueType}' = 'to_process', CURRENT_TIMESTAMP(), IDENTIFY_QUEUED) \
	  , IDENTIFY_STARTED = (CASE WHEN '{queueType}' = 'to_process' THEN NULL \
	                             WHEN '{queueType}' = 'started' THEN CURRENT_TIMESTAMP() \
	                             ELSE IDENTIFY_STARTED END) \
	  , IDENTIFY_FINISHED = (CASE WHEN '{queueType}' IN ('failed', 'completed') THEN CURRENT_TIMESTAMP() \
	                              WHEN '{queueType}' = 'started' THEN IDENTIFY_FINISHED \
	                              ELSE NULL END) \
	  , IDENTIFY_STATUS = (CASE WHEN '{queueType}' = 'failed' THEN 'KO' \
	                            WHEN '{queueType}' = 'completed' THEN 'OK' \
	                            WHEN '{queueType}' = 'started' THEN IDENTIFY_STATUS \
	                            ELSE NULL END) \
      , IDENTIFY_KB_VERSION = (CASE WHEN '{queueType}' ='completed' THEN '{kbVersion}' ELSE IDENTIFY_KB_VERSION END ) \
	  , ERROR_MESSAGE = (CASE WHEN '{queueType}' = 'failed' THEN '{errorMessage}' ELSE ERROR_MESSAGE END) \
	where IDDSR = '{idDsr}'


filter.dsr_steps_monitoring.sql.update.newidentification_ra=\
	update DSR_STEPS_MONITORING \
	set IDENTIFY_STATUS = (CASE WHEN '{queueType}' = 'failed' THEN 'KO' \
	                            ELSE IDENTIFY_STATUS END) \
	  , ERROR_MESSAGE = (CASE WHEN '{queueType}' = 'failed' THEN '{errorMessage}' ELSE ERROR_MESSAGE END) \
	where IDDSR = '{idDsr}'

filter.dsr_steps_monitoring.sql.update.pricing=\
	update DSR_STEPS_MONITORING \
	set PRICE_QUEUED = IF ('{queueType}' = 'to_process', CURRENT_TIMESTAMP(), PRICE_QUEUED) \
	  , PRICE_STARTED = (CASE WHEN '{queueType}' = 'to_process' THEN NULL \
	                          WHEN '{queueType}' = 'started' THEN CURRENT_TIMESTAMP() \
	                          ELSE PRICE_STARTED END) \
	  , PRICE_FINISHED = (CASE WHEN '{queueType}' IN ('failed', 'completed') THEN CURRENT_TIMESTAMP() \
	                           WHEN '{queueType}' = 'started' THEN PRICE_FINISHED \
	                           ELSE NULL END) \
	  , PRICE_STATUS = (CASE WHEN '{queueType}' = 'failed' THEN 'KO' \
	                         WHEN '{queueType}' = 'completed' THEN 'OK' \
	                         WHEN '{queueType}' = 'started' THEN PRICE_STATUS \
	                         ELSE NULL END) \
	where IDDSR = '{idDsr}'

filter.dsr_steps_monitoring.sql.update.claim=\
	update DSR_STEPS_MONITORING \
	set CLAIM_QUEUED = IF ('{queueType}' = 'to_process', CURRENT_TIMESTAMP(), CLAIM_QUEUED) \
	  , CLAIM_STARTED = (CASE WHEN '{queueType}' = 'to_process' THEN NULL \
	                          WHEN '{queueType}' = 'started' THEN CURRENT_TIMESTAMP() \
	                          ELSE CLAIM_STARTED END) \
	  , CLAIM_FINISHED = (CASE WHEN '{queueType}' IN ('failed', 'completed') THEN CURRENT_TIMESTAMP() \
	                           WHEN '{queueType}' = 'started' THEN CLAIM_FINISHED \
	                           ELSE NULL END) \
	  , CLAIM_STATUS = (CASE WHEN '{queueType}' = 'failed' THEN 'KO' \
	                         WHEN '{queueType}' = 'completed' THEN 'OK' \
	                         WHEN '{queueType}' = 'started' THEN CLAIM_STATUS \
	                         ELSE NULL END) \
	where IDDSR = '{idDsr}'


filter.dsr_steps_monitoring.sql.update.pricingclaim=\
	update DSR_STEPS_MONITORING \
	set CLAIM_QUEUED = IF ('{queueType}' = 'to_process', CURRENT_TIMESTAMP(), CLAIM_QUEUED) \
	  , CLAIM_STARTED = (CASE WHEN '{queueType}' = 'to_process' THEN NULL \
	                          WHEN '{queueType}' = 'started' THEN CURRENT_TIMESTAMP() \
	                          ELSE CLAIM_STARTED END) \
	  , CLAIM_FINISHED = (CASE WHEN '{queueType}' IN ('failed', 'completed') THEN CURRENT_TIMESTAMP() \
	                           WHEN '{queueType}' = 'started' THEN CLAIM_FINISHED \
	                           ELSE NULL END) \
	  , CLAIM_STATUS = (CASE WHEN '{queueType}' = 'failed' THEN 'KO' \
	                         WHEN '{queueType}' = 'completed' THEN 'OK' \
	                         WHEN '{queueType}' = 'started' THEN CLAIM_STATUS \
	                         ELSE NULL END) \
      , PRICE_QUEUED = IF ('{queueType}' = 'to_process', CURRENT_TIMESTAMP(), PRICE_QUEUED) \
	  , PRICE_STARTED = (CASE WHEN '{queueType}' = 'to_process' THEN NULL \
	                          WHEN '{queueType}' = 'started' THEN CURRENT_TIMESTAMP() \
	                          ELSE PRICE_STARTED END) \
	  , PRICE_FINISHED = (CASE WHEN '{queueType}' IN ('failed', 'completed') THEN CURRENT_TIMESTAMP() \
	                           WHEN '{queueType}' = 'started' THEN PRICE_FINISHED \
	                           ELSE NULL END) \
	  , PRICE_STATUS = (CASE WHEN '{queueType}' = 'failed' THEN 'KO' \
	                         WHEN '{queueType}' = 'completed' THEN 'OK' \
	                         WHEN '{queueType}' = 'started' THEN PRICE_STATUS \
	                         ELSE NULL END) \
  	  , ERROR_MESSAGE = (CASE WHEN '{queueType}' = 'failed' THEN '{errorMessage}' ELSE ERROR_MESSAGE END) \
	where IDDSR = '{idDsr}'



filter.dsr_steps_monitoring.sql.update.pre_learning=\
	update DSR_STEPS_MONITORING \
	set PRELEARN_QUEUED = IF ('{queueType}' = 'to_process', CURRENT_TIMESTAMP(), PRELEARN_QUEUED) \
	  , PRELEARN_STARTED = (CASE WHEN '{queueType}' = 'to_process' THEN NULL \
	                             WHEN '{queueType}' = 'started' THEN CURRENT_TIMESTAMP() \
	                             ELSE PRELEARN_STARTED END) \
	  , PRELEARN_FINISHED = (CASE WHEN '{queueType}' IN ('failed', 'completed') THEN CURRENT_TIMESTAMP() \
	                              WHEN '{queueType}' = 'started' THEN PRELEARN_FINISHED \
	                              ELSE NULL END) \
	  , PRELEARN_STATUS = (CASE WHEN '{queueType}' = 'failed' THEN 'KO' \
	                            WHEN '{queueType}' = 'completed' THEN 'OK' \
	                            WHEN '{queueType}' = 'started' THEN PRELEARN_STATUS \
	                            ELSE NULL END) \
	  , LEARN_FINISHED = NULL \
	  , LEARN_QUEUED = NULL \
	  , LEARN_STARTED = NULL \
	  , LEARN_STATUS = NULL \
	where IDDSR = '{idDsr}'

filter.dsr_steps_monitoring.sql.update.learning=\
	update DSR_STEPS_MONITORING \
	set LEARN_QUEUED = IF ('{queueType}' = 'to_process', CURRENT_TIMESTAMP(), LEARN_QUEUED) \
	  , LEARN_STARTED = (CASE WHEN '{queueType}' = 'to_process' THEN NULL \
	                          WHEN '{queueType}' = 'started' THEN CURRENT_TIMESTAMP() \
	                          ELSE LEARN_STARTED END) \
	  , LEARN_FINISHED = (CASE WHEN '{queueType}' IN ('failed', 'completed') THEN CURRENT_TIMESTAMP() \
	                           WHEN '{queueType}' = 'started' THEN LEARN_FINISHED \
	                           ELSE NULL END) \
	  , LEARN_STATUS = (CASE WHEN '{queueType}' = 'failed' THEN 'KO' \
	                         WHEN '{queueType}' = 'completed' THEN 'OK' \
	                         WHEN '{queueType}' = 'started' THEN LEARN_STATUS \
	                         ELSE NULL END) \
	where IDDSR = '{idDsr}'

filter.dsr_steps_monitoring.sql.update.unidentified=\
	update DSR_STEPS_MONITORING \
	set UNILOAD_QUEUED = IF ('{queueType}' = 'to_process', CURRENT_TIMESTAMP(), UNILOAD_QUEUED) \
	  , UNILOAD_STARTED = (CASE WHEN '{queueType}' = 'to_process' THEN NULL \
	                            WHEN '{queueType}' = 'started' THEN CURRENT_TIMESTAMP() \
	                            ELSE UNILOAD_STARTED END) \
	  , UNILOAD_FINISHED = (CASE WHEN '{queueType}' IN ('failed', 'completed') THEN CURRENT_TIMESTAMP() \
	                             WHEN '{queueType}' = 'started' THEN UNILOAD_FINISHED \
	                             ELSE UNILOAD_FINISHED END) \
	  , UNILOAD_STATUS = (CASE WHEN '{queueType}' = 'failed' THEN 'KO' \
	                           WHEN '{queueType}' = 'completed' THEN 'OK' \
	                           WHEN '{queueType}' = 'started' THEN UNILOAD_STATUS \
	                           ELSE UNILOAD_STATUS END) \
	where IDDSR = '{idDsr}'

        

filter.dsr_steps_monitoring.sql.update.newunidentified=\
	update DSR_STEPS_MONITORING \
	set UNILOAD_QUEUED = IF ('{queueType}' = 'to_process', CURRENT_TIMESTAMP(), UNILOAD_QUEUED) \
	  , UNILOAD_STARTED = (CASE WHEN '{queueType}' = 'to_process' THEN NULL \
	                            WHEN '{queueType}' = 'started' THEN CURRENT_TIMESTAMP() \
	                            ELSE UNILOAD_STARTED END) \
	  , UNILOAD_FINISHED = (CASE WHEN '{queueType}' IN ('failed', 'completed') THEN CURRENT_TIMESTAMP() \
	                             WHEN '{queueType}' = 'started' THEN UNILOAD_FINISHED \
	                             ELSE UNILOAD_FINISHED END) \
	  , UNILOAD_STATUS = (CASE WHEN '{queueType}' = 'failed' THEN 'KO' \
	                           WHEN '{queueType}' = 'completed' THEN 'OK' \
	                           WHEN '{queueType}' = 'started' THEN UNILOAD_STATUS \
	                           ELSE UNILOAD_STATUS END) \
	where IDDSR = '{idDsr}'


notifica_mail.sql= \
  select replace(group_CONCAT(distinct EMAIL_LIST),';',',') \
  from MM_NOTIFICA_EMAIL \
  where IDDSP='{dsp}' and TIPO={tipo}

notifica_mail_all.sql= \
  select replace(group_CONCAT(distinct EMAIL_LIST),';',',') \
  from MM_NOTIFICA_EMAIL \
  where TIPO={tipo}


# sqs failed
sqsfailed.queues.regex=.*/(${default.environment}_failed_.*)


# ipi ulisse sqs merger
ipiulisse.queue.ipi=${default.environment}_completed_ipi
ipiulisse.queue.ulisse=${default.environment}_completed_ulisse
ipiulisse.queue.service_bus=${default.environment}_service_bus
ipiulisse.queue.ipi_ulisse=${default.environment}_completed_ipi_ulisse
ipiulisse.repository=${default.environment}_ipi_ulisse.repository


s3_new_dsr_from_BMAT.sqs.sender_name=service_bus
s3_new_dsr_from_BMAT.sqs.service_bus_queue=${default.environment_sqs}_service_bus
s3_new_dsr_from_BMAT.sqs.to_process_queue=${default.environment_sqs}_s3_new_dsr_from_BMAT
#s3_new_dsr_from_BMAT.sqs.started_queue=${default.environment_sqs}_started_sophia_tools
#s3_new_dsr_from_BMAT.sqs.completed_queue=${default.environment_sqs}_completed_sophia_tools
#s3_new_dsr_from_BMAT.sqs.failed_queue=${default.environment_sqs}_failed_sophia_tools



filter.update_schemi_riparto.enabled=true
filter.update_schemi_riparto.queue.source.service_name=update_schemi_riparto
filter.update_schemi_riparto.dump_process_names=siae_dump_v2,ipi_dump_v2,ucmr_ada_dump_v2,tis_dump_v2

filter.update_schemi_riparto.sqs.siae_dump_v2.sender_name=service_bus
filter.update_schemi_riparto.sqs.siae_dump_v2.service_bus_queue=${default.environment}_service_bus
filter.update_schemi_riparto.sqs.siae_dump_v2.to_process_queue=${default.environment}_to_process_siae_dump_v2
filter.update_schemi_riparto.sqs.siae_dump_v2.started_queue=${default.environment}_started_siae_dump_v2
filter.update_schemi_riparto.sqs.siae_dump_v2.completed_queue=${default.environment}_completed_siae_dump_v2
filter.update_schemi_riparto.sqs.siae_dump_v2.failed_queue=${default.environment}_failed_siae_dump_v2
filter.update_schemi_riparto.sqs.siae_dump_v2.body_template=\
  {"year":"{year}","month":"{month}","day":"01","techsysdate":true,"updateId":"{updateId}"}

filter.update_schemi_riparto.sqs.ipi_dump_v2.sender_name=service_bus
filter.update_schemi_riparto.sqs.ipi_dump_v2.service_bus_queue=${default.environment}_service_bus
filter.update_schemi_riparto.sqs.ipi_dump_v2.to_process_queue=${default.environment}_to_process_ipi_dump_v2
filter.update_schemi_riparto.sqs.ipi_dump_v2.started_queue=${default.environment}_started_ipi_dump_v2
filter.update_schemi_riparto.sqs.ipi_dump_v2.completed_queue=${default.environment}_completed_ipi_dump_v2
filter.update_schemi_riparto.sqs.ipi_dump_v2.failed_queue=${default.environment}_failed_ipi_dump_v2
filter.update_schemi_riparto.sqs.ipi_dump_v2.body_template=\
  {"year":"{year}","month":"{month}","day":"01","techsysdate":true,"updateId":"{updateId}"}

filter.update_schemi_riparto.sqs.ucmr_ada_dump_v2.sender_name=service_bus
filter.update_schemi_riparto.sqs.ucmr_ada_dump_v2.service_bus_queue=${default.environment}_service_bus
filter.update_schemi_riparto.sqs.ucmr_ada_dump_v2.to_process_queue=${default.environment}_to_process_ucmr_ada_dump_v2
filter.update_schemi_riparto.sqs.ucmr_ada_dump_v2.started_queue=${default.environment}_started_ucmr_ada_dump_v2
filter.update_schemi_riparto.sqs.ucmr_ada_dump_v2.completed_queue=${default.environment}_completed_ucmr_ada_dump_v2
filter.update_schemi_riparto.sqs.ucmr_ada_dump_v2.failed_queue=${default.environment}_failed_ucmr_ada_dump_v2
filter.update_schemi_riparto.sqs.ucmr_ada_dump_v2.body_template=\
  {"year":"{year}","month":"{month}","day":"01","society": "UCMR-ADA","updateId":"{updateId}"}

filter.update_schemi_riparto.sqs.tis_dump_v2.sender_name=service_bus
filter.update_schemi_riparto.sqs.tis_dump_v2.service_bus_queue=${default.environment}_service_bus
filter.update_schemi_riparto.sqs.tis_dump_v2.to_process_queue=${default.environment}_to_process_tis_dump_v2
filter.update_schemi_riparto.sqs.tis_dump_v2.started_queue=${default.environment}_started_tis_dump_v2
filter.update_schemi_riparto.sqs.tis_dump_v2.completed_queue=${default.environment}_completed_tis_dump_v2
filter.update_schemi_riparto.sqs.tis_dump_v2.failed_queue=${default.environment}_failed_tis_dump_v2
filter.update_schemi_riparto.sqs.tis_dump_v2.body_template=\
  {"year":"{year}","month":"{month}","day":"01","updateId":"{updateId}"}

filter.update_schemi_riparto.sqs.self.sender_name=service_bus
filter.update_schemi_riparto.sqs.self.service_bus_queue=${default.environment}_service_bus
filter.update_schemi_riparto.sqs.self.to_process_queue=${default.environment}_to_process_update_schemi_riparto
filter.update_schemi_riparto.sqs.self.started_queue=${default.environment}_started_update_schemi_riparto
filter.update_schemi_riparto.sqs.self.completed_queue=${default.environment}_completed_update_schemi_riparto
filter.update_schemi_riparto.sqs.self.failed_queue=${default.environment}_failed_update_schemi_riparto
filter.update_schemi_riparto.sqs.self.body_template=\
  {"year":"{year}","month":"{month}","day":"01","updateId":"{updateId}"}

filter.update_schemi_riparto.sql.check_executions=\
  select SERVICE_NAME, QUEUE_TYPE, max(INSERT_TIME) AS INSERT_TIME \
  from SQS_SERVICE_BUS \
  where service_name in ({services}) \
  and queue_type in ('failed', 'completed') \
  and json_extract(MESSAGE_JSON, '$.input.body.updateId') = '{updateId}' \
  group by SERVICE_NAME, QUEUE_TYPE

filter.update_schemi_riparto.sql.find_input_message=\
  select json_extract(MESSAGE_JSON, '$.input') INPUT_MESSAGE \
  from SQS_SERVICE_BUS \
  where SERVICE_NAME = 'update_schemi_riparto' \
  and QUEUE_TYPE = 'started' \
  and json_extract(MESSAGE_JSON, '$.input.body.updateId') = '{updateId}'
