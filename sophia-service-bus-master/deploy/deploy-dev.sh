#!/bin/sh

SOURCE_DIR=/home/roberto.cerfogli/service-bus
DEPLOY_DIR=/var/local/sophia/dev
 
cd $SOURCE_DIR

sudo cp -rf dev/* $DEPLOY_DIR
sudo cp sophia-service-bus-1.0.2-jar-with-dependencies.jar $DEPLOY_DIR
sudo chgrp -R sophia $DEPLOY_DIR


