# sophia-devops-ansible

devops automatizzate sophia

## Comandi

ansible-playbook --key-file=.ssh/id_rsa -i ./inventories/prod_multimediale.yml ./playbooks/cronjobs_backup.yml

ansible-playbook --key-file=.ssh/id_rsa -i ./inventories/prod_multimediale.yml ./playbooks/cronjobs_install.yml

ansible-playbook --key-file=.ssh/id_rsa -i ./inventories/prod_multimediale.yml ./playbooks/propagate-identification-jar.yml

ansible-playbook --key-file=.ssh/id_rsa -i ./inventories/prod_multimediale.yml ./playbooks/check_identification_running.yml

ansible-playbook --key-file=.ssh/id_rsa -i ./inventories/prod_multimediale.yml ./playbooks/uname.yml

ansible-playbook --key-file=.ssh/id_rsa -i ./inventories/prod_performing.yml ./playbooks/fetch_performing.yml