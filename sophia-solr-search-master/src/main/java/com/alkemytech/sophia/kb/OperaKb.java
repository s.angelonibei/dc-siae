package com.alkemytech.sophia.kb;

public class OperaKb {
	private String codiceOpera;
	private String codiceSiae;
	private String titoloOriginale;
	private String autore;
	private String compositore;
	private String interprete;
	private Double rilevanza;
	private Double gradoDiConfidenza;

	private String confidenzaTitoloOriginale;
	private String confidenzaAutore;
	private String confCompositore;
	private String confInterprete;
	private String iswc;
	private String tipoCodifica;
	
	public String getTitoloOriginale() {
		return titoloOriginale;
	}
	public void setTitoloOriginale(String titoloOriginale) {
		this.titoloOriginale = titoloOriginale;
	}
	public String getConfidenzaTitoloOriginale() {
		return confidenzaTitoloOriginale;
	}
	public void setConfidenzaTitoloOriginale(String confidenzaTitoloOriginale) {
		this.confidenzaTitoloOriginale = confidenzaTitoloOriginale;
	}
	public String getAutore() {
		return autore;
	}
	public void setAutore(String autore) {
		this.autore = autore;
	}
	public String getConfidenzaAutore() {
		return confidenzaAutore;
	}
	public void setConfidenzaAutore(String confidenzaAutore) {
		this.confidenzaAutore = confidenzaAutore;
	}
	public String getCompositore() {
		return compositore;
	}
	public void setCompositore(String compositore) {
		this.compositore = compositore;
	}
	public String getConfCompositore() {
		return confCompositore;
	}
	public void setConfCompositore(String confCompositore) {
		this.confCompositore = confCompositore;
	}
	public String getInterprete() {
		return interprete;
	}
	public void setInterprete(String interprete) {
		this.interprete = interprete;
	}
	public String getConfInterprete() {
		return confInterprete;
	}
	public void setConfInterprete(String confInterprete) {
		this.confInterprete = confInterprete;
	}
	public Double getGradoDiConfidenza() {
		return gradoDiConfidenza;
	}
	public void setGradoDiConfidenza(Double gradoDiConfidenza) {
		this.gradoDiConfidenza = gradoDiConfidenza;
	}
	public String getCodiceOpera() {
		return codiceOpera;
	}
	public void setCodiceOpera(String codiceOpera) {
		this.codiceOpera = codiceOpera;
	}
	public String getIswc() {
		return iswc;
	}
	public void setIswc(String iswc) {
		this.iswc = iswc;
	}
	public String getTipoCodifica() {
		return tipoCodifica;
	}
	public void setTipoCodifica(String tipoCodifica) {
		this.tipoCodifica = tipoCodifica;
	}
	public String getCodiceSiae() {
		return codiceSiae;
	}
	public void setCodiceSiae(String codiceSiae) {
		this.codiceSiae = codiceSiae;
	}
	
	public Double getRilevanza() {
		return rilevanza;
	}
	public void setRilevanza(Double rilevanza) {
		this.rilevanza = rilevanza;
	}
	@Override
	public String toString() {
		return "OperaKb [codiceOpera=" + codiceOpera + ", codiceSiae=" + codiceSiae + ", titoloOriginale="
				+ titoloOriginale + ", autore=" + autore + ", compositore=" + compositore + ", interprete=" + interprete
				+ ", gradoDiConfidenza=" + gradoDiConfidenza + ", confidenzaTitoloOriginale="
				+ confidenzaTitoloOriginale + ", confidenzaAutore=" + confidenzaAutore + ", confCompositore="
				+ confCompositore + ", confInterprete=" + confInterprete + ", iswc=" + iswc + ", tipoCodifica="
				+ tipoCodifica + "]";
	}
	
	
	
}
