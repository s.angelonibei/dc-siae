package com.alkemytech.sophia.solr;


/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class SearchInfo {

	private final String title;
	private final String artists;
	private final String roles;
	
	public SearchInfo(String title, String artists, String roles) {
		super();
		this.title = title;
		this.artists = artists;
		this.roles = roles;
	}

	public String getTitle() {
		return title;
	}

	public String getArtists() {
		return artists;
	}

	public String getRoles() {
		return roles;
	}
	
}
