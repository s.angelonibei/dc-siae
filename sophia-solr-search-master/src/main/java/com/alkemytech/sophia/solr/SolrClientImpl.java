package com.alkemytech.sophia.solr;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;

import com.alkemytech.sophia.common.tools.StringTools;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class SolrClientImpl implements SolrClient {
	
	private final double pesoCampoTitolo;
	private final double pesoCampoCompositore;
	private final double pesoCampoAutore;
	private final double pesoCampoInterprete;
	private final String nazionalita;
	private final String tipologiaArtista;
	private final double minGradoConfidenzaRicercaEsatta;
	private final int maxResults;
	private final double discardConfidenzaMinoreDi;
	private final double includeRilevanzaMaggioreDi;
	private final double includeScoreMaggioreDi;
	private final String highlightPrefix;
	private final String highlightPostfix;
	private final int highlightSnippets;	
	private final CloudSolrClient solrClient;

	protected SolrClientImpl(double pesoCampoTitolo, double pesoCampoCompositore,
			double pesoCampoAutore, double pesoCampoInterprete,
			int maxResults, double discardConfidenzaMinoreDi,
			double includeRilevanzaMaggioreDi, double includeScoreMaggioreDi,
			String nazionalita, String tipologiaArtista,
			double minGradoConfidenzaRicercaEsatta, String solrCollection,
			String zkHosts, int zkClientTimeout, int zkConnectTimeout, int httpMaxConnections,
			String highlightPrefix, String highlightPostfix, int highlightSnippets) {
		super();

		this.pesoCampoTitolo = pesoCampoTitolo;
		this.pesoCampoCompositore = pesoCampoCompositore;
		this.pesoCampoAutore = pesoCampoAutore;
		this.pesoCampoInterprete = pesoCampoInterprete;
		this.maxResults = maxResults;
		this.discardConfidenzaMinoreDi = discardConfidenzaMinoreDi;
		this.includeRilevanzaMaggioreDi = includeRilevanzaMaggioreDi;
		this.includeScoreMaggioreDi = includeScoreMaggioreDi;
		this.nazionalita = nazionalita;
		this.tipologiaArtista = tipologiaArtista;
		this.minGradoConfidenzaRicercaEsatta = minGradoConfidenzaRicercaEsatta;
		this.highlightPrefix = highlightPrefix;
		this.highlightPostfix = highlightPostfix;
		this.highlightSnippets = highlightSnippets;

		final HttpClient httpClient = HttpClientBuilder.create()
				.disableCookieManagement()
				.setMaxConnTotal(httpMaxConnections)
				.setMaxConnPerRoute(httpMaxConnections)
				.build();
		solrClient = new CloudSolrClient(zkHosts, httpClient);
		solrClient.setDefaultCollection(solrCollection);
		solrClient.setZkClientTimeout(zkClientTimeout);
		solrClient.setZkConnectTimeout(zkConnectTimeout);
	}

	@Override
	public List<SearchResult> searchOpereCodifica(String title, String artists, String roles) {
		return search(new Searcher() {
			@Override
			public List<Opera> search(String titolo, String artisti, String nazionalita, String tipologiaArtista, boolean highlight) {
				return searchOpereCodifica(titolo, artisti, nazionalita, tipologiaArtista, true);
			}
			@Override
			public String getMatchType() {
				return "E";
			}
		}, title, artists, roles);
	}
	
	@Override
	public List<SearchResult> searchOpereCodificaFuzzy(String title, String artists, String roles) {
		return search(new Searcher() {
			@Override
			public List<Opera> search(String titolo, String artisti, String nazionalita, String tipologiaArtista, boolean highlight) {
				return searchOpereCodificaFuzzy(titolo, artisti, nazionalita, tipologiaArtista, true);
			}
			@Override
			public String getMatchType() {
				return "F";
			}
		}, title, artists, roles);
	}
	
	@Override
	public boolean checkGradoConfidenzaMinimo(List<SearchResult> results) {
		for (SearchResult result : results) {
			if (result.getConfidenza() >= minGradoConfidenzaRicercaEsatta){
				return true;
			}
		}
		return false;
	}
	
	private List<Opera> searchOpereCodifica(String titolo, String artista, String ambito, String qualifica, boolean highlight) {
		titolo = deepCleanSearchTerm(titolo);
		artista = deepCleanSearchTerm(artista);
		if (StringUtils.isEmpty(titolo) && StringUtils.isEmpty(artista)) {
			return null;
		}
		final String query = searchOpereQueryCodifica(titolo, artista, ambito, qualifica != null ? qualifica.toLowerCase() : "");
		if (StringUtils.isEmpty(query)) {
			return null;
		}
//		System.out.println(query);
		final List<Opera> result = StringUtils.isEmpty(artista) ?
				queryCodificaAutomatica(query, "/searchOpereCodifica", false) : // if artist input is blank, don't highlight since only interpreti field is highlighted
					queryCodificaAutomatica(query, "/searchOpereCodifica", true);
//		System.out.println(null == result ? 0 : result.size());
		return result;
	}
	
	private List<Opera> searchOpereCodificaFuzzy(String titolo, String artista, String ambito, String qualifica, boolean highlight) {
		titolo = deepCleanSearchTerm(titolo);
		artista = deepCleanSearchTerm(artista);
		if (StringUtils.isEmpty(titolo) && StringUtils.isEmpty(artista)) {
			return null;
		}
		final String query = searchOpereFuzzyQueryCodifica(titolo, artista, ambito, qualifica != null ? qualifica.toLowerCase() : "");
		if (StringUtils.isEmpty(query)) {
			return null;
		}
//		System.out.println(query);
		List<Opera> result = StringUtils.isEmpty(artista) ?
				queryCodificaAutomatica(query, "/searchOpereCodificaFuzzy", false) : // if artist input is blank, don't highlight since only interpreti field is highlighted
					queryCodificaAutomatica(query, "/searchOpereCodificaFuzzy", highlight);
//		System.out.println(null == result ? 0 : result.size());
		return result;
	}
	
	private List<SearchResult> search(Searcher searcher, String title, String artists, String roles) {
		final Set<SearchResult> results = new HashSet<SearchResult>();
		// prepara elenco artisti e ruoli
		final String titolo = deepCleanSearchTerm(title);
		final String[] artisti = null == artists ? new String[0] : StringTools.split(artists, "\\|");
		final String[] tipologieArtista = null == roles ? new String[0] : StringTools.split(roles, "\\|");
		for (int i = 0; i < artisti.length; i ++) {
			artisti[i] = cancellaNomePuntato(deepCleanSearchTerm(artisti[i]));
			tipologieArtista[i] = decodeRole(tipologieArtista[i]);
		}
		// use cleaned values
		final SearchInfo searchInfo = new SearchInfo(titolo, 
				StringTools.asCsv('|', artisti),
				StringTools.asCsv('|', tipologieArtista));
		// ricerca con elenco completo artisti
		List<Opera> opere = searcher.search(titolo, 
				StringTools.asCsv(' ', artisti), nazionalita, tipologiaArtista, true);
		// [1..N] ricerche con un artista per volta
		if (CollectionUtils.isEmpty(opere)) {
			if (null != artisti) {
				for (int i = 0; i < artisti.length; i ++) {
					if (!StringTools.isNullOrEmpty(artisti[i])) {
						opere = searcher.search(titolo, artisti[i], nazionalita, tipologieArtista[i], true);
						if (!CollectionUtils.isEmpty(opere)) {
							for (Opera opera : opere) {
								final double confidenza = computeConfidenza(searchInfo, opera, tipologieArtista[i]);
								if (results.size() < maxResults ||
										confidenza >= discardConfidenzaMinoreDi ||
										opera.getRilevanza() >= includeRilevanzaMaggioreDi ||
										opera.getScore() >= includeScoreMaggioreDi) {
									results.add(new SearchResult()
										.setOpera(opera)
										.setTitolo(titolo)
										.setArtista(artisti[i])
										.setRuolo(tipologieArtista[i])
										.setMatchType(searcher.getMatchType() + "S")
										.setConfidenza(confidenza));
								}
							}
						}
					}
				}
			}
		} else {
			for (Opera opera : opere) {
				final double confidenza = computeConfidenza(searchInfo, opera, tipologiaArtista);
				if (results.size() < maxResults ||
						confidenza >= discardConfidenzaMinoreDi ||
						opera.getRilevanza() >= includeRilevanzaMaggioreDi ||
						opera.getScore() >= includeScoreMaggioreDi) {
					results.add(new SearchResult()
						.setOpera(opera)
						.setTitolo(titolo)
						.setArtista(StringTools.asCsv(' ', artisti))
						.setRuolo(tipologiaArtista)
						.setMatchType(searcher.getMatchType() + "M")
						.setConfidenza(confidenza));
				}

			}
		}
		// ordina risultato per confidenza
		final List<SearchResult> sorted = new ArrayList<SearchResult>(results);
		Collections.sort(sorted, new Comparator<SearchResult>() {
			@Override
			public int compare(SearchResult o1, SearchResult o2) {
				return - Double.compare(o1.getConfidenza(), o2.getConfidenza()); // descending order
			}
		});
		return sorted;
	}

	private String decodeRole(String role) {
//		if ("NotProvided".equals(role)) {
//			return "";
//		}
		if ("MainArtist".equals(role)) {
			return TipologiaArtista.Autore.getText();
		}
		if ("Composer".equals(role)) {
			return TipologiaArtista.Compositore.getText();
		}
		if ("ComposerAuthor".equals(role)) {
			return TipologiaArtista.Autore.getText();
		}
		if ("Author".equals(role)) {
			return TipologiaArtista.Autore.getText();
		}
		return "";
	}

	private double computeConfidenza(SearchInfo searchInfo, Opera opera, String qualifica) {
		final String[] artisti = StringTools.split(searchInfo.getArtists(), "\\|");
		double pesoCompositori = 0.0;
		double pesoAutori = 0.0;
		double pesoInterpreti = 0.0;
		double pesoTitolo = calcolaDistanzaTitolo(searchInfo, opera);
		if (StringUtils.isEmpty(qualifica)) {
			pesoCompositori = calcolaDistanzaCompositori(artisti, opera);
			pesoAutori = calcolaDistanzaAutori(artisti, opera);
			pesoInterpreti = calcolaDistanzaInterpreti(artisti, opera);
		} else {
			if (qualifica.contains(TipologiaArtista.Compositore.getText().toLowerCase())) {
				pesoCompositori = calcolaDistanzaCompositori(artisti, opera);
			}
			if (qualifica.contains(TipologiaArtista.Autore.getText().toLowerCase())) {
				pesoAutori = calcolaDistanzaAutori(artisti, opera);
			}
			if (qualifica.contains(TipologiaArtista.Interprete.getText().toLowerCase())) {
				pesoInterpreti = calcolaDistanzaInterpreti(artisti, opera);
			}
		}
		return (pesoTitolo * pesoCampoTitolo + Math.max(pesoCompositori * pesoCampoCompositore,
						Math.max(pesoAutori * pesoCampoAutore, pesoInterpreti * pesoCampoInterprete)));
	}
	
	private double calcolaDistanza(String input, String output) {
		if (StringUtils.isEmpty(input) || StringUtils.isEmpty(output)) {
			return 0.0;
		}
		double resultStringhe = calcolaDistanzaStringhe(input, output);
		if (resultStringhe == 1.0) {
			return resultStringhe;
		}
		String[] inputStr = input.split(" ");
		String[] outputStr = output.split(" ");
		double resultTerms = 0.0;
		if (inputStr.length == outputStr.length && inputStr.length > 1) {// same number of terms
			Map<String, Double> distanzaTerminiTemp = new HashMap<>();
			Map<String, Double> distanzaTermini = new HashMap<>();
			for (String inputTerm : inputStr) {
				for (String outputTerm : outputStr) {
					distanzaTerminiTemp.put(outputTerm, calcolaDistanzaStringhe(inputTerm, outputTerm));
				}
				distanzaTermini.put(inputTerm, Collections.max(distanzaTerminiTemp.values()));
			}
			for (Double value : distanzaTermini.values()) {
				resultTerms += value;
			}
			resultTerms = resultTerms / (double) distanzaTermini.size();
		}
		return Math.max(resultTerms, resultStringhe);
	}
	
	private double calcolaDistanzaStringhe(String ricercaStr, String resultStr) {
		if (StringUtils.isEmpty(ricercaStr) || StringUtils.isEmpty(resultStr)) {
			return 0.0;
		}
		int levenshteinDistance = StringUtils.getLevenshteinDistance(ricercaStr.toLowerCase(), resultStr.toLowerCase());
		Levenshtein bean = new Levenshtein(levenshteinDistance, Math.max(resultStr.length(), ricercaStr.length()));
		return (bean.getMaxLength() - bean.getDistance()) / (1.0 * bean.getMaxLength());
	}

	private double calcolaDistanzaTitolo(SearchInfo searchInfo, Opera opera) {
		if (StringUtils.isEmpty(searchInfo.getTitle())) {
			return 0.0;
		}
//	  WARNING: modified to String by SIAE IT on 2017-02
//		Map<String, Double> distanzaTitoli = new HashMap<>();
//		for (String titolo : opera.getTitoloRicerca()) {
//			titolo = titolo.trim();
//			distanzaTitoli.put(titolo, calcolaDistanzaStringhe(searchInfo.getTitle(), titolo));
//		}
//		return Collections.max(distanzaTitoli.values());
		return calcolaDistanzaStringhe(searchInfo.getTitle(), opera.getTitoloRicerca());
	}

	private double calcolaDistanzaCompositori(String[] artisti, Opera opera) {
		List<String> compositori = opera.getIpi_name_compositori();
		if (CollectionUtils.isEmpty(compositori)) {
			compositori = opera.getCompositori();
			if (CollectionUtils.isEmpty(compositori)) {
				return 0.0;
			}
		}
		double somiglianza = 0.0;
		Map<String, Double> distanze = new HashMap<>();
		for (String compositore : compositori) {
			compositore = compositore.trim();
			for (String artista : artisti) {
				somiglianza = Math.max(somiglianza,  calcolaDistanza(artista, compositore));
			}
			if (somiglianza == 1.0) {
				return somiglianza;
			}
			distanze.put(compositore, somiglianza);
		}
		return Collections.max(distanze.values());
	}

	private double calcolaDistanzaAutori(String[] artisti, Opera opera) {
		List<String> autori = opera.getIpi_name_autori();
		if (CollectionUtils.isEmpty(autori)) {
			autori = opera.getAutori();
			if (CollectionUtils.isEmpty(autori)) {
				return 0.0;
			}
		}
		double somiglianza = 0.0;
		Map<String, Double> distanze = new HashMap<>();
		for (String autore : autori) {
			autore = autore.trim();
			for (String artista : artisti) {
				somiglianza = Math.max(somiglianza,  calcolaDistanza(artista, autore));
			}
			if (somiglianza == 1.0) {
				return somiglianza;
			}
			distanze.put(autore, somiglianza);
		}
		return distanze.isEmpty() ? 0.0 : Collections.max(distanze.values());
	}
	
	private double calcolaDistanzaInterpreti(String[] artisti, Opera opera) {
		if (CollectionUtils.isEmpty(opera.getInterpreti())) {
			return 0.0;
		}
		double somiglianza = 0.0;
		Map<String, Double> distanze = new HashMap<>();
		for (String interprete : opera.getInterpreti()) {
			interprete = interprete.trim();
			for (String artista : artisti) {
				somiglianza = Math.max(somiglianza,  calcolaDistanza(artista, interprete));
			}
			if (somiglianza == 1.0) {
				return somiglianza;
			}
			distanze.put(interprete, somiglianza);
		}
		return Collections.max(distanze.values());
	}

	private String cancellaNomePuntato(String compositore) {
		if (null != compositore) {
			compositore = compositore.trim();
			while (1 == compositore.indexOf(".")) {
				compositore = compositore.substring(2);
			}
			compositore = compositore.trim();
		}
		return compositore;
	}

	private String deepCleanSearchTerm(String searchTerm) {
		if (!StringUtils.isEmpty(searchTerm)) {
			searchTerm = searchTerm.replace("\\", "");
			searchTerm = searchTerm.replace("'", "\\'");
			searchTerm = searchTerm.replaceAll("[/\\+\\-\\&\\|\\!\\(\\)\\{\\}\\[\\]\\^\\~\\*\\?\\:\\\"\\;]", "");
			searchTerm = searchTerm.replaceAll("\\b(AND|OR|NOT|and|or|not)\\b\\s?", "");
			searchTerm = (searchTerm.replaceAll("[ ]{2,}", " ")).trim(); // matches two or more spaces
		}
		return searchTerm;
	}

	private String cleanSearchTerm(String searchTerm) {
		return searchTerm.replaceAll("[ ]{2,}", " ").trim(); // matches two or more spaces
	}
	
	private String searchOpereQueryCodifica(String titolo, String artista, String ambito, String qualifica) {
		// PER TITOLO
		final StringBuffer fq = new StringBuffer();
		boolean flag = false;
		StringBuffer ipiBuf = createTitoloQuery(titolo);
		if (ipiBuf.length() > 0) {
			fq.append("(").append(ipiBuf).append(")");
			flag = true;
		}
		// PER AMBITO
		if (!StringUtils.isEmpty(ambito)) {
			ipiBuf = createAmbitoQuery(ambito);
			if (ipiBuf.length() > 0) {
				fq.append(flag ? " AND (" : "(")
					.append(ipiBuf).append(")");
				flag = true;
			}
		}
		// PER ARTISTA
		if (!StringUtils.isEmpty(artista)) {
			ipiBuf = createArtistaQuery(artista, qualifica);
			if (ipiBuf.length() > 0) {
				fq.append(flag ? " AND (" : "(")
					.append(ipiBuf).append(")");
//				flag = true;
			}
		}
		return fq.toString();
	}
	
	private StringBuffer createTitoloQuery(String titolo) {
		final StringBuffer fq = new StringBuffer();
		if (!StringUtils.isEmpty(titolo)) {
			// exact match
			fq.append("(" + SolrSchema.titoloRicerca + ":\"" + titolo + "\"^100)");
			// prefix
			fq.append(" OR ({!prefix f=" + SolrSchema.titoloRicercaPrefix + " v=\'" + titolo.toLowerCase() + "\'}^40)");
			// contains
			String[] titoli = titolo.split(" ");
			fq.append(" OR (")
				.append(SolrSchema.titoloRicerca + ":\"" + titoli[0] + "\"");
			for (int i = 1; i < titoli.length; i++) {
				fq.append(" AND ")
					.append(SolrSchema.titoloRicerca + ":\"" + titoli[i] + "\"");
			}
			fq.append(")");
		}
		return fq;
	}
	
	private StringBuffer createArtistaQuery(String artista, String qualifica) {
		final StringBuffer fq = new StringBuffer();
		boolean flag = false;
		// EXACT MATCH
		if (qualifica.contains(TipologiaArtista.Compositore.getText().toLowerCase())) {
			fq.append(SolrSchema.ipi_name_compositori + ":\"" + artista + "\"^100");
			flag = true;
		}
		if (qualifica.contains(TipologiaArtista.Autore.getText().toLowerCase())) {
			fq.append(flag ? " OR " : "")
				.append(SolrSchema.ipi_name_autori + ":\"" + artista + "\"^80");
			flag = true;
		}
		if (StringUtils.isEmpty(qualifica)) { // don't filter!!
			fq.append(flag ? " OR " : "")
				.append("(" + SolrSchema.ipi_name_compositori + ":\"" + artista + "\"^100" + " OR "
					+ SolrSchema.ipi_name_autori + ":\"" + artista + "\"^80" + " OR " + SolrSchema.interpreti + ":\""
					+ artista + "\"^60)");
			flag = true;
		}
		// PREFIX
		if (qualifica.contains(TipologiaArtista.Compositore.getText().toLowerCase())) {
			fq.append(flag ? " OR " : "")
				.append("({!prefix f=" + SolrSchema.ipi_name_compositoriPrefix + " v=\'" + artista.toLowerCase() + "\'}^50)");
			flag = true;
		}
		if (qualifica.contains(TipologiaArtista.Autore.getText().toLowerCase())) {
			fq.append(flag ? " OR " : "")
				.append("({!prefix f=" + SolrSchema.ipi_name_autoriPrefix + " v=\'" + artista.toLowerCase() + "\'}^50)");
			flag = true;
		}
		if (StringUtils.isEmpty(qualifica)) { // don't filter!!
			fq.append(flag ? " OR " : "")
				.append("({!prefix f=" + SolrSchema.ipi_name_compositoriPrefix + " v=\'" + artista.toLowerCase()
					+ "\'}^50) OR ({!prefix f=" + SolrSchema.ipi_name_autoriPrefix + " v=\'" + artista.toLowerCase()
					+ "\'}^50) OR ({!prefix f=" + SolrSchema.interpretiPrefix + " v=\'" + artista.toLowerCase() + "\'}^50)");
			flag = true;
		}
		// CONTIENE
		final String[] compositori = artista.split(" ");
		if (qualifica.contains(TipologiaArtista.Compositore.getText().toLowerCase())) {
			fq.append(flag ? " OR (" : "(");
			boolean and = false;
			for (String artist : compositori) {
				fq.append(and ? " AND " : "")
					.append(SolrSchema.ipi_name_compositori + ":\"" + artist + "\"");
				and = true;
			}
			fq.append(")");
			flag = true;
		}
		if (qualifica.contains(TipologiaArtista.Autore.getText().toLowerCase())) {
			fq.append(flag ? " OR (" : "(");
			boolean and = false;
			for (String artist : compositori) {
				fq.append(and ? " AND " : "")
					.append(SolrSchema.ipi_name_autori + ":\"" + artist + "\"");
				and = true;
			}
			fq.append(")");
			flag = true;
		}
		if (StringUtils.isEmpty(qualifica)) { // don't filter!!
			fq.append(flag ? " OR (" : "(");
			boolean and = false;
			for (String artist : compositori) {
				fq.append(and ? " AND " : "")
					.append(" (" + SolrSchema.ipi_name_compositori + ":\"" + artist + "\" OR "
						+ SolrSchema.ipi_name_autori + ":\"" + artist + "\" OR " + SolrSchema.interpreti + ":\""
						+ artist + "\")");
				and = true;
			}
			fq.append(")");
//			flag = true;
		}
		return fq;
	}

	private StringBuffer createAmbitoQuery(String ambito) {
		final StringBuffer fq = new StringBuffer();
		final String[] ambiti = ambito.split(",");
		fq.append(SolrSchema.ambito + ":\"" + ambiti[0] + "\"");
		for (int i = 1; i < ambiti.length; i ++) {
			fq.append(" OR " + SolrSchema.ambito + ":\"" + ambiti[i] + "\"");
		}
		return fq;
	}

	private String searchOpereFuzzyQueryCodifica(String titolo, String artista, String ambito, String qualifica) {
		// PER TITOLO
		final StringBuffer fq = new StringBuffer();
		boolean flag = false;
		StringBuffer ipiBuf = createTitoloQueryFuzzy(titolo);
		if (ipiBuf.length() > 0) {
			fq.append("(").append(ipiBuf).append(")");
			flag = true;
		}
		// PER AMBITO
		if (!StringUtils.isEmpty(ambito)) {
			ipiBuf = createAmbitoQuery(ambito);
			if (ipiBuf.length() > 0) {
				fq.append(flag ? " AND (" : "(")
					.append(ipiBuf.toString()).append(")");
				flag = true;
			}
		}
		// PER ARTISTA
		if (!StringUtils.isEmpty(artista)) {
			ipiBuf = createArtistaQueryFuzzy(artista, qualifica);
			if (ipiBuf.length() > 0) {
				fq.append(flag ? " AND (" : "(")
					.append(ipiBuf.toString()).append(")");
//				flag = true;
			}
		}
		return fq.toString();
	}
	
	private StringBuffer createTitoloQueryFuzzy(String titolo) {
		final StringBuffer fq = new StringBuffer();
		String[] titoli = null;
		// String regexOmit3charWords = "\\b[A-Za-z0-9]{1,3}\\b\\s?";
		if (!StringUtils.isEmpty(titolo)) {
			titolo = cleanSearchTerm(titolo);
			titoli = titolo.split(" ");
			titoli = omitWords(titoli, 3); // considerare parole con almeno 4 chars
		}
		if (titoli != null && titoli.length > 0) {
			fq.append(queryOnlyFuzzyWordsInAND(SolrSchema.titoloRicerca.toString(), titoli));
		}
		return fq;
	}
	
	private String[] omitWords(String[] values, int limit) {
		final List<String> list = new ArrayList<String>();
		for (String value : values) {
			if ((value.contains("\'") && (value.length() - 1 > limit)) || (!value.contains("\'") && (value.length() > limit))) {
				list.add(value);
			}
		}
		return list.toArray(new String[list.size()]);
	}
	
	private String queryOnlyFuzzyWordsInAND(String field, String[] words) {
		final StringBuffer fq = new StringBuffer();
		if (null != words && words.length > 0) {
			fq.append("(" + is(field, fuzzy(words[0])));
			for (int i = 1; i < words.length; i++) {
				fq.append(" AND ")
					.append(is(field, fuzzy(words[i])));
			}
			fq.append(")");
		}
		return fq.toString();
	}

	private String is(String field, String value) {
		return field + ":" + value;
	}

	private String fuzzy(String value, float distance) {
		return value + "~" + distance;
	}

	private String fuzzy(String value) {
		return fuzzy(value, 2f);
	}
	
	private StringBuffer createArtistaQueryFuzzy(String artista, String qualifica) {
		final StringBuffer fq = new StringBuffer();
		boolean flag = false;
		if (!StringUtils.isEmpty(artista)) {
			artista = cleanSearchTerm(artista);
			String[] compositori = null;
			compositori = artista.split(" ");
			compositori = omitWords(compositori, 3); // considerare parole con almeno 4 chars
			if (compositori.length > 0) {
				if (qualifica.contains(TipologiaArtista.Compositore.getText().toLowerCase())) {
					fq.append(flag ? " OR " : "")
						.append(queryOnlyFuzzyWordsInAND(SolrSchema.ipi_name_compositori.toString(), compositori));
					flag = true;
				}
				if (qualifica.contains(TipologiaArtista.Autore.getText().toLowerCase())) {
					fq.append(flag ? " OR " : "")
						.append(queryOnlyFuzzyWordsInAND(SolrSchema.ipi_name_autori.toString(), compositori));
					flag = true;
				}
				if (StringUtils.isEmpty(qualifica)) { // don't filter!!
					fq.append(flag ? " OR " : "")
						.append(queryOnlyFuzzyWordsInAND(SolrSchema.ipi_name_compositori.toString(), compositori))
						.append(queryOnlyFuzzyWordsInAND(SolrSchema.ipi_name_autori.toString(), compositori))
						.append(queryOnlyFuzzyWordsInAND(SolrSchema.interpreti.toString(), compositori));
//					flag = true;
				}
			}
		}
		return fq;
	}
	
	private List<Opera> queryCodificaAutomatica( String query, String requestHandler, boolean highlight ) {
		List<Opera> results = null;
		try {		
			final SolrQuery solrQuery = new SolrQuery();
			solrQuery.setRequestHandler(requestHandler);
			solrQuery.setQuery(query);
			solrQuery.setIncludeScore(true);
			if (highlight) {
				solrQuery.setHighlight(highlight);
				solrQuery.setHighlightSimplePre(highlightPrefix);
				solrQuery.setHighlightSimplePost(highlightPostfix);
				solrQuery.setHighlightSnippets(highlightSnippets);
				solrQuery.setHighlightRequireFieldMatch(true);
				solrQuery.addHighlightField(SolrSchema.interpreti.toString());
			}
			final QueryResponse response = solrClient.query(solrQuery, SolrRequest.METHOD.POST);
			results = response.getBeans(Opera.class);
			if (highlight && !results.isEmpty()) {
				final Map<String, Map<String, List<String>>> highlightings = response.getHighlighting();
				for (Opera result : results) {
					final Map<String, List<String>> highlighting = highlightings.get(result.getId());
					if (null != highlighting) {
						final List<String> interpreti = highlighting.get(SolrSchema.interpreti.toString());
						if (null != interpreti && !interpreti.isEmpty()) {
							result.setInterpreti(interpreti);
						}					
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("query: " + query);
			System.err.println("requestHandler: " + requestHandler);
			System.err.println("highlight: " + highlight);
		}
		return results;
	}
	
}
