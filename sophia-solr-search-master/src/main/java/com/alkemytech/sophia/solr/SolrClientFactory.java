package com.alkemytech.sophia.solr;

import java.util.Properties;

import com.alkemytech.sophia.kb.KbClientImpl;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public abstract class SolrClientFactory {

	public static SolrClient getInstance(Properties properties) {
		return new SolrClientImpl(Double.parseDouble(properties.getProperty("codifica.pesoTitolo", "70")),
				Double.parseDouble(properties.getProperty("codifica.pesoCompositore", "30")),
				Double.parseDouble(properties.getProperty("codifica.pesoAutore", "30")),
				Double.parseDouble(properties.getProperty("codifica.pesoInterprete", "25")),
				Integer.parseInt(properties.getProperty("codifica.maxResults", "100")),
				Double.parseDouble(properties.getProperty("codifica.discardConfidenzaMinoreDi", "25")),
				Double.parseDouble(properties.getProperty("codifica.includeRilevanzaMaggioreDi", "0.000000001")),
				Double.parseDouble(properties.getProperty("codifica.includeScoreMaggioreDi", "10.0")),
				properties.getProperty("codifica.nazionalita", ""),
				properties.getProperty("codifica.tipologiaArtista", ""),
				Double.parseDouble(properties.getProperty("codifica.minGradoConfidenzaRicercaEsatta", "50")),
				properties.getProperty("solr.server.collection", "opereMusicaReadAlias"),
				properties.getProperty("solr.server.zkHost", "localhost:2181"),
				Integer.parseInt(properties.getProperty("solr.client.zkClientTimeout", "30000")),
				Integer.parseInt(properties.getProperty("solr.client.zkConnectTimeout", "30000")),
				Integer.parseInt(properties.getProperty("solr.client.httpMaxConnections", "10")),
				properties.getProperty("solr.highlight.prefix", ""),
				properties.getProperty("solr.highlight.postfix", ""),
				Integer.parseInt(properties.getProperty("solr.highlight.snippets", "5")));
	}

	public static SolrClient getInstance(double pesoCampoTitolo, double pesoCampoCompositore,
			double pesoCampoAutore, double pesoCampoInterprete,
			int maxResults, double discardConfidenzaMinoreDi,
			double includeRilevanzaMaggioreDi, double includeScoreMaggioreDi,
			String nazionalita, String tipologiaArtista,
			double minGradoConfidenzaRicercaEsatta, String solrCollection,
			String zkHosts, int zkClientTimeout, int zkConnectTimeout, int httpMaxConnections,
			String highlightPrefix, String highlightPostfix, int highlightSnippets) {
		return new SolrClientImpl(pesoCampoTitolo, pesoCampoCompositore, pesoCampoAutore, pesoCampoInterprete, 
				maxResults, discardConfidenzaMinoreDi, includeRilevanzaMaggioreDi, includeScoreMaggioreDi, nazionalita, 
				tipologiaArtista, minGradoConfidenzaRicercaEsatta, solrCollection, zkHosts, zkClientTimeout, 
				zkConnectTimeout, httpMaxConnections, highlightPrefix, highlightPostfix, highlightSnippets);
	}
	
	public static SolrClient getInstance(int httpMaxConnections,
			String endPoint) {
		return new KbClientImpl(httpMaxConnections,endPoint);
	}
}
