package com.alkemytech.sophia.solr;

public class Levenshtein {

	Integer distance;
	Integer maxLength;

	public Levenshtein(Integer distance, Integer maxLength) {
		this.distance = distance;
		this.maxLength = maxLength;
	}

	public Integer getDistance() {
		return distance;
	}

	public Integer getMaxLength() {
		return maxLength;
	}
	
}
