package com.alkemytech.sophia.solr;

public enum SolrSchema {

	codiceOpera, 
//	codiceOperaRoot,
//	codiceOperaProgressive,
	posizioneSIAE,
	rilevanza,
	titoloOT,
	titoloOTPrefix,
	titoliAT,
	titoliATPrefix,
	titoliAT_MCDEM,
	titoliAT_DRM,
	titoloRicerca,
	titoloRicercaPrefix,
	AADD_AO,
	AADD_CO_CA,
	AADD_ALTRI,
	artisti,
	artistiPrefix,
	compositori,
	autori,
	interpreti,
	interpretiPrefix,
	ipi_name_number_compositori,
	ipi_name_number_autori,
	ipi_name_compositori,
	ipi_name_autori,
	ipi_name_compositoriPrefix,
	ipi_name_autoriPrefix,
	ambito,
	qualificaRicercata

}
