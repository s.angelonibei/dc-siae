package com.alkemytech.sophia.solr;

public enum TipologiaArtista {

	Interprete( "Interprete" ), Autore( "Autore" ), Compositore( "Compositore" );

	private String text;

	TipologiaArtista( String text ) {
		this.text = text;
	}

	@Override
	public String toString() {
		return text;
	}

	public String getText() {
		return text;
	}

	public static TipologiaArtista valueOfDescription( String description ) {
		for ( TipologiaArtista v : values() ) {
			if ( v.toString().equals( description ) ) { return v; }
		}
		throw new IllegalArgumentException( "No enum const " + TipologiaArtista.class + "@description." + description );
	}

}
