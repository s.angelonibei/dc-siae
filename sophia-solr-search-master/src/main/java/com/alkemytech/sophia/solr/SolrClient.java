package com.alkemytech.sophia.solr;

import java.util.List;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public interface SolrClient {

	public List<SearchResult> searchOpereCodifica(String title, String artists, String roles);
	public List<SearchResult> searchOpereCodificaFuzzy(String title, String artists, String roles);
	public boolean checkGradoConfidenzaMinimo(List<SearchResult> results);
	
}
