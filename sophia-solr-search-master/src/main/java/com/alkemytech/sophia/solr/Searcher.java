package com.alkemytech.sophia.solr;

import java.util.List;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public interface Searcher {

	public String getMatchType();
	public List<Opera> search(String titolo, String artisti, String nazionalita, String tipologiaArtista, boolean highlight);

}
