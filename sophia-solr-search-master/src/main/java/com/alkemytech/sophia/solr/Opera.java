package com.alkemytech.sophia.solr;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.solr.client.solrj.beans.Field;

public class Opera {

	@Field
	private String id; // =codiceOpera
	@Field
	private String codiceOpera;
	// example: codiceOpera:94901010800, root:949010108, progressive:00
	// example: codiceOpera:94901010801, root:949010108, progressive:01
	// solr pre-processed
	// @Field
	// private String codiceOperaRoot; // solr pre-processed
	// @Field
	// private String codiceOperaProgressive;// orderBY //solr pre-processed
	@Field
	private Double rilevanza;
	@Field
	private List<String> posizioneSIAE = new ArrayList<String>();
	@Field
	private String titoloOT;
//	@Field
	private List<String> titoliAT = new ArrayList<String>(); // solr pre-processed titoliAT_MCDEM+titoliAT_DRM
	@Field
	private List<String> titoliAT_MCDEM = new ArrayList<String>();
	@Field
	private List<String> titoliAT_DRM = new ArrayList<String>();
	@Field
	private List<String> autore_MCDEM = new ArrayList<String>();
	@Field
	private List<String> AADD_AO = new ArrayList<String>();
	@Field
	private List<String> AADD_CO_CA = new ArrayList<String>();
	@Field
	private List<String> AADD_ALTRI = new ArrayList<String>();
	// autori = {Autore_MCDEM, Autore_DRM, AVENTEDIRITTO AO}
	@Field
	private List<String> autori = new ArrayList<String>();// solr pre-processed AADD_AO, set for unique values
	// compositori = {Autore_MCDEM, AVENTEDIRITTO CO,CA}
	@Field
	private List<String> compositori = new ArrayList<String>();// solr pre-processed AADD_CO_CA+autore_MCDEM, set for unique values
	@Field
	private List<String> interpreti = new ArrayList<String>();
	@Field
	private List<String> artisti = new ArrayList<String>();// solr pre-processed autori+compositori+interpreti, set for unique values
	@Field
	private String flag_pd;
	@Field
	private String flag_el;
	@Field
	private String ambito;
//  WARNING: modified to String by SIAE IT on 2017-02
//	@Field
//	private List<String> titoloRicerca = new ArrayList<String>();
	@Field
	private String titoloRicerca;
	@Field
	private List<String> ipi_name_number_compositori = new ArrayList<String>();
	@Field
	private List<String> ipi_name_number_autori = new ArrayList<String>();
	@Field
	private List<String> ipi_name_compositori = new ArrayList<String>();
	@Field
	private List<String> ipi_name_autori = new ArrayList<String>();
	@Field
	private Float score;
	
	public Opera() {
		super();
	}	

	public Float getScore() {
		return score;
	}

	public void setScore(Float score) {
		this.score = score;
	}

	public String getFlag_pd() {
		return flag_pd;
	}

	public void setFlag_pd(String flag_pd) {
		this.flag_pd = flag_pd;
	}

	public String getFlag_el() {
		return flag_el;
	}

	public void setFlag_el(String flag_el) {
		this.flag_el = flag_el;
	}

	public List<String> getIpi_name_number_compositori() {
		return ipi_name_number_compositori;
	}

	public void setIpi_name_number_compositori(List<String> ipi_name_number_compositori) {
		this.ipi_name_number_compositori = ipi_name_number_compositori;
	}

	public List<String> getIpi_name_number_autori() {
		return ipi_name_number_autori;
	}

	public void setIpi_name_number_autori(List<String> ipi_name_number_autori) {
		this.ipi_name_number_autori = ipi_name_number_autori;
	}

	public List<String> getIpi_name_compositori() {
		return ipi_name_compositori;
	}

	public void setIpi_name_compositori(List<String> ipi_name_compositori) {
		this.ipi_name_compositori = ipi_name_compositori;
	}

	public List<String> getIpi_name_autori() {
		return ipi_name_autori;
	}

	public void setIpi_name_autori(List<String> ipi_name_autori) {
		this.ipi_name_autori = ipi_name_autori;
	}

	public String getAmbito() {
		return ambito;
	}

	public void setAmbito(String ambito) {
		this.ambito = ambito;
	}

	public String getTitoloRicerca() {
		return titoloRicerca;
	}

	public void setTitoloRicerca(String titoloRicerca) {
		this.titoloRicerca = titoloRicerca;
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCodiceOpera() {
		return codiceOpera;
	}

	public void setCodiceOpera(String codiceOpera) {
		this.codiceOpera = codiceOpera;
	}

	public String getTitoloOT() {
		return titoloOT;
	}

	public void setTitoloOT(String titoloOT) {
		this.titoloOT = titoloOT;
	}

	public Double getRilevanza() {
		return rilevanza;
	}

	public void setRilevanza(Double rilevanza) {
		this.rilevanza = rilevanza;
	}

	public List<String> getPosizioneSIAE() {
		return posizioneSIAE;
	}

	public void setPosizioneSIAE(List<String> posizioneSIAE) {
		this.posizioneSIAE = posizioneSIAE;
	}

	public List<String> getTitoliAT() {
		return titoliAT;
	}

	@Field
	public void setTitoliAT(List<String> titoliAT) {
		this.titoliAT = titoliAT;
	}

	public List<String> getTitoliAT_MCDEM() {
		return titoliAT_MCDEM;
	}

	public void setTitoliAT_MCDEM(List<String> titoliAT_MCDEM) {
		this.titoliAT_MCDEM = titoliAT_MCDEM;
	}

	public List<String> getTitoliAT_DRM() {
		return titoliAT_DRM;
	}

	public void setTitoliAT_DRM(List<String> titoliAT_DRM) {
		this.titoliAT_DRM = titoliAT_DRM;
	}

	public List<String> getAutore_MCDEM() {
		return autore_MCDEM;
	}

	public void setAutore_MCDEM(List<String> autore_MCDEM) {
		this.autore_MCDEM = autore_MCDEM;
	}

	public List<String> getAADD_AO() {
		return AADD_AO;
	}

	public void setAADD_AO(List<String> aADD_AO) {
		AADD_AO = aADD_AO;
	}

	public List<String> getAADD_CO_CA() {
		return AADD_CO_CA;
	}

	public void setAADD_CO_CA(List<String> aADD_CO_CA) {
		AADD_CO_CA = aADD_CO_CA;
	}

	public List<String> getAADD_ALTRI() {
		return AADD_ALTRI;
	}

	public void setAADD_ALTRI(List<String> aADD_ALTRI) {
		AADD_ALTRI = aADD_ALTRI;
	}

	public List<String> getAutori() {
		return autori;
	}

	public void setAutori(List<String> autori) {
		this.autori = autori;
	}

	public List<String> getCompositori() {
		return compositori;
	}

	public void setCompositori(List<String> compositori) {
		this.compositori = compositori;
	}

	public List<String> getInterpreti() {
		return interpreti;
	}

	public void setInterpreti(List<String> interpreti) {
		this.interpreti = interpreti;
	}

	public List<String> getArtisti() {
		return artisti;
	}

	public void setArtisti(List<String> artisti) {
		this.artisti = artisti;
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((AADD_ALTRI == null) ? 0 : AADD_ALTRI.hashCode());
		result = prime * result + ((AADD_AO == null) ? 0 : AADD_AO.hashCode());
		result = prime * result + ((AADD_CO_CA == null) ? 0 : AADD_CO_CA.hashCode());
		result = prime * result + ((artisti == null) ? 0 : artisti.hashCode());
		result = prime * result + ((autore_MCDEM == null) ? 0 : autore_MCDEM.hashCode());
		result = prime * result + ((autori == null) ? 0 : autori.hashCode());
		result = prime * result + ((codiceOpera == null) ? 0 : codiceOpera.hashCode());
		result = prime * result + ((compositori == null) ? 0 : compositori.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((interpreti == null) ? 0 : interpreti.hashCode());
		result = prime * result + ((posizioneSIAE == null) ? 0 : posizioneSIAE.hashCode());
		result = prime * result + ((rilevanza == null) ? 0 : rilevanza.hashCode());
		result = prime * result + ((titoliAT == null) ? 0 : titoliAT.hashCode());
		result = prime * result + ((titoliAT_DRM == null) ? 0 : titoliAT_DRM.hashCode());
		result = prime * result + ((titoliAT_MCDEM == null) ? 0 : titoliAT_MCDEM.hashCode());
		result = prime * result + ((titoloOT == null) ? 0 : titoloOT.hashCode());
		result = prime * result + ((ambito == null) ? 0 : ambito.hashCode());
		result = prime * result + ((flag_el == null) ? 0 : flag_el.hashCode());
		result = prime * result + ((flag_pd == null) ? 0 : flag_pd.hashCode());
		result = prime * result + ((ipi_name_autori == null) ? 0 : ipi_name_autori.hashCode());
		result = prime * result + ((ipi_name_compositori == null) ? 0 : ipi_name_compositori.hashCode());
		result = prime * result + ((ipi_name_number_autori == null) ? 0 : ipi_name_number_autori.hashCode());
		result = prime * result + ((ipi_name_number_compositori == null) ? 0 : ipi_name_number_compositori.hashCode());
		result = prime * result + ((titoloRicerca == null) ? 0 : titoloRicerca.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Opera other = (Opera) obj;
		
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;

		if (codiceOpera == null) {
			if (other.codiceOpera != null)
				return false;
		} else if (!codiceOpera.equals(other.codiceOpera))
			return false;

		if (rilevanza == null) {
			if (other.rilevanza != null)
				return false;
		} else if (!rilevanza.equals(other.rilevanza))
			return false;

		if (titoloOT == null) {
			if (other.titoloOT != null)
				return false;
		} else if (!titoloOT.equals(other.titoloOT))
			return false;
		
		if (ambito == null) {
			if (other.ambito != null)
				return false;
		} else if (!ambito.equals(other.ambito))
			return false;
		
		if (flag_el == null) {
			if (other.flag_el != null)
				return false;
		} else if (!flag_el.equals(other.flag_el))
			return false;
		
		if (flag_pd == null) {
			if (other.flag_pd != null)
				return false;
		} else if (!flag_pd.equals(other.flag_pd))
			return false;
		
		if (ipi_name_autori == null) {
			if (other.ipi_name_autori != null)
				return false;
		} else if (!ipi_name_autori.equals(other.ipi_name_autori))
			return false;
		
		if (ipi_name_compositori == null) {
			if (other.ipi_name_compositori != null)
				return false;
		} else if (!ipi_name_compositori.equals(other.ipi_name_compositori))
			return false;
		
		if (ipi_name_number_autori == null) {
			if (other.ipi_name_number_autori != null)
				return false;
		} else if (!ipi_name_number_autori.equals(other.ipi_name_number_autori))
			return false;
		if (ipi_name_number_compositori == null) {
			if (other.ipi_name_number_compositori != null)
				return false;
			
		} else if (!ipi_name_number_compositori.equals(other.ipi_name_number_compositori))
			return false;
		if (titoloRicerca == null) {
			if (other.titoloRicerca != null)
				return false;
		} else if (!titoloRicerca.equals(other.titoloRicerca))
			return false;
		
		if (!CollectionUtils.isEqualCollection(AADD_ALTRI, other.AADD_ALTRI)) {
			return false;
		}		
		if (!CollectionUtils.isEqualCollection(AADD_AO, other.AADD_AO)) {
			return false;
		}
		if (!CollectionUtils.isEqualCollection(AADD_CO_CA, other.AADD_CO_CA)) {
			return false;
		}
		if (!CollectionUtils.isEqualCollection(titoliAT, other.titoliAT)) {
			return false;
		}
		if (!CollectionUtils.isEqualCollection(titoliAT_DRM, other.titoliAT_DRM)) {
			return false;
		}
		if (!CollectionUtils.isEqualCollection(titoliAT_MCDEM, other.titoliAT_MCDEM)) {
			return false;
		}
		if (!CollectionUtils.isEqualCollection(interpreti, other.interpreti)) {
			return false;
		}
		if (!CollectionUtils.isEqualCollection(autori, other.autori)) {
			return false;
		}
		if (!CollectionUtils.isEqualCollection(compositori, other.compositori)) {
			return false;
		}
		if (!CollectionUtils.isEqualCollection(artisti, other.artisti)) {
			return false;
		}
		if (!CollectionUtils.isEqualCollection(autore_MCDEM, other.autore_MCDEM)) {
			return false;
		}
		if (!CollectionUtils.isEqualCollection(posizioneSIAE, other.posizioneSIAE)) {
			return false;
		}
		
		return true;
	}

}
