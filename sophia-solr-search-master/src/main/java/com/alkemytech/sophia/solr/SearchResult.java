package com.alkemytech.sophia.solr;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class SearchResult {

	private Opera opera;
	private String matchType;
	private Double confidenza;
	private String titolo;
	private String artista;
	private String ruolo;
	
	public Opera getOpera() {
		return opera;
	}
	public SearchResult setOpera(Opera opera) {
		this.opera = opera;
		return this;
	}
	public String getMatchType() {
		return matchType;
	}
	public SearchResult setMatchType(String matchType) {
		this.matchType = matchType;
		return this;
	}
	public Double getConfidenza() {
		return confidenza;
	}
	public SearchResult setConfidenza(Double confidenza) {
		this.confidenza = confidenza;
		return this;
	}
	public String getTitolo() {
		return titolo;
	}
	public SearchResult setTitolo(String titolo) {
		this.titolo = titolo;
		return this;
	}
	public String getArtista() {
		return artista;
	}
	public SearchResult setArtista(String artista) {
		this.artista = artista;
		return this;
	}
	public String getRuolo() {
		return ruolo;
	}
	public SearchResult setRuolo(String ruolo) {
		this.ruolo = ruolo;
		return this;
	}
	@Override
	public int hashCode() {
		return 31 + ((opera.getCodiceOpera() == null) ? 0 : opera.getCodiceOpera().hashCode());
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SearchResult other = (SearchResult) obj;
		if (opera.getCodiceOpera() == null) {
			if (other.opera.getCodiceOpera() != null) {
				return false;
			}
		} else if (!opera.getCodiceOpera().equals(other.opera.getCodiceOpera())) {
			return false;
		}
		return true;
	}
}
