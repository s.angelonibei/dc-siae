-- ESEGUITO 2019-06-30
create table MCMDB_debug.INVOICE_ACCANTONAMENTO
(
  ID_INVOICE_ACCANTONAMENTO int auto_increment primary key,
  ID_INVOICE int not null,
  NOTE varchar(512) null,
  PERCENTUALE decimal(11,2) not null,
  DATA_INSERIMENTO datetime default CURRENT_TIMESTAMP not null,
  DATA_FINE_VALIDITA datetime null,
  UTENTE varchar(32) null,
  IMPORTO_ACCANTONATO decimal(34,20) not null,
  IMPORTO_ACCANTONATO_UTILIZZABILE decimal(12,2) null,
  constraint FK_INVOICE_ACCANTONAMENTO_INVOICE_ID
  foreign key (ID_INVOICE) references MCMDB_debug.INVOICE (ID_INVOICE)
);

create index FK_INVOICE_ACCANTONAMENTO_INVOICE_ID
  on INVOICE_ACCANTONAMENTO (ID_INVOICE);

create table MCMDB_debug.INVOICE_RECLAMO
(
  ID_INVOICE_RECLAMO int auto_increment primary key,
  ID_DSP varchar(100) not null,
  COMMERCIAL_OFFER varchar(100) not null,
  COUNTRY varchar(3) not null,
  DATA_INSERIMENTO datetime default CURRENT_TIMESTAMP not null,
  DATA_ULTIMA_MODIFICA datetime null,
  UTENTE varchar(32) not null,
  PERIOD int not null,
  PERIOD_TYPE varchar(10) not null,
  YEAR int not null,
  STATO int not null,
  ID_DSR varchar(512) not null,
  FILE_LOCATION varchar(512) null,
  FILE_NAME varchar(512) null
);
