-- ESEGUITO PROD 2019-06-30
START TRANSACTION;

INSERT INTO permission (code,parent,url,name,menuVoice)
VALUES
  ('mandatoConfig','muConfigurazione','#!/mandatoConfig','Configurazione Mandati',true);

INSERT INTO role_permission (role,permission)
VALUES
  ('DEVELOPMENT','mandatoConfig'),
  ('BU-MULTIMEDIALE','mandatoConfig'),
  ('IT','mandatoConfig'),
  ('GA','mandatoConfig'),
  ('MU-MULTITERRITORIALE','mandatoConfig');

COMMIT;
