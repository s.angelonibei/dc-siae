create table MM_RIPARTIZIONE_CARICO
(
	ID BIGINT auto_increment PRIMARY KEY ,
	ID_RIPARTIZIONE_SIADA BIGINT not null,
	DATA_ORA_CREAZIONE TIMESTAMP default CURRENT_TIMESTAMP not null,
	VALORE_TOTALE_RIPARTIZIONE DECIMAL(60,10) null,
	STATO VARCHAR(32) not null,
	VERSIONE INT null
);

create unique index MM_RIPARTIZIONE_CARICO_ID_uindex
	on MM_RIPARTIZIONE_CARICO (ID);


create table MM_RIPARTIZIONE_CARICO_CCID
(
  ID BIGINT auto_increment primary key ,
  ID_RIPARTIZIONE_CARICO BIGINT(20) not null,
  ID_INVOICE_ITEM_TO_CCID int(11) unsigned not null,
  constraint MM_RIPARTIZIONE_CARICO_CCID_INVOICE_ITEM_TO_CCID_id_fk
    foreign key (ID_INVOICE_ITEM_TO_CCID) references INVOICE_ITEM_TO_CCID (id),
  constraint MM_RIPARTIZIONE_CARICO_CCID_MM_RIPARTIZIONE_CARICO_ID_fk
    foreign key (ID_RIPARTIZIONE_CARICO) references MM_RIPARTIZIONE_CARICO (ID)
);

create unique index MM_RIPARTIZIONE_CARICO_CCID_ID_uindex
  on MM_RIPARTIZIONE_CARICO_CCID (ID);


create table MM_RIPARTIZIONE_648
(
	ID BIGINT auto_increment primary key ,
	ID_RIPARTIZIONE_SIADA int(11) not null,
	DATA_ORA_CREAZIONE TIMESTAMP default CURRENT_TIMESTAMP not null,
	ID_SOCIETA_TUTELA int(11) not null,
	ID_CCID_METADATA varchar(1024) not null,
	VALORE_LORDO decimal(32,20) not null,
	VALORE_AGGIO_DEM decimal(32,20) not null,
	VALORE_AGGIO_DRM decimal(32,20) not null,
	VALORE_TRATTENUTA decimal(32,20) not null,
	VALORE_NETTO decimal(32,20) not null,
	PATH_S3_648 VARCHAR(1024) not null,
	NUMERO_FATTURA varchar(256) not null,

	constraint MM_RIPARTIZIONE_648_ID_SOCIETA_TUTELA_fk
		foreign key (ID_SOCIETA_TUTELA) references SOCIETA_TUTELA (ID),
);

create unique index MM_RIPARTIZIONE_648_ID_uindex
	on MM_RIPARTIZIONE_648 (ID);



// step monitoring per dividere il vacchio flusso dal nuovo.

alter table steps_monitoring
	add NEW_CLAIM varchar(32) null;

update steps_monitoring set NEW_CLAIM = 'NO';











