create table MCMDB_debug.MM_KB_UPDATE
(
	ID bigint auto_increment
		primary key,
	LAUNCH_ID tinytext not null,
	SERVICE_NAME tinytext not null,
	STATUS enum('to_process', 'started', 'completed', 'failed') null,
	STARTED datetime null,
	FINISHED datetime null
);

create table MM_KB_INGESTION_ISRC
(
	ID BIGINT auto_increment,
	UUID VARCHAR(36) not null,
	ISRC VARCHAR(12) not null,
	INSERTED TINYINT default false not null,
	constraint MM_KB_INGESTION_ISRC_pk
		primary key (ID),
	constraint UUID_ISRC
		unique (UUID, ISRC)
);


