/**SOSTITUIRE DEBUG CON PROD **/

CREATE EXTERNAL TABLE `debug_ccid_v2_14`(
  `record_type` string, 
  `transaction_type` string, 
  `ref_id` string, 
  `correction_reference` string, 
  `sales_transaction_id` string, 
  `work_id` string, 
  `release_id` string, 
  `resource_id` string, 
  `isrc` string, 
  `iswc` string, 
  `workcode` string, 
  `work_title` string, 
  `use_quantity` string, 
  `applied_tariff` string, 
  `royalty_type` string, 
  `revenue_basis` string, 
  `original_release_revenue_basis` string, 
  `original_resource_revenue_basis` string, 
  `royalty` string, 
  `resource_share` string, 
  `restrictions` string, 
  `claim_licensor_combined` string, 
  `claim_copcon_combined` string, 
  `claim_unmatched_combined` string, 
  `claim_pd_combined` string, 
  `claim_not_collected_combined` string, 
  `amount_invoiced_total` string, 
  `claim_licensor_mech` string, 
  `claim_licensor_perf` string, 
  `amount_licensor_mech` string, 
  `amount_licensor_perf` string, 
  `amount_copcon_mech` string, 
  `amount_copcon_perf` string, 
  `amount_pd_mech` string, 
  `amount_pd_perf` string, 
  `amount_not_collected_mech` string, 
  `amount_not_collected_perf` string, 
  `amount_unmatched_mech` string, 
  `amount_unmatched_perf` string, 
  `claim_copcon_mech` string, 
  `claim_copcon_perf` string, 
  `claim_pd_mech` string, 
  `claim_pd_perf` string, 
  `claim_not_collected_mech` string, 
  `claim_not_collected_perf` string, 
  `claim_unmatched_mech` string, 
  `claim_unmatched_perf` string, 
  `idutil` string)
PARTITIONED BY ( 
  `dsp_p` string, 
  `year_p` string, 
  `month_p` string, 
  `dsr_p` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe' 
WITH SERDEPROPERTIES ( 
  'field.delim'='\t', 
  'serialization.format'='\t') 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  's3://siae-sophia-datalake/debug/ccid-v2_14'
tblproperties('skip.header.line.count'='1', 'skip.footer.line.count'='1');




Msck repair table debug_ccid_v2_14;

CREATE EXTERNAL TABLE `prod_ccid_v2_13`(
  `record_type` string, 
  `transaction_type` string, 
  `ref_id` string, 
  `correction_reference` string, 
  `sales_transaction_id` string, 
  `trading_brand` string, 
  `release_id` string, 
  `resource_id` string, 
  `isrc` string, 
  `iswc` string, 
  `workcode` string, 
  `work_title` string, 
  `service_type` string, 
  `use_type` string, 
  `use_quantity` string, 
  `applied_tariff` string, 
  `royalty_type` string, 
  `price_basis` string, 
  `original_price_basis` string, 
  `royalty` string, 
  `music_share` string, 
  `restrictions` string, 
  `fst_license` string, 
  `claim_licensor_combined` string, 
  `claim_pai_combined` string, 
  `claim_unmatched_combined` string, 
  `claim_pd_combined` string, 
  `claim_not_collected_combined` string, 
  `amount_invoiced_total` string, 
  `claim_licensor_mech` string, 
  `claim_licensor_perf` string, 
  `amount_licensor_mech` string, 
  `amount_licensor_perf` string, 
  `amount_pai_mech` string, 
  `amount_pai_perf` string, 
  `amount_pd_mech` string, 
  `amount_pd_perf` string, 
  `amount_not_collected_mech` string, 
  `amount_not_collected_perf` string, 
  `amount_unmatched_mech` string, 
  `amount_unmatched_perf` string, 
  `claim_pai_mech` string, 
  `claim_pai_perf` string, 
  `claim_pd_mech` string, 
  `claim_pd_perf` string, 
  `claim_not_collected_mech` string, 
  `claim_not_collected_perf` string, 
  `claim_unmatched_mech` string, 
  `claim_unmatched_perf` string, 
  `idutil` string)
PARTITIONED BY ( 
  `dsp_p` string, 
  `year_p` string, 
  `month_p` string, 
  `dsr_p` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.OpenCSVSerde' 
WITH SERDEPROPERTIES ( 
  'field.delim'='|', 
  'separatorChar'='|',
  'quoteChar' = '"') 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  's3://siae-sophia-datalake/prod/ccid-v2_13'
tblproperties('skip.header.line.count'='1', 'skip.footer.line.count'='1');


CREATE EXTERNAL TABLE `test_ccid_v2_13`(
  `record_type` string, 
  `transaction_type` string, 
  `ref_id` string, 
  `correction_reference` string, 
  `sales_transaction_id` string, 
  `trading_brand` string, 
  `release_id` string, 
  `resource_id` string, 
  `isrc` string, 
  `iswc` string, 
  `workcode` string, 
  `work_title` string, 
  `service_type` string, 
  `use_type` string, 
  `use_quantity` string, 
  `applied_tariff` string, 
  `royalty_type` string, 
  `price_basis` string, 
  `original_price_basis` string, 
  `royalty` string, 
  `music_share` string, 
  `restrictions` string, 
  `fst_license` string, 
  `claim_licensor_combined` string, 
  `claim_pai_combined` string, 
  `claim_unmatched_combined` string, 
  `claim_pd_combined` string, 
  `claim_not_collected_combined` string, 
  `amount_invoiced_total` string, 
  `claim_licensor_mech` string, 
  `claim_licensor_perf` string, 
  `amount_licensor_mech` string, 
  `amount_licensor_perf` string, 
  `amount_pai_mech` string, 
  `amount_pai_perf` string, 
  `amount_pd_mech` string, 
  `amount_pd_perf` string, 
  `amount_not_collected_mech` string, 
  `amount_not_collected_perf` string, 
  `amount_unmatched_mech` string, 
  `amount_unmatched_perf` string, 
  `claim_pai_mech` string, 
  `claim_pai_perf` string, 
  `claim_pd_mech` string, 
  `claim_pd_perf` string, 
  `claim_not_collected_mech` string, 
  `claim_not_collected_perf` string, 
  `claim_unmatched_mech` string, 
  `claim_unmatched_perf` string, 
  `idutil` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.OpenCSVSerde' 
WITH SERDEPROPERTIES ( 
  'field.delim'='|', 
  'separatorChar'='|',
  'quoteChar' = '"') 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  's3://siae-sophia-datalake/prod/ccid-v2_13/dsp_p=spotify/year_p=2017/month_p=07/dsr_p=spotify_siae_IT_201707_free_dsr_BC0001/'
tblproperties('skip.header.line.count'='1', 'skip.footer.line.count'='1');



Msck repair table debug_ccid_v2_13;

