
INSERT INTO role_permission (`role`, `permission`) VALUES ('GA', 'lavorazioneDSR');
INSERT INTO role_permission (`role`, `permission`) VALUES ('DEVELOPMENT', 'lavorazioneDSR');
INSERT INTO role_permission (`role`, `permission`) VALUES ('GA', 'preparatoryActivities');
INSERT INTO role_permission (`role`, `permission`) VALUES ('DEVELOPMENT', 'preparatoryActivities');
INSERT INTO role_permission (`role`, `permission`) VALUES ('GA', 'royaltiesSearch');
INSERT INTO role_permission (`role`, `permission`) VALUES ('IT', 'royaltiesSearch');
INSERT INTO role_permission (`role`, `permission`) VALUES ('BU-MULTIMEDIALE', 'royaltiesSearch');
INSERT INTO role_permission (`role`, `permission`) VALUES ('DEVELOPMENT', 'royaltiesSearch');
