create table if not exists MM_INVIO_CCID
(
	ID bigint auto_increment primary key,
	ID_CCID_METDATA bigint not null,
	PATH_SCARICO varchar(1024)  null,
	PATH_CONSEGNA varchar(1024)  null,
	SCARICATO bit(1),
	CONFERMATO bit(1),
	CONSEGNATO bit(1),
	DATA_SCARICO datetime  null,
	DATA_CONFERMA datetime  null,
	DATA_CONSEGNA datetime  null,
	ERRORE_CONSEGNA varchar(2048) null
	);

create unique index MM_INVIO_CCID_ID_CCID_METDATA_uindex
	on MM_INVIO_CCID (ID_CCID_METDATA);

alter table MM_INVIO_CCID
	add constraint MM_INVIO_CCID_ID_CCID_METADATA_fk
		foreign key (ID_CCID_METDATA) references CCID_METADATA (ID_CCID_METADATA);




create table if not exists MM_MODALITA_INVIO_CCID
(
	ID bigint auto_increment primary key,
	IDDSP varchar(100) not null,
	HOST varchar(1024) not null,
	PORT varchar(1024) not null,
	PROTOCOL varchar(100) not  null,
	USER varchar(100) not null,
	PWD varchar(100)  null,
	CERT_PATH varchar(1024)  null);

create unique index MM_INVIO_CCID_ID_CCID_METDATA_uindex
	on MM_MODALITA_INVIO_CCID (IDDSP);

alter table MM_MODALITA_INVIO_CCID
	add constraint MM_MODALITA_INVIO_CCID_IDDSP_fk
		foreign key (IDDSP) references ANAG_DSP (IDDSP);



create table if not exists MM_DESTINAZIONE_INVIO_CCID
(
	ID bigint auto_increment primary key,
	IDDSP varchar(100) not null,
	UTILIZATION_TYPE varchar(20) not null,
	COMMERCIAL_OFFERS varchar(20) not null,
	PATH varchar(256) not  null,
	RANK int(11) not  null);

create unique index MM_INVIO_CCID_ID_DSP_uindex
	on MM_DESTINAZIONE_INVIO_CCID (IDDSP);

alter table MM_DESTINAZIONE_INVIO_CCID
	add constraint MM_DESTINAZIONE_INVIO_CCID_IDDSP_fk
		foreign key (IDDSP) references ANAG_DSP (IDDSP);

create unique index MM_DESTINAZIONE_INVIO_CCID_UNIQUE_01
	on MM_DESTINAZIONE_INVIO_CCID (IDDSP,UTILIZATION_TYPE, COMMERCIAL_OFFERS);

insert into role_permission (role, permission) VALUES ('DEVELOPMENT','deliverCcid');
insert into role_permission (role, permission) VALUES ('DEVELOPMENT','configureDeliverCcid');
