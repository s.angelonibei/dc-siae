create table if not exists DSR_METADATA_FILE
(
	IDDSR varchar(512) not null
		primary key,
	FILE_PATH varchar(2048) not null,
	INSERT_TIME datetime default CURRENT_TIMESTAMP not null
);

create index idx_DSR_METADATA_FILE_8055281b0dd3dffb
	on DSR_METADATA_FILE (IDDSR);

alter table DSR_METADATA_FILE
	add FORCED bool default false not null;


delete from DSR_METADATA_FILE where forced = 1;
insert into DSR_METADATA_FILE (IDDSR, FILE_PATH,INSERT_TIME,FORCED)
select DSR_METADATA.IDDSR,'',now() - interval 10 year,true from DSR_METADATA left join DSR_METADATA_FILE on DSR_METADATA.IDDSR =DSR_METADATA_FILE.IDDSR where DSR_METADATA_FILE.IDDSR is null;

alter table DSR_STEPS_MONITORING
	add IDENTIFY_KB_VERSION int null;

alter table DSR_STEPS_MONITORING
	add ERROR_MESSAGE text null;


alter table DSR_METADATA_FILE
	add LAST_START_TIME datetime null;


alter table DSR_STEPS_MONITORING modify IDENTIFY_KB_VERSION varchar(256) null;


create table if not exists MCMDB_debug.MM_NOTIFICA_EMAIL
(
	ID int auto_increment
		primary key,
	ID_LANCIO varchar(512) null,
	IDDSP varchar(100) not null,
	EMAIL_LIST text not null,
	TIPO int not null,
	INSERT_TIME datetime default CURRENT_TIMESTAMP null
);














