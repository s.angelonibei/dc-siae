create table BDC_AMOUNT_SAP_FILE
(
	ID int auto_increment
		primary key,
	PATH_S3 text null,
	PATH_SIAE text null,
	PROCESS_ID varchar(255) not null,
	STATUS varchar(20) not null,
	REQUEST_DATE timestamp null,
	RESPONSE_DATE timestamp null,
	UPLOAD_DATE timestamp null,
	ERROR_MESSAGE text null
);

create table BDC_INGESTION_SAP
(
	ID int auto_increment
		primary key,
	TRIPLETTA_CANALE_PERIODO_ID int null,
	TESTO_TESTATA_DOCUMENTO text null,
	IMPORTO_TOTALE_DOCUMENTO varchar(255) not null,
	DATA_DA date null,
	DATA_A date null,
	ES_MESE varchar(255) null,
	DATA_DI_REGISTRAZIONE datetime null,
	CHIAVE_RIF2 varchar(255) not null,
	TESTO text null,
	DATA_DI_INSERIMENTO datetime null,
	TIPO_DI_INSERIMENTO varchar(255) null,
	AMOUNT_SAP_FILE_ID int not null,
	PREALLOCAZIONE_ID int null,
	PREALLOCAZIONE_PERCENTUALE double null,
	STATO varchar(32) not null,
	ERRORE text null,
	TIPO_DIRITTO varchar(32) null,
	TRIPLETTA varchar(255) null,
	IMPORTO_RELATIVO double null,
	DATA_DOCUMENTO varchar(64) null,
	RECORD_SAP text null,
	ATTRIBUZIONE varchar(255) null,
	CHIAVE_RIF3 varchar(255) null,
	CONTO varchar(255) null,
	constraint BDC_INGESTION_SAP_BDC_AMMOUNT_SAP_FILE_ID_fk
		foreign key (AMOUNT_SAP_FILE_ID) references BDC_AMOUNT_SAP_FILE (ID),
	constraint BDC_INGESTION_SAP_BDC_PIANO_DEI_CONTI_ID_BDC_PIANO_DEI_CONTI_fk
		foreign key (PREALLOCAZIONE_ID) references BDC_PIANO_DEI_CONTI (ID_BDC_PIANO_DEI_CONTI),
	constraint BDC_INGESTION_SAP_BDC_TRIPLETTA_CANALE_PERIODO_ID_fk
		foreign key (TRIPLETTA_CANALE_PERIODO_ID) references BDC_TRIPLETTA_CANALE_PERIODO (ID)
);
alter table BDC_INGESTION_SAP modify PREALLOCAZIONE_PERCENTUALE DECIMAL(6,2) null;
alter table BDC_INGESTION_SAP modify IMPORTO_RELATIVO DECIMAL(14,4) null;
alter table BDC_INGESTION_SAP
	add TRIPLETTA_ID int null;

	alter table BDC_INGESTION_SAP
		add constraint BDC_INGESTION_SAP_BDC_TRIPLETTA_ID_fk
				foreign key (TRIPLETTA_ID) references BDC_TRIPLETTA (ID);

create table BDC_TRIPLETTA
(
	ID int auto_increment
		primary key,
	TRIPLETTA varchar(64) not null,
	ATTRIBUZIONE varchar(16) not null,
	CHIAVE_RIF3 varchar(16) not null,
	CONTO varchar(16) null,
	TIPO_DIRITTO varchar(4) not null,
	RIF_TEMPORALE_INCASSI varchar(32) not null,
	PREALLOCAZIONE_INCASSI_DA varchar(16) not null,
	PREALLOCAZIONE_INCASSI_A varchar(16) not null,
	DESCRIZIONE_SAP varchar(256) null,
	DATA_INSERIMENTO timestamp not null,
	TIPO_INSERIMENTO varchar(255) not null,
	AMMOUNT_SAP_FILE_ID int null,
	STATO varchar(16) not null
);

create table BDC_TRIPLETTA_CANALE_PERIODO
(
	ID int auto_increment
		primary key,
	CANALE_ID int not null,
	TRIPLETTA_ID int not null,
	ATTIVO_DAL datetime null,
	ATTIVO_AL datetime null,
	PREALLOCAZIONE_ID int null,
	PREALLOCAZIONE_PERCENTUALE int null,
	SINGOLO_VS_VASCA varchar(30) not null,
	NUMERO_CANALI_VASCA int null,
	NOMI_CANALI_VASCA text null,
	DATA_PREALLOCAZIONE date null,
	DATA_DISATTIVAZIONE datetime null,
	UTENTE_DISATTIVAZIONE varchar(32) null,
	constraint BDC_TRIPLETTA_CANALE_PERIODO_BDC_PIANO_DEI_CONTI_ID_fk
		foreign key (PREALLOCAZIONE_ID) references BDC_PIANO_DEI_CONTI (ID_BDC_PIANO_DEI_CONTI),
	constraint BDC_TRIPLETTA_CANALE_PERIODO_BDC_TRIPLETTA_ID_fk
		foreign key (TRIPLETTA_ID) references BDC_TRIPLETTA (ID),
	constraint BDC_TRIPLETTA_CANALE_PERIODO_bdc_canali_id_fk
		foreign key (CANALE_ID) references bdc_canali (id)
);
alter table BDC_TRIPLETTA_CANALE_PERIODO modify PREALLOCAZIONE_PERCENTUALE DECIMAL(6,2) null;
alter table BDC_TRIPLETTA_CANALE_PERIODO
	add UTENTE_INSERIMENTO varchar(32) null;

create table TMP_TRIPLETTA
(
	TRIPLETTA text null,
	RAGIONE_SOCIALE_EMITTENTE text null,
	NOME_EMITTENTE text null,
	CANALE text null,
	CANALE_MINORE_VS_BIG text null,
	TIPO_TRASMISSIONE text null,
	TIPO_DIRITTO text null,
	NOTE text null,
	SINGOLO_VS_VASCA text null,
	RIF_TEMPORALE_INCASSI text null,
	PREALLOCAZIONE_DA text null,
	PREALLOCAZIONE_A text null,
	CANALI_VASCA text null,
	NUMERO_CANALI_VASCA text null,
	ATTRIBUZIONE text null,
	CHIAVE_RIF3 text null,
	CONTO text null,
	DESCRIZIONE_SAP text null,
	ATTIVO text null,
	ATTIVO_DA text null,
	ATTIVO_A text null
);

create table TMP_TRIPLETTA_CANALE
(
	TRIPLETTA text null,
	ID_BROADCASTER int null,
	ID_CANALE int null,
	NOME_BROADCASTER_SAP text null,
	NOME_CANALE_SAP text null,
	NOME_BROADCASTER_SOPHIA text null,
	NOME_CANALE_SOPHIA text null,
	FLG_CON_UTENTI int null,
	ID int null,
	TIPO text null

);


