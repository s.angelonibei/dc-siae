\\Insert delle utenze

USE MCMDB_debug;

insert into user_role(username, password, roles)
values('roberto.stella', 'a8d3c8201243e3257c7ae2389943e216', 'DEVELOPMENT');

insert into user_role(username, password, roles)
values('utenteDirettore', 'a8d3c8201243e3257c7ae2389943e216', 'PERF_DIRETTORE');

insert into user_role(username, password, roles)
values('utenteFunzionario', 'a8d3c8201243e3257c7ae2389943e216', 'PERF_FUNZIONARIO');

insert into user_role(username, password, roles)
values('utenteCoordinatore', 'a8d3c8201243e3257c7ae2389943e216', 'PERF_COORDINATORE');

insert into user_role(username, password, roles)
values('utenteOperatore', 'a8d3c8201243e3257c7ae2389943e216', 'PERF_OPERATORE');

insert into user_role(username, password, roles)
values('codificatoreBase', 'a8d3c8201243e3257c7ae2389943e216', 'CODIFICATOREBASE');

insert into user_role(username, password, roles)
values('codificatoreEsperto', 'a8d3c8201243e3257c7ae2389943e216', 'CODIFICATORESPERTO');

\\insert permessi dei ruoli

INSERT INTO role_permission(role, permission) values ('PERF_OPERATORE', 'prVisualizzazioneEventi');

INSERT INTO role_permission(role, permission) values ('PERF_OPERATORE', 'prVisualizzazioneMovimenti');

INSERT INTO role_permission(role, permission) values ('PERF_OPERATORE', 'prVisualizzazioneTracciamentoPM');

INSERT INTO role_permission(role, permission) values ('PERF_OPERATORE', 'prVisualizzazioneFatture');

INSERT INTO role_permission(role, permission) values ('PERF_OPERATORE', 'prRiconciliazioneImporti');

INSERT INTO role_permission(role, permission) values ('PERF_OPERATORE', 'prAggiornamentoSun');

INSERT INTO role_permission(role, permission) values ('PERF_OPERATORE', 'prCodificaManualeEsperto');

INSERT INTO role_permission(role, permission) values ('PERF_OPERATORE', 'prCodificaManualeBase');

INSERT INTO role_permission(role, permission) values ('PERF_OPERATORE', 'prCodificaManualeBase');

INSERT INTO role_permission(role, permission) values ('PERF_OPERATORE', 'prMonitoraggioMp');

INSERT INTO role_permission(role, permission) values ('PERF_OPERATORE', 'prCruscotti');

INSERT INTO role_permission(role, permission) values ('PERF_OPERATORE', 'prCodifica');

INSERT INTO role_permission(role, permission) values ('PERF_OPERATORE', 'prRadioInStore');

INSERT INTO role_permission(role, permission) values ('PERF_OPERATORE', 'guPerforming');

INSERT INTO role_permission(role, permission) values ('PERF_COORDINATORE', 'prVisualizzazioneEventi');

INSERT INTO role_permission(role, permission) values ('PERF_COORDINATORE', 'prVisualizzazioneMovimenti');

INSERT INTO role_permission(role, permission) values ('PERF_COORDINATORE', 'prVisualizzazioneTracciamentoPM');

INSERT INTO role_permission(role, permission) values ('PERF_COORDINATORE', 'prVisualizzazioneFatture');

INSERT INTO role_permission(role, permission) values ('PERF_COORDINATORE', 'prRiconciliazioneImporti');

INSERT INTO role_permission(role, permission) values ('PERF_COORDINATORE', 'prAggiornamentoSun');

INSERT INTO role_permission(role, permission) values ('PERF_COORDINATORE', 'prCodificaManualeEsperto');

INSERT INTO role_permission(role, permission) values ('PERF_COORDINATORE', 'prCodificaManualeBase');

INSERT INTO role_permission(role, permission) values ('PERF_COORDINATORE', 'prCodificaManualeBase');

INSERT INTO role_permission(role, permission) values ('PERF_COORDINATORE', 'prMonitoraggioMp');

INSERT INTO role_permission(role, permission) values ('PERF_COORDINATORE', 'prRegoleRipartizione');

INSERT INTO role_permission(role, permission) values ('PERF_COORDINATORE', 'prGestioneMp');

INSERT INTO role_permission(role, permission) values ('PERF_COORDINATORE', 'prConfigurazioneCampionamento');

INSERT INTO role_permission(role, permission) values ('PERF_COORDINATORE', 'prEsecuzioneCompionamento');

INSERT INTO role_permission(role, permission) values ('PERF_COORDINATORE', 'prCampionamentoFlussoGuida');

INSERT INTO role_permission(role, permission) values ('PERF_COORDINATORE', 'prStoricoCampionamento');

INSERT INTO role_permission(role, permission) values ('PERF_COORDINATORE', 'prStoricoEsecuzioni');

INSERT INTO role_permission(role, permission) values ('PERF_COORDINATORE', 'prCampionamento');

INSERT INTO role_permission(role, permission) values ('PERF_COORDINATORE', 'prCruscotti');

INSERT INTO role_permission(role, permission) values ('PERF_COORDINATORE', 'prCodifica');

INSERT INTO role_permission(role, permission) values ('PERF_COORDINATORE', 'prRadioInStore');

INSERT INTO role_permission(role, permission) values ('PERF_COORDINATORE', 'prGestioneMegaconcerti');

INSERT INTO role_permission(role, permission) values ('PERF_COORDINATORE', 'guPerforming');

INSERT INTO role_permission(role, permission) values ('PERF_COORDINATORE', 'prScodifica');

INSERT INTO role_permission(role, permission) values ('PERF_FUNZIONARIO', 'guPerforming');

INSERT INTO role_permission(role, permission) values ('PERF_FUNZIONARIO', 'prVisualizzazioneEventi');

INSERT INTO role_permission(role, permission) values ('PERF_FUNZIONARIO', 'prVisualizzazioneMovimenti');

INSERT INTO role_permission(role, permission) values ('PERF_FUNZIONARIO', 'prVisualizzazioneTracciamentoPM');

INSERT INTO role_permission(role, permission) values ('PERF_FUNZIONARIO', 'prVisualizzazioneFatture');

INSERT INTO role_permission(role, permission) values ('PERF_FUNZIONARIO', 'prRiconciliazioneImporti');

INSERT INTO role_permission(role, permission) values ('PERF_FUNZIONARIO', 'prAggiornamentoSun');

INSERT INTO role_permission(role, permission) values ('PERF_FUNZIONARIO', 'prCodificaManualeEsperto');

INSERT INTO role_permission(role, permission) values ('PERF_FUNZIONARIO', 'prCodificaManualeBase');

INSERT INTO role_permission(role, permission) values ('PERF_FUNZIONARIO', 'prCodificaManualeBase');

INSERT INTO role_permission(role, permission) values ('PERF_FUNZIONARIO', 'prMonitoraggioCodificato');

INSERT INTO role_permission(role, permission) values ('PERF_FUNZIONARIO', 'prMonitoraggioMp');

INSERT INTO role_permission(role, permission) values ('PERF_FUNZIONARIO', 'prRegoleRipartizione');

INSERT INTO role_permission(role, permission) values ('PERF_FUNZIONARIO', 'prGestioneMp');

INSERT INTO role_permission(role, permission) values ('PERF_FUNZIONARIO', 'prConfigurazioneCampionamento');

INSERT INTO role_permission(role, permission) values ('PERF_FUNZIONARIO', 'prEsecuzioneCompionamento');

INSERT INTO role_permission(role, permission) values ('PERF_FUNZIONARIO', 'prCampionamentoFlussoGuida');

INSERT INTO role_permission(role, permission) values ('PERF_FUNZIONARIO', 'prStoricoCampionamento');

INSERT INTO role_permission(role, permission) values ('PERF_FUNZIONARIO', 'prStoricoEsecuzioni');

INSERT INTO role_permission(role, permission) values ('PERF_FUNZIONARIO', 'prConfigurazioniSoglie');

INSERT INTO role_permission(role, permission) values ('PERF_FUNZIONARIO', 'prCarichiRipartizione');

INSERT INTO role_permission(role, permission) values ('PERF_FUNZIONARIO', 'prKPICodifica');

INSERT INTO role_permission(role, permission) values ('PERF_FUNZIONARIO', 'prMonitoringOperatoriCodifica');

INSERT INTO role_permission(role, permission) values ('PERF_FUNZIONARIO', 'prCampionamento');

INSERT INTO role_permission(role, permission) values ('PERF_FUNZIONARIO', 'prCruscotti');

INSERT INTO role_permission(role, permission) values ('PERF_FUNZIONARIO', 'prScodifica');

INSERT INTO role_permission(role, permission) values ('PERF_FUNZIONARIO', 'prCodifica');

INSERT INTO role_permission(role, permission) values ('PERF_FUNZIONARIO', 'prRadioInStore');

INSERT INTO role_permission(role, permission) values ('PERF_FUNZIONARIO', 'prGestioneMegaconcerti');

INSERT INTO role_permission(role, permission) values ('PERF_DIRETTORE', 'prVisualizzazioneEventi');

INSERT INTO role_permission(role, permission) values ('PERF_DIRETTORE', 'prVisualizzazioneMovimenti');

INSERT INTO role_permission(role, permission) values ('PERF_DIRETTORE', 'prVisualizzazioneTracciamentoPM');

INSERT INTO role_permission(role, permission) values ('PERF_DIRETTORE', 'prVisualizzazioneFatture');

INSERT INTO role_permission(role, permission) values ('PERF_DIRETTORE', 'prRiconciliazioneImporti');

INSERT INTO role_permission(role, permission) values ('PERF_DIRETTORE', 'prAggiornamentoSun');

INSERT INTO role_permission(role, permission) values ('PERF_DIRETTORE', 'prCodificaManualeEsperto');

INSERT INTO role_permission(role, permission) values ('PERF_DIRETTORE', 'prCodificaManualeBase');

INSERT INTO role_permission(role, permission) values ('PERF_DIRETTORE', 'prCodificaManualeBase');

INSERT INTO role_permission(role, permission) values ('PERF_DIRETTORE', 'prMonitoraggioCodificato');

INSERT INTO role_permission(role, permission) values ('PERF_DIRETTORE', 'prMonitoraggioMp');

INSERT INTO role_permission(role, permission) values ('PERF_DIRETTORE', 'prRegoleRipartizione');

INSERT INTO role_permission(role, permission) values ('PERF_DIRETTORE', 'prGestioneMp');

INSERT INTO role_permission(role, permission) values ('PERF_DIRETTORE', 'prConfigurazioneCampionamento');

INSERT INTO role_permission(role, permission) values ('PERF_DIRETTORE', 'prEsecuzioneCompionamento');

INSERT INTO role_permission(role, permission) values ('PERF_DIRETTORE', 'prCampionamentoFlussoGuida');

INSERT INTO role_permission(role, permission) values ('PERF_DIRETTORE', 'prStoricoCampionamento');

INSERT INTO role_permission(role, permission) values ('PERF_DIRETTORE', 'prStoricoEsecuzioni');

INSERT INTO role_permission(role, permission) values ('PERF_DIRETTORE', 'prConfigurazioniSoglie');

INSERT INTO role_permission(role, permission) values ('PERF_DIRETTORE', 'prCarichiRipartizione');

INSERT INTO role_permission(role, permission) values ('PERF_DIRETTORE', 'prKPICodifica');

INSERT INTO role_permission(role, permission) values ('PERF_DIRETTORE', 'prMonitoringOperatoriCodifica');

INSERT INTO role_permission(role, permission) values ('PERF_DIRETTORE', 'prCampionamento');

INSERT INTO role_permission(role, permission) values ('PERF_DIRETTORE', 'prCruscotti');

INSERT INTO role_permission(role, permission) values ('PERF_DIRETTORE', 'prCodifica');

INSERT INTO role_permission(role, permission) values ('PERF_DIRETTORE', 'prRadioInStore');

INSERT INTO role_permission(role, permission) values ('PERF_DIRETTORE', 'guPerforming');

INSERT INTO role_permission(role, permission) values ('PERF_DIRETTORE', 'prGestioneMegaconcerti');

INSERT INTO role_permission(role, permission) values ('PERF_DIRETTORE', 'prScodifica');

INSERT INTO role_permission(role, permission) values ('DEVELOPMENT', 'prScodifica');

INSERT INTO role_permission(role, permission) values ('IT', 'prAggiornamentoSun');

INSERT INTO role_permission(role, permission) values ('IT', 'prCodificaManualeEsperto');

INSERT INTO role_permission(role, permission) values ('IT', 'prCodificaManualeBase');

INSERT INTO role_permission(role, permission) values ('IT', 'prMonitoraggioCodificato');

INSERT INTO role_permission(role, permission) values ('IT', 'prMonitoraggioMp');

INSERT INTO role_permission(role, permission) values ('IT', 'prRegoleRipartizione');

INSERT INTO role_permission(role, permission) values ('IT', 'prGestioneMp');

INSERT INTO role_permission(role, permission) values ('IT', 'prConfigurazioniSoglie');

INSERT INTO role_permission(role, permission) values ('IT', 'prCarichiRipartizione');

INSERT INTO role_permission(role, permission) values ('IT', 'prKPICodifica');

INSERT INTO role_permission(role, permission) values ('IT', 'prMonitoringOperatoriCodifica');

INSERT INTO role_permission(role, permission) values ('IT', 'prRadioInStore');

INSERT INTO role_permission(role, permission) values ('IT', 'prRadioInStore');

INSERT INTO role_permission(role, permission) values ('IT', 'prRadioInStore');

INSERT INTO role_permission(role, permission) values ('GA', 'prAggiornamentoSun');

INSERT INTO role_permission(role, permission) values ('GA', 'prCodificaManualeBase');

INSERT INTO role_permission(role, permission) values ('GA', 'prCodificaManualeEsperto');

INSERT INTO role_permission(role, permission) values ('GA', 'prMonitoraggioCodificato');

INSERT INTO role_permission(role, permission) values ('GA', 'prRegoleRipartizione');

INSERT INTO role_permission(role, permission) values ('GA', 'prCarichiRipartizione');

INSERT INTO role_permission(role, permission) values ('GA', 'prKPICodifica');

INSERT INTO role_permission(role, permission) values ('GA', 'prMonitoringOperatoriCodifica');

INSERT INTO role_permission(role, permission) values ('GA', 'guPerforming');

INSERT INTO role_permission(role, permission) values ('GA', 'prScodifica');

INSERT INTO role_permission(role, permission) values ('BU-MUSICA', 'prCodificaManualeEsperto');
