


alter table IMPORTI_ANTICIPO drop foreign key IMPORTI_ANTICIPO_ibfk_1;

alter table IMPORTI_ANTICIPO drop column IDDSP;


alter table INVOICE drop foreign key FK_IDDSP_INVOICE;

alter table INVOICE drop column IDDSP;


alter table INVOICE_ITEM_TO_CCID
	add PERIODO_RIPARTIZIONE_ID bigint(22) null;

alter table INVOICE_ITEM_TO_CCID
	add constraint INVOICE_ITEM_TO_CCID_PERIODO_RIPARTIZIONE_fk
		foreign key (PERIODO_RIPARTIZIONE_ID) references PERIODO_RIPARTIZIONE (ID_PERIODO_RIPARTIZIONE);




create table CLIENT_TO_DSP
(
  ID int auto_increment primary key,
  ANAG_CLIENT_ID int,
	IDDSP varchar(100),
	LICENCE TEXT null,
	START_DATE DATETIME null,
	END_DATE DATETIME null,
		constraint FK_CLIENT_TO_DSP_ANAG_CLIENT
		  foreign key (ANAG_CLIENT_ID) references ANAG_CLIENT (ID_ANAG_CLIENT),
		constraint FK_CLIENT_TO_DSP_IDDSP
		  foreign key (IDDSP) references ANAG_DSP(IDDSP)
);

create index FK_CLIENT_TO_DSP_IDDSP_idx
	on CLIENT_TO_DSP (IDDSP);

create index FK_CLIENT_TO_DSP_ANAG_CLIENT_ID_idx
	on CLIENT_TO_DSP (ANAG_CLIENT_ID);

alter table CLIENT_TO_DSP drop foreign key FK_CLIENT_TO_DSP_IDDSP;
#### riporto le informazioni esistenti sull associativa

insert into CLIENT_TO_DSP (ANAG_CLIENT_ID,IDDSP,LICENCE,START_DATE,END_DATE) (
select ID_ANAG_CLIENT,IDDSP,LICENCE, START_DATE,END_DATE from ANAG_CLIENT);

## spacco tutto

alter table ANAG_CLIENT drop foreign key FK_IDDSP_ANAG_CLIENT;
drop index FK_IDDSP_ANAG_CLIENT_idx on ANAG_CLIENT;


alter table ANAG_CLIENT drop column LICENCE;

alter table ANAG_CLIENT drop column START_DATE;

alter table ANAG_CLIENT drop column END_DATE;

alter table ANAG_CLIENT drop column IDDSP;

drop index FK_CLIENT_TO_DSP_IDDSP_idx on CLIENT_TO_DSP;

create unique index FK_CLIENT_TO_DSP_IDDSP_idx
	on CLIENT_TO_DSP (IDDSP);

alter table INVOICE_ITEM_TO_CCID
	add VALORE_APPLICATO_IN_CHIUSURA decimal(34,20) default 0.00000000000000000000 null;

alter table INVOICE_ITEM_TO_CCID
	add OPERAZIONE varchar(32) null;





