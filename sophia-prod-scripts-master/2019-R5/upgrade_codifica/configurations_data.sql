INSERT INTO PERF_CONFIGURATION (ACTIVE, CREATED, CREATED_BY, DOMAIN , EDITED_BY, CONF_KEY, CONF_LABEL, MODIFIED, TOP_LEVEL, VALID_FROM, VALID_TO)
VALUES
(1,now(), 'Esercizio', 'codifica', null, 'view-info','Informazioni Visualizzate',null, true,now(),null),
(1,now(), 'Esercizio', 'codifica', null, 'maturato','Mostra Maturato',null, false,now(),null),
(1,now(), 'Esercizio', 'codifica', null, 'rischio','Mostra Rischiosità',null, false,now(),null),
(1,now(), 'Esercizio', 'codifica', null, 'stato-opera','Stato Opera',null, true,now(),null),
(1,now(), 'Esercizio', 'codifica', null, 'ranking','Parametri di ranking',null, true,now(),null),
(1,now(), 'Esercizio', 'codifica', null, 'thresholds','Soglie di codifica',null, true,now(),null)
;

INSERT INTO PERF_SETTING ( SETTING_TYPE, SETTING_KEY, ORDINAL, SETTING_LABEL,VALUE_TYPE, VALUE , CAPTION, RELATION  )
   VALUES
   ('key_value','maturato.show',1,'Mostra Maturato' ,'Boolean','1',null,null ),
   ('threshold_set','maturato.thresholdset',2,'Mostra maturato in simboli' ,'Boolean','0',null,null ),
   ('threshold','maturato.threshold.1',1,'' ,'Integer','1000','€','LT' ),
   ('threshold','maturato.threshold.2',2,'' ,'Integer','10000','€€','LT' ),
   ('threshold','maturato.threshold.3',3,'' ,'Integer','100000','€€€','LT' ),
   ('threshold','maturato.threshold.4',4,'' ,'Integer','100000','€€€€','GT' )
;

INSERT INTO PERF_SETTING ( SETTING_TYPE, SETTING_KEY,ORDINAL, SETTING_LABEL,VALUE_TYPE, VALUE , CAPTION, RELATION  )
   VALUES
   ('key_value','rischio.show',1,'Mostra Rischiosità' ,'Boolean','1',null,null ),
   ('threshold_set','rischio.thresholdset',2,'Mostra rischiosità in simboli' ,'Boolean','0',null,null ),
   ('threshold','rischio.threshold.1',1,'' ,'Decimal','0.25','!','LT' ),
   ('threshold','rischio.threshold.2',2,'' ,'Decimal','0.50','!!','LT' ),
   ('threshold','rischio.threshold.3',3,'' ,'Decimal','0.50','!!!','GT' )
;

INSERT INTO PERF_SETTING ( SETTING_TYPE, SETTING_KEY, ORDINAL, SETTING_LABEL,VALUE_TYPE, VALUE , CAPTION, RELATION  )
   VALUES
   ('key_value','stato-opera.show',1,'Mostra Stato Opera' ,'Boolean','1',null,null )
;

INSERT INTO PERF_SETTING ( SETTING_TYPE, SETTING_KEY,ORDINAL, SETTING_LABEL,VALUE_TYPE, VALUE , CAPTION, RELATION  )
   VALUES
   ('key_value','title_weight',1,'Peso Titolo' ,'Decimal','0.667',null,null),
   ('key_value','title_exponent',2,'Esponente Titolo' ,'Decimal','1.0',null,null),
   ('key_value','artist_weight',3,'Peso Artista' ,'Decimal','0.333',null,null),
   ('key_value','artist_exponent',4,'Esponente Artista' ,'Decimal','1.0',null,null),
   ('threshold_set','relevance_flag',5,'Flag relevance','Boolean','0',null,null),
   ('key_value','relevance_weight',1,'Peso Maturato','Decimal','0.333',null,null),
   ('key_value','relevance_exponent',2,'Esponente Maturato' ,'Decimal','1.0',null,null),
   ('key_value','relevance_period',3,'Numero periodi maturato' ,'Integer','4',null,null ),
   ('threshold_set','risk_flag',6,'Flag risk','Boolean','0',null,null),
   ('key_value','risk_weight',1,'Peso Rischio','Decimal','0.333',null,null),
   ('key_value','risk_exponent',2,'Esponente Rischio' ,'Decimal','1.0',null,null),
   ('key_value','risk_period',3,'Numero periodi rischiosità' ,'Integer','4',null,null )
;

INSERT INTO PERF_SETTING ( SETTING_TYPE, SETTING_KEY,ORDINAL, SETTING_LABEL,VALUE_TYPE, VALUE , CAPTION, RELATION  )
   VALUES
   ('threshold','auto.single-result.sogliaConf',1,'Risultato Singolo Automatico: Soglia di Confidenza','Decimal','95.5',null,'GT'),
   ('threshold','auto.single-result.distSecond',2,'Risultato Singolo Automatico: Distanza dalla Seconda','Decimal','100.00',null,'GTE'),
   ('threshold','auto.single-result.ecoValue',3,'Risultato Singolo Automatico: Valore Economico','Decimal','0.00',null,'LTE'),
   ('threshold','auto.multiple-result.sogliaConf',4,'Risultato Multiplo  Automatico: Soglia di Confidenza','Decimal','85.5',null,'GT'),
   ('threshold','auto.multiple-result.distSecond',5,'Risultato Multiplo Automatico: Distanza dalla Seconda (inf)','Decimal','30.00',null,'GT'),
   ('threshold','auto.multiple-result.distSecond',6,'Risultato Multiplo Automatico: Distanza dalla Seconda (sup)','Decimal','100.00',null,'LT'),
   ('threshold','auto.multiple-result.ecoValue',7,'Risultato Multiplo Automatico: Valore Economico','Decimal','0.00',null,'LTE'),
   ('key_value','manual.risk-rule.rischio',8,'Soglia massima del Rischio per codifica automatica' ,'Decimal','1000.00',null,null),
   ('key_value','manual.publico-dominio.flagPD',9,'Codificare manualmente Opere Pubblico Dominio' ,'Boolean','1',null,null),
   ('key_value','manual.elaborata.flagEL',10,'Codificare manualmente Opere Elaborate' ,'Boolean','1',null,null),
   ('key_value','manual.irregolare.statoOpera',11,'Codificare manualmente Opere Irregolare' ,'Boolean','1',null,null)
 ;

INSERT INTO PERF_SETTING_THRESHOLDS (THRESHOLD_SET_ID, THRESHOLDS_ID)
select parent,child from (
SELECT SETTING_ID  parent FROM PERF_SETTING WHERE setting_key ='rischio.thresholdset') p,
(SELECT SETTING_ID child FROM PERF_SETTING WHERE setting_key  like 'rischio.threshold.%') c;


INSERT INTO PERF_SETTING_THRESHOLDS (THRESHOLD_SET_ID, THRESHOLDS_ID)
select parent,child from (
SELECT SETTING_ID  parent FROM PERF_SETTING WHERE setting_key ='maturato.thresholdset') p,
(SELECT SETTING_ID child FROM PERF_SETTING WHERE setting_key  like 'maturato.threshold.%') c;

INSERT INTO PERF_CONFIGURATION_SETTINGS (CONFIGURATION_ID,SETTINGS_ID)
select parent, child from (
SELECT CONFIGURATION_ID  parent FROM PERF_CONFIGURATION WHERE CONF_KEY='maturato') p ,
(SELECT SETTING_ID child FROM PERF_SETTING WHERE setting_key like 'maturato.%' and setting_key not like 'maturato.threshold.%') c;

INSERT INTO PERF_CONFIGURATION_SETTINGS (CONFIGURATION_ID,SETTINGS_ID)
select parent, child from (
SELECT CONFIGURATION_ID  parent FROM PERF_CONFIGURATION WHERE CONF_KEY='rischio') p ,
(SELECT SETTING_ID child FROM PERF_SETTING WHERE setting_key like '%rischio.%' and setting_key  not like 'rischio.threshold.%') c;

INSERT INTO PERF_CONFIGURATION_CONFIGURATIONS (CONFIGURATION_ID,CONFIGURATIONS_ID)
select parent, child from (
SELECT CONFIGURATION_ID  parent FROM PERF_CONFIGURATION WHERE CONF_KEY='view-info') p ,
(SELECT CONFIGURATION_ID child FROM PERF_CONFIGURATION WHERE CONF_KEY in ('maturato','rischio','stato-opera')) c;

INSERT INTO PERF_CONFIGURATION_SETTINGS (CONFIGURATION_ID,SETTINGS_ID)
select parent, child from (
SELECT CONFIGURATION_ID  parent FROM PERF_CONFIGURATION WHERE CONF_KEY='ranking') p ,
(SELECT SETTING_ID child FROM PERF_SETTING WHERE setting_key in(
'title_weight',
'title_exponent',
'artist_weight',
'artist_exponent',
'relevance_flag',
'risk_flag'

)
) c;

INSERT INTO PERF_CONFIGURATION_SETTINGS (CONFIGURATION_ID,SETTINGS_ID)
select parent, child from (
SELECT CONFIGURATION_ID  parent FROM PERF_CONFIGURATION WHERE CONF_KEY='thresholds') p ,
(SELECT SETTING_ID child FROM PERF_SETTING WHERE (setting_key like 'auto.%' or setting_key like 'manual.%')) c;

INSERT INTO PERF_SETTING_THRESHOLDS (THRESHOLD_SET_ID, THRESHOLDS_ID)
select parent,child from (
SELECT SETTING_ID  parent FROM PERF_SETTING WHERE setting_key ='relevance_flag') p,
(SELECT SETTING_ID child FROM PERF_SETTING WHERE setting_key  like 'relevance_%' AND setting_key !='relevance_flag') c;


INSERT INTO PERF_SETTING_THRESHOLDS (THRESHOLD_SET_ID, THRESHOLDS_ID)
select parent,child from (
SELECT SETTING_ID  parent FROM PERF_SETTING WHERE setting_key ='risk_flag') p,
(SELECT SETTING_ID child FROM PERF_SETTING WHERE setting_key  like 'risk_%' AND setting_key !='risk_flag') c;

INSERT INTO PERF_CONFIGURATION_SETTINGS (CONFIGURATION_ID,SETTINGS_ID)
select parent, child from (
SELECT CONFIGURATION_ID  parent FROM PERF_CONFIGURATION WHERE CONF_KEY='stato-opera') p ,
(SELECT SETTING_ID child FROM PERF_SETTING WHERE setting_key like 'stato-opera.%') c;

commit;
