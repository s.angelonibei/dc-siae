INSERT INTO PERF_SETTING_PROCESSOR (PROCESSOR_TYPE,PROCESSOR,PROCESSOR_CONFIG,APPLICATION_PATTERN, PROCESSING_ORDER,ACTIVE)
VALUES
    ('drools_rule','ProposalFlagRule',JSON_OBJECT('action','manual'),'manual.publico-dominio.',1,1),
    ('drools_rule','ProposalFlagRule',JSON_OBJECT('action','manual'),'manual.elaborata.',2,1),
    ('drools_rule','ProposalFieldRule',JSON_OBJECT('action','manual','value','"Irregolare"','relation','EQ'),'manual.irregolare.',3,1),
    ('drools_rule','ProposalFieldRule',JSON_OBJECT('action','manual', 'relation','GT'),'manual.risk-rule.',4,1),
    ('drools_rule','MetadatiRisultatiCodificaRule',JSON_OBJECT('action','auto'),'auto.single-result.',5,1),
    ('drools_rule','MetadatiRisultatiCodificaRule',JSON_OBJECT('action','auto'),'auto.multiple-result.',6,1)
;
commit;
