DROP TABLE IF EXISTS PERF_CODIFICA_MASSIVA;

create table PERF_CODIFICA_MASSIVA
(
 ID bigint auto_increment primary key,
 RECEIVED_MESSAGE json not null,
 STATE varchar(256) not null,
 INSERT_TIME timestamp default CURRENT_TIMESTAMP not null,
 END_WORK_TIME timestamp null,
 UTENTE varchar(256) not null,
 FILE_NAME varchar(512) null,
 FILE_INPUT varchar(512) null,
 FILE_OUTPUT varchar(512) null,
ERROR_MESSAGE varchar(512) null,
 TOKEN varchar(512) null
);

create index PERF_CODIFICA_MASSIVA_STATE_IDX1
 on PERF_CODIFICA_MASSIVA (STATE)
;
