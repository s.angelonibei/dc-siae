# Ingestion Broadcasting

Censimento property: microservices.api.url

## Steps

1. verificare di essere loggati come root
2. effettare un backup del file /home/orchestrator/sophia/webapp/configuration.properties
3. aprire il file /home/orchestrator/sophia/webapp/configuration.properties
4. rimuove la property con chiave 'microservices.api.url'
5. aggiungere la seguente property: ms.ingestion.api.path=api/ms-ingestion-sap/
6. salvare il file
7. riavviare l'app cruscottoUtilizzazioni

## Script automatico

In alternativa, usare lanciare lo script ingestion-gestioneutilizzazioni-properties-patch.sh

```
sh ingestion-gestioneutilizzazioni-properties-patch.sh
```
