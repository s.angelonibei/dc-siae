#!/bin/sh
#
# Add property to configuration
# 
#

#
# Changelog
# v1.0  f.ranieri 27/02/2019 - Original version
# v1.1  f.ranieri 12/03/2019 - updated api path
#

#
# Versioning Info called by -v flag
#
VERSION="1.1"
LASTUPDATE="12 Marzo 2019"

#
# Exit Codes
#
# 1 = Not root
# 5 = -v Version Number

#
# Set PATH - Edit as required
# 
#PATH=/usr/bin:/usr/sbin:/bin:/sbin:/etc:/usr/local/bin
#export PATH

#
#Optional Debug - uncomment to enable debugging
#
#set -x

#
# Verify running as ROOT
#
if [ `whoami` != root ] ; then
    echo "--- ERROR:  You must be root to run $basename $0"
    exit 1
fi

#
# VERSION Function w/exit code
#
version () {
  /usr/bin/echo "--- Running Script = `basename $0` ---"
  /usr/bin/echo "--- Version = $VERSION ---"
  /usr/bin/echo "--- Last Updated on $LASTUPDATE ---"
  exit 5
}

# 
# Usage Statement Example - Function
# 
# 
usage () {
  echo "usage:  $(basename $0)"
  echo "        Example"
  echo "	`basename $0`"
  exit 3
}

#
# GETOPTS - Adjust based on required flags for script
#
#
while getopts hv OPTIONS
do
    case "$OPTIONS"
    in
        v)  version ;;
		h)  usage ;;
        *)  usage ;;
    esac
done

echo -n "configuration.properties's location (default is '/home/orchestrator/sophia/webapp/configuration.properties'): "
read properties
properties=${properties:-/home/orchestrator/sophia/webapp/configuration.properties}
echo "$properties"

cp "$properties" "$properties.backup.$(date +%Y%m%d_%H%M%S)"

if grep -q 'microservices.api.url=' "$properties"; then
  sed -i 's#microservices.api.url=.*$#ms.ingestion.api.path=api/ms-ingestion-sap/#g' "$properties"
else
   echo -e 'ms.ingestion.api.path=api/ms-ingestion-sap/\n' >> "$properties"
fi

#EOF
