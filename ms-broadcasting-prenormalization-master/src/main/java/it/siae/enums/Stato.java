package it.siae.enums;

public enum Stato {

    DA_ELABORARE("DA_ELABORARE"),
    DA_CONVERTIRE("DA_CONVERTIRE"),
    IN_CONVERSIONE("IN_CONVERSIONE"),
    ERRORE("ERRORE");


    String text;

    Stato(String text) {
        this.text = text;
    }


    @Override
    public String toString() {
        return text;
    }

    public static Stato valueOfDescription(String description) {

        Stato stato=null;

        for (Stato v : values()) {

            if (v.toString().equals(description)) {
                stato = v;
            }
        }

        return stato;
    }
}
