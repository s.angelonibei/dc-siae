package it.siae.utils;

public class Constants {

    public static final String EXCEL_EXT_XLSX = "xlsx";
    public static final String EXCEL_EXT_XLS = "xls";
    public static final String EXCEL_DATE_FORMAT = "dd/MM/yyyy";
    public static final String EXCEL_TIME_FORMAT = "HH:mm:ss";
    public static final String RULE_DURATA_TRASMISSIONE = "00:00:00";
    public static final String RULE_CATEGORIAUSO_1 = "1";
    public static final String RULE_CATEGORIAUSO_2 = "2";
    public static final String RULE_CATEGORIAUSO_3 = "3";
    public static final String RULE_CATEGORIAUSO_4 = "4";
    public static final String RULE_CATEGORIAUSO_5 = "5";
    public static final String RULE_TITOLO_OPERA_MUSICALE = "Chiusura Trasmissione";
    public static final String RULE_GENERE_TRASMISSIONE_3702 = "3702";
    public static final String RULE_GENERE_TRASMISSIONE_10 = "10";
    public static final String RULE_GENERE_TRASMISSIONE_9 = "9";
}
