package it.siae.entity;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.google.gson.annotations.Expose;
import it.siae.enums.Stato;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@XmlRootElement
@Entity
@Table(name = "bdc_utilization_file")
@NamedQuery(name = "BdcInformationFileEntity.findAll", query = "SELECT b FROM BdcInformationFileEntity b")
@NamedNativeQuery(name = "BdcInformationFileEntity.getListaFilesBroadcaster", query = "SELECT id, nome_file, data_Upload, percorso, tipo_Upload, CASE stato WHEN ?1 THEN ?3 WHEN ?2 THEN ?3 ELSE stato END as stato, data_Processamento FROM bdc_utilization_file WHERE emittente=?4 AND NOME_REPERTORIO=?5 ORDER BY data_Upload DESC",resultClass = BdcInformationFileEntity.class)
@JsonAutoDetect
public class BdcInformationFileEntity implements Serializable{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne
    @JoinColumn(name = "emittente")
    @Expose
    private BdcBroadcasters emittente;

    @Expose
    @OneToOne
    @JoinColumn(name = "canale")
    private BdcCanali canale;

    @Expose
    @Lob
    @Column(name = "nome_file", nullable = false)
    private String nomeFile;

    @Expose
    @Column(name = "anno")
    private Integer anno;

    @Expose
    @Column(name = "mese")
    private Integer mese;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_upload", nullable = false)
    @Expose
    private Date dataUpload;

    @Expose
    @Column(name = "NOME_REPERTORIO")
    private String repertorio;

    @Transient
    private String sDataUpload;

    public String getsDataUpload() {
        return sDataUpload;
    }

    public void setsDataUpload(String sDataUpload) {
        this.sDataUpload = sDataUpload;
    }

    @Column(name = "percorso", nullable = false)
    @Expose
    private String percorso;

    @Column(name = "tipo_upload", nullable = false)
    @Expose
    private String tipoUpload;

    //    @Convert(converter = StatoTypeConverter.class)
    @Enumerated(EnumType.STRING)
    @Column(name = "stato", nullable = false)
    private Stato stato;

    @Column(name = "data_processamento", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataProcessamento;


    @OneToOne
    @JoinColumn(name = "id_utente")
    @Expose
    private BdcUtenti idUtente;


    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_INVIO_REM", nullable = true)
    @Expose
    private Date dataInvioRem;

    @Column(name = "STACK_TRACE", nullable = true)
    @Expose
    private String stackTrace;


    @Column(name = "DECODIFICA_ERRORE", nullable = true)
    @Expose
    private String decodificaErrore;

    @Column(name = "KPI_OK", nullable = true)
    @Expose
    private Integer kpiOk;

    @Column(name = "AMBITO_KPI", nullable = true)
    @Expose
    private String ambitoKpi;




    public BdcInformationFileEntity() {
    }

/*
    public BdcInformationFileEntity(BdcBroadcasters emittente, BdcCanali canale, String nomeFile, Integer anno, Integer mese, Date dataUpload, String percorso, String tipoUpload, Stato stato, Date dataProcessamento, BdcUtenti idUtente) {
        this.emittente = emittente;
        this.canale = canale;
        this.nomeFile = nomeFile;
        this.anno = anno;
        this.mese = mese;
        this.dataUpload = dataUpload;
        this.percorso = percorso;
        this.tipoUpload = tipoUpload;
        this.stato = stato;
        this.dataProcessamento = dataProcessamento;
        this.idUtente = idUtente;
    }
*/

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BdcBroadcasters getEmittente() {
        return emittente;
    }

    public void setEmittente(BdcBroadcasters emittente) {
        this.emittente = emittente;
    }

    public BdcCanali getCanale() {
        return canale;
    }

    public void setCanale(BdcCanali canale) {
        this.canale = canale;
    }

    public String getNomeFile() {
        return nomeFile;
    }

    public void setNomeFile(String nomeFile) {
        this.nomeFile = nomeFile;
    }

    public Integer getAnno() {
        return anno;
    }

    public void setAnno(Integer anno) {
        this.anno = anno;
    }

    public Integer getMese() {
        return mese;
    }

    public void setMese(Integer mese) {
        this.mese = mese;
    }

    public Date getDataUpload() {
        return dataUpload;
    }

    public void setDataUpload(Date dataUpload) {
        this.dataUpload = dataUpload;
    }

    public String getPercorso() {
        return percorso;
    }

    public void setPercorso(String percorso) {
        this.percorso = percorso;
    }

    public String getTipoUpload() {
        return tipoUpload;
    }

    public void setTipoUpload(String tipoUpload) {
        this.tipoUpload = tipoUpload;
    }

    public Date getDataProcessamento() {
        return dataProcessamento;
    }

    public void setDataProcessamento(Date dataProcessamento) {
        this.dataProcessamento = dataProcessamento;
    }

    public BdcUtenti getIdUtente() {
        return idUtente;
    }

    public void setIdUtente(BdcUtenti idUtente) {
        this.idUtente = idUtente;
    }

    public Stato getStato() {
        return stato;
    }

    public void setStato(Stato stato) {
        this.stato = stato;
    }

    public String getsStato() {
        return stato.toString();
    }

    public String getRepertorio() {
        return repertorio;
    }

    public void setRepertorio(String repertorio) {
        this.repertorio = repertorio;
    }

    public Date getDataInvioRem() {
        return dataInvioRem;
    }

    public void setDataInvioRem(Date dataInvioRem) {
        this.dataInvioRem = dataInvioRem;
    }

    public String getStackTrace() {
        return stackTrace;
    }

    public void setStackTrace(String stackTrace) {
        this.stackTrace = stackTrace;
    }

    public String getDecodificaErrore() {
        return decodificaErrore;
    }

    public void setDecodificaErrore(String decodificaErrore) {
        this.decodificaErrore = decodificaErrore;
    }

    public Integer getKpiOk() {
        return kpiOk;
    }

    public void setKpiOk(Integer kpiOk) {
        this.kpiOk = kpiOk;
    }

    public String getAmbitoKpi() {
        return ambitoKpi;
    }

    public void setAmbitoKpi(String ambitoKpi) {
        this.ambitoKpi = ambitoKpi;
    }

/*
    public BdcInformationFileEntity(Integer id, String nomeFile, Date dataUpload, String percorso, String tipoUpload, Stato stato, Date dataProcessamento) {
        this.id = id;
        this.nomeFile = nomeFile;
        this.dataUpload = dataUpload;
        this.percorso = percorso;
        this.tipoUpload = tipoUpload;
        this.dataProcessamento = dataProcessamento;
        this.stato = stato;
    }
*/


    public BdcInformationFileEntity(BdcBroadcasters emittente, BdcCanali canale, String nomeFile, Integer anno, Integer mese, Date dataUpload, String percorso, String tipoUpload, Stato stato, Date dataProcessamento, BdcUtenti idUtente,Date dataInvioRem, String StackTrace ,String decodificaErrore, Integer kpiOk, String ambitoKpi) {
        this.emittente = emittente;
        this.canale = canale;
        this.nomeFile = nomeFile;
        this.anno = anno;
        this.mese = mese;
        this.dataUpload = dataUpload;
        this.percorso = percorso;
        this.tipoUpload = tipoUpload;
        this.stato = stato;
        this.dataProcessamento = dataProcessamento;
        this.idUtente = idUtente;
        this.dataInvioRem = dataInvioRem;
        this.stackTrace = StackTrace;
        this.decodificaErrore = decodificaErrore;
        this.kpiOk = kpiOk;
        this.ambitoKpi = ambitoKpi;
    }

    public BdcInformationFileEntity(Object obj[]) {
        try {
            id = (Integer) obj[0];
            nomeFile = (String) obj[1];
            dataUpload = (Date) obj[2];
            percorso = (String) obj[3];
            tipoUpload = (String) obj[4];
            stato = (Stato) obj[5];
            dataProcessamento = (Date) obj[6];

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "BdcInformationFileEntity{" +
                "id=" + id +
                ", emittente=" + emittente +
                ", canale=" + canale +
                ", nomeFile='" + nomeFile + '\'' +
                ", anno=" + anno +
                ", mese=" + mese +
                ", dataUpload=" + dataUpload +
                ", repertorio='" + repertorio + '\'' +
                ", sDataUpload='" + sDataUpload + '\'' +
                ", percorso='" + percorso + '\'' +
                ", tipoUpload='" + tipoUpload + '\'' +
                ", stato=" + stato +
                ", dataProcessamento=" + dataProcessamento +
                ", idUtente=" + idUtente +
                ", dataInvioRem=" + dataInvioRem +
                ", stackTrace='" + stackTrace + '\'' +
                ", decodificaErrore='" + decodificaErrore + '\'' +
                ", kpiOk=" + kpiOk +
                ", ambitoKpi='" + ambitoKpi + '\'' +
                '}';
    }
}
