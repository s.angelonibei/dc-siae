package it.siae.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Table(name ="BDC_PRENORMALIZATION_CATEGORIA_USO",schema = "MCMDB_debug")
@Entity
public class BdcPrenormalizationCategoriaUso {

    @Id
    @Column(name = "genere_trasmissione")
    private String genereTrasmissione;

    @Column(name = "categoria_uso")
    private String categoriaUso;
}
