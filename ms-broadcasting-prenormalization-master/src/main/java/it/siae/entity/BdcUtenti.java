package it.siae.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@XmlRootElement
@Entity
@Table(name = "bdc_utenti")
public class BdcUtenti {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(nullable = false, unique = true)
    private String username;

    @Column(nullable = false, unique = true)
    private String email;

    private String password;

    @OneToOne
    @JoinColumn(name = "id_broadcaster")
    private BdcBroadcasters bdcBroadcasters;

    @OneToOne
    @JoinColumn(name = "id_ruolo")
    private BdcRuoli bdcRuoli;

    @Column(name = "data_creazione", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date dataCreazione;

    @Column(name = "data_ultima_modifica", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date dataUltimaModifica;

    @Column(name = "utente_ultima_modifica", nullable = false, columnDefinition = "STRING DEFAULT 'esercizio'")
    private String utenteUltimaModifica;

    @OneToMany
    @JoinColumn(name = "id_utente")
    private List<BdcUtentiRepertorio> repertori;

    public BdcUtenti(Integer id, String username, String email, String password, BdcBroadcasters bdcBroadcasters,
                     BdcRuoli bdcRuoli, Date dataCreazione, Date dataUltimaModifica, String utenteUltimaModifica) {
        super();
        this.id = id;
        this.username = username;
        this.email = email;
        this.password = password;
        this.bdcBroadcasters = bdcBroadcasters;
        this.bdcRuoli = bdcRuoli;
        this.dataCreazione = dataCreazione;
        this.dataUltimaModifica = dataUltimaModifica;
        this.utenteUltimaModifica = utenteUltimaModifica;
    }

    public BdcUtenti() {
    }

    public BdcUtenti(Object[] row) {
        this.id = (Integer) row[0];
        this.username = (String) row[1];
        this.email = (String) row[2];
        this.password = (String) row[3];
        BdcBroadcasters bdcBroadcasters = new BdcBroadcasters();
        bdcBroadcasters.setId((Integer) row[4]);
        this.bdcBroadcasters = bdcBroadcasters;
        BdcRuoli bdcRuoli = new BdcRuoli();
        bdcRuoli.setId((Integer) row[5]);
        this.bdcRuoli = bdcRuoli;
        this.dataCreazione = (Date) row[6];
        this.dataUltimaModifica = (Date) row[7];
        this.utenteUltimaModifica = (String) row[8];
        String repertori=(String) row[9];
        String repertorioDefault=(String) row[10];
        ArrayList<BdcUtentiRepertorio> repertorioList= new ArrayList<>();
        if (repertori!=null) {
            for (String repertorio : repertori.split(",")) {
                BdcRepertorio repertorio2= new BdcRepertorio(repertorio, "", "");
                repertorioList.add(new BdcUtentiRepertorio(null, id, repertorio2,  repertorio2.getNome().equalsIgnoreCase(repertorioDefault)));
            }
            this.repertori = repertorioList;
        }

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public BdcBroadcasters getBdcBroadcasters() {
        return bdcBroadcasters;
    }

    public void setBdcBroadcasters(BdcBroadcasters bdcBroadcasters) {
        this.bdcBroadcasters = bdcBroadcasters;
    }

    @JsonIgnore
    public String getPassword() {
        return this.password;
    }

    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }

    public BdcRuoli getBdcRuoli() {
        return bdcRuoli;
    }

    public void setBdcRuoli(BdcRuoli bdcRuoli) {
        this.bdcRuoli = bdcRuoli;
    }

    public Date getDataCreazione() {
        return dataCreazione;
    }

    public void setDataCreazione(Date dataCreazione) {
        this.dataCreazione = dataCreazione;
    }

    public Date getDataUltimaModifica() {
        return dataUltimaModifica;
    }

    public void setDataUltimaModifica(Date dataUltimaModifica) {
        this.dataUltimaModifica = dataUltimaModifica;
    }

    public String getUtenteUltimaModifica() {
        return utenteUltimaModifica;
    }

    public void setUtenteUltimaModifica(String utenteUltimaModifica) {
        this.utenteUltimaModifica = utenteUltimaModifica;
    }

    public List<BdcUtentiRepertorio> getRepertori() {
        return repertori;
    }

    public void setRepertori(List<BdcUtentiRepertorio> repertori) {
        this.repertori = repertori;
    }

}
