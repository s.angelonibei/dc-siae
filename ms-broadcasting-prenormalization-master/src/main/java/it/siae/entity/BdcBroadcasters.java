package it.siae.entity;

import it.siae.enums.TipoBroadcaster;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;


@Entity
@Table(name = "bdc_broadcasters")
@NamedQuery(name = "BdcBroadcasters.findAll", query = "SELECT b FROM BdcBroadcasters b")
@XmlRootElement
public class BdcBroadcasters implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "nome", nullable = false)
    private String nome;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_broadcaster", nullable = false)
    private TipoBroadcaster tipo_broadcaster;

    @Lob
    @Column(name = "image", nullable = true, columnDefinition = "BLOB")
    private byte[] logo;

    @Column(name = "data_creazione", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date data_creazione;

    @Transient
    private Date data_disattivazione;

    public BdcBroadcasters() {
    }

    public BdcBroadcasters(TipoBroadcaster tipo_broadcaster) {
        this.tipo_broadcaster = tipo_broadcaster;
    }

    public BdcBroadcasters(String nome) {
        this.nome = nome;
    }

    public BdcBroadcasters(String nome, TipoBroadcaster tipo_broadcaster) {
        this.nome = nome;
        this.tipo_broadcaster = tipo_broadcaster;
    }

    public BdcBroadcasters(String nome, TipoBroadcaster tipo_broadcaster, byte[] logo) {
        super();
        this.nome = nome;
        this.tipo_broadcaster = tipo_broadcaster;
        this.logo = logo;
    }

    public BdcBroadcasters(Integer id, String nome, TipoBroadcaster tipo_broadcaster, byte[] logo,
                           Date data_creazione) {
        super();
        this.id = id;
        this.nome = nome;
        this.tipo_broadcaster = tipo_broadcaster;
        this.logo = logo;
        this.data_creazione = data_creazione;
    }

    public BdcBroadcasters(Object[] row) {
        SimpleDateFormat sdf= new SimpleDateFormat("dd/MM/yyyy");
        this.id = (Integer) row[0];
        this.nome = (String) row[1];
        this.tipo_broadcaster =TipoBroadcaster.valueOf((String) row[2]);
        this.data_creazione = (Timestamp) row[4];
        this.data_disattivazione=(Timestamp) row[5];
    }

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public TipoBroadcaster getTipo_broadcaster() {
        return tipo_broadcaster;
    }

    public void setTipo_broadcaster(TipoBroadcaster tipo_broadcaster) {
        this.tipo_broadcaster = tipo_broadcaster;
    }

    public byte[] getLogo() {
        return logo;
    }

    public void setLogo(byte[] logo) {
        this.logo = logo;
    }

    public Date getData_creazione() {
        return data_creazione;
    }

    public void setData_creazione(Date data_creazione) {
        this.data_creazione = data_creazione;
    }

    public Date getData_disattivazione() {
        return data_disattivazione;
    }

    public void setData_disattivazione(Date data_disattivazione) {
        this.data_disattivazione = data_disattivazione;
    }

    @Override
    public String toString() {
        return nome;
    }
}
