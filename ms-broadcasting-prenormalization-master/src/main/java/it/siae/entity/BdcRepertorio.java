package it.siae.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(name = "BDC_REPERTORIO")
public class BdcRepertorio {

    @Id
    @Column(nullable = false, unique = true)
    private String nome;

    @Column(nullable = false)
    private String descrizione;

    @Column(name = "url_path", nullable = false)
    private String urlPath;

    public BdcRepertorio() {
        super();
    }

    public BdcRepertorio(String nome, String descrizione, String urlPath) {
        super();
        this.nome = nome;
        this.descrizione = descrizione;
        this.urlPath = urlPath;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public String getUrlPath() {
        return urlPath;
    }

    public void setUrlPath(String urlPath) {
        this.urlPath = urlPath;
    }


}

