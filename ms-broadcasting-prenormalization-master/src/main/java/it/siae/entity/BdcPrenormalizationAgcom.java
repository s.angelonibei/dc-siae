package it.siae.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Table(name ="BDC_PRENORMALIZATION_AGCOM",schema = "MCMDB_debug")
@Entity
public class BdcPrenormalizationAgcom {

    @Id
    @Column(name = "genere_trasmissione")
    private String genereTrasmissione;

    @Column(name = "genere_agcom")
    private String genereAgcom;
}
