package it.siae.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.annotations.Expose;
import it.siae.enums.TipoBroadcaster;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@XmlRootElement
@Entity
@Table(name="bdc_canali")
@NamedQueries({
        @NamedQuery(name="BdcCanali.findAll", query="SELECT b FROM BdcCanali b")
})
public class BdcCanali implements Serializable {
    private static final long serialVersionUID = 1L;
    @Expose
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id", nullable=false)
    private Integer id;

    @Expose
    @OneToOne
    @JoinColumn(name="id_broadcaster")
    private BdcBroadcasters bdcBroadcasters;
    @Expose
    @Column(name="nome", nullable=false)
    private String nome;
    @Expose
    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_canale", nullable=false)
    private TipoBroadcaster tipoCanale;
    @Expose
    @Column(name = "generalista", nullable = false, columnDefinition = "INT DEFAULT '0'")
    private int generalista;
    @Transient
    @Expose
    private int active;
    @Expose
    @Column(name = "special_radio", nullable = false, columnDefinition = "INT DEFAULT '0'")
    private int specialRadio;
    @Expose
    @Column(name = "data_inizio_valid", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date dataInizioValid;
    @Expose
    @Column(name = "data_fine_valid")
    private Date dataFineValid;
    @Expose
    @Column(name = "data_creazione", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date dataCreazione;
    @Expose
    @Column(name = "data_ultima_modifica",nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date dataUltimaModifica;
    @Expose
    @Column(name = "utente_ultima_modifica", nullable = false, columnDefinition = "STRING DEFAULT 'esercizio'")
    private String utenteUltimaModifica;

    public BdcCanali() {
        super();
    }

    public BdcCanali(Integer id, BdcBroadcasters bdcBroadcasters, String nome, TipoBroadcaster tipoCanale, int generalista,
                     int active, int specialRadio, Date dataInizioValid, Date dataFineValid, Date dataUltimaModifica,
                     String utenteUltimaModifica, Date dataCreazione) {
        super();
        this.id = id;
        this.bdcBroadcasters = bdcBroadcasters;
        this.nome = nome;
        this.tipoCanale = tipoCanale;
        this.generalista = generalista;
        this.active = active;
        this.specialRadio = specialRadio;
        this.dataInizioValid = dataInizioValid;
        this.dataFineValid = dataFineValid;
        this.dataUltimaModifica = dataUltimaModifica;
        this.utenteUltimaModifica = utenteUltimaModifica;
        this.dataCreazione = dataCreazione;
    }




    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BdcBroadcasters getBdcBroadcasters() {
        return bdcBroadcasters;
    }

    public void setBdcBroadcasters(BdcBroadcasters idBroadcaster) {
        this.bdcBroadcasters = idBroadcaster;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public TipoBroadcaster getTipoCanale() {
        return tipoCanale;
    }

    public void setTipoCanale(TipoBroadcaster tipo_canale) {
        this.tipoCanale = tipo_canale;
    }

    public int getGeneralista() {
        return generalista;
    }

    public void setGeneralista(int generalista) {
        this.generalista = generalista;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public int getSpecialRadio() {
        return specialRadio;
    }

    public void setSpecialRadio(int specialRadio) {
        this.specialRadio = specialRadio;
    }

    public Date getDataInizioValid() {
        return dataInizioValid;
    }

    public void setDataInizioValid(Date dataInizioValid) {
        this.dataInizioValid = dataInizioValid;
    }

    public Date getDataFineValid() {
        return dataFineValid;
    }

    public void setDataFineValid(Date dataFineValid) {
        this.dataFineValid = dataFineValid;
    }

    public Date getDataUltimaModifica() {
        return dataUltimaModifica;
    }

    public void setDataUltimaModifica(Date dataUltimaModifica) {
        this.dataUltimaModifica = dataUltimaModifica;
    }

    public String getUtenteUltimaModifica() {
        return utenteUltimaModifica;
    }

    public void setUtenteUltimaModifica(String utenteUltimaModifica) {
        this.utenteUltimaModifica = utenteUltimaModifica;
    }

    public Date getDataCreazione() {
        return dataCreazione;
    }


    public void setDataCreazione(Date dataCreazione) {
        this.dataCreazione = dataCreazione;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BdcCanali)) return false;

        BdcCanali bdcCanali = (BdcCanali) o;

        if (id != null ? !id.equals(bdcCanali.id) : bdcCanali.id != null) return false;
        if (nome != null ? !nome.equals(bdcCanali.nome) : bdcCanali.nome != null) return false;
        return tipoCanale == bdcCanali.tipoCanale;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (nome != null ? nome.hashCode() : 0);
        result = 31 * result + (tipoCanale != null ? tipoCanale.hashCode() : 0);
        return result;
    }

    public boolean isSpecialRadio(){
        return this.specialRadio > 0;
    }

    @Override
    public String toString() {
        return "BdcCanali{" +
                "id=" + id +
                ", bdcBroadcasters=" + bdcBroadcasters +
                ", nome='" + nome + '\'' +
                ", tipoCanale=" + tipoCanale +
                ", generalista=" + generalista +
                ", active=" + active +
                ", specialRadio=" + specialRadio +
                ", dataInizioValid=" + dataInizioValid +
                ", dataFineValid=" + dataFineValid +
                ", dataCreazione=" + dataCreazione +
                ", dataUltimaModifica=" + dataUltimaModifica +
                ", utenteUltimaModifica='" + utenteUltimaModifica + '\'' +
                '}';
    }
}