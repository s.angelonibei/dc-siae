package it.siae.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Table(name ="BDC_PRENORMALIZATION_RULE",schema = "MCMDB_debug")
@NamedQueries({@NamedQuery(name = "BdcPrenormalizationRule.all", query = "select x from BdcPrenormalizationRule x order by x.id desc")})
@Entity
public class BdcPrenormalizationRule {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(insertable = false, name = "id", nullable = false)
    private int id;

    @Column(name = "rule")
    private String rule;

    @Column(name = "flag_active")
    private int flagActive;
}
