package it.siae.scheduledTask;

import com.alkemytech.sophia.common.config.ConfigurationLoader;
import com.alkemytech.sophia.common.tools.DateTools;
import com.alkemytech.sophia.commons.aws.S3;
import com.mysql.jdbc.StringUtils;
import it.siae.csv.CsvInput;
import it.siae.entity.BdcInformationFileEntity;
import it.siae.entity.BdcPrenormalizationRule;
import it.siae.enums.Stato;
import it.siae.service.InformationFileEntityService;
import it.siae.utils.Constants;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.QuoteMode;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Data
@Slf4j
public class ConverterJob implements Runnable {

    private BdcInformationFileEntity fileEntity;
    private List<BdcPrenormalizationRule> categoriaUsoRule;
    private Map<String, String> categoriaUsoMap;
    private Map<String, String> genereAgcomMap;
    private InformationFileEntityService informationFileEntityService;

    private String sheetName;

    private Properties properties = new ConfigurationLoader()
            .withFilePath("configuration.properties")
            .load();

    public ConverterJob(BdcInformationFileEntity fileEntity,
                        List<BdcPrenormalizationRule> categoriaUsoRule,
                        Map<String, String> categoriaUsoMap,
                        Map<String, String> genereAgcomMap,
                        InformationFileEntityService informationFileEntityService) {
        this.fileEntity = fileEntity;
        this.categoriaUsoRule = categoriaUsoRule;
        this.categoriaUsoMap = categoriaUsoMap;
        this.genereAgcomMap = genereAgcomMap;
        this.informationFileEntityService = informationFileEntityService;
    }


    @Override
    public void run() {
        log.info("[{}] STARTING CONVERSION ON FILE", fileEntity.getNomeFile());
        log.debug("[{}] List RULE: {}", fileEntity.getNomeFile(), categoriaUsoRule.size());
        log.debug("[{}] List CATEGORIAUSO: {}", fileEntity.getNomeFile(), categoriaUsoMap.size());
        log.debug("[{}] List genereAGcom: {}", fileEntity.getNomeFile(), genereAgcomMap.size());
        log.debug("[{}] PATH S3: {}", fileEntity.getNomeFile(), fileEntity.getPercorso());

        //Recupero il file originale da s3
        try {
            File tempFile = File.createTempFile("buffer", ".tmp");
            log.debug("[{}] TEMP FILE CREATO", fileEntity.getNomeFile());
            tempFile.deleteOnExit();
            List<CsvInput> records = downloadFromS3(fileEntity.getPercorso());
            log.debug("[{}] RECORDS GENERATI DA S3", fileEntity.getNomeFile());
            fileEntity.setStato(Stato.IN_CONVERSIONE);
            log.debug("[{}] File found on s3 - saving status to IN_CONVERSIONE", fileEntity.getNomeFile());
            log.debug("[{}] Entity: {}", fileEntity.getNomeFile(), fileEntity.toString());
            informationFileEntityService.save(fileEntity);
            elaborateFile(records, tempFile, fileEntity.getPercorso());
            log.debug("[{}] Report Generated on S3", fileEntity.getNomeFile());
        } catch (Exception e) {
            fileEntity.setStato(Stato.ERRORE);
            fileEntity.setDecodificaErrore(e.getMessage());
            fileEntity.setStackTrace(ExceptionUtils.getStackTrace(e));
            try {
                informationFileEntityService.save(fileEntity);
            } catch (ParseException parseException) {
                parseException.printStackTrace();
            }
            log.error(e.getMessage());
        }
    }

    public List<CsvInput> downloadFromS3(String path) throws IOException {
        S3 s3 = new S3(properties);
        s3.startup();
        List<CsvInput> fileParsed = new ArrayList<>();
        S3.Url s3Url = new S3.Url(path);
        log.debug("[{}] RECUPERO I RECORD DA S3 CON URI: {}", fileEntity.getNomeFile(), path);
        InputStream in = s3.getObject(s3Url).getObjectContent();
        log.debug("InputStream recuperato da s3");
        if (fileEntity.getNomeFile().endsWith(Constants.EXCEL_EXT_XLSX) || fileEntity.getNomeFile().endsWith(Constants.EXCEL_EXT_XLS)) {
            fileParsed = parseFromExcel(in);
        } else {
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));

            int lineCounter = 0;
            String line = null;
            while (reader.ready() && (line = reader.readLine()) != null) {

                log.debug("[{}] Line parsing: {}", fileEntity.getNomeFile(), line);

                CsvInput input = new CsvInput(line.split(";"));
                fileParsed.add(input);
                lineCounter++;
            }
            log.info("[{}] Line parsed: {}", fileEntity.getNomeFile(), lineCounter);
        }

        return fileParsed;
    }


    private void elaborateFile(List<CsvInput> records, File tempFile, String path) throws Exception {
        log.info("ELABORATE FILE");
        log.debug("[{}] Applying rule...", fileEntity.getNomeFile());
        S3 s3 = new S3(properties);
        s3.startup();
        S3.Url s3Url = new S3.Url(path);
        log.debug("[{}] s3Url created", fileEntity.getNomeFile());
        int linecounter = 0;
        for (CsvInput record : records) {

            log.info("Applying rule on record: {}", record.toString());

            if (linecounter >= 0) {

                log.info("{}", linecounter);

                //boolean rule3Applied = false;

                /**
                 * @Author: Nacucchio Francesco
                 * @Version: R9-2022
                1)	Categorie d’uso 4 e 5  Se i campi della colonna U “Categoria d’uso”
                del file di input Rai sono “vuoti”, essi andranno popolati con
                la categoria d’uso 4 o 5 a seconda del “Genere trasmissione Rai”
                indicato nella seguente tabella di transcodifica e presente nella colonna B
                Se nella colonna B del file di input Rai
                non sono presenti i codici dei generi trasmissione indicati
                nella soprastante tabella di transcodifica o i
                relativi campi sono del tutto privi di un genere trasmissione,
                il campo della colonna U “Categoria d’uso” rimarrà “vuoto”.
                 **/

                log.debug("[{}] Applico regola 1...", fileEntity.getNomeFile());

                if (categoriaUsoRule.get(0).getFlagActive() == 1) {
                    if (StringUtils.isNullOrEmpty(record.getCategoriaduso()) && categoriaUsoMap.containsKey(record.getGenereTrasmissione())) {
                        record.setCategoriaduso(categoriaUsoMap.get(record.getGenereTrasmissione()));
                    }
                }


                /**
                 * @Author: Nacucchio Francesco
                 * @Version: R9-2022
                2)	Durata trasmissione “nulla”  Se i campi della “Durata trasmissione”
                (colonna H del file di input Rai) sono uguali a 00:00:00 e i campi della colonna U
                del file di input Rai (aggiornata dopo la regola 1)
                sono privi di categoria d’uso, il campo della colonna U andrà popolato
                con il codice 5 e quello della colonna Q “Titolo opera musicale”
                con la dicitura “Chiusura trasmissione”.
                 */

                log.debug("[{}] Applico regola 2...", fileEntity.getNomeFile());
                if (categoriaUsoRule.get(1).getFlagActive() == 1) {
                    if (record.getDurataTrasmissione().equals(Constants.RULE_DURATA_TRASMISSIONE)
                            && StringUtils.isNullOrEmpty(record.getCategoriaduso())) {
                        record.setCategoriaduso(Constants.RULE_CATEGORIAUSO_5);
                        record.setTitoloOperaMusicale(Constants.RULE_TITOLO_OPERA_MUSICALE);
                        //rule3Applied = true;
                    }
                }

                /**
                 * @Author: Nacucchio Francesco
                 * @Version: R9-2022
                3)	Durata trasmissione “calcolata”  Il campo della “Durata trasmissione”
                (colonna H del file di input Rai) dovrà essere calcolato come differenza
                tra la colonna G “Orario fine trasmissione” e
                la colonna F “Orario inizio trasmissione”.
                 */

                log.debug("[{}] Applico regola 3...", fileEntity.getNomeFile());
                if (categoriaUsoRule.get(2).getFlagActive() == 1 && linecounter > 0) { //&& rule3Applied) {
                    if (!StringUtils.isNullOrEmpty(record.getOrarioInizioTrasmissione())
                            && !StringUtils.isNullOrEmpty(record.getOrarioFineTrasmissione())
                            && !StringUtils.isNullOrEmpty(record.getDataInizioTrasmissione()))
                        record.setDurataTrasmissione(
                                formatDate(
                                        record.getDataInizioTrasmissione(),
                                        record.getOrarioInizioTrasmissione(),
                                        record.getOrarioFineTrasmissione()
                                ));
                }

                /**
                 * @Author: Nacucchio Francesco
                 * @Version: R9-2022
                4)	Genere trasmissione 3702 Se il “Genere trasmissione” della colonna B del
                file di input Rai è 3702 e i campi della colonna U del file di input Rai
                hanno categoria d’uso 1, 2 o 3, il campo della colonna B del “Genere trasmissione”
                del file di input Rai sarà 10; mentre se i campi della colonna U del file di
                input Rai hanno categoria d’uso 4, il campo della colonna B del “Genere trasmissione”
                del file di input Rai sarà 9.
                 */

                log.debug("[{}] Applico regola 4...", fileEntity.getNomeFile());
                if (categoriaUsoRule.get(3).getFlagActive() == 1) {

                    if (record.getGenereTrasmissione() != null && record.getCategoriaduso() != null) {
                        if (record.getGenereTrasmissione().equals(Constants.RULE_GENERE_TRASMISSIONE_3702)) {
                            if (record.getCategoriaduso().equals(Constants.RULE_CATEGORIAUSO_1)
                                    || record.getCategoriaduso().equals(Constants.RULE_CATEGORIAUSO_2)
                                    || record.getCategoriaduso().equals(Constants.RULE_CATEGORIAUSO_3)) {
                                record.setGenereTrasmissione(Constants.RULE_GENERE_TRASMISSIONE_10);
                            } else if (record.getCategoriaduso().equals(Constants.RULE_CATEGORIAUSO_4)) {
                                record.setGenereTrasmissione(Constants.RULE_GENERE_TRASMISSIONE_9);
                            }
                        }
                    }
                }

                /**
                 * @Author: Nacucchio Francesco
                 * @Version: R9-2022
                5)	Genere trasmissione Agcom I codici presenti nella colonna B “Genere trasmissione”
                del file di input Rai dovranno essere convertiti nel relativo genere Agcom.
                Nel file di input della colonna B “Genere trasmissione” andrà applicata la tabella di transcodifica
                 */

                log.debug("[{}] Applico regola 5...", fileEntity.getNomeFile());
                if (categoriaUsoRule.get(4).getFlagActive() == 1) {
                    if (record.getGenereTrasmissione() != null
                            && genereAgcomMap.containsKey(record.getGenereTrasmissione())) {
                        record.setGenereTrasmissione(genereAgcomMap.get(record.getGenereTrasmissione()));
                    }


                }
            }
            linecounter++;
        }

        log.debug("[{}] Generarting Report", fileEntity.getNomeFile());
        if (fileEntity.getNomeFile().endsWith(Constants.EXCEL_EXT_XLSX) || fileEntity.getNomeFile().endsWith(Constants.EXCEL_EXT_XLS)) {
            generateReportExcel(tempFile, records);
        } else {
            generateReportCSV(tempFile, records);
        }

        log.debug("[{}] Rule applied, overwriting file to S3", fileEntity.getNomeFile());
        s3.upload(s3Url, tempFile);
        log.info("[{}] Report Generated and Overwrited to S3", fileEntity.getNomeFile());
        fileEntity.setStato(Stato.DA_ELABORARE);
        informationFileEntityService.save(fileEntity);
    }

    public boolean isValid(String dateStr) {
        DateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        sdf.setLenient(false);
        try {
            sdf.parse(dateStr);
        } catch (ParseException e) {
            return false;
        }
        return true;
    }

    private String formatDate(String dataInizioTrasmissione, String orarioInizioTrasmissione, String orarioFineTrasmissione){
        String begin = dataInizioTrasmissione + " " + orarioInizioTrasmissione;
        Date beginTime = DateTools.stringToDate(begin, dataInizioTrasmissione.contains("/") ? DateTools.DATETIME_ITA_FORMAT_SLASH : DateTools.DATETIME_ITA_FORMAT_DASH);
        Date endTime = null;
        String result = "";
        if (orarioFineTrasmissione != null && DateTools.stringToDate(orarioFineTrasmissione, DateTools.TIME_COMPLETE_FORMAT) != null) {
            String end = dataInizioTrasmissione + " " + orarioFineTrasmissione;
            endTime = DateTools.stringToDate(end, dataInizioTrasmissione.contains("/") ? DateTools.DATETIME_ITA_FORMAT_SLASH : DateTools.DATETIME_ITA_FORMAT_DASH);
            if (endTime != null && beginTime != null && endTime.before(beginTime)) {
                endTime = DateUtils.addDays(endTime, 1);
            }
            result = calculateTrasmissionTime(beginTime, endTime);
            log.debug("[{}] Result calculate differences: {} - {} = {}", fileEntity.getNomeFile(), beginTime, endTime, result);
        }
        return result;
    }

    private String calculateTrasmissionTime(Date startingTime, Date endTime) {


        final LocalTime firstTime = LocalDateTime.ofInstant(startingTime.toInstant(), ZoneId.systemDefault()).toLocalTime();
        final LocalTime secondTime = LocalDateTime.ofInstant(endTime.toInstant(), ZoneId.systemDefault()).toLocalTime();

        final LocalDateTime firstDateTime = LocalDateTime.of(LocalDate.now(), firstTime);
        final LocalDateTime secondDateTime = LocalDateTime.of(
                LocalDate.now().plusDays(endTime.equals("00:00:00") ? 1 : 0),
                secondTime
        );

        final Duration diff = Duration.between(firstDateTime, secondDateTime).abs();

        return String.format(
                "%02d:%02d:%02d",
                diff.toDays() < 1 ? diff.toHours() : 24,
                diff.toMinutes() % 60,
                diff.getSeconds() - diff.toMinutes() * 60
        );
    }

    private void generateReportCSV(File file, List<CsvInput> report) throws Exception {
        try (final FileWriter fw = new FileWriter(file, false)) {
            try (final CSVPrinter printer = new CSVPrinter(new BufferedWriter(fw),
                    CSVFormat.EXCEL.withDelimiter(';').withQuoteMode(QuoteMode.MINIMAL))) {

                for (CsvInput r : report) {
                    log.debug("[{}] Raw CsvInput: {}", fileEntity.getNomeFile(), r.toString());
                    printer.print(r.getCanale() != null ? r.getCanale().trim() : r.getCanale());
                    printer.print(r.getGenereTrasmissione() != null ? r.getGenereTrasmissione().trim() : r.getGenereTrasmissione());
                    printer.print(r.getTitoloTrasmissione() != null ? r.getTitoloTrasmissione().trim() : r.getTitoloTrasmissione());
                    printer.print(r.getTitoloOriginaleTrasmissione() != null ? r.getTitoloOriginaleTrasmissione().trim() : r.getTitoloOriginaleTrasmissione());
                    printer.print(r.getDataInizioTrasmissione() != null ? r.getDataInizioTrasmissione().trim() : r.getDataInizioTrasmissione());
                    printer.print(r.getOrarioInizioTrasmissione() != null ? r.getOrarioInizioTrasmissione().trim() : r.getOrarioInizioTrasmissione());
                    printer.print(r.getOrarioFineTrasmissione() != null ? r.getOrarioFineTrasmissione().trim() : r.getOrarioFineTrasmissione());
                    printer.print(r.getDurataTrasmissione() != null ? r.getDurataTrasmissione().trim() : r.getDurataTrasmissione());
                    printer.print(r.getReplicaOPrimaEsecuzione() != null ? r.getReplicaOPrimaEsecuzione().trim() : r.getReplicaOPrimaEsecuzione());
                    printer.print(r.getPaeseDiProduzione() != null ? r.getPaeseDiProduzione().trim() : r.getPaeseDiProduzione());
                    printer.print(r.getAnnoDiProduzione() != null ? r.getAnnoDiProduzione().trim() : r.getAnnoDiProduzione());
                    printer.print(r.getProduttore() != null ? r.getProduttore().trim() : r.getProduttore());
                    printer.print(r.getRegista() != null ? r.getRegista().trim() : r.getRegista());
                    printer.print(r.getTitoloEpisodio() != null ? r.getTitoloEpisodio().trim() : r.getTitoloEpisodio());
                    printer.print(r.getTitoloOriginaleEpisodio() != null ? r.getTitoloOriginaleEpisodio().trim() : r.getTitoloOriginaleEpisodio());
                    printer.print(r.getNumeroProgressivoEpisodio() != null ? r.getNumeroProgressivoEpisodio().trim() : r.getNumeroProgressivoEpisodio());
                    printer.print(r.getTitoloOperaMusicale() != null ? r.getTitoloOperaMusicale().trim() : r.getTitoloOperaMusicale());
                    printer.print(r.getCompositoreAutore1() != null ? r.getCompositoreAutore1().trim() : r.getCompositoreAutore1());
                    printer.print(r.getCompositoreAutore2() != null ? r.getCompositoreAutore2().trim() : r.getCompositoreAutore2());
                    printer.print(r.getDurataEffettivaDiUtilizzo() != null ? r.getDurataEffettivaDiUtilizzo().trim() : r.getDurataEffettivaDiUtilizzo());
                    printer.print(r.getCategoriaduso() != null ? r.getCategoriaduso().trim() : r.getCategoriaduso());
                    printer.print(r.getDiffusioneRegionale() != null ? r.getDiffusioneRegionale().trim() : r.getDiffusioneRegionale());
                    printer.print(r.getIdentificativoRai() != null ? r.getIdentificativoRai().trim() : r.getIdentificativoRai());
                    printer.println();
                }
            }
        }
    }

    private String getGenericCellValue(Cell cell) {
        String formattedCell;
        switch (cell.getCellTypeEnum()) {
            case STRING:
                formattedCell = cell.getStringCellValue();
                break;
            case NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    formattedCell = new SimpleDateFormat("dd/MM/yyyy").format(DateUtil.getJavaDate(cell.getNumericCellValue()));
                } else {
                    formattedCell = NumberToTextConverter.toText(cell.getNumericCellValue());
                }
                break;
            case BOOLEAN:
                formattedCell = Boolean.toString(cell.getBooleanCellValue());
                break;
            default:
                return null;
        }
        return formattedCell;
    }

    public List<CsvInput> parseFromExcel(InputStream in) throws IOException {

        //Create Workbook instance holding reference to .xlsx file
        log.debug("reading records from excel...");
        List<CsvInput> listDto = new ArrayList<>();

        //Get first/desired sheet from the workbook
        try (Workbook workbook = WorkbookFactory.create(in)) {
            Sheet sheet = workbook.getSheetAt(0);
            sheetName = sheet.getSheetName();

            for (int r = 1; r <= sheet.getLastRowNum(); r++) {
                Row row = sheet.getRow(r);
                if (row != null && row.getCell(0) != null && !(row.getCell(0).toString().equalsIgnoreCase(""))) {
                    //For each row, iterate through all the columns
                    Iterator<Cell> cellIterator = row.cellIterator();

                    CsvInput dto = new CsvInput();
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        switch (cell.getColumnIndex()) {
                            case 0:
                                dto.setCanale(getGenericCellValue(cell));
                                break;
                            case 1:
                                dto.setGenereTrasmissione(getGenericCellValue(cell));
                                break;
                            case 2:
                                dto.setTitoloTrasmissione(getGenericCellValue(cell));
                                break;
                            case 3:
                                dto.setTitoloOriginaleTrasmissione(getGenericCellValue(cell));
                                break;
                            case 4:
                                dto.setDataInizioTrasmissione(new SimpleDateFormat(Constants.EXCEL_DATE_FORMAT).format(cell.getDateCellValue()));
                                break;
                            case 5:
                                dto.setOrarioInizioTrasmissione(new SimpleDateFormat(Constants.EXCEL_TIME_FORMAT).format(cell.getDateCellValue()));
                                break;
                            case 6:
                                dto.setOrarioFineTrasmissione(new SimpleDateFormat(Constants.EXCEL_TIME_FORMAT).format(cell.getDateCellValue()));
                                break;
                            case 7:
                                dto.setDurataTrasmissione(new SimpleDateFormat(Constants.EXCEL_TIME_FORMAT).format(cell.getDateCellValue()));
                                break;
                            case 8:
                                dto.setReplicaOPrimaEsecuzione(getGenericCellValue(cell));
                                break;
                            case 9:
                                dto.setPaeseDiProduzione(getGenericCellValue(cell));
                                break;
                            case 10:
                                dto.setAnnoDiProduzione(getGenericCellValue(cell));
                                break;
                            case 11:
                                dto.setProduttore(getGenericCellValue(cell));
                                break;
                            case 12:
                                dto.setRegista(getGenericCellValue(cell));
                                break;
                            case 13:
                                dto.setTitoloEpisodio(getGenericCellValue(cell));
                                break;
                            case 14:
                                dto.setTitoloOriginaleEpisodio(getGenericCellValue(cell));
                                break;
                            case 15:
                                dto.setNumeroProgressivoEpisodio(getGenericCellValue(cell));
                                break;
                            case 16:
                                dto.setTitoloOperaMusicale(getGenericCellValue(cell));
                                break;
                            case 17:
                                dto.setCompositoreAutore1(getGenericCellValue(cell));
                                break;
                            case 18:
                                dto.setCompositoreAutore2(getGenericCellValue(cell));
                                break;
                            case 19:
                                dto.setDurataEffettivaDiUtilizzo(new SimpleDateFormat(Constants.EXCEL_TIME_FORMAT).format(cell.getDateCellValue()));
                                break;
                            case 20:
                                dto.setCategoriaduso(getGenericCellValue(cell));
                                break;
                            case 21:
                                dto.setDiffusioneRegionale(getGenericCellValue(cell));
                                break;
                            case 22:
                                dto.setIdentificativoRai(getGenericCellValue(cell));
                                break;
                        }
                    }
                    listDto.add(dto);
                }
            }
        }
        return listDto;
    }

    public void generateReportExcel(File file, List<CsvInput> csvInputs) throws IOException {

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet(sheetName);
        sheet.addIgnoredErrors(new CellRangeAddress(0, csvInputs.size(), 0, 22), IgnoredErrorType.NUMBER_STORED_AS_TEXT);
        XSSFCellStyle generaltext = workbook.createCellStyle();
        generaltext.setAlignment(HorizontalAlignment.RIGHT);

        int rowCount = 1;

        Row header = sheet.createRow(0);
        header.createCell(0).setCellValue("Canale");
        header.createCell(1).setCellValue("Genere trasmissione");
        header.createCell(2).setCellValue("Titolo trasmissione");
        header.createCell(3).setCellValue("Titolo originale trasmissione");
        header.createCell(4).setCellValue("Data inizio trasmissione");
        header.createCell(5).setCellValue("Orario inizio trasmissione");
        header.createCell(6).setCellValue("Orario fine trasmissione");
        header.createCell(7).setCellValue("Durata trasmissione");
        header.createCell(8).setCellValue("Replica o prima esecuzione");
        header.createCell(9).setCellValue("Paese di produzione");
        header.createCell(10).setCellValue("Anno di produzione");
        header.createCell(11).setCellValue("Produttore");
        header.createCell(12).setCellValue("Regista");
        header.createCell(13).setCellValue("Titolo episodio");
        header.createCell(14).setCellValue("Titolo originale episodio");
        header.createCell(15).setCellValue("Numero progressivo episodio");
        header.createCell(16).setCellValue("Titolo opera musicale");
        header.createCell(17).setCellValue("Compositore/Autore (1)");
        header.createCell(18).setCellValue("Compositore/Autore (2)");
        header.createCell(19).setCellValue("Durata effettiva di utilizzo");
        header.createCell(20).setCellValue("Categoria d'uso");
        header.createCell(21).setCellValue("Diffusione regionale");
        header.createCell(22).setCellValue("Identificativo Rai");

        for (CsvInput record : csvInputs) {

            Row row = sheet.createRow(rowCount++);

            if (record.getCanale() != null)
                row.createCell(0).setCellValue(record.getCanale());

            if (record.getGenereTrasmissione() != null) {
                row.createCell(1).setCellValue(record.getGenereTrasmissione());
                row.getCell(1).setCellStyle(generaltext);
            }

            if (record.getTitoloTrasmissione() != null)
                row.createCell(2).setCellValue(record.getTitoloTrasmissione());

            if (record.getTitoloOriginaleTrasmissione() != null)
                row.createCell(3).setCellValue(record.getTitoloOriginaleTrasmissione());

            if (record.getDataInizioTrasmissione() != null) {
                row.createCell(4).setCellValue(record.getDataInizioTrasmissione());
                row.getCell(4).setCellStyle(generaltext);
            }

            if (record.getOrarioInizioTrasmissione() != null) {
                row.createCell(5).setCellValue(record.getOrarioInizioTrasmissione());
                row.getCell(5).setCellStyle(generaltext);
            }

            if (record.getOrarioFineTrasmissione() != null) {
                row.createCell(6).setCellValue(record.getOrarioFineTrasmissione());
                row.getCell(6).setCellStyle(generaltext);
            }

            if (record.getDurataTrasmissione() != null) {
                row.createCell(7).setCellValue(record.getDurataTrasmissione());
                row.getCell(7).setCellStyle(generaltext);
            }

            if (record.getReplicaOPrimaEsecuzione() != null) {
                row.createCell(8).setCellValue(record.getReplicaOPrimaEsecuzione());
                row.getCell(8).setCellStyle(generaltext);
            }

            if (record.getPaeseDiProduzione() != null)
                row.createCell(9).setCellValue(record.getPaeseDiProduzione());

            if (record.getAnnoDiProduzione() != null) {
                row.createCell(10).setCellValue(record.getAnnoDiProduzione());
                row.getCell(10).setCellStyle(generaltext);
            }

            if (record.getProduttore() != null)
                row.createCell(11).setCellValue(record.getProduttore());

            if (record.getRegista() != null)
                row.createCell(12).setCellValue(record.getRegista());

            if (record.getTitoloEpisodio() != null)
                row.createCell(13).setCellValue(record.getTitoloEpisodio());

            if (record.getTitoloOriginaleEpisodio() != null)
                row.createCell(14).setCellValue(record.getTitoloOriginaleEpisodio());

            if (record.getNumeroProgressivoEpisodio() != null) {
                row.createCell(15).setCellValue(record.getNumeroProgressivoEpisodio());
                row.getCell(15).setCellStyle(generaltext);
            }

            if (record.getTitoloOperaMusicale() != null)
                row.createCell(16).setCellValue(record.getTitoloOperaMusicale());

            if (record.getCompositoreAutore1() != null)
                row.createCell(17).setCellValue(record.getCompositoreAutore1());

            if (record.getCompositoreAutore2() != null)
                row.createCell(18).setCellValue(record.getCompositoreAutore2());

            if (record.getDurataEffettivaDiUtilizzo() != null) {
                row.createCell(19).setCellValue(record.getDurataEffettivaDiUtilizzo());
                row.getCell(19).setCellStyle(generaltext);
            }

            if (record.getCategoriaduso() != null) {
                row.createCell(20).setCellValue(record.getCategoriaduso());
                row.getCell(20).setCellStyle(generaltext);
            }

            if (record.getDiffusioneRegionale() != null) {
                row.createCell(21).setCellValue(record.getDiffusioneRegionale());
                row.getCell(21).setCellStyle(generaltext);
            }

            if (record.getIdentificativoRai() != null) {
                row.createCell(22).setCellValue(record.getIdentificativoRai());
                row.getCell(22).setCellStyle(generaltext);
            }
        }

        try (FileOutputStream outputStream = new FileOutputStream(file)) {
            workbook.write(outputStream);
        }
    }
}
