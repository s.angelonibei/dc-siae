package it.siae.scheduledTask;


import it.siae.entity.BdcInformationFileEntity;
import it.siae.entity.BdcPrenormalizationRule;
import it.siae.enums.Stato;
import it.siae.repository.IBdcPrenormalizationAgcom;
import it.siae.repository.IBdcPrenormalizationCategoriaUso;
import it.siae.repository.IBdcPrenormalizationRule;
import it.siae.service.InformationFileEntityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.concurrent.RejectedExecutionException;

@Component
@Slf4j
public class ConverterEngine {

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");

    @Autowired
    private InformationFileEntityService informationFileEntityService;

    @Autowired
    private IBdcPrenormalizationRule bdcPrenormalizationRule;

    @Autowired
    private IBdcPrenormalizationCategoriaUso bdcPrenormalizationCategoriaUso;

    @Autowired
    private IBdcPrenormalizationAgcom bdcPrenormalizationAgcom;

    @Autowired
    @Qualifier("CustomTaskExecutor")
    private TaskExecutor taskExecutor;

    @Scheduled(fixedRate = 10000)
    public void scheduleTaskWithCronExpression() {

        log.info("Prenormalizzazione Rai Service is running");
        log.info("Cron Task: Current Time - {}", formatter.format(LocalDateTime.now()));
        List<BdcInformationFileEntity> reports = informationFileEntityService.findByStato(Stato.DA_CONVERTIRE);
        log.info("File conflitti da Elaborare: " + reports.size());
        List<BdcPrenormalizationRule> categoriaUsoRule = bdcPrenormalizationRule.findAll();
        Map<String, String> categoriaUsoMap = bdcPrenormalizationCategoriaUso.findAllMap();
        Map<String, String> genereAgcomMap = bdcPrenormalizationAgcom.findAllMap();
        log.info("File conflitti da Elaborare: " + reports.size());
        for (BdcInformationFileEntity report : reports) {
            ConverterJob job = new ConverterJob(report, categoriaUsoRule, categoriaUsoMap, genereAgcomMap, informationFileEntityService);
            try {
                taskExecutor.execute(job);
            } catch (RejectedExecutionException r) {
                log.info("Impossibile prendere in carico il file: tutti gli slot disponibili sono occupati");
            }
        }
        log.info("Converter Engine is running");
    }
}
