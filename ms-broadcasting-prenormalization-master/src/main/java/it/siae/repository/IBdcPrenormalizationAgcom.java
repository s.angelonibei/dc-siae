package it.siae.repository;

import it.siae.entity.BdcPrenormalizationAgcom;
import it.siae.entity.BdcPrenormalizationCategoriaUso;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public interface IBdcPrenormalizationAgcom extends JpaRepository<BdcPrenormalizationAgcom, String> {

    List<BdcPrenormalizationAgcom> findAll();

    default Map<String, String> findAllMap() {
        return findAll().stream().collect(Collectors.toMap(BdcPrenormalizationAgcom::getGenereTrasmissione, BdcPrenormalizationAgcom::getGenereAgcom));
    }
}
