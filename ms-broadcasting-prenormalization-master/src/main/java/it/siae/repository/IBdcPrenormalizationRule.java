package it.siae.repository;

import it.siae.entity.BdcPrenormalizationRule;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IBdcPrenormalizationRule extends JpaRepository<BdcPrenormalizationRule, Integer> {

    List<BdcPrenormalizationRule> findAll();
}
