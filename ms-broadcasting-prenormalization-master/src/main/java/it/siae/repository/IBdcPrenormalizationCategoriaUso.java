package it.siae.repository;

import it.siae.entity.BdcPrenormalizationCategoriaUso;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public interface IBdcPrenormalizationCategoriaUso extends JpaRepository<BdcPrenormalizationCategoriaUso, String> {

    List<BdcPrenormalizationCategoriaUso> findAll();

    default Map<String, String> findAllMap() {
        return findAll().stream().collect(Collectors.toMap(BdcPrenormalizationCategoriaUso::getGenereTrasmissione, BdcPrenormalizationCategoriaUso::getCategoriaUso));
    }
}
