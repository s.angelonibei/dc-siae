package it.siae.csv;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.sf.jsefa.csv.annotation.CsvDataType;

import java.io.Serializable;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class CsvInput implements Serializable {

    String canale;
    String genereTrasmissione;
    String titoloTrasmissione;
    String titoloOriginaleTrasmissione;
    String dataInizioTrasmissione;
    String orarioInizioTrasmissione;
    String orarioFineTrasmissione;
    String durataTrasmissione;
    String replicaOPrimaEsecuzione;
    String paeseDiProduzione;
    String annoDiProduzione;
    String produttore;
    String regista;
    String titoloEpisodio;
    String titoloOriginaleEpisodio;
    String numeroProgressivoEpisodio;
    String titoloOperaMusicale;
    String compositoreAutore1;
    String compositoreAutore2;
    String durataEffettivaDiUtilizzo;
    String categoriaduso;
    String diffusioneRegionale;
    String identificativoRai;

    public CsvInput(Object [] obj){
        if (null != obj && obj.length == 22) {
            try {
                canale = (String) obj[0];
                genereTrasmissione = (String) obj[1];
                titoloTrasmissione = (String) obj[2];
                titoloOriginaleTrasmissione = (String) obj[3];
                dataInizioTrasmissione = (String) obj[4];
                orarioInizioTrasmissione = (String) obj[5];
                orarioFineTrasmissione = (String) obj[6];
                durataTrasmissione = (String) obj[7];
                replicaOPrimaEsecuzione = (String) obj[8];
                paeseDiProduzione = (String) obj[9];
                annoDiProduzione = (String) obj[10];
                produttore = (String) obj[11];
                regista = (String) obj[12];
                titoloEpisodio = (String) obj[13];
                titoloOriginaleEpisodio = (String) obj[14];
                numeroProgressivoEpisodio = (String) obj[15];
                titoloOperaMusicale = (String) obj[16];
                compositoreAutore1 = (String) obj[17];
                compositoreAutore2 = (String) obj[18];
                durataEffettivaDiUtilizzo = (String) obj[19];
                categoriaduso = (String) obj[20];
                diffusioneRegionale = (String) obj[21];
                identificativoRai = null;


            } catch (Throwable e) {
                e.printStackTrace();
            }
        } else if (null != obj && obj.length == 23){
            canale = (String) obj[0];
            genereTrasmissione = (String) obj[1];
            titoloTrasmissione = (String) obj[2];
            titoloOriginaleTrasmissione = (String) obj[3];
            dataInizioTrasmissione = (String) obj[4];
            orarioInizioTrasmissione = (String) obj[5];
            orarioFineTrasmissione = (String) obj[6];
            durataTrasmissione = (String) obj[7];
            replicaOPrimaEsecuzione = (String) obj[8];
            paeseDiProduzione = (String) obj[9];
            annoDiProduzione = (String) obj[10];
            produttore = (String) obj[11];
            regista = (String) obj[12];
            titoloEpisodio = (String) obj[13];
            titoloOriginaleEpisodio = (String) obj[14];
            numeroProgressivoEpisodio = (String) obj[15];
            titoloOperaMusicale = (String) obj[16];
            compositoreAutore1 = (String) obj[17];
            compositoreAutore2 = (String) obj[18];
            durataEffettivaDiUtilizzo = (String) obj[19];
            categoriaduso = (String) obj[20];
            diffusioneRegionale = (String) obj[21];
            identificativoRai = (String) obj[22];
        }
    }
}
