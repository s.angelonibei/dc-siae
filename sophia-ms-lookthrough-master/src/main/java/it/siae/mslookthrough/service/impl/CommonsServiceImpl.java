/*
 * @Autor A.D.C.
 */

package it.siae.mslookthrough.service.impl;


import it.siae.mslookthrough.entity.MmRichiesteLookThrough;
import it.siae.mslookthrough.repository.MmRichiesteLookThroughRepository;
import it.siae.mslookthrough.service.CommonsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;


@Service
@Transactional
public class CommonsServiceImpl implements CommonsService {

    private final Logger logger = LoggerFactory.getLogger(CommonsServiceImpl.class);

    @Autowired
    private MmRichiesteLookThroughRepository LookThroughRepo;

    @Override
    public void updateReportStatus(MmRichiesteLookThrough report) {
        report.setUltimoAggiornamento(new Date());
        LookThroughRepo.save(report);
        LookThroughRepo.flush();
    }

}
