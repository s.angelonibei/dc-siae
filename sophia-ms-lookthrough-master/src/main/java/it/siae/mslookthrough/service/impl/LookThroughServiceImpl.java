/*
 * @Autor A.D.C.
 */

package it.siae.mslookthrough.service.impl;

import it.siae.mslookthrough.Filter.CreateRequestLookThrough;
import it.siae.mslookthrough.dto.MmRichiesteLookThroughDTO;
import it.siae.mslookthrough.entity.MmRichiesteLookThrough;
import it.siae.mslookthrough.entity.enumeration.StatoLookThroughEnum;
import it.siae.mslookthrough.repository.MmRichiesteLookThroughRepository;
import it.siae.mslookthrough.service.LookThroughService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class LookThroughServiceImpl implements LookThroughService {

    @Autowired
    MmRichiesteLookThroughRepository LookThroughRepo;

    Logger logger = LoggerFactory.getLogger(LookThroughServiceImpl.class);

    @Override
    public List<MmRichiesteLookThrough> createReportRequest(CreateRequestLookThrough request) throws Exception {
        MmRichiesteLookThrough r = new MmRichiesteLookThrough();
        String idsLabel = "";
        int i = 0;
        List<String> ids = request.getIdLabel().stream().sorted((o1, o2) -> o1.compareTo(o2)).collect(Collectors.toList());

        for (String s : ids) {
            idsLabel = idsLabel.concat(s);
            i++;
            if (i < request.getIdLabel().size()) {
                idsLabel = idsLabel.concat("-");
            }
        }
        r.setIdRipartizioneSiada(request.getIdRipartizioneSiada());
        r.setIdLabel(idsLabel);
        r.setTipoDiritto(request.getTipoDiritto().size()>1 ? request.getTipoDiritto().get(0)+"_"+request.getTipoDiritto().get(1) : request.getTipoDiritto().get(0));
        r.setSocieta(request.getSocieta());
        r.setStato(StatoLookThroughEnum.DA_ELABORARE.getName());
        r.setDataCreazione(new Date());
        r.setUltimoAggiornamento(new Date());
        r.setCreatoDa(request.getCreatoDa());
        List<MmRichiesteLookThrough> l = LookThroughRepo.findByIdRipartizioneSiadaAndIdLabelAndTipoDirittoAndStatoNot(r.getIdRipartizioneSiada(), r.getIdLabel(),r.getTipoDiritto(),StatoLookThroughEnum.ERRORE.getName());
        if (l != null && !(l.size() > 0)) {
            LookThroughRepo.save(r);
        } else {
            logger.info("ERROR !! Duplicate key , insert into MM_RICHIESTE_LookThrough FAILED");
            throw new Exception("E' stata già trovata una richiesta con i parametri inseriti");
        }
        return LookThroughRepo.findByIdRipartizioneSiadaAndIdLabelAndStato(r.getIdRipartizioneSiada(), r.getIdLabel(),r.getStato());
    }

    public List<MmRichiesteLookThroughDTO> toDtoList(List<MmRichiesteLookThrough> list){
        List<MmRichiesteLookThroughDTO> dtoList = new ArrayList<>();
        for(MmRichiesteLookThrough m : list){
            MmRichiesteLookThroughDTO dto = new MmRichiesteLookThroughDTO();
            dto.setIdRichiesta(m.getIdRichiesta());
            dto.setIdRipartizioneSiada(m.getIdRipartizioneSiada());
            dto.setIdLabel(m.getIdLabel());
            dto.setTipoDiritto(m.getTipoDiritto());
            dto.setSocieta(m.getSocieta());
            dto.setStato(m.getStato());
            dto.setDataCreazione(m.getDataCreazione());
            dto.setUltimoAggiornamento(m.getUltimoAggiornamento());
            dto.setCreatoDa(m.getCreatoDa());
            dto.setErrore(m.getErrore());
            dto.setFileDem(m.getFileDem());
            dto.setFileDrm(m.getFileDrm());

            dtoList.add(dto);
        }
        return dtoList;
    }


}
