/*
 * @Autor A.D.C.
 */

package it.siae.mslookthrough.service;


import it.siae.mslookthrough.entity.MmRichiesteLookThrough;

public interface CommonsService {
    void updateReportStatus(MmRichiesteLookThrough report);
}
