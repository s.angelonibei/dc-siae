/*
 * @Autor A.D.C.
 */

package it.siae.mslookthrough.service;

import it.siae.mslookthrough.Filter.CreateRequestLookThrough;
import it.siae.mslookthrough.dto.MmRichiesteLookThroughDTO;
import it.siae.mslookthrough.entity.MmRichiesteLookThrough;

import java.util.List;

public interface LookThroughService {
    List<MmRichiesteLookThrough> createReportRequest(CreateRequestLookThrough createRequestLookThrough) throws Exception;
    List<MmRichiesteLookThroughDTO> toDtoList(List<MmRichiesteLookThrough> mmRichiesteLookThrough);
}
