/*
 * @Autor A.D.C.
 */

package it.siae.mslookthrough.scheduledTask;

import it.siae.mslookthrough.entity.MmRichiesteLookThrough;
import it.siae.mslookthrough.entity.enumeration.StatoLookThroughEnum;
import it.siae.mslookthrough.report.csv.WriteCsv;
import it.siae.mslookthrough.service.CommonsService;
import lombok.Data;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

import java.io.File;
import java.text.SimpleDateFormat;


@Data
public class LookThroughJob implements Runnable {

    private final Logger logger = LoggerFactory.getLogger(LookThroughJob.class);

    private MmRichiesteLookThrough report;

    private CommonsService commonsService;

    private WriteCsv writeCsv;

    private Environment env;


    public LookThroughJob(MmRichiesteLookThrough report, CommonsService commonsService, Environment env, WriteCsv writeCsv) {
        this.report = report;
        this.commonsService = commonsService;
        this.env = env;
        this.writeCsv = writeCsv;
    }

    @Override
    public void run() {

        try {
            logger.info("STARTING LookThroughService for report {}", report.getIdRichiesta());

            report.setStato(StatoLookThroughEnum.IN_CORSO.getName());
            report.setFileDem(""); //percorso finale del file dem
            report.setFileDrm(""); //percorso finale del file drm
            report.setErrore("");
            commonsService.updateReportStatus(report);

            logger.info("Load data from Hive for report {}", +report.getIdRichiesta());
            String[] tipi = report.getTipoDiritto().split("_");

            for(String tipoDiritto : tipi) {

                // temporary output file
                final File file = new File(report.getIdRipartizioneSiada() + "_" + tipoDiritto + "_"
                        + new SimpleDateFormat("ddMMyyyyHHmmss").format(report.getDataCreazione()) + ".csv");
                logger.info("processCsvFromHive of report Id  " + report.getIdRichiesta() + " : created temporary file {}", file);

                // create csv file from hive query
                logger.info("processCsvFromHive of report Id  " + report.getIdRichiesta() + " : creating CSV file for "+tipoDiritto+" type");
                boolean created = false;
                boolean response = false;
                String path = "C:";
                created = writeCsv.createCsvFile(path, file, report,tipoDiritto);
                logger.info("processCsvFromHive of report Id  " + report.getIdRichiesta() + " : CSV file size {}", file.length());
                if (created) {

                    // upload file to S3
                    if (file.length() > 0L) {
                        logger.info("processCsvFromHive of report Id  " + report.getIdRichiesta() + " : uploading to S3");
                        writeCsv.uploadCsvFile(report, file,tipoDiritto);
                        response = true;
                        // add partition to hive table
//                        logger.info("processCsvFromHive of report Id  "+report.getIdRichiesta()+" : creating hive partition");
//                        writeCsv.addHivePartition(path);
                        logger.info("report Id  " + report.getIdRichiesta() + "COMPLETE");
                    }

                }

                // delete temporary output file
                file.delete();
                logger.info("processCsvFromHive of report Id  " + report.getIdRichiesta() + " : deleted temporary file {}", file);

                logger.info("Update report {} status", report.getIdRichiesta());
            }
        } catch (Exception e) {
            report.setStato(StatoLookThroughEnum.ERRORE.getName());
            report.setErrore(ExceptionUtils.getStackTrace(e));
            report.setFileDem("");
            report.setFileDrm("");
            logger.error("Error occurred!", e);
            commonsService.updateReportStatus(report);
        }

    }

}

