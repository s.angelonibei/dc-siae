/*
 * @Autor A.D.C.
 */

package it.siae.mslookthrough.scheduledTask;

import it.siae.mslookthrough.entity.MmRichiesteLookThrough;
import it.siae.mslookthrough.entity.enumeration.StatoLookThroughEnum;
import it.siae.mslookthrough.report.csv.WriteCsv;
import it.siae.mslookthrough.repository.MmRichiesteLookThroughRepository;
import it.siae.mslookthrough.service.CommonsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.RejectedExecutionException;

@Component
public class ReportEngine {

    private static final Logger logger = LoggerFactory.getLogger(ReportEngine.class);
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
    @Autowired
    MmRichiesteLookThroughRepository LookThroughRepo;

    @Autowired
    private CommonsService commonsService;

    @Autowired
    private Environment env;

    @Autowired
    private WriteCsv writeCsv;

    @Value("${spring.hive.clusterListRestUrlSiae}")
    public String hiveUrl;

    @Autowired
    @Qualifier("CustomTaskExecutor")
    private TaskExecutor taskExecutor;


    // add scheduled methods here
    @Scheduled(fixedRate = 30000)
    public void scheduleTaskWithCronExpression() {

        logger.info("Report Service is running");
        logger.info("Cron Task: Current Time - {}", formatter.format(LocalDateTime.now()));
        List<MmRichiesteLookThrough> reports = LookThroughRepo.findByStatoIgnoreCaseOrderByDataCreazioneAsc(StatoLookThroughEnum.DA_ELABORARE.getName());
        List<MmRichiesteLookThrough> reportsrunning = LookThroughRepo.findByStatoIgnoreCaseOrderByDataCreazioneAsc(StatoLookThroughEnum.IN_CORSO.getName());
        logger.info("Reports da Elaborare: " + reports.size());
        logger.info("Reports in corso: " + reportsrunning.size());
        for (MmRichiesteLookThrough report : reports) {
            LookThroughJob job = new LookThroughJob(report, commonsService, env, writeCsv);
            try {
                taskExecutor.execute(job);
            } catch (RejectedExecutionException r) {
                logger.info("Impossibile prendere in carico il report: tutti gli slot disponibili sono occupati");
            }
        }
        logger.info("Report Engine is running");
    }

}