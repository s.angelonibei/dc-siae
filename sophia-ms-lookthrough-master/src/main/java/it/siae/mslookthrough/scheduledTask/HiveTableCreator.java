/*
 * @Autor A.D.C.
 */

package it.siae.mslookthrough.scheduledTask;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import it.siae.mslookthrough.client.RestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

@Component
//public class HiveTableCreator implements CommandLineRunner {
public class HiveTableCreator {
    private static final Logger logger = LoggerFactory.getLogger(HiveTableCreator.class);

    @Autowired
    private Environment env;

    @Value("${spring.hive.clusterListRestUrlSiae}")
    public String hiveUrl;

    //    FIX TIMEOUT PROD
    // Per la costruzione delle tabelle , partendo ora dagli id ripartizione e droppandole al termine del metodo di write csv ,
    // non eseguiremo più lo script allo start del servizio LookThrough ma ogni volta che prendiamo in carico una richiesta
//    @Override
//    public void run(String... args) throws Exception {
    public String createHiveTable(String idLabel, String idRichiesta, String societa) throws Exception {
        logger.info("Prova di connessione : profilo " + env.getActiveProfiles().toString(), hiveUrl);
        // Register driver and create driver instance
        Class.forName(env.getProperty("hive.jdbc.driver"));
        String htablename = "";
        final RestClient.Result result = RestClient
                .get(hiveUrl, null);
        if (200 == result.getStatusCode()) {
            final JsonObject resultJson = result.getJsonElement().getAsJsonObject();
            logger.info("createTableFile: resultJson {}", resultJson);
            if ("OK".equals(resultJson.get("status").getAsString())) {
                final JsonArray activeEmrClusters = resultJson.getAsJsonArray("activeEmrClusters");
                if (null != activeEmrClusters) for (JsonElement activeEmrCluster : activeEmrClusters) {
                    final JsonObject clusterJson = (JsonObject) activeEmrCluster;
                    logger.info("createTableFile: clusterJson {}", clusterJson);
                    String hiveJdbcUrl = clusterJson.get("hiveJdbcUrl").getAsString();
                    String hiveJdbcUser = clusterJson.get("hiveJdbcUser").getAsString();
                    String hiveJdbcPassword = clusterJson.get("hiveJdbcPassword").getAsString();
                    hiveJdbcUrl = env.getProperty("hive.jdbc.url", hiveJdbcUrl);
                    hiveJdbcUser = env.getProperty("hive.jdbc.user", hiveJdbcUser);
                    hiveJdbcPassword = env.getProperty("hive.jdbc.password", hiveJdbcPassword);
                    logger.info("createTableFile: connecting to {}...", hiveJdbcUrl);
                    DriverManager.setLoginTimeout(0);
                    try (final Connection connection = DriverManager
                            .getConnection(hiveJdbcUrl, hiveJdbcUser, hiveJdbcPassword)) {
                        logger.info("createCsvFile: connected to {}", hiveJdbcUrl);
                        try (final Statement statement = connection.createStatement()) {
                            //Build qry that give me data to export in CSV
                            //Creating a reader object
                            logger.info("createTableFile : read and modify file sql");
                            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
                            InputStream f = classloader.getResourceAsStream("riparto_siada_648_ddl.sql");
                            BufferedReader br = new BufferedReader(new InputStreamReader(f));
                            String line = br.readLine();
                            StringBuilder sb = new StringBuilder();
                            while (line != null) {
                                sb.append(line).append("\n");
                                line = br.readLine();
                            }
                            htablename = env.getProperty("spring.profiles.active") + "_riparto_siada_648_"+ idRichiesta +"_"+ idLabel;
                            String hql = sb.toString().replace(":table", htablename).replace(":location", "'" + env.getProperty("spring.s3uri.location") + "/" + idLabel + "/"+societa.toUpperCase()+"'");
                            logger.info("createTableFile : ready to run the script : " + hql);
                            //Running the script
                            statement.executeUpdate(hql);
                            logger.info("createTableFile : Ok ...");
                        }
                    } catch (Exception e) {
                        logger.error("Error connection to create table ", e);
                    }
                }
            }
        }
        return htablename;
    }
}