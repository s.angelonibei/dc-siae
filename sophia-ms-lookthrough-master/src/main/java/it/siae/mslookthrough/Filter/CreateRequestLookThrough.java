/*
 * @Autor A.D.C.
 */

package it.siae.mslookthrough.Filter;

import lombok.Data;

import java.util.List;

@Data
//Create request LookThrough
public class CreateRequestLookThrough {
    private String idRipartizioneSiada ;
    private List<String> idLabel;
    private List<String> tipoDiritto;
    private String societa;
    private String creatoDa;
}
