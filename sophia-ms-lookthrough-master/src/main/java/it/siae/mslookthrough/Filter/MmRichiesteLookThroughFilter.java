/*
 * @Autor A.D.C.
 */

package it.siae.mslookthrough.Filter;

import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
public class MmRichiesteLookThroughFilter {
    private Long idRichiesta;
    private String posizioneSiae;
    private String listaRipartizione;
    private String stato;
    private BigDecimal valoreNettoDem;
    private BigDecimal valoreNettoDrm;
    private Timestamp dataCreazione;
    private Timestamp ultimoAggiornamento;
    private String richiestaDa;
    private String descrizioneErrore;
    private String pathS3LookThrough;
}
