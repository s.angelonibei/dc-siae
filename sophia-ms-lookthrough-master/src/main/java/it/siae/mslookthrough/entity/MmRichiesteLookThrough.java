package it.siae.mslookthrough.entity;

import lombok.Data;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@Data
@Table(name ="MM_RICHIESTE_LOOKTHROUGH",schema = "MCMDB_debug")
@NamedQueries({@NamedQuery(name = "MmRichiesteLookThrough.all", query = "select x from MmRichiesteLookThrough x order by x.dataCreazione desc")})
@Entity
@XmlRootElement
public class MmRichiesteLookThrough {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(insertable = false, name = "ID_RICHIESTA", nullable = false)
    private Long idRichiesta;

    @Column(name = "ID_RIPARTIZIONE_SIADA")
    private String idRipartizioneSiada;

    @Column(name = "LISTA_ID_LABEL")
    private String idLabel;

    @Column(name = "TIPO_DIRITTO")
    private String tipoDiritto;

    @Column(name = "CODICE_SOCIETA_TUTELA")
    private String societa;

    @Column(name = "STATO")
    private String stato;

    @Column(name = "DATA_CREAZIONE")
    private Date dataCreazione;

    @Column(name = "ULTIMO_AGGIORNAMENTO")
    private Date ultimoAggiornamento;

    @Column(name = "RICHIESTA_DA")
    private String creatoDa;

    @Column(name = "DESCRIZIONE_ERRORE")
    private String errore;

    @Column(name = "PATH_S3_LOOKTHROUGH_DEM")
    private String fileDem;

    @Column(name = "PATH_S3_LOOKTHROUGH_DRM")
    private String fileDrm;

    public String getErrore() {
        return errore;
    }

    public void setErrore(String errore) {
        if(errore.length()>400){
            errore = errore.substring(0,400);
        }
        this.errore = errore;
    }
}
