/*
 * @Autor A.D.C.
 */

package it.siae.mslookthrough.entity.enumeration;

public enum StatoLookThroughEnum {
    DA_ELABORARE("DA ELABORARE"),
    IN_CORSO("IN CORSO"),
    COMPLETATA("COMPLETATA"),
    ERRORE("ERRORE");

    private String name;

    StatoLookThroughEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
