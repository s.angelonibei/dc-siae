/*
 * @Autor A.D.C.
 */

package it.siae.mslookthrough.report.csv;

import it.siae.mslookthrough.entity.MmRichiesteLookThrough;

import java.io.File;

public interface WriteCsv {
    boolean createCsvFile(String path, File file, MmRichiesteLookThrough report,String tipoDiritto) throws Exception;

    boolean uploadCsvFile(MmRichiesteLookThrough report, File file, String tipoDiritto) throws Exception;

}
