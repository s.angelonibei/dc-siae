/*
 * @Autor A.D.C.
 */

package it.siae.mslookthrough.report.csv;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.event.ProgressEvent;
import com.amazonaws.event.ProgressEventType;
import com.amazonaws.event.ProgressListener;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import it.siae.mslookthrough.client.RestClient;
import it.siae.mslookthrough.aws.S3;
import it.siae.mslookthrough.entity.MmRichiesteLookThrough;
import it.siae.mslookthrough.entity.enumeration.StatoLookThroughEnum;
import it.siae.mslookthrough.scheduledTask.HiveTableCreator;
import it.siae.mslookthrough.service.CommonsService;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.QuoteMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
@Transactional
@EnableConfigurationProperties
public class WriteCsvImpl implements WriteCsv {

    private Logger logger = LoggerFactory.getLogger(WriteCsvImpl.class);

    @Autowired
    private Environment env;

    @Autowired
    private CommonsService commonsService;

    //FIX TIMEOUT
    @Autowired
    private HiveTableCreator hiveTableCreator;

    @Value("${hive.drs648.select}")
    public String hiveSelect;

    @Value("${hive.drs648.selectOne}")
    public String hiveSelectOne;

    @Value("${hive.drs648.selectextstart}")
    public String hiveSelectExtStart;

    @Value("${hive.drs648.selectextend}")
    public String hiveSelectExtEnd;

    @Value("${spring.hive.clusterListRestUrlSiae}")
    public String hiveUrl;

    @Value("${spring.s3uri.path}")
    public String path;

    @Value("${s3.retry.put_object}")
    private String put_object;

    @Value("${spring.cloud.aws.credentials.accessKey}")
    private String accessKey;

    @Value("${spring.cloud.aws.credentials.secretKey}")
    private String secretKey;

    @Value("${spring.cloud.aws.region.static}")
    private String region;

    @Value("${hive.engine}")
    private String engine;

    public boolean createCsvFile(String path, File file, MmRichiesteLookThrough report, String tipoDiritto) throws Exception {

        String query = null;
        List<String> htablenames = new ArrayList<>();
        final RestClient.Result result = RestClient
                .get(hiveUrl, report);
        if (200 == result.getStatusCode()) {
            final JsonObject resultJson = result.getJsonElement().getAsJsonObject();
            logger.info("createCsvFile: resultJson {}", resultJson);
            if ("OK".equals(resultJson.get("status").getAsString())) {
                final JsonArray activeEmrClusters = resultJson.getAsJsonArray("activeEmrClusters");
                if (null != activeEmrClusters)
                    for (JsonElement activeEmrCluster : activeEmrClusters) {
                        final JsonObject clusterJson = (JsonObject) activeEmrCluster;
                        logger.info("createCsvFile: clusterJson {}", clusterJson);
                        String hiveJdbcUrl = clusterJson.get("hiveJdbcUrl").getAsString().concat("?hive.execution.engine=" + engine); //FIX TIMEOUT add engine for union select
                        String hiveJdbcUser = clusterJson.get("hiveJdbcUser").getAsString();
                        String hiveJdbcPassword = clusterJson.get("hiveJdbcPassword").getAsString();
                        hiveJdbcUrl = env.getProperty("hive.jdbc.url", hiveJdbcUrl);
                        hiveJdbcUser = env.getProperty("hive.jdbc.user", hiveJdbcUser);
                        hiveJdbcPassword = env.getProperty("hive.jdbc.password", hiveJdbcPassword);
                        logger.info("createCsvFile: connecting to {}...", hiveJdbcUrl);
                        DriverManager.setLoginTimeout(0);
                        try (final Connection connection = DriverManager
                                .getConnection(hiveJdbcUrl, hiveJdbcUser, hiveJdbcPassword)) {
                            logger.info("createCsvFile: connected to {}", hiveJdbcUrl);
                            try (final Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                                    ResultSet.CONCUR_READ_ONLY)) {
                                //Build qry that give me data to export in CSV

                                // FIX TIMEOUT CREATE TABLES
                                String[] listaLabels = report.getIdLabel().split("-");
                                // Dallo split degli id ripartizione costruisco le tabelle hive che puntano ai percorsi degli id selezionati
                                String hql = "";
                                if (listaLabels.length > 0) {
                                    String htablename = null;
                                    for (String label : listaLabels) {
                                        logger.info("Create hive table for ID Ripartizione : " + label);
                                        //Mi salvo i nomi delle tabelle create per ogni id ripartizione per effettuare la DROP TABLE al termine delle operazioni.
                                        htablename = hiveTableCreator.createHiveTable(label, report.getIdRichiesta().toString(), report.getSocieta());
                                        htablenames.add(htablename);
                                    }
                                    logger.info("HIVE TABLES CREATED OK ...");
                                    logger.info("Building hive query");
                                    List<String> hiveSelectslist = new ArrayList<>();

                                    if (htablenames.size() > 1) {
                                        // Con i nomi delle tabelle costruite proseguo alla costruzione delle query eventuali da mettere in UNION ALL
                                        for (String table : htablenames) {
                                            logger.info("table name ==> " + table);
                                            hiveSelectslist.add(hiveSelect.replace(":table", table));
                                        }

                                        for (String select : hiveSelectslist) {
                                            if (hql.equalsIgnoreCase("")) {
                                                hql = select;
                                            } else {
                                                hql = hql.concat(" UNION ALL ").concat(select);
                                            }
                                        }
                                        query = hiveSelectExtStart + hql + hiveSelectExtEnd;
                                    }else {
                                        query = hiveSelectOne.replace(":table",htablenames.get(0));
                                    }
                                }
                                query = query.replace(":idripsiada", report.getIdRipartizioneSiada())
                                        .replace(":tipodiritto", tipoDiritto)
                                        .replace(":compenso", tipoDiritto.equalsIgnoreCase("DEM") ? "compenso_dem_netto" : "compenso_drm_netto");

                                logger.info("query ===> " + query);
                                logger.info("createCsvFile of report Id  " + report.getIdRichiesta() + " : executing query {}", query);
                                try (ResultSet resultSet = statement.executeQuery(query)) {
                                    if (resultSet != null && resultSet.next() == true) {
                                        resultSet.beforeFirst();
                                        saveCsvFile(resultSet, path, file, report, tipoDiritto);
                                    } else {
                                        report.setStato(StatoLookThroughEnum.COMPLETATA.getName());
                                        report.setErrore("Non sono stati trovati record con i filtri impostati.");
                                        report.setFileDem("");
                                        report.setFileDrm("");
                                        commonsService.updateReportStatus(report);
                                        logger.info("Non sono stati trovati record con i filtri impostati. Report Id " + report.getIdRichiesta() + "");
                                    }
                                }
                                return true;
                            }
                        } catch (Exception e) {
                            report.setStato(StatoLookThroughEnum.ERRORE.getName());
                            report.setErrore(e.getMessage());
                            report.setFileDem("");
                            report.setFileDrm("");
                            commonsService.updateReportStatus(report);
                            logger.error("createCsvFile report Id  " + report.getIdRichiesta() + "", e);
                        } finally {
                            // In ogni caso al termine delle operazioni , proseguo alla DROP delle tabelle create
                            for (String t : htablenames) {
                                logger.info("DROP TABLE " + t);
                                final Connection connection = DriverManager
                                        .getConnection(hiveJdbcUrl, hiveJdbcUser, hiveJdbcPassword);
                                final Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                                        ResultSet.CONCUR_READ_ONLY);
                                statement.executeUpdate("DROP TABLE IF EXISTS " + t);
                            }
                        }
                    }
            }
        }
        return false;
    }


    public boolean uploadCsvFile(MmRichiesteLookThrough report, File file, String tipoDiritto) throws Exception {
        final S3.Url s3Uri = new S3.Url(path, report);
        logger.info("uploadCsvFile for report Id " + report.getIdRichiesta() + " s3Uri: {}", s3Uri);
        if (this.upload(s3Uri, file, report)) {
            logger.info("uploadCsvFile for report Id " + report.getIdRichiesta() + " s3Uri {}", s3Uri);
            System.out.println(s3Uri);
            report.setUltimoAggiornamento(new Date());
            if (tipoDiritto.equalsIgnoreCase("DEM")) {
                report.setFileDem(path + file.getName());
            } else if (tipoDiritto.equalsIgnoreCase("DRM")) {
                report.setFileDrm(path + file.getName());
            }
            report.setErrore("");
            report.setStato(StatoLookThroughEnum.COMPLETATA.getName());
            commonsService.updateReportStatus(report);
            return true;
        }
        report.setStato(StatoLookThroughEnum.ERRORE.getName());
        report.setFileDem("");
        report.setFileDrm("");
        report.setErrore("Upload failed!");
        commonsService.updateReportStatus(report);
        return false;
    }


    public boolean upload(S3.Url url, final File file, MmRichiesteLookThrough report) {
        try {
            for (int retries = Integer.parseInt(put_object);
                 retries > 0; retries--) {
                try {
                    logger.info("uploading file {} for " + "report " + report.getIdRichiesta() + " to {}", file.getName(), url);
                    final PutObjectRequest putObjectRequest = new PutObjectRequest(url.bucket,
                            url.key.replace("//", "/"), file);
                    putObjectRequest.setGeneralProgressListener(new ProgressListener() {

                        private long accumulator = 0L;
                        private long total = 0L;
                        private long chunk = Math.max(256 * 1024, file.length() / 10L);

                        @Override
                        public void progressChanged(ProgressEvent event) {
                            if (ProgressEventType.REQUEST_BYTE_TRANSFER_EVENT
                                    .equals(event.getEventType())) {
                                accumulator += event.getBytes();
                                total += event.getBytes();
                                if (accumulator >= chunk) {
                                    accumulator -= chunk;
                                    logger.info("transferred {}Kb/{}Kb of file {}",
                                            total / 1024L, file.length() / 1024L, file.getName());
                                }
                            } else if (ProgressEventType.TRANSFER_COMPLETED_EVENT
                                    .equals(event.getEventType())) {
                                logger.info("transfer of file for report Id " + report.getIdRichiesta() + " completed", file.getName());
                            }
                        }

                    });
                    AWSCredentials credentials = new BasicAWSCredentials(
                            accessKey,
                            secretKey
                    );
                    AmazonS3 s3client = AmazonS3ClientBuilder
                            .standard()
                            .withCredentials(new AWSStaticCredentialsProvider(credentials))
                            .withRegion(region)
                            .build();
                    if (!s3client.doesBucketExist(url.bucket)) {
                        s3client.createBucket(url.bucket);
                    }
                    s3client.putObject(url.bucket, url.key + file.getName(), file);
                    logger.info("file {} successfully uploaded for " + "report " + report.getIdRichiesta() + " to {}", file.getName(), url);
                    return true;
                } catch (Exception e) {
                    report.setStato(StatoLookThroughEnum.ERRORE.getName());
                    report.setErrore(e.getMessage());
                    report.setFileDem("");
                    report.setFileDrm("");
                    commonsService.updateReportStatus(report);
                    logger.error("upload report Id  " + report.getIdRichiesta() + "", e);
                }
            }
        } catch (NumberFormatException | NullPointerException e) {
            report.setStato(StatoLookThroughEnum.ERRORE.getName());
            report.setErrore(e.getMessage());
            report.setFileDem("");
            report.setFileDrm("");
            commonsService.updateReportStatus(report);
            logger.error("upload report Id  " + report.getIdRichiesta() + "", e);
        }
        logger.info("upload: upload failed, report Id  " + report.getIdRichiesta() + "", file.getName());

        return false;
    }

    private void saveCsvFile(ResultSet resultSet, String path, File file, MmRichiesteLookThrough report, String tipoDiritto) throws Exception {
        try (final FileWriter writer = new FileWriter(file, false)) {
            try (final CSVPrinter printer = new CSVPrinter(new BufferedWriter(writer),
                    CSVFormat.EXCEL.withDelimiter(';').withQuoteMode(QuoteMode.MINIMAL).withHeader(
                            "Ipi number",
                            "Importo " + tipoDiritto,
                            "Tipo Di Diritto",
                            "Ripartizione"
                    ))) {
                final int cols = resultSet.getMetaData().getColumnCount();
                logger.info("Numero cols :" + cols);

                final int idxCompenso = Integer.parseInt(Objects.requireNonNull(env.getProperty("hive.column.compenso")));

                logger.info("saveCsvFile of " + "report " + report.getIdRichiesta() + " : output columns number: {}", cols);
                int rows = 0;
                BigDecimal sumCompenso = new BigDecimal(0);
                while (resultSet.next()) {
                    final String[] values = new String[cols];
                    for (int i = 0; i < cols; i++) {
                        values[i] = resultSet.getString(i + 1);
                    }
                    // compensi netti
                    values[idxCompenso] = fixNumber(values[idxCompenso]);

                    // print columns
                    for (int i = 0; i < cols; i++) {
                        printer.print(values[i]);
                    }
                    printer.println();
                    rows++;
                    if (0 == (rows % 1000)) {
                        logger.info("saveCsvFile of " + "report " + report.getIdRichiesta() + " : rows {}", rows);
                    }
                }

            } catch (Exception e) {
                report.setStato(StatoLookThroughEnum.ERRORE.getName());
                report.setErrore(e.getMessage());
                report.setFileDem("");
                report.setFileDrm("");
                commonsService.updateReportStatus(report);
                logger.error("createCsvFile of " + "report Id  " + report.getIdRichiesta() + "", e);
            }
        }
    }

    //Private Methods
    private BigDecimal toBigDecimal(String value) {
        try {
            return (null == value) ? null : new BigDecimal(value.replace(',', '.'));
        } catch (NumberFormatException e) {
            logger.info(String.valueOf(e));
            return BigDecimal.ZERO;
        }
    }

    private String fixNumber(String value) {
        try {
            return fixNumber(toBigDecimal(value));
        } catch (NumberFormatException e) {
        }
        return value;
    }

    private String fixNumber(BigDecimal number) {
        if (null == number) {
            return null;
        }
        String value = number.toPlainString();
        value = value.replace('.', ',');
        int idx = value.indexOf(",");
        if (-1 != idx) {
            while (value.endsWith("0")) {
                value = value.substring(0, value.length() - 1);
            }
        }
        if (value.endsWith(",")) {
            value = value.substring(0, value.length() - 1);
        }
        idx = value.indexOf(",0000000000");
        if (-1 != idx) {
            value = value.substring(0, idx);
        }
        return value;
    }
}
