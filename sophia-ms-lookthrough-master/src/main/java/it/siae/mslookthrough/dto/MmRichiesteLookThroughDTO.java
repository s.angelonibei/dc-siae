package it.siae.mslookthrough.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class MmRichiesteLookThroughDTO implements Serializable {
    private Long idRichiesta;
    private String idRipartizioneSiada;
    private String idLabel;
    private String tipoDiritto;
    private String societa;
    private String stato;
    private Date dataCreazione;
    private Date ultimoAggiornamento;
    private String creatoDa;
    private String errore;
    private String fileDem;
    private String fileDrm;
}
