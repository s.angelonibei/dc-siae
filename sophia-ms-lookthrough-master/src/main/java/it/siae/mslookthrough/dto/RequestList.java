/*
 * @Autor A.D.C.
 */

/*
 * @Autor A.D.C.
 */

package it.siae.mslookthrough.dto;

import it.siae.mslookthrough.entity.MmRichiesteLookThrough;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class RequestList implements Serializable {

    private List<MmRichiesteLookThrough> requestsList;
}
