/*
 * @Autor A.D.C.
 */

package it.siae.mslookthrough.repository;

import it.siae.mslookthrough.entity.MmRichiesteLookThrough;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MmRichiesteLookThroughRepository extends JpaRepository<MmRichiesteLookThrough, Long> {
    List<MmRichiesteLookThrough> findByIdRipartizioneSiadaAndIdLabelAndStato(String idRipartizioneSiada, String listaIdLabel, String stato);

    List<MmRichiesteLookThrough> findByStatoIgnoreCaseOrderByDataCreazioneAsc(String daElaborare);

    List<MmRichiesteLookThrough> findByIdRipartizioneSiadaAndIdLabelAndTipoDirittoAndStatoNot(String idRipartizioneSiada, String listaIdLabel,String tipoDiritto,String stato);
}