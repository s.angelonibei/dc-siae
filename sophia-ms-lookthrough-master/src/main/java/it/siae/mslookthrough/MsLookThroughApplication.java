/*
 * @Autor A.D.C.
 */

package it.siae.mslookthrough;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan
@SpringBootApplication
@EnableAutoConfiguration
@EnableEurekaClient
public class MsLookThroughApplication {

    public static void main(String[] args)  {

        SpringApplication.run(MsLookThroughApplication.class, args);
    }

}
