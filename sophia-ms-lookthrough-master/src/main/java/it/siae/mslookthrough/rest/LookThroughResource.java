/*
 * @Autor A.D.C.
 */

package it.siae.mslookthrough.rest;

import com.google.gson.Gson;
import it.siae.mslookthrough.Filter.CreateRequestLookThrough;
import it.siae.mslookthrough.dto.MmRichiesteLookThroughDTO;
import it.siae.mslookthrough.entity.MmRichiesteLookThrough;
import it.siae.mslookthrough.repository.MmRichiesteLookThroughRepository;
import it.siae.mslookthrough.service.LookThroughService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Description;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.QueryParam;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("lookthrough")
public class LookThroughResource {

    private final Logger logger = LoggerFactory.getLogger(LookThroughResource.class);
//    private final Gson gson;

    @Autowired
    LookThroughService service;

    @Autowired
    MmRichiesteLookThroughRepository LookThroughRepo;

    public LookThroughResource() {

    }

    @PostMapping("/createRequest")
    @Description("Api for create report csv request in MMRichiesteLookThrough")
    public List<MmRichiesteLookThrough> createReportRequest(@Valid @RequestBody CreateRequestLookThrough createRequestLookThrough) throws Exception {
        logger.info("REST request to create new request into MmRichiesteLookThrough: {} ", createRequestLookThrough);
        return this.service.createReportRequest(createRequestLookThrough);
    }

    @GetMapping("/getList")
    @Description("Api for get all request from MMRichiesteLookThrough orderBy DataCreation Desc")
    public Object getAllRequest(@DefaultValue("0") @QueryParam("first") int first,
                                @DefaultValue("50") @QueryParam("last") int last) {
        logger.info("REST request to find all request into MmRichiesteLookThrough: {} ");
        final PagedResult pagedResult = new PagedResult();
        try {
            List<MmRichiesteLookThroughDTO> resultList = this.service.toDtoList(this.LookThroughRepo.findAll()
                    .stream()
                    .sorted(Comparator.comparing(MmRichiesteLookThrough::getDataCreazione)
                            .reversed())
                    .skip(first) //offset
                    .limit(last-first)
                    .collect(Collectors.toList()));

            if (null != resultList) {
                final int maxrows = last - first;
                final boolean hasNext = resultList.size() > maxrows;
                pagedResult.setRows(resultList)
                        .setMaxrows(maxrows)
                        .setFirst(first)
                        .setLast(hasNext ? last : first + resultList.size())
                        .setHasNext(hasNext)
                        .setHasPrev(first > 0);
            } else {
                pagedResult.setMaxrows(0).setFirst(0).setLast(0).setHasNext(false).setHasPrev(false);
            }
        } catch (Exception e) {
            logger.error("all", e);
        }
        return pagedResult;
    }

}
