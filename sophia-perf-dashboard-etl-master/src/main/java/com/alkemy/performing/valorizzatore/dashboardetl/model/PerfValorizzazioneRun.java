package com.alkemy.performing.valorizzatore.dashboardetl.model;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Data
@Entity
@Table(name = "PERF_VALORIZZAZIONE_RUN")
public class PerfValorizzazioneRun {
    @Id
    @Column(name = "ID_VALORIZZAZIONE_RUN")
    private Long idValorizzazioneRun;
    @Column(name = "DATA_INIZIO")
    private Timestamp dataInizio;
    @Column(name = "DATA_FINE")
    private Timestamp dataFine;
    @Column(name = "PERIODO_RIPARTIZIONE")
    private Long periodoRipartizione;
    @Column(name = "INFO_RUN")
    private String infoRun;
    @OneToMany(mappedBy = "perfValorizzazioneRunByIdValorizzazioneRun")
    private Collection<PerfMonitoraggioCodificato> perfMonitoraggioCodificatoesByIdValorizzazioneRun;

}
