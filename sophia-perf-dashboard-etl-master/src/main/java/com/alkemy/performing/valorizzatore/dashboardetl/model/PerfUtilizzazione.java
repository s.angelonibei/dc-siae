package com.alkemy.performing.valorizzatore.dashboardetl.model;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

@Data
@Entity
@Table(name = "PERF_UTILIZZAZIONE")
public class PerfUtilizzazione {
    @Id
    @Column(name = "ID_UTILIZZAZIONE")
    private Long idUtilizzazione;
    @Column(name = "ID_PROGRAMMA_MUSICALE")
    private Long idProgrammaMusicale;
    @Column(name = "CODICE_OPERA")
    private String codiceOpera;
    @Column(name = "TITOLO_COMPOSIZIONE")
    private String titoloComposizione;
    @Column(name = "COMPOSITORE")
    private String compositore;
    @Column(name = "NUMERO_PROGRAMMA_MUSICALE")
    private Long numeroProgrammaMusicale;
    @Column(name = "DURATA")
    private Long durata;
    @Column(name = "DURATA_INFERIORE_30SEC")
    private String durataInferiore30Sec;
    @Column(name = "AUTORI")
    private String autori;
    @Column(name = "INTERPRETI")
    private String interpreti;
    @Column(name = "FLAG_CODIFICA")
    private String flagCodifica;
    @Column(name = "COMBINAZIONE_CODIFICA")
    private String combinazioneCodifica;
    @Column(name = "FLAG_PUBBLICO_DOMINIO")
    private String flagPubblicoDominio;
    @Column(name = "FLAG_MAG_NON_CONCESSA")
    private String flagMagNonConcessa;
    @Column(name = "TIPO_ACCERTAMENTO")
    private String tipoAccertamento;
    @Column(name = "NETTO_MATURATO")
    private BigDecimal nettoMaturato;
    @Column(name = "DATA_PRIMA_CODIFICA")
    private Timestamp dataPrimaCodifica;
    @Column(name = "DATA_INS")
    private Timestamp dataIns;
    @Column(name = "PUNTI_ORIGINALI")
    private BigDecimal puntiOriginali;
    @Column(name = "PUNTI_RICALCOLATI")
    private BigDecimal puntiRicalcolati;
    @Column(name = "FLAG_CEDOLA_ESCLUSA")
    private String flagCedolaEsclusa;

    @Column(name = "ID_CODIFICA")
    private Long idCodifica;

}
