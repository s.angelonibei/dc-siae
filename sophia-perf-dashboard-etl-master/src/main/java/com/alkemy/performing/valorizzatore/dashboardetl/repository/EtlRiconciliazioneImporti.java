package com.alkemy.performing.valorizzatore.dashboardetl.repository;


import com.alkemy.performing.valorizzatore.dashboardetl.jooq.routines.RefreshPerfRiconcMvNow;
import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;

@Repository
@RequiredArgsConstructor
@CommonsLog
public class EtlRiconciliazioneImporti {

    private final DataSource datasource;

    public void callRefreshPerfRiconcMvNowProcedure(){
        DSLContext create = DSL.using(datasource, SQLDialect.MYSQL);

        RefreshPerfRiconcMvNow refreshPerfRiconcMvNow = new RefreshPerfRiconcMvNow();
        refreshPerfRiconcMvNow.execute(create.configuration());
    }
}
