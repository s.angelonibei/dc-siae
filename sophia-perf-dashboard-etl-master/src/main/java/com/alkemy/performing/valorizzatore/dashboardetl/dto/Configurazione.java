package com.alkemy.performing.valorizzatore.dashboardetl.dto;


import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Configurazione {
    private Long valoreSoglia;
    List<Valori> valoreEconomicoList = new ArrayList<>();
    List<Valori> livelloSomiglianzaList = new ArrayList<>();
}
