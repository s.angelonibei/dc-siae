package com.alkemy.performing.valorizzatore.dashboardetl.dto;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Valori {

    private Long id;
    private String header;
    private Long inferiore;
    private Long superiore;
}
