package com.alkemy.performing.valorizzatore.dashboardetl.dto;


import lombok.Data;

import java.util.Date;

@Data
public class KpiDto {
    private Long idLancio;
    private Long periodoRipartizioneId;
    private String codiceVoceIncasso;
    private Long numeroCombana;
    private Long numeroUtilizzazioni;
    private Long nrUtilCodificaAutomatica;
    private Long nrUtilCodificaManuale;
    private Long nrUtilDaCodificareManualmente;
    private Long nrUtilCodificatoDaGirare;
    private Double valoreEconomicoCodificato;
    private Double valoreEconomicoNonCodificato;
    private Date competenzeDa;
    private Date competenzeA;


}


/*
subQuery.field("PERIODO",Long.class),
subQuery.field(PERF_MOVIMENTO_CONTABILE.CODICE_VOCE_INCASSO),
countDistinct(subQuery.field(PERF_UTILIZZAZIONE.ID_COMBANA)),
count(subQuery.field(PERF_UTILIZZAZIONE.ID_UTILIZZAZIONE)),
sum(subQuery.field("NR_UTIL_CODIFICA_AUTOMATICA",Long.class)),
sum(subQuery.field("NR_UTIL_CODIFICA_MANUALE",Long.class)),
sum(subQuery.field("NR_UTIL_DA_CODIFICARE_MANUALMENTE",Long.class)),
sum(subQuery.field("NR_UTIL_CODIFICATORE_DA_GIRARE",Long.class)),
sum(subQuery.field("IMPORTO_A",Long.class)).add(sum(subQuery.field("IMPORTO_M",Long.class))),
sum(subQuery.field("IMPORTO_C",Long.class)).add(sum(subQuery.field("IMPORTO_D",Long.class)))
 */