package com.alkemy.performing.valorizzatore.dashboardetl.model;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Data
@Entity
@Table(name = "PERF_MONITORAGGIO_CODIFICATO")
public class PerfMonitoraggioCodificato {
    @Id
    @Column(name = "ID_MONITORAGGIO_CODIFICATO")
    private Integer idMonitoraggioCodificato;

    @Column(name = "VALORI")
    private String valori;

    @Column(name = "DATA_CREAZIONE")
    private Timestamp dataCreazione;

    @ManyToOne
    @JoinColumn(name = "ID_VALORIZZAZIONE_RUN", referencedColumnName = "ID_VALORIZZAZIONE_RUN", nullable = false)
    private PerfValorizzazioneRun perfValorizzazioneRunByIdValorizzazioneRun;

    @ManyToOne
    @JoinColumn(name = "ID_MONITORAGGIO_CODIFICATO_CONF", referencedColumnName = "ID_MONITORAGGIO_CODIFICATO_CONF", nullable = false)
    private PerfMonitoraggioCodificatoConf perfMonitoraggioCodificatoConfByIdMonitoraggioCodificatoConf;

}
