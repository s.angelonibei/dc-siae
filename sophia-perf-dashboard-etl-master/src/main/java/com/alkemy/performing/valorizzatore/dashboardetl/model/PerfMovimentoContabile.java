package com.alkemy.performing.valorizzatore.dashboardetl.model;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

@Data
@Entity
@Table(name = "PERF_MOVIMENTO_CONTABILE")
public class PerfMovimentoContabile {
    @Id
    @Column(name = "ID_MOVIMENTO_CONTABILE")
    private Long idMovimentoContabile;
    @Column(name = "ID_EVENTO")
    private Long idEvento;

    @Column(name = "TIPOLOGIA_MOVIMENTO")
    private String tipologiaMovimento;
    @Column(name = "IMPORTO_MANUALE")
    private BigDecimal importoManuale;
    @Column(name = "PM_RIPARTITO_SEMESTRE_PREC")
    private String pmRipartitoSemestrePrec;
    @Column(name = "DATA_ORA_ULTIMA_MODIFICA")
    private Timestamp dataOraUltimaModifica;
    @Column(name = "CODICE_VOCE_INCASSO")
    private String codiceVoceIncasso;
    @Column(name = "NUMERO_PM")
    private String numeroPm;
    @Column(name = "NUMERO_PERMESSO")
    private String numeroPermesso;
    @Column(name = "TOTALE_DEM_LORDO_PM")
    private BigDecimal totaleDemLordoPm;
    @Column(name = "NUMERO_REVERSALE")
    private Long numeroReversale;
    @Column(name = "FLAG_SOSPESO")
    private String flagSospeso;
    @Column(name = "FLAG_NO_MAGGIORAZIONE")
    private String flagNoMaggiorazione;
    @Column(name = "FLAG_GRUPPO_PRINCIPALE")
    private String flagGruppoPrincipale;
    @Column(name = "NUMERO_FATTURA")
    private Long numeroFattura;
    @Column(name = "IMPORTO_DEM_TOTALE")
    private BigDecimal importoDemTotale;
    @Column(name = "NUMERO_PM_PREVISTI")
    private Long numeroPmPrevisti;
    @Column(name = "NUMERO_PM_PREVISTI_SPALLA")
    private Long numeroPmPrevistiSpalla;
    @Column(name = "TIPO_DOCUMENTO_CONTABILE")
    private String tipoDocumentoContabile;
    @Column(name = "UTENTE_ULTIMA_MODIFICA")
    private String utenteUltimaModifica;
    @Column(name = "DATA_INS")
    private Timestamp dataIns;
    @Column(name = "CONTABILITA")
    private Long contabilita;
    @Column(name = "DATA_REVERSALE")
    private Timestamp dataReversale;
    @Column(name = "DATA_RIENTRO")
    private Timestamp dataRientro;
    @Column(name = "CONTABILITA_ORIG")
    private Long contabilitaOrig;
    @Column(name = "CODICE_RAGGRUPPAMENTO")
    private String codiceRaggruppamento;
    @Column(name = "SEDE")
    private String sede;
    @Column(name = "AGENZIA")
    private String agenzia;
    @Column(name = "PUNTI_PROGRAMMA_MUSICALE")
    private BigDecimal puntiProgrammaMusicale;
    @Column(name = "IMPORTO_VALORIZZATO")
    private BigDecimal importoValorizzato;
    @Column(name = "ID_PERIODO_RIPARTIZIONE")
    private Long idPeriodoRipartizione;

    @Column(name = "ID_PROGRAMMA_MUSICALE")
    private Long idProgrammaMusicale;

}
