package com.alkemy.performing.valorizzatore.dashboardetl.model;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Data
@Entity
@Table(name = "PERF_CODIFICA")
public class PerfCodifica {
    @Id
    @Column(name = "ID_CODIFICA")
    private Long idCodifica;

    @Column(name = "ID_COMBANA")
    private Long idCombana;

    @Column(name = "CODICE_OPERA_SUGGERITO")
    private String codiceOperaSuggerito;

    @Column(name = "DATA_CODIFICA")
    private Timestamp dataCodifica;

    @Column(name = "DIST_SEC_CANDIDATA")
    private BigDecimal distSecCandidata;

    @Column(name = "CONFIDENZA")
    private BigDecimal confidenza;

    @Column(name = "VALORE_ECONOMICO")
    private BigDecimal valoreEconomico;

    @Column(name = "CANDIDATE")
    private String candidate;

    @Column(name = "TIPO_APPROVAZIONE")
    private String tipoApprovazione;

    @Column(name = "DATA_APPROVAZIONE")
    private Timestamp dataApprovazione;

    @Column(name = "STATO_APPROVAZIONE")
    private String statoApprovazione;

    @Column(name = "CODICE_OPERA_APPROVATO")
    private String codiceOperaApprovato;

    @Column(name = "DATA_FINE_VALIDITA")
    private Timestamp dataFineValidita;

    @Column(name = "N_UTILIZZAZIONI")
    private Long nUtilizzazioni;

    @Column(name = "PREZIOSA")
    private Boolean preziosa;

}
