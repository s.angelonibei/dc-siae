package com.alkemy.performing.valorizzatore.dashboardetl.repository;

import com.alkemy.performing.valorizzatore.dashboardetl.dto.KpiDto;
import com.alkemy.performing.valorizzatore.dashboardetl.jooq.enums.PerfCodificaTipoApprovazione;
import com.alkemy.performing.valorizzatore.dashboardetl.jooq.tables.records.PerfMonitoraggioKpiRecord;
import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import org.jooq.*;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import static com.alkemy.performing.valorizzatore.dashboardetl.jooq.Tables.*;
import static org.jooq.impl.DSL.*;

@Repository
@RequiredArgsConstructor
@CommonsLog
public class EtlKpi {

    private final DataSource datasource;

    public void generaKpi() {
        log.info("Inizio aggiornamento dati KPI");
        long idLancio = new Date().getTime();
        DSLContext create = DSL.using(datasource, SQLDialect.MYSQL);

        SelectOnConditionStep<Record14<Long, java.sql.Date, java.sql.Date, String, Long, Serializable, Serializable, Serializable, Serializable, Long, Integer, Integer, Integer, Integer>> subQuery = create.select(
                PERIODO_RIPARTIZIONE.ID_PERIODO_RIPARTIZIONE.as("PERIODO"),
                PERIODO_RIPARTIZIONE.COMPETENZE_DA.as("COMPETENZE_DA"),
                PERIODO_RIPARTIZIONE.COMPETENZE_A.as("COMPETENZE_A"),
                PERF_MOVIMENTO_CONTABILE.CODICE_VOCE_INCASSO,
                PERF_UTILIZZAZIONE.ID_UTILIZZAZIONE,
                when(PERF_CODIFICA.TIPO_APPROVAZIONE.eq(PerfCodificaTipoApprovazione.M).and(PERF_CODIFICA.ID_CODIFICA.isNotNull()),
                        coalesce(PERF_UTILIZZAZIONE.PUNTI_RICALCOLATI.multiply(PERF_MOVIMENTO_CONTABILE.IMPORTO_VALORIZZATO
                                .divide(PERF_MOVIMENTO_CONTABILE.PUNTI_PROGRAMMA_MUSICALE)), 0))
                        .otherwise(0).as("IMPORTO_M"),
                when(PERF_CODIFICA.TIPO_APPROVAZIONE.eq(PerfCodificaTipoApprovazione.A),
                        coalesce(PERF_UTILIZZAZIONE.PUNTI_RICALCOLATI.multiply(PERF_MOVIMENTO_CONTABILE.IMPORTO_VALORIZZATO
                                .divide(PERF_MOVIMENTO_CONTABILE.PUNTI_PROGRAMMA_MUSICALE)), 0))
                        .otherwise(0).as("IMPORTO_A"),
                when(PERF_CODIFICA.TIPO_APPROVAZIONE.isNull().and(PERF_CODIFICA.ID_CODIFICA.isNull()),
                        coalesce(PERF_UTILIZZAZIONE.PUNTI_RICALCOLATI.multiply(PERF_MOVIMENTO_CONTABILE.IMPORTO_VALORIZZATO
                                .divide(PERF_MOVIMENTO_CONTABILE.PUNTI_PROGRAMMA_MUSICALE)), 0))
                        .otherwise(0).as("IMPORTO_C"),
                when(PERF_CODIFICA.TIPO_APPROVAZIONE.eq(PerfCodificaTipoApprovazione.M).and(PERF_CODIFICA.ID_CODIFICA.isNotNull()
                                .and(PERF_CODIFICA.DATA_APPROVAZIONE.isNull())),
                        coalesce(PERF_UTILIZZAZIONE.PUNTI_RICALCOLATI.multiply(PERF_MOVIMENTO_CONTABILE.IMPORTO_VALORIZZATO
                                .divide(PERF_MOVIMENTO_CONTABILE.PUNTI_PROGRAMMA_MUSICALE)), 0))
                        .otherwise(0).as("IMPORTO_D"),
                PERF_UTILIZZAZIONE.ID_COMBANA,
                when(PERF_CODIFICA.TIPO_APPROVAZIONE.eq(PerfCodificaTipoApprovazione.A), 1)
                        .otherwise(0).as("NR_UTIL_CODIFICA_AUTOMATICA"),
                when(PERF_CODIFICA.TIPO_APPROVAZIONE.eq(PerfCodificaTipoApprovazione.M).and(PERF_CODIFICA.ID_CODIFICA.isNotNull()
                        .and(PERF_CODIFICA.DATA_APPROVAZIONE.isNotNull())), 1)
                        .otherwise(0).as("NR_UTIL_CODIFICA_MANUALE"),
                when(PERF_CODIFICA.TIPO_APPROVAZIONE.eq(PerfCodificaTipoApprovazione.M).and(PERF_CODIFICA.ID_CODIFICA.isNotNull()
                        .and(PERF_CODIFICA.DATA_APPROVAZIONE.isNull())), 1)
                        .otherwise(0).as("NR_UTIL_DA_CODIFICARE_MANUALMENTE"),
                when(PERF_CODIFICA.TIPO_APPROVAZIONE.isNull().and(PERF_CODIFICA.ID_CODIFICA.isNull()), 1)
                        .otherwise(0).as("NR_UTIL_CODIFICATORE_DA_GIRARE")
        )
                .from(PERF_UTILIZZAZIONE)
                .join(PERF_MOVIMENTO_CONTABILE).on(PERF_UTILIZZAZIONE.ID_PROGRAMMA_MUSICALE.eq(PERF_MOVIMENTO_CONTABILE.ID_PROGRAMMA_MUSICALE))
                .join(PERF_COMBANA).on(PERF_UTILIZZAZIONE.ID_COMBANA.eq(PERF_COMBANA.ID_COMBANA))
                .join(PERF_CODIFICA).on(PERF_UTILIZZAZIONE.ID_COMBANA.eq(PERF_CODIFICA.ID_COMBANA))
                .join(PERF_PROGRAMMA_MUSICALE).on(PERF_UTILIZZAZIONE.ID_PROGRAMMA_MUSICALE.eq(PERF_PROGRAMMA_MUSICALE.ID_PROGRAMMA_MUSICALE))
                .join(PERF_EVENTI_PAGATI).on(PERF_MOVIMENTO_CONTABILE.CODICE_VOCE_INCASSO.eq(PERF_EVENTI_PAGATI.VOCE_INCASSO)
                        .and(PERF_MOVIMENTO_CONTABILE.ID_EVENTO.eq(PERF_EVENTI_PAGATI.ID_EVENTO)))
                .leftJoin(PERIODO_RIPARTIZIONE).on(PERF_EVENTI_PAGATI.ID_PERIODO_RIPARTIZIONE.eq(PERIODO_RIPARTIZIONE.ID_PERIODO_RIPARTIZIONE));


        List<KpiDto> result = create.select(subQuery.field("PERIODO", Long.class).as("PERIODO_RIPARTIZIONE_ID"),
                subQuery.field("COMPETENZE_DA",java.sql.Date.class).as("COMPETENZE_DA"),
                subQuery.field("COMPETENZE_A",java.sql.Date.class).as("COMPETENZE_A"),
                subQuery.field(PERF_MOVIMENTO_CONTABILE.CODICE_VOCE_INCASSO).as("CODICE_VOCE_INCASSO"),
                countDistinct(subQuery.field(PERF_UTILIZZAZIONE.ID_COMBANA)).as("NUMERO_COMBANA"),
                count(subQuery.field(PERF_UTILIZZAZIONE.ID_UTILIZZAZIONE)).as("NUMERO_UTILIZZAZIONI"),
                sum(subQuery.field("NR_UTIL_CODIFICA_AUTOMATICA", Long.class)).as("NR_UTIL_CODIFICA_AUTOMATICA"),
                sum(subQuery.field("NR_UTIL_CODIFICA_MANUALE", Long.class)).as("NR_UTIL_CODIFICA_MANUALE"),
                sum(subQuery.field("NR_UTIL_DA_CODIFICARE_MANUALMENTE", Long.class)).as("NR_UTIL_DA_CODIFICARE_MANUALMENTE"),
                sum(subQuery.field("NR_UTIL_CODIFICATORE_DA_GIRARE", Long.class)).as("NR_UTIL_CODIFICATO_DA_GIRARE"),
                sum(subQuery.field("IMPORTO_A", Long.class)).add(sum(subQuery.field("IMPORTO_M", Long.class))).as("VALORE_ECONOMICO_CODIFICATO"),
                sum(subQuery.field("IMPORTO_C", Long.class)).add(sum(subQuery.field("IMPORTO_D", Long.class))).as("VALORE_ECONOMICO_NON_CODIFICATO")
        )
            .from(subQuery)
            .groupBy(subQuery.field(PERIODO_RIPARTIZIONE.ID_PERIODO_RIPARTIZIONE), subQuery.field(PERF_MOVIMENTO_CONTABILE.CODICE_VOCE_INCASSO)).fetch().into(KpiDto.class);




        result.forEach(dto -> {
            PerfMonitoraggioKpiRecord kpi = create.newRecord(PERF_MONITORAGGIO_KPI, dto);
            kpi.setIdLancio(idLancio);
            kpi.store();

        });

        log.info("Fine caricamento KPI record: "+ ((result != null)? result.size() : 0) + " idLancio: "+ idLancio);


    }

    /*
    SELECT
  ID_PERIODO_RIPARTIZIONE,
  CODICE_VOCE_INCASSO,
  count(DISTINCT ID_COMBANA)             NUMERO_COMBANA,
  count(ID_UTILIZZAZIONE)                NUMERO_UTILIZZAZIONI,
  sum(NR_UTIL_CODIFICA_AUTOMATICA)       NR_UTIL_CODIFICA_AUTOMATICA,
  sum(NR_UTIL_CODIFICA_MANUALE)          NR_UTIL_CODIFICA_MANUALE,
  sum(NR_UTIL_DA_CODIFICARE_MANUALMENTE) NR_UTIL_DA_CODIFICARE_MANUALMENTE,
  sum(NR_UTIL_CODIFICATORE_DA_GIRARE)    NR_UTIL_CODIFICATORE_DA_GIRARE,
  SUM(IMPORTO_A) + SUM(IMPORTO_M)        VALORE_ECONOMICO_CODIFICATO,
  SUM(IMPORTO_C) + SUM(IMPORTO_D)        VALORE_ECONOMICO_NON_CODIFICATO
FROM
  (
    SELECT
      COALESCE(PERIODO_RIPARTIZIONE.ID_PERIODO_RIPARTIZIONE, 0) ID_PERIODO_RIPARTIZIONE,
      CODICE_VOCE_INCASSO,
      ID_UTILIZZAZIONE,
      CASE WHEN TIPO_APPROVAZIONE = 'M'  AND PERF_CODIFICA.ID_CODIFICA IS NOT NULL
        THEN COALESCE(PERF_UTILIZZAZIONE.PUNTI_RICALCOLATI * (PERF_MOVIMENTO_CONTABILE.IMPORTO_VALORIZZATO /
                                                              PERF_MOVIMENTO_CONTABILE.PUNTI_PROGRAMMA_MUSICALE), 0)
      ELSE 0 END                                                IMPORTO_M,
      CASE WHEN TIPO_APPROVAZIONE = 'A'
        THEN COALESCE(PERF_UTILIZZAZIONE.PUNTI_RICALCOLATI * (PERF_MOVIMENTO_CONTABILE.IMPORTO_VALORIZZATO /
                                                              PERF_MOVIMENTO_CONTABILE.PUNTI_PROGRAMMA_MUSICALE), 0)
      ELSE 0 END                                                IMPORTO_A,

      CASE WHEN TIPO_APPROVAZIONE IS NULL AND PERF_CODIFICA.ID_CODIFICA IS NULL
        THEN COALESCE(PERF_UTILIZZAZIONE.PUNTI_RICALCOLATI * (PERF_MOVIMENTO_CONTABILE.IMPORTO_VALORIZZATO /PERF_MOVIMENTO_CONTABILE.PUNTI_PROGRAMMA_MUSICALE), 0)
      ELSE 0 END IMPORTO_C,
      CASE WHEN TIPO_APPROVAZIONE = 'M' AND PERF_CODIFICA.ID_CODIFICA IS NOT NULL AND PERF_CODIFICA.DATA_APPROVAZIONE IS NULL
        THEN COALESCE(PERF_UTILIZZAZIONE.PUNTI_RICALCOLATI * (PERF_MOVIMENTO_CONTABILE.IMPORTO_VALORIZZATO /PERF_MOVIMENTO_CONTABILE.PUNTI_PROGRAMMA_MUSICALE), 0)
      ELSE 0 END IMPORTO_D,
      #1 a
      PERF_UTILIZZAZIONE.ID_COMBANA,
      #2 b
      CASE WHEN TIPO_APPROVAZIONE = 'A'
        THEN 1
      ELSE 0 END                                                NR_UTIL_CODIFICA_AUTOMATICA,
      #3 c
      CASE WHEN TIPO_APPROVAZIONE = 'M' AND PERF_CODIFICA.ID_CODIFICA IS NOT NULL AND PERF_CODIFICA.DATA_APPROVAZIONE IS not NULL
        THEN 1
      ELSE 0 END                                                NR_UTIL_CODIFICA_MANUALE,
      #4 d
      CASE WHEN
        TIPO_APPROVAZIONE = 'M' AND PERF_CODIFICA.ID_CODIFICA IS NOT NULL AND PERF_CODIFICA.DATA_APPROVAZIONE IS NULL
        THEN 1
      ELSE 0 END                                                NR_UTIL_DA_CODIFICARE_MANUALMENTE,
      #5 e
      CASE WHEN TIPO_APPROVAZIONE IS NULL AND PERF_CODIFICA.ID_CODIFICA IS NULL
        THEN 1
      ELSE 0 END                                                NR_UTIL_CODIFICATORE_DA_GIRARE
    FROM
      PERF_UTILIZZAZIONE
      JOIN PERF_MOVIMENTO_CONTABILE
        ON PERF_UTILIZZAZIONE.ID_PROGRAMMA_MUSICALE = PERF_MOVIMENTO_CONTABILE.ID_PROGRAMMA_MUSICALE
      JOIN PERF_COMBANA ON PERF_UTILIZZAZIONE.ID_COMBANA = PERF_COMBANA.ID_COMBANA
      JOIN PERF_CODIFICA ON PERF_UTILIZZAZIONE.ID_COMBANA = PERF_CODIFICA.ID_COMBANA
      JOIN PERF_PROGRAMMA_MUSICALE pm ON PERF_UTILIZZAZIONE.ID_PROGRAMMA_MUSICALE = pm.ID_PROGRAMMA_MUSICALE
      JOIN PERF_EVENTI_PAGATI ep ON (PERF_MOVIMENTO_CONTABILE.CODICE_VOCE_INCASSO = ep.VOCE_INCASSO AND
                                     PERF_MOVIMENTO_CONTABILE.ID_EVENTO = ep.ID_EVENTO)
      LEFT JOIN PERIODO_RIPARTIZIONE ON ep.ID_PERIODO_RIPARTIZIONE = PERIODO_RIPARTIZIONE.ID_PERIODO_RIPARTIZIONE
  ) A
WHERE CODICE_VOCE_INCASSO = 2244
GROUP BY ID_PERIODO_RIPARTIZIONE,
  CODICE_VOCE_INCASSO;
     */

}