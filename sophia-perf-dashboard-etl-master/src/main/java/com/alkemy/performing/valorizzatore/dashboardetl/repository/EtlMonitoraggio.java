package com.alkemy.performing.valorizzatore.dashboardetl.repository;

import com.alkemy.performing.valorizzatore.dashboardetl.dto.Configurazione;
import com.alkemy.performing.valorizzatore.dashboardetl.jooq.tables.records.PerfMonitoraggioCodificatoRecord;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import org.jooq.*;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static com.alkemy.performing.valorizzatore.dashboardetl.jooq.Tables.*;
import static org.jooq.impl.DSL.*;

@Repository
@RequiredArgsConstructor
@CommonsLog
public class EtlMonitoraggio {

    private final DataSource datasource;

    public void generaDatiMonitorCodificato(boolean generaNonRipartito) {

        log.info("Inizio aggiornamento dati monitor codificato");

        DSLContext create = DSL.using(datasource, SQLDialect.MYSQL);

        //Prendo l'ultima PERF_VALORIZZAZIONE
        //Prendo l'ultima PERF_MONITORAGGIO_CONF

        Record2<Long, Long> valorizzazioneRecord = create.select(PERF_VALORIZZAZIONE_RUN.ID_VALORIZZAZIONE_RUN, PERF_VALORIZZAZIONE_RUN.PERIODO_RIPARTIZIONE)
                .from(PERF_VALORIZZAZIONE_RUN)
                .where(PERF_VALORIZZAZIONE_RUN.INFO_RUN.like("%\"result\": \"OK\"%"))
                .orderBy(PERF_VALORIZZAZIONE_RUN.ID_VALORIZZAZIONE_RUN.desc())
                .limit(1).fetchOne();


        Record2<Integer, Object> config = create.select(PERF_MONITORAGGIO_CODIFICATO_CONF.ID_MONITORAGGIO_CODIFICATO_CONF, PERF_MONITORAGGIO_CODIFICATO_CONF.CONFIGURAZIONE)
                .from(PERF_MONITORAGGIO_CODIFICATO_CONF)
                .where(PERF_MONITORAGGIO_CODIFICATO_CONF.DATA_FINE_VALIDITA.greaterOrEqual(new Timestamp(new Date().getTime()))
                        .or(PERF_MONITORAGGIO_CODIFICATO_CONF.DATA_FINE_VALIDITA.isNull()))
                .orderBy(PERF_MONITORAGGIO_CODIFICATO_CONF.ID_MONITORAGGIO_CODIFICATO_CONF.desc())
                .limit(1).fetchOne();

        if (config == null) {
            log.error("Non esiste una configurazione attiva per il monitoraggio");
            log.error("Exit");
            return;
        }

        Gson gson = new Gson();
        Configurazione conf = gson.fromJson(new String(config.component2() + ""), Configurazione.class);


        try{


            Record1<String> soglia = create.select(PERF_SETTING.VALUE)
                    .from(PERF_CONFIGURATION)
                    .join(PERF_CONFIGURATION_SETTINGS).on(PERF_CONFIGURATION.CONFIGURATION_ID.eq(PERF_CONFIGURATION_SETTINGS.CONFIGURATION_ID))
                    .join(PERF_SETTING).on(PERF_CONFIGURATION_SETTINGS.SETTINGS_ID.eq(PERF_SETTING.SETTING_ID))
                    .where(PERF_CONFIGURATION.DOMAIN.eq("codifica"))
                    .and(PERF_CONFIGURATION.CONF_KEY.eq("thresholds"))
                    .and(PERF_CONFIGURATION.ACTIVE.eq(Byte.valueOf("1")))
                    .and(PERF_SETTING.RELATION.eq("GT"))
                    .and(PERF_SETTING.SETTING_KEY.eq("auto.multiple-result.distSecond")).fetchOne();

            if(soglia.component1() != null){
                conf.setValoreSoglia(new Double(soglia.component1()).longValue());

                log.info("Aggiornata la configurazione con valore:"+ conf.getValoreSoglia());
            }

        }catch(Exception e){log.error("impossibile reperire la nuova conf del valore soglia",e);}

        Result<PerfMonitoraggioCodificatoRecord> valorizzazioneRun = null;
        Condition periodoCondition = trueCondition();


        if(generaNonRipartito){
            log.info("Verifica che non esistano dati per il periodo corrente");
            valorizzazioneRun = create.selectFrom(PERF_MONITORAGGIO_CODIFICATO)
                    .where(PERF_MONITORAGGIO_CODIFICATO.ID_VALORIZZAZIONE_RUN.isNull().and(date(PERF_MONITORAGGIO_CODIFICATO.DATA_CREAZIONE).eq(date(new Date())))).fetch();

            periodoCondition = PERF_MOVIMENTO_CONTABILE.ID_PERIODO_RIPARTIZIONE.isNull();
            //cancello la calorizzazione in modo da non abbinarla a quel periodo di ripartizione (è quello corrente)
            valorizzazioneRecord= null;


        }else{
            log.info("Verifica che non esistano dati per l'ultima RUN");
            valorizzazioneRun = create.selectFrom(PERF_MONITORAGGIO_CODIFICATO)
                    .where(PERF_MONITORAGGIO_CODIFICATO.ID_VALORIZZAZIONE_RUN.eq(valorizzazioneRecord.component1())).fetch();

            periodoCondition = PERF_MOVIMENTO_CONTABILE.ID_PERIODO_RIPARTIZIONE.eq(valorizzazioneRecord.component2());
        }

        if (!CollectionUtils.isEmpty(valorizzazioneRun)) {
            log.info("Esistono già i dati per l'ultima RUN o del periodo corrente");
            log.info("Exit");
            return;
        }

        insertMonitoraggio(create, valorizzazioneRecord, config, gson, conf, periodoCondition);
        log.info("Exit");

    }

    private void insertMonitoraggio(DSLContext create, Record2<Long, Long> valorizazioneRecord, Record2<Integer, Object> config, Gson gson, Configurazione conf, Condition periodoCondition) {
        Table<Record4<Long, BigDecimal, BigDecimal, BigDecimal>> nestedQuery = create
                .select(PERF_UTILIZZAZIONE.ID_COMBANA,
                        sum(coalesce(PERF_UTILIZZAZIONE.PUNTI_RICALCOLATI.multiply(PERF_PROGRAMMA_MUSICALE.IMPORTO_VALORIZZATO.divide(PERF_MOVIMENTO_CONTABILE.PUNTI_PROGRAMMA_MUSICALE)),BigDecimal.ZERO)).as("importo"),
                        PERF_CODIFICA.CONFIDENZA,
                        PERF_CODIFICA.DIST_SEC_CANDIDATA
                ).from(PERF_UTILIZZAZIONE)
                .join(PERF_CODIFICA).on(PERF_UTILIZZAZIONE.ID_COMBANA.eq(PERF_CODIFICA.ID_COMBANA))
                .join(PERF_PROGRAMMA_MUSICALE).on(PERF_PROGRAMMA_MUSICALE.ID_PROGRAMMA_MUSICALE.eq(PERF_UTILIZZAZIONE.ID_PROGRAMMA_MUSICALE))
                .join(PERF_MOVIMENTO_CONTABILE).on(PERF_MOVIMENTO_CONTABILE.ID_PROGRAMMA_MUSICALE.eq(PERF_PROGRAMMA_MUSICALE.ID_PROGRAMMA_MUSICALE))
                .where(periodoCondition)
                .groupBy(PERF_UTILIZZAZIONE.ID_COMBANA,
                        PERF_CODIFICA.CONFIDENZA,
                        PERF_CODIFICA.DIST_SEC_CANDIDATA).asTable();


        List<Field<BigDecimal>> listImportiRosso = new ArrayList();
        List<Field<BigDecimal>> listImportiVerde = new ArrayList();
        List<Field<Integer>> listaConfidenza = new ArrayList();

//        AggregateFunction<BigDecimal>[] listImportiRosso  = null;

        conf.getValoreEconomicoList().forEach(val -> {

            Condition conditionVerde = nestedQuery.field(PERF_CODIFICA.DIST_SEC_CANDIDATA).greaterThan(new BigDecimal(conf.getValoreSoglia()));
            Condition conditionRosso = nestedQuery.field(PERF_CODIFICA.DIST_SEC_CANDIDATA).lessOrEqual(new BigDecimal(conf.getValoreSoglia()));


            if (val.getSuperiore() != null) {
                conditionVerde = conditionVerde.and(nestedQuery.field("importo", BigDecimal.class).lessThan(BigDecimal.valueOf(val.getSuperiore())));
                conditionRosso = conditionRosso.and(nestedQuery.field("importo", BigDecimal.class).lessThan(BigDecimal.valueOf(val.getSuperiore())));
            }
            if (val.getInferiore() != null) {
                conditionVerde = conditionVerde.and(nestedQuery.field("importo", BigDecimal.class).greaterOrEqual(BigDecimal.valueOf(val.getInferiore())));
                conditionRosso = conditionRosso.and(nestedQuery.field("importo", BigDecimal.class).greaterOrEqual(BigDecimal.valueOf(val.getInferiore())));
            }

            listImportiRosso.add(sum(
                    when(conditionRosso, 1)
                            .otherwise(0)));

            listImportiVerde.add(sum(
                    when(conditionVerde, 1)
                            .otherwise(0)));
        });

        conf.getLivelloSomiglianzaList().forEach(val -> {

            Condition condition = trueCondition();

            if (val.getInferiore() != null)
                condition = condition.and(nestedQuery.field(PERF_CODIFICA.CONFIDENZA).greaterOrEqual(BigDecimal.valueOf(val.getInferiore())));
            if (val.getSuperiore() != null)
                condition = condition.and(nestedQuery.field(PERF_CODIFICA.CONFIDENZA).lessThan(BigDecimal.valueOf(val.getSuperiore())));

            listaConfidenza.add(
                    when(condition, 1)
                            .otherwise(0).as("" + val.getId()));
        });


        Result<Record> fetchData = create
                .select(listImportiRosso)
                .select(listImportiVerde)
                .select(listaConfidenza)
                .from(nestedQuery)
                .groupBy(listaConfidenza)
                .orderBy(listaConfidenza)
                .fetch();

        JsonObject result = new JsonObject();
        JsonObject valori = new JsonObject();


        int i = 1;

        /*
        Riempio la mappa con valori a 0
         */

        HashMap<String, JsonObject> listaValore = new HashMap<>();
        conf.getValoreEconomicoList().forEach(valore -> {

            JsonObject jImporti = new JsonObject();
            jImporti.addProperty("secondaMinore", 0 + "");
            jImporti.addProperty("secondaMaggiore", 0 + "");
            listaValore.put("" + valore.getId(), jImporti);
        });


        for (Record rows : fetchData) {

            JsonObject jRows = new JsonObject();

            //ciclo tutti i risultati per tutte le righe di importi verde + rosso
            for (int j = 0; j < listImportiRosso.size(); j++) {
                JsonObject jImporti = new JsonObject();
                jImporti.addProperty("secondaMinore", rows.getValue(j) + "");
                jImporti.addProperty("secondaMaggiore", rows.getValue(j + (listImportiRosso.size())) + "");

                JsonArray array = new JsonArray();
                array.add(jImporti);
                jRows.add("" + (j + 1), array);

            }


            conf.getLivelloSomiglianzaList().forEach(confidenza -> {
                if (rows.get("" + confidenza.getId(), Integer.class) == 1) {
                    valori.add("" + confidenza.getId(), jRows);
                }
            });


        }

        conf.getLivelloSomiglianzaList().forEach(confidenza -> {
            if (valori.get("" + confidenza.getId()) == null) {
                JsonObject jRows = new JsonObject();
                conf.getValoreEconomicoList().forEach(val -> {

                    JsonObject jImportiZero = new JsonObject();
                    jImportiZero.addProperty("secondaMinore", 0 + "");
                    jImportiZero.addProperty("secondaMaggiore", 0 + "");

                    JsonArray array = new JsonArray();
                    array.add(jImportiZero);
                    jRows.add("" + (val.getId()), array);
                });
                valori.add("" + confidenza.getId(), jRows);
            }
        });


        result.add("valori", valori);
        String conv = gson.toJson(conf);
        JsonElement elemConf = gson.fromJson(conv, JsonElement.class);
        result.add("configurazioni", elemConf);


        create.insertInto(PERF_MONITORAGGIO_CODIFICATO,
                PERF_MONITORAGGIO_CODIFICATO.DATA_CREAZIONE,
                PERF_MONITORAGGIO_CODIFICATO.ID_MONITORAGGIO_CODIFICATO_CONF,
                PERF_MONITORAGGIO_CODIFICATO.ID_VALORIZZAZIONE_RUN,
                PERF_MONITORAGGIO_CODIFICATO.VALORI)
                .values(new Timestamp(new Date().getTime()), config.component1(), valorizazioneRecord != null ? valorizazioneRecord.component1(): null, result.toString())
                .execute();
    }
//
//}

/*QUERY OK
select
  sum(CASE WHEN a.IMPORTO < 5 AND a.DIST_SEC_CANDIDATA >= 20
             THEN 1 ELSE 0 END) importo5_verde,
  sum(CASE WHEN a.IMPORTO < 5 AND a.DIST_SEC_CANDIDATA < 20
             THEN 1 ELSE 0 END) importo5_rosso,
  sum(CASE WHEN a.IMPORTO > 5 AND a.IMPORTO <= 15
             THEN 1 ELSE 0 END) importo15,
  sum(CASE WHEN a.IMPORTO > 15 AND a.IMPORTO <= 30
             THEN 1 ELSE 0 END) importo30,
  sum(CASE WHEN a.IMPORTO > 30 AND a.IMPORTO < 3000000
             THEN 1 ELSE 0 END) importo30,

  CASE WHEN CONFIDENZA >= 95
         THEN 1 ELSE 0 END conf100,
  CASE WHEN CONFIDENZA < 95
  THEN 1 ELSE 0 END conf95

from(
select
  PC.ID_COMBANA ID_COMBANA,
     sum(COALESCE(PUNTI_RICALCOLATI * (PERF_PROGRAMMA_MUSICALE.IMPORTO_VALORIZZATO/PUNTI_PROGRAMMA_MUSICALE),0)) IMPORTO ,
     DIST_SEC_CANDIDATA,
     CONFIDENZA
from PERF_UTILIZZAZIONE uti
       join PERF_CODIFICA PC on uti.ID_CODIFICA = PC.ID_CODIFICA
       join PERF_PROGRAMMA_MUSICALE on PERF_PROGRAMMA_MUSICALE.ID_PROGRAMMA_MUSICALE = uti.ID_PROGRAMMA_MUSICALE
       join PERF_MOVIMENTO_CONTABILE on PERF_MOVIMENTO_CONTABILE.ID_PROGRAMMA_MUSICALE = PERF_PROGRAMMA_MUSICALE.ID_PROGRAMMA_MUSICALE
where PERF_MOVIMENTO_CONTABILE.ID_PERIODO_RIPARTIZIONE is null
group by ID_COMBANA,CONFIDENZA,DIST_SEC_CANDIDATA
) a
group by conf100,conf95;

 */


}