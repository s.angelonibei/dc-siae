package com.alkemy.performing.valorizzatore.dashboardetl.model;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Data
@Entity
@Table(name = "PERF_MONITORAGGIO_CODIFICATO_CONF")
public class PerfMonitoraggioCodificatoConf {
    @Id
    @Column(name = "ID_MONITORAGGIO_CODIFICATO_CONF")
    private Integer idMonitoraggioCodificatoConf;

    @Column(name = "CONFIGURAZIONE")
    private String configurazione;

    @Column(name = "DATA_FINE_VALIDITA")
    private Timestamp dataFineValidita;

    @OneToMany(mappedBy = "perfMonitoraggioCodificatoConfByIdMonitoraggioCodificatoConf")
    private Collection<PerfMonitoraggioCodificato> perfMonitoraggioCodificatoesByIdMonitoraggioCodificatoConf;

}
