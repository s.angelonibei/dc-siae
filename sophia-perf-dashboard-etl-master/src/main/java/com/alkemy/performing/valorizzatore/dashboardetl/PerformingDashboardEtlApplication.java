package com.alkemy.performing.valorizzatore.dashboardetl;

import com.alkemy.performing.valorizzatore.dashboardetl.service.DashBoardEtlService;
import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@CommonsLog
@RequiredArgsConstructor
public class PerformingDashboardEtlApplication implements CommandLineRunner {

//	private final EtlMonitoraggio etlMonitoraggio;
	private final DashBoardEtlService dashBoardEtlService;
//	private final EtlKpi etlKpi;

	public static void main(String[] args) {
		SpringApplication.run(PerformingDashboardEtlApplication.class, args);
	}

	public void run(String... strings) throws Exception {
		log.info("Genera dati monitoraggio performing started....");

		if (strings.length == 0){
			logErrorUsage();
		}else{

			if(strings[0].equalsIgnoreCase("ALL")){
				dashBoardEtlService.generaDatiMonitorCodificato(true);
				dashBoardEtlService.generaDatiMonitorCodificato(false);
				dashBoardEtlService.generaKpi();
				dashBoardEtlService.callRefreshPerfRiconcMvNowProcedure();
			}else if(strings[0].equalsIgnoreCase("MONITOR")) {
				dashBoardEtlService.generaDatiMonitorCodificato(true);
				dashBoardEtlService.generaDatiMonitorCodificato(false);
			}
			else if (strings[0].equalsIgnoreCase("KPI"))
				dashBoardEtlService.generaKpi();
			else if (strings[0].equalsIgnoreCase("RICONCILIA"))
				dashBoardEtlService.callRefreshPerfRiconcMvNowProcedure();
			else
				logErrorUsage();
		}



	}

	private void logErrorUsage(){
		log.error("NOTHING TO DO");
		log.error("Usage: parametri: MONITOR - per generare i dati del monitor codificato");
		log.error("Usage: parametri: KPI - per generare i dati dei KPI");
		log.error("Usage: parametri: ALL - per generare tutto");
	}

}
