package com.alkemy.performing.valorizzatore.dashboardetl.service;

import com.alkemy.performing.valorizzatore.dashboardetl.repository.EtlKpi;
import com.alkemy.performing.valorizzatore.dashboardetl.repository.EtlMonitoraggio;
import com.alkemy.performing.valorizzatore.dashboardetl.repository.EtlRiconciliazioneImporti;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DashBoardEtlService {

    private final EtlMonitoraggio etlMonitoraggio;
    private final EtlKpi etlKpi;
    private final EtlRiconciliazioneImporti etlRiconciliazioneImporti;

    public void generaDatiMonitorCodificato(boolean generaNonRipartito){
        etlMonitoraggio.generaDatiMonitorCodificato(generaNonRipartito);
    }

    public void generaKpi(){
        etlKpi.generaKpi();
    }

    public void callRefreshPerfRiconcMvNowProcedure() {
        etlRiconciliazioneImporti.callRefreshPerfRiconcMvNowProcedure();
    }

}
