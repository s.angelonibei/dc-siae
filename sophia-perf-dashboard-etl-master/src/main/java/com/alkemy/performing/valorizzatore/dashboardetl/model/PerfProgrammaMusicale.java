package com.alkemy.performing.valorizzatore.dashboardetl.model;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
@Entity
@Table(name = "PERF_PROGRAMMA_MUSICALE")
public class PerfProgrammaMusicale {

    @Id
    @Column(name = "CODICE_CAMPIONAMENTO")
    private Long codiceCampionamento;

    @Column(name = "ID_PROGRAMMA_MUSICALE")
    private Long idProgrammaMusicale;

    @Column(name = "NUMERO_PROGRAMMA_MUSICALE")
    private Long numeroProgrammaMusicale;
    @Column(name = "TIPO_PM")
    private String tipoPm;
    @Column(name = "SUPPORTO_PM")
    private String supportoPm;
    @Column(name = "AGENZIA_DI_CARICO")
    private String agenziaDiCarico;
    @Column(name = "DATA_ASSEGNAZIONE")
    private Timestamp dataAssegnazione;
    @Column(name = "DATA_RESTITUZIONE")
    private Timestamp dataRestituzione;
    @Column(name = "DATA_COMPILAZIONE")
    private Timestamp dataCompilazione;
    @Column(name = "STATO_PM")
    private String statoPm;
    @Column(name = "DATA_ANNULLAMENTO")
    private Timestamp dataAnnullamento;
    @Column(name = "DATA_ACQUISIZIONE_SOPHIA")
    private Timestamp dataAcquisizioneSophia;
    @Column(name = "DATA_RIPARTIZIONE")
    private Timestamp dataRipartizione;
    @Column(name = "DATA_INVIO_DG")
    private Timestamp dataInvioDg;
    @Column(name = "DATA_ACQUISIZIONE_IMMAGINE")
    private Timestamp dataAcquisizioneImmagine;
    @Column(name = "DATA_RECEZIONE_INFO_DIG")
    private Timestamp dataRecezioneInfoDig;
    @Column(name = "FOGLIO_SEGUE_DI")
    private String foglioSegueDi;
    @Column(name = "CAUSA_ANNULLAMENTO")
    private String causaAnnullamento;
    @Column(name = "PROGRESSIVO")
    private Long progressivo;
    @Column(name = "TOTALE_CEDOLE")
    private Long totaleCedole;
    @Column(name = "TOTALE_DURATA_CEDOLE")
    private Long totaleDurataCedole;
    @Column(name = "FOGLIO")
    private String foglio;
    @Column(name = "PM_RIFERIMENTO")
    private String pmRiferimento;
    @Column(name = "RISULTATO_CALCOLO_RESTO")
    private Long risultatoCalcoloResto;
    @Column(name = "DATA_ORA_ULTIMA_MODIFICA")
    private Timestamp dataOraUltimaModifica;
    @Column(name = "UTENTE_ULTIMA_MODIFICA")
    private String utenteUltimaModifica;
    @Column(name = "GIORNATE_TRATTENIMENTO")
    private Long giornateTrattenimento;
    @Column(name = "FOGLI")
    private Long fogli;
    @Column(name = "FLAG_PM_CORRENTE")
    private String flagPmCorrente;
    @Column(name = "DATA_ASSEGNAZIONE_INCARICO")
    private Timestamp dataAssegnazioneIncarico;
    @Column(name = "DATA_REGISTRAZIONE_RILEVAZIONE")
    private Timestamp dataRegistrazioneRilevazione;
    @Column(name = "DURATA_RILEVAZIONE")
    private Long durataRilevazione;
    @Column(name = "PROTOCOLLO_REGISTRAZIONE")
    private Long protocolloRegistrazione;
    @Column(name = "PROTOCOLLO_TRASMISSIONE")
    private Long protocolloTrasmissione;

    @Column(name = "FLAG_DA_CAMPIONARE")
    private String flagDaCampionare;
    @Column(name = "DATA_INS")
    private Timestamp dataIns;
    @Column(name = "FLAG_GRUPPO_PRINCIPALE")
    private String flagGruppoPrincipale;
    @Column(name = "IMPORTO_VALORIZZATO")
    private BigDecimal importoValorizzato;
    @Column(name = "STATO_WEB")
    private String statoWeb;

}
