package com.alkemytech.sophia.pm.ndm;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.pm.BasicDao;

/*

CREATE TABLE IF NOT EXISTS PERF_NDM_FILE
( ID_NDM_FILE         BIGINT NOT NULL AUTO_INCREMENT
, NOME_FILE           VARCHAR(100) NOT NULL
, S3_PATH             VARCHAR(1000) NOT NULL
, ULTIMA_LAVORAZIONE  DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
, NUMERO_LAVORAZIONI  INT
, NUMERO_RECORD       BIGINT
, STATISTICHE         JSON
, PRIMARY KEY (ID_NDM_FILE)
);

CREATE UNIQUE INDEX IDX_PERF_NDM_FILE_01
   ON PERF_NDM_FILE (NOME_FILE ASC);


CREATE TABLE IF NOT EXISTS PERF_NDM_VOCE_FATTURA
( ID_NDM_VOCE_FATTURA    BIGINT NOT NULL AUTO_INCREMENT
, ID_NDM_FILE            BIGINT NOT NULL
, ANNO_FATTURA           INT NOT NULL
, MESE_FATTURA           INT NOT NULL
, DATA_FATTURA           DATETIME NOT NULL
, ANNO_CONTABILE_FATTURA INT NOT NULL
, MESE_CONTABILE_FATTURA INT NOT NULL
, DATA_CONTABILE_FATTURA DATETIME NOT NULL
, ID_INCASSO             BIGINT
, ID_QUIETANZA           BIGINT
, NUMERO_REVERSALE       VARCHAR(20)
, NUMERO_FATTURA         VARCHAR(20)
, VOCE                   VARCHAR(20)
, IMPORTO_ORIGINALE      DECIMAL(30,15)
, IMPORTO_AGGIO          DECIMAL(30,15)
, PERCENTUALE_AGGIO      DECIMAL(30,15)
, PRIMARY KEY (ID_NDM_VOCE_FATTURA)
);

CREATE UNIQUE INDEX IDX_PERF_NDM_VOCE_FATTURA_01
   ON PERF_NDM_VOCE_FATTURA (ID_NDM_VOCE_FATTURA ASC, VOCE ASC);


CREATE TABLE IF NOT EXISTS PERF_NDM_VOCE_SCOMPOSTA
( ID_NDM_VOCE_SCOMPOSTA    BIGINT NOT NULL AUTO_INCREMENT
, ID_NDM_FILE              BIGINT NOT NULL
, ID_NDM_VOCE_FATTURA      BIGINT NOT NULL
, SEQUENZA                 INT NOT NULL
, TIPO_PENALE              VARCHAR(20)
, IMPORTO_ORIGINALE        DECIMAL
, PERCENTUALE_RIPARTITA    DECIMAL
, IMPORTO_RIPARTITO        DECIMAL
, PERCENTUALE_AGGIO        DECIMAL
, IMPORTO_AGGIO            DECIMAL
, PRIMARY KEY (ID_NDM_VOCE_SCOMPOSTA)
);
 
 */

public class NdmDao extends BasicDao {
	
	private static final Logger logger = LoggerFactory.getLogger(NdmDao.class);

	private final Connection connection;

	private boolean enableGeneratedKeys;	
	private long lastGeneratedKey;

	public NdmDao(Connection connection) {
		this(connection, true);
	}

	public NdmDao(Connection connection, boolean enableGeneratedKeys) {
		super();
		this.connection = connection;
		this.enableGeneratedKeys = enableGeneratedKeys;
	}

	public boolean isEnableGeneratedKeys() {
		return enableGeneratedKeys;
	}

	public void setEnableGeneratedKeys(boolean enableGeneratedKeys) {
		this.enableGeneratedKeys = enableGeneratedKeys;
	}

	public long getLastGeneratedKey() {
		if (enableGeneratedKeys) {
			return lastGeneratedKey;
		}
		throw new IllegalStateException("generated PKs not enabled");
	}
	
	private void setLastGeneratedKey(Statement stmt) throws SQLException {
		lastGeneratedKey = 0L;
		try (final ResultSet rset = stmt.getGeneratedKeys()) {
			if (rset.next())
				lastGeneratedKey = rset.getLong(1);
		}
	}
	
	public long getIdNdmFile(String filename) throws SQLException {
		final StringBuilder sql = new StringBuilder()
			.append(" select ID_NDM_FILE ")
			.append("   from PERF_NDM_FILE ")
			.append("  where NOME_FILE = ").append(toVarchar(filename));
		try (final Statement stmt = connection.createStatement()) {
			try (final ResultSet rset = stmt.executeQuery(sql.toString())) {
				if (rset.next())
					return rset.getLong(1);
			}
		}
		return -1L;
	}
	
	public boolean insertNdmFile(String filename, String s3path) throws SQLException {
		final StringBuilder sql = new StringBuilder()
			.append(" insert into PERF_NDM_FILE ")
			.append(" ( NOME_FILE ")
			.append(" , S3_PATH ")
			.append(" , ULTIMA_LAVORAZIONE ")
			.append(" , NUMERO_LAVORAZIONI ")
			.append(" ) values ")
			.append(" ( ").append(toVarchar(filename))
			.append(" , ").append(toVarchar(s3path))
			.append(" , now() ")
			.append(" , 1 ")
			.append(" ) ");
		try (final Statement stmt = connection.createStatement()) {
			if (enableGeneratedKeys) {
				if (1 == stmt.executeUpdate(sql.toString(), Statement.RETURN_GENERATED_KEYS)) {
					setLastGeneratedKey(stmt);
					return true;
				}
			} else if (1 == stmt.executeUpdate(sql.toString())) {
				return true;
			}
		} catch (SQLException e) {
			if (ERROR_DUPLICATE_KEY != e.getErrorCode()) {
				logger.error("insertNdmFile: {}", sql);
				throw e;
		    }
		}
		return false;
	}
	
	public boolean reprocessNdmFile(long idNdmFile) throws SQLException {
		final StringBuilder sql = new StringBuilder()
			.append(" update PERF_NDM_FILE ")
			.append(" set ULTIMA_LAVORAZIONE = now() ")
			.append(" , NUMERO_LAVORAZIONI = NUMERO_LAVORAZIONI + 1 ")
			.append(" , NUMERO_RECORD = NULL ")
			.append(" , STATISTICHE = NULL ")
			.append(" where ID_NDM_FILE = ").append(idNdmFile);
		try (final Statement stmt = connection.createStatement()) {
			return 1 == stmt.executeUpdate(sql.toString());
		} catch (SQLException e) {
			logger.error("reprocessNdmFile: {}", sql);
			throw e;
		}
	}
	
	public boolean updateNdmFile(long idNdmFile, long numeroRecord, String statistiche) throws SQLException {
		final StringBuilder sql = new StringBuilder()
			.append(" update PERF_NDM_FILE ")
			.append(" set NUMERO_RECORD = ").append(numeroRecord)
			.append(" , STATISTICHE = ").append(toVarchar(statistiche))
			.append(" where ID_NDM_FILE = ").append(idNdmFile);
		try (final Statement stmt = connection.createStatement()) {
			return 1 == stmt.executeUpdate(sql.toString());
		} catch (SQLException e) {
			logger.error("updateNdmFile: {}", sql);
			throw e;
		}
	}
	
	public boolean insertVoceFattura(long idNdmFile, String annoFattura, String meseFattura, String dataFattura, String annoContabileFattura,
			String meseContabileFattura, String dataContabileFattura, String idIncasso, String idQuietanza, String numeroReversale,
			String numeroFattura, String voce, BigDecimal importoOriginale, BigDecimal importoAggio, BigDecimal percentualeAggio) throws SQLException {
		final StringBuilder sql = new StringBuilder()
			.append(" insert into PERF_NDM_VOCE_FATTURA ")
			.append(" ( ID_NDM_FILE ")
			.append(" , ANNO_FATTURA ")
			.append(" , MESE_FATTURA ")
			.append(" , DATA_FATTURA ")
			.append(" , ANNO_CONTABILE_FATTURA ")
			.append(" , MESE_CONTABILE_FATTURA ")
			.append(" , DATA_CONTABILE_FATTURA ")
			.append(" , ID_INCASSO ")
			.append(" , ID_QUIETANZA ")
			.append(" , NUMERO_REVERSALE ")
			.append(" , NUMERO_FATTURA ")
			.append(" , VOCE ")
			.append(" , IMPORTO_ORIGINALE ")
			.append(" , IMPORTO_AGGIO ")
			.append(" , PERCENTUALE_AGGIO ")
			.append(" ) values ")
			.append(" ( ").append(idNdmFile)
			.append(" , ").append(toLong(annoFattura))
			.append(" , ").append(toLong(meseFattura))
			.append(" , ").append(toDateTime(dataFattura))
			.append(" , ").append(toLong(annoContabileFattura))
			.append(" , ").append(toLong(meseContabileFattura))
			.append(" , ").append(toDateTime(dataContabileFattura))
			.append(" , ").append(toLong(idIncasso))
			.append(" , ").append(toLong(idQuietanza))
			.append(" , ").append(toVarchar(numeroReversale))
			.append(" , ").append(toVarchar(numeroFattura))
			.append(" , ").append(toVarchar(voce))
			.append(" , ").append(importoOriginale.stripTrailingZeros().toPlainString())
			.append(" , ").append(importoAggio.stripTrailingZeros().toPlainString())
			.append(" , ").append(percentualeAggio.stripTrailingZeros().toPlainString())
			.append(" ) ");
		try (final Statement stmt = connection.createStatement()) {
			if (enableGeneratedKeys) {
				if (1 == stmt.executeUpdate(sql.toString(), Statement.RETURN_GENERATED_KEYS)) {
					setLastGeneratedKey(stmt);
					return true;
				}
			} else if (1 == stmt.executeUpdate(sql.toString())) {
				return true;
			}
		} catch (SQLException e) {
			if (ERROR_DUPLICATE_KEY != e.getErrorCode()) {
				logger.error("insertVoceFattura: {}", sql);
				throw e;
		    }
		}
		return false;
	}

	public boolean insertVoceScomposta(long idNdmFile, long idNdmVoceFattura, String sequenza, String tipoPenale, BigDecimal importoOriginale,
			BigDecimal percentualeRipartita, BigDecimal importoRipartito, BigDecimal percentualeAggio, BigDecimal importoAggio) throws SQLException {
		final StringBuilder sql = new StringBuilder()
			.append(" insert into PERF_NDM_VOCE_SCOMPOSTA ")
			.append(" ( ID_NDM_FILE ")
			.append(" , ID_NDM_VOCE_FATTURA ")
			.append(" , SEQUENZA ")
			.append(" , TIPO_PENALE ")
			.append(" , IMPORTO_ORIGINALE ")
			.append(" , PERCENTUALE_RIPARTITA ")
			.append(" , IMPORTO_RIPARTITO ")
			.append(" , PERCENTUALE_AGGIO ")
			.append(" , IMPORTO_AGGIO ")
			.append(" ) values ")
			.append(" ( ").append(idNdmFile)
			.append(" , ").append(idNdmVoceFattura)
			.append(" , ").append(toLong(sequenza))
			.append(" , ").append(toVarchar(tipoPenale))
			.append(" , ").append(importoOriginale.stripTrailingZeros().toPlainString())
			.append(" , ").append(percentualeRipartita.stripTrailingZeros().toPlainString())
			.append(" , ").append(importoRipartito.stripTrailingZeros().toPlainString())
			.append(" , ").append(percentualeAggio.stripTrailingZeros().toPlainString())
			.append(" , ").append(importoAggio.stripTrailingZeros().toPlainString())
			.append(" ) ");
		try (final Statement stmt = connection.createStatement()) {
			if (enableGeneratedKeys) {
				if (1 == stmt.executeUpdate(sql.toString(), Statement.RETURN_GENERATED_KEYS)) {
					setLastGeneratedKey(stmt);
					return true;
				}
			} else if (1 == stmt.executeUpdate(sql.toString())) {
				return true;
			}
		} catch (SQLException e) {
			if (ERROR_DUPLICATE_KEY != e.getErrorCode()) {
				logger.error("insertVoceScomposta: {}", sql);
				throw e;
		    }
		}
		return false;
	}
	
	public void deleteFileRecords(long idNdmFile) throws SQLException {
		StringBuilder sql = new StringBuilder()
			.append(" delete from PERF_NDM_VOCE_SCOMPOSTA ")
			.append(" where ID_NDM_FILE = ").append(idNdmFile);
		try (final Statement stmt = connection.createStatement()) {
			stmt.executeUpdate(sql.toString());
		} catch (SQLException e) {
			logger.error("deleteFileRecords: {}", sql);
			throw e;
		}
		sql = new StringBuilder()
			.append(" delete from PERF_NDM_VOCE_FATTURA ")
			.append(" where ID_NDM_FILE = ").append(idNdmFile);
		try (final Statement stmt = connection.createStatement()) {
			stmt.executeUpdate(sql.toString());
		} catch (SQLException e) {
			logger.error("deleteFileRecords: {}", sql);
			throw e;
		}
//		sql = new StringBuilder()
//			.append(" delete from PERF_NDM_FILE ")
//			.append(" where ID_NDM_FILE = ").append(idNdmFile);
//		try (final Statement stmt = connection.createStatement()) {
//			stmt.executeUpdate(sql.toString());
//		} catch (SQLException e) {
//			logger.error("deleteFileRecords: {}", sql);
//			throw e;
//		}
	}
	
}
