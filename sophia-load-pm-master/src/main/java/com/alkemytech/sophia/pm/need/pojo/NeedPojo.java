package com.alkemytech.sophia.pm.need.pojo;

public abstract class NeedPojo {

	protected String toString(byte[] buffer, int offset, int length) {
		final String s = new String(buffer, offset, length).trim();
		return "".equals(s) ? null : s;
	}
	
}
