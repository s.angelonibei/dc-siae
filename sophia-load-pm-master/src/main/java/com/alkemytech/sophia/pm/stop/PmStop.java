package com.alkemytech.sophia.pm.stop;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.sqs.SqsS3Events;
import com.alkemytech.sophia.commons.sqs.SqsS3EventPump;
import com.alkemytech.sophia.pm.McmdbDataSource;
import com.amazonaws.services.s3.model.S3Object;
import com.google.common.base.Strings;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Scopes;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class PmStop {
	
	private static final Logger logger = LoggerFactory.getLogger(PmStop.class);

	private static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// amazon service(s)
			bind(S3.class)
				.in(Scopes.SINGLETON);
			bind(SQS.class)
				.in(Scopes.SINGLETON);
			// data source(s)
			bind(DataSource.class)
				.annotatedWith(Names.named("MCMDB"))
				.to(McmdbDataSource.class)
				.in(Scopes.SINGLETON);
			// self
			bind(PmStop.class).asEagerSingleton();
		}
		
	}
		
	public static void main(String[] args) {
		try {
			final Injector injector = Guice
					.createInjector(new GuiceModuleExtension(args, "/sophia-stop-pm.properties"));
			final PmStop instance = injector
					.getInstance(PmStop.class)
					.startup();
			try {
				instance.process();
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}

	private final Properties configuration;
	private final S3 s3;
	private final SQS sqs;
	private final DataSource mcmdbDS;

	@Inject
	protected PmStop(@Named("configuration") Properties configuration,
			S3 s3, SQS sqs,
			SqsS3Events s3Events,
			@Named("MCMDB") DataSource mcmdbDS) {
		super();
		this.configuration = configuration;
		this.s3 = s3;
		this.sqs = sqs;
		this.mcmdbDS = mcmdbDS;
	}
	
	public PmStop startup() {
		s3.startup();
		sqs.startup();
		return this;
	}
	
	public PmStop shutdown() {
		s3.shutdown();
		sqs.shutdown();
		return this;
	}

	public void process() throws Exception {
		
		final int listenPort = Integer.parseInt(configuration.getProperty("pmstop.bind_port",
				configuration.getProperty("default.bind_port", "0")));
		
		try (final ServerSocket socket = new ServerSocket(listenPort)) {
			logger.info("process: listening on {}", socket.getLocalSocketAddress());

			final SqsS3Events s3Events = new SqsS3Events(SqsS3Events.OBJECT_CRATED);
			final SqsS3EventPump sqsS3EventPump = new SqsS3EventPump(sqs, s3Events, configuration, "pmstop");
			sqsS3EventPump.pollingLoop(new SqsS3EventPump.Consumer() {
				
				@Override
				public void consume(String bucket, String key) {
					try {
						process(bucket, key);
					} catch (Exception e) {
						logger.error("consume", e);
					}
				}
				
			});
		}
		
	}

	public void process(String bucket, String key) throws Exception {
		logger.info("process: bucket {}", bucket);
		logger.info("process: key {}", key);

		final char delimiter = configuration.getProperty("csv.separator.field", ",").charAt(0);
		final char recordSeparator = configuration.getProperty("csv.separator.line", "\n").charAt(0);
		final String statoPmNew = configuration.getProperty("pmstop.stato_pm_new");
		final List<String> statoPmOld = Arrays.asList(configuration.getProperty("pmstop.stato_pm_old", "V").split(","));
		final boolean useNow = "true".equalsIgnoreCase(configuration.getProperty("pmstop.use_now"));
		final String nettoMaturatoBackup = configuration.getProperty("pmstop.netto_maturato_backup");
		
		// connect to db
		try (final Connection mcmdb = mcmdbDS.getConnection()) {
			logger.info("process: connected to {} {}", mcmdb.getMetaData()
					.getDatabaseProductName(), mcmdb.getMetaData().getURL());
			mcmdb.setAutoCommit(true);
			
			final S3Object s3obj = s3.getObject(new S3.Url(bucket, key));
			if (null == s3obj) {
				throw new IOException("unable to download s3://" + bucket + "/" + key);
			}

			// stats
			int errored = 0;
			int malformed = 0;
			int updated = 0;
			int notFound = 0;
			int badStatus = 0;

			try (final InputStream in = s3obj.getObjectContent();
					final InputStreamReader reader = new InputStreamReader(in, StandardCharsets.UTF_8);
					final CSVParser parser = new CSVParser(reader, CSVFormat.DEFAULT.withIgnoreSurroundingSpaces()
							.withDelimiter(delimiter).withRecordSeparator(recordSeparator))) {
				
				// prepare statements
				final StringBuilder sqlSelectPM = new StringBuilder()
					.append(" select ID_PROGRAMMA_MUSICALE ")
					.append(" , STATO_PM ")
					.append(" from PERF_PROGRAMMA_MUSICALE ")
					.append(" where NUMERO_PROGRAMMA_MUSICALE = ? ");
				final PreparedStatement stmtSelectPM = mcmdb.prepareStatement(sqlSelectPM.toString());

				final StringBuilder sqlUpdatePM = new StringBuilder()
					.append(" update PERF_PROGRAMMA_MUSICALE ")
					.append(" set CAUSA_ANNULLAMENTO = ? ")
					.append(useNow ? 
							" , DATA_ANNULLAMENTO = now() " :
							" , DATA_ANNULLAMENTO = ? ")
					.append(" , STATO_PM = ? ")
					.append(" where ID_PROGRAMMA_MUSICALE = ? ");
				final PreparedStatement stmtUpdatePM = mcmdb.prepareStatement(sqlUpdatePM.toString());

				final StringBuilder sqlUpdateUT = !Strings.isNullOrEmpty(nettoMaturatoBackup) ?
					new StringBuilder()
						.append(" update PERF_UTILIZZAZIONE ")
						.append(" set ").append(nettoMaturatoBackup).append(" = NETTO_MATURATO ")
						.append(" , NETTO_MATURATO = 0 ")
						.append(" where ID_PROGRAMMA_MUSICALE = ? ") :
					new StringBuilder()
						.append(" update PERF_UTILIZZAZIONE ")
						.append(" set NETTO_MATURATO = 0 ")
						.append(" where ID_PROGRAMMA_MUSICALE = ? ");
				final PreparedStatement stmtUpdateUT = mcmdb.prepareStatement(sqlUpdateUT.toString());

				// loop on file rows
				for (CSVRecord record : parser) {
					try {
						// validate record
						if (record.size() < 2) {
							logger.error("process: missing column(s)");
							malformed ++;
							continue;
						}
						final String numeroProgrammaMusicale = record.get(0);
						if (Strings.isNullOrEmpty(numeroProgrammaMusicale)){
							logger.error("process: missing numeroProgrammaMusicale");
							malformed ++;
							continue;
						}
						try {
							Long.parseLong(numeroProgrammaMusicale);
						} catch (NumberFormatException e) {
							logger.error("process: non numeric numeroProgrammaMusicale");
							malformed ++;
							continue;
						}
						final String causaAnnullamento = record.get(1);
						if (Strings.isNullOrEmpty(causaAnnullamento)){
							logger.error("process: missing causaAnnullamento");
							malformed ++;
							continue;
						}
						
						// select PM
						int i = 1;
						stmtSelectPM.setLong(i ++, Long.parseLong(numeroProgrammaMusicale));
						Long idProgrammaMusicale = null;
						String statoPm = null;
						try (ResultSet rs = stmtSelectPM.executeQuery()) {
							if (rs.next()) {
								idProgrammaMusicale = rs.getLong(1);
								statoPm = rs.getString(2);
							}
						}
						if (null == idProgrammaMusicale) {
							notFound ++;
							continue;
						}
						if (null == statoPm || !statoPmOld.contains(statoPm)) {
							badStatus ++;
							continue;
						}
						
						// update PM
						i = 1;
						stmtUpdatePM.setString(i++, causaAnnullamento);
						if (!useNow) {
							stmtUpdatePM.setTimestamp(i++, new java.sql.Timestamp(Calendar
									.getInstance(Locale.ITALY).getTimeInMillis()));							
						}
						stmtUpdatePM.setString(i++, statoPmNew);
						stmtUpdatePM.setLong(i++, idProgrammaMusicale);
						final int rownum = stmtUpdatePM.executeUpdate();					
						if (rownum > 0) {
							updated ++;
						} else {
							notFound ++;
						}
						
						// update UT
						i = 1;
						stmtUpdateUT.setLong(i ++, idProgrammaMusicale);
						stmtUpdateUT.executeUpdate();	
						
					} catch (Exception e) {
						logger.error("process", e);
						errored ++;
					}
				}
				
				stmtUpdateUT.close();
				stmtUpdatePM.close();
				stmtSelectPM.close();
				
			}
			
			logger.info("process: errored {}", errored);
			logger.info("process: malformed {}", malformed);
			logger.info("process: updated {}", updated);
			logger.info("process: notFound {}", notFound);
			logger.info("process: badStatus {}", badStatus);

			// upload stats file
			final StringBuilder stats = new StringBuilder()
				.append("Total input rows: ").append(errored + malformed + updated + notFound)
				.append("\nUpdated records: ").append(updated)
				.append("\nNot found records: ").append(notFound)
				.append("\nMalformed records: ").append(malformed)
				.append("\nIllegal state records: ").append(badStatus)
				.append("\nErrors: ").append(errored)
				.append("\n");
			final File file = File.createTempFile("", "");
			file.deleteOnExit();
			try (final FileOutputStream out = new FileOutputStream(file)) {
				out.write(stats.toString().getBytes("UTF-8"));
			}
			s3.upload(new S3.Url(bucket, key + ".log"), file);
			file.delete();
			
			// delete file
			s3.delete(new S3.Url(bucket, key));
			
		}
	}
	
}




