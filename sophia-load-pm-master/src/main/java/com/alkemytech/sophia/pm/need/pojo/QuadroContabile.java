package com.alkemytech.sophia.pm.need.pojo;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class QuadroContabile extends NeedPojo {

	// TRK 30
	// [1,2] "30"
//	private String cartella;				// [3] "A"
//	private String codiceSede;				// [4,5] "03"
//	private String codiceProvincia;			// [6,8] "032"
//	private String unitaTerritoriale;		// [9,10] "08"
	private String codiceVoceIncasso;		// [11,14] "2222"
//	private String progressivoCartella;		// [15,16] "01"
	private String contabilita;				// [17,22] "201608"
	private String numeroProgrammaMusicale;	// [23,30] "90195603"
	private String totaleCedole;			// [31,33] "056"
	private String totaleDurataCedole;		// [34,38] "00000"
	private String importoContabile;		// [39,50] "000000010039"
	private String fogli;					// [51] "1"
	private String giornate;				// [52,53] "01"
	private String numeroReversale;			// [54,60] "0059905"
	private String codiceComune;			// [61,63] "061"
	private String codiceLocale;			// [64,70] "1141404"
	private String rendicontoContabile;		// [71,76] "072016"
	private String sospeso;					// [77] " "
	private String senzaMaggiorazione;		// [78] " "
	private String numeroBobina;			// [79,83] "15408"
	private String numeroFotogramma;		// [84,88] "05947"
	private String tipoEvidenza;			// [89,90] "  "
	private String codificazione;			// [91,92] "  "
	private String numeroPermesso;			// [93,99] "0000837"
	private String tipoValuta;				// [100] "E"
	private String codiceOperatore;			// [101] " "
	private String incassoDemLordoLire;		// [102,113] "000000000000"
	// [114,180] (unused)
	
	public QuadroContabile(byte[] buffer) {
		super();
//		this.cartella = toString(buffer, 2, 1);
//		this.codiceSede = toString(buffer, 3, 2);
//		this.codiceProvincia = toString(buffer, 5, 3);
//		this.unitaTerritoriale = toString(buffer, 8, 2);
		this.codiceVoceIncasso = toString(buffer, 10, 4);
//		this.progressivoCartella = toString(buffer, 14, 2);
		this.contabilita = toString(buffer, 16, 6);
		this.numeroProgrammaMusicale = toString(buffer, 22, 8);
		this.totaleCedole = toString(buffer, 30, 3);
		this.totaleDurataCedole = toString(buffer, 33, 5);
		this.importoContabile = toString(buffer, 38, 12);
		this.fogli = toString(buffer, 50, 1);
		this.giornate = toString(buffer, 51, 2);
		this.numeroReversale = toString(buffer, 53, 7);
		this.codiceComune = toString(buffer, 60, 3);
		this.codiceLocale = toString(buffer, 63, 7);
		this.rendicontoContabile = toString(buffer, 70, 6);
		this.sospeso = toString(buffer, 76, 1);
		this.senzaMaggiorazione = toString(buffer, 77, 1);
		this.numeroBobina = toString(buffer, 78, 5);
		this.numeroFotogramma = toString(buffer, 83, 5);
		this.tipoEvidenza = toString(buffer, 88, 2);
		this.codificazione = toString(buffer, 90, 2);
		this.numeroPermesso = toString(buffer, 92, 7);
		this.tipoValuta = toString(buffer, 99, 1);
		this.codiceOperatore = toString(buffer, 100, 1);
		this.incassoDemLordoLire = toString(buffer, 101, 12);
	}
	
	public String getCodiceVoceIncasso() {
		return codiceVoceIncasso;
	}
	public void setCodiceVoceIncasso(String codiceVoceIncasso) {
		this.codiceVoceIncasso = codiceVoceIncasso;
	}
	public String getContabilita() {
		return contabilita;
	}
	public void setContabilita(String contabilita) {
		this.contabilita = contabilita;
	}
	public String getNumeroProgrammaMusicale() {
		return numeroProgrammaMusicale;
	}
	public void setNumeroProgrammaMusicale(String numeroProgrammaMusicale) {
		this.numeroProgrammaMusicale = numeroProgrammaMusicale;
	}
	public String getTotaleCedole() {
		return totaleCedole;
	}
	public void setTotaleCedole(String totaleCedole) {
		this.totaleCedole = totaleCedole;
	}
	public String getTotaleDurataCedole() {
		return totaleDurataCedole;
	}
	public void setTotaleDurataCedole(String totaleDurataCedole) {
		this.totaleDurataCedole = totaleDurataCedole;
	}
	public String getImportoContabile() {
		return importoContabile;
	}
	public void setImportoContabile(String importoContabile) {
		this.importoContabile = importoContabile;
	}
	public String getFogli() {
		return fogli;
	}
	public void setFogli(String fogli) {
		this.fogli = fogli;
	}
	public String getGiornate() {
		return giornate;
	}
	public void setGiornate(String giornate) {
		this.giornate = giornate;
	}
	public String getNumeroReversale() {
		return numeroReversale;
	}
	public void setNumeroReversale(String numeroReversale) {
		this.numeroReversale = numeroReversale;
	}
	public String getCodiceComune() {
		return codiceComune;
	}
	public void setCodiceComune(String codiceComune) {
		this.codiceComune = codiceComune;
	}
	public String getCodiceLocale() {
		return codiceLocale;
	}
	public void setCodiceLocale(String codiceLocale) {
		this.codiceLocale = codiceLocale;
	}
	public String getRendicontoContabile() {
		return rendicontoContabile;
	}
	public void setRendicontoContabile(String rendicontoContabile) {
		this.rendicontoContabile = rendicontoContabile;
	}
	public String getSospeso() {
		return sospeso;
	}
	public void setSospeso(String sospeso) {
		this.sospeso = sospeso;
	}
	public String getSenzaMaggiorazione() {
		return senzaMaggiorazione;
	}
	public void setSenzaMaggiorazione(String senzaMaggiorazione) {
		this.senzaMaggiorazione = senzaMaggiorazione;
	}
	public String getNumeroBobina() {
		return numeroBobina;
	}
	public void setNumeroBobina(String numeroBobina) {
		this.numeroBobina = numeroBobina;
	}
	public String getNumeroFotogramma() {
		return numeroFotogramma;
	}
	public void setNumeroFotogramma(String numeroFotogramma) {
		this.numeroFotogramma = numeroFotogramma;
	}
	public String getTipoEvidenza() {
		return tipoEvidenza;
	}
	public void setTipoEvidenza(String tipoEvidenza) {
		this.tipoEvidenza = tipoEvidenza;
	}
	public String getCodificazione() {
		return codificazione;
	}
	public void setCodificazione(String codificazione) {
		this.codificazione = codificazione;
	}
	public String getNumeroPermesso() {
		return numeroPermesso;
	}
	public void setNumeroPermesso(String numeroPermesso) {
		this.numeroPermesso = numeroPermesso;
	}
	public String getTipoValuta() {
		return tipoValuta;
	}
	public void setTipoValuta(String tipoValuta) {
		this.tipoValuta = tipoValuta;
	}
	public String getCodiceOperatore() {
		return codiceOperatore;
	}
	public void setCodiceOperatore(String codiceOperatore) {
		this.codiceOperatore = codiceOperatore;
	}
	public String getIncassoDemLordoLire() {
		return incassoDemLordoLire;
	}
	public void setIncassoDemLordoLire(String incassoDemLordoLire) {
		this.incassoDemLordoLire = incassoDemLordoLire;
	}

}
