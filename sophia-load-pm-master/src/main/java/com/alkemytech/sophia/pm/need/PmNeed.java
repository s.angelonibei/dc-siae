package com.alkemytech.sophia.pm.need;

import java.io.InputStream;
import java.net.ServerSocket;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.sqs.SqsS3EventPump;
import com.alkemytech.sophia.commons.sqs.SqsS3Events;
import com.alkemytech.sophia.pm.McmdbDataSource;
import com.alkemytech.sophia.pm.need.pojo.Foglio;
import com.alkemytech.sophia.pm.need.pojo.PerfNeedRecord;
import com.alkemytech.sophia.pm.need.pojo.Record;
import com.alkemytech.sophia.pm.need.pojo.Utilizzazione;
import com.amazonaws.services.s3.model.S3Object;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Scopes;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class PmNeed {

	private static final Logger logger = LoggerFactory.getLogger(PmNeed.class);

	private static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// amazon service(s)
			bind(S3.class)
				.in(Scopes.SINGLETON);
			bind(SQS.class)
				.in(Scopes.SINGLETON);
			// data source(s)
			bind(DataSource.class)
				.annotatedWith(Names.named("MCMDB"))
				.to(McmdbDataSource.class)
				.in(Scopes.SINGLETON);
			// self
			bind(PmNeed.class).asEagerSingleton();
		}

	}

	public static void main(String[] args) {
		try {
			final Injector injector = Guice
					.createInjector(new GuiceModuleExtension(args, "/sophia-need-pm.properties"));
			final PmNeed instance = injector
					.getInstance(PmNeed.class)
					.startup();
			try {
				instance.process();
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}
	
	private static final int FLAG_QUANDRO_COTABILE = 0x0001;
	private static final int FLAG_DIRETTORE_ESECUZIONE = 0x0002;
	private static final int FLAG_UTILIZZAZIONI = 0x0004;

	private static final int DEADLOCK_RETRIES = 3;
	private static final long DEADLOCK_TIMEOUT = 1000L;

	private final Properties configuration;
	private final SQS sqs;
	private final S3 s3;
	private final DataSource mcmdbDS;
	private final String tipoDocumentoContabile;
	private final String statoPm;

	@Inject
	protected PmNeed(@Named("configuration") Properties configuration,
			S3 s3, SQS sqs, 
			@Named("MCMDB") DataSource mcmdbDS) {
		super();
		this.configuration = configuration;
		this.sqs = sqs;
		this.s3 = s3;
		this.mcmdbDS = mcmdbDS;
		this.tipoDocumentoContabile = configuration
				.getProperty("pmneed.tipo_documento_contabile", "501");
		this.statoPm = configuration
				.getProperty("pmneed.stato_pm", "A");
	}
	
	public PmNeed startup() {
		s3.startup();
		sqs.startup();
		return this;
	}
	
	public PmNeed shutdown() {
		s3.shutdown();
		sqs.shutdown();
		return this;
	}

	public void process() throws Exception {

		final int listenPort = Integer.parseInt(configuration.getProperty("pmneed.bind_port",
				configuration.getProperty("default.bind_port", "0")));
		
		try (final ServerSocket socket = new ServerSocket(listenPort)) {
			logger.info("execute: listening on {}", socket.getLocalSocketAddress());
			
			final SqsS3Events s3Events = new SqsS3Events(SqsS3Events.OBJECT_CRATED);
			final SqsS3EventPump sqsS3EventPump = new SqsS3EventPump(sqs, s3Events, configuration, "pmneed");
			sqsS3EventPump.pollingLoop(new SqsS3EventPump.Consumer() {
				
				@Override
				public void consume(String bucket, String key) {
					try {
						process(bucket, key);
						if (s3.copy(new S3.Url(bucket, key), new S3.Url(bucket, key +
								configuration.getProperty("pmneed.processed_suffix", ".processed")))) {
							s3.delete(new S3.Url(bucket, key));
						}
					} catch (Exception e) {
						logger.error("consume", e);
					}
				}
				
			});
		}
		
	}
	
	private void process(String s3Bucket, String s3Key) throws Exception {
		logger.info("process: s3uri \"s3://{}/{}\"", s3Bucket, s3Key);

		final String filename = s3Key.substring(1 + s3Key.lastIndexOf('/'));
		logger.info("process: filename \"{}\"", filename);

		final boolean skipProcessedFiles = "true"
				.equalsIgnoreCase(configuration.getProperty("pmneed.skip_processed_files", "true"));			
		final boolean skipProcessedRecords = "true"
				.equalsIgnoreCase(configuration.getProperty("pmneed.skip_processed_records", "true"));
		final int skipProcessedRecordsBitmask = Integer
				.parseInt(configuration.getProperty("pmneed.skip_processed_records_bitmask", "ff"), 16);
		final int workerThreads = Integer
				.parseInt(configuration.getProperty("pmneed.worker_threads", "1"));
		final Pattern entryRegex = Pattern.compile(configuration
				.getProperty("pmneed.zip_entry_regex", ".*"));

//		final Gson gson = new GsonBuilder()
//			.setPrettyPrinting()
//			.create();
		final long startTimeMillis = System.currentTimeMillis();

		// connect to database
		try (final Connection connection = mcmdbDS.getConnection()) {
			logger.info("process: connected to {} {}", connection.getMetaData()
					.getDatabaseProductName(), connection.getMetaData().getURL());
			
			// enable auto-commit
			connection.setAutoCommit(true);
			
			// download file
			final S3Object s3Object = s3.getObject(new S3.Url(s3Bucket, s3Key));
			if (null == s3Object) {
				logger.warn("process: unable to download s3uri \"s3://{}/{}\"", s3Bucket, s3Key);
				return;
			}

			// select PERF_NEED_FILE.ID_NEED_FILE
			final NeedDao dao = new NeedDao(connection, true);
			final AtomicLong idNeedFile = new AtomicLong(dao.getIdNeedFile(filename));
			logger.debug("process: idNeedFile {}", idNeedFile);
			if (idNeedFile.get() < 0L) {
				// insert PERF_NEED_FILE
				if (!dao.insertNeedFile(filename, String
						.format("s3://%s/%s", s3Bucket, s3Key))) {
					logger.error("process: errore insert PERF_NEED_FILE");
					return;
				}
				idNeedFile.set(dao.getLastGeneratedKey());
				logger.debug("process: idNeedFile {}", idNeedFile);
			} else if (skipProcessedFiles) {
				logger.info("process: file presente in PERF_NEED_FILE");
				return;
			} else {
				// update PERF_NEED_FILE
				if (!dao.updateNeedFile(idNeedFile.get())) {
					logger.error("process: errore update PERF_NEED_FILE");
					return;
				}
			}

			// stats
			final AtomicLong numeroRecord = new AtomicLong(0L);
			final AtomicLong numeroFogli = new AtomicLong(0L);
			final AtomicLong numeroFogliOk = new AtomicLong(0L);
			final AtomicReference<Exception> exception = new AtomicReference<>(null);

			// unzip and parse file
			try (final InputStream in = s3Object.getObjectContent();
					final ZipInputStream zip = new ZipInputStream(in);
					final NeedReader needReader = new NeedReader(zip)) {
			
				for (ZipEntry entry; null != (entry = zip.getNextEntry()); ) {	
					logger.debug("process: zip entry {} length {} added on {}",
							entry.getName(), entry.getSize(), new Date(entry.getTime()));					

					if (!entryRegex.matcher(entry.getName()).matches()) {
						logger.debug("process: skipping zip entry {}", entry.getName());
						continue;
					}

					// spawn worker thread(s)
					final ExecutorService executor = Executors.newCachedThreadPool();
					final AtomicBoolean interrupted = new AtomicBoolean(false);
					for (int i = 0; i < workerThreads; i ++) {
						executor.execute(new Runnable() {

							@Override
							public void run() {
								try {
									process();
								} catch (Exception e) {
									logger.error("run", e);
									exception.compareAndSet(null, e);
									interrupted.set(true);
								}
							}
							
							private void process() throws Exception {
								
								// dedicated thread connection
								try (final Connection connection = mcmdbDS.getConnection()) {
									logger.info("process: connected to {} {}", connection.getMetaData()
											.getDatabaseProductName(), connection.getMetaData().getURL());

									// dedicated thread dao
									final NeedDao dao = new NeedDao(connection, false);

									// loop on record(s)
									while (!interrupted.get()) {
											
										// read next record from file
										final Record record;
										synchronized (needReader) {
											record = needReader.next();
										}
										if (null == record) {
											return;
										}
										numeroRecord.incrementAndGet();
//										if (logger.isDebugEnabled()) {
//											logger.debug("process: {}", gson.toJson(record));
//										}
										
										// loop on fogli
										for (Foglio foglio : record.getFogli()) {
											
											numeroFogli.incrementAndGet();
											
											// numero programma musicale
											final long numeroProgrammaMusicale;
											try {
												if (null != foglio.getQuadroContabile()) {
													numeroProgrammaMusicale = Long.parseLong(foglio
															.getQuadroContabile().getNumeroProgrammaMusicale());
												} else if (null != foglio.getManifestazione()) {
													numeroProgrammaMusicale = Long.parseLong(foglio
															.getManifestazione().getNumeroProgrammaMusicale());
												} else if (null != foglio.getEsecutore()) {
													numeroProgrammaMusicale = Long.parseLong(foglio
															.getEsecutore().getNumeroProgrammaMusicale());
												} else if (null != foglio.getDirettore1()) {
													numeroProgrammaMusicale = Long.parseLong(foglio
															.getDirettore1().getNumeroProgrammaMusicale());
												} else if (null != foglio.getDirettore2()) {
													numeroProgrammaMusicale = Long.parseLong(foglio
															.getDirettore2().getNumeroProgrammaMusicale());
												} else {
													logger.debug("process: numero programma musicale non trovato");
													continue;
												}
											} catch (NullPointerException | NumberFormatException e) {
												logger.debug("process: numero programma musicale invalido");
												continue;												
											}
//											logger.debug("process: numeroProgrammaMusicale {}", numeroProgrammaMusicale);
											
											// check PERF_NEED_RECORD.NUMERO_LAVORAZIONI
											final PerfNeedRecord perfNeedRecord = dao
													.getPerfNeedRecord(idNeedFile.get(), numeroProgrammaMusicale);
//											logger.debug("process: perNeedRecord {}", perNeedRecord);
											if (skipProcessedRecords && null != perfNeedRecord) {
												if (perfNeedRecord.getNumeroLavorazioni(0) > 0) {
													if (0 == skipProcessedRecordsBitmask ||
															0 != (skipProcessedRecordsBitmask & perfNeedRecord.getEsitoLavorazione(0))) {
														logger.debug("process: skip programma musicale lavorato");
														continue;
													}
												}
											}
											final int numeroLavorazioni = null == perfNeedRecord ? 
													0 : perfNeedRecord.getNumeroLavorazioni(0);
											
											// select PERF_MOVIMENTO_CONTABILE.ID_PROGRAMMA_MUSICALE
											long idProgrammaMusicale = dao.getIdProgrammaMusicale(numeroProgrammaMusicale);
											if (-1L == idProgrammaMusicale) {
												// select PERF_PROGRAMMA_MUSICALE.ID_PROGRAMMA_MUSICALE
												idProgrammaMusicale = dao.getIdProgrammaMusicalePM(numeroProgrammaMusicale);
											}
//											logger.debug("process: idProgrammaMusicale {}", idProgrammaMusicale);
											final boolean existingProgrammaMusicale = idProgrammaMusicale > 0L;
											
											// update PERF_NEED_PM_ID set ID = LAST_INSERT_ID(ID+1)
											if (!existingProgrammaMusicale) {
												idProgrammaMusicale = dao.nextIdProgrammaMusicale();
//												logger.debug("process: idProgrammaMusicale {}", idProgrammaMusicale);
											}
											
											// process foglio
											final int esitoLavorazione = idProgrammaMusicale <= 0L ? 0 :
												processFoglio(connection, dao, foglio, idProgrammaMusicale, numeroLavorazioni, existingProgrammaMusicale);
												
											// enable auto-commit
											connection.setAutoCommit(true);

											// insert/update PERF_NEED_RECORD
											if (null != perfNeedRecord) {
												if (!dao.updateNeedRecord(idNeedFile.get(),
														numeroProgrammaMusicale, esitoLavorazione)) {
													logger.error("process: error update PERF_NEED_RECORD");
												}
											} else if (!dao.insertNeedRecord(idNeedFile.get(),
													numeroProgrammaMusicale, esitoLavorazione)) {
												if (!dao.updateNeedRecord(idNeedFile.get(),
														numeroProgrammaMusicale, esitoLavorazione)) {
													logger.error("process: error insert/update PERF_NEED_RECORD");
												}
											}

											if (0 != esitoLavorazione) {
												numeroFogliOk.incrementAndGet();
											}
																						
										}
										
									}								
								
								}
							
							}
							
						});
					}
					
					// wait worker thread(s)
					executor.shutdown();
					while (!executor.awaitTermination(5L, TimeUnit.SECONDS)) {
						logger.debug("process: numeroRecord {} numeroFogli {} numeroFogliOk {}",
								numeroRecord, numeroFogli, numeroFogliOk);
					}
										
				}
			}
			
			// update PERF_NEED_FILE
			if (!dao.updateNeedFile(idNeedFile.get(), numeroFogli.get(), numeroFogliOk.get(), exception.get())) {
				logger.error("process: errore update PERF_NEED_FILE");
			}

			// re-throw exception
			if (null != exception.get()) {
				throw exception.get();
			}

			// stats
			logger.info("process: numeroRecord {}", numeroRecord);
			logger.info("process: numeroFogli {}", numeroFogli);
			logger.info("process: numeroFogliOk {}", numeroFogliOk);
		}
		
		// timing
		final long elapsedMillis = System.currentTimeMillis() - startTimeMillis;
		final long elapsedSeconds = elapsedMillis / 1000L;
		logger.info("process: completed in {}h {}m {}s {}ms",
				elapsedSeconds / 3600,
				(elapsedSeconds % 3600) / 60,
				(elapsedSeconds % 3600) % 60,
				elapsedMillis % 1000);
	}
	
	private int processFoglio(Connection connection, NeedDao dao, Foglio foglio, long idProgrammaMusicale, int numeroLavorazioni, boolean existingProgrammaMusicale) throws SQLException {
		
		// disable auto-commit
		connection.setAutoCommit(false);
		
		for (int deadlocks = 0; deadlocks < DEADLOCK_RETRIES; ) {
			
			boolean rollback = true;
			try {
				int esitoLavorazione = 0;
				
				// update PERF_PROGRAMMA_MUSICALE
				if (null != foglio.getQuadroContabile()) {
					
					// PERF_PROGRAMMA_MUSICALE.TOTALE_CEDOLE
					int totaleCedole = 0;
					try {
						totaleCedole = Integer.parseInt(foglio.getQuadroContabile().getTotaleCedole());
					} catch (NumberFormatException | NullPointerException e) { }
					if (0 == totaleCedole && null != foglio.getUtilizzazioni()) {
						totaleCedole = foglio.getUtilizzazioni().size();
					}
					foglio.getQuadroContabile()
						.setTotaleCedole(String.format("%03d", totaleCedole));
//						logger.debug("processFoglio: totaleCedole {}", totaleCedole);
					
					// PERF_PROGRAMMA_MUSICALE.TOTALE_DURATA_CEDOLE
					int totaleDurataCedole = dao.toSeconds(foglio.getQuadroContabile().getTotaleDurataCedole());
					if (0 == totaleDurataCedole && null != foglio.getUtilizzazioni()) {
						for (Utilizzazione utilizzazione : foglio.getUtilizzazioni()) {
							totaleDurataCedole += dao.toSeconds(utilizzazione.getDurata());
						}
					}
					foglio.getQuadroContabile().setTotaleDurataCedole(String
							.format("%03d%02d", totaleDurataCedole / 60, totaleDurataCedole % 60));
//						logger.debug("processFoglio: totaleDurataCedole {}", totaleDurataCedole);
					
					if (existingProgrammaMusicale) {
						// update PERF_PROGRAMMA_MUSICALE
						if (!dao.updateProgrammaMusicale(idProgrammaMusicale, foglio.getQuadroContabile())) {
							return 0;
						}
					} else {
						// insert PERF_PROGRAMMA_MUSICALE
						if (!dao.insertProgrammaMusicale(idProgrammaMusicale, foglio.getQuadroContabile(), statoPm)) {
							return 0;
						}
						// insert PERF_MOVIMENTO_CONTABILE
						if (!dao.insertMovimentoContabile(idProgrammaMusicale, foglio.getQuadroContabile(), tipoDocumentoContabile)) {
							return 0;
						}
						
					}
					esitoLavorazione |= FLAG_QUANDRO_COTABILE;
				}

				// insert PERF_UTILIZZAZIONI
				if (null != foglio.getUtilizzazioni() &&
						!foglio.getUtilizzazioni().isEmpty()) {
					if (numeroLavorazioni > 0) {
						dao.deleteUtilizzazioni(idProgrammaMusicale);
					}
					for (Utilizzazione utilizzazione : foglio.getUtilizzazioni()) {
						if (!dao.insertUtilizzazione(idProgrammaMusicale, utilizzazione)) {
							return 0;
						}
						esitoLavorazione |= FLAG_UTILIZZAZIONI;
					}
				}
				
				// insert/update PERF_DIRETTORE_ESECUZIONE
				if (null != foglio.getDirettore1() && null != foglio.getDirettore2()) {
					if (0 == numeroLavorazioni) {
						if (!dao.insertDirettore(idProgrammaMusicale, foglio.getDirettore1(), foglio.getDirettore2())) {
							if (!dao.updateDirettore(idProgrammaMusicale, foglio.getDirettore1(), foglio.getDirettore2())) {
								return 0;
							}
						}
					} else {
						if (!dao.updateDirettore(idProgrammaMusicale, foglio.getDirettore1(), foglio.getDirettore2())) {
							if (!dao.insertDirettore(idProgrammaMusicale, foglio.getDirettore1(), foglio.getDirettore2())) {
								return 0;
							}
						}
					}
					esitoLavorazione |= FLAG_DIRETTORE_ESECUZIONE;
				}
				
				// commit work
				if (0 != esitoLavorazione) {
					connection.commit();	
					rollback = false;
					return esitoLavorazione;
				}
				
			} catch (SQLException e) {
				
				logger.error("processFoglio", e);

				if (NeedDao.ERROR_DEADLOCK == e.getErrorCode()) { // deadlock
					deadlocks ++;
					logger.error("processFoglio: deadlock retry ({} of {})", deadlocks, DEADLOCK_RETRIES);
					if (deadlocks < DEADLOCK_RETRIES) {
						try {
							Thread.sleep(DEADLOCK_TIMEOUT);
						} catch (InterruptedException ignore) {
							logger.error("processFoglio", ignore);
						}
					}
					continue;
				}
				
				return 0;
				
			} finally {
			
				// roll-back work
				if (rollback) {
					try {
						connection.rollback();	
					} catch (SQLException e) {
						logger.error("processFoglio", e);
					}
				}

			}

		}
		
		return 0;

	}
	
}
