package com.alkemytech.sophia.pm.need.pojo;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class Cartella extends NeedPojo {

	// TRK 10
	// [1,2] "10"
	private String cartella;				// [3] "A"
	private String codiceSede;				// [4,5] "03"
	private String codiceProvincia;			// [6,8] "032"
	private String unitaTerritoriale;		// [9,10] "08"
	private String codiceVoceIncasso;		// [11,14] "2222"
	private String progressivoCartella;		// [15,16] "01"
	private String contabilita;				// [17,22] "201608"
	// [23,30] (unused)
	private String totalePm;				// [31,33] "003"
	private String totaleFogli;				// [34,36] "003"
	private String importoParzialeDemLire;	// [37,48] "000000000000"
	private String importoContabile;		// [49,59] "00000026903"
	// [60,76] (unused)
	private String sospeso;					// [77] " "
	private String senzaMaggiorazione;		// [78] " "
	private String numeroBobina;			// [79,83] "15408"
	private String numeroFotogramma;		// [84,88] "05943"
	private String tipoEvidenza;			// [89,90] "  "
	private String codificazione;			// [91,92] "  "
	// [93,180] (unused)
		
	public Cartella(byte[] buffer) {
		super();
		this.cartella = toString(buffer, 2, 1);
		this.codiceSede = toString(buffer, 3, 2);
		this.codiceProvincia = toString(buffer, 5, 3);
		this.unitaTerritoriale = toString(buffer, 8, 2);
		this.codiceVoceIncasso = toString(buffer, 10, 4);
		this.progressivoCartella = toString(buffer, 14, 2);
		this.contabilita = toString(buffer, 16, 6);
		this.totalePm = toString(buffer, 30, 3);
		this.totaleFogli = toString(buffer, 33, 3);
		this.importoParzialeDemLire = toString(buffer, 36, 12);
		this.importoContabile = toString(buffer, 48, 11);
		this.sospeso = toString(buffer, 76, 1);
		this.senzaMaggiorazione = toString(buffer, 77, 1);
		this.numeroBobina = toString(buffer, 78, 5);
		this.numeroFotogramma = toString(buffer, 83, 5);
		this.tipoEvidenza = toString(buffer, 88, 2);
		this.codificazione = toString(buffer, 90, 2);
	}
	
	public String getCartella() {
		return cartella;
	}
	public void setCartella(String cartella) {
		this.cartella = cartella;
	}
	public String getCodiceSede() {
		return codiceSede;
	}
	public void setCodiceSede(String codiceSede) {
		this.codiceSede = codiceSede;
	}
	public String getCodiceProvincia() {
		return codiceProvincia;
	}
	public void setCodiceProvincia(String codiceProvincia) {
		this.codiceProvincia = codiceProvincia;
	}
	public String getUnitaTerritoriale() {
		return unitaTerritoriale;
	}
	public void setUnitaTerritoriale(String unitaTerritoriale) {
		this.unitaTerritoriale = unitaTerritoriale;
	}
	public String getCodiceVoceIncasso() {
		return codiceVoceIncasso;
	}
	public void setCodiceVoceIncasso(String codiceVoceIncasso) {
		this.codiceVoceIncasso = codiceVoceIncasso;
	}
	public String getProgressivoCartella() {
		return progressivoCartella;
	}
	public void setProgressivoCartella(String progressivoCartella) {
		this.progressivoCartella = progressivoCartella;
	}
	public String getContabilita() {
		return contabilita;
	}
	public void setContabilita(String contabilita) {
		this.contabilita = contabilita;
	}
	public String getTotalePm() {
		return totalePm;
	}
	public void setTotalePm(String totalePm) {
		this.totalePm = totalePm;
	}
	public String getTotaleFogli() {
		return totaleFogli;
	}
	public void setTotaleFogli(String totaleFogli) {
		this.totaleFogli = totaleFogli;
	}
	public String getImportoParzialeDemLire() {
		return importoParzialeDemLire;
	}
	public void setImportoParzialeDemLire(String importoParzialeDemLire) {
		this.importoParzialeDemLire = importoParzialeDemLire;
	}
	public String getImportoContabile() {
		return importoContabile;
	}
	public void setImportoContabile(String importoContabile) {
		this.importoContabile = importoContabile;
	}
	public String getSospeso() {
		return sospeso;
	}
	public void setSospeso(String sospeso) {
		this.sospeso = sospeso;
	}
	public String getSenzaMaggiorazione() {
		return senzaMaggiorazione;
	}
	public void setSenzaMaggiorazione(String senzaMaggiorazione) {
		this.senzaMaggiorazione = senzaMaggiorazione;
	}
	public String getNumeroBobina() {
		return numeroBobina;
	}
	public void setNumeroBobina(String numeroBobina) {
		this.numeroBobina = numeroBobina;
	}
	public String getNumeroFotogramma() {
		return numeroFotogramma;
	}
	public void setNumeroFotogramma(String numeroFotogramma) {
		this.numeroFotogramma = numeroFotogramma;
	}
	public String getTipoEvidenza() {
		return tipoEvidenza;
	}
	public void setTipoEvidenza(String tipoEvidenza) {
		this.tipoEvidenza = tipoEvidenza;
	}
	public String getCodificazione() {
		return codificazione;
	}
	public void setCodificazione(String codificazione) {
		this.codificazione = codificazione;
	}
	
}
