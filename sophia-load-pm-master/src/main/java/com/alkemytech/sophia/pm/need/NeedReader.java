package com.alkemytech.sophia.pm.need;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import com.alkemytech.sophia.pm.need.pojo.Associati;
import com.alkemytech.sophia.pm.need.pojo.Cartella;
import com.alkemytech.sophia.pm.need.pojo.Direttore1;
import com.alkemytech.sophia.pm.need.pojo.Direttore2;
import com.alkemytech.sophia.pm.need.pojo.Esecutore;
import com.alkemytech.sophia.pm.need.pojo.Foglio;
import com.alkemytech.sophia.pm.need.pojo.Manifestazione;
import com.alkemytech.sophia.pm.need.pojo.QuadroContabile;
import com.alkemytech.sophia.pm.need.pojo.Record;
import com.alkemytech.sophia.pm.need.pojo.Utilizzazione;


public class NeedReader implements Closeable {

	private final InputStream stream;
	private byte[] buffered;
	
	public NeedReader(InputStream stream) {
		super();
		this.stream = stream;
	}

	@Override
	public void close() throws IOException {
		if (null != stream) {
			stream.close();
		}
	}

	public Record next() throws IOException {
			
		Record record = null;
		Foglio foglio = null;

		final byte[] buffer = new byte[180];
		while (true) {
			if (null == buffered) {
				int length = 0;
				while (-1 != stream.read(buffer, 0, 1)) {
					if (buffer[0] >= '0' && buffer[0] <= '9') {
						for (length = 1; length < 180; ) {
							int count = stream.read(buffer, length, 180 - length);
							if (-1 == count) {
								return record;
							}
							length += count;
						}
						break;
					}
				}
				if (0 == length) {
					return record;
				}
//				System.out.println(new String(buffer, 0, 180));
			} else {
				System.arraycopy(buffered, 0, buffer, 0, 180);
				buffered = null;
//				System.out.println("*" + new String(buffer, 0, 180));
			}
			final int trk = Integer.parseInt(new String(buffer, 0, 2));
			switch (trk) {
			case 10:
				if (null != record && null != record.getCartella()) {
					buffered = buffer;
					return record;
				}
				record = new Record();
				record.setCartella(new Cartella(buffer));
				foglio = null;
				break;
			case 20:
				if (null == foglio || null != foglio.getEsecutore()) {
					foglio = new Foglio();
					record.getFogli().add(foglio);
				}
				foglio.setEsecutore(new Esecutore(buffer));
				break;
			case 30:
				if (null == foglio || null != foglio.getQuadroContabile()) {
					foglio = new Foglio();
					record.getFogli().add(foglio);
				}
				foglio.setQuadroContabile(new QuadroContabile(buffer));
				break;
			case 40:
				if (null == foglio) {
					foglio = new Foglio();
					record.getFogli().add(foglio);
				} else if (null == foglio.getUtilizzazioni()) {
					foglio.setUtilizzazioni(new ArrayList<Utilizzazione>());
				}
				foglio.getUtilizzazioni().add(new Utilizzazione(buffer));
				break;
			case 50:
				if (null == foglio || null != foglio.getManifestazione()) {
					foglio = new Foglio();
					record.getFogli().add(foglio);
				}
				foglio.setManifestazione(new Manifestazione(buffer));
				break;
			case 60:
				if (null == foglio || null != foglio.getDirettore1()) {
					foglio = new Foglio();
					record.getFogli().add(foglio);
				}
				foglio.setDirettore1(new Direttore1(buffer));
				break;
			case 70:
				if (null == foglio || null != foglio.getDirettore2()) {
					foglio = new Foglio();
					record.getFogli().add(foglio);
				}
				foglio.setDirettore2(new Direttore2(buffer));
				break;
			case 90:
				if (null == foglio || null != foglio.getAssociati()) {
					foglio = new Foglio();
					record.getFogli().add(foglio);
				}
				foglio.setAssociati(new Associati(buffer));
				break;
//			default:
//				System.out.println("unhandled TRK " + trk);
//				break;
			}
		}
		
	}
	
}
