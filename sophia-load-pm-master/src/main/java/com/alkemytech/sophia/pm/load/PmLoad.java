package com.alkemytech.sophia.pm.load;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.ServerSocket;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.zip.GZIPInputStream;

import javax.sql.DataSource;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.pm.McmdbDataSource;
import com.amazonaws.services.s3.AmazonS3URI;
import com.amazonaws.services.s3.model.S3Object;
import com.google.common.base.Strings;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Scopes;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

/**
 * @author roberto.cerfogli <roberto@seqrware.com>
 */
public class PmLoad {
	
	private static final Logger logger = LoggerFactory.getLogger(PmLoad.class);

	private static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// amazon service(s)
			bind(S3.class)
				.in(Scopes.SINGLETON);
			// data source(s)
			bind(DataSource.class)
				.annotatedWith(Names.named("MCMDB"))
				.to(McmdbDataSource.class)
				.in(Scopes.SINGLETON);
			// resume db
			bind(ResumeDb.class).asEagerSingleton();
			// self
			bind(PmLoad.class).asEagerSingleton();
		}
		
	}
		
	public static void main(String[] args) {
		try {
			final Injector injector = Guice
					.createInjector(new GuiceModuleExtension(args, "/sophia-load-pm.properties"));
			final PmLoad instance = injector
					.getInstance(PmLoad.class)
					.startup();
			try {
				instance.process();
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}
	
	
	private final Properties configuration;
	private final S3 s3;
	private final DataSource mcmdbDS;
	private final ResumeDb resumeDb;

	@Inject
	protected PmLoad(@Named("configuration") Properties configuration,
			S3 s3, ResumeDb resumeDb,
			@Named("MCMDB") DataSource mcmdbDS) {
		super();
		this.configuration = configuration;
		this.s3 = s3;
		this.resumeDb = resumeDb;
		this.mcmdbDS = mcmdbDS;
	}
	
	public PmLoad startup() {
		s3.startup();
		return this;
	}

	public PmLoad shutdown() {
		s3.shutdown();
		return this;
	}

	private long loadLastInsertedRecord(String year, String month, String entity) {
		final Map<String, Long> resumeMap = resumeDb.load();
		final Long lastInsertedRecord = resumeMap.get(resumeDb.keyFor(year, month, entity));
		return null == lastInsertedRecord ? 0L : lastInsertedRecord.longValue();
	}

	private void saveLastInsertedRecord(String year, String month, String entity, long lastInsertedRecord) {
		final Map<String, Long> resumeMap = resumeDb.load();
		resumeMap.put(resumeDb.keyFor(year, month, entity), lastInsertedRecord);
		resumeDb.save(resumeMap);
	}

	public void process() throws Exception {
		final int bindPort = Integer.parseInt(configuration.getProperty("pmload.bind_port",
				configuration.getProperty("default.bind_port", "0")));
		try (final ServerSocket socket = new ServerSocket(bindPort)) {
			logger.info("load: listening on {}", socket.getLocalSocketAddress());
			// format fake message
			final Calendar calendar = Calendar.getInstance();
			final JsonObject inputJsonBody = new JsonObject();
			final JsonObject outputJson = new JsonObject();
			final int year = Integer.parseInt(configuration
					.getProperty("pmload.local.year", Integer.toString(calendar.get(Calendar.YEAR))));
			final int month = Integer.parseInt(configuration
					.getProperty("pmload.local.month", Integer.toString(1 + calendar.get(Calendar.MONTH))));
			inputJsonBody.addProperty("year", year);
			inputJsonBody.addProperty("month", month);
			inputJsonBody.addProperty("bucket", configuration
					.getProperty("pmload.local.s3.bucket"));
			final String s3folder = configuration
					.getProperty("pmload.local.s3.path") + String.format("/%04d_%02d", year, month);
			inputJsonBody.addProperty("folder", s3folder);
			final JsonArray entities = new JsonArray();
			for (String entity : configuration.getProperty("pmload.local.entities").split(",")) {
				final JsonObject object = new JsonObject();
				object.addProperty("entity", entity);
				final String filename = entity + ".csv.gz";
				object.addProperty("filename", filename);
				object.addProperty("url", "s3://" + configuration
						.getProperty("pmload.local.s3.bucket") + "/" + s3folder + "/" + filename);
				object.addProperty("rows", 0);
				entities.add(object);
			}
			inputJsonBody.add("entities", entities);
			// process message
			processMessage(inputJsonBody, outputJson);
		}
	}

	public boolean isValidMessageBody(JsonObject inputJsonBody) {
		final String year = GsonUtils.getAsString(inputJsonBody, "year");
		final String month = GsonUtils.getAsString(inputJsonBody, "month");
		final JsonArray entities = inputJsonBody.getAsJsonArray("entities");
		if (Strings.isNullOrEmpty(year) || Strings.isNullOrEmpty(month) || 
				null == entities || entities.isJsonNull() || entities.size() < 1) {
			return false;
		}
		return true;
	}
	
	public void processMessage(JsonObject inputJsonBody, JsonObject outputJson) throws Exception {

		// parse input json
		logger.debug("inputJsonBody {}", inputJsonBody);
		final String year = GsonUtils.getAsString(inputJsonBody, "year");
		final String month = GsonUtils.getAsString(inputJsonBody, "month");
		final JsonArray entities = inputJsonBody.getAsJsonArray("entities");
		if (null == entities || entities.isJsonNull() || entities.size() < 1) {
			throw new IllegalArgumentException("missing mandatory parameter(s)");
		}
		final JsonArray results = new JsonArray();
		for (JsonElement element : entities) {
			final JsonObject object = element.getAsJsonObject();
			final String entity = GsonUtils.getAsString(object, "entity");
			final String url = GsonUtils.getAsString(object, "url");
			if (Strings.isNullOrEmpty(entity) || 
					Strings.isNullOrEmpty(url)) {
				throw new IllegalArgumentException("missing mandatory parameter(s)");
			}
			// download and insert
			final JsonObject result = downloadAndInsert(year, month, entity, url);
			results.add(result);
		}
		outputJson.add("pmLoadResults", results);
		logger.debug("outputJson {}", outputJson);

	}
	
	private JsonObject downloadAndInsert(String year, String month, final String entity, String url) throws Exception {
		final char delimiter = configuration.getProperty("csv.separator.field", ",").charAt(0);
		final char recordSeparator = configuration.getProperty("csv.separator.line", "\n").charAt(0);
		final boolean ignoreDuplicateKey = "true".equalsIgnoreCase(configuration
				.getProperty("pmload.ignore_duplicate_key", "true"));
		final boolean ignoreSqlException = "true".equalsIgnoreCase(configuration
				.getProperty("pmload.ignore_sql_exception"));
		final int workerThreads = Integer.parseInt(configuration
				.getProperty("pmload.worker_threads", "1"));
//		final Pattern isint = Pattern.compile("\\d+");
//		final Pattern isnum = Pattern.compile("\\d+\\.?(\\d+)?");
//		final Pattern isdate = Pattern.compile("[0-9]{4}\\-[0-9]{2}\\-[0-9]{2}\\s[0-9]{2}\\-:[0-9]{2}\\:[0-9]{2}");

		final AmazonS3URI s3uri = new AmazonS3URI(url);
		logger.info("downloadAndInsert: s3uri {}", s3uri);
		final S3Object s3obj = s3.getObject(new S3.Url(s3uri.getBucket(), s3uri.getKey()));
		if (null == s3obj) {
			throw new IOException("unable to download " + s3uri);
		}
		
		// stats
		final AtomicInteger rownum = new AtomicInteger(0);
		final AtomicInteger skipped = new AtomicInteger(0);
		final AtomicInteger inserted = new AtomicInteger(0);
		final AtomicInteger duplicateKeys = new AtomicInteger(0);
		final AtomicInteger sqlExceptions = new AtomicInteger(0);
		
		try (final InputStream in = s3obj.getObjectContent();
				final GZIPInputStream gzin = new GZIPInputStream(in);
				final InputStreamReader reader = new InputStreamReader(gzin, StandardCharsets.UTF_8);
				final CSVParser parser = new CSVParser(reader, CSVFormat.DEFAULT.withIgnoreSurroundingSpaces()
						.withDelimiter(delimiter).withRecordSeparator(recordSeparator))) {
			
			final Iterator<CSVRecord> records = parser.iterator();
			// prepare statement (using column names from first row)
			final CSVRecord columnNames = records.next();
			final StringBuilder sql = new StringBuilder()
				.append("insert into ")
				.append(entity)
				.append("(")
				.append(columnNames.get(0));
			for (int i = 1; i < columnNames.size(); i ++) {
				sql.append(",")
					.append(columnNames.get(i));
			}
			sql.append(") values (?");
			for (int i = 1; i < columnNames.size(); i ++) {
				sql.append(", ?");
			}
			sql.append(")");
			logger.debug("downloadAndInsert: sql {}", sql);
			
			// column types (from second row)
			final CSVRecord columnTypes = records.next();
			final int[] sqlTypes = new int[columnTypes.size()];
			for (int i = 0; i < sqlTypes.length; i ++) {
				sqlTypes[i] = Integer.parseInt(columnTypes.get(i));
			}

			// skip already inserted records
			final long lastInsertedRecord = loadLastInsertedRecord(year, month, entity);
			logger.info("downloadAndInsert: lastInsertedRecord {}", lastInsertedRecord);
			for (long i = 0L; i < lastInsertedRecord && records.hasNext(); i ++) {
				records.next();
				rownum.incrementAndGet();
				skipped.incrementAndGet();
			}
			
			// spawn worker thread(s)
			final AtomicReference<Exception> exception = new AtomicReference<>(null);
			if (records.hasNext()) {
				final AtomicBoolean interrupted = new AtomicBoolean(false);
				final ExecutorService executor = Executors.newCachedThreadPool();
				for (int i = 0; i < workerThreads; i ++) {
					executor.execute(new Runnable() {

						@Override
						public void run() {
							try {
								process();
							} catch (Exception e) {
								logger.error("run", e);
								exception.compareAndSet(null, e);
								interrupted.set(true);
							}
						}
						
						private void process() throws Exception {
							// dedicated thread connection
							try (final Connection connection = mcmdbDS.getConnection()) {
								logger.info("process: connected to {} {}", connection.getMetaData()
										.getDatabaseProductName(), connection.getMetaData().getURL());
								connection.setAutoCommit(true);
								final PreparedStatement stmt = connection.prepareStatement(sql.toString());
								final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

								// loop on record(s)
								while (!interrupted.get()) {
									
									// read next record
									final CSVRecord record;
									synchronized (records) {
										record = records.hasNext() ? records.next() : null;
									}
									if (null == record) {
										return;
									}
									rownum.incrementAndGet();

									// prepare statement
									try {
										int i = 1;
										for (String value : record) {
											if (Strings.isNullOrEmpty(value)) {
												stmt.setNull(i, sqlTypes[i-1]);							
											} else {
												switch (sqlTypes[i-1]) {
												case Types.DATE:
													stmt.setDate(i, new java.sql.Date(format.parse(value).getTime()));
													break;
												case Types.TIMESTAMP:
													stmt.setTimestamp(i, new java.sql.Timestamp(format.parse(value).getTime()));
													break;
												case Types.NUMERIC:
													stmt.setBigDecimal(i, new BigDecimal(value));
													break;
												case Types.BOOLEAN:
												case Types.BIT:
												case Types.TINYINT:
												case Types.SMALLINT:
												case Types.INTEGER:
												case Types.BIGINT:
													stmt.setInt(i, Integer.parseInt(value));
													break;
												case Types.CHAR:
												case Types.VARCHAR:
													stmt.setString(i, value);
													break;
												}							
											}
											i ++;
//											if (StringTools.isNullOrEmpty(value)) {
//												stmt.setNull(i, sqlTypes[i - 1]);							
//											} else if (isint.matcher(value).matches()) {
//												stmt.setInt(i, Integer.parseInt(value));
//											} else if (isnum.matcher(value).matches()) {
//												stmt.setBigDecimal(i, new BigDecimal(value));
//											} else if (isdate.matcher(value).matches()) {
//												stmt.setDate(i, new java.sql.Date(format.parse(value).getTime()));
//											} else {
//												stmt.setString(i, value);
//											}
										}
									} catch (Exception e) {
										logger.error("process: malformed record {}", record);
										throw e;									
									}
									
									// insert
									try {
										if (1 == stmt.executeUpdate()) {
											inserted.incrementAndGet();
										}
									} catch (SQLException e) {
										if (1062 == e.getErrorCode()) {
											if (!ignoreDuplicateKey) {
												logger.error("process: bad record {}", record);
												throw e;
											}
									        // ignore duplicate primary key
											duplicateKeys.incrementAndGet();
									    } else {
											if (!ignoreSqlException) {
												logger.error("process: bad record {}", record);
												throw e;
											}
									        // ignore duplicate primary key
											sqlExceptions.incrementAndGet();
									    }
									}

								}
							}
						}

					});
				}
				
				// wait worker thread(s)
				executor.shutdown();
				while (!executor.awaitTermination(5L, TimeUnit.SECONDS)) {
					logger.debug("downloadAndInsert: {} rownum {} inserted {}", entity, rownum, inserted);
				}
			}
			
			// update resume db
			saveLastInsertedRecord(year, month, entity, rownum.longValue());
			
			// re-throw exception
			if (null != exception.get()) {
				throw exception.get();
			}
			
			logger.debug("downloadAndInsert: {} rownum {}", entity, rownum);			
			logger.debug("downloadAndInsert: {} skipped {}", entity, skipped);			
			logger.debug("downloadAndInsert: {} inserted {}", entity, inserted);			
			logger.debug("downloadAndInsert: {} duplicateKeys {}", entity, duplicateKeys);			
			logger.debug("downloadAndInsert: {} sqlExceptions {}", entity, sqlExceptions);			
			
			// stats json object
			final JsonObject jsonStats = new JsonObject();
			jsonStats.addProperty("entity", entity);
			jsonStats.addProperty("total_rows", rownum.get());
			jsonStats.addProperty("skipped_rows", skipped.get());
			jsonStats.addProperty("inserted_rows", inserted.get());
			jsonStats.addProperty("duplicate_keys", duplicateKeys.get());
			jsonStats.addProperty("sql_exceptions", sqlExceptions.get());
			return jsonStats;
		}
	}
	
}




