package com.alkemytech.sophia.pm.recording.pojo;

import java.util.Date;

public class PerfRecordingFile {

	private Long idRecordingFile;
	private String fileName;
	private String fileType;
	private String s3Path;
	private Date ultimaLavorazione;
	private Integer numeroLavorazioni;
	private Long numeroEventi;
	private Long numeroUtilizzazioni;
	private String esitoLavorazione;
	private String erroreLavorazione;
	
	public Long getIdRecordingFile() {
		return idRecordingFile;
	}
	public PerfRecordingFile setIdRecordingFile(Long idRecordingFile) {
		this.idRecordingFile = idRecordingFile;
		return this;
	}
	public String getFileName() {
		return fileName;
	}
	public PerfRecordingFile setFileName(String fileName) {
		this.fileName = fileName;
		return this;
	}
	public String getFileType() {
		return fileType;
	}
	public PerfRecordingFile setFileType(String fileType) {
		this.fileType = fileType;
		return this;
	}
	public String getS3Path() {
		return s3Path;
	}
	public PerfRecordingFile setS3Path(String s3Path) {
		this.s3Path = s3Path;
		return this;
	}
	public Date getUltimaLavorazione() {
		return ultimaLavorazione;
	}
	public PerfRecordingFile setUltimaLavorazione(Date ultimaLavorazione) {
		this.ultimaLavorazione = ultimaLavorazione;
		return this;
	}
	public Integer getNumeroLavorazioni() {
		return numeroLavorazioni;
	}
	public PerfRecordingFile setNumeroLavorazioni(Integer numeroLavorazioni) {
		this.numeroLavorazioni = numeroLavorazioni;
		return this;
	}
	public Long getNumeroEventi() {
		return numeroEventi;
	}
	public PerfRecordingFile setNumeroEventi(Long numeroEventi) {
		this.numeroEventi = numeroEventi;
		return this;
	}
	public Long getNumeroUtilizzazioni() {
		return numeroUtilizzazioni;
	}
	public PerfRecordingFile setNumeroUtilizzazioni(Long numeroUtilizzazioni) {
		this.numeroUtilizzazioni = numeroUtilizzazioni;
		return this;
	}
	public String getEsitoLavorazione() {
		return esitoLavorazione;
	}
	public PerfRecordingFile setEsitoLavorazione(String esitoLavorazione) {
		this.esitoLavorazione = esitoLavorazione;
		return this;
	}
	public String getErroreLavorazione() {
		return erroreLavorazione;
	}
	public PerfRecordingFile setErroreLavorazione(String erroreLavorazione) {
		this.erroreLavorazione = erroreLavorazione;
		return this;
	}
	
}
