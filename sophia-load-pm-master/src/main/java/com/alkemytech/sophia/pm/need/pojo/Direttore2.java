package com.alkemytech.sophia.pm.need.pojo;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class Direttore2 extends NeedPojo {

	// TRK 70
	// [1,2] "70"
//	private String cartella;				// [3] "A"
//	private String codiceSede;				// [4,5] "03"
//	private String codiceProvincia;			// [6,8] "032"
//	private String unitaTerritoriale;		// [9,10] "08"
//	private String codiceVoceIncasso;		// [11,14] "2222"
//	private String progressivoCartella;		// [15,16] "01"
//	private String contabilita;				// [17,22] "201608"
	private String numeroProgrammaMusicale;	// [23,30] "90195603"
	private String codiceFiscale;			// [31,46] "BTTLRS83C26L885J"
	private String sesso;					// [47] "M"
	private String comuneNascita;			// [48,72] "VIGNOLA                  "
	private String provinciaNascita;		// [73,74] "MO"
	private String dataNascita;				// [75,82] "26031983"
	private String noFirma;					// [83] "0"
	private String siFirmaPrivacy;			// [84] "1"
	// [85,180] (unused)

	public Direttore2(byte[] buffer) {
		super();	
//		this.cartella = toString(buffer, 2, 1);
//		this.codiceSede = toString(buffer, 3, 2);
//		this.codiceProvincia = toString(buffer, 5, 3);
//		this.unitaTerritoriale = toString(buffer, 8, 2);
//		this.codiceVoceIncasso = toString(buffer, 10, 4);
//		this.progressivoCartella = toString(buffer, 14, 2);
//		this.contabilita = toString(buffer, 16, 6);
		this.numeroProgrammaMusicale = toString(buffer, 22, 8);
		this.codiceFiscale = toString(buffer, 30, 16);
		this.sesso = toString(buffer, 46, 1);
		this.comuneNascita = toString(buffer, 47, 25);
		this.provinciaNascita = toString(buffer, 72, 2);
		this.dataNascita = toString(buffer, 74, 8);
		this.noFirma = toString(buffer, 82, 1);
		this.siFirmaPrivacy = toString(buffer, 83, 1);
	}
	
	public String getNumeroProgrammaMusicale() {
		return numeroProgrammaMusicale;
	}
	public void setNumeroProgrammaMusicale(String numeroProgrammaMusicale) {
		this.numeroProgrammaMusicale = numeroProgrammaMusicale;
	}
	public String getCodiceFiscale() {
		return codiceFiscale;
	}
	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}
	public String getSesso() {
		return sesso;
	}
	public void setSesso(String sesso) {
		this.sesso = sesso;
	}
	public String getComuneNascita() {
		return comuneNascita;
	}
	public void setComuneNascita(String comuneNascita) {
		this.comuneNascita = comuneNascita;
	}
	public String getProvinciaNascita() {
		return provinciaNascita;
	}
	public void setProvinciaNascita(String provinciaNascita) {
		this.provinciaNascita = provinciaNascita;
	}
	public String getDataNascita() {
		return dataNascita;
	}
	public void setDataNascita(String dataNascita) {
		this.dataNascita = dataNascita;
	}
	public String getNoFirma() {
		return noFirma;
	}
	public void setNoFirma(String noFirma) {
		this.noFirma = noFirma;
	}
	public String getSiFirmaPrivacy() {
		return siFirmaPrivacy;
	}
	public void setSiFirmaPrivacy(String siFirmaPrivacy) {
		this.siFirmaPrivacy = siFirmaPrivacy;
	}
	
}
