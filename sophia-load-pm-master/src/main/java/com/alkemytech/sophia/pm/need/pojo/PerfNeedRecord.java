package com.alkemytech.sophia.pm.need.pojo;

import java.util.Date;

public class PerfNeedRecord {
	
	private Long idNeedFile;
	private Long numeroProgrammaMusicale;
	private Date ultimaLavorazione;
	private Integer numeroLavorazioni;
	private Integer esitoLavorazione;
	
	public Long getIdNeedFile() {
		return idNeedFile;
	}
	public PerfNeedRecord setIdNeedFile(Long idNeedFile) {
		this.idNeedFile = idNeedFile;
		return this;
	}
	public Long getNumeroProgrammaMusicale() {
		return numeroProgrammaMusicale;
	}
	public PerfNeedRecord setNumeroProgrammaMusicale(Long numeroProgrammaMusicale) {
		this.numeroProgrammaMusicale = numeroProgrammaMusicale;
		return this;
	}
	public Date getUltimaLavorazione() {
		return ultimaLavorazione;
	}
	public PerfNeedRecord setUltimaLavorazione(Date ultimaLavorazione) {
		this.ultimaLavorazione = ultimaLavorazione;
		return this;
	}
	public Integer getNumeroLavorazioni(int ifNull) {
		return null == numeroLavorazioni ? ifNull : numeroLavorazioni.intValue();
	}
	public PerfNeedRecord setNumeroLavorazioni(Integer numeroLavorazioni) {
		this.numeroLavorazioni = numeroLavorazioni;
		return this;
	}
	public Integer getEsitoLavorazione(int ifNull) {
		return null == esitoLavorazione ? ifNull : esitoLavorazione.intValue();
	}
	public PerfNeedRecord setEsitoLavorazione(Integer esitoLavorazione) {
		this.esitoLavorazione = esitoLavorazione;
		return this;
	}
	
}
