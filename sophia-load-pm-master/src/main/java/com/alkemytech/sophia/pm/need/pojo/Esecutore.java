package com.alkemytech.sophia.pm.need.pojo;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class Esecutore extends NeedPojo {

	// TRK 20
	// [1,2] "20"
//	private String cartella;				// [3] "A"
//	private String codiceSede;				// [4,5] "03"
//	private String codiceProvincia;			// [6,8] "032"
//	private String unitaTerritoriale;		// [9,10] "08"
//	private String codiceVoceIncasso;		// [11,14] "2222"
//	private String progressivoCartella;		// [15,16] "01"
//	private String contabilita;				// [17,22] "201608"
	private String numeroProgrammaMusicale;	// [23,30] "90195603"
	private String denominazioneComplesso;	// [31,55] "SOS MUSICISTI            "
	private String codiceFiscale;			// [56,71] "     03382350365"
	private String indirizzo;				// [72,101] "VIA MAVORE 41 5               "
	private String cap;						// [102,106] "41015"
	private String comune;					// [107,126] "NONANTOLA           "
	private String provincia;				// [127,128] "MO"
	// [129,180] (unused)
	
	public Esecutore(byte[] buffer) {
		super();
//		this.cartella = toString(buffer, 2, 1);
//		this.codiceSede = toString(buffer, 3, 2);
//		this.codiceProvincia = toString(buffer, 5, 3);
//		this.unitaTerritoriale = toString(buffer, 8, 2);
//		this.codiceVoceIncasso = toString(buffer, 10, 4);
//		this.progressivoCartella = toString(buffer, 14, 2);
//		this.contabilita = toString(buffer, 16, 6);
		this.numeroProgrammaMusicale = toString(buffer, 22, 8);
		this.denominazioneComplesso = toString(buffer, 30, 25);
		this.codiceFiscale = toString(buffer, 55, 16);
		this.indirizzo = toString(buffer, 71, 30);
		this.cap = toString(buffer, 101, 5);
		this.comune = toString(buffer, 106, 20);
		this.provincia = toString(buffer, 126, 2);
	}
	
	public String getNumeroProgrammaMusicale() {
		return numeroProgrammaMusicale;
	}
	public void setNumeroProgrammaMusicale(String numeroProgrammaMusicale) {
		this.numeroProgrammaMusicale = numeroProgrammaMusicale;
	}
	public String getDenominazioneComplesso() {
		return denominazioneComplesso;
	}
	public void setDenominazioneComplesso(String denominazioneComplesso) {
		this.denominazioneComplesso = denominazioneComplesso;
	}
	public String getCodiceFiscale() {
		return codiceFiscale;
	}
	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}
	public String getIndirizzo() {
		return indirizzo;
	}
	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}
	public String getCap() {
		return cap;
	}
	public void setCap(String cap) {
		this.cap = cap;
	}
	public String getComune() {
		return comune;
	}
	public void setComune(String comune) {
		this.comune = comune;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	
}
