package com.alkemytech.sophia.pm.need.pojo;

import java.util.Date;

public class PerfNeedFile {

	private Long idNeedFile;
	private String fileName;
	private String s3Path;
	private Date ultimaLavorazione;
	private Integer numeroLavorazioni;
	private Long numeroRecord;
	private Long numeroRecordOk;
	private String esitoLavorazione;
	private String erroreLavorazione;
	
	public Long getIdNeedFile() {
		return idNeedFile;
	}
	public PerfNeedFile setIdNeedFile(Long idNeedFile) {
		this.idNeedFile = idNeedFile;
		return this;
	}
	public String getFileName() {
		return fileName;
	}
	public PerfNeedFile setFileName(String fileName) {
		this.fileName = fileName;
		return this;
	}
	public String getS3Path() {
		return s3Path;
	}
	public PerfNeedFile setS3Path(String s3Path) {
		this.s3Path = s3Path;
		return this;
	}
	public Date getUltimaLavorazione() {
		return ultimaLavorazione;
	}
	public PerfNeedFile setUltimaLavorazione(Date ultimaLavorazione) {
		this.ultimaLavorazione = ultimaLavorazione;
		return this;
	}
	public Integer getNumeroLavorazioni() {
		return numeroLavorazioni;
	}
	public PerfNeedFile setNumeroLavorazioni(Integer numeroLavorazioni) {
		this.numeroLavorazioni = numeroLavorazioni;
		return this;
	}
	public Long getNumeroRecord() {
		return numeroRecord;
	}
	public PerfNeedFile setNumeroRecord(Long numeroRecord) {
		this.numeroRecord = numeroRecord;
		return this;
	}
	public Long getNumeroRecordOk() {
		return numeroRecordOk;
	}
	public PerfNeedFile setNumeroRecordOk(Long numeroRecordOk) {
		this.numeroRecordOk = numeroRecordOk;
		return this;
	}
	public String getEsitoLavorazione() {
		return esitoLavorazione;
	}
	public PerfNeedFile setEsitoLavorazione(String esitoLavorazione) {
		this.esitoLavorazione = esitoLavorazione;
		return this;
	}
	public String getErroreLavorazione() {
		return erroreLavorazione;
	}
	public PerfNeedFile setErroreLavorazione(String erroreLavorazione) {
		this.erroreLavorazione = erroreLavorazione;
		return this;
	}
	
}
