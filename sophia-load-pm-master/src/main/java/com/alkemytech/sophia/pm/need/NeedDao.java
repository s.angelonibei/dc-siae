package com.alkemytech.sophia.pm.need;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.pm.BasicDao;
import com.alkemytech.sophia.pm.need.pojo.Direttore1;
import com.alkemytech.sophia.pm.need.pojo.Direttore2;
import com.alkemytech.sophia.pm.need.pojo.PerfNeedRecord;
import com.alkemytech.sophia.pm.need.pojo.QuadroContabile;
import com.alkemytech.sophia.pm.need.pojo.Utilizzazione;

/*

 DROP TABLE PERF_NEED_FILE;
 CREATE TABLE PERF_NEED_FILE
 (
    ID_NEED_FILE        BIGINT          NOT NULL AUTO_INCREMENT,
    FILE_NAME           VARCHAR(100)    NOT NULL,
    S3_PATH             VARCHAR(1000)   NOT NULL,
    ULTIMA_LAVORAZIONE  DATETIME        NOT NULL,
    NUMERO_LAVORAZIONI  INT             NOT NULL,
    NUMERO_RECORD       BIGINT,
    NUMERO_RECORD_OK    BIGINT,
    ESITO_LAVORAZIONE   VARCHAR(100),
    ERRORE_LAVORAZIONE  TEXT,
    PRIMARY KEY (ID_NEED_FILE)
 );
 
 CREATE UNIQUE INDEX PERF_NEED_FILE_IDX_01
   ON PERF_NEED_FILE (FILE_NAME ASC);
 
 DROP TABLE PERF_NEED_RECORD;
 CREATE TABLE PERF_NEED_RECORD
 (
    ID_NEED_FILE               BIGINT     NOT NULL,
    NUMERO_PROGRAMMA_MUSICALE  BIGINT     NOT NULL,
    ULTIMA_LAVORAZIONE         DATETIME   NOT NULL,
    NUMERO_LAVORAZIONI         INT        NOT NULL,
    ESITO_LAVORAZIONE          INT        NOT NULL,
    PRIMARY KEY (ID_NEED_FILE, NUMERO_PROGRAMMA_MUSICALE)
 );
 
 DROP TABLE PERF_NEED_PM_ID;
 CREATE TABLE PERF_NEED_PM_ID (ID BIGINT NOT NULL);
 INSERT INTO PERF_NEED_PM_ID VALUES (99000000);
 
 */
public class NeedDao extends BasicDao {

	private static final Logger logger = LoggerFactory.getLogger(NeedDao.class);
	
	private final Connection connection;

	private boolean enableGeneratedKeys;	
	private long lastGeneratedKey;

	public NeedDao(Connection connection) {
		this(connection, true);
	}

	public NeedDao(Connection connection, boolean enableGeneratedKeys) {
		super();
		this.connection = connection;
		this.enableGeneratedKeys = enableGeneratedKeys;
	}
	
	public boolean isEnableGeneratedKeys() {
		return enableGeneratedKeys;
	}

	public void setEnableGeneratedKeys(boolean enableGeneratedKeys) {
		this.enableGeneratedKeys = enableGeneratedKeys;
	}

	public long getLastGeneratedKey() {
		if (enableGeneratedKeys) {
			return lastGeneratedKey;
		}
		throw new IllegalStateException("generated PKs not enabled");
	}
	
	private void setLastGeneratedKey(Statement stmt) throws SQLException {
		lastGeneratedKey = 0L;
		try (final ResultSet rset = stmt.getGeneratedKeys()) {
			if (rset.next())
				lastGeneratedKey = rset.getLong(1);
		}
	}
	
	public long getIdNeedFile(String filename) throws SQLException {
		final StringBuilder sql = new StringBuilder()
			.append(" select ID_NEED_FILE ")
			.append("   from PERF_NEED_FILE ")
			.append("  where FILE_NAME = ")
			.append(toVarchar(filename));
		try (final Statement stmt = connection.createStatement()) {
			try (final ResultSet rset = stmt.executeQuery(sql.toString())) {
				if (rset.next())
					return rset.getLong(1);
			}
		}
		return -1L;
	}
	
	/*
	 * UPDATE PERF_NEED_PM_ID SET ID = LAST_INSERT_ID(ID+1);
	 * SELECT LAST_INSERT_ID();
	 */
	public long nextIdProgrammaMusicale() throws SQLException {		
		try (final Statement stmt = connection.createStatement()) {
			if (1 == stmt.executeUpdate("UPDATE PERF_NEED_PM_ID SET ID = LAST_INSERT_ID(ID+1)")) {
				try (final ResultSet rset = stmt.executeQuery("SELECT LAST_INSERT_ID()")) {
					if (rset.next())
						return rset.getLong(1);
				}	
			}
		}
		return -1L;
	}
	
	public long getIdProgrammaMusicale(long numeroProgrammaMusicale) throws SQLException {
		final StringBuilder sql = new StringBuilder()
			.append(" select ID_PROGRAMMA_MUSICALE ")
			.append(" from PERF_MOVIMENTO_CONTABILE ")
			.append(" where NUMERO_PM = ")
			.append(numeroProgrammaMusicale);
		try (final Statement stmt = connection.createStatement()) {
			try (final ResultSet rset = stmt.executeQuery(sql.toString())) {
				if (rset.next())
					return rset.getLong(1);
			}
		}
		return -1L;
	}
	
	public long getIdProgrammaMusicalePM(long numeroProgrammaMusicale) throws SQLException {
		final StringBuilder sql = new StringBuilder()
			.append(" select ID_PROGRAMMA_MUSICALE ")
			.append(" from PERF_PROGRAMMA_MUSICALE ")
			.append(" where NUMERO_PROGRAMMA_MUSICALE = ")
			.append(numeroProgrammaMusicale);
		try (final Statement stmt = connection.createStatement()) {
			try (final ResultSet rset = stmt.executeQuery(sql.toString())) {
				if (rset.next())
					return rset.getLong(1);
			}
		}
		return -1L;
	}

	public PerfNeedRecord getPerfNeedRecord(long idNeedFile, long numeroProgrammaMusicale) throws SQLException {
		final StringBuilder sql = new StringBuilder()
			.append(" select ULTIMA_LAVORAZIONE ")
			.append(" , NUMERO_LAVORAZIONI ")
			.append(" , ESITO_LAVORAZIONE ")
			.append(" from PERF_NEED_RECORD ")
			.append(" where ID_NEED_FILE = ").append(idNeedFile)
			.append(" and NUMERO_PROGRAMMA_MUSICALE = ").append(numeroProgrammaMusicale);
		try (final Statement stmt = connection.createStatement()) {
			try (final ResultSet rset = stmt.executeQuery(sql.toString())) {
				if (rset.next()) {
					int i = 1;
					return new PerfNeedRecord()
						.setIdNeedFile(idNeedFile)
						.setNumeroProgrammaMusicale(numeroProgrammaMusicale)
						.setUltimaLavorazione(getAsDate(rset, i++))
						.setNumeroLavorazioni(getAsInteger(rset, i++))
						.setEsitoLavorazione(getAsInteger(rset, i++));
				}
			}
		}
		return null;
	}
	
	public boolean insertNeedFile(String filename, String s3path) throws SQLException {
		final StringBuilder sql = new StringBuilder()
			.append(" insert into PERF_NEED_FILE ")
			.append(" ( FILE_NAME ")
			.append(" , S3_PATH ")
			.append(" , ULTIMA_LAVORAZIONE ")
			.append(" , NUMERO_LAVORAZIONI ")
			.append(" ) values ")
			.append(" ( ").append(toVarchar(filename))
			.append(" , ").append(toVarchar(s3path))
			.append(" , now() ")
			.append(" , 1 ")
			.append(" ) ");
		try (final Statement stmt = connection.createStatement()) {
			if (enableGeneratedKeys) {
				if (1 == stmt.executeUpdate(sql.toString(), Statement.RETURN_GENERATED_KEYS)) {
					setLastGeneratedKey(stmt);
					return true;
				}
			} else if (1 == stmt.executeUpdate(sql.toString())) {
				return true;
			}
		} catch (SQLException e) {
			if (ERROR_DUPLICATE_KEY != e.getErrorCode()) {
				logger.error("insertNeedFile: {}", sql);
				throw e;
		    }
		}
		return false;
	}
	
	public boolean updateNeedFile(long idNeedFile) throws SQLException {
		final StringBuilder sql = new StringBuilder()
			.append(" update PERF_NEED_FILE ")
			.append(" set ULTIMA_LAVORAZIONE = now() ")
			.append(" , NUMERO_LAVORAZIONI = NUMERO_LAVORAZIONI + 1 ")
			.append(" , NUMERO_RECORD = NULL ")
			.append(" , NUMERO_RECORD_OK = NULL ")
			.append(" , ESITO_LAVORAZIONE = NULL ")
			.append(" , ERRORE_LAVORAZIONE = NULL ")
			.append(" where ID_NEED_FILE = ").append(idNeedFile);
		try (final Statement stmt = connection.createStatement()) {
			return 1 == stmt.executeUpdate(sql.toString());
		} catch (SQLException e) {
			logger.error("updateNeedFile: {}", sql);
			throw e;
		}
	}
	
	public boolean updateNeedFile(long idNeedFile, long numeroRecord, long numeroRecordOk, Exception exception) throws SQLException {
		final StringBuilder sql = new StringBuilder()
			.append(" update PERF_NEED_FILE ")
			.append(" set NUMERO_RECORD = ").append(numeroRecord)
			.append(" , NUMERO_RECORD_OK = ").append(numeroRecordOk)
			.append(" , ESITO_LAVORAZIONE = ").append(toVarchar(null == exception ? "OK" : "KO"))
			.append(" , ERRORE_LAVORAZIONE = ").append(toVarchar(exception))
			.append(" where ID_NEED_FILE = ").append(idNeedFile);
		try (final Statement stmt = connection.createStatement()) {
			return 1 == stmt.executeUpdate(sql.toString());
		} catch (SQLException e) {
			logger.error("updateNeedFile: {}", sql);
			throw e;
		}
	}
	
	public boolean insertNeedRecord(long idNeedFile, long numeroProgrammaMusicale, int esitoLavorazione) throws SQLException {
		final StringBuilder sql = new StringBuilder()
			.append(" insert into PERF_NEED_RECORD ")
			.append(" ( ID_NEED_FILE ")
			.append(" , NUMERO_PROGRAMMA_MUSICALE ")
			.append(" , ULTIMA_LAVORAZIONE ")
			.append(" , NUMERO_LAVORAZIONI ")
			.append(" , ESITO_LAVORAZIONE ")
			.append(" ) values ")
			.append(" ( ").append(idNeedFile)
			.append(" , ").append(numeroProgrammaMusicale)
			.append(" , now() ")
			.append(" , 1 ")
			.append(" , ").append(esitoLavorazione)
			.append(" ) ");
		try (final Statement stmt = connection.createStatement()) {
			if (enableGeneratedKeys) {
				if (1 == stmt.executeUpdate(sql.toString(), Statement.RETURN_GENERATED_KEYS)) {
					setLastGeneratedKey(stmt);
					return true;
				}
			} else if (1 == stmt.executeUpdate(sql.toString())) {
				return true;
			}
		} catch (SQLException e) {
			if (ERROR_DUPLICATE_KEY != e.getErrorCode()) {
				logger.error("insertNeedRecord: {}", sql);
				throw e;
		    }
		}
		return false;
	}
	
	public boolean updateNeedRecord(long idNeedFile, long numeroProgrammaMusicale, int esitoLavorazione) throws SQLException {
		final StringBuilder sql = new StringBuilder()
			.append(" update PERF_NEED_RECORD ")
			.append(" set ULTIMA_LAVORAZIONE = now() ")
			.append(" , NUMERO_LAVORAZIONI = NUMERO_LAVORAZIONI + 1 ")
			.append(" , ESITO_LAVORAZIONE = ").append(esitoLavorazione)
			.append(" where ID_NEED_FILE = ").append(idNeedFile)
			.append(" and NUMERO_PROGRAMMA_MUSICALE = ").append(numeroProgrammaMusicale);
		try (final Statement stmt = connection.createStatement()) {
			return 1 == stmt.executeUpdate(sql.toString());
		} catch (SQLException e) {
			logger.error("updateNeedRecord: {}", sql);
			throw e;
		}
	}
	
	public boolean insertDirettore(long idProgrammaMusicale, Direttore1 direttore1, Direttore2 direttore2) throws SQLException {
		final StringBuilder sql = new StringBuilder()
			.append(" insert into PERF_DIRETTORE_ESECUZIONE ")
			.append(" ( ID_PROGRAMMA_MUSICALE ")
			.append(" , NUMERO_PROGRAMMA_MUSICALE ")
			.append(" , DIRETTORE_ESECUZIONE ")
			.append(" , POSIZIONE_SIAE ")
			.append(" , INDIRIZZO ")
			.append(" , CAP ")
			.append(" , COMUNE ")
			.append(" , SIGLA_PROVINCIA ")
			.append(" , CODICE_SAP ")
			.append(" , CODICE_FISCALE ")
			.append(" , SESSO ")
			.append(" , COMUNE_NASCITA ")
			.append(" , SIGLA_PROVINCIA_NASCITA ")
			.append(" , DATA_NASCITA ")
			.append(" , FLAG_MANCANZA_FIRMA ")
			.append(" , FLAG_PRESENZA_FIRMA_PRIVACY ")
			.append(" , CIVICO_RESIDENZA ")
			.append(" ) values ")
			.append(" ( ").append(idProgrammaMusicale)
			.append(" , ").append(toLong(direttore1.getNumeroProgrammaMusicale()))
			.append(" , ").append(toVarchar(direttore1.getDirettoreEsecuzione()))
			.append(" , ").append(toVarchar(direttore1.getPosizioneSiae()))
			.append(" , ").append(toVarchar(direttore1.getIndirizzo()))
			.append(" , ").append(toVarchar(direttore1.getCap()))
			.append(" , ").append(toVarchar(direttore1.getComune()))
			.append(" , ").append(toVarchar(direttore1.getProvincia()))
			.append(" , NULL ")
			.append(" , ").append(toVarchar(direttore2.getCodiceFiscale()))
			.append(" , ").append(toVarchar(direttore2.getSesso()))
			.append(" , ").append(toVarchar(direttore2.getComuneNascita()))
			.append(" , ").append(toVarchar(direttore2.getProvinciaNascita()))
			.append(" , ").append(toDateTime(direttore2.getDataNascita()))
			.append(" , ").append(toVarchar(direttore2.getNoFirma()))
			.append(" , ").append(toVarchar(direttore2.getSiFirmaPrivacy()))
			.append(" , NULL ")
			.append(" ) ");
		try (final Statement stmt = connection.createStatement()) {
			if (enableGeneratedKeys) {
				if (1 == stmt.executeUpdate(sql.toString(), Statement.RETURN_GENERATED_KEYS)) {
					setLastGeneratedKey(stmt);
					return true;
				}
			} else if (1 == stmt.executeUpdate(sql.toString())) {
				return true;
			}
		} catch (SQLException e) {
			if (ERROR_DUPLICATE_KEY != e.getErrorCode()) {
				logger.error("insertDirettore: {}", sql);
				throw e;
		    }
		}
		return false;
	}
	
	public boolean updateDirettore(long idProgrammaMusicale, Direttore1 direttore1, Direttore2 direttore2) throws SQLException {
		final StringBuilder sql = new StringBuilder()
			.append(" update PERF_DIRETTORE_ESECUZIONE ")
			.append(" set NUMERO_PROGRAMMA_MUSICALE = ").append(toLong(direttore1.getNumeroProgrammaMusicale()))
			.append(" , DIRETTORE_ESECUZIONE = ").append(toVarchar(direttore1.getDirettoreEsecuzione()))
			.append(" , POSIZIONE_SIAE = ").append(toVarchar(direttore1.getPosizioneSiae()))
			.append(" , INDIRIZZO = ").append(toVarchar(direttore1.getIndirizzo()))
			.append(" , CAP = ").append(toVarchar(direttore1.getCap()))
			.append(" , COMUNE = ").append(toVarchar(direttore1.getComune()))
			.append(" , SIGLA_PROVINCIA = ").append(toVarchar(direttore1.getProvincia()))
			.append(" , CODICE_SAP = NULL ")
			.append(" , CODICE_FISCALE = ").append(toVarchar(direttore2.getCodiceFiscale()))
			.append(" , SESSO = ").append(toVarchar(direttore2.getSesso()))
			.append(" , COMUNE_NASCITA = ").append(toVarchar(direttore2.getComuneNascita()))
			.append(" , SIGLA_PROVINCIA_NASCITA = ").append(toVarchar(direttore2.getProvinciaNascita())) 
			.append(" , DATA_NASCITA = ").append(toDateTime(direttore2.getDataNascita()))
			.append(" , FLAG_MANCANZA_FIRMA = ").append(toVarchar(direttore2.getNoFirma()))
			.append(" , FLAG_PRESENZA_FIRMA_PRIVACY = ").append(toVarchar(direttore2.getSiFirmaPrivacy()))
			.append(" , CIVICO_RESIDENZA = NULL ")
			.append(" where ID_PROGRAMMA_MUSICALE = ").append(idProgrammaMusicale);
		try (final Statement stmt = connection.createStatement()) {
			return 1 == stmt.executeUpdate(sql.toString());
		} catch (SQLException e) {
			logger.error("updateDirettore: {}", sql);
			throw e;
		}
	}
	
	public boolean insertProgrammaMusicale(long idProgrammaMusicale, QuadroContabile quadroContabile, String statoPm) throws SQLException {
		final StringBuilder sql = new StringBuilder()
		.append(" insert into PERF_PROGRAMMA_MUSICALE ")
		.append(" ( ID_PROGRAMMA_MUSICALE ")
		.append(" , NUMERO_PROGRAMMA_MUSICALE ")
		.append(" , TOTALE_CEDOLE ")
		.append(" , TOTALE_DURATA_CEDOLE ")
		.append(" , STATO_PM ")
		.append(" ) values ")
		.append(" ( ").append(idProgrammaMusicale)
		.append(" , ").append(toLong(quadroContabile.getNumeroProgrammaMusicale()))
		.append(" , ").append(quadroContabile.getTotaleCedole())
		.append(" , ").append(toSeconds(quadroContabile.getTotaleDurataCedole()))
		.append(" , ").append(toVarchar(statoPm))
		.append(" ) ");
		try (final Statement stmt = connection.createStatement()) {
			return 1 == stmt.executeUpdate(sql.toString());
		} catch (SQLException e) {
			logger.error("insertProgrammaMusicale: {}", sql);
			throw e;
		}
	}
	
	public boolean updateProgrammaMusicale(long idProgrammaMusicale, QuadroContabile quadroContabile) throws SQLException {
		final StringBuilder sql = new StringBuilder()
			.append(" update PERF_PROGRAMMA_MUSICALE ")
			.append(" set TOTALE_CEDOLE = ").append(quadroContabile.getTotaleCedole())
			.append(" , TOTALE_DURATA_CEDOLE = ").append(toSeconds(quadroContabile.getTotaleDurataCedole()))
			.append(" where ID_PROGRAMMA_MUSICALE = ").append(idProgrammaMusicale);
		try (final Statement stmt = connection.createStatement()) {
			return 1 == stmt.executeUpdate(sql.toString());
		} catch (SQLException e) {
			logger.error("updateProgrammaMusicale: {}", sql);
			throw e;
		}
	}

	public void deleteUtilizzazioni(long idProgrammaMusicale) throws SQLException {
		final StringBuilder sql = new StringBuilder()
			.append(" delete from PERF_UTILIZZAZIONE ")
			.append(" where ID_PROGRAMMA_MUSICALE = ")
			.append(idProgrammaMusicale);
		try (final Statement stmt = connection.createStatement()) {
			stmt.executeUpdate(sql.toString());
		} catch (SQLException e) {
			logger.error("deleteUtilizzazioni: {}", sql);
			throw e;
		}
	}
	
	public boolean insertUtilizzazione(long idProgrammaMusicale, Utilizzazione utilizzazione) throws SQLException {
		final StringBuilder sql = new StringBuilder()
			.append(" insert into PERF_UTILIZZAZIONE ")
			.append(" ( ID_PROGRAMMA_MUSICALE ")
			.append(" , TITOLO_COMPOSIZIONE ")
			.append(" , COMPOSITORE ")
			.append(" , NUMERO_PROGRAMMA_MUSICALE ")
			.append(" , DURATA ")
			.append(" , DURATA_INFERIORE_30SEC ")
			.append(" ) values ")
			.append(" ( ").append(idProgrammaMusicale)
			.append(" , ").append(toVarchar(utilizzazione.getTitolo()))
			.append(" , ").append(toVarchar(utilizzazione.getCompositore()))
			.append(" , ").append(toLong(utilizzazione.getNumeroProgrammaMusicale()))
			.append(" , ").append(toSeconds(utilizzazione.getDurata()))
			.append(" , ").append(toVarchar(utilizzazione.getDurataInferiore()))
			.append(" ) ");
		try (final Statement stmt = connection.createStatement()) {
			if (enableGeneratedKeys) {
				if (1 == stmt.executeUpdate(sql.toString(), Statement.RETURN_GENERATED_KEYS)) {
					setLastGeneratedKey(stmt);
					return true;
				}
			} else if (1 == stmt.executeUpdate(sql.toString())) {
				return true;
			}
		} catch (SQLException e) {
			if (ERROR_DUPLICATE_KEY != e.getErrorCode()) {
				logger.error("insertUtilizzazione: {}", sql);
				throw e;
		    }
		}
		return false;
	}
	
	public boolean insertMovimentoContabile(long idProgrammaMusicale, QuadroContabile quadroContabile, String tipoDocumentoContabile) throws SQLException {
		final StringBuilder sql = new StringBuilder()
		.append(" insert into PERF_MOVIMENTO_CONTABILE ")
		.append(" ( ID_PROGRAMMA_MUSICALE ")
		.append(" , CODICE_VOCE_INCASSO ")
		.append(" , NUMERO_PM ")
		.append(" , NUMERO_REVERSALE ")
		.append(" , IMPORTO_DEM_TOTALE ")
		.append(" , TOTALE_DEM_LORDO_PM ")
		.append(" , CONTABILITA ")
		.append(" , TIPO_DOCUMENTO_CONTABILE ")
		.append(" ) values ")
		.append(" ( ").append(idProgrammaMusicale)
		.append(" , ").append(toVarchar(quadroContabile.getCodiceVoceIncasso()))
		.append(" , ").append(toVarchar(quadroContabile.getNumeroProgrammaMusicale()))
		.append(" , ").append(toLong(quadroContabile.getNumeroReversale()))
		.append(" , ").append(toEuro(quadroContabile.getImportoContabile()))
		.append(" , ").append(toEuro(quadroContabile.getImportoContabile()))
		.append(" , ").append(toVarchar(quadroContabile.getContabilita()))
		.append(" , ").append(toVarchar(tipoDocumentoContabile)) // .append(" , '501' ")
		.append(" ) ");
		try (final Statement stmt = connection.createStatement()) {
			return 1 == stmt.executeUpdate(sql.toString());
		} catch (SQLException e) {
			logger.error("insertMovimentoContabile: {}", sql);
			throw e;
		}
	}

}
