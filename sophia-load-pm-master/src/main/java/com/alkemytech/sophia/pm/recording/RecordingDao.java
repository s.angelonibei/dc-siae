package com.alkemytech.sophia.pm.recording;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.pm.BasicDao;

/*

 DROP TABLE IF EXISTS PERF_RECORDING_FILE;
 CREATE TABLE PERF_RECORDING_FILE
 (
    ID_RECORDING_FILE    BIGINT          NOT NULL AUTO_INCREMENT,
    FILE_NAME            VARCHAR(100)    NOT NULL,
    FILE_TYPE            VARCHAR(100)    NOT NULL, -- 'PRELUDIO' | 'REGBSM'
    S3_PATH              VARCHAR(1000)   NOT NULL,
    ULTIMA_LAVORAZIONE   DATETIME        NOT NULL DEFAULT CURRENT_TIMESTAMP,
    NUMERO_LAVORAZIONI   INT             NOT NULL,
    NUMERO_EVENTI        BIGINT,
    NUMERO_SCARTI        BIGINT,
    NUMERO_UTILIZZAZIONI BIGINT,
    ESITO_LAVORAZIONE    VARCHAR(100),
    ERRORE_LAVORAZIONE   TEXT,
    PRIMARY KEY (ID_RECORDING_FILE)
 );
 
 CREATE UNIQUE INDEX PERF_RECORDING_FILE_IDX_01
   ON PERF_RECORDING_FILE (FILE_NAME ASC, FILE_TYPE ASC);


 DROP TABLE IF EXISTS PERF_RECORDING_RECORD;
 CREATE TABLE PERF_RECORDING_RECORD
 (
    ID_RECORDING_FILE          BIGINT        NOT NULL,
    ID_PROGRAMMA_MUSICALE      BIGINT        NOT NULL,
    NUMERO_PROGRAMMA_MUSICALE  VARCHAR(20)   ,
    PRIMARY KEY (ID_PROGRAMMA_MUSICALE)
 );

 CREATE UNIQUE INDEX PERF_RECORDING_RECORD_IDX_01
   ON PERF_RECORDING_RECORD (NUMERO_PROGRAMMA_MUSICALE ASC);


 DROP TABLE IF EXISTS PERF_RECORDING_SCARTO;
 CREATE TABLE PERF_RECORDING_SCARTO
 (
    ID_RECORDING_SCARTO        BIGINT        NOT NULL AUTO_INCREMENT,
    ID_RECORDING_FILE          BIGINT        NOT NULL,
    ID_EVENTO                  VARCHAR(20)   ,
    DATA_INIZIO_EVENTO         VARCHAR(10)   ,
    CODICE_BA_LOCALE           VARCHAR(20)   ,
    VOCE_INCASSO               VARCHAR(20)   ,
    NUMERO_PROGRAMMA_MUSICALE  VARCHAR(20)   ,
    DATA_CARICAMENTO           DATETIME      NOT NULL DEFAULT CURRENT_TIMESTAMP,
    MOTIVO_SCARTO              TEXT          ,
    PRIMARY KEY (ID_RECORDING_SCARTO)
 );
 
 
 DROP TABLE IF EXISTS PERF_RECORDING_PM_ID;
 CREATE TABLE PERF_RECORDING_PM_ID (ID BIGINT NOT NULL);
 INSERT INTO PERF_RECORDING_PM_ID VALUES (98000000);
 COMMIT;
 
 */

public class RecordingDao extends BasicDao {
	
	private static final Logger logger = LoggerFactory.getLogger(RecordingDao.class);
	
	public static final String FILE_TYPE_PRELUDIO = "PRELUDIO";
	public static final String FILE_TYPE_REGBSM = "REGBSM";
	
	private final Connection connection;

	private boolean enableGeneratedKeys;	
	private long lastGeneratedKey;

	public RecordingDao(Connection connection) {
		this(connection, true);
	}

	public RecordingDao(Connection connection, boolean enableGeneratedKeys) {
		super();
		this.connection = connection;
		this.enableGeneratedKeys = enableGeneratedKeys;
	}

	public boolean isEnableGeneratedKeys() {
		return enableGeneratedKeys;
	}

	public void setEnableGeneratedKeys(boolean enableGeneratedKeys) {
		this.enableGeneratedKeys = enableGeneratedKeys;
	}

	public long getLastGeneratedKey() {
		if (enableGeneratedKeys) {
			return lastGeneratedKey;
		}
		throw new IllegalStateException("generated PKs not enabled");
	}
	
	private void setLastGeneratedKey(Statement stmt) throws SQLException {
		lastGeneratedKey = 0L;
		try (final ResultSet rset = stmt.getGeneratedKeys()) {
			if (rset.next())
				lastGeneratedKey = rset.getLong(1);
		}
	}
	
	public long getIdRecordingFile(String filename, String fileType) throws SQLException {
		final StringBuilder sql = new StringBuilder()
			.append(" select ID_RECORDING_FILE ")
			.append("   from PERF_RECORDING_FILE ")
			.append("  where FILE_NAME = ").append(toVarchar(filename))
			.append("    and FILE_TYPE = ").append(toVarchar(fileType));
		try (final Statement stmt = connection.createStatement()) {
			try (final ResultSet rset = stmt.executeQuery(sql.toString())) {
				if (rset.next())
					return rset.getLong(1);
			}
		}
		return -1L;
	}
	
	/*
	 * UPDATE PERF_RECORDING_PM_ID SET ID = LAST_INSERT_ID(ID+1);
	 * SELECT LAST_INSERT_ID();
	 */
	public long nextIdProgrammaMusicale() throws SQLException {		
		try (final Statement stmt = connection.createStatement()) {
			if (1 == stmt.executeUpdate("UPDATE PERF_RECORDING_PM_ID SET ID = LAST_INSERT_ID(ID+1)")) {
				try (final ResultSet rset = stmt.executeQuery("SELECT LAST_INSERT_ID()")) {
					if (rset.next())
						return rset.getLong(1);
				}				
			}
		}
		return -1L;
	}
	
	public long getIdProgrammaMusicale(long numeroProgrammaMusicale) throws SQLException {
		final StringBuilder sql = new StringBuilder()
			.append(" select ID_PROGRAMMA_MUSICALE ")
			.append(" from PERF_PROGRAMMA_MUSICALE ")
			.append(" where NUMERO_PROGRAMMA_MUSICALE = ").append(numeroProgrammaMusicale);
		try (final Statement stmt = connection.createStatement()) {
			try (final ResultSet rset = stmt.executeQuery(sql.toString())) {
				if (rset.next())
					return rset.getLong(1);
			}
		}
		return -1L;
	}
	
	public boolean insertRecordingFile(String filename, String fileType, String s3path) throws SQLException {
		final StringBuilder sql = new StringBuilder()
			.append(" insert into PERF_RECORDING_FILE ")
			.append(" ( FILE_NAME ")
			.append(" , FILE_TYPE ")
			.append(" , S3_PATH ")
			.append(" , ULTIMA_LAVORAZIONE ")
			.append(" , NUMERO_LAVORAZIONI ")
			.append(" ) values ")
			.append(" ( ").append(toVarchar(filename))
			.append(" , ").append(toVarchar(fileType))
			.append(" , ").append(toVarchar(s3path))
			.append(" , now() ")
			.append(" , 1 ")
			.append(" ) ");
		try (final Statement stmt = connection.createStatement()) {
			if (enableGeneratedKeys) {
				if (1 == stmt.executeUpdate(sql.toString(), Statement.RETURN_GENERATED_KEYS)) {
					setLastGeneratedKey(stmt);
					return true;
				}
			} else if (1 == stmt.executeUpdate(sql.toString())) {
				return true;
			}
		} catch (SQLException e) {
			if (ERROR_DUPLICATE_KEY != e.getErrorCode()) {
				logger.error("insertRecordingFile: {}", sql);
				throw e;
		    }
		}
		return false;
	}

	public boolean insertRecordingRecord(long idRecordingFile, long idProgrammaMusicale, String numeroProgrammaMusicale) throws SQLException {
		final StringBuilder sql = new StringBuilder()
			.append(" insert into PERF_RECORDING_RECORD ")
			.append(" ( ID_RECORDING_FILE ")
			.append(" , ID_PROGRAMMA_MUSICALE ")
			.append(" , NUMERO_PROGRAMMA_MUSICALE ")
			.append(" ) values ")
			.append(" ( ").append(idRecordingFile)
			.append(" , ").append(idProgrammaMusicale)
			.append(" , ").append(toVarchar(numeroProgrammaMusicale))
			.append(" ) ");
		try (final Statement stmt = connection.createStatement()) {
			if (1 == stmt.executeUpdate(sql.toString())) {
				return true;
			}
		} catch (SQLException e) {
			if (ERROR_DUPLICATE_KEY != e.getErrorCode()) {
				logger.error("insertRecordingRecord: {}", sql);
				throw e;
		    }
		}
		return false;
	}
	
	public void deleteRecordingRecord(long idProgrammaMusicale) throws SQLException {
		final StringBuilder sql = new StringBuilder()
			.append(" delete from PERF_RECORDING_RECORD ")
			.append(" where ID_PROGRAMMA_MUSICALE = ").append(idProgrammaMusicale);
		try (final Statement stmt = connection.createStatement()) {
			stmt.executeUpdate(sql.toString());
		} catch (SQLException e) {
			logger.error("deleteRecordingRecord: {}", sql);
			throw e;
		}
	}
	
	public boolean updateRecordingFile(long idRecordingFile) throws SQLException {
		final StringBuilder sql = new StringBuilder()
			.append(" update PERF_RECORDING_FILE ")
			.append(" set ULTIMA_LAVORAZIONE = now() ")
			.append(" , NUMERO_LAVORAZIONI = NUMERO_LAVORAZIONI + 1 ")
			.append(" , NUMERO_EVENTI = NULL ")
			.append(" , NUMERO_SCARTI = NULL ")
			.append(" , NUMERO_UTILIZZAZIONI = NULL ")
			.append(" , ESITO_LAVORAZIONE = NULL ")
			.append(" , ERRORE_LAVORAZIONE = NULL ")
			.append(" where ID_RECORDING_FILE = ").append(idRecordingFile);
		try (final Statement stmt = connection.createStatement()) {
			return 1 == stmt.executeUpdate(sql.toString());
		} catch (SQLException e) {
			logger.error("updateRecordingFile: {}", sql);
			throw e;
		}
	}
	
	public boolean updateRecordingFile(long idRecordingFile, long numeroEventi, long numeroScarti, long numeroUtilizzazioni, Exception exception) throws SQLException {
		final StringBuilder sql = new StringBuilder()
			.append(" update PERF_RECORDING_FILE ")
			.append(" set NUMERO_EVENTI = ").append(numeroEventi)
			.append(" , NUMERO_SCARTI = ").append(numeroScarti)
			.append(" , NUMERO_UTILIZZAZIONI = ").append(numeroUtilizzazioni)
			.append(" , ESITO_LAVORAZIONE = ").append(toVarchar(null == exception ? "OK" : "KO"))
			.append(" , ERRORE_LAVORAZIONE = ").append(toVarchar(exception))
			.append(" where ID_RECORDING_FILE = ").append(idRecordingFile);
		try (final Statement stmt = connection.createStatement()) {
			return 1 == stmt.executeUpdate(sql.toString());
		} catch (SQLException e) {
			logger.error("updateRecordingFile: {}", sql);
			throw e;
		}
	}
	
	public boolean insertRecordingScarto(long idRecordingFile, String idEvento, String dataInizioEvento, String codiceBaLocale, String voceIncasso, String numeroProgrammaMusicale, String motivoScarto) throws SQLException {
		final StringBuilder sql = new StringBuilder()
			.append(" insert into PERF_RECORDING_SCARTO ")
			.append(" ( ID_RECORDING_FILE ")
			.append(" , ID_EVENTO ")
			.append(" , DATA_INIZIO_EVENTO ")
			.append(" , CODICE_BA_LOCALE ")
			.append(" , VOCE_INCASSO ")
			.append(" , NUMERO_PROGRAMMA_MUSICALE ")
			.append(" , DATA_CARICAMENTO ")
			.append(" , MOTIVO_SCARTO ")
			.append(" ) values ")
			.append(" ( ").append(idRecordingFile)
			.append(" , ").append(toVarchar(idEvento))
			.append(" , ").append(toVarchar(dataInizioEvento))
			.append(" , ").append(toVarchar(codiceBaLocale))
			.append(" , ").append(toVarchar(voceIncasso))			
			.append(" , ").append(toVarchar(numeroProgrammaMusicale))
			.append(" , now() ")
			.append(" , ").append(toVarchar(motivoScarto))
			.append(" ) ");
		try (final Statement stmt = connection.createStatement()) {
			if (enableGeneratedKeys) {
				if (1 == stmt.executeUpdate(sql.toString(), Statement.RETURN_GENERATED_KEYS)) {
					setLastGeneratedKey(stmt);
					return true;
				}
			} else if (1 == stmt.executeUpdate(sql.toString())) {
				return true;
			}
		} catch (SQLException e) {
			if (ERROR_DUPLICATE_KEY != e.getErrorCode()) {
				logger.error("insertRecordingRecord: {}", sql);
				throw e;
		    }
		}
		return false;
	}

	public Long getIdEvento(String idEvento) throws SQLException {
		final StringBuilder sql = new StringBuilder()
			.append(" select ID_EVENTO ")
			.append(" from PERF_MANIFESTAZIONE ")
			.append(" where ID_EVENTO = ").append(toLong(idEvento));
//		logger.debug(sql.toString());
		try (final Statement stmt = connection.createStatement()) {
			try (final ResultSet rset = stmt.executeQuery(sql.toString())) {
				if (rset.next()) {
					final long result = rset.getLong(1);
					return rset.wasNull() ? null : result;
				}
			}
		}
		return null;
	}
	
	public List<Long> getIdEventi(String dataInizioEvento, String codiceBaLocale, String voceIncasso) throws SQLException {
		final List<Long> result = new ArrayList<>(1);
		final StringBuilder sql = new StringBuilder()
			.append(" select ID_EVENTO ")
			.append(" from PERF_EVENTI_PAGATI ")
			.append(" where CODICE_BA_LOCALE = ").append(toVarchar(codiceBaLocale))
			.append(" and VOCE_INCASSO = ").append(toVarchar(voceIncasso))
			.append(" and DATA_INIZIO_EVENTO = STR_TO_DATE(").append(toVarchar(dataInizioEvento)).append(", '%Y%m%d')");
//		logger.debug(sql.toString());
		try (final Statement stmt = connection.createStatement()) {
			try (final ResultSet rset = stmt.executeQuery(sql.toString())) {
				while (rset.next())
					result.add(rset.getLong(1));
			}
		}
		return result;
	}
	
	public boolean insertProgrammaMusicale(long idProgrammaMusicale, long numeroProgrammaMusicale, String statoPm) throws SQLException {
		final StringBuilder sql = new StringBuilder()
		.append(" insert into PERF_PROGRAMMA_MUSICALE ")
		.append(" ( ID_PROGRAMMA_MUSICALE ")
		.append(" , NUMERO_PROGRAMMA_MUSICALE ")
		.append(" , STATO_PM ")
		.append(" ) values ")
		.append(" ( ").append(idProgrammaMusicale)
		.append(" , ").append(numeroProgrammaMusicale)
		.append(" , ").append(toVarchar(statoPm))
		.append(" ) ");
		try (final Statement stmt = connection.createStatement()) {
			return 1 == stmt.executeUpdate(sql.toString());
		} catch (SQLException e) {
			logger.error("insertProgrammaMusicale: {}", sql);
			throw e;
		}
	}

	public void deleteProgrammaMusicale(long idProgrammaMusicale) throws SQLException {
		final StringBuilder sql = new StringBuilder()
			.append(" delete from PERF_PROGRAMMA_MUSICALE ")
			.append(" where ID_PROGRAMMA_MUSICALE = ").append(idProgrammaMusicale);
		try (final Statement stmt = connection.createStatement()) {
			stmt.executeUpdate(sql.toString());
		} catch (SQLException e) {
			logger.error("deleteRecordingRecord: {}", sql);
			throw e;
		}
	}
	
	public boolean insertUtilizzazione(long idProgrammaMusicale, String codiceOpera, String titolo, String compositore, String autori, String interpreti, String numeroProgrammaMusicale, String durata, String durataInferiore30Sec) throws SQLException {
		final StringBuilder sql = new StringBuilder()
			.append(" insert into PERF_UTILIZZAZIONE ")
			.append(" ( ID_PROGRAMMA_MUSICALE ")
			.append(" , NUMERO_PROGRAMMA_MUSICALE ")
			.append(" , CODICE_OPERA ")
			.append(" , TITOLO_COMPOSIZIONE ")
			.append(" , COMPOSITORE ")
			.append(" , AUTORI ")
			.append(" , INTERPRETI ")
			.append(" , DURATA ")
			.append(" , DURATA_INFERIORE_30SEC ")
			.append(" ) values ")
			.append(" ( ").append(idProgrammaMusicale)
			.append(" , ").append(toLong(numeroProgrammaMusicale))
			.append(" , ").append(toVarchar(codiceOpera))
			.append(" , ").append(toVarchar(titolo))
			.append(" , ").append(toVarchar(compositore))
			.append(" , ").append(toVarchar(autori))
			.append(" , ").append(toVarchar(interpreti))
			.append(" , ").append(toDuration(durata))
			.append(" , ").append(toVarchar(durataInferiore30Sec))
			.append(" ) ");
		try (final Statement stmt = connection.createStatement()) {
			if (enableGeneratedKeys) {
				if (1 == stmt.executeUpdate(sql.toString(), Statement.RETURN_GENERATED_KEYS)) {
					setLastGeneratedKey(stmt);
					return true;
				}
			} else if (1 == stmt.executeUpdate(sql.toString())) {
				return true;
			}
		} catch (SQLException e) {
			if (ERROR_DUPLICATE_KEY != e.getErrorCode()) {
				logger.error("insertUtilizzazione: {}", sql);
				throw e;
		    }
		}
		return false;
	}
	
	public boolean insertUtilizzazioneScarto(long idRecordingFile, String codiceOpera, String titolo, String compositore, String autori, String interpreti, String numeroProgrammaMusicale, String durata, String durataInferiore30Sec) throws SQLException {
		final StringBuilder sql = new StringBuilder()
			.append(" insert into PERF_RECORDING_UTILIZZAZIONE_SCARTO ")
			.append(" ( ID_RECORDING_FILE ")
			.append(" , NUMERO_PROGRAMMA_MUSICALE ")
			.append(" , CODICE_OPERA ")
			.append(" , TITOLO_COMPOSIZIONE ")
			.append(" , COMPOSITORE ")
			.append(" , AUTORI ")
			.append(" , INTERPRETI ")
			.append(" , DURATA ")
			.append(" , DURATA_INFERIORE_30SEC ")
			.append(" ) values ")
			.append(" ( ").append(idRecordingFile)
			.append(" , ").append(toLong(numeroProgrammaMusicale))
			.append(" , ").append(toVarchar(codiceOpera))
			.append(" , ").append(toVarchar(titolo))
			.append(" , ").append(toVarchar(compositore))
			.append(" , ").append(toVarchar(autori))
			.append(" , ").append(toVarchar(interpreti))
			.append(" , ").append(toDuration(durata))
			.append(" , ").append(toVarchar(durataInferiore30Sec))
			.append(" ) ");
		try (final Statement stmt = connection.createStatement()) {
			if (enableGeneratedKeys) {
				if (1 == stmt.executeUpdate(sql.toString(), Statement.RETURN_GENERATED_KEYS)) {
					setLastGeneratedKey(stmt);
					return true;
				}
			} else if (1 == stmt.executeUpdate(sql.toString())) {
				return true;
			}
		} catch (SQLException e) {
			if (ERROR_DUPLICATE_KEY != e.getErrorCode()) {
				logger.error("insertUtilizzazioneScarto: {}", sql);
				throw e;
		    }
		}
		return false;
	}
	
	public boolean insertMovimentoContabile(long idProgrammaMusicale, String idEvento, String codiceVoceIncasso, String numeroProgrammaMusicale, String numeroReversale, String permesso, String contabilita) throws SQLException {
		final StringBuilder sql = new StringBuilder()
		.append(" insert into PERF_MOVIMENTO_CONTABILE ")
		.append(" ( ID_PROGRAMMA_MUSICALE ")
		.append(" , ID_EVENTO ")
		.append(" , CODICE_VOCE_INCASSO ")
		.append(" , NUMERO_PM ")
		.append(" , NUMERO_REVERSALE ")
		.append(" , NUMERO_PERMESSO ")
		.append(" , CONTABILITA ")
		.append(" ) values ")
		.append(" ( ").append(idProgrammaMusicale)
		.append(" , ").append(toLong(idEvento))
		.append(" , ").append(toVarchar(codiceVoceIncasso))
		.append(" , ").append(toVarchar(numeroProgrammaMusicale))
		.append(" , ").append(toLong(numeroReversale))
		.append(" , ").append(toLong(permesso))
		.append(" , ").append(toVarchar(contabilita))
		.append(" ) ");
		try (final Statement stmt = connection.createStatement()) {
			return 1 == stmt.executeUpdate(sql.toString());
		} catch (SQLException e) {
			logger.error("insertMovimentoContabile: {}", sql);
			throw e;
		}
	}

	public boolean isNumeroProgrammaMusicaleLavorato(String numeroProgrammaMusicale) throws SQLException {
		final StringBuilder sql = new StringBuilder()
			.append(" select ID_PROGRAMMA_MUSICALE ")
			.append(" from PERF_RECORDING_RECORD ")
			.append(" where NUMERO_PROGRAMMA_MUSICALE = ").append(toVarchar(numeroProgrammaMusicale));
		try (final Statement stmt = connection.createStatement()) {
			try (final ResultSet rset = stmt.executeQuery(sql.toString())) {
				if (rset.next())
					return true;
			}
		}
		return false;
	}
	
}
