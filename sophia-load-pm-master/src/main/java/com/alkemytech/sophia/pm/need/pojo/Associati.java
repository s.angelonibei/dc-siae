package com.alkemytech.sophia.pm.need.pojo;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class Associati extends NeedPojo {

	// TRK 90
	// [1,2] "90"
//	private String cartella;				// [3] "A"
//	private String codiceSede;				// [4,5] "03"
//	private String codiceProvincia;			// [6,8] "032"
//	private String unitaTerritoriale;		// [9,10] "08"
//	private String codiceVoceIncasso;		// [11,14] "2222"
//	private String progressivoCartella;		// [15,16] "01"
//	private String contabilita;				// [17,22] "201608"
	private String numeroProgrammaMusicale;	// [23,30] "90195603"
	private String nomeCognome1;			// [31,45] "$              "
	private String nomeCognome2;			// [46,60] "               "
	private String nomeCognome3;			// [61,75] "               "
	private String posizioneSiae1;			// [76,84] "000000000"
	private String posizioneSiae2;			// [85,93] "000000000"
	private String posizioneSiae3;			// [94,102] "000000000"
	// [103,180] (unused)
	
	public Associati(byte[] buffer) {
		super();
//		this.cartella = toString(buffer, 2, 1);
//		this.codiceSede = toString(buffer, 3, 2);
//		this.codiceProvincia = toString(buffer, 5, 3);
//		this.unitaTerritoriale = toString(buffer, 8, 2);
//		this.codiceVoceIncasso = toString(buffer, 10, 4);
//		this.progressivoCartella = toString(buffer, 14, 2);
//		this.contabilita = toString(buffer, 16, 6);
		this.numeroProgrammaMusicale = toString(buffer, 22, 8);
		this.nomeCognome1 = toString(buffer, 30, 15);
		this.nomeCognome2 = toString(buffer, 45, 15);
		this.nomeCognome3 = toString(buffer, 60, 15);
		this.posizioneSiae1 = toString(buffer, 75, 9);
		this.posizioneSiae2 = toString(buffer, 84, 9);
		this.posizioneSiae3 = toString(buffer, 93, 9);
	}
	
	public String getNumeroProgrammaMusicale() {
		return numeroProgrammaMusicale;
	}
	public void setNumeroProgrammaMusicale(String numeroProgrammaMusicale) {
		this.numeroProgrammaMusicale = numeroProgrammaMusicale;
	}
	public String getNomeCognome1() {
		return nomeCognome1;
	}
	public void setNomeCognome1(String nomeCognome1) {
		this.nomeCognome1 = nomeCognome1;
	}
	public String getNomeCognome2() {
		return nomeCognome2;
	}
	public void setNomeCognome2(String nomeCognome2) {
		this.nomeCognome2 = nomeCognome2;
	}
	public String getNomeCognome3() {
		return nomeCognome3;
	}
	public void setNomeCognome3(String nomeCognome3) {
		this.nomeCognome3 = nomeCognome3;
	}
	public String getPosizioneSiae1() {
		return posizioneSiae1;
	}
	public void setPosizioneSiae1(String posizioneSiae1) {
		this.posizioneSiae1 = posizioneSiae1;
	}
	public String getPosizioneSiae2() {
		return posizioneSiae2;
	}
	public void setPosizioneSiae2(String posizioneSiae2) {
		this.posizioneSiae2 = posizioneSiae2;
	}
	public String getPosizioneSiae3() {
		return posizioneSiae3;
	}
	public void setPosizioneSiae3(String posizioneSiae3) {
		this.posizioneSiae3 = posizioneSiae3;
	}

}
