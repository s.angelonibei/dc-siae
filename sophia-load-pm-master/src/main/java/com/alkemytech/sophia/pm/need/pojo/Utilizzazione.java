package com.alkemytech.sophia.pm.need.pojo;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class Utilizzazione extends NeedPojo {

	// TRK 40
	// [1,2] "40"
//	private String cartella;				// [3] "A"
//	private String codiceSede;				// [4,5] "03"
//	private String codiceProvincia;			// [6,8] "032"
//	private String unitaTerritoriale;		// [9,10] "08"
//	private String codiceVoceIncasso;		// [11,14] "2222"
//	private String progressivoCartella;		// [15,16] "01"
//	private String contabilita;				// [17,22] "201608"
	private String numeroProgrammaMusicale;	// [23,30] "90195603"
	private String compositore;				// [31,45] "CASTELLINA     "
	private String titolo;					// [46,69] "RADIO POLKA             "
	private String durataInferiore;			// [70] " "
	private String durata;					// [71,75] "00000"
	private String elaboratore;				// [76,90] "$              "
	// [91,180] (unused)
	
	public Utilizzazione(byte[] buffer) {
		super();
//		this.cartella = toString(buffer, 2, 1);
//		this.codiceSede = toString(buffer, 3, 2);
//		this.codiceProvincia = toString(buffer, 5, 3);
//		this.unitaTerritoriale = toString(buffer, 8, 2);
//		this.codiceVoceIncasso = toString(buffer, 10, 4);
//		this.progressivoCartella = toString(buffer, 14, 2);
//		this.contabilita = toString(buffer, 16, 6);
		this.numeroProgrammaMusicale = toString(buffer, 22, 8);
		this.compositore = toString(buffer, 30, 15);
		this.titolo = toString(buffer, 45, 24);
		this.durataInferiore = toString(buffer, 69, 1);
		this.durata = toString(buffer, 70, 5);
		this.elaboratore = toString(buffer, 75, 15);
	}
	
	public String getNumeroProgrammaMusicale() {
		return numeroProgrammaMusicale;
	}
	public void setNumeroProgrammaMusicale(String numeroProgrammaMusicale) {
		this.numeroProgrammaMusicale = numeroProgrammaMusicale;
	}
	public String getCompositore() {
		return compositore;
	}
	public void setCompositore(String compositore) {
		this.compositore = compositore;
	}
	public String getTitolo() {
		return titolo;
	}
	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}
	public String getDurataInferiore() {
		return durataInferiore;
	}
	public void setDurataInferiore(String durataInferiore) {
		this.durataInferiore = durataInferiore;
	}
	public String getDurata() {
		return durata;
	}
	public void setDurata(String durata) {
		this.durata = durata;
	}
	public String getElaboratore() {
		return elaboratore;
	}
	public void setElaboratore(String elaboratore) {
		this.elaboratore = elaboratore;
	}
	
}
