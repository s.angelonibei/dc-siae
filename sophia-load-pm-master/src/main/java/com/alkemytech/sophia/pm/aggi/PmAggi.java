package com.alkemytech.sophia.pm.aggi;

import java.math.BigDecimal;
import java.net.ServerSocket;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.common.megaconcert.MegaConcertUtils;
import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.sqs.McmdbMessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.MessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.SqsMessageHelper;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.pm.McmdbDataSource;
import com.google.common.base.Strings;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Scopes;
import com.google.inject.name.Named;
import com.google.inject.name.Names;


public class PmAggi {

	/*
	 * modulo legge da coda SQS con campi { year e month }
	 * 
	 * per tutte le voci NDM
	 * 
	 * cerco su PERF_EVENTI_PAGATI con NUMERO_FATTURA and VOCE_INCASSO
	 * 
	 * poi cerco su PERF_MANIFESTAZIONE	con ID_EVENTO e guardo FLAG_MEGACONCERT
	 * 
	 *    IMPORTO_DEM               DECIMAL(15,7),
	 *    IMPORTO_AGGIO             DECIMAL(30,15), -- lo cambio secondo algoritmo
	 *    QUADRATURA_NDM            CHAR(1) -- 0 (diversi) o 1 (uguali e cambio IMPORTO_AGGIO con algoritmo)
	 *    IMPORTO_EX_ART_18         DECIMAL(30,15), -- (IMPORTO_DEM - IMPORTO_AGGIO) * 0.05
	 * 
	 * algoritmo se QUADRATURA_NDM = 0
	 * - metto IMPORTO_AGGIO = null, QUADRATURA_NDM = 0, IMPORTO_EX_ART_18 = null
	 * 
	 * se non trovo su NDM la voce fattura 
	 * - metto IMPORTO_AGGIO = null, QUADRATURA_NDM = null, IMPORTO_EX_ART_18 = null
	 * 
	 * algoritmo se QUADRATURA_NDM = 1:
	 * - se voce=2244 and flag_megaconcert=1 -> ricalcolo 
	 * - altrimenti uso aggio NDM
	 * 
	 * algoritmo ricalcolo:
	 * - solo FLAG_ATTIVO = 1
	 * - cerco regola attiva su PERF_AGGI_MC con INIZIO_PERIODO_VALIDAZIONE e FINE_PERIODO_VALIDAZIONE 
	 * 
	 * - PERF_EVENTI_PAGATI . NUMERO_PM_ATTESI_SPALLA dice il numero di gruppi spalla (da usare per il CAP)
	 *   - se NUMERO_PM_ATTESI_SPALLA null oppure 0 => SOGLIA è la soglia effettiva
	 *   - se NUMERO_PM_ATTESI_SPALLA > 0 e FLAG_CAP_MULTIPLO = 0 => SOGLIA + CAP è la soglia effettiva
	 *   - se NUMERO_PM_ATTESI_SPALLA > 0 e FLAG_CAP_MULTIPLO = 1 => SOGLIA + CAP * NUMERO_PM_ATTESI_SPALLA è la soglia effettiva
	 *   
	 *   la parte IMPORTO_DEM sotto soglia calcolata ha aggio pari a AGGIO_SOTTOSOGLIA
	 *   la parte IMPORTO_DEM che eccede la soglia calcolata ha aggio pari a AGGIO_SOPRASOGLIA
	 *   sommo i due "pezzi" per l'aggio totale
	 * 
	 * mettere in classe common il metodo per mega_aggio...
	 * 
	 */
	
	private static final Logger logger = LoggerFactory.getLogger(PmAggi.class);
	
	private static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// amazon service(s)
			bind(S3.class)
				.in(Scopes.SINGLETON);
			bind(SQS.class)
				.in(Scopes.SINGLETON);
			// data source(s)
			bind(DataSource.class)
				.annotatedWith(Names.named("MCMDB"))
				.to(McmdbDataSource.class)
				.in(Scopes.SINGLETON);
			// message deduplicator(s)
			bind(MessageDeduplicator.class)
				.to(McmdbMessageDeduplicator.class)
				.in(Scopes.SINGLETON);
			// self
			bind(PmAggi.class).asEagerSingleton();
		}
		
	}
		
	public static void main(String[] args) {
		try {
			final Injector injector = Guice
					.createInjector(new GuiceModuleExtension(args, "/sophia-aggi-pm.properties"));
			final PmAggi instance = injector
					.getInstance(PmAggi.class)
					.startup();
			try {
				instance.process();
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}
	
	private final Properties configuration;
	private final SQS sqs;
	private final DataSource mcmdbDS;
	private final MessageDeduplicator messageDeduplicator;

	@Inject
	protected PmAggi(@Named("configuration") Properties configuration,
			MessageDeduplicator messageDeduplicator,
			SQS sqs, @Named("MCMDB") DataSource mcmdbDS) {
		super();
		this.configuration = configuration;
		this.sqs = sqs;
		this.mcmdbDS = mcmdbDS;
		this.messageDeduplicator = messageDeduplicator;
	}
	
	public PmAggi startup() {
		sqs.startup();
		return this;
	}
	
	public PmAggi shutdown() {
		sqs.shutdown();
		return this;
	}

	private void process() throws Exception {
		
		logger.debug("execute: start time {}", DateFormat.getTimeInstance().format(new Date()));
		final int lockPort = Integer.parseInt(configuration.getProperty("pmaggi.bind_port",
				configuration.getProperty("default.bind_port", "0")));
		try (final ServerSocket socket = new ServerSocket(lockPort); // avoid duplicate executions
				final Connection mcmdb = mcmdbDS.getConnection()) { 
			logger.info("execute: listening on {}", socket.getLocalSocketAddress());				
			logger.info("execute: connected to {} {}", mcmdb.getMetaData()
					.getDatabaseProductName(), mcmdb.getMetaData().getURL());

			final SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration, "pmaggi.sqs");
			sqsMessagePump.pollingLoop(messageDeduplicator, new SqsMessagePump.Consumer() {
				
				private JsonObject output;
				private JsonObject error;

				@Override
				public JsonObject getStartedMessagePayload(JsonObject message) {
					output = null;
					error = null;
					return SqsMessageHelper.formatContext();
				}

				@Override
				public boolean consumeMessage(JsonObject message) {
					try {
						output = new JsonObject();
						processMessage(message, output);
						error = null;
						return true;
					} catch (Exception e) {
						output = null;
						error = SqsMessageHelper.formatError(e);
						return false;
					}
				}

				@Override
				public JsonObject getCompletedMessagePayload(JsonObject message) {
					return output;
				}

				@Override
				public JsonObject getFailedMessagePayload(JsonObject message) {
					return error;
				}

			});
			
		}
		logger.debug("execute: end time {}", DateFormat.getTimeInstance().format(new Date()));

	}
	
	private void processMessage(JsonObject input, JsonObject output) throws Exception {
		
//		{
//			"body": {
//				"year": "2016",
//				"month": "07"
//			},
//			"header": {
//				"queue": "dev_to_process_pmaggi",
//				"timestamp": "2017-05-09T01:15:00.000000+02:00",
//				"uuid": "b3e00ae5-5145-40e6-96d4-f63cac680aaa",
//				"sender": "aws-console"
//			}
//		}

		// parse input json
		final JsonObject inputJsonBody = input.getAsJsonObject("body");
		System.out.println("inputJsonBody " + inputJsonBody);
		String year = GsonUtils.getAsString(inputJsonBody, "year");
		String month = GsonUtils.getAsString(inputJsonBody, "month");
		if (Strings.isNullOrEmpty(year) || Strings.isNullOrEmpty(month))
			throw new IllegalArgumentException("missing mandatory parameter(s)");
		year = String.format("%04d", Integer.parseInt(year));
		month = String.format("%02d", Integer.parseInt(month));

		// config
		final Set<String> vociIncasso = new HashSet<>(Arrays.asList(configuration
				.getProperty("pmaggi.voci_incasso", "2244").split(",")));
		final BigDecimal percentualeExArt18 = new BigDecimal(configuration
				.getProperty("pmaggi.percentuale_ex_art_18", "0.05"));
		final BigDecimal percentualeExArt18Mc = new BigDecimal(configuration
				.getProperty("pmaggi.percentuale_ex_art_18_mc", "0"));
		
		try (Connection connection = mcmdbDS.getConnection()) {
			connection.setAutoCommit(false);
			try (Statement statement = connection.createStatement()) {
				
				// rollback eventi pagati forzati automaticamente ndm SRS #128957
				String sql = configuration.getProperty("pmaggi.sql.update_0")
						.replace("{year}", year)
						.replace("{month}", month);
				logger.info("processMessage: executing statement {}", sql);
				int count = statement.executeUpdate(sql);
				logger.info("processMessage: {} rows updated", count);				
				
				// eventi pagati che non quadrano (o che mancano su ndm)
				sql = configuration.getProperty("pmaggi.sql.update_1")
						.replace("{year}", year)
						.replace("{month}", month);
				logger.info("processMessage: executing statement {}", sql);
				count = statement.executeUpdate(sql);
				logger.info("processMessage: {} rows updated", count);
				
				// eventi pagati che quadrano
				sql = configuration.getProperty("pmaggi.sql.update_2")
						.replace("{year}", year)
						.replace("{month}", month);
				logger.info("processMessage: executing statement {}", sql);
				count = statement.executeUpdate(sql);
				logger.info("processMessage: {} rows updated", count);
				
				// eventi pagati che mancano su ndm
				sql = configuration.getProperty("pmaggi.sql.update_3")
						.replace("{year}", year)
						.replace("{month}", month);
				logger.info("processMessage: executing statement {}", sql);
				count = statement.executeUpdate(sql);
				logger.info("processMessage: {} rows updated", count);
				
				// Forzatura Automatica NDM SRS #128957
				sql = configuration.getProperty("pmaggi.sql.update_4")
						.replace("{year}", year)
						.replace("{month}", month);
				logger.info("processMessage: executing statement {}", sql);
				count = statement.executeUpdate(sql);
				logger.info("processMessage: {} rows updated", count);				
				
				// cerca eventi pagati
				sql = configuration.getProperty("pmaggi.sql.query_id_evento_megaconcert")
						.replace("{year}", year)
						.replace("{month}", month);
				logger.info("processMessage: executing statement {}", sql);
				try (ResultSet resultSet = statement.executeQuery(sql)) {

					while (resultSet.next()) {
						final long idEvento = resultSet.getLong(1);
						logger.debug("processMessage: MegaConcertUtils.updateEvento({}, {}, {})",
								idEvento, vociIncasso, percentualeExArt18);
						MegaConcertUtils.updateEvento(connection, idEvento,
								vociIncasso, percentualeExArt18, percentualeExArt18Mc);	
					}

				}
				connection.commit();				
			} catch (Exception e) {
				logger.error("processMessage", e);
				connection.rollback();
				throw e;
			}
		}
		
	}

}
