package com.alkemytech.sophia.pm.recording;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.sql.DataSource;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.sqs.SqsS3EventPump;
import com.alkemytech.sophia.commons.sqs.SqsS3Events;
import com.alkemytech.sophia.pm.McmdbDataSource;
import com.amazonaws.services.s3.model.S3Object;
import com.google.common.base.Strings;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Scopes;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class PmRegBsm {

	private static final Logger logger = LoggerFactory.getLogger(PmRegBsm.class);

	private static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// amazon service(s)
			bind(S3.class)
				.in(Scopes.SINGLETON);
			bind(SQS.class)
				.in(Scopes.SINGLETON);
			// data source(s)
			bind(DataSource.class)
				.annotatedWith(Names.named("MCMDB"))
				.to(McmdbDataSource.class)
				.in(Scopes.SINGLETON);
			// self
			bind(PmRegBsm.class).asEagerSingleton();
		}

	}

	public static void main(String[] args) {
		try {
			final Injector injector = Guice
					.createInjector(new GuiceModuleExtension(args, "/sophia-regbsm-pm.properties"));
			final PmRegBsm instance = injector
					.getInstance(PmRegBsm.class)
					.startup();
			try {
				instance.process();
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}

	private final Properties configuration;
	private final SQS sqs;
	private final S3 s3;
	private final DataSource mcmdbDS;
	private final String statoPm;

	@Inject
	protected PmRegBsm(@Named("configuration") Properties configuration,
			S3 s3, SQS sqs,
			@Named("MCMDB") DataSource mcmdbDS) {
		super();
		this.configuration = configuration;
		this.sqs = sqs;
		this.s3 = s3;
		this.mcmdbDS = mcmdbDS;
		this.statoPm = configuration
				.getProperty("pmregbsm.stato_pm", "A");
	}
	
	public PmRegBsm startup() {
		s3.startup();
		sqs.startup();
		return this;
	}
	
	public PmRegBsm shutdown() {
		s3.shutdown();
		sqs.shutdown();
		return this;
	}

	public void process() throws Exception {
		
		final int listenPort = Integer.parseInt(configuration.getProperty("pmregbsm.bind_port",
				configuration.getProperty("default.bind_port", "0")));
		
		try (final ServerSocket socket = new ServerSocket(listenPort)) {
			logger.info("process: listening on {}", socket.getLocalSocketAddress());
			
			final SqsS3Events s3Events = new SqsS3Events(SqsS3Events.OBJECT_CRATED);
			final SqsS3EventPump sqsS3EventPump = new SqsS3EventPump(sqs, s3Events, configuration, "pmregbsm");
			sqsS3EventPump.pollingLoop(new SqsS3EventPump.Consumer() {
				
				@Override
				public void consume(String bucket, String key) {
					try {
						process(bucket, key);
						if (s3.copy(new S3.Url(bucket, key), new S3.Url(bucket, key +
								configuration.getProperty("pmregbsm.processed_suffix", ".processed")))) {
							s3.delete(new S3.Url(bucket, key));
						}
					} catch (Exception e) {
						logger.error("consume", e);
					}
				}
				
			});

		}

	}
	
	private void process(String s3Bucket, String s3Key) throws Exception {
		logger.info("process: s3uri \"s3://{}/{}\"", s3Bucket, s3Key);

		final String filename = s3Key.substring(1 + s3Key.lastIndexOf('/'));
		logger.info("process: filename \"{}\"", filename);

		final boolean skipProcessedFiles = "true"
				.equalsIgnoreCase(configuration.getProperty("pmregbsm.skip_processed_files"));			
		final int workerThreads = Integer
				.parseInt(configuration.getProperty("pmregbsm.worker_threads", "1"));
		final Pattern entryRegexEventi = Pattern.compile(configuration
				.getProperty("pmregbsm.zip_entry_regex.eventi", "(?i)eventi\\.csv"));
		final Pattern entryRegexUtilizzazioni = Pattern.compile(configuration
				.getProperty("pmregbsm.zip_entry_regex.utilizzazioni", "(?i)utilizzazioni\\.csv"));
		final char delimiter = configuration.getProperty("pmregbsm.csv.separator.field", ",").charAt(0);
		final char recordSeparator = configuration.getProperty("pmregbsm.csv.separator.line", "\n").charAt(0);
		final String codiceVoceIncasso = configuration.getProperty("pmregbsm.voce_incasso", "2299");

//		final Gson gson = new GsonBuilder()
//			.setPrettyPrinting()
//			.create();
		final long startTimeMillis = System.currentTimeMillis();

		// connect to database
		try (final Connection connection = mcmdbDS.getConnection()) {
			logger.info("process: connected to {} {}", connection.getMetaData()
					.getDatabaseProductName(), connection.getMetaData().getURL());
			
			// enable auto-commit
			connection.setAutoCommit(true);
			
			// download file
			final S3Object s3Object = s3.getObject(new S3.Url(s3Bucket, s3Key));
			if (null == s3Object) {
				logger.warn("process: unable to download s3uri \"s3://{}/{}\"", s3Bucket, s3Key);
				return;
			}
			final File tempFile = new File(UUID.randomUUID().toString());
			try (final InputStream in = s3Object.getObjectContent();
					final OutputStream out = new FileOutputStream(tempFile)) {
				final byte[] buffer = new byte[1024];
				for (int count = 0; -1 != (count = in.read(buffer)); ) {
					if (count > 0)
						out.write(buffer, 0, count);
				}
			}
			tempFile.deleteOnExit();

			// select PERF_RECORDING_FILE.ID_RECORDING_FILE
			final RecordingDao dao = new RecordingDao(connection, true);
			final AtomicLong idRecordingFile = new AtomicLong(dao
					.getIdRecordingFile(filename, RecordingDao.FILE_TYPE_REGBSM));
			logger.debug("process: idRecordingFile {}", idRecordingFile);
			if (idRecordingFile.get() < 0L) {
				// insert PERF_RECORDING_FILE
				if (!dao.insertRecordingFile(filename, RecordingDao.FILE_TYPE_REGBSM,
						String.format("s3://%s/%s", s3Bucket, s3Key))) {
					logger.error("process: errore insert PERF_RECORDING_FILE");
					return;
				}
				idRecordingFile.set(dao.getLastGeneratedKey());
				logger.debug("process: idRecordingFile {}", idRecordingFile);
			} else if (skipProcessedFiles) {
				logger.info("process: file presente in PERF_RECORDING_FILE");
				return;
			} else {
				// update PERF_RECORDING_FILE
				if (!dao.updateRecordingFile(idRecordingFile.get())) {
					logger.error("process: errore update PERF_RECORDING_FILE");
					return;
				}
			}

			// stats
			final AtomicLong numeroEventi = new AtomicLong(0L);			
			final AtomicLong numeroScarti = new AtomicLong(0L);			
			final AtomicLong numeroUtilizzazioni = new AtomicLong(0L);	
			final Map<String, Long> insertedPm = new HashMap<>(4096);
			final AtomicReference<Exception> exception = new AtomicReference<>(null);

			// unzip and parse file eventi.csv
			try (final InputStream in = new FileInputStream(tempFile);
					final ZipInputStream zip = new ZipInputStream(in);
					final InputStreamReader reader = new InputStreamReader(zip, StandardCharsets.UTF_8);
					final CSVParser parser = new CSVParser(reader, CSVFormat.DEFAULT.withIgnoreSurroundingSpaces()
							.withDelimiter(delimiter).withRecordSeparator(recordSeparator))) {
			
				for (ZipEntry entry; null != (entry = zip.getNextEntry()); ) {	
					logger.debug("process: zip entry {} length {} added on {}",
							entry.getName(), entry.getSize(), new Date(entry.getTime()));					

					if (!entryRegexEventi.matcher(entry.getName()).matches()) {
						logger.debug("process: skipping zip entry {}", entry.getName());
						continue;
					}
					
					// column indexes
					final int idxIdEvento = Integer
							.parseInt(configuration.getProperty("pmregbsm.csv.column.evento.ID_EVENTO", "-1"));
					final int idxDataInizioEvento = Integer
							.parseInt(configuration.getProperty("pmregbsm.csv.column.evento.DATA_INIZIO_EVENTO"));
					final int idxCodiceBaLocale = Integer
							.parseInt(configuration.getProperty("pmregbsm.csv.column.evento.CODICE_BA_LOCALE"));
					final int idxVoceIncasso = Integer
							.parseInt(configuration.getProperty("pmregbsm.csv.column.evento.VOCE_INCASSO"));
//					final int idxDenominazioneLocale = Integer
//							.parseInt(configuration.getProperty("pmregbsm.csv.column.evento.DENOMINAZIONE_LOCALE"));
//					final int idxComuneLocale = Integer
//							.parseInt(configuration.getProperty("pmregbsm.csv.column.evento.COMUNE_LOCALE"));
//					final int idxProvinciaLocale = Integer
//							.parseInt(configuration.getProperty("pmregbsm.csv.column.evento.PROVINCIA_LOCALE"));
					final int idxNumeroProgrammaMusicale = Integer
							.parseInt(configuration.getProperty("pmregbsm.csv.column.evento.NUMERO_PROGRAMMA_MUSICALE"));
//					final int idxSede = Integer
//							.parseInt(configuration.getProperty("pmregbsm.csv.column.evento.SEDE"));
//					final int idxSeprag = Integer
//							.parseInt(configuration.getProperty("pmregbsm.csv.column.evento.SEPRAG"));
					final int idxContabilita = Integer
							.parseInt(configuration.getProperty("pmregbsm.csv.column.evento.CONTABILITA"));
					final int idxNumeroReversale = Integer
							.parseInt(configuration.getProperty("pmregbsm.csv.column.evento.NUMERO_REVERSALE"));
					final int idxPermesso = Integer
							.parseInt(configuration.getProperty("pmregbsm.csv.column.evento.PERMESSO"));
//					final int idxOraInizio = Integer
//							.parseInt(configuration.getProperty("pmregbsm.csv.column.evento.ORA_INIZIO"));
//					final int idxOraFine = Integer
//							.parseInt(configuration.getProperty("pmregbsm.csv.column.evento.ORA_FINE"));

					final Iterator<CSVRecord> records = parser.iterator();
					for (int skipRows = Integer.parseInt(configuration
							.getProperty("pmregbsm.csv.skip_rows", "0")); skipRows > 0; skipRows --)
						if (records.hasNext())
							records.next();

					// spawn worker thread(s)
					final ExecutorService executor = Executors.newCachedThreadPool();
					final AtomicBoolean interrupted = new AtomicBoolean(false);
					for (int i = 0; i < workerThreads; i ++) {
						executor.execute(new Runnable() {

							@Override
							public void run() {
								try {
									process();
								} catch (Exception e) {
									logger.error("run", e);
									exception.compareAndSet(null, e);
									interrupted.set(true);
								}
							}
							
							private void process() throws Exception {
								
								// dedicated thread connection
								try (final Connection connection = mcmdbDS.getConnection()) {
									logger.info("process: connected to {} {}", connection.getMetaData()
											.getDatabaseProductName(), connection.getMetaData().getURL());

									// dedicated thread dao
									final RecordingDao dao = new RecordingDao(connection, false);

									// loop on record(s)
									while (!interrupted.get()) {
											
										// read next record from file
										final CSVRecord record;
										synchronized (records) {
											record = records.hasNext() ? records.next() : null;
										}
										if (null == record) {
											return;
										}
										numeroEventi.incrementAndGet();
//										if (logger.isDebugEnabled()) {
//											logger.debug("process: {}", gson.toJson(record));
//										}
										
//										String idEvento = record.get(idxIdEvento);
										String idEvento = null;
										final String dataInizioEvento = record.get(idxDataInizioEvento);
										final String codiceBaLocale = record.get(idxCodiceBaLocale);
										final String voceIncasso = record.get(idxVoceIncasso);
//										final String denominazioneLocale = record.get(idxDenominazioneLocale);
//										final String comuneLocale = record.get(idxComuneLocale);
//										final String provinciaLocale = record.get(idxProvinciaLocale);
										String numeroProgrammaMusicale = record.get(idxNumeroProgrammaMusicale);
//										final String sede = record.get(idxSede);
//										final String seprag = record.get(idxSeprag);
										final String contabilita = record.get(idxContabilita);
										final String numeroReversale = record.get(idxNumeroReversale);
										final String permesso = record.get(idxPermesso);
//										final String oraInizio = record.get(idxOraInizio);
//										final String oraFine = record.get(idxOraFine);
										final String idEventoFromFile = -1 == idxIdEvento ? null : record.get(idxIdEvento);
										
										// skip if already present in PERF_RECORDING_RECORD
										if (dao.isNumeroProgrammaMusicaleLavorato(numeroProgrammaMusicale)) {
											logger.debug("process: NUMERO_PROGRAMMA_MUSICALE già lavorato");
											continue;
										}
										
										// validate NUMERO_PROGRAMMA_MUSICALE
										long numeroPm;
										try {
											numeroPm = Long.parseLong(numeroProgrammaMusicale);
											numeroProgrammaMusicale = Long.toString(numeroPm);
										} catch (Exception e) {
											numeroScarti.incrementAndGet();
											dao.insertRecordingScarto(idRecordingFile.get(), idEventoFromFile, dataInizioEvento,
													codiceBaLocale, voceIncasso, numeroProgrammaMusicale, "NUMERO_PROGRAMMA_MUSICALE non valido");
											logger.error("process: invalid NUMERO_PROGRAMMA_MUSICALE", e);
											continue;
										}
										
										// CR 117497
										// se nel tracciato id_evento è null procedo come adesso										
										// sennò cerco perf_manifestazione.id_evento
										//  - se non lo trovo cerco come adesso 
										//  - altrimenti uso id_evento trovato
										if (!Strings.isNullOrEmpty(idEventoFromFile)) {
											final Long idEventoFromDb = dao.getIdEvento(idEventoFromFile);
											if (null != idEventoFromDb)
												idEvento = Long.toString(idEventoFromDb);
										}
										if (null == idEvento) {
											// lookup PERF_EVENTI_PAGATI
											List<Long> idEventi = dao.getIdEventi(dataInizioEvento, codiceBaLocale, voceIncasso);
//											logger.debug("process: idEventi {}", idEventi);
											// no match or multiple matches
											if (idEventi.size() < 1) {
												numeroScarti.incrementAndGet();
												dao.insertRecordingScarto(idRecordingFile.get(), idEventoFromFile, dataInizioEvento,
														codiceBaLocale, voceIncasso, numeroProgrammaMusicale, "ID_EVENTO non trovato");
												logger.debug("process: ID_EVENTO non trovato");
											} else if (idEventi.size() > 1) {
												numeroScarti.incrementAndGet();
												dao.insertRecordingScarto(idRecordingFile.get(), idEventoFromFile, dataInizioEvento,
														codiceBaLocale, voceIncasso, numeroProgrammaMusicale, "ID_EVENTO multipli " + idEventi);
												logger.debug("process: ID_EVENTO multipli " + idEventi);
												continue;
											} else  { //idEventi.size() == 1
												idEvento = Long.toString(idEventi.get(0));
												logger.debug("process: ID_EVENTO trovato {}", idEvento);
											}
										}

										// update PERF_RECORDING_PM_ID set ID = LAST_INSERT_ID(ID+1)
										final long idProgrammaMusicale = dao.nextIdProgrammaMusicale();
//										logger.debug("process: idProgrammaMusicale {}", idProgrammaMusicale);
										if (idProgrammaMusicale < 1L) {
											numeroScarti.incrementAndGet();
											dao.insertRecordingScarto(idRecordingFile.get(), idEventoFromFile, dataInizioEvento,
													codiceBaLocale, voceIncasso, numeroProgrammaMusicale, "Errore generazione ID_PROGRAMMA_MUSICALE");
											logger.error("process: errore generazione ID_PROGRAMMA_MUSICALE");
											continue;											
										}
										
										// insert PERF_RECORDING_RECORD
										if (!dao.insertRecordingRecord(idRecordingFile.get(), idProgrammaMusicale, numeroProgrammaMusicale)) {
											numeroScarti.incrementAndGet();
											dao.insertRecordingScarto(idRecordingFile.get(), idEventoFromFile, dataInizioEvento,
													codiceBaLocale, voceIncasso, numeroProgrammaMusicale, "Errore inserimento PERF_RECORDING_RECORD");
											logger.error("process: errore inserimento PERF_RECORDING_RECORD");
											continue;																					
										}

										// insert PERF_PROGRAMMA_MUSICALE
										if (!dao.insertProgrammaMusicale(idProgrammaMusicale, numeroPm, statoPm)) {
											numeroScarti.incrementAndGet();
											dao.deleteRecordingRecord(idProgrammaMusicale);
											dao.insertRecordingScarto(idRecordingFile.get(), idEventoFromFile, dataInizioEvento,
													codiceBaLocale, voceIncasso, numeroProgrammaMusicale, "Errore inserimento PERF_PROGRAMMA_MUSICALE");
											logger.error("process: errore inserimento PERF_PROGRAMMA_MUSICALE");
											continue;
										}

										// insert PERF_MOVIMENTO_CONTABILE
										if (!dao.insertMovimentoContabile(idProgrammaMusicale, idEvento, codiceVoceIncasso,
												numeroProgrammaMusicale, numeroReversale, permesso, contabilita)) {
											numeroScarti.incrementAndGet();
											dao.deleteRecordingRecord(idProgrammaMusicale);
											dao.deleteProgrammaMusicale(idProgrammaMusicale);
											dao.insertRecordingScarto(idRecordingFile.get(), idEventoFromFile, dataInizioEvento,
													codiceBaLocale, voceIncasso, numeroProgrammaMusicale, "Errore inserimento PERF_MOVIMENTO_CONTABILE");
											logger.error("process: errore inserimento PERF_MOVIMENTO_CONTABILE");
											continue;
										}

										// save inserted idProgrammaMusicale
										insertedPm.put(numeroProgrammaMusicale, idProgrammaMusicale);
									}								
								
								}
							
							}
							
						});
					}
					
					// wait worker thread(s)
					executor.shutdown();
					while (!executor.awaitTermination(5L, TimeUnit.SECONDS)) {
						logger.debug("process: numeroEventi {} ", numeroEventi);
					}
										
				}
				
			}
			
			// eventually re-throw exception
			if (null != exception.get()) {
				// update PERF_RECORDING_FILE
				if (!dao.updateRecordingFile(idRecordingFile.get(), numeroEventi.get(),
						numeroScarti.get(), numeroUtilizzazioni.get(), exception.get())) {
					logger.error("process: errore update PERF_RECORDING_FILE");
				}
				throw exception.get();
			}
			
			// unzip and parse file utilizzazioni.csv
			try (final InputStream in = new FileInputStream(tempFile);
					final ZipInputStream zip = new ZipInputStream(in);
					final InputStreamReader reader = new InputStreamReader(zip, StandardCharsets.UTF_8);
					final CSVParser parser = new CSVParser(reader, CSVFormat.DEFAULT.withIgnoreSurroundingSpaces()
							.withDelimiter(delimiter).withRecordSeparator(recordSeparator))) {
			
				for (ZipEntry entry; null != (entry = zip.getNextEntry()); ) {	
					logger.debug("process: zip entry {} length {} added on {}",
							entry.getName(), entry.getSize(), new Date(entry.getTime()));					

					if (!entryRegexUtilizzazioni.matcher(entry.getName()).matches()) {
						logger.debug("process: skipping zip entry {}", entry.getName());
						continue;
					}
					
					// column indexes
					final int idxCodiceOpera = Integer
							.parseInt(configuration.getProperty("pmregbsm.csv.column.utilizzazione.CODICE_OPERA"));
					final int idxTitoloComposizione = Integer
							.parseInt(configuration.getProperty("pmregbsm.csv.column.utilizzazione.TITOLO_COMPOSIZIONE"));
					final int idxCompositore = Integer
							.parseInt(configuration.getProperty("pmregbsm.csv.column.utilizzazione.COMPOSITORE"));
					final int idxNumeroProgrammaMusicale = Integer
							.parseInt(configuration.getProperty("pmregbsm.csv.column.utilizzazione.NUMERO_PROGRAMMA_MUSICALE"));
					final int idxDurata = Integer
							.parseInt(configuration.getProperty("pmregbsm.csv.column.utilizzazione.DURATA"));
					final int idxDurataInferiore30Sec = Integer
							.parseInt(configuration.getProperty("pmregbsm.csv.column.utilizzazione.DURATA_INFERIORE_30SEC"));
					final int idxAutori = Integer
							.parseInt(configuration.getProperty("pmregbsm.csv.column.utilizzazione.AUTORI"));
					final int idxInterpreti = Integer
							.parseInt(configuration.getProperty("pmregbsm.csv.column.utilizzazione.INTERPRETI"));
//					final int idxIsrc = Integer
//							.parseInt(configuration.getProperty("pmregbsm.csv.column.utilizzazione.ISRC"));
//					final int idxIswc = Integer
//							.parseInt(configuration.getProperty("pmregbsm.csv.column.utilizzazione.ISWC"));
					
					final Iterator<CSVRecord> records = parser.iterator();
					for (int skipRows = Integer.parseInt(configuration
							.getProperty("pmregbsm.csv.skip_rows", "0")); skipRows > 0; skipRows --)
						if (records.hasNext())
							records.next();

					// spawn worker thread(s)
					final ExecutorService executor = Executors.newCachedThreadPool();
					final AtomicBoolean interrupted = new AtomicBoolean(false);
					for (int i = 0; i < workerThreads; i ++) {
						executor.execute(new Runnable() {

							@Override
							public void run() {
								try {
									process();
								} catch (Exception e) {
									logger.error("run", e);
									exception.compareAndSet(null, e);
									interrupted.set(true);
								}
							}

							private void process() throws Exception {
								
								// dedicated thread connection
								try (final Connection connection = mcmdbDS.getConnection()) {
									logger.info("process: connected to {} {}", connection.getMetaData()
											.getDatabaseProductName(), connection.getMetaData().getURL());

									// dedicated thread dao
									final RecordingDao dao = new RecordingDao(connection, false);

									// loop on record(s)
									while (!interrupted.get()) {
											
										// read next record from file
										final CSVRecord record;
										synchronized (records) {
											record = records.hasNext() ? records.next() : null;
										}
										if (null == record) {
											return;
										}
										numeroUtilizzazioni.incrementAndGet();
//										if (logger.isDebugEnabled()) {
//											logger.debug("process: {}", gson.toJson(record));
//										}

										final String codiceOpera;
										final String titoloComposizione;
										final String compositore;
										String numeroProgrammaMusicale = null;
										final String durata;
										String durataInferiore30Sec = null;
										final String autori;
										final String interpreti;
										try {
											codiceOpera = record.get(idxCodiceOpera);
											titoloComposizione = record.get(idxTitoloComposizione);
											compositore = record.get(idxCompositore);
											numeroProgrammaMusicale = record.get(idxNumeroProgrammaMusicale);
											durata = record.get(idxDurata);
											durataInferiore30Sec = record.get(idxDurataInferiore30Sec);
											autori = record.get(idxAutori);
											interpreti = record.get(idxInterpreti);
										} catch (Exception e) {
											logger.error("process: invalid record {}", record.getRecordNumber());
											logger.error("", e);
											continue;
										}
//										final String isrc = record.get(idxIsrc);
//										final String iswc = record.get(idxIswc);

										long numeroPm;
										try {
											numeroPm = Long.parseLong(numeroProgrammaMusicale);
											numeroProgrammaMusicale = Long.toString(numeroPm);
										} catch (Exception e) {
											logger.error("process: invalid NUMERO_PROGRAMMA_MUSICALE record {}", record.getRecordNumber());
											logger.error("", e);
											continue;
										}
										
										// raise short song flag using actual duration
										if (null != durataInferiore30Sec)
											durataInferiore30Sec = "X".equalsIgnoreCase(durataInferiore30Sec.trim()) ? "1" : null;
										if (Strings.isNullOrEmpty(durataInferiore30Sec) &&
												!Strings.isNullOrEmpty(durata)) {
											try {
												final int seconds = Integer.parseInt(durata);
												if (seconds > 0 && seconds <= 30)
													durataInferiore30Sec = "1";
											} catch (Exception ignore) { }											
										}

										// CR 117497
										// in caso di programma musicale scartato inserire l'utilizzazione in un tabella
										// scarti PERF_RECORDING_UTILIZZAZIONE_SCARTO dove userò il numero_programma_musicale
										// anziché id_programma_musicale e ID_RECORDING_FILE
										
										// PERF_PROGRAMMA_MUSICALE.ID_PROGRAMMA_MUSICALE
										final Long idProgrammaMusicale = insertedPm.get(numeroProgrammaMusicale);
//										logger.debug("process: idProgrammaMusicale {}", idProgrammaMusicale);
										if (null == idProgrammaMusicale) {
											// insert PERF_RECORDING_UTILIZZAZIONE_SCARTO
											if (!dao.insertUtilizzazioneScarto(idRecordingFile.get(), codiceOpera, titoloComposizione,
													compositore, autori, interpreti, numeroProgrammaMusicale, durata, durataInferiore30Sec)) {
												logger.error("process: errore inserimento PERF_RECORDING_UTILIZZAZIONE_SCARTO");
												continue;
											}
										} else {
											// insert PERF_UTILIZZAZIONE
											if (!dao.insertUtilizzazione(idProgrammaMusicale, codiceOpera, titoloComposizione,
													compositore, autori, interpreti, numeroProgrammaMusicale, durata, durataInferiore30Sec)) {
												logger.error("process: errore inserimento PERF_UTILIZZAZIONE");
												continue;
											}
											
										}
										
									}
								
								}
							
							}
							
						});
					}
					
					// wait worker thread(s)
					executor.shutdown();
					while (!executor.awaitTermination(5L, TimeUnit.SECONDS)) {
						logger.debug("process: numeroUtilizzazioni {}", numeroUtilizzazioni);
					}
										
				}
				
			}
			
			// stats
			logger.debug("process: numeroEventi {} numeroUtilizzazioni {}", numeroEventi, numeroUtilizzazioni);

			// update PERF_RECORDING_FILE
			if (!dao.updateRecordingFile(idRecordingFile.get(), numeroEventi.get(),
					numeroScarti.get(), numeroUtilizzazioni.get(), exception.get())) {
				logger.error("process: errore update PERF_RECORDING_FILE");
			}

			// re-throw exception
			if (null != exception.get()) {
				throw exception.get();
			}

		}
		
		// timing
		final long elapsedMillis = System.currentTimeMillis() - startTimeMillis;
		final long elapsedSeconds = elapsedMillis / 1000L;
		logger.info("process: completed in {}h {}m {}s {}ms",
				elapsedSeconds / 3600,
				(elapsedSeconds % 3600) / 60,
				(elapsedSeconds % 3600) % 60,
				elapsedMillis % 1000);
	}
	
}
