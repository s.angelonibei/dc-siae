package com.alkemytech.sophia.pm.bonifica;

import java.net.ServerSocket;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.alkemytech.sophia.pm.McmdbDAO;
import com.alkemytech.sophia.pm.McmdbDataSource;
import com.alkemytech.sophia.pm.load.ResumeDb;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Scopes;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

/**
 * @author roberto.cerfogli <roberto@seqrware.com>
 */
public class PmBonifica {
	
	private static final Logger logger = LoggerFactory.getLogger(PmBonifica.class);
	
	private static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// amazon service(s)
			bind(S3.class)
				.in(Scopes.SINGLETON);
			bind(SQS.class)
				.in(Scopes.SINGLETON);
			// data source(s)
			bind(DataSource.class)
				.annotatedWith(Names.named("MCMDB"))
				.to(McmdbDataSource.class)
				.in(Scopes.SINGLETON);
			// data access object(s)
			bind(McmdbDAO.class)
				.in(Scopes.SINGLETON);
			// resume db
			bind(ResumeDb.class).asEagerSingleton();
			// self
			bind(PmBonifica.class).asEagerSingleton();
		}
		
	}
		
	public static void main(String[] args) {
		try {
			final Injector injector = Guice
					.createInjector(new GuiceModuleExtension(args, "/sophia-bonifica-pm.properties"));
			final PmBonifica instance = injector
					.getInstance(PmBonifica.class)
					.startup();
			try {
				instance.process();
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}
	
	private final Properties configuration;
	private final McmdbDAO dao;

	@Inject
	protected PmBonifica(@Named("configuration") Properties configuration,
			McmdbDAO dao) {
		super();
		this.configuration = configuration;
		this.dao = dao;
	}
	
	public PmBonifica startup() {
		return this;
	}
	
	public PmBonifica shutdown() {
		return this;
	}

	private void process() throws Exception {

		final int bindPort = Integer.parseInt(configuration.getProperty("pmbonifica.bind_port",
				configuration.getProperty("default.bind_port", "0")));
		try (final ServerSocket socket = new ServerSocket(bindPort)) {
			logger.info("load: listening on {}", socket.getLocalSocketAddress());
			// format fake message
			final Calendar calendar = Calendar.getInstance();
			final JsonObject inputJsonBody = new JsonObject();
			final JsonObject outputJson = new JsonObject();
			final int year = Integer.parseInt(configuration
					.getProperty("pmbonifica.local.year", Integer.toString(calendar.get(Calendar.YEAR))));
			final int month = Integer.parseInt(configuration
					.getProperty("pmbonifica.local.month", Integer.toString(1 + calendar.get(Calendar.MONTH))));
			final int day = Integer.parseInt(configuration
					.getProperty("pmbonifica.local.day", Integer.toString(calendar.get(Calendar.DAY_OF_MONTH))));
			inputJsonBody.addProperty("year", year);
			inputJsonBody.addProperty("month", month);
			inputJsonBody.addProperty("day", day);
			// process message
			processMessage(inputJsonBody, outputJson);
		}

	}
	
	private Map<String,Set<String>> loadAutomaticColumnsConfig() throws SQLException {
		
		// select columns for automatic updates
		List<Map<String,String>> rows = dao
				.executeQuery("pmbonifica.sql.select.perf_bonifica_automatica");
		logger.debug("rows {}", rows);
		
		// build automatic column(s) map
		final Map<String,Set<String>> automaticColumnsMap = new HashMap<>();
		for (Map<String,String> row : rows) {
			final String tableName = row.get("nome_tabella").toLowerCase();
			Set<String> columnNames = automaticColumnsMap.get(tableName);
			if (null == columnNames) {
				columnNames = new HashSet<>();
				automaticColumnsMap.put(tableName, columnNames);
			}
			columnNames.add(row.get("nome_colonna").toLowerCase());
		}
		logger.debug("automaticColumnsMap {}", automaticColumnsMap);

		return automaticColumnsMap;
	}
	
	public void processMessage(JsonObject inputJsonBody, JsonObject outputJson) throws Exception {

		// parse input json
		logger.debug("inputJsonBody {}", inputJsonBody);
		final String year = GsonUtils.getAsString(inputJsonBody, "year");
		final String month = GsonUtils.getAsString(inputJsonBody, "month");
		final String day = GsonUtils.getAsString(inputJsonBody, "day");
		
		// data_bonifica
		final String dataBonifica = String.format(configuration
				.getProperty("pmbonifica.java_date_format"), 
					Integer.parseInt(year), Integer.parseInt(month),
						Integer.parseInt(day));
		logger.debug("dataBonifica {}", dataBonifica);
		
		// diff table(s)
		final JsonArray results = new JsonArray();
		final Map<String,Set<String>> automaticColumnsConfig = loadAutomaticColumnsConfig();
		final String[] tableNames = TextUtils.split(configuration
				.getProperty("pmbonifica.table_names"), ",");
		for (String tableName : tableNames) {
			results.add(diff(tableName, dataBonifica, automaticColumnsConfig));
		}		
		outputJson.add("pmBonificaResults", results);
		logger.debug("outputJson {}", outputJson);
		
	}
	
	private Set<String> getKeysWithDifferentValue(Map<String,String> left, Map<String,String> right) {
		final Set<String> keysWithDifferentValue = new HashSet<>();
		for (Map.Entry<String,String> leftEntry : left.entrySet()) {
			final String rightValue = right.get(leftEntry.getKey());
			boolean different = true;
			if (null == rightValue) {
				if (null == leftEntry.getValue()) {
					different = false;
				}
			} else if (null != leftEntry.getValue()) {
				different = !rightValue.equals(leftEntry.getValue());
			}
			if (different) {
				keysWithDifferentValue.add(leftEntry.getKey());
			}
		}
		return keysWithDifferentValue;
	}

	private JsonObject diff(String tableName, String dataBonifica, Map<String,Set<String>> automaticColumnsConfig) throws SQLException {
				
		// additional columns for modified table(s)
		final Map<String,String> modifiedWhere = new HashMap<>();
		modifiedWhere.put("data_bonifica", dataBonifica);
		modifiedWhere.put("record_elaborato", "0"); // non elaborato
		
		// select modified rows
		tableName = tableName.toLowerCase();
		final String modifiedTableSuffix = configuration
				.getProperty("pmbonifica.table_name_suffix", "_bonifica")
					.toLowerCase();
		final List<Map<String,String>> modifiedRows = dao
				.executeQuery("pmbonifica.sql.select." + tableName + modifiedTableSuffix, modifiedWhere);
		logger.debug("modifiedRows size {}", modifiedRows.size());
		final long numeroRigheBonifica = modifiedRows.size();

		long numeroInserimenti = 0L;
		long numeroModificheAutomatiche = 0L;
		long numeroModificheManuali = 0L;
		long numeroRecordIdentici = 0L;
		for (Map<String,String> modifiedRow : modifiedRows) {
			logger.debug("modifiedRow {}", modifiedRow);
			
			// select original row
			final Map<String,String> originalRow = dao
					.executeSingleRowQuery("pmbonifica.sql.select." + tableName, modifiedRow);
			logger.debug("originalRow {}", originalRow);

			if (null == originalRow) { // insert new

				if (1 != dao.executeUpdate("pmbonifica.sql.insert." + tableName, modifiedRow)) {
					logger.error("error inserting into " + tableName);
				} else {
					numeroInserimenti ++;
					modifiedWhere.put("record_elaborato", "1"); // inserimento automatico
				}
				
			} else { // update existing

				// diff column(s)
				final Set<String> differentColumns = getKeysWithDifferentValue(modifiedRow, originalRow);
				differentColumns.addAll(getKeysWithDifferentValue(originalRow, modifiedRow));
				logger.debug("differentColumns {}", differentColumns);

				// automatic/manual update
				if (!differentColumns.isEmpty()) {
					final Set<String> automaticColumns = automaticColumnsConfig.get(tableName);
					logger.debug("automaticColumns {}", automaticColumns);
					if (null == automaticColumns ||
							automaticColumns.containsAll(differentColumns)) {
						logger.debug("automatic update");
						if (1 != dao.executeUpdate("pmbonifica.sql.update." + tableName, modifiedRow)) {
							logger.error("error updating " + tableName);
						} else {
							numeroModificheAutomatiche ++;
							modifiedWhere.put("record_elaborato", "2"); // modifica automatica
						}
					} else {
						numeroModificheManuali ++;
						modifiedWhere.put("record_elaborato", "3"); // modifica manuale
					}
				} else {
					modifiedWhere.put("record_elaborato", "4"); // record identici				
					numeroRecordIdentici ++;
				}

			}					

			// update modified row
			modifiedRow.putAll(modifiedWhere);
			if (1 != dao.executeUpdate("pmbonifica.sql.update." + tableName + modifiedTableSuffix, modifiedRow)) {
				logger.error("error updating " + tableName + modifiedTableSuffix);
			}
		}

		final JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("nomeTabella", tableName);
		jsonObject.addProperty("dataBonifica", dataBonifica);
		jsonObject.addProperty("numeroRigheBonifica", numeroRigheBonifica);
		jsonObject.addProperty("numeroInserimenti", numeroInserimenti);
		jsonObject.addProperty("numeroModificheAutomatiche", numeroModificheAutomatiche);
		jsonObject.addProperty("numeroModificheManuali", numeroModificheManuali);
		jsonObject.addProperty("numeroRecordIdentici", numeroRecordIdentici);
		return jsonObject;

	}
	
	
}
