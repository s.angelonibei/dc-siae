package com.alkemytech.sophia.pm.need.pojo;

import java.util.ArrayList;
import java.util.List;

public class Record {

	private Cartella cartella;
	private List<Foglio> fogli;
	
	public Record() {
		super();
		fogli = new ArrayList<>();
	}
	
	public Cartella getCartella() {
		return cartella;
	}
	public void setCartella(Cartella cartella) {
		this.cartella = cartella;
	}
	public List<Foglio> getFogli() {
		return fogli;
	}
	public void setFogli(List<Foglio> fogli) {
		this.fogli = fogli;
	}
	
}
