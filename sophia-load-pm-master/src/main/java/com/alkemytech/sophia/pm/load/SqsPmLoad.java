package com.alkemytech.sophia.pm.load;

import java.net.ServerSocket;
import java.util.Properties;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.sqs.McmdbMessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.MessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.SqsMessageHelper;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.alkemytech.sophia.pm.McmdbDataSource;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Scopes;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

/**
 * @author roberto.cerfogli <roberto@seqrware.com>
 */
public class SqsPmLoad {
	
	private static final Logger logger = LoggerFactory.getLogger(SqsPmLoad.class);
		
	private static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// amazon service(s)
			bind(S3.class)
				.in(Scopes.SINGLETON);
			bind(SQS.class)
				.in(Scopes.SINGLETON);
			// data source(s)
			bind(DataSource.class)
				.annotatedWith(Names.named("MCMDB"))
				.to(McmdbDataSource.class)
				.in(Scopes.SINGLETON);
			// message deduplicator(s)
			bind(MessageDeduplicator.class)
				.to(McmdbMessageDeduplicator.class)
				.in(Scopes.SINGLETON);
			// resume db
			bind(ResumeDb.class).asEagerSingleton();
			// self
			bind(PmLoad.class).asEagerSingleton();
			bind(SqsPmLoad.class).asEagerSingleton();									
		}
		
	}
	
	public static void main(String[] args) {
		try {
			final Injector injector = Guice
					.createInjector(new GuiceModuleExtension(args, "/sophia-load-pm.properties"));
			final SqsPmLoad instance = injector
					.getInstance(SqsPmLoad.class)
					.startup();
			try {
				instance.process();
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}
	
	private final Properties configuration;
	private final SQS sqs;
	private final MessageDeduplicator messageDeduplicator;
	private final PmLoad pmLoad;

	@Inject
	protected SqsPmLoad(@Named("configuration") Properties configuration,
			SQS sqs,
			MessageDeduplicator messageDeduplicator,
			PmLoad pmLoad) {
		super();
		this.configuration = configuration;
		this.sqs = sqs;
		this.messageDeduplicator = messageDeduplicator;
		this.pmLoad = pmLoad;
	}
	
	protected SqsPmLoad startup() {
		sqs.startup();
		pmLoad.startup();
		return this;
	}

	protected SqsPmLoad shutdown() {
		sqs.shutdown();
		pmLoad.shutdown();
		return this;
	}

	public void process() throws Exception {
		final int listenPort = Integer.parseInt(configuration.getProperty("pmload.bind_port",
				configuration.getProperty("default.bind_port", "0")));
		try (final ServerSocket socket = new ServerSocket(listenPort)) {
			logger.info("load: listening on {}", socket.getLocalSocketAddress());				
			
			final SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration, "pmload.sqs");
			sqsMessagePump.pollingLoop(messageDeduplicator, new SqsMessagePump.Consumer() {
				
				private JsonObject output;
				private JsonObject error;

				@Override
				public JsonObject getStartedMessagePayload(JsonObject message) {
					output = null;
					error = null;
					return SqsMessageHelper.formatContext();
				}

				@Override
				public boolean consumeMessage(JsonObject message) {
					try {
						output = new JsonObject();
						processMessage(message, output);
						error = null;
						return true;
					} catch (Exception e) {
						output = null;
						error = SqsMessageHelper.formatError(e);
						return false;
					}
				}

				@Override
				public JsonObject getCompletedMessagePayload(JsonObject message) {
					return output;
				}

				@Override
				public JsonObject getFailedMessagePayload(JsonObject message) {
					return error;
				}

			});
		}
		
	}
	
	private void processMessage(JsonObject input, JsonObject output) throws Exception {
//	{
//		body:{
//			"year":2017,
//			"month":10,
//			"bucket":"siae-sophia-datalake",
//			"folder":"pm-dump/2017_10",
//			"entities":[
//			    {
//			    	"entity":"PERF_PROGRAMMA_MUSICALE",
//			    	"filename":"PERF_PROGRAMMA_MUSICALE.csv.gz",
//			    	"url":"s3://siae-sophia-datalake/pm-dump/2017_10/programma_musicale.csv.gz",
//			    	"rows":96
//			    },{
//			    	"entity":"PERF_MANIFESTAZIONE",
//			    	"filename":"PERF_MANIFESTAZIONE.csv.gz",
//			    	"url":"s3://siae-sophia-datalake/pm-dump/2017_10/manifestazione.csv.gz",
//			    	"rows":87
//			    },{
//			    	"entity":"PERF_UTILIZZAZIONE",
//			    	"filename":"PERF_UTILIZZAZIONE.csv.gz",
//			    	"url":"s3://siae-sophia-datalake/pm-dump/2017_10/utilizzazione.csv.gz",
//			    	"rows":46
//			    },{
//			    	"entity":"PERF_MOVIMENTO_CONTABILE",
//			    	"filename":"PERF_MOVIMENTO_CONTABILE.csv.gz",
//			    	"url":"s3://siae-sophia-datalake/pm-dump/2017_10/movimento_contabile.csv.gz",
//			    	"rows":100
//			    }
//			]
//		},
//		"header": {
//			"queue": "dev_to_process_pmload",
//			"timestamp": "2017-05-09T01:15:00.000000+02:00",
//			"uuid": "b3e00ae5-5145-40e6-96d4-f63cac680aaa",
//			"sender": "pmdump-filter"
//		}
//	}
		
		pmLoad.processMessage(input.getAsJsonObject("body"), output);
		
	}
	
}
