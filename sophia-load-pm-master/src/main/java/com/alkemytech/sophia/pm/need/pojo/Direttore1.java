package com.alkemytech.sophia.pm.need.pojo;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class Direttore1 extends NeedPojo {

	// TRK 60
	// [1,2] "60"
//	private String cartella;				// [3] "A"
//	private String codiceSede;				// [4,5] "03"
//	private String codiceProvincia;			// [6,8] "032"
//	private String unitaTerritoriale;		// [9,10] "08"
//	private String codiceVoceIncasso;		// [11,14] "2222"
//	private String progressivoCartella;		// [15,16] "01"
//	private String contabilita;				// [17,22] "201608"
	private String numeroProgrammaMusicale;	// [23,30] "90195603"
	private String direttoreEsecuzione;		// [31,62] "BATTISTINI LORIS                "
	private String posizioneSiae;			// [63,71] "000000000"
	private String indirizzo;				// [72,101] "VIA RIVA 405                  "
	private String cap;						// [102,106] "41055"
	private String comune;					// [107,126] "MONTESE             "
	private String provincia;				// [127,128] "MO"
	// [129,180] (unused)
	
	public Direttore1(byte[] buffer) {
		super();
//		this.cartella = toString(buffer, 2, 1);
//		this.codiceSede = toString(buffer, 3, 2);
//		this.codiceProvincia = toString(buffer, 5, 3);
//		this.unitaTerritoriale = toString(buffer, 8, 2);
//		this.codiceVoceIncasso = toString(buffer, 10, 4);
//		this.progressivoCartella = toString(buffer, 14, 2);
//		this.contabilita = toString(buffer, 16, 6);
		this.numeroProgrammaMusicale = toString(buffer, 22, 8);
		this.direttoreEsecuzione = toString(buffer, 30, 32);
		this.posizioneSiae = toString(buffer, 62, 9);
		this.indirizzo = toString(buffer, 71, 30);
		this.cap = toString(buffer, 101, 5);
		this.comune = toString(buffer, 106, 20);
		this.provincia = toString(buffer, 126, 2);
	}
	
	public String getNumeroProgrammaMusicale() {
		return numeroProgrammaMusicale;
	}
	public void setNumeroProgrammaMusicale(String numeroProgrammaMusicale) {
		this.numeroProgrammaMusicale = numeroProgrammaMusicale;
	}
	public String getDirettoreEsecuzione() {
		return direttoreEsecuzione;
	}
	public void setDirettoreEsecuzione(String direttoreEsecuzione) {
		this.direttoreEsecuzione = direttoreEsecuzione;
	}
	public String getPosizioneSiae() {
		return posizioneSiae;
	}
	public void setPosizioneSiae(String posizioneSiae) {
		this.posizioneSiae = posizioneSiae;
	}
	public String getIndirizzo() {
		return indirizzo;
	}
	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}
	public String getCap() {
		return cap;
	}
	public void setCap(String cap) {
		this.cap = cap;
	}
	public String getComune() {
		return comune;
	}
	public void setComune(String comune) {
		this.comune = comune;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	
}
