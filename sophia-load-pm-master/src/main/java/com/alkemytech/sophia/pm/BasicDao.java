package com.alkemytech.sophia.pm;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import com.google.common.base.Strings;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public abstract class BasicDao {

	public static final int ERROR_DEADLOCK = 1213;
	public static final int ERROR_DUPLICATE_KEY = 1062;
	
	protected String toLong(String value) {
		if (Strings.isNullOrEmpty(value))
			return "NULL";
		if ("$".equals(value))
			return "NULL";
		try {
			return Long.toString(Long.parseLong(value));
		} catch (Exception ignore) { }
		return "NULL";
	}

	protected String toEuro(String value) {
		if (Strings.isNullOrEmpty(value))
			return "NULL";
		if ("$".equals(value))
			return "NULL";
		try {
			long number = Long.parseLong(value);
			return String.format("%01d.%02d", number / 100L, number % 100L);
		} catch (Exception ignore) { }
		return "NULL";
	}
	
	protected String toVarchar(String value) {
		if (Strings.isNullOrEmpty(value))
			return "NULL";
		if ("$".equals(value))
			return "NULL";
		return String.format("'%s'", value
				.replace("\'", "\'\'").replace("\\", "\\\\"));
	}

	protected String toDateTime(String value) {
		if (Strings.isNullOrEmpty(value))
			return "NULL";
		if ("$".equals(value))
			return "NULL";
		if ("00000000".equals(value))
			return "NULL";
		return String.format("STR_TO_DATE('%s','%%d%%m%%Y')", value);
	}

	protected String toVarchar(Exception value) {
		if (null != value)
			try {
				final StringWriter out = new StringWriter();
				value.printStackTrace(new PrintWriter(out));
				return toVarchar(out.toString());
			} catch (Exception e) {}
		return "NULL";
	}

	public int toSeconds(String durata) {
		if ("-".equals(durata))
			return 0;
		if ("$".equals(durata))
			return 0;
		if (!Strings.isNullOrEmpty(durata))
			try {
				final int length = durata.length();
				switch (length) {
				case 1: // s
				case 2: // ss
					return Integer.parseInt(durata);
				case 3: // mss
				case 4: // mmss
				case 5: // mmmss
					return Integer.parseInt(durata.substring(length - 2)) +
							60 * Integer.parseInt(durata.substring(0, length - 2));
				}
			} catch (NumberFormatException | NullPointerException ignore) { }
		return 0;
	}
	
	public int toDuration(String seconds) {
		if ("-".equals(seconds))
			return 0;
		if ("$".equals(seconds))
			return 0;
		if (!Strings.isNullOrEmpty(seconds))
			try {
				int duration = Integer.parseInt(seconds);
				return (duration % 60) +
						((duration / 60) % 60) * 100 +
						(duration / 3600) * 10000;
			} catch (NumberFormatException | NullPointerException ignore) { }
		return 0;
	}

	protected Integer getAsInteger(ResultSet resultSet, int columnIndex) throws SQLException {
		final int value = resultSet.getInt(columnIndex);
		return resultSet.wasNull() ? null : value;
	}
		
	protected Long getAsLong(ResultSet resultSet, int columnIndex) throws SQLException {
		final long value = resultSet.getLong(columnIndex);
		return resultSet.wasNull() ? null : value;
	}

	protected Date getAsDate(ResultSet resultSet, int columnIndex) throws SQLException {
		final Timestamp value = resultSet.getTimestamp(columnIndex);
		return null == value ? null : new Date(value.getTime());
	}

}
