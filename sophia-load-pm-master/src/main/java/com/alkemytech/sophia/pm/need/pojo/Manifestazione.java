package com.alkemytech.sophia.pm.need.pojo;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class Manifestazione extends NeedPojo {

	// TRK 50
	// [1,2] "50"
//	private String cartella;				// [3] "A"
//	private String codiceSede;				// [4,5] "03"
//	private String codiceProvincia;			// [6,8] "032"
//	private String unitaTerritoriale;		// [9,10] "08"
//	private String codiceVoceIncasso;		// [11,14] "2222"
//	private String progressivoCartella;		// [15,16] "01"
//	private String contabilita;				// [17,22] "201608"
	private String numeroProgrammaMusicale;	// [23,30] "90195603"
	private String denominazioneLocale;		// [31,50] "ALL APERTO          "
	private String localita;				// [51,70] "VALSAMOGGIA         "
	private String dataInizio;				// [71,78] "08072016"
	private String dataFine;				// [79,86] "08072016"
	private String tipologiaLocale;			// [87,88] "00"
	private String dalleOre;				// [89,92] "1800"
	private String alleOre;					// [93,96] "2400"
	private String totaleMinuti;			// [97,99] "000"
	private String provinciaLocale;			// [100,101] "BO"
	private String incasso;					// [102,113] "000000000000"
	private String tipoValuta;				// [114] "E"
	private String incassoLire;				// [115,126] "000000000000"
	// [127,180] (nunused)
	
	public Manifestazione(byte[] buffer) {
		super();
//		this.cartella = toString(buffer, 2, 1);
//		this.codiceSede = toString(buffer, 3, 2);
//		this.codiceProvincia = toString(buffer, 5, 3);
//		this.unitaTerritoriale = toString(buffer, 8, 2);
//		this.codiceVoceIncasso = toString(buffer, 10, 4);
//		this.progressivoCartella = toString(buffer, 14, 2);
//		this.contabilita = toString(buffer, 16, 6);
		this.numeroProgrammaMusicale = toString(buffer, 22, 8);
		this.denominazioneLocale = toString(buffer, 30, 20);
		this.localita = toString(buffer, 50, 20);
		this.dataInizio = toString(buffer, 70, 8);
		this.dataFine = toString(buffer, 78, 8);
		this.tipologiaLocale = toString(buffer, 86, 2);
		this.dalleOre = toString(buffer, 88, 4);
		this.alleOre = toString(buffer, 92, 4);
		this.totaleMinuti = toString(buffer, 96, 3);
		this.provinciaLocale = toString(buffer, 99, 2);
		this.incasso = toString(buffer, 101, 12);
		this.tipoValuta = toString(buffer, 113, 1);
		this.incassoLire = toString(buffer, 114, 12);
	}
	
	public String getNumeroProgrammaMusicale() {
		return numeroProgrammaMusicale;
	}
	public void setNumeroProgrammaMusicale(String numeroProgrammaMusicale) {
		this.numeroProgrammaMusicale = numeroProgrammaMusicale;
	}
	public String getDenominazioneLocale() {
		return denominazioneLocale;
	}
	public void setDenominazioneLocale(String denominazioneLocale) {
		this.denominazioneLocale = denominazioneLocale;
	}
	public String getLocalita() {
		return localita;
	}
	public void setLocalita(String localita) {
		this.localita = localita;
	}
	public String getDataInizio() {
		return dataInizio;
	}
	public void setDataInizio(String dataInizio) {
		this.dataInizio = dataInizio;
	}
	public String getDataFine() {
		return dataFine;
	}
	public void setDataFine(String dataFine) {
		this.dataFine = dataFine;
	}
	public String getTipologiaLocale() {
		return tipologiaLocale;
	}
	public void setTipologiaLocale(String tipologiaLocale) {
		this.tipologiaLocale = tipologiaLocale;
	}
	public String getDalleOre() {
		return dalleOre;
	}
	public void setDalleOre(String dalleOre) {
		this.dalleOre = dalleOre;
	}
	public String getAlleOre() {
		return alleOre;
	}
	public void setAlleOre(String alleOre) {
		this.alleOre = alleOre;
	}
	public String getTotaleMinuti() {
		return totaleMinuti;
	}
	public void setTotaleMinuti(String totaleMinuti) {
		this.totaleMinuti = totaleMinuti;
	}
	public String getProvinciaLocale() {
		return provinciaLocale;
	}
	public void setProvinciaLocale(String provinciaLocale) {
		this.provinciaLocale = provinciaLocale;
	}
	public String getIncasso() {
		return incasso;
	}
	public void setIncasso(String incasso) {
		this.incasso = incasso;
	}
	public String getTipoValuta() {
		return tipoValuta;
	}
	public void setTipoValuta(String tipoValuta) {
		this.tipoValuta = tipoValuta;
	}
	public String getIncassoLire() {
		return incassoLire;
	}
	public void setIncassoLire(String incassoLire) {
		this.incassoLire = incassoLire;
	}

}
