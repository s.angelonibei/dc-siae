package com.alkemytech.sophia.pm.load;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.name.Named;

public class ResumeDb {

	private static final Logger logger = LoggerFactory.getLogger(ResumeDb.class);

	private static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// self
			bind(ResumeDb.class).asEagerSingleton();
		}
		
	}
		
	public static void main(String[] args) {
		try {
			Guice.createInjector(new GuiceModuleExtension(args, "/configuration.properties"))
				.getInstance(ResumeDb.class).debug();
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}
	
	private final Properties configuration;

	@Inject
	protected ResumeDb(@Named("configuration") Properties configuration) {
		super();
		this.configuration = configuration;
	}
	
	private boolean isEnabled() {
		return "true".equalsIgnoreCase(configuration.getProperty("pmload.resume_db.enabled"));
	}
	
	public Map<String, Long> load() {
		if (isEnabled()) {
			try (FileInputStream in = new FileInputStream(new File(configuration
						.getProperty("pmload.resume_db.file", "resume-db.dat")));
					ObjectInputStream oin = new ObjectInputStream(in)) {
				final Object ref = oin.readObject();
				@SuppressWarnings("unchecked")
				final Map<String, Long> resumeDb = (Map<String, Long>) ref;
				return resumeDb;
			} catch (Exception e) {
				logger.error("load", e);
			}
		}
		return new HashMap<String, Long>();
	}
	
	public void save(Map<String, Long> resumeDb) {
		if (isEnabled()) {
			try (FileOutputStream out = new FileOutputStream(new File(configuration
						.getProperty("pmload.resume_db.file", "resume-db.dat")));
					ObjectOutputStream oout = new ObjectOutputStream(out)) {
				oout.writeObject(resumeDb);
			} catch (Exception e) {
				logger.error("save", e);
			}
		}
	}
	
	public String keyFor(String year, String month, String entity) {
		return new StringBuilder()
			.append(year)
			.append(month)
			.append(entity)
			.toString();
	}
	
	private void debug() throws Exception {
		final Map<String, Long> resumeMap = load();
		
		resumeMap.put(keyFor("2017", "01", "PERF_PROGRAMMA_MUSICALE"), 113242L);
		resumeMap.put(keyFor("2017", "01", "PERF_MANIFESTAZIONE"), 102901L);
		resumeMap.put(keyFor("2017", "01", "PERF_UTILIZZAZIONE"), 210501L);
		resumeMap.put(keyFor("2017", "01", "PERF_MOVIMENTO_CONTABILE"), 161252L);
		resumeMap.put(keyFor("2017", "01", "PERF_DIRETTORE_ESECUZIONE"), 5745L);
		resumeMap.put(keyFor("2017", "01", "PERF_EVENTI_PAGATI"), 107895L);

		resumeMap.put(keyFor("2017", "02", "PERF_PROGRAMMA_MUSICALE"), 90291L);
		resumeMap.put(keyFor("2017", "02", "PERF_MANIFESTAZIONE"), 82331L);
		resumeMap.put(keyFor("2017", "02", "PERF_UTILIZZAZIONE"), 241727L);
		resumeMap.put(keyFor("2017", "02", "PERF_MOVIMENTO_CONTABILE"), 123471L);
		resumeMap.put(keyFor("2017", "02", "PERF_DIRETTORE_ESECUZIONE"), 6660L);
		resumeMap.put(keyFor("2017", "02", "PERF_EVENTI_PAGATI"), 102780L);

		resumeMap.put(keyFor("2017", "03", "PERF_PROGRAMMA_MUSICALE"), 102817L);
		resumeMap.put(keyFor("2017", "03", "PERF_MANIFESTAZIONE"), 96213L);
		resumeMap.put(keyFor("2017", "03", "PERF_UTILIZZAZIONE"), 307601L);
		resumeMap.put(keyFor("2017", "03", "PERF_MOVIMENTO_CONTABILE"), 139669L);
		resumeMap.put(keyFor("2017", "03", "PERF_DIRETTORE_ESECUZIONE"), 8570L);
		resumeMap.put(keyFor("2017", "03", "PERF_EVENTI_PAGATI"), 121950L);

		resumeMap.put(keyFor("2017", "04", "PERF_PROGRAMMA_MUSICALE"), 84488L);
		resumeMap.put(keyFor("2017", "04", "PERF_MANIFESTAZIONE"), 79000L);
		resumeMap.put(keyFor("2017", "04", "PERF_UTILIZZAZIONE"), 360600L);
		resumeMap.put(keyFor("2017", "04", "PERF_MOVIMENTO_CONTABILE"), 111337L);
		resumeMap.put(keyFor("2017", "04", "PERF_DIRETTORE_ESECUZIONE"), 9555L);
		resumeMap.put(keyFor("2017", "04", "PERF_EVENTI_PAGATI"), 102399L);

		resumeMap.put(keyFor("2017", "05", "PERF_PROGRAMMA_MUSICALE"), 108914L);
		resumeMap.put(keyFor("2017", "05", "PERF_MANIFESTAZIONE"), 102781L);
		resumeMap.put(keyFor("2017", "05", "PERF_UTILIZZAZIONE"), 516167L);
		resumeMap.put(keyFor("2017", "05", "PERF_MOVIMENTO_CONTABILE"), 150144L);
		resumeMap.put(keyFor("2017", "05", "PERF_DIRETTORE_ESECUZIONE"), 13529L);
		resumeMap.put(keyFor("2017", "05", "PERF_EVENTI_PAGATI"), 139581L);

		resumeMap.put(keyFor("2017", "06", "PERF_PROGRAMMA_MUSICALE"), 114121L);
		resumeMap.put(keyFor("2017", "06", "PERF_MANIFESTAZIONE"), 109217L);
		resumeMap.put(keyFor("2017", "06", "PERF_UTILIZZAZIONE"), 592099L);
		resumeMap.put(keyFor("2017", "06", "PERF_MOVIMENTO_CONTABILE"), 171073L);
		resumeMap.put(keyFor("2017", "06", "PERF_DIRETTORE_ESECUZIONE"), 16441L);
		resumeMap.put(keyFor("2017", "06", "PERF_EVENTI_PAGATI"), 169355L);

		save(resumeMap);
	}
	
}
