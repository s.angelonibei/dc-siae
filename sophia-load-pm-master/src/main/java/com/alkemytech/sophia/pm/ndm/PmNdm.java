package com.alkemytech.sophia.pm.ndm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.ServerSocket;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;

import javax.sql.DataSource;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.sqs.SqsMessageHelper;
import com.alkemytech.sophia.commons.sqs.SqsS3EventPump;
import com.alkemytech.sophia.commons.sqs.SqsS3Events;
import com.alkemytech.sophia.pm.McmdbDataSource;
import com.amazonaws.services.s3.model.S3Object;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Scopes;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class PmNdm {

	private static final Logger logger = LoggerFactory.getLogger(PmNdm.class);

	private static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// amazon service(s)
			bind(S3.class)
				.in(Scopes.SINGLETON);
			bind(SQS.class)
				.in(Scopes.SINGLETON);
			// data source(s)
			bind(DataSource.class)
				.annotatedWith(Names.named("MCMDB"))
				.to(McmdbDataSource.class)
				.in(Scopes.SINGLETON);
			// self
			bind(PmNdm.class).asEagerSingleton();
		}

	}

	public static void main(String[] args) {
		try {
			final Injector injector = Guice
					.createInjector(new GuiceModuleExtension(args, "/sophia-ndm-pm.properties"));
			final PmNdm instance = injector
					.getInstance(PmNdm.class)
					.startup();
			try {
				instance.process();
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}

	private final Properties configuration;
	private final SQS sqs;
	private final S3 s3;
	private final DataSource mcmdbDS;

	@Inject
	protected PmNdm(@Named("configuration") Properties configuration,
			S3 s3, SQS sqs,
			@Named("MCMDB") DataSource mcmdbDS) {
		super();
		this.configuration = configuration;
		this.sqs = sqs;
		this.s3 = s3;
		this.mcmdbDS = mcmdbDS;
	}
	
	public PmNdm startup() {
		s3.startup();
		sqs.startup();
		return this;
	}
	
	public PmNdm shutdown() {
		s3.shutdown();
		sqs.shutdown();
		return this;
	}
	
	public void process() throws Exception {

		final int listenPort = Integer.parseInt(configuration.getProperty("pmndm.bind_port",
				configuration.getProperty("default.bind_port", "0")));
		
		try (final ServerSocket socket = new ServerSocket(listenPort)) {
			logger.info("execute: listening on {}", socket.getLocalSocketAddress());
			
			final SqsS3Events s3Events = new SqsS3Events(SqsS3Events.OBJECT_CRATED);
			final SqsS3EventPump sqsS3EventPump = new SqsS3EventPump(sqs, s3Events, configuration, "pmndm");
			sqsS3EventPump.pollingLoop(new SqsS3EventPump.Consumer() {
				
				@Override
				public void consume(String bucket, String key) {
					try {
						process(bucket, key);
						if (s3.copy(new S3.Url(bucket, key), new S3.Url(bucket, key +
								configuration.getProperty("pmndm.processed_suffix", ".processed")))) {
							s3.delete(new S3.Url(bucket, key));
						}
					} catch (Exception e) {
						logger.error("consume", e);
					}
				}
				
			});	
		}
		
	}
	
	private BigDecimal parseBigDecimal(String value) {
		if (Strings.isNullOrEmpty(value))
			return BigDecimal.ZERO;
		if (Strings.isNullOrEmpty(value.trim()))
			return BigDecimal.ZERO;		
		try {
			return new BigDecimal(value.replace(",", "."));
		} catch (NumberFormatException e) {
			logger.error("parseBigDecimal: trying to parse \"{}\"", value);
			throw e;
		}
	}
	
	private void handleVoceFattura(NdmDao dao, long idNdmFile, List<VoceFattura> vociFatturaScomposte, BigDecimal sommaAggio) throws SQLException {
	
		BigDecimal sommaPercentualeAggio = BigDecimal.ZERO;
		for (VoceFattura voceScomposta : vociFatturaScomposte) {
			if (!Strings.isNullOrEmpty(voceScomposta.tipoPenale) &&
					!BigDecimal.ZERO.equals(voceScomposta.importoAggio)) {
				final BigDecimal percentuale = voceScomposta.percRipartita.divide(new BigDecimal(100));
				sommaPercentualeAggio = sommaPercentualeAggio.add(percentuale.multiply(voceScomposta.percAggio));
			}			
		}
		
		final VoceFattura voceFattura = vociFatturaScomposte.get(0);
		
		dao.insertVoceFattura(idNdmFile,
				voceFattura.annoFattura,
				voceFattura.meseFattura,
				voceFattura.dataFattura,
				voceFattura.annoContabileFattura,
				voceFattura.meseContabileFattura,
				voceFattura.dataContabileFattura,
				voceFattura.idIncasso,
				voceFattura.idQuietanza,
				voceFattura.numeroReversale,
				voceFattura.numeroFattura,
				voceFattura.voce,
				voceFattura.importoOriginale,
				sommaAggio,
				sommaPercentualeAggio);

		final long idNdmVoceFattura = dao.getLastGeneratedKey();
		
		for (VoceFattura voceScomposta : vociFatturaScomposte) {
			if (!Strings.isNullOrEmpty(voceScomposta.tipoPenale) &&
					!BigDecimal.ZERO.equals(voceScomposta.importoAggio)) {
				dao.insertVoceScomposta(idNdmFile, idNdmVoceFattura,
						voceScomposta.sequenza,
						voceScomposta.tipoPenale,
						voceScomposta.importoOriginale,
						voceScomposta.percRipartita,
						voceScomposta.importoRipartito,
						voceScomposta.percAggio,
						voceScomposta.importoAggio);
			}			
		}
		
	}

	private void process(String s3Bucket, String s3Key) throws Exception {
		logger.info("process: s3uri \"s3://{}/{}\"", s3Bucket, s3Key);

		final String filename = s3Key.substring(1 + s3Key.lastIndexOf('/'));
		logger.info("process: filename \"{}\"", filename);

		// config
		final boolean skipProcessedFiles = "true".equalsIgnoreCase(configuration
				.getProperty("pmndm.skip_processed_files"));			
		final char delimiter = configuration
				.getProperty("pmndm.csv.separator.field", ",").charAt(0);
		final char recordSeparator = configuration
				.getProperty("pmndm.csv.separator.line", "\n").charAt(0);
		final SimpleDateFormat dateInputFormat = new SimpleDateFormat(configuration
				.getProperty("pmndm.date.input_format", "dd/MM/yyyy HH:mm:ss"));		
		final SimpleDateFormat dateOutputFormat = new SimpleDateFormat(configuration
				.getProperty("pmndm.date.output_format", "ddMMyyyy"));

		// column indexes
		final int idxAnnoFattura = Integer
				.parseInt(configuration.getProperty("pmndm.csv.column.ANNO_FATTURA"));
		final int idxMeseFattura = Integer
				.parseInt(configuration.getProperty("pmndm.csv.column.MESE_FATTURA"));
		final int idxDataFattura = Integer
				.parseInt(configuration.getProperty("pmndm.csv.column.DATA_FATTURA"));
		final int idxAnnoContabileFattura = Integer
				.parseInt(configuration.getProperty("pmndm.csv.column.ANNO_CONTABILE_FATTURA"));
		final int idxMeseContabileFattura = Integer
				.parseInt(configuration.getProperty("pmndm.csv.column.MESE_CONTABILE_FATTURA"));
		final int idxDataContabileFattura = Integer
				.parseInt(configuration.getProperty("pmndm.csv.column.DATA_CONTABILE_FATTURA"));
		final int idxIdIncasso = Integer
				.parseInt(configuration.getProperty("pmndm.csv.column.ID_INCASSO"));
		final int idxIdQuietanza = Integer
				.parseInt(configuration.getProperty("pmndm.csv.column.ID_QUIETANZA"));
		final int idxNumeroReversale = Integer
				.parseInt(configuration.getProperty("pmndm.csv.column.NUMERO_REVERSALE"));
		final int idxNumeroFattura = Integer
				.parseInt(configuration.getProperty("pmndm.csv.column.NUMERO_FATTURA"));
		final int idxVoce = Integer
				.parseInt(configuration.getProperty("pmndm.csv.column.VOCE"));
		final int idxSequenza = Integer
				.parseInt(configuration.getProperty("pmndm.csv.column.SEQUENZA"));
		final int idxTipoPenale = Integer
				.parseInt(configuration.getProperty("pmndm.csv.column.TIPO_PENALE"));
		final int idxImportoOriginale = Integer
				.parseInt(configuration.getProperty("pmndm.csv.column.IMPORTO_ORIGINALE"));
		final int idxPercRipartita = Integer
				.parseInt(configuration.getProperty("pmndm.csv.column.PERC_RIPARTITA"));
		final int idxImportoRipartito = Integer
				.parseInt(configuration.getProperty("pmndm.csv.column.IMPORTO_RIPARTITO"));
		final int idxPercAggio = Integer
				.parseInt(configuration.getProperty("pmndm.csv.column.PERC_AGGIO"));
		final int idxImportoAggio = Integer
				.parseInt(configuration.getProperty("pmndm.csv.column.IMPORTO_AGGIO"));

		final boolean debug = "true".equalsIgnoreCase(configuration.getProperty("pmndm.debug",
				configuration.getProperty("debug")));
		
		final Gson gson = new GsonBuilder()
//			.setPrettyPrinting()
			.create();
		final long startTimeMillis = System.currentTimeMillis();

		// stats
		long numeroRecord = 0L;			
		long numeroVociFattura = 0L;
		final Set<String> uniqueContabilita = new HashSet<>();

		// connect to database
		try (final Connection connection = mcmdbDS.getConnection()) {
			logger.info("process: connected to {} {}", connection.getMetaData()
					.getDatabaseProductName(), connection.getMetaData().getURL());
			
			// enable auto-commit
			connection.setAutoCommit(true);
			
			// download file
			final S3Object s3Object = s3.getObject(new S3.Url(s3Bucket, s3Key));
			if (null == s3Object) {
				logger.warn("process: unable to download s3uri \"s3://{}/{}\"", s3Bucket, s3Key);
				return;
			}
			final File tempFile = new File(UUID.randomUUID().toString());
			try (final InputStream in = s3Object.getObjectContent();
					final OutputStream out = new FileOutputStream(tempFile)) {
				final byte[] buffer = new byte[1024];
				for (int count = 0; -1 != (count = in.read(buffer)); ) {
					if (count > 0)
						out.write(buffer, 0, count);
				}
			}
			tempFile.deleteOnExit();

			// select PERF_NDM_FILE.ID_NDM_FILE
			final NdmDao dao = new NdmDao(connection, true);
			long idNdmFile = dao.getIdNdmFile(filename);
			logger.debug("process: idNdmFile {}", idNdmFile);
			if (idNdmFile < 0L) {
				// insert PERF_NDM_FILE
				if (!dao.insertNdmFile(filename, String.format("s3://%s/%s", s3Bucket, s3Key))) {
					logger.error("process: errore insert PERF_NDM_FILE");
					return;
				}
				idNdmFile = dao.getLastGeneratedKey();
				logger.debug("process: idNdmFile {}", idNdmFile);
			} else if (skipProcessedFiles) {
				logger.info("process: file già presente in PERF_NDM_FILE");
				return;
			} else {
				// delete previously processed row(s) and update PERF_NDM_FILE
				try {
					connection.setAutoCommit(false);
					dao.deleteFileRecords(idNdmFile);
					if (!dao.reprocessNdmFile(idNdmFile)) {
						logger.error("process: errore update PERF_NDM_FILE");
						connection.rollback();
						return;
					}
					connection.commit();
				} catch (SQLException e) {
					logger.error("process", e);
					connection.rollback();
				} finally {
					connection.setAutoCommit(true);
				}
			}

			// parse file .csv
			try (final InputStream in = new FileInputStream(tempFile);
				final InputStreamReader reader = new InputStreamReader(in, StandardCharsets.UTF_8);
				final CSVParser parser = new CSVParser(reader, CSVFormat.DEFAULT.withIgnoreSurroundingSpaces()
						.withDelimiter(delimiter).withRecordSeparator(recordSeparator))) {

				// skip row(s)
				final Iterator<CSVRecord> records = parser.iterator();
				for (int skipRows = Integer.parseInt(configuration
						.getProperty("pmndm.csv.skip_rows", "0")); skipRows > 0; skipRows --) {
					if (records.hasNext()) {
						final CSVRecord record = records.next();
						if (debug)
							logger.debug("skipping record {}", record);
					}
				}

				// parse row(s)
				final List<VoceFattura> voceFatturaScomposta = new ArrayList<>();
				BigDecimal sommaAggio = BigDecimal.ZERO;
				while (records.hasNext()) {
					final CSVRecord record = records.next();
					numeroRecord ++;
					if (debug)
						logger.debug("parsing record {} {}", numeroRecord, record);
					
					final VoceFattura voceFattura = new VoceFattura();
					voceFattura.annoFattura = record.get(idxAnnoFattura);
					voceFattura.meseFattura = record.get(idxMeseFattura);
					voceFattura.dataFattura = dateOutputFormat
							.format(dateInputFormat.parse(record.get(idxDataFattura)));
					voceFattura.annoContabileFattura = record.get(idxAnnoContabileFattura);
					voceFattura.meseContabileFattura = record.get(idxMeseContabileFattura);
					voceFattura.dataContabileFattura = dateOutputFormat
							.format(dateInputFormat.parse(record.get(idxDataContabileFattura)));
					voceFattura.idIncasso = record.get(idxIdIncasso);
					voceFattura.idQuietanza = record.get(idxIdQuietanza);
					voceFattura.numeroReversale = record.get(idxNumeroReversale);
					voceFattura.numeroFattura = record.get(idxNumeroFattura);
					voceFattura.voce = record.get(idxVoce);
					voceFattura.sequenza = record.get(idxSequenza);
					voceFattura.tipoPenale = record.get(idxTipoPenale);
					voceFattura.importoOriginale = parseBigDecimal(record.get(idxImportoOriginale));
					voceFattura.percRipartita = parseBigDecimal(record.get(idxPercRipartita));
					voceFattura.importoRipartito = parseBigDecimal(record.get(idxImportoRipartito));
					voceFattura.percAggio = parseBigDecimal(record.get(idxPercAggio));
					voceFattura.importoAggio = parseBigDecimal(record.get(idxImportoAggio));
					
					uniqueContabilita.add(String.format("%04d%02d",
							Integer.parseInt(voceFattura.annoFattura),
							Integer.parseInt(voceFattura.meseFattura)));

					if (!Strings.isNullOrEmpty(voceFattura.tipoPenale))
						voceFattura.tipoPenale = voceFattura.tipoPenale.trim();
					
					if ("0".equals(voceFattura.sequenza)) { // nuova voce fattura
						numeroVociFattura ++;							
						if (!voceFatturaScomposta.isEmpty()) {
							handleVoceFattura(dao, idNdmFile, voceFatturaScomposta, sommaAggio);
						}
						sommaAggio = BigDecimal.ZERO;
						voceFatturaScomposta.clear();
					}
					sommaAggio = sommaAggio.add(voceFattura.importoAggio);
					voceFatturaScomposta.add(voceFattura);
				}
				if (!voceFatturaScomposta.isEmpty()) {
					handleVoceFattura(dao, idNdmFile, voceFatturaScomposta, sommaAggio);
				}
				
			}
		
			// stats
			logger.debug("process: numeroRecord {}", numeroRecord);
			logger.debug("process: numeroVociFattura {}", numeroVociFattura);
			logger.debug("process: uniqueContabilita {}", uniqueContabilita);

			final JsonObject statistiche = new JsonObject();
			statistiche.addProperty("numeroRecord", numeroRecord);
			statistiche.addProperty("numeroVociFattura", numeroVociFattura);

			// update PERF_NDM_FILE
			if (!dao.updateNdmFile(idNdmFile, numeroRecord, gson.toJson(statistiche))) {
				logger.error("process: errore update PERF_NDM_FILE");
			}
		}
		
		// send message(s) to aggi queue
		try {
			final String aggiToProcesssQueue = configuration
					.getProperty("pmndm.aggi.to_process_queue");
			final String aggiServiceBusQueueUrl = sqs.getOrCreateUrl(configuration
					.getProperty("pmndm.aggi.service_bus_queue"));
			for (String contabilita : uniqueContabilita) {			
				final JsonObject body = new JsonObject();
				body.addProperty("year", contabilita.substring(0, 4));
				body.addProperty("month", contabilita.substring(4));
				sqs.sendMessage(aggiServiceBusQueueUrl, gson.toJson(SqsMessageHelper
						.formatToProcessJson(aggiToProcesssQueue, "pm-ndm", body)));
			}
		} catch (Exception e) {
			logger.error("process", e);
		}
		
		// timing
		final long elapsedMillis = System.currentTimeMillis() - startTimeMillis;
		final long elapsedSeconds = elapsedMillis / 1000L;
		logger.info("process: completed in {}h {}m {}s {}ms",
				elapsedSeconds / 3600,
				(elapsedSeconds % 3600) / 60,
				(elapsedSeconds % 3600) % 60,
				elapsedMillis % 1000);
	}
	
}
