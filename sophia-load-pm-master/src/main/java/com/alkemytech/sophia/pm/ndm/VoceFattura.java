package com.alkemytech.sophia.pm.ndm;

import java.math.BigDecimal;

public class VoceFattura {

	public String annoFattura;
	public String meseFattura;
	public String dataFattura;
	public String annoContabileFattura;
	public String meseContabileFattura;
	public String dataContabileFattura;
	public String idIncasso;
	public String idQuietanza;
	public String numeroReversale;
	public String numeroFattura;
	public String voce;
	public String sequenza;
	public String tipoPenale;
	public BigDecimal importoOriginale;
	public BigDecimal percRipartita;
	public BigDecimal importoRipartito;
	public BigDecimal percAggio;
	public BigDecimal importoAggio;
		
}
