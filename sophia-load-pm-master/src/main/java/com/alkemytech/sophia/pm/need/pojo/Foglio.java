package com.alkemytech.sophia.pm.need.pojo;

import java.util.List;

/**
 * @author roberto cerfogli <roberto@cerfogli.eu>
 */
public class Foglio {

	private Esecutore esecutore;
	private QuadroContabile quadroContabile;
	private List<Utilizzazione> utilizzazioni;
	private Manifestazione manifestazione;
	private Direttore1 direttore1;
	private Direttore2 direttore2;
	private Associati associati;
	
	public Esecutore getEsecutore() {
		return esecutore;
	}
	public void setEsecutore(Esecutore esecutore) {
		this.esecutore = esecutore;
	}
	public QuadroContabile getQuadroContabile() {
		return quadroContabile;
	}
	public void setQuadroContabile(QuadroContabile quadroContabile) {
		this.quadroContabile = quadroContabile;
	}
	public List<Utilizzazione> getUtilizzazioni() {
		return utilizzazioni;
	}
	public void setUtilizzazioni(List<Utilizzazione> utilizzazioni) {
		this.utilizzazioni = utilizzazioni;
	}
	public Manifestazione getManifestazione() {
		return manifestazione;
	}
	public void setManifestazione(Manifestazione manifestazione) {
		this.manifestazione = manifestazione;
	}
	public Direttore1 getDirettore1() {
		return direttore1;
	}
	public void setDirettore1(Direttore1 direttore1) {
		this.direttore1 = direttore1;
	}
	public Direttore2 getDirettore2() {
		return direttore2;
	}
	public void setDirettore2(Direttore2 direttore2) {
		this.direttore2 = direttore2;
	}
	public Associati getAssociati() {
		return associati;
	}
	public void setAssociati(Associati associati) {
		this.associati = associati;
	}

}
