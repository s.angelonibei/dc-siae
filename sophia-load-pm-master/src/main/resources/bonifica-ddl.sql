
-- ------------------------ --
-- PERF_BONIFICA_AUTOMATICA --
-- ------------------------ --

DROP TABLE IF EXISTS PERF_BONIFICA_AUTOMATICA;

CREATE TABLE PERF_BONIFICA_AUTOMATICA
( ID_PERF_BONIFICA_AUTOMATICA		BIGINT			NOT NULL AUTO_INCREMENT
, NOME_TABELLA						VARCHAR(100)	NOT NULL
, NOME_COLONNA						VARCHAR(100)	NOT NULL
, FLAG_ATTIVA						BIT				DEFAULT TRUE
, PRIMARY KEY (ID_PERF_BONIFICA_AUTOMATICA)
);

CREATE UNIQUE INDEX PERF_BONIFICA_AUTOMATICA_IDX_01
   ON PERF_BONIFICA_AUTOMATICA (NOME_TABELLA, NOME_COLONNA);


insert into PERF_BONIFICA_AUTOMATICA (NOME_TABELLA, NOME_COLONNA) values ('PERF_PROGRAMMA_MUSICALE', 'CAUSA_ANNULLAMENTO');
insert into PERF_BONIFICA_AUTOMATICA (NOME_TABELLA, NOME_COLONNA) values ('PERF_PROGRAMMA_MUSICALE', 'STATO_WEB');
insert into PERF_BONIFICA_AUTOMATICA (NOME_TABELLA, NOME_COLONNA) values ('PERF_PROGRAMMA_MUSICALE', 'FLAG_GRUPPO_PRINCIPALE');

insert into PERF_BONIFICA_AUTOMATICA (NOME_TABELLA, NOME_COLONNA) values ('PERF_MANIFESTAZIONE', 'CODICE_LOCALE');
insert into PERF_BONIFICA_AUTOMATICA (NOME_TABELLA, NOME_COLONNA) values ('PERF_MANIFESTAZIONE', 'DENOMINAZIONE_LOCALE');
insert into PERF_BONIFICA_AUTOMATICA (NOME_TABELLA, NOME_COLONNA) values ('PERF_MANIFESTAZIONE', 'PARTITA_IVA_ORGANIZZATORE');

insert into PERF_BONIFICA_AUTOMATICA (NOME_TABELLA, NOME_COLONNA) values ('PERF_MOVIMENTO_CONTABILE', 'NUMERO_PM_PREVISTI');
insert into PERF_BONIFICA_AUTOMATICA (NOME_TABELLA, NOME_COLONNA) values ('PERF_MOVIMENTO_CONTABILE', 'SEDE');
insert into PERF_BONIFICA_AUTOMATICA (NOME_TABELLA, NOME_COLONNA) values ('PERF_MOVIMENTO_CONTABILE', 'AGENZIA');

insert into PERF_BONIFICA_AUTOMATICA (NOME_TABELLA, NOME_COLONNA) values ('PERF_DIRETTORE_ESECUZIONE', 'INDIRIZZO');
insert into PERF_BONIFICA_AUTOMATICA (NOME_TABELLA, NOME_COLONNA) values ('PERF_DIRETTORE_ESECUZIONE', 'COMUNE');
insert into PERF_BONIFICA_AUTOMATICA (NOME_TABELLA, NOME_COLONNA) values ('PERF_DIRETTORE_ESECUZIONE', 'CAP');

commit;


-- -------------------------------- --
-- PERF_PROGRAMMA_MUSICALE_BONIFICA --
-- -------------------------------- --

DROP TABLE IF EXISTS PERF_PROGRAMMA_MUSICALE_BONIFICA;

CREATE TABLE PERF_PROGRAMMA_MUSICALE_BONIFICA
(
   ID_PROGRAMMA_MUSICALE           BIGINT                                               NOT NULL,
   NUMERO_PROGRAMMA_MUSICALE       BIGINT                                               NOT NULL,
   TIPO_PM                         VARCHAR(20) CHARSET utf8 COLLATE utf8_general_ci,
   SUPPORTO_PM                     VARCHAR(4000) CHARSET utf8 COLLATE utf8_general_ci,
   FOGLIO                          VARCHAR(2) CHARSET utf8 COLLATE utf8_general_ci,
   DATA_ASSEGNAZIONE               DATETIME,
   DATA_RESTITUZIONE               DATETIME,
   DATA_COMPILAZIONE               DATETIME,
   STATO_PM                        VARCHAR(10) CHARSET utf8 COLLATE utf8_general_ci,
   DATA_ANNULLAMENTO               DATETIME,
   FOGLIO_SEGUE_DI                 VARCHAR(10) CHARSET utf8 COLLATE utf8_general_ci,
   PM_RIFERIMENTO                  VARCHAR(20) CHARSET utf8 COLLATE utf8_general_ci,
   CAUSA_ANNULLAMENTO              VARCHAR(4000) CHARSET utf8 COLLATE utf8_general_ci,
   STATO_WEB                       VARCHAR(4000) CHARSET utf8 COLLATE utf8_general_ci,
   TOTALE_CEDOLE                   BIGINT,
   TOTALE_DURATA_CEDOLE            BIGINT,
   GIORNATE_TRATTENIMENTO          BIGINT,
   FLAG_GRUPPO_PRINCIPALE          VARCHAR(1) CHARSET utf8 COLLATE utf8_general_ci,
   DATA_BONIFICA                   DATETIME    NOT NULL,
   RECORD_ELABORATO                INT         DEFAULT 0 comment '0=non elaborato, 1=inserimento automatico, 2=modifica automatica, 3=modifica manuale'
)
COLLATE=utf8_general_ci;

CREATE UNIQUE INDEX PERF_PROGRAMMA_MUSICALE_BONIFICA_IDX_01
   ON PERF_PROGRAMMA_MUSICALE_BONIFICA (ID_PROGRAMMA_MUSICALE, DATA_BONIFICA);



-- ---------------------------- --
-- PERF_MANIFESTAZIONE_BONIFICA --
-- ---------------------------- --

DROP TABLE IF EXISTS PERF_MANIFESTAZIONE_BONIFICA;

CREATE TABLE PERF_MANIFESTAZIONE_BONIFICA
(
   ID_EVENTO                       BIGINT                                              NOT NULL,
   DATA_INIZIO_EVENTO              DATETIME,
   DATA_FINE_EVENTO                DATETIME,
   ORA_INIZIO                      VARCHAR(10) CHARSET utf8 COLLATE utf8_general_ci,
   ORA_FINE                        VARCHAR(10) CHARSET utf8 COLLATE utf8_general_ci,
   CODICE_LOCALE                   VARCHAR(13) CHARSET utf8 COLLATE utf8_general_ci,
   DENOMINAZIONE_LOCALE            VARCHAR(110) CHARSET utf8 COLLATE utf8_general_ci,
   CODICE_COMUNE                   VARCHAR(6) CHARSET utf8 COLLATE utf8_general_ci,
   COMUNE_LOCALE                   VARCHAR(100) CHARSET utf8 COLLATE utf8_general_ci,
   PROVINCIA_LOCALE                VARCHAR(100) CHARSET utf8 COLLATE utf8_general_ci,
   REGIONE_LOCALE                  VARCHAR(100) CHARSET utf8 COLLATE utf8_general_ci,
   CODICE_BA_LOCALE                VARCHAR(100) CHARSET utf8 COLLATE utf8_general_ci,
   CAP_LOCALE                      VARCHAR(5) CHARSET utf8 COLLATE utf8_general_ci,
   SIGLA_LOCALE                    VARCHAR(2) CHARSET utf8 COLLATE utf8_general_ci,
   SPAZIO_LOCALE                   VARCHAR(100) CHARSET utf8 COLLATE utf8_general_ci,
   GENERE_LOCALE                   VARCHAR(100) CHARSET utf8 COLLATE utf8_general_ci,
   DENOMINAZIONE_LOCALITA          VARCHAR(100) CHARSET utf8 COLLATE utf8_general_ci,
   CODICE_SAP_ORGANIZZATORE        VARCHAR(15) CHARSET utf8 COLLATE utf8_general_ci,
   PARTITA_IVA_ORGANIZZATORE       VARCHAR(20) CHARSET utf8 COLLATE utf8_general_ci,
   CODICE_FISCALE_ORGANIZZATORE    VARCHAR(20) CHARSET utf8 COLLATE utf8_general_ci,
   NOME_ORGANIZZATORE              VARCHAR(210) CHARSET utf8 COLLATE utf8_general_ci,
   COMUNE_ORGANIZZATORE            VARCHAR(110) CHARSET utf8 COLLATE utf8_general_ci,
   PROVINCIA_ORGANIZZATORE         VARCHAR(110) CHARSET utf8 COLLATE utf8_general_ci,
   TITOLO                          VARCHAR(140) CHARSET utf8 COLLATE utf8_general_ci,
   TITOLO_ORIGINALE                VARCHAR(140) CHARSET utf8 COLLATE utf8_general_ci,
   DATA_BONIFICA                   DATETIME    NOT NULL,
   RECORD_ELABORATO                INT         DEFAULT 0 comment '0=non elaborato, 1=inserimento automatico, 2=modifica automatica, 3=modifica manuale'
)
COLLATE=utf8_general_ci;

CREATE UNIQUE INDEX PERF_MANIFESTAZIONE_BONIFICA_IDX_01
   ON PERF_MANIFESTAZIONE_BONIFICA (ID_EVENTO, DATA_BONIFICA);



-- --------------------------- --
-- PERF_UTILIZZAZIONE_BONIFICA --
-- --------------------------- --

-- WARNING: non essendoci primary key non è possibile fare la differenza di due record...



-- --------------------------------- --
-- PERF_MOVIMENTO_CONTABILE_BONIFICA --
-- --------------------------------- --

DROP TABLE IF EXISTS PERF_MOVIMENTO_CONTABILE_BONIFICA;

CREATE TABLE PERF_MOVIMENTO_CONTABILE_BONIFICA
(
   ID_PROGRAMMA_MUSICALE       BIGINT,
   NUMERO_PM                   VARCHAR(20) CHARSET utf8 COLLATE utf8_general_ci,
   ID_EVENTO                   BIGINT,
   TIPOLOGIA_MOVIMENTO         VARCHAR(20) CHARSET utf8 COLLATE utf8_general_ci,
   IMPORTO_MANUALE             DECIMAL(15,7),
   CODICE_VOCE_INCASSO         VARCHAR(4) CHARSET utf8 COLLATE utf8_general_ci,
   NUMERO_PERMESSO             VARCHAR(50) CHARSET utf8 COLLATE utf8_general_ci,
   TOTALE_DEM_LORDO_PM         DECIMAL(15,7),
   NUMERO_REVERSALE            BIGINT,
   FLAG_GRUPPO_PRINCIPALE      CHAR(1) CHARSET utf8 COLLATE utf8_general_ci,
   NUMERO_FATTURA              BIGINT,
   IMPORTO_DEM_TOTALE          DECIMAL(15,7),
   NUMERO_PM_PREVISTI          BIGINT,
   NUMERO_PM_PREVISTI_SPALLA   BIGINT,
   DATA_REVERSALE              DATETIME,
   DATA_RIENTRO                DATETIME,
   CONTABILITA                 BIGINT,
   TIPO_DOCUMENTO_CONTABILE    VARCHAR(3) CHARSET utf8 COLLATE utf8_general_ci,
   CODICE_RAGGRUPPAMENTO       VARCHAR(50) CHARSET utf8 COLLATE utf8_general_ci,
   SEDE                        VARCHAR(50) CHARSET utf8 COLLATE utf8_general_ci,
   AGENZIA                     VARCHAR(50) CHARSET utf8 COLLATE utf8_general_ci,
   DATA_BONIFICA               DATETIME    NOT NULL,
   RECORD_ELABORATO            INT         DEFAULT 0 comment '0=non elaborato, 1=inserimento automatico, 2=modifica automatica, 3=modifica manuale'
)
COLLATE=utf8_general_ci;

CREATE UNIQUE INDEX PERF_MOVIMENTO_CONTABILE_BONIFICA_IDX_01
   ON PERF_MOVIMENTO_CONTABILE_BONIFICA (ID_EVENTO, CODICE_VOCE_INCASSO, DATA_BONIFICA);
   


-- ---------------------------------- --
-- PERF_DIRETTORE_ESECUZIONE_BONIFICA --
-- ---------------------------------- --

DROP TABLE IF EXISTS PERF_DIRETTORE_ESECUZIONE_BONIFICA;

CREATE TABLE PERF_DIRETTORE_ESECUZIONE_BONIFICA
(
   ID_PROGRAMMA_MUSICALE        BIGINT,
   NUMERO_PROGRAMMA_MUSICALE    BIGINT,
   DIRETTORE_ESECUZIONE         VARCHAR(400) CHARSET utf8 COLLATE utf8_general_ci,
   SESSO                        VARCHAR(1) CHARSET utf8 COLLATE utf8_general_ci,
   POSIZIONE_SIAE               VARCHAR(20) CHARSET utf8 COLLATE utf8_general_ci,
   COMUNE_NASCITA               VARCHAR(400) CHARSET utf8 COLLATE utf8_general_ci,
   SIGLA_PROVINCIA_NASCITA      VARCHAR(2) CHARSET utf8 COLLATE utf8_general_ci,
   DATA_NASCITA                 DATETIME,
   CODICE_FISCALE               VARCHAR(20) CHARSET utf8 COLLATE utf8_general_ci,
   INDIRIZZO                    VARCHAR(400) CHARSET utf8 COLLATE utf8_general_ci,
   CIVICO_RESIDENZA             VARCHAR(50) CHARSET utf8 COLLATE utf8_general_ci,
   COMUNE                       VARCHAR(400) CHARSET utf8 COLLATE utf8_general_ci,
   SIGLA_PROVINCIA              VARCHAR(2) CHARSET utf8 COLLATE utf8_general_ci,
   CAP                          VARCHAR(10) CHARSET utf8 COLLATE utf8_general_ci,
   DATA_BONIFICA                DATETIME    NOT NULL,
   RECORD_ELABORATO             INT         DEFAULT 0 comment '0=non elaborato, 1=inserimento automatico, 2=modifica automatica, 3=modifica manuale'
)
COLLATE=utf8_general_ci;

CREATE UNIQUE INDEX PERF_DIRETTORE_ESECUZIONE_BONIFICA_IDX_01
   ON PERF_DIRETTORE_ESECUZIONE_BONIFICA (ID_PROGRAMMA_MUSICALE, DATA_BONIFICA);



-- --------------------------- --
-- PERF_EVENTI_PAGATI_BONIFICA --
-- --------------------------- --

DROP TABLE IF EXISTS PERF_EVENTI_PAGATI_BONIFICA;

CREATE TABLE PERF_EVENTI_PAGATI_BONIFICA
(
   ID_EVENTO                 BIGINT,
   VOCE_INCASSO              VARCHAR(5) CHARSET utf8 COLLATE utf8_general_ci,
   SEPRAG                    VARCHAR(10) CHARSET utf8 COLLATE utf8_general_ci,
   NUMERO_REVERSALE          VARCHAR(20) CHARSET utf8 COLLATE utf8_general_ci,
   IMPORTO_DEM               DECIMAL(15,7),
   CONTABILITA               BIGINT,
   TIPO_DOCUMENTO_CONTABILE  VARCHAR(20) CHARSET utf8 COLLATE utf8_general_ci,
   NUMERO_FATTURA            VARCHAR(20) CHARSET utf8 COLLATE utf8_general_ci,
   DENOMINAZIONE_LOCALE      VARCHAR(110) CHARSET utf8 COLLATE utf8_general_ci,
   CODICE_BA_LOCALE          VARCHAR(20) CHARSET utf8 COLLATE utf8_general_ci,
   CODICE_SPEI_LOCALE        VARCHAR(20) CHARSET utf8 COLLATE utf8_general_ci,
   DATA_INIZIO_EVENTO        DATETIME,
   ORA_INIZIO_EVENTO         VARCHAR(5) CHARSET utf8 COLLATE utf8_general_ci,
   NUMERO_PM_ATTESI          BIGINT,
   NUMERO_PM_ATTESI_SPALLA   BIGINT,
   CODICE_RAGGRUPPAMENTO     VARCHAR(50) CHARSET utf8 COLLATE utf8_general_ci,
   SEDE                      VARCHAR(50) CHARSET utf8 COLLATE utf8_general_ci,
   AGENZIA                   VARCHAR(50) CHARSET utf8 COLLATE utf8_general_ci,
   PERMESSO                  VARCHAR(19) CHARSET utf8 COLLATE utf8_general_ci,
   DATA_REVERSALE            DATETIME,
   DATA_BONIFICA             DATETIME    NOT NULL,
   RECORD_ELABORATO          INT         DEFAULT 0 comment '0=non elaborato, 1=inserimento automatico, 2=modifica automatica, 3=modifica manuale'
)
COLLATE=utf8_general_ci;

CREATE UNIQUE INDEX PERF_EVENTI_PAGATI_BONIFICA_IDX_01
   ON PERF_EVENTI_PAGATI_BONIFICA (ID_EVENTO, VOCE_INCASSO, DATA_BONIFICA);



