package com.alkemytech.sophia.pm.load;

import java.util.Properties;

import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Scopes;
import com.google.inject.name.Named;

public class PmAggiSendSQSMessage {

    private static class GuiceModuleExtension extends GuiceModule {

        public GuiceModuleExtension(String[] args, String resourceName) {
            super(args, resourceName);
        }

        @Override
        protected void configure() {
            super.configure();
            bind(SQS.class).in(Scopes.SINGLETON);
        }

    }

    private final SQS sqs;
    private final Properties configuration;

    @Inject
    protected PmAggiSendSQSMessage(@Named("configuration") Properties configuration,
                     @Named("default.bind_port") int lockPort,
                     SQS sqs) {
        super();
        this.configuration = configuration;
        this.sqs = sqs;
    }

	public static void main(String[] args) throws Exception {
		final Injector injector = Guice.createInjector(new PmAggiSendSQSMessage.GuiceModuleExtension(args, "/sophia-aggi-pm.properties"));
		injector.getInstance(PmAggiSendSQSMessage.class).run();		
	}

    private PmAggiSendSQSMessage run() {
        sqs.startup();
        final SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration, "pmaggi.sqs");
        JsonObject jsObj = new JsonObject();
        jsObj.addProperty("year","2018");
        jsObj.addProperty("month","01");
        sqsMessagePump.sendToProcessMessage(jsObj,false);
        return this;
    }
}
