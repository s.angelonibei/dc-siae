#!/bin/sh

SOURCE_DIR=/home/roberto.cerfogli/pm
DEPLOY_DIR=/home/orchestrator/sophia/pm
 
cd $SOURCE_DIR

sudo cp -rf prod/* $DEPLOY_DIR
sudo cp sophia-load-pm-1.1-jar-with-dependencies.jar $DEPLOY_DIR
sudo chown -R orchestrator:orchestrator $DEPLOY_DIR
sudo chgrp -R orchestrator $DEPLOY_DIR

cd $DEPLOY_DIR
sudo su orchestrator


