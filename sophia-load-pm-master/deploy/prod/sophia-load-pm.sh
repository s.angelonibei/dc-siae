#!/bin/sh

ENVNAME=prod
APPNAME=sophia-load-pm
VERSION=1.1

JARNAME=$APPNAME-$VERSION-jar-with-dependencies.jar
CFGNAME=$APPNAME.properties
HOME=/home/orchestrator/sophia/pm
#OUTPUT=/dev/null
OUTPUT=$HOME/logs/$APPNAME.out

echo "$(date +%Y%m%d%H%M%S) $ENVNAME $APPNAME" >> $OUTPUT

nohup java -Ddefault.home_folder=$HOME -cp $HOME/$JARNAME com.alkemytech.sophia.pm.load.PmLoad $HOME/$CFGNAME 2>&1 >> $OUTPUT &

echo "$(date +%Y%m%d%H%M%S) $ENVNAME $APPNAME" >> $HOME/logs/crontab.log

