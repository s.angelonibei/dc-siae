#!/bin/sh

ENVNAME=dev
APPNAME=sophia-preludio-pm
VERSION=1.1

JARNAME=sophia-load-pm-$VERSION-jar-with-dependencies.jar
CFGNAME=$APPNAME.properties
HOME=/var/local/sophia/$ENVNAME/pm
#HOME=/home/orchestrator/sophia/pmload
#OUTPUT=/dev/null
OUTPUT=$HOME/logs/$APPNAME.out

echo "$(date +%Y%m%d%H%M%S) $ENVNAME $APPNAME" >> $OUTPUT

nohup java -Ddefault.home_folder=$HOME -cp $HOME/$JARNAME com.alkemytech.sophia.pm.recording.PmPreludio $HOME/$CFGNAME 2>&1 >> $OUTPUT &

echo "$(date +%Y%m%d%H%M%S) $ENVNAME $APPNAME" >> $HOME/logs/crontab.log
