#!/bin/sh

SOURCE_DIR=/home/roberto.cerfogli/pm
DEPLOY_DIR=/var/local/sophia/test/pm
 
cd $SOURCE_DIR

sudo cp -rf test/* $DEPLOY_DIR
sudo cp sophia-load-pm-1.1-jar-with-dependencies.jar $DEPLOY_DIR
sudo chown -R roberto.cerfogli:sophia $DEPLOY_DIR
sudo chgrp -R sophia $DEPLOY_DIR


