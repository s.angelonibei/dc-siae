SELECT GPM.ID_PROGRAMMA_MUSICALE,
  PM.NUMERO AS NUMERO_PM,
  GPM.ID_EVENTO AS ID_EVENTO,
  VPQ.ID_TIPO_MODELLO AS TIPOLOGIA_MOVIMENTO,
  GPM.IMPORTO_DDA AS IMPORTO_MANUALE,
  PME.COD_VOCE_INCASSO AS CODICE_VOCE_INCASSO,
  PM.PROG_PROTOC_PERMESSO AS NUMERO_PERMESSO,
  VPQ.IMPORTO AS TOTALE_DEM_LORDO_PM,
  VPQ.REVERSALE AS NUMERO_REVERSALE,
  PME.FLAG_GRUPPO_PRINCIPALE AS FLAG_GRUPPO_PRINCIPALE,
  Q.NUM_FATTURA AS NUMERO_FATTURA,
  VPQ.IMPORTO AS IMPORTO_DEM_TOTALE,
  NVL(GPE.ATTESI,0) AS NUMERO_PM_PREVISTI,
  CASE WHEN GPM.COD_VOCE_INCASSO='2244' THEN (NVL(GPE.ATTESI,0) - NVL(GPE.GRUPPOPRINCIPALE,0)) ELSE 0 END AS NUMERO_PM_PREVISTI_SPALLA,
  VPQ.DATA AS DATA_REVERSALE,
  PM.DATA_RESTITUZIONE AS DATA_RIENTRO,
  CASE WHEN Q.DATA>=PM.DATA_RESTITUZIONE THEN TO_CHAR(Q.DATA,'YYYYMM') ELSE TO_CHAR(PM.DATA_RESTITUZIONE,'YYYYMM') END AS CONTABILITA,
  CASE  WHEN VPQ.ID_TIPO_MODELLO = 1 THEN '501'
  WHEN VPQ.ID_TIPO_MODELLO = 17 OR VPQ.ID_TIPO_MODELLO = 7 THEN '221'
  ELSE '501' END AS TIPO_DOCUMENTO_CONTABILE,
  VPQ.COD_RAGGRUPPAMENTO AS CODICE_RAGGRUPPAMENTO,
  SED.DESCRIZIONE AS SEDE,
  CIR.DENOMINAZIONE AS AGENZIA,
  PME.DATA_BONIFICA as DATA_BONIFICA
FROM PROGRAMMA_MUSICALE PM
  JOIN PROGRAMMA_MUSICALE_EVENTO PME ON PME.ID_PROGRAMMA_MUSICALE = PM.ID_PROGRAMMA_MUSICALE
  JOIN GESTIONE_PM GPM ON GPM.ID_PROGRAMMA_MUSICALE=PM.ID_PROGRAMMA_MUSICALE and PME.ID_EVENTO = GPM.ID_EVENTO
  JOIN SPEI_ADMIN.VIEW_PMO_DISTRIBUZIONE VPQ ON VPQ.ID_QUIETANZA = GPM.ID_QUIETANZA AND VPQ.ID_EVENTO = PME.ID_EVENTO AND VPQ.COD_VOCE_INCASSO = PME.COD_VOCE_INCASSO
  JOIN QUIETANZA Q ON Q.ID_QUIETANZA = GPM.ID_QUIETANZA
  LEFT JOIN GPM_EVENTO GPE ON GPE.ID_EVENTO=PME.ID_EVENTO AND PME.COD_VOCE_INCASSO = GPE.COD_VOCE_INCASSO
  LEFT JOIN SEDE SED ON SED.COD_SEDE = SUBSTR(VPQ.SEPRAG,1,2)
  LEFT JOIN CIRCOSCRIZIONE CIR ON CIR.COD_SEPRAG =  VPQ.SEPRAG
WHERE PM.STATO='V'
      AND PM.DATA_158 IS NOT NULL
      AND GPM.STATO=1
      AND GPM.ID_QUIETANZA IS NOT NULL
      AND (
        PME.DATA_BONIFICA > TO_DATE('{year}-{month}-{day}', 'YYYY-MM-DD')
        or PM.DATA_BONIFICA > TO_DATE('{year}-{month}-{day}', 'YYYY-MM-DD')
        or GPM.DATA_BONIFICA > TO_DATE('{year}-{month}-{day}', 'YYYY-MM-DD')
        or Q.DATA_BONIFICA > TO_DATE('{year}-{month}-{day}', 'YYYY-MM-DD')
      )
      AND TRUNC(GPM.DATA_158,'MONTH')=TO_DATE('{year}-{month}','YYYY-MM')
