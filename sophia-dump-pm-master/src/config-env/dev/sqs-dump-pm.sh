#!/bin/sh

APPNAME=sqs-dump-pm
VERSION=1.1

JARNAME=sophia-dump-pm-$VERSION-jar-with-dependencies.jar
CFGNAME=dump-pm.properties
HOME=/home/sualkemy/sophia-dump-dev
#OUTPUT=/dev/null
OUTPUT=$HOME/$APPNAME.out
#OUTPUT=$APPNAME-$(date +%Y%m%d%H%M%S).log
JVMOPTS="-Ddefault.home_folder=$HOME -Dlog4j.configuration=file://$HOME/log4j.properties -Dlog4j.configurationFile=$HOME/log4j2.xml"

cd $HOME

echo "$(date +%Y%m%d%H%M%S) $APPNAME" >> $OUTPUT

nohup java $JVMOPTS -cp $HOME/$JARNAME com.alkemytech.sophia.pm.dump.SqsPmDump $HOME/$CFGNAME 2>&1 >> $OUTPUT &

echo "$(date +%Y%m%d%H%M%S) $APPNAME" >> $HOME/crontab.log


