package com.alkemytech.sophia.pm.dump;

import java.net.ServerSocket;
import java.text.DateFormat;
import java.util.Date;
import java.util.Properties;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.sqs.SqsMessageHelper;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Scopes;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

public class SqsPmDump {

	private static final Logger logger = LoggerFactory.getLogger(SqsPmDump.class);
	
	private static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// amazon service(s)
			bind(S3.class)
				.in(Scopes.SINGLETON);
			bind(SQS.class)
				.in(Scopes.SINGLETON);
			// data source(s)
			bind(DataSource.class)
				.annotatedWith(Names.named("SUN"))
				.to(SUnDataSource.class)
				.in(Scopes.SINGLETON);
			// other binding(s)
			bind(Date.class)
				.annotatedWith(Names.named("start_date"))
				.toInstance(new Date());
			// self
			bind(PmDump.class).asEagerSingleton();
			bind(SqsPmDump.class).asEagerSingleton();
		}
		
	}

	public static void main(String[] args) {
		try {
			final Injector injector = Guice
					.createInjector(new GuiceModuleExtension(args, "/configuration.properties"));
			final SqsPmDump instance = injector
					.getInstance(SqsPmDump.class)
					.startup();
			try {
				instance.process();
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}
	
	private final Properties configuration;
	private final int lockPort;
	private final SQS sqs;
	private final PmDump pmDump;

	@Inject
	protected SqsPmDump(@Named("configuration") Properties configuration,
			@Named("default.bind_port") int lockPort,
			SQS sqs, 
			PmDump pmDump) {
		super();
		this.configuration = configuration;
		this.lockPort = lockPort;
		this.sqs = sqs;
		this.pmDump = pmDump;
	}

	private SqsPmDump startup() {
		sqs.startup();
		pmDump.startup();
		return this;
	}

	private SqsPmDump shutdown() {
		sqs.shutdown();
		pmDump.shutdown();
		return this;
	}

	private void process() throws Exception {
		
		logger.debug("process: start time {}", 
				DateFormat.getTimeInstance().format(new Date()));
		
		try (final ServerSocket lockSocket = new ServerSocket(lockPort)) { // avoid duplicate executions
			final SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration, "pmdump.sqs");
			sqsMessagePump.pollingLoop(null, new SqsMessagePump.Consumer() {
				
				private JsonObject output;
				private JsonObject error;

				@Override
				public JsonObject getStartedMessagePayload(JsonObject message) {
					output = null;
					error = null;
					return SqsMessageHelper.formatContext();
				}

				@Override
				public boolean consumeMessage(JsonObject message) {
					try {
						output = new JsonObject();
						processMessage(message, output);
						error = null;
						return true;
					} catch (Exception e) {
						output = null;
						error = SqsMessageHelper.formatError(e);
						return false;
					}
				}

				@Override
				public JsonObject getCompletedMessagePayload(JsonObject message) {
					return output;
				}

				@Override
				public JsonObject getFailedMessagePayload(JsonObject message) {
					return error;
				}

			});
		}
		
		logger.debug("dump: end time {}", DateFormat.getTimeInstance().format(new Date()));

	}
	
	private void processMessage(JsonObject input, JsonObject output) throws Exception {
		
//		{
//			"body": {
//				"year": "2016",
//				"month": "07"
//			},
//			"header": {
//				"queue": "dev_to_process_pmdump",
//				"timestamp": "2017-05-09T01:15:00.000000+02:00",
//				"uuid": "b3e00ae5-5145-40e6-96d4-f63cac680aaa",
//				"sender": "aws-console"
//			}
//		}

		pmDump.dump(input.getAsJsonObject("body"), output);

	}
	
}
