package com.alkemytech.sophia.pm.dump;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.sql.Types;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;
import java.util.zip.GZIPOutputStream;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.google.common.base.Strings;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Scopes;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

/**
 * @author roberto cerfogli <r.cerfogli@alkemytech.it>
 */
public class PmDump {

	private static final Logger logger = LoggerFactory.getLogger(PmDump.class);

	private static class GuiceModuleExtension extends GuiceModule {
		
		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// amazon service(s)
			bind(S3.class)
				.in(Scopes.SINGLETON);
			bind(SQS.class)
				.in(Scopes.SINGLETON);
			// data source(s)
			bind(DataSource.class)
				.annotatedWith(Names.named("SUN"))
				.to(SUnDataSource.class)
				.in(Scopes.SINGLETON);
			// other binding(s)
			bind(Date.class)
				.annotatedWith(Names.named("start_date"))
				.toInstance(new Date());
			// self
			bind(PmDump.class).asEagerSingleton();
		}
		
	}
		
	public static void main(String[] args) {
		try {
			final Injector injector = Guice
					.createInjector(new GuiceModuleExtension(args, "/configuration.properties"));
			final PmDump instance = injector
					.getInstance(PmDump.class)
					.startup();
			try {
				instance.process();
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}
	
	private final Properties configuration;
	private final DataSource dataSource;
	private final String csvFieldSeparator;
	private final String csvLineSeparator;
	private final S3 s3;

	@Inject
	protected PmDump(@Named("configuration") Properties configuration,
			@Named("SUN") DataSource dataSource,
			@Named("csv.separator.field") String csvFieldSeparator,
			@Named("csv.separator.line") String csvLineSeparator,
			S3 s3) {
		super();
		this.configuration = configuration;
		this.dataSource = dataSource;
		this.csvFieldSeparator = csvFieldSeparator;
		this.csvLineSeparator = csvLineSeparator;
		this.s3 = s3;
	}
	
	protected PmDump startup() {
		s3.startup();
		return this;
	}

	protected PmDump shutdown() {
		s3.shutdown();
		return this;
	}

	private String escape(String value) {
		return escape(value, "");
	}

	private String escape(String value, String nullValue) {
		return null == value ? nullValue :
			'"' + value.replace("\"", "\"\"") + '"';
	}
	
	private void uploadToS3AndDelete(String s3bucket, String s3folder, File file) {
		if ("true".equalsIgnoreCase(configuration.getProperty("pmdump.s3.upload_flag"))) {
			final String key = String.format("%s/%s", s3folder, file.getName());
			if (s3.upload(new S3.Url(s3bucket, key), file)) {
				if ("true".equalsIgnoreCase(configuration.getProperty("pmdump.s3.delete_on_success"))) {
					file.delete();
				}
			}
		}
	}
	
	private int dump(Connection connection, String sql, String s3bucket, String s3folder, String filename) throws Exception {
		logger.debug("dump: sql {}", sql);
		logger.debug("dump: s3bucket {}", s3bucket);
		logger.debug("dump: s3folder {}", s3folder);
		logger.debug("dump: filename {}", filename);
		final DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		final File file = new File(filename);
		logger.debug("dump: file {}", file);
		try {
			int row = 0;
			try (final FileOutputStream stream = new FileOutputStream(file);
					final GZIPOutputStream out = new GZIPOutputStream(stream);
					final Statement stmt = connection.createStatement();
					final ResultSet rset = stmt.executeQuery(sql)) {
				
				final ResultSetMetaData meta = rset.getMetaData();
				final int cols = meta.getColumnCount();
				
				// column names
				if ("true".equalsIgnoreCase(configuration.getProperty("pmdump.csv.print_column_names"))) {
					out.write(meta.getColumnName(1).getBytes("UTF-8"));
					for (int i = 2; i <= cols; i ++) {
						out.write(csvFieldSeparator.getBytes("UTF-8"));
						out.write(meta.getColumnName(i).getBytes("UTF-8"));					
					}
					out.write(csvLineSeparator.getBytes("UTF-8"));
				}
				
				// column types
				if ("true".equalsIgnoreCase(configuration.getProperty("pmdump.csv.print_column_types"))) {
					out.write(Integer.toString(meta.getColumnType(1)).getBytes("UTF-8"));
					for (int i = 2; i <= cols; i ++) {
						out.write(csvFieldSeparator.getBytes("UTF-8"));
						out.write(Integer.toString(meta.getColumnType(i)).getBytes("UTF-8"));
					}
					out.write(csvLineSeparator.getBytes("UTF-8"));
				}

				// row(s)
				while (rset.next()) {
					row ++;
					for (int col = 1; col <= cols; col ++) {
						String value = "";
						switch (meta.getColumnType(col)) {
						case Types.DATE: {
							final Date date = rset.getDate(col);
							if (null != date) {
								value = escape(format.format(date));
							}
						} break;
						case Types.TIMESTAMP: {
							final Date date = rset.getTimestamp(col);
							if (null != date) {
								value = escape(format.format(date));
							}
						} break;
						case Types.NUMERIC: {
							final BigDecimal number = rset.getBigDecimal(col);
							if (null != number) {
								value = number.toPlainString();
							}
						} break;
						case Types.BOOLEAN:
						case Types.BIT:
						case Types.TINYINT:
						case Types.SMALLINT:
						case Types.INTEGER:
						case Types.BIGINT:
							value = Integer.toString(rset.getInt(col));
							break;
						case Types.CHAR:
						case Types.VARCHAR:
						default:
							value = escape(rset.getString(col));
							break;
						}
						if (rset.wasNull()) {
							value = "";
						}
						if (col > 1) {
							out.write(csvFieldSeparator.getBytes("UTF-8"));
						}
						out.write(value.getBytes("UTF-8"));
					}
					out.write(csvLineSeparator.getBytes("UTF-8"));
					out.flush();
					
					if (0 == (row % 1000)) {
						System.out.println("" + row);
					}
				}

			} finally {
				logger.info("dump: row {}", row);
			}
			
			// upload to s3
			uploadToS3AndDelete(s3bucket, s3folder, file);
			return row;
		} catch (Exception e) {
			logger.error("dump", e);
//			file.delete();
			throw e;
		}
	}
	
	public void process() throws Exception {
		
		final Calendar calendar = Calendar.getInstance();
		final JsonObject inputJsonBody = new JsonObject();
		final JsonObject outputJson = new JsonObject();

		inputJsonBody.addProperty("year", configuration
				.getProperty("pmdump.local.year", Integer.toString(calendar.get(Calendar.YEAR))));
		inputJsonBody.addProperty("month", configuration
				.getProperty("pmdump.local.month", Integer.toString(1 + calendar.get(Calendar.MONTH))));
		inputJsonBody.addProperty("day", configuration
				.getProperty("pmdump.local.day", Integer.toString(calendar.get(Calendar.DAY_OF_MONTH))));

		dump(inputJsonBody, outputJson);

	}

	public void dump(JsonObject inputJsonBody, JsonObject outputJson) throws Exception {
		try (final Connection connection = dataSource.getConnection()) {
			logger.debug("jdbc connected to {} {}", connection.getMetaData()
					.getDatabaseProductName(), connection.getMetaData().getURL());

			// unique run identifier
			final String runId = UUID.randomUUID().toString();
			
			// parse input
			System.out.println("inputJson " + inputJsonBody);
			final int year = GsonUtils.getAsInt(inputJsonBody, "year", -1);
			final int month = GsonUtils.getAsInt(inputJsonBody, "month", -1);
			final int day = GsonUtils.getAsInt(inputJsonBody, "day", 1);
			if (-1 == year || -1 == month) {
				throw new IllegalArgumentException("missing mandatory parameter");
			}
			final String fromYearMonth = GsonUtils.getAsString(inputJsonBody, "fromYearMonth",
					String.format("%04d-%02d", year - (month <= 6 ? 1 : 0), month + (month > 6 ? -6 : +6)));
			final String toYearMonth = GsonUtils.getAsString(inputJsonBody, "toYearMonth",
					String.format("%04d-%02d", year, month));
			final String s3bucket = configuration.getProperty("pmdump.s3.bucket");
			final String s3folder = configuration.getProperty("pmdump.s3.path")
					.replace("{year}", String.format("%04d", year))
					.replace("{month}", String.format("%02d", month))
					.replace("{day}", String.format("%02d", day));

			// sqs completed message
			outputJson.addProperty("runId", runId);
			outputJson.addProperty("year", String.format("%04d", year));
			outputJson.addProperty("month", String.format("%02d", month));
			outputJson.addProperty("day", String.format("%02d", day));
			outputJson.addProperty("bucket", s3bucket);
			outputJson.addProperty("folder", s3folder);
			final JsonArray entities = new JsonArray();

			Set<Integer> doNotSkip = null;
			final String executionSubSet = configuration.getProperty("pmdump.query.execution_subset");
			if (!Strings.isNullOrEmpty(executionSubSet))
				try {
					doNotSkip = new HashSet<>();
					for (String index : executionSubSet.split(","))
						doNotSkip.add(Integer.parseInt(index));
				} catch (Exception e) {
					doNotSkip = null;
				}
			
			// queries
			for (int i = 0; ; i ++) {
				final String entity = configuration.getProperty("pmdump.query." + i + ".entity");
				String sql = configuration.getProperty("pmdump.query." + i + ".sql");
				if (Strings.isNullOrEmpty(entity) ||
						Strings.isNullOrEmpty(sql)) {
					break;
				}
				if (null != doNotSkip && !doNotSkip.contains(i))
					continue;
				sql = sql.replace("{year}", String.format("%04d", year))
					.replace("{month}", String.format("%02d", month))
					.replace("{day}", String.format("%02d", day))
					.replace("{from-year-month}", fromYearMonth)
					.replace("{to-year-month}", toYearMonth);
				logger.debug("executng sql\n{}", sql);
				final String filename = String.format("%s-%s.csv.gz", entity, runId);
				
				// dump table
				final int rownum = dump(connection, sql, s3bucket, s3folder, filename);
				
				final JsonObject object = new JsonObject();
				object.addProperty("entity", entity);
				object.addProperty("filename", filename);
				object.addProperty("url", String
						.format("s3://%s/%s/%s", s3bucket, s3folder, filename));
				object.addProperty("rows", rownum);
				entities.add(object);
			}
			
			outputJson.add("entities", entities);
			System.out.println("outputJson " + outputJson);
			
		}
	}
	
}
