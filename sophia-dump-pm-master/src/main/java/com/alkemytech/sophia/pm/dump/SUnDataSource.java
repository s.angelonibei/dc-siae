package com.alkemytech.sophia.pm.dump;


import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.jdbc.DBCP2DataSource;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class SUnDataSource extends DBCP2DataSource {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Inject
	public SUnDataSource(@Named("configuration") Properties configuration,
			@Named("sun_data_source") String propertyPrefix) {
		super(configuration, propertyPrefix);
		logger.debug("propertyPrefix {}", propertyPrefix);
	}

}


