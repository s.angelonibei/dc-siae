package com.alkemy.sophia.mm.extract;

import java.io.Serializable;

public class DsrMetadataDTO implements Serializable {
	
	private static final long serialVersionUID = 2102269135184810535L;
	private String status;
	private String errorMessage;
	
	private String dsp;
	private String utilizationType;
	private String idUtilizationType;
	private String commercialOffer;
	private Integer idCommercialOffer;
	private String country;
	private String territory;
	private String periodType;
	private Integer period;
	private Integer year;
	private String periodStartDate;
	private String periodEndDate;
	private String dspCode;
	private Boolean invoiced;
	
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	public String getDsp() {
		return dsp;
	}
	public void setDsp(String dsp) {
		this.dsp = dsp;
	}
	
	public String getUtilizationType() {
		return utilizationType;
	}
	public void setUtilizationType(String utilizationType) {
		this.utilizationType = utilizationType;
	}
	
	public String getCommercialOffer() {
		return commercialOffer;
	}
	public void setCommercialOffer(String commercialOffer) {
		this.commercialOffer = commercialOffer;
	}
	
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getPeriodType() {
		return periodType;
	}
	public void setPeriodType(String periodType) {
		this.periodType = periodType;
	}
	
	public String getPeriodStartDate() {
		return periodStartDate;
	}
	public void setPeriodStartDate(String periodStartDate) {
		this.periodStartDate = periodStartDate;
	}
	
	public String getPeriodEndDate() {
		return periodEndDate;
	}
	public void setPeriodEndDate(String periodEndDate) {
		this.periodEndDate = periodEndDate;
	}
	public Integer getIdCommercialOffer() {
		return idCommercialOffer;
	}
	public void setIdCommercialOffer(Integer idCommercialOffer) {
		this.idCommercialOffer = idCommercialOffer;
	}
	public Integer getPeriod() {
		return period;
	}
	public void setPeriod(Integer period) {
		this.period = period;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public String getDspCode() {
		return dspCode;
	}
	public void setDspCode(String dspCode) {
		this.dspCode = dspCode;
	}
	public String getIdUtilizationType() {
		return idUtilizationType;
	}
	public void setIdUtilizationType(String idUtilizationType) {
		this.idUtilizationType = idUtilizationType;
	}
	public String getTerritory() {
		return territory;
	}
	public void setTerritory(String territory) {
		this.territory = territory;
	}
	public Boolean getInvoiced() {
		return invoiced;
	}
	public void setInvoiced(Boolean invoiced) {
		this.invoiced = invoiced;
	}
	
}
