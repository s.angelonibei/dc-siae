package com.alkemy.sophia.mm.extract;

import com.alkemytech.sophia.commons.aws.S3;
import org.apache.commons.lang.StringUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public class NormalizeUtil {
    
    public static List<String> listTitleInitRemove= null;
    public static Map<String, String> mapTitleSub = null;
    public static Map<String, String> mapArtistsSub = null;
    public static Configuration configuration;
    
    private static String[] specialCharArray;
    private static int maxTitLenght = 40;


    public static String getSiadaTitle(String title){
        final String[] result = {title};
        
        
        if(StringUtils.isNotBlank(result[0])){
            mapTitleSub.forEach((k, v) -> result[0] = result[0].replace(k, v));


            listTitleInitRemove.stream()
                    .filter( p -> result[0].indexOf(p) == 0)
                    .forEach( s ->  result[0] = result[0].substring(s.length()));

            result[0] = result[0].replace("."," ");
            result[0] = result[0].replace("'"," ");
            
            result[0] = result[0].replaceAll("\\([^)]*\\)", "");
            result[0] = result[0].replaceAll("\\s{2,}", " ");
            result[0] = result[0].trim().toUpperCase();
            
            if (StringUtils.isNotBlank(result[0]))
                if(result[0].length()> maxTitLenght)
                    return result[0].substring(0,maxTitLenght);
                else
                    return result[0];
            
        }
        return "SOPHIANOTITLE";

    }

    public static String getSiadaArtists(String artists){
        final String[] result = {artists+"|"};
        final String resultFilter = "";
        
        if(StringUtils.isNotBlank(result[0])){

            mapArtistsSub.forEach((k, v) -> result[0] = result[0].replace(k, v));

            result[0] = result[0].toUpperCase();
            //EFFETTUA LA SEGUENTE SOSTITUZIONE PER EVITARE SUCCESSIVAMENTE DI PERDERE EVENTUALI
            //        ARTISTI IL CUI COGNOME SIA WITH
            result[0] = result[0].replace("WITH |"," WITH|");

            //SPLITTA GLI ARTISTI IN BASE AI SEPARATORI "|", ",", ";" e " WITH "
            String[] splitted = result[0].split("\\||,|;| WITH ");
            StringJoiner joiner = new StringJoiner("|");
            for (String artist : splitted) {
                for (String c : specialCharArray) {
                    artist = artist.replace(c ,"");
                }
                artist = artist.replaceAll("\\([^)]*\\)", "");
                artist = artist.trim();
                joiner.add(artist);
            }
            
            if(joiner.length() > 0)
                return joiner.toString();
            
            
        }
        return "SOPHIANOARTISTS";

    }


    public static void initTitleInitRemove(String file) {
        
//        String file = "/list_title_init_remove";
        File strToRem = new File(file);
        
        try (final InputStreamReader reader = new InputStreamReader(new FileInputStream(strToRem), StandardCharsets.UTF_8);
             final BufferedReader br = new BufferedReader(reader)){
            listTitleInitRemove = br.lines().filter(a -> a.indexOf("#") != 0).collect(Collectors.toList());
            return;
        }catch (Exception e){}
        
        try (final InputStreamReader reader = new InputStreamReader(NormalizeUtil.class.getResourceAsStream(file), StandardCharsets.UTF_8);
             final BufferedReader br = new BufferedReader(reader)){
            listTitleInitRemove = br.lines().filter(a -> a.indexOf("#") != 0).collect(Collectors.toList());
            return;

        }catch (Exception e){}
       

        throw new RuntimeException("file list_title_init_remove non trovato");
      
    }
    
    public static void initTitleChange(String file){
      
//        String file = "/list_title_sub";
        File strToChange = new File(file);
        try (final InputStreamReader reader = new InputStreamReader(new FileInputStream(strToChange), StandardCharsets.UTF_8);
             final BufferedReader br = new BufferedReader(reader)){
            
            mapTitleSub = br.lines()
                    .filter(a -> a.indexOf("#") != 0)
                    .map(str -> str.split("->"))
                    .collect(Collectors.toMap(str -> str[0], str -> str.length==1 ?  "" : str[1]));
            return;

        }catch (Exception e){}

       
        try (final InputStreamReader reader = new InputStreamReader(NormalizeUtil.class.getResourceAsStream(file), StandardCharsets.UTF_8);
             final BufferedReader br = new BufferedReader(reader)){
            
            mapTitleSub = br.lines()
                    .filter(a -> a.indexOf("#") != 0)
                    .map(str -> str.split("->"))
                    .collect(Collectors.toMap(str -> str[0], str -> str.length==1 ?  "" : str[1]));
            return;
            
        }catch (Exception e){e.printStackTrace();}


        throw new RuntimeException("file list_title_sub non trovato");
        
    }


    public static void initArtistsChange(String file){
        
//        String file = "/list_artists_sub";
        File strToChange = new File(file);
        try (final InputStreamReader reader = new InputStreamReader(new FileInputStream(strToChange), StandardCharsets.UTF_8);
             final BufferedReader br = new BufferedReader(reader)){
 
            mapArtistsSub = br.lines()
                    .filter(a -> a.indexOf("#") != 0)
                    .map(str -> str.split("->"))
                    .collect(Collectors.toMap(str -> str[0], str -> str.length==1 ?  "" : str[1]));
            return;

        }catch (Exception e){}

       
        try (final InputStreamReader reader = new InputStreamReader(NormalizeUtil.class.getResourceAsStream(file), StandardCharsets.UTF_8);
             final BufferedReader br = new BufferedReader(reader)){

            mapArtistsSub = br.lines()
                    .filter(a -> a.indexOf("#") != 0)
                    .map(str -> str.split("->"))
                    .collect(Collectors.toMap(str -> str[0], str -> str.length==1 ?  "" : str[1]));
            return;
        }catch (Exception e){}

        throw new RuntimeException("file list_artists_sub non trovato");

    }

    public static void init(Configuration conf) {
        /*
        si l"init cosi è brutto poi lo cambio
         */
      
        configuration=conf;
        
        maxTitLenght = Integer.parseInt(configuration.getProperty("output.max_tit_length", "40"));
        String specialChar = configuration.getProperty("output.special_char", "?¿«€ªæ¥µ³§£¬@²¹");
        String fileTitleRemove = configuration.getProperty("extract_normalization.fileTitleRemove", "");
        String fileTitle = configuration.getProperty("extract_normalization.fileTitle", "");
        String fileArtists = configuration.getProperty("extract_normalization.fileArtists", "");

        specialCharArray = specialChar.split("");
        
        initTitleInitRemove(fileTitleRemove);
        initTitleChange(fileTitle);
        initArtistsChange(fileArtists);
        
    }

    public static void main(String[] args) {
        init(null);
        String title = "IL Com' bello il mondo";
        String artists = "D. Magro|alessandra amoroso";
        
        System.out.println(title);
        System.out.println(getSiadaTitle(title));

        System.out.println(artists);
        System.out.println(getSiadaArtists(artists));
    }
    
    
    
        
    

}
