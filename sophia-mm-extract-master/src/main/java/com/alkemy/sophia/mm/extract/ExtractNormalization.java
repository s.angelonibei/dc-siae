package com.alkemy.sophia.mm.extract;

import com.alkemy.sophia.mm.extract.jdbc.McmdbDAO;
import com.alkemy.sophia.mm.extract.jdbc.McmdbDataSource;
import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.guice.GuiceModule;
import com.alkemytech.sophia.commons.http.HTTP;
import com.alkemytech.sophia.commons.sqs.McmdbMessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.MessageDeduplicator;
import com.alkemytech.sophia.commons.sqs.SqsMessageHelper;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.S3Utils;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.google.common.base.Strings;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Scopes;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import org.apache.commons.csv.*;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.*;
import java.math.BigDecimal;
import java.net.ServerSocket;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Stream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class ExtractNormalization {

	private static final Logger logger = LoggerFactory.getLogger(ExtractNormalization.class);

	private static class GuiceModuleExtension extends GuiceModule {

		public GuiceModuleExtension(String[] args, String resourceName) {
			super(args, resourceName);
		}

		@Override
		protected void configure() {
			super.configure();
			// amazon service(s)
			bind(S3.class)
					.in(Scopes.SINGLETON);
			bind(SQS.class)
					.in(Scopes.SINGLETON);
			// http
			bind(HTTP.class)
					.toInstance(new HTTP(configuration));

			// message deduplicator(s)
			bind(MessageDeduplicator.class)
					.to(McmdbMessageDeduplicator.class)
					.in(Scopes.SINGLETON);
			bind(Configuration.class)
					.toInstance(new Configuration(configuration));
			//datasource
			// data source(s)
			bind(DataSource.class)
					.annotatedWith(Names.named("MCMDB"))
					.to(McmdbDataSource.class)
					.asEagerSingleton();
			// data access object(s)
			bind(McmdbDAO.class)
					.asEagerSingleton();

			bind(ExtractNormalization.class)
					.asEagerSingleton();
		}

	}


	public static void main(String[] args) {
		try {
			final ExtractNormalization instance = Guice
					.createInjector(new GuiceModuleExtension(args,
							"/extract_normalization.properties"))
					.getInstance(ExtractNormalization.class)
					.startup();
			try {
				instance.process(args);
			} finally {
				instance.shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}

	private final Configuration configuration;
	private final Charset charset;
	private final Gson gson;
	private final S3 s3;
	private final SQS sqs;
	private final HTTP http;
	private final MessageDeduplicator deduplicator;
	private final McmdbDAO dao;


	@Inject
	protected ExtractNormalization(Configuration configuration,
								   @Named("charset") Charset charset, Gson gson,
								   McmdbDAO dao, S3 s3, SQS sqs, HTTP http, MessageDeduplicator deduplicator) {
		super();
		this.configuration = configuration;
		this.charset = charset;
		this.gson = gson;
		this.s3 = s3;
		this.sqs = sqs;
		this.http = http;
		this.dao = dao;
		this.deduplicator = deduplicator;
	}


	public ExtractNormalization startup() throws IOException {
		s3.startup();
		sqs.startup();
		http.startup();
		return this;
	}
	public ExtractNormalization shutdown() throws IOException {
		sqs.shutdown();
		s3.shutdown();
		http.shutdown();
		return this;
	}


	private ExtractNormalization process(String[] args) throws Exception {

		final int bindPort = Integer.parseInt(configuration.getProperty("extract_normalization.bind_port", configuration.getProperty("default.bind_port", "0")));

		// bind lock tcp port
		try (final ServerSocket socket = new ServerSocket(bindPort)) {
			logger.info("socket bound to {}", socket.getLocalSocketAddress());

			process();
		}

		return this;
	}

	private void process() throws Exception {
		
		String fakeMsg =  configuration.getProperty("fake_msg");
		if(fakeMsg!= null && fakeMsg.length()>0){
			processMessage( GsonUtils.
					fromJson(fakeMsg, JsonObject.class));
		}else {


			final SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration.getProperties(), "extract_normalization.sqs");
			
			sqsMessagePump.pollingLoop(null, new SqsMessagePump.Consumer() {

				private JsonObject output;
				private JsonObject error;

				@Override
				public JsonObject getStartedMessagePayload(JsonObject message) {
//					List<S3.Url> filesV = S3Utils.getS3UrlFromS3Message(message);
//					S3.Url s3VFile = filesV.get(0);
//					
//					String bucket = s3VFile.bucket;
//					
//					String idDsr = s3VFile.key.substring(0,s3VFile.key.lastIndexOf("_V.csv")).substring(s3VFile.key.lastIndexOf("/")+1);
//					
//					String dsp = getDspFromKey(s3VFile.key,configuration.getProperty("extract_normalization.original_source_path","original-source"));

//					// 1- call dsrmetadata ws service
//					configuration.setTag("{dsr}", idDsr);
//					configuration.setTag("{dsp}", dsp);
//
//					DsrMetadataDTO dsrMetadata = getDsrMetadata();

					
//					JsonObject result = SqsMessageHelper.formatContext();
//					output = new JsonObject();
//					output.addProperty("idDsr",idDsr);
//					output.addProperty("dsp",dsp);
//
//					sqsMessagePump.sendToProcessMessage(configuration.getProperty("extract_normalization.sqs.started_queue"),output,false);

					
					return SqsMessageHelper.formatContext();
				}

				@Override
				public boolean consumeMessage(JsonObject message) {

					try {
						output = processMessage(message);
						sqsMessagePump.sendToProcessMessage(configuration.getProperty("extract_normalization.to_identification"),output,false);
						return true;
					} catch (Exception e) {
						logger.error("", e);
						error = SqsMessageHelper.formatError(e);
						return false;
					}


				}

				@Override
				public JsonObject getCompletedMessagePayload(JsonObject message) {
					return output;
				}

				@Override
				public JsonObject getFailedMessagePayload(JsonObject message) {
					return error;
				}

			});
		}

	
	}

	private BigDecimal toBigDecimal(String value) {
		if (null == value) {
			return null;
		}
		try {
			return new BigDecimal(value.replace(',', '.'));
		} catch (NumberFormatException e) {
			return null;
		}
	}


	private JsonObject processMessage(JsonObject message){
        final JsonObject output = new JsonObject();
//				final String srcBucket = configuration.getProperty("s3.src_bucket");

		JsonObject body = GsonUtils.getAsJsonObject(message, "body");
		String filePath = GsonUtils.getAsString(body, "file_path");
		S3.Url fileV= new S3.Url(filePath);
		

//		if(filesV.size() > 1){
//			splitAndSend(message);
//			return null;
//		}else if (filesV.size() == 1){
			return processLogic(fileV);
//		}else{
//			throw new RuntimeException("No V files in Record");
//		}


	}

	private void splitAndSend(JsonObject message){

		//for JsonObject get Records ssend message

	}
	
	/*
	INPUT FIELD
0  "DSRID"
1  "TERRITORY"
2  "TRANSACTION_ID"
3  "ALBUM_TITLE"
4  "ALBUM_PROPRIETARY_ID"
5  "PROPRIETARY_ID"
6  "TITLE"
7  "ARTISTS"
8  "IPNNS"
9  "ROLES"
10 "ISWC"
11 "LABEL"
12 "ISRC"
13 "CURRENCY"
14 "COMMERCIALMODEL"
15 "UNITARY-PRICE"
16 "SALES-COUNT"
17 "SOCCDE"
18 "SOCWORKCDE"
19 "PERF_CLAIM"
20 "MECH_CLAIM"
21 "PERF_PD"
22 "MECH_PD"
23 "AMOUNT"
24 "PEDL"
25 "SOCREFCDE"
26 "SOCIETY-LICENSE-ID"
27 "ALBUM-ARTIST"
28 "TRANSACTION-TYPE"
29 "RELEASE_TYPE"
30 "UPGRADE"
31 "DRM_FLAG"
32 "UPC"
33 "GRiD"
34 "TRACK-MEDIA-TYPE"
35 "TRACK-DURATION"
36 "CONFLICT-TYPE"
37 "WAIVE_FLAG"
38 "WPB_FLAG"
39 "ORIGINAL_TITLE"
40 "ORIGINAL_ARTISTS"
41 "ORIGINAL_ROLES"
42 "ORIGINAL_ISWC"
43 "ORIGINAL_ISRC"

ADD ON OUTPUT:

44 "DSP"
45 "IDDSR"
46 "YEAR"
47 "MONTH"
48 "MONTH_END"
49 "QUARTER"
50 "SERVICE"
51 "ORIGINAL_SERVICE"
52 "IDUTIL"
53 "HASHTITLE"
54 "HASHARTISTS"
55 "IDSONG_SOPHIA"
56 "SIADA_TITLE"
57 "SIADA_ARTISTS"
58 "TRANSACTION_TRACKS"
59 "ALBUM_TRACKS"
	
	
	
	 */


	private JsonObject processLogic(S3.Url fileV){

		NormalizeUtil.init(configuration);

		JsonObject output = new JsonObject();

		final int maxRows = Integer.parseInt(configuration.getProperty("output.max_rows", "10000"));

		final int idxCurrency = Integer.parseInt(configuration.getProperty("input.column.currency", "13"));
		final int idxTransactionId = Integer.parseInt(configuration.getProperty("input.column.transaction_id", "2"));
		final int idxAlbumProprietaryId = Integer.parseInt(configuration.getProperty("input.column.album_proprietary_id", "4"));
		final int idxProprietaryId = Integer.parseInt(configuration.getProperty("input.column.proprietary_id", "5"));
		final int idxTitle = Integer.parseInt(configuration.getProperty("input.column.title", "6"));
		final int idxArtists = Integer.parseInt(configuration.getProperty("input.column.artists", "7"));
		
		final int idxTransactionTracks = Integer.parseInt(configuration.getProperty("input.column.transactionTracks", "58"));
		final int idxAlbumTracks = Integer.parseInt(configuration.getProperty("input.column.albumtracks", "59"));

		final String destinationUrlPath = configuration.getProperty("extract_normalization.output_url");
		final String env = configuration.getProperty("default.environment");
		final String originalSourceFolder = configuration.getProperty("extract_normalization.original_source_path","original-source");


		final String[] cols = new String[60];
		final AtomicLong tid = new AtomicLong(1L);

		S3Object s3VFile = s3.getObject(fileV);

		String bucket = s3VFile.getBucketName();
		String idDsr = s3VFile.getKey().substring(0,s3VFile.getKey().lastIndexOf("_V.csv")).substring(s3VFile.getKey().lastIndexOf("/")+1);
		String path = s3VFile.getKey().substring(0,s3VFile.getKey().lastIndexOf("/")+1);
		String dsp = getDspFromKey(s3VFile.getKey(),originalSourceFolder);

		S3.Url s3Gz = new S3.Url(bucket,path+idDsr+".csv.gz");

		// 1- call dsrmetadata ws service
        configuration.setTag("{dsr}", idDsr);
		configuration.setTag("{dsp}", dsp);
		
		clearTmpFolder();

        DsrMetadataDTO dsrMetadata = getDsrMetadata();

		String quarter;
		String month;
		String endMonth;
		
		String s3Period;
		
		if(dsrMetadata.getPeriodType().equalsIgnoreCase("quarter")){
			quarter = "Q" + dsrMetadata.getPeriod();
			month = quarter;
			endMonth = (3* dsrMetadata.getPeriod()) + "";
			s3Period=month;
		}else{
			quarter = "Q" + new Double(Math.ceil(dsrMetadata.getPeriod()/3.0)).intValue();
			month = dsrMetadata.getPeriod().toString();
			endMonth = month;

			s3Period=String.format("%02d", dsrMetadata.getPeriod());
		}

		// configuration tags
		configuration.setTag("{year}", dsrMetadata.getYear()+"");
		configuration.setTag("{period}", s3Period);


		// 2 - check is invoiced -> no run

        if(dsrMetadata.getInvoiced()){
//		if (isDsrInvoiced()) {
			throw new IllegalStateException("DSR already invoiced: " + idDsr);
		}
        //TODO: verificare anche se backlaimato, in caso non posso constinuare

		// 3 - saveStorico ws

        saveStorico();

        BigDecimal totalValue = new BigDecimal(0);
        long totalSales = 0L;
        long totalRows = 0L;

        boolean isFirst = true;
        String currency = "";


		// 4 - first iterate

		// TRANSACTION_TRACKS hashmap <String,Set<String>>  <TRANSACTioN_ID,set<ID_UTIL>>
		// ALBUM_TRACKS hasmap<String,Set<String>>  <TRANSACTioN_ID_ALBUM_PROPRIETARY_ID,set<PROPRIETARY_ID>>
		//count su total_lines

		final List<String[]> colsToPrint = new ArrayList<>();
		
		String lastTransactionId = null;


		FileOutputStream out = null;
		CSVPrinter printer = null;

		totalRows=0;
		try (final InputStream in = new FileInputStream(new File(downloadAndSort(s3Gz,idDsr)));
			 final InputStreamReader reader = new InputStreamReader(in, StandardCharsets.UTF_8);
			 final CSVParser parser = new CSVParser(reader, CSVFormat.newFormat(';'))) {


			int partFile = 0;
			String uuid = UUID.randomUUID().toString();
			// loop on csv row(s)
			for (CSVRecord record : parser) {

				if(totalRows % maxRows == 0){
					//close file
					if(printer != null) printer.close();
					if(out != null) out.close();

					//out = new GZIPOutputStream(new FileOutputStream("part-"+(String.format("%05d",++partFile))+"-"+uuid+".csv", false));
					out = new FileOutputStream(configuration.getProperty("extract_normalization.working_directory")+"part-"+(String.format("%05d",++partFile))+"-"+uuid+".csv", false);
					printer = new CSVPrinter(new OutputStreamWriter(out,StandardCharsets.UTF_8),CSVFormat.EXCEL.withDelimiter(';').withQuoteMode(QuoteMode.MINIMAL));
					//open new file
					logger.info("creo file part-"+(String.format("%05d",partFile))+"-"+uuid+".csv  rows: " + totalRows);
				}
				if(totalRows%10000 == 0)
					logger.info("line read "+ totalRows);
				
				totalRows++;

				int count = 0;
				if (record.size() > 30) {  // <- valore a cazium

					for (String col : record) {
						cols[count++] = col;
					}
					cols[count++] = dsp;
					cols[count++] = idDsr;
					cols[count++] = dsrMetadata.getYear()+"";
					cols[count++] = month;
					cols[count++] = endMonth;
					cols[count++] = quarter;
					cols[count++] = dsrMetadata.getUtilizationType();
					cols[count++] = dsrMetadata.getCommercialOffer();
					cols[count++] = idDsr + "_" + totalRows;
					cols[count++] = "HASHTITLE";
					cols[count++] = "HASHARTISTS";
					cols[count++] = "IDSONG_SOPHIA";
					cols[count++] = NormalizeUtil.getSiadaTitle(cols[idxTitle]);
					cols[count++] = NormalizeUtil.getSiadaArtists(cols[idxArtists]);

					if (isFirst) {
						//get CURRENCY
						isFirst = false;
						currency = cols[idxCurrency];
					}

					if(lastTransactionId != null && !colsToPrint.isEmpty()){
						//check
						if(!colsToPrint.get(0)[idxTransactionId].equals(cols[idxTransactionId])){

							//calcolo transactionid e albumnid
							
							String transactionTracks= colsToPrint.size()+"";
							if(colsToPrint.size()>1)
								logger.info("transactionTrack: " + transactionTracks);
							Set<String> setAlbumTracks = new HashSet<>();

							for (String[] col : colsToPrint) {
								col[idxTransactionTracks]=transactionTracks;
								
								for (String[] colTmp : colsToPrint) {
									if(col[idxAlbumProprietaryId].equals(colTmp[idxAlbumProprietaryId])){
										setAlbumTracks.add(colTmp[idxProprietaryId]);
									}
								}
								if(setAlbumTracks.size()>1)
									logger.info("setAlbumTracks: " + setAlbumTracks.size());
								col[idxAlbumTracks] = setAlbumTracks.size()+"";
								setAlbumTracks.clear();
								
							}

							//print csv
							for (String[] cList : colsToPrint) {
								for (String c : cList) {
									printer.print(c);
								}
								printer.println();
							}
							//purge array
							colsToPrint.clear();
							colsToPrint.add(cols.clone());
							lastTransactionId=cols[idxTransactionId];

						}else{
							//add to list
							colsToPrint.add(cols.clone());
						}
					}else{
						lastTransactionId=cols[idxTransactionId];
						colsToPrint.add(cols.clone());
					}

				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}finally {
			try {
				//TODO: printa l'ultima lista di oggetti
				String transactionTracks= colsToPrint.size()+"";
				Set<String> setAlbumTracks = new HashSet<>();

				for (String[] col : colsToPrint) {
					col[idxTransactionTracks]=transactionTracks;

					for (String[] colTmp : colsToPrint) {
						if(col[idxAlbumProprietaryId].equals(colTmp[idxAlbumProprietaryId])){
							setAlbumTracks.add(colTmp[idxProprietaryId]);
						}
					}
					col[idxAlbumTracks] = setAlbumTracks.size()+"";

				}

				//print csv
				for (String[] cList : colsToPrint) {
					for (String c : cList) {
						printer.print(c);
					}
					printer.println();
				}
				//purge array
				colsToPrint.clear();
				colsToPrint.add(cols.clone());
				lastTransactionId=cols[idxTransactionId];
				
				
				if (printer != null) printer.close();
				if (out != null) out.close();
			}catch (Exception e){e.printStackTrace();}
		}
		
		BigDecimal totalValueV = BigDecimal.ZERO;
		BigDecimal totalSubscriptionV = BigDecimal.ZERO;
		BigDecimal totalSalesV = BigDecimal.ZERO;
		
		
		//prendo i dati dal file V
		try (final InputStream in = s3VFile.getObjectContent();
			 final InputStreamReader reader = new InputStreamReader(in, StandardCharsets.UTF_8);
			 final CSVParser parser = new CSVParser(reader, CSVFormat.newFormat(';'))) {



			// loop on csv row(s)
			for (CSVRecord record : parser) {

				totalValueV = toBigDecimal(record.get(0));
				totalSubscriptionV = toBigDecimal(record.get(1));
				totalSalesV= toBigDecimal(record.get(2));

				logger.debug("doWork: totalValue {} --> {}", totalValueV, totalValue);
				logger.debug("doWork: totalSales {} --> {}", totalSalesV, totalSales);
				
				break;
			}
		}catch (Exception e){ throw new RuntimeException(e.getMessage());}
		
		//TODO: remove all s3 old file in norm source
		List<S3ObjectSummary> listToDelete = s3.listObjects(new S3.Url(configuration.getProperty("extract_normalization.output_url")+ "/"));

		for (S3ObjectSummary s3ObjectSummary : listToDelete) {
			s3.delete(new S3.Url(s3ObjectSummary.getBucketName(),s3ObjectSummary.getKey()));
		}

		try (Stream<Path> paths = Files.walk(Paths.get(configuration.getProperty("extract_normalization.working_directory")))) {
			paths.filter(f -> Files.isRegularFile(f) )
					.forEach(f-> {
						try {
							if(f.toString().endsWith(".csv")) {
								//upload to s3 and delete
								s3.upload(new S3.Url(configuration.getProperty("extract_normalization.output_url")+ "/" +f.getFileName()),f.toFile());
								logger.info("upload file " + f.getFileName());
								Files.delete(f);
							}else if(f.toString().endsWith(".sort")){
								Files.delete(f);
							}else{
								logger.warn("keep file:" + f.getFileName());
							}
						} catch (IOException e) {
							e.printStackTrace();
						}
					});
		}catch (Exception e){
			//swallow
			e.printStackTrace();
		}
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		df.setMinimumFractionDigits(0);
		df.setGroupingUsed(false);

		//5 - aggiorno tabella hive original-source e norm-source?

        //5a - aggiorno tabella steps_monitoring con lo status (COMPLETED - FAILED)
		try (final Connection mcmdb = dao.getDataSource().getConnection()) {
			logger.debug("process: connected to {} {}", mcmdb.getMetaData()
					.getDatabaseProductName(), mcmdb.getMetaData().getURL());
			mcmdb.setAutoCommit(true);
			
			
			/*
			step_monitoring ???? serve? ancora usata?
			IDDSR
			IDDDSP
			DEVC_STATUS
			DEVC_VALIDITY_RESULT
			TIMESTAMP
			 */
			//delete from DSR_METADATA ??
			
			
			try (final Statement stmt = mcmdb.createStatement()) {
				
			

				
				
				
				String insertDsrMetadataSql = configuration
						.getProperty("extract_normalization.DSR_METADATA.insert")
						.replace("{IDDSR}",idDsr)
						.replace("{TOTAL_VALUE}",df.format(totalValueV))
						.replace("{SUBSCRIPTIONS_NUM}",df.format(totalSubscriptionV))
						.replace("{SALES_LINES_NUM}",df.format(totalSalesV))
						.replace("{YEAR}",dsrMetadata.getYear()+"")
						.replace("{IDDSP}",dsrMetadata.getDsp())
						.replace("{COUNTRY}",dsrMetadata.getCountry())
						.replace("{PERIOD}",dsrMetadata.getPeriod() + "")
						.replace("{SERVICE_CODE}",dsrMetadata.getIdCommercialOffer()+"")
						.replace("{CREATION_TIMESTAMP}",(new Date()).getTime()/1000 +"")
						.replace("{PERIOD_TYPE}",dsrMetadata.getPeriodType())
						.replace("{CURRENCY}",currency);

				String updateDsrMetadataSql = configuration
						.getProperty("extract_normalization.DSR_METADATA.update")
						.replace("{IDDSR}",idDsr)
						.replace("{TOTAL_VALUE}",df.format(totalValueV))
						.replace("{SUBSCRIPTIONS_NUM}",df.format(totalSubscriptionV))
						.replace("{SALES_LINES_NUM}",df.format(totalSalesV))
						.replace("{YEAR}",dsrMetadata.getYear()+"")
						.replace("{IDDSP}",dsrMetadata.getDsp())
						.replace("{COUNTRY}",dsrMetadata.getCountry())
						.replace("{PERIOD}",dsrMetadata.getPeriod() + "")
						.replace("{SERVICE_CODE}",dsrMetadata.getIdCommercialOffer()+"")
						.replace("{CREATION_TIMESTAMP}",(new Date()).getTime()/1000 +"")
						.replace("{PERIOD_TYPE}",dsrMetadata.getPeriodType())
						.replace("{CURRENCY}",currency);

				logger.info(insertDsrMetadataSql);
				logger.info(updateDsrMetadataSql);
				
				
//				String insertDsrMetadataSql = configuration.getProperty("extract_normalization.DSR_METADATA.insert");

				String insertDsrStatisticsSql = configuration
						.getProperty("extract_normalization.DSR_STATISTICS.insert")
						.replace("{IDDSR}",idDsr)
						.replace("{TOTAL_LINES}",totalRows+"");

				String updateDsrStatisticsSql = configuration
						.getProperty("extract_normalization.DSR_STATISTICS.update")
						.replace("{IDDSR}",idDsr)
						.replace("{TOTAL_LINES}",totalRows+"");

				logger.info(insertDsrStatisticsSql);
				logger.info(updateDsrStatisticsSql);
				
				//				String insertDsrStatisticsSql = configuration.getProperty("extract_normalization.DSR_STATISTICS.insert");
				
				if(stmt.executeUpdate(updateDsrMetadataSql)!=1)
					stmt.executeUpdate(insertDsrMetadataSql);

				if(stmt.executeUpdate(updateDsrStatisticsSql)!=1)
					stmt.executeUpdate(insertDsrStatisticsSql);
				

			}
		}catch (Exception e){
			e.printStackTrace();
			throw new RuntimeException("impossibile salvare le statistiche: " + e.getMessage());
		}

		//6 - send complete & to_process_new_identification

		
		output.addProperty("TOTAL_VALUE",df.format(totalValueV));
		output.addProperty("SUBSCRIPTIONS_NUM",df.format(totalSubscriptionV));
		output.addProperty("SALES_LINES_NUM",df.format(totalSalesV));
		output.addProperty("YEAR",dsrMetadata.getYear()+"");
		output.addProperty("IDDSP",dsrMetadata.getDsp());
		output.addProperty("COUNTRY",dsrMetadata.getCountry());
		output.addProperty("PERIOD",dsrMetadata.getPeriod() + "");
		output.addProperty("SERVICE_CODE",dsrMetadata.getIdCommercialOffer()+"");
		output.addProperty("CREATION_TIMESTAMP",(new Date()).getTime()/1000 +"");
		output.addProperty("PERIOD_TYPE",dsrMetadata.getPeriodType());
		output.addProperty("CURRENCY",currency);
		output.addProperty("idDsr",idDsr);
		output.addProperty("dsp",dsrMetadata.getDsp());
		output.addProperty("country",dsrMetadata.getCountry());
		output.addProperty("dspCode",dsrMetadata.getDspCode());
		output.addProperty("dsr_file_norm",configuration.getProperty("extract_normalization.output_url").substring(5));
//		sqs.sendMessage(configuration.getProperty("extract_normalization.real_completed_queue"),output.toString());

		return output;
	}

	private void clearTmpFolder() {
		try (Stream<Path> paths = Files.walk(Paths.get(configuration.getProperty("extract_normalization.working_directory")))) {
			paths
					.filter(f -> Files.isRegularFile(f) )
					.forEach(f-> {
						try {
							Files.delete(f);
						} catch (IOException e) {
							e.printStackTrace();
						}
					});
		}catch (Exception e){
			//swallow
			e.printStackTrace();
		}
	}


	private String getDspFromKey(String key, String originalSourcePath){

		String[] split = key.split("/");

		for(int i=0; i<split.length;i++){
			if(split[i].equalsIgnoreCase(originalSourcePath)){
				//dsp
				return split[i+1];
			}
		}
		return null;
	}

    private DsrMetadataDTO getDsrMetadata() {
        try {
            final HTTP.Response response = http.get(configuration
                    .getProperty("extract_normalization.extract_metadata_url"));
            logger.debug("getDsrMetadata: response status  {}", response.status);
            logger.debug("getDsrMetadata: response body {}", response.getBodyAsString(charset));
            if (response.isOK()) {
                final DsrMetadataDTO responseBody = GsonUtils
                        .fromJson(response.getBodyAsString(charset),DsrMetadataDTO.class);
                return responseBody;
            }
        } catch (Exception e) {
            logger.error("isDsrInvoiced", e);
        }
        return null;
    }
    private boolean saveStorico() {
        try {
            final HTTP.Response response = http.get(configuration
                    .getProperty("extract_normalization.save_storico_url"));
            logger.debug("saveStorico: response status  {}", response.status);
            logger.debug("saveStorico: response body {}", response.getBodyAsString(charset));
            if (response.isOK()) {
                final DsrMetadataDTO responseBody = GsonUtils
                        .fromJson(response.getBodyAsString(charset),DsrMetadataDTO.class);
                return true;
            }
        } catch (Exception e) {
            logger.error("isDsrInvoiced", e);
        }
        return false;
    }

    private boolean upsertDsrMetadata(){
		//TODO
	    throw new UnsupportedOperationException();
    }

	private boolean upsertDsrStatistics(){
		//TODO
		throw new UnsupportedOperationException();
	}


	public String downloadAndSort(S3.Url gzFile, String idDsr){
		
		final String inputFileName = configuration.getProperty("extract_normalization.working_directory")  + idDsr + ".csv.gz";
		final String outputFileName = configuration.getProperty("extract_normalization.working_directory")  + idDsr + ".csv";
		final String sortedFileName = configuration.getProperty("extract_normalization.working_directory") +  idDsr + ".sort";
		
	
		// data folder
		final File dataFolder = new File(configuration.getProperty("extract_normalization.working_directory"));
		dataFolder.mkdirs();

		final File inputFile = new File(inputFileName);



		if (!s3.download(gzFile, inputFile)) {
			inputFile.delete();
			throw new RuntimeException("file download error: " + gzFile.key);
		}

		//exect gunzip e  sort
		// untar archive file
		final String[] cmdlineGunzip = new String[] {
				configuration.getProperty("extract_normalization.gunzip_path", "/bin/gunzip"),
				inputFileName
		};

		final String[] cmdlineSort = new String[] {
				configuration.getProperty("extract_normalization.gunzip_path", "/usr/bin/sort"),
				"--field-separator=;",
				"--key=3,5",
				"--output="+sortedFileName,
				outputFileName
		};

		try {
			final int exitCode1 = Runtime
					.getRuntime().exec(cmdlineGunzip).waitFor();


			final Process proc = Runtime
					.getRuntime().exec(cmdlineSort);

			InputStream stdin = proc.getErrorStream();
			InputStreamReader isr = new InputStreamReader(stdin);
			BufferedReader br = new BufferedReader(isr);
			String line = null;
			System.out.println("<OUTPUT>");
			while ( (line = br.readLine()) != null)
				System.out.println(line);
			System.out.println("</OUTPUT>");
			final int exitCode2 = proc.waitFor();


		}catch (Exception e){
			throw new RuntimeException("Errore nell'esecuzione di gunzip o sort");
		}finally {
			//TODO: remove gzip file e unzipped file
			inputFile.delete();
			new File(outputFileName).delete();
			
		}
		return sortedFileName;
	}


}
