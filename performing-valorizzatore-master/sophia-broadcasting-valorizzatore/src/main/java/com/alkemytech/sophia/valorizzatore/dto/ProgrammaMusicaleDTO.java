package com.alkemytech.sophia.valorizzatore.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ProgrammaMusicaleDTO {
	private Long idProgrammaMusicale;
	private Double valoreEconomico;
	private Long puntiPm;
	private List<UtilizzazioneDTO> utilizzazioni;
	private Long puntiEsclusi;
	private Long nUtilizzazioniEscluse;
	

	public ProgrammaMusicaleDTO() {
		super();
	}

	public ProgrammaMusicaleDTO(Long idProgrammaMusicale, Double valoreEconomico, Long puntiPm,
			List<UtilizzazioneDTO> utilizzazioni) {
		super();
		this.idProgrammaMusicale = idProgrammaMusicale;
		this.valoreEconomico = valoreEconomico;
		this.puntiPm = puntiPm;
		this.utilizzazioni = utilizzazioni;
	}

	public Long getIdProgrammaMusicale() {
		return idProgrammaMusicale;
	}

	public void setIdProgrammaMusicale(Long idProgrammaMusicale) {
		this.idProgrammaMusicale = idProgrammaMusicale;
	}

	public Double getValoreEconomico() {
		return valoreEconomico;
	}

	public void setValoreEconomico(Double valoreEconomico) {
		this.valoreEconomico = valoreEconomico;
	}

	public Long getPuntiPm() {
		return puntiPm;
	}

	public void setPuntiPm(Long puntiPm) {
		this.puntiPm = puntiPm;
	}

	public List<UtilizzazioneDTO> getUtilizzazioni() {
		return utilizzazioni;
	}

	public void setUtilizzazioni(List<UtilizzazioneDTO> utilizzazioni) {
		this.utilizzazioni = utilizzazioni;
	}

	public Long getPuntiEsclusi() {
		return puntiEsclusi;
	}

	public void setPuntiEsclusi(Long puntiEsclusi) {
		this.puntiEsclusi = puntiEsclusi;
	}

	public Long getnUtilizzazioniEscluse() {
		return nUtilizzazioniEscluse;
	}

	public void setnUtilizzazioniEscluse(Long nUtilizzazioniEscluse) {
		this.nUtilizzazioniEscluse = nUtilizzazioniEscluse;
	}


	
}
