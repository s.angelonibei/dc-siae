package com.alkemytech.sophia.valorizzatore.utils;

import java.util.ArrayList;
import java.util.List;

import com.alkemytech.sophia.valorizzatore.dto.ConfigurationDTO;
import com.alkemytech.sophia.valorizzatore.dto.EventoDTO;
import com.alkemytech.sophia.valorizzatore.dto.ProgrammaMusicaleDTO;
import com.alkemytech.sophia.valorizzatore.dto.TipologiaPM;
import com.alkemytech.sophia.valorizzatore.dto.UtilizzazioneDTO;
import com.alkemytech.sophia.valorizzatore.dto.ConfigurationDTO;
import com.alkemytech.sophia.valorizzatore.dto.TipologiaPM;;

public class TestingCreaEventi {
	
	
	
public static List<EventoDTO> creaEventi(){
		
	  TipologiaPM tipologiaPMdigitale=TipologiaPM.CARTACEO;
      TipologiaPM tipologiaPMcartacea=TipologiaPM.DIGITALE;
      
		List<EventoDTO> eventi = new ArrayList<>();
		List<ProgrammaMusicaleDTO> listPMe1=new ArrayList<>();// 1 Pm atteso 0 rientrati
		List<ProgrammaMusicaleDTO> listPMe2=new ArrayList<>();// 2 PM attesi 1 rientrato
		List<ProgrammaMusicaleDTO> listPMe3=new ArrayList<>();// 6 PM attesi 3 rientrato
		List<ProgrammaMusicaleDTO> listPMe4=new ArrayList<>();// 5 PM attesi 6 rientrati
		
		//inizializzazione dei programmi musicali da passare agli eventi 
		listPMe1= creaProgrammiMusicaliE1();
		listPMe2= creaProgrammiMusicaliE2();
		listPMe3= creaProgrammiMusicaliE3();
		listPMe4= creaProgrammiMusicaliE4();
		
		
		
		// Inserire all'interno 
//		 EventoDTO  e1= new EventoDTO(1l,"171","2223",listPMe1,1,0,tipologiaPMdigitale,1000.0,0l);
//		 EventoDTO  e2= new EventoDTO(2l,"171","2223",listPMe2,2,1,tipologiaPMdigitale,1000.0,0l);
//		 EventoDTO  e3= new EventoDTO(3l,"171","2223",listPMe3,6,3,tipologiaPMdigitale,1000.0,0l);
//		 EventoDTO  e4= new EventoDTO(4l,"171","2223",listPMe4,5,6,tipologiaPMdigitale,1000.0,0l);
//		
//		 EventoDTO  e5= new EventoDTO(5l,"171","2223",listPMe1,1,0,tipologiaPMcartacea,1000.0,0l);
//		 EventoDTO  e6= new EventoDTO(6l,"171","2223",listPMe2,2,1,tipologiaPMcartacea,1000.0,0l);
//		 EventoDTO  e7= new EventoDTO(7l,"171","2223",listPMe3,6,3,tipologiaPMcartacea,1000.0,0l);
//		 EventoDTO  e8= new EventoDTO(8l,"171","2223",listPMe4,5,6,tipologiaPMcartacea,1000.0,0l);
		 
		 EventoDTO  e9= new EventoDTO(1l,"171","2224",listPMe1,1,0,tipologiaPMdigitale,1000.0,0l);
		 EventoDTO  e10= new EventoDTO(2l,"171","2224",listPMe2,2,1,tipologiaPMdigitale,1000.0,0l);
		 EventoDTO  e11= new EventoDTO(3l,"171","2224",listPMe3,6,3,tipologiaPMdigitale,1000.0,0l);
		 EventoDTO  e12= new EventoDTO(4l,"171","2224",listPMe4,5,6,tipologiaPMdigitale,1000.0,0l);
		
		 EventoDTO  e13= new EventoDTO(5l,"171","2224",listPMe1,1,0,tipologiaPMcartacea,1000.0,0l);
		 EventoDTO  e14= new EventoDTO(6l,"171","2224",listPMe2,2,1,tipologiaPMcartacea,1000.0,0l);
		 EventoDTO  e15= new EventoDTO(7l,"171","2224",listPMe3,6,3,tipologiaPMcartacea,1000.0,0l);
		 EventoDTO  e16= new EventoDTO(8l,"171","2224",listPMe4,5,6,tipologiaPMcartacea,1000.0,0l);
		 
		 
		 
		 
//		eventi.add(e1);// 1 Pm atteso 0 rientrati  Tipologia digitale
//		eventi.add(e2);// 2 PM attesi 1 rientrato  ...
//		eventi.add(e3);// 6 PM attesi 3 rientrato
//		eventi.add(e4);// 5 PM attesi 6 rientrati
//		
//		eventi.add(e5);// 1 Pm atteso 0 rientrati  Tipologia cartacea...
//		eventi.add(e6);// 2 PM attesi 1 rientrato
//		eventi.add(e7);// 6 PM attesi 3 rientrato
//		eventi.add(e8);// 5 PM attesi 6 rientrati
//		
		eventi.add(e9);
		eventi.add(e10);
		eventi.add(e11);
		eventi.add(e12);
		
		eventi.add(e13);
		eventi.add(e14);
		eventi.add(e15);
		eventi.add(e16);
		return eventi;

}	
	
public static List<ProgrammaMusicaleDTO> creaProgrammiMusicaliE1() {
	
	
	
	// Caso dell'evento con unico programma musicale
	
	List<ProgrammaMusicaleDTO> listPM=new ArrayList<ProgrammaMusicaleDTO>();
	List<UtilizzazioneDTO> listUtilizzazioniPM1 =new ArrayList<>();
	
	
			
	// Creazione  dei Pm che si trovano all'interno di e1
	
	ProgrammaMusicaleDTO PM1e1=new ProgrammaMusicaleDTO(1l,1000.0,0l,listUtilizzazioniPM1);
	listPM.add(PM1e1);
	
	
	return listPM;
}

//Caso con 2 programmi musicali ed 1 ricevuto
public static List<ProgrammaMusicaleDTO> creaProgrammiMusicaliE2() {
	
	List<ProgrammaMusicaleDTO> listPM=new ArrayList<ProgrammaMusicaleDTO>();
	List<UtilizzazioneDTO> listUtilizzazioniPM1= new ArrayList<>();
	List<UtilizzazioneDTO> listUtilizzazioniPM2= new ArrayList<>();
	
	
	
	/* Inizializzazione delle utilizzazioni da inserire nei rispettivi PM LA somma dei punti pm vale 200 
	   Faccio in modo che i punti associati al PM siano diversi 
	   
	   Utilizzazioni per PM1 
	*/
	//public UtilizzazioneDTO(Long idUtilizzazione, Long idCombana, Long durataSec)
	
	UtilizzazioneDTO u1=new UtilizzazioneDTO(1l,45l,20l); //durata inferiore a 30 secondi
	UtilizzazioneDTO u2=new UtilizzazioneDTO(2l,46l,60l);
	UtilizzazioneDTO u3=new UtilizzazioneDTO(3l,47l,40l);	
	listUtilizzazioniPM1.add(u1);
	listUtilizzazioniPM1.add(u2);
	listUtilizzazioniPM1.add(u3);
	
	
	// Utilizzazioni per PM2
	UtilizzazioneDTO u1PM2=new UtilizzazioneDTO(1l,45l,20l);
	UtilizzazioneDTO u2PM2=new UtilizzazioneDTO(5l,45l,240l);
	UtilizzazioneDTO u3PM2=new UtilizzazioneDTO(6l,45l,160l);	
	listUtilizzazioniPM2.add(u1PM2);
	listUtilizzazioniPM2.add(u2PM2);
	listUtilizzazioniPM2.add(u3PM2);
	
			
	// Creazione  dei Pm che si trovano all'interno di e1
	//public ProgrammaMusicaleDTO(Long idProgrammaMusicale, Double valoreEconomico, Long puntiPm,List<UtilizzazioneDTO> utilizzazioni) {
	
	ProgrammaMusicaleDTO PM1e2=new ProgrammaMusicaleDTO(1l,1000.0,0l,listUtilizzazioniPM1);
	ProgrammaMusicaleDTO PM2e2=new ProgrammaMusicaleDTO(2l,1000.0,0l,listUtilizzazioniPM2);
	listPM.add(PM1e2);
	listPM.add(PM2e2);
	
	
	return listPM;
}
//Caso con Evento avente 6 programmi attesi 3 rientrati
public static List<ProgrammaMusicaleDTO> creaProgrammiMusicaliE3() {
	
	List<ProgrammaMusicaleDTO> listPM=new ArrayList<ProgrammaMusicaleDTO>();
	List<UtilizzazioneDTO> listUtilizzazioniPM1= new ArrayList<>();
	List<UtilizzazioneDTO> listUtilizzazioniPM2= new ArrayList<>();
	List<UtilizzazioneDTO> listUtilizzazioniPM3= new ArrayList<>();
	
	
	/* Inizializzazione delle utilizzazioni da inserire nei rispettivi PM
	
	   Utilizzazioni per PM1 
	  
	  Aggiungere caso in cui il PM si annulla... potrebbe esserci questa opzione
	
	public UtilizzazioneDTO(Long idUtilizzazione, Long idCombana, Long durataSec)
	
	*/
	
	UtilizzazioneDTO u1=new UtilizzazioneDTO(1l,45l,25l); // esclusione per durata sotto 30
	UtilizzazioneDTO u2=new UtilizzazioneDTO(2l,46l,60l);
	UtilizzazioneDTO u3=new UtilizzazioneDTO(3l,48l,40l);
	listUtilizzazioniPM1.add(u1);
	listUtilizzazioniPM1.add(u2);
	listUtilizzazioniPM1.add(u3);
	
	// Utilizzazioni per PM2    punti =3 x punti_PM1 
	UtilizzazioneDTO u1PM2=new UtilizzazioneDTO(1l,45l,25l);
	UtilizzazioneDTO u2PM2=new UtilizzazioneDTO(2l,45l,180l);
	UtilizzazioneDTO u3PM2=new UtilizzazioneDTO(3l,45l,120l);
	listUtilizzazioniPM2.add(u1PM2);
	listUtilizzazioniPM2.add(u2PM2);
	listUtilizzazioniPM2.add(u3PM2);
	
	// Utilizzazioni per PM3    punti= 6 x punti_PM1
	UtilizzazioneDTO u1PM3=new UtilizzazioneDTO(5l,45l,25l);
	UtilizzazioneDTO u2PM3=new UtilizzazioneDTO(6l,45l,360l);
	UtilizzazioneDTO u3PM3=new UtilizzazioneDTO(7l,45l,240l);
	listUtilizzazioniPM3.add(u1PM3);
	listUtilizzazioniPM3.add(u2PM3);
	listUtilizzazioniPM3.add(u3PM3);
	
		
	// Creazione  dei Pm che si trovano all'interno di e1
	
	ProgrammaMusicaleDTO PM1e3=new ProgrammaMusicaleDTO(1l,0.0,0l,listUtilizzazioniPM1);
	ProgrammaMusicaleDTO PM2e3=new ProgrammaMusicaleDTO(2l,0.0,0l,listUtilizzazioniPM2);
	ProgrammaMusicaleDTO PM3e3=new ProgrammaMusicaleDTO(3l,0.0,0l,listUtilizzazioniPM3);
	listPM.add(PM1e3);
	listPM.add(PM2e3);
	listPM.add(PM3e3);
	
	
	
	return listPM;
}
//CAso con 5 programmi attesi e 6 rientrati
public static List<ProgrammaMusicaleDTO> creaProgrammiMusicaliE4() {
	
	
	List<ProgrammaMusicaleDTO> listPM=new ArrayList<ProgrammaMusicaleDTO>();
	List<UtilizzazioneDTO> listUtilizzazioniPM1= new ArrayList<>();
	List<UtilizzazioneDTO> listUtilizzazioniPM2= new ArrayList<>();
	List<UtilizzazioneDTO> listUtilizzazioniPM3= new ArrayList<>();
	List<UtilizzazioneDTO> listUtilizzazioniPM4= new ArrayList<>();
	List<UtilizzazioneDTO> listUtilizzazioniPM5= new ArrayList<>();
	List<UtilizzazioneDTO> listUtilizzazioniPM6= new ArrayList<>();
	
	/* Inizializzazione delle utilizzazioni da inserire nei rispettivi PM
	
	   Utilizzazioni per PM1 
	*/
	UtilizzazioneDTO u1=new UtilizzazioneDTO(1l,45l,25l);
	UtilizzazioneDTO u2=new UtilizzazioneDTO(5l,45l,30l);
	UtilizzazioneDTO u3=new UtilizzazioneDTO(5l,45l,40l);	
	listUtilizzazioniPM1.add(u1);
	listUtilizzazioniPM1.add(u2);
	listUtilizzazioniPM1.add(u3);
	
	// Utilizzazioni per PM2
	UtilizzazioneDTO u1PM2=new UtilizzazioneDTO(3l,45l,10l);
	UtilizzazioneDTO u2PM2=new UtilizzazioneDTO(5l,45l,60l);
	UtilizzazioneDTO u3PM2=new UtilizzazioneDTO(6l,45l,50l);
	listUtilizzazioniPM2.add(u1PM2);
	listUtilizzazioniPM2.add(u2PM2);
	listUtilizzazioniPM2.add(u3PM2);
	
	// Utilizzazioni per PM3 
	UtilizzazioneDTO u1PM3=new UtilizzazioneDTO(7l,45l,70l);
	UtilizzazioneDTO u2PM3=new UtilizzazioneDTO(8l,45l,130l);
	UtilizzazioneDTO u3PM3=new UtilizzazioneDTO(9l,45l,100l);
	listUtilizzazioniPM3.add(u1PM3);
	listUtilizzazioniPM3.add(u2PM3);
	listUtilizzazioniPM3.add(u3PM3);
	
	// Utilizzazioni per PM4 
	UtilizzazioneDTO u1PM4=new UtilizzazioneDTO(10l,46l,100l);
	UtilizzazioneDTO u2PM4=new UtilizzazioneDTO(11l,47l,50l);
	UtilizzazioneDTO u3PM4=new UtilizzazioneDTO(12l,48l,250l);
	listUtilizzazioniPM4.add(u1PM4);
	listUtilizzazioniPM4.add(u2PM4);
	listUtilizzazioniPM4.add(u3PM4);
	
	
	// Utilizzazioni per PM5 
	UtilizzazioneDTO u1PM5=new UtilizzazioneDTO(5l,45l,25l);
	UtilizzazioneDTO u2PM5=new UtilizzazioneDTO(5l,45l,20l);
	UtilizzazioneDTO u3PM5=new UtilizzazioneDTO(5l,45l,15l);
	listUtilizzazioniPM5.add(u1PM5);
	listUtilizzazioniPM5.add(u2PM5);
	listUtilizzazioniPM5.add(u3PM5);
	
	
	
	// Utilizzazioni per PM6
	UtilizzazioneDTO u1PM6=new UtilizzazioneDTO(5l,45l,25l);
	UtilizzazioneDTO u2PM6=new UtilizzazioneDTO();
	UtilizzazioneDTO u3PM6=new UtilizzazioneDTO();
	listUtilizzazioniPM6.add(u1PM6);
	listUtilizzazioniPM6.add(u2PM6);
	listUtilizzazioniPM6.add(u3PM6);
	
	
	
	// Creazione  dei Pm che si trovano all'interno di e1
	
	ProgrammaMusicaleDTO PM1e4=new ProgrammaMusicaleDTO(1l,0.0,0l,listUtilizzazioniPM1);
	ProgrammaMusicaleDTO PM2e4=new ProgrammaMusicaleDTO(2l,0.0,0l,listUtilizzazioniPM2);
	ProgrammaMusicaleDTO PM3e4=new ProgrammaMusicaleDTO(3l,0.0,0l,listUtilizzazioniPM3);
	ProgrammaMusicaleDTO PM4e4=new ProgrammaMusicaleDTO(4l,0.0,0l,listUtilizzazioniPM4);
	ProgrammaMusicaleDTO PM5e4=new ProgrammaMusicaleDTO(5l,0.0,0l,listUtilizzazioniPM5);
	ProgrammaMusicaleDTO PM6e4=new ProgrammaMusicaleDTO(6l,0.0,0l,listUtilizzazioniPM6);
	listPM.add(PM1e4);
	listPM.add(PM2e4);
	listPM.add(PM3e4);
	listPM.add(PM4e4);
	listPM.add(PM5e4);
	listPM.add(PM6e4);
			
	return listPM;
}

}
