package com.alkemytech.sophia.valorizzatore.service;

import com.alkemytech.sophia.valorizzatore.service.interfaces.ValorizzazioneService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class ValorizzazioneServiceImpl implements ValorizzazioneService {

	private final Provider<EntityManager> provider;

	@Inject
	public ValorizzazioneServiceImpl(Provider<EntityManager> provider) {
		this.provider = provider;
	}

	@Override
	public List<Object> findAll() {
		EntityManager em = null;
		em = provider.get();

		final Query q = em.createQuery("SELECT a FROM P a");

		return q.getResultList();
	}
}
