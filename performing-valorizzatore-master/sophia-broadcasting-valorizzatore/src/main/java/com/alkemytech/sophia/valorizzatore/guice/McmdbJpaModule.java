package com.alkemytech.sophia.valorizzatore.guice;

import com.alkemytech.sophia.common.config.ConfigurationLoader;
import com.alkemytech.sophia.common.s3.S3Service;
import com.alkemytech.sophia.valorizzatore.service.ValorizzazioneServiceImpl;
import com.alkemytech.sophia.valorizzatore.service.interfaces.ValorizzazioneService;
import com.alkemytech.sophia.valorizzatore.Valorizzatore;
import com.google.inject.PrivateModule;
import com.google.inject.Provider;
import com.google.inject.name.Names;
import com.google.inject.persist.PersistService;
import com.google.inject.persist.jpa.JpaPersistModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by Alessandro Russo on 01/12/2017.
 */
public class McmdbJpaModule extends PrivateModule {

    public static final String UNIT_NAME = "mcmdb";

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final String[] args;

    public McmdbJpaModule(String[] args) {
        super();
        this.args = args;
    }

    private Properties loadConfig() {
        final Properties properties = new ConfigurationLoader().withCommandLineArgs(args).load();
        Names.bindProperties(binder(), properties);
        bind(Properties.class).annotatedWith(Names.named("configuration")).toInstance(properties);
        final File file = new File(properties.getProperty("jpa." + UNIT_NAME + ".config"));
        logger.debug("loadConfig: configuration path [{}]", file.getAbsolutePath());
        try (final InputStream in = new FileInputStream(file)) {
            properties.load(in);
            logger.debug("loadConfig: properties {}", properties);
            Names.bindProperties(binder(), properties);
        }
        catch (IOException e) {
            logger.error("loadConfig", e);
        }
        return properties;
    }

    @Override
    protected void configure() {
        final JpaPersistModule jpaPersistModule = new JpaPersistModule(UNIT_NAME);
        Properties moduleProperties = loadConfig();
        jpaPersistModule.properties(moduleProperties);
        install(jpaPersistModule);
        final Provider<EntityManager> entityManagerProvider = binder().getProvider(EntityManager.class);

        //BIND
        bind(ValorizzazioneService.class).toInstance(new ValorizzazioneServiceImpl(entityManagerProvider));
     
        bind(S3Service.class).asEagerSingleton();
        bind(Valorizzatore.class).asEagerSingleton();

        //EXPOSE
        expose(ValorizzazioneService.class);
        expose(PersistService.class);
        expose(S3Service.class);
        expose(Valorizzatore.class);
    }
}
