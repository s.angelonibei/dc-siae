package com.alkemytech.sophia.valorizzatore.service;

import com.google.inject.Singleton;
import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.*;

/**
 * Created by Alessandro Russo on 05/12/2017.
 */
public class RulesEngineService {

    public KnowledgeBase createKnowledgeBaseFromSpreadsheet(String resourcePath) throws Exception {
        DecisionTableConfiguration dtconf = KnowledgeBuilderFactory.newDecisionTableConfiguration();
        dtconf.setInputType(DecisionTableInputType.XLS);
        KnowledgeBuilder knowledgeBuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
        knowledgeBuilder.add(org.drools.io.ResourceFactory.newFileResource(resourcePath), ResourceType.DTABLE, dtconf);
        if (knowledgeBuilder.hasErrors()) {
            throw new RuntimeException(knowledgeBuilder.getErrors().toString());
        }
        KnowledgeBase knowledgeBase = KnowledgeBaseFactory.newKnowledgeBase();
        knowledgeBase.addKnowledgePackages(knowledgeBuilder.getKnowledgePackages());
        return knowledgeBase;
    }
}
