package com.alkemytech.sophia.valorizzatore.parser;


import com.alkemytech.sophia.common.mapper.TemplateMapper;
import com.alkemytech.sophia.common.s3.S3Service;
import com.alkemytech.sophia.valorizzatore.service.RulesEngineService;
import com.amazonaws.util.StringUtils;
import org.drools.runtime.StatefulKnowledgeSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Alessandro Russo on 07/12/2017.
 */
public abstract class GenericParser {

    Logger logger = LoggerFactory.getLogger(getClass());

    public static final String PARSED_RECORDS = "records";
    public static final String ERRORS = "errors";

    protected S3Service s3Service;
    protected TemplateMapper mapper;
   // protected DecodeService decodeService;

    public abstract Map<String, Object> parse(Map<String, Object> parameters);

    protected List<Map<String,Object>> normalize(Integer idBroadcaster, String normalizedRule, List<Object> records) throws Exception {
        List<Map<String,Object> > normalizedRecords = new ArrayList<>();
        StatefulKnowledgeSession session = new RulesEngineService().createKnowledgeBaseFromSpreadsheet(normalizedRule).newStatefulKnowledgeSession();
       	/*for(Object record : records){
     
            Map<String, Object> castRecord = (Map<String, Object>) record;
            if( Utilities.isFilleMap(castRecord) ){
                logger.debug("- NORMALIZED RECORD -> {}", castRecord.toString());
                Map<String, Object> normalizedRecord = new HashMap<>();
                List<String> errors = new ArrayList<>();
                session.setGlobal(Constants.ID_BROADCASTER, idBroadcaster);
                session.setGlobal(Constants.GLOBAL_RECORD, normalizedRecord);
                session.setGlobal(Constants.GLOBAL_ERROR, errors);
                session.insert(castRecord);
                session.fireAllRules();
                logger.debug("- NORMALIZED RECORD -> {}", normalizedRecord.toString());
                normalizedRecords.add(normalizedRecord);
            }
        }*/
        return normalizedRecords;
    }

    public void setMapper(TemplateMapper mapper) {
        this.mapper = mapper;
    }

    public void setS3Service(S3Service s3Service) {
        this.s3Service = s3Service;
    }

 
    protected String getConfigProperty(String configProperty, String type){
        String[] configPropertyArray = getConfigList(configProperty);
        if(configPropertyArray != null){
            for(String currentValue: configPropertyArray){
                if(currentValue.contains(type)){
                    return currentValue;
                }
            }
        }
        return null;
    }

    private String[] getConfigList (String configProperty){
        if(!StringUtils.isNullOrEmpty( configProperty ) ){
            return configProperty.split("\\|");
        }
        return null;
    }
}
