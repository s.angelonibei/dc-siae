package com.alkemytech.sophia.valorizzatore.dto;

public class UtilizzazioneDTO {
	private Long idUtilizzazione;
	private Long idCombana;
	private Long durataSec;
	private Long punti;
	private String causaEsclusione;
	private Long puntiFinali;
	
	
	

	public UtilizzazioneDTO() {
		super();
	}

	public UtilizzazioneDTO(Long idUtilizzazione, Long idCombana, Long durataSec) {
		super();
		this.idUtilizzazione = idUtilizzazione;
		this.idCombana = idCombana;
		this.durataSec = durataSec;
	}

	public Long getIdUtilizzazione() {
		return idUtilizzazione;
	}

	public void setIdUtilizzazione(Long idUtilizzazione) {
		this.idUtilizzazione = idUtilizzazione;
	}

	public Long getIdCombana() {
		return idCombana;
	}

	public void setIdCombana(Long idCombana) {
		this.idCombana = idCombana;
	}

	public Long getDurataSec() {
		return durataSec;
	}

	public void setDurataSec(Long durataSec) {
		this.durataSec = durataSec;
	}

	public Long getPunti() {
		return punti;
	}

	public void setPunti(Long punti) {
		this.punti = punti;
	}

	public String getCausaEsclusione() {
		return causaEsclusione;
	}

	public void setCausaEsclusione(String causaEsclusione) {
		this.causaEsclusione = causaEsclusione;
	}

	public Long getPuntiFinali() {
		return puntiFinali;
	}

	public void setPuntiFinali(Long puntiFinali) {
		this.puntiFinali = puntiFinali;
	}

}
