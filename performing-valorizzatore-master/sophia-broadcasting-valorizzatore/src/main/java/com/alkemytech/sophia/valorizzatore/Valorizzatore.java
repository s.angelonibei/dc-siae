package com.alkemytech.sophia.valorizzatore;

import com.alkemytech.sophia.common.s3.S3Service;
import com.alkemytech.sophia.valorizzatore.dto.ConfigurationDTO;
import com.alkemytech.sophia.valorizzatore.dto.EsclusioneDTO;
import com.alkemytech.sophia.valorizzatore.dto.EventoDTO;
import com.alkemytech.sophia.valorizzatore.dto.ProgrammaMusicaleDTO;
import com.alkemytech.sophia.valorizzatore.dto.UtilizzazioneDTO;
import com.alkemytech.sophia.valorizzatore.guice.McmdbJpaModule;
import com.alkemytech.sophia.valorizzatore.service.interfaces.ValorizzazioneService;
import com.alkemytech.sophia.valorizzatore.utils.TestingCreaConfigurazioni;
import com.alkemytech.sophia.valorizzatore.utils.TestingCreaEventi;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.name.Named;
import com.google.inject.persist.PersistService;


import org.apache.http.util.TextUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Alessandro Russo on 01/12/2017.
 */
public class Valorizzatore {

	private static final Logger logger = LoggerFactory.getLogger(Valorizzatore.class);

	private final Integer listenPort;
	private ValorizzazioneService valorizzazioneService;

	@Inject
	public Valorizzatore(@Named("listenPort") Integer listenPort,
			ValorizzazioneService valorizzazioneService) {
		this.listenPort = listenPort;
		this.valorizzazioneService = valorizzazioneService;
	}

	public static void main(String[] args) throws Exception {
		try {
			final Injector injector = Guice.createInjector(new McmdbJpaModule(args));
			try {
				// Setup services
				injector.getInstance(PersistService.class).start();
				injector.getInstance(S3Service.class).startup();
				Valorizzatore valorizzatore = injector.getInstance(Valorizzatore.class);

				// Run business logic
				valorizzatore.process();
			} finally {
				// Stop services
				injector.getInstance(PersistService.class).stop();
				injector.getInstance(S3Service.class).shutdown();
			}
		} catch (Throwable e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}

	/**
	 * Process Original EVENTI da valorizzare
	 * 
	 * @throws Exception
	 */
	private void process() throws Exception {
		// Esecuzione Query per recupero degli Eventi(CaricoRiparto) da valorizzare
		// List<BdcInformationFileEntity> reportToProcessList =
		// informationFileEntityService.findByStato(Stato.DA_ELABORARE);
		List<EventoDTO> eventiDaValorizzare =TestingCreaEventi.creaEventi();  //new ArrayList<>();
		List<ConfigurationDTO> configurazioni =TestingCreaConfigurazioni.getConfiguration(); //new ArrayList<>();
		try (final ServerSocket socket = new ServerSocket(listenPort)) {
			logger.info("VALORIZZAZIONE TASK START: listening on {}", socket.getLocalSocketAddress());
			
			//CICLA GLI EVENTI DI UN PERIODO
			for (EventoDTO eventoDTO : eventiDaValorizzare) {
				
				// Recupero Configurazione per voceIncasso,Periodo,TipoPm Relativo all'evento
				ConfigurationDTO configurationDTO = ConfigurationDTO.getConfigurazione(configurazioni,
						eventoDTO.getVoceIncasso(), eventoDTO.getPeriodo(), eventoDTO.getTipologiaPM());
				
				Long puntiEvento = 0l;
				Long maxPuntiProgrammaMusicale = 0l;
				
				//CICLO PM DELL'EVENTO
				for (ProgrammaMusicaleDTO programmaMusicaleDTO : eventoDTO.getProgrammiMusicali()) {
					//Valorizzazione dei punti del programma musicale, valorizzandone i punti associati senza tener conto delle regole di esclusione.
					valorizzaPM(programmaMusicaleDTO, configurationDTO.getTipoAlgoritmo());

					//Valorizzazione dei punti del programma musicale, valorizzandone i punti associati senza tener conto delle regole di esclusione.
					Long puntiEsclusi = applicaEsclusioni(programmaMusicaleDTO, configurationDTO.getEsclusioni());
					
					//Calcolo dei punti finali delle utilizzazioni tenendo conto di tutti i punti esclusi dalle regole nella configurazione.
					ricalcoloPuntiUtilizzazioniFinali(programmaMusicaleDTO, puntiEsclusi);
					
					//calcolo i punti per l'evento
					puntiEvento = puntiEvento + programmaMusicaleDTO.getPuntiPm();
					
					//calcolo del PM con il massimo valore dei punti per le regole di numerosità dei PM ATTESI/RIENTRATI
					if (programmaMusicaleDTO.getPuntiPm() > maxPuntiProgrammaMusicale) {
						maxPuntiProgrammaMusicale = programmaMusicaleDTO.getPuntiPm();
					}
				}
				eventoDTO.setPuntiTotali(puntiEvento);
				
				calcoloSospesoPm(eventoDTO, maxPuntiProgrammaMusicale);
			}

		} catch (Exception e) {
			logger.info("-- VALORIZZAZIONE TASK ERROR --");
		}

		logger.info("-- VALORIZZAZIONE TASK END --");

	}

	private void calcoloSospesoPm(EventoDTO eventoDTO, Long maxPuntiProgrammaMusicale) {
		if (eventoDTO.getPmAttesi() > eventoDTO.getPmRientrati()) {
			eventoDTO.setPuntiPMNonRientrati(
					maxPuntiProgrammaMusicale * (eventoDTO.getPmAttesi() - eventoDTO.getPmRientrati()));
			
			eventoDTO.setPuntiTotali(eventoDTO.getPuntiTotali()+eventoDTO.getPuntiPMNonRientrati());
		}
		if (eventoDTO.getPmRientrati() == 0) {
			// SOSPESO
		}
		if (eventoDTO.getPmAttesi() < eventoDTO.getPmRientrati()) {
			eventoDTO.setPmAttesi(eventoDTO.getPmRientrati());
		}
	}

	private void ricalcoloPuntiUtilizzazioniFinali(ProgrammaMusicaleDTO programmaMusicaleDTO, Long puntiEsclusi) {
		// FORMULA CALCOLO puntiFinali = nPunti + nPuntiEsclusi/numeroUtilizzazioniRimanenti
		for (UtilizzazioneDTO utilizzazioneDTO : programmaMusicaleDTO.getUtilizzazioni()) {
			if (TextUtils.isBlank(utilizzazioneDTO.getCausaEsclusione())) {
				Long puntiFinali = utilizzazioneDTO.getPunti()
						+ (programmaMusicaleDTO.getPuntiEsclusi() / (programmaMusicaleDTO.getUtilizzazioni().size()
								- programmaMusicaleDTO.getnUtilizzazioniEscluse()));
				
				utilizzazioneDTO.setPuntiFinali(puntiFinali);
			}
		}

	}

	private void valorizzaPM(ProgrammaMusicaleDTO programmaMusicaleDTO, String tipoAlgoritmo) {
		Long puntiPm = 0l;
		for (UtilizzazioneDTO utilizzazioneDTO : programmaMusicaleDTO.getUtilizzazioni()) {
			if (ConfigurationDTO.PRO_QUOTA.equals(tipoAlgoritmo)) {
				puntiPm = puntiPm + 1;
				utilizzazioneDTO.setPunti(1l);
			} else if (ConfigurationDTO.PRO_DURATA.equals(tipoAlgoritmo)) {

				puntiPm = puntiPm + utilizzazioneDTO.getDurataSec();
				utilizzazioneDTO.setPunti(utilizzazioneDTO.getDurataSec());
			}
		}
		programmaMusicaleDTO.setPuntiPm(puntiPm);
	}

	private Long applicaEsclusioni(ProgrammaMusicaleDTO programmaMusicaleDTO, EsclusioneDTO esclusioni) {
		// TODO Auto-generated method stub
		Long puntiEsclusi = 0l;
		Long nEsclusi = 0l;

		HashMap<Long, UtilizzazioneDTO> utilizzazioniSingole = new HashMap<>();
		for (UtilizzazioneDTO utilizzazioneDTO : programmaMusicaleDTO.getUtilizzazioni()) {

			if (esclusioni.getEsclusioneDurata()!=null) {
				if (utilizzazioneDTO.getDurataSec() < esclusioni.getEsclusioneDurata()) {
					utilizzazioneDTO.setPuntiFinali(0l);
					if (TextUtils.isEmpty(utilizzazioneDTO.getCausaEsclusione())) {
						puntiEsclusi = puntiEsclusi + utilizzazioneDTO.getPunti();
						nEsclusi = +1l;
					}
					utilizzazioneDTO.setCausaEsclusione(ConfigurationDTO.ESC_DURATA);
				}
			}

			if (esclusioni.isEsclusioneBis()) {
				if (!utilizzazioniSingole.containsKey(utilizzazioneDTO.getIdCombana())) {
					utilizzazioniSingole.put(utilizzazioneDTO.getIdCombana(), utilizzazioneDTO);
				} else {
					UtilizzazioneDTO utilizzazioneEsclusa = utilizzazioneDTO;
					utilizzazioneEsclusa.setPuntiFinali(0l);
					if (TextUtils.isEmpty(utilizzazioneEsclusa.getCausaEsclusione())) {
						puntiEsclusi = puntiEsclusi + utilizzazioneEsclusa.getPunti();
						nEsclusi = +1l;
					}
					utilizzazioneEsclusa.setCausaEsclusione(ConfigurationDTO.ESC_BIS);
				}
			}

		}
		programmaMusicaleDTO.setnUtilizzazioniEscluse(nEsclusi);
		programmaMusicaleDTO.setPuntiEsclusi(puntiEsclusi);
		return puntiEsclusi;
	}

}