package com.alkemytech.sophia.valorizzatore.dto;

public class EsclusioneDTO {
	private boolean esclusioneBis;
	private Integer esclusioneDurata;
	private boolean esclusioneCedola;
	private boolean esclusionePubblicoDominio;

	public EsclusioneDTO() {
		super();
	}

	public EsclusioneDTO(boolean esclusioneBis, Integer esclusioneDurata, boolean esclusioneCedola,
			boolean esclusionePubblicoDominio) {
		super();
		this.esclusioneBis = esclusioneBis;
		this.esclusioneDurata = esclusioneDurata;
		this.esclusioneCedola = esclusioneCedola;
		this.esclusionePubblicoDominio = esclusionePubblicoDominio;
	}

	public EsclusioneDTO(boolean esclusioneBis, Integer esclusioneDurata) {
		super();
		this.esclusioneBis = esclusioneBis;
		this.esclusioneDurata = esclusioneDurata;
	}

	public boolean isEsclusioneBis() {
		return esclusioneBis;
	}

	public void setEsclusioneBis(boolean esclusioneBis) {
		this.esclusioneBis = esclusioneBis;
	}

	public Integer getEsclusioneDurata() {
		return esclusioneDurata;
	}

	public void setEsclusioneDurata(Integer esclusioneDurata) {
		this.esclusioneDurata = esclusioneDurata;
	}

	public boolean isEsclusioneCedola() {
		return esclusioneCedola;
	}

	public void setEsclusioneCedola(boolean esclusioneCedola) {
		this.esclusioneCedola = esclusioneCedola;
	}

	public boolean isEsclusionePubblicoDominio() {
		return esclusionePubblicoDominio;
	}

	public void setEsclusionePubblicoDominio(boolean esclusionePubblicoDominio) {
		this.esclusionePubblicoDominio = esclusionePubblicoDominio;
	}

}
