package com.alkemytech.sophia.invoice.integration.model

/**
 * Created by stefanolizzi on 09/05/17.
 */
class DatiStampa {

    String riferimentiCodicePrag
    String riferimentiDataFatturaRiferimento
    String riferimentiDescrizioneAgenzia
    String riferimentiDescrizioneSede
    String riferimentiFatturariferimento
    String associazioneAccordoSmad
    String associazionePeriodoAl
    String associazionePeriodoDal
    String clienteCittaDelegatoRimborso
    String clienteDelegatoRimborso
    String clienteDocumentoDelegato
    String clienteOpzioneLegge398
    String localeDenominazione
    String annotazioni
    String annotazioniStrumenti
    String localeCap
    String localeCategoriaLocale
    String localeCodiceBA
    String localeCodiceComune
    String localeCodiceSpei
    String localeGenereLocale
    String localeIndirizzo
    String localeLocalita
    String localeTitolare
    String manifestazioneAutore
    String manifestazioneCodOperaTeatrale
    String manifestazioneCodiceGenereManifestazione
    String manifestazioneDataFineManifestazione
    String manifestazioneDataInizioManifestazione
    String manifestazioneDataPermesso
    String manifestazioneGenereManifestazione
    String manifestazioneNumPermesso
    String manifestazioneNumeroEventi
    String manifestazioneNumeroGiornate
    String manifestazioneTitoloLavoro
    String riepilogoIncassoNetto
    String riepilogoNumeroFinaleDistinta
    String riepilogoNumeroInizialeDistinta
    String riepilogoNumeroFinaleDichiarazione
    String riepilogoNumeroInizialeDichiarazione
    String riepilogoTotaleDistinte
    String riepilogoTotaleDichiarazioni
    String riepilogoPresenze
    String riepilogoBigliettiEsitati
    String riferimentiNumeroReversale

    String getRiferimentiCodicePrag() {
        return Configuration.getValue(riferimentiCodicePrag)
    }

    String getRiferimentiDataFatturaRiferimento() {
        return Configuration.getValue(riferimentiDataFatturaRiferimento)
    }

    String getRiferimentiDescrizioneAgenzia() {
        return Configuration.getValue(riferimentiDescrizioneAgenzia)
    }

    String getRiferimentiDescrizioneSede() {
        return Configuration.getValue(riferimentiDescrizioneSede)
    }

    String getRiferimentiFatturariferimento() {
        return Configuration.getValue(riferimentiFatturariferimento)
    }

    String getAssociazioneAccordoSmad() {
        return Configuration.getValue(associazioneAccordoSmad)
    }

    String getAssociazionePeriodoAl() {
        return Configuration.getValue(associazionePeriodoAl)
    }

    String getAssociazionePeriodoDal() {
        return Configuration.getValue(associazionePeriodoDal)
    }

    String getClienteCittaDelegatoRimborso() {
        return Configuration.getValue(clienteCittaDelegatoRimborso)
    }

    String getClienteDelegatoRimborso() {
        return Configuration.getValue(clienteDelegatoRimborso)
    }

    String getClienteDocumentoDelegato() {
        return Configuration.getValue(clienteDocumentoDelegato)
    }

    String getClienteOpzioneLegge398() {
        return Configuration.getValue(clienteOpzioneLegge398)
    }

    String getAnnotazioni() {
        return Configuration.getValue(annotazioni)
    }

    String getLocaleDenominazione() {
        return Configuration.getValue(localeDenominazione)
    }

    String getAnnotazioniStrumenti() {
        return Configuration.getValue(annotazioniStrumenti)
    }

    String getLocaleCap() {
        return Configuration.getValue(localeCap)
    }

    String getLocaleCategoriaLocale() {
        return Configuration.getValue(localeCategoriaLocale)
    }

    String getLocaleCodiceBA() {
        return Configuration.getValue(localeCodiceBA)
    }

    String getLocaleCodiceComune() {
        return Configuration.getValue(localeCodiceComune)
    }

    String getLocaleCodiceSpei() {
        return Configuration.getValue(localeCodiceSpei)
    }

    String getLocaleGenereLocale() {
        return Configuration.getValue(localeGenereLocale)
    }

    String getLocaleIndirizzo() {
        return Configuration.getValue(localeIndirizzo)
    }

    String getLocaleLocalita() {
        return Configuration.getValue(localeLocalita)
    }

    String getLocaleTitolare() {
        return Configuration.getValue(localeTitolare)
    }

    String getManifestazioneAutore() {
        return Configuration.getValue(manifestazioneAutore)
    }

    String getManifestazioneCodOperaTeatrale() {
        return Configuration.getValue(manifestazioneCodOperaTeatrale)
    }

    String getManifestazioneCodiceGenereManifestazione() {
        return Configuration.getValue(manifestazioneCodiceGenereManifestazione)
    }

    String getManifestazioneDataFineManifestazione() {
        return Configuration.getValue(manifestazioneDataFineManifestazione)
    }

    String getManifestazioneDataInizioManifestazione() {
        return Configuration.getValue(manifestazioneDataInizioManifestazione)
    }

    String getManifestazioneDataPermesso() {
        return Configuration.getValue(manifestazioneDataPermesso)
    }

    String getManifestazioneGenereManifestazione() {
        return Configuration.getValue(manifestazioneGenereManifestazione)
    }

    String getManifestazioneNumPermesso() {
        return Configuration.getValue(manifestazioneNumPermesso)
    }

    String getManifestazioneNumeroEventi() {
        return Configuration.getValue(manifestazioneNumeroEventi)
    }

    String getManifestazioneNumeroGiornate() {
        return Configuration.getValue(manifestazioneNumeroGiornate)
    }

    String getManifestazioneTitoloLavoro() {
        return Configuration.getValue(manifestazioneTitoloLavoro)
    }

    String getRiepilogoIncassoNetto() {
        return Configuration.getValue(riepilogoIncassoNetto)
    }

    String getRiepilogoNumeroFinaleDistinta() {
        return Configuration.getValue(riepilogoNumeroFinaleDistinta)
    }

    String getRiepilogoNumeroInizialeDistinta() {
        return Configuration.getValue(riepilogoNumeroInizialeDistinta)
    }

    String getRiepilogoNumeroFinaleDichiarazione() {
        return Configuration.getValue(riepilogoNumeroFinaleDichiarazione)
    }

    String getRiepilogoNumeroInizialeDichiarazione() {
        return Configuration.getValue(riepilogoNumeroInizialeDichiarazione)
    }

    String getRiepilogoTotaleDistinte() {
        return Configuration.getValue(riepilogoTotaleDistinte)
    }

    String getRiepilogoTotaleDichiarazioni() {
        return Configuration.getValue(riepilogoTotaleDichiarazioni)
    }

    String getRiepilogoPresenze() {
        return Configuration.getValue(riepilogoPresenze)
    }

    String getRiepilogoBigliettiEsitati() {
        return Configuration.getValue(riepilogoBigliettiEsitati)
    }

    String getRiferimentiNumeroReversale() {
        return Configuration.getValue(riferimentiNumeroReversale)
    }
}
