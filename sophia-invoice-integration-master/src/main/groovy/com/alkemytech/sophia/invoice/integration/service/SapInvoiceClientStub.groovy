package com.alkemytech.sophia.invoice.integration.service

import com.alkemytech.sophia.invoice.integration.model.Configuration
import com.alkemytech.sophia.invoice.integration.model.InvoiceOrder
import com.alkemytech.sophia.invoice.integration.model.InvoiceResponse

/**
 * Created by Mirko on 27/06/2017.
*/

class SapPiInvoiceClientStub implements InvoiceServiceClientInterface {

    Configuration configuration
    String template
    String templatePdf
    Tracer tracer
    Random random



    @Override
    void init(Configuration configuration, Tracer tracer) {
        this.configuration = configuration
        template = getTemplatefile(configuration.jsonRequestTemplate)
        templatePdf = getTemplatefile(configuration.jsonRequestTemplatePdf)
        this.tracer = tracer
        this.random = new Random()
    }


    @Override
    InvoiceResponse sendInvoiceOrder(InvoiceOrder invoiceOrder, String requestId, Integer invoiceId) {
        InvoiceResponse invoiceResponse = new InvoiceResponse()
        invoiceResponse.setPathFilePdf("/path/to/file.pdf");
        invoiceResponse.setNumeroDocumento(random.nextInt(10000).toString());
        return invoiceResponse
    }



    @Override
    List<Byte> getInvoice(String pdfPath, String requestId, Integer invoiceId) {
        InputStream inputStream= getClass().getResourceAsStream('/fattura.pdf')
        return inputStream.bytes
    }

    private String getTemplatefile(String name) {
        InputStream inputStream= getClass().getResourceAsStream(name)
        return inputStream.text
    }

}
