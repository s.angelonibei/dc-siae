package com.alkemytech.sophia.invoice.integration.service

import com.alkemytech.sophia.invoice.integration.model.Configuration
import com.alkemytech.sophia.invoice.integration.model.DatiFattura
import com.alkemytech.sophia.invoice.integration.model.DatiStampa
import com.alkemytech.sophia.invoice.integration.model.Header
import com.alkemytech.sophia.invoice.integration.model.InvoiceOrder
import com.alkemytech.sophia.invoice.integration.model.InvoiceResponse
import com.alkemytech.sophia.invoice.integration.model.Posizione
import com.alkemytech.sophia.invoice.integration.utils.TracerStub
import groovy.json.JsonOutput
import groovy.text.SimpleTemplateEngine
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import org.apache.http.conn.scheme.Scheme
import org.apache.http.conn.ssl.SSLSocketFactory

import java.security.KeyStore
import java.text.SimpleDateFormat

import static groovyx.net.http.ContentType.*
import static groovyx.net.http.Method.*


/**
 * Created by stefanolizzi on 08/05/17.
 */
class SapPiInvoiceServiceClientImpl implements InvoiceServiceClientInterface {

    Configuration configuration
    String template
    String templatePdf
    Tracer tracer
    def keyStore
    SimpleTemplateEngine engine = new groovy.text.SimpleTemplateEngine()


    @Override
    void init(Configuration configuration, Tracer tracer) {
        this.configuration = configuration
        template = getTemplatefile(configuration.jsonRequestTemplate)
        templatePdf = getTemplatefile(configuration.jsonRequestTemplatePdf)
        this.tracer = tracer

        if (configuration.https) {
            keyStore = KeyStore.getInstance(KeyStore.defaultType)
           new File(configuration.certificatePath).withInputStream {
                keyStore.load(it, configuration.trustStorePwd.toCharArray())
            }
        }

    }


    @Override
    InvoiceResponse sendInvoiceOrder(InvoiceOrder invoiceOrder, String requestId, Integer invoiceId) {
        def binding = [:]
        binding.header = new Header();
        binding.datiFattura = invoiceOrder.datiFattura
        binding.datiStampa = invoiceOrder.datiStampa
        def message = engine.createTemplate(template).make(binding)
        def http = new HTTPBuilder(configuration.esbPath + '/createBillingDocument')
        if (configuration.https) {
            http.ignoreSSLIssues()
//            http.client.connectionManager.schemeRegistry.register(
//                    new Scheme("https", 443, new SSLSocketFactory(keyStore)))
        }

        InvoiceResponse invoiceResponse
        traceMessage(message.toString(), requestId, invoiceId)
        try {
            http.request(POST, JSON) { req ->
                body = message.toString()
                response.success = { resp, json ->
                    invoiceResponse = initInvoiceResponse(json)
                    traceMessage(JsonOutput.toJson(json), requestId, invoiceId)

                }
                response.failure = {
                    resp ->
                        traceMessage(resp.toString(), requestId, invoiceId)
                        throw new IntegrationException(IntegrationErrorCode.APPLICATION_ERROR, resp.responseBase.toString())

                }
            }

        } catch (Exception e) {
            String errorMessage = e.getMessage()
            if (!errorMessage && e.getCause()) {
                errorMessage = e.getCause().getMessage()
            }
            traceMessage("error message: " + errorMessage + " --- stackTrace: " + e.getStackTrace().toString(), requestId, invoiceId)

        }
        return invoiceResponse
    }


    private traceMessage(String message, String uuid, Integer invoiceId) {
        try {
            tracer.trace(message, uuid, invoiceId)
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    List<Byte> getInvoice(String pdfPath, String requestId, Integer invoiceId) {
        def binding = ['pdfPath': pdfPath, 'requestId': requestId]
        def message = engine.createTemplate(templatePdf).make(binding)
        def http = new HTTPBuilder(configuration.esbPath + '/downloadFile')
        if (configuration.https) {
            http.ignoreSSLIssues()
//            http.client.connectionManager.schemeRegistry.register(
//                    new Scheme("https", 443, new SSLSocketFactory(keyStore)))
        }
        List<Byte> invoiceResponse

        def uuid = UUID.randomUUID() as String
        traceMessage(message.toString(), requestId, invoiceId)
        try {
            http.request(POST, JSON) { req ->
                body = message.toString()
                response.success = { resp, json ->
                    invoiceResponse = initInvoicePdfResponse(json)
                    traceMessage(JsonOutput.toJson(json), requestId, invoiceId)

                }
                response.failure = { resp ->
                    traceMessage(resp.toString(), requestId, invoiceId)
                    throw new IntegrationException(IntegrationErrorCode.APPLICATION_ERROR, resp.toString())
                }
            }
        }
        catch (Exception e) {
            def cause
            if (e.getCause() != null && e.getCause().getMessage() != null) {
                cause = e.getCause().getMessage()
            }
            traceMessage(cause + " - " + e.getStackTrace().toString(), requestId, invoiceId)

        }

        return invoiceResponse
    }

    private void manageKo(Map jsonResp) {
        def errorMessage = jsonResp.errorMessage
        def errorCode = jsonResp.errorCode
        String message = "{\"errorCode\":\"$errorCode\" ,\"errorMessage\": \"$errorMessage\"}"
        throw new IntegrationException(IntegrationErrorCode.DATA_ERROR, message)

    }


    private InvoiceResponse initInvoiceResponse(Map jsonResp) {
        if (jsonResp.KO) {
            manageKo(jsonResp)
        }
        return new InvoiceResponse(jsonResp.content)
    }

    private List<Byte> initInvoicePdfResponse(Map jsonResp) {
        if (jsonResp.KO) {
            manageKo(jsonResp)
        }
        ByteArrayOutputStream invoice = new ByteArrayOutputStream()
        invoice.write(jsonResp.content.blob.decodeBase64())
        return invoice.toByteArray()

    }


    private String getTemplatefile(String name) {
//        def url = getClass().getResource(name)
//        def file = new File(url.toURI())
//        return file.text
//
        InputStream inputStream = getClass().getResourceAsStream(name)
        return inputStream.text
    }


    public static void main(String[] params) {

        SapPiInvoiceServiceClientImpl sapPiInvoiceServiceClient = new SapPiInvoiceServiceClientImpl()
        Configuration conf = new Configuration(['esbPath': 'https://muletst.ws.siae.it:443/bpmServices/mule',
        'jsonRequestTemplate': '/template.json', 'jsonRequestTemplatePdf': '/templatePdf.json', 'https': true,
        'certificatePath': 'C:\\Users\\Mirko\\git\\sophia-invoice-integration\\src\\main\\resources\\truststore.ks', 'trustStorePwd': 'test1234'])
//        Configuration conf = new Configuration(['esbPath'            : 'http://localhost:80/bpmServices/mule',
//                                                'jsonRequestTemplate': '/template.json', 'jsonRequestTemplatePdf': '/templatePdf.json', 'https': false])
        TracerStub tracerStub = new TracerStub()
        sapPiInvoiceServiceClient.init(conf, tracerStub)

        Random rand = new Random()

        SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
        String requestId = rand.nextInt(100000).toString()
        Date date = new Date();
        DatiFattura datiFattura = new DatiFattura();
        datiFattura.idRichiesta = requestId
        datiFattura.setCodiceCliente('1000189001');
        datiFattura.setDataCompetenza(DATE_FORMAT.format(date));
        datiFattura.setDataDocumento(DATE_FORMAT.format(date));
        datiFattura.setDataAccredito(DATE_FORMAT.format(date));
        datiFattura.setDataPagamento(DATE_FORMAT.format(date))
        datiFattura.divisione = 'DGEN'
        datiFattura.tipoDocumento = '3'
       // datiFattura.attribuzione = 'MULCL n. ord. d\'acquisto'
        datiFattura.setIncoterms2('C001')
        datiFattura.setTestoTestata('Licenza 3241/I/2934')

        List<Posizione> positions = new ArrayList<>();
        Posizione position = new Posizione();
        position.setCodiceMateriale('ID52B');
        position.setCodiceIVASAP('DE258657906');
        position.setNumeroPosizione('1');
        position.setQuantita('1');
        position.setPrezzoUnitario('45000')
        position.setDescrizioneMateriale('STREAMING E DOWNL. A PAGAM. DOR');
        positions.add(position);



        datiFattura.setPosizioni(positions);

        InvoiceOrder order = new InvoiceOrder()
        order.setDatiFattura(datiFattura)
        DatiStampa datiStampa = new DatiStampa()


        order.setDatiStampa(datiStampa)
        Integer invoiceId = 1001
        InvoiceResponse response = sapPiInvoiceServiceClient.sendInvoiceOrder(order, requestId, invoiceId)
        def pdfName = response.pathFilePdf.substring(response.pathFilePdf.lastIndexOf("\\") + 1 , response.pathFilePdf.length())

        byte[] pdf=   sapPiInvoiceServiceClient.getInvoice(pdfName,'123', 12)

        FileOutputStream fos = new FileOutputStream("C:\\Users\\Mirko\\Desktop\\pdffattureprova\\fattTest.pdf");
        fos.write(pdf)
        fos.close()

        print "------------- end invoice order ------------";


    }
}