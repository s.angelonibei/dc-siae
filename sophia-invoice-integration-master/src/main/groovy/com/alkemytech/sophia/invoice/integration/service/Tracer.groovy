package com.alkemytech.sophia.invoice.integration.service

interface Tracer {

    public void trace(String message, String uuid, Integer invoiceId);
}
