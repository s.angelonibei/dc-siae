package com.alkemytech.sophia.invoice.integration.utils

import com.alkemytech.sophia.invoice.integration.service.Tracer
import groovy.json.JsonOutput

import java.text.SimpleDateFormat

public class TracerStub implements Tracer{
    Map<String, List<Object>> fakeRepository

    public TracerStub(){
        this.fakeRepository = new HashMap<>()

    }
    @Override
    public void trace(String message, String uuid, Integer invoiceId){
        if(!fakeRepository.containsKey(uuid)){
            List<String> params = new ArrayList<>()
            params.add(uuid)
            params.add(message)
            params.add(new Date())
            fakeRepository.put(uuid,params)
            println 'TRACER STUB:::::: REQUEST' + JsonOutput.prettyPrint(JsonOutput.toJson(fakeRepository))
        }else{

            Date startDate  = (Date)fakeRepository.get(uuid).get(2);
            Date now = new Date()
            long millis = now.getTime() - startDate.getTime()
            fakeRepository.get(uuid).add(message)
            fakeRepository.get(uuid).add(millis.toString())
            println 'TRACER STUB:::::: RESPONSE' + JsonOutput.prettyPrint(JsonOutput.toJson(fakeRepository))
        }
    }
}