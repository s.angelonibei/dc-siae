package com.alkemytech.sophia.invoice.integration.model

/**
 * Created by stefanolizzi on 09/05/17.
 */
class Riferimenti {


    String riferimentoNumeroLinea = ""
    String idDocumento = ""
    String data = ""
    String numItem = ""
    String tipo = ""
    String codiceCommessaConvenzione = ""
    String codiceCUP = ""
    String codiceCIG = ""

    String getRiferimentoNumeroLinea() {
        return Configuration.getValue(riferimentoNumeroLinea)
    }

    String getIdDocumento() {
        return Configuration.getValue(idDocumento)
    }

    String getData() {
        return Configuration.getValue(data)
    }

    String getNumItem() {
        return Configuration.getValue(numItem)
    }

    String getTipo() {
        return Configuration.getValue(tipo)
    }

    String getCodiceCommessaConvenzione() {
        return Configuration.getValue(codiceCommessaConvenzione)
    }

    String getCodiceCUP() {
        return Configuration.getValue(codiceCUP)
    }

    String getCodiceCIG() {
        return Configuration.getValue(codiceCIG)
    }
}
