package com.alkemytech.sophia.invoice.integration.model

import javax.xml.datatype.DatatypeFactory
import javax.xml.datatype.XMLGregorianCalendar
import java.sql.Timestamp
import java.text.SimpleDateFormat

/**
 * Created by stefanolizzi on 09/05/17.
 */
class Header {
    String uuid
    String riferimento = null
    String timeStamp

    Header(){
        uuid = ("\""+ java.util.UUID.randomUUID()+"\"").replaceAll("-", "")
        timeStamp =  new SimpleDateFormat(("yyyy-MM-dd'T'HH:mm:ssXXX")).format(new Date())
    }
    String getRiferimento() {
        Configuration.getValue(riferimento)
    }

    String getTimeStamp(){
        Configuration.getValue(timeStamp)
    }

    static void main(String[] args){
        String generatedUUID = new Header().uuid
        println generatedUUID
        println "SIZE: " + generatedUUID.size()

        println generatedUUID.replaceAll("-", "")
        println generatedUUID.replaceAll("-", "").size()

        println generatedUUID.replaceAll("-", "").replaceAll("\"","")
        println generatedUUID.replaceAll("-", "").replaceAll("\"","").size()

        String dateString = new SimpleDateFormat(("yyyy-MM-dd'T'HH:mm:ssXXX")).format(new Date())
        System.out.println(dateString );



    }
}
