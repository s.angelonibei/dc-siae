package com.alkemytech.sophia.invoice.integration.service

/**
 * Created by stefanolizzi on 27/02/17.
 */
enum IntegrationErrorCode {
    NO_ERROR('0'), APPLICATION_ERROR('1'),DATA_ERROR('3')

    IntegrationErrorCode(String value) {
        this.value = value
    }
    private final String value
    String getValue() {
        value
    }
}