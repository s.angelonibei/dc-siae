package com.alkemytech.sophia.invoice.integration.service

/**
 * Created by stefanolizzi on 29/05/17.
 */



class IntegrationException extends Exception {
    IntegrationErrorCode errorCode;
    IntegrationException(IntegrationErrorCode errorCode, String message){
        super(message);
        this.errorCode=errorCode
    }

    IntegrationException(IntegrationErrorCode errorCode, String message, Throwable cause){
        super(message,cause);
        this.errorCode=errorCode
    }

}
