package com.alkemytech.sophia.invoice.integration.service

import com.alkemytech.sophia.invoice.integration.model.Configuration
import com.alkemytech.sophia.invoice.integration.model.InvoiceOrder
import com.alkemytech.sophia.invoice.integration.model.InvoiceResponse

interface InvoiceServiceClientInterface {



    void init(Configuration configuration, Tracer tracer)

    InvoiceResponse sendInvoiceOrder(InvoiceOrder invoiceOrder, String requestId, Integer invoiceId)

    List<Byte> getInvoice(String pdfPath, String requestId,Integer invoiceId)



}