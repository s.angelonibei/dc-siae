package com.alkemytech.sophia.invoice.integration.model

/**
 * Created by stefanolizzi on 09/05/17.
 */
class Posizione {
    def codiceIVASAP
    def codiceMateriale
    def descrizioneMateriale
    def imponibilePosizione
    def importoImposte
    def numeroPosizione
    def prezzoUnitario
    def quantita
    def totalePosizione



    def getNumeroPosizione() {
       Configuration.getValue(numeroPosizione)
    }

    def getCodiceIVASAP() {
        Configuration.getValue(codiceIVASAP)
    }

    def getCodiceMateriale() {
        Configuration.getValue(codiceMateriale)

    }

    def getDescrizioneMateriale() {
        Configuration.getValue(descrizioneMateriale)
    }

    def getImponibilePosizione() {
        Configuration.getValue(imponibilePosizione)
    }

    def getImportoImposte() {
        Configuration.getValue(importoImposte)
    }

    def getPrezzoUnitario() {
        Configuration.getValue(prezzoUnitario)
    }

    def getQuantita() {
        Configuration.getValue(quantita)
    }

    def getTotalePosizione() {
        Configuration.getValue(totalePosizione)
    }
}

