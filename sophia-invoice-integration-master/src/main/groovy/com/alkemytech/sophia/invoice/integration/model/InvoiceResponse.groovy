package com.alkemytech.sophia.invoice.integration.model


/**
 * Created by stefanolizzi on 09/05/17.
 */
class InvoiceResponse {

    String attribuzione
    String dataFattura
    String idRichiesta
    String importoBollo
    String importoImposte
    String numeroDocumento
    String pathFilePdf
    List<Posizione> posizioni
    String regimaIvaApplicato
    String tipoBollo
    String tipoDocumento
    String totaleDaPagare
    String totaleDocumento
    String descrizioneTipoDocumento
    String languageIsoTipoDocumento

    void setPosizioni(Map<String, List<Posizione>> posizioni) {
        this.posizioni = posizioni.get("posizione").collect {m ->
             new Posizione(m)
         }
    }
}
