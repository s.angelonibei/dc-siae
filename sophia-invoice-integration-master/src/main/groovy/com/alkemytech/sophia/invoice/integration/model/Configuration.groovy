package com.alkemytech.sophia.invoice.integration.model

/**
 * Created by stefanolizzi on 09/05/17.
 */
class Configuration {
    String esbPath
    String jsonRequestTemplate ='/template.json'
    String jsonRequestTemplatePdf = '/templatePdf.json'
    Boolean https = false
    String certificatePath
    String trustStorePwd

    static String DA_DEFINIRE = "Da definire"

    static String getValue(String value){
        if(value){
            return "\"$value\""
        }
        return "\"\""
    }
}
