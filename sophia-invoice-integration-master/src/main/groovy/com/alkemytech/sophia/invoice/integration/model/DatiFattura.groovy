package com.alkemytech.sophia.invoice.integration.model

/**
 * Created by stefanolizzi on 09/05/17.
 */
class DatiFattura {

    String codiceOperazione = "FATT04"
    String codiceCliente
    String codiceUfficio
    String dataDocumento
    String dataCompetenza
    String dataInizioValidita
    String dataFineValidita
    String dataAccredito
    String dataPagamento
    String metodoPagamento
    String tipoDocumento
    String tipoMovimento
    String seprag
    String incoterms2
    String totaleDocumento
    String idRichiesta
    String idDocumentoRiferimento
    String divisione
    String rimborsoIvaScaduto
    String attribuzione
    String testoTestata
    Riferimenti riferimenti = new Riferimenti()

    String modalitaDiRimborso

    String tipoBancaPartner

    List<Posizione> posizioni = [new Posizione(),new Posizione()]

    String getCodiceOperazione() {
        Configuration.getValue(codiceOperazione)
    }

    String getCodiceCliente() {
        Configuration.getValue(codiceCliente)
    }

    String getCodiceUfficio() {
        Configuration.getValue(codiceUfficio)
    }


    String getDataDocumento() {
        Configuration.getValue(dataDocumento)
    }

    String getDataCompetenza() {
        Configuration.getValue(dataCompetenza)
    }

    String getDataInizioValidita() {
        Configuration.getValue(dataInizioValidita)
    }

    String getDataFineValidita() {
        Configuration.getValue(dataFineValidita)
    }

    String getDataPagamento() {
        Configuration.getValue(dataPagamento)
    }

    String getDataAccredito() {
        Configuration.getValue(dataAccredito)
    }

    String getMetodoPagamento() {
        Configuration.getValue(metodoPagamento)
    }

    String getTipoDocumento() {
        Configuration.getValue(tipoDocumento)
    }

    String getTipoMovimento() {
        Configuration.getValue(tipoMovimento)
    }

    String getSeprag() {
        Configuration.getValue(seprag)
    }

    String getIncoterms2() {
        Configuration.getValue(incoterms2)
    }

    String getTotaleDocumento() {
        Configuration.getValue(totaleDocumento)
    }

    String getIdRichiesta() {
        Configuration.getValue(idRichiesta)
    }

    String getIdDocumentoRiferimento() {
        Configuration.getValue(idDocumentoRiferimento)
    }

    String getDivisione() {
        Configuration.getValue(divisione)
    }

    String getRimborsoIvaScaduto() {
        Configuration.getValue(rimborsoIvaScaduto)
    }

    String getAttribuzione() {
        Configuration.getValue(attribuzione)
    }

    String getTestoTestata() {
        Configuration.getValue(testoTestata)
    }



    String getModalitaDiRimborso() {
        Configuration.getValue(modalitaDiRimborso)
    }

    String getTipoBancaPartner() {
        Configuration.getValue(tipoBancaPartner)
    }

    public static void main(String[] args){
        DatiFattura df = new DatiFattura()
        print 'value' +  Configuration.getValue(df.getDataAccredito()) +  "end"
    }

}


