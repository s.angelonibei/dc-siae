INSERT INTO `role_permission` (`role`, `permission`) VALUES ('DEVELOPMENT', 'brPreallocazioneIncasso');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('IT', 'brPreallocazioneIncasso');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('BU-MUSICA', 'brPreallocazioneIncasso');

INSERT INTO `role_permission` (`role`, `permission`) VALUES ('GA', 'guMultimedialeLocale');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('GA', 'muLocale');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('GA', 'muLocaleRegolare');

INSERT INTO `role_permission` (`role`, `permission`) VALUES ('DEVELOPMENT', 'muLocaleRegolare');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('IT', 'muLocaleRegolare');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('MU-LOCALE', 'muLocaleRegolare');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('BU-MULTIMEDIALE', 'muLocaleRegolare');



