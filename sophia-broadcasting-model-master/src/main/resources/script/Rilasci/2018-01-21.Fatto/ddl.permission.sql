CREATE TABLE `permission` (
  `code` varchar(30) NOT NULL,
  `parent` varchar(30) DEFAULT NULL,
  `url` varchar(50) DEFAULT NULL,
  `name` varchar(45) NOT NULL,
  `menuVoice` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`code`),
  KEY `FK_PERMISSION_PARENT_idx` (`parent`),
  CONSTRAINT `FK_PERMISSION_PARENT` FOREIGN KEY (`parent`) REFERENCES `permission` (`code`) ON DELETE NO ACTION ON UPDATE NO ACTION
)