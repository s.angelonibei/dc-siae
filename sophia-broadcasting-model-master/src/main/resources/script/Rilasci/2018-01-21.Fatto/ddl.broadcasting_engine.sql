-- BDC_BROADCASTER_CONFIG
DROP TABLE IF EXISTS BDC_BROADCASTER_CONFIG;
CREATE TABLE BDC_BROADCASTER_CONFIG
(
  ID_BROADCASTER_CONFIG   BIGINT(20)                  NOT NULL    AUTO_INCREMENT,
  ID_BROADCASTER          INT(11)                     NOT NULL,
  ID_CHANNEL              INT(11),
  VALID_FROM              DATETIME    DEFAULT NOW()   NOT NULL ,
  VALID_TO                DATETIME,
  CREATION_DATE           DATETIME,
  CREATION_USER           VARCHAR(50),
  MODIFY_DATE             DATETIME,
  MODIFY_USER             VARCHAR(50),
  CONFIGURATION           JSON                        NOT NULL,
  FIELDS_MAPPING          JSON,
  PRIMARY KEY (ID_BROADCASTER_CONFIG)
)
COMMENT = 'Tabella per la gestione delle configurazioni nella lettura del tracciato file per ciascun canale';

ALTER TABLE BDC_BROADCASTER_CONFIG ADD CONSTRAINT FK_CONFIG_BROADCASTER
FOREIGN KEY (ID_BROADCASTER) REFERENCES bdc_broadcasters(id);

ALTER TABLE BDC_BROADCASTER_CONFIG ADD CONSTRAINT FK_CONFIG_CHANNEL
FOREIGN KEY (ID_CHANNEL) REFERENCES bdc_canali (id);