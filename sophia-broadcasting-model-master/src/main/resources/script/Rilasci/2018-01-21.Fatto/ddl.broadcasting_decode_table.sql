-- ---------------------------
-- bdc_broadcasters
-- ---------------------------
DROP TABLE IF EXISTS bdc_broadcasters;
CREATE TABLE bdc_broadcasters
(
  id int(11) NOT NULL AUTO_INCREMENT,
  nome varchar(255) NOT NULL,
  tipo_broadcaster enum('RADIO','TELEVISIONE') NOT NULL,
  image blob,
  data_creazione DATETIME DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
)
  COMMENT = 'Tabella rappresentante anagrafica emittenti';
COMMIT;

-- ---------------------------
-- bdc_canali
-- ---------------------------
DROP TABLE IF EXISTS bdc_canali;
CREATE TABLE bdc_canali
(
  id int(11) NOT NULL AUTO_INCREMENT,
  id_broadcaster int(11) NOT NULL,
  nome varchar(255) NOT NULL,
  tipo_canale enum('RADIO','TELEVISIONE') NOT NULL,
  generalista tinyint(1) DEFAULT 0,
  active tinyint(1) DEFAULT 1,
  special_radio TINYINT(1) DEFAULT 0,
  data_inizio_valid datetime DEFAULT NULL,
  data_fine_valid datetime DEFAULT NULL,
  data_creazione DATETIME DEFAULT CURRENT_TIMESTAMP,
  data_ultima_modifica DATETIME DEFAULT CURRENT_TIMESTAMP,
  utente_ultima_modifica varchar(255) DEFAULT 'esercizio',
  PRIMARY KEY (id),
  KEY fk_bdc_canali_1_idx (id_broadcaster),
  CONSTRAINT fk_bdc_canali_1 FOREIGN KEY (id_broadcaster) REFERENCES bdc_broadcasters (id) ON DELETE NO ACTION ON UPDATE NO ACTION
)
  COMMENT = 'Tabella di anagrafica canali';

ALTER TABLE `bdc_canali`
  CHANGE COLUMN `data_inizio_valid` `data_inizio_valid` DATETIME NULL DEFAULT CURRENT_TIMESTAMP ;

COMMIT;

-- ---------------------------
-- BDC_STORICO_CANALE
-- ---------------------------
DROP TABLE IF EXISTS BDC_STORICO_CANALE;
CREATE TABLE BDC_STORICO_CANALE
(
  ID_STORICO_CANALE int(11) NOT NULL AUTO_INCREMENT,
  ID_CANALE INT(11) NOT NULL,
  ID_BROADCASTER int(11) NOT NULL,
  NOME_CANALE varchar(255) NOT NULL,
  TIPO_CANALE enum('RADIO','TELEVISIONE') NOT NULL,
  GENERALISTA tinyint(1) DEFAULT 0,
  SPECIAL_RADIO TINYINT(1) DEFAULT 0,
  DATA_INIZIO_VALIDIT datetime DEFAULT NULL,
  DATA_FINE_VALIDIT datetime DEFAULT NULL,
  DATA_CREAZIONE DATETIME DEFAULT NULL,
  DATA_MODIFICA DATETIME DEFAULT CURRENT_TIMESTAMP,
  UTENTE_MODIFICA varchar(255) DEFAULT 'esercizio',
  PRIMARY KEY (ID_STORICO_CANALE),
  KEY fk_bdc_broadcaster_1_idx (ID_BROADCASTER),
  KEY fk_bdc_canali_2_idx (ID_CANALE),
  CONSTRAINT fk_bdc_broadcaster_1 FOREIGN KEY (ID_BROADCASTER) REFERENCES bdc_broadcasters (id),
  CONSTRAINT fk_bdc_canali_2 FOREIGN KEY (ID_CANALE) REFERENCES bdc_canali (id)
)
  COMMENT = 'Tabella dello storico modifiche anagrafica canali';
COMMIT;

-- ---------------------------
-- bdc_ruoli
-- ---------------------------
DROP TABLE IF EXISTS bdc_ruoli;
CREATE TABLE bdc_ruoli
(
  id int(11) NOT NULL AUTO_INCREMENT,
  ruolo varchar(255) NOT NULL,
  PRIMARY KEY (id)
)
  COMMENT = 'Tabella di decodifica dei ruoli degli utenti';
COMMIT;

-- ---------------------------
-- bdc_utenti
-- ---------------------------
DROP TABLE IF EXISTS bdc_utenti;
CREATE TABLE bdc_utenti
(
  id int(11) NOT NULL AUTO_INCREMENT,
  email varchar(255) NOT NULL,
  username varchar(255) NOT NULL,
  password varchar(255) NOT NULL,
  id_broadcaster int(11) NOT NULL,
  id_ruolo int(11) NOT NULL,
  data_creazione DATETIME DEFAULT CURRENT_TIMESTAMP,
  data_ultima_modifica DATETIME DEFAULT CURRENT_TIMESTAMP,
  utente_ultima_modifica varchar(255) DEFAULT 'esercizio',
  PRIMARY KEY (id),
  UNIQUE KEY email (email),
  UNIQUE KEY username (username),
  KEY FKrhdt7t6mak523i02vgrmf5v2t (id_broadcaster),
  KEY fk_bdc_utenti_1_idx (id_ruolo),
  CONSTRAINT FKrhdt7t6mak523i02vgrmf5v2t FOREIGN KEY (id_broadcaster) REFERENCES bdc_broadcasters (id),
  CONSTRAINT fk_bdc_utenti_1 FOREIGN KEY (id_ruolo) REFERENCES bdc_ruoli (id)
)
  COMMENT = 'Tabella anagrafica utenti';
COMMIT;

-- ---------------------------
-- bdc_kpi
-- ---------------------------
DROP TABLE IF EXISTS bdc_kpi;
CREATE TABLE bdc_kpi
(
  id int(11) NOT NULL AUTO_INCREMENT,
  emittente int(11) NOT NULL,
  canale int(11) NOT NULL,
  anno int(11) NOT NULL,
  mese int(11) NOT NULL,
  indicatore varchar(255) DEFAULT NULL,
  valore int(11) DEFAULT NULL,
  last_update datetime DEFAULT NULL,
  PRIMARY KEY (id),
  KEY index_calculated_data_broadcasting (emittente,canale,anno,mese),
  KEY canale (canale),
  CONSTRAINT bdc_kpi_ibfk_1 FOREIGN KEY (canale) REFERENCES bdc_canali (id)
)
  COMMENT = 'Tabella degli indicatori su cui calcolare i KPI';
COMMIT;

-- ---------------------------
-- bdc_utilization_file
-- ---------------------------
DROP TABLE IF EXISTS bdc_utilization_file;
CREATE TABLE bdc_utilization_file
(
  id int(11) NOT NULL AUTO_INCREMENT,
  emittente int(11) NOT NULL,
  canale int(11) DEFAULT NULL,
  nome_file text NOT NULL,
  anno int(11) DEFAULT NULL,
  mese int(11) DEFAULT NULL,
  data_upload datetime NOT NULL,
  percorso text NOT NULL,
  tipo_upload varchar(255) NOT NULL,
  stato enum('DA_ELABORARE','ELABORATO','VALIDATO', 'ERRORE', 'ELIMINATO') NOT NULL,
  data_processamento datetime DEFAULT NULL,
  id_utente int(11) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY broadcaster (emittente),
  KEY id_utente (id_utente),
  KEY fk_bdc_utilization_file_1_idx (canale),
  CONSTRAINT bdc_utilization_file_ibfk_1 FOREIGN KEY (id_utente) REFERENCES bdc_utenti (id),
  CONSTRAINT broadcaster FOREIGN KEY (emittente) REFERENCES bdc_broadcasters (id),
  CONSTRAINT fk_bdc_utilization_file_1 FOREIGN KEY (canale) REFERENCES bdc_canali (id)
)
  COMMENT = 'Tabella elenco file upload in S3 dei report broadcasters';
COMMIT;

-- ---------------------------
-- BDC_NEWS
-- ---------------------------
DROP TABLE IF EXISTS BDC_NEWS;
CREATE TABLE BDC_NEWS (
  BDC_NEWS_ID INT(11) AUTO_INCREMENT NOT NULL,
  TITLE VARCHAR(45) NOT NULL,
  NEWS VARCHAR(1000) NOT NULL,
  ID_BROADCASTER INT(11) NULL,
  ID_USER INT(11) NULL,
  FLAG_FOR_ALL_BDC TINYINT(1) NOT NULL DEFAULT 1,
  FLAG_FOR_ALL_BDC_USER TINYINT(1) NOT NULL DEFAULT 1,
  CREATOR VARCHAR(100) NOT NULL DEFAULT 'NEWS_ADMIN',
  INSERT_DATE DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  VALID_FROM DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  VALID_TO DATETIME NOT NULL,
  ACTIVE tinyint(1) NOT NULL DEFAULT 1,
  DEACTIVATION_DATE datetime DEFAULT NULL,
  PRIMARY KEY (BDC_NEWS_ID),
  INDEX FK_NEWS_BROADCASTER_ID_idx (ID_BROADCASTER ASC),
  CONSTRAINT FK_NEWS_BROADCASTER_ID
  FOREIGN KEY (ID_BROADCASTER)
  REFERENCES bdc_broadcasters (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  COMMENT = 'Tabella delle notizie e comunicazioni da visualizzare nella homepage';
COMMIT;

-- ---------------------------
-- BDC_SHOW_TYPE
-- ---------------------------
DROP TABLE IF EXISTS BDC_SHOW_TYPE;
CREATE TABLE BDC_SHOW_TYPE
(
   ID_SHOW_TYPE   BIGINT(20)            NOT NULL   AUTO_INCREMENT,
   CATEGORY            VARCHAR(200)  NOT NULL,
   NAME                   VARCHAR(200)  NOT NULL,
   PRIMARY KEY (ID_SHOW_TYPE)
)
COMMENT = 'Tabella di decodifica dei generi rappresentati nel formato armonizzato';
COMMIT;

-- ---------------------------
-- BDC_MUSIC_TYPE
-- ---------------------------
DROP TABLE IF EXISTS BDC_MUSIC_TYPE;
CREATE TABLE BDC_MUSIC_TYPE
(
  ID_MUSIC_TYPE   BIGINT(20)           NOT NULL   AUTO_INCREMENT,
  CATEGORY            VARCHAR(200)     NOT NULL,
  NAME                   VARCHAR(200)  NOT NULL,
  PRIMARY KEY (ID_MUSIC_TYPE)
)
COMMENT = 'Tabella di decodifica delle categorie musicali rappresentate nel formato armonizzato';
COMMIT;

-- ---------------------------
-- BDC_MUSIC_TYPE_CATEGORY
-- ---------------------------
DROP TABLE IF EXISTS BDC_MUSIC_TYPE_CATEGORY;
CREATE TABLE BDC_MUSIC_TYPE_CATEGORY
(
  ID_MUSIC_TYPE_CATEGORY    BIGINT(20)           NOT NULL   AUTO_INCREMENT,
  ID_MUSIC_TYPE             BIGINT(20)           NOT NULL,
  CATEGORY                  VARCHAR(200)  NULL,
  PRIMARY KEY (ID_MUSIC_TYPE_CATEGORY)
)
  COMMENT = 'Tabella di relazione tra Tipo Musica e Categoria Tipo Trasmissione';

ALTER TABLE BDC_MUSIC_TYPE_CATEGORY ADD CONSTRAINT FK_MUSIC_TYPE_CAT
FOREIGN KEY (ID_MUSIC_TYPE) REFERENCES BDC_MUSIC_TYPE (ID_MUSIC_TYPE);
COMMIT;

-- ---------------------------
-- BDC_DECODE_SHOW_TYPE
-- ---------------------------
DROP TABLE IF EXISTS BDC_DECODE_SHOW_TYPE;
CREATE TABLE BDC_DECODE_SHOW_TYPE
(
  ID_DECODE_SHOW_TYPE    BIGINT(20)    NOT NULL   AUTO_INCREMENT,
  ID_SHOW_TYPE           BIGINT(20)    NOT NULL,
  ID_BROADCASTER         INT(11)       NOT NULL,
  VALUE                  VARCHAR(200)  NOT NULL,
  SCOPE                  CHAR          NOT NULL,
  PRIMARY KEY (ID_DECODE_SHOW_TYPE)
)
  COMMENT = 'Tabella corrispondenza tra i generi presenti nei file originali e rappresentate nel formato armonizzato';

ALTER TABLE BDC_DECODE_SHOW_TYPE ADD CONSTRAINT FK_DECODE_SHOW_TYPE
FOREIGN KEY (ID_SHOW_TYPE) REFERENCES BDC_SHOW_TYPE (ID_SHOW_TYPE);

ALTER TABLE BDC_DECODE_SHOW_TYPE ADD CONSTRAINT FK_DECODE_SHOW_BROAD
FOREIGN KEY (ID_BROADCASTER) REFERENCES bdc_broadcasters (id);
COMMIT;

-- ---------------------------
-- BDC_DECODE_MUSIC_TYPE
-- ---------------------------
DROP TABLE IF EXISTS BDC_DECODE_MUSIC_TYPE;
CREATE TABLE BDC_DECODE_MUSIC_TYPE
(
  ID_DECODE_MUSIC_TYPE   BIGINT(20)    NOT NULL   AUTO_INCREMENT,
  ID_MUSIC_TYPE          BIGINT(20)    NOT NULL,
  ID_BROADCASTER         INT(11)       NOT NULL,
  VALUE                  VARCHAR(200)  NOT NULL,
  SCOPE                  CHAR          NOT NULL,
  PRIMARY KEY (ID_DECODE_MUSIC_TYPE)
)
COMMENT = 'Tabella corrispondenza tra categorie musicali presenti nei file originali e rappresentate nel formato armonizzato';

ALTER TABLE BDC_DECODE_MUSIC_TYPE ADD CONSTRAINT FK_DECODE_MUSIC_TYPE
FOREIGN KEY (ID_MUSIC_TYPE) REFERENCES BDC_MUSIC_TYPE (ID_MUSIC_TYPE);

ALTER TABLE BDC_DECODE_MUSIC_TYPE ADD CONSTRAINT FK_DECODE_MUSIC_BROAD
FOREIGN KEY (ID_BROADCASTER) REFERENCES bdc_broadcasters (id);
COMMIT;

-- ---------------------------
-- BDC_DECODE_CHANNEL
-- ---------------------------
DROP TABLE IF EXISTS BDC_DECODE_CHANNEL;
CREATE TABLE BDC_DECODE_CHANNEL
(
  ID_DECODE_CHANNEL       BIGINT(20)              NOT NULL   AUTO_INCREMENT,
  ID_CHANNEL              INT(11)                 NOT NULL,
  VALUE                   VARCHAR(200)            NOT NULL,
  REGIONAL                BIT           DEFAULT 0 NOT NULL,
  PRIMARY KEY (ID_DECODE_CHANNEL)
)
  COMMENT = 'Tabella di decodifica dei canali rappresentati nel formato armonizzato';

ALTER TABLE BDC_DECODE_CHANNEL ADD CONSTRAINT FK_DECODE_CHANNEL
FOREIGN KEY (ID_CHANNEL) REFERENCES bdc_canali (id);
COMMIT;

-- BDC_NORMALIZED_FILE
DROP TABLE IF EXISTS BDC_NORMALIZED_FILE;
CREATE TABLE BDC_NORMALIZED_FILE
(
  ID_NORMALIZED_FILE        BIGINT(20) NOT NULL AUTO_INCREMENT,
  FILE_NAME                 TEXT,
  FILE_BUCKET               TEXT,
  CREATION_DATE                       DATETIME    DEFAULT NOW() NOT NULL ,
  PRIMARY KEY (ID_NORMALIZED_FILE)
)
COMMENT = 'Tabella contenente i path S3 dei file normalizzati necessari per il popolamento delle utilizzazioni';

-- BDC_UTILIZATION_NORMALIZED_FILE
DROP TABLE IF EXISTS BDC_UTILIZATION_NORMALIZED_FILE;
CREATE TABLE BDC_UTILIZATION_NORMALIZED_FILE
(
  ID_BDC_UTILIZATION_NORMALIZED_FILE  BIGINT(20)                NOT NULL AUTO_INCREMENT,
  ID_UTILIZATION_FILE                 INT(11)                   NOT NULL,
  ID_NORMALIZED_FILE                  BIGINT(20)                NOT NULL,
  PRIMARY KEY (ID_BDC_UTILIZATION_NORMALIZED_FILE)
)
COMMENT = 'Tabella relazionale tra report originali e report normalizzati';

ALTER TABLE BDC_UTILIZATION_NORMALIZED_FILE ADD CONSTRAINT FK_UTIL_FILE
FOREIGN KEY (ID_UTILIZATION_FILE) REFERENCES bdc_utilization_file (id);

ALTER TABLE BDC_UTILIZATION_NORMALIZED_FILE ADD CONSTRAINT FK_NORM_FILE
FOREIGN KEY (ID_NORMALIZED_FILE) REFERENCES BDC_NORMALIZED_FILE (ID_NORMALIZED_FILE);