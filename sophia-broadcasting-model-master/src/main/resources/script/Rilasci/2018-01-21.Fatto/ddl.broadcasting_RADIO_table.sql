-- ---------------------------
-- BDC_RD_SHOW_SCHEDULE
-- ---------------------------
DROP TABLE IF EXISTS BDC_RD_SHOW_SCHEDULE;
CREATE TABLE BDC_RD_SHOW_SCHEDULE
(
  ID_RD_SHOW_SCHEDULE     BIGINT(20)                  NOT NULL    AUTO_INCREMENT,
  ID_NORMALIZED_FILE      BIGINT(20)                  NOT NULL,
  ID_CHANNEL              INT(11),
  TITLE                   VARCHAR(400),
  BEGIN_TIME              DATETIME,
  END_TIME                DATETIME,
  DURATION                INT(11),
  REGIONAL_OFFICE         VARCHAR(10),
  NOTE                    VARCHAR(400),
  OVERLAP                 BIT                         NOT NULL   DEFAULT b'0',

  /*CAMPI PER IL CALCOLO DEI KPI*/
  SCHEDULE_YEAR           INT(4)                      NOT NULL,
  SCHEDULE_MONTH          INT(2)                      NOT NULL,
  DAYS_FROM_UPLOAD        INT(11)       DEFAULT 0     NOT NULL,

  /*CAMPI DI MONITORAGGIO*/
  CREATION_DATE           DATETIME      DEFAULT NOW() NOT NULL,
  MODIFY_DATE             DATETIME,

  PRIMARY KEY (ID_RD_SHOW_SCHEDULE)
)
COMMENT = 'Tabella per la gestione delle utilizzazioni relative alle Trasmissioni RADIO';

ALTER  TABLE BDC_RD_SHOW_SCHEDULE ADD CONSTRAINT FK_RD_SHOW_NORM_FILE
FOREIGN KEY (ID_NORMALIZED_FILE) REFERENCES BDC_NORMALIZED_FILE(ID_NORMALIZED_FILE);

ALTER TABLE BDC_RD_SHOW_SCHEDULE ADD CONSTRAINT FK_RD_SHOW_CHANNEL
FOREIGN KEY (ID_CHANNEL) REFERENCES bdc_canali (id);

CREATE INDEX IDX_RD_SHOW_BEGIN_DATE ON BDC_RD_SHOW_SCHEDULE (BEGIN_TIME);

-- ---------------------------
-- BDC_RD_SHOW_MUSIC
-- ---------------------------
DROP TABLE IF EXISTS BDC_RD_SHOW_MUSIC;
CREATE TABLE BDC_RD_SHOW_MUSIC
(
  ID_RD_SHOW_MUSIC        BIGINT(20)                  NOT NULL    AUTO_INCREMENT,
  ID_NORMALIZED_FILE      BIGINT(20)                  NOT NULL,
  ID_RD_SHOW_SCHEDULE     BIGINT(20),
  ID_MUSIC_TYPE           BIGINT(20),
  TITLE                   VARCHAR(200),
  COMPOSER                VARCHAR(200),
  PERFORMER               VARCHAR(200),
  ALBUM                   VARCHAR(200),
  DURATION                INT(11),
  ISWC_CODE               VARCHAR(50),
  ISRC_CODE               VARCHAR(50),
  BEGIN_TIME              DATETIME,
  REAL_DURATION           INT(11),
  NOTE                    VARCHAR(400),

  /*CAMPI PER IL CALCOLO DEI KPI*/
  DAYS_FROM_UPLOAD        INT(11)       DEFAULT 0     NOT NULL,

  /*CAMPI DI MONITORAGGIO*/
  CREATION_DATE           DATETIME      DEFAULT NOW() NOT NULL ,
  MODIFY_DATE             DATETIME,

  PRIMARY KEY (ID_RD_SHOW_MUSIC)
)
COMMENT = 'Tabella per la gestione delle utilizzazioni relative alle Opere Musicali contenute nelle Trasmissioni RD';

ALTER  TABLE BDC_RD_SHOW_MUSIC ADD CONSTRAINT FK_RD_MUSIC_NORM_FILE
FOREIGN KEY (ID_NORMALIZED_FILE) REFERENCES BDC_NORMALIZED_FILE(ID_NORMALIZED_FILE);

ALTER TABLE BDC_RD_SHOW_MUSIC ADD CONSTRAINT FK_RD_MUSIC_SHOW
FOREIGN KEY (ID_RD_SHOW_SCHEDULE) REFERENCES BDC_RD_SHOW_SCHEDULE (ID_RD_SHOW_SCHEDULE);

ALTER TABLE BDC_RD_SHOW_MUSIC ADD CONSTRAINT FK_RD_MUSIC_TYPE
FOREIGN KEY (ID_MUSIC_TYPE) REFERENCES BDC_MUSIC_TYPE (ID_MUSIC_TYPE);

-- ---------------------------
-- BDC_ERR_RD_SHOW_SCHEDULE
-- ---------------------------
DROP TABLE IF EXISTS BDC_ERR_RD_SHOW_SCHEDULE;
CREATE TABLE BDC_ERR_RD_SHOW_SCHEDULE
(
  ID_ERR_RD_SHOW_SCHEDULE BIGINT(20)                  NOT NULL    AUTO_INCREMENT,
  ID_NORMALIZED_FILE      BIGINT(20)                  NOT NULL,
  ID_CHANNEL              INT(11),
  TITLE                   VARCHAR(400),
  BEGIN_TIME              DATETIME,
  END_TIME                DATETIME,
  DURATION                INT(11),
  REGIONAL_OFFICE         VARCHAR(10),
  ERROR                   JSON,
  GLOBAL_ERROR            VARCHAR(400),

  -- CAMPI_ORIGINALE
  REPORT_CHANNEL          VARCHAR(100),
  REPORT_BEGIN_DATE       VARCHAR(100),
  REPORT_BEGIN_TIME       VARCHAR(100),
  REPORT_DURATION         VARCHAR(100),

  /*CAMPI PER IL CALCOLO DEI KPI*/
  SCHEDULE_YEAR           INT(4),
  SCHEDULE_MONTH          INT(2),
  DAYS_FROM_UPLOAD        INT(11)       DEFAULT 0,

  /*CAMPI DI MONITORAGGIO*/
  CREATION_DATE           DATETIME      DEFAULT NOW() NOT NULL,
  MODIFY_DATE             DATETIME,

  PRIMARY KEY (ID_ERR_RD_SHOW_SCHEDULE)
)
  COMMENT = 'Tabella per la gestione delle utilizzazioni relative alle Trasmissioni RD';

ALTER  TABLE BDC_ERR_RD_SHOW_SCHEDULE ADD CONSTRAINT FK_ERR_RD_SHOW_NORM_FILE
FOREIGN KEY (ID_NORMALIZED_FILE) REFERENCES BDC_NORMALIZED_FILE(ID_NORMALIZED_FILE);

ALTER TABLE BDC_ERR_RD_SHOW_SCHEDULE ADD CONSTRAINT FK_ERR_RD_SHOW_CHANNEL
FOREIGN KEY (ID_CHANNEL) REFERENCES bdc_canali (id);

CREATE INDEX IDX_ERR_RD_SHOW_BEGIN_DATE ON BDC_ERR_RD_SHOW_SCHEDULE (BEGIN_TIME);

-- ---------------------------
-- BDC_ERR_RD_SHOW_MUSIC
-- ---------------------------
DROP TABLE IF EXISTS BDC_ERR_RD_SHOW_MUSIC;
CREATE TABLE BDC_ERR_RD_SHOW_MUSIC
(
  ID_ERR_RD_SHOW_MUSIC    BIGINT(20)                  NOT NULL    AUTO_INCREMENT,
  ID_NORMALIZED_FILE      BIGINT(20)                  NOT NULL,
  ID_ERR_RD_SHOW_SCHEDULE BIGINT(20),
  ID_MUSIC_TYPE           BIGINT(20),
  TITLE                   VARCHAR(200),
  COMPOSER                VARCHAR(200),
  PERFORMER               VARCHAR(200),
  ALBUM                   VARCHAR(200),
  DURATION                INT(11),
  ISWC_CODE               VARCHAR(50),
  ISRC_CODE               VARCHAR(50),
  BEGIN_TIME              DATETIME,
  REAL_DURATION           INT(11),

  /* CAMPI_ORIGINALE */
  REPORT_DURATION         VARCHAR(100),
  REPORT_BEGIN_TIME       VARCHAR(100),
  REPORT_MUSIC_TYPE       VARCHAR(100),

  /*CAMPI PER IL CALCOLO DEI KPI*/
  DAYS_FROM_UPLOAD        INT(11)       DEFAULT 0,

  /*CAMPI DI MONITORAGGIO*/
  CREATION_DATE           DATETIME      DEFAULT NOW() NOT NULL ,
  MODIFY_DATE             DATETIME,

  PRIMARY KEY (ID_ERR_RD_SHOW_MUSIC)
)
  COMMENT = 'Tabella per la gestione delle utilizzazioni relative alle Opere Musicali contenute nelle Trasmissioni RD';

ALTER TABLE BDC_ERR_RD_SHOW_MUSIC ADD CONSTRAINT FK_ERR_RD_MUSIC_SHOW
FOREIGN KEY (ID_ERR_RD_SHOW_SCHEDULE) REFERENCES BDC_ERR_RD_SHOW_SCHEDULE (ID_ERR_RD_SHOW_SCHEDULE);

ALTER  TABLE BDC_ERR_RD_SHOW_MUSIC ADD CONSTRAINT FK_ERR_RD_MUSIC_NORM_FILE
FOREIGN KEY (ID_NORMALIZED_FILE) REFERENCES BDC_NORMALIZED_FILE(ID_NORMALIZED_FILE);

ALTER TABLE BDC_ERR_RD_SHOW_MUSIC ADD CONSTRAINT FK_ERR_RD_MUSIC_TYPE
FOREIGN KEY (ID_MUSIC_TYPE) REFERENCES BDC_MUSIC_TYPE (ID_MUSIC_TYPE);

-- ---------------------------
-- BDC_RD_ERR_BRANO_TITOLO
-- ---------------------------
DROP TABLE IF EXISTS BDC_RD_ERR_BRANO_TITOLO;
CREATE TABLE BDC_RD_ERR_BRANO_TITOLO
(
  ID_RD_ERR_BRANO_TITOLO INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  RD_ERR_BRANO_TITOLO varchar(400)
);

-- ---------------------------
-- BDC_RD_ERR_COMPOSITORE_TITOLO
-- ---------------------------
DROP TABLE IF EXISTS BDC_RD_ERR_COMPOSITORE_TITOLO;
CREATE TABLE BDC_RD_ERR_COMPOSITORE_TITOLO
(
  ID_RD_ERR_COMPOSITORE_TITOLO INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  RD_ERR_COMPOSITORE_TITOLO varchar(400)
);