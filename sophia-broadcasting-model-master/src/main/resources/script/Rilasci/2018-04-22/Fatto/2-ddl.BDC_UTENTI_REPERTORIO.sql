SET FOREIGN_KEY_CHECKS = 0;
CREATE TABLE `BDC_UTENTI_REPERTORIO` (
  `id_utente_repertorio` int(11) NOT NULL AUTO_INCREMENT,
  `id_utente` int(11) NOT NULL,
  `nome_repertorio` varchar(100) NOT NULL,
  `default_repertorio` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id_utente_repertorio`),
  UNIQUE KEY `BDC_UTENTI_REPERTORIO_ID_UTENTI_REPERTORIO_IDX` (`id_utente_repertorio`),
  KEY `BDC_UTENTI_REPERTORIO_ID_UTENTE_IDX` (`id_utente`),
  KEY `BDC_UTENTI_REPERTORIO_NOME_REPERTORIO_IDX` (`nome_repertorio`),
  CONSTRAINT `BDC_UTENTI_REPERTORIO_BDC_REPERTORIO_FK` FOREIGN KEY (`nome_repertorio`) REFERENCES `BDC_REPERTORIO` (`nome`),
  CONSTRAINT `BDC_UTENTI_REPERTORIO_bdc_utenti_FK` FOREIGN KEY (`id_utente`) REFERENCES `bdc_utenti` (`id`)
);
commit;