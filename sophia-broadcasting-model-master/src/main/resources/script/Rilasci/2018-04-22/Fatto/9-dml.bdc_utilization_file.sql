SET FOREIGN_KEY_CHECKS = 0;
ALTER TABLE `bdc_utilization_file`
  ADD COLUMN `NOME_REPERTORIO` VARCHAR(100) NULL AFTER `id_utente`,
  ADD INDEX `fk_bdc_utilization_file_2_idx` (`NOME_REPERTORIO` ASC);
commit;