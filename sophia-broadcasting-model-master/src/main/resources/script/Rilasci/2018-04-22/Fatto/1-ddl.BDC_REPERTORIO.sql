SET FOREIGN_KEY_CHECKS = 0;
CREATE TABLE `BDC_REPERTORIO` (
  `nome` varchar(100) NOT NULL,
  `descrizione` varchar(100) DEFAULT NULL,
  `url_path` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`nome`),
  UNIQUE KEY `BDC_REPERTORIO_NOME_IDX` (`nome`)
);
commit;