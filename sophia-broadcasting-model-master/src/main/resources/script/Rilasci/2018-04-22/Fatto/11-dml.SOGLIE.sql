SET FOREIGN_KEY_CHECKS = 0;
INSERT INTO `SOGLIE` (`ID`, `CODICE`, `REPERTORIO`, `AMBITO`, `VALORE`, `OPERATORE`, `DESCRIZIONE`, `VALIDO_DA`) VALUES ('1', 'ML_VAL_ACC', 'MULTIMEDIALE_LOCALE', 'INGESTION', '85.00', '>', 'Soglia percentuale del numero di righe valide di un report', '2018-01-01');
INSERT INTO `SOGLIE` (`ID`, `CODICE`, `REPERTORIO`, `AMBITO`, `VALORE`, `OPERATORE`, `DESCRIZIONE`, `VALIDO_DA`) VALUES ('2', 'ML_COD_ACC', 'MULTIMEDIALE_LOCALE', 'CODIFICA', '70.00', '>', 'Soglia percentuale del numero di utilizzazzioni codificate di un report', '2018-01-01');
commit;