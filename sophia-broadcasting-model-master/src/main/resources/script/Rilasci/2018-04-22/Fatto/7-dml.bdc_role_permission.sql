SET FOREIGN_KEY_CHECKS = 0;
ALTER TABLE `permission`
  DROP FOREIGN KEY `FK_PERMISSION_PARENT`;
SET FOREIGN_KEY_CHECKS = 0;
ALTER TABLE `permission`
  CHANGE COLUMN `code` `code` VARCHAR(50) NOT NULL ,
  CHANGE COLUMN `parent` `parent` VARCHAR(50) NULL DEFAULT NULL ,
  CHANGE COLUMN `name` `name` VARCHAR(50) NOT NULL ;
SET FOREIGN_KEY_CHECKS = 0;
ALTER TABLE `permission`
  ADD CONSTRAINT `FK_PERMISSION_PARENT`
FOREIGN KEY (`parent`)
REFERENCES `permission` (`code`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
SET FOREIGN_KEY_CHECKS = 0;



INSERT INTO `permission` (`code`, `parent`, `name`, `menuVoice`) VALUES ('prCruscotti', 'guPerforming', 'Cruscotti', '1');
INSERT INTO `permission` (`code`, `parent`, `url`, `name`, `menuVoice`) VALUES ('prVisualizzazioneEventi', 'prCruscotti', '#!/visualizzazioneEventi', 'Visualizzazione Eventi', '1');
INSERT INTO `permission` (`code`, `parent`, `url`, `name`, `menuVoice`) VALUES ('prVisualizzazioneMovimenti', 'prCruscotti', '#!/visualizzazioneMovimenti', 'Visualizzazione Movimenti', '1');
INSERT INTO `permission` (`code`, `parent`, `url`, `name`, `menuVoice`) VALUES ('prVisualizzazioneTracciamentoPM', 'prCruscotti', '#!/tracciamentoPM', 'Tracciamento PM', '1');
INSERT INTO `permission` (`code`, `parent`, `url`, `name`, `menuVoice`) VALUES ('prVisualizzazioneFatture', 'prCruscotti', '#!/fattureList', 'Dati Economici', '1');
INSERT INTO `permission` (`code`, `name`, `menuVoice`) VALUES ('guMultimedialeLocale', 'Multimediale Locale', '1');
INSERT INTO `permission` (`code`, `parent`, `url`, `name`, `menuVoice`) VALUES ('muLocale', 'guMultimedialeLocale', '#!/multimedialeLocale', 'Visualizza Report', '1');



INSERT INTO `role_permission` (`role`, `permission`) VALUES ('DEVELOPMENT', 'prCruscotti');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('IT', 'prCruscotti');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('GA', 'prCruscotti');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('BU-MUSICA', 'prCruscotti');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('DEVELOPMENT', 'prVisualizzazioneEventi');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('BU-MUSICA', 'prVisualizzazioneEventi');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('IT', 'prVisualizzazioneEventi');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('GA', 'prVisualizzazioneEventi');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('DEVELOPMENT', 'prVisualizzazioneMovimenti');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('BU-MUSICA', 'prVisualizzazioneMovimenti');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('IT', 'prVisualizzazioneMovimenti');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('GA', 'prVisualizzazioneMovimenti');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('DEVELOPMENT', 'prVisualizzazioneTracciamentoPM');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('BU-MUSICA', 'prVisualizzazioneTracciamentoPM');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('IT', 'prVisualizzazioneTracciamentoPM');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('GA', 'prVisualizzazioneTracciamentoPM');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('DEVELOPMENT', 'prVisualizzazioneFatture');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('BU-MUSICA', 'prVisualizzazioneFatture');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('IT', 'prVisualizzazioneFatture');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('GA', 'prVisualizzazioneFatture');


INSERT INTO `role_permission` (`role`, `permission`) VALUES ('MU-MULTITERRITORIALE', 'guMultimediale');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('MU-MULTITERRITORIALE', 'muRiconoscimento');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('MU-MULTITERRITORIALE', 'riconoscimento');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('MU-MULTITERRITORIALE', 'ricerca');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('MU-MULTITERRITORIALE', 'dspStatistics');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('MU-MULTITERRITORIALE', 'muFatturazione');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('MU-MULTITERRITORIALE', 'anagClient');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('MU-MULTITERRITORIALE', 'invoices');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('MU-MULTITERRITORIALE', 'createInvoice');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('MU-MULTITERRITORIALE', 'anticipi');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('MU-MULTITERRITORIALE', 'consumoAnticipi');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('MU-MULTITERRITORIALE', 'aggValFatt');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('MU-MULTITERRITORIALE', 'muConfigurazione');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('MU-MULTITERRITORIALE', 'dsrConfig');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('MU-MULTITERRITORIALE', 'dspConfig');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('MU-MULTITERRITORIALE', 'commercialOffersConfig');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('MU-MULTITERRITORIALE', 'processManagement');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('MU-MULTITERRITORIALE', 'anagDspConfig');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('MU-MULTITERRITORIALE', 'dsrMetadataConfig');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('MU-MULTITERRITORIALE', 'ccidConfig');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('MU-MULTITERRITORIALE', 'droolsPricing');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('MU-MULTITERRITORIALE', 'blacklist');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('MU-MULTITERRITORIALE', 'commonData');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('MU-MULTITERRITORIALE', 'awsService');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('MU-MULTITERRITORIALE', 'knowledgeBase');

commit;


###################################################################################
##   DA ESEGUIRE SOLO SE SI HA AVUTO IL GO AL RILASCIO INTEGRAZIONE GOAL/SPHIA   ##
###################################################################################

INSERT INTO `role_permission` (`role`, `permission`) VALUES ('DEVELOPMENT', 'guMultimedialeLocale');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('IT', 'guMultimedialeLocale');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('MU-LOCALE', 'guMultimedialeLocale');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('BU-MULTIMEDIALE', 'guMultimedialeLocale');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('DEVELOPMENT', 'muLocale');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('IT', 'muLocale');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('MU-LOCALE', 'muLocale');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('BU-MULTIMEDIALE', 'muLocale');

COMMIT;
