INSERT INTO
`PERF_VOCE_INCASSO` (`COD_VOCE_INCASSO`, `DESCRIZIONE`, `COD_VOCE_IVA`, `COD_VOCE_MORA`, `PERCENT_MORA`, `COD_INTERESSI_MORA`, `BASE_CALC_INTER_MORA`, `FLAG_BOLLO`, `COD_POSIZIONE_IVA`, `FLAG_ENTR_USC`, `ID_TIPO_MODELLO`, `COD_IMPONIBILE_IVA`, `ID_SOPRACONTO`, `TIPO_VOCE`, `FLAG_PM`, `COD_VOCE_240`, `COD_VOCE_48M`, `FLAG_OC`, `DESCRIZIONE_BREVE`, `FLAG_PRIVACY`, `DATA_CESSAZIONE_VALIDITA`, `TIPO_PM`, `COD_VOCE_LIRICA`, `ABILITA_DIRITTI_SEGRETERIA`, `FLAG_VOCE_DEPOSITO`, `FLAG_VOCE_ORDINE_E_CONTO`, `FLAG_VOCE_VERSAMENTO`, `FLAG_AUTORALE`, `COD_VOCE_SOSTITUTIVA`, `FLAG_ALLEGATO`, `FLAG_STORNO_INTERNO`, `FLAG_NCA`, `FLAG_ENABLE_QUIETANZA_MANUALE`, `FLAG_DIRITTO_CONNESSO`)
VALUES
(
'1', 'IMPOSTA SPETTACOLI CINEMA LORDA', '5082', '', 0, '', 'I', 'S', 'a', 'Y', 7, '03', 1, '', 0, '1', '0001', 0, 'IS CINEM.LORDA', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'101', 'IMPOSTA SPETTACOLI CINEMA NETTA', '0', '0901', 3, '0191', 'I', 'S', 'a', 'Y', 1, '03', 1, 'E', 0, '101', '01', 0, 'IS CINEM.NETTA', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'1010', 'IVA ASSOCIAZIONI', '0', '0910', 3, '1090', 'V', 'S', 'a', 'Y', 1, '03', 1, 'E', 0, '1010', '02', 0, 'IVA ASSOCIAZIO', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'1011', 'IVA ASSOC.ACQ.INTERCOMUNITARI', '0', '1090', 3, '1090', 'I', 'S', 'a', 'Y', 1, '03', 1, 'E', 0, '1011', '02', 0, 'IVA ASS.ACQ.IN', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'1090', 'INT.MORA IVA ASSOCIAZIONI', '0', '', 0, '', 'T', 'S', 'a', 'Y', 1, '03', 1, 'E', 0, '1090', '02', 0, 'IVA ASS.MORA', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'1101', 'CONS. DICH. RILASCIATE DALLA D.G.', '5183', '', 0, '', 'I', 'N', 'g', 'Y', 1, '01', 1, 'g', 0, '1101', '11', 0, 'CONS.DICH.D.G.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'1212', 'MUSICA EMITTENTI PRIVATE (MEP) RADIO', '5183', '2442', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '1212', '12', 0, 'MUS.EM.PR.RADI', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 0, 0
)
,
(
'1213', 'MEP TV ETERE', '5183', '2442', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '1213', '12', 0, 'MEP TV ETERE', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 0, 0
)
,
(
'1214', 'MEP TV CAVO', '5183', '2442', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '1214', '12', 0, 'MEP TV CAVO', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'1215', 'MUSICA RADIOFONICA NAZIONALE', '5183', '2442', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '1215', '12', 0, 'MUS.RADIOF.NAZ', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'1225', 'MUSICA RR EMITTENTI ESTERE', '5183', '2442', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '1225', '12', 0, 'MUS.RR.PR.EST.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'1226', 'MUSICA TV EMITTENTI ESTERE', '5183', '2442', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '1226', '12', 0, 'MUS.TV.PR.EST.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'1232', 'MEP RADIO ARRETRATI ANNI PRECEDENTI', '5183', '2442', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '1232', '12', 0, 'MEP RADIO ARRE', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 0, 0
)
,
(
'1233', 'MEP TV ETERE ARRETRATI ANNI PRECEDENTI', '5183', '2442', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '1233', '12', 0, 'MEP TV ETERE A', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 0, 0
)
,
(
'1234', 'MEP TV CAVO ARRETRATI ANNI PRECEDENTI', '5183', '2442', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '1234', '12', 0, 'MEP TV CAVO AR', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'1235', 'MUSICA RADIOF.NAZ.ARRETRATI', '5183', '2442', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '1235', '12', 0, 'MUSICA RADIOF.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'1240', 'MUSICA RADIO SATELLITARE', '5183', '2442', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '1240', '12', 0, 'MUS.RADIO SAT.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'1241', 'MUSICA TV SATELLITARE', '5183', '2442', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '1241', '12', 0, 'MUS.TV SAT.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'1250', 'MUSICA RR TRASMISS.DEDICATE', '5183', '2442', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '1250', '12', 0, 'MUS.RR TR.DEDI', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'1251', 'MUSICA TV TRASMISS.DEDICATE', '5183', '2442', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '1251', '12', 0, 'MUS.TV TR.DEDI', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'1260', 'MUSICA RR RETI TELEMATICHE', '5183', '2442', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '1260', '12', 0, 'MUS.RR RETI TE', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'1261', 'MUSICA TV RETI TELEMATICHE', '5183', '2442', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '1261', '12', 0, 'MUS.TV RETI TE', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'1313', 'DOR EMITTENTI PRIVATE', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '1313', '13', 0, 'DOR-LIR.EM.RR.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'1314', 'DLEP TV ETERE', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '1314', '13', 0, 'DLEP TV ETERE', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'1315', 'DLEP TV CAVO', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '1315', '13', 0, 'DLEP TV CAVO', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'1319', 'LIRICA EMITTENTI LOCALI RADIO/TELEVISIVE', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '1319', '13', 0, 'LIR.EM.RR.TV.', 0, NULL, '', 'G', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'1377', 'FITTI ATTIVI NON COMMERCIALI', '0', '', 0, '', 'O', 'S', 'i', 'Y', 1, '03', 1, '', 0, '5486', '54', 0, 'FITTI ATT.N.C.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'1378', 'CESSIONI MACCHINE ED ARREDI', '5183', '', 0, '', 'O', 'N', 'q', 'Y', 1, '01', 1, '', 0, '5486', '54', 0, 'CES.MACCH.ARR.', 0, '2013-12-31 00:00:00', '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'1379', 'VENDITA ROTTAMI E MATERIALI DI RECUPERO', '5183', '', 0, '', 'O', 'N', 'p', 'Y', 1, '01', 1, '', 0, '5486', '54', 0, 'VEND.ROTT.MAT.', 0, '2013-12-31 00:00:00', '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'1414', 'DOR PUBBLICO DOMINIO', '0', '0914', 3, '1494', 'I', 'S', 'a', 'Y', 1, '03', 1, 'E', 0, '1414', '14', 0, 'DOR PUBBL.DOM.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'1494', 'INT.MORA DOR P.D.31/12/96', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, 'a', 0, '1494', '14', 0, 'DOR PD INT.MOR', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'1515', 'DOR TEATRO', '5183', '1818', 0, '1818', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '1515', '15', 0, 'DOR TEATRO', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'1516', 'DOR VIDEORAPPRESENTAZIONI', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '1516', '15', 0, 'DOR VIDEORAPP.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'1533', 'DRM VIDEOGRAMMI DOR', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '1533', '15', 0, 'DRM VIDEOG.DOR', 0, NULL, '', '', 0, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'1534', 'DRM VIDEOGRAMMI DOR EXPORT', '0', '', 0, '', 'I', 'S', 'l', 'Y', 1, '04', 1, '', 0, '1534', '15', 0, 'DRM VD.DOR.EXP', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'1535', 'DOR DRM AUDIO', '5183', '1818', 0, '1818', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '1535', '15', 0, 'DOR DRM A.', 0, NULL, '', '', 0, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'1567', 'DOR DIVERSI ART. 72 III C.', '0', '', 0, '', 'I', 'S', 'm', 'Y', 1, '03', 1, '', 0, '1567', '15', 0, 'DOR DIV.ART.72', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'1615', 'DOR TV CAMERE', '5183', '1818', 0, '1818', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '1615', '16', 0, 'DOR TV CAMERE', 0, '2013-12-31 00:00:00', '', '', 1, 0, 0, 0, 1, '2215', 0, 0, 1, 1, 0
)
,
(
'1616', 'DOR TV', '5183', '1818', 0, '1818', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '1616', '16', 0, 'DOR TV', 0, '2013-12-31 00:00:00', '', '', 1, 0, 0, 0, 1, '2216', 0, 0, 1, 1, 0
)
,
(
'1660', 'APPARECCHI TELEVISIVI DOR SU NAVI TRASP. INTERNAZ.', '0', '', 0, '', 'I', 'S', 'l', 'Y', 1, '04', 1, '', 0, '1660', '16', 0, 'DOR APP.TELEV.NAVI INT.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'1705', 'RITENUTE F/S REPERTORIO NAPOLETANO', '0', '', 0, '', 'O', 'N', 'a', 'Y', 1, '03', 1, '', 0, '5486', '54', 0, 'RIT.F/S REP.NA', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'1706', 'RITENUTE SPESE POSTALI REP. NAPOLETANO', '0', '', 0, '', 'O', 'N', 'a', 'Y', 1, '03', 1, '', 0, '5486', '54', 0, 'RIT.S/P REP.NA', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'1708', 'REPERTORIO NAPOLETANO AVENTI DIRITTO', '0', '', 0, '', 'I', 'N', 'a', 'N', 1, '03', 1, '', 0, '1708', '7', 0, 'REP.NAP.AV.DIR', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'1709', 'RITENUTA ACCONTO SU REPERT. NAPOLETANO', '0', '', 0, '', 'O', 'N', 'a', 'Y', 1, '03', 1, '', 0, '5486', '54', 0, 'R.A. REP.NAP.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'1717', 'DOR REPERTORIO NAPOLETANO', '5183', '1818', 5, '1818', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '1717', '17', 0, 'DOR REP.NAP.', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'1788', 'REP. NAPOLETANO AVENTI DIRITTO IMP. 22%', '5183', '', 0, '', 'I', 'N', 'd', 'N', 1, '01', 1, '', 0, '1788', '7', 0, 'REP.NAP.AV.DIR', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'1818', 'DOR INTERESSI MORATORI E PENALITA', '0', '', 0, '', 'I', 'S', 'k', 'Y', 1, '03', 1, 'S', 0, '1818', '18', 0, 'DOR INT.MORAT.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'1820', 'INCASSI COMPLEMENTARI SEZIONE LIRICA', '0', '', 0, '', 'I', 'S', 'k', 'Y', 1, '03', 1, '', 0, '1820', '18', 0, 'INC.COMP.LIRIC', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'191', 'INT.MORA IMP.SPETT CINEMA', '0', '', 0, '', 'S', 'S', 'a', 'Y', 1, '03', 1, 'a', 0, '191', '01', 0, 'IS CINEMA INT.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'1919', 'LIRICA NOLEGGIO MATERIALE', '5183', '', 0, '', 'I', 'N', 'f', 'Y', 1, '01', 1, 'M', 0, '1919', '19', 0, 'LIRICA NOL.MAT', 0, NULL, '', 'C', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'1929', 'EDIZIONI CRITICHE', '5183', '1818', 14, '1818', 'I', 'N', 'f', 'Y', 1, '01', 1, 'M', 0, '1929', '19', 0, 'EDIZIONI CRIT.', 0, NULL, '', 'D', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'202', 'IMPOSTA SPETTACOLI ORDINARIO', '0', '0902', 3, '0292', 'I', 'S', 'a', 'Y', 1, '03', 1, 'E', 0, '202', '02', 0, 'IS ORDINARIO', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'2020', 'LIRICA TUTELATO/OPERE ED ORATORI', '5183', '2090', 0, '2090', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2020', '20', 0, 'LIRICA TUT.O.O', 0, NULL, '', 'A', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2021', 'LIRICA ELABORATO/OPERE ED ORATORI', '5183', '2090', 0, '2090', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2021', '20', 0, 'LIRICA ELA.O.O', 0, NULL, '', 'B', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2022', 'LIRICA/BALLETTO/COREOGRAFIA', '5183', '2090', 0, '2090', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2022', '20', 0, 'LIRICA BAL.COR', 0, NULL, '', 'V', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2023', 'LIRICA/BALLETTO/MUSICA', '5183', '2090', 0, '2090', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2023', '20', 0, 'LIRICA BAL.MUS', 0, NULL, '', 'Z', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2033', 'DRM VIDEOGRAMMI LIRICA', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '2033', '20', 0, 'DRM VIDEOG.LIR', 0, NULL, '', 'E', 0, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2034', 'DRM VIDEOGRAMMI LIRICA EXPORT', '0', '', 0, '', 'I', 'S', 'l', 'Y', 1, '04', 1, '', 0, '2034', '20', 0, 'DRM VID.LIR.EX', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2067', 'LIRICA DIVERSI ART. 72 III C.', '0', '', 0, '', 'I', 'S', 'm', 'Y', 1, '03', 1, 'M', 0, '2067', '20', 0, 'LIR.DIV.ART.72', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2072', 'LIRICA - DRM DIRITTI FONO', '5183', '2090', 0, '2090', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2072', '20', 0, 'DRM FONO', 0, NULL, '', 'S', 0, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2073', 'LIRICA - DRM DIRITTI VIDEO', '5183', '2090', 0, '2090', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2073', '20', 0, 'DRM VID.', 0, NULL, '', 'T', 0, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2074', 'LIRICA DRM DD. FONO EXPORT DA DICH.INT.', '0', '', 0, '', 'I', 'S', 'l', 'Y', 1, '04', 1, '', 0, '2074', '20', 0, 'DRM FONO EXP', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2090', 'LIRICA - INTERESSI COMPENSATIVI AA.DD', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, 'S', 0, '2090', '20', 0, 'LIR.INT.AA.DD', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'2121', 'LIRICA PUBBLICO DOMINIO', '0', '0921', 3, '2191', 'I', 'S', 'a', 'Y', 1, '03', 1, 'E', 0, '2121', '21', 0, 'LIRICA P.D.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'2157', 'RIMB.SPESE DA LOCAZIONI USO ABITATIVO', '0', '', 0, '', 'O', 'S', 'a', 'Y', 1, '03', 1, '', 0, '2157', '54B', 1, 'R.SPESE LOCAZ.', 0, '2013-12-31 00:00:00', '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2158', 'RIMBORSO CONDOMINIO USO COMMERCIALE', '6404', '', 0, '', 'O', 'S', 'a', 'Y', 1, '03', 1, '', 0, '2158', '54B', 1, 'R.SPESE LOCAZ.', 0, '2013-12-31 00:00:00', '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2170', 'OPERAZIONI VARIE NON SOGGETTE', '0', '', 0, '', 'O', 'N', 'a', 'N', 1, '03', 1, '', 0, '5387', '10U', 1, 'OP.VARIE NX', 0, NULL, '', '', 1, 0, 1, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2171', 'PAGAMENTI DIVERSI A FORNITORI', '0', '', 0, '', 'O', 'N', 'a', 'N', 1, '03', 1, '', 0, '2171', '10U', 1, 'PAG.DIV.FORN.', 0, '2013-12-31 00:00:00', '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2172', 'PAGAMENTI DIVERSI AD ALTRI SOGGETTI', '0', '', 0, '', 'O', 'N', 'a', 'N', 1, '03', 1, '', 0, '2172', '10U', 1, 'PAG.DIV.ALTRI', 0, '2013-12-31 00:00:00', '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2175', 'PROVVIDENZA AGENTI TERREMOTATI', '0', '', 0, '', 'O', 'N', 'a', 'N', 1, '03', 1, '', 0, '5387', '10U', 1, 'PR.AGEN.TERR.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2191', 'INT.MORA LIRICA P.D. 31/12/96', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, 'a', 0, '2191', '21', 0, 'INT.LIRICA PD', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'2201', 'OPERE DI PUBBLICO DOMINIO', '0', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 1, '0', '', 0, 'OPERE DI PUBBLICO DOMINIO', 0, NULL, '107/OR', '', 1, 0, 0, 0, 1, '', 0, 0, 1, 1, 0
)
,
(
'2202', 'OPERE NON TUTELATE DALLA SIAE', '0', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 1, '0', '', 0, 'OPERE NON TUTELATE SIAE', 0, NULL, '107/OR', '', 1, 0, 0, 0, 1, '', 0, 0, 1, 1, 0
)
,
(
'2208', 'MIOBORDERO\' TRATTENIMENTI SENZA BALLO CON OR IN CIRCOLI ARCI', '5183', '2442', 0, '2442', 'M', 'N', 'd', 'Y', 1, '01', 1, 'M', 1, '2208', '22', 0, 'MB.TRATT. SN BALLO CIRC.ARCI OR', 0, NULL, '107/OR', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2209', 'MIOBORDERO\' TRATTENIMENTI SENZA BALLO CON SM IN CIRCOLI ARCI', '5183', '2442', 0, '2442', 'M', 'N', 'd', 'Y', 1, '01', 1, 'M', 1, '2209', '22', 0, 'MB.TRATT. SN BALLO CIRC.ARCI SM', 0, NULL, '107/SM', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2210', 'MIOBORDERO\' BALLO SM IN CIRCOLI ARCI', '5183', '2442', 0, '2442', 'M', 'N', 'd', 'Y', 1, '01', 1, 'M', 1, '2210', '22', 0, 'MB.BL.SM CIRC.ARCI', 0, NULL, '107/SM', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2211', 'MIOBORDERO\' BALLO OR IN CIRCOLI ARCI', '5183', '2442', 0, '2442', 'M', 'N', 'd', 'Y', 1, '01', 1, 'M', 1, '2211', '22', 0, 'MB.BL.OR CIRC.ARCI', 0, NULL, '107/OR', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2215', 'TV CAMERE MUSICA D\'AMBIENTE', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2215', '22B', 0, 'TV CAMERE MUSICA AMBIENTE', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2216', 'TV MUSICA D\'AMBIENTE', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2216', '22B', 0, 'TV MUSICA AMBIENTE', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2218', 'TRATTENIMENTI SENZA BALLO CON OR IN FESTE PRIVATE E CIRCOLI', '5183', '2442', 0, '2442', 'M', 'N', 'd', 'Y', 1, '01', 1, 'M', 1, '2218', '22', 0, 'TRATT. SN BALLO FES. PRIV. OR', 0, NULL, '107/OR', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2219', 'TRATTENIMENTI SENZA BALLO CON SM IN FESTE PRIVATE E CIRCOLI', '5183', '2442', 0, '2442', 'M', 'N', 'd', 'Y', 1, '01', 1, 'M', 1, '2219', '22', 0, 'TRATT. SN BALLO FES. PRIV. SM', 0, NULL, '107/SM', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2220', 'BALLO SM FESTE PRIVATE E CIRCOLI', '5183', '2442', 0, '2442', 'M', 'N', 'd', 'Y', 1, '01', 1, 'M', 1, '2220', '22', 0, 'BL.SM FES.PRIV', 0, NULL, '107/SM', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2221', 'BALLO DAL VIVO  FESTE PRIVATE E CIRCOLI', '5183', '2442', 0, '2442', 'M', 'N', 'd', 'Y', 1, '01', 1, 'M', 1, '2221', '22', 0, 'BL.OR.FES.PRIV', 0, NULL, '107/OR', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2222', 'BALLO DAL VIVO ', '5183', '2442', 0, '2442', 'M', 'N', 'd', 'Y', 1, '01', 1, 'M', 1, '2222', '22', 0, 'BALLO DAL VIVO', 0, NULL, '107/OR', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2223', 'BALLO CON SM', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 1, '2223', '22', 0, 'BALLO CON SM', 0, NULL, '107/SM', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2224', 'MUSICHE DI SCENA', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 1, '2224', '22', 0, 'MUSICHE SCENA', 0, NULL, '107/C', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2225', 'MUSICA IN ARTE VARIA NIGHT', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 1, '2225', '22', 0, 'MUSICA A.V. NIGHT', 0, NULL, '107/OR', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2226', 'MUSICA IN ARTE VARIA E CABARET', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 1, '2226', '22', 0, 'MUSICA A.V. CABARET', 0, NULL, '107/OR', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2227', 'MUSICA D\'AMBIENTE. ABBONAMENTI STRUMENTI MECCANICI', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2227', '22', 0, 'STRUM.MECC.ABB', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2228', 'DIAPOSITIVE PUBBLICITARIE', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2228', '22', 0, 'DIAPOS.PUBBL.', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2229', 'MUSICA D\'AMBIENTE JUKEBOX - KARAOKE', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2229', '22', 0, 'SM JB KARAOKE', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2230', 'MUSICA D\'AMBIENTE - VIDEOREGISTRATORI ', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2230', '22', 0, 'VIDEOR.IN ABB.', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2231', 'MUSICA D\'AMBIENTE APPARECCHIO RADIORICEVENTE', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2231', '22', 0, 'APP. RADIORIC.', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2232', 'FILODIFFUSIONE - MUSICA D\'AMBIENTE ', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2232', '22', 0, 'FILODIFFUSIONE', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2233', 'SPETTACOLI DI DANZA', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 1, '2233', '22', 0, 'SPETT. DANZA', 0, NULL, '107/C', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2234', 'CONCERTI MUSICA CLASSICA', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 1, '2234', '22', 0, 'CONC.MUS.CLASS', 0, NULL, '107/C', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2235', 'TRATTENIMENTI DAL VIVO SENZA BALLO ', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 1, '2235', '22', 0, 'TRATT. VIVO SN BALLO', 0, NULL, '107/OR', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2236', 'ESECUZIONI BANDE E CORI IN ABBONAMENTO', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 1, '2236', '22', 0, 'CON. BAND. ABB.', 0, NULL, '107/OR', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2237', 'ESECUZIONI BANDE E CORI FUORI ABBONAMENTO', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 1, '2237', '22', 0, 'CON. BAND. F. ABB.', 0, NULL, '107/OR', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2238', 'ESECUZIONI BANDE A PAGAMENTO ', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 1, '2238', '22', 0, 'ESEC. BAND. PAG.', 0, NULL, '107/OR', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2239', 'CONCERTINI: CINEBOX,VIDEOREGISTRATORI E ALTRO', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 1, '2239', '22', 0, 'CONC. CINEBOX VIDEOREG.', 0, NULL, '107/SM', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2240', 'TRATTENIMENTI SM SENZA BALLO ', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 1, '2240', '22', 0, 'TRATT. SM SN BALL0', 0, NULL, '107/SM', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2241', 'MUSICA D\'AMBIENTE:VIDEOGIOCHI', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2241', '22', 0, 'SM VIDEOGIOCHI', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2242', 'TRATTENIMENTI KARAOKE', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 1, '2242', '22', 0, 'TRATT. KARAOKE', 0, NULL, '107/OR', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2244', 'CONCERTI/SPETTACOLI  DI MUSICA LEGGERA', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 1, '2244', '22', 0, 'CONC/SPETT. MUS. LEGG.', 0, NULL, '107/OR', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2245', 'MEGACONCERT', '0', '', 0, '', 'I', 'N', '', 'Y', 1, '', 1, '', 0, '2244', '22', 0, 'MEGACONCERTI', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2246', 'MUSICA TEMATICA PARCHI MUSEI MOSTRE', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 1, '2246', '22', 0, 'MUSICA TEMATICA', 0, NULL, '107/C', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2247', 'ESECUZIONI CON STRUMENTI MECCANICI RESIDUALE', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2247', '22', 0, 'STR.MECC.RES.', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2248', 'VIDEOREGISTRATORI CAMERE', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2248', '22', 0, 'VIDEOREG.CAM.', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2249', 'MUSICA TEMATICA IN LUDOTECHE E SPAZI PER L¿INFANZIA', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 1, '2249', '22', 0, 'MUS.TEM.LUDOT.', 0, NULL, '107/C', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2250', 'MERCOLEDI\' LIVE', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 1, '2250', '22', 0, 'MERCOLEDI\' LIVE', 0, NULL, '107/OR', '', 0, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2252', 'TRATTENIMENTI DAL VIVO SENZA BALLO SU MEZZI TRASPORTO NAZIONALE ', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 1, '2252', '22', 0, 'TRATT. OR SN BALLO MTN', 0, NULL, '107/OR', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2253', 'BALLO  DAL VIVO MEZZI TRASPORTO nazionale', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 1, '2253', '22', 0, 'BALLO VIVO MTN', 0, NULL, '107/OR', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2254', 'BALLO CON SM MEZZI TRASPORTO nazionale', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 1, '2254', '22', 0, 'BALLO SM MTN', 0, NULL, '107/OR', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2255', 'TRATTENIMENTI SM SENZA BALLO SU MEZZI TRASPORTO NAZIONALE ', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 1, '2255', '22', 0, 'TRATT. SM SN BALLO MTN', 0, NULL, '107/SM', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2258', 'SPETTACOLI MUSICALI MEZZI TRASPORTO  nazionale', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 1, '2258', '22', 0, 'SPETT.MUS. MTN', 0, NULL, '107/OR', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2259', 'MUSICA D\'AMBIENTE SM MULTIMEDIALE DEDICATO', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2259', '22', 0, 'MUS.SM.MD.DED.', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2260', 'MUSICA D\'AMBIENTE: STRUMENTI MECCANICI  SU NAVI TRASPORTO INTERNAZIONALE', '0', '2442', 0, '2442', 'I', 'S', 'l', 'Y', 1, '04', 1, 'M', 0, '2260', '22', 0, 'SM SU NAVI INT.', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2261', 'RADIO RICEVENTI EXTRATERRITORIALITA\'', '0', '2442', 0, '2442', 'I', 'S', 'l', 'Y', 1, '04', 1, 'M', 0, '2261', '22', 0, 'RADIORIC. EXTRAT.', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2262', 'TRATTENIMENTI DAL VIVO SENZA BALLO SU MEZZI TRASPORTO INTERNAZIONALE ', '0', '2442', 0, '2442', 'I', 'S', 'l', 'Y', 1, '04', 1, 'M', 1, '2262', '22', 0, 'TRATT. OR SN BALLO INTER.', 0, NULL, '107/OR', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2263', 'BALLO  DAL VIVO NAVI TRASPORTO INTERNAZIONALE', '0', '', 0, '', 'I', 'S', 'l', 'Y', 1, '04', 1, 'M', 1, '2263', '22', 0, 'BALLO VIVO INTER.', 0, NULL, '107/OR', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2264', 'BALLO CON SM NAVI TRASPORTO INTERNAZIONALE', '0', '2442', 0, '2442', 'I', 'S', 'l', 'Y', 1, '04', 1, 'M', 1, '2264', '22', 0, 'BALLO SM INTER.', 0, NULL, '107/SM', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2265', 'TRATTENIMENTI SM SENZA BALLO SU MEZZI TRASPORTO INTERNAZIONALE ', '0', '', 0, '', 'M', 'S', 'm', 'Y', 1, '03', 1, 'M', 1, '2265', '22', 0, 'TRATT. SM SN BALLO INTERN', 0, NULL, '107/SM', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2266', 'BANDE NATO EXTRATERRITORIALITA\'', '0', '', 0, '', 'I', 'S', 'l', 'Y', 1, '04', 1, '', 1, '2266', '22', 0, 'BANDE NATO', 0, NULL, '107/OR', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2267', 'DEM DIVERSI NON IMPONIBILE', '0', '2442', 0, '2442', 'I', 'S', 'l', 'Y', 1, '04', 1, 'M', 1, '2267', '22', 0, 'DEM ESEC. DIV. INTER.', 0, NULL, '107/OR', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2268', 'SPETTACOLI MUSICA LEGGERA NAVI TRASPORTO INTERNAZIONALE', '0', '', 0, '', 'M', 'S', 'm', 'Y', 1, '03', 1, '', 1, '2268', '22', 0, 'SP.MUS.NAV.TI', 0, NULL, '107/OR', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2271', 'MUSICA D\'AMBIENTE STRUMENTI MUSICALI A DISPOSIZIONE ', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2271', '22', 0, 'SM ABB. A DISP.', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2272', 'MUSICA D\'AMBIENTE: APP. RIPR. AUDIO (LETTORI CD, MP3, PC, MULTIMEDIALI, ETC.)', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2272', '22', 0, 'SM APP. RIPR. AUDIO', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2273', 'TOGLIERE ', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2273', '22', 0, 'SM.MECC.MULTIM', 0, '2014-09-08 00:00:00', '', '', 1, 0, 0, 0, 1, '2272', 0, 0, 1, 1, 0
)
,
(
'2276', 'MUSICA D\'AMBIENTE ALTOPARLANTI RR', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2276', '22', 0, 'MUS.AMB.ALT.RR', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2277', 'MUSICA D\'AMBIENTE ALTOPARLANTI FD', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2277', '22', 0, 'MUS.AMB.ALT.FD', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2278', 'MUSICA D\'AMBIENTE ALTOPARLANTI APPARECCHI RIPRODUTTORI AUDIO', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2278', '22', 0, 'MUS.AMB.ALTOP.', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2279', 'MUSICA D\'AMBIENTE SM MULTIMEDIALE DEDICATO ALTOPARLANTI', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2279', '22', 0, 'SM.ALTOP.MD.D.', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2281', 'MUSICA D\'AMBIENTE ATTESE TELEF0NICHE', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2281', '22', 0, 'MUS.ATT.TELEF.', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2282', 'FLAT AUDIO', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2282', '22B', 0, 'FLAT AUDIO', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2283', 'FLAT VIDEO', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2283', '22B', 0, 'FLAT VIDEO', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2284', 'FLAT ALL IN ONE', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2284', '22B', 0, 'FLAT ALL IN ONE', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2292', 'TOGLIERE ', '0', '', 0, '', 'I', 'N', 'n', 'Y', 1, '04', 1, 'n', 0, '2292', '22', 0, 'BAL.OR.IVA FT.', 0, '2013-12-31 00:00:00', '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'2293', 'SPETTACOLI MUSICA JAZZ', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 1, '2293', '22', 0, 'SPETT. MUS. JAZZ', 0, NULL, '107/C', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2294', 'SFILATE MODA CARRI CARNEVALESCHI RIEVOCAZIONI STORICHE', '5183', '2442', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 1, '2294', '22', 0, 'SFIL.MODA/CAR.', 0, NULL, '107/OR', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2296', 'CANZONI SCENEGGIATE - CIRCHI ', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 1, '2296', '22', 0, 'CANZ.SCEN.CIRC', 0, NULL, '107/OR', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2297', 'MUSICA IN SPETTACOLI VIAGGIANTI', '5183', '2442', 3, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2297', '22', 0, 'MUSICA IN SV', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2340', 'MUSICA FILM ', '0', '2442', 0, '2442', 'M', 'S', 'b', 'Y', 1, '03', 1, 'M', 0, '2340', '23', 0, 'MUSICA FILM', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2341', 'MUSICA FILM PUBBLICITARI', '5183', '', 0, '', 'M', 'N', 'd', 'Y', 1, '01', 1, 'M', 1, '2341', '23', 0, 'MUS.FILM PUBBL', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2342', 'SCONTO ACCORDO PDM', '0', '', 0, '', '', '', '', 'Y', 1, '', 0, 'M', 0, '2342', '23', 0, 'SCONTO ACCORDO PDM', 0, NULL, '', '', 0, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2365', 'FILM PUBBLICITARI - ESECUZIONI EXTRATERRITORIALI', '0', '', 0, '', 'I', 'S', 'l', 'Y', 1, '04', 1, '', 0, '2365', '23', 0, 'FILM PUBBL. EXTRAT.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2442', 'MUSICA INTERESSI MORATORI E PENALITA', '0', '', 0, '', 'I', 'S', 'k', 'Y', 1, '03', 1, 'S', 0, '2442', '24', 0, 'MUSICA INT.MOR', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'2501', 'UTILIZZAZIONE BENI CULTURALI', '5183', '', 0, '', 'I', 'N', 'g', 'Y', 1, '01', 1, '', 0, '2501', '25', 0, 'UT.BENI CULTUR', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2502', 'VIDIMAZIONE BENI CULTURALI', '5183', '', 0, '', 'I', 'N', 'g', 'Y', 1, '01', 1, '', 0, '2502', '25', 0, 'VID.BENI CULTU', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2509', 'REGISTRO CONVENZIONE LIUTAI', '5183', '', 0, '', 'M', 'N', 'g', 'Y', 1, '01', 1, 'M', 0, '2509', '25', 0, 'REG.LIUTAI', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2510', 'NOLEGGIO ISTITUTO LUCE', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, 'Q', 0, '2510', '25', 0, 'NOL.ISTIT.LUCE', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'2511', 'NOLEGGIO LADY FILM', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, 'Q', 0, '2511', '25', 0, 'NOL.LADY FILM', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'2512', 'NOLEGGIO LANTIA CINEMA e AUD.', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, 'Q', 0, '2512', '25', 0, 'NOL.LANTIA CIN', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'2513', 'NOLEGGIO LION PICTURES', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, 'Q', 0, '2513', '25', 0, 'NOL.LION PICT.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'2514', 'NOLEGGIO LUCKY RED', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, 'Q', 0, '2514', '25', 0, 'NOL.LUCKY RED', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'2515', 'NOLEGGIO MORGAN FILM', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, 'Q', 0, '2515', '25', 0, 'NOL.MORGAN FIL', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'2547', '', '0', '', 0, '', 'I', 'N', '', 'Y', 1, '04', 1, '', 0, '2547', '25', 0, 'NON UTILIZZATO', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2550', 'NOLEGGIO COMMERCIAL VIDEO', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '2550', '25B', 0, 'NOL.COM.VIDEO', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2551', 'PENALITA\' NOLEGGIO COMM. VIDEO', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '2551', '25B', 0, 'P.NOL.COM.VID', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2552', 'INCASSI COMPLEM. NOLEGGIO C.V.', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '2552', '25B', 0, 'NOL.COM.VIDEO', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2570', 'TOSAP', '0', '2571', 3, '2572', 'I', 'S', 'a', 'Y', 1, '03', 1, 'E', 0, '2570', '25', 0, 'TOSAP', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'2571', 'TOSAP SOPRATTASSA', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, 'E', 0, '2571', '25', 0, 'TOSAP SOPRATT.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'2572', 'TOSAP INTERESSI MORATORI', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, '', 0, '2572', '25', 0, 'TOSAP INT.MOR.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2573', 'TOSAP SANZIONI PECUNIARIE', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, '', 0, '2573', '25', 0, 'TOSAP SAN.PEC.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2574', 'IMPOSTA PUBBLICITA', '0', '2575', 3, '2576', 'I', 'S', 'a', 'Y', 1, '03', 1, 'E', 0, '2574', '25', 0, 'IMP.PUBBLICITA', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'2575', 'IMPOSTA PUBBLICITA SOPRATTASSA', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, 'E', 0, '2575', '25', 0, 'IMP.PUB.SOPRAT', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'2576', 'IMPOSTA PUBBLICITA INT.MORA', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, '', 0, '2576', '25', 0, 'IMP.PUB.MORA', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2577', 'IMPOSTA PUBBLICITA SANZ.PEC.', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, '', 0, '2577', '25', 0, 'IMP.PUB.SAN.PE', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2578', 'DIRITTI AFFISSIONE', '0', '2580', 3, '2581', 'I', 'S', 'a', 'Y', 1, '03', 1, 'E', 0, '2578', '25', 0, 'DIR.AFFISSIONE', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'2579', 'DIRITTI D\'AFFISSIONE URGENZA', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, '', 0, '2579', '25', 0, 'AFF.URGENZA', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2580', 'DIRITTI AFFISSIONE SOPRATTASSA', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, 'E', 0, '2580', '25', 0, 'AFFISS.SOPRATT', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'2581', 'DIRITTI AFFISSIONE INT.MORA', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, '', 0, '2581', '25', 0, 'AFFISS.INT.MOR', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2582', 'DIRITTI AFFISSIONE SANZ.PEC.', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, '', 0, '2582', '25', 0, 'AFFISS.SANZ.PE', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2590', 'RAI ABBONAMENTI SPECIALI RADIO', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, '', 0, '2590', '25', 0, 'RAI SP.RADIO', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2591', 'RAI ABBONAMENTI SPECIALI TV', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, '', 0, '2591', '25', 0, 'RAI SP.TV', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2592', 'RAI ABB.SPECIALI TV PASS.CAT', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, '', 0, '2592', '25', 0, 'RAI SP.TV PASS', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2593', 'RAI ABB.SPECIALI TV INTEGR.', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, '', 0, '2593', '25', 0, 'RAI SP.TV INTE', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2701', 'OLAF REPROGRAFIA COPISTERIE', '5183', '', 0, '', 'M', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2701', '27', 0, 'OLAF REPR.COP.', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 0, 1, 1, 0
)
,
(
'2702', 'OLAF REPROGRAFIA INTERESSI', '5183', '', 0, '', 'M', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2702', '27', 0, 'OLAF REPR.INT.', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 0, 1, 1, 0
)
,
(
'2703', 'OLAF REPROGRAFIA UNIVERSITA', '5183', '', 0, '', 'M', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2703', '27B', 0, 'OLAF REPR.UN.', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 0, 1, 1, 0
)
,
(
'2704', 'OLAF REPROGRAFIA BIBL.SCOLASTICHE', '5183', '', 0, '', 'M', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2704', '27', 0, 'OLAF REPR.UN.', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 0, 1, 1, 0
)
,
(
'2705', 'OLAF REPROGRAFIA ENTI TERRITORIALI', '5183', '', 0, '', 'M', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2705', '27', 0, 'OLAF ENTI.TER.', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 0, 1, 1, 0
)
,
(
'2706', 'OLAF REPROGRAFIA BIBLIOTECHE', '5183', '', 0, '', 'M', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2706', '27', 0, 'OLAF REPR.BIBL', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 0, 1, 1, 0
)
,
(
'2711', 'NON UTILIZZARE', '5183', '', 0, '', 'M', 'S', 'a', 'Y', 1, '03', 1, 'M', 0, '2711', '27', 0, 'NON UTILIZZARE', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 0, 1, 1, 0
)
,
(
'2712', 'OLAF REPROGRAFIA INTERESSI', '5183', '', 0, '', 'M', 'S', 'a', 'Y', 1, '03', 1, 'M', 0, '2712', '27', 0, 'OLAF REPR.INT.', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2713', 'OLAF REPROGRAFIA UNIVERSITA', '5183', '', 0, '', 'M', 'S', 'a', 'Y', 1, '03', 1, 'M', 0, '2713', '27B', 0, 'OLAF REPR.UN.', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2714', 'OLAF REPROGRAFIA BIBL.SCOLASTICHE', '5183', '', 0, '', 'M', 'S', 'a', 'Y', 1, '03', 1, 'M', 0, '2714', '27', 0, 'OLAF REPR.B.S.', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2715', 'OLAF REPROGRAFIA ENTI TERRITORIALI', '5183', '', 0, '', 'M', 'S', 'a', 'Y', 1, '03', 1, 'M', 0, '2715', '27', 0, 'OLAF REPR.E.T.', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2716', 'OLAF REPROGRAFIA BIBLIOTECHE', '5183', '', 0, '', 'M', 'S', 'a', 'Y', 1, '03', 1, 'M', 0, '2716', '27', 0, 'OLAF REPR.BIBL', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2717', 'OLAF REPROGRAFIA UNIVERSITA\' ESTERE', '0', '', 0, '', '', 'S', 'a', 'Y', 1, '03', 0, '', 0, '2717', '27', 0, 'OLAF UNIV ESTERE', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2721', 'OLAF REPROGRAFIA PUNTI DI RIPRODUZIONE', '5183', '', 0, '', 'M', 'S', 'a', 'Y', 1, '03', 1, 'M', 0, '2721', '27', 0, 'REPR.PUNTI RIP', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2733', 'UNIVERSITA\' CRUI', '5183', '', 0, '', 'M', 'S', 'a', 'Y', 0, '03', 0, 'M', 0, '2733', '27B', 0, 'OUNIVERSITA\' CRUI', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2734', 'FONDO SIAE/CRUI', '5183', '', 0, '', 'M', 'S', 'a', 'Y', 0, '03', 0, 'M', 0, '2734', '27B', 0, 'FONDO SIAE/CRUI', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2817', 'OLAF EMITT.PRIV.RADIO', '5183', '', 0, '', 'M', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2817', '28', 0, 'OLAF EMITT RAD', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2818', 'OLAF EMITT.PRIV.TV ETERE', '5183', '', 0, '', 'M', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2818', '28', 0, 'OLAF EMITT.TV', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2819', 'OLAF EMITT.PRIV.TV CAVO', '5183', '', 0, '', 'M', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2819', '28', 0, 'OLAF EM.TVCAVO', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'282', 'IS PROV.CONN. ORD.', '0', '0902', 3, '0292', 'I', 'S', 'a', 'Y', 1, '03', 1, 'E', 0, '282', '02', 0, 'IS ORD.PR.CONN', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'2830', 'OLAF D.R.M. FONO', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '2830', '28', 0, 'OLAF DRM FONO', 0, NULL, '', '', 0, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2831', 'OLAF VIDEOGR.OPERE LETTER.', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '2831', '28', 0, 'OLAF VID.OP.L.', 0, NULL, '', '', 0, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2832', 'OLAF VIDEOG.OPERE LETT.EXPORT', '0', '', 0, '', 'I', 'N', 'd', 'Y', 1, '03', 1, '', 0, '2832', '28', 0, 'OLAF VID.EXP.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2833', 'OLAF VIDEOGR. ARTI FIGURATIVE', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '2833', '28', 0, 'OLAF VID.FIG.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2834', 'OLAF VIDEOGR.ARTI FIGUR.EXPORT', '0', '', 0, '', 'I', 'S', 'l', 'Y', 1, '04', 1, '', 0, '2834', '28', 0, 'OLAF VID.F.EXP', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2835', 'OLAF MULTIMEDIALE OPERE LETT.', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '2835', '28', 0, 'OLAF MUL.OP.L.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2836', 'OLAF MULTIMEDIALE ARTI FIGUR.', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '2836', '28', 0, 'OLAF MULT.FIG.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2837', 'OLAF DIRITTI EDITORIALI OP.MULTIMEDIALI', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, '', 0, '2837', '28', 0, 'OLAF DIR.ED.MU', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2851', 'OLAF PICCOLI DIRITTI LETTERARI', '5183', '1818', 0, '1818', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '2851', '28', 0, 'OLAF PDL', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'2852', 'OLAF DIRIT.RIPR.ARTI FIG.', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '2852', '28', 0, 'OLAF RIPR.FIG.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2853', 'OLAF DIRIT.RIPR.ARTI FIG EXP.', '0', '', 0, '', 'I', 'S', 'l', 'Y', 1, '04', 1, '', 0, '2853', '28', 0, 'OLAF R.FIG.EXP', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2867', 'OLAF DIVERSI ART. 72 III C.', '0', '', 0, '', 'I', 'S', 'm', 'Y', 1, '03', 1, '', 0, '2867', '28', 0, 'OLAF DIV.AR.72', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'2895', 'OLAF DIRITTO DI SEGUITO RETE TERR.', '0', '', 0, '', '', 'S', 'a', 'Y', 0, '03', 0, 'M', 0, '2895', '27', 0, 'OLAF DIR.SEGUI', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'292', 'INT.MORA IMP.SPETT.ORDINARIO', '0', '', 0, '', 'S', 'S', 'a', 'Y', 1, '03', 1, 'a', 0, '292', '02', 0, 'IS ORD.INT.MOR', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'2952', 'OLAF BOLLATURA FRONTESPIZI', '0', '', 0, '', 'I', 'S', 'c', 'Y', 1, '03', 1, '', 0, '2952', '29', 0, 'OLAF BOLL.FRON', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'303', 'IMPOSTA SPETTACOLI SPORT', '0', '0903', 3, '0393', 'I', 'S', 'a', 'Y', 1, '03', 1, 'E', 0, '303', '02', 0, 'IS SPORT', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3030', 'DONAZIONE TELETHON', '0', '', 0, '', '', 'N', 'a', 'Y', 1, '03', 1, 'a', 0, '3030', '46', 0, 'DON.TELETHON', 1, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3035', 'VINCITE PROVE DI GIOCO', '0', '', 0, '', '', 'N', 'a', 'Y', 1, '03', 1, 'a', 0, '3035', '46', 0, 'VINCITE PROVE DI GIOCO', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3053', 'QUOTE ANICA', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3053', '30', 0, 'QT ANICA', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3101', 'CESSIONE PUBBLICAZIONI S.C.D.C.', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, '', 0, '5486', '54', 0, 'CESS.PUB.SCDC', 0, '2013-12-31 00:00:00', '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3120', 'ENTRATE VARIE  NON SOGGETTE A IVA', '0', '', 0, '', 'O', 'Y', 'a', 'Y', 1, '03', 1, '', 0, '3120', '54B', 1, 'ENTRATE VARIE NO IVA', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3130', 'REC.SPESE ASS.INSOLUTI', '0', '', 0, '', 'O', 'Y', 'a', 'Y', 1, '03', 1, '', 0, '3120', '54B', 1, 'REC.SPESE ASS.INSOLUTI', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3148', 'OPERAZIONI VARIE ESENTI', '0', '', 0, '', 'O', 'S', 's', 'Y', 1, '02', 1, '', 0, '3148', '54B', 1, 'OPER.VARIE ES.', 0, '2013-12-31 00:00:00', '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3150', 'RIENTRI DA CICLO PASSIVO', '0', '', 0, '', 'O', 'N', 'a', 'Y', 1, '03', 1, '', 0, '3150', '54', 0, 'RIENT.CIC.PASS', 0, '2013-12-31 00:00:00', '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3153', 'QUOTA FIPE-SIB -  P.A.', '0', '', 0, '', '', '', '', 'Y', 1, '', 0, 'M', 0, '0', '31', 0, 'DIR.FIPE-SIB P.A.', 0, NULL, '', '', 0, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3154', 'QUOTA FIPE BALLO', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3154', '31', 0, 'QT. FIPE BALLO', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3155', 'QUOTA FIPE - J.B. S.M.', '0', '', 0, '', 'P', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3155', '31', 0, 'DIR.FIPE JB SM', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3156', 'QUOTA FIPE TRATTENIMENTI MUSICALI SENZA BALLO', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3156', '31', 0, 'QT. FIPE TRATT. SN BALLO', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3157', 'QUOTA FIPE-SILB ARTE VARIA', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3157', '31', 0, 'FIPE-SILB A.V.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3158', 'QUOTA FIPE-SIB -  J.B. S.M.', '0', '', 0, '', '', '', '', 'Y', 1, '', 0, 'M', 0, '0', '31', 0, 'DIR.FIPE-SIB JB SM', 0, NULL, '', '', 0, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3159', 'SILB QUOTA ASSOCIATIVA STRAORDINARIA', '0', '', 0, '', 'I', 'N', 'b', 'Y', 1, '03', 1, 'Q', 0, '3159', '46', 0, 'QT. SILB STRAORD', 0, NULL, '', '', 0, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3160', 'OP. VARIE 4%', '6405', '', 0, '', 'O', 'N', 'v', 'Y', 1, '01', 1, '', 0, '3160', '54B', 1, 'OP. VARIE 4%', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3161', 'SOC. EST. UE REVERSE CHARGE OP. VARIE', '0', '', 0, '', '', 'S', 'u', 'Y', 1, '04', 1, '', 0, '3161', '54B', 0, 'VAT UE', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3162', 'SOC. EST. EXTRA UE REVERSE CHARGE OP. VARIE', '0', '', 0, '', '', 'S', 'y', 'Y', 1, '04', 1, '', 0, '3162', '54B', 0, 'VAT EXTRA UE', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3163', 'OP. VARIE NON IMPONIBILI', '0', '', 0, '', 'O', '', 'l', 'Y', 1, '04', 1, '', 0, '3163', '54B', 1, 'OP. VARIE N.I.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3164', 'OP. VARIE 22%', '5183', '', 0, '', 'O', 'N', 'r', 'Y', 1, '01', 1, '', 0, '3164', '54B', 1, 'IVA P. 22% REN', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3165', 'OPERAZIONI VARIE AL 10%', '6403', '', 0, '', 'O', 'N', 'p', 'Y', 1, '01', 1, '', 0, '3165', '54B', 1, 'OP. VARIE 10%', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3166', 'OPERAZIONI VARIE AL 16%', '6406', '', 0, '', 'O', 'N', 'w', 'Y', 1, '01', 1, '', 0, '5486', '54', 0, 'OP. VARIE 16%', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3168', 'OPER. ESCLUSE BASE IMPON. ART. 15', '0', '', 0, '', 'O', 'S', 'k', 'Y', 1, '03', 1, '', 0, '5486', '54', 0, 'OP.ESCLUSE B.I', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3170', 'ENTRATE O/C VARIE NON SOGGETTE', '0', '', 0, '', 'O', 'N', 'a', 'N', 1, '03', 1, '', 0, '3170', '54B', 1, 'ENTRATE O/C NX', 0, NULL, '', '', 1, 0, 1, 1, 0, '', 0, 1, 1, 1, 0
)
,
(
'3172', 'RECUPERO SPESE LEGALI NON SOGGETTE A IVA', '6404', '', 0, '', 'O', 'S', 'a', 'Y', 0, '03', 0, '', 0, '3172', '54B', 0, 'REC.SP.LEG.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3173', 'RECUPERO SPESE LEGALI SOGGETTE A IVA', '5183', '', 0, '', 'O', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3173', '54B', 1, 'REC.SP.LEG.IVA', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3174', 'SPESE LEG. RECUP. RATE ENCICLOPEDICHE', '6404', '', 0, '', 'O', 'N', 'g', 'Y', 1, '01', 1, '', 0, '5486', '54', 0, 'SP.LEG.REC.ENC', 0, '2013-12-31 00:00:00', '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3175', 'INCASSI SIS SPORT.AUT.SEDI', '0', '', 0, '', 'I', 'N', 'a', 'Y', 1, '03', 1, '', 0, '3175', '54B', 1, 'SIS SPORT.A.S.', 0, '2013-12-31 00:00:00', '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3176', 'CONTROLLO FATTURAZIONE ACCENTRATA', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 0, '', 0, '3176', '54B', 1, 'CONTR.FATT.ACC', 0, NULL, '', '', 1, 0, 1, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3177', 'INTERESSI DI MORA NON SOGGETTI AD IVA', '0', '', 0, '', 'O', 'S', 'a', 'Y', 1, '03', 0, 'S', 0, '3177', '54B', 1, 'INT.MORA NXIVA', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3180', 'RIMB.SP.LEG. ATTIVITA\' DIVIS.INT. SOGG.IVA', '5183', '', 0, '', 'O', 'N', 'g', 'Y', 1, '01', 1, '', 0, '3180', '54B', 1, 'RIM.SP.LEG.IVA', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3181', 'RIMB.SP.LEG. ATTIVITA\' DIVIS.INT. NON SOGG.IVA', '0', '', 0, '', 'O', 'S', 'a', 'Y', 0, '03', 0, '', 0, '3181', '54B', 1, 'RIM.SP.LG.NOIV', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3190', 'REINTEGRO GARANZIA FIPE', '0', '', 0, '', 'I', 'N', 'a', 'N', 1, '03', 1, '', 0, '3190', '31', 0, 'REINTEGRO GARANZIA FIPE', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3232', 'QUOTA FIPE-ARAG', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3232', '32', 0, 'QT FIPE-ANAG', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3301', 'D.R.F. PERMESSO GEN.ANTICIPI', '5183', '', 0, '2020', 'E', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3301', '33', 0, 'DRF P.GEN.ANT.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3302', 'D.R.F. PERMESSO GEN. ANTIC ESPORT.ABIT', '0', '', 0, '', 'I', 'S', 'l', 'Y', 1, '04', 1, '', 0, '3302', '33', 0, 'DRF P.G.ANT.EX', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3303', 'D.R.F. PUBBL.PERIOD.ANTICIPI', '5183', '', 0, '', 'E', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3303', '33', 0, 'DRF PUBBL.ANT.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3311', 'D.R.F. PERMESSO GEN.SALDO RENDIC', '5183', '', 0, '', 'E', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3311', '33', 0, 'DRF P.GEN.SAL.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3312', 'D.R.F. PERMESSO GEN.SALDO REND.ESP.AB', '0', '', 0, '', 'I', 'S', 'l', 'Y', 1, '04', 1, '', 0, '3312', '33', 0, 'DRF PG.SAL.EX.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3313', 'D.R.F. PUBBL.PERIOD.SALDO REND.', '5183', '', 0, '', 'E', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3313', '33', 0, 'DRF PUBB.SALDO', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3337', 'MUSICA MFV COPIE LAVORO DJ', '5183', '', 0, '', '', 'N', 'd', 'Y', 0, '01', 0, '', 0, '3337', '25C', 0, 'MFV COPIE DJ', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3342', 'INTERESSI VIDEO', '0', '', 0, '', 'I', 'S', 'k', 'Y', 1, '03', 1, '', 0, '3342', '33', 0, 'INTERES. VIDEO', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3355', 'MUSICA IMPORTAZIONI AUDIOVISIVI', '5183', '', 0, '', 'E', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3355', '33', 0, 'IMPOR.AUDIOVIS', 0, NULL, '', '', 0, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3356', 'DIRITTI DI RIPRODUZIONE FONOGRAFICA', '5183', '', 0, '', 'E', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3356', '33', 0, 'DIR.RIPR.FON.', 0, NULL, '', '', 0, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3357', 'D.R.F. IMPORT', '5183', '', 0, '', 'E', 'N', 'd', 'Y', 1, '01', 1, 'd', 0, '3357', '33', 0, 'D.R.F. IMPORT', 0, NULL, '', '', 0, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3358', 'D.R.F. EXPORT', '0', '', 0, '', 'I', 'S', 'l', 'Y', 1, '04', 1, '', 0, '3358', '33', 0, 'D.R.F. EXPORT', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3359', 'DRM PROGRAMMI RADIOFONICI', '5183', '', 0, '', 'E', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3359', '33', 0, 'DRM PROGR.RR', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3361', 'DRM PROGRAMMI RADIOFONICI EXPORT', '0', '', 0, '', 'I', 'S', 'l', 'Y', 1, '04', 1, '', 0, '3361', '33', 0, 'DRM PROG.RR EX', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3362', 'DRM PROGRAMMI TELEVISIVI', '5183', '', 0, '', 'E', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3362', '33', 0, 'DRM PROGR. TV', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3364', 'DRM PROGRAMMI TELEVISIVI EXPORT', '0', '', 0, '', 'I', 'S', 'l', 'Y', 1, '04', 1, '', 0, '3364', '33', 0, 'DRM PROG.TV EX', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3365', 'DRM VIDEOGRAMMI', '5183', '', 0, '', 'E', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3365', '33', 0, 'DRM VIDEOGRAM.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3366', 'DRM VIDEOMUSICALI', '5183', '', 0, '', 'E', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3366', '33', 0, 'DRM VIDEOMUS.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3367', 'DRM VIDEOGRAMMI EXPORT', '0', '', 0, '', 'I', 'S', 'l', 'Y', 1, '04', 1, '', 0, '3367', '33', 0, 'DRM VIDEOG.EXP', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3368', 'DRM VIDEOGRAMMI FILM', '5183', '', 0, '', 'E', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3368', '33', 0, 'DRM VID. FILM', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3369', 'DRM VIDEOMUSICALI EXPORT', '0', '', 0, '', 'I', 'S', 'l', 'Y', 1, '04', 1, '', 0, '3369', '33', 0, 'DRM VD.MUS.EXP', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3370', 'DRM VIDEOGRAMMI FILM EXPORT', '0', '', 0, '', 'I', 'S', 'l', 'Y', 1, '04', 1, '', 0, '3370', '33', 0, 'DRM VD.FILM EX', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3371', 'DRM NOLEGGIO VIDEOGRAMMI AD USO PRIVATO', '5183', '', 0, '', 'E', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3371', '33', 0, 'DRM NOLEG.VID.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3374', 'DRM VIDEOGRAMMI VARI', '5183', '', 0, '', 'E', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3374', '33', 0, 'DRM VID. VARI', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3375', 'DRF IMPORT.DA ESPORT.ABIT', '0', '', 0, '', 'I', 'S', 'l', 'Y', 1, '04', 1, '', 0, '3375', '33', 0, 'DRF IMP.EX.AB.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3380', 'CD-ROM DIDATTICO', '5183', '', 0, '', 'E', 'N', 'd', 'Y', 1, '01', 1, 'd', 0, '3380', '33', 0, 'CD-ROM DIDATT.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3381', 'CD-ROM MUSICALE', '5183', '', 0, '', 'E', 'N', 'd', 'Y', 1, '01', 1, 'd', 0, '3381', '33', 0, 'CD-ROM MUSIC.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3385', 'DVD FILM', '5183', '', 0, '', 'E', 'N', 'd', 'Y', 1, '01', 1, 'd', 0, '3385', '33', 0, 'DVD FILM', 0, NULL, '', '', 0, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3386', 'DVD MUSICALE', '5183', '', 0, '', 'E', 'N', 'd', 'Y', 1, '01', 1, 'd', 0, '3386', '33', 0, 'DVD MUSICALE', 0, NULL, '', '', 0, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3387', 'MUS. PRIMO PIANO SUPPORTI VIDEOGR.', '5183', '', 0, '', 'E', 'N', 'd', 'Y', 1, '01', 1, 'd', 0, '3387', '33', 0, 'MUS. SUPPORTI VIDEOGR.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3399', 'INTERESSI D.R.F.', '0', '', 0, '', 'I', 'S', 'k', 'Y', 1, '03', 1, '', 0, '3399', '33', 0, 'INTERESSI DRF', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3457', 'VENDITA BIGLIETTI TIPO N', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3457', '34', 0, 'VEN BIGL TIPO N', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3458', 'VENDITA FASCICOLI MOD. 1026', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3458', '34', 0, 'VEN FASC 1026', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3459', 'VENDITA BIGLIETTI TIPO AUTORALE', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3459', '34', 0, 'VEN. BIGL. AUT.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3460', 'VENDITA MOD. SD1 30x2', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3460', '34', 0, 'VEN. SD1 30x2', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3461', 'VENDITA MOD. SD1 1x2', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3461', '34', 0, 'VEN. SD1 1x2', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3462', 'VENDITA MOD. C2 1x3', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3462', '34', 0, 'VEN. C2 1x3', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3463', 'VENDITA MOD. C1 1x3', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3463', '34', 0, 'VEN. C1 1x3', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3559', 'FITTI IMPIANTI SPORTIVI', '5183', '', 0, '', 'I', 'N', 'f', 'Y', 1, '01', 1, '', 0, '3559', '35', 0, 'FITTI IM.SPORT', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3560', 'CONTRIBUTO ILLUMINAZIONE CAMPI SPORTIVI', '0', '', 0, '', 'm', 'S', 'a', 'Y', 1, '03', 1, '', 0, '3560', '35', 0, 'CONT.ILLUM.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3659', 'PUNZONATURA BIGLIETTI', '5183', '', 0, '', 'I', 'N', 'g', 'Y', 1, '01', 1, '', 0, '3659', '36', 0, 'PUNZONAT.BIGL.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3720', 'MULTIMED.SITI WEB PERIFERIA', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3720', '', 0, 'SITI WEB PERIF', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3721', 'MULTIMED.SITI WEB SPETT.A PAGAMENTO', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3721', '', 0, 'SITI WEB SPET', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3760', 'DIRITTI AMMINISTRATIVI DI PROCEDURA', '5183', '', 0, '', 'I', 'N', 'g', 'Y', 1, '01', 1, '', 0, '3760', '37', 0, 'DIR.AMM.PROC.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3770', 'DIRITTI DI SEGRETERIA DIVISIONE MUSICA', '5183', '', 0, '', 'I', 'N', 'g', 'Y', 1, '01', 1, '', 0, '3770', '37', 0, 'DIR.S.DIV.MUS.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3790', 'DIRITTI DI SEGRETERIA PER RIL.FOTOCOPIE', '5183', '', 0, '', 'I', 'N', 'g', 'Y', 1, '01', 1, '', 0, '3790', '37', 0, 'DIR.S.RIL.FOT.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3791', 'SERVIZ. VIDIMAZIONE NORMALE', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, 'g', 0, '3791', '37', 0, 'SERV.VID.NORM.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3792', 'SERVIZ. VIDIMAZIONE CD-ROM', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, 'g', 0, '3792', '37', 0, 'SERV.VIDIM.CD', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3793', 'SERVIZ. VIDIMAZIONE SUPP.FONO', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, 'g', 0, '3793', '37', 0, 'SERV.VID.FONO', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3794', 'SERVIZ. VIDIMAZIONE DVD', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, 'g', 0, '3794', '37', 0, 'SERV.VID.DVD', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3795', 'CONTRASS.SUPP.GRATUITI', '0', '', 0, '', 'I', 'S', 'c', 'Y', 1, '03', 1, 'g', 0, '3795', '26', 0, 'SUP.GRAT', 0, NULL, '', '', 0, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3796', 'CONTRASS.SUPP.PAGAM.', '0', '', 0, '', 'I', 'S', 'c', 'Y', 1, '03', 1, 'g', 0, '3796', '26', 0, 'SUPP.PAG.', 0, NULL, '', '', 0, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3797', 'DIRITTO FISSO(EX.ART.7 DPCM 338 11.7.01)', '0', '', 0, '', 'I', 'S', 'c', 'Y', 1, '03', 1, 'g', 0, '3797', '26', 0, 'DIR.FIS.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3798', 'TARIFFA ORARIA(EX.ART.7 DPCM 338)', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, 'g', 0, '3798', '26', 0, 'TAR.ORARIA', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'383', 'IS PROV.CONN.SPORT', '0', '0903', 3, '0393', 'I', 'S', 'a', 'Y', 1, '03', 1, 'E', 0, '383', '02', 0, 'IS SPORT CONN.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3831', 'Contributo Associativo a AGIS Cinema', '0', '', 0, '', '', 'N', 'x', 'Y', 1, '03', 0, 'Q', 0, '3831', '38', 0, 'Contr Assoc AGIS Cinema', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 0, 0
)
,
(
'3836', 'Contributo Associativo ACEC', '0', '', 0, '', '', 'N', 'x', 'Y', 1, '03', 0, 'Q', 0, '3836', '38', 0, 'Contr Assoc ACEC', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 0, 0
)
,
(
'3839', 'Contributo Associativo ANEM', '0', '', 0, '', '', 'N', 'x', 'Y', 1, '03', 0, 'Q', 0, '3839', '38', 0, 'Contr Assoc ANEM', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 0, 0
)
,
(
'3840', 'QUOTA AGIS TEATRO', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3840', '38', 0, 'QUOTA AGIS TEATRO', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3850', 'QUOTA CONVENZIONE API', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3850', '38', 0, 'QT API', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3851', 'QUOTA ASSICURATIVA A.L.E.F. ASSO INTRATTENIMENTO', '0', '', 0, '', '', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3851', '27B', 0, 'QT. A.L.E.F.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3854', 'QUOTA ASSOCIATIVA AICA', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3854', '38', 0, 'QT ASS. AICA', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3860', 'QUOTE ASSOCIAZIONE AGIS FEDERFESTIVAL', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3860', '38', 0, 'QT AGIS FEDERF', 0, '2016-02-25 00:00:00', '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3861', 'QUOTA AGIS CINEMA', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3861', '38', 0, 'QT AGIS CINEMA', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3862', 'QUOTE ASSOCIAZIONE AGIS AVANSPETTACOLO', '0', '', 0, '', 'I', 'S', 'x', 'Y', 1, '03', 1, 'Q', 0, '3862', '38', 0, 'QT AGIS AVANSP', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3863', 'QUOTA AGIS TEATRO', '0', '', 0, '', 'I', 'S', 'x', 'Y', 1, '03', 1, 'Q', 0, '3863', '38', 0, 'QT AGIS TEATRO', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3864', 'QUOTE ASSOCIAZIONE AGIS ANESV', '0', '', 0, '', 'I', 'S', 'x', 'Y', 1, '03', 1, 'Q', 0, '3864', '38', 0, 'QT AGIS ANESV', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3865', 'QUOTA AGIS SAPAR J.B.', '0', '', 0, '', 'I', 'S', 'x', 'Y', 1, '03', 1, 'Q', 0, '3865', '38', 0, 'QT SAPAR J.B.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3866', 'QUOTE ACEC', '0', '', 0, '', 'I', 'S', 'x', 'Y', 1, '03', 1, 'Q', 0, '3866', '38', 0, 'QT ACEC', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3867', 'QUOTE AGIS ENC', '0', '', 0, '', 'I', 'S', 'x', 'Y', 1, '03', 1, 'Q', 0, '3867', '38', 0, 'QT AGIS ENC', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3868', 'QUOTE ASSOCIAZIONE AGIS SAPAR', '0', '', 0, '', 'I', 'S', 'x', 'Y', 1, '03', 1, 'Q', 0, '3868', '38', 0, 'QT AGIS SAPAR', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3869', 'QUOTA AGIS REGIONALE INTEGRATIVA', '0', '', 0, '', 'I', 'S', 'x', 'Y', 1, '03', 1, 'Q', 0, '3869', '38', 0, 'QT AGIS REG. INTEGR.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3870', 'QUOTE ASSOCIAZIONE AGIS NAZIONALE ACEC', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3870', '38', 0, 'QT ACEC NAZ. AGIS', 0, '2016-02-25 00:00:00', '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3871', 'QUOTE AGIS MUSICA', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3871', '38', 0, 'QT AGIS MUSICA', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3872', 'QUOTA CONFCOMMERCIO', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3872', '38', 0, 'QT CONFCOMMER.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3873', 'QUOTA FISMO', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3873', '38', 0, 'QT. FISMO', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3874', 'QUOTA FEDERALBERGHI - TRATTENIMENTI E SPETTACOLI', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3874', '38', 0, 'QT FEDERALBER.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3875', 'QUOTA ASSOCIATIVA FEDERALBEGRHI SM', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3875', '38', 0, 'QT. FEDER. SM', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3876', 'QUOTA CONFESERCENTI', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3876', '38', 0, 'QT CONFESERC.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3877', 'QUOTA F.R.T.', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3877', '38', 0, 'QT F.R.T.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3878', 'QUOTA FAITA TRATTENIMENTI E SPETTACOLI', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3878', '38', 0, 'QT FAITA TRATT', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3879', 'QUOTA FAITA SM', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3879', '38', 0, 'QT. FAITA SM.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3880', 'QUOTA FIEPeT CONFESERC. S.M.', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3880', '38', 0, 'QT CONFESER.SM', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3881', 'QUOTA FIEPeT CONFESERC. BALLO', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3881', '38', 0, 'QT CONFES.BAL.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3882', 'QUOTA L.N.C.', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3882', '38', 0, 'QT L.N.C.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3883', 'QUOTA CONFARTIGIANATO', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3883', '38', 0, 'QT CONFARTIG.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3884', 'QUOTA ANCRA', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3884', '38', 0, 'QT ANCRA', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3885', 'QUOTA FIEPeT TRATTENIMENTI MUSICALI SENZA BALLO', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3885', '38', 0, 'QT FIEPeT TRATT. MUS. SN. BALLO', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3886', 'QUOTA TERZO POLO', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3886', '38', 0, 'QT TERZO POLO', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3887', 'CONTRIBUTI ASSIC. AGIS/ANEC', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3887', '38', 0, 'CONT.AGIS/ANEC', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3888', 'QUOTA ASSOMUSICA', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3888', '38', 0, 'QT ASSOMUSICA', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3889', 'QUOTA ASSHOTEL', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3889', '38', 0, 'QT ASSOHOTEL', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3890', 'QUOTA FISPE', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3890', '38', 0, 'QT FISPE', 0, '2016-02-25 00:00:00', '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3891', 'QUOTA UNPLI', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3891', '38', 0, 'QT UNPLI', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3892', 'QUOTA AGIS TEATRO FITA', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3892', '38', 0, 'QT AGIS TEAT. FITA', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3893', 'AGIS-ZURIGO ASSICURAZIONI', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3893', '38', 0, 'AGIS ZUR.ASSIC', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3894', 'QUOTA CIDEC', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3894', '38', 0, 'QT CIDEC', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3895', 'QUOTA ASSO INTRATTENIMENTO', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3895', '38', 0, 'QT ASSOINTRATT', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3896', 'QUOTA A.N.C.', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3896', '38', 0, 'QT A.N.C.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3897', 'QUOTA ACEC REGIONALE', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3897', '38', 0, 'QT ACEC REG.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3898', 'QUOTA ACEC TEATRO', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3898', '38', 0, 'QT ACEC TEATRO', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3899', 'QUOTA ASSOCIATIVA ANNUALE ASSOINTRATTENIMENTO', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '3899', '38', 0, 'QT ANN. ASSOINTR.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3903', 'APP.REG.AUDIO', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3903', '39', 0, 'APP.REG.AUDIO', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3905', 'SUPPORTI VIDEO', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3905', '39', 0, 'SUPPORTI VIDEO', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3910', 'SUPPORTI AUDIO', '0', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3910', '39', 0, 'SUPPORTI AUDIO', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3913', 'INTER.COMP.COPIA.PRIV.APP.REG.', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3913', '39', 0, 'INT.COPIA PRIV', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3920', 'INT.COMP.COPIA.PRIV.AUDIO.', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3920', '39', 0, 'INT.COPIA PRIV', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3923', 'INT.MOR.PEN.COPIA.PRIV.APP.REG.', '0', '', 0, '', 'I', 'S', 'k', 'Y', 1, '03', 1, '', 0, '3923', '39', 0, 'INT.MOR.PEN.CP', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'393', 'INT.MORA IMP.SPETT.SPORT', '0', '', 0, '', 'S', 'S', 'a', 'Y', 1, '03', 1, 'a', 0, '393', '02', 0, 'IS SPORT MORA', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'3930', 'INT.MOR.PEN.COPIA.PRIV.AUDIO', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, '', 0, '3930', '39', 0, 'INT.MOR.PEN.CP', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3933', 'RIMB.SP.LEG.COPIA PRIV.APP.REG', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3933', '39', 0, 'RIMB.SP.LEG.CP', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3940', 'RIMB.SPESE LEG.COP.PRIV.AUDIO', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3940', '39', 0, 'RIMB.SPESE LEG', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3943', 'RIMB.SP.VER.COP.PRIV.APP.REG.', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3943', '39', 0, 'RIMB.SP.VER.CP', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3950', 'RIMB.SPESE.VER.COP.PRIV.AUDIO', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3950', '39', 0, 'RIMB.SP.VER.CP', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3955', 'INTER.COMPENS.COPIA PRIV.VIDEO', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3955', '39', 0, 'INT.COMP.COP.V', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3965', 'INT.MOR.PEN.COPIA PRIV.VIDEO', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, '', 0, '3965', '39', 0, 'INT.MOR.PEN.CP', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3975', 'RIMB.SPESE LEG.COPIA PRIV.VIDEO', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3975', '39', 0, 'RIMB.SP.LEG.CP', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3985', 'RIMB.SP.VER.SIAE COPIA PRIV.VIDEO', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3985', '39', 0, 'R.SP.VER.SIAE', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3990', 'PRODUCTION MUSIC DRF', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3990', '39', 0, 'PROD.MUS.DRF', 0, NULL, '', '', 0, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3991', 'PRODUCTION MUSIC DRF', '0', '', 0, '', 'I', 'S', 'l', 'Y', 1, '04', 1, '', 0, '3991', '39', 0, 'PROD.MUS.DRF', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3992', 'VIDEO CERIMONIE NUZIALI SOGGETTO A IVA', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3992', '39', 0, 'VID.CER.NUZ.IV', 0, NULL, '', '', 0, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3993', 'VIDEO CERIMONIE NUZIALI NON IMPONIBILE', '5183', '', 0, '', 'I', 'S', 'l', 'Y', 1, '04', 1, '', 0, '3993', '39', 0, 'VID.CER.NUZ.NI', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3994', 'MUSICA DRF FORFETTARI PRODUCTION MUSIC', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '3994', '39', 0, 'FORF.MUS.DRF', 0, NULL, '', '', 0, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'3995', 'MUSICA DRF FORFETTARI PRODUCTION MUSIC', '0', '', 0, '', 'V', 'S', 'l', 'Y', 1, '04', 1, '', 0, '3995', '39', 0, 'FORF.MUS.DRF', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'404', 'IMPOSTA SPETTACOLI SCOMMESSE IN GENERE', '0', '0904', 3, '0494', 'I', 'S', 'a', 'Y', 1, '03', 1, 'E', 0, '404', '02', 0, 'IS SCOMM.GENER', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'4072', 'QUOTE ASSICURAZIONE FILM', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '4072', '40', 0, 'QT ASSIC.FILM', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'4171', 'QUOTE ASSIC.NE LOCALI MULTISALA', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'a', 0, '4171', '41', 0, 'QT ASS.LOC.MUL', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'4173', 'QUOTE ASSICURAZIONE LOCALI', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '4173', '41', 0, 'QT ASS.LOCALI', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'4174', 'CINEMA EQUO COMPENSO-SUPPORTI VIDEOGRAF.', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '4174', '27B', 0, 'EQ.COMP.VIDEOG', 0, NULL, '', '', 0, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'4175', 'CINEMA EQUO COMPENSO-IMPORT', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '4175', '27B', 0, 'EQ.COMP.IMPORT', 0, NULL, '', '', 0, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'4375', 'ADDIZIONALE ERARIALE', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, '', 0, '4375', '43', 0, 'ADDIZ.ERARIALE', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'444', '', '0', '', 0, '', '', '', '', '', 1, '', 0, '', 0, '0', '', 0, '', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'4476', 'CONTRIBUTI E LIBERALITA', '5183', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, 'a', 0, '4476', '44', 0, 'CONTRIBUTI LIB', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'4477', 'FONDO RICOSTR.TEATRO COMUN.L\'AQUILA', '5183', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, 'a', 0, '4477', '44', 0, 'RIC.TEATRO AQU', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'4478', 'RACCOLTA FONDI PRO MANDATARI TERREMOTATI DELL\'EMILIA ROMAGNA', '0', '', 0, '', '', 'N', 'x', 'Y', 0, '03', 0, 'a', 0, '4478', '46', 0, 'RACC. MAND. TERR. EMROM.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'4577', 'QUOTE SINDACALI MANDATARI-A', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 18, 'Q', 0, '4577', '45', 0, 'QT SIND. A', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'4578', 'QUOTE SINDACALI MANDATARI-B', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '4578', '45', 0, 'QT SIND. B', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'4579', 'QUOTE SINDACALI MANDATARI-C', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '4579', '45', 0, 'QT SIND. C', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'4580', 'QUOTE SINDACALI MANDATARI-D', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'Q', 0, '4580', '45', 0, 'QT SIND. D', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'4646', 'RIMBORSO SPESE BANCARIE', '0', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, '', 0, '4646', '46', 0, 'RIMB.SPE.BANC.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'4686', 'NA-FOTORIPRODUZIONE TESTI BIBLIOTECA CAPRIOLO', '5183', '', 0, '', 'I', 'N', 'd', 'N', 1, '01', 1, '', 0, '4686', '46', 0, 'NA-FOTORIPR. BIBL. CAPRIOLO', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'4701', 'DEPOSITI CAUZIONALI ONLINE', '0', '', 0, '', 'I', 'S', 'a', 'N', 1, '03', 1, 'E', 0, '4701', '47', 0, 'DEPOSITI CAUZIONALI ONLINE', 0, NULL, '', '', 1, 1, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'4702', 'DEPOSITI CAUZIONALI ONLINE', '0', '', 0, '', 'I', 'S', 'a', 'N', 1, '03', 1, 'U', 0, '4702', '47', 0, 'DEPOSITI CAUZIONALI ONLINE', 0, NULL, '', '', 1, 1, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'4779', 'DEPOSITI CAUZIONALI', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, 'D', 0, '4779', '47', 0, 'DEPOS.CAUZION.', 0, NULL, '', '', 1, 1, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'4780', 'GIRO DEPOSITI CAUZ. IN D.G.', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, 'D', 0, '4780', '54B', 1, 'GIRO.DEPOS.DG.', 0, NULL, '', '', 1, 1, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'4787', 'DEPOSITI CAUZIONALI', '0', '', 0, '', 'I', 'S', 'a', 'N', 1, '03', 1, 'D', 0, '4787', '2', 0, 'RIMB.DEP.CAUZ.', 0, NULL, '', '', 1, 1, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'4848', 'RITENUTA ACCONTO SU RIVALSA', '0', '', 0, '', 'I', 'N', 'a', 'Y', 1, '03', 1, '', 0, '4848', '48', 0, 'RIT.ACC.SU RIV', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'4880', 'RITENUTA D\'ACCONTO', '0', '', 0, '', 'I', 'N', 'a', 'Y', 1, '03', 1, '', 0, '4880', '48', 0, 'RITENUTA ACC.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'4908', 'CONGUAGLIO POSITIVO DI CASSA', '0', '', 0, '', 'I', 'N', 'a', 'Y', 1, '03', 1, '', 0, '4908', '49', 0, 'CONG.POS.CASSA', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'4920', 'CONGUAGLIO DI CASSA MODD.501 LEGGE 96/97', '0', '', 0, '', 'I', 'N', 'a', 'Y', 1, '03', 1, '', 0, '4920', '49', 0, 'ARROT.L. 96/97', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'494', 'INT.MORA IMP.SPET.SCOM IN GEN.', '0', '', 0, '', 'S', 'S', 'a', 'Y', 1, '03', 1, '', 0, '494', '02', 0, 'IS SCOMM.MORA', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'4981', 'NON UTILIZZATO', '0', '', 0, '', 'I', 'N', 'a', 'Y', 1, '03', 1, '', 0, '4981', '49', 0, 'NON UTILIZZATO', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'505', 'IMPOSTA SPETTACOLI SCOMMESSE CORSE CAV.', '0', '0905', 3, '0595', 'I', 'S', 'a', 'Y', 0, '03', 0, 'E', 0, '505', '02', 0, 'IS SCOMM.CAVAL', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'5082', 'IVA  16% DPR 633/72 E SEGUENTI', '0', '', 0, '', 'I', 'N', '', 'Y', 1, '05', 1, 'I', 0, '5082', '50', 0, 'IVA 16% 633/72', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'5183', 'IVA ORDINARIA DPR 633/72 E SEGUENTI', '0', '', 0, '', 'I', 'N', '', 'Y', 1, '05', 1, 'I', 0, '5183', '51', 0, 'IVA ORDINARIA 633/72', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'5184', 'IVA 21% DPR 633/72 E SEGUENTI', '0', '', 0, '', 'I', '', '', 'Y', 1, '05', 1, '', 0, '5184', '51', 0, 'IVA 21% 633/72', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'5185', '22% IVA SCISSIONE PAGAMENTI EX DPR 633/72 ART. 17-TER', '0', '', 0, '', 'I', '', '', 'Y', 1, '05', 1, 'I', 0, '0', '51', 0, 'IVA SCISSIONE PAG', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 0, 1, 0
)
,
(
'5284', 'IVA ATTIVA OPERAZIONI O/C DELLA D.G.', '0', '', 0, '', 'I', 'N', '', 'Y', 1, '05', 1, '', 0, '5284', '52', 1, 'IVA OP.O/C DG', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'5288', 'IVA 22% PASSIVA DI RENDICONTO', '0', '', 0, '', 'I', 'N', '', 'N', 1, '05', 1, 'I', 0, '5288', '8', 0, 'IVA P. 22% REN', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'5387', 'OPERAZIONI O/C DELLA D.G.', '0', '', 0, '', 'O', 'N', 'a', 'N', 1, '03', 1, '', 0, '5387', '10U', 1, 'OPERAZ.O/C DG', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'5411', 'EROGAZIONE ANTICIPO MISSIONE', '0', '', 0, '', 'I', 'N', 'a', 'N', 1, '03', 1, '', 0, '5411', '10U', 1, 'EROG.ANTIC.MIS', 0, '2013-12-31 00:00:00', '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'5416', 'LIQ.NE ANTICIPAZIONI TRAVEL', '0', '', 0, '', 'I', 'N', 'a', 'N', 1, '03', 1, '', 0, '5416', '10U', 1, 'LIQ.ANT.TRAVEL', 0, '2013-12-31 00:00:00', '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'5431', 'ATTIVAZIONE GARANZIA AGIS ANEC', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, '', 0, '5431', '54B', 1, 'ATT.GAR.AGIS ANEC', 0, NULL, '', '', 1, 0, 1, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'5436', 'ATTIVAZIONE GARANZIA AGIS ACEC', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, '', 0, '5436', '54B', 1, 'ATT.GAR.AGIS ACEC', 0, NULL, '', '', 1, 0, 1, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'5439', 'ATTIVAZIONE GARANZIA AGIS ANEM', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, '', 0, '5439', '54B', 1, 'ATT.GAR.AGIS ANEM', 0, NULL, '', '', 1, 0, 1, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'5450', 'DD.DD EMITT.PRIVATA', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, '', 0, '5486', '54B', 1, 'DD.DD.EM.PRIV.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'5459', 'ATT.GAR.AGIS DOR TEATRO', '0', '', 0, '', '', 'N', 'x', 'Y', 0, '03', 0, '', 0, '5469', '54B', 0, 'ATT.GAR.AGIS T', 0, NULL, '', '', 1, 0, 1, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'5460', 'ATT.GAR.ANESV', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, '', 0, '5460', '54B', 1, 'ATT.GAR.ANESV', 0, NULL, '', '', 1, 0, 1, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'5461', 'ATT.GAR.SAPAR', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, '', 0, '5461', '54B', 1, 'ATT.GAR.SAPAR', 0, NULL, '', '', 1, 0, 1, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'5462', 'ATT.GAR.SINDAUT', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, '', 0, '5462', '54B', 1, 'ATT.G.SINDAUT', 0, NULL, '', '', 1, 0, 1, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'5463', 'ATT.GAR.SNISV', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, '', 0, '5463', '54B', 1, 'ATTIV.GAR.SNIV', 0, NULL, '', '', 1, 0, 1, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'5464', 'ATT.GAR.ENC', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, '', 0, '5464', '54B', 1, 'ATTIV.GAR.ENC', 0, NULL, '', '', 1, 0, 1, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'5465', 'ATT.GAR.ANSVA', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, '', 0, '5465', '54B', 1, 'ATT.GAR.ANSVA', 0, NULL, '', '', 1, 0, 1, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'5466', 'ATT.GAR.CONV.SIAE/UNPLI', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, '', 0, '5466', '54B', 1, 'ATT.GAR.UNPLI', 0, NULL, '', '', 1, 0, 1, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'5467', 'ATT.GAR.SNAV', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, '', 0, '5467', '54B', 1, 'ATTIV.GAR.SNAV', 0, NULL, '', '', 1, 0, 1, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'5468', 'ATTI.GAR.ASSOSPETTACOLO', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, '', 0, '5468', '54B', 1, 'ATT.GAR.ASSOSP', 0, NULL, '', '', 1, 0, 1, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'5469', 'ATT.GAR. SNAVA', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, '', 0, '5469', '54B', 1, 'ATT.GAR.SVANA', 0, NULL, '', '', 1, 0, 1, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'5470', 'ATT.GAR.FASSV', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, '', 0, '5470', '54B', 1, 'ATT.GAR.FASSV', 0, NULL, '', '', 1, 0, 1, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'5471', 'ATT.GAR.SVATA', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, '', 0, '5471', '54B', 1, 'ATT.GAR.SVATA', 0, NULL, '', '', 1, 0, 1, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'5472', 'ATTIVAZIONE GARANZIA A.N.C.', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, '', 0, '5472', '54B', 1, 'ATT.GAR.A.N.C.', 0, NULL, '', '', 1, 0, 1, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'5473', 'ATT.GAR.CONV.FIPE', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, '', 0, '5473', '54B', 1, 'ATT.GAR.FIPE', 0, NULL, '', '', 1, 0, 1, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'5474', 'DEPOSITO GARANZIA COLL.ASSOMUSICA', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, '', 0, '5474', '54B', 1, 'GAR.ASSOMUSICA', 0, NULL, '', '', 1, 0, 1, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'5475', 'ATTIV.GAR.FIEPeT.CONFESERCENTI', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, '', 0, '5475', '54B', 1, 'A.GR.FIEPeT', 0, NULL, '', '', 1, 0, 1, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'5476', 'ATTIV.GAR.ASSOINTRATTENIMENTO', '0', '', 0, '', 'I', 'N', 'x', 'Y', 1, '03', 1, '', 0, '5476', '54B', 1, 'A.GR.ASSOI', 0, NULL, '', '', 1, 0, 1, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'5477', 'INCASSI PER FITTI ATTIVI USO COMMERCIALE DA FATTURARE', '6404', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, '', 0, '5477', '54B', 1, 'FITTI ATT.IMM.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'5478', 'INCASSI PER FITTI ATTIVI USO ABITATIVO DA FATTURARE', '0', '', 0, '2020', 'I', 'S', 'a', 'Y', 1, '03', 1, '', 0, '5478', '54B', 1, 'FITTI ATT.ABIT', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'5479', 'SPESE POSTALI', '0', '', 0, '2020', 'O', 'N', 'a', 'N', 1, '03', 1, '', 0, '5387', '10U', 1, 'SPESE POSTALI', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'5480', 'ACQUISTO RIVISTE E GIORNALI', '0', '', 0, '2020', 'O', 'N', 'a', 'N', 1, '03', 1, '', 0, '5387', '10U', 1, 'ACQ.GIORN.RIV.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'5485', 'IVA PASSIVA 22% SU COMPETENZE', '0', '', 0, '', 'I', 'N', '', 'N', 1, '05', 1, 'I', 0, '5485', '8', 0, 'IVA P. 22% REN', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'5486', 'OPERAZIONI O/C DELLA D.G.', '0', '', 0, '', 'O', 'N', '', 'Y', 1, '04', 1, '', 0, '5486', '54', 0, 'OPERAZ. O/C DG', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'5490', 'RIVALSA BOLLO VIRTUALE', '0', '', 0, '', 'I', 'N', 'a', 'Y', 1, '03', 1, 'B', 0, '5490', '46', 0, 'RIV.BOL.VIRT.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'5491', 'RIVALSA BOLLO VIRTUALE 221', '0', '', 0, '', 'I', '', 'a', 'Y', 1, '03', 0, 'B', 0, '5491', '9A', 0, 'RIV.BOL.VIRT.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'595', 'INT.MORA I.S.SCOM.CAV.31/12/96', '0', '', 0, '', 'S', 'S', 'a', 'Y', 1, '03', 1, '', 0, '595', '02', 0, 'IS SCOM.C.MORA', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'601', 'IVA SPETT.INGRESSI E IMP.MEDI', '0', '0906', 3, '0690', 'V', 'S', 'a', 'Y', 1, '03', 1, 'E', 0, '601', '02', 0, 'IVA ING.IMP.M.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'602', 'IVA SPETT.PROV.CONNESSI', '0', '0906', 3, '0690', 'V', 'S', 'a', 'Y', 1, '03', 1, 'E', 0, '602', '02', 0, 'IVA SP.PR.CONN', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'603', 'IVA SPETT.SPONSORIZZAZIONI', '0', '0906', 3, '0690', 'V', 'S', 'a', 'Y', 1, '03', 1, 'E', 0, '603', '02', 0, 'IVA SPETT.SPON', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'604', 'IVA SPETT.RIPRESE RR-TV', '0', '0906', 3, '0690', 'V', 'S', 'a', 'Y', 1, '03', 1, 'E', 0, '604', '02', 0, 'IVA SP.RIP.RTV', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'605', 'IVA SPETT.PROV.NON CONNESSI', '0', '0906', 3, '0690', 'V', 'S', 'a', 'Y', 1, '03', 1, 'E', 0, '605', '02', 0, 'IVA SP.PR.N.C.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'606', 'IVA SPETT.MANIF.SINO 31/12/97', '0', '0906', 3, '0696', 'V', 'S', 'a', 'Y', 1, '03', 1, 'E', 0, '606', '02', 0, 'IVA SPETTACOLI', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'6402', 'IVA 19% O/C ATTIVA', '0', '', 0, '', 'I', 'N', '', 'Y', 1, '05', 1, '', 0, '5284', '52', 1, 'IVA 19% O/C DG', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'6403', 'IVA 10% O/C   D.G. ATTIVA', '0', '', 0, '', 'I', 'N', '', 'Y', 1, '05', 1, 'I', 0, '5284', '52', 1, 'IVA 10% O/C DG', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'6404', 'IVA 22% O/C   D.G. ATTIVA', '0', '', 0, '', 'I', 'N', '', 'Y', 1, '05', 1, 'I', 0, '5284', '52', 1, 'IVA 22% O/C DG', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'6405', 'IVA  4% O/C   D.G. ATTIVA', '0', '', 0, '', 'I', 'N', '', 'Y', 1, '05', 1, 'I', 0, '5284', '52', 1, 'IVA  4% O/C D.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'6406', 'IVA 16% O/C   D.G. ATTIVA', '0', '', 0, '', 'I', 'N', '', 'Y', 1, '01', 1, 'I', 0, '5284', '52', 1, 'IVA 16% O/C DG', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'6407', 'IVA 21% O/C   D.G. ATTIVA', '0', '', 0, '', 'I', '', '', 'Y', 1, '05', 1, '', 0, '5284', '52', 1, 'IVA 21% O/C DG', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'6520', 'IVA PASSIVA AL 22%', '0', '', 0, '', 'I', 'N', '', 'N', 1, '05', 1, '', 0, '5286', '8B', 1, 'IVA PASS. 22%', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'690', 'INT.MORA IVA SPETTACOLI', '0', '', 0, '', 'T', 'S', 'a', 'Y', 1, '03', 1, 'a', 0, '690', '02', 0, 'IVA SP.INT.MOR', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'696', 'INT.MORA IVA SPETT.31/12/97', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, 'a', 0, '696', '02', 0, 'IVA SP.INT.MOR', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'707', 'DIRITTI CONNESSI LOCALI BALLO', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '707', '07', 0, 'DIR.CONN.BALLO', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 0, 1, 1, 1
)
,
(
'708', 'DIRITTI CONNESSI ELETTROGRAMMOFONI', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '708', '07', 0, 'DIR.CONN.EGG', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 0, 1, 1, 1
)
,
(
'709', 'D.CON.DIFF.RR', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '709', '07', 0, 'D.CON.DIFF.RR', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 0, 1, 1, 0
)
,
(
'710', 'D.CON.RIPR.RR', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '710', '07', 0, 'D.CON.RIPR.RR', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 0, 1, 1, 0
)
,
(
'7101', 'CONTRIBUTO SPESE DI SOGGIORNO 55% A.F.', '0', '', 0, '', 'I', 'N', 'h', 'N', 1, '03', 1, '', 0, '7100', '4B', 0, 'CONTR.SOGG.55%', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'7102', 'CONTRIBUTO SPESE DI SOGGIORNO 45% A.F.', '0', '', 0, '', 'I', 'N', 'a', 'N', 1, '03', 1, '', 0, '7100', '4B', 0, 'CONTR.SOGG.45%', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'7103', 'RIMBORSO SPESE VIAGGIO 55% A.F.', '0', '', 0, '', 'I', 'N', 'h', 'N', 1, '03', 1, '', 0, '7100', '4B', 0, 'RIMB.VIAG.55%', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'7104', 'RIMBORSO SPESE VIAGGIO 45% A.F.', '0', '', 0, '', 'I', 'S', '', 'N', 1, '04', 1, '', 0, '7100', '4B', 0, 'RIMB.VIAG.45%', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'711', 'QUOTA AFI E MANDANTI DIRETTI MUSICA D\'AMBIENTE', '5183', '', 0, '', '', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '711', '07', 0, 'Q.AFI MAND.DIR', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 0, 1, 1, 0
)
,
(
'712', 'QUOTA AFI E MANDANTI DIRETTI ATTIVITA\' LUDICO RICREATIVE', '5183', '', 0, '', '', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '712', '07', 0, 'Q.AFI ATT.LUDI', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 0, 1, 1, 0
)
,
(
'713', 'QUOTA AFI E MANDANTI DIRETTI TRATTENIMENTI CON BALLO', '5183', '', 0, '', '', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '713', '07', 0, 'Q.AFI TRAT.BAL', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 0, 1, 1, 0
)
,
(
'714', 'QUOTA AFI E MANDANTI DIRETTI TRATTENIMENTI SENZA BALLO', '5183', '', 0, '', '', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '714', '07', 0, 'Q.AFI SENZ.BAL', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 0, 1, 1, 0
)
,
(
'720', 'DIRITTI CONNESSI FESTE PRIVATE', '5183', '', 0, '', '', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '720', '07', 0, 'DIRITTI FESTE PRIVATE', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 1
)
,
(
'723', 'DIRITTI CONNESSI BALLO CON LICENZA', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '723', '07', 0, 'DIR.CONN.BALLO LIC.', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 1
)
,
(
'724', 'DIRITTI CONNESSI - TRATTENIMENTI CON CARNET', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '724', '07', 0, 'DIR.CONN.TRATT.CARN.', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 1
)
,
(
'740', 'DIRITTI CONNESSI INTRATTENIMENTO', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '740', '07', 0, 'DIR.CONN.INTRATT.', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 1
)
,
(
'759', 'DIRITTI CONNESSI MUSICA D\'AMBIENTE RADIO IN STORE', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '759', '07', 0, 'DIR.CONN. MUS.AMB.RADIO STORE', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 1
)
,
(
'772', 'DIRITTI SCF ART. 72', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '772', '07', 0, 'DIR.ART.72 LDA', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'779', 'DIRITTI CONNESSI MUSICA D\'AMBIENTE RADIO IN STORE ALTOPARLANTI STACCATI', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '779', '07', 0, 'DIR.CONN. MUS.AMB.ALT.ST. RR STORE', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 1
)
,
(
'780', 'DIRITTI CONNESSI ALBERGHI', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '780', '07', 0, 'DIR.CONN. ALBERGHI', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 1
)
,
(
'782', 'FLAT AUDIO DIRITTI CONNESSI', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '782', '22B', 0, 'FLAT AUDIO DIRITTI CONNESSI', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'783', 'FLAT ALL IN ONE DIRITTI CONNESSI', '5183', '2442', 0, '2442', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '783', '22B', 0, 'FLAT ALL IN ONE DIRITTI CONNESSI', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 0
)
,
(
'784', 'DIRITTI CONNESSI MUSICA D\'AMBIENTE IN PUBBLICI ESERCIZI RADIO', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '784', '07', 0, 'DIR.CONN. MUS.PE.AMB.RR', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 1
)
,
(
'785', 'DIRITTI CONNESSI MUSICA D\'AMBIENTE IN PUBBLICI ESERCIZI TV', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '785', '07', 0, 'DIR.CONN. MUS.PE.AMB.TV', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 1
)
,
(
'786', 'DIRITTI CONNESSI MUSICA D\'AMBIENTE IN PUBBLICI ESERCIZI ATRI APPARECCHI', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '786', '07', 0, 'DIR.CONN. MUS.PE.AMB.ALTRO', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 1
)
,
(
'790', 'DIRITTI CONNESSI MANIFESTAZIONI PRO LOCO', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '790', '07', 0, 'DIR.CONN.MAN.PROLOCO', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 1
)
,
(
'791', 'DIRITTI CONNESSI CIRCOLI', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '791', '07', 0, 'DIR.CONN. ARCI', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 1
)
,
(
'792', 'DIRITTI CONNESSI MUSICA D\'AMBIENTE RADIO', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '792', '07', 0, 'DIR.CONN. MUS.AMB.RR', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 1
)
,
(
'793', 'DIRITTI CONNESSI MUSICA D\'AMBIENTE TV', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '793', '07', 0, 'DIR.CONN. MUS.AMB.TV', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 1
)
,
(
'794', 'DIRITTI CONNESSI MUSICA D\'AMBIENTE RADIO + TV', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '794', '07', 0, 'DIR.CONN. MUS.AMB.RR+TV', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 1
)
,
(
'795', 'DIRITTI CONNESSI MUSICA D\'AMBIENTE ALTRI STRUMENTI', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '795', '07', 0, 'DIR.CONN. MUS.AMB.S.M.', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 1
)
,
(
'796', 'DIRITTI CONNESSI MUSICA D\'AMBIENTE ALTOPARLANTI STACCATI RADIO', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '796', '07', 0, 'DIR.CONN. MUS.AMB.A.S.', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 1
)
,
(
'797', 'DIRITTI CONNESSI MUSICA D\'AMBIENTE MONITOR', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '797', '07', 0, 'DIR.CONN. MUS.AMB.MONITOR', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 1
)
,
(
'798', 'DIRITTI CONNESSI MUSICA D\'AMBIENTE SCHERMO > 37 POLLICI', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '798', '07', 0, 'DIR.CONN. MUS.AMB.SCHERMO > 37', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 1
)
,
(
'799', 'DIRITTI CONNESSI MUSICA D\'AMBIENTE ALTOPARLANTI STACCATI ALTRI', '5183', '', 0, '', 'I', 'N', 'd', 'Y', 1, '01', 1, 'M', 0, '799', '07', 0, 'D.C.A.S.ALTRI STR', 0, NULL, '', '', 1, 0, 0, 0, 1, '', 0, 1, 1, 1, 1
)
,
(
'851', 'SANZIONE PECUNIARIA INTRATTENIMENTI', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, 'E', 0, '851', '02', 0, 'SANZ.PEC.INTRA', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'868', 'IVA INTRATTENIMENTI', '0', '0851', 3, '0869', 'V', 'S', 'a', 'Y', 1, '03', 1, 'E', 0, '868', '02', 0, 'IVA INTRATT.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'869', 'INT.MORA IVA INTRATTENIMENTI', '0', '', 0, '', 'T', 'S', 'a', 'Y', 1, '03', 1, 'a', 0, '869', '02', 0, 'INT.MORA IV.IN', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'8803', 'RIVALSA INPS RILEV.CENSIMENTO', '5288', '', 0, '', 'I', 'N', 'd', 'N', 1, '01', 1, '', 0, '8803', '7D', 0, 'RIV.INPS CENS.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'8813', 'RIVALSA INPS CONT.INT.ANN. ES', '0', '', 0, '', 'I', 'N', 'h', 'N', 1, '03', 1, '', 0, '8813', '7D', 0, 'RIV.INPS C.I.A', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'8814', 'RIVALSA INPS CONT.INT.ANN. NX', '0', '', 0, '', 'I', 'N', 'a', 'N', 1, '03', 1, '', 0, '8814', '7D', 0, 'RIV.INPS CIA N', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'8821', 'RIVALSA INPS CONT.SP.SOGG. ES', '0', '', 0, '', 'I', 'N', 'h', 'N', 1, '03', 1, '', 0, '8821', '7D', 0, 'RIV.INPS CSS E', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'8822', 'RIVALSA INPS CONT.SP.SOGG. NX', '0', '', 0, '', 'I', 'N', 'a', 'N', 1, '03', 1, '', 0, '8822', '7D', 0, 'RIV.INPS CSS N', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'8823', 'RIVALSA INPS CONT.SP.VIAG. ES', '0', '', 0, '', 'I', 'N', 'h', 'N', 1, '03', 1, '', 0, '8823', '7D', 0, 'RIV.INPS CSV E', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'8824', 'RIVALSA INPS CONT.SP.VIAG. NX', '0', '', 0, '', 'I', 'N', 'a', 'N', 1, '03', 1, '', 0, '8824', '7D', 0, 'RIV.INPS CSV N', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'888', 'IMPOSTA INTRATTENIMENTI', '0', '0851', 3, '0898', 'I', 'S', 'a', 'Y', 1, '03', 1, 'E', 0, '888', '02', 0, 'IMP.INTRATT.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'8891', 'RIVALSA INPS NON SOGGETTA', '0', '', 0, '', 'I', 'S', 'a', 'N', 1, '03', 1, '', 0, '8891', '7D', 0, 'RIVALS.INPS NX', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'8892', 'RIVALSA INPS ESENTE', '0', '', 0, '', 'I', 'N', 'h', 'N', 1, '03', 1, '', 0, '8892', '7D', 0, 'RIVALS.INPS ES', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'8893', 'RIVALSA INPS SOGGETTA', '5485', '', 0, '', 'I', 'N', 'd', 'N', 1, '01', 1, '', 0, '8893', '7D', 0, 'RIVALS.INPS SG', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'898', 'INT.MORA IMPOSTA INTRATTEN.', '0', '', 0, '', 'S', 'S', 'a', 'Y', 1, '03', 1, 'a', 0, '898', '02', 0, 'INT.MORA IMP.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'8989', 'NON UTILIZZATO', '0', '', 0, '', 'I', 'N', '', 'N', 1, '02', 1, '', 0, '8989', '4', 0, 'NON UTILIZZATO', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'8990', 'NON UTILIZZATO', '0', '', 0, '', 'I', 'N', '', 'N', 1, '04', 1, '', 0, '8990', '4', 0, 'NON UTILIZZATO', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'8993', 'CONTRIBUTO INTEGR. ANNUALE 55% ES', '0', '', 0, '', 'I', 'N', 'h', 'N', 1, '03', 1, '', 0, '8993', '4', 0, 'C.I.ANN.55% ES', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'8994', 'CONTRIBUTO INTEGR. ANNUALE 45% NX', '0', '', 0, '', 'I', 'N', 'a', 'N', 1, '03', 1, '', 0, '8994', '4', 0, 'C.I.ANN.45% NX', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'9002', 'CONGUAGLIO DI CASSA MODD.221 LEGGE 96/97', '0', '', 0, '', 'I', 'N', 'a', 'N', 1, '03', 1, '', 0, '9002', '9', 0, 'ARROT. L.96/97', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'9009', 'CONGUAGLIO NEGATIVO CASSA', '0', '', 0, '', 'I', 'N', 'a', 'N', 1, '03', 1, '', 0, '9009', '9', 0, 'CONG.NEG.CASSA', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'901', 'SANZ.PECUN.IMP.SPETT.CINEMA', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, 'E', 0, '901', '02', 0, 'SANZ.P.IS CINE', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'902', 'SANZ.PECUN.IMP.SPETT.ORDINARIO', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, 'E', 0, '902', '02', 0, 'SANZ.P.IS ORD.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'903', 'SANZ.PECUN.IMP.SPETT.SPORT', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, 'E', 0, '903', '02', 0, 'SANZ.P.IS SPOR', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'904', 'SANZ.PEC.IMP.SPET.SCOM.IN.GEN.', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, 'E', 0, '904', '02', 0, 'SANZ.P.IS SCOM', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'905', 'SANZ.PEC.IMP.SPET.SCOM.CAV.31/12/96', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, 'E', 0, '905', '02', 0, 'SANZ.P.IS CAVA', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'9050', 'RIMBORSO PRESTITI A MAND.Q.CAPITALE', '0', '', 0, '2020', 'I', 'S', 'a', 'Y', 1, '03', 1, '', 0, '9050', '54B', 1, 'RIMB.MAND.CAP.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'9051', 'RIMBORSO PRESTITI A MAND. Q.INTERESSI', '0', '', 0, '2020', 'I', 'S', 's', 'Y', 1, '03', 1, '', 0, '9051', '54B', 1, 'RIMB.MAND.INT.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'9055', 'RIMB. SPESE POSTALI M,ATARI', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, '', 0, '9055', '54', 0, 'RIMB.SPESE.POS', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'906', 'SANZIONE PECUNIARIA IVA SPETT.', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, 'E', 0, '906', '02', 0, 'SANZ.P.IVA SPE', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'9080', 'DIFFERENZE CAMBIO EURO/LIRE', '0', '', 0, '', 'I', 'N', 'a', 'N', 1, '03', 1, '', 0, '9080', '9A', 0, 'DIF.CAMBIO L/E', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'909', 'ERARIO ORDINARIO', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, '', 0, '909', '02', 0, 'ERARIO ORDINAR', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'9090', 'SPESE PER VERSAMENTI IN C/C POSTALE', '0', '', 0, '', 'I', 'N', 'a', 'N', 1, '03', 1, '', 0, '9090', '5', 0, 'SPESE VERS.C/C', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'9091', 'SPESE SERVIZIO BIGLIETTI', '0', '', 0, '', 'I', 'N', 'a', 'N', 1, '03', 1, '', 0, '9091', '10U', 1, 'SPESE SERV.BIG', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'910', 'SANZIONE PECUNIARIA IVA ASS.', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, 'E', 0, '910', '02', 0, 'SANZ.P.IVA ASS', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'914', 'SANZ.PEC.DOR PUB.DOM.31/12/96', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, 'E', 0, '914', '02', 0, 'SANZ.P.DOR PD', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'9191', 'PROVVIGIONI NON SOGGETTE', '0', '', 0, '', 'I', 'N', 'a', 'N', 1, '03', 1, '', 0, '9191', '7A', 0, 'PROVVIGIONI NX', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'9192', 'PROVVIGIONI ESENTI', '0', '', 0, '', 'I', 'N', 'h', 'N', 1, '03', 1, '', 0, '9192', '7B', 0, 'PROVVIGIONI ES', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'9193', 'PROVVIGIONI SOGGETTE', '5485', '', 0, '', 'I', 'N', 'd', 'N', 1, '01', 1, '', 0, '9193', '7C', 0, 'PROVVIGIONI SG', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'9200', 'PRELEVAMENTI BANCARI', '0', '', 0, '', '', 'N', 'a', 'N', 0, '03', 0, '', 0, '9200', '12U', 0, 'PRELEV.BANCARI', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'921', 'SANZ.PEC.LIR.PUB.DOM 31/12/96', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, 'E', 0, '921', '02', 0, 'SANZ.P.LIR.PD', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'9230', 'VERSAMENTI C/C BANCARIO PRESIDIO (EX SEDE)', '0', '', 0, '', 'I', 'N', 'a', 'N', 1, '03', 1, '', 0, '9230', '12U', 0, 'VERS. C/C BANC PRES. (EX SEDE)', 0, NULL, '', '', 1, 0, 0, 1, 0, '', 1, 1, 1, 1, 0
)
,
(
'9231', 'VERSAMENTI C/C POSTALE PRESIDIO (EX SEDE)', '0', '', 0, '', 'I', 'N', 'a', 'N', 1, '03', 1, '', 0, '9231', '12U', 0, 'VERS. C/C POSTALE PRES. (EX SEDE)', 0, NULL, '', '', 1, 0, 0, 1, 0, '', 1, 1, 1, 1, 0
)
,
(
'9233', 'VERSAMENTI POS/CARTE DI CREDITO PRESIDIO (EX SEDE)', '0', '', 0, '', 'I', 'N', 'a', 'N', 1, '03', 1, '', 0, '9233', '12U', 0, 'VERS. POS/CARTE PRES. (EX SEDE)', 0, NULL, '', '', 1, 0, 0, 1, 0, '', 1, 1, 1, 1, 0
)
,
(
'9234', 'VERSAMENTI POS/CARTE DI CREDITO SENZA FILI PRESIDIO (EX SEDE)', '0', '', 0, '', 'I', 'N', 'a', 'N', 1, '03', 1, '', 0, '9234', '12U', 0, 'VERS. POS/CARTE SENZA FILI PRES. (EX SEDE)', 0, NULL, '', '', 1, 0, 0, 1, 0, '', 1, 1, 1, 1, 0
)
,
(
'9270', 'INCASSI A MEZZO RID', '0', '', 0, '', 'I', 'N', 'a', 'N', 1, '03', 1, '', 0, '9270', '54B', 1, 'INCASSI RID', 0, NULL, '', '', 1, 0, 1, 1, 0, '', 0, 1, 1, 1, 0
)
,
(
'9275', 'RIEPILOGO INCASSI CARTE', '0', '', 0, '', 'I', 'N', 'a', 'N', 1, '03', 0, '', 0, '9275', '12U', 0, 'RIEPILOGO INCASSI CARTE', 0, NULL, '', '', 1, 0, 0, 1, 0, '', 1, 1, 1, 1, 0
)
,
(
'9276', 'VERSAMENTI POS/CARTE CR. SENZA FILI', '0', '', 0, '', 'I', 'N', 'a', 'Y', 1, '03', 0, '', 0, '9276', '12U', 0, 'VERS. POS/CARTE SENZA FILI', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 1, 1, 1, 1, 0
)
,
(
'9282', 'VERSAMENTI IN EURO', '0', '', 0, '', 'I', 'N', 'a', 'N', 1, '03', 1, '', 0, '9282', '12U', 0, 'VERSAM.IN EURO', 0, NULL, '', '', 1, 0, 0, 1, 0, '', 1, 1, 1, 1, 0
)
,
(
'9283', 'VERSAMENTI III CONTO: BRUNICO', '0', '', 0, '', 'I', 'N', 'a', 'N', 1, '03', 1, '', 0, '9283', '12U', 0, 'VERSAM.BRUNICO', 0, '2012-05-16 18:20:05', '', '', 1, 0, 0, 1, 0, '', 1, 1, 1, 1, 0
)
,
(
'9284', 'VERSAMENTI III CONTO: MERANO', '0', '', 0, '', 'I', 'N', 'a', 'N', 1, '03', 1, '', 0, '9284', '12U', 0, 'VERSAM.MERANO', 0, '2012-05-16 18:20:05', '', '', 1, 0, 0, 1, 0, '', 1, 1, 1, 1, 0
)
,
(
'9285', 'VERSAMENTI III CONTO: SIL,RO', '0', '', 0, '', 'I', 'N', 'a', 'N', 1, '03', 1, '', 0, '9285', '12U', 0, 'VERSAM.SIL,R', 0, '2012-05-16 18:20:05', '', '', 1, 0, 0, 1, 0, '', 1, 1, 1, 1, 0
)
,
(
'9286', 'VERSAMENTI III CONTO: BRESSANONE', '0', '', 0, '', 'I', 'N', 'a', 'N', 1, '03', 1, '', 0, '9286', '12U', 0, 'VERSAM.BRESSAN', 0, '2012-05-16 18:20:05', '', '', 1, 0, 0, 1, 0, '', 1, 1, 1, 1, 0
)
,
(
'9290', 'VERSAMENTI DA BONIFICO', '0', '', 0, '', 'I', 'N', 'a', 'N', 1, '03', 1, '', 0, '9290', '12U', 0, 'VERS.DA BONIF.', 0, NULL, '', '', 1, 0, 0, 1, 0, '', 1, 1, 1, 1, 0
)
,
(
'9292', 'VERSAMENTI III CONTO', '0', '', 0, '', 'I', 'N', 'a', 'N', 1, '03', 1, '', 0, '9292', '12U', 0, 'VERSAMENTI III CONTO', 0, NULL, '', '', 1, 0, 0, 1, 0, '', 1, 1, 1, 1, 0
)
,
(
'9294', 'VERSAMENTI ALTRO CONTO', '0', '', 0, '', 'I', 'N', 'a', 'N', 1, '03', 1, '', 0, '9294', '12U', 0, 'VER.ALTRO CONT', 0, NULL, '', '', 1, 0, 0, 1, 0, '', 1, 1, 1, 1, 0
)
,
(
'9295', 'VERSAMENTI C/C POSTALE', '0', '', 0, '', 'I', 'N', 'a', 'N', 1, '03', 1, '', 0, '9295', '12U', 0, 'VERSAM.C/C POSTALE', 0, NULL, '', '', 1, 0, 0, 1, 0, '', 1, 1, 1, 1, 0
)
,
(
'9296', 'BONIFICI MANDATARI MPS', '0', '', 0, '', 'I', 'N', 'a', 'N', 1, '03', 1, '', 0, '9296', '12U', 0, 'BONIFICI MANDATARI MPS', 0, NULL, '', '', 1, 0, 0, 1, 0, '', 1, 1, 1, 1, 0
)
,
(
'9297', 'MPS-RCG SEDE', '0', '', 0, '', 'I', 'N', 'a', 'N', 1, '03', 1, '', 0, '9297', '12U', 0, 'MPS-RCG SEDE', 0, NULL, '', '', 1, 0, 0, 1, 0, '', 1, 1, 1, 1, 0
)
,
(
'9393', 'RILEVAZIONE CENSIMENTO', '5183', '', 0, '', 'I', 'N', 'd', 'N', 1, '01', 1, '', 0, '9393', '3B', 0, 'RILEVAZ. CENS.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'9493', 'SPESE SERVIZIO BIGLIETTI IMP. 22%', '5288', '', 0, '', 'I', 'N', 'd', 'N', 1, '01', 1, '', 0, '9493', '6', 0, 'SP.BIGL.IMP 22', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'9494', 'SPESE SERV.ZIO BIGLIETTI', '0', '', 0, '', 'I', 'N', 'h', 'N', 1, '03', 1, '', 0, '9494', '6', 0, 'SPESE SERV.BIG', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'950', 'SANZIONE PECUNIARIA', '0', '', 0, '', 'I', 'S', 'a', 'Y', 1, '03', 1, 'a', 0, '950', '02', 0, 'SANZIONE PECUN', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 0, 1, 1, 0
)
,
(
'9595', 'ABBUONI DOR', '0', '', 0, '', 'I', 'S', 'a', 'N', 1, '03', 1, '', 0, '9595', '3', 0, 'ABBUONI DOR', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'9696', 'ABBUONI STRAORDINARI MUSICA', '0', '', 0, '', 'I', 'S', 'a', 'N', 1, '03', 1, '', 0, '9696', '3', 0, 'ABBUONI STRAORDINARI MUSICA', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'9797', 'ABBUONI PICCOLO DIRITTO', '0', '', 0, '', 'I', 'S', 'a', 'N', 1, '03', 1, '', 0, '9797', '3', 0, 'ABBUONI PICCOLO DIRITTO', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'9806', 'COMPENSAZIONE IVA SPETTACOLI', '0', '', 0, '', 'I', 'S', 'a', 'N', 1, '03', 1, '', 0, '9806', '1', 0, 'COMP.IVA SPETT', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'9898', 'ABBUONI IMPOSTA SPETTACOLI CINEMA', '0', '', 0, '', 'I', 'S', 'a', 'N', 1, '03', 1, '', 0, '9898', '1', 0, 'ABB. IS CINEMA', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'9899', 'ABBUONI SU ERARIO CINEMA', '0', '', 0, '', 'I', 'S', 'a', 'N', 1, '03', 1, '', 0, '9899', '1', 0, 'ABB.SU ER.CIN.', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
)
,
(
'9999', 'ABBUONI CONTESTUALI CINEMA', '0', '', 0, '', 'I', 'S', 'a', 'N', 1, '03', 1, '', 0, '9999', '9999', 0, 'ABB.CONT.CINEM', 0, NULL, '', '', 1, 0, 0, 0, 0, '', 0, 1, 1, 1, 0
);
INSERT INTO `PERF_VOCE_INCASSO` (`COD_VOCE_INCASSO`, `DESCRIZIONE`, `COD_VOCE_IVA`, `PERCENT_MORA`, `BASE_CALC_INTER_MORA`, `FLAG_BOLLO`, `COD_POSIZIONE_IVA`, `FLAG_ENTR_USC`, `ID_TIPO_MODELLO`, `COD_IMPONIBILE_IVA`, `ID_SOPRACONTO`, `FLAG_PM`, `FLAG_VOCE_RIPARTIZIONE_FITTIZIA`) VALUES ('2295', 'VOCE FITTIZIA', '0', '0', 'I', 'S', 'a', 'N', '1', '03', '1', '1', '1');
INSERT INTO `PERF_VOCE_INCASSO` (`COD_VOCE_INCASSO`, `DESCRIZIONE`, `COD_VOCE_IVA`, `PERCENT_MORA`, `BASE_CALC_INTER_MORA`, `FLAG_BOLLO`, `COD_POSIZIONE_IVA`, `FLAG_ENTR_USC`, `ID_TIPO_MODELLO`, `COD_IMPONIBILE_IVA`, `ID_SOPRACONTO`, `FLAG_PM`, `FLAG_VOCE_RIPARTIZIONE_FITTIZIA`) VALUES ('2299', 'VOCE FITTIZIA', '0', '0', 'I', 'S', 'a', 'N', '1', '03', '1', '1', '1');
COMMIT;