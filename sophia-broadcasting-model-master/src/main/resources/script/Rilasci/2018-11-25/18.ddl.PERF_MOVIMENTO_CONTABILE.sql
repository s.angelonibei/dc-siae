ALTER TABLE `PERF_MOVIMENTO_CONTABILE`
ADD COLUMN `PUNTI_PROGRAMMA_MUSICALE` DECIMAL(15,7) NULL AFTER `AGENZIA`,
ADD COLUMN `IMPORTO_VALORIZZATO` DECIMAL(15,7) NULL AFTER `PUNTI_PROGRAMMA_MUSICALE`;
COMMIT;