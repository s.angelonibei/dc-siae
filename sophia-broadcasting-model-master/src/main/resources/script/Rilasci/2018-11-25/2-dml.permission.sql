INSERT INTO `permission` (`code`, `parent`, `url`, `name`, `menuVoice`) VALUES ('prCarichiRipartizione', 'prCruscotti', '#!/carichiRipartizione', 'Carichi Ripartizione', '1');
INSERT INTO `permission` (`code`, `parent`, `url`, `name`, `menuVoice`) VALUES ('prRegoleRipartizione', 'prCruscotti', '#!/regoleRipartizione', 'Regole Ripartizione', '1');
INSERT INTO `permission` (`code`, `parent`, `url`, `name`, `menuVoice`) VALUES ('prCodificaManualeBase', 'prCruscotti', '#!/codificaManualeBase', 'Codifica Manuale Base', '1');
INSERT INTO `permission` (`code`, `parent`, `url`, `name`, `menuVoice`) VALUES ('prAggiornamentoSun', 'guPerforming', '#!/aggiornamentoDatiSun', 'Aggiornamento Dati Sun', '1');
COMMIT;