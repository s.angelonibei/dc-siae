CREATE TABLE `PERIODO_RIPARTIZIONE` (
  `ID_PERIODO_RIPARTIZIONE` bigint(22) NOT NULL AUTO_INCREMENT,
  `CODICE` varchar(45) NOT NULL,
  `REPERTORIO` varchar(45) NOT NULL,
  `AMBITO` varchar(45) NOT NULL,
  `COMPETENZE_DA` date NOT NULL,
  `COMPETENZE_A` date NOT NULL,
  `DATA_APPROVAZIONE_REGOLE_VALORIZZAZIONE` date DEFAULT NULL,
  `APPROVATORE_REGOLE_VALORIZZAZIONE` varchar(255) DEFAULT NULL,
  `DATA_APPROVAZIONE_CARICHI_RIPARTO` date DEFAULT NULL,
  `APPROVATORE_CARICHI_RIPARTO` varchar(255) DEFAULT NULL,
  `STATO_APPROVAZIONE_CARICHI_RIPARTO` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`ID_PERIODO_RIPARTIZIONE`),
  UNIQUE KEY `ID_PERIODO_RIPARTIZIONE_UNIQUE` (`ID_PERIODO_RIPARTIZIONE`)
) ENGINE=InnoDB;
COMMIT;
