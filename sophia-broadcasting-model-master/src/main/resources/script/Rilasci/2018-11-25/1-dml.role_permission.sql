INSERT INTO `role_permission` (`role`, `permission`) VALUES ('DEVELOPMENT', 'prCarichiRipartizione');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('BU-MUSICA', 'prCarichiRipartizione');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('IT', 'prCarichiRipartizione');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('GA', 'prCarichiRipartizione');

INSERT INTO `role_permission` (`role`, `permission`) VALUES ('DEVELOPMENT', 'prRegoleRipartizione');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('BU-MUSICA', 'prRegoleRipartizione');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('IT', 'prRegoleRipartizione');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('GA', 'prRegoleRipartizione');

INSERT INTO `role_permission` (`role`, `permission`) VALUES ('DEVELOPMENT', 'prCodificaManualeBase');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('CODIFICATOREBASE', 'prCodificaManualeBase');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('BU-MUSICA', 'prCodificaManualeBase');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('IT', 'prCodificaManualeBase');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('GA', 'prCodificaManualeBase');

INSERT INTO `role_permission` (`role`, `permission`) VALUES ('DEVELOPMENT', 'prAggiornamentoSun');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('BU-MUSICA', 'prAggiornamentoSun');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('IT', 'prAggiornamentoSun');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('GA', 'prAggiornamentoSun');

COMMIT;