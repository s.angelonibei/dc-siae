DELETE FROM `role_permission` WHERE `id`='36';
DELETE FROM `role_permission` WHERE `id`='63';
DELETE FROM `role_permission` WHERE `id`='99';
DELETE FROM `role_permission` WHERE `id`='125';

INSERT INTO `role_permission` (`role`,`permission`) VALUES ('DEVELOPMENT','prConfigurazioneCampionamento');
INSERT INTO `role_permission` (`role`,`permission`) VALUES ('DEVELOPMENT','prStoricoCampionamento');
INSERT INTO `role_permission` (`role`,`permission`) VALUES ('DEVELOPMENT','prStoricoEsecuzioni');
INSERT INTO `role_permission` (`role`,`permission`) VALUES ('DEVELOPMENT','prEsecuzioneCompionamento');
INSERT INTO `role_permission` (`role`,`permission`) VALUES ('DEVELOPMENT','prCampionamentoFlussoGuida');
INSERT INTO `role_permission` (`role`,`permission`) VALUES ('DEVELOPMENT','prCampionamento');
INSERT INTO `role_permission` (`role`,`permission`) VALUES ('DEVELOPMENT','guPerforming');

INSERT INTO `role_permission` (`role`,`permission`) VALUES ('BU-MUSICA','prConfigurazioneCampionamento');
INSERT INTO `role_permission` (`role`,`permission`) VALUES ('BU-MUSICA','prStoricoCampionamento');
INSERT INTO `role_permission` (`role`,`permission`) VALUES ('BU-MUSICA','prStoricoEsecuzioni');
INSERT INTO `role_permission` (`role`,`permission`) VALUES ('BU-MUSICA','prEsecuzioneCompionamento');
INSERT INTO `role_permission` (`role`,`permission`) VALUES ('BU-MUSICA','prCampionamentoFlussoGuida');
INSERT INTO `role_permission` (`role`,`permission`) VALUES ('BU-MUSICA','prCampionamento');
INSERT INTO `role_permission` (`role`,`permission`) VALUES ('BU-MUSICA','guPerforming');

INSERT INTO `role_permission` (`role`,`permission`) VALUES ('IT','prConfigurazioneCampionamento');
INSERT INTO `role_permission` (`role`,`permission`) VALUES ('IT','prStoricoCampionamento');
INSERT INTO `role_permission` (`role`,`permission`) VALUES ('IT','prStoricoEsecuzioni');
INSERT INTO `role_permission` (`role`,`permission`) VALUES ('IT','prEsecuzioneCompionamento');
INSERT INTO `role_permission` (`role`,`permission`) VALUES ('IT','prCampionamentoFlussoGuida');
INSERT INTO `role_permission` (`role`,`permission`) VALUES ('IT','prCampionamento');
INSERT INTO `role_permission` (`role`,`permission`) VALUES ('IT','guPerforming');

INSERT INTO `role_permission` (`role`,`permission`) VALUES ('GA','prConfigurazioneCampionamento');
INSERT INTO `role_permission` (`role`,`permission`) VALUES ('GA','prStoricoCampionamento');
INSERT INTO `role_permission` (`role`,`permission`) VALUES ('GA','prStoricoEsecuzioni');
INSERT INTO `role_permission` (`role`,`permission`) VALUES ('GA','prEsecuzioneCompionamento');
INSERT INTO `role_permission` (`role`,`permission`) VALUES ('GA','prCampionamentoFlussoGuida');
INSERT INTO `role_permission` (`role`,`permission`) VALUES ('GA','prCampionamento');
INSERT INTO `role_permission` (`role`,`permission`) VALUES ('GA','guPerforming');