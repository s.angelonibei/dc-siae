INSERT INTO `role_permission` (`role`, `permission`) VALUES ('DEVELOPMENT', 'prCodifica');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('BU-MUSICA', 'prCodifica');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('IT', 'prCodifica');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('GA', 'prCodifica');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('CODIFICATORESPERTO', 'prCodifica');

INSERT INTO `role_permission` (`role`, `permission`) VALUES ('DEVELOPMENT', 'prConfigurazioniSoglie');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('BU-MUSICA', 'prConfigurazioniSoglie');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('IT', 'prConfigurazioniSoglie');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('GA', 'prConfigurazioniSoglie');

INSERT INTO `role_permission` (`role`, `permission`) VALUES ('DEVELOPMENT', 'prCodificaManualeEsperto');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('CODIFICATORESPERTO', 'prCodificaManualeEsperto');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('BU-MUSICA', 'prCodificaManualeEsperto');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('IT', 'prCodificaManualeEsperto');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('GA', 'prCodificaManualeEsperto');

COMMIT;