INSERT INTO `permission` (`code`, `parent`, `name`, `menuVoice`) VALUES ('prCodifica', 'guPerforming', 'Codifica', 1);
INSERT INTO `permission` (`code`, `parent`, `url`, `name`, `menuVoice`) VALUES ('prConfigurazioniSoglie', 'prCodifica', '#!/configurazioniSoglie', 'Configurazioni Soglie', 1);
INSERT INTO `permission` (`code`, `parent`, `url`, `name`, `menuVoice`) VALUES ('prCodificaManualeEsperto', 'prCodifica', '#!/codificaManualeEsperto', 'Codifica Manuale Esperto', 1);
INSERT INTO `permission` (`code`, `parent`, `url`, `name`, `menuVoice`) VALUES ('prCodificaManualeBase', 'prCodifica', '#!/codificaManualeBase', 'Codifica Manuale Base', 1);
COMMIT;