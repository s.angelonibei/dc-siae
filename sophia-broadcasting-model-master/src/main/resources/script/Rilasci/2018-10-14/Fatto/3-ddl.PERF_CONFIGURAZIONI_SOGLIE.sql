CREATE TABLE `PERF_CONFIGURAZIONI_SOGLIE` (
  `ID_CONFIGURAZIONE_SOGLIE` bigint(22) NOT NULL AUTO_INCREMENT,
  `CODICE_CONFIGURAZIONE` bigint(22) NOT NULL,
  `INIZIO_PERIODO_COMPETENZA` datetime NOT NULL,
  `FINE_PERIODO_COMPETENZA` datetime DEFAULT NULL,
  `UTENTE_CARICAMENTO_FILE` varchar(255) NOT NULL,
  `DATA_CARICAMENTO_FILE` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DATA_ULTIMA_MODIFICA` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `UTENTE_ULTIMA_MODIFICA` varchar(255) NOT NULL,
  `PATH_FILE` varchar(255) NOT NULL,
  `NOME_ORIGINALE_FILE` varchar(255) NOT NULL,
  `FLAG_ATTIVO` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`ID_CONFIGURAZIONE_SOGLIE`)
) ENGINE=InnoDB COMMENT='Tabella per la gestione delle configurazioni delle soglie di confidenza';
COMMIT;