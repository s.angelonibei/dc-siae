INSERT INTO `role_permission` (`role`, `permission`) VALUES ('DEVELOPMENT', 'brCarichiRiparto');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('BU-MUSICA', 'brCarichiRiparto');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('IT', 'brCarichiRiparto');
commit;

INSERT INTO `permission` (`code`, `parent`, `url`, `name`, `menuVoice`) VALUES ('brCarichiRiparto', 'guBroadcasting', '#!/carichi-ripartizione', 'Carichi di Ripartizione', '1');
commit;