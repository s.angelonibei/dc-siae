CREATE TABLE `BDC_PASSWORD_RESET_TOKEN` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TOKEN` varchar(255) NOT NULL,
  `DATA_CREAZIONE` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ID_UTENTE` int(11) NOT NULL,
  `ACTIVE` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`),
  KEY `BDC_PASSWORD_RESET_TOKEN_bdc_utenti_FK` (`ID_UTENTE`),
  CONSTRAINT `BDC_PASSWORD_RESET_TOKEN_bdc_utenti_FK` FOREIGN KEY (`ID_UTENTE`) REFERENCES `bdc_utenti` (`id`)
) ENGINE=InnoDB;

COMMIT;