INSERT INTO `role_permission` (`role`, `permission`) VALUES ('DEVELOPMENT', 'prGestioneMegaconcerti');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('BU-MUSICA', 'prGestioneMegaconcerti');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('IT', 'prGestioneMegaconcerti');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('GA', 'prGestioneMegaconcerti');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('DEVELOPMENT', 'prGestioneRipartizione');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('BU-MUSICA', 'prGestioneRipartizione');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('IT', 'prGestioneRipartizione');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('GA', 'prGestioneRipartizione');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('DEVELOPMENT', 'prRiconciliazioneImporti');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('BU-MUSICA', 'prRiconciliazioneImporti');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('IT', 'prRiconciliazioneImporti');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('GA', 'prRiconciliazioneImporti');

INSERT INTO `role_permission` (`role`, `permission`) VALUES ('DEVELOPMENT', 'brLoginAlterEgo');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('IT', 'brLoginAlterEgo');
INSERT INTO `role_permission` (`role`, `permission`) VALUES ('BU-MUSICA', 'brLoginAlterEgo');

commit;