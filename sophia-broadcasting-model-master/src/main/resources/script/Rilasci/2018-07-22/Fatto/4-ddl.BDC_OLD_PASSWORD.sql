CREATE TABLE `BDC_OLD_PASSWORD` (
  `id_old_password` int(11) NOT NULL AUTO_INCREMENT,
  `old_password` varchar(255) NOT NULL,
  `data_creazione` datetime DEFAULT CURRENT_TIMESTAMP,
  `data_ultima_modifica` datetime DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id_old_password`),
  KEY `fk_oldpassword_user` (`user_id`),
  CONSTRAINT `fk_oldpassword_user` FOREIGN KEY (`user_id`) REFERENCES `bdc_utenti` (`id`)
) ENGINE=InnoDB;
COMMIT;