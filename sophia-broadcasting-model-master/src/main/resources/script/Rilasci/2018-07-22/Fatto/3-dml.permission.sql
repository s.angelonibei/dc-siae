INSERT INTO `permission` (`code`, `parent`, `url`, `name`, `menuVoice`) VALUES ('prRiconciliazioneImporti', 'guPerforming', '#!/riconciliazioneImporti', 'Riconciliazione Importi', '1');
INSERT INTO `permission` (`code`, `parent`, `url`, `name`, `menuVoice`) VALUES ('prGestioneMegaconcerti', 'prCruscotti', '#!/gestioneMegaconcerti', 'Gestione Megaconcerti', '1');
INSERT INTO `permission` (`code`, `parent`, `url`, `name`, `menuVoice`) VALUES ('prGestioneRipartizione', 'prCruscotti', '#!/gestioneRipartizione', 'Gestione Ripartizione', '1');
INSERT INTO `permission` (`code`, `parent`, `url`, `name`, `menuVoice`) VALUES ('brLoginAlterEgo', 'guBroadcasting', '#!/alterEgo', 'Login Alter Ego', '1');
commit;