ALTER TABLE `BDC_UTENTI_REPERTORIO`
  ADD UNIQUE INDEX `utente-repertorio` (`id_utente` ASC, `nome_repertorio` ASC, `default_repertorio` ASC);
