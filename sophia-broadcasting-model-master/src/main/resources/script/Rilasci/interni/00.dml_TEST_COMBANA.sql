-- QUERY PULIZIA AMBIENTE PERFORMING
SET FOREIGN_KEY_CHECKS=0;
truncate table MCMDB_debug.PERF_AGGI_MC;
truncate table MCMDB_debug.PERF_CODIFICA;
truncate table MCMDB_debug.PERF_COMBANA;
truncate table MCMDB_debug.PERF_DIRETTORE_ESECUZIONE;
truncate table MCMDB_debug.PERF_EVENTI_PAGATI;
truncate table MCMDB_debug.PERF_IMPORTO_RICALCOLATO;
truncate table MCMDB_debug.PERF_MANIFESTAZIONE;
truncate table MCMDB_debug.PERF_MOVIMENTAZIONE_LOGISTICA_PM;
truncate table MCMDB_debug.PERF_MOVIMENTI_SIADA;
truncate table MCMDB_debug.PERF_MOVIMENTO_CONTABILE;
truncate table MCMDB_debug.PERF_NDM_FILE;
truncate table MCMDB_debug.PERF_NDM_VOCE_FATTURA;
truncate table MCMDB_debug.PERF_NDM_VOCE_SCOMPOSTA;
truncate table MCMDB_debug.PERF_NEED_FILE;
truncate table MCMDB_debug.PERF_NEED_RECORD;
truncate table MCMDB_debug.PERF_PROGRAMMA_MUSICALE;
truncate table MCMDB_debug.PERF_RECORDING_FILE;
truncate table MCMDB_debug.PERF_RECORDING_RECORD;
truncate table MCMDB_debug.PERF_RECORDING_SCARTO;
truncate table MCMDB_debug.PERF_RECORDING_UTILIZZAZIONE_SCARTO;
truncate table MCMDB_debug.PERF_SESSIONE_CODIFICA;
truncate table MCMDB_debug.PERF_SOSPESI_RICALCOLO;
truncate table MCMDB_debug.PERF_TRACCIAMENTO_LOG_APPLICATIVO;
truncate table MCMDB_debug.PERF_TRATTAMENTO_PM;
truncate table MCMDB_debug.PERF_UTILIZZAZIONE;
truncate table MCMDB_debug.PERF_UTILIZZAZIONI_ASSEGNATE;
SET FOREIGN_KEY_CHECKS=1;
COMMIT;



-- INSERT DI COMBANA E CODIFICHE

INSERT INTO `PERF_COMBANA` (`CODICE_COMBANA`, `TITOLO`, `COMPOSITORI`) VALUES ('11111111', 'LA MORDIDITA', 'GOMEZ-MARTINEZ JOSE M');
INSERT INTO `PERF_COMBANA` (`CODICE_COMBANA`, `TITOLO`, `COMPOSITORI`) VALUES ('11111112', 'DESPACITO FEAT DADDY YANKEE', 'AYALA RAMON L');
INSERT INTO `PERF_COMBANA` (`CODICE_COMBANA`, `TITOLO`, `COMPOSITORI`) VALUES ('11111113', 'BENNY HILL SHOW', 'BERLIN IRVING');
INSERT INTO `PERF_COMBANA` (`CODICE_COMBANA`, `TITOLO`, `COMPOSITORI`) VALUES ('11111114', 'AI SE EU TE PEGO', 'ARCOVERDE SHARON ACIOLY');
INSERT INTO `PERF_COMBANA` (`CODICE_COMBANA`, `TITOLO`, `COMPOSITORI`) VALUES ('11111115', 'LOVUMBA', 'AYALA RAMON L');
INSERT INTO `PERF_COMBANA` (`CODICE_COMBANA`, `TITOLO`, `COMPOSITORI`) VALUES ('11111116', 'IL CIRCO DELLE MERAVIGLIE', 'SCIARRONE PASQUALE');
INSERT INTO `PERF_COMBANA` (`CODICE_COMBANA`, `TITOLO`, `COMPOSITORI`) VALUES ('11111117', 'METELA SACALA', 'CARIAS CACERES CESAR ALEJANDRO');
INSERT INTO `PERF_COMBANA` (`CODICE_COMBANA`, `TITOLO`, `COMPOSITORI`) VALUES ('11111118', 'IL PULCINO PIO', 'FARIAS GOMEZ PEDRO ALBERTO');
INSERT INTO `PERF_COMBANA` (`CODICE_COMBANA`, `TITOLO`, `COMPOSITORI`) VALUES ('11111119', 'QUE VIVA LA VIDA', 'DELGADO VICTOR EDMUNDO');
INSERT INTO `PERF_COMBANA` (`CODICE_COMBANA`, `TITOLO`, `COMPOSITORI`) VALUES ('11111120', 'COMUNQUE ANDARE', 'TOFFOLI ELISA');
INSERT INTO `PERF_COMBANA` (`CODICE_COMBANA`, `TITOLO`, `COMPOSITORI`) VALUES ('11111121', 'OCCIDENTALI\'S KARMA', 'CHIARAVALLI LUCA PAOLO');
INSERT INTO `PERF_COMBANA` (`CODICE_COMBANA`, `TITOLO`, `COMPOSITORI`) VALUES ('11111122', 'ROMA BANGKOK', 'ABBATE FEDERICA');

INSERT INTO `PERF_CODIFICA` (`ID_CODIFICA`, `ID_COMBANA`, `CODICE_OPERA_SUGGERITO`, `DATA_CODIFICA`, `DIST_SEC_CANDIDATA`, `CONFIDENZA`, `VALORE_ECONOMICO`, `CANDIDATE`, `TIPO_APPROVAZIONE`, `N_UTILIZZAZIONI`, `PREZIOSA`) VALUES ('1', '1', '14618403900', '2018-09-07 09:00:00', '5.0000000', '95.0000000', '0.0000000', '[{\"autore\": \"\", \"titolo\": \"LA MORDIDITA\", \"interprete\": \"\", \"codiceOpera\": \"14618403900\", \"compositore\": \"GOMEZ-MARTINEZ JOSE M\", \"gradoDiConf\": \"95\"}, {\"autore\": \"\", \"titolo\": \"LA MORETTA\", \"interprete\": \"\", \"codiceOpera\": \"13007074300\", \"compositore\": \"CACIORGNA MASSIMILIANO\", \"gradoDiConf\": \"90\"}]', 'M', '1000', 0);
INSERT INTO `PERF_CODIFICA` (`ID_CODIFICA`, `ID_COMBANA`, `CODICE_OPERA_SUGGERITO`, `DATA_CODIFICA`, `DIST_SEC_CANDIDATA`, `CONFIDENZA`, `VALORE_ECONOMICO`, `CANDIDATE`, `TIPO_APPROVAZIONE`, `N_UTILIZZAZIONI`, `PREZIOSA`) VALUES ('2', '2', '16604347300', '2018-09-07 09:10:00', '10', '95', '0', '[{\"autore\": \"\", \"titolo\": \"DESPACITO FEAT DADDY YANKEE\", \"interprete\": \"\", \"codiceOpera\": \"16604347300\", \"compositore\": \"AYALA RAMON L\", \"gradoDiConf\": \"95\"}, {\"autore\": \"\", \"titolo\": \"DESPERIA PATRIA\", \"interprete\": \"\", \"codiceOpera\": \"10319080100\", \"compositore\": \"FRANCESCO CASTELLI\", \"gradoDiConf\": \"85\"}]', 'M', '1500', 0);
INSERT INTO `PERF_CODIFICA` (`ID_CODIFICA`, `ID_COMBANA`, `CODICE_OPERA_SUGGERITO`, `DATA_CODIFICA`, `DIST_SEC_CANDIDATA`, `CONFIDENZA`, `VALORE_ECONOMICO`, `CANDIDATE`, `TIPO_APPROVAZIONE`, `N_UTILIZZAZIONI`, `PREZIOSA`) VALUES ('3', '3', '11872371100', '2018-09-07 09:20:00', '5', '95', '0', '[{\"autore\": \"\", \"titolo\": \"BENNY HILL SHOW\", \"interprete\": \"\", \"codiceOpera\": \"11872371100\", \"compositore\": \"BERLIN IRVING\", \"gradoDiConf\": \"95\"}, {\"autore\": \"\", \"titolo\": \"BENNY\", \"interprete\": \"\", \"codiceOpera\": \"16286042500\", \"compositore\": \"LODELAN\", \"gradoDiConf\": \"90\"}]', 'M', '2000', 0);
INSERT INTO `PERF_CODIFICA` (`ID_CODIFICA`, `ID_COMBANA`, `CODICE_OPERA_SUGGERITO`, `DATA_CODIFICA`, `DIST_SEC_CANDIDATA`, `CONFIDENZA`, `VALORE_ECONOMICO`, `CANDIDATE`, `TIPO_APPROVAZIONE`, `N_UTILIZZAZIONI`) VALUES ('4', '5', '12910046900', '2018-09-07 09:30:00', '5', '95', '0', '[{\"autore\": \"\", \"titolo\": \"LOVUMBA\", \"interprete\": \"\", \"codiceOpera\": \"12910046900\", \"compositore\": \"AYALA RAMON L\", \"gradoDiConf\": \"95\"}, {\"autore\": \"\", \"titolo\": \"LOVIN YOU\", \"interprete\": \"\", \"codiceOpera\": \"13277068600\", \"compositore\": \"CANINI STEFANO\", \"gradoDiConf\": \"90\"}]', 'M', '1000');
INSERT INTO `PERF_CODIFICA` (`ID_CODIFICA`, `ID_COMBANA`, `CODICE_OPERA_SUGGERITO`, `DATA_CODIFICA`, `DIST_SEC_CANDIDATA`, `CONFIDENZA`, `VALORE_ECONOMICO`, `CANDIDATE`, `TIPO_APPROVAZIONE`, `N_UTILIZZAZIONI`) VALUES ('5', '6', '13068010400', '2018-09-07 09:40:00', '5', '95', '0', '[{\"autore\": \"\", \"titolo\": \"IL CIRCO DELLE MERAVIGLIE\", \"interprete\": \"\", \"codiceOpera\": \"13068010400\", \"compositore\": \"SCIARRONE PASQUALE\", \"gradoDiConf\": \"95\"}, {\"autore\": \"\", \"titolo\": \"IL CIRCO\", \"interprete\": \"\", \"codiceOpera\": \"03149039500\", \"compositore\": \"GALLIA LUIGI\", \"gradoDiConf\": \"90\"}]', 'M', '1000');
INSERT INTO `PERF_CODIFICA` (`ID_CODIFICA`, `ID_COMBANA`, `CODICE_OPERA_SUGGERITO`, `DATA_CODIFICA`, `DIST_SEC_CANDIDATA`, `CONFIDENZA`, `VALORE_ECONOMICO`, `CANDIDATE`, `TIPO_APPROVAZIONE`, `N_UTILIZZAZIONI`) VALUES ('6', '7', '12626084400', '2018-09-07 09:50:00', '5', '95', '0', '[{\"autore\": \"\", \"titolo\": \"METELA SACALA\", \"interprete\": \"\", \"codiceOpera\": \"12626084400\", \"compositore\": \"CARIAS CACERES CESAR ALEJANDRO\", \"gradoDiConf\": \"95\"}, {\"autore\": \"\", \"titolo\": \"METELE PASION\", \"interprete\": \"\", \"codiceOpera\": \"09048078400\", \"compositore\": \"SCALICI MASSIMO\", \"gradoDiConf\": \"90\"}]', 'M', '1000');
INSERT INTO `PERF_CODIFICA` (`ID_CODIFICA`, `ID_COMBANA`, `CODICE_OPERA_SUGGERITO`, `DATA_CODIFICA`, `DIST_SEC_CANDIDATA`, `CONFIDENZA`, `VALORE_ECONOMICO`, `CANDIDATE`, `TIPO_APPROVAZIONE`, `N_UTILIZZAZIONI`) VALUES ('7', '8', '12240062410', '2018-09-07 10:00:00', '5', '95', '0', '[{\"autore\": \"\", \"titolo\": \"IL PULCINO PIO\", \"interprete\": \"\", \"codiceOpera\": \"12240062410\", \"compositore\": \"FARIAS GOMEZ PEDRO ALBERTO\", \"gradoDiConf\": \"95\"}, {\"autore\": \"\", \"titolo\": \"IL PULLMAN\", \"interprete\": \"\", \"codiceOpera\": \"10287034610\", \"compositore\": \"MONTI GIOVANNI\", \"gradoDiConf\": \"90\"}]', 'M', '1000');
INSERT INTO `PERF_CODIFICA` (`ID_CODIFICA`, `ID_COMBANA`, `CODICE_OPERA_SUGGERITO`, `DATA_CODIFICA`, `DIST_SEC_CANDIDATA`, `CONFIDENZA`, `VALORE_ECONOMICO`, `CANDIDATE`, `TIPO_APPROVAZIONE`, `N_UTILIZZAZIONI`) VALUES ('8', '9', '13856468100', '2018-09-07 10:10:00', '5', '95', '0', '[{\"autore\": \"\", \"titolo\": \"QUE VIVA LA VIDA\", \"interprete\": \"\", \"codiceOpera\": \"13856468100\", \"compositore\": \"DELGADO VICTOR EDMUNDO\", \"gradoDiConf\": \"95\"}, {\"autore\": \"\", \"titolo\": \"QUE VIDA\", \"interprete\": \"\", \"codiceOpera\": \"09316049900\", \"compositore\": \"DELBARBI MAURO\", \"gradoDiConf\": \"90\"}]', 'M', '2000');
INSERT INTO `PERF_CODIFICA` (`ID_CODIFICA`, `ID_COMBANA`, `CODICE_OPERA_SUGGERITO`, `DATA_CODIFICA`, `DIST_SEC_CANDIDATA`, `CONFIDENZA`, `VALORE_ECONOMICO`, `CANDIDATE`, `TIPO_APPROVAZIONE`, `N_UTILIZZAZIONI`) VALUES ('9', '10', '15205068100', '2018-09-07 10:20:00', '5', '95', '0', '[{\"autore\": \"\", \"titolo\": \"COMUNQUE ANDARE\", \"interprete\": \"\", \"codiceOpera\": \"15205068100\", \"compositore\": \"TOFFOLI ELISA\", \"gradoDiConf\": \"95\"}, {\"autore\": \"\", \"titolo\": \"COMUNQUE\", \"interprete\": \"\", \"codiceOpera\": \"13129005700\", \"compositore\": \"DAVIDE LUIGI ALESSANDRO AUTELITANO RIGAMONTI\", \"gradoDiConf\": \"90\"}]', 'M', '3500');
INSERT INTO `PERF_CODIFICA` (`ID_CODIFICA`, `ID_COMBANA`, `CODICE_OPERA_SUGGERITO`, `DATA_CODIFICA`, `DIST_SEC_CANDIDATA`, `CONFIDENZA`, `VALORE_ECONOMICO`, `CANDIDATE`, `TIPO_APPROVAZIONE`, `N_UTILIZZAZIONI`) VALUES ('10', '11', '17040005800', '2018-09-07 10:30:00', '10', '95', '0', '[{\"autore\": \"\", \"titolo\": \"OCCIDENTALI\'S KARMA\", \"interprete\": \"\", \"codiceOpera\": \"17040005800\", \"compositore\": \"CHIARAVALLI LUCA PAOLO\", \"gradoDiConf\": \"95\"}, {\"autore\": \"\", \"titolo\": \"OCEAN DRIVE\", \"interprete\": \"\", \"codiceOpera\": \"15847198500\", \"compositore\": \"ALUO TIMUCIN FABIAN KWONG WAH\", \"gradoDiConf\": \"85\"}]', 'M', '4000');
INSERT INTO `PERF_CODIFICA` (`ID_CODIFICA`, `ID_COMBANA`, `CODICE_OPERA_SUGGERITO`, `DATA_CODIFICA`, `DIST_SEC_CANDIDATA`, `CONFIDENZA`, `VALORE_ECONOMICO`, `CANDIDATE`, `TIPO_APPROVAZIONE`, `N_UTILIZZAZIONI`) VALUES ('11', '12', '15209024100', '2018-09-07 10:40:00', '10', '95', '0', '[{\"autore\": \"\", \"titolo\": \"ROMA BANGKOK\", \"interprete\": \"\", \"codiceOpera\": \"15209024100\", \"compositore\": \"ABBATE FEDERICA\", \"gradoDiConf\": \"95\"}, {\"autore\": \"\", \"titolo\": \"ROMANTICA SERA\", \"interprete\": \"\", \"codiceOpera\": \"02325042300\", \"compositore\": \"LANDI WILCHES\", \"gradoDiConf\": \"85\"}]', 'M', '4500');

UPDATE `PERF_UTILIZZAZIONE` SET `ID_COMBANA`='3' WHERE `ID_UTILIZZAZIONE`='3';
UPDATE `PERF_UTILIZZAZIONE` SET `ID_COMBANA`='4' WHERE `ID_UTILIZZAZIONE`='8';
UPDATE `PERF_UTILIZZAZIONE` SET `ID_COMBANA`='1' WHERE `ID_UTILIZZAZIONE`='10';
UPDATE `PERF_UTILIZZAZIONE` SET `ID_COMBANA`='2' WHERE `ID_UTILIZZAZIONE`='14';
UPDATE `PERF_UTILIZZAZIONE` SET `ID_COMBANA`='2' WHERE `ID_UTILIZZAZIONE`='324005';
UPDATE `PERF_UTILIZZAZIONE` SET `ID_COMBANA`='5' WHERE `ID_UTILIZZAZIONE`='4';
UPDATE `PERF_UTILIZZAZIONE` SET `ID_COMBANA`='6' WHERE `ID_UTILIZZAZIONE`='5';
UPDATE `PERF_UTILIZZAZIONE` SET `ID_COMBANA`='7' WHERE `ID_UTILIZZAZIONE`='6';
UPDATE `PERF_UTILIZZAZIONE` SET `ID_COMBANA`='8' WHERE `ID_UTILIZZAZIONE`='7';
UPDATE `PERF_UTILIZZAZIONE` SET `ID_COMBANA`='9' WHERE `ID_UTILIZZAZIONE`='9';
UPDATE `PERF_UTILIZZAZIONE` SET `ID_COMBANA`='10' WHERE `ID_UTILIZZAZIONE`='11';
UPDATE `PERF_UTILIZZAZIONE` SET `ID_COMBANA`='11' WHERE `ID_UTILIZZAZIONE`='15';
UPDATE `PERF_UTILIZZAZIONE` SET `ID_COMBANA`='12' WHERE `ID_UTILIZZAZIONE`='12';

UPDATE `MCMDB_debug`.`PERF_CODIFICA` SET `CANDIDATE`='[{\"autore\": \"\", \"titolo\": \"BENNY HILL SHOW\", \"confAutore\": \"0\", \"confTitolo\": \"93.55\", \"interprete\": \"\", \"codiceOpera\": \"11872371100\", \"compositore\": \"BERLIN IRVING\", \"gradoDiConf\": \"95\", \"confInterprete\": \"0\", \"confCompositore\": \"88.77\"}, {\"autore\": \"\", \"titolo\": \"BENNY\", \"confAutore\": \"0\", \"confTitolo\": \"71.18\", \"interprete\": \"\", \"codiceOpera\": \"16286042500\", \"compositore\": \"LODELAN\", \"gradoDiConf\": \"90\", \"confInterprete\": \"0\", \"confCompositore\": \"85.77\"}]' WHERE `ID_CODIFICA`='3';
UPDATE `MCMDB_debug`.`PERF_CODIFICA` SET `CANDIDATE`='[{\"autore\": \"\", \"titolo\": \"LA MORDIDITA\", \"confAutore\": \"0\", \"confTitolo\": \"93.51\", \"interprete\": \"\", \"codiceOpera\": \"14618403900\", \"compositore\": \"GOMEZ-MARTINEZ JOSE M\", \"gradoDiConf\": \"95\", \"confInterprete\": \"0\", \"confCompositore\": \"79.77\"}, {\"autore\": \"\", \"titolo\": \"LA MORETTA\", \"confAutore\": \"0\", \"confTitolo\": \"78.37\", \"interprete\": \"\", \"codiceOpera\": \"13007074300\", \"compositore\": \"CACIORGNA MASSIMILIANO\", \"gradoDiConf\": \"90\", \"confInterprete\": \"0\", \"confCompositore\": \"83.01\"}]' WHERE `ID_CODIFICA`='1';
UPDATE `MCMDB_debug`.`PERF_CODIFICA` SET `CANDIDATE`='[{\"autore\": \"\", \"titolo\": \"DESPACITO FEAT DADDY YANKEE\", \"confAutore\": \"0\", \"confTitolo\": \"90.51\", \"interprete\": \"\", \"codiceOpera\": \"16604347300\", \"compositore\": \"AYALA RAMON L\", \"gradoDiConf\": \"95\", \"confInterprete\": \"0\", \"confCompositore\": \"75.77\"}, {\"autore\": \"\", \"titolo\": \"DESPERIA PATRIA\", \"confAutore\": \"0\", \"confTitolo\": \"90.51\", \"interprete\": \"\", \"codiceOpera\": \"10319080100\", \"compositore\": \"FRANCESCO CASTELLI\", \"gradoDiConf\": \"85\", \"confCompositore\": \"75.77\"}]' WHERE `ID_CODIFICA`='2';
UPDATE `MCMDB_debug`.`PERF_CODIFICA` SET `CANDIDATE`='[{\"autore\": \"\", \"titolo\": \"LOVUMBA\", \"confAutore\": \"0\", \"confTitolo\": \"94.15\", \"interprete\": \"\", \"codiceOpera\": \"12910046900\", \"compositore\": \"AYALA RAMON L\", \"gradoDiConf\": \"95.78\", \"confInterprete\": \"0\", \"confCompositore\": \"85.77\"}, {\"autore\": \"\", \"titolo\": \"LOVIN YOU\", \"confAutore\": \"0\", \"confTitolo\": \"74.71\", \"interprete\": \"\", \"codiceOpera\": \"13277068600\", \"compositore\": \"CANINI STEFANO\", \"gradoDiConf\": \"90.11\", \"confInterprete\": \"0\", \"confCompositore\": \"85.77\"}]' WHERE `ID_CODIFICA`='4';
UPDATE `MCMDB_debug`.`PERF_CODIFICA` SET `CANDIDATE`='[{\"autore\": \"\", \"titolo\": \"IL CIRCO DELLE MERAVIGLIE\", \"confAutore\": \"0\", \"confTitolo\": \"93.15\", \"interprete\": \"\", \"codiceOpera\": \"13068010400\", \"compositore\": \"SCIARRONE PASQUALE\", \"gradoDiConf\": \"95.78\", \"confInterprete\": \"0\", \"confCompositore\": \"88.77\"}, {\"autore\": \"\", \"titolo\": \"IL CIRCO\", \"confAutore\": \"0\", \"confTitolo\": \"74.18\", \"interprete\": \"\", \"codiceOpera\": \"03149039500\", \"compositore\": \"GALLIA LUIGI\", \"gradoDiConf\": \"90\", \"confInterprete\": \"0\", \"confCompositore\": \"75.77\"}]' WHERE `ID_CODIFICA`='5';
UPDATE `MCMDB_debug`.`PERF_CODIFICA` SET `CANDIDATE`='[{\"autore\": \"\", \"titolo\": \"METELA SACALA\", \"confAutore\": \"0\", \"confTitolo\": \"93.55\", \"interprete\": \"\", \"codiceOpera\": \"12626084400\", \"compositore\": \"CARIAS CACERES CESAR ALEJANDRO\", \"gradoDiConf\": \"95\", \"confInterprete\": \"0\", \"confCompositore\": \"88.77\"}, {\"autore\": \"\", \"titolo\": \"METELE PASION\", \"confAutore\": \"0\", \"confTitolo\": \"71.18\", \"interprete\": \"\", \"codiceOpera\": \"09048078400\", \"compositore\": \"SCALICI MASSIMO\", \"gradoDiConf\": \"90\", \"confInterprete\": \"0\", \"confCompositore\": \"85.77\"}]' WHERE `ID_CODIFICA`='6';
UPDATE `MCMDB_debug`.`PERF_CODIFICA` SET `CANDIDATE`='[{\"autore\": \"\", \"titolo\": \"IL PULCINO PIO\", \"confAutore\": \"0\", \"confTitolo\": \"91.58\", \"interprete\": \"\", \"codiceOpera\": \"12240062410\", \"compositore\": \"FARIAS GOMEZ PEDRO ALBERTO\", \"gradoDiConf\": \"95\", \"confInterprete\": \"0\", \"confCompositore\": \"89.74\"}, {\"autore\": \"\", \"titolo\": \"IL PULLMAN\", \"confAutore\": \"0\", \"confTitolo\": \"81.48\", \"interprete\": \"\", \"codiceOpera\": \"10287034610\", \"compositore\": \"MONTI GIOVANNI\", \"gradoDiConf\": \"90\", \"confInterprete\": \"0\", \"confCompositore\": \"85.77\"}]' WHERE `ID_CODIFICA`='7';
UPDATE `MCMDB_debug`.`PERF_CODIFICA` SET `CANDIDATE`='[{\"autore\": \"\", \"titolo\": \"QUE VIVA LA VIDA\", \"confAutore\": \"0\", \"confTitolo\": \"96.50\", \"interprete\": \"\", \"codiceOpera\": \"13856468100\", \"compositore\": \"DELGADO VICTOR EDMUNDO\", \"gradoDiConf\": \"95\", \"confInterprete\": \"0\", \"confCompositore\": \"90.45\"}, {\"autore\": \"\", \"titolo\": \"QUE VIDA\", \"confAutore\": \"0\", \"confTitolo\": \"80.40\", \"interprete\": \"\", \"codiceOpera\": \"09316049900\", \"compositore\": \"DELBARBI MAURO\", \"gradoDiConf\": \"90\", \"confInterprete\": \"0\", \"confCompositore\": \"80.70\"}]' WHERE `ID_CODIFICA`='8';
UPDATE `MCMDB_debug`.`PERF_CODIFICA` SET `CANDIDATE`='[{\"autore\": \"\", \"titolo\": \"COMUNQUE ANDARE\", \"confAutore\": \"0\", \"confTitolo\": \"93.50\", \"interprete\": \"\", \"codiceOpera\": \"15205068100\", \"compositore\": \"TOFFOLI ELISA\", \"gradoDiConf\": \"95\", \"confInterprete\": \"0\", \"confCompositore\": \"93.45\"}, {\"autore\": \"\", \"titolo\": \"COMUNQUE\", \"confAutore\": \"0\", \"confTitolo\": \"77.40\", \"interprete\": \"\", \"codiceOpera\": \"13129005700\", \"compositore\": \"DAVIDE LUIGI ALESSANDRO AUTELITANO RIGAMONTI\", \"gradoDiConf\": \"90\", \"confInterprete\": \"0\", \"confCompositore\": \"77.70\"}]' WHERE `ID_CODIFICA`='9';
UPDATE `MCMDB_debug`.`PERF_CODIFICA` SET `CANDIDATE`='[{\"autore\": \"\", \"titolo\": \"OCCIDENTALI\'S KARMA\", \"confAutore\": \"0\", \"confTitolo\": \"90.50\", \"interprete\": \"\", \"codiceOpera\": \"17040005800\", \"compositore\": \"CHIARAVALLI LUCA PAOLO\", \"gradoDiConf\": \"95\", \"confInterprete\": \"0\", \"confCompositore\": \"92.45\"}, {\"autore\": \"\", \"titolo\": \"OCEAN DRIVE\", \"confAutore\": \"0\", \"confTitolo\": \"87.14\", \"interprete\": \"\", \"codiceOpera\": \"15847198500\", \"compositore\": \"ALUO TIMUCIN FABIAN KWONG WAH\", \"gradoDiConf\": \"85\", \"confInterprete\": \"0\", \"confCompositore\": \"78.77\"}]' WHERE `ID_CODIFICA`='10';
UPDATE `MCMDB_debug`.`PERF_CODIFICA` SET `CANDIDATE`='[{\"autore\": \"\", \"titolo\": \"ROMA BANGKOK\", \"confAutore\": \"0\", \"confTitolo\": \"91.12\", \"interprete\": \"\", \"codiceOpera\": \"15209024100\", \"compositore\": \"ABBATE FEDERICA\", \"gradoDiConf\": \"95\", \"confInterprete\": \"0\", \"confCompositore\": \"92.15\"}, {\"autore\": \"\", \"titolo\": \"ROMANTICA SERA\", \"confAutore\": \"0\", \"confTitolo\": \"80.41\", \"interprete\": \"\", \"codiceOpera\": \"02325042300\", \"compositore\": \"LANDI WILCHES\", \"gradoDiConf\": \"85\", \"confInterprete\": \"0\", \"confCompositore\": \"81.11\"}]' WHERE `ID_CODIFICA`='11';


UPDATE `MCMDB_dev`.`PERF_UTILIZZAZIONE` SET `ID_COMBANA`='5' WHERE `ID_UTILIZZAZIONE`='3';
UPDATE `MCMDB_dev`.`PERF_UTILIZZAZIONE` SET `ID_COMBANA`='6' WHERE `ID_UTILIZZAZIONE`='8';
UPDATE `MCMDB_dev`.`PERF_UTILIZZAZIONE` SET `ID_COMBANA`='7' WHERE `ID_UTILIZZAZIONE`='7';
UPDATE `MCMDB_dev`.`PERF_UTILIZZAZIONE` SET `ID_COMBANA`='8' WHERE `ID_UTILIZZAZIONE`='6';
UPDATE `MCMDB_dev`.`PERF_UTILIZZAZIONE` SET `ID_COMBANA`='9' WHERE `ID_UTILIZZAZIONE`='9';
UPDATE `MCMDB_dev`.`PERF_UTILIZZAZIONE` SET `ID_COMBANA`='10' WHERE `ID_UTILIZZAZIONE`='10';
UPDATE `MCMDB_dev`.`PERF_UTILIZZAZIONE` SET `ID_COMBANA`='11' WHERE `ID_UTILIZZAZIONE`='15';
UPDATE `MCMDB_dev`.`PERF_UTILIZZAZIONE` SET `ID_COMBANA`='12' WHERE `ID_UTILIZZAZIONE`='14';


UPDATE `MCMDB_debug`.`PERF_UTILIZZAZIONE` SET `CODICE_OPERA`=NULL, `ID_CODIFICA`=NULL WHERE `ID_UTILIZZAZIONE`='324005';
UPDATE `MCMDB_debug`.`PERF_CODIFICA` SET `DATA_APPROVAZIONE`=NULL, `STATO_APPROVAZIONE`=NULL, `CODICE_OPERA_APPROVATO`=NULL WHERE `ID_CODIFICA`='2';