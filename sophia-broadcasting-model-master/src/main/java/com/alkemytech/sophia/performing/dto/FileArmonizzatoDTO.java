package com.alkemytech.sophia.performing.dto;

import com.alkemytech.sophia.broadcasting.enums.ArmonizedField;
import com.alkemytech.sophia.common.Constants;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;
import org.apache.commons.lang.StringUtils;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class FileArmonizzatoDTO {
    @CsvBindByName
    @CsvBindByPosition(position = 0)
    private String palinsesto;
    @CsvBindByName
    @CsvBindByPosition(position = 1)
    private String titoloTrasmissione;
    @CsvBindByName
    @CsvBindByPosition(position = 2)
    private String dataInizioTrasmissione;
    @CsvBindByName
    @CsvBindByPosition(position = 3)
    private String orarioInizioTrasmissione;
    @CsvBindByName
    @CsvBindByPosition(position = 4)
    private String durataTrasmissione;
    @CsvBindByName
    @CsvBindByPosition(position = 5)
    private String titoloBrano;
    @CsvBindByName
    @CsvBindByPosition(position = 6)
    private String compositoreAutore;
    @CsvBindByName
    @CsvBindByPosition(position = 7)
    private String esecutorePerformer;
    @CsvBindByPosition(position = 8)
    @CsvBindByName
    private String album;
    @CsvBindByPosition(position = 9)
    @CsvBindByName
    private String durataBrano;
    @CsvBindByPosition(position = 10)
    @CsvBindByName
    private String codiceISWC;
    @CsvBindByPosition(position = 11)
    @CsvBindByName
    private String codiceISRC;
    @CsvBindByPosition(position = 12)
    @CsvBindByName
    private String orarioInizioBrano;
    @CsvBindByPosition(position = 13)
    @CsvBindByName
    private String categoriaUso;
    @CsvBindByPosition(position = 14)
    @CsvBindByName
    private Integer diffusioneRegionale;
    @CsvBindByPosition(position = 15)
    @CsvBindByName
    private String stato;
    @CsvBindByPosition(position = 16)
    @CsvBindByName
    private String campiObbligatoriMancanti;
    @CsvBindByPosition(position = 17)
    @CsvBindByName
    private String campiFormatoNonSupportato;
    @CsvBindByPosition(position = 18)
    @CsvBindByName
    private String campiConValoreInatteso;
    @CsvBindByPosition(position = 19)
    @CsvBindByName
    private String altriMotiviDiScarto;
    //R6-22-tracciato-rai
    @CsvBindByPosition(position = 20)
    @CsvBindByName
    private String identificativoRai;

    public FileArmonizzatoDTO(String palinsesto, String titoloTrasmissione, String dataInizioTrasmissione, String orarioInizioTrasmissione, String durataTrasmissione, String titoloBrano, String compositoreAutore, String esecutorePerformer, String album, String durataBrano, String codiceISWC, String codiceISRC, String orarioInizioBrano, String categoriaUso, Integer diffusioneRegionale, String stato, String errore, String altriMotiviDiScarto, String identificativoRai) {
        this.palinsesto = palinsesto;
        this.titoloTrasmissione = titoloTrasmissione;
        this.dataInizioTrasmissione = dataInizioTrasmissione;
        this.orarioInizioTrasmissione = orarioInizioTrasmissione;
        this.durataTrasmissione = durataTrasmissione;
        this.titoloBrano = titoloBrano;
        this.compositoreAutore = compositoreAutore;
        this.esecutorePerformer = esecutorePerformer;
        this.album = album;
        this.durataBrano = durataBrano;
        this.codiceISWC = codiceISWC;
        this.codiceISRC = codiceISRC;
        this.orarioInizioBrano = orarioInizioBrano;
        this.categoriaUso = categoriaUso;
        this.diffusioneRegionale = diffusioneRegionale;
        this.stato = stato;
        this.campiObbligatoriMancanti = getRegola(errore, Constants.REGOLA1);
        this.campiFormatoNonSupportato = getRegola(errore, Constants.REGOLA2);
        this.campiConValoreInatteso = getRegola(errore, Constants.REGOLA3);
        this.altriMotiviDiScarto = altriMotiviDiScarto;
        //R6-22
        this.identificativoRai = identificativoRai;
    }
    private String getRegola(String reg, String nomeRegola) {
        if(reg != null) {
            StringBuffer errorString = new StringBuffer();
            ObjectMapper objectMapper = new ObjectMapper();
            Gson gson = new Gson();
            Map<String, List<Map<String,String>>> mapErrori = gson.fromJson(reg, Map.class);
            errorToString(mapErrori, errorString, nomeRegola);
            if(errorString.length() > 0){
                return errorString.toString();
            }
        }
        return "";
    }


    public String getPalinsesto() {
        return palinsesto;
    }

    public void setPalinsesto(String palinsesto) {
        this.palinsesto = palinsesto;
    }

    public String getTitoloTrasmissione() {
        return titoloTrasmissione;
    }

    public void setTitoloTrasmissione(String titoloTrasmissione) {
        this.titoloTrasmissione = titoloTrasmissione;
    }

    public String getDataInizioTrasmissione() {
        return dataInizioTrasmissione;
    }

    public void setDataInizioTrasmissione(String dataInizioTrasmissione) {
        this.dataInizioTrasmissione = dataInizioTrasmissione;
    }

    public String getOrarioInizioTrasmissione() {
        return orarioInizioTrasmissione;
    }

    public void setOrarioInizioTrasmissione(String orarioInizioTrasmissione) {
        this.orarioInizioTrasmissione = orarioInizioTrasmissione;
    }

    public String getDurataTrasmissione() {
        return durataTrasmissione;
    }

    public void setDurataTrasmissione(String durataTrasmissione) {
        this.durataTrasmissione = durataTrasmissione;
    }

    public String getTitoloBrano() {
        return titoloBrano;
    }

    public void setTitoloBrano(String titoloBrano) {
        this.titoloBrano = titoloBrano;
    }

    public String getCompositoreAutore() {
        return compositoreAutore;
    }

    public void setCompositoreAutore(String compositoreAutore) {
        this.compositoreAutore = compositoreAutore;
    }

    public String getEsecutorePerformer() {
        return esecutorePerformer;
    }

    public void setEsecutorePerformer(String esecutorePerformer) {
        this.esecutorePerformer = esecutorePerformer;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getDurataBrano() {
        return durataBrano;
    }

    public void setDurataBrano(String durataBrano) {
        this.durataBrano = durataBrano;
    }

    public String getCodiceISWC() {
        return codiceISWC;
    }

    public void setCodiceISWC(String codiceISWC) {
        this.codiceISWC = codiceISWC;
    }

    public String getCodiceISRC() {
        return codiceISRC;
    }

    public void setCodiceISRC(String codiceISRC) {
        this.codiceISRC = codiceISRC;
    }

    public String getOrarioInizioBrano() {
        return orarioInizioBrano;
    }

    public void setOrarioInizioBrano(String orarioInizioBrano) {
        this.orarioInizioBrano = orarioInizioBrano;
    }

    public String getCategoriaUso() {
        return categoriaUso;
    }

    public void setCategoriaUso(String categoriaUso) {
        this.categoriaUso = categoriaUso;
    }

    public Integer getDiffusioneRegionale() {
        return diffusioneRegionale;
    }

    public void setDiffusioneRegionale(Integer diffusioneRegionale) {
        this.diffusioneRegionale = diffusioneRegionale;
    }

    public String getStato() {
        return stato;
    }

    public void setStato(String stato) {
        this.stato = stato;
    }

    public String getCampiObbligatoriMancanti() {
        return campiObbligatoriMancanti;
    }

    public void setCampiObbligatoriMancanti(String campiObbligatoriMancanti) {
        this.campiObbligatoriMancanti = campiObbligatoriMancanti;
    }

    public String getCampiFormatoNonSupportato() {
        return campiFormatoNonSupportato;
    }

    public void setCampiFormatoNonSupportato(String campiFormatoNonSupportato) {
        this.campiFormatoNonSupportato = campiFormatoNonSupportato;
    }

    public String getCampiConValoreInatteso() {
        return campiConValoreInatteso;
    }

    public void setCampiConValoreInatteso(String campiConValoreInatteso) {
        this.campiConValoreInatteso = campiConValoreInatteso;
    }

    public String getAltriMotiviDiScarto() {
        return altriMotiviDiScarto;
    }

    public void setAltriMotiviDiScarto(String altriMotiviDiScarto) {
        this.altriMotiviDiScarto = altriMotiviDiScarto;
    }

    //R6-22
    public String getIdentificativoRai(){return identificativoRai;}

    public void setIdentificativoRai(String identificativoRai){this.identificativoRai = identificativoRai;}

    public FileArmonizzatoDTO() {
    }

    public String[] getMappingStrategy(){
        return new String[]{
                ArmonizedField.PALINSESTO.getDescription(),
                ArmonizedField.TITOLO_TRASMISSIONE.getDescription(),
                ArmonizedField.DATA_INIZIO_TRASMISSIONE.getDescription(),
                ArmonizedField.ORARIO_INIZIO_TRASMISSIONE.getDescription(),
                ArmonizedField.DURATA_TRASMISSIONE.getDescription(),
                ArmonizedField.TITOLO_BRANO.getDescription(),
                ArmonizedField.COMPOSITORE_OP.getDescription(),
                ArmonizedField.ESECUTORE_OP.getDescription(),
                ArmonizedField.ALBUM_OP.getDescription(),
                ArmonizedField.DURATA_BRANO.getDescription(),
                ArmonizedField.CODICE_ISWC_OP.getDescription(),
                ArmonizedField.CODICE_ISRC_OP.getDescription(),
                ArmonizedField.ORARIO_INIZIO_OP.getDescription(),
                ArmonizedField.CATEGORIA_USO.getDescription(),
                ArmonizedField.DIFFUSIONE_REGIONALE_RD.getDescription(),
                ArmonizedField.STATO.getDescription(),
                ArmonizedField.REGOLA1.getDescription(),
                ArmonizedField.REGOLA2.getDescription(),
                ArmonizedField.REGOLA3.getDescription(),
                ArmonizedField.ERRORE_GENERICO.getDescription(),
                //R9-22
                ArmonizedField.IDENTIFICATIVO_RAI.getDescription()
        };
    }

//    public FileArmonizzatoDTO(Object[] obj){
//        try{
//            palinsesto = (String) obj[0];
//            titoloTrasmissione = (String) obj[1];
//            dataInizioTrasmissione = (String) obj[2];
//            orarioInizioTrasmissione = (String) obj[3];
//            durataTrasmissione = (String) obj[4];
//            titoloBrano = (String) obj[5];
//            compositoreAutore = (String) obj[6];
//            esecutorePerformer = (String) obj[7];
//            album = (String) obj[8];
//            durataBrano = (String) obj[9];
//            codiceISWC = (String) obj[10];
//            codiceISRC = (String) obj[11];
//            orarioInizioBrano = (String) obj[12];
//            categoriaUso = (String) obj[13];
//            diffusioneRegionale = (Integer) obj[14];
//            stato = (String) obj[15];
//            String reg1 = (String) obj[16];
//            String reg2 = (String) obj[16];
//            String reg3 = (String) obj[16];
//            altriMotiviDiScarto = (String) obj[17];
//            campiObbligatoriMancanti = regola1(reg1);
//            campiFormatoNonSupportato = regola2(reg2);
//            campiConValoreInatteso = regola3(reg3);
//            } catch (Throwable e) {
//            e.printStackTrace();
//        }
//    }

    private void errorToString(Map<String, List<Map<String,String>>> mapErrori, StringBuffer errorString, String errorKey){
        if(mapErrori.containsKey(errorKey)){
            List<Map<String,String>> errList = mapErrori.get(errorKey);
            if(!errList.isEmpty()){
                Iterator<Map<String,String>> iter = errList.iterator();
                Map<String,String> error = null;
                while(iter.hasNext()){
                    error = iter.next();
                    if(error != null && error.containsKey("fieldName")){
                        String field = ArmonizedField.findDescription(error.get("fieldName"));
                        if(StringUtils.isNotEmpty(field)){
                            errorString.append(field);
                            if(iter.hasNext()){
                                errorString.append(", ");
                            }
                        }
                    }
                }
            }
        }
    }
}
