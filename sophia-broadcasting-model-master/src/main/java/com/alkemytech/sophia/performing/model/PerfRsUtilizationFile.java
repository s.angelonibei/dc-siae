package com.alkemytech.sophia.performing.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PERF_RS_UTILIZATION_FILE database table.
 * 
 */
@Entity
@Table(name="PERF_RS_UTILIZATION_FILE")
@NamedQuery(name="PerfRsUtilizationFile.findAll", query="SELECT p FROM PerfRsUtilizationFile p")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PerfRsUtilizationFile implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID", unique=true, nullable=false)
	private Long id;

	@Column(name="ANNO")
	private Short anno;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_PROCESSAMENTO")
	private Date dataProcessamento;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_UPLOAD", nullable=false)
	private Date dataUpload;

	@Column(name="MESE")
	private Short mese;

	@Column(name="NOME_FILE", nullable=false)
	private String nomeFile;

	@Column(name="PERCORSO", nullable=false)
	private String percorso;

	@Column(name="STATO", nullable=false, length=1)
	private String stato;

	@Column(name="TIPO_UPLOAD", nullable=false, length=255)
	private String tipoUpload;

	//bi-directional many-to-one association to PerfMusicProvider

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MUSIC_PROVIDER", nullable=false)
	private PerfMusicProvider perfMusicProvider;

	//bi-directional many-to-one association to PerfPalinsesto

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PALINSESTO")
	private PerfPalinsesto perfPalinsesto;

	//bi-directional many-to-one association to PerfRsUtente

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_UTENTE", nullable=false)
	private PerfRsUtente perfRsUtente;

	//bi-directional many-to-one association to PerfRsUtilizationNormalizedFile

	@OneToMany(mappedBy="perfRsUtilizationFile")
	@XmlTransient
	private List<PerfRsUtilizationNormalizedFile> perfRsUtilizationNormalizedFiles;

	public PerfRsUtilizationFile() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Short getAnno() {
		return this.anno;
	}

	public void setAnno(Short anno) {
		this.anno = anno;
	}

	public Date getDataProcessamento() {
		return this.dataProcessamento;
	}

	public void setDataProcessamento(Date dataProcessamento) {
		this.dataProcessamento = dataProcessamento;
	}

	public Date getDataUpload() {
		return this.dataUpload;
	}

	public void setDataUpload(Date dataUpload) {
		this.dataUpload = dataUpload;
	}

	public Short getMese() {
		return this.mese;
	}

	public void setMese(Short mese) {
		this.mese = mese;
	}

	public String getNomeFile() {
		return this.nomeFile;
	}

	public void setNomeFile(String nomeFile) {
		this.nomeFile = nomeFile;
	}

	public String getPercorso() {
		return this.percorso;
	}

	public void setPercorso(String percorso) {
		this.percorso = percorso;
	}

	public String getStato() {
		return this.stato;
	}

	public void setStato(String stato) {
		this.stato = stato;
	}

	public String getTipoUpload() {
		return this.tipoUpload;
	}

	public void setTipoUpload(String tipoUpload) {
		this.tipoUpload = tipoUpload;
	}

	public PerfMusicProvider getPerfMusicProvider() {
		return this.perfMusicProvider;
	}

	public void setPerfMusicProvider(PerfMusicProvider perfMusicProvider) {
		this.perfMusicProvider = perfMusicProvider;
	}

	public PerfPalinsesto getPerfPalinsesto() {
		return this.perfPalinsesto;
	}

	public void setPerfPalinsesto(PerfPalinsesto perfPalinsesto) {
		this.perfPalinsesto = perfPalinsesto;
	}



	public PerfRsUtente getPerfRsUtente() {
		return this.perfRsUtente;
	}

	public void setPerfRsUtente(PerfRsUtente perfRsUtente) {
		this.perfRsUtente = perfRsUtente;
	}

	public List<PerfRsUtilizationNormalizedFile> getPerfRsUtilizationNormalizedFiles() {
		return this.perfRsUtilizationNormalizedFiles;
	}

	public void setPerfRsUtilizationNormalizedFiles(List<PerfRsUtilizationNormalizedFile> perfRsUtilizationNormalizedFiles) {
		this.perfRsUtilizationNormalizedFiles = perfRsUtilizationNormalizedFiles;
	}

	public PerfRsUtilizationNormalizedFile addPerfRsUtilizationNormalizedFile(PerfRsUtilizationNormalizedFile perfRsUtilizationNormalizedFile) {
		getPerfRsUtilizationNormalizedFiles().add(perfRsUtilizationNormalizedFile);
		perfRsUtilizationNormalizedFile.setPerfRsUtilizationFile(this);

		return perfRsUtilizationNormalizedFile;
	}

	public PerfRsUtilizationNormalizedFile removePerfRsUtilizationNormalizedFile(PerfRsUtilizationNormalizedFile perfRsUtilizationNormalizedFile) {
		getPerfRsUtilizationNormalizedFiles().remove(perfRsUtilizationNormalizedFile);
		perfRsUtilizationNormalizedFile.setPerfRsUtilizationFile(null);

		return perfRsUtilizationNormalizedFile;
	}

}