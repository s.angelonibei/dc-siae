package com.alkemytech.sophia.performing.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Date;
import java.math.BigInteger;


/**
 * The persistent class for the PERF_NEWS database table.
 * 
 */
@Entity
@Table(name="PERF_NEWS")
@NamedQuery(name="PerfNew.findAll", query="SELECT p FROM PerfNews p")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PerfNews implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_PERF_NEWS", unique=true, nullable=false)
	private BigInteger idPerfNews;

	@Column(name="CREATOR", nullable=false, length=100)
	private String creator;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DEACTIVATION_DATE")
	private Date deactivationDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="INSERT_DATE", nullable=false)
	private Date insertDate;

	@Lob
	@Column(name="NEWS", nullable=false)
	private String news;

	@Column(name="TITLE", nullable=false, length=45)
	private String title;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="VALID_FROM", nullable=false)
	private Date validFrom;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="VALID_TO", nullable=false)
	private Date validTo;

	//bi-directional many-to-one association to PerfMusicProvider

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MUSIC_PROVIDER")
	private PerfMusicProvider perfMusicProvider;

	//bi-directional many-to-one association to PerfRsUtente

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PERF_RS_UTENTE")
	private PerfRsUtente perfRsUtente;

	public PerfNews() {
	}

	public BigInteger getIdPerfNews() {
		return this.idPerfNews;
	}

	public void setIdPerfNews(BigInteger idPerfNews) {
		this.idPerfNews = idPerfNews;
	}

	public String getCreator() {
		return this.creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public Date getDeactivationDate() {
		return this.deactivationDate;
	}

	public void setDeactivationDate(Date deactivationDate) {
		this.deactivationDate = deactivationDate;
	}

	public Date getInsertDate() {
		return this.insertDate;
	}

	public void setInsertDate(Date insertDate) {
		this.insertDate = insertDate;
	}

	public String getNews() {
		return this.news;
	}

	public void setNews(String news) {
		this.news = news;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public PerfMusicProvider getPerfMusicProvider() {
		return this.perfMusicProvider;
	}

	public void setPerfMusicProvider(PerfMusicProvider perfMusicProvider) {
		this.perfMusicProvider = perfMusicProvider;
	}

	public PerfRsUtente getPerfRsUtente() {
		return this.perfRsUtente;
	}

	public void setPerfRsUtente(PerfRsUtente perfRsUtente) {
		this.perfRsUtente = perfRsUtente;
	}

}