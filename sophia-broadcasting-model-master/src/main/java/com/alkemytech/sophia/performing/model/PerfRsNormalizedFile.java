package com.alkemytech.sophia.performing.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PERF_RS_NORMALIZED_FILE database table.
 * 
 */
@Entity
@Table(name="PERF_RS_NORMALIZED_FILE")
@NamedQuery(name="PerfRsNormalizedFile.findAll", query="SELECT p FROM PerfRsNormalizedFile p")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PerfRsNormalizedFile implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_NORMALIZED_FILE", unique=true, nullable=false)
	private Long idNormalizedFile;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE", nullable=false)
	private Date creationDate;

	@Lob
	@Column(name="FILE_BUCKET", nullable=false)
	private String fileBucket;

	@Lob
	@Column(name="FILE_NAME", nullable=false)
	private String fileName;

	//bi-directional many-to-one association to PerfRsErrShowMusic

	@OneToMany(mappedBy="perfRsNormalizedFile")
	@XmlTransient
	private List<PerfRsErrShowMusic> perfRsErrShowMusics;

	//bi-directional many-to-one association to PerfRsErrShowSchedule

	@OneToMany(mappedBy="perfRsNormalizedFile")
	@XmlTransient
	private List<PerfRsErrShowSchedule> perfRsErrShowSchedules;

	//bi-directional many-to-one association to PerfRsShowMusic

	@OneToMany(mappedBy="perfRsNormalizedFile")
	@XmlTransient
	private List<PerfRsShowMusic> perfRsShowMusics;

	//bi-directional many-to-one association to PerfRsShowSchedule

	@OneToMany(mappedBy="perfRsNormalizedFile")
	@XmlTransient
	private List<PerfRsShowSchedule> perfRsShowSchedules;

	//bi-directional many-to-one association to PerfRsUtilizationNormalizedFile

	@OneToMany(mappedBy="perfRsNormalizedFile")
	@XmlTransient
	private List<PerfRsUtilizationNormalizedFile> perfRsUtilizationNormalizedFiles;

	public PerfRsNormalizedFile() {
	}

	public Long getIdNormalizedFile() {
		return this.idNormalizedFile;
	}

	public void setIdNormalizedFile(Long idNormalizedFile) {
		this.idNormalizedFile = idNormalizedFile;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getFileBucket() {
		return this.fileBucket;
	}

	public void setFileBucket(String fileBucket) {
		this.fileBucket = fileBucket;
	}

	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public List<PerfRsErrShowMusic> getPerfRsErrShowMusics() {
		return this.perfRsErrShowMusics;
	}

	public void setPerfRsErrShowMusics(List<PerfRsErrShowMusic> perfRsErrShowMusics) {
		this.perfRsErrShowMusics = perfRsErrShowMusics;
	}

	public PerfRsErrShowMusic addPerfRsErrShowMusic(PerfRsErrShowMusic perfRsErrShowMusic) {
		getPerfRsErrShowMusics().add(perfRsErrShowMusic);
		perfRsErrShowMusic.setPerfRsNormalizedFile(this);

		return perfRsErrShowMusic;
	}

	public PerfRsErrShowMusic removePerfRsErrShowMusic(PerfRsErrShowMusic perfRsErrShowMusic) {
		getPerfRsErrShowMusics().remove(perfRsErrShowMusic);
		perfRsErrShowMusic.setPerfRsNormalizedFile(null);

		return perfRsErrShowMusic;
	}

	public List<PerfRsErrShowSchedule> getPerfRsErrShowSchedules() {
		return this.perfRsErrShowSchedules;
	}

	public void setPerfRsErrShowSchedules(List<PerfRsErrShowSchedule> perfRsErrShowSchedules) {
		this.perfRsErrShowSchedules = perfRsErrShowSchedules;
	}

	public PerfRsErrShowSchedule addPerfRsErrShowSchedule(PerfRsErrShowSchedule perfRsErrShowSchedule) {
		getPerfRsErrShowSchedules().add(perfRsErrShowSchedule);
		perfRsErrShowSchedule.setPerfRsNormalizedFile(this);

		return perfRsErrShowSchedule;
	}

	public PerfRsErrShowSchedule removePerfRsErrShowSchedule(PerfRsErrShowSchedule perfRsErrShowSchedule) {
		getPerfRsErrShowSchedules().remove(perfRsErrShowSchedule);
		perfRsErrShowSchedule.setPerfRsNormalizedFile(null);

		return perfRsErrShowSchedule;
	}

	public List<PerfRsShowMusic> getPerfRsShowMusics() {
		return this.perfRsShowMusics;
	}

	public void setPerfRsShowMusics(List<PerfRsShowMusic> perfRsShowMusics) {
		this.perfRsShowMusics = perfRsShowMusics;
	}

	public PerfRsShowMusic addPerfRsShowMusic(PerfRsShowMusic perfRsShowMusic) {
		getPerfRsShowMusics().add(perfRsShowMusic);
		perfRsShowMusic.setPerfRsNormalizedFile(this);

		return perfRsShowMusic;
	}

	public PerfRsShowMusic removePerfRsShowMusic(PerfRsShowMusic perfRsShowMusic) {
		getPerfRsShowMusics().remove(perfRsShowMusic);
		perfRsShowMusic.setPerfRsNormalizedFile(null);

		return perfRsShowMusic;
	}

	public List<PerfRsShowSchedule> getPerfRsShowSchedules() {
		return this.perfRsShowSchedules;
	}

	public void setPerfRsShowSchedules(List<PerfRsShowSchedule> perfRsShowSchedules) {
		this.perfRsShowSchedules = perfRsShowSchedules;
	}

	public PerfRsShowSchedule addPerfRsShowSchedule(PerfRsShowSchedule perfRsShowSchedule) {
		getPerfRsShowSchedules().add(perfRsShowSchedule);
		perfRsShowSchedule.setPerfRsNormalizedFile(this);

		return perfRsShowSchedule;
	}

	public PerfRsShowSchedule removePerfRsShowSchedule(PerfRsShowSchedule perfRsShowSchedule) {
		getPerfRsShowSchedules().remove(perfRsShowSchedule);
		perfRsShowSchedule.setPerfRsNormalizedFile(null);

		return perfRsShowSchedule;
	}

	public List<PerfRsUtilizationNormalizedFile> getPerfRsUtilizationNormalizedFiles() {
		return this.perfRsUtilizationNormalizedFiles;
	}

	public void setPerfRsUtilizationNormalizedFiles(List<PerfRsUtilizationNormalizedFile> perfRsUtilizationNormalizedFiles) {
		this.perfRsUtilizationNormalizedFiles = perfRsUtilizationNormalizedFiles;
	}

	public PerfRsUtilizationNormalizedFile addPerfRsUtilizationNormalizedFile(PerfRsUtilizationNormalizedFile perfRsUtilizationNormalizedFile) {
		getPerfRsUtilizationNormalizedFiles().add(perfRsUtilizationNormalizedFile);
		perfRsUtilizationNormalizedFile.setPerfRsNormalizedFile(this);

		return perfRsUtilizationNormalizedFile;
	}

	public PerfRsUtilizationNormalizedFile removePerfRsUtilizationNormalizedFile(PerfRsUtilizationNormalizedFile perfRsUtilizationNormalizedFile) {
		getPerfRsUtilizationNormalizedFiles().remove(perfRsUtilizationNormalizedFile);
		perfRsUtilizationNormalizedFile.setPerfRsNormalizedFile(null);

		return perfRsUtilizationNormalizedFile;
	}

}