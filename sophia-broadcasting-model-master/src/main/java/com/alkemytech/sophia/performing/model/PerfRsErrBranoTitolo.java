package com.alkemytech.sophia.performing.model;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * The persistent class for the PERF_RS_ERR_BRANO_TITOLO database table.
 * 
 */
@Entity
@Table(name="PERF_RS_ERR_BRANO_TITOLO")
@NamedQuery(name="PerfRsErrBranoTitolo.findAll", query="SELECT p FROM PerfRsErrBranoTitolo p")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PerfRsErrBranoTitolo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_RS_ERR_BRANO_TITOLO", unique=true, nullable=false)
	private Long idRsErrBranoTitolo;

	@Column(name="RS_ERR_BRANO_TITOLO", length=400)
	private String rsErrBranoTitolo;

	public PerfRsErrBranoTitolo() {
	}

	public Long getIdRsErrBranoTitolo() {
		return idRsErrBranoTitolo;
	}

	public void setIdRsErrBranoTitolo(Long idRsErrBranoTitolo) {
		this.idRsErrBranoTitolo = idRsErrBranoTitolo;
	}

	public String getRsErrBranoTitolo() {
		return this.rsErrBranoTitolo;
	}

	public void setRsErrBranoTitolo(String rsErrBranoTitolo) {
		this.rsErrBranoTitolo = rsErrBranoTitolo;
	}

}