package com.alkemytech.sophia.performing.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.math.BigInteger;


/**
 * The persistent class for the PERF_PUNTO_VENDITA database table.
 * 
 */
@Entity
@Table(name="PERF_PUNTO_VENDITA")
@NamedQuery(name="PerfPuntoVendita.findAll", query="SELECT p FROM PerfPuntoVendita p")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PerfPuntoVendita implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_PUNTO_VENDITA", unique=true, nullable=false)
	private BigInteger idPuntoVendita;

	@Column(name="NOME", nullable=false, length=255)
	private String nome;

	//bi-directional many-to-one association to PerfPalinsesto

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PALINSESTO", nullable=false)
	private PerfPalinsesto perfPalinsesto;

	public PerfPuntoVendita() {
	}

	public BigInteger getIdPuntoVendita() {
		return this.idPuntoVendita;
	}

	public void setIdPuntoVendita(BigInteger idPuntoVendita) {
		this.idPuntoVendita = idPuntoVendita;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public PerfPalinsesto getPerfPalinsesto() {
		return perfPalinsesto;
	}

	public void setPerfPalinsesto(PerfPalinsesto perfPalinsesto) {
		this.perfPalinsesto = perfPalinsesto;
	}
}