package com.alkemytech.sophia.performing.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;


/**
 * The persistent class for the PERF_RS_UTILIZATION_NORMALIZED_FILE database table.
 * 
 */
@Entity
@Table(name="PERF_RS_UTILIZATION_NORMALIZED_FILE")
@NamedQuery(name="PerfRsUtilizationNormalizedFile.findAll", query="SELECT p FROM PerfRsUtilizationNormalizedFile p")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PerfRsUtilizationNormalizedFile implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_UTIL_NORM_FILE", unique=true, nullable=false)
	private Long idUtilNormFile;

	//bi-directional many-to-one association to PerfRsNormalizedFile

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_NORMALIZED_FILE", nullable=false)
	private PerfRsNormalizedFile perfRsNormalizedFile;

	//bi-directional many-to-one association to PerfRsUtilizationFile

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_UTILIZATION_FILE", nullable=false)
	private PerfRsUtilizationFile perfRsUtilizationFile;

	public PerfRsUtilizationNormalizedFile() {
	}

	public Long getIdUtilNormFile() {
		return this.idUtilNormFile;
	}

	public void setIdUtilNormFile(Long idUtilNormFile) {
		this.idUtilNormFile = idUtilNormFile;
	}

	public PerfRsNormalizedFile getPerfRsNormalizedFile() {
		return this.perfRsNormalizedFile;
	}

	public void setPerfRsNormalizedFile(PerfRsNormalizedFile perfRsNormalizedFile) {
		this.perfRsNormalizedFile = perfRsNormalizedFile;
	}

	public PerfRsUtilizationFile getPerfRsUtilizationFile() {
		return this.perfRsUtilizationFile;
	}

	public void setPerfRsUtilizationFile(PerfRsUtilizationFile perfRsUtilizationFile) {
		this.perfRsUtilizationFile = perfRsUtilizationFile;
	}

}