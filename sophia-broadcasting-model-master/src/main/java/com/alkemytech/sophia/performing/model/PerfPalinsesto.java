package com.alkemytech.sophia.performing.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Date;
import java.math.BigInteger;
import java.util.List;


/**
 * The persistent class for the PERF_PALINSESTO database table.
 * 
 */
@Entity
@Table(name="PERF_PALINSESTO")
@NamedQuery(name="PerfPalinsesto.findAll", query="SELECT p FROM PerfPalinsesto p")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PerfPalinsesto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_PALINSESTO", unique=true, nullable=false)
	private BigInteger idPalinsesto;

	@Column(name="ATTIVO", nullable=false)
	private Boolean attivo;

	@Column(name="CODICE_DITTA", nullable=false, length=255)
	private String codiceDitta;

	@Column(name="CODICE_ID", length=2)
	private String codiceId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_CREAZIONE", nullable=false)
	private Date dataCreazione;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_FINE_VALIDITA")
	private Date dataFineValidita;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_INIZIO_VALIDITA")
	private Date dataInizioValidita;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_ULTIMA_MODIFICA", nullable=false)
	private Date dataUltimaModifica;

	@Column(name="NOME", nullable=false, length=255)
	private String nome;

	@Column(name="UTENTE_ULTIMA_MODIFICA", length=255)
	private String utenteUltimaModifica;

	//bi-directional many-to-one association to PerfMusicProvider

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MUSIC_PROVIDER")
	private PerfMusicProvider perfMusicProvider;

	//bi-directional many-to-one association to PerfPalinsestoStorico

	@OneToMany(mappedBy="perfPalinsesto")
	@XmlTransient
	private List<PerfPalinsestoStorico> perfPalinsestoStoricos;

	//bi-directional many-to-one association to PerfPuntoVendita


	@OneToMany(mappedBy="perfPalinsesto")
	@XmlTransient
	private List<PerfPuntoVendita> perfPuntoVenditas;

	//bi-directional many-to-one association to PerfRsUtilizationFile

	@OneToMany(mappedBy="perfPalinsesto")
	@XmlTransient
	private List<PerfRsUtilizationFile> perfRsUtilizationFiles;

	//bi-directional many-to-one association to PerfRsConfig

	@OneToMany(mappedBy="perfPalinsesto")
	@XmlTransient
	private List<PerfRsConfig> perfRsConfigs;

	//bi-directional many-to-one association to PerfRsErrShowSchedule

	@OneToMany(mappedBy="perfPalinsesto")
	@XmlTransient
	private List<PerfRsErrShowSchedule> perfRsErrShowSchedules;

	//bi-directional many-to-one association to PerfRsKpi

	@OneToMany(mappedBy="perfPalinsesto")
	@XmlTransient
	private List<PerfRsKpi> perfRsKpis;

	//bi-directional many-to-one association to PerfRsShowSchedule

	@OneToMany(mappedBy="perfPalinsesto")
	@XmlTransient
	private List<PerfRsShowSchedule> perfRsShowSchedules;

	public PerfPalinsesto() {
	}

	public BigInteger getIdPalinsesto() {
		return this.idPalinsesto;
	}

	public void setIdPalinsesto(BigInteger idPalinsesto) {
		this.idPalinsesto = idPalinsesto;
	}

	public Boolean getAttivo() {
		return this.attivo;
	}

	public void setAttivo(Boolean attivo) {
		this.attivo = attivo;
	}

	public String getCodiceDitta() {
		return this.codiceDitta;
	}

	public void setCodiceDitta(String codiceDitta) {
		this.codiceDitta = codiceDitta;
	}

	public String getCodiceId() {
		return this.codiceId;
	}

	public void setCodiceId(String codiceId) {
		this.codiceId = codiceId;
	}

	public Date getDataCreazione() {
		return this.dataCreazione;
	}

	public void setDataCreazione(Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}

	public Date getDataFineValidita() {
		return this.dataFineValidita;
	}

	public void setDataFineValidita(Date dataFineValidita) {
		this.dataFineValidita = dataFineValidita;
	}

	public Date getDataInizioValidita() {
		return this.dataInizioValidita;
	}

	public void setDataInizioValidita(Date dataInizioValidita) {
		this.dataInizioValidita = dataInizioValidita;
	}

	public Date getDataUltimaModifica() {
		return this.dataUltimaModifica;
	}

	public void setDataUltimaModifica(Date dataUltimaModifica) {
		this.dataUltimaModifica = dataUltimaModifica;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getUtenteUltimaModifica() {
		return this.utenteUltimaModifica;
	}

	public void setUtenteUltimaModifica(String utenteUltimaModifica) {
		this.utenteUltimaModifica = utenteUltimaModifica;
	}

	public PerfMusicProvider getPerfMusicProvider() {
		return this.perfMusicProvider;
	}

	public void setPerfMusicProvider(PerfMusicProvider perfMusicProvider) {
		this.perfMusicProvider = perfMusicProvider;
	}

	public List<PerfPalinsestoStorico> getPerfPalinsestoStoricos() {
		return this.perfPalinsestoStoricos;
	}

	public void setPerfPalinsestoStoricos(List<PerfPalinsestoStorico> perfPalinsestoStoricos) {
		this.perfPalinsestoStoricos = perfPalinsestoStoricos;
	}

	public PerfPalinsestoStorico addPerfPalinsestoStorico(PerfPalinsestoStorico perfPalinsestoStorico) {
		getPerfPalinsestoStoricos().add(perfPalinsestoStorico);
		perfPalinsestoStorico.setPerfPalinsesto(this);

		return perfPalinsestoStorico;
	}

	public PerfPalinsestoStorico removePerfPalinsestoStorico(PerfPalinsestoStorico perfPalinsestoStorico) {
		getPerfPalinsestoStoricos().remove(perfPalinsestoStorico);
		perfPalinsestoStorico.setPerfPalinsesto(null);

		return perfPalinsestoStorico;
	}

	public List<PerfPuntoVendita> getPerfPuntoVenditas() {
		return this.perfPuntoVenditas;
	}

	public void setPerfPuntoVenditas(List<PerfPuntoVendita> perfPuntoVenditas) {
		this.perfPuntoVenditas = perfPuntoVenditas;
	}

	public PerfPuntoVendita addPerfPuntoVendita(PerfPuntoVendita perfPuntoVendita) {
		getPerfPuntoVenditas().add(perfPuntoVendita);
		perfPuntoVendita.setPerfPalinsesto(this);

		return perfPuntoVendita;
	}

	public PerfPuntoVendita removePerfPuntoVendita(PerfPuntoVendita perfPuntoVendita) {
		getPerfPuntoVenditas().remove(perfPuntoVendita);
		perfPuntoVendita.setPerfPalinsesto(null);

		return perfPuntoVendita;
	}

	public List<PerfRsUtilizationFile> getPerfRsUtilizationFiles() {
		return this.perfRsUtilizationFiles;
	}

	public void setPerfRsUtilizationFiles(List<PerfRsUtilizationFile> perfRsUtilizationFiles) {
		this.perfRsUtilizationFiles = perfRsUtilizationFiles;
	}

	public PerfRsUtilizationFile addPerfRsUtilizationFile(PerfRsUtilizationFile perfRsUtilizationFile) {
		getPerfRsUtilizationFiles().add(perfRsUtilizationFile);
		perfRsUtilizationFile.setPerfPalinsesto(this);

		return perfRsUtilizationFile;
	}

	public PerfRsUtilizationFile removePerfRsUtilizationFile(PerfRsUtilizationFile perfRsUtilizationFile) {
		getPerfRsUtilizationFiles().remove(perfRsUtilizationFile);
		perfRsUtilizationFile.setPerfPalinsesto(null);

		return perfRsUtilizationFile;
	}

	public List<PerfRsConfig> getPerfRsConfigs() {
		return this.perfRsConfigs;
	}

	public void setPerfRsConfigs(List<PerfRsConfig> perfRsConfigs) {
		this.perfRsConfigs = perfRsConfigs;
	}

	public PerfRsConfig addPerfRsConfig(PerfRsConfig perfRsConfig) {
		getPerfRsConfigs().add(perfRsConfig);
		perfRsConfig.setPerfPalinsesto(this);

		return perfRsConfig;
	}

	public PerfRsConfig removePerfRsConfig(PerfRsConfig perfRsConfig) {
		getPerfRsConfigs().remove(perfRsConfig);
		perfRsConfig.setPerfPalinsesto(null);

		return perfRsConfig;
	}

	public List<PerfRsErrShowSchedule> getPerfRsErrShowSchedules() {
		return this.perfRsErrShowSchedules;
	}

	public void setPerfRsErrShowSchedules(List<PerfRsErrShowSchedule> perfRsErrShowSchedules) {
		this.perfRsErrShowSchedules = perfRsErrShowSchedules;
	}

	public PerfRsErrShowSchedule addPerfRsErrShowSchedule(PerfRsErrShowSchedule perfRsErrShowSchedule) {
		getPerfRsErrShowSchedules().add(perfRsErrShowSchedule);
		perfRsErrShowSchedule.setPerfPalinsesto(this);

		return perfRsErrShowSchedule;
	}

	public PerfRsErrShowSchedule removePerfRsErrShowSchedule(PerfRsErrShowSchedule perfRsErrShowSchedule) {
		getPerfRsErrShowSchedules().remove(perfRsErrShowSchedule);
		perfRsErrShowSchedule.setPerfPalinsesto(null);

		return perfRsErrShowSchedule;
	}

	public List<PerfRsKpi> getPerfRsKpis() {
		return this.perfRsKpis;
	}

	public void setPerfRsKpis(List<PerfRsKpi> perfRsKpis) {
		this.perfRsKpis = perfRsKpis;
	}

	public List<PerfRsShowSchedule> getPerfRsShowSchedules() {
		return this.perfRsShowSchedules;
	}

	public void setPerfRsShowSchedules(List<PerfRsShowSchedule> perfRsShowSchedules) {
		this.perfRsShowSchedules = perfRsShowSchedules;
	}

	public PerfRsShowSchedule addPerfRsShowSchedule(PerfRsShowSchedule perfRsShowSchedule) {
		getPerfRsShowSchedules().add(perfRsShowSchedule);
		perfRsShowSchedule.setPerfPalinsesto(this);

		return perfRsShowSchedule;
	}

	public PerfRsShowSchedule removePerfRsShowSchedule(PerfRsShowSchedule perfRsShowSchedule) {
		getPerfRsShowSchedules().remove(perfRsShowSchedule);
		perfRsShowSchedule.setPerfPalinsesto(null);

		return perfRsShowSchedule;
	}

}