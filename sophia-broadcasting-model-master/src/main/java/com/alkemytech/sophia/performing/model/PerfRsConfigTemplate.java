package com.alkemytech.sophia.performing.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PERF_RS_CONFIG_TEMPLATE database table.
 * 
 */
@Entity
@Table(name="PERF_RS_CONFIG_TEMPLATE")
@NamedQuery(name="PerfRsConfigTemplate.findAll", query="SELECT p FROM PerfRsConfigTemplate p")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PerfRsConfigTemplate implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_PERF_RS_CONFIG_TEMPLATE", unique=true, nullable=false)
	private Long idPerfRsConfigTemplate;

	@Column(name="CONFIGURAZIONE", nullable=false)
	private String configurazione;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_CREAZIONE", columnDefinition = "datetime NOT NULL DEFAULT CURRENT_TIMESTAMP", insertable = false, updatable = false)
	private Date dataCreazione;

	@Column(name="FLAG_ATTIVO", nullable=false)
	private Boolean flagAttivo;

	@Column(name="FLAG_DEFAULT", nullable=false)
	private Boolean flagDefault;

	@Column(name="NOME_TEMPLATE", nullable=false, length=45)
	private String nomeTemplate;

	//bi-directional many-to-one association to PerfRsConfig
	@OneToMany(mappedBy="perfRsConfigTemplate")

	@XmlTransient
	private List<PerfRsConfig> perfRsConfigs;

	public PerfRsConfigTemplate() {
	}

	public Long getIdPerfRsConfigTemplate() {
		return this.idPerfRsConfigTemplate;
	}

	public void setIdPerfRsConfigTemplate(Long idPerfRsConfigTemplate) {
		this.idPerfRsConfigTemplate = idPerfRsConfigTemplate;
	}

	public Date getDataCreazione() {
		return this.dataCreazione;
	}

	public void setDataCreazione(Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}

	public void setConfigurazione(String configurazione) {
		this.configurazione = configurazione;
	}

	public Boolean getFlagAttivo() {
		return flagAttivo;
	}

	public void setFlagAttivo(Boolean flagAttivo) {
		this.flagAttivo = flagAttivo;
	}

	public Boolean getFlagDefault() {
		return flagDefault;
	}

	public void setFlagDefault(Boolean flagDefault) {
		this.flagDefault = flagDefault;
	}

	public String getNomeTemplate() {
		return this.nomeTemplate;
	}

	public void setNomeTemplate(String nomeTemplate) {
		this.nomeTemplate = nomeTemplate;
	}

	public List<PerfRsConfig> getPerfRsConfigs() {
		return this.perfRsConfigs;
	}

	public void setPerfRsConfigs(List<PerfRsConfig> perfRsConfigs) {
		this.perfRsConfigs = perfRsConfigs;
	}

	public PerfRsConfig addPerfRsConfig(PerfRsConfig perfRsConfig) {
		getPerfRsConfigs().add(perfRsConfig);
		perfRsConfig.setPerfRsConfigTemplate(this);

		return perfRsConfig;
	}

	public PerfRsConfig removePerfRsConfig(PerfRsConfig perfRsConfig) {
		getPerfRsConfigs().remove(perfRsConfig);
		perfRsConfig.setPerfRsConfigTemplate(null);

		return perfRsConfig;
	}

}