package com.alkemytech.sophia.performing.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Date;
import java.math.BigInteger;


/**
 * The persistent class for the PERF_PALINSESTO_STORICO database table.
 * 
 */
@Entity
@Table(name="PERF_PALINSESTO_STORICO")
@NamedQuery(name="PerfPalinsestoStorico.findAll", query="SELECT p FROM PerfPalinsestoStorico p")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PerfPalinsestoStorico implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_PALINSESTO_STORICO", unique=true, nullable=false)
	private BigInteger idPalinsestoStorico;

	@Column(name="ATTIVO", nullable=false)
	private Boolean attivo;

	@Column(name="CODICE_DITTA", nullable=false, length=255)
	private String codiceDitta;

	@Column(name="CODICE_ID", length=2)
	private String codiceId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_CREAZIONE", nullable=false)
	private Date dataCreazione;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_FINE_VALIDITA")
	private Date dataFineValidita;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_INIZIO_VALIDITA")
	private Date dataInizioValidita;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_ULTIMA_MODIFICA", nullable=false)
	private Date dataUltimaModifica;

	@Column(name="NOME", nullable=false, length=255)
	private String nome;

	@Column(name="UTENTE_ULTIMA_MODIFICA", length=255)
	private String utenteUltimaModifica;

	//bi-directional many-to-one association to PerfMusicProvider
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MUSIC_PROVIDER")

	private PerfMusicProvider perfMusicProvider;

	//bi-directional many-to-one association to PerfPalinsesto
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PALINSESTO", nullable=false)

	private PerfPalinsesto perfPalinsesto;

	public PerfPalinsestoStorico() {
	}

	public BigInteger getIdPalinsestoStorico() {
		return this.idPalinsestoStorico;
	}

	public void setIdPalinsestoStorico(BigInteger idPalinsestoStorico) {
		this.idPalinsestoStorico = idPalinsestoStorico;
	}

	public Boolean getAttivo() {
		return this.attivo;
	}

	public void setAttivo(Boolean attivo) {
		this.attivo = attivo;
	}

	public String getCodiceDitta() {
		return this.codiceDitta;
	}

	public void setCodiceDitta(String codiceDitta) {
		this.codiceDitta = codiceDitta;
	}

	public String getCodiceId() {
		return this.codiceId;
	}

	public void setCodiceId(String codiceId) {
		this.codiceId = codiceId;
	}

	public Date getDataCreazione() {
		return this.dataCreazione;
	}

	public void setDataCreazione(Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}

	public Date getDataFineValidita() {
		return this.dataFineValidita;
	}

	public void setDataFineValidita(Date dataFineValidita) {
		this.dataFineValidita = dataFineValidita;
	}

	public Date getDataInizioValidita() {
		return this.dataInizioValidita;
	}

	public void setDataInizioValidita(Date dataInizioValidita) {
		this.dataInizioValidita = dataInizioValidita;
	}

	public Date getDataUltimaModifica() {
		return this.dataUltimaModifica;
	}

	public void setDataUltimaModifica(Date dataUltimaModifica) {
		this.dataUltimaModifica = dataUltimaModifica;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getUtenteUltimaModifica() {
		return this.utenteUltimaModifica;
	}

	public void setUtenteUltimaModifica(String utenteUltimaModifica) {
		this.utenteUltimaModifica = utenteUltimaModifica;
	}

	public PerfMusicProvider getPerfMusicProvider() {
		return this.perfMusicProvider;
	}

	public void setPerfMusicProvider(PerfMusicProvider perfMusicProvider) {
		this.perfMusicProvider = perfMusicProvider;
	}

	public PerfPalinsesto getPerfPalinsesto() {
		return this.perfPalinsesto;
	}

	public void setPerfPalinsesto(PerfPalinsesto perfPalinsesto) {
		this.perfPalinsesto = perfPalinsesto;
	}

}