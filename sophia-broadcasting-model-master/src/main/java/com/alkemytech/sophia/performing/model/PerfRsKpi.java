package com.alkemytech.sophia.performing.model;

import com.alkemytech.sophia.broadcasting.dto.IndicatoreDTO;
import com.alkemytech.sophia.broadcasting.dto.KPI;
import com.alkemytech.sophia.broadcasting.dto.performing.PalinsestoDTO;
import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "PERF_RS_KPI")
@SqlResultSetMapping(name="PerfRsIndicatoreMapping",
        classes = {
                @ConstructorResult(targetClass = IndicatoreDTO.class,
                        columns = {
                                @ColumnResult(name="INDICATORE", type = String.class),
                                @ColumnResult(name="VALORE", type = Integer.class),
                                @ColumnResult(name="LAST_UPDATE", type = Date.class)

                        }
                )
        }
)
public class PerfRsKpi{
    public static final String DURATA_DISPONIBILE = "Durata disponibile";
    public static final String DURATA_FILM = "Durata film";
    public static final String DURATA_GENERI_VUOTI = "Durata generi vuoti";
    public static final String DURATA_PUBBLICITA = "Durata pubblicita";
    public static final String DURATA_TRASMISSIONE = "Durata trasmissione";
    public static final String RECORD_IN_RITARDO = "Record in ritardo";
    public static final String RECORD_SCARTATI = "Record scartati";
    public static final String RECORD_TOTALI = "Record totali";
    public static final String DURATA_BRANI = "Durata brani";
    public static final String DURATA_BRANI_DICHIARATI = "Durata totale brani dichiarati";
    public static final String DURATA_BRANI_TITOLI_ERRONEI = "Durata brani titoli erronei";
    public static final String DURATA_BRANI_AUTORI_ERRONEI = "Durata brani autori erronei";
    public static final String DURATA_FILM_TITOLI_ERRONEI = "Durata film titoli erronei";
    public static final String DURATA_PROGRAMMI_TITOLI_ERRONEI = "Durata programmi titoli erronei";
    public static final String TRASMISSIONI_MUSICA_ECCEDENTE = "Trasmissioni musica eccedente";
    public static final String TRASMISSIONI_TOTALI = "Trasmissioni totali";

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="ID_PERF_RS_KPI", nullable=false)
    private BigInteger id;
    
    @OneToOne
    @JoinColumn(name="ID_MUSIC_PROVIDER")
    private PerfMusicProvider musicProvider;
       
    @Column(name="ANNO")
    private int anno;
    
    @Column(name="MESE")
    private int mese;
    
    @Column(name="INDICATORE")
    private String indicatore;
    
    @Column(name="VALORE")
    private Long valore;
    
    @Column(name = "LAST_UPDATE")
    private Date lastUpdate;

    @Transient
    private List<PalinsestoDTO> palinsesti;
    
    @Transient
    private List<Short> mesi;
    
    @Transient
    private List<KPI> listaKPI;

    //bi-directional many-to-one association to PerfPalinsesto
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ID_PALINSESTO", nullable=false)
    @XmlTransient

    private PerfPalinsesto perfPalinsesto;

    public PerfRsKpi() {
    }

    public PerfRsKpi(PerfMusicProvider musicProvider, PerfPalinsesto perfPalinsesto, int anno, int mese, String indicatore, Long valore, Date lastUpdate, List<Short> mesi, List<KPI> listaKPI, List<PalinsestoDTO> palinsesti) {
        this.musicProvider = musicProvider;
        this.perfPalinsesto = perfPalinsesto;
        this.anno = anno;
        this.mese = mese;
        this.indicatore = indicatore;
        this.valore = valore;
        this.lastUpdate = lastUpdate;
        this.mesi = mesi;
        this.listaKPI = listaKPI;
        this.palinsesti = palinsesti;
    }

    @Transient
    public List<Short> getMesi() {
        return mesi;
    }

    public String getMesiToString() {
        String sMesi = "";
        for (Short m: mesi
                ) {
            if(!sMesi.equals("")) {
                sMesi += "," + m;
            }else{
                sMesi += m;
            }
        }
        return sMesi;
    }

    public void setMesi(List<Short> mesi) {
        this.mesi = mesi;
    }

  

    public String getPalinsestoToString () {
        String sCanali = "";
        for (PalinsestoDTO n: palinsesti
                ) {
            if(!sCanali.equals("")) {
                sCanali += "," + n;
            }else{
                sCanali += n;
            }
        }
        return sCanali;
    }

    public void setCanali(List<PalinsestoDTO> palinsesti) {
        this.palinsesti = palinsesti;
    }

    @Transient
    public List<KPI> getListaKPI() {
        return listaKPI;
    }

    public void setListaKPI(List<KPI> listaKPI) {
        this.listaKPI = listaKPI;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PerfRsKpi perfRsKpi = (PerfRsKpi) o;
        return anno == perfRsKpi.anno &&
                mese == perfRsKpi.mese &&
                Objects.equals(id, perfRsKpi.id) &&
                Objects.equals(musicProvider, perfRsKpi.musicProvider) &&
                Objects.equals(indicatore, perfRsKpi.indicatore) &&
                Objects.equals(valore, perfRsKpi.valore) &&
                Objects.equals(lastUpdate, perfRsKpi.lastUpdate) &&
                Objects.equals(palinsesti, perfRsKpi.palinsesti) &&
                Objects.equals(mesi, perfRsKpi.mesi) &&
                Objects.equals(listaKPI, perfRsKpi.listaKPI) &&
                Objects.equals(perfPalinsesto, perfRsKpi.perfPalinsesto);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, musicProvider, anno, mese, indicatore, valore, lastUpdate, palinsesti, mesi, listaKPI, perfPalinsesto);
    }

    @Override
    public String toString() {
        return "PerfRsKpi{" +
                "id=" + id +
                ", musicProvider=" + musicProvider +
                ", anno=" + anno +
                ", mese=" + mese +
                ", indicatore='" + indicatore + '\'' +
                ", valore=" + valore +
                ", lastUpdate=" + lastUpdate +
                ", palinsesti=" + palinsesti +
                ", mesi=" + mesi +
                ", listaKPI=" + listaKPI +
                ", perfPalinsesto=" + perfPalinsesto +
                '}';
    }
}