package com.alkemytech.sophia.performing.service;

import com.alkemytech.sophia.broadcasting.dto.FileArmonizzatoRadioDTO;
import com.alkemytech.sophia.broadcasting.model.BdcBroadcasters;
import com.alkemytech.sophia.broadcasting.model.BdcCanali;

import java.util.List;

public interface PerfFileArmonizzatoService {
    List<FileArmonizzatoRadioDTO> getFileArmonizzatoRadio(BdcBroadcasters broadcasters, List<BdcCanali> canali, Integer anno, List<Integer> mesi);
}
