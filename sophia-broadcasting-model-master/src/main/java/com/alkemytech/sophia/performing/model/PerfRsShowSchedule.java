package com.alkemytech.sophia.performing.model;

import com.alkemytech.sophia.performing.dto.FileArmonizzatoDTO;
import com.fasterxml.jackson.annotation.JsonBackReference;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PERF_RS_SHOW_SCHEDULE database table.
 * 
 */
@Entity
@Table(name="PERF_RS_SHOW_SCHEDULE")
@NamedQuery(name="PerfRsShowSchedule.findAll", query="SELECT p FROM PerfRsShowSchedule p")
@SqlResultSetMapping(
        name = "FileArmonizzatoMapping",
        classes = {
        	@ConstructorResult(targetClass = FileArmonizzatoDTO.class,
					columns = {
							@ColumnResult(name="palinsesto", type = String.class),
							@ColumnResult(name="titoloTrasmissione", type = String.class),
							@ColumnResult(name="dataInizioTrasmissione", type = String.class),
							@ColumnResult(name="orarioInizioTrasmissione", type = String.class),
							@ColumnResult(name="durataTrasmissione", type = String.class),
							@ColumnResult(name="titoloBrano", type = String.class),
							@ColumnResult(name="compositoreAutore", type = String.class),
							@ColumnResult(name="esecutorePerformer", type = String.class),
							@ColumnResult(name="album", type = String.class),
							@ColumnResult(name="durataBrano", type = String.class),
							@ColumnResult(name="codiceISWC", type = String.class),
							@ColumnResult(name="codiceISRC", type = String.class),
							@ColumnResult(name="orarioInizioBrano", type = String.class),
							@ColumnResult(name="categoriaDUso", type = String.class),
							@ColumnResult(name="diffusioneRegionale", type = Integer.class),
							@ColumnResult(name="stato", type = String.class),
							@ColumnResult(name="errore", type = String.class),
							@ColumnResult(name="altriMotiviDiScarto", type = String.class),
					}
			)
		})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PerfRsShowSchedule implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_RS_SHOW_SCHEDULE", unique=true, nullable=false)
	private Long idRsShowSchedule;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="BEGIN_TIME")
	private Date beginTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Column(name="DAYS_FROM_UPLOAD", nullable=false)
	private int daysFromUpload;

	@Column(name="DURATION")
	private int duration;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="END_TIME")
	private Date endTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="MODIFY_DATE")
	private Date modifyDate;

	@Column(name="NOTE", length=400)
	private String note;

	@Column(name="OVERLAP")
	private Boolean overlap;

	@Column(name="REGIONAL_OFFICE", length=10)
	private String regionalOffice;

	@Column(name="SCHEDULE_MONTH", nullable=false)
	private int scheduleMonth;

	@Column(name="SCHEDULE_YEAR", nullable=false)
	private int scheduleYear;

	@Column(name="TITLE", length=400)
	private String title;

	//bi-directional many-to-one association to PerfRsShowMusic
	@OneToMany(mappedBy="perfRsShowSchedule")

	@XmlTransient
	private List<PerfRsShowMusic> perfRsShowMusics;

	//bi-directional many-to-one association to PerfRsNormalizedFile
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_NORMALIZED_FILE", nullable=false)

	private PerfRsNormalizedFile perfRsNormalizedFile;

	//bi-directional many-to-one association to PerfPalinsesto
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PALINSESTO", nullable=false)

	private PerfPalinsesto perfPalinsesto;

	public PerfRsShowSchedule() {
	}

	public Long getIdRsShowSchedule() {
		return this.idRsShowSchedule;
	}

	public void setIdRsShowSchedule(Long idRsShowSchedule) {
		this.idRsShowSchedule = idRsShowSchedule;
	}

	public Date getBeginTime() {
		return this.beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public int getDaysFromUpload() {
		return this.daysFromUpload;
	}

	public void setDaysFromUpload(int daysFromUpload) {
		this.daysFromUpload = daysFromUpload;
	}

	public int getDuration() {
		return this.duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public Date getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Date getModifyDate() {
		return this.modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getNote() {
		return this.note;
	}

	public void setNote(String note) {
		this.note = note;
	}



	public String getRegionalOffice() {
		return this.regionalOffice;
	}

	public void setRegionalOffice(String regionalOffice) {
		this.regionalOffice = regionalOffice;
	}

	public int getScheduleMonth() {
		return this.scheduleMonth;
	}

	public void setScheduleMonth(int scheduleMonth) {
		this.scheduleMonth = scheduleMonth;
	}

	public int getScheduleYear() {
		return this.scheduleYear;
	}

	public void setScheduleYear(int scheduleYear) {
		this.scheduleYear = scheduleYear;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<PerfRsShowMusic> getPerfRsShowMusics() {
		return this.perfRsShowMusics;
	}

	public void setPerfRsShowMusics(List<PerfRsShowMusic> perfRsShowMusics) {
		this.perfRsShowMusics = perfRsShowMusics;
	}

	public PerfRsShowMusic addPerfRsShowMusic(PerfRsShowMusic perfRsShowMusic) {
		getPerfRsShowMusics().add(perfRsShowMusic);
		perfRsShowMusic.setPerfRsShowSchedule(this);

		return perfRsShowMusic;
	}

	public PerfRsShowMusic removePerfRsShowMusic(PerfRsShowMusic perfRsShowMusic) {
		getPerfRsShowMusics().remove(perfRsShowMusic);
		perfRsShowMusic.setPerfRsShowSchedule(null);

		return perfRsShowMusic;
	}

	public PerfRsNormalizedFile getPerfRsNormalizedFile() {
		return this.perfRsNormalizedFile;
	}

	public void setPerfRsNormalizedFile(PerfRsNormalizedFile perfRsNormalizedFile) {
		this.perfRsNormalizedFile = perfRsNormalizedFile;
	}

	public PerfPalinsesto getPerfPalinsesto() {
		return this.perfPalinsesto;
	}

	public void setPerfPalinsesto(PerfPalinsesto perfPalinsesto) {
		this.perfPalinsesto = perfPalinsesto;
	}

}