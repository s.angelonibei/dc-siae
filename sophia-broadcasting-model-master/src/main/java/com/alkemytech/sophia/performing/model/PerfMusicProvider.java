package com.alkemytech.sophia.performing.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PERF_MUSIC_PROVIDER database table.
 * 
 */
@Entity
@Table(name="PERF_MUSIC_PROVIDER")
@NamedQuery(name="PerfMusicProvider.findAll", query="SELECT p FROM PerfMusicProvider p")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PerfMusicProvider implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_MUSIC_PROVIDER", unique=true, nullable=false)
	private BigInteger idMusicProvider;

	@Column(name="ATTIVO", nullable=false)
	private Boolean attivo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_CREAZIONE", columnDefinition = "datetime NOT NULL DEFAULT CURRENT_TIMESTAMP", insertable = false, updatable = false)
	private Date dataCreazione;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_FINE_VALIDITA")
	private Date dataFineValidita;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_INIZIO_VALIDITA")
	private Date dataInizioValidita;

	@Column(name="NOME", nullable=false, length=255)
	private String nome;

	//bi-directional many-to-one association to PerfNew

	@OneToMany(mappedBy="perfMusicProvider")
	@XmlTransient
	private List<PerfNews> perfNews;

	//bi-directional many-to-one association to PerfPalinsesto

	@OneToMany(mappedBy="perfMusicProvider")
	@XmlTransient
	private List<PerfPalinsesto> perfPalinsestos;

	//bi-directional many-to-one association to PerfPalinsestoStorico

	@OneToMany(mappedBy="perfMusicProvider")
	@XmlTransient
	private List<PerfPalinsestoStorico> perfPalinsestoStoricos;

	//bi-directional many-to-one association to PerfRsUtente

	@OneToMany(mappedBy="perfMusicProvider")
	@XmlTransient
	private List<PerfRsUtente> perfRsUtentes;

	//bi-directional many-to-one association to PerfRsUtilizationFile

	@OneToMany(mappedBy="perfMusicProvider")
	@XmlTransient
	private List<PerfRsUtilizationFile> perfRsUtilizationFiles;

	//bi-directional many-to-one association to PerfRsConfig

	@OneToMany(mappedBy="perfMusicProvider")
	@XmlTransient
	private List<PerfRsConfig> perfRsConfigs;

	public PerfMusicProvider() {
	}

	public BigInteger getIdMusicProvider() {
		return this.idMusicProvider;
	}

	public void setIdMusicProvider(BigInteger idMusicProvider) {
		this.idMusicProvider = idMusicProvider;
	}

	public Boolean getAttivo() {
		return this.attivo;
	}

	public void setAttivo(Boolean attivo) {
		this.attivo = attivo;
	}

	public Date getDataCreazione() {
		return this.dataCreazione;
	}

	public void setDataCreazione(Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}

	public Date getDataFineValidita() {
		return this.dataFineValidita;
	}

	public void setDataFineValidita(Date dataFineValidita) {
		this.dataFineValidita = dataFineValidita;
	}

	public Date getDataInizioValidita() {
		return this.dataInizioValidita;
	}

	public void setDataInizioValidita(Date dataInizioValidita) {
		this.dataInizioValidita = dataInizioValidita;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<PerfNews> getPerfNews() {
		return this.perfNews;
	}

	public void setPerfNews(List<PerfNews> perfNews) {
		this.perfNews = perfNews;
	}

	public PerfNews addPerfNew(PerfNews perfNew) {
		getPerfNews().add(perfNew);
		perfNew.setPerfMusicProvider(this);

		return perfNew;
	}

	public PerfNews removePerfNew(PerfNews perfNew) {
		getPerfNews().remove(perfNew);
		perfNew.setPerfMusicProvider(null);

		return perfNew;
	}

	public List<PerfPalinsesto> getPerfPalinsestos() {
		return this.perfPalinsestos;
	}

	public void setPerfPalinsestos(List<PerfPalinsesto> perfPalinsestos) {
		this.perfPalinsestos = perfPalinsestos;
	}

	public PerfPalinsesto addPerfPalinsesto(PerfPalinsesto perfPalinsesto) {
		getPerfPalinsestos().add(perfPalinsesto);
		perfPalinsesto.setPerfMusicProvider(this);

		return perfPalinsesto;
	}

	public PerfPalinsesto removePerfPalinsesto(PerfPalinsesto perfPalinsesto) {
		getPerfPalinsestos().remove(perfPalinsesto);
		perfPalinsesto.setPerfMusicProvider(null);

		return perfPalinsesto;
	}

	public List<PerfPalinsestoStorico> getPerfPalinsestoStoricos() {
		return this.perfPalinsestoStoricos;
	}

	public void setPerfPalinsestoStoricos(List<PerfPalinsestoStorico> perfPalinsestoStoricos) {
		this.perfPalinsestoStoricos = perfPalinsestoStoricos;
	}

	public PerfPalinsestoStorico addPerfPalinsestoStorico(PerfPalinsestoStorico perfPalinsestoStorico) {
		getPerfPalinsestoStoricos().add(perfPalinsestoStorico);
		perfPalinsestoStorico.setPerfMusicProvider(this);

		return perfPalinsestoStorico;
	}

	public PerfPalinsestoStorico removePerfPalinsestoStorico(PerfPalinsestoStorico perfPalinsestoStorico) {
		getPerfPalinsestoStoricos().remove(perfPalinsestoStorico);
		perfPalinsestoStorico.setPerfMusicProvider(null);

		return perfPalinsestoStorico;
	}

	public List<PerfRsUtente> getPerfRsUtentes() {
		return this.perfRsUtentes;
	}

	public void setPerfRsUtentes(List<PerfRsUtente> perfRsUtentes) {
		this.perfRsUtentes = perfRsUtentes;
	}

	public PerfRsUtente addPerfRsUtente(PerfRsUtente perfRsUtente) {
		getPerfRsUtentes().add(perfRsUtente);
		perfRsUtente.setPerfMusicProvider(this);

		return perfRsUtente;
	}

	public PerfRsUtente removePerfRsUtente(PerfRsUtente perfRsUtente) {
		getPerfRsUtentes().remove(perfRsUtente);
		perfRsUtente.setPerfMusicProvider(null);

		return perfRsUtente;
	}

	public List<PerfRsUtilizationFile> getPerfRsUtilizationFiles() {
		return this.perfRsUtilizationFiles;
	}

	public void setPerfRsUtilizationFiles(List<PerfRsUtilizationFile> perfRsUtilizationFiles) {
		this.perfRsUtilizationFiles = perfRsUtilizationFiles;
	}

	public PerfRsUtilizationFile addPerfRsUtilizationFile(PerfRsUtilizationFile perfRsUtilizationFile) {
		getPerfRsUtilizationFiles().add(perfRsUtilizationFile);
		perfRsUtilizationFile.setPerfMusicProvider(this);

		return perfRsUtilizationFile;
	}

	public PerfRsUtilizationFile removePerfRsUtilizationFile(PerfRsUtilizationFile perfRsUtilizationFile) {
		getPerfRsUtilizationFiles().remove(perfRsUtilizationFile);
		perfRsUtilizationFile.setPerfMusicProvider(null);

		return perfRsUtilizationFile;
	}

	public List<PerfRsConfig> getPerfRsConfigs() {
		return this.perfRsConfigs;
	}

	public void setPerfRsConfigs(List<PerfRsConfig> perfRsConfigs) {
		this.perfRsConfigs = perfRsConfigs;
	}

	public PerfRsConfig addPerfRsConfig(PerfRsConfig perfRsConfig) {
		getPerfRsConfigs().add(perfRsConfig);
		perfRsConfig.setPerfMusicProvider(this);

		return perfRsConfig;
	}

	public PerfRsConfig removePerfRsConfig(PerfRsConfig perfRsConfig) {
		getPerfRsConfigs().remove(perfRsConfig);
		perfRsConfig.setPerfMusicProvider(null);

		return perfRsConfig;
	}
}