package com.alkemytech.sophia.performing.service;

import com.alkemytech.sophia.broadcasting.dto.*;
import com.alkemytech.sophia.broadcasting.model.BdcBroadcasters;
import com.alkemytech.sophia.broadcasting.model.BdcCanali;
import com.alkemytech.sophia.broadcasting.model.TipoBroadcaster;
import com.alkemytech.sophia.broadcasting.model.utilizations.*;
import com.alkemytech.sophia.broadcasting.utils.EntityToFieldConverter;
import com.google.inject.Inject;
import com.google.inject.Provider;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PerfFileArmonizzatoServiceImpl implements PerfFileArmonizzatoService {

	private final Provider<EntityManager> provider;

	@Inject
	public PerfFileArmonizzatoServiceImpl(Provider<EntityManager> provider) {
		this.provider = provider;
	}

	@Override
	public List<FileArmonizzatoRadioDTO> getFileArmonizzatoRadio(BdcBroadcasters broadcasters, List<BdcCanali> canali,
																 Integer anno, List<Integer> mesi) {
		EntityManager entityManager = provider.get();
		List<FileArmonizzatoRadioDTO> fileArmonizzatoRadioDTOS = new ArrayList<>();

		List<Integer> selectedValues = new ArrayList<>();
		for (BdcCanali bdcCanale : canali) {
			selectedValues.add(bdcCanale.getId());
		}

		StringBuffer nomeMesi = new StringBuffer();
		nomeMesi.append("(");
		for (Integer mese : mesi) {
			nomeMesi.append(mese + ", ");
		}
		nomeMesi.delete(nomeMesi.length() - 2, nomeMesi.length());
		nomeMesi.append(")");

		String w ="SELECT * FROM " +
				"(SELECT " +
				"bdc_canali.nome AS canale, " +
				"BDC_RD_SHOW_SCHEDULE.TITLE AS titoloTrasmissione, " +
				"DATE_FORMAT(BDC_RD_SHOW_SCHEDULE.BEGIN_TIME, '%d/%m/%Y') AS dataInizioTrasmissione, " +
				"IF(DATE_FORMAT(BDC_RD_SHOW_SCHEDULE.BEGIN_TIME, '%H:%i:%s') = '00:00:00', NULL, DATE_FORMAT(BDC_RD_SHOW_SCHEDULE.BEGIN_TIME, '%H:%i:%s'))  AS orarioInizioTrasmissione, " +
				"CAST(SEC_TO_TIME(BDC_RD_SHOW_SCHEDULE.DURATION) AS CHAR(11)) AS durataTrasmissione, " +
				"BDC_RD_SHOW_MUSIC.TITLE AS titoloBrano, " +
				"BDC_RD_SHOW_MUSIC.COMPOSER AS compositoreAutore, " +
				"BDC_RD_SHOW_MUSIC.PERFORMER AS esecutorePerformer, " +
				"BDC_RD_SHOW_MUSIC.ALBUM AS album, " +
				"CAST(SEC_TO_TIME(BDC_RD_SHOW_MUSIC.DURATION) AS CHAR(11)) AS durataBrano, " +
				"BDC_RD_SHOW_MUSIC.ISWC_CODE AS codiceISWC, " +
				"BDC_RD_SHOW_MUSIC.ISRC_CODE AS codiceISRC, " +
				"DATE_FORMAT(BDC_RD_SHOW_MUSIC.BEGIN_TIME, '%H:%i:%s')  AS orarioInizioBrano, " +
				"BDC_RD_SHOW_MUSIC.ID_MUSIC_TYPE AS categoriaDUso, " +
				"IF(BDC_RD_SHOW_SCHEDULE.REGIONAL_OFFICE IS NULL, 0, 1) AS diffusioneRegionale, " +
				"'OK' AS stato, "+
				"NULL AS errore, " +
				"NULL AS erroreGlobale, " +
				"BDC_RD_SHOW_MUSIC.ID_RD_SHOW_MUSIC, " +
				"CONCAT(IF(DATE_FORMAT(BDC_RD_SHOW_SCHEDULE.BEGIN_TIME, '%Y/%m/%d') IS NULL, COALESCE(NULL, ''), DATE_FORMAT(BDC_RD_SHOW_SCHEDULE.BEGIN_TIME, '%Y/%m/%d')), IF(IF(bdc_canali.special_radio = 0, null, DATE_FORMAT(BDC_RD_SHOW_SCHEDULE.BEGIN_TIME, '%H:%i:%s')) IS NULL, COALESCE(NULL, ''), IF(bdc_canali.special_radio = 0, null, DATE_FORMAT(BDC_RD_SHOW_SCHEDULE.BEGIN_TIME, '%H:%i:%s')))) as ordi " +
				"FROM BDC_RD_SHOW_MUSIC " +
				"RIGHT JOIN BDC_RD_SHOW_SCHEDULE " +
				"ON BDC_RD_SHOW_MUSIC.ID_RD_SHOW_SCHEDULE=BDC_RD_SHOW_SCHEDULE.ID_RD_SHOW_SCHEDULE " +
				"LEFT JOIN bdc_canali " +
				"ON BDC_RD_SHOW_SCHEDULE.ID_CHANNEL=bdc_canali.id " +
				"WHERE BDC_RD_SHOW_SCHEDULE.ID_CHANNEL IN :canali " +
				"AND BDC_RD_SHOW_SCHEDULE.SCHEDULE_YEAR = :anno " +
				"AND BDC_RD_SHOW_SCHEDULE.SCHEDULE_MONTH IN :mesi " +
				"GROUP BY BDC_RD_SHOW_MUSIC.ID_RD_SHOW_MUSIC " +
				"ORDER BY ordi ASC) AS validi " +
				"UNION ALL " +
				"SELECT * FROM " +
				"(SELECT " +
				"IF(BDC_ERR_RD_SHOW_SCHEDULE.ID_CHANNEL IS NULL, BDC_ERR_RD_SHOW_SCHEDULE.REPORT_CHANNEL, bdc_canali.nome) AS canale, " +
				"BDC_ERR_RD_SHOW_SCHEDULE.TITLE AS titoloTrasmissione, " +
				"BDC_ERR_RD_SHOW_SCHEDULE.REPORT_BEGIN_DATE AS dataInizioTrasmissione, " +
				"BDC_ERR_RD_SHOW_SCHEDULE.REPORT_BEGIN_TIME AS orarioInizioTrasmissione, " +
				"IF(BDC_ERR_RD_SHOW_SCHEDULE.DURATION IS NULL, BDC_ERR_RD_SHOW_SCHEDULE.REPORT_DURATION, CAST(SEC_TO_TIME(BDC_ERR_RD_SHOW_SCHEDULE.DURATION) AS CHAR(11))) AS durataTrasmissione, " +
				"BDC_ERR_RD_SHOW_MUSIC.TITLE AS titoloBrano, " +
				"BDC_ERR_RD_SHOW_MUSIC.COMPOSER AS compositoreAutore, " +
				"BDC_ERR_RD_SHOW_MUSIC.PERFORMER AS esecutorePerformer, " +
				"BDC_ERR_RD_SHOW_MUSIC.ALBUM AS album, " +
				"IF(BDC_ERR_RD_SHOW_MUSIC.DURATION IS NULL, BDC_ERR_RD_SHOW_MUSIC.REPORT_DURATION, CAST(SEC_TO_TIME(BDC_ERR_RD_SHOW_MUSIC.DURATION) AS CHAR(11))) AS durataBrano, " +
				"BDC_ERR_RD_SHOW_MUSIC.ISWC_CODE AS codiceISWC, " +
				"BDC_ERR_RD_SHOW_MUSIC.ISRC_CODE AS codiceISRC, " +
				"IF(BDC_ERR_RD_SHOW_MUSIC.BEGIN_TIME IS NULL, BDC_ERR_RD_SHOW_MUSIC.REPORT_BEGIN_TIME, DATE_FORMAT(BDC_ERR_RD_SHOW_MUSIC.BEGIN_TIME, '%H:%i:%s')) AS orarioInizioBrano, " +
				"IF(BDC_ERR_RD_SHOW_MUSIC.ID_MUSIC_TYPE IS NULL, BDC_ERR_RD_SHOW_MUSIC.REPORT_MUSIC_TYPE, BDC_ERR_RD_SHOW_MUSIC.ID_MUSIC_TYPE) AS categoriaDUso, " +
				"IF(BDC_ERR_RD_SHOW_SCHEDULE.REGIONAL_OFFICE IS NULL, 0, 1) AS diffusioneRegionale, " +
				"'KO' AS stato, "+
				"BDC_ERR_RD_SHOW_SCHEDULE.ERROR AS errore, " +
				"BDC_ERR_RD_SHOW_SCHEDULE.GLOBAL_ERROR AS erroreGlobale, " +
				"BDC_ERR_RD_SHOW_MUSIC.ID_ERR_RD_SHOW_MUSIC, " +
				"CONCAT(IF(IF(BDC_ERR_RD_SHOW_SCHEDULE.BEGIN_TIME IS NULL, BDC_ERR_RD_SHOW_SCHEDULE.REPORT_BEGIN_DATE, DATE_FORMAT(BDC_ERR_RD_SHOW_SCHEDULE.BEGIN_TIME, '%Y/%m/%d')) IS NULL, COALESCE(NULL, ''), IF(BDC_ERR_RD_SHOW_SCHEDULE.BEGIN_TIME IS NULL, BDC_ERR_RD_SHOW_SCHEDULE.REPORT_BEGIN_DATE, DATE_FORMAT(BDC_ERR_RD_SHOW_SCHEDULE.BEGIN_TIME, '%Y/%m/%d'))), IF(IF (bdc_canali.special_radio = 0, NULL, IF(BDC_ERR_RD_SHOW_SCHEDULE.BEGIN_TIME IS NULL, BDC_ERR_RD_SHOW_SCHEDULE.REPORT_BEGIN_TIME, DATE_FORMAT(BDC_ERR_RD_SHOW_SCHEDULE.BEGIN_TIME, '%H:%i:%s'))) IS NULL, COALESCE(NULL, ''), IF (bdc_canali.special_radio = 0, NULL, IF(BDC_ERR_RD_SHOW_SCHEDULE.BEGIN_TIME IS NULL, BDC_ERR_RD_SHOW_SCHEDULE.REPORT_BEGIN_TIME, DATE_FORMAT(BDC_ERR_RD_SHOW_SCHEDULE.BEGIN_TIME, '%H:%i:%s'))))) as ordi " +
				"FROM BDC_ERR_RD_SHOW_MUSIC " +
				"RIGHT JOIN BDC_ERR_RD_SHOW_SCHEDULE " +
				"ON BDC_ERR_RD_SHOW_MUSIC.ID_ERR_RD_SHOW_SCHEDULE=BDC_ERR_RD_SHOW_SCHEDULE.ID_ERR_RD_SHOW_SCHEDULE " +
				"LEFT JOIN bdc_canali " +
				"ON BDC_ERR_RD_SHOW_SCHEDULE.ID_CHANNEL=bdc_canali.id " +
				"LEFT JOIN BDC_UTILIZATION_NORMALIZED_FILE " +
				"ON BDC_ERR_RD_SHOW_SCHEDULE.ID_NORMALIZED_FILE = BDC_UTILIZATION_NORMALIZED_FILE.ID_NORMALIZED_FILE " +
				"LEFT JOIN bdc_utilization_file " +
				"ON BDC_UTILIZATION_NORMALIZED_FILE.ID_UTILIZATION_FILE = bdc_utilization_file.id " +
				"WHERE ((BDC_ERR_RD_SHOW_SCHEDULE.ID_CHANNEL IN :canali AND bdc_utilization_file.canale IS NULL) OR bdc_utilization_file.canale IN :canali) " +
				"AND ((BDC_ERR_RD_SHOW_SCHEDULE.SCHEDULE_YEAR = :anno AND bdc_utilization_file.anno IS NULL) OR (bdc_utilization_file.anno = :anno)) " +
				"AND ((BDC_ERR_RD_SHOW_SCHEDULE.SCHEDULE_MONTH IN :mesi AND bdc_utilization_file.mese IS NULL) OR bdc_utilization_file.mese IN :mesi ) " +
				"GROUP BY BDC_ERR_RD_SHOW_MUSIC.ID_ERR_RD_SHOW_MUSIC " +
				"ORDER BY ordi ASC) AS errori " +
				"ORDER BY ordi ASC ";

		final Query q = entityManager.createNativeQuery(w);

		List<Object[]> result = (List<Object[]>) q.getResultList();
		for (Object[] object : result) {
			fileArmonizzatoRadioDTOS.add(new FileArmonizzatoRadioDTO(object));
		}

		return fileArmonizzatoRadioDTOS;
	}
}
