package com.alkemytech.sophia.performing.model;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * The persistent class for the PERF_RS_ERR_COMPOSITORE_TITOLO database table.
 * 
 */
@Entity
@Table(name="PERF_RS_ERR_COMPOSITORE_TITOLO")
@NamedQuery(name="PerfRsErrCompositoreTitolo.findAll", query="SELECT p FROM PerfRsErrCompositoreTitolo p")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PerfRsErrCompositoreTitolo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_RS_ERR_COMPOSITORE_TITOLO", unique=true, nullable=false)
	private Long idRsErrCompositoreTitolo;

	@Column(name="RS_ERR_COMPOSITORE_TITOLO", length=400)
	private String rsErrCompositoreTitolo;

	public PerfRsErrCompositoreTitolo() {
	}

	public Long getIdRsErrCompositoreTitolo() {
		return idRsErrCompositoreTitolo;
	}

	public void setIdRsErrCompositoreTitolo(Long idRsErrCompositoreTitolo) {
		this.idRsErrCompositoreTitolo = idRsErrCompositoreTitolo;
	}

	public String getRsErrCompositoreTitolo() {
		return this.rsErrCompositoreTitolo;
	}

	public void setRsErrCompositoreTitolo(String rsErrCompositoreTitolo) {
		this.rsErrCompositoreTitolo = rsErrCompositoreTitolo;
	}

}