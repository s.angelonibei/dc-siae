package com.alkemytech.sophia.performing.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Date;


/**
 * The persistent class for the PERF_RS_ERR_SHOW_MUSIC database table.
 * 
 */
@Entity
@Table(name="PERF_RS_ERR_SHOW_MUSIC")
@NamedQuery(name="PerfRsErrShowMusic.findAll", query="SELECT p FROM PerfRsErrShowMusic p")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PerfRsErrShowMusic implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_RS_ERR_SHOW_MUSIC", unique=true, nullable=false)
	private Long idRsErrShowMusic;

	@Lob
	@Column(name="ALBUM")
	private String album;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="BEGIN_TIME")
	private Date beginTime;

	@Lob
	@Column(name="COMPOSER")
	private String composer;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATION_DATE", nullable=false)
	private Date creationDate;

	@Column(name="DAYS_FROM_UPLOAD")
	private int daysFromUpload;

	@Column(name="DURATION")
	private int duration;

	@Column(name="ID_MUSIC_TYPE")
	private java.math.BigInteger idMusicType;

	@Column(name="ISRC_CODE", length=50)
	private String isrcCode;

	@Column(name="ISWC_CODE", length=50)
	private String iswcCode;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="MODIFY_DATE")
	private Date modifyDate;

	@Column(name="PERFORMER")
	private String performer;

	@Column(name="REAL_DURATION")
	private int realDuration;

	@Column(name="REPORT_BEGIN_TIME", length=100)
	private String reportBeginTime;

	@Column(name="REPORT_DURATION", length=100)
	private String reportDuration;

	@Column(name="REPORT_MUSIC_TYPE", length=100)
	private String reportMusicType;

	@Column(name="TITLE")
	private String title;

	//bi-directional many-to-one association to PerfRsErrShowSchedule
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_RS_ERR_SHOW_SCHEDULE")

	private PerfRsErrShowSchedule perfRsErrShowSchedule;

	//bi-directional many-to-one association to PerfRsNormalizedFile
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_NORMALIZED_FILE", nullable=false)

	private PerfRsNormalizedFile perfRsNormalizedFile;

	public PerfRsErrShowMusic() {
	}

	public Long getIdRsErrShowMusic() {
		return this.idRsErrShowMusic;
	}

	public void setIdRsErrShowMusic(Long idRsErrShowMusic) {
		this.idRsErrShowMusic = idRsErrShowMusic;
	}

	public String getAlbum() {
		return this.album;
	}

	public void setAlbum(String album) {
		this.album = album;
	}

	public Date getBeginTime() {
		return this.beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public String getComposer() {
		return this.composer;
	}

	public void setComposer(String composer) {
		this.composer = composer;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public int getDaysFromUpload() {
		return this.daysFromUpload;
	}

	public void setDaysFromUpload(int daysFromUpload) {
		this.daysFromUpload = daysFromUpload;
	}

	public int getDuration() {
		return this.duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public java.math.BigInteger getIdMusicType() {
		return this.idMusicType;
	}

	public void setIdMusicType(java.math.BigInteger idMusicType) {
		this.idMusicType = idMusicType;
	}

	public String getIsrcCode() {
		return this.isrcCode;
	}

	public void setIsrcCode(String isrcCode) {
		this.isrcCode = isrcCode;
	}

	public String getIswcCode() {
		return this.iswcCode;
	}

	public void setIswcCode(String iswcCode) {
		this.iswcCode = iswcCode;
	}

	public Date getModifyDate() {
		return this.modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getPerformer() {
		return this.performer;
	}

	public void setPerformer(String performer) {
		this.performer = performer;
	}

	public int getRealDuration() {
		return this.realDuration;
	}

	public void setRealDuration(int realDuration) {
		this.realDuration = realDuration;
	}

	public String getReportBeginTime() {
		return this.reportBeginTime;
	}

	public void setReportBeginTime(String reportBeginTime) {
		this.reportBeginTime = reportBeginTime;
	}

	public String getReportDuration() {
		return this.reportDuration;
	}

	public void setReportDuration(String reportDuration) {
		this.reportDuration = reportDuration;
	}

	public String getReportMusicType() {
		return this.reportMusicType;
	}

	public void setReportMusicType(String reportMusicType) {
		this.reportMusicType = reportMusicType;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public PerfRsErrShowSchedule getPerfRsErrShowSchedule() {
		return this.perfRsErrShowSchedule;
	}

	public void setPerfRsErrShowSchedule(PerfRsErrShowSchedule perfRsErrShowSchedule) {
		this.perfRsErrShowSchedule = perfRsErrShowSchedule;
	}

	public PerfRsNormalizedFile getPerfRsNormalizedFile() {
		return this.perfRsNormalizedFile;
	}

	public void setPerfRsNormalizedFile(PerfRsNormalizedFile perfRsNormalizedFile) {
		this.perfRsNormalizedFile = perfRsNormalizedFile;
	}

}