package com.alkemytech.sophia.performing.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Date;


/**
 * The persistent class for the PERF_RS_SHOW_MUSIC database table.
 * 
 */
@Entity
@Table(name="PERF_RS_SHOW_MUSIC")
@NamedQuery(name="PerfRsShowMusic.findAll", query="SELECT p FROM PerfRsShowMusic p")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PerfRsShowMusic implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_RS_SHOW_MUSIC", unique=true, nullable=false)
	private Long idRsShowMusic;

	@Lob
	@Column(name="ALBUM")
	private String album;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="BEGIN_TIME")
	private Date beginTime;

	@Lob
	@Column(name="COMPOSER")
	private String composer;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Column(name="DAYS_FROM_UPLOAD", nullable=false)
	private int daysFromUpload;

	@Column(name="DURATION")
	private int duration;

	@Column(name="ID_MUSIC_TYPE")
	private java.math.BigInteger idMusicType;

	@Column(name="ISRC_CODE", length=50)
	private String isrcCode;

	@Column(name="ISWC_CODE", length=50)
	private String iswcCode;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="MODIFY_DATE")
	private Date modifyDate;

	@Column(name="NOTE", length=400)
	private String note;

	@Lob
	@Column(name="PERFORMER")
	private String performer;

	@Column(name="REAL_DURATION")
	private int realDuration;

	@Lob
	@Column(name="TITLE")
	private String title;

	//bi-directional many-to-one association to PerfRsNormalizedFile
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_NORMALIZED_FILE", nullable=false)

	private PerfRsNormalizedFile perfRsNormalizedFile;

	//bi-directional many-to-one association to PerfRsShowSchedule
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_RS_SHOW_SCHEDULE")

	private PerfRsShowSchedule perfRsShowSchedule;

	public PerfRsShowMusic() {
	}

	public Long getIdRsShowMusic() {
		return this.idRsShowMusic;
	}

	public void setIdRsShowMusic(Long idRsShowMusic) {
		this.idRsShowMusic = idRsShowMusic;
	}

	public String getAlbum() {
		return this.album;
	}

	public void setAlbum(String album) {
		this.album = album;
	}

	public Date getBeginTime() {
		return this.beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public String getComposer() {
		return this.composer;
	}

	public void setComposer(String composer) {
		this.composer = composer;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public int getDaysFromUpload() {
		return this.daysFromUpload;
	}

	public void setDaysFromUpload(int daysFromUpload) {
		this.daysFromUpload = daysFromUpload;
	}

	public int getDuration() {
		return this.duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public java.math.BigInteger getIdMusicType() {
		return this.idMusicType;
	}

	public void setIdMusicType(java.math.BigInteger idMusicType) {
		this.idMusicType = idMusicType;
	}

	public String getIsrcCode() {
		return this.isrcCode;
	}

	public void setIsrcCode(String isrcCode) {
		this.isrcCode = isrcCode;
	}

	public String getIswcCode() {
		return this.iswcCode;
	}

	public void setIswcCode(String iswcCode) {
		this.iswcCode = iswcCode;
	}

	public Date getModifyDate() {
		return this.modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getNote() {
		return this.note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getPerformer() {
		return this.performer;
	}

	public void setPerformer(String performer) {
		this.performer = performer;
	}

	public int getRealDuration() {
		return this.realDuration;
	}

	public void setRealDuration(int realDuration) {
		this.realDuration = realDuration;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public PerfRsNormalizedFile getPerfRsNormalizedFile() {
		return this.perfRsNormalizedFile;
	}

	public void setPerfRsNormalizedFile(PerfRsNormalizedFile perfRsNormalizedFile) {
		this.perfRsNormalizedFile = perfRsNormalizedFile;
	}

	public PerfRsShowSchedule getPerfRsShowSchedule() {
		return this.perfRsShowSchedule;
	}

	public void setPerfRsShowSchedule(PerfRsShowSchedule perfRsShowSchedule) {
		this.perfRsShowSchedule = perfRsShowSchedule;
	}

}