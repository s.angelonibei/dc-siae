package com.alkemytech.sophia.performing.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Date;
import java.math.BigInteger;
import java.util.List;


/**
 * The persistent class for the PERF_RS_UTENTE database table.
 * 
 */
@Entity
@Table(name="PERF_RS_UTENTE")
@NamedQuery(name="PerfRsUtente.findAll", query="SELECT p FROM PerfRsUtente p")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PerfRsUtente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID", unique=true, nullable=false)
	private BigInteger id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_CREAZIONE")
	private Date dataCreazione;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_ULTIMA_MODIFICA")
	private Date dataUltimaModifica;

	@Column(name="EMAIL", nullable=false, length=255)
	private String email;

	@Column(name="ID_RUOLO", nullable=false)
	private int idRuolo;

	@Column(name="PASSWORD", nullable=false, length=255)
	private String password;

	@Column(name="USERNAME", nullable=false, length=255)
	private String username;

	@Column(name="UTENTE_ULTIMA_MODIFICA", length=255)
	private String utenteUltimaModifica;

	//bi-directional many-to-one association to PerfNew

	@OneToMany(mappedBy="perfRsUtente")
	@XmlTransient
	private List<PerfNews> perfNews;

	//bi-directional many-to-one association to PerfMusicProvider

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MUSIC_PROVIDER", nullable=false)
	private PerfMusicProvider perfMusicProvider;

	//bi-directional many-to-one association to PerfRsUtilizationFile

	@OneToMany(mappedBy="perfRsUtente")
	@XmlTransient
	private List<PerfRsUtilizationFile> perfRsUtilizationFiles;

	public PerfRsUtente() {
	}

	public BigInteger getId() {
		return this.id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public Date getDataCreazione() {
		return this.dataCreazione;
	}

	public void setDataCreazione(Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}

	public Date getDataUltimaModifica() {
		return this.dataUltimaModifica;
	}

	public void setDataUltimaModifica(Date dataUltimaModifica) {
		this.dataUltimaModifica = dataUltimaModifica;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getIdRuolo() {
		return this.idRuolo;
	}

	public void setIdRuolo(int idRuolo) {
		this.idRuolo = idRuolo;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUtenteUltimaModifica() {
		return this.utenteUltimaModifica;
	}

	public void setUtenteUltimaModifica(String utenteUltimaModifica) {
		this.utenteUltimaModifica = utenteUltimaModifica;
	}

	public List<PerfNews> getPerfNews() {
		return this.perfNews;
	}

	public void setPerfNews(List<PerfNews> perfNews) {
		this.perfNews = perfNews;
	}

	public PerfNews addPerfNew(PerfNews perfNew) {
		getPerfNews().add(perfNew);
		perfNew.setPerfRsUtente(this);

		return perfNew;
	}

	public PerfNews removePerfNew(PerfNews perfNew) {
		getPerfNews().remove(perfNew);
		perfNew.setPerfRsUtente(null);

		return perfNew;
	}

	public PerfMusicProvider getPerfMusicProvider() {
		return this.perfMusicProvider;
	}

	public void setPerfMusicProvider(PerfMusicProvider perfMusicProvider) {
		this.perfMusicProvider = perfMusicProvider;
	}

	public List<PerfRsUtilizationFile> getPerfRsUtilizationFiles() {
		return this.perfRsUtilizationFiles;
	}

	public void setPerfRsUtilizationFiles(List<PerfRsUtilizationFile> perfRsUtilizationFiles) {
		this.perfRsUtilizationFiles = perfRsUtilizationFiles;
	}

	public PerfRsUtilizationFile addPerfRsUtilizationFile(PerfRsUtilizationFile perfRsUtilizationFile) {
		getPerfRsUtilizationFiles().add(perfRsUtilizationFile);
		perfRsUtilizationFile.setPerfRsUtente(this);

		return perfRsUtilizationFile;
	}

	public PerfRsUtilizationFile removePerfRsUtilizationFile(PerfRsUtilizationFile perfRsUtilizationFile) {
		getPerfRsUtilizationFiles().remove(perfRsUtilizationFile);
		perfRsUtilizationFile.setPerfRsUtente(null);

		return perfRsUtilizationFile;
	}

}