package com.alkemytech.sophia.performing.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PERF_RS_ERR_SHOW_SCHEDULE database table.
 * 
 */
@Entity
@Table(name="PERF_RS_ERR_SHOW_SCHEDULE")
@NamedQuery(name="PerfRsErrShowSchedule.findAll", query="SELECT p FROM PerfRsErrShowSchedule p")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PerfRsErrShowSchedule implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_RS_ERR_SHOW_SCHEDULE", unique=true, nullable=false)
	private Long idRsErrShowSchedule;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="BEGIN_TIME")
	private Date beginTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATION_DATE", nullable=false)
	private Date creationDate;

	@Column(name="DAYS_FROM_UPLOAD")
	private Integer daysFromUpload;

	@Column(name="DURATION")
	private Integer duration;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="END_TIME")
	private Date endTime;

	@Column(name="ERROR")
	private String error;

	@Column(name="GLOBAL_ERROR", length=400)
	private String globalError;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="MODIFY_DATE")
	private Date modifyDate;

	@Column(name="REGIONAL_OFFICE", length=10)
	private String regionalOffice;

	@Column(name="REPORT_BEGIN_DATE", length=100)
	private String reportBeginDate;

	@Column(name="REPORT_BEGIN_TIME", length=100)
	private String reportBeginTime;

	@Column(name="REPORT_DURATION", length=100)
	private String reportDuration;

	@Column(name="REPORT_PALINSESTO", length=100)
	private String reportPalinsesto;

	@Column(name="SCHEDULE_MONTH")
	private Integer scheduleMonth;

	@Column(name="SCHEDULE_YEAR")
	private Integer scheduleYear;

	@Column(name="TITLE")
	private String title;

	//bi-directional many-to-one association to PerfRsErrShowMusic

	@OneToMany(mappedBy="perfRsErrShowSchedule")
	@XmlTransient
	private List<PerfRsErrShowMusic> perfRsErrShowMusics;

	//bi-directional many-to-one association to PerfRsNormalizedFile
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_NORMALIZED_FILE", nullable=false)

	private PerfRsNormalizedFile perfRsNormalizedFile;

	//bi-directional many-to-one association to PerfPalinsesto
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PALINSESTO")

	private PerfPalinsesto perfPalinsesto;

	public PerfRsErrShowSchedule() {
	}

	public Long getIdRsErrShowSchedule() {
		return this.idRsErrShowSchedule;
	}

	public void setIdRsErrShowSchedule(Long idRsErrShowSchedule) {
		this.idRsErrShowSchedule = idRsErrShowSchedule;
	}

	public Date getBeginTime() {
		return this.beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public int getDaysFromUpload() {
		return this.daysFromUpload;
	}

	public void setDaysFromUpload(int daysFromUpload) {
		this.daysFromUpload = daysFromUpload;
	}

	public int getDuration() {
		return this.duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public Date getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getGlobalError() {
		return this.globalError;
	}

	public void setGlobalError(String globalError) {
		this.globalError = globalError;
	}

	public Date getModifyDate() {
		return this.modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getRegionalOffice() {
		return this.regionalOffice;
	}

	public void setRegionalOffice(String regionalOffice) {
		this.regionalOffice = regionalOffice;
	}

	public String getReportBeginDate() {
		return this.reportBeginDate;
	}

	public void setReportBeginDate(String reportBeginDate) {
		this.reportBeginDate = reportBeginDate;
	}

	public String getReportBeginTime() {
		return this.reportBeginTime;
	}

	public void setReportBeginTime(String reportBeginTime) {
		this.reportBeginTime = reportBeginTime;
	}

	public String getReportDuration() {
		return this.reportDuration;
	}

	public void setReportDuration(String reportDuration) {
		this.reportDuration = reportDuration;
	}

	public String getReportPalinsesto() {
		return this.reportPalinsesto;
	}

	public void setReportPalinsesto(String reportPalinsesto) {
		this.reportPalinsesto = reportPalinsesto;
	}

	public int getScheduleMonth() {
		return this.scheduleMonth;
	}

	public void setScheduleMonth(int scheduleMonth) {
		this.scheduleMonth = scheduleMonth;
	}

	public int getScheduleYear() {
		return this.scheduleYear;
	}

	public void setScheduleYear(int scheduleYear) {
		this.scheduleYear = scheduleYear;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<PerfRsErrShowMusic> getPerfRsErrShowMusics() {
		return this.perfRsErrShowMusics;
	}

	public void setPerfRsErrShowMusics(List<PerfRsErrShowMusic> perfRsErrShowMusics) {
		this.perfRsErrShowMusics = perfRsErrShowMusics;
	}

	public PerfRsErrShowMusic addPerfRsErrShowMusic(PerfRsErrShowMusic perfRsErrShowMusic) {
		getPerfRsErrShowMusics().add(perfRsErrShowMusic);
		perfRsErrShowMusic.setPerfRsErrShowSchedule(this);

		return perfRsErrShowMusic;
	}

	public PerfRsErrShowMusic removePerfRsErrShowMusic(PerfRsErrShowMusic perfRsErrShowMusic) {
		getPerfRsErrShowMusics().remove(perfRsErrShowMusic);
		perfRsErrShowMusic.setPerfRsErrShowSchedule(null);

		return perfRsErrShowMusic;
	}

	public PerfRsNormalizedFile getPerfRsNormalizedFile() {
		return this.perfRsNormalizedFile;
	}

	public void setPerfRsNormalizedFile(PerfRsNormalizedFile perfRsNormalizedFile) {
		this.perfRsNormalizedFile = perfRsNormalizedFile;
	}

	public PerfPalinsesto getPerfPalinsesto() {
		return this.perfPalinsesto;
	}

	public void setPerfPalinsesto(PerfPalinsesto perfPalinsesto) {
		this.perfPalinsesto = perfPalinsesto;
	}

}