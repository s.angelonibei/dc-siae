package com.alkemytech.sophia.performing.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the PERF_RS_CONFIG database table.
 * 
 */
@Entity
@Table(name="PERF_RS_CONFIG")
@NamedQuery(name="PerfRsConfig.findAll", query="SELECT p FROM PerfRsConfig p")
public class PerfRsConfig implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_PERF_RS_CONFIG", unique=true, nullable=false)
	private Long idPerfRsConfig;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATION_DATE", columnDefinition = "datetime NOT NULL DEFAULT CURRENT_TIMESTAMP", insertable = false, updatable = false)
	private Date creationDate;

	@Column(name="CREATION_USER", length=50)
	private String creationUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="MODIFY_DATE")
	private Date modifyDate;

	@Column(name="MODIFY_USER", length=50)
	private String modifyUser;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="VALID_FROM", columnDefinition = "datetime NOT NULL DEFAULT CURRENT_TIMESTAMP", insertable = false, updatable = false)
	private Date validFrom;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="VALID_TO")
	private Date validTo;

	//bi-directional many-to-one association to PerfRsConfigTemplate

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CONFIGURATION")
	private PerfRsConfigTemplate perfRsConfigTemplate;

	//bi-directional many-to-one association to PerfMusicProvider

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MUSIC_PROVIDER", nullable=false)
	private PerfMusicProvider perfMusicProvider;

	//bi-directional many-to-one association to PerfPalinsesto

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_PALINSESTO")
	private PerfPalinsesto perfPalinsesto;

	public PerfRsConfig() {
	}

	public Long getIdPerfRsConfig() {
		return this.idPerfRsConfig;
	}

	public void setIdPerfRsConfig(Long idPerfRsConfig) {
		this.idPerfRsConfig = idPerfRsConfig;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getCreationUser() {
		return this.creationUser;
	}

	public void setCreationUser(String creationUser) {
		this.creationUser = creationUser;
	}

	public Date getModifyDate() {
		return this.modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getModifyUser() {
		return this.modifyUser;
	}

	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return this.validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public PerfRsConfigTemplate getPerfRsConfigTemplate() {
		return this.perfRsConfigTemplate;
	}

	public void setPerfRsConfigTemplate(PerfRsConfigTemplate perfRsConfigTemplate) {
		this.perfRsConfigTemplate = perfRsConfigTemplate;
	}

	public PerfMusicProvider getPerfMusicProvider() {
		return this.perfMusicProvider;
	}

	public void setPerfMusicProvider(PerfMusicProvider perfMusicProvider) {
		this.perfMusicProvider = perfMusicProvider;
	}

	public PerfPalinsesto getPerfPalinsesto() {
		return this.perfPalinsesto;
	}

	public void setPerfPalinsesto(PerfPalinsesto perfPalinsesto) {
		this.perfPalinsesto = perfPalinsesto;
	}

}