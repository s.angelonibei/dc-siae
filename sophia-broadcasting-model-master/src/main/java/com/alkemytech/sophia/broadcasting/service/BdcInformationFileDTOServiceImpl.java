package com.alkemytech.sophia.broadcasting.service;

import com.alkemytech.sophia.broadcasting.dto.BdcInformationFileDTO;
import com.alkemytech.sophia.broadcasting.dto.RequestMonitoraggioKPIDTO;
import com.alkemytech.sophia.broadcasting.enums.Stato;
import com.alkemytech.sophia.broadcasting.service.interfaces.BdcInformationFileDTOService;
import com.alkemytech.sophia.broadcasting.utils.QueryUtils;
import com.google.inject.Inject;
import com.google.inject.Provider;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class BdcInformationFileDTOServiceImpl implements BdcInformationFileDTOService {

    private final Provider<EntityManager> provider;

    @Inject
    public BdcInformationFileDTOServiceImpl(Provider<EntityManager> provider) {
        this.provider = provider;
    }

    @Override
    public List<BdcInformationFileDTO> fileDTOS(RequestMonitoraggioKPIDTO monitoraggioKPI) {

        EntityManager entityManager = provider.get();
        List<BdcInformationFileDTO> bdcInformationFileDTOS= new ArrayList<>();

        String w = "SELECT nome_file, emittente, bdc_broadcasters.nome AS nomeEmittente, canale, bdc_canali.nome AS nomeCanale, data_upload, tipo_upload, stato FROM bdc_utilization_file LEFT JOIN bdc_broadcasters ON bdc_utilization_file.emittente = bdc_broadcasters.id LEFT JOIN bdc_canali ON bdc_utilization_file.canale = bdc_canali.id WHERE emittente = ?1 AND (canale IN (?2) OR canale IS NULL) AND ((anno = ?3 AND mese IN (?4)) OR (anno IS NULL AND mese IS NULL)) AND stato = ?5";

        final Query q = entityManager.createNativeQuery(w);

        List<Object[]> result = q
                .setParameter(1, monitoraggioKPI.getRequestData().getEmittente().getId())
                .setParameter(2, QueryUtils.listToInClause(monitoraggioKPI.getRequestData().getCanali()))
                .setParameter(3, monitoraggioKPI.getRequestData().getAnno())
                .setParameter(4, QueryUtils.listToInClause(monitoraggioKPI.getRequestData().getMesi()))
                .setParameter(5, Stato.DA_ELABORARE.toString())
                .getResultList();

        for(Object[] object: result) {
            bdcInformationFileDTOS.add(new BdcInformationFileDTO(object));
        }

        return bdcInformationFileDTOS;
    }
}
