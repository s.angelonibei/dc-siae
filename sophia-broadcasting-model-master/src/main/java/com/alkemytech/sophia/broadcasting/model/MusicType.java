package com.alkemytech.sophia.broadcasting.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Alessandro Russo on 09/12/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "BDC_MUSIC_TYPE")
@NamedQuery(name="MusicType.findAll", query="SELECT m FROM MusicType m")
public class MusicType implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_MUSIC_TYPE", nullable = false)
    public Long id;



    @Column(name = "NAME")
    public String name;

    @Column(name = "CATEGORY")
    public String category;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MusicType)) return false;

        MusicType musicType = (MusicType) o;

        if (id != null ? !id.equals(musicType.id) : musicType.id != null) return false;
        return name != null ? name.equals(musicType.name) : musicType.name == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "MusicType{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", category='" + category + '\'' +
                '}';
    }
}
