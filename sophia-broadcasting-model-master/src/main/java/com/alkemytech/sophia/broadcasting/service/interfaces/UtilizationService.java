package com.alkemytech.sophia.broadcasting.service.interfaces;

import com.alkemytech.sophia.broadcasting.model.BdcNormalizedFile;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Alessandro Russo on 14/12/2017.
 */
public interface UtilizationService {

    public boolean evaluateKPI(Integer idCanale, Integer anno, Integer mese);

    public Map<Integer, Set<String>> saveTvEntityList(List<Object> rows, BdcNormalizedFile normalizedFile);

    public Map<Integer, Set<String>> saveRadioEntityList(List<Object> rows, BdcNormalizedFile normalizedFile,Integer overlapLimit);



}
