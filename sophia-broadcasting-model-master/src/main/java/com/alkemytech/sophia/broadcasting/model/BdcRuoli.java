package com.alkemytech.sophia.broadcasting.model;

import com.alkemytech.sophia.performing.model.PerfRsUtente;
import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.List;

@XmlRootElement
@Entity
@Table(name = "bdc_ruoli")
public class BdcRuoli {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id", nullable=false)
    private Integer id;

    @NotNull
    @Column(name="ruolo", nullable = false)
    private String ruolo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRuolo() {
        return ruolo;
    }

    public void setRuolo(String ruolo) {
        this.ruolo = ruolo;
    }

    public BdcRuoli() {
    }

    public BdcRuoli(String ruolo) {
        this.ruolo = ruolo;
    }
}
