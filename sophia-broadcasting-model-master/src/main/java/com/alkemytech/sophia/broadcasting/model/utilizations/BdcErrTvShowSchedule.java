package com.alkemytech.sophia.broadcasting.model.utilizations;

import com.alkemytech.sophia.broadcasting.model.BdcCanali;
import com.alkemytech.sophia.broadcasting.model.BdcNormalizedFile;
import com.alkemytech.sophia.broadcasting.model.ShowType;
import com.alkemytech.sophia.common.Constants;
import com.alkemytech.sophia.common.tools.StringTools;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang3.math.NumberUtils;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.alkemytech.sophia.broadcasting.utils.Utilities.getMapValue;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "BDC_ERR_TV_SHOW_SCHEDULE")
public class BdcErrTvShowSchedule implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_ERR_TV_SHOW_SCHEDULE", nullable = false)
    private Long id;

    @OneToMany
    @JoinColumn(name = "ID_ERR_TV_SHOW_SCHEDULE")
    List<BdcErrTvShowMusic> errTvShowMusicList;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_NORMALIZED_FILE", nullable = false)
    private BdcNormalizedFile normalizedFile;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_CHANNEL", nullable = false)
    private BdcCanali canale;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_SHOW_TYPE")
    private ShowType showType;

    @Column(name = "TITLE", nullable = false)
    private String title;

    @Column( name = "ORIGINAL_TITLE")
    private String originalTitle;

    @Column( name = "BEGIN_TIME")
    private Date beginTime;

    @Column( name = "END_TIME")
    private Date endTime;

    @Column( name = "DURATION")
    private Integer duration;

    @Column( name = "REPLICA")
    private String replica;

    @Column( name = "PROD_COUNTRY")
    private String prodCountry;

    @Column( name = "PROD_YEAR")
    private Integer prodYear;

    @Column( name = "PRODUCTOR")
    private String productor;

    @Column( name = "DIRECTOR")
    private String director;

    @Column( name = "EPISODE_TITLE")
    private String episodeTitle;

    @Column( name = "EPISODE_ORIGINAL_TITLE")
    private String episodeOriginalTitle;

    @Column( name = "EPISODE_NUMBER")
    private Integer episodeNumber;

    @Column( name = "REGIONAL_OFFICE")
    private String regionalOffice;

    @Column( name = "ERROR")
    private String error;

    @Column( name = "GLOBAL_ERROR")
    private String globalError;

    @Column( name = "REPORT_CHANNEL")
    private String reportChannel;

    @Column( name = "REPORT_SHOW_TYPE")
    private String reportShowType;

    @Column( name = "REPORT_BEGIN_DATE")
    private String reportBeginDate;

    @Column( name = "REPORT_BEGIN_TIME")
    private String reportBeginTime;

    @Column( name = "REPORT_END_TIME")
    private String reportEndTime;

    @Column( name = "REPORT_DURATION")
    private String reportDuration;

    @Column( name = "REPORT_PROD_YEAR")
    private String reportProdYear;

    @Column( name = "REPORT_EPISODE_NUMBER")
    private String reportEpisodeNumber;

    @Column( name = "SCHEDULE_YEAR")
    private Integer schedulYear;

    @Column( name = "SCHEDULE_MONTH")
    private Integer scheduleMonth;

    @Column( name = "DAYS_FROM_UPLOAD")
    private Integer daysFromUpload;

    @Column( name = "CREATION_DATE")
    private Date creationDate;

    @Column( name = "MODIFY_DATE")
    private Date modifyDate;

    public BdcErrTvShowSchedule(BdcErrTvShowMusic errTvShowMusic, BdcCanali canale, BdcNormalizedFile normalizedFile, ShowType showType, Date beginTime, Date endTime, Integer duration, Map<String, Object> record) {
        this.errTvShowMusicList = new ArrayList<>();
        this.errTvShowMusicList.add(errTvShowMusic);
        this.canale = canale;
        this.normalizedFile = normalizedFile;
        this.showType = showType;
        this.beginTime = beginTime;
        this.endTime = endTime;
        this.duration = duration;
        this.title = getMapValue(record, Constants.TITOLO_TX);
        this.regionalOffice = getMapValue(record, Constants.DIFFUSIONE_REGIONALE);
        this.error = getMapValue(record, Constants.ERRORS);
        this.reportChannel = getMapValue(record, Constants.CANALE);
        this.reportShowType = getMapValue(record, Constants.GENERE_TX);
        this.reportBeginDate = getMapValue(record, Constants.DATA_INIZIO_TX);
        this.reportBeginTime = getMapValue(record, Constants.ORARIO_INIZIO_TX);
        this.reportEndTime = getMapValue(record, Constants.ORARIO_FINE_TX);
        this.reportDuration = getMapValue(record, Constants.DURATA_TX);
        //this.reportProdYear = getMapValue(record, Constants.ANNO_PRODUZIONE_TX);
        //this.reportEpisodeNumber = getMapValue(record, Constants.NUM_PROGRESSIVO_EPISODIO_TX);
//        this.originalTitle = getMapValue(record, Constants.TITOLO_ORIGINALE_TX);
//        this.replica = getMapValue(record, Constants.REPLICA_TX);
//        this.prodYear = getMapValue(record, Constants.ANNO_PRODUZIONE_TX) != null && NumberUtils.isParsable(getMapValue(record, Constants.ANNO_PRODUZIONE_TX)) ? new Integer(getMapValue(record, Constants.ANNO_PRODUZIONE_TX)) : null;
//        this.prodCountry = getMapValue(record, Constants.PAESE_PRODUZIONE_TX);
//        this.productor = getMapValue(record, Constants.PRODUTTORE_TX);
//        this.director = getMapValue(record, Constants.REGISTA_TX);
//        this.episodeTitle = getMapValue(record, Constants.TITOLO_EPISODIO_TX);
//        this.episodeOriginalTitle = getMapValue(record, Constants.TITOLO_ORIGINALE_EPISODIO_TX);
//        this.episodeNumber = getMapValue(record, Constants.NUM_PROGRESSIVO_EPISODIO_TX) != null && NumberUtils.isParsable(getMapValue(record, Constants.NUM_PROGRESSIVO_EPISODIO_TX)) ? new Integer(getMapValue(record, Constants.NUM_PROGRESSIVO_EPISODIO_TX)) : null;
        this.creationDate = new Date();
    }

    public BdcErrTvShowSchedule() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<BdcErrTvShowMusic> getErrTvShowMusicList() {
        return errTvShowMusicList;
    }

    public void setErrTvShowMusicList(List<BdcErrTvShowMusic> errTvShowMusicList) {
        this.errTvShowMusicList = errTvShowMusicList;
    }

    public BdcNormalizedFile getNormalizedFile() {
        return normalizedFile;
    }

    public void setNormalizedFile(BdcNormalizedFile normalizedFile) {
        this.normalizedFile = normalizedFile;
    }

    public BdcCanali getCanale() {
        return canale;
    }

    public void setCanale(BdcCanali canale) {
        this.canale = canale;
    }

    public ShowType getShowType() {
        return showType;
    }

    public void setShowType(ShowType showType) {
        this.showType = showType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getReplica() {
        return replica;
    }

    public void setReplica(String replica) {
        this.replica = replica;
    }

    public String getProdCountry() {
        return prodCountry;
    }

    public void setProdCountry(String prodCountry) {
        this.prodCountry = prodCountry;
    }

    public Integer getProdYear() {
        return prodYear;
    }

    public void setProdYear(Integer prodYear) {
        this.prodYear = prodYear;
    }

    public String getProductor() {
        return productor;
    }

    public void setProductor(String productor) {
        this.productor = productor;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getEpisodeTitle() {
        return episodeTitle;
    }

    public void setEpisodeTitle(String episodeTitle) {
        this.episodeTitle = episodeTitle;
    }

    public String getEpisodeOriginalTitle() {
        return episodeOriginalTitle;
    }

    public void setEpisodeOriginalTitle(String episodeOriginalTitle) {
        this.episodeOriginalTitle = episodeOriginalTitle;
    }

    public Integer getEpisodeNumber() {
        return episodeNumber;
    }

    public void setEpisodeNumber(Integer episodeNumber) {
        this.episodeNumber = episodeNumber;
    }

    public String getRegionalOffice() {
        return regionalOffice;
    }

    public void setRegionalOffice(String regionalOffice) {
        this.regionalOffice = regionalOffice;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getGlobalError() {
        return globalError;
    }

    public void setGlobalError(String globalError) {
        this.globalError = globalError;
    }

    public String getReportChannel() {
        return reportChannel;
    }

    public void setReportChannel(String reportChannel) {
        this.reportChannel = reportChannel;
    }

    public String getReportShowType() {
        return reportShowType;
    }

    public void setReportShowType(String reportShowType) {
        this.reportShowType = reportShowType;
    }

    public String getReportBeginDate() {
        return reportBeginDate;
    }

    public void setReportBeginDate(String reportBeginDate) {
        this.reportBeginDate = reportBeginDate;
    }

    public String getReportBeginTime() {
        return reportBeginTime;
    }

    public void setReportBeginTime(String reportBeginTime) {
        this.reportBeginTime = reportBeginTime;
    }

    public String getReportEndTime() {
        return reportEndTime;
    }

    public void setReportEndTime(String reportEndTime) {
        this.reportEndTime = reportEndTime;
    }

    public String getReportDuration() {
        return reportDuration;
    }

    public void setReportDuration(String reportDuration) {
        this.reportDuration = reportDuration;
    }

    public String getReportProdYear() {
        return reportProdYear;
    }

    public void setReportProdYear(String reportProdYear) {
        this.reportProdYear = reportProdYear;
    }

    public String getReportEpisodeNumber() {
        return reportEpisodeNumber;
    }

    public void setReportEpisodeNumber(String reportEpisodeNumber) {
        this.reportEpisodeNumber = reportEpisodeNumber;
    }

    public Integer getSchedulYear() {
        return schedulYear;
    }

    public void setSchedulYear(Integer schedulYear) {
        this.schedulYear = schedulYear;
    }

    public Integer getScheduleMonth() {
        return scheduleMonth;
    }

    public void setScheduleMonth(Integer scheduleMonth) {
        this.scheduleMonth = scheduleMonth;
    }

    public Integer getDaysFromUpload() {
        return daysFromUpload;
    }

    public void setDaysFromUpload(Integer daysFromUpload) {
        this.daysFromUpload = daysFromUpload;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Override
    public String toString() {
        return "BdcErrTvShowSchedule{" +
                "id=" + id +
                ", errTvShowMusicList=" + errTvShowMusicList +
                ", normalizedFile=" + normalizedFile +
                ", canale=" + canale +
                ", showType=" + showType +
                ", title='" + title + '\'' +
                ", originalTitle='" + originalTitle + '\'' +
                ", beginTime=" + beginTime +
                ", endTime=" + endTime +
                ", duration=" + duration +
                ", replica='" + replica + '\'' +
                ", prodCountry='" + prodCountry + '\'' +
                ", prodYear=" + prodYear +
                ", productor='" + productor + '\'' +
                ", director='" + director + '\'' +
                ", episodeTitle='" + episodeTitle + '\'' +
                ", episodeOriginalTitle='" + episodeOriginalTitle + '\'' +
                ", episodeNumber=" + episodeNumber +
                ", regionalOffice='" + regionalOffice + '\'' +
                ", error='" + error + '\'' +
                ", globalError='" + globalError + '\'' +
                ", reportChannel='" + reportChannel + '\'' +
                ", reportShowType='" + reportShowType + '\'' +
                ", reportBeginDate='" + reportBeginDate + '\'' +
                ", reportBeginTime='" + reportBeginTime + '\'' +
                ", reportEndTime='" + reportEndTime + '\'' +
                ", reportDuration='" + reportDuration + '\'' +
                ", reportProdYear='" + reportProdYear + '\'' +
                ", reportEpisodeNumber='" + reportEpisodeNumber + '\'' +
                ", schedulYear=" + schedulYear +
                ", scheduleMonth=" + scheduleMonth +
                ", daysFromUpload=" + daysFromUpload +
                ", creationDate=" + creationDate +
                ", modifyDate=" + modifyDate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BdcErrTvShowSchedule)) return false;

        BdcErrTvShowSchedule that = (BdcErrTvShowSchedule) o;

        if (title != null ? !title.equals(that.title) : that.title != null) return false;
        if (originalTitle != null ? !originalTitle.equals(that.originalTitle) : that.originalTitle != null)
            return false;
        if (beginTime != null ? !beginTime.equals(that.beginTime) : that.beginTime != null) return false;
        if (endTime != null ? !endTime.equals(that.endTime) : that.endTime != null) return false;
        if (reportBeginDate != null ? !reportBeginDate.equals(that.reportBeginDate) : that.reportBeginDate != null)
            return false;
        if (reportBeginTime != null ? !reportBeginTime.equals(that.reportBeginTime) : that.reportBeginTime != null)
            return false;
        return reportEndTime != null ? reportEndTime.equals(that.reportEndTime) : that.reportEndTime == null;

    }

    @Override
    public int hashCode() {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + (originalTitle != null ? originalTitle.hashCode() : 0);
        result = 31 * result + (beginTime != null ? beginTime.hashCode() : 0);
        result = 31 * result + (endTime != null ? endTime.hashCode() : 0);
        result = 31 * result + (reportBeginDate != null ? reportBeginDate.hashCode() : 0);
        result = 31 * result + (reportBeginTime != null ? reportBeginTime.hashCode() : 0);
        result = 31 * result + (reportEndTime != null ? reportEndTime.hashCode() : 0);
        return result;
    }
}