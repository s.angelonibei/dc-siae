package com.alkemytech.sophia.broadcasting.dto.harmonization;

/**
 * Created by Alessandro Russo on 08/12/2017.
 */
public class RadioHarmonizedDTO extends BaseRulesDTO{

    private String nomeEmittente;
    private String canale;
    private String titoloTx;
    private String dataDiffusioneTx;
    private String orarioInizioTx;
    private String durataTx;
    private String titoloOp;
    private String compositoreOp;
    private String esecutoreOp;
    private String albumOp;
    private String durataOp;
    private String codiceIswcOp;
    private String codiceIsrcOp;
    private String orarioInizioOp;
    private String categoriaUsoOp;
    private String diffusioneRegionale;
    private String identificativoRai;

    public String getNomeEmittente() {
        return nomeEmittente;
    }

    public void setNomeEmittente(String nomeEmittente) {
        this.nomeEmittente = nomeEmittente;
    }

    public String getCanale() {
        return canale;
    }

    public void setCanale(String canale) {
        this.canale = canale;
    }

    public String getTitoloTx() {
        return titoloTx;
    }

    public void setTitoloTx(String titoloTx) {
        this.titoloTx = titoloTx;
    }

    public String getDataDiffusioneTx() {
        return dataDiffusioneTx;
    }

    public void setDataDiffusioneTx(String dataDiffusioneTx) {
        this.dataDiffusioneTx = dataDiffusioneTx;
    }

    public String getOrarioInizioTx() {
        return orarioInizioTx;
    }

    public void setOrarioInizioTx(String orarioInizioTx) {
        this.orarioInizioTx = orarioInizioTx;
    }

    public String getDurataTx() {
        return durataTx;
    }

    public void setDurataTx(String durataTx) {
        this.durataTx = durataTx;
    }

    public String getTitoloOp() {
        return titoloOp;
    }

    public void setTitoloOp(String titoloOp) {
        this.titoloOp = titoloOp;
    }

    public String getCompositoreOp() {
        return compositoreOp;
    }

    public void setCompositoreOp(String compositoreOp) {
        this.compositoreOp = compositoreOp;
    }

    public String getEsecutoreOp() {
        return esecutoreOp;
    }

    public void setEsecutoreOp(String esecutoreOp) {
        this.esecutoreOp = esecutoreOp;
    }

    public String getAlbumOp() {
        return albumOp;
    }

    public void setAlbumOp(String albumOp) {
        this.albumOp = albumOp;
    }

    public String getDurataOp() {
        return durataOp;
    }

    public void setDurataOp(String durataOp) {
        this.durataOp = durataOp;
    }

    public String getCodiceIswcOp() {
        return codiceIswcOp;
    }

    public void setCodiceIswcOp(String codiceIswcOp) {
        this.codiceIswcOp = codiceIswcOp;
    }

    public String getCodiceIsrcOp() {
        return codiceIsrcOp;
    }

    public void setCodiceIsrcOp(String codiceIsrcOp) {
        this.codiceIsrcOp = codiceIsrcOp;
    }

    public String getOrarioInizioOp() {
        return orarioInizioOp;
    }

    public void setOrarioInizioOp(String orarioInizioOp) {
        this.orarioInizioOp = orarioInizioOp;
    }

    public String getCategoriaUsoOp() {
        return categoriaUsoOp;
    }

    public void setCategoriaUsoOp(String categoriaUsoOp) {
        this.categoriaUsoOp = categoriaUsoOp;
    }

    public String getDiffusioneRegionale() {
        return diffusioneRegionale;
    }

    public void setDiffusioneRegionale(String diffusioneRegionale) {
        this.diffusioneRegionale = diffusioneRegionale;
    }

    public String getIdentificativoRai() {
        return identificativoRai;
    }

    public void setIdentificativoRai(String identificativoRai) {
        this.identificativoRai = identificativoRai;
    }
}
