package com.alkemytech.sophia.broadcasting.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlRootElement
@Entity
@Table(name = "BDC_USER_TOKEN")
public class BdcUserToken {

	@Id
	@Column(name = "BKO_USER", nullable = false)
	private String user;
	@Column(name = "BDC_USER", nullable = false)
	private Integer bdcUser;
	@Column(name = "TOKEN", nullable = false)
	private String token;
	@Column(name = "DATA_CREAZIONE", nullable = false)
	private Date dataCreazione;
	@Column(name = "DATA_FINE_VALIDITA", nullable = false)
	private Date dataFineValidita;
	@Column(name = "REPERTORI", nullable = false)
	private String repertori;
	@Column(name = "REPERTORIO_DEFAULT", nullable = false)
	private String repertorioDefault;
	@Column(name = "DATA_LOGIN", nullable = false)
	private Date dataLogin;
	@Column(name = "TOKEN_NAVIGAZIONE", nullable = false)
	private String tokenNavigazione;

	public BdcUserToken() {
		super();
	}

	public BdcUserToken(String user, Integer bdcUser, String token, Date dataCreazione, Date dataFineValidita,
			String repertori, String repertorioDefault, Date dataLogin, String tokenNavigazione) {
		super();
		this.user = user;
		this.bdcUser = bdcUser;
		this.token = token;
		this.dataCreazione = dataCreazione;
		this.dataFineValidita = dataFineValidita;
		this.repertori = repertori;
		this.repertorioDefault = repertorioDefault;
		this.dataLogin = dataLogin;
		this.tokenNavigazione = tokenNavigazione;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public Integer getBdcUser() {
		return bdcUser;
	}

	public void setBdcUser(Integer bdcUser) {
		this.bdcUser = bdcUser;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getDataCreazione() {
		return dataCreazione;
	}

	public void setDataCreazione(Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}

	public Date getDataFineValidita() {
		return dataFineValidita;
	}

	public void setDataFineValidita(Date dataFineValidita) {
		this.dataFineValidita = dataFineValidita;
	}

	public String getRepertori() {
		return repertori;
	}

	public void setRepertori(String repertori) {
		this.repertori = repertori;
	}

	public String getRepertorioDefault() {
		return repertorioDefault;
	}

	public void setRepertorioDefault(String repertorioDefault) {
		this.repertorioDefault = repertorioDefault;
	}

	public Date getDataLogin() {
		return dataLogin;
	}

	public void setDataLogin(Date dataLogin) {
		this.dataLogin = dataLogin;
	}

	public String getTokenNavigazione() {
		return tokenNavigazione;
	}

	public void setTokenNavigazione(String tokenNavigazione) {
		this.tokenNavigazione = tokenNavigazione;
	}

}
