package com.alkemytech.sophia.broadcasting.model;

public enum TipoBroadcaster {
    TELEVISIONE,
    RADIO
}