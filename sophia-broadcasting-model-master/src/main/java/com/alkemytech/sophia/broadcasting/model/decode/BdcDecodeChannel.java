package com.alkemytech.sophia.broadcasting.model.decode;

import com.alkemytech.sophia.broadcasting.model.BdcCanali;
import com.alkemytech.sophia.broadcasting.model.ShowType;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.core.util.Constants;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Alessandro Russo on 13/12/2017.
 */
@Entity
@Table( name = "BDC_DECODE_CHANNEL" )
@NamedQuery(name="BdcDecodeChannel.findAll", query="SELECT d FROM BdcDecodeChannel d")
public class BdcDecodeChannel implements Serializable {

    private final static String FLAG_NO 		= "0";
    private final static String FLAG_YES 	    = "1";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_DECODE_CHANNEL")
    private Long id;

    @ManyToOne
    @JoinColumn(name="ID_CHANNEL")
    private BdcCanali canale;

    @Column(name = "VALUE")
    private String value;

    @Column(name = "REGIONAL")
    private boolean regional;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isRegional() {
        return regional;
    }

    public void setRegional(boolean regional) {
        this.regional = regional;
    }

    public BdcCanali getCanale() {
        return canale;
    }

    public void setCanale(BdcCanali canale) {
        this.canale = canale;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BdcDecodeChannel{" +
                "id=" + id +
                ", canale=" + canale +
                ", value='" + value + '\'' +
                ", regional='" + regional + '\'' +
                '}';
    }
}
