package com.alkemytech.sophia.broadcasting.model.harmonize;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the BDC_ERR_RD_SHOW_MUSIC database table.
 * 
 */
@Entity(name="ErrRdShowMusic")
@Table(name="BDC_ERR_RD_SHOW_MUSIC")
@NamedQuery(name="ErrRdShowMusic.findAll", query="SELECT b FROM ErrRdShowMusic b")
public class BdcErrRdShowMusic implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_ERR_RD_SHOW_MUSIC", unique=true, nullable=false)
	private Integer idErrRdShowMusic;

	@Lob
	@Column(name="ALBUM")
	private String album;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="BEGIN_TIME")
	private Date beginTime;

	@Lob
	@Column(name="COMPOSER")
	private String composer;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATION_DATE", nullable=false)
	private Date creationDate;

	@Column(name="DAYS_FROM_UPLOAD")
	private Integer daysFromUpload;

	@Column(name="DURATION")
	private Integer duration;

	@Column(name="ID_MUSIC_TYPE")
	private java.math.BigInteger idMusicType;

	@Column(name="ID_NORMALIZED_FILE", nullable=false)
	private java.math.BigInteger idNormalizedFile;

	@Column(name="ISRC_CODE", length=50)
	private String isrcCode;

	@Column(name="ISWC_CODE", length=50)
	private String iswcCode;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="MODIFY_DATE")
	private Date modifyDate;

	@Lob
	@Column(name="PERFORMER")
	private String performer;

	@Column(name="REAL_DURATION")
	private Integer realDuration;

	@Column(name="REPORT_BEGIN_TIME", length=100)
	private String reportBeginTime;

	@Column(name="REPORT_DURATION", length=100)
	private String reportDuration;

	@Column(name="REPORT_MUSIC_TYPE", length=100)
	private String reportMusicType;

	@Lob
	@Column(name="TITLE")
	private String title;

	//bi-directional many-to-one association to BdcErrRdShowSchedule
	@JsonBackReference
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_ERR_RD_SHOW_SCHEDULE")
	private BdcErrRdShowSchedule bdcErrRdShowSchedule;

	//R9-22
	@Column(name = "IDENTIFICATIVO_RAI")
	private String identificativoRai;

	public BdcErrRdShowMusic() {
	}

	public Integer getIdErrRdShowMusic() {
		return this.idErrRdShowMusic;
	}

	public void setIdErrRdShowMusic(Integer idErrRdShowMusic) {
		this.idErrRdShowMusic = idErrRdShowMusic;
	}

	public String getAlbum() {
		return this.album;
	}

	public void setAlbum(String album) {
		this.album = album;
	}

	public Date getBeginTime() {
		return this.beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public String getComposer() {
		return this.composer;
	}

	public void setComposer(String composer) {
		this.composer = composer;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Integer getDaysFromUpload() {
		return this.daysFromUpload;
	}

	public void setDaysFromUpload(Integer daysFromUpload) {
		this.daysFromUpload = daysFromUpload;
	}

	public Integer getDuration() {
		return this.duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public java.math.BigInteger getIdMusicType() {
		return this.idMusicType;
	}

	public void setIdMusicType(java.math.BigInteger idMusicType) {
		this.idMusicType = idMusicType;
	}

	public java.math.BigInteger getIdNormalizedFile() {
		return this.idNormalizedFile;
	}

	public void setIdNormalizedFile(java.math.BigInteger idNormalizedFile) {
		this.idNormalizedFile = idNormalizedFile;
	}

	public String getIsrcCode() {
		return this.isrcCode;
	}

	public void setIsrcCode(String isrcCode) {
		this.isrcCode = isrcCode;
	}

	public String getIswcCode() {
		return this.iswcCode;
	}

	public void setIswcCode(String iswcCode) {
		this.iswcCode = iswcCode;
	}

	public Date getModifyDate() {
		return this.modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getPerformer() {
		return this.performer;
	}

	public void setPerformer(String performer) {
		this.performer = performer;
	}

	public Integer getRealDuration() {
		return this.realDuration;
	}

	public void setRealDuration(Integer realDuration) {
		this.realDuration = realDuration;
	}

	public String getReportBeginTime() {
		return this.reportBeginTime;
	}

	public void setReportBeginTime(String reportBeginTime) {
		this.reportBeginTime = reportBeginTime;
	}

	public String getReportDuration() {
		return this.reportDuration;
	}

	public void setReportDuration(String reportDuration) {
		this.reportDuration = reportDuration;
	}

	public String getReportMusicType() {
		return this.reportMusicType;
	}

	public void setReportMusicType(String reportMusicType) {
		this.reportMusicType = reportMusicType;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public BdcErrRdShowSchedule getBdcErrRdShowSchedule() {
		return this.bdcErrRdShowSchedule;
	}

	public void setBdcErrRdShowSchedule(BdcErrRdShowSchedule bdcErrRdShowSchedule) {
		this.bdcErrRdShowSchedule = bdcErrRdShowSchedule;
	}

	public String getIdentificativoRai() {
		return identificativoRai;
	}

	public void setIdentificativoRai(String identificativoRai) {
		this.identificativoRai = identificativoRai;
	}
}