package com.alkemytech.sophia.broadcasting.model;

import com.google.gson.GsonBuilder;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@XmlRootElement
@Entity
@Table(name = "BDC_BROADCASTER_CONFIG_TEMPLATE")
@XmlAccessorType(XmlAccessType.FIELD)
public class BdcBroadcasterConfigTemplate implements Serializable {

    public BdcBroadcasterConfigTemplate() {
		super();
	}

	public BdcBroadcasterConfigTemplate(Integer id, String nomeTemplate, String configurazione, String tipoEmittente,
			Date dataCreazione, Boolean flagAttivo, Boolean flagDefault) {
		super();
		this.id = id;
		this.nomeTemplate = nomeTemplate;
		this.configurazione = configurazione;
		this.tipoEmittente = tipoEmittente;
		this.dataCreazione = dataCreazione;
		this.flagAttivo = flagAttivo;
		this.flagDefault = flagDefault;
	}

	@Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="ID_BROADCASTER_CONFIG_TEMPLATE")
    private Integer id;

    @Column(name="NOME_TEMPLATE")
    private String nomeTemplate ;

    @Column(name = "CONFIGURAZIONE", columnDefinition = "json")
    private String configurazione;

    @Column(name="TIPO_EMITTENTE")
    private String tipoEmittente;

    @Column(name="DATA_CREAZIONE")
    private Date dataCreazione;

    @Column(name="FLAG_ATTIVO")
    private Boolean flagAttivo;

    @Column(name="FLAG_DEFAULT")
    private Boolean flagDefault;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNomeTemplate() {
        return nomeTemplate;
    }

    public void setNomeTemplate(String nomeTemplate) {
        this.nomeTemplate = nomeTemplate;
    }

    public String getConfigurazione() {
        return configurazione;
    }

    public void setConfigurazione(String configurazione) {
        this.configurazione = configurazione;
    }

    public String getTipoEmittente() {
        return tipoEmittente;
    }

    public void setTipoEmittente(String tipoEmittente) {
        this.tipoEmittente = tipoEmittente;
    }

    public Date getDataCreazione() {
        return dataCreazione;
    }

    public void setDataCreazione(Date dataCreazione) {
        this.dataCreazione = dataCreazione;
    }

    public Boolean getFlagAttivo() {
        return flagAttivo;
    }

    public void setFlagAttivo(Boolean flagAttivo) {
        this.flagAttivo = flagAttivo;
    }

    public Boolean getFlagDefault() {
        return flagDefault;
    }

    public void setFlagDefault(Boolean flagDefault) {
        this.flagDefault = flagDefault;
    }

    @Override
    public String toString() {
        return new GsonBuilder()
                .setPrettyPrinting()
                .create()
                .toJson(this);
    }
}
