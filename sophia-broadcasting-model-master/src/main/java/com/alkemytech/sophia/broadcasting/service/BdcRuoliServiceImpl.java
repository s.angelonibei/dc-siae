package com.alkemytech.sophia.broadcasting.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.alkemytech.sophia.broadcasting.model.BdcRuoli;
import com.alkemytech.sophia.broadcasting.service.interfaces.BdcRuoliService;
import com.google.inject.Inject;
import com.google.inject.Provider;

public class BdcRuoliServiceImpl implements BdcRuoliService{


	private final Provider<EntityManager> provider;

	@Inject
	public BdcRuoliServiceImpl(Provider<EntityManager> provider) {
		this.provider = provider;
	}
	
	@Override
	public List<BdcRuoli> findAll() {

		EntityManager em = null;
		em = provider.get();

		final Query q = em.createQuery("SELECT a FROM BdcRuoli a ");

		return (List<BdcRuoli>) q.getResultList();
	}

}
