package com.alkemytech.sophia.broadcasting.service.performing;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.alkemytech.sophia.broadcasting.dto.performing.RSInformationFileDTO;
import com.alkemytech.sophia.broadcasting.dto.performing.RSRequestMonitoraggioKPIDTO;
import com.alkemytech.sophia.broadcasting.enums.Stato;
import com.alkemytech.sophia.broadcasting.utils.QueryUtils;
import com.alkemytech.sophia.performing.model.PerfRsUtilizationFile;
import com.google.inject.Inject;
import com.google.inject.Provider;

public class RsInformationFileServiceImpl implements RsInformationFileService {

    private final Provider<EntityManager> provider;

    @Inject
    public RsInformationFileServiceImpl(Provider<EntityManager> provider) {
        this.provider = provider;
    }

    @Override
    public List<RSInformationFileDTO> fileDTOS(RSRequestMonitoraggioKPIDTO monitoraggioKPI) {

        EntityManager entityManager = provider.get();
        List<RSInformationFileDTO> bdcInformationFileDTOS= new ArrayList<>();

        String w = 
        		" SELECT \n" + 
        		"    PERF_RS_UTILIZATION_FILE.NOME_FILE,\n" + 
        		"    PERF_RS_UTILIZATION_FILE.ID_MUSIC_PROVIDER,\n" + 
        		"    PERF_MUSIC_PROVIDER.NOME AS NOME_MUSIC_PROVIDER,\n" + 
        		"    PERF_RS_UTILIZATION_FILE.ID_PALINSESTO,\n" + 
        		"    PERF_PALINSESTO.NOME AS NOME_PALINSESTI,\n" + 
        		"    DATA_UPLOAD,\n" + 
        		"    TIPO_UPLOAD,\n" + 
        		"    STATO\n" + 
        		" FROM\n" + 
        		"    PERF_RS_UTILIZATION_FILE\n" + 
        		"        LEFT JOIN\n" + 
        		"    PERF_MUSIC_PROVIDER ON PERF_MUSIC_PROVIDER.ID_MUSIC_PROVIDER = PERF_RS_UTILIZATION_FILE.ID_MUSIC_PROVIDER\n" + 
        		"        LEFT JOIN\n" + 
        		"    PERF_PALINSESTO ON PERF_RS_UTILIZATION_FILE.ID_PALINSESTO = PERF_PALINSESTO.ID_PALINSESTO\n" + 
        		" WHERE PERF_MUSIC_PROVIDER.ID_MUSIC_PROVIDER = ?1 AND (PERF_PALINSESTO.ID_PALINSESTO IN (?2) OR PERF_PALINSESTO.ID_PALINSESTO IS NULL) AND ((PERF_RS_UTILIZATION_FILE.ANNO = ?3 AND PERF_RS_UTILIZATION_FILE.MESE IN (?4)) OR (PERF_RS_UTILIZATION_FILE.ANNO IS NULL AND PERF_RS_UTILIZATION_FILE.MESE IS NULL)) AND PERF_RS_UTILIZATION_FILE.STATO = ?5";

        final Query q = entityManager.createNativeQuery(w);

        List<Object[]> result = q
                .setParameter(1, monitoraggioKPI.getMusicProvider().getIdMusicProvider())
                .setParameter(2, QueryUtils.listToInClause(monitoraggioKPI.getPalinsesti()))
                .setParameter(3, monitoraggioKPI.getAnno())
                .setParameter(4, QueryUtils.listToInClause(monitoraggioKPI.getMesi()))
                .setParameter(5, Stato.DA_ELABORARE.toString())
                .getResultList();

        for(Object[] object: result) {
            bdcInformationFileDTOS.add(new RSInformationFileDTO(object));
        }

        return bdcInformationFileDTOS;
    }


	@Override
	public PerfRsUtilizationFile findRsUtilizationById(Integer id) {
        EntityManager entityManager = provider.get(); 
		return (PerfRsUtilizationFile) entityManager.createQuery("Select x from PerfRsUtilizationFile x Where x.id=:id").setParameter("id", id).getSingleResult();
	}}
