package com.alkemytech.sophia.broadcasting.service.performing;

import com.alkemytech.sophia.broadcasting.data.IndicatoriPerCanale;
import com.alkemytech.sophia.broadcasting.dto.IndicatoreDTO;
import com.alkemytech.sophia.broadcasting.dto.performing.RSKPI;
import com.alkemytech.sophia.broadcasting.model.TipoBroadcaster;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

public interface RsKpiService {
    RSKPI calcolaKpiCopertura(long durataFilm, long durataGeneriVuoti, long durataPubblicita, long durataTrasmissioni, Integer anno, List<Short> mesi);
    RSKPI calcolaKpiCopertura(long durataFilm, long durataGeneriVuoti, long durataPubblicita,long durataTrasmissioni, Date dataDa, Date dataA);
    RSKPI calcolaKpiDurataMusicaDichiarata(HashMap<String, IndicatoreDTO> mapIndicatori);
    RSKPI calcolaKpiRecordScartati(HashMap<String, IndicatoreDTO> mapIndicatori);
    RSKPI calcolaKpiBraniTitoliErronei(HashMap<String, IndicatoreDTO> mapIndicatori);
    RSKPI calcolaKpiAutoriTitoliErronei(HashMap<String, IndicatoreDTO> mapIndicatori);
    RSKPI calcolaKpiFilmTitoliErronei(HashMap<String, IndicatoreDTO> mapIndicatori);
    RSKPI calcolaKpiProgrammiTitoliErronei(HashMap<String, IndicatoreDTO> mapIndicatori);
    RSKPI calcolaKpiMusicaEccedenteTrasmissione(HashMap<String, IndicatoreDTO> mapIndicatori);
    RSKPI calcolaKpiRecordInRitardo(HashMap<String, IndicatoreDTO> mapIndicatori);
    RSKPI calcolaKpiDurataBraniDichiaratiRD(long durataMusiche,Integer anno, List<Short> mesi);
    RSKPI calcolaKpiDurataBraniDichiaratiRD(long durataMusiche, Date dataDa, Date dataA);
    RSKPI calcolaKpiBraniTitoliErroneiRD(HashMap<String, IndicatoreDTO> mapIndicatori);
    RSKPI calcolaKpiAutoriTitoliErroneiRD(HashMap<String, IndicatoreDTO> mapIndicatori);
    HashMap<Integer, List<RSKPI>> findKpiRelativi(Date dateFrom, Date dateTo, TipoBroadcaster tipoBroadcaster);
    List<RSKPI> listKpi(Date dateFrom, Date dateTo, Integer idCanale, TipoBroadcaster tipoBroadcaster, IndicatoriPerCanale indicatoriPerCanale);
	IndicatoriPerCanale indicatoriPerCanale(Date dataDa, Date dataA);
}