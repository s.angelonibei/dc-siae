package com.alkemytech.sophia.broadcasting.dto;

import com.alkemytech.sophia.broadcasting.model.BdcInformationFileEntity;

import java.util.Map;
import java.util.Set;

public class FileKpiDTO
{
    private BdcInformationFileEntity bdcUtilizationFile;
    private Map<Integer,Set<String>> ambitoKpi;


    public Map<Integer, Set<String>> getAmbitoKpi() {
        return ambitoKpi;
    }

    public void setAmbitoKpi(Map<Integer, Set<String>> ambitoKpi) {
        this.ambitoKpi = ambitoKpi;
    }

    public BdcInformationFileEntity getBdcUtilizationFile() {
        return bdcUtilizationFile;
    }

    public void setBdcUtilizationFile(BdcInformationFileEntity bdcUtilizationFile) {
        this.bdcUtilizationFile = bdcUtilizationFile;
    }
}
