package com.alkemytech.sophia.broadcasting.service;

import com.alkemytech.sophia.broadcasting.model.BdcCanaleStorico;
import com.alkemytech.sophia.broadcasting.model.BdcCanali;
import com.alkemytech.sophia.broadcasting.service.interfaces.BdcCanaliService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Singleton
public class BdcCanaliServiceImpl implements BdcCanaliService {

	private final Provider<EntityManager> provider;

	@Inject
	public BdcCanaliServiceImpl(Provider<EntityManager> provider) {
		this.provider = provider;
	}

	@Override
	public List<BdcCanali> findAll() {
		EntityManager em = null;
		final Query q = em.createQuery("SELECT a FROM BdcCanali a ORDER BY a.id");
		return (List<BdcCanali>) q.getResultList();
	}

	@Override
	public List<BdcCanali> findAllActive() {
		EntityManager em = null;
		final Query q = em.createQuery(
				"SELECT a FROM BdcCanali a WHERE  (a.dataInizioValid < CURRENT_TIMESTAMP AND (a.dataFineValid IS NULL OR a.dataFineValid> CURRENT_TIMESTAMP)) ORDER BY a.id");
		return (List<BdcCanali>) q.getResultList();
	}

	@Override
	public List<BdcCanali> findByBroadcasterId(Integer idBroadcaster) {
		EntityManager em = null;
		em = provider.get();
		final Query q = em.createQuery("SELECT a FROM BdcCanali a  WHERE a.bdcBroadcasters.id=:id");
		return (List<BdcCanali>) q.setParameter("id", idBroadcaster).getResultList();
	}

	@Override
	public List<BdcCanali> findActiveByBroadcasterId(Integer idBroadcaster) {
		EntityManager em = null;
		em = provider.get();
		final Query q = em.createQuery(
				"SELECT a FROM BdcCanali a  WHERE a.bdcBroadcasters.id=:id AND (a.dataInizioValid < CURRENT_TIMESTAMP AND (a.dataFineValid IS NULL OR a.dataFineValid> CURRENT_TIMESTAMP)) ORDER BY a.id");

		return (List<BdcCanali>) q.setParameter("id", idBroadcaster).getResultList();
	}

	@Override
	public BdcCanali findById(Integer id) {
		return provider.get().find(BdcCanali.class, id);
	}

	@Override
	public boolean addCannel(BdcCanali bdcCanali) {
		EntityManager entityManager = provider.get();
		entityManager.getTransaction().begin();
		try {
			entityManager.persist(bdcCanali);
			entityManager.flush();
			BdcCanaleStorico bdcCanaleStorico = new BdcCanaleStorico(bdcCanali);
			entityManager.persist(bdcCanaleStorico);
			entityManager.getTransaction().commit();
			return true;
		} catch (Exception e) {

			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
			return false;
		}
	}

	@Override
	public Integer editCannel(BdcCanali bdcCanali) {
		EntityManager entityManager = provider.get();
		try {
			entityManager.getTransaction().begin();
			BdcCanali modifyCanali = findById(bdcCanali.getId());
			modifyCanali.setDataFineValid(bdcCanali.getDataFineValid());
			modifyCanali.setDataInizioValid(bdcCanali.getDataInizioValid());
			modifyCanali.setNome(bdcCanali.getNome());
			modifyCanali.setTipoCanale(bdcCanali.getTipoCanale());
			modifyCanali.setGeneralista(bdcCanali.getGeneralista());
			entityManager.merge(modifyCanali);
			BdcCanaleStorico bdcCanaleStorico = new BdcCanaleStorico(bdcCanali);
			entityManager.persist(bdcCanaleStorico);
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
			return 406;
		}

		return 200;
	}

	@Override
	public List<BdcCanaleStorico> findAllRecent(Integer idCanale) {
		EntityManager em = null;
		em = provider.get();
		final Query q = em
				.createQuery("SELECT a FROM BdcCanaleStorico a  WHERE a.bdcCanali=:id ORDER BY a.dataModifica DESC");
		return (List<BdcCanaleStorico>) q.setParameter("id", idCanale).getResultList();
	}

	@Override
	public boolean findCanaleByName(Integer bdcBroadcastersId, String nome) {
		EntityManager em = null;
		em = provider.get();
		final Query q = em.createQuery("SELECT a FROM BdcCanali a  WHERE a.bdcBroadcasters.id=:id AND a.nome=:nome");

		List<BdcCanali> selectedName = (List<BdcCanali>) q.setParameter("id", bdcBroadcastersId)
				.setParameter("nome", nome).getResultList();

		if (selectedName != null && selectedName.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

}
