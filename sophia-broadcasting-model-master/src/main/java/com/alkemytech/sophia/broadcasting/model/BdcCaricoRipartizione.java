package com.alkemytech.sophia.broadcasting.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.broadcasting.dto.KPI;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;


@Entity
@XmlRootElement
@Table(name="BDC_CARICO_RIPARTIZIONE")
@XmlAccessorType(XmlAccessType.FIELD)
public class BdcCaricoRipartizione {
	
	@Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name="ID_CARICO_RIPARTIZIONE", nullable=false)
	private Integer id;

    @OneToOne
	@JoinColumn(name = "ID_CANALE")
	private BdcCanali canale;
    
    @Column(name="INCASSO_NETTO", nullable=false)
    private BigDecimal incassoNetto;

    @Enumerated(EnumType.STRING)
    @Column(name="INCASSO", nullable=false)
    private TIPO_INCASSO incasso;
    
    @Enumerated(EnumType.STRING)
    @Column(name="TIPO_DIRITTO", nullable=false)
    private TIPO_DIRITTO tipoDiritto;
    
    @Column(name="SOSPESO_PER_CONTENZIOSO", nullable=false)
    private Boolean sospesoPerContenzioso;

    @Column(name="REPORT_DISPONIBILE", nullable=false)
    private Boolean repoDisponibile;
    
    @Column(name="IMPORTO_RIPARTIBILE", nullable=false)
    private Boolean importoRipartibile;
    
    @Column(name="INIZIO_PERIODO_COMPETENZA", nullable=false)
    private Date inizioPeriodoCompetenza;   

    @Column(name="FINE_PERIODO_COMPETENZA", nullable=false)
    private Date finePeriodoCompetenza;
    
    @Column(name="NOTA_COMMENTO_DIREZIONE_RIPARTIZIONE", nullable=false)
    private String notaCommentoUffEmittenti;

    @Column(name="NOTA_COMMENTO_UFFICIO_EMITTENTI", nullable=false)
    private String notaCommentoDirRipartizione;
    
    @Transient
    private Boolean kpiValido;
    
    @Transient
    private List<KPI> kpi; 
    public enum TIPO_INCASSO {
        CONSOLIDATO ("CONSOLIDATO"),
        PARZIALE ("PARZIALE");
    		
    		private final String name;       

    	    private TIPO_INCASSO(String s) {
    	        name = s;
    	    }

    	    public boolean equalsName(String otherName) {
    	        // (otherName == null) check is not needed because name.equals(null) returns false 
    	        return name.equals(otherName);
    	    }

    	    public String toString() {
    	       return this.name;
    	    }
    	    
    }
    public enum TIPO_DIRITTO {
    		DEM ("DEM"),
    		DRM ("DRM");
    		
    		private final String name;       

    	    private TIPO_DIRITTO(String s) {
    	        name = s;
    	    }

    	    public boolean equalsName(String otherName) {
    	        // (otherName == null) check is not needed because name.equals(null) returns false 
    	        return name.equals(otherName);
    	    }

    	    public String toString() {
    	       return this.name;
    	    }
    	    
    }

    public BdcCaricoRipartizione() {
		super();
	}
    
	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}


	public BdcCanali getCanale() {
		return canale;
	}

	public void setCanale(BdcCanali canale) {
		this.canale = canale;
	}

	public BigDecimal getIncassoNetto() {
		return incassoNetto;
	}



	public void setIncassoNetto(BigDecimal incassoNetto) {
		this.incassoNetto = incassoNetto;
	}



	public TIPO_INCASSO getIncasso() {
		return incasso;
	}



	public void setIncasso(TIPO_INCASSO incasso) {
		this.incasso = incasso;
	}



	public TIPO_DIRITTO getTipoDiritto() {
		return tipoDiritto;
	}



	public void setTipoDiritto(TIPO_DIRITTO tipoDiritto) {
		this.tipoDiritto = tipoDiritto;
	}



	public Boolean isSospesoPerContenzioso() {
		return sospesoPerContenzioso;
	}



	public void setSospesoPerContenzioso(Boolean sospesoPerContenzioso) {
		this.sospesoPerContenzioso = sospesoPerContenzioso;
	}



	public Boolean isRepoDisponibile() {
		return repoDisponibile;
	}



	public void setRepoDisponibile(Boolean repoDisponibile) {
		this.repoDisponibile = repoDisponibile;
	}



	public Boolean isImportoRipartibile() {
		return importoRipartibile;
	}



	public void setImportoRipartibile(Boolean importoRipartibile) {
		this.importoRipartibile = importoRipartibile;
	}



	public Date getInizioPeriodoCompetenza() {
		return inizioPeriodoCompetenza;
	}



	public void setInizioPeriodoCompetenza(Date inizioPeriodoCompetenza) {
		this.inizioPeriodoCompetenza = inizioPeriodoCompetenza;
	}



	public Date getFinePeriodoCompetenza() {
		return finePeriodoCompetenza;
	}



	public void setFinePeriodoCompetenza(Date finePeriodoCompetenza) {
		this.finePeriodoCompetenza = finePeriodoCompetenza;
	}



	public String getNotaCommentoUffEmittenti() {
		return notaCommentoUffEmittenti;
	}



	public void setNotaCommentoUffEmittenti(String notaCommentoUffEmittenti) {
		this.notaCommentoUffEmittenti = notaCommentoUffEmittenti;
	}



	public String getNotaCommentoDirRipartizione() {
		return notaCommentoDirRipartizione;
	}



	public void setNotaCommentoDirRipartizione(String notaCommentoDirRipartizione) {
		this.notaCommentoDirRipartizione = notaCommentoDirRipartizione;
	}

	public List<KPI>  getKpi() {
		return kpi;
	}

	public void setKpi(List<KPI> kpi) {
		this.kpi = kpi;
	}

	public Boolean getKpiValido() {
		return kpiValido;
	}

	public void setKpiValido(Boolean kpiValido) {
		this.kpiValido = kpiValido;
	}

	public Boolean getSospesoPerContenzioso() {
		return sospesoPerContenzioso;
	}

	public Boolean getRepoDisponibile() {
		return repoDisponibile;
	}

	public Boolean getImportoRipartibile() {
		return importoRipartibile;
	}

    
}
