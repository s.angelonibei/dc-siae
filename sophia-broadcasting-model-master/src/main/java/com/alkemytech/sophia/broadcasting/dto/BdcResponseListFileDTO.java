package com.alkemytech.sophia.broadcasting.dto;


import com.google.gson.annotations.SerializedName;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BdcResponseListFileDTO extends ResponseDTO {
    @XmlElement(name = "ResponseData")
    @SerializedName("ResponseData")
    private BdcResponseDataDTO responseData;

    public BdcResponseDataDTO getResponseData() {
        return responseData;
    }

    public void setResponseData(BdcResponseDataDTO responseData) {
        this.responseData = responseData;
    }
}
