package com.alkemytech.sophia.broadcasting.model.utilizations;

import com.alkemytech.sophia.broadcasting.model.BdcCanali;
import com.alkemytech.sophia.broadcasting.model.BdcNormalizedFile;
import com.alkemytech.sophia.broadcasting.model.ShowType;
import com.alkemytech.sophia.common.Constants;
import com.alkemytech.sophia.common.tools.StringTools;
import com.amazonaws.util.CollectionUtils;
import com.amazonaws.util.DateUtils;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang3.math.NumberUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static com.alkemytech.sophia.broadcasting.utils.Utilities.getMapValue;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "BDC_TV_SHOW_SCHEDULE")
public class BdcTvShowSchedule implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_TV_SHOW_SCHEDULE", nullable = false)
    private Long id;

    @OneToMany (cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "ID_TV_SHOW_SCHEDULE")
    List<BdcTvShowMusic> tvShowMusicList;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_NORMALIZED_FILE", nullable = false)
    private BdcNormalizedFile normalizedFile;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_CHANNEL", nullable = false)
    private BdcCanali canale;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_SHOW_TYPE")
    private ShowType showType;

    @Column(name = "TITLE", nullable = false)
    private String title;

    @Column( name = "ORIGINAL_TITLE")
    private String originalTitle;

    @Column( name = "BEGIN_TIME")
    private Date beginTime;

    @Column( name = "END_TIME")
    private Date endTime;

    @Column( name = "DURATION")
    private Integer duration;

    @Column( name = "REGIONAL_OFFICE")
    private String regionalOffice;

    @Column( name = "NOTE")
    private String note;

    @Column( name = "OVERLAP" )
    private boolean overlap;

    @Column( name = "SCHEDULE_YEAR")
    private Integer scheduleYear;

    @Column( name = "SCHEDULE_MONTH")
    private Integer scheduleMonth;

    @Column( name = "DAYS_FROM_UPLOAD")
    private Integer daysFromUpload;

    @Column( name = "CREATION_DATE")
    private Date creationDate;

    @Column( name = "MODIFY_DATE")
    private Date modifyDate;

    @Column( name = "FLAG_CONTENUTI")
    private  boolean flagContenuti;

    public BdcTvShowSchedule(BdcTvShowMusic tvShowMusic, BdcCanali canale, BdcNormalizedFile normalizedFile, ShowType showType, Date beginTime, Date endTime, Integer duration, Map<String, Object> record) {
        this.tvShowMusicList = new ArrayList<>();
        this.tvShowMusicList.add(tvShowMusic);
        this.canale = canale;
        this.normalizedFile = normalizedFile;
        this.showType = showType;
        this.beginTime = beginTime;
        this.endTime = endTime;
        this.duration = duration ;
        this.title = getMapValue(record, Constants.TITOLO_TX);
        this.originalTitle = getMapValue(record, Constants.TITOLO_ORIGINALE_TX);
        this.regionalOffice = getMapValue(record, Constants.DIFFUSIONE_REGIONALE);
        this.creationDate = new Date();
    }

    public BdcTvShowSchedule() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<BdcTvShowMusic> getTvShowMusicList() {
        return tvShowMusicList;
    }

    public void setTvShowMusicList(List<BdcTvShowMusic> tvShowMusicList) {
        this.tvShowMusicList = tvShowMusicList;
    }

    public BdcNormalizedFile getNormalizedFile() {
        return normalizedFile;
    }

    public void setNormalizedFile(BdcNormalizedFile normalizedFile) {
        this.normalizedFile = normalizedFile;
    }

    public BdcCanali getCanale() {
        return canale;
    }

    public void setCanale(BdcCanali canale) {
        this.canale = canale;
    }

    public ShowType getShowType() {
        return showType;
    }

    public void setShowType(ShowType showType) {
        this.showType = showType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }
    public String getRegionalOffice() {
        return regionalOffice;
    }

    public void setRegionalOffice(String regionalOffice) {
        this.regionalOffice = regionalOffice;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public boolean isOverlap() {
        return overlap;
    }

    public void setOverlap(boolean overlap) {
        this.overlap = overlap;
    }

    public Integer getScheduleYear() {
        return scheduleYear;
    }

    public void setScheduleYear(Integer scheduleYear) {
        this.scheduleYear = scheduleYear;
    }

    public Integer getScheduleMonth() {
        return scheduleMonth;
    }

    public void setScheduleMonth(Integer scheduleMonth) {
        this.scheduleMonth = scheduleMonth;
    }

    public Integer getDaysFromUpload() {
        return daysFromUpload;
    }

    public void setDaysFromUpload(Integer daysFromUpload) {
        this.daysFromUpload = daysFromUpload;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public void setFlagContenuti(boolean flagContenuti) {
        this.flagContenuti = flagContenuti;
    }

    public boolean isFlagContenuti() {
        return flagContenuti;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BdcTvShowSchedule)) return false;

        BdcTvShowSchedule that = (BdcTvShowSchedule) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (showType != null ? !showType.equals(that.showType) : that.showType != null) return false;
        return beginTime != null ? beginTime.equals(that.beginTime) : that.beginTime == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (showType != null ? showType.hashCode() : 0);
        result = 31 * result + (beginTime != null ? beginTime.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "BdcTvShowSchedule{" +
                "id=" + id +
                ", tvShowMusicList=" + tvShowMusicList +
                ", normalizedFile=" + normalizedFile +
                ", canale=" + canale +
                ", showType=" + showType +
                ", title='" + title + '\'' +
                ", originalTitle='" + originalTitle + '\'' +
                ", beginTime=" + beginTime +
                ", endTime=" + endTime +
                ", duration=" + duration +
                ", regionalOffice='" + regionalOffice + '\'' +
                ", note='" + note + '\'' +
                ", overlap=" + overlap +
                ", scheduleYear=" + scheduleYear +
                ", scheduleMonth=" + scheduleMonth +
                ", daysFromUpload=" + daysFromUpload +
                ", creationDate=" + creationDate +
                ", modifyDate=" + modifyDate +
                ", flagContenuti" + flagContenuti +
                '}';
    }

    public boolean isShow() {
        if( !CollectionUtils.isNullOrEmpty(this.getTvShowMusicList()) ) {
            String currentMusicType = this.getTvShowMusicList().get(0).getMusicType() != null ? this.getTvShowMusicList().get(0).getMusicType().getName(): "";
            if( !"COMMENTO".equalsIgnoreCase(currentMusicType) && !"PUBBLICITA".equalsIgnoreCase(currentMusicType)){
                return true;
            }
        }
        return false;
    }

    public boolean isPubblicita() {
        if( !CollectionUtils.isNullOrEmpty(this.getTvShowMusicList()) ) {
            String currentMusicType = this.getTvShowMusicList().get(0).getMusicType() != null ? this.getTvShowMusicList().get(0).getMusicType().getName(): "";
            if( "PUBBLICITA".equalsIgnoreCase(currentMusicType) ){
                return true;
            }
        }
        return false;
    }
    public boolean isFilm(){
        return !isShow() && !isPubblicita();
    }
}