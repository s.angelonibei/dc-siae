package com.alkemytech.sophia.broadcasting.dto.harmonization;

/**
 * Created by Alessandro Russo on 13/12/2017.
 */
public class FieldErrorDTO {

    private String fieldName;
    private String inputValue;

    public FieldErrorDTO() {
    }

    public FieldErrorDTO(String fieldName, String inputValue) {
        this.fieldName = fieldName;
        this.inputValue = inputValue;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getInputValue() {
        return inputValue;
    }

    public void setInputValue(String inputValue) {
        this.inputValue = inputValue;
    }

    @Override
    public String toString() {
        return "FieldErrorDTO{" +
                "fieldName='" + fieldName + '\'' +
                ", inputValue='" + inputValue + '\'' +
                '}';
    }
}