package com.alkemytech.sophia.broadcasting.dto;

import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.broadcasting.model.BdcBroadcasterConfigTemplate;

import java.util.Date;

/**
 * Created by Alessandro Russo on 27/11/2017.
 */
@Produces("application/json")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BdcConfigurationDTO {

    private Integer idConfiguration;
    private Integer idBroadcaster;
    private Integer idChannel;
    private String validFrom;
    private String validTo;
    private BdcBroadcasterConfigTemplate bdcBroadcasterConfigTemplate;
    private String username;
    
    public BdcConfigurationDTO() {
    }

    public BdcConfigurationDTO(Integer idBroadcaster, Integer idChannel) {
        this.idBroadcaster = idBroadcaster;
        this.idChannel = idChannel;
    }

    public Integer getIdConfiguration() {
		return idConfiguration;
	}

	public void setIdConfiguration(Integer idConfiguration) {
		this.idConfiguration = idConfiguration;
	}

	public Integer getIdBroadcaster() {
        return idBroadcaster;
    }

    public void setIdBroadcaster(Integer idBroadcaster) {
        this.idBroadcaster = idBroadcaster;
    }

    public Integer getIdChannel() {
        return idChannel;
    }

    public void setIdChannel(Integer idChannel) {
        this.idChannel = idChannel;
    }

	public String getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(String validFrom) {
		this.validFrom = validFrom;
	}

	public String getValidTo() {
		return validTo;
	}

	public void setValidTo(String validTo) {
		this.validTo = validTo;
	}

	public BdcBroadcasterConfigTemplate getBdcBroadcasterConfigTemplate() {
		return bdcBroadcasterConfigTemplate;
	}

	public void setBdcBroadcasterConfigTemplate(BdcBroadcasterConfigTemplate bdcBroadcasterConfigTemplate) {
		this.bdcBroadcasterConfigTemplate = bdcBroadcasterConfigTemplate;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}



   
}
