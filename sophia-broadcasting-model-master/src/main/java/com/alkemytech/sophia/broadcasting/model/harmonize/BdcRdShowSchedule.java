package com.alkemytech.sophia.broadcasting.model.harmonize;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the BDC_RD_SHOW_SCHEDULE database table.
 * 
 */
@Entity(name="RdShowSchedule")
@Table(name="BDC_RD_SHOW_SCHEDULE")
@NamedQuery(name="RdShowSchedule.findAll", query="SELECT b FROM RdShowSchedule b")
public class BdcRdShowSchedule implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_RD_SHOW_SCHEDULE", unique=true, nullable=false)
	private Integer idRdShowSchedule;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="BEGIN_TIME")
	private Date beginTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATION_DATE", nullable=false)
	private Date creationDate;

	@Column(name="DAYS_FROM_UPLOAD", nullable=false)
	private Integer daysFromUpload;

	@Column(name="DURATION")
	private Integer duration;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="END_TIME")
	private Date endTime;

	@Column(name="ID_NORMALIZED_FILE", nullable=false)
	private java.math.BigInteger idNormalizedFile;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="MODIFY_DATE")
	private Date modifyDate;

	@Column(name="NOTE", length=400)
	private String note;

	@Column(name="OVERLAP", nullable=false)
	private Boolean overlap;

	@Column(name="REGIONAL_OFFICE", length=10)
	private String regionalOffice;

	@Column(name="SCHEDULE_MONTH", nullable=false)
	private Integer scheduleMonth;

	@Column(name="SCHEDULE_YEAR", nullable=false)
	private Integer scheduleYear;

	@Lob
	@Column(name="TITLE")
	private String title;

	//bi-directional many-to-one association to BdcRdShowMusic
	@JsonManagedReference
	@OneToMany(mappedBy="bdcRdShowSchedule")
	private List<BdcRdShowMusic> bdcRdShowMusics;

	//bi-directional many-to-one association to BdcCanali
	@JsonBackReference
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CHANNEL")
	private BdcCanali bdcCanali;

	public BdcRdShowSchedule() {
	}

	public Integer getIdRdShowSchedule() {
		return this.idRdShowSchedule;
	}

	public void setIdRdShowSchedule(Integer idRdShowSchedule) {
		this.idRdShowSchedule = idRdShowSchedule;
	}

	public Date getBeginTime() {
		return this.beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Integer getDaysFromUpload() {
		return this.daysFromUpload;
	}

	public void setDaysFromUpload(Integer daysFromUpload) {
		this.daysFromUpload = daysFromUpload;
	}

	public Integer getDuration() {
		return this.duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Date getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public java.math.BigInteger getIdNormalizedFile() {
		return this.idNormalizedFile;
	}

	public void setIdNormalizedFile(java.math.BigInteger idNormalizedFile) {
		this.idNormalizedFile = idNormalizedFile;
	}

	public Date getModifyDate() {
		return this.modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getNote() {
		return this.note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Boolean getOverlap() {
		return this.overlap;
	}

	public void setOverlap(Boolean overlap) {
		this.overlap = overlap;
	}

	public String getRegionalOffice() {
		return this.regionalOffice;
	}

	public void setRegionalOffice(String regionalOffice) {
		this.regionalOffice = regionalOffice;
	}

	public Integer getScheduleMonth() {
		return this.scheduleMonth;
	}

	public void setScheduleMonth(Integer scheduleMonth) {
		this.scheduleMonth = scheduleMonth;
	}

	public Integer getScheduleYear() {
		return this.scheduleYear;
	}

	public void setScheduleYear(Integer scheduleYear) {
		this.scheduleYear = scheduleYear;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<BdcRdShowMusic> getBdcRdShowMusics() {
		return this.bdcRdShowMusics;
	}

	public void setBdcRdShowMusics(List<BdcRdShowMusic> bdcRdShowMusics) {
		this.bdcRdShowMusics = bdcRdShowMusics;
	}

	public BdcRdShowMusic addBdcRdShowMusic(BdcRdShowMusic bdcRdShowMusic) {
		getBdcRdShowMusics().add(bdcRdShowMusic);
		bdcRdShowMusic.setBdcRdShowSchedule(this);

		return bdcRdShowMusic;
	}

	public BdcRdShowMusic removeBdcRdShowMusic(BdcRdShowMusic bdcRdShowMusic) {
		getBdcRdShowMusics().remove(bdcRdShowMusic);
		bdcRdShowMusic.setBdcRdShowSchedule(null);

		return bdcRdShowMusic;
	}

	public BdcCanali getBdcCanali() {
		return this.bdcCanali;
	}

	public void setBdcCanali(BdcCanali bdcCanali) {
		this.bdcCanali = bdcCanali;
	}

}