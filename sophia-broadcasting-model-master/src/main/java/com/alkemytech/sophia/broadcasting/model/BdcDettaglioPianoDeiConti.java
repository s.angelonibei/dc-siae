package com.alkemytech.sophia.broadcasting.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

@XmlRootElement
@Entity
@Table(name = "BDC_DETTAGLIO_PIANO_DEI_CONTI")
public class BdcDettaglioPianoDeiConti implements Serializable {
	@Expose
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_BDC_DETTAGLIO_PIANO_DEI_CONTI", nullable = false)
	private Long idBdcDettaglioPianoDeiConti;
	@Expose
	@Column(name = "PERCENTUALE_ALLOCAZIONE", nullable = false)
	private Double percentualeAllocazione;
	@Expose
	@Column(name = "IMPORTO_ALLOCATO_DEFINITIVO", nullable = false)
	private Double importoAllocazioneDefinitivo;
	
	@Expose
	@ManyToOne
	@JoinColumn(name = "ID_CANALE", referencedColumnName = "id")
	private BdcCanali canali;

	@ManyToOne
	@JoinColumn(name = "ID_PIANO_DEI_CONTI")
	private BdcPianoDeiConti pianoDeiConti;

	public BdcDettaglioPianoDeiConti() {
		super();
	}

	public BdcDettaglioPianoDeiConti(Long idBdcDettaglioPianoDeiConti, Double percentualeAllocazione,
			Double importoAllocazioneDefinitivo, BdcCanali canali, BdcPianoDeiConti pianoDeiConti) {
		super();
		this.idBdcDettaglioPianoDeiConti = idBdcDettaglioPianoDeiConti;
		this.percentualeAllocazione = percentualeAllocazione;
		this.importoAllocazioneDefinitivo = importoAllocazioneDefinitivo;
		this.canali = canali;
		this.pianoDeiConti = pianoDeiConti;
	}

	public Long getIdBdcDettaglioPianoDeiConti() {
		return idBdcDettaglioPianoDeiConti;
	}

	public void setIdBdcDettaglioPianoDeiConti(Long idBdcDettaglioPianoDeiConti) {
		this.idBdcDettaglioPianoDeiConti = idBdcDettaglioPianoDeiConti;
	}

	public Double getPercentualeAllocazione() {
		return percentualeAllocazione;
	}

	public void setPercentualeAllocazione(Double percentualeAllocazione) {
		this.percentualeAllocazione = percentualeAllocazione;
	}

	public Double getImportoAllocazioneDefinitivo() {
		return importoAllocazioneDefinitivo;
	}

	public void setImportoAllocazioneDefinitivo(Double importoAllocazioneDefinitivo) {
		this.importoAllocazioneDefinitivo = importoAllocazioneDefinitivo;
	}

	public BdcCanali getCanali() {
		return canali;
	}

	public void setCanali(BdcCanali canali) {
		this.canali = canali;
	}

	public BdcPianoDeiConti getPianoDeiConti() {
		return pianoDeiConti;
	}

	public void setPianoDeiConti(BdcPianoDeiConti pianoDeiConti) {
		this.pianoDeiConti = pianoDeiConti;
	}

}
