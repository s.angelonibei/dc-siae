package com.alkemytech.sophia.broadcasting.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@XmlRootElement
@Entity
@Table(name = "BDC_CONTO")
public class BdcConto implements Serializable {

    @Id
    @Expose
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CODICE_CONTO", nullable = false)
    private String codiceConto;
    @Expose
    @Column(name = "IMPORTO", nullable = false)
    private Double importo;
    @Expose
    @Column(name = "PERIODO_DA", nullable = false)
    private Date periodoDa;
    @Expose
    @Column(name = "PERIODO_A", nullable = false)
    private Date periodoA;
    @Expose
    @Column(name = "DATA_ACQUISIZIONE", nullable = false)
    private Date dataAcquisizione;
    @Expose
    @JoinColumn(name = "REPERTORIO")
    private String repertorio;
    @Expose
    @Column(name = "TIPO_DIRITTO", nullable = false)
    private String tipoDiritto;

    @OneToMany(mappedBy = "codiceConto")
    private List<BdcPianoDeiConti> pianoDeiConti;

    public String getCodiceConto() {
        return codiceConto;
    }

    public void setCodiceConto(String codiceConto) {
        this.codiceConto = codiceConto;
    }

    public Double getImporto() {
        return importo;
    }

    public void setImporto(Double importo) {
        this.importo = importo;
    }

    public Date getPeriodoDa() {
        return periodoDa;
    }

    public void setPeriodoDa(Date periodoDa) {
        this.periodoDa = periodoDa;
    }

    public Date getPeriodoA() {
        return periodoA;
    }

    public void setPeriodoA(Date periodoA) {
        this.periodoA = periodoA;
    }

    public Date getDataAcquisizione() {
        return dataAcquisizione;
    }

    public void setDataAcquisizione(Date dataAcquisizione) {
        this.dataAcquisizione = dataAcquisizione;
    }

    public String getRepertorio() {
        return repertorio;
    }

    public void setRepertorio(String repertorio) {
        this.repertorio = repertorio;
    }

    public String getTipoDiritto() {
        return tipoDiritto;
    }

    public void setTipoDiritto(String tipoDiritto) {
        this.tipoDiritto = tipoDiritto;
    }

    public List<BdcPianoDeiConti> getPianoDeiConti() {
        return pianoDeiConti;
    }

    public void setPianoDeiConti(List<BdcPianoDeiConti> pianoDeiConti) {
        this.pianoDeiConti = pianoDeiConti;
    }

    @Override
    public String toString() {
        return "BdcConto{" +
                "codiceConto='" + codiceConto + '\'' +
                ", importo=" + importo +
                ", periodoDa=" + periodoDa +
                ", periodoA=" + periodoA +
                ", dataAcquisizione=" + dataAcquisizione +
                ", repertorio='" + repertorio + '\'' +
                ", tipoDiritto='" + tipoDiritto + '\'' +
                ", pianoDeiConti=" + pianoDeiConti +
                '}';
    }
}
