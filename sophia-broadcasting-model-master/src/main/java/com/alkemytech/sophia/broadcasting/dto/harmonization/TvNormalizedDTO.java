package com.alkemytech.sophia.broadcasting.dto.harmonization;

/**
 * Created by Alessandro Russo on 08/12/2017.
 */
public class TvNormalizedDTO extends BaseHarmonizedDTO {

    private String canale;
    private String genereTx;
    private String titoloTx;
    private String titoloOriginaleTx;
    private String dataInizioTx;
    private String oraInizioTx;
    private String oraFineTx;
    private String durataTx;
    private String replicaTx;
    private String paeseProduzioneTx;
    private String annoProduzioneTx;
    private String produttoreTx;
    private String registaTx;
    private String titoloEpisodioTx;
    private String titoloOriginaleEpisodioTx;
    private String numeroProgressivoEpisodioTx;

    private String titoloOp;
    private String compositoreAutoreOp1;
    private String compositoreAutoreOp2;
    private String durataOp;
    private String durataEffettivaUtilizzoOp;
    private String categoriaUsoOp;
    private String identificativoRai;

    public String getCanale() {
        return canale;
    }

    public void setCanale(String canale) {
        this.canale = canale;
    }

    public String getGenereTx() {
        return genereTx;
    }

    public void setGenereTx(String genereTx) {
        this.genereTx = genereTx;
    }

    public String getTitoloTx() {
        return titoloTx;
    }

    public void setTitoloTx(String titoloTx) {
        this.titoloTx = titoloTx;
    }

    public String getTitoloOriginaleTx() {
        return titoloOriginaleTx;
    }

    public void setTitoloOriginaleTx(String titoloOriginaleTx) {
        this.titoloOriginaleTx = titoloOriginaleTx;
    }

    public String getDataInizioTx() {
        return dataInizioTx;
    }

    public void setDataInizioTx(String dataInizioTx) {
        this.dataInizioTx = dataInizioTx;
    }

    public String getOraInizioTx() {
        return oraInizioTx;
    }

    public void setOraInizioTx(String oraInizioTx) {
        this.oraInizioTx = oraInizioTx;
    }

    public String getOraFineTx() {
        return oraFineTx;
    }

    public void setOraFineTx(String oraFineTx) {
        this.oraFineTx = oraFineTx;
    }

    public String getDurataTx() {
        return durataTx;
    }

    public void setDurataTx(String durataTx) {
        this.durataTx = durataTx;
    }

    public String getReplicaTx() {
        return replicaTx;
    }

    public void setReplicaTx(String replicaTx) {
        this.replicaTx = replicaTx;
    }

    public String getPaeseProduzioneTx() {
        return paeseProduzioneTx;
    }

    public void setPaeseProduzioneTx(String paeseProduzioneTx) {
        this.paeseProduzioneTx = paeseProduzioneTx;
    }

    public String getAnnoProduzioneTx() {
        return annoProduzioneTx;
    }

    public void setAnnoProduzioneTx(String annoProduzioneTx) {
        this.annoProduzioneTx = annoProduzioneTx;
    }

    public String getProduttoreTx() {
        return produttoreTx;
    }

    public void setProduttoreTx(String produttoreTx) {
        this.produttoreTx = produttoreTx;
    }

    public String getRegistaTx() {
        return registaTx;
    }

    public void setRegistaTx(String registaTx) {
        this.registaTx = registaTx;
    }

    public String getTitoloEpisodioTx() {
        return titoloEpisodioTx;
    }

    public void setTitoloEpisodioTx(String titoloEpisodioTx) {
        this.titoloEpisodioTx = titoloEpisodioTx;
    }

    public String getTitoloOriginaleEpisodioTx() {
        return titoloOriginaleEpisodioTx;
    }

    public void setTitoloOriginaleEpisodioTx(String titoloOriginaleEpisodioTx) {
        this.titoloOriginaleEpisodioTx = titoloOriginaleEpisodioTx;
    }

    public String getNumeroProgressivoEpisodioTx() {
        return numeroProgressivoEpisodioTx;
    }

    public void setNumeroProgressivoEpisodioTx(String numeroProgressivoEpisodioTx) {
        this.numeroProgressivoEpisodioTx = numeroProgressivoEpisodioTx;
    }

    public String getTitoloOp() {
        return titoloOp;
    }

    public void setTitoloOp(String titoloOp) {
        this.titoloOp = titoloOp;
    }

    public String getCompositoreAutoreOp1() {
        return compositoreAutoreOp1;
    }

    public void setCompositoreAutoreOp1(String compositoreAutoreOp1) {
        this.compositoreAutoreOp1 = compositoreAutoreOp1;
    }

    public String getCompositoreAutoreOp2() {
        return compositoreAutoreOp2;
    }

    public void setCompositoreAutoreOp2(String compositoreAutoreOp2) {
        this.compositoreAutoreOp2 = compositoreAutoreOp2;
    }

    public String getDurataOp() {
        return durataOp;
    }

    public void setDurataOp(String durataOp) {
        this.durataOp = durataOp;
    }

    public String getDurataEffettivaUtilizzoOp() {
        return durataEffettivaUtilizzoOp;
    }

    public void setDurataEffettivaUtilizzoOp(String durataEffettivaUtilizzoOp) {
        this.durataEffettivaUtilizzoOp = durataEffettivaUtilizzoOp;
    }

    public String getCategoriaUsoOp() {
        return categoriaUsoOp;
    }

    public void setCategoriaUsoOp(String categoriaUsoOp) {
        this.categoriaUsoOp = categoriaUsoOp;
    }

    public String getIdentificativoRai() {
        return identificativoRai;
    }

    public void setIdentificativoRai(String identificativoRai) {
        this.identificativoRai = identificativoRai;
    }
}
