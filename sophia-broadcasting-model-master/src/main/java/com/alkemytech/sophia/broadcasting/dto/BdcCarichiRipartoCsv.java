package com.alkemytech.sophia.broadcasting.dto;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.broadcasting.model.BdcCaricoRipartizione;
import com.alkemytech.sophia.broadcasting.model.BdcCaricoRipartizione.TIPO_DIRITTO;
import com.alkemytech.sophia.broadcasting.model.BdcCaricoRipartizione.TIPO_INCASSO;
import com.alkemytech.sophia.broadcasting.model.TipoBroadcaster;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BdcCarichiRipartoCsv {
	
    @CsvBindByPosition(position = 0)
    @CsvBindByName
	private TipoBroadcaster tipoEmittente;

    @CsvBindByPosition(position = 1)
    @CsvBindByName
	private String emittente;
    
    @CsvBindByPosition(position = 2)
    @CsvBindByName
	private String canale;
   
    @CsvBindByPosition(position = 3)
    @CsvBindByName
    private String inizioPeriodoCompetenza;   
    
    @CsvBindByPosition(position = 4)
    @CsvBindByName
    private String finePeriodoCompetenza;

    @CsvBindByPosition(position = 5)
    @CsvBindByName
    private String incassoNetto;
    
    @CsvBindByPosition(position = 6)
    @CsvBindByName
    private TIPO_DIRITTO tipoDiritto;
    
    @CsvBindByPosition(position = 7)
    @CsvBindByName
    private TIPO_INCASSO incasso;

    @CsvBindByPosition(position = 8)
    @CsvBindByName
    private String sospesoPerContenzioso;
    
    @CsvBindByPosition(position = 9)
    @CsvBindByName
    private String notaCommentoUffEmittenti;
    
    @CsvBindByPosition(position = 10)
    @CsvBindByName
    private String repoDisponibile;

    @CsvBindByPosition(position = 11)
    @CsvBindByName
    private String notaCommentoDirRipartizione;
    
    @CsvBindByPosition(position = 12)
    @CsvBindByName
    private String importoRipartibile;

    @CsvBindByPosition(position = 13)
    @CsvBindByName
    private String completezzaPalinsesto;
    
    @CsvBindByPosition(position = 14)
    @CsvBindByName
    private String musicaDichiarata; 
    
    @CsvBindByPosition(position = 15)
    @CsvBindByName
    private String opereMusicaliTitoloErroneo; 
    
    @CsvBindByPosition(position = 16)
    @CsvBindByName
    private String opereFilmicheTitoloErroneo; 
    
    @CsvBindByPosition(position = 17)
    @CsvBindByName
    private String autoriTitoloErroneo; 
    
    @CsvBindByPosition(position = 18)
    @CsvBindByName
    private String durataBraniDichiarati; 
    
    @CsvBindByPosition(position = 19)
    @CsvBindByName
    private String braniRadioTitoloErroneo; 
    
    @CsvBindByPosition(position = 20)
    @CsvBindByName
    private String autoriRadioTitoloErroneo;   
  

	public BdcCarichiRipartoCsv() {
		super();
	}

	public BdcCarichiRipartoCsv(
			String nomeCanale,
			String nomeEmittente, 
			TipoBroadcaster tipoCanale,
			BigDecimal incassoNetto,
			BdcCaricoRipartizione.TIPO_INCASSO incasso,
			boolean sospesoPerContenzioso,
			BdcCaricoRipartizione.TIPO_DIRITTO tipoDiritto,
			boolean repoDisponibile, 
			boolean importoRipartibile, 
			Date inizioPeriodoCompetenza,
			Date finePeriodoCompetenza,
			String notaCommentoUffEmittenti, 
			String notaCommentoDirRipartizione) {
		this.canale = nomeCanale;
		this.emittente = nomeEmittente;
		this.tipoEmittente = tipoCanale;
		this.incassoNetto = String.format("%4.2f" , incassoNetto).replace(".", ",");
		this.incasso = incasso;
		this.tipoDiritto = tipoDiritto;
		this.sospesoPerContenzioso = boolToString(sospesoPerContenzioso);
		this.repoDisponibile = boolToString(repoDisponibile);
		this.importoRipartibile = boolToString(importoRipartibile);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		this.inizioPeriodoCompetenza = sdf.format(inizioPeriodoCompetenza);
		this.finePeriodoCompetenza = sdf.format(finePeriodoCompetenza);
		this.notaCommentoUffEmittenti = notaCommentoUffEmittenti;
		this.notaCommentoDirRipartizione = notaCommentoDirRipartizione;
	}
	
	private String boolToString(boolean value) {
		if (value) {
			return "Si";
		}else {
			return "No";
		}
		
	}	

	public String getCompletezzaPalinsesto() {
		return completezzaPalinsesto;
	}

	public void setCompletezzaPalinsesto(String completezzaPalinsesto) {
		this.completezzaPalinsesto = completezzaPalinsesto;
	}

	public String getMusicaDichiarata() {
		return musicaDichiarata;
	}

	public void setMusicaDichiarata(String musicaDichiarata) {
		this.musicaDichiarata = musicaDichiarata;
	}

	public String getOpereMusicaliTitoloErroneo() {
		return opereMusicaliTitoloErroneo;
	}

	public void setOpereMusicaliTitoloErroneo(String opereMusicaliTitoloErroneo) {
		this.opereMusicaliTitoloErroneo = opereMusicaliTitoloErroneo;
	}

	public String getOpereFilmicheTitoloErroneo() {
		return opereFilmicheTitoloErroneo;
	}

	public void setOpereFilmicheTitoloErroneo(String opereFilmicheTitoloErroneo) {
		this.opereFilmicheTitoloErroneo = opereFilmicheTitoloErroneo;
	}

	public String getAutoriTitoloErroneo() {
		return autoriTitoloErroneo;
	}

	public void setAutoriTitoloErroneo(String autoriTitoloErroneo) {
		this.autoriTitoloErroneo = autoriTitoloErroneo;
	}

	public String getCanale() {
		return canale;
	}

	public void setCanale(String canale) {
		this.canale = canale;
	}

	public String getEmittente() {
		return emittente;
	}

	public void setEmittente(String emittente) {
		this.emittente = emittente;
	}

	public TipoBroadcaster getTipoEmittente() {
		return tipoEmittente;
	}

	public void setTipoEmittente(TipoBroadcaster tipoEmittente) {
		this.tipoEmittente = tipoEmittente;
	}

	public String getIncassoNetto() {
		return incassoNetto;
	}

	public void setIncassoNetto(String incassoNetto) {
		this.incassoNetto = incassoNetto;
	}

	public TIPO_INCASSO getIncasso() {
		return incasso;
	}

	public void setIncasso(TIPO_INCASSO incasso) {
		this.incasso = incasso;
	}

	public TIPO_DIRITTO getTipoDiritto() {
		return tipoDiritto;
	}

	public void setTipoDiritto(TIPO_DIRITTO tipoDiritto) {
		this.tipoDiritto = tipoDiritto;
	}

	public String getSospesoPerContenzioso() {
		return sospesoPerContenzioso;
	}

	public void setSospesoPerContenzioso(String sospesoPerContenzioso) {
		this.sospesoPerContenzioso = sospesoPerContenzioso;
	}

	public String getRepoDisponibile() {
		return repoDisponibile;
	}

	public void setRepoDisponibile(String repoDisponibile) {
		this.repoDisponibile = repoDisponibile;
	}

	public String getImportoRipartibile() {
		return importoRipartibile;
	}

	public void setImportoRipartibile(String importoRipartibile) {
		this.importoRipartibile = importoRipartibile;
	}

	public String getInizioPeriodoCompetenza() {
		return inizioPeriodoCompetenza;
	}

	public void setInizioPeriodoCompetenza(String inizioPeriodoCompetenza) {
		this.inizioPeriodoCompetenza = inizioPeriodoCompetenza;
	}

	public String getFinePeriodoCompetenza() {
		return finePeriodoCompetenza;
	}

	public void setFinePeriodoCompetenza(String finePeriodoCompetenza) {
		this.finePeriodoCompetenza = finePeriodoCompetenza;
	}

	public String getNotaCommentoUffEmittenti() {
		return notaCommentoUffEmittenti;
	}

	public void setNotaCommentoUffEmittenti(String notaCommentoUffEmittenti) {
		this.notaCommentoUffEmittenti = notaCommentoUffEmittenti;
	}

	public String getNotaCommentoDirRipartizione() {
		return notaCommentoDirRipartizione;
	}

	public void setNotaCommentoDirRipartizione(String notaCommentoDirRipartizione) {
		this.notaCommentoDirRipartizione = notaCommentoDirRipartizione;
	}

	

	
	public String getDurataBraniDichiarati() {
		return durataBraniDichiarati;
	}

	public void setDurataBraniDichiarati(String durataBraniDichiarati) {
		this.durataBraniDichiarati = durataBraniDichiarati;
	}

	public String getBraniRadioTitoloErroneo() {
		return braniRadioTitoloErroneo;
	}

	public void setBraniRadioTitoloErroneo(String braniRadioTitoloErroneo) {
		this.braniRadioTitoloErroneo = braniRadioTitoloErroneo;
	}

	public String getAutoriRadioTitoloErroneo() {
		return autoriRadioTitoloErroneo;
	}

	public void setAutoriRadioTitoloErroneo(String autoriRadioTitoloErroneo) {
		this.autoriRadioTitoloErroneo = autoriRadioTitoloErroneo;
	}

	public String[] getMappingStrategy() {
	      return new String[]{
	        	"Tipo Emittente",
        		"Emittente",
        		"Canale",
        		"Inizio periodo competenza",
        		"Fine periodo competenza",
        		"Incasso Netto",
        		"Tipo Diritto",
        		"Incasso",
        		"Sospeso per contenzioso",
        		"Nota commento uff emittenti",
        		"Report disponibile",
        		"Nota commento dir ripartizione",
        		"Importo Ripartibile",
        		"% Completezza palinsesto",
        		"% Musica dichiarata",
        		"Opere musicali con titolo erroneo",
        		"Opere filmiche con titolo erroneo",
        		"Autori con titolo erroneo",
        		"Durata brani dichiarati",
        		"Brani radio con titoli erronei",
        		"Autoriradio con titoli erronei"
	      };
	 }
    
}


