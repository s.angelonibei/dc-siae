package com.alkemytech.sophia.broadcasting.dto;

import com.alkemytech.sophia.broadcasting.enums.ArmonizedField;
import com.alkemytech.sophia.common.Constants;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;
import org.apache.commons.lang.StringUtils;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class FileArmonizzatoTvDTO {

    @CsvBindByPosition(position = 0)
    @CsvBindByName
    private String canale;
    @CsvBindByPosition(position = 1)
    @CsvBindByName
    private String genereTrasmissione;
    @CsvBindByName
    @CsvBindByPosition(position = 2)
    private String titoloTrasmissione;
    @CsvBindByName
    @CsvBindByPosition(position = 3)
    private String titoloOriginaleTrasmissione;
    @CsvBindByName
    @CsvBindByPosition(position = 4)
    private String dataInizioTrasmissione;
    @CsvBindByName
    @CsvBindByPosition(position = 5)
    private String orarioInizioTrasmissione;
    @CsvBindByName
    @CsvBindByPosition(position = 6)
    private String orarioFineTrasmissione;
    @CsvBindByName
    @CsvBindByPosition(position = 7)
    private String durataTrasmissione;
    @CsvBindByName
    @CsvBindByPosition(position = 8)
    private String replicaOPrimaEsecuzione;
    @CsvBindByName
    @CsvBindByPosition(position = 9)
    private String paeseDiProduzione;
    @CsvBindByName
    @CsvBindByPosition(position = 10)
    private String annoDiProduzione;
    @CsvBindByName
    @CsvBindByPosition(position = 11)
    private String produttore;
    @CsvBindByPosition(position = 12)
    @CsvBindByName
    private String regista;
    @CsvBindByPosition(position = 13)
    @CsvBindByName
    private String titoloEpisodio;
    @CsvBindByName
    @CsvBindByPosition(position = 14)
    private String titoloOriginaleEpisodio;
    @CsvBindByName
    @CsvBindByPosition(position = 15)
    private String numeroProgressivoEpisodio;
    @CsvBindByName
    @CsvBindByPosition(position = 16)
    private String titoloOperaMusicale;
    @CsvBindByPosition(position = 17)
    @CsvBindByName
    private String compositoreAutore1;
    @CsvBindByPosition(position = 18)
    @CsvBindByName
    private String compositoreAutore2;
    @CsvBindByPosition(position = 19)
    @CsvBindByName
    private String durataEffettivaDiUtilizzo;
    @CsvBindByPosition(position = 20)
    @CsvBindByName
    private String categoriaDUso;
    @CsvBindByPosition(position = 21)
    @CsvBindByName
    private Integer diffusioneRegionale;
    @CsvBindByPosition(position = 22)
    @CsvBindByName
    private String stato;
    @CsvBindByPosition(position = 23)
    @CsvBindByName
    private String campiObbligatoriMancanti;
    @CsvBindByPosition(position = 24)
    @CsvBindByName
    private String campiFormatoNonSupportato;
    @CsvBindByPosition(position = 25)
    @CsvBindByName
    private String campiConValoreInatteso;
    @CsvBindByPosition(position = 26)
    @CsvBindByName
    private String altriMotiviDiScarto;
    @CsvBindByPosition(position = 27)
    @CsvBindByName
    private String dataUpload;
    @CsvBindByPosition(position = 28)
    @CsvBindByName
    private String oraUpload;
    @CsvBindByPosition(position = 29)
    @CsvBindByName
    private String nomeFile;

    public FileArmonizzatoTvDTO() {

    }

    public String getCanale() {
        return canale;
    }

    public void setCanale(String canale) {
        this.canale = canale;
    }

    public String getGenereTrasmissione() {
        return genereTrasmissione;
    }

    public void setGenereTrasmissione(String genereTrasmissione) {
        this.genereTrasmissione = genereTrasmissione;
    }

    public String getTitoloTrasmissione() {
        return titoloTrasmissione;
    }

    public void setTitoloTrasmissione(String titoloTrasmissione) {
        this.titoloTrasmissione = titoloTrasmissione;
    }

    public String getTitoloOriginaleTrasmissione() {
        return titoloOriginaleTrasmissione;
    }

    public void setTitoloOriginaleTrasmissione(String titoloOriginaleTrasmissione) {
        this.titoloOriginaleTrasmissione = titoloOriginaleTrasmissione;
    }

    public String getDataInizioTrasmissione() {
        return dataInizioTrasmissione;
    }

    public void setDataInizioTrasmissione(String dataInizioTrasmissione) {
        this.dataInizioTrasmissione = dataInizioTrasmissione;
    }

    public String getOrarioInizioTrasmissione() {
        return orarioInizioTrasmissione;
    }

    public void setOrarioInizioTrasmissione(String orarioInizioTrasmissione) {
        this.orarioInizioTrasmissione = orarioInizioTrasmissione;
    }

    public String getOrarioFineTrasmissione() {
        return orarioFineTrasmissione;
    }

    public void setOrarioFineTrasmissione(String orarioFineTrasmissione) {
        this.orarioFineTrasmissione = orarioFineTrasmissione;
    }

    public String getDurataTrasmissione() {
        return durataTrasmissione;
    }

    public void setDurataTrasmissione(String durataTrasmissione) {
        this.durataTrasmissione = durataTrasmissione;
    }

    public String getReplicaOPrimaEsecuzione() {
        return replicaOPrimaEsecuzione;
    }

    public void setReplicaOPrimaEsecuzione(String replicaOPrimaEsecuzione) {
        this.replicaOPrimaEsecuzione = replicaOPrimaEsecuzione;
    }

    public String getPaeseDiProduzione() {
        return paeseDiProduzione;
    }

    public void setPaeseDiProduzione(String paeseDiProduzione) {
        this.paeseDiProduzione = paeseDiProduzione;
    }

    public String getAnnoDiProduzione() {
        return annoDiProduzione;
    }

    public void setAnnoDiProduzione(String annoDiProduzione) {
        this.annoDiProduzione = annoDiProduzione;
    }

    public String getProduttore() {
        return produttore;
    }

    public void setProduttore(String produttore) {
        this.produttore = produttore;
    }

    public String getRegista() {
        return regista;
    }

    public void setRegista(String regista) {
        this.regista = regista;
    }

    public String getTitoloEpisodio() {
        return titoloEpisodio;
    }

    public void setTitoloEpisodio(String titoloEpisodio) {
        this.titoloEpisodio = titoloEpisodio;
    }

    public String getTitoloOriginaleEpisodio() {
        return titoloOriginaleEpisodio;
    }

    public void setTitoloOriginaleEpisodio(String titoloOriginaleEpisodio) {
        this.titoloOriginaleEpisodio = titoloOriginaleEpisodio;
    }

    public String getNumeroProgressivoEpisodio() {
        return numeroProgressivoEpisodio;
    }

    public void setNumeroProgressivoEpisodio(String numeroProgressivoEpisodio) {
        this.numeroProgressivoEpisodio = numeroProgressivoEpisodio;
    }

    public String getTitoloOperaMusicale() {
        return titoloOperaMusicale;
    }

    public void setTitoloOperaMusicale(String titoloOperaMusicale) {
        this.titoloOperaMusicale = titoloOperaMusicale;
    }

    public String getCompositoreAutore1() {
        return compositoreAutore1;
    }

    public void setCompositoreAutore1(String compositoreAutore1) {
        this.compositoreAutore1 = compositoreAutore1;
    }

    public String getCompositoreAutore2() {
        return compositoreAutore2;
    }

    public void setCompositoreAutore2(String compositoreAutore2) {
        this.compositoreAutore2 = compositoreAutore2;
    }

//    public String getDurataDellOpera() {
//        return durataDellOpera;
//    }
//
//    public void setDurataDellOpera(String durataDellOpera) {
//        this.durataDellOpera = durataDellOpera;
//    }

    public String getDurataEffettivaDiUtilizzo() {
        return durataEffettivaDiUtilizzo;
    }

    public void setDurataEffettivaDiUtilizzo(String durataEffettivaDiUtilizzo) {
        this.durataEffettivaDiUtilizzo = durataEffettivaDiUtilizzo;
    }

    public String getCategoriaDUso() {
        return categoriaDUso;
    }

    public void setCategoriaDUso(String categoriaDUso) {
        this.categoriaDUso = categoriaDUso;
    }

    public Integer getDiffusioneRegionale() {
        return diffusioneRegionale;
    }

    public void setDiffusioneRegionale(Integer diffusioneRegionale) {
        this.diffusioneRegionale = diffusioneRegionale;
    }

    public String getStato() {
        return stato;
    }

    public void setStato(String stato) {
        this.stato = stato;
    }

    public String getCampiObbligatoriMancanti() {
        return campiObbligatoriMancanti;
    }

    public void setCampiObbligatoriMancanti(String campiObbligatoriMancanti) {
        this.campiObbligatoriMancanti = campiObbligatoriMancanti;
    }

    public String getCampiFormatoNonSupportato() {
        return campiFormatoNonSupportato;
    }

    public void setCampiFormatoNonSupportato(String campiFormatoNonSupportato) {
        this.campiFormatoNonSupportato = campiFormatoNonSupportato;
    }

    public String getCampiConValoreInatteso() {
        return campiConValoreInatteso;
    }

    public void setCampiConValoreInatteso(String campiConValoreInatteso) {
        this.campiConValoreInatteso = campiConValoreInatteso;
    }

    public String getAltriMotiviDiScarto() {
        return altriMotiviDiScarto;
    }

    public void setAltriMotiviDiScarto(String altriMotiviDiScarto) {
        this.altriMotiviDiScarto = altriMotiviDiScarto;
    }

    public String getNomeFile() {
        return nomeFile;
    }

    public void setNomeFile(String nomeFile) {
        this.nomeFile = nomeFile;
    }

    public String getDataUpload() {
        return dataUpload;
    }

    public void setDataUpload(String dataUpload) {
        this.dataUpload = dataUpload;
    }

    public String getOraUpload() {
        return oraUpload;
    }

    public void setOraUpload(String oraUpload) {
        this.oraUpload = oraUpload;
    }

    public String[] getMappingStrategy() {
        return new String[]{

                ArmonizedField.CANALE.getDescription(),
                ArmonizedField.GENERE_TRASMISSIONE.getDescription(),
                ArmonizedField.TITOLO_TRASMISSIONE.getDescription(),
                ArmonizedField.TITOLO_ORIGINALE_TRASMISSIONE.getDescription(),
                ArmonizedField.DATA_INIZIO_TRASMISSIONE.getDescription(),
                ArmonizedField.ORARIO_INIZIO_TRASMISSIONE.getDescription(),
                ArmonizedField.ORARIO_FINE_TRASMISSIONE.getDescription(),
                ArmonizedField.DURATA_TRASMISSIONE.getDescription(),
                ArmonizedField.REPLICA_O_PRIMA_ESECUZIONE.getDescription(),
                ArmonizedField.PAESE_DI_PRODUZIONE.getDescription(),
                ArmonizedField.ANNO_DI_PRODUZIONE.getDescription(),
                ArmonizedField.PRODUTTORE.getDescription(),
                ArmonizedField.REGISTA.getDescription(),
                ArmonizedField.TITOLO_EPISODIO.getDescription(),
                ArmonizedField.TITOLO_ORIGINALE_EPISODIO.getDescription(),
                ArmonizedField.NUMERO_PROGRESSIVO_EPISODIO.getDescription(),
                ArmonizedField.TITOLO_OPERA_MUSICALE.getDescription(),
                ArmonizedField.COMPOSITORE_AUTORE_1.getDescription(),
                ArmonizedField.COMPOSITORE_AUTORE_2.getDescription(),
                ArmonizedField.DURATA.getDescription(),
                ArmonizedField.CATEGORIA_USO.getDescription(),
                ArmonizedField.DIFFUSIONE_REGIONALE_TV.getDescription(),
                ArmonizedField.STATO.getDescription(),
                ArmonizedField.REGOLA1.getDescription(),
                ArmonizedField.REGOLA2.getDescription(),
                ArmonizedField.REGOLA3.getDescription(),
                ArmonizedField.ERRORE_GENERICO.getDescription(),

                ArmonizedField.DATA_UPLOAD.getDescription(),
                ArmonizedField.ORA_UPLOAD.getDescription(),
                ArmonizedField.NOME_FILE.getDescription(),
        };
    }

    public FileArmonizzatoTvDTO(java.lang.String canale,
                                java.lang.String genereTrasmissione,
                                java.lang.String titoloTrasmissione,
                                java.lang.String titoloOriginaleTrasmissione,
                                java.lang.String dataInizioTrasmissione,
                                java.lang.String orarioInizioTrasmissione,
                                java.lang.String orarioFineTrasmissione,
                                java.lang.String durataTrasmissione,
                                java.lang.String replicaOPrimaEsecuzione,
                                java.lang.String paeseDiProduzione,
                                java.lang.String annoDiProduzione,
                                java.lang.String produttore,
                                java.lang.String regista,
                                java.lang.String titoloEpisodio,
                                java.lang.String titoloOriginaleEpisodio,
                                java.lang.String numeroProgressivoEpisodio,
                                java.lang.String titoloOperaMusicale,
                                java.lang.String compositoreAutore1,
                                java.lang.String compositoreAutore2,
                                java.lang.String durataEffettivaDiUtilizzo,
                                java.lang.String categoriaDUso,
                                java.lang.Integer diffusioneRegionale,
                                java.lang.String nomeFile,
                                java.lang.String dataUpload,
                                java.lang.String oraUpload) {
        this.canale = canale;
        this.genereTrasmissione = genereTrasmissione;
        this.titoloTrasmissione = titoloTrasmissione;
        this.titoloOriginaleTrasmissione = titoloOriginaleTrasmissione;
        this.dataInizioTrasmissione = dataInizioTrasmissione;
        this.orarioInizioTrasmissione = orarioInizioTrasmissione;
        this.orarioFineTrasmissione = orarioFineTrasmissione;
        this.durataTrasmissione = durataTrasmissione;
        this.replicaOPrimaEsecuzione = replicaOPrimaEsecuzione;
        this.paeseDiProduzione = paeseDiProduzione;
        this.annoDiProduzione = annoDiProduzione;
        this.produttore = produttore;
        this.regista = regista;
        this.titoloEpisodio = titoloEpisodio;
        this.titoloOriginaleEpisodio = titoloOriginaleEpisodio;
        this.numeroProgressivoEpisodio = numeroProgressivoEpisodio;
        this.titoloOperaMusicale = titoloOperaMusicale;
        this.compositoreAutore1 = compositoreAutore1;
        this.compositoreAutore2 = compositoreAutore2;
        this.durataEffettivaDiUtilizzo = durataEffettivaDiUtilizzo;
        this.categoriaDUso = categoriaDUso;
        this.diffusioneRegionale = diffusioneRegionale;
        this.stato = "OK";
        this.nomeFile = nomeFile;
        this.dataUpload = dataUpload;
        this.oraUpload = oraUpload;
    }
    public FileArmonizzatoTvDTO(java.lang.String canale,
                                java.lang.String genereTrasmissione,
                                java.lang.String titoloTrasmissione,
                                java.lang.String titoloOriginaleTrasmissione,
                                java.lang.String dataInizioTrasmissione,
                                java.lang.String orarioInizioTrasmissione,
                                java.lang.String orarioFineTrasmissione,
                                java.lang.String durataTrasmissione,
                                java.lang.String replicaOPrimaEsecuzione,
                                java.lang.String paeseDiProduzione,
                                java.lang.String annoDiProduzione,
                                java.lang.String produttore,
                                java.lang.String regista,
                                java.lang.String titoloEpisodio,
                                java.lang.String titoloOriginaleEpisodio,
                                java.lang.String numeroProgressivoEpisodio,
                                java.lang.String titoloOperaMusicale,
                                java.lang.String compositoreAutore1,
                                java.lang.String compositoreAutore2,
                                java.lang.String durataEffettivaDiUtilizzo,
                                java.lang.String categoriaDUso,
                                java.lang.Integer diffusioneRegionale,
                                java.lang.String altriMotiviDiScarto,
                                java.lang.String nomeFile,
                                java.lang.String dataUpload,
                                java.lang.String oraUpload) {
        this.canale = canale;
        this.genereTrasmissione = genereTrasmissione;
        this.titoloTrasmissione = titoloTrasmissione;
        this.titoloOriginaleTrasmissione = titoloOriginaleTrasmissione;
        this.dataInizioTrasmissione = dataInizioTrasmissione;
        this.orarioInizioTrasmissione = orarioInizioTrasmissione;
        this.orarioFineTrasmissione = orarioFineTrasmissione;
        this.durataTrasmissione = durataTrasmissione;
        this.replicaOPrimaEsecuzione = replicaOPrimaEsecuzione;
        this.paeseDiProduzione = paeseDiProduzione;
        this.annoDiProduzione = annoDiProduzione;
        this.produttore = produttore;
        this.regista = regista;
        this.titoloEpisodio = titoloEpisodio;
        this.titoloOriginaleEpisodio = titoloOriginaleEpisodio;
        this.numeroProgressivoEpisodio = numeroProgressivoEpisodio;
        this.titoloOperaMusicale = titoloOperaMusicale;
        this.compositoreAutore1 = compositoreAutore1;
        this.compositoreAutore2 = compositoreAutore2;
        this.durataEffettivaDiUtilizzo = durataEffettivaDiUtilizzo;
        this.categoriaDUso = categoriaDUso;
        this.diffusioneRegionale = diffusioneRegionale;
        this.stato = "OK";
        this.altriMotiviDiScarto=altriMotiviDiScarto;
        this.nomeFile = nomeFile;
        this.dataUpload = dataUpload;
        this.oraUpload = oraUpload;
    }


    public FileArmonizzatoTvDTO(java.lang.String canale,
                                java.lang.String genereTrasmissione,
                                java.lang.String titoloTrasmissione,
                                java.lang.String titoloOriginaleTrasmissione,
                                java.lang.String dataInizioTrasmissione,
                                java.lang.String orarioInizioTrasmissione,
                                java.lang.String orarioFineTrasmissione,
                                java.lang.String durataTrasmissione,
                                java.lang.String replicaOPrimaEsecuzione,
                                java.lang.String paeseDiProduzione,
                                java.lang.String annoDiProduzione,
                                java.lang.String produttore,
                                java.lang.String regista,
                                java.lang.String titoloEpisodio,
                                java.lang.String titoloOriginaleEpisodio,
                                java.lang.String numeroProgressivoEpisodio,
                                java.lang.String titoloOperaMusicale,
                                java.lang.String compositoreAutore1,
                                java.lang.String compositoreAutore2,
                                java.lang.String durataEffettivaDiUtilizzo,
                                java.lang.String categoriaDUso,
                                java.lang.Integer diffusioneRegionale,
                                java.lang.String stato,
                                java.lang.String errore,
                                java.lang.String altriMotiviDiScarto,
                                java.lang.String nomeFile,
                                java.lang.String dataUpload,
                                java.lang.String oraUpload) {
        this.canale = canale;
        this.genereTrasmissione = genereTrasmissione;
        this.titoloTrasmissione = titoloTrasmissione;
        this.titoloOriginaleTrasmissione = titoloOriginaleTrasmissione;
        this.dataInizioTrasmissione = dataInizioTrasmissione;
        this.orarioInizioTrasmissione = orarioInizioTrasmissione;
        this.orarioFineTrasmissione = orarioFineTrasmissione;
        this.durataTrasmissione = durataTrasmissione;
        this.replicaOPrimaEsecuzione = replicaOPrimaEsecuzione;
        this.paeseDiProduzione = paeseDiProduzione;
        this.annoDiProduzione = annoDiProduzione;
        this.produttore = produttore;
        this.regista = regista;
        this.titoloEpisodio = titoloEpisodio;
        this.titoloOriginaleEpisodio = titoloOriginaleEpisodio;
        this.numeroProgressivoEpisodio = numeroProgressivoEpisodio;
        this.titoloOperaMusicale = titoloOperaMusicale;
        this.compositoreAutore1 = compositoreAutore1;
        this.compositoreAutore2 = compositoreAutore2;
        this.durataEffettivaDiUtilizzo = durataEffettivaDiUtilizzo;
        this.categoriaDUso = categoriaDUso;
        this.diffusioneRegionale = diffusioneRegionale;
        this.stato = stato;
        if (stato.equals("KO")) {
            this.campiObbligatoriMancanti = regola(errore, Constants.REGOLA1);
            this.campiFormatoNonSupportato = regola(errore, Constants.REGOLA2);
            this.campiConValoreInatteso = regola(errore, Constants.REGOLA3);
            this.altriMotiviDiScarto = altriMotiviDiScarto;
        }
        this.nomeFile = nomeFile;
        this.dataUpload = dataUpload;
        this.oraUpload = oraUpload;
    }

    public FileArmonizzatoTvDTO(Object[] obj) {
        try {
            canale = (String) obj[0];
            genereTrasmissione = (String) obj[1];
            titoloTrasmissione = (String) obj[2];
            titoloOriginaleTrasmissione = (String) obj[3];
            dataInizioTrasmissione = (String) obj[4];
            orarioInizioTrasmissione = (String) obj[5];
            orarioFineTrasmissione = (String) obj[6];
            durataTrasmissione = (String) obj[7];
            replicaOPrimaEsecuzione = (String) obj[8];
            paeseDiProduzione = (String) obj[9];
            annoDiProduzione = (String) obj[10];
            produttore = (String) obj[11];
            regista = (String) obj[12];
            titoloEpisodio = (String) obj[13];
            titoloOriginaleEpisodio = (String) obj[14];
            numeroProgressivoEpisodio = (String) obj[15];
            titoloOperaMusicale = (String) obj[16];
            compositoreAutore1 = (String) obj[17];
            compositoreAutore2 = (String) obj[18];
            durataEffettivaDiUtilizzo = (String) obj[20];
            categoriaDUso = (String) obj[21];
            diffusioneRegionale = Integer.parseInt("" + obj[22]);
            stato = (String) obj[23];
            campiObbligatoriMancanti = regola((String) obj[24], Constants.REGOLA1);
            campiFormatoNonSupportato = regola((String) obj[24], Constants.REGOLA2);
            campiConValoreInatteso = regola((String) obj[24], Constants.REGOLA3);
            altriMotiviDiScarto = (String) obj[25];
            nomeFile = (String) obj[26];
            dataUpload = (String) obj[27];
            oraUpload = (String) obj[28];
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private String regola(String regola, String descRegola) {
        if (StringUtils.isNotEmpty(regola)) {
            StringBuffer errorString = new StringBuffer();
            ObjectMapper objectMapper = new ObjectMapper();
            Gson gson = new Gson();
            Map<String, List<Map<String, String>>> mapErrori = gson.fromJson(regola, Map.class);
            errorToString(mapErrori, errorString, descRegola);
            if (errorString.length() > 0) {
                return errorString.toString();
            }
        }
        return "";
    }

    private void errorToString(Map<String, List<Map<String, String>>> mapErrori, StringBuffer errorString, String errorKey) {
        if (mapErrori.containsKey(errorKey)) {
            List<Map<String, String>> errList = mapErrori.get(errorKey);
            if (!errList.isEmpty()) {
                Iterator<Map<String, String>> iter = errList.iterator();
                Map<String, String> error = null;
                while (iter.hasNext()) {
                    error = iter.next();
                    if (error != null && error.containsKey("fieldName")) {
                        String field = ArmonizedField.findDescription(error.get("fieldName"));
                        if (StringUtils.isNotEmpty(field)) {
                            errorString.append(field);
                            if (iter.hasNext()) {
                                errorString.append(", ");
                            }
                        }
                    }
                }
            }
        }
    }
}