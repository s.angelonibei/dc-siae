package com.alkemytech.sophia.broadcasting.dto;

public class BdcContiDTO {
	private Integer pianoConti;
	private Integer idBroadcaster;
	private String tipoEmittente;
	private String emittente;
	private String listaCanali;
	private String tipoDiritto;
	private String periodoDa;
	private String periodoA;
	private String conto;
	private String dataCreazione;
	private String utenteUltimaModifica;
	public Integer getPianoConti() {
		return pianoConti;
	}

	public void setPianoConti(Integer pianoConti) {
		this.pianoConti = pianoConti;
	}

	public Integer getIdBroadcaster() {
		return idBroadcaster;
	}

	public void setIdBroadcaster(Integer idBroadcaster) {
		this.idBroadcaster = idBroadcaster;
	}

	public String getTipoEmittente() {
		return tipoEmittente;
	}

	public void setTipoEmittente(String tipoEmittente) {
		this.tipoEmittente = tipoEmittente;
	}

	public String getEmittente() {
		return emittente;
	}

	public void setEmittente(String emittente) {
		this.emittente = emittente;
	}

	public String getListaCanali() {
		return listaCanali;
	}

	public void setListaCanali(String listaCanali) {
		this.listaCanali = listaCanali;
	}

	public String getTipoDiritto() {
		return tipoDiritto;
	}

	public void setTipoDiritto(String tipoDiritto) {
		this.tipoDiritto = tipoDiritto;
	}

	public String getPeriodoDa() {
		return periodoDa;
	}

	public void setPeriodoDa(String periodoDa) {
		this.periodoDa = periodoDa;
	}

	public String getPeriodoA() {
		return periodoA;
	}

	public void setPeriodoA(String periodoA) {
		this.periodoA = periodoA;
	}

	public String getConto() {
		return conto;
	}

	public void setConto(String conto) {
		this.conto = conto;
	}

	public String getDataCreazione() {
		return dataCreazione;
	}

	public void setDataCreazione(String dataCreazione) {
		this.dataCreazione = dataCreazione;
	}

	public String getUtenteUltimaModifica() {
		return utenteUltimaModifica;
	}

	public void setUtenteUltimaModifica(String utenteUltimaModifica) {
		this.utenteUltimaModifica = utenteUltimaModifica;
	}

}
