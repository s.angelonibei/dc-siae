package com.alkemytech.sophia.broadcasting.utils;

import java.util.List;

public class QueryUtils {
    public static String listToInClause(List list) {
        if(list == null){
            return null;
        }
        String output = "";
        for (Object o: list
                ) {
            if(!output.equals("")) {
                output += "," + o;
            }else{
                output += o;
            }
        }
        return output;
    }
}
