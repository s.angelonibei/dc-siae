package com.alkemytech.sophia.broadcasting.dto.performing;

import com.alkemytech.sophia.performing.model.PerfPalinsesto;

import java.util.Date;
import java.util.List;

public class RSMonitoraggioKPIOutputDTO {
    private Date lastUpdate;
    private int kpiAttesi;
    private PerfPalinsesto palinsesti;
    private List<RSKPI> listaKPI;
    private List<RSInformationFileDTO> fileDaElaborare;

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public int getKpiAttesi() {
        return kpiAttesi;
    }

    public void setKpiAttesi(int kpiAttesi) {
        this.kpiAttesi = kpiAttesi;
    }

    public List<RSKPI> getListaKPI() {
		return listaKPI;
	}

	public void setListaKPI(List<RSKPI> listaKPI) {
		this.listaKPI = listaKPI;
	}

	public List<RSInformationFileDTO> getFileDaElaborare() {
        return fileDaElaborare;
    }

    public void setFileDaElaborare(List<RSInformationFileDTO> fileDaElaborare) {
        this.fileDaElaborare = fileDaElaborare;
    }

	public PerfPalinsesto getPalinsesti() {
		return palinsesti;
	}

	public void setPalinsesti(PerfPalinsesto palinsesti) {
		this.palinsesti = palinsesti;
	}

}
