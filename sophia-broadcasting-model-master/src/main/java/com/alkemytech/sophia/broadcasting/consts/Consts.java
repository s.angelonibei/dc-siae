package com.alkemytech.sophia.broadcasting.consts;

public interface Consts {

    static final int KPIATTESITV = 3;
    static final int KPIATTESIRADIO = 2;
    static final int KPIATTESITVBKO = 9;
    static final int KPIATTESIRADIOBKO = 5;

    static final String SINGLE_UPLOAD   = "SINGOLO";
    static final String MASSIVE_UPLOAD  = "MASSIVO";

    static final String ACCEPTED    = "accepted";
    static final String DISCARDED   = "discarded";

}
