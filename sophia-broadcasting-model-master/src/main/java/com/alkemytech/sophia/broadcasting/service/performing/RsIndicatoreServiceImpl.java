package com.alkemytech.sophia.broadcasting.service.performing;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.alkemytech.sophia.broadcasting.data.IndicatoriPerCanale;
import com.alkemytech.sophia.broadcasting.dto.IndicatoreDTO;
import com.alkemytech.sophia.broadcasting.dto.performing.RSRequestMonitoraggioKPIDTO;
import com.alkemytech.sophia.broadcasting.model.BdcKpiEntity;
import com.alkemytech.sophia.performing.model.PerfPalinsesto;
import com.google.inject.Inject;
import com.google.inject.Provider;

public class RsIndicatoreServiceImpl implements RsIndicatoreService {

    private final Provider<EntityManager> provider;

    @Inject
    public RsIndicatoreServiceImpl(Provider<EntityManager> provider) {
        this.provider = provider;
    }

    @SuppressWarnings("unchecked")
	@Override
    public List<IndicatoreDTO> listaInd(RSRequestMonitoraggioKPIDTO monitoraggioKPI) {

        EntityManager entityManager = provider.get();
        List<IndicatoreDTO> listaIndicatori;


        String q = "select new com.alkemytech.sophia.broadcasting.dto.IndicatoreDTO (b.indicatore, sum(b.valore), max(b.lastUpdate)) " +
                "from PerfRsKpi b " +
                "WHERE b.musicProvider.idMusicProvider=:idMusicProvider " +
                "AND b.perfPalinsesto IN :palinsesti " +
                "AND b.anno=:anno " +
                "AND b.mese IN :mese " +
                "group by b.indicatore";

        TypedQuery<IndicatoreDTO> queryKPI = entityManager.createQuery(q, IndicatoreDTO.class);

        listaIndicatori = queryKPI
                .setParameter("idMusicProvider", monitoraggioKPI.getMusicProvider().getIdMusicProvider())
                .setParameter("palinsesti", monitoraggioKPI.getPalinsesti())
                .setParameter("anno", monitoraggioKPI.getAnno())
                .setParameter("mese", monitoraggioKPI.getMesi())
                .getResultList();

        return listaIndicatori;
    }

   
   

    private Long calcolaInRitardo(Long idCanale, Integer anno, Integer mese) {
		EntityManager entityManager = provider.get();
        String q;
        PerfPalinsesto palinsesto = entityManager.find(PerfPalinsesto.class,  BigInteger.valueOf(idCanale));
            q = "SELECT count(*) "
                    + " FROM BDC_RD_SHOW_SCHEDULE t LEFT JOIN BDC_RD_SHOW_MUSIC m ON t.ID_RD_SHOW_SCHEDULE = m.ID_RD_SHOW_SCHEDULE "
                    + " WHERE t.ID_CHANNEL = ?1 "
                    + " AND t.SCHEDULE_YEAR = ?2"
                    + "	AND t.SCHEDULE_MONTH = ?3 "
                    + " AND t.DAYS_FROM_UPLOAD > ?4"
                    + " AND t.REGIONAL_OFFICE IS NULL ";

	    Query query = entityManager.createNativeQuery(q);
	
	    Object ret = query.setParameter(1, idCanale)
	    		.setParameter(2, anno)
	            .setParameter(3, mese)
	            .setParameter(4, 90)
	            .getSingleResult();
	    
	    Long recordInRitardo = new Long(0); 
	    if(ret != null && ret instanceof Long){
	    	recordInRitardo = (Long)ret;
	    }    	
	    return recordInRitardo;
    }

    private Long calcolaRecordTotali(Integer idCanale, Integer anno, Integer mese) {
    	EntityManager entityManager = provider.get();
	    	String q;
	    PerfPalinsesto palinsesto = entityManager.find(PerfPalinsesto.class,  BigInteger.valueOf(idCanale));
                q = "select count(*) "
                + " FROM BDC_RD_SHOW_SCHEDULE t left join BDC_RD_SHOW_MUSIC m on t.ID_RD_SHOW_SCHEDULE = m.ID_RD_SHOW_SCHEDULE "
                + " WHERE t.ID_CHANNEL = ?1 "
                + " AND t.SCHEDULE_YEAR = ?2"
                + "	AND t.SCHEDULE_MONTH = ?3"
                + " AND t.REGIONAL_OFFICE IS NULL ";
        Query query = entityManager.createNativeQuery(q);

        Object ret = query.setParameter(1, idCanale)
        		.setParameter(2, anno)
                .setParameter(3, mese)
                .getSingleResult();
        
        Long recordValidi = new Long(0); 
        if(ret != null && ret instanceof Long){
        	recordValidi = (Long)ret;
        }    	
        return recordValidi;
    }

    private Long calcolaRecordScartati(Integer idCanale, Integer anno,Integer mese) {
    	EntityManager entityManager = provider.get();
        String q;
        PerfPalinsesto palinsesto = entityManager.find(PerfPalinsesto.class,  BigInteger.valueOf(idCanale));

            q =       " SELECT COUNT(*) FROM( "
                    + " SELECT DISTINCT t.ID_ERR_RD_SHOW_SCHEDULE "
                    + " FROM BDC_ERR_RD_SHOW_SCHEDULE t "
                    + " LEFT JOIN BDC_ERR_RD_SHOW_MUSIC m "
                    + " ON t.ID_ERR_RD_SHOW_SCHEDULE = m.ID_ERR_RD_SHOW_SCHEDULE "
                    + " LEFT JOIN BDC_UTILIZATION_NORMALIZED_FILE z "
                    + " ON t.ID_NORMALIZED_FILE = z.ID_NORMALIZED_FILE "
                    + " LEFT JOIN bdc_utilization_file b "
                    + " ON z.ID_UTILIZATION_FILE = b.id "
                    + " WHERE ((t.ID_CHANNEL = ?1 AND b.palinsesto IS NULL) OR b.palinsesto = ?1) "
                    + " AND ((t.SCHEDULE_YEAR  = ?2 AND b.anno IS NULL) OR b.anno = ?2) "
                    + "	AND ((t.SCHEDULE_MONTH = ?3 AND b.mese IS NULL) OR b.mese = ?3) "
                    + " AND t.REGIONAL_OFFICE IS NULL "
                    + " GROUP BY t.ID_ERR_RD_SHOW_SCHEDULE) as conteggio ";
        Query query = entityManager.createNativeQuery(q);

        Object ret = query.setParameter(1, idCanale)
        		.setParameter(2, anno)
                .setParameter(3, mese)
                .getSingleResult();
        
        Long recordScartati = new Long(0); 
        if(ret != null && ret instanceof Long){
        	recordScartati = (Long)ret;
        }    	
        return recordScartati;
    }

    private Long calcolaDurataBrani(Integer idCanale, Integer anno, Integer mese) {
        EntityManager entityManager = provider.get();
        String q;
        PerfPalinsesto palinsesto = entityManager.find(PerfPalinsesto.class,  BigInteger.valueOf(idCanale));
     
        q = "SELECT sum(st.DURATION) "
                + " FROM BDC_RD_SHOW_SCHEDULE t, BDC_RD_SHOW_MUSIC st "
                + " WHERE t.ID_RD_SHOW_SCHEDULE = st.ID_RD_SHOW_SCHEDULE "
                + " AND t.ID_CHANNEL = ?1 "
                + " AND t.SCHEDULE_YEAR = ?2 "
                + "	AND t.SCHEDULE_MONTH = ?3 "
                + " AND t.REGIONAL_OFFICE IS NULL ";
        

        Query query = entityManager.createNativeQuery(q);

        Object ret = query.setParameter(1, idCanale)
                .setParameter(2, anno)
                .setParameter(3, mese)
                .getSingleResult();

        Long durata = new Long(0);
        if(ret != null && ret instanceof BigDecimal){
            durata = ((BigDecimal)ret).longValue();
        }
        return durata;
    }

    private Long calcolaDurataBraniDichiarati(Integer idCanale, Integer anno, Integer mese) {
        EntityManager entityManager = provider.get();

           String q = "SELECT sum(st.REAL_DURATION) "
                    + " FROM BDC_TV_SHOW_SCHEDULE t, BDC_TV_SHOW_MUSIC st, BDC_SHOW_TYPE w "
                    + " WHERE t.ID_TV_SHOW_SCHEDULE = st.ID_TV_SHOW_SCHEDULE "
                    + " AND t.ID_SHOW_TYPE = w.ID_SHOW_TYPE "
                    + " AND t.ID_CHANNEL = ?1 "
                    + " AND t.SCHEDULE_YEAR = ?2 "
                    + "	AND t.SCHEDULE_MONTH = ?3 "
                    + " AND t.REGIONAL_OFFICE IS NULL ";

        Query query = entityManager.createNativeQuery(q);

        Object ret = query.setParameter(1, idCanale)
                .setParameter(2, anno)
                .setParameter(3, mese)
                .getSingleResult();

        Long durata = new Long(0);
        if(ret != null && ret instanceof BigDecimal){
            durata = ((BigDecimal)ret).longValue();
        }
        return durata;
    }

    private Long calcolaDurataBraniTitoliErronei(Integer idCanale, Integer anno, Integer mese) {
        EntityManager entityManager = provider.get();
        String q;
        PerfPalinsesto palinsesto = entityManager.find(PerfPalinsesto.class,  BigInteger.valueOf(idCanale));
     
        q = "SELECT sum(BDC_RD_SHOW_MUSIC.DURATION) "
                + " FROM BDC_RD_ERR_BRANO_TITOLO, BDC_RD_SHOW_SCHEDULE "
                + " LEFT JOIN BDC_RD_SHOW_MUSIC "
                + " ON BDC_RD_SHOW_SCHEDULE.ID_RD_SHOW_SCHEDULE = BDC_RD_SHOW_MUSIC.ID_RD_SHOW_SCHEDULE "
                + " WHERE BDC_RD_SHOW_SCHEDULE.ID_CHANNEL = ?1 "
                + " AND BDC_RD_SHOW_SCHEDULE.SCHEDULE_YEAR = ?2 "
                + "	AND BDC_RD_SHOW_SCHEDULE.SCHEDULE_MONTH = ?3 "
                + " AND BDC_RD_SHOW_SCHEDULE.REGIONAL_OFFICE IS NULL "
                + " AND LOWER(BDC_RD_SHOW_MUSIC.TITLE) IN (LOWER(BDC_RD_ERR_BRANO_TITOLO.RD_ERR_BRANO_TITOLO)) ";
        

        Query query = entityManager.createNativeQuery(q);

        Object ret = query.setParameter(1, idCanale)
                .setParameter(2, anno)
                .setParameter(3, mese)
                .getSingleResult();

        Long durata = new Long(0);
        if(ret != null && ret instanceof BigDecimal){
            durata = ((BigDecimal)ret).longValue();
        }
        return durata;
    }

    private Long calcolaDurataFilm(Integer idCanale, Integer anno, Integer mese) {
    	EntityManager entityManager = provider.get();

    	String q = "select sum(t.DURATION) "
    			+ " FROM BDC_TV_SHOW_SCHEDULE t, BDC_SHOW_TYPE st "
    			+ " WHERE t.ID_SHOW_TYPE = st.ID_SHOW_TYPE "
    			+ " AND t.ID_CHANNEL = ?1 "
    			+ " AND t.SCHEDULE_YEAR = ?2 "
    			+ "	AND t.SCHEDULE_MONTH = ?3 "
    			+ " AND st.CATEGORY IN ('FILM', 'TELEFILM') "
                + " AND t.REGIONAL_OFFICE IS NULL ";

        Query query = entityManager.createNativeQuery(q);

        Object ret4 = query.setParameter(1, idCanale)
        		.setParameter(2, anno)
                .setParameter(3, mese)
                .getSingleResult();
        
        Long durata = new Long(0); 
        if(ret4 != null && ret4 instanceof BigDecimal){
        	durata = ((BigDecimal)ret4).longValue();
        }    	
        return durata;
    }

    private Long calcolaDurataGeneriTrasmissioneVuoti(Integer idCanale, Integer anno, Integer mese) {
        EntityManager entityManager = provider.get();

        String q = "select sum(t.DURATION) "
                + " FROM BDC_TV_SHOW_SCHEDULE t "
                + " WHERE t.ID_CHANNEL = ?1 "
                + " AND t.SCHEDULE_YEAR = ?2 "
                + "	AND t.SCHEDULE_MONTH = ?3 "
                + " AND t.ID_SHOW_TYPE IS NULL "
                + " AND t.REGIONAL_OFFICE IS NULL ";

        Query query = entityManager.createNativeQuery(q);

        Object ret4 = query.setParameter(1, idCanale)
                .setParameter(2, anno)
                .setParameter(3, mese)
                .getSingleResult();

        Long durata = new Long(0);
        if(ret4 != null && ret4 instanceof BigDecimal){
            durata = ((BigDecimal)ret4).longValue();
        }
        return durata;
    }

    private Long calcolaDurataPublicita(Integer idCanale, Integer anno, Integer mese) {
    	EntityManager entityManager = provider.get();

    	String q = "select sum(t.DURATION) "
    			+ " FROM BDC_TV_SHOW_SCHEDULE t, BDC_SHOW_TYPE st "
    			+ " WHERE t.ID_SHOW_TYPE = st.ID_SHOW_TYPE "
    			+ " AND t.ID_CHANNEL = ?1 "
    			+ " AND t.SCHEDULE_YEAR = ?2 "
    			+ "	AND t.SCHEDULE_MONTH = ?3 "
    			+ " AND t.OVERLAP = 0 "
    			+ " AND st.CATEGORY LIKE 'PUBBLICIT%'"
                + " AND t.REGIONAL_OFFICE IS NULL ";

        Query query = entityManager.createNativeQuery(q);

        Object ret3 = query.setParameter(1, idCanale)
        		.setParameter(2, anno)
                .setParameter(3, mese)
                .getSingleResult();
        
        Long durata = new Long(0); 
        if(ret3 != null && ret3 instanceof BigDecimal){
        	durata = ((BigDecimal)ret3).longValue();
        }    
        return durata;
    }

    private Long calcolaDurataTrasmissioni(Integer idCanale, Integer anno, Integer mese) {
    	EntityManager entityManager = provider.get();

    	String q = "select sum(t.DURATION) "
    			+ " FROM BDC_TV_SHOW_SCHEDULE t, BDC_SHOW_TYPE st "
    			+ " WHERE t.ID_SHOW_TYPE = st.ID_SHOW_TYPE "
    			+ " AND t.ID_CHANNEL = ?1 "
    			+ " AND t.SCHEDULE_YEAR = ?2 "
    			+ "	AND t.SCHEDULE_MONTH = ?3 "
    			+ " AND st.CATEGORY = 'SHOW TV'"
                + " AND t.REGIONAL_OFFICE IS NULL ";

        Query query = entityManager.createNativeQuery(q);

        Object ret2 = query.setParameter(1, idCanale)
        		.setParameter(2, anno)
                .setParameter(3, mese)
                .getSingleResult();
        
        Long durata = new Long(0); 
        if(ret2 != null && ret2 instanceof BigDecimal){
        	durata = ((BigDecimal)ret2).longValue();
        }    	
        return durata;
    }

    private Long calcolaTrasmissioniTotali(Integer idCanale, Integer anno, Integer mese) {
        EntityManager entityManager = provider.get();

        String q = " SELECT COUNT(*) " +
                   " FROM BDC_TV_SHOW_SCHEDULE " +
                   " LEFT JOIN BDC_SHOW_TYPE " +
                   " ON BDC_TV_SHOW_SCHEDULE.ID_SHOW_TYPE = BDC_SHOW_TYPE.ID_SHOW_TYPE " +
                   " WHERE BDC_TV_SHOW_SCHEDULE.ID_CHANNEL = ?1 " +
                   " AND BDC_TV_SHOW_SCHEDULE.SCHEDULE_YEAR = ?2 " +
                   " AND BDC_TV_SHOW_SCHEDULE.SCHEDULE_MONTH = ?3 " +
                   " AND BDC_TV_SHOW_SCHEDULE.REGIONAL_OFFICE IS NULL " +
                   " AND BDC_SHOW_TYPE.CATEGORY IN ('SHOW TV') ";

        Query query = entityManager.createNativeQuery(q);

        Object ret = query.setParameter(1, idCanale)
                .setParameter(2, anno)
                .setParameter(3, mese)
                .getSingleResult();

        Long trasmissioniValide = new Long(0);
        if(ret != null && ret instanceof Long){
            trasmissioniValide = (Long)ret;
        }
        return trasmissioniValide;
    }

    private Long calcolaTrasmissioniMusicheEccedenti(Integer idCanale, Integer anno, Integer mese) {
        EntityManager entityManager = provider.get();

        String q = " SELECT SUM(cont) FROM " +
                   " (SELECT " +
                   " IF((SUM(BDC_TV_SHOW_MUSIC.REAL_DURATION)>BDC_TV_SHOW_SCHEDULE.DURATION), 1, 0) AS cont " +
                   " FROM BDC_TV_SHOW_SCHEDULE " +
                   " LEFT JOIN BDC_TV_SHOW_MUSIC " +
                   " ON BDC_TV_SHOW_SCHEDULE.ID_TV_SHOW_SCHEDULE = BDC_TV_SHOW_MUSIC.ID_TV_SHOW_SCHEDULE " +
                   " LEFT JOIN BDC_SHOW_TYPE " +
                   " ON BDC_TV_SHOW_SCHEDULE.ID_SHOW_TYPE = BDC_SHOW_TYPE.ID_SHOW_TYPE " +
                   " WHERE BDC_TV_SHOW_SCHEDULE.ID_CHANNEL = ?1 " +
                   " AND BDC_TV_SHOW_SCHEDULE.SCHEDULE_YEAR = ?2 " +
                   " AND BDC_TV_SHOW_SCHEDULE.SCHEDULE_MONTH = ?3 " +
                   " AND BDC_SHOW_TYPE.CATEGORY IN ('SHOW TV') " +
                   " AND BDC_TV_SHOW_SCHEDULE.REGIONAL_OFFICE IS NULL " +
                   " GROUP BY BDC_TV_SHOW_MUSIC.ID_TV_SHOW_SCHEDULE)as conteggio ";

        Query query = entityManager.createNativeQuery(q);

        Object ret1 = query.setParameter(1, idCanale)
                .setParameter(2, anno)
                .setParameter(3, mese)
                .getSingleResult();

        Long trasmissioniEccedenti = new Long(0);
        if(ret1 != null && ret1 instanceof BigDecimal){
            trasmissioniEccedenti = ((BigDecimal)ret1).longValue();
        }
        return trasmissioniEccedenti;
    }

    private Long calcolaDurataAutoriTitoliErronei(Integer idCanale, Integer anno, Integer mese) {
        EntityManager entityManager = provider.get();
        String q;
        PerfPalinsesto palinsesto = entityManager.find(PerfPalinsesto.class,  BigInteger.valueOf(idCanale));
     
            q = "SELECT sum(BDC_RD_SHOW_MUSIC.DURATION) "
                    + " FROM BDC_RD_ERR_COMPOSITORE_TITOLO, BDC_RD_SHOW_SCHEDULE "
                    + " LEFT JOIN BDC_RD_SHOW_MUSIC "
                    + " ON BDC_RD_SHOW_SCHEDULE.ID_RD_SHOW_SCHEDULE = BDC_RD_SHOW_MUSIC.ID_RD_SHOW_SCHEDULE "
                    + " WHERE BDC_RD_SHOW_SCHEDULE.ID_CHANNEL = ?1 "
                    + " AND BDC_RD_SHOW_SCHEDULE.SCHEDULE_YEAR = ?2 "
                    + "	AND BDC_RD_SHOW_SCHEDULE.SCHEDULE_MONTH = ?3 "
                    + " AND BDC_RD_SHOW_SCHEDULE.REGIONAL_OFFICE IS NULL "
                    + " AND LOWER(BDC_RD_SHOW_MUSIC.TITLE) IN (LOWER(BDC_RD_ERR_COMPOSITORE_TITOLO.RD_ERR_COMPOSITORE_TITOLO)) ";
        Query query = entityManager.createNativeQuery(q);

        Object ret = query.setParameter(1, idCanale)
                .setParameter(2, anno)
                .setParameter(3, mese)
                .getSingleResult();

        Long durata = new Long(0);
        if(ret != null && ret instanceof BigDecimal){
            durata = ((BigDecimal)ret).longValue();
        }
        return durata;
    }

    private Long calcolaFilmTitoliErronei(Integer idCanale, Integer anno, Integer mese) {
        EntityManager entityManager = provider.get();

        String q =  " SELECT " +
                    " SUM(BDC_TV_SHOW_SCHEDULE.DURATION) " +
                    " FROM BDC_TV_ERR_FILM_TITOLO, BDC_TV_SHOW_SCHEDULE " +
                    " LEFT JOIN BDC_SHOW_TYPE " +
                    " ON BDC_TV_SHOW_SCHEDULE.ID_SHOW_TYPE = BDC_SHOW_TYPE.ID_SHOW_TYPE " +
                    " WHERE BDC_TV_SHOW_SCHEDULE.ID_CHANNEL = ?1 " +
                    " AND BDC_TV_SHOW_SCHEDULE.SCHEDULE_YEAR = ?2 " +
                    " AND BDC_TV_SHOW_SCHEDULE.SCHEDULE_MONTH = ?3 " +
                    " AND BDC_SHOW_TYPE.CATEGORY IN ('FILM', 'TELEFILM') " +
                    " AND BDC_TV_SHOW_SCHEDULE.REGIONAL_OFFICE IS NULL " +
                    " AND LOWER(BDC_TV_SHOW_SCHEDULE.TITLE) IN (LOWER(BDC_TV_ERR_FILM_TITOLO.TV_ERR_FILM_TITOLO)) ";

        Query query = entityManager.createNativeQuery(q);

        Object ret1 = query.setParameter(1, idCanale)
                .setParameter(2, anno)
                .setParameter(3, mese)
                .getSingleResult();

        Long trasmissioniEccedenti = new Long(0);
        if(ret1 != null && ret1 instanceof BigDecimal){
            trasmissioniEccedenti = ((BigDecimal)ret1).longValue();
        }
        return trasmissioniEccedenti;
    }

    private Long calcolaProgrammiTitoliErronei(Integer idCanale, Integer anno, Integer mese) {
        EntityManager entityManager = provider.get();

        String q =  " SELECT " +
                " SUM(BDC_TV_SHOW_SCHEDULE.DURATION) " +
                " FROM BDC_TV_ERR_PROGRAMMA_TITOLO, BDC_TV_SHOW_SCHEDULE " +
                " LEFT JOIN BDC_SHOW_TYPE " +
                " ON BDC_TV_SHOW_SCHEDULE.ID_SHOW_TYPE = BDC_SHOW_TYPE.ID_SHOW_TYPE " +
                " WHERE BDC_TV_SHOW_SCHEDULE.ID_CHANNEL = ?1 " +
                " AND BDC_TV_SHOW_SCHEDULE.SCHEDULE_YEAR = ?2 " +
                " AND BDC_TV_SHOW_SCHEDULE.SCHEDULE_MONTH = ?3 " +
                " AND BDC_TV_SHOW_SCHEDULE.REGIONAL_OFFICE IS NULL " +
                " AND BDC_SHOW_TYPE.CATEGORY IN ('SHOW TV') " +
                " AND LOWER(BDC_TV_SHOW_SCHEDULE.TITLE) IN (LOWER(BDC_TV_ERR_PROGRAMMA_TITOLO.TV_ERR_PROGRAMMA_TITOLO)) ";

        Query query = entityManager.createNativeQuery(q);

        Object ret1 = query.setParameter(1, idCanale)
                .setParameter(2, anno)
                .setParameter(3, mese)
                .getSingleResult();

        Long trasmissioniEccedenti = new Long(0);
        if(ret1 != null && ret1 instanceof BigDecimal){
            trasmissioniEccedenti = ((BigDecimal)ret1).longValue();
        }
        return trasmissioniEccedenti;
    }
   
    public IndicatoriPerCanale indicatoriPerCanale(Integer annoDa, Integer meseDa, Integer annoA, Integer meseA){
	    	IndicatoriPerCanale indicatoriPerCanale = new IndicatoriPerCanale();
	    	
	    	final int TIPO_EMITTENTE = 0;
	    	final int CANALE = 1;
	    	final int ANNO = 2;
	    	final int MESE = 3;
	    	final int INDICATORE = 4;
	    	final int VALORE = 5;
	    	
	    	EntityManager entityManager = provider.get();
	        String q =
	        		   " SELECT " +
	        		   "   bdc_canali.tipo_palinsesto, " +
	        		   "   bdc_kpi.palinsesto, " +
	        		   "   bdc_kpi.anno, " +
	        		   "   bdc_kpi.mese, " +
	        		   "   bdc_kpi.indicatore, " +
	        		   "   bdc_kpi.valore " +
	        	       " FROM bdc_kpi LEFT JOIN bdc_canali ON bdc_kpi.palinsesto = bdc_canali.id " +
	        	       " WHERE ( bdc_kpi.anno > ?1 OR (bdc_kpi.anno = ?1 AND bdc_kpi.mese >= ?2) ) " +
	        	       "   AND ( bdc_kpi.anno < ?3 OR (bdc_kpi.anno = ?3 AND bdc_kpi.mese <= ?4) ) " +
	        	       "   AND bdc_kpi.indicatore <> ?5 ";
	        
	        Query query = entityManager.createNativeQuery(q);
	        List resultList = query
	        		.setParameter(1, annoDa)
	                .setParameter(2, meseDa)
	                .setParameter(3, annoA)
	                .setParameter(4, meseA)
	                .setParameter(5, BdcKpiEntity.DURATA_DISPONIBILE.toString())
	                .getResultList();
	        
	        if(resultList != null){
	            for(Object result : resultList){
	                if(result instanceof Object[]){
	                    Object[] record = (Object[])result;
	                    String tipoMusicProvider = (String)record[TIPO_EMITTENTE];
	                    Integer palinsesto = (Integer)record[CANALE];
	                    Integer anno = (Integer)record[ANNO];
	                    Integer mese = (Integer)record[MESE];
	                    String indicatore = (String)record[INDICATORE];                    
	                    Long valore = 
	                    	(record[VALORE] instanceof BigDecimal)?
	                            ((BigDecimal)record[VALORE]).longValue() 
	                        :
	                            ((record[VALORE] instanceof Long) ? 
	                            	(Long)record[VALORE]	
	                            :
	                            	((Integer)record[VALORE]).longValue()
	                            );
	                    indicatoriPerCanale.add(tipoMusicProvider, palinsesto, anno, mese, indicatore, valore);
	                }
	            }
	        }
	    	return indicatoriPerCanale;
    }
    
    
    /**
        restituisce una Map< idCanale, Map<indicatore,valore> >
     */
    public HashMap<Integer, Map<String,Long>> mapCanaleIndicatore(Integer annoDa, Integer meseDa, Integer annoA, Integer meseA){

        HashMap<Integer, Map<String,Long>> retMap = new HashMap<Integer, Map<String,Long>>();

        EntityManager entityManager = provider.get();
        String q =
        		   " SELECT palinsesto, indicatore, SUM(valore) AS valore " +
        	               " FROM bdc_kpi LEFT JOIN bdc_canali ON bdc_kpi.palinsesto = bdc_canali.id " +
        	               " WHERE ( anno > ?1 OR (anno = ?1 AND mese >= ?2) ) " +
        	               " AND ( anno < ?3 OR (anno = ?3 AND mese <= ?4) ) " +
        	               " AND bdc_canali.tipo_palinsesto = ?5 "+
        	               " AND indicatore <> ?6 " +
        	               " GROUP BY indicatore, palinsesto " +
        	               " ORDER BY palinsesto ASC ";

        Query query = entityManager.createNativeQuery(q);
        List resultList = query
        			.setParameter(1, annoDa)
                .setParameter(2, meseDa)
                .setParameter(3, annoA)
                .setParameter(4, meseA)
                .setParameter(5, BdcKpiEntity.DURATA_DISPONIBILE.toString())
                .getResultList();

 
        if(resultList != null){
            for(Object result : resultList){
                if(result instanceof Object[]){
                    Object[] record = (Object[])result;
                    Integer idCanale = (Integer)record[0];
                    String indicatore = (String)record[1];
                    Long valore = (record[2] instanceof BigDecimal)?
                            ((BigDecimal)record[2]).longValue() :
                            (Long)record[2];

                    if(!retMap.containsKey(idCanale)){
                        retMap.put(idCanale,new HashMap<String,Long>());
                    }
                    Map<String,Long> mapIndicatori = retMap.get(idCanale);
                    mapIndicatori.put(indicatore, valore);
                }
            }
        }

        return retMap;
    }

    public Long calcolaDurataDisponibile(Date dataDa, Date dataA) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(dataDa);

        GregorianCalendar calendarDa = new GregorianCalendar();
        calendarDa.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
        calendarDa.set(Calendar.MONTH, calendar.get(Calendar.MONTH));
        calendarDa.set(Calendar.DAY_OF_MONTH, 1);

        calendar.setTime(dataA);
        GregorianCalendar calendarA = new GregorianCalendar();
        calendarA.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
        calendarA.set(Calendar.MONTH, calendar.get(Calendar.MONTH));
        calendarA.set(Calendar.DAY_OF_MONTH, 1);
        calendarA.add(Calendar.MONTH, 1);


        Long startMs = calendarDa.getTimeInMillis();
        Long fineMs = calendarA.getTimeInMillis();
        Long seconds = (fineMs - startMs) / 1000;
        return seconds;
    }

    public Long calcolaDurataDisponibile(Integer anno, List<Short> mesi) {
        Long seconds = Long.valueOf(0);
        for (int i = mesi.size()-1; i>=0; i--) {
            Calendar calendar = GregorianCalendar.getInstance();
            calendar.set(Calendar.YEAR, anno);
            calendar.set(Calendar.MONTH, mesi.get(i)-1);
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            Long inizioMese = calendar.getTimeInMillis();
            calendar.add(Calendar.MONTH, 1);
            Long inizioMeseSuccessivo = calendar.getTimeInMillis();
            seconds += (inizioMeseSuccessivo - inizioMese) / 1000;
        }
        return seconds;
    }
}