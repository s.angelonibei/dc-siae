package com.alkemytech.sophia.broadcasting.dto;

import com.alkemytech.sophia.broadcasting.model.BdcInformationFileEntity;

public class ResponseDataInformationFileDTO {

    private String output;
    private BdcInformationFileEntity input;

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public BdcInformationFileEntity getInput() {
        return input;
    }

    public void setInput(BdcInformationFileEntity input) {
        this.input = input;
    }
}
