package com.alkemytech.sophia.broadcasting.model.harmonize;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the bdc_broadcasters database table.
 * 
 */
@Entity
@Table(name="bdc_broadcasters")
@NamedQuery(name="BdcBroadcaster.findAll", query="SELECT b FROM BdcBroadcaster b")
public class BdcBroadcaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private Integer id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_creazione")
	private Date dataCreazione;

	@Lob
	private byte[] image;

	@Column(nullable=false, length=255)
	private String nome;

	@Column(name="tipo_broadcaster", nullable=false, length=1)
	private String tipoBroadcaster;

	//bi-directional many-to-one association to BdcCanali
	@JsonBackReference
	@OneToMany(mappedBy="bdcBroadcaster")
	private List<BdcCanali> bdcCanalis;

	//bi-directional many-to-one association to BdcUtilizationFile
	@JsonBackReference
	@OneToMany(mappedBy="bdcBroadcaster")
	private List<BdcUtilizationFile> bdcUtilizationFiles;

	public void setId(Integer id) {
		this.id = id;
	}

	public BdcBroadcaster() {
	}

	public Date getDataCreazione() {
		return this.dataCreazione;
	}

	public void setDataCreazione(Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}

	public byte[] getImage() {
		return this.image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTipoBroadcaster() {
		return this.tipoBroadcaster;
	}

	public void setTipoBroadcaster(String tipoBroadcaster) {
		this.tipoBroadcaster = tipoBroadcaster;
	}

	public List<BdcCanali> getBdcCanalis() {
		return this.bdcCanalis;
	}

	public void setBdcCanalis(List<BdcCanali> bdcCanalis) {
		this.bdcCanalis = bdcCanalis;
	}

	public List<BdcUtilizationFile> getBdcUtilizationFiles() {
		return this.bdcUtilizationFiles;
	}

	public void setBdcUtilizationFiles(List<BdcUtilizationFile> bdcUtilizationFiles) {
		this.bdcUtilizationFiles = bdcUtilizationFiles;
	}
}