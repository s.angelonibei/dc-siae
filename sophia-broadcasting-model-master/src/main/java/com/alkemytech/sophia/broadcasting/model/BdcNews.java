package com.alkemytech.sophia.broadcasting.model;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.text.SimpleDateFormat;
import java.util.Date;

@XmlRootElement
@Entity
@Table(name = "BDC_NEWS")
public class BdcNews {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "BDC_NEWS_ID", nullable = false)
	private Integer id;

	@Column(name = "TITLE", nullable = false)
	@CsvBindByPosition(position = 0)
	@CsvBindByName
	private String title;

	@Column(name = "NEWS", nullable = false)
	@CsvBindByPosition(position = 1)
	@CsvBindByName
	private String news;

	@OneToOne
	@JoinColumn(name = "ID_BROADCASTER")
	/*@CsvBindByPosition(position = 2)
	@CsvBindByName*/
	private BdcBroadcasters bdcBroadcasters;

	@Column(name = "ID_USER")
	/*@CsvBindByPosition(position = 3)
	@CsvBindByName*/
	private Integer idUtente;

	@Column(name = "FLAG_FOR_ALL_BDC", nullable = false)
	/*@CsvBindByPosition(position = 4)
	@CsvBindByName*/
	private Boolean flagForAllBdc;

	@Column(name = "FLAG_FOR_ALL_BDC_USER", nullable = false)
	/*@CsvBindByPosition(position = 5)
	@CsvBindByName*/
	private Boolean flagForAllBdcUser;

	@Column(name = "CREATOR", nullable = false)
	/*@CsvBindByPosition(position = 2)
	@CsvBindByName*/
	private String creator;

	@Column(name = "INSERT_DATE", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date insertDate;

	@Column(name = "VALID_FROM", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date validFrom;

	@Column(name = "VALID_TO", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date validTo;

	@Column(name = "ACTIVE", nullable = false)
	/*@CsvBindByPosition(position = 5)
	@CsvBindByName*/
	private Boolean flagActive;

	@Column(name = "DEACTIVATION_DATE")
	private Date deactivationDate;

	@Transient
	@CsvBindByPosition(position = 2)
	@CsvBindByName
	private String insertDateString;

	@Transient
	@CsvBindByPosition(position = 3)
	@CsvBindByName
	private String validFromString;

	@Transient
	@CsvBindByPosition(position = 4)
	@CsvBindByName
	private String validToString;

	@Transient
    @CsvBindByPosition(position = 5)
    @CsvBindByName
    private String flagActiveToString;

	@Transient
	/*@CsvBindByPosition(position = 10)
	@CsvBindByName*/
	private String deactivationDateString;

	public BdcNews() {
		
	}

	public BdcNews(String title, String news, BdcBroadcasters bdcBroadcasters, Integer idUtente, Boolean flagForAllBdc,
			Boolean flagForAllBdcUser, String creator, Date insertDate, Date validFrom, Date validTo,
			Boolean flagActive, Date deactivationDate) {
		super();
		this.title = title;
		this.news = news;
		this.bdcBroadcasters = bdcBroadcasters;
		this.idUtente = idUtente;
		this.flagForAllBdc = flagForAllBdc;
		this.flagForAllBdcUser = flagForAllBdcUser;
		this.creator = creator;
		this.insertDate = insertDate;
		this.validFrom = validFrom;
		this.validTo = validTo;
		this.flagActive = flagActive;
		this.deactivationDate = deactivationDate;
	}

	public BdcNews(Integer id, String title, String news, BdcBroadcasters bdcBroadcasters, Integer idUtente,
			Boolean flagForAllBdc, Boolean flagForAllBdcUser, String creator, Date insertDate, Date validFrom,
			Date validTo, Boolean flagActive, Date deactivationDate) {
		super();
		this.id = id;
		this.title = title;
		this.news = news;
		this.bdcBroadcasters = bdcBroadcasters;
		this.idUtente = idUtente;
		this.flagForAllBdc = flagForAllBdc;
		this.flagForAllBdcUser = flagForAllBdcUser;
		this.creator = creator;
		this.insertDate = insertDate;
		this.validFrom = validFrom;
		this.validTo = validTo;
		this.flagActive = flagActive;
		this.deactivationDate = deactivationDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getNews() {
		return news;
	}

	public void setNews(String news) {
		this.news = news;
	}

	public BdcBroadcasters getBdcBroadcasters() {
		return bdcBroadcasters;
	}

	public void setBdcBroadcasters(BdcBroadcasters bdcBroadcasters) {
		this.bdcBroadcasters = bdcBroadcasters;
	}

	public Integer getIdUtente() {
		return idUtente;
	}

	public void setIdUtente(Integer idUtente) {
		this.idUtente = idUtente;
	}

	public Boolean getFlagForAllBdc() {
		return flagForAllBdc;
	}

	public void setFlagForAllBdc(Boolean flagForAllBdc) {
		this.flagForAllBdc = flagForAllBdc;
	}

	public Boolean getFlagForAllBdcUser() {
		return flagForAllBdcUser;
	}

	public void setFlagForAllBdcUser(Boolean flagForAllBdcUser) {
		this.flagForAllBdcUser = flagForAllBdcUser;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public Date getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(Date insertDate) {
		this.insertDate = insertDate;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public Boolean getFlagActive() {
		return flagActive;
	}

	public void setFlagActive(Boolean flagActive) {
		this.flagActive = flagActive;
	}

	public Date getDeactivationDate() {
		return deactivationDate;
	}

	public void setDeactivationDate(Date deactivationDate) {
		this.deactivationDate = deactivationDate;
	}

	public String getInsertDateString() {
		return insertDateString;
	}

	public void setInsertDateString(String insertDateString) {
		this.insertDateString = insertDateString;
	}

	public String getValidFromString() {
		return validFromString;
	}

	public void setValidFromString(String validFromString) {
		this.validFromString = validFromString;
	}

	public String getValidToString() {
		return validToString;
	}

	public void setValidToString(String validToString) {
		this.validToString = validToString;
	}

	public String getDeactivationDateString() {
		return deactivationDateString;
	}

	public void setDeactivationDateString(String deactivationDateString) {
		this.deactivationDateString = deactivationDateString;
	}

    public String getFlagActiveToString() {
        return flagActiveToString;
    }

    public void setFlagActiveToString(String flagActiveToString) {
        this.flagActiveToString = flagActiveToString;
    }

    public void setTransitionalValue() {
		SimpleDateFormat sdf= new SimpleDateFormat("dd/MM/yyyy hh:mm");
		try {
			insertDateString=sdf.format(insertDate);
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			validFromString=sdf.format(validFrom);
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			validToString=sdf.format(validTo);
		} catch (Exception e) {

		}
		try {
			deactivationDateString=sdf.format(deactivationDate);
		} catch (Exception e) {
			// TODO: handle exception
		}try {
		    if (flagActive){
		        flagActiveToString = "Attiva";
            }else{
		        flagActiveToString = "Disattivata";
            }
        }catch (Exception e){

        }
	}
	public String[] getMappingStrategy() {
		return new String[] { "Titolo", "Testo",
//                "Broadcaster", "Id Utente", "Flag tutti i broadcaster", "Flag tutti gli utenti",
//                "Utente ultima modifica",
                "Data di inserimento", "Valido da", "Valido a",
//				"Data di disattivazione", "Attivo"
                "Stato"
		};
	}

}
