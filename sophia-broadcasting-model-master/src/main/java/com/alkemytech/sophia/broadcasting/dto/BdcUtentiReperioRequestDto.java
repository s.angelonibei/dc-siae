package com.alkemytech.sophia.broadcasting.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.broadcasting.model.BdcBroadcasters;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BdcUtentiReperioRequestDto {

	private BdcBroadcasters bdcBroadcasters;
	private String repertori;

	public BdcUtentiReperioRequestDto() {
		super();
	}

	public BdcUtentiReperioRequestDto(BdcBroadcasters bdcBroadcasters, String repertori) {
		super();
		this.bdcBroadcasters = bdcBroadcasters;
		this.repertori = repertori;
	}

	public BdcBroadcasters getBdcBroadcasters() {
		return bdcBroadcasters;
	}

	public void setBdcBroadcasters(BdcBroadcasters bdcBroadcasters) {
		this.bdcBroadcasters = bdcBroadcasters;
	}

	public String getRepertori() {
		return repertori;
	}

	public void setRepertori(String repertori) {
		this.repertori = repertori;
	}

}
