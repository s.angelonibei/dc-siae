package com.alkemytech.sophia.broadcasting.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Alessandro Russo on 09/12/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "BDC_SHOW_TYPE")
@NamedQuery(name="ShowType.findAll", query="SELECT s FROM ShowType s")
public class ShowType implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_SHOW_TYPE", nullable = false)
    public Long id;

    @Column(name = "NAME")
    public String name;

    @Column(name = "CATEGORY")
    public String category;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public boolean isPubblicita(){
        return name.contains("PUBBL");
    }

//    public boolean isShow(){
//        return name.contains("SHOW");
//    }
//
//    public boolean isTelefilm(){
//        return name.contains("TELE");
//    }
//
//    public boolean isFilm(){
//        return (!isPubblicita() && !isShow() && !isTelefilm());
//    }

    @Override
    public String toString() {
        return "ShowType{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", category='" + category + '\'' +
                '}';
    }
}
