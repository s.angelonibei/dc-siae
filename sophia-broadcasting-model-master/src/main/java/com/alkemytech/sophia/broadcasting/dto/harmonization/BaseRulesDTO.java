package com.alkemytech.sophia.broadcasting.dto.harmonization;

/**
 * Created by Alessandro Russo on 09/12/2017.
 */
public class BaseRulesDTO {

    private String regola1;
    private String regola2;
    private String regola3;

    public String getRegola1() {
        return regola1;
    }

    public void setRegola1(String regola1) {
        this.regola1 = regola1;
    }

    public String getRegola2() {
        return regola2;
    }

    public void setRegola2(String regola2) {
        this.regola2 = regola2;
    }

    public String getRegola3() {
        return regola3;
    }

    public void setRegola3(String regola3) {
        this.regola3 = regola3;
    }
}
