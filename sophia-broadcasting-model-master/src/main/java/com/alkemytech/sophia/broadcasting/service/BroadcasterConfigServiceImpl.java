package com.alkemytech.sophia.broadcasting.service;

import com.alkemytech.sophia.broadcasting.dto.BdcConfigDTO;
import com.alkemytech.sophia.broadcasting.dto.SearchBroadcasterConfigDTO;
import com.alkemytech.sophia.broadcasting.model.BdcBroadcasterConfig;
import com.alkemytech.sophia.broadcasting.model.BdcBroadcasterConfigTemplate;
import com.alkemytech.sophia.broadcasting.service.interfaces.BroadcasterConfigService;
import com.alkemytech.sophia.broadcasting.utils.Utilities;
import com.amazonaws.util.CollectionUtils;
import com.google.gson.Gson;
import com.google.inject.Provider;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Created by Alessandro Russo on 27/11/2017.
 */
public class BroadcasterConfigServiceImpl implements BroadcasterConfigService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final Provider<EntityManager> provider;

    public BroadcasterConfigServiceImpl(Provider<EntityManager> provider) {
        this.provider = provider;
    }

    @Override
    public BdcBroadcasterConfig findConfig(Integer id) {
        EntityManager em = provider.get();
        final Query q = em.createQuery("SELECT x FROM BdcBroadcasterConfig x WHERE x.id = :id");
        q.setParameter("id", id);
        return (BdcBroadcasterConfig) q.getSingleResult();
    }

    @Override
    public List<BdcBroadcasterConfig> findAll(){
        EntityManager em = provider.get();
        final Query q = em.createQuery("SELECT x FROM BdcBroadcasterConfig x");
        return  (List<BdcBroadcasterConfig>) q.getResultList();
    }



    @Override
    public List<BdcConfigDTO> findAllActive(SearchBroadcasterConfigDTO dto) {
        List<BdcConfigDTO> ret = new ArrayList<>();
        List<BdcBroadcasterConfig> configList = retrieveConfigurations( dto );
        if( configList != null && configList.size() > 0){
            for( BdcBroadcasterConfig config : configList ){
                if( StringUtils.isNotEmpty(config.getConfiguration().getConfigurazione()) ){
                    ret.add( fillConfigDTO(config.getConfiguration().getConfigurazione() ) );
                }
            }
        }
        return ret;
    }



    @Override
    public BdcConfigDTO findActive(SearchBroadcasterConfigDTO dto) {
        BdcConfigDTO ret = null;
        List<BdcBroadcasterConfig> configList = retrieveConfigurations( dto );
        if( !CollectionUtils.isNullOrEmpty(configList) ){
            for( BdcBroadcasterConfig currentConfig : configList ){
                if (  StringUtils.isNotEmpty( currentConfig.getConfiguration().getConfigurazione() ) ){
                    if(currentConfig.getIdChannel() != null ){
                        if(currentConfig.getIdChannel().equals(dto.getIdChannel().longValue())){
                            ret = fillConfigDTO(currentConfig.getConfiguration().getConfigurazione());
                            break;
                        }
                    }else{
                        ret = fillConfigDTO(currentConfig.getConfiguration().getConfigurazione());
                    }
                }
            }
        }
        return ret;
    }

    @Override
    public Integer updateConfig(Integer idConfig, Date validFrom, Date validTo, String modifyUser, BdcBroadcasterConfigTemplate idConfiguration) {
        BdcBroadcasterConfig bdcBroadcasterConfig = findConfig(idConfig);
        EntityManager entityManager = provider.get();
        String query="";
        if (bdcBroadcasterConfig.getIdChannel() == null) {
    			query="SELECT x FROM BdcBroadcasterConfig x where x.idBroadcaster=:idBroadcaster AND (x.idChannel IS NULL) ";
    		}else {
    			query="SELECT x FROM BdcBroadcasterConfig x where x.idBroadcaster=:idBroadcaster AND (x.idChannel=:idChannel ) ";
    		}
        final Query q = entityManager.createQuery(query);
        q.setParameter("idBroadcaster", bdcBroadcasterConfig.getIdBroadcaster());
        if (bdcBroadcasterConfig.getIdChannel() != null) {
        		q.setParameter("idChannel", bdcBroadcasterConfig.getIdChannel());
        }
        List<BdcBroadcasterConfig> configs= q.getResultList();
        if (validTo!=null && (validTo.before(validFrom)||validTo.equals(validFrom))) {
            return 502;
        }
        for(BdcBroadcasterConfig config:configs) {
            if (config.getId()!=bdcBroadcasterConfig.getId()) {
                if (validFrom.before(config.getValidFrom())&&(validTo!=null&&validTo.before(config.getValidFrom()))) {
                    //BUONA
                }else if (config.getValidTo() !=null && validFrom.after(config.getValidTo() )){
                    //BUONA
                }else {
                    //CATTIVA
                    return 501;
                }
            }
        }
        try {
            if (validFrom != null) {
                bdcBroadcasterConfig.setValidFrom(validFrom);
            }
            bdcBroadcasterConfig.setValidTo(validTo);
            bdcBroadcasterConfig.setModifyUser(modifyUser);
            if (idConfiguration != null) {
                bdcBroadcasterConfig.setConfiguration(idConfiguration);
            }
            bdcBroadcasterConfig.setModifyDate(new Date());
            entityManager.getTransaction().begin();
            entityManager.persist(bdcBroadcasterConfig);
            entityManager.getTransaction().commit();
            return 200;
        } catch (Exception e){
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            return 500;
        }
    }

    @Override
    public Integer createConfig(Integer idBroadcaster, Integer idCanale, Date validFrom, Date validTo, BdcBroadcasterConfigTemplate idConfiguration,String modifyUser) {
        BdcBroadcasterConfig bdcBroadcasterConfig = new BdcBroadcasterConfig();
        EntityManager entityManager = provider.get();
        String query="";
        if (idCanale == null) {
			query="SELECT x FROM BdcBroadcasterConfig x where x.idBroadcaster=:idBroadcaster AND (x.idChannel IS NULL) ";
		}else {
			query="SELECT x FROM BdcBroadcasterConfig x where x.idBroadcaster=:idBroadcaster AND (x.idChannel=:idChannel ) ";
		}
        
        final Query q = entityManager.createQuery(query);
        q.setParameter("idBroadcaster", idBroadcaster);
        if (idCanale != null) {
        		q.setParameter("idChannel", idCanale);
        }
        List<BdcBroadcasterConfig> configs= q.getResultList();
        if (validTo!=null && (validTo.before(validFrom)||validTo.equals(validFrom))) {
            return 502;
        }
        for(BdcBroadcasterConfig config:configs) {
            if (validFrom.before(config.getValidFrom())&&(validTo!=null&&validTo.before(config.getValidFrom()))) {
                //BUONA
            }else if (config.getValidTo() !=null && validFrom.after(config.getValidTo() )){
                //BUONA
            }else {
                //CATTIVA
                return 501;
            }
        }

        try {
            if (idBroadcaster != null) {
                bdcBroadcasterConfig.setIdBroadcaster(idBroadcaster.longValue());
            }
            if (idCanale != null) {
                bdcBroadcasterConfig.setIdChannel(idCanale.longValue());
            }
            if (validFrom != null) {
                bdcBroadcasterConfig.setValidFrom(validFrom);
            }
            if (validTo != null) {
                bdcBroadcasterConfig.setValidTo(validTo);
            }
            if (idConfiguration != null) {
                bdcBroadcasterConfig.setConfiguration(idConfiguration);
            }
            if (modifyUser != null) {
                bdcBroadcasterConfig.setCreationUser(modifyUser);
                bdcBroadcasterConfig.setModifyUser(modifyUser);
            }
            bdcBroadcasterConfig.setCreationDate(new Date());
            bdcBroadcasterConfig.setModifyDate(new Date());
            entityManager.getTransaction().begin();
            entityManager.persist(bdcBroadcasterConfig);
            entityManager.getTransaction().commit();
            return 200;
        } catch (Exception e){
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            return 500;
        }
    }


    @Override
    public List<BdcBroadcasterConfigTemplate> getBdcBroadcastingTemplate(String tipoEmittente) {
        EntityManager em = provider.get();
        final Query q = em.createQuery("SELECT a FROM BdcBroadcasterConfigTemplate a where a.tipoEmittente =:tipoEmittente AND a.flagAttivo=1 ORDER BY a.flagDefault DESC ");
        q.setParameter("tipoEmittente", tipoEmittente);
        return (List<BdcBroadcasterConfigTemplate>) q.getResultList();
    }

    private BdcConfigDTO fillConfigDTO(String configJson) {
        Gson gson = new Gson();
        BdcConfigDTO current = gson.fromJson( configJson, BdcConfigDTO.class);
        return current;
    }

    private List<BdcBroadcasterConfig> retrieveConfigurations( SearchBroadcasterConfigDTO dto ){

        EntityManager em = provider.get();
        Date activeDate = StringUtils.isEmpty(dto.getActiveDate()) ? new Date() : Utilities.stringToDate(dto.getActiveDate(), Utilities.DATETIME_MYSQL_FORMAT);
        StringBuilder queryString = new StringBuilder("SELECT x FROM BdcBroadcasterConfig x WHERE ((:activeDate BETWEEN x.validFrom AND x.validTo) or (:activeDate >= x.validFrom and x.validTo is null))" );
        if(dto.getIdBroadcaster() != null ){
            queryString.append(" AND x.idBroadcaster = :idBroadcaster");
        }
        final Query q = em.createQuery(queryString.toString());
        q.setParameter("activeDate", activeDate);
        if(dto.getIdBroadcaster() != null ){
            q.setParameter("idBroadcaster", dto.getIdBroadcaster());
        }
        return  (List<BdcBroadcasterConfig>) q.getResultList();
    }

    @Override
    public List<BdcBroadcasterConfig> findBroadcasterActivConfiguration(Integer idEmittente) {
        EntityManager em = provider.get();
        final Query q = em.createQuery("SELECT c.nome, x FROM BdcBroadcasterConfig x LEFT JOIN BdcCanali c on c.id = x.idChannel where x.idBroadcaster=:idBroadcaster AND (x.validFrom <=:now AND (x.validTo>=:now OR x.validTo IS NULL))");
        q.setParameter("idBroadcaster", idEmittente);
        q.setParameter("now", new Date());
        return  (List<BdcBroadcasterConfig>) q.getResultList();
    }

    @Override
    public Integer deleteConfig(Integer id, String userModifier) {
        EntityManager entityManager = provider.get();
        BdcBroadcasterConfig bdcBroadcasterConfig = findConfig(id);
        Date del = new Date();
        try {
            if (bdcBroadcasterConfig != null) {
                bdcBroadcasterConfig.setValidTo(del);
                bdcBroadcasterConfig.setModifyUser(userModifier);
                bdcBroadcasterConfig.setModifyDate(del);
            }
            entityManager.getTransaction().begin();
            entityManager.persist(bdcBroadcasterConfig);
            entityManager.getTransaction().commit();
            return 200;
        } catch (Exception e){
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            return 500;
        }
    }
}
