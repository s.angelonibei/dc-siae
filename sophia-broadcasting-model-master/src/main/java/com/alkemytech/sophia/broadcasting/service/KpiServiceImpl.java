package com.alkemytech.sophia.broadcasting.service;

import com.alkemytech.sophia.broadcasting.data.IndicatoriPerCanale;
import com.alkemytech.sophia.broadcasting.dto.IndicatoreDTO;
import com.alkemytech.sophia.broadcasting.dto.KPI;
import com.alkemytech.sophia.broadcasting.model.BdcKpiEntity;
import com.alkemytech.sophia.broadcasting.model.TipoBroadcaster;
import com.alkemytech.sophia.broadcasting.service.interfaces.BdcIndicatoreDTOService;
import com.alkemytech.sophia.broadcasting.service.interfaces.KpiService;
import com.google.inject.Inject;
import com.google.inject.Provider;

import javax.persistence.EntityManager;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

public class KpiServiceImpl implements KpiService {

    private Provider<EntityManager> provider;

    @Inject
    private BdcIndicatoreDTOService indicatoreDTOService;

    public KpiServiceImpl(BdcIndicatoreDTOService indicatoreDTOService) {
        this.indicatoreDTOService = indicatoreDTOService;
    }

    @Override
    public KPI calcolaKpiRecordScartati(HashMap<String, IndicatoreDTO> mapIndicatori) {
        KPI kpi = new KPI(true, KPI.KPI_RECORD_ACCETTATI, KPI.DESC_KPI_RECORD_ACCETTATI, BigDecimal.valueOf(0));

        long recordScartati = 0;
        long recordValidi = 0;

        if (mapIndicatori.containsKey(BdcKpiEntity.RECORD_TOTALI)) {
            recordValidi = mapIndicatori.get(BdcKpiEntity.RECORD_TOTALI).getValore();
        }

        if (mapIndicatori.containsKey(BdcKpiEntity.RECORD_SCARTATI)) {
            recordScartati = mapIndicatori.get(BdcKpiEntity.RECORD_SCARTATI).getValore();
        }

        if(recordValidi + recordScartati != 0){
            BigDecimal valoreKPI = BigDecimal.valueOf(100).subtract(BigDecimal.valueOf(100 * recordScartati).divide(BigDecimal.valueOf(recordValidi + recordScartati), 2, RoundingMode.HALF_UP));
            kpi.setValore(valoreKPI);
        }

        return kpi;
    }

    @Override
    public KPI calcolaKpiBraniTitoliErronei(HashMap<String, IndicatoreDTO> mapIndicatori) {
        KPI kpi = new KPI(true, KPI.KPI_BRANI_TITOLI_ERRONEI, KPI.DESC_KPI_BRANI_TITOLI_ERRONEI, BigDecimal.valueOf(0));
        long durataBraniTitoliErronei = 0;
        long durataBraniTotaliDichiarati = 0;

        if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_BRANI)) {
            durataBraniTotaliDichiarati = mapIndicatori.get(BdcKpiEntity.DURATA_BRANI).getValore();
        }
        if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_BRANI_TITOLI_ERRONEI)) {
            durataBraniTitoliErronei = mapIndicatori.get(BdcKpiEntity.DURATA_BRANI_TITOLI_ERRONEI).getValore();
        }

        if(durataBraniTotaliDichiarati != 0){
            BigDecimal valoreKPI = BigDecimal.valueOf(100 * durataBraniTitoliErronei).divide(BigDecimal.valueOf(durataBraniTotaliDichiarati), 2, RoundingMode.HALF_UP);
            kpi.setValore(valoreKPI);
        }
        return kpi;
    }

    @Override
    public KPI calcolaKpiAutoriTitoliErronei(HashMap<String, IndicatoreDTO> mapIndicatori) {
        KPI kpi = new KPI(true, KPI.KPI_AUTORI_TITOLI_ERRONEI, KPI.DESC_KPI_AUTORI_TITOLI_ERRONEI, BigDecimal.valueOf(0));
        long durataBraniRdAutoriTitoliErronei = 0;
        long durataBraniDichiarati = 0;

        if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_BRANI_AUTORI_ERRONEI)) {
            durataBraniRdAutoriTitoliErronei = mapIndicatori.get(BdcKpiEntity.DURATA_BRANI_AUTORI_ERRONEI).getValore();
        }
        if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_BRANI)) {
            durataBraniDichiarati = mapIndicatori.get(BdcKpiEntity.DURATA_BRANI).getValore();
        }

        if(durataBraniDichiarati != 0){
            BigDecimal valoreKPI = BigDecimal.valueOf(100 * durataBraniRdAutoriTitoliErronei).divide(BigDecimal.valueOf(durataBraniDichiarati), 2, RoundingMode.HALF_UP);
            kpi.setValore(valoreKPI);
        }
        return kpi;
    }

    @Override
    public KPI calcolaKpiFilmTitoliErronei(HashMap<String, IndicatoreDTO> mapIndicatori) {
        KPI kpi = new KPI(true, KPI.KPI_FILM_TITOLI_ERRONEI, KPI.DESC_KPI_FILM_TITOLI_ERRONEI, BigDecimal.valueOf(0));
        long durataFilmTitoliErronei = 0;
        long durataFilm = 0;

        if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_FILM)) {
            durataFilm = mapIndicatori.get(BdcKpiEntity.DURATA_FILM).getValore();
        }
        if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_FILM_TITOLI_ERRONEI)) {
            durataFilmTitoliErronei = mapIndicatori.get(BdcKpiEntity.DURATA_FILM_TITOLI_ERRONEI).getValore();
        }

        if(durataFilm != 0){
            BigDecimal valoreKPI = BigDecimal.valueOf(100 * durataFilmTitoliErronei).divide(BigDecimal.valueOf(durataFilm), 2, RoundingMode.HALF_UP);
            kpi.setValore(valoreKPI);
        }

        return kpi;
    }

    @Override
    public KPI calcolaKpiProgrammiTitoliErronei(HashMap<String, IndicatoreDTO> mapIndicatori) {
        KPI kpi = new KPI(true, KPI.KPI_PROGRAMMI_TITOLI_ERRONEI, KPI.DESC_KPI_PROGRAMMI_TITOLI_ERRONEI, BigDecimal.valueOf(0));
        long durataTrasmissioniTitoliErronei = 0;
        long durataTrasmissioni = 0;
        long durataGeneriVuoti = 0;


        if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_TRASMISSIONE)) {
            durataTrasmissioni = mapIndicatori.get(BdcKpiEntity.DURATA_TRASMISSIONE).getValore();
        }
        if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_PROGRAMMI_TITOLI_ERRONEI)) {
            durataTrasmissioniTitoliErronei = mapIndicatori.get(BdcKpiEntity.DURATA_PROGRAMMI_TITOLI_ERRONEI).getValore();
        }
        if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_GENERI_VUOTI)) {
            durataGeneriVuoti = mapIndicatori.get(BdcKpiEntity.DURATA_GENERI_VUOTI).getValore();
        }

        if(durataTrasmissioni != 0){
            BigDecimal valoreKPI = BigDecimal.valueOf(100 * durataTrasmissioniTitoliErronei).divide(BigDecimal.valueOf(durataTrasmissioni+durataGeneriVuoti), 2, RoundingMode.HALF_UP);
            kpi.setValore(valoreKPI);
        }

        return kpi;
    }

    @Override
    public KPI calcolaKpiMusicaEccedenteTrasmissione(HashMap<String, IndicatoreDTO> mapIndicatori) {
        KPI kpi = new KPI(true, KPI.KPI_TRASMISSIONI_MUSICHE_ECCEDENTI, KPI.DESC_KPI_TRASMISSIONI_MUSICHE_ECCEDENTI, BigDecimal.valueOf(0));
        long trasmissioniEccedenti = 0;
        long trasmissioniValide = 0;

        if (mapIndicatori.containsKey(BdcKpiEntity.TRASMISSIONI_MUSICA_ECCEDENTE)) {
            trasmissioniEccedenti = mapIndicatori.get(BdcKpiEntity.TRASMISSIONI_MUSICA_ECCEDENTE).getValore();
        }
        if (mapIndicatori.containsKey(BdcKpiEntity.TRASMISSIONI_TOTALI)) {
            trasmissioniValide = mapIndicatori.get(BdcKpiEntity.TRASMISSIONI_TOTALI).getValore();
        }

        if(trasmissioniValide != 0){
            BigDecimal valoreKPI = BigDecimal.valueOf(100 * trasmissioniEccedenti).divide(BigDecimal.valueOf(trasmissioniValide), 2, RoundingMode.HALF_UP);
            kpi.setValore(valoreKPI);
        }

        return kpi;
    }

    @Override
    public KPI calcolaKpiDurataMusicaDichiarata(HashMap<String, IndicatoreDTO> mapIndicatori) {
        KPI kpi = new KPI(true, KPI.KPI_DURATA_MUSICA_DICHIARATA, KPI.DESC_KPI_DURATA_MUSICA_DICHIARATA, BigDecimal.valueOf(0));
        long durataBrani = 0;
        long durataFilm = 0;
        long durataPubblicita = 0;
        long durataTrasmissioni = 0;
        long durataAudiovisiviContenuti = 0;
        long durataGeneriVuoti = 0;

        if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_BRANI)) {
            durataBrani = mapIndicatori.get(BdcKpiEntity.DURATA_BRANI).getValore();
        }
        if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_FILM)) {
            durataFilm = mapIndicatori.get(BdcKpiEntity.DURATA_FILM).getValore();
        }
        if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_TRASMISSIONE)) {
            durataTrasmissioni = mapIndicatori.get(BdcKpiEntity.DURATA_TRASMISSIONE).getValore();
        }
        if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_PUBBLICITA)) {
            durataPubblicita = mapIndicatori.get(BdcKpiEntity.DURATA_PUBBLICITA).getValore();
        }
        if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_AUDIOVISIVI_CONTENUTI)) {
            durataAudiovisiviContenuti = mapIndicatori.get(BdcKpiEntity.DURATA_AUDIOVISIVI_CONTENUTI).getValore();
        }
        if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_GENERI_VUOTI)) {
            durataGeneriVuoti = mapIndicatori.get(BdcKpiEntity.DURATA_GENERI_VUOTI).getValore();
        }
        if((durataTrasmissioni + durataFilm + durataPubblicita + durataAudiovisiviContenuti) != 0){
            BigDecimal valoreKPI = (BigDecimal.valueOf(100 * (durataBrani + (durataFilm * 0.6) + (durataAudiovisiviContenuti * 0.6))).divide(BigDecimal.valueOf(durataTrasmissioni + durataFilm + durataPubblicita +durataGeneriVuoti), 2, RoundingMode.HALF_UP));
            kpi.setValore(valoreKPI);
        }

        return kpi;
    }

    @Override
    public KPI calcolaKpiRecordInRitardo(HashMap<String, IndicatoreDTO> mapIndicatori) {
        KPI kpi = new KPI(true, KPI.KPI_RECORD_IN_TEMPO, KPI.DESC_KPI_RECORD_IN_TEMPO, BigDecimal.valueOf(0));
        long recordInRitardo = 0;
        long recordTotali = 0;
        if (mapIndicatori.containsKey(BdcKpiEntity.RECORD_TOTALI)) {
            recordTotali = mapIndicatori.get(BdcKpiEntity.RECORD_TOTALI).getValore();
        }
        if (mapIndicatori.containsKey(BdcKpiEntity.RECORD_IN_RITARDO)) {
            recordInRitardo = mapIndicatori.get(BdcKpiEntity.RECORD_IN_RITARDO).getValore();
        }

        if(recordTotali != 0){
            BigDecimal valoreKPI = BigDecimal.valueOf(100).subtract(BigDecimal.valueOf(100 * recordInRitardo).divide(BigDecimal.valueOf(recordTotali), 2, RoundingMode.HALF_UP));
            kpi.setValore(valoreKPI);
        }

        return kpi;
    }


    @Override
    public KPI calcolaKpiDurataBraniDichiaratiRD(long durataMusiche,Integer anno, List<Short> mesi){
        long durataDisponibile = indicatoreDTOService.calcolaDurataDisponibile( anno, mesi);
        return calcolaKpiDurataBraniDichiaratiRD(durataMusiche,  durataDisponibile);
    }

    @Override
    public KPI calcolaKpiDurataBraniDichiaratiRD(long durataMusiche, Date dataDa, Date dataA){
        long durataDisponibile = indicatoreDTOService.calcolaDurataDisponibile(dataDa, dataA);
        return calcolaKpiDurataBraniDichiaratiRD(durataMusiche,  durataDisponibile);
    }


    private KPI calcolaKpiDurataBraniDichiaratiRD(long durataMusiche, long durataDisponibile) {
        KPI kpi = new KPI(true, KPI.KPI_DURATA_BRANI_DICHIARATI, KPI.DESC_KPI_DURATA_BRANI_DICHIARATI, BigDecimal.valueOf(0));
        if(durataDisponibile != 0){
            BigDecimal valoreKPI = BigDecimal.valueOf(100 * durataMusiche).divide(BigDecimal.valueOf(durataDisponibile), 2, RoundingMode.HALF_UP);
            kpi.setValore(valoreKPI);
        }
        return kpi;
    }


    @Override
    public KPI calcolaKpiBraniTitoliErroneiRD(HashMap<String, IndicatoreDTO> mapIndicatori) {
        KPI kpi = new KPI(true, KPI.KPI_BRANI_RD_TITOLI_ERRONEI, KPI.DESC_KPI_BRANI_RD_TITOLI_ERRONEI, BigDecimal.valueOf(0));
        long durataBraniRdTitoliErronei = 0;
        long durataBraniDichiarati = 0;

        if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_BRANI_TITOLI_ERRONEI)) {
            durataBraniRdTitoliErronei = mapIndicatori.get(BdcKpiEntity.DURATA_BRANI_TITOLI_ERRONEI).getValore();
        }
        if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_BRANI)) {
            durataBraniDichiarati = mapIndicatori.get(BdcKpiEntity.DURATA_BRANI).getValore();
        }

        if(durataBraniDichiarati != 0){
            BigDecimal valoreKPI = BigDecimal.valueOf(100 * durataBraniRdTitoliErronei).divide(BigDecimal.valueOf(durataBraniDichiarati), 2, RoundingMode.HALF_UP);
            kpi.setValore(valoreKPI);
        }
        return kpi;
    }

    @Override
    public KPI calcolaKpiAutoriTitoliErroneiRD(HashMap<String, IndicatoreDTO> mapIndicatori) {
        KPI kpi = new KPI(true, KPI.KPI_AUTORI_RD_TITOLI_ERRONEI, KPI.DESC_KPI_AUTORI_RD_TITOLI_ERRONEI, BigDecimal.valueOf(0));
        long durataBraniRdAutoriTitoliErronei = 0;
        long durataBraniDichiarati = 0;

        if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_BRANI_AUTORI_ERRONEI)) {
            durataBraniRdAutoriTitoliErronei = mapIndicatori.get(BdcKpiEntity.DURATA_BRANI_AUTORI_ERRONEI).getValore();
        }
        if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_BRANI)) {
            durataBraniDichiarati = mapIndicatori.get(BdcKpiEntity.DURATA_BRANI).getValore();
        }

        if(durataBraniDichiarati != 0){
            BigDecimal valoreKPI = BigDecimal.valueOf(100 * durataBraniRdAutoriTitoliErronei).divide(BigDecimal.valueOf(durataBraniDichiarati), 2, RoundingMode.HALF_UP);
            kpi.setValore(valoreKPI);
        }
        return kpi;
    }

    public KPI calcolaKpiCopertura(long durataFilm, long durataPubblicita,
                                   long durataTrasmissioni,Integer anno, List<Short> mesi)
    {
        long durataDisponibile = indicatoreDTOService.calcolaDurataDisponibile( anno, mesi);
        return calcolaKpiCopertura(durataFilm, durataPubblicita, durataTrasmissioni, durataDisponibile);

    }

    public KPI calcolaKpiCopertura(long durataFilm, long durataGeneriVuoti, long durataPubblicita,
                                   long durataTrasmissioni,Integer anno, List<Short> mesi)
    {
        long durataDisponibile = indicatoreDTOService.calcolaDurataDisponibile( anno, mesi);
        return calcolaKpiCopertura(durataFilm, durataGeneriVuoti, durataPubblicita, durataTrasmissioni, durataDisponibile);

    }

    public KPI calcolaKpiCopertura(long durataFilm, long durataGeneriVuoti, long durataPubblicita,
                                   long durataTrasmissioni, Date dataDa, Date dataA)
    {
        long durataDisponibile = indicatoreDTOService.calcolaDurataDisponibile(dataDa, dataA);
        return calcolaKpiCopertura(durataFilm, durataGeneriVuoti, durataPubblicita, durataTrasmissioni, durataDisponibile);
    }


    private KPI calcolaKpiCopertura(long durataFilm, long durataGeneriVuoti, long durataPubblicita,
                                    long durataTrasmissioni, long durataDisponibile)
    {
        KPI kpi = new KPI(true, KPI.KPI_COPERTURA, KPI.DESC_KPI_COPERTURA, BigDecimal.valueOf(0));
            if(durataDisponibile != 0){
                BigDecimal valoreKPI = BigDecimal.valueOf(100 * (durataGeneriVuoti + durataFilm + durataPubblicita + durataTrasmissioni)).divide(BigDecimal.valueOf(durataDisponibile), 2, RoundingMode.HALF_UP);
                kpi.setValore(valoreKPI);
            }
        return kpi;
    }

    private KPI calcolaKpiCopertura(long durataFilm, long durataPubblicita,
                                    long durataTrasmissioni, long durataDisponibile)
    {
        KPI kpi = new KPI(true, KPI.KPI_COPERTURA, KPI.DESC_KPI_COPERTURA, BigDecimal.valueOf(0));
        if(durataDisponibile != 0){
            BigDecimal valoreKPI = BigDecimal.valueOf(100 * (durataFilm + durataPubblicita + durataTrasmissioni)).divide(BigDecimal.valueOf(durataDisponibile), 2, RoundingMode.HALF_UP);
            kpi.setValore(valoreKPI);
        }
        return kpi;
    }

	@Override
	public HashMap<Integer, List<KPI>> findKpiRelativi(Date dateFrom, Date dateTo, TipoBroadcaster tipoBroadcaster) {
        HashMap<Integer, List<KPI>> retKPIMap = new HashMap<Integer, List<KPI>>();

        if(dateFrom != null && dateTo != null && tipoBroadcaster != null ){

            Calendar calendar = new GregorianCalendar();
            calendar.setTime(dateFrom);
            Integer annoDa = calendar.get(Calendar.YEAR);
            Integer meseDa = calendar.get(Calendar.MONTH) + 1;

            calendar.setTime(dateTo);
            Integer annoA = calendar.get(Calendar.YEAR);
            Integer meseA = calendar.get(Calendar.MONTH) + 1;

            HashMap<Integer, Map<String,Long>> mapCanaleIndicatore = indicatoreDTOService.mapCanaleIndicatore(
                    annoDa,meseDa,annoA,meseA,tipoBroadcaster);

            for(Integer idCanale : mapCanaleIndicatore.keySet()){

                Map<String,Long> mapIndicatori = mapCanaleIndicatore.get(idCanale);

                List<KPI> kpiList = calcolaListaKpi(tipoBroadcaster, mapIndicatori, dateFrom, dateTo);
                retKPIMap.put(idCanale, kpiList);
            }

        }

		return retKPIMap;
	}

    private List<KPI> calcolaListaKpi(TipoBroadcaster tipoBroadcaster, Map<String, Long> mapIndicatori,
                                      Date dateFrom, Date dateTo)
    {

        List<KPI> listaKPI = new ArrayList<KPI>();

        HashMap<String, IndicatoreDTO> mapIndicatoriDTO = convertiMapIndicatori(mapIndicatori);

        if(TipoBroadcaster.TELEVISIONE.equals(tipoBroadcaster)){
            long durataFilm = 0;
            long durataGeneriVuoti =0;
            long durataPubblicita = 0;
            long durataTrasmissioni = 0;
            if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_FILM)) {
                durataFilm = mapIndicatori.get(BdcKpiEntity.DURATA_FILM);
            }
            if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_PUBBLICITA)) {
                durataPubblicita = mapIndicatori.get(BdcKpiEntity.DURATA_PUBBLICITA);
            }
            if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_TRASMISSIONE)) {
                durataTrasmissioni = mapIndicatori.get(BdcKpiEntity.DURATA_TRASMISSIONE);
            }
            if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_GENERI_VUOTI)) {
                durataGeneriVuoti = mapIndicatori.get(BdcKpiEntity.DURATA_GENERI_VUOTI);
            }
            listaKPI.add(calcolaKpiCopertura(durataFilm, durataGeneriVuoti, durataPubblicita,
                    durataTrasmissioni, dateFrom, dateTo));
            listaKPI.add(calcolaKpiDurataMusicaDichiarata(mapIndicatoriDTO));
            listaKPI.add(calcolaKpiBraniTitoliErronei(mapIndicatoriDTO));
            listaKPI.add(calcolaKpiFilmTitoliErronei(mapIndicatoriDTO));
            listaKPI.add(calcolaKpiAutoriTitoliErronei(mapIndicatoriDTO));

        }else if(TipoBroadcaster.RADIO.equals(tipoBroadcaster)){
            long durataMusiche = 0;
            if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_BRANI)) {
                durataMusiche = mapIndicatori.get(BdcKpiEntity.DURATA_BRANI);
            }
            listaKPI.add(calcolaKpiDurataBraniDichiaratiRD(durataMusiche, dateFrom, dateTo));
            listaKPI.add(calcolaKpiBraniTitoliErroneiRD(mapIndicatoriDTO));
            listaKPI.add(calcolaKpiAutoriTitoliErroneiRD(mapIndicatoriDTO));

        }

        return listaKPI;
    }

    private HashMap<String, IndicatoreDTO> convertiMapIndicatori(Map<String, Long> mapIndicatori){
        HashMap<String, IndicatoreDTO> retMap = new HashMap<String, IndicatoreDTO>();

        for(String indicatore : mapIndicatori.keySet()){
            IndicatoreDTO bdcIndicatoreDTO = new IndicatoreDTO();
            bdcIndicatoreDTO.setValore(mapIndicatori.get(indicatore));
            bdcIndicatoreDTO.setIndicatore(indicatore);

            retMap.put(indicatore,bdcIndicatoreDTO);
        }

        return retMap;
    }

	@Override
	public List<KPI> listKpi(Date dateFrom, Date dateTo, Integer idCanale,
			TipoBroadcaster tipoBroadcaster,
			IndicatoriPerCanale indicatoriPerCanale)
	{
		if(dateFrom != null && dateTo != null && dateFrom.before(dateTo) && idCanale != null
				&& tipoBroadcaster != null && indicatoriPerCanale != null )
		{
			Map<String, Long> mapIndicatori = indicatoriPerCanale.mapIndicatori(idCanale, dateFrom, dateTo);
			if(mapIndicatori != null){
				List<KPI> listaKPI = new ArrayList<KPI>();
				HashMap<String, IndicatoreDTO> mapIndicatoriDTO = convertiMapIndicatori(mapIndicatori);
				if(TipoBroadcaster.TELEVISIONE.equals(tipoBroadcaster)){
		            long durataFilm = 0;
		            long durataGeneriVuoti = 0;
                    long durataPubblicita = 0;
		            long durataTrasmissioni = 0;
		            if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_FILM)) {
		                durataFilm = mapIndicatori.get(BdcKpiEntity.DURATA_FILM);
		            }
		            if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_PUBBLICITA)) {
		                durataPubblicita = mapIndicatori.get(BdcKpiEntity.DURATA_PUBBLICITA);
		            }
		            if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_TRASMISSIONE)) {
		                durataTrasmissioni = mapIndicatori.get(BdcKpiEntity.DURATA_TRASMISSIONE);
		            }
                    if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_GENERI_VUOTI)) {
                        durataGeneriVuoti = mapIndicatori.get(BdcKpiEntity.DURATA_GENERI_VUOTI);
                    }
		            listaKPI.add(calcolaKpiCopertura( durataFilm, durataGeneriVuoti, durataPubblicita,
		                    durataTrasmissioni, dateFrom, dateTo));
		            listaKPI.add(calcolaKpiDurataMusicaDichiarata(mapIndicatoriDTO));
		            listaKPI.add(calcolaKpiBraniTitoliErronei(mapIndicatoriDTO));
		            listaKPI.add(calcolaKpiFilmTitoliErronei(mapIndicatoriDTO));
		            listaKPI.add(calcolaKpiAutoriTitoliErronei(mapIndicatoriDTO));
	
		        }else if(TipoBroadcaster.RADIO.equals(tipoBroadcaster)){
		            long durataMusiche = 0;
		            if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_BRANI)) {
		                durataMusiche = mapIndicatori.get(BdcKpiEntity.DURATA_BRANI);
		            }
		            listaKPI.add(calcolaKpiDurataBraniDichiaratiRD(durataMusiche, dateFrom, dateTo));
		            listaKPI.add(calcolaKpiBraniTitoliErroneiRD(mapIndicatoriDTO));
		            listaKPI.add(calcolaKpiAutoriTitoliErroneiRD(mapIndicatoriDTO));
		        }else{
		        	return null;
		        }
				return listaKPI;
			}			
		}
		return null;
	}
	
	@Override
	public IndicatoriPerCanale indicatoriPerCanale(Date dataDa, Date dataA){
		IndicatoriPerCanale indicatoriPerCanale = new IndicatoriPerCanale();
		if(dataDa != null && dataA != null && dataDa.before(dataA)){
			Calendar calendar = new GregorianCalendar();
            calendar.setTime(dataDa);
            Integer annoDa = calendar.get(Calendar.YEAR);
            Integer meseDa = calendar.get(Calendar.MONTH) + 1;

            calendar.setTime(dataA);
            Integer annoA = calendar.get(Calendar.YEAR);
            Integer meseA = calendar.get(Calendar.MONTH) + 1;
            
            indicatoriPerCanale = indicatoreDTOService.indicatoriPerCanale(annoDa, meseDa, annoA, meseA);
		}
		return indicatoriPerCanale;
		
	}
}