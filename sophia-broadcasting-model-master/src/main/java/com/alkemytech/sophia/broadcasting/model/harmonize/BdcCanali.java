package com.alkemytech.sophia.broadcasting.model.harmonize;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the bdc_canali database table.
 * 
 */
@Entity(name = "Canale")
@Table(name="bdc_canali")
@NamedQuery(name="Canale.findAll", query="SELECT b FROM Canale b")
public class BdcCanali implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private Integer id;

	private Boolean active;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_creazione")
	private Date dataCreazione;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_fine_valid")
	private Date dataFineValid;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_inizio_valid")
	private Date dataInizioValid;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_ultima_modifica")
	private Date dataUltimaModifica;

	private Boolean generalista;

	@Column(nullable=false, length=255)
	private String nome;

	@Column(name="special_radio")
	private Boolean specialRadio;

	@Column(name="tipo_canale", nullable=false, length=1)
	private String tipoCanale;

	@Column(name="utente_ultima_modifica", length=255)
	private String utenteUltimaModifica;

	//bi-directional many-to-one association to BdcErrRdShowSchedule
	@JsonManagedReference
	@OneToMany(mappedBy="bdcCanali")
	private List<BdcErrRdShowSchedule> bdcErrRdShowSchedules;

	//bi-directional many-to-one association to BdcErrTvShowSchedule
	@JsonManagedReference
	@OneToMany(mappedBy="bdcCanali")
	private List<BdcErrTvShowSchedule> bdcErrTvShowSchedules;

	//bi-directional many-to-one association to BdcRdShowSchedule
	@JsonManagedReference
	@OneToMany(mappedBy="bdcCanali")
	private List<BdcRdShowSchedule> bdcRdShowSchedules;

	//bi-directional many-to-one association to BdcTvShowSchedule
	@JsonManagedReference
	@OneToMany(mappedBy="bdcCanali")
	private List<BdcTvShowSchedule> bdcTvShowSchedules;

	//bi-directional many-to-one association to BdcUtilizationFile
	@JsonManagedReference
	@OneToMany(mappedBy="bdcCanali")
	private List<BdcUtilizationFile> bdcUtilizationFiles;

	//bi-directional many-to-one association to BdcBroadcaster
	@JsonBackReference
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_broadcaster", nullable=false)
	private BdcBroadcaster bdcBroadcaster;

	public BdcBroadcaster getBdcBroadcaster() {
		return bdcBroadcaster;
	}

	public void setBdcBroadcaster(BdcBroadcaster bdcBroadcaster) {
		this.bdcBroadcaster = bdcBroadcaster;
	}

	public BdcCanali() {
	}



	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Date getDataCreazione() {
		return this.dataCreazione;
	}

	public void setDataCreazione(Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}

	public Date getDataFineValid() {
		return this.dataFineValid;
	}

	public void setDataFineValid(Date dataFineValid) {
		this.dataFineValid = dataFineValid;
	}

	public Date getDataInizioValid() {
		return this.dataInizioValid;
	}

	public void setDataInizioValid(Date dataInizioValid) {
		this.dataInizioValid = dataInizioValid;
	}

	public Date getDataUltimaModifica() {
		return this.dataUltimaModifica;
	}

	public void setDataUltimaModifica(Date dataUltimaModifica) {
		this.dataUltimaModifica = dataUltimaModifica;
	}

	public Boolean getGeneralista() {
		return this.generalista;
	}

	public void setGeneralista(Boolean generalista) {
		this.generalista = generalista;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Boolean getSpecialRadio() {
		return this.specialRadio;
	}

	public void setSpecialRadio(Boolean specialRadio) {
		this.specialRadio = specialRadio;
	}

	public String getTipoCanale() {
		return this.tipoCanale;
	}

	public void setTipoCanale(String tipoCanale) {
		this.tipoCanale = tipoCanale;
	}

	public String getUtenteUltimaModifica() {
		return this.utenteUltimaModifica;
	}

	public void setUtenteUltimaModifica(String utenteUltimaModifica) {
		this.utenteUltimaModifica = utenteUltimaModifica;
	}

	public List<BdcErrRdShowSchedule> getBdcErrRdShowSchedules() {
		return this.bdcErrRdShowSchedules;
	}

	public void setBdcErrRdShowSchedules(List<BdcErrRdShowSchedule> bdcErrRdShowSchedules) {
		this.bdcErrRdShowSchedules = bdcErrRdShowSchedules;
	}

	public BdcErrRdShowSchedule addBdcErrRdShowSchedule(BdcErrRdShowSchedule bdcErrRdShowSchedule) {
		getBdcErrRdShowSchedules().add(bdcErrRdShowSchedule);
		bdcErrRdShowSchedule.setBdcCanali(this);

		return bdcErrRdShowSchedule;
	}

	public BdcErrRdShowSchedule removeBdcErrRdShowSchedule(BdcErrRdShowSchedule bdcErrRdShowSchedule) {
		getBdcErrRdShowSchedules().remove(bdcErrRdShowSchedule);
		bdcErrRdShowSchedule.setBdcCanali(null);

		return bdcErrRdShowSchedule;
	}

	public List<BdcErrTvShowSchedule> getBdcErrTvShowSchedules() {
		return this.bdcErrTvShowSchedules;
	}

	public void setBdcErrTvShowSchedules(List<BdcErrTvShowSchedule> bdcErrTvShowSchedules) {
		this.bdcErrTvShowSchedules = bdcErrTvShowSchedules;
	}

	public BdcErrTvShowSchedule addBdcErrTvShowSchedule(BdcErrTvShowSchedule bdcErrTvShowSchedule) {
		getBdcErrTvShowSchedules().add(bdcErrTvShowSchedule);
		bdcErrTvShowSchedule.setBdcCanali(this);

		return bdcErrTvShowSchedule;
	}

	public BdcErrTvShowSchedule removeBdcErrTvShowSchedule(BdcErrTvShowSchedule bdcErrTvShowSchedule) {
		getBdcErrTvShowSchedules().remove(bdcErrTvShowSchedule);
		bdcErrTvShowSchedule.setBdcCanali(null);

		return bdcErrTvShowSchedule;
	}

	public List<BdcRdShowSchedule> getBdcRdShowSchedules() {
		return this.bdcRdShowSchedules;
	}

	public void setBdcRdShowSchedules(List<BdcRdShowSchedule> bdcRdShowSchedules) {
		this.bdcRdShowSchedules = bdcRdShowSchedules;
	}

	public BdcRdShowSchedule addBdcRdShowSchedule(BdcRdShowSchedule bdcRdShowSchedule) {
		getBdcRdShowSchedules().add(bdcRdShowSchedule);
		bdcRdShowSchedule.setBdcCanali(this);

		return bdcRdShowSchedule;
	}

	public BdcRdShowSchedule removeBdcRdShowSchedule(BdcRdShowSchedule bdcRdShowSchedule) {
		getBdcRdShowSchedules().remove(bdcRdShowSchedule);
		bdcRdShowSchedule.setBdcCanali(null);

		return bdcRdShowSchedule;
	}

	public List<BdcTvShowSchedule> getBdcTvShowSchedules() {
		return this.bdcTvShowSchedules;
	}

	public void setBdcTvShowSchedules(List<BdcTvShowSchedule> bdcTvShowSchedules) {
		this.bdcTvShowSchedules = bdcTvShowSchedules;
	}

	public BdcTvShowSchedule addBdcTvShowSchedule(BdcTvShowSchedule bdcTvShowSchedule) {
		getBdcTvShowSchedules().add(bdcTvShowSchedule);
		bdcTvShowSchedule.setBdcCanali(this);

		return bdcTvShowSchedule;
	}

	public BdcTvShowSchedule removeBdcTvShowSchedule(BdcTvShowSchedule bdcTvShowSchedule) {
		getBdcTvShowSchedules().remove(bdcTvShowSchedule);
		bdcTvShowSchedule.setBdcCanali(null);

		return bdcTvShowSchedule;
	}

	public List<BdcUtilizationFile> getBdcUtilizationFiles() {
		return this.bdcUtilizationFiles;
	}

	public void setBdcUtilizationFiles(List<BdcUtilizationFile> bdcUtilizationFiles) {
		this.bdcUtilizationFiles = bdcUtilizationFiles;
	}

	public BdcUtilizationFile addBdcUtilizationFile(BdcUtilizationFile bdcUtilizationFile) {
		getBdcUtilizationFiles().add(bdcUtilizationFile);
		bdcUtilizationFile.setBdcCanali(this);

		return bdcUtilizationFile;
	}

	public BdcUtilizationFile removeBdcUtilizationFile(BdcUtilizationFile bdcUtilizationFile) {
		getBdcUtilizationFiles().remove(bdcUtilizationFile);
		bdcUtilizationFile.setBdcCanali(null);

		return bdcUtilizationFile;
	}

}