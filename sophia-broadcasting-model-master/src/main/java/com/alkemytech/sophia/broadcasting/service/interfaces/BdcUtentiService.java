package com.alkemytech.sophia.broadcasting.service.interfaces;

import com.alkemytech.sophia.broadcasting.dto.RepertorioDTO;
import com.alkemytech.sophia.broadcasting.model.BdcBroadcasters;
import com.alkemytech.sophia.broadcasting.model.BdcRepertorio;
import com.alkemytech.sophia.broadcasting.model.BdcUtenti;
import com.alkemytech.sophia.broadcasting.model.BdcUtentiRepertorio;

import java.util.List;

public interface BdcUtentiService {
	BdcUtenti findById(Integer id);

	List<BdcUtenti> findAll();

	List<BdcUtenti> findAllByBroadcaster(BdcBroadcasters bdcBroadcasters);

	BdcUtenti findByUsername(String username);

	BdcUtenti findByEmail(String email);

	boolean signup(BdcUtenti bdcUtente, List<RepertorioDTO> list);

	boolean deleteUser(BdcUtenti bdcUtente);

	int updateUser(BdcUtenti bdcUtente, List<RepertorioDTO> list);

	boolean changePassword(BdcUtenti bdcUtente);

	List<BdcRepertorio> findRepertori();

	List<BdcUtentiRepertorio> findRepertoriByUserId(int idUtente);

	List<BdcUtenti> findByRepertori(BdcBroadcasters bdcBroadcasters, String repertori);
}
