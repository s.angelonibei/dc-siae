package com.alkemytech.sophia.broadcasting.data;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alkemytech.sophia.broadcasting.exception.NoIndicatoriException;
import com.alkemytech.sophia.broadcasting.model.BdcKpiEntity;
import com.alkemytech.sophia.broadcasting.model.TipoBroadcaster;

public class IndicatoriPerCanale {
	
	//Map<tipoEmittente, Map<canale, Map<anno_mese, Map<indictore, valore>>>>
	private Map<String, Map<Integer, Map<String, Map<String, Long>>>> mapIndicatoriPerCanale;
	
	public IndicatoriPerCanale(){
		mapIndicatoriPerCanale = new HashMap<String, Map<Integer,Map<String,Map<String,Long>>>>();
		mapIndicatoriPerCanale.put(TipoBroadcaster.TELEVISIONE.toString(), new HashMap<Integer, Map<String,Map<String,Long>>>());
		mapIndicatoriPerCanale.put(TipoBroadcaster.RADIO.toString(), new HashMap<Integer, Map<String,Map<String,Long>>>());
	}
	
	public boolean add(String tipoEmittente, Integer canale, Integer anno, Integer mese, String indicatore, Long valore){
		if((TipoBroadcaster.TELEVISIONE.toString().equals(tipoEmittente) || TipoBroadcaster.RADIO.toString().equals(tipoEmittente))
				&& canale != null && anno != null && mese != null && indicatore != null && valore != null
			)
		{
			if(!mapIndicatoriPerCanale.containsKey(tipoEmittente)){
				mapIndicatoriPerCanale.put(tipoEmittente, new HashMap<Integer, Map<String,Map<String,Long>>>());
			}
			Map<Integer,Map<String,Map<String,Long>>> mapCanali = mapIndicatoriPerCanale.get(tipoEmittente);
			if(!mapCanali.containsKey(canale)){
				mapCanali.put(canale, new HashMap<String, Map<String,Long>>());
			}
			Map<String,Map<String,Long>> mapAnnoMese = mapCanali.get(canale);
			String annoMese = chiaveAnnoMese(anno, mese);
			if(!mapAnnoMese.containsKey(annoMese)){
				mapAnnoMese.put(annoMese, new HashMap<String, Long>());
			}
			Map<String,Long> mapIndicatori = mapAnnoMese.get(annoMese);
			mapIndicatori.put(indicatore, valore);
			return true;
		}
		return false;
	}
	
	public Map<String, Long> mapIndicatori(Integer canale, Date dataDa, Date dataA){
		Map<String, Long> retMap = new HashMap<String, Long>();
		if(mapIndicatoriPerCanale!=null && canale != null && dataDa != null && dataA != null && dataDa.before(dataA)){
			String tipoEmittente = null;			
			if(mapIndicatoriPerCanale.get(TipoBroadcaster.TELEVISIONE.toString()).containsKey(canale)){
				tipoEmittente = TipoBroadcaster.TELEVISIONE.toString();
			}else if(mapIndicatoriPerCanale.get(TipoBroadcaster.RADIO.toString()).containsKey(canale)){
				tipoEmittente = TipoBroadcaster.RADIO.toString();
			}else{
				return null;
			}
			try{
				if(tipoEmittente !=null){//il canale è presente all'interno della mappa con indicatori di qualche periodo
					Map<String, Map<String, Long>> mapIndicatoriAnnoMese = mapIndicatoriPerCanale.get(tipoEmittente).get(canale);
					List<String> listaChiaviAnnoMese = listaChiaviAnnoMese(dataDa, dataA);
					if(TipoBroadcaster.TELEVISIONE.toString().equals(tipoEmittente)){
						List<String> listaIndicatori = indicatoriTV();
						for(String indicatore : listaIndicatori){
							retMap.put(indicatore, sommaValoriIndicatore(mapIndicatoriAnnoMese,listaChiaviAnnoMese,indicatore));
						}
					}else if(TipoBroadcaster.RADIO.toString().equals(tipoEmittente)){
						List<String> listaIndicatori = indicatoriRadio();
						for(String indicatore : listaIndicatori){
							retMap.put(indicatore, sommaValoriIndicatore(mapIndicatoriAnnoMese,listaChiaviAnnoMese,indicatore));
						}
					}
				}				
			}catch(NoIndicatoriException e){
				return null;
			}
		}else{
			return null;
		}		
		return retMap;
	}
	
	
	private String chiaveAnnoMese(Integer anno, Integer mese){
		return anno + "_" + mese;
	}
	
	private List<String> listaChiaviAnnoMese(Date dataDa, Date dataA){
		List<String> keyList = new ArrayList<String>();
		if(dataDa != null && dataA != null && dataDa.before(dataA)){
			GregorianCalendar calendar = new GregorianCalendar();
			calendar.setTime(dataDa);
			int annoDa = calendar.get(Calendar.YEAR);
			int meseDa = calendar.get(Calendar.MONTH) + 1;
			
			calendar.setTime(dataA);
			int annoA = calendar.get(Calendar.YEAR);
			int meseA = calendar.get(Calendar.MONTH) + 1;
			
			if(annoDa == annoA){
				for(int m = meseDa; m <= meseA; m++){
					keyList.add(chiaveAnnoMese(annoDa, m));
				}
			}else{
				for(int m = meseDa; m <= 12; m++){
					keyList.add(chiaveAnnoMese(annoDa, m));
				}
				for(int anno=annoDa+1; anno<annoA; anno++){
					for(int m = 1; m <= 12; m++){
						keyList.add(chiaveAnnoMese(anno, m));
					}
				}				
				for(int m = 1; m <= meseA; m++){
					keyList.add(chiaveAnnoMese(annoA, m));
				}
			}
		}
		return keyList;
	}
	
	private Long sommaValoriIndicatore(Map<String, Map<String, Long>> mapIndicatoriAnnoMese, List<String> listaChiaviAnnoMese, String indicatore) throws NoIndicatoriException{
		Long somma = new Long(0);
		boolean indicatoriTrovati = false;
		if(mapIndicatoriAnnoMese != null && listaChiaviAnnoMese != null && indicatore != null){
			for(String annoMese : listaChiaviAnnoMese){
				if(mapIndicatoriAnnoMese.containsKey(annoMese)){
					Map<String, Long> mappaIndicatori = mapIndicatoriAnnoMese.get(annoMese);
					if(mappaIndicatori != null && mappaIndicatori.containsKey(indicatore)){
						indicatoriTrovati = true;
						somma += mappaIndicatori.get(indicatore);
					}
				}
			}
		}
		if(!indicatoriTrovati){
			throw new NoIndicatoriException();
		}
		return somma;
	}
		
	private static List<String> indicatoriTV(){
		ArrayList<String> indicatori = new ArrayList<String>();
		indicatori.add(BdcKpiEntity.RECORD_TOTALI);
		indicatori.add(BdcKpiEntity.DURATA_PUBBLICITA);
		indicatori.add(BdcKpiEntity.DURATA_BRANI);
		indicatori.add(BdcKpiEntity.DURATA_BRANI_DICHIARATI);
		indicatori.add(BdcKpiEntity.DURATA_BRANI_TITOLI_ERRONEI);
		indicatori.add(BdcKpiEntity.RECORD_SCARTATI);
		indicatori.add(BdcKpiEntity.RECORD_IN_RITARDO);
		indicatori.add(BdcKpiEntity.DURATA_PROGRAMMI_TITOLI_ERRONEI);
		indicatori.add(BdcKpiEntity.DURATA_FILM);
		indicatori.add(BdcKpiEntity.TRASMISSIONI_MUSICA_ECCEDENTE);
		indicatori.add(BdcKpiEntity.DURATA_TRASMISSIONE);
		indicatori.add(BdcKpiEntity.DURATA_BRANI_AUTORI_ERRONEI);
		indicatori.add(BdcKpiEntity.TRASMISSIONI_TOTALI);
		indicatori.add(BdcKpiEntity.DURATA_FILM_TITOLI_ERRONEI);
		return indicatori;
	}
	
	private static List<String> indicatoriRadio(){
		ArrayList<String> indicatori = new ArrayList<String>();
		indicatori.add(BdcKpiEntity.RECORD_TOTALI);
		indicatori.add(BdcKpiEntity.DURATA_BRANI_AUTORI_ERRONEI);
		indicatori.add(BdcKpiEntity.DURATA_BRANI);
		indicatori.add(BdcKpiEntity.DURATA_BRANI_TITOLI_ERRONEI);
		indicatori.add(BdcKpiEntity.RECORD_SCARTATI);
		indicatori.add(BdcKpiEntity.RECORD_IN_RITARDO);
		return indicatori;
			}
}
