package com.alkemytech.sophia.broadcasting.dto;

import com.alkemytech.sophia.broadcasting.model.BdcBroadcasters;
import com.alkemytech.sophia.broadcasting.model.BdcCanali;
import com.alkemytech.sophia.performing.model.PerfMusicProvider;
import com.alkemytech.sophia.performing.model.PerfPalinsesto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class FileArmonizzatoRequestDTO {
	private BdcBroadcasters broadcasters;
	private List<BdcCanali> canali;
	private PerfMusicProvider musicProvider;
	private List<PerfPalinsesto> palinsesti;
	private Integer anno;
	private List<Integer> mesi;

	public FileArmonizzatoRequestDTO() {
	}

	public FileArmonizzatoRequestDTO(PerfMusicProvider musicProvider, List<PerfPalinsesto> palinsesti, Integer anno, List<Integer> mesi) {
		this.musicProvider = musicProvider;
		this.palinsesti = palinsesti;
		this.anno = anno;
		this.mesi = mesi;
	}

	public FileArmonizzatoRequestDTO(BdcBroadcasters broadcasters, List<BdcCanali> canali, Integer anno, List<Integer> mesi) {
		this.broadcasters = broadcasters;
		this.canali = canali;
		this.anno = anno;
		this.mesi = mesi;
	}

	public FileArmonizzatoRequestDTO(BdcBroadcasters broadcasters, List<BdcCanali> canali, PerfMusicProvider musicProvider, List<PerfPalinsesto> palinsesti, Integer anno, List<Integer> mesi) {
		this.broadcasters = broadcasters;
		this.canali = canali;
		this.musicProvider = musicProvider;
		this.palinsesti = palinsesti;
		this.anno = anno;
		this.mesi = mesi;
	}

	public BdcBroadcasters getBroadcasters() {
		return broadcasters;
	}

	public void setBroadcasters(BdcBroadcasters broadcasters) {
		this.broadcasters = broadcasters;
	}

	public List<BdcCanali> getCanali() {
		return canali;
	}

	public void setCanali(List<BdcCanali> canali) {
		this.canali = canali;
	}

	public PerfMusicProvider getMusicProvider() {
		return musicProvider;
	}

	public void setMusicProvider(PerfMusicProvider musicProvider) {
		this.musicProvider = musicProvider;
	}

	public List<PerfPalinsesto> getPalinsesti() {
		return palinsesti;
	}

	public void setPalinsesti(List<PerfPalinsesto> palinsesti) {
		this.palinsesti = palinsesti;
	}

	public Integer getAnno() {
		return anno;
	}

	public void setAnno(Integer anno) {
		this.anno = anno;
	}

	public List<Integer> getMesi() {
		return mesi;
	}

	public void setMesi(List<Integer> mesi) {
		this.mesi = mesi;
	}

	public String getHarmonizedFileName(){
		String fileName = "download";
		StringBuilder sbFileName = new StringBuilder();
		//appendCanaliOrPalinsesti(sbFileName);
		if(anno != null){
			sbFileName.append(anno).append("-");
		}
		if(mesi!=null){
			for(Integer mese : mesi){
				sbFileName.append(mese).append("-");
			}
		}
		if(sbFileName.length() > 1){
			fileName = sbFileName.toString();
			fileName = fileName.substring(0, fileName.length() -1);
		}
		return broadcasters.getNome().trim()+ "_" + fileName;// + "_" + System.currentTimeMillis();
	}

	private void appendCanaliOrPalinsesti(StringBuilder sbFileName) {
		if(canali != null && !canali.isEmpty()){
			for(BdcCanali canale : canali){
				sbFileName.append(canale.getId()).append("-");
			}
		}
		else if(palinsesti != null && !palinsesti.isEmpty()){
			for(PerfPalinsesto palinsesto : palinsesti){
				sbFileName.append(palinsesto.getCodiceId()).append("-");
			}
		}
	}
}
