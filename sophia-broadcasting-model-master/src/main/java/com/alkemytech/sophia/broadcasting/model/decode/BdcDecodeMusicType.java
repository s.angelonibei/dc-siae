package com.alkemytech.sophia.broadcasting.model.decode;

import com.alkemytech.sophia.broadcasting.model.BdcBroadcasters;
import com.alkemytech.sophia.broadcasting.model.MusicType;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Alessandro Russo on 09/12/2017.
 */
@Entity
@Table(name = "BDC_DECODE_MUSIC_TYPE")
@NamedQuery(name="BdcDecodeMusicType.findAll", query="SELECT d FROM BdcDecodeMusicType d")
public class BdcDecodeMusicType implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_DECODE_MUSIC_TYPE")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "ID_BROADCASTER")
    private BdcBroadcasters broadcaster;

    @ManyToOne
    @JoinColumn(name="ID_MUSIC_TYPE")
    private MusicType musicType;

    @Column(name = "VALUE")
    private String value;

    @Column(name = "SCOPE")
    private String scope;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BdcBroadcasters getBroadcaster() {
        return broadcaster;
    }

    public void setBroadcaster(BdcBroadcasters broadcaster) {
        this.broadcaster = broadcaster;
    }

    public MusicType getMusicType() {
        return musicType;
    }

    public void setMusicType(MusicType musicType) {
        this.musicType = musicType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    @Override
    public String toString() {
        return "BdcDecodeMusicType{" +
                "id=" + id +
                ", broadcaster=" + broadcaster +
                ", musicType=" + musicType +
                ", value='" + value + '\'' +
                ", scope='" + scope + '\'' +
                '}';
    }
}
