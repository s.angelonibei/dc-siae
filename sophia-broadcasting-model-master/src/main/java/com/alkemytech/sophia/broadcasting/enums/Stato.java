package com.alkemytech.sophia.broadcasting.enums;

public enum Stato {

    ELABORATO("ELABORATO"),
    DA_ELABORARE("DA_ELABORARE"),
    DA_CONVERTIRE("DA_CONVERTIRE"),
    VALIDATO("VALIDATO"),
    IN_CONVERSIONE("IN_CONVERSIONE"),
    ERRORE("ERRORE"),
    IN_LAVORAZIONE("IN_LAVORAZIONE"),
    ELIMINATO("ELIMINATO"),
    ACQUISITO("ACQUISITO"),
    ANNULLATO("ANNULLATO"),
    CONFERMATO("CONFERMATO");


    String text;

    Stato(String text) {
        this.text = text;
    }


    @Override
    public String toString() {
        return text;
    }

    public static Stato valueOfDescription(String description) {

        Stato stato=null;

        for (Stato v : values()) {

            if (v.toString().equals(description)) {
                stato = v;
            }
        }

        return stato;
    }
}
