package com.alkemytech.sophia.broadcasting.dto;

import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.broadcasting.model.BdcCaricoRipartizione;

@XmlRootElement
public class UpdateCarichiRipartizioneRequestDto {
	private String username;
	private BdcCaricoRipartizione caricoRipartizione;

	public UpdateCarichiRipartizioneRequestDto() {
		super();
	}

	public UpdateCarichiRipartizioneRequestDto(String username, BdcCaricoRipartizione caricoRipartizione) {
		super();
		this.username = username;
		this.caricoRipartizione = caricoRipartizione;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public BdcCaricoRipartizione getCaricoRipartizione() {
		return caricoRipartizione;
	}

	public void setCaricoRipartizione(BdcCaricoRipartizione caricoRipartizione) {
		this.caricoRipartizione = caricoRipartizione;
	}

}
