package com.alkemytech.sophia.broadcasting.dto;

import com.google.gson.annotations.SerializedName;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ResponseMonitoraggioKPIDTO extends ResponseDTO {
    @XmlElement(name = "ResponseData")
    @SerializedName("ResponseData")
    private ResponseMonitoraggioKPIDataDTO responseData;

    public ResponseMonitoraggioKPIDataDTO getResponseData() {
        return responseData;
    }

    public void setResponseData(ResponseMonitoraggioKPIDataDTO responseData) {
        this.responseData = responseData;
    }
}
