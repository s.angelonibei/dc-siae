package com.alkemytech.sophia.broadcasting.service.interfaces;

import com.alkemytech.sophia.broadcasting.dto.FileArmonizzatoRequestDTO;
import com.alkemytech.sophia.broadcasting.dto.harmonization.TvHarmonizedDTO;

import java.util.List;

/**
 * Created by Alessandro Russo on 14/12/2017.
 */
public interface HarmonizedFileService {

    public List<TvHarmonizedDTO> findHarmonizedRecords(FileArmonizzatoRequestDTO request);
}
