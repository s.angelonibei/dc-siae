package com.alkemytech.sophia.broadcasting.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.broadcasting.model.BdcUtenti;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BdcUtenteDto {
	private BdcUtenti bdcUtenti;
	private List<RepertorioDTO> repertori;
	private boolean sendMail;

	public BdcUtenteDto() {
		super();
	}

	public BdcUtenteDto(BdcUtenti bdcUtenti, List<RepertorioDTO> repertori, boolean sendMail) {
		super();
		this.bdcUtenti = bdcUtenti;
		this.repertori = repertori;
		this.sendMail = sendMail;
	}

	public BdcUtenti getBdcUtenti() {
		return bdcUtenti;
	}

	public void setBdcUtenti(BdcUtenti bdcUtenti) {
		this.bdcUtenti = bdcUtenti;
	}

	public List<RepertorioDTO> getRepertori() {
		return repertori;
	}

	public void setRepertori(List<RepertorioDTO> repertori) {
		this.repertori = repertori;
	}

	public boolean isSendMail() {
		return sendMail;
	}

	public void setSendMail(boolean sendMail) {
		this.sendMail = sendMail;
	}

}
