package com.alkemytech.sophia.broadcasting.service;

import com.alkemytech.sophia.broadcasting.enums.Stato;
import com.alkemytech.sophia.broadcasting.model.*;
import com.alkemytech.sophia.broadcasting.model.utilizations.*;
import com.alkemytech.sophia.broadcasting.service.interfaces.BdcCanaliService;
import com.alkemytech.sophia.broadcasting.service.interfaces.DecodeService;
import com.alkemytech.sophia.broadcasting.service.interfaces.NormalizedFileService;
import com.alkemytech.sophia.common.Constants;
import com.alkemytech.sophia.common.tools.DateTools;
import com.alkemytech.sophia.common.tools.StringTools;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.alkemytech.sophia.broadcasting.utils.Utilities.getMapValue;

/**
 * Created by Alessandro Russo on 09/12/2017.
 */
@Singleton
public class NormalizedServiceImpl implements NormalizedFileService {

    Logger logger = LoggerFactory.getLogger(getClass());

    private final Provider<EntityManager> provider;
    private final DecodeService decodeService;
    private final BdcCanaliService canaliService;

    @Inject
    public NormalizedServiceImpl(Provider<EntityManager> provider, DecodeService decodeService, BdcCanaliService canaliService) {
        this.provider = provider;
        this.decodeService = decodeService;
        this.canaliService = canaliService;
    }

    @Override
    public List<BdcNormalizedFile> getNormalizedFileList(Stato originalReportStatus) {
        EntityManager em = provider.get();
        StringBuilder queryString = new StringBuilder("SELECT distinct n.* FROM BDC_NORMALIZED_FILE n " +
                "INNER JOIN BDC_UTILIZATION_NORMALIZED_FILE r ON r.ID_NORMALIZED_FILE = n.ID_NORMALIZED_FILE " +
                "INNER JOIN bdc_utilization_file u ON u.id = r.ID_UTILIZATION_FILE " +
                "WHERE u.stato = ?1 ");
        final Query q = em.createNativeQuery(queryString.toString(), BdcNormalizedFile.class).setParameter(1, originalReportStatus.toString());
        List<BdcNormalizedFile> result = q.getResultList();
        return result;
    }

    @Transactional
    public BdcNormalizedFile save(BdcNormalizedFile normalizedFile) {
        EntityManager em = provider.get();
        em.getTransaction().begin();
        try {
            em.merge(normalizedFile);
            em.flush();
            em.getTransaction().commit();
            return normalizedFile;
        }
        catch (Exception e){
            logger.error(e.getMessage());
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
            return null;
        }
    }

    @Override
    public void saveNormalizedTvReport(List<BdcTvShowSchedule> records) {
        EntityManager em = provider.get();
        em.getTransaction().begin();
        try {
            for (BdcTvShowSchedule tvShow : records) {
                em.persist(tvShow);
                em.flush();
            }
            em.getTransaction().commit();
        }
        catch (Exception e){
            logger.error(e.getMessage());
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
        }
    }

    @Override
    public void saveNormalizedRdReport(List<BdcRdShowSchedule> records) {
        EntityManager em = provider.get();
        em.getTransaction().begin();
        try {
            for (BdcRdShowSchedule rsShow : records) {
                em.persist(rsShow);
                em.flush();
            }
            em.getTransaction().commit();
        }
        catch (Exception e){
            logger.error(e.getMessage());
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
        }
    }

    @Override
    public BdcTvShowSchedule fromMapToTvShow(Map<String, Object> record, BdcNormalizedFile normalizedFile) {
        logger.debug("fromMapToTvShow");
        Long idMusicType = getMapValue(record, Constants.DECODE_ID_CATEGORIA_USO_OP) != null ? Long.parseLong(getMapValue(record, Constants.DECODE_ID_CATEGORIA_USO_OP)) : null;
        logger.debug("idMusicType " + idMusicType);
        MusicType musicType = null;
        if (idMusicType != null) {
            musicType = decodeService.findMusicTypeById(idMusicType);
        }
        logger.debug("DOPO IF IDMUSICTYPE ");
        BdcTvShowMusic tvShowMusic = new BdcTvShowMusic(musicType, normalizedFile, record);
        logger.debug("PRE CANALE ");
        BdcCanali canale = null;
        if (getMapValue(record, Constants.DECODE_ID_CANALE) != null) {
            canale = canaliService.findById(new Integer(getMapValue(record, Constants.DECODE_ID_CANALE)));
            if (canale != null)
                logger.debug("canale " + canale.toString());
        }
        if (musicType != null) {
            logger.debug("musicType " + musicType.toString());
        }
        logger.debug("tvShowMusic " + tvShowMusic.toString());
        Long idShowType = getMapValue(record, Constants.DECODE_ID_GENERE_TX) != null ? Long.parseLong(getMapValue(record, Constants.DECODE_ID_GENERE_TX)) : null;
        logger.debug("idShowType " + idShowType);
        ShowType showType = null;
        if (idShowType != null) {
            showType = decodeService.findShowTypeById(idShowType);
        }

        String begin = getMapValue(record, Constants.DATA_INIZIO_TX) + " " + getMapValue(record, Constants.ORARIO_INIZIO_TX);
        Date beginTime = DateTools.stringToDate(begin, DateTools.DATETIME_ITA_FORMAT_SLASH);
        Date endTime = null;
        Integer duration = null;
        if (getMapValue(record, Constants.ORARIO_FINE_TX) != null && DateTools.stringToDate(getMapValue(record, Constants.ORARIO_FINE_TX), DateTools.TIME_COMPLETE_FORMAT) != null) {
            String end = getMapValue(record, Constants.DATA_INIZIO_TX) + " " + getMapValue(record, Constants.ORARIO_FINE_TX);
            endTime = DateTools.stringToDate(end, DateTools.DATETIME_ITA_FORMAT_SLASH);
            if (endTime != null && beginTime != null && endTime.before(beginTime)) {
                endTime = DateUtils.addDays(endTime, 1);
            }
        }
        if (getMapValue(record, Constants.DURATA_TX) != null && getMapValue(record, Constants.DURATA_TX).matches(StringTools.DURATION_FORMAT)) {
            logger.debug("IF riga 149");
            this.logger.debug("DURATA TX "+getMapValue(record, Constants.DURATA_TX));
            duration = Integer.parseInt(StringTools.parseTimeToSeconds(getMapValue(record, Constants.DURATA_TX)));
            this.logger.debug("duration "+duration);
        }

        //nel caso di film conentuti non va ricalcolato durata e fine show
        if (!"COMMENTO".equalsIgnoreCase(musicType.getName())) {
            this.logger.debug("IF riga 161");
            endTime = evaluateEndTime(beginTime, endTime, duration);
            duration = evaluateDuration(beginTime, endTime, duration);
        } else
            this.logger.debug("IF riga 167");
            tvShowMusic.setDuration(duration);

        BdcTvShowSchedule tvShowSchedule = new BdcTvShowSchedule(tvShowMusic, canale, normalizedFile, showType, beginTime, endTime, duration, record);
        this.logger.debug("tvShowSchedule "+tvShowSchedule.toString());
        //SET KPI PROPERTIES
        Calendar cal = Calendar.getInstance();
        cal.setTime(beginTime);
        tvShowSchedule.setScheduleYear(cal.get(Calendar.YEAR));
        tvShowSchedule.setScheduleMonth(cal.get(Calendar.MONTH) + 1);
        long diff = normalizedFile.getFirstOriginalReport().getDataUpload().getTime() - tvShowSchedule.getBeginTime().getTime();
        this.logger.debug("diff "+diff);
        tvShowSchedule.setDaysFromUpload((int) TimeUnit.MILLISECONDS.toDays(diff));
        tvShowMusic.setDaysFromUpload((int) TimeUnit.MILLISECONDS.toDays(diff));
        logger.debug("TV RECORD: {}", tvShowSchedule.toString());
        return tvShowSchedule;
    }

    private Date evaluateEndTime(Date beginTime, Date endTime, Integer duration) {
        if (endTime == null) {
            if (beginTime != null && duration != null) {
                return DateUtils.addSeconds(beginTime, duration);
            }
        }
        return endTime;
    }

    private Integer evaluateDuration(Date beginTime, Date endTime, Integer duration) {
        Long diffSeconds = null;
        if (endTime != null && beginTime != null) {
            long diff = endTime.getTime() - beginTime.getTime();
            diffSeconds = TimeUnit.MILLISECONDS.toSeconds(diff);
        }

        if (duration == null) {
            return diffSeconds != null ? diffSeconds.intValue() : null;
        } else {
            if (diffSeconds != null) {
                return duration > diffSeconds.intValue() ? diffSeconds.intValue() : duration;
            }
        }
        return duration;
    }

    @Override
    public BdcErrTvShowSchedule fromMapToErrTvShow(Map<String, Object> record, BdcNormalizedFile normalizedFile) {
        MusicType musicType = null;
        if (getMapValue(record, Constants.DECODE_ID_CATEGORIA_USO_OP) != null) {
            musicType = decodeService.findMusicTypeById(Long.parseLong(getMapValue(record, Constants.DECODE_ID_CATEGORIA_USO_OP)));
        }
        BdcErrTvShowMusic errTvShowMusic = new BdcErrTvShowMusic(musicType, normalizedFile, record);

        BdcCanali canale = getMapValue(record, Constants.DECODE_ID_CANALE) != null ? canaliService.findById(new Integer(getMapValue(record, Constants.DECODE_ID_CANALE))) : null;
        ShowType showType = getMapValue(record, Constants.DECODE_ID_GENERE_TX) != null ? decodeService.findShowTypeById(Long.parseLong(getMapValue(record, Constants.DECODE_ID_GENERE_TX))) : null;
        String begin = getMapValue(record, Constants.DATA_INIZIO_TX);
        String time = getMapValue(record, Constants.ORARIO_INIZIO_TX);
        String beginOp = getMapValue(record, Constants.ORARIO_INIZIO_OP);
//        String durataOp = getMapValue(record, Constants.DURATA_OP);
        begin = (StringUtils.isNotEmpty(begin) ? begin : "") + " " + (StringUtils.isNotEmpty(time) ? time : (StringUtils.isNotEmpty(beginOp) ? beginOp : "00:00:00"));
        Date beginTime = DateTools.stringToDate(begin, DateTools.DATETIME_ITA_FORMAT_SLASH);
        Date endTime = null;
        Integer duration = null;
        if (beginTime != null && getMapValue(record, Constants.ORARIO_FINE_TX) != null && DateTools.stringToDate(getMapValue(record, Constants.ORARIO_FINE_TX), DateTools.TIME_COMPLETE_FORMAT) != null) {
            String end = getMapValue(record, Constants.DATA_INIZIO_TX) + " " + getMapValue(record, Constants.ORARIO_FINE_TX);
            endTime = DateTools.stringToDate(end, DateTools.DATETIME_ITA_FORMAT_SLASH);
            if (endTime != null && endTime.before(beginTime)) {
                endTime = DateUtils.addDays(endTime, 1);
            }
        }
        if (getMapValue(record, Constants.DURATA_TX) != null && getMapValue(record, Constants.DURATA_TX).matches(StringTools.DURATION_FORMAT)) {
            duration = Integer.parseInt(StringTools.parseTimeToSeconds(getMapValue(record, Constants.DURATA_TX)));
        }

        endTime = evaluateEndTime(beginTime, endTime, duration);

        duration = evaluateDuration(beginTime, endTime, duration);

        BdcErrTvShowSchedule errTvShowSchedule = new BdcErrTvShowSchedule(errTvShowMusic, canale, normalizedFile, showType, beginTime, endTime, duration, record);

        //SET KPI PROPERTIES
        if (beginTime != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(beginTime);
            errTvShowSchedule.setSchedulYear(cal.get(Calendar.YEAR));
            errTvShowSchedule.setScheduleMonth(cal.get(Calendar.MONTH) + 1);
            long diff = normalizedFile.getFirstOriginalReport().getDataUpload().getTime() - errTvShowSchedule.getBeginTime().getTime();
            errTvShowSchedule.setDaysFromUpload((int) TimeUnit.MILLISECONDS.toDays(diff));
            errTvShowMusic.setDaysFromUpload((int) TimeUnit.MILLISECONDS.toDays(diff));
        }
        logger.debug("TV ERROR: {}", errTvShowSchedule.toString());
        return errTvShowSchedule;
    }

    @Override
    public BdcRdShowSchedule fromMapToRdShow(Map<String, Object> record, BdcNormalizedFile normalizedFile) {
        Long idMusicType = getMapValue(record, Constants.DECODE_ID_CATEGORIA_USO_OP) != null ? Long.parseLong(getMapValue(record, Constants.DECODE_ID_CATEGORIA_USO_OP)) : null;
        MusicType musicType = null;
        if (idMusicType != null) {
            musicType = decodeService.findMusicTypeById(idMusicType);
        }

        BdcCanali canale = getMapValue(record, Constants.DECODE_ID_CANALE) != null ? canaliService.findById(new Integer(getMapValue(record, Constants.DECODE_ID_CANALE))) : null;
        String begin = getMapValue(record, Constants.DATA_INIZIO_TX);
        String timeTx = getMapValue(record, Constants.ORARIO_INIZIO_TX);
        String timeOp = getMapValue(record, Constants.ORARIO_INIZIO_OP);
        String beginTx = (StringUtils.isNotEmpty(begin) ? begin : "") + " " + (StringUtils.isNotEmpty(timeTx) ? timeTx : (StringUtils.isNotEmpty(timeOp) ? timeOp : "00:00:00"));
        String beginOp = begin + " " + timeOp;
        Date beginTimeTx = DateTools.stringToDate(beginTx, DateTools.DATETIME_ITA_FORMAT_SLASH);
        Date beginTimeOp = StringUtils.isNotEmpty(beginOp.trim()) ? DateTools.stringToDate(beginOp, DateTools.DATETIME_ITA_FORMAT_SLASH) : null;
        Date endTimeTx;
        Integer duration = null;
        if (getMapValue(record, Constants.DURATA_TX) != null && getMapValue(record, Constants.DURATA_TX).matches(StringTools.DURATION_FORMAT)) {
            duration = Integer.parseInt(StringTools.parseTimeToSeconds(getMapValue(record, Constants.DURATA_TX)));
        }
        endTimeTx = (beginTimeTx != null && duration != null) ? DateTools.manageDate(beginTimeTx, duration, Calendar.SECOND) : null;

        BdcRdShowMusic rdShowMusic = new BdcRdShowMusic(musicType, normalizedFile, beginTimeOp, record);

        BdcRdShowSchedule rdShowSchedule = new BdcRdShowSchedule(rdShowMusic, canale, normalizedFile, beginTimeTx, endTimeTx, record);
        //SET KPI PROPERTIES
        Date recordDate = beginTimeTx != null ? beginTimeTx : beginTimeOp;
        Calendar cal = Calendar.getInstance();
        cal.setTime(recordDate);
        rdShowSchedule.setSchedulYear(cal.get(Calendar.YEAR));
        rdShowSchedule.setScheduleMonth(cal.get(Calendar.MONTH) + 1);
        long diff = normalizedFile.getFirstOriginalReport().getDataUpload().getTime() - recordDate.getTime();
        rdShowSchedule.setDaysFromUpload((int) TimeUnit.MILLISECONDS.toDays(diff));
        rdShowMusic.setDaysFromUpload((int) TimeUnit.MILLISECONDS.toDays(diff));
        logger.debug("RADIO RECORD: {}", rdShowSchedule);
        return rdShowSchedule;
    }

    @Override
    public BdcErrRdShowSchedule fromMapToErrRdShow(Map<String, Object> record, BdcNormalizedFile normalizedFile) {
        MusicType musicType = null;
        if (getMapValue(record, Constants.DECODE_ID_CATEGORIA_USO_OP) != null) {
            musicType = decodeService.findMusicTypeById(Long.parseLong(getMapValue(record, Constants.DECODE_ID_CATEGORIA_USO_OP)));
        }
        BdcCanali canale = getMapValue(record, Constants.DECODE_ID_CANALE) != null ? canaliService.findById(new Integer(getMapValue(record, Constants.DECODE_ID_CANALE))) : null;
        String begin = getMapValue(record, Constants.DATA_INIZIO_TX);
        String timeTx = getMapValue(record, Constants.ORARIO_INIZIO_TX);
        String timeOp = getMapValue(record, Constants.ORARIO_INIZIO_OP);
        String beginTx = (StringUtils.isNotEmpty(begin) ? begin : "") + " " + (StringUtils.isNotEmpty(timeTx) && StringTools.parseTimeToSeconds(timeTx) != null ? timeTx : (StringUtils.isNotEmpty(timeOp) && StringTools.parseTimeToSeconds(timeOp) != null ? timeOp : "00:00:00"));
        String beginOp = begin + " " + timeOp;
        Date beginTimeTx = DateTools.stringToDate(beginTx, DateTools.DATETIME_ITA_FORMAT_SLASH);
        Date beginTimeOp = StringUtils.isNotEmpty(beginOp.trim()) ? DateTools.stringToDate(beginOp, DateTools.DATETIME_ITA_FORMAT_SLASH) : null;
        Date endTimeTx;
        Integer duration = null;
        if (getMapValue(record, Constants.DURATA_TX) != null && getMapValue(record, Constants.DURATA_TX).matches(StringTools.DURATION_FORMAT)) {
            duration = Integer.parseInt(StringTools.parseTimeToSeconds(getMapValue(record, Constants.DURATA_TX)));
        }
        endTimeTx = (beginTimeTx != null && duration != null) ? DateTools.manageDate(beginTimeTx, duration, Calendar.SECOND) : null;

        BdcErrRdShowMusic errRdShowMusic = new BdcErrRdShowMusic(musicType, normalizedFile, beginTimeOp, record);

        BdcErrRdShowSchedule errRdShowSchedule = new BdcErrRdShowSchedule(errRdShowMusic, canale, normalizedFile, beginTimeTx, endTimeTx, record);

        //SET KPI PROPERTIES
        Date recordDate = beginTimeTx != null ? beginTimeTx : beginTimeOp;
        if (recordDate != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(recordDate);
            errRdShowSchedule.setSchedulYear(cal.get(Calendar.YEAR));
            errRdShowSchedule.setScheduleMonth(cal.get(Calendar.MONTH) + 1);
            long diff = normalizedFile.getFirstOriginalReport().getDataUpload().getTime() - recordDate.getTime();
            errRdShowSchedule.setDaysFromUpload((int) TimeUnit.MILLISECONDS.toDays(diff));
            errRdShowMusic.setDaysFromUpload((int) TimeUnit.MILLISECONDS.toDays(diff));
        }
        logger.debug("RADIO ERROR: {}", errRdShowSchedule.toString());
        return errRdShowSchedule;
    }
}
