package com.alkemytech.sophia.broadcasting.dto;

import java.math.BigDecimal;

public class KPI {
    public final static String KPI_COPERTURA = "% Completezza Palinsesto";
    public final static String KPI_RECORD_IN_TEMPO = "% Record Inviati In Tempo";
    public final static String KPI_RECORD_ACCETTATI = "% Record Accettati";
    public final static String KPI_DURATA_MUSICA_DICHIARATA = "% Durata Musica Dichiarata";
    public final static String KPI_BRANI_TITOLI_ERRONEI = "% Brani con titoli erronei";
    public final static String KPI_AUTORI_TITOLI_ERRONEI = "% Autori con titoli erronei";
    public final static String KPI_FILM_TITOLI_ERRONEI = "% Film con titoli erronei";
    public final static String KPI_PROGRAMMI_TITOLI_ERRONEI = "% Programmi con titoli erronei";
    public final static String KPI_TRASMISSIONI_MUSICHE_ECCEDENTI = "% Trasmissioni con musiche eccedenti";
    public final static String KPI_DURATA_BRANI_DICHIARATI = "% Durata brani dichiarati";
    public final static String KPI_BRANI_RD_TITOLI_ERRONEI = "% Brani radio con titoli erronei";
    public final static String KPI_AUTORI_RD_TITOLI_ERRONEI = "% Autori radio con titoli erronei";

    public final static String DESC_KPI_COPERTURA = "Copertura del palinsesto rispetto alla durata disponibile";
    public final static String DESC_KPI_RECORD_IN_TEMPO = "Record inviati in tempo rispetto alle scadenze del DLSG 35/2017";
    public final static String DESC_KPI_RECORD_ACCETTATI = "Record conformi allo standard di caricamento dati";
    public final static String DESC_KPI_DURATA_MUSICA_DICHIARATA = "Percentuale della durata della musica dichiarata nei canali";
    public final static String DESC_KPI_BRANI_TITOLI_ERRONEI = "Percentuale di brani con titoli generici e chiaramente erronei";
    public final static String DESC_KPI_AUTORI_TITOLI_ERRONEI = "Percentuale di compositori con titoli generici e chiaramente erronei";
    public final static String DESC_KPI_FILM_TITOLI_ERRONEI = "Percentuale di film con titoli generici e chiaramente erronei";
    public final static String DESC_KPI_PROGRAMMI_TITOLI_ERRONEI = "Percentuale di programmi con titoli generici e chiaramente erronei";
    public final static String DESC_KPI_TRASMISSIONI_MUSICHE_ECCEDENTI = "Percentuale di trasmissioni il cui dettaglio musicale supera la durata della trasmissione";
    public final static String DESC_KPI_DURATA_BRANI_DICHIARATI = "Percentuale della durata dei brani dichiarata nella radio";
    public final static String DESC_KPI_BRANI_RD_TITOLI_ERRONEI = "Percentuale di brani radio con titoli generici e chiaramente erronei";
    public final static String DESC_KPI_AUTORI_RD_TITOLI_ERRONEI = "Percentuale di compositori radio con titoli generici e chiaramente erronei";

    private boolean valido;
    private String kpi;
    private String descKpi;
    private BigDecimal valore;

    public KPI() {
    }

    public KPI(boolean b, String kpiCopertura, String descKpiCopertura, Object o) {

    }

    public KPI(boolean valido, String kpi, String descKpi, BigDecimal valore) {
        this.valido = valido;
        this.kpi = kpi;
        this.descKpi = descKpi;
        this.valore = valore;
    }

    public String getKpi() {
        return kpi;
    }

    public void setKpi(String kpi) {
        this.kpi = kpi;
    }

    public BigDecimal getValore() {
        return valore;
    }

    public void setValore(BigDecimal valore) {
        this.valore = valore;
    }

    public boolean isValido() {
        return valido;
    }

    public void setValido(boolean valido) {
        this.valido = valido;
    }

    public String getDescKpi() {
        return descKpi;
    }

    public void setDescKpi(String descKpi) {
        this.descKpi = descKpi;
    }
}
