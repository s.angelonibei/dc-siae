package com.alkemytech.sophia.broadcasting.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.validation.Valid;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class RequestMonitoraggioKPIDTO extends RequestDTO implements Serializable {

    @Expose
    @Valid
    @XmlElement(name = "RequestData")
    @SerializedName("RequestData")
    private MonitoraggioKPIDTO requestData;
    @JsonProperty("RequestData")
    public MonitoraggioKPIDTO getRequestData() {
        return requestData;
    }

    public void setRequestData(MonitoraggioKPIDTO requestData) {
        this.requestData = requestData;
    }
}
