package com.alkemytech.sophia.broadcasting.dto;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BdcNewsRequestDTO {
    private Integer idUser;
    private Integer idBroadcaster;

    public BdcNewsRequestDTO() {
    }

    public BdcNewsRequestDTO(Integer idUser, Integer idBroadcaster) {
        this.idUser = idUser;
        this.idBroadcaster = idBroadcaster;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public Integer getIdBroadcaster() {
        return idBroadcaster;
    }

    public void setIdBroadcaster(Integer idBroadcaster) {
        this.idBroadcaster = idBroadcaster;
    }
}
