package com.alkemytech.sophia.broadcasting.model.harmonize;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the BDC_TV_SHOW_MUSIC database table.
 * 
 */
@Entity(name="TvShowMusic")
@Table(name="BDC_TV_SHOW_MUSIC")
@NamedQuery(name="TvShowMusic.findAll", query="SELECT b FROM TvShowMusic b")
public class BdcTvShowMusic implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_TV_SHOW_MUSIC", unique=true, nullable=false)
	private Integer idTvShowMusic;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATION_DATE", nullable=false)
	private Date creationDate;

	@Column(name="DAYS_FROM_UPLOAD", nullable=false)
	private Integer daysFromUpload;

	@Column(name="DURATION")
	private Integer duration;

	@Column(name="FIRST_COMPOSER", length=200)
	private String firstComposer;

	@Column(name="ID_MUSIC_TYPE", nullable=false)
	private java.math.BigInteger idMusicType;

	@Column(name="ID_NORMALIZED_FILE", nullable=false)
	private java.math.BigInteger idNormalizedFile;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="MODIFY_DATE")
	private Date modifyDate;

	@Column(name="NOTE", length=400)
	private String note;

	@Column(name="REAL_DURATION")
	private Integer realDuration;

	@Column(name="SECOND_COMPOSER", length=200)
	private String secondComposer;

	@Column(name="TITLE", length=200)
	private String title;

	@Column( name = "ALERT", length=200)
	private String alert;

	@Column( name = "REPLICA")
	private String replica;

	@Column( name = "PROD_COUNTRY")
	private String prodCountry;

	@Column( name = "PROD_YEAR")
	private String prodYear;

	@Column( name = "PRODUCTOR")
	private String productor;

	@Column( name = "DIRECTOR")
	private String director;

	@Column( name = "EPISODE_TITLE")
	private String episodeTitle;

	@Column( name = "EPISODE_ORIGINAL_TITLE")
	private String episodeOriginalTitle;

	@Column( name = "EPISODE_NUMBER")
	private Integer episodeNumber;

	@Column( name = "ORIGINAL_TITLE")
	private String originalTitle;

	//bi-directional many-to-one association to BdcTvShowSchedule
	@JsonBackReference
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_TV_SHOW_SCHEDULE")
	private BdcTvShowSchedule bdcTvShowSchedule;

	//R9-22
	@Column(name = "IDENTIFICATIVO_RAI")
	private String identificativoRai;

	public BdcTvShowMusic() {
	}

	public Integer getIdTvShowMusic() {
		return this.idTvShowMusic;
	}

	public void setIdTvShowMusic(Integer idTvShowMusic) {
		this.idTvShowMusic = idTvShowMusic;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Integer getDaysFromUpload() {
		return this.daysFromUpload;
	}

	public void setDaysFromUpload(Integer daysFromUpload) {
		this.daysFromUpload = daysFromUpload;
	}

	public Integer getDuration() {
		return this.duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public String getFirstComposer() {
		return this.firstComposer;
	}

	public void setFirstComposer(String firstComposer) {
		this.firstComposer = firstComposer;
	}

	public java.math.BigInteger getIdMusicType() {
		return this.idMusicType;
	}

	public void setIdMusicType(java.math.BigInteger idMusicType) {
		this.idMusicType = idMusicType;
	}

	public java.math.BigInteger getIdNormalizedFile() {
		return this.idNormalizedFile;
	}

	public void setIdNormalizedFile(java.math.BigInteger idNormalizedFile) {
		this.idNormalizedFile = idNormalizedFile;
	}

	public Date getModifyDate() {
		return this.modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getNote() {
		return this.note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Integer getRealDuration() {
		return this.realDuration;
	}

	public void setRealDuration(Integer realDuration) {
		this.realDuration = realDuration;
	}

	public String getSecondComposer() {
		return this.secondComposer;
	}

	public String getAlert() {
		return alert;
	}

	public void setAlert(String alert) {
		this.alert = alert;
	}

	public void setSecondComposer(String secondComposer) {
		this.secondComposer = secondComposer;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public BdcTvShowSchedule getBdcTvShowSchedule() {
		return this.bdcTvShowSchedule;
	}

	public void setBdcTvShowSchedule(BdcTvShowSchedule bdcTvShowSchedule) {
		this.bdcTvShowSchedule = bdcTvShowSchedule;
	}

	public String getReplica() {return replica;}

	public void setReplica(String replica) {this.replica = replica;}

	public String getProdCountry() {return prodCountry;}

	public void setProdCountry(String prodCountry) {this.prodCountry = prodCountry;}

	public String getProdYear() {return prodYear;}

	public void setProdYear(String prodYear) {this.prodYear = prodYear;}

	public String getProductor() { return productor;}

	public void setProductor(String productor) {this.productor = productor; }

	public String getDirector() {return director;}

	public void setDirector(String director) {this.director = director;}

	public String getEpisodeTitle() {return episodeTitle;}

	public void setEpisodeTitle(String episodeTitle) {this.episodeTitle = episodeTitle;}

	public String getEpisodeOriginalTitle() {return episodeOriginalTitle;}

	public void setEpisodeOriginalTitle(String episodeOriginalTitle) {this.episodeOriginalTitle = episodeOriginalTitle;}

	public Integer getEpisodeNumber() {return episodeNumber;}

	public void setEpisodeNumber(Integer episodeNumber) {this.episodeNumber = episodeNumber;}

	public String getOriginalTitle() {return originalTitle;}

	public void setOriginalTitle(String originalTitle) {this.originalTitle = originalTitle;}

	public String getIdentificativoRai() {
		return identificativoRai;
	}

	public void setIdentificativoRai(String identificativoRai) {
		this.identificativoRai = identificativoRai;
	}
}