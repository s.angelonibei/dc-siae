package com.alkemytech.sophia.broadcasting.dto;

import java.sql.Timestamp;

public class BdcNewsDTO {
	private Integer id;
    private String title;
    private String news;
    private Boolean flagForAllBdc;
    private Boolean flagForAllBdcUser;
    private Timestamp pubblicationDate;

    public BdcNewsDTO() {
    }

	public BdcNewsDTO(Integer id, String title, String news, Boolean flagForAllBdc, Boolean flagForAllBdcUser,
			Timestamp pubblicationDate) {
		super();
		this.id = id;
		this.title = title;
		this.news = news;
		this.flagForAllBdc = flagForAllBdc;
		this.flagForAllBdcUser = flagForAllBdcUser;
		this.pubblicationDate = pubblicationDate;
	}

	public BdcNewsDTO(Object obj){
        if(obj instanceof Object[]){
            Object[] notizia = (Object[]) obj;
            if (notizia.length == 6){
                id = (Integer)notizia[0];
                title = (String)notizia[1];
                news = (String)notizia[2];
                flagForAllBdc = (Boolean)notizia[3];
                flagForAllBdcUser = (Boolean)notizia[4];
                pubblicationDate = (Timestamp)notizia[5];
            }
        }
        System.out.println(obj);
    }

    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNews() {
        return news;
    }

    public void setNews(String news) {
        this.news = news;
    }

    public Boolean getFlagForAllBdc() {
        return flagForAllBdc;
    }

    public void setFlagForAllBdc(Boolean flagForAllBdc) {
        this.flagForAllBdc = flagForAllBdc;
    }

    public Boolean getFlagForAllBdcUser() {
        return flagForAllBdcUser;
    }

    public void setFlagForAllBdcUser(Boolean flagForAllBdcUser) {
        this.flagForAllBdcUser = flagForAllBdcUser;
    }

    public Timestamp getPubblicationDate() {
        return pubblicationDate;
    }

    public void setPubblicationDate(Timestamp pubblicationDate) {
        this.pubblicationDate = pubblicationDate;
    }
}
