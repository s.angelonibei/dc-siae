package com.alkemytech.sophia.broadcasting.dto;

import com.alkemytech.sophia.broadcasting.model.BdcBroadcasters;

public class BdcRequestStatoDTO {
    private BdcBroadcasters emittente;
    private String stato;
    private String repertorio;

    public String getRepertorio() {
        return repertorio;
    }

    public void setRepertorio(String repertorio) {
        this.repertorio = repertorio;
    }

    public BdcBroadcasters getEmittente() {
        return emittente;
    }

    public void setEmittente(BdcBroadcasters emittente) {
        this.emittente = emittente;
    }

    public String getStato() {
        return stato;
    }

    public void setStato(String stato) {
        this.stato = stato;
    }


}
