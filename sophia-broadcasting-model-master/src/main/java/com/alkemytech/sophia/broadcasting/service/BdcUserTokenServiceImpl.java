package com.alkemytech.sophia.broadcasting.service;

import java.util.Date;

import javax.persistence.EntityManager;

import org.apache.http.util.TextUtils;

import com.alkemytech.sophia.broadcasting.model.BdcUserToken;
import com.alkemytech.sophia.broadcasting.service.interfaces.BdcUserTokenService;
import com.google.inject.Inject;
import com.google.inject.Provider;

public class BdcUserTokenServiceImpl implements BdcUserTokenService {
	private final Provider<EntityManager> provider;

	@Inject
	public BdcUserTokenServiceImpl(Provider<EntityManager> provider) {
		this.provider = provider;
	}

	@Override
	public String createToken(Integer bdcUser, Date dataCreazione, Date dataFineValidita, Date dataLogin,
			String repertori, String repertorioDefault, String token, String tokenNavigazione, String user) {
		EntityManager entityManager = provider.get();
		BdcUserToken bdcUserToken = new BdcUserToken();
		bdcUserToken.setBdcUser(bdcUser);
		bdcUserToken.setDataCreazione(dataCreazione);
		bdcUserToken.setDataFineValidita(dataFineValidita);
		bdcUserToken.setDataLogin(dataLogin);
		if (repertori!=null && (TextUtils.isEmpty(repertorioDefault)||!repertori.contains(repertorioDefault))) {
			repertorioDefault = repertori.split(",")[0];
		} 
		bdcUserToken.setRepertori(repertori);
		bdcUserToken.setRepertorioDefault(repertorioDefault);
		bdcUserToken.setToken(token);
		bdcUserToken.setTokenNavigazione(tokenNavigazione);
		bdcUserToken.setUser(user);
		try {
			entityManager.getTransaction().begin();
			entityManager.persist(bdcUserToken);
			entityManager.getTransaction().commit();

			return token;
		} catch (Exception e) {
			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
			return null;
		}
	}

}
