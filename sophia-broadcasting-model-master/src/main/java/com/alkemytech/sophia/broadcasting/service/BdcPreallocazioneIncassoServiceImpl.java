package com.alkemytech.sophia.broadcasting.service;

import com.alkemytech.sophia.broadcasting.dto.BdcContiDTO;
import com.alkemytech.sophia.broadcasting.dto.BdcUpdatePianoContiRequestDTO;
import com.alkemytech.sophia.broadcasting.model.BdcCanali;
import com.alkemytech.sophia.broadcasting.model.BdcCaricoRipartizione;
import com.alkemytech.sophia.broadcasting.model.BdcCaricoRipartizione.TIPO_DIRITTO;
import com.alkemytech.sophia.broadcasting.model.BdcConto;
import com.alkemytech.sophia.broadcasting.model.BdcDettaglioPianoDeiConti;
import com.alkemytech.sophia.broadcasting.model.BdcPianoDeiConti;
import com.alkemytech.sophia.broadcasting.service.interfaces.BdcPreallocazioneIncassoService;
import com.google.inject.Inject;
import com.google.inject.Provider;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import java.math.BigDecimal;
import java.security.InvalidParameterException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BdcPreallocazioneIncassoServiceImpl implements BdcPreallocazioneIncassoService {

	private final Provider<EntityManager> provider;

	@Inject
	public BdcPreallocazioneIncassoServiceImpl(Provider<EntityManager> provider) {
		this.provider = provider;
	}

	@Override
	public List<BdcContiDTO> findAll(Date dataFrom, Date dataTo, String tipoEmittente, Integer idEmittente,
			Integer idCanale, String tipoDiritto, Integer first, Integer last, Integer maxrow) {

		List<BdcContiDTO> bdcContiDTOs = new ArrayList<>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		EntityManager em = null;
		em = provider.get();
		StringBuffer query = new StringBuffer();
		query.append(
				"select piano_dei_conti, id_broadcaster, tipo_emittente, emittente, group_concat(canale order by canale asc separator ', ') as lista_canali, tipo_diritto, periodo_da, periodo_a, conto, data_creazione, utente_ultima_modifica  "
						+ "from( " + "select  " + "	c.nome as canale, " + "    c.id as id_canale, "
						+ "	 pc.ID_BDC_PIANO_DEI_CONTI as piano_dei_conti, " + "    pc.CODICE_CONTO as conto, "
						+ "    pc.DATA_CREAZIONE as data_creazione, " + "    pc.UTENTE as utente_ultima_modifica, "
						+ "    co.PERIODO_DA as periodo_da, " + "    co.PERIODO_A as periodo_a, "
						+ "    co.TIPO_DIRITTO as tipo_diritto, " + "    b.nome as emittente, "
						+ "    b.id as id_broadcaster, " + "    b.tipo_broadcaster as tipo_emittente " + "from  "
						+ "	BDC_CONTO co, " + "    BDC_PIANO_DEI_CONTI pc, " + "	BDC_DETTAGLIO_PIANO_DEI_CONTI dpc, "
						+ "    bdc_canali c, " + "    bdc_broadcasters b " + "where co.CODICE_CONTO = pc.CODICE_CONTO "
						+ "and dpc.ID_PIANO_DEI_CONTI = pc.ID_BDC_PIANO_DEI_CONTI " + "and c.id = dpc.ID_CANALE "
						+ "and c.id_broadcaster = b.id " + "and pc.ATTIVO = 1 ");
		if (idCanale != null) {
			query.append("and exists( " + "	select *  " + "    from  " + "		BDC_PIANO_DEI_CONTI pc1, "
					+ "		BDC_DETTAGLIO_PIANO_DEI_CONTI dpc1 " + "        where  "
					+ "			dpc1.ID_PIANO_DEI_CONTI = pc1.ID_BDC_PIANO_DEI_CONTI "
					+ "            and pc1.ID_BDC_PIANO_DEI_CONTI = pc.ID_BDC_PIANO_DEI_CONTI "
					+ "			and dpc1.ID_CANALE = " + idCanale + " " + ")");
		}

		if (dataFrom != null) {
			query.append(" and co.PERIODO_DA >= " + "'" + sdf.format(dataFrom) + "'");
		}
		if (dataTo != null) {
			query.append(" and co.PERIODO_A <= " + "'" + sdf.format(dataTo) + "'");
		}
		if (tipoDiritto != null) {
			query.append(" and co.TIPO_DIRITTO = " + "'" + tipoDiritto + "'");
		}
		if (tipoEmittente != null) {
			query.append(" and b.tipo_broadcaster = " + "'" + tipoEmittente + "'");
		}
		if (idEmittente != null) {
			query.append(" and b.id = " + idEmittente);
		}

		query.append(") as info_piano_dei_conti " + "group by id_broadcaster, piano_dei_conti "
				+ "order by periodo_da desc, tipo_emittente desc, emittente asc;");
		List<Object[]> result = em.createNativeQuery(query.toString()).setFirstResult(first)
				.setMaxResults(1 + last - first).getResultList();

		for (Object[] record : result) {
			BdcContiDTO conto = new BdcContiDTO();
			conto.setPianoConti((Integer) record[0]);
			conto.setIdBroadcaster((Integer) record[1]);
			conto.setTipoEmittente((String) record[2]);
			conto.setEmittente((String) record[3]);
			conto.setListaCanali((String) record[4]);
			conto.setTipoDiritto((String) record[5]);
			java.sql.Date dateDa = (java.sql.Date) record[6];
			java.sql.Date dateA = (java.sql.Date) record[7];
			conto.setPeriodoDa(sdf.format(dateDa));
			conto.setPeriodoA(sdf.format(dateA));
			conto.setConto((String) record[8]);
			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			java.sql.Timestamp dateCreazione = (java.sql.Timestamp) record[9];
			conto.setDataCreazione(sdf1.format(dateCreazione));
			conto.setUtenteUltimaModifica((String) record[10]);
			bdcContiDTOs.add(conto);
		}

		return bdcContiDTOs;

	}

	@Override
	public BdcPianoDeiConti getPianoContoDettails(Integer idBdcPianoDeiConti) {
		EntityManager em = null;
		em = provider.get();
		Query q = em.createQuery("Select x From BdcPianoDeiConti x where x.idBdcPianoDeiConti=:idBdcPianoDeiConti")
				.setParameter("idBdcPianoDeiConti", idBdcPianoDeiConti);
		return (BdcPianoDeiConti) q.getSingleResult();
	}

	@Override
	public BdcPianoDeiConti updatePianoConti(BdcUpdatePianoContiRequestDTO bdcUpdatePianoContiRequestDTO) {
		EntityManager em = null;
		em = provider.get();
		em.getTransaction().begin();
		Query q1 = em.createQuery("Select x From BdcConto x where x.codiceConto=:codiceConto")
				.setParameter("codiceConto", bdcUpdatePianoContiRequestDTO.getConto());
		BdcConto bdcConto = (BdcConto) q1.getSingleResult();
		BdcPianoDeiConti olBdcPianoDeiConti = (BdcPianoDeiConti) em
				.createQuery("Select x From BdcPianoDeiConti x where x.idBdcPianoDeiConti=:idBdcPianoDeiConti")
				.setParameter("idBdcPianoDeiConti", bdcUpdatePianoContiRequestDTO.getIdPianoDeiConti())
				.getSingleResult();
		boolean hasChange = false;
		for (BdcDettaglioPianoDeiConti olBdcPianoDeiContiDettails : olBdcPianoDeiConti.getDettaglioPianoDeiConti()) {
			for (BdcDettaglioPianoDeiConti newBdcPianoDeiContiDettails : bdcUpdatePianoContiRequestDTO
					.getDettaglioPianoDeiConti()) {
				if (newBdcPianoDeiContiDettails.getIdBdcDettaglioPianoDeiConti()
						.equals(olBdcPianoDeiContiDettails.getIdBdcDettaglioPianoDeiConti())) {
					if (!olBdcPianoDeiContiDettails.getPercentualeAllocazione()
							.equals(newBdcPianoDeiContiDettails.getPercentualeAllocazione())) {
						hasChange = true;
					}
				}
			}
		}
		if (!hasChange) {
			throw new InvalidParameterException();
		}
		try {
			olBdcPianoDeiConti.setAttivo(false);
			em.merge(olBdcPianoDeiConti);
			BdcPianoDeiConti bdcPianoDeiConti = new BdcPianoDeiConti();
			bdcPianoDeiConti.setAttivo(true);
			bdcPianoDeiConti.setCodiceConto(bdcConto);
			bdcPianoDeiConti.setDataCreazione(new Date());
			bdcPianoDeiConti.setUtente(bdcUpdatePianoContiRequestDTO.getUtente());
			em.persist(bdcPianoDeiConti);
			em.flush();

			ArrayList<BdcDettaglioPianoDeiConti> conti = new ArrayList<>();
			for (BdcDettaglioPianoDeiConti bdcDettaglioConto : bdcUpdatePianoContiRequestDTO.getDettaglioPianoDeiConti()) {
				bdcDettaglioConto.setIdBdcDettaglioPianoDeiConti(null);
				bdcDettaglioConto.setPianoDeiConti(bdcPianoDeiConti);
				em.persist(bdcDettaglioConto);
				conti.add(bdcDettaglioConto);
			}
			em.flush();
			bdcPianoDeiConti.setDettaglioPianoDeiConti(conti);
			em.merge(bdcPianoDeiConti);
			updateCaricoDiRipartoDaConto(em, bdcConto.getCodiceConto());
			em.getTransaction().commit();
			return bdcPianoDeiConti;
		} catch (Exception e) {
			em.getTransaction().rollback();
			return null;
		}
		
	}

	@Override
	public Integer updateCaricoDiRipartoDaConto(EntityManager em, String numeroConto) {
		try {
			Query q1 = em.createQuery("Select x From BdcPianoDeiConti x where x.codiceConto.codiceConto=:numeroConto and x.attivo=:attivo")
					.setParameter("numeroConto", numeroConto).setParameter("attivo", true);
			BdcPianoDeiConti bdcPianoDeiConti = (BdcPianoDeiConti) q1.getSingleResult();
			for (BdcDettaglioPianoDeiConti bdcDettaglioPianoDeiConti : bdcPianoDeiConti.getDettaglioPianoDeiConti()) {
				try {
					Query q2 = em
							.createQuery("Select x From BdcCaricoRipartizione x where " + "x.canale.id=:idCanale and "
									+ "x.inizioPeriodoCompetenza=:inizioPeriodoCompetenza and "
									+ "x.finePeriodoCompetenza=:finePeriodoCompetenza and "
									+ "x.tipoDiritto=:tipoDiritto")
							.setParameter("idCanale", bdcDettaglioPianoDeiConti.getCanali().getId())
							.setParameter("inizioPeriodoCompetenza", bdcPianoDeiConti.getCodiceConto().getPeriodoDa())
							.setParameter("finePeriodoCompetenza", bdcPianoDeiConti.getCodiceConto().getPeriodoA())
							.setParameter("tipoDiritto", TIPO_DIRITTO.valueOf(bdcPianoDeiConti.getCodiceConto().getTipoDiritto()));
					BdcCaricoRipartizione caricoRipartizione = (BdcCaricoRipartizione) q2.getSingleResult();
					caricoRipartizione
							.setIncassoNetto(new BigDecimal(bdcDettaglioPianoDeiConti.getPercentualeAllocazione()
									* bdcPianoDeiConti.getCodiceConto().getImporto() / 100.0));
					em.merge(caricoRipartizione);
					em.flush();

				} catch (NoResultException e) {

				} catch (Exception e) {
					em.getTransaction().rollback();
					return 400;
				}
			}
			return 200;
		} catch (Exception e) {
			em.getTransaction().rollback();
			return 400;
		}
	}

	@Override
	public List<BdcContiDTO> getStoricoConto(String numeroConto) {

		List<BdcContiDTO> bdcContiDTOs = new ArrayList<>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		EntityManager em = null;
		em = provider.get();
		StringBuffer query = new StringBuffer();
		query.append(
				"select piano_dei_conti, id_broadcaster, tipo_emittente, emittente, group_concat(canale order by canale asc separator ', ') as lista_canali, tipo_diritto, periodo_da, periodo_a, conto , data_creazione , utente_ultima_modifica "
						+ "		from( " + "		select  " + "			c.nome as canale, "
						+ "			pc.ID_BDC_PIANO_DEI_CONTI as piano_dei_conti, "
						+ "		    pc.CODICE_CONTO as conto, " + "           pc.DATA_CREAZIONE as data_creazione, "
						+ "           pc.UTENTE as utente_ultima_modifica, "
						+ "		    co.PERIODO_DA as periodo_da, " + "		    co.PERIODO_A as periodo_a, "
						+ "		    co.TIPO_DIRITTO as tipo_diritto, " + "		    b.nome as emittente, "
						+ "		    b.id as id_broadcaster, " + "		    b.tipo_broadcaster as tipo_emittente "
						+ "		from  " + "			BDC_CONTO co, " + "		    BDC_PIANO_DEI_CONTI pc, "
						+ "			BDC_DETTAGLIO_PIANO_DEI_CONTI dpc, " + "		    bdc_canali c, "
						+ "		    bdc_broadcasters b " + "		where co.CODICE_CONTO = pc.CODICE_CONTO "
						+ "		and dpc.ID_PIANO_DEI_CONTI = pc.ID_BDC_PIANO_DEI_CONTI "
						+ "		and c.id = dpc.ID_CANALE " + "		and c.id_broadcaster = b.id "
						+ "		and pc.ATTIVO = 0 " + "		) as info_piano_dei_conti " + "		where conto = "
						+ numeroConto + "		group by id_broadcaster, piano_dei_conti "
						+ "		order by periodo_da desc, tipo_emittente desc, emittente asc;");
		List<Object[]> result = em.createNativeQuery(query.toString()).getResultList();

		for (Object[] record : result) {
			BdcContiDTO conto = new BdcContiDTO();
			conto.setPianoConti((Integer) record[0]);
			conto.setIdBroadcaster((Integer) record[1]);
			conto.setTipoEmittente((String) record[2]);
			conto.setEmittente((String) record[3]);
			conto.setListaCanali((String) record[4]);
			conto.setTipoDiritto((String) record[5]);
			java.sql.Date dateDa = (java.sql.Date) record[6];
			java.sql.Date dateA = (java.sql.Date) record[7];
			conto.setPeriodoDa(sdf.format(dateDa));
			conto.setPeriodoA(sdf.format(dateA));
			conto.setConto((String) record[8]);
			java.sql.Timestamp dateCreazione = (java.sql.Timestamp) record[9];
			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			conto.setDataCreazione(sdf1.format(dateCreazione));
			conto.setUtenteUltimaModifica((String) record[10]);
			bdcContiDTOs.add(conto);
		}

		return bdcContiDTOs;

	}

}