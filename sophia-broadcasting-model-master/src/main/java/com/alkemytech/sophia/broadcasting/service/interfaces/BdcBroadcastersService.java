package com.alkemytech.sophia.broadcasting.service.interfaces;

import com.alkemytech.sophia.broadcasting.model.BdcBroadcasterConfigTemplate;
import com.alkemytech.sophia.broadcasting.model.BdcBroadcasters;
import com.alkemytech.sophia.broadcasting.model.TipoBroadcaster;

import java.util.Date;
import java.util.List;

public interface BdcBroadcastersService {
	List<BdcBroadcasters> findAll();

	BdcBroadcasters findById(Integer id);

	List<BdcBroadcasters> findFiltered(String fiteredName, String fiteredType);

	boolean uploadImage(byte[] imageByte, Integer broadcasterId);

	boolean deleteImage(Integer broadcasterId);

	boolean addBroadcaster(String broadcasterName, TipoBroadcaster broadcasterType,
						   BdcBroadcasterConfigTemplate bdcBroadcasterConfigTemplate, String username, Date inizioValid,
						   Date fineValid, String nuovoTracciatoRai);
}
