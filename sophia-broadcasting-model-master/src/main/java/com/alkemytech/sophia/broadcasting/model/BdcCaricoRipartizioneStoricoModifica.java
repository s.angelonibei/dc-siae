package com.alkemytech.sophia.broadcasting.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@Table(name = "BDC_CARICO_RIPARTIZIONE_STORICO_MODIFICHE")
@XmlAccessorType(XmlAccessType.FIELD)
public class BdcCaricoRipartizioneStoricoModifica {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_CARICO_RIPARTIZIONE_STORICO_MODIFICHE", nullable = false)
	private Integer id;

	@Column(name = "ID_CARICO_RIPARTIZIONE", nullable = false)
	private Integer idCaricoRipartizione;

	@Column(name = "NOME_CAMPO", nullable = false)
	private String nomeCampo;

	@Column(name = "VALORE_OLD", nullable = false)
	private String valoreOld;

	@Column(name = "VALORE_NEW", nullable = false)
	private String valoreNew;

	@Column(name = "USERNAME", nullable = false)
	private String username;

	@Column(name = "DATA_MODIFICA", nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date dataModifica;

	public BdcCaricoRipartizioneStoricoModifica() {
		super();
	}

	public BdcCaricoRipartizioneStoricoModifica(Integer idCaricoRipartizione, String nomeCampo,
			String valoreOld, String valoreNew, String username, Date dataModifica) {
		super();
		this.idCaricoRipartizione = idCaricoRipartizione;
		this.nomeCampo = nomeCampo;
		this.valoreOld = valoreOld;
		this.valoreNew = valoreNew;
		this.username = username;
		this.dataModifica = dataModifica;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdCaricoRipartizione() {
		return idCaricoRipartizione;
	}

	public void setIdCaricoRipartizione(Integer idCaricoRipartizione) {
		this.idCaricoRipartizione = idCaricoRipartizione;
	}

	public String getNomeCampo() {
		return nomeCampo;
	}

	public void setNomeCampo(String nomeCampo) {
		this.nomeCampo = nomeCampo;
	}

	public String getValoreOld() {
		return valoreOld;
	}

	public void setValoreOld(String valoreOld) {
		this.valoreOld = valoreOld;
	}

	public String getValoreNew() {
		return valoreNew;
	}

	public void setValoreNew(String valoreNew) {
		this.valoreNew = valoreNew;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Date getDataModifica() {
		return dataModifica;
	}

	public void setDataModifica(Date dataModifica) {
		this.dataModifica = dataModifica;
	}

}
