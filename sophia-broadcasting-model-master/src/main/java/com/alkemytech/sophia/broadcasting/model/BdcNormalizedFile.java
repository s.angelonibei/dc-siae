package com.alkemytech.sophia.broadcasting.model;

import com.alkemytech.sophia.broadcasting.consts.Consts;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Alessandro Russo on 09/12/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "BDC_NORMALIZED_FILE")
public class BdcNormalizedFile implements Serializable{

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "ID_NORMALIZED_FILE", nullable = false)
    private Long id;

    @Column(name = "FILE_NAME")
    private String fileName;

    @Column(name = "FILE_BUCKET")
    private String fileBucket;

    @Column(name = "CREATION_DATE")
    private Date creationDate;

    @OneToMany( fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
            name = "BDC_UTILIZATION_NORMALIZED_FILE",
            joinColumns = @JoinColumn( name = "ID_NORMALIZED_FILE"),
            inverseJoinColumns = @JoinColumn(name = "ID_UTILIZATION_FILE" )
    )
    private List<BdcInformationFileEntity> originalReport;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileBucket() {
        return fileBucket;
    }

    public void setFileBucket(String fileBucket) {
        this.fileBucket = fileBucket;
    }

    public List<BdcInformationFileEntity> getOriginalReport() {
        if( originalReport == null){
            originalReport = new ArrayList<>();
        }
        return originalReport;
    }

    public void setOriginalReport(List<BdcInformationFileEntity> originalReport) {
        this.originalReport = originalReport;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    //SHORTCUT
    public BdcInformationFileEntity getFirstOriginalReport(){
        if( getOriginalReport().size() > 0 ){
            return  getOriginalReport().get(0);
        }
        return null;
    }

    public BdcBroadcasters getBroadcaster(){
        if( getOriginalReport().size() > 0 ){
            return  getOriginalReport().get(0).getEmittente();
        }
        return null;
    }

    public BdcCanali getCanale(){
        if(getOriginalReport().size() > 0 ){
            return getOriginalReport().get(0).getCanale();
        }
        return null;
    }

    public String getUploadType(){
        if( getOriginalReport().size() > 0 ){
            return getOriginalReport().get(0).getTipoUpload();
        }
        return null;
    }

    public boolean isSingleUpload(){
        return  Consts.SINGLE_UPLOAD.equals(this.getUploadType());
    }

    @Override
    public String toString() {
        return "BdcNormalizedFile{" +
                "id=" + id +
                ", fileName='" + fileName + '\'' +
                ", fileBucket='" + fileBucket + '\'' +
                ", creationDate=" + creationDate +
                ", originalReport=" + originalReport +
                '}';
    }
}
