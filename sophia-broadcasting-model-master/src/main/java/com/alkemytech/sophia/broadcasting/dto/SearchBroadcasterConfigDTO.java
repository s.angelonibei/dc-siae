package com.alkemytech.sophia.broadcasting.dto;

import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

/**
 * Created by Alessandro Russo on 27/11/2017.
 */
@Produces("application/json")
@XmlRootElement
public class SearchBroadcasterConfigDTO {

    private Integer idBroadcaster;
    private Integer idChannel;
    private String activeDate;

    public SearchBroadcasterConfigDTO() {
    }

    public SearchBroadcasterConfigDTO(Integer idBroadcaster, Integer idChannel) {
        this.idBroadcaster = idBroadcaster;
        this.idChannel = idChannel;
    }

    public Integer getIdBroadcaster() {
        return idBroadcaster;
    }

    public void setIdBroadcaster(Integer idBroadcaster) {
        this.idBroadcaster = idBroadcaster;
    }

    public Integer getIdChannel() {
        return idChannel;
    }

    public void setIdChannel(Integer idChannel) {
        this.idChannel = idChannel;
    }

    public String getActiveDate() {
        return activeDate;
    }

    public void setActiveDate(String activeDate) {
        this.activeDate = activeDate;
    }

    @Override
    public String toString() {
        return "SearchBroadcasterConfigDTO{" +
                "idBroadcaster=" + idBroadcaster +
                ", idChannel=" + idChannel +
                ", activeDate='" + activeDate + '\'' +
                '}';
    }
}
