package com.alkemytech.sophia.broadcasting.dto;

import com.alkemytech.sophia.broadcasting.deserialized.MultiTimeDeserializer;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonPropertyOrder({"Canale", "Genere trasmissione", "Titolo trasmissione", "Titolo originale trasmissione", "Data inizio trasmissione", "Orario inizio trasmissione", "Orario fine trasmissione", "Durata trasmissione", "Replica o prima esecuzione", "Paese di produzione", "Anno di produzione", "Produttore", "Regista", "Titolo episodio", "Titolo originale episodio", "Numero progressivo episodio", "Titolo opera musicale", "Compositore/Autore (1)", "Compositore/Autore (2)", "Durata effettiva di utilizzo", "Categoria uso", "Diffusione regionale", "Id Canale", "Id Genere TX", "Desc Genere TX", "Decode Data inizio Tx", "Decode Orario inizio Tx", "Decode Orario Fine Tx", "Decode Durata Tx", "Decode Anno produzione Tx", "Decode Durata effettiva Op", "Decode Orario inizio Op", "Decode Id Categoria Uso", " "})

public class NormalizedOrderDTO implements Serializable {
    @JsonProperty("Canale")
    String Canale;
    @JsonProperty("Genere trasmissione")
    String GenereTrasmissione;
    @JsonProperty("Titolo trasmissione")
    String TitoloTrasmissione;
    @JsonProperty("Titolo originale trasmissione")
    String TitoloOriginaleTrasmissione;

    @JsonProperty("Data inizio trasmissione")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    Date DataInizioTrasmissione;
    //@JsonProperty("Orario inizio trasmissione")

    @JsonProperty("Orario inizio trasmissione")
    //@JsonDeserialize(using = MultiTimeDeserializer.class)
    //@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
    String OrarioInizioTrasmissione;
    //@JsonProperty("Orario fine trasmissione")

    @JsonProperty("Orario fine trasmissione")
    //@JsonDeserialize(using = MultiTimeDeserializer.class)
    //@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
    String OrarioFineTrasmissione;

    @JsonProperty("Durata trasmissione")
    String DurataTrasmissione;
    @JsonProperty("Replica o prima esecuzione")
    String ReplicaPrimaEsecuzione;
    @JsonProperty("Paese di produzione")
    String PaeseDiProduzione;
    @JsonProperty("Anno di produzione")
    String AnnoDiProduzione;
    @JsonProperty("Produttore")
    String Produttore;
    @JsonProperty("Regista")
    String Regista;
    @JsonProperty("Titolo episodio")
    String TitoloEpisodio;
    @JsonProperty("Titolo originale episodio")
    String TitoloOriginaleEpisodio;
    @JsonProperty("Numero progressivo episodio")
    String NumeroProgressivoEpisodio;
    @JsonProperty("Titolo opera musicale")
    String TitoloOperaMusicale;
    @JsonProperty("Compositore/Autore (1)")
    String CompositoreAutore1;
    @JsonProperty("Compositore/Autore (2)")
    String CompositoreAutore2;
    @JsonProperty("Durata effettiva di utilizzo")
    String DurataEffettivaDiUtilizzo;
    @JsonProperty("Categoria uso")
    String CategoriaUso;
    @JsonProperty("Diffusione regionale")
    String DiffusioneRegionale;
    @JsonProperty("Id Canale")
    String IdCanale;
    @JsonProperty("Id Genere TX")
    String IdGenereTX;
    @JsonProperty("Desc Genere TX")
    String DescGenereTX;
    @JsonProperty("Decode Data inizio Tx")
    String DecodeDatainizioTx;
    @JsonProperty("Decode Orario inizio Tx")
    String DecodeOrarioinizioTx;
    @JsonProperty("Decode Orario Fine Tx")
    String DecodeOrarioFineTx;
    @JsonProperty("Decode Durata Tx")
    String DecodeDurataTx;
    @JsonProperty("Decode Anno produzione Tx")
    String DecodeAnnoProduzioneTx;
    @JsonProperty("Decode Durata effettiva Op")
    String DecodeDurataEffettivaOp;
    @JsonProperty("Decode Orario inizio Op")
    String DecodeOrarioInizioOp;
    @JsonProperty("Decode Id Categoria Uso")
    String DecodeIdCategoriaUso;
    @JsonProperty("Identificativo Rai")
    String IdentificativoRai;
    @JsonProperty(" ")
    @JsonAlias("")
    String lastCol;
}

