package com.alkemytech.sophia.broadcasting.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alessandro Russo on 06/12/2017.
 */
public class BdcConfigDTO {
    //GENERAL
    String  supportedFormat;    //supported file format for input file (regex)
    Integer expectedFiles;      //number of expected files
    String  fileValidator;      //class for validate input file

    //PARSER CONFIG (BEANIO)
    String  parseConfigPath;    //configuration files path for parsing input file ( |(pipe) separated )
    String  parseStreamName;    //configuration streamnames for parsing input file ( |(pipe) separated )
    String  fileParser;         //class for parsing file

    //NORMALIZATION AND VALIDATION (DROOLS)
    String  normalizeRulePath;  //rules files path for mapping from original to normalized ( |(pipe) separated )
    String  validateRulePath;   //rules files path for validate fields ( |(pipe) separated )

    //WRITE NORMALIZED FILE (BEANIO)
    String writeConfigPath;     //configuration file path for write normalized file
    String writeConfigStream;   //configuration stramname for write normalized file
    String writeTemplateBucket; //Amazon S3 bucket where empty template is located
    String writeUploadBucket;   //Amazon S3 bucket where upload normalized file
    String writeFileExtension;  //File extension of normalized File;

    public String getSupportedFormat() {
        return supportedFormat;
    }

    public void setSupportedFormat(String supportedFormat) {
        this.supportedFormat = supportedFormat;
    }

    public String getParseConfigPath() {
        return parseConfigPath;
    }

    public void setParseConfigPath(String parseConfigPath) {
        this.parseConfigPath = parseConfigPath;
    }

    public String getParseStreamName() {
        return parseStreamName;
    }

    public void setParseStreamName(String parseStreamName) {
        this.parseStreamName = parseStreamName;
    }

    public Integer getExpectedFiles() {
        if( expectedFiles == null ){
            expectedFiles = 0;
        }
        return expectedFiles;
    }

    public void setExpectedFiles(Integer expectedFiles) {
        this.expectedFiles = expectedFiles;
    }

    public String getFileValidator() {
        return fileValidator;
    }

    public void setFileValidator(String fileValidator) {
        this.fileValidator = fileValidator;
    }

    public String getFileParser() {
        return fileParser;
    }

    public void setFileParser(String fileParser) {
        this.fileParser = fileParser;
    }

    public String getNormalizeRulePath() {
        return normalizeRulePath;
    }

    public void setNormalizeRulePath(String normalizedRulePath) {
        this.normalizeRulePath = normalizedRulePath;
    }

    public String getValidateRulePath() {
        return validateRulePath;
    }

    public void setValidateRulePath(String validateRulePath) {
        this.validateRulePath = validateRulePath;
    }

    public String getWriteConfigPath() {
        return writeConfigPath;
    }

    public void setWriteConfigPath(String writeConfigPath) {
        this.writeConfigPath = writeConfigPath;
    }

    public String getWriteConfigStream() {
        return writeConfigStream;
    }

    public void setWriteConfigStream(String writeConfigStream) {
        this.writeConfigStream = writeConfigStream;
    }

    public String getWriteTemplateBucket() {
        return writeTemplateBucket;
    }

    public void setWriteTemplateBucket(String writeTemplateBucket) {
        this.writeTemplateBucket = writeTemplateBucket;
    }

    public String getWriteUploadBucket() {
        return writeUploadBucket;
    }

    public void setWriteUploadBucket(String writeUploadBucket) {
        this.writeUploadBucket = writeUploadBucket;
    }

    public String getWriteFileExtension() {
        return writeFileExtension;
    }

    public void setWriteFileExtension(String writeFileExtension) {
        this.writeFileExtension = writeFileExtension;
    }

    @Override
    public String toString() {
        return "BdcConfigDTO{" +
                "supportedFormat='" + supportedFormat + '\'' +
                ", expectedFiles=" + expectedFiles +
                ", fileValidator='" + fileValidator + '\'' +
                ", parseConfigPath='" + parseConfigPath + '\'' +
                ", parseStreamName='" + parseStreamName + '\'' +
                ", fileParser='" + fileParser + '\'' +
                ", normalizeRulePath='" + normalizeRulePath + '\'' +
                ", validateRulePath='" + validateRulePath + '\'' +
                ", writeConfigPath='" + writeConfigPath + '\'' +
                ", writeConfigStream='" + writeConfigStream + '\'' +
                ", writeTemplateBucket='" + writeTemplateBucket + '\'' +
                ", writeUploadBucket='" + writeUploadBucket + '\'' +
                ", writeFileExtension='" + writeFileExtension + '\'' +
                '}';
    }

}