package com.alkemytech.sophia.broadcasting.service.interfaces;

import java.util.List;

import com.alkemytech.sophia.broadcasting.model.BdcRuoli;

public interface BdcRuoliService {

	List<BdcRuoli> findAll();

}
