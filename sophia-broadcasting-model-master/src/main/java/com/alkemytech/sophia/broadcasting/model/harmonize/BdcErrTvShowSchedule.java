package com.alkemytech.sophia.broadcasting.model.harmonize;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the BDC_ERR_TV_SHOW_SCHEDULE database table.
 * 
 */
@Entity(name="ErrTvShowSchedule")
@Table(name="BDC_ERR_TV_SHOW_SCHEDULE")
@NamedQuery(name="ErrTvShowSchedule.findAll", query="SELECT b FROM ErrTvShowSchedule b")
public class BdcErrTvShowSchedule implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_ERR_TV_SHOW_SCHEDULE", unique=true, nullable=false)
	private Integer idErrTvShowSchedule;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="BEGIN_TIME")
	private Date beginTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATION_DATE", nullable=false)
	private Date creationDate;

	@Column(name="DAYS_FROM_UPLOAD")
	private Integer daysFromUpload;

	@Column(name="DIRECTOR", length=400)
	private String director;

	@Column(name="DURATION")
	private Integer duration;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="END_TIME")
	private Date endTime;

	@Column(name="EPISODE_NUMBER")
	private Integer episodeNumber;

	@Column(name="EPISODE_ORIGINAL_TITLE", length=400)
	private String episodeOriginalTitle;

	@Column(name="EPISODE_TITLE", length=400)
	private String episodeTitle;

	@Column(name="ERROR")
	private String error;

	@Column(name="GLOBAL_ERROR", length=400)
	private String globalError;

	@Column(name="ID_NORMALIZED_FILE", nullable=false)
	private java.math.BigInteger idNormalizedFile;

	@Column(name="ID_SHOW_TYPE")
	private java.math.BigInteger idShowType;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="MODIFY_DATE")
	private Date modifyDate;

	@Column(name="ORIGINAL_TITLE", length=400)
	private String originalTitle;

	@Column(name="PROD_COUNTRY", length=400)
	private String prodCountry;

	@Column(name="PROD_YEAR")
	private Integer prodYear;

	@Column(name="PRODUCTOR", length=400)
	private String productor;

	@Column(name="REGIONAL_OFFICE", length=10)
	private String regionalOffice;

	@Column(name="REPLICA", length=4)
	private String replica;

	@Column(name="REPORT_BEGIN_DATE", length=100)
	private String reportBeginDate;

	@Column(name="REPORT_BEGIN_TIME", length=100)
	private String reportBeginTime;

	@Column(name="REPORT_CHANNEL", length=100)
	private String reportChannel;

	@Column(name="REPORT_DURATION", length=100)
	private String reportDuration;

	@Column(name="REPORT_END_TIME", length=100)
	private String reportEndTime;

	@Column(name="REPORT_EPISODE_NUMBER", length=100)
	private String reportEpisodeNumber;

	@Column(name="REPORT_PROD_YEAR", length=100)
	private String reportProdYear;

	@Column(name="REPORT_SHOW_TYPE", length=100)
	private String reportShowType;

	@Column(name="SCHEDULE_MONTH")
	private Integer scheduleMonth;

	@Column(name="SCHEDULE_YEAR")
	private Integer scheduleYear;

	@Column(name="TITLE", length=400)
	private String title;

	//bi-directional many-to-one association to BdcErrTvShowMusic
	@JsonManagedReference
	@OneToMany(mappedBy="bdcErrTvShowSchedule")
	private List<BdcErrTvShowMusic> bdcErrTvShowMusics;

	//bi-directional many-to-one association to BdcCanali
	@JsonBackReference
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CHANNEL")
	private BdcCanali bdcCanali;

	public BdcErrTvShowSchedule() {
	}

	public Integer getIdErrTvShowSchedule() {
		return this.idErrTvShowSchedule;
	}

	public void setIdErrTvShowSchedule(Integer idErrTvShowSchedule) {
		this.idErrTvShowSchedule = idErrTvShowSchedule;
	}

	public Date getBeginTime() {
		return this.beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Integer getDaysFromUpload() {
		return this.daysFromUpload;
	}

	public void setDaysFromUpload(Integer daysFromUpload) {
		this.daysFromUpload = daysFromUpload;
	}

	public String getDirector() {
		return this.director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public Integer getDuration() {
		return this.duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Date getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Integer getEpisodeNumber() {
		return this.episodeNumber;
	}

	public void setEpisodeNumber(Integer episodeNumber) {
		this.episodeNumber = episodeNumber;
	}

	public String getEpisodeOriginalTitle() {
		return this.episodeOriginalTitle;
	}

	public void setEpisodeOriginalTitle(String episodeOriginalTitle) {
		this.episodeOriginalTitle = episodeOriginalTitle;
	}

	public String getEpisodeTitle() {
		return this.episodeTitle;
	}

	public void setEpisodeTitle(String episodeTitle) {
		this.episodeTitle = episodeTitle;
	}

	public String getError() {
		return this.error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getGlobalError() {
		return this.globalError;
	}

	public void setGlobalError(String globalError) {
		this.globalError = globalError;
	}

	public java.math.BigInteger getIdNormalizedFile() {
		return this.idNormalizedFile;
	}

	public void setIdNormalizedFile(java.math.BigInteger idNormalizedFile) {
		this.idNormalizedFile = idNormalizedFile;
	}

	public java.math.BigInteger getIdShowType() {
		return this.idShowType;
	}

	public void setIdShowType(java.math.BigInteger idShowType) {
		this.idShowType = idShowType;
	}

	public Date getModifyDate() {
		return this.modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getOriginalTitle() {
		return this.originalTitle;
	}

	public void setOriginalTitle(String originalTitle) {
		this.originalTitle = originalTitle;
	}

	public String getProdCountry() {
		return this.prodCountry;
	}

	public void setProdCountry(String prodCountry) {
		this.prodCountry = prodCountry;
	}

	public Integer getProdYear() {
		return this.prodYear;
	}

	public void setProdYear(Integer prodYear) {
		this.prodYear = prodYear;
	}

	public String getProductor() {
		return this.productor;
	}

	public void setProductor(String productor) {
		this.productor = productor;
	}

	public String getRegionalOffice() {
		return this.regionalOffice;
	}

	public void setRegionalOffice(String regionalOffice) {
		this.regionalOffice = regionalOffice;
	}

	public String getReplica() {
		return this.replica;
	}

	public void setReplica(String replica) {
		this.replica = replica;
	}

	public String getReportBeginDate() {
		return this.reportBeginDate;
	}

	public void setReportBeginDate(String reportBeginDate) {
		this.reportBeginDate = reportBeginDate;
	}

	public String getReportBeginTime() {
		return this.reportBeginTime;
	}

	public void setReportBeginTime(String reportBeginTime) {
		this.reportBeginTime = reportBeginTime;
	}

	public String getReportChannel() {
		return this.reportChannel;
	}

	public void setReportChannel(String reportChannel) {
		this.reportChannel = reportChannel;
	}

	public String getReportDuration() {
		return this.reportDuration;
	}

	public void setReportDuration(String reportDuration) {
		this.reportDuration = reportDuration;
	}

	public String getReportEndTime() {
		return this.reportEndTime;
	}

	public void setReportEndTime(String reportEndTime) {
		this.reportEndTime = reportEndTime;
	}

	public String getReportEpisodeNumber() {
		return this.reportEpisodeNumber;
	}

	public void setReportEpisodeNumber(String reportEpisodeNumber) {
		this.reportEpisodeNumber = reportEpisodeNumber;
	}

	public String getReportProdYear() {
		return this.reportProdYear;
	}

	public void setReportProdYear(String reportProdYear) {
		this.reportProdYear = reportProdYear;
	}

	public String getReportShowType() {
		return this.reportShowType;
	}

	public void setReportShowType(String reportShowType) {
		this.reportShowType = reportShowType;
	}

	public Integer getScheduleMonth() {
		return this.scheduleMonth;
	}

	public void setScheduleMonth(Integer scheduleMonth) {
		this.scheduleMonth = scheduleMonth;
	}

	public Integer getScheduleYear() {
		return this.scheduleYear;
	}

	public void setScheduleYear(Integer scheduleYear) {
		this.scheduleYear = scheduleYear;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<BdcErrTvShowMusic> getBdcErrTvShowMusics() {
		return this.bdcErrTvShowMusics;
	}

	public void setBdcErrTvShowMusics(List<BdcErrTvShowMusic> bdcErrTvShowMusics) {
		this.bdcErrTvShowMusics = bdcErrTvShowMusics;
	}

	public BdcErrTvShowMusic addBdcErrTvShowMusic(BdcErrTvShowMusic bdcErrTvShowMusic) {
		getBdcErrTvShowMusics().add(bdcErrTvShowMusic);
		bdcErrTvShowMusic.setBdcErrTvShowSchedule(this);

		return bdcErrTvShowMusic;
	}

	public BdcErrTvShowMusic removeBdcErrTvShowMusic(BdcErrTvShowMusic bdcErrTvShowMusic) {
		getBdcErrTvShowMusics().remove(bdcErrTvShowMusic);
		bdcErrTvShowMusic.setBdcErrTvShowSchedule(null);

		return bdcErrTvShowMusic;
	}

	public BdcCanali getBdcCanali() {
		return this.bdcCanali;
	}

	public void setBdcCanali(BdcCanali bdcCanali) {
		this.bdcCanali = bdcCanali;
	}

}