package com.alkemytech.sophia.broadcasting.enums;


public enum UtilizationField {
	CANALE("canale"),
	GENERE_TX("genereTx"),
    TITOLO_TX("titoloTx"),
	TITOLO_ORIGINALE_TX("titoloOriginaleTx"),
	DATA_INIZIO_TX("dataInizioTx"),
	ORARIO_INIZIO_TX("orarioInizioTx"),
	ORARIO_FINE_TX("orarioFineTx"),
	DURATA_TX("durataTx"),
	REPLICA_TX("replicaTx"),
	PAESE_PRODUZIONE_TX("paeseProduzioneTx"),
	ANNO_PRODUZIONE_TX("annoProduzioneTx"),
	PRODUTTORE_TX("produttoreTx"),
	REGISTA_TX("registaTx"),
	TITOLO_EPISODIO_TX("titoloEpisodioTx"),
	TITOLO_ORIGINALE_EPISODIO_TX("titoloOriginaleEpisodioTx"),
	NUM_PROGRESSIVO_EPISODIO_TX("numProgressivoEpisodioTx"),
	TITOLO_OP("titoloOp"),
	COMPOSITORE1_OP("compositore1Op"),
	COMPOSITORE2_OP("compositore2Op"),
	DURATA_OP("durataOp"),
	DURATA_EFFETTIVA_OP("durataEffettivaOp"),
	CATEGORIA_USO_OP("categoriaUsoOp"),
	DIFFUSIONE_REGIONALE("diffusioneRegionale"),
	DECODE_ID_CANALE("DecodeIdCanale"),
	DECODE_ID_GENERE_TX("DecodiIdGenereTx"),
	DECODE_DESC_GENERE_TX("DecodeGenereTx"),
	DECODE_DATA_INIZIO_TX("DecodeDataInizioTx"),
	DECODE_ORARIO_INIZIO_TX("DecodeOrarioInizioTx"),
	DECODE_ORARIO_FINE_TX("DecodeOrarioFineTx"),
	DECODE_DURATA_TX("DecodeDurataTx"),
	DECODE_ANNO_PRODUZIONE_TX("DecodeAnnoProduzioneTx"),
	DECODE_DURATA_OP("DecodeDurataOp"),
	DECODE_DURATA_EFFETTIVA_OP("DecodeDurataEffettivaOp"),
	DECODE_ORARIO_INIZIO_OP("DecodeOrarioInizioOp"),
	DECODE_ID_CATEGORIA_USO_OP("DecodeIdCategoriaUsoOp"),
	NOME_EMITTENTE		("nomeEmittente"),
	DATA_DIFFUSIONE_TX	("dataDiffusioneTx"),
	COMPOSITORE_OP		("compositoreOp"),
	ESECUTORE_OP			("esecutoreOp"),
	ALBUM_OP				("albumOp"),
	CODICE_ISWC_OP		("codiceIswcOp"),
	CODICE_ISRC_OP		("codiceIsrcOp"),
	ORARIO_INIZIO_OP		("orarioInizioOp"),
	DECODE_DESC_CATEGORIA_USO_OP	("decodeCategoriaUsoOp"),
	DECODE_IDENTIFICATIVO_RAI("IdentificativoRai");
   
	String text;

    UtilizationField(String text) {
        this.text = text;
    }


    @Override
    public String toString() {
        return text;
    }

    public static UtilizationField valueOfDescription(String description) {

    	UtilizationField stato=null;

        for (UtilizationField v : values()) {

            if (v.toString().equals(description)) {
                stato = v;
            }
        }

        return stato;
    }
	
	//NORMALIZED FILE PROPERTY
		// -- original fields

}
