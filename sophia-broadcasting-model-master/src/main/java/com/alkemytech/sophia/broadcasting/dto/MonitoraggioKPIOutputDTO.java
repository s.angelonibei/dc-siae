package com.alkemytech.sophia.broadcasting.dto;

import com.alkemytech.sophia.broadcasting.model.BdcCanali;

import java.util.Date;
import java.util.List;

public class MonitoraggioKPIOutputDTO {
    private Date lastUpdate;
    private int kpiAttesi;
    private BdcCanali bdcCanali;
    private List<KPI> listaKPI;
    private List<BdcInformationFileDTO> fileDaElaborare;

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public int getKpiAttesi() {
        return kpiAttesi;
    }

    public void setKpiAttesi(int kpiAttesi) {
        this.kpiAttesi = kpiAttesi;
    }

    public List<KPI> getListaKPI() {
        return listaKPI;
    }

    public void setListaKPI(List<KPI> listaKPI) {
        this.listaKPI = listaKPI;
    }

    public List<BdcInformationFileDTO> getFileDaElaborare() {
        return fileDaElaborare;
    }

    public void setFileDaElaborare(List<BdcInformationFileDTO> fileDaElaborare) {
        this.fileDaElaborare = fileDaElaborare;
    }

    public BdcCanali getBdcCanali() {
        return bdcCanali;
    }

    public void setBdcCanali(BdcCanali bdcCanali) {
        this.bdcCanali = bdcCanali;
    }
}
