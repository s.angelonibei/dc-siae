package com.alkemytech.sophia.broadcasting.dto;


public class BdcScheduleFieldDTO {

    private String nomeCampo;
    private String valoreCampo;
    private String errori;
    private Boolean editabile;

    public BdcScheduleFieldDTO() {
    }

    public BdcScheduleFieldDTO(String nomeCampo, String valoreCampo, String errori, Boolean editabile) {
        this.nomeCampo = nomeCampo;
        this.valoreCampo = valoreCampo;
        this.errori = errori;
        this.editabile = editabile;
    }

    public String getNomeCampo() {
        return nomeCampo;
    }

    public void setNomeCampo(String nomeCampo) {
        this.nomeCampo = nomeCampo;
    }

    public String getValoreCampo() {
        return valoreCampo;
    }

    public void setValoreCampo(String valoreCampo) {
        this.valoreCampo = valoreCampo;
    }

    public String getErrori() {
        return errori;
    }

    public void setErrori(String errori) {
        this.errori = errori;
    }

    public Boolean getEditabile() {
        return editabile;
    }

    public void setEditabile(Boolean editabile) {
        this.editabile = editabile;
    }
}
