package com.alkemytech.sophia.broadcasting.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.broadcasting.model.BdcUtenti;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BdcGenerateUserTokenRequestDto {
	private String username;
	private Integer bdcUserId;

	public BdcGenerateUserTokenRequestDto() {
		super();
	}

	public BdcGenerateUserTokenRequestDto(String username, Integer bdcUserId) {
		super();
		this.username = username;
		this.bdcUserId = bdcUserId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Integer getBdcUserId() {
		return bdcUserId;
	}

	public void setBdcUserId(Integer bdcUserId) {
		this.bdcUserId = bdcUserId;
	}


}
