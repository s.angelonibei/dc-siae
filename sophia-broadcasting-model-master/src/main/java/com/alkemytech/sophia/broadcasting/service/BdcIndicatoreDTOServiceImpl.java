package com.alkemytech.sophia.broadcasting.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.lang.StringUtils;

import com.alkemytech.sophia.broadcasting.data.IndicatoriPerCanale;
import com.alkemytech.sophia.broadcasting.dto.IndicatoreDTO;
import com.alkemytech.sophia.broadcasting.dto.RequestMonitoraggioKPIDTO;
import com.alkemytech.sophia.broadcasting.model.BdcBroadcasters;
import com.alkemytech.sophia.broadcasting.model.BdcCanali;
import com.alkemytech.sophia.broadcasting.model.BdcKpiEntity;
import com.alkemytech.sophia.broadcasting.model.TipoBroadcaster;
import com.alkemytech.sophia.broadcasting.service.interfaces.BdcCanaliService;
import com.alkemytech.sophia.broadcasting.service.interfaces.BdcIndicatoreDTOService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BdcIndicatoreDTOServiceImpl implements BdcIndicatoreDTOService {

    private final Provider<EntityManager> provider;
    private final BdcCanaliService bdcCanaliService;
    Logger logger = LoggerFactory.getLogger(getClass());

    @Inject
    public BdcIndicatoreDTOServiceImpl(Provider<EntityManager> provider, BdcCanaliService bdcCanaliService) {
        this.provider = provider;
        this.bdcCanaliService = bdcCanaliService;
    }

    @SuppressWarnings("unchecked")
	@Override
    public List<IndicatoreDTO> listaInd(RequestMonitoraggioKPIDTO monitoraggioKPI) {

        EntityManager entityManager = provider.get();
        List<IndicatoreDTO> listaIndicatori;


        String q = "select new com.alkemytech.sophia.broadcasting.dto.IndicatoreDTO (b.indicatore, sum(b.valore), max(b.lastUpdate)) " +
                "from BdcKpiEntity b " +
                "WHERE b.emittente.id=:emittenteId " +
                "AND b.canale.id IN :canaleId " +
                "AND b.anno=:anno " +
                "AND b.mese IN :mese " +
                "group by b.indicatore";

        TypedQuery<IndicatoreDTO> queryKPI = entityManager.createQuery(q, IndicatoreDTO.class);

        listaIndicatori = queryKPI
                .setParameter("emittenteId", monitoraggioKPI.getRequestData().getEmittente().getId())
                .setParameter("canaleId", monitoraggioKPI.getRequestData().getCanali())
                .setParameter("anno", monitoraggioKPI.getRequestData().getAnno())
                .setParameter("mese", monitoraggioKPI.getRequestData().getMesi())
                .getResultList();

        return listaIndicatori;
    }

    @Override
    public List<BdcKpiEntity> listaIndicatori(Integer emittenteId, Integer canaleId, Integer anno, Integer mese) {
        if(emittenteId == null || canaleId == null || anno == null || mese == null){
            return new ArrayList<BdcKpiEntity>();
        }
        EntityManager entityManager = provider.get();
        List<BdcKpiEntity> listaIndicatori;


        String q = "select b " +
                "from BdcKpiEntity b " +
                "WHERE b.emittente.id = :emittenteId " +
                "AND b.canale.id = :canaleId " +
                "AND b.anno=:anno " +
                "AND b.mese=:mese ";

        TypedQuery<BdcKpiEntity> queryKPI = entityManager.createQuery(q, BdcKpiEntity.class);

        listaIndicatori = queryKPI
                .setParameter("emittenteId", emittenteId)
                .setParameter("canaleId", canaleId)
                .setParameter("anno", anno)
                .setParameter("mese", mese)
                .getResultList();

        return listaIndicatori;
    }

    @Override
    public Map<String, BdcKpiEntity> mapIndicatori(Integer emittenteId, Integer canaleId, Integer anno, Integer mese) {
        Map<String, BdcKpiEntity> mapIndicatori = new HashMap<String, BdcKpiEntity>();

        List<BdcKpiEntity> listaIndicatori = listaIndicatori(emittenteId, canaleId, anno, mese);
        for(BdcKpiEntity indicatore : listaIndicatori){
            if(StringUtils.isNotBlank(indicatore.getIndicatore())){
                mapIndicatori.put(indicatore.getIndicatore(), indicatore);
            }
        }
        return mapIndicatori;
    }

    @Override
    public boolean saveIndicatori(List<BdcKpiEntity> listaIndicatoriDaPersistere) {
        EntityManager em = provider.get();
        try{
            em.getTransaction().begin();
            for(BdcKpiEntity indicatore : listaIndicatoriDaPersistere){
                if(indicatore != null){
                    em.merge(indicatore);
                    em.flush();
                }else{
                    throw new Exception();
                }
            }
            em.getTransaction().commit();
            return true;
        }catch(Exception e){
            if(em.getTransaction().isActive()){
                em.getTransaction().rollback();
            }
        }
        return false;
    }

    @Override
    public boolean evaluateIndicatoriTv(Integer idBroadcaster, Integer idCanale, Integer anno, Integer mese) {
    	if(idBroadcaster == null ||
    	   idCanale == null ||
    	   anno == null ||
    	   mese == null || mese < 1 || mese > 12)
    	{
    		return false;
    	}

        Long durataTrasmissioni = calcolaDurataTrasmissioni(idCanale, anno, mese);
        if(durataTrasmissioni == null){
            return false;
        }
        Long durataPublicita = calcolaDurataPublicita(idCanale, anno, mese);
        if(durataPublicita == null){
            return false;
        }
        Long durataFilm = calcolaDurataFilm(idCanale, anno, mese);
        if(durataFilm == null){
            return false;
        }

        Long durataAudiovisiviContenuti = calcolaDurataAudiovisiviContenuti(idCanale, anno, mese);
        if(durataAudiovisiviContenuti == null){
            return false;
        }

      /*  Long durataGeneriVuoti = calcolaDurataGeneriTrasmissioneVuoti(idCanale, anno, mese);
        if(durataGeneriVuoti == null){
            return false;
        }*/

        Long recordScartati = calcolaRecordScartati(idCanale, anno, mese);
        if(recordScartati == null){
            return false;
        }
        Long recordTotali = calcolaRecordTotali(idCanale, anno, mese);
        if(recordTotali == null){
            return false;
        }
        Long recordInRitardo = calcolaInRitardo(idCanale, anno, mese);
        if(recordInRitardo == null){
            return false;
        }

        Long durataBrani = calcolaDurataBrani(idCanale, anno, mese);
        if(durataBrani == null){
            return false;
        }

        Long durataBraniDichiarati = calcolaDurataBraniDichiarati(idCanale, anno, mese);
        if(durataBraniDichiarati == null){
            return false;
        }

        Long durataBraniTitoliErronei = calcolaDurataBraniTitoliErronei(idCanale, anno, mese);
        if(durataBraniTitoliErronei == null){
            return false;
        }

        Long durataBraniAutoriErronei = calcolaDurataAutoriTitoliErronei(idCanale, anno, mese);
        if(durataBraniAutoriErronei == null){
            return false;
        }

        Long durataFilmTitoliErronei = calcolaFilmTitoliErronei(idCanale, anno, mese);
        if(durataFilmTitoliErronei == null){
            return false;
        }

        Long durataProgrammiTitoliErronei = calcolaProgrammiTitoliErronei(idCanale, anno, mese);
        if(durataProgrammiTitoliErronei == null){
            return false;
        }

        Long trasmissioniMusicaEccedente = calcolaTrasmissioniMusicheEccedenti(idCanale, anno, mese);
        if(trasmissioniMusicaEccedente == null){
            return false;
        }

        Long trasmissioniTotali = calcolaTrasmissioniTotali(idCanale, anno, mese);
        if(trasmissioniTotali == null){
            return false;
        }

        Map<String, Long> mapIndicatoriRicalcolati = new HashMap<String, Long>();
        mapIndicatoriRicalcolati.put(BdcKpiEntity.DURATA_TRASMISSIONE, durataTrasmissioni);
        mapIndicatoriRicalcolati.put(BdcKpiEntity.DURATA_PUBBLICITA, durataPublicita);
        mapIndicatoriRicalcolati.put(BdcKpiEntity.DURATA_FILM, durataFilm);
        mapIndicatoriRicalcolati.put(BdcKpiEntity.DURATA_AUDIOVISIVI_CONTENUTI, durataAudiovisiviContenuti);
        //mapIndicatoriRicalcolati.put(BdcKpiEntity.DURATA_GENERI_VUOTI, durataGeneriVuoti);
        mapIndicatoriRicalcolati.put(BdcKpiEntity.RECORD_SCARTATI, recordScartati);
        mapIndicatoriRicalcolati.put(BdcKpiEntity.RECORD_TOTALI, recordTotali);
        mapIndicatoriRicalcolati.put(BdcKpiEntity.RECORD_IN_RITARDO, recordInRitardo);
        mapIndicatoriRicalcolati.put(BdcKpiEntity.DURATA_BRANI, durataBrani);
        mapIndicatoriRicalcolati.put(BdcKpiEntity.DURATA_BRANI_DICHIARATI, durataBraniDichiarati);
        mapIndicatoriRicalcolati.put(BdcKpiEntity.TRASMISSIONI_MUSICA_ECCEDENTE, trasmissioniMusicaEccedente);
        mapIndicatoriRicalcolati.put(BdcKpiEntity.TRASMISSIONI_TOTALI, trasmissioniTotali);
        mapIndicatoriRicalcolati.put(BdcKpiEntity.DURATA_BRANI_TITOLI_ERRONEI, durataBraniTitoliErronei);
        mapIndicatoriRicalcolati.put(BdcKpiEntity.DURATA_BRANI_AUTORI_ERRONEI, durataBraniAutoriErronei);
        mapIndicatoriRicalcolati.put(BdcKpiEntity.DURATA_FILM_TITOLI_ERRONEI, durataFilmTitoliErronei);
        mapIndicatoriRicalcolati.put(BdcKpiEntity.DURATA_PROGRAMMI_TITOLI_ERRONEI, durataProgrammiTitoliErronei);



        Map<String, BdcKpiEntity> mapIndicatori = mapIndicatori(idBroadcaster, idCanale, anno, mese);

        List<BdcKpiEntity> listaIndicatoriDaPersistere =
                listaIndicatoriDaPersistere(mapIndicatori, mapIndicatoriRicalcolati, idBroadcaster, idCanale, anno, mese);

        return saveIndicatori(listaIndicatoriDaPersistere);

    }

    @Override
    public boolean evaluateIndicatoriRadio(Integer idBroadcaster, Integer idCanale, Integer anno, Integer mese) {
        if(idBroadcaster == null ||
                idCanale == null ||
                anno == null ||
                mese == null || mese < 1 || mese > 12)
        {
            return false;
        }

        Long recordScartati = calcolaRecordScartati(idCanale, anno, mese);
        if(recordScartati == null){
            return false;
        }
        Long recordTotali = calcolaRecordTotali(idCanale, anno, mese);
        if(recordTotali == null){
            return false;
        }
        Long recordInRitardo = calcolaInRitardo(idCanale, anno, mese);
        if(recordInRitardo == null){
            return false;
        }

        Long durataBrani = calcolaDurataBrani(idCanale, anno, mese);
        if(durataBrani == null){
            return false;
        }

        Long durataBraniTitoliErronei = calcolaDurataBraniTitoliErronei(idCanale, anno, mese);
        if(durataBraniTitoliErronei == null){
            return false;
        }

        Long durataBraniAutoriErronei = calcolaDurataAutoriTitoliErronei(idCanale, anno, mese);
        if(durataBraniAutoriErronei == null){
            return false;
        }

        Map<String, Long> mapIndicatoriRicalcolati = new HashMap<String, Long>();
        mapIndicatoriRicalcolati.put(BdcKpiEntity.RECORD_SCARTATI, recordScartati);
        mapIndicatoriRicalcolati.put(BdcKpiEntity.RECORD_TOTALI, recordTotali);
        mapIndicatoriRicalcolati.put(BdcKpiEntity.RECORD_IN_RITARDO, recordInRitardo);
        mapIndicatoriRicalcolati.put(BdcKpiEntity.DURATA_BRANI, durataBrani);
        mapIndicatoriRicalcolati.put(BdcKpiEntity.DURATA_BRANI_TITOLI_ERRONEI, durataBraniTitoliErronei);
        mapIndicatoriRicalcolati.put(BdcKpiEntity.DURATA_BRANI_AUTORI_ERRONEI, durataBraniAutoriErronei);

        Map<String, BdcKpiEntity> mapIndicatori = mapIndicatori(idBroadcaster, idCanale, anno, mese);

        List<BdcKpiEntity> listaIndicatoriDaPersistere =
                listaIndicatoriDaPersistere(mapIndicatori, mapIndicatoriRicalcolati, idBroadcaster, idCanale, anno, mese);

        return saveIndicatori(listaIndicatoriDaPersistere);

    }

    private List<BdcKpiEntity> listaIndicatoriDaPersistere(
            Map<String, BdcKpiEntity> mapIndicatori, Map<String, Long> mapIndicatoriRicalcolati,
            Integer idBroadcaster, Integer idCanale, Integer anno, Integer mese)
    {
        List<BdcKpiEntity> listaIndicatoriDaPersistere = new ArrayList<BdcKpiEntity>();

        Set<String> keySet = mapIndicatoriRicalcolati.keySet();
        for(String keyIndicatore : keySet){
            BdcKpiEntity indicatore = null;
            Long valoreIndicatore = mapIndicatoriRicalcolati.get(keyIndicatore);
            if(mapIndicatori.containsKey(keyIndicatore)){
                indicatore = mapIndicatori.get(keyIndicatore);
                //aggiorno il valore dell'indicatore e la data di ultima modifica
                indicatore.setValore(valoreIndicatore);
                indicatore.setLastUpdate(new Date());
            }else{
                indicatore = new BdcKpiEntity();
                BdcBroadcasters emittente = new BdcBroadcasters();
                emittente.setId(idBroadcaster);
                BdcCanali canale = new BdcCanali();
                canale.setId(idCanale);

                indicatore.setEmittente(emittente);
                indicatore.setCanale(canale);
                indicatore.setAnno(anno);
                indicatore.setMese(mese);
                indicatore.setIndicatore(keyIndicatore);
                indicatore.setValore(valoreIndicatore);
                indicatore.setLastUpdate(new Date());
            }
            listaIndicatoriDaPersistere.add(indicatore);
        }
        return listaIndicatoriDaPersistere;
    }

    private Long calcolaInRitardo(Integer idCanale, Integer anno, Integer mese) {
		EntityManager entityManager = provider.get();
        String q;
        BdcCanali canale = bdcCanaliService.findById(idCanale);
        if (TipoBroadcaster.TELEVISIONE.equals(canale.getTipoCanale())) {
            q =
                      " SELECT count(*) "
                    + " FROM BDC_TV_SHOW_SCHEDULE t LEFT JOIN BDC_TV_SHOW_MUSIC m ON t.ID_TV_SHOW_SCHEDULE = m.ID_TV_SHOW_SCHEDULE "
                    + " WHERE t.ID_CHANNEL = ?1 "
                    + " AND t.SCHEDULE_YEAR = ?2 "
                    + "	AND t.SCHEDULE_MONTH = ?3 "
                    + " AND t.DAYS_FROM_UPLOAD > ?4"
                    + " AND t.REGIONAL_OFFICE IS NULL "
                    + " AND t.ID_TV_SHOW_SCHEDULE = m.ID_TV_SHOW_SCHEDULE ";
        } else {
            q = "SELECT count(*) "
                    + " FROM BDC_RD_SHOW_SCHEDULE t LEFT JOIN BDC_RD_SHOW_MUSIC m ON t.ID_RD_SHOW_SCHEDULE = m.ID_RD_SHOW_SCHEDULE "
                    + " WHERE t.ID_CHANNEL = ?1 "
                    + " AND t.SCHEDULE_YEAR = ?2"
                    + "	AND t.SCHEDULE_MONTH = ?3 "
                    + " AND t.DAYS_FROM_UPLOAD > ?4"
                    + " AND t.REGIONAL_OFFICE IS NULL ";
        }

	    Query query = entityManager.createNativeQuery(q);
	
	    Object ret = query.setParameter(1, idCanale)
	    		.setParameter(2, anno)
	            .setParameter(3, mese)
	            .setParameter(4, 90)
	            .getSingleResult();
	    
	    Long recordInRitardo = new Long(0); 
	    if(ret != null && ret instanceof Long){
	    	recordInRitardo = (Long)ret;
	    }    	
	    return recordInRitardo;
    }

    private Long calcolaRecordTotali(Integer idCanale, Integer anno, Integer mese) {
    	EntityManager entityManager = provider.get();
    	String q;
        BdcCanali canale = bdcCanaliService.findById(idCanale);
        if (TipoBroadcaster.TELEVISIONE.equals(canale.getTipoCanale())) {
    	        q = " select count(*) "
    			+ " FROM BDC_TV_SHOW_SCHEDULE t left join BDC_TV_SHOW_MUSIC m on t.ID_TV_SHOW_SCHEDULE = m.ID_TV_SHOW_SCHEDULE "
    			+ " WHERE t.ID_CHANNEL = ?1 "
    			+ " AND t.SCHEDULE_YEAR = ?2 "
    			+ "	AND t.SCHEDULE_MONTH = ?3 "
                + " AND t.REGIONAL_OFFICE IS NULL "
                + " AND t.ID_TV_SHOW_SCHEDULE = m.ID_TV_SHOW_SCHEDULE ";
        }else {
                q = "select count(*) "
                + " FROM BDC_RD_SHOW_SCHEDULE t left join BDC_RD_SHOW_MUSIC m on t.ID_RD_SHOW_SCHEDULE = m.ID_RD_SHOW_SCHEDULE "
                + " WHERE t.ID_CHANNEL = ?1 "
                + " AND t.SCHEDULE_YEAR = ?2"
                + "	AND t.SCHEDULE_MONTH = ?3"
                + " AND t.REGIONAL_OFFICE IS NULL ";
        }

        Query query = entityManager.createNativeQuery(q);

        Object ret = query.setParameter(1, idCanale)
        		.setParameter(2, anno)
                .setParameter(3, mese)
                .getSingleResult();
        
        Long recordValidi = new Long(0); 
        if(ret != null && ret instanceof Long){
        	recordValidi = (Long)ret;
        }    	
        return recordValidi;
    }

    private Long calcolaRecordScartati(Integer idCanale, Integer anno,Integer mese) {
    	EntityManager entityManager = provider.get();
        String q;
        BdcCanali canale = bdcCanaliService.findById(idCanale);
        if (TipoBroadcaster.TELEVISIONE.equals(canale.getTipoCanale())) {
            q =       " SELECT COUNT(*) FROM( "
                    + " SELECT DISTINCT t.ID_ERR_TV_SHOW_SCHEDULE "
                    + " FROM BDC_ERR_TV_SHOW_SCHEDULE t "
                    + " LEFT JOIN BDC_ERR_TV_SHOW_MUSIC m "
                    + " ON t.ID_ERR_TV_SHOW_SCHEDULE = m.ID_ERR_TV_SHOW_SCHEDULE "
                    + " LEFT JOIN BDC_UTILIZATION_NORMALIZED_FILE z "
                    + " ON t.ID_NORMALIZED_FILE = z.ID_NORMALIZED_FILE "
                    + " LEFT JOIN bdc_utilization_file b "
                    + " ON z.ID_UTILIZATION_FILE = b.id "
                    + " WHERE ((t.ID_CHANNEL = ?1 AND b.canale IS NULL) OR b.canale = ?1) "
                    + " AND ((t.SCHEDULE_YEAR  = ?2 AND b.anno IS NULL) OR b.anno = ?2) "
                    + "	AND ((t.SCHEDULE_MONTH = ?3 AND b.mese IS NULL) OR b.mese = ?3) "
                    + " AND t.REGIONAL_OFFICE IS NULL "
                    + " GROUP BY t.ID_ERR_TV_SHOW_SCHEDULE) AS conteggio ";
        } else {
            q =       " SELECT COUNT(*) FROM( "
                    + " SELECT DISTINCT t.ID_ERR_RD_SHOW_SCHEDULE "
                    + " FROM BDC_ERR_RD_SHOW_SCHEDULE t "
                    + " LEFT JOIN BDC_ERR_RD_SHOW_MUSIC m "
                    + " ON t.ID_ERR_RD_SHOW_SCHEDULE = m.ID_ERR_RD_SHOW_SCHEDULE "
                    + " LEFT JOIN BDC_UTILIZATION_NORMALIZED_FILE z "
                    + " ON t.ID_NORMALIZED_FILE = z.ID_NORMALIZED_FILE "
                    + " LEFT JOIN bdc_utilization_file b "
                    + " ON z.ID_UTILIZATION_FILE = b.id "
                    + " WHERE ((t.ID_CHANNEL = ?1 AND b.canale IS NULL) OR b.canale = ?1) "
                    + " AND ((t.SCHEDULE_YEAR  = ?2 AND b.anno IS NULL) OR b.anno = ?2) "
                    + "	AND ((t.SCHEDULE_MONTH = ?3 AND b.mese IS NULL) OR b.mese = ?3) "
                    + " AND t.REGIONAL_OFFICE IS NULL "
                    + " GROUP BY t.ID_ERR_RD_SHOW_SCHEDULE) as conteggio ";
        }
        Query query = entityManager.createNativeQuery(q);

        Object ret = query.setParameter(1, idCanale)
        		.setParameter(2, anno)
                .setParameter(3, mese)
                .getSingleResult();
        
        Long recordScartati = new Long(0); 
        if(ret != null && ret instanceof Long){
        	recordScartati = (Long)ret;
        }    	
        return recordScartati;
    }

    private Long calcolaDurataBrani(Integer idCanale, Integer anno, Integer mese) {
        EntityManager entityManager = provider.get();
        String q;
        BdcCanali canale = bdcCanaliService.findById(idCanale);
        if (TipoBroadcaster.TELEVISIONE.equals(canale.getTipoCanale())) {
            q = "SELECT sum(st.REAL_DURATION) "
                    + " FROM BDC_TV_SHOW_SCHEDULE t, BDC_TV_SHOW_MUSIC st, BDC_MUSIC_TYPE_CATEGORY c "
                    + " WHERE t.ID_TV_SHOW_SCHEDULE = st.ID_TV_SHOW_SCHEDULE "
                    + " AND c.ID_MUSIC_TYPE = st.ID_MUSIC_TYPE "
                    + " AND t.ID_CHANNEL = ?1 "
                    + " AND t.SCHEDULE_YEAR = ?2 "
                    + "	AND t.SCHEDULE_MONTH = ?3 "
                    //+ " AND c.CATEGORY IN ('SHOW TV','PUBBLICITA') "
                    + " AND c.CATEGORY = 'SHOW TV' "
                    + " AND t.REGIONAL_OFFICE IS NULL ";
        } else {
            q = "SELECT sum(st.DURATION) "
                    + " FROM BDC_RD_SHOW_SCHEDULE t, BDC_RD_SHOW_MUSIC st "
                    + " WHERE t.ID_RD_SHOW_SCHEDULE = st.ID_RD_SHOW_SCHEDULE "
                    + " AND t.ID_CHANNEL = ?1 "
                    + " AND t.SCHEDULE_YEAR = ?2 "
                    + "	AND t.SCHEDULE_MONTH = ?3 "
                    + "	AND st.ID_MUSIC_TYPE != 5"
                    + " AND t.REGIONAL_OFFICE IS NULL ";
        }

        Query query = entityManager.createNativeQuery(q);

        Object ret = query.setParameter(1, idCanale)
                .setParameter(2, anno)
                .setParameter(3, mese)
                .getSingleResult();

        Long durata = new Long(0);
        if(ret != null && ret instanceof BigDecimal){
            durata = ((BigDecimal)ret).longValue();
        }
        return durata;
    }

    private Long calcolaDurataBraniDichiarati(Integer idCanale, Integer anno, Integer mese) {
        EntityManager entityManager = provider.get();

           String q = "SELECT sum(st.REAL_DURATION) "
                    + " FROM BDC_TV_SHOW_SCHEDULE t, BDC_TV_SHOW_MUSIC st "
                    + " WHERE t.ID_TV_SHOW_SCHEDULE = st.ID_TV_SHOW_SCHEDULE "
                    + " AND t.ID_CHANNEL = ?1 "
                    + " AND t.SCHEDULE_YEAR = ?2 "
                    + "	AND t.SCHEDULE_MONTH = ?3 "
                    //+ " AND t.FLAG_CONTENUTI = 0 "
                    + " AND t.REGIONAL_OFFICE IS NULL ";

        Query query = entityManager.createNativeQuery(q);

        Object ret = query.setParameter(1, idCanale)
                .setParameter(2, anno)
                .setParameter(3, mese)
                .getSingleResult();

        Long durata = new Long(0);
        if(ret != null && ret instanceof BigDecimal){
            durata = ((BigDecimal)ret).longValue();
        }
        return durata;
    }

    private Long calcolaDurataBraniTitoliErronei(Integer idCanale, Integer anno, Integer mese) {

        // INTERVENTO DI OTTIMIZZAZIONE QUERY 28/11/2020

        EntityManager entityManager = provider.get();
        String q;
        BdcCanali canale = bdcCanaliService.findById(idCanale);

        logger.info("ID CANALE ==>",idCanale);
        logger.info("ANNO ==>",anno);
        logger.info("MESE ==>",mese);

        logger.info("BDC_CANALI {}",canale.toString());
        if (TipoBroadcaster.TELEVISIONE.equals(canale.getTipoCanale())) {
            q = "SELECT sum(BDC_TV_SHOW_MUSIC.REAL_DURATION) "
                    //+ " FROM BDC_TV_ERR_BRANO_TITOLO, BDC_TV_SHOW_SCHEDULE "
                    + " FROM BDC_TV_SHOW_SCHEDULE "
                    + " LEFT JOIN BDC_TV_SHOW_MUSIC "
                    + " ON BDC_TV_SHOW_SCHEDULE.ID_TV_SHOW_SCHEDULE = BDC_TV_SHOW_MUSIC.ID_TV_SHOW_SCHEDULE "
                    + " WHERE BDC_TV_SHOW_SCHEDULE.ID_CHANNEL = ?1 "
                    + " AND BDC_TV_SHOW_SCHEDULE.SCHEDULE_YEAR = ?2 "
                    + "	AND BDC_TV_SHOW_SCHEDULE.SCHEDULE_MONTH = ?3 "
                    + " AND BDC_TV_SHOW_SCHEDULE.REGIONAL_OFFICE IS NULL "
                   // + " AND LOWER(BDC_TV_SHOW_MUSIC.TITLE) IN (LOWER(BDC_TV_ERR_BRANO_TITOLO.TV_ERR_BRANO_TITOLO)) ";
                    + " AND EXISTS (select 1 from BDC_TV_ERR_BRANO_TITOLO where LOWER(BDC_TV_SHOW_MUSIC.TITLE) = LOWER(BDC_TV_ERR_BRANO_TITOLO.TV_ERR_BRANO_TITOLO)) ";
        } else {
            q = "SELECT sum(BDC_RD_SHOW_MUSIC.DURATION) "
                    //+ " FROM BDC_RD_ERR_BRANO_TITOLO, BDC_RD_SHOW_SCHEDULE "
                    + " FROM BDC_RD_SHOW_SCHEDULE "
                    + " LEFT JOIN BDC_RD_SHOW_MUSIC "
                    + " ON BDC_RD_SHOW_SCHEDULE.ID_RD_SHOW_SCHEDULE = BDC_RD_SHOW_MUSIC.ID_RD_SHOW_SCHEDULE "
                    + " WHERE BDC_RD_SHOW_SCHEDULE.ID_CHANNEL = ?1 "
                    + " AND BDC_RD_SHOW_SCHEDULE.SCHEDULE_YEAR = ?2 "
                    + "	AND BDC_RD_SHOW_SCHEDULE.SCHEDULE_MONTH = ?3 "
                    + " AND BDC_RD_SHOW_SCHEDULE.REGIONAL_OFFICE IS NULL "
                   // + " AND LOWER(BDC_RD_SHOW_MUSIC.TITLE) IN (LOWER(BDC_RD_ERR_BRANO_TITOLO.RD_ERR_BRANO_TITOLO)) ";
                    + " AND EXISTS ( SELECT 1 FROM BDC_RD_ERR_BRANO_TITOLO WHERE LOWER(BDC_RD_SHOW_MUSIC.TITLE) =  LOWER(BDC_RD_ERR_BRANO_TITOLO.RD_ERR_BRANO_TITOLO))";
        }

        Query query = entityManager.createNativeQuery(q);

        Object ret = query.setParameter(1, idCanale)
                .setParameter(2, anno)
                .setParameter(3, mese)
                .getSingleResult();

        Long durata = new Long(0);
        if(ret != null && ret instanceof BigDecimal){
            durata = ((BigDecimal)ret).longValue();
        }
        return durata;
    }

    private Long calcolaDurataFilm(Integer idCanale, Integer anno, Integer mese) {
    	EntityManager entityManager = provider.get();

     /*	String q = " select sum(t.DURATION) "
                + " FROM BDC_TV_SHOW_SCHEDULE t, "
                + "(select  distinct t.ID_TV_SHOW_SCHEDULE "
    			+ " FROM BDC_TV_SHOW_SCHEDULE t, BDC_TV_SHOW_MUSIC m, BDC_MUSIC_TYPE_CATEGORY c "
    			+ " WHERE t.ID_TV_SHOW_SCHEDULE = m.ID_TV_SHOW_SCHEDULE "
                + " AND c.ID_MUSIC_TYPE = m.ID_MUSIC_TYPE "
                //+ " AND t.ID_SHOW_TYPE IS NOT NULL "
                + " AND t.ID_CHANNEL = ?1 "
    			+ " AND t.SCHEDULE_YEAR = ?2 "
    			+ "	AND t.SCHEDULE_MONTH = ?3 "
    			+ " AND c.CATEGORY IN ('FILM', 'TELEFILM') "
                + " AND t.FLAG_CONTENUTI = 0 "
                + " AND t.REGIONAL_OFFICE IS NULL) m "
                + " WHERE t.ID_TV_SHOW_SCHEDULE = m.ID_TV_SHOW_SCHEDULE ";


      */
        // ottimizzazione query 26/11/2020

        String q =  " SELECT SUM(T1.DURATION)  " +
                    " FROM  BDC_TV_SHOW_SCHEDULE T1 " +
                    " WHERE T1.ID_CHANNEL = ?1 " +
                    " AND T1.SCHEDULE_YEAR = ?2 " +
                    " AND T1.SCHEDULE_MONTH = ?3 " +
                    " AND T1.REGIONAL_OFFICE IS NULL " +
                    " AND T1.FLAG_CONTENUTI = 0 " +
                    " AND EXISTS ( SELECT 1 " +
                    " FROM BDC_TV_SHOW_SCHEDULE T, BDC_TV_SHOW_MUSIC M, BDC_MUSIC_TYPE_CATEGORY C" +
                    " WHERE T.ID_TV_SHOW_SCHEDULE = M.ID_TV_SHOW_SCHEDULE" +
                    " AND C.ID_MUSIC_TYPE = M.ID_MUSIC_TYPE" +
                    " AND T.ID_CHANNEL = T1.ID_CHANNEL" +
                    " AND T.SCHEDULE_YEAR = T1.SCHEDULE_YEAR" +
                    " AND T.SCHEDULE_MONTH = T1.SCHEDULE_MONTH" +
                    " AND C.CATEGORY IN ('FILM', 'TELEFILM')" +
                    " AND T.FLAG_CONTENUTI = T1.FLAG_CONTENUTI" +
                    " AND T.REGIONAL_OFFICE IS NULL " +
                    " AND T.ID_TV_SHOW_SCHEDULE = T1.ID_TV_SHOW_SCHEDULE) ";

        Query query = entityManager.createNativeQuery(q);

        Object ret4 = query.setParameter(1, idCanale)
        		.setParameter(2, anno)
                .setParameter(3, mese)
                .getSingleResult();
        
        Long durata = new Long(0); 
        if(ret4 != null && ret4 instanceof BigDecimal){
        	durata = ((BigDecimal)ret4).longValue();
        }    	
        return durata;
    }

    private Long calcolaDurataAudiovisiviContenuti(Integer idCanale, Integer anno, Integer mese) {
        EntityManager entityManager = provider.get();

        String q = " select sum(m.DURATION) "
                + " FROM BDC_TV_SHOW_SCHEDULE t JOIN BDC_TV_SHOW_MUSIC m "
                + " ON  t.ID_TV_SHOW_SCHEDULE = m.ID_TV_SHOW_SCHEDULE "
                + " WHERE t.ID_CHANNEL = ?1 "
                + " AND t.SCHEDULE_YEAR = ?2 "
                + "	AND t.SCHEDULE_MONTH = ?3 "
                + " AND m.ID_MUSIC_TYPE = 4 "
                + " AND t.FLAG_CONTENUTI = 1 "
                + " AND t.REGIONAL_OFFICE IS NULL ";

        Query query = entityManager.createNativeQuery(q);

        Object ret4 = query.setParameter(1, idCanale)
                .setParameter(2, anno)
                .setParameter(3, mese)
                .getSingleResult();

        Long durata = new Long(0);
        if(ret4 != null && ret4 instanceof BigDecimal){
            durata = ((BigDecimal)ret4).longValue();
        }
        return durata;
    }

    private Long calcolaDurataGeneriTrasmissioneVuoti(Integer idCanale, Integer anno, Integer mese) {
        EntityManager entityManager = provider.get();

        String q = "select sum(t.DURATION) "
                + " FROM BDC_TV_SHOW_SCHEDULE t "
                + " WHERE t.ID_CHANNEL = ?1 "
                + " AND t.SCHEDULE_YEAR = ?2 "
                + "	AND t.SCHEDULE_MONTH = ?3 "
                + " AND t.ID_SHOW_TYPE IS NULL "
                + " AND t.REGIONAL_OFFICE IS NULL ";

        Query query = entityManager.createNativeQuery(q);

        Object ret4 = query.setParameter(1, idCanale)
                .setParameter(2, anno)
                .setParameter(3, mese)
                .getSingleResult();

        Long durata = new Long(0);
        if(ret4 != null && ret4 instanceof BigDecimal){
            durata = ((BigDecimal)ret4).longValue();
        }
        return durata;
    }

    private Long calcolaDurataPublicita(Integer idCanale, Integer anno, Integer mese) {
    	EntityManager entityManager = provider.get();

        String q =
                  " select sum(t.DURATION) "
                + " FROM BDC_TV_SHOW_SCHEDULE t, "
                + " (select  distinct t.ID_TV_SHOW_SCHEDULE "
                + " FROM BDC_TV_SHOW_SCHEDULE t, BDC_TV_SHOW_MUSIC m, BDC_MUSIC_TYPE_CATEGORY c"
                + " WHERE t.ID_TV_SHOW_SCHEDULE = m.ID_TV_SHOW_SCHEDULE "
                + " AND m.ID_MUSIC_TYPE = c.ID_MUSIC_TYPE "
                + " AND t.ID_CHANNEL = ?1 "
                + " AND t.SCHEDULE_YEAR = ?2 "
                + "	AND t.SCHEDULE_MONTH = ?3 "
                + " AND t.OVERLAP = 0 "
                + " AND c.CATEGORY LIKE 'PUBBLICIT%' "
                + " AND t.REGIONAL_OFFICE IS NULL) m "
                + " WHERE t.ID_TV_SHOW_SCHEDULE = m.ID_TV_SHOW_SCHEDULE ";

        Query query = entityManager.createNativeQuery(q);

        Object ret3 = query.setParameter(1, idCanale)
        		.setParameter(2, anno)
                .setParameter(3, mese)
                .getSingleResult();
        
        Long durata = new Long(0); 
        if(ret3 != null && ret3 instanceof BigDecimal){
        	durata = ((BigDecimal)ret3).longValue();
        }    
        return durata;
    }

    private Long calcolaDurataTrasmissioni(Integer idCanale, Integer anno, Integer mese) {
    	EntityManager entityManager = provider.get();

    	/*String q =
                  " select sum(t.DURATION) "
                + " FROM BDC_TV_SHOW_SCHEDULE t, "
                + " (select  distinct t.ID_TV_SHOW_SCHEDULE "
    			+ " FROM BDC_TV_SHOW_SCHEDULE t, BDC_TV_SHOW_MUSIC m, BDC_MUSIC_TYPE_CATEGORY c "
    			+ " WHERE t.ID_TV_SHOW_SCHEDULE = m.ID_TV_SHOW_SCHEDULE "
                + " AND c.ID_MUSIC_TYPE = m.ID_MUSIC_TYPE "
    			+ " AND t.ID_CHANNEL = ?1 "
    			+ " AND t.SCHEDULE_YEAR = ?2 "
    			+ "	AND t.SCHEDULE_MONTH = ?3 "
    			+ " AND c.CATEGORY = 'SHOW TV' "
                //+ " AND t.ID_SHOW_TYPE IS NOT NULL"
                + " AND t.REGIONAL_OFFICE IS NULL) m "
                + " WHERE t.ID_TV_SHOW_SCHEDULE = m.ID_TV_SHOW_SCHEDULE ";

           */

        // ottimizzazione query 26/11/2020

        String q =  " SELECT SUM(T1.DURATION)  " +
                " FROM  BDC_TV_SHOW_SCHEDULE T1 " +
                " WHERE T1.ID_CHANNEL = ?1 " +
                " AND T1.SCHEDULE_YEAR = ?2 " +
                " AND T1.SCHEDULE_MONTH = ?3 " +
                " AND T1.REGIONAL_OFFICE IS NULL " +
                " AND EXISTS ( SELECT 1 " +
                " FROM BDC_TV_SHOW_SCHEDULE T, BDC_TV_SHOW_MUSIC M, BDC_MUSIC_TYPE_CATEGORY C" +
                " WHERE T.ID_TV_SHOW_SCHEDULE = M.ID_TV_SHOW_SCHEDULE" +
                " AND C.ID_MUSIC_TYPE = M.ID_MUSIC_TYPE" +
                " AND T.ID_CHANNEL = T1.ID_CHANNEL" +
                " AND T.SCHEDULE_YEAR = T1.SCHEDULE_YEAR" +
                " AND T.SCHEDULE_MONTH = T1.SCHEDULE_MONTH" +
                " AND C.CATEGORY = 'SHOW TV'" +
                " AND T.REGIONAL_OFFICE IS NULL " +
                " AND T.ID_TV_SHOW_SCHEDULE = T1.ID_TV_SHOW_SCHEDULE) ";

        Query query = entityManager.createNativeQuery(q);

        Object ret2 = query.setParameter(1, idCanale)
        		.setParameter(2, anno)
                .setParameter(3, mese)
                .getSingleResult();
        
        Long durata = new Long(0); 
        if(ret2 != null && ret2 instanceof BigDecimal){
        	durata = ((BigDecimal)ret2).longValue();
        }    	
        return durata;
    }

    private Long calcolaTrasmissioniTotali(Integer idCanale, Integer anno, Integer mese) {
        EntityManager entityManager = provider.get();

        String q = " SELECT COUNT(*) " +
                   " from BDC_TV_SHOW_SCHEDULE t, "+
                   " (select distinct aa.ID_TV_SHOW_SCHEDULE " +
                   " FROM BDC_TV_SHOW_SCHEDULE aa, BDC_TV_SHOW_MUSIC, BDC_MUSIC_TYPE_CATEGORY " +
                   " WHERE aa.ID_TV_SHOW_SCHEDULE = BDC_TV_SHOW_MUSIC.ID_TV_SHOW_SCHEDULE " +
                   " AND BDC_TV_SHOW_MUSIC.ID_MUSIC_TYPE = BDC_MUSIC_TYPE_CATEGORY.ID_MUSIC_TYPE " +
                   " AND aa.ID_CHANNEL = ?1 " +
                   " AND aa.SCHEDULE_YEAR = ?2 " +
                   " AND aa.SCHEDULE_MONTH = ?3 " +
                   " AND aa.REGIONAL_OFFICE IS NULL " +
                   " AND BDC_MUSIC_TYPE_CATEGORY.CATEGORY IN ('SHOW TV')) m " +
                   " WHERE t.ID_TV_SHOW_SCHEDULE = m.ID_TV_SHOW_SCHEDULE";

        Query query = entityManager.createNativeQuery(q);

        Object ret = query.setParameter(1, idCanale)
                .setParameter(2, anno)
                .setParameter(3, mese)
                .getSingleResult();

        Long trasmissioniValide = new Long(0);
        if(ret != null && ret instanceof Long){
            trasmissioniValide = (Long)ret;
        }
        return trasmissioniValide;
    }

    private Long calcolaTrasmissioniMusicheEccedenti(Integer idCanale, Integer anno, Integer mese) {
        EntityManager entityManager = provider.get();

        String q = " SELECT SUM(cont) FROM " +
                   " (SELECT " +
                   " IF((SUM(BDC_TV_SHOW_MUSIC.REAL_DURATION)>BDC_TV_SHOW_SCHEDULE.DURATION), 1, 0) AS cont " +
                   " FROM BDC_TV_SHOW_SCHEDULE, BDC_TV_SHOW_MUSIC, BDC_MUSIC_TYPE_CATEGORY " +
                   " WHERE BDC_TV_SHOW_SCHEDULE.ID_TV_SHOW_SCHEDULE = BDC_TV_SHOW_MUSIC.ID_TV_SHOW_SCHEDULE " +
                   " AND BDC_TV_SHOW_MUSIC.ID_MUSIC_TYPE = BDC_MUSIC_TYPE_CATEGORY.ID_MUSIC_TYPE " +
                   " AND BDC_TV_SHOW_SCHEDULE.ID_CHANNEL = ?1 " +
                   " AND BDC_TV_SHOW_SCHEDULE.SCHEDULE_YEAR = ?2 " +
                   " AND BDC_TV_SHOW_SCHEDULE.SCHEDULE_MONTH = ?3 " +
                   " AND BDC_MUSIC_TYPE_CATEGORY.CATEGORY IN ('SHOW TV') " +
                   " AND BDC_TV_SHOW_SCHEDULE.REGIONAL_OFFICE IS NULL " +
                   " GROUP BY BDC_TV_SHOW_MUSIC.ID_TV_SHOW_SCHEDULE)as conteggio ";

        Query query = entityManager.createNativeQuery(q);

        Object ret1 = query.setParameter(1, idCanale)
                .setParameter(2, anno)
                .setParameter(3, mese)
                .getSingleResult();

        Long trasmissioniEccedenti = new Long(0);
        if(ret1 != null && ret1 instanceof BigDecimal){
            trasmissioniEccedenti = ((BigDecimal)ret1).longValue();
        }
        return trasmissioniEccedenti;
    }

    private Long calcolaDurataAutoriTitoliErronei(Integer idCanale, Integer anno, Integer mese) {
        EntityManager entityManager = provider.get();
        String q;
        BdcCanali canale = bdcCanaliService.findById(idCanale);
        if (TipoBroadcaster.TELEVISIONE.equals(canale.getTipoCanale())) {
            q = "SELECT sum(BDC_TV_SHOW_MUSIC.REAL_DURATION) "
                    //+ " FROM BDC_TV_ERR_COMPOSITORE_TITOLO, BDC_TV_SHOW_SCHEDULE "
                    + " FROM BDC_TV_SHOW_SCHEDULE "
                    + " LEFT JOIN BDC_TV_SHOW_MUSIC "
                    + " ON BDC_TV_SHOW_SCHEDULE.ID_TV_SHOW_SCHEDULE = BDC_TV_SHOW_MUSIC.ID_TV_SHOW_SCHEDULE "
                    + " WHERE BDC_TV_SHOW_SCHEDULE.ID_CHANNEL = ?1 "
                    + " AND BDC_TV_SHOW_SCHEDULE.SCHEDULE_YEAR = ?2 "
                    + "	AND BDC_TV_SHOW_SCHEDULE.SCHEDULE_MONTH = ?3 "
                    + " AND BDC_TV_SHOW_SCHEDULE.REGIONAL_OFFICE IS NULL "
                    //+ " AND LOWER(BDC_TV_SHOW_MUSIC.FIRST_COMPOSER) IN (LOWER(BDC_TV_ERR_COMPOSITORE_TITOLO.TV_ERR_COMPOSITORE_TITOLO)) ";
                    + " AND EXISTS (SELECT 1 FROM BDC_TV_ERR_COMPOSITORE_TITOLO WHERE LOWER(BDC_TV_ERR_COMPOSITORE_TITOLO.TV_ERR_COMPOSITORE_TITOLO) = LOWER(BDC_TV_SHOW_MUSIC.FIRST_COMPOSER))";
        } else {
            q = "SELECT sum(BDC_RD_SHOW_MUSIC.DURATION) "
                    //+ " FROM BDC_RD_ERR_COMPOSITORE_TITOLO, BDC_RD_SHOW_SCHEDULE "
                    + " FROM BDC_RD_SHOW_SCHEDULE "
                    + " LEFT JOIN BDC_RD_SHOW_MUSIC "
                    + " ON BDC_RD_SHOW_SCHEDULE.ID_RD_SHOW_SCHEDULE = BDC_RD_SHOW_MUSIC.ID_RD_SHOW_SCHEDULE "
                    + " WHERE BDC_RD_SHOW_SCHEDULE.ID_CHANNEL = ?1 "
                    + " AND BDC_RD_SHOW_SCHEDULE.SCHEDULE_YEAR = ?2 "
                    + "	AND BDC_RD_SHOW_SCHEDULE.SCHEDULE_MONTH = ?3 "
                    + " AND BDC_RD_SHOW_SCHEDULE.REGIONAL_OFFICE IS NULL "
                    //+ " AND LOWER(BDC_RD_SHOW_MUSIC.COMPOSER) IN (LOWER(BDC_RD_ERR_COMPOSITORE_TITOLO.RD_ERR_COMPOSITORE_TITOLO)) ";
                    + " AND EXISTS (SELECT 1 FROM BDC_RD_ERR_COMPOSITORE_TITOLO WHERE LOWER(BDC_RD_ERR_COMPOSITORE_TITOLO.RD_ERR_COMPOSITORE_TITOLO) = LOWER(BDC_RD_SHOW_MUSIC.COMPOSER))";
        }

        Query query = entityManager.createNativeQuery(q);

        Object ret = query.setParameter(1, idCanale)
                .setParameter(2, anno)
                .setParameter(3, mese)
                .getSingleResult();

        Long durata = new Long(0);
        if(ret != null && ret instanceof BigDecimal){
            durata = ((BigDecimal)ret).longValue();
        }
        return durata;
    }

    private Long calcolaFilmTitoliErronei(Integer idCanale, Integer anno, Integer mese) {
        EntityManager entityManager = provider.get();

        String q =
                    " select sum(t.DURATION) " +
                    " FROM BDC_TV_SHOW_SCHEDULE t, " +
                    " (SELECT distinct aa.ID_TV_SHOW_SCHEDULE " +
                   // " FROM BDC_TV_ERR_FILM_TITOLO, BDC_TV_SHOW_SCHEDULE aa, BDC_TV_SHOW_MUSIC, BDC_MUSIC_TYPE_CATEGORY
                    " FROM BDC_TV_SHOW_SCHEDULE aa, BDC_TV_SHOW_MUSIC, BDC_MUSIC_TYPE_CATEGORY " +
                    " WHERE aa.ID_TV_SHOW_SCHEDULE = BDC_TV_SHOW_MUSIC.ID_TV_SHOW_SCHEDULE " +
                    " AND BDC_TV_SHOW_MUSIC.ID_MUSIC_TYPE = BDC_MUSIC_TYPE_CATEGORY.ID_MUSIC_TYPE " +
                    " AND aa.ID_CHANNEL = ?1 " +
                    " AND aa.SCHEDULE_YEAR = ?2 " +
                    " AND aa.SCHEDULE_MONTH = ?3 " +
                    " AND BDC_MUSIC_TYPE_CATEGORY.CATEGORY IN ('FILM', 'TELEFILM') " +
                    " AND aa.REGIONAL_OFFICE IS NULL " +
                    //" AND LOWER(aa.TITLE) IN (LOWER(BDC_TV_ERR_FILM_TITOLO.TV_ERR_FILM_TITOLO))) m "
                    " AND EXISTS (SELECT 1 FROM BDC_TV_ERR_FILM_TITOLO WHERE LOWER(aa.TITLE) =  LOWER(BDC_TV_ERR_FILM_TITOLO.TV_ERR_FILM_TITOLO))) m " +
                    " WHERE t.ID_TV_SHOW_SCHEDULE = m.ID_TV_SHOW_SCHEDULE ";

        Query query = entityManager.createNativeQuery(q);

        Object ret1 = query.setParameter(1, idCanale)
                .setParameter(2, anno)
                .setParameter(3, mese)
                .getSingleResult();

        Long trasmissioniEccedenti = new Long(0);
        if(ret1 != null && ret1 instanceof BigDecimal){
            trasmissioniEccedenti = ((BigDecimal)ret1).longValue();
        }
        return trasmissioniEccedenti;
    }

    private Long calcolaProgrammiTitoliErronei(Integer idCanale, Integer anno, Integer mese) {
        EntityManager entityManager = provider.get();

        String q =
                " select sum(t.DURATION) " +
                " FROM BDC_TV_SHOW_SCHEDULE t, "+
                " (select  distinct aa.ID_TV_SHOW_SCHEDULE " +
                //+ " FROM BDC_TV_ERR_PROGRAMMA_TITOLO, BDC_TV_SHOW_SCHEDULE aa, BDC_TV_SHOW_MUSIC, BDC_MUSIC_TYPE_CATEGORY "
                " FROM  BDC_TV_SHOW_SCHEDULE aa, BDC_TV_SHOW_MUSIC, BDC_MUSIC_TYPE_CATEGORY " +
                " WHERE aa.ID_TV_SHOW_SCHEDULE = BDC_TV_SHOW_MUSIC.ID_TV_SHOW_SCHEDULE " +
                " AND BDC_TV_SHOW_MUSIC.ID_MUSIC_TYPE = BDC_MUSIC_TYPE_CATEGORY.ID_MUSIC_TYPE " +
                " AND aa.ID_CHANNEL = ?1 " +
                " AND aa.SCHEDULE_YEAR = ?2 " +
                " AND aa.SCHEDULE_MONTH = ?3 " +
                " AND aa.REGIONAL_OFFICE IS NULL " +
                " AND BDC_MUSIC_TYPE_CATEGORY.CATEGORY IN ('SHOW TV') " +
                //" AND LOWER(aa.TITLE) IN (LOWER(BDC_TV_ERR_PROGRAMMA_TITOLO.TV_ERR_PROGRAMMA_TITOLO))) m " +
                " AND EXISTS ( SELECT 1 FROM BDC_TV_ERR_PROGRAMMA_TITOLO WHERE LOWER(aa.TITLE)  = LOWER(BDC_TV_ERR_PROGRAMMA_TITOLO.TV_ERR_PROGRAMMA_TITOLO))) m"+
                " WHERE t.ID_TV_SHOW_SCHEDULE = m.ID_TV_SHOW_SCHEDULE ";

        Query query = entityManager.createNativeQuery(q);

        Object ret1 = query.setParameter(1, idCanale)
                .setParameter(2, anno)
                .setParameter(3, mese)
                .getSingleResult();

        Long trasmissioniEccedenti = new Long(0);
        if(ret1 != null && ret1 instanceof BigDecimal){
            trasmissioniEccedenti = ((BigDecimal)ret1).longValue();
        }
        return trasmissioniEccedenti;
    }
    
    @Override
    public IndicatoriPerCanale indicatoriPerCanale(Integer annoDa, Integer meseDa, Integer annoA, Integer meseA){
    	IndicatoriPerCanale indicatoriPerCanale = new IndicatoriPerCanale();
    	
    	final int TIPO_EMITTENTE = 0;
    	final int CANALE = 1;
    	final int ANNO = 2;
    	final int MESE = 3;
    	final int INDICATORE = 4;
    	final int VALORE = 5;
    	
    	EntityManager entityManager = provider.get();
        String q =
        		   " SELECT " +
        		   "   bdc_canali.tipo_canale, " +
        		   "   bdc_kpi.canale, " +
        		   "   bdc_kpi.anno, " +
        		   "   bdc_kpi.mese, " +
        		   "   bdc_kpi.indicatore, " +
        		   "   bdc_kpi.valore " +
        	       " FROM bdc_kpi LEFT JOIN bdc_canali ON bdc_kpi.canale = bdc_canali.id " +
        	       " WHERE ( bdc_kpi.anno > ?1 OR (bdc_kpi.anno = ?1 AND bdc_kpi.mese >= ?2) ) " +
        	       "   AND ( bdc_kpi.anno < ?3 OR (bdc_kpi.anno = ?3 AND bdc_kpi.mese <= ?4) ) " +
        	       "   AND bdc_kpi.indicatore <> ?5 ";
        
        Query query = entityManager.createNativeQuery(q);
        List resultList = query
        		.setParameter(1, annoDa)
                .setParameter(2, meseDa)
                .setParameter(3, annoA)
                .setParameter(4, meseA)
                .setParameter(5, BdcKpiEntity.DURATA_DISPONIBILE.toString())
                .getResultList();
        
        if(resultList != null){
            for(Object result : resultList){
                if(result instanceof Object[]){
                    Object[] record = (Object[])result;
                    String tipoEmittente = (String)record[TIPO_EMITTENTE];
                    Integer canale = (Integer)record[CANALE];
                    Integer anno = (Integer)record[ANNO];
                    Integer mese = (Integer)record[MESE];
                    String indicatore = (String)record[INDICATORE];                    
                    Long valore = 
                    	(record[VALORE] instanceof BigDecimal)?
                            ((BigDecimal)record[VALORE]).longValue() 
                        :
                            ((record[VALORE] instanceof Long) ? 
                            	(Long)record[VALORE]	
                            :
                            	((Integer)record[VALORE]).longValue()
                            );
                    indicatoriPerCanale.add(tipoEmittente, canale, anno, mese, indicatore, valore);
                }
            }
        }
    	return indicatoriPerCanale;
    }
    
    
    /**
        restituisce una Map< idCanale, Map<indicatore,valore> >
     */
    public HashMap<Integer, Map<String,Long>> mapCanaleIndicatore(Integer annoDa, Integer meseDa, Integer annoA, Integer meseA,
                      TipoBroadcaster tipoBroadcaster){

        HashMap<Integer, Map<String,Long>> retMap = new HashMap<Integer, Map<String,Long>>();

        EntityManager entityManager = provider.get();
        String q =
        		   " SELECT canale, indicatore, SUM(valore) AS valore " +
        	               " FROM bdc_kpi LEFT JOIN bdc_canali ON bdc_kpi.canale = bdc_canali.id " +
        	               " WHERE ( anno > ?1 OR (anno = ?1 AND mese >= ?2) ) " +
        	               " AND ( anno < ?3 OR (anno = ?3 AND mese <= ?4) ) " +
        	               " AND bdc_canali.tipo_canale = ?5 "+
        	               " AND indicatore <> ?6 " +
        	               " GROUP BY indicatore, canale " +
        	               " ORDER BY canale ASC ";

        Query query = entityManager.createNativeQuery(q);
        List resultList = query
        			.setParameter(1, annoDa)
                .setParameter(2, meseDa)
                .setParameter(3, annoA)
                .setParameter(4, meseA)
                .setParameter(5, tipoBroadcaster.toString())
                .setParameter(6, BdcKpiEntity.DURATA_DISPONIBILE.toString())
                .getResultList();

 
        if(resultList != null){
            for(Object result : resultList){
                if(result instanceof Object[]){
                    Object[] record = (Object[])result;
                    Integer idCanale = (Integer)record[0];
                    String indicatore = (String)record[1];
                    Long valore = (record[2] instanceof BigDecimal)?
                            ((BigDecimal)record[2]).longValue() :
                            (Long)record[2];

                    if(!retMap.containsKey(idCanale)){
                        retMap.put(idCanale,new HashMap<String,Long>());
                    }
                    Map<String,Long> mapIndicatori = retMap.get(idCanale);
                    mapIndicatori.put(indicatore, valore);
                }
            }
        }

        return retMap;
    }

    public Long calcolaDurataDisponibile(Date dataDa, Date dataA) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(dataDa);

        GregorianCalendar calendarDa = new GregorianCalendar();
        calendarDa.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
        calendarDa.set(Calendar.MONTH, calendar.get(Calendar.MONTH));
        calendarDa.set(Calendar.DAY_OF_MONTH, 1);

        calendar.setTime(dataA);
        GregorianCalendar calendarA = new GregorianCalendar();
        calendarA.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
        calendarA.set(Calendar.MONTH, calendar.get(Calendar.MONTH));
        calendarA.set(Calendar.DAY_OF_MONTH, 1);
        calendarA.add(Calendar.MONTH, 1);


        Long startMs = calendarDa.getTimeInMillis();
        Long fineMs = calendarA.getTimeInMillis();
        Long seconds = (fineMs - startMs) / 1000;
        return seconds;
    }

    @Override
    public Long calcolaDurataDisponibile(Integer anno, List<Short> mesi) {
        Long seconds = Long.valueOf(0);
        for (int i = mesi.size()-1; i>=0; i--) {
            Calendar calendar = GregorianCalendar.getInstance();
            calendar.set(Calendar.YEAR, anno);
            calendar.set(Calendar.MONTH, mesi.get(i)-1);
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            Long inizioMese = calendar.getTimeInMillis();
            calendar.add(Calendar.MONTH, 1);
            Long inizioMeseSuccessivo = calendar.getTimeInMillis();
            seconds += (inizioMeseSuccessivo - inizioMese) / 1000;
        }
        return seconds;
    }
}