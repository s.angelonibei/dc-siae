package com.alkemytech.sophia.broadcasting.dto;

import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.broadcasting.model.BdcBroadcasterConfigTemplate;

@XmlRootElement
@Entity
public class BdcAddEmittenteDTO {
	private String nomeEmittente;
	private String tipoBroadcaster;
	private BdcBroadcasterConfigTemplate bdcBroadcasterConfigTemplate;
	private String username;
	private String dataInizioValidita;
	private String dataFineValidita;
	private String nuovoTracciatoRai;
	
	public BdcAddEmittenteDTO() {
		super();
	}


	public BdcAddEmittenteDTO(String nomeEmittente, String tipoBroadcaster,
			BdcBroadcasterConfigTemplate bdcBroadcasterConfigTemplate, String username, String dataInizioValidita,
			String dataFineValidita,String nuovoTracciatoRai) {
		super();
		this.nomeEmittente = nomeEmittente;
		this.tipoBroadcaster = tipoBroadcaster;
		this.bdcBroadcasterConfigTemplate = bdcBroadcasterConfigTemplate;
		this.username = username;
		this.dataInizioValidita = dataInizioValidita;
		this.dataFineValidita = dataFineValidita;
		this.nuovoTracciatoRai = nuovoTracciatoRai;
	}


	public String getTipoBroadcaster() {
		return tipoBroadcaster;
	}


	public void setTipoBroadcaster(String tipoBroadcaster) {
		this.tipoBroadcaster = tipoBroadcaster;
	}


	public String getNomeEmittente() {
		return nomeEmittente;
	}

	public void setNomeEmittente(String nomeEmittente) {
		this.nomeEmittente = nomeEmittente;
	}


	public BdcBroadcasterConfigTemplate getBdcBroadcasterConfigTemplate() {
		return bdcBroadcasterConfigTemplate;
	}


	public void setBdcBroadcasterConfigTemplate(BdcBroadcasterConfigTemplate bdcBroadcasterConfigTemplate) {
		this.bdcBroadcasterConfigTemplate = bdcBroadcasterConfigTemplate;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getDataInizioValidita() {
		return dataInizioValidita;
	}


	public void setDataInizioValidita(String dataInizioValidita) {
		this.dataInizioValidita = dataInizioValidita;
	}


	public String getDataFineValidita() {
		return dataFineValidita;
	}


	public void setDataFineValidita(String dataFineValidita) {
		this.dataFineValidita = dataFineValidita;
	}

	public String getNuovoTracciatoRai() {
		return nuovoTracciatoRai;
	}

	public void setNuovoTracciatoRai(String nuovoTracciatoRai) {
		this.nuovoTracciatoRai = nuovoTracciatoRai;
	}
}
