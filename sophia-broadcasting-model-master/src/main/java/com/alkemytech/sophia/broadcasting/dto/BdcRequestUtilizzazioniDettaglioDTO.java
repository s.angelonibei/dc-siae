package com.alkemytech.sophia.broadcasting.dto;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.broadcasting.model.BdcBroadcasters;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BdcRequestUtilizzazioniDettaglioDTO {
	
	private BdcBroadcasters bdcBroadcasters;
	private Integer idShowSchedule;
	private boolean error;
	
	public BdcRequestUtilizzazioniDettaglioDTO() {
		super();
	}

	public BdcRequestUtilizzazioniDettaglioDTO(BdcBroadcasters bdcBroadcasters, Integer idShowSchedule, boolean error) {
		super();
		this.bdcBroadcasters = bdcBroadcasters;
		this.idShowSchedule = idShowSchedule;
		this.error = error; 
	}

	public BdcBroadcasters getBdcBroadcasters() {
		return bdcBroadcasters;
	}

	public void setBdcBroadcasters(BdcBroadcasters bdcBroadcasters) {
		this.bdcBroadcasters = bdcBroadcasters;
	}

	public Integer getIdShowSchedule() {
		return idShowSchedule;
	}

	public void setIdShowSchedule(Integer idShowSchedule) {
		this.idShowSchedule = idShowSchedule;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}




}
