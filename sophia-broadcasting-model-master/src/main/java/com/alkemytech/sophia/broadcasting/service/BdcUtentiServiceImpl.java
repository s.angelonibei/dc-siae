package com.alkemytech.sophia.broadcasting.service;

import com.alkemytech.sophia.broadcasting.dto.RepertorioDTO;
import com.alkemytech.sophia.broadcasting.enums.Repertorio;
import com.alkemytech.sophia.broadcasting.model.BdcBroadcasters;
import com.alkemytech.sophia.broadcasting.model.BdcRepertorio;
import com.alkemytech.sophia.broadcasting.model.BdcUtenti;
import com.alkemytech.sophia.broadcasting.model.BdcUtentiRepertorio;
import com.alkemytech.sophia.broadcasting.service.interfaces.BdcUtentiService;
import com.google.inject.Inject;
import com.google.inject.Provider;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BdcUtentiServiceImpl implements BdcUtentiService {

	private final Provider<EntityManager> provider;

	@Inject
	public BdcUtentiServiceImpl(Provider<EntityManager> provider) {
		this.provider = provider;
	}

	@Override
	public BdcUtenti findById(Integer id) {
		EntityManager em = null;
		em = provider.get();
		final Query q = em.createQuery("SELECT a FROM BdcUtenti a WHERE a.id=:id").setParameter("id", id);
		return (BdcUtenti) q.getSingleResult();
	}

	@Override
	public List<BdcUtenti> findAll() {

		EntityManager em = null;
		em = provider.get();

		final Query q = em.createQuery("SELECT a FROM BdcUtenti a ");

		return (List<BdcUtenti>) q.getResultList();
	}

	@Override
	public List<BdcUtenti> findAllByBroadcaster(BdcBroadcasters bdcBroadcasters) {

		EntityManager em = null;
		em = provider.get();

		final Query q = em.createQuery("SELECT a FROM BdcUtenti a WHERE a.bdcBroadcasters=:id").setParameter("id",
				bdcBroadcasters);

		return (List<BdcUtenti>) q.getResultList();
	}

	@Override
	public BdcUtenti findByUsername(String username) {
		EntityManager em = null;
		em = provider.get();
		final Query q = em.createQuery("SELECT a FROM BdcUtenti a WHERE a.username=:username").setParameter("username",
				username);
		try {
			return (BdcUtenti) q.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	@Override
	public BdcUtenti findByEmail(String email) {
		EntityManager em = null;
		em = provider.get();
		final Query q = em.createQuery("SELECT a FROM BdcUtenti a WHERE a.email=:email").setParameter("email", email);
		try {
			return (BdcUtenti) q.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	@Override
	public List<BdcUtenti> findByRepertori(BdcBroadcasters bdcBroadcasters, String repertori) {

		EntityManager em = null;
		em = provider.get();
		String query = "Select \n" + "u.*,\n"
				+ "(Select group_concat(r.nome_repertorio) from BDC_UTENTI_REPERTORIO r where r.id_utente=u.id) as repertori,\n"
				+ "(Select if(r.default_repertorio = true, r.nome_repertorio, null) from BDC_UTENTI_REPERTORIO r where r.id_utente=u.id and r.default_repertorio=true) as repertorioDefault   \n"
				+ "from bdc_utenti u left join BDC_UTENTI_REPERTORIO g on u.id=g.id_utente\n"
				+ "where u.id_broadcaster = " + bdcBroadcasters.getId() + " AND  g.nome_repertorio IN ('"
				+ repertori.replace(",", "','") + "')\n" + "group by u.id";
		final Query q = em.createNativeQuery(query);
		try {
			List<Object[]> result = q.getResultList();
			List<BdcUtenti> bdcUtenti = new ArrayList<>();
			for (Object[] row : result) {
				bdcUtenti.add(new BdcUtenti(row));
			}
			return bdcUtenti;
		} catch (NoResultException e) {
			return null;
		}
	}

	@Override
	public boolean signup(BdcUtenti bdcUtente, List<RepertorioDTO> repertoriDTO) {

		EntityManager entityManager = provider.get();
		entityManager.getTransaction().begin();
		try {
			entityManager.persist(bdcUtente);
			entityManager.flush();
			List<BdcUtentiRepertorio> repertoriList = new ArrayList<>();
			for (RepertorioDTO repertorioDTO : repertoriDTO) {
				if (repertorioDTO.isActive()) {
					BdcUtentiRepertorio repertorio = new BdcUtentiRepertorio();
					repertorio.setIdUtente(bdcUtente.getId());
					repertorio.setDefaultRepertorio(repertorioDTO.isDefaultValue());
					final Query q = entityManager.createQuery("SELECT a FROM BdcRepertorio a where a.nome=:nome")
							.setParameter("nome", repertorioDTO.getNome());
					BdcRepertorio rep = (BdcRepertorio) q.getSingleResult();
					repertorio.setRepertorio(rep);
					repertoriList.add(repertorio);
				}
			}
			bdcUtente.setRepertori(repertoriList);
			entityManager.merge(bdcUtente);
			entityManager.getTransaction().commit();
			return true;
		} catch (Exception e) {
			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
			return false;
		}
	}

	@Override
	public int updateUser(BdcUtenti bdcUtente, List<RepertorioDTO> repertoriDTO) {
		EntityManager entityManager = provider.get();
		entityManager.getTransaction().begin();

		BdcUtenti utente = findById(bdcUtente.getId());
		for (BdcUtentiRepertorio repertorio : utente.getRepertori()) {
			entityManager.remove(repertorio);
		}
		try {
			utente.setEmail(bdcUtente.getEmail());
			utente.setUsername(bdcUtente.getUsername());
			utente.setUtenteUltimaModifica(bdcUtente.getUtenteUltimaModifica());
			utente.setDataUltimaModifica(new Date());
			List<BdcUtentiRepertorio> repertoriList = new ArrayList<>();
			for (RepertorioDTO repertorioDTO : repertoriDTO) {
				if (repertorioDTO.isActive()) {
					BdcUtentiRepertorio repertorio = new BdcUtentiRepertorio();
					repertorio.setIdUtente(utente.getId());
					repertorio.setDefaultRepertorio(repertorioDTO.isDefaultValue());
					final Query q = entityManager.createQuery("SELECT a FROM BdcRepertorio a where a.nome=:nome")
							.setParameter("nome", repertorioDTO.getNome());
					BdcRepertorio rep = (BdcRepertorio) q.getSingleResult();
					repertorio.setRepertorio(rep);
					repertoriList.add(repertorio);
				}
			}
			utente.setRepertori(repertoriList);
			entityManager.merge(utente);
			entityManager.getTransaction().commit();
			return 200;
		} catch (Exception e) {
			e.printStackTrace();
			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
			return 502;
		}
	}

	@Override
	public boolean changePassword(BdcUtenti bdcUtente) {
		EntityManager entityManager = provider.get();
		entityManager.getTransaction().begin();
		try {
			BdcUtenti utente = findById(bdcUtente.getId());
			utente.setPassword(bdcUtente.getPassword());
			utente.setUtenteUltimaModifica(bdcUtente.getUtenteUltimaModifica());
			utente.setDataUltimaModifica(new Date());
			entityManager.merge(utente);
			entityManager.getTransaction().commit();
			return true;
		} catch (Exception e) {
			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
			return false;
		}
	}

	@Override
	public boolean deleteUser(BdcUtenti bdcUtente) {
		EntityManager entityManager = provider.get();
		try {
			entityManager.getTransaction().begin();
			entityManager.remove(findById(bdcUtente.getId()));
			entityManager.getTransaction().commit();
			return true;
		} catch (Exception e) {
			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
			return false;
		}
	}

	@Override
	public List<BdcRepertorio> findRepertori() {
		EntityManager em = null;
		em = provider.get();
		final Query q = em.createQuery("SELECT a FROM BdcRepertorio a");
		try {
			List<BdcRepertorio> repertori = (List<BdcRepertorio>) q.getResultList();
			return repertori;
		} catch (NoResultException e) {
			return null;
		}
	}

	@Override
	public List<BdcUtentiRepertorio> findRepertoriByUserId(int id) {
		EntityManager em = null;
		em = provider.get();
		final Query q = em.createQuery("SELECT a FROM BdcRepertorio a");
		try {
			final Query q1 = em.createQuery("SELECT a FROM BdcUtentiRepertorio a where a.idUtente=:idUtente")
					.setParameter("idUtente", id);
			List<BdcUtentiRepertorio> repertoriUtente = (List<BdcUtentiRepertorio>) q1.getResultList();
			return repertoriUtente;
		} catch (NoResultException e) {
			return null;
		}
	}
}
