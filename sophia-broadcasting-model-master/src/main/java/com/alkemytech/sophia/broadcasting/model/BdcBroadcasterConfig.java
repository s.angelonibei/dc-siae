package com.alkemytech.sophia.broadcasting.model;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Alessandro Russo on 27/11/2017.
 */
@XmlRootElement
@Entity
@Table(name = "BDC_BROADCASTER_CONFIG")
public class BdcBroadcasterConfig implements Serializable {


	
    public BdcBroadcasterConfig() {
		super();
	}

	public BdcBroadcasterConfig(Long idBroadcaster, Long idChannel, Date validFrom, Date validTo,
			BdcBroadcasterConfigTemplate configuration, String fieldsMapping, Date creationDate, String creationUser,
			Date modifyDate, String modifyUser) {
		super();
		this.idBroadcaster = idBroadcaster;
		this.idChannel = idChannel;
		this.validFrom = validFrom;
		this.validTo = validTo;
		this.configuration = configuration;
		this.fieldsMapping = fieldsMapping;
		this.creationDate = creationDate;
		this.creationUser = creationUser;
		this.modifyDate = modifyDate;
		this.modifyUser = modifyUser;
	}

	@Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="ID_BROADCASTER_CONFIG", nullable=false)
    private Long id;

    @Column(name="ID_BROADCASTER", nullable=false)
    private Long idBroadcaster;

    @Column(name="ID_CHANNEL")
    private Long idChannel;

    @Column(name="VALID_FROM", nullable=false)
    private Date validFrom;

    @Column(name="VALID_TO")
    private Date validTo;

    @Expose
    @OneToOne
    @JoinColumn(name="CONFIGURATION")
    private BdcBroadcasterConfigTemplate configuration;

    @Column(name = "FIELDS_MAPPING", columnDefinition = "json")
    private String fieldsMapping;

    @Column(name="CREATION_DATE", nullable=false)
    private Date creationDate;

    @Column(name="CREATION_USER", nullable=false)
    private String creationUser;

    @Column(name="MODIFY_DATE")
    private Date modifyDate;

    @Column(name="MODIFY_USER")
    private String modifyUser;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdBroadcaster() {
        return idBroadcaster;
    }

    public void setIdBroadcaster(Long idBroadcaster) {
        this.idBroadcaster = idBroadcaster;
    }

    public Long getIdChannel() {
        return idChannel;
    }

    public void setIdChannel(Long idChannel) {
        this.idChannel = idChannel;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    public BdcBroadcasterConfigTemplate getConfiguration() {
        return configuration;
    }

    public void setConfiguration(BdcBroadcasterConfigTemplate configuration) {
        this.configuration = configuration;
    }

    public String getFieldsMapping() {
        return fieldsMapping;
    }

    public void setFieldsMapping(String fieldsMapping) {
        this.fieldsMapping = fieldsMapping;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreationUser() {
        return creationUser;
    }

    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getModifyUser() {
        return modifyUser;
    }

    public void setModifyUser(String modifyUser) {
        this.modifyUser = modifyUser;
    }

    @Override
    public String toString() {
        return new GsonBuilder()
                .setPrettyPrinting()
                .create()
                .toJson(this);
    }
}
