package com.alkemytech.sophia.broadcasting.enums;

import com.alkemytech.sophia.common.Constants;

public enum ArmonizedField {

    //COMMON
    CANALE(Constants.CANALE, "Canale"),
    PALINSESTO(Constants.PALINSESTO, "Palinsesto"),
    TITOLO_TRASMISSIONE(Constants.TITOLO_TX, "Titolo trasmissione"),
    DATA_INIZIO_TRASMISSIONE(Constants.DATA_INIZIO_TX, "Data inizio trasmissione"),
    ORARIO_INIZIO_TRASMISSIONE(Constants.ORARIO_INIZIO_TX, "Orario inizio trasmissione"),
    DURATA_TRASMISSIONE(Constants.DURATA_TX, "Durata trasmissione"),
    CATEGORIA_USO(Constants.CATEGORIA_USO_OP, "Categoria d'uso"),
    ERRORE("errore", "errore"),
    STATO("stato", "Stato"),
    REGOLA1(Constants.REGOLA1, "Campi obbligatori mancanti"),
    REGOLA2(Constants.REGOLA2, "Campi con formato non supportato"),
    REGOLA3(Constants.REGOLA3, "Campi con valore inatteso"),
    ERRORE_GENERICO(Constants.GLOBAL_ERROR, "Altre segnalazioni"),

    NOME_FILE("NOME_FILE_ARMONIZZATO", "Nome File"),
    DATA_UPLOAD("DATA_UPLOAD_FILE", "Data Upload"),
    ORA_UPLOAD("ORA_UPLOAD_FILE", "Ora Upload"),

    //RADIO
    TITOLO_BRANO(Constants.TITOLO_OP, "Titolo brano"),
    COMPOSITORE_OP(Constants.COMPOSITORE_OP, "Compositore/Autore"),
    ESECUTORE_OP(Constants.ESECUTORE_OP, "Esecutore/Performer"),
    ALBUM_OP(Constants.ALBUM_OP, "Album"),
    DURATA_BRANO(Constants.DURATA_OP, "Durata brano"),
    CODICE_ISWC_OP(Constants.CODICE_ISWC_OP, "Codice ISWC"),
    CODICE_ISRC_OP(Constants.CODICE_ISRC_OP, "Codice ISRC"),
    ORARIO_INIZIO_OP(Constants.ORARIO_INIZIO_OP, "Orario inizio brano"),
    DIFFUSIONE_REGIONALE_RD(Constants.DIFFUSIONE_REGIONALE, "Diffusione regionale"),


    //TELEVISIONE
    GENERE_TRASMISSIONE(Constants.GENERE_TX, "Genere trasmissione"),
    TITOLO_ORIGINALE_TRASMISSIONE(Constants.TITOLO_ORIGINALE_TX, "Titolo originale trasmissione"),
    ORARIO_FINE_TRASMISSIONE(Constants.ORARIO_FINE_TX, "Orario fine trasmissione"),
    REPLICA_O_PRIMA_ESECUZIONE(Constants.REPLICA_TX, "Replica o prima esecuzione"),
    PAESE_DI_PRODUZIONE(Constants.PAESE_PRODUZIONE_TX, "Paese di produzione"),
    ANNO_DI_PRODUZIONE(Constants.ANNO_PRODUZIONE_TX, "Anno di produzione"),
    PRODUTTORE(Constants.PRODUTTORE_TX, "Produttore"),
    REGISTA(Constants.REGISTA_TX, "Regista"),
    TITOLO_EPISODIO(Constants.TITOLO_EPISODIO_TX, "Titolo episodio"),
    TITOLO_ORIGINALE_EPISODIO(Constants.TITOLO_ORIGINALE_EPISODIO_TX, "Titolo originale episodio"),
    NUMERO_PROGRESSIVO_EPISODIO(Constants.NUM_PROGRESSIVO_EPISODIO_TX, "Numero progressivo episodio"),
    TITOLO_OPERA_MUSICALE(Constants.TITOLO_OP, "Titolo opera musicale"),
    COMPOSITORE_AUTORE_1(Constants.COMPOSITORE1_OP, "Compositore/Autore (1)"),
    COMPOSITORE_AUTORE_2(Constants.COMPOSITORE2_OP, "Compositore/Autore (2)"),
    DURATA_DELL_OPERA(Constants.DURATA_OP, "Durata dell'opera"),
    DURATA(Constants.DURATA_EFFETTIVA_OP, "Durata effettiva di utilizzo"),
    DIFFUSIONE_REGIONALE_TV(Constants.DIFFUSIONE_REGIONALE, "Diffusione regionale"),
    //R9-22
    IDENTIFICATIVO_RAI(Constants.IDENTIFICATIVO_RAI, "Identificativo Rai");


    private String field;
    private String description;

    ArmonizedField(String field, String description) {
        this.field = field;
        this.description = description;
    }

    public String getField() {
        return field;
    }

    public String getDescription() {
        return description;
    }

    public static ArmonizedField find(String field){
        if (field == null){
            return null;
        }
        for (ArmonizedField af : values()) {
            if(af.getField().equals(field)){
                return af;
            }
        }
        return null;
    }

    public static String findDescription(String field){
        ArmonizedField af =find(field);
        if(af != null){
            return af.getDescription();
        }
        return null;
    }

}
