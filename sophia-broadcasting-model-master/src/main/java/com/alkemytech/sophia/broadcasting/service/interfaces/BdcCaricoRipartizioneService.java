package com.alkemytech.sophia.broadcasting.service.interfaces;

import java.util.Date;
import java.util.List;

import com.alkemytech.sophia.broadcasting.model.BdcCaricoRipartizione;
import com.alkemytech.sophia.broadcasting.model.BdcCaricoRipartizioneStoricoModifica;


public interface BdcCaricoRipartizioneService {
	List<BdcCaricoRipartizione> findAll(Date dateFrom, Date dateTo, String tipoEmittente, Integer idEmittente, Integer idCanale,
			Integer first, Integer last, Integer maxrow);
	
	Integer updateCaricoRipartizione(String username, BdcCaricoRipartizione caricoRipartizione);

	List<BdcCaricoRipartizioneStoricoModifica> findAllStorico(Integer idCaricoRipartizione);
	
	
}
