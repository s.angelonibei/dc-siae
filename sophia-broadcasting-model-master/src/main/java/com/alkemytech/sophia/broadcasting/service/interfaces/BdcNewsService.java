package com.alkemytech.sophia.broadcasting.service.interfaces;

import com.alkemytech.sophia.broadcasting.dto.BdcNewsDTO;
import com.alkemytech.sophia.broadcasting.dto.BdcNewsListRequest;
import com.alkemytech.sophia.broadcasting.model.BdcNews;

import java.util.List;

public interface BdcNewsService {
    List<BdcNewsDTO> findNewsForBdc(Integer idUser, Integer idBroadcaster);

	List<BdcNewsDTO> findAll();

	boolean addNews(BdcNews addNewsRequestDTO);

	boolean updateNews(BdcNews bdcNews);

	List<BdcNews> findAllExplose(BdcNewsListRequest bdcNewsListRequest, int first, int last);

	List<BdcNews> findAllExplose(BdcNewsListRequest bdcNewsListRequest);
	
	boolean deleteNews(BdcNews bdcNews);

	boolean deactivateNews(BdcNews bdcNews);

	boolean activateNews(BdcNews bdcNews);

	BdcNews findbyId(Integer id);
}
