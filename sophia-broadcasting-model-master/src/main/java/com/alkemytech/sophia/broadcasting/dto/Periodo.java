package com.alkemytech.sophia.broadcasting.dto;

import java.util.Date;

public class Periodo {
	private Integer id;
	private String nome; 
	private String ambito;
	private Date dataInizioPeriodo;
	private Date dataFinePeriodo;
	
	
	public Periodo() {
		super();
	}


	public Periodo(Integer id, String nome, String ambito, Date dataInizioPeriodo, Date dataFinePeriodo) {
		super();
		this.id = id;
		this.nome = nome;
		this.ambito = ambito;
		this.dataInizioPeriodo = dataInizioPeriodo;
		this.dataFinePeriodo = dataFinePeriodo;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getAmbito() {
		return ambito;
	}


	public void setAmbito(String ambito) {
		this.ambito = ambito;
	}


	public Date getDataInizioPeriodo() {
		return dataInizioPeriodo;
	}


	public void setDataInizioPeriodo(Date dataInizioPeriodo) {
		this.dataInizioPeriodo = dataInizioPeriodo;
	}


	public Date getDataFinePeriodo() {
		return dataFinePeriodo;
	}


	public void setDataFinePeriodo(Date dataFinePeriodo) {
		this.dataFinePeriodo = dataFinePeriodo;
	}

	@Override
	public String toString() {
		return "periodo dal "+dataInizioPeriodo+" al "+dataFinePeriodo;
	}


}
