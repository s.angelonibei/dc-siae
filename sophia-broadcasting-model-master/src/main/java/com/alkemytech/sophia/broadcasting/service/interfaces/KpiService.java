package com.alkemytech.sophia.broadcasting.service.interfaces;

import com.alkemytech.sophia.broadcasting.data.IndicatoriPerCanale;
import com.alkemytech.sophia.broadcasting.dto.IndicatoreDTO;
import com.alkemytech.sophia.broadcasting.dto.KPI;
import com.alkemytech.sophia.broadcasting.model.TipoBroadcaster;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

public interface KpiService {
    KPI calcolaKpiCopertura(long durataFilm, long durataGeneriVuoti, long durataPubblicita, long durataTrasmissioni, Integer anno, List<Short> mesi);
    KPI calcolaKpiCopertura(long durataFilm, long durataPubblicita, long durataTrasmissioni, Integer anno, List<Short> mesi);
    KPI calcolaKpiCopertura(long durataFilm, long durataGeneriVuoti, long durataPubblicita,long durataTrasmissioni, Date dataDa, Date dataA);
    KPI calcolaKpiDurataMusicaDichiarata(HashMap<String, IndicatoreDTO> mapIndicatori);
    KPI calcolaKpiRecordScartati(HashMap<String, IndicatoreDTO> mapIndicatori);
    KPI calcolaKpiBraniTitoliErronei(HashMap<String, IndicatoreDTO> mapIndicatori);
    KPI calcolaKpiAutoriTitoliErronei(HashMap<String, IndicatoreDTO> mapIndicatori);
    KPI calcolaKpiFilmTitoliErronei(HashMap<String, IndicatoreDTO> mapIndicatori);
    KPI calcolaKpiProgrammiTitoliErronei(HashMap<String, IndicatoreDTO> mapIndicatori);
    KPI calcolaKpiMusicaEccedenteTrasmissione(HashMap<String, IndicatoreDTO> mapIndicatori);
    KPI calcolaKpiRecordInRitardo(HashMap<String, IndicatoreDTO> mapIndicatori);
    KPI calcolaKpiDurataBraniDichiaratiRD(long durataMusiche,Integer anno, List<Short> mesi);
    KPI calcolaKpiDurataBraniDichiaratiRD(long durataMusiche, Date dataDa, Date dataA);
    KPI calcolaKpiBraniTitoliErroneiRD(HashMap<String, IndicatoreDTO> mapIndicatori);
    KPI calcolaKpiAutoriTitoliErroneiRD(HashMap<String, IndicatoreDTO> mapIndicatori);
    HashMap<Integer, List<KPI>> findKpiRelativi(Date dateFrom, Date dateTo, TipoBroadcaster tipoBroadcaster);
    List<KPI> listKpi(Date dateFrom, Date dateTo, Integer idCanale, TipoBroadcaster tipoBroadcaster, IndicatoriPerCanale indicatoriPerCanale);
	IndicatoriPerCanale indicatoriPerCanale(Date dataDa, Date dataA);
}