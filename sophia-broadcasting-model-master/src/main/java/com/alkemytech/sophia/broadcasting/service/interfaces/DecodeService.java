package com.alkemytech.sophia.broadcasting.service.interfaces;

import com.alkemytech.sophia.broadcasting.model.BdcCanali;
import com.alkemytech.sophia.broadcasting.model.MusicType;
import com.alkemytech.sophia.broadcasting.model.ShowType;
import com.alkemytech.sophia.broadcasting.model.decode.BdcDecodeChannel;
import com.alkemytech.sophia.broadcasting.model.decode.BdcDecodeMusicType;
import com.alkemytech.sophia.broadcasting.model.decode.BdcDecodeShowType;

import java.util.Date;
import java.util.List;

/**
 * Created by Alessandro Russo on 09/12/2017.
 */
public interface DecodeService {

    public void start();

    //SHOW TYPE
    Long verifyIdShowType(String checkShowType);

    //SHOW TYPE
    ShowType findShowTypeById(Long idShowType);
    Long findIdShowTypeByValueAndScope(Integer idBroacaster, String value, String scope);
    Long findIdShowTypeByName(String name);
    String getNameShowTypeById(Long id);
    String findCategoryShowTypeByValueAndScope(Integer idBroacaster, String value, String scope);


    //MUSIC TYPE
    Long verifyIdMusicType(String checkMusicType);

    //MUSIC TYPE
    MusicType findMusicTypeById(Long idMusicType);
    Long findIdMusicTypeByValueAndScope(Integer idBroacaster, String value, String scope);
    Long findIdMusicTypeByName(String name);
    String getNameMusicTypeById(Long id);
    Long findIdMusicTypeByShowTypeAndName(String showTypeName, String name);
    Long findIdMusicTypeByShowTypeAndValue(String showTypeName, Integer idBroacaster, String value, String scope);

    //CHANNEL
    BdcCanali findChannelById(Integer idChannel);
    List<BdcCanali> findCanaliByIdBroadcaster(Integer idBroadcaster);
    Integer findIdChannelByValue(Integer idBroacaster, String value);
    Integer findIdChannelByName(Integer idBroacaster, String name);
    String getNameChannelById(Integer id);
    boolean isRegionalChannel(Integer idBroacaster, String value);

    //BROADCASTER
    Integer findIdBroadcasterByName(String name, String scope);
}
