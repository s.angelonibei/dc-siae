package com.alkemytech.sophia.broadcasting.dto.performing;

import com.alkemytech.sophia.broadcasting.enums.Stato;

import java.util.Date;

public class RSInformationFileDTO
{
    private String nomeFile;
    private Integer idEmittente;
    private String emittente;
    private Integer idCanale;
    private String canale;
    private Date dataUpload;
    private String tipoUpload;

    public RSInformationFileDTO() {
    }

    public RSInformationFileDTO(String nomeFile, Integer idEmittente, String emittente,
                                 Integer idCanale, String canale,
                                 Date dataUpload, String tipoUpload) {
        this.nomeFile = nomeFile;
        this.idEmittente = idEmittente;
        this.emittente = emittente;
        this.idCanale = idCanale;
        this.canale = canale;
        this.dataUpload = dataUpload;
        this.tipoUpload = tipoUpload;
    }

    public String getNomeFile() {
        return nomeFile;
    }

    public void setNomeFile(String nomeFile) {
        this.nomeFile = nomeFile;
    }

    public Integer getIdEmittente() {
        return idEmittente;
    }

    public void setIdEmittente(Integer idEmittente) {
        this.idEmittente = idEmittente;
    }

    public String getEmittente() {
        return emittente;
    }

    public void setEmittente(String emittente) {
        this.emittente = emittente;
    }

    public Integer getIdCanale() {
        return idCanale;
    }

    public void setIdCanale(Integer idCanale) {
        this.idCanale = idCanale;
    }

    public String getCanale() {
        return canale;
    }

    public void setCanale(String canale) {
        this.canale = canale;
    }

    public Date getDataUpload() {
        return dataUpload;
    }

    public void setDataUpload(Date dataUpload) {
        this.dataUpload = dataUpload;
    }

    public String getTipoUpload() {
        return tipoUpload;
    }

    public void setTipoUpload(String tipoUpload) {
        this.tipoUpload = tipoUpload;
    }



    public RSInformationFileDTO(Object[] obj) {
        try {
            nomeFile = (String) obj[0];
            idEmittente = (Integer) obj[1];
            emittente = (String) obj[2];
            idCanale = (Integer) obj[3];
            canale = (String) obj[4];
            dataUpload = (Date) obj[5];
            tipoUpload = (String) obj[6];
        }catch (Throwable e) {
                e.printStackTrace();
            }
        }
}
