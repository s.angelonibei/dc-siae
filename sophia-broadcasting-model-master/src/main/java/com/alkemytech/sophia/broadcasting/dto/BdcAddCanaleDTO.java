package com.alkemytech.sophia.broadcasting.dto;

import java.util.Date;

import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
public class BdcAddCanaleDTO {
	
    private Integer bdcBroadcastersId;
    private String nome;
    private boolean generalista;
    private boolean specialRadio;
    private String dataInizioValid;
    private String dataFineValid;
    private String utenteUltimaModifica;
    
    
    
	public BdcAddCanaleDTO() {
		super();
	}



	public BdcAddCanaleDTO(Integer bdcBroadcastersId, String nome, boolean generalista, boolean specialRadio,
			String dataInizioValid, String dataFineValid, String utenteUltimaModifica) {
		super();
		this.bdcBroadcastersId = bdcBroadcastersId;
		this.nome = nome;
		this.generalista = generalista;
		this.specialRadio = specialRadio;
		this.dataInizioValid = dataInizioValid;
		this.dataFineValid = dataFineValid;
		this.utenteUltimaModifica = utenteUltimaModifica;
	}



	public Integer getBdcBroadcastersId() {
		return bdcBroadcastersId;
	}



	public void setBdcBroadcastersId(Integer bdcBroadcastersId) {
		this.bdcBroadcastersId = bdcBroadcastersId;
	}



	public String getNome() {
		return nome;
	}



	public void setNome(String nome) {
		this.nome = nome;
	}



	public boolean getGeneralista() {
		return generalista;
	}



	public void setGeneralista(boolean generalista) {
		this.generalista = generalista;
	}



	public boolean getSpecialRadio() {
		return specialRadio;
	}



	public void setSpecialRadio(boolean specialRadio) {
		this.specialRadio = specialRadio;
	}



	public String getDataInizioValid() {
		return dataInizioValid;
	}



	public void setDataInizioValid(String dataInizioValid) {
		this.dataInizioValid = dataInizioValid;
	}



	public String getDataFineValid() {
		return dataFineValid;
	}



	public void setDataFineValid(String dataFineValid) {
		this.dataFineValid = dataFineValid;
	}



	public String getUtenteUltimaModifica() {
		return utenteUltimaModifica;
	}



	public void setUtenteUltimaModifica(String utenteUltimaModifica) {
		this.utenteUltimaModifica = utenteUltimaModifica;
	}
    
	
    
}
