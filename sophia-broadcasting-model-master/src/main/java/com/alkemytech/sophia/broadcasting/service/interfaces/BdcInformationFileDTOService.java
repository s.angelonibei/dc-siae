package com.alkemytech.sophia.broadcasting.service.interfaces;

import com.alkemytech.sophia.broadcasting.dto.BdcInformationFileDTO;
import com.alkemytech.sophia.broadcasting.dto.RequestMonitoraggioKPIDTO;

import java.util.List;

public interface BdcInformationFileDTOService {
    List<BdcInformationFileDTO> fileDTOS(RequestMonitoraggioKPIDTO monitoraggioKPI);
}
