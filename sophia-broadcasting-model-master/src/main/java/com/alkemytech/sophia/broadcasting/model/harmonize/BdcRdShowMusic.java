package com.alkemytech.sophia.broadcasting.model.harmonize;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the BDC_RD_SHOW_MUSIC database table.
 * 
 */
@Entity(name="RdShowMusic")
@Table(name="BDC_RD_SHOW_MUSIC")
@NamedQuery(name="RdShowMusic.findAll", query="SELECT b FROM RdShowMusic b")
public class BdcRdShowMusic implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_RD_SHOW_MUSIC", unique=true, nullable=false)
	private Integer idRdShowMusic;

	@Lob
	@Column(name="ALBUM")
	private String album;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="BEGIN_TIME")
	private Date beginTime;

	@Lob
	@Column(name="COMPOSER")
	private String composer;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATION_DATE", nullable=false)
	private Date creationDate;

	@Column(name="DAYS_FROM_UPLOAD", nullable=false)
	private Integer daysFromUpload;

	@Column(name="DURATION")
	private Integer duration;

	@Column(name="ID_MUSIC_TYPE")
	private java.math.BigInteger idMusicType;

	@Column(name="ID_NORMALIZED_FILE", nullable=false)
	private java.math.BigInteger idNormalizedFile;

	@Column(name="ISRC_CODE", length=50)
	private String isrcCode;

	@Column(name="ISWC_CODE", length=50)
	private String iswcCode;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="MODIFY_DATE")
	private Date modifyDate;

	@Column(name="NOTE", length=400)
	private String note;

	@Lob
	@Column(name="PERFORMER")
	private String performer;

	@Column(name="REAL_DURATION")
	private Integer realDuration;

	@Lob
	@Column(name="TITLE")
	private String title;


	@Column( name = "ALERT")
	private String alert;


	//bi-directional many-to-one association to BdcRdShowSchedule
	@JsonBackReference
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_RD_SHOW_SCHEDULE")
	private BdcRdShowSchedule bdcRdShowSchedule;

	//R9-22
	@Column(name = "IDENTIFICATIVO_RAI")
	private String identificativoRai;

	public BdcRdShowMusic() {
	}

	public Integer getIdRdShowMusic() {
		return this.idRdShowMusic;
	}

	public void setIdRdShowMusic(Integer idRdShowMusic) {
		this.idRdShowMusic = idRdShowMusic;
	}

	public String getAlbum() {
		return this.album;
	}

	public void setAlbum(String album) {
		this.album = album;
	}

	public Date getBeginTime() {
		return this.beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public String getComposer() {
		return this.composer;
	}

	public void setComposer(String composer) {
		this.composer = composer;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Integer getDaysFromUpload() {
		return this.daysFromUpload;
	}

	public void setDaysFromUpload(Integer daysFromUpload) {
		this.daysFromUpload = daysFromUpload;
	}

	public Integer getDuration() {
		return this.duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public java.math.BigInteger getIdMusicType() {
		return this.idMusicType;
	}

	public void setIdMusicType(java.math.BigInteger idMusicType) {
		this.idMusicType = idMusicType;
	}

	public java.math.BigInteger getIdNormalizedFile() {
		return this.idNormalizedFile;
	}

	public void setIdNormalizedFile(java.math.BigInteger idNormalizedFile) {
		this.idNormalizedFile = idNormalizedFile;
	}

	public String getIsrcCode() {
		return this.isrcCode;
	}

	public void setIsrcCode(String isrcCode) {
		this.isrcCode = isrcCode;
	}

	public String getIswcCode() {
		return this.iswcCode;
	}

	public void setIswcCode(String iswcCode) {
		this.iswcCode = iswcCode;
	}

	public Date getModifyDate() {
		return this.modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getNote() {
		return this.note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getPerformer() {
		return this.performer;
	}

	public void setPerformer(String performer) {
		this.performer = performer;
	}

	public Integer getRealDuration() {
		return this.realDuration;
	}

	public void setRealDuration(Integer realDuration) {
		this.realDuration = realDuration;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public BdcRdShowSchedule getBdcRdShowSchedule() {
		return this.bdcRdShowSchedule;
	}

	public void setBdcRdShowSchedule(BdcRdShowSchedule bdcRdShowSchedule) {
		this.bdcRdShowSchedule = bdcRdShowSchedule;
	}

	public String getAlert() {
		return alert;
	}

	public void setAlert(String alert) {
		this.alert = alert;
	}

	public String getIdentificativoRai() {
		return identificativoRai;
	}

	public void setIdentificativoRai(String identificativoRai) {
		this.identificativoRai = identificativoRai;
	}
}