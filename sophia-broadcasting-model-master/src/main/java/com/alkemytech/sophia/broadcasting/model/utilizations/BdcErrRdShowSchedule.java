package com.alkemytech.sophia.broadcasting.model.utilizations;

import com.alkemytech.sophia.broadcasting.model.BdcCanali;
import com.alkemytech.sophia.broadcasting.model.BdcNormalizedFile;
import com.alkemytech.sophia.broadcasting.model.ShowType;
import com.alkemytech.sophia.common.Constants;
import com.alkemytech.sophia.common.tools.StringTools;
import org.apache.commons.lang3.math.NumberUtils;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.alkemytech.sophia.broadcasting.utils.Utilities.getMapValue;

@XmlRootElement
@Entity
@Table(name = "BDC_ERR_RD_SHOW_SCHEDULE")
public class BdcErrRdShowSchedule implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column( name="ID_ERR_RD_SHOW_SCHEDULE", nullable = false)
    private Long id;

    @OneToMany
    @JoinColumn(name = "ID_ERR_RD_SHOW_SCHEDULE")
    List<BdcErrRdShowMusic> errRdShowMusicList;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_NORMALIZED_FILE", nullable = false)
    private BdcNormalizedFile normalizedFile;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_CHANNEL", nullable = false)
    private BdcCanali canale;

    @Column( name="TITLE")
    private String title;

    @Column( name = "BEGIN_TIME")
    private Date beginTime;

    @Column( name = "END_TIME")
    private Date endTime;

    @Column( name = "DURATION")
    private Integer duration;

    @Column( name = "REGIONAL_OFFICE")
    private String regionalOffice;

    @Column( name = "ERROR")
    private String error;

    @Column( name = "GLOBAL_ERROR")
    private String globalError;

    @Column( name = "REPORT_CHANNEL")
    private String reportChannel;

    @Column( name = "REPORT_BEGIN_DATE")
    private String reportBeginDate;

    @Column( name = "REPORT_BEGIN_TIME")
    private String reportBeginTime;

    @Column( name = "REPORT_DURATION")
    private String reportDuration;

    @Column( name = "SCHEDULE_YEAR")
    private Integer schedulYear;

    @Column( name = "SCHEDULE_MONTH")
    private Integer scheduleMonth;

    @Column( name = "DAYS_FROM_UPLOAD")
    private Integer daysFromUpload;

    @Column( name = "CREATION_DATE")
    private Date creationDate;

    @Column( name = "MODIFY_DATE")
    private Date modifyDate;

    public BdcErrRdShowSchedule() {
    }

    public BdcErrRdShowSchedule(BdcErrRdShowMusic errRdShowMusic, BdcCanali canale, BdcNormalizedFile normalizedFile, Date beginTimeTx, Date endTimeTx, Map<String, Object> record) {
        this.errRdShowMusicList = new ArrayList<>();
        this.errRdShowMusicList.add(errRdShowMusic);
        this.canale = canale;
        this.normalizedFile = normalizedFile;
        this.beginTime = beginTimeTx;
        this.endTime = endTimeTx;
        if( getMapValue(record, Constants.DURATA_TX) != null && getMapValue(record, Constants.DURATA_TX).matches(StringTools.DURATION_FORMAT) ){
            this.duration = Integer.parseInt( StringTools.parseTimeToSeconds( getMapValue(record, Constants.DURATA_TX) ) ) ;
        }
        this.title = getMapValue(record, Constants.TITOLO_TX);
        this.regionalOffice = getMapValue(record, Constants.DIFFUSIONE_REGIONALE);
        this.error = getMapValue(record, Constants.ERRORS);
        this.reportChannel = getMapValue(record, Constants.CANALE);
        this.reportBeginDate = getMapValue(record, Constants.DATA_INIZIO_TX);
        this.reportBeginTime = getMapValue(record, Constants.ORARIO_INIZIO_TX);
        this.reportDuration = getMapValue(record, Constants.DURATA_TX   );
        this.creationDate = new Date();
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<BdcErrRdShowMusic> getErrRdShowMusicList() {
        return errRdShowMusicList;
    }

    public void setErrRdShowMusicList(List<BdcErrRdShowMusic> errRdShowMusicList) {
        this.errRdShowMusicList = errRdShowMusicList;
    }

    public BdcNormalizedFile getNormalizedFile() {
        return normalizedFile;
    }

    public void setNormalizedFile(BdcNormalizedFile normalizedFile) {
        this.normalizedFile = normalizedFile;
    }

    public BdcCanali getCanale() {
        return canale;
    }

    public void setCanale(BdcCanali canale) {
        this.canale = canale;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getRegionalOffice() {
        return regionalOffice;
    }

    public void setRegionalOffice(String regionalOffice) {
        this.regionalOffice = regionalOffice;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getGlobalError() {
        return globalError;
    }

    public void setGlobalError(String globalError) {
        this.globalError = globalError;
    }

    public String getReportChannel() {
        return reportChannel;
    }

    public void setReportChannel(String reportChannel) {
        this.reportChannel = reportChannel;
    }

    public String getReportBeginDate() {
        return reportBeginDate;
    }

    public void setReportBeginDate(String reportBeginDate) {
        this.reportBeginDate = reportBeginDate;
    }

    public String getReportBeginTime() {
        return reportBeginTime;
    }

    public void setReportBeginTime(String reportBeginTime) {
        this.reportBeginTime = reportBeginTime;
    }

    public String getReportDuration() {
        return reportDuration;
    }

    public void setReportDuration(String reportDuration) {
        this.reportDuration = reportDuration;
    }

    public Integer getSchedulYear() {
        return schedulYear;
    }

    public void setSchedulYear(Integer schedulYear) {
        this.schedulYear = schedulYear;
    }

    public Integer getScheduleMonth() {
        return scheduleMonth;
    }

    public void setScheduleMonth(Integer scheduleMonth) {
        this.scheduleMonth = scheduleMonth;
    }

    public Integer getDaysFromUpload() {
        return daysFromUpload;
    }

    public void setDaysFromUpload(Integer daysFromUpload) {
        this.daysFromUpload = daysFromUpload;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Override
    public String toString() {
        return "BdcErrRdShowSchedule{" +
                "id=" + id +
                ", errRdShowMusicList=" + errRdShowMusicList +
                ", normalizedFile=" + normalizedFile +
                ", canale=" + canale +
                ", title='" + title + '\'' +
                ", beginTime=" + beginTime +
                ", endTime=" + endTime +
                ", duration=" + duration +
                ", regionalOffice='" + regionalOffice + '\'' +
                ", error='" + error + '\'' +
                ", globalError='" + globalError + '\'' +
                ", reportChannel='" + reportChannel + '\'' +
                ", reportBeginDate='" + reportBeginDate + '\'' +
                ", reportBeginTime='" + reportBeginTime + '\'' +
                ", reportDuration='" + reportDuration + '\'' +
                ", schedulYear=" + schedulYear +
                ", scheduleMonth=" + scheduleMonth +
                ", daysFromUpload=" + daysFromUpload +
                ", creationDate=" + creationDate +
                ", modifyDate=" + modifyDate +
                '}';
    }
}