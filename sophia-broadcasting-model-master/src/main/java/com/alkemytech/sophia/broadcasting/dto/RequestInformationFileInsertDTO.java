package com.alkemytech.sophia.broadcasting.dto;

import com.alkemytech.sophia.broadcasting.model.BdcInformationFileEntity;

import javax.validation.Valid;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RequestInformationFileInsertDTO extends RequestDTO
{
    @XmlElement(name = "RequestData")
    @Valid
    private BdcInformationFileEntity requestData;

    public RequestInformationFileInsertDTO() {
    }

    public RequestInformationFileInsertDTO(BdcInformationFileEntity requestData) {
        this.requestData = requestData;
    }

    public BdcInformationFileEntity getRequestData() {
        return requestData;
    }

    public void setRequestData(BdcInformationFileEntity requestData) {
        this.requestData = requestData;
    }
}