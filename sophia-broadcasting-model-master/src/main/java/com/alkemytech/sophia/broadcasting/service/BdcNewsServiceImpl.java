package com.alkemytech.sophia.broadcasting.service;

import com.alkemytech.sophia.broadcasting.dto.BdcNewsDTO;
import com.alkemytech.sophia.broadcasting.dto.BdcNewsListRequest;
import com.alkemytech.sophia.broadcasting.model.BdcNews;
import com.alkemytech.sophia.broadcasting.service.interfaces.BdcNewsService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.http.util.TextUtils;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BdcNewsServiceImpl implements BdcNewsService {

    private final Provider<EntityManager> provider;

    @Inject
    public BdcNewsServiceImpl(Provider<EntityManager> provider) {
        this.provider = provider;
    }

    @Override
    public List<BdcNewsDTO> findNewsForBdc(Integer idUser, Integer idBroadcaster) {
        List<BdcNewsDTO> retList = new ArrayList<>();

        if (idUser == null || idBroadcaster == null) {
            return retList;
        }

        EntityManager entityManager = provider.get();
        List<Object> listaNotizie;

        String w =
                "SELECT BDC_NEWS_ID, TITLE, NEWS, FLAG_FOR_ALL_BDC, FLAG_FOR_ALL_BDC_USER, " +
                        "IF(VALID_FROM > INSERT_DATE, VALID_FROM, INSERT_DATE) as PUBLICATION_DATE" +
                        " FROM BDC_NEWS WHERE ACTIVE = 1 AND(CURRENT_TIMESTAMP BETWEEN VALID_FROM and VALID_TO)" +
                        " AND ( (FLAG_FOR_ALL_BDC = 1 AND ID_BROADCASTER IS NULL) OR (FLAG_FOR_ALL_BDC = 0 " +
                        "AND ID_BROADCASTER =?1)) AND ( (FLAG_FOR_ALL_BDC_USER = 1 AND ID_USER IS NULL) " +
                        "OR (FLAG_FOR_ALL_BDC_USER = 0 AND ID_USER =?2) ) ORDER BY INSERT_DATE DESC";

        Query query = entityManager.createNativeQuery(w);

        listaNotizie = query.setParameter(1, idBroadcaster).setParameter(2, idUser).getResultList();

        if (listaNotizie != null) {
            for (Object record : listaNotizie) {
                if (record != null) {
                    BdcNewsDTO notizia = new BdcNewsDTO(record);
                    retList.add(notizia);
                }
            }
        }
        return retList;
    }

    @Override
    public List<BdcNewsDTO> findAll() {
        List<BdcNewsDTO> retList = new ArrayList<BdcNewsDTO>();

        EntityManager entityManager = provider.get();
        List<Object> listaNotizie;

        String w = "SELECT BDC_NEWS_ID, TITLE, NEWS, FLAG_FOR_ALL_BDC, FLAG_FOR_ALL_BDC_USER, IF(VALID_FROM > INSERT_DATE," +
                " VALID_FROM, INSERT_DATE) as PUBLICATION_DATE FROM BDC_NEWS WHERE(CURRENT_TIMESTAMP BETWEEN VALID_FROM and VALID_TO) ORDER BY INSERT_DATE DESC";

        Query query = entityManager.createNativeQuery(w);

        listaNotizie = query.getResultList();

        if (listaNotizie != null) {
            for (Object record : listaNotizie) {
                if (record != null) {
                    BdcNewsDTO notizia = new BdcNewsDTO(record);
                    retList.add(notizia);
                }
            }
        }
        return retList;
    }

    @Override
    public List<BdcNews> findAllExplose(BdcNewsListRequest bdcNewsListRequest, int first, int last) {

        EntityManager em = null;
        em = provider.get();

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<BdcNews> query = criteriaBuilder.createQuery(BdcNews.class);
        Root<BdcNews> news = query.from(BdcNews.class);

//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        List<Predicate> predicates = new ArrayList<>();
        if (null != bdcNewsListRequest.getTitolo() && !TextUtils.isEmpty(bdcNewsListRequest.getTitolo())
                && !TextUtils.isBlank(bdcNewsListRequest.getTitolo())) {
            predicates.add(criteriaBuilder.like(news.<String>get("title"), "%"+bdcNewsListRequest.getTitolo() + "%"));
        }
        if (null != bdcNewsListRequest.getTesto() && !TextUtils.isEmpty(bdcNewsListRequest.getTesto())
                && !TextUtils.isBlank(bdcNewsListRequest.getTesto())) {
            predicates.add(criteriaBuilder.like(news.<String>get("news"), "%"+bdcNewsListRequest.getTesto() + "%"));
        }
        if (null != bdcNewsListRequest.getFlagActive() && bdcNewsListRequest.getFlagActive() >= 0) {
            predicates.add(criteriaBuilder.equal(news.get("flagActive"), bdcNewsListRequest.getFlagActive()));
        }
        if (null != bdcNewsListRequest.getIdBroadcaster() && bdcNewsListRequest.getIdBroadcaster().size() > 0) {
            Expression<Boolean> exp = news.get("bdcBroadcasters").get("id");
            Predicate predicate = exp.in(bdcNewsListRequest.getIdBroadcaster());
            predicates.add(predicate);
        }
        try {
            if (null != bdcNewsListRequest.getValidaDa()) {
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(news.<Date>get("validFrom"), bdcNewsListRequest.getValidaDa()));
            }

            if (null != bdcNewsListRequest.getValidaA()) {
                predicates.add(criteriaBuilder.lessThanOrEqualTo(news.<Date>get("validTo"), bdcNewsListRequest.getValidaA()));
            }
            if (null != bdcNewsListRequest.getDataCreazioneDa()) {
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(news.<Date>get("insertDate"), (bdcNewsListRequest.getDataCreazioneDa())));
            }

            if (null != bdcNewsListRequest.getDataCreazioneA()) {
                predicates.add(criteriaBuilder.lessThanOrEqualTo(news.<Date>get("insertDate"), (bdcNewsListRequest.getDataCreazioneA())));
            }

        } catch (Exception e) {

        }
        query.select(news).where(predicates.toArray(new Predicate[]{}));

        return em.createQuery(query).setFirstResult(first).setMaxResults(1 + last - first)
                .getResultList();

    }

    @Override
    public List<BdcNews> findAllExplose(BdcNewsListRequest bdcNewsListRequest) {

        EntityManager em = null;
        em = provider.get();

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<BdcNews> query = criteriaBuilder.createQuery(BdcNews.class);
        Root<BdcNews> news = query.from(BdcNews.class);

//        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        List<Predicate> predicates = new ArrayList<>();
        if (null != bdcNewsListRequest.getTitolo() && !TextUtils.isEmpty(bdcNewsListRequest.getTitolo())
                && !TextUtils.isBlank(bdcNewsListRequest.getTitolo())) {
            predicates.add(criteriaBuilder.like(news.<String>get("title"), "%"+bdcNewsListRequest.getTitolo() + "%"));
        }
        if (null != bdcNewsListRequest.getTesto() && !TextUtils.isEmpty(bdcNewsListRequest.getTesto())
                && !TextUtils.isBlank(bdcNewsListRequest.getTesto())) {
            predicates.add(criteriaBuilder.like(news.<String>get("news"), "%"+bdcNewsListRequest.getTesto() + "%"));
        }
        if (null != bdcNewsListRequest.getFlagActive() && bdcNewsListRequest.getFlagActive() >= 0) {
            predicates.add(criteriaBuilder.equal(news.get("flagActive"), bdcNewsListRequest.getFlagActive()));
        }
        if (null != bdcNewsListRequest.getIdBroadcaster() && bdcNewsListRequest.getIdBroadcaster().size() > 0) {
            Expression<Boolean> exp = news.get("bdcBroadcasters").get("id");
            Predicate predicate = exp.in(bdcNewsListRequest.getIdBroadcaster());
            predicates.add(predicate);
        }
        try {
            if (null != bdcNewsListRequest.getValidaDa()) {
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(news.<Date>get("validFrom"), bdcNewsListRequest.getValidaDa()));
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
        try {
            if (null != bdcNewsListRequest.getValidaA()) {
                predicates.add(criteriaBuilder.lessThanOrEqualTo(news.<Date>get("validTo"), bdcNewsListRequest.getValidaA()));
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
        query.select(news).where(predicates.toArray(new Predicate[]{}));
        List<BdcNews> newsResult = em.createQuery(query).getResultList();
        for (BdcNews bdcNews : newsResult) {
            bdcNews.setTransitionalValue();
        }
        return newsResult;
    }

    @Override
    public BdcNews findbyId(Integer id) {

        EntityManager em = null;
        em = provider.get();

        final Query q = em.createQuery("SELECT a FROM BdcNews a where a.id=:id").setParameter("id", id);

        return (BdcNews) q.getSingleResult();
    }

    @Override
    public boolean addNews(BdcNews bdcNews) {
        EntityManager entityManager = provider.get();
        entityManager.getTransaction().begin();
        try {
            entityManager.persist(bdcNews);
            entityManager.getTransaction().commit();
            return true;
        } catch (Exception e) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            return false;
        }
    }

    @Override
    public boolean updateNews(BdcNews bdcNews) {
        EntityManager entityManager = provider.get();
        entityManager.getTransaction().begin();
        try {
            BdcNews modifyNews = findbyId(bdcNews.getId());
            modifyNews.setTitle(bdcNews.getTitle());
            modifyNews.setBdcBroadcasters(bdcNews.getBdcBroadcasters());
            modifyNews.setIdUtente(bdcNews.getIdUtente());
            modifyNews.setNews(bdcNews.getNews());
            modifyNews.setValidFrom(bdcNews.getValidFrom());
            modifyNews.setValidTo(bdcNews.getValidTo());
            modifyNews.setFlagForAllBdc(bdcNews.getFlagForAllBdc());
            modifyNews.setFlagForAllBdcUser(bdcNews.getFlagForAllBdcUser());
            entityManager.merge(modifyNews);
            entityManager.getTransaction().commit();
            return true;
        } catch (Exception e) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            return false;
        }
    }

    @Override
    public boolean deleteNews(BdcNews bdcNews) {

        EntityManager entityManager = provider.get();
        entityManager.getTransaction().begin();
        try {
            entityManager.remove(findbyId(bdcNews.getId()));
            entityManager.getTransaction().commit();
            return true;
        } catch (Exception e) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            return false;
        }
    }

    @Override
    public boolean deactivateNews(BdcNews bdcNews) {

        EntityManager entityManager = provider.get();
        entityManager.getTransaction().begin();
        try {
            BdcNews deativateNews = findbyId(bdcNews.getId());
            deativateNews.setFlagActive(false);
            deativateNews.setDeactivationDate(new Date());
            entityManager.merge(deativateNews);
            entityManager.getTransaction().commit();
            return true;
        } catch (Exception e) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            return false;
        }
    }

    @Override
    public boolean activateNews(BdcNews bdcNews) {
        EntityManager entityManager = provider.get();
        entityManager.getTransaction().begin();
        try {
            BdcNews deativateNews = findbyId(bdcNews.getId());
            deativateNews.setFlagActive(true);
            deativateNews.setDeactivationDate(null);
            entityManager.merge(deativateNews);
            entityManager.getTransaction().commit();
            return true;
        } catch (Exception e) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            return false;
        }
    }

}