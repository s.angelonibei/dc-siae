
package com.alkemytech.sophia.broadcasting.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.validation.Valid;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.util.List;


@XmlAccessorType(XmlAccessType.FIELD)
public class MessaggiDTO implements Serializable
{

    @Expose
    @Valid
    @XmlElement(name = "item")
    @SerializedName("item")
    private List<ItemDTO> itemDTO = null;
    private final static long serialVersionUID = -7759952005116889181L;

    public List<ItemDTO> getItemDTO() {
        return itemDTO;
    }

    public void setItemDTO(List<ItemDTO> itemDTO) {
        this.itemDTO = itemDTO;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("itemDTO", itemDTO).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(itemDTO).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof MessaggiDTO) == false) {
            return false;
        }
        MessaggiDTO rhs = ((MessaggiDTO) other);
        return new EqualsBuilder().append(itemDTO, rhs.itemDTO).isEquals();
    }

}
