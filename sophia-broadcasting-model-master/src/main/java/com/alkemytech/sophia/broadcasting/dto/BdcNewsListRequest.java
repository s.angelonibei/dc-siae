package com.alkemytech.sophia.broadcasting.dto;


import com.alkemytech.sophia.broadcasting.utils.DateAdapter;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BdcNewsListRequest {
	private String titolo;
	private String testo;
	@XmlJavaTypeAdapter(value = DateAdapter.class, type = Date.class)
	private Date validaDa;
	@XmlJavaTypeAdapter(value = DateAdapter.class, type = Date.class)
	private Date validaA;
	@XmlJavaTypeAdapter(value = DateAdapter.class, type = Date.class)
	private Date dataCreazioneDa;
	@XmlJavaTypeAdapter(value = DateAdapter.class, type = Date.class)
	private Date dataCreazioneA;
	private Integer flagActive;
	private List<Integer> idBroadcaster;
	private int first;
	private int last;
	
	public BdcNewsListRequest() {
		super();
	}

	public BdcNewsListRequest(String titolo, String testo, Date validaDa, Date validaA, Date dataCreazioneDa, Date dataCreazioneA, Integer flagActive, List<Integer> idBroadcaster, int first, int last) {
		this.titolo = titolo;
		this.testo = testo;
		this.validaDa = validaDa;
		this.validaA = validaA;
		this.dataCreazioneDa = dataCreazioneDa;
		this.dataCreazioneA = dataCreazioneA;
		this.flagActive = flagActive;
		this.idBroadcaster = idBroadcaster;
		this.first = first;
		this.last = last;
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	public String getTesto() {
		return testo;
	}

	public void setTesto(String testo) {
		this.testo = testo;
	}

	public Date getValidaDa() {
		return validaDa;
	}

	public void setValidaDa(Date validaDa) {
		this.validaDa = validaDa;
	}

	public Date getValidaA() {
		return validaA;
	}

	public void setValidaA(Date validaA) {
		this.validaA = validaA;
	}

	public Date getDataCreazioneDa() {
		return dataCreazioneDa;
	}

	public void setDataCreazioneDa(Date dataCreazioneDa) {
		this.dataCreazioneDa = dataCreazioneDa;
	}

	public Date getDataCreazioneA() {
		return dataCreazioneA;
	}

	public void setDataCreazioneA(Date dataCreazioneA) {
		this.dataCreazioneA = dataCreazioneA;
	}

	public Integer getFlagActive() {
		return flagActive;
	}

	public void setFlagActive(Integer flagActive) {
		this.flagActive = flagActive;
	}

	public List<Integer> getIdBroadcaster() {
		return idBroadcaster;
	}

	public void setIdBroadcaster(List<Integer> idBroadcaster) {
		this.idBroadcaster = idBroadcaster;
	}

	public int getFirst() {
		return first;
	}

	public void setFirst(int first) {
		this.first = first;
	}

	public int getLast() {
		return last;
	}

	public void setLast(int last) {
		this.last = last;
	}
}
