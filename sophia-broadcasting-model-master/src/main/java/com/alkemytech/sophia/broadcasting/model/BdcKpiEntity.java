package com.alkemytech.sophia.broadcasting.model;

import com.alkemytech.sophia.broadcasting.dto.IndicatoreDTO;
import com.alkemytech.sophia.broadcasting.dto.KPI;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;

@XmlRootElement
@Entity
@Table(name = "bdc_kpi")
@SqlResultSetMapping(name="BdcIndicatoreMapping",
        classes = {
                @ConstructorResult(targetClass = IndicatoreDTO.class,
                        columns = {
                                @ColumnResult(name="indicatore", type = String.class),
                                @ColumnResult(name="valore", type = Integer.class),
                                @ColumnResult(name="lastUpdate", type = Date.class)

                        }
                )
        }
)
public class BdcKpiEntity{
    public static final String DURATA_DISPONIBILE = "Durata disponibile";
    public static final String DURATA_FILM = "Durata film";
    public static final String DURATA_AUDIOVISIVI_CONTENUTI = "Durata audiovisivi contenuti";
    public static final String DURATA_GENERI_VUOTI = "Durata generi vuoti";
    public static final String DURATA_PUBBLICITA = "Durata pubblicita";
    public static final String DURATA_TRASMISSIONE = "Durata trasmissione";
    public static final String RECORD_IN_RITARDO = "Record in ritardo";
    public static final String RECORD_SCARTATI = "Record scartati";
    public static final String RECORD_TOTALI = "Record totali";
    public static final String DURATA_BRANI = "Durata brani";
    public static final String DURATA_BRANI_DICHIARATI = "Durata totale brani dichiarati";
    public static final String DURATA_BRANI_TITOLI_ERRONEI = "Durata brani titoli erronei";
    public static final String DURATA_BRANI_AUTORI_ERRONEI = "Durata brani autori erronei";
    public static final String DURATA_FILM_TITOLI_ERRONEI = "Durata film titoli erronei";
    public static final String DURATA_PROGRAMMI_TITOLI_ERRONEI = "Durata programmi titoli erronei";
    public static final String TRASMISSIONI_MUSICA_ECCEDENTE = "Trasmissioni musica eccedente";
    public static final String TRASMISSIONI_TOTALI = "Trasmissioni totali";

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="ID", nullable=false)
    private int id;
    @OneToOne
    @JoinColumn(name="emittente")
    private BdcBroadcasters emittente;
    @OneToOne
    @JoinColumn(name="canale")
    private BdcCanali canale;
    @Column(name="anno")
    private int anno;
    @Column(name="mese")
    private int mese;
    @Column(name="indicatore")
    private String indicatore;
    @Column(name="valore")
    private Long valore;
    @Column(name = "last_update")
    private Date lastUpdate;

    @Transient
    private List<BdcCanali> canali;
    @Transient
    private List<Short> mesi;
    @Transient
    private List<KPI> listaKPI;

    public BdcKpiEntity() {
    }

    public BdcKpiEntity(BdcBroadcasters emittente, BdcCanali canale, int anno, int mese, String indicatore, Long valore, Date lastUpdate, List<Short> mesi, List<KPI> listaKPI, List<BdcCanali> canali) {
        this.emittente = emittente;
        this.canale = canale;
        this.anno = anno;
        this.mese = mese;
        this.indicatore = indicatore;
        this.valore = valore;
        this.lastUpdate = lastUpdate;
        this.mesi = mesi;
        this.listaKPI = listaKPI;
        this.canali = canali;
    }

    @Transient
    public List<Short> getMesi() {
        return mesi;
    }

    public String getMesiToString() {
        String sMesi = "";
        for (Short m: mesi
                ) {
            if(!sMesi.equals("")) {
                sMesi += "," + m;
            }else{
                sMesi += m;
            }
        }
        return sMesi;
    }

    public void setMesi(List<Short> mesi) {
        this.mesi = mesi;
    }

    @Transient
    public List<BdcCanali> getCanali() {
        return canali;
    }

    public String getCanaliToString() {
        String sCanali = "";
        for (BdcCanali n: canali
                ) {
            if(!sCanali.equals("")) {
                sCanali += "," + n;
            }else{
                sCanali += n;
            }
        }
        return sCanali;
    }

    public void setCanali(List<BdcCanali> canali) {
        this.canali = canali;
    }

    @Transient
    public List<KPI> getListaKPI() {
        return listaKPI;
    }

    public void setListaKPI(List<KPI> listaKPI) {
        this.listaKPI = listaKPI;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BdcBroadcasters getEmittente() {
        return emittente;
    }

    public void setEmittente(BdcBroadcasters emittente) {
        this.emittente = emittente;
    }

    public BdcCanali getCanale() {
        return canale;
    }

    public void setCanale(BdcCanali canale) {
        this.canale = canale;
    }

    public int getAnno() {
        return anno;
    }

    public void setAnno(int anno) {
        this.anno = anno;
    }

    public int getMese() {
        return mese;
    }

    public void setMese(int mese) {
        this.mese = mese;
    }

    public String getIndicatore() {
        return indicatore;
    }

    public void setIndicatore(String indicatore) {
        this.indicatore = indicatore;
    }

    public Long getValore() {
        return valore;
    }

    public void setValore(Long valore) {
        this.valore = valore;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BdcKpiEntity that = (BdcKpiEntity) o;

        if (id != that.id) return false;
        if (anno != that.anno) return false;
        if (mese != that.mese) return false;
        if (!emittente.equals(that.emittente)) return false;
        if (!canale.equals(that.canale)) return false;
        if (!indicatore.equals(that.indicatore)) return false;
        if (!valore.equals(that.valore)) return false;
        if (!lastUpdate.equals(that.lastUpdate)) return false;
        if (!canali.equals(that.canali)) return false;
        if (!mesi.equals(that.mesi)) return false;
        return listaKPI.equals(that.listaKPI);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + emittente.hashCode();
        result = 31 * result + canale.hashCode();
        result = 31 * result + anno;
        result = 31 * result + mese;
        result = 31 * result + indicatore.hashCode();
        result = 31 * result + valore.hashCode();
        result = 31 * result + lastUpdate.hashCode();
        result = 31 * result + canali.hashCode();
        result = 31 * result + mesi.hashCode();
        result = 31 * result + listaKPI.hashCode();
        return result;
    }
}