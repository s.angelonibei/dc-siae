package com.alkemytech.sophia.broadcasting.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.broadcasting.model.BdcDettaglioPianoDeiConti;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BdcUpdatePianoContiRequestDTO {
	private int idPianoDeiConti;
	private String conto;
	private String utente;
	private List<BdcDettaglioPianoDeiConti> dettaglioPianoDeiConti;

	public List<BdcDettaglioPianoDeiConti> getDettaglioPianoDeiConti() {
		return dettaglioPianoDeiConti;
	}

	public void setDettaglioPianoDeiConti(List<BdcDettaglioPianoDeiConti> dettaglioPianoDeiConti) {
		this.dettaglioPianoDeiConti = dettaglioPianoDeiConti;
	}

	public int getIdPianoDeiConti() {
		return idPianoDeiConti;
	}

	public void setIdPianoDeiConti(int idPianoDeiConti) {
		this.idPianoDeiConti = idPianoDeiConti;
	}

	public String getConto() {
		return conto;
	}

	public void setConto(String conto) {
		this.conto = conto;
	}

	public String getUtente() {
		return utente;
	}

	public void setUtente(String utente) {
		this.utente = utente;
	}
	
}
