package com.alkemytech.sophia.broadcasting.service.performing;

import com.alkemytech.sophia.broadcasting.data.IndicatoriPerCanale;
import com.alkemytech.sophia.broadcasting.dto.IndicatoreDTO;
import com.alkemytech.sophia.broadcasting.dto.performing.RSKPI;
import com.alkemytech.sophia.broadcasting.model.BdcKpiEntity;
import com.alkemytech.sophia.broadcasting.model.TipoBroadcaster;
import com.alkemytech.sophia.broadcasting.service.interfaces.KpiService;
import com.google.inject.Inject;
import com.google.inject.Provider;

import javax.persistence.EntityManager;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

public class RsKpiServiceImpl implements RsKpiService {

    private Provider<EntityManager> provider;

    @Inject
    private  RsIndicatoreServiceImpl rsIndicatoreService;

    public RsKpiServiceImpl(RsIndicatoreServiceImpl rsIndicatoreService) {
        this.rsIndicatoreService = rsIndicatoreService;
    }

    @Override
    public RSKPI calcolaKpiRecordScartati(HashMap<String, IndicatoreDTO> mapIndicatori) {
        RSKPI kpi = new RSKPI(true, RSKPI.KPI_RECORD_ACCETTATI, RSKPI.DESC_KPI_RECORD_ACCETTATI, BigDecimal.valueOf(0));

        long recordScartati = 0;
        long recordValidi = 0;

        if (mapIndicatori.containsKey(BdcKpiEntity.RECORD_TOTALI)) {
            recordValidi = mapIndicatori.get(BdcKpiEntity.RECORD_TOTALI).getValore();
        }

        if (mapIndicatori.containsKey(BdcKpiEntity.RECORD_SCARTATI)) {
            recordScartati = mapIndicatori.get(BdcKpiEntity.RECORD_SCARTATI).getValore();
        }

        if(recordValidi + recordScartati != 0){
            BigDecimal valoreKPI = BigDecimal.valueOf(100).subtract(BigDecimal.valueOf(100 * recordScartati).divide(BigDecimal.valueOf(recordValidi + recordScartati), 2, RoundingMode.HALF_UP));
            kpi.setValore(valoreKPI);
        }

        return kpi;
    }

    @Override
    public RSKPI calcolaKpiBraniTitoliErronei(HashMap<String, IndicatoreDTO> mapIndicatori) {
        RSKPI kpi = new RSKPI(true, RSKPI.KPI_BRANI_TITOLI_ERRONEI, RSKPI.DESC_KPI_BRANI_TITOLI_ERRONEI, BigDecimal.valueOf(0));
        long durataBraniTitoliErronei = 0;
        long durataBraniTotaliDichiarati = 0;

        if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_BRANI_DICHIARATI)) {
            durataBraniTotaliDichiarati = mapIndicatori.get(BdcKpiEntity.DURATA_BRANI_DICHIARATI).getValore();
        }
        if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_BRANI_TITOLI_ERRONEI)) {
            durataBraniTitoliErronei = mapIndicatori.get(BdcKpiEntity.DURATA_BRANI_TITOLI_ERRONEI).getValore();
        }

        if(durataBraniTotaliDichiarati != 0){
            BigDecimal valoreKPI = BigDecimal.valueOf(100 * durataBraniTitoliErronei).divide(BigDecimal.valueOf(durataBraniTotaliDichiarati), 2, RoundingMode.HALF_UP);
            kpi.setValore(valoreKPI);
        }
        return kpi;
    }

    @Override
    public RSKPI calcolaKpiAutoriTitoliErronei(HashMap<String, IndicatoreDTO> mapIndicatori) {
        RSKPI kpi = new RSKPI(true, RSKPI.KPI_AUTORI_TITOLI_ERRONEI, RSKPI.DESC_KPI_AUTORI_TITOLI_ERRONEI, BigDecimal.valueOf(0));
        long durataBraniRdAutoriTitoliErronei = 0;
        long durataBraniDichiarati = 0;

        if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_BRANI_AUTORI_ERRONEI)) {
            durataBraniRdAutoriTitoliErronei = mapIndicatori.get(BdcKpiEntity.DURATA_BRANI_AUTORI_ERRONEI).getValore();
        }
        if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_BRANI)) {
            durataBraniDichiarati = mapIndicatori.get(BdcKpiEntity.DURATA_BRANI).getValore();
        }

        if(durataBraniDichiarati != 0){
            BigDecimal valoreKPI = BigDecimal.valueOf(100 * durataBraniRdAutoriTitoliErronei).divide(BigDecimal.valueOf(durataBraniDichiarati), 2, RoundingMode.HALF_UP);
            kpi.setValore(valoreKPI);
        }
        return kpi;
    }

    @Override
    public RSKPI calcolaKpiFilmTitoliErronei(HashMap<String, IndicatoreDTO> mapIndicatori) {
        RSKPI kpi = new RSKPI(true, RSKPI.KPI_FILM_TITOLI_ERRONEI, RSKPI.DESC_KPI_FILM_TITOLI_ERRONEI, BigDecimal.valueOf(0));
        long durataFilmTitoliErronei = 0;
        long durataFilm = 0;

        if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_FILM)) {
            durataFilm = mapIndicatori.get(BdcKpiEntity.DURATA_FILM).getValore();
        }
        if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_FILM_TITOLI_ERRONEI)) {
            durataFilmTitoliErronei = mapIndicatori.get(BdcKpiEntity.DURATA_FILM_TITOLI_ERRONEI).getValore();
        }

        if(durataFilm != 0){
            BigDecimal valoreKPI = BigDecimal.valueOf(100 * durataFilmTitoliErronei).divide(BigDecimal.valueOf(durataFilm), 2, RoundingMode.HALF_UP);
            kpi.setValore(valoreKPI);
        }

        return kpi;
    }

    @Override
    public RSKPI calcolaKpiProgrammiTitoliErronei(HashMap<String, IndicatoreDTO> mapIndicatori) {
        RSKPI kpi = new RSKPI(true, RSKPI.KPI_PROGRAMMI_TITOLI_ERRONEI, RSKPI.DESC_KPI_PROGRAMMI_TITOLI_ERRONEI, BigDecimal.valueOf(0));
        long durataTrasmissioniTitoliErronei = 0;
        long durataTrasmissioni = 0;

        if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_TRASMISSIONE)) {
            durataTrasmissioni = mapIndicatori.get(BdcKpiEntity.DURATA_TRASMISSIONE).getValore();
        }
        if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_PROGRAMMI_TITOLI_ERRONEI)) {
            durataTrasmissioniTitoliErronei = mapIndicatori.get(BdcKpiEntity.DURATA_PROGRAMMI_TITOLI_ERRONEI).getValore();
        }

        if(durataTrasmissioni != 0){
            BigDecimal valoreKPI = BigDecimal.valueOf(100 * durataTrasmissioniTitoliErronei).divide(BigDecimal.valueOf(durataTrasmissioni), 2, RoundingMode.HALF_UP);
            kpi.setValore(valoreKPI);
        }

        return kpi;
    }

    @Override
    public RSKPI calcolaKpiMusicaEccedenteTrasmissione(HashMap<String, IndicatoreDTO> mapIndicatori) {
        RSKPI kpi = new RSKPI(true, RSKPI.KPI_TRASMISSIONI_MUSICHE_ECCEDENTI, RSKPI.DESC_KPI_TRASMISSIONI_MUSICHE_ECCEDENTI, BigDecimal.valueOf(0));
        long trasmissioniEccedenti = 0;
        long trasmissioniValide = 0;

        if (mapIndicatori.containsKey(BdcKpiEntity.TRASMISSIONI_MUSICA_ECCEDENTE)) {
            trasmissioniEccedenti = mapIndicatori.get(BdcKpiEntity.TRASMISSIONI_MUSICA_ECCEDENTE).getValore();
        }
        if (mapIndicatori.containsKey(BdcKpiEntity.TRASMISSIONI_TOTALI)) {
            trasmissioniValide = mapIndicatori.get(BdcKpiEntity.TRASMISSIONI_TOTALI).getValore();
        }

        if(trasmissioniValide != 0){
            BigDecimal valoreKPI = BigDecimal.valueOf(100 * trasmissioniEccedenti).divide(BigDecimal.valueOf(trasmissioniValide), 2, RoundingMode.HALF_UP);
            kpi.setValore(valoreKPI);
        }

        return kpi;
    }

    @Override
    public RSKPI calcolaKpiDurataMusicaDichiarata(HashMap<String, IndicatoreDTO> mapIndicatori) {
        RSKPI kpi = new RSKPI(true, RSKPI.KPI_DURATA_MUSICA_DICHIARATA, RSKPI.DESC_KPI_DURATA_MUSICA_DICHIARATA, BigDecimal.valueOf(0));
        long durataBrani = 0;
        long durataFilm = 0;
        long durataPubblicita = 0;
        long durataTrasmissioni = 0;

        if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_BRANI)) {
            durataBrani = mapIndicatori.get(BdcKpiEntity.DURATA_BRANI).getValore();
        }
        if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_FILM)) {
            durataFilm = mapIndicatori.get(BdcKpiEntity.DURATA_FILM).getValore();
        }
        if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_TRASMISSIONE)) {
            durataTrasmissioni = mapIndicatori.get(BdcKpiEntity.DURATA_TRASMISSIONE).getValore();
        }
        if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_PUBBLICITA)) {
            durataPubblicita = mapIndicatori.get(BdcKpiEntity.DURATA_PUBBLICITA).getValore();
        }

        if((durataTrasmissioni + durataFilm + durataPubblicita) != 0){
            BigDecimal valoreKPI = (BigDecimal.valueOf(100 * (durataBrani + durataFilm * 0.6)).divide(BigDecimal.valueOf(durataTrasmissioni + durataFilm + durataPubblicita), 2, RoundingMode.HALF_UP));
            kpi.setValore(valoreKPI);
        }

        return kpi;
    }

    @Override
    public RSKPI calcolaKpiRecordInRitardo(HashMap<String, IndicatoreDTO> mapIndicatori) {
        RSKPI kpi = new RSKPI(true, RSKPI.KPI_RECORD_IN_TEMPO, RSKPI.DESC_KPI_RECORD_IN_TEMPO, BigDecimal.valueOf(0));
        long recordInRitardo = 0;
        long recordTotali = 0;
        if (mapIndicatori.containsKey(BdcKpiEntity.RECORD_TOTALI)) {
            recordTotali = mapIndicatori.get(BdcKpiEntity.RECORD_TOTALI).getValore();
        }
        if (mapIndicatori.containsKey(BdcKpiEntity.RECORD_IN_RITARDO)) {
            recordInRitardo = mapIndicatori.get(BdcKpiEntity.RECORD_IN_RITARDO).getValore();
        }

        if(recordTotali != 0){
            BigDecimal valoreKPI = BigDecimal.valueOf(100).subtract(BigDecimal.valueOf(100 * recordInRitardo).divide(BigDecimal.valueOf(recordTotali), 2, RoundingMode.HALF_UP));
            kpi.setValore(valoreKPI);
        }

        return kpi;
    }


    @Override
    public RSKPI calcolaKpiDurataBraniDichiaratiRD(long durataMusiche,Integer anno, List<Short> mesi){
        long durataDisponibile = rsIndicatoreService.calcolaDurataDisponibile( anno, mesi);
        return calcolaKpiDurataBraniDichiaratiRD(durataMusiche,  durataDisponibile);
    }

    @Override
    public RSKPI calcolaKpiDurataBraniDichiaratiRD(long durataMusiche, Date dataDa, Date dataA){
        long durataDisponibile = rsIndicatoreService.calcolaDurataDisponibile(dataDa, dataA);
        return calcolaKpiDurataBraniDichiaratiRD(durataMusiche,  durataDisponibile);
    }


    private RSKPI calcolaKpiDurataBraniDichiaratiRD(long durataMusiche, long durataDisponibile) {
        RSKPI kpi = new RSKPI(true, RSKPI.KPI_DURATA_BRANI_DICHIARATI, RSKPI.DESC_KPI_DURATA_BRANI_DICHIARATI, BigDecimal.valueOf(0));
        if(durataDisponibile != 0){
            BigDecimal valoreKPI = BigDecimal.valueOf(100 * durataMusiche).divide(BigDecimal.valueOf(durataDisponibile), 2, RoundingMode.HALF_UP);
            kpi.setValore(valoreKPI);
        }
        return kpi;
    }


    @Override
    public RSKPI calcolaKpiBraniTitoliErroneiRD(HashMap<String, IndicatoreDTO> mapIndicatori) {
        RSKPI kpi = new RSKPI(true, RSKPI.KPI_BRANI_RD_TITOLI_ERRONEI, RSKPI.DESC_KPI_BRANI_RD_TITOLI_ERRONEI, BigDecimal.valueOf(0));
        long durataBraniRdTitoliErronei = 0;
        long durataBraniDichiarati = 0;

        if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_BRANI_TITOLI_ERRONEI)) {
            durataBraniRdTitoliErronei = mapIndicatori.get(BdcKpiEntity.DURATA_BRANI_TITOLI_ERRONEI).getValore();
        }
        if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_BRANI)) {
            durataBraniDichiarati = mapIndicatori.get(BdcKpiEntity.DURATA_BRANI).getValore();
        }

        if(durataBraniDichiarati != 0){
            BigDecimal valoreKPI = BigDecimal.valueOf(100 * durataBraniRdTitoliErronei).divide(BigDecimal.valueOf(durataBraniDichiarati), 2, RoundingMode.HALF_UP);
            kpi.setValore(valoreKPI);
        }
        return kpi;
    }

    @Override
    public RSKPI calcolaKpiAutoriTitoliErroneiRD(HashMap<String, IndicatoreDTO> mapIndicatori) {
        RSKPI kpi = new RSKPI(true, RSKPI.KPI_AUTORI_RD_TITOLI_ERRONEI, RSKPI.DESC_KPI_AUTORI_RD_TITOLI_ERRONEI, BigDecimal.valueOf(0));
        long durataBraniRdAutoriTitoliErronei = 0;
        long durataBraniDichiarati = 0;

        if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_BRANI_AUTORI_ERRONEI)) {
            durataBraniRdAutoriTitoliErronei = mapIndicatori.get(BdcKpiEntity.DURATA_BRANI_AUTORI_ERRONEI).getValore();
        }
        if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_BRANI)) {
            durataBraniDichiarati = mapIndicatori.get(BdcKpiEntity.DURATA_BRANI).getValore();
        }

        if(durataBraniDichiarati != 0){
            BigDecimal valoreKPI = BigDecimal.valueOf(100 * durataBraniRdAutoriTitoliErronei).divide(BigDecimal.valueOf(durataBraniDichiarati), 2, RoundingMode.HALF_UP);
            kpi.setValore(valoreKPI);
        }
        return kpi;
    }



    public RSKPI calcolaKpiCopertura(long durataFilm, long durataGeneriVuoti, long durataPubblicita,
                                   long durataTrasmissioni,Integer anno, List<Short> mesi)
    {
        long durataDisponibile = rsIndicatoreService.calcolaDurataDisponibile( anno, mesi);
        return calcolaKpiCopertura(durataFilm, durataGeneriVuoti, durataPubblicita, durataTrasmissioni, durataDisponibile);

    }

    public RSKPI calcolaKpiCopertura(long durataFilm, long durataGeneriVuoti, long durataPubblicita,
                                   long durataTrasmissioni, Date dataDa, Date dataA)
    {
        long durataDisponibile = rsIndicatoreService.calcolaDurataDisponibile(dataDa, dataA);
        return calcolaKpiCopertura(durataFilm, durataGeneriVuoti, durataPubblicita, durataTrasmissioni, durataDisponibile);
    }


    private RSKPI calcolaKpiCopertura(long durataFilm, long durataGeneriVuoti, long durataPubblicita,
                                    long durataTrasmissioni, long durataDisponibile)
    {
        RSKPI kpi = new RSKPI(true, RSKPI.KPI_COPERTURA, RSKPI.DESC_KPI_COPERTURA, BigDecimal.valueOf(0));
        if(durataDisponibile != 0){
            BigDecimal valoreKPI = BigDecimal.valueOf(100 * (durataGeneriVuoti + durataFilm + durataPubblicita + durataTrasmissioni)).divide(BigDecimal.valueOf(durataDisponibile), 2, RoundingMode.HALF_UP);
            kpi.setValore(valoreKPI);
        }
        return kpi;
    }


	@Override
	public HashMap<Integer, List<RSKPI>> findKpiRelativi(Date dateFrom, Date dateTo, TipoBroadcaster tipoBroadcaster) {
        HashMap<Integer, List<RSKPI>> retKPIMap = new HashMap<Integer, List<RSKPI>>();

        if(dateFrom != null && dateTo != null && tipoBroadcaster != null ){

            Calendar calendar = new GregorianCalendar();
            calendar.setTime(dateFrom);
            Integer annoDa = calendar.get(Calendar.YEAR);
            Integer meseDa = calendar.get(Calendar.MONTH) + 1;

            calendar.setTime(dateTo);
            Integer annoA = calendar.get(Calendar.YEAR);
            Integer meseA = calendar.get(Calendar.MONTH) + 1;

            HashMap<Integer, Map<String,Long>> mapCanaleIndicatore = rsIndicatoreService.mapCanaleIndicatore(
                    annoDa,meseDa,annoA,meseA);

            for(Integer idCanale : mapCanaleIndicatore.keySet()){

                Map<String,Long> mapIndicatori = mapCanaleIndicatore.get(idCanale);

                List<RSKPI> kpiList = calcolaListaKpi(tipoBroadcaster, mapIndicatori, dateFrom, dateTo);
                retKPIMap.put(idCanale, kpiList);
            }

        }

		return retKPIMap;
	}

    private List<RSKPI> calcolaListaKpi(TipoBroadcaster tipoBroadcaster, Map<String, Long> mapIndicatori,
                                      Date dateFrom, Date dateTo)
    {

        List<RSKPI> listaKPI = new ArrayList<RSKPI>();

        HashMap<String, IndicatoreDTO> mapIndicatoriDTO = convertiMapIndicatori(mapIndicatori);

        if(TipoBroadcaster.TELEVISIONE.equals(tipoBroadcaster)){
            long durataFilm = 0;
            long durataGeneriVuoti =0;
            long durataPubblicita = 0;
            long durataTrasmissioni = 0;
            if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_FILM)) {
                durataFilm = mapIndicatori.get(BdcKpiEntity.DURATA_FILM);
            }
            if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_PUBBLICITA)) {
                durataPubblicita = mapIndicatori.get(BdcKpiEntity.DURATA_PUBBLICITA);
            }
            if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_TRASMISSIONE)) {
                durataTrasmissioni = mapIndicatori.get(BdcKpiEntity.DURATA_TRASMISSIONE);
            }
            if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_GENERI_VUOTI)) {
                durataGeneriVuoti = mapIndicatori.get(BdcKpiEntity.DURATA_GENERI_VUOTI);
            }
            listaKPI.add(calcolaKpiCopertura(durataFilm, durataGeneriVuoti, durataPubblicita,
                    durataTrasmissioni, dateFrom, dateTo));
            listaKPI.add(calcolaKpiDurataMusicaDichiarata(mapIndicatoriDTO));
            listaKPI.add(calcolaKpiBraniTitoliErronei(mapIndicatoriDTO));
            listaKPI.add(calcolaKpiFilmTitoliErronei(mapIndicatoriDTO));
            listaKPI.add(calcolaKpiAutoriTitoliErronei(mapIndicatoriDTO));

        }else if(TipoBroadcaster.RADIO.equals(tipoBroadcaster)){
            long durataMusiche = 0;
            if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_BRANI)) {
                durataMusiche = mapIndicatori.get(BdcKpiEntity.DURATA_BRANI);
            }
            listaKPI.add(calcolaKpiDurataBraniDichiaratiRD(durataMusiche, dateFrom, dateTo));
            listaKPI.add(calcolaKpiBraniTitoliErroneiRD(mapIndicatoriDTO));
            listaKPI.add(calcolaKpiAutoriTitoliErroneiRD(mapIndicatoriDTO));

        }

        return listaKPI;
    }

    private HashMap<String, IndicatoreDTO> convertiMapIndicatori(Map<String, Long> mapIndicatori){
        HashMap<String, IndicatoreDTO> retMap = new HashMap<String, IndicatoreDTO>();

        for(String indicatore : mapIndicatori.keySet()){
            IndicatoreDTO bdcIndicatoreDTO = new IndicatoreDTO();
            bdcIndicatoreDTO.setValore(mapIndicatori.get(indicatore));
            bdcIndicatoreDTO.setIndicatore(indicatore);

            retMap.put(indicatore,bdcIndicatoreDTO);
        }

        return retMap;
    }

	@Override
	public List<RSKPI> listKpi(Date dateFrom, Date dateTo, Integer idCanale,
			TipoBroadcaster tipoBroadcaster,
			IndicatoriPerCanale indicatoriPerCanale)
	{
		if(dateFrom != null && dateTo != null && dateFrom.before(dateTo) && idCanale != null
				&& tipoBroadcaster != null && indicatoriPerCanale != null )
		{
			Map<String, Long> mapIndicatori = indicatoriPerCanale.mapIndicatori(idCanale, dateFrom, dateTo);
			if(mapIndicatori != null){
				List<RSKPI> listaKPI = new ArrayList<RSKPI>();
				HashMap<String, IndicatoreDTO> mapIndicatoriDTO = convertiMapIndicatori(mapIndicatori);
				if(TipoBroadcaster.TELEVISIONE.equals(tipoBroadcaster)){
		            long durataFilm = 0;
		            long durataGeneriVuoti = 0;
                    long durataPubblicita = 0;
		            long durataTrasmissioni = 0;
		            if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_FILM)) {
		                durataFilm = mapIndicatori.get(BdcKpiEntity.DURATA_FILM);
		            }
		            if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_PUBBLICITA)) {
		                durataPubblicita = mapIndicatori.get(BdcKpiEntity.DURATA_PUBBLICITA);
		            }
		            if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_TRASMISSIONE)) {
		                durataTrasmissioni = mapIndicatori.get(BdcKpiEntity.DURATA_TRASMISSIONE);
		            }
                    if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_GENERI_VUOTI)) {
                        durataGeneriVuoti = mapIndicatori.get(BdcKpiEntity.DURATA_GENERI_VUOTI);
                    }
		            listaKPI.add(calcolaKpiCopertura( durataFilm, durataGeneriVuoti, durataPubblicita,
		                    durataTrasmissioni, dateFrom, dateTo));
		            listaKPI.add(calcolaKpiDurataMusicaDichiarata(mapIndicatoriDTO));
		            listaKPI.add(calcolaKpiBraniTitoliErronei(mapIndicatoriDTO));
		            listaKPI.add(calcolaKpiFilmTitoliErronei(mapIndicatoriDTO));
		            listaKPI.add(calcolaKpiAutoriTitoliErronei(mapIndicatoriDTO));
	
		        }else if(TipoBroadcaster.RADIO.equals(tipoBroadcaster)){
		            long durataMusiche = 0;
		            if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_BRANI)) {
		                durataMusiche = mapIndicatori.get(BdcKpiEntity.DURATA_BRANI);
		            }
		            listaKPI.add(calcolaKpiDurataBraniDichiaratiRD(durataMusiche, dateFrom, dateTo));
		            listaKPI.add(calcolaKpiBraniTitoliErroneiRD(mapIndicatoriDTO));
		            listaKPI.add(calcolaKpiAutoriTitoliErroneiRD(mapIndicatoriDTO));
		        }else{
		        	return null;
		        }
				return listaKPI;
			}			
		}
		return null;
	}
	
	@Override
	public IndicatoriPerCanale indicatoriPerCanale(Date dataDa, Date dataA){
		IndicatoriPerCanale indicatoriPerCanale = new IndicatoriPerCanale();
		if(dataDa != null && dataA != null && dataDa.before(dataA)){
			Calendar calendar = new GregorianCalendar();
            calendar.setTime(dataDa);
            Integer annoDa = calendar.get(Calendar.YEAR);
            Integer meseDa = calendar.get(Calendar.MONTH) + 1;

            calendar.setTime(dataA);
            Integer annoA = calendar.get(Calendar.YEAR);
            Integer meseA = calendar.get(Calendar.MONTH) + 1;
            
            indicatoriPerCanale = rsIndicatoreService.indicatoriPerCanale(annoDa, meseDa, annoA, meseA);
		}
		return indicatoriPerCanale;
		
	}
}