package com.alkemytech.sophia.broadcasting.utils;

import com.alkemytech.sophia.broadcasting.enums.Stato;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class StatoTypeConverter implements AttributeConverter<Stato, String> {
    @Override
    public String convertToDatabaseColumn(Stato stato) {
        return stato.toString();
    }
 
    @Override
    public Stato convertToEntityAttribute(String dbValue) {
        // this can still return null unless it throws IllegalArgumentException
        // which would be in line with enums static valueOf method
        return Stato.valueOfDescription(dbValue);
    }
}