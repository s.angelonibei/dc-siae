package com.alkemytech.sophia.broadcasting.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(name = "BDC_UTENTI_REPERTORIO")
public class BdcUtentiRepertorio {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_utente_repertorio",nullable = false, unique = true)
	private Integer idUtenteRepertorio;
	
    @NotNull
    @Column(name = "id_utente", nullable = false)
    private Integer idUtente;
    
    @NotNull
    @ManyToOne
    @JoinColumn(name = "nome_repertorio")
    private BdcRepertorio repertorio;
    
    @NotNull
    @Column(name = "default_repertorio", nullable = false)
    private Boolean defaultRepertorio;
    
    
	public BdcUtentiRepertorio() {
		super();
	}

	public BdcUtentiRepertorio(Integer idUtenteRepertorio, Integer idUtente, BdcRepertorio repertorio,
			Boolean defaultRepertorio) {
		super();
		this.idUtenteRepertorio = idUtenteRepertorio;
		this.idUtente = idUtente;
		this.repertorio = repertorio;
		this.defaultRepertorio = defaultRepertorio;
	}

	public Integer getIdUtenteRepertorio() {
		return idUtenteRepertorio;
	}

	public void setIdUtenteRepertorio(Integer idUtenteRepertorio) {
		this.idUtenteRepertorio = idUtenteRepertorio;
	}

	public Integer getIdUtente() {
		return idUtente;
	}

	public void setIdUtente(Integer idUtente) {
		this.idUtente = idUtente;
	}



	public BdcRepertorio getRepertorio() {
		return repertorio;
	}

	public void setRepertorio(BdcRepertorio repertorio) {
		this.repertorio = repertorio;
	}

	public Boolean getDefaultRepertorio() {
		return defaultRepertorio;
	}

	public void setDefaultRepertorio(Boolean defaultRepertorio) {
		this.defaultRepertorio = defaultRepertorio;
	}
    
    
}
