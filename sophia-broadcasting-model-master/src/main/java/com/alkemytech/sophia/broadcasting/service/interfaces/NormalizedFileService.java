package com.alkemytech.sophia.broadcasting.service.interfaces;

import com.alkemytech.sophia.broadcasting.enums.Stato;
import com.alkemytech.sophia.broadcasting.model.BdcNormalizedFile;
import com.alkemytech.sophia.broadcasting.model.utilizations.BdcErrRdShowSchedule;
import com.alkemytech.sophia.broadcasting.model.utilizations.BdcErrTvShowSchedule;
import com.alkemytech.sophia.broadcasting.model.utilizations.BdcRdShowSchedule;
import com.alkemytech.sophia.broadcasting.model.utilizations.BdcTvShowSchedule;

import java.util.List;
import java.util.Map;

/**
 * Created by Alessandro Russo on 09/12/2017.
 */
public interface NormalizedFileService {

    //NORMALIZED FILE
    List<BdcNormalizedFile> getNormalizedFileList(Stato originalReportStatus);

    BdcNormalizedFile save(BdcNormalizedFile normalizedFile);

    //PERSIST NORMALIZED RECORD
    void saveNormalizedTvReport(List<BdcTvShowSchedule> records);

    void saveNormalizedRdReport(List<BdcRdShowSchedule> records);

    //MANIPULATE UTILIZATION
    BdcTvShowSchedule fromMapToTvShow(Map<String, Object> record, BdcNormalizedFile normalizedFile);

    BdcErrTvShowSchedule fromMapToErrTvShow(Map<String, Object> record, BdcNormalizedFile normalizedFile);

    BdcRdShowSchedule fromMapToRdShow(Map<String, Object> record, BdcNormalizedFile normalizedFile);

    BdcErrRdShowSchedule fromMapToErrRdShow(Map<String, Object> record, BdcNormalizedFile normalizedFile);
}
