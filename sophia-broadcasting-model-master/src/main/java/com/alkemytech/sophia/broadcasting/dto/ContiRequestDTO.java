package com.alkemytech.sophia.broadcasting.dto;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ContiRequestDTO {
	private String dateFrom;
	private String dateTo;
	private String tipoEmittente;
	private Integer idEmittente;
	private Integer idCanale;
	private String tipoDiritto;
	private Integer first;
	private Integer last;
	private Integer maxrow;

	public ContiRequestDTO() {
		super();
	}

	public ContiRequestDTO(String dateFrom, String dateTo, String tipoEmittente, Integer idEmittente,
			Integer idCanale, Integer first, Integer last, Integer maxrow, String tipoDiritto) {
		super();
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		this.tipoEmittente = tipoEmittente;
		this.idEmittente = idEmittente;
		this.idCanale = idCanale;
		this.first = first;
		this.last = last;
		this.maxrow = maxrow;
		this.tipoDiritto = tipoDiritto;
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	public String getTipoEmittente() {
		return tipoEmittente;
	}

	public void setTipoEmittente(String tipoEmittente) {
		this.tipoEmittente = tipoEmittente;
	}

	public Integer getIdEmittente() {
		return idEmittente;
	}

	public void setIdEmittente(Integer idEmittente) {
		this.idEmittente = idEmittente;
	}

	public Integer getIdCanale() {
		return idCanale;
	}

	public void setIdCanale(Integer idCanale) {
		this.idCanale = idCanale;
	}

	public Integer getFirst() {
		return first;
	}

	public void setFirst(Integer first) {
		this.first = first;
	}

	public Integer getLast() {
		return last;
	}

	public void setLast(Integer last) {
		this.last = last;
	}

	public Integer getMaxrow() {
		return maxrow;
	}

	public void setMaxrow(Integer maxrow) {
		this.maxrow = maxrow;
	}

	public String getTipoDiritto() {
		return tipoDiritto;
	}

	public void setTipoDiritto(String tipoDiritto) {
		this.tipoDiritto = tipoDiritto;
	}

}
