package com.alkemytech.sophia.broadcasting.model.utilizations;

import com.alkemytech.sophia.broadcasting.model.BdcCanali;
import com.alkemytech.sophia.broadcasting.model.BdcNormalizedFile;
import com.alkemytech.sophia.common.Constants;
import com.alkemytech.sophia.common.tools.StringTools;
import com.amazonaws.util.CollectionUtils;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.alkemytech.sophia.broadcasting.utils.Utilities.getMapValue;

@XmlRootElement
@Entity
@Table(name = "BDC_RD_SHOW_SCHEDULE")
public class BdcRdShowSchedule implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column( name="ID_RD_SHOW_SCHEDULE", nullable = false)
    private Long id;

    @OneToMany (cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "ID_RD_SHOW_SCHEDULE")
    List<BdcRdShowMusic> rdShowMusicList;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_NORMALIZED_FILE", nullable = false)
    private BdcNormalizedFile normalizedFile;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_CHANNEL", nullable = false)
    private BdcCanali canale;

    @Column( name="TITLE")
    private String title;

    @Column( name = "BEGIN_TIME")
    private Date beginTime;

    @Column( name = "END_TIME")
    private Date endTime;

    @Column( name = "DURATION")
    private Integer duration;

    @Column( name = "REGIONAL_OFFICE")
    private String regionalOffice;

    @Column( name = "NOTE")
    private String note;

    @Column( name = "OVERLAP" )
    private boolean overlap;

    @Column( name = "SCHEDULE_YEAR")
    private Integer schedulYear;

    @Column( name = "SCHEDULE_MONTH")
    private Integer scheduleMonth;

    @Column( name = "DAYS_FROM_UPLOAD")
    private Integer daysFromUpload;

    @Column( name = "CREATION_DATE")
    private Date creationDate;

    @Column( name = "MODIFY_DATE")
    private Date modifyDate;

    public BdcRdShowSchedule() {
    }

    public BdcRdShowSchedule(BdcRdShowMusic rdShowMusic, BdcCanali canale, BdcNormalizedFile normalizedFile, Date beginTimeTx, Date endTimeTx, Map<String, Object> record) {
        this.rdShowMusicList = new ArrayList<>();
        this.rdShowMusicList.add(rdShowMusic);
        this.canale = canale;
        this.normalizedFile = normalizedFile;
        this.beginTime = beginTimeTx;
        this.endTime = endTimeTx;
        if( getMapValue(record, Constants.DURATA_TX) != null && getMapValue(record, Constants.DURATA_TX).matches(StringTools.DURATION_FORMAT) ){
            this.duration = Integer.parseInt( StringTools.parseTimeToSeconds( getMapValue(record, Constants.DURATA_TX) ) ) ;
        }
        this.title = getMapValue(record, Constants.TITOLO_TX);
        this.regionalOffice = getMapValue(record, Constants.DIFFUSIONE_REGIONALE);
        this.creationDate = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<BdcRdShowMusic> getRdShowMusicList() {
        return rdShowMusicList;
    }

    public void setRdShowMusicList(List<BdcRdShowMusic> rdShowMusicList) {
        this.rdShowMusicList = rdShowMusicList;
    }

    public BdcNormalizedFile getNormalizedFile() {
        return normalizedFile;
    }

    public void setNormalizedFile(BdcNormalizedFile normalizedFile) {
        this.normalizedFile = normalizedFile;
    }

    public BdcCanali getCanale() {
        return canale;
    }

    public void setCanale(BdcCanali canale) {
        this.canale = canale;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getRegionalOffice() {
        return regionalOffice;
    }

    public void setRegionalOffice(String regionalOffice) {
        this.regionalOffice = regionalOffice;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public boolean isOverlap() {
        return overlap;
    }

    public void setOverlap(boolean overlap) {
        this.overlap = overlap;
    }

    public Integer getSchedulYear() {
        return schedulYear;
    }

    public void setSchedulYear(Integer schedulYear) {
        this.schedulYear = schedulYear;
    }

    public Integer getScheduleMonth() {
        return scheduleMonth;
    }

    public void setScheduleMonth(Integer scheduleMonth) {
        this.scheduleMonth = scheduleMonth;
    }

    public Integer getDaysFromUpload() {
        return daysFromUpload;
    }

    public void setDaysFromUpload(Integer daysFromUpload) {
        this.daysFromUpload = daysFromUpload;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public Date getDate(){
        Date ret = this.getBeginTime();
        if( ret == null ){
            if( !CollectionUtils.isNullOrEmpty(this.getRdShowMusicList() ) ){
                for(BdcRdShowMusic music : this.getRdShowMusicList()){
                    if(music.getBeginTime() != null){
                        ret = music.getBeginTime();
                        break;
                    }
                }
            }
        }
        return ret;
    }

    @Override
    public String toString() {
        return "BdcRdShowSchedule{" +
                "id=" + id +
                ", rdShowMusicList=" + rdShowMusicList +
                ", normalizedFile=" + normalizedFile +
                ", canale=" + canale +
                ", title='" + title + '\'' +
                ", beginTime=" + beginTime +
                ", endTime=" + endTime +
                ", duration=" + duration +
                ", regionalOffice='" + regionalOffice + '\'' +
                ", note='" + note + '\'' +
                ", overlap=" + overlap +
                ", schedulYear=" + schedulYear +
                ", scheduleMonth=" + scheduleMonth +
                ", daysFromUpload=" + daysFromUpload +
                ", creationDate=" + creationDate +
                ", modifyDate=" + modifyDate +
                '}';
    }
}