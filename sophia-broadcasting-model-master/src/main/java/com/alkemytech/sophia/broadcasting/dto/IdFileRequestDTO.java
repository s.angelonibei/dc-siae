package com.alkemytech.sophia.broadcasting.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class IdFileRequestDTO {

  private Integer id;

  public IdFileRequestDTO() {
  }

  public IdFileRequestDTO(Integer id) {
    this.id = id;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }
}
