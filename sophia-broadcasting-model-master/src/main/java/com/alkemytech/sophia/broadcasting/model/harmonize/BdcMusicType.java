package com.alkemytech.sophia.broadcasting.model.harmonize;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the BDC_MUSIC_TYPE database table.
 * 
 */
@Entity
@Table(name="BDC_MUSIC_TYPE")
@NamedQuery(name="BdcMusicType.findAll", query="SELECT b FROM BdcMusicType b")
public class BdcMusicType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_MUSIC_TYPE")
	private Integer idMusicType;

	@Column(name="CATEGORY")
	private String category;

	@Column(name="NAME")
	private String name;

	//bi-directional many-to-one association to BdcErrRdShowMusic
	@JsonManagedReference
	@OneToMany(mappedBy="bdcMusicType")
	private List<BdcErrRdShowMusic> bdcErrRdShowMusics;

	//bi-directional many-to-one association to BdcErrTvShowMusic
	@JsonManagedReference
	@OneToMany(mappedBy="bdcMusicType")
	private List<BdcErrTvShowMusic> bdcErrTvShowMusics;

	//bi-directional many-to-one association to BdcTvShowMusic
	@JsonManagedReference
	@OneToMany(mappedBy="bdcMusicType")
	private List<BdcTvShowMusic> bdcTvShowMusics;

	public BdcMusicType() {
	}

	public Integer getIdMusicType() {
		return idMusicType;
	}

	public void setIdMusicType(Integer idMusicType) {
		this.idMusicType = idMusicType;
	}

	public String getCategory() {
		return this.category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<BdcErrRdShowMusic> getBdcErrRdShowMusics() {
		return this.bdcErrRdShowMusics;
	}

	public void setBdcErrRdShowMusics(List<BdcErrRdShowMusic> bdcErrRdShowMusics) {
		this.bdcErrRdShowMusics = bdcErrRdShowMusics;
	}




	public List<BdcErrTvShowMusic> getBdcErrTvShowMusics() {
		return this.bdcErrTvShowMusics;
	}

	public void setBdcErrTvShowMusics(List<BdcErrTvShowMusic> bdcErrTvShowMusics) {
		this.bdcErrTvShowMusics = bdcErrTvShowMusics;
	}



	public List<BdcTvShowMusic> getBdcTvShowMusics() {
		return this.bdcTvShowMusics;
	}

	public void setBdcTvShowMusics(List<BdcTvShowMusic> bdcTvShowMusics) {
		this.bdcTvShowMusics = bdcTvShowMusics;
	}


}