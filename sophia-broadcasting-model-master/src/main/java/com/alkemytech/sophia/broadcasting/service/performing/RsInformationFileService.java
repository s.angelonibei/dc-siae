package com.alkemytech.sophia.broadcasting.service.performing;

import com.alkemytech.sophia.broadcasting.dto.performing.RSInformationFileDTO;
import com.alkemytech.sophia.broadcasting.dto.performing.RSRequestMonitoraggioKPIDTO;
import com.alkemytech.sophia.performing.model.PerfRsUtilizationFile;

import java.util.List;

public interface RsInformationFileService {
    List<RSInformationFileDTO> fileDTOS(RSRequestMonitoraggioKPIDTO monitoraggioKPI);

    PerfRsUtilizationFile findRsUtilizationById(Integer id);

}
