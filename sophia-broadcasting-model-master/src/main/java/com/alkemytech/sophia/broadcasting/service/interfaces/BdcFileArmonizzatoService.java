package com.alkemytech.sophia.broadcasting.service.interfaces;

import com.alkemytech.sophia.broadcasting.dto.*;
import com.alkemytech.sophia.broadcasting.model.BdcBroadcasters;
import com.alkemytech.sophia.broadcasting.model.BdcCanali;

import java.util.List;

public interface BdcFileArmonizzatoService {

        List<FileArmonizzatoTvDTO> getFileArmonizzato (BdcBroadcasters broadcasters, List<BdcCanali> canali, Integer anno, List<Integer> mesi);

    List<FileNewTracciatoArmonizzatoTvDTO> getFileArmonizzatoNewTracciato(BdcBroadcasters broadcasters, List<BdcCanali> canali,
                                                                          Integer anno, List<Integer> mesi);

    List<FileArmonizzatoRadioDTO> getFileArmonizzatoRadio (BdcBroadcasters broadcasters, List<BdcCanali> canali, Integer anno, List<Integer> mesi);

    List<FileNewTracciatoArmonizzatoRadioDTO> getFileArmonizzatoRadioNewTracciato(BdcBroadcasters broadcasters, List<BdcCanali> canali,
                                                                                  Integer anno, List<Integer> mesi);

    List<UtilizzazioniArmonizzatoDTO> getUtilizzazioniArmonizzate(BdcBroadcasters broadcasters, BdcCanali canali, String titoloTrasmissione, String inizioTrasmissioneFrom, String inizioTrasmissioneTo);
        List<BdcShowMusicDTO> getShowMusic(BdcBroadcasters broadcasters,Integer idShowSchedule,boolean isError);
		List<BdcScheduleFieldDTO> getScheduleField(BdcBroadcasters broadcasters, Integer idShowSchedule,boolean isError);
}
