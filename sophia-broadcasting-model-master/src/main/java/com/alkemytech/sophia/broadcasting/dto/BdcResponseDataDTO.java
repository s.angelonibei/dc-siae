package com.alkemytech.sophia.broadcasting.dto;

import com.alkemytech.sophia.broadcasting.model.BdcInformationFileEntity;
import com.google.gson.annotations.SerializedName;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BdcResponseDataDTO {

    @XmlElement( name = "output")
    @SerializedName("output")
    private List<BdcInformationFileEntity> output;

    public List<BdcInformationFileEntity> getOutput() {
        return output;
    }

    public void setOutput(List<BdcInformationFileEntity> output) {
        this.output = output;
    }

}
