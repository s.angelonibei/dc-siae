package com.alkemytech.sophia.broadcasting.model.harmonize;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the BDC_ERR_RD_SHOW_SCHEDULE database table.
 * 
 */
@Entity(name="ErrRdShowSchedule")
@Table(name="BDC_ERR_RD_SHOW_SCHEDULE")
@NamedQuery(name="ErrRdShowSchedule.findAll", query="SELECT b FROM ErrRdShowSchedule b")
public class BdcErrRdShowSchedule implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_ERR_RD_SHOW_SCHEDULE", unique=true, nullable=false)
	private Integer idErrRdShowSchedule;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="BEGIN_TIME")
	private Date beginTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATION_DATE", nullable=false)
	private Date creationDate;

	@Column(name="DAYS_FROM_UPLOAD")
	private Integer daysFromUpload;

	@Column(name="DURATION")
	private Integer duration;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="END_TIME")
	private Date endTime;

	@Column(name="ERROR")
	private String error;

	@Column(name="GLOBAL_ERROR", length=400)
	private String globalError;

	@Column(name="ID_NORMALIZED_FILE", nullable=false)
	private java.math.BigInteger idNormalizedFile;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="MODIFY_DATE")
	private Date modifyDate;

	@Column(name="REGIONAL_OFFICE", length=10)
	private String regionalOffice;

	@Column(name="REPORT_BEGIN_DATE", length=100)
	private String reportBeginDate;

	@Column(name="REPORT_BEGIN_TIME", length=100)
	private String reportBeginTime;

	@Column(name="REPORT_CHANNEL", length=100)
	private String reportChannel;

	@Column(name="REPORT_DURATION", length=100)
	private String reportDuration;

	@Column(name="SCHEDULE_MONTH")
	private Integer scheduleMonth;

	@Column(name="SCHEDULE_YEAR")
	private Integer scheduleYear;

	@Lob
	@Column(name="TITLE")
	private String title;

	//bi-directional many-to-one association to BdcErrRdShowMusic
	@JsonManagedReference
	@OneToMany(mappedBy="bdcErrRdShowSchedule")
	private List<BdcErrRdShowMusic> bdcErrRdShowMusics;

	//bi-directional many-to-one association to BdcCanali
	@JsonBackReference
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CHANNEL")
	private BdcCanali bdcCanali;

	public BdcErrRdShowSchedule() {
	}

	public Integer getIdErrRdShowSchedule() {
		return this.idErrRdShowSchedule;
	}

	public void setIdErrRdShowSchedule(Integer idErrRdShowSchedule) {
		this.idErrRdShowSchedule = idErrRdShowSchedule;
	}

	public Date getBeginTime() {
		return this.beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Integer getDaysFromUpload() {
		return this.daysFromUpload;
	}

	public void setDaysFromUpload(Integer daysFromUpload) {
		this.daysFromUpload = daysFromUpload;
	}

	public Integer getDuration() {
		return this.duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Date getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getError() {
		return this.error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getGlobalError() {
		return this.globalError;
	}

	public void setGlobalError(String globalError) {
		this.globalError = globalError;
	}

	public java.math.BigInteger getIdNormalizedFile() {
		return this.idNormalizedFile;
	}

	public void setIdNormalizedFile(java.math.BigInteger idNormalizedFile) {
		this.idNormalizedFile = idNormalizedFile;
	}

	public Date getModifyDate() {
		return this.modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getRegionalOffice() {
		return this.regionalOffice;
	}

	public void setRegionalOffice(String regionalOffice) {
		this.regionalOffice = regionalOffice;
	}

	public String getReportBeginDate() {
		return this.reportBeginDate;
	}

	public void setReportBeginDate(String reportBeginDate) {
		this.reportBeginDate = reportBeginDate;
	}

	public String getReportBeginTime() {
		return this.reportBeginTime;
	}

	public void setReportBeginTime(String reportBeginTime) {
		this.reportBeginTime = reportBeginTime;
	}

	public String getReportChannel() {
		return this.reportChannel;
	}

	public void setReportChannel(String reportChannel) {
		this.reportChannel = reportChannel;
	}

	public String getReportDuration() {
		return this.reportDuration;
	}

	public void setReportDuration(String reportDuration) {
		this.reportDuration = reportDuration;
	}

	public Integer getScheduleMonth() {
		return this.scheduleMonth;
	}

	public void setScheduleMonth(Integer scheduleMonth) {
		this.scheduleMonth = scheduleMonth;
	}

	public Integer getScheduleYear() {
		return this.scheduleYear;
	}

	public void setScheduleYear(Integer scheduleYear) {
		this.scheduleYear = scheduleYear;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<BdcErrRdShowMusic> getBdcErrRdShowMusics() {
		return this.bdcErrRdShowMusics;
	}

	public void setBdcErrRdShowMusics(List<BdcErrRdShowMusic> bdcErrRdShowMusics) {
		this.bdcErrRdShowMusics = bdcErrRdShowMusics;
	}

	public BdcErrRdShowMusic addBdcErrRdShowMusic(BdcErrRdShowMusic bdcErrRdShowMusic) {
		getBdcErrRdShowMusics().add(bdcErrRdShowMusic);
		bdcErrRdShowMusic.setBdcErrRdShowSchedule(this);

		return bdcErrRdShowMusic;
	}

	public BdcErrRdShowMusic removeBdcErrRdShowMusic(BdcErrRdShowMusic bdcErrRdShowMusic) {
		getBdcErrRdShowMusics().remove(bdcErrRdShowMusic);
		bdcErrRdShowMusic.setBdcErrRdShowSchedule(null);

		return bdcErrRdShowMusic;
	}

	public BdcCanali getBdcCanali() {
		return this.bdcCanali;
	}

	public void setBdcCanali(BdcCanali bdcCanali) {
		this.bdcCanali = bdcCanali;
	}

}