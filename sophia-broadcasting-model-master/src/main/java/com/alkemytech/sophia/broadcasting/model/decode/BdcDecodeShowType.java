package com.alkemytech.sophia.broadcasting.model.decode;

import com.alkemytech.sophia.broadcasting.model.BdcBroadcasters;
import com.alkemytech.sophia.broadcasting.model.ShowType;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Alessandro Russo on 09/12/2017.
 */
@Entity
@Table(name = "BDC_DECODE_SHOW_TYPE")
@NamedQuery(name="BdcDecodeShowType.findAll", query="SELECT d FROM BdcDecodeShowType d")
public class BdcDecodeShowType implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_DECODE_SHOW_TYPE")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "ID_BROADCASTER")
    private BdcBroadcasters broadcaster;

    @ManyToOne
    @JoinColumn(name="ID_SHOW_TYPE")
    private ShowType showType;

    @Column(name = "VALUE")
    private String value;

    @Column(name = "SCOPE")
    private String scope;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BdcBroadcasters getBroadcaster() {
        return broadcaster;
    }

    public void setBroadcaster(BdcBroadcasters broadcaster) {
        this.broadcaster = broadcaster;
    }

    public ShowType getShowType() {
        return showType;
    }

    public void setShowType(ShowType showType) {
        this.showType = showType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    @Override
    public String toString() {
        return "BdcDecodeShowType{" +
                "id=" + id +
                ", broadcaster=" + broadcaster +
                ", showType=" + showType +
                ", value='" + value + '\'' +
                ", scope='" + scope + '\'' +
                '}';
    }
}
