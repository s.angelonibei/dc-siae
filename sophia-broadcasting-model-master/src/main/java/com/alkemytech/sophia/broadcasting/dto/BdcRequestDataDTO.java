package com.alkemytech.sophia.broadcasting.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import javax.validation.Valid;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class BdcRequestDataDTO extends RequestDTO {

    @XmlElement(name = "RequestData")
    @SerializedName("RequestData")
    @Valid
    private BdcRequestStatoDTO requestData;

    @JsonProperty("RequestData")
    public BdcRequestStatoDTO getRequestData() {
        return requestData;
    }

    public void setRequestData(BdcRequestStatoDTO requestData) {
        this.requestData = requestData;
    }

    public BdcRequestDataDTO() {
    }

    public BdcRequestDataDTO(BdcRequestStatoDTO requestData) {
        this.requestData = requestData;
    }
}
