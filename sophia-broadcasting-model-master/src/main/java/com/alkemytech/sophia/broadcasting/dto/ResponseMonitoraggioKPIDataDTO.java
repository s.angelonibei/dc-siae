package com.alkemytech.sophia.broadcasting.dto;

public class ResponseMonitoraggioKPIDataDTO {
    private MonitoraggioKPIDTO input;
    private MonitoraggioKPIOutputDTO output;

    public MonitoraggioKPIDTO getInput() {
        return input;
    }

    public void setInput(MonitoraggioKPIDTO input) {
        this.input = input;
    }

    public MonitoraggioKPIOutputDTO getOutput() {
        return output;
    }

    public void setOutput(MonitoraggioKPIOutputDTO output) {
        this.output = output;
    }
}
