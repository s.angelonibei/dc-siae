package com.alkemytech.sophia.broadcasting.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.broadcasting.model.BdcBroadcasters;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BdcNewsEditRequestDTO {
	
	    private Integer id;
	    private String title;
	    private String news;
	    private BdcBroadcasters bdcBroadcasters;
	    private Integer idUtente;
	    private Boolean flagForAllBdc;
	    private Boolean flagForAllBdcUser;
	    private String creator;
	    private String insertDate;
	    private String validFrom;
	    private String validTo;



    public BdcNewsEditRequestDTO(Integer id, String title, String news, BdcBroadcasters bdcBroadcasters,
				Integer idUtente, Boolean flagForAllBdc, Boolean flagForAllBdcUser, String creator, String insertDate,
				String validFrom, String validTo) {
			super();
			this.id = id;
			this.title = title;
			this.news = news;
			this.bdcBroadcasters = bdcBroadcasters;
			this.idUtente = idUtente;
			this.flagForAllBdc = flagForAllBdc;
			this.flagForAllBdcUser = flagForAllBdcUser;
			this.creator = creator;
			this.insertDate = insertDate;
			this.validFrom = validFrom;
			this.validTo = validTo;
		}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNews() {
        return news;
    }

    public void setNews(String news) {
        this.news = news;
    }

    public BdcBroadcasters getBdcBroadcasters() {
        return bdcBroadcasters;
    }

    public void setBdcBroadcasters(BdcBroadcasters bdcBroadcasters) {
        this.bdcBroadcasters = bdcBroadcasters;
    }

    public Integer getIdUtente() {
        return idUtente;
    }

    public void setIdUtente(Integer idUtente) {
        this.idUtente = idUtente;
    }

    public Boolean getFlagForAllBdc() {
        return flagForAllBdc;
    }

    public void setFlagForAllBdc(Boolean flagForAllBdc) {
        this.flagForAllBdc = flagForAllBdc;
    }

    public Boolean getFlagForAllBdcUser() {
        return flagForAllBdcUser;
    }

    public void setFlagForAllBdcUser(Boolean flagForAllBdcUser) {
        this.flagForAllBdcUser = flagForAllBdcUser;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(String insertDate) {
        this.insertDate = insertDate;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

	public String getValidTo() {
		return validTo;
	}

	public void setValidTo(String validTo) {
		this.validTo = validTo;
	}
}
