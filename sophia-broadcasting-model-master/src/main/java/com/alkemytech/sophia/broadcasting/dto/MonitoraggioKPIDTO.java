package com.alkemytech.sophia.broadcasting.dto;

import com.alkemytech.sophia.broadcasting.model.BdcBroadcasters;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

public class MonitoraggioKPIDTO implements Serializable
{
    @NotNull
    private BdcBroadcasters emittente;

    @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
    @NotNull
    @Size(min=1)
    private List<Integer> canali;
    @NotNull
    @Min(2010)
    private Integer anno;

    @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
    @NotNull
    @Size(min=1)
    private List<Short> mesi;
    @NotNull
    private String tipoCanali;

    private String platform;

    @JsonIgnore
    public String getCanaliToString() {
        String sCanali = "";
        for (Integer n: canali
                ) {
            if(!sCanali.equals("")) {
                sCanali += "," + n;
            }else{
                sCanali += n;
            }
        }
        return sCanali;
    }

    @JsonIgnore
    public String getMesiToString() {
        String sMesi = "";
        for (Short m: mesi
                ) {
            if(!sMesi.equals("")) {
                sMesi += "," + m;
            }else{
                sMesi += m;
            }
        }
        return sMesi;
    }

    public BdcBroadcasters getEmittente() {
        return emittente;
    }

    public void setEmittente(BdcBroadcasters emittente) {
        this.emittente = emittente;
    }

    public List<Integer> getCanali() {
        return canali;
    }

    public void setCanali(List<Integer> canali) {
        this.canali = canali;
    }

    public Integer getAnno() {
        return anno;
    }

    public void setAnno(Integer anno) {
        this.anno = anno;
    }

    public List<Short> getMesi() {
        return mesi;
    }

    public void setMesi(List<Short> mesi) {
        this.mesi = mesi;
    }

    public String getTipoCanali() {
        return tipoCanali;
    }

    public void setTipoCanali(String tipoCanali) {
        this.tipoCanali = tipoCanali;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }
}
