package com.alkemytech.sophia.broadcasting.model.harmonize;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the bdc_utilization_file database table.
 * 
 */
@Entity(name="UtilizationFile")
@Table(name="bdc_utilization_file")
@NamedQuery(name="UtilizationFile.findAll", query="SELECT b FROM UtilizationFile b")
public class BdcUtilizationFile implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private Integer id;

	private Integer anno;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_processamento")
	private Date dataProcessamento;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_upload", nullable=false)
	private Date dataUpload;

	@Column(name="id_utente")
	private int idUtente;

	private Integer mese;

	@Lob
	@Column(name="nome_file", nullable=false)
	private String nomeFile;

	@Column(name="NOME_REPERTORIO", length=100)
	private String nomeRepertorio;

	@Lob
	@Column(nullable=false)
	private String percorso;

	@Column(nullable=false, length=1)
	private String stato;

	@Column(name="tipo_upload", nullable=false, length=255)
	private String tipoUpload;

	//bi-directional many-to-one association to BdcUtilizationNormalizedFile
	@JsonManagedReference
	@OneToMany(mappedBy="bdcUtilizationFile")
	private List<BdcUtilizationNormalizedFile> bdcUtilizationNormalizedFiles;

	//bi-directional many-to-one association to BdcBroadcaster
	@JsonBackReference
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="emittente", nullable=false)
	private BdcBroadcaster bdcBroadcaster;

	//bi-directional many-to-one association to BdcCanali
	@JsonBackReference
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="canale")
	private BdcCanali bdcCanali;

	public BdcUtilizationFile() {
	}

	public BdcBroadcaster getBdcBroadcaster() {
		return bdcBroadcaster;
	}

	public void setBdcBroadcaster(BdcBroadcaster bdcBroadcaster) {
		this.bdcBroadcaster = bdcBroadcaster;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAnno() {
		return this.anno;
	}

	public void setAnno(Integer anno) {
		this.anno = anno;
	}

	public Date getDataProcessamento() {
		return this.dataProcessamento;
	}

	public void setDataProcessamento(Date dataProcessamento) {
		this.dataProcessamento = dataProcessamento;
	}

	public Date getDataUpload() {
		return this.dataUpload;
	}

	public void setDataUpload(Date dataUpload) {
		this.dataUpload = dataUpload;
	}

	public int getIdUtente() {
		return this.idUtente;
	}

	public void setIdUtente(int idUtente) {
		this.idUtente = idUtente;
	}

	public Integer getMese() {
		return this.mese;
	}

	public void setMese(Integer mese) {
		this.mese = mese;
	}

	public String getNomeFile() {
		return this.nomeFile;
	}

	public void setNomeFile(String nomeFile) {
		this.nomeFile = nomeFile;
	}

	public String getNomeRepertorio() {
		return this.nomeRepertorio;
	}

	public void setNomeRepertorio(String nomeRepertorio) {
		this.nomeRepertorio = nomeRepertorio;
	}

	public String getPercorso() {
		return this.percorso;
	}

	public void setPercorso(String percorso) {
		this.percorso = percorso;
	}

	public String getStato() {
		return this.stato;
	}

	public void setStato(String stato) {
		this.stato = stato;
	}

	public String getTipoUpload() {
		return this.tipoUpload;
	}

	public void setTipoUpload(String tipoUpload) {
		this.tipoUpload = tipoUpload;
	}

	public List<BdcUtilizationNormalizedFile> getBdcUtilizationNormalizedFiles() {
		return this.bdcUtilizationNormalizedFiles;
	}

	public void setBdcUtilizationNormalizedFiles(List<BdcUtilizationNormalizedFile> bdcUtilizationNormalizedFiles) {
		this.bdcUtilizationNormalizedFiles = bdcUtilizationNormalizedFiles;
	}

	public BdcUtilizationNormalizedFile addBdcUtilizationNormalizedFile(BdcUtilizationNormalizedFile bdcUtilizationNormalizedFile) {
		getBdcUtilizationNormalizedFiles().add(bdcUtilizationNormalizedFile);
		bdcUtilizationNormalizedFile.setBdcUtilizationFile(this);

		return bdcUtilizationNormalizedFile;
	}

	public BdcUtilizationNormalizedFile removeBdcUtilizationNormalizedFile(BdcUtilizationNormalizedFile bdcUtilizationNormalizedFile) {
		getBdcUtilizationNormalizedFiles().remove(bdcUtilizationNormalizedFile);
		bdcUtilizationNormalizedFile.setBdcUtilizationFile(null);

		return bdcUtilizationNormalizedFile;
	}

	public BdcCanali getBdcCanali() {
		return this.bdcCanali;
	}

	public void setBdcCanali(BdcCanali bdcCanali) {
		this.bdcCanali = bdcCanali;
	}

}