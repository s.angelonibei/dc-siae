package com.alkemytech.sophia.broadcasting.service;

import com.alkemytech.sophia.broadcasting.model.BdcBroadcasterConfig;
import com.alkemytech.sophia.broadcasting.model.BdcBroadcasterConfigTemplate;
import com.alkemytech.sophia.broadcasting.model.BdcBroadcasters;
import com.alkemytech.sophia.broadcasting.model.TipoBroadcaster;
import com.alkemytech.sophia.broadcasting.service.interfaces.BdcBroadcastersService;
import com.google.inject.Inject;
import com.google.inject.Provider;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.apache.http.util.TextUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BdcBroadcastersServiceImpl implements BdcBroadcastersService {

	private final Provider<EntityManager> provider;

	@Inject
	public BdcBroadcastersServiceImpl(Provider<EntityManager> provider) {
		this.provider = provider;
	}

	@Override
	public List<BdcBroadcasters> findAll() {
		EntityManager em = null;
		em = provider.get();
		final Query q = em.createQuery("SELECT a FROM BdcBroadcasters a ORDER BY a.nome");
		return (List<BdcBroadcasters>) q.getResultList();
	}

	@Override
	public BdcBroadcasters findById(Integer id) {
		EntityManager em = null;
		BdcBroadcasters result = null;
		em = provider.get();
		final Query q = em.createQuery("SELECT b FROM BdcBroadcasters b WHERE b.id=:id");
		q.setParameter("id", id);
		return (BdcBroadcasters) q.getSingleResult();
	}

	@Override
	public List<BdcBroadcasters> findFiltered(String fiteredName, String fiteredType) {
		EntityManager em = null;
		em = provider.get();

		StringBuffer query = new StringBuffer("select * ,\n" + " if((SELECT count(*) FROM bdc_canali\n"
				+ "WHERE (data_fine_valid IS NULL OR data_fine_valid>=now())and id_broadcaster=b.id)=0 ,(SELECT max(data_fine_valid) FROM bdc_canali WHERE id_broadcaster=b.id) ,null)\n"
				+ "as dataDisattivazione\n" + "FROM bdc_broadcasters b ");
		if (null != fiteredName && !TextUtils.isEmpty(fiteredName) && !TextUtils.isBlank(fiteredName)) {
			query.append(" WHERE b.nome like '%" + fiteredName + "%' ");
			if (null != fiteredType && !TextUtils.isEmpty(fiteredType) && !TextUtils.isBlank(fiteredType)) {
				query.append(" AND b.tipo_broadcaster = '" + fiteredType + "' ");
			}
		} else {
			if (null != fiteredType && !TextUtils.isEmpty(fiteredType) && !TextUtils.isBlank(fiteredType)) {
				query.append(" WHERE b.tipo_broadcaster = '" + fiteredType + "' ");
			}
		}
		query.append(" ORDER BY b.nome ");
		List<BdcBroadcasters> result = new ArrayList<>();
		List<Object[]> nativeResult = em.createNativeQuery(query.toString()).getResultList();
		for (Object[] row : nativeResult) {
			result.add(new BdcBroadcasters(row));
		}
		return result;
	}

	@Override
	public boolean uploadImage(byte[] imageByte, Integer broadcasterId) {
		EntityManager entityManager = provider.get();
		entityManager.getTransaction().begin();
		try {
			final Query q = entityManager.createQuery("SELECT a FROM BdcBroadcasters a WHERE a.id =:id")
					.setParameter("id", broadcasterId);
			BdcBroadcasters bdcBroadcasters = (BdcBroadcasters) q.getSingleResult();
			bdcBroadcasters.setLogo(imageByte);
			entityManager.merge(bdcBroadcasters);
			entityManager.getTransaction().commit();
			return true;
		} catch (Exception e) {
			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
			return false;
		}

	}

	@Override
	public boolean deleteImage(Integer broadcasterId) {
		EntityManager entityManager = provider.get();
		entityManager.getTransaction().begin();
		try {
			final Query q = entityManager.createQuery("SELECT a FROM BdcBroadcasters a WHERE a.id =:id")
					.setParameter("id", broadcasterId);
			BdcBroadcasters bdcBroadcasters = (BdcBroadcasters) q.getSingleResult();
			bdcBroadcasters.setLogo(null);
			entityManager.merge(bdcBroadcasters);
			entityManager.getTransaction().commit();
			return true;
		} catch (Exception e) {
			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
			return false;
		}

	}

	@Override
	public boolean addBroadcaster(String broadcasterName, TipoBroadcaster broadcasterType,
								  BdcBroadcasterConfigTemplate bdcBroadcasterConfigTemplate, String username, Date inizioValid,
								  Date fineValid, String nuovoTracciatoRai) {
		EntityManager entityManager = provider.get();
		entityManager.getTransaction().begin();
		Date now = new Date();
		try {
			BdcBroadcasters bdcBroadcasters = new BdcBroadcasters(broadcasterName, broadcasterType,nuovoTracciatoRai);
			bdcBroadcasters.setData_creazione(new Date());
			entityManager.persist(bdcBroadcasters);
			entityManager.flush();
			BdcBroadcasterConfig bdcBroadcasterConfig = new BdcBroadcasterConfig(bdcBroadcasters.getId().longValue(),
					null, inizioValid, fineValid, bdcBroadcasterConfigTemplate, null, now, username, now, username);
			entityManager.persist(bdcBroadcasterConfig);
			entityManager.getTransaction().commit();
			return true;
		} catch (Exception e) {
			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
			return false;
		}
	}
}
