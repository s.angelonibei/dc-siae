package com.alkemytech.sophia.broadcasting.dto;

import com.alkemytech.sophia.broadcasting.enums.ArmonizedField;
import com.alkemytech.sophia.common.Constants;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;
import org.apache.commons.lang.StringUtils;

import java.math.BigInteger;
import java.sql.Time;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class FileArmonizzatoRadioDTO {

    //    @CsvBindByPosition(position = 0)
//    @CsvBindByName
//    private String nomeEmittente;
    @CsvBindByName
    @CsvBindByPosition(position = 0)
    private String canale;
    @CsvBindByName
    @CsvBindByPosition(position = 1)
    private String titoloTrasmissione;
    @CsvBindByName
    @CsvBindByPosition(position = 2)
    private String dataInizioTrasmissione;
    @CsvBindByName
    @CsvBindByPosition(position = 3)
    private String orarioInizioTrasmissione;
    @CsvBindByName
    @CsvBindByPosition(position = 4)
    private String durataTrasmissione;
    @CsvBindByName
    @CsvBindByPosition(position = 5)
    private String titoloBrano;
    @CsvBindByName
    @CsvBindByPosition(position = 6)
    private String compositoreAutore;
    @CsvBindByName
    @CsvBindByPosition(position = 7)
    private String esecutorePerformer;
    @CsvBindByPosition(position = 8)
    @CsvBindByName
    private String album;
    @CsvBindByPosition(position = 9)
    @CsvBindByName
    private String durataBrano;
    @CsvBindByPosition(position = 10)
    @CsvBindByName
    private String codiceISWC;
    @CsvBindByPosition(position = 11)
    @CsvBindByName
    private String codiceISRC;
    @CsvBindByPosition(position = 12)
    @CsvBindByName
    private String orarioInizioBrano;
    @CsvBindByPosition(position = 13)
    @CsvBindByName
    private String categoriaUso;
    @CsvBindByPosition(position = 14)
    @CsvBindByName
    private Integer diffusioneRegionale;
    @CsvBindByPosition(position = 15)
    @CsvBindByName
    private String stato;
    @CsvBindByPosition(position = 16)
    @CsvBindByName
    private String campiObbligatoriMancanti;
    @CsvBindByPosition(position = 17)
    @CsvBindByName
    private String campiFormatoNonSupportato;
    @CsvBindByPosition(position = 18)
    @CsvBindByName
    private String campiConValoreInatteso;
    @CsvBindByPosition(position = 19)
    @CsvBindByName
    private String altriMotiviDiScarto;
    @CsvBindByPosition(position = 20)
    @CsvBindByName
    private String dataUpload;
    @CsvBindByPosition(position = 21)
    @CsvBindByName
    private String oraUpload;
    @CsvBindByPosition(position = 22)
    @CsvBindByName
    private String nomeFile;


//    public String getNomeEmittente() {
//        return nomeEmittente;
//    }
//
//    public void setNomeEmittente(String nomeEmittente) {
//        this.nomeEmittente = nomeEmittente;
//    }


    public String getNomeFile() {
        return nomeFile;
    }

    public void setNomeFile(String nomeFile) {
        this.nomeFile = nomeFile;
    }

    public String getDataUpload() {
        return dataUpload;
    }

    public void setDataUpload(String dataUpload) {
        this.dataUpload = dataUpload;
    }

    public String getOraUpload() {
        return oraUpload;
    }

    public void setOraUpload(String oraUpload) {
        this.oraUpload = oraUpload;
    }

    public String getCanale() {
        return canale;
    }

    public void setCanale(String canale) {
        this.canale = canale;
    }

    public String getTitoloTrasmissione() {
        return titoloTrasmissione;
    }

    public void setTitoloTrasmissione(String titoloTrasmissione) {
        this.titoloTrasmissione = titoloTrasmissione;
    }

    public String getDataInizioTrasmissione() {
        return dataInizioTrasmissione;
    }

    public void setDataInizioTrasmissione(String dataInizioTrasmissione) {
        this.dataInizioTrasmissione = dataInizioTrasmissione;
    }

    public String getOrarioInizioTrasmissione() {
        return orarioInizioTrasmissione;
    }

    public void setOrarioInizioTrasmissione(String orarioInizioTrasmissione) {
        this.orarioInizioTrasmissione = orarioInizioTrasmissione;
    }

    public String getDurataTrasmissione() {
        return durataTrasmissione;
    }

    public void setDurataTrasmissione(String durataTrasmissione) {
        this.durataTrasmissione = durataTrasmissione;
    }

    public String getTitoloBrano() {
        return titoloBrano;
    }

    public void setTitoloBrano(String titoloBrano) {
        this.titoloBrano = titoloBrano;
    }

    public String getCompositoreAutore() {
        return compositoreAutore;
    }

    public void setCompositoreAutore(String compositoreAutore) {
        this.compositoreAutore = compositoreAutore;
    }

    public String getEsecutorePerformer() {
        return esecutorePerformer;
    }

    public void setEsecutorePerformer(String esecutorePerformer) {
        this.esecutorePerformer = esecutorePerformer;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getDurataBrano() {
        return durataBrano;
    }

    public void setDurataBrano(String durataBrano) {
        this.durataBrano = durataBrano;
    }

    public String getCodiceISWC() {
        return codiceISWC;
    }

    public void setCodiceISWC(String codiceISWC) {
        this.codiceISWC = codiceISWC;
    }

    public String getCodiceISRC() {
        return codiceISRC;
    }

    public void setCodiceISRC(String codiceISRC) {
        this.codiceISRC = codiceISRC;
    }

    public String getOrarioInizioBrano() {
        return orarioInizioBrano;
    }

    public void setOrarioInizioBrano(String orarioInizioBrano) {
        this.orarioInizioBrano = orarioInizioBrano;
    }

    public String getCategoriaUso() {
        return categoriaUso;
    }

    public void setCategoriaUso(String categoriaUso) {
        this.categoriaUso = categoriaUso;
    }

    public Integer getDiffusioneRegionale() {
        return diffusioneRegionale;
    }

    public void setDiffusioneRegionale(Integer diffusioneRegionale) {
        this.diffusioneRegionale = diffusioneRegionale;
    }

    public String getStato() {
        return stato;
    }

    public void setStato(String stato) {
        this.stato = stato;
    }

    public String getCampiObbligatoriMancanti() {
        return campiObbligatoriMancanti;
    }

    public void setCampiObbligatoriMancanti(String campiObbligatoriMancanti) {
        this.campiObbligatoriMancanti = campiObbligatoriMancanti;
    }

    public String getCampiFormatoNonSupportato() {
        return campiFormatoNonSupportato;
    }

    public void setCampiFormatoNonSupportato(String campiFormatoNonSupportato) {
        this.campiFormatoNonSupportato = campiFormatoNonSupportato;
    }

    public String getCampiConValoreInatteso() {
        return campiConValoreInatteso;
    }

    public void setCampiConValoreInatteso(String campiConValoreInatteso) {
        this.campiConValoreInatteso = campiConValoreInatteso;
    }

    public String getAltriMotiviDiScarto() {
        return altriMotiviDiScarto;
    }

    public void setAltriMotiviDiScarto(String altriMotiviDiScarto) {
        this.altriMotiviDiScarto = altriMotiviDiScarto;
    }

    public FileArmonizzatoRadioDTO() {
    }

    public FileArmonizzatoRadioDTO(String canale,
                                   String titoloTrasmissione,
                                   String dataInizioTrasmissione,
                                   String orarioInizioTrasmissione,
                                   Time durataTrasmissione,
                                   String titoloBrano,
                                   String compositoreAutore,
                                   String esecutorePerformer,
                                   String album,
                                   Time durataBrano,
                                   String codiceISWC,
                                   String codiceISRC,
                                   String orarioInizioBrano,
                                   BigInteger categoriaUso,
                                   Integer diffusioneRegionale,
                                   String nomeFile,
                                   String dataUpload,
                                   String oraUpload) {
//        this.nomeEmittente = nomeEmittente;
        this.canale = canale;
        this.titoloTrasmissione = titoloTrasmissione;
        this.dataInizioTrasmissione = dataInizioTrasmissione;
        this.orarioInizioTrasmissione = orarioInizioTrasmissione;
        this.durataTrasmissione = durataTrasmissione != null ? durataTrasmissione.toString() : null;
        this.titoloBrano = titoloBrano;
        this.compositoreAutore = compositoreAutore;
        this.esecutorePerformer = esecutorePerformer;
        this.album = album;
        this.durataBrano = durataBrano != null ? durataBrano.toString() : null;
        this.codiceISWC = codiceISWC;
        this.codiceISRC = codiceISRC;
        this.orarioInizioBrano = orarioInizioBrano;
        this.categoriaUso = categoriaUso != null ? categoriaUso.toString() : null;
        this.diffusioneRegionale = diffusioneRegionale;
        this.stato = "OK";
        this.nomeFile = nomeFile;
        this.dataUpload = dataUpload;
        this.oraUpload = oraUpload;
    }

    public FileArmonizzatoRadioDTO(String canale,
                                   String titoloTrasmissione,
                                   String dataInizioTrasmissione,
                                   String orarioInizioTrasmissione,
                                   Time durataTrasmissione,
                                   String titoloBrano,
                                   String compositoreAutore,
                                   String esecutorePerformer,
                                   String album,
                                   Time durataBrano,
                                   String codiceISWC,
                                   String codiceISRC,
                                   String orarioInizioBrano,
                                   BigInteger categoriaUso,
                                   Integer diffusioneRegionale,
                                   String altriMotiviDiScarto,
                                   String nomeFile,
                                   String dataUpload,
                                   String oraUpload) {
//        this.nomeEmittente = nomeEmittente;
        this.canale = canale;
        this.titoloTrasmissione = titoloTrasmissione;
        this.dataInizioTrasmissione = dataInizioTrasmissione;
        this.orarioInizioTrasmissione = orarioInizioTrasmissione;
        this.durataTrasmissione = durataTrasmissione != null ? durataTrasmissione.toString() : null;
        this.titoloBrano = titoloBrano;
        this.compositoreAutore = compositoreAutore;
        this.esecutorePerformer = esecutorePerformer;
        this.album = album;
        this.durataBrano = durataBrano != null ? durataBrano.toString() : null;
        this.codiceISWC = codiceISWC;
        this.codiceISRC = codiceISRC;
        this.orarioInizioBrano = orarioInizioBrano;
        this.categoriaUso = categoriaUso != null ? categoriaUso.toString() : null;
        this.diffusioneRegionale = diffusioneRegionale;
        this.stato = "OK";
        this.altriMotiviDiScarto = altriMotiviDiScarto;
        this.nomeFile = nomeFile;
        this.dataUpload = dataUpload;
        this.oraUpload = oraUpload;
    }

    public FileArmonizzatoRadioDTO(java.lang.Object canale,
                                   java.lang.String titoloTrasmissione,
                                   java.lang.String dataInizioTrasmissione,
                                   java.lang.String orarioInizioTrasmissione,
                                   java.lang.String durataTrasmissione,
                                   java.lang.String titoloBrano,
                                   java.lang.String compositoreAutore,
                                   java.lang.String esecutorePerformer,
                                   java.lang.String album,
                                   java.lang.String durataBrano,
                                   java.lang.String codiceISWC,
                                   java.lang.String codiceISRC,
                                   java.lang.Object orarioInizioBrano,
                                   java.lang.Object categoriaUso,
                                   java.lang.Object diffusioneRegionale,
                                   java.lang.String stato,
                                   java.lang.String errore,
                                   java.lang.String altriMotiviDiScarto,
                                   java.lang.String nomeFile,
                                   java.lang.String dataUpload,
                                   java.lang.String oraUpload) {
//        this.nomeEmittente = nomeEmittente;
        this.canale = canale != null ? canale.toString() : null;
        this.titoloTrasmissione = titoloTrasmissione;
        this.dataInizioTrasmissione = dataInizioTrasmissione;
        this.orarioInizioTrasmissione = orarioInizioTrasmissione;
        this.durataTrasmissione = durataTrasmissione != null ? durataTrasmissione.toString() : null;
        this.titoloBrano = titoloBrano;
        this.compositoreAutore = compositoreAutore;
        this.esecutorePerformer = esecutorePerformer;
        this.album = album;
        this.durataBrano = durataBrano != null ? durataBrano.toString() : null;
        this.codiceISWC = codiceISWC;
        this.codiceISRC = codiceISRC;
        this.orarioInizioBrano = orarioInizioBrano != null ? orarioInizioBrano.toString() : null;
        this.categoriaUso = categoriaUso  != null ? categoriaUso.toString() : null;
        this.diffusioneRegionale = diffusioneRegionale  != null ? Integer.parseInt(diffusioneRegionale.toString()) : null;
        this.stato = stato;
        this.campiObbligatoriMancanti = regola(errore, Constants.REGOLA1);
        this.campiFormatoNonSupportato = regola(errore, Constants.REGOLA2);
        this.campiConValoreInatteso = regola(errore, Constants.REGOLA3);
        this.altriMotiviDiScarto = altriMotiviDiScarto;
        this.nomeFile = nomeFile;
        this.dataUpload = dataUpload;
        this.oraUpload = oraUpload;
    }

    public String[] getMappingStrategy() {
        return new String[]{
                ArmonizedField.CANALE.getDescription(),
                ArmonizedField.TITOLO_TRASMISSIONE.getDescription(),
                ArmonizedField.DATA_INIZIO_TRASMISSIONE.getDescription(),
                ArmonizedField.ORARIO_INIZIO_TRASMISSIONE.getDescription(),
                ArmonizedField.DURATA_TRASMISSIONE.getDescription(),
                ArmonizedField.TITOLO_BRANO.getDescription(),
                ArmonizedField.COMPOSITORE_OP.getDescription(),
                ArmonizedField.ESECUTORE_OP.getDescription(),
                ArmonizedField.ALBUM_OP.getDescription(),
                ArmonizedField.DURATA_BRANO.getDescription(),
                ArmonizedField.CODICE_ISWC_OP.getDescription(),
                ArmonizedField.CODICE_ISRC_OP.getDescription(),
                ArmonizedField.ORARIO_INIZIO_OP.getDescription(),
                ArmonizedField.CATEGORIA_USO.getDescription(),
                ArmonizedField.DIFFUSIONE_REGIONALE_RD.getDescription(),
                ArmonizedField.STATO.getDescription(),
                ArmonizedField.REGOLA1.getDescription(),
                ArmonizedField.REGOLA2.getDescription(),
                ArmonizedField.REGOLA3.getDescription(),
                ArmonizedField.ERRORE_GENERICO.getDescription(),

                ArmonizedField.DATA_UPLOAD.getDescription(),
                ArmonizedField.ORA_UPLOAD.getDescription(),
                ArmonizedField.NOME_FILE.getDescription(),
        };
    }

    public FileArmonizzatoRadioDTO(Object[] obj) {
        try {
            canale = (String) obj[0];
            titoloTrasmissione = (String) obj[1];
            dataInizioTrasmissione = (String) obj[2];
            orarioInizioTrasmissione = (String) obj[3];
            durataTrasmissione = (String) "" + obj[4];
            titoloBrano = (String) obj[5];
            compositoreAutore = (String) obj[6];
            esecutorePerformer = (String) obj[7];
            album = (String) obj[8];
            durataBrano = (String) "" + obj[9];
            codiceISWC = (String) obj[10];
            codiceISRC = (String) obj[11];
            orarioInizioBrano = (String) obj[12];
            categoriaUso = (String) "" + obj[13];
            diffusioneRegionale = Integer.parseInt("" + obj[14]);
            stato = (String) obj[15];
            altriMotiviDiScarto = (String) obj[17];
            campiObbligatoriMancanti = regola((String) obj[16], Constants.REGOLA1);
            campiFormatoNonSupportato = regola((String) obj[16], Constants.REGOLA2);
            campiConValoreInatteso = regola((String) obj[16], Constants.REGOLA3);
            nomeFile = (String) obj[18];
            dataUpload = (String) obj[19];
            oraUpload = (String) obj[20];
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private String regola(String reg, String descRegola) {
        if (StringUtils.isNotEmpty(reg)) {
            StringBuffer errorString = new StringBuffer();
            ObjectMapper objectMapper = new ObjectMapper();
            Gson gson = new Gson();
            Map<String, List<Map<String, String>>> mapErrori = gson.fromJson(reg, Map.class);
            errorToString(mapErrori, errorString, descRegola);
            if (errorString.length() > 0) {
                return errorString.toString();
            }
        }
        return "";
    }

    private void errorToString(Map<String, List<Map<String, String>>> mapErrori, StringBuffer errorString, String errorKey) {
        if (mapErrori.containsKey(errorKey)) {
            List<Map<String, String>> errList = mapErrori.get(errorKey);
            if (!errList.isEmpty()) {
                Iterator<Map<String, String>> iter = errList.iterator();
                Map<String, String> error = null;
                while (iter.hasNext()) {
                    error = iter.next();
                    if (error != null && error.containsKey("fieldName")) {
                        String field = ArmonizedField.findDescription(error.get("fieldName"));
                        if (StringUtils.isNotEmpty(field)) {
                            errorString.append(field);
                            if (iter.hasNext()) {
                                errorString.append(", ");
                            }
                        }
                    }
                }
            }
        }
    }
}
