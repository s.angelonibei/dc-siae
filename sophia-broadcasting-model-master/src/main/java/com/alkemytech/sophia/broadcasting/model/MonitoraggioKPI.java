package com.alkemytech.sophia.broadcasting.model;

import com.alkemytech.sophia.broadcasting.dto.KPI;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;

@XmlRootElement
public class MonitoraggioKPI {
    @NotNull
    private BdcBroadcasters emittente;
    @NotNull
    private List<BdcCanali> canali;
    @NotNull
    @Min( value = 1900)
    private int anno;
    @NotNull
    private List<Short> mesi;

    private java.util.Date aggiornatoAl;

    private List<KPI> listaKPI;

    public BdcBroadcasters getEmittente() {
        return emittente;
    }

    public void setEmittente(BdcBroadcasters emittente) {
        this.emittente = emittente;
    }

    public List<BdcCanali> getCanali() {
        return canali;
    }

    public void setCanali(List<BdcCanali> canali) {
        this.canali = canali;
    }

    public int getAnno() {
        return anno;
    }

    public void setAnno(int anno) {
        this.anno = anno;
    }

    public List<Short> getMesi() {
        return mesi;
    }

    public void setMesi(List<Short> mesi) {
        this.mesi = mesi;
    }

    public Date getAggiornatoAl() {
        return aggiornatoAl;
    }

    public void setAggiornatoAl(Date aggiornatoAl) {
        this.aggiornatoAl = aggiornatoAl;
    }

    public List<KPI> getListaKPI() {
        return listaKPI;
    }

    public void setListaKPI(List<KPI> listaKPI) {
        this.listaKPI = listaKPI;
    }
}
