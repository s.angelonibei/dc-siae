package com.alkemytech.sophia.broadcasting.model.harmonize;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the BDC_TV_SHOW_SCHEDULE database table.
 * 
 */
@Entity(name="TvShowSchedule")
@Table(name="BDC_TV_SHOW_SCHEDULE")
@NamedQuery(name="TvShowSchedule.findAll", query="SELECT b FROM TvShowSchedule b")
public class BdcTvShowSchedule implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_TV_SHOW_SCHEDULE", unique=true, nullable=false)
	private Integer idTvShowSchedule;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="BEGIN_TIME", nullable=false)
	private Date beginTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATION_DATE", nullable=false)
	private Date creationDate;

	@Column(name="DAYS_FROM_UPLOAD", nullable=false)
	private Integer daysFromUpload;

	@Column(name="DIRECTOR", length=400)
	private String director;

	@Column(name="DURATION")
	private Integer duration;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="END_TIME", nullable=false)
	private Date endTime;

	@Column(name="EPISODE_NUMBER")
	private Integer episodeNumber;

	@Column(name="EPISODE_ORIGINAL_TITLE", length=400)
	private String episodeOriginalTitle;

	@Column(name="EPISODE_TITLE", length=400)
	private String episodeTitle;

	@Column(name="ID_NORMALIZED_FILE", nullable=false)
	private java.math.BigInteger idNormalizedFile;

	@Column(name="ID_SHOW_TYPE")
	private java.math.BigInteger idShowType;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="MODIFY_DATE")
	private Date modifyDate;

	@Column(name="NOTE", length=400)
	private String note;

	@Column(name="ORIGINAL_TITLE", length=400)
	private String originalTitle;

	@Column(name="OVERLAP", nullable=false)
	private Boolean overlap;

	@Column(name="PROD_COUNTRY", length=400)
	private String prodCountry;

	@Column(name="PROD_YEAR", length=50)
	private String prodYear;

	@Column(name="PRODUCTOR", length=400)
	private String productor;

	@Column(name="REGIONAL_OFFICE", length=10)
	private String regionalOffice;

	@Column(name="REPLICA", length=4)
	private String replica;

	@Column(name="SCHEDULE_MONTH", nullable=false)
	private Integer scheduleMonth;

	@Column(name="SCHEDULE_YEAR", nullable=false)
	private Integer scheduleYear;

	@Column(name="TITLE", length=400)
	private String title;

	//bi-directional many-to-one association to BdcTvShowMusic
	@JsonManagedReference
	@OneToMany(mappedBy="bdcTvShowSchedule")
	private List<BdcTvShowMusic> bdcTvShowMusics;

	//bi-directional many-to-one association to BdcCanali
	@JsonBackReference
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CHANNEL", nullable=false)
	private BdcCanali bdcCanali;

	public BdcTvShowSchedule() {
	}

	public Integer getIdTvShowSchedule() {
		return this.idTvShowSchedule;
	}

	public void setIdTvShowSchedule(Integer idTvShowSchedule) {
		this.idTvShowSchedule = idTvShowSchedule;
	}

	public Date getBeginTime() {
		return this.beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Integer getDaysFromUpload() {
		return this.daysFromUpload;
	}

	public void setDaysFromUpload(Integer daysFromUpload) {
		this.daysFromUpload = daysFromUpload;
	}

	public String getDirector() {
		return this.director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public Integer getDuration() {
		return this.duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Date getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Integer getEpisodeNumber() {
		return this.episodeNumber;
	}

	public void setEpisodeNumber(Integer episodeNumber) {
		this.episodeNumber = episodeNumber;
	}

	public String getEpisodeOriginalTitle() {
		return this.episodeOriginalTitle;
	}

	public void setEpisodeOriginalTitle(String episodeOriginalTitle) {
		this.episodeOriginalTitle = episodeOriginalTitle;
	}

	public String getEpisodeTitle() {
		return this.episodeTitle;
	}

	public void setEpisodeTitle(String episodeTitle) {
		this.episodeTitle = episodeTitle;
	}

	public java.math.BigInteger getIdNormalizedFile() {
		return this.idNormalizedFile;
	}

	public void setIdNormalizedFile(java.math.BigInteger idNormalizedFile) {
		this.idNormalizedFile = idNormalizedFile;
	}

	public java.math.BigInteger getIdShowType() {
		return this.idShowType;
	}

	public void setIdShowType(java.math.BigInteger idShowType) {
		this.idShowType = idShowType;
	}

	public Date getModifyDate() {
		return this.modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getNote() {
		return this.note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getOriginalTitle() {
		return this.originalTitle;
	}

	public void setOriginalTitle(String originalTitle) {
		this.originalTitle = originalTitle;
	}

	public Boolean getOverlap() {
		return this.overlap;
	}

	public void setOverlap(Boolean overlap) {
		this.overlap = overlap;
	}

	public String getProdCountry() {
		return this.prodCountry;
	}

	public void setProdCountry(String prodCountry) {
		this.prodCountry = prodCountry;
	}

	public String getProdYear() {
		return this.prodYear;
	}

	public void setProdYear(String prodYear) {
		this.prodYear = prodYear;
	}

	public String getProductor() {
		return this.productor;
	}

	public void setProductor(String productor) {
		this.productor = productor;
	}

	public String getRegionalOffice() {
		return this.regionalOffice;
	}

	public void setRegionalOffice(String regionalOffice) {
		this.regionalOffice = regionalOffice;
	}

	public String getReplica() {
		return this.replica;
	}

	public void setReplica(String replica) {
		this.replica = replica;
	}

	public Integer getScheduleMonth() {
		return this.scheduleMonth;
	}

	public void setScheduleMonth(Integer scheduleMonth) {
		this.scheduleMonth = scheduleMonth;
	}

	public Integer getScheduleYear() {
		return this.scheduleYear;
	}

	public void setScheduleYear(Integer scheduleYear) {
		this.scheduleYear = scheduleYear;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<BdcTvShowMusic> getBdcTvShowMusics() {
		return this.bdcTvShowMusics;
	}

	public void setBdcTvShowMusics(List<BdcTvShowMusic> bdcTvShowMusics) {
		this.bdcTvShowMusics = bdcTvShowMusics;
	}

	public BdcTvShowMusic addBdcTvShowMusic(BdcTvShowMusic bdcTvShowMusic) {
		getBdcTvShowMusics().add(bdcTvShowMusic);
		bdcTvShowMusic.setBdcTvShowSchedule(this);

		return bdcTvShowMusic;
	}

	public BdcTvShowMusic removeBdcTvShowMusic(BdcTvShowMusic bdcTvShowMusic) {
		getBdcTvShowMusics().remove(bdcTvShowMusic);
		bdcTvShowMusic.setBdcTvShowSchedule(null);

		return bdcTvShowMusic;
	}

	public BdcCanali getBdcCanali() {
		return this.bdcCanali;
	}

	public void setBdcCanali(BdcCanali bdcCanali) {
		this.bdcCanali = bdcCanali;
	}

}