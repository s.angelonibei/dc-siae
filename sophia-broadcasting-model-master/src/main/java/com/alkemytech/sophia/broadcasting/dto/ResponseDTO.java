package com.alkemytech.sophia.broadcasting.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ResponseDTO extends RequestDTO
{
    @XmlElement( name = "Esito")
    @SerializedName("Esito")
    @JsonProperty("Esito")
    private EsitoDTO esito;

    public EsitoDTO getEsito() {
        return esito;
    }

    public void setEsito(EsitoDTO esito) {
        this.esito = esito;
    }
}