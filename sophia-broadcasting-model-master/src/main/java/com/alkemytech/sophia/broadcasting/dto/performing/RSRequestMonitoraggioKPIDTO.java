package com.alkemytech.sophia.broadcasting.dto.performing;

import com.alkemytech.sophia.broadcasting.dto.RequestDTO;
import com.alkemytech.sophia.performing.model.PerfMusicProvider;
import com.alkemytech.sophia.performing.model.PerfPalinsesto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class RSRequestMonitoraggioKPIDTO extends RequestDTO implements Serializable {
    private List<PerfPalinsesto> palinsesti;
    private List<Short> mesi;
    private Integer anno;
    private PerfMusicProvider musicProvider;

    public List<PerfPalinsesto> getPalinsesti() {
        return palinsesti;
    }

    public void setPalinsesti(List<PerfPalinsesto> palinsesti) {
        this.palinsesti = palinsesti;
    }

    public List<Short> getMesi() {
        return mesi;
    }

    public void setMesi(List<Short> mesi) {
        this.mesi = mesi;
    }

    public Integer getAnno() {
        return anno;
    }

    public void setAnno(Integer anno) {
        this.anno = anno;
    }

    public PerfMusicProvider getMusicProvider() {
        return musicProvider;
    }

    public void setMusicProvider(PerfMusicProvider musicProvider) {
        this.musicProvider = musicProvider;
    }

    public RSRequestMonitoraggioKPIDTO(List<PerfPalinsesto> palinsesti, List<Short> mesi, Integer anno, PerfMusicProvider musicProvider) {
        this.palinsesti = palinsesti;
        this.mesi = mesi;
        this.anno = anno;
        this.musicProvider = musicProvider;
    }

    public RSRequestMonitoraggioKPIDTO() {
    }
}
