package com.alkemytech.sophia.broadcasting.service.interfaces;

import java.util.Date;

public interface BdcUserTokenService {

	String createToken(Integer bdcUser, Date dataCreazione, Date dataFineValidita, Date dataLogin, String repertori,
			 String repertorioDefault, String token, String tokenNavigazione, String user);
	
}

