package com.alkemytech.sophia.broadcasting.dto;

import java.sql.Date;
import java.sql.Time;
import java.util.GregorianCalendar;

public class UtilizzazioniArmonizzatoDTO {

	private String canale;
	private String genereTrasmissione;
	private String titoloTrasmissione;
	private String titoloOriginaleTrasmissione;
	private Date dataInizioTrasmissione;
	private Time orarioInizioTrasmissione;
	private Time orarioFineTrasmissione;
	private String durataTrasmissione;
	private String replicaOPrimaEsecuzione;
	private String paeseDiProduzione;
	private String annoDiProduzione;
	private String produttore;
	private String regista;
	private String titoloEpisodio;
	private String titoloOriginaleEpisodio;
	private Integer numeroProgressivoEpisodio;
	private String titoloOperaMusicale;
	private String compositoreAutore1;
	private String compositoreAutore2;
	private String durataDellOpera;
	private String durataEffettivaDiUtilizzo;
	private String categoriaDUso;
	private String diffusioneRegionale;
	private String regola1;
	private String regola2;
	private String regola3;
	private String nomeEmittente;
	private String titoloBrano;
	private String compositoreAutore;
	private String esecutorePerformer;
	private String album;
	private String durataBrano;
	private String codiceISWC;
	private String codiceISRC;
	private Time orarioInizioBrano;
	private String categoriaUso;
	private Long id;

	public UtilizzazioniArmonizzatoDTO() {

	}

	public UtilizzazioniArmonizzatoDTO(String canale, String genereTrasmissione, String titoloTrasmissione,
			String titoloOriginaleTrasmissione, Date dataInizioTrasmissione, Time orarioInizioTrasmissione,
			Time orarioFineTrasmissione, String durataTrasmissione, String replicaOPrimaEsecuzione,
			String paeseDiProduzione, String annoDiProduzione, String produttore, String regista, String titoloEpisodio,
			String titoloOriginaleEpisodio, Integer numeroProgressivoEpisodio, String titoloOperaMusicale,
			String compositoreAutore1, String compositoreAutore2, String durataDellOpera,
			String durataEffettivaDiUtilizzo, String categoriaDUso, String diffusioneRegionale, String regola1,
			String regola2, String regola3, String nomeEmittente, String titoloBrano, String compositoreAutore,
			String esecutorePerformer, String album, String durataBrano, String codiceISWC, String codiceISRC,
			Time orarioInizioBrano, String categoriaUso, Long id) {
		super();
		this.canale = canale;
		this.genereTrasmissione = genereTrasmissione;
		this.titoloTrasmissione = titoloTrasmissione;
		this.titoloOriginaleTrasmissione = titoloOriginaleTrasmissione;
		this.dataInizioTrasmissione = dataInizioTrasmissione;
		this.orarioInizioTrasmissione = orarioInizioTrasmissione;
		this.orarioFineTrasmissione = orarioFineTrasmissione;
		this.durataTrasmissione = durataTrasmissione;
		this.replicaOPrimaEsecuzione = replicaOPrimaEsecuzione;
		this.paeseDiProduzione = paeseDiProduzione;
		this.annoDiProduzione = annoDiProduzione;
		this.produttore = produttore;
		this.regista = regista;
		this.titoloEpisodio = titoloEpisodio;
		this.titoloOriginaleEpisodio = titoloOriginaleEpisodio;
		this.numeroProgressivoEpisodio = numeroProgressivoEpisodio;
		this.titoloOperaMusicale = titoloOperaMusicale;
		this.compositoreAutore1 = compositoreAutore1;
		this.compositoreAutore2 = compositoreAutore2;
		this.durataDellOpera = durataDellOpera;
		this.durataEffettivaDiUtilizzo = durataEffettivaDiUtilizzo;
		this.categoriaDUso = categoriaDUso;
		this.diffusioneRegionale = diffusioneRegionale;
		this.regola1 = regola1;
		this.regola2 = regola2;
		this.regola3 = regola3;
		this.nomeEmittente = nomeEmittente;
		this.titoloBrano = titoloBrano;
		this.compositoreAutore = compositoreAutore;
		this.esecutorePerformer = esecutorePerformer;
		this.album = album;
		this.durataBrano = durataBrano;
		this.codiceISWC = codiceISWC;
		this.codiceISRC = codiceISRC;
		this.orarioInizioBrano = orarioInizioBrano;
		this.categoriaUso = categoriaUso;
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCanale() {
		return canale;
	}

	public void setCanale(String canale) {
		this.canale = canale;
	}

	public String getGenereTrasmissione() {
		return genereTrasmissione;
	}

	public void setGenereTrasmissione(String genereTrasmissione) {
		this.genereTrasmissione = genereTrasmissione;
	}

	public String getTitoloTrasmissione() {
		return titoloTrasmissione;
	}

	public void setTitoloTrasmissione(String titoloTrasmissione) {
		this.titoloTrasmissione = titoloTrasmissione;
	}

	public String getTitoloOriginaleTrasmissione() {
		return titoloOriginaleTrasmissione;
	}

	public void setTitoloOriginaleTrasmissione(String titoloOriginaleTrasmissione) {
		this.titoloOriginaleTrasmissione = titoloOriginaleTrasmissione;
	}

	public Date getDataInizioTrasmissione() {
		return dataInizioTrasmissione;
	}

	public void setDataInizioTrasmissione(Date dataInizioTrasmissione) {
		this.dataInizioTrasmissione = dataInizioTrasmissione;
	}

	public Time getOrarioInizioTrasmissione() {
		return orarioInizioTrasmissione;
	}

	public void setOrarioInizioTrasmissione(Time orarioInizioTrasmissione) {
		this.orarioInizioTrasmissione = orarioInizioTrasmissione;
	}

	public Time getOrarioFineTrasmissione() {
		return orarioFineTrasmissione;
	}

	public void setOrarioFineTrasmissione(Time orarioFineTrasmissione) {
		this.orarioFineTrasmissione = orarioFineTrasmissione;
	}

	public String getDurataTrasmissione() {
		return durataTrasmissione;
	}

	public void setDurataTrasmissione(String durataTrasmissione) {
		this.durataTrasmissione = durataTrasmissione;
	}

	public String getReplicaOPrimaEsecuzione() {
		return replicaOPrimaEsecuzione;
	}

	public void setReplicaOPrimaEsecuzione(String replicaOPrimaEsecuzione) {
		this.replicaOPrimaEsecuzione = replicaOPrimaEsecuzione;
	}

	public String getPaeseDiProduzione() {
		return paeseDiProduzione;
	}

	public void setPaeseDiProduzione(String paeseDiProduzione) {
		this.paeseDiProduzione = paeseDiProduzione;
	}

	public String getAnnoDiProduzione() {
		return annoDiProduzione;
	}

	public void setAnnoDiProduzione(String annoDiProduzione) {
		this.annoDiProduzione = annoDiProduzione;
	}

	public String getProduttore() {
		return produttore;
	}

	public void setProduttore(String produttore) {
		this.produttore = produttore;
	}

	public String getRegista() {
		return regista;
	}

	public void setRegista(String regista) {
		this.regista = regista;
	}

	public String getTitoloEpisodio() {
		return titoloEpisodio;
	}

	public void setTitoloEpisodio(String titoloEpisodio) {
		this.titoloEpisodio = titoloEpisodio;
	}

	public String getTitoloOriginaleEpisodio() {
		return titoloOriginaleEpisodio;
	}

	public void setTitoloOriginaleEpisodio(String titoloOriginaleEpisodio) {
		this.titoloOriginaleEpisodio = titoloOriginaleEpisodio;
	}

	public Integer getNumeroProgressivoEpisodio() {
		return numeroProgressivoEpisodio;
	}

	public void setNumeroProgressivoEpisodio(Integer numeroProgressivoEpisodio) {
		this.numeroProgressivoEpisodio = numeroProgressivoEpisodio;
	}

	public String getTitoloOperaMusicale() {
		return titoloOperaMusicale;
	}

	public void setTitoloOperaMusicale(String titoloOperaMusicale) {
		this.titoloOperaMusicale = titoloOperaMusicale;
	}

	public String getCompositoreAutore1() {
		return compositoreAutore1;
	}

	public void setCompositoreAutore1(String compositoreAutore1) {
		this.compositoreAutore1 = compositoreAutore1;
	}

	public String getCompositoreAutore2() {
		return compositoreAutore2;
	}

	public void setCompositoreAutore2(String compositoreAutore2) {
		this.compositoreAutore2 = compositoreAutore2;
	}

	public String getDurataDellOpera() {
		return durataDellOpera;
	}

	public void setDurataDellOpera(String durataDellOpera) {
		this.durataDellOpera = durataDellOpera;
	}

	public String getDurataEffettivaDiUtilizzo() {
		return durataEffettivaDiUtilizzo;
	}

	public void setDurataEffettivaDiUtilizzo(String durataEffettivaDiUtilizzo) {
		this.durataEffettivaDiUtilizzo = durataEffettivaDiUtilizzo;
	}

	public String getCategoriaDUso() {
		return categoriaDUso;
	}

	public void setCategoriaDUso(String categoriaDUso) {
		this.categoriaDUso = categoriaDUso;
	}

	public String getDiffusioneRegionale() {
		return diffusioneRegionale;
	}

	public void setDiffusioneRegionale(String diffusioneRegionale) {
		this.diffusioneRegionale = diffusioneRegionale;
	}

	public String getRegola1() {
		return regola1;
	}

	public void setRegola1(String regola1) {
		this.regola1 = regola1;
	}

	public String getRegola2() {
		return regola2;
	}

	public void setRegola2(String regola2) {
		this.regola2 = regola2;
	}

	public String getRegola3() {
		return regola3;
	}

	public void setRegola3(String regola3) {
		this.regola3 = regola3;
	}

	public String getNomeEmittente() {
		return nomeEmittente;
	}

	public void setNomeEmittente(String nomeEmittente) {
		this.nomeEmittente = nomeEmittente;
	}

	public String getTitoloBrano() {
		return titoloBrano;
	}

	public void setTitoloBrano(String titoloBrano) {
		this.titoloBrano = titoloBrano;
	}

	public String getCompositoreAutore() {
		return compositoreAutore;
	}

	public void setCompositoreAutore(String compositoreAutore) {
		this.compositoreAutore = compositoreAutore;
	}

	public String getEsecutorePerformer() {
		return esecutorePerformer;
	}

	public void setEsecutorePerformer(String esecutorePerformer) {
		this.esecutorePerformer = esecutorePerformer;
	}

	public String getAlbum() {
		return album;
	}

	public void setAlbum(String album) {
		this.album = album;
	}

	public String getDurataBrano() {
		return durataBrano;
	}

	public void setDurataBrano(String durataBrano) {
		this.durataBrano = durataBrano;
	}

	public String getCodiceISWC() {
		return codiceISWC;
	}

	public void setCodiceISWC(String codiceISWC) {
		this.codiceISWC = codiceISWC;
	}

	public String getCodiceISRC() {
		return codiceISRC;
	}

	public void setCodiceISRC(String codiceISRC) {
		this.codiceISRC = codiceISRC;
	}

	public Time getOrarioInizioBrano() {
		return orarioInizioBrano;
	}

	public void setOrarioInizioBrano(Time orarioInizioBrano) {
		this.orarioInizioBrano = orarioInizioBrano;
	}

	public String getCategoriaUso() {
		return categoriaUso;
	}

	public void setCategoriaUso(String categoriaUso) {
		this.categoriaUso = categoriaUso;
	}

	public UtilizzazioniArmonizzatoDTO(Object[] obj, boolean tv) {

		if (tv) {
			try {
				canale = (String) obj[0];
				id = (Long) obj[1];
				genereTrasmissione = (String) obj[2];
				titoloTrasmissione = (String) obj[3];
				titoloOriginaleTrasmissione = (String) obj[4];
				dataInizioTrasmissione = (Date) obj[5];
				orarioInizioTrasmissione = (Time) obj[6];
				orarioFineTrasmissione = (Time) obj[7];
				durataTrasmissione = (String) obj[8];
				replicaOPrimaEsecuzione = (String) obj[9];
				paeseDiProduzione = (String) obj[10];
				annoDiProduzione = (((Integer) obj[11]))+"";
				produttore = (String) (obj[12]);
				regista = (String) (obj[13]);
				titoloEpisodio = (String) (obj[14]);
				titoloOriginaleEpisodio = (String) (obj[15]);
				numeroProgressivoEpisodio = (Integer) (obj[16]);
				diffusioneRegionale = (String) (obj[17]);
				regola1 = (String) (obj[18]);
				regola2 = (String) (obj[19]);
				regola3 = (String) (obj[20]);

			} catch (Throwable e) {
				e.printStackTrace();
			}
		} else {
			try {
				canale = (String) obj[0];
				id = (Long) obj[1];
				titoloTrasmissione = (String) obj[2];
				dataInizioTrasmissione = (Date) obj[3];
				orarioInizioTrasmissione = (Time) obj[4];
				orarioFineTrasmissione = (Time) obj[5];
				durataTrasmissione = (String) obj[6];
				titoloBrano = (String) obj[7];
				compositoreAutore = (String) obj[8];
				esecutorePerformer = (String) obj[9];
				album = (String) obj[10];
				durataBrano = ((Integer) (obj[11]))+"";
				codiceISWC = (String) (obj[12]);
				codiceISRC = (String) (obj[13]);
				orarioInizioBrano = (Time) (obj[14]);
				categoriaDUso = (String) (obj[15]);
				diffusioneRegionale= (String) (obj[16]);
				regola1 = (String) (obj[17]);
				regola2 = (String) (obj[18]);
				regola3 = (String) (obj[19]);
			} catch (Throwable e) {
				e.printStackTrace();
			}
			
		}

	}
}
