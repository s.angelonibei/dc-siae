package com.alkemytech.sophia.broadcasting.utils;

public interface Constants {
	//GENERAL
	public final static String SCOPE_TELE	= "T";
	public final static String SCOPE_RADIO	= "R";
	public final static String FLAG_NO 		= "0";
	public final static String FLAG_YES 	= "1";


	//HARMONIZATION UTILS
	public final static String HARMONIZED_FILE_PREFIX = "HARM_";
	public final static String LOCK_SUFFIX = ".lock";
    public final static String PRELOCK_SUFFIX = ".prelock";
	
	//DROOLS
	public final static String GLOBAL_TV_RECORD = 	    "tvRecord";
	public final static String GLOBAL_RADIO_RECORD = 	"radioRecord";
	public final static String GLOBAL_ERROR = 	        "errors";
		 
	//BEANIO
	public final static String RECORD_HEADER = 	"header";
	public final static String RECORD_BODY = 	"record";
	public final static String RECORD_FOOTER = 	"footer";

	//NORMALIZED DEFAULT
	public final static String DEFAULT_WRITE_EXT			=".csv";
	public final static String DEFAULT_WRITE_BUCKET			="s3://siae-sophia-broadcasting/";

	//NORMALIZED FILE PROPERTY
	// -- original fields
	public final static String CANALE						="canale";
	public final static String GENERE_TX					="genereTx";
	public final static String TITOLO_TX					="titoloTx";
	public final static String TITOLO_ORIGINALE_TX			="titoloOriginaleTx";
	public final static String DATA_INIZIO_TX				="dataInizioTx";
	public final static String ORARIO_INIZIO_TX				="orarioInizioTx";
	public final static String ORARIO_FINE_TX				="orarioFineTx";
	public final static String DURATA_TX					="durataTx";
	public final static String REPLICA_TX					="replicaTx";
	public final static String PAESE_PRODUZIONE_TX			="paeseProduzioneTx";
	public final static String ANNO_PRODUZIONE_TX			="annoProduzioneTx";
	public final static String PRODUTTORE_TX				="produttoreTx";
	public final static String REGISTA_TX					="registaTx";
	public final static String TITOLO_EPISODIO_TX			="titoloEpisodioTx";
	public final static String TITOLO_ORIGINALE_EPISODIO_TX	="titoloOriginaleEpisodioTx";
	public final static String NUM_PROGRESSIVO_EPISODIO_TX	="numProgressivoEpisodioTx";
	public final static String TITOLO_OP					="titoloOp";
	public final static String COMPOSITORE1_OP				="compositore1Op";
	public final static String COMPOSITORE2_OP				="compositore2Op";
	public final static String DURATA_OP					="durataOp";
	public final static String DURATA_EFFETTIVA_OP			="durataEffettivaOp";
	public final static String CATEGORIA_USO_OP				="categoriaUsoOp";
	public final static String DIFFUSIONE_REGIONALE			="diffusioneRegionale";
	// -- nuovo tracciato RAI
	public final static String IDENTIFICATIVO_RAI = "identificativoRai";

	// -- decoded fields
	public final static String DECODE_ID_CANALE				="DecodeIdCanale";
	public final static String DECODE_ID_GENERE_TX			="DecodiIdGenereTx";
	public final static String DECODE_DESC_GENERE_TX		="DecodeGenereTx";
	public final static String DECODE_DATA_INIZIO_TX		="DecodeDataInizioTx";
	public final static String DECODE_ORARIO_INIZIO_TX		="DecodeOrarioInizioTx";
	public final static String DECODE_ORARIO_FINE_TX		="DecodeOrarioFineTx";
	public final static String DECODE_DURATA_TX				="DecodeDurataTx";
	public final static String DECODE_ANNO_PRODUZIONE_TX	="DecodeAnnoProduzioneTx";
	public final static String DECODE_DURATA_OP				="DecodeDurataOp";
	public final static String DECODE_DURATA_EFFETTIVA_OP	="DecodeDurataEffettivaOp";
	public final static String DECODE_ORARIO_INIZIO_OP		="DecodeOrarioInizioOp";
	public final static String DECODE_ID_CATEGORIA_USO_OP	="DecodeIdCategoriaUsoOp";
	// -- nuovo tracciato RAI
	public final static String DECODE_IDENTIFICATIVO_RAI = "IdentificativoRai";

	// -- error field
	public final static String ERRORS						="Errors";

	// -- rule fields
	public final static String REGOLA1						="regola1";
	public final static String REGOLA2						="regola2";
	public final static String REGOLA3						="regola3";


}
