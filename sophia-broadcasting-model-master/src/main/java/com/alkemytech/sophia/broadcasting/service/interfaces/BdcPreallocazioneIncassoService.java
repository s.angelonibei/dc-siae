package com.alkemytech.sophia.broadcasting.service.interfaces;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import com.alkemytech.sophia.broadcasting.dto.BdcContiDTO;
import com.alkemytech.sophia.broadcasting.dto.BdcUpdatePianoContiRequestDTO;
import com.alkemytech.sophia.broadcasting.model.BdcPianoDeiConti;


public interface BdcPreallocazioneIncassoService {
	List<BdcContiDTO> findAll(Date dateFrom, Date dateTo, String tipoEmittente, Integer idEmittente, Integer idCanale,String tipoDiritto,
			Integer first, Integer last, Integer maxrow);
	
	BdcPianoDeiConti getPianoContoDettails(Integer idPianoDeiConti);

	BdcPianoDeiConti updatePianoConti(BdcUpdatePianoContiRequestDTO bdcUpdatePianoContiRequestDTO);

	Integer updateCaricoDiRipartoDaConto(EntityManager em, String numeroConto);
	
	List<BdcContiDTO> getStoricoConto(String idPianoDeiConti);

	
}
