package com.alkemytech.sophia.broadcasting.service;

import com.alkemytech.sophia.broadcasting.dto.FileKpiDTO;
import com.alkemytech.sophia.broadcasting.enums.Repertorio;
import com.alkemytech.sophia.broadcasting.enums.Stato;
import com.alkemytech.sophia.broadcasting.model.BdcBroadcasters;
import com.alkemytech.sophia.broadcasting.model.BdcInformationFileEntity;
import com.alkemytech.sophia.broadcasting.service.interfaces.InformationFileEntityService;
import com.alkemytech.sophia.performing.model.PerfRsUtilizationFile;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.apache.commons.lang.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.lang.reflect.Type;
import java.util.*;

public class InformationFileEntityServiceImpl implements InformationFileEntityService {

	private final Provider<EntityManager> provider;

	@Inject
	public InformationFileEntityServiceImpl(Provider<EntityManager> provider) {
		this.provider = provider;
	}

	@Override
	public List<BdcInformationFileEntity> findAll() {
		EntityManager em = null;
		em = provider.get();

		final Query q = em.createQuery("SELECT a FROM BdcInformationFileEntity a");

		return (List<BdcInformationFileEntity>) q.getResultList();
	}

	@Override
	public List<BdcInformationFileEntity> findByStato(Stato stato) {

		EntityManager em = null;
		em = provider.get();

		final Query q = em.createQuery("SELECT a FROM BdcInformationFileEntity a WHERE a.stato=:stato")
				.setParameter("stato", stato);

		return (List<BdcInformationFileEntity>) q.getResultList();
	}

	@Override
	public BdcInformationFileEntity findById(Integer id) {
		EntityManager em = null;
		em = provider.get();

		final Query q = em.createQuery("SELECT a FROM BdcInformationFileEntity a WHERE a.id=:id").setParameter("id",
				id);
		return (BdcInformationFileEntity) q.getSingleResult();
	}

	@Override
	public List<BdcInformationFileEntity> getListaFiles(BdcBroadcasters emittente, Stato stato) {
		EntityManager entityManager = provider.get();
		List<BdcInformationFileEntity> fileEntityList;
		String w = "SELECT new com.alkemytech.sophia.broadcasting.model.BdcInformationFileEntity(b.id, b.nomeFile, b.dataUpload, b.percorso, b.tipoUpload, b.stato, b.dataProcessamento) "
				+ "FROM BdcInformationFileEntity b WHERE b.emittente=:emittente AND b.stato=:stato ORDER BY b.dataUpload DESC";
		TypedQuery<BdcInformationFileEntity> queryListFiles = entityManager.createQuery(w,
				BdcInformationFileEntity.class);
		if (emittente != null && stato != null) {
			fileEntityList = queryListFiles.setParameter("emittente", emittente).setParameter("stato", stato)
					.getResultList();
		} else {
			fileEntityList = queryListFiles.getResultList();
		}
		return fileEntityList;
	}

	@Override
	public List<BdcInformationFileEntity> getListaFilesBroadcaster(BdcBroadcasters emittente, Repertorio repertorio) {
		EntityManager entityManager = provider.get();

		// String w = "SELECT new
		// com.alkemytech.sophia.broadcasting.model.BdcInformationFileEntity(" +
		// "b.id, " +
		// "b.nomeFile, " +
		// "b.dataUpload, " +
		// "b.percorso, " +
		// "b.tipoUpload, " +
		// "CASE b.stato WHEN :daElaborare THEN :inLavorazione " +
		// "WHEN :validato THEN :inLavorazione " +
		// "ELSE b.stato END, "+
		// "b.dataProcessamento) " +
		// "FROM BdcInformationFileEntity b WHERE b.emittente.id=:idEmittente ORDER BY
		// b.dataUpload DESC";

		Query queryListFiles = entityManager.createNamedQuery("BdcInformationFileEntity.getListaFilesBroadcaster");
		queryListFiles.setParameter(1, Stato.DA_ELABORARE.toString());
		queryListFiles.setParameter(2, Stato.VALIDATO.toString());
		queryListFiles.setParameter(3, Stato.IN_CONVERSIONE.toString());
		queryListFiles.setParameter(4, Stato.IN_LAVORAZIONE.toString());
		queryListFiles.setParameter(5, emittente.getId());
		queryListFiles.setParameter(6, repertorio.toString());
		List<BdcInformationFileEntity> resultList = queryListFiles.getResultList();

		// for(BdcInformationFileEntity file : resultList){
		// if(Stato.DA_ELABORARE.equals(file.getStato()) ||
		// Stato.VALIDATO.equals(file.getStato())){
		// file.setStato(Stato.IN_LAVORAZIONE);
		// }
		// }

		return resultList;
	}

	@Override
	public BdcInformationFileEntity save(BdcInformationFileEntity entity) {

		EntityManager entityManager = provider.get();
		entityManager.getTransaction().begin();
		if (StringUtils.isNotEmpty(entity.getsDataUpload())) {
			entity.setDataUpload(new Date(Long.parseLong(entity.getsDataUpload())));
		}
		try {
			if (entity.getId() == null) {
				entityManager.persist(entity);
			} else {
				entityManager.merge(entity);
				entityManager.flush();
			}
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
		}

		final Query q = entityManager.createQuery("SELECT a FROM BdcInformationFileEntity a WHERE a.id=:id")
				.setParameter("id", entity.getId());

		return (BdcInformationFileEntity) q.getSingleResult();
	}

	@Override
	public List<FileKpiDTO> getFileKpiToWork() {
		EntityManager em = provider.get();
		StringBuilder queryString = new StringBuilder("SELECT distinct a.* FROM bdc_utilization_file a WHERE a.stato in ('CONFERMATO','ELABORATO','ANNULLATO') AND a.KPI_OK IS NULL" );
		Gson gson = new GsonBuilder().create();
		final Query q = em.createNativeQuery(queryString.toString(), BdcInformationFileEntity.class);
		List<BdcInformationFileEntity> resultList = q.getResultList();
		List<FileKpiDTO> mapList=null;
		if(resultList != null){
			mapList= new ArrayList();
			for (BdcInformationFileEntity result : resultList) {
				Map<Integer, Set<String>> mapResult = null;
				Type type = new TypeToken<Map<String, Set<String>>>() {}.getType();
				String ambitoKpi = result.getAmbitoKpi() != null ? result.getAmbitoKpi() : "{}";
				Map<String, Set<String>> map = gson.fromJson(ambitoKpi, type);
				mapResult = new HashMap<>();

				for ( String key : map.keySet() ) {
					Integer idCanale = Integer.parseInt(key);

					mapResult.put(idCanale, new TreeSet<String>());

					for (String s : map.get(key)) {
						mapResult.get(idCanale).add(s);
					}

				}
				FileKpiDTO fileKpiDTO = new FileKpiDTO();
				fileKpiDTO.setBdcUtilizationFile(result);
				fileKpiDTO.setAmbitoKpi(mapResult);
				mapList.add(fileKpiDTO);
			}
		}
		return  mapList;
	}


	@Override
	public Boolean cancelUtilizationRecords(Integer idUtilizationFile) {

		//TODO controllare che email del broadcaster sia uguale a quella per cui si sta richiedendo la cancellazione
		EntityManager entityManager = provider.get();
		entityManager.getTransaction().begin();

		try {

			StringBuilder queryString = new StringBuilder("DELETE A FROM BDC_TV_SHOW_MUSIC A WHERE A.ID_NORMALIZED_FILE in ( SELECT ID_NORMALIZED_FILE FROM BDC_UTILIZATION_NORMALIZED_FILE WHERE ID_UTILIZATION_FILE = "+idUtilizationFile+" ) " );
			entityManager.createNativeQuery(queryString.toString()).executeUpdate();

			queryString = new StringBuilder("DELETE A FROM BDC_ERR_TV_SHOW_MUSIC A WHERE A.ID_NORMALIZED_FILE in ( SELECT ID_NORMALIZED_FILE FROM BDC_UTILIZATION_NORMALIZED_FILE WHERE ID_UTILIZATION_FILE = "+idUtilizationFile+" ) " );
			entityManager.createNativeQuery(queryString.toString()).executeUpdate();

			queryString = new StringBuilder("DELETE A FROM BDC_TV_SHOW_SCHEDULE A WHERE A.ID_NORMALIZED_FILE in ( SELECT ID_NORMALIZED_FILE FROM BDC_UTILIZATION_NORMALIZED_FILE WHERE ID_UTILIZATION_FILE = "+idUtilizationFile+" ) " );
			entityManager.createNativeQuery(queryString.toString()).executeUpdate();

			queryString = new StringBuilder("DELETE A FROM BDC_ERR_TV_SHOW_SCHEDULE A WHERE A.ID_NORMALIZED_FILE in ( SELECT ID_NORMALIZED_FILE FROM BDC_UTILIZATION_NORMALIZED_FILE WHERE ID_UTILIZATION_FILE = "+idUtilizationFile+" ) " );
			entityManager.createNativeQuery(queryString.toString()).executeUpdate();

			queryString = new StringBuilder("DELETE A FROM BDC_RD_SHOW_MUSIC A WHERE A.ID_NORMALIZED_FILE in ( SELECT ID_NORMALIZED_FILE FROM BDC_UTILIZATION_NORMALIZED_FILE WHERE ID_UTILIZATION_FILE = "+idUtilizationFile+" ) " );
			entityManager.createNativeQuery(queryString.toString()).executeUpdate();

			queryString = new StringBuilder("DELETE A FROM BDC_ERR_RD_SHOW_MUSIC A WHERE A.ID_NORMALIZED_FILE in ( SELECT ID_NORMALIZED_FILE FROM BDC_UTILIZATION_NORMALIZED_FILE WHERE ID_UTILIZATION_FILE = "+idUtilizationFile+" ) " );
			entityManager.createNativeQuery(queryString.toString()).executeUpdate();

			queryString = new StringBuilder("DELETE A FROM BDC_RD_SHOW_SCHEDULE A WHERE A.ID_NORMALIZED_FILE in ( SELECT ID_NORMALIZED_FILE FROM BDC_UTILIZATION_NORMALIZED_FILE WHERE ID_UTILIZATION_FILE = "+idUtilizationFile+" ) " );
			entityManager.createNativeQuery(queryString.toString()).executeUpdate();

			queryString = new StringBuilder("DELETE A FROM BDC_ERR_RD_SHOW_SCHEDULE A WHERE A.ID_NORMALIZED_FILE in ( SELECT ID_NORMALIZED_FILE FROM BDC_UTILIZATION_NORMALIZED_FILE WHERE ID_UTILIZATION_FILE = "+idUtilizationFile+" ) " );
			entityManager.createNativeQuery(queryString.toString()).executeUpdate();


			queryString = new StringBuilder("update bdc_utilization_file set stato='ANNULLATO', KPI_OK = NULL WHERE ID="+idUtilizationFile );
			entityManager.createNativeQuery(queryString.toString()).executeUpdate();

			entityManager.flush();
			entityManager.getTransaction().commit();

		} catch (Exception e) {
			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
			return false;
		}

		return true;
	}

	@Override
	public Boolean confirmUtilizationRecords(Integer idUtilizationFile) {

		//TODO controllare che email del broadcaster sia uguale a quella per cui si sta richiedendo la cancellazione
		EntityManager entityManager = provider.get();
		entityManager.getTransaction().begin();

		try {

			StringBuilder queryString = new StringBuilder("update bdc_utilization_file set stato='CONFERMATO' WHERE ID="+idUtilizationFile );
			entityManager.createNativeQuery(queryString.toString()).executeUpdate();

			entityManager.flush();
			entityManager.getTransaction().commit();

		} catch (Exception e) {
			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
			return false;
		}

		return true;
	}

}
