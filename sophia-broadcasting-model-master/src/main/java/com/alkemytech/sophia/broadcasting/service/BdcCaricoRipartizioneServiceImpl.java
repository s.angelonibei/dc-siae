package com.alkemytech.sophia.broadcasting.service;

import com.alkemytech.sophia.broadcasting.data.IndicatoriPerCanale;
import com.alkemytech.sophia.broadcasting.dto.KPI;
import com.alkemytech.sophia.broadcasting.model.BdcCaricoRipartizione;
import com.alkemytech.sophia.broadcasting.model.BdcCaricoRipartizione.TIPO_INCASSO;
import com.alkemytech.sophia.broadcasting.model.BdcCaricoRipartizioneStoricoModifica;
import com.alkemytech.sophia.broadcasting.model.TipoBroadcaster;
import com.alkemytech.sophia.broadcasting.service.interfaces.BdcCaricoRipartizioneService;
import com.alkemytech.sophia.broadcasting.service.interfaces.KpiService;
import com.google.inject.Inject;
import com.google.inject.Provider;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class BdcCaricoRipartizioneServiceImpl implements BdcCaricoRipartizioneService {

	private final Provider<EntityManager> provider;
	private KpiService kpiService;

	@Inject
	public BdcCaricoRipartizioneServiceImpl(Provider<EntityManager> provider,KpiService kpiService) {
		this.provider = provider;
		this.kpiService = kpiService;
	}

	@Override
	public List<BdcCaricoRipartizione> findAll(Date dataFrom,Date dataTo, String tipoEmittente, Integer idEmittente, Integer idCanale,
			Integer first, Integer last, Integer maxrow ) {
		EntityManager em = null;
		em = provider.get();
		List<BdcCaricoRipartizione> bdcCaricoRipartizioni= null;
		
		HashMap<String, Double>mappaSoglie= new HashMap<>();
		mappaSoglie.put(KPI.KPI_COPERTURA, 0.70);
		mappaSoglie.put(KPI.KPI_DURATA_MUSICA_DICHIARATA, 0.30);
		mappaSoglie.put(KPI.KPI_BRANI_TITOLI_ERRONEI, 0.30);
		mappaSoglie.put(KPI.KPI_FILM_TITOLI_ERRONEI, 0.30);
		mappaSoglie.put(KPI.KPI_AUTORI_TITOLI_ERRONEI, 0.30);
		mappaSoglie.put(KPI.KPI_DURATA_BRANI_DICHIARATI, 0.60);
		mappaSoglie.put(KPI.KPI_BRANI_RD_TITOLI_ERRONEI, 0.30);
		mappaSoglie.put(KPI.KPI_AUTORI_RD_TITOLI_ERRONEI, 0.30);
		
		
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

		CriteriaQuery<BdcCaricoRipartizione> query = criteriaBuilder.createQuery(BdcCaricoRipartizione.class);
		Root<BdcCaricoRipartizione> bdcCaricoRipartizione = query.from(BdcCaricoRipartizione.class);

        
		List<Predicate> predicates = new ArrayList<>();
		if (null != dataTo && null != dataFrom) {
			predicates.add(criteriaBuilder.greaterThanOrEqualTo(bdcCaricoRipartizione.<Date>get("inizioPeriodoCompetenza"),dataFrom));	
			predicates.add(criteriaBuilder.lessThanOrEqualTo(bdcCaricoRipartizione.<Date>get("finePeriodoCompetenza"), dataTo));	
		}
		if (null != idCanale) {
			predicates.add(criteriaBuilder.equal(bdcCaricoRipartizione.get("canale").get("id"), idCanale));
		}
		if (null != idEmittente) {
			predicates.add(criteriaBuilder.equal(bdcCaricoRipartizione.get("canale").get("bdcBroadcasters").get("id"), idEmittente));
		}
		
		if (null != tipoEmittente) {
			predicates.add(criteriaBuilder.equal(bdcCaricoRipartizione.get("canale").get("tipoCanale"),TipoBroadcaster.valueOf(tipoEmittente)));
		}
		
		query
		.where(predicates.toArray(new Predicate[] {}))
		.orderBy(criteriaBuilder.asc(bdcCaricoRipartizione.get("tipoDiritto")), criteriaBuilder.asc(bdcCaricoRipartizione.get("canale").get("bdcBroadcasters").get("nome")), criteriaBuilder.asc(bdcCaricoRipartizione.get("canale").get("id")));
		TipoBroadcaster tipo=null;
		
		
		/*
		if (first!=null&&last!=null) {
			bdcCaricoRipartizioni= (List<BdcCaricoRipartizione>) em.createQuery(query).setFirstResult(first).setMaxResults(1 + last - first).getResultList();
		}else {*/
			bdcCaricoRipartizioni= (List<BdcCaricoRipartizione>) em.createQuery(query).getResultList();
		//}
		

		IndicatoriPerCanale indicatoriPerCanale = kpiService.indicatoriPerCanale(dataFrom, dataTo);
		
		for(BdcCaricoRipartizione caricoRipartizione: bdcCaricoRipartizioni) {
			List<KPI> kpiList = kpiService.listKpi(caricoRipartizione.getInizioPeriodoCompetenza(), 
												   caricoRipartizione.getFinePeriodoCompetenza(), 
												   caricoRipartizione.getCanale().getId(),
												   caricoRipartizione.getCanale().getTipoCanale(), 
												   indicatoriPerCanale);
			caricoRipartizione.setKpi(kpiList);	
			
			if (kpiList!=null&&!kpiList.isEmpty()) {
				boolean kpiValido= checkKpi(caricoRipartizione,kpiList,mappaSoglie);
				caricoRipartizione.setKpiValido(kpiValido);
				caricoRipartizione.setRepoDisponibile(true);
			}else {
				caricoRipartizione.setRepoDisponibile(false);
				caricoRipartizione.setKpiValido(false);
			}
			
			caricoRipartizione.setImportoRipartibile(checkImportoRipartibile(caricoRipartizione, kpiList, mappaSoglie));
		}		
		return bdcCaricoRipartizioni;
		
	}



	@Override
	public Integer updateCaricoRipartizione(String username, BdcCaricoRipartizione caricoRipartizione) {
		EntityManager em = null;
		em = provider.get();
		boolean asChange=false;
		BdcCaricoRipartizione oldBdcCaricoRipartizione= (BdcCaricoRipartizione) em.createQuery("Select x from BdcCaricoRipartizione x where x.id=:id").setParameter("id", caricoRipartizione.getId()).getSingleResult();
		em.getTransaction().begin();
		ArrayList<BdcCaricoRipartizioneStoricoModifica> arrayList=new ArrayList<>();
		
		Date date=new Date();
		if (!oldBdcCaricoRipartizione.getIncasso().equals(caricoRipartizione.getIncasso())) {
			asChange=true;
			arrayList.add(new BdcCaricoRipartizioneStoricoModifica(caricoRipartizione.getId(), "Incasso", oldBdcCaricoRipartizione.getIncasso()+"", caricoRipartizione.getIncasso()+"", username,date));
		
			oldBdcCaricoRipartizione.setIncasso(caricoRipartizione.getIncasso());
		}
		
		
		if (oldBdcCaricoRipartizione.isSospesoPerContenzioso()!=caricoRipartizione.isSospesoPerContenzioso()) {
			asChange=true;
			arrayList.add(new BdcCaricoRipartizioneStoricoModifica(caricoRipartizione.getId(), "Sospeso Per Contenzioso", 
					oldBdcCaricoRipartizione.isSospesoPerContenzioso()+"", caricoRipartizione.isSospesoPerContenzioso()+"", username,date));
			

			oldBdcCaricoRipartizione.setSospesoPerContenzioso(caricoRipartizione.isSospesoPerContenzioso());
		}
		
		if(oldBdcCaricoRipartizione.getNotaCommentoUffEmittenti()==null && caricoRipartizione.getNotaCommentoUffEmittenti()==null) {
			
			
		} else if (oldBdcCaricoRipartizione.getNotaCommentoUffEmittenti()==null && caricoRipartizione.getNotaCommentoUffEmittenti()!=null) {
			asChange=true;
			arrayList.add(new BdcCaricoRipartizioneStoricoModifica(caricoRipartizione.getId(), "Nota Commento Uff. Emittenti", 
					"", caricoRipartizione.getNotaCommentoUffEmittenti(), username,date));
			

			oldBdcCaricoRipartizione.setNotaCommentoUffEmittenti(caricoRipartizione.getNotaCommentoUffEmittenti());	
		} else if (oldBdcCaricoRipartizione.getNotaCommentoUffEmittenti()!=null && caricoRipartizione.getNotaCommentoUffEmittenti()==null) {
			asChange=true;
			arrayList.add(new BdcCaricoRipartizioneStoricoModifica(caricoRipartizione.getId(), "Nota Commento Uff. Emittenti", 
					oldBdcCaricoRipartizione.getNotaCommentoUffEmittenti()+"", "", username,date));
			

			oldBdcCaricoRipartizione.setNotaCommentoUffEmittenti(caricoRipartizione.getNotaCommentoUffEmittenti());	
		}else if (!oldBdcCaricoRipartizione.getNotaCommentoUffEmittenti().equals(caricoRipartizione.getNotaCommentoUffEmittenti())) {
			asChange=true;
			arrayList.add(new BdcCaricoRipartizioneStoricoModifica(caricoRipartizione.getId(), "Nota Commento Uff. Emittenti", 
					oldBdcCaricoRipartizione.getNotaCommentoUffEmittenti()+"", caricoRipartizione.getNotaCommentoUffEmittenti()+"", username,date));
			

			oldBdcCaricoRipartizione.setNotaCommentoUffEmittenti(caricoRipartizione.getNotaCommentoUffEmittenti());
		}
		
		
		
		
		if(oldBdcCaricoRipartizione.getNotaCommentoDirRipartizione()==null && caricoRipartizione.getNotaCommentoDirRipartizione()==null) {
			
		} else if (oldBdcCaricoRipartizione.getNotaCommentoDirRipartizione()==null && caricoRipartizione.getNotaCommentoDirRipartizione()!=null) {
			asChange=true;
			arrayList.add(new BdcCaricoRipartizioneStoricoModifica(caricoRipartizione.getId(), "Nota Commento Dir. Ripartizione", 
					"", caricoRipartizione.getNotaCommentoDirRipartizione(), username,date));
			

			oldBdcCaricoRipartizione.setNotaCommentoDirRipartizione(caricoRipartizione.getNotaCommentoDirRipartizione());	
		} else if (oldBdcCaricoRipartizione.getNotaCommentoDirRipartizione()!=null && caricoRipartizione.getNotaCommentoDirRipartizione()==null) {
			asChange=true;
			arrayList.add(new BdcCaricoRipartizioneStoricoModifica(caricoRipartizione.getId(), "Nota Commento Dir. Ripartizione", 
					oldBdcCaricoRipartizione.getNotaCommentoDirRipartizione()+"", "", username,date));
			

			oldBdcCaricoRipartizione.setNotaCommentoDirRipartizione(caricoRipartizione.getNotaCommentoDirRipartizione());	
		}else if (!oldBdcCaricoRipartizione.getNotaCommentoDirRipartizione().equals(caricoRipartizione.getNotaCommentoDirRipartizione())) {
			asChange=true;
			arrayList.add(new BdcCaricoRipartizioneStoricoModifica(caricoRipartizione.getId(), "Nota Commento Dir. Ripartizione", 
					oldBdcCaricoRipartizione.getNotaCommentoDirRipartizione()+"", caricoRipartizione.getNotaCommentoDirRipartizione()+"", username,date));
			

			oldBdcCaricoRipartizione.setNotaCommentoDirRipartizione(caricoRipartizione.getNotaCommentoDirRipartizione());
		}
		
		if (caricoRipartizione.isImportoRipartibile()!=null&&oldBdcCaricoRipartizione.isImportoRipartibile()!=caricoRipartizione.isImportoRipartibile()) {
			asChange=true;
			arrayList.add(new BdcCaricoRipartizioneStoricoModifica(caricoRipartizione.getId(), "Importo Ripartibile", 
					oldBdcCaricoRipartizione.isImportoRipartibile()+"", caricoRipartizione.isImportoRipartibile()+"", username,date));

			oldBdcCaricoRipartizione.setImportoRipartibile(caricoRipartizione.isImportoRipartibile());

		}
		try {
			if (asChange) {
				for (BdcCaricoRipartizioneStoricoModifica modifica:arrayList) {
					em.persist(modifica);
					em.flush();	
				}
				em.persist(oldBdcCaricoRipartizione);
				em.flush();	
				em.getTransaction().commit();
				return 200;
			}else {
				em.getTransaction().commit();
				return 409;
			}	
		} catch (Exception e) {
			em.getTransaction().rollback();
			return 500;
		}
	
	}

	@Override
	public List<BdcCaricoRipartizioneStoricoModifica> findAllStorico(Integer idCaricoRipartizione) {
		EntityManager em = null;
		em = provider.get();
		List<BdcCaricoRipartizioneStoricoModifica> list= (List<BdcCaricoRipartizioneStoricoModifica>) em.createQuery("Select x from BdcCaricoRipartizioneStoricoModifica x where x.idCaricoRipartizione=:idCaricoRipartizione").setParameter("idCaricoRipartizione", idCaricoRipartizione).getResultList();
		return list;
		
	}
	
	
	private boolean checkImportoRipartibile(BdcCaricoRipartizione bdcCaricoRipartizione,List<KPI> kpiList,HashMap<String, Double> mappaSoglie) {
		
		// CONTROLLO SE IMPORTO RIPARTIBILE è PERSISTITO
		if (bdcCaricoRipartizione.isImportoRipartibile()!=null) {
			return bdcCaricoRipartizione.isImportoRipartibile();
		}
		// CONTROLLO I FLAG PER LA RIPARTIBILITà DEL CARICO DI RIPARTO
		if (!bdcCaricoRipartizione.isRepoDisponibile() ||
			!bdcCaricoRipartizione.getKpiValido()||
			bdcCaricoRipartizione.isSospesoPerContenzioso()||
			TIPO_INCASSO.PARZIALE.equals(bdcCaricoRipartizione.getIncasso())) {
			
			return false;
		}
		return true;
	}
	
	private boolean checkKpi(BdcCaricoRipartizione bdcCaricoRipartizione,List<KPI> kpiList,HashMap<String, Double> mappaSoglie) {
		HashMap<String, KPI> mappaKpi= new HashMap<>();
		for (KPI kpi: kpiList) {
			mappaKpi.put(kpi.getKpi(), kpi);
		}
		if (TipoBroadcaster.TELEVISIONE.equals(bdcCaricoRipartizione.getCanale().getTipoCanale())) {
			if (mappaKpi.get(KPI.KPI_COPERTURA)==null || mappaKpi.get(KPI.KPI_COPERTURA).getValore().doubleValue()<=(mappaSoglie.get(KPI.KPI_COPERTURA)*100)) {
				return false;
			}
			if (mappaKpi.get(KPI.KPI_DURATA_MUSICA_DICHIARATA)==null || mappaKpi.get(KPI.KPI_DURATA_MUSICA_DICHIARATA).getValore().doubleValue()<=(mappaSoglie.get(KPI.KPI_DURATA_MUSICA_DICHIARATA)*100)) {
				return false;
			}
			if (mappaKpi.get(KPI.KPI_BRANI_TITOLI_ERRONEI)==null || mappaKpi.get(KPI.KPI_BRANI_TITOLI_ERRONEI).getValore().doubleValue()>=(mappaSoglie.get(KPI.KPI_BRANI_TITOLI_ERRONEI)*100)) {
				return false;
			}
			if (mappaKpi.get(KPI.KPI_FILM_TITOLI_ERRONEI)==null || mappaKpi.get(KPI.KPI_FILM_TITOLI_ERRONEI).getValore().doubleValue()>=(mappaSoglie.get(KPI.KPI_FILM_TITOLI_ERRONEI)*100)) {
				return false;
			}
			if (mappaKpi.get(KPI.KPI_AUTORI_TITOLI_ERRONEI)==null || mappaKpi.get(KPI.KPI_AUTORI_TITOLI_ERRONEI).getValore().doubleValue()>=(mappaSoglie.get(KPI.KPI_AUTORI_TITOLI_ERRONEI)*100)) {
				return false;
			}
			return true;
		}else if (TipoBroadcaster.RADIO.equals(bdcCaricoRipartizione.getCanale().getTipoCanale())) {
			if (mappaKpi.get(KPI.KPI_DURATA_BRANI_DICHIARATI)==null || mappaKpi.get(KPI.KPI_DURATA_BRANI_DICHIARATI).getValore().doubleValue()<=(mappaSoglie.get(KPI.KPI_DURATA_BRANI_DICHIARATI)*100)) {
				return false;
			}
			if (mappaKpi.get(KPI.KPI_BRANI_RD_TITOLI_ERRONEI)==null || mappaKpi.get(KPI.KPI_BRANI_RD_TITOLI_ERRONEI).getValore().doubleValue()>=(mappaSoglie.get(KPI.KPI_BRANI_RD_TITOLI_ERRONEI)*100)) {
				return false;
			}
			if (mappaKpi.get(KPI.KPI_AUTORI_RD_TITOLI_ERRONEI)==null || mappaKpi.get(KPI.KPI_AUTORI_RD_TITOLI_ERRONEI).getValore().doubleValue()>=(mappaSoglie.get(KPI.KPI_AUTORI_RD_TITOLI_ERRONEI)*100)) {
				return false;
			}
			return true;
		}
		return false;
	}
	
	
	
	
}