package com.alkemytech.sophia.broadcasting.dto;

import java.util.Date;
import com.alkemytech.sophia.broadcasting.model.MusicType;
import com.alkemytech.sophia.broadcasting.model.utilizations.BdcErrRdShowMusic;
import com.alkemytech.sophia.broadcasting.model.utilizations.BdcErrTvShowMusic;
import com.alkemytech.sophia.broadcasting.model.utilizations.BdcRdShowMusic;
import com.alkemytech.sophia.broadcasting.model.utilizations.BdcTvShowMusic;

public class BdcShowMusicDTO {

	private Long id;
	private MusicType musicType;
	private String title;
	private String composer;
	private String performer;
	private String album;
	private Integer duration;
	private String iswcCode;
	private String isrcCode;
	private Date beginTime;
	private Integer realDuration;
	private String error;
	private String reportDuration;
	private String reportBeginTime;
	private String reportMusicType;
	private Date creationDate;
	private Date modifyDate;
	private String firstComposer;
	private String secondComposer;
	private String reportRealDuration;
	private String note;
	private Integer year;
	private Integer month;
	private Integer daysFromUpload;

	public BdcShowMusicDTO() {
		super();
	}

	public BdcShowMusicDTO(Long id, MusicType musicType, String title, String composer, String performer, String album,
			Integer duration, String iswcCode, String isrcCode, Date beginTime, Integer realDuration, Boolean editable,
			String error, String reportDuration, String reportBeginTime, String reportMusicType, Date creationDate,
			Date modifyDate, String firstComposer, String secondComposer, String reportRealDuration, String note,
			Integer year, Integer month, Integer daysFromUpload) {
		super();
		this.id = id;
		this.musicType = musicType;
		this.title = title;
		this.composer = composer;
		this.performer = performer;
		this.album = album;
		this.duration = duration;
		this.iswcCode = iswcCode;
		this.isrcCode = isrcCode;
		this.beginTime = beginTime;
		this.realDuration = realDuration;
		this.error = error;
		this.reportDuration = reportDuration;
		this.reportBeginTime = reportBeginTime;
		this.reportMusicType = reportMusicType;
		this.creationDate = creationDate;
		this.modifyDate = modifyDate;
		this.firstComposer = firstComposer;
		this.secondComposer = secondComposer;
		this.reportRealDuration = reportRealDuration;
		this.note = note;
		this.year = year;
		this.month = month;
		this.daysFromUpload = daysFromUpload;
	}

	public BdcShowMusicDTO(BdcErrRdShowMusic bdcErrRdShowMusic) {
		super();
		this.id = bdcErrRdShowMusic.getId();
		this.musicType = bdcErrRdShowMusic.getMusicType();
		this.title = bdcErrRdShowMusic.getAlbum();
		this.composer = bdcErrRdShowMusic.getComposer();
		this.performer = bdcErrRdShowMusic.getPerformer();
		this.album = bdcErrRdShowMusic.getAlbum();
		this.duration = bdcErrRdShowMusic.getDuration();
		this.iswcCode = bdcErrRdShowMusic.getIswcCode();
		this.isrcCode = bdcErrRdShowMusic.getIswcCode();
		this.beginTime = bdcErrRdShowMusic.getBeginTime();
		this.realDuration = bdcErrRdShowMusic.getRealDuration();
		this.reportDuration = bdcErrRdShowMusic.getReportDuration() + "";
		this.reportBeginTime = bdcErrRdShowMusic.getReportBeginTime() + "";
		this.reportMusicType = bdcErrRdShowMusic.getReportMusicType() + "";
		this.creationDate = bdcErrRdShowMusic.getCreationDate();
		this.modifyDate = bdcErrRdShowMusic.getModifyDate();
		this.firstComposer = null;
		this.secondComposer = null;
		this.reportRealDuration = null;
		this.note = null;
		this.year = null;
		this.month = null;
		this.daysFromUpload = null;
	}

	public BdcShowMusicDTO(BdcErrTvShowMusic bdcErrTvShowMusic) {
		super();
		this.id = bdcErrTvShowMusic.getId();
		this.musicType = bdcErrTvShowMusic.getMusicType();
		this.title = bdcErrTvShowMusic.getTitle();
		this.composer = bdcErrTvShowMusic.getFirstComposer();
		this.performer = null;
		this.album = null;
		this.duration = bdcErrTvShowMusic.getDuration();
		this.iswcCode = null;
		this.isrcCode = null;
		this.beginTime = null;
		this.realDuration = bdcErrTvShowMusic.getRealDuration();
		this.reportBeginTime = bdcErrTvShowMusic.getReportDuration();
		this.reportMusicType = bdcErrTvShowMusic.getReportMusicType();
		this.creationDate = bdcErrTvShowMusic.getCreationDate();
		this.modifyDate = bdcErrTvShowMusic.getModifyDate();
		this.firstComposer = bdcErrTvShowMusic.getFirstComposer();
		this.secondComposer = bdcErrTvShowMusic.getSecondComposer();
		this.reportRealDuration = bdcErrTvShowMusic.getReportRealDuration();
		this.note = null;
		this.year = null;
		this.month = null;
		this.daysFromUpload = null;
	}

	public BdcShowMusicDTO(BdcTvShowMusic bdcTvShowMusic) {
		super();
		this.id = bdcTvShowMusic.getId();
		this.musicType = bdcTvShowMusic.getMusicType();
		this.title = bdcTvShowMusic.getTitle();
		this.composer = bdcTvShowMusic.getFirstComposer();
		this.performer = null;
		this.album = null;
		this.duration = bdcTvShowMusic.getDuration();
		this.iswcCode = null;
		this.isrcCode = null;
		this.beginTime = getBeginTime();
		this.realDuration = bdcTvShowMusic.getRealDuration();
		this.error = null;
		this.reportDuration = null;
		this.reportBeginTime = null;
		this.reportMusicType = null;
		this.creationDate = bdcTvShowMusic.getCreationDate();
		this.modifyDate = bdcTvShowMusic.getModifyDate();
		this.firstComposer = bdcTvShowMusic.getFirstComposer();
		this.secondComposer = bdcTvShowMusic.getSecondComposer();
		this.reportRealDuration = null;
		this.note = bdcTvShowMusic.getNote();
		this.daysFromUpload = bdcTvShowMusic.getDaysFromUpload();
	}

	public BdcShowMusicDTO(BdcRdShowMusic bdcRdShowMusic) {
		super();
		this.id = bdcRdShowMusic.getId();
		this.musicType = bdcRdShowMusic.getMusicType();
		this.title = bdcRdShowMusic.getTitle();
		this.composer = bdcRdShowMusic.getComposer();
		this.performer = bdcRdShowMusic.getPerformer();
		this.album = bdcRdShowMusic.getAlbum();
		this.duration = bdcRdShowMusic.getDuration();
		this.iswcCode = bdcRdShowMusic.getIswcCode();
		this.isrcCode = bdcRdShowMusic.getIsrcCode();
		this.beginTime = bdcRdShowMusic.getBeginTime();
		this.realDuration = bdcRdShowMusic.getRealDuration();
		this.error = null;
		this.reportDuration = null;
		this.reportBeginTime = null;
		this.reportMusicType = null;
		this.creationDate = bdcRdShowMusic.getCreationDate();
		this.modifyDate = bdcRdShowMusic.getModifyDate();
		this.firstComposer = null;
		this.secondComposer = null;
		this.reportRealDuration = null;
		this.note = bdcRdShowMusic.getNote();
		this.daysFromUpload = bdcRdShowMusic.getDaysFromUpload();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public MusicType getMusicType() {
		return musicType;
	}

	public void setMusicType(MusicType musicType) {
		this.musicType = musicType;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getComposer() {
		return composer;
	}

	public void setComposer(String composer) {
		this.composer = composer;
	}

	public String getPerformer() {
		return performer;
	}

	public void setPerformer(String performer) {
		this.performer = performer;
	}

	public String getAlbum() {
		return album;
	}

	public void setAlbum(String album) {
		this.album = album;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public String getIswcCode() {
		return iswcCode;
	}

	public void setIswcCode(String iswcCode) {
		this.iswcCode = iswcCode;
	}

	public String getIsrcCode() {
		return isrcCode;
	}

	public void setIsrcCode(String isrcCode) {
		this.isrcCode = isrcCode;
	}

	public Date getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public Integer getRealDuration() {
		return realDuration;
	}

	public void setRealDuration(Integer realDuration) {
		this.realDuration = realDuration;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getReportDuration() {
		return reportDuration;
	}

	public void setReportDuration(String reportDuration) {
		this.reportDuration = reportDuration;
	}

	public String getReportBeginTime() {
		return reportBeginTime;
	}

	public void setReportBeginTime(String reportBeginTime) {
		this.reportBeginTime = reportBeginTime;
	}

	public String getReportMusicType() {
		return reportMusicType;
	}

	public void setReportMusicType(String reportMusicType) {
		this.reportMusicType = reportMusicType;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getFirstComposer() {
		return firstComposer;
	}

	public void setFirstComposer(String firstComposer) {
		this.firstComposer = firstComposer;
	}

	public String getSecondComposer() {
		return secondComposer;
	}

	public void setSecondComposer(String secondComposer) {
		this.secondComposer = secondComposer;
	}

	public String getReportRealDuration() {
		return reportRealDuration;
	}

	public void setReportRealDuration(String reportRealDuration) {
		this.reportRealDuration = reportRealDuration;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Integer getDaysFromUpload() {
		return daysFromUpload;
	}

	public void setDaysFromUpload(Integer daysFromUpload) {
		this.daysFromUpload = daysFromUpload;
	}

}
