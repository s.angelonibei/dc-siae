package com.alkemytech.sophia.broadcasting.service.interfaces;

import com.alkemytech.sophia.broadcasting.dto.BdcConfigDTO;
import com.alkemytech.sophia.broadcasting.dto.SearchBroadcasterConfigDTO;
import com.alkemytech.sophia.broadcasting.model.BdcBroadcasterConfig;
import com.alkemytech.sophia.broadcasting.model.BdcBroadcasterConfigTemplate;

import java.util.Date;
import java.util.List;

/**
 * Created by Alessandro Russo on 30/11/2017.
 */
public interface BroadcasterConfigService {

    public BdcBroadcasterConfig findConfig(Integer id);

    public List<BdcBroadcasterConfigTemplate> getBdcBroadcastingTemplate(String tipoEmittente);

    public List<BdcBroadcasterConfig> findAll();

    public List<BdcBroadcasterConfig> findBroadcasterActivConfiguration(Integer idEmittente);

    public List<BdcConfigDTO> findAllActive(SearchBroadcasterConfigDTO dto);

    public BdcConfigDTO findActive(SearchBroadcasterConfigDTO dto);

    public Integer updateConfig(Integer idConfig, Date validFrom, Date validTo, String modifyUser, BdcBroadcasterConfigTemplate idConfiguration);

    public Integer createConfig(Integer idBroadcaster, Integer idCanale, Date validFrom, Date validTo, BdcBroadcasterConfigTemplate idConfiguration ,String modifyUser);

    public Integer deleteConfig(Integer id, String userModifier);
}
