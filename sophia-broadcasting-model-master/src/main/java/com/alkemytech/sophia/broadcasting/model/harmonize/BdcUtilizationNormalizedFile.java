package com.alkemytech.sophia.broadcasting.model.harmonize;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the BDC_UTILIZATION_NORMALIZED_FILE database table.
 * 
 */
@Entity(name="UtilizationNormalizedFile")
@Table(name="BDC_UTILIZATION_NORMALIZED_FILE")
@NamedQuery(name="UtilizationNormalizedFile.findAll", query="SELECT b FROM UtilizationNormalizedFile b")
public class BdcUtilizationNormalizedFile implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID_BDC_UTILIZATION_NORMALIZED_FILE", unique=true, nullable=false)
	private Integer idBdcUtilizationNormalizedFile;

	@Column(name="ID_NORMALIZED_FILE", nullable=false)
	private java.math.BigInteger idNormalizedFile;

	//bi-directional many-to-one association to BdcUtilizationFile
	@JsonBackReference
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_UTILIZATION_FILE", nullable=false)
	private BdcUtilizationFile bdcUtilizationFile;

	public BdcUtilizationNormalizedFile() {
	}

	public Integer getIdBdcUtilizationNormalizedFile() {
		return this.idBdcUtilizationNormalizedFile;
	}

	public void setIdBdcUtilizationNormalizedFile(Integer idBdcUtilizationNormalizedFile) {
		this.idBdcUtilizationNormalizedFile = idBdcUtilizationNormalizedFile;
	}

	public java.math.BigInteger getIdNormalizedFile() {
		return this.idNormalizedFile;
	}

	public void setIdNormalizedFile(java.math.BigInteger idNormalizedFile) {
		this.idNormalizedFile = idNormalizedFile;
	}

	public BdcUtilizationFile getBdcUtilizationFile() {
		return this.bdcUtilizationFile;
	}

	public void setBdcUtilizationFile(BdcUtilizationFile bdcUtilizationFile) {
		this.bdcUtilizationFile = bdcUtilizationFile;
	}

}