package com.alkemytech.sophia.broadcasting.service;

import com.alkemytech.sophia.broadcasting.consts.Consts;
import com.alkemytech.sophia.broadcasting.dto.harmonization.FieldErrorDTO;
import com.alkemytech.sophia.broadcasting.model.BdcCanali;
import com.alkemytech.sophia.broadcasting.model.BdcNormalizedFile;
import com.alkemytech.sophia.broadcasting.model.TipoBroadcaster;
import com.alkemytech.sophia.broadcasting.model.utilizations.*;
import com.alkemytech.sophia.broadcasting.service.interfaces.BdcCanaliService;
import com.alkemytech.sophia.broadcasting.service.interfaces.BdcIndicatoreDTOService;
import com.alkemytech.sophia.broadcasting.service.interfaces.NormalizedFileService;
import com.alkemytech.sophia.broadcasting.service.interfaces.UtilizationService;
import com.alkemytech.sophia.broadcasting.utils.Utilities;
import com.alkemytech.sophia.common.Constants;
import com.amazonaws.util.CollectionUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by Alessandro Russo on 15/12/2017.
 */
@Singleton
public class UtilizationServiceImpl implements UtilizationService {

    private final static String MUSICT_TYPE_PROTAGONISTA = "PROTAGONISTA";
    private final static String MUSICT_TYPE_FILM = "COMMENTO";
    private final static String MUSICT_TYPE_IDENTIFICATIVA = "IDENTIFICATIVA";
    private final static String MUSICT_TYPE_SOTTOFONDO = "SOTTOFONDO";
    private final static String MSG_MUSIC_ALREADY_EXISTS = "Opera musicale gia' presente.";
    private final static String MSG_SHOW_WITHOUT_MUSIC = "SHOW TV senza opere musicali";
    private final static String MSG_RADIO_WITHOUT_MUSIC = "Utilizzazione Radio senza opere musicali";
    private final static String MSG_OVERLAPPING = "Utilizzazione in sovrapposizione";
    private final static String MSG_SAME_SHOW = "Utilizzazione in conflitto";
    private final static String MSG_CANALE_NON_ATTIVO = "Canale non attivo";
    private final static String MSG_UTILIZZAZIONE_DUPLICATA = "Opera musicale gia' presente";
    private final Provider<EntityManager> provider;
    private final BdcCanaliService bdcCanaliService;
    private final NormalizedFileService normalizedFileService;
    private final BdcIndicatoreDTOService bdcIndicatoreService;
    Logger logger = LoggerFactory.getLogger(getClass());

    @Inject
    public UtilizationServiceImpl(Provider<EntityManager> provider, NormalizedFileService normalizedFileService,
                                  BdcCanaliService bdcCanaliService, BdcIndicatoreDTOService bdcIndicatoreService) {
        this.provider = provider;
        this.normalizedFileService = normalizedFileService;
        this.bdcCanaliService = bdcCanaliService;
        this.bdcIndicatoreService = bdcIndicatoreService;
    }

    /**
     * @param rows
     * @param normalizedFile
     * @return
     */
    @Override
    public Map<Integer, Set<String>> saveTvEntityList(List<Object> rows, BdcNormalizedFile normalizedFile) {
        EntityManager em = provider.get();
        em.getTransaction().begin();
        em.createNativeQuery("SET SESSION sql_mode = ''").executeUpdate();
        String repertorio = normalizedFile.getFirstOriginalReport().getRepertorio();
        Map<String, Date> dateCanaliRepertori = new HashMap<>(); //lista date validità canali per repertorio
        List<Object> discardedRows = new ArrayList<>(); //lista delle utilizzazioni da scartare
        Map<Integer, Set<String>> ret = new HashMap<>(); //mappa <idCanale, List<Anno-Mese>>
        Map<Integer, Map<String, Map<String, List<BdcTvShowSchedule>>>> existingMap = new HashMap<>(); //utilizzazioni già presenti
        if (normalizedFile.getFirstOriginalReport().getTipoUpload().equals(Consts.SINGLE_UPLOAD)) {
            ret.put(normalizedFile.getFirstOriginalReport().getCanale().getId(), new TreeSet<String>());
            String yearMonth = String.format("%d-%d", normalizedFile.getFirstOriginalReport().getAnno(), normalizedFile.getFirstOriginalReport().getMese());
            ret.get(normalizedFile.getFirstOriginalReport().getCanale().getId()).add(yearMonth);
        }

        // ordinamento



        try {
            for (Object currentRow : rows) {

                if (containsErrors((Map<String, Object>) currentRow)) {
                    logger.debug("IN IF CONTAINS ERRORS");
                    try {
                        logger.debug("try discardedrows");
                        discardedRows.addAll(verifyNormalizedErrTvEntity(normalizedFileService.fromMapToErrTvShow((Map<String, Object>) currentRow, normalizedFile), repertorio, dateCanaliRepertori));
                    } catch (Exception e) {
                        logger.error("RECORD EXCEPTION {}", ExceptionUtils.getMessage(e));
                        BdcErrTvShowSchedule errShow = normalizedFileService.fromMapToErrTvShow((Map<String, Object>) currentRow, normalizedFile);
                        errShow.setGlobalError(e.getMessage());
                        discardedRows.add(errShow);
                    }
                } else {
                    logger.debug("IN ELSE BEFORE CORRECTSHOW");
                    BdcTvShowSchedule correctShow = normalizedFileService.fromMapToTvShow((Map<String, Object>) currentRow, normalizedFile);
                    logger.debug("CORRECTSHOW ESEGUITO");
                    try {
                        for (BdcTvShowSchedule showToPersist : verifyNormalizedTvEntity(correctShow, existingMap, repertorio, dateCanaliRepertori)) {
                            logger.debug("FOR SHOWTOPERSIST");
                            //todo verificare ad ogni passaggio cosa arriva
                            showToPersist.setModifyDate(showToPersist.getId() != null ? new Date() : null);
                            em.persist(showToPersist);

                            String yearMonth = String.format("%d-%d", showToPersist.getScheduleYear(), showToPersist.getScheduleMonth());
                            String day = Integer.toString(Utilities.getDateField(showToPersist.getBeginTime(), Calendar.DAY_OF_MONTH));
                            updateKpiToEvaluate(ret, showToPersist.getCanale().getId(), yearMonth);
                            if (showToPersist.getModifyDate() == null) {
                                logger.debug("BEFORE updateTvExistingMap");
                                updateTvExistingMap(yearMonth, day, showToPersist, existingMap);
                            }
                        }
                    } catch (Exception e) {
                        logger.error("RECORD EXCEPTION {}", ExceptionUtils.getMessage(e));
                        BdcErrTvShowSchedule errShow = normalizedFileService.fromMapToErrTvShow((Map<String, Object>) currentRow, normalizedFile);
                        errShow.setGlobalError(e.getMessage());
                        discardedRows.add(errShow);
                    }
                }
            }
            //PERSIST SHOW WITH ERROR
            for (Object obj : discardedRows) {
                logger.debug("FOR DISCAREDROWS");
                BdcErrTvShowSchedule errShow = (BdcErrTvShowSchedule) obj;
                if (errShow.getCanale() != null) {
                    if (errShow.getSchedulYear() != null && errShow.getScheduleMonth() != null) {
                        String yearMonth = String.format("%d-%d", errShow.getSchedulYear(), errShow.getScheduleMonth());
                        updateKpiToEvaluate(ret, errShow.getCanale().getId(), yearMonth);
                    }
                }
                em.merge(errShow);
            }
            em.flush();
            em.getTransaction().commit();
            return ret;
        } catch (Exception e) {
            logger.error("UNEXPECTED EXCEPTION {}", ExceptionUtils.getMessage(e));
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
            return null;
        }
    }



    /**
     * @param rows
     * @param normalizedFile
     * @return
     */
    @Override
    public Map<Integer, Set<String>> saveRadioEntityList(List<Object> rows, BdcNormalizedFile normalizedFile, Integer overlapLimit) {
        EntityManager em = provider.get();
        em.getTransaction().begin();
        em.createNativeQuery("SET SESSION sql_mode = ''").executeUpdate();
        String repertorio = normalizedFile.getFirstOriginalReport().getRepertorio();
        Map<String, Date> dateCanaliRepertori = new HashMap<>(); //lista date validità canali per repertorio
        List<Object> discardedRows = new ArrayList<>(); //lista delle utilizzazioni da scartare
        Map<Integer, Set<String>> ret = new HashMap<>(); //mappa <idCanale, List<Anno-Mese>>
        Map<Integer, Map<String, Map<String, List<BdcRdShowSchedule>>>> existingMap = new HashMap<>(); //utilizzazioni già presenti
        if (normalizedFile.getFirstOriginalReport().getTipoUpload().equals(Consts.SINGLE_UPLOAD)) {
            ret.put(normalizedFile.getFirstOriginalReport().getCanale().getId(), new TreeSet<String>());
            String yearMonth = String.format("%d-%d", normalizedFile.getFirstOriginalReport().getAnno(), normalizedFile.getFirstOriginalReport().getMese());
            ret.get(normalizedFile.getFirstOriginalReport().getCanale().getId()).add(yearMonth);
        }





        try {
            for (Object currentRow : rows) {
                if (containsErrors((Map<String, Object>) currentRow)) {
                    try {
                        discardedRows.addAll(verifyNormalizedErrRdEntity(normalizedFileService.fromMapToErrRdShow((Map<String, Object>) currentRow, normalizedFile), repertorio, dateCanaliRepertori));
                    } catch (Exception e) {
                        logger.error("RECORD EXCEPTION {}", ExceptionUtils.getMessage(e));
                        BdcErrRdShowSchedule errShow = normalizedFileService.fromMapToErrRdShow((Map<String, Object>) currentRow, normalizedFile);
                        errShow.setGlobalError(e.getMessage());
                        discardedRows.add(errShow);
                    }
                } else {
                    BdcRdShowSchedule correctShow = normalizedFileService.fromMapToRdShow((Map<String, Object>) currentRow, normalizedFile);
                    try {
                        for (BdcRdShowSchedule showToPersist : verifyNormalizedRdEntity(correctShow, existingMap, repertorio, dateCanaliRepertori, overlapLimit)) {
                            showToPersist.setModifyDate(showToPersist.getId() != null ? new Date() : null);
                            em.persist(showToPersist);

                            String yearMonth = String.format("%d-%d", showToPersist.getSchedulYear(), showToPersist.getScheduleMonth());
                            String day = Integer.toString(Utilities.getDateField(showToPersist.getDate(), Calendar.DAY_OF_MONTH));
                            updateKpiToEvaluate(ret, showToPersist.getCanale().getId(), yearMonth);
                            if (showToPersist.getModifyDate() == null) {
                                updateRdExistingMap(yearMonth, day, showToPersist, existingMap);
                            }
                        }
                    } catch (Exception e) {
                        BdcErrRdShowSchedule errShow = normalizedFileService.fromMapToErrRdShow((Map<String, Object>) currentRow, normalizedFile);
                        errShow.setGlobalError(e.getMessage());
                        discardedRows.add(errShow);
                    }
                }
            }
            //PERSIST SHOW WITH ERROR
            for (Object obj : discardedRows) {
                BdcErrRdShowSchedule errShow = (BdcErrRdShowSchedule) obj;
                if (errShow.getCanale() != null) {
                    if (errShow.getSchedulYear() != null && errShow.getScheduleMonth() != null) {
                        String yearMonth = String.format("%d-%d", errShow.getSchedulYear(), errShow.getScheduleMonth());
                        updateKpiToEvaluate(ret, errShow.getCanale().getId(), yearMonth);
                    }
                }
                em.merge(errShow);
            }
            em.flush();
            em.getTransaction().commit();
            return ret;
        } catch (Exception e) {
            logger.error(e.getMessage());
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
            return null;
        }
    }

    private void updateKpiToEvaluate(Map<Integer, Set<String>> kpiToEvaluate, Integer idChannel, String yearMonth) {
        if (idChannel != null) {
            if (!kpiToEvaluate.containsKey(idChannel)) {
                kpiToEvaluate.put(idChannel, new TreeSet<String>());
            }
            kpiToEvaluate.get(idChannel).add(yearMonth);
        }
    }

    private void updateTvExistingMap(String yearMonth, String day, BdcTvShowSchedule showToPersist, Map<Integer, Map<String, Map<String, List<BdcTvShowSchedule>>>> existingMap) {
        if (!existingMap.containsKey(showToPersist.getCanale().getId())) {
            existingMap.put(showToPersist.getCanale().getId(), new HashMap<String, Map<String, List<BdcTvShowSchedule>>>());
        }
        if (!existingMap.get(showToPersist.getCanale().getId()).containsKey(yearMonth)) {
            existingMap.get(showToPersist.getCanale().getId()).put(yearMonth, new HashMap<String, List<BdcTvShowSchedule>>());
        }
        if (!existingMap.get(showToPersist.getCanale().getId()).get(yearMonth).containsKey(day)) {
            existingMap.get(showToPersist.getCanale().getId()).get(yearMonth).put(day, new ArrayList<BdcTvShowSchedule>());
        }
        List<BdcTvShowSchedule> updatedExistingShows = existingMap.get(showToPersist.getCanale().getId()).get(yearMonth).get(day);
        int index = 0;
        for (int i = 0; i < updatedExistingShows.size(); i++) {
            BdcTvShowSchedule existingShow = updatedExistingShows.get(i);
            if (showToPersist.getBeginTime().before(existingShow.getBeginTime())) {
                break;
            }
            index++;
        }
        updatedExistingShows.add(index, showToPersist);
    }

    private void updateRdExistingMap(String yearMonth, String day, BdcRdShowSchedule showToPersist, Map<Integer, Map<String, Map<String, List<BdcRdShowSchedule>>>> existingMap) {
        if (!existingMap.containsKey(showToPersist.getCanale().getId())) {
            existingMap.put(showToPersist.getCanale().getId(), new HashMap<String, Map<String, List<BdcRdShowSchedule>>>());
        }
        if (!existingMap.get(showToPersist.getCanale().getId()).containsKey(yearMonth)) {
            existingMap.get(showToPersist.getCanale().getId()).put(yearMonth, new HashMap<String, List<BdcRdShowSchedule>>());
        }
        if (!existingMap.get(showToPersist.getCanale().getId()).get(yearMonth).containsKey(day)) {
            existingMap.get(showToPersist.getCanale().getId()).get(yearMonth).put(day, new ArrayList<BdcRdShowSchedule>());
        }
        List<BdcRdShowSchedule> updatedExistingShows = existingMap.get(showToPersist.getCanale().getId()).get(yearMonth).get(day);
        int index = 0;
        for (int i = 0; i < updatedExistingShows.size(); i++) {
            BdcRdShowSchedule existingShow = updatedExistingShows.get(i);
            if (showToPersist.getBeginTime() != null && existingShow.getBeginTime() != null) {
                if (showToPersist.getBeginTime().before(existingShow.getBeginTime())) {
                    break;
                }
            }
            index++;
        }
        updatedExistingShows.add(index, showToPersist);
    }

    private boolean containsErrors(Map<String, Object> currentRow) {
        if (currentRow.containsKey(Constants.ERRORS) && StringUtils.isNotEmpty((String) currentRow.get(Constants.ERRORS))) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                Map<String, List<FieldErrorDTO>> errorList = mapper.readValue((String) currentRow.get(Constants.ERRORS), Map.class);
                for (String key : errorList.keySet()) {
                    if (!CollectionUtils.isNullOrEmpty(errorList.get(key))) {
                        return true;
                    }
                }
            } catch (IOException e) {
                logger.error(e.getMessage());
                return true;
            }
        }
        return false;
    }
/*
    private void printExistingMap(Map<Integer, Map<String, Map<String, List<BdcTvShowSchedule>>>> existingMap) {

        Set<Integer> l1 = existingMap.keySet();
        Map<String, Map<String, List<BdcTvShowSchedule>>> m1;
        Map<String, List<BdcTvShowSchedule>> m2;
        List<BdcTvShowSchedule> list;

        logger.debug("++++Print Existing Map+++");
        for (Integer i : l1) {
            Set<String> l2 = existingMap.get(i).keySet();
            m1 = existingMap.get(i);
            for (String s1 : l2) {
                Set<String> l3 = m1.get(s1).keySet();
                m2 = m1.get(s1);
                for (String s2 : l3) {
                    Integer size = m2.get(s2).size();
                    list = m2.get(s2);
                    for (BdcTvShowSchedule tv : list) {
                        logger.debug("keys parent: " + i + "->" + s1 + "->" + s2 + ". Listsize: " + size);
                        logger.debug("element: " + tv.toString());

                    }
                }
            }
        }
        logger.debug("++++END Print Existing Map+++");

    }
*/
    //VALIDATION
    private Set<BdcTvShowSchedule> verifyNormalizedTvEntity(BdcTvShowSchedule show, Map<Integer, Map<String, Map<String, List<BdcTvShowSchedule>>>> existingMap, String repertorio, Map<String, Date> dateCanaliRepertori) throws Exception {
        //logger.info("verifyNormalizedTvEntity mapSize: " + existingMap.size());

        if (show.isShow() && CollectionUtils.isNullOrEmpty(show.getTvShowMusicList())) {
            throw new Exception(MSG_SHOW_WITHOUT_MUSIC);
        }

        if (!dateCanaliRepertori.containsKey("dataInizioValid|" + show.getCanale().getId() + "|" + repertorio) && !dateCanaliRepertori.containsKey("dataFineValid|" + show.getCanale().getId() + "|" + repertorio)) {
            dateCanaliRepertori.putAll(dateCanaleRepertorio(show.getCanale().getId(), repertorio));
        }

        evaluteValidityDateTVChannel(show, repertorio, dateCanaliRepertori);

        Set<BdcTvShowSchedule> showsToPersist = new HashSet<>();
        //calculate map keys
        Integer channel = show.getCanale().getId();
        String yearMonth = String.format("%d-%d", show.getScheduleYear(), show.getScheduleMonth());
        String day = Integer.toString(Utilities.getDateField(show.getBeginTime(), Calendar.DAY_OF_MONTH));
        //load shows from DB
        loadTvShows(channel, show.getScheduleYear(), show.getScheduleMonth(), existingMap);
        //logger.info("loadTvShows() mapSize: " + existingMap.size());

        loadTvNearMonth(show, existingMap, true);
        //logger.info("loadTvNearMonth() mapSize: " + existingMap.size());

        loadTvNearMonth(show, existingMap, false);
        //logger.info("loadTvNearMonth() mapSize: " + existingMap.size());

        //Verify contemporary record

        List<BdcTvShowSchedule> sameShows = getSameShow(channel, yearMonth, day, show, existingMap);
        //logger.info("getSameShow() mapSize: " + existingMap.size());
        //printExistingMap(existingMap);

        if (CollectionUtils.isNullOrEmpty(sameShows)) {
            //Verify overlapping record
            List<BdcTvShowSchedule> overlappingShowList = evaluateOverlappingShow(channel, yearMonth, day, show, existingMap);
            showsToPersist.add(show);
            if (!CollectionUtils.isNullOrEmpty(overlappingShowList)) {
                showsToPersist.addAll(overlappingShowList);
            }
        }

        return showsToPersist;
    }

    private Map<String, Date> dateCanaleRepertorio(Integer idChannel, String repertorio) {

        Map<String, Date> dateCanalStringDateMap = new HashMap<>();

        BdcCanali canale = bdcCanaliService.findById(idChannel);

        if (canale.getDataFineValid() != null) {
            dateCanalStringDateMap.put("dataFineValid|" + canale.getId() + "|" + repertorio, canale.getDataFineValid());
        }
        if (canale.getDataInizioValid() != null) {
            dateCanalStringDateMap.put("dataInizioValid|" + canale.getId() + "|" + repertorio, canale.getDataInizioValid());
        }
        return dateCanalStringDateMap;
    }

    private Set<BdcErrTvShowSchedule> verifyNormalizedErrTvEntity(BdcErrTvShowSchedule show, String repertorio, Map<String, Date> dateCanaliRepertori) throws Exception {
        Set<BdcErrTvShowSchedule> showsErrToPersist = new HashSet<>();
        if (show.getCanale() != null && show.getBeginTime() != null) {
            if (!dateCanaliRepertori.containsKey("dataInizioValid|" + repertorio + "|" + show.getCanale().getId()) && !dateCanaliRepertori.containsKey("dataFineValid|" + repertorio + "|" + show.getCanale().getId())) {
                dateCanaliRepertori.putAll(dateCanaleRepertorio(show.getCanale().getId(), repertorio));
            }
            evaluteValidityDateTVChannelErr(show, repertorio, dateCanaliRepertori);
        }
        showsErrToPersist.add(show);
        return showsErrToPersist;
    }

    private Set<BdcRdShowSchedule> verifyNormalizedRdEntity(BdcRdShowSchedule show, Map<Integer, Map<String, Map<String, List<BdcRdShowSchedule>>>> existingMap, String repertorio, Map<String, Date> dateCanaliRepertori, Integer overlapLimit) throws Exception {
        if (CollectionUtils.isNullOrEmpty(show.getRdShowMusicList())) {
            throw new Exception(MSG_RADIO_WITHOUT_MUSIC);
        }

        if (!dateCanaliRepertori.containsKey("dataInizioValid|" + show.getCanale().getId() + "|" + repertorio) && !dateCanaliRepertori.containsKey("dataFineValid|" + show.getCanale().getId() + "|" + repertorio)) {
            dateCanaliRepertori.putAll(dateCanaleRepertorio(show.getCanale().getId(), repertorio));
        }

        evaluteValidityDateRDChannel(show, repertorio, dateCanaliRepertori);

        Set<BdcRdShowSchedule> showsToPersist = new HashSet<>();
        //calculate map keys
        Integer channel = show.getCanale().getId();
        String yearMonth = String.format("%d-%d", show.getSchedulYear(), show.getScheduleMonth());
        Date recordDate = show.getDate();
        String day = recordDate != null ? new Integer(Utilities.getDateField(recordDate, Calendar.DAY_OF_MONTH)).toString() : "0";
        //load shows from DB
        loadRadioShows(channel, show.getSchedulYear(), show.getScheduleMonth(), existingMap);
        loadRadioNearMonth(show, existingMap, true);
        loadRadioNearMonth(show, existingMap, false);

        BdcRdShowSchedule sameShow = getSameRadioShow(channel, yearMonth, day, show, existingMap);
        if (sameShow != null) {
            showsToPersist.add(sameShow);
        } else {
            //Verify overlapping record
            List<BdcRdShowSchedule> overlappingShowList = evaluateOverlappingRadioShow(channel, yearMonth, day, show, existingMap, overlapLimit);
            showsToPersist.add(show);
            if (!CollectionUtils.isNullOrEmpty(overlappingShowList)) {
                showsToPersist.addAll(overlappingShowList);
            }
        }

        return showsToPersist;
    }

    private Set<BdcErrRdShowSchedule> verifyNormalizedErrRdEntity(BdcErrRdShowSchedule show, String repertorio, Map<String, Date> dateCanaliRepertori) throws Exception {
        Set<BdcErrRdShowSchedule> showsErrToPersist = new HashSet<>();
        if (show.getCanale() != null && show.getBeginTime() != null) {
            if (!dateCanaliRepertori.containsKey("dataInizioValid|" + repertorio + "|" + show.getCanale().getId()) && !dateCanaliRepertori.containsKey("dataFineValid|" + repertorio + "|" + show.getCanale().getId())) {
                dateCanaliRepertori.putAll(dateCanaleRepertorio(show.getCanale().getId(), repertorio));
            }
            evaluteValidityDateRDChannelErr(show, repertorio, dateCanaliRepertori);

        }
        showsErrToPersist.add(show);
        return showsErrToPersist;
    }

    private List<BdcRdShowSchedule> evaluateOverlappingRadioShow(Integer channel, String yearMonth, String day, BdcRdShowSchedule show, Map<Integer, Map<String, Map<String, List<BdcRdShowSchedule>>>> existingMap) throws Exception {
        List<BdcRdShowSchedule> overlappingShowList = new ArrayList<>();
        boolean existShowList = existingMap.containsKey(channel) && existingMap.get(channel).containsKey(yearMonth) && existingMap.get(channel).get(yearMonth).containsKey(day);
        BdcRdShowSchedule lastShowPreviousDay = getNearRadioShow(show, existingMap, true);
        BdcRdShowSchedule firstShowNextDay = getNearRadioShow(show, existingMap, false);
        List<BdcRdShowSchedule> dayShowList = existShowList ? new ArrayList<>(existingMap.get(channel).get(yearMonth).get(day)) : new ArrayList<BdcRdShowSchedule>();
        if (lastShowPreviousDay != null) {
            dayShowList.add(0, lastShowPreviousDay);
        }
        if (firstShowNextDay != null) {
            dayShowList.add(dayShowList.size(), firstShowNextDay);
        }
        for (BdcRdShowSchedule existingShow : dayShowList) {
            boolean isBothNational = StringUtils.isEmpty(show.getRegionalOffice()) && StringUtils.isEmpty(existingShow.getRegionalOffice());
            boolean isSameRegionalOffice = StringUtils.isNotEmpty(show.getRegionalOffice()) && StringUtils.isNotEmpty(existingShow.getRegionalOffice()) && show.getRegionalOffice().equalsIgnoreCase(existingShow.getRegionalOffice());
            if (isBothNational || isSameRegionalOffice) {
                Boolean startInside = null;
                Boolean endInside = null;
                Boolean completeOverlap = null;
                if (show.getCanale().isSpecialRadio()) { // RADIO RAI
                    startInside = show.getBeginTime().after(existingShow.getBeginTime()) && show.getBeginTime().before(existingShow.getEndTime());
                    endInside = show.getEndTime().after(existingShow.getBeginTime()) && (show.getEndTime().before(existingShow.getEndTime()) || show.getEndTime().equals(existingShow.getEndTime()));
                    completeOverlap = show.getBeginTime().before(existingShow.getBeginTime()) && show.getEndTime().after(existingShow.getEndTime());
                    if (startInside || endInside || completeOverlap) {
                        throw new Exception(MSG_OVERLAPPING);
                    }
                } else {
                    for (BdcRdShowMusic existingMusic : existingShow.getRdShowMusicList()) {
                        Date beginCurrent = existingMusic.getBeginTime();
                        Date endCurrent = DateUtils.addSeconds(beginCurrent, existingMusic.getDuration());
                        for (BdcRdShowMusic newMusic : show.getRdShowMusicList()) {
                            Date beginNew = newMusic.getBeginTime();
                            Date endNew = DateUtils.addSeconds(beginNew, newMusic.getDuration());

                            startInside = (beginNew.after(beginCurrent) && beginNew.before(endCurrent)) || beginNew.equals(beginCurrent);
                            endInside = (endNew.after(beginCurrent) && endNew.before(endCurrent)) || endNew.equals(endCurrent);
                            completeOverlap = beginNew.before(beginCurrent) && endNew.after(endCurrent);
                            if (startInside || endInside || completeOverlap) {
                                throw new Exception(MSG_OVERLAPPING);
                            }
                        }
                    }
                }
            }
        }
        return overlappingShowList;
    }

    private List<BdcRdShowSchedule> evaluateOverlappingRadioShow(Integer channel, String yearMonth, String day, BdcRdShowSchedule show, Map<Integer, Map<String, Map<String, List<BdcRdShowSchedule>>>> existingMap, Integer overlapLimit) throws Exception {
/*
        if( overlapLimit >0 ){
            logger.debug("overlapLimit="+overlapLimit);
        }
*/

        List<BdcRdShowSchedule> overlappingShowList = new ArrayList<>();
        boolean existShowList = existingMap.containsKey(channel) && existingMap.get(channel).containsKey(yearMonth) && existingMap.get(channel).get(yearMonth).containsKey(day);
        BdcRdShowSchedule lastShowPreviousDay = getNearRadioShow(show, existingMap, true);
        BdcRdShowSchedule firstShowNextDay = getNearRadioShow(show, existingMap, false);
        List<BdcRdShowSchedule> dayShowList = existShowList ? new ArrayList<>(existingMap.get(channel).get(yearMonth).get(day)) : new ArrayList<BdcRdShowSchedule>();
        if (lastShowPreviousDay != null) {
            dayShowList.add(0, lastShowPreviousDay);
        }
        if (firstShowNextDay != null) {
            dayShowList.add(dayShowList.size(), firstShowNextDay);
        }
        for (BdcRdShowSchedule existingShow : dayShowList) {
            boolean isBothNational = StringUtils.isEmpty(show.getRegionalOffice()) && StringUtils.isEmpty(existingShow.getRegionalOffice());
            boolean isSameRegionalOffice = StringUtils.isNotEmpty(show.getRegionalOffice()) && StringUtils.isNotEmpty(existingShow.getRegionalOffice()) && show.getRegionalOffice().equalsIgnoreCase(existingShow.getRegionalOffice());
            if (isBothNational || isSameRegionalOffice) {
                Boolean startInside = null;
                Boolean endInside = null;
                Boolean completeOverlap = null;


                if (show.getCanale().isSpecialRadio()) { // RADIO RAI
                    startInside = show.getBeginTime().after(existingShow.getBeginTime()) && show.getBeginTime().before(existingShow.getEndTime());
                    endInside = show.getEndTime().after(existingShow.getBeginTime()) && (show.getEndTime().before(existingShow.getEndTime()) || show.getEndTime().equals(existingShow.getEndTime()));
                    completeOverlap = show.getBeginTime().before(existingShow.getBeginTime()) && show.getEndTime().after(existingShow.getEndTime());
                    if (startInside || endInside || completeOverlap) {
                        throw new Exception(MSG_OVERLAPPING);
                    }
                } else {
                    for (BdcRdShowMusic existingMusic : existingShow.getRdShowMusicList()) {
                        Date beginCurrent = existingMusic.getBeginTime();
                        Date endCurrent = DateUtils.addSeconds(beginCurrent, existingMusic.getDuration());
                        for (BdcRdShowMusic newMusic : show.getRdShowMusicList()) {
                            Date beginNew = newMusic.getBeginTime();
                            Date endNew = DateUtils.addSeconds(beginNew, newMusic.getDuration());


                            startInside = (beginNew.after(beginCurrent) && beginNew.before(endCurrent)) || beginNew.equals(beginCurrent);
                            endInside = (endNew.after(beginCurrent) && endNew.before(endCurrent)) || endNew.equals(endCurrent);
                            completeOverlap = beginNew.before(beginCurrent) && endNew.after(endCurrent);
                            if (startInside || endInside || completeOverlap) {
                                newMusic.setAlert(MSG_OVERLAPPING);
                                if (valutaSovrapposizione(beginNew, endNew, beginCurrent, endCurrent, overlapLimit))
                                    throw new Exception(MSG_OVERLAPPING);
                            }

                        }
                    }
                }
            }
        }
        return overlappingShowList;
    }

    private void evaluteValidityDateTVChannel(BdcTvShowSchedule show, String repertorio, Map<String, Date> dateCanaliRepertori) throws Exception {
        if (show.getCanale().getDataFineValid() != null) {
            if (show.getBeginTime().after(dateCanaliRepertori.get("dataFineValid|" + show.getCanale().getId() + "|" + repertorio))) {
                throw new Exception(MSG_CANALE_NON_ATTIVO);
            }
        }
        if (show.getCanale().getDataInizioValid() != null) {
            if ((show.getBeginTime().before(dateCanaliRepertori.get("dataInizioValid|" + show.getCanale().getId() + "|" + repertorio)))) {
                throw new Exception(MSG_CANALE_NON_ATTIVO);
            }
        }
    }

    private void evaluteValidityDateTVChannelErr(BdcErrTvShowSchedule show, String repertorio, Map<String, Date> dateCanaliRepertori) throws Exception {
        if (show.getCanale().getDataFineValid() != null) {
            if (show.getBeginTime().after(dateCanaliRepertori.get("dataFineValid|" + show.getCanale().getId() + "|" + repertorio))) {
                throw new Exception(MSG_CANALE_NON_ATTIVO);
            }
        }
        if (show.getCanale().getDataInizioValid() != null) {
            if ((show.getBeginTime().before(dateCanaliRepertori.get("dataInizioValid|" + show.getCanale().getId() + "|" + repertorio)))) {
                throw new Exception(MSG_CANALE_NON_ATTIVO);
            }
        }
    }

    private void evaluteValidityDateRDChannel(BdcRdShowSchedule show, String repertorio, Map<String, Date> dateCanaliRepertori) throws Exception {
        if (show.getCanale().getDataFineValid() != null) {
            if (show.getBeginTime().after(dateCanaliRepertori.get("dataFineValid|" + show.getCanale().getId() + "|" + repertorio))) {
                throw new Exception(MSG_CANALE_NON_ATTIVO);
            }
        }
        if (show.getCanale().getDataInizioValid() != null) {
            if ((show.getBeginTime().before(dateCanaliRepertori.get("dataInizioValid|" + show.getCanale().getId() + "|" + repertorio)))) {
                throw new Exception(MSG_CANALE_NON_ATTIVO);
            }
        }
    }

    private void evaluteValidityDateRDChannelErr(BdcErrRdShowSchedule show, String repertorio, Map<String, Date> dateCanaliRepertori) throws Exception {
        if (show.getCanale().getDataFineValid() != null) {
            if (show.getBeginTime().after(dateCanaliRepertori.get("dataFineValid|" + show.getCanale().getId() + "|" + repertorio))) {
                throw new Exception(MSG_CANALE_NON_ATTIVO);
            }
        }
        if (show.getCanale().getDataInizioValid() != null) {
            if ((show.getBeginTime().before(dateCanaliRepertori.get("dataInizioValid|" + show.getCanale().getId() + "|" + repertorio)))) {
                throw new Exception(MSG_CANALE_NON_ATTIVO);
            }
        }
    }

    private List<BdcTvShowSchedule> evaluateOverlappingShow(Integer channel, String yearMonth, String day, BdcTvShowSchedule show, Map<Integer, Map<String, Map<String, List<BdcTvShowSchedule>>>> existingMap) throws Exception {

        List<BdcTvShowSchedule> overlappingShowList = new ArrayList<>();
        boolean existShowList = existingMap.containsKey(channel) && existingMap.get(channel).containsKey(yearMonth) && existingMap.get(channel).get(yearMonth).containsKey(day);
        BdcTvShowSchedule lastShowPreviousDay = getNearShow(show, existingMap, true);
        BdcTvShowSchedule firstShowNextDay = getNearShow(show, existingMap, false);
        List<BdcTvShowSchedule> dayShowList = existShowList ? new ArrayList<>(existingMap.get(channel).get(yearMonth).get(day)) : new ArrayList<BdcTvShowSchedule>();

        if (lastShowPreviousDay != null) {
            dayShowList.add(0, lastShowPreviousDay);
        }
        if (firstShowNextDay != null) {
            dayShowList.add(dayShowList.size(), firstShowNextDay);
        }
        boolean isContenuto = false;

        //logger.debug("Show: title: " + show.getTitle() + " - BE: " + show.getBeginTime() + " - EE: " + show.getEndTime() + " - showType: " + show.getShowType() + " - MusicType :" + show.getTvShowMusicList().get(0).getMusicType() + " - isFilm :" + show.isFilm() + " - isShow: " + show.isShow() + " -isContenuto: " + show.isFlagContenuti() );

        // serve per controllare se nell'existingShow ho un show contenitore.
        // nn va fatto nel  successivo ciclo in quanto il file di input non è ordinato per cat.uso 4
        // cosi facendo mi setto a priori se lo show ha un contenitore per evitare di fare il controllo di scarto di sopravrapposizione per cat.4
        // che è valida solo per gli audiovisivi cat 4 uso singolo (non contenitore).

        for (BdcTvShowSchedule existingShow : dayShowList) {
            boolean dupInside = show.getTitle().equals(existingShow.getTitle()) && show.getBeginTime().equals(existingShow.getBeginTime()) && show.getEndTime().equals(existingShow.getEndTime());
            if (show.isFilm() && existingShow.isShow() && dupInside && existingShow.isFlagContenuti())
            {
               // logger.debug("show con contenuti audiovisivi");
                isContenuto = true;
                break;
            }
        }

        for (BdcTvShowSchedule existingShow : dayShowList) {

            boolean isBothNational = StringUtils.isEmpty(show.getRegionalOffice()) && StringUtils.isEmpty(existingShow.getRegionalOffice());
            boolean isSameRegionalOffice = StringUtils.isNotEmpty(show.getRegionalOffice()) && StringUtils.isNotEmpty(existingShow.getRegionalOffice()) && show.getRegionalOffice().equalsIgnoreCase(existingShow.getRegionalOffice());
            if (isBothNational || isSameRegionalOffice) {

                boolean startInside = show.getBeginTime().after(existingShow.getBeginTime()) && show.getBeginTime().before(existingShow.getEndTime());
                boolean endInside = show.getEndTime().after(existingShow.getBeginTime()) && (show.getEndTime().before(existingShow.getEndTime()) || show.getEndTime().equals(existingShow.getEndTime()));
                boolean completeOverlap = show.getBeginTime().before(existingShow.getBeginTime()) && show.getEndTime().after(existingShow.getEndTime());

                boolean dupInside = show.getTitle().equals(existingShow.getTitle()) && show.getBeginTime().equals(existingShow.getBeginTime()) && show.getEndTime().equals(existingShow.getEndTime());


                //logger.debug("ExistingShow: title: " + existingShow.getTitle() + " - BE: " + existingShow.getBeginTime() + " - EE: " + existingShow.getEndTime() + " - showType: " + existingShow.getShowType() + " - MusicType :" + existingShow.getTvShowMusicList().get(0).getMusicType() + " - isFilm :" + existingShow.isFilm() + " - isShow: " + existingShow.isShow() + " - isContenuto: " + existingShow.isFlagContenuti()) ;
                //logger.debug("dupInside: " + dupInside);



                if (show.isFilm() && existingShow.isFilm() && dupInside && !isContenuto ) {//categoria uso 4 esclusione record duplicato tranne per i contenuti
                    logger.info("categoria uso 4 esclusione record duplicato per gli audiovisivi singoli");
                    throw new Exception(MSG_OVERLAPPING);

                } else {

                    if (show.isPubblicita()) {
                        if (startInside && endInside) {
                            if (existingShow.isPubblicita()) {
                                logger.debug("sovrapposizione per pubblicità");
                                throw new Exception(MSG_OVERLAPPING);
                            }
                            show.setOverlap(true);
                            break;
                        } else if (startInside || endInside || completeOverlap) {
                            logger.debug("sovrapposizione per complete overlap");
                            throw new Exception(MSG_OVERLAPPING);
                        }
                    } else if (!show.isPubblicita()) {
                        if (completeOverlap) {
                            if (existingShow.isPubblicita()) {
                                existingShow.setOverlap(true);
                                overlappingShowList.add(existingShow);
                                //non interrompo il ciclo per verificare altre sovrapposizioni con utilizzazioni successive
                            } else {
                                logger.debug("sovrapposizione per complete overlap e diverso da pubblicità");
                                throw new Exception(MSG_OVERLAPPING);
                            }
                        } else if (startInside || endInside) {
                            logger.debug("sovrapposizione show diverso da pubblicita e not complete overlap");
                            throw new Exception(MSG_OVERLAPPING);

                        }
                    }
                }
            }
        }
        return overlappingShowList;
    }

    private BdcTvShowSchedule getNearShow(BdcTvShowSchedule show, Map<Integer, Map<String, Map<String, List<BdcTvShowSchedule>>>> existingMap, boolean isLast) {
        Date updatedDate = isLast ? Utilities.manageDate(show.getBeginTime(), -1, Calendar.DAY_OF_MONTH) : Utilities.manageDate(show.getBeginTime(), 1, Calendar.DAY_OF_MONTH);
        String yearMonth = String.format("%d-%d", Utilities.getDateField(updatedDate, Calendar.YEAR), Utilities.getDateField(updatedDate, Calendar.MONTH) + 1);
        Integer day = Utilities.getDateField(updatedDate, Calendar.DAY_OF_MONTH);

        if (existingMap.containsKey(show.getCanale().getId()) && existingMap.get(show.getCanale().getId()).containsKey(yearMonth) && existingMap.get(show.getCanale().getId()).get(yearMonth).containsKey(day.toString())) {
            List<BdcTvShowSchedule> dailyShows = existingMap.get(show.getCanale().getId()).get(yearMonth).get(day.toString());
            if (!CollectionUtils.isNullOrEmpty(dailyShows)) {
                List<BdcTvShowSchedule> listDailyShows = new ArrayList<>(dailyShows);
                return isLast ? listDailyShows.get(dailyShows.size() - 1) : listDailyShows.get(0);
            }
        }
        return null;
    }

    private BdcRdShowSchedule getNearRadioShow(BdcRdShowSchedule show, Map<Integer, Map<String, Map<String, List<BdcRdShowSchedule>>>> existingMap, boolean isLast) {
        Date updatedDate = isLast ? Utilities.manageDate(show.getDate(), -1, Calendar.DAY_OF_MONTH) : Utilities.manageDate(show.getDate(), 1, Calendar.DAY_OF_MONTH);
        String yearMonth = String.format("%d-%d", Utilities.getDateField(updatedDate, Calendar.YEAR), Utilities.getDateField(updatedDate, Calendar.MONTH) + 1);
        Integer day = Utilities.getDateField(updatedDate, Calendar.DAY_OF_MONTH);

        if (existingMap.containsKey(show.getCanale().getId()) && existingMap.get(show.getCanale().getId()).containsKey(yearMonth) && existingMap.get(show.getCanale().getId()).get(yearMonth).containsKey(day.toString())) {
            List<BdcRdShowSchedule> dailyShows = existingMap.get(show.getCanale().getId()).get(yearMonth).get(day.toString());
            if (!CollectionUtils.isNullOrEmpty(dailyShows)) {
                List<BdcRdShowSchedule> listDailyShows = new ArrayList<>(dailyShows);
                return isLast ? listDailyShows.get(dailyShows.size() - 1) : listDailyShows.get(0);
            }
        }
        return null;
    }

    private List<BdcTvShowSchedule> getSameShow(Integer channel, String yearMonth, String day, BdcTvShowSchedule show, Map<Integer, Map<String, Map<String, List<BdcTvShowSchedule>>>> existingMap) throws Exception {
        List<BdcTvShowSchedule> listShow = new ArrayList<>();
        if (existingMap.containsKey(channel) && existingMap.get(channel).containsKey(yearMonth) && existingMap.get(channel).get(yearMonth).containsKey(day)) {
            if (!CollectionUtils.isNullOrEmpty(existingMap.get(channel).get(yearMonth).get(day))) {
                for (BdcTvShowSchedule existingShow : existingMap.get(channel).get(yearMonth).get(day)) {
//                    boolean isSameMusicType = isSameMusicType(show.getTvShowMusicList(), existingShow.getTvShowMusicList() );
                    boolean isSameBeginTime = existingShow.getBeginTime().equals(show.getBeginTime());

                    boolean isBothNational = StringUtils.isEmpty(show.getRegionalOffice()) && StringUtils.isEmpty(existingShow.getRegionalOffice()) ? true : false;
                    boolean isSameRegionalOffice = StringUtils.isNotEmpty(show.getRegionalOffice()) && StringUtils.isNotEmpty(existingShow.getRegionalOffice()) && show.getRegionalOffice().equalsIgnoreCase(existingShow.getRegionalOffice());

                    if (isBothNational || isSameRegionalOffice) {
                        if (isSameBeginTime) {
                            //ANDREA MODIFICA
                            boolean isSameTitle = StringUtils.equalsIgnoreCase(show.getTitle(), existingShow.getTitle());
                            if (isSameTitle) {
                                //UPDATE MUSIC LIST
                                if (show.isShow() && existingShow.isShow()) {
                                    evaluateDuration(existingShow, show);
                                    existingShow.setTvShowMusicList(updateTvMusicList(show, existingShow));
                                    listShow.add(show);
                                }
                                if ((show.isShow() && existingShow.isFilm())) {
                                    existingShow.setFlagContenuti(true);

                                    existingShow.setDuration(show.getDuration());
                                    existingShow.setTvShowMusicList(updateTvMusicList(show, existingShow));
                                    listShow.add(show);

                                }
                                if ((show.isFilm() && existingShow.isFilm() || (show.isFilm() && existingShow.isShow()))) {
                                    //logger.info("same show is film");
                                    //logger.info("same show media type " + existingShow.isShow());

                                    boolean isSameEndTime = false;
                                    if (existingShow.isFilm()) {
                                        isSameEndTime = existingShow.getEndTime().equals(show.getEndTime());
                                       // logger.info("same show is same time: " + isSameEndTime);

                                    }
                                    if (!isSameEndTime) {
                                        existingShow.setFlagContenuti(true);
                                        existingShow.setTvShowMusicList(updateTvMusicList(show, existingShow));
                                        //logger.info("same show show: " + show.toString());
                                        //logger.info("same show existingShow: " + existingShow.toString());
                                        listShow.add(show);
                                    }

                                }
                            } else {
                                //logger.info("Not same title: " + show.getTitle() + " - " + existingShow.getTitle());
                                throw new Exception(MSG_SAME_SHOW);
                            }
                        }
                    }
                }
            }
        }
        return listShow;
    }

    private BdcRdShowSchedule getSameRadioShow(Integer channel, String yearMonth, String day, BdcRdShowSchedule show, Map<Integer, Map<String, Map<String, List<BdcRdShowSchedule>>>> existingMap) throws Exception {
        if (existingMap.containsKey(channel) && existingMap.get(channel).containsKey(yearMonth) && existingMap.get(channel).get(yearMonth).containsKey(day)) {
            if (!CollectionUtils.isNullOrEmpty(existingMap.get(channel).get(yearMonth).get(day))) {
                for (BdcRdShowSchedule existingShow : existingMap.get(channel).get(yearMonth).get(day)) {
                    boolean isBothNational = StringUtils.isEmpty(show.getRegionalOffice()) && StringUtils.isEmpty(existingShow.getRegionalOffice());
                    boolean isSameRegionalOffice = StringUtils.isNotEmpty(show.getRegionalOffice()) && StringUtils.isNotEmpty(existingShow.getRegionalOffice()) && show.getRegionalOffice().equalsIgnoreCase(existingShow.getRegionalOffice());
                    boolean isRai = show.getCanale().isSpecialRadio();
                    if (isBothNational || isSameRegionalOffice) {
                        if (isRai) {
                            boolean isSameBeginTime = existingShow.getBeginTime().equals(show.getBeginTime());
                            if (isSameBeginTime) {
                                if (StringUtils.isNotEmpty(show.getTitle()) && StringUtils.isNotEmpty(existingShow.getTitle()) && show.getTitle().equalsIgnoreCase(existingShow.getTitle())) {
                                    //UPDATE MUSIC LIST
                                    existingShow.setRdShowMusicList(updateRadioMusicList(show, existingShow));
                                    return existingShow;
                                } else {
                                    throw new Exception(MSG_SAME_SHOW);
                                }
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    /*private boolean isSameMusicType(List<BdcTvShowMusic> currentMusicList, List<BdcTvShowMusic> existingMusicList){
        if( CollectionUtils.isNullOrEmpty(currentMusicList) && CollectionUtils.isNullOrEmpty(existingMusicList) ) {
            return true;
        }
        String currentMusicType = currentMusicList.get(0).getMusicType() != null ? currentMusicList.get(0).getMusicType().getName(): "";
        String existingMusicType = existingMusicList.get(0).getMusicType() != null ? existingMusicList.get(0).getMusicType().getName(): "";
        if("COMMENTO".equalsIgnoreCase(currentMusicType) && "COMMENTO".equalsIgnoreCase(existingMusicType)){
            return true;
        }if ("PUBBLICITA".equalsIgnoreCase(currentMusicType) && "PUBBLICITA".equalsIgnoreCase(existingMusicType)){
            return true;
        }if(!"COMMENTO".equalsIgnoreCase(currentMusicType) && !"COMMENTO".equalsIgnoreCase(existingMusicType)
                && !"PUBBLICITA".equalsIgnoreCase(currentMusicType) && !"PUBBLICITA".equalsIgnoreCase(existingMusicType)){
            return true;
        }
        return false;
    }
*/
    private List<BdcTvShowMusic> updateTvMusicList(BdcTvShowSchedule current, BdcTvShowSchedule existing) throws Exception {
        List<BdcTvShowMusic> updatedList = new ArrayList<>();
        updatedList.addAll(existing.getTvShowMusicList());

        //logger.info("update list: "+updatedList.toString());//7logger.info(" current music list:" + current.getTvShowMusicList().toString());
        for (BdcTvShowMusic music : current.getTvShowMusicList()) {


            if (!music.getMusicType().getCategory().equals(MUSICT_TYPE_IDENTIFICATIVA)
                    && !music.getMusicType().getCategory().equals(MUSICT_TYPE_SOTTOFONDO)
                    && !music.getMusicType().getCategory().equals(MUSICT_TYPE_FILM)
                    && !music.getMusicType().getCategory().equals(MUSICT_TYPE_PROTAGONISTA)) {
                for (BdcTvShowMusic existingMusic : existing.getTvShowMusicList()) {
                    if (existingMusic.equals(music)) {
                        throw new Exception(new String(MSG_MUSIC_ALREADY_EXISTS.getBytes(), "UTF-8"));
                    }
                }
            }
            //TODO VERIFICARE CHE IL METODO SIA NEL POSTO GIUSTO
            if (music.getMusicType().getCategory().equals(MUSICT_TYPE_IDENTIFICATIVA) || music.getMusicType().getCategory().equals(MUSICT_TYPE_SOTTOFONDO) || music.getMusicType().getCategory().equals(MUSICT_TYPE_PROTAGONISTA)) {
                for (BdcTvShowMusic existingMusic : existing.getTvShowMusicList()) {
                    if (existingMusic.equals(music)) {
                        music.setAlert(MSG_UTILIZZAZIONE_DUPLICATA);
                    }
                }
            }
            // logger.info("music: "+ music.toString());
            updatedList.add(music);
        }
        return updatedList;
    }

    private List<BdcTvShowMusic> updateTvMusicFilmList(BdcTvShowSchedule current, BdcTvShowSchedule existing) throws Exception {
        List<BdcTvShowMusic> updatedList = new ArrayList<>();
        updatedList.addAll(existing.getTvShowMusicList());
        for (BdcTvShowMusic music : current.getTvShowMusicList()) {
            if (music.getMusicType().getCategory().equals(MUSICT_TYPE_FILM)) {
                for (BdcTvShowMusic existingMusic : existing.getTvShowMusicList()) {
                    if (existingMusic.equalsFilm(music)) {
                        throw new Exception(new String(MSG_MUSIC_ALREADY_EXISTS.getBytes(), "UTF-8"));
                    }
                }
            }
            //TODO VERIFICARE CHE IL METODO SIA NEL POSTO GIUSTO
            if (music.getMusicType().getCategory().equals(MUSICT_TYPE_IDENTIFICATIVA) || music.getMusicType().getCategory().equals(MUSICT_TYPE_SOTTOFONDO) || music.getMusicType().getCategory().equals(MUSICT_TYPE_PROTAGONISTA)) {
                for (BdcTvShowMusic existingMusic : existing.getTvShowMusicList()) {
                    if (existingMusic.equals(music)) {
                        music.setAlert(MSG_UTILIZZAZIONE_DUPLICATA);
                    }
                }
            }
            updatedList.add(music);
        }
        return updatedList;
    }

    private List<BdcRdShowMusic> updateRadioMusicList(BdcRdShowSchedule show, BdcRdShowSchedule existingShow) throws Exception {
        List<BdcRdShowMusic> updatedList = new ArrayList<>();
        updatedList.addAll(existingShow.getRdShowMusicList());
        for (BdcRdShowMusic music : show.getRdShowMusicList()) {
            if (!music.getMusicType().getCategory().equals(MUSICT_TYPE_IDENTIFICATIVA)) {
                for (BdcRdShowMusic existingMusic : existingShow.getRdShowMusicList()) {
                    if (existingMusic.equals(music)) {
                        throw new Exception(new String(MSG_MUSIC_ALREADY_EXISTS.getBytes(), "UTF-8"));
                    }
                }
            }
            updatedList.add(music);
        }
        return updatedList;
    }

    @Override
    public boolean evaluateKPI(Integer idCanale, Integer anno, Integer mese) {
        if (idCanale == null || anno == null || mese == null) {
            return false;
        }
        BdcCanali canale = bdcCanaliService.findById(idCanale);
        if (canale == null ||
                canale.getTipoCanale() == null ||
                canale.getBdcBroadcasters() == null ||
                canale.getBdcBroadcasters().getId() == null) {
            return false;
        }
        Integer idBroadcaster = canale.getBdcBroadcasters().getId();
        TipoBroadcaster tipoCanale = canale.getTipoCanale();

        if (TipoBroadcaster.TELEVISIONE.equals(tipoCanale)) {
            return bdcIndicatoreService.evaluateIndicatoriTv(idBroadcaster, idCanale, anno, mese);
        } else if (TipoBroadcaster.RADIO.equals(tipoCanale)) {
            return bdcIndicatoreService.evaluateIndicatoriRadio(idBroadcaster, idCanale, anno, mese);
        }
        return false;
    }


    //LOAD TV SCHEDULE
    private void loadTvShows(Integer idChannel, Integer scheduleYear, Integer scheduleMonth, Map<Integer, Map<String, Map<String, List<BdcTvShowSchedule>>>> existingMap) {
        String yearMonth = String.format("%d-%d", scheduleYear, scheduleMonth);
        if (existingMap.containsKey(idChannel) && existingMap.get(idChannel).containsKey(yearMonth)) {
            return;
        } else {
            Map<String, List<BdcTvShowSchedule>> dailyShowSchedule = loadTvDailySchedule(idChannel, scheduleYear, scheduleMonth);
            if (!dailyShowSchedule.isEmpty()) {
                if (!existingMap.containsKey(idChannel)) {
                    existingMap.put(idChannel, new HashMap<String, Map<String, List<BdcTvShowSchedule>>>());
                }
                if (!existingMap.get(idChannel).containsKey(yearMonth)) {
                    existingMap.get(idChannel).put(yearMonth, new HashMap<String, List<BdcTvShowSchedule>>());
                }
                existingMap.get(idChannel).get(yearMonth).putAll(dailyShowSchedule);
            }
        }
    }

    private Map<String, List<BdcTvShowSchedule>> loadTvDailySchedule(Integer idCanale, Integer scheduleYear, Integer scheduleMonth) {
        Map<String, List<BdcTvShowSchedule>> ret = new HashMap<>();
        try {
            EntityManager em = provider.get();
            TypedQuery<BdcTvShowSchedule> q = em.createQuery("Select show FROM BdcTvShowSchedule show " +
                    "WHERE show.canale.id = :idCanale and show.scheduleYear = :scheduleYear and show.scheduleMonth = :scheduleMonth " +
                    "ORDER BY show.beginTime ASC ", BdcTvShowSchedule.class);
            q.setParameter("idCanale", idCanale);
            q.setParameter("scheduleYear", scheduleYear);
            q.setParameter("scheduleMonth", scheduleMonth);
            List<BdcTvShowSchedule> resulSet = q.getResultList();

            if (!CollectionUtils.isNullOrEmpty(resulSet)) {
                for (BdcTvShowSchedule current : resulSet) {
                    Integer day = Utilities.getDateField(current.getBeginTime(), Calendar.DAY_OF_MONTH);
                    if (!ret.containsKey(day.toString())) {
                        ret.put(day.toString(), new ArrayList<BdcTvShowSchedule>());
                    }
                    ret.get(day.toString()).add(current);
                }
            }
        } catch (NoResultException e) {
            logger.error("No result found for channel {} year {} month {} ", idCanale, scheduleYear, scheduleMonth);
        }
        return ret;
    }

    private void loadTvNearMonth(BdcTvShowSchedule show, Map<Integer, Map<String, Map<String, List<BdcTvShowSchedule>>>> existingMap, boolean isPrevious) {
        int quantity = isPrevious ? -1 : 1;
        Date updatedDate = show.getBeginTime();
        Utilities.manageDate(updatedDate, quantity, Calendar.MONTH);
        loadTvShows(show.getCanale().getId(), Utilities.getDateField(updatedDate, Calendar.YEAR), Utilities.getDateField(updatedDate, Calendar.MONTH) + 1, existingMap);
    }

    //LOAD RADIO SCHEDULE
    private void loadRadioShows(Integer idChannel, Integer scheduleYear, Integer scheduleMonth, Map<Integer, Map<String, Map<String, List<BdcRdShowSchedule>>>> existingMap) {
        String yearMonth = String.format("%d-%d", scheduleYear, scheduleMonth);
        if (existingMap.containsKey(idChannel) && existingMap.get(idChannel).containsKey(yearMonth)) {
            return;
        } else {
            Map<String, List<BdcRdShowSchedule>> dailyShowSchedule = loadRadioDailySchedule(idChannel, scheduleYear, scheduleMonth);
            if (!dailyShowSchedule.isEmpty()) {
                if (!existingMap.containsKey(idChannel)) {
                    existingMap.put(idChannel, new HashMap<String, Map<String, List<BdcRdShowSchedule>>>());
                }
                if (!existingMap.get(idChannel).containsKey(yearMonth)) {
                    existingMap.get(idChannel).put(yearMonth, new HashMap<String, List<BdcRdShowSchedule>>());
                }
                existingMap.get(idChannel).get(yearMonth).putAll(dailyShowSchedule);
            }
        }
    }

    private Map<String, List<BdcRdShowSchedule>> loadRadioDailySchedule(Integer idCanale, Integer scheduleYear, Integer scheduleMonth) {
        Map<String, List<BdcRdShowSchedule>> ret = new HashMap<>();
        try {
            EntityManager em = provider.get();
            TypedQuery<BdcRdShowSchedule> q = em.createQuery("Select show FROM BdcRdShowSchedule show " +
                    "WHERE show.canale.id = :idCanale and show.schedulYear = :scheduleYear and show.scheduleMonth = :scheduleMonth " +
                    "ORDER BY show.beginTime ASC ", BdcRdShowSchedule.class);
            q.setParameter("idCanale", idCanale);
            q.setParameter("scheduleYear", scheduleYear);
            q.setParameter("scheduleMonth", scheduleMonth);
            List<BdcRdShowSchedule> resulSet = q.getResultList();

            if (!CollectionUtils.isNullOrEmpty(resulSet)) {
                for (BdcRdShowSchedule current : resulSet) {
                    Integer day = Utilities.getDateField(current.getDate(), Calendar.DAY_OF_MONTH);
                    if (!ret.containsKey(day.toString())) {
                        ret.put(day.toString(), new ArrayList<BdcRdShowSchedule>());
                    }
                    ret.get(day.toString()).add(current);
                }
            }
        } catch (NoResultException e) {
            logger.error("No result found for channel {} year {} month {} ", idCanale, scheduleYear, scheduleMonth);
        }
        return ret;
    }

    private void loadRadioNearMonth(BdcRdShowSchedule show, Map<Integer, Map<String, Map<String, List<BdcRdShowSchedule>>>> existingMap, boolean isPrevious) {
        int quantity = isPrevious ? -1 : 1;
        Date updatedDate = show.getDate();
        Utilities.manageDate(updatedDate, quantity, Calendar.MONTH);
        loadRadioShows(show.getCanale().getId(), Utilities.getDateField(updatedDate, Calendar.YEAR), Utilities.getDateField(updatedDate, Calendar.MONTH) + 1, existingMap);
    }


    private void evaluateDuration(BdcTvShowSchedule existingShow, BdcTvShowSchedule show) {
        boolean existingDuration = false;
        long diffExisting = TimeUnit.MILLISECONDS.toSeconds(existingShow.getEndTime().getTime() - existingShow.getBeginTime().getTime());
        if (diffExisting == existingShow.getDuration().longValue()) {
            existingDuration = true;
        }

        boolean duration = false;
        long diff = TimeUnit.MILLISECONDS.toSeconds(existingShow.getEndTime().getTime() - existingShow.getBeginTime().getTime());
        if (diff == show.getDuration().longValue()) {
            duration = true;
        }

        if (!existingDuration && !duration) {
            existingShow.setDuration(new Integer((int) diff));
        }

    }

    private boolean valutaSovrapposizione(Date inizioCorrente, Date fineCorrente, Date inizioPrecedente, Date finePrecedente, Integer overlapLimit) {

        boolean result = false;
        Date maxInizio = inizioCorrente;
        Date minFine = fineCorrente;

        if (inizioCorrente.compareTo(inizioPrecedente) < 0) {
            maxInizio = inizioPrecedente;
            logger.info("maxInizio=" + maxInizio);
        }
        if (fineCorrente.compareTo(finePrecedente) > 0) {
            minFine = finePrecedente;

        }

        long diff = TimeUnit.MILLISECONDS.toSeconds(minFine.getTime() - maxInizio.getTime());
        Long overlap = diff - overlapLimit.longValue();

        if (overlap.compareTo(new Long(0)) > 0) {
            result = true;
        }

        return result;
    }

}
