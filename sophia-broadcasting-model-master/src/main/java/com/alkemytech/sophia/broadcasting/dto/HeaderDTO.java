
package com.alkemytech.sophia.broadcasting.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.validation.Valid;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;


@XmlAccessorType(XmlAccessType.FIELD)
public class HeaderDTO implements Serializable
{

    @Expose
    @XmlElement(name = "CodiceProcesso")
    @SerializedName("CodiceProcesso")
    private String codiceProcesso;

    @Expose
    @XmlElement(name = "Mittente")
    @SerializedName("Mittente")
    private String mittente;
    @Expose
    @XmlElement(name = "Destinatario")
    @SerializedName("Destinatario")
    private String destinatario;
    @Expose
    @Valid
    @XmlElement(name = "Messaggi")
    @SerializedName("Messaggi")
    private MessaggiDTO messaggiDTO;
    @Expose
    @XmlElement(name = "TipoElaborazione")
    @SerializedName("TipoElaborazione")
    private String tipoElaborazione;
    @Expose
    @XmlElement(name = "UserName")
    @SerializedName("UserName")
    private String userName;
    private final static long serialVersionUID = 7578800960776775038L;

    public String getCodiceProcesso() {
        return codiceProcesso;
    }

    public void setCodiceProcesso(String codiceProcesso) {
        this.codiceProcesso = codiceProcesso;
    }

    public String getMittente() {
        return mittente;
    }

    public void setMittente(String mittente) {
        this.mittente = mittente;
    }

    public String getDestinatario() {
        return destinatario;
    }

    public void setDestinatario(String destinatario) {
        this.destinatario = destinatario;
    }

    public MessaggiDTO getMessaggiDTO() {
        return messaggiDTO;
    }

    public void setMessaggiDTO(MessaggiDTO messaggiDTO) {
        this.messaggiDTO = messaggiDTO;
    }

    public String getTipoElaborazione() {
        return tipoElaborazione;
    }

    public void setTipoElaborazione(String tipoElaborazione) {
        this.tipoElaborazione = tipoElaborazione;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("codiceProcesso", codiceProcesso).append("mittente", mittente).append("destinatario", destinatario).append("messaggiDTO", messaggiDTO).append("tipoElaborazione", tipoElaborazione).append("userName", userName).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(tipoElaborazione).append(userName).append(mittente).append(codiceProcesso).append(destinatario).append(messaggiDTO).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof HeaderDTO) == false) {
            return false;
        }
        HeaderDTO rhs = ((HeaderDTO) other);
        return new EqualsBuilder().append(tipoElaborazione, rhs.tipoElaborazione).append(userName, rhs.userName).append(mittente, rhs.mittente).append(codiceProcesso, rhs.codiceProcesso).append(destinatario, rhs.destinatario).append(messaggiDTO, rhs.messaggiDTO).isEquals();
    }

}
