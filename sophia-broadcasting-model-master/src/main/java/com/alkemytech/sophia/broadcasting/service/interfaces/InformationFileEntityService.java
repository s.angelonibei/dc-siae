package com.alkemytech.sophia.broadcasting.service.interfaces;

import com.alkemytech.sophia.broadcasting.dto.FileKpiDTO;
import com.alkemytech.sophia.broadcasting.enums.Repertorio;
import com.alkemytech.sophia.broadcasting.enums.Stato;
import com.alkemytech.sophia.broadcasting.model.BdcBroadcasters;
import com.alkemytech.sophia.broadcasting.model.BdcInformationFileEntity;
import com.alkemytech.sophia.performing.model.PerfRsUtilizationFile;

import java.text.ParseException;
import java.util.List;

public interface InformationFileEntityService {

    List<BdcInformationFileEntity> findAll();

    List<BdcInformationFileEntity> findByStato(Stato stato);

    BdcInformationFileEntity findById(Integer id);

    List<BdcInformationFileEntity> getListaFiles(BdcBroadcasters emittente, Stato stato);

    List<BdcInformationFileEntity> getListaFilesBroadcaster(BdcBroadcasters emittente , Repertorio repertorio);

    BdcInformationFileEntity save(BdcInformationFileEntity entity) throws ParseException;

    List<FileKpiDTO> getFileKpiToWork();

    Boolean cancelUtilizationRecords(Integer idUtilizationFile);
    Boolean confirmUtilizationRecords(Integer idUtilizationFile);
}
