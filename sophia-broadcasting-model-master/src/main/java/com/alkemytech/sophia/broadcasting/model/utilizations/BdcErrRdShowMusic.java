package com.alkemytech.sophia.broadcasting.model.utilizations;

import com.alkemytech.sophia.broadcasting.model.BdcNormalizedFile;
import com.alkemytech.sophia.broadcasting.model.MusicType;
import com.alkemytech.sophia.broadcasting.utils.Utilities;
import com.alkemytech.sophia.common.Constants;
import com.alkemytech.sophia.common.tools.StringTools;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import static com.alkemytech.sophia.broadcasting.utils.Utilities.getMapValue;

@Entity
@Table(name="BDC_ERR_RD_SHOW_MUSIC")
@NamedQuery(name="BdcErrRdShowMusic.findAll", query="SELECT b FROM BdcErrRdShowMusic b")
public class BdcErrRdShowMusic implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue( strategy =  GenerationType.IDENTITY)
    @Column( name= "ID_ERR_RD_SHOW_MUSIC")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn (name = "ID_ERR_RD_SHOW_SCHEDULE")
    private BdcErrRdShowSchedule errRdShowSchedule;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_NORMALIZED_FILE", nullable = false)
    private BdcNormalizedFile normalizedFile;

    @ManyToOne
    @JoinColumn( name = "ID_MUSIC_TYPE")
    private MusicType musicType;

    @Column( name = "TITLE")
    private String title;

    @Column( name= "COMPOSER")
    private String composer;

    @Column( name= "PERFORMER")
    private String performer;

    @Column( name= "ALBUM")
    private String album;

    @Column( name = "DURATION")
    private Integer duration;

    @Column( name= "ISWC_CODE")
    private String iswcCode;

    @Column( name= "ISRC_CODE")
    private String isrcCode;

    @Column( name = "BEGIN_TIME")
    private Date beginTime;

    @Column( name = "REAL_DURATION")
    private Integer realDuration;

    @Column( name = "REPORT_DURATION")
    private String reportDuration;

    @Column( name = "REPORT_BEGIN_TIME")
    private String reportBeginTime;

    @Column( name = "REPORT_MUSIC_TYPE")
    private String reportMusicType;

    @Column( name = "DAYS_FROM_UPLOAD")
    private Integer daysFromUpload;

    @Column( name = "CREATION_DATE")
    private Date creationDate;

    @Column( name = "MODIFY_DATE")
    private Date modifyDate;

    //R6-22
    @Column(name = "IDENTIFICATIVO_RAI")
    private String identificativoRai;

    public BdcErrRdShowMusic() {
    }

    public BdcErrRdShowMusic(MusicType musicType, BdcNormalizedFile normalizedFile, Date beginTime, Map<String, Object> record) {
        this.musicType = musicType;
        this.normalizedFile = normalizedFile;
        this.title = Utilities.getMapValue(record, Constants.TITOLO_OP);
        this.composer = Utilities.getMapValue(record,Constants.COMPOSITORE_OP);
        this.performer = Utilities.getMapValue(record,Constants.ESECUTORE_OP);
        this.album = Utilities.getMapValue(record,Constants.ALBUM_OP);
        if( getMapValue(record, Constants.DURATA_OP) != null && getMapValue(record, Constants.DURATA_OP).matches(StringTools.DURATION_FORMAT) ){
            this.duration = Integer.parseInt( StringTools.parseTimeToSeconds( getMapValue(record, Constants.DURATA_OP) ) ) ;
        }
        if( getMapValue(record, Constants.DURATA_EFFETTIVA_OP) != null && getMapValue(record, Constants.DURATA_EFFETTIVA_OP).matches(StringTools.DURATION_FORMAT) ){
            this.realDuration = Integer.parseInt( StringTools.parseTimeToSeconds( getMapValue(record, Constants.DURATA_EFFETTIVA_OP) ) ) ;
        }
        this.iswcCode = Utilities.getMapValue(record,Constants.CODICE_ISWC_OP);
        this.isrcCode = Utilities.getMapValue(record,Constants.CODICE_ISRC_OP);
        this.beginTime = beginTime;
        this.reportDuration = Utilities.getMapValue(record, Constants.DURATA_OP);
        this.reportMusicType = Utilities.getMapValue(record, Constants.CATEGORIA_USO_OP);
        this.reportBeginTime = Utilities.getMapValue(record, Constants.ORARIO_INIZIO_OP);
        this.creationDate = new Date();
        //R6-22
        this.identificativoRai = Utilities.getMapValue(record, Constants.IDENTIFICATIVO_RAI);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BdcErrRdShowSchedule getErrRdShowSchedule() {
        return errRdShowSchedule;
    }

    public void setErrRdShowSchedule(BdcErrRdShowSchedule errRdShowSchedule) {
        this.errRdShowSchedule = errRdShowSchedule;
    }

    public BdcNormalizedFile getNormalizedFile() {
        return normalizedFile;
    }

    public void setNormalizedFile(BdcNormalizedFile normalizedFile) {
        this.normalizedFile = normalizedFile;
    }

    public MusicType getMusicType() {
        return musicType;
    }

    public void setMusicType(MusicType musicType) {
        this.musicType = musicType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getComposer() {
        return composer;
    }

    public void setComposer(String composer) {
        this.composer = composer;
    }

    public String getPerformer() {
        return performer;
    }

    public void setPerformer(String performer) {
        this.performer = performer;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getIswcCode() {
        return iswcCode;
    }

    public void setIswcCode(String iswcCode) {
        this.iswcCode = iswcCode;
    }

    public String getIsrcCode() {
        return isrcCode;
    }

    public void setIsrcCode(String isrcCode) {
        this.isrcCode = isrcCode;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Integer getRealDuration() {
        return realDuration;
    }

    public void setRealDuration(Integer realDuration) {
        this.realDuration = realDuration;
    }

    public String getReportDuration() {
        return reportDuration;
    }

    public void setReportDuration(String reportDuration) {
        this.reportDuration = reportDuration;
    }

    public String getReportBeginTime() {
        return reportBeginTime;
    }

    public void setReportBeginTime(String reportBeginTime) {
        this.reportBeginTime = reportBeginTime;
    }

    public String getReportMusicType() {
        return reportMusicType;
    }

    public void setReportMusicType(String reportMusicType) {
        this.reportMusicType = reportMusicType;
    }

    public Integer getDaysFromUpload() {
        return daysFromUpload;
    }

    public void setDaysFromUpload(Integer daysFromUpload) {
        this.daysFromUpload = daysFromUpload;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    //R6-22
    public String getIdentificativoRai(){return identificativoRai;}

    public void setIdentificativoRai(String identificativoRai){this.identificativoRai = identificativoRai;}

    @Override
    public String toString() {
        return "BdcErrRdShowMusic{" +
                "id=" + id +
                ", errRdShowSchedule=" + errRdShowSchedule +
                ", normalizedFile=" + normalizedFile +
                ", musicType=" + musicType +
                ", title='" + title + '\'' +
                ", composer='" + composer + '\'' +
                ", performer='" + performer + '\'' +
                ", album='" + album + '\'' +
                ", duration=" + duration +
                ", iswcCode='" + iswcCode + '\'' +
                ", isrcCode='" + isrcCode + '\'' +
                ", beginTime=" + beginTime +
                ", realDuration=" + realDuration +
                ", reportDuration='" + reportDuration + '\'' +
                ", reportBeginTime='" + reportBeginTime + '\'' +
                ", reportMusicType='" + reportMusicType + '\'' +
                ", daysFromUpload=" + daysFromUpload +
                ", creationDate=" + creationDate +
                ", modifyDate=" + modifyDate +
                //R6-22
                ", identificativoRai=" + identificativoRai +
                '}';
    }
}