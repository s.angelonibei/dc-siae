package com.alkemytech.sophia.broadcasting.model.utilizations;

import com.alkemytech.sophia.broadcasting.model.BdcNormalizedFile;
import com.alkemytech.sophia.broadcasting.model.MusicType;
import com.alkemytech.sophia.broadcasting.utils.Utilities;
import com.alkemytech.sophia.common.Constants;
import com.alkemytech.sophia.common.tools.StringTools;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang3.math.NumberUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import javax.persistence.*;

import static com.alkemytech.sophia.broadcasting.utils.Utilities.getMapValue;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name="BDC_TV_SHOW_MUSIC")
@NamedQuery(name="BdcTvShowMusic.findAll", query="SELECT b FROM BdcTvShowMusic b")
public class BdcTvShowMusic implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue( strategy =  GenerationType.IDENTITY)
    @Column( name = "ID_TV_SHOW_MUSIC")
    private Long id;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn (name = "ID_TV_SHOW_SCHEDULE")
    private BdcTvShowSchedule tvShowSchedule;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_NORMALIZED_FILE", nullable = false)
    private BdcNormalizedFile normalizedFile;

    @ManyToOne
    @JoinColumn( name = "ID_MUSIC_TYPE")
    private MusicType musicType;

    @Column( name = "TITLE")
    private String title;

    @Column( name = "FIRST_COMPOSER")
    private String firstComposer;

    @Column( name = "SECOND_COMPOSER")
    private String secondComposer;

    @Column( name = "DURATION")
    private Integer duration;

    @Column( name = "REAL_DURATION")
    private Integer realDuration;

    @Column( name = "NOTE")
    private String note;

    @Column( name = "DAYS_FROM_UPLOAD")
    private Integer daysFromUpload;

    @Column( name = "CREATION_DATE")
    private Date creationDate;

    @Column( name = "MODIFY_DATE")
    private Date modifyDate;

    @Column( name = "ALERT")
    private String alert;

    @Column( name = "REPLICA")
    private String replica;

    @Column( name = "PROD_COUNTRY")
    private String prodCountry;

    @Column( name = "PROD_YEAR")
    private String prodYear;

    @Column( name = "PRODUCTOR")
    private String productor;

    @Column( name = "DIRECTOR")
    private String director;

    @Column( name = "EPISODE_TITLE")
    private String episodeTitle;

    @Column( name = "EPISODE_ORIGINAL_TITLE")
    private String episodeOriginalTitle;

    @Column( name = "EPISODE_NUMBER")
    private Integer episodeNumber;

    @Column( name = "ORIGINAL_TITLE")
    private String originalTitle;

    //R6-22
    @Column( name = "IDENTIFICATIVO_RAI")
    private String identificativoRai;



    public BdcTvShowMusic() {
    }

    public BdcTvShowMusic( MusicType musicType, BdcNormalizedFile normalizedFile, Map<String, Object> record ) {
        this.musicType = musicType;
        this.normalizedFile = normalizedFile;
        this.title = Utilities.getMapValue(record,Constants.TITOLO_OP);
        this.firstComposer = Utilities.getMapValue(record,Constants.COMPOSITORE1_OP);
        this.secondComposer = Utilities.getMapValue(record,Constants.COMPOSITORE2_OP);
        this.originalTitle = getMapValue(record, Constants.TITOLO_ORIGINALE_TX);
        this.replica = getMapValue(record, Constants.REPLICA_TX);
        this.prodYear = getMapValue(record, Constants.ANNO_PRODUZIONE_TX) != null ? getMapValue(record, Constants.ANNO_PRODUZIONE_TX) : null;
        this.prodCountry = getMapValue(record, Constants.PAESE_PRODUZIONE_TX);
        this.productor = getMapValue(record, Constants.PRODUTTORE_TX);
        this.director = getMapValue(record, Constants.REGISTA_TX);
        this.episodeTitle = getMapValue(record, Constants.TITOLO_EPISODIO_TX);
        this.episodeOriginalTitle = getMapValue(record, Constants.TITOLO_ORIGINALE_EPISODIO_TX);
        this.episodeNumber = getMapValue(record, Constants.NUM_PROGRESSIVO_EPISODIO_TX) != null && NumberUtils.isParsable(getMapValue(record, Constants.NUM_PROGRESSIVO_EPISODIO_TX)) ? new Integer(getMapValue(record, Constants.NUM_PROGRESSIVO_EPISODIO_TX)) : null;


        if( getMapValue(record, Constants.DURATA_OP) != null && getMapValue(record, Constants.DURATA_OP).matches(StringTools.DURATION_FORMAT) ){
            this.duration = Integer.parseInt( StringTools.parseTimeToSeconds( getMapValue(record, Constants.DURATA_OP) ) ) ;
        }
        if( getMapValue(record, Constants.DURATA_EFFETTIVA_OP) != null && getMapValue(record, Constants.DURATA_EFFETTIVA_OP).matches(StringTools.DURATION_FORMAT) ){
            this.realDuration = Integer.parseInt( StringTools.parseTimeToSeconds( getMapValue(record, Constants.DURATA_EFFETTIVA_OP) ) ) ;
        }
        this.creationDate = new Date();
        //R6-22
        this.identificativoRai = getMapValue(record, Constants.IDENTIFICATIVO_RAI);
    }

    public String getAlert() {return alert;}

    public void setAlert(String alert) {this.alert = alert;}

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BdcNormalizedFile getNormalizedFile() {
        return normalizedFile;
    }

    public void setNormalizedFile(BdcNormalizedFile normalizedFile) {
        this.normalizedFile = normalizedFile;
    }

    public MusicType getMusicType() {
        return musicType;
    }

    public void setMusicType(MusicType musicType) {
        this.musicType = musicType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstComposer() {
        return firstComposer;
    }

    public void setFirstComposer(String firstComposer) {
        this.firstComposer = firstComposer;
    }

    public String getSecondComposer() {
        return secondComposer;
    }

    public void setSecondComposer(String secondComposer) {
        this.secondComposer = secondComposer;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getRealDuration() {
        return realDuration;
    }

    public void setRealDuration(Integer realDuration) {
        this.realDuration = realDuration;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getDaysFromUpload() {
        return daysFromUpload;
    }

    public void setDaysFromUpload(Integer daysFromUpload) {
        this.daysFromUpload = daysFromUpload;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getOriginalTitle() {return originalTitle;}

    public void setOriginalTitle(String originalTitle) {this.originalTitle = originalTitle;}

    public String getReplica() {
        return replica;
    }

    public void setReplica(String replica) {
        this.replica = replica;
    }

    public String getProdCountry() {
        return prodCountry;
    }

    public void setProdCountry(String prodCountry) {
        this.prodCountry = prodCountry;
    }

    public String getProdYear() {
        return prodYear;
    }

    public void setProdYear(String prodYear) {
        this.prodYear = prodYear;
    }

    public String getProductor() {
        return productor;
    }

    public void setProductor(String productor) {
        this.productor = productor;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getEpisodeTitle() {
        return episodeTitle;
    }

    public void setEpisodeTitle(String episodeTitle) {
        this.episodeTitle = episodeTitle;
    }

    public String getEpisodeOriginalTitle() {
        return episodeOriginalTitle;
    }

    public void setEpisodeOriginalTitle(String episodeOriginalTitle) {this.episodeOriginalTitle = episodeOriginalTitle;}

    public Integer getEpisodeNumber() {
        return episodeNumber;
    }

    public void setEpisodeNumber(Integer episodeNumber) {
        this.episodeNumber = episodeNumber;
    }



    public BdcTvShowSchedule getTvShowSchedule() {
		return tvShowSchedule;
	}

	public void setTvShowSchedule(BdcTvShowSchedule tvShowSchedule) {
		this.tvShowSchedule = tvShowSchedule;
	}

	//R6-22
    public String getIdentificativoRai(){return identificativoRai;}

    public void setIdentificativoRai(String identificativoRai){this.identificativoRai = identificativoRai;}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BdcTvShowMusic)) return false;

        BdcTvShowMusic that = (BdcTvShowMusic) o;

        if (musicType != null ? !musicType.equals(that.musicType) : that.musicType != null) return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;
        return realDuration != null ? realDuration.equals(that.realDuration) : that.realDuration == null;

    }

    public boolean equalsFilm(Object o) {
        if (this == o) return true;
        if (!(o instanceof BdcTvShowMusic)) return false;

        BdcTvShowMusic that = (BdcTvShowMusic) o;

        if (musicType != null ? !musicType.equals(that.musicType) : that.musicType != null) return false;
        if (originalTitle != null ? !originalTitle.equals(that.originalTitle) : that.originalTitle != null) return false;
        return duration != null ? duration.equals(that.duration) : that.duration == null;

    }

    @Override
    public int hashCode() {
        int result = musicType != null ? musicType.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (realDuration != null ? realDuration.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "BdcTvShowMusic{" +
                "id=" + id +
                ", tvShowSchedule=" + tvShowSchedule +
                ", normalizedFile=" + normalizedFile +
                ", musicType=" + musicType +
                ", title='" + title + '\'' +
                ", firstComposer='" + firstComposer + '\'' +
                ", secondComposer='" + secondComposer + '\'' +
                ", duration=" + duration +
                ", realDuration=" + realDuration +
                ", note='" + note + '\'' +
                ", daysFromUpload=" + daysFromUpload +
                ", creationDate=" + creationDate +
                ", modifyDate=" + modifyDate +
                ", alert=" + alert +
                ", replica='" + replica + '\'' +
                ", prodCountry='" + prodCountry + '\'' +
                ", prodYear='" + prodYear + '\'' +
                ", productor='" + productor + '\'' +
                ", director='" + director + '\'' +
                ", episodeTitle='" + episodeTitle + '\'' +
                ", episodeOriginalTitle='" + episodeOriginalTitle + '\'' +
                ", episodeNumber=" + episodeNumber +
                ", originalTitle='" + originalTitle + '\'' +
                ", identificativoRai='" + identificativoRai + '\'' +
                '}';
    }
}