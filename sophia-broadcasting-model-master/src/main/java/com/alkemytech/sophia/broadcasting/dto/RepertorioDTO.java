package com.alkemytech.sophia.broadcasting.dto;

import com.alkemytech.sophia.broadcasting.model.BdcRepertorio;

public class RepertorioDTO {
	
	private String nome;

	private boolean active;
	
	private boolean defaultValue;

	
	public RepertorioDTO() {
		super();
	}

	public RepertorioDTO(String nome, boolean active, boolean defaultValue) {
		super();
		this.nome = nome;
		this.active = active;
		this.defaultValue = defaultValue;
	}

	

	public RepertorioDTO(BdcRepertorio bdcRepertorio) {
		this.nome = bdcRepertorio.getNome();
		this.active = false;
		this.defaultValue = false;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(boolean defaultValue) {
		this.defaultValue = defaultValue;
	}
	
	
}
