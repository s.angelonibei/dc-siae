package com.alkemytech.sophia.broadcasting.dto;

import java.util.List;

public class RegoleErroreDTO {

	public List<Regola> regola1;
	public List<Regola> regola2;
	public List<Regola> regola3;

	public RegoleErroreDTO() {
		super();
	}

	public RegoleErroreDTO(List<Regola> regola1, List<Regola> regola2, List<Regola> regola3) {
		super();
		this.regola1 = regola1;
		this.regola2 = regola2;
		this.regola3 = regola3;
	}

	public List<Regola> getRegola1() {
		return regola1;
	}

	public void setRegola1(List<Regola> regola1) {
		this.regola1 = regola1;
	}

	public List<Regola> getRegola2() {
		return regola2;
	}

	public void setRegola2(List<Regola> regola2) {
		this.regola2 = regola2;
	}

	public List<Regola> getRegola3() {
		return regola3;
	}

	public void setRegola3(List<Regola> regola3) {
		this.regola3 = regola3;
	}

	public class Regola {
		public String fieldName;
		public String inputValue;

		public Regola() {
			super();
		}

		public Regola(String fieldName, String inputValue) {
			super();
			this.fieldName = fieldName;
			this.inputValue = inputValue;
		}

		public String getFieldName() {
			return fieldName;
		}

		public void setFieldName(String fieldName) {
			this.fieldName = fieldName;
		}

		public String getInputValue() {
			return inputValue;
		}

		public void setInputValue(String inputValue) {
			this.inputValue = inputValue;
		}

	}
}
