package com.alkemytech.sophia.broadcasting.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "BDC_STORICO_CANALE")
public class BdcCanaleStorico implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_STORICO_CANALE", nullable = false)
	private Integer id;
	@Column(name = "ID_CANALE", nullable = false)
	private Integer bdcCanali;
	@Column(name = "ID_BROADCASTER", nullable = false)
	private Integer bdcBroadcasters;
	@Column(name = "NOME_CANALE", nullable = false)
	private String nome;
	@Enumerated(EnumType.STRING)
	@Column(name = "TIPO_CANALE", nullable = false)
	private TipoBroadcaster tipoCanale;
	@Column(name = "GENERALISTA")
	private Integer generalista;
	@Transient
	private Integer active;
	@Column(name = "SPECIAL_RADIO")
	private Integer specialRadio;
	@Column(name = "DATA_INIZIO_VALIDIT")
	private Date dataInizioValid;
	@Column(name = "DATA_FINE_VALIDIT")
	private Date dataFineValid;
	@Column(name = "DATA_CREAZIONE")
	private Date dataCreazione;
	@Column(name = "DATA_MODIFICA")
	private Date dataModifica;
	@Column(name = "UTENTE_MODIFICA")
	private String utenteModifica;

	public BdcCanaleStorico() {
	}

	public BdcCanaleStorico(Integer bdcCanali, Integer bdcBroadcasters, String nome, TipoBroadcaster tipoCanale,
			Integer generalista, Integer active, Integer specialRadio, Date dataInizioValid, Date dataFineValid,
			Date dataCreazione, Date dataModifica, String utenteModifica) {
		this.bdcCanali = bdcCanali;
		this.bdcBroadcasters = bdcBroadcasters;
		this.nome = nome;
		this.tipoCanale = tipoCanale;
		this.generalista = generalista;
		this.active = active;
		this.specialRadio = specialRadio;
		this.dataInizioValid = dataInizioValid;
		this.dataFineValid = dataFineValid;
		this.dataCreazione = dataCreazione;
		this.dataModifica = dataModifica;
		this.utenteModifica = utenteModifica;
	}

	public BdcCanaleStorico(BdcCanali bdcCanali) {
		this.bdcCanali = bdcCanali.getId();
		this.bdcBroadcasters = bdcCanali.getBdcBroadcasters().getId();
		this.nome = bdcCanali.getNome();
		this.tipoCanale = bdcCanali.getTipoCanale();
		this.generalista = bdcCanali.getGeneralista();
		this.specialRadio = bdcCanali.getSpecialRadio();
		this.dataInizioValid = bdcCanali.getDataInizioValid();
		this.dataFineValid = bdcCanali.getDataFineValid();
		this.dataModifica = new Date();
		this.utenteModifica = bdcCanali.getUtenteUltimaModifica();
	}

	public BdcCanaleStorico(Integer id, Integer bdcBroadcasters, String nome, TipoBroadcaster tipoCanale,
			Integer generalista, Integer active, Integer specialRadio, Date dataInizioValid, Date dataFineValid,
			Date dataModifica, String utenteModifica) {
		super();
		this.id = id;
		this.bdcBroadcasters = bdcBroadcasters;
		this.nome = nome;
		this.tipoCanale = tipoCanale;
		this.generalista = generalista;
		this.specialRadio = specialRadio;
		this.dataInizioValid = dataInizioValid;
		this.dataFineValid = dataFineValid;
		this.dataModifica = dataModifica;
		this.utenteModifica = utenteModifica;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getBdcBroadcasters() {
		return bdcBroadcasters;
	}

	public void setBdcBroadcasters(Integer idBroadcaster) {
		this.bdcBroadcasters = idBroadcaster;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public TipoBroadcaster getTipoCanale() {
		return tipoCanale;
	}

	public void setTipoCanale(TipoBroadcaster tipo_canale) {
		this.tipoCanale = tipo_canale;
	}

	public Integer getGeneralista() {
		return generalista;
	}

	public void setGeneralista(Integer generalista) {
		this.generalista = generalista;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Integer getSpecialRadio() {
		return specialRadio;
	}

	public void setSpecialRadio(Integer specialRadio) {
		this.specialRadio = specialRadio;
	}

	public Date getDataInizioValid() {
		return dataInizioValid;
	}

	public void setDataInizioValid(Date dataInizioValid) {
		this.dataInizioValid = dataInizioValid;
	}

	public Date getDataFineValid() {
		return dataFineValid;
	}

	public void setDataFineValid(Date dataFineValid) {
		this.dataFineValid = dataFineValid;
	}

	public Date getDataModifica() {
		return dataModifica;
	}

	public void setDataModifica(Date dataModifica) {
		this.dataModifica = dataModifica;
	}

	public String getUtenteModifica() {
		return utenteModifica;
	}

	public void setUtenteModifica(String utenteModifica) {
		this.utenteModifica = utenteModifica;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public Integer getBdcCanali() {
		return bdcCanali;
	}

	public void setBdcCanali(Integer bdcCanali) {
		this.bdcCanali = bdcCanali;
	}

	public Date getDataCreazione() {
		return dataCreazione;
	}

	public void setDataCreazione(Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof BdcCanaleStorico))
			return false;

		BdcCanaleStorico bdcCanali = (BdcCanaleStorico) o;

		if (id != null ? !id.equals(bdcCanali.id) : bdcCanali.id != null)
			return false;
		if (nome != null ? !nome.equals(bdcCanali.nome) : bdcCanali.nome != null)
			return false;
		return tipoCanale == bdcCanali.tipoCanale;

	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (nome != null ? nome.hashCode() : 0);
		result = 31 * result + (tipoCanale != null ? tipoCanale.hashCode() : 0);
		return result;
	}

	public boolean isSpecialRadio() {
		return this.specialRadio > 0;
	}
}