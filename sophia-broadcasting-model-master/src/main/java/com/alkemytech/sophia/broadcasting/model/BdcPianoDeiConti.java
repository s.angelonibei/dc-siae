package com.alkemytech.sophia.broadcasting.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;
import java.util.Date;

@XmlRootElement
@Entity
@Table(name = "BDC_PIANO_DEI_CONTI")
public class BdcPianoDeiConti implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="ID_BDC_PIANO_DEI_CONTI", nullable=false)
    private Long idBdcPianoDeiConti;

    @Column(name="DATA_CREAZIONE", nullable=false)
    private Date dataCreazione;

    @Column(name="ATTIVO", nullable=false)
    private Boolean attivo;

    @Column(name="UTENTE", nullable=false)
    private String utente;

    @Column(name="DESCRIZIONE", nullable=false)
    private String descrizione;

    @ManyToOne
    @JoinColumn(name = "CODICE_CONTO")
    private BdcConto codiceConto;

    @OneToMany(mappedBy = "pianoDeiConti")
    private List<BdcDettaglioPianoDeiConti> dettaglioPianoDeiConti;


    public Long getIdBdcPianoDeiConti() {
        return idBdcPianoDeiConti;
    }

    public void setIdBdcPianoDeiConti(Long idBdcPianoDeiConti) {
        this.idBdcPianoDeiConti = idBdcPianoDeiConti;
    }

    public Date getDataCreazione() {
        return dataCreazione;
    }

    public void setDataCreazione(Date dataCreazione) {
        this.dataCreazione = dataCreazione;
    }

    public Boolean getAttivo() {
        return attivo;
    }

    public void setAttivo(Boolean attivo) {
        this.attivo = attivo;
    }

    public String getUtente() {
        return utente;
    }

    public void setUtente(String utente) {
        this.utente = utente;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public BdcConto getCodiceConto() {
        return codiceConto;
    }

    public void setCodiceConto(BdcConto codiceConto) {
        this.codiceConto = codiceConto;
    }

    public List<BdcDettaglioPianoDeiConti> getDettaglioPianoDeiConti() {
        return dettaglioPianoDeiConti;
    }

    public void setDettaglioPianoDeiConti(List<BdcDettaglioPianoDeiConti> dettaglioPianoDeiConti) {
        this.dettaglioPianoDeiConti = dettaglioPianoDeiConti;
    }

    @Override
    public String toString() {
        return "BdcPianoDeiConti{" +
                "idBdcPianoDeiConti=" + idBdcPianoDeiConti +
                ", dataCreazione=" + dataCreazione +
                ", attivo=" + attivo +
                ", utente='" + utente + '\'' +
                ", descrizione='" + descrizione + '\'' +
                ", codiceConto=" + codiceConto +
                ", dettaglioPianoDeiConti=" + dettaglioPianoDeiConti +
                '}';
    }
}
