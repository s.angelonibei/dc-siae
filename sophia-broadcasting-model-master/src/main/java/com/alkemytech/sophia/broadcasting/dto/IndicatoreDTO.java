package com.alkemytech.sophia.broadcasting.dto;

import java.util.Date;

public class IndicatoreDTO {

    private String indicatore;
    private long valore;
    private Date lastUpdate;

    public IndicatoreDTO() {
    }

    public IndicatoreDTO(String indicatore, long valore, Date lastUpdate) {
        this.indicatore = indicatore;
        this.valore = valore;
        this.lastUpdate = lastUpdate;
    }

    public String getIndicatore() {
        return indicatore;
    }

    public void setIndicatore(String indicatore) {
        this.indicatore = indicatore;
    }

    public long getValore() {
        return valore;
    }

    public void setValore(long valore) {
        this.valore = valore;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
}
