package com.alkemytech.sophia.broadcasting.service;

import com.alkemytech.sophia.broadcasting.dto.*;
import com.alkemytech.sophia.broadcasting.model.BdcBroadcasters;
import com.alkemytech.sophia.broadcasting.model.BdcCanali;
import com.alkemytech.sophia.broadcasting.model.TipoBroadcaster;
import com.alkemytech.sophia.broadcasting.model.utilizations.*;
import com.alkemytech.sophia.broadcasting.service.interfaces.BdcFileArmonizzatoService;
import com.alkemytech.sophia.broadcasting.utils.EntityToFieldConverter;
import com.google.inject.Inject;
import com.google.inject.Provider;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BdcFileArmonizzatoServiceImpl implements BdcFileArmonizzatoService {

	private final Provider<EntityManager> provider;
	Logger logger = LoggerFactory.getLogger(getClass());

	public static String getValuesCollection(List<Integer> list) {
		String result = "";
		String valueSeparator = " , ";
		for (Integer e : list) {
			result = result.concat("").concat(e.toString()).concat("").concat(valueSeparator);
		}
		result = result.substring(0, result.lastIndexOf(valueSeparator));

		return result;
	}

	@Inject
	public BdcFileArmonizzatoServiceImpl(Provider<EntityManager> provider) {
		this.provider = provider;
	}

	@Override
	public List<FileArmonizzatoTvDTO> getFileArmonizzato(BdcBroadcasters broadcasters, List<BdcCanali> canali,
														 Integer anno, List<Integer> mesi) {

		EntityManager entityManager = provider.get();
		//List<FileArmonizzatoTvDTO> fileArmonizzatoTvDTOList = new ArrayList<>();

		List<Integer> selectedValues = new ArrayList<Integer>();
		for (BdcCanali bdcCanale : canali) {
			selectedValues.add(bdcCanale.getId());
		}

		String	 validi = ""
					+ "SELECT    new com.alkemytech.sophia.broadcasting.dto.FileArmonizzatoTvDTO("
					+ "          canali.nome, "
					+ "          CAST(tvShowSchedule.idShowType AS CHAR), "
					+ "          tvShowSchedule.title , "
					+ "          tvShowMusic.originalTitle , "
					+ "          CAST(FUNCTION( 'DATE_FORMAT', tvShowSchedule.beginTime, '%d/%m/%Y' ) AS CHAR), "
					+ "          CAST(FUNCTION( 'DATE_FORMAT', tvShowSchedule.beginTime, '%H:%i:%s' ) AS CHAR), "
					+ "          CAST(FUNCTION( 'DATE_FORMAT', tvShowSchedule.endTime, '%H:%i:%s' ) AS CHAR), "
//			+ "          CAST(FUNCTION( 'SEC_TO_TIME', tvShowSchedule.duration ) AS CHAR), "
					+ "          CAST(FUNCTION( 'SEC_TO_TIME', case when tvShowMusic.idMusicType=4 then tvShowMusic.duration else tvShowSchedule.duration end ) AS CHAR), "
					+ "          tvShowMusic.replica, "
					+ "          tvShowMusic.prodCountry, "
					+ "          tvShowMusic.prodYear, "
					+ "          tvShowMusic.productor, "
					+ "          tvShowMusic.director, "
					+ "          tvShowMusic.episodeTitle, "
					+ "          tvShowMusic.episodeOriginalTitle, "
					+ "          CAST(tvShowMusic.episodeNumber AS CHAR), "
					+ "          tvShowMusic.title, "
					+ "          tvShowMusic.firstComposer, "
					+ "          tvShowMusic.secondComposer, "
//		    + "          FUNCTION( 'SEC_TO_TIME', tvShowMusic.duration ), "
					+ "          CAST(FUNCTION( 'SEC_TO_TIME', tvShowMusic.realDuration ) AS CHAR), "
					+ "          CAST(tvShowMusic.idMusicType AS CHAR), "
					+ "          CASE "
					+ "                    WHEN tvShowSchedule.regionalOffice IS NULL THEN 0 "
					+ "                    ELSE 1 "
					+ "          END "
					+ ",          COALESCE(tvShowMusic.alert,''), "
					//R21-06
					+ " 		 utilizationFile.nomeFile, "
					+ "CAST(FUNCTION( 'DATE_FORMAT', utilizationFile.dataUpload,'%Y-%m-%d' ) AS CHAR), "
					+ "CAST(FUNCTION( 'DATE_FORMAT', utilizationFile.dataUpload,'%H:%i:%S' ) AS CHAR)"
					+ ")"
					+ "FROM      Canale canali, "
					//R21-06
					+ "UtilizationFile utilizationFile "
					+ "JOIN utilizationFile.bdcUtilizationNormalizedFiles utilizationNormalizedFile "
					+ "JOIN 	 canali.bdcTvShowSchedules tvShowSchedule "
					//+ "JOIN 	 UtilizationFile.bdcUtilizationNormalizedFiles "
					+ "LEFT JOIN      tvShowSchedule.bdcTvShowMusics tvShowMusic "
					+ "WHERE     tvShowSchedule.bdcCanali.id IN :listaCanali "
					+ "AND       tvShowSchedule.scheduleYear = :anno "
					+ "AND       tvShowSchedule.scheduleMonth IN :mesi "
					+ "       AND canali.bdcBroadcaster.id = :broadcaster "
					//R21-06
					+ "AND tvShowMusic.idNormalizedFile = utilizationNormalizedFile.idNormalizedFile "
					//+ "AND bunf.bdcUtilizationFile = UtilizationFile "
					//
					+ "GROUP BY  tvShowMusic.idTvShowMusic "
					+ "ORDER BY  CONCAT( "
					+ "          CASE "
					+ "                    WHEN FUNCTION( 'DATE_FORMAT', tvShowSchedule.beginTime, '%Y/%m/%d' ) IS NULL THEN '' "
					+ "                    ELSE FUNCTION( 'DATE_FORMAT', tvShowSchedule.beginTime, '%Y/%m/%d' ) "
					+ "          END, "
					+ "          CASE "
					+ "                    WHEN FUNCTION( 'DATE_FORMAT', tvShowSchedule.beginTime, '%H:%i:%s' ) IS NULL THEN '' "
					+ "                    ELSE FUNCTION( 'DATE_FORMAT', tvShowSchedule.beginTime, '%H:%i:%s' ) "
					+ "          END ) ASC";

		String errori = ""
					+ "SELECT new com.alkemytech.sophia.broadcasting.dto.FileArmonizzatoTvDTO("
					+ "          CASE "
					+ "                    WHEN canali.id IS NULL THEN errTvShowSchedule.reportChannel "
					+ "                    ELSE canali.nome "
					+ "          END, "
					+ "          CAST(COALESCE(errTvShowSchedule.idShowType, errTvShowSchedule.reportShowType) AS CHAR), "
					+ "          errTvShowSchedule.title, "
					+ "          errTvShowMusic.originalTitle, "
					+ "          CAST(errTvShowSchedule.reportBeginDate AS CHAR), "
					+ "          CAST(errTvShowSchedule.reportBeginTime AS CHAR), "
					+ "          CAST(errTvShowSchedule.reportEndTime AS CHAR), "
					+ "          CAST(CASE "
					+ "                    WHEN errTvShowSchedule.duration IS NULL THEN errTvShowSchedule.reportDuration "
					+ "                    ELSE FUNCTION( 'SEC_TO_TIME', errTvShowSchedule.duration ) "
					+ "          END AS CHAR), "
					+ "          errTvShowMusic.replica, "
					+ "          errTvShowMusic.prodCountry, "
					+ "          COALESCE(errTvShowMusic.prodYear, errTvShowMusic.reportProdYear), "
					+ "          errTvShowMusic.productor, "
					+ "          errTvShowMusic.director, "
					+ "          errTvShowMusic.episodeTitle, "
					+ "          errTvShowMusic.episodeOriginalTitle, "
					+ "          CAST(COALESCE(errTvShowMusic.episodeNumber, errTvShowMusic.reportEpisodeNumber) AS CHAR), "
					+ "          errTvShowMusic.title, "
					+ "          errTvShowMusic.firstComposer, "
					+ "          errTvShowMusic.secondComposer, "
//                + "          COALESCE( errTvShowMusic.duration, errTvShowMusic.reportDuration), "
					+ "          CAST(COALESCE( FUNCTION( 'SEC_TO_TIME', errTvShowMusic.realDuration), errTvShowMusic.reportRealDuration) AS CHAR), "
					+ "          CAST(COALESCE( errTvShowMusic.idMusicType, errTvShowMusic.reportMusicType) AS CHAR), "
					+ "          CASE "
					+ "                    WHEN errTvShowSchedule.regionalOffice IS NULL THEN 0 "
					+ "                    ELSE 1 "
					+ "          END, "
					+ "          'KO', "
					+ "          errTvShowSchedule.error, "
					+ "          errTvShowSchedule.globalError, "
					//R21-06
					+ " 	utilizationFile.nomeFile, "
					+ "CAST(FUNCTION( 'DATE_FORMAT', utilizationFile.dataUpload,'%Y-%m-%d' ) AS CHAR), "
					+ "CAST(FUNCTION( 'DATE_FORMAT', utilizationFile.dataUpload,'%H:%i:%S' ) AS CHAR)"
					+ ") "
					+ "FROM UtilizationFile utilizationFile "
					//R21-06
					+ "JOIN utilizationFile.bdcUtilizationNormalizedFiles utilizationNormalizedFile "
					+ "JOIN ErrTvShowSchedule errTvShowSchedule ON "
					+ "errTvShowSchedule.idNormalizedFile = utilizationNormalizedFile.idNormalizedFile "
					+ "JOIN errTvShowSchedule.bdcErrTvShowMusics errTvShowMusic "
					+ "LEFT JOIN errTvShowSchedule.bdcCanali canali "
					+ "WHERE     (( "
					+ "                              canali.id IN :listaCanali "
					+ "                    AND       utilizationFile.bdcCanali IS NULL ) "
					+ "          OR        utilizationFile.bdcCanali.id IN :listaCanali) "
					+ "AND       (( "
					+ "                              errTvShowSchedule.scheduleYear = :anno "
					+ "                    AND       utilizationFile.anno IS NULL ) "
					+ "          OR        utilizationFile.anno = :anno ) "
					+ "AND       (( "
					+ "                              errTvShowSchedule.scheduleMonth IN :mesi "
					+ "                    AND       utilizationFile.mese IS NULL ) "
					+ "          OR        utilizationFile.mese IN :mesi) "
					+ "       AND utilizationFile.bdcBroadcaster.id = :broadcaster "
					//R21-06
					//+ "AND utilizationNormalizedFile.bdcUtilizationFile = utilizationFile.id "
					+ "GROUP BY  errTvShowMusic.idErrTvShowMusic "
					+ "ORDER BY  CONCAT( COALESCE( COALESCE(FUNCTION('DATE_FORMAT',errTvShowSchedule.beginTime, '%Y/%m/%d' ), errTvShowSchedule.reportBeginDate), ''), COALESCE( COALESCE(FUNCTION('DATE_FORMAT',errTvShowSchedule.beginTime, '%H:%i:%s' ), errTvShowSchedule.reportBeginTime), '')) ASC";


		List<FileArmonizzatoTvDTO> recordValidi = (List<FileArmonizzatoTvDTO>)
				entityManager.createQuery(validi, FileArmonizzatoTvDTO.class)
						.setParameter("listaCanali", selectedValues)
						.setParameter("anno", anno)
						.setParameter("mesi", mesi)
						.setParameter("broadcaster", broadcasters.getId())
						.getResultList();

		List<FileArmonizzatoTvDTO> recordErrati = (List<FileArmonizzatoTvDTO>)
				entityManager.createQuery(errori, FileArmonizzatoTvDTO.class)
						.setParameter("listaCanali", selectedValues)
						.setParameter("anno", anno)
						.setParameter("mesi", mesi)
						.setParameter("broadcaster", broadcasters.getId())
						.getResultList();

		recordValidi.addAll(recordErrati);

		return recordValidi;

	}

	@Override
	public List<FileNewTracciatoArmonizzatoTvDTO> getFileArmonizzatoNewTracciato(BdcBroadcasters broadcasters, List<BdcCanali> canali,
																				 Integer anno, List<Integer> mesi) {

		EntityManager entityManager = provider.get();
		//List<FileArmonizzatoTvDTO> fileArmonizzatoTvDTOList = new ArrayList<>();

		List<Integer> selectedValues = new ArrayList<Integer>();
		for (BdcCanali bdcCanale : canali) {
			selectedValues.add(bdcCanale.getId());
		}

		String validi = ""
				+ "SELECT    new com.alkemytech.sophia.broadcasting.dto.FileNewTracciatoArmonizzatoTvDTO("
				+ "          canali.nome, "
				+ "          CAST(tvShowSchedule.idShowType AS CHAR), "
				+ "          tvShowSchedule.title , "
				+ "          tvShowMusic.originalTitle , "
				+ "          CAST(FUNCTION( 'DATE_FORMAT', tvShowSchedule.beginTime, '%d/%m/%Y' ) AS CHAR), "
				+ "          CAST(FUNCTION( 'DATE_FORMAT', tvShowSchedule.beginTime, '%H:%i:%s' ) AS CHAR), "
				+ "          CAST(FUNCTION( 'DATE_FORMAT', tvShowSchedule.endTime, '%H:%i:%s' ) AS CHAR), "
//			+ "          CAST(FUNCTION( 'SEC_TO_TIME', tvShowSchedule.duration ) AS CHAR), "
				+ "          CAST(FUNCTION( 'SEC_TO_TIME', case when tvShowMusic.idMusicType=4 then tvShowMusic.duration else tvShowSchedule.duration end ) AS CHAR), "
				+ "          tvShowMusic.replica, "
				+ "          tvShowMusic.prodCountry, "
				+ "          tvShowMusic.prodYear, "
				+ "          tvShowMusic.productor, "
				+ "          tvShowMusic.director, "
				+ "          tvShowMusic.episodeTitle, "
				+ "          tvShowMusic.episodeOriginalTitle, "
				+ "          CAST(tvShowMusic.episodeNumber AS CHAR), "
				+ "          tvShowMusic.title, "
				+ "          tvShowMusic.firstComposer, "
				+ "          tvShowMusic.secondComposer, "
//		    + "          FUNCTION( 'SEC_TO_TIME', tvShowMusic.duration ), "
				+ "          CAST(FUNCTION( 'SEC_TO_TIME', tvShowMusic.realDuration ) AS CHAR), "
				+ "          CAST(tvShowMusic.idMusicType AS CHAR), "
				+ "          CASE "
				+ "                    WHEN tvShowSchedule.regionalOffice IS NULL THEN 0 "
				+ "                    ELSE 1 "
				+ "          END "
				+ ",          COALESCE(tvShowMusic.alert,''), "
				//R21-06
				+ " 		 utilizationFile.nomeFile, "
				+ "CAST(FUNCTION( 'DATE_FORMAT', utilizationFile.dataUpload,'%Y-%m-%d' ) AS CHAR), "
				+ "CAST(FUNCTION( 'DATE_FORMAT', utilizationFile.dataUpload,'%H:%i:%S' ) AS CHAR)"
				+ ",tvShowMusic.identificativoRai"
				+ ")"
				+ "FROM      Canale canali, "
				//R21-06
				+ "UtilizationFile utilizationFile "
				+ "JOIN utilizationFile.bdcUtilizationNormalizedFiles utilizationNormalizedFile "
				+ "JOIN 	 canali.bdcTvShowSchedules tvShowSchedule "
				//+ "JOIN 	 UtilizationFile.bdcUtilizationNormalizedFiles "
				+ "LEFT JOIN      tvShowSchedule.bdcTvShowMusics tvShowMusic "
				+ "WHERE     tvShowSchedule.bdcCanali.id IN :listaCanali "
				+ "AND       tvShowSchedule.scheduleYear = :anno "
				+ "AND       tvShowSchedule.scheduleMonth IN :mesi "
				+ "       AND canali.bdcBroadcaster.id = :broadcaster "
				//R21-06
				+ "AND tvShowMusic.idNormalizedFile = utilizationNormalizedFile.idNormalizedFile "
				//+ "AND bunf.bdcUtilizationFile = UtilizationFile "
				//
				+ "GROUP BY  tvShowMusic.idTvShowMusic "
				+ "ORDER BY  CONCAT( "
				+ "          CASE "
				+ "                    WHEN FUNCTION( 'DATE_FORMAT', tvShowSchedule.beginTime, '%Y/%m/%d' ) IS NULL THEN '' "
				+ "                    ELSE FUNCTION( 'DATE_FORMAT', tvShowSchedule.beginTime, '%Y/%m/%d' ) "
				+ "          END, "
				+ "          CASE "
				+ "                    WHEN FUNCTION( 'DATE_FORMAT', tvShowSchedule.beginTime, '%H:%i:%s' ) IS NULL THEN '' "
				+ "                    ELSE FUNCTION( 'DATE_FORMAT', tvShowSchedule.beginTime, '%H:%i:%s' ) "
				+ "          END ) ASC";

		String errori = ""
				+ "SELECT new com.alkemytech.sophia.broadcasting.dto.FileNewTracciatoArmonizzatoTvDTO("
				+ "          CASE "
				+ "                    WHEN canali.id IS NULL THEN errTvShowSchedule.reportChannel "
				+ "                    ELSE canali.nome "
				+ "          END, "
				+ "          CAST(COALESCE(errTvShowSchedule.idShowType, errTvShowSchedule.reportShowType) AS CHAR), "
				+ "          errTvShowSchedule.title, "
				+ "          errTvShowMusic.originalTitle, "
				+ "          CAST(errTvShowSchedule.reportBeginDate AS CHAR), "
				+ "          CAST(errTvShowSchedule.reportBeginTime AS CHAR), "
				+ "          CAST(errTvShowSchedule.reportEndTime AS CHAR), "
				+ "          CAST(CASE "
				+ "                    WHEN errTvShowSchedule.duration IS NULL THEN errTvShowSchedule.reportDuration "
				+ "                    ELSE FUNCTION( 'SEC_TO_TIME', errTvShowSchedule.duration ) "
				+ "          END AS CHAR), "
				+ "          errTvShowMusic.replica, "
				+ "          errTvShowMusic.prodCountry, "
				+ "          COALESCE(errTvShowMusic.prodYear, errTvShowMusic.reportProdYear), "
				+ "          errTvShowMusic.productor, "
				+ "          errTvShowMusic.director, "
				+ "          errTvShowMusic.episodeTitle, "
				+ "          errTvShowMusic.episodeOriginalTitle, "
				+ "          CAST(COALESCE(errTvShowMusic.episodeNumber, errTvShowMusic.reportEpisodeNumber) AS CHAR), "
				+ "          errTvShowMusic.title, "
				+ "          errTvShowMusic.firstComposer, "
				+ "          errTvShowMusic.secondComposer, "
//                + "          COALESCE( errTvShowMusic.duration, errTvShowMusic.reportDuration), "
				+ "          CAST(COALESCE( FUNCTION( 'SEC_TO_TIME', errTvShowMusic.realDuration), errTvShowMusic.reportRealDuration) AS CHAR), "
				+ "          CAST(COALESCE( errTvShowMusic.idMusicType, errTvShowMusic.reportMusicType) AS CHAR), "
				+ "          CASE "
				+ "                    WHEN errTvShowSchedule.regionalOffice IS NULL THEN 0 "
				+ "                    ELSE 1 "
				+ "          END, "
				+ "          'KO', "
				+ "          errTvShowSchedule.error, "
				+ "          errTvShowSchedule.globalError, "
				//R21-06
				+ " 	utilizationFile.nomeFile, "
				+ "CAST(FUNCTION( 'DATE_FORMAT', utilizationFile.dataUpload,'%Y-%m-%d' ) AS CHAR), "
				+ "CAST(FUNCTION( 'DATE_FORMAT', utilizationFile.dataUpload,'%H:%i:%S' ) AS CHAR)"
				+ ",errTvShowMusic.identificativoRai"
				+ ") "
				+ "FROM UtilizationFile utilizationFile "
				//R21-06
				+ "JOIN utilizationFile.bdcUtilizationNormalizedFiles utilizationNormalizedFile "
				+ "JOIN ErrTvShowSchedule errTvShowSchedule ON "
				+ "errTvShowSchedule.idNormalizedFile = utilizationNormalizedFile.idNormalizedFile "
				+ "JOIN errTvShowSchedule.bdcErrTvShowMusics errTvShowMusic "
				+ "LEFT JOIN errTvShowSchedule.bdcCanali canali "
				+ "WHERE     (( "
				+ "                              canali.id IN :listaCanali "
				+ "                    AND       utilizationFile.bdcCanali IS NULL ) "
				+ "          OR        utilizationFile.bdcCanali.id IN :listaCanali) "
				+ "AND       (( "
				+ "                              errTvShowSchedule.scheduleYear = :anno "
				+ "                    AND       utilizationFile.anno IS NULL ) "
				+ "          OR        utilizationFile.anno = :anno ) "
				+ "AND       (( "
				+ "                              errTvShowSchedule.scheduleMonth IN :mesi "
				+ "                    AND       utilizationFile.mese IS NULL ) "
				+ "          OR        utilizationFile.mese IN :mesi) "
				+ "       AND utilizationFile.bdcBroadcaster.id = :broadcaster "
				//R21-06
				//+ "AND utilizationNormalizedFile.bdcUtilizationFile = utilizationFile.id "
				+ "GROUP BY  errTvShowMusic.idErrTvShowMusic "
				+ "ORDER BY  CONCAT( COALESCE( COALESCE(FUNCTION('DATE_FORMAT',errTvShowSchedule.beginTime, '%Y/%m/%d' ), errTvShowSchedule.reportBeginDate), ''), COALESCE( COALESCE(FUNCTION('DATE_FORMAT',errTvShowSchedule.beginTime, '%H:%i:%s' ), errTvShowSchedule.reportBeginTime), '')) ASC";


		List<FileNewTracciatoArmonizzatoTvDTO> recordValidi = entityManager.createQuery(validi, FileNewTracciatoArmonizzatoTvDTO.class)
				.setParameter("listaCanali", selectedValues)
				.setParameter("anno", anno)
				.setParameter("mesi", mesi)
				.setParameter("broadcaster", broadcasters.getId())
				.getResultList();

		List<FileNewTracciatoArmonizzatoTvDTO> recordErrati = entityManager.createQuery(errori, FileNewTracciatoArmonizzatoTvDTO.class)
				.setParameter("listaCanali", selectedValues)
				.setParameter("anno", anno)
				.setParameter("mesi", mesi)
				.setParameter("broadcaster", broadcasters.getId())
				.getResultList();

		recordValidi.addAll(recordErrati);

		return recordValidi;

	}

	@Override
	public List<FileArmonizzatoRadioDTO> getFileArmonizzatoRadio(BdcBroadcasters broadcasters, List<BdcCanali> canali,
																 Integer anno, List<Integer> mesi) {
		EntityManager entityManager = provider.get();

		List<Integer> selectedValues = new ArrayList<Integer>();
		for (BdcCanali bdcCanale : canali) {
			selectedValues.add(bdcCanale.getId());
		}

		String validi = ""
					+ "SELECT new com.alkemytech.sophia.broadcasting.dto.FileArmonizzatoRadioDTO("
					+ "		  canali.nome, "
					+ "       rdShowSchedule.title, "
					+ "       FUNCTION('DATE_FORMAT',rdShowSchedule.beginTime, '%d/%m/%Y'), "
					+ "       FUNCTION('DATE_FORMAT',rdShowSchedule.beginTime, '%H:%i:%s'), "
					+ "       FUNCTION('SEC_TO_TIME', rdShowSchedule.duration), "
					+ "       rdShowMusic.title, "
					+ "       rdShowMusic.composer, "
					+ "       rdShowMusic.performer, "
					+ "       rdShowMusic.album, "
					+ "       FUNCTION('SEC_TO_TIME', rdShowMusic.duration ), "
					+ "       rdShowMusic.iswcCode, "
					+ "       rdShowMusic.isrcCode, "
					+ "       FUNCTION('DATE_FORMAT',rdShowMusic.beginTime, '%H:%i:%s'), "
					+ "       rdShowMusic.idMusicType, "
					+ "       CASE WHEN rdShowSchedule.regionalOffice IS NULL THEN 0 ELSE 1 END "
					+ ",          COALESCE(rdShowMusic.alert,''), "
					+ " 	  utilizationFile.nomeFile, "
					+ "CAST(FUNCTION( 'DATE_FORMAT', utilizationFile.dataUpload,'%Y-%m-%d' ) AS CHAR), "
				+"CAST(FUNCTION( 'DATE_FORMAT', utilizationFile.dataUpload,'%H:%i:%S' ) AS CHAR)"
					+ ")"
					+ "FROM   RdShowSchedule rdShowSchedule, "
					+ " UtilizationFile utilizationFile "
					+ "JOIN utilizationFile.bdcUtilizationNormalizedFiles utilizationNormalizedFile "
					+ "       JOIN rdShowSchedule.bdcRdShowMusics rdShowMusic "
					+ "       LEFT JOIN rdShowSchedule.bdcCanali canali "
					+ "WHERE  canali.id IN :listaCanali "
					+ "       AND rdShowSchedule.scheduleYear = :anno "
					+ "       AND rdShowSchedule.scheduleMonth IN :mesi "
					+ "       AND canali.bdcBroadcaster.id = :broadcaster "
					+ "		  AND rdShowMusic.idNormalizedFile = utilizationNormalizedFile.idNormalizedFile "
					+ "GROUP  BY rdShowMusic.idRdShowMusic "
					+ "ORDER BY CONCAT(COALESCE( FUNCTION('DATE_FORMAT',rdShowSchedule.beginTime, '%Y/%m/%d'), ''), "
					+ "          		CASE WHEN canali.specialRadio = 0 THEN '' ELSE "
					+ "                     FUNCTION('DATE_FORMAT', rdShowSchedule.beginTime, '%H:%i:%s') END ) ASC";


		String errori = ""
					+ "SELECT new com.alkemytech.sophia.broadcasting.dto.FileArmonizzatoRadioDTO("
					+ "		  CAST(CASE WHEN canali.id IS NULL THEN "
					+ "       errRdShowSchedule.reportChannel ELSE canali.nome END as CHAR), "
					+ "       errRdShowSchedule.title, "
					+ "       errRdShowSchedule.reportBeginDate, "
					+ "       errRdShowSchedule.reportBeginTime, "
					+ "       CAST(COALESCE(FUNCTION('SEC_TO_TIME',errRdShowSchedule.duration), "
					+ "       errRdShowSchedule.reportDuration) as CHAR), "
					+ "       errRdShowMusic.title, "
					+ "       errRdShowMusic.composer, "
					+ "       errRdShowMusic.performer, "
					+ "       errRdShowMusic.album, "
					+ "       CAST(COALESCE(FUNCTION('SEC_TO_TIME',errRdShowMusic.duration), "
					+ "       errRdShowMusic.realDuration) AS CHAR), "
					+ "       errRdShowMusic.iswcCode, "
					+ "       errRdShowMusic.isrcCode, "
					+ "       COALESCE(FUNCTION('DATE_FORMAT',errRdShowMusic.beginTime,'%H:%i:%s'), "
					+ "       errRdShowMusic.reportBeginTime), "
					+ "       COALESCE(errRdShowMusic.idMusicType, "
					+ "       errRdShowMusic.reportMusicType), "
					+ "       CAST(CASE WHEN errRdShowSchedule.regionalOffice IS NULL THEN 0 ELSE 1 END AS CHAR), "
					+ "       'KO', "
					+ "       errRdShowSchedule.error, "
					+ "       errRdShowSchedule.globalError, "
					+ " 	  utilizationFile.nomeFile, "
					+ "CAST(FUNCTION( 'DATE_FORMAT', utilizationFile.dataUpload,'%Y-%m-%d' ) AS CHAR), "
					+ "CAST(FUNCTION( 'DATE_FORMAT', utilizationFile.dataUpload,'%H:%i:%S' ) AS CHAR)"
					+ ")"
					+ "FROM UtilizationFile utilizationFile "
					+ "JOIN utilizationFile.bdcUtilizationNormalizedFiles utilizationNormalizedFile "
					+ "JOIN ErrRdShowSchedule errRdShowSchedule ON "
					+ "errRdShowSchedule.idNormalizedFile = utilizationNormalizedFile.idNormalizedFile "
					+ "JOIN errRdShowSchedule.bdcErrRdShowMusics errRdShowMusic "
					+ "LEFT JOIN errRdShowSchedule.bdcCanali canali "
					+ "WHERE  ( ( canali.id IN :listaCanali "
					+ "           AND utilizationFile.bdcCanali IS NULL ) "
					+ "          OR utilizationFile.bdcCanali.id IN :listaCanali ) "
					+ "       AND ( ( errRdShowSchedule.scheduleYear = :anno "
					+ "               AND utilizationFile.anno IS NULL ) "
					+ "              OR ( utilizationFile.anno = :anno ) ) "
					+ "       AND ( ( errRdShowSchedule.scheduleMonth IN :mesi "
					+ "               AND utilizationFile.mese IS NULL ) "
					+ "              OR utilizationFile.mese IN :mesi ) "
					+ "       AND utilizationFile.bdcBroadcaster.id = :broadcaster "
					+ "GROUP  BY errRdShowMusic.idErrRdShowMusic "
					+ "ORDER BY CONCAT( COALESCE( FUNCTION('DATE_FORMAT',errRdShowSchedule.beginTime, '%Y/%m/%d' ), "
					+ "errRdShowSchedule.reportBeginDate, '' ), COALESCE( CASE WHEN "
					+ "canali.specialRadio = 0 THEN NULL ELSE COALESCE( FUNCTION('DATE_FORMAT', "
					+ "errRdShowSchedule.beginTime, '%H:%i:%s' ), "
					+ "errRdShowSchedule.reportBeginTime ) END, '' ) ) ASC";

			logger.debug( " --------> COUNT VALIDI: {}", validi.length());
			logger.debug( " --------> COUNT ERRORI: {}",  errori.length());
		/*List<Object[]> result = (List<Object[]>)
				entityManager.createQuery(validi)
						.setParameter("listaCanali", selectedValues)
						.setParameter("anno", anno)
						.setParameter("mesi", mesi)
						.getResultList();

		List<FileArmonizzatoRadioDTO> fileArmonizzatoRadioDTOS = new ArrayList<>();

		for (Object[] object : result) {
			fileArmonizzatoRadioDTOS.add(new FileArmonizzatoRadioDTO(object));
		}*/

		List<FileArmonizzatoRadioDTO> recordValidi = (List<FileArmonizzatoRadioDTO>)
				entityManager.createQuery(validi, FileArmonizzatoRadioDTO.class)
						.setParameter("listaCanali", selectedValues)
						.setParameter("anno", anno)
						.setParameter("mesi", mesi)
						.setParameter("broadcaster", broadcasters.getId())
						.getResultList();


		List<FileArmonizzatoRadioDTO> recordErrati = (List<FileArmonizzatoRadioDTO>)
				entityManager.createQuery(errori, FileArmonizzatoRadioDTO.class)
						.setParameter("listaCanali", selectedValues)
						.setParameter("anno", anno)
						.setParameter("mesi", mesi)
						.setParameter("broadcaster", broadcasters.getId())
						.getResultList();

		recordValidi.addAll(recordErrati);

		return recordValidi;
	}

	@Override
	public List<FileNewTracciatoArmonizzatoRadioDTO> getFileArmonizzatoRadioNewTracciato(BdcBroadcasters broadcasters, List<BdcCanali> canali,
																						 Integer anno, List<Integer> mesi) {
		EntityManager entityManager = provider.get();

		List<Integer> selectedValues = new ArrayList<Integer>();
		for (BdcCanali bdcCanale : canali) {
			selectedValues.add(bdcCanale.getId());
		}

		String validi = ""
				+ "SELECT new com.alkemytech.sophia.broadcasting.dto.FileNewTracciatoArmonizzatoRadioDTO("
				+ "		  canali.nome, "
				+ "       rdShowSchedule.title, "
				+ "       FUNCTION('DATE_FORMAT',rdShowSchedule.beginTime, '%d/%m/%Y'), "
				+ "       FUNCTION('DATE_FORMAT',rdShowSchedule.beginTime, '%H:%i:%s'), "
				+ "       FUNCTION('SEC_TO_TIME', rdShowSchedule.duration), "
				+ "       rdShowMusic.title, "
				+ "       rdShowMusic.composer, "
				+ "       rdShowMusic.performer, "
				+ "       rdShowMusic.album, "
				+ "       FUNCTION('SEC_TO_TIME', rdShowMusic.duration ), "
				+ "       rdShowMusic.iswcCode, "
				+ "       rdShowMusic.isrcCode, "
				+ "       FUNCTION('DATE_FORMAT',rdShowMusic.beginTime, '%H:%i:%s'), "
				+ "       rdShowMusic.idMusicType, "
				+ "       CASE WHEN rdShowSchedule.regionalOffice IS NULL THEN 0 ELSE 1 END "
				+ ",          COALESCE(rdShowMusic.alert,''), "
				+ " 	  utilizationFile.nomeFile, "
				+ "CAST(FUNCTION( 'DATE_FORMAT', utilizationFile.dataUpload,'%Y-%m-%d' ) AS CHAR), "
				+"CAST(FUNCTION( 'DATE_FORMAT', utilizationFile.dataUpload,'%H:%i:%S' ) AS CHAR)," +
				"rdShowMusic.identificativoRai"
				+ ")"
				+ "FROM   RdShowSchedule rdShowSchedule, "
				+ " UtilizationFile utilizationFile "
				+ "JOIN utilizationFile.bdcUtilizationNormalizedFiles utilizationNormalizedFile "
				+ "       JOIN rdShowSchedule.bdcRdShowMusics rdShowMusic "
				+ "       LEFT JOIN rdShowSchedule.bdcCanali canali "
				+ "WHERE  canali.id IN :listaCanali "
				+ "       AND rdShowSchedule.scheduleYear = :anno "
				+ "       AND rdShowSchedule.scheduleMonth IN :mesi "
				+ "       AND canali.bdcBroadcaster.id = :broadcaster "
				+ "		  AND rdShowMusic.idNormalizedFile = utilizationNormalizedFile.idNormalizedFile "
				+ "GROUP  BY rdShowMusic.idRdShowMusic "
				+ "ORDER BY CONCAT(COALESCE( FUNCTION('DATE_FORMAT',rdShowSchedule.beginTime, '%Y/%m/%d'), ''), "
				+ "          		CASE WHEN canali.specialRadio = 0 THEN '' ELSE "
				+ "                     FUNCTION('DATE_FORMAT', rdShowSchedule.beginTime, '%H:%i:%s') END ) ASC";


		String errori = ""
				+ "SELECT new com.alkemytech.sophia.broadcasting.dto.FileNewTracciatoArmonizzatoRadioDTO("
				+ "		  CAST(CASE WHEN canali.id IS NULL THEN "
				+ "       errRdShowSchedule.reportChannel ELSE canali.nome END as CHAR), "
				+ "       errRdShowSchedule.title, "
				+ "       errRdShowSchedule.reportBeginDate, "
				+ "       errRdShowSchedule.reportBeginTime, "
				+ "       CAST(COALESCE(FUNCTION('SEC_TO_TIME',errRdShowSchedule.duration), "
				+ "       errRdShowSchedule.reportDuration) as CHAR), "
				+ "       errRdShowMusic.title, "
				+ "       errRdShowMusic.composer, "
				+ "       errRdShowMusic.performer, "
				+ "       errRdShowMusic.album, "
				+ "       CAST(COALESCE(FUNCTION('SEC_TO_TIME',errRdShowMusic.duration), "
				+ "       errRdShowMusic.realDuration) AS CHAR), "
				+ "       errRdShowMusic.iswcCode, "
				+ "       errRdShowMusic.isrcCode, "
				+ "       COALESCE(FUNCTION('DATE_FORMAT',errRdShowMusic.beginTime,'%H:%i:%s'), "
				+ "       errRdShowMusic.reportBeginTime), "
				+ "       COALESCE(errRdShowMusic.idMusicType, "
				+ "       errRdShowMusic.reportMusicType), "
				+ "       CAST(CASE WHEN errRdShowSchedule.regionalOffice IS NULL THEN 0 ELSE 1 END AS CHAR), "
				+ "       'KO', "
				+ "       errRdShowSchedule.error, "
				+ "       errRdShowSchedule.globalError, "
				+ " 	  utilizationFile.nomeFile, "
				+ "CAST(FUNCTION( 'DATE_FORMAT', utilizationFile.dataUpload,'%Y-%m-%d' ) AS CHAR), "
				+ "CAST(FUNCTION( 'DATE_FORMAT', utilizationFile.dataUpload,'%H:%i:%S' ) AS CHAR)"
				+ ",errRdShowMusic.identificativoRai"
				+ ")"
				+ "FROM UtilizationFile utilizationFile "
				+ "JOIN utilizationFile.bdcUtilizationNormalizedFiles utilizationNormalizedFile "
				+ "JOIN ErrRdShowSchedule errRdShowSchedule ON "
				+ "errRdShowSchedule.idNormalizedFile = utilizationNormalizedFile.idNormalizedFile "
				+ "JOIN errRdShowSchedule.bdcErrRdShowMusics errRdShowMusic "
				+ "LEFT JOIN errRdShowSchedule.bdcCanali canali "
				+ "WHERE  ( ( canali.id IN :listaCanali "
				+ "           AND utilizationFile.bdcCanali IS NULL ) "
				+ "          OR utilizationFile.bdcCanali.id IN :listaCanali ) "
				+ "       AND ( ( errRdShowSchedule.scheduleYear = :anno "
				+ "               AND utilizationFile.anno IS NULL ) "
				+ "              OR ( utilizationFile.anno = :anno ) ) "
				+ "       AND ( ( errRdShowSchedule.scheduleMonth IN :mesi "
				+ "               AND utilizationFile.mese IS NULL ) "
				+ "              OR utilizationFile.mese IN :mesi ) "
				+ "       AND utilizationFile.bdcBroadcaster.id = :broadcaster "
				+ "GROUP  BY errRdShowMusic.idErrRdShowMusic "
				+ "ORDER BY CONCAT( COALESCE( FUNCTION('DATE_FORMAT',errRdShowSchedule.beginTime, '%Y/%m/%d' ), "
				+ "errRdShowSchedule.reportBeginDate, '' ), COALESCE( CASE WHEN "
				+ "canali.specialRadio = 0 THEN NULL ELSE COALESCE( FUNCTION('DATE_FORMAT', "
				+ "errRdShowSchedule.beginTime, '%H:%i:%s' ), "
				+ "errRdShowSchedule.reportBeginTime ) END, '' ) ) ASC";

		logger.debug( " --------> COUNT VALIDI: {}", validi.length());
		logger.debug( " --------> COUNT ERRORI: {}",  errori.length());
		/*List<Object[]> result = (List<Object[]>)
				entityManager.createQuery(validi)
						.setParameter("listaCanali", selectedValues)
						.setParameter("anno", anno)
						.setParameter("mesi", mesi)
						.getResultList();

		List<FileArmonizzatoRadioDTO> fileArmonizzatoRadioDTOS = new ArrayList<>();

		for (Object[] object : result) {
			fileArmonizzatoRadioDTOS.add(new FileArmonizzatoRadioDTO(object));
		}*/

		List<FileNewTracciatoArmonizzatoRadioDTO> recordValidi = entityManager.createQuery(validi, FileNewTracciatoArmonizzatoRadioDTO.class)
				.setParameter("listaCanali", selectedValues)
				.setParameter("anno", anno)
				.setParameter("mesi", mesi)
				.setParameter("broadcaster", broadcasters.getId())
				.getResultList();


		List<FileNewTracciatoArmonizzatoRadioDTO> recordErrati = entityManager.createQuery(errori, FileNewTracciatoArmonizzatoRadioDTO.class)
				.setParameter("listaCanali", selectedValues)
				.setParameter("anno", anno)
				.setParameter("mesi", mesi)
				.setParameter("broadcaster", broadcasters.getId())
				.getResultList();

		recordValidi.addAll(recordErrati);

		return recordValidi;
	}

	@Override
	public List<UtilizzazioniArmonizzatoDTO> getUtilizzazioniArmonizzate(BdcBroadcasters broadcasters, BdcCanali canali,
																		 String titoloTrasmissione, String inizioTrasmissioneFrom, String inizioTrasmissioneTo) {
		EntityManager entityManager = provider.get();
		List<UtilizzazioniArmonizzatoDTO> fileArmonizzatoTvDTO = new ArrayList<>();
		List<UtilizzazioniArmonizzatoDTO> fileArmonizzatoRadioDTO = new ArrayList<>();
		if (broadcasters.getTipo_broadcaster().equals(TipoBroadcaster.TELEVISIONE)) {
			try {

				SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				Date d = sdf1.parse(inizioTrasmissioneFrom);
				Date d1 = sdf1.parse(inizioTrasmissioneTo);
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String w = "(SELECT "
						+ "bdc_canali.nome AS canale, "
						+ "BDC_TV_SHOW_SCHEDULE.ID_TV_SHOW_SCHEDULE AS id, "
						+ "BDC_SHOW_TYPE.NAME AS genereTrasmissione, "
						+ "BDC_TV_SHOW_SCHEDULE.TITLE AS titoloTrasmissione, "
						+ "BDC_TV_SHOW_SCHEDULE.ORIGINAL_TITLE AS titoloOriginaleTrasmissione, "
						+ "DATE(BDC_TV_SHOW_SCHEDULE.BEGIN_TIME) AS dataInizioTrasmissione, "
						+ "TIME(BDC_TV_SHOW_SCHEDULE.BEGIN_TIME) AS orarioInizioTrasmissione, "
						+ "TIME(BDC_TV_SHOW_SCHEDULE.END_TIME) AS orarioFineTrasmissione, "
						+ "CAST(SEC_TO_TIME(BDC_TV_SHOW_SCHEDULE.DURATION) AS CHAR(11)) AS durataTrasmissione, "
						+ "BDC_TV_SHOW_SCHEDULE.REPLICA AS replicaOPrimaEsecuzione, "
						+ "BDC_TV_SHOW_SCHEDULE.PROD_COUNTRY AS paeseDiProduzione, "
						+ "BDC_TV_SHOW_SCHEDULE.PROD_YEAR AS annoDiProduzione, "
						+ "BDC_TV_SHOW_SCHEDULE.PRODUCTOR AS produttore, "
						+ "BDC_TV_SHOW_SCHEDULE.DIRECTOR AS regista, "
						+ "BDC_TV_SHOW_SCHEDULE.EPISODE_TITLE AS titoloEpisodio, "
						+ "BDC_TV_SHOW_SCHEDULE.EPISODE_ORIGINAL_TITLE AS titoloOriginaleEpisodio, "
						+ "BDC_TV_SHOW_SCHEDULE.EPISODE_NUMBER AS numeroProgressivoEpisodio, "
						+ "BDC_TV_SHOW_SCHEDULE.REGIONAL_OFFICE AS diffusioneRegionale, " + "NULL AS Regola1, "
						+ "NULL AS Regola2, " + "NULL AS Regola3 " + "FROM BDC_TV_SHOW_SCHEDULE "
						+ "LEFT JOIN bdc_canali " + "ON " + "BDC_TV_SHOW_SCHEDULE.ID_CHANNEL = bdc_canali.id "
						+ "LEFT JOIN BDC_SHOW_TYPE " + "ON "
						+ "BDC_TV_SHOW_SCHEDULE.ID_SHOW_TYPE=BDC_SHOW_TYPE.ID_SHOW_TYPE "
						+ "WHERE BDC_TV_SHOW_SCHEDULE.ID_CHANNEL = " + canali.getId() + " "
						+ "AND BDC_TV_SHOW_SCHEDULE.TITLE LIKE '%" + titoloTrasmissione + "%' "
						+ "AND BDC_TV_SHOW_SCHEDULE.BEGIN_TIME >= '" + sdf.format(d) + "' "
						+ "AND BDC_TV_SHOW_SCHEDULE.END_TIME <= '" + sdf.format(d1) + "')" + "UNION ALL"
						+ "(SELECT bdc_canali.nome AS canale, "
						+ "BDC_ERR_TV_SHOW_SCHEDULE.ID_ERR_TV_SHOW_SCHEDULE AS id, "
						+ "BDC_SHOW_TYPE.NAME AS genereTrasmissione, "
						+ "BDC_ERR_TV_SHOW_SCHEDULE.TITLE AS titoloTrasmissione, "
						+ "BDC_ERR_TV_SHOW_SCHEDULE.ORIGINAL_TITLE AS titoloOriginaleTrasmissione, "
						+ "DATE(BDC_ERR_TV_SHOW_SCHEDULE.BEGIN_TIME) AS dataInizioTrasmissione, "
						+ "TIME(BDC_ERR_TV_SHOW_SCHEDULE.BEGIN_TIME) AS orarioInizioTrasmissione, "
						+ "TIME(BDC_ERR_TV_SHOW_SCHEDULE.END_TIME) AS orarioFineTrasmissione, "
						+ "CAST(BDC_ERR_TV_SHOW_SCHEDULE.DURATION AS CHAR(11)) AS durataTrasmissione, "
						+ "BDC_ERR_TV_SHOW_SCHEDULE.REPLICA AS replicaOPrimaEsecuzione, "
						+ "BDC_ERR_TV_SHOW_SCHEDULE.PROD_COUNTRY AS paeseDiProduzione, "
						+ "BDC_ERR_TV_SHOW_SCHEDULE.PROD_YEAR AS annoDiProduzione, "
						+ "BDC_ERR_TV_SHOW_SCHEDULE.PRODUCTOR AS produttore, "
						+ "BDC_ERR_TV_SHOW_SCHEDULE.DIRECTOR AS regista, "
						+ "BDC_ERR_TV_SHOW_SCHEDULE.EPISODE_TITLE AS titoloEpisodio, "
						+ "BDC_ERR_TV_SHOW_SCHEDULE.EPISODE_ORIGINAL_TITLE AS titoloOriginaleEpisodio, "
						+ "BDC_ERR_TV_SHOW_SCHEDULE.EPISODE_NUMBER AS numeroProgressivoEpisodio, "
						+ "BDC_ERR_TV_SHOW_SCHEDULE.REGIONAL_OFFICE AS diffusioneRegionale, "
						+ "IF (BDC_ERR_TV_SHOW_SCHEDULE.ERROR->\"$.regola1\" = JSON_ARRAY(),NULL, CONCAT(\"Campo obbligatorio: \", BDC_ERR_TV_SHOW_SCHEDULE.ERROR->\"$.regola1\")) AS Regola1, "
						+ "IF (BDC_ERR_TV_SHOW_SCHEDULE.ERROR->\"$.regola2\" = JSON_ARRAY(),NULL, CONCAT(\"Formato non supportato: \", BDC_ERR_TV_SHOW_SCHEDULE.ERROR->\"$.regola2\")) AS Regola2, "
						+ "IF (BDC_ERR_TV_SHOW_SCHEDULE.ERROR->\"$.regola3\" = JSON_ARRAY(),NULL, CONCAT(\"Valore inatteso: \", BDC_ERR_TV_SHOW_SCHEDULE.ERROR->\"$.regola3\")) AS Regola3 "
						+ "FROM BDC_ERR_TV_SHOW_SCHEDULE " + "LEFT JOIN bdc_canali " + "ON "
						+ "BDC_ERR_TV_SHOW_SCHEDULE.ID_CHANNEL = bdc_canali.id " + "LEFT JOIN BDC_SHOW_TYPE " + "ON "
						+ "BDC_ERR_TV_SHOW_SCHEDULE.ID_SHOW_TYPE=BDC_SHOW_TYPE.ID_SHOW_TYPE "
						+ "WHERE BDC_ERR_TV_SHOW_SCHEDULE.ID_CHANNEL = " + canali.getId() + " "
						+ "AND BDC_ERR_TV_SHOW_SCHEDULE.TITLE LIKE '%" + titoloTrasmissione + "%' "
						+ "AND BDC_ERR_TV_SHOW_SCHEDULE.BEGIN_TIME >= '" + sdf.format(d) + "' "
						+ "AND BDC_ERR_TV_SHOW_SCHEDULE.END_TIME <= '" + sdf.format(d1) + "')";
				final Query q = entityManager.createNativeQuery(w);
				List<Object[]> result = (List<Object[]>) q.getResultList();
				for (Object[] object : result) {
					fileArmonizzatoTvDTO.add(new UtilizzazioniArmonizzatoDTO(object, true));
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return fileArmonizzatoTvDTO;
		} else {
			try {
				SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				Date d = sdf1.parse(inizioTrasmissioneFrom);
				Date d1 = sdf1.parse(inizioTrasmissioneTo);
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String w = "(SELECT " + "bdc_canali.nome AS canale, "
						+ "BDC_RD_SHOW_SCHEDULE.ID_RD_SHOW_SCHEDULE AS id, "
						+ "BDC_RD_SHOW_SCHEDULE.TITLE AS titoloTrasmissione, "
						+ "DATE(BDC_RD_SHOW_SCHEDULE.BEGIN_TIME) AS dataInizioTrasmissione, "
						+ "TIME(BDC_RD_SHOW_SCHEDULE.BEGIN_TIME) AS orarioInizioTrasmissione, "
						+ "TIME(BDC_RD_SHOW_SCHEDULE.END_TIME) AS orarioFineTrasmissione, "
						+ "CAST(SEC_TO_TIME(BDC_RD_SHOW_SCHEDULE.DURATION) AS CHAR(11)) AS durataTrasmissione, "
						+ "BDC_RD_SHOW_MUSIC.TITLE AS titoloBrano, "
						+ "BDC_RD_SHOW_MUSIC.COMPOSER AS compositoreAutore, "
						+ "BDC_RD_SHOW_MUSIC.PERFORMER AS esecutorePerformer, " + "BDC_RD_SHOW_MUSIC.ALBUM AS album, "
						+ "CAST(SEC_TO_TIME(BDC_RD_SHOW_MUSIC.REAL_DURATION) AS CHAR(11)) AS durataBrano, "
						+ "BDC_RD_SHOW_MUSIC.ISWC_CODE AS codiceISWC, " + "BDC_RD_SHOW_MUSIC.ISRC_CODE AS codiceISRC, "
						+ "TIME(BDC_RD_SHOW_MUSIC.BEGIN_TIME) AS orarioInizioBrano, "
						+ "BDC_MUSIC_TYPE.NAME AS categoriaDUso, "
						+ "BDC_RD_SHOW_SCHEDULE.REGIONAL_OFFICE AS diffusioneRegionale, " + "NULL AS regola1, "
						+ "NULL AS regola2, " + "NULL AS regola3 " + "FROM BDC_RD_SHOW_MUSIC "
						+ "LEFT JOIN BDC_RD_SHOW_SCHEDULE " + "ON "
						+ "BDC_RD_SHOW_MUSIC.ID_RD_SHOW_SCHEDULE=BDC_RD_SHOW_SCHEDULE.ID_RD_SHOW_SCHEDULE "
						+ "LEFT JOIN bdc_canali " + "ON " + "BDC_RD_SHOW_SCHEDULE.ID_CHANNEL=bdc_canali.id "
						+ "LEFT JOIN bdc_broadcasters " + "ON " + "bdc_canali.id_broadcaster=bdc_broadcasters.id "
						+ "LEFT JOIN BDC_MUSIC_TYPE " + "ON "
						+ "BDC_RD_SHOW_MUSIC.ID_MUSIC_TYPE=BDC_MUSIC_TYPE.ID_MUSIC_TYPE "
						+ "WHERE BDC_RD_SHOW_SCHEDULE.ID_CHANNEL = " + canali.getId() + " "
						+ "AND BDC_RD_SHOW_SCHEDULE.TITLE LIKE '%" + titoloTrasmissione + "%' "
						+ "AND BDC_RD_SHOW_SCHEDULE.BEGIN_TIME >= '" + sdf.format(d) + "' "
						+ "AND BDC_RD_SHOW_SCHEDULE.END_TIME <= '" + sdf.format(d1) + "')" + "UNION ALL " + "(SELECT "
						+ "bdc_canali.nome AS canale, " + "BDC_ERR_RD_SHOW_SCHEDULE.ID_ERR_RD_SHOW_SCHEDULE AS id, "
						+ "BDC_ERR_RD_SHOW_SCHEDULE.TITLE AS titoloTrasmissione, "
						+ "DATE(BDC_ERR_RD_SHOW_SCHEDULE.BEGIN_TIME) AS dataInizioTrasmissione, "
						+ "TIME(BDC_ERR_RD_SHOW_SCHEDULE.BEGIN_TIME) AS orarioInizioTrasmissione, "
						+ "TIME(BDC_ERR_RD_SHOW_SCHEDULE.END_TIME) AS orarioFineTrasmissione, "
						+ "CAST(BDC_ERR_RD_SHOW_SCHEDULE.DURATION AS CHAR(11)) AS durataTrasmissione, "
						+ "BDC_ERR_RD_SHOW_MUSIC.TITLE AS titoloBrano, "
						+ "BDC_ERR_RD_SHOW_MUSIC.COMPOSER AS compositoreAutore, "
						+ "BDC_ERR_RD_SHOW_MUSIC.PERFORMER AS esecutorePerformer, "
						+ "BDC_ERR_RD_SHOW_MUSIC.ALBUM AS album, "
						+ "CAST(BDC_ERR_RD_SHOW_MUSIC.REAL_DURATION AS CHAR(11))  AS durataBrano, "
						+ "BDC_ERR_RD_SHOW_MUSIC.ISWC_CODE AS codiceISWC, "
						+ "BDC_ERR_RD_SHOW_MUSIC.ISRC_CODE AS codiceISRC, "
						+ "TIME(BDC_ERR_RD_SHOW_MUSIC.BEGIN_TIME) AS orarioInizioBrano, "
						+ "BDC_MUSIC_TYPE.NAME AS categoriaDUso, "
						+ "BDC_ERR_RD_SHOW_SCHEDULE.REGIONAL_OFFICE AS diffusioneRegionale, "
						+ "IF (BDC_ERR_RD_SHOW_SCHEDULE.ERROR->\"$.regola1\" = JSON_ARRAY(),NULL, CONCAT(\"Campo obbligatorio: \", BDC_ERR_RD_SHOW_SCHEDULE.ERROR->\"$.regola1\")) AS Regola1, "
						+ "IF (BDC_ERR_RD_SHOW_SCHEDULE.ERROR->\"$.regola2\" = JSON_ARRAY(),NULL, CONCAT(\"Formato non supportato: \", BDC_ERR_RD_SHOW_SCHEDULE.ERROR->\"$.regola2\")) AS Regola2, "
						+ "IF (BDC_ERR_RD_SHOW_SCHEDULE.ERROR->\"$.regola3\" = JSON_ARRAY(),NULL, CONCAT(\"Valore inatteso: \", BDC_ERR_RD_SHOW_SCHEDULE.ERROR->\"$.regola3\")) AS Regola3 "
						+ "FROM BDC_ERR_RD_SHOW_MUSIC " + "LEFT JOIN BDC_ERR_RD_SHOW_SCHEDULE " + "ON "
						+ "BDC_ERR_RD_SHOW_MUSIC.ID_ERR_RD_SHOW_SCHEDULE=BDC_ERR_RD_SHOW_SCHEDULE.ID_ERR_RD_SHOW_SCHEDULE "
						+ "LEFT JOIN bdc_canali " + "ON " + "BDC_ERR_RD_SHOW_SCHEDULE.ID_CHANNEL=bdc_canali.id "
						+ "LEFT JOIN bdc_broadcasters " + "ON " + "bdc_canali.id_broadcaster=bdc_broadcasters.id "
						+ "LEFT JOIN BDC_MUSIC_TYPE " + "ON "
						+ "BDC_ERR_RD_SHOW_MUSIC.ID_MUSIC_TYPE=BDC_MUSIC_TYPE.ID_MUSIC_TYPE "
						+ "WHERE BDC_ERR_RD_SHOW_SCHEDULE.ID_CHANNEL = " + canali.getId() + " "
						+ "AND BDC_ERR_RD_SHOW_SCHEDULE.TITLE LIKE '%" + titoloTrasmissione + "%' "
						+ "AND BDC_ERR_RD_SHOW_SCHEDULE.BEGIN_TIME >= '" + sdf.format(d) + "' "
						+ "AND BDC_ERR_RD_SHOW_SCHEDULE.END_TIME <= '" + sdf.format(d1) + "')";
				final Query q = entityManager.createNativeQuery(w);
				List<Object[]> result = (List<Object[]>) q.getResultList();
				for (Object[] object : result) {
					fileArmonizzatoRadioDTO.add(new UtilizzazioniArmonizzatoDTO(object, false));
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return fileArmonizzatoRadioDTO;
		}
	}

	@Override
	public List<BdcScheduleFieldDTO> getScheduleField(BdcBroadcasters broadcasters, Integer idShowSchedule,
													  boolean isError) {
		// TODO Auto-generated method stub

		if (broadcasters.getTipo_broadcaster().equals(TipoBroadcaster.TELEVISIONE)) {
			if (isError) {
				EntityManager em = null;
				em = provider.get();
				final Query q = em.createQuery("SELECT a FROM BdcErrTvShowSchedule a WHERE a.id=:id").setParameter("id",
						idShowSchedule);

				BdcShowMusicDTO bdcShowMusicDTO = null;
				try {
					final Query q1 = em.createQuery("SELECT a FROM BdcErrTvShowMusic a WHERE a.errTvShowSchedule.id=:id")
							.setParameter("id", idShowSchedule.longValue());

					BdcErrTvShowMusic bdcErrTvShowMusic = (BdcErrTvShowMusic) q1.getSingleResult();

					bdcShowMusicDTO = new BdcShowMusicDTO(bdcErrTvShowMusic);

				}catch (Exception e) {
					// TODO: handle exception
				}

				return EntityToFieldConverter.parseBdcErrTVShowSchedule((BdcErrTvShowSchedule) q.getSingleResult(), bdcShowMusicDTO);
			} else {
				EntityManager em = null;
				em = provider.get();

				final Query q = em.createQuery("SELECT a FROM BdcTvShowSchedule a WHERE a.id=:id").setParameter("id",
						idShowSchedule);
				return EntityToFieldConverter.parseBdcTVShowSchedule((BdcTvShowSchedule) q.getSingleResult());
			}
		} else {
			if (isError) {
				EntityManager em = null;
				em = provider.get();

				final Query q = em.createQuery("SELECT a FROM BdcErrRdShowSchedule a WHERE a.id=:id").setParameter("id",
						idShowSchedule);
				return EntityToFieldConverter.parseBdcErrRdShowSchedule((BdcErrRdShowSchedule) q.getSingleResult());
			} else {
				EntityManager em = null;
				em = provider.get();

				final Query q = em.createQuery("SELECT a FROM BdcRdShowSchedule a WHERE a.id=:id").setParameter("id",
						idShowSchedule);
				return EntityToFieldConverter.parseBdcRdShowSchedule((BdcRdShowSchedule) q.getSingleResult());
			}
		}

	}

	@Override
	public List<BdcShowMusicDTO> getShowMusic(BdcBroadcasters broadcasters, Integer idShowSchedule, boolean isError) {

		List<BdcShowMusicDTO> result = new ArrayList<>();
		if (broadcasters.getTipo_broadcaster().equals(TipoBroadcaster.TELEVISIONE)) {

			if (isError) {
				EntityManager em = null;
				em = provider.get();

				final Query q = em.createQuery("SELECT a FROM BdcErrTvShowMusic a WHERE a.errTvShowSchedule.id=:id")
						.setParameter("id", idShowSchedule.longValue());

				List<BdcErrTvShowMusic> list = (List<BdcErrTvShowMusic>) q.getResultList();
				for (BdcErrTvShowMusic bdcErrTvShowMusic : list) {
					result.add(new BdcShowMusicDTO(bdcErrTvShowMusic));
				}
				return result;
			} else {
				EntityManager em = null;
				em = provider.get();

				final Query q = em.createQuery("SELECT a FROM BdcTvShowMusic a WHERE a.tvShowSchedule.id=:id")
						.setParameter("id", idShowSchedule.longValue());

				List<BdcTvShowMusic> list = (List<BdcTvShowMusic>) q.getResultList();
				for (BdcTvShowMusic bdcTvShowMusic : list) {
					result.add(new BdcShowMusicDTO(bdcTvShowMusic));
				}
				return result;
			}
		} else {
			if (isError) {
				EntityManager em = null;
				em = provider.get();

				final Query q = em.createQuery("SELECT a FROM BdcErrRdShowMusic a WHERE a.errRdShowSchedule.id=:id")
						.setParameter("id", idShowSchedule.longValue());

				List<BdcErrRdShowMusic> list = (List<BdcErrRdShowMusic>) q.getResultList();
				for (BdcErrRdShowMusic bdcErrRdShowMusic : list) {
					result.add(new BdcShowMusicDTO(bdcErrRdShowMusic));
				}
				return result;
			} else {
				EntityManager em = null;
				em = provider.get();

				final Query q = em.createQuery("SELECT a FROM BdcRdShowMusic a WHERE a.rdShowSchedule.id=:id")
						.setParameter("id", idShowSchedule.longValue());

				List<BdcRdShowMusic> list = (List<BdcRdShowMusic>) q.getResultList();
				for (BdcRdShowMusic bdcRdShowMusic : list) {
					result.add(new BdcShowMusicDTO(bdcRdShowMusic));
				}
				return result;
			}
		}

	}

}
