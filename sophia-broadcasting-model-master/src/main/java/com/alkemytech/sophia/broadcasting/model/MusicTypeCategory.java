package com.alkemytech.sophia.broadcasting.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Alessandro Russo on 09/12/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "BDC_MUSIC_TYPE_CATEGORY")
@NamedQuery(name="MusicTypeCategory.findAll", query="SELECT m FROM MusicTypeCategory m")
public class MusicTypeCategory implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_MUSIC_TYPE_CATEGORY", nullable = false)
    public Long id;

    @ManyToOne
    @JoinColumn(name="ID_MUSIC_TYPE")
    private MusicType musicType;

    @Column(name = "CATEGORY")
    public String category;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MusicType getMusicType() {
        return musicType;
    }

    public void setMusicType(MusicType musicType) {
        this.musicType = musicType;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "MusicTypeCategory{" +
                "id=" + id +
                ", musicType=" + musicType +
                ", category='" + category + '\'' +
                '}';
    }
}
