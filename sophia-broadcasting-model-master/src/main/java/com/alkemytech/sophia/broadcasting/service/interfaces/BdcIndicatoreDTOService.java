package com.alkemytech.sophia.broadcasting.service.interfaces;

import com.alkemytech.sophia.broadcasting.data.IndicatoriPerCanale;
import com.alkemytech.sophia.broadcasting.dto.IndicatoreDTO;
import com.alkemytech.sophia.broadcasting.dto.RequestMonitoraggioKPIDTO;
import com.alkemytech.sophia.broadcasting.model.BdcKpiEntity;
import com.alkemytech.sophia.broadcasting.model.TipoBroadcaster;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface BdcIndicatoreDTOService {

    List<IndicatoreDTO> listaInd(RequestMonitoraggioKPIDTO monitoraggioKPI);

    List<BdcKpiEntity> listaIndicatori(Integer emittenteId, Integer canaleId, Integer anno, Integer mese);

    Map<String, BdcKpiEntity> mapIndicatori(Integer emittenteId, Integer canaleId, Integer anno, Integer mese);

	boolean evaluateIndicatoriTv(Integer idBroadcaster, Integer idCanale, Integer anno, Integer mese);

	boolean evaluateIndicatoriRadio(Integer idBroadcaster, Integer idCanale, Integer anno, Integer mese);

	boolean saveIndicatori(List<BdcKpiEntity> listaIndicatoriDaPersistere);

    /**
     restituisce una Map< idCanale, Map<indicatore,valore> >
     */
    HashMap<Integer, Map<String,Long>> mapCanaleIndicatore(Integer annoDa, Integer meseDa, Integer annoA,
                                                                  Integer meseA, TipoBroadcaster tipoBroadcaster);
    Long calcolaDurataDisponibile(Date dataDa, Date dataA);

    Long calcolaDurataDisponibile(Integer anno, List<Short> mesi);

	IndicatoriPerCanale indicatoriPerCanale(Integer annoDa, Integer meseDa,	Integer annoA, Integer meseA);
}
