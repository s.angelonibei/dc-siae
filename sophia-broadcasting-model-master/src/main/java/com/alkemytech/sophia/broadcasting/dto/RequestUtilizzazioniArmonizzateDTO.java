package com.alkemytech.sophia.broadcasting.dto;

import com.alkemytech.sophia.broadcasting.model.BdcBroadcasters;
import com.alkemytech.sophia.broadcasting.model.BdcCanali;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class RequestUtilizzazioniArmonizzateDTO {

    private BdcBroadcasters broadcasters;
    private BdcCanali canali;
    private String titoloTrasmissione;
    private String inizioTrasmissioneFrom;
    private String inizioTrasmissioneTo;

    public RequestUtilizzazioniArmonizzateDTO() {
    }

    public RequestUtilizzazioniArmonizzateDTO(BdcBroadcasters broadcasters, BdcCanali canali, String titoloTrasmissione,String inizioTrasmissioneFrom,String inizioTrasmissioneTo) {
        this.broadcasters = broadcasters;
        this.canali = canali;
        this.titoloTrasmissione = titoloTrasmissione;
        this.inizioTrasmissioneFrom = inizioTrasmissioneFrom;
        this.inizioTrasmissioneTo = inizioTrasmissioneTo;
    }

    public BdcBroadcasters getBroadcasters() {
        return broadcasters;
    }

    public void setBroadcasters(BdcBroadcasters broadcasters) {
        this.broadcasters = broadcasters;
    }

    public BdcCanali getCanali() {
        return canali;
    }

    public void setCanali(BdcCanali canali) {
        this.canali = canali;
    }

    public String getTitoloTrasmissione() {
        return titoloTrasmissione;
    }

    public void setTitoloTrasmissione(String titoloTrasmissione) {
        this.titoloTrasmissione = titoloTrasmissione;
    }

    public String getInizioTrasmissioneFrom() {
        return inizioTrasmissioneFrom;
    }

    public void setInizioTrasmissioneFrom(String inizioTrasmissioneFrom) {
        this.inizioTrasmissioneFrom = inizioTrasmissioneFrom;
    }

    public String getInizioTrasmissioneTo() {
        return inizioTrasmissioneTo;
    }

    public void setInizioTrasmissioneTo(String inizioTrasmissioneTo) {
        this.inizioTrasmissioneTo = inizioTrasmissioneTo;
    }
}
