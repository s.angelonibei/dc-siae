package com.alkemytech.sophia.broadcasting.utils;

import com.alkemytech.sophia.broadcasting.dto.BdcScheduleFieldDTO;
import com.alkemytech.sophia.broadcasting.dto.BdcShowMusicDTO;
import com.alkemytech.sophia.broadcasting.dto.RegoleErroreDTO;
import com.alkemytech.sophia.broadcasting.dto.RegoleErroreDTO.Regola;
import com.alkemytech.sophia.broadcasting.enums.UtilizationField;
import com.alkemytech.sophia.broadcasting.model.utilizations.BdcErrRdShowSchedule;
import com.alkemytech.sophia.broadcasting.model.utilizations.BdcErrTvShowSchedule;
import com.alkemytech.sophia.broadcasting.model.utilizations.BdcRdShowSchedule;
import com.alkemytech.sophia.broadcasting.model.utilizations.BdcTvShowSchedule;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class EntityToFieldConverter {

	private static final String FIELD_VALUE_NAME = "Il Campo ";
	private static final String ERROR_REGOLA_1 = " è vuoto ";
	private static final String ERROR_REGOLA_2 = " non è corretto, Valore inserito: ";
	private static final String ERROR_REGOLA_3 = " non è stato accettato, Valore inserito: ";

	private static final String FIELD_TITLE = "Titolo";
	private static final String FIELD_TITLE_ORIGIN = "Titolo originale";
	private static final String FIELD_CATEGORY = "Categoria";
	private static final String FIELD_DURATION = "Durata";
	private static final String FIELD_BEGIN_DATE = "Data d'inizio";
	private static final String FIELD_END_DATE = "Data di fine";
	private static final String FIELD_PRODUCTION_COUNTRY = "Paese di produzione";
	private static final String FIELD_PRODUCTION_YEAR = "Anno di produzione";
	private static final String FIELD_PRODUCTOR = "Produttore";
	private static final String FIELD_DIRECTOR = "Regista";
	private static final String FIELD_RETURN_COUNTER = "Numero repliche";
	private static final String FIELD_EPISODE_TITLE = "Titolo episodio";
	private static final String FIELD_EPISODE_TITLE_ORIGIN = "Titolo episodio originale";
	private static final String FIELD_EPISODE_NUMBER = "Numero episodio";
	private static final String FIELD_REGIONAL_OFFICE = "Ufficio Regionale";
	private static final String FIELD_CREATION_DATE = "Data di creazione";
	private static final String FIELD_MODIFY_DATE = "Data di modifica";
	private static final String FIELD_DAYS_FROM_UPLOAD = "Giorni dall'upload";
	private static final String FIELD_YEAR = "Anno";
	private static final String FIELD_MONTH = "Mese";

	// MUSICA
	private static final String FIELD_TITOLO_OP = "Titolo opera";
	private static final String FIELD_COMPOSITORE1 = "Primo compositore";
	private static final String FIELD_COMPOSITORE2 = "Secondo Compositore";
	private static final String FIELD_DURATA_OPERA = "Durata opera";
	private static final String FIELD_DURATA_EFFETTIVA_OPERA = "Durata effettiva opera";
	private static final String FIELD_CATEGORIA_USO_OPERA = "Categoria opera";
	private static final String FIELD_DECODE_ANNO_PRODUZIONE_OPERA = "Anno di produzione opera";
	private static final String FIELD_DECODE_DURATA_OPERA = "Durata opera inserita";
	private static final String FIELD_DECODE_DURATA_EFFETTIVA_OPERA = "Durata opera effettiva inserita";
	private static final String FIELD_DECODE_ORARIO_INIZIO_OPERA = "Orario inizio opera inserito";
	private static final String FIELD_DECODE_CATEGORIA_USO_OPERA = "Categoria opera inserita";

	public static List<BdcScheduleFieldDTO> parseBdcTVShowSchedule(BdcTvShowSchedule bdcTVShowSchedule) {
		List<BdcScheduleFieldDTO> result = new ArrayList<>();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy");
		result.add(new BdcScheduleFieldDTO(FIELD_TITLE, bdcTVShowSchedule.getTitle(), null, false));
		result.add(new BdcScheduleFieldDTO(FIELD_TITLE_ORIGIN, bdcTVShowSchedule.getOriginalTitle(), null, false));
		if (bdcTVShowSchedule.getShowType() != null) {
			result.add(new BdcScheduleFieldDTO(FIELD_CATEGORY, bdcTVShowSchedule.getShowType().getName(), null, false));
		}
		result.add(new BdcScheduleFieldDTO(FIELD_DURATION, convertSecond(bdcTVShowSchedule.getDuration().toString()),
				null, false));
		result.add(
				new BdcScheduleFieldDTO(FIELD_BEGIN_DATE, sdf.format(bdcTVShowSchedule.getBeginTime()), null, false));
		result.add(new BdcScheduleFieldDTO(FIELD_END_DATE, sdf.format(bdcTVShowSchedule.getEndTime()), null, false));

		//modifica Andrea per gestione programmi contenitore --> cambiando logica non può funzionare la pagina
		/*result.add(new BdcScheduleFieldDTO(FIELD_PRODUCTION_COUNTRY, bdcTVShowSchedule.getProdCountry(), null, false));
		if (bdcTVShowSchedule.getProdYear() != null) {
			result.add(
					new BdcScheduleFieldDTO(FIELD_PRODUCTION_YEAR, bdcTVShowSchedule.getProdYear() + "", null, false));
		}
		result.add(new BdcScheduleFieldDTO(FIELD_PRODUCTOR, bdcTVShowSchedule.getProductor(), null, false));
		result.add(new BdcScheduleFieldDTO(FIELD_DIRECTOR, bdcTVShowSchedule.getDirector(), null, false));
		result.add(new BdcScheduleFieldDTO(FIELD_RETURN_COUNTER, bdcTVShowSchedule.getReplica(), null, false));
		result.add(new BdcScheduleFieldDTO(FIELD_EPISODE_TITLE, bdcTVShowSchedule.getEpisodeTitle(), null, false));
		result.add(new BdcScheduleFieldDTO(FIELD_EPISODE_TITLE_ORIGIN, bdcTVShowSchedule.getEpisodeOriginalTitle(),
				null, false));
		result.add(
				new BdcScheduleFieldDTO(FIELD_EPISODE_NUMBER, bdcTVShowSchedule.getEpisodeNumber() + "", null, false));

		*/
		//fine modifica
		result.add(new BdcScheduleFieldDTO(FIELD_REGIONAL_OFFICE, bdcTVShowSchedule.getRegionalOffice(), null, false));
		if (bdcTVShowSchedule.getCreationDate() != null) {
			result.add(new BdcScheduleFieldDTO(FIELD_CREATION_DATE, sdf.format(bdcTVShowSchedule.getCreationDate()),
					null, false));
		}
		if (bdcTVShowSchedule.getModifyDate() != null) {
			result.add(new BdcScheduleFieldDTO(FIELD_MODIFY_DATE, sdf.format(bdcTVShowSchedule.getModifyDate()), null,
					false));
		}
		result.add(new BdcScheduleFieldDTO(FIELD_DAYS_FROM_UPLOAD, bdcTVShowSchedule.getDaysFromUpload().toString(),
				null, false));
		if (bdcTVShowSchedule.getScheduleYear() != null) {
			result.add(
					new BdcScheduleFieldDTO(FIELD_YEAR, bdcTVShowSchedule.getScheduleYear().toString(), null, false));
		}
		if (bdcTVShowSchedule.getScheduleMonth() != null) {
			result.add(
					new BdcScheduleFieldDTO(FIELD_MONTH, bdcTVShowSchedule.getScheduleMonth().toString(), null, false));
		}
		return result;

	}

	public static List<BdcScheduleFieldDTO> parseBdcErrTVShowSchedule(BdcErrTvShowSchedule bdcErrTvShowSchedule,
			BdcShowMusicDTO bdcShowMusicDTO) {
		List<BdcScheduleFieldDTO> result = new ArrayList<>();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy");

		Gson gson = new Gson();
		RegoleErroreDTO regoleErroreDTO = gson.fromJson(bdcErrTvShowSchedule.getError(), RegoleErroreDTO.class);

		result.add(new BdcScheduleFieldDTO(FIELD_TITLE, bdcErrTvShowSchedule.getTitle(),
				decodeError(UtilizationField.TITOLO_TX, regoleErroreDTO, FIELD_TITLE), false));

		result.add(new BdcScheduleFieldDTO(FIELD_TITLE_ORIGIN, bdcErrTvShowSchedule.getOriginalTitle(),
				decodeError(UtilizationField.TITOLO_ORIGINALE_TX, regoleErroreDTO, FIELD_TITLE_ORIGIN), false));

		if (bdcErrTvShowSchedule.getShowType() != null) {
			result.add(new BdcScheduleFieldDTO(FIELD_CATEGORY, bdcErrTvShowSchedule.getShowType().getName(),
					decodeError(UtilizationField.GENERE_TX, regoleErroreDTO, FIELD_CATEGORY), false));
		}
		if (bdcErrTvShowSchedule.getDuration() != null) {
			result.add(new BdcScheduleFieldDTO(FIELD_DURATION, bdcErrTvShowSchedule.getDuration().toString(),
					decodeError(UtilizationField.DURATA_TX, regoleErroreDTO, FIELD_DURATION), false));
		} else {
			result.add(new BdcScheduleFieldDTO(FIELD_DURATION, "",
					decodeError(UtilizationField.DURATA_TX, regoleErroreDTO, FIELD_DURATION), false));
		}

		result.add(new BdcScheduleFieldDTO(FIELD_BEGIN_DATE, sdf.format(bdcErrTvShowSchedule.getBeginTime()),
				decodeError(UtilizationField.ORARIO_INIZIO_TX, regoleErroreDTO, FIELD_BEGIN_DATE), false));

		result.add(new BdcScheduleFieldDTO(FIELD_END_DATE, sdf.format(bdcErrTvShowSchedule.getEndTime()),
				decodeError(UtilizationField.ORARIO_FINE_TX, regoleErroreDTO, FIELD_END_DATE), false));

		result.add(new BdcScheduleFieldDTO(FIELD_PRODUCTION_COUNTRY, bdcErrTvShowSchedule.getProdCountry(),
				decodeError(UtilizationField.PAESE_PRODUZIONE_TX, regoleErroreDTO, FIELD_PRODUCTION_COUNTRY), false));

		if (bdcErrTvShowSchedule.getProdYear() != null) {
			result.add(new BdcScheduleFieldDTO(FIELD_PRODUCTION_YEAR, bdcErrTvShowSchedule.getProdYear() + "",
					decodeError(UtilizationField.ANNO_PRODUZIONE_TX, regoleErroreDTO, FIELD_PRODUCTION_YEAR), false));
		}
		result.add(new BdcScheduleFieldDTO(FIELD_PRODUCTOR, bdcErrTvShowSchedule.getProductor(),
				decodeError(UtilizationField.PRODUTTORE_TX, regoleErroreDTO, FIELD_PRODUCTOR), false));
		result.add(new BdcScheduleFieldDTO(FIELD_DIRECTOR, bdcErrTvShowSchedule.getDirector(),
				decodeError(UtilizationField.REGISTA_TX, regoleErroreDTO, FIELD_DIRECTOR), false));
		result.add(new BdcScheduleFieldDTO(FIELD_RETURN_COUNTER, bdcErrTvShowSchedule.getReplica(),
				decodeError(UtilizationField.REPLICA_TX, regoleErroreDTO, FIELD_RETURN_COUNTER), false));
		result.add(new BdcScheduleFieldDTO(FIELD_EPISODE_TITLE, bdcErrTvShowSchedule.getEpisodeTitle(),
				decodeError(UtilizationField.TITOLO_EPISODIO_TX, regoleErroreDTO, FIELD_EPISODE_TITLE), false));
		result.add(new BdcScheduleFieldDTO(FIELD_EPISODE_TITLE_ORIGIN, bdcErrTvShowSchedule.getEpisodeOriginalTitle(),
				decodeError(UtilizationField.TITOLO_ORIGINALE_EPISODIO_TX, regoleErroreDTO, FIELD_EPISODE_TITLE_ORIGIN),
				false));
		result.add(new BdcScheduleFieldDTO(FIELD_EPISODE_NUMBER, bdcErrTvShowSchedule.getEpisodeNumber() + "",
				decodeError(UtilizationField.NUM_PROGRESSIVO_EPISODIO_TX, regoleErroreDTO, FIELD_EPISODE_NUMBER),
				false));
		result.add(new BdcScheduleFieldDTO(FIELD_REGIONAL_OFFICE, bdcErrTvShowSchedule.getRegionalOffice(),
				decodeError(UtilizationField.DIFFUSIONE_REGIONALE, regoleErroreDTO, FIELD_REGIONAL_OFFICE), false));
		if (bdcErrTvShowSchedule.getCreationDate() != null) {
			result.add(new BdcScheduleFieldDTO(FIELD_CREATION_DATE, sdf.format(bdcErrTvShowSchedule.getCreationDate()),
					null, false));
		} else {
			result.add(new BdcScheduleFieldDTO(FIELD_CREATION_DATE, "", null, false));
		}
		if (bdcErrTvShowSchedule.getModifyDate() != null) {
			result.add(new BdcScheduleFieldDTO(FIELD_MODIFY_DATE, sdf.format(bdcErrTvShowSchedule.getModifyDate()),
					null, false));
		} else {
			result.add(new BdcScheduleFieldDTO(FIELD_MODIFY_DATE, "", null, false));
		}
		if (bdcShowMusicDTO != null) {
			result.add(new BdcScheduleFieldDTO(FIELD_TITOLO_OP, bdcShowMusicDTO.getTitle(),
					decodeError(UtilizationField.TITOLO_OP, regoleErroreDTO, FIELD_TITOLO_OP), false));

			result.add(new BdcScheduleFieldDTO(FIELD_COMPOSITORE1, bdcShowMusicDTO.getFirstComposer(),
					decodeError(UtilizationField.COMPOSITORE1_OP, regoleErroreDTO, FIELD_COMPOSITORE1), false));

			result.add(new BdcScheduleFieldDTO(FIELD_COMPOSITORE2, bdcShowMusicDTO.getSecondComposer(),
					decodeError(UtilizationField.COMPOSITORE2_OP, regoleErroreDTO, FIELD_COMPOSITORE2), false));

			// if (bdcShowMusicDTO.getDuration() != null) {
			// result.add(
			// new BdcScheduleFieldDTO(FIELD_DURATA_OPERA, bdcShowMusicDTO.getDuration() +
			// "",
			// decodeError(UtilizationField.DURATA_OP, regoleErroreDTO, FIELD_DURATA_OPERA),
			// false));
			// } else {
			// result.add(new BdcScheduleFieldDTO(FIELD_DURATA_OPERA, "",
			// decodeError(UtilizationField.DURATA_OP, regoleErroreDTO, FIELD_DURATA_OPERA),
			// false));
			// }

			if (bdcShowMusicDTO.getRealDuration() != null) {
				result.add(new BdcScheduleFieldDTO(FIELD_DURATA_EFFETTIVA_OPERA, bdcShowMusicDTO.getRealDuration() + "",
						decodeError(UtilizationField.DURATA_EFFETTIVA_OP, regoleErroreDTO,
								FIELD_DURATA_EFFETTIVA_OPERA),
						false));
			} else {
				result.add(new BdcScheduleFieldDTO(FIELD_DURATA_EFFETTIVA_OPERA, "", decodeError(
						UtilizationField.DURATA_EFFETTIVA_OP, regoleErroreDTO, FIELD_DURATA_EFFETTIVA_OPERA), false));
			}

			if (bdcShowMusicDTO.getMusicType() != null) {
				result.add(new BdcScheduleFieldDTO(FIELD_CATEGORIA_USO_OPERA, bdcShowMusicDTO.getMusicType().category,
						decodeError(UtilizationField.CATEGORIA_USO_OP, regoleErroreDTO, FIELD_CATEGORIA_USO_OPERA),
						false));
			} else {
				result.add(new BdcScheduleFieldDTO(FIELD_CATEGORIA_USO_OPERA, "",
						decodeError(UtilizationField.CATEGORIA_USO_OP, regoleErroreDTO, FIELD_CATEGORIA_USO_OPERA),
						false));
			}
		}

		return result;
	}

	public static List<BdcScheduleFieldDTO> parseBdcRdShowSchedule(BdcRdShowSchedule bdcRdShowSchedule) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		List<BdcScheduleFieldDTO> result = new ArrayList<>();
		result.add(new BdcScheduleFieldDTO(FIELD_TITLE, bdcRdShowSchedule.getTitle(), null, false));
		if (null != bdcRdShowSchedule.getBeginTime()) {
			result.add(new BdcScheduleFieldDTO(FIELD_BEGIN_DATE, sdf.format(bdcRdShowSchedule.getBeginTime()), null,
					false));
		} else {
			result.add(new BdcScheduleFieldDTO(FIELD_BEGIN_DATE, "", null, false));
		}
		if (null != bdcRdShowSchedule.getEndTime()) {
			result.add(
					new BdcScheduleFieldDTO(FIELD_END_DATE, sdf.format(bdcRdShowSchedule.getEndTime()), null, false));
		} else {
			result.add(new BdcScheduleFieldDTO(FIELD_END_DATE, "", null, false));
		}
		if (null != bdcRdShowSchedule.getDuration()) {
			result.add(new BdcScheduleFieldDTO(FIELD_DURATION,
					convertSecond(bdcRdShowSchedule.getDuration().toString()), null, false));
		} else {
			result.add(new BdcScheduleFieldDTO(FIELD_DURATION, "", null, false));
		}
		result.add(new BdcScheduleFieldDTO(FIELD_REGIONAL_OFFICE, bdcRdShowSchedule.getRegionalOffice(), null, false));
		if (null != bdcRdShowSchedule.getCreationDate()) {
			result.add(new BdcScheduleFieldDTO(FIELD_CREATION_DATE, sdf.format(bdcRdShowSchedule.getCreationDate()),
					null, false));
		} else {
			result.add(new BdcScheduleFieldDTO(FIELD_CREATION_DATE, "", null, false));
		}
		if (null != bdcRdShowSchedule.getModifyDate()) {
			result.add(new BdcScheduleFieldDTO(FIELD_MODIFY_DATE, sdf.format(bdcRdShowSchedule.getModifyDate()), null,
					false));
		} else {
			result.add(new BdcScheduleFieldDTO(FIELD_MODIFY_DATE, "", null, false));
		}
		return result;

	}

	public static List<BdcScheduleFieldDTO> parseBdcErrRdShowSchedule(BdcErrRdShowSchedule bdcErrRdShowSchedule) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		List<BdcScheduleFieldDTO> result = new ArrayList<>();

		Gson gson = new Gson();
		RegoleErroreDTO regoleErroreDTO = gson.fromJson(bdcErrRdShowSchedule.getError(), RegoleErroreDTO.class);

		result.add(new BdcScheduleFieldDTO(FIELD_TITLE, bdcErrRdShowSchedule.getTitle(),
				decodeError(UtilizationField.TITOLO_TX, regoleErroreDTO, FIELD_TITLE), false));
		
		if (null != bdcErrRdShowSchedule.getBeginTime()) {
			result.add(new BdcScheduleFieldDTO(FIELD_BEGIN_DATE, sdf.format(bdcErrRdShowSchedule.getBeginTime()),
					decodeError(UtilizationField.ORARIO_INIZIO_TX, regoleErroreDTO, FIELD_BEGIN_DATE), false));
		} else {
			result.add(new BdcScheduleFieldDTO(FIELD_BEGIN_DATE, "",
					decodeError(UtilizationField.ORARIO_INIZIO_TX, regoleErroreDTO, FIELD_BEGIN_DATE), false));
		}
		if (null != bdcErrRdShowSchedule.getEndTime()) {
			result.add(new BdcScheduleFieldDTO(FIELD_END_DATE, sdf.format(bdcErrRdShowSchedule.getEndTime()),
					decodeError(UtilizationField.ORARIO_FINE_TX, regoleErroreDTO, FIELD_END_DATE), false));
		} else {
			result.add(new BdcScheduleFieldDTO(FIELD_END_DATE, "",
					decodeError(UtilizationField.ORARIO_FINE_TX, regoleErroreDTO, FIELD_END_DATE), false));
		}
	
		if (bdcErrRdShowSchedule.getDuration()!=null) {
			result.add(new BdcScheduleFieldDTO(FIELD_DURATION, bdcErrRdShowSchedule.getDuration().toString(),
					decodeError(UtilizationField.DURATA_TX, regoleErroreDTO, FIELD_DURATION), false));
		}else {
			result.add(new BdcScheduleFieldDTO(FIELD_DURATION, "",
					decodeError(UtilizationField.DURATA_TX, regoleErroreDTO, FIELD_DURATION), false));
		}
		
		result.add(new BdcScheduleFieldDTO(FIELD_REGIONAL_OFFICE, bdcErrRdShowSchedule.getRegionalOffice(),
				decodeError(UtilizationField.DIFFUSIONE_REGIONALE, regoleErroreDTO, FIELD_REGIONAL_OFFICE), false));
		
		if (null != bdcErrRdShowSchedule.getCreationDate()) {
			result.add(new BdcScheduleFieldDTO(FIELD_CREATION_DATE, sdf.format(bdcErrRdShowSchedule.getCreationDate()),
					null, false));
		} else {
			result.add(new BdcScheduleFieldDTO(FIELD_CREATION_DATE, "",
					null, false));
		}
		if (null != bdcErrRdShowSchedule.getModifyDate()) {
			result.add(new BdcScheduleFieldDTO(FIELD_MODIFY_DATE, sdf.format(bdcErrRdShowSchedule.getModifyDate()),
					null, false));
		} else {
			result.add(new BdcScheduleFieldDTO(FIELD_MODIFY_DATE, "",
					null, false));
		}
		
		return result;
	}

	private static String decodeError(UtilizationField fieldNameType, RegoleErroreDTO error, String fieldName) {
		for (Regola regola : error.getRegola1()) {
			if (UtilizationField.valueOfDescription(regola.getFieldName()).equals(fieldNameType)) {
				return getStringFromError(1, fieldName, regola.inputValue);
			}
		}
		for (Regola regola : error.getRegola2()) {
			if (UtilizationField.valueOfDescription(regola.getFieldName()).equals(fieldNameType)) {
				return getStringFromError(2, fieldName, regola.inputValue);
			}
		}
		for (Regola regola : error.getRegola3()) {
			if (UtilizationField.valueOfDescription(regola.getFieldName()).equals(fieldNameType)) {
				return getStringFromError(3, fieldName, regola.inputValue);
			}
		}

		return "";

	}

	private static String getStringFromError(int nRegola, String fieldName, String insertValue) {
		if (nRegola == 1) {
			return FIELD_VALUE_NAME + fieldName + ERROR_REGOLA_1;
		} else if (nRegola == 2) {
			return FIELD_VALUE_NAME + fieldName + ERROR_REGOLA_2 + insertValue;
		} else if (nRegola == 3) {
			return FIELD_VALUE_NAME + fieldName + ERROR_REGOLA_3 + insertValue;
		} else {
			return "";
		}

	}

	private static String convertSecond(String second) {
		try {
			Date d = new Date(Integer.parseInt(second) * 1000L);
			SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss"); // HH for 0-23
			df.setTimeZone(TimeZone.getTimeZone("GMT"));
			String time = df.format(d);
			return time;
		} catch (Exception e) {
			return "";
		}

	}

}
