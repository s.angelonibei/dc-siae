package com.alkemytech.sophia.broadcasting.deserialized;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.TimeZone;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public class MultiTimeDeserializer extends StdDeserializer<Date> {
    private static final long serialVersionUID = 1L;

    private static final String[] DATE_FORMATS = new String[] {
            "HH:mm:ss",
            "HH:mm"

    };

    public MultiTimeDeserializer() {
        this(null);
    }

    public MultiTimeDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Date deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        JsonNode node = jp.getCodec().readTree(jp);
        String date = node.textValue();

        for (String DATE_FORMAT : DATE_FORMATS) {
            try {
                SimpleDateFormat shortTimeFormat = new SimpleDateFormat(DATE_FORMAT);
                shortTimeFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

                return shortTimeFormat.parse(date);

            } catch (ParseException e) {
            }
        }

        return null;
        //throw new JsonParseException("Formato non supportato: " +  Arrays.toString(DATE_FORMATS),null);

    }
}