package com.alkemytech.sophia.broadcasting.service.interfaces;

import com.alkemytech.sophia.broadcasting.model.BdcCanaleStorico;
import com.alkemytech.sophia.broadcasting.model.BdcCanali;

import java.util.List;

import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

public interface BdcCanaliService {
    List<BdcCanali> findAll();
    List<BdcCanali> findAllActive();
    List<BdcCanali> findByBroadcasterId(Integer id);
	List<BdcCanali> findActiveByBroadcasterId(Integer idBroadcaster);
    BdcCanali findById(Integer id);
    boolean addCannel(BdcCanali bdcCanali);
    Integer editCannel(BdcCanali bdcCanali);

    List<BdcCanaleStorico> findAllRecent(Integer id);
	boolean findCanaleByName(Integer bdcBroadcastersId, String nome);
 
}
