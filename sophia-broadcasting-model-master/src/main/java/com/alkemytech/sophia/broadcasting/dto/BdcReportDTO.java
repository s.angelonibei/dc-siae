package com.alkemytech.sophia.broadcasting.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class BdcReportDTO {

	private String tipoTrasmissione;
	private String erroreTrasmissione;
	private List<BdcScheduleFieldDTO> bdcCampoDTO;
	private List<BdcShowMusicDTO> showMusicDTO;

	public BdcReportDTO() {
		super();
	}

	public BdcReportDTO(String tipoTrasmissione, String erroreTrasmissione, List<BdcScheduleFieldDTO> bdcCampoDTO,
			List<BdcShowMusicDTO> showMusicDTO) {
		super();
		this.tipoTrasmissione = tipoTrasmissione;
		this.erroreTrasmissione = erroreTrasmissione;
		this.bdcCampoDTO = bdcCampoDTO;
		this.showMusicDTO = showMusicDTO;
	}

	public String getTipoTrasmissione() {
		return tipoTrasmissione;
	}

	public void setTipoTrasmissione(String tipoTrasmissione) {
		this.tipoTrasmissione = tipoTrasmissione;
	}

	public String getErroreTrasmissione() {
		return erroreTrasmissione;
	}

	public void setErroreTrasmissione(String erroreTrasmissione) {
		this.erroreTrasmissione = erroreTrasmissione;
	}

	public List<BdcScheduleFieldDTO> getBdcCampoDTO() {
		return bdcCampoDTO;
	}

	public void setBdcCampoDTO(List<BdcScheduleFieldDTO> bdcCampoDTO) {
		this.bdcCampoDTO = bdcCampoDTO;
	}

	public List<BdcShowMusicDTO> getShowMusicDTO() {
		return showMusicDTO;
	}

	public void setShowMusicDTO(List<BdcShowMusicDTO> showMusicDTO) {
		this.showMusicDTO = showMusicDTO;
	}

}
