package com.alkemytech.sophia.broadcasting.enums;

public enum Repertorio {

    MUSICA("MUSICA"),
    CINEMA("CINEMA"),
    LIRICA("LIRICA"),
    DOR("DOR"),
    OLAF("OLAF");

    String text;

    Repertorio(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }

    public static Repertorio valueOfDescription(String description) {

        Repertorio repertorio=null;

        for (Repertorio v : values()) {

            if (v.toString().equals(description)) {
                repertorio = v;
            }
        }

        return repertorio;
    }
}
