package com.alkemytech.sophia.broadcasting.service;

import com.alkemytech.sophia.broadcasting.model.*;
import com.alkemytech.sophia.broadcasting.model.decode.BdcDecodeChannel;
import com.alkemytech.sophia.broadcasting.model.decode.BdcDecodeMusicType;
import com.alkemytech.sophia.broadcasting.model.decode.BdcDecodeShowType;
import com.alkemytech.sophia.broadcasting.service.interfaces.DecodeService;
import com.alkemytech.sophia.common.tools.DateTools;
import com.amazonaws.util.CollectionUtils;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.util.*;

/**
 * Created by Alessandro Russo on 09/12/2017.
 */
@Singleton
public class DecodeServiceImpl implements DecodeService {

    Logger logger = LoggerFactory.getLogger(getClass());

    Provider<EntityManager> provider;

    Map<String, Map<Integer, Map<String, BdcDecodeShowType>>> mapDecodeShowType = new HashMap<>();
    Map<String, Map<Integer, Map<String, BdcDecodeMusicType>>> mapDecodeMusicType = new HashMap<>();
    Map<Integer, Map<String, BdcDecodeChannel>> mapDecodeChannel = new HashMap<>();
    Map<Integer, List<BdcCanali>> mapChannel = new HashMap<>();

    List<ShowType> listShowType = new ArrayList<>();
    List<MusicType> listMusicType = new ArrayList<>();
    List<MusicTypeCategory> listMusicTypeCategory = new ArrayList<>();
    List<BdcCanali> listChannel = new ArrayList<>();

    @Inject
    public DecodeServiceImpl(Provider<EntityManager> provider) {
        this.provider = provider;
    }

    @Override
    public void start() {
        EntityManager em = provider.get();
        //Extract Decode Tables
        List<BdcDecodeShowType> allDecodeShowType = em.createNamedQuery("BdcDecodeShowType.findAll", BdcDecodeShowType.class).getResultList();
        List<BdcDecodeMusicType> allDecodeMusicType = em.createNamedQuery("BdcDecodeMusicType.findAll", BdcDecodeMusicType.class).getResultList();
        List<BdcDecodeChannel> allDecodeChannel = em.createNamedQuery("BdcDecodeChannel.findAll", BdcDecodeChannel.class).getResultList();
        //Extract LOV Tables
        listShowType = em.createNamedQuery("ShowType.findAll", ShowType.class).getResultList();
        listMusicType = em.createNamedQuery("MusicType.findAll", MusicType.class).getResultList();
        listMusicTypeCategory = em.createNamedQuery("MusicTypeCategory.findAll", MusicTypeCategory.class).getResultList();
        listChannel = em.createNamedQuery("BdcCanali.findAll", BdcCanali.class).getResultList();

        if (!CollectionUtils.isNullOrEmpty(allDecodeShowType)) {
            for (BdcDecodeShowType current : allDecodeShowType) {
                if (!mapDecodeShowType.containsKey(current.getScope())) {
                    mapDecodeShowType.put(current.getScope(), new HashMap<Integer, Map<String, BdcDecodeShowType>>());
                }
                if (!mapDecodeShowType.get(current.getScope()).containsKey(current.getBroadcaster().getId())) {
                    mapDecodeShowType.get(current.getScope()).put(current.getBroadcaster().getId(), new HashMap<String, BdcDecodeShowType>());
                }
                mapDecodeShowType.get(current.getScope()).get(current.getBroadcaster().getId()).put(current.getValue(), current);
            }
        }

        if (!CollectionUtils.isNullOrEmpty(allDecodeMusicType)) {
            for (BdcDecodeMusicType current : allDecodeMusicType) {
//                logger.info("Current decode music type {} broadcaster {}", current.getId(), current.getBroadcaster().getId());
                if (!mapDecodeMusicType.containsKey(current.getScope())) {
                    mapDecodeMusicType.put(current.getScope(), new HashMap<Integer, Map<String, BdcDecodeMusicType>>());
                }
                if (!mapDecodeMusicType.get(current.getScope()).containsKey(current.getBroadcaster().getId())) {
                    mapDecodeMusicType.get(current.getScope()).put(current.getBroadcaster().getId(), new HashMap<String, BdcDecodeMusicType>());
                }
                mapDecodeMusicType.get(current.getScope()).get(current.getBroadcaster().getId()).put(current.getValue(), current);
            }
        }

        if (!CollectionUtils.isNullOrEmpty(allDecodeChannel)) {
            for (BdcDecodeChannel current : allDecodeChannel) {
                if (!mapDecodeChannel.containsKey(current.getCanale().getBdcBroadcasters().getId())) {
                    mapDecodeChannel.put(current.getCanale().getBdcBroadcasters().getId(), new HashMap<String, BdcDecodeChannel>());
                }
                mapDecodeChannel.get(current.getCanale().getBdcBroadcasters().getId()).put(current.getValue(), current);
            }
        }

        if (!CollectionUtils.isNullOrEmpty(listChannel)) {
            for (BdcCanali current : listChannel) {
                if (!mapChannel.containsKey(current.getBdcBroadcasters().getId())) {
                    mapChannel.put(current.getBdcBroadcasters().getId(), new ArrayList<BdcCanali>());
                }
                mapChannel.get(current.getBdcBroadcasters().getId()).add(current);
            }
        }
    }

    //SHOW TYPE
    @Override
    public Long verifyIdShowType(String checkShowType) {
        if (!CollectionUtils.isNullOrEmpty(listShowType)) {
            try {
                Long idShowType = Long.parseLong(checkShowType);
                for (ShowType current : listShowType) {
                    if (idShowType.equals(current.getId())) {
                        return idShowType;
                    }
                }
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        }
        return null;
    }

    @Override
    public ShowType findShowTypeById(Long idShowType) {
        if (!CollectionUtils.isNullOrEmpty(listShowType)) {
            for (ShowType current : listShowType) {
                if (idShowType.equals(current.getId())) {
                    return current;
                }
            }
        }
        return null;
    }

    @Override
    public Long findIdShowTypeByValueAndScope(Integer idBroacaster, String value, String scope) {
        logger.debug(String.format("Search for Show Type: idBroad -> %s, value -> %s, scope -> %s", idBroacaster, value, scope));
        BdcDecodeShowType decodeShowType = findDecodeShowByBroadcasterAndValueAndScope(idBroacaster, value, scope);
        logger.debug(decodeShowType != null ? String.format("Output: idDecode -> %s, showType -> %s", decodeShowType.getId(), decodeShowType.getShowType().getCategory()) : "Output: decodifica non trovata");
        return decodeShowType != null ? decodeShowType.getShowType().getId() : null;
    }

    @Override
    public Long findIdShowTypeByName(String name) {
        ShowType showType = findShowTypeByName(name);
        return showType != null ? showType.getId() : null;
    }

    @Override
    public String getNameShowTypeById(Long id) {
        ShowType showType = findShowTypeById(id);
        return showType != null ? showType.getName() : null;
    }

    @Override
    public String findCategoryShowTypeByValueAndScope(Integer idBroacaster, String value, String scope) {
        logger.debug(String.format("Search for Category: idBroad -> %s, value -> %s, scope -> %s", idBroacaster, value, scope));
        BdcDecodeShowType decodeShowType = findDecodeShowByBroadcasterAndValueAndScope(idBroacaster, value, scope);
        logger.debug(decodeShowType != null ? String.format("Output: idDecode -> %s, showType category -> %s", decodeShowType.getId(), decodeShowType.getShowType().getCategory()) : "Output: decodifica non trovata");
        return decodeShowType != null ? decodeShowType.getShowType().getCategory() : null;
    }

    //MUSIC TYPE
    @Override
    public Long verifyIdMusicType(String checkMusicType) {
        if (!CollectionUtils.isNullOrEmpty(listMusicType)) {
            try {
                Long idMusicType = Long.parseLong(checkMusicType);
                for (MusicType current : listMusicType) {
                    if (idMusicType.equals(current.getId())) {
                        return idMusicType;
                    }
                }
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        }
        return null;
    }

    @Override
    public MusicType findMusicTypeById(Long idMusicType) {
        if (!CollectionUtils.isNullOrEmpty(listMusicType)) {
            for (MusicType current : listMusicType) {
                if (idMusicType.equals(current.getId())) {
                    return current;
                }
            }
        }
        return null;
    }

    @Override
    public Long findIdMusicTypeByValueAndScope(Integer idBroacaster, String value, String scope) {
        logger.debug(String.format("Search for Music Type: idBroad -> %s, value -> %s, scope -> %s", idBroacaster, value, scope));
        BdcDecodeMusicType decodeMusicType = findDecodeMusicByBroadcasterAndValueAndScope(idBroacaster, value, scope);
        logger.debug(decodeMusicType != null ? String.format("Output: idDecode -> %s, musicType -> %s", decodeMusicType.getId(), decodeMusicType.getMusicType().getCategory()) : "Output: decodifica non trovata");
        return decodeMusicType != null ? decodeMusicType.getMusicType().getId() : null;
    }

    @Override
    public Long findIdMusicTypeByName(String name) {
        logger.debug(String.format("Search for Music Type: musicTypeName -> %s", name));
        MusicType musicType = findMusicTypeByName(name);
        logger.debug(musicType != null ? String.format("Output: idMusicType -> %s", musicType.getId()) : "Output: decodifica non trovata");
        return musicType != null ? musicType.getId() : null;
    }

    @Override
    public String getNameMusicTypeById(Long id) {
        MusicType musicType = findMusicTypeById(id);
        return musicType != null ? musicType.getName() : null;
    }

    @Override
    public Long findIdMusicTypeByShowTypeAndName(String showTypeCategory, String name) {
        logger.debug(String.format("Search for Show Type: showTypeCategory -> %s, name -> %s", showTypeCategory, name));
        if (StringUtils.isNotEmpty(showTypeCategory)) {
            if (!CollectionUtils.isNullOrEmpty(listMusicTypeCategory)) {
                for (MusicTypeCategory current : listMusicTypeCategory) {
                    if (current.getCategory().equals(showTypeCategory) && current.getMusicType().getName().equalsIgnoreCase(name)) {
                        logger.debug(String.format("Output: idCategory -> %s, idMusicType -> %s", current.getId(), current.getMusicType().getId()));
                        return current.getMusicType().getId();
                    }
                }
            }
        }
        logger.debug("Output: decodifica non trovata");
        return null;
    }

    @Override
    public Long findIdMusicTypeByShowTypeAndValue(String showTypeCategory, Integer idBroacaster, String value, String scope) {
        logger.debug(String.format("Search for Music Type: ShowType Category -> %s, idBroad -> %s, value -> %s, scope -> %s", showTypeCategory, idBroacaster, value, scope));
        BdcDecodeMusicType decodeMusicType = findDecodeMusicByBroadcasterAndValueAndScope(idBroacaster, value, scope);
        if (StringUtils.isNotEmpty(showTypeCategory) && decodeMusicType != null) {
            if (!CollectionUtils.isNullOrEmpty(listMusicTypeCategory)) {
                for (MusicTypeCategory current : listMusicTypeCategory) {
                    if (current.getCategory().equals(showTypeCategory)) {
                        if (current.getMusicType().getId().equals(decodeMusicType.getMusicType().getId())) {
                            logger.debug(String.format("Output: idDecode -> %s, musicType -> %s", decodeMusicType.getId(), decodeMusicType.getMusicType().getCategory()));
                            return current.getMusicType().getId();
                        }
                    }
                }
            }
        }
        logger.debug("Output: decodifica non trovata");
        return null;
    }

    //CHANNEL
    @Override
    public BdcCanali findChannelById(Integer idChannel) {
        if (!CollectionUtils.isNullOrEmpty(listChannel)) {
            for (BdcCanali current : listChannel) {
                if (idChannel.equals(current.getId())) {
                    return current;
                }
            }
        }
        return null;
    }

    @Override
    public List<BdcCanali> findCanaliByIdBroadcaster(Integer idBroadcaster) {
        for (Integer currentBroadcaster : mapChannel.keySet()) {
            if (idBroadcaster.equals(currentBroadcaster)) {
                return mapChannel.get(idBroadcaster);
            }
        }
        return null;
    }

    @Override
    public Integer findIdChannelByValue(Integer idBroacaster, String value) {
        if (mapDecodeChannel.containsKey(idBroacaster)) {
            if (mapDecodeChannel.get(idBroacaster).containsKey(value.toUpperCase())) {
                return mapDecodeChannel.get(idBroacaster).get(value.toUpperCase()).getCanale().getId();
            }
        }
        return null;
    }

    @Override
    public Integer findIdChannelByName(Integer idBroacaster, String name) {
        if (mapChannel.containsKey(idBroacaster)) {
            if (!CollectionUtils.isNullOrEmpty(mapChannel.get(idBroacaster))) {
                for (BdcCanali current : mapChannel.get(idBroacaster)) {
                    if (current.getNome().equalsIgnoreCase(name)) {
                        return current.getId();
                    }
                }
            }
        }
        return null;
    }

    @Override
    public String getNameChannelById(Integer id) {
        BdcCanali channel = findChannelById(id);
        return channel != null ? channel.getNome() : null;
    }

    @Override
    public boolean isRegionalChannel(Integer idBroacaster, String value) {
        BdcDecodeChannel decodeChannel = findDecodeChannelByBroadcasterAndValue(idBroacaster, value);
        if (decodeChannel != null) {
            return decodeChannel.isRegional();
        }
        return false;
    }

    //BROADCASTER
    @Override
    public Integer findIdBroadcasterByName(String name, String scope) {
        for (Integer idBroadcaster : mapChannel.keySet()) {
            if (!CollectionUtils.isNullOrEmpty(mapChannel.get(idBroadcaster))) {
                BdcBroadcasters broadcaster = mapChannel.get(idBroadcaster).get(0).getBdcBroadcasters();
                if (broadcaster.getNome().equalsIgnoreCase(name) && broadcaster.getTipo_broadcaster().toString().equalsIgnoreCase(scope)) {
                    return broadcaster.getId();
                }
            }
        }
        return null;
    }

    private BdcDecodeShowType findDecodeShowByBroadcasterAndValueAndScope(Integer idBroadcaster, String value, String scope) {
        try {
            String normalizedValue = StringUtils.isNotEmpty(value) ? value.toUpperCase() : value;
            return mapDecodeShowType.get(scope).get(idBroadcaster).get(normalizedValue);
        } catch (Exception e) {
            logger.error("No result found for value {} and scope {}", value, scope);
            return null;
        }
    }

    private BdcDecodeMusicType findDecodeMusicByBroadcasterAndValueAndScope(Integer idBroadcaster, String value, String scope) {
        try {
            String normalizedValue = StringUtils.isNotEmpty(value) ? value.toUpperCase() : value;
            return mapDecodeMusicType.get(scope).get(idBroadcaster).get(normalizedValue);
        } catch (Exception e) {
            logger.error("No result found for value {} and scope {}", value, scope);
            return null;
        }
    }

    private BdcDecodeChannel findDecodeChannelByBroadcasterAndValue(Integer idBroacaster, String value) {
        try {
            String normalizedValue = StringUtils.isNotEmpty(value) ? value.toUpperCase() : value;
            return mapDecodeChannel.get(idBroacaster).get(normalizedValue);
        } catch (Exception e) {
            logger.error("No result found for value {}", value);
            return null;
        }
    }

    private ShowType findShowTypeByName(String name) {
        if (!CollectionUtils.isNullOrEmpty(listShowType)) {
            for (ShowType current : listShowType) {
                if (current.getName().equalsIgnoreCase(name)) {
                    return current;
                }
            }
        }
        return null;
    }

    private MusicType findMusicTypeByName(String name) {
        if (!CollectionUtils.isNullOrEmpty(listMusicTypeCategory)) {
            for (MusicTypeCategory current : listMusicTypeCategory) {
                if (current.getMusicType().getName().equalsIgnoreCase(name)) {
                    return current.getMusicType();
                }
            }
        }
        return null;
    }
}