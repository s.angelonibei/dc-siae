package com.alkemytech.sophia.broadcasting.service.performing;

import java.util.List;

import com.alkemytech.sophia.broadcasting.dto.IndicatoreDTO;
import com.alkemytech.sophia.broadcasting.dto.performing.RSRequestMonitoraggioKPIDTO;


public interface RsIndicatoreService {

	List<IndicatoreDTO> listaInd(RSRequestMonitoraggioKPIDTO monitoraggioKPI);

}
