package com.alkemy.sophia.api.sophiamsingestionsap.dto;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.databind.util.StdConverter;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PeriodoConverter extends StdConverter<String, String> {

	@Override
	public String convert(String value) {
		String importo = value;
		try {
			Pattern pattern = Pattern.compile("^[0-9]{2,10} ([0-9\\-]+ ?[\\w]*)$");
			Matcher matcher = pattern.matcher(value);
			if (matcher.find())
				importo = matcher.group(1);

		} catch (Exception e) {
			log.debug(String.format("CSV field transforming error: cannot identify periodo: %s", value));
		}
		return importo;
	}

}
