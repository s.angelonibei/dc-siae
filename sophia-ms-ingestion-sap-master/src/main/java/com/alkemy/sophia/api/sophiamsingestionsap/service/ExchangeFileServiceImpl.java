package com.alkemy.sophia.api.sophiamsingestionsap.service;

import com.alkemy.sophia.api.sophiamsingestionsap.dto.response.MuleRequest;
import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.BdcAmountSapFile;
import com.alkemy.sophia.api.sophiamsingestionsap.model.repo.BdcAmountSapFileRepo;
import com.alkemy.sophia.api.sophiamsingestionsap.service.interfaces.ExchangeFileService;
import com.alkemy.sophia.api.sophiamsingestionsap.service.interfaces.IngestionSapService;
import com.alkemy.sophia.api.sophiamsingestionsap.sqs.BodySqs;
import com.alkemy.sophia.api.sophiamsingestionsap.sqs.MessageFactory;
import com.alkemy.sophia.api.sophiamsingestionsap.sqs.QueueState;
import com.alkemy.sophia.api.sophiamsingestionsap.sqs.SqsBaseMessage;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.aws.messaging.config.annotation.EnableSqs;
import org.springframework.cloud.aws.messaging.listener.SqsMessageDeletionPolicy;
import org.springframework.cloud.aws.messaging.listener.annotation.SqsListener;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.*;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@CommonsLog
@EnableSqs
public class ExchangeFileServiceImpl implements ExchangeFileService {
    private final IngestionSapService ingestionSapService;
    private final BdcAmountSapFileRepo bdcAmountSapFileRepo;
    private final MessageFactory messageFactory;


    @Value("${mule.host}")
    private String muleUrl;

    @Value("${mule.service}")
    private String muleService;


    @Value("${ingestion.reloadTime}")
    private String reloadTime;


    @Override
    @Transactional
    public String sendSqsMessageForCopyFile(BodySqs request) {
        //find record by identificativo.
        //check status
        //update db record

        BdcAmountSapFile sapFile = bdcAmountSapFileRepo.findByProcessIdAndStatus(request.getIdentificativo(), BdcAmountSapFile.STATO_FILE.RICHIESTO);

        Assert.notNull(sapFile,"Richiesta " + request.getIdentificativo() + " non trovata in stato RICHIESTO");

        sapFile.setStatus(BdcAmountSapFile.STATO_FILE.NOTIFICATO);

        //TODO: potrebbe essere necessario fare logica qui per l'applicativo saploader
        //splittando file e path
        sapFile.setPathSiae(request.getFileName());
        sapFile.setResponseDate(new Date());
        bdcAmountSapFileRepo.save(sapFile);

        messageFactory.createMessageAndSend(QueueState.to_process,request,null);

        return "OK";
    }

    @SqsListener(value = "#{'${default.environment}'+ '_' + '${sqs.input.completed}' + '_' + '${sqs.input.queue}'}", deletionPolicy = SqsMessageDeletionPolicy.ON_SUCCESS)
    public void receiveMessageCompleted(String message) {
        log.info("Received SQS message Completed" + message);


        SqsBaseMessage messageDecode = null;
        String identificativo = "";
        try {
            ObjectMapper mapper = new ObjectMapper();
            messageDecode = mapper.readValue(message, SqsBaseMessage.class);
            identificativo = messageDecode.getInput().getBody().getIdentificativo();
        } catch (IOException e) {
            e.printStackTrace();
        }

        log.info("Received SQS message processId" +  identificativo);
        try {
            BdcAmountSapFile fileSap = bdcAmountSapFileRepo.findByProcessIdAndStatus(identificativo, BdcAmountSapFile.STATO_FILE.NOTIFICATO);
            fileSap.setUploadDate(new Date());
            fileSap.setPathS3(messageDecode.getOutput().getUrl());
            fileSap.setPathSiae(messageDecode.getOutput().getSiaePath()+messageDecode.getOutput().getFilename());
            fileSap.setStatus(BdcAmountSapFile.STATO_FILE.DA_LAVORARE);
            bdcAmountSapFileRepo.saveAndFlush(fileSap);
            //get id from message


            //execute ingestion.
            ingestionSapService.ingestionSap(identificativo);

        }catch(Exception e){log.error("Errore nella ricerca della tripletta",e);}

    }

    @SqsListener(value = "#{'${default.environment}'+ '_' + '${sqs.input.failed}' + '_' + '${sqs.input.queue}'}", deletionPolicy = SqsMessageDeletionPolicy.ON_SUCCESS)
    @Transactional
    public void receiveMessageFailed(String message) {
        log.info("Received SQS message Failed" + message);


        SqsBaseMessage messageDecode = null;
        String identificativo = "";
        try {
            ObjectMapper mapper = new ObjectMapper();
            messageDecode = mapper.readValue(message, SqsBaseMessage.class);
            identificativo = messageDecode.getInput().getBody().getIdentificativo();
        } catch (IOException e) {
            e.printStackTrace();
        }

        log.info("Received SQS message processId" +  identificativo);
        try {
            BdcAmountSapFile fileSap = bdcAmountSapFileRepo.findByProcessIdAndStatus(identificativo, BdcAmountSapFile.STATO_FILE.NOTIFICATO);
            fileSap.setUploadDate(new Date());
            fileSap.setStatus(BdcAmountSapFile.STATO_FILE.ERRORE);
            fileSap.setErrorMessage("Errore nel processing del file: " + identificativo);
            bdcAmountSapFileRepo.saveAndFlush(fileSap);

        }catch(Exception e){log.error("Errore nella ricerca della tripletta",e);}
        //get id from message


        //execute ingestion.
//        ingestionSapService.ingestionSap((messageDecode.getBody()).getIdentificativo());

    }

    @Override
    public BdcAmountSapFile spotRequest(){
        List<BdcAmountSapFile.STATO_FILE> listaStati= new ArrayList<>();
        listaStati.add(BdcAmountSapFile.STATO_FILE.ERRORE);

        BdcAmountSapFile lastFile = bdcAmountSapFileRepo.findFirstByOrderByRequestDateDesc();
        boolean processo = false;

        if(lastFile == null) {
            processo = true;
        }else {
            switch (lastFile.getStatus()) {

                case RICHIESTO:
                case NOTIFICATO:
                case DA_LAVORARE:
                case IN_LAVORAZIONE:
                case LAVORATO:
                    //devono essere passate almeno N ore

                    DateTime richiesta = new DateTime(lastFile.getRequestDate());

                    //aggiunto parametro
                    int reloadTimeInt = Integer.parseInt(reloadTime);
                    if(richiesta.plusHours(reloadTimeInt).isBefore(DateTime.now()))
                        processo = true;

                    break;
                case NON_PROCESSATO:
                case ERRORE:
                    //non faccio controlli lo faccio rilanciare
                    processo = true;
                    break;
            }
        }
        if(processo)
            return requestSapFile();
        else
            return null;

    }

//    @Scheduled(cron = "0 * * * * *")
    @Scheduled(cron = "${ingestion.cron:0 0 4 * * MON}") //lunedi alle 4 di notte
    @Override
    public BdcAmountSapFile requestSapFile(){
        log.info("Starting Job RequestFile Sap");



        log.info("TODO: invio richiesta a mule");
        //https://stackoverflow.com/questions/1725863/why-cant-i-find-the-truststore-for-an-ssl-handshake




        log.info("salvataggio dati db");
        BdcAmountSapFile sapFileRequest = new BdcAmountSapFile();
        sapFileRequest.setStatus(BdcAmountSapFile.STATO_FILE.RICHIESTO);
        sapFileRequest.setRequestDate(new Date());
        sapFileRequest.setProcessId(UUID.randomUUID().toString().substring(0,15));
        bdcAmountSapFileRepo.saveAndFlush(sapFileRequest);

        try{

// ------------Inizio
            disableSslVerification();
// ------------Fine

            /*
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

            MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
            map.add("email", "first.last@example.com");

            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

             */
            RestTemplate restTemplate = new RestTemplate();
            String resource  = muleUrl+muleService;

            log.info("MULE url: "+resource);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

            map.add("userName", "Sophia");
            map.add("identificativo", sapFileRequest.getProcessId());
            map.add("data", sdf.format(sapFileRequest.getRequestDate()));
            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

            log.info("Parametri inviati a MULE:");
            map.forEach((k,v)->log.info("Key : " + k + " Val : " + v));
            ResponseEntity<String> response = restTemplate.postForEntity( resource, request , String.class );

            log.info("MULE response: "+response.getBody());

        }catch (Exception e){log.error("impossibile contattare mule",e);}


        log.info("End Job RequestFile Sap");
        return sapFileRequest;
    }


    public static void trustSelfSignedSSL() {
        try {
            SSLContext ctx = SSLContext.getInstance("TLS");
            X509TrustManager tm = new X509TrustManager() {

                public void checkClientTrusted(X509Certificate[] xcs, String string) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] xcs, String string) throws CertificateException {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            };
            ctx.init(null, new TrustManager[]{tm}, null);
            SSLContext.setDefault(ctx);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static void disableSslVerification() {
        try
        {
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }
            };

            // Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };

            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
    }

}
