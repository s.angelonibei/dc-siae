package com.alkemy.sophia.api.sophiamsingestionsap.sqs;

import lombok.Data;

@Data
public class SqsOutputPart {
    private String siaePath;
    private String filename;
    private String url;
}


/*
{
  "header": {
    "uuid": "44651481-9331-4830-8624-1370dead3af9",
    "timestamp": "2019-01-30T10:21:04.633Z",
    "queue": "dev_completed_saploader",
    "sender": "sap-loader",
    "hostname": "DESKTOP-TN1TAAO"
  },
  "input": {
    "header": {
      "uuid": "4f99c388-17c6-4928-839e-8cacc30f03c3",
      "timestamp": "2019-01-30T10:11:18.264Z",
      "queue": "dev_to_process_saploader",
      "sender": "sap-loader"
    },
    "body": {
      "fileName": "file_bello_da_sucare.csv"
    }
  },
  "output": {
    "siaePath": "/var/local/sophia/dev/saploader/siae/",
    "filename": "file_bello_da_sucare.csv",
    "url": "s3://siae-sophia-datalake/dev/broadcasting/saploader//file_bello_da_sucare.csv"
  }
}

{
  "header": {
    "uuid": "b17cbb0d-a2f0-46f4-aca7-037b50aeaf1d",
    "timestamp": "2018-11-23T10:05:36.649Z",
    "queue": "dev_completed_coreid",
    "sender": "sophia-coreid",
    "hostname": "siae_sophia_codifiche_dev"
  },
  "input": {
    "header": {
      "uuid": "6479449h-221d-4690-92c1-1c8dt57a6c57-zaza",
      "timestamp": "2018-10-23T00:00:00.000Z",
      "queue": "dev_to_process_coreid",
      "sender": "aws-console"
    },
    "body": {
      "version": "1",
      "compression": "guess",
      "configUrl": "s3://siae-sophia-identification/dev/config/dsr-bmat-v2.properties",
      "items": [
        {
          "inputUrl": "s3://siae-sophia-datalake/prod/original-source/spotify/201807/spotify_siae_IT_201807_basic-desktop_dsr.csv.gz",
          "outputUrl": "s3://siae-sophia-identification/dev/output/spotify_siae_IT_201807_basic-desktop_dsr.csv.gz"
        }
      ],
      "idDsr": "spotify_siae_IT_201807_basic-desktop_dsr",
      "dspCode": "SPO",
      "country": "ITA",
      "year": "2018",
      "month": "07"
    }
  },
  "output": {
    "items": [
      {
        "inputUrl": "s3://siae-sophia-datalake/prod/original-source/spotify/201807/spotify_siae_IT_201807_basic-desktop_dsr.csv.gz",
        "outputUrl": "s3://siae-sophia-identification/dev/output/spotify_siae_IT_201807_basic-desktop_dsr.csv.gz",
        "inputFileSize": 18418827,
        "outputFileSize": 19432431,
        "totalRecords": 154272,
        "identifiedRecords": 107080,
        "unidentifiedRecords": 47192,
        "invalidRecords": 42,
        "beginTimeMillis": 1542961363251,
        "endTimeMillis": 1542967536286,
        "elapsedTimeMillis": 6173035,
        "beginTime": "Nov 23, 2018 8:22:43 AM",
        "endTime": "Nov 23, 2018 10:05:36 AM",
        "elapsedTime": "1h42m53s35ms",
        "titleQueryPostings": 235079136,
        "titleQueries": 122405,
        "isrcCodeQueriesWithResults": 0,
        "iswcCodeQueries": 42616,
        "identifiedRecordsTitleArtist": 75255,
        "identifiedRecordsIswcCode": 31825,
        "isrcCodeQueries": 146014,
        "titleArtistQueriesWithResults": 93078,
        "iswcCodeQueriesWithResults": 31962,
        "identifiedRecordsIsrcCode": 0,
        "titleArtistQueryPostings": 9802478,
        "iswcCodePostings": 37880,
        "isrcCodePostings": 0,
        "titleArtistQueries": 112260
      }
    ]
  }
}
 */