package com.alkemy.sophia.api.sophiamsingestionsap.model.repo.querydsl;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.StringTemplate;

import java.util.Date;

public class Utils {

    public static BooleanExpression dateCompare(DateTimePath<Date> dateTimePath, Date date, TypeCompare typeCompare) {
        StringTemplate dbDate = Expressions.stringTemplate("DATE({0})", dateTimePath);
        StringTemplate compareDate = Expressions.stringTemplate("DATE({0})", date);
        switch (typeCompare) {
            case EQ:
                return dbDate.eq(compareDate);
            case GT:
                return dbDate.gt(compareDate);
            case GOE:
                return dbDate.goe(compareDate);
            case LT:
                return dbDate.lt(compareDate);
            case LOE:
                return dbDate.loe(compareDate);
            default:
                return dbDate.eq(compareDate);
        }
    }

    protected enum TypeCompare {
        EQ,
        GT,
        GOE,
        LT,
        LOE
    }
}
