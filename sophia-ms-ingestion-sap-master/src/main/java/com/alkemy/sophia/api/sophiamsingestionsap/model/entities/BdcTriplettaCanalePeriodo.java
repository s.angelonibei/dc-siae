package com.alkemy.sophia.api.sophiamsingestionsap.model.entities;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

@Data
@Entity
@Table(name = "BDC_TRIPLETTA_CANALE_PERIODO")
public class BdcTriplettaCanalePeriodo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "ATTIVO_DAL")
    private Date attivoDal;
    @Column(name = "ATTIVO_AL")
    private Date attivoAl;
    @Column(name = "PREALLOCAZIONE_PERCENTUALE")
    private Double preallocazionePercentuale;
    @Enumerated(EnumType.STRING)
    @Column(name = "SINGOLO_VS_VASCA")
    private SingoloVsVasca singoloVsVasca;
    @Column(name = "NUMERO_CANALI_VASCA")
    private Integer numeroCanaliVasca;
    @Column(name = "NOMI_CANALI_VASCA")
    private String nomiCanaliVasca;
    @Column(name = "DATA_DISATTIVAZIONE")
    private Date dataDisattivazione;
    @Column(name = "UTENTE_DISATTIVAZIONE")
    private String utenteDisattivazione;
    @Column(name = "UTENTE_INSERIMENTO")
    private String utenteInserimento;

    @ManyToOne
    @JoinColumn(name="TRIPLETTA_ID")
    private BdcTripletta bdcTripletta;

    @ManyToOne
    @JoinColumn(name = "CANALE_ID", referencedColumnName = "id", nullable = true)
    private BdcCanali bdcCanali;


    public enum SingoloVsVasca {
        Singolo,
        Vasca
    }

}

