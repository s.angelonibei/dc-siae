package com.alkemy.sophia.api.sophiamsingestionsap.dto;


import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

import java.util.Date;

@Data
public class SapDettaglioDto {

//    private Date dataAggiornamento;
    private String tripletta;
    private String periodo;
    private String tipoConto;
    private String importo;
    private String tipoErrore;

}
