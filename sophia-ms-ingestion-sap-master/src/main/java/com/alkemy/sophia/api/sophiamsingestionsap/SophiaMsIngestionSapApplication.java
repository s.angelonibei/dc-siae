package com.alkemy.sophia.api.sophiamsingestionsap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableEurekaClient
@EnableJpaRepositories
@EnableScheduling
public class SophiaMsIngestionSapApplication {


	public static void main(String[] args) {
		SpringApplication.run(SophiaMsIngestionSapApplication.class, args);
	}

}

