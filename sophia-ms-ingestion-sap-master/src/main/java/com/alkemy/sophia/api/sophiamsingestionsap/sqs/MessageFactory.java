package com.alkemy.sophia.api.sophiamsingestionsap.sqs;

import com.alkemy.sophia.api.sophiamsingestionsap.dto.response.MuleRequest;
import com.alkemy.sophia.api.sophiamsingestionsap.utils.Env;
import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.UUID;

@Component
@RequiredArgsConstructor
@CommonsLog
public class MessageFactory {
    private final QueueMessagingTemplate messagingTemplate;

    public <T extends SqsOutputPart> SqsBaseMessage createMessageAndSend(QueueState stato, BodySqs bodyPart, SqsBaseMessage prevMessage){

        SqsBaseMessage  message = new SqsBaseMessage();
        SqsHeaderPart header = new SqsHeaderPart();
        header.setUuid(UUID.randomUUID().toString());
        header.setTimestamp(new Date().getTime()+"");

        header.setSender(Env.appName);
        header.setQueue(String.join("_",Env.environment,stato.name(),Env.outputQueue));

        if(prevMessage != null)
            message.setInput(prevMessage);

        message.setBody(bodyPart);
        message.setHeader(header);

        messagingTemplate.convertAndSend(String.join("_",Env.environment,Env.serviceBusQueue),message);
//        messagingTemplate.convertAndSend(String.join("_",Env.environment,Env.inputQueue),message);


        return message;

    }
}
