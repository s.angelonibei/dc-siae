package com.alkemy.sophia.api.sophiamsingestionsap.model.repo;

import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.BdcCanali;
import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.BdcTriplettaCanalePeriodo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BdcCanaleRepo extends JpaRepository<BdcCanali, Long> {

    List<BdcCanali> findByBdcBroadcastersId(Long id_broadcaster);

}
