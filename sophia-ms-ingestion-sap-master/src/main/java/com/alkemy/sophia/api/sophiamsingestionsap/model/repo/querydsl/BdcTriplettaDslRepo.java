package com.alkemy.sophia.api.sophiamsingestionsap.model.repo.querydsl;

import com.alkemy.sophia.api.sophiamsingestionsap.dto.BdcTriplettaDto;
import com.alkemy.sophia.api.sophiamsingestionsap.dto.FilterTripletteDto;
import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.*;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.QueryModifiers;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.*;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.List;

@RequiredArgsConstructor
@CommonsLog
@Repository
public class BdcTriplettaDslRepo  {

    private final JPAQueryFactory queryFactory;
    private final QBdcTriplettaCanalePeriodo qBdcTriplettaCanalePeriodo = QBdcTriplettaCanalePeriodo.bdcTriplettaCanalePeriodo;
    private final QBdcTripletta qBdcTripletta = QBdcTripletta.bdcTripletta;
    private final QBdcCanali qBdcCanali = QBdcCanali.bdcCanali;


    public Long countTriplette(FilterTripletteDto filter){

        BooleanBuilder predicate = getPredicate(filter);

        Long result = queryFactory.select(
                qBdcTriplettaCanalePeriodo.bdcTripletta.id.countDistinct())
                .from(qBdcTriplettaCanalePeriodo)
                .where(predicate)
                .fetchOne();
        return result;

    }

    public List<BdcTriplettaDto> getTriplette(FilterTripletteDto filter, BdcTriplettaDto.ORDER_COLUMN orderColumn, boolean ascType){

        BooleanBuilder predicate = getPredicate(filter).and(qBdcTriplettaCanalePeriodo.dataDisattivazione.isNull());

        QueryModifiers limit = new QueryModifiers(filter.getPageSize(), filter.getPage()*filter.getPageSize());
        OrderSpecifier order = qBdcTriplettaCanalePeriodo.bdcTripletta.id.desc();
        ComparableExpressionBase p = null;

        switch (orderColumn){
            case tripletta:
                p=qBdcTriplettaCanalePeriodo.bdcTripletta.tripletta;
                break;
            case tipoDiritto:
                p=qBdcTriplettaCanalePeriodo.bdcTripletta.tipoDiritto;
                break;
            case singoloVsVasca:
                p=qBdcTriplettaCanalePeriodo.singoloVsVasca;
                break;
            case stato:
                p=qBdcTriplettaCanalePeriodo.bdcTripletta.stato;
                break;
            case nomeEmittente:
                p=qBdcTriplettaCanalePeriodo.bdcCanali.bdcBroadcasters.nome;
                break;
            case tipoEmittente:
                p=qBdcTriplettaCanalePeriodo.bdcCanali.bdcBroadcasters.tipoBroadcaster;
                break;
        }
        if (ascType) order = p.asc();
        else order = p.desc();

               //StringExpression importoMaturatoTrunc = Expressions.stringTemplate("group_concat({0} SEPARATOR ', ')", qBdcTriplettaCanalePeriodo.bdcCanali.nome);
        List<BdcTriplettaDto> result = queryFactory.select(Projections.bean(
                BdcTriplettaDto.class,
                qBdcTriplettaCanalePeriodo.bdcTripletta.id,
                qBdcTriplettaCanalePeriodo.bdcTripletta.tripletta,
                qBdcTriplettaCanalePeriodo.bdcTripletta.tipoDiritto,
                qBdcTriplettaCanalePeriodo.bdcTripletta.rifTemporaleIncassi,
                qBdcTriplettaCanalePeriodo.bdcTripletta.preallocazioneIncassiDa.as("giornoMeseDa"),
                qBdcTriplettaCanalePeriodo.bdcTripletta.preallocazioneIncassiA.as("giornoMeseA"),
                qBdcTriplettaCanalePeriodo.bdcTripletta.chiaveRif3,
                qBdcTriplettaCanalePeriodo.bdcTripletta.conto,
                qBdcTriplettaCanalePeriodo.bdcTripletta.attribuzione,
                qBdcTriplettaCanalePeriodo.bdcTripletta.descrizioneSap,
                qBdcTriplettaCanalePeriodo.bdcTripletta.dataInserimento,
                qBdcTriplettaCanalePeriodo.bdcTripletta.stato,
                qBdcTriplettaCanalePeriodo.singoloVsVasca,
                qBdcTriplettaCanalePeriodo.bdcCanali.bdcBroadcasters.nome.as("nomeEmittente"),
                qBdcTriplettaCanalePeriodo.bdcCanali.bdcBroadcasters.tipoBroadcaster.as("tipoEmittente"),
                Expressions.numberTemplate(Long.class," GROUP_CONCAT({0} )", qBdcTriplettaCanalePeriodo.bdcCanali.nome.prepend(" ")).stringValue().as("nomiCanali")
        )).from(qBdcTriplettaCanalePeriodo)
                .where(predicate)
                .groupBy(qBdcTriplettaCanalePeriodo.bdcTripletta.id,
                        qBdcTriplettaCanalePeriodo.bdcTripletta.tripletta,
                        qBdcTriplettaCanalePeriodo.bdcTripletta.tipoDiritto,
                        qBdcTriplettaCanalePeriodo.bdcTripletta.rifTemporaleIncassi,
                        qBdcTriplettaCanalePeriodo.bdcTripletta.preallocazioneIncassiDa,
                        qBdcTriplettaCanalePeriodo.bdcTripletta.preallocazioneIncassiA,
                        qBdcTriplettaCanalePeriodo.bdcTripletta.chiaveRif3,
                        qBdcTriplettaCanalePeriodo.bdcTripletta.conto,
                        qBdcTriplettaCanalePeriodo.bdcTripletta.attribuzione,
                        qBdcTriplettaCanalePeriodo.bdcTripletta.descrizioneSap,
                        qBdcTriplettaCanalePeriodo.bdcTripletta.dataInserimento,
                        qBdcTriplettaCanalePeriodo.bdcTripletta.stato,
                        qBdcTriplettaCanalePeriodo.bdcCanali.bdcBroadcasters.nome,
                        qBdcTriplettaCanalePeriodo.bdcCanali.bdcBroadcasters.tipoBroadcaster)
                .orderBy(order,qBdcTriplettaCanalePeriodo.bdcTripletta.id.desc())
                .restrict(limit).fetch();
        return result;
    }

    private BooleanBuilder getPredicate(FilterTripletteDto filter) {
        BooleanBuilder predicate = new BooleanBuilder();
        if(filter.getIdEmittente() != null)
            predicate.and(qBdcTriplettaCanalePeriodo.bdcCanali.bdcBroadcasters.id.eq(filter.getIdEmittente()));
        if(filter.getIdCanale() != null)
            predicate.and(qBdcTriplettaCanalePeriodo.bdcCanali.id.eq(filter.getIdCanale()));
        if(filter.getTipoDiritto() != null)
            predicate.and(qBdcTriplettaCanalePeriodo.bdcTripletta.tipoDiritto.eq(filter.getTipoDiritto()));
        if(filter.getTipoEmittente() != null)
            predicate.and(qBdcTriplettaCanalePeriodo.bdcCanali.bdcBroadcasters.tipoBroadcaster.eq(filter.getTipoEmittente()));
        if(filter.getStato() != null)
            predicate.and(qBdcTriplettaCanalePeriodo.bdcTripletta.stato.eq(filter.getStato()));
        if(StringUtils.hasLength(filter.getTripletta()))
            predicate.and(qBdcTriplettaCanalePeriodo.bdcTripletta.tripletta.like("%"+filter.getTripletta()+"%"));
        return predicate;
    }

}
