package com.alkemy.sophia.api.sophiamsingestionsap.dto;


import lombok.Data;

import java.util.List;

/*
Header SAP inutile
 */
@Data
public class HeaderSap {

    String CodiceProcesso;
    String Mittente;
    String Destinatario;
    String TipoElaborazione;
    String UserName;
    List<MessaggioDto> Messaggi;
}
