package com.alkemy.sophia.api.sophiamsingestionsap.utils;

import org.joda.time.DateTime;

public class DateUtils {

    public static DateTime latest(DateTime ... list)
    {
        DateTime result = list[0];
        for (DateTime dTmp : list) {
            if(dTmp != null)
                result = dTmp.compareTo(result) > 0 ? dTmp : result;
        }

        return result;
    }

    public static DateTime oldest(DateTime ... list)
    {
        DateTime result = list[0];
        for (DateTime dTmp : list) {
            if(dTmp != null)
                result = dTmp.compareTo(result) < 0 ? dTmp : result;
        }

        return result;
    }


}
