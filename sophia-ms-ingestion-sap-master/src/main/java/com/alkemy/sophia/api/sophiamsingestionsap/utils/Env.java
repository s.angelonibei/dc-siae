package com.alkemy.sophia.api.sophiamsingestionsap.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Env {


    public static String environment;
    public static String appName;
    public static String serviceBusQueue;
    public static String outputQueue;
    public static String inputQueue;


    @Value("${default.environment:dev}")
    public void setEnvironment(String environment) {
        Env.environment = environment;
    }

    @Value("${sqs.app-name:UNKNOW}")
    public void setAppName(String appName) {
        Env.appName = appName;
    }

    @Value("${sqs.sb-queue}")
    public void setServiceBusQueue(String defQueue) {
        Env.serviceBusQueue = defQueue;
    }

    @Value("${sqs.output.queue}")
    public void setOutputQueue(String appQueue) {
        Env.outputQueue = appQueue;
    }

    @Value("${sqs.input.queue}")
    public void setInputQueue(String appQueue) {
        Env.inputQueue = appQueue;
    }
}
