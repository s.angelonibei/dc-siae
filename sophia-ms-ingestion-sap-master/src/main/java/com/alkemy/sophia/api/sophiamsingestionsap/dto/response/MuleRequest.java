package com.alkemy.sophia.api.sophiamsingestionsap.dto.response;


import com.alkemy.sophia.api.sophiamsingestionsap.dto.HeaderSap;
import com.alkemy.sophia.api.sophiamsingestionsap.sqs.SqsOutputPart;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Date;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MuleRequest  {
    HeaderSap header;
    String identificativo;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    Date data;
    String path;
}
