package com.alkemy.sophia.api.sophiamsingestionsap.model.repo.querydsl;

import com.alkemy.sophia.api.sophiamsingestionsap.dto.SapDettaglioIngestionDto;
import com.alkemy.sophia.api.sophiamsingestionsap.dto.SapListaIngestionDto;
import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.*;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.QueryModifiers;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.*;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;

@RequiredArgsConstructor
@CommonsLog
@Repository
public class BdcIngestionRepo {
    private final JPAQueryFactory queryFactory;
    private final QBdcTriplettaCanalePeriodo qBdcTriplettaCanalePeriodo = QBdcTriplettaCanalePeriodo.bdcTriplettaCanalePeriodo;
    private final QBdcTripletta qBdcTripletta = QBdcTripletta.bdcTripletta;
    private final QBdcCanali qBdcCanali = QBdcCanali.bdcCanali;
    private final QBdcBroadcasters qBdcBroadcasters = QBdcBroadcasters.bdcBroadcasters;
    private final QBdcAmountSapFile qBdcAmountSapFile = QBdcAmountSapFile.bdcAmountSapFile;
    private final QBdcIngestionSap qBdcIngestionSap = QBdcIngestionSap.bdcIngestionSap;

    NumberExpression<Long> tripletteOk = new CaseBuilder()
            .when(qBdcIngestionSap.stato.eq(BdcIngestionSap.STATO.OK))
            .then(1l)
            .otherwise(0l);

    NumberExpression<Long> tripletteKo= new CaseBuilder()
            .when(qBdcIngestionSap.stato.ne(BdcIngestionSap.STATO.OK))
            .then(1l)
            .otherwise(0l);


    public List<BdcTriplettaCanalePeriodo> allRefFromTriplettaAndPeriod(Long idTripletta, Date annoInizio, Date annofine){


        //check period
        //https://stackoverflow.com/questions/143552/comparing-date-ranges
        //        SELECT *
        //                FROM periods
        //        WHERE range_start <= @check_period_end
        //        AND range_end >= @check_period_start

        BooleanBuilder predicate = new BooleanBuilder();

        predicate.and(qBdcTriplettaCanalePeriodo.bdcTripletta.id.eq(idTripletta));
        predicate.and(qBdcTriplettaCanalePeriodo.attivoDal.coalesce(annofine).asDate().loe(annofine));
        predicate.and(qBdcTriplettaCanalePeriodo.attivoAl.coalesce(annoInizio).asDate().goe(annoInizio));
        predicate.and(qBdcTriplettaCanalePeriodo.dataDisattivazione.isNull());

        List<BdcTriplettaCanalePeriodo> result = queryFactory.select(qBdcTriplettaCanalePeriodo)
                .from(qBdcTriplettaCanalePeriodo)
                .where(predicate)
                .fetch();


        return result;


    }



    public List<SapListaIngestionDto> allDtoOrderByDate(Date fromDate, Date toDate, SapListaIngestionDto.ORDER_COLUMN orderColumn,boolean ascType, Long size, Long page){


        BooleanBuilder predicate = new BooleanBuilder();

        if(fromDate != null)
            predicate.and(Utils.dateCompare(qBdcAmountSapFile.requestDate, fromDate, Utils.TypeCompare.GOE));
        if(toDate != null)
            predicate.and(Utils.dateCompare(qBdcAmountSapFile.requestDate, toDate, Utils.TypeCompare.LOE));


        QueryModifiers limit = new QueryModifiers(size, page*size);

        OrderSpecifier order = qBdcAmountSapFile.id.desc();

        ComparableExpressionBase p = null;
        switch (orderColumn){

            case nomeFile:
                p = qBdcAmountSapFile.id;
                break;
            case dataAggiornamento:
                p = qBdcAmountSapFile.requestDate;
                break;
            case esitoIngestion:
                p = qBdcAmountSapFile.status;
                break;
            case tipoErrore:
                p = qBdcAmountSapFile.status;
                break;
            case numTripletteOk:
                p = tripletteOk.sum();
                break;
            case numTripletteKo:
                p = tripletteKo.sum();
                break;
        }
        if (ascType) order = p.asc();
        else order = p.desc();


        List<SapListaIngestionDto> result = queryFactory.select(Projections.bean(
                SapListaIngestionDto.class,
                qBdcAmountSapFile.id.as("idIngestion"),
                qBdcAmountSapFile.pathS3.as("nomeFile"),
                qBdcAmountSapFile.requestDate.as("dataAggiornamento"),
                qBdcAmountSapFile.status.as("esitoIngestion"),
                qBdcAmountSapFile.errorMessage.as("tipoErrore"),
                tripletteOk.sum().as("numTripletteOk"),
                tripletteKo.sum().as("numTripletteKo")
        )).from(qBdcIngestionSap).rightJoin( qBdcIngestionSap.bdcAmountSapFile,qBdcAmountSapFile )
        		.where(predicate)
                .groupBy(qBdcAmountSapFile.id,
                        qBdcAmountSapFile.pathSiae,
                        qBdcAmountSapFile.requestDate,
                        qBdcAmountSapFile.status)
                .orderBy(order,qBdcAmountSapFile.id.desc())
                .restrict(limit).fetch();

        return result;

    }

    public Long countAmountSapFile(Date fromDate, Date toDate){

        BooleanBuilder predicate = new BooleanBuilder();


        if(fromDate != null)
            predicate.and(Utils.dateCompare(qBdcAmountSapFile.requestDate, fromDate, Utils.TypeCompare.GOE));
        if(toDate != null)
            predicate.and(Utils.dateCompare(qBdcAmountSapFile.requestDate, toDate, Utils.TypeCompare.LOE));


        Long result = queryFactory.select(
                qBdcAmountSapFile.id.countDistinct())
                .from(qBdcIngestionSap)
                .rightJoin( qBdcIngestionSap.bdcAmountSapFile,qBdcAmountSapFile )
                .where(predicate)
                .fetchOne();



        return result;

    }

    public List<SapDettaglioIngestionDto> allIngestionSap(Long id, String tripletta,  SapDettaglioIngestionDto.ORDER_COLUMN orderColumn, boolean ascType,boolean withoutLimit, Long size, Long page){


        BooleanBuilder predicate = new BooleanBuilder();
        predicate.and(qBdcIngestionSap.bdcAmountSapFile.id.eq(id));

        if(!StringUtils.isEmpty(tripletta))
            predicate.and(qBdcIngestionSap.tripletta.like("%"+tripletta+"%")) ;


        OrderSpecifier order = qBdcIngestionSap.id.desc();

        ComparableExpressionBase p = null;
        switch (orderColumn) {
            case tripletta:
                p = qBdcIngestionSap.tripletta;
                break;
            case tipoTripletta:
                p = qBdcTriplettaCanalePeriodo.singoloVsVasca;
                break;
            case canale:
                p = qBdcCanali.nome;
                break;
            case periodo:
                p = qBdcIngestionSap.dataDocumento;
                break;
            case tipoDiritto:
                p = qBdcIngestionSap.tipoDiritto;
                break;
            case importo:
                p = qBdcIngestionSap.importoRelativo;
                break;
            case dataDa:
                p = qBdcIngestionSap.dataDa;
                break;
            case dataA:
                p = qBdcIngestionSap.dataA;
                break;
            case regolaPreallocazione:
                p = qBdcIngestionSap.preallocazionePercentuale;
                break;
            case errore:
                p = qBdcIngestionSap.errore;
                break;
            case stato:
                p = qBdcIngestionSap.stato;
                break;
            case emittente:
                p = qBdcTriplettaCanalePeriodo.bdcCanali.bdcBroadcasters.nome;
                break;
            case tipoEmittente:
                p = qBdcTriplettaCanalePeriodo.bdcCanali.bdcBroadcasters.tipoBroadcaster;
                break;
        }

        if (ascType) order = p.asc();
        else order = p.desc();


        JPAQuery<SapDettaglioIngestionDto> queryWithoutLimit = queryFactory.select(Projections.bean(
                SapDettaglioIngestionDto.class,
                qBdcIngestionSap.tripletta.as("tripletta"),
                qBdcTriplettaCanalePeriodo.singoloVsVasca.as("tipoTripletta"),
                qBdcBroadcasters.nome.as("emittente"),
                qBdcBroadcasters.tipoBroadcaster.as("tipoEmittente"),
//                qBdcCanali.nome.as("canale"),
                Expressions.numberTemplate(Long.class," GROUP_CONCAT({0} )", qBdcTriplettaCanalePeriodo.bdcCanali.nome.prepend(" ")).stringValue().as("canale"),
                qBdcIngestionSap.dataDocumento.as("periodo"),
                qBdcIngestionSap.tipoDiritto.as("tipoDiritto"),
                qBdcIngestionSap.importoRelativo.as("importo"),
                qBdcIngestionSap.dataDa.as("dataDa"),
                qBdcIngestionSap.dataA.as("dataA"),
                qBdcIngestionSap.preallocazionePercentuale.as("regolaPreallocazione"),
                qBdcIngestionSap.stato.as("stato"),
                qBdcIngestionSap.errore.as("errore"),
                qBdcIngestionSap.recordSap.as("recordSap"),
                qBdcIngestionSap.attribuzione.as("attribuzione"),
                qBdcIngestionSap.chiaveRif3.as("chiaveRif3"),
                qBdcIngestionSap.conto.as("conto")
        )).from(qBdcIngestionSap)

                .leftJoin(qBdcIngestionSap.bdcTripletta, qBdcTripletta)
                .leftJoin(qBdcTripletta.bdcTriplettaCanalePeriodoList, qBdcTriplettaCanalePeriodo)
                .leftJoin(qBdcTriplettaCanalePeriodo.bdcCanali, qBdcCanali)
                .leftJoin(qBdcCanali.bdcBroadcasters, qBdcBroadcasters)
                .where(predicate)
                .groupBy(
                        //prenderà il primo... bella Mysql
                        qBdcIngestionSap.id
//                        qBdcIngestionSap.tripletta,

//                        qBdcTriplettaCanalePeriodo.singoloVsVasca,
//                        qBdcTriplettaCanalePeriodo.bdcCanali.bdcBroadcasters.nome,
//                        qBdcTriplettaCanalePeriodo.bdcCanali.bdcBroadcasters.tipoBroadcaster,
//                        qBdcCanali.nome,
//                        qBdcIngestionSap.dataDocumento,
//                        qBdcIngestionSap.tipoDiritto,
//                        qBdcIngestionSap.importoRelativo,
//                        qBdcIngestionSap.dataDa,
//                        qBdcIngestionSap.dataA,
//                        qBdcIngestionSap.preallocazionePercentuale,
//                        qBdcIngestionSap.stato,
//                        qBdcIngestionSap.errore,
//                        qBdcIngestionSap.recordSap,
//                        qBdcIngestionSap.attribuzione,
//                        qBdcIngestionSap.chiaveRif3,
//                        qBdcIngestionSap.conto
                )
                .orderBy(order,qBdcIngestionSap.id.desc());


        if(withoutLimit)
            return queryWithoutLimit.fetch();

        else{
            QueryModifiers limit = new QueryModifiers(size, page*size);
            List<SapDettaglioIngestionDto> result = queryWithoutLimit
                    .restrict(limit).fetch();

            return result;
        }



    }

    public Long countIngestionSap(Long id, String tripletta){

        BooleanBuilder predicate = new BooleanBuilder();
        predicate.and(qBdcIngestionSap.bdcAmountSapFile.id.eq(id));

        if(!StringUtils.isEmpty(tripletta))
            predicate.and(qBdcIngestionSap.tripletta.like("%"+tripletta+"%"));

        Long result = queryFactory.select(
                qBdcIngestionSap.count())
                .from(qBdcIngestionSap)
                .where(predicate)
                .fetchOne();

        return result;

    }
}
