package com.alkemy.sophia.api.sophiamsingestionsap.dto.response;


import lombok.Data;

@Data
public abstract class PaginatorResponse {
    private Long count;
}
