package com.alkemy.sophia.api.sophiamsingestionsap.model.repo;

import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.BdcIngestionSap;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BdcIngestionSapRepo extends JpaRepository<BdcIngestionSap, Long> {
}
