package com.alkemy.sophia.api.sophiamsingestionsap.sqs;

public enum QueueState {
    to_process,
    started,
    completed,
    failed
}
