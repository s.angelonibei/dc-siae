package com.alkemy.sophia.api.sophiamsingestionsap.model.entities;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "BDC_AMOUNT_SAP_FILE")
public class BdcAmountSapFile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;
    @Column(name = "PATH_S3")
    private String pathS3;
    @Column(name = "PATH_SIAE")
    private String pathSiae;
    @Column(name = "PROCESS_ID")
    private String processId;
    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS")
    private STATO_FILE status;
    @Column(name = "REQUEST_DATE")
    private Date requestDate;
    @Column(name = "RESPONSE_DATE")
    private Date responseDate;
    @Column(name = "UPLOAD_DATE")
    private Date uploadDate;

    @Column(name = "ERROR_MESSAGE")
    private String errorMessage;

    @OneToMany(mappedBy = "bdcAmountSapFile")
    private List<BdcIngestionSap> bdcIngestionSapList;


    public enum STATO_FILE{
        RICHIESTO, //file richiesto a sap
        NOTIFICATO, //file notificato come pronto da SAP
        DA_LAVORARE, //file pronto per essere elaborato
        IN_LAVORAZIONE, //file in lavorazione
        LAVORATO, //file lavorato
        NON_PROCESSATO, //file non processato perche' presenti altri lanci futuri
        ERRORE  //ingestion in errore

    }

}
