package com.alkemy.sophia.api.sophiamsingestionsap.service.interfaces;

import com.alkemy.sophia.api.sophiamsingestionsap.dto.response.MuleRequest;
import com.alkemy.sophia.api.sophiamsingestionsap.dto.response.MuleResponse;
import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.BdcAmountSapFile;
import com.alkemy.sophia.api.sophiamsingestionsap.sqs.BodySqs;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Transactional;

public interface ExchangeFileService {


    //job ogni settimana per richiesta sap

    //gestione della response di sap
    String sendSqsMessageForCopyFile(BodySqs request);

    void receiveMessageCompleted(String message);
    void receiveMessageFailed(String message);

    BdcAmountSapFile spotRequest();

    BdcAmountSapFile requestSapFile();
}
