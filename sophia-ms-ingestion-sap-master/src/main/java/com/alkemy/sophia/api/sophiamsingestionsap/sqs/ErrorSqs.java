package com.alkemy.sophia.api.sophiamsingestionsap.sqs;


import com.alkemy.sophia.api.sophiamsingestionsap.dto.HeaderSap;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class ErrorSqs {
    private String code;
    private String description;
    private String stackTrace;
}
