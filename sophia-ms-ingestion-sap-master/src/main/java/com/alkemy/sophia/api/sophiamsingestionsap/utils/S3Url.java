package com.alkemy.sophia.api.sophiamsingestionsap.utils;

import lombok.Data;

@Data
public class S3Url {

    private String bucket;
    private String key;

    public S3Url(String url) {
        super();
        if (!url.startsWith("s3://")) {
            throw new IllegalArgumentException(String
                    .format("invalid url \"%s\"", url));
        }
        this.bucket = url.substring(5, url.indexOf('/', 5));
        this.key = url.substring(1 + url.indexOf('/', 5));
    }

    public S3Url(String bucket, String key) {
        super();
        this.bucket = bucket;
        this.key = key;
    }

    @Override
    public int hashCode() {
        int result = 31 + (null == bucket ? 0 : bucket.hashCode());
        return 31 * result + (null == key ? 0 : key.hashCode());
    }

    @Override
    public boolean equals(Object object) {
        if (this == object)
            return true;
        if (null == object)
            return false;
        if (getClass() != object.getClass())
            return false;
        final S3Url other = (S3Url) object;
        if (null == bucket) {
            if (null != other.bucket)
                return false;
        } else if (!bucket.equals(other.bucket))
            return false;
        if (null == key) {
            return null == other.key;
        } else return key.equals(other.key);
    }

    @Override
    public String toString() {
        return String.format("s3://%s/%s", bucket, key);
    }

}