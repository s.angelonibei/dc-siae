package com.alkemy.sophia.api.sophiamsingestionsap.dto;


import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.BdcBroadcasters;
import lombok.Data;

import java.util.Date;

@Data
public class BdcBroadcasterDto {


    private Long id;
    private String nome;
    private Date dataCreazione;
    private BdcBroadcasters.TIPO_CANALE tipoBroadcaster;
}
