package com.alkemy.sophia.api.sophiamsingestionsap.dto.response;

import com.alkemy.sophia.api.sophiamsingestionsap.dto.BdcTriplettaDto;
import lombok.Data;

import java.util.List;

@Data
public class RiepilogoTripletteResponse extends PaginatorResponse {

    List<BdcTriplettaDto> list;
}
