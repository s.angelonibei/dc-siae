package com.alkemy.sophia.api.sophiamsingestionsap.dto.response;

import com.alkemy.sophia.api.sophiamsingestionsap.dto.SapListaIngestionDto;
import lombok.Data;

import java.util.List;

@Data
public class ListaIngestioneResponse extends PaginatorResponse {

    List<SapListaIngestionDto> list;
}
