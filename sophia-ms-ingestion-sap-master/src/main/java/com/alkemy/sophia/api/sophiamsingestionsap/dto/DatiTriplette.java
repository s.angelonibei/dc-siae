package com.alkemy.sophia.api.sophiamsingestionsap.dto;

import lombok.Data;
import org.joda.time.DateTime;


@Data
public class DatiTriplette {
    private DateTime dataInizio;
    private DateTime dataFine;
    private double numGiorni;
}
