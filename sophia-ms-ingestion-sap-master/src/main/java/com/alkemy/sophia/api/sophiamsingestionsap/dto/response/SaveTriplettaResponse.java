package com.alkemy.sophia.api.sophiamsingestionsap.dto.response;


import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.BdcTripletta;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SaveTriplettaResponse {
	
	EsitoSaveTripletta esito;

	BdcTripletta tripletta;
}