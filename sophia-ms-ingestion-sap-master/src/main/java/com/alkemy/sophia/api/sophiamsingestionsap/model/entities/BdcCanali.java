package com.alkemy.sophia.api.sophiamsingestionsap.model.entities;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

@Data
@Entity
@Table(name = "bdc_canali")
public class BdcCanali {

    @Id
    @Column(name = "id")
    private Long id;
    @Column(name = "nome")
    private String nome;
    @Column(name = "generalista")
    private Boolean generalista;
    @Column(name = "active")
    private Boolean active;
    @Column(name = "special_radio")
    private Boolean specialRadio;
    @Column(name = "data_inizio_valid")
    private Date dataInizioValid;
    @Column(name = "data_fine_valid")
    private Date dataFineValid;
    @Column(name = "data_creazione")
    private Date dataCreazione;
    @Column(name = "data_ultima_modifica")
    private Date dataUltimaModifica;
    @Column(name = "utente_ultima_modifica")
    private String utenteUltimaModifica;

    @ManyToOne
    @JoinColumn(name = "id_broadcaster", referencedColumnName = "id", nullable = false)
    private BdcBroadcasters bdcBroadcasters;

    @Column(name = "tipo_canale")
    @Enumerated(EnumType.STRING)
    private TIPO_CANALE tipoCanale;


    public enum TIPO_CANALE {
        RADIO,
        TELEVISIONE
    }

}
