package com.alkemy.sophia.api.sophiamsingestionsap.model.entities;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "BDC_INGESTION_SAP")
public class BdcIngestionSap {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "TESTO_TESTATA_DOCUMENTO")
    private String testoTestataDocumento;
    @Column(name = "IMPORTO_TOTALE_DOCUMENTO")
    private String importoTotaleDocumento;
    @Column(name = "IMPORTO_RELATIVO")
    private Double importoRelativo;

    @Column(name = "DATA_DOCUMENTO")
    private String dataDocumento;
    @Column(name = "DATA_DA")
    private Date dataDa;
    @Column(name = "DATA_A")
    private Date dataA;
    @Column(name = "ES_MESE")
    private String esMese;
    @Column(name = "DATA_DI_REGISTRAZIONE")
    private Date dataDiRegistrazione;
    @Column(name = "CHIAVE_RIF2")
    private String chiaveRif2;
    @Column(name = "TESTO")
    private String testo;
    @Column(name = "DATA_DI_INSERIMENTO")
    private Date dataDiInserimento;
    @Column(name = "TIPO_DI_INSERIMENTO")
    private String tipoDiInserimento;
    @Column(name = "PREALLOCAZIONE_PERCENTUALE")
    private Double preallocazionePercentuale;
    @Enumerated(EnumType.STRING)
    @Column(name = "STATO")
    private STATO stato;
    @Column(name = "ERRORE")
    private String errore;
    @Column(name = "TRIPLETTA")
    private String tripletta;

    @Column(name = "RECORD_SAP")
    private String recordSap;

    @Enumerated(EnumType.STRING)
    @Column(name = "TIPO_DIRITTO")
    private Diritto tipoDiritto;

    @Column(name = "ATTRIBUZIONE")
    private String attribuzione;

    @Column(name = "CHIAVE_RIF3")
    private String chiaveRif3;

    @Column(name = "CONTO")
    private String conto;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="AMOUNT_SAP_FILE_ID")
    private BdcAmountSapFile bdcAmountSapFile;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="TRIPLETTA_CANALE_PERIODO_ID")
    private BdcTriplettaCanalePeriodo bdcTriplettaCanalePeriodo;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="TRIPLETTA_ID")
    private BdcTripletta bdcTripletta;

    public enum STATO{
        OK,
        ERR_TRIPLETTA,
        ERR_CSV_RECORD

    }


}
