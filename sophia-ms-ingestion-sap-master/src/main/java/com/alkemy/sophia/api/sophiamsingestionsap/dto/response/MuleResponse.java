package com.alkemy.sophia.api.sophiamsingestionsap.dto.response;


import com.alkemy.sophia.api.sophiamsingestionsap.dto.EsitoMule;
import com.alkemy.sophia.api.sophiamsingestionsap.dto.HeaderSap;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Date;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MuleResponse {
    HeaderSap header;
    private EsitoMule esito;
}
