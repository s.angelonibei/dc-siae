package com.alkemy.sophia.api.sophiamsingestionsap.dto;


import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.BdcAmountSapFile;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

import java.util.Date;

@Data
public class SapIngestionResponse {

    private BdcAmountSapFile.STATO_FILE stato;
    private String esito;
    private Date data;

}
