package com.alkemy.sophia.api.sophiamsingestionsap.dto.response;

import com.alkemy.sophia.api.sophiamsingestionsap.dto.SapDettaglioIngestionDto;
import com.alkemy.sophia.api.sophiamsingestionsap.dto.SapListaIngestionDto;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class ListaIngestioneDettaglioResponse extends PaginatorResponse {

    String nomeFile;
    Date dataAggiornamento;

    List<SapDettaglioIngestionDto> list;
}
