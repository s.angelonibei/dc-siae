package com.alkemy.sophia.api.sophiamsingestionsap.model.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Data
@Entity
@Table(name = "BDC_TRIPLETTA")
public class BdcTripletta {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "TRIPLETTA")
    private String tripletta;
    @Column(name = "ATTRIBUZIONE")
    private Long attribuzione;
    @Column(name = "CHIAVE_RIF3")
    private String chiaveRif3;
    @Column(name = "CONTO")
    private String conto;
    @Enumerated(EnumType.STRING)
    @Column(name = "TIPO_DIRITTO")
    private Diritto tipoDiritto;
    @Column(name = "RIF_TEMPORALE_INCASSI")
    private String rifTemporaleIncassi;
    @Column(name = "PREALLOCAZIONE_INCASSI_DA")
    private String preallocazioneIncassiDa;
    @Column(name = "PREALLOCAZIONE_INCASSI_A")
    private String preallocazioneIncassiA;
    @Column(name = "DESCRIZIONE_SAP")
    private String descrizioneSap;
    @Column(name = "DATA_INSERIMENTO")
    private Date dataInserimento;
    @Column(name = "TIPO_INSERIMENTO")
    private String tipoInserimento;
    @Column(name = "AMMOUNT_SAP_FILE_ID")
    private Long ammountSapFileId;
    @Column(name = "STATO")
    @Enumerated(EnumType.STRING)
    private Stato stato;

    @OneToMany(mappedBy = "bdcTripletta")
    private Collection<BdcTriplettaCanalePeriodo> bdcTriplettaCanalePeriodoList;

    public enum Stato {
        VALIDA,
        DISATTIVA
    }



}
