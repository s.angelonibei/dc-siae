package com.alkemy.sophia.api.sophiamsingestionsap.model.entities;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;

@Data
@Entity
@Table(name = "bdc_broadcasters")
public class BdcBroadcasters {

    @Id
    @Column(name = "id")
    private Long id;
    @Column(name = "nome")
    private String nome;
    @Column(name = "data_creazione")
    private Date dataCreazione;
    
    @OneToMany(mappedBy = "bdcBroadcasters")
    private Collection<BdcCanali> bdcCanalisById;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_broadcaster")
    private TIPO_CANALE tipoBroadcaster;

//    @Column(name = "image")
//    private byte[] image;

    public enum TIPO_CANALE {
        RADIO,
        TELEVISIONE
    }

}
