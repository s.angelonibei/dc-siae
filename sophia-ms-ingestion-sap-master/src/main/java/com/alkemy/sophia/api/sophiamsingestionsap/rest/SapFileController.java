package com.alkemy.sophia.api.sophiamsingestionsap.rest;


import com.alkemy.sophia.api.sophiamsingestionsap.dto.EsitoMule;
import com.alkemy.sophia.api.sophiamsingestionsap.dto.HeaderSap;
import com.alkemy.sophia.api.sophiamsingestionsap.dto.MessaggioDto;
import com.alkemy.sophia.api.sophiamsingestionsap.dto.SapIngestionResponse;
import com.alkemy.sophia.api.sophiamsingestionsap.dto.response.*;
import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.BdcAmountSapFile;

import com.alkemy.sophia.api.sophiamsingestionsap.service.interfaces.ExchangeFileService;
import com.alkemy.sophia.api.sophiamsingestionsap.service.interfaces.IngestionSapService;
import com.alkemy.sophia.api.sophiamsingestionsap.sqs.BodySqs;
import com.alkemy.sophia.api.sophiamsingestionsap.sqs.SqsBaseMessage;
import com.alkemy.sophia.api.sophiamsingestionsap.sqs.SqsHeaderPart;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mysema.commons.lang.Assert;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;

import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.UUID;

@RestController
@CommonsLog
@RequiredArgsConstructor
@RequestMapping("")
public class SapFileController {
    private final IngestionSapService ingestionSapService;
    private final ExchangeFileService exchangeFileService;

    private final Long PAGE_SIZE = 50l;

    @Value("${ingestion.reloadTime}")
    private String reloadTime;

    /**
     * Metodo di test per avviare l'ingestion di un determinato processo
     * @param id
     * @return
     */
    @GetMapping(value = "/test/ingestion/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody SapIngestionResponse getFile(@PathVariable String id) {

        return ingestionSapService.ingestionSap(id);
    }

    /**
     * Metodo di test per generare la richiesta fuori dallo scheduler
     * @param
     * @return
     * @throws Exception
     */
    @GetMapping(value = "/test/ingestion/init", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody BdcAmountSapFile getFile() throws Exception{


        return exchangeFileService.requestSapFile();
    }

    /**
     * Metodo di test per generare la richiesta fuori dallo scheduler
     * @param
     * @return
     * @throws Exception
     */
    @GetMapping(value = "/ingestion/init", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody BdcAmountSapFile spotRequest() throws Exception{


        BdcAmountSapFile result = exchangeFileService.spotRequest();
        if(result == null){
         result = new   BdcAmountSapFile();
         result.setErrorMessage("Richiesta già sottomessa nelle ultime "+reloadTime+" ore");
        }
            return result;
    }

    @GetMapping(value = "/ingestion-files", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ListaIngestioneResponse getIngestion(
            @RequestParam(value = "fromDate", required = false)  @DateTimeFormat(pattern="dd-MM-yyyy") Date fromDate,
            @RequestParam(value = "toDate", required = false)  @DateTimeFormat(pattern="dd-MM-yyyy") Date toDate,
            @RequestParam(value = "order") String order,
            @RequestParam(value = "page") Long page,
            @RequestParam(value = "count") Boolean count,
            @RequestParam(value = "pageSize", required = false) Long pageSize) {

        if(pageSize == null)
            pageSize = PAGE_SIZE;

		ListaIngestioneResponse response = ingestionSapService.listaIngestion(fromDate, toDate, order, page, count, pageSize);
        
        return response;
    }

    @GetMapping(value = "/ingestion-files/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ListaIngestioneDettaglioResponse getIngestion(@PathVariable String id,
                                                  @RequestParam(value = "order") String order,
                                                  @RequestParam(value = "tripletta", required = false) String tripletta,
                                                  @RequestParam(value = "page") Long page,
                                                  @RequestParam(value = "count") Boolean count,
                                                  @RequestParam(value = "pageSize", required = false) Long pageSize) {
        if(pageSize == null)
            pageSize = PAGE_SIZE;

        return ingestionSapService.listaIngestionDettaglio( Long.parseLong(id),tripletta,  order, page, count, pageSize);
    }


    @GetMapping(value = "/ingestion-files/{id}/output-file", produces = MediaType.TEXT_PLAIN_VALUE)
    public void getIngestionSapFile(@PathVariable Long id, @RequestParam(value = "order") String order, HttpServletResponse response) throws Exception {



        BdcAmountSapFile amountFile = ingestionSapService.ingestionRecord(id);
        String filename = amountFile.getPathS3().substring(amountFile.getPathS3().lastIndexOf("/")+1);

        response.addHeader("Content-disposition", "attachment;filename=out_" + filename);
        response.setContentType("txt/plain");


        // Copy the stream to the response's body stream.
        InputStream is =  ingestionSapService.getIngestionCsv(id,order);
        IOUtils.copy(is, response.getOutputStream());
        response.flushBuffer();

    }

    @GetMapping(value = "/ingestion-files/{id}/input-file", produces = MediaType.TEXT_PLAIN_VALUE)
    public void getIngestionSapFile(@PathVariable Long id,  HttpServletResponse response) throws Exception {


        BdcAmountSapFile amountFile = ingestionSapService.ingestionRecord(id);
        String filename = amountFile.getPathS3().substring(amountFile.getPathS3().lastIndexOf("/")+1);

        response.addHeader("Content-disposition", "attachment;filename=" + filename);
        response.setContentType("txt/plain");


        // Copy the stream to the response's body stream.
        InputStream is = ingestionSapService.getSapFile(id);
        IOUtils.copy(is, response.getOutputStream());
        response.flushBuffer();

    }


    @PostMapping(path = "/ingestion-sap-file", consumes = "application/json", produces = "application/json")
    public MuleResponse saveFile(@RequestBody SapRequest request){
        MuleResponse response = new MuleResponse();
        HeaderSap headerSap = new HeaderSap();
        if(request.getHeader() != null){
            headerSap.setCodiceProcesso(request.getHeader().getCodiceProcesso() != null? request.getHeader().getCodiceProcesso() : "INGESTION-SAP-01");
            headerSap.setDestinatario(request.getHeader().getMittente() != null? request.getHeader().getMittente() : "SAP");
            headerSap.setMittente(request.getHeader().getDestinatario() != null ? request.getHeader().getDestinatario() : "SOPHIA");
            headerSap.setUserName("SOPHIA");

        }else{
            headerSap.setCodiceProcesso("INGESTION-SAP-01");
            headerSap.setDestinatario("SAP");
            headerSap.setMittente("SOPHIA");
            headerSap.setUserName("SOPHIA");
        }
        MessaggioDto messaggio = new MessaggioDto();
        messaggio.setID(UUID.randomUUID().toString());
        messaggio.setRiferimento("");
        messaggio.setTimeStamp(new Date());
        headerSap.setMessaggi(Collections.singletonList(messaggio));

        headerSap.setTipoElaborazione("S");
        //l'haeder non viene passato ma noi siamo furbi e ce lo teniamo al pizzo -_- . ... ora si
        response.setHeader(headerSap);
        EsitoMule esito = new EsitoMule();

        try {
            Assert.hasLength(request.getIdentificativo(), "identificativo non valorizzato");
            Assert.hasLength(request.getPath(), "path non valorizzato");
            Assert.notNull(request.getData(), "data non valorizzata");

            String pathBonificato = request.getPath().substring(request.getPath().lastIndexOf("\\")+1);

            log.info("Path originale: "+request.getPath()+" bonificato: "+ pathBonificato);

            BodySqs bodySqs = new BodySqs();
            bodySqs.setIdentificativo(request.getIdentificativo());
            bodySqs.setFileName(pathBonificato);
            esito.setCodice(exchangeFileService.sendSqsMessageForCopyFile(bodySqs));
            esito.setDescrizione("Elaborazione avvanuta con successo");

        }catch(Exception e){
            esito.setCodice("KO");
            if(e.getMessage() != null)
                esito.setDescrizione(e.getMessage());
            else
                esito.setDescrizione(e.toString());
        }
        response.setEsito(esito);

        return response;

    }

    public static void main(String[] args) throws Exception{

        /*//save Tripletta
        BdcTriplettaDto bdcTriplettaDto = new BdcTriplettaDto();

        bdcTriplettaDto.setAttribuzione(new Long(1000141545));
        BdcCanaliDto canaliDto1 = new BdcCanaliDto();
        canaliDto1.setNome("ciao");
        BdcCanaliDto canaliDto2 = new BdcCanaliDto();
        canaliDto2.setNome("ciao");
        List<BdcCanaliDto> list = new ArrayList<BdcCanaliDto>();
        list.add(canaliDto2);
        list.add(canaliDto1);
        bdcTriplettaDto.setCanali(list);
        ObjectMapper mapper = new ObjectMapper();
         System.out.println(mapper.writeValueAsString(bdcTriplettaDto));
*/

        //getTriplette
        /*FilterTripletteDto filterTripletteDto = new FilterTripletteDto();

        filterTripletteDto.setIdEmittente(new Long(19));

        ObjectMapper mapper = new ObjectMapper();
        System.out.println(mapper.writeValueAsString(filterTripletteDto));
*/
//        String path = "\\\\\\\\siae\\\\fs\\\\sapexDVP\\\\20190227123211_DatiIncassiBroadcasting.CSV";
//        String pathBonificato = path.substring(path.lastIndexOf("\\")+1);
//
//        log.info("Path originale: "+path+" bonificato: "+ pathBonificato);


    }


}
