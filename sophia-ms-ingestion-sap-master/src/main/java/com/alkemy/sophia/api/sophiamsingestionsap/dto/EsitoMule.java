package com.alkemy.sophia.api.sophiamsingestionsap.dto;

import lombok.Data;

@Data
public class EsitoMule {
    String codice;
    String descrizione;
}
