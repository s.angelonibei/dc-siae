package com.alkemy.sophia.api.sophiamsingestionsap.service.interfaces;

import com.alkemy.sophia.api.sophiamsingestionsap.dto.SapIngestionResponse;
import com.alkemy.sophia.api.sophiamsingestionsap.dto.response.ListaIngestioneDettaglioResponse;
import com.alkemy.sophia.api.sophiamsingestionsap.dto.response.ListaIngestioneResponse;
import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.BdcAmountSapFile;

import java.io.InputStream;
import java.util.Date;

public interface IngestionSapService {

    SapIngestionResponse ingestionSap(String processId);

    BdcAmountSapFile ingestionRecord(Long ingestionId);

    ListaIngestioneResponse listaIngestion(Date fromDate, Date toDate, String order, Long page, Boolean count,Long pageSize);

    ListaIngestioneDettaglioResponse listaIngestionDettaglio(Long idIngestion, String tripletta, String order, Long page, Boolean count, Long pageSize);

    InputStream getSapFile(Long idIngestion);
    InputStream getIngestionCsv(Long idIngestion, String order);
}
