package com.alkemy.sophia.api.sophiamsingestionsap.dto;

import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.BdcBroadcasters;
import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.BdcTripletta;
import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.Diritto;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FilterTripletteDto {

    public String order;
    public Long page;
    public Boolean count;
    public Long pageSize;
    public Long idEmittente;
    public Long idCanale;
    public BdcBroadcasters.TIPO_CANALE tipoEmittente;
    public Diritto tipoDiritto;
    public BdcTripletta.Stato stato;
    public String tripletta;
    public Boolean withCanali;

   /* @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    public Date dateFrom;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    public Date dateTo;
*/

}
