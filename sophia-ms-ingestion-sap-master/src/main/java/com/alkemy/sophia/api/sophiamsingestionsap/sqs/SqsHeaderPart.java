package com.alkemy.sophia.api.sophiamsingestionsap.sqs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SqsHeaderPart {
    String queue;
    String timestamp;
    String uuid;
    String sender;
}


/*
{
  "header": {
    "uuid": "44651481-9331-4830-8624-1370dead3af9",
    "timestamp": "2019-01-30T10:21:04.633Z",
    "queue": "dev_completed_saploader",
    "sender": "sap-loader",
    "hostname": "DESKTOP-TN1TAAO"
  },
  "input": {
    "header": {
      "uuid": "4f99c388-17c6-4928-839e-8cacc30f03c3",
      "timestamp": "2019-01-30T10:11:18.264Z",
      "queue": "dev_to_process_saploader",
      "sender": "sap-loader"
    },
    "body": {
      "fileName": "file_bello_da_sucare.csv"
    }
  },
  "body": {
    "siaePath": "/var/local/sophia/dev/saploader/siae/",
    "filename": "file_bello_da_sucare.csv",
    "url": "s3://siae-sophia-datalake/dev/broadcasting/saploader//file_bello_da_sucare.csv"
  }
}
 */