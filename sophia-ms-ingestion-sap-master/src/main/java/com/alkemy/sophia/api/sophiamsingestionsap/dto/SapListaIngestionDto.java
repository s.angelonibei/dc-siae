package com.alkemy.sophia.api.sophiamsingestionsap.dto;


import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.BdcAmountSapFile;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

import java.util.Date;

@Data
public class SapListaIngestionDto {

    private String nomeFile;
    private Long idIngestion;
    private Date dataAggiornamento;
    private BdcAmountSapFile.STATO_FILE esitoIngestion;
    private String tipoErrore;
    private Long numTripletteOk;
    private Long numTripletteKo;


    public enum ORDER_COLUMN{
        nomeFile,
        dataAggiornamento,
        esitoIngestion,
        tipoErrore,
        numTripletteOk,
        numTripletteKo
    }

}
