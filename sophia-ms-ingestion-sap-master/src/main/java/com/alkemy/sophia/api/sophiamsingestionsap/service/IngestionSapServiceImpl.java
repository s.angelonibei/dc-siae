package com.alkemy.sophia.api.sophiamsingestionsap.service;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.alkemy.sophia.api.sophiamsingestionsap.dto.DatiTriplette;
import com.alkemy.sophia.api.sophiamsingestionsap.dto.SapCsvDto;
import com.alkemy.sophia.api.sophiamsingestionsap.dto.SapDettaglioIngestionDto;
import com.alkemy.sophia.api.sophiamsingestionsap.dto.SapDettaglioIngestionDto.SapDettaglioIngestionDtoCSVExportFields;
import com.alkemy.sophia.api.sophiamsingestionsap.dto.SapIngestionResponse;
import com.alkemy.sophia.api.sophiamsingestionsap.dto.SapListaIngestionDto;
import com.alkemy.sophia.api.sophiamsingestionsap.dto.response.ListaIngestioneDettaglioResponse;
import com.alkemy.sophia.api.sophiamsingestionsap.dto.response.ListaIngestioneResponse;
import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.BdcAmountSapFile;
import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.BdcIngestionSap;
import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.BdcTripletta;
import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.BdcTriplettaCanalePeriodo;
import com.alkemy.sophia.api.sophiamsingestionsap.model.repo.BdcAmountSapFileRepo;
import com.alkemy.sophia.api.sophiamsingestionsap.model.repo.BdcIngestionSapRepo;
import com.alkemy.sophia.api.sophiamsingestionsap.model.repo.BdcTriplettaCanalePeriodoRepo;
import com.alkemy.sophia.api.sophiamsingestionsap.model.repo.BdcTriplettaRepo;
import com.alkemy.sophia.api.sophiamsingestionsap.model.repo.querydsl.BdcIngestionRepo;
import com.alkemy.sophia.api.sophiamsingestionsap.service.interfaces.IngestionSapService;
import com.alkemy.sophia.api.sophiamsingestionsap.utils.DateUtils;
import com.alkemy.sophia.api.sophiamsingestionsap.utils.S3Url;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;


@Service
@RequiredArgsConstructor
@CommonsLog
public class IngestionSapServiceImpl implements IngestionSapService {
    private final AmazonS3 s3Client;
    private final BdcTriplettaRepo bdcTriplettaRepo;
    private final BdcTriplettaCanalePeriodoRepo bdcTriplettaCanalePeriodoRepo;
    private final BdcIngestionRepo bdcIngestionRepo;
    private final BdcAmountSapFileRepo bdcAmountSapFileRepo;
    private final BdcIngestionSapRepo bdcIngestionSapRepo;

    @Value("#{'${default.environment}'+ '_' + '${sqs.input.completed}' + '_' + '${sqs.input.queue}'}")
    private String test;

    @Override
    public SapIngestionResponse ingestionSap(String processId) {
        SapIngestionResponse response = new SapIngestionResponse();

        //logica per verificare se e' l'ultimo processo e non è gia' stato avviato

        List<BdcAmountSapFile.STATO_FILE> listaStati= new ArrayList<>();
        listaStati.add(BdcAmountSapFile.STATO_FILE.DA_LAVORARE);
        listaStati.add(BdcAmountSapFile.STATO_FILE.NOTIFICATO);
        listaStati.add(BdcAmountSapFile.STATO_FILE.RICHIESTO);

        List<BdcAmountSapFile> listaFile = bdcAmountSapFileRepo.findByStatusInOrderByRequestDateDesc(listaStati);

        if(CollectionUtils.isEmpty(listaFile)){
            BdcAmountSapFile fileTmp = bdcAmountSapFileRepo.findByProcessId(processId);

            if(fileTmp != null)
                updateFileAndResponse(response,fileTmp,"processId "+processId + " non trovato o gia' elaborato",BdcAmountSapFile.STATO_FILE.ERRORE);
//
//            response.setStato(BdcAmountSapFile.STATO_FILE.ERRORE);
//            response.setEsito("processId "+processId + " non trovato o gia' elaborato");
            return response;
        }



//        if(!listaFile.get(0).getProcessId().equals(processId)){
//            BdcAmountSapFile fileTmp = bdcAmountSapFileRepo.findByProcessId(processId);
//
//            if(fileTmp != null)
//                updateFileAndResponse(response,fileTmp,"Esistono altri lanci eseguiti dopo "+processId + " es:"+listaFile.get(0).getProcessId() ,BdcAmountSapFile.STATO_FILE.ERRORE);
//
//            return response;
//        }

        BdcAmountSapFile ingestionConf = bdcAmountSapFileRepo.findByProcessIdAndStatus(processId, BdcAmountSapFile.STATO_FILE.DA_LAVORARE);


        if(ingestionConf == null){
            BdcAmountSapFile fileTmp = bdcAmountSapFileRepo.findByProcessId(processId);


            if(fileTmp != null)
                updateFileAndResponse(response,fileTmp,"Il lancio "+processId + " non è in stato "+BdcAmountSapFile.STATO_FILE.DA_LAVORARE ,BdcAmountSapFile.STATO_FILE.ERRORE);

            return response;
        }

//        BdcAmountSapFile ingestionConf = listaFile.get(0);
//
        //invalidate recent record
//        for (BdcAmountSapFile aFile : listaFile) {
//            if(aFile.getProcessId().equals(processId)){
//                aFile.setStatus(BdcAmountSapFile.STATO_FILE.IN_LAVORAZIONE);
//            }else{
//                aFile.setStatus(BdcAmountSapFile.STATO_FILE.NON_PROCESSATO);
//            }
//            bdcAmountSapFileRepo.save(aFile);
//        }




        if(ingestionConf.getPathS3() == null){
            updateFileAndResponse(response,ingestionConf,"Errore nel file path caricato su S3" ,BdcAmountSapFile.STATO_FILE.ERRORE);
            return response;
        }

        S3Url url = new S3Url(ingestionConf.getPathS3());

        S3Object s3object = s3Client.getObject(new GetObjectRequest(url.getBucket(), url.getKey()));


        CsvMapper mapper = new CsvMapper();
        CsvSchema schema = mapper.schemaFor(SapCsvDto.class).withHeader().withColumnSeparator(';');


        try(final BufferedReader reader = new BufferedReader(new InputStreamReader(s3object.getObjectContent()))){

            MappingIterator<SapCsvDto> it = mapper
                    .readerFor(SapCsvDto.class)
                    .with(schema)
                    .readValues(reader);

            while (it.hasNext()) {
                elaborateRecord(it.next(),ingestionConf);
            }


        } catch (IOException e) {
            updateFileAndResponse(response,ingestionConf,e.getMessage(),BdcAmountSapFile.STATO_FILE.ERRORE);
            return response;
        }

        updateFileAndResponse(response,ingestionConf,"OK",BdcAmountSapFile.STATO_FILE.LAVORATO);
        return response;

    }

    @Override
    public BdcAmountSapFile ingestionRecord(Long ingestionId) {
        return bdcAmountSapFileRepo.getOne(ingestionId);
    }

    @Override
    public ListaIngestioneResponse listaIngestion(Date fromDate, Date toDate, String order, Long page, Boolean count,Long pageSize){

        ListaIngestioneResponse response = new ListaIngestioneResponse();

		if (count) {
			response.setCount(bdcIngestionRepo.countAmountSapFile(fromDate, toDate));
			return response;
		}
        SapListaIngestionDto.ORDER_COLUMN orderColumn = null;
		boolean asc = false;
		try{

            String[] split = order.split(" ");
            orderColumn = SapListaIngestionDto.ORDER_COLUMN.valueOf(split[0]);
            asc = split.length > 1 &&  split[1].equalsIgnoreCase("asc") ? true : false ;

        }catch (Exception e){log.error("Errore nell'ordinamento e: " + ExceptionUtils.getStackTrace(e));}

		response.setList(bdcIngestionRepo.allDtoOrderByDate(fromDate, toDate, orderColumn, asc, pageSize, page));

        return response;

    }

    @Override
    public ListaIngestioneDettaglioResponse listaIngestionDettaglio(Long idIngestion,String tripletta, String order, Long page, Boolean count, Long pageSize){
        ListaIngestioneDettaglioResponse response = new ListaIngestioneDettaglioResponse();

        BdcAmountSapFile amountFile = bdcAmountSapFileRepo.getOne(idIngestion);
        String filename = amountFile.getPathS3().substring(amountFile.getPathS3().lastIndexOf("/")+1);

        response.setDataAggiornamento(amountFile.getRequestDate());
        response.setNomeFile(filename);

        response.setCount(bdcIngestionRepo.countIngestionSap(idIngestion,tripletta));
        
        SapDettaglioIngestionDto.ORDER_COLUMN orderColumn = null;
        boolean asc = false;
        try{

            String[] split = order.split(" ");
            orderColumn = SapDettaglioIngestionDto.ORDER_COLUMN.valueOf(split[0]);
            asc = split.length > 1 &&  split[1].equalsIgnoreCase("asc") ? true : false ;

        }catch (Exception e){log.error("Errore nell'ordinamento e: " + ExceptionUtils.getStackTrace(e));}

        response.setList(bdcIngestionRepo.allIngestionSap(idIngestion,tripletta,orderColumn,asc,false,pageSize,page));

        return response;
    }

    @Override
    public InputStream getSapFile(Long idIngestion) {
        BdcAmountSapFile ingestionConf = bdcAmountSapFileRepo.getOne(idIngestion);

        S3Url url = new S3Url(ingestionConf.getPathS3());

        S3Object s3object = s3Client.getObject(new GetObjectRequest(url.getBucket(), url.getKey()));

        return s3object.getObjectContent();


    }

    @Override
    public InputStream getIngestionCsv(Long idIngestion, String order) {

        SapDettaglioIngestionDto.ORDER_COLUMN orderColumn = null;
        boolean asc = false;
        try{
            orderColumn = SapDettaglioIngestionDto.ORDER_COLUMN.valueOf(order.split(" ")[0]);
            asc = order.split(" ")[1].equalsIgnoreCase("asc");
        }catch (Exception e){log.error("Errore nell'ordinamento e: " + ExceptionUtils.getStackTrace(e));}


        List<SapDettaglioIngestionDto> list = bdcIngestionRepo.allIngestionSap(idIngestion, null,orderColumn, asc, true, 0l, 0l);

        CsvMapper mapper = new CsvMapper();
        mapper.addMixIn(SapDettaglioIngestionDto.class, SapDettaglioIngestionDto.SapDettaglioIngestionDtoCSVExportFields.class);
        CsvSchema schema = mapper.schemaFor(SapDettaglioIngestionDto.class).withHeader().withColumnSeparator(';');
        ObjectWriter myObjectWriter = mapper.writer(schema);
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            myObjectWriter.writeValue(baos, list);

            return new ByteArrayInputStream(baos.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }


        return null;
    }



    private void elaborateRecord(SapCsvDto record, BdcAmountSapFile sapFile){

        //matching rules tripletta in BDC_TRIPLETTA_CANALE_PERIODO
        //tripletta and period
        //get min and max date from period and sap year

        //if not match -> write error on BDC_INGESTION_SAP
        //if error is tripletta not found -> insert in anag

        //else apply rules:
        // single channel -> write record

        //multi channel -> search preallocazione
        //write N record

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date dataRegistrazione = null;
        double importo = 0 ;
        double percentuale = 100;
        int annoInizio = 0;
        int annoFine = 0;

//        try{
//            dataRegistrazione = sdf.parse(record.getDataReg());
//        }catch (Exception e){
//            saveIngestionRecord(record,sapFile,null,BdcIngestionSap.STATO.ERR_CSV_RECORD,"Data Registrazione non valida",importo,dataRegistrazione,percentuale,null,null);
//            return;
//        }


        NumberFormat format = NumberFormat.getInstance(Locale.US);
        try{
            Number number = format.parse(record.getImporto());
            importo = number.doubleValue();

//            if(importo >= 0){
//                saveIngestionRecord(record,sapFile,null,BdcIngestionSap.STATO.ERR_CSV_RECORD,"Importo  "+record.getImporto()+" valore positivo ",importo,dataRegistrazione,percentuale,null,null);
//                return;
//            }


        }catch (Exception e){
            saveIngestionRecord(record,sapFile,null,BdcIngestionSap.STATO.ERR_CSV_RECORD,"Importo  "+record.getImporto()+" formato non corretto ",importo,dataRegistrazione,percentuale,null,null);
            return;
        }



        log.info("TRIPLETTA:" + record.getTripletta());

        if(record.getTripletta() == null ||
                record.getConto() == null ||
                record.getChiaveRif3() == null ||
                record.getAttribuzione() == null ||
                record.getConto().length() < 8 ||
                record.getConto().length() > 10 || //aggiunto per mail di sap del 6/3 in cui affermano che i conti arrivano fino a 10
                record.getChiaveRif3().length() != 5 ){

            saveIngestionRecord(record,sapFile,null,BdcIngestionSap.STATO.ERR_CSV_RECORD,"Dominio della tripletta errato ",importo,dataRegistrazione,percentuale,null,null);
            return;

        }

        BdcTripletta tripletta = bdcTriplettaRepo.findByTripletta(record.getTripletta());

        if(tripletta == null){
            saveIngestionRecord(record,sapFile,null,BdcIngestionSap.STATO.ERR_TRIPLETTA,"Tripletta non trovata",importo,dataRegistrazione,percentuale,null,null);
            return;
        }



        if(StringUtils.isEmpty(record.getAttribuzione())){
            saveIngestionRecord(record,sapFile,null,BdcIngestionSap.STATO.ERR_CSV_RECORD,"Attribuzione non valorizzato",importo,dataRegistrazione,percentuale,null,null);
            return;
        }

        String periodo="";
        try {
            periodo = record.getAttribuzione().substring(record.getAttribuzione().indexOf(" ") + 1);

            if(periodo.length() == 4){
                annoInizio = Integer.parseInt(periodo);
                annoFine = annoInizio;
            }

            else if(periodo.length() == 7){
                annoFine = Integer.parseInt(periodo.substring(0,4));
                annoInizio = Integer.parseInt(periodo.substring(0,2) + periodo.substring(5,7));

                //swap if necessary !?! =_=
                if(annoFine < annoInizio){
                    int annoTmp = annoFine;
                    annoFine= annoInizio;
                    annoInizio = annoTmp;
                }


            }else{
                saveIngestionRecord(record,sapFile,null,BdcIngestionSap.STATO.ERR_CSV_RECORD,"Attribuzione formato errato: " + periodo,importo,dataRegistrazione,percentuale,null,null);
                return;
            }
        }catch(Exception e){
            saveIngestionRecord(record,sapFile,null,BdcIngestionSap.STATO.ERR_CSV_RECORD,"Periodo Attribuzione errato: " + record.getAttribuzione(),importo,dataRegistrazione,percentuale,null,null);
            return;
        }


        LocalDate dateIni = LocalDate.of(annoInizio,Month.JANUARY, 1);
        LocalDate datefin = LocalDate.of(annoFine,Month.DECEMBER, 31);

        List<BdcTriplettaCanalePeriodo> listaTriplette = bdcIngestionRepo.allRefFromTriplettaAndPeriod(
                tripletta.getId(),
                Date.from(dateIni.atStartOfDay(ZoneId.systemDefault()).toInstant()),
                Date.from(datefin.atStartOfDay(ZoneId.systemDefault()).toInstant()));





        if(CollectionUtils.isEmpty(listaTriplette)){
            saveIngestionRecord(record,sapFile,null,BdcIngestionSap.STATO.ERR_TRIPLETTA,"Tripletta "+record.getTripletta()+" non associata per il periodo " + periodo,importo,dataRegistrazione,percentuale,null,null);
            return;
        }else{
            double totalNumOfDays  = 0;

            HashMap<Long, DatiTriplette> mappaTriplettaGiorni = new HashMap<>();

            try {

                DateTime dtInitSap = null;
                DateTime dtEndSap = null;

                for (BdcTriplettaCanalePeriodo tripCanale : listaTriplette) {

                    String patternDa = tripCanale.getBdcTripletta().getPreallocazioneIncassiDa();
                    String patternA = tripCanale.getBdcTripletta().getPreallocazioneIncassiA();

                    //('01/09/yyyy', '31/08/yyyy+1');
                    String dateIniString = patternDa.substring(0, 6) + annoInizio;
                    String datefinString = patternA.substring(0, 6) + annoFine;

                    SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy");
                    sdf2.setTimeZone(TimeZone.getTimeZone("UTC"));



//                    DateTime dtInitSap = null;
                    dtInitSap = new DateTime(sdf2.parse(dateIniString));

//                    DateTime dtEndSap = null;
                    dtEndSap = new DateTime(sdf2.parse(datefinString));

                    DateTime dtInitTripletta = null;
                    if(tripCanale.getAttivoDal() != null)
                        dtInitTripletta = new DateTime(tripCanale.getAttivoDal());


                    DateTime dtEndTripletta = null;
                    if(tripCanale.getAttivoAl()!=null)
                        dtEndTripletta = new DateTime(tripCanale.getAttivoAl());

                    DateTime dtInitCanale = null;
                    if(tripCanale.getBdcCanali().getDataInizioValid()!=null)
                        dtInitCanale = new DateTime(tripCanale.getBdcCanali().getDataInizioValid());

                    DateTime dtEndCanale = null;
                    if(tripCanale.getBdcCanali().getDataFineValid()!=null)
                        dtEndCanale = new DateTime(tripCanale.getBdcCanali().getDataFineValid());


                    //https://wmfexcel.com/2014/10/25/how-to-calculate-number-of-overlapping-days-for-two-periods/

                    DateTime dtInitMax = DateUtils.latest(dtInitSap, dtInitTripletta, dtInitCanale);
                    DateTime dtEndMin = DateUtils.oldest(dtEndSap, dtEndTripletta, dtEndCanale);


                    double numOfDays = Days.daysBetween(dtInitMax, dtEndMin).getDays() + 1;

                    if(tripCanale.getPreallocazionePercentuale() != null){
                        numOfDays = numOfDays*tripCanale.getPreallocazionePercentuale()/100;
                    }

                    totalNumOfDays += numOfDays;

                    DatiTriplette datiTriplette = new DatiTriplette();
                    datiTriplette.setNumGiorni(numOfDays);
                    datiTriplette.setDataInizio(dtInitMax);
                    datiTriplette.setDataFine(dtEndMin);

                    mappaTriplettaGiorni.put(tripCanale.getId(), datiTriplette);
                }

                if(totalNumOfDays <= 0){
                    saveIngestionRecord(record,sapFile,null,BdcIngestionSap.STATO.ERR_TRIPLETTA,"Tripletta "+record.getTripletta()+" non associata per il periodo " + periodo,importo,dataRegistrazione,percentuale,null,null);
                    return;
                }

//                for (BdcTriplettaCanalePeriodo tripCanale : listaTriplette) {
//                    DatiTriplette datiTriplette = mappaTriplettaGiorni.get(tripCanale.getId());
//                    double importoParziale = (importo / totalNumOfDays) * datiTriplette.getNumGiorni();
//                    double percParziale = (datiTriplette.getNumGiorni() * 100) / totalNumOfDays;
//                    saveIngestionRecord(record, sapFile, tripCanale, BdcIngestionSap.STATO.OK, null, importoParziale, dataRegistrazione, percParziale, datiTriplette.getDataInizio().toDate(), datiTriplette.getDataFine().toDate());
//                }


                saveIngestionRecord(record, sapFile, listaTriplette.get(0), BdcIngestionSap.STATO.OK, null, importo, dataRegistrazione, 0, dtInitSap.toDate(), dtEndSap.toDate());

            }catch (Exception e){
                saveIngestionRecord(record,sapFile,null,BdcIngestionSap.STATO.ERR_CSV_RECORD,"Errore nello split della tripletta per canale: "+e.getMessage(),importo,dataRegistrazione,percentuale,null,null);
                log.error("",e);
                return;
            }

        }
        return;

    }

    private void saveIngestionRecord(SapCsvDto record,
                                     BdcAmountSapFile sapFile,
                                     BdcTriplettaCanalePeriodo triplettaCanalePeriodo,
                                     BdcIngestionSap.STATO stato,
                                     String errore,
                                     double importo,
                                     Date dataRegistrazione,
                                     double percentuale,
                                     Date dataInizio,
                                     Date dataFine){
        
        BdcIngestionSap outTemp = new BdcIngestionSap();
        outTemp.setBdcAmountSapFile(sapFile);
        outTemp.setDataDocumento(record.getAttribuzione());
        outTemp.setDataDiInserimento(new Date());
        outTemp.setChiaveRif2(record.getChiaveRif2());
        outTemp.setTesto(record.getTesto());
        outTemp.setTestoTestataDocumento(record.getTestata());
        outTemp.setTripletta(record.getTripletta());
        outTemp.setBdcTriplettaCanalePeriodo(triplettaCanalePeriodo);
        if(triplettaCanalePeriodo != null)
            outTemp.setBdcTripletta(triplettaCanalePeriodo.getBdcTripletta());


        outTemp.setImportoRelativo(importo * -1);
        if(outTemp.getImportoRelativo()< 0){
            stato = BdcIngestionSap.STATO.ERR_CSV_RECORD;
            errore = "Importo  "+record.getImporto()+" valore positivo in SAP";
        }

        outTemp.setErrore(errore);
        outTemp.setStato(stato);

        outTemp.setImportoTotaleDocumento(record.getImporto());
        outTemp.setDataDiRegistrazione(dataRegistrazione);
        outTemp.setBdcTriplettaCanalePeriodo(triplettaCanalePeriodo);
        outTemp.setPreallocazionePercentuale(percentuale);
        outTemp.setDataDa(dataInizio);
        outTemp.setDataA(dataFine);
        outTemp.setEsMese(record.getPeriodo());
        

        if(triplettaCanalePeriodo!=null) {
            outTemp.setTipoDiritto(triplettaCanalePeriodo.getBdcTripletta().getTipoDiritto());
        }

        ObjectMapper objectMapper = new ObjectMapper();

        try{
            outTemp.setChiaveRif3(record.getChiaveRif3());
            outTemp.setConto(record.getConto());

            String anagrafica = record.getAttribuzione().substring(0, record.getAttribuzione().indexOf(" "));

            outTemp.setAttribuzione(anagrafica);
        }catch(Exception e){}

        try {
            outTemp.setRecordSap(objectMapper.writeValueAsString(record));
        } catch (JsonProcessingException e) {
            log.error("impossibile convertire in json l'oggetto");
        }

        bdcIngestionSapRepo.save(outTemp);

    }

    private void updateFileAndResponse(SapIngestionResponse response,BdcAmountSapFile file, String errorMessage, BdcAmountSapFile.STATO_FILE stato){
        log.error("IngestionSapService esito: "+ errorMessage);
        response.setEsito(errorMessage);
        response.setStato(stato);
        file.setErrorMessage(errorMessage);
        file.setStatus(stato);
        bdcAmountSapFileRepo.save(file);
    }


}
