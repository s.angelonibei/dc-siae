package com.alkemy.sophia.api.sophiamsingestionsap.sqs;

import com.alkemy.sophia.api.sophiamsingestionsap.dto.response.MuleRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;

@Data
public class SqsBaseMessage {
    private SqsBaseMessage input;

    private SqsHeaderPart header;
    private BodySqs body;
    private SqsOutputPart output;
    private ErrorSqs error;



}
