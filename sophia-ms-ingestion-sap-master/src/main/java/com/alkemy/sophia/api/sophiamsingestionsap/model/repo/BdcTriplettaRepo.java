package com.alkemy.sophia.api.sophiamsingestionsap.model.repo;

import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.BdcTripletta;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BdcTriplettaRepo extends JpaRepository<BdcTripletta, Long> {

    BdcTripletta  findByTripletta(String tripletta);

    BdcTripletta  findByIdAndStato(Long id, BdcTripletta.Stato stato);

}
