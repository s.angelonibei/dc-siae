package com.alkemy.sophia.api.sophiamsingestionsap.model.repo;

import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.BdcTriplettaCanalePeriodo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BdcTriplettaCanalePeriodoRepo extends JpaRepository<BdcTriplettaCanalePeriodo, Long> {

    List<BdcTriplettaCanalePeriodo> findByBdcTriplettaIdAndDataDisattivazioneIsNull(Long idtripletta);

}
