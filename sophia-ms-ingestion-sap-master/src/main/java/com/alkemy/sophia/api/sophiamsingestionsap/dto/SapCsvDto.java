package com.alkemy.sophia.api.sophiamsingestionsap.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

import java.util.Date;

@Data
@JsonPropertyOrder({ "conto", "testata", "testo", "chiaveRif3", "attribuzione", "importo" })
public class SapCsvDto {

    private String conto;
    private String testata;
    private String testo;
    private String chiaveRif3;
    private String attribuzione;
    private String importo;
    private String periodo;
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private String dataReg;
    private String chiaveRif2;


    private String tripletta;

    public String getTripletta(){
        try {
            String anagrafica = attribuzione.substring(0, attribuzione.indexOf(" "));

            return conto + chiaveRif3 + anagrafica;
        }catch (Exception e){
            return null;
        }
    }
}
