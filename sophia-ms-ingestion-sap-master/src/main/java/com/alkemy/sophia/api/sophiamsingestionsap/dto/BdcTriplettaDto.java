package com.alkemy.sophia.api.sophiamsingestionsap.dto;


import com.alkemy.sophia.api.sophiamsingestionsap.dto.BdcCanaliDto;
import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.BdcBroadcasters;
import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.BdcTripletta;
import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.BdcTriplettaCanalePeriodo;
import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.Diritto;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class BdcTriplettaDto {


    Long id;
    String tripletta;
    Diritto tipoDiritto;
    String rifTemporaleIncassi;
    String giornoMeseDa;
    String giornoMeseA;
    String chiaveRif3;
    String conto;
    Long attribuzione;
    String descrizioneSap;
    String nomiCanali;
    BdcTripletta.Stato stato;
    String nomeEmittente;
    BdcBroadcasters.TIPO_CANALE tipoEmittente;
    Long idEmittente;


    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date dataInserimento;

    String utente;
    //BdcTriplettaCanalePeriodo.SingoloVsVasca singoloVsVasca;
    List<BdcTriplettaCanalePeriodoDto> canali;


    public enum ORDER_COLUMN{
        tripletta,
        tipoDiritto,
        singoloVsVasca,
        stato,
        nomeEmittente,
        tipoEmittente
    }




}
