package com.alkemy.sophia.api.sophiamsingestionsap.model.repo;

import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.BdcAmountSapFile;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BdcAmountSapFileRepo extends JpaRepository<BdcAmountSapFile, Long> {

    BdcAmountSapFile findByProcessIdAndStatus(String processId, BdcAmountSapFile.STATO_FILE stato);
    BdcAmountSapFile findByProcessId(String processId);
    List<BdcAmountSapFile> findByStatusInOrderByRequestDateDesc(List<BdcAmountSapFile.STATO_FILE> stato);
    BdcAmountSapFile findFirstByOrderByRequestDateDesc();
}
