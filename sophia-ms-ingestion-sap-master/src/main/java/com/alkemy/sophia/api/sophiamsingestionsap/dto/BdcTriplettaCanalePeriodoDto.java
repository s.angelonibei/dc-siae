package com.alkemy.sophia.api.sophiamsingestionsap.dto;


import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.BdcCanali;
import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.BdcTripletta;
import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.BdcTriplettaCanalePeriodo;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
public class BdcTriplettaCanalePeriodoDto {


//campi tripletta canale periodo
   private Long id;
    private Double preallocazionePercentuale;
    private BdcTriplettaCanalePeriodo.SingoloVsVasca singoloVsVasca;
    private Integer numeroCanaliVasca;
    private String nomiCanaliVasca;
    private String utenteDisattivazione;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date attivoDal;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date attivoAl;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date dataDisattivazione;


//campi canale
    private Long idCanale;
    private Long idBroadcaster;
    private String nomeCanale;
    private Boolean generalistaCanale;
    private Boolean activeCanale;
    private Boolean specialRadioCanale;
    private String utenteUltimaModificaCanale;
    private BdcCanali.TIPO_CANALE tipoCanale;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date dataInizioValidCanale;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date dataFineValidCanale;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date dataCreazioneCanale;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date dataUltimaModificaCanale;
}
