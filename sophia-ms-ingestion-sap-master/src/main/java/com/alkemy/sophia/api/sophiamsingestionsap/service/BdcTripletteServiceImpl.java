package com.alkemy.sophia.api.sophiamsingestionsap.service;

import com.alkemy.sophia.api.sophiamsingestionsap.dto.*;
import com.alkemy.sophia.api.sophiamsingestionsap.dto.FilterTripletteDto;
import com.alkemy.sophia.api.sophiamsingestionsap.dto.response.MuleResponse;
import com.alkemy.sophia.api.sophiamsingestionsap.dto.response.RiepilogoTripletteResponse;
import com.alkemy.sophia.api.sophiamsingestionsap.dto.response.SaveTriplettaResponse;
import com.alkemy.sophia.api.sophiamsingestionsap.dto.BdcTriplettaDto;
import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.*;
import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.BdcCanali.TIPO_CANALE;
import com.alkemy.sophia.api.sophiamsingestionsap.model.repo.BdcBroadcasterRepo;
import com.alkemy.sophia.api.sophiamsingestionsap.model.repo.BdcCanaleRepo;
import com.alkemy.sophia.api.sophiamsingestionsap.model.repo.BdcTriplettaCanalePeriodoRepo;
import com.alkemy.sophia.api.sophiamsingestionsap.model.repo.BdcTriplettaRepo;
import com.alkemy.sophia.api.sophiamsingestionsap.model.repo.querydsl.BdcTriplettaDslRepo;
import com.alkemy.sophia.api.sophiamsingestionsap.service.interfaces.BdcTripletteService;
import com.amazonaws.util.CollectionUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
@CommonsLog
public class BdcTripletteServiceImpl implements BdcTripletteService {
    private final BdcBroadcasterRepo bdcBroadcasterRepo;
    private final BdcCanaleRepo bdcCanaleRepo;
    private final BdcTriplettaRepo bdcTriplettaRepo;
    private final BdcTriplettaCanalePeriodoRepo bdcTriplettaCanalePeriodoRepo;
    private final BdcTriplettaDslRepo bdcTriplettaDslRepo;

    @Override
    public List<BdcBroadcasterDto> getBroadcaster(){

        List<BdcBroadcasters> bdcBroadcasterList = bdcBroadcasterRepo.findAll();
        if(CollectionUtils.isNullOrEmpty(bdcBroadcasterList)){
            throw new RuntimeException("broadcasters non presenti");
        }

        List<BdcBroadcasterDto> response= new ArrayList<BdcBroadcasterDto>();
        for (BdcBroadcasters bdcBroadcasters : bdcBroadcasterList) {
            BdcBroadcasterDto element = new BdcBroadcasterDto();
            element.setDataCreazione(bdcBroadcasters.getDataCreazione());
            element.setId(bdcBroadcasters.getId());
            element.setNome(bdcBroadcasters.getNome());
            element.setTipoBroadcaster(bdcBroadcasters.getTipoBroadcaster());
            response.add(element);
        }
        return response;
    }

    @Override
    public List<BdcCanaliDto> getCanaliByIdBroadcaster(Long id){

        List<BdcCanali> bdcCanaliList = bdcCanaleRepo.findByBdcBroadcastersId(id);
        if(bdcCanaliList == null || bdcCanaliList.size()==0)
            throw new RuntimeException("canali non presenti");

        List<BdcCanaliDto> response= new ArrayList<BdcCanaliDto>();
        for (BdcCanali bdcCanali : bdcCanaliList) {
            BdcCanaliDto element = new BdcCanaliDto();
            element.setActive(bdcCanali.getActive());
            element.setDataCreazione(bdcCanali.getDataCreazione());
            element.setDataFineValid(bdcCanali.getDataFineValid());
            element.setDataInizioValid(bdcCanali.getDataInizioValid());
            element.setDataUltimaModifica(bdcCanali.getDataUltimaModifica());
            element.setGeneralista(bdcCanali.getGeneralista());
            element.setNome(bdcCanali.getNome());
            element.setTipoCanale(bdcCanali.getTipoCanale());
            element.setSpecialRadio(bdcCanali.getSpecialRadio());
            element.setId(bdcCanali.getId());
            response.add(element);
        }
        return response;
    }



    @Override
    public SaveTriplettaResponse saveTripletta(BdcTriplettaDto request) {

    	SaveTriplettaResponse response = new SaveTriplettaResponse();
        List<BdcTriplettaCanalePeriodoDto> canaliDtoList = request.getCanali();
        EsitoMule esito = new EsitoMule();

        //verifica se la tripletta è presente
        String tripletta = request.getConto() + request.getChiaveRif3() + request.getAttribuzione();
        log.info("controllo la presenza della tripletta " + tripletta);

        BdcTripletta bdcTripletta = bdcTriplettaRepo.findByTripletta(tripletta);
        if (bdcTripletta != null) {

            log.info("tripletta presente " + tripletta);
            List<BdcTriplettaCanalePeriodo> bdcTriplettaCanalePeriodoList = bdcTriplettaCanalePeriodoRepo.findByBdcTriplettaIdAndDataDisattivazioneIsNull(bdcTripletta.getId());

            if (!bdcTriplettaCanalePeriodoList.isEmpty()) {
                for (BdcTriplettaCanalePeriodo bdcTriplettaCanalePeriodo : bdcTriplettaCanalePeriodoList) {
                    log.info("disattivazione del record sulla tabella tripletta canale periodo id=" + bdcTriplettaCanalePeriodo.getId());
                    bdcTriplettaCanalePeriodo.setDataDisattivazione(new Date());
                    bdcTriplettaCanalePeriodo.setUtenteDisattivazione(request.getUtente());
                    bdcTriplettaCanalePeriodoRepo.save(bdcTriplettaCanalePeriodo);
                }
            }
        }else{
            bdcTripletta = new BdcTripletta();
        }

        bdcTripletta.setPreallocazioneIncassiDa(request.getGiornoMeseDa()==null? null:request.getGiornoMeseDa());
        bdcTripletta.setPreallocazioneIncassiA(request.getGiornoMeseA()==null? null:request.getGiornoMeseA());
        bdcTripletta.setDescrizioneSap(request.getDescrizioneSap()==null? null:request.getDescrizioneSap());
        bdcTripletta.setTipoDiritto(request.getTipoDiritto()==null? null:request.getTipoDiritto());
        bdcTripletta.setTripletta(tripletta);
        bdcTripletta.setAttribuzione(request.getAttribuzione());
        bdcTripletta.setConto(request.getConto());
        bdcTripletta.setChiaveRif3(request.getChiaveRif3());
        bdcTripletta.setTipoInserimento("FE");
        bdcTripletta.setDataInserimento( new Date());
        bdcTripletta.setRifTemporaleIncassi(request.getRifTemporaleIncassi());
        bdcTripletta.setStato(BdcTripletta.Stato.VALIDA);
        BdcTripletta savedTripletta = bdcTriplettaRepo.save(bdcTripletta);
        BdcTripletta resultId = new BdcTripletta();
        resultId.setId(savedTripletta.getId());
        response.setTripletta(resultId);
        for (BdcTriplettaCanalePeriodoDto bdcCanaliDto : canaliDtoList) {

            Optional<BdcCanali> optionalBdcCanali = bdcCanaleRepo.findById(bdcCanaliDto.getIdCanale());
            BdcCanali bdcCanale = new BdcCanali();
            if (optionalBdcCanali.isPresent())
                bdcCanale = optionalBdcCanali.get();
            else {
                log.error("il canale non risulta presente bdcCanale=" + bdcCanaliDto.getNomeCanale());
                throw new RuntimeException("il canale non risulta presente bdcCanale=" + bdcCanaliDto.getNomeCanale());
            }

            BdcTriplettaCanalePeriodo bdcTriplettaCanalePeriodo = new BdcTriplettaCanalePeriodo();
            bdcTriplettaCanalePeriodo.setAttivoDal(bdcCanaliDto.getAttivoDal()==null? null:bdcCanaliDto.getAttivoDal());
            bdcTriplettaCanalePeriodo.setAttivoAl(bdcCanaliDto.getAttivoAl()==null? null:bdcCanaliDto.getAttivoAl());
            bdcTriplettaCanalePeriodo.setBdcCanali(bdcCanale);
            bdcTriplettaCanalePeriodo.setBdcTripletta(bdcTripletta);
            bdcTriplettaCanalePeriodo.setSingoloVsVasca(bdcCanaliDto.getSingoloVsVasca());
            bdcTriplettaCanalePeriodo.setUtenteInserimento(request.getUtente());
            if(!canaliDtoList.isEmpty()) {
                bdcTriplettaCanalePeriodo.setNomiCanaliVasca(canaliDtoList.stream().map(BdcTriplettaCanalePeriodoDto::getNomeCanale).collect(Collectors.joining(",")));
                bdcTriplettaCanalePeriodo.setNumeroCanaliVasca(canaliDtoList.size());
            }
            bdcTriplettaCanalePeriodo.setPreallocazionePercentuale(bdcCanaliDto.getPreallocazionePercentuale());
            bdcTriplettaCanalePeriodoRepo.save(bdcTriplettaCanalePeriodo);
        }

        return response;
    }



    public RiepilogoTripletteResponse getTriplette(FilterTripletteDto filter){

        RiepilogoTripletteResponse response = new RiepilogoTripletteResponse();
        BdcTriplettaDto.ORDER_COLUMN orderColumn = null;

        if (filter.getCount()) {
            response.setCount(bdcTriplettaDslRepo.countTriplette(filter));
            return response;
        }

        boolean asc = false;
        try{
            orderColumn = BdcTriplettaDto.ORDER_COLUMN.valueOf(filter.getOrder().split(" ")[0]);
            asc = filter.getOrder().split(" ")[1].equalsIgnoreCase("asc");
        }catch (Exception e){
            log.error("Errore nell'ordinamento e: " + ExceptionUtils.getStackTrace(e));
            throw new RuntimeException("Errore nell'ordinamento e: " + ExceptionUtils.getStackTrace(e));
        }

        List<BdcTriplettaDto> listaResult = bdcTriplettaDslRepo.getTriplette(filter,orderColumn,asc);
        if(Boolean.TRUE.equals(filter.getWithCanali())){
            List<BdcTriplettaDto> listaResultDettaglio = new ArrayList<>();
            for (BdcTriplettaDto bdcTriplettaDto : listaResult) {
                listaResultDettaglio.add(geTripletta(bdcTriplettaDto.getId()));
            }
            response.setList(listaResultDettaglio);
        }else{
            response.setList(listaResult);
        }

        return response;
    }

    @Override
    public BdcTriplettaDto geTripletta(Long id) {

        BdcTripletta bdcTripletta = bdcTriplettaRepo.findByIdAndStato(id,BdcTripletta.Stato.VALIDA);

        if (bdcTripletta == null){
            log.error("tripletta non presente id=" + id);
            throw new RuntimeException("id triplett{a non presente");
        }

        log.info("tripletta presente " + bdcTripletta.getTripletta());
        BdcTriplettaDto bdcTriplettaDto = new BdcTriplettaDto();
        bdcTriplettaDto.setAttribuzione(bdcTripletta.getAttribuzione());
        bdcTriplettaDto.setChiaveRif3(bdcTripletta.getChiaveRif3());
        bdcTriplettaDto.setConto(bdcTripletta.getConto());
        bdcTriplettaDto.setDataInserimento(bdcTripletta.getDataInserimento());
        bdcTriplettaDto.setDescrizioneSap(bdcTripletta.getDescrizioneSap());
        bdcTriplettaDto.setGiornoMeseA(bdcTripletta.getPreallocazioneIncassiA());
        bdcTriplettaDto.setGiornoMeseDa(bdcTripletta.getPreallocazioneIncassiDa());
        bdcTriplettaDto.setId(bdcTripletta.getId());
        bdcTriplettaDto.setRifTemporaleIncassi(bdcTripletta.getRifTemporaleIncassi());
        bdcTriplettaDto.setStato(bdcTripletta.getStato());
        bdcTriplettaDto.setTipoDiritto(bdcTripletta.getTipoDiritto());




        List<BdcTriplettaCanalePeriodo> bdcTriplettaCanalePeriodoList = bdcTriplettaCanalePeriodoRepo.findByBdcTriplettaIdAndDataDisattivazioneIsNull(id);
        List<BdcTriplettaCanalePeriodoDto> bdcTriplettaCanalePeriodoDtolist = new ArrayList<BdcTriplettaCanalePeriodoDto>();
        if (!bdcTriplettaCanalePeriodoList.isEmpty()) {
        	BdcBroadcasters bdcBroadcasters = null;
            Optional<BdcCanali> optionalBdcCanali = bdcCanaleRepo.findById(bdcTriplettaCanalePeriodoList.get(0).getBdcCanali().getId());
            if (optionalBdcCanali.isPresent()) {
            	bdcBroadcasters = optionalBdcCanali.get().getBdcBroadcasters();
            }
            for (BdcTriplettaCanalePeriodo bdcTriplettaCanalePeriodo : bdcTriplettaCanalePeriodoList) {
                BdcTriplettaCanalePeriodoDto bdcTriplettaCanalePeriodoDto = new BdcTriplettaCanalePeriodoDto();

                //tripletta canale periodo
                bdcTriplettaCanalePeriodoDto.setAttivoDal(bdcTriplettaCanalePeriodo.getAttivoDal());
                bdcTriplettaCanalePeriodoDto.setAttivoAl(bdcTriplettaCanalePeriodo.getAttivoAl());
                bdcTriplettaCanalePeriodoDto.setId(bdcTriplettaCanalePeriodo.getId());
                bdcTriplettaCanalePeriodoDto.setIdBroadcaster(bdcTriplettaCanalePeriodo.getBdcCanali().getBdcBroadcasters().getId());
                bdcTriplettaCanalePeriodoDto.setNomiCanaliVasca(bdcTriplettaCanalePeriodo.getNomiCanaliVasca());
                bdcTriplettaCanalePeriodoDto.setPreallocazionePercentuale(bdcTriplettaCanalePeriodo.getPreallocazionePercentuale());
                bdcTriplettaCanalePeriodoDto.setSingoloVsVasca(bdcTriplettaCanalePeriodo.getSingoloVsVasca());
                bdcTriplettaCanalePeriodoDto.setPreallocazionePercentuale(bdcTriplettaCanalePeriodo.getPreallocazionePercentuale());

                //canale
                bdcTriplettaCanalePeriodoDto.setActiveCanale(bdcTriplettaCanalePeriodo.getBdcCanali().getActive());
                bdcTriplettaCanalePeriodoDto.setDataCreazioneCanale(bdcTriplettaCanalePeriodo.getBdcCanali().getDataCreazione());
                bdcTriplettaCanalePeriodoDto.setDataInizioValidCanale(bdcTriplettaCanalePeriodo.getBdcCanali().getDataInizioValid());
                bdcTriplettaCanalePeriodoDto.setDataFineValidCanale(bdcTriplettaCanalePeriodo.getBdcCanali().getDataFineValid());
                bdcTriplettaCanalePeriodoDto.setDataUltimaModificaCanale(bdcTriplettaCanalePeriodo.getBdcCanali().getDataUltimaModifica());
                bdcTriplettaCanalePeriodoDto.setIdCanale(bdcTriplettaCanalePeriodo.getBdcCanali().getId());
                bdcTriplettaCanalePeriodoDto.setGeneralistaCanale(bdcTriplettaCanalePeriodo.getBdcCanali().getGeneralista());
                bdcTriplettaCanalePeriodoDto.setNomeCanale(bdcTriplettaCanalePeriodo.getBdcCanali().getNome());
                bdcTriplettaCanalePeriodoDto.setSpecialRadioCanale(bdcTriplettaCanalePeriodo.getBdcCanali().getSpecialRadio());
                bdcTriplettaCanalePeriodoDto.setTipoCanale(bdcTriplettaCanalePeriodo.getBdcCanali().getTipoCanale());
                
                bdcTriplettaDto.setNomeEmittente(bdcBroadcasters.getNome());
                bdcTriplettaDto.setTipoEmittente(bdcBroadcasters.getTipoBroadcaster());
                bdcTriplettaDto.setIdEmittente(bdcBroadcasters.getId());
                bdcTriplettaDto.setUtente(bdcTriplettaCanalePeriodo.getUtenteInserimento());
                
                bdcTriplettaCanalePeriodoDtolist.add(bdcTriplettaCanalePeriodoDto);

            }
        }

        bdcTriplettaDto.setCanali(bdcTriplettaCanalePeriodoDtolist);
        return bdcTriplettaDto;

    }



}
