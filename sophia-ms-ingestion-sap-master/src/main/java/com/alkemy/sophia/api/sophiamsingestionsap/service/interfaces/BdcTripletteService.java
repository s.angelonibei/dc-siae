package com.alkemy.sophia.api.sophiamsingestionsap.service.interfaces;

import com.alkemy.sophia.api.sophiamsingestionsap.dto.*;
import com.alkemy.sophia.api.sophiamsingestionsap.dto.response.MuleResponse;
import com.alkemy.sophia.api.sophiamsingestionsap.dto.response.RiepilogoTripletteResponse;
import com.alkemy.sophia.api.sophiamsingestionsap.dto.response.SaveTriplettaResponse;

import java.util.List;

public interface BdcTripletteService {

    public SaveTriplettaResponse saveTripletta(BdcTriplettaDto request );

    public List<BdcBroadcasterDto> getBroadcaster();

    public RiepilogoTripletteResponse getTriplette(FilterTripletteDto filter);
    
    public BdcTriplettaDto geTripletta (Long id);

    public List<BdcCanaliDto> getCanaliByIdBroadcaster(Long id);

/*
    public BdcTripletta findTripletta(String id);

    public List<BdcTriplettaCanalePeriodo> findByBdcTriplettaId(Long idtripletta);

    public Optional<BdcBroadcasters> findBroadcasterById(Long idBroadcaster);

    public Optional<BdcCanali> findCanaleById(Long idCanale);*/


}
