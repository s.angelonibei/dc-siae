package com.alkemy.sophia.api.sophiamsingestionsap.sqs;


import lombok.Data;

@Data
public class BodySqs {
    private String fileName;
    private String identificativo;
}
