package com.alkemy.sophia.api.sophiamsingestionsap.dto;


import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.BdcCanali;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class BdcCanaliDto {

    private Long id;
    private Long idBroadcaster;
    private String nome;
    private Boolean generalista;
    private Boolean active;
    private Boolean specialRadio;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date dataInizioValid;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date dataFineValid;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date dataCreazione;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date dataUltimaModifica;
    private String utenteUltimaModifica;
    private BdcCanali.TIPO_CANALE tipoCanale;


}
