package com.alkemy.sophia.api.sophiamsingestionsap.rest;


import com.alkemy.sophia.api.sophiamsingestionsap.dto.*;
import com.alkemy.sophia.api.sophiamsingestionsap.dto.response.*;
import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.*;
import com.alkemy.sophia.api.sophiamsingestionsap.service.interfaces.BdcTripletteService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mysema.commons.lang.Assert;
import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.List;

@RestController
@CommonsLog
@RequiredArgsConstructor
@RequestMapping("")
public class GestioneTripletteController {

    private final BdcTripletteService bdcTripletteService;

    private final Long PAGE_SIZE = 50l;

    @GetMapping(value = "/broadcasters", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    List<BdcBroadcasterDto> getBroadcaster() {
        return bdcTripletteService.getBroadcaster();
    }


    @GetMapping(value = "/broadcasters/{id}/canali", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    List<BdcCanaliDto> getBroadcaster(@PathVariable Long id) {

        return bdcTripletteService.getCanaliByIdBroadcaster(id);
    }

    @GetMapping(value = "/triplette", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    RiepilogoTripletteResponse getTriplette(@RequestParam(value = "order") String order,
                                            @RequestParam(value = "page") Long page,
                                            @RequestParam(value = "count") Boolean count,
                                            @RequestParam(value = "pageSize", required = false) Long pageSize,
                                            @RequestParam(value = "idEmittente", required = false) Long idEmittente,
                                            @RequestParam(value = "idCanale", required = false) Long idCanale,
                                            @RequestParam(value = "tipoEmittente", required = false) BdcBroadcasters.TIPO_CANALE tipoEmittente,
                                            @RequestParam(value = "tipoDiritto", required = false) Diritto tipoDiritto,
                                            @RequestParam(value = "stato", required = false) BdcTripletta.Stato stato,
                                            @RequestParam(value = "tripletta", required = false) String tripletta,
                                            @RequestParam(value = "canali", required = false) Boolean withCanali
                                                  ) {
        if(pageSize == null)
            pageSize = PAGE_SIZE;

        FilterTripletteDto filterTripletteDto =
                new FilterTripletteDto(order,page,count,pageSize,idEmittente,idCanale,tipoEmittente,tipoDiritto,stato,tripletta,withCanali);
        return bdcTripletteService.getTriplette( filterTripletteDto );

    }

    @GetMapping(value = "/triplette/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    BdcTriplettaDto getTripletta(@PathVariable Long id) {

        return bdcTripletteService.geTripletta(id);
    }

    @Transactional
    @PostMapping(path = "/triplette", consumes = "application/json", produces = "application/json")
    public @ResponseBody SaveTriplettaResponse savetripletta(@RequestBody BdcTriplettaDto request){
    	SaveTriplettaResponse response = new SaveTriplettaResponse();
        EsitoSaveTripletta esito = new EsitoSaveTripletta();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        List<BdcTriplettaCanalePeriodoDto> canaliDtoList = request.getCanali();

        try {

            log.info("inizio controlli di coerenza");
            log.info("request.getCanali()=" + request.getCanali());
            log.info("request.getAttribuzione()=" + request.getAttribuzione());
            log.info("request.getConto()=" + request.getConto());
            log.info("request.getChiaveRif3()=" + request.getChiaveRif3());
            log.info("request.getRifTemporaleIncassi()=" + request.getRifTemporaleIncassi());
            log.info("request.getTipoDiritto()=" + request.getTipoDiritto());
            log.info("request.getGiornoMeseDa()=" + request.getGiornoMeseDa());
            log.info("request.getGiornoMeseA()="+ request.getGiornoMeseA());

            Assert.notNull(request.getCanali(), "Canale/i non valorizzato");
            Assert.notNull(request.getAttribuzione(), "attribuzione non valorizzato");
            Assert.notNull(request.getConto(), "conto non valorizzato");
            Assert.notNull(request.getChiaveRif3(), "chiave rif3 non valorizzato");
            Assert.notNull(request.getRifTemporaleIncassi(), "riferimento temporale non valorizzato");
            Assert.notNull(request.getTipoDiritto(), "tipo Diritto non valorizzato");
            Assert.notNull(request.getUtente(), "Utente non valorizzato");
            Assert.notNull(request.getGiornoMeseDa(), "GiornoMeseDa non valorizzato");
            Assert.notNull(request.getGiornoMeseA(), "GiornoMeseA non valorizzato");


            //controllo che se valorizzata la percentuale di preallocazione faccia 100%
            log.info("controllo che se valorizzata la percentuale di preallocazione faccia 100%" );
            Double controlloPreallocazione=new Double(0);
            for (BdcTriplettaCanalePeriodoDto bdcCanaliDto : canaliDtoList) {
                if(bdcCanaliDto.getPreallocazionePercentuale() != null) {
                    controlloPreallocazione=controlloPreallocazione+bdcCanaliDto.getPreallocazionePercentuale();
                    log.info("il valore del controllo = " + controlloPreallocazione );
                }else{
                    controlloPreallocazione=new Double(0);
                    break;
                }
            }
            if(!controlloPreallocazione.equals(new Double(0)) && !controlloPreallocazione.equals(new Double(100)) ){

                log.error("La somma della preallocazione incassi, se valorizzata deve valere 100");
                esito.setCodice("KO");
                esito.setDescrizione("La somma della preallocazione incassi, se valorizzata deve valere 100");
                response.setEsito(esito);
                //throw new Exception()
                return response;
            }

            log.info("fine controlli di coerenza");

        }catch(Exception e){
            esito.setCodice("KO");
            if(e.getMessage() != null)
                esito.setDescrizione(e.getMessage());
            else
                esito.setDescrizione(e.toString());
            response.setEsito(esito);
            return response;
        }

            return bdcTripletteService.saveTripletta(request);

    }
    public static void main(String[] args) throws Exception{

        /*//save Tripletta
        BdcTriplettaDto bdcTriplettaDto = new BdcTriplettaDto();

        bdcTriplettaDto.setAttribuzione(new Long(1000141545));
        BdcCanaliDto canaliDto1 = new BdcCanaliDto();
        canaliDto1.setNome("ciao");
        BdcCanaliDto canaliDto2 = new BdcCanaliDto();
        canaliDto2.setNome("ciao");
        List<BdcCanaliDto> list = new ArrayList<BdcCanaliDto>();
        list.add(canaliDto2);
        list.add(canaliDto1);
        bdcTriplettaDto.setCanali(list);
        ObjectMapper mapper = new ObjectMapper();
         System.out.println(mapper.writeValueAsString(bdcTriplettaDto));
*/

        //getTriplette
        /*FilterTripletteDto filterTripletteDto = new FilterTripletteDto();

        filterTripletteDto.setIdEmittente(new Long(19));

        ObjectMapper mapper = new ObjectMapper();
        System.out.println(mapper.writeValueAsString(filterTripletteDto));
*/


    }

}
