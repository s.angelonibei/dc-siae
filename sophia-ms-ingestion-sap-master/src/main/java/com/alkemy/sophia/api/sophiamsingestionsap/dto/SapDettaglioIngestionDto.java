package com.alkemy.sophia.api.sophiamsingestionsap.dto;


import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.BdcBroadcasters;
import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.BdcBroadcasters.TIPO_CANALE;
import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.BdcIngestionSap;
import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.BdcIngestionSap.STATO;
import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.BdcTriplettaCanalePeriodo;
import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.BdcTriplettaCanalePeriodo.SingoloVsVasca;
import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.Diritto;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.util.Date;

@Data
@JsonPropertyOrder({ "tripletta", "tipoTripletta", "tipoDiritto", "emittente", "tipoEmittente", "canale", "importo",
		"dataDa", "dataA", "stato", "errore", "regolaPreallocazione" })
public class SapDettaglioIngestionDto {

    private String tripletta;
    private BdcTriplettaCanalePeriodo.SingoloVsVasca tipoTripletta;
    private String canale;
    private String periodo;
    private Diritto tipoDiritto;
    private Double importo;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date dataDa;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date dataA;
    private Double regolaPreallocazione;
    private BdcIngestionSap.STATO stato;
    private String errore;
    private String recordSap;

    private String emittente;
    private BdcBroadcasters.TIPO_CANALE tipoEmittente;

    private String conto;
    private String attribuzione;
    private String chiaveRif3;

    public enum ORDER_COLUMN{
        tripletta,
        tipoTripletta,
        canale,
        periodo,
        tipoDiritto,
        importo,
        dataDa,
        dataA,
        regolaPreallocazione,
        stato,
        errore,
        emittente,
        tipoEmittente

    }

	public abstract class SapDettaglioIngestionDtoCSVExportFields {

		@JsonIgnore
		public Double regolaPreallocazione;

		@JsonSerialize(converter = NumberDeserializer.class)
		public Double importo;
		
		@JsonSerialize(converter = PeriodoConverter.class)
		private String periodo;

	}

}
