package com.alkemy.sophia.api.sophiamsingestionsap.dto;


import lombok.Data;

import java.util.Date;
import java.util.List;

/*
Header SAP inutile
 */
@Data
public class MessaggioDto {

    String ID;
    Date TimeStamp;
    String Riferimento;
}
