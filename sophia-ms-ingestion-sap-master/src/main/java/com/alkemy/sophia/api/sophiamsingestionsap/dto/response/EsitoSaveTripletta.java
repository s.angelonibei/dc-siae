package com.alkemy.sophia.api.sophiamsingestionsap.dto.response;

import lombok.Data;

@Data
public
class EsitoSaveTripletta {
    String codice;
    String descrizione;
}