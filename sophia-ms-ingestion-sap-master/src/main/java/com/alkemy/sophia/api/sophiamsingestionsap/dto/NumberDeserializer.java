package com.alkemy.sophia.api.sophiamsingestionsap.dto;

import com.fasterxml.jackson.databind.util.StdConverter;

import java.text.NumberFormat;
import java.util.Locale;


public class NumberDeserializer extends StdConverter<Double, String> {


        @Override
        public String convert(Double value) {
            try {
                Locale locale = new Locale("it", "IT");
                NumberFormat numberFormat = NumberFormat.getInstance(locale);

                return numberFormat.format(value);
            }catch(Exception e){ }
            return "";
        }
    }
