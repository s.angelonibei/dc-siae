package com.alkemy.sophia.api.sophiamsingestionsap.model.repo;

import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.BdcBroadcasters;
import com.alkemy.sophia.api.sophiamsingestionsap.model.entities.BdcTripletta;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BdcBroadcasterRepo extends JpaRepository<BdcBroadcasters, Long> {
}
