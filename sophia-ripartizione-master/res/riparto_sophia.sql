DROP  TABLE dev_riparto_siada

CREATE EXTERNAL TABLE dev_riparto_siada
(
   codice_provider                    STRING,
   offerta_commerciale                STRING,
   codice_tipo_utilizzo               STRING,
   territorio                         STRING,
   mese_incasso_fattura               STRING,
   anno_riferimento_utilizzazione     STRING,
   tipo_periodo_di_riferimento        STRING,
   periodo_riferimento_utilizzazione  STRING,
   tipo_utilizzazione                 STRING,
   tipo_repertorio                    STRING,
   quantita_utilizzazioni             STRING,
   codice_opera                       STRING,
   flag_opera                         STRING,
   tipo_codifica                      STRING,
   qualifica_avente_diritto           STRING,
   posizione_siae_avente_diritto      STRING,
   ipi_number_ad                      STRING,
   numeratore_quota_dem               STRING,
   denominatore_quota_dem             STRING,
   codice_conto_dem                   STRING,
   numero_conto_dem                   STRING,
   compenso_dem_punto                 STRING,
   numeratore_quota_drm               STRING,
   denominatore_quota_drm             STRING,
   codice_conto_drm                   STRING,
   numero_conto_drm                   STRING,
   compenso_drm_punto                 STRING,
   percentuale_di_claim               STRING,
   titolo_utilizzazione               STRING,
   nominativo_utilizzazione           STRING,
   compenso_dem_netto                 STRING,
   compenso_drm_netto                 STRING
)
PARTITIONED BY
(
  dsp_p STRING,
  year_p STRING,
  month_p STRING,
  dsr_p STRING
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
        WITH SERDEPROPERTIES (
           "separatorChar" = "\;",
           "quoteChar"     = '\"'
        )
STORED AS TEXTFILE
LOCATION 's3://siae-sophia-datalake/dev/riparto-siada';
