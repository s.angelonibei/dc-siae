#!/bin/sh

ENVNAME=dev
APPNAME=sophia-ripartizione
HOME=/var/local/sophia/ripartizione
#HOME=/home/orchestrator/sophia/ripartizione
VERSION=1.0.0
JVMOPTS="-Dlog4j.configuration=file://$HOME/log4j.properties -Dlog4j.configurationFile=$HOME/log4j2.xml"

java $JVMOPTS -jar $HOME/$APPNAME-$VERSION-jar-with-dependencies.jar $HOME/$APPNAME.properties $1 2>&1
