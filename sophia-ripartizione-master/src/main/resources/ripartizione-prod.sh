#!/bin/sh

ENV=prod
APPNAME=sophia-ripartizione
HOME=/home/orchestrator/sophia/ripartizione
VERSION=1.0.2
JVMOPTS="-Dspring.profiles.active=prod -Dspring.config.location=$HOME/ripartizione.yml "
JSTDOUT="$HOME/sophia-ripartizione.log"

echo "nohup /opt/java8/bin/java $JVMOPTS -jar $HOME/$APPNAME-$VERSION-spring-boot.jar >> $JSTDOUT 2>&1 &"

nohup /opt/java8/bin/java $JVMOPTS -jar $HOME/$APPNAME-$VERSION-spring-boot.jar >> $JSTDOUT 2>&1 &


