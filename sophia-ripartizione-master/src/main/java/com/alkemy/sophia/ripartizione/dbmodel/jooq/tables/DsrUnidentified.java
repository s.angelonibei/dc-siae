/*
 * This file is generated by jOOQ.
 */
package com.alkemy.sophia.ripartizione.dbmodel.jooq.tables;


import com.alkemy.sophia.ripartizione.dbmodel.jooq.DefaultSchema;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.Indexes;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.Keys;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.tables.records.DsrUnidentifiedRecord;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.9"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class DsrUnidentified extends TableImpl<DsrUnidentifiedRecord> {

    private static final long serialVersionUID = 621706958;

    /**
     * The reference instance of <code>DSR_UNIDENTIFIED</code>
     */
    public static final DsrUnidentified DSR_UNIDENTIFIED = new DsrUnidentified();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<DsrUnidentifiedRecord> getRecordType() {
        return DsrUnidentifiedRecord.class;
    }

    /**
     * The column <code>DSR_UNIDENTIFIED.IDDSR</code>.
     */
    public final TableField<DsrUnidentifiedRecord, String> IDDSR = createField("IDDSR", org.jooq.impl.SQLDataType.VARCHAR(512).nullable(false), this, "");

    /**
     * The column <code>DSR_UNIDENTIFIED.INSERT_TIME</code>.
     */
    public final TableField<DsrUnidentifiedRecord, Timestamp> INSERT_TIME = createField("INSERT_TIME", org.jooq.impl.SQLDataType.TIMESTAMP.nullable(false).defaultValue(org.jooq.impl.DSL.field("CURRENT_TIMESTAMP", org.jooq.impl.SQLDataType.TIMESTAMP)), this, "");

    /**
     * The column <code>DSR_UNIDENTIFIED.TOTAL_ROWS</code>.
     */
    public final TableField<DsrUnidentifiedRecord, Long> TOTAL_ROWS = createField("TOTAL_ROWS", org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * Create a <code>DSR_UNIDENTIFIED</code> table reference
     */
    public DsrUnidentified() {
        this(DSL.name("DSR_UNIDENTIFIED"), null);
    }

    /**
     * Create an aliased <code>DSR_UNIDENTIFIED</code> table reference
     */
    public DsrUnidentified(String alias) {
        this(DSL.name(alias), DSR_UNIDENTIFIED);
    }

    /**
     * Create an aliased <code>DSR_UNIDENTIFIED</code> table reference
     */
    public DsrUnidentified(Name alias) {
        this(alias, DSR_UNIDENTIFIED);
    }

    private DsrUnidentified(Name alias, Table<DsrUnidentifiedRecord> aliased) {
        this(alias, aliased, null);
    }

    private DsrUnidentified(Name alias, Table<DsrUnidentifiedRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> DsrUnidentified(Table<O> child, ForeignKey<O, DsrUnidentifiedRecord> key) {
        super(child, key, DSR_UNIDENTIFIED);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return DefaultSchema.DEFAULT_SCHEMA;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.DSR_UNIDENTIFIED_PRIMARY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<DsrUnidentifiedRecord> getPrimaryKey() {
        return Keys.KEY_DSR_UNIDENTIFIED_PRIMARY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<DsrUnidentifiedRecord>> getKeys() {
        return Arrays.<UniqueKey<DsrUnidentifiedRecord>>asList(Keys.KEY_DSR_UNIDENTIFIED_PRIMARY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DsrUnidentified as(String alias) {
        return new DsrUnidentified(DSL.name(alias), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DsrUnidentified as(Name alias) {
        return new DsrUnidentified(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public DsrUnidentified rename(String name) {
        return new DsrUnidentified(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public DsrUnidentified rename(Name name) {
        return new DsrUnidentified(name, null);
    }
}
