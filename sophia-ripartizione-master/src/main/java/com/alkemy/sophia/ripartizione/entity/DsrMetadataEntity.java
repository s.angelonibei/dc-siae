package com.alkemy.sophia.ripartizione.entity;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by ftomei on 28/06/19.
 */
@Entity
@Table(name = "DSR_METADATA", schema = "", catalog = "MCMDB_debug")
public class DsrMetadataEntity {
	private String iddsr;
	private String iddsp;
	private Long salesLinesNum;
	private BigDecimal totalValue;
	private Long subscriptionsNum;
	private String creationTimestamp;
	private String periodType;
	private Integer period;
	private Integer year;
	private String currency;
	private long backclaim;

	@Id
	@Column(name = "IDDSR", nullable = false, insertable = true, updatable = true, length = 512)
	public String getIddsr() {
		return iddsr;
	}

	public void setIddsr(String iddsr) {
		this.iddsr = iddsr;
	}

	@Column(name = "IDDSP", nullable = false, insertable = true, updatable = true, length = 100)
	public String getIddsp() {
		return iddsp;
	}

	public void setIddsp(String iddsp) {
		this.iddsp = iddsp;
	}

	@Column(name = "SALES_LINES_NUM", nullable = true, insertable = true, updatable = true)
	public Long getSalesLinesNum() {
		return salesLinesNum;
	}

	public void setSalesLinesNum(Long salesLinesNum) {
		this.salesLinesNum = salesLinesNum;
	}

	@Column(name = "TOTAL_VALUE", nullable = true, insertable = true, updatable = true, precision = 20)
	public BigDecimal getTotalValue() {
		return totalValue;
	}

	public void setTotalValue(BigDecimal totalValue) {
		this.totalValue = totalValue;
	}

	@Column(name = "SUBSCRIPTIONS_NUM", nullable = true, insertable = true, updatable = true)
	public Long getSubscriptionsNum() {
		return subscriptionsNum;
	}

	public void setSubscriptionsNum(Long subscriptionsNum) {
		this.subscriptionsNum = subscriptionsNum;
	}

	@Column(name = "CREATION_TIMESTAMP", nullable = true, insertable = true, updatable = true, length = 11)
	public String getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(String creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	@Column(name = "PERIOD_TYPE", nullable = true, insertable = true, updatable = true, length = 10)
	public String getPeriodType() {
		return periodType;
	}

	public void setPeriodType(String periodType) {
		this.periodType = periodType;
	}

	@Column(name = "PERIOD", nullable = true, insertable = true, updatable = true)
	public Integer getPeriod() {
		return period;
	}

	public void setPeriod(Integer period) {
		this.period = period;
	}

	@Column(name = "YEAR", nullable = true, insertable = true, updatable = true)
	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	@Column(name = "CURRENCY", nullable = true, insertable = true, updatable = true, length = 65535)
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	@Column(name = "BACKCLAIM", nullable = false, insertable = true, updatable = true)
	public long getBackclaim() {
		return backclaim;
	}

	public void setBackclaim(long backclaim) {
		this.backclaim = backclaim;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		DsrMetadataEntity that = (DsrMetadataEntity) o;

		if (backclaim != that.backclaim) return false;
		if (creationTimestamp != null ? !creationTimestamp.equals(that.creationTimestamp) : that.creationTimestamp != null)
			return false;
		if (currency != null ? !currency.equals(that.currency) : that.currency != null) return false;
		if (iddsp != null ? !iddsp.equals(that.iddsp) : that.iddsp != null) return false;
		if (iddsr != null ? !iddsr.equals(that.iddsr) : that.iddsr != null) return false;
		if (period != null ? !period.equals(that.period) : that.period != null) return false;
		if (periodType != null ? !periodType.equals(that.periodType) : that.periodType != null) return false;
		if (salesLinesNum != null ? !salesLinesNum.equals(that.salesLinesNum) : that.salesLinesNum != null) return false;
		if (subscriptionsNum != null ? !subscriptionsNum.equals(that.subscriptionsNum) : that.subscriptionsNum != null)
			return false;
		if (totalValue != null ? !totalValue.equals(that.totalValue) : that.totalValue != null) return false;
		if (year != null ? !year.equals(that.year) : that.year != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = iddsr != null ? iddsr.hashCode() : 0;
		result = 31 * result + (iddsp != null ? iddsp.hashCode() : 0);
		result = 31 * result + (salesLinesNum != null ? salesLinesNum.hashCode() : 0);
		result = 31 * result + (totalValue != null ? totalValue.hashCode() : 0);
		result = 31 * result + (subscriptionsNum != null ? subscriptionsNum.hashCode() : 0);
		result = 31 * result + (creationTimestamp != null ? creationTimestamp.hashCode() : 0);
		result = 31 * result + (periodType != null ? periodType.hashCode() : 0);
		result = 31 * result + (period != null ? period.hashCode() : 0);
		result = 31 * result + (year != null ? year.hashCode() : 0);
		result = 31 * result + (currency != null ? currency.hashCode() : 0);
		result = 31 * result + (int) (backclaim ^ (backclaim >>> 32));
		return result;
	}
}
