/*
 * This file is generated by jOOQ.
 */
package com.alkemy.sophia.ripartizione.dbmodel.jooq;


import com.alkemy.sophia.ripartizione.dbmodel.jooq.tables.AnagClient;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.tables.AnagDsp;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.tables.AnagUtilizationType;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.tables.BackclaimInProgress;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.tables.CcidMetadata;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.tables.CcidS3Path;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.tables.ClientToDsp;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.tables.CommercialOffers;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.tables.CurrencyRate;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.tables.DsrMetadata;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.tables.DsrMetadataConfig;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.tables.DsrStatistics;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.tables.DsrStatisticsSophia;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.tables.DsrStepsMonitoring;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.tables.DsrUnidentified;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.tables.ImportiAnticipo;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.tables.Invoice;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.tables.InvoiceAccantonamento;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.tables.InvoiceItem;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.tables.InvoiceItemToCcid;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.tables.InvoiceLog;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.tables.InvoiceReclamo;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.tables.ItemInvoiceReason;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.tables.MmRipartizioneCarico;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.tables.MmRipartizioneCaricoCcid;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.tables.MmRipartizione_648;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.tables.UnidentifiedSongDsr;

import javax.annotation.Generated;

import org.jooq.Index;
import org.jooq.OrderField;
import org.jooq.impl.Internal;


/**
 * A class modelling indexes of tables of the <code></code> schema.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.9"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Indexes {

    // -------------------------------------------------------------------------
    // INDEX definitions
    // -------------------------------------------------------------------------

    public static final Index ANAG_CLIENT_FK_COUNTRY_ANAG_CLIENT_IDX = Indexes0.ANAG_CLIENT_FK_COUNTRY_ANAG_CLIENT_IDX;
    public static final Index ANAG_CLIENT_PRIMARY = Indexes0.ANAG_CLIENT_PRIMARY;
    public static final Index ANAG_DSP_CODE_UNIQUE = Indexes0.ANAG_DSP_CODE_UNIQUE;
    public static final Index ANAG_DSP_NAME_UNIQUE = Indexes0.ANAG_DSP_NAME_UNIQUE;
    public static final Index ANAG_DSP_PRIMARY = Indexes0.ANAG_DSP_PRIMARY;
    public static final Index ANAG_UTILIZATION_TYPE_PRIMARY = Indexes0.ANAG_UTILIZATION_TYPE_PRIMARY;
    public static final Index BACKCLAIM_IN_PROGRESS_PRIMARY = Indexes0.BACKCLAIM_IN_PROGRESS_PRIMARY;
    public static final Index CCID_METADATA_FK_ID_DSR_CCID_METADATA_IDX = Indexes0.CCID_METADATA_FK_ID_DSR_CCID_METADATA_IDX;
    public static final Index CCID_METADATA_FK_ID_INVOICE_ITEM_CCID_METADATA = Indexes0.CCID_METADATA_FK_ID_INVOICE_ITEM_CCID_METADATA;
    public static final Index CCID_METADATA_IDX_UQ__CCID_DSR_CCID_METADATA = Indexes0.CCID_METADATA_IDX_UQ__CCID_DSR_CCID_METADATA;
    public static final Index CCID_METADATA_PRIMARY = Indexes0.CCID_METADATA_PRIMARY;
    public static final Index CCID_S3_PATH_IDDSR_FK_IDX = Indexes0.CCID_S3_PATH_IDDSR_FK_IDX;
    public static final Index CCID_S3_PATH_IDX_UQ_DSR_CCID_PATH = Indexes0.CCID_S3_PATH_IDX_UQ_DSR_CCID_PATH;
    public static final Index CCID_S3_PATH_IX_CCID_S3_PATH_8055281B0DD3DFFB = Indexes0.CCID_S3_PATH_IX_CCID_S3_PATH_8055281B0DD3DFFB;
    public static final Index CCID_S3_PATH_PRIMARY = Indexes0.CCID_S3_PATH_PRIMARY;
    public static final Index CLIENT_TO_DSP_FK_CLIENT_TO_DSP_ANAG_CLIENT_ID_IDX = Indexes0.CLIENT_TO_DSP_FK_CLIENT_TO_DSP_ANAG_CLIENT_ID_IDX;
    public static final Index CLIENT_TO_DSP_FK_CLIENT_TO_DSP_IDDSP_IDX = Indexes0.CLIENT_TO_DSP_FK_CLIENT_TO_DSP_IDDSP_IDX;
    public static final Index CLIENT_TO_DSP_PRIMARY = Indexes0.CLIENT_TO_DSP_PRIMARY;
    public static final Index COMMERCIAL_OFFERS_FK_CO_APPLIED_TARIFF_IDX = Indexes0.COMMERCIAL_OFFERS_FK_CO_APPLIED_TARIFF_IDX;
    public static final Index COMMERCIAL_OFFERS_FK_CO_SERVICE_TYPE_IDX = Indexes0.COMMERCIAL_OFFERS_FK_CO_SERVICE_TYPE_IDX;
    public static final Index COMMERCIAL_OFFERS_FK_CO_USE_TYPE_IDX = Indexes0.COMMERCIAL_OFFERS_FK_CO_USE_TYPE_IDX;
    public static final Index COMMERCIAL_OFFERS_IDX_UQ_CONFIG = Indexes0.COMMERCIAL_OFFERS_IDX_UQ_CONFIG;
    public static final Index COMMERCIAL_OFFERS_PRIMARY = Indexes0.COMMERCIAL_OFFERS_PRIMARY;
    public static final Index CURRENCY_RATE_PRIMARY = Indexes0.CURRENCY_RATE_PRIMARY;
    public static final Index DSR_METADATA_DSR_METADATA_BACKCLAIM_IDX = Indexes0.DSR_METADATA_DSR_METADATA_BACKCLAIM_IDX;
    public static final Index DSR_METADATA_DSR_METADATA_BACKCLAIM_IN_PROGRESS_IDX = Indexes0.DSR_METADATA_DSR_METADATA_BACKCLAIM_IN_PROGRESS_IDX;
    public static final Index DSR_METADATA_FK_DSR_METADATA_ANAG_COUNTRY_IDX = Indexes0.DSR_METADATA_FK_DSR_METADATA_ANAG_COUNTRY_IDX;
    public static final Index DSR_METADATA_FK_DSR_METADATA_COMMERCIAL_OFFER_IDX = Indexes0.DSR_METADATA_FK_DSR_METADATA_COMMERCIAL_OFFER_IDX;
    public static final Index DSR_METADATA_IX_DSR_METADATA_8055281B0DD3DFFB = Indexes0.DSR_METADATA_IX_DSR_METADATA_8055281B0DD3DFFB;
    public static final Index DSR_METADATA_PRIMARY = Indexes0.DSR_METADATA_PRIMARY;
    public static final Index DSR_METADATA_CONFIG_FK_ID_COMMERCIAL_OFFER_IDX = Indexes0.DSR_METADATA_CONFIG_FK_ID_COMMERCIAL_OFFER_IDX;
    public static final Index DSR_METADATA_CONFIG_PRIMARY = Indexes0.DSR_METADATA_CONFIG_PRIMARY;
    public static final Index DSR_STATISTICS_IX_DSR_STATISTICS_8055281B0DD3DFFB = Indexes0.DSR_STATISTICS_IX_DSR_STATISTICS_8055281B0DD3DFFB;
    public static final Index DSR_STATISTICS_PRIMARY = Indexes0.DSR_STATISTICS_PRIMARY;
    public static final Index DSR_STATISTICS_SOPHIA_IX_DSR_STATISTICS_8055281B0DD3DFFB = Indexes0.DSR_STATISTICS_SOPHIA_IX_DSR_STATISTICS_8055281B0DD3DFFB;
    public static final Index DSR_STATISTICS_SOPHIA_IX_DSR_STATISTICS_SOPHIA_8055281B0DD3DFFB = Indexes0.DSR_STATISTICS_SOPHIA_IX_DSR_STATISTICS_SOPHIA_8055281B0DD3DFFB;
    public static final Index DSR_STATISTICS_SOPHIA_PRIMARY = Indexes0.DSR_STATISTICS_SOPHIA_PRIMARY;
    public static final Index DSR_STEPS_MONITORING_DSR_STEPS_MONITORING_IDX_01 = Indexes0.DSR_STEPS_MONITORING_DSR_STEPS_MONITORING_IDX_01;
    public static final Index DSR_STEPS_MONITORING_PRIMARY = Indexes0.DSR_STEPS_MONITORING_PRIMARY;
    public static final Index DSR_UNIDENTIFIED_PRIMARY = Indexes0.DSR_UNIDENTIFIED_PRIMARY;
    public static final Index IMPORTI_ANTICIPO_FK_IMPORTI_ANTICIPO_1_IDX = Indexes0.IMPORTI_ANTICIPO_FK_IMPORTI_ANTICIPO_1_IDX;
    public static final Index IMPORTI_ANTICIPO_PRIMARY = Indexes0.IMPORTI_ANTICIPO_PRIMARY;
    public static final Index INVOICE_FK_CLIENT_ID_INVOICE_IDX = Indexes0.INVOICE_FK_CLIENT_ID_INVOICE_IDX;
    public static final Index INVOICE_PRIMARY = Indexes0.INVOICE_PRIMARY;
    public static final Index INVOICE_ACCANTONAMENTO_FK_INVOICE_ACCANTONAMENTO_INVOICE_ID = Indexes0.INVOICE_ACCANTONAMENTO_FK_INVOICE_ACCANTONAMENTO_INVOICE_ID;
    public static final Index INVOICE_ACCANTONAMENTO_PRIMARY = Indexes0.INVOICE_ACCANTONAMENTO_PRIMARY;
    public static final Index INVOICE_ITEM_INVOICE_ITEM_INDEX_01 = Indexes0.INVOICE_ITEM_INVOICE_ITEM_INDEX_01;
    public static final Index INVOICE_ITEM_INVOICE_ITEM_INDEX_02 = Indexes0.INVOICE_ITEM_INVOICE_ITEM_INDEX_02;
    public static final Index INVOICE_ITEM_PRIMARY = Indexes0.INVOICE_ITEM_PRIMARY;
    public static final Index INVOICE_ITEM_TO_CCID_FK_TO_CCID_METADATA = Indexes0.INVOICE_ITEM_TO_CCID_FK_TO_CCID_METADATA;
    public static final Index INVOICE_ITEM_TO_CCID_FK_TO_INVOICE_ITEM = Indexes0.INVOICE_ITEM_TO_CCID_FK_TO_INVOICE_ITEM;
    public static final Index INVOICE_ITEM_TO_CCID_PRIMARY = Indexes0.INVOICE_ITEM_TO_CCID_PRIMARY;
    public static final Index INVOICE_LOG_FK_INVOICE_LOG_01 = Indexes0.INVOICE_LOG_FK_INVOICE_LOG_01;
    public static final Index INVOICE_LOG_INVOICE_LOG_UNIQUE_01 = Indexes0.INVOICE_LOG_INVOICE_LOG_UNIQUE_01;
    public static final Index INVOICE_LOG_PRIMARY = Indexes0.INVOICE_LOG_PRIMARY;
    public static final Index INVOICE_RECLAMO_PRIMARY = Indexes0.INVOICE_RECLAMO_PRIMARY;
    public static final Index ITEM_INVOICE_REASON_ITEM_INVOICE_REASON_UNIQUE_01 = Indexes0.ITEM_INVOICE_REASON_ITEM_INVOICE_REASON_UNIQUE_01;
    public static final Index ITEM_INVOICE_REASON_ITEM_INVOICE_REASON_UNIQUE_02 = Indexes0.ITEM_INVOICE_REASON_ITEM_INVOICE_REASON_UNIQUE_02;
    public static final Index ITEM_INVOICE_REASON_PRIMARY = Indexes0.ITEM_INVOICE_REASON_PRIMARY;
    public static final Index MM_RIPARTIZIONE_648_MM_RIPARTIZIONE_648_ID_SOCIETA_TUTELA_FK = Indexes0.MM_RIPARTIZIONE_648_MM_RIPARTIZIONE_648_ID_SOCIETA_TUTELA_FK;
    public static final Index MM_RIPARTIZIONE_648_MM_RIPARTIZIONE_648_ID_UINDEX = Indexes0.MM_RIPARTIZIONE_648_MM_RIPARTIZIONE_648_ID_UINDEX;
    public static final Index MM_RIPARTIZIONE_648_PRIMARY = Indexes0.MM_RIPARTIZIONE_648_PRIMARY;
    public static final Index MM_RIPARTIZIONE_CARICO_MM_RIPARTIZIONE_CARICHI_ID_UINDEX = Indexes0.MM_RIPARTIZIONE_CARICO_MM_RIPARTIZIONE_CARICHI_ID_UINDEX;
    public static final Index MM_RIPARTIZIONE_CARICO_PRIMARY = Indexes0.MM_RIPARTIZIONE_CARICO_PRIMARY;
    public static final Index MM_RIPARTIZIONE_CARICO_CCID_MM_RIPARTIZIONE_CARICO_CCID_ID_UINDEX = Indexes0.MM_RIPARTIZIONE_CARICO_CCID_MM_RIPARTIZIONE_CARICO_CCID_ID_UINDEX;
    public static final Index MM_RIPARTIZIONE_CARICO_CCID_MM_RIPARTIZIONE_CARICO_CCID_INVOICE_ITEM_TO_CCID_ID_FK = Indexes0.MM_RIPARTIZIONE_CARICO_CCID_MM_RIPARTIZIONE_CARICO_CCID_INVOICE_ITEM_TO_CCID_ID_FK;
    public static final Index MM_RIPARTIZIONE_CARICO_CCID_MM_RIPARTIZIONE_CARICO_CCID_MM_RIPARTIZIONE_CARICHI_ID_FK = Indexes0.MM_RIPARTIZIONE_CARICO_CCID_MM_RIPARTIZIONE_CARICO_CCID_MM_RIPARTIZIONE_CARICHI_ID_FK;
    public static final Index MM_RIPARTIZIONE_CARICO_CCID_PRIMARY = Indexes0.MM_RIPARTIZIONE_CARICO_CCID_PRIMARY;
    public static final Index UNIDENTIFIED_SONG_DSR_PRIMARY = Indexes0.UNIDENTIFIED_SONG_DSR_PRIMARY;
    public static final Index UNIDENTIFIED_SONG_DSR_UNIDENTIFIED_SONG_DSR_IDX_01 = Indexes0.UNIDENTIFIED_SONG_DSR_UNIDENTIFIED_SONG_DSR_IDX_01;
    public static final Index UNIDENTIFIED_SONG_DSR_UNIDENTIFIED_SONG_DSR_IDX_02 = Indexes0.UNIDENTIFIED_SONG_DSR_UNIDENTIFIED_SONG_DSR_IDX_02;

    // -------------------------------------------------------------------------
    // [#1459] distribute members to avoid static initialisers > 64kb
    // -------------------------------------------------------------------------

    private static class Indexes0 {
        public static Index ANAG_CLIENT_FK_COUNTRY_ANAG_CLIENT_IDX = Internal.createIndex("FK_COUNTRY_ANAG_CLIENT_idx", AnagClient.ANAG_CLIENT, new OrderField[] { AnagClient.ANAG_CLIENT.COUNTRY }, false);
        public static Index ANAG_CLIENT_PRIMARY = Internal.createIndex("PRIMARY", AnagClient.ANAG_CLIENT, new OrderField[] { AnagClient.ANAG_CLIENT.ID_ANAG_CLIENT }, true);
        public static Index ANAG_DSP_CODE_UNIQUE = Internal.createIndex("CODE_UNIQUE", AnagDsp.ANAG_DSP, new OrderField[] { AnagDsp.ANAG_DSP.CODE }, true);
        public static Index ANAG_DSP_NAME_UNIQUE = Internal.createIndex("NAME_UNIQUE", AnagDsp.ANAG_DSP, new OrderField[] { AnagDsp.ANAG_DSP.NAME }, true);
        public static Index ANAG_DSP_PRIMARY = Internal.createIndex("PRIMARY", AnagDsp.ANAG_DSP, new OrderField[] { AnagDsp.ANAG_DSP.IDDSP }, true);
        public static Index ANAG_UTILIZATION_TYPE_PRIMARY = Internal.createIndex("PRIMARY", AnagUtilizationType.ANAG_UTILIZATION_TYPE, new OrderField[] { AnagUtilizationType.ANAG_UTILIZATION_TYPE.ID_UTILIZATION_TYPE }, true);
        public static Index BACKCLAIM_IN_PROGRESS_PRIMARY = Internal.createIndex("PRIMARY", BackclaimInProgress.BACKCLAIM_IN_PROGRESS, new OrderField[] { BackclaimInProgress.BACKCLAIM_IN_PROGRESS.ID_BACKCLAIM_IN_PROGRESS }, true);
        public static Index CCID_METADATA_FK_ID_DSR_CCID_METADATA_IDX = Internal.createIndex("FK_ID_DSR_CCID_METADATA_idx", CcidMetadata.CCID_METADATA, new OrderField[] { CcidMetadata.CCID_METADATA.ID_DSR }, false);
        public static Index CCID_METADATA_FK_ID_INVOICE_ITEM_CCID_METADATA = Internal.createIndex("FK_ID_INVOICE_ITEM_CCID_METADATA", CcidMetadata.CCID_METADATA, new OrderField[] { CcidMetadata.CCID_METADATA.INVOICE_ITEM }, false);
        public static Index CCID_METADATA_IDX_UQ__CCID_DSR_CCID_METADATA = Internal.createIndex("IDX_UQ__CCID_DSR_CCID_METADATA", CcidMetadata.CCID_METADATA, new OrderField[] { CcidMetadata.CCID_METADATA.ID_CCID, CcidMetadata.CCID_METADATA.ID_DSR }, true);
        public static Index CCID_METADATA_PRIMARY = Internal.createIndex("PRIMARY", CcidMetadata.CCID_METADATA, new OrderField[] { CcidMetadata.CCID_METADATA.ID_CCID_METADATA }, true);
        public static Index CCID_S3_PATH_IDDSR_FK_IDX = Internal.createIndex("IDDSR_FK_idx", CcidS3Path.CCID_S3_PATH, new OrderField[] { CcidS3Path.CCID_S3_PATH.IDDSR }, false);
        public static Index CCID_S3_PATH_IDX_UQ_DSR_CCID_PATH = Internal.createIndex("IDX_UQ_DSR_CCID_PATH", CcidS3Path.CCID_S3_PATH, new OrderField[] { CcidS3Path.CCID_S3_PATH.IDDSR, CcidS3Path.CCID_S3_PATH.S3_PATH }, true);
        public static Index CCID_S3_PATH_IX_CCID_S3_PATH_8055281B0DD3DFFB = Internal.createIndex("ix_CCID_S3_PATH_8055281b0dd3dffb", CcidS3Path.CCID_S3_PATH, new OrderField[] { CcidS3Path.CCID_S3_PATH.IDDSR }, false);
        public static Index CCID_S3_PATH_PRIMARY = Internal.createIndex("PRIMARY", CcidS3Path.CCID_S3_PATH, new OrderField[] { CcidS3Path.CCID_S3_PATH.ID_CCID_PATH }, true);
        public static Index CLIENT_TO_DSP_FK_CLIENT_TO_DSP_ANAG_CLIENT_ID_IDX = Internal.createIndex("FK_CLIENT_TO_DSP_ANAG_CLIENT_ID_idx", ClientToDsp.CLIENT_TO_DSP, new OrderField[] { ClientToDsp.CLIENT_TO_DSP.ANAG_CLIENT_ID }, false);
        public static Index CLIENT_TO_DSP_FK_CLIENT_TO_DSP_IDDSP_IDX = Internal.createIndex("FK_CLIENT_TO_DSP_IDDSP_idx", ClientToDsp.CLIENT_TO_DSP, new OrderField[] { ClientToDsp.CLIENT_TO_DSP.IDDSP }, false);
        public static Index CLIENT_TO_DSP_PRIMARY = Internal.createIndex("PRIMARY", ClientToDsp.CLIENT_TO_DSP, new OrderField[] { ClientToDsp.CLIENT_TO_DSP.ID }, true);
        public static Index COMMERCIAL_OFFERS_FK_CO_APPLIED_TARIFF_IDX = Internal.createIndex("FK_CO_APPLIED_TARIFF_idx", CommercialOffers.COMMERCIAL_OFFERS, new OrderField[] { CommercialOffers.COMMERCIAL_OFFERS.CCID_APPLIED_TARIFF_CODE }, false);
        public static Index COMMERCIAL_OFFERS_FK_CO_SERVICE_TYPE_IDX = Internal.createIndex("FK_CO_SERVICE_TYPE_idx", CommercialOffers.COMMERCIAL_OFFERS, new OrderField[] { CommercialOffers.COMMERCIAL_OFFERS.CCID_SERVICE_TYPE_CODE }, false);
        public static Index COMMERCIAL_OFFERS_FK_CO_USE_TYPE_IDX = Internal.createIndex("FK_CO_USE_TYPE_idx", CommercialOffers.COMMERCIAL_OFFERS, new OrderField[] { CommercialOffers.COMMERCIAL_OFFERS.CCID_USE_TYPE_CODE }, false);
        public static Index COMMERCIAL_OFFERS_IDX_UQ_CONFIG = Internal.createIndex("IDX_UQ_CONFIG", CommercialOffers.COMMERCIAL_OFFERS, new OrderField[] { CommercialOffers.COMMERCIAL_OFFERS.IDDSP, CommercialOffers.COMMERCIAL_OFFERS.ID_UTIILIZATION_TYPE, CommercialOffers.COMMERCIAL_OFFERS.OFFERING }, true);
        public static Index COMMERCIAL_OFFERS_PRIMARY = Internal.createIndex("PRIMARY", CommercialOffers.COMMERCIAL_OFFERS, new OrderField[] { CommercialOffers.COMMERCIAL_OFFERS.ID_COMMERCIAL_OFFERS }, true);
        public static Index CURRENCY_RATE_PRIMARY = Internal.createIndex("PRIMARY", CurrencyRate.CURRENCY_RATE, new OrderField[] { CurrencyRate.CURRENCY_RATE.YEAR, CurrencyRate.CURRENCY_RATE.MONTH, CurrencyRate.CURRENCY_RATE.SRC_CURRENCY, CurrencyRate.CURRENCY_RATE.DST_CURRENCY }, true);
        public static Index DSR_METADATA_DSR_METADATA_BACKCLAIM_IDX = Internal.createIndex("DSR_METADATA_BACKCLAIM_IDX", DsrMetadata.DSR_METADATA, new OrderField[] { DsrMetadata.DSR_METADATA.BACKCLAIM }, false);
        public static Index DSR_METADATA_DSR_METADATA_BACKCLAIM_IN_PROGRESS_IDX = Internal.createIndex("DSR_METADATA_BACKCLAIM_IN_PROGRESS_IDX", DsrMetadata.DSR_METADATA, new OrderField[] { DsrMetadata.DSR_METADATA.ID_BACKCLAIM_IN_PROGRESS }, false);
        public static Index DSR_METADATA_FK_DSR_METADATA_ANAG_COUNTRY_IDX = Internal.createIndex("FK_DSR_METADATA_ANAG_COUNTRY_idx", DsrMetadata.DSR_METADATA, new OrderField[] { DsrMetadata.DSR_METADATA.COUNTRY }, false);
        public static Index DSR_METADATA_FK_DSR_METADATA_COMMERCIAL_OFFER_IDX = Internal.createIndex("FK_DSR_METADATA_COMMERCIAL_OFFER_idx", DsrMetadata.DSR_METADATA, new OrderField[] { DsrMetadata.DSR_METADATA.SERVICE_CODE }, false);
        public static Index DSR_METADATA_IX_DSR_METADATA_8055281B0DD3DFFB = Internal.createIndex("ix_DSR_METADATA_8055281b0dd3dffb", DsrMetadata.DSR_METADATA, new OrderField[] { DsrMetadata.DSR_METADATA.IDDSR }, false);
        public static Index DSR_METADATA_PRIMARY = Internal.createIndex("PRIMARY", DsrMetadata.DSR_METADATA, new OrderField[] { DsrMetadata.DSR_METADATA.IDDSR }, true);
        public static Index DSR_METADATA_CONFIG_FK_ID_COMMERCIAL_OFFER_IDX = Internal.createIndex("FK_ID_COMMERCIAL_OFFER_idx", DsrMetadataConfig.DSR_METADATA_CONFIG, new OrderField[] { DsrMetadataConfig.DSR_METADATA_CONFIG.ID_COMMERCIAL_OFFER }, false);
        public static Index DSR_METADATA_CONFIG_PRIMARY = Internal.createIndex("PRIMARY", DsrMetadataConfig.DSR_METADATA_CONFIG, new OrderField[] { DsrMetadataConfig.DSR_METADATA_CONFIG.ID_DSR_METADATA_CONFIG }, true);
        public static Index DSR_STATISTICS_IX_DSR_STATISTICS_8055281B0DD3DFFB = Internal.createIndex("ix_DSR_STATISTICS_8055281b0dd3dffb", DsrStatistics.DSR_STATISTICS, new OrderField[] { DsrStatistics.DSR_STATISTICS.IDDSR }, false);
        public static Index DSR_STATISTICS_PRIMARY = Internal.createIndex("PRIMARY", DsrStatistics.DSR_STATISTICS, new OrderField[] { DsrStatistics.DSR_STATISTICS.IDDSR }, true);
        public static Index DSR_STATISTICS_SOPHIA_IX_DSR_STATISTICS_8055281B0DD3DFFB = Internal.createIndex("ix_DSR_STATISTICS_8055281b0dd3dffb", DsrStatisticsSophia.DSR_STATISTICS_SOPHIA, new OrderField[] { DsrStatisticsSophia.DSR_STATISTICS_SOPHIA.IDDSR }, false);
        public static Index DSR_STATISTICS_SOPHIA_IX_DSR_STATISTICS_SOPHIA_8055281B0DD3DFFB = Internal.createIndex("ix_DSR_STATISTICS_SOPHIA_8055281b0dd3dffb", DsrStatisticsSophia.DSR_STATISTICS_SOPHIA, new OrderField[] { DsrStatisticsSophia.DSR_STATISTICS_SOPHIA.IDDSR }, false);
        public static Index DSR_STATISTICS_SOPHIA_PRIMARY = Internal.createIndex("PRIMARY", DsrStatisticsSophia.DSR_STATISTICS_SOPHIA, new OrderField[] { DsrStatisticsSophia.DSR_STATISTICS_SOPHIA.IDDSR }, true);
        public static Index DSR_STEPS_MONITORING_DSR_STEPS_MONITORING_IDX_01 = Internal.createIndex("DSR_STEPS_MONITORING_IDX_01", DsrStepsMonitoring.DSR_STEPS_MONITORING, new OrderField[] { DsrStepsMonitoring.DSR_STEPS_MONITORING.LAST_UPDATE }, false);
        public static Index DSR_STEPS_MONITORING_PRIMARY = Internal.createIndex("PRIMARY", DsrStepsMonitoring.DSR_STEPS_MONITORING, new OrderField[] { DsrStepsMonitoring.DSR_STEPS_MONITORING.IDDSR }, true);
        public static Index DSR_UNIDENTIFIED_PRIMARY = Internal.createIndex("PRIMARY", DsrUnidentified.DSR_UNIDENTIFIED, new OrderField[] { DsrUnidentified.DSR_UNIDENTIFIED.IDDSR }, true);
        public static Index IMPORTI_ANTICIPO_FK_IMPORTI_ANTICIPO_1_IDX = Internal.createIndex("fk_IMPORTI_ANTICIPO_1_idx", ImportiAnticipo.IMPORTI_ANTICIPO, new OrderField[] { ImportiAnticipo.IMPORTI_ANTICIPO.ID_INVOICE }, false);
        public static Index IMPORTI_ANTICIPO_PRIMARY = Internal.createIndex("PRIMARY", ImportiAnticipo.IMPORTI_ANTICIPO, new OrderField[] { ImportiAnticipo.IMPORTI_ANTICIPO.ID_IMPORTO_ANTICIPO }, true);
        public static Index INVOICE_FK_CLIENT_ID_INVOICE_IDX = Internal.createIndex("FK_CLIENT_ID_INVOICE_idx", Invoice.INVOICE, new OrderField[] { Invoice.INVOICE.CLIENT_ID }, false);
        public static Index INVOICE_PRIMARY = Internal.createIndex("PRIMARY", Invoice.INVOICE, new OrderField[] { Invoice.INVOICE.ID_INVOICE }, true);
        public static Index INVOICE_ACCANTONAMENTO_FK_INVOICE_ACCANTONAMENTO_INVOICE_ID = Internal.createIndex("FK_INVOICE_ACCANTONAMENTO_INVOICE_ID", InvoiceAccantonamento.INVOICE_ACCANTONAMENTO, new OrderField[] { InvoiceAccantonamento.INVOICE_ACCANTONAMENTO.ID_INVOICE }, false);
        public static Index INVOICE_ACCANTONAMENTO_PRIMARY = Internal.createIndex("PRIMARY", InvoiceAccantonamento.INVOICE_ACCANTONAMENTO, new OrderField[] { InvoiceAccantonamento.INVOICE_ACCANTONAMENTO.ID_INVOICE_ACCANTONAMENTO }, true);
        public static Index INVOICE_ITEM_INVOICE_ITEM_INDEX_01 = Internal.createIndex("INVOICE_ITEM_INDEX_01", InvoiceItem.INVOICE_ITEM, new OrderField[] { InvoiceItem.INVOICE_ITEM.DESCRIPTION }, false);
        public static Index INVOICE_ITEM_INVOICE_ITEM_INDEX_02 = Internal.createIndex("INVOICE_ITEM_INDEX_02", InvoiceItem.INVOICE_ITEM, new OrderField[] { InvoiceItem.INVOICE_ITEM.ID_INVOICE }, false);
        public static Index INVOICE_ITEM_PRIMARY = Internal.createIndex("PRIMARY", InvoiceItem.INVOICE_ITEM, new OrderField[] { InvoiceItem.INVOICE_ITEM.ID_ITEM }, true);
        public static Index INVOICE_ITEM_TO_CCID_FK_TO_CCID_METADATA = Internal.createIndex("FK_TO_CCID_METADATA", InvoiceItemToCcid.INVOICE_ITEM_TO_CCID, new OrderField[] { InvoiceItemToCcid.INVOICE_ITEM_TO_CCID.ID_CCID_METADATA }, false);
        public static Index INVOICE_ITEM_TO_CCID_FK_TO_INVOICE_ITEM = Internal.createIndex("FK_TO_INVOICE_ITEM", InvoiceItemToCcid.INVOICE_ITEM_TO_CCID, new OrderField[] { InvoiceItemToCcid.INVOICE_ITEM_TO_CCID.ID_INVOICE_ITEM }, false);
        public static Index INVOICE_ITEM_TO_CCID_PRIMARY = Internal.createIndex("PRIMARY", InvoiceItemToCcid.INVOICE_ITEM_TO_CCID, new OrderField[] { InvoiceItemToCcid.INVOICE_ITEM_TO_CCID.ID }, true);
        public static Index INVOICE_LOG_FK_INVOICE_LOG_01 = Internal.createIndex("FK_INVOICE_LOG_01", InvoiceLog.INVOICE_LOG, new OrderField[] { InvoiceLog.INVOICE_LOG.ID_INVOICE }, false);
        public static Index INVOICE_LOG_INVOICE_LOG_UNIQUE_01 = Internal.createIndex("INVOICE_LOG_UNIQUE_01", InvoiceLog.INVOICE_LOG, new OrderField[] { InvoiceLog.INVOICE_LOG.REQUEST_ID }, true);
        public static Index INVOICE_LOG_PRIMARY = Internal.createIndex("PRIMARY", InvoiceLog.INVOICE_LOG, new OrderField[] { InvoiceLog.INVOICE_LOG.ID_INVOICE_LOG }, true);
        public static Index INVOICE_RECLAMO_PRIMARY = Internal.createIndex("PRIMARY", InvoiceReclamo.INVOICE_RECLAMO, new OrderField[] { InvoiceReclamo.INVOICE_RECLAMO.ID_INVOICE_RECLAMO }, true);
        public static Index ITEM_INVOICE_REASON_ITEM_INVOICE_REASON_UNIQUE_01 = Internal.createIndex("ITEM_INVOICE_REASON_UNIQUE_01", ItemInvoiceReason.ITEM_INVOICE_REASON, new OrderField[] { ItemInvoiceReason.ITEM_INVOICE_REASON.CODE }, true);
        public static Index ITEM_INVOICE_REASON_ITEM_INVOICE_REASON_UNIQUE_02 = Internal.createIndex("ITEM_INVOICE_REASON_UNIQUE_02", ItemInvoiceReason.ITEM_INVOICE_REASON, new OrderField[] { ItemInvoiceReason.ITEM_INVOICE_REASON.DESCRIPTION }, true);
        public static Index ITEM_INVOICE_REASON_PRIMARY = Internal.createIndex("PRIMARY", ItemInvoiceReason.ITEM_INVOICE_REASON, new OrderField[] { ItemInvoiceReason.ITEM_INVOICE_REASON.ID_ITEM_INVOICE_REASON }, true);
        public static Index MM_RIPARTIZIONE_648_MM_RIPARTIZIONE_648_ID_SOCIETA_TUTELA_FK = Internal.createIndex("MM_RIPARTIZIONE_648_ID_SOCIETA_TUTELA_fk", MmRipartizione_648.MM_RIPARTIZIONE_648, new OrderField[] { MmRipartizione_648.MM_RIPARTIZIONE_648.ID_SOCIETA_TUTELA }, false);
        public static Index MM_RIPARTIZIONE_648_MM_RIPARTIZIONE_648_ID_UINDEX = Internal.createIndex("MM_RIPARTIZIONE_648_ID_uindex", MmRipartizione_648.MM_RIPARTIZIONE_648, new OrderField[] { MmRipartizione_648.MM_RIPARTIZIONE_648.ID }, true);
        public static Index MM_RIPARTIZIONE_648_PRIMARY = Internal.createIndex("PRIMARY", MmRipartizione_648.MM_RIPARTIZIONE_648, new OrderField[] { MmRipartizione_648.MM_RIPARTIZIONE_648.ID }, true);
        public static Index MM_RIPARTIZIONE_CARICO_MM_RIPARTIZIONE_CARICHI_ID_UINDEX = Internal.createIndex("MM_RIPARTIZIONE_CARICHI_ID_uindex", MmRipartizioneCarico.MM_RIPARTIZIONE_CARICO, new OrderField[] { MmRipartizioneCarico.MM_RIPARTIZIONE_CARICO.ID }, true);
        public static Index MM_RIPARTIZIONE_CARICO_PRIMARY = Internal.createIndex("PRIMARY", MmRipartizioneCarico.MM_RIPARTIZIONE_CARICO, new OrderField[] { MmRipartizioneCarico.MM_RIPARTIZIONE_CARICO.ID }, true);
        public static Index MM_RIPARTIZIONE_CARICO_CCID_MM_RIPARTIZIONE_CARICO_CCID_ID_UINDEX = Internal.createIndex("MM_RIPARTIZIONE_CARICO_CCID_ID_uindex", MmRipartizioneCaricoCcid.MM_RIPARTIZIONE_CARICO_CCID, new OrderField[] { MmRipartizioneCaricoCcid.MM_RIPARTIZIONE_CARICO_CCID.ID }, true);
        public static Index MM_RIPARTIZIONE_CARICO_CCID_MM_RIPARTIZIONE_CARICO_CCID_INVOICE_ITEM_TO_CCID_ID_FK = Internal.createIndex("MM_RIPARTIZIONE_CARICO_CCID_INVOICE_ITEM_TO_CCID_id_fk", MmRipartizioneCaricoCcid.MM_RIPARTIZIONE_CARICO_CCID, new OrderField[] { MmRipartizioneCaricoCcid.MM_RIPARTIZIONE_CARICO_CCID.ID_INVOICE_ITEM_TO_CCID }, false);
        public static Index MM_RIPARTIZIONE_CARICO_CCID_MM_RIPARTIZIONE_CARICO_CCID_MM_RIPARTIZIONE_CARICHI_ID_FK = Internal.createIndex("MM_RIPARTIZIONE_CARICO_CCID_MM_RIPARTIZIONE_CARICHI_ID_fk", MmRipartizioneCaricoCcid.MM_RIPARTIZIONE_CARICO_CCID, new OrderField[] { MmRipartizioneCaricoCcid.MM_RIPARTIZIONE_CARICO_CCID.ID_RIPARTIZIONE_CARICO }, false);
        public static Index MM_RIPARTIZIONE_CARICO_CCID_PRIMARY = Internal.createIndex("PRIMARY", MmRipartizioneCaricoCcid.MM_RIPARTIZIONE_CARICO_CCID, new OrderField[] { MmRipartizioneCaricoCcid.MM_RIPARTIZIONE_CARICO_CCID.ID }, true);
        public static Index UNIDENTIFIED_SONG_DSR_PRIMARY = Internal.createIndex("PRIMARY", UnidentifiedSongDsr.UNIDENTIFIED_SONG_DSR, new OrderField[] { UnidentifiedSongDsr.UNIDENTIFIED_SONG_DSR.ID_UTIL }, true);
        public static Index UNIDENTIFIED_SONG_DSR_UNIDENTIFIED_SONG_DSR_IDX_01 = Internal.createIndex("unidentified_song_dsr_idx_01", UnidentifiedSongDsr.UNIDENTIFIED_SONG_DSR, new OrderField[] { UnidentifiedSongDsr.UNIDENTIFIED_SONG_DSR.HASH_ID }, false);
        public static Index UNIDENTIFIED_SONG_DSR_UNIDENTIFIED_SONG_DSR_IDX_02 = Internal.createIndex("unidentified_song_dsr_idx_02", UnidentifiedSongDsr.UNIDENTIFIED_SONG_DSR, new OrderField[] { UnidentifiedSongDsr.UNIDENTIFIED_SONG_DSR.INSERT_TIME }, false);
    }
}
