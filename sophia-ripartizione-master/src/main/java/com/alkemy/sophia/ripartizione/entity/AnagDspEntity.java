package com.alkemy.sophia.ripartizione.entity;

import javax.persistence.*;

/**
 * Created by ftomei on 28/06/19.
 */
@Entity
@Table(name = "ANAG_DSP", schema = "", catalog = "MCMDB_debug")
public class AnagDspEntity {

	@Id
	@Column(name = "IDDSP", nullable = false, insertable = true, updatable = true, length = 100)
	private String iddsp;

	@Column(name = "CODE", nullable = false, insertable = true, updatable = true, length = 10)
	private String code;

	@Column(name = "NAME", nullable = false, insertable = true, updatable = true, length = 100)
	private String name;

	@Column(name = "DESCRIPTION", nullable = true, insertable = true, updatable = true, length = 200)
	private String description;

	@Column(name = "FTP_SOURCE_PATH", nullable = true, insertable = true, updatable = true, length = 45)
	private String ftpSourcePath;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		AnagDspEntity that = (AnagDspEntity) o;

		if (code != null ? !code.equals(that.code) : that.code != null) return false;
		if (description != null ? !description.equals(that.description) : that.description != null) return false;
		if (ftpSourcePath != null ? !ftpSourcePath.equals(that.ftpSourcePath) : that.ftpSourcePath != null) return false;
		if (iddsp != null ? !iddsp.equals(that.iddsp) : that.iddsp != null) return false;
		if (name != null ? !name.equals(that.name) : that.name != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = iddsp != null ? iddsp.hashCode() : 0;
		result = 31 * result + (code != null ? code.hashCode() : 0);
		result = 31 * result + (name != null ? name.hashCode() : 0);
		result = 31 * result + (description != null ? description.hashCode() : 0);
		result = 31 * result + (ftpSourcePath != null ? ftpSourcePath.hashCode() : 0);
		return result;
	}
}
