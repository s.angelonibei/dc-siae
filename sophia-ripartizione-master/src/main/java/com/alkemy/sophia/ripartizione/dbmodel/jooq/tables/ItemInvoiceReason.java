/*
 * This file is generated by jOOQ.
 */
package com.alkemy.sophia.ripartizione.dbmodel.jooq.tables;


import com.alkemy.sophia.ripartizione.dbmodel.jooq.DefaultSchema;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.Indexes;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.Keys;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.tables.records.ItemInvoiceReasonRecord;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.9"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class ItemInvoiceReason extends TableImpl<ItemInvoiceReasonRecord> {

    private static final long serialVersionUID = 1310308513;

    /**
     * The reference instance of <code>ITEM_INVOICE_REASON</code>
     */
    public static final ItemInvoiceReason ITEM_INVOICE_REASON = new ItemInvoiceReason();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<ItemInvoiceReasonRecord> getRecordType() {
        return ItemInvoiceReasonRecord.class;
    }

    /**
     * The column <code>ITEM_INVOICE_REASON.ID_ITEM_INVOICE_REASON</code>.
     */
    public final TableField<ItemInvoiceReasonRecord, Integer> ID_ITEM_INVOICE_REASON = createField("ID_ITEM_INVOICE_REASON", org.jooq.impl.SQLDataType.INTEGER.nullable(false).identity(true), this, "");

    /**
     * The column <code>ITEM_INVOICE_REASON.CODE</code>.
     */
    public final TableField<ItemInvoiceReasonRecord, String> CODE = createField("CODE", org.jooq.impl.SQLDataType.VARCHAR(45).nullable(false), this, "");

    /**
     * The column <code>ITEM_INVOICE_REASON.DESCRIPTION</code>.
     */
    public final TableField<ItemInvoiceReasonRecord, String> DESCRIPTION = createField("DESCRIPTION", org.jooq.impl.SQLDataType.VARCHAR(255).nullable(false), this, "");

    /**
     * The column <code>ITEM_INVOICE_REASON.CONTO_SAP</code>.
     */
    public final TableField<ItemInvoiceReasonRecord, String> CONTO_SAP = createField("CONTO_SAP", org.jooq.impl.SQLDataType.VARCHAR(45).nullable(false), this, "");

    /**
     * Create a <code>ITEM_INVOICE_REASON</code> table reference
     */
    public ItemInvoiceReason() {
        this(DSL.name("ITEM_INVOICE_REASON"), null);
    }

    /**
     * Create an aliased <code>ITEM_INVOICE_REASON</code> table reference
     */
    public ItemInvoiceReason(String alias) {
        this(DSL.name(alias), ITEM_INVOICE_REASON);
    }

    /**
     * Create an aliased <code>ITEM_INVOICE_REASON</code> table reference
     */
    public ItemInvoiceReason(Name alias) {
        this(alias, ITEM_INVOICE_REASON);
    }

    private ItemInvoiceReason(Name alias, Table<ItemInvoiceReasonRecord> aliased) {
        this(alias, aliased, null);
    }

    private ItemInvoiceReason(Name alias, Table<ItemInvoiceReasonRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> ItemInvoiceReason(Table<O> child, ForeignKey<O, ItemInvoiceReasonRecord> key) {
        super(child, key, ITEM_INVOICE_REASON);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return DefaultSchema.DEFAULT_SCHEMA;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.ITEM_INVOICE_REASON_ITEM_INVOICE_REASON_UNIQUE_01, Indexes.ITEM_INVOICE_REASON_ITEM_INVOICE_REASON_UNIQUE_02, Indexes.ITEM_INVOICE_REASON_PRIMARY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Identity<ItemInvoiceReasonRecord, Integer> getIdentity() {
        return Keys.IDENTITY_ITEM_INVOICE_REASON;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<ItemInvoiceReasonRecord> getPrimaryKey() {
        return Keys.KEY_ITEM_INVOICE_REASON_PRIMARY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<ItemInvoiceReasonRecord>> getKeys() {
        return Arrays.<UniqueKey<ItemInvoiceReasonRecord>>asList(Keys.KEY_ITEM_INVOICE_REASON_PRIMARY, Keys.KEY_ITEM_INVOICE_REASON_ITEM_INVOICE_REASON_UNIQUE_01, Keys.KEY_ITEM_INVOICE_REASON_ITEM_INVOICE_REASON_UNIQUE_02);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ItemInvoiceReason as(String alias) {
        return new ItemInvoiceReason(DSL.name(alias), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ItemInvoiceReason as(Name alias) {
        return new ItemInvoiceReason(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public ItemInvoiceReason rename(String name) {
        return new ItemInvoiceReason(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public ItemInvoiceReason rename(Name name) {
        return new ItemInvoiceReason(name, null);
    }
}
