/*
 * This file is generated by jOOQ.
 */
package com.alkemy.sophia.ripartizione.dbmodel.jooq.tables.records;


import com.alkemy.sophia.ripartizione.dbmodel.jooq.tables.InvoiceReclamo;

import java.sql.Timestamp;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record14;
import org.jooq.Row14;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.9"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class InvoiceReclamoRecord extends UpdatableRecordImpl<InvoiceReclamoRecord> implements Record14<Integer, String, String, String, Timestamp, Timestamp, String, Integer, String, Integer, Integer, String, String, String> {

    private static final long serialVersionUID = 1755670361;

    /**
     * Setter for <code>INVOICE_RECLAMO.ID_INVOICE_RECLAMO</code>.
     */
    public void setIdInvoiceReclamo(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>INVOICE_RECLAMO.ID_INVOICE_RECLAMO</code>.
     */
    public Integer getIdInvoiceReclamo() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>INVOICE_RECLAMO.ID_DSP</code>.
     */
    public void setIdDsp(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>INVOICE_RECLAMO.ID_DSP</code>.
     */
    public String getIdDsp() {
        return (String) get(1);
    }

    /**
     * Setter for <code>INVOICE_RECLAMO.COMMERCIAL_OFFER</code>.
     */
    public void setCommercialOffer(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>INVOICE_RECLAMO.COMMERCIAL_OFFER</code>.
     */
    public String getCommercialOffer() {
        return (String) get(2);
    }

    /**
     * Setter for <code>INVOICE_RECLAMO.COUNTRY</code>.
     */
    public void setCountry(String value) {
        set(3, value);
    }

    /**
     * Getter for <code>INVOICE_RECLAMO.COUNTRY</code>.
     */
    public String getCountry() {
        return (String) get(3);
    }

    /**
     * Setter for <code>INVOICE_RECLAMO.DATA_INSERIMENTO</code>.
     */
    public void setDataInserimento(Timestamp value) {
        set(4, value);
    }

    /**
     * Getter for <code>INVOICE_RECLAMO.DATA_INSERIMENTO</code>.
     */
    public Timestamp getDataInserimento() {
        return (Timestamp) get(4);
    }

    /**
     * Setter for <code>INVOICE_RECLAMO.DATA_ULTIMA_MODIFICA</code>.
     */
    public void setDataUltimaModifica(Timestamp value) {
        set(5, value);
    }

    /**
     * Getter for <code>INVOICE_RECLAMO.DATA_ULTIMA_MODIFICA</code>.
     */
    public Timestamp getDataUltimaModifica() {
        return (Timestamp) get(5);
    }

    /**
     * Setter for <code>INVOICE_RECLAMO.UTENTE</code>.
     */
    public void setUtente(String value) {
        set(6, value);
    }

    /**
     * Getter for <code>INVOICE_RECLAMO.UTENTE</code>.
     */
    public String getUtente() {
        return (String) get(6);
    }

    /**
     * Setter for <code>INVOICE_RECLAMO.PERIOD</code>.
     */
    public void setPeriod(Integer value) {
        set(7, value);
    }

    /**
     * Getter for <code>INVOICE_RECLAMO.PERIOD</code>.
     */
    public Integer getPeriod() {
        return (Integer) get(7);
    }

    /**
     * Setter for <code>INVOICE_RECLAMO.PERIOD_TYPE</code>.
     */
    public void setPeriodType(String value) {
        set(8, value);
    }

    /**
     * Getter for <code>INVOICE_RECLAMO.PERIOD_TYPE</code>.
     */
    public String getPeriodType() {
        return (String) get(8);
    }

    /**
     * Setter for <code>INVOICE_RECLAMO.YEAR</code>.
     */
    public void setYear(Integer value) {
        set(9, value);
    }

    /**
     * Getter for <code>INVOICE_RECLAMO.YEAR</code>.
     */
    public Integer getYear() {
        return (Integer) get(9);
    }

    /**
     * Setter for <code>INVOICE_RECLAMO.STATO</code>.
     */
    public void setStato(Integer value) {
        set(10, value);
    }

    /**
     * Getter for <code>INVOICE_RECLAMO.STATO</code>.
     */
    public Integer getStato() {
        return (Integer) get(10);
    }

    /**
     * Setter for <code>INVOICE_RECLAMO.ID_DSR</code>.
     */
    public void setIdDsr(String value) {
        set(11, value);
    }

    /**
     * Getter for <code>INVOICE_RECLAMO.ID_DSR</code>.
     */
    public String getIdDsr() {
        return (String) get(11);
    }

    /**
     * Setter for <code>INVOICE_RECLAMO.FILE_LOCATION</code>.
     */
    public void setFileLocation(String value) {
        set(12, value);
    }

    /**
     * Getter for <code>INVOICE_RECLAMO.FILE_LOCATION</code>.
     */
    public String getFileLocation() {
        return (String) get(12);
    }

    /**
     * Setter for <code>INVOICE_RECLAMO.FILE_NAME</code>.
     */
    public void setFileName(String value) {
        set(13, value);
    }

    /**
     * Getter for <code>INVOICE_RECLAMO.FILE_NAME</code>.
     */
    public String getFileName() {
        return (String) get(13);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record14 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row14<Integer, String, String, String, Timestamp, Timestamp, String, Integer, String, Integer, Integer, String, String, String> fieldsRow() {
        return (Row14) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row14<Integer, String, String, String, Timestamp, Timestamp, String, Integer, String, Integer, Integer, String, String, String> valuesRow() {
        return (Row14) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field1() {
        return InvoiceReclamo.INVOICE_RECLAMO.ID_INVOICE_RECLAMO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field2() {
        return InvoiceReclamo.INVOICE_RECLAMO.ID_DSP;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field3() {
        return InvoiceReclamo.INVOICE_RECLAMO.COMMERCIAL_OFFER;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field4() {
        return InvoiceReclamo.INVOICE_RECLAMO.COUNTRY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Timestamp> field5() {
        return InvoiceReclamo.INVOICE_RECLAMO.DATA_INSERIMENTO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Timestamp> field6() {
        return InvoiceReclamo.INVOICE_RECLAMO.DATA_ULTIMA_MODIFICA;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field7() {
        return InvoiceReclamo.INVOICE_RECLAMO.UTENTE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field8() {
        return InvoiceReclamo.INVOICE_RECLAMO.PERIOD;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field9() {
        return InvoiceReclamo.INVOICE_RECLAMO.PERIOD_TYPE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field10() {
        return InvoiceReclamo.INVOICE_RECLAMO.YEAR;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field11() {
        return InvoiceReclamo.INVOICE_RECLAMO.STATO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field12() {
        return InvoiceReclamo.INVOICE_RECLAMO.ID_DSR;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field13() {
        return InvoiceReclamo.INVOICE_RECLAMO.FILE_LOCATION;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field14() {
        return InvoiceReclamo.INVOICE_RECLAMO.FILE_NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component1() {
        return getIdInvoiceReclamo();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component2() {
        return getIdDsp();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component3() {
        return getCommercialOffer();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component4() {
        return getCountry();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Timestamp component5() {
        return getDataInserimento();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Timestamp component6() {
        return getDataUltimaModifica();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component7() {
        return getUtente();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component8() {
        return getPeriod();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component9() {
        return getPeriodType();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component10() {
        return getYear();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component11() {
        return getStato();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component12() {
        return getIdDsr();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component13() {
        return getFileLocation();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component14() {
        return getFileName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value1() {
        return getIdInvoiceReclamo();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value2() {
        return getIdDsp();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value3() {
        return getCommercialOffer();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value4() {
        return getCountry();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Timestamp value5() {
        return getDataInserimento();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Timestamp value6() {
        return getDataUltimaModifica();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value7() {
        return getUtente();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value8() {
        return getPeriod();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value9() {
        return getPeriodType();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value10() {
        return getYear();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value11() {
        return getStato();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value12() {
        return getIdDsr();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value13() {
        return getFileLocation();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value14() {
        return getFileName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InvoiceReclamoRecord value1(Integer value) {
        setIdInvoiceReclamo(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InvoiceReclamoRecord value2(String value) {
        setIdDsp(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InvoiceReclamoRecord value3(String value) {
        setCommercialOffer(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InvoiceReclamoRecord value4(String value) {
        setCountry(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InvoiceReclamoRecord value5(Timestamp value) {
        setDataInserimento(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InvoiceReclamoRecord value6(Timestamp value) {
        setDataUltimaModifica(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InvoiceReclamoRecord value7(String value) {
        setUtente(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InvoiceReclamoRecord value8(Integer value) {
        setPeriod(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InvoiceReclamoRecord value9(String value) {
        setPeriodType(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InvoiceReclamoRecord value10(Integer value) {
        setYear(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InvoiceReclamoRecord value11(Integer value) {
        setStato(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InvoiceReclamoRecord value12(String value) {
        setIdDsr(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InvoiceReclamoRecord value13(String value) {
        setFileLocation(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InvoiceReclamoRecord value14(String value) {
        setFileName(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InvoiceReclamoRecord values(Integer value1, String value2, String value3, String value4, Timestamp value5, Timestamp value6, String value7, Integer value8, String value9, Integer value10, Integer value11, String value12, String value13, String value14) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        value7(value7);
        value8(value8);
        value9(value9);
        value10(value10);
        value11(value11);
        value12(value12);
        value13(value13);
        value14(value14);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached InvoiceReclamoRecord
     */
    public InvoiceReclamoRecord() {
        super(InvoiceReclamo.INVOICE_RECLAMO);
    }

    /**
     * Create a detached, initialised InvoiceReclamoRecord
     */
    public InvoiceReclamoRecord(Integer idInvoiceReclamo, String idDsp, String commercialOffer, String country, Timestamp dataInserimento, Timestamp dataUltimaModifica, String utente, Integer period, String periodType, Integer year, Integer stato, String idDsr, String fileLocation, String fileName) {
        super(InvoiceReclamo.INVOICE_RECLAMO);

        set(0, idInvoiceReclamo);
        set(1, idDsp);
        set(2, commercialOffer);
        set(3, country);
        set(4, dataInserimento);
        set(5, dataUltimaModifica);
        set(6, utente);
        set(7, period);
        set(8, periodType);
        set(9, year);
        set(10, stato);
        set(11, idDsr);
        set(12, fileLocation);
        set(13, fileName);
    }
}
