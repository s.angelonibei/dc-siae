package com.alkemy.sophia.ripartizione.aws.sqs;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public interface MessageDeduplicator {

	public boolean deduplicate(String uuid, String queueName);
	
}
