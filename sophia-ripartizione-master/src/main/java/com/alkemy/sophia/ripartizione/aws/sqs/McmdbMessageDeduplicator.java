package com.alkemy.sophia.ripartizione.aws.sqs;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class McmdbMessageDeduplicator implements MessageDeduplicator {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final DataSource dataSource;
	
	public McmdbMessageDeduplicator(@Qualifier("MCMDB") DataSource dataSource) {
		super();
		this.dataSource = dataSource;
	}

	@Override
	public boolean deduplicate(String uuid, String queueName) {
		if (Strings.isNullOrEmpty(uuid))
			return false;
		final String sql = new StringBuilder()
				.append("INSERT INTO SQS_DEDUPLICATION (UUID, QUEUE_NAME) VALUES ('")
				.append(uuid)
				.append("', '")
				.append(queueName)
				.append("')")
				.toString();
		logger.debug("executing sql {}", sql);
		try (final Connection connection = dataSource.getConnection();
				final Statement statement = connection.createStatement()) {
			logger.debug("jdbc connected to {} {}", connection.getMetaData()
					.getDatabaseProductName(), connection.getMetaData().getURL());
			return 1 == statement.executeUpdate(sql);
		} catch (SQLException e) {
			if (1062 == e.getErrorCode()) // duplicate key violation
				return false;
			logger.error("deduplicate", e);
		}
		return false;
	}
	
}
