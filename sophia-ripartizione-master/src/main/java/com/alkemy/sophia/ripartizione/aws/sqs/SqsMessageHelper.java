package com.alkemy.sophia.ripartizione.aws.sqs;

import java.util.Date;
import java.util.UUID;

import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.OsUtils;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.google.gson.JsonObject;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public abstract class SqsMessageHelper {
	
	public static String getMessageUuid(JsonObject message) {
		return GsonUtils.getAsString(GsonUtils
				.getAsJsonObject(message, "header"), "uuid");
	}
	
	public static JsonObject formatContext() {
		final JsonObject context = new JsonObject();
		context.addProperty("emrClusterId", OsUtils.getHostname());
		context.addProperty("applicationId", OsUtils.getProcessId());
		context.addProperty("ipAddress", OsUtils.getIpAddress());
		return context;		
	}

	public static JsonObject formatError(String code, String description) {
		final JsonObject error = new JsonObject();
		error.addProperty("code", code);
		error.addProperty("description", description);
		return error;		
	}
	
	public static JsonObject formatError(Exception e) {
		final JsonObject error = new JsonObject();
		error.addProperty("code", "0");
		error.addProperty("description", e.getMessage());
		error.addProperty("stackTrace", TextUtils.printStackTrace(e));
		return error;
	}

	public static JsonObject formatToProcessJson(String queue, String sender, JsonObject body) {
		return formatToProcessJson(queue, UUID.randomUUID().toString(), sender, body);
	}
	
	public static JsonObject formatToProcessJson(String queue, String uuid, String sender, JsonObject body) {
		final JsonObject header = new JsonObject();
		header.addProperty("uuid", uuid);
		header.addProperty("timestamp", com.alkemytech.sophia.commons.sqs.Iso8601Helper.format(new Date()));
		header.addProperty("queue", queue);
		header.addProperty("sender", sender);
		final JsonObject message = new JsonObject();
		message.add("header", header);
		message.add("body", body);
		return message;
	}

	public static JsonObject formatStartedJson(String queue, String sender, JsonObject input, JsonObject context) {
		final String hostname = OsUtils.getHostname();
		final JsonObject header = new JsonObject();
		header.addProperty("uuid", UUID.randomUUID().toString());
		header.addProperty("timestamp", com.alkemytech.sophia.commons.sqs.Iso8601Helper.format(new Date()));
		header.addProperty("queue", queue);
		header.addProperty("sender", sender);
		header.addProperty("hostname", hostname);
		final JsonObject message = new JsonObject();
		message.add("header", header);
		message.add("input", input);
		message.add("context", context);
		return message;
	}

	public static JsonObject formatCompletedJson(String queue, String sender, JsonObject input, JsonObject output) {
		final JsonObject header = new JsonObject();
		header.addProperty("uuid", UUID.randomUUID().toString());
		header.addProperty("timestamp", com.alkemytech.sophia.commons.sqs.Iso8601Helper.format(new Date()));
		header.addProperty("queue", queue);
		header.addProperty("sender", sender);
		header.addProperty("hostname", OsUtils.getHostname());
		final JsonObject message = new JsonObject();
		message.add("header", header);
		message.add("input", input);
		message.add("output", output);
		return message;
	}
	
	public static JsonObject formatFailedJson(String queue, String sender, JsonObject input, JsonObject error) {
		final JsonObject header = new JsonObject();
		header.addProperty("uuid", UUID.randomUUID().toString());
		header.addProperty("timestamp", com.alkemytech.sophia.commons.sqs.Iso8601Helper.format(new Date()));
		header.addProperty("queue", queue);
		header.addProperty("sender", sender);
		header.addProperty("hostname", OsUtils.getHostname());
		final JsonObject message = new JsonObject();
		message.add("header", header);
		message.add("input", input);
		message.add("error", error);
		return message;
	}

}
