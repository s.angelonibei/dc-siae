package com.alkemy.sophia.ripartizione.model;

import com.google.gson.GsonBuilder;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Created by ftomei on 27/06/19.
 */
@Data
@Accessors( chain = true)
public class DsrInfo {
	private String idDsr;
	private String year;
	private String month;
	private String idDsp;
	private String ftpSourcePath;
	private String dspCode;
	private String country;
	private String idUtilizationType;
	private String offering;

	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}

}
