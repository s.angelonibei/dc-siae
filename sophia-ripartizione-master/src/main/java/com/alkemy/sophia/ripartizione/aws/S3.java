package com.alkemy.sophia.ripartizione.aws;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.auth.PropertiesFileCredentialsProvider;
import com.amazonaws.event.ProgressEvent;
import com.amazonaws.event.ProgressEventType;
import com.amazonaws.event.ProgressListener;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.s3.transfer.Download;
import com.amazonaws.services.s3.transfer.Transfer.TransferState;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import com.amazonaws.services.s3.transfer.Upload;
import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class S3 {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	public static class Url {
		
		public final String bucket;
		public final String key;
		
		public Url(String url) {
			super();
			if (!url.startsWith("s3://")) {
				throw new IllegalArgumentException(String
						.format("invalid url \"%s\"", url));
			}
			this.bucket = url.substring(5, url.indexOf('/', 5));
			this.key = url.substring(1 + url.indexOf('/', 5));
		}

		public Url(String bucket, String key) {
			super();
			this.bucket = bucket;
			this.key = key;
		}
		
		@Override
		public int hashCode() {
			int result = 31 + (null == bucket ? 0 : bucket.hashCode());
			return 31 * result + (null == key ? 0 : key.hashCode());
		}

		@Override
		public boolean equals(Object object) {
			if (this == object)
				return true;
			if (null == object)
				return false;
			if (getClass() != object.getClass())
				return false;
			final Url other = (Url) object;
			if (null == bucket) {
				if (null != other.bucket)
					return false;
			} else if (!bucket.equals(other.bucket))
				return false;
			if (null == key) {
				if (null != other.key)
					return false;
			} else if (!key.equals(other.key))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return String.format("s3://%s/%s", bucket, key);
		}

	}
	
	private final Properties configuration;

	private TransferManager transferManager;
	private AmazonS3 s3Client;

	@Inject
	public S3(@Named("configuration") Properties configuration) {
		super();
		this.configuration = configuration;
	}

	public synchronized S3 startup() {
		if (null == transferManager) {
			final String proxyHost = configuration.getProperty("http.proxy.host");
			final String proxyPort = configuration.getProperty("http.proxy.port", "8080");
			final ClientConfiguration clientConfiguration = new ClientConfiguration();
			if (!Strings.isNullOrEmpty(proxyHost)) {
				clientConfiguration.setProxyHost(proxyHost);
				clientConfiguration.setProxyPort(Integer.parseInt(proxyPort));
			}
			final String region = configuration.getProperty("aws.region", "eu-west-1");
			final String credentialsFilePath = configuration.getProperty("aws.credentials");
			final AWSCredentialsProvider credentials = Strings.isNullOrEmpty(credentialsFilePath)
					? new ClasspathPropertiesFileCredentialsProvider()
					: new PropertiesFileCredentialsProvider(credentialsFilePath);
			s3Client = AmazonS3ClientBuilder.standard()
					.withCredentials(credentials)
					.withClientConfiguration(clientConfiguration)
					.withRegion(region)
					.build();
			transferManager = TransferManagerBuilder.standard()
					.withS3Client(s3Client)
					.build();
		}
		return this;
	}

	public synchronized S3 shutdown() {
		if (null != transferManager) {
			transferManager.shutdownNow(true);
			transferManager = null;
		}
		if (null != s3Client) {
			s3Client.shutdown();
			s3Client = null;
		}
		return this;
	}

	public boolean upload(Url url, final File file) {
		for (int retries = Integer.parseInt(configuration
					.getProperty("s3.retry.put_object", "1"));
				retries > 0; retries--) {
			try {
				logger.debug("uploading file {} to {}", file.getName(), url);
				final PutObjectRequest putObjectRequest = new PutObjectRequest(url.bucket,
						url.key.replace("//", "/"), file);
				putObjectRequest.setGeneralProgressListener(new ProgressListener() {

					private long accumulator = 0L;
					private long total = 0L;
					private long chunk = Math.max(256 * 1024, file.length() / 10L);
					
					@Override
					public void progressChanged(ProgressEvent event) {
						if (ProgressEventType.REQUEST_BYTE_TRANSFER_EVENT
								.equals(event.getEventType())) {
							accumulator += event.getBytes();
							total += event.getBytes();
							if (accumulator >= chunk) {
								accumulator -= chunk;
								logger.debug("transferred {}Kb/{}Kb of file {}",
										total / 1024L, file.length() / 1024L, file.getName());
							}
						} else if (ProgressEventType.TRANSFER_COMPLETED_EVENT
								.equals(event.getEventType())) {
							logger.debug("transfer of file {} completed", file.getName());
						}
					}
					
				});
				final Upload upload = transferManager.upload(putObjectRequest);
				upload.waitForCompletion();
				logger.debug("file {} successfully uploaded to {}", file.getName(), url);
				return true;
			} catch (Exception e) {
				logger.error("upload", e);
			}
		}
		logger.debug("upload: upload failed {}", file.getName());
		return false;
	}
	
	public boolean copy(Url fromUrl, Url toUrl) {
		try {
			final CopyObjectRequest request = new CopyObjectRequest(fromUrl.bucket,
					fromUrl.key, toUrl.bucket, toUrl.key);
			return null != s3Client.copyObject(request);
		} catch (Exception e) {
			logger.error("copy", e);
			return false;
		}
	}
	
	public boolean delete(Url url) {
		for (int retries = Integer.parseInt(configuration
					.getProperty("s3.retry.delete_object", "1"));
				retries > 0; retries--) {
			try {
				s3Client.deleteObject(url.bucket, url.key);
				return true;
			} catch (Exception e) {
				logger.error("delete", e);
			}
		}
		return false;
	}

	public boolean download(Url url, File file) {
		for (int retries = Integer.parseInt(configuration
					.getProperty("s3.retry.download_to_file", "1"));
				retries > 0; retries--) {
			try {
				file.delete();
				final Download download = transferManager
						.download(url.bucket, url.key, file);
				download.waitForCompletion();
				if (TransferState.Completed == download.getState()
						&& file.exists()) {
					return true;
				}
			} catch (Exception e) {
				logger.error("download", e);
			}
		}
		return false;
	}

	public boolean download(Url url, OutputStream out) {
		try {
			final S3Object object = s3Client
					.getObject(new GetObjectRequest(url.bucket, url.key));
			try (final InputStream in = object.getObjectContent()) {
				final byte[] bytes = new byte[1024];
				for (int count = 0; -1 != (count = in.read(bytes)); ) {
					out.write(bytes, 0, count);
				}
			}
			return true;
		} catch (Exception e) {
			logger.error("download", e);
		}
		return false;
	}
	
	public List<S3ObjectSummary> listObjects(Url url) {
		for (int retries = Integer.parseInt(configuration
					.getProperty("s3.retry.list_objects", "1"));
				retries > 0; retries--) {
			try {
				List<S3ObjectSummary> objects = null;
				final ListObjectsV2Request request = new ListObjectsV2Request()
						.withBucketName(url.bucket).withPrefix(url.key);
				ListObjectsV2Result result;
				do {
					result = s3Client.listObjectsV2(request);
					final List<S3ObjectSummary> summaries = result.getObjectSummaries();
					if (null != summaries) {
						if (null == objects) {
							objects = new ArrayList<S3ObjectSummary>();
						}
						objects.addAll(summaries);
					}
					request.setContinuationToken(result.getNextContinuationToken());
				} while (result.isTruncated());
				return objects;
			} catch (Exception e) {
				logger.error("listObjects", e);
			}
		}
		return null;
	}

	public S3Object getObject(Url url) {
		for (int retries = Integer.parseInt(configuration
					.getProperty("s3.retry.get_object", "1"));
				retries > 0; retries--) {
			try {
				return s3Client.getObject(new GetObjectRequest(url.bucket, url.key));
			} catch (Exception e) {
				logger.error("getObject", e);
			}
		}
		return null;
	}

	public boolean doesObjectExist(Url url) {
		try {
			return s3Client.doesObjectExist(url.bucket, url.key);
		} catch (Exception e) {
			logger.error("doesObjectExist", e);
			return false;
		}
	}

	public String getObjectContents(Url url, Charset charset) {
		if (doesObjectExist(url)) {
			final ByteArrayOutputStream out = new ByteArrayOutputStream();
			try (final S3Object s3object = getObject(url);
					final InputStream in = s3object.getObjectContent()) {
				final byte[] bytes = new byte[256];
				for (int length = 0; -1 != length; ) {
					length = in.read(bytes);
					if (length > 0)
						out.write(bytes, 0, length);
				}
			} catch (IOException e) {
				logger.error("getObjectContents", e);
			}
			return new String(out.toByteArray(), charset);
		}
		return null;
	}
	
}
