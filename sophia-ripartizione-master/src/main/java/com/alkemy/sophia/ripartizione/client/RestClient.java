package com.alkemy.sophia.ripartizione.client;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class RestClient {

	public static class Result {
		
		private int statusCode;
		private JsonElement jsonElement;
		
		public int getStatusCode() {
			return statusCode;
		}
		public Result setStatusCode(int statusCode) {
			this.statusCode = statusCode;
			return this;
		}
		public JsonElement getJsonElement() {
			return jsonElement;
		}
		public Result setJsonElement(JsonElement jsonElement) {
			this.jsonElement = jsonElement;
			return this;
		}
		
		@Override
		public String toString() {
			return new GsonBuilder()
				.setPrettyPrinting().create().toJson(this);
		}
	}
	
	private static Result fromResponse(CloseableHttpResponse response) throws IOException {
		Result result = new Result()
			.setStatusCode(response.getStatusLine().getStatusCode());
	    HttpEntity responseEntity = response.getEntity();
	    if (null != responseEntity) {
		    try (InputStream in = responseEntity.getContent()) {
		    	byte[] buffer = new byte[1024];
		    	ByteArrayOutputStream out = new ByteArrayOutputStream();
		    	for (int count; -1 != (count = in.read(buffer)); ) {
		    		out.write(buffer, 0, count);
		    	}
		    	result.setJsonElement(new GsonBuilder().create()
			    		.fromJson(out.toString("UTF-8"), JsonElement.class));
		    }					
	    }
	    return result;
	}
	
	public static Result get(String url) throws IOException {
		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
			HttpGet httpGet = new HttpGet(url);
			try (CloseableHttpResponse response = httpClient.execute(httpGet)) {
			    return fromResponse(response);
			}
		}
	}
	
	public static Result post(String url, JsonObject requestJson) throws IOException {
		return post(url, requestJson.toString());
	}
	
	public static Result post(String url, String requestJson) throws IOException {
		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
			HttpPost httpPost = new HttpPost(url);
			StringEntity requestEntity = new StringEntity(requestJson.toString(), 
					   ContentType.create("application/json", "UTF-8"));
			httpPost.setEntity(requestEntity);
			try (CloseableHttpResponse response = httpClient.execute(httpPost)) {
			    return fromResponse(response);
			}
		}
	}
	
	public static Result put(String url, JsonObject requestJson) throws IOException {
		return put(url, requestJson.toString());
	}
	
	public static Result put(String url, String requestJson) throws IOException {
		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
			HttpPut httpPut = new HttpPut(url);
			StringEntity requestEntity = new StringEntity(requestJson.toString(), 
					   ContentType.create("application/json", "UTF-8"));
			httpPut.setEntity(requestEntity);
			try (CloseableHttpResponse response = httpClient.execute(httpPut)) {
			    return fromResponse(response);
			}
		}
	}
	
//	public static void main(String[] args) throws Exception {
//		
//		System.out.println(post("http://localhost:8080/cruscotto/rest/kb/upsert",
//				"{\"xxx\":\"xxx\"}"));
//		
//	}
	
}
