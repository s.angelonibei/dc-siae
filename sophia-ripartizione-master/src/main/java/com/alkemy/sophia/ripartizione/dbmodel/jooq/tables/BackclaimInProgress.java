/*
 * This file is generated by jOOQ.
 */
package com.alkemy.sophia.ripartizione.dbmodel.jooq.tables;


import com.alkemy.sophia.ripartizione.dbmodel.jooq.DefaultSchema;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.Indexes;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.Keys;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.tables.records.BackclaimInProgressRecord;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.9"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class BackclaimInProgress extends TableImpl<BackclaimInProgressRecord> {

    private static final long serialVersionUID = -771798061;

    /**
     * The reference instance of <code>BACKCLAIM_IN_PROGRESS</code>
     */
    public static final BackclaimInProgress BACKCLAIM_IN_PROGRESS = new BackclaimInProgress();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<BackclaimInProgressRecord> getRecordType() {
        return BackclaimInProgressRecord.class;
    }

    /**
     * The column <code>BACKCLAIM_IN_PROGRESS.ID_BACKCLAIM_IN_PROGRESS</code>.
     */
    public final TableField<BackclaimInProgressRecord, Short> ID_BACKCLAIM_IN_PROGRESS = createField("ID_BACKCLAIM_IN_PROGRESS", org.jooq.impl.SQLDataType.SMALLINT.nullable(false).identity(true), this, "");

    /**
     * The column <code>BACKCLAIM_IN_PROGRESS.DESCRIPTION</code>.
     */
    public final TableField<BackclaimInProgressRecord, String> DESCRIPTION = createField("DESCRIPTION", org.jooq.impl.SQLDataType.VARCHAR(100).nullable(false), this, "");

    /**
     * Create a <code>BACKCLAIM_IN_PROGRESS</code> table reference
     */
    public BackclaimInProgress() {
        this(DSL.name("BACKCLAIM_IN_PROGRESS"), null);
    }

    /**
     * Create an aliased <code>BACKCLAIM_IN_PROGRESS</code> table reference
     */
    public BackclaimInProgress(String alias) {
        this(DSL.name(alias), BACKCLAIM_IN_PROGRESS);
    }

    /**
     * Create an aliased <code>BACKCLAIM_IN_PROGRESS</code> table reference
     */
    public BackclaimInProgress(Name alias) {
        this(alias, BACKCLAIM_IN_PROGRESS);
    }

    private BackclaimInProgress(Name alias, Table<BackclaimInProgressRecord> aliased) {
        this(alias, aliased, null);
    }

    private BackclaimInProgress(Name alias, Table<BackclaimInProgressRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> BackclaimInProgress(Table<O> child, ForeignKey<O, BackclaimInProgressRecord> key) {
        super(child, key, BACKCLAIM_IN_PROGRESS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return DefaultSchema.DEFAULT_SCHEMA;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.BACKCLAIM_IN_PROGRESS_PRIMARY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Identity<BackclaimInProgressRecord, Short> getIdentity() {
        return Keys.IDENTITY_BACKCLAIM_IN_PROGRESS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<BackclaimInProgressRecord> getPrimaryKey() {
        return Keys.KEY_BACKCLAIM_IN_PROGRESS_PRIMARY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<BackclaimInProgressRecord>> getKeys() {
        return Arrays.<UniqueKey<BackclaimInProgressRecord>>asList(Keys.KEY_BACKCLAIM_IN_PROGRESS_PRIMARY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BackclaimInProgress as(String alias) {
        return new BackclaimInProgress(DSL.name(alias), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BackclaimInProgress as(Name alias) {
        return new BackclaimInProgress(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public BackclaimInProgress rename(String name) {
        return new BackclaimInProgress(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public BackclaimInProgress rename(Name name) {
        return new BackclaimInProgress(name, null);
    }
}
