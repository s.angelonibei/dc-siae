package com.alkemy.sophia.ripartizione.model;

import java.math.BigDecimal;
import lombok.Data;
import com.google.gson.GsonBuilder;

@Data
public class DsrAmount {

	private String idDsr;
	private BigDecimal amountDem;
	private BigDecimal amountDrm;
	private String billDate;
	private String invoiceCode;
	private String outputPath;

	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}

	public String getInvoiceCode() {
		return invoiceCode;
	}
}
