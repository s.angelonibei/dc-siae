package com.alkemy.sophia.ripartizione.aws;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.amazonaws.auth.BasicAWSCredentials;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.auth.PropertiesFileCredentialsProvider;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.DeleteMessageResult;
import com.amazonaws.services.sqs.model.GetQueueUrlRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.QueueDeletedRecentlyException;
import com.amazonaws.services.sqs.model.QueueDoesNotExistException;
import com.amazonaws.services.sqs.model.QueueNameExistsException;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
@Component
public class SQS {

	@Value("${http.proxy.host:}")
	private String httpProxyHost;

	@Value("${http.proxy.port:}")
	private String httpProxyPort;

	@Value("${sqs.endpoint}")
	private String sqsEndpoint;

	@Value("${sqs.visibility_timeout:300}")
	private String sqsVisibilityTimeout;

	@Value("${sqs.max_number_of_messages:1}")
	private String sqsMaxMessages;

	@Value("${cloud.aws.region.static:eu-west-1}")
	private String awsRegion;

	@Value("${cloud.aws.credentials.accessKey}")
	private String awsCredentialsAccessKey;

	@Value("${cloud.aws.credentials.secretKey}")
	private String awsCredentialsSecretKey;

	private final Logger logger = LoggerFactory.getLogger(getClass());

	public static class Msg {
		
		public final String text;
		public final String receiptHandle;
		
		public Msg(String text, String receiptHandle) {
			super();
			this.text = text;
			this.receiptHandle = receiptHandle;
		}
		
		@Override
		public int hashCode() {
			int result = 31 + (null == receiptHandle ? 0 : receiptHandle.hashCode());
			return 31 * result + (null == text ? 0 : text.hashCode());
		}

		@Override
		public boolean equals(Object object) {
			if (this == object)
				return true;
			if (null == object)
				return false;
			if (getClass() != object.getClass())
				return false;
			final Msg other = (Msg) object;
			if (null == receiptHandle) {
				if (null != other.receiptHandle)
					return false;
			} else if (!receiptHandle.equals(other.receiptHandle))
				return false;
			if (null == text) {
				if (null != other.text)
					return false;
			} else if (!text.equals(other.text))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return text;
		}
	
	}

	private AmazonSQS sqsClient;
	
	public synchronized SQS startup() {
		if (null == sqsClient) {
			final String proxyHost = httpProxyHost;
			final String proxyPort = httpProxyPort;
		    final ClientConfiguration clientConfiguration = new ClientConfiguration();
		    if (!Strings.isNullOrEmpty(proxyHost)) {
		    	clientConfiguration.setProxyHost(proxyHost);
		    	clientConfiguration.setProxyPort(Integer.parseInt(proxyPort));
		    }
			final String region = null != awsRegion ? awsRegion : "eu-west-1";
			final String endpoint = null != sqsEndpoint ? sqsEndpoint : "https://sqs.eu-west-1.amazonaws.com";
		    final AWSCredentialsProvider credentials = ( Strings.isNullOrEmpty(awsCredentialsAccessKey) || Strings.isNullOrEmpty(awsCredentialsSecretKey) )
			    ? new ClasspathPropertiesFileCredentialsProvider()
					: (AWSCredentialsProvider) new BasicAWSCredentials(awsCredentialsAccessKey, awsCredentialsSecretKey);
			sqsClient = AmazonSQSClientBuilder.standard()
				.withCredentials(credentials)
				.withClientConfiguration(clientConfiguration)
				.withEndpointConfiguration(new EndpointConfiguration(endpoint, region))
				.build();
		}
		return this;
	}
		
	public synchronized SQS shutdown() {
		if (null != sqsClient) {
			sqsClient.shutdown();
			sqsClient = null;
		}
		return this;
	}

	public List<String> getQueueUrls() {
		return sqsClient.listQueues()
				.getQueueUrls();
	}

	public String getUrl(String queueName) {
		
		// get existing queue url
		logger.debug("opening queue {}", queueName);
		try {
			return sqsClient
					.getQueueUrl(new GetQueueUrlRequest(queueName))
					.getQueueUrl();
		} catch (QueueDoesNotExistException e) {
			logger.error("getUrl", e);
		}
		
		// error
		return null;
		
	}
	
	public String getOrCreateUrl(String queueName) {
		
		// get existing queue url
		logger.debug("opening queue {}", queueName);
		try {
			return sqsClient
					.getQueueUrl(new GetQueueUrlRequest(queueName))
					.getQueueUrl();
		} catch (QueueDoesNotExistException e) {
			logger.error("getOrCreateUrl", e);
		}
		
		// create new queue
		logger.debug("getOrCreateUrl: creating queue {}", queueName);
		try {
			final Map<String, String> attributes = new HashMap<String, String>();
			attributes.put("VisibilityTimeout", sqsVisibilityTimeout); // default to 5 minutes
			return sqsClient.createQueue(new CreateQueueRequest()
				.withQueueName(queueName)
				.withAttributes(attributes))
					.getQueueUrl();
		} catch (QueueNameExistsException | QueueDeletedRecentlyException  e) {
			logger.error("getOrCreateUrl", e);
		}
		
		// error
		return null;
	}
	
	public boolean sendMessage(String queueUrl, String text) {
        final SendMessageRequest request = new SendMessageRequest()
        	.withMessageBody(text)
        	.withQueueUrl(queueUrl);
		final SendMessageResult result = sqsClient.sendMessage(request);
		return null != result && null != result.getSdkHttpMetadata() && 
				200 == result.getSdkHttpMetadata().getHttpStatusCode();
	}

	public List<Msg> receiveMessages(String queueUrl) {
		final Integer maxNumberOfMessages = Integer.valueOf(sqsMaxMessages);
		return receiveMessages(queueUrl, maxNumberOfMessages);
	}

	public List<Msg> receiveMessages(String queueUrl, Integer maxNumberOfMessages) {
        final ReceiveMessageRequest request = new ReceiveMessageRequest()
        	.withQueueUrl(queueUrl)
        	.withMaxNumberOfMessages(maxNumberOfMessages);
        final List<Msg> result = new ArrayList<Msg>();
        final List<Message> messages = sqsClient
        		.receiveMessage(request).getMessages();        
        if (null != messages) {
        	for (Message message : messages)
	        	result.add(new Msg(message.getBody(),
	        			message.getReceiptHandle()));
        }
        return result;
	}
	
	public boolean deleteMessage(String queueUrl, String receiptHandle) {
		final DeleteMessageRequest request = new DeleteMessageRequest()
			.withQueueUrl(queueUrl)
			.withReceiptHandle(receiptHandle);
        final DeleteMessageResult result = sqsClient.deleteMessage(request);
		return null != result && null != result.getSdkHttpMetadata() && 
				200 == result.getSdkHttpMetadata().getHttpStatusCode();
	}
	
}
