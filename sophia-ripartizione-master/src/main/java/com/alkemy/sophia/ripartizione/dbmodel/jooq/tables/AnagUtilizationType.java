/*
 * This file is generated by jOOQ.
 */
package com.alkemy.sophia.ripartizione.dbmodel.jooq.tables;


import com.alkemy.sophia.ripartizione.dbmodel.jooq.DefaultSchema;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.Indexes;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.Keys;
import com.alkemy.sophia.ripartizione.dbmodel.jooq.tables.records.AnagUtilizationTypeRecord;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.9"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class AnagUtilizationType extends TableImpl<AnagUtilizationTypeRecord> {

    private static final long serialVersionUID = 617284646;

    /**
     * The reference instance of <code>ANAG_UTILIZATION_TYPE</code>
     */
    public static final AnagUtilizationType ANAG_UTILIZATION_TYPE = new AnagUtilizationType();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<AnagUtilizationTypeRecord> getRecordType() {
        return AnagUtilizationTypeRecord.class;
    }

    /**
     * The column <code>ANAG_UTILIZATION_TYPE.ID_UTILIZATION_TYPE</code>.
     */
    public final TableField<AnagUtilizationTypeRecord, String> ID_UTILIZATION_TYPE = createField("ID_UTILIZATION_TYPE", org.jooq.impl.SQLDataType.VARCHAR(20).nullable(false), this, "");

    /**
     * The column <code>ANAG_UTILIZATION_TYPE.NAME</code>.
     */
    public final TableField<AnagUtilizationTypeRecord, String> NAME = createField("NAME", org.jooq.impl.SQLDataType.VARCHAR(45).nullable(false), this, "");

    /**
     * The column <code>ANAG_UTILIZATION_TYPE.DESCRIPTION</code>.
     */
    public final TableField<AnagUtilizationTypeRecord, String> DESCRIPTION = createField("DESCRIPTION", org.jooq.impl.SQLDataType.VARCHAR(100).nullable(false), this, "");

    /**
     * Create a <code>ANAG_UTILIZATION_TYPE</code> table reference
     */
    public AnagUtilizationType() {
        this(DSL.name("ANAG_UTILIZATION_TYPE"), null);
    }

    /**
     * Create an aliased <code>ANAG_UTILIZATION_TYPE</code> table reference
     */
    public AnagUtilizationType(String alias) {
        this(DSL.name(alias), ANAG_UTILIZATION_TYPE);
    }

    /**
     * Create an aliased <code>ANAG_UTILIZATION_TYPE</code> table reference
     */
    public AnagUtilizationType(Name alias) {
        this(alias, ANAG_UTILIZATION_TYPE);
    }

    private AnagUtilizationType(Name alias, Table<AnagUtilizationTypeRecord> aliased) {
        this(alias, aliased, null);
    }

    private AnagUtilizationType(Name alias, Table<AnagUtilizationTypeRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> AnagUtilizationType(Table<O> child, ForeignKey<O, AnagUtilizationTypeRecord> key) {
        super(child, key, ANAG_UTILIZATION_TYPE);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return DefaultSchema.DEFAULT_SCHEMA;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.ANAG_UTILIZATION_TYPE_PRIMARY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<AnagUtilizationTypeRecord> getPrimaryKey() {
        return Keys.KEY_ANAG_UTILIZATION_TYPE_PRIMARY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<AnagUtilizationTypeRecord>> getKeys() {
        return Arrays.<UniqueKey<AnagUtilizationTypeRecord>>asList(Keys.KEY_ANAG_UTILIZATION_TYPE_PRIMARY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AnagUtilizationType as(String alias) {
        return new AnagUtilizationType(DSL.name(alias), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AnagUtilizationType as(Name alias) {
        return new AnagUtilizationType(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public AnagUtilizationType rename(String name) {
        return new AnagUtilizationType(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public AnagUtilizationType rename(Name name) {
        return new AnagUtilizationType(name, null);
    }
}
