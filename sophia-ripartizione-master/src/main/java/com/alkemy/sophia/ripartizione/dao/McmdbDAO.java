package com.alkemy.sophia.ripartizione.dao;

import com.alkemy.sophia.ripartizione.dto.DsrInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ftomei on 09/07/19.
 */
@Service
public class McmdbDAO {

	@Autowired
	private EntityManager em;

	public String obtainOriginalDspCodeByIdDsrReclamo(String idDSR) {
		try {
			//query per hack youtube(trimestrali). scusate
			return em.createNativeQuery("SELECT DSP.CODE " +
					"FROM INVOICE_RECLAMO IR JOIN ANAG_DSP DSP " +
						" ON replace(replace(DSP.IDDSP,'(',''),')','') = IR.ID_DSP WHERE IR.ID_DSR = ?1 ")
					.setParameter(1, idDSR)
					.getSingleResult().toString();
		}catch(Exception e){
			return null;
		}

	}

	public List getCcidByCaricoRipartizione(Long idCaricoRipartizione) {
		List<DsrInfo> list = new ArrayList<>();

 		em.createNativeQuery("select DSR_METADATA.IDDSP,\n" +
				"       PERIOD_TYPE,\n" +
				"       PERIOD,\n" +
				"       DSR_METADATA.YEAR,\n" +
				"       DSR_METADATA.COUNTRY,\n" +
				"       ID_UTIILIZATION_TYPE,\n" +
				"       OFFERING,\n" +
				"       substring_index(S3_PATH, '/', -1 ),\n" +
				"       VALORE_FATTURABILE,\n" +
				"       RIGHT_SPLIT.VALID_FROM,\n" +
				"       RIGHT_SPLIT.DEM,\n" +
				"       RIGHT_SPLIT.DRM,\n" +
				"       sum(RIGHT_SPLIT.DEM * VALORE) as valore_dem,\n" +
				"       sum(RIGHT_SPLIT.DRM * VALORE) as valore_drm,\n" +
				"       CASE WHEN (DSR_METADATA.YEAR >= 2016)\n" +
				"                then sum(RIGHT_SPLIT.DEM * VALORE * (SELECT\n" +
				"                                                         GROUP_CONCAT(IF (s.SETTING_KEY = 'threshold.aggioDRM',s.value,NULL)) AS 'aggioDRM'\n" +
				"\n" +
				"                                                     FROM PERF_CONFIGURATION c\n" +
				"                                                              JOIN PERF_CONFIGURATION_SETTINGS ps ON c.CONFIGURATION_ID = ps.CONFIGURATION_ID\n" +
				"                                                              JOIN PERF_SETTING s ON ps.SETTINGS_ID = s.SETTING_ID\n" +
				"                                                     WHERE  c.domain = 'multimediale.aggi'\n" +
				"                                                       AND   c.active = 1\n" +
				"                                                       AND c.CONF_KEY='SIAE'\n" +
				"                                                       AND now()  >= c.VALID_FROM AND (VALID_TO  IS NULL OR now() < VALID_TO)\n" +
				"                                                     GROUP BY CONF_KEY ORDER BY VALID_FROM DESC LIMIT 1 ) * (\n" +
				"                             SELECT\n" +
				"                                 GROUP_CONCAT(IF (s.SETTING_KEY = 'threshold.aggioDEM',s.value,NULL)) AS 'aggioDEM'\n" +
				"                             FROM PERF_CONFIGURATION c\n" +
				"                                      JOIN PERF_CONFIGURATION_SETTINGS ps ON c.CONFIGURATION_ID = ps.CONFIGURATION_ID\n" +
				"                                      JOIN PERF_SETTING s ON ps.SETTINGS_ID = s.SETTING_ID\n" +
				"                             WHERE  c.domain = 'multimediale.aggi'\n" +
				"                               AND   c.active = 1\n" +
				"                               AND c.CONF_KEY='SIAE'\n" +
				"                               AND now()  >= c.VALID_FROM AND (VALID_TO  IS NULL OR now() < VALID_TO)\n" +
				"                             GROUP BY CONF_KEY ORDER BY VALID_FROM DESC LIMIT 1\n" +
				"                         ) )\n" +
				"            else sum(RIGHT_SPLIT.DEM * VALORE * (\n" +
				"                SELECT\n" +
				"                    GROUP_CONCAT(IF (s.SETTING_KEY = 'threshold.aggioDRM',s.value,NULL)) AS 'aggioDRM'\n" +
				"                FROM PERF_CONFIGURATION c\n" +
				"                         JOIN PERF_CONFIGURATION_SETTINGS ps ON c.CONFIGURATION_ID = ps.CONFIGURATION_ID\n" +
				"                         JOIN PERF_SETTING s ON ps.SETTINGS_ID = s.SETTING_ID\n" +
				"                WHERE  c.domain = 'multimediale.aggi'\n" +
				"                  AND   c.active = 1\n" +
				"                  AND c.CONF_KEY='SIAE'\n" +
				"                  AND now()  >= c.VALID_FROM AND (VALID_TO  IS NULL OR now() < VALID_TO)\n" +
				"                GROUP BY CONF_KEY ORDER BY VALID_FROM DESC LIMIT 1\n" +
				"            ) )\n" +
				"           end as valore_dem_netto,\n" +
				"       sum(RIGHT_SPLIT.DRM * VALORE * (\n" +
				"           SELECT\n" +
				"               GROUP_CONCAT(IF (s.SETTING_KEY = 'threshold.aggioDRM',s.value,NULL)) AS 'aggioDRM'\n" +
				"           FROM PERF_CONFIGURATION c\n" +
				"                    JOIN PERF_CONFIGURATION_SETTINGS ps ON c.CONFIGURATION_ID = ps.CONFIGURATION_ID\n" +
				"                    JOIN PERF_SETTING s ON ps.SETTINGS_ID = s.SETTING_ID\n" +
				"           WHERE  c.domain = 'multimediale.aggi'\n" +
				"             AND   c.active = 1\n" +
				"             AND c.CONF_KEY='SIAE'\n" +
				"             AND now()  >= c.VALID_FROM AND (VALID_TO  IS NULL OR now() < VALID_TO)\n" +
				"           GROUP BY CONF_KEY ORDER BY VALID_FROM DESC LIMIT 1\n" +
				"       )) as valore_drm_netto,\n" +
				"       'FATTURATO',\n" +
				"       I.INVOICE_CODE,\n" +
				"       I.TOTAL_INVOICE\n" +
				"from MM_RIPARTIZIONE_CARICO RC\n" +
				"         join MM_RIPARTIZIONE_CARICO_CCID RCC ON RC.ID = RCC.ID_RIPARTIZIONE_CARICO\n" +
				"         join INVOICE_ITEM_TO_CCID IITC on RCC.ID_INVOICE_ITEM_TO_CCID = IITC.id\n" +
				"         join CCID_METADATA CM on IITC.ID_CCID_METADATA = CM.ID_CCID_METADATA\n" +
				"         join INVOICE_ITEM II on IITC.ID_INVOICE_ITEM = II.ID_ITEM\n" +
				"         join INVOICE I on II.ID_INVOICE = I.ID_INVOICE\n" +
				"         join DSR_METADATA  on CM.ID_DSR = DSR_METADATA.IDDSR\n" +
				"         join steps_monitoring SM on SM.IDDSR = DSR_METADATA.IDDSR\n" +
				"         join COMMERCIAL_OFFERS on DSR_METADATA.SERVICE_CODE = COMMERCIAL_OFFERS.ID_COMMERCIAL_OFFERS\n" +
				"         join CCID_S3_PATH on DSR_METADATA.IDDSR = CCID_S3_PATH.IDDSR,\n" +
				"     RIGHT_SPLIT\n" +
				"where\n" +
				"        RC.ID = ?1 AND SM.NEW_CLAIM = 'SI' AND\n" +
				"    RIGHT_SPLIT.ID =\n" +
				"    (select max(ID) as ID from RIGHT_SPLIT where (RIGHT_SPLIT.COUNTRY = DSR_METADATA.COUNTRY and VALID_FROM <\n" +
				"    CASE when (PERIOD_TYPE = 'quarter')\n" +
				"    then STR_TO_DATE(concat(1,'-',1+(3* PERIOD -3),'-',DSR_METADATA.YEAR),'%d-%m-%Y')\n" +
				"    else STR_TO_DATE(concat(1,'-',PERIOD,'-',DSR_METADATA.YEAR),'%d-%m-%Y')\n" +
				"    end and COMMERCIAL_OFFERS.ID_UTIILIZATION_TYPE= RIGHT_SPLIT.ID_UTILIZATION_TYPE))\n" +
				"group by DSR_METADATA.IDDSP,\n" +
				"    PERIOD_TYPE,\n" +
				"    PERIOD,\n" +
				"         DSR_METADATA.YEAR,\n" +
				"    DSR_METADATA.COUNTRY,\n" +
				"    ID_UTIILIZATION_TYPE,\n" +
				"    OFFERING,\n" +
				"    substring_index(S3_PATH, '/', -1 ),\n" +
				"    VALORE_FATTURABILE,\n" +
				"    RIGHT_SPLIT.VALID_FROM,\n" +
				"    RIGHT_SPLIT.DEM,\n" +
				"    RIGHT_SPLIT.DRM,\n" +
				"    I.INVOICE_CODE,\n" +
				"    I.TOTAL_INVOICE").setParameter(1, idCaricoRipartizione).getResultList().stream().forEach( objArray -> {
		  list.add(
			  new DsrInfo()
				  .setIdDsp((String) ((Object[]) objArray)[0])
				  .setCode((String) ((Object[]) objArray)[17])
				  .setFtpSourcePath((String) ((Object[]) objArray)[7])
			  .setIdUtiilizationType((String) ((Object[]) objArray)[5])
			  .setPeriod((Integer) ((Object[]) objArray)[2])
			  .setPeriodType((String) ((Object[]) objArray)[1])
			  .setYear((Integer) ((Object[]) objArray)[3])
		  );

	  });

	return list;

	}

}
