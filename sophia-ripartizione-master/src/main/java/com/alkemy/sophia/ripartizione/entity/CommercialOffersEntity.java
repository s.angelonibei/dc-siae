package com.alkemy.sophia.ripartizione.entity;

import javax.persistence.*;

/**
 * Created by ftomei on 28/06/19.
 */
@Entity
@Table(name = "COMMERCIAL_OFFERS", schema = "", catalog = "MCMDB_debug")
public class CommercialOffersEntity {
	private int idCommercialOffers;
	private String iddsp;
	private String idUtiilizationType;
	private String offering;
	private String tradingBrand;

	@Id
	@Column(name = "ID_COMMERCIAL_OFFERS", nullable = false, insertable = true, updatable = true)
	public int getIdCommercialOffers() {
		return idCommercialOffers;
	}

	public void setIdCommercialOffers(int idCommercialOffers) {
		this.idCommercialOffers = idCommercialOffers;
	}

	@Column(name = "IDDSP", nullable = false, insertable = true, updatable = true, length = 100)
	public String getIddsp() {
		return iddsp;
	}

	public void setIddsp(String iddsp) {
		this.iddsp = iddsp;
	}

	@Column(name = "ID_UTIILIZATION_TYPE", nullable = false, insertable = true, updatable = true, length = 20)
	public String getIdUtiilizationType() {
		return idUtiilizationType;
	}

	public void setIdUtiilizationType(String idUtiilizationType) {
		this.idUtiilizationType = idUtiilizationType;
	}

	@Column(name = "OFFERING", nullable = false, insertable = true, updatable = true, length = 255)
	public String getOffering() {
		return offering;
	}

	public void setOffering(String offering) {
		this.offering = offering;
	}

	@Column(name = "TRADING_BRAND", nullable = false, insertable = true, updatable = true, length = 25)
	public String getTradingBrand() {
		return tradingBrand;
	}

	public void setTradingBrand(String tradingBrand) {
		this.tradingBrand = tradingBrand;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		CommercialOffersEntity that = (CommercialOffersEntity) o;

		if (idCommercialOffers != that.idCommercialOffers) return false;
		if (idUtiilizationType != null ? !idUtiilizationType.equals(that.idUtiilizationType) : that.idUtiilizationType != null)
			return false;
		if (iddsp != null ? !iddsp.equals(that.iddsp) : that.iddsp != null) return false;
		if (offering != null ? !offering.equals(that.offering) : that.offering != null) return false;
		if (tradingBrand != null ? !tradingBrand.equals(that.tradingBrand) : that.tradingBrand != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = idCommercialOffers;
		result = 31 * result + (iddsp != null ? iddsp.hashCode() : 0);
		result = 31 * result + (idUtiilizationType != null ? idUtiilizationType.hashCode() : 0);
		result = 31 * result + (offering != null ? offering.hashCode() : 0);
		result = 31 * result + (tradingBrand != null ? tradingBrand.hashCode() : 0);
		return result;
	}
}
