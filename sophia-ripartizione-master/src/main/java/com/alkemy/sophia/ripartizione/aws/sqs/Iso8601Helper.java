package com.alkemy.sophia.ripartizione.aws.sqs;

import java.util.Calendar;
import java.util.Date;

import javax.xml.bind.DatatypeConverter;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class Iso8601Helper {

	public static String format(Date date) {
		final Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return DatatypeConverter.printDateTime(calendar);
	}
	
	public static Date parse(String date) {
		final Calendar calendar = DatatypeConverter.parseDateTime(date);
		return calendar.getTime();
	}
	
}
