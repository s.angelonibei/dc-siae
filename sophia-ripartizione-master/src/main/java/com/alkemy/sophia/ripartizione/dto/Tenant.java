
package com.alkemy.sophia.ripartizione.dto;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "black_list_url",
    "society",
    "territory"
})
public class Tenant {

    @JsonProperty("black_list_url")
    private String blackListUrl;
    @JsonProperty("society")
    private String society;
    @JsonProperty("territory")
    private String territory;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("black_list_url")
    public String getBlackListUrl() {
        return blackListUrl;
    }

    @JsonProperty("black_list_url")
    public void setBlackListUrl(String blackListUrl) {
        this.blackListUrl = blackListUrl;
    }

    public Tenant withBlackListUrl(String blackListUrl) {
        this.blackListUrl = blackListUrl;
        return this;
    }

    @JsonProperty("society")
    public String getSociety() {
        return society;
    }

    @JsonProperty("society")
    public void setSociety(String society) {
        this.society = society;
    }

    public Tenant withSociety(String society) {
        this.society = society;
        return this;
    }

    @JsonProperty("territory")
    public String getTerritory() {
        return territory;
    }

    @JsonProperty("territory")
    public void setTerritory(String territory) {
        this.territory = territory;
    }

    public Tenant withTerritory(String territory) {
        this.territory = territory;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Tenant withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

}
