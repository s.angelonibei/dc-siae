package com.alkemy.sophia.ripartizione.dto;

/**
 * Created by ftomei on 28/06/19.
 */

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@Setter
public class DsrInfo {

	private String code;
	private String ftpSourcePath;
	private String idDsp;
	private String idUtiilizationType;
	private String periodType;
	private Integer period;
	private Integer year;


}
