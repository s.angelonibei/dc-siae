package com.alkemy.sophia.ripartizione.aws.sqs;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import com.alkemytech.sophia.commons.sqs.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.mmap.MemoryMapException;
import com.alkemytech.sophia.commons.mmap.UnsafeMemoryMap;
import com.alkemytech.sophia.commons.trie.BooleanBinding;
import com.alkemytech.sophia.commons.trie.Trie;
import com.google.common.base.Strings;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
public class TrieMessageDeduplicator implements com.alkemytech.sophia.commons.sqs.MessageDeduplicator {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	private final Trie<Boolean> trie;
	
	public TrieMessageDeduplicator(File folder) throws IOException, NoSuchAlgorithmException {
		super();
		trie = Trie.wrap(UnsafeMemoryMap
				.open(folder, 65536, false), new BooleanBinding());
	}

	@Override
	public boolean deduplicate(String uuid, String queueName) {
		if (Strings.isNullOrEmpty(uuid))
			return false;
		try {
			return null == trie.upsert(uuid, true);
		} catch (MemoryMapException e) {
			logger.error("deduplicate", e);
		}
		return false;
	}

}
