package com.alkemy.sophia.ripartizione.aws.sqs;

import java.util.List;
import java.util.Properties;
import java.util.UUID;

import com.alkemy.sophia.ripartizione.aws.SQS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.commons.util.GsonUtils;
import com.alkemytech.sophia.commons.util.TextUtils;
import com.google.common.base.Strings;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * @author roberto cerfogli <roberto@seqrware.com>
 */
@Component
//@PropertySource(value = "application.yml")
public class SqsMessagePump {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Value("${default.environment}_$sqs.queuePrefix:mm_ripartizione.sqs")
	private String queuePrefix;

	public static interface Consumer {
		
		/**
		 * Returns a context json object if started message should be sent, <code>null</code> otherwise.
		 * <br/>
		 * This method is always invoked before {@link consumeMessage()}.sqs.
		 * 
		 * @return a context json object if started message should be sent, <code>null</code> otherwise.
		 */
		public JsonObject getStartedMessagePayload(JsonObject message);

		/**
		 * Process a standard json message.
		 * 
		 * @param message the json message to process
		 * @return <code>true</code> if message was successfully processed, <code>false</code> otherwise.
		 */
		public boolean consumeMessage(JsonObject message);
		
		/**
		 * Returns a output json object if completed message should be sent, <code>null</code> otherwise.
		 * <br/>
		 * This method is invoked only if {@link consumeMessage()} returns <code>true</code>.
		 * 
		 * @return a output json object if completed message should be sent, <code>null</code> otherwise.
		 */
		public JsonObject getCompletedMessagePayload(JsonObject message);

		/**
		 * Returns a error json object if completed message should be sent, <code>null</code> otherwise.
		 * <br/>
		 * This method is invoked only if {@link consumeMessage()} returns <code>false</code>.
		 * 
		 * @return a error json object if completed message should be sent, <code>null</code> otherwise.
		 */
		public JsonObject getFailedMessagePayload(JsonObject message);

	}

	@Autowired
	@Qualifier("sqs")
	private SQS sqs;

	@Value("${${sqs.queuePrefix}.sqs.max_messages_per_run}")
	private int maxMessagesPerRun;

	@Value("${${sqs.queuePrefix}.sqs.max_polling_duration_millis}")
	private long maxPollingDurationMillis;

	@Value("${${sqs.queuePrefix}.sqs.polling_period}")
	private long pollingPeriod;

	@Value("${${sqs.queuePrefix}.sqs.sender_name}")
	private String senderName;

	@Value("${default.environment}_${${sqs.queuePrefix}.sqs.service_bus_queue}")
	private String serviceBusQueue;

	@Value("${default.environment}_${${sqs.queuePrefix}.sqs.to_process_queue}")
	private String toProcessQueue;

	@Value("${default.environment}_${${sqs.queuePrefix}.sqs.started_queue}")
	private String startedQueue;

	@Value("${default.environment}_${${sqs.queuePrefix}.sqs.completed_queue}")
	private String completedQueue;

	@Value("${default.environment}_${${sqs.queuePrefix}.sqs.failed_queue}")
	private String failedQueue;

	public SqsMessagePump() {
		super();
	}

	public void sendToProcessMessage(JsonObject body, boolean skipServiceBus) {
		sendToProcessMessage(toProcessQueue, body, skipServiceBus);
	}

	public void sendToProcessMessage(String queueName, JsonObject body, boolean skipServiceBus) {
		final String queueUrl = sqs.getOrCreateUrl(skipServiceBus || Strings
				.isNullOrEmpty(serviceBusQueue) ? queueName : serviceBusQueue);
		final JsonObject message = com.alkemytech.sophia.commons.sqs.SqsMessageHelper
				.formatToProcessJson(queueName, UUID.randomUUID().toString(), senderName, body);
		logger.debug("queueUrl {}", queueUrl);
		logger.debug("message {}", message);
		sqs.sendMessage(queueUrl, GsonUtils.toJson(message));
	}
	
	public void sendStartedMessage(JsonObject input, JsonObject context, boolean skipServiceBus) {
		sendStartedMessage(startedQueue, input, context, skipServiceBus);
	}

	public void sendStartedMessage(String queueName, JsonObject input, JsonObject context, boolean skipServiceBus) {
		final String queueUrl = sqs.getOrCreateUrl(skipServiceBus || Strings
				.isNullOrEmpty(serviceBusQueue) ? queueName : serviceBusQueue);
		final JsonObject message = com.alkemytech.sophia.commons.sqs.SqsMessageHelper
				.formatStartedJson(queueName, senderName, input, context);
		logger.debug("queueUrl {}", queueUrl);
		logger.debug("message {}", message);
		sqs.sendMessage(queueUrl, GsonUtils.toJson(message));
	}
	
	public void sendCompletedMessage(JsonObject input, JsonObject output, boolean skipServiceBus) {
		sendCompletedMessage(completedQueue, input, output, skipServiceBus);
	}

	public void sendCompletedMessage(String queueName, JsonObject input, JsonObject output, boolean skipServiceBus) {
		final String queueUrl = sqs.getOrCreateUrl(skipServiceBus || Strings
				.isNullOrEmpty(serviceBusQueue) ? queueName : serviceBusQueue);
		final JsonObject message = com.alkemytech.sophia.commons.sqs.SqsMessageHelper
				.formatCompletedJson(queueName, senderName, input, output);
		logger.debug("queueUrl {}", queueUrl);
		logger.debug("message {}", message);
		sqs.sendMessage(queueUrl, GsonUtils.toJson(message));
	}
	
	public void sendFailedMessage(JsonObject input, JsonObject error, boolean skipServiceBus) {
		sendFailedMessage(failedQueue, input, error, skipServiceBus);
	}
	
	public void sendFailedMessage(String queueName, JsonObject input, JsonObject error, boolean skipServiceBus) {
		final String queueUrl = sqs.getOrCreateUrl(skipServiceBus || Strings
				.isNullOrEmpty(serviceBusQueue) ? queueName : serviceBusQueue);
		final JsonObject message = com.alkemytech.sophia.commons.sqs.SqsMessageHelper
				.formatFailedJson(queueName, senderName, input, error);
		logger.debug("queueUrl {}", queueUrl);
		logger.debug("message {}", message);
		sqs.sendMessage(queueUrl, GsonUtils.toJson(message));
	}
	
	public <T extends MessageDeduplicator> void pollingLoop(T deduplicator, Consumer handler) throws Exception {
		
		final String toProcessQueueUrl = sqs.getOrCreateUrl(toProcessQueue);
		logger.debug("toProcessQueueUrl {}", toProcessQueueUrl);
		
		final long startTimeMillis = System.currentTimeMillis();
		for (int receivedMessages = 0; receivedMessages < maxMessagesPerRun &&
				System.currentTimeMillis() - startTimeMillis < maxPollingDurationMillis; ) {
			
			// read messages from to_process queue
			final List<SQS.Msg> messages = sqs.receiveMessages(toProcessQueueUrl);
			logger.debug("{} message(s) received", messages.size());
			if (null == messages || messages.isEmpty()) {
				Thread.sleep(pollingPeriod);
				continue;
			}
			
			// loop on received message(s)
			for (SQS.Msg message : messages) {
				receivedMessages ++;
				try {
					
					// parse message json
					logger.debug("processing message {}", message.text);
					final JsonObject messageJson = GsonUtils.
							fromJson(message.text, JsonObject.class);
					
					// deduplicate message
					if (null != deduplicator && !deduplicator
							.deduplicate(SqsMessageHelper.getMessageUuid(messageJson), toProcessQueue)) {
						logger.warn("duplicate message");
					}
					
					// process message
					else {
						
						// send started message
						final JsonObject context = handler.getStartedMessagePayload(messageJson);
						if (null != context && !Strings.isNullOrEmpty(startedQueue)) {
							sendStartedMessage(startedQueue, messageJson, context, false);
						}

						// consume message
						if (handler.consumeMessage(messageJson)) {
							
							// send completed message
							final JsonObject output = handler.getCompletedMessagePayload(messageJson);
							if (null != output && !Strings.isNullOrEmpty(completedQueue)) {
								sendCompletedMessage(completedQueue, messageJson, output, false);
							}

						} else {
							
							// send failed message
							final JsonObject error = handler.getFailedMessagePayload(messageJson);
							if (null != error && !Strings.isNullOrEmpty(failedQueue)) {
								sendFailedMessage(failedQueue, messageJson, error, false);
							}

						}
						
					}

					// delete from to_process queue
					sqs.deleteMessage(toProcessQueueUrl, message.receiptHandle);
					
				} catch (Exception e) {
					logger.error("messageLoop", e);
				}
			}
		}
		logger.info("exiting polling loop after {}", TextUtils
				.formatDuration(System.currentTimeMillis() - startTimeMillis));
		
	}

	public String getFailedQueue() {
		return failedQueue;
	}

	public void setFailedQueue(String failedQueue) {
		this.failedQueue = failedQueue;
	}

	public int getMaxMessagesPerRun() {
		return maxMessagesPerRun;
	}

	public void setMaxMessagesPerRun(int maxMessagesPerRun) {
		this.maxMessagesPerRun = maxMessagesPerRun;
	}

	public long getMaxPollingDurationMillis() {
		return maxPollingDurationMillis;
	}

	public void setMaxPollingDurationMillis(long maxPollingDurationMillis) {
		this.maxPollingDurationMillis = maxPollingDurationMillis;
	}

	public long getPollingPeriod() {
		return pollingPeriod;
	}

	public void setPollingPeriod(long pollingPeriod) {
		this.pollingPeriod = pollingPeriod;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public String getServiceBusQueue() {
		return serviceBusQueue;
	}

	public void setServiceBusQueue(String serviceBusQueue) {
		this.serviceBusQueue = serviceBusQueue;
	}

	public String getToProcessQueue() {
		return toProcessQueue;
	}

	public void setToProcessQueue(String toProcessQueue) {
		this.toProcessQueue = toProcessQueue;
	}

	public String getStartedQueue() {
		return startedQueue;
	}

	public void setStartedQueue(String startedQueue) {
		this.startedQueue = startedQueue;
	}

	public String getCompletedQueue() {
		return completedQueue;
	}

	public void setCompletedQueue(String completedQueue) {
		this.completedQueue = completedQueue;
	}

	public String getQueuePrefix() {
		return queuePrefix;
	}

	public void setQueuePrefix(String queuePrefix) {
		this.queuePrefix = queuePrefix;
	}
}
