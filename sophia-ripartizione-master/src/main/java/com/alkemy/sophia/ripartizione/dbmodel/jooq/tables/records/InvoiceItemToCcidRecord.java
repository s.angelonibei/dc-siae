/*
 * This file is generated by jOOQ.
 */
package com.alkemy.sophia.ripartizione.dbmodel.jooq.tables.records;


import com.alkemy.sophia.ripartizione.dbmodel.jooq.tables.InvoiceItemToCcid;

import java.math.BigDecimal;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record7;
import org.jooq.Row7;
import org.jooq.impl.UpdatableRecordImpl;
import org.jooq.types.UInteger;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.9"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class InvoiceItemToCcidRecord extends UpdatableRecordImpl<InvoiceItemToCcidRecord> implements Record7<UInteger, Long, Integer, BigDecimal, Long, BigDecimal, String> {

    private static final long serialVersionUID = 1126770863;

    /**
     * Setter for <code>INVOICE_ITEM_TO_CCID.id</code>.
     */
    public void setId(UInteger value) {
        set(0, value);
    }

    /**
     * Getter for <code>INVOICE_ITEM_TO_CCID.id</code>.
     */
    public UInteger getId() {
        return (UInteger) get(0);
    }

    /**
     * Setter for <code>INVOICE_ITEM_TO_CCID.ID_CCID_METADATA</code>.
     */
    public void setIdCcidMetadata(Long value) {
        set(1, value);
    }

    /**
     * Getter for <code>INVOICE_ITEM_TO_CCID.ID_CCID_METADATA</code>.
     */
    public Long getIdCcidMetadata() {
        return (Long) get(1);
    }

    /**
     * Setter for <code>INVOICE_ITEM_TO_CCID.ID_INVOICE_ITEM</code>.
     */
    public void setIdInvoiceItem(Integer value) {
        set(2, value);
    }

    /**
     * Getter for <code>INVOICE_ITEM_TO_CCID.ID_INVOICE_ITEM</code>.
     */
    public Integer getIdInvoiceItem() {
        return (Integer) get(2);
    }

    /**
     * Setter for <code>INVOICE_ITEM_TO_CCID.VALORE</code>.
     */
    public void setValore(BigDecimal value) {
        set(3, value);
    }

    /**
     * Getter for <code>INVOICE_ITEM_TO_CCID.VALORE</code>.
     */
    public BigDecimal getValore() {
        return (BigDecimal) get(3);
    }

    /**
     * Setter for <code>INVOICE_ITEM_TO_CCID.PERIODO_RIPARTIZIONE_ID</code>.
     */
    public void setPeriodoRipartizioneId(Long value) {
        set(4, value);
    }

    /**
     * Getter for <code>INVOICE_ITEM_TO_CCID.PERIODO_RIPARTIZIONE_ID</code>.
     */
    public Long getPeriodoRipartizioneId() {
        return (Long) get(4);
    }

    /**
     * Setter for <code>INVOICE_ITEM_TO_CCID.VALORE_APPLICATO_IN_CHIUSURA</code>.
     */
    public void setValoreApplicatoInChiusura(BigDecimal value) {
        set(5, value);
    }

    /**
     * Getter for <code>INVOICE_ITEM_TO_CCID.VALORE_APPLICATO_IN_CHIUSURA</code>.
     */
    public BigDecimal getValoreApplicatoInChiusura() {
        return (BigDecimal) get(5);
    }

    /**
     * Setter for <code>INVOICE_ITEM_TO_CCID.OPERAZIONE</code>.
     */
    public void setOperazione(String value) {
        set(6, value);
    }

    /**
     * Getter for <code>INVOICE_ITEM_TO_CCID.OPERAZIONE</code>.
     */
    public String getOperazione() {
        return (String) get(6);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<UInteger> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record7 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row7<UInteger, Long, Integer, BigDecimal, Long, BigDecimal, String> fieldsRow() {
        return (Row7) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row7<UInteger, Long, Integer, BigDecimal, Long, BigDecimal, String> valuesRow() {
        return (Row7) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<UInteger> field1() {
        return InvoiceItemToCcid.INVOICE_ITEM_TO_CCID.ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Long> field2() {
        return InvoiceItemToCcid.INVOICE_ITEM_TO_CCID.ID_CCID_METADATA;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field3() {
        return InvoiceItemToCcid.INVOICE_ITEM_TO_CCID.ID_INVOICE_ITEM;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<BigDecimal> field4() {
        return InvoiceItemToCcid.INVOICE_ITEM_TO_CCID.VALORE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Long> field5() {
        return InvoiceItemToCcid.INVOICE_ITEM_TO_CCID.PERIODO_RIPARTIZIONE_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<BigDecimal> field6() {
        return InvoiceItemToCcid.INVOICE_ITEM_TO_CCID.VALORE_APPLICATO_IN_CHIUSURA;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field7() {
        return InvoiceItemToCcid.INVOICE_ITEM_TO_CCID.OPERAZIONE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UInteger component1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long component2() {
        return getIdCcidMetadata();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component3() {
        return getIdInvoiceItem();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BigDecimal component4() {
        return getValore();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long component5() {
        return getPeriodoRipartizioneId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BigDecimal component6() {
        return getValoreApplicatoInChiusura();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component7() {
        return getOperazione();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UInteger value1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long value2() {
        return getIdCcidMetadata();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value3() {
        return getIdInvoiceItem();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BigDecimal value4() {
        return getValore();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long value5() {
        return getPeriodoRipartizioneId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BigDecimal value6() {
        return getValoreApplicatoInChiusura();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value7() {
        return getOperazione();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InvoiceItemToCcidRecord value1(UInteger value) {
        setId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InvoiceItemToCcidRecord value2(Long value) {
        setIdCcidMetadata(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InvoiceItemToCcidRecord value3(Integer value) {
        setIdInvoiceItem(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InvoiceItemToCcidRecord value4(BigDecimal value) {
        setValore(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InvoiceItemToCcidRecord value5(Long value) {
        setPeriodoRipartizioneId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InvoiceItemToCcidRecord value6(BigDecimal value) {
        setValoreApplicatoInChiusura(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InvoiceItemToCcidRecord value7(String value) {
        setOperazione(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InvoiceItemToCcidRecord values(UInteger value1, Long value2, Integer value3, BigDecimal value4, Long value5, BigDecimal value6, String value7) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        value7(value7);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached InvoiceItemToCcidRecord
     */
    public InvoiceItemToCcidRecord() {
        super(InvoiceItemToCcid.INVOICE_ITEM_TO_CCID);
    }

    /**
     * Create a detached, initialised InvoiceItemToCcidRecord
     */
    public InvoiceItemToCcidRecord(UInteger id, Long idCcidMetadata, Integer idInvoiceItem, BigDecimal valore, Long periodoRipartizioneId, BigDecimal valoreApplicatoInChiusura, String operazione) {
        super(InvoiceItemToCcid.INVOICE_ITEM_TO_CCID);

        set(0, id);
        set(1, idCcidMetadata);
        set(2, idInvoiceItem);
        set(3, valore);
        set(4, periodoRipartizioneId);
        set(5, valoreApplicatoInChiusura);
        set(6, operazione);
    }
}
