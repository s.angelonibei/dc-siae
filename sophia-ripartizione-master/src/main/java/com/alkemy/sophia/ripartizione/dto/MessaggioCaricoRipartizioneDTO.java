
package com.alkemy.sophia.ripartizione.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
	"applied_tariff",
	"baseRefId",
	"businessOffer",
	"carichiRipartizione",
	"ccidVersion",
	"ccid_id",
	"claimPeriodType",
	"claimUtilizationType",
	"commercialOffer",
	"conversion_rate",
	"country",
	"decisionTableUrl",
	"dsp",
	"dspCode",
	"encoded",
	"encodedProvisional",
	"idDsr",
	"idUtilizationType",
	"mandators",
	"month",
	"notEncoded",
	"original_price_basis_currency",
	"period",
	"periodEndDate",
	"periodStartDate",
	"periodType",
	"priceModel",
	"receiver",
	"sender",
	"service_type",
	"sliceDspCode",
	"sliceMonth",
	"split_mech",
	"split_perf",
	"tenant",
	"territory",
	"totalSales",
	"trading_brand",
	"use_type",
	"work_code_type",
	"year"
})
public class MessaggioCaricoRipartizioneDTO {

	@JsonProperty("applied_tariff")
	private String appliedTariff;
	@JsonProperty("baseRefId")
	private String baseRefId;
	@JsonProperty("businessOffer")
	private String businessOffer;
	@JsonProperty("carichiRipartizione")
	private List<CaricoRipartizioneDTO> carichiRipartizione = new ArrayList<>();
	@JsonProperty("ccidVersion")
	private String ccidVersion;
	@JsonProperty("ccid_id")
	private String ccidId;
	@JsonProperty("claimPeriodType")
	private String claimPeriodType;
	@JsonProperty("claimUtilizationType")
	private String claimUtilizationType;
	@JsonProperty("commercialOffer")
	private String commercialOffer;
	@JsonProperty("conversion_rate")
	private BigDecimal conversionRate;
	@JsonProperty("country")
	private String country;
	@JsonProperty("decisionTableUrl")
	private String decisionTableUrl;
	@JsonProperty("dsp")
	private String dsp;
	@JsonProperty("dspCode")
	private String dspCode;
	@JsonProperty("encoded")
	private String encoded;
	@JsonProperty("encodedProvisional")
	private String encodedProvisional;
	@JsonProperty("idDsr")
	private String idDsr;
	@JsonProperty("idUtilizationType")
	private String idUtilizationType;
	@JsonProperty("mandators")
	private List<MandatorDTO> mandators = new ArrayList<>();
	@JsonProperty("month")
	private String month;
	@JsonProperty("notEncoded")
	private String notEncoded;
	@JsonProperty("original_price_basis_currency")
	private String originalPriceBasisCurrency;
	@JsonProperty("period")
	private String period;
	@JsonProperty("periodEndDate")
	private String periodEndDate;
	@JsonProperty("periodStartDate")
	private String periodStartDate;
	@JsonProperty("periodType")
	private String periodType;
	@JsonProperty("priceModel")
	private String priceModel;
	@JsonProperty("receiver")
	private String receiver;
	@JsonProperty("sender")
	private String sender;
	@JsonProperty("service_type")
	private String serviceType;
	@JsonProperty("sliceDspCode")
	private String sliceDspCode;
	@JsonProperty("sliceMonth")
	private String sliceMonth;
	@JsonProperty("split_mech")
	private String splitMech;
	@JsonProperty("split_perf")
	private String splitPerf;
	@JsonProperty("tenant")
	private Tenant tenant;
	@JsonProperty("territory")
	private String territory;
	@JsonProperty("totalSales")
	private String totalSales;
	@JsonProperty("trading_brand")
	private String tradingBrand;
	@JsonProperty("use_type")
	private String useType;
	@JsonProperty("work_code_type")
	private String workCodeType;
	@JsonProperty("year")
	private String year;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("applied_tariff")
	public String getAppliedTariff() {
		return appliedTariff;
	}

	@JsonProperty("applied_tariff")
	public void setAppliedTariff(String appliedTariff) {
		this.appliedTariff = appliedTariff;
	}

	public MessaggioCaricoRipartizioneDTO withAppliedTariff(String appliedTariff) {
		this.appliedTariff = appliedTariff;
		return this;
	}

	@JsonProperty("baseRefId")
	public String getBaseRefId() {
		return baseRefId;
	}

	@JsonProperty("baseRefId")
	public void setBaseRefId(String baseRefId) {
		this.baseRefId = baseRefId;
	}

	public MessaggioCaricoRipartizioneDTO withBaseRefId(String baseRefId) {
		this.baseRefId = baseRefId;
		return this;
	}

	@JsonProperty("businessOffer")
	public String getBusinessOffer() {
		return businessOffer;
	}

	@JsonProperty("businessOffer")
	public void setBusinessOffer(String businessOffer) {
		this.businessOffer = businessOffer;
	}

	public MessaggioCaricoRipartizioneDTO withBusinessOffer(String businessOffer) {
		this.businessOffer = businessOffer;
		return this;
	}

	@JsonProperty("carichiRipartizione")
	public List<CaricoRipartizioneDTO> getCarichiRipartizione() {
		return carichiRipartizione;
	}

	@JsonProperty("carichiRipartizione")
	public void setCarichiRipartizione(List<CaricoRipartizioneDTO> carichiRipartizione) {
		this.carichiRipartizione = carichiRipartizione;
	}

	public MessaggioCaricoRipartizioneDTO withCarichiRipartizione(List<CaricoRipartizioneDTO> carichiRipartizione) {
		this.carichiRipartizione = carichiRipartizione;
		return this;
	}

	@JsonProperty("ccidVersion")
	public String getCcidVersion() {
		return ccidVersion;
	}

	@JsonProperty("ccidVersion")
	public void setCcidVersion(String ccidVersion) {
		this.ccidVersion = ccidVersion;
	}

	public MessaggioCaricoRipartizioneDTO withCcidVersion(String ccidVersion) {
		this.ccidVersion = ccidVersion;
		return this;
	}

	@JsonProperty("ccid_id")
	public String getCcidId() {
		return ccidId;
	}

	@JsonProperty("ccid_id")
	public void setCcidId(String ccidId) {
		this.ccidId = ccidId;
	}

	public MessaggioCaricoRipartizioneDTO withCcidId(String ccidId) {
		this.ccidId = ccidId;
		return this;
	}

	@JsonProperty("claimPeriodType")
	public String getClaimPeriodType() {
		return claimPeriodType;
	}

	@JsonProperty("claimPeriodType")
	public void setClaimPeriodType(String claimPeriodType) {
		this.claimPeriodType = claimPeriodType;
	}

	public MessaggioCaricoRipartizioneDTO withClaimPeriodType(String claimPeriodType) {
		this.claimPeriodType = claimPeriodType;
		return this;
	}

	@JsonProperty("claimUtilizationType")
	public String getClaimUtilizationType() {
		return claimUtilizationType;
	}

	@JsonProperty("claimUtilizationType")
	public void setClaimUtilizationType(String claimUtilizationType) {
		this.claimUtilizationType = claimUtilizationType;
	}

	public MessaggioCaricoRipartizioneDTO withClaimUtilizationType(String claimUtilizationType) {
		this.claimUtilizationType = claimUtilizationType;
		return this;
	}

	@JsonProperty("commercialOffer")
	public String getCommercialOffer() {
		return commercialOffer;
	}

	@JsonProperty("commercialOffer")
	public void setCommercialOffer(String commercialOffer) {
		this.commercialOffer = commercialOffer;
	}

	public MessaggioCaricoRipartizioneDTO withCommercialOffer(String commercialOffer) {
		this.commercialOffer = commercialOffer;
		return this;
	}

	@JsonProperty("conversion_rate")
	public BigDecimal getConversionRate() {
		return conversionRate;
	}

	@JsonProperty("conversion_rate")
	public void setConversionRate(BigDecimal conversionRate) {
		this.conversionRate = conversionRate;
	}

	public MessaggioCaricoRipartizioneDTO withConversionRate(BigDecimal conversionRate) {
		this.conversionRate = conversionRate;
		return this;
	}

	@JsonProperty("country")
	public String getCountry() {
		return country;
	}

	@JsonProperty("country")
	public void setCountry(String country) {
		this.country = country;
	}

	public MessaggioCaricoRipartizioneDTO withCountry(String country) {
		this.country = country;
		return this;
	}

	@JsonProperty("decisionTableUrl")
	public String getDecisionTableUrl() {
		return decisionTableUrl;
	}

	@JsonProperty("decisionTableUrl")
	public void setDecisionTableUrl(String decisionTableUrl) {
		this.decisionTableUrl = decisionTableUrl;
	}

	public MessaggioCaricoRipartizioneDTO withDecisionTableUrl(String decisionTableUrl) {
		this.decisionTableUrl = decisionTableUrl;
		return this;
	}

	@JsonProperty("dsp")
	public String getDsp() {
		return dsp;
	}

	@JsonProperty("dsp")
	public void setDsp(String dsp) {
		this.dsp = dsp;
	}

	public MessaggioCaricoRipartizioneDTO withDsp(String dsp) {
		this.dsp = dsp;
		return this;
	}

	@JsonProperty("dspCode")
	public String getDspCode() {
		return dspCode;
	}

	@JsonProperty("dspCode")
	public void setDspCode(String dspCode) {
		this.dspCode = dspCode;
	}

	public MessaggioCaricoRipartizioneDTO withDspCode(String dspCode) {
		this.dspCode = dspCode;
		return this;
	}

	@JsonProperty("encoded")
	public String getEncoded() {
		return encoded;
	}

	@JsonProperty("encoded")
	public void setEncoded(String encoded) {
		this.encoded = encoded;
	}

	public MessaggioCaricoRipartizioneDTO withEncoded(String encoded) {
		this.encoded = encoded;
		return this;
	}

	@JsonProperty("encodedProvisional")
	public String getEncodedProvisional() {
		return encodedProvisional;
	}

	@JsonProperty("encodedProvisional")
	public void setEncodedProvisional(String encodedProvisional) {
		this.encodedProvisional = encodedProvisional;
	}

	public MessaggioCaricoRipartizioneDTO withEncodedProvisional(String encodedProvisional) {
		this.encodedProvisional = encodedProvisional;
		return this;
	}

	@JsonProperty("idDsr")
	public String getIdDsr() {
		return idDsr;
	}

	@JsonProperty("idDsr")
	public void setIdDsr(String idDsr) {
		this.idDsr = idDsr;
	}

	public MessaggioCaricoRipartizioneDTO withIdDsr(String idDsr) {
		this.idDsr = idDsr;
		return this;
	}

	@JsonProperty("idUtilizationType")
	public String getIdUtilizationType() {
		return idUtilizationType;
	}

	@JsonProperty("idUtilizationType")
	public void setIdUtilizationType(String idUtilizationType) {
		this.idUtilizationType = idUtilizationType;
	}

	public MessaggioCaricoRipartizioneDTO withIdUtilizationType(String idUtilizationType) {
		this.idUtilizationType = idUtilizationType;
		return this;
	}

	@JsonProperty("mandators")
	public List<MandatorDTO> getMandators() {
		return mandators;
	}

	@JsonProperty("mandators")
	public void setMandators(List<MandatorDTO> mandators) {
		this.mandators = mandators;
	}

	public MessaggioCaricoRipartizioneDTO withMandators(List<MandatorDTO> mandators) {
		this.mandators = mandators;
		return this;
	}

	@JsonProperty("month")
	public String getMonth() {
		return month;
	}

	@JsonProperty("month")
	public void setMonth(String month) {
		this.month = month;
	}

	public MessaggioCaricoRipartizioneDTO withMonth(String month) {
		this.month = month;
		return this;
	}

	@JsonProperty("notEncoded")
	public String getNotEncoded() {
		return notEncoded;
	}

	@JsonProperty("notEncoded")
	public void setNotEncoded(String notEncoded) {
		this.notEncoded = notEncoded;
	}

	public MessaggioCaricoRipartizioneDTO withNotEncoded(String notEncoded) {
		this.notEncoded = notEncoded;
		return this;
	}

	@JsonProperty("original_price_basis_currency")
	public String getOriginalPriceBasisCurrency() {
		return originalPriceBasisCurrency;
	}

	@JsonProperty("original_price_basis_currency")
	public void setOriginalPriceBasisCurrency(String originalPriceBasisCurrency) {
		this.originalPriceBasisCurrency = originalPriceBasisCurrency;
	}

	public MessaggioCaricoRipartizioneDTO withOriginalPriceBasisCurrency(String originalPriceBasisCurrency) {
		this.originalPriceBasisCurrency = originalPriceBasisCurrency;
		return this;
	}

	@JsonProperty("period")
	public String getPeriod() {
		return period;
	}

	@JsonProperty("period")
	public void setPeriod(String period) {
		this.period = period;
	}

	public MessaggioCaricoRipartizioneDTO withPeriod(String period) {
		this.period = period;
		return this;
	}

	@JsonProperty("periodEndDate")
	public String getPeriodEndDate() {
		return periodEndDate;
	}

	@JsonProperty("periodEndDate")
	public void setPeriodEndDate(String periodEndDate) {
		this.periodEndDate = periodEndDate;
	}

	public MessaggioCaricoRipartizioneDTO withPeriodEndDate(String periodEndDate) {
		this.periodEndDate = periodEndDate;
		return this;
	}

	@JsonProperty("periodStartDate")
	public String getPeriodStartDate() {
		return periodStartDate;
	}

	@JsonProperty("periodStartDate")
	public void setPeriodStartDate(String periodStartDate) {
		this.periodStartDate = periodStartDate;
	}

	public MessaggioCaricoRipartizioneDTO withPeriodStartDate(String periodStartDate) {
		this.periodStartDate = periodStartDate;
		return this;
	}

	@JsonProperty("periodType")
	public String getPeriodType() {
		return periodType;
	}

	@JsonProperty("periodType")
	public void setPeriodType(String periodType) {
		this.periodType = periodType;
	}

	public MessaggioCaricoRipartizioneDTO withPeriodType(String periodType) {
		this.periodType = periodType;
		return this;
	}

	@JsonProperty("priceModel")
	public String getPriceModel() {
		return priceModel;
	}

	@JsonProperty("priceModel")
	public void setPriceModel(String priceModel) {
		this.priceModel = priceModel;
	}

	public MessaggioCaricoRipartizioneDTO withPriceModel(String priceModel) {
		this.priceModel = priceModel;
		return this;
	}

	@JsonProperty("receiver")
	public String getReceiver() {
		return receiver;
	}

	@JsonProperty("receiver")
	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public MessaggioCaricoRipartizioneDTO withReceiver(String receiver) {
		this.receiver = receiver;
		return this;
	}

	@JsonProperty("sender")
	public String getSender() {
		return sender;
	}

	@JsonProperty("sender")
	public void setSender(String sender) {
		this.sender = sender;
	}

	public MessaggioCaricoRipartizioneDTO withSender(String sender) {
		this.sender = sender;
		return this;
	}

	@JsonProperty("service_type")
	public String getServiceType() {
		return serviceType;
	}

	@JsonProperty("service_type")
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public MessaggioCaricoRipartizioneDTO withServiceType(String serviceType) {
		this.serviceType = serviceType;
		return this;
	}

	@JsonProperty("sliceDspCode")
	public String getSliceDspCode() {
		return sliceDspCode;
	}

	@JsonProperty("sliceDspCode")
	public void setSliceDspCode(String sliceDspCode) {
		this.sliceDspCode = sliceDspCode;
	}

	public MessaggioCaricoRipartizioneDTO withSliceDspCode(String sliceDspCode) {
		this.sliceDspCode = sliceDspCode;
		return this;
	}

	@JsonProperty("sliceMonth")
	public String getSliceMonth() {
		return sliceMonth;
	}

	@JsonProperty("sliceMonth")
	public void setSliceMonth(String sliceMonth) {
		this.sliceMonth = sliceMonth;
	}

	public MessaggioCaricoRipartizioneDTO withSliceMonth(String sliceMonth) {
		this.sliceMonth = sliceMonth;
		return this;
	}

	@JsonProperty("split_mech")
	public String getSplitMech() {
		return splitMech;
	}

	@JsonProperty("split_mech")
	public void setSplitMech(String splitMech) {
		this.splitMech = splitMech;
	}

	public MessaggioCaricoRipartizioneDTO withSplitMech(String splitMech) {
		this.splitMech = splitMech;
		return this;
	}

	@JsonProperty("split_perf")
	public String getSplitPerf() {
		return splitPerf;
	}

	@JsonProperty("split_perf")
	public void setSplitPerf(String splitPerf) {
		this.splitPerf = splitPerf;
	}

	public MessaggioCaricoRipartizioneDTO withSplitPerf(String splitPerf) {
		this.splitPerf = splitPerf;
		return this;
	}

	@JsonProperty("tenant")
	public Tenant getTenant() {
		return tenant;
	}

	@JsonProperty("tenant")
	public void setTenant(Tenant tenant) {
		this.tenant = tenant;
	}

	public MessaggioCaricoRipartizioneDTO withTenant(Tenant tenant) {
		this.tenant = tenant;
		return this;
	}

	@JsonProperty("territory")
	public String getTerritory() {
		return territory;
	}

	@JsonProperty("territory")
	public void setTerritory(String territory) {
		this.territory = territory;
	}

	public MessaggioCaricoRipartizioneDTO withTerritory(String territory) {
		this.territory = territory;
		return this;
	}

	@JsonProperty("totalSales")
	public String getTotalSales() {
		return totalSales;
	}

	@JsonProperty("totalSales")
	public void setTotalSales(String totalSales) {
		this.totalSales = totalSales;
	}

	public MessaggioCaricoRipartizioneDTO withTotalSales(String totalSales) {
		this.totalSales = totalSales;
		return this;
	}

	@JsonProperty("trading_brand")
	public String getTradingBrand() {
		return tradingBrand;
	}

	@JsonProperty("trading_brand")
	public void setTradingBrand(String tradingBrand) {
		this.tradingBrand = tradingBrand;
	}

	public MessaggioCaricoRipartizioneDTO withTradingBrand(String tradingBrand) {
		this.tradingBrand = tradingBrand;
		return this;
	}

	@JsonProperty("use_type")
	public String getUseType() {
		return useType;
	}

	@JsonProperty("use_type")
	public void setUseType(String useType) {
		this.useType = useType;
	}

	public MessaggioCaricoRipartizioneDTO withUseType(String useType) {
		this.useType = useType;
		return this;
	}

	@JsonProperty("work_code_type")
	public String getWorkCodeType() {
		return workCodeType;
	}

	@JsonProperty("work_code_type")
	public void setWorkCodeType(String workCodeType) {
		this.workCodeType = workCodeType;
	}

	public MessaggioCaricoRipartizioneDTO withWorkCodeType(String workCodeType) {
		this.workCodeType = workCodeType;
		return this;
	}

	@JsonProperty("year")
	public String getYear() {
		return year;
	}

	@JsonProperty("year")
	public void setYear(String year) {
		this.year = year;
	}

	public MessaggioCaricoRipartizioneDTO withYear(String year) {
		this.year = year;
		return this;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	public MessaggioCaricoRipartizioneDTO withAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
		return this;
	}

}
