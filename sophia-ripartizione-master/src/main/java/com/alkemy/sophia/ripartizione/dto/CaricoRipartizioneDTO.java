
package com.alkemy.sophia.ripartizione.dto;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "dataFattura",
    "idInvoice",
    "pathOutput",
    "society",
    "valoreDem",
    "valoreDrm"
})
public class CaricoRipartizioneDTO {

    @JsonProperty("dateOfPertinence")
    private String dataFattura;
    @JsonProperty("idInvoice")
    private Long numeroFattura;
    @JsonProperty("pathS3648")
    private String pathOutput;
    @JsonProperty("society")
    private String society;
    @JsonProperty("valoreDem")
    private BigDecimal valoreDem;
    @JsonProperty("valoreDrm")
    private BigDecimal valoreDrm;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

     public String getDataFattura() {
        return dataFattura;
    }

     public void setDataFattura(String dataFattura) {
        this.dataFattura = dataFattura;
    }

    public CaricoRipartizioneDTO withDataFattura(String dataFattura) {
        this.dataFattura = dataFattura;
        return this;
    }


    public Long getNumeroFattura() {
        return numeroFattura;
    }


    public void setNumeroFattura(Long numeroFattura) {
        this.numeroFattura = numeroFattura;
    }

    public CaricoRipartizioneDTO withNumeroFattura(Long numeroFattura) {
        this.numeroFattura = numeroFattura;
        return this;
    }

    public String getPathOutput() {
        return pathOutput;
    }

    public void setPathOutput(String pathOutput) {
        this.pathOutput = pathOutput;
    }

    public CaricoRipartizioneDTO withPathOutput(String pathOutput) {
        this.pathOutput = pathOutput;
        return this;
    }

    public String getSociety() {
        return society;
    }

    @JsonProperty("society")
    public void setSociety(String society) {
        this.society = society;
    }

    public CaricoRipartizioneDTO withSociety(String society) {
        this.society = society;
        return this;
    }

    public BigDecimal getValoreDem() {
        return valoreDem;
    }

    public void setValoreDem(BigDecimal valoreDem) {
        this.valoreDem = valoreDem;
    }

    public CaricoRipartizioneDTO withValoreDem(BigDecimal valoreDem) {
        this.valoreDem = valoreDem;
        return this;
    }

    public BigDecimal getValoreDrm() {
        return valoreDrm;
    }

    public void setValoreDrm(BigDecimal valoreDrm) {
        this.valoreDrm = valoreDrm;
    }

    public CaricoRipartizioneDTO withValoreDrm(BigDecimal valoreDrm) {
        this.valoreDrm = valoreDrm;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public CaricoRipartizioneDTO withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

}
