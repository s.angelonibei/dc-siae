package com.alkemy.sophia.ripartizione.config;


import com.alkemy.sophia.ripartizione.aws.S3;
import com.alkemy.sophia.ripartizione.aws.SQS;
import com.alkemy.sophia.ripartizione.aws.sqs.McmdbMessageDeduplicator;
import com.alkemy.sophia.ripartizione.aws.sqs.MessageDeduplicator;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.AmazonSQSAsyncClientBuilder;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.cloud.aws.messaging.config.QueueMessageHandlerFactory;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.cloud.aws.messaging.listener.QueueMessageHandler;
import org.springframework.cloud.aws.messaging.listener.SimpleMessageListenerContainer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories
@ConfigurationProperties(ignoreUnknownFields =  true, ignoreInvalidFields = true)
public class Config {


	@Bean
	public S3 s3ServiceNew(@Value("${cloud.aws.credentials.pathConfigFile}") String pathConfigFile,
	                       @Value("${cloud.aws.region.static}") String regionStatic
	) {
		Properties config = new Properties();
		config.put("aws.region", regionStatic);
		config.put("aws.credential", pathConfigFile);
		S3 s3 = new S3(config);
		s3.startup();
		return s3;
	}

	@Value("${sqs.endpoint}")
	private String awsSqsMockEndpoint;

/*	@Bean
	@Primary
	public AmazonSQSAsync awsSqsClientMock(){
		return AmazonSQSAsyncClientBuilder.standard()
			.withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials("x", "x")))
			.withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(awsSqsMockEndpoint, "elasticmq"))
			.build();
	}*/
/*

	@Bean
	public SimpleMessageListenerContainer simpleMessageListenerContainer(AmazonSQSAsync amazonSQSAsync, QueueMessageHandler queueMessageHandler) {
		SimpleMessageListenerContainer simpleMessageListenerContainer = new SimpleMessageListenerContainer();
		simpleMessageListenerContainer.setAmazonSqs(amazonSQSAsync);
		simpleMessageListenerContainer.setMessageHandler(queueMessageHandler);
		simpleMessageListenerContainer.setMaxNumberOfMessages(10);
		simpleMessageListenerContainer.setTaskExecutor(threadPoolTaskExecutor());
		return simpleMessageListenerContainer;
	}
*/
/*

	@Bean
	public QueueMessageHandler queueMessageHandler(AmazonSQSAsync amazonSQSAsync) {
		QueueMessageHandlerFactory queueMessageHandlerFactory = new QueueMessageHandlerFactory();
		queueMessageHandlerFactory.setAmazonSqs(amazonSQSAsync);
		QueueMessageHandler queueMessageHandler = queueMessageHandlerFactory.createQueueMessageHandler();
		return queueMessageHandler;
	}
*/

	@Bean
	public ThreadPoolTaskExecutor threadPoolTaskExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(10);
		executor.setMaxPoolSize(10);
		executor.initialize();
		return executor;
	}

	@Bean
	@Qualifier("sqs")
	public SQS getSqs() {
		SQS sqs = new SQS().startup();
		return sqs;
	}

	/*@Bean
	public QueueMessagingTemplate queueMessagingTemplate(
		AmazonSQSAsync amazonSQSAsync) {
		return new QueueMessagingTemplate(amazonSQSAsync);
	}*/

	@Bean
	@Qualifier("messageDeduplicator")
	public McmdbMessageDeduplicator getMessageDeduplicator() {
		return new McmdbMessageDeduplicator(dataSource());
	}

	@Bean
	public JPAQueryFactory jpaQueryFactory(EntityManager em) {
		return new JPAQueryFactory(em);
	}

	@Bean(name = "mcmdbDataSource")
	@ConfigurationProperties(prefix = "spring.datasource")
	@Primary
	public DataSource dataSource() {
		return DataSourceBuilder.create().build();
	}

	@Bean(name = "entityManagerFactory")
	@Primary
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(
		EntityManagerFactoryBuilder builder,
		@Qualifier("mcmdbDataSource") DataSource dataSource) {
		return builder
			.dataSource(dataSource)
			.packages("com.alkemy.sophia.ripartizione.entity")
			.persistenceUnit("mcmdb")
			.build();
	}

	@Bean(name = "transactionManager")
	@Primary
	public PlatformTransactionManager transactionManager(
		@Qualifier("entityManagerFactory") EntityManagerFactory entityManagerFactory) {
		return new JpaTransactionManager(entityManagerFactory);
	}

}
