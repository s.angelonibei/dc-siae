package com.alkemy.sophia.ripartizione;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.ServerSocket;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

import javax.sql.DataSource;

import com.alkemy.sophia.ripartizione.aws.S3;
import com.alkemy.sophia.ripartizione.aws.SQS;
import com.alkemy.sophia.ripartizione.aws.sqs.McmdbMessageDeduplicator;
import com.alkemy.sophia.ripartizione.aws.sqs.SqsMessageHelper;
import com.alkemy.sophia.ripartizione.aws.sqs.SqsMessagePump;
import com.alkemy.sophia.ripartizione.client.RestClient;
import com.alkemy.sophia.ripartizione.dao.McmdbDAO;
import com.alkemy.sophia.ripartizione.dto.CaricoRipartizioneDTO;
import com.alkemy.sophia.ripartizione.dto.MessaggioCaricoRipartizioneDTO;
import com.alkemy.sophia.ripartizione.model.DsrAmount;
import com.alkemy.sophia.ripartizione.model.DsrInfo;
import com.alkemytech.sophia.commons.util.GsonUtils;
import com.amazonaws.util.StringUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.QuoteMode;
import org.jooq.DSLContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.google.common.base.Strings;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.aws.messaging.config.annotation.EnableSqs;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;


@SpringBootApplication
@EnableScheduling
@EnableJpaRepositories
@EnableConfigurationProperties
@EnableSqs
public class RipartizioneService {

	private static final Logger logger = LoggerFactory.getLogger(RipartizioneService.class);

	@Autowired
	private Environment env;

	@Autowired
	@Qualifier("sqs")
	private SQS sqs;

	@Autowired
	private S3 s3;

	@Autowired
	private DSLContext dsl;

	@Autowired
	private McmdbMessageDeduplicator messageDeduplicator;

	@Autowired
	private DataSource mcmdbDS;

	@Autowired
	@Value("${listenPort:9090}")
	private int listenPort ;

	@Autowired
	private McmdbDAO dao;

	@Autowired
	private SqsMessagePump sqsMessagePump;

	/**
	 * Process all queued DSRs
	 */
	public void processDSRs(MessaggioCaricoRipartizioneDTO message, JsonObject output) throws Exception {

		// load Hive JDBC driver class
//		Class.forName(env.getProperty("hive.jdbc.driver"));

		// parse input CSV file
		final List<DsrAmount> dsrAmounts = new ArrayList<>();
		for (CaricoRipartizioneDTO carico : message.getCarichiRipartizione()) {
			if ("SIAE".equals(carico.getSociety())) {
				final DsrAmount dsrAmount = new DsrAmount();
				int i = 0;
				dsrAmount.setIdDsr(message.getIdDsr());
				dsrAmount.setAmountDem(carico.getValoreDem());
				dsrAmount.setAmountDrm(carico.getValoreDrm());
				dsrAmount.setBillDate(carico.getDataFattura());
				dsrAmount.setInvoiceCode(carico.getNumeroFattura().toString());
				dsrAmount.setOutputPath(carico.getPathOutput());
				// controllare se questa sia la data oppure il periodo della fattura
				dsrAmounts.add(dsrAmount);
			}
		}
		logger.debug("processDSRs: dsrAmounts {}", dsrAmounts);


		// connect to db
		try (final Connection mcmdb = mcmdbDS.getConnection()) {
			logger.debug("processDSRs: connected to {} {}", mcmdb.getMetaData()
					.getDatabaseProductName(), mcmdb.getMetaData().getURL());
			// DAO
//			final McmdbDAO mcmdbDAO = new McmdbDAO(mcmdb);

			// loop on DSRs
			for (DsrAmount dsrAmount : dsrAmounts) {
				logger.debug("processDSRs: processing {}", dsrAmount.getIdDsr());

				String dspCodeReclami = dao.obtainOriginalDspCodeByIdDsrReclamo(message.getIdDsr());


				// DSR metadata
				final DsrInfo metadata = new DsrInfo()
					.setCountry(message.getCountry())
					.setDspCode(null != dspCodeReclami ? dspCodeReclami : message.getDspCode())
					.setIdDsr(message.getIdDsr())
					.setIdDsp(message.getDsp())
					.setIdUtilizationType(message.getIdUtilizationType())
					.setFtpSourcePath(message.getDsp())
					.setOffering(message.getBusinessOffer())
					.setMonth(message.getMonth())
					.setYear(message.getYear());



				logger.debug("processDSRs: metadata {}", metadata);
				if (null == metadata) { // dsr not found
					continue;
				}

				// check whether ccid total value exists
/*				if (!existsTotalValue(mcmdb, dsrAmount.getIdDsr())) {
					logger.debug("processDSRs: ccid total value does not exist");
					continue;
				}*/

				// check whether s3 file already exists
				if ("true".equalsIgnoreCase(env.getProperty("overwriteS3Files"))) {
					logger.debug("processDSRs: overwriting s3 file");
				} else if (s3FileExists(metadata, dsrAmount)) {
					logger.debug("processDSRs: s3 file already exists");
					continue;
				}

				// temporary output file
				final File file = new File(dsrAmount.getIdDsr() + "_" + dsrAmount.getInvoiceCode() + ".csv");
				logger.debug("processDSRs: created temporary file {}", file);

				// create csv file from hive query
				logger.debug("processDSRs: creating CSV file");
				boolean created = false;
				if ("true".equalsIgnoreCase(env.getProperty("downloadFromS3"))) {
					created = createCsvFileNoHive(dsrAmount, metadata, file);
				} else {
					created = createCsvFile(dsrAmount, metadata, file);
				}
				logger.debug("processDSRs: CSV file size {}", file.length());
				if (created) {

					// upload file to S3
					if (file.length() > 0L) {
						logger.debug("processDSRs: uploading to S3");
						if (uploadCsvFile(metadata, dsrAmount, file)) {

							// add partition to hive table
							logger.debug("processDSRs: creating hive partition");
							addHivePartition(dsrAmount, metadata);

						}
					}

				}

				// delete temporary output file
				file.delete();
				logger.debug("processDSRs: deleted temporary file {}", file);

			}

		}

	}

	private boolean createCsvFile(DsrAmount dsrAmount, DsrInfo metadata, File file) throws Exception {
		if (null == dsrAmount ||
				null == dsrAmount.getAmountDem() ||
				null == dsrAmount.getAmountDrm() ||
				StringUtils.isNullOrEmpty(dsrAmount.getIdDsr()) ||
				StringUtils.isNullOrEmpty(dsrAmount.getBillDate())) {
			logger.warn("createCsvFile: missing mandatory input data");
			return false;
		}
		if (null == metadata ||
				StringUtils.isNullOrEmpty(metadata.getFtpSourcePath()) ||
				StringUtils.isNullOrEmpty(metadata.getYear()) ||
				StringUtils.isNullOrEmpty(metadata.getMonth())) {
			logger.warn("createCsvFile: missing mandatory DSR info");
			return false;
		}
		final RestClient.Result result = RestClient
				.get(env.getProperty("hive.clusterListRestUrl"));
		if (200 == result.getStatusCode()) {
			final JsonObject resultJson = result.getJsonElement().getAsJsonObject();
			logger.debug("createCsvFile: resultJson {}", resultJson);
			if ("OK".equals(resultJson.get("status").getAsString())) {
				final JsonArray activeEmrClusters = resultJson.getAsJsonArray("activeEmrClusters");
				if (null != activeEmrClusters) for (JsonElement activeEmrCluster : activeEmrClusters) {
					final JsonObject clusterJson = (JsonObject) activeEmrCluster;
					logger.debug("createCsvFile: clusterJson {}", clusterJson);
					String hiveJdbcUrl = clusterJson.get("hiveJdbcUrl").getAsString();
					String hiveJdbcUser = clusterJson.get("hiveJdbcUser").getAsString();
					String hiveJdbcPassword = clusterJson.get("hiveJdbcPassword").getAsString();
					hiveJdbcUrl = env.getProperty("hive.jdbc.url", hiveJdbcUrl);
					hiveJdbcUser = env.getProperty("hive.jdbc.user", hiveJdbcUser);
					hiveJdbcPassword = env.getProperty("hive.jdbc.password", hiveJdbcPassword);
					logger.debug("createCsvFile: connecting to {}...", hiveJdbcUrl);
					try (final Connection connection = DriverManager
							.getConnection(hiveJdbcUrl, hiveJdbcUser, hiveJdbcPassword)) {
						logger.debug("createCsvFile: connected to {}", hiveJdbcUrl);
						try (final Statement statement = connection.createStatement()) {
							final String hql = env.getProperty("hive.statement")
									.replace(":dem", dsrAmount.getAmountDem().toString())
									.replace(":drm", dsrAmount.getAmountDrm().toString())
									.replace(":date", dsrAmount.getBillDate())
									.replace(":dsp", metadata.getFtpSourcePath())
									.replace(":year", metadata.getYear())
									.replace(":month", metadata.getMonth())
									.replace(":dsr", dsrAmount.getIdDsr());
							logger.debug("createCsvFile: executing hql {}", hql);
							try (ResultSet resultSet = statement.executeQuery(hql)) {
								saveCsvFile(resultSet, metadata, file);
							}
							return true;
						}
					} catch (Exception e) {
						logger.error("createCsvFile", e);
					}
				}
			}
		}
		return false;
	}

	private boolean createCsvFileNoHive(DsrAmount dsrAmount, DsrInfo metadata, File file) {
		if (null == dsrAmount ||
				null == dsrAmount.getAmountDem() ||
				null == dsrAmount.getAmountDrm() ||
				StringUtils.isNullOrEmpty(dsrAmount.getIdDsr()) ||
				StringUtils.isNullOrEmpty(dsrAmount.getBillDate())) {
			logger.warn("createCsvFileNoHive: missing mandatory input data");
			return false;
		}
		if (null == metadata ||
				StringUtils.isNullOrEmpty(metadata.getFtpSourcePath()) ||
				StringUtils.isNullOrEmpty(metadata.getYear()) ||
				StringUtils.isNullOrEmpty(metadata.getMonth())) {
			logger.warn("createCsvFileNoHive: missing mandatory DSR info");
			return false;
		}
		// input file uri
		final S3.Url s3Url = new S3.Url(env.getProperty("s3uri.input", "")
			.replace("{dsr}", dsrAmount.getIdDsr())
			.replace("{dsp}", metadata.getFtpSourcePath())
			.replace("{year}", metadata.getYear())
			.replace("{month}", metadata.getMonth()));
		logger.debug("createCsvFileNoHive s3Uri: {}", s3Url);
		final String inputS3uriPartsExtension = env
				.getProperty("input.s3uri.parts.extension", ".csv").toLowerCase();
		final String inputS3uriPartsPrefix = String.format("%s/%s", s3Url,
				env.getProperty("input.s3uri.parts.prefix", "part-")).toLowerCase().replaceFirst("s3://siae-sophia-datalake/","");
		// list directory
		final List<S3ObjectSummary> s3Listing = s3.listObjects(s3Url);
		if (null != s3Listing) {
			logger.info("createCsvFileNoHive: s3 parts number {}", s3Listing.size());
			final AtomicReference<BigDecimal> sumDem;
			final AtomicReference<BigDecimal> sumDrm;
			if (!Strings.isNullOrEmpty(env.getProperty("skipWorkCodePrefixes"))) {
				sumDem = new AtomicReference<>(BigDecimal.ZERO);
				sumDrm = new AtomicReference<>(BigDecimal.ZERO);
				try {
					// loop on part(s)
					for (final S3ObjectSummary s3Summary : s3Listing) {
						// parse s3 part
						final String s3Key = URLDecoder.decode(s3Summary.getKey(), "UTF-8").toLowerCase();
						logger.debug("createCsvFileNoHive: s3Key {}", s3Key);
						if (s3Key.startsWith(inputS3uriPartsPrefix) && s3Key.endsWith(inputS3uriPartsExtension)) {
							logger.info("createCsvFileNoHive: parsing {}", s3Summary);
							final S3Object s3Object = s3.getObject(new S3.Url(s3Summary.getBucketName(),s3Summary.getKey()));
							if (null == s3Object) {
								throw new IOException("unable to download s3://" + s3Summary.getBucketName()
										+ "/" + s3Summary.getKey() + " (" + s3Summary.getSize() + ")");
							}
							try (final InputStream in = s3Object.getObjectContent();
									final InputStreamReader reader = new InputStreamReader(in, StandardCharsets.UTF_8);
									final CSVParser parser = new CSVParser(reader, CSVFormat.newFormat(';'))) {
								sumCsvFileNoHive(parser, dsrAmount, metadata, sumDem, sumDrm);
							}
						}
					}
				} catch (IOException e) {
					logger.error("createCsvFileNoHive", e);
				}
			} else {
				sumDem = new AtomicReference<>(BigDecimal.ONE);
				sumDrm = new AtomicReference<>(BigDecimal.ONE);
			}
			logger.debug("createCsvFileNoHive: sumDem {}", sumDem.get());
			logger.debug("createCsvFileNoHive: sumDrm {}", sumDrm.get());
			final BigDecimal demScale;
			final BigDecimal drmScale;
			if (BigDecimal.ZERO.equals(sumDem.get())) {
				logger.warn("createCsvFileNoHive: DEM sum is zero");
				demScale = BigDecimal.ONE;
			} else {
				demScale = BigDecimal.ONE
					.divide(sumDem.get(), 20, RoundingMode.HALF_UP);
			}
			if (BigDecimal.ZERO.equals(sumDrm.get())) {
				logger.warn("createCsvFileNoHive: DRM sum is zero");
				drmScale = BigDecimal.ONE;
			} else {
				drmScale = BigDecimal.ONE
					.divide(sumDrm.get(), 20, RoundingMode.HALF_UP);
			}
			logger.debug("createCsvFileNoHive: demScale {}", demScale);
			logger.debug("createCsvFileNoHive: drmScale {}", drmScale);
			// create csv printer
			try (final FileWriter writer = new FileWriter(file, false);
					final CSVPrinter printer = new CSVPrinter(new BufferedWriter(writer),
						CSVFormat.EXCEL.withDelimiter(';').withQuoteMode(QuoteMode.MINIMAL))) {
				// loop on part(s)
				for (final S3ObjectSummary s3Summary : s3Listing) {
					// parse s3 part
					final String s3Key = URLDecoder.decode(s3Summary.getKey(), "UTF-8").toLowerCase();
					logger.debug("createCsvFileNoHive: s3Key {}", s3Key);
					if (s3Key.startsWith(inputS3uriPartsPrefix) && s3Key.endsWith(inputS3uriPartsExtension)) {
						logger.info("createCsvFileNoHive: parsing {}", s3Summary);
						final S3Object s3Object = s3.getObject(new S3.Url("s3://" + s3Summary.getBucketName() + "/" + s3Summary.getKey()));
						if (null == s3Object) {
							throw new IOException("unable to download s3://" + s3Summary.getBucketName()
									+ "/" + s3Summary.getKey() + " (" + s3Summary.getSize() + ")");
						}
						try (final InputStream in = s3Object.getObjectContent();
								final InputStreamReader reader = new InputStreamReader(in, StandardCharsets.UTF_8);
								final CSVParser parser = new CSVParser(reader, CSVFormat.newFormat(';'))) {
							saveCsvFileNoHive(parser, dsrAmount, metadata, demScale, drmScale, printer);
						}
					}
				}
				return true;
			} catch (IOException e) {
				logger.error("createCsvFileNoHive", e);
			}
		}
		return false;
	}

	private BigDecimal toBigDecimal(String value) {
		try {
			return (null == value) ? null : new BigDecimal(value.replace(',', '.'));
		} catch (NumberFormatException e) {
			return BigDecimal.ZERO;
		}
	}

	private String fixNumber(String value) {
		try {
			return fixNumber(toBigDecimal(value));
		} catch (NumberFormatException e) {}
		return value;
	}

	private String fixNumber(BigDecimal number) {
		if (null == number) {
			return null;
		}
		String value = number.toPlainString();
		value = value.replace('.', ',');
		int idx = value.indexOf(",");
		if (-1 != idx) {
			while (value.endsWith("0")) {
				value = value.substring(0, value.length() - 1);
			}
		}
		if (value.endsWith(",")) {
			value = value.substring(0, value.length() - 1);
		}
		idx = value.indexOf(",0000000000");
		if (-1 != idx) {
			value = value.substring(0, idx);
		}
		return value;
	}

	private void saveCsvFile(ResultSet resultSet, DsrInfo metadata, File file) throws Exception {
		try (final FileWriter writer = new FileWriter(file, false)) {
			try (final CSVPrinter printer = new CSVPrinter(new BufferedWriter(writer),
					CSVFormat.EXCEL.withDelimiter(';').withQuoteMode(QuoteMode.MINIMAL))) {
				final int cols = resultSet.getMetaData().getColumnCount();
				final String[] values = new String[cols];
				final BigDecimal siadaScale = new BigDecimal(10000);
				final int idxCompensoDemNetto = Integer.parseInt(env.getProperty("hive.column.compenso_dem_netto")) - 1;
				final int idxCompensoDrmNetto = Integer.parseInt(env.getProperty("hive.column.compenso_drm_netto")) - 1;
				final int idxNumeratoreQuotaDrm = Integer.parseInt(env.getProperty("hive.column.numeratore_quota_drm")) - 1;
				final int idxDenominatoreQuotaDrm = Integer.parseInt(env.getProperty("hive.column.denominatore_quota_drm")) - 1;
				final int idxPercentualeDiClaim = Integer.parseInt(env.getProperty("hive.column.percentuale_di_claim")) - 1;
				logger.debug("saveCsvFile: output columns number: {}", cols);
				int rows = 0;
				while (resultSet.next()) {
					for (int i = 0; i < cols; i ++) {
						values[i] = resultSet.getString(i+1);
					}
					// compensi netti
					values[idxCompensoDemNetto] = fixNumber(values[idxCompensoDemNetto]);
					values[idxCompensoDrmNetto] = fixNumber(values[idxCompensoDrmNetto]);
					// numeratore denominatore drm
					final BigDecimal numeratoreQuotaDrm = toBigDecimal(values[idxNumeratoreQuotaDrm]);
					final BigDecimal denominatoreQuotaDrm = toBigDecimal(values[idxDenominatoreQuotaDrm]);
					final BigDecimal percentualeQuotaDrm = numeratoreQuotaDrm
							.divide(denominatoreQuotaDrm, 20, RoundingMode.HALF_UP);
					values[idxNumeratoreQuotaDrm] = fixNumber(percentualeQuotaDrm.multiply(siadaScale));
					values[idxDenominatoreQuotaDrm] = fixNumber(siadaScale);
					// percentuale claim
					final BigDecimal percentualeDiClaim = toBigDecimal(values[idxPercentualeDiClaim]);
					values[idxPercentualeDiClaim] = fixNumber(percentualeDiClaim.multiply(siadaScale));
					// print columns
					for (int i = 0; i < cols; i ++) {
						printer.print(values[i]);
					}
					printer.println();
					rows ++;
					if (0 == (rows % 1000)) {
						logger.debug("saveCsvFile: rows {}", rows);
					}
				}
				// footer with number of rows
				if (rows > 0) {
					printer.print(rows);
					printer.println();
					logger.debug("saveCsvFile: rows {}", rows);
				}
			}
		}
	}

	private void sumCsvFileNoHive(CSVParser parser, DsrAmount dsrAmount, DsrInfo metadata, AtomicReference<BigDecimal> sumDem, AtomicReference<BigDecimal> sumDrm) throws IOException {
		final String[] skipWorkCodePrefixes = env.getProperty("skipWorkCodePrefixes","@").split(",");
		final int idxCodiceOpera = Integer.parseInt(env.getProperty("hive.column.codice_opera")) - 1;
		final int idxCompensoDemNetto = Integer.parseInt(env.getProperty("hive.column.compenso_dem_netto")) - 1;
		final int idxCompensoDrmNetto = Integer.parseInt(env.getProperty("hive.column.compenso_drm_netto")) - 1;
		int rows = 0;
		for (CSVRecord record : parser) {
			final String codiceOpera = record.get(idxCodiceOpera);
			if (null != codiceOpera) {
				boolean skipWorkCode = false;
				for (String skipWorkCodePrefix : skipWorkCodePrefixes) {
					if (codiceOpera.startsWith(skipWorkCodePrefix)) {
						skipWorkCode = true;
						break;
					}
				}
				if (!skipWorkCode) {
					sumDem.set(sumDem.get().add(toBigDecimal(record.get(idxCompensoDemNetto))));
					sumDrm.set(sumDrm.get().add(toBigDecimal(record.get(idxCompensoDrmNetto))));
				}
			}
			rows ++;
			if (0 == (rows % 1000)) {
				logger.debug("sumCsvFile: rows {}", rows);
			}
		}
		logger.debug("sumCsvFile: rows {}", rows);
		logger.debug("sumCsvFile: sumDem {}", sumDem.get());
		logger.debug("sumCsvFile: sumDrm {}", sumDrm.get());
	}

	private void saveCsvFileNoHive(CSVParser parser, DsrAmount dsrAmount, DsrInfo metadata, BigDecimal demScale, BigDecimal drmScale, CSVPrinter printer) throws IOException {
		final String[] values = new String[50];
		final BigDecimal siadaScale = new BigDecimal(10000);
		final int idxMeseIncassoFattura = Integer.parseInt(env.getProperty("hive.column.mese_incasso_fattura")) - 1;
		final int idxCompensoDemNetto = Integer.parseInt(env.getProperty("hive.column.compenso_dem_netto")) - 1;
		final int idxCompensoDrmNetto = Integer.parseInt(env.getProperty("hive.column.compenso_drm_netto")) - 1;
		final int idxNumeratoreQuotaDrm = Integer.parseInt(env.getProperty("hive.column.numeratore_quota_drm")) - 1;
		final int idxDenominatoreQuotaDrm = Integer.parseInt(env.getProperty("hive.column.denominatore_quota_drm")) - 1;
		final int idxPercentualeDiClaim = Integer.parseInt(env.getProperty("hive.column.percentuale_di_claim")) - 1;
		final int maxCols = Integer.parseInt(env.getProperty("hive.column.count", "30"));
		int rows = 0;
		for (CSVRecord record : parser) {
			int length = 0;
			for (String value : record) {
				values[length ++] = value;
			}
			length = Math.min(length, maxCols);

			//hack per reclami
			values[0] = metadata.getDspCode();
			// mese incasso fattura
			values[idxMeseIncassoFattura] = dsrAmount.getBillDate();
			// compensi netti
			values[idxCompensoDemNetto] = fixNumber(toBigDecimal(values[idxCompensoDemNetto])
					.multiply(dsrAmount.getAmountDem()).multiply(demScale));
			values[idxCompensoDrmNetto] = fixNumber(toBigDecimal(values[idxCompensoDrmNetto])
					.multiply(dsrAmount.getAmountDrm()).multiply(drmScale));
			// numeratore denominatore drm
			final BigDecimal numeratoreQuotaDrm = toBigDecimal(values[idxNumeratoreQuotaDrm]);
			final BigDecimal denominatoreQuotaDrm = toBigDecimal(values[idxDenominatoreQuotaDrm]);
			final BigDecimal percentualeQuotaDrm = numeratoreQuotaDrm
					.divide(denominatoreQuotaDrm, 20, RoundingMode.HALF_UP);
			values[idxNumeratoreQuotaDrm] = fixNumber(percentualeQuotaDrm.multiply(siadaScale));
			values[idxDenominatoreQuotaDrm] = fixNumber(siadaScale);
			// percentuale claim
			final BigDecimal percentualeDiClaim = toBigDecimal(values[idxPercentualeDiClaim]);
			values[idxPercentualeDiClaim] = fixNumber(percentualeDiClaim.multiply(siadaScale));
			// print columns
			for (int i = 0; i < length; i ++) {
				printer.print(values[i]);
			}
			printer.println();
			rows ++;
			if (0 == (rows % 1000)) {
				logger.debug("saveCsvFile: rows {}", rows);
			}
		}
		// footer with number of rows
		if (rows > 0) {
			printer.print(rows);
			printer.println();
			logger.debug("saveCsvFile: rows {}", rows);
		}
	}

	private boolean uploadCsvFile(DsrInfo metadata, DsrAmount dsrAmount, File file) throws Exception {
		final S3.Url s3Uri = new S3.Url(dsrAmount.getOutputPath());
		logger.debug("uploadCsvFile s3Uri: {}", s3Uri);
		if (s3.upload(s3Uri, file)) {
			logger.debug("uploadCsvFile: s3Uri {}", s3Uri);
			System.out.println(s3Uri);
			return true;
		}
		return false;
	}

	private boolean s3FileExists(DsrInfo metadata, DsrAmount dsrAmount) throws Exception {
		final S3.Url s3Uri = new S3.Url(dsrAmount.getOutputPath());
		return s3.doesObjectExist(s3Uri);
	}

	private boolean addHivePartition(DsrAmount dsrAmount, DsrInfo metadata) throws IOException {
		if (StringUtils.isNullOrEmpty(dsrAmount.getIdDsr()) ||
				StringUtils.isNullOrEmpty(metadata.getFtpSourcePath()) ||
				StringUtils.isNullOrEmpty(metadata.getYear()) ||
				StringUtils.isNullOrEmpty(metadata.getMonth())) {
			return false;
		}
		final RestClient.Result result = RestClient
				.get(env.getProperty("hive.clusterListRestUrl"));
		if (200 == result.getStatusCode()) {
			final JsonObject resultJson = result.getJsonElement().getAsJsonObject();
			logger.debug("addHivePartition: resultJson {}", resultJson);
			if ("OK".equals(resultJson.get("status").getAsString())) {
				final JsonArray activeEmrClusters = resultJson.getAsJsonArray("activeEmrClusters");
				if (null != activeEmrClusters) for (JsonElement activeEmrCluster : activeEmrClusters) {
					final JsonObject clusterJson = (JsonObject) activeEmrCluster;
					logger.debug("addHivePartition: clusterJson {}", clusterJson);
					final String hiveJdbcUrl = clusterJson.get("hiveJdbcUrl").getAsString();
					final String hiveJdbcUser = clusterJson.get("hiveJdbcUser").getAsString();
					final String hiveJdbcPassword = clusterJson.get("hiveJdbcPassword").getAsString();
					logger.debug("addHivePartition: connecting to {}...", hiveJdbcUrl);
					try (final Connection connection = DriverManager
							.getConnection(hiveJdbcUrl, hiveJdbcUser, hiveJdbcPassword)) {
						logger.debug("addHivePartition: connected to {}", hiveJdbcUrl);
						try (final Statement statement = connection.createStatement()) {
							final String hql = env.getProperty("hive.alter_table")
									.replace(":dsp", metadata.getFtpSourcePath())
									.replace(":year", metadata.getYear())
									.replace(":month", metadata.getMonth())
									.replace(":dsr", dsrAmount.getIdDsr());
							logger.debug("addHivePartition: executing hql {}", hql);
							final int count = statement.executeUpdate(hql);
							logger.debug("addHivePartition: hql result {}", count);
							return count >= 0;
						}
					} catch (Exception e) {
						logger.error("addHivePartition", e);
					}
				}
			}
		}
		return false;
	}

/*	private boolean existsTotalValue(Connection connection, String idDsr) {
		try (Statement statement = connection.createStatement()) {
			final String sql = new StringBuilder()
				.append("select TOTAL_VALUE")
				.append(" from CCID_METADATA")
				.append(" where ID_DSR = '").append(idDsr)
				.append("'")
				.toString();
			try (final ResultSet resultSet = statement.executeQuery(sql)) {
				if (resultSet.next()) {
					final String totalValue = resultSet.getString("TOTAL_VALUE");
					if (!StringUtils.isNullOrEmpty(totalValue)) {
						return new BigDecimal(totalValue.replace(',', '.')).compareTo(BigDecimal.ZERO) > 0;
					}
				}
			}
		} catch (SQLException e) {
			logger.error("existsTotalValue", e);
		}
		return false;
	}*/

	public static void main(String[] args) throws Exception {
		SpringApplication.run(RipartizioneService.class, args);
	}

	/**
	 * Process all queued DSRs
	 */
	@Scheduled(cron = "${ripartizione.cron:* * * * * *}")
	public void processDSRs() throws Exception {

		try (final ServerSocket socket = new ServerSocket(listenPort)) {
			logger.info("processDSRs: listening on {}", socket.getLocalSocketAddress());

			//final SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs);
			sqsMessagePump.pollingLoop(messageDeduplicator, new SqsMessagePump.Consumer() {

				private JsonObject output;
				private JsonObject error;

				@Override
				public JsonObject getStartedMessagePayload(JsonObject message) {
					output = null;
					error = null;
					return SqsMessageHelper.formatContext();
				}

				@Override
				public boolean consumeMessage(JsonObject message) {
					try {
						output = new JsonObject();
						processMessage(message, output);
						error = null;
						return true;
					} catch (Exception e) {
						output = null;
						error = SqsMessageHelper.formatError(e);
						return false;
					}
				}

				@Override
				public JsonObject getCompletedMessagePayload(JsonObject message) {
					return output;
				}

				@Override
				public JsonObject getFailedMessagePayload(JsonObject message) {
					return error;
				}

			});

		}
	}

	private void processMessage(JsonObject message, JsonObject output) {
		MessaggioCaricoRipartizioneDTO dto;
		logger.info(String.format("Received message: %s", GsonUtils.toJson(message)));
		try {
			dto = new ObjectMapper().readValue(message.getAsJsonObject("body").toString(), MessaggioCaricoRipartizioneDTO.class);
			processDSRs(dto,output);
		} catch (Exception e) {
			logger.error("Error processing message",e);
		}
	}


}
