/*
 * @Autor A.D.C.
 */

/*
 * @Autor A.D.C.
 */

/*
 * @Autor A.D.C.
 */

package it.siae.sophia.ms.pricelist.aws;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;


public class S3 {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	public static class Url {
		
		public final String bucket;
		public final String key;
		
		public Url(String url) {
			super();
			if (!url.startsWith("s3://")) {
				throw new IllegalArgumentException(String
						.format("invalid url \"%s\"", url));
			}
			this.bucket = url.substring(5, url.indexOf('/', 5));
			this.key = url.substring(1 + url.indexOf('/', 5));
		}

		public Url(String bucket, String key) {
			super();
			this.bucket = bucket;
			this.key = key;
		}
		
		@Override
		public int hashCode() {
			int result = 31 + (null == bucket ? 0 : bucket.hashCode());
			return 31 * result + (null == key ? 0 : key.hashCode());
		}

		@Override
		public boolean equals(Object object) {
			if (this == object)
				return true;
			if (null == object)
				return false;
			if (getClass() != object.getClass())
				return false;
			final Url other = (Url) object;
			if (null == bucket) {
				if (null != other.bucket)
					return false;
			} else if (!bucket.equals(other.bucket))
				return false;
			if (null == key) {
				if (null != other.key)
					return false;
			} else if (!key.equals(other.key))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return String.format("s3://%s/%s", bucket, key);
		}

	}
	
	private final Properties configuration;

	@Inject
	public S3(@Named("configuration") Properties configuration) {
		super();
		this.configuration = configuration;
	}


}
