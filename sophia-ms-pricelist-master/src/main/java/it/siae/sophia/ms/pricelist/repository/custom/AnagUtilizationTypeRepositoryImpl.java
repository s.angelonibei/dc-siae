package it.siae.sophia.ms.pricelist.repository.custom;

import it.siae.sophia.ms.pricelist.entity.AnagUtilizationType;
import it.siae.sophia.ms.pricelist.query.PricelistProperites;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.util.ArrayList;

@Repository
public class AnagUtilizationTypeRepositoryImpl implements AnagUtilizationTypeRepositoryCustom {

    @Autowired
    private PricelistProperites pricelistProperites;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private final Logger logger = LoggerFactory.getLogger(DroolsPricingRepositoryImpl.class);

    @Override
    public AnagUtilizationType idUtilizationTypeDecoded(String IdUtilizzationType) {
        AnagUtilizationType ut = null ;
        logger.info("Request to decode name of utilizzation type : {}");
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        StringBuffer sb = new StringBuffer();
        sb.append(pricelistProperites.getAppends().getNames());
        namedParameters.addValue("type", IdUtilizzationType);
        try {
            logger.info("decode name of utilizzation type query {} - namedParameters {}", sb.toString(), namedParameters.getValues());
            ut
                    = this.namedParameterJdbcTemplate.queryForObject(sb.toString(),
                    namedParameters, (ResultSet rs, int rowNum) -> {
                        AnagUtilizationType dto = new AnagUtilizationType();
                        dto.setName(rs.getString("names"));
                        dto.setDescription(rs.getString("description"));
                        return dto;
                    });

        } catch (Exception e) {
            logger.info("searchAll, Exception " + e.getMessage());
        }
        return ut;
    }
}
