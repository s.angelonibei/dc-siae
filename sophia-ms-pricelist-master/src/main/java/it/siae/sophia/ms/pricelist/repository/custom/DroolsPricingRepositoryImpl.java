package it.siae.sophia.ms.pricelist.repository.custom;

import it.siae.sophia.ms.pricelist.dto.StringListDTO;
import it.siae.sophia.ms.pricelist.entity.DroolsPricing;
import it.siae.sophia.ms.pricelist.query.PricelistProperites;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Array;
import java.sql.ResultSet;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class DroolsPricingRepositoryImpl implements DroolsPricingRepositoryCustom {

    private final Logger logger = LoggerFactory.getLogger(DroolsPricingRepositoryImpl.class);

    @Autowired
    private PricelistProperites pricelistProperites;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public ArrayList findByDspAndIdUtilizationTypeAndValidFromAndValidTo(DroolsPricing price) {
        logger.info("Request to findByDspAndIdUtilizationTypeAndValidFromAndValidTo : {}");
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        StringBuffer sb = new StringBuffer();
        sb.append(pricelistProperites.getAppends().getSelectIdDsp());
        ArrayList c = new ArrayList();
        if (price.getIddsp() != null) {
            sb.append(" ").append(pricelistProperites.getAppends().getIddsp());
            namedParameters.addValue("dsp", price.getIddsp());
        }
        if (price.getIdUtilizationType() != null) {
            sb.append(" ").append(pricelistProperites.getAppends().getUtilizzationType());
            namedParameters.addValue("tipoUtilizzo", price.getIdUtilizationType());
        }

        sb.append(" AND (");
        sb.append(" ").append(pricelistProperites.getAppends().getDataInizioFineValidita());
        namedParameters.addValue("dataInizioValidita", price.getValidFrom().toString().substring(0, 10));
        namedParameters.addValue("dataFineValidita", price.getValidFrom().toString().substring(0, 10));
        sb.append(" )");

        try {

            logger.info("findByDspAndIdUtilizationTypeAndValidFromAndValidTo query {} - namedParameters {}", sb.toString(), namedParameters.getValues());
            this.namedParameterJdbcTemplate.queryForObject(sb.toString(),
                    namedParameters,
                    (ResultSet rs, int rowNum) -> c.add(rs.getLong("ID"))
            );

        } catch (Exception e) {
            logger.info("searchAll, Exception " + e.getMessage());
        }
        return c;
    }

    @Override
    public ArrayList findDuplicate(DroolsPricing price) {
        logger.info("Request to findByDspAndIdUtilizationTypeAndValidFromAndValidTo : {}");
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        StringBuffer sb = new StringBuffer();
        sb.append(pricelistProperites.getAppends().getSelectIdDsp());
        ArrayList c = new ArrayList();
        if (price.getIddsp() != null) {
            sb.append(" ").append(pricelistProperites.getAppends().getIddsp());
            namedParameters.addValue("dsp", price.getIddsp());
        }
        if (price.getIdUtilizationType() != null) {
            sb.append(" ").append(pricelistProperites.getAppends().getUtilizzationType());
            namedParameters.addValue("tipoUtilizzo", price.getIdUtilizationType());
        }

        if (price.getValidFrom() != null) {
            sb.append(" ").append(pricelistProperites.getAppends().getDataInizioValidita());
            namedParameters.addValue("dataInizioValidita", price.getValidFrom().toString().substring(0, 10));
        }

        try {

            logger.info("findDuplicate query {} - namedParameters {}", sb.toString(), namedParameters.getValues());
            this.namedParameterJdbcTemplate.queryForObject(sb.toString(),
                    namedParameters,
                    (ResultSet rs, int rowNum) -> c.add(rs.getLong("ID"))
            );

        } catch (Exception e) {
            logger.info("searchAll, Exception " + e.getMessage());
        }
        return c;
    }

    @Override
    public DroolsPricing findPathExcelPricelistByParams(String dsp, String tipoUtilizzo, String dataInizioValidita, String dataFineValidita) throws ParseException {
        DroolsPricing b = new DroolsPricing();
        logger.info("Request to findPathCsvBlacklistByParams : {}");
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        StringBuffer sb = new StringBuffer();
        sb.append(pricelistProperites.getAppends().getSelect());
        if (dsp != null) {
            sb.append(" ").append(pricelistProperites.getAppends().getIddsp());
            namedParameters.addValue("dsp", dsp);
        }
        if (tipoUtilizzo != null) {
            sb.append(" ").append(pricelistProperites.getAppends().getUtilizzationType());
            namedParameters.addValue("tipoUtilizzo", tipoUtilizzo);
        }
        if (dataInizioValidita != null) {
//            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
//            Date date = formatter.parse(dataInizioValidita.substring(0,10));
//            String d = new SimpleDateFormat("yyyy-MM-dd").format(date);
            sb.append(" ").append(pricelistProperites.getAppends().getDataInizioValidita());
            namedParameters.addValue("dataInizioValidita", dataInizioValidita.substring(0, 10));
            if (dataFineValidita != null) {
                sb.append(" ").append(pricelistProperites.getAppends().getDataFineValidita());
                namedParameters.addValue("dataFineValidita", dataFineValidita.substring(0, 10));
            }
        } else {
            //COSTRUISCO UNA SELECT NELLA WHERE PER CALCOLARE LA DATA MASSIMA PER LE RIGHE OTTENUTE
            sb.append(" ").append(pricelistProperites.getAppends().getDataMaxStart());
            sb.append(" ").append(pricelistProperites.getAppends().getMaxDateFrom());
            if (dsp != null) {
                sb.append(" ").append(pricelistProperites.getAppends().getIddsp());
                namedParameters.addValue("dsp", dsp);
            }
            if (tipoUtilizzo != null) {
                sb.append(" ").append(pricelistProperites.getAppends().getUtilizzationType());
                namedParameters.addValue("tipoUtilizzo", tipoUtilizzo);
            }
            sb.append(" ").append(pricelistProperites.getAppends().getDataMaxEnd());
        }

        try {

            logger.info("findAllByFilter query {} - namedParameters {}", sb.toString(), namedParameters.getValues());
            b
                    = this.namedParameterJdbcTemplate.queryForObject(sb.toString(),
                    namedParameters, (ResultSet rs, int rowNum) -> {
                        DroolsPricing dto = new DroolsPricing();
                        dto.setId(rs.getLong("ID"));
                        dto.setIddsp(rs.getString("IDDSP"));
                        dto.setIdUtilizationType(rs.getString("ID_UTILIZATION_TYPE"));
                        dto.setValidFrom(rs.getTimestamp("VALID_FROM"));
                        dto.setValidTo(rs.getTimestamp("VALID_TO"));
                        dto.setXlsS3Url(rs.getString("XLS_S3_URL"));
                        return dto;
                    });

        } catch (Exception e) {
            logger.info("searchAll, Exception " + e.getMessage());
        }

        return b;
    }
}
