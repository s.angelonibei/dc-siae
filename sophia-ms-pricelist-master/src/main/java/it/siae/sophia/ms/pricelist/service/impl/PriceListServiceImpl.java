package it.siae.sophia.ms.pricelist.service.impl;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.google.gson.Gson;
import it.siae.sophia.ms.pricelist.aws.S3;
import it.siae.sophia.ms.pricelist.dto.PriceListDTO;
import it.siae.sophia.ms.pricelist.dto.StringListDTO;
import it.siae.sophia.ms.pricelist.entity.*;
import it.siae.sophia.ms.pricelist.errors.BadRequestAlertException;
import it.siae.sophia.ms.pricelist.excel.ExcelUtilsServiceImpl;
import it.siae.sophia.ms.pricelist.repository.AnagDspRepository;
import it.siae.sophia.ms.pricelist.repository.AnagUtilizationTypeRepository;
import it.siae.sophia.ms.pricelist.repository.DroolsPricingRepository;
import it.siae.sophia.ms.pricelist.service.PriceListService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ejb.DuplicateKeyException;
import java.io.*;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static it.siae.sophia.ms.pricelist.errors.ErrorConstants.*;

@Service
@Transactional
public class PriceListServiceImpl implements PriceListService {

    Logger logger = LoggerFactory.getLogger(PriceListServiceImpl.class);


    @Autowired
    DroolsPricingRepository pricingRepository;

    @Autowired
    AnagDspRepository anagDspRepository;

    @Autowired
    AnagUtilizationTypeRepository anagUtilTypeRepo;

    @Autowired
    ExcelUtilsServiceImpl excelUtilsService;

    @Autowired
    private Environment env;

    @Value("${spring.s3uri.path}")
    public String amazonpath;

    @Value("${spring.cloud.aws.credentials.accessKey}")
    private String accessKey;

    @Value("${spring.cloud.aws.credentials.secretKey}")
    private String secretKey;

    @Value("${spring.cloud.aws.region.static}")
    private String region;

    @Value("${spring.datasource.url}")
    private String url;

    @Value("${spring.datasource.username}")
    private String user;

    @Value("${spring.datasource.password}")
    private String password;

    @Override
    public boolean savePriceList(PriceListDTO priceListDTO) throws Exception {
        boolean response = false;

        // TO DO REST OF SAVE PRICELIST
        //  1. create EXCEL FILE
        String amazonUrl = null;
        String dataforname = new SimpleDateFormat("yyyyMMdd").format(new SimpleDateFormat("yyyy-MM-dd").parse(priceListDTO.getDataInizioValidita()));
        //s3://siae-sophia-datalake/{env}/rules/{iddsp}/{id-utilizzation-type}/{valid-from}/

        //CONTROLLO SE SONO IN MODIFICA
        String excelName = null;
        boolean newExcel = false;
        DroolsPricing droolsPricing = new DroolsPricing();
        if (priceListDTO.getNomeFileExcel() == null || priceListDTO.getNomeFileExcel().isEmpty()) {
            //NEW
            newExcel = true;

            //s3://siae-sophia-datalake/debug/rules/
            amazonUrl = amazonpath.concat(priceListDTO.getDsp()).concat("/").concat(priceListDTO.getTipoUtilizzo()).concat("/").concat(dataforname).concat("/");
            String name = anagUtilTypeRepo.idUtilizationTypeDecoded(priceListDTO.getTipoUtilizzo()).getName();
            excelName = priceListDTO.getDsp() + "_".concat(name) + "_" + (dataforname);
            if (priceListDTO.getDataFineValidita() != null) {
                String dataEndforname = new SimpleDateFormat("yyyyMMdd").format(new SimpleDateFormat("yyyy-MM-dd").parse(priceListDTO.getDataFineValidita()));
                excelName = excelName.concat("_").concat(dataEndforname);
                //Abbiamo forzato l'ora della data di validità portandolo alle ore 23 della data in input per bypassare l'UTC-1 impostato sul Db
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(priceListDTO.getDataFineValidita()));
                calendar.add(Calendar.HOUR, 23);
                droolsPricing.setValidTo(new Timestamp(calendar.getTimeInMillis()));
            }
            excelName = excelName + ".xls";
            droolsPricing.setId(null);
            droolsPricing.setIddsp(priceListDTO.getDsp());
            droolsPricing.setIdUtilizationType(priceListDTO.getTipoUtilizzo());
            //Abbiamo forzato l'ora della data di validità portandolo alle ore 23 della data in input per bypassare l'UTC-1 impostato sul Db
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(priceListDTO.getDataInizioValidita()));
            calendar.add(Calendar.HOUR, 23);
            droolsPricing.setValidFrom(new Timestamp(calendar.getTimeInMillis()));
            droolsPricing.setXlsS3Url(amazonUrl.concat(excelName));
        } else {
            droolsPricing = pricingRepository.findPathExcelPricelistByParams(priceListDTO.getDsp(), priceListDTO.getTipoUtilizzo(), priceListDTO.getDataInizioValidita(), priceListDTO.getDataFineValidita());
            if (droolsPricing == null && (droolsPricing.getXlsS3Url() != null && !(droolsPricing.getXlsS3Url().equalsIgnoreCase(""))))
                throw new BadRequestAlertException(EXCEL_NOT_FOUND_TYPE, "SAVE PRICELIST", "errorPricelist");
            excelName = priceListDTO.getNomeFileExcel();
            amazonUrl = droolsPricing.getXlsS3Url().replace(priceListDTO.getNomeFileExcel(), "");
        }
        ArrayList existingConfiguration = pricingRepository.findDuplicate(droolsPricing);
        if (newExcel && existingConfiguration.size() > 0) {
            String idList = (String) existingConfiguration
                    .stream()
                    .map(el -> el.toString())
                    .collect(Collectors.joining(","));
            throw new DuplicateKeyException(ROW_DUPLICATE + " : Riga duplicata. - [" + idList + "]");
            //throw new DuplicateKeyException(ROW_ALREADY_EXISTS + " : Esiste già una configurazione per i parametri scelti. - [" + idList + "]");
        } else {
            existingConfiguration = pricingRepository.findByDspAndIdUtilizationTypeAndValidFromAndValidTo(droolsPricing);
            if (newExcel && existingConfiguration.size() > 0) {
                String idList = (String) existingConfiguration
                        .stream()
                        .map(el -> el.toString())
                        .collect(Collectors.joining(","));
                throw new DuplicateKeyException(ROW_ALREADY_EXISTS + " : Esiste già una configurazione per i parametri scelti. - [" + idList + "]");
            }
        }
        boolean created = excelUtilsService.saveExcelFile(priceListDTO, excelName);
        //  2. save row on PRICELIST table with EXCEL PATH CREATED
        if (created) {
            //GET FILE CREATED FROM LOCAL DIR
            File excel = new File(excelName);
            // upload file to S3
            if (excel.length() > 0L) {
                logger.info("Upload FILE : " + excel.getName() + " to S3");
                if (excelUtilsService.uploadExcelFile(excel, amazonUrl)) {
                    response = true;
                    logger.debug("PRICELIST EXCEL " + excel.getName() + " COMPLETE");

                    if (newExcel) {
                        pricingRepository.save(droolsPricing);
                    }
                }
                excel.delete();
            }
        }
        return response;
    }

    @Override
    public String getImportPriceList(String dsp, String tipoUtilizzo, String dataInizioValidita, String dataFineValidita, String checkboxSelezionata) throws Exception {
        DroolsPricing pricelist = pricingRepository.findPathExcelPricelistByParams(dsp, tipoUtilizzo, dataInizioValidita, dataFineValidita);
        //TO DO CSV TO BLACKLISTDTO
        Gson gson = new Gson();
        if (pricelist.getXlsS3Url() != null && !(pricelist.getXlsS3Url().isEmpty())) {
            return gson.toJson(this.leggiExcel(this.getAmazonS3File(pricelist), pricelist, checkboxSelezionata));
        } else {
            return gson.toJson(new DroolsPricing());
        }
    }

    @Override
    public StringListDTO getAllDspNames() {
        StringListDTO dto = new StringListDTO();
        dto.setNames(anagDspRepository.findAll().stream()
                .map(AnagDsp::getNAME)
                .collect(Collectors.toList()));
        return dto;
    }

    @Override
    public StringListDTO getAllUtilizzationTypes() {
        StringListDTO dto = new StringListDTO();
        dto.setNames(anagUtilTypeRepo.findAll().stream()
                .map(AnagUtilizationType::getName)
                .collect(Collectors.toList()));
        return dto;
    }

    public PriceListDTO leggiExcel(File f, DroolsPricing b, String checkboxSelezionata) throws IOException, ParseException {
        PriceListDTO dto = new PriceListDTO();
        //Set della testata dell'oggetto
        dto.setDataInizioValidita(b.getValidFrom().toString());
        dto.setDataFineValidita(b.getValidTo() != null ? b.getValidTo().toString() : null);
        dto.setTipoUtilizzo(b.getIdUtilizationType());
        dto.setDsp(b.getIddsp());
        dto.setRecordDettaglio(new ArrayList<PriceListDTO.Dettaglio>());
        dto.setCheckboxSelezionata(checkboxSelezionata);
        //
        logger.debug("TESTATA PRICELIST : DSP ->" + dto.getDsp() + " UTILIZZATION_TYPE ->" + dto.getTipoUtilizzo() + " DATA VALIDITA ->" + dto.getDataInizioValidita());
        logger.debug("STAMPO RIGHE DALL' EXCEL ...");
        switch (dto.getTipoUtilizzo()) {
            case "STRM_AD":
                excelUtilsService.readStreamingAdOrYouTubeAT(f, dto);
                break;
            case "STRM_SUB":
                excelUtilsService.readTerritoryOrCountries(f, dto);
                break;
            case "DWN":
                excelUtilsService.readDownload(f, dto);
                break;
            case "STRM_GEN":
                excelUtilsService.readStreamingGen(f, dto);
                break;
            case "Cloud":
                excelUtilsService.readCloud(f, dto);
                break;
            case "MASTERLIST":
                excelUtilsService.readMasterlistOrYouTubeStreaming(f, dto);
                break;
            case "YOUTUBE":
                excelUtilsService.readMasterlistOrYouTubeStreaming(f, dto);
                break;
            case "YOU_AUDIO":
                excelUtilsService.readStreamingAdOrYouTubeAT(f, dto);
                break;
        }

        dto.toString();
        return dto;
    }


    // ********* UTILS METHOD *********
    public File getAmazonS3File(DroolsPricing b) throws Exception {
        if (b.getXlsS3Url() != null) {
            File file;
            ByteArrayOutputStream out;
            file = new File(Paths.get(b.getXlsS3Url().substring(5)).getFileName().toString());
            final S3.Url url = new S3.Url(b.getXlsS3Url());
            out = new ByteArrayOutputStream();
            AWSCredentials credentials = new BasicAWSCredentials(
                    accessKey,
                    secretKey
            );
            AmazonS3 s3client = AmazonS3ClientBuilder
                    .standard()
                    .withCredentials(new AWSStaticCredentialsProvider(credentials))
                    .withRegion(region)
                    .build();
            s3client.getObject(new GetObjectRequest(url.bucket, url.key), file);
            return file;
        } else {
            throw new Exception("Selezione non disponibile");
        }

    }
}
