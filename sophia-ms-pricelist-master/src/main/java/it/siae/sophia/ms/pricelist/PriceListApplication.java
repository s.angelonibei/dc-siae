/*
 * @Autor A.D.C.
 */
package it.siae.sophia.ms.pricelist;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan
@EnableJpaRepositories
@SpringBootApplication
@EnableEurekaClient
public class PriceListApplication {

    public static void main(String[] args)  {

        SpringApplication.run(PriceListApplication.class, args);
    }

}
