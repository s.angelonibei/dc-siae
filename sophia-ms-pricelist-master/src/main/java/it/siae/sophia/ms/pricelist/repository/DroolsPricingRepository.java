package it.siae.sophia.ms.pricelist.repository;

import it.siae.sophia.ms.pricelist.entity.DroolsPricing;
import it.siae.sophia.ms.pricelist.repository.custom.DroolsPricingRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DroolsPricingRepository extends JpaRepository<DroolsPricing, Long>, DroolsPricingRepositoryCustom {

}