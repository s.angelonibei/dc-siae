package it.siae.sophia.ms.pricelist.dto;

import lombok.Data;

import java.util.List;

@Data
public class StringListDTO {
    private List<String> names;
}
