package it.siae.sophia.ms.pricelist.repository;

import it.siae.sophia.ms.pricelist.entity.AnagDsp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface AnagDspRepository extends JpaRepository<AnagDsp, String>, JpaSpecificationExecutor<AnagDsp> {

}