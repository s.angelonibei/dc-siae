package it.siae.sophia.ms.pricelist.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "ANAG_UTILIZATION_TYPE",schema = "MCMDB_debug")
@Entity
@Data
public class AnagUtilizationType implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID_UTILIZATION_TYPE", insertable = false, nullable = false)
    private String idUtilizationType;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "DESCRIPTION", nullable = false)
    private String description;

    
}