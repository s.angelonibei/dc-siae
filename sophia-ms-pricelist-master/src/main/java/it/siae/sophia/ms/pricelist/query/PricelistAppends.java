/*
 * @Autor A.D.C.
 */

package it.siae.sophia.ms.pricelist.query;

import lombok.Data;

@Data
public class PricelistAppends extends AbstractQuery {
    private String iddsp;
    private String selectIdDsp;
    private String utilizzationType;
    private String dataInizioValidita;
    private String dataFineValidita;
    private String dataInizioFineValidita;
    private String maxDateFrom;
    private String dataMaxStart;
    private String dataMaxEnd;
    private String names;
}
