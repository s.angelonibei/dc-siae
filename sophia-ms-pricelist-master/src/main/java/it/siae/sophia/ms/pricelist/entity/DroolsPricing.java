package it.siae.sophia.ms.pricelist.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDate;

@Data
@Entity(name = "DroolsPricing")
@Table(name = "DROOLS_PRICING",schema = "MCMDB_debug")
public class DroolsPricing implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", insertable = false, nullable = false)
    private Long id;

    @Column(name = "IDDSP", nullable = false)
    private String iddsp;

    @Column(name = "ID_UTILIZATION_TYPE", nullable = false)
    private String idUtilizationType;

    @Column(name = "VALID_FROM", nullable = false)
    private Timestamp validFrom;

    @Column(name = "VALID_TO")
    private Timestamp validTo;

    @Column(name = "XLS_S3_URL", nullable = false)
    private String xlsS3Url;

    
}