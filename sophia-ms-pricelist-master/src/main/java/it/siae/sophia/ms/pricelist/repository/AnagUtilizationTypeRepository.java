package it.siae.sophia.ms.pricelist.repository;

import it.siae.sophia.ms.pricelist.entity.AnagUtilizationType;
import it.siae.sophia.ms.pricelist.repository.custom.AnagUtilizationTypeRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnagUtilizationTypeRepository extends JpaRepository<AnagUtilizationType, String>, AnagUtilizationTypeRepositoryCustom {

}