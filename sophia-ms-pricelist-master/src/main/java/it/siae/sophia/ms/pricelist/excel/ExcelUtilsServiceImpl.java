package it.siae.sophia.ms.pricelist.excel;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.event.ProgressEvent;
import com.amazonaws.event.ProgressEventType;
import com.amazonaws.event.ProgressListener;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.PutObjectRequest;
import it.siae.sophia.ms.pricelist.aws.S3;
import it.siae.sophia.ms.pricelist.dto.PriceListDTO;
import it.siae.sophia.ms.pricelist.errors.BadRequestAlertException;
import it.siae.sophia.ms.pricelist.errors.ErrorConstants;
import it.siae.sophia.ms.pricelist.repository.AnagUtilizationTypeRepository;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;

import static it.siae.sophia.ms.pricelist.errors.ErrorConstants.INVALID_CHECKBOX_TYPE;

@Service
@Transactional
public class ExcelUtilsServiceImpl implements ExcelUtilsService {
    Logger logger = LoggerFactory.getLogger(ExcelUtilsServiceImpl.class);

    @Value("${s3.retry.put_object}")
    private String put_object;

    @Value("${spring.cloud.aws.credentials.accessKey}")
    private String accessKey;

    @Value("${spring.cloud.aws.credentials.secretKey}")
    private String secretKey;

    @Value("${spring.cloud.aws.region.static}")
    private String region;

    @Value("${template.cloud}")
    private String cloud;

    @Value("${template.territory}")
    private String territory;

    @Value("${template.countries}")
    private String countries;

    @Value("${template.download}")
    private String download;

    @Value("${template.masterlist}")
    private String masterlist;

    @Value("${template.streamingAd}")
    private String streamingAd;

    @Value("${template.streamingGenerico}")
    private String streamingGenerico;

    @Value("${template.youtubeAudioTier}")
    private String youtubeAudioTier;

    @Value("${template.youtubeStreaming}")
    private String youtubeStreaming;

    private String CHECKBOX_TERRITORY = "Territory";
    private String CHECKBOX_COUNTRIES = "Countries";

    @Autowired
    private AnagUtilizationTypeRepository anagUtilTypeRepo;
    
    @Override
    public boolean saveExcelFile(PriceListDTO priceListDTO, String excelName) {
        boolean created = true;
        //DETERMINO IL TEMPLATE DA UTILIZZARE
        switch (priceListDTO.getTipoUtilizzo()) {
            case "STRM_AD":
                this.writeStreamingAdExcel(priceListDTO, excelName);
                break;
            case "STRM_SUB":
                //TO DO
                if (priceListDTO.getCheckboxSelezionata().equalsIgnoreCase(CHECKBOX_TERRITORY)) {
                    this.writeTerritoryExcel(priceListDTO, excelName);
                } else if (priceListDTO.getCheckboxSelezionata().equalsIgnoreCase(CHECKBOX_COUNTRIES)) {
                    //THEN I'm in Countries
                    this.writeCountriesExcel(priceListDTO, excelName);
                } else {
                    throw new BadRequestAlertException(INVALID_CHECKBOX_TYPE.toString(), "SAVE PRICELIST", "errorPricelistCheckBox");
                }
                break;
            case "DWN":
                this.writeDownloadExcel(priceListDTO, excelName);
                break;
            case "STRM_GEN":
                this.writeStreamingGenExcel(priceListDTO, excelName);
                break;
            case "Cloud":
                this.writeCloudExcel(priceListDTO, excelName);
                break;
            case "MASTERLIST":
                this.writeMasterlistExcel(priceListDTO, excelName);
                break;
            case "YOUTUBE":
                this.writeYoutubeStreamingExcel(priceListDTO, excelName);
                break;
            case "YOU_AUDIO":
                this.writeYoutubeAudioExcel(priceListDTO, excelName);
                break;
        }

        //
        return created;
    }

    private void writeYoutubeAudioExcel(PriceListDTO priceListDTO, String excelName) {
        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream(youtubeAudioTier);
            HSSFWorkbook workbook = new HSSFWorkbook(inputStream);
            HSSFCellStyle green = workbook.createCellStyle();
            green.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index);
            green.setFillPattern(CellStyle.SOLID_FOREGROUND);
            HSSFCellStyle lightblue = workbook.createCellStyle();
            lightblue.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
            lightblue.setFillPattern(CellStyle.SOLID_FOREGROUND);
            HSSFCellStyle amarillo = workbook.createCellStyle();
            amarillo.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
            amarillo.setFillPattern(CellStyle.SOLID_FOREGROUND);
            Sheet sheet = workbook.getSheetAt(0);

            int rowCount = 7;

            sheet.getRow(rowCount).getCell(4).setCellValue(priceListDTO.getDsp()+" "+anagUtilTypeRepo.idUtilizationTypeDecoded(priceListDTO.getTipoUtilizzo()).getDescription());

            rowCount = 11;

            for (PriceListDTO.Dettaglio d : priceListDTO.getRecordDettaglio()) {
                Row row = sheet.createRow(++rowCount);

                int columnCount = 1;
                row.createCell(columnCount).setCellValue(d.getRuleName());
                row.getCell(columnCount++).setCellStyle(green);
                row.createCell(columnCount).setCellValue(d.getOffertaCommerciale());
                row.getCell(columnCount++).setCellStyle(lightblue);
                row.createCell(columnCount).setCellValue(d.getTerritorio());
                row.getCell(columnCount++).setCellStyle(lightblue);
                row.createCell(columnCount).setCellValue(d.getSiaeProRata());
                row.getCell(columnCount++).setCellStyle(amarillo);
                row.createCell(columnCount).setCellValue(d.getMinimumPerStream());
                row.getCell(columnCount++).setCellStyle(amarillo);
                row.createCell(columnCount).setCellValue(d.getMinimumCurrency());
                row.getCell(columnCount++).setCellStyle(amarillo);
                row.createCell(columnCount).setCellValue("n/a");
                row.getCell(columnCount++).setCellStyle(amarillo);

            }

            inputStream.close();

            FileOutputStream outputStream = new FileOutputStream(excelName);
            workbook.write(outputStream);
            workbook.close();
            outputStream.close();

        } catch (IOException | EncryptedDocumentException ex) {
            ex.printStackTrace();
        }
    }

    private void writeYoutubeStreamingExcel(PriceListDTO priceListDTO, String excelName) {
        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream(youtubeStreaming);
            HSSFWorkbook workbook = new HSSFWorkbook(inputStream);
            HSSFCellStyle green = workbook.createCellStyle();
            green.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index);
            green.setFillPattern(CellStyle.SOLID_FOREGROUND);
            HSSFCellStyle lightblue = workbook.createCellStyle();
            lightblue.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
            lightblue.setFillPattern(CellStyle.SOLID_FOREGROUND);
            HSSFCellStyle amarillo = workbook.createCellStyle();
            amarillo.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
            amarillo.setFillPattern(CellStyle.SOLID_FOREGROUND);
            Sheet sheet = workbook.getSheetAt(0);

            int rowCount = 7;

            sheet.getRow(rowCount).getCell(3).setCellValue(priceListDTO.getDsp()+" "+anagUtilTypeRepo.idUtilizationTypeDecoded(priceListDTO.getTipoUtilizzo()).getDescription());

            rowCount = 11;

            for (PriceListDTO.Dettaglio d : priceListDTO.getRecordDettaglio()) {
                Row row = sheet.createRow(++rowCount);

                int columnCount = 1;
                row.createCell(columnCount).setCellValue(d.getRuleName());
                row.getCell(columnCount++).setCellStyle(green);
                row.createCell(columnCount).setCellValue(d.getOffertaCommerciale());
                row.getCell(columnCount++).setCellStyle(lightblue);
                row.createCell(columnCount).setCellValue(d.getSiaeProRata());
                row.getCell(columnCount++).setCellStyle(amarillo);
                row.createCell(columnCount).setCellValue(d.getMinimoAbbonamento());
                row.getCell(columnCount++).setCellStyle(amarillo);
                row.createCell(columnCount).setCellValue(d.getMinimumCurrency());
                row.getCell(columnCount++).setCellStyle(amarillo);
                row.createCell(columnCount).setCellValue("n/a");
                row.getCell(columnCount++).setCellStyle(amarillo);

            }

            inputStream.close();

            FileOutputStream outputStream = new FileOutputStream(excelName);
            workbook.write(outputStream);
            workbook.close();
            outputStream.close();

        } catch (IOException | EncryptedDocumentException ex) {
            ex.printStackTrace();
        }
    }

    private void writeMasterlistExcel(PriceListDTO priceListDTO, String excelName) {
        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream(masterlist);
            HSSFWorkbook workbook = new HSSFWorkbook(inputStream);
            HSSFCellStyle green = workbook.createCellStyle();
            green.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index);
            green.setFillPattern(CellStyle.SOLID_FOREGROUND);
            HSSFCellStyle lightblue = workbook.createCellStyle();
            lightblue.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
            lightblue.setFillPattern(CellStyle.SOLID_FOREGROUND);
            HSSFCellStyle amarillo = workbook.createCellStyle();
            amarillo.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
            amarillo.setFillPattern(CellStyle.SOLID_FOREGROUND);
            Sheet sheet = workbook.getSheetAt(0);

            int rowCount = 7;

            sheet.getRow(rowCount).getCell(4).setCellValue(priceListDTO.getDsp()+" "+anagUtilTypeRepo.idUtilizationTypeDecoded(priceListDTO.getTipoUtilizzo()).getDescription());

            rowCount = 11;

            for (PriceListDTO.Dettaglio d : priceListDTO.getRecordDettaglio()) {
                Row row = sheet.createRow(++rowCount);

                int columnCount = 1;
                row.createCell(columnCount).setCellValue(d.getRuleName());
                row.getCell(columnCount++).setCellStyle(green);
                row.createCell(columnCount).setCellValue(d.getOffertaCommerciale());
                row.getCell(columnCount++).setCellStyle(lightblue);
                row.createCell(columnCount).setCellValue(d.getSiaeProRata());
                row.getCell(columnCount++).setCellStyle(amarillo);
                row.createCell(columnCount).setCellValue(d.getMinimoAbbonamento());
                row.getCell(columnCount++).setCellStyle(amarillo);
                row.createCell(columnCount).setCellValue(d.getMinimumCurrency());
                row.getCell(columnCount++).setCellStyle(amarillo);
                row.createCell(columnCount).setCellValue("n/a");
                row.getCell(columnCount++).setCellStyle(amarillo);

            }

            inputStream.close();

            FileOutputStream outputStream = new FileOutputStream(excelName);
            workbook.write(outputStream);
            workbook.close();
            outputStream.close();

        } catch (IOException | EncryptedDocumentException ex) {
            ex.printStackTrace();
        }
    }

    private void writeStreamingGenExcel(PriceListDTO priceListDTO, String excelName) {
        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream(streamingGenerico);
            HSSFWorkbook workbook = new HSSFWorkbook(inputStream);
            HSSFCellStyle green = workbook.createCellStyle();
            green.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index);
            green.setFillPattern(CellStyle.SOLID_FOREGROUND);
            HSSFCellStyle lightblue = workbook.createCellStyle();
            lightblue.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
            lightblue.setFillPattern(CellStyle.SOLID_FOREGROUND);
            HSSFCellStyle amarillo = workbook.createCellStyle();
            amarillo.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
            amarillo.setFillPattern(CellStyle.SOLID_FOREGROUND);
            Sheet sheet = workbook.getSheetAt(0);

            int rowCount = 7;

            sheet.getRow(rowCount).getCell(4).setCellValue(priceListDTO.getDsp()+" "+anagUtilTypeRepo.idUtilizationTypeDecoded(priceListDTO.getTipoUtilizzo()).getDescription());

            rowCount = 11;

            for (PriceListDTO.Dettaglio d : priceListDTO.getRecordDettaglio()) {
                Row row = sheet.createRow(++rowCount);

                int columnCount = 1;
                row.createCell(columnCount).setCellValue(d.getRuleName());
                row.getCell(columnCount++).setCellStyle(green);
                row.createCell(columnCount).setCellValue(d.getOffertaCommerciale());
                row.getCell(columnCount++).setCellStyle(lightblue);
                row.createCell(columnCount).setCellValue(d.getTerritorio());
                row.getCell(columnCount++).setCellStyle(lightblue);
                row.createCell(columnCount).setCellValue(d.getSiaeProRata());
                row.getCell(columnCount++).setCellStyle(amarillo);
                row.createCell(columnCount).setCellValue(d.getMinimoAbbonamento());
                row.getCell(columnCount++).setCellStyle(amarillo);
                row.createCell(columnCount).setCellValue(d.getMinimumPerStream());
                row.getCell(columnCount++).setCellStyle(amarillo);
                row.createCell(columnCount).setCellValue(d.getMinimumCurrency());
                row.getCell(columnCount++).setCellStyle(amarillo);
                row.createCell(columnCount).setCellValue("n/a");
                row.getCell(columnCount++).setCellStyle(amarillo);

            }

            inputStream.close();

            FileOutputStream outputStream = new FileOutputStream(excelName);
            workbook.write(outputStream);
            workbook.close();
            outputStream.close();

        } catch (IOException | EncryptedDocumentException ex) {
            ex.printStackTrace();
        }
    }

    private void writeDownloadExcel(PriceListDTO priceListDTO, String excelName) {
        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream(download);
            HSSFWorkbook workbook = new HSSFWorkbook(inputStream);

            HSSFCellStyle green = workbook.createCellStyle();
            green.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index);
            green.setFillPattern(FillPatternType.SOLID_FOREGROUND);

            HSSFCellStyle lightblue = workbook.createCellStyle();
            lightblue.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
            lightblue.setFillPattern(FillPatternType.SOLID_FOREGROUND);

            HSSFCellStyle amarillo = workbook.createCellStyle();
            amarillo.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
            amarillo.setFillPattern(FillPatternType.SOLID_FOREGROUND);

            Sheet sheet = workbook.getSheetAt(0);

            int shiftColumn = 0;
            int minColumn = 6;
            int numMaxMinAlbum = priceListDTO.getRecordDettaglio().get(0).getSiaeMinimumAlbum().size();

            int maxColumn = minColumn + numMaxMinAlbum;

            // Imposto la stringa in base al numero di parametri

            if (priceListDTO.getNumParametri() == 3) {
                sheet.getRow(10).getCell(5).setCellValue("line.addMinimum(\"$1\",\"$2\",\"$3\");");
            } else {
                sheet.getRow(10).getCell(5).setCellValue("line.addMinProp(\"$1\",\"$2\");");
            }
            // Aggiungo le colonne per Siae Minimum Album

            for (int r = 7; r <= 10; r++) {
                Row currentRow = sheet.getRow(r);
                Cell oldCell = currentRow.getCell(7);
                Cell newCell = currentRow.createCell(7 + numMaxMinAlbum - 1);
                cloneCellValue(oldCell, newCell);

                currentRow = sheet.getRow(r);
                oldCell = currentRow.getCell(6);
                newCell = currentRow.createCell(6 + numMaxMinAlbum - 1);
                cloneCellValue(oldCell, newCell);

                Cell minAlbumCells = currentRow.getCell(5);
                for (int c = 6; c < numMaxMinAlbum+5; c++) {
                    newCell = currentRow.createCell(c);
                    cloneCellValue(minAlbumCells, newCell);
                    sheet.setColumnWidth(c, 7000);
                }
            }

            sheet.setColumnWidth(7 + numMaxMinAlbum, 7000);
            sheet.setColumnWidth(6 + numMaxMinAlbum, 7000);
            sheet.setColumnWidth(5 + numMaxMinAlbum, 7000);


            // Verifico quante celle devono essere spostate
            int rowCount = 7;
            if (priceListDTO.getTotalSales() && priceListDTO.getOffertaCommerciale()) {
                shiftColumn = shiftColumn + 2;
            } else if (priceListDTO.getOffertaCommerciale() || priceListDTO.getTotalSales()) {
                shiftColumn++;
            } else
                ;

            // Sposto le colonne se necessario

            if (shiftColumn != 0) {
                Row terryoryRow = sheet.getRow(11);

                Cell terryoryCell = terryoryRow.getCell(2);

                for (int r = 7; r <= 11; r++) {
                    Row currentRow = sheet.getRow(r);
                    for (int c = maxColumn; c >= 3; c--) {

                        Cell oldCell = currentRow.getCell(c);
                        Cell newCell = currentRow.createCell(c + shiftColumn);
                        cloneCellValue(oldCell, newCell);
                    }
                }

                if (priceListDTO.getTotalSales()) {
                    sheet.getRow(8).getCell(shiftColumn + 2).setCellValue("CONDITION");
                    sheet.getRow(10).getCell(shiftColumn + 2).setCellValue("totalSales >= $1, totalSales <= $2");

                    sheet.setColumnWidth(shiftColumn + 2, 7000);
                    terryoryRow.getCell(shiftColumn + 2).setCellStyle(terryoryCell.getCellStyle());
                    terryoryRow.getCell(shiftColumn + 2).setCellValue("Total Sales");
                }
                if (priceListDTO.getOffertaCommerciale()) {
                    sheet.getRow(8).getCell(3).setCellValue("CONDITION");
                    sheet.getRow(10).getCell(2).setCellValue("businessOffer");
                    sheet.getRow(10).getCell(3).setCellValue("territory");
                    sheet.setColumnWidth(2, 7000);
                    sheet.setColumnWidth(3, 5000);

                    Cell newCell = terryoryRow.getCell(3);

                    newCell.setCellStyle(terryoryCell.getCellStyle());
                    newCell.setCellValue("Terrotory");
                    terryoryRow.getCell(2).setCellValue("Offerta Commerciale");
                }
                sheet.addMergedRegion(new CellRangeAddress(9, 9, 2, shiftColumn + 2));
            }

            sheet.getRow(rowCount).getCell(3 + shiftColumn).setCellValue(priceListDTO.getDsp());

            rowCount = 11;

            // Carico i dati
            Row header = sheet.getRow(rowCount);
            int pos = 0;

            for (int c = 0; c <= numMaxMinAlbum; c++) {
                pos = c + 5 + shiftColumn;
                header.createCell(pos).setCellValue("SIAE Minimum Album");
                header.getCell(pos).setCellStyle(header.getCell(4 + shiftColumn).getCellStyle());
            }
            header.createCell(pos).setCellValue("Minimum Currency");
            header.getCell(pos++).setCellStyle(header.getCell(4 + shiftColumn).getCellStyle());
            header.createCell(pos).setCellValue("n/a");
            header.getCell(pos++).setCellStyle(header.getCell(4 + shiftColumn).getCellStyle());

            for (PriceListDTO.Dettaglio d : priceListDTO.getRecordDettaglio()) {
                Row row = sheet.createRow(++rowCount);
                int columnCount = 1;
                row.createCell(columnCount).setCellValue(d.getRuleName());
                row.getCell(columnCount++).setCellStyle(green);

                if (priceListDTO.getOffertaCommerciale()) {

                    row.createCell(2).setCellValue(d.getOffertaCommerciale());
                    row.getCell(2).setCellStyle(lightblue);
                    columnCount++;
                }

                row.createCell(columnCount).setCellValue(d.getTerritorio());
                row.getCell(columnCount++).setCellStyle(lightblue);

                if (priceListDTO.getTotalSales()) {

                    row.createCell(columnCount).setCellValue(d.getTotalSales());
                    row.getCell(columnCount++).setCellStyle(lightblue);
                }

                row.createCell(columnCount).setCellValue(d.getSiaeProRata());
                row.getCell(columnCount++).setCellStyle(amarillo);
                row.createCell(columnCount).setCellValue(d.getSiaeMinimumTrack());
                row.getCell(columnCount++).setCellStyle(amarillo);
                for (String s : d.getSiaeMinimumAlbum()) {
                    row.createCell(columnCount).setCellValue(s);
                    row.getCell(columnCount++).setCellStyle(amarillo);
                }
                row.createCell(columnCount).setCellValue(d.getMinimumCurrency());
                row.getCell(columnCount++).setCellStyle(amarillo);
                row.createCell(columnCount).setCellValue("n/a");
                row.getCell(columnCount++).setCellStyle(amarillo);

            }
            inputStream.close();

            FileOutputStream outputStream = new FileOutputStream(excelName);
            workbook.write(outputStream);
            workbook.close();
            outputStream.close();

        } catch (IOException | EncryptedDocumentException ex) {
            ex.printStackTrace();
        }
    }

    private void writeStreamingAdExcel(PriceListDTO priceListDTO, String excelName) {
        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream(streamingAd);
            HSSFWorkbook workbook = new HSSFWorkbook(inputStream);
            HSSFCellStyle green = workbook.createCellStyle();
            green.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index);
            green.setFillPattern(CellStyle.SOLID_FOREGROUND);
            HSSFCellStyle lightblue = workbook.createCellStyle();
            lightblue.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
            lightblue.setFillPattern(CellStyle.SOLID_FOREGROUND);
            HSSFCellStyle amarillo = workbook.createCellStyle();
            amarillo.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
            amarillo.setFillPattern(CellStyle.SOLID_FOREGROUND);
            Sheet sheet = workbook.getSheetAt(0);

            int rowCount = 7;

            sheet.getRow(rowCount).getCell(3).setCellValue(priceListDTO.getDsp()+" "+anagUtilTypeRepo.idUtilizationTypeDecoded(priceListDTO.getTipoUtilizzo()).getDescription());

            rowCount = 11;

            for (PriceListDTO.Dettaglio d : priceListDTO.getRecordDettaglio()) {
                Row row = sheet.createRow(++rowCount);

                int columnCount = 1;
                row.createCell(columnCount).setCellValue(d.getRuleName());
                row.getCell(columnCount++).setCellStyle(green);
                row.createCell(columnCount).setCellValue(d.getOffertaCommerciale());
                row.getCell(columnCount++).setCellStyle(lightblue);
                row.createCell(columnCount).setCellValue(d.getTerritorio());
                row.getCell(columnCount++).setCellStyle(lightblue);
                row.createCell(columnCount).setCellValue(d.getSiaeProRata());
                row.getCell(columnCount++).setCellStyle(amarillo);
                row.createCell(columnCount).setCellValue(d.getMinimumPerStream());
                row.getCell(columnCount++).setCellStyle(amarillo);
                row.createCell(columnCount).setCellValue(d.getMinimumCurrency());
                row.getCell(columnCount++).setCellStyle(amarillo);
                row.createCell(columnCount).setCellValue("n/a");
                row.getCell(columnCount++).setCellStyle(amarillo);

            }

            inputStream.close();

            FileOutputStream outputStream = new FileOutputStream(excelName);
            workbook.write(outputStream);
            workbook.close();
            outputStream.close();

        } catch (IOException | EncryptedDocumentException ex) {
            ex.printStackTrace();
        }
    }

    private void writeCountriesExcel(PriceListDTO priceListDTO, String excelName) {
        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream(countries);
            HSSFWorkbook workbook = new HSSFWorkbook(inputStream);
            HSSFCellStyle green = workbook.createCellStyle();
            green.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index);
            green.setFillPattern(CellStyle.SOLID_FOREGROUND);
            HSSFCellStyle lightblue = workbook.createCellStyle();
            lightblue.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
            lightblue.setFillPattern(CellStyle.SOLID_FOREGROUND);
            HSSFCellStyle amarillo = workbook.createCellStyle();
            amarillo.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
            amarillo.setFillPattern(CellStyle.SOLID_FOREGROUND);
            Sheet sheet = workbook.getSheetAt(0);

            int rowCount = 7;

            sheet.getRow(rowCount).getCell(4).setCellValue(priceListDTO.getDsp()+" "+anagUtilTypeRepo.idUtilizationTypeDecoded(priceListDTO.getTipoUtilizzo()).getDescription());

            rowCount = 11;

            for (PriceListDTO.Dettaglio d : priceListDTO.getRecordDettaglio()) {
                Row row = sheet.createRow(++rowCount);

                int columnCount = 1;
                row.createCell(columnCount).setCellValue(d.getRuleName());
                row.getCell(columnCount++).setCellStyle(green);
                row.createCell(columnCount).setCellValue(d.getCountries());
                row.getCell(columnCount++).setCellStyle(lightblue);
                row.createCell(columnCount).setCellValue(d.getOffertaCommerciale());
                row.getCell(columnCount++).setCellStyle(lightblue);
                row.createCell(columnCount).setCellValue(d.getSiaeProRata());
                row.getCell(columnCount++).setCellStyle(amarillo);
                row.createCell(columnCount).setCellValue(d.getMinimoAbbonamento());
                row.getCell(columnCount++).setCellStyle(amarillo);
                row.createCell(columnCount).setCellValue(d.getMinimumCurrency());
                row.getCell(columnCount++).setCellStyle(amarillo);
                row.createCell(columnCount).setCellValue("n/a");
                row.getCell(columnCount++).setCellStyle(amarillo);

            }

            inputStream.close();

            FileOutputStream outputStream = new FileOutputStream(excelName);
            workbook.write(outputStream);
            workbook.close();
            outputStream.close();

        } catch (IOException | EncryptedDocumentException ex) {
            ex.printStackTrace();
        }
    }

    private void writeCloudExcel(PriceListDTO priceListDTO, String excelName) {

        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream(cloud);
            HSSFWorkbook workbook = new HSSFWorkbook(inputStream);
            HSSFCellStyle green = workbook.createCellStyle();
            green.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index);
            green.setFillPattern(CellStyle.SOLID_FOREGROUND);
            HSSFCellStyle lightblue = workbook.createCellStyle();
            lightblue.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
            lightblue.setFillPattern(CellStyle.SOLID_FOREGROUND);
            HSSFCellStyle amarillo = workbook.createCellStyle();
            amarillo.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
            amarillo.setFillPattern(CellStyle.SOLID_FOREGROUND);
            Sheet sheet = workbook.getSheetAt(0);

            int rowCount = 7;

            sheet.getRow(rowCount).getCell(4).setCellValue(priceListDTO.getDsp()+" "+anagUtilTypeRepo.idUtilizationTypeDecoded(priceListDTO.getTipoUtilizzo()).getDescription());

            rowCount = 11;

            for (PriceListDTO.Dettaglio d : priceListDTO.getRecordDettaglio()) {
                Row row = sheet.createRow(++rowCount);

                int columnCount = 1;
                row.createCell(columnCount).setCellValue(d.getRuleName());
                row.getCell(columnCount++).setCellStyle(green);
                row.createCell(columnCount).setCellValue(d.getOffertaCommerciale());
                row.getCell(columnCount++).setCellStyle(lightblue);
                row.createCell(columnCount).setCellValue(d.getTerritorio());
                row.getCell(columnCount++).setCellStyle(lightblue);
                row.createCell(columnCount).setCellValue(d.getSiaeProRata());
                row.getCell(columnCount++).setCellStyle(amarillo);
                row.createCell(columnCount).setCellValue(d.getMinimoAbbonamento());
                row.getCell(columnCount++).setCellStyle(amarillo);
                row.createCell(columnCount).setCellValue(d.getMinimumCurrency());
                row.getCell(columnCount++).setCellStyle(amarillo);
                row.createCell(columnCount).setCellValue("n/a");
                row.getCell(columnCount++).setCellStyle(amarillo);

            }

            inputStream.close();

            FileOutputStream outputStream = new FileOutputStream(excelName);
            workbook.write(outputStream);
            workbook.close();
            outputStream.close();

        } catch (IOException | EncryptedDocumentException ex) {
            ex.printStackTrace();
        }
    }

    private void writeTerritoryExcel(PriceListDTO priceListDTO, String excelName) {
        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream(territory);
            HSSFWorkbook workbook = new HSSFWorkbook(inputStream);
            HSSFCellStyle green = workbook.createCellStyle();
            green.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index);
            green.setFillPattern(CellStyle.SOLID_FOREGROUND);
            HSSFCellStyle lightblue = workbook.createCellStyle();
            lightblue.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
            lightblue.setFillPattern(CellStyle.SOLID_FOREGROUND);
            HSSFCellStyle amarillo = workbook.createCellStyle();
            amarillo.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
            amarillo.setFillPattern(CellStyle.SOLID_FOREGROUND);
            Sheet sheet = workbook.getSheetAt(0);

            int rowCount = 7;

            sheet.getRow(rowCount).getCell(4).setCellValue(priceListDTO.getDsp()+" "+anagUtilTypeRepo.idUtilizationTypeDecoded(priceListDTO.getTipoUtilizzo()).getDescription());

            rowCount = 11;

            for (PriceListDTO.Dettaglio d : priceListDTO.getRecordDettaglio()) {
                Row row = sheet.createRow(++rowCount);

                int columnCount = 1;
                row.createCell(columnCount).setCellValue(d.getRuleName());
                row.getCell(columnCount++).setCellStyle(green);
                row.createCell(columnCount).setCellValue(d.getOffertaCommerciale());
                row.getCell(columnCount++).setCellStyle(lightblue);
                row.createCell(columnCount).setCellValue(d.getTerritorio());
                row.getCell(columnCount++).setCellStyle(lightblue);
                row.createCell(columnCount).setCellValue(d.getSiaeProRata());
                row.getCell(columnCount++).setCellStyle(amarillo);
                row.createCell(columnCount).setCellValue(d.getMinimoAbbonamento());
                row.getCell(columnCount++).setCellStyle(amarillo);
                row.createCell(columnCount).setCellValue(d.getMinimumCurrency());
                row.getCell(columnCount++).setCellStyle(amarillo);
                row.createCell(columnCount).setCellValue("n/a");
                row.getCell(columnCount++).setCellStyle(amarillo);

            }

            inputStream.close();

            FileOutputStream outputStream = new FileOutputStream(excelName);
            workbook.write(outputStream);
            workbook.close();
            outputStream.close();

        } catch (IOException | EncryptedDocumentException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public boolean uploadExcelFile(File file, String amazonUrl) {
        final S3.Url s3Uri = new S3.Url(amazonUrl);
        logger.info("I'm uploading CSV FILE " + file.getName() + " s3Uri: {}", s3Uri);
        if (this.upload(s3Uri, file)) {
            logger.info("uploadCsvFile " + file.getName() + " s3Uri {}", s3Uri);
            System.out.println(s3Uri);
            return true;
        }
        return false;
    }

    public boolean upload(S3.Url url, final File file) {
        try {
            for (int retries = Integer.parseInt(put_object);
                 retries > 0; retries--) {
                try {
                    logger.debug("I'm uploading CSV FILE " + file.getName() + " to {}", file.getName(), url);
                    final PutObjectRequest putObjectRequest = new PutObjectRequest(url.bucket,
                            url.key.replace("//", "/"), file);
                    putObjectRequest.setGeneralProgressListener(new ProgressListener() {

                        private long accumulator = 0L;
                        private long total = 0L;
                        private long chunk = Math.max(256 * 1024, file.length() / 10L);

                        @Override
                        public void progressChanged(ProgressEvent event) {
                            if (ProgressEventType.REQUEST_BYTE_TRANSFER_EVENT
                                    .equals(event.getEventType())) {
                                accumulator += event.getBytes();
                                total += event.getBytes();
                                if (accumulator >= chunk) {
                                    accumulator -= chunk;
                                    logger.info("transferred {}Kb/{}Kb of file {}",
                                            total / 1024L, file.length() / 1024L, file.getName());
                                }
                            } else if (ProgressEventType.TRANSFER_COMPLETED_EVENT
                                    .equals(event.getEventType())) {
                                logger.debug("transfer of file " + file.getName() + " completed");
                            }
                        }

                    });
                    AWSCredentials credentials = new BasicAWSCredentials(
                            accessKey,
                            secretKey
                    );
                    AmazonS3 s3client = AmazonS3ClientBuilder
                            .standard()
                            .withCredentials(new AWSStaticCredentialsProvider(credentials))
                            .withRegion(region)
                            .build();
                    if (!s3client.doesBucketExist(url.bucket)) {
                        s3client.createBucket(url.bucket);
                    }
                    s3client.putObject(url.bucket, url.key + file.getName(), file);
                    logger.info("file {} successfully uploaded to {}", file.getName(), url);
                    return true;
                } catch (Exception e) {
                    logger.error("upload failed " + file.getName() + "", e);
                }
            }
        } catch (NumberFormatException | NullPointerException e) {
            file.delete();
            logger.error("upload failed " + file.getName() + "", e);
        }
        return false;
    }

    public PriceListDTO readTerritoryOrCountries(File file, PriceListDTO dto) throws IOException {
        try {
            InputStream f = new FileInputStream(file);
            //Create Workbook instance holding reference to .xlsx file
            Workbook workbook = WorkbookFactory.create(f);

            //Get first/desired sheet from the workbook
            Sheet sheet = workbook.getSheetAt(0);

            logger.debug("Ho creato l'oggetto Excel da leggere ..");

            String terrOrCountr = null;
            for (int r = 11; r <= sheet.getLastRowNum(); r++) {
                Row row = sheet.getRow(r);
                if (row != null) {
                    //For each row, iterate through all the columns
                    Iterator<Cell> cellIterator = row.cellIterator();

                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() < 4)
                            if (cell.getStringCellValue().equalsIgnoreCase(CHECKBOX_TERRITORY) || cell.getStringCellValue().equalsIgnoreCase(CHECKBOX_COUNTRIES))
                                terrOrCountr = cell.getStringCellValue();
                    }
                }
            }
            dto.setCheckboxSelezionata(terrOrCountr);
            if (terrOrCountr.equalsIgnoreCase(CHECKBOX_TERRITORY)) {
                for (int r = 12; r <= sheet.getLastRowNum(); r++) {
                    Row row = sheet.getRow(r);
                    if (row != null && !(row.getCell(1).toString().equalsIgnoreCase(""))) {
                        //For each row, iterate through all the columns
                        Iterator<Cell> cellIterator = row.cellIterator();

                        PriceListDTO.Dettaglio d = new PriceListDTO.Dettaglio();
                        while (cellIterator.hasNext()) {
                            Cell cell = cellIterator.next();
                            if (cell.getColumnIndex() > 0 && cell.getColumnIndex() < 7) {
                                //Check the cell type and format accordingly

                                switch (cell.getColumnIndex()) {
                                    case 1:
                                        d.setRuleName(cell.getStringCellValue());
                                        break;
                                    case 2:
                                        d.setOffertaCommerciale(cell.getStringCellValue());
                                        break;
                                    case 3:
                                        d.setTerritorio(cell.getStringCellValue());
                                        break;
                                    case 4:
                                        d.setSiaeProRata(cell.getStringCellValue());
                                        break;
                                    case 5:
                                        d.setMinimoAbbonamento(cell.getStringCellValue());
                                        break;
                                    case 6:
                                        d.setMinimumCurrency(cell.getStringCellValue());
                                        break;
                                }
                            }
                        }
                        if (d != null) {
                            dto.getRecordDettaglio().add(d);
                            logger.debug("Ho aggiunt la riga --> " + d.toString());
                        }
                    }
                }
            } else if (terrOrCountr.equalsIgnoreCase(CHECKBOX_COUNTRIES)) {
                for (int r = 12; r <= sheet.getLastRowNum(); r++) {
                    Row row = sheet.getRow(r);
                    if (row != null && row.getCell(1) != null && !(row.getCell(1).toString().equalsIgnoreCase(""))
                            && !(row.getCell(1).toString().equalsIgnoreCase(""))) {
                        //For each row, iterate through all the columns
                        Iterator<Cell> cellIterator = row.cellIterator();

                        PriceListDTO.Dettaglio d = new PriceListDTO.Dettaglio();
                        while (cellIterator.hasNext()) {
                            Cell cell = cellIterator.next();
                            if (cell.getColumnIndex() > 0 && cell.getColumnIndex() < 7) {
                                //Check the cell type and format accordingly

                                switch (cell.getColumnIndex()) {
                                    case 1:
                                        d.setRuleName(cell.getStringCellValue());
                                        break;
                                    case 2:
                                        d.setCountries(cell.getStringCellValue());
                                        break;
                                    case 3:
                                        d.setOffertaCommerciale(cell.getStringCellValue());
                                        break;
                                    case 4:
                                        d.setSiaeProRata(cell.getStringCellValue());
                                        break;
                                    case 5:
                                        d.setMinimoAbbonamento(cell.getStringCellValue());
                                        break;
                                    case 6:
                                        d.setMinimumCurrency(cell.getStringCellValue());
                                        break;
                                }
                            }
                        }
                        if (d != null) {
                            dto.getRecordDettaglio().add(d);
                            logger.debug("Ho aggiunt la riga --> " + d.toString());
                        }
                    }
                }
            } else {
                throw new BadRequestAlertException(INVALID_CHECKBOX_TYPE, "IMPORT PRICELIST", "errorPricelistCheckBox");
            }

            f.close();
            file.deleteOnExit();
        } catch (Exception e) {
            file.deleteOnExit();
            e.printStackTrace();
        }
        return dto;
    }

    public PriceListDTO readCloud(File file, PriceListDTO dto) throws IOException {
        try {
            InputStream f = new FileInputStream(file);
            //Create Workbook instance holding reference to .xlsx file
            Workbook workbook = WorkbookFactory.create(f);

            //Get first/desired sheet from the workbook
            Sheet sheet = workbook.getSheetAt(0);

            logger.debug("Ho creato l'oggetto Excel da leggere ..");
            for (int r = 12; r <= sheet.getLastRowNum(); r++) {
                Row row = sheet.getRow(r);
                if (row != null && row.getCell(1) != null && !(row.getCell(1).toString().equalsIgnoreCase(""))) {
                    //For each row, iterate through all the columns
                    Iterator<Cell> cellIterator = row.cellIterator();

                    PriceListDTO.Dettaglio d = new PriceListDTO.Dettaglio();
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() > 0 && cell.getColumnIndex() < 7) {
                            //Check the cell type and format accordingly

                            switch (cell.getColumnIndex()) {
                                case 1:
                                    d.setRuleName(cell.getStringCellValue());
                                    break;
                                case 2:
                                    d.setOffertaCommerciale(cell.getStringCellValue());
                                    break;
                                case 3:
                                    d.setTerritorio(cell.getStringCellValue());
                                    break;
                                case 4:
                                    d.setSiaeProRata(cell.getStringCellValue());
                                    break;
                                case 5:
                                    d.setMinimoAbbonamento(cell.getStringCellValue());
                                    break;
                                case 6:
                                    d.setMinimumCurrency(cell.getStringCellValue());
                                    break;
                            }
                        }
                    }
                    if (d != null) {
                        dto.getRecordDettaglio().add(d);
                        logger.debug("Ho aggiunt la riga --> " + d.toString());
                    }
                }
            }
            f.close();
            file.deleteOnExit();
        } catch (Exception e) {
            file.deleteOnExit();
            e.printStackTrace();
        }
        return dto;
    }

    public PriceListDTO readDownload(File file, PriceListDTO dto) throws IOException {
        try {
            InputStream f = new FileInputStream(file);
            Workbook workbook = WorkbookFactory.create(f);

            Sheet sheet = workbook.getSheetAt(0);
            int shiftColumn = 0;
            int numSiaeMinAlbum = 0;

            final int basicStartMinAlbum = 4;
            final int indexRowParametri = 10;
            final int indexRowIntestazione = 11;
            final int startIndexValuesRow = 12;

            dto.setOffertaCommerciale(false);
            dto.setTotalSales(false);

            Row rowIntestazione = sheet.getRow(indexRowIntestazione);
            if (rowIntestazione != null) {
                Iterator<Cell> cellIterator = rowIntestazione.cellIterator();
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();

                    if (cell.getStringCellValue().equalsIgnoreCase("Offerta Commerciale")) {
                        dto.setOffertaCommerciale(true);
                        shiftColumn++;
                    } else if (cell.getStringCellValue().equalsIgnoreCase("Total Sales")) {
                        dto.setTotalSales(true);
                        shiftColumn++;
                    } else if (cell.getStringCellValue().equalsIgnoreCase("SIAE Minimum Album"))
                        numSiaeMinAlbum++;
                }
            }

            String contenutoCellParametri = sheet.getRow(indexRowParametri)
                    .getCell(basicStartMinAlbum + numSiaeMinAlbum + shiftColumn).getStringCellValue();
            if (contenutoCellParametri.contains("3")) {
                dto.setNumParametri(3);
            } else {
                dto.setNumParametri(2);
            }

            boolean hasNextRow = false;
            int rowIndex = startIndexValuesRow;

            do {
                Row valuesRow = sheet.getRow(rowIndex);
                PriceListDTO.Dettaglio d = new PriceListDTO.Dettaglio();
                d.setSiaeMinimumAlbum(new ArrayList<>());
                if (valuesRow != null) {
                    int cellIndex = 1;
                    Cell ruleNameCell = valuesRow.getCell(cellIndex++);
                    if (ruleNameCell != null && ruleNameCell.getStringCellValue().length() != 0) {
                        hasNextRow = true;
                        rowIndex++;
                        d.setRuleName(ruleNameCell.getStringCellValue());

                        if (dto.getOffertaCommerciale()) {
                            d.setOffertaCommerciale(valuesRow.getCell(cellIndex++).getStringCellValue());
                        }
                        d.setTerritorio(valuesRow.getCell(cellIndex++).getStringCellValue());

                        if (dto.getTotalSales()) {
                            d.setTotalSales(valuesRow.getCell(cellIndex++).getStringCellValue());
                        }

                        d.setSiaeProRata(valuesRow.getCell(cellIndex++).getStringCellValue());
                        d.setSiaeMinimumTrack(valuesRow.getCell(cellIndex++).getStringCellValue());

                        Cell minAlbumCell;
                        int numOfColumn = cellIndex + numSiaeMinAlbum;

                        while (cellIndex < numOfColumn) {
                            minAlbumCell = valuesRow.getCell(cellIndex);
                            d.getSiaeMinimumAlbum().add(minAlbumCell.getStringCellValue());

                            cellIndex++;
                        }
                        d.setMinimumCurrency(valuesRow.getCell(cellIndex).getStringCellValue());
                        dto.getRecordDettaglio().add(d);

                    } else {
                        hasNextRow = false;
                    }
                }

            } while (hasNextRow);

            f.close();
            file.deleteOnExit();
        } catch (Exception e) {
            file.deleteOnExit();
            e.printStackTrace();
        }
        return dto;
    }

    public PriceListDTO readStreamingAdOrYouTubeAT(File file, PriceListDTO dto) throws IOException {
        try {
            InputStream f = new FileInputStream(file);
            //Create Workbook instance holding reference to .xlsx file
            Workbook workbook = WorkbookFactory.create(f);

            //Get first/desired sheet from the workbook
            Sheet sheet = workbook.getSheetAt(0);

            logger.debug("Ho creato l'oggetto Excel da leggere ..");
            for (int r = 12; r <= sheet.getLastRowNum(); r++) {
                Row row = sheet.getRow(r);
                if (row != null && row.getCell(1) != null && !(row.getCell(1).toString().equalsIgnoreCase(""))) {
                    //For each row, iterate through all the columns
                    Iterator<Cell> cellIterator = row.cellIterator();

                    PriceListDTO.Dettaglio d = new PriceListDTO.Dettaglio();
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() > 0 && cell.getColumnIndex() < 7) {
                            //Check the cell type and format accordingly

                            switch (cell.getColumnIndex()) {
                                case 1:
                                    d.setRuleName(cell.getStringCellValue());
                                    break;
                                case 2:
                                    d.setOffertaCommerciale(cell.getStringCellValue());
                                    break;
                                case 3:
                                    d.setTerritorio(cell.getStringCellValue());
                                    break;
                                case 4:
                                    d.setSiaeProRata(cell.getStringCellValue());
                                    break;
                                case 5:
                                    d.setMinimumPerStream(cell.getStringCellValue());
                                    break;
                                case 6:
                                    d.setMinimumCurrency(cell.getStringCellValue());
                                    break;
                            }
                        }
                    }
                    if (d != null) {
                        dto.getRecordDettaglio().add(d);
                        logger.debug("Ho aggiunt la riga --> " + d.toString());
                    }
                }
            }
            f.close();
            file.deleteOnExit();
        } catch (Exception e) {
            file.deleteOnExit();
            e.printStackTrace();
        }
        return dto;
    }

    public PriceListDTO readMasterlistOrYouTubeStreaming(File file, PriceListDTO dto) throws IOException {
        try {
            InputStream f = new FileInputStream(file);
            //Create Workbook instance holding reference to .xlsx file
            Workbook workbook = WorkbookFactory.create(f);

            //Get first/desired sheet from the workbook
            Sheet sheet = workbook.getSheetAt(0);

            logger.debug("Ho creato l'oggetto Excel da leggere ..");
            for (int r = 12; r <= sheet.getLastRowNum(); r++) {
                Row row = sheet.getRow(r);
                if (row != null && row.getCell(1) != null && !(row.getCell(1).toString().equalsIgnoreCase(""))) {
                    //For each row, iterate through all the columns
                    Iterator<Cell> cellIterator = row.cellIterator();

                    PriceListDTO.Dettaglio d = new PriceListDTO.Dettaglio();
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() > 0 && cell.getColumnIndex() < 7) {
                            //Check the cell type and format accordingly

                            switch (cell.getColumnIndex()) {
                                case 1:
                                    d.setRuleName(cell.getStringCellValue());
                                    break;
                                case 2:
                                    d.setOffertaCommerciale(cell.getStringCellValue());
                                    break;
                                case 3:
                                    d.setSiaeProRata(cell.getStringCellValue());
                                    break;
                                case 4:
                                    d.setMinimoAbbonamento(cell.getStringCellValue());
                                    break;
                                case 5:
                                    d.setMinimumCurrency(cell.getStringCellValue());
                                    break;
                            }
                        }
                    }
                    if (d != null) {
                        dto.getRecordDettaglio().add(d);
                        logger.debug("Ho aggiunt la riga --> " + d.toString());
                    }
                }
            }
            f.close();
            file.deleteOnExit();
        } catch (Exception e) {
            file.deleteOnExit();
            e.printStackTrace();
        }
        return dto;
    }

    public PriceListDTO readStreamingGen(File file, PriceListDTO dto) throws IOException {
        try {
            InputStream f = new FileInputStream(file);
            //Create Workbook instance holding reference to .xlsx file
            Workbook workbook = WorkbookFactory.create(f);

            //Get first/desired sheet from the workbook
            Sheet sheet = workbook.getSheetAt(0);

            logger.debug("Ho creato l'oggetto Excel da leggere ..");
            for (int r = 12; r <= sheet.getLastRowNum(); r++) {
                Row row = sheet.getRow(r);
                if (row != null && row.getCell(1) != null && !(row.getCell(1).toString().equalsIgnoreCase(""))) {
                    //For each row, iterate through all the columns
                    Iterator<Cell> cellIterator = row.cellIterator();

                    PriceListDTO.Dettaglio d = new PriceListDTO.Dettaglio();
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() > 0 && cell.getColumnIndex() <= 7) {
                            //Check the cell type and format accordingly

                            switch (cell.getColumnIndex()) {
                                case 1:
                                    d.setRuleName(cell.getStringCellValue());
                                    break;
                                case 2:
                                    d.setOffertaCommerciale(cell.getStringCellValue());
                                    break;
                                case 3:
                                    d.setTerritorio(cell.getStringCellValue());
                                    break;
                                case 4:
                                    d.setSiaeProRata(cell.getStringCellValue());
                                    break;
                                case 5:
                                    d.setMinimoAbbonamento(cell.getStringCellValue());
                                    break;
                                case 6:
                                    d.setMinimumPerStream(cell.getStringCellValue());
                                    break;
                                case 7:
                                    d.setMinimumCurrency(cell.getStringCellValue());
                                    break;
                            }
                        }
                    }
                    if (d != null) {
                        dto.getRecordDettaglio().add(d);
                        logger.debug("Ho aggiunt la riga --> " + d.toString());
                    }
                }
            }
            f.close();
            file.deleteOnExit();
        } catch (Exception e) {
            file.deleteOnExit();
            e.printStackTrace();
        }
        return dto;
    }

    private static void cloneCellValue(Cell oldCell, Cell newCell) {

        newCell.setCellStyle(oldCell.getCellStyle());
        switch (oldCell.getCellTypeEnum()) {
            case STRING:
                newCell.setCellValue(oldCell.getStringCellValue());
                break;
            case NUMERIC:
                newCell.setCellValue(oldCell.getNumericCellValue());
                break;
            case BOOLEAN:
                newCell.setCellValue(oldCell.getBooleanCellValue());
                break;
            case FORMULA:
                newCell.setCellFormula(oldCell.getCellFormula());
                break;
            case ERROR:
                newCell.setCellErrorValue(oldCell.getErrorCellValue());
            case BLANK:
            case _NONE:
                break;
        }
    }
}
