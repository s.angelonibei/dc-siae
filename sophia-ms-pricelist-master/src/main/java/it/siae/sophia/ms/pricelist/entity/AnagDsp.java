package it.siae.sophia.ms.pricelist.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "ANAG_DSP",schema = "MCMDB_debug")
@Entity
@Data
public class AnagDsp implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(insertable = false, name = "IDDSP", nullable = false)
    private String IDDSP;

    @Column(name = "CODE", nullable = false)
    private String CODE;

    @Column(name = "NAME", nullable = false)
    private String NAME;

    @Column(name = "DESCRIPTION")
    private String DESCRIPTION;

    @Column(name = "FTP_SOURCE_PATH")
    private String ftpSourcePath;

    
}