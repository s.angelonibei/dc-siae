package it.siae.sophia.ms.pricelist.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class PriceListDTO implements Serializable {

    //TESTATA
    private String dsp;
    private String dataInizioValidita;
    private String dataFineValidita;
    private String nomeFileExcel;
    private String tipoUtilizzo;
    private String checkboxSelezionata;
    private List<Dettaglio> recordDettaglio;
    private Boolean offertaCommerciale;
    private Boolean totalSales;
    private Integer numParametri;

    @Data
    public static class Dettaglio {
        private String ruleName;
        private String offertaCommerciale;
        private String territorio;
        private String countries;
        private String siaeProRata;
        private String minimumPerStream;
        private String siaeMinimumTrack; // siae Minimum 1 track
        private List<String> siaeMinimumAlbum;
        private String minimoAbbonamento;
        private String minimumCurrency;
        private String totalSales;
    }

}
