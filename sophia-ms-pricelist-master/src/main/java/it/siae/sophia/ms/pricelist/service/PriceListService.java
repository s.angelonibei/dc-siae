package it.siae.sophia.ms.pricelist.service;

import it.siae.sophia.ms.pricelist.dto.StringListDTO;
import it.siae.sophia.ms.pricelist.dto.PriceListDTO;

public interface PriceListService {
    boolean savePriceList(PriceListDTO priceListDTO) throws Exception;

    String getImportPriceList(String dsp, String tipoUtilizzo, String dataInizioValidita, String dataFineValidita, String checkboxSelezionata) throws Exception;

    StringListDTO getAllDspNames();

    StringListDTO getAllUtilizzationTypes();
}
