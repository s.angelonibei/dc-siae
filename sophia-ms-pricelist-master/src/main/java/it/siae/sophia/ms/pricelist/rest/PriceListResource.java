/*
 * @Autor A.D.C.
 */

package it.siae.sophia.ms.pricelist.rest;

import it.siae.sophia.ms.pricelist.dto.PriceListDTO;
import it.siae.sophia.ms.pricelist.dto.StringListDTO;
import it.siae.sophia.ms.pricelist.entity.DroolsPricing;
import it.siae.sophia.ms.pricelist.repository.DroolsPricingRepository;
import it.siae.sophia.ms.pricelist.service.PriceListService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Description;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.ws.rs.QueryParam;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("pricelist")
public class PriceListResource {

    private final Logger logger = LoggerFactory.getLogger(PriceListResource.class);

    @Autowired
    PriceListService service;

    @Autowired
    DroolsPricingRepository pricelistRepository;

    @PostMapping("/savePriceList")
    @Description("API to create a new Excel (or edit if it exists), from the information received by DTO")
    public boolean createReportRequest(@Valid @RequestBody PriceListDTO priceListDTO) throws Exception {
        logger.info("REST request to create a new Excel (or edit if it exists), from the information received by DTO: {} ", priceListDTO);
        return this.service.savePriceList(priceListDTO);
    }

    @GetMapping("/getPriceList")
    @Description("Api for get all PriceList ordered by id desc")
    public List<DroolsPricing> getPriceList() {
        logger.info("REST request to find all BlackList ordered by id desc : {} ");
        List<DroolsPricing> b = (this.pricelistRepository.findAll()
                .stream()
                .sorted(Comparator.comparing(DroolsPricing::getId)
                        .reversed())
                .collect(Collectors.toList()));
        return b;
    }

    @GetMapping("/importPriceList")
    @Description("Api for get a PriceList with MAX DROOLS_PRICING.VALID_FROM")
    public String getBlacklistDTO(@QueryParam("idDsp") String idDsp, @QueryParam("tipoUtilizzo") String tipoUtilizzo, @QueryParam("validFrom") String validFrom,@QueryParam("validTo") String validTo,@QueryParam("checkboxSelezionata") String checkboxSelezionata) throws Exception {
        logger.info("REST request to find a PriceList filtered by params : {} ", idDsp, tipoUtilizzo, validFrom,validTo,checkboxSelezionata);
        String b = service.getImportPriceList(idDsp, tipoUtilizzo, validFrom,validTo,checkboxSelezionata);
        return b;
    }

    @GetMapping("dsp")
    @Description("Api for get all Pricelist's dsp")
    public StringListDTO getAllDspNames() {
        return service.getAllDspNames();
    }

    @GetMapping("utilizzationTypes")
    @Description("Api for get all Pricelist's utilizzationTypes")
    public StringListDTO getAllUtilizzationTypes() {
        return service.getAllUtilizzationTypes();
    }

}
