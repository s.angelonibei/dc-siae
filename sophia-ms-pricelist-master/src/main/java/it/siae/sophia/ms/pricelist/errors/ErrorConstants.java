/*
 * @Autor A.D.C.
 */

package it.siae.sophia.ms.pricelist.errors;


public final class ErrorConstants {

    public static final String ERR_CONCURRENCY_FAILURE = "error.concurrencyFailure";
    public static final String ERR_VALIDATION = "error.validation";
    public static final String PROBLEM_BASE_URL = "sophia-ms-pricelist";
    public static final String DEFAULT_TYPE = (PROBLEM_BASE_URL + "/problem-with-message");
    public static final String CONSTRAINT_VIOLATION_TYPE = (PROBLEM_BASE_URL + "/contraint-violation");
    public static final String PARAMETERIZED_TYPE = (PROBLEM_BASE_URL + "/parameterized");
    public static final String INVALID_CHECKBOX_TYPE = (PROBLEM_BASE_URL + "/invalid-checkbox : Must be Territory or Countries");
    public static final String ROW_ALREADY_EXISTS = (PROBLEM_BASE_URL + "/row-already-exists");
    public static final String ROW_DUPLICATE = (PROBLEM_BASE_URL + "/row-duplicate");
    public static final String LOGIN_ALREADY_USED_TYPE = (PROBLEM_BASE_URL + "/login-already-used");
    public static final String EXCEL_NOT_FOUND_TYPE = (PROBLEM_BASE_URL + "/email-not-found");
    public static final String INVALID_VERSION = (PROBLEM_BASE_URL + "/invalid-version");

    private ErrorConstants() {
    }
}
