package it.siae.sophia.ms.pricelist.excel;

import it.siae.sophia.ms.pricelist.dto.PriceListDTO;

import java.io.File;

public interface ExcelUtilsService {
    boolean saveExcelFile(PriceListDTO priceListDTO, String excelName);

    boolean uploadExcelFile(File excel, String amazonUrl);
}
