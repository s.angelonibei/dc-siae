/*
 * @Autor A.D.C.
 */

package it.siae.sophia.ms.pricelist.query;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:queries/pricelist.properties")
@ConfigurationProperties(prefix = "pricelist", ignoreUnknownFields = false)
@Data
public class PricelistProperites {
    private PricelistAppends appends;
}
