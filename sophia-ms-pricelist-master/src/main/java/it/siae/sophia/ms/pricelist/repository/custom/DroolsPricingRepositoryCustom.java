package it.siae.sophia.ms.pricelist.repository.custom;

import it.siae.sophia.ms.pricelist.entity.DroolsPricing;

import java.text.ParseException;
import java.util.ArrayList;

public interface DroolsPricingRepositoryCustom {
    ArrayList findByDspAndIdUtilizationTypeAndValidFromAndValidTo(DroolsPricing droolsPricing);

    ArrayList findDuplicate(DroolsPricing droolsPricing);

    DroolsPricing findPathExcelPricelistByParams(String dsp, String tipoUtilizzo, String dataInizioValidita,String dataInizioFine) throws ParseException;
}
