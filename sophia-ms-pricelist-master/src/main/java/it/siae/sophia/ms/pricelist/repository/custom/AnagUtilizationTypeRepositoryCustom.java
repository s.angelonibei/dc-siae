package it.siae.sophia.ms.pricelist.repository.custom;

import it.siae.sophia.ms.pricelist.entity.AnagUtilizationType;

public interface AnagUtilizationTypeRepositoryCustom {
    AnagUtilizationType idUtilizationTypeDecoded (String IdUtilizzationType);
}
