package it.siae.scheduledtask;

import com.jcraft.jsch.SftpException;
import it.siae.entity.BdcInformationFileEntity;
import it.siae.entity.BdcUploadAutomation;
import it.siae.entity.BdcUtenti;
import it.siae.enums.Stato;
import it.siae.model.S3Config;
import it.siae.repository.IBdcBroadcasters;
import it.siae.repository.IBdcUploadAutomation;
import it.siae.service.InformationFileEntityService;
import it.siae.util.Constant;
import it.siae.util.SFtpClient;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.bcel.Const;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.FileSystemUtils;

import java.io.File;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

@Component
@Slf4j
public class UploadEngine {

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");

    @Autowired
    @Qualifier("CustomTaskExecutor")
    private TaskExecutor taskExecutor;

    @Value("${sftp.server}")
    private String sftpServer;

    @Value("${sftp.port}")
    private int sftpPort;

    @Value("${sftp.user}")
    private String sftpUser;

    @Value("${sftp.password}")
    private String sftpPassword;

    @Value("${sftp.ftpFolderPath}")
    private String ftpFolderPath;

    @Value("${sftp.ftpElaboratedPath}")
    private String ftpElaboratedPath;

    @Value("${sftp.localFolderPath}")
    private String localFolderPath;

    @Value("${core.emittenti}")
    private String emittenti;

    @Value("${core.repertori}")
    private String repertori;

    @Value("${core.idUtente}")
    private int idUtente;

    @Value("${core.repertorioMusicaRefSiae}")
    private String repertorioMusicaRefSiae;

    @Value("${cloud.s3.security-year-path}")
    private String securityYearPath;

    @Value("${cloud.s3.security-year-days}")
    private int securityYearDays;

    @Autowired
    private S3Config s3;

    @Autowired
    private IBdcUploadAutomation bdcUploadAutomationRepo;

    @Autowired
    private IBdcBroadcasters bdcBroadcasters;

    @Autowired
    private InformationFileEntityService fileEntityService;

    //Valore Configurabile
    @Scheduled(cron = "${core.scheduledUpload}")
    public void scheduledUploadToPlatform() throws Throwable {

        FileSystemUtils.deleteRecursively(new File(localFolderPath));

        List<String> emittentiL = Arrays.asList(emittenti.split(","));
        List<String> repertoriL = Arrays.asList(repertori.split(","));

        SFtpClient sftpClient = new SFtpClient();

        log.info("Report Upload Engine is running");
        log.info("Cron Task: Current Time - {}", formatter.format(LocalDateTime.now()));
        File localFolder = new File(localFolderPath);
        sftpClient.init(sftpServer, sftpPort, sftpUser, sftpPassword, null);
        //sftpClient.open();
        new File(localFolderPath).mkdir();
        log.info("connected to sftp");
        for (String repertorio : repertoriL) {
            new File(localFolderPath + repertorio).mkdir();
            for (String emittente : emittentiL) {
                new File(localFolderPath + repertorio + "/" + emittente).mkdir();
                log.info("copying locally remote file from sftp for emittente {} and repertorio {}", emittente, repertorio);
                log.info("copying into {}", localFolderPath + repertorio + "/" + emittente);
                sftpClient.downloadFromFolder(
                        ftpFolderPath.replace(Constant.PROP_EMITTENTE, emittente)
                                .replace(Constant.PROP_REPERTORIO, repertorio.equalsIgnoreCase(Constant.REPERTORIO_MUSICA) ? repertorioMusicaRefSiae : repertorio)
                        , localFolderPath + repertorio + "/" + emittente);
            }
        }


        if (localFolder.exists() && localFolder.isDirectory()) {
            File[] repertorioDir = localFolder.listFiles();
            if (repertorioDir != null && repertorioDir.length > 0) {
                log.info("repertori da lavorare: {}", repertorioDir.length);
                for (File rep : repertorioDir) {
                    File[] emittentiDir = rep.listFiles();
                    if (emittentiDir != null && emittentiDir.length > 0) {
                        log.info("emittenti da lavorare: {}", emittentiDir.length);
                        for (File em : emittentiDir) {
                            File[] reportList = em.listFiles();
                            if (reportList != null && reportList.length > 0) {
                                for (File report : reportList) {
                                    if (report.isDirectory()) {
                                        File[] subFolderRep = report.listFiles();
                                        if (subFolderRep != null) {
                                            for (File subRep : subFolderRep) {
                                                String subFolerName = subRep.getParentFile().getName() + "/";
                                                process(subRep, em, rep, sftpClient, subFolerName, true);
                                            }
                                        }
                                    } else {
                                        process(report, em, rep, sftpClient, null, false);
                                    }
                                }
                            }
                        }
                    }


                }
            }
        }
        log.info("Upload Engine is finished");
    }

    private void process(File report, File em, File rep, SFtpClient sftpClient, String subFolderName, boolean isSubFolder) throws ParseException, SftpException {
        log.info("[{}] start working on file...", report.getName());
        log.info("[{}] idUtente: {}", report.getName(), idUtente);

        long processingTime = System.currentTimeMillis();
        String sourcePath = ftpFolderPath.replace(Constant.PROP_EMITTENTE, em.getName()).replace(Constant.PROP_REPERTORIO, rep.getName().equalsIgnoreCase(Constant.REPERTORIO_MUSICA) ? repertorioMusicaRefSiae : rep.getName());
        String processedPath = ftpElaboratedPath.replace(Constant.PROP_EMITTENTE, em.getName()).replace(Constant.PROP_REPERTORIO, rep.getName().equalsIgnoreCase(Constant.REPERTORIO_MUSICA) ? repertorioMusicaRefSiae : rep.getName());
        String fileEXT = report.getName().substring(report.getName().lastIndexOf("."));
        String fileName = report.getName().replace(fileEXT, "_" + processingTime) + fileEXT;

        UploadJob job = new UploadJob(
                report,
                s3,
                rep.getName(),
                em.getName(),
                bdcBroadcasters,
                bdcUploadAutomationRepo,
                fileEntityService,
                idUtente,
                processingTime,
                processedPath,
                fileName);

        job.uploadToPlatform();

        if (isSubFolder) {
            sftpClient.moveToProcessed(sourcePath + subFolderName, processedPath, report.getName(), fileName);
        } else {
            sftpClient.moveToProcessed(sourcePath, processedPath, report.getName(), fileName);
        }
    }

    //Valore Configurabile
    @Scheduled(cron = "${core.scheduledYearCheck}")
    public void scheduledYearCheck() throws Throwable {
        log.info("Year Check Engine is running");
        log.info("Cron Task: Current Time - {}", formatter.format(LocalDateTime.now()));

        List<BdcUploadAutomation> processedAgeing = bdcUploadAutomationRepo.findByAgeing(securityYearDays);

        //List<String> emittentiL = Arrays.asList(emittenti.split(","));
        //List<String> repertoriL = Arrays.asList(repertori.split(","));

        SFtpClient sftpClient = new SFtpClient();
        sftpClient.init(sftpServer, sftpPort, sftpUser, sftpPassword, null);
        sftpClient.open();
        if (!processedAgeing.isEmpty()) {
            for (BdcUploadAutomation processed : processedAgeing) {
                sftpClient.checkSecurityYear(s3, processed.getPathSftpProcessed(), processed.getNomeFile(), securityYearPath, securityYearDays);
                try {
                    processed.setAged("Y");
                    bdcUploadAutomationRepo.save(processed);
                } catch (Exception e) {
                    log.error("error updating aging flag on {}", processed.getNomeFile());
                }
            }
        }
        /*for (String emittente : emittentiL) {
            for (String repertorio : repertoriL) {
                sftpClient.checkSecurityYear(ftpElaboratedPath
                                .replace(Constant.PROP_EMITTENTE, emittente)
                                .replace(Constant.PROP_REPERTORIO, repertorio.equals(Constant.REPERTORIO_MUSICA) ? repertorioMusicaRefSiae : repertorio)
                        , s3, securityYearPath, securityYearDays);
            }
        }
         */
        sftpClient.close();
        log.info("Year Check Engine is finished");
    }

    //Valore Configurabile
    @Scheduled(cron = "${core.scheduledStatusUpdate}")
    public void scheduledUpdateTable() throws Throwable {

        log.info("Update Table Engine is running");
        log.info("Cron Task: Current Time - {}", formatter.format(LocalDateTime.now()));

        BdcUtenti utenti = new BdcUtenti();
        utenti.setId(idUtente);
        List<BdcInformationFileEntity> informationFileEntities = fileEntityService.findByIdUtente(utenti);
        log.info("bdc information files retrieved");

        for (BdcInformationFileEntity fileEntity : informationFileEntities) {
            BdcUploadAutomation entity = bdcUploadAutomationRepo.findByNomeFile(fileEntity.getPercorso());
            if (entity != null) {
                entity.setStato(fileEntity.getStato().toString());
                try {
                    bdcUploadAutomationRepo.save(entity);
                } catch (Exception e) {
                    log.error("cannot save entity");
                    throw e;
                }
            }
        }

        log.info("Update Table Engine is finished");
    }
}
