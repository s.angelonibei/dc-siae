package it.siae.scheduledtask;

import com.alkemytech.sophia.commons.aws.S3;
import it.siae.entity.BdcBroadcasters;
import it.siae.entity.BdcInformationFileEntity;
import it.siae.entity.BdcUploadAutomation;
import it.siae.entity.BdcUtenti;
import it.siae.enums.Stato;
import it.siae.model.S3Config;
import it.siae.repository.IBdcBroadcasters;
import it.siae.repository.IBdcUploadAutomation;
import it.siae.service.InformationFileEntityService;
import it.siae.util.Constant;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.NonUniqueResultException;
import org.springframework.stereotype.Component;

import java.io.File;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.GregorianCalendar;
import java.util.Locale;

@Slf4j
@Component
@AllArgsConstructor
@NoArgsConstructor
public class UploadJob {

    private File report;
    private String repertorio;
    private String emittente;
    private S3Config s3;
    private IBdcUploadAutomation bdcUploadAutomationRepo;
    private IBdcBroadcasters bdcBroadcasters;
    private InformationFileEntityService informationFileEntityService;
    private Integer idUtente;
    private BdcUtenti utente;
    private boolean hasError = false;
    private String error = "";
    private long processingTime;
    private String processedPath;
    private String fileName;



    public UploadJob(File report, S3Config s3, String repertorio, String emittente, IBdcBroadcasters iBdcBroadcasters, IBdcUploadAutomation iBdcUploadAutomation, InformationFileEntityService informationFileEntityService, Integer idUtente, long processingTime, String processedPath, String fileName) {
        this.report = report;
        this.s3 = s3;
        this.repertorio = repertorio;
        this.emittente = emittente;
        this.bdcBroadcasters = iBdcBroadcasters;
        this.bdcUploadAutomationRepo = iBdcUploadAutomation;
        this.informationFileEntityService = informationFileEntityService;
        this.idUtente = idUtente;
        this.utente = new BdcUtenti();
        this.processingTime = processingTime;
        this.processedPath = processedPath;
        this.fileName = fileName;
    }

    public void uploadToPlatform() throws ParseException {

        s3.getS3().startup();
        utente.setId(idUtente);
        BdcBroadcasters broadcaster = null;
        BdcInformationFileEntity fileEntity = new BdcInformationFileEntity();
        boolean isRaiTV = false;
        String tipoEmittente = "";

        try {
            if (Constant.EMITTENTE_RAI.equalsIgnoreCase(emittente)) {
                if (report.getName().contains(Constant.RAI_TV) || report.getName().contains(Constant.RAI_RADIO)) {
                    log.info("[{}] report contains -Tv_ or -Rf_ = RAI report", report.getName());
                    if (report.getName().contains("-Tv_")) {
                        tipoEmittente = Constant.TIPO_TELEVISIONE;
                        isRaiTV = true;
                    } else {
                        tipoEmittente = Constant.TIPO_RADIO;
                    }
                    broadcaster = bdcBroadcasters.findByNameAndType(emittente, tipoEmittente);

                } else {
                    log.info("[{}] report does not contains -Tv_ or -Rf_", report.getName());
                    hasError = true;
                    error = "Il naming del file per l'emittente RAI non è valido";
                }
            } else {
                broadcaster = bdcBroadcasters.findByName(emittente);
            }
        } catch (NonUniqueResultException e) {
            log.error("[{}] Impossibile effettuare il caricamento: Rilevati più id per lo stesso emittente con tipo emittente {}", report.getName(), tipoEmittente);
            hasError = true;
            error = Constant.ERROR_NON_UNIQUE_ID;
        }

        //String fileEXT = report.getName().substring(report.getName().lastIndexOf("."));
        //String s3FileName = report.getName().replace(fileEXT, "_" + processingTime) + fileEXT;
        S3.Url s3Url = new S3.Url("s3://" + s3.getBucket() + "/" + s3.getKeyPrefix() + "/" + repertorio + "/" + emittente + "/" + fileName);
        log.info("[{}] generated s3 url to upload file: {}", report.getName(), s3Url.toString());
        s3.getS3().upload(s3Url, report);

        enrichFileEntity(fileEntity, broadcaster, isRaiTV, s3Url);

        if(broadcaster != null) {
            try {
                informationFileEntityService.save(fileEntity);
            } catch (ParseException | SQLException e) {
                log.error("[{}]" + e.getMessage(), report.getName());
                hasError = true;
                error = e.getMessage();
            }
        }

        BdcUploadAutomation entity = new BdcUploadAutomation();
        entity.setNomeFile(s3Url.toString());
        entity.setDataUploadSftp(new Timestamp(fileEntity.getDataUpload().getTime()));
        entity.setDataProcessamento(new Timestamp(processingTime));
        entity.setPathSftpProcessed(processedPath + fileName);
        entity.setAged("N");
        if (hasError) {
            entity.setStato(Stato.ERRORE.toString());
            entity.setDescErrore(error);
        } else {
            entity.setStato(Stato.CARICATO.toString());
        }
        try {
            bdcUploadAutomationRepo.save(entity);
        } catch (Exception e) {
            log.error("[{}] cannot save entity on BdcUploadAutomation", report.getName());
            log.error(e.getMessage());
        }
        report.deleteOnExit();
    }

    private void enrichFileEntity(BdcInformationFileEntity fileEntity, BdcBroadcasters bdcBroadcasters, boolean isRaiTv, S3.Url s3Url) {
        fileEntity.setEmittente(bdcBroadcasters);
        if (hasError){
            fileEntity.setStato(Stato.ERRORE);
            fileEntity.setDecodificaErrore(error);
        } else if (repertorio.equalsIgnoreCase(Constant.REPERTORIO_MUSICA)) {
            fileEntity.setStato(isRaiTv ? Stato.DA_CONVERTIRE : Stato.DA_ELABORARE);
        } else {
            fileEntity.setStato(Stato.ACQUISITO);
        }
        fileEntity.setNomeFile(report.getName());
        fileEntity.setDataUpload(GregorianCalendar.getInstance(Locale.ITALY).getTime());
        fileEntity.setsDataUpload(fileEntity.getDataUpload().getTime() + "");
        fileEntity.setPercorso(s3Url.toString());
        fileEntity.setTipoUpload(Constant.TIPO_UPLOAD_MASSIVO);
        fileEntity.setIdUtente(utente);
        fileEntity.setRepertorio(repertorio);
    }
}
