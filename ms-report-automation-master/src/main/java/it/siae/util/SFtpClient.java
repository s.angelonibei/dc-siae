package it.siae.util;

import com.alkemytech.sophia.commons.aws.S3;
import com.jcraft.jsch.*;
import it.siae.entity.BdcUploadAutomation;
import it.siae.model.S3Config;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.joda.time.DateTime;
import org.joda.time.Days;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Vector;

@Slf4j
public class SFtpClient {

    private String server;
    private int port;
    private String user;
    private String password;
    private String sftpPrivateKey;

    private JSch jsch;
    private Session session;
    private Channel channel;

    public SFtpClient() {
        jsch = new JSch();
    }


    public void init(String server, int port, String user, String password, String sftpPrivateKey) {
        this.server = server;
        this.port = port;
        this.user = user;
        this.password = password;
        this.sftpPrivateKey = sftpPrivateKey;
    }

    public void open() throws Throwable {
        if (this.sftpPrivateKey != null && !this.sftpPrivateKey.isEmpty()) {
            File privateKey = new File(this.sftpPrivateKey);
            if (privateKey.exists() && privateKey.isFile())
                this.jsch.addIdentity(this.sftpPrivateKey);
        }
        this.session = this.jsch.getSession(this.user, this.server, this.port);
        if (this.password != null)
            this.session.setPassword(this.password);
        this.session.setConfig("StrictHostKeyChecking", "no");
        this.session.setConfig("PreferredAuthentications",
                "publickey,keyboard-interactive,password");
        this.session.connect(60000);
        this.channel = session.openChannel("sftp");
        this.channel.connect();
    }

    public void close() {
        if (this.channel != null)
            this.channel.disconnect();

        if (this.session != null)
            this.session.disconnect();
    }

    public void downloadFromFolder(String remotePath, String localPath) throws SftpException {
        try {
            open();
            ChannelSftp channelSftp = (ChannelSftp) this.channel;
            Vector<ChannelSftp.LsEntry> entries = channelSftp.ls(remotePath);

            log.info("entries: {}", entries);

            //download all from root folder
            for (ChannelSftp.LsEntry en : entries) {
                if (!en.getFilename().equals(".") && !en.getFilename().equals("..") && !en.getFilename().equalsIgnoreCase("Report Processati") && en.getAttrs().isDir()) {
                    Path path = Paths.get(localPath + "/" + en.getFilename());
                    Files.createDirectories(path);
                    downloadFromFolder(remotePath + "/" + en.getFilename(), path.toString());
                    continue;
                }
                if (en.getFilename().equals(".") || en.getFilename().equals("..") || en.getFilename().equalsIgnoreCase("Report Processati")) {
                    continue;
                }

                log.info("Trying to download... {}", en.getFilename());
                channelSftp.get(remotePath + "/" + en.getFilename(), localPath + "/" + en.getFilename());
            }
        } catch (Exception e) {
            log.info(e.getMessage());
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        } finally {
            close();
        }
    }

    public int getDays(Date fileDate) {
        DateTime now = DateTime.now();
        DateTime then = new DateTime(fileDate.getTime());
        return Days.daysBetween(then, now).getDays();
    }

    public void checkSecurityYear(S3Config s3, String processedPath, String nomeFile, String securityYearPath ,int days) {
        s3.getS3().startup();
        try {
            ChannelSftp channelSftp = (ChannelSftp) this.channel;
            log.debug("file {} is older than {} days", processedPath, days);
            String fileName = nomeFile.substring(nomeFile.lastIndexOf("/"));
            S3.Url s3Url = new S3.Url("s3://" + s3.getBucket() + "/" + securityYearPath + "/" + fileName);
            File f = File.createTempFile(fileName + System.currentTimeMillis(), FilenameUtils.getExtension(fileName));
            f.deleteOnExit();
            FileUtils.copyInputStreamToFile(channelSftp.get(processedPath), f);
            if (s3.getS3().upload(s3Url, f)) {
                channelSftp.rm(processedPath);
            }
        } catch (Exception e) {
            log.info(e.getMessage());
        }
    }

    /*public void checkSecurityYear(String remotePath, S3Config s3, String path, int days) {
        s3.getS3().startup();
        try {
            ChannelSftp channelSftp = (ChannelSftp) this.channel;
            Vector<ChannelSftp.LsEntry> entries = channelSftp.ls(remotePath);
            log.info("entries: {}", entries);

            Date fDate = null;
            for (ChannelSftp.LsEntry en : entries) {
                if (!en.getFilename().equals(".") && !en.getFilename().equals("..") && !en.getAttrs().isDir()) {
                    fDate = getCreationDate(remotePath + en.getFilename());
                    log.debug("file date creation is {}", fDate);
                    if (getDays(fDate) >= days) {
                        log.debug("file is older than {} days", days);
                        S3.Url s3Url = new S3.Url("s3://" + s3.getBucket() + "/" + path + "/" + en.getFilename());
                        File f = File.createTempFile(en.getFilename() + System.currentTimeMillis(), FilenameUtils.getExtension(en.getFilename()));
                        f.deleteOnExit();
                        FileUtils.copyInputStreamToFile(channelSftp.get(remotePath + en.getFilename()), f);
                        if (s3.getS3().upload(s3Url, f)) {
                            channelSftp.rm(remotePath + en.getFilename());
                        }
                    }
                    continue;
                }
                if (en.getFilename().equals(".") || en.getFilename().equals("..")) {
                    continue;
                }

                log.info(en.getFilename());
            }
        } catch (Exception e) {
            log.info(e.getMessage());
        }
    }*/

    public void moveToProcessed(String remotePath, String remoteElaboratedPath, String fileName, String fileNameMod) throws SftpException {
        try {
            open();
            ChannelSftp channelSftp = (ChannelSftp) this.channel;
            //String fileEXT = fileName.substring(fileName.lastIndexOf("."));
            //String fileNameWithTime = fileName.replace(fileEXT, "_" + processingTime) + fileEXT;
            log.info("moving file from: {} to {}", remotePath + fileName, remoteElaboratedPath + fileNameMod);
            channelSftp.rename(remotePath + fileName, remoteElaboratedPath + fileNameMod);
        } catch (Throwable e) {
            log.error("Sftp moving to processed error: " + e);
        } finally {
            close();
        }
    }

    public Date getCreationDate(String filename) throws SftpException {
        ChannelSftp channelSftp = (ChannelSftp) this.channel;
        Vector vec = channelSftp.ls(filename);
        Date modTime = new Date();
        if (vec != null && vec.size() == 1) {
            ChannelSftp.LsEntry details = (ChannelSftp.LsEntry) vec.get(0);
            SftpATTRS attrs = details.getAttrs();

            int t = attrs.getMTime();
            modTime = new Date(t * 1000L);
        }
        return modTime;
    }
}