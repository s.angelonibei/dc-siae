package it.siae.util;

public class Constant {

    public static final String PROP_REPERTORIO = "{repertorio}";
    public static final String PROP_EMITTENTE = "{emittente}";
    public static final String EMITTENTE_RAI = "RAI";
    public static final String RAI_TV = "-Tv_";
    public static final String RAI_RADIO = "-Rf_";
    public static final String TIPO_TELEVISIONE = "TELEVISIONE";
    public static final String TIPO_RADIO = "RADIO";
    public static final String REPERTORIO_MUSICA = "MUSICA";
    public static final String TIPO_UPLOAD_MASSIVO = "MASSIVO";


    public static final String ERROR_NON_UNIQUE_ID = "Impossibile effettuare il caricamento: Rilevati più id per lo stesso emittente";
}
