package it.siae.model;

import com.alkemytech.sophia.commons.aws.S3;
import com.alkemytech.sophia.commons.guice.ConfigurationLoader;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Properties;

@Data
@JsonAutoDetect
@Component
public class S3Config {

    private String keyPrefix;

    private String bucket;

    private S3 s3;


    @Autowired
    public S3Config(Properties prop, @Value("${cloud.s3.key-prefix}") String keyPrefix,  @Value("${cloud.s3.bucket}") String bucket){
        this.s3 = new S3(new ConfigurationLoader()
                .withFilePath("configuration.properties")
                .load());
        this.keyPrefix = keyPrefix;
        this. bucket = bucket;
    }
}

