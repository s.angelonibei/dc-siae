package it.siae.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.*;

import java.io.Serializable;
import java.util.List;

@Data
@JsonAutoDetect
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class UploadDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idBroadcaster;
    private Integer idUtente;
    private String nomerepertorio;
    private List<String> filenames = null;
    private List<String> keys = null;
    private Integer idCanale;
    private Integer anno;
    private Integer mese;
    private Integer idUtilizationFile;

}
