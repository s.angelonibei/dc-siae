package it.siae.repository;

import it.siae.entity.BdcUploadAutomation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.NamedQuery;
import java.util.List;

@Repository
public interface IBdcUploadAutomation extends JpaRepository<BdcUploadAutomation, Integer> {

    @Query(value = "SELECT * FROM BDC_UPLOAD_AUTOMATION WHERE nome_file = ?1", nativeQuery = true)
    BdcUploadAutomation findByNomeFile(String filename);

    @Query(value = "SELECT * FROM BDC_UPLOAD_AUTOMATION bua where aged = \"N\" AND DATEDIFF(bua.data_processamento , CURDATE()) <= ?1", nativeQuery = true)
    List<BdcUploadAutomation> findByAgeing(int days);
}
