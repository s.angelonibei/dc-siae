package it.siae.repository;

import it.siae.entity.BdcBroadcasters;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface IBdcBroadcasters extends JpaRepository<BdcBroadcasters, Long> {

    @Query(value = "SELECT * FROM bdc_broadcasters b WHERE b.nome = ?1 and b.tipo_broadcaster = ?2", nativeQuery = true)
    BdcBroadcasters findByNameAndType(String name, String type);

    @Query(value = "SELECT * FROM bdc_broadcasters b WHERE b.nome = ?1", nativeQuery = true)
    BdcBroadcasters findByName(String name);
}
