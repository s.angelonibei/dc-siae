package it.siae;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"it.siae"})
@EnableAutoConfiguration
public class MsReportAutomation {

    public static void main(String[] args) {
        SpringApplication.run(MsReportAutomation.class);
    }

}
