package it.siae.enums;

public enum Stato {

    DA_ELABORARE("DA_ELABORARE"),
    DA_CONVERTIRE("DA_CONVERTIRE"),
    IN_CONVERSIONE("IN_CONVERSIONE"),
    ACQUISITO("ACQUISITO"),
    CARICATO("CARICATO"),
    ERRORE("ERRORE"),
    ELABORATO("ELABORATO"),
    VALIDATO("VALIDATO"),
    IN_LAVORAZIONE("IN_LAVORAZIONE"),
    ELIMINATO("ELIMINATO"),
    ANNULLATO("ANNULLATO"),
    CONFERMATO("CONFERMATO");


    String text;

    Stato(String text) {
        this.text = text;
    }


    @Override
    public String toString() {
        return text;
    }

    public static it.siae.enums.Stato valueOfDescription(String description) {

        it.siae.enums.Stato stato=null;

        for (it.siae.enums.Stato v : values()) {

            if (v.toString().equals(description)) {
                stato = v;
            }
        }

        return stato;
    }
}
