package it.siae.enums;

public enum TipoBroadcaster {
    TELEVISIONE,
    RADIO
}
