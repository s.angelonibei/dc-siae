package it.siae.service;

import it.siae.entity.BdcInformationFileEntity;
import it.siae.entity.BdcUtenti;
import it.siae.enums.Stato;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

public interface InformationFileEntityService {

    List<BdcInformationFileEntity> findByStato(Stato stato);
    BdcInformationFileEntity save(BdcInformationFileEntity entity) throws ParseException, SQLException;
    List<BdcInformationFileEntity> findByIdUtente(BdcUtenti idUtente);
}
