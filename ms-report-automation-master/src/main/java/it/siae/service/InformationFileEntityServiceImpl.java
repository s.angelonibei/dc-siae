package it.siae.service;

import it.siae.entity.BdcInformationFileEntity;
import it.siae.entity.BdcUtenti;
import it.siae.enums.Stato;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class InformationFileEntityServiceImpl implements InformationFileEntityService {

    @Autowired
    EntityManager em;

    @Override
    public List<BdcInformationFileEntity> findByStato(Stato stato) {
        final Query q = em.createQuery("SELECT a FROM BdcInformationFileEntity a WHERE a.stato=:stato")
                .setParameter("stato", stato);

        return (List<BdcInformationFileEntity>) q.getResultList();
    }

    @Override
    public BdcInformationFileEntity save(BdcInformationFileEntity entity) {
        if (StringUtils.isNotEmpty(entity.getsDataUpload())) {
            entity.setDataUpload(new Date(Long.parseLong(entity.getsDataUpload())));
        }
        if (entity.getId() == null) {
            em.persist(entity);
        } else {
            em.merge(entity);
            em.flush();
        }

        final Query q = em.createQuery("SELECT a FROM BdcInformationFileEntity a WHERE a.id=:id")
                .setParameter("id", entity.getId());

        return (BdcInformationFileEntity) q.getSingleResult();
    }

    @Override
    public List<BdcInformationFileEntity> findByIdUtente(BdcUtenti idUtente) {
        final Query q = em.createNamedQuery("BdcInformationFileEntity.findByUtente")
                .setParameter(1, idUtente);

        return (List<BdcInformationFileEntity>) q.getResultList();
    }
}
