package it.siae.entity;


import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(name = "bdc_ruoli")
public class BdcRuoli {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id", nullable=false)
    private Integer id;

    @Column(name="ruolo", nullable = false)
    private String ruolo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRuolo() {
        return ruolo;
    }

    public void setRuolo(String ruolo) {
        this.ruolo = ruolo;
    }

    public BdcRuoli() {
    }

    public BdcRuoli(String ruolo) {
        this.ruolo = ruolo;
    }
}

