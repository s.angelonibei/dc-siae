package it.siae.entity;

import it.siae.enums.Stato;
import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;

@Table(name = "BDC_UPLOAD_AUTOMATION", schema = "MCMDB_debug")
@Data
@Entity
public class BdcUploadAutomation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "nome_file")
    private String nomeFile;

    @Column(name = "data_upload_sftp")
    private Timestamp dataUploadSftp;

    @Column(name = "stato")
    private String stato;

    @Column(name = "desc_errore")
    private String descErrore;

    @Column(name = "data_processamento")
    private Timestamp dataProcessamento;

    @Column(name = "path_sftp_processed")
    private String pathSftpProcessed;

    @Column(name = "aged")
    private String aged;
}
