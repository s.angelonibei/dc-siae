<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@
	include file="../navbar.jsp"%>

<div class="bodyContents">
	<div class="mainContainer row-fluid">

		<div id="companyLogo" class="navbar commonActionsContainer noprint">
			<div class="actionsContainer row-fluid">
				<div class="span2">
					<!--  -- >
					<span class="companyLogo"><img src="images/siae_logo_siaetrasparente.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
					 -->
					<span class="companyLogo"><img src="images/logo_siae.jpg"
						title="SIAE" alt="SIAE">&nbsp;</span>
				</div>
				<span class="btn-toolbar span4">
					<div class="pageNumbers alignTop pull-right"
						style="text-align: center;">
						<span class="pageNumbersText"><strong>Aggiorna valore fatturabile</strong></span>
					</div>
				</span>
			</div>
		</div>

		<div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px;">
			<div class="listViewPageDiv">
				<!-- contenitore principale -->
			
				<div ng-show="ctrl.hideEditing">
					<div class="listViewTopMenuDiv noprint">
						<div class="listViewActionsDiv row-fluid">
							<div class="pageNumbers alignTop ">
								<span class="btn-toolbar span4"> 
									<span class="customFilterMainSpan btn-group" style="text-align: center;">
										<span class="pageNumbersText"><strong> <!-- Titolo --></strong></span>
									</span>
								</span>
							</div>
							<div class="listViewActions pull-right">
								<div class="pageNumbers alignTop ">
									<span ng-show="hasPrev() || hasNext()">
										<span class="pageNumbersText" style="padding-right: 5px">
											Record da <strong>{{getFirst()}}</strong> a <strong>{{ctrl.last}}</strong>
										</span>
										<button title="Precedente" class="btn" id="listViewPreviousPageButton" type="button" ng-click="navigateToPreviousPage()" ng-show="hasPrev()">
											<span class="glyphicon glyphicon-chevron-left"></span>
										</button>
										<button title="Successiva" class="btn" id="listViewNextPageButton" type="button" ng-click="navigateToNextPage()" ng-show="hasNext()">
											<span class="glyphicon glyphicon-chevron-right"></span>
										</button>
									</span>
								</div>
							</div>
							<div class="clearfix"></div>
							<!--  -->
						</div>
					</div>

					
					<!-- qui va il filtro-->
					<%--<div ng-include src="'pages/filters/filter-update-ccid.html'"></div>--%>

					<filter-dsp-statistics dsps-utilizations-commercial-offers-countries="dspsUtilizationsCommercialOffersCountries"
										anag-client-data="anagClientData"
										filter-parameters="filterParameters"
										on-filter-apply="apply(parameters)"
										hide-back-claim-select="true"
										hide-client-select="false">
					</filter-dsp-statistics>


					<!-- table -->
					<div class="listViewContentDiv" id="listViewContents">
						<div class="contents-topscroll noprint">
							<div class="topscroll-div" style="width: 95%">&nbsp;</div>
						</div>
						<div class="listViewEntriesDiv contents-bottomscroll">
							<div class="bottomscroll-div"
								style="width: 95%; min-height: 80px">
								<table class="table table-bordered listViewEntriesTable">
									<thead>
										<tr class="listViewHeaders">
											<th nowrap class="medium"><a href=""
												class="listViewHeaderValues">DSR</a></th>
											<th><a href="" class="listViewHeaderValues">Periodo</a></th>
											<th><a href="" class="listViewHeaderValues">Territorio</a></th>
											<th><a href="" class="listViewHeaderValues">Tipo
													di utilizzo</a></th>
											<th><a href="" class="listViewHeaderValues">Offerta
													commerciale</a></th>
											<th nowrap class="medium" style="text-align: right;"><a
												href="" class="listViewHeaderValues">Valore (&euro;)</a></th>
			
											<th style="text-align: right;"><a href=""
														class="listViewHeaderValues"> Valore fatturabile (&euro;)</a></th>
			
											<th style="text-align: right;"><a href=""
														class="listViewHeaderValues"> Valore fatturabile residuo (&euro;)</a></th>
											<th nowrap class="medium"></th>
											<th nowrap class="medium"></th>
										</tr>
									</thead>
									<tbody>
										<tr class="listViewEntries" ng-repeat="item in ctrl.data.rows">					
											<td nowrap class="medium" class="listViewEntryValue ">{{item.idDsr}}</td>
											<td class="listViewEntryValue ">{{item.periodString}}</td>
											<td class="listViewEntryValue ">{{item.country}}</td>
											<td class="listViewEntryValue ">{{item.utilizationType}}</td>
											<td class="listViewEntryValue ">{{item.commercialOffer}}</td>
											<td nowrap class="medium" class="listViewEntryValue "
												style="text-align: right;">{{formatNumber(item.totalValue)}}</td>
											<td class="listViewEntryValue " style="text-align: right;">{{item.invoiceAmount | number:2}}</td>
											<td class="listViewEntryValue " style="text-align: right;">{{item.valoreResiduo | number:2}}</td>
											<td class="listViewEntryValue "
												style="text-align: right;">	
												<button class="btn addButton" ng-click="edit(item)">
													<strong>Modifica</strong>
												</button>
											</td>

										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- /table -->
					<div style="margin-top: 15px">
						<table width="100%">
							<tr>
								<td align="right">
									<button ng-disabled="selectionIsEmpty()" class="btn addButton" ng-click="confirm()">
										<strong>Conferma</strong>
									</button>
								</td>
							</tr>
						</table>
					</div>
				</div>
				<!-- contenitore principale -->
			</div>
		</div>
	</div>		
</div>