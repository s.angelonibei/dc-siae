<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="../navbar.jsp" %>
<div class="bodyContents">
  <div class="mainContainer row-fluid">
    <div id="companyLogo" class="navbar commonActionsContainer noprint">
      <div class="actionsContainer row-fluid">
        <div class="span2"> <span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE"
              alt="SIAE">&nbsp;</span> </div>
        <span class="btn-toolbar span4">
          <div class="pageNumbers alignTop pull-right" style="text-align: center;"><span
              class="pageNumbersText"><strong>Anticipi</strong></span></div>
        </span>
      </div>
    </div>
    <div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px;">
      <div class="listViewPageDiv">
        <div class="listViewActionsDiv row-fluid" ng-if="!ctrl.advancePayment">
          <span class="btn-toolbar span4"> </span> <span class="btn-toolbar span4"> </span>
          <span class="span4 btn-toolbar">
            <div class="listViewActions pull-right"><span class="btn-group pull-right"><button class="btn addButton"
                  ng-click="showAddNewAdvancePayment($event)"><span
                    class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;<strong>Aggiungi
                    Anticipo</strong></button></span></div>
            <div class="clearfix"></div>
          </span>
        </div>
        <div>
          <div class="listViewTopMenuDiv noprint"
            ng-if="ctrl.advancePayment && (ctrl.data.hasPrev || ctrl.data.hasNext)">
            <div class="listViewActionsDiv row-fluid">
              <span class="btn-toolbar span4"> <span class="btn-group"></span></span>
              <span class="btn-toolbar span4">
                <span class="customFilterMainSpan btn-group" style="text-align: center;">
                  <div class="pageNumbers alignTop "><span class="pageNumbersText"><strong> </strong></span></div>
                </span>
              </span>
              <span class="span4 btn-toolbar">
                <div class="listViewActions pull-right">
                  <div class="pageNumbers alignTop "><span> <span class="pageNumbersText"
                        style="padding-right: 5px">Record da
                        <strong>{{ctrl.data.currentPage*ctrl.data.maxrows + 1}}</strong> a
                        <strong>{{ctrl.data.currentPage*ctrl.data.maxrows + ctrl.data.rows.length}}</strong>
                      </span><button title="Precedente" class="btn" type="button" ng-click="navigateToPreviousPage()"
                        ng-if="ctrl.data.hasPrev"><span class="glyphicon glyphicon-chevron-left"></span></button><button
                        title="Successiva" class="btn" type="button" ng-click="navigateToNextPage()"
                        ng-if="ctrl.data.hasNext"><span
                          class="glyphicon glyphicon-chevron-right"></span></button></span></div>
                </div>
                <div class="clearfix"></div>
              </span>
            </div>
          </div>
          <div ng-show="!ctrl.advancePayment">
            <filter-advance-payment selected-client="ctrl.filterParameters.selectedClient"
              on-filter-apply="filterApply(parameters)" anag-client-data="anagClientData"
              on-filter-dsp="filterDsp(value, values)"> </filter-advance-payment>
          </div>
          <div class="listViewContentDiv" id="listViewContents"
            ng-if="!ctrl.advancePayment && ctrl.data && ctrl.data.rows && ctrl.data.rows.length > 0">
            <div class="contents-topscroll noprint">
              <div class="topscroll-div" style="width: 95%">&nbsp;</div>
            </div>
            <div class="listViewEntriesDiv contents-bottomscroll">
              <div class="bottomscroll-div" style="width: 95%; min-height: 80px">
                <table class="table table-bordered listViewEntriesTable">
                  <thead>
                    <tr class="listViewHeaders">
                      <th style="width: 10%;"><a class="listViewHeaderValues">Numero fattura</a></th>
                      <th style="width: 10%;"><a class="listViewHeaderValues">Periodo di competenza</a></th>
                      <th style="width: 10%;"><a class="listViewHeaderValues">Importo originario (&euro;)</a></th>
                      <th style="width: 10%;"><a class="listViewHeaderValues">Valore residuo (&euro;)</a></th>
                      <th style="width: 10%;"><a class="listViewHeaderValues">Valore accantonato (&euro;)</a></th>
                      <th style="width: 10%;"><a class="listViewHeaderValues">Valore accantonato residuo (&euro;)</a>
                      </th>
                      <th style="width: 10%;"><a class="listViewHeaderValues">Percentuale per reclami (&#37;)</a></th>
                      <th style="width: 10%;"><a class="listViewHeaderValues">CCID associati</a></th>
                      <th style="width: 10%;">Azioni su Anticipi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr class="listViewEntries" ng-repeat="item in ctrl.data.rows">
                      <td class="listViewEntryValue ">{{item.invoiceNumber}}</td>
                      <td class="listViewEntryValue ">{{item.periodFrom}}-{{item.periodTo}}</td>
                      <td class="listViewEntryValue ">{{item.originalAmount | number: 2}}</td>
                      <td class="listViewEntryValue ">{{item.amountUsed | number: 2}}</td>
                      <td class="listViewEntryValue ">{{item.importoAccantonato | number: 2}}</td>
                      <td class="listViewEntryValue ">{{item.importoAccantonatoUtilizzabile | number: 2}}</td>
                      <td class="listViewEntryValue ">{{item.percentuale | number: 2}}</td>
                      <td align="center"><a ng-click="showCcidInfo(item)"><i title="Lista CCID Associati"
                            class="glyphicon glyphicon-info-sign alignMiddle"></i></a>&nbsp; </td>
                      <td class="listViewEntryValue" style="text-align: right">
                        <!-- Single button -->
                        <div class="btn-group">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            Azioni &nbsp;&nbsp; <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu azioni-anticipi" role="menu">
                            <li><a ng-if="item.amountUsed!==0" ng-click="useAdvancePayment(item)"
                                style="text-align: left;">Consuma</a></li>
                            <li><a ng-if="item.amountUsed!==0" ng-click="readjustAdvancePayment(item)"
                                style="text-align: left;">Riproporziona</a></li>
                            <li><a ng-if="item.amountUsed!==0" style="text-align: left;"
                                ng-click="goToAccantona(item, ctrl)">Accantona e Reclami</a></li>
                            <li><a ng-if="item.amountUsed!==0" ng-click="showUpdateAdvancePayment(item)"
                                style="text-align: left;"> <span class="icon-pencil icon-black"></span> &nbsp;&nbsp;
                                Modifica</a></li>
                            <li><a ng-if="item.amountUsed!==0" ng-click="closeAdvancePayment(item)"
                                style="text-align: left;"><span class="icon-lock icon-black"></span> &nbsp;&nbsp;
                                Chiudi</a></li>
                            <li><a ng-click="deleteDialogAnticipo(item)" style="text-align: left;"><span
                                  class="icon-trash icon-black"></span> &nbsp;&nbsp; Elimina</a></li>
                          </ul>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div ng-show="ctrl.advancePayment">
            <span class="btn-toolbar span4">
              <div class="pageNumbers alignTop pull-right" style="text-align: center; margin-top: 12px">
                <a href="" class="listViewHeaderValues">
                  {{pagina === CONSUMA ? 'Consuma' : (pagina === RIPROPORZIONA ? 'Riproporziona' : 'Chiudi')}} anticipo
                  selezionato</a></div>
            </span>
            <span class="btn-toolbar span2">
              <div class="pageNumbers alignTop pull-right" style="text-align: center;"><button
                  class="btn addButton pull-right" ng-click="cancel()"><strong>Annulla</strong></button></div>
            </span>

            <filter-dsp-statistics
              dsps-utilizations-commercial-offers-countries="dspsUtilizationsCommercialOffersCountries"
              filter-parameters="filterParameters" on-filter-apply="invoiceApply(parameters)"
              hide-back-claim-select="true" hide-client-select="true" date-from="ctrl.advancePayment.periodFrom"
              date-to="ctrl.advancePayment.periodTo" selected-client="ctrl.filterParameters.selectedClient">
            </filter-dsp-statistics>

            <table class="table table-bordered listViewEntriesTable">
              <thead>
                <tr class="listViewHeaders">
                  <th><a class="listViewHeaderValues">Cliente</a></th>
                  <th><a class="listViewHeaderValues">Numero fattura</a></th>
                  <th><a class="listViewHeaderValues">Periodo di competenza</a></th>
                  <th><a class="listViewHeaderValues">Importo originario (&euro;)</a></th>
                  <th><a class="listViewHeaderValues">Valore residuo (&euro;)</a></th>
                  <th><a class="listViewHeaderValues">Valore CCID (&euro;)</a></th>
                  <th><a class="listViewHeaderValues">CCID associati</a> </th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <tr class="listViewEntries">
                  <td class="listViewEntryValue">{{ctrl.filterParameters.selectedClient.companyName}}</td>
                  <td class="listViewEntryValue">{{ctrl.advancePayment.invoiceNumber}}</td>
                  <td class="listViewEntryValue">{{ctrl.advancePayment.periodFrom}}-{{ctrl.advancePayment.periodTo}}
                  </td>
                  <td class="listViewEntryValue">{{ctrl.advancePayment.originalAmount | number: 2}}</td>
                  <td class="listViewEntryValue">
                    {{(ctrl.advancePayment.amountUsed - ctrl.advancePayment.totalSelected>0?ctrl.advancePayment.amountUsed - ctrl.advancePayment.totalSelected : 0) | number: 2}}
                  </td>
                  <td class="listViewEntryValue">{{(cCID.total ? cCID.total : 0) | number: 2}}</td>
                  <td class="listViewEntryValue"> <a ng-click="showCcidInfo( ctrl.advancePayment)"><i
                        title="Lista CCID Associati" class="glyphicon glyphicon-info-sign alignMiddle"></i></a>&nbsp;
                  </td>
                  <td></td>
                </tr>
              </tbody>
            </table>

            <div class="listViewTopMenuDiv noprint"
              ng-if="ctrl.advancePayment && (ctrl.dataCcid.hasPrev || ctrl.dataCcid.hasNext)">
              <div class="listViewActionsDiv row-fluid">
                <span class="btn-toolbar span4"> <span class="btn-group"></span></span>
                <span class="btn-toolbar span4">
                  <span class="customFilterMainSpan btn-group" style="text-align: center;">
                    <div class="pageNumbers alignTop "><span class="pageNumbersText"><strong> </strong></span></div>
                  </span>
                </span>
                <span class="span4 btn-toolbar">
                  <div class="listViewActions pull-right">
                    <div class="pageNumbers alignTop "><span> <span class="pageNumbersText" style="padding-right: 5px">
                          Record da <strong>{{ctrl.dataCcid.currentPage*ctrl.dataCcid.maxrows+1}}</strong> a
                          <strong>{{ctrl.dataCcid.currentPage*ctrl.dataCcid.maxrows + ctrl.dataCcid.rows.length}}</strong>
                        </span>
                        <button title="Precedente" class="btn" id="listViewPreviousPageButton" type="button"
                          ng-click="navigateToPreviousCcidPage()" ng-if="ctrl.dataCcid.hasPrev"><span
                            class="glyphicon glyphicon-chevron-left"></span></button>
                        <button title="Successiva" class="btn" id="listViewNextPageButton" type="button"
                          ng-click="navigateToNextCcidPage()" ng-if="ctrl.dataCcid.hasNext"><span
                            class="glyphicon glyphicon-chevron-right"></span></button></span></div>
                  </div>
                  <div class="clearfix"></div>
                </span>
              </div>
            </div>

            <span class="btn-toolbar span4">
              <div class="pageNumbers alignTop pull-right" style="text-align: center;"><span
                  class="pageNumbersText"><strong>Seleziona CCID da
                    {{pagina === CONSUMA ? 'consumare' : (pagina === RIPROPORZIONA ? 'riproporzionare' : 'chiudere')}}</strong></span>
              </div>
            </span>

            <table class="table table-bordered listViewEntriesTable">
              <thead>
                <tr class="listViewHeaders">
                  <th>
                    <input type="checkbox" ng-model="allSelected" ng-checked="selectedItemsEqualNumberOfRows()"
                      ng-click="toggleItem(allSelected);toggleCCID()" />
                  </th>
                  <th ng-click="sort('idDsr')" class="listViewHeaderValues">DSR
                    <span
                      class="glyphicon glyphicon-sort{{sortBy==='idDsr' ? (asc ? '-by-alphabet' : '-by-alphabet-alt') : '' }}"></span>
                  </th>
                  <th ng-click="sort('periodString')" class="listViewHeaderValues">Periodo
                    <span
                      class="glyphicon glyphicon-sort{{sortBy==='periodString' ? (asc ? '-by-order' : '-by-order-alt') : '' }}"></span>
                  </th>
                  <th ng-click="sort('country')" class="listViewHeaderValues">Territorio
                    <span
                      class="glyphicon glyphicon-sort{{sortBy==='country' ? (asc ? '-by-alphabet' : '-by-alphabet-alt') : '' }}"></span>
                  </th>
                  <th ng-click="sort('utilizationType')" class="listViewHeaderValues">Tipo di utilizzo
                    <span
                      class="glyphicon glyphicon-sort{{sortBy==='utilizationType' ? (asc ? '-by-alphabet' : '-by-alphabet-alt') : '' }}"></span>
                  </th>
                  <th ng-click="sort('commercialOffer')" class="listViewHeaderValues">Offerta commerciale
                    <span
                      class="glyphicon glyphicon-sort{{sortBy==='commercialOffer' ? (asc ? '-by-alphabet' : '-by-alphabet-alt') : '' }}"></span>
                  </th>
                  <th nowrap class="medium" style="text-align: right;" ng-click="sort('numeroFattura')"
                    class="listViewHeaderValues">Valore (&euro;)
                    <span
                      class="glyphicon glyphicon-sort{{sortBy==='numeroFattura' ? (asc ? '-by-order' : '-by-order-alt') : '' }}"></span>
                  </th>
                  <th style="text-align: right;" ng-click="sort('invoiceAmount')" class="listViewHeaderValues">Valore
                    fatturabile (&euro;)
                    <span
                      class="glyphicon glyphicon-sort{{sortBy==='invoiceAmount' ? (asc ? '-by-order' : '-by-order-alt') : '' }}"></span>
                  </th>
                  <th style="text-align: right;" ng-click="sort('valoreResiduo')" class="listViewHeaderValues">Valore
                    fatturabile residuo (&euro;)
                    <span
                      class="glyphicon glyphicon-sort{{sortBy==='valoreResiduo' ? (asc ? '-by-order' : '-by-order-alt') : '' }}"></span>
                  </th>
                  <th ng-if="pagina === RIPROPORZIONA || pagina === CHIUDI" style="text-align: right"
                    class="listViewHeaderValues">
                    Valore Aggiornato (&euro;)
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr class="listViewEntries" ng-repeat="item in ctrl.dataCcid.rows">
                  <td nowrap class="medium">
                    <input type="checkbox" ng-model="item.checked"
                      ng-disabled="(!item.checked&&ctrl.advancePayment.amountUsed - ctrl.advancePayment.totalSelected<0)"
                      ng-change="toggleItem(item);toggleCCID();" />
                  </td>
                  <td nowrap class="medium" class="listViewEntryValue ">{{item.idDsr}}</td>
                  <td class="listViewEntryValue ">{{item.periodString}}</td>
                  <td class="listViewEntryValue ">{{item.country}}</td>
                  <td class="listViewEntryValue ">{{item.utilizationType}}</td>
                  <td class="listViewEntryValue ">{{item.commercialOffer}}</td>
                  <td nowrap class="medium" class="listViewEntryValue " style="text-align: right;">
                    {{item.totalValue | number: 2}}</td>
                  <td class="listViewEntryValue " style="text-align: right;">{{item.invoiceAmount | number: 2}}</td>
                  <td class="listViewEntryValue " style="text-align: right;">{{item.valoreResiduo | number: 2}}</td>
                  <td class="listViewEntryValue " style="text-align: right;"
                    ng-if="pagina === RIPROPORZIONA || pagina === CHIUDI">
                    {{(item.invoiceAmountNew ? item.invoiceAmountNew : 0) | number: 2}}</td>
                </tr>
              </tbody>
            </table>
            <div style="margin-top: 15px">
              <table width="100%">
                <tr>
                  <td align="right"> <button ng-disabled="selectionIsEmpty()" ng-if="pagina === CONSUMA"
                      class="btn addButton" ng-click="confirmUseAdvancePayment()"> <strong>Consuma</strong> </button>
                  </td>
                  <td align="right"> <button ng-disabled="selectionIsEmpty()" ng-if="pagina === RIPROPORZIONA"
                      class="btn addButton" ng-click="confirmReadjustAdvancePayment()"> <strong>Riproporziona</strong>
                    </button> </td>
                  <td align="right"> <button ng-disabled="selectionIsEmpty()" ng-if="pagina === CHIUDI"
                      class="btn addButton" ng-click="confirmCloseAdvancePayment()"> <strong>Chiudi</strong> </button>
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>