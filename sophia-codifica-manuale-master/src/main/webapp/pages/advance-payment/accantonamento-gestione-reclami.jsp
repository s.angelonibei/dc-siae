<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="../navbar.jsp" %>
<div class="bodyContents">
    <div class="mainContainer row-fluid">
        <div id="companyLogo" class="navbar commonActionsContainer noprint">
            <div class="actionsContainer row-fluid">
                <div class="span2"> <span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE"
                            alt="SIAE">&nbsp;</span> </div>
                <span class="btn-toolbar span5" style="margin-left:0px;">
                    <div class="pageNumbers alignTop pull-right" style="text-align: center;"><span
                            class="pageNumbersText"><strong>Accantonamento</strong></span></div>
                </span>
            </div>
        </div>
        <div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px;">
            <div class="listViewPageDiv">
                <div>
                    <div class="listViewTopMenuDiv noprint" ng-if="(hasPrev() || hasNext())">
                        <div class="listViewActionsDiv row-fluid">
                            <span class="btn-toolbar span4"> <span class="btn-group"></span></span>
                            <span class="btn-toolbar span4">
                                <span class="customFilterMainSpan btn-group" style="text-align: center;">
                                    <div class="pageNumbers alignTop "><span class="pageNumbersText"><strong>
                                            </strong></span></div>
                                </span>
                            </span>
                            <span class="span4 btn-toolbar">
                                <div class="listViewActions pull-right">
                                    <div class="pageNumbers alignTop "><span> <span class="pageNumbersText"
                                                style="padding-right: 5px">Record da <strong>{{ctrl.dataCcid.first +1}}</strong> a
                                                <strong>{{ctrl.dataCcid.last}}</strong></span><button title="Precedente"
                                                class="btn" type="button" ng-click="navigateToPreviousPage()"
                                                ng-if="ctrl.dataCcid.hasPrev"><span
                                             class="glyphicon glyphicon-chevron-left"></span></button><button
                                                title="Successiva" class="btn" type="button"
                                                ng-click="navigateToNextPage()" ng-if="ctrl.dataCcid.hasNext"><span
                                                    class="glyphicon glyphicon-chevron-right"></span></button></span>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </span>
                        </div>
                    </div>
                    <div class="alert alert-danger" ng-show="messages.error">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                            <span><strong>Attenzione: </strong>{{messages.error}}</span>
                        </div>
                    <div>
                            <div class="pageNumbers alignTop pull-right" style="text-align: center;"><button
                                    class="btn addButton pull-right"
                                    ng-click="goToAnticipi(ctrl)"><strong>Ritorna ad Anticipi</strong></button></div>
                        <br>
                        <br>
                        <br>
                        <table class="table table-bordered listViewEntriesTable">
                            <thead>
                                <tr class="listViewHeaders">
                                    <th style="width: 10%; text-align: center;"><a class="listViewHeaderValues">Cliente</a></th>
                                    <th style="width: 10%; text-align: center;"><a class="listViewHeaderValues">Numero fattura</a></th>
                                    <th style="width: 10%; text-align: center;"><a class="listViewHeaderValues">Periodo di competenza</a></th>
                                    <th style="width: 10%; text-align: center;"><a class="listViewHeaderValues">Importo originario (&euro;)</a></th>
                                    <th style="width: 10%; text-align: center;"><a class="listViewHeaderValues">Valore residuo (&euro;)</a></th>
                                    <th style="width: 10%; text-align: center;"><a class="listViewHeaderValues">Valore accantonato (&euro;)</a></th>
                                    <th style="width: 10%; text-align: center;"><a class="listViewHeaderValues">Valore accantonato residuo (&euro;)</a></th>
                                    <th style="width: 10%; text-align: center;"><a class="listViewHeaderValues">Percentuale per reclami (&#37;)</a></th>
                                    <th style="width: 5%; text-align: center;"><a class="listViewHeaderValues">CCID Associati</a> </th>
                                    <th style="width: 10%; text-align: center;">Azioni</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="listViewEntries">
                                    <td class="listViewEntryValue" style="width: 10%; text-align: center;">{{ctrl.filterParameters.selectedClient.companyName}}</td>
                                    <td class="listViewEntryValue" style="width: 10%; text-align: center;">{{item.invoiceNumber}}</td>
                                    <td class="listViewEntryValue" style="width: 10%; text-align: center;">
                                        {{item.periodFrom}}&nbsp;/&nbsp;{{item.periodTo}}</td>
                                    <td class="listViewEntryValue" style="width: 10%; text-align: center;">{{item.originalAmount | number: 2}}
                                    </td>
                                    <td class="listViewEntryValue" style="width: 10%; text-align: center;">
                                        {{item.amountUsed | number: 2}}
                                    </td>
                                    <td class="listViewEntryValue " style="width: 10%; text-align: center;">{{item.importoAccantonato | number: 2}}</td>
                                    <td class="listViewEntryValue " style="width: 10%; text-align: center;">{{item.importoAccantonatoUtilizzabile | number: 2}}</td>
                                    <td class="listViewEntryValue " style="width: 10%; text-align: center;">
                                        <span> {{item.percentuale | number: 2}} &nbsp;
                                            <i class="fas fa-exclamation-circle" data-toggle="tooltip-note-reclami" 
                                            title={{item.note}}
                                            data-html="true" rel="tooltip"></i>
                                            <script type="text/javascript">
                                                $('[data-toggle="tooltip-note-reclami"]').tooltip();
                                            </script>
                                        </span>	 </td>
                                    <td class="listViewEntryValue" style="text-align: center;"> <a ng-click="showCcidReclamiInfo(item)">
                                        <i title="Lista CCID Associati" class="glyphicon glyphicon-info-sign"></i></a>&nbsp; 
                                    </td>        
                                    <td style="text-align: center;" style="width: 10%; text-align: center;"><button class="btn addButton" ng-click="openModal(item)"
                                        ng-show="item.importoAccantonato"><span id="modifica"> Modifica </span></button>
                                        <button class="btn addButton" ng-click="openModal(item)" ng-show="!item.importoAccantonato" ><span 
                                            id="crea"> Crea </span></button></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="listViewTopMenuDiv noprint"
                            ng-if="(hasPrevCcid() || hasNextCcid())">
                            <div class="listViewActionsDiv row-fluid">
                                <span class="btn-toolbar span4"> <span class="btn-group"></span></span>
                                <span class="btn-toolbar span4">
                                    <span class="customFilterMainSpan btn-group" style="text-align: center;">
                                        <div class="pageNumbers alignTop "><span class="pageNumbersText"><strong>
                                                </strong></span></div>
                                    </span>
                                </span>
                                <span class="span4 btn-toolbar">
                                    <div class="listViewActions pull-right">
                                        <div class="pageNumbers alignTop "><span> <span class="pageNumbersText"
                                                    style="padding-right: 5px">Record da
                                                    <strong>{{getFirstCcid()}}</strong> a
                                                    <strong>{{ctrl.ccidLast}}</strong></span><button title="Precedente"
                                                    class="btn" id="listViewPreviousPageButton" type="button"
                                                    ng-click="navigateToPreviousCcidPage()" ng-if="hasPrevCcid()"><span
                                                        class="glyphicon glyphicon-chevron-left"></span></button><button
                                                    title="Successiva" class="btn" id="listViewNextPageButton"
                                                    type="button" ng-click="navigateToNextCcidPage()"
                                                    ng-if="hasNextCcid()"><span
                                                        class="glyphicon glyphicon-chevron-right"></span></button></span>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </span>
                            </div>
                        </div>
                    </div>

                        <br>
                        <br>
                        <div>
                        <div style="text-align: center; width: 100%"><span
                        class="pageNumbersText" style="float: center;"><strong>Gestione Reclami</strong></span></div>
                            <filter-dsp-statistics dsps-utilizations-commercial-offers-countries="dspsUtilizationsCommercialOffersCountries"
                                                   filter-parameters="filterParameters"
                                                   on-filter-apply="apply(parameters)"
                                                   hide-back-claim-select="true"
                                                   hide-client-select="true"
                                                   selected-client="ctrl.filterParameters.selectedClient"
                                                   date-from="ctrl.selectedPeriodFrom"
                                                   date-to="ctrl.selectedPeriodTo">
                            </filter-dsp-statistics>
                        <br>
                        <br>
                        <span class="btn-toolbar span4">
                            <div class="pageNumbers alignTop" style="text-align: center;"><span
                                    class="pageNumbersText"><strong>Seleziona CCID reclami</strong></span>
                            </div>
                        </span>
                        <table class="table table-bordered listViewEntriesTable">
                            <thead>
                                <tr class="listViewHeaders">
                                    <th>
                                        <input type="checkbox" ng-model="allSelected"
                                            ng-checked="selectedItemsEqualNumberOfRows()"
                                            ng-click="toggleItem(allSelected);toggleCCID()" />
                                    </th>
                                    <th ng-click="sort('idDsr')" class="listViewHeaderValues">DSR
                                        <span
                                            class="glyphicon glyphicon-sort{{sortBy==='idDsr' ? (asc ? '-by-alphabet' : '-by-alphabet-alt') : '' }}"></span>
                                    </th>
                                    <th ng-click="sort('periodString')" class="listViewHeaderValues">Periodo
                                        <span
                                            class="glyphicon glyphicon-sort{{sortBy==='periodString' ? (asc ? '-by-order' : '-by-order-alt') : '' }}"></span>
                                    </th>
                                    <th ng-click="sort('country')" class="listViewHeaderValues">Territorio
                                        <span
                                            class="glyphicon glyphicon-sort{{sortBy==='country' ? (asc ? '-by-alphabet' : '-by-alphabet-alt') : '' }}"></span>
                                    </th>
                                    <th ng-click="sort('utilizationType')" class="listViewHeaderValues">Tipo di utilizzo
                                        <span
                                            class="glyphicon glyphicon-sort{{sortBy==='utilizationType' ? (asc ? '-by-alphabet' : '-by-alphabet-alt') : '' }}"></span>
                                    </th>
                                    <th ng-click="sort('commercialOffer')" class="listViewHeaderValues">Offerta
                                        commerciale
                                        <span
                                            class="glyphicon glyphicon-sort{{sortBy==='commercialOffer' ? (asc ? '-by-alphabet' : '-by-alphabet-alt') : '' }}"></span>
                                    </th>
                                    <th nowrap class="medium" style="text-align: right;" ng-click="sort('totalValue')"
                                        class="listViewHeaderValues">Valore (&euro;)
                                        <span
                                            class="glyphicon glyphicon-sort{{sortBy==='totalValue' ? (asc ? '-by-order' : '-by-order-alt') : '' }}"></span>
                                    </th>
                                    <th style="text-align: right;" ng-click="sort('invoiceAmount')"
                                        class="listViewHeaderValues">Valore fatturabile (&euro;)
                                        <span
                                            class="glyphicon glyphicon-sort{{sortBy==='invoiceAmount' ? (asc ? '-by-order' : '-by-order-alt') : '' }}"></span>
                                    </th>
                                    <th style="text-align: right;" ng-click="sort('valoreResiduo')"
                                        class="listViewHeaderValues">Valore fatturabile residuo (&euro;)
                                        <span
                                            class="glyphicon glyphicon-sort{{sortBy==='valoreResiduo' ? (asc ? '-by-order' : '-by-order-alt') : '' }}"></span>
                                    </th>
                                    <th ng-if="pagina === RIPROPORZIONA || pagina === CHIUDI" style="text-align: right"
                                        class="listViewHeaderValues">
                                        Valore Aggiornato (&euro;)
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="listViewEntries" ng-repeat="ccid in ctrl.dataCcid.rows">
                                    <td nowrap class="medium">
                                        <input type="checkbox" ng-model="ccid.checked"
                                            ng-disabled="(!ccid.checked&&ctrl.advancePayment.amountUsed - ctrl.advancePayment.totalSelected<0)"
                                            ng-change="toggleItem(ccid);toggleCCID();" />
                                    </td>
                                    <td nowrap class="medium" class="listViewEntryValue ">{{ccid.idDsr}}</td>
                                    <td class="listViewEntryValue ">{{ccid.periodString}}</td>
                                    <td class="listViewEntryValue ">{{ccid.country}}</td>
                                    <td class="listViewEntryValue ">{{ccid.utilizationType}}</td>
                                    <td class="listViewEntryValue ">{{ccid.commercialOffer}}</td>
                                    <td nowrap class="medium" class="listViewEntryValue " style="text-align: right;">
                                        {{ccid.totalValue | number: 2}}</td>
                                    <td class="listViewEntryValue " style="text-align: right;">
                                        {{ccid.invoiceAmount | number: 2}}</td>
                                    <td class="listViewEntryValue " style="text-align: right;">
                                        {{ccid.valoreResiduo | number: 2}}</td>
                                    <td class="listViewEntryValue " style="text-align: right;"
                                        ng-if="pagina === RIPROPORZIONA || pagina === CHIUDI">
                                        {{(ccid.invoiceAmountNew ? ccid.invoiceAmountNew : 0) | number: 2}}</td>
                                </tr>
                                <tr class="listViewEntries">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td style="text-align:right;"> <button  class="btn addButton" ng-click="addCcidReclamo(item)"> <strong>Associa</strong> </button> </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>