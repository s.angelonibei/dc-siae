<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../navbar.jsp"%>
<!-- path has to be adjusted-->

<div class="bodyContents" ng-init="init()">
	<div class="mainContainer row-fluid">

		<div class="contents-topscroll noprint">
			<div class="topscroll-div" style="width: 95%;margin-bottom: 20px">&nbsp;</div>
		</div>

		<div class="sticky">
			<div id="companyLogo" class="navbar commonActionsContainer noprint">
				<div class="actionsContainer row-fluid">
					<div class="span2">
						<span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
					</div>
					<span class="btn-toolbar span5">
						<div class="pageNumbers alignTop pull-right" style="text-align: center;">
							<span id="up" class="pageNumbersText"><strong>{{ctrl.pageTitle}}</strong></span>
						</div>
					</span>
				</div>
			</div>
		</div>

		<div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px;margin-top: 25px;">
			<div class="listViewPageDiv">
				<!-- messaggi -->
				<div class="alert alert-warning" ng-show="ctrl.messages.warning" style="margin-top:20px">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
					<span>{{ctrl.messages.warning}}</span>
				</div>
				<div class="alert alert-danger" ng-show="ctrl.messages.error" style="margin-top:20px">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
					<span><strong>Attenzione: </strong>{{ctrl.messages.error}}</span>
				</div>
				<div class="alert alert-success" ng-show="ctrl.messages.info" style="margin-top:20px">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
					<span>{{ctrl.messages.info}}</span>
				</div>
			</div>

			<div class="listViewPageDiv">
				<div class="listViewActionsDiv row-fluid">
					<span class="span12 btn-toolbar">
						<div class="listViewActions pull-right">
							<span class="btn-group">
								<button class="btn addButton" ng-click="editAlert()">
									<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;<strong>Nuova Mail di
										Notifica</strong>
								</button>
							</span>
						</div>
						<div class="clearfix"></div>
					</span>
				</div>
			</div>

			<div class="listViewPageDiv">
				<!-- results table -->
				<div class="listViewContentDiv" id="listViewContents" ng-show="ctrl.data.alerts.length != 0">

					<div class="contents-topscroll noprint">
						<div class="topscroll-div" style="width: 95%">&nbsp;</div>
					</div>

					<div class="listViewEntriesDiv contents-bottomscroll">
						<div class="bottomscroll-div" style="width: 95%; min-height: 80px">
							<table class="table table-bordered listViewEntriesTable">
								<thead>
									<tr class="listViewHeaders">
										<th style="width: 25%">DSP</th>
										<th style="width: 12.5%">Claim in Errore</th>
										<th style="width: 12.5%">Claim Terminato</th>
										<th style="width: 12.5%">CCID Negativo</th>
										<th style="width: 25%">Indirizzi</th>
										<th style="width: 12.5%">Azioni</th>
									</tr>
								</thead>
								<tbody>
									<tr class="listViewEntries" ng-repeat="item in ctrl.data.alerts">
										<td class="listViewEntryValue medium" ng-init="dspviewKey=(longerThan(mapDsps(item.dsps),'description',80)? 'dspNamesShortened':'dspNames'); addressviewKey=(longerThan(item.addresses,'value',80)? 'addressesShortened':'addressesFull')">
											<span
												ng-show="longerThan(mapDsps(item.dsps),'description',80) && dspviewKey == 'dspNamesShortened'">{{arrayShortenedJoined(mapDsps(item.dsps),'description',80)}}...<span
													class="glyphicon glyphicon-collapse-down"
													ng-click="dspviewKey='dspNames'"></span>
											</span>
											<span
												ng-show="longerThan(mapDsps(item.dsps),'description',80) && dspviewKey == 'dspNames'">{{arrayJoined(mapDsps(item.dsps),'description',80)}}&nbsp;&nbsp;&nbsp;<span
													class="glyphicon glyphicon-collapse-up"
													ng-click="dspviewKey='dspNamesShortened'"></span>
											</span>
											<span ng-show="!longerThan(mapDsps(item.dsps),'description',80)">{{arrayJoined(mapDsps(item.dsps),'description',80)}}</span>
										</td>
										<td class="listViewEntryValue medium">
											<span class="glyphicon glyphicon-ok" ng-show="item.failedClaim"></span>
											<span class="glyphicon glyphicon-remove" ng-show="!item.failedClaim"></span>
										</td>
										<td class="listViewEntryValue medium">
											<span class="glyphicon glyphicon-ok" ng-show="item.completedClaim"></span>
											<span class="glyphicon glyphicon-remove"
												ng-show="!item.completedClaim"></span>
										</td>
										<td class="listViewEntryValue medium">
											<span class="glyphicon glyphicon-ok" ng-show="item.negativeClaim"></span>
											<span class="glyphicon glyphicon-remove"
												ng-show="!item.negativeClaim"></span>
										</td>
										<td class="listViewEntryValue medium">
											<span
												ng-show="longerThan(item.addresses,'value',80) && addressviewKey == 'addressesShortened'">{{arrayShortenedJoined(item.addresses,'value',80)}}...<span
													class="glyphicon glyphicon-collapse-down"
													ng-click="addressviewKey='addressesFull'"></span>
											</span>
											<span
												ng-show="longerThan(item.addresses,'value',80) && addressviewKey == 'addressesFull'">{{arrayJoined(item.addresses,'value',80)}}&nbsp;&nbsp;&nbsp;<span
													class="glyphicon glyphicon-collapse-up"
													ng-click="addressviewKey='addressesShortened'"></span>
											</span>
											<span ng-show="!longerThan(item.addresses,'value',80)">{{arrayJoined(item.addresses,'value',80)}}</span>
										</td>
										<td>
											<a ng-click="deleteAlert(item)"><i title="Elimina"
													class="glyphicon glyphicon-trash "></i></a>&nbsp;
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<%--<a ng-click="editAlert(item)"><i title="Modifica"--%>
													<%--class="glyphicon glyphicon-edit "></i></a>&nbsp;--%>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- results table -->
			</div>
		</div>
	</div>
</div>
<!-- <pre>{{ctrl | json}}</pre> -->