codmanApp.service('ricercaCodificatoService', ['$http', 'configService', '$location', function ($http, configService, $location) {

    var service = this;
    const baseUrl = "rest/new-load-identified/";
    const codificatoList = "searchCodificato";
    const getDsrByHashId = "getDsrByHashId/{id}";
    const insertManual = "insertManual";
    const addToBlacklist = "addToBlacklist";

    service.prefixUrlRequest = `${$location.protocol()}://${$location.host()}:${$location.port()}/${baseUrl}`;

    /* 
       input parameter:
           "titolo": string
            "artista": string
            "codiceUUID": string-number?
            "priorita": string
            "flagCodificaManuale": boolean
            first: number
            last: number
    */
    service.searchCodificato = function (request) {
        const { title, uuid, artists, first, last } = request;
        return $http({
            method: 'GET',
            url: baseUrl + codificatoList,
            params: {
                title,
                uuid,
                artists,
                first,
                last
            }
        });

    };

    service.getDsrByHashId = function (hashId) {

    /*     mock = [{
            idUtil: "spotify_siae_IT_202006_free_dsr_241323",
            hashId: "152c3ab497ef0c3d3811778dba1d350ebd4a4ee6",
            idDsr: "spotify_siae_IT_202006_free_dsr",
            albumTitle: "Mediterranea",
            proprietaryId: "IT",
            isrc: "ITQ002000349",
            iswc: "",
            salesCount: 9495378,
            dsp: "spotify",
            insertTime: 1608713873651
        }];

        return new Promise((resolve) => {
            resolve(mock)
        }); */
        return $http({
            method: 'GET',
            url: `${baseUrl}${getDsrByHashId}`.replace("{id}", hashId)
        });
    };
    /* 
    IdentifiedSong
        private String hashId;
        private String title;
        private String artists;
        private String siadaTitle;
        private String siadaArtists;
        private String roles;
        private String siaeWorkCode; -codice opera
        ----------------------------------
        private String identificationType;
        private Long insertTime;
        private Long sophiaUpdateTime;
     */
    service.insertManual = function (data) {
        return $http({
            method: 'PUT',
            url: `${baseUrl}${insertManual}`,
            data
        });
    };
    /* 
    *   title - siadaTitle
    *   artists - siadaArtists
    *   uuid
    *  */
    service.addToBlacklist = function (data) {
        return $http({
            method: 'POST',
            url: `${baseUrl}${addToBlacklist}`,
            data
        });
    };

}]);