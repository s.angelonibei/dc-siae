<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <%@ include file="../navbar.jsp" %>

        <div class="bodyContents">

            <div class="mainContainer row-fluid">

                <div class="contents-topscroll noprint">
                    <div class="topscroll-div" style="width: 95%; margin-bottom: 20px">&nbsp;</div>
                </div>

                <div class="sticky">
                    <div id="companyLogo" class="navbar commonActionsContainer noprint">
                        <div class="actionsContainer row-fluid">
                            <div class="span2">
                                <span class="companyLogo">
                                    <img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;
                                </span>
                            </div>
                            <span class="btn-toolbar span8">
                                <span class="customFilterMainSpan btn-group" style="text-align: center;">
                                    <div class="pageNumbers alignTop ">
                                        <span class="pageNumbersText">
                                            <strong>Ricerca opere codificate</strong>
                                        </span>
                                    </div>
                                </span>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px;"
                    id="lthroughContainer">
                    <div class="listViewPageDiv">
                        <div class="alert alert-danger" ng-show="messages.error && (!messages.info)">
                            <button type="button" class="close" ng-click="clearMessages()" aria-hidden="true">x</button>
                            <span><strong>Errore: </strong>{{messages.error}}</span>
                        </div>
                        <div class="alert alert-success" ng-show="messages.info && (!messages.error)">
                            <button type="button" class="close" ng-click="clearMessages()" aria-hidden="true">x</button>
                            <span>{{messages.info}}</span>
                        </div>
                        <div class="listViewActionsDiv row-fluid" style="padding-top: 8px;">
                            <!-- Campi di ricerca -->

                            <div class="equalSplit table-bordered flexRowHNoneVCenter paddingLeftx8"
                                style="background-color: #f5f5f5;">
                                <span class="glyphicon glyphicon-expand paddingRightLeftx4">
                                </span>
                                <div class="blockHeader titleBlockFont " colspan="6">Parametri Ricerca </div>
                            </div>
                            <div class="table-bordered flexRowCenterSpaceEvenly " style="padding:8px;">

                                <div class="flexRowHCenterVCenter">
                                    <div style=" font:bold;">Titolo:</div>
                                    <div class="fieldValue medium flex-100">
                                        <input type="text" class="form-control maxWidth15" ng-model="search.title">
                                    </div>
                                </div>

                                <div class="flexRowHCenterVCenter">
                                    <div style=" font:bold;">Artista:</div>
                                    <div class="fieldValue medium flex-100">
                                        <input type="text" class="form-control maxWidth15" ng-model="search.artists">
                                    </div>
                                </div>

                                <div class="flexRowHCenterVCenter">
                                    <div style=" font:bold;">Codice UUID:</div>
                                    <div class="fieldValue medium flex-100">
                                        <input type="text" class="form-control maxWidth15" ng-model="search.uuid">
                                    </div>
                                </div>
                            </div>
                            <div class="equalSplit table-bordered"
                                style="padding: 6px 0px; background-color: #f5f5f5;margin: 0px;">

                                <div class="flex-100"
                                    style="display:flex;justify-content:flex-end;flex: right;width: 100%;">
                                    <button class="btn addButton marginRightLeftx4" ng-click="ricerca()">
                                        <span class="glyphicon glyphicon-search"></span>
                                        <strong>Ricerca</strong>
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="flexColumnStretch">
                            <!-- pagination -->
                            <div class="paddingTopBottomx8">
                                <span class="flexRowHCenterVCenter  pull-right"
                                    ng-show="paging.hasNext || paging.hasPrev">
                                    <span class="pageNumbersText" style="padding-right: 5px">
                                        Record da <strong>{{paging.firstRecord}}</strong> a
                                        <strong>{{paging.lastRecord}}</strong>
                                    </span>
                                    <button title="Precedente" class="btn marginRightLeftx4"
                                        id="listViewPreviousPageButton" type="button"
                                        ng-click="navigateToPreviousPage()" ng-show="paging.hasPrev">
                                        <span class="glyphicon glyphicon-chevron-left"></span>
                                    </button>
                                    <button title="Successiva" class="btn marginRightLeftx4" id="listViewNextPageButton"
                                        type="button" ng-click="navigateToNextPage()" ng-show="paging.hasNext">
                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                    </button>
                                </span>
                            </div>
                            <div id="ricercaCodificatoTable">
                                <div class="table-responsive text-nowrap">
                                    <table class="table table-bordered listViewEntriesTable">
                                        <thead>
                                            <tr>
                                                <th ng-repeat="head in headerTitles" ng-class="{
                                                    'max-W20' : 'Titolo'== head || head == 'Artista' || head == 'Flag codifica manuale',
                                                    'max-W30' : head == 'Codice UUID' ,
                                                    'max-W10': head == 'Priorita'
                                                }">{{head}}</th>
                                                <!-- empty column for download button -->
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr mdbTableCol ng-repeat="item in dataTable track by $index">
                                                <td>{{item.title}}</td>
                                                <td>{{item.artists}}</td>
                                                <td>{{item.uuid}}</td>
                                                <td>{{item.salesCount}}</td>
                                                <td class="textAlignCenter">
                                                    <span class="glyphicon" ng-class="{
                                                        'glyphicon glyphicon-ok': item.identificationType == true,
                                                      }" aria-hidden="true">
                                                    </span>
                                                </td>
                                                <td>
                                                    <div class="actions pull-right">
                                                        <span  aria-hidden="true">
                                                            <a ng-click="gotoRiconoscimento(item.hashId,'<%=(String) session.getAttribute("sso.user.userName")%>')">
                                                                <i title="Riconosci"
                                                                    class="glyphicon glyphicon-eye-open alignMiddle"></i>
                                                            </a>
                                                        </span>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div ng-show="!dataTable.length" style="margin-top: 16px;">
                                        <div style="text-align: center;">Al momento non ci sono record.</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>