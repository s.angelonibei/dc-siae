codmanApp.controller('ricercaCodificatoCtrl', ['$scope', 'ricercaCodificatoService', '$location',
    function ($scope, ricercaCodificatoService, $location) {

        $scope.search = {
            title: null,
            artists: null,
            uuid: null
        };
        $scope.bodyParams = {
            first: 0,
            last: 50
        };
        $scope.defaultPaging = {
            maxRows: 50,
            hasNext: false,
            hasPrev: false,
            firstRecord: 1,
            lastRecord: 50,
        };
        $scope.paging = { ...$scope.defaultPaging };
        $scope.headerTitles = ["Titolo", "Artista", "Codice UUID", "Priorità", "Flag codifica manuale"];

        $scope.messages = {
            info: "",
            error: "",
            warning: ""
        };
        $scope.dataTable = [];

        $scope.clearMessages = function () {
            $scope.messages = {
                info: "",
                error: "",
                warning: ""
            }
        };

        $scope.warningMessage = function (message) {
            $scope.messages.warning = message;
        };

        $scope.infoMessage = function (message) {
            $scope.messages.info = message;
        };

        $scope.errorMessage = function (message) {
            $scope.messages.error = message;
        };

        $scope.init = function () {

            $scope.getTableData();
        };

        $scope.getTableData = function () {
            $scope.clearMessages();
            $scope.bodyParams = Object.assign({
                ...$scope.bodyParams
            }, $scope.search);
            ricercaCodificatoService
                .searchCodificato($scope.bodyParams)
                .then(({ data }) => {
                    $scope.clearMessages();
                    setTimeout(() => {

                        const { rows = [], first, last, hasNext, hasPrev } = data;
                        $scope.dataTable = rows
                            .map(r => ({ ...r, identificationType: !!r.identificationType }));

                        if (!$scope.dataTable.length) {
                            $scope.paging = { ...$scope.defaultPaging };
                        } else {
							$scope.paging = {
								...$scope.paging,
								firstRecord: first + 1,
								lastRecord: last,
								hasNext,
								hasPrev
							};
						}
                        $scope.$apply();
                    }, 100);
                }, function (error) {
                    $scope.errorMessage('Qualcosa è andato storto.');
                });

        };

        $scope.init();

        $scope.navigateToNextPage = function () {
            const first = $scope.bodyParams.first + $scope.paging.maxRows;
            const last = $scope.bodyParams.last + $scope.paging.maxRows;
            // update first, last value
            $scope.bodyParams = {
                first,
                last
            };
            $scope.getTableData();
        };

        $scope.navigateToPreviousPage = function () {
            const first = $scope.bodyParams.first - $scope.paging.maxRows;
            const last = $scope.bodyParams.last - $scope.paging.maxRows;
            // update first, last value
            $scope.bodyParams = {
                first,
                last
            };
            $scope.getTableData();
        };

        $scope.ricerca = function () {
            $scope.bodyParams.first = 0;
            $scope.bodyParams.last = $scope.defaultPaging.maxRows;
            $scope.getTableData();
        };

        $scope.gotoRiconoscimento = function (code, username) {
            if (!code) {
                $scope.errorMessage("HashId mancante.");
                return;
            }
            $location.search('hashId', code);
            $location.path('/scodifica-ricodifica');
        };

    }]);