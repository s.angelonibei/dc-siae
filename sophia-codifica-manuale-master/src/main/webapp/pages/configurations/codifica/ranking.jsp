<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../navbar.jsp"%>


<div class="bodyContents" id="panel">
    <div class="mainContainer row-fluid">

        <div class="contents-topscroll noprint">
            <div class="topscroll-div" style="width: 95%;margin-bottom: 20px">&nbsp;</div>
        </div>

        <div class="sticky">
            <div id="companyLogo" class="navbar commonActionsContainer noprint">
                <div class="actionsContainer row-fluid">
                    <div class="span2">
                        <span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
                    </div>
                    <span class="btn-toolbar span5" style="margin-left:0%;">
                        <div class="pageNumbers alignTop pull-right" style="text-align: center;">
                            <span id="up" class="pageNumbersText"><strong>{{pageTitle}}</strong></span>
                        </div>
                    </span>
                </div>
            </div>
        </div>
       
   
        <div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px; margin-top: 25px;">
            <div class="listViewPageDiv" ng-init="collapseConf=false">
                    <div class="alert alert-danger" ng-show="messages.error">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                            <span><strong>Attenzione: </strong>{{messages.error}}</span>
                        </div>
                    <form name="configurationForm" autocomplete="off">

                <div class="listViewActionsDiv row-fluid">
                    <table class="table table-bordered blockContainer showInlineTable equalSplit noHoverTable">
                        <thead>
                                <tr>
                                        <th class="blockHeader" colspan="6" ng-show="collapseConf"><span
                                                ng-click="collapseConf=false"
                                                class="glyphicon glyphicon-expand"></span>&nbsp;{{configuration.active ? 'Configurazione in corso - valida dal '+formatDate(configuration.validFrom): 'Configurazione Storica - valida dal: '+configuration.validFrom+' al: '+ configuration.validTo}}</th>
                                        <th class="blockHeader" colspan="6" ng-hide="collapseConf"><span
                                                ng-click="collapseConf=true"
                                                class="glyphicon glyphicon-collapse-down"></span>&nbsp;{{configuration.active ? 'Configurazione in corso - valida dal '+formatDate(configuration.validFrom): 'Configurazione Storica - valida dal '+formatDate(configuration.validFrom)+' al '+ formatDate(configuration.validTo)}}
                                        </th>
                                    </tr>
                            <tr>                                
                        </thead>
                                                        <tbody ng-hide="collapseConf" style="border:4px;">
                                                            <tr >
                                                                <td style="border-top:1px; height:35px;">
                                                                <b style="font-size: 18px">
                                                                    Titolo
                                                                </b>
                                                            </td>
                                                            </tr>
                                                            <tr ng-repeat-start="setting in configuration.settings">
                                                                <tr  ng-show="setting.key =='artist_weight'">
                                                                    <td style="text-align: left; height:35px; width: 200%">
                                                                            <b style="font-size: 18px">Artista</b>
                                                                        </td>
                                                                        <td style="border-left:unset;">

                                                                        </td>
                                                                    </tr>
                                                                <td style="text-align: left; width: 50%;" ng-show="setting.settingType!='threshold_set'">
                                                                    <label>{{setting.label}}</label>
                                                                    
                                                                </td>
                                                                
                                                                <td style="text-align: left; width: 50%;" ng-show="setting.key =='relevance_flag'">
                                                                    <b style="font-size: 18px">Maturato</b>
                                                                </td>
                                                                <td style="text-align: left; width: 50%;" ng-show="setting.key =='risk_flag'">
                                                                    <b style="font-size: 18px">Rischio</b>
                                                                </td>

                                                                <!-- Number Configuration -->
                                                        
                                                                <td style="text-align: center;" style="font-weight: 1000;"
                                                                    ng-show="setting.valueType=='Decimal' && setting.settingType=='key_value'">
                                                                    <input ng-show="configuration.active==true" type="text" class="form-control" style="width:700px; margin-bottom:0%;"
                                                                        ng-model="setting.value">
                                                                    <div ng-show="setting.errorMessages && setting.errorMessages.length > 0" style="text-align:left;">
                                                                                <p style="color:red; font-size: 75%; margin-bottom:0%;">{{setting.errorMessages[0]}}</p>
                                                                    </div>
                                                                    <input ng-show="configuration.active==false" type="text" class="form-control" style="width:700px"
                                                                        ng-model="setting.value" readonly>
                                                                </td>
                                                                <!-- Threshold Configuration -->
                                                        
                                                                <td ng-show="setting.valueType=='Boolean' && setting.settingType=='threshold_set'">
                                                                    <md-switch ng-show="configuration.active==false" class="md-primary" ng-model="setting.value"
                                                                        style="margin:0px" aria-label="setting.value" ng-disabled="true">
                                                                    </md-switch>
                                                                    <md-switch ng-show="configuration.active==true" class="md-primary" ng-model="setting.value"
                                                                        style="margin:0px" aria-label="setting.value" id="setting-input-{{subConf.key}}-{{setting.key}}">
                                                                    </md-switch>
                                                                </td>
                                                            </tr> 
                                                        <tr ng-repeat-end ng-show="setting.valueType=='Boolean' && setting.settingType=='threshold_set' && setting.value==true"
                                                        ng-repeat="threshold in setting.thresholds">
                                                               
                                                                <td>
                                                                       
                                                                            {{threshold.label}}
                                                                            </td>
                                                                            <td>
                                                                            <input type="text" class="form-control" ng-show="configuration.active==true" style="width:700px; margin-bottom:0%;"
                                                                            id="threshold-input-{{subConf.key}}-{{threshold.key}}" ng-model="threshold.value">
                                                                            <div ng-show="threshold.errorMessages && threshold.errorMessages.length > 0" style="text-align:left;">
                                                                                    <p style="color:red; font-size: 75%; margin-bottom:0%;">{{threshold.errorMessages[0]}}</p>
                                                                            </div>
                                                                            <input type="text" class="form-control" ng-show="configuration.active==false" style="width:700px"
                                                                            id="threshold-input-{{subConf.key}}-{{threshold.key}}" ng-model="threshold.value" readonly>
                                                                    
                                                                    </td>                                                         
                                                            </tr>
                                                       
                                                        </tbody>
                        <tfoot ng-hide="collapseConf">
                            <tr>
                                <td class="fieldLabel medium" style="text-align: right; width: 16.66% nowrap"
                                    colspan="6">
                                    <button class="btn addButton" ng-click="saveConfiguration()" ng-show="configuration.active==true">
                                        <span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Applica</strong>
                                    </button>
                                    <button type="button addButton" class="btn" ng-click="returnToCurrentConf()"
                                    ng-show="configuration.active==false">
                                    <span> Ritorna alla configurazione in corso</span>                                            
                                    </button>
                                    <button style="float: left;" type="button addButton" class="btn" ng-click="saveAsNew()"
                                    ng-show="configuration.active==false">
                                    <span> Ripristina configurazione</span>                                            
                                    </button>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </form>
            </div>

           <div class="listViewPageDiv" ng-init="collapseHistory=false">
                <div class="listViewActionsDiv row-fluid">
                    <table class="table table-bordered blockContainer showInlineTable equalSplit noHoverTable" >
                            <thead>
                                    <tr>
                                        <th class="blockHeader" colspan="6" ng-show="collapseHistory"><span
                                                ng-click="collapseHistory=false"
                                                class="glyphicon glyphicon-expand"></span>&nbsp;{{historyData.label}}</th>
                                        <th class="blockHeader" colspan="6" ng-hide="collapseHistory"><span
                                                ng-click="collapseHistory=true"
                                                class="glyphicon glyphicon-collapse-down"></span>&nbsp;{{historyData.label}}
                                        </th>
                                    </tr>
                                </thead>
                                <tbody ng-hide="collapseHistory">
                                    <tr>
                                        <td style="padding: 0px;">
                                            <table class="equalSplit noHoverTable" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th style="text-align: center; width: 25%">
                                                            Autore modifica
                                                        </th>
                                                        <th style="text-align: center; width: 25%">
                                                            Valida dal
                                                        </th>
                                                        <th style="text-align: center; width: 25%">
                                                            Valida al
                                                        </th>
                                                        <th style="text-align: center; width: 25%">
                                                            Visualizza
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr ng-repeat="subConf in historyData.configurations">
                                                        <td style="text-align: center; width: 25%;">
                                                            <label>{{subConf.editedBy}}</label>
                                                        </td>
                                                        <td style="text-align: center; width: 25%;">
                                                            <label>{{subConf.validFrom| date: 'dd/MM/yyyy &nbsp;&nbsp;  HH:mm'}}</label>
                                                        </td>
                                                        <td style="text-align: center; width: 25%;">
                                                            <label>{{subConf.validTo| date: 'dd/MM/yyyy  &nbsp;&nbsp; HH:mm'}}</label>
                                                        </td>
                                                        <td style="text-align: center; width: 25%;">
                                                            <button type="button" class="btn" data-dir="up" ng-click="showHistoryConfiguration(subConf.id)"><span
                                                                    class="glyphicon glyphicon-eye-open"></span></button>
                                                
                                                        </td>
                                                
                                                    </tr>
                                                </tbody>
                                                </table>
            
            
                                            </td>
                                        </tr>
                                    </tbody>
                    </table>
                </div>
            </div>
        </div>




        <!-- fine -->
    </div>
</div>