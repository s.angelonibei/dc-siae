<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../navbar.jsp"%>

<div class="bodyContents" id="panel">
    <div class="mainContainer row-fluid" >

        <div class="contents-topscroll noprint">
            <div class="topscroll-div" style="width: 95%;margin-bottom: 20px">&nbsp;</div>
        </div>

        <div class="sticky">
            <div id="companyLogo" class="navbar commonActionsContainer noprint">
                <div class="actionsContainer row-fluid">
                    <div class="span2">
                        <span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
                    </div>
                    <span class="btn-toolbar span5" style="margin-left:0%;">
                        <div class="pageNumbers alignTop pull-right" style="text-align: center;">
                            <span id="up" class="pageNumbersText"><strong>{{pageTitle}}</strong></span>
                        </div>
                    </span>
                </div>
            </div>
        </div>


        <div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px; margin-top: 25px;">
            <div class="listViewPageDiv" ng-init="collapseConf=false">
                    <div class="alert alert-danger" ng-show="messages.error">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                            <span><strong>Attenzione: </strong>{{messages.error}}</span>
                        </div>
                <form name="configurationForm" autocomplete="off">

                <div class="listViewActionsDiv row-fluid">
                    <table class="table table-bordered blockContainer showInlineTable equalSplit noHoverTable">
                        <thead>
                                <tr>
                                        <th class="blockHeader" colspan="6" ng-show="collapseConf"><span
                                                ng-click="collapseConf=false"
                                                class="glyphicon glyphicon-expand"></span>&nbsp;{{configuration.active ? 'Configurazione in corso - valida dal '+formatDate(configuration.validFrom): 'Configurazione Storica - valida dal: '+configuration.validFrom+' al: '+ configuration.validTo}}</th>
                                        <th class="blockHeader" colspan="6" ng-hide="collapseConf"><span
                                                ng-click="collapseConf=true"
                                                class="glyphicon glyphicon-collapse-down"></span>&nbsp;{{configuration.active ? 'Configurazione in corso - valida dal '+formatDate(configuration.validFrom): 'Configurazione Storica - valida dal '+formatDate(configuration.validFrom)+' al '+ formatDate(configuration.validTo)}}
                                        </th>
                                    </tr>
                            <tr>
                        </thead>
                        <tbody ng-hide="collapseConf">
                                            <tr ng-repeat="setting in configuration.settings">
                                                <td>
                                                    <label>{{setting.label}}</label>
                                                </td>
                                                <!-- Number Configuration -->
                                                <td ng-show="setting.valueType=='Decimal' && setting.settingType=='key_value'" >
                                                    <span class="inner-addon right-addon" ng-show="configuration.active==true">
                                                    <input type="text"
                                                    ng-show="configuration.active==true"
                                                    name="number-input-{{subConf.key}}-{{setting.key}}"
                                                    class="form-control text-center" ng-model="setting.value"
                                                    style="height: 25px;  margin-bottom:0%;">
                                                            <p ng-show="setting.errorMessages && setting.errorMessages.length > 0" 
                                                            style="color:red; font-size: 75%; margin-top:9px; float:right; margin-right:35%;">{{setting.errorMessages[0]}}</p>
                                                    </span>
                                                    <div class="inner-addon right-addon" ng-show="configuration.active==false">
                                                    <input type="text" 
                                                        name="number-input-{{subConf.key}}-{{setting.key}}"
                                                        class="form-control text-center" ng-model="setting.value"
                                                        readonly style="height: 25px; width: 180px">  
                                                    </div>
                                                </td>
                                                <td ng-show="setting.settingType=='threshold'">
                                                    <div class="inner-addon right-addon" ng-show="configuration.active==true">
                                                    <select ng-model="setting.relation" required
                                                        md-no-asterisk="false" style="width:60px; float: left; height: 35px;">
                                                        <option value="GTE" style="font-weight: 100;"> &ge; </option>
                                                        <option value="GT" style="font-size: 150;"> &gt; </option>
                                                        <option value="LTE" style="font-weight: 100;"> &le; </option>
                                                        <option value="LT" style="font-size: 150;"> &lt; </option>
                                                    </select>
                                                    <input type="text" class="form-control"
                                                    ng-model="setting.value" style="width: 150px; height: 25px;  margin-bottom:0%;">
                                                    <i  ng-show="!setting.key.includes('ecoValue')" class="fas fa-percent" style="font-size: 16px;"></i>
                                                    <p ng-show="setting.errorMessages && setting.errorMessages.length > 0" 
                                                    style="color:red; font-size: 75%; margin-top:9px; float:right; margin-right:35%;">{{setting.errorMessages[0]}}</p>
                                                </div>
                                                <div class="inner-addon right-addon" ng-show="configuration.active==false">
                                                    <span class="glyphicon" ng-show="configuration.active==false && setting.relation =='GT'"
                                                        style="font-weight: 1000;">&gt;</span>
                                                        <span class="glyphicon" ng-show="configuration.active==false && setting.relation =='LT'"
                                                        style="font-weight: 1000;">&lt;</span>
                                                        <span class="glyphicon" ng-show="configuration.active==false && setting.relation =='LTE'"
                                                        style="font-weight: 1000;">&le;</span>
                                                        <span class="glyphicon" ng-show="configuration.active==false && setting.relation =='GTE'"
                                                        style="font-weight: 1000;">&ge;</span>  
                                                    <input type="text" class="form-control"
                                                        ng-model="setting.value" readonly style="width: 168px; height: 25px;">
                                                        <i ng-show="!setting.key.includes('ecoValue')" class="fas fa-percent" style="font-size: 16px;"></i>
                                                    </div>
                                                </td>

                                                <!-- Boolean Configuration -->
                                                <td
                                                    ng-show="setting.valueType=='Boolean' && setting.settingType=='key_value'">
                                                    <md-switch ng-show="configuration.active==false" class="md-primary"
                                                        ng-model="setting.value" style="margin:0px"
                                                        aria-label="setting.value" ng-disabled="true">
                                                    </md-switch>
                                                    <md-switch ng-show="configuration.active==true" class="md-primary"
                                                        ng-model="setting.value" style="margin:0px"
                                                        aria-label="setting.value">
                                                    </md-switch>
                                                </td>
                                            </tr>
                                        </tbody>
                        <tfoot ng-hide="collapseConf">
                            <tr>
                                <td class="fieldLabel medium" style="text-align: right; width: 16.66% nowrap"
                                    colspan="6">
                                    <button class="btn addButton" ng-click="saveConfiguration()"
                                        ng-show="configuration.active==true">
                                        <span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Applica</strong>
                                    </button>
                                    <button type="btn addButton" class="btn" ng-click="returnToCurrentConf()"
                                        ng-show="configuration.active==false">
                                        <span> Ritorna alla configurazione in corso</span>
                                    </button>
                                    <button style="float: left;" type="button addButton" class="btn" ng-click="saveAsNew()"
                                    ng-show="configuration.active==false">
                                    <span> Ripristina configurazione</span>                                            
                                    </button>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                </form>
            </div>
            <div class="listViewPageDiv" ng-init="collapseHistory=false">
                <div class="listViewActionsDiv row-fluid">
                    <table class="table table-bordered blockContainer showInlineTable equalSplit noHoverTable">
                        <thead>
                            <tr>
                                <th class="blockHeader" colspan="6" ng-show="collapseHistory"><span
                                        ng-click="collapseHistory=false"
                                        class="glyphicon glyphicon-expand"></span>&nbsp;{{historyData.label}}</th>
                                <th class="blockHeader" colspan="6" ng-hide="collapseHistory"><span
                                        ng-click="collapseHistory=true"
                                        class="glyphicon glyphicon-collapse-down"></span>&nbsp;{{historyData.label}}
                                </th>
                            </tr>
                        </thead>
                        <tbody ng-hide="collapseHistory">
                            <tr>
                                <td style="padding: 0px;">
                                    <table class="equalSplit noHoverTable" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th style="text-align: center; width: 25%;">
                                                    Autore modifica
                                                </th>
                                                <th style="text-align: center; width: 25%;">
                                                        Valida dal
                                                </th>
                                                <th style="text-align: center; width: 25%;">
                                                        Valida al
                                                </th>
                                                <th style="text-align: center; width: 25%;">
                                                    Visualizza
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="subConf in historyData.configurations">
                                                <td style="text-align: center; width: 25%;">
                                                    <label>{{subConf.editedBy}}</label>
                                                </td>
                                                <td style="text-align: center; width: 25%;">
                                                        <label>{{subConf.validFrom| date: 'dd/MM/yyyy  &nbsp;&nbsp; HH:mm'}}</label>
                                                    </td>
                                                    <td style="text-align: center; width: 25%;">
                                                        <label>{{subConf.validTo| date: 'dd/MM/yyyy &nbsp;&nbsp;  HH:mm'}}</label>
                                                    </td>
                                                <td style="text-align: center; width: 25%;">
                                                    <button type="button" class="btn" data-dir="up"
                                                        ng-click="showHistoryConfiguration(subConf.id)"><span
                                                            class="glyphicon glyphicon-eye-open"></span></button>

                                                </td>

                                            </tr>
                                        </tbody>
                                    </table>


                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>



        <!-- fine -->
    </div>
</div>