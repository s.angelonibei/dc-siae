<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../navbar.jsp"%>

<div class="bodyContents" id="panel">
    <div class="mainContainer row-fluid">

        <div class="contents-topscroll noprint">
            <div class="topscroll-div" style="width: 95%;margin-bottom: 20px">&nbsp;</div>
        </div>

        <div class="sticky">
            <div id="companyLogo" class="navbar commonActionsContainer noprint">
                <div class="actionsContainer row-fluid">
                    <div class="span2">
                        <span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
                    </div>
                    <span class="btn-toolbar span5">
                        <div class="pageNumbers alignTop pull-right" style="text-align: center;">
                            <span id="up" class="pageNumbersText"><strong>{{pageTitle}}</strong></span>
                        </div>
                    </span>
                </div>
            </div>
        </div>

        <div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px; margin-top: 25px;">
            <div class="listViewPageDiv" ng-init="collapseConf=false">
                <div class="alert alert-danger" ng-show="messages.error">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                            <span><strong>Attenzione: </strong>{{messages.error}}</span>
                        </div> 
                <form name="configurationForm" autocomplete="off" novalidate>
                    <div class="listViewActionsDiv row-fluid">
                        <table class="table table-bordered blockContainer showInlineTable equalSplit noHoverTable">
                            <thead>
                                <tr>
                                    <th class="blockHeader" colspan="6" ng-show="collapseConf"><span
                                            ng-click="collapseConf=false"
                                            class="glyphicon glyphicon-expand"></span>&nbsp;{{configuration.active ? 'Configurazione in corso - valida dal '+formatDate(configuration.validFrom): 'Configurazione Storica - valida dal: '+configuration.validFrom+' al: '+ configuration.validTo}}</th>
                                    <th class="blockHeader" colspan="6" ng-hide="collapseConf"><span
                                            ng-click="collapseConf=true"
                                            class="glyphicon glyphicon-collapse-down"></span>&nbsp;{{configuration.active ? 'Configurazione in corso - valida dal '+formatDate(configuration.validFrom): 'Configurazione Storica - valida dal '+formatDate(configuration.validFrom)+' al '+ formatDate(configuration.validTo)}}
                                    </th>
                                </tr>
                            </thead>
                            <tbody ng-hide="collapseConf">
                                <tr ng-repeat="subConf in configuration.configurations">
                                    <td style="padding: 0px;">
                                        <table class="noHoverTable" style="width: 100%">
                                            <thead>
                                                <tr>
                                                    <th colspan="2">
                                                        {{subConf.label}}
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat-start="setting in subConf.settings">
                                                    <td style="width:50%"> 
                                                        <label>{{setting.label}}</label>
                                                    </td>
                                                    <!-- On Off Configuration -->
                                                    <td style="width:50%"
                                                        ng-show="setting.valueType=='Boolean' && setting.settingType=='key_value'">
                                                        <md-switch ng-show="configuration.active==false"
                                                            class="md-primary" ng-model="setting.value"
                                                            style="margin:0px" aria-label="setting.value"
                                                            ng-disabled="true">
                                                        </md-switch>
                                                        <md-switch ng-show="configuration.active==true"
                                                            class="md-primary" ng-model="setting.value"
                                                            style="margin:0px" aria-label="setting.value"
                                                            id="setting-input-{{subConf.key}}-{{setting.key}}">
                                                        </md-switch>
                                                    </td>
                                                    <!-- Threshold Configuration -->

                                                    <td style="width:50%"
                                                        ng-show="setting.valueType=='Boolean' && setting.settingType=='threshold_set'">
                                                        <md-switch ng-show="configuration.active==false"
                                                            class="md-primary" ng-model="setting.value"
                                                            style="margin:0px" aria-label="setting.value"
                                                            ng-disabled="true">
                                                        </md-switch>
                                                        <md-switch ng-show="configuration.active==true"
                                                            class="md-primary" ng-model="setting.value"
                                                            style="margin:0px" aria-label="setting.value"
                                                            id="setting-input-{{subConf.key}}-{{setting.key}}">
                                                        </md-switch>
                                                    </td>

                                                    <!-- Number Configuration -->
                                                    <td class="col-md-6" ng-show="setting.valueType=='Integer'" style="width:50%">
                                                        <input type="text" ng-show="configuration.active==true && setting.valueType=='Integer'" style="width: 50px"
                                                            name="number-input-{{subConf.key}}-{{setting.key}}"
                                                            class="form-control text-center" ng-model="setting.value"
                                                            id="setting-input-{{subConf.key}}-{{setting.key}}"
                                                             style=" margin-bottom:0%;">
                                                        </div> 
                                                        <input type="text" ng-show="configuration.active==false"
                                                            name="number-input-{{subConf.key}}-{{setting.key}}"
                                                            class="form-control text-center" ng-model="setting.value"
                                                            readonly>
                                                    </td>
                                                </tr>
                                                <tr ng-repeat-end
                                                    ng-show="setting.valueType=='Boolean' && setting.settingType=='threshold_set' && setting.value==true">
                                                    <td>{{thresholdsLabel}}</td>
                                                    <td>
                                                        <div ng-repeat="threshold in setting.thresholds">
                                                            <span class="glyphicon"
                                                                style="font-weight: 1000;">{{threshold.relation =='LT'? '&lt;':'&ge;'}}</span>
                                                            <input type="text" class="form-control" ng-show="configuration.active==true && $index &lt; (setting.thresholds.length -1)"
                                                                id="threshold-input-{{subConf.key}}-{{threshold.key}}"
                                                                ng-model="threshold.value" ng-change="adjustLastThreshold($index, setting.thresholds)">
                                                            <input type="text" class="form-control" ng-show="configuration.active==true && $index == (setting.thresholds.length -1)"
                                                                id="threshold-input-{{subConf.key}}-{{threshold.key}}"
                                                                ng-model="threshold.value"
                                                                readonly>                                                                
                                                            <input type="text" class="form-control"
                                                                ng-show="configuration.active==false"
                                                                ng-model="threshold.value" readonly>
                                                            <span class="glyphicon" ng-show="subConf.key =='maturato'" ng-bind-html="getMaturatoHtml(threshold.caption)"></span>
                                                            <span class="glyphicon" ng-show="subConf.key =='rischio'" ng-bind-html="getRischioHtml(threshold.caption)"></span>
                                                            <p ng-show="threshold.errorMessages && threshold.errorMessages.length > 0" 
                                                                style="color:red; font-size: 75%; margin-top:9px; float:right; margin-right:35%;">{{threshold.errorMessages[0]}}</p>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr ng-repeat="setting in subConf.settings">
                                                    <td style="width:50%" ng-show="subConf.key=='maturato' && $index == (subConf.settings.length -1)">
                                                        <label>{{periodiMaturato.label}}</label>
                                                    </td>
                                                    <td class="col-md-6" style="width:50%" ng-show="subConf.key=='maturato' && $index == (subConf.settings.length -1)">
                                                        <input type="text" class="form-control text-center" ng-model="periodiMaturato.value" readonly>
                                                    </td>
                                                
                                                    <td style="width:50%" ng-show="subConf.key=='rischio' && $index == (subConf.settings.length -1)">
                                                        <label>{{periodiRischio.label}}</label>
                                                    </td>
                                                    <td class="col-md-6" style="width:50%" ng-show="subConf.key=='rischio' && $index == (subConf.settings.length -1)">
                                                        <input type="text" class="form-control text-center" ng-model="periodiRischio.value" readonly>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>


                                    </td>
                                </tr>
                            </tbody>
                            <tfoot ng-hide="collapseConf">
                                <tr>
                                    <td class="fieldLabel medium" style="text-align: right; width: 16.66% nowrap"
                                        colspan="6">
                                        <button class="btn addButton" ng-click="saveConfiguration()" ng-show="configuration.active==true && !configurationForm.$pristine"
                                            ng-show="configuration.active==true" type="submit">
                                            <span
                                                class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Applica</strong>
                                        </button>
                                        <button type="button addButton" class="btn" ng-click="returnToCurrentConf()"
                                            ng-show="configuration.active==false">
                                            <span> Ritorna alla configurazione in corso</span>
                                        </button>
                                        <button  style="float: left;" type="button addButton" class="btn" ng-click="saveAsNew()"
                                    ng-show="configuration.active==false">
                                    <span> Ripristina configurazione</span>                                            
                                    </button>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                        </td>
                        </tr>
                    </div>
                </form>
            </div>
            <div class="listViewPageDiv" ng-init="collapseHistory=true">
                <div class="listViewActionsDiv row-fluid">
                    <table class="table table-bordered blockContainer showInlineTable equalSplit noHoverTable">
                        <thead>
                            <tr>
                                <th class="blockHeader" colspan="6" ng-show="collapseHistory"><span
                                        ng-click="collapseHistory=false"
                                        class="glyphicon glyphicon-expand"></span>&nbsp;{{historyData.label}}</th>
                                <th class="blockHeader" colspan="6" ng-hide="collapseHistory"><span
                                        ng-click="collapseHistory=true"
                                        class="glyphicon glyphicon-collapse-down"></span>&nbsp;{{historyData.label}}
                                </th>
                            </tr>
                        </thead>
                        <tbody ng-hide="collapseHistory">
                            <tr>
                                <td style="padding: 0px;">
                                    <table class="equalSplit noHoverTable" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th style="text-align: center; width: 25%;">
                                                    Autore modifica
                                                </th>
                                                <th style="text-align: center; width: 25%;">
                                                    Valida dal
                                                </th>
                                                <th style="text-align: center; width: 25%;">
                                                    Valida al
                                                </th>
                                                <th style="text-align: center; width: 25%;">
                                                    Visualizza
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="historicConf in historyData.configurations">
                                                <td style="text-align: center; width: 25%;">
                                                    <label>{{historicConf.editedBy}}</label>
                                                </td>
                                                <td style="text-align: center; width: 25%;">
                                                    <label>{{historicConf.validFrom| date: 'dd/MM/yyyy &nbsp;&nbsp; HH:mm'}}</label>
                                                </td>
                                                <td style="text-align: center; width: 25%;">
                                                    <label>{{historicConf.validTo| date: 'dd/MM/yyyy &nbsp;&nbsp; HH:mm'}}</label>
                                                </td>
                                                <td style="text-align: center; width: 25%;">
                                                    <button type="button" class="btn"
                                                        ng-click="showHistoryConfiguration(historicConf.id)"><span
                                                            class="glyphicon glyphicon-eye-open"></span></button>

                                                </td>

                                            </tr>
                                        </tbody>
                                    </table>


                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <pre>{{configuration | json}}</pre> -->
