<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%@
	page import="java.util.List" %><%

@SuppressWarnings("unchecked")
final List<String> groups = (List<String>) session.getAttribute("sso.user.groups");
if (null == groups) {
	response.sendError(500);
}

if ("DEVELOPMENT".equals(groups.get(0))) {
%> <div data-ng-init="navigateTo('/dspStatistics')"></div> <%	
} else if ("BU-MULTIMEDIALE".equals(groups.get(0))) {
	%> <div data-ng-init="navigateTo('/dspStatistics')"></div> <%	
} else if ("BU-MUSICA".equals(groups.get(0))) {
	%> <div data-ng-init="navigateTo('/evaluate-channel')"></div> <%	
} else if ("IT".equals(groups.get(0))) {
	%> <div data-ng-init="navigateTo('/dspStatistics')"></div> <%	
} else if ("GA".equals(groups.get(0))) {
%> <div data-ng-init="navigateTo('/dspStatistics')"></div> <%	
} else if ("MU-MULTITERRITORIALE".equals(groups.get(0))) {
	%> <div data-ng-init="navigateTo('/dspStatistics')"></div> <%	
} else if ("MU-LOCALE".equals(groups.get(0))) {
	%> <div data-ng-init="navigateTo('/multimedialeLocale')"></div> <%	
} else if ("DEV".equals(groups.get(0))) {
	%> <div data-ng-init="navigateTo('/dspStatistics')"></div> <%	
}  else if ("CODIFICATORESPERTO".equals(groups.get(0))) {
	%> <div data-ng-init="navigateTo('/codificaManualeEsperto')"></div> <%	
} else if ("CODIFICATOREBASE".equals(groups.get(0))) {
	%> <div data-ng-init="navigateTo('/codificaManualeBase')"></div> <%	
}else if("PERF_OPERATORE".equals(groups.get(0))){
	%> <div data-ng-init="navigateTo('/visualizzazioneEventi')"></div> <%
}else if("PERF_COORDINATORE".equals(groups.get(0))){
	%> <div data-ng-init="navigateTo('/visualizzazioneEventi')"></div> <%
}else if("PERF_FUNZIONARIO".equals(groups.get(0))){
	%> <div data-ng-init="navigateTo('/visualizzazioneEventi')"></div> <%
}else if("PERF_DIRETTORE".equals(groups.get(0))){
	%> <div data-ng-init="navigateTo('/visualizzazioneEventi')"></div> <%
} else {
	response.sendError(500);
}%>