<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../navbar.jsp"%>


<div class="bodyContents" id="panel">
	<div class="mainContainer row-fluid">

		<div class="contents-topscroll noprint">
			<div class="topscroll-div" style="width: 95%;margin-bottom: 20px">&nbsp;</div>
		</div>

		<div class="sticky">
			<div id="companyLogo" class="navbar commonActionsContainer noprint">
				<div class="actionsContainer row-fluid">
					<div class="span2">
						<span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
					</div>
					<span class="btn-toolbar span5" style="margin-left:0%;">
						<div class="pageNumbers alignTop pull-right" style="text-align: center;">
							<span id="up" class="pageNumbersText"><strong>{{pageTitle}}</strong></span>
						</div>
					</span>
				</div>
			</div>
		</div>

		<div style="padding:0 20px;margin-top: 25px">
			<button class="btn addButton" ng-click="upsertConfiguration()">
				<span class="glyphicon glyphicon-cloud-upload"></span>&nbsp;&nbsp;<strong>Nuova Configurazione</strong>
			</button>
		</div>

		<div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px; margin-top: 25px">
			<div class="listViewPageDiv">

			<div class="alert alert-warning" ng-show="messages.warning" style="margin-top:20px">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
					<span>{{messages.warning}}</span>
				</div>
				<div class="alert alert-danger" ng-show="messages.error" style="margin-top:20px">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
					<span><strong>Attenzione: </strong>{{messages.error}}</span>
				</div>
				<div class="alert alert-success" ng-show="messages.info" style="margin-top:20px">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
					<span>{{messages.info}}</span>
				</div>

				<div class="listViewActionsDiv row-fluid">
					<table class="table table-bordered blockContainer showInlineTable equalSplit">
						<thead>
							<tr>
								<th class="blockHeader" colspan=6><span
										class="glyphicon glyphicon-expand"></span>&nbsp;Parametri
									Ricerca</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="fieldLabel medium">
									<label class="muted pull-right marginRight10px">
										Voce di incasso
									</label>
								</td>
								<td>
									<input type="text" class="form-control" ng-model="codVoceIncasso">
								</td>
							</tr>
						</tbody>
						<tfoot>
							<tr>
								<td class="fieldLabel medium" style="text-align: right" nowrap colspan="6">
									<button id="applicaTitoloAutore" class="btn addButton"
										ng-click="searchConfiguration(codVoceIncasso)" type="submit">
										<span
											class="glyphicon glyphicon-search"></span><strong>&nbsp;&nbsp;Cerca&nbsp;&nbsp;</strong>
									</button>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>

			<br>
			<!-- table risultati -->
			<div class="listViewContentDiv" id="listViewContents">

				<div class="contents-topscroll noprint">
					<div class="topscroll-div" style="width: 95%"></div>
				</div>
				<div class="listViewEntriesDiv contents-bottomscroll"
					ng-show="configurations && configurations.length > 0">
					<div class="bottomscroll-div" style="width: 95%; min-height: 80px">
						<table class="table table-bordered listViewEntriesTable tableFormatted">
							<thead>
								<tr class="listViewHeaders">
									<th style="text-align: center;">Voce Incasso</th>
									<th style="text-align: center;">Soglia</th>
									<th style="text-align: center;">Autore ultima modifica</th>
									<th style="text-align: center;">Data ultima modifica</th>
									<th style="text-align: center;">
										Azioni
									</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="configuration in configurations | orderBy:'key'">
									<td style="text-align: center;">{{configuration.key}}</td>
									<td style="text-align: center;">
										&plusmn;&nbsp;&nbsp;{{configuration.settings[getValueSettingIndex(configuration)].value}}&nbsp;&nbsp;{{configuration.settings[getTypeSettingIndex(configuration)].value? ' &#37;':' &euro;'}}
									</td>
									<td style="text-align: center;">{{configuration.createdBy}}</td>
									<td style="text-align: center;">
										{{configuration.validFrom | date: 'dd/MM/yyyy &nbsp;&nbsp; HH:mm'}}</td>
									<td style="text-align: center;"><button class="btn addButton"
											ng-click="upsertConfiguration(configuration)" title="Modifica"><span
												id="modifica" class="glyphicon glyphicon-pencil"></span></button></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<!-- fine -->
		</div>
	</div>