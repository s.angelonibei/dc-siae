<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@
	include file="../../../../navbar.jsp"%>

<div class="bodyContents" ng-init="init()">
	<div class="mainContainer row-fluid">
	<div class="contents-topscroll noprint">
		<div class="topscroll-div" style="width: 95%;margin-bottom: 20px">&nbsp;</div>
	</div>
		<div class="sticky">
			<div id="companyLogo" class="navbar commonActionsContainer noprint">
				<div class="actionsContainer row-fluid">
					<div class="span2">
						<span class="companyLogo"><img src="images/logo_siae.jpg"
							title="SIAE" alt="SIAE">&nbsp;</span>
					</div>
					<span class="btn-toolbar span4">
						<div class="pageNumbers alignTop pull-right"
							style="text-align: center;">
							<span class="pageNumbersText"><strong>Visualizzazione Dettaglio Movimento</strong></span>
						</div>
					</span>
				</div>
			</div>
		</div>
		
		<div class="contentsDiv marginLeftZero" id="rightPanel"
			style="min-height: 80px;">
			<div class="listViewPageDiv"></div>
		<div class="contentsDiv marginLeftZero" id="rightPanel"
			style="min-height: 80px;">
			<div class="listViewPageDiv">
				<div class="listViewActionsDiv row-fluid">
				
				<div class="listViewPageDiv">
					<div class="listViewActionsDiv row-fluid">
						<span class="pageNumbersText">
							<button class="btn addButton" ng-click="back()">
								<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Indietro</strong>
							</button>
						</span>
					</div>
				</div>
				
					<table class="table table-bordered blockContainer showInlineTable equalSplit">
						<thead>
							<tr>
								<th class="blockHeader" colspan="12">Informazioni Evento</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="fieldLabel medium">
									<label class="muted pull-right marginRight10px">ID Evento</label>
								</td>
								<td>
									<label><a ng-click="goTo('/eventoDetail/',evento.idEvento,'')"><u>{{evento.idEvento}}</u></a></label>
								</td>
								<td class="fieldLabel medium">
									<label class="muted pull-right marginRight10px">Seprag punto territoriale</label>
								</td>
								<td>
									<label>{{evento.seprag}}</label>
								</td>
							</tr>
							<tr>
								<td class="fieldLabel medium">
									<label class="muted pull-right marginRight10px">Locale</label>
								</td>
								<td>
									<label>{{evento.denominazioneLocale}}</label>
								</td>
								<td class="fieldLabel medium">
									<label class="muted pull-right marginRight10px">Codice BA</label>
								</td>
								<td>
									<label>{{evento.codiceBaLocale}}</label>
								</td>
							</tr>
							<tr>
								<td class="fieldLabel medium">
									<label class="muted pull-right marginRight10px">Codice SPEI</label>
								</td>
								<td>
									<label>{{evento.codiceSpeiLocale}}</label>
								</td>
								<td class="fieldLabel medium">
									<label class="muted pull-right marginRight10px">Inizio evento</label>
								</td>
								<td>
									<label>{{formatDate(evento.dataInizioEvento)}}</label>
								</td>
							</tr>
							<tr>
								<td class="fieldLabel medium">
									<label class="muted pull-right marginRight10px">Fine evento</label>
								</td>
								<td>
									<label>{{formatDate(manifestazione.dataFineEvento)}}</label>
								</td>
								<td class="fieldLabel medium">
									<label class="muted pull-right marginRight10px">Località</label>
								</td>
								<td>
									<label>{{evento.manifestazione.denominazioneLocalita}}</label>
								</td>
							</tr>
							<tr>
								<td class="fieldLabel medium">
									<label class="muted pull-right marginRight10px">Comune (Provincia)</label>
								</td>
								<td>
									<label>{{evento.manifestazione.comuneLocale}}</label><label ng-show="evento.manifestazione.siglaLocale"> ({{evento.manifestazione.siglaLocale}})</label>
								</td>
								<td class="fieldLabel medium">
								</td>
								<td>
								</td>
							</tr>
							<tr>
								<td class="fieldLabel medium">
									<label class="muted pull-right marginRight10px">Organizzatore</label>
								</td>
								<td>
									<label>{{manifestazione.nomeOrganizzatore}}</label>
								</td>
								<td class="fieldLabel medium">
									<label class="muted pull-right marginRight10px">P.IVA<br/>CF</label>
								</td>
								<td>
									<label>{{manifestazione.partitaIvaOrganizzatore}}<br/>{{manifestazione.codiceFiscaleOrganizzatore}}</label>
								</td>
							</tr>
							<tr>
								<td class="fieldLabel medium">
									<label class="muted pull-right marginRight10px">Codice SAP</label>
								</td>
								<td>
									<label>{{manifestazione.codiceSapOrganizzatore}}</label>
								</td>
								<td class="fieldLabel medium">
								</td>
								<td>
								</td>
							</tr>
						</tbody>
					</table>
					
					<div class="listViewContentDiv" id="listViewContents">

						<div class="contents-topscroll noprint">
							<div class="topscroll-div" style="width: 95%">&nbsp;</div>
						</div>

						<div class="listViewEntriesDiv contents-bottomscroll">
							<div class="bottomscroll-div" style="width: 95%; min-height: 80px">

								<table class="table table-bordered listViewEntriesTable">
									<thead>
										<tr class="listViewHeaders">
											<th class="blockHeader" colspan="12">Informazioni Contabili
												<!-- <button class="btn addButton muted pull-right marginRight10px" ng-click="addContabili(user);"><span class="glyphicon glyphicon-plus"></span></button> -->
											</th>
										</tr>
										<tr>
											<th style="color:#888888">Periodo pagamento</th>
											<th style="color:#888888">Voce incasso</th>
											<th style="color:#888888">Numero fattura</th>
											<th style="color:#888888">Importo DEM</th>
											<th style="color:#888888">Tipologia documento</th>
											<!-- <th style="color:#888888"></th> -->
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="sampling in samplings" class="listViewEntries">
											<td>{{sampling.contabilita}}</td>
											<td>{{sampling.voceIncasso}}</td>
											<td><a ng-click="goTo('/fatturaDetail/',sampling.numeroFattura,'')"><u>{{sampling.numeroFattura}}</u></a></td>
											<td id="importoDEM">{{importoDEM(sampling.importDem,sampling.tipoDocumentoContabile) | currency : "€" : 2}}</td>
											<td>{{sampling.tipoDocumentoContabile}}</td>
											<!-- <td><button class="btn addButton muted pull-right marginRight10px" ng-click="removeContabili()"><span class="glyphicon glyphicon-minus"></span></button></td> -->
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="listViewContentDiv" id="listViewContents">

						<div class="contents-topscroll noprint">
							<div class="topscroll-div" style="width: 95%">&nbsp;</div>
						</div>

						<div class="listViewEntriesDiv contents-bottomscroll">
							<div class="bottomscroll-div" style="width: 95%; min-height: 80px">

								<table class="table table-bordered listViewEntriesTable">
									<thead>
										<tr class="listViewHeaders">
											<th class="blockHeader" colspan="12">Programmi Musicali attesi/rientrati</th>
										</tr>
										<tr>
											<th style="color:#888888">Voce incasso</th>
											<th style="color:#888888">Numero fattura</th>
											<th style="color:#888888">PM Attesi</th>
											<th style="color:#888888">PM Rientrati</th>
											<th style="color:#888888">Importo DEM</th>
											<th style="color:#888888">Importo assegnato</th>
											<th style="color:#888888"></th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="dettaglioPM in dettagliPM track by $index" class="listViewEntries">
											<td>{{dettaglioPM.voceIncasso}}</td>
											<td>{{dettaglioPM.numeroFattura}}</td>
											<td>{{PMAttesi(dettaglioPM.voceIncasso,dettaglioPM.pmPrevisti,dettaglioPM.pmPrevistiSpalla,$index)}}
												<input type="number" style="width: 50px;" id="pmPrevisti{{$index}}" ng-model="pmPrevisti[$index]" ng-show="!pmShow[$index]" readonly>
												<input type="number" style="width: 50px;" id="pmPrevistiPrincipale{{$index}}" ng-model="pmPrevistiPrincipale[$index]" ng-show="pmShow[$index]" readonly>
												<input type="number" style="width: 50px;" id="pmPrevistiSpalla{{$index}}" ng-model="pmPrevistiSpalla[$index]" ng-show="pmShow[$index]" readonly>
											</td>
											<td>{{PMRientrati(dettaglioPM.voceIncasso,dettaglioPM.pmRientrati,dettaglioPM.pmRientratiSpalla)}}</td>
											<td>{{dettaglioPM.importoAssegnato | currency : "€" : 2}}</td>
											<td>{{dettaglioPM.importoSospeso | currency : "€" : 2}}</td>
											<td>
												<button ng-disabled="!evento.id" id="pmPrevistiRientrati{{$index}}" class="btn addButton muted pull-right marginRight10px" ng-click="visibleAndSave(pmPrevisti,pmPrevistiPrincipale,pmPrevistiSpalla,$index)"><span id="spanPM{{$index}}" class="glyphicon glyphicon-pencil"></span></button>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="listViewContentDiv" id="listViewContents">

						<div class="contents-topscroll noprint">
							<div class="topscroll-div" style="width: 95%">&nbsp;</div>
						</div>

						<div class="listViewEntriesDiv contents-bottomscroll">
							<div class="bottomscroll-div" style="width: 95%; min-height: 80px">

								<table class="table table-bordered listViewEntriesTable">
									<thead>
										<tr class="listViewHeaders">
											<th class="blockHeader" colspan="12">Dettaglio movimentazione Programma Musicale
												<!-- <button class="btn addButton muted pull-right marginRight10px" ng-click="addPM(evento.idEvento,user);"><span class="glyphicon glyphicon-plus"></span></button> -->
											</th>
										</tr>
										<tr>
											<th style="color:#888888">Periodo Rientro</th>
											<th style="color:#888888">Voce incasso</th>
											<th style="color:#888888">Numero fattura</th>
											<th style="color:#888888">Numero P.M.</th>
											<th style="color:#888888">Permesso</th>
											<th style="color:#888888">Importo SUN</th>
											<th style="color:#888888">Importo Manuale</th>
											<!-- <th style="color:#888888"></th> -->
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="movimentazionePM in movimentazioniPM" class="listViewEntries">
											<td>{{movimentazionePM.contabilita}}</td>
											<td>{{movimentazionePM.voceIncasso}}</td>
											<td><a ng-click="goTo('/fatturaDetail/', movimentazionePM.numeroFattura, '')"><u>{{movimentazionePM.numeroFattura}}</u></a></td>
											<td><a ng-click="goTo('/pmDetail/', movimentazionePM.idProgrammaMusicale, '/' + movimentazionePM.id)">
												<u>{{numeroPM(movimentazionePM.voceIncasso,movimentazionePM.flagGruppoPrincipale,movimentazionePM.numProgrammaMusicale)}}</u></a>
											</td>
											<td>{{movimentazionePM.numeroPermesso}}</td>
											<td>{{importoDEM(movimentazionePM.importoTotDemLordo,movimentazionePM.tipoDocumentoContabile) | currency : "€" : 2}}</td>
											<td>{{importoDEM(movimentazionePM.importoManuale,movimentazionePM.tipoDocumentoContabile) | currency : "€" : 2}}</td>
											<!-- <td><button class="btn addButton muted pull-right marginRight10px" ng-click="removePM(movimentazionePM.idMovimentoContabile)"><span class="glyphicon glyphicon-minus"></span></button></td> -->
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
</div>