<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@
	include file="../../../navbar.jsp"%>
	
<div class="bodyContents">
	<div class="mainContainer row-fluid">

	<div class="contents-topscroll noprint">
		<div class="topscroll-div" style="width: 95%;margin-bottom: 20px">&nbsp;</div>
	</div>

	<div class="sticky">	
		<div id="companyLogo" class="navbar commonActionsContainer noprint">
			<div class="actionsContainer row-fluid">
				<div class="span2">
					<span class="companyLogo"><img src="images/logo_siae.jpg"
						title="SIAE" alt="SIAE">&nbsp;</span>
				</div>
				<span class="btn-toolbar span4">
					<div class="pageNumbers alignTop pull-right"
						style="text-align: center;">
						<span id="up" class="pageNumbersText"><strong>Ricerca Eventi Pagati</strong></span>
					</div>
				</span>
			</div>
		</div>
	</div>



		<div class="contentsDiv marginLeftZero" id="rightPanel"
			style="min-height: 80px;">
			<div class="listViewPageDiv">
		<!-- table inserimento campi-->
				<div class="listViewActionsDiv row-fluid">
					<table
						class="table table-bordered blockContainer showInlineTable equalSplit">
						<thead>
							<tr>
								<th class="blockHeader" colspan="6" ng-show="ctrl.hideForm"><span
									ng-click="ctrl.hideForm=false"
									class="glyphicon glyphicon-expand"></span>&nbsp;Parametri
									Ricerca</th>
								<th class="blockHeader" colspan="6" ng-hide="ctrl.hideForm"><span
									ng-click="ctrl.hideForm=true"
									class="glyphicon glyphicon-collapse-down"></span>&nbsp;Parametri
									Ricerca</th>
							</tr>
						</thead>
						<tbody ng-hide="ctrl.hideForm">
							<tr>
								<td class="fieldLabel medium"><label
									class="muted pull-right marginRight10px">
										Contabilit&agrave da </label></td>
								<td>
									<div class="controls">
										<div class="input-append date" id="datepickerEventContabilityStartDate"
											data-date-format="mm-yyyy">

											<input placeholder="mm-aaaa" type="text" name="date"
												ng-model="ctrl.selectedContabilityDateFrom"
												style="width: 125px; height: 23px"> <span
												class="add-on" style="height: 23px"><i
												class="icon-th"></i></span>
										</div>
										<script type="text/javascript">
											$("#datepickerEventContabilityStartDate")
												.datepicker(
													{
														format : 'mm-yyyy',
														language : 'it',
														viewMode : "months",
														minViewMode : "months",
														orientation : "bottom auto",
														autoclose : "true"
													});
										</script>
									</div>
								</td>

								<td class="fieldLabel medium"><label
									class="muted pull-right "> <span class="redColor"
										ng-show="form.process.$invalid"></span>Contabilit&agrave a
								</label></td>
								<td>
									<div class="controls">
										<div class="input-append date" id="datepickerEventContabilityEndDate"
											data-date-format="mm-yyyy">

											<input placeholder="mm-aaaa" type="text" name="date"
												ng-model="ctrl.selectedContabilityDateTo"
												style="width: 125px; height: 23px"> <span
												class="add-on" style="height: 23px"><i
												class="icon-th"></i></span>
										</div>
										<script type="text/javascript">
											$("#datepickerEventContabilityEndDate")
												.datepicker(
													{
														format : 'mm-yyyy',
														language : 'it',
														viewMode : "months",
														minViewMode : "months",
														orientation : "bottom auto",
														autoclose : "true"
													});
										</script>
									</div>
								</td>
								<td>
									<select id="tipoDocumento" class="js-basic-single-select" ng-model="tipoDocumento" name="tipoDocumento" style="width: 150px; display: block;">
                                		<option value="">Tipo Documento</option>
                                		<option value="501">Entrate (501)</option>
                                		<option value="221">Uscite (221)</option>
                                	</select>
								</td>
							</tr>
							<tr>
								<td>
									<input id="programmaMusicale" placeholder="Numero Programma Musicale" ng-model="numeroPM">
								</td>
								<td>
									<input id="idEvento" placeholder="Id Evento" ng-model="idEvento">
								</td>
								<td>
									<input id="fattura" placeholder="Fattura" ng-model="fattura">
								</td>
								<td>
									<input id="voceIncasso" placeholder="Voce Incasso" ng-model="voceIncasso">
								</td>
								<td>
									<input id="reversale" placeholder="Reversale" ng-model="reversale">
								</td>
							</tr>
							<tr>
								<td>
								<div class="controls">
									<div class="input-append date" id="datepickerEventStartEventDate"
										data-date-format="dd-mm-yyyy">

										<input placeholder="Data Inizio Evento Da" type="text" name="date"
											ng-model="dataInizioEvento"
											style="width: 135px; height: 25px"> <span
											class="add-on" style="height: 25px"><i class="icon-th"></i></span>
									</div>
									<script type="text/javascript">
										$("#datepickerEventStartEventDate")
											.datepicker(
												{
													format : 'dd-mm-yyyy',
													language : 'it',
													viewMode : "days",
													minViewMode : "days",
													orientation : "bottom auto",
													autoclose : "true"
												});
									</script>
								</div>
								</td>
								<td>
									<div class="controls">
									<div class="input-append date" id="datepickerEventEndEventDate"
										data-date-format="dd-mm-yyyy">

										<input placeholder="Data Inizio Evento A" type="text" name="date"
											ng-model="dataFineEvento"
											style="width: 135px; height: 25px"> <span
											class="add-on" style="height: 25px"><i class="icon-th"></i></span>
									</div>
									<script type="text/javascript">
										$("#datepickerEventEndEventDate")
											.datepicker(
												{
													format : 'dd-mm-yyyy',
													language : 'it',
													viewMode : "days",
													minViewMode : "days",
													orientation : "bottom auto",
													autoclose : "true"
												});
									</script>
								</div>
								</td>
								<td>
									<input id="locale" placeholder="Locale" ng-model="locale">
								</td>
								<td>
									<input id="codiceBA" placeholder="Codice BA" ng-model="codiceBA">
								</td>
								<td>
									<input id="seprag" placeholder="Seprag" ng-model="seprag">
								</td>
							</tr>
							<tr>
								<td>
									<input id="codiceSAP" placeholder="codiceSAP" ng-model="codiceSAP">
								</td>
								<td>
									<input id="ivaCF" placeholder="P.IVA o CF Organizzatore" ng-model="ivaCF">
								</td>
								<td>
									<input id="DE" style="width: 260px;" placeholder="Direttore Esecuzione(CF o Nome-Cognome)" ng-model="DE">
								</td>
								<td>
									<input id="permesso" placeholder="Permesso" ng-model="permesso">
								</td>
								<td>
									<input id="titoloManifestazione" placeholder="Titolo Manifestazione" ng-model="titoloOpera">
								</td>
							</tr>
							<tr>
								<td>
									<select style="width:150px" id="FatturaValidata" class="js-basic-single-select" ng-model="fatturaValidata">
										<option value="">Validazione Fatture:</option>
										<option value="notValidated">Fatture non validate</option>
										<option value="validated">Fatture validate</option>
										<option value="ok">Fattura validata OK</option>
										<option value="ko">Fattura validata KO</option>
									</select>
								</td>
								<td>
								</td>
								<td>
								</td>
								<td>
								</td>
								<td>
								</td>
							</tr>
							<tr>
								<td class="fieldLabel medium" style="text-align: right" nowrap colspan="6">
									<button	class="btn addButton" ng-click="filterApply()">
										<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Applica</strong>
									</button>
								</td>

							</tr>
						</tbody>
					</table>
					<!-- table inserimento campi -->
					</div>
					
					<div>
					<!-- actions and pagination -->
						<div class="listViewTopMenuDiv noprint">
							<div class="listViewActionsDiv row-fluid">
								<!--  -->
								<span class="btn-toolbar span4"> <span class="btn-group">
	
								</span>
								</span>
								<!--  -->
								<!--  -->
								<span class="btn-toolbar span4"> <span
									class="customFilterMainSpan btn-group"
									style="text-align: center;">
										<div class="pageNumbers alignTop ">
											<span class="pageNumbersText"><strong> <!-- Titolo -->
											</strong></span>
										</div>
								</span>
								</span>
								<!--  -->
								<!--  -->
								<span class="span4 btn-toolbar">
									<div class="listViewActions pull-right">
	
										<div class="pageNumbers alignTop ">
											<span ng-show="ctrl.totRecords > 50">
												<span class="pageNumbersText" style="padding-right: 5px">
													Record da <strong>{{ctrl.page*50+1}}</strong> a <strong>{{(ctrl.page*50) + ctrl.sampling.length}}</strong> su <strong>{{totRecords}}</strong>
											</span>
												<button title="Precedente" class="btn"
													id="listViewPreviousPageButton" type="button"
													ng-click="navigateToPreviousPage()"
													ng-show="ctrl.page > 0">
													<span class="glyphicon glyphicon-chevron-left"></span>
												</button>
												<button title="Successiva" class="btn"
													id="listViewNextPageButton" type="button"
													ng-click="navigateToNextPage()" ng-show="ctrl.page < ctrl.pageTot">
													<span class="glyphicon glyphicon-chevron-right"></span>
												</button>
												<button title="Fine" class="btn"
													id="listViewNextPageButton" type="button"
													ng-click="navigateToEndPage()" ng-show="ctrl.page < ctrl.pageTot">
													<span class="glyphicon glyphicon-step-forward"></span>
												</button>
											</span>
										</div>
									</div>
									<div class="clearfix"></div>
								</span>
								<!--  -->
							</div>
						</div>
					<!-- table risultati -->
					<div class="listViewContentDiv" id="listViewContents">

						<div class="contents-topscroll noprint">
							<div class="topscroll-div" style="width: 95%">&nbsp;</div>
						</div>

						<div class="listViewEntriesDiv contents-bottomscroll">
							<div class="bottomscroll-div" style="width: 95%; min-height: 80px">

								<table class="table table-bordered listViewEntriesTable tableFormatted">
									<thead>
										<tr class="listViewHeaders"> <!-- PRIMA DI CAMBIARE LA CLASSE DELLO SPAN, CONTROLLARE LA LOGICA SUL CONTROLLER DI ANGULAR. -->
											<th>Evento&nbsp;<span ng-click="orderImage('spanEvento','idEvento')" id="spanEvento" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
											<th>Permesso&nbsp;<span ng-click="orderImage('spanPermesso','permesso')" id="spanPermesso" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
											<th>Contabilità&nbsp;<span ng-click="orderImage('spanContabilita','contabilita')" id="spanContabilita" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
											<th>Fattura&nbsp;<span ng-click="orderImage('spanFattura','numeroFattura')" id="spanFattura" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
											<th>Voce<br>Incasso&nbsp;<span ng-click="orderImage('spanVoceIncasso','voceIncasso')" id="spanVoceIncasso" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
											<th>Data<br>Inizio&nbsp;<span ng-click="orderImage('spanDataInizioEvento','dataInizioEvento')" id="spanDataInizioEvento" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
											<th>Ora<br>Inizio&nbsp;<span ng-click="orderImage('spanOraInizioEvento','oraInizioEvento')" id="spanOraInizioEvento" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
											<th>Locale<br>Spazio&nbsp;<span ng-click="orderImage('spanDenominazioneLocale','locale')" id="spanDenominazioneLocale" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
											<th>Organizzatore&nbsp;<span ng-click="orderImage('spanNomeOrganizzatore','organizzatore')" id="spanNomeOrganizzatore" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
											<th>Documento&nbsp;<span ng-click="orderImage('spanDocumento','tipoDocumento')" id="spanDocumento" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
											<th>Importo&nbsp;<span ng-click="orderImage('spanImportDem','importoDem')" id="spanImportDem" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
										</tr>
									</thead>
									<tbody>
										<tr class="listViewEntries" ng-repeat="item in ctrl.sampling">
											<td><a ng-click="goTo('/eventoDetail/', item.idEvento)"><u>{{item.idEvento}}</u></a></td>
											<td>{{item.permesso}}</td>
											<td>{{item.contabilita}}</td>
											<td><a ng-click="goTo('/fatturaDetail/',item.numeroFattura)"><u>{{item.numeroFattura}}</u></a></td>
											<td>{{item.voceIncasso}}</td>
											<td>{{formatDate(item.dataInizioEvento)}}</td>
											<td>{{formatHour(item.oraInizioEvento)}}</td>
											<td>{{item.denominazioneLocale}}</td>
											<td>{{item.manifestazione.nomeOrganizzatore}}</td>
											<td>{{item.tipoDocumentoContabile}}</td>
											<td>{{item.importDem | currency : "€" : 2}}</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- table risultati -->
				</div>
			</div>
			<button class="btn addButton pull-right" ng-click="exportExcel()" style="margin: 0 2% 2% 0">Esportazione csv &nbsp;&nbsp;<span class="glyphicon glyphicon-download"></span></button>
		</div>
	</div>
</div>
