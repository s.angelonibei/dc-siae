<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%><%@
	include file="../../../navbar.jsp"%>
	<input type="hidden" id="user" value="<%=(String) session.getAttribute("sso.user.userName")%>">
	<div class="bodyContents" >
	<div class="mainContainer row-fluid">
	
	<div class="contents-topscroll noprint">
		<div class="topscroll-div" style="width: 95%;margin-bottom: 20px">&nbsp;</div>
	</div>

	<div class="sticky">
		<div id="companyLogo" class="navbar commonActionsContainer noprint">
			<div class="actionsContainer row-fluid">
				<div class="span2">
					<span class="companyLogo"><img src="images/logo_siae.jpg"
						title="SIAE" alt="SIAE">&nbsp;</span>
				</div>
				<span class="btn-toolbar span4">
					<div class="pageNumbers alignTop pull-right"
						style="text-align: center;">
						<span class="pageNumbersText"><strong>Gestione Megaconcerti</strong></span>
					</div>
				</span>
			</div>
		</div>
	</div>

		
		<div class="contentsDiv marginLeftZero" id="rightPanel"
			style="min-height: 80px;">
			<button ng-click="openModel(false)" style="margin:0 20px 20px 0;" class="btn addButton pull-right"><span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;<strong>Nuovo</strong></button>
			<div class="listViewPageDiv">
				<div class="listViewActionsDiv row-fluid">
				<table class="table table-bordered blockContainer showInlineTable equalSplit">
					<thead>
						<tr>
							<th class="blockHeader" colspan="6" ng-show="ctrl.hideForm"><span
								ng-click="ctrl.hideForm=false"
								class="glyphicon glyphicon-expand"></span>&nbsp;Parametri
								Ricerca</th>
							<th class="blockHeader" colspan="6" ng-hide="ctrl.hideForm"><span
								ng-click="ctrl.hideForm=true"
								class="glyphicon glyphicon-collapse-down"></span>&nbsp;Parametri
								Ricerca</th>
						</tr>
					</thead>
					<tbody ng-hide="ctrl.hideForm">
						<tr>
							<td class="fieldLabel medium"><label
								class="muted pull-right marginRight10px">
									Valido da </label>
							</td>
							<td>
								<div class="controls">
									<div class="input-append date" id="datepickerMCFatturaStartDate"
										data-date-format="mm-yyyy">

										<input placeholder="gg-mm-aaaa" type="text" name="date"
											ng-model="params.inizioPeriodoValidazione"
											style="width: 125px; height: 23px"> <span
											class="add-on" style="height: 23px"><i
											class="icon-th"></i></span>
									</div>
									<script type="text/javascript">
										$("#datepickerMCFatturaStartDate")
											.datepicker(
												{
													format : 'dd-mm-yyyy',
													language : 'it',
													viewMode : "days",
													minViewMode : "days",
													orientation : "bottom auto",
													autoclose : "true"
												});
									</script>
								</div>
							</td>

							<td class="fieldLabel medium"><label
								class="muted pull-right "> <span class="redColor"
									ng-show="form.process.$invalid"></span>Valido a</label>
							</td>
							<td>
								<div class="controls">
									<div class="input-append date" id="datepickerMCFatturaEndDate"
										data-date-format="mm-yyyy">

										<input placeholder="gg-mm-aaaa" type="text" name="date"
											ng-model="params.finePeriodoValidazione"
											style="width: 125px; height: 23px"> <span
											class="add-on" style="height: 23px"><i
											class="icon-th"></i></span>
									</div>
									<script type="text/javascript">
										$("#datepickerMCFatturaEndDate")
											.datepicker(
												{
													format : 'dd-mm-yyyy',
													language : 'it',
													viewMode : "days",
													minViewMode : "days",
													orientation : "bottom auto",
													autoclose : "true"
												});
									</script>
								</div>
							</td>
							<td>
								<input type="text" placeholder="Codice Regola" id="codiceRegola" ng-model="params.codiceRegola">
							</td>
						</tr>
						<tr>
							<td class="fieldLabel medium" style="text-align: right" nowrap colspan="6">
								<button	id="applica" class="btn addButton" ng-click="filterApply(params)">
									<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Applica</strong>
								</button>
							</td>
						</tr>
					</tbody>
				</table>
				
				<div>
					<div class="listViewTopMenuDiv noprint">
						<div class="listViewActionsDiv row-fluid">
							<!--  -->
							<span class="btn-toolbar span4"> <span class="btn-group">

							</span>
							</span>
							<!--  -->
							<!--  -->
							<span class="btn-toolbar span4"> <span
								class="customFilterMainSpan btn-group"
								style="text-align: center;">
									<div class="pageNumbers alignTop ">
										<span class="pageNumbersText"><strong> <!-- Titolo -->
										</strong></span>
									</div>
							</span>
							</span>
							<!--  -->
							<!--  -->
							<span class="span4 btn-toolbar">
								<div class="listViewActions pull-right">
									<div class="pageNumbers alignTop ">
										<span ng-show="sampling.totRecords > 50">
											<span class="pageNumbersText" style="padding-right: 5px">
												Record da <strong>{{saveQuery.page*50+1}}</strong> a <strong>{{(saveQuery.page*50) + sampling.records.length}}</strong> su <strong>{{sampling.totRecords}}</strong>
										</span>
											<button title="Precedente" class="btn"
												id="listViewPreviousPageButton" type="button"
												ng-click="navigateToPreviousPage()"
												ng-show="saveQuery.page > 0">
												<span class="glyphicon glyphicon-chevron-left"></span>
											</button>
											<button title="Successiva" class="btn"
												id="listViewNextPageButton" type="button"
												ng-click="navigateToNextPage()" ng-show="saveQuery.page < sampling.pages.length - 1">
												<span class="glyphicon glyphicon-chevron-right"></span>
											</button>
											<button title="Fine" class="btn"
												id="listViewNextPageButton" type="button"
												ng-click="navigateToEndPage()" ng-show="saveQuery.page < sampling.pages.length - 1">
												<span class="glyphicon glyphicon-step-forward"></span>
											</button>
										</span>
									</div>
								</div>
								<div class="clearfix"></div>
							</span>
							<!--  -->
						</div>
					</div>

					<div class="listViewContentDiv" id="listViewContents">
						<div class="contents-topscroll noprint">
							<div class="topscroll-div" style="width: 95%">&nbsp;</div>
						</div>

					<div class="listViewEntriesDiv contents-bottomscroll">
						<div class="bottomscroll-div" style="width: 95%; min-height: 80px">

						<table class="table table-bordered listViewEntriesTable tableFormatted">
								<thead>
									<tr class="listViewHeaders">
										<th>Valido da&nbsp;<span ng-click="orderImage('spanValidoDa','inizioPeriodoValidazione')" id="spanValidoDa" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
										<th>Valido a&nbsp;<span ng-click="orderImage('spanValidoA','finePeriodoValidazione')" id="spanValidoA" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
										<th>Soglia&nbsp;<span ng-click="orderImage('spanSoglia','soglia')" id="spanSoglia" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
										<th>% Aggio sottosoglia&nbsp;<span ng-click="orderImage('spanSottoSoglia','aggioSottosoglia')" id="spanSottoSoglia" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
										<th>% Aggio soprasoglia&nbsp;<span ng-click="orderImage('spanSopraSoglia','aggioSoprasoglia')" id="spanSopraSoglia" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
										<th>CAP&nbsp;<span ng-click="orderImage('spanCAP','valoreCap')" id="spanCAP" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
										<th>Tipo CAP&nbsp;<span ng-click="orderImage('spanTipoCAP','flagCapMultiplo')" id="spanTipoCAP" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<tr class="listViewEntries" ng-repeat="item in sampling.records">
										<td ng-bind="(item.inizioPeriodoValidazione  | date: 'dd/MM/yyyy')"></td>
										<td ng-bind="item.finePeriodoValidazione ? (item.finePeriodoValidazione | date: 'dd/MM/yyyy') : 'in corso'"></td>
										<td ng-bind="(item.soglia | currency : '&euro;' : 2)"></td>
										<td ng-bind="(item.aggioSottosoglia | currency : '%' : 2)"></td>
										<td ng-bind="(item.aggioSoprasoglia | currency : '%' : 2)"></td>
										<td ng-bind="(item.valoreCap | currency : '&euro;' : 2)"></td>
										<td ng-bind="(item.flagCapMultiplo == true ? 'multiplo' : 'singolo')"></td>
										<td><button class="btn addButton muted pull-right marginRight10px" ng-click="openModel(true,item)"><span id="spanMC{{$index}}" class="glyphicon glyphicon-pencil"></span></button></td>
									</tr>
								</tbody>
							</table>
						</div>
  					</div>
  				</div>
  			</div>
		</div>
  		</div>
  	</div>
</div>
</div>