<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@
	include file="../../../../navbar.jsp"%>
	<%@
	page import="java.util.List" %><%
	final List<String> groups = (List<String>) session.getAttribute("sso.user.groups");
	%>
	
	<div class="bodyContents" ng-init="init()">
	<div class="mainContainer row-fluid">

	<div class="contents-topscroll noprint">
		<div class="topscroll-div" style="width: 95%;margin-bottom: 20px">&nbsp;</div>
	</div>
	<div class="sticky">
		<div id="companyLogo" class="navbar commonActionsContainer noprint">
			<div class="actionsContainer row-fluid">
				<div class="span2">
					<span class="companyLogo"><img src="images/logo_siae.jpg"
						title="SIAE" alt="SIAE">&nbsp;</span>
				</div>
				<span class="btn-toolbar span4">
					<div class="pageNumbers alignTop pull-right"
						style="text-align: center;">
						<span class="pageNumbersText"><strong>Dettagli Fattura</strong></span>
					</div>
				</span>
			</div>
		</div>
	</div>
		
		<div class="contentsDiv marginLeftZero" id="rightPanel"
			style="min-height: 80px;">
			<div class="listViewPageDiv">
				<div class="listViewActionsDiv row-fluid">
					<div class="listViewContentDiv" id="listViewContents">
						<div class="contents-topscroll noprint">
							<div class="topscroll-div" style="width: 95%">&nbsp;</div>
						</div>

						<div class="listViewEntriesDiv contents-bottomscroll">
							<div class="bottomscroll-div" style="width: 95%; min-height: 80px">
							
								<div class="listViewPageDiv">
									<div class="listViewActionsDiv row-fluid">
										<span class="pageNumbersText">
											<button class="btn addButton" ng-click="back()">
												<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Indietro</strong>
											</button>
										</span>
									</div>
								</div>
								
								<table class="table table-bordered blockContainer showInlineTable equalSplit">
									<thead>
										<tr>
											<th class="blockHeader" colspan="12">Riepilogo Fattura
												<button class="btn addButton pull-right" ng-click="exportFattura(sampling.numeroFattura)">Esportazione Fattura &nbsp;&nbsp;<span class="glyphicon glyphicon-download"></span></button>
											</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="fieldLabel medium">
												<label class="muted pull-right marginRight10px">Numero Fattura</label>
											</td>
											<td>
												<label>{{sampling.numeroFattura}}</label>
											</td>
											<td class="fieldLabel medium">
												<label class="muted pull-right marginRight10px">Data fattura</label>
											</td>
											<td>
												<label>{{sampling.dataFattura}}</label>
											</td>
										</tr>
										<tr>
											<td class="fieldLabel medium">
												<label class="muted pull-right marginRight10px">Numero Reversale</label>
											</td>
											<td>
												<label>{{sampling.reversale}}</label>
											</td>
											<td class="fieldLabel medium">
												<label class="muted pull-right marginRight10px">Seprag</label>
											</td>
											<td>
												<label>{{sampling.seprag}}</label>
											</td>
										</tr>
										<tr>
											<td class="fieldLabel medium">
												<label class="muted pull-right marginRight10px">Nominativo organizzatore</label>
											</td>
											<td>
												<label>{{sampling.nomeOrganizzatore}}</label>
											</td>
											<td class="fieldLabel medium">
												<label class="muted pull-right marginRight10px">P.IVA<br>C.F.</label>
											</td>
											<td>
												<label>{{sampling.pIvaOrganizzatore}}<br>{{sampling.cfOrganizzatore}}</label>
											</td>
										</tr>
										<tr>
											<td class="fieldLabel medium">
												<label class="muted pull-right marginRight10px">Codice SAP</label>
											</td>
											<td>
												<label>{{sampling.codiceSap}}</label>
											</td>
											<td class="fieldLabel medium">
												<label class="muted pull-right marginRight10px">Tipo documento</label>
											</td>
											<td>
												<label>{{sampling.tipoDocumento}}</label>
											</td>
										</tr>
										<tr>
											<td class="fieldLabel medium">
												<label class="muted pull-right marginRight10px">Contabilità</label>
											</td>
											<td>
												<label>{{sampling.eventiAssociati[0].contabilita}}</label>
											</td>
											<td class="fieldLabel medium">
												<label class="muted pull-right marginRight10px"></label>
											</td>
											<td>
												<label></label>
											</td>
										</tr>
									</tbody>
								</table> 
								
								<div class="contents-topscroll noprint">
									<div class="topscroll-div" style="width: 95%">&nbsp;</div>
								</div>
								
								<table class="table table-bordered listViewEntriesTable">
									<thead>
										<tr class="listViewHeaders">
											<th class="blockHeader" colspan="17">Dettaglio eventi legati a fattura</th>
										</tr>
										<tr>
											<th style="color:#888888">Voce incasso</th>
											<th style="color:#888888">Tipo documento</th>
											<th style="color:#888888">Id eventi</th>
											<th style="color:#888888">Data evento</th>
											<th style="color:#888888">Locale-Spazio</th>
											<th style="color:#888888">Comune Locale </th>
											<th style="color:#888888">Importo Evento</th>
											<th style="color:#888888">N° PM attesi</th>
											<th style="color:#888888">N° PM rientrati</th>
											<th style="color:#888888">Fattura Validata</th>
											<th style="color:#888888">Valore netto</th>
											<th style="color:#888888">Aggio</th>
											<th style="color:#888888">Ex Art. 18</th>
											<th style="color:#888888">Flag megaconcert</th>
										</tr>
									</thead>
									<tbody>
										<tr class="listViewEntries" ng-repeat="evento in eventi track by $index">
											<td>{{evento.voceIncasso}}</td>
											<td>{{evento.tipoDocumentoContabile}}</td>
											<td><a ng-click="goTo('/eventoDetail/', evento.idEvento)"><u>{{evento.idEvento}}</u></a></td>
											<td>{{formatDate(evento.dataInizioEvento)}}</td>
											<td>{{evento.manifestazione.denominazioneLocale}}</td>
											<td>{{evento.manifestazione.comuneLocale}}</td>
											<td>{{evento.importDem | currency : "€" : 2}}</td>
											<td>{{previstiRientratiPrincipaliSpalla(evento.dettaglioPM[0].pmPrevisti,evento.dettaglioPM[0].pmPrevistiSpalla,evento.voceIncasso)}}</td>
											<td>{{previstiRientratiPrincipaliSpalla(evento.dettaglioPM[0].pmRientrati,evento.dettaglioPM[0].pmRientratiSpalla,evento.voceIncasso)}}</td>
											<td>{{evento.quadraturaNDM == 1 ? 'OK' : evento.quadraturaNDM == 0 ? 'KO' : ''}}</td>
											<td ng-show="evento.quadraturaNDM == 1 ? true : false">{{evento.importoSophiaNetto | currency : "€" : 2}}</td>
											<td ng-hide="evento.quadraturaNDM == 1 ? true : false"></td>
											<td>{{evento.importoAggio | currency : "€" : 2}}</td>
											<td>{{evento.importoExArt | currency : "€" : 2}}</td>
											<td ng-hide="evento.voceIncasso != 2244 || evento.quadraturaNDM != 1">
												<md-switch class="md-primary" ng-model="evento.manifestazione.flagMegaConcert" ng-change="saveMegaConcerto(evento.idEvento,evento.manifestazione.flagMegaConcert,$index)">
												{{evento.manifestazione.flagMegaConcert == true ? 'Si' : evento.manifestazione.flagMegaConcert == false ? 'No' : ''}}
												</md-switch>
											</td>
											<td ng-show="evento.voceIncasso != 2244 || evento.quadraturaNDM != 1">
											</td>
										</tr>
									</tbody>
								</table>
								<div class="contents-topscroll noprint">
									<div class="topscroll-div" style="width: 95%">&nbsp;</div>
								</div>
								<table class="table table-bordered listViewEntriesTable">
									<thead>
										<tr class="listViewHeaders">
											<th class="blockHeader" colspan="12">Riconciliazione fattura</th>
										</tr>
										<tr>
											<th style="color:#888888; width: 12%; text-align: center;" >Voci Incasso</th>
											<th style="color:#888888; width: 12%; text-align: center;" >Importo Sophia (SUn)</th>
											<th style="color:#888888; width: 12%; text-align: center;" >Importo NDM</th>
											<th style="color:#888888; width: 12%; text-align: center;" >Delta assoluto</th>
											<th style="color:#888888; width: 12%; text-align: center;" >Delta %</th>
											<th style="color:#888888; width: 12%; text-align: center;" >Forzatura</th>
											<th style="color:#888888; width: 12%; text-align: center;" >Azioni</th>
										</tr>
									</thead>
									<tbody>
										<tr class="listViewEntries" ng-repeat="item in riconciliazioneFattura">
											<td style="width: 12%; text-align: center;">{{item.voceIncasso}}</td>
											<td ng-class="item.semaforo == 'OK' ? 'black' : 'red'" style="width: 12%; text-align: center;">{{item.totaleLordoSophia | currency : "€" : 2}}</td>
											<td ng-class="item.semaforo == 'OK' ? 'black' : 'red'" style="width: 12%; text-align: center;">{{item.totaleLordoNDM | currency : "€" : 2}}</td>
											<td style="width: 12%; text-align: center;">{{item.deltaAssoluto | currency : "€" : 2}}</td>
											<td style="width: 12%; text-align: center;">{{item.deltaPercentuale | currency : "%" : 2}}</td>
											<td style="width: 12%; text-align: center;">{{item.forzaturaNDM}}</td>
										<td style="width: 23%; text-align: center;">
											<% if ("PERF_FUNZIONARIO".equals(groups.get(0))) {%>
											<button class="btn addButton" ng-show="item.riconciliabilePerImporto" ng-click="riconciliaImporti(item)"><span
													id="Riconcilia"></span> Riconcilia Importi </button>
											&nbsp;&nbsp;&nbsp;
											<button class="btn addButton" ng-show="item.riconciliabilePerVoceIncasso" ng-click="riconciliaVociIncasso(item)"><span
													id="Riconcilia"></span> Riconcilia
												Voci Incasso </button>
											&nbsp;&nbsp;&nbsp;
											<button class="btn addButton" ng-show="item.forzaturaNDM == 'M'" ng-click="ripristinaRiconciliazione(item)"><span
													id="Ripristina"></span> Ripristina </button>
											<%}%>
										</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>		
	</div>
</div>