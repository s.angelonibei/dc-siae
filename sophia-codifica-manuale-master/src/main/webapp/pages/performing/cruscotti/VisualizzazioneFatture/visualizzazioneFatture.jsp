<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@
	include file="../../../navbar.jsp"%>
	
	<div class="bodyContents">
	<div class="mainContainer row-fluid">

	<div class="contents-topscroll noprint">
		<div class="topscroll-div" style="width: 95%;margin-bottom: 20px">&nbsp;</div>
	</div>
	<div class="sticky">
		<div id="companyLogo" class="navbar commonActionsContainer noprint">
			<div class="actionsContainer row-fluid">
				<div class="span2">
					<span class="companyLogo"><img src="images/logo_siae.jpg"
						title="SIAE" alt="SIAE">&nbsp;</span>
				</div>
				<span class="btn-toolbar span4">
					<div class="pageNumbers alignTop pull-right"
						style="text-align: center;">
						<span class="pageNumbersText"><strong>Ricerca Fatture</strong></span>
					</div>
				</span>
			</div>
		</div>
	</div>
		
		<div class="contentsDiv marginLeftZero" id="rightPanel"
			style="min-height: 80px;">
			<div class="listViewPageDiv">
		<!-- table inserimento campi-->
				<div class="listViewActionsDiv row-fluid">
					<table
						class="table table-bordered blockContainer showInlineTable equalSplit">
						<thead>
							<tr>
								<th class="blockHeader" colspan="6" ng-show="ctrl.hideForm"><span
									ng-click="ctrl.hideForm=false"
									class="glyphicon glyphicon-expand"></span>&nbsp;Parametri
									Ricerca</th>
								<th class="blockHeader" colspan="6" ng-hide="ctrl.hideForm"><span
									ng-click="ctrl.hideForm=true"
									class="glyphicon glyphicon-collapse-down"></span>&nbsp;Parametri
									Ricerca</th>
							</tr>
						</thead>
						<tbody ng-hide="ctrl.hideForm">
							<tr>
								<td>
									<input id="sede" placeholder="Sede" ng-model="sede">
								</td>
								<td>
									<input id="seprag" placeholder="Seprag" ng-model="seprag">
								</td>
								<td>
									<input id="agenzia" placeholder="Agenzia" ng-model="agenzia">
								</td>
								<td>
									<input id="fattura" placeholder="Fattura" ng-model="fattura">
								</td>
								<td>
									<input id="reversale" placeholder="Reversale" ng-model="reversale">
								</td>
							</tr>
							<tr>
								<td>
									<input id="voceIncasso" placeholder="Voce Incasso" ng-model="voceIncasso">
								</td>
								<td>
									<input id="iva/CF" placeholder="P.IVA o CF" ng-model="ivaCF">
								</td>
								<td>
									<input id="codiceSAP" placeholder="Codice SAP" ng-model="codiceSAP">
								</td>
								<td>
									<select id="tipoDocumento" class="js-basic-single-select" ng-model="tipoDocumento" name="tipoDocumento" style="width: 150px; display: block;">
                                		<option value="">Tipo Documento</option>
                                		<option value="501">Entrate (501)</option>
                                		<option value="221">Uscite (221)</option>
                                	</select>
								</td>
								<td>
									<input id="idEvento" placeholder="Id Evento" ng-model="idEvento">
								</td>
							</tr>
							<tr>
								<td>
								<div class="controls">
									<div class="input-append date" id="datepickerFatturaStartFatturaDate"
										data-date-format="dd-mm-yyyy">
										<input placeholder="Data Fattura da" type="text" name="date"
											ng-model="dataFatturaDa"
											style="width: 135px; height: 25px"> <span
											class="add-on" style="height: 25px"><i class="icon-th"></i></span>
									</div>
									<script type="text/javascript">
										$("#datepickerFatturaStartFatturaDate")
											.datepicker(
												{
													format : 'dd-mm-yyyy',
													language : 'it',
													viewMode : "days",
													minViewMode : "days",
													orientation : "bottom auto",
													autoclose : "true"
												});
									</script>
								</div>
								</td>
								<td>
								<div class="controls">
								<div class="input-append date" id="datepickerFatturaEndFatturaDate"
								data-date-format="dd-mm-yyyy">
								<input placeholder="Data Fattura a" type="text" name="date"
								ng-model="dataFatturaA"
								style="width: 135px; height: 25px"> <span
								class="add-on" style="height: 25px"><i class="icon-th"></i></span>
								</div>
								<script type="text/javascript">
								$("#datepickerFatturaEndFatturaDate")
								.datepicker(
								{
								format : 'dd-mm-yyyy',
								language : 'it',
								viewMode : "days",
								minViewMode : "days",
								orientation : "bottom auto",
								autoclose : "true"
								});
								</script>
								</div>
								</td>

								<td>
								<div class="controls">
									<div class="input-append date" id="datepickerFatturaStartEventDate"
										data-date-format="dd-mm-yyyy">

										<input placeholder="Data Evento da" type="text" name="date"
											ng-model="dataEventoDa"
											style="width: 135px; height: 25px"> <span
											class="add-on" style="height: 25px"><i class="icon-th"></i></span>
									</div>
									<script type="text/javascript">
										$("#datepickerFatturaStartEventDate")
											.datepicker(
												{
													format : 'dd-mm-yyyy',
													language : 'it',
													viewMode : "days",
													minViewMode : "days",
													orientation : "bottom auto",
													autoclose : "true"
												});
									</script>
								</div>
								</td>
								<td>
								<div class="controls">
								<div class="input-append date" id="datepickerFatturaEndEventDate"
								data-date-format="dd-mm-yyyy">

								<input placeholder="Data Evento a" type="text" name="date"
								ng-model="dataEventoA"
								style="width: 135px; height: 25px"> <span
								class="add-on" style="height: 25px"><i class="icon-th"></i></span>
								</div>
								<script type="text/javascript">
								$("#datepickerFatturaEndEventDate")
								.datepicker(
								{
								format : 'dd-mm-yyyy',
								language : 'it',
								viewMode : "days",
								minViewMode : "days",
								orientation : "bottom auto",
								autoclose : "true"
								});
								</script>
								</div>
								</td>
								<td>
									<input id="importo" placeholder="Importo" ng-model="importo">
								</td>
							</tr>
							<tr>
							<td>
							<select style="width:150px" id="FatturaValidata" class="js-basic-single-select" ng-model="fatturaValidata">
							<option value="">Validazione Fatture:</option>
							<option value="notValidated">Fatture non validate</option>
							<option value="validated">Fatture validate</option>
							<option value="ok">Fattura validata OK</option>
							<option value="ko">Fattura validata KO</option>
							</select>
							</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							</tr>
							<tr>
								<td class="fieldLabel medium" style="text-align: right" nowrap
									colspan="6"><label class="muted pull-left marginLeft10px">Per effettuare la ricerca inserire almeno un campo</label>
									<button class="btn addButton" ng-click="filterApply()" 
									ng-show="(sede!=undefined && sede != '') || 
											 (seprag!=undefined && seprag != '') || 
											 (agenzia!=undefined && agenzia != '') || 
											 (fattura!=undefined && fattura != '') || 
											 (reversale!=undefined && reversale != '') || 
											 (voceIncasso!=undefined && voceIncasso != '') || 
											 (ivaCF!=undefined && ivaCF != '') || 
											 (codiceSAP!=undefined && codiceSAP != '') || 
											 (tipoDocumento!=undefined && tipoDocumento != '') || 
											 (idEvento!=undefined && idEvento != '') || 
											 (dataFatturaA!=undefined && dataFatturaA != '') ||
										     (dataFatturaDa!=undefined && dataFatturaDa != '') ||
											 (dataEventoA!=undefined && dataEventoA != '') ||
											 (dataEventoDa!=undefined && dataEventoDa != '') ||
											 (importo!=undefined && importo != '') ||
											 (fatturaValidata!=undefined && fatturaValidata != '')">
										<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Applica</strong>
									</button>
								</td>

							</tr>
						</tbody>
					</table>
					</div>
					
					<div>
					<!-- actions and pagination -->
						<div class="listViewTopMenuDiv noprint">
							<div class="listViewActionsDiv row-fluid">
								<!--  -->
								<span class="btn-toolbar span4"> <span class="btn-group">
	
								</span>
								</span>
								<!--  -->
								<!--  -->
								<span class="btn-toolbar span4"> <span
									class="customFilterMainSpan btn-group"
									style="text-align: center;">
										<div class="pageNumbers alignTop ">
											<span class="pageNumbersText"><strong> <!-- Titolo -->
											</strong></span>
										</div>
								</span>
								</span>
								<!--  -->
								<!--  -->
								<span class="span4 btn-toolbar">
									<div class="listViewActions pull-right">
	
										<div class="pageNumbers alignTop ">
											<span ng-show="ctrl.totRecords > 50">
												<span class="pageNumbersText" style="padding-right: 5px">
													Record da <strong>{{ctrl.page*50+1}}</strong> a <strong>{{(ctrl.page*50) + ctrl.samplings.length}}</strong> su <strong>{{totRecords}}</strong>
											</span>
												<button title="Precedente" class="btn"
													id="listViewPreviousPageButton" type="button"
													ng-click="navigateToPreviousPage()"
													ng-show="ctrl.page > 0">
													<span class="glyphicon glyphicon-chevron-left"></span>
												</button>
												<button title="Successiva" class="btn"
													id="listViewNextPageButton" type="button"
													ng-click="navigateToNextPage()" ng-show="ctrl.page < ctrl.pageTot">
													<span class="glyphicon glyphicon-chevron-right"></span>
												</button>
												<button title="Fine" class="btn"
													id="listViewNextPageButton" type="button"
													ng-click="navigateToEndPage()" ng-show="ctrl.page < ctrl.pageTot">
													<span class="glyphicon glyphicon-step-forward"></span>
												</button>
											</span>
										</div>
									</div>
									<div class="clearfix"></div>
								</span>
								<!--  -->
							</div>
						</div>
					<!-- table risultati -->
					<div class="listViewContentDiv" id="listViewContents">

						<div class="contents-topscroll noprint">
							<div class="topscroll-div" style="width: 95%">&nbsp;</div>
						</div>

						<div class="listViewEntriesDiv contents-bottomscroll">
							<div class="bottomscroll-div" style="width: 95%; min-height: 80px">

								<table class="table table-bordered listViewEntriesTable tableFormatted">
									<thead>
										<tr class="listViewHeaders">
											<th>Sede&nbsp;<span ng-click="orderImage('spanSede','SEDE')" id="spanSede" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
											<th>Seprag&nbsp;<span ng-click="orderImage('spanSeprag','SEPRAG')" id="spanSeprag" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
											<th>Agenzia&nbsp;<span ng-click="orderImage('spanAgenzia','AGENZIA')" id="spanAgenzia" style="font-size:80%" class="glyphicon glyphicon-sort"></span> </th>
											<th>Numero fattura&nbsp;<span ng-click="orderImage('spanFattura','NUMERO_FATTURA')" id="spanFattura" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
											<th>Numero reversale&nbsp;<span ng-click="orderImage('spanReversale','NUMERO_REVERSALE')" id="spanReversale" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
											<th>Voce incasso&nbsp;<span ng-click="orderImage('spanIncasso','VOCE_INCASSO')" id="spanIncasso" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
											<th>P.IVA&nbsp;<span ng-click="orderImage('spanOrganizzatorePIVA','PARTITA_IVA_ORGANIZZATORE')" id="spanOrganizzatorePIVA" style="font-size:80%" class="glyphicon glyphicon-sort"></span> /CF&nbsp;<span ng-click="orderImage('spanOrganizzatoreCF','CODICE_FISCALE_ORGANIZZATORE')" id="spanOrganizzatoreCF" style="font-size:80%" class="glyphicon glyphicon-sort"></span> Organizzatore</th>
											<th>Codice<br>SAP&nbsp;<span ng-click="orderImage('spanSAP','CODICE_SAP_ORGANIZZATORE')" id="spanSAP" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
											<th>Tipo documento&nbsp;<span ng-click="orderImage('spanDocumento','TIPO_DOCUMENTO_CONTABILE')" id="spanDocumento" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
											<th>Id<br>evento&nbsp;<span ng-click="orderImage('spanEvento','ID_EVENTO')" id="spanEvento" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
											<th>Data fattura&nbsp;<span ng-click="orderImage('spanDataFattura','DATA_REVERSALE')" id="spanDataFattura" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
											<th>Data evento&nbsp;<span ng-click="orderImage('spanDataEvento','DATA_INIZIO_EVENTO')" id="spanDataEvento" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
											<th>Importo voce incasso&nbsp;<span ng-click="orderImage('spanImportoIncasso','IMPORTO_DEM')" id="spanImportoIncasso" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
										</tr>
									</thead>
									<tbody>
									<!-- ngRepeat qui sotto! ng-repet -->
										<tr class="listViewEntries" ng-repeat="sampling in ctrl.samplings">
											<td>{{sampling.sede}}</td>
											<td>{{sampling.seprag}}</td>
											<td>{{sampling.agenzia}}</td>
											<td><a ng-click="goToFattura('/fatturaDetail/',sampling.numeroFattura)"><u>{{sampling.numeroFattura}}</u></a></td>
											<td>{{sampling.reversale}}</td>
											<td>{{sampling.voceIncasso}}</td>
											<td>{{sampling.manifestazione.partitaIvaOrganizzatore}}{{sampling.evento.partitaIvaOrganizzatore}}<br>{{sampling.manifestazione.codiceFiscaleOrganizzatore}}{{sampling.evento.codiceFiscaleOrganizzatore}}</td>
											<td>{{sampling.manifestazione.codiceSapOrganizzatore}}{{sampling.evento.codiceSapOrganizzatore}}</td>
											<td>{{sampling.tipoDocumentoContabile}}</td>
											<td><a ng-click="goTo('/eventoDetail/',sampling.idEvento)"><u>{{sampling.idEvento}}</u></a></td>
											<td>{{formatDate(sampling.dataReversale)}}</td>
											<td>{{formatDate(sampling.dataInizioEvento)}}</td>
											<td >{{sampling.importDem | currency : "€" : 2}} {{sampling.importoTotDem | currency : "€" : 2}}</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- table risultati -->
				</div>
			</div>
			<!-- <button class="btn addButton pull-right" ng-click="exportExcel()" style="margin: 0 2% 2% 0">Esportazione excel &nbsp;&nbsp;<span class="glyphicon glyphicon-download"></span></button> -->
		</div>	
	</div>
</div>