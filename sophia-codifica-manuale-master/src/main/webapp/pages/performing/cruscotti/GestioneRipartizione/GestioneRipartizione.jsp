<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%><%@
	include file="../../../navbar.jsp"%>
	
<div class="bodyContents">
	<div class="mainContainer row-fluid">
	
	<div class="contents-topscroll noprint">
		<div class="topscroll-div" style="width: 95%;margin-bottom: 20px">&nbsp;</div>
	</div>

	<div class="sticky">
		<div id="companyLogo" class="navbar commonActionsContainer noprint">
			<div class="actionsContainer row-fluid">
				<div class="span2">
					<span class="companyLogo"><img src="images/logo_siae.jpg"
						title="SIAE" alt="SIAE">&nbsp;</span>
				</div>
				<span class="btn-toolbar span4">
					<div class="pageNumbers alignTop pull-right"
						style="text-align: center;">
						<span class="pageNumbersText"><strong>Carichi di Ripartizione</strong></span>
					</div>
				</span>
			</div>
		</div>
	</div>

		<div class="contentsDiv marginLeftZero" id="rightPanel"	style="min-height: 80px;">
			<div class="listViewPageDiv">
				<div class="listViewActionsDiv row-fluid">
				
				<table class="table table-bordered blockContainer showInlineTable equalSplit">
					<thead>
						<tr>
							<th class="blockHeader" colspan="6" ng-show="ctrl.hideForm"><span
								ng-click="ctrl.hideForm=false"
								class="glyphicon glyphicon-expand"></span>&nbsp;Parametri
								Ricerca</th>
							<th class="blockHeader" colspan="6" ng-hide="ctrl.hideForm"><span
								ng-click="ctrl.hideForm=true"
								class="glyphicon glyphicon-collapse-down"></span>&nbsp;Parametri
								Ricerca</th>
						</tr>
					</thead>
					<tbody ng-hide="ctrl.hideForm">
						<tr>
							<td class="fieldLabel medium"><label
								class="muted pull-right marginRight10px">
									Contabilit&agrave da </label>
							</td>
							<td>
								<div class="controls">
									<div class="input-append date" id="datepickerRipartizioneContabilityStartDate"
										data-date-format="mm-yyyy">

										<input placeholder="mm-aaaa" type="text" name="date"
											ng-model="params.selectedContabilityDateFrom"
											style="width: 125px; height: 23px"> <span
											class="add-on" style="height: 23px"><i
											class="icon-th"></i></span>
									</div>
									<script type="text/javascript">
										$("#datepickerRipartizioneContabilityStartDate")
											.datepicker(
												{
													format : 'mm-yyyy',
													language : 'it',
													viewMode : "months",
													minViewMode : "months",
													orientation : "bottom auto",
													autoclose : "true"
												});
									</script>
								</div>
							</td>

							<td class="fieldLabel medium"><label
								class="muted pull-right "> <span class="redColor"
									ng-show="form.process.$invalid"></span>Contabilit&agrave a</label>
							</td>
							<td>
								<div class="controls">
									<div class="input-append date" id="datepickerRipartizioneContabilityEndDate"
										data-date-format="mm-yyyy">

										<input placeholder="mm-aaaa" type="text" name="date"
											ng-model="params.selectedContabilityDateTo"
											style="width: 125px; height: 23px"> <span
											class="add-on" style="height: 23px"><i
											class="icon-th"></i></span>
									</div>
									<script type="text/javascript">
										$("#datepickerRipartizioneContabilityEndDate")
											.datepicker(
												{
													format : 'mm-yyyy',
													language : 'it',
													viewMode : "months",
													minViewMode : "months",
													orientation : "bottom auto",
													autoclose : "true"
												});
									</script>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<input id="voceIncasso" placeholder="Voce Incasso" ng-model="params.voceIncasso">
							</td>
							<td>
								<input id="voceRipartizione" placeholder="Voce Ripartizione" ng-model="params.voceRipartizione">
							</td>
							<td>
							</td>
							<td>
							</td>
						</tr>
						<tr>
							<td class="fieldLabel medium" style="text-align: right" nowrap colspan="6">
								<button	class="btn addButton" ng-click="checkParams(params)">
									<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Applica</strong>
								</button>
							</td>
						</tr>
					</tbody>
				</table>
				
				<div>
					<div class="listViewTopMenuDiv noprint">
						<div class="listViewActionsDiv row-fluid">
							<!--  -->
							<span class="btn-toolbar span4"> <span class="btn-group">

							</span>
							</span>
							<!--  -->
							<!--  -->
							<span class="btn-toolbar span4"> <span
								class="customFilterMainSpan btn-group"
								style="text-align: center;">
									<div class="pageNumbers alignTop ">
										<span class="pageNumbersText"><strong> <!-- Titolo -->
										</strong></span>
									</div>
							</span>
							</span>
							<!--  -->
							<!--  -->
							<span class="span4 btn-toolbar">
								<div class="listViewActions pull-right">
									<div class="pageNumbers alignTop ">
										<span ng-show="sampling.totRecords > 50">
											<span class="pageNumbersText" style="padding-right: 5px">
												Record da <strong>{{saveQuery.page*50+1}}</strong> a <strong>{{(saveQuery.page*50) + sampling.length}}</strong> su <strong>{{sampling.totRecords}}</strong>
										</span>
											<button title="Precedente" class="btn"
												id="listViewPreviousPageButton" type="button"
												ng-click="navigateToPreviousPage()"
												ng-show="saveQuery.page > 0">
												<span class="glyphicon glyphicon-chevron-left"></span>
											</button>
											<button title="Successiva" class="btn"
												id="listViewNextPageButton" type="button"
												ng-click="navigateToNextPage()" ng-show="saveQuery.page < sampling.pageTot">
												<span class="glyphicon glyphicon-chevron-right"></span>
											</button>
											<button title="Fine" class="btn"
												id="listViewNextPageButton" type="button"
												ng-click="navigateToEndPage()" ng-show="saveQuery.page < sampling.pageTot">
												<span class="glyphicon glyphicon-step-forward"></span>
											</button>
										</span>
									</div>
								</div>
								<div class="clearfix"></div>
							</span>
							<!--  -->
						</div>
					</div>
				
					<div class="listViewContentDiv" id="listViewContents">
						<div class="contents-topscroll noprint">
							<div class="topscroll-div" style="width: 95%">&nbsp;</div>
						</div>

					<div class="listViewEntriesDiv contents-bottomscroll">
						<div class="bottomscroll-div" style="width: 95%; min-height: 80px">
						
						<table class="table table-bordered listViewEntriesTable">
								<thead>
									<tr class="listViewHeaders">
										<th>Periodo Contabile<br>(mese/anno)<button class="sortButton btn addButton" ng-click="orderImage('spanContabilita','')" id="buttonContabilita"><span id="spanContabilita" style="font-size:80%" class="glyphicon glyphicon-sort"></span></button></th>
										<th>Voce Ripartizione<button class="sortButton btn addButton" ng-click="orderImage('spanVoceRipartizione','')" id="buttonVoceRipartizione"><span id="spanVoceRipartizione" style="font-size:80%" class="glyphicon glyphicon-sort"></span></button></th>
										<th>Voce Incasso<button class="sortButton btn addButton" ng-click="orderImage('spanVoceIncasso','')" id="buttonVoceIncasso"><span id="spanVoceIncasso" style="font-size:80%" class="glyphicon glyphicon-sort"></span></button></th>
										<th>Totale SAP<button class="sortButton btn addButton" ng-click="orderImage('spanTotaleSAP','')" id="buttonTotaleSAP"><span id="spanTotaleSAP" style="font-size:80%" class="glyphicon glyphicon-sort"></span></button></th>
										<th>Totale SOPHIA<button class="sortButton btn addButton" ng-click="orderImage('spanTotaleSophia','')" id="buttonTotaleSophia"><span id="spanTotaleSophia" style="font-size:80%" class="glyphicon glyphicon-sort"></span></button></th>
										<th>Flag OK/KO<button class="sortButton btn addButton" ng-click="orderImage('spanFlag','')" id="buttonFlag"><span id="spanFlag" style="font-size:80%" class="glyphicon glyphicon-sort"></span></button></th>
									</tr>
								</thead>
								<tbody>
									<tr class="listViewEntries" ng-repeat="item in sampling">
										<td></td>
										<td></td>
										<td></td>
										<td>€</td>
										<td>€</td>
										<td></td>
									</tr>
								</tbody>
							</table>
						</div>
  					</div>
  				</div>
  			</div>
		</div>
  		</div>
  	</div>
  </div>
</div>