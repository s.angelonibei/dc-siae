<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%><%@
	include file="../../../navbar.jsp"%>
	
<div class="bodyContents">
	<div class="mainContainer row-fluid" ng-init="init({})">
	
		<div class="contents-topscroll noprint">
			<div class="topscroll-div" style="width: 95%;margin-bottom: 20px">&nbsp;</div>
		</div>
	
		<div class="sticky">
			<div id="companyLogo" class="navbar commonActionsContainer noprint">
				<div class="actionsContainer row-fluid">
					<div class="span2">
						<span class="companyLogo"><img src="images/logo_siae.jpg"
							title="SIAE" alt="SIAE">&nbsp;</span>
					</div>
					<span class="btn-toolbar span4">
						<div class="pageNumbers alignTop pull-right"
							style="text-align: center;">
							<span class="pageNumbersText"><strong>Regole di Ripartizione</strong></span>
						</div>
					</span>
				</div>
			</div>
		</div>
		
		<div style="padding:0 20px;">
			<button class="btn addButton" ng-click="openModelNew()">
				<span class="glyphicon glyphicon-cloud-upload"></span>&nbsp;&nbsp;<strong>Nuova Regola</strong>
			</button>
		</div>
		
		<div class="contentsDiv marginLeftZero" id="rightPanel"	style="min-height: 80px;">
			<div class="listViewPageDiv">
				<div class="listViewActionsDiv row-fluid">
					<table class="table table-bordered blockContainer showInlineTable equalSplit">
						<thead>
							<tr>
								<th class="blockHeader" colspan=6 ng-show="ctrl.hideForm"><span
									ng-click="ctrl.hideForm=false"
									class="glyphicon glyphicon-expand"></span>&nbsp;Parametri
									Ricerca</th>
								<th class="blockHeader" colspan=6 ng-hide="ctrl.hideForm"><span
									ng-click="ctrl.hideForm=true"
									class="glyphicon glyphicon-collapse-down"></span>&nbsp;Parametri
									Ricerca</th>
							</tr>
						</thead>
						<tbody ng-hide="ctrl.hideForm">
							<tr>
								<td class="fieldLabel medium">
									<label class="muted pull-right marginRight10px">
										Periodo di Ripartizione
									</label>
								</td>
								<td>
									<select ng-model="ricerca.periodoRipartizione">
										<option value=""></option>
										<option value="{{periodo.codice}}" ng-repeat="periodo in periodiRipartizione">{{periodo.codice + ' ' + '(' + (periodo.competenzeDa  | date: 'dd-MM-yyyy') + ' / ' + (periodo.competenzeA | date: 'dd-MM-yyyy') + ')' }}</option>
									</select>
								</td>
								<td class="fieldLabel medium">
									<label class="muted pull-right marginRight10px">
										Voce di incasso
									</label>
								</td>
								<td>
									<select ng-model="ricerca.voceIncasso">
										<option value=""></option>
										<option value="{{voce.codVoceIncasso}}" ng-repeat="voce in vociIncasso">{{voce.codVoceIncasso}}</option>
									</select>
								</td>
							</tr>
							<tr>
								<td class="fieldLabel medium">
									<label class="muted pull-right marginRight10px">
										Tipo di report
									</label>
								</td>
								<td>
									<select ng-model="ricerca.tipologiaReport">
										<option value=""></option>
										<option value="CARTACEO">Cartaceo</option>
										<option value="DIGITALE">Digitale</option>
										<option value="NOT">Non report</option>
									</select>
								</td>
								<td class="fieldLabel medium">
									<label class="muted pull-right marginRight10px">
										Regola Valorizzazione
									</label>
								</td>
								<td>
									<select ng-model="ricerca.regola">
										<option value=""></option>
										<option value="PRO_QUOTA">Pro quota</option>
										<option value="PRO_DURATA">Pro durata</option>
									</select>
								</td>
							</tr>
							<tr>
								<td class="fieldLabel medium">
									<label class="muted pull-right marginRight10px">
										Regole Ripartizione
									</label>
								</td>
								<td>
									<select ng-model="ricerca.ripartizione">
										<option value=""></option>
										<option value="analitico">Analitico</option>
										<option value="proporzionale">Proporzionale</option>
										<option value="campionario">Campionario</option>
									</select>
								</td>
								<td class="fieldLabel medium">
								</td>
								<td>
								</td>
							</tr>
							<tr>
								<td class="fieldLabel medium" style="text-align: right" nowrap colspan="6">
									<button	id="applica" class="btn addButton" ng-click="search(ricerca)">
										<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Applica</strong>
									</button>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			
			<div class="listViewContentDiv" id="listViewContents" ng-repeat = "configurazioniInfo in storico">
				<div class="contents-topscroll noprint">
					<div class="topscroll-div" style="width: 95%">&nbsp;</div>
				</div>
				<div class="listViewEntriesDiv contents-bottomscroll">
					<div class="bottomscroll-div" style="width: 95%; min-height: 80px">
						<table class="table table-bordered listViewEntriesTable tableFormatted">
							<thead>
								<tr ng-show="showConfigurazioni" ng-click="showConfigurazioni = !showConfigurazioni">
									<th style="border-bottom: 1px solid #dddddd !important;" colspan=10>
										{{configurazioniInfo.periodo}} ({{(configurazioniInfo.competenzaDa| date: 'dd/MM/yyyy') + '/' + (configurazioniInfo.competenzaA| date: 'dd/MM/yyyy')}})
										<span ng-show="getApproved(configurazioniInfo.periodo)" > APPROVATO</span>	
										<span class="glyphicon glyphicon-triangle-bottom pull-right"></span>	
									</th>
								</tr>
								<tr ng-show="!showConfigurazioni" ng-click="showConfigurazioni = !showConfigurazioni">
									<th style="border-bottom: 1px solid #dddddd !important;" colspan=10>
										{{configurazioniInfo.periodo}} ({{(configurazioniInfo.competenzaDa| date: 'dd/MM/yyyy') + '/' + (configurazioniInfo.competenzaA| date: 'dd/MM/yyyy')}})
										<span ng-show="getApproved(configurazioniInfo.periodo)" > - APPROVATO</span>	
										<span class="glyphicon glyphicon-triangle-top pull-right"></span>	
									</th>
								</tr>
								<tr class="listViewHeaders" ng-show="showConfigurazioni">
									<th>Voce Incasso</th>
									<th>Tipo report</th>
									<th>Regola Valorizzazione</th>
									<th>Ripartizione</th>
									<th>Tipo ripartizione</th>
									<th>Dettaglio</th>
									<th>Esclusioni</th>
									<th>Utente Ultima Modifica</th>
									<th>Data Ultima Modifica</th>
									<th>Azioni</th>
								</tr>
							</thead>
							<tbody ng-show="showConfigurazioni">
								<tr class="listViewEntries" ng-repeat="configurazione in configurazioniInfo.regoleRipartizioneDTO">
									<td>{{configurazione.voceIncasso}}</td>
									<td>{{configurazione.tipologiaReport}}</td>
									<td>{{configurazione.valorizzazione.regola}}</td>
									<td>{{configurazione.ripartizioneOS}}</td>
									<td>
										<label ng-repeat="(key, value) in configurazione.ripartizione">{{value > 0.00  && key != 'voceIncassoRilevazioni' && key != 'campionarioProgrammi' && key != 'campionarioRilevazioni' ? key + ' ' + value + '%' : ''}}</label>
									</td>
									<td>
										<label ng-if="configurazione.ripartizione.campionario > 0.00 && configurazione.ripartizione.campionarioProgrammi > 0.00">
											Programmi {{configurazione.ripartizione.campionarioProgrammi}} %
										</label>
										<label ng-if="configurazione.ripartizione.campionario > 0.00 && configurazione.ripartizione.campionarioRilevazioni > 0.00">
											Rilevazioni {{configurazione.ripartizione.campionarioRilevazioni}} %
										</label>
										<label ng-if="configurazione.ripartizione.campionario > 0.00 && configurazione.ripartizione.campionarioRilevazioni > 0.00">
											Voce Inc Ril {{configurazione.ripartizione.voceIncassoRilevazioni}} 
										</label>
										<label ng-if="!configurazione.ripartizione.campionario || configurazione.ripartizione.campionario <= 0.00">-</label>
									</td>
									<td>
										<label ng-repeat="(key, value) in configurazione.ripartizione.esclusioniUtilizzazioni">
											{{value ? (key == 'durataMinima' && value > 0 ? 'Esecuzioni sotto i ' + value + ' secondi' : key) : ''}}
										</label>
									</td>
									<td>{{configurazione.user}}</td>
									<td ng-bind="(configurazione.dataUltimaModifica | date: 'dd/MM/yyyy HH:mm:ss')"></td>
									<td>
										<button class="btn addButton" ng-click="openModelStorico(configurazioniInfo.periodo, configurazione)">
											<span class="glyphicon glyphicon glyphicon-th-list"></span>
										</button>
										<button class="btn addButton" ng-click="openModelModifica(configurazioniInfo.periodo, configurazione)">
											<span class="glyphicon glyphicon glyphicon-pencil"></span>
										</button> <br><br>
										<button class="btn addButton" ng-click="deleteConfigurazione(configurazioniInfo.periodo, configurazione)">
											<span class="glyphicon glyphicon glyphicon-trash"></span>
										</button>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
 				</div>
 			</div>
		</div>
	</div>
</div>