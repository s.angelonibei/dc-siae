<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="../../../../navbar.jsp" %>

<div class="bodyContents" ng-init="init()">
  <div class="mainContainer row-fluid">

    <div class="contents-topscroll noprint">
      <div class="topscroll-div" style="width: 95%;margin-bottom: 20px">
        &nbsp;
      </div>
    </div>
    <div class="sticky">
      <div id="companyLogo" class="navbar commonActionsContainer noprint">
        <div class="actionsContainer row-fluid">
          <div class="span2">
                        <span class="companyLogo"><img
                            src="images/logo_siae.jpg" title="SIAE"
                            alt="SIAE">&nbsp;</span>
          </div>
          <span class="btn-toolbar span4">
                        <div class="pageNumbers alignTop pull-right"
                             style="text-align: center;">
                            <span class="pageNumbersText"><strong>Dettagli Programma Musicale</strong></span>
                        </div>
                    </span>
        </div>
      </div>
    </div>

    <div class="contentsDiv marginLeftZero" id="rightPanel"
         style="min-height: 80px;">
      <div class="listViewPageDiv">
        <div class="listViewActionsDiv row-fluid">
          <div class="listViewPageDiv">
            <div class="listViewActionsDiv row-fluid">
                            <span class="pageNumbersText">
                                <button class="btn addButton" ng-click="back()">
                                    <span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Indietro</strong>
                                </button>
                            </span>
            </div>
          </div>

          <table
              class="table table-bordered blockContainer showInlineTable equalSplit">
            <thead>
            <tr>
              <th class="blockHeader" colspan="12">Riepilogo
                Programma Musicale
                <!-- <button class="btn addButton pull-right" ng-click="exportPM()">Esportazione PM &nbsp;&nbsp;<span class="glyphicon glyphicon-download"></span></button> -->
              </th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td class="fieldLabel medium">
                <label class="muted pull-right marginRight10px">Numero
                  Programma</label>
              </td>
              <td>
                <label>{{sampling.numeroProgrammaMusicale}}</label>
              </td>
              <td class="fieldLabel medium">
                <label class="muted pull-right marginRight10px">Voce
                  incasso</label>
              </td>
              <td>
                <label>{{sampling.voceIncasso}}</label>
              </td>
            </tr>
            <tr>
              <td class="fieldLabel medium">
                <label class="muted pull-right marginRight10px">Tipo
                  supporto</label>
              </td>
              <td>
                <label>{{formatFormato(sampling.foglio)}}</label>
              </td>
              <td class="fieldLabel medium">
                <label class="muted pull-right marginRight10px">Tipo</label>
              </td>
              <td>
                <label>{{sampling.tipoPm}}</label>
              </td>
            </tr>
            <tr>
              <td class="fieldLabel medium">
                <label class="muted pull-right marginRight10px">Flag
                  campionamento</label>
              </td>
              <td>
                <select id="flagCampionamento" style="width:35%"
                        ng-model="flagDaCampionare">
                  <!-- <option>{{sampling.flagDaCampionare}}</option> -->
                  <option value="1">Si</option>
                  <option value="0">No</option>
                </select>
                <button id="flagButton"
                        class="btn addButton pull-right"
                        style="display:inline"
                        ng-click="saveFlagCampionamento()"><span
                    class="glyphicon glyphicon-ok"></span>
                </button>
              </td>
              <td class="fieldLabel medium">
                <label class="muted pull-right marginRight10px">Codice
                  campionamento</label>
              </td>
              <td>
                <label>{{sampling.codiceCampionamento}}</label>
              </td>
            </tr>
            <tr>
              <td class="fieldLabel medium">
                <label class="muted pull-right marginRight10px">Risultato
                  calcolo campionamento</label>
              </td>
              <td>
                <label>{{sampling.risultatoCalcoloResto}}</label>
              </td>
              <td class="fieldLabel medium">
                <label class="muted pull-right marginRight10px">Totale
                  durata cedole</label>
              </td>
              <td>
                <label>{{sampling.totaleDurataCedole}}</label>
              </td>
            </tr>

            <tr>
              <td class="fieldLabel medium">
                <label class="muted pull-right marginRight10px">Totale
                  cedole</label>
              </td>
              <td>
                <label>{{sampling.totaleCedole}}</label>
              </td>
              <td class="fieldLabel medium">
                <label class="muted pull-right marginRight10px">Programma
                  foglio segue di:</label>
              </td>
              <td>
                <label>{{sampling.foglioSegueDi}}</label>
              </td>
            </tr>
            <tr>
              <td class="fieldLabel medium">
                <label class="muted pull-right marginRight10px">Data
                  rientro</label>
              </td>
              <td>
                <label>{{sampling.dataRientroPM}}</label>
              </td>
              <td class="fieldLabel medium">
                <label class="muted pull-right marginRight10px">Direttore
                  Esecuzione</label>
              </td>
              <td>
                <label>{{direttoreEsecuzione.codiceFiscale}}</label>
              </td>
            </tr>
            <tr>
              <td class="fieldLabel medium">
                <label
                    class="muted pull-right marginRight10px">Complesso</label>
              </td>
              <td>
                <label></label>
              </td>
              <td class="fieldLabel medium">
                <label class="muted pull-right marginRight10px">Flag
                  Gruppo Principale/Gruppo Spalla</label>
              </td>
              <td>
                <select id="flagGPS" style="width:35%"
                        ng-model="flagGruppoPrincipaleSpalla">
                  <!--  <option>{{sampling.flagGruppoPrincipale}}</option>-->
                  <option value="1">Si</option>
                  <option value="0">No</option>
                </select>
                {{flagGruppoPS(sampling.voceIncasso,sampling.flagGruppoPrincipale)}}{{gruppoTipo}}
                <button id="buttonGPS"
                        class="btn addButton pull-right"
                        style="display:inline"
                        ng-click="saveFlagGPS()"><span
                    class="glyphicon glyphicon-ok"></span>
                </button>
              </td>
            </tr>
            <tr>
              <td class="fieldLabel medium">
                <label class="muted pull-right marginRight10px">Stato</label>
              </td>
              <td>
                <label>{{getStatoPM(sampling.statoPm)}}</label>
              </td>
              <td class="fieldLabel medium">
                <label class="muted pull-right marginRight10px">Stato
                  Web</label>
              </td>
              <td>
                <label>{{sampling.statoWeb}}</label>
              </td>
            </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>


    <div class="contentsDiv marginLeftZero"
         style="min-height: 80px;">
      <div class="listViewPageDiv">
        <div class="listViewActionsDiv row-fluid">
          <table ng-repeat="evento in eventi"
                 class="table table-bordered blockContainer showInlineTable equalSplit">
            <thead>
            <tr>
              <th class="blockHeader" colspan="12">Riepilogo
                Evento
              </th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td class="fieldLabel medium">
                <label class="muted pull-right marginRight10px">Seprag
                  punto territoriale</label>
              </td>
              <td>
                <label>{{evento.seprag}}</label>
              </td>
              <td class="fieldLabel medium">
                <label class="muted pull-right marginRight10px">Locale</label>
              </td>
              <td>
                <label>{{evento.denominazioneLocale}}</label>
              </td>
            </tr>
            <tr>
              <td class="fieldLabel medium">
                <label class="muted pull-right marginRight10px">Codice
                  locale</label>
              </td>
              <td>
                <label>{{evento.codiceLocale}}</label>
              </td>
              <td class="fieldLabel medium">
                <label class="muted pull-right marginRight10px">Località</label>
              </td>
              <td>
                <label>{{evento.denominazioneLocalita}}</label>
              </td>
            </tr>
            <tr>
              <td class="fieldLabel medium">
                <label class="muted pull-right marginRight10px">Comune
                  (Provincia)</label>
              </td>
              <td>
                <label>{{evento.comuneLocale}}</label><label
                  ng-show="evento.siglaLocale">
                ({{evento.siglaLocale}})</label>
              </td>
              <td class="fieldLabel medium">
                <label class="muted pull-right marginRight10px"></label>
              </td>
              <td>
                <label></label>
              </td>
            </tr>
            <tr>
              <td class="fieldLabel medium">
                <label class="muted pull-right marginRight10px">Data
                  inizio evento</label>
              </td>
              <td>
                <label
                    ng-bind="(evento.dataInizioEvento | date: 'dd/MM/yyyy')"></label>
              </td>
              <td class="fieldLabel medium">
                <label class="muted pull-right marginRight10px">Data
                  fine evento</label>
              </td>
              <td>
                <label
                    ng-bind="(evento.dataFineEvento| date: 'dd/MM/yyyy')"></label>
              </td>
            </tr>

            <tr>
              <td class="fieldLabel medium">
                <label class="muted pull-right marginRight10px">Ora
                  inizio evento</label>
              </td>
              <td>
                <label ng-bind="formatHour(evento.oraInizioEvento)"></label>
              </td>
              <td class="fieldLabel medium">
                <label class="muted pull-right marginRight10px">Ora
                  fine evento</label>
              </td>
              <td>
                <label ng-bind="formatHour(evento.oraFineEvento)"></label>
              </td>
            </tr>
            <tr>
              <td class="fieldLabel medium" colspan="12"><b>Organizzatore</b>
              </td>
            </tr>
            <tr>
              <td class="fieldLabel medium">
                <label
                    class="muted pull-right marginRight10px">Nominativo</label>
              </td>
              <td>
                <label>{{evento.nomeOrganizzatore}}</label>
              </td>
              <td class="fieldLabel medium">
                <label
                    class="muted pull-right marginRight10px">P.IVA<br/>CF</label>
              </td>
              <td>
                <label>{{evento.partitaIvaOrganizzatore}}<br/>{{evento.codiceFiscaleOrganizzatore}}</label>
              </td>
            </tr>
            <tr>
              <td class="fieldLabel medium">
                <label class="muted pull-right marginRight10px">Codice
                  SAP</label>
              </td>
              <td>
                <label>{{evento.codiceSapOrganizzatore}}</label>
              </td>
              <td class="fieldLabel medium">
              </td>
              <td>
              </td>
            </tr>
            </tbody>
          </table>
          <div class="listViewContentDiv" id="listViewContents">

            <div class="contents-topscroll noprint">
              <div class="topscroll-div" style="width: 95%">
                &nbsp;
              </div>
            </div>

            <div class="listViewEntriesDiv contents-bottomscroll">
              <div class="bottomscroll-div"
                   style="width: 95%; min-height: 80px">

                <table class="table table-bordered listViewEntriesTable">
                  <thead>
                  <tr class="listViewHeaders">
                    <th class="blockHeader" colspan="12">
                      Riepilogo Movimenti
                    </th>
                  </tr>
                  <tr>
                    <th style="color:#888">Periodo rientro
                    </th>
                    <th style="color:#888">Evento</th>
                    <th style="color:#888">Voce incasso</th>
                    <th style="color:#888">Numero fattura
                    </th>
                    <th style="color:#888">Numero
                      reversale
                    </th>
                    <th style="color:#888">Data reversale
                    </th>
                    <th style="color:#888">Numero PM</th>
                    <th style="color:#888">Importo Netto Singolo PM</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr class="listViewEntries"
                      ng-repeat="movimento in movimenti track by $index">
                    <td>{{movimento.contabilita}}</td>
                    <td>
                      <a ng-click="goTo('/eventoDetail/', movimento.idEvento)"><u>{{movimento.idEvento}}</u></a>
                    </td>
                    <td>{{movimento.voceIncasso}}</td>
                    <td>
                      <a ng-click="goTo('/fatturaDetail/', movimento.numeroFattura)"><u>{{movimento.numeroFattura}}</u></a>
                    </td>
                    <td>{{movimento.reversale}}</td>
                    <td ng-bind="(movimento.dataReversale | date: 'dd/MM/yyyy')"></td>
                    <td>{{movimento.numProgrammaMusicale}}
                    </td>
                    <td id="importoRicalcolato{{$index}}">
                      {{movimento.importoValorizzato | currency : "€" : 2}}
                    </td>
                  </tr>
                  <tr>
                    <td colspan="7"><strong>Totale</strong>
                    </td>

                    <td><strong>{{totaleImportoRicalcolato |
                      currency : "€" : 2}}</strong></td>


                  </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <div class="listViewContentDiv">

            <div class="contents-topscroll noprint">
              <div class="topscroll-div" style="width: 95%">
                &nbsp;
              </div>
            </div>

            <div class="listViewEntriesDiv contents-bottomscroll">
              <div class="bottomscroll-div"
                   style="width: 95%; min-height: 80px">
                <form>
                  <table class="table table-responsive table-hover codificaPM">
                    <thead>
                    <tr class="listViewHeaders">
                      <th class="blockHeader"
                          colspan="14">Lista Opere
                        <button class="btn addButton pull-right"
                                ng-show="opere.length > 0"
                                style="margin-left:1%"
                                ng-click="exportOpere()">
                          Esportazione Lista Opere
                          &nbsp;&nbsp;<span
                            class="glyphicon glyphicon-download"></span>
                        </button>
                        <button class="btn addButton muted pull-right"
                                ng-click="addOpera(user,idProgrammaMusicale,numProgrammaMusicale,showError);">
                          <span class="glyphicon glyphicon-plus"></span>
                        </button>
                      </th>
                    </tr>
                    <tr>
                      <th style="color:#888">Codice</th>
                      <th style="color:#888">Titolo</th>
                      <th style="color:#888">Compositore
                      </th>
                      <th style="color:#888">Autore</th>
                      <th style="color:#888">Interprete
                      </th>
                      <th style="color:#888">Durata</th>
                      <th style="color:#888">Durata<br>&lt;
                        30 sec
                      </th>
                      <th style="color:#888">Importo<br>Netto<br>Cedola<br>Valorizzata
                      </th>
                      <th style="color:#888">Importo<br>Netto
                        In<br>Ripartizione
                      </th>
                      <th style="color:#888">Flag<br>PD
                      </th>
                      <th style="color:#888">Flag<br>Cedola<br>Esclusa
                      </th>
                      <%--<th style="color:#888; width: 1%">&nbsp;</th>--%>
                      <th style="color:#888; width: 1%">
                        &nbsp;
                      </th>
                      <th style="color:#888; width: 1%">
                        &nbsp;
                      </th>
                      <th style="color:#888; width: 1%">
                        &nbsp;
                      </th>
                    </tr>
                    </thead>
                    <tbody ng-repeat-start="opera in opere track by $index">
                    <tr id="opera{{$index}}">

                      <td ng-bind="checkNumOpera(opera.codiceOpera, '#opera' + $index,opera.tipoApprovazione)||opereDaApprovare[opera.idCombana].codApprov"></td>
                      <td ng-bind="opera.titoloComposizione"></td>
                      <td ng-bind="opera.compositore"></td>
                      <td ng-bind="opera.autori"></td>
                      <td ng-bind="opera.interpreti"></td>
                      <td ng-bind="formatDurata(opera.durata,opera.durataInferiore30sec,'#opera' + $index)"></td>
                      <td ng-bind="formatDurataInferiore(opera.durataInferiore30sec)"></td>
                      <td ng-bind="opera.nettoOriginario | currency : '€' : 2"></td>
                      <td ng-bind="opera.importoValorizzato | currency : '€' : 2"></td>
                      <td ng-bind="formatFlagPD(opera.flagPubblicoDominio)"></td>
                      <td ng-bind="formatFlagPD(opera.flagCedolaEsclusa)"></td>
                      <%--<td><button class="btn addButton pull-right" ng-click="searchCorresponding(user,opera,$index,showError,checkNumOpera)"><span class="glyphicon glyphicon-search"></span></button></td>--%>
                      <td style="text-align:center;">
                        <button type="button"
                                class="btn btn-default btn-xs"
                                ng-click="getCandidate(opera, user)"
                                ng-disabled="opera.codiceOpera"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Espandi e Codifica">
                          <span
                              class="glyphicon glyphicon-chevron-{{!codifica[opera.idCombana] || codifica[opera.idCombana].hidden ? 'right' : 'down'}}"
                              aria-hidden="true"></span>
                        </button>
                      </td>
                      <td>
                        <button class="btn addButton pull-right"
                                ng-click="modifyOpera(opera,formatDurata(opera.durata),showError,user,init);">
                          <span class="glyphicon glyphicon-pencil"></span>
                        </button>
                      </td>
                      <td>
                        <button
                            class="btn addButton muted pull-right marginRight10px"
                            ng-click="removeOpera(opera.id,showError,user,opera.codiceOpera,init);">
                          <span class="glyphicon glyphicon-minus"></span>
                        </button>
                      </td>
                    </tr>
                    </tbody>
                    <tbody
                        ng-if="codifica[opera.idCombana] && !codifica[opera.idCombana].hidden">
                    <tr>
                      <th>&nbsp;</th>
                      <th>Codice Opera</th>
                      <th>Titolo</th>
                      <th>Compositore</th>
                      <th>Autore</th>
                      <th>Interprete</th>
                      <th colspan="6">Grado di Confidenza
                      </th>
                      <th colspan="1"></th>
                      <th></th>
                    </tr>
                    <tr ng-if="codifica[opera.idCombana] && !codifica[opera.idCombana].hidden"
                        ng-repeat="candidata in codifica[opera.idCombana].candidate | limitTo: 2"
                        ng-click="addOperaCandidata(candidata, opera, $index)"
                        style="cursor: pointer"
                        ng-class="{selected: opereDaApprovare[opera.idCombana].codApprov === candidata.codiceOpera}">
                      <td>&nbsp;</td>
                      <td ng-bind="candidata.codiceOpera"></td>
                      <td>
                        <span ng-bind="candidata.titolo"></span><br>
                        <span
                            ng-bind="candidata.confTitolo | currency: '%' : 2"></span>
                      </td>
                      <td>
                        <span ng-bind="candidata.compositore"></span><br>
                        <span
                            ng-bind="candidata.confCompositore | currency: '%' : 2"></span>
                      </td>
                      <td>
                        <span ng-bind="candidata.autore"></span><br>
                        <span
                            ng-bind="candidata.confAutore | currency: '%' : 2"></span>
                      </td>
                      <td>
                        <span ng-bind="candidata.interprete"></span><br>
                        <span
                            ng-bind="candidata.confInterprete | currency: '%' : 2"></span>
                      </td>
                      <td colspan="6"
                          ng-bind="candidata.gradoDiConf | currency: '%' : 2"
                          style="vertical-align: middle;">
                      </th>
                      <td></td>
                      <td colspan="1"
                          style="text-align: center; vertical-align: middle">
                                                <span
                                                    class="glyphicon glyphicon-ok"
                                                    aria-hidden="true"
                                                    ng-if="opereDaApprovare[opera.idCombana].codApprov === candidata.codiceOpera"></span>
                      </td>
                    </tr>
                    </tbody>
                    <tbody ng-repeat-end
                           ng-if="codifica[opera.idCombana] && !codifica[opera.idCombana].hidden">
                    <tr>
                      <th colspan="14">
                        <button ng-click="invia()"
                                ng-submit="invia()"
                                class="btn addButton pull-right"
                                type="submit"
                                ng-disabled="!isValid()">
                          <span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Conferma</strong>
                        </button>
                      </th>
                    </tr>
                    </tbody>
                  </table>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>