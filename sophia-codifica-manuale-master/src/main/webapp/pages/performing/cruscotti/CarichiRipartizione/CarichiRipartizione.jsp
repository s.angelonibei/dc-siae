<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%><%@
	include file="../../../navbar.jsp"%>
	<input type="hidden" id="user" value="<%=(String) session.getAttribute("sso.user.userName")%>">
	
	<div class="bodyContents">
		<div class="mainContainer row-fluid" ng-init="codicePeriodo = null; init(codicePeriodo)">
			<div class="contents-topscroll noprint">
				<div class="topscroll-div" style="width: 95%;margin-bottom: 20px">&nbsp;</div>
			</div>
			<div class="sticky">
				<div id="companyLogo" class="navbar commonActionsContainer noprint">
					<div class="actionsContainer row-fluid">
						<div class="span2">
							<span class="companyLogo">
								<img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;
							</span>
						</div>
						<span class="btn-toolbar span4">
							<div class="pageNumbers alignTop pull-right" style="text-align: center;">
								<span class="pageNumbersText"><strong>Ripartizione Carichi</strong></span>
							</div>
						</span>
					</div>
				</div>
			</div>
			<div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px;">
				<div class="listViewPageDiv">
					<div class="listViewActionsDiv row-fluid">
					
					<table class="table table-bordered blockContainer showInlineTable equalSplit">
						<tbody>
							<tr>
								<td class="fieldLabel medium">
									<label class="muted pull-right marginRight10px">
										Data ultima valorizzazione
									</label>
								</td>
								<td>
									<label>{{infoValorizzatore.dataInizio}}</label>
								</td>
								<td class="fieldLabel medium">
									<label class="muted pull-right marginRight10px">
										Periodo di riferimento ultima valorizzazione
									</label>
								</td>
								<td>
									<label>{{getPeriodoCompetenzaCompleto(infoValorizzatore.periodoRipartizione)}} </label>
								</td>
							</tr>
							<tr>
								<td class="fieldLabel medium">
									<label class="muted pull-right marginRight10px">
										Valorizzazione in corso
									</label>
								</td>
								<td>
									<label>{{!infoValorizzatore.dataFine ? 'Si' : 'No'}}</label>
								</td>
								<td class="fieldLabel medium">
									<label class="muted pull-right marginRight10px">
										Periodo di riferimento valorizzazione in corso
									</label>
								</td>
								<td>
									<label>{{!infoValorizzatore.dataFine ? getPeriodoCompetenzaCompleto(infoValorizzatore.periodoRipartizione) : ''}}</label>
								</td>
							</tr>
							<tr>
								<td class="fieldLabel medium">
									<label class="muted pull-right marginRight10px">
										Periodo in corso
									</label>
								</td>
								<td>
									<label>{{getPeriodoCompetenzaCompleto(codicePeriodoInCorso)}}</label>
								</td>
								<td class="fieldLabel medium">
								</td>
								<td>
								</td>
							</tr>
						</tbody>
					</table>
			
					<div style="margin-top:5%" class="listViewContentDiv" id="listViewContents">
						<div style="padding:0 20px;">
							<label style="display:inline">Periodo Competenza Incassi: </label>
							<!-- <input style="text-align:center" value="{{carichiRipartizione.periodo}}" disabled> -->
							<select ng-model="codicePeriodo" ng-change=chiamata()>
								<option ng-repeat="periodo in periodiRipartizione" value="{{periodo.codice}}">
									{{periodo.codice + ' ' + '(' + (periodo.competenzeDa| date: 'dd-MM-yyyy') + ' / ' + (periodo.competenzeA| date: 'dd-MM-yyyy') + ')' }}
								</option>
							</select>
						</div>
						<div class="contents-topscroll noprint">
							<div class="topscroll-div" style="width: 95%">&nbsp;</div>
						</div>
						<!-- <label><b>{{carichiRipartizione.periodo}}</b></label> -->
						<label ng-show="carichiRipartizione.approvatore && carichiRipartizione.dataApprovazione">
							Nome approvatore: {{carichiRipartizione.approvatore}}, data approvazione: {{carichiRipartizione.dataApprovazione}}
						</label>
						
						<div class="listViewEntriesDiv contents-bottomscroll" ng-show="carichiRipartizione.length > 0">
							<div class="bottomscroll-div" style="width: 95%; min-height: 80px">
								<table class="table table-bordered listViewEntriesTable tableFormatted">
									<thead>
										<tr>
											<th class="blockHeader" colspan="10" ng-show="ctrl.hideCarichi"><span
												ng-click="ctrl.hideCarichi=false" class="glyphicon glyphicon-expand"></span>&nbsp;Carichi di Ripartizione</th>
												
											<th class="blockHeader" colspan="10" ng-hide="ctrl.hideCarichi"><span
												ng-click="ctrl.hideCarichi=true"
												class="glyphicon glyphicon-collapse-down"></span>&nbsp;Carichi</th>
										</tr>
										<tr ng-hide="ctrl.hideCarichi" class="listViewHeaders">
											<th>Voce Incasso</th>
											<th>Incasso Netto</th>
											<th>PM mancanti</th>
											<th>PM annullati</th>
											<th>Carico Disponibile</th>
											<th>Non codificato</th>
											<th>Bollettini irregolari</th>
											<th>Altre Cause Sospensione</th>
											<th>Carico Effettivo</th>
											<th>Approva</th>
										</tr>
									</thead>
									<tbody ng-hide="ctrl.hideCarichi" >
										<tr  class="listViewEntries" ng-repeat="voce in carichiRipartizione">
											<td>{{voce.voceIncasso}}</td>
											<td>{{voce.incassoNetto | currency : '€' : 2}}</td>
											<td>{{voce.pmMancanti ? voce.pmMancanti : 0 | currency : '€' : 2}}</td>
											<td>{{voce.pmAnnullati ? voce.pmAnnullati : 0 | currency : '€' : 2}}</td>
											<td>{{voce.caricoRipartizione | currency : '€' : 2}}</td>
											<td>{{voce.nonCodificato ? voce.nonCodificato : 0 | currency : '€' : 2}}</td>
											<td>{{voce.bollettiniIrregolari ? voce.bollettiniIrregolari : 0 | currency : '€' : 2}}</td>
											<td>{{voce.causeSospensione ? voce.causeSospensione : 0 | currency : '€' : 2}}</td>
											<td>{{voce.caricoRipartibile | currency : '€' : 2}}</td>
											<td ng-class="voce.ripartizione == true ? 'green' : ''">
												<div ng-hide="codicePeriodoInCorso != codicePeriodo" 
													 ng-click="codicePeriodoInCorso == codicePeriodo ? voce.ripartizione = !voce.ripartizione : null"
													 style="display: inline-block; position: relative; overflow: hidden; font-size:140%">
													<div class="box">
														<span ng-show="voce.ripartizione != undefined" style="margin-top:15%" ng-class="voce.ripartizione == true ? 'glyphicon glyphicon-ok-sign' : ''"></span>
													</div>
												</div>
												<div ng-show="codicePeriodoInCorso != codicePeriodo" 
													 style="display: inline-block; position: relative; overflow: hidden; font-size:140%">
													<span ng-show="voce.ripartizione != undefined" style="margin-top:15%" ng-class="voce.ripartizione == true ? 'glyphicon glyphicon-ok-sign' : ''"></span>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
		 				</div>
			 			
			 			<div class="listViewEntriesDiv contents-bottomscroll"  ng-repeat="(key, value) in carryOver">
							<div class="bottomscroll-div" style="width: 95%; min-height: 80px">
								<table class="table table-bordered listViewEntriesTable tableFormatted">
									<thead>
										<tr>
											<th class="blockHeader" colspan="10" ng-show="ctrl.hideCarryOver"><span
												ng-click="ctrl.hideCarryOver=false" class="glyphicon glyphicon-expand"></span>&nbsp;Carichi Non Approvati Periodo: {{getPeriodoCompetenza(key)}}</th>
											<th class="blockHeader" colspan="10" ng-hide="ctrl.hideCarryOver"><span
												ng-click="ctrl.hideCarryOver=true"
												class="glyphicon glyphicon-collapse-down"></span>&nbsp;Carichi Non Approvati Periodo: {{getPeriodoCompetenza(key)}}</th>
										</tr>
										<tr ng-hide="ctrl.hideCarryOver" class="listViewHeaders">
											<th>Voce Incasso</th>
											<th>Incasso Netto</th>
											<th>PM mancanti</th>
											<th>PM annullati</th>
											<th>Carico Disponibile</th>
											<th>Non codificato</th>
											<th>Bollettini irregolari</th>
											<th>Altre Cause Sospensione</th>
											<th>Carico Effettivo</th>
											<th>Approva</th>
										</tr>
									</thead>
									<tbody ng-hide="ctrl.hideCarryOver">
										<tr class="listViewEntries" ng-repeat="voce in value">
											<td>{{voce.voceIncasso}}</td>
											<td>{{voce.incassoNetto | currency : '€' : 2}}</td>
											<td>{{voce.pmMancanti ? voce.pmMancanti : 0 | currency : '€' : 2}}</td>
											<td>{{voce.pmAnnullati ? voce.pmAnnullati : 0 | currency : '€' : 2}}</td>
											<td>{{voce.caricoRipartizione | currency : '€' : 2}}</td>
											<td>{{voce.nonCodificato ? voce.nonCodificato : 0 | currency : '€' : 2}}</td>
											<td>{{voce.bollettiniIrregolari ? voce.bollettiniIrregolari : 0 | currency : '€' : 2}}</td>
											<td>{{voce.causeSospensione ? voce.causeSospensione : 0 | currency : '€' : 2}}</td>
											<td>{{voce.caricoRipartibile | currency : '€' : 2}}</td>
											<td ng-class="voce.ripartizione == true ? 'green' : ''">
												<div ng-hide="codicePeriodoInCorso != codicePeriodo" 
													 ng-click="codicePeriodoInCorso == codicePeriodo ? voce.ripartizione = !voce.ripartizione : null"
													 style="display: inline-block; position: relative; overflow: hidden; font-size:140%">
													<div class="box">
														<span ng-show="voce.ripartizione != undefined" style="margin-top:15%" ng-class="voce.ripartizione == true ? 'glyphicon glyphicon-ok-sign' : ''"></span>
													</div>
												</div>
												<div ng-show="codicePeriodoInCorso != codicePeriodo" 
													 style="display: inline-block; position: relative; overflow: hidden; font-size:140%">
													<span ng-show="voce.ripartizione != undefined" style="margin-top:15%" ng-class="voce.ripartizione == true ? 'glyphicon glyphicon-ok-sign' : ''"></span>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
		 				</div>
						<div class="listViewEntriesDiv contents-bottomscroll" ng-hide="CarichiVuoti(carichiRipartizione,carryOver)">
							<div class="bottomscroll-div" style="width: 95%; min-height: 80px">
				 				<table class="table table-bordered listViewEntriesTable tableFormatted">
				 					<tbody >
										<tr ng-hide="codicePeriodoInCorso != codicePeriodo || ripartiti">
											<td class="fieldLabel medium" style="text-align: right" nowrap colspan=10>
												<label class="muted pull-left marginLeft10px">Per scegliere se ripartire o meno, cliccare sul riquadro della colonna 'Ripartire'</label>
												<input type="button" id="applica" class="btn" style="background: #00b4f9; color: #fff !important; border: #00b4f9 1px solid; border-radius: 17px !important;" 
												ng-click="approva(carichiRipartizione,carryOver)" 
														ng-disabled="infoValorizzatore.running" 
														value="Approva">
											</td>
										</tr>	
									</tbody>
								</table>
							</div>
						</div>
		 				<div>
						 	<label ng-show="CarichiVuoti(carichiRipartizione,carryOver)"><b>Non ci sono carichi per il periodo selezionato</b></label>
						</div>
		 			</div>
		 		</div>
		 	</div>
		 </div>
	</div>
</div>