<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%><%@
	include file="../../../navbar.jsp"%>
	<input type="hidden" id="user" value="<%=(String) session.getAttribute("sso.user.userName")%>">
	
	<div class="bodyContents" ng-init="init({})">
		<div class="mainContainer row-fluid">
		
			<div class="contents-topscroll noprint">
				<div class="topscroll-div" style="width: 95%;margin-bottom: 20px">&nbsp;</div>
			</div>
			
			<div class="sticky">
				<div id="companyLogo" class="navbar commonActionsContainer noprint">
					<div class="actionsContainer row-fluid">
						<div class="span2">
							<span class="companyLogo">
								<img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;
							</span>
						</div>
						<span class="btn-toolbar span4">
							<div class="pageNumbers alignTop pull-right" style="text-align: center;">
								<span class="pageNumbersText"><strong>Configurazioni Soglie</strong></span>
							</div>
						</span>
					</div>
				</div>
			</div>
			
			<div style="padding:0 20px;">
				<button class="btn addButton" ng-click="openModelUpload(user)">
					<span class="glyphicon glyphicon-cloud-upload"></span>&nbsp;&nbsp;<strong>Nuova Configurazione</strong>
				</button>
			</div>
			
			<div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px;">
				<div class="listViewPageDiv">
					<div class="listViewActionsDiv row-fluid">
					
						<table class="table table-bordered blockContainer showInlineTable equalSplit">
							<thead>
								<tr>
									<th class="blockHeader" colspan="6" ng-show="ctrl.hideForm"><span
										ng-click="ctrl.hideForm=false"
										class="glyphicon glyphicon-expand"></span>&nbsp;Parametri
										Ricerca</th>
									<th class="blockHeader" colspan="6" ng-hide="ctrl.hideForm"><span
										ng-click="ctrl.hideForm=true"
										class="glyphicon glyphicon-collapse-down"></span>&nbsp;Parametri
										Ricerca</th>
								</tr>
							</thead>
							<tbody ng-hide="ctrl.hideForm">
								<tr>
									<td class="fieldLabel medium"><label
										class="muted pull-right marginRight10px">
											Periodo Competenza da </label>
									</td>
									<td>
										<div class="controls">
											<div class="input-append date" id="datepickerCSStartDate"
												data-date-format="mm-yyyy">
		
												<input placeholder="gg-mm-aaaa" type="text" name="date"
													ng-model="cs.inizioPeriodoCompetenza"
													style="width: 125px; height: 23px"> <span
													class="add-on" style="height: 23px"><i
													class="icon-th"></i></span>
											</div>
											<script type="text/javascript">
												$("#datepickerCSStartDate")
													.datepicker(
														{
															format : 'dd-mm-yyyy',
															language : 'it',
															viewMode : "days",
															minViewMode : "days",
															orientation : "bottom auto",
															autoclose : "true"
														});
											</script>
										</div>
									</td>
		
									<td class="fieldLabel medium"><label
										class="muted pull-right "> <span class="redColor"
											ng-show="form.process.$invalid"></span>Periodo Competenza a</label>
									</td>
									<td>
										<div class="controls">
											<div class="input-append date" id="datepickerCSEndDate"
												data-date-format="mm-yyyy">
		
												<input placeholder="gg-mm-aaaa" type="text" name="date"
													ng-model="cs.finePeriodoCompetenza"
													style="width: 125px; height: 23px"> <span
													class="add-on" style="height: 23px"><i
													class="icon-th"></i></span>
											</div>
											<script type="text/javascript">
												$("#datepickerCSEndDate")
													.datepicker(
														{
															format : 'dd-mm-yyyy',
															language : 'it',
															viewMode : "days",
															minViewMode : "days",
															orientation : "bottom auto",
															autoclose : "true"
														});
											</script>
										</div>
									</td>
								</tr>
								<tr>
									<td class="fieldLabel medium" style="text-align: right" nowrap colspan="6">
										<button	id="applica" class="btn addButton" ng-click="search(cs)">
											<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Applica</strong>
										</button>
									</td>
								</tr>
							</tbody>
						</table>
						
						
						<div class="listViewTopMenuDiv noprint">
							<div class="listViewActionsDiv row-fluid">
								<!--  -->
								<span class="btn-toolbar span4"> <span class="btn-group">
	
								</span>
								</span>
								<!--  -->
								<!--  -->
								<span class="btn-toolbar span4"> <span
									class="customFilterMainSpan btn-group"
									style="text-align: center;">
										<div class="pageNumbers alignTop ">
											<span class="pageNumbersText"><strong> <!-- Titolo -->
											</strong></span>
										</div>
								</span>
								</span>
								<!--  -->
								<!--  -->
								<span class="span4 btn-toolbar">
									<div class="listViewActions pull-right">
										<div class="pageNumbers alignTop ">
											<span ng-show="paging.totRecords > 50">
												<span class="pageNumbersText" style="padding-right: 5px">
													Record da <strong>{{paging.firstRecord}}</strong> a <strong>{{paging.lastRecord}}</strong> su <strong>{{paging.totRecords}}</strong>
											</span>
												<button title="Precedente" class="btn"
													id="listViewPreviousPageButton" type="button"
													ng-click="navigateToPreviousPage()"
													ng-show="paging.currentPage > 1">
													<span class="glyphicon glyphicon-chevron-left"></span>
												</button>
												<button title="Successiva" class="btn"
													id="listViewNextPageButton" type="button"
													ng-click="navigateToNextPage()" ng-show="paging.currentPage < paging.totPages">
													<span class="glyphicon glyphicon-chevron-right"></span>
												</button>
												<button title="Fine" class="btn"
													id="listViewNextPageButton" type="button"
													ng-click="navigateToEndPage()" ng-show="paging.currentPage < paging.totPages">
													<span class="glyphicon glyphicon-step-forward"></span>
												</button>
											</span>
										</div>
									</div>
									<div class="clearfix"></div>
								</span>
								<!--  -->
							</div>
						</div>
						
						<div class="listViewContentDiv" id="listViewContents">
							<div class="contents-topscroll noprint">
								<div class="topscroll-div" style="width: 95%">&nbsp;</div>
							</div>
							<div class="listViewEntriesDiv contents-bottomscroll">
								<div class="bottomscroll-div" style="width: 95%; min-height: 80px">
									<table class="table table-bordered listViewEntriesTable tableFormatted">
										<thead>
											<tr class="listViewHeaders">
												<th colspan="1">ID</th>
												<th colspan="2">Periodo Competenza da</th>
												<th colspan="2">Periodo Competenza a</th>
												<th colspan="2">Data caricamento</th>
												<th colspan="2">Data ultima modifica</th>
												<th colspan="1">Utente</th>
												<th colspan="2">Nome file</th>
												<th colspan="1">Download</th>
												<th colspan="1">Storico</th>
												<th colspan="1">Modifica</th>
											</tr>
										</thead>
										<tbody >
											<tr class="listViewEntries" ng-repeat="data in response">
												<td colspan="1" ng-bind="data.codiceConfigurazione"></td>
												<td colspan="2" ng-bind="(data.inizioPeriodoCompetenza | date: 'dd/MM/yyyy')"></td>
												<td colspan="2" ng-bind="(data.finePeriodoCompetenza | date: 'dd/MM/yyyy')"></td>
												<td colspan="2" ng-bind="(data.dataCaricamentoFile | date: 'dd/MM/yyyy HH:mm:ss')"></td>
												<td colspan="2" ng-bind="(data.dataUltimaModifica | date: 'dd/MM/yyyy HH:mm:ss')"></td>
												<td colspan="1" ng-bind="data.utenteUltimaModifica"></td>
												<td colspan="2" ng-bind="data.nomeOriginaleFile"></td>
												<td colspan="1"><button class="btn addButton" ng-click="downloadConfigurazione(data)"><span class="glyphicon glyphicon glyphicon-download"></span></button></td>
												<td colspan="1"><button class="btn addButton" ng-click="openModelStorico(data.codiceConfigurazione)"><span class="glyphicon glyphicon glyphicon-th-list"></span></button></td>
												<td colspan="1"><button class="btn addButton" ng-click="openModelModifica(data)"><span class="glyphicon glyphicon glyphicon-pencil"></span></button></td>
											</tr>
										</tbody>
									</table>
								</div>
			 				</div>
			 			</div>
					</div>
				</div>
			</div>
		</div>
	</div>