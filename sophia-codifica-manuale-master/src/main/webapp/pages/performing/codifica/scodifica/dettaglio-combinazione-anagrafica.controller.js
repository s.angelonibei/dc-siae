(function () {
    'use strict';
    angular.module('codmanApp')
        .controller('DettaglioCombinazioneAnagraficaController',DettaglioCombinazioneAnagraficaController);

    DettaglioCombinazioneAnagraficaController.$inject = ['ngDialog', 'dca', '$scope','UtilService','scodificaService'];

    function DettaglioCombinazioneAnagraficaController(ngDialog,dca, $scope, UtilService, scodificaService){
        var vm = this;
        vm.dca = dca;

        vm.modificaCodiceOpera = modificaCodiceOpera;
        vm.cancel = cancel;
        vm.filterTotals = filterTotals;
        vm.filterOthers = filterOthers;
        vm.filterImportiByPR = filterImportiByPR;
        vm.exportTable = UtilService.exportTable;
        vm.getListaUtilizzazioni = getListaUtilizzazioni;

        function cancel() {
            ngDialog.closeAll(false);
        }

        function modificaCodiceOpera() {
            scodificaService.getSessioneScodifica($scope.ngDialogData.combana, $scope.ngDialogData.user)
        }

        function filterTotals(e){
            return e.area === 'Totale';
        }

        function filterOthers(e){
            return e.area !== 'Totale';
        }

        function filterImportiByPR(e){
            return e.periodoRipartizione === importi.periodoRipartizione;
        }

        function getListaUtilizzazioni() {
            return scodificaService.getListaUtilizzazioni($scope.ngDialogData.combana.idCombana);
        }
    }
})();