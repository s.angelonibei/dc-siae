<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="../../../navbar.jsp" %>

<div class="bodyContents">
  <div class="mainContainer row-fluid">

    <div class="contents-topscroll noprint">
      <div class="topscroll-div" style="width: 95%;margin-bottom: 20px">
        &nbsp;
      </div>
    </div>

    <div class="sticky">
      <div id="companyLogo" class="navbar commonActionsContainer noprint">
        <div class="actionsContainer row-fluid">
          <div class="span2">
							<span class="companyLogo">
								<img src="images/logo_siae.jpg" title="SIAE"
                     alt="SIAE">&nbsp;
							</span>
          </div>
          <span class="btn-toolbar span4">
							<div class="pageNumbers alignTop pull-right"
                   style="text-align: center;">
								<span class="pageNumbersText"><strong>Scodifica</strong></span>
							</div>
						</span>
        </div>
      </div>
    </div>

    <div class="contentsDiv marginLeftZero" id="rightPanel"
         style="min-height: 80px;">
      <div class="listViewPageDiv">
        <div class="listViewActionsDiv row-fluid">
          <form name="form">
            <input type="hidden" id="user" name="user" ng-value="'<%=(String) session.getAttribute("sso.user.userName")%>'" ng-model="vm.scodificaRequest.user">
            <table class="table table-bordered blockContainer showInlineTable equalSplit">
              <thead>
              <tr>
                <th class="blockHeader" colspan="8"><span
                    ng-click="vm.hideForm=!vm.hideForm"
                    class="glyphicon glyphicon-expand"></span>&nbsp;Parametri
                  Ricerca
                </th>
              </tr>
              </thead>
              <tbody ng-hide="vm.hideForm">
              <tr>
                <td class="fieldLabel medium"><label class="muted pull-right">Titolo</label></td>
                <td>
                  <input ng-model="vm.scodificaRequest.titolo" name="titolo" id="titolo" ng-minlength="3" />
                  <label class="muted">
                    <span class="redColor" ng-show="form.titolo.$error.minlength">La lunghezza minima è di 3 caratteri</span>
                  </label>
                </td>
                <td class="fieldLabel medium"><label class="muted pull-right">Nominativo</label></td>
                <td>
                  <input ng-model="vm.scodificaRequest.nominativo" name="nominativo" id="nominativo" ng-minlength="3" />
                  <label class="muted">
                    <span class="redColor" ng-show="form.nominativo.$error.minlength">La lunghezza minima è di 3 caratteri</span>
                  </label>
                </td>
                <td class="fieldLabel medium"><label class="muted pull-right">Codice Opera</label></td>
                <td>
                  <input ng-model="vm.scodificaRequest.codiceOperaApprovato" name="codiceOperaApprovato" id="codiceOperaApprovato" ng-minlength="11" ng-maxlength="11" maxlength="11"/>
                  <label class="muted">
                    <span class="redColor" ng-show="form.codiceOperaApprovato.$error.minlength||form.codiceOperaApprovato.$error.maxlength">La lunghezza deve essere esattamente di 11 caratteri</span>
                  </label>
                </td>
                <td class="fieldLabel medium">
                  <label class="muted pull-right ">
                    ID Combana
                  </label>
                </td>
                <td><input ng-model="vm.scodificaRequest.idCombana"/></td>

              </tr>
              <%--<tr>
                  <td class="fieldLabel medium"><label class="muted pull-right">Ripartizioni di riferimento Da</label></td>
                  <td style="white-space: nowrap">
                      <div class="dropdown dropdown1-parent">
                          <div class="input-group">
                              <input type="text" name="ripartizioniRiferimentoDa" id="ripartizioniRiferimentoDa" class="form-control" data-ng-model="vm.scodificaRequest.ripartizioniRiferimentoDa" data-date-time-input="DD-MM-YYYY" role="button" data-toggle="dropdown" data-target=".dropdown1-parent" onkeydown="return false;">
                              <i class="glyphicon glyphicon-calendar"></i>
                              <i class="glyphicon glyphicon-erase" ng-click="vm.scodificaRequest.ripartizioniRiferimentoDa=undefined"></i>
                          </div>
                          <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                              <datetimepicker   data-ng-model="vm.scodificaRequest.ripartizioniRiferimentoDa"
                                                data-datetimepicker-config="{ dropdownSelector: '#ripartizioniRiferimentoDa', minView: 'day'}"></datetimepicker>
                          </ul>
                      </div>
                  </td>
                  <td class="fieldLabel medium"><label class="muted pull-right ">A</label></td>
                  <td style="white-space: nowrap">
                      <div class="dropdown dropdown2-parent">
                          <div class="input-group">
                              <input type="text" name="ripartizioniRiferimentoA" id="ripartizioniRiferimentoA" class="form-control" data-ng-model="vm.scodificaRequest.ripartizioniRiferimentoA" data-date-time-input="DD-MM-YYYY" role="button" data-toggle="dropdown" data-target=".dropdown2-parent" onkeydown="return false;">
                              <i class="glyphicon glyphicon-calendar"></i>
                              <i class="glyphicon glyphicon-erase" ng-click="vm.scodificaRequest.ripartizioniRiferimentoA=undefined"></i>
                          </div>
                          <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                              <datetimepicker   data-ng-model="vm.scodificaRequest.ripartizioniRiferimentoA"
                                                data-datetimepicker-config="{ dropdownSelector: '#ripartizioniRiferimentoA', minView: 'day'}"></datetimepicker>
                          </ul>
                      </div>
                  </td>
                  <td class="fieldLabel medium"><label class="muted pull-right ">N. Programma Musicale</label></td>
                  <td colspan="2"><input ng-model="vm.scodificaRequest.numeroProgrammaMusicale"/></td>
              </tr>--%>
              <tr>
                <td class="fieldLabel medium"
                    style="text-align: right" nowrap colspan="8">
                  <button type="submit" id="applica" class="btn addButton"
                          ng-click="vm.getListaScodifica(0)"
                          ng-disabled="(!form.titolo.$valid && !form.nominativo.$valid && !form.codiceOperaApprovato.$valid)  || !vm.isValid()">
                    <span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Applica</strong>
                  </button>
                  <button type="button"
                          ng-click="vm.getListaScodifica()"
                          ng-disabled="!vm.isValid() || !vm.listaScodifica || vm.listaScodifica.length === 0"
                          class="btn addButton">
                    <span class="glyphicon glyphicon-export"></span>&nbsp;&nbsp;
                    <strong>Esporta Risultati</strong>
                  </button>
                </td>
              </tr>
              </tbody>
            </table>
          </form>
          <div class="listViewTopMenuDiv noprint" ng-show="vm.pagination">
            <div class="listViewActionsDiv row-fluid">
                            <span class="btn-toolbar span4">
                                <span class="btn-group"></span>
                            </span>
              <span class="btn-toolbar span4"> <span
                  class="customFilterMainSpan btn-group"
                  style="text-align: center;">
										<div class="pageNumbers alignTop ">
											<span class="pageNumbersText"><strong>
											</strong></span>
										</div>
								</span>
								</span>
              <span class="span4 btn-toolbar">
									<div class="listViewActions pull-right">
										<div class="pageNumbers alignTop ">
											<span>
												<span class="pageNumbersText"
                              style="padding-right: 5px">
													Record da <strong ng-bind="((vm.pagination.pageNumber - 1) * vm.pagination.pageSize) + 1"></strong> a <strong ng-bind="((vm.pagination.pageNumber - 1) * vm.pagination.pageSize + vm.pagination.pageSize)"></strong>
											</span>
												<button title="Precedente"
                                class="btn"
                                id="listViewPreviousPageButton"
                                type="button"
                                ng-click="vm.getListaScodifica(-1);"
                                ng-show="vm.pagination.pageNumber > 1">
													<span class="glyphicon glyphicon-chevron-left"></span>
												</button>
												<button title="Successiva"
                                class="btn"
                                id="listViewNextPageButton"
                                type="button"
                                ng-click="vm.getListaScodifica(1);"
                                ng-show="vm.listaScodifica.length === vm.pagination.pageSize">
													<span class="glyphicon glyphicon-chevron-right"></span>
												</button>
											</span>
										</div>
									</div>
									<div class="clearfix"></div>
								</span>
              <!--  -->
            </div>
          </div>

          <div class="listViewContentDiv" id="listViewContents" ng-if="vm.listaScodifica">
            <div class="contents-topscroll noprint">
              <div class="topscroll-div" style="width: 95%">
                &nbsp;
              </div>
            </div>
            <div class="listViewEntriesDiv contents-bottomscroll">
              <div class="bottomscroll-div"
                   style="width: 95%; min-height: 80px">
                <table class="table table-bordered listViewEntriesTable">
                  <thead>
                  <tr class="listViewHeaders">
                    <th style="color:#888">ID Combana</th>
                    <th style="color:#888">Titolo</th>
                    <th style="color:#888">Compositori</th>
                    <th style="color:#888">Autori</th>
                    <th style="color:#888">Interpreti</th>
                    <th style="color:#888">Codice Opera</th>
                    <th style="color:#888">Importo &euro;</th>
                    <th style="color:#888">N. Utilizzazioni</th>
                    <th colspan="2">&nbsp;</th>
                  </tr>
                  </thead>
                  <tbody
                      ng-repeat-start="scodifica in vm.listaScodifica track by $index">
                  <tr class="listViewEntries">
                    <td><span ng-bind="scodifica.idCombana" ng-click="vm.getDettaglioCombinazioneAnagrafica(scodifica,'<%=(String) session.getAttribute("sso.user.userName")%>')" class="clickable"></span></td>
                    <td ng-bind="scodifica.titolo"></td>
                    <td ng-bind="scodifica.compositori"></td>
                    <td ng-bind="scodifica.autori"></td>
                    <td ng-bind="scodifica.interpreti"></td>
                    <td ng-bind="scodifica.codiceOperaApprovato"></td>
                    <td ng-bind="scodifica.valoreEconomico | currency : '&euro;' : 2"></td>
                    <td ng-bind="scodifica.nUtilizzazioni"></td>
                    <td>
                      <button type="button" class="btn btn-default" aria-label="Dettaglio" ng-click="vm.showDetail(scodifica)" ng-disabled="!scodifica.idCodifica">
                        <span class="glyphicon glyphicon-chevron-{{!scodifica.showDetail || !scodifica.dettaglio ? 'right' : 'down'}}" aria-hidden="true"></span>
                      </button>
                    </td>
                    <td>
                      <button type="button" class="btn btn-default" aria-label="Modifica" ng-click="vm.modificaCodiceOpera(scodifica,'<%=(String) session.getAttribute("sso.user.userName")%>')">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                      </button>
                    </td>
                  </tr>
                  </tbody>
                  <tbody ng-repeat-end>
                  <tr ng-if="scodifica.showDetail && scodifica.dettaglio">
                    <th>&nbsp;</th>
                    <th>Data Creazione</th>
                    <th>Data Codifica</th>
                    <th>Tipo Acquisizione</th>
                    <th>Tipo Approvazione</th>
                    <th>Confidenza</th>
                    <th>Utente Codifica</th>
                    <th colspan="4">&nbsp;</th>
                  </tr>
                  <tr ng-if="scodifica.showDetail && scodifica.dettaglio">
                    <td>&nbsp;</td>
                    <td ng-bind="scodifica.dettaglio.dataCreazione | date : 'dd/MM/yyyy'"></td>
                    <td ng-bind="scodifica.dettaglio.dataCodifica | date : 'dd/MM/yyyy'"></td>
                    <td ng-bind="scodifica.dettaglio.descrizione"></td>
                    <td ng-bind="scodifica.dettaglio.tipoApprovazione"></td>
                    <td ng-bind="scodifica.dettaglio.confidenza | currency : '%' : 2"></td>
                    <td ng-bind="scodifica.dettaglio.utenteCodifica"></td>
                    <td colspan="4">&nbsp;</td>
                  </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
