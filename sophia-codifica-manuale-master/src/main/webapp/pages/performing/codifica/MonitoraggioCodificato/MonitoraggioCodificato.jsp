<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="../../../navbar.jsp" %>
<input type="hidden" id="user" value="<%=(String) session.getAttribute(" sso.user.userName")%>" />
<div class="bodyContents">
    <div class="mainContainer row-fluid">
        <div class="contents-topscroll noprint">
            <div class="topscroll-div" style="width: 95%;margin-bottom: 20px">&nbsp;</div>
        </div>
        <div class="sticky">
            <div id="companyLogo" class="navbar commonActionsContainer noprint">
                <div class="actionsContainer row-fluid">
                    <div class="span2">
            <span class="companyLogo">
              <img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
                    </div>
                    <span class="btn-toolbar span4">
            <div class="pageNumbers alignTop pull-right" style="text-align: center;">
              <span class="pageNumbersText"><strong>Monitoraggio Codificato</strong></span>
            </div>
          </span>
                </div>
            </div>
        </div>
        <div class="contentsDiv marginLeftZero" id="rightPanel">
            <div class="listViewPageDiv">
                <div class="listViewActionsDiv row-fluid">
                    <table class="table table-bordered blockContainer showInlineTable equalSplit">
                        <thead>
                        <tr>
                            <th class="blockHeader" colspan=6 ng-show="vm.hideForm"><span ng-click="vm.hideForm=false" class="glyphicon glyphicon-expand"></span>&nbsp;Parametri Ricerca </th>
                            <th class="blockHeader" colspan=6 ng-hide="vm.hideForm"><span ng-click="vm.hideForm=true" class="glyphicon glyphicon-collapse-down"></span>&nbsp;Parametri Ricerca </th>
                        </tr>
                        </thead>
                        <tbody ng-hide="vm.hideForm">
                        <tr>
                            <td class="fieldLabel medium">
                                <label class="muted pull-right marginRight10px"> Periodo di Ripartizione </label>
                            </td>
                            <td>
                                <select ng-options="periodo.idPeriodoRipartizione as periodo.codice + ' ' + '(' + (periodo.competenzeDa| date:'dd-MM-yyyy') + ' / ' + (periodo.competenzeA| date:'dd-MM-yyyy') + ')' for periodo in vm.periodoRipartizione track by periodo.idPeriodoRipartizione" ng-model="vm.model.idPeriodoRipartizione">
                                    <option value="">Non ripartito</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="fieldLabel medium" style="text-align: right" nowrap colspan="6">
                                <button id="applica" class="btn addButton" ng-click="vm.search(vm.ricerca)">
                                    <span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Applica</strong>
                                </button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="listViewPageDiv">
                <div class="listViewActionsDiv row-fluid">
                    <div class="listViewContentDiv" id="listViewContents">
                        <div class="listViewEntriesDiv contents-bottomscroll">
                            <div class="bottomscroll-div" style="width: 95%; min-height: 80px">
                                <table class="table table-hover table-bordered table-striped" style="box-sizing:content-box" ng-if="vm.monitoraggio.configurazioni.livelloSomiglianzaList">
                                    <thead>
                                    <tr>
                                        <th class="blockHeader" style="text-align: center; white-space: nowrap; width: 1%"> Livello di somiglianza </th>
                                        <th ng-repeat="valoreEconomico in vm.monitoraggio.configurazioni.valoreEconomicoList" colspan="2" style="text-align: center">
                                            {{valoreEconomico.header}}
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="livelloSomiglianza in vm.monitoraggio.configurazioni.livelloSomiglianzaList">
                                        <th class="blockHeader" style="text-align: center; white-space: nowrap;">
                                            {{livelloSomiglianza.header}}
                                        </th>
                                        <td ng-repeat-start="valoreEconomico in vm.monitoraggio.configurazioni.valoreEconomicoList" style="background: #afa">
                                            {{vm.monitoraggio.valori[livelloSomiglianza.id][valoreEconomico.id][0].secondaMaggiore}}
                                        </td>
                                        <td ng-repeat-end style="background: #faa">
                                            {{vm.monitoraggio.valori[livelloSomiglianza.id][valoreEconomico.id][0].secondaMinore}}
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="listViewPageDiv" style="margin-left: 8%;width: 50%" ng-if="vm.monitoraggio.configurazioni">
            <table style="border-collapse: separate;border-spacing: 10px;">
                <tr>
                    <th style="width: 20%;border: 1px solid black;background: #afa">&nbsp;</th>
                    <td>Distanza dalla seconda maggiore di <span ng-bind="vm.monitoraggio.configurazioni.valoreSoglia"></span> *</td>
                </tr>
                <tr>
                    <th style="border: 1px solid black;background: #faa">&nbsp;</th>
                    <td>Distanza dalla seconda minore di <span ng-bind="vm.monitoraggio.configurazioni.valoreSoglia"></span> *</td>
                </tr>
                <tfoot>
                <tr>
                    <td colspan="2">
                        <span style="font-style: italic">* dinamico in base al valore selezionato</span>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
