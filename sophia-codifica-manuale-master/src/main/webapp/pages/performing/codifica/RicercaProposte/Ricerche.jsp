<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@
	include file="../../../navbar.jsp"%>

<div class="bodyContents">

	<div class="mainContainer row-fluid">

		<div class="contents-topscroll noprint">
			<div class="topscroll-div" style="width: 95%; margin-bottom: 20px"></div>
		</div>

		<div class="sticky">
			<div id="companyLogo" class="navbar commonActionsContainer noprint">
				<div class="actionsContainer row-fluid">
					<div class="span2">
						<span class="companyLogo"> <img src="images/logo_siae.jpg"
							title="SIAE" alt="SIAE">
						</span>
					</div>
					<span style="position: absolute;left: 50%; margin-left: -100px;margin-top:10px;">
						<div class="pageNumbers alignTop pull-right" style="text-align: center;">
							<span class="pageNumbersText"><strong>Ricerca Proposte</strong></span>
						</div>
					</span>
				</div>
			</div>
		</div>
		
		<div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px; margin-top: 40px;">
			<div class="listViewPageDiv">
					
				<!-- table inserimento campi-->
				<div class="listViewActionsDiv row-fluid">
						<form name="basicSearchForm" autocomplete="off">
				<!-- Ricerca Semplice -->
					<table class="table table-bordered blockContainer showInlineTable equalSplit">
						<thead>
							<tr>
								<th class="blockHeader" colspan="6"><span
									ng-click="ctrl.hideForm=!ctrl.hideForm;resetData()"
									class="glyphicon glyphicon-{{!ctrl.hideForm ? 'collapse-down' : 'expand'}}"></span>
									Parametri Ricerca Semplice</th>
							</tr>
						</thead>
						<tbody ng-hide="ctrl.hideForm">
							<tr>
								<td style="text-align: right; font:bold; width:20%; ">Titolo :</td>
								<td style="width: 60%;">
									<input id="Titolo" placeholder="Titolo" style="width:98%;" ng-model="request.titolo">
								</td>
								<td style="text-align: right; font:bold; width:20%;"></td>
							</tr>
							<tr>
								<td style="text-align: right; font:bold; width:20%;">Nominativo :</td>
								<td>
									<input id="Nominativo" ng-model="request.compositori" style="width:98%;" placeholder="per pi&ugrave; Nominativi separarli con la virgola">
								</td>
								<td style="text-align: right; font:bold;width:20%;"></td>
							</tr>
							<tr>							
								<td class="fieldLabel medium" style="text-align: right" nowrap colspan="6">
									<label class="muted pull-left marginLeft10px">Per effettuare la ricerca valorizzare entrambi i campi</label>						
									<button	id="applicaTitoloAutore" class="btn addButton" 
											ng-click="search(request)" 
											ng-show="(request.titolo && request.compositori)">
										<span class="glyphicon glyphicon-search"></span><strong>&nbsp;&nbsp;Cerca&nbsp;&nbsp;</strong>
									</button>
									<button	id="resetData" class="btn addButton" style="width: 102px; height: 27px;"
											ng-click="resetData()" type="button"
											ng-show="(request.titolo && request.compositori)">
										<span class="fas fa-eraser"></span><strong>&nbsp;&nbsp;&nbsp;Reset&nbsp;&nbsp;</strong>
									</button>
								</td>
							</tr>
						</tbody>
					</table>
					<br>
				</form>
				<form name="advancedSearchForm" autocomplete="off">
				<!-- Ricerca Avanzata -->
					<table class="table table-bordered blockContainer showInlineTable equalSplit">
						<thead>
							<tr>
								<th class="blockHeader" colspan="6"><span
									ng-click="ctrl.hideForm=!ctrl.hideForm;resetData()" 
									class="glyphicon glyphicon-{{ctrl.hideForm ? 'collapse-down' : 'expand'}}"></span>
									Parametri Ricerca Avanzata</th>
							</tr>
						</thead>
						<tbody ng-show="ctrl.hideForm">
							<tr>
								<td style="text-align: right; font:bold; width: 10%;">Titolo :</td>
								<td style="width: 35%;">
									<input id="Titolo" ng-model="request.titolo" placeholder="Titolo" style="width: 98%;">
								</td>
								<td style="text-align: right; font:bold; width: 10%;">Titolo Alternativo :</td>
								<td style="width: 35%;">
									<input id="TitoloAlternativo" ng-model="request.titoloAlternativo" placeholder="Titolo Alternativo" style="width: 98%;">
								</td>
								<td style="text-align: right; font:bold; width: 10%;"></td>
							</tr>
							<tr>
								<td style="text-align: right; font:bold; width:10%;">Compositore :</td>
								<td>
									<input id="Compositori" ng-model="request.compositori" style="width:98%;" placeholder="per pi&ugrave; Compositori separarli con la virgola">
								</td>								
								<td style="text-align: right; font:bold; width:10%;">Autore :</td>
								<td>
									<input id="Autore" ng-model="request.autori" style="width:98%;" placeholder="per pi&ugrave; Autori separarli con la virgola">
								</td>
								<td style="text-align: right; font:bold;width:10%;"></td>
							</tr>								
							<tr>
								<td style="text-align: right; font:bold; width:10%;">Interprete :</td>
								<td>
									<input id="Interprete" ng-model="request.interpreti"  style="width:98%;" placeholder="per pi&ugrave; Interpreti separarli con la virgola">
								</td>								
								<td style="text-align: right; font:bold; width:10%;"></td>
								<td></td>
								<td style="text-align: right; font:bold;width:10%;"></td>
							</tr>												
							<tr>
								<td class="fieldLabel medium" style="text-align: right" nowrap
									colspan="6">
									<label class="muted pull-left marginLeft10px">Per effettuare la ricerca valorizzare Titolo  e Compositore o Autore o Interprete</label>
									<button class="btn addButton" 
									ng-click="search(request)"
									ng-show="(request.titolo || request.titoloAlternativo) &&
											  (request.compositori || request.autori || request.interpreti)">
								<span class="glyphicon glyphicon-search"></span><strong>&nbsp;&nbsp;Cerca</strong>
									</button>
									<button	id="resetData" class="btn addButton" style="width: 102px; height: 27px;"
											ng-click="resetData()" type="button"
											ng-show="(request.titolo && request.compositori)">
										<span class="fas fa-eraser"></span><strong>&nbsp;&nbsp;&nbsp;Reset&nbsp;&nbsp;</strong>
									</button>

								</td>

							</tr>
						</tbody>
					</table>
					</form>
				</div>

				<div>
				</br>
				</br>
					<!-- table risultati -->
					<div class="listViewContentDiv" id="listViewContents">

						<div class="contents-topscroll noprint">
							<div class="topscroll-div" style="width: 95%"></div>
						</div>

						<div class="alert alert-warning" ng-show="warningMessage">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
							<span>{{warningMessage}}</span>
						</div>
								<div class="alert alert-danger" ng-show="errorMessage">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
									<span><strong>Attenzione: </strong>{{errorMessage}}</span>
								</div>
									<div class="listViewEntriesDiv contents-bottomscroll" ng-show="results && (results.length!=0)">
									
									<div class="bottomscroll-div" style="width: 95%; min-height: 80px">
										<table class="table table-bordered listViewEntriesTable tableFormatted" id="nohover">
											<thead>
												<tr class="listViewHeaders"> 
													<th>Titolo opera</th>
													<th>Titolo alternativo</th>
													<th>Compositori</th>
													<th>Autori</th>
													<th>Interpreti</th>
													<th>Codice Opera</th>
													<th>Grado di confidenza</th>
												</tr>
											</thead>
											<tbody>
													<tr class="listViewEntries" style="background-color: #d0d0e0; font-size: 16px;">
															<td style="border-top: 1px solid black;">{{headers.titolo}}</td>
															<td style="border-top: 1px solid black;">{{headers.titoloAlternativo}}</td>
															<td style="border-top: 1px solid black;">{{headers.compositori}}</td>
															<td style="border-top: 1px solid black;">{{headers.autori}}</td>
															<td style="border-top: 1px solid black;">{{headers.interpreti}}</td>
															<td style="border-top: 1px solid black;"></td>
															<td style="border-top: 1px solid black;"></td>
														</tr>
														<tr class="danger" ng-show="results">
															<th style="border-top: 1px solid rgba(0, 0, 0, 0.1) !important;">Titolo opera&nbsp;</th>
															<th style="border-top: 1px solid rgba(0, 0, 0, 0.1) !important;">Titolo alternativo&nbsp;</th>
															<th style="border-top: 1px solid rgba(0, 0, 0, 0.1) !important;">Compositori&nbsp;</th>
															<th style="border-top: 1px solid rgba(0, 0, 0, 0.1) !important;">Autori&nbsp;</th>
															<th style="border-top: 1px solid rgba(0, 0, 0, 0.1) !important;">Interpreti&nbsp;</th>
															<th style="border-top: 1px solid rgba(0, 0, 0, 0.1) !important;">Codice Opera&nbsp;</th>
															<th style="border-top: 1px solid rgba(0, 0, 0, 0.1) !important;">Grado di confidenza&nbsp;</th>
													</tr>
												<tr 
													id="tabellaCodifica" style="background-color:rgba(255, 255, 255)" 
													ng-repeat="opera in results" ng-show="results"
												>													
													<td>{{opera.titoloOriginale}}</td>
													<td>{{opera.titoloAlternativo}}</td>
													<td>{{opera.compositore}}</td>
													<td>{{opera.autore}}</td>
													<td>{{getInterpreti(opera.interprete)}}
														<span ng-show="(opera.interprete.split(',')).length > 12">...<span class="glyphicon glyphicon-info-sign" style="color: rgba(0,180,249,1)" data-toggle="tooltip-opera" 
															title="{{getListaInterpreti(opera.interprete)}}"
															data-html="true" rel="tooltip"></span>
															<script type="text/javascript">
																$('[data-toggle="tooltip-opera"]').tooltip();
															</script>
														</span>
													</td>
													<td>{{opera.codiceOpera}}</td>
													<td>{{opera.gradoDiConfidenza | currency : "%" : 2}}
													</td>
												</tr>
											</tbody>
										</table>
									</div>
			  					</div>
							</div>
						</div>
				   </div>
			  </div>
		</div>
</div>