
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@
	include file="../../../navbar.jsp"%>
	<input type="hidden" id="user" value="<%=(String) session.getAttribute("sso.user.userName")%>">


<div class="bodyContents">

	<div class="mainContainer row-fluid">

		<div class="contents-topscroll noprint">
			<div class="topscroll-div" style="width: 95%; margin-bottom: 20px">&nbsp;</div>
		</div>

		<div class="sticky">
		     <div id="companyLogo" class="navbar commonActionsContainer noprint">
		          <div class="actionsContainer row-fluid">
		             <div class="span2">
		                 <span class="companyLogo">
							<img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;
				     	 </span>
					 </div>
					 <div style="text-align: center; whidth:100%;">
						<span style="position: absolute;left: 50%; margin-left: -160px;margin-top:10px;">
							<div class="pageNumbers alignTop pull-right" style="text-align: center;">
								<span class="pageNumbersText">
									<strong class="ng-binding">Upload File CSV &amp; Risultati di Codifica Massiva</strong>
								</span>
							</div>
						</span>
				     </div>                
		           </div>
		      </div>
		</div>
		
		
		<div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px;">
			<div class="listViewPageDiv">
				<!-- table inserimento campi-->
				
								<div class="listViewActionsDiv row-fluid">
					<table
						class="table table-bordered blockContainer showInlineTable equalSplit">
						<tbody ng-hide="ctrl.hideForm">
							<tr>
								<td Style="text-align: right; width:25%;">&nbsp;&nbsp;&nbsp;</td>
								<td class="medium" style="width: 50%; font-family: Arial,sans-serif">
									<div class="row-fluid" ngf-drop ng-model="cs.file" ngf-max-size="20MB" ngf-allow-dir="false">
										<label style="margin-top: 3px;">Drag &amp; Drop File</label>
											<br>
											<div class="row-fluid" style="margin: 3px">
												<strong>{{cs.file.name}}</strong>
											</div>
											<label style="margin-top: 20px;">Oppure carica un File in formato .CSV:</label>
											<button class="btn addButton"  type="file" ngf-select name="file" accept=".csv,.txt"
												ng-model="cs.file" ngf-max-size="30MB" ngf-allow-dir="false" style="height: 30px;" ng-click="clearMessages()">
												<span class="glyphicon glyphicon-folder-open">&nbsp;</span>
												<strong style="font-family: Arial,sans-serif;font-size:16px;">Scegli</strong>
											</button>
											<div ngf-no-file-drop>File Drag/Drop non &egrave; supportato da questo browser</div>
									</div>
								</td>
								<td Style="text-align: right; width:25%;">&nbsp;&nbsp;&nbsp;</td>
							</tr>
							<tr>
								<td class="fieldLabel medium" style="text-align: right" nowrap colspan="6">
									<label class="muted pull-left marginLeft10px">Selezionare un File</label>
									<button	class="btn addButton" ng-click="uploadFile(cs)"
											ng-show="cs.file !=undefined && cs.file != ''">
										<span class="glyphicon glyphicon-upload"></span>&nbsp;&nbsp;<strong>Upload</strong>
									</button>
								</td>

							</tr>
						</tbody>
						<div class="alert alert-danger" ng-show="messages.error && (!messages.info)">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
							<span><strong>Errore: </strong>{{messages.error}}</span>
						</div>
						<div class="alert alert-success" ng-show="messages.info && (!messages.error)">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
							<span>{{messages.info}}</span>
						</div>
					</table>
					</div>

				<div>
					<!-- actions and pagination -->
					<div class="listViewTopMenuDiv noprint">
						<div class="listViewActionsDiv row-fluid">
							<span class="btn-toolbar span4"> 
								<span class="btn-group">
								</span>
							</span>
							<span class="btn-toolbar span4"> 
								<span class="customFilterMainSpan btn-group" style="text-align: center;">
										<div class="pageNumbers alignTop ">
											<span class="pageNumbersText">
												<strong>Console dei Risultati</strong>
											</span>
										</div>
								</span>
							</span>
								<span class="span4 btn-toolbar">
								<div class="listViewActions pull-right">
								<div class="pageNumbers alignTop ">
										<span ng-show="paging.totRecords > 50"> <span
											class="pageNumbersText" style="padding-right: 5px">
												Record da <strong>{{paging.firstRecord}}</strong> a <strong>{{paging.lastRecord}}</strong> su <strong>{{paging.totRecords}}</strong>
										</span>
											<button title="Precedente" class="btn"
												id="listViewPreviousPageButton" type="button"
												ng-click="navigateToPreviousPage()" ng-show="paging.currentPage > 1">
												<span class="glyphicon glyphicon-chevron-left"></span>
											</button>
											<button title="Successiva" class="btn"
												id="listViewNextPageButton" type="button"
												ng-click="navigateToNextPage()"
												ng-show="paging.currentPage < paging.totPages">
												<span class="glyphicon glyphicon-chevron-right"></span>
											</button>
											<button title="Fine" class="btn" id="listViewNextPageButton"
												type="button" ng-click="navigateToEndPage()"
												ng-show="paging.currentPage < paging.totPages">
												<span class="glyphicon glyphicon-step-forward"></span>
											</button>
										</span>
									</div>
								</div>
								<div class="clearfix"></div>
							</span>
							<!--  -->
						</div>
					</div>
					<!-- table risultati -->
					<div class="listViewContentDiv" id="listViewContents">
					

						<div class="contents-topscroll noprint">
							<div class="topscroll-div" style="width: 95%">&nbsp;</div>
						</div>
						
						<div class="listViewEntriesDiv contents-bottomscroll" >
							<div class="bottomscroll-div" style="width: 95%; min-height: 80px;">
								<table class="table table-bordered listViewEntriesTable tableFormatted">
									<thead>
										<tr class="listViewHeaders"> 
										<!-- PRIMA DI CAMBIARE LA CLASSE DELLO SPAN, CONTROLLARE LA LOGICA SUL CONTROLLER DI ANGULAR. -->
											<th style="width:20%; text-align: center;">Id Configurazione Utilizzata</th>
											<th style="width:20%; text-align: center;">Data Elaborazione</th>
											<th style="width:20%; text-align: center;">Stato</th>
											<th style="width:20%; text-align: center;">Download File Input</th>
											<th style="width:20%; text-align: center;">Download File Output</th>
										</tr>
									</thead>
									<tbody>
										<tr class="listViewEntries" ng-repeat="file in uploadedFileList">
											<td style="width:20%; text-align: center; font-size:16px;">{{getIdConfigurazione(file.receivedMessage)}}</td>
											<td style="width:20%; text-align: center; font-size:16px;">{{file.endWorkTime | date: 'dd/MM/yyyy &nbsp;&nbsp;   HH:mm'}}</td>
											<td style="width:20%; text-align: center; font-size:16px;">{{file.state}}
												<span ng-show="file.state=='Fallita'"><span class="glyphicon glyphicon-info-sign" style="color: red" data-toggle="tooltip" 
													title="<p>Errore nell'elaborazione!</p><p>{{file.errorMessage}}</p>"
													data-html="true" rel="tooltip"></span>
													<script type="text/javascript">
														$('[data-toggle="tooltip"]').tooltip();
													</script>
												</span>												
											</td>
											<td style="width:20%; text-align: center;">
												<button class="btn addButton" ng-click="downloadFile(file.fileInput, file.fileName)">
												<span class="glyphicon glyphicon glyphicon-download"></span></button></td>
													<td style="width:20%; text-align: center;"><button ng-show="file.state=='Terminata'" class="btn addButton" ng-click="downloadFile(file.fileOutput, file.fileName)">
												<span class="glyphicon glyphicon glyphicon-download"></span>
											</button></td>
										</tr>
									</tbody>
								</table>
							</div>
			  			</div>
						
					</div>

					<!-- table risultati -->
			</div>
		</div>
	</div>
</div>
