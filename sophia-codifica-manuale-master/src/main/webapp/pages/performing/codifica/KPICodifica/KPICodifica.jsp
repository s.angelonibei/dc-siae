<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%><%@
	include file="../../../navbar.jsp"%>
	<input type="hidden" id="user" value="<%=(String) session.getAttribute("sso.user.userName")%>">
	
	<div class="bodyContents" >  <!--  ng-init="init({})" -->
		<div class="mainContainer row-fluid">
		
			<div class="contents-topscroll noprint">
				<div class="topscroll-div" style="width: 95%;margin-bottom: 20px">&nbsp;</div>
			</div>
			
			<div class="sticky">
				<div id="companyLogo" class="navbar commonActionsContainer noprint">
					<div class="actionsContainer row-fluid">
						<div class="span2">
							<span class="companyLogo">
								<img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;
							</span>
						</div>
						<span class="btn-toolbar span4">
							<div class="pageNumbers alignTop pull-right" style="text-align: center;">
								<span class="pageNumbersText"><strong>KPI Codifica</strong></span>
							</div>
						</span>
					</div>
				</div>
			</div>
			
			<div class="contentsDiv marginLeftZero" id="rightPanel"	style="min-height: 80px;">
			<div class="listViewPageDiv">
				<div class="listViewActionsDiv row-fluid">
					<table class="table table-bordered blockContainer showInlineTable equalSplit">
						<thead>
							<tr>
								<th class="blockHeader" colspan=6 ng-show="vm.hideForm"><span
									ng-click="vm.hideForm=false"
									class="glyphicon glyphicon-expand"></span>&nbsp;Parametri
									Ricerca</th>
								<th class="blockHeader" colspan=6 ng-hide="vm.hideForm"><span
									ng-click="vm.hideForm=true"
									class="glyphicon glyphicon-collapse-down"></span>&nbsp;Parametri
									Ricerca</th>
							</tr>
						</thead>
						<tbody ng-hide="vm.hideForm">
							<tr>
								<td class="fieldLabel medium">
									<label class="muted pull-right marginRight10px">
										Periodo di Ripartizione
									</label>
								</td>
								<td>
									<select ng-model="vm.ricerca.periodoRipartizione">
										<option value="">Non ripartito</option>
										<option value="{{periodo.idPeriodoRipartizione}}" ng-repeat="periodo in vm.periodoRipartizione">{{periodo.codice + ' ' + '(' + (periodo.competenzeDa| date: 'dd/MM/yyyy') + ' / ' + (periodo.competenzeA| date: 'dd/MM/yyyy') + ')' }}</option>
									</select>
								</td>
								<td class="fieldLabel medium">
									<label class="muted pull-right marginRight10px">
										Voce di Incasso
									</label>
								</td>
								<td>
									<select ng-model="vm.ricerca.voceIncasso">
										<option value="">Seleziona voce</option>
										<option value="{{voce.codVoceIncasso}}" ng-repeat="voce in vm.vociIncasso">{{voce.codVoceIncasso}}</option>
									</select>
								</td>
								<%--<td class="fieldLabel medium">--%>
									<%--<label class="muted pull-right marginRight10px">--%>
										<%--Tipologia Report--%>
									<%--</label>--%>
								<%--</td>--%>
	                          	<%--<td>--%>
									<%--<select ng-model="vm.ricerca.tipologiaReport">--%>
										<%--<option value="">Seleziona supporto</option>--%>
										<%--<option value="CARTACEO">Cartaceo</option>--%>
										<%--<option value="DIGITALE">Digitale</option>--%>
									<%--</select>--%>
								<%--</td>--%>
							</tr>
							<tr>
								<td class="fieldLabel medium" style="text-align: right" nowrap colspan="6">
									<button	id="applica" class="btn addButton" ng-click="vm.search(vm.ricerca)">
										<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Applica</strong>
									</button>
								</td>
							</tr>
			                 </tbody>
					</table>
				</div>
			</div>
			
			<!-- vm.periodoRipartizione   report in vm.storico.periodo -->
			<div class="listViewContentDiv" id="listViewContents" ng-repeat = "report in vm.storico.periodo">
				<div class="contents-topscroll noprint">
					<div class="topscroll-div" style="width: 95%">&nbsp;</div>
				</div>
				<div class="listViewEntriesDiv contents-bottomscroll">
					<div class="bottomscroll-div" style="width: 95%; min-height: 80px">
						<table class="table table-bordered listViewEntriesTable tableFormatted">
							<thead>
								<tr ng-show="!showConfigurazioni" ng-click="showConfigurazioni = !showConfigurazioni">
									<th style="border-bottom: 1px solid #dddddd !important;" colspan=9>
										{{vm.getValue(report)}} <!--  ({{vm.parseDate(report.competenzeDa) + '/' + vm.parseDate(report.competenzeA)}}) -->
										<span class="glyphicon glyphicon-triangle-bottom pull-right"></span>	
									</th>
								</tr>
								<tr ng-show="showConfigurazioni" ng-click="showConfigurazioni = !showConfigurazioni">
									<th style="border-bottom: 1px solid #dddddd !important;" colspan=10>
										{{vm.getValue(report)}} <!--({{vm.parseDate(report.competenzeDa) + '/' + vm.parseDate(report.competenzeA)}})-->
										<span class="glyphicon glyphicon-triangle-top pull-right"></span>	
									</th>
								</tr>
								<tr class="listViewHeaders" ng-show="!showConfigurazioni">
									<th>Voce incasso</th>
									<th>Numero combana</th>
									<th>Numero Utilizzazioni</th>
									<th>% Codificato automatico</th>
									<th>% Codificato manuale</th>
									<th>% Non Codificato ( combane da approvare )</th>
									<th>% Non Codificato ( combana scartate )</th>
									<th>Valore economico codificato</th>
									<th>Valore economico non codificato</th>
								</tr>
							</thead>
							<tbody ng-show="!showConfigurazioni">
								<tr class="listViewEntries" ng-repeat="configurazione in vm.storico.records">
									<td>{{configurazione.voceIncasso}}</td>
									<td>{{configurazione.numeroCombana}}</td>
									<td>{{configurazione.numeroUtilizzazioni}}</td>
									<td>{{configurazione.percentCodificatoAutomatico | currency: '%' : 2}}</td>
									<td>{{configurazione.percentCodificatoManuale | currency: '%' : 2}}</td>
									<td>{{configurazione.percentNonCodificatoCombDaApprovare | currency: '%' : 2}}</td>
									<td>{{configurazione.percentNonCodificatoCombDaAScartate | currency: '%' : 2}}</td>
									<td>{{vm.checkValue(configurazione.valoreEconomicoCodificato)}}</td>
									<td>	{{vm.checkValue(configurazione.valoreEconomicoNonCodificato)}}</td>
									
								</tr>
							</tbody>
						</table>
					</div>
 				</div>
 			</div>							
			
		</div>
	</div>