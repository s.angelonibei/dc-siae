(function () {
    'use strict';
    angular.module('codmanApp')
        .controller('ModificaCodiceOperaController', ModificaCodiceOperaController);

    ModificaCodiceOperaController.$inject = ['ngDialog', 'UtilService', '$scope', 'PerformingCodificaManualeEspertoService', 'scodificaService'];

    function ModificaCodiceOperaController(ngDialog, UtilService, $scope, PerformingCodificaManualeEspertoService, scodificaService) {
        var vm = this;
        vm.user = $scope.ngDialogData.user;
        vm.combana = $scope.ngDialogData.combana;
        vm.sessione = $scope.ngDialogData.sessione;

        vm.cancel = cancel;
        vm.search = search;
        vm.combine = combine;
        vm.elimina = elimina;

        function cancel() {
            ngDialog.closeAll(false);
        }

        function search(codiceOpera) {
            if (!new RegExp('^[0-9]{11}$').exec(codiceOpera)) {
                UtilService.showError("Attenzione", "Il codice opera deve essere composte da 11 numeri");
                return
            }

            PerformingCodificaManualeEspertoService.searchOpera(codiceOpera).then(function (response) {
                vm.result = response.data.content[codiceOpera];
                if (!vm.result) {
                    UtilService.showError("Errore", "Non è stata trovata nessuna opera per il codice opera inserito");

                }
            }, function (response) {
                UtilService.showError("Errore", "Non è stato possibile ottenere informazioni riguardo al codice opera inserito")
            })
        }

        function combine(result) {
            return UtilService.showConfirm("Attenzione", "Sei sicuro di voler modificare il codice opera per la combinazione indicata?")
                .then(function (model) {
                    var object = [];
                    vm.combana.statApprov = "M";
                    vm.combana.codApprov = result.codiceOpera;
                    object.push(vm.combana);
                    PerformingCodificaManualeEspertoService.sceltaProposte(object, vm.user).then(function (response) {
                        UtilService.showError("Successo", "Abbinazione avvenuta con successo");
                        ngDialog.closeAll(true);
                    }, function (response) {
                        UtilService.showError("Errore", "Non è stato possibile abbinare il codice opera")
                        ngDialog.closeAll(false);
                    })
                })
        }

        function elimina(result) {
            return UtilService.showConfirm("Attenzione", "Sei sicuro di voler scodificare la combinazione anagrafica identificata?").then(function (model) {
                return scodificaService.eliminaCodiceOpera(result).then(function(data){
                    ngDialog.closeAll(true);
                }, function(data) {
                    ngDialog.closeAll(false);
                })
            })
        }
    }
})();