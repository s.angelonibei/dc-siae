<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%><%@
	include file="../../../navbar.jsp"%>
	<input type="hidden" id="user" value="<%=(String) session.getAttribute("sso.user.userName")%>">

	<div class="bodyContents">
		<div class="mainContainer row-fluid">

			<div class="contents-topscroll noprint">
				<div class="topscroll-div" style="width: 95%;margin-bottom: 20px">&nbsp;</div>
			</div>

			<div class="sticky">
				<div id="companyLogo" class="navbar commonActionsContainer noprint">
					<div class="actionsContainer row-fluid">
						<div class="span2">
							<span class="companyLogo">
								<img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;
							</span>
						</div>
						<span class="btn-toolbar span4">
							<div class="pageNumbers alignTop pull-right" style="text-align: center;">
								<span class="pageNumbersText"><strong>Codifica Manuale Esperto</strong></span>
							</div>
						</span>
						<div ng-show="data != undefined">
							<label class="pageNumbers pull-right" style="color:red; margin-top:1%; margin-right:2%">
								<!-- <strong style="font-size: 20px;">{{minutes < 10 ? '0' : ''}}{{minutes}} : {{seconds < 10 ? '0' : ''}}{{seconds}}</strong> -->
							</label>
							<label ng-show="warningTimer" class="pageNumbers pull-right" style="color:red; margin-top:1%; margin-right:2%" ng-click="resetTimer()">
								<strong style="font-size: 15px;">Attenzione, manca solo 1 minuto. Clicca qui per resettare il timer</strong>
							</label>
						</div>
					</div>
				</div>
			</div>


			<div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px;">
				<div class="listViewPageDiv">
					<div class="listViewActionsDiv row-fluid">
						<table style="table-layout: fixed;" class="table table-bordered blockContainer showInlineTable equalSplit">
							<thead>
								<tr>
									<th class="blockHeader" ng-show="ctrl.assegnazioneAutomatica" colspan="1">
										<span ng-click="ctrl.assegnazioneAutomatica=false" class="glyphicon glyphicon-expand"></span>
										&nbsp;Assegnazioni Automatiche
									</th>
									<th class="blockHeader" ng-hide="ctrl.assegnazioneAutomatica" colspan="1">
										<span ng-click="ctrl.assegnazioneAutomatica=true" class="glyphicon glyphicon-collapse-down"></span>
										&nbsp;Assegnazioni Automatiche
									</th>
								</tr>
							</thead>
							<tbody ng-hide="ctrl.assegnazioneAutomatica">
								<tr>
									<td class="fieldLabel medium" style="text-align: right" nowrap colspan="1">
										<button	id="applicaVoce" class="btn addButton" ng-click="filterApply({})">
											<span class="glyphicon glyphicon-search"></span>&nbsp;&nbsp;<strong>Ricerca Automatica</strong>
										</button>
									</td>
								</tr>
							</tbody>
						</table>

						<br>

						<table style="table-layout: fixed;" class="table table-bordered blockContainer showInlineTable equalSplit">
							<thead>
								<tr>
									<th class="blockHeader" ng-show="ctrl.ricercaVoce" colspan="4">
										<span ng-click="ctrl.ricercaVoce=false" class="glyphicon glyphicon-expand"></span>
										&nbsp;Ricerca per Voce di Incasso
									</th>
									<th class="blockHeader" ng-hide="ctrl.ricercaVoce" colspan="4">
										<span ng-click="ctrl.ricercaVoce=true" class="glyphicon glyphicon-collapse-down"></span>
										&nbsp;Ricerca per Voce di Incasso
									</th>
								</tr>
							</thead>
							<tbody ng-hide="ctrl.ricercaVoce">
								<tr>
									<td class="fieldLabel medium" colspan=1>
										<label class="muted pull-right marginRight10px" style="margin-top: 1%;">Voce incasso</label>
									</td>
									<td colspan=1>
										<input id="voce" placeholder="Voce" ng-model="paramsVoce.voce">
									</td>
									<td colspan=2></td>
								</tr>
								<tr>
									<td class="fieldLabel medium" style="text-align: right" nowrap colspan="4">
										<label class="muted pull-left marginLeft10px">Per effettuare la ricerca inserire il campo</label>
										<button	id="applicaVoce" class="btn addButton" ng-click="filterApply({voce: paramsVoce.voce})"
										ng-show="(paramsVoce.voce !=undefined && paramsVoce.voce != '')">
											<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Applica</strong>
										</button>
									</td>
								</tr>
							</tbody>
						</table>

						<br>

						<table style="table-layout: fixed;" class="table table-bordered blockContainer showInlineTable equalSplit">
							<thead>
								<tr>
									<th class="blockHeader" ng-show="ctrl.ricercaTitoloAutore" colspan="4">
										<span ng-click="ctrl.ricercaTitoloAutore=false" class="glyphicon glyphicon-expand"></span>
										&nbsp;Ricerca per Titolo e Artisti
									</th>
									<th class="blockHeader" ng-hide="ctrl.ricercaTitoloAutore" colspan="4">
										<span ng-click="ctrl.ricercaTitoloAutore=true" class="glyphicon glyphicon-collapse-down"></span>
										&nbsp;Ricerca per Titolo e Artisti
									</th>
								</tr>
							</thead>
							<tbody ng-hide="ctrl.ricercaTitoloAutore">
								<tr>
									<td class="fieldLabel medium">
										<label class="muted pull-right marginRight10px" style="margin-top: 2%;">Titolo</label>
									</td>
									<td>
										<input id="titolo" placeholder="Titolo" ng-model="paramsTitoloAutore.titolo">
									</td>
									<td class="fieldLabel medium">
										<label class="muted pull-right marginRight10px" style="margin-top: 2%;">Nominativo</label>
									</td>
									<td>
										<input id="nominativo" placeholder="Nominativo" ng-model="paramsTitoloAutore.nominativo">
									</td>
								</tr>
								<tr>
									<td class="fieldLabel medium" style="text-align: right" nowrap colspan="4">
										<label class="muted pull-left marginLeft10px">Per effettuare la ricerca inserire almeno un campo</label>
										<button	id="applicaTitoloAutore" class="btn addButton" ng-click="filterApply({titolo: paramsTitoloAutore.titolo, nominativo: paramsTitoloAutore.nominativo})"
										ng-show="(paramsTitoloAutore.titolo !=undefined && paramsTitoloAutore.titolo != '') ||
												 (paramsTitoloAutore.nominativo !=undefined && paramsTitoloAutore.nominativo != '')">
											<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Applica</strong>
										</button>
									</td>
								</tr>
							</tbody>
						</table>
						<br>
						<div>
							<div class="listViewContentDiv" id="listViewContents">
								<div class="contents-topscroll noprint">
									<div class="topscroll-div" style="width: 95%">&nbsp;</div>
								</div>
								<div class="listViewEntriesDiv contents-bottomscroll" ng-show="data != undefined">
									<div class="bottomscroll-div" style="width: 95%; min-height: 80px">
										<table class="table table-bordered listViewEntriesTable tableFormatted" id="nohover">
											<thead>
												<tr class="listViewHeaders">
													<th>Titolo Utilizzazione</th>
													<th>Compositori</th>
													<th>Autori</th>
													<th>Interpreti</th>
													<th>N. Utilizzazioni</th>
													<th>Valore Economico</th>
													<th>Codice Opera Selezionato</th>
													<th ng-show="extraFieldsToShow.includes('statoOpera')"></th>
													<th></th>
													<th>Codifica Manuale</th>
													<th ng-show="extraFieldsToShow.includes('rischio')">Rischiosità</th>
												</tr>
											</thead>
											<tbody ng-repeat="combana in data | orderBy:'-numeroUtilizzazioni'">
												<tr class="listViewEntries" style="background-color: #d0d0e0; font-size: 16px;" ng-click="showProposte = !showProposte">
													<td style="border-top: 1px solid black;">{{combana.titolo}}</td>
													<td style="border-top: 1px solid black;">{{combana.compositori}}</td>
													<td style="border-top: 1px solid black;">{{combana.autori}}</td>
													<td style="border-top: 1px solid black;">{{combana.interpreti}}</td>
													<td style="border-top: 1px solid black;">{{combana.nUtilizzazioni}}</td>
													<td style="border-top: 1px solid black;">{{combana.valoreEconomico | currency : "€" : 2}}</td>
													<td style="border-top: 1px solid black;">
														<span style="margin-left:1%;margin-right:25%">{{combana.codApprov}}</span><br>
														<button ng-show="combana.codApprov != undefined" ng-click="deleteElements(data[$index]); $event.stopPropagation(); showProposte=true" type="button" class="btn addButton">
															<span class="glyphicon glyphicon-trash"></span>
														</button>
													</td>
													<td style="border-top: 1px solid black;"
													ng-show="extraFieldsToShow.includes('statoOpera')">

														</td>
													<td style="border-top: 1px solid black;"></td>
													<td style="border-top: 1px solid black;" ng-click="$event.stopPropagation()"><button class="btn addButton muted" ng-click="openModelCodifica(combana)" >Codifica Manuale</button></td>
													<td style="border-top: 1px solid black;"
													ng-show="extraFieldsToShow.includes('rischio')"
													>

													</td>
												</tr>
												<tr class="danger" ng-show="showProposte">
													<th style="border-top: 1px solid rgba(0, 0, 0, 0.1) !important;">Titolo Opera</th>
													<th style="border-top: 1px solid rgba(0, 0, 0, 0.1) !important;">Compositori</th>
													<th style="border-top: 1px solid rgba(0, 0, 0, 0.1) !important;">Autori</th>
													<th style="border-top: 1px solid rgba(0, 0, 0, 0.1) !important;">Interpreti</th>
													<th style="border-top: 1px solid rgba(0, 0, 0, 0.1) !important;"></th>
													<th style="border-top: 1px solid rgba(0, 0, 0, 0.1) !important;"><span 
														ng-show="extraFieldsToShow.includes('maturato')">Maturato</span></th>
													<th style="border-top: 1px solid rgba(0, 0, 0, 0.1) !important;">Codice Opera</th>
													<th style="border-top: 1px solid rgba(0, 0, 0, 0.1) !important; text-align: center;"
													ng-show="extraFieldsToShow.includes('statoOpera')"
													>Stato Opera</th>
													<th style="border-top: 1px solid rgba(0, 0, 0, 0.1) !important;"
													></th>
													<th style="border-top: 1px solid rgba(0, 0, 0, 0.1) !important;">Grado di confidenza</th>
													<th style="border-top: 1px solid rgba(0, 0, 0, 0.1) !important; text-align: center;"
													ng-show="extraFieldsToShow.includes('rischio')"
													>Rischiosità</th>
												</tr>
												<tr id="tabellaCodifica" style="background-color:rgba(255, 255, 255)" ng-repeat="proposta in combana.candidate | limitTo: numeroCandidate : 0" ng-show="showProposte"
													ng-click="data[$parent.$index].codApprov = proposta.codiceOpera;
													$index ? data[$parent.$index].statApprov = 'A' : data[$parent.$index].statApprov = 'P';
													$parent.showProposte = !$parent.showProposte">
													<td>{{proposta.titoloOriginale}}<br>{{proposta.confTitolo | currency : "%" : 2}}
														<div ng-show="proposta.titolo != proposta.titoloOriginale" style="color:blue">Titolo Alternativo: {{proposta.titolo}}
														</div>
													</td>
													<td>{{proposta.compositore}}<br>{{proposta.confCompositore | currency : "%" : 2}}</td>
													<td>{{proposta.autore}}<br>{{proposta.confAutore | currency : "%" : 2}}</td>
													<td>{{proposta.interprete}}<br>{{proposta.confInterprete | currency : "%" : 2}}</td>
													<td></td>
													<td><span ng-show="extraFieldsToShow.includes('maturato')" ng-bind-html="getMaturatoHtml(proposta.maturato)">
														</span>
													</td>
													<td><span style="cursor: pointer;"
															ng-click="goToDocumentazione($event, proposta.codiceOpera)"><u>{{proposta.codiceOpera}}</u></span>												
													</td>
													<td ng-show="extraFieldsToShow.includes('statoOpera')" style="text-align: center;" >
															<span ng-show="true" ng-bind-html="getStatoOperaHtml(proposta)"> 
																</span>
													</td>
													<td style="text-align: center">
														<!-- da aggiungere ng-show -->
														<span ng-show="proposta.flagDoppioDeposito=='1'">
															<i class="fas fa-exclamation-circle" data-toggle="tooltip-doppio-deposito" 
															title="Probabile doppio deposito!"
															data-html="true" rel="tooltip"></i>
															<script type="text/javascript">
																$('[data-toggle="tooltip-doppio-deposito"]').tooltip();
															</script>
														</span>															
													</td>
													<td>
														{{proposta.gradoDiConf | currency : "%" : 2}}
														<span ng-class="proposta.codiceOpera == combana.codApprov ? 'glyphicon glyphicon-ok pull-right' : ''"></span>
													</td>
													<td
													ng-show="extraFieldsToShow.includes('rischio')"
													ng-bind-html="getRischioHtml(proposta.rischio)" style="text-align: center;">
												</td>
												</tr>
											</tbody>
										</table>
									</div>
									<button ng-click="sceltaProposte(data)" type="button" class="btn addButton pull-right" style="margin-top: 1%;">
										<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Conferma</strong>
									</button>
			  					</div>
		  					</div>
	  					</div>
					</div>
		  		</div>
	  		</div>
		</div>
	</div>