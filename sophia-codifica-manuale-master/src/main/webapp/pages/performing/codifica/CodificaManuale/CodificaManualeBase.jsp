<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%@
        include file="../../../navbar.jsp"%>
<input type="hidden" id="user" value="<%=(String) session.getAttribute("sso.user.userName")%>" >

<div class="bodyContents" keypress-events>
    <div class="mainContainer row-fluid" ng-init="init()">
        <div class="contents-topscroll noprint">
            <div class="topscroll-div" style="width: 95%;margin-bottom: 20px">&nbsp;</div>
        </div>

        <div class="sticky">
            <div id="companyLogo" class="navbar commonActionsContainer noprint">
                <div class="actionsContainer row-fluid">
                    <div class="span2">
							<span class="companyLogo">
								<img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;
							</span>
                    </div>
                    <span class="btn-toolbar span4">
							<div class="pageNumbers alignTop pull-right" style="text-align: center;">
								<span class="pageNumbersText"><strong>Codifica Manuale Base</strong></span>
							</div>
						</span>
                    <div>
                        <label class="pageNumbers pull-right" style="color:red; margin-top:1%; margin-right:2%">
                            <!-- <strong style="font-size: 20px;">{{minutes < 10 ? '0' : ''}}{{minutes}} : {{seconds < 10 ? '0' : ''}}{{seconds}}</strong> -->
                        </label>
                        <label ng-show="warningTimer"
                               class="pageNumbers pull-right"
                               style="color:red; margin-top:1%; margin-right:2%" ng-click="resetTimer()">
                            <strong style="font-size: 15px;">Attenzione, manca solo 1 minuto. Clicca qui per aggiornare la sessione</strong>
                        </label>
                        <label ng-show="sessioneScaduta"
                               class="pageNumbers pull-right"
                               style="color:red; margin-top:1%; margin-right:2%" ng-click="init()">
                            <strong style="font-size: 15px;">Attenzione, la sessione è scaduta, Clicca qui per ricaricare la pagina</strong>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <button
                    ng-show="utilizzazioni.length > 0 && index>0"
                    id="back"
                    class="btn addButton"
                    style="margin-left: 2%"
                    ng-click="tornaIndietro($event)">
                <span class="glyphicon glyphicon-backward"></span>&nbsp;&nbsp; <strong>Precedente</strong>
            </button>

            <button
                    ng-show="utilizzazioni.length > 0 && index < utilizzazioni.length-1"
                    id="next"
                    ng-keyup="null"
                    class="btn addButton"
                    style="margin-left: 2%"
                    ng-click="skippa(utilizzazioni[index],$event)">
                <span class="glyphicon glyphicon-forward"></span>&nbsp;&nbsp; <strong>Successiva</strong>
            </button>
        </div>

        <div>
            <div class="listViewContentDiv" id="listViewContents">
                <div class="contents-topscroll noprint">
                    <div class="topscroll-div" style="width: 95%">&nbsp;</div>
                </div>
                <div class="listViewEntriesDiv contents-bottomscroll">
                    <div class="bottomscroll-div" style="width: 95%; min-height: 80px">
                        <table class="table table-bordered listViewEntriesTable tableFormatted" id="nohover">
                            <thead>
                            <tr class="listViewHeaders">
                                <th>Titolo Utilizzazione</th>
                                <th>Compositori</th>
                                <th>Autori</th>
                                <th>Interpreti</th>
                                <th>Codice Opera Selezionato</th>
                                <th>&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="listViewEntries" style="background-color: #d0d0e0;font-size: 16px;height: 50px;">
                                <td style="border-top: 1px solid black;">{{utilizzazioni[index].titolo}}</td>
                                <td style="border-top: 1px solid black;">{{utilizzazioni[index].compositori}}</td>
                                <td style="border-top: 1px solid black;">{{utilizzazioni[index].autori}}</td>
                                <td style="border-top: 1px solid black;">{{utilizzazioni[index].interpreti}}</td>
                                <td style="border-top: 1px solid black;">
                                    <span style="margin-left:1%;margin-right:25%">{{utilizzazioni[index].codApprov}}</span>
                                    <button ng-show="utilizzazioni[index].codApprov != undefined" ng-click="deleteElements(utilizzazioni[index]); $event.stopPropagation();" type="button" class="btn addButton">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </button>
                                </td>
                                <td style="border-top: 1px solid black;">&nbsp;</td>
                            </tr>
                            <tr class="danger">
                                <th style="border-top: 1px solid rgba(0, 0, 0, 0.1) !important;">Titolo Opera</th>
                                <th style="border-top: 1px solid rgba(0, 0, 0, 0.1) !important;">Compositori</th>
                                <th style="border-top: 1px solid rgba(0, 0, 0, 0.1) !important;">Autori</th>
                                <th style="border-top: 1px solid rgba(0, 0, 0, 0.1) !important;">Interpreti</th>
                                <th style="border-top: 1px solid rgba(0, 0, 0, 0.1) !important;">Codice Opera</th>
                                <th style="border-top: 1px solid rgba(0, 0, 0, 0.1) !important;">Grado di confidenza</th>
                            </tr>
                            <tr id="tabellaCodifica" style="background-color:rgba(255, 255, 255)" ng-repeat="proposta in utilizzazioni[index].candidate  | limitTo: numeroCandidate : 0" ng-click="utilizzazioni[index].gradoConf = proposta.gradoDiConf;utilizzazioni[index].codApprov = proposta.codiceOpera; $index ? utilizzazioni[index].statApprov = 'A' : utilizzazioni[index].statApprov = 'P'; ">
                                <td>{{proposta.titolo}}<br>{{proposta.confTitolo | currency : "%" : 2}}</td>
                                <td>{{proposta.compositore}}<br>{{proposta.confCompositore | currency : "%" : 2}}</td>
                                <td>{{proposta.autore}}<br>{{proposta.confAutore | currency : "%" : 2}}</td>
                                <td>{{proposta.interprete}}<br>{{proposta.confInterprete | currency : "%" : 2}}</td>
                                <td>{{proposta.codiceOpera}}</td>
                                <td>
                                    {{proposta.gradoDiConf | currency : "%" : 2}}
                                    <span ng-class="proposta.codiceOpera == utilizzazioni[index].codApprov ? 'glyphicon glyphicon-ok pull-right' : ''"></span>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

					<label class="pageNumbers pull-left" style="margin-top:1%; margin-left:1%">
						*Utilizza i tasti da 1 a 5 per selezionare l'opera, il tasto INVIA per procedere e la barra spaziatrice per tornare indietro
					</label>
                    <button id="codifica" class="btn addButton" ng-click="sceltaProposte(utilizzazioni,'<%=userName%>')" style="float:right; margin-right:1% ;margin-top:1%;  margin-bottom:2%">
                        <span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;
                        <strong>Riepilogo Codifiche</strong>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--
<div id="myModal" class="modal fade modeless" tabindex="-1" role="dialog" data-show="true" data-keyboard="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" aria-hidden="true" aria-label="Chiudi">&times;
</button>
<h4 class="modal-title">Scorciatoie da tastiera</h4>

</div>
<div class="modal-body">
<p>Utilizza i tasti da 1 a 5 per selezionare l'opera, il tasto INVIA per procedere e la barra spaziatrice per tornare indietro</p>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">Chiudi
</button>
</div>
</div>
</div>
</div>
-->
