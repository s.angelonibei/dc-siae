<%@ page contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@
        include file="../../../navbar.jsp" %>
<input type="hidden" id="user"
       value="<%=(String) session.getAttribute("sso.user.userName")%>">

<div class="bodyContents">
    <div class="mainContainer row-fluid">

        <div class="contents-topscroll noprint">
            <div class="topscroll-div" style="width: 95%;margin-bottom: 20px">
                &nbsp;
            </div>
        </div>

        <div class="sticky">
            <div id="companyLogo" class="navbar commonActionsContainer noprint">
                <div class="actionsContainer row-fluid">
                    <div class="span2">
							<span class="companyLogo">
								<img src="images/logo_siae.jpg" title="SIAE"
                                     alt="SIAE">&nbsp;
							</span>
                    </div>
                    <span class="btn-toolbar span4">
							<div class="pageNumbers alignTop pull-right"
                                 style="text-align: center;">
								<span class="pageNumbersText"><strong>Monitoraggio Operatori Codifica</strong></span>
							</div>
						</span>
                </div>
            </div>
        </div>

        <div class="contentsDiv marginLeftZero" id="rightPanel"
             style="min-height: 80px;">
            <div class="listViewPageDiv">
                <div class="listViewActionsDiv row-fluid">

                    <table class="table table-bordered blockContainer showInlineTable equalSplit">
                        <thead>
                        <tr>
                            <th class="blockHeader" colspan="6"
                                ng-show="ctrl.hideForm"><span
                                    ng-click="ctrl.hideForm=false"
                                    class="glyphicon glyphicon-expand"></span>&nbsp;Parametri
                                Ricerca
                            </th>
                            <th class="blockHeader" colspan="6"
                                ng-hide="ctrl.hideForm"><span
                                    ng-click="ctrl.hideForm=true"
                                    class="glyphicon glyphicon-collapse-down"></span>&nbsp;Parametri
                                Ricerca
                            </th>
                        </tr>
                        </thead>
                        <tbody ng-hide="ctrl.hideForm">
                        <tr>
                            <td>
                                <select ng-model="mo.idPeriodoRipartizione"
                                        ng-change="init()"
                                        ng-options="periodo.codice as (periodo.codice + ' (' + (periodo.competenzeDa| date: 'dd-MM-yyyy') + '/' + (periodo.competenzeA | date: 'dd-MM-yyyy') + ')') for periodo in periodiRipartizione">
                                    <option></option>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td class="fieldLabel medium"
                                style="text-align: right" nowrap colspan="6">
                                <button id="applica" class="btn addButton"
                                        ng-click="search(periodoselezionato)">
                                    <span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Applica</strong>
                                </button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="listViewTopMenuDiv noprint">
                        <div class="listViewActionsDiv row-fluid">
                            <!--  -->
                            <span class="btn-toolbar span4"> <span
                                    class="btn-group">

								</span>
								</span>
                            <!--  -->
                            <!--  -->
                            <span class="btn-toolbar span4"> <span
                                    class="customFilterMainSpan btn-group"
                                    style="text-align: center;">
										<div class="pageNumbers alignTop ">
											<span class="pageNumbersText"><strong> <!-- Titolo -->
											</strong></span>
										</div>
								</span>
								</span>
                            <!--  -->
                            <!--  -->
                            <span class="span4 btn-toolbar">
									<div class="listViewActions pull-right">
										<div class="pageNumbers alignTop ">
											<span ng-show="paging.totRecords > 50">
												<span class="pageNumbersText"
                                                      style="padding-right: 5px">
													Record da <strong>{{paging.firstRecord}}</strong> a <strong>{{paging.lastRecord}}</strong> su <strong>{{paging.totRecords}}</strong>
											</span>
												<button title="Precedente"
                                                        class="btn"
                                                        id="listViewPreviousPageButton"
                                                        type="button"
                                                        ng-click="navigateToPreviousPage()"
                                                        ng-show="paging.currentPage > 1">
													<span class="glyphicon glyphicon-chevron-left"></span>
												</button>
												<button title="Successiva"
                                                        class="btn"
                                                        id="listViewNextPageButton"
                                                        type="button"
                                                        ng-click="navigateToNextPage()"
                                                        ng-show="paging.currentPage < paging.totPages">
													<span class="glyphicon glyphicon-chevron-right"></span>
												</button>
												<button title="Fine" class="btn"
                                                        id="listViewNextPageButton"
                                                        type="button"
                                                        ng-click="navigateToEndPage()"
                                                        ng-show="paging.currentPage < paging.totPages">
													<span class="glyphicon glyphicon-step-forward"></span>
												</button>
											</span>
										</div>
									</div>
									<div class="clearfix"></div>
								</span>
                            <!--  -->
                        </div>
                    </div>

                    <div class="listViewContentDiv" id="listViewContents">
                        <div class="contents-topscroll noprint">
                            <div class="topscroll-div" style="width: 95%">
                                &nbsp;
                            </div>
                        </div>
                        <div class="listViewEntriesDiv contents-bottomscroll">
                            <div class="bottomscroll-div"
                                 style="width: 95%; min-height: 80px">
                                <table class="table table-bordered listViewEntriesTable tableFormatted">
                                    <thead>
                                    <tr class="listViewHeaders">

                                        <th>Nome operatore</th>
                                        <th>Numero combane codificate</th>
                                        <th>Valore economico sbloccato</th>
                                        <!-- <th>Numero utilizzazioni scodificate </th>
                                        <th>Valore economico relativo alle scodifiche</th> -->
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="listViewEntries"
                                        ng-repeat="voce in records">
                                        <td ng-bind="voce.nomeOperatore"></td>
                                        <td ng-bind="voce.numeroUtilizzazioniCodificate"></td>
                                        <td ng-bind="voce.valoreEconomicoSbloccato | currency : '&euro;' : 2"></td>
                                        <!-- <td ng-bind="voce.numeroUtilizzazioniScodificate"></td>
                                        <td ng-bind="voce.valoreEconmicoScodifiche | currency : '&euro;' : 2"></td>-->
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
