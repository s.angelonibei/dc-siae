<%@ page import="com.google.inject.Injector"%><%@
	page
	import="com.google.inject.Key"%><%@
	page
	import="com.google.inject.name.Names"%><%@
	page language="java"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	final Injector injector = (Injector) pageContext.getServletContext().getAttribute(Injector.class.getName());
	final String userName = (String) session.getAttribute("sso.user.userName");
	final String homeUrl = injector.getInstance(Key.get(String.class, Names.named("home.url")));
%>
<form name="form" novalidate>


	<div id="top_of_dialog">
		<strong>Storico modifiche palinsesti</strong>
	</div>

	<div style="margin-top: 15px">
		<table class="table table-bordered listViewEntriesTable">
			<thead>
				<tr class="listViewHeaders">
					<th><a href="" class="listViewHeaderValues">Nome Palinsesto </a></th>
					<th><a href="" class="listViewHeaderValues">Codice Ditta </a></th>
					
					<th><a href="" class="listViewHeaderValues">Data Inizio
							Validit&agrave</a></th>
					<th><a href="" class="listViewHeaderValues">Data Fine
							Validit&agrave</a></th>
					<th><a href="" class="listViewHeaderValues">Data Ultima
							Modifica</a></th>
					<th><a href="" class="listViewHeaderValues">Utente Ultima
							Modifica</a></th>

				</tr>
			</thead>
			<tbody>

				<tr ng-hide="checkSize()" class="listViewEntries"
					ng-repeat="item in ctrl.palinsesti">
					<td class="listViewEntryValue ">{{item.nome}}</td>
					<td class="listViewEntryValue">{{item.codiceDitta}}</td>
					
					<td class="listViewEntryValue ">{{formatDate(item.dataInizioValidita)}}</td>
					<td class="listViewEntryValue ">{{formatDate(item.dataFineValidita)}}</td>
					<td class="listViewEntryValue ">{{formatDate(item.dataUltimaModifica)}}</td>
					<td class="listViewEntryValue ">{{item.utenteUltimaModifica}}</td>
				</tr>

				<tr ng-hide="!checkSize()">
					<td colspan="7">Non sono state effettuate modifiche al palinsesto
					</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div style="margin-top: 15px">
		<table width="100%">
			<tr>
				<td>
					<button type="button" class="btn addButton" ng-click="cancel()">
						<span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp;<strong>Annulla</strong>
					</button>
				</td>
			</tr>
		</table>
	</div>

</form>

<script type="text/ng-template" id="errorDialogId">

        <div id="target"  >
          <strong>{{ngDialogData}}</strong>
        </div>
    </script>

<script type="text/javascript">
	$('#top_of_dialog')[0].scrollIntoView(true);
</script>
