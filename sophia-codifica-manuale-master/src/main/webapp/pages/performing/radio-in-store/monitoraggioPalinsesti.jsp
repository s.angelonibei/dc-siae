<%@ page contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%><%@
		include file="../../navbar.jsp"%>

<div class="bodyContents">

	<div class="mainContainer row-fluid">

		<div id="companyLogo" class="navbar commonActionsContainer noprint">
			<div class="actionsContainer row-fluid">
				<div class="span2">
					<span class="companyLogo"><img src="images/logo_siae.jpg"
												   title="SIAE" alt="SIAE">&nbsp;</span>
				</div>
				<span class="btn-toolbar span4">
					<div class="pageNumbers alignTop pull-right"
						 style="text-align: center;">
						<span class="pageNumbersText"><strong>Monitoraggio
								Palinsesti</strong></span>
					</div>
				</span>
			</div>
		</div>

		<div class="contentsDiv marginLeftZero" id="rightPanel"
			 style="min-height: 180px;">
			<div class="listViewPageDiv">

				<div class="listViewTopMenuDiv noprint">
					<div class="listViewTopMenuDiv noprint">
						<div class="listViewActionsDiv row-fluid">

							<span class="btn-toolbar span4"> <span class="btn-group">

							</span>
							</span> <span class="btn-toolbar span4"> <span
								class="customFilterMainSpan btn-group"
								style="text-align: center;">
									<div class="pageNumbers alignTop "></div>
							</span>
						</div>
					</div>
				</div>

				<!-- table -->
				<div class="listViewContentDiv" id="listViewContents">
					<div class="listViewEntriesDiv">
						<div style="width: 100%; min-height: 400px">

							<table
									class="table table-bordered blockContainer showInlineTable ">
								<thead>
								<tr>
									<th class="blockHeader" colspan="12" ng-hide="ctrl.hideForm">
										Verifica Palinsesto</th>
								</tr>
								</thead>

								<tbody ng-hide="ctrl.hideForm">
								<tr>
									<td class="fieldLabel medium"><label
											class="muted pull-right marginRight10px"> <span
											class="redColor" ng-show="form.process.$invalid">*</span>Music
										Provider
									</label></td>
									<td class="fieldValue medium">
										<div class="row-fluid">
												<span class="span10">
												<select name="musicProvider"
														id="musicProvider" ng-change="getPalinsesti(ctrl.musicProvider)"
														ng-model="ctrl.musicProvider"
														ng-options="item.nome for item in ctrl.musicProviders"
												>
												</select>
												</span>
										</div>
									</td>


									<td class="fieldLabel medium"><label
											class="muted pull-right "> <span class="redColor"
																			 ng-show="form.process.$invalid">* </span>Palinsesto
									</label></td>
									<td class="fieldValue medium">
										<div class="row-fluid">
												<span class="span10">
												<select name="palinsesto"
														ng-disabled="!ctrl.musicProvider || ctrl.musicProvider === {}"
														id="palinsesto"
														ng-model="ctrl.palinsesto"
														ng-options="item.nome for item in ctrl.palinsesti"
												>
												</select>
												</span>
										</div>
									</td>
								</tr>
								<tr>
									<td class="fieldLabel medium"><label
											class="muted pull-right marginRight10px"> <span
											class="redColor" ng-show="form.process.$invalid">*</span>
										Anno
									</label></td>
									<td class="fieldValue medium">
										<div class="row-fluid">
												<span class="span10"> <select name="utilization"
																			  id="utilization" ng-model="ctrl.anno"
																			  ng-options="item for item in ctrl.anni">
												</select>
												</span>
										</div>
									</td>

									<td class="fieldLabel medium"><label
											class="muted pull-right marginRight10px"> <span
											class="redColor" ng-show="form.process.$invalid">*</span>
										Mesi
									</label></td>
									<td class="fieldValue medium">
										<div class="input-large">
											<div class="row-fluid">
													<span class="span10">
														<div class="input-large">
															<div ng-dropdown-multiselect="" options="ctrl.mesi"
																 selected-model="ctrl.mesiSelected" checkboxes="false"
																 extra-settings="multiselectSettings"></div>
														</div>
													</span>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td class="fieldLabel medium" style="text-align: right" nowrap
										colspan="12">
										<button
												ng-disabled="!(ctrl.musicProvider && ctrl.palinsesto && ctrl.anno && ctrl.mesiSelected && ctrl.mesiSelected.length > 0)"
												class="btn addButton"
												ng-click="scaricaFileArmonizzato(ctrl.musicProvider,ctrl.palinsesto,ctrl.anno,ctrl.mesiSelected)">
											<span class="glyphicon glyphicon-align-justify"></span>&nbsp;&nbsp;<strong>Scarica File Armonizzato</strong>
										</button>
										<button
												class="btn addButton"
												ng-click="apply(ctrl.musicProvider,ctrl.palinsesto,ctrl.anno,ctrl.mesiSelected)">
											<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Visualizza
											Risultati</strong>
										</button>
									</td>
								</tr>
								</tbody>
							</table>
							<!-- table -->

							<div class="listViewContentDiv" id="listViewContents">

								<div class="contents-topscroll noprint">
									<div class="topscroll-div" style="width: 95%">&nbsp;</div>
								</div>

								<div class="listViewEntriesDiv contents-bottomscroll">
									<div class="bottomscroll-div" style="width: 95%; min-height: 80px">

										<table class="table table-striped table-responsive-md btn-table">
											<thead>
											<tr class="listViewHeaders">
												<th>Nome<br>Music Provider&nbsp;<span ng-click="orderImage('spanEvento','idEvento')" id="spanEvento" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
												<th>Nome<br>Palinsesto&nbsp;<span ng-click="orderImage('spanPermesso','permesso')" id="spanPermesso" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
												<th>Anno/mese<br>riferimento&nbsp;<span ng-click="orderImage('spanContabilita','contabilita')" id="spanContabilita" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
												<th>Data<br>caricamento&nbsp;<span ng-click="orderImage('spanFattura','numeroFattura')" id="spanFattura" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
												<th>Utente<br>caricamento&nbsp;<span ng-click="orderImage('spanVoceIncasso','voceIncasso')" id="spanVoceIncasso" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
												<th>Report<br>Utilizzazioni&nbsp;<span ng-click="orderImage('spanDataInizioEvento','dataInizioEvento')" id="spanDataInizioEvento" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
												<th>File<br>Armonizzato&nbsp;<span ng-click="orderImage('spanOraInizioEvento','oraInizioEvento')" id="spanOraInizioEvento" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
											</tr>
											</thead>
											<tbody>
											<tr class="listViewEntries" ng-repeat="item in listUtilizzazioni">
												<td>{{item.perfMusicProvider.nome}}</td>
												<td>{{item.perfPalinsesto.nome}}</td>
												<td>{{item.anno ? ctrl.mesi[item.mese - 1].label + ' ' + item.anno : ''}}</td>
												<td>{{item.dataUpload | date : 'dd/MM/yyyy'}}</td>
												<td>{{item.utente.username}}</td>
												<td>
													<a class="btn addButton" ng-href="rest/performing/radioInStore/utilizationFiles/{{item.id}}">
														<span class="glyphicon glyphicon-list"></span>
													</a>
												</td>
												<td>
													<a class="btn addButton" href="javascript:void(0) "ng-click="downloadArmonizzato(item)">
														<span class="glyphicon glyphicon-align-justify"></span>
													</a>
												</td>
											</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>

							<!-- /table -->

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>