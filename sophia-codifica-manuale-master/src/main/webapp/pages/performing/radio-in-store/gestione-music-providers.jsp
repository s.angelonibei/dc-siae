<%@ page contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@
        include file="../../navbar.jsp" %>

<div class="bodyContents">
    <div class="mainContainer row-fluid">

        <div id="companyLogo" class="navbar commonActionsContainer noprint">
            <div class="actionsContainer row-fluid">
                <div class="span2">
                    <span class="companyLogo"><img src="images/logo_siae.jpg"
                                                   title="SIAE" alt="SIAE">&nbsp;</span>
                </div>
                <span class="btn-toolbar span4">
					<div class="pageNumbers alignTop pull-right"
                         style="text-align: center;">
						<span
                                ng-hide="ctrl.selectedMusicProvider!=undefined&&ctrl.selectedMusicProvider!=null"
                                class="pageNumbersText"> <strong>Gestione
								Music Provider</strong>
						</span> <span
                            ng-show="ctrl.selectedMusicProvider!=undefined&&ctrl.selectedMusicProvider!=null"
                            class="pageNumbersText"> <strong>Dettaglio
								Music Provider</strong>
						</span>
					</div>
				</span>
            </div>
        </div>

        <div
                ng-hide="ctrl.selectedMusicProvider!=undefined&&ctrl.selectedMusicProvider!=null"
                class="contentsDiv marginLeftZero" id="rightPanel"
                style="min-height: 80px;">


            <div class="listViewPageDiv">
                <!-- contenitore principale -->


                <div class="listViewActionsDiv row-fluid">
                    <table
                            class="table table-bordered blockContainer showInlineTable equalSplit">
                        <thead>
                        <tr>
                            <th class="blockHeader" colspan="6"
                                ng-show="ctrl.hideForm"><span
                                    ng-click="ctrl.hideForm=false"
                                    class="glyphicon glyphicon-expand"></span>&nbsp;Parametri
                                Ricerca
                            </th>
                            <th class="blockHeader" colspan="6"
                                ng-hide="ctrl.hideForm"><span
                                    ng-click="ctrl.hideForm=true"
                                    class="glyphicon glyphicon-collapse-down"></span>&nbsp;Parametri
                                Ricerca
                            </th>
                        </tr>
                        </thead>
                        <tbody ng-hide="ctrl.hideForm">
                        <tr>
                            <td class="fieldLabel medium"><label
                                    class="muted pull-right marginRight10px">
                                Music
                                Provider </label></td>
                            <td class="fieldValue medium">
                                <div class="row-fluid">
										<span class="span10">
											<input ng-model="ctrl.filterMusicProviderName">
										</span>
                                </div>
                            </td>

                        </tr>
                        <tr>
                            <td class="fieldLabel medium"
                                style="text-align: right" nowrap
                                colspan="6">
                                <button ng-disabled="form.$invalid"
                                        class="btn addButton"
                                        ng-click="getMusicProviders(ctrl.filterMusicProviderName)">
                                    <span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Applica</strong>
                                </button>
                            </td>

                        </tr>
                        </tbody>
                    </table>

                </div>
                <div>
                    <!-- actions and pagination -->
                    <div class="listViewTopMenuDiv noprint">
                        <div class="listViewActionsDiv row-fluid">

							<span class="btn-toolbar span4"> <span
                                    class="btn-group"></span>
							</span> <span class="btn-toolbar span4"> <span
                                class="customFilterMainSpan btn-group"
                                style="text-align: center;">
									<div class="pageNumbers alignTop ">
										<!--  -- >
					 				<span class="pageNumbersText"><strong>Titolo</strong></span>
<!--  -->
									</div>
							</span>
							</span> <span class="span4 btn-toolbar">
								<div class="listViewActions pull-right">
									<span class="btn-group"></span>
								</div>
								<div class="clearfix"></div>
							</span>

                        </div>
                    </div>
                    <div class="listViewTopMenuDiv noprint">
                        <div class="listViewActionsDiv row-fluid">
                            <%
                                if (null != session.getAttribute("brGestioneUtenti")) {
                            %>
                            <button class="btn addButton pull-right"
                                    ng-click="showAddMusicProvider()">
                                <span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;<strong>Nuovo
                                Music Provider</strong>
                            </button>
                            <%
                                }
                            %>
                        </div>
                    </div>


                    <!-- table -->
                    <div class="listViewContentDiv" id="listViewContents">

                        <div class="contents-topscroll noprint">
                            <div class="topscroll-div" style="width: 95%">
                                &nbsp;
                            </div>
                        </div>

                        <div class="listViewEntriesDiv contents-bottomscroll">
                            <div class="bottomscroll-div"
                                 style="width: 95%; min-height: 80px">

                                <table class="table table-bordered listViewEntriesTable">
                                    <thead>
                                    <tr class="listViewHeaders">
                                        <th><a href=""
                                               ng-click="sort('NomeEmittente')"
                                               class="listViewHeaderValues">Music<br>Provider
                                            <span
                                                    class="glyphicon glyphicon-sort"></span></a>
                                        </th>
                                        <th><a href=""
                                               ng-click="sort('DataCreazione')"
                                               class="listViewHeaderValues">Data<br>Creazione
                                            <span
                                                    class="glyphicon glyphicon-sort"></span></a>
                                        </th>
                                        <th><a href="" ng-click="sort('Attivo')"
                                               class="listViewHeaderValues">Attivo
                                            <span
                                                    class="glyphicon glyphicon-sort"></span></a>
                                        </th>
                                        <th><a href=""
                                               ng-click="sort('DataFineValidita')"
                                               class="listViewHeaderValues">
                                            Data<br>Fine Validità
                                            <span class="glyphicon glyphicon-sort"></span></a>
                                        </th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <tr class="listViewEntries"
                                        ng-repeat="item in ctrl.musicProvider |orderBy : ctrl.sortType">

                                        <td class="listViewEntryValue ">
                                            {{item.nome}}
                                        </td>
                                        <td class="listViewEntryValue ">
                                            {{formatDate(item.dataCreazione)}}
                                        </td>
                                        <td class="listViewEntryValue"><span
                                                ng-hide="item.dataFineValidita==null||item.dataFineValidita==undefined"
                                                class="glyphicon glyphicon-remove"></span>
                                            <span
                                                    ng-hide="item.dataFineValidita!=null&&item.dataFineValidita!=undefined"
                                                    class="glyphicon glyphicon-ok"></span>
                                        </td>
                                        <td class="listViewEntryValue ">
                                            {{formatDate(item.dataFineValidita)}}
                                        </td>
                                        <td class="listViewEntryValue ">
                                            <div class="pull-right"
                                                 style="text-align: center; margin-right: 12px">
                                                <button ng-hide="item.amountUsed === 0"
                                                        class="btn addButton"
                                                        ng-click="showMusicProvider(item)">
                                                    <strong>Gestione MP</strong>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /table -->

                </div>
            </div>
        </div>

        <input id="defvalue" type="hidden"> <input id="defvalue2"
                                                   type="hidden">
        <div
                ng-show="ctrl.selectedMusicProvider!=undefined&&ctrl.selectedMusicProvider!=null"
                class="contentsDiv marginLeftZero Row" id="rightPanel"
                style="min-height: 80px;">
            <div class="listViewPageDiv">
                <!-- contenitore principale -->
                <div class="listViewActionsDiv row-fluid">
					<span
                            ng-show="ctrl.selectedMusicProvider!=undefined&&ctrl.selectedMusicProvider!=null"
                            class="pageNumbersText">

						<button class="btn addButton" ng-click="back()">
							<span class="glyphicon glyphicon-arrow-left"></span>&nbsp;&nbsp;<strong>Indietro</strong>
						</button>

					</span>
                </div>
            </div>

            <div class="listViewPageDiv">
                <div>
                    <div class="listViewContentDiv" id="listViewContents">

                        <div class="listViewEntriesDiv contents-bottomscroll">
                            <div class="bottomscroll-div"
                                 style="width: 95%; min-height: 80px">

                                <table class="table table-bordered listViewEntriesTable">
                                    <thead>
                                    <tr class="listViewHeaders">
                                        <th><a href=""
                                               class="listViewHeaderValues">
                                            Music Provider</a></th>
                                        <th><a href=""
                                               class="listViewHeaderValues">Data
                                            Creazione</a></th>
                                        <th><a href=""
                                               class="listViewHeaderValues">Attivo</a>
                                        </th>
                                        <th><a href=""
                                               class="listViewHeaderValues">Data
                                            Fine Validità</a></th>
                                        <!-- <th>Logo</th>
                                        <th></th> -->
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <tr class="listViewEntries">
                                        <td class="listViewEntryValue ">
                                            {{ctrl.selectedMusicProvider.nome}}
                                        </td>
                                        <td class="listViewEntryValue ">
                                            {{formatDate(ctrl.selectedMusicProvider.data_creazione)}}
                                        </td>
                                        <td class="listViewEntryValue"><span
                                                ng-hide="ctrl.selectedMusicProvider.dataFineValidita==null||ctrl.selectedMusicProvider.dataFineValidita==undefined"
                                                class="glyphicon glyphicon-remove"></span>
                                            <span
                                               ng-hide="ctrl.selectedMusicProvider.dataFineValidita!=null&&ctrl.selectedMusicProvider.dataFineValidita!=undefined"
                                               class="glyphicon glyphicon-ok"></span>
                                        </td>
                                        <td class="listViewEntryValue ">
                                            {{formatDate(ctrl.selectedMusicProvider.dataFineValidita)}}
                                        </td>
                                        <!-- <td class="listViewEntryValue "><img
                                            style="max-width: 100px; max-height: 100px;"
                                            ngf-thumbnail="ctrl.file"></td>

                                        <td class="listViewEntryValue ">
                                            <div class="pull-right"
                                                style="text-align: center; margin-right: 12px">
                                                <button ng-hide="true"
                                                    class="btn addButton" ng-click="showEditImage(item)">
                                                    <strong>Cambia Immagine</strong>
                                                </button>
                                            </div>
                                        </td>-->
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div style="margin-top: 25px; margin-bottom: 15px">
                        <ul class="nav nav-tabs" id="inoviceTab">
                            <li class="active" ng-click="setCollapse(true)"><a
                                    href="#dsr">PALINSESTI</a></li>
                            <li><a href="#items" ng-click="setCollapse(false)">
                                UTENTI</a></li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane active" id="dsr">
                                <div class="Column">

                                    <button class="btn addButton pull-left"
                                            ng-click="showAddPalinsesto($event)">
                                        <span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;<strong>Aggiungi
                                        Palinsesto</strong>
                                    </button>
                                </div>
                                <div>
                                    <div class="listViewContentDiv"
                                         id="listViewContents">

                                        <div class="listViewEntriesDiv contents-bottomscroll">
                                            <div class="bottomscroll-div"
                                                 style="width: 95%; min-height: 80px">

                                                <table class="table table-bordered listViewEntriesTable">
                                                    <thead>
                                                    <tr class="listViewHeaders">
                                                        <th><a href=""
                                                               class="listViewHeaderValues">Nome
                                                            Palinsesto </a></th>
                                                        <th><a href=""
                                                               class="listViewHeaderValues">Codice
                                                            ditta
                                                        </a></th>
                                                        <th><a href=""
                                                               class="listViewHeaderValues">Attivo
                                                        </a></th>
                                                        <th><a href=""
                                                               class="listViewHeaderValues">Data
                                                            Creazione</a></th>
                                                        <th><a href=""
                                                               class="listViewHeaderValues">Data
                                                            inizio
                                                            Validit&agrave</a>
                                                        </th>
                                                        <th><a href=""
                                                               class="listViewHeaderValues">Data
                                                            Fine
                                                            Validit&agrave</a>
                                                        </th>
                                                        <th><a href=""
                                                               class="listViewHeaderValues">Data
                                                            Ultima Modifica</a>
                                                        </th>
                                                        <th><a href=""
                                                               class="listViewHeaderValues">Utente
                                                            Ultima Modifica</a>
                                                        </th>
                                                        <th></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    <tr class="listViewEntries"
                                                        ng-repeat="item in ctrl.palinsesti">
                                                        <td class="listViewEntryValue ">
                                                            {{item.nome}}
                                                        </td>
                                                        <td class="listViewEntryValue">
                                                            {{item.codiceDitta}}
                                                        </td>
                                                        <td class="listViewEntryValue">
																<span
                                                                        ng-hide="item.active==true"
                                                                        class="glyphicon glyphicon-remove">
																	</span>
                                                            <span
                                                                    ng-hide="item.active==false"
                                                                    class="glyphicon glyphicon-ok">
																</span>
                                                        </td>
                                                        <td class="listViewEntryValue ">
                                                            {{formatDate(item.dataCreazione)}}
                                                        </td>
                                                        <td class="listViewEntryValue ">
                                                            {{formatDate(item.dataInizioValidita)}}
                                                        </td>
                                                        <td class="listViewEntryValue ">
                                                            {{formatDate(item.dataFineValidita)}}
                                                        </td>
                                                        <td class="listViewEntryValue ">
                                                            {{formatDate(item.dataUltimaModifica)}}
                                                        </td>
                                                        <td class="listViewEntryValue ">
                                                            {{item.utenteUltimaModifica}}
                                                        </td>
                                                        <td><span
                                                                ng-click="showStoricoPalinsesti(item)"
                                                                class="glyphicon glyphicon-list-alt"></span>&nbsp;&nbsp;<strong></strong>

                                                            <span ng-click="showUpdatePalinsesto('<%=userName%>',item)"
                                                                  class="glyphicon glyphicon-edit"></span>&nbsp;&nbsp;<strong></strong>

                                                            <span ng-click="download(item)"
                                                                  class="glyphicon glyphicon-download-alt"></span><strong></strong>
                                                        </td>
                                                        <!--  glyphicon glyphicon-download-alt  glyphicon glyphicon-cloud-download alignMiddle  -->
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="items">
                                <div class="listViewPageDiv">
                                    <%
                                        if (null != session.getAttribute("brGestioneUtenti")) {
                                    %>
                                    <div class="Column">

                                        <button class="btn addButton pull-left"
                                                ng-click="showAddUtente('<%=userName%>')">
                                            <span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;<strong>Aggiungi
                                            Utente</strong>
                                        </button>
                                    </div>
                                    <%
                                        }
                                    %>
                                    <div>

                                        <div class="listViewContentDiv"
                                             id="listViewContents">

                                            <div class="listViewEntriesDiv contents-bottomscroll">
                                                <div class="bottomscroll-div"
                                                     style="width: 95%; min-height: 80px">

                                                    <table class="table table-bordered listViewEntriesTable">
                                                        <thead>
                                                        <tr class="listViewHeaders">
                                                            <th><a href=""
                                                                   class="listViewHeaderValues">Username
                                                            </a></th>
                                                            <th><a href=""
                                                                   class="listViewHeaderValues">Email
                                                            </a></th>
                                                            <th><a href=""
                                                                   class="listViewHeaderValues">Data
                                                                Creazione </a>
                                                            </th>
                                                            <th><a href=""
                                                                   class="listViewHeaderValues">Data
                                                                Ultima
                                                                Modifica </a>
                                                            </th>
                                                            <%
                                                                if (null != session.getAttribute("brGestioneUtenti")) {
                                                            %>
                                                            <th></th>
                                                            <%
                                                                }
                                                            %>

                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        <tr class="listViewEntries"
                                                            ng-repeat="item in ctrl.utenti">
                                                            <td class="listViewEntryValue ">
                                                                {{item.username}}
                                                            </td>
                                                            <td class="listViewEntryValue ">
                                                                {{item.email}}
                                                            </td>
                                                            <td class="listViewEntryValue ">
                                                                {{formatDate(item.dataCreazione)}}
                                                            </td>
                                                            <td class="listViewEntryValue ">
                                                                {{formatDate(item.dataUltimaModifica)}}
                                                            </td>
                                                            <%
                                                                if (null != session.getAttribute("brGestioneUtenti")) {
                                                            %>

                                                            <td class="listViewEntryValue ">
                                                                <div class="pull-right"
                                                                     style="text-align: center; margin-right: 12px">
                                                                    <button ng-hide="item.amountUsed === 0"
                                                                            class="btn addButton"
                                                                            ng-click="showUpdateUtente('<%=userName%>',item)">
                                                                        <strong>Modifica
                                                                            Utente</strong>
                                                                    </button>
                                                                </div>
                                                            </td>
                                                            <%
                                                                }
                                                            %>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script>
                            $('#inoviceTab a').click(function (e) {
                                e.preventDefault();
                                $(this).tab('show');
                            })
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>