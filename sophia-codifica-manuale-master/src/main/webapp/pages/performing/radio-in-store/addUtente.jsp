<%@ page import="com.google.inject.Injector"%><%@
	page
	import="com.google.inject.Key"%><%@
	page
	import="com.google.inject.name.Names"%><%@
	page language="java"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	final Injector injector = (Injector) pageContext.getServletContext().getAttribute(Injector.class.getName());
	final String userName = (String) session.getAttribute("sso.user.userName");
	final String homeUrl = injector.getInstance(Key.get(String.class, Names.named("home.url")));
%>
<form name="form" novalidate>

	<div id="top_of_dialog">
		<strong>Aggiungi Utente</strong>
	</div>


	*La password deve rispettare i requisiti di sicurezza SIAE <br><br>
	<span ng-hide="checkSize(ctrl.selectedPassword)" class="glyphicon glyphicon-remove"></span><span ng-show="checkSize(ctrl.selectedPassword)" class="glyphicon glyphicon-ok"></span> La lunghezza minima della password deve essere di almento 8 caratteri alfanumerici con:<br>
	<span ng-hide="checkNumber(ctrl.selectedPassword)" class="glyphicon glyphicon-remove"></span><span ng-show="checkNumber(ctrl.selectedPassword)" class="glyphicon glyphicon-ok"></span> almeno un carattere numerico  <br>
	<span ng-hide="checkLowercase(ctrl.selectedPassword)" class="glyphicon glyphicon-remove"></span><span ng-show="checkLowercase(ctrl.selectedPassword)" class="glyphicon glyphicon-ok"></span> almeno una lettera minuscola  <br>
	<span ng-hide="checkUppercase(ctrl.selectedPassword)" class="glyphicon glyphicon-remove"></span><span ng-show="checkUppercase(ctrl.selectedPassword)" class="glyphicon glyphicon-ok"></span> almeno una lettera maiuscola  <br>
	<span ng-hide="checkSpecialChar(ctrl.selectedPassword)" class="glyphicon glyphicon-remove"></span><span ng-show="checkSpecialChar(ctrl.selectedPassword)" class="glyphicon glyphicon-ok"></span> almeno un carattere speciale tra ($@!%*#?&-+_) <br>

	<div style="margin-top: 15px">
		<table
			class="table table-bordered blockContainer showInlineTable equalSplit">
			<thead>
				<tr>
					<th class="blockHeader" colspan="2">Utente</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="fieldLabel medium"><label
						class="muted pull-right marginRight10px"> <span
							class="redColor" ng-show="form.process.$invalid">*</span>Username
					</label></td>
					<td class="fieldValue medium">
						<div class="row-fluid">
							<span class="span10"> <input type="text"
								class="form-control span4" id="name"
								placeholder="Inserisci la username" ng-model="ctrl.selectedUsername"
								required>
							</span>
						</div>
					</td>
				</tr>

				<tr>
					<td class="fieldLabel medium"><label
						class="muted pull-right marginRight10px"> <span
							class="redColor" ng-show="form.process.$invalid">*</span>Password
					</label></td>
					<td class="fieldValue medium">
						<div class="row-fluid">
							<span class="span10"> <input type="text"
								class="form-control" id="name"
								placeholder="Inserisci la password" ng-model="ctrl.selectedPassword"
								required>
							</span>
						</div>
					</td>
				</tr>
			
				<tr>
					<td class="fieldLabel medium"><label
						class="muted pull-right marginRight10px"> <span
							class="redColor" ng-show="form.process.$invalid">*</span>Email
					</label></td>
					<td class="fieldValue medium">
						<div class="row-fluid">
							<span class="span10"> <input type="text"
								class="form-control span4" id="name"
								placeholder="Inserisci l' Email" ng-model="ctrl.selectedEmail"
								required>
							</span>
						</div>
					</td>
				</tr>
				
				<tr>
					<td class="fieldLabel medium"><label
						class="muted pull-right marginRight10px"> <span
							class="redColor" ng-show="form.process.$invalid">*</span>Invia Email
					</label></td>
					<td class="fieldValue medium">
						<div class="row-fluid">
							<span class="span10"> <input type="checkbox"
								class="form-control span4" id="name"
								ng-model="ctrl.sendMail">
							</span>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div 
		style="margin-top: 15px"
		id="top_of_dialog">
		<strong>Repertori Utente</strong>
	</div>
	
	<div style="margin-top: 15px">
		<table width="100%">
			<tr>
				<td>
					<button type="button" class="btn addButton" ng-click="cancel()">
						<span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp;<strong>Annulla</strong>
					</button>
				</td>
				<td align="right">
					<button ng-disabled="form.$invalid || 
									!checkSize(ctrl.selectedPassword)||
									!checkNumber(ctrl.selectedPassword)|| 
									!checkLowercase(ctrl.selectedPassword)|| 
									!checkUppercase(ctrl.selectedPassword)|| 
									!checkSpecialChar(ctrl.selectedPassword)"
						class="btn addButton" ng-click="save('<%=userName%>')">
						<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;<strong>Aggiungi</strong>
					</button>
				</td>
			</tr>
		</table>
	</div>

</form>

<script type="text/ng-template" id="errorDialogId">

        <div id="target"  >
          <strong>{{ngDialogData}}</strong>
        </div>
    </script>

<script type="text/javascript">
	$('#top_of_dialog')[0].scrollIntoView(true);
</script>
