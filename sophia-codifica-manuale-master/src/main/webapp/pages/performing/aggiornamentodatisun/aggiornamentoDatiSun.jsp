<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@
	include file="../../navbar.jsp"%>
<input type="hidden" id="user"
	value="<%=(String) session.getAttribute("sso.user.userName")%>">

<div class="bodyContents">
	<!--  ng-init="init({})" -->
	<div class="mainContainer row-fluid">

		<div class="contents-topscroll noprint">
			<div class="topscroll-div" style="width: 95%; margin-bottom: 20px">&nbsp;</div>
		</div>

		<div class="sticky">
			<div id="companyLogo" class="navbar commonActionsContainer noprint">
				<div class="actionsContainer row-fluid">
					<div class="span2">
						<span class="companyLogo"> <img src="images/logo_siae.jpg"
							title="SIAE" alt="SIAE">&nbsp;
						</span>
					</div>
					<span class="btn-toolbar span4">
						<div class="pageNumbers alignTop pull-right"
							style="text-align: center;">
							<span class="pageNumbersText"><strong>Aggiornamento
									Dati Sun</strong></span>
						</div>
					</span>
				</div>
			</div>
		</div>

		<div class="contentsDiv marginLeftZero" id="rightPanel"
			style="min-height: 80px;">
			<div class="listViewPageDiv">
				<div class="listViewActionsDiv row-fluid">
					<table
						class="table table-bordered blockContainer showInlineTable ">
						<thead>
							<tr>
								<th class="blockHeader" colspan=6 ng-show="vm.hideForm"><span
									ng-click="vm.hideForm=false" class="glyphicon glyphicon-expand"></span>&nbsp;Parametri
									Ricerca</th>
								<th class="blockHeader" colspan=6 ng-hide="vm.hideForm"><span
									ng-click="vm.hideForm=true"
									class="glyphicon glyphicon-collapse-down"></span>&nbsp;Parametri
									Ricerca</th>
							</tr>
						</thead>
						<tbody ng-hide="vm.hideForm">
							<tr>
								<td class="fieldLabel medium"><label
									class="muted pull-right "> Data Aggiornamento </label></td>
								<td colspan="3"><select
									ng-model="vm.ricerca.dataAggiornamento">
										<option value="">Seleziona Data Aggiornamento</option>
										<option value="{{periodo}}"
											ng-repeat="periodo in vm.dateAggiornamento">{{(periodo|
											date: 'dd/MM/yyyy')}}</option>
								</select></td>
							</tr>
							<tr>
								<td class="fieldLabel medium" style="text-align: right" nowrap
									colspan="4">
									<button id="applica" class="btn addButton"
										ng-click="vm.search(vm.ricerca.dataAggiornamento)">
										<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Applica</strong>
									</button>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

			<div class="listViewContentDiv" id="listViewContents">
				<div class="contents-topscroll noprint">
					<div class="topscroll-div" style="width: 95%">&nbsp;</div>
				</div>
				<div class="listViewEntriesDiv contents-bottomscroll">
					<div class="bottomscroll-div" style="width: 95%; min-height: 80px">
						<div style="border: 1px solid #333333;margin-bottom: 10px;"class="listViewEntries"
							ng-repeat="configurazione in vm.aggiornamentiSun">
							<table
								class="table table-bordered blockContainer showInlineTable ">
								<tbody>
									<tr>
										<td class="fieldLabel medium">Data Bonifica</td>
										<td>{{configurazione.dataBonifica| date: 'dd/MM/yyyy
											hh:mm'}}</td>
									</tr>
									<tr>
										<td class="fieldLabel medium">Record Totali</td>
										<td colspan="4">{{configurazione.recordTotali}}</td>
									</tr>
									<tr>
										<td class="fieldLabel medium">Record Inseriti</td>
										<td colspan="4">{{configurazione.recordInseriti}}</td>
									</tr>
									<tr>
										<td class="fieldLabel medium">Record Aggiornati</td>
										<td colspan="4">{{configurazione.recordAggiornati}}</td>
									</tr>
									<tr>
										<td class="fieldLabel medium">Record Identici</td>
										<td colspan="4">{{configurazione.recordIdentici}}</td>
									</tr>
									<tr>
										<td class="fieldLabel medium" colspan="5" style="text-align: center;">
											Record Non Aggiornati
										</td>
									</tr>
									<tr>
										<td>
											Programma Musicale
											<span title="Export Bonifica Programmi Musicali" ng-show="configurazione.recordNonAggiornatiProgrammaMusicale>0" ng-click="vm.exportProgrammaMusicale(configurazione.dataBonifica)" class="glyphicon 	glyphicon glyphicon-cloud-download"></span>
										</td>
										<td>
											Manifestazioni
											<span title="Export Bonifica Manifestazioni"  ng-show="configurazione.recordNonAggiornatiManifestazioni>0" ng-click="vm.exportManifestazioni(configurazione.dataBonifica)" class="glyphicon 	glyphicon glyphicon-cloud-download"></span>
						 				</td>
										<td>
											Movimento Contabile 
											<span title="Export Bonifica Movimento Contabile"  ng-show="configurazione.recordNonAggiornatiMovimentoContabile>0" ng-click="vm.exportMovimentiContabili(configurazione.dataBonifica)" class="glyphicon 	glyphicon glyphicon-cloud-download"></span>
										</td>
										<td>
											Direttore Esecuzione 
											<span  title="Export Bonifica Direttore Esecuzione "  ng-show="configurazione.recordNonAggiornatiDirettore>0"  ng-click="vm.exportDirettoriEsecuzione(configurazione.dataBonifica)" class="glyphicon 	glyphicon glyphicon-cloud-download"></span>
										</td>
										<td>
											Eventi Pagati 
											<span  title="Export Bonifica Eventi Pagati"  ng-show="configurazione.recordNonAggiornatiEventiPagati>0" ng-click="vm.exportEventiPagati(configurazione.dataBonifica)"  class="glyphicon 	glyphicon glyphicon-cloud-download"></span>
										</td>
									</tr>
									<tr>
										<td>
											{{configurazione.recordNonAggiornatiProgrammaMusicale}}<br>
										</td>
										<td>
											{{configurazione.recordNonAggiornatiManifestazioni}}
						 				</td>
										<td>
											{{configurazione.recordNonAggiornatiMovimentoContabile}}<br> 
										</td>
										<td>
											{{configurazione.recordNonAggiornatiDirettore}}<br>
										</td>
										<td>
											{{configurazione.recordNonAggiornatiEventiPagati}}<br>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>
		</div>