<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../navbar.jsp"%>
<div class="bodyContents">
  <div class="mainContainer row-fluid">
    <div class="contents-topscroll noprint">
      <div class="topscroll-div" style="width: 95%;margin-bottom: 20px">&nbsp;</div>
    </div>
    <div class="sticky">
      <div id="companyLogo" class="navbar commonActionsContainer noprint">
        <div class="actionsContainer row-fluid">
          <div class="span2">
                  <span class="companyLogo"><img src="images/logo_siae.jpg"
                                                 title="SIAE" alt="SIAE">&nbsp;</span>
          </div>
          <span class="btn-toolbar span4">
                  <div class="pageNumbers alignTop pull-right"
                       style="text-align: center;">
                     <span class="pageNumbersText"><strong>Ricerca Importi Riconciliati</strong></span>
                  </div>
               </span>
        </div>
      </div>
    </div>
    <div class="contentsDiv marginLeftZero" id="rightPanel"
         style="min-height: 80px;">
      <div class="listViewPageDiv">
        <div class="listViewActionsDiv row-fluid">
          <form name="form" novalidate>
            <table class="table table-bordered blockContainer showInlineTable equalSplit">
              <thead>
              <tr>
                <th class="blockHeader" colspan="6" ng-show="ctrl.hideForm"><span
                    ng-click="ctrl.hideForm=false"
                    class="glyphicon glyphicon-expand"></span>&nbsp;Parametri
                  Ricerca
                </th>
                <th class="blockHeader" colspan="6" ng-hide="ctrl.hideForm"><span
                    ng-click="ctrl.hideForm=true"
                    class="glyphicon glyphicon-collapse-down"></span>&nbsp;Parametri
                  Ricerca
                </th>
              </tr>
              </thead>
              <tbody ng-hide="ctrl.hideForm">
              <tr>
                <td class="fieldLabel medium"><label
                    class="muted pull-right marginRight10px">
                  Contabilit&agrave da </label>
                </td>
                <td>
                  <div class="controls">
                    <div class="input-append date" id="datepickerRiconciliazioneContabilityStartDate"
                         data-date-format="mm-yyyy">
                      <input placeholder="mm-aaaa" type="text" name="date"
                             ng-model="params.inizioPeriodoContabile"
                             style="width: 125px; height: 23px"> <span
                        class="add-on" style="height: 23px"><i
                        class="icon-th"></i></span>
                    </div>
                    <script type="text/javascript">
                        $("#datepickerRiconciliazioneContabilityStartDate")
                            .datepicker(
                                {
                                    format : 'mm-yyyy',
                                    language : 'it',
                                    viewMode : "months",
                                    minViewMode : "months",
                                    orientation : "bottom auto",
                                    autoclose : "true"
                                });
                    </script>
                  </div>
                </td>
                <td class="fieldLabel medium"><label
                    class="muted pull-right "> <span class="redColor"
                                                     ng-show="form.process.$invalid"></span>Contabilit&agrave a</label>
                </td>
                <td>
                  <div class="controls">
                    <div class="input-append date" id="datepickerRiconciliazioneContabilityEndDate"
                         data-date-format="mm-yyyy">
                      <input placeholder="mm-aaaa" type="text" name="date"
                             ng-model="params.finePeriodoContabile"
                             style="width: 125px; height: 23px"> <span
                        class="add-on" style="height: 23px"><i
                        class="icon-th"></i></span>
                    </div>
                    <script type="text/javascript">
                        $("#datepickerRiconciliazioneContabilityEndDate")
                            .datepicker(
                                {
                                    format : 'mm-yyyy',
                                    language : 'it',
                                    viewMode : "months",
                                    minViewMode : "months",
                                    orientation : "bottom auto",
                                    autoclose : "true"
                                });
                    </script>
                  </div>
                </td>
                <td>
                </td>
              </tr>
              <tr>
                <td class="fieldLabel medium"><label
                    class="muted pull-right marginRight10px">
                  Fattura da </label>
                </td>
                <td>
                  <div class="controls">
                    <datepicker date-value="params.inizioPeriodoFattura"
                                data-format="dd-mm-yyyy"
                                data-language="it"
                                data-view-mode="days"
                                data-min-view-mode="days"
                                data-orientation="bottom auto"
                                data-autoclose="true"
                                data-start-date="config.dateLimits.minDataFattura"
                                data-end-date="config.dateLimits.maxDataFattura"
                                data-on-update="updateDateValue('inizioPeriodoFattura', value)"
                    ></datepicker>
                  </div>
                </td>
                <td class="fieldLabel medium"><label
                    class="muted pull-right "> <span class="redColor"
                                                     ng-show="form.process.$invalid"></span>Fattura a</label>
                </td>
                <td>
                  <div class="controls">
                    <datepicker date-value="params.finePeriodoFattura"
                                data-format="dd-mm-yyyy"
                                data-language="it"
                                data-view-mode="days"
                                data-min-view-mode="days"
                                data-orientation="bottom auto"
                                data-autoclose="true"
                                data-start-date="config.dateLimits.minDataFattura"
                                data-end-date="config.dateLimits.maxDataFattura"
                                data-on-update="updateDateValue('finePeriodoFattura', value)"
                    ></datepicker>
                  </div>
                </td>
                <td>
                </td>
              </tr>
              <tr>
                <td>
                  <input id="agenzia" placeholder="Agenzia" ng-model="params.agenzia" uib-typeahead="agenzia for agenzia in config.agenzie | filter:$viewValue | limitTo:8"
                         typeahead-loading="loadingAgenzie" typeahead-no-results="noResultsAgenzie"
                         typeahead-wait-ms="500">
                  <i ng-show="loadingAgenzie" class="glyphicon glyphicon-refresh"></i>
                  <div ng-show="noResultsAgenzie">
                    <i class="glyphicon glyphicon-remove"></i> Nessun Risultato
                  </div>
                </td>
                <td>
                  <input id="sede" placeholder="Sede" ng-model="params.sede" uib-typeahead="sede for sede in config.sedi | filter:$viewValue | limitTo:8" typeahead-min-length="3"
                         typeahead-loading="loadingSede" typeahead-no-results="noResultsSede"
                         typeahead-wait-ms="500">
                  <i ng-show="loadingSede" class="glyphicon glyphicon-refresh"></i>
                  <div ng-show="noResultsSede">
                    <i class="glyphicon glyphicon-remove"></i> Nessun Risultato
                  </div>
                </td>
                <td>
                  <input id="singolaFattura" placeholder="Singola Fattura" ng-model="params.singolaFattura">
                </td>
                <td>
                  <input id="voceIncasso" placeholder="Voce Incasso" ng-model="params.voceIncasso" uib-typeahead="voceIncasso for voceIncasso in config.vociIncasso | filter:$viewValue | limitTo:8"
                         typeahead-loading="loadginVoceIncasso" typeahead-no-results="noResultsVoceIncasso"
                         typeahead-wait-ms="500">
                  <i ng-show="loadginVoceIncasso" class="glyphicon glyphicon-refresh"></i>
                  <div ng-show="noResultsVoceIncasso">
                    <i class="glyphicon glyphicon-remove"></i> Nessun Risultato
                  </div>
                </td>
                <td>
                  <input id="seprag" placeholder="Seprag" ng-model="params.seprag">
                </td>
              </tr>
                <tr>
                <td>
                  <select id="FatturaValidata" class="js-basic-single-select" ng-model="params.fatturaValidata">
                    <option value="tutti">Validazione Fatture:</option>
                    <option value="si">Fattura validata SI</option>
                    <option value="no">Fattura validata NO</option>
                  </select>
                </td>
                 <td>
                    <select id="FatturaValidata" class="js-basic-single-select" ng-model="params.megaConcerto">
                      <option value="tutti">Mega Concerto:</option>
                      <option value="si">Mega Concerto SI</option>
                      <option value="no">Mega Concerto NO</option>
                    </select>
                </td>
                <td></td>
                <td></td>
                <td></td>
                </tr>
              <tr>
                <td class="fieldLabel medium" style="text-align: right" nowrap colspan="6">
                  <button	type="submit" class="btn addButton" ng-click="checkParams(params)" ng-disabled="!isValid(params)">
                    <span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Applica</strong>
                  </button>
                </td>
              </tr>
              </tbody>
            </table>
          </form>
          <div ng-if="sampling && sampling.records && sampling.records.length > 0">
            <!-- actions and pagination -->
            <div class="listViewTopMenuDiv noprint">
              <div class="listViewActionsDiv row-fluid">
                        <span class="btn-toolbar span4"> <span class="btn-group">
                        </span>
                        </span>
                <span class="btn-toolbar span4">
                           <span
                               class="customFilterMainSpan btn-group"
                               style="text-align: center;">
                              <div class="pageNumbers alignTop ">
                                 <span class="pageNumbersText">
                                    <strong>
                                       <!-- Titolo -->
                                    </strong>
                                 </span>
                              </div>
                           </span>
                        </span>
                <span class="span4 btn-toolbar">
                           <div class="listViewActions pull-right">
                              <div class="pageNumbers alignTop ">
                                 <span ng-show="totRecords > 50">
                                 <span class="pageNumbersText" style="padding-right: 5px">
                                 Record da <strong ng-bind="(page)*50+1"></strong> a <strong ng-bind="(page*50) + sampling.records.length"></strong> su <strong ng-bind="totRecords"></strong>
                                 </span>
								 <button title="Inizio" class="btn"
                         ng-click="navigateToStartPage()" ng-show="page > 0">
                                 <span class="glyphicon glyphicon-step-backward"></span>
                                 </button>
                                 <button title="Precedente" class="btn"
                                         ng-click="navigateToPreviousPage()"
                                         ng-show="page > 0">
                                 <span class="glyphicon glyphicon-chevron-left"></span>
                                 </button>
                                 <button title="Successiva" class="btn"
                                         ng-click="navigateToNextPage()" ng-show="page < pageTot">
                                 <span class="glyphicon glyphicon-chevron-right"></span>
                                 </button>
                                 <button title="Fine" class="btn"
                                         ng-click="navigateToEndPage()" ng-show="page < pageTot">
                                 <span class="glyphicon glyphicon-step-forward"></span>
                                 </button>
                                 </span>
                              </div>
                           </div>
                           <div class="clearfix"></div>
                        </span>
              </div>
            </div>
            <div class="listViewContentDiv" id="listViewContents">
              <div class="contents-topscroll noprint">
                <div class="topscroll-div" style="width: 95%">&nbsp;</div>
              </div>
              <div class="listViewEntriesDiv contents-bottomscroll">
                <div class="bottomscroll-div" style="width: 95%; min-height: 80px">
                  <table class="table table-bordered listViewEntriesTable">
                    <thead>
                    <tr class="listViewHeaders">
                      <th>Periodo Contabile<br>(mese/anno)<button class="sortButton btn addButton" ng-click="orderImage('spanContabilita','periodo')" id="buttonContabilita"><span id="spanContabilita" style="font-size:80%" class="glyphicon glyphicon-sort"></span></button></th>
                      <th>Numero<br>Fattura<button class="sortButton btn addButton" ng-click="orderImage('spanNumeroFattura','numeroFattura')" id="buttonNumeroFattura"><span id="spanNumeroFattura" style="font-size:80%" class="glyphicon glyphicon-sort"></span></button></th>
                      <th>Voce<br>Incasso<button class="sortButton btn addButton" ng-click="orderImage('spanVoceIncasso','voceIncasso')" id="buttonVoceIncasso"><span id="spanVoceIncasso" style="font-size:80%" class="glyphicon glyphicon-sort"></span></button></th>
                      <th>Lordo Sophia<br>(al netto dell'iva)<button class="sortButton btn addButton" ng-click="orderImage('spanTotaleLordoSophia','lordoSophia')" id="buttonTotaleLordoSophia"><span id="spanTotaleLordoSophia" style="font-size:80%" class="glyphicon glyphicon-sort"></span></button></th>
                      <th>Lordo NDM<br>(al netto dell'iva)<button class="sortButton btn addButton" ng-click="orderImage('spanTotaleLordoNDM','lordoNdm')" id="buttonTotaleLordoNDM"><span id="spanTotaleLordoNDM" style="font-size:80%" class="glyphicon glyphicon-sort"></span></button></th>
                      <th>Fattura Validata<button class="sortButton btn addButton" ng-click="orderImage('spanFlag','semaforo')" id="buttonFlag"><span id="spanFlag" style="font-size:80%" class="glyphicon glyphicon-sort"></span></button></th>
                      <th>Sophia netto Aggio<br>(al netto di iva e aggio)<button class="sortButton btn addButton" ng-click="orderImage('spanTotaleSophiaAggio','nettoSophia')" id="buttonTotaleSophiaAggio"><span id="spanTotaleSophiaAggio" style="font-size:80%" class="glyphicon glyphicon-sort"></span></button></th>
                      <th>NDM netto Aggio<br>(al netto di iva e aggio)<button class="sortButton btn addButton" ng-click="orderImage('spanTotaleNDMAggio','nettoNdm')" id="buttonTotaleNDMAggio"><span id="spanTotaleNDMAggio" style="font-size:80%" class="glyphicon glyphicon-sort"></span></button></th>
                      <th>Perc. Aggio NDM<button class="sortButton btn addButton" ng-click="orderImage('spanPercentualeAggio','percAggioNdm')" id="buttonPercentualeAggio"><span id="spanPercentualeAggio" style="font-size:80%" class="glyphicon glyphicon-sort"></span></button></th>
                      <th>Sophia netto<br>(al netto di iva, aggio e 5%)<button class="sortButton btn addButton" ng-click="orderImage('spanTotaleSophiaNetto','nettoSophiaArt')" id="buttonTotaleSophiaNetto"><span id="spanTotaleSophiaNetto" style="font-size:80%" class="glyphicon glyphicon-sort"></span></button></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-if="sampling.records && sampling.records.length > 0">
                       <td> </td>
                       <td> </td>
                       <td> </td>
                       <td>{{totaliRiconciliazione.totaleLordoSophia | currency : '&euro;' : 2}}</td>
                       <td>{{totaliRiconciliazione.totaleLordoNdm | currency : '&euro;' : 2}}</td>
                       <td> </td>
                       <td>{{totaliRiconciliazione.totaleNettoSophiaAggio | currency : '&euro;' : 2}}</td>
                       <td>{{totaliRiconciliazione.totaleNettoNdmAggio | currency : '&euro;' : 2}}</td>
                       <td> </td>
                       <td>{{totaliRiconciliazione.totaleNettoSophia | currency : '&euro;' : 2}}</td>
                    </tr>
                    <tr class="listViewEntries" ng-repeat="item in sampling.records">
                      <td ng-bind="item.periodo"></td>
                      <td>
                        <a ng-click="goTo('/fatturaDetail/', item.numeroFattura)"><u>{{item.numeroFattura}}</u></a>
                      </td>
                      <td ng-bind="item.voceIncasso"></td>
                      <td ng-bind="item.lordoSophia | currency : '&euro;' : 2"></td>
                      <td ng-bind="item.lordoNdm | currency : '&euro;' : 2"></td>
                      <td ng-bind="item.semaforo ? 'SI' : 'NO' " ng-class="item.semaforo ? 'green' : 'red' "></td>
                      <td ng-bind="item.nettoSophia | currency : '&euro;' : 2"></td>
                      <td ng-bind="item.nettoNdm | currency : '&euro;' : 2"></td>
                      <td ng-bind="item.percAggioNdm | currency : '%' : 2"></td>
                      <td ng-bind="item.nettoSophiaArt | currency : '&euro;' : 2" ng-show="item.semaforo"></td>
                      <td ng-hide="item.semaforo"></td>
                    </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
    </div>
    <button ng-if="sampling.records && sampling.records.length > 0" class="btn addButton pull-right" ng-click="exportExcel()" style="margin: 0 2% 2% 0">Esportazione csv &nbsp;&nbsp;<span class="glyphicon glyphicon-download"></span></button>
    </div>
</div>