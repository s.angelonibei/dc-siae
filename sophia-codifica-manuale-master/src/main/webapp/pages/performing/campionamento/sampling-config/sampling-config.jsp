<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@
	include file="../../../navbar.jsp"%>

<script type="text/javascript">

	var noIntMessage = "Attenzione!\nOgni valore deve essere un intero compreso tra 1 e 90";
	var duplicateValueMessage = "Attenzione !\nNon e' possibile inserire valori duplicati.";
	var emptyMessageBsm = "Attenzione!\nInserire tutti e 5 i valori necessari per la configurazione BSM.";
	var emptyMessageConcertini = "Attenzione!\nInserire tutti e 5 i valori necessari per la configurazione Concertini.";

	$("#bsm1Id").on("change", function() {
		if (!isCorrectInt($(this).val())) {
			alert(noIntMessage);
		} else {
			startCalcoloRestoBsm();
		}
	});

	$("#bsm2Id").on("change", function() {
		if (!isCorrectInt($(this).val())) {
			alert(noIntMessage);
		} else {
			startCalcoloRestoBsm();
		}
	});
	$("#bsm3Id").on("change", function() {
		if (!isCorrectInt($(this).val())) {
			alert(noIntMessage);
		} else {
			startCalcoloRestoBsm();
		}
	});
	$("#bsm4Id").on("change", function() {
		if (!isCorrectInt($(this).val())) {
			alert(noIntMessage);
		} else {
			startCalcoloRestoBsm();
		}
	});
	$("#bsm5Id").on("change", function() {
		if (!isCorrectInt($(this).val())) {
			alert(noIntMessage);
		} else {
			startCalcoloRestoBsm();
		}
	});


	$("#concertini1Id").on("change", function() {
		if (!isCorrectInt($(this).val())) {
			alert(noIntMessage);
		} else {
			startCalcoloRestoConcertini();
		}
	});

	$("#concertini2Id").on("change", function() {
		if (!isCorrectInt($(this).val())) {
			alert(noIntMessage);
		} else {
			startCalcoloRestoConcertini();
		}
	});
	$("#concertini3Id").on("change", function() {
		if (!isCorrectInt($(this).val())) {
			alert(noIntMessage);
		} else {
			startCalcoloRestoConcertini();
		}
	});
	$("#concertini4Id").on("change", function() {
		if (!isCorrectInt($(this).val())) {
			alert(noIntMessage);
		} else {
			startCalcoloRestoConcertini();
		}
	});
	$("#concertini5Id").on("change", function() {
		if (!isCorrectInt($(this).val())) {
			alert(noIntMessage);
		} else {
			startCalcoloRestoConcertini();
		}
	});

	function startCalcoloRestoBsm() {
		if ($("#bsm1Id").val() != '' && $("#bsm2Id").val() != '' && $("#bsm3Id").val() != '' && $("#bsm4Id").val() != '' && $("#bsm5Id").val() != '') {

			var canGo = true;

			if ($("#bsm1Id").val() == $("#bsm2Id").val() || $("#bsm1Id").val() == $("#bsm3Id").val() || $("#bsm1Id").val() == $("#bsm4Id").val() || $("#bsm1Id").val() == $("#bsm5Id").val() ||
				$("#bsm2Id").val() == $("#bsm3Id").val() || $("#bsm2Id").val() == $("#bsm4Id").val() || $("#bsm2Id").val() == $("#bsm5Id").val() ||
				$("#bsm3Id").val() == $("#bsm4Id").val() || $("#bsm3Id").val() == $("#bsm5Id").val() ||
				$("#bsm4Id").val() == $("#bsm5Id").val()
			) {
				alert(duplicateValueMessage);
				canGo = false;
			}

			if (canGo == true) {
				var x = $("#bsm1Id").val();
				var r;
				var p;

				if (x >= 0 && x <= 18)
					r = 0;
				else if (x >= 19 && x <= 36)
					r = 1;
				else if (x >= 37 && x <= 54)
					r = 2;
				else if (x >= 55 && x <= 72)
					r = 3;
				else if (x >= 73 && x <= 90)
					r = 4;
				$("#resto5bsmId").val(r);

				p = $("#bsm1Id").val() * $("#bsm2Id").val() * $("#bsm3Id").val() * $("#bsm4Id").val() * $("#bsm5Id").val();
				r = p % 61;
				$("#resto61bsmId").val(r);
			}

		}
	}

	function startCalcoloRestoConcertini() {
		if ($("#concertini1Id").val() != '' && $("#concertini2Id").val() != '' && $("#concertini3Id").val() != '' && $("#concertini4Id").val() != '' && $("#concertini5Id").val() != '') {

			var canGo = true;

			if ($("#concertini1Id").val() == $("#concertini2Id").val() || $("#concertini1Id").val() == $("#concertini3Id").val() || $("#concertini1Id").val() == $("#concertini4Id").val() || $("#concertini1Id").val() == $("#concertini5Id").val() ||
				$("#concertini2Id").val() == $("#concertini3Id").val() || $("#concertini2Id").val() == $("#concertini4Id").val() || $("#concertini2Id").val() == $("#concertini5Id").val() ||
				$("#concertini3Id").val() == $("#concertini4Id").val() || $("#concertini3Id").val() == $("#concertini5Id").val() ||
				$("#concertini4Id").val() == $("#concertini5Id").val()
			) {
				alert(duplicateValueMessage);
				canGo = false;
			}

			if (canGo == true) {
				var x = $("#concertini1Id").val();
				var r;
				var p;

				if (x >= 0 && x <= 18)
					r = 0;
				else if (x >= 19 && x <= 36)
					r = 1;
				else if (x >= 37 && x <= 54)
					r = 2;
				else if (x >= 55 && x <= 72)
					r = 3;
				else if (x >= 73 && x <= 90)
					r = 4;
				$("#resto5concertiniId").val(r);

				p = $("#concertini1Id").val() * $("#concertini2Id").val() * $("#concertini3Id").val() * $("#concertini4Id").val() * $("#concertini5Id").val();
				r = p % 61;
				$("#resto61concertiniId").val(r);
			}
		}
	}

	function isCorrectInt(value) {
		var isValidNumber = !isNaN(value) && parseInt(Number(value)) == value && !isNaN(parseInt(value, 10));
		if (isValidNumber) {
			if (value < 1 || value > 90)
				isValidNumber = false;
		}

		return isValidNumber;
	}

	$(function() {
		$("#dataInizioLavorazioneId").datepicker();
		$("#dataEstrazioneLottoId").datepicker();

		$("#mesePeriodoContabilitaId").val("${mesePeriodoContabilita}");
		$("#annoPeriodoContabilitaId").val("${annoPeriodoContabilita}");

	});
</script>
<div class="bodyContents">
	<div class="mainContainer row-fluid">

		<div id="companyLogo" class="navbar commonActionsContainer noprint">
			<div class="actionsContainer row-fluid">
				<div class="span2">
					<!--  -- >
					<span class="companyLogo"><img src="images/siae_logo_siaetrasparente.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
					<!--  -->
					<span class="companyLogo"><img src="images/logo_siae.jpg"
						title="SIAE" alt="SIAE">&nbsp;</span>
				</div>
				<span class="btn-toolbar span4">
					<div class="pageNumbers alignTop pull-right"
						style="text-align: center;">
						<span class="pageNumbersText"><strong>Configurazione
								Campionamento</strong></span>
					</div>
				</span>
			</div>
		</div>


		<!-- table -->
		<div class="listViewContentDiv" id="listViewContents">

			<div class="contents-topscroll noprint">
				<div class="topscroll-div" style="width: 95%">&nbsp;</div>
			</div>

			<div class="contentMargin contents-bottomscroll">
				<div class="bottomscroll-div" style="width: 95%; min-height: 80px">

					<!-- INIZIO SEZIONE DATI -->

					<table style="width: 100%">
						<thead>
							<tr>
								<th class="blockHeader">CONTABILITA'</th>
								<th class="blockHeader">INIZIO<br />LAVORAZIONE
								</th>
								<th class="blockHeader">ESTRAZIONE<br />LOTTO
								</th>
								<th class="blockHeader"></th>
								<th class="blockHeader">ESTRATTI</th>
								<th class="blockHeader">RESTO<br />MODULO 5
								</th>
								<th class="blockHeader">RESTO<br />MODULO 61
								</th>
							</tr>
						</thead>
						<tr>
							<td>
								<div class="controls">
									<div class="input-append date" id="datepickerContability"
										data-date-format="mm-yyyy">

										<input type="text" name="date"
											ng-model="ctrl.selectedContabilityDate"
											style="width: 125px; height: 25px"> <span
											class="add-on" style="height: 25px"><i class="icon-th"></i></span>
									</div>
									<script type="text/javascript">
										$("#datepickerContability")
											.datepicker(
												{
													format : 'mm-yyyy',
													language : 'it',
													viewMode : "months",
													minViewMode : "months",
													orientation : "bottom auto",
													autoclose : "true"
												});
									</script>
								</div>
							</td>


							<td>
								<div class="controls">
									<div class="input-append date" id="datepickerWorkingDate"
										data-date-format="dd-mm-yyyy">

										<input type="text" name="date"
											ng-model="ctrl.selectedWorkingDate"
											style="width: 125px; height: 25px"> <span
											class="add-on" style="height: 25px"><i class="icon-th"></i></span>
									</div>
									<script type="text/javascript">
										$("#datepickerWorkingDate")
											.datepicker(
												{
													format : 'dd-mm-yyyy',
													language : 'it',
													viewMode : "days",
													minViewMode : "days",
													orientation : "bottom auto",
													autoclose : "true"
												});
									</script>
								</div>
							</td>


							<td>
								<div class="controls">
									<div class="input-append date" id="datepickerExtractionDate"
										data-date-format="dd-mm-yyyy">

										<input type="text" name="date" ng-model="ctrl.selectedDateTo"
											style="width: 125px; height: 25px"> <span
											class="add-on" style="height: 25px"><i class="icon-th"></i></span>
									</div>
									<script type="text/javascript">
										$("#datepickerExtractionDate")
											.datepicker(
												{
													format : 'dd-mm-yyyy',
													language : 'it',
													viewMode : "days",
													minViewMode : "days",
													orientation : "bottom auto",
													autoclose : "true"
												});
									</script>
								</div>
								</div>
							</td>

							<td align="center">

								<table border="0">
									<tr>
										<td height="20"></td>
										<td height="20"></td>
									</tr>
								</table>

								<table border="1" style="border-color: #575e78;">
									<tr>
										<td height="35" width="100" align="center" valign="center">BSM</td>
										<td height="35" width="100" align="center" valign="center">ROMA</td>
									</tr>
									<tr>
										<td height="35" width="100" align="center" valign="center">CONCERTINI</td>
										<td height="35" width="100" align="center" valign="center">MILANO</td>
									</tr>
								</table>
							</td>
							<td align="center">
								<table border="1" style="border-color: #575e78;">
									<tr>
										<td align="center" style="width: 59px;">1</td>
										<td align="center" style="width: 59px;">2</td>
										<td align="center" style="width: 59px;">3</td>
										<td align="center" style="width: 59px;">4</td>
										<td align="center" style="width: 59px;">5</td>
									</tr>
								</table>
								<table border="0">
									<tr>
										<td align="center"><input style="width: 50px;"
											type="text" value="${bsm1}" ng-model="ctrl.bsm1" name="bsm1" id="bsm1Id"
											required="true" /></td>
										<td align="center"><input style="width: 50px;"
											type="text" value="${bsm2}" ng-model="ctrl.bsm2" name="bsm2" id="bsm2Id"
											required="true" /></td>
										<td align="center"><input style="width: 50px;"
											type="text" value="${bsm3}" name="bsm3" ng-model="ctrl.bsm3"id="bsm3Id"
											required="true" /></td>
										<td align="center"><input style="width: 50px;"
											type="text" value="${bsm4}" name="bsm4" ng-model="ctrl.bsm4"id="bsm4Id"
											required="true" /></td>
										<td align="center"><input style="width: 50px;"
											type="text" value="${bsm5}" name="bsm5" ng-model="ctrl.bsm5"id="bsm5Id"
											required="true" /></td>
									</tr>
									<tr>
										<td><input style="width: 50px;" type="text"
											value="${concertini1}" name="concertini1" ng-model="ctrl.concertini1" id="concertini1Id"
											required="true" /></td>
										<td><input style="width: 50px;" type="text"
											value="${concertini2}" name="concertini2" ng-model="ctrl.concertini2" id="concertini2Id"
											required="true" /></td>
										<td><input style="width: 50px;" type="text"
											value="${concertini3}" name="concertini3" ng-model="ctrl.concertini3" id="concertini3Id"
											required="true" /></td>
										<td><input style="width: 50px;" type="text"
											value="${concertini4}" name="concertini4" ng-model="ctrl.concertini4" id="concertini4Id"
											required="true" /></td>
										<td><input style="width: 50px;" type="text"
											value="${concertini5}" name="concertini5" ng-model="ctrl.concertini5" id="concertini5Id"
											required="true" /></td>
									</tr>
								</table>
							</td>

							<td align="center" valign="center">
								<table border="0">
									<tr>
										<td height="20"></td>
										<td height="20"></td>
									</tr>
								</table>
								<table border="0">
									<tr>
										<td align="center" valign="center">
										<input ng-model="ctrl.resto5bsm"
											style="width: 50px;" type="text" readonly
											value="${resto5bsm}"   name="resto5bsm" id="resto5bsmId"
											required="true" /></td>
									</tr>
									<tr>
										<td align="center" valign="center">
										<input ng-model="ctrl.resto5concertini" 
											style="width: 50px;" type="text" readonly
											value="${resto5concertini}" name="resto5concertini"
											id="resto5concertiniId" required="true" /></td>
									</tr>
								</table>
							</td>

							<td align="center" valign="center">
								<table border="0">
									<tr>
										<td height="20"></td>
										<td height="20"></td>
									</tr>
								</table>
								<table border="0">
									<tr>
										<td align="center" valign="center">
										<input ng-model="ctrl.resto61bsmId" 
											style="width: 50px;" type="text" readonly
											value="${resto61bsm}" name="resto61bsm" id="resto61bsmId"
											required="true" /></td>
									</tr>
									<tr>
										<td align="center" valign="center">
										<input ng-model="ctrl.resto61concertini" 
											style="width: 50px;" type="text" readonly
											value="${resto61concertini}" name="resto61concertini"
											id="resto61concertiniId" required="true" /></td>
									</tr>
								</table>
							</td>

						</tr>

					</table>
					<div class="contentMargin">
						<table width="100%">
							<tr>
								<td align="right">
									<button ng-hide="checkStatusButton()" type="button" class="btn addButton"
										ng-click="saveConfiguration('<%=userName%>')">
										<span class="glyphicon glyphicon-save-file"></span>&nbsp;&nbsp;<strong>Salva</strong>
									</button>
								</td>
							</tr>
						</table>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>