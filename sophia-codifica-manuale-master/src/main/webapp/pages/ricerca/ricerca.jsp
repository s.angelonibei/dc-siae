<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../navbar.jsp" %>

<div class="bodyContents" >
	<div class="mainContainer row-fluid">
		<div id="companyLogo" class="navbar commonActionsContainer noprint">
			<div class="actionsContainer row-fluid">
				<div class="span2">
					<span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
				</div>
				<span class="btn-toolbar span4">
					<div class="pageNumbers alignTop pull-right" style="text-align: center;">
		 				<span class="pageNumbersText"><strong>Ricerca opere non riconosciute</strong></span>
					</div>
				</span>
			</div>
		</div>

		<div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px;">
			<div class="listViewPageDiv">

				<!-- form ricerca -->
				<div class="listViewTopMenuDiv noprint">
					<div class="listViewActionsDiv row-fluid">
						<form name="myForm" ng-submit="onRicerca()" novalidate>
							<input type="hidden" ng-model="username" ng-value="<%=(String) session.getAttribute("sso.user.userName")%>" value="<%=(String) session.getAttribute("sso.user.userName")%>">
							<table class="table table-bordered blockContainer showInlineTable equalSplit">
								<thead>
								<tr>
									<th class="blockHeader" colspan="6">
										<span ng-click="ctrl.hideForm=!ctrl.hideForm" class="glyphicon glyphicon-expand"></span>&nbsp;Parametri Ricerca
									</th>
								</tr>
								</thead>
								<tbody ng-hide="ctrl.hideForm">
								<tr>
									<td class="fieldLabel small" nowrap>
										<label class="muted pull-right marginRight10px">
											Titolo
										</label>
									</td>
									<td class="fieldValue large">
										<div class="row-fluid" style="max-width: 450px;">
											<input class="input-large" type="text" ng-model="ctrl.title">
										</div>
									</td>

									<td class="fieldLabel small" nowrap>
										<label class="muted pull-right marginRight10px">
											Artisti
										</label>
									</td>
									<td class="fieldValue large">
										<div class="row-fluid" style="max-width: 450px;">
											<input class="input-large" type="text" ng-model="ctrl.artists">
										</div>
									</td>

									<td class="fieldLabel samll" nowrap>
										<label class="muted pull-right marginRight10px">
											Tipologia Artista
										</label>
									</td>
									<td class="fieldValue large">
										<div class="row-fluid" style="max-width: 450px;">
											<input class="input-large" type="text" ng-model="ctrl.roles">
										</div>
									</td>
								</tr>

								<tr>
									<td class="fieldLabel medium" style="text-align:right" nowrap colspan="6">
										<button type="button" class="btn btn-success" ng-click="$event.preventDefault();onAzzera()"><span class="glyphicon glyphicon-erase"></span>&nbsp;<strong>Cancella</strong></button>
										<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-search"></span>&nbsp;<strong>Ricerca</strong></button>
									</td>
								</tr>
								</tbody>
							</table>
						</form>

					</div>
				</div>

				<!-- actions and pagination -->
				<div class="listViewTopMenuDiv noprint">
					<div class="listViewActionsDiv row-fluid">
						<!--  -->
						<span class="btn-toolbar span4">
							<span class="btn-group">

 							</span>
						</span>
						<!--  -->
						<!--  -->
						<span class="btn-toolbar span4">
							<span class="customFilterMainSpan btn-group" style="text-align: center;">
								<div class="pageNumbers alignTop ">
					 				<span class="pageNumbersText"><strong><!-- Titolo --></strong></span>
								</div>
						 	</span>
						</span>
						<!--  -->
						<!--  -->
						<span class="span4 btn-toolbar">
							<div class="listViewActions pull-right">

					 			<div class="pageNumbers alignTop ">
						 			<span ng-show="unidentifiedSongs.hasPrev || unidentifiedSongs.hasNext">
						 				<span class="pageNumbersText" style="padding-right:5px">
						 					Record da <strong ng-bind="unidentifiedSongs.first+1"></strong> a <strong ng-bind="unidentifiedSongs.last"></strong>
						 				</span>
					 					<button title="Precedente" class="btn" type="button"
												ng-click="$event.preventDefault();navigateToPreviousPage()"
												ng-show="unidentifiedSongs.hasPrev">
					 						<span class="glyphicon glyphicon-chevron-left"></span>
					 					</button>
					 					<button title="Successiva" class="btn" type="button"
												ng-click="$event.preventDefault();navigateToNextPage()"
												ng-show="unidentifiedSongs.hasNext">
					 						<span class="glyphicon glyphicon-chevron-right"></span>
					 					</button>
					 				</span>
						 		</div>
					 		</div>
					 		<div class="clearfix"></div>
					 	</span>
					</div>
				</div>

				<!-- riga da riconoscere -->
				<!-- table -->
				<div class="listViewContentDiv" id="listViewContents" ng-if="unidentifiedSongs.rows && unidentifiedSongs.rows.length > 0">

					<div class="contents-topscroll noprint">
						<div class="topscroll-div" style="width: 95%">&nbsp;</div>
					</div>

					<div class="listViewEntriesDiv contents-bottomscroll">
						<div class="bottomscroll-div" style="width: 95%; min-height: 80px">

							<table class="table table-bordered listViewEntriesTable">
								<thead>
                  <tr class="listViewHeaders">
                    <th></th>
                    <th nowrap><a ng-click="sort('title')" class="listViewHeaderValues" >Titolo
                      <span ng-show="ctrl.sortType=='title' && !ctrl.sortReverse" class="glyphicon glyphicon-sort-by-alphabet"></span>
                      <span ng-show="ctrl.sortType=='title' && ctrl.sortReverse" class="glyphicon glyphicon-sort-by-alphabet-alt"></span>
                    </a></th>
                    <th nowrap><a ng-click="sort('artists')" class="listViewHeaderValues" >Artisti
                      <span ng-show="ctrl.sortType=='artists' && !ctrl.sortReverse" class="glyphicon glyphicon-sort-by-alphabet"></span>
                      <span ng-show="ctrl.sortType=='artists' && ctrl.sortReverse" class="glyphicon glyphicon-sort-by-alphabet-alt"></span>
                    </a></th>
                    <th nowrap><a ng-click="sort('roles')" class="listViewHeaderValues" >Tipologia Artisti
                      <span ng-show="ctrl.sortType=='roles' && !ctrl.sortReverse" class="glyphicon glyphicon-sort-by-alphabet"></span>
                      <span ng-show="ctrl.sortType=='roles' && ctrl.sortReverse" class="glyphicon glyphicon-sort-by-alphabet-alt"></span>
                    </a></th>
                    <th nowrap><a ng-click="sort('priority')" class="listViewHeaderValues" >Priorit&agrave;
                      <span ng-show="ctrl.sortType=='priority' && !ctrl.sortReverse" class="glyphicon glyphicon-sort-by-attributes"></span>
                      <span ng-show="ctrl.sortType=='priority' && ctrl.sortReverse" class="glyphicon glyphicon-sort-by-attributes-alt"></span>
                    </a></th>
                    <th></th>
                  </tr>
								</thead>
								<tbody >
								<tr class="listViewEntries" ng-repeat="item in unidentifiedSongs.rows | orderBy:ctrl.sortType:ctrl.sortReverse">

									<td class="listViewEntryValue medium" nowrap>
										<span is-locked="item.lastManualTime"
													class="glyphicon glyphicon-eye-close"
													title="{{item.username}}" data-toggle="tooltip"></span>
									</td>
									<td class="listViewEntryValue medium" nowrap ng-bind="item.title"></td>
									<td class="listViewEntryValue medium" ng-bind="item.artists.split('|').join(', ')"></td>
									<td class="listViewEntryValue medium" ng-bind="item.roles.split('|').join(', ')"></td>
									<td class="listViewEntryValue medium" nowrap ng-bind="item.priority | number"></td>

									<td nowrap class="medium">
										<div class="actions pull-right">
											<!--  -->
											<span class="actionImages">
						 					<a ng-click="gotoRiconoscimento(item.hashId,'<%=(String) session.getAttribute("sso.user.userName")%>')"><i title="Riconosci" class="glyphicon glyphicon-eye-open alignMiddle"></i></a>&nbsp;
						 				</span>
											<!--  -->
										</div>
									</td>

								</tr>
								</tbody>
							</table>

						</div>
					</div>
				</div>
				<!-- /table -->

				<!-- pagination -->
				<div class="listViewTopMenuDiv noprint">
					<div class="listViewActionsDiv row-fluid">

						<span class="btn-toolbar span4">
						</span>

						<span class="btn-toolbar span4">
						</span>

						<span class="span4 btn-toolbar">
							<div class="listViewActions pull-right">

					 			<div class="pageNumbers alignTop ">
						 			<span ng-show="unidentifiedSongs.hasPrev || unidentifiedSongs.hasNext">
						 				<span class="pageNumbersText" style="padding-right:5px">
						 					Record da <strong ng-bind="unidentifiedSongs.first+1"></strong> a <strong ng-bind="unidentifiedSongs.last"></strong>
						 				</span>
					 					<button title="Precedente" class="btn" id="listViewPreviousPageButton" type="button"
												ng-click="navigateToPreviousPage()"
												ng-show="unidentifiedSongs.hasPrev">
					 						<span class="glyphicon glyphicon-chevron-left"></span>
					 					</button>
					 					<button title="Successiva" class="btn" id="listViewNextPageButton" type="button"
												ng-click="navigateToNextPage()"
												ng-show="unidentifiedSongs.hasNext">
					 						<span class="glyphicon glyphicon-chevron-right"></span>
					 					</button>
					 				</span>
						 		</div>

					 		</div>
					 		<div class="clearfix"></div>
					 	</span>
					</div>
				</div>
				<!-- /pagination -->

			</div>
		</div>
	</div>
	<!-- -- >
    <pre ng-bind="ctrl.properties"></pre>
  < !-- -->
</div>