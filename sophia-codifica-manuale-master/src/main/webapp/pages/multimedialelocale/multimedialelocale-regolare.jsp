<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@
	include file="../navbar.jsp"%>

<div class="bodyContents">

	<div class="mainContainer row-fluid">

		<div id="companyLogo" class="navbar commonActionsContainer noprint">
			<div class="actionsContainer row-fluid">
				<div class="span2">
					<span class="companyLogo"><img src="images/logo_siae.jpg"
						title="SIAE" alt="SIAE">&nbsp;</span>
				</div>
				<span class="btn-toolbar span4">
					<div class="pageNumbers alignTop pull-right"
						style="text-align: center;">
						<span class="pageNumbersText"><strong>Monitoraggio Codificato Regolare</strong></span>
					</div>
				</span>
			</div>
		</div>

		<div class="contentsDiv marginLeftZero" id="rightPanel"
			style="min-height: 180px;">
			<div
				ng-hide="ctrl.dettailSelected!=null&&ctrl.dettailSelected!=undefined">
				<div class="listViewPageDiv">

					<div class="listViewTopMenuDiv noprint">
						<div class="listViewTopMenuDiv noprint">
							<div class="listViewActionsDiv row-fluid">

								<span class="btn-toolbar span4"> <span class="btn-group">

								</span>
								</span> <span class="btn-toolbar span4"> <span
									class="customFilterMainSpan btn-group"
									style="text-align: center;">
										<div class="pageNumbers alignTop "></div>
								</span>
								</span> <span class="span4 btn-toolbar">
									<div class="listViewActions pull-right">
										<!-- span class="btn-group">
										<button class="btn addButton" ng-click="showUpload($event)">
											<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;<strong>Nuovo</strong>
										</button>
									</span> -->
									</div>
									<div class="clearfix"></div>
								</span>

							</div>
						</div>
					</div>

					<!-- table -->
					<div class="listViewContentDiv" id="listViewContents">

						<div class="listViewEntriesDiv">
							<div style="width: 100%; min-height: 400px">

								<table
									class="table table-bordered blockContainer showInlineTable ">
									<thead>
										<tr>
											<th class="blockHeader" colspan="12" ng-hide="ctrl.hideForm">
												Parametri di Ricerca</th>
										</tr>
									</thead>

									<tbody ng-hide="ctrl.hideForm">

										<tr>
											<td class="fieldLabel medium"><label
												class="muted pull-right marginRight10px"> <span
													class="redColor" ng-show="form.process.$invalid">*</span>Data
													Avvio da
											</label></td>
											<td class="fieldLabel medium" nowrap><input type="text"
												class="span9 input-small datepickerAvvioRipartizioneDa"
												style="width: 180px" ng-model="ctrl.selectedPeriodStartFrom">
												<script type="text/javascript">
													$(
															".datepickerAvvioRipartizioneDa")
															.datepicker(
																	{
																		format : 'dd-mm-yyyy',
																		language : 'it',
																		viewMode : "days",
																		minViewMode : "days",
																		autoclose : true,
																		todayHighlight : true
																	});
												</script> <span class="glyphicon glyphicon-erase"
												style="margin-top: 6px;"
												ng-click="ctrl.selectedPeriodStartFrom=undefined;"></span></td>

											<td class="fieldLabel medium"><label
												class="muted pull-right marginRight10px"> <span
													class="redColor" ng-show="form.process.$invalid">*</span>Data
													Avvio a
											</label></td>
											<td class="fieldLabel medium" nowrap><input type="text"
												class="span9 input-small datepickerAvvioRipartizioneA"
												style="width: 180px" ng-model="ctrl.selectedPeriodStartTo">
												<script type="text/javascript">
													$(
															".datepickerAvvioRipartizioneA")
															.datepicker(
																	{
																		format : 'dd-mm-yyyy',
																		language : 'it',
																		viewMode : "days",
																		minViewMode : "days",
																		autoclose : true,
																		todayHighlight : true
																	});
												</script> <span class="glyphicon glyphicon-erase"
												style="margin-top: 6px;"
												ng-click="ctrl.selectedPeriodStartTo=undefined;"></span></td>
											<td class="fieldLabel medium" nowrap colspan="4"></td>
										</tr>
										<tr>
											<td class="fieldLabel medium" style="text-align: right"
												nowrap colspan="12">
												<button class="btn addButton"
													ng-click="apply(ctrl.selectedPeriodStartFrom,ctrl.selectedPeriodStartTo)">
													<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Applica</strong>
												</button>

											</td>
										</tr>
									</tbody>
								</table>
								<br>
								<div class="listViewActions pull-right">
						 			
						 			<div class="pageNumbers alignTop ">
							 			<span ng-show="ctrl.report.hasPrev || ctrl.report.hasNext">
							 				<span class="pageNumbersText" style="padding-right:5px">
							 					Record da <strong>{{ctrl.report.first+1}}</strong> a <strong>{{ctrl.report.last}}</strong>
							 				</span>
						 					<button title="Precedente" class="btn" id="listViewPreviousPageButton" type="button" 
						 							ng-click="navigateToPreviousPage()"
						 							ng-show="ctrl.dsrProcessConfig.hasPrev">
						 						<span class="glyphicon glyphicon-chevron-left"></span>
						 					</button>
						 					<button title="Successiva" class="btn" id="listViewNextPageButton" type="button"
						 							ng-click="navigateToNextPage()"
						 							ng-show="ctrl.dsrProcessConfig.hasNext">
						 						<span class="glyphicon glyphicon-chevron-right"></span>
						 					</button>
						 				</span>
							 		</div>
						 		</div>
								<br>
								<!-- table -->
								<div class="listViewEntriesDiv contents-bottomscroll">
									<div class="bottomscroll-div"
										style="width: 95%; min-height: 80px">

										<table class="table table-bordered listViewEntriesTable ">
											<thead>
												<tr class="listViewHeaders">
													<th><a href="" class="listViewHeaderValues">Data
															Avvio </a></th>
													<th><a href="" class="listViewHeaderValues">N.
															Report da Riprocessare </a></th>
													<th><a href="" class="listViewHeaderValues">N.
															Report Riprocessati </a></th>
													<th><a href="" class="listViewHeaderValues">Periodo di Competenza
													</a></th>
													<th><a href="" class="listViewHeaderValues"></a></th>
												</tr>
											</thead>
											<tbody>

												<tr class="listViewEntries " ng-repeat="item in ctrl.report.rows">
													<td class="listViewEntryValue ">{{formatDate(item.dataAvvio)}}</td>
													<td class="listViewEntryValue ">{{item.reportDaRiprocessare}}</td>
													<td class="listViewEntryValue ">{{item.reportRiprocessati}}</td>
													<td class="listViewEntryValue ">{{item.ripartizione}}</td>
													<td class="listViewEntryValue "><span
														ng-click="showDettails(item)"
														class="glyphicon glyphicon-list-alt"></span></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div
				ng-hide="ctrl.dettailSelected==null||ctrl.dettailSelected==undefined">

				<br> <br>
				<div id="top_of_dialog">
					<strong>Dettaglio Report</strong>
				</div>
				<br> <br>
				<button class="btn addButton" ng-click="back()">
					<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Indietro</strong>
				</button>

				<br> <br>
				<table class="table table-bordered listViewEntriesTable">
					<tbody>

						<tr class="listViewEntries">
							<td
								style="color: #444; font-weight: bold; font-size: 16px !important;"
								class="fieldLabel ">Acquisizione Documentazione</td>
							<td class="listViewEntryValue ">{{formatDate(ctrl.dettailSelected.dataAcquisizioneDocumentazione)}}</td>
						</tr>
					</tbody>
				</table>
				<br> <br> Periodi Rilavorati
				<table class="table table-bordered listViewEntriesTable ">
					<thead>
						<tr class="listViewHeaders">
							<th><a href="" class="listViewHeaderValues">Periodo </a></th>
							<th><a href="" class="listViewHeaderValues">Data
									Acquisizione Schema Riparto </a></th>
							<th><a href="" class="listViewHeaderValues">N. Report da
									Rilavorare </a></th>
							<th><a href="" class="listViewHeaderValues">N. Report 
									Rilavorati </a></th>
						</tr>
					</thead>
					<tbody>

						<tr class="listViewEntries " ng-repeat="item in ctrl.dettailSelected.periodiRilavorati">
							<td class="listViewEntryValue ">{{item.periodo}}</td>
							<td class="listViewEntryValue ">{{formatDate(item.dataAcquisizioneSchemaRiparto)}}</td>
							<td class="listViewEntryValue ">{{item.reportDaRilavorare}}</td>
							<td class="listViewEntryValue ">{{item.reportRilavorati}}</td>
						</tr>
					</tbody>
				</table>
				<br> <br> Stato Lavorazione
				<table class="table table-bordered listViewEntriesTable">
					<tbody>

						<tr class="listViewEntries">
							<td
								style="color: #444; font-weight: bold; font-size: 16px !important;"
								class="fieldLabel ">N. Report da Riprocessare</td>
							<td class="listViewEntryValue ">{{ctrl.dettailSelected.reportDaRiprocessare}}</td>

							<td
								style="color: #444; font-weight: bold; font-size: 16px !important;"
								class="fieldLabel ">N. Report Riprocessati</td>
							<td class="listViewEntryValue ">{{ctrl.dettailSelected.reportRiprocessati}}</td>

							<td
								style="color: #444; font-weight: bold; font-size: 16px !important;"
								class="fieldLabel ">% Report Processati</td>
							<td class="listViewEntryValue ">{{calcolaPercentuale(ctrl.dettailSelected.reportRiprocessati,ctrl.dettailSelected.reportDaRiprocessare)}}</td>
						</tr>

						<tr class="listViewEntries">
							<td
								style="color: #444; font-weight: bold; font-size: 16px !important;"
								class="fieldLabel ">N. Record da Riprocessare</td>
							<td class="listViewEntryValue ">{{ctrl.dettailSelected.recordDaRiprocessare}}</td>

							<td
								style="color: #444; font-weight: bold; font-size: 16px !important;"
								class="fieldLabel ">N. Record Riprocessati</td>
							<td class="listViewEntryValue ">{{ctrl.dettailSelected.recordRiprocessati}}</td>

							<td
								style="color: #444; font-weight: bold; font-size: 16px !important;"
								class="fieldLabel ">% Record Processati</td>
							<td class="listViewEntryValue ">{{calcolaPercentuale(ctrl.dettailSelected.recordRiprocessati,ctrl.dettailSelected.recordDaRiprocessare)}}</td>
						</tr>

						<tr class="listViewEntries">
							<td
								style="color: #444; font-weight: bold; font-size: 16px !important;"
								class="fieldLabel ">N. Utilizzazioni da Riprocessare</td>
							<td class="listViewEntryValue ">{{ctrl.dettailSelected.utilizzazioniDaRiprocessare}}</td>

							<td
								style="color: #444; font-weight: bold; font-size: 16px !important;"
								class="fieldLabel ">N. Utilizzazioni Riprocessate</td>
							<td class="listViewEntryValue ">{{ctrl.dettailSelected.utilizzazioniRiprocessati}}</td>

							<td
								style="color: #444; font-weight: bold; font-size: 16px !important;"
								class="fieldLabel ">% Utilizzazioni Processate</td>
							
							<td class="listViewEntryValue ">{{calcolaPercentuale(ctrl.dettailSelected.utilizzazioniRiprocessati,ctrl.dettailSelected.utilizzazioniDaRiprocessare)}}</td>
						</tr>

						 <tr class="listViewEntries">
							<td
								style="color: #444; font-weight: bold; font-size: 16px !important;"
								class="fieldLabel ">Report Aggiornati</td>
							<td class="listViewEntryValue ">{{ctrl.dettailSelected.reportAggiornati}} di {{ctrl.dettailSelected.reportRiprocessati}}</td>

							<td
								style="color: #444; font-weight: bold; font-size: 16px !important;"
								class="fieldLabel "></td>
							<td class="listViewEntryValue "></td>

							<td
								style="color: #444; font-weight: bold; font-size: 16px !important;"
								class="fieldLabel "></td>
							<td class="listViewEntryValue "></td>
						</tr>
					</tbody>
				</table>
				<br> <br> Esito Lavorazione
				<table class="table table-bordered listViewEntriesTable">

					<tbody>
						<tr class="listViewEntries">
							<td
								style="color: #444; font-weight: bold; font-size: 16px !important;"
								class="fieldLabel ">N. Record Codificati</td>
							<td class="listViewEntryValue ">{{ctrl.dettailSelected.recordCodificati}}</td>
							<td
								style="color: #444; font-weight: bold; font-size: 16px !important;"
								class="fieldLabel ">N. Utilizzazioni Codificate</td>
							<td class="listViewEntryValue ">{{ctrl.dettailSelected.utilizzazioniCodificate}}</td>
						</tr>
						
						<tr class="listViewEntries">
							<td
								style="color: #444; font-weight: bold; font-size: 16px !important;"
								class="fieldLabel ">N. Record Codificati Regolari</td>
							<td class="listViewEntryValue ">{{ctrl.dettailSelected.recordCodificatiRegolari}}</td>
							<td
								style="color: #444; font-weight: bold; font-size: 16px !important;"
								class="fieldLabel ">N. Utilizzazioni Codificate Regolari</td>
							<td class="listViewEntryValue ">{{ctrl.dettailSelected.utilizzazioniCodificateRegolari}}</td>
						</tr>
					
					</tbody>
				</table>
				<br> <br> Report Rilavorati
				<div class="listViewEntriesDiv contents-bottomscroll">
					<div class="bottomscroll-div" style="width: 95%; min-height: 80px">

						<table class="table table-bordered listViewEntriesTable ">
							<thead>
								<tr class="listViewHeaders">
									<th><a href="" class="listViewHeaderValues">Data<br>Codifica
									</a></th>
									<th><a href="" class="listViewHeaderValues">Licenziatario</a></th>
									<th><a href="" class="listViewHeaderValues">Codice
											Licenza </a></th>
									<th><a href="" class="listViewHeaderValues">Tipologia
											di<br>Servizio
									</a></th>
									<th><a href="" class="listViewHeaderValues">Periodo<br>
											competenza
									</a></th>
									<th><a href="" class="listViewHeaderValues">Modalità<br>utilizzazione
									</a></th>
									<th><a href="" class="listViewHeaderValues">Modalità<br>fruizione
									</a></th>
									<th><a href="" class="listViewHeaderValues">In<br>Abbonamento
									</a></th>
									<th><a href="" class="listViewHeaderValues">Stato</a></th>
									<th><a href="" class="listViewHeaderValues">% Codificato Precedente </a></th>
									<th><a href="" class="listViewHeaderValues">% Codificato </a></th>
									<th><a href="" class="listViewHeaderValues">Periodi <br>di Ripartizione
									</a></th>
								</tr>
							</thead>
							<tbody>

								<tr class="listViewEntries " ng-repeat="item in ctrl.reportRilavorati.rows">
									<td class="listViewEntryValue "><span
										ng-hide="item.posizioneReportCodificato==null&&item.posizioneReportCodificato==undefined"
										ng-click="downloadReport(item,'CODIFICATO')"
										class="	glyphicon glyphicon-cloud-download"></span></span>
										&nbsp;{{formatDate(item.dataCodifica)}}</td>
									<td class="listViewEntryValue ">{{item.nominativoLicenziatario}}</td>
									<td class="listViewEntryValue ">{{item.licenza}}</td>
									<td class="listViewEntryValue ">{{item.servizio}}</td>
									<td class="listViewEntryValue ">{{item.origPeriodoCompetenza}}</td>
									<td class="listViewEntryValue ">{{item.modUtilizzazione}}</td>
									<td class="listViewEntryValue ">{{item.modFruizione}}</td>
									<td class="listViewEntryValue ">{{getTextFromBool(item.inAbbonamento)}}</td>
									<td class="listViewEntryValue ">{{item.statoLogico}}</td>
									<td class="listViewEntryValue ">{{formatPercent(item.vecchioCodificatoUtilizzazioni)}}</td>
									<td class="listViewEntryValue ">{{formatPercent(item.codificatoUtilizzazioni)}}</td>
									<td class="listViewEntryValue ">{{item.periodoRipartizione}}</td>

								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>


	</div>


</div>