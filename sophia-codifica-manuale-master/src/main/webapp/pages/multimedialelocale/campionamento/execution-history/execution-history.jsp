<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@
	include file="../../../navbar.jsp"%>

<div class="bodyContents">
	<div class="mainContainer row-fluid">

		<div id="companyLogo" class="navbar commonActionsContainer noprint">
			<div class="actionsContainer row-fluid">
				<div class="span2">
					<!--  -- >
					<span class="companyLogo"><img src="images/siae_logo_siaetrasparente.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
					<!--  -->
					<span class="companyLogo"><img src="images/logo_siae.jpg"
						title="SIAE" alt="SIAE">&nbsp;</span>
				</div>
				<span class="btn-toolbar span4">
					<div class="pageNumbers alignTop pull-right"
						style="text-align: center;">
						<span class="pageNumbersText"><strong>Storico
								Esecuzioni</strong></span>
					</div>
				</span>
			</div>
		</div>

		<div class="contentsDiv marginLeftZero" id="rightPanel"
			style="min-height: 80px;">
			<div class="listViewPageDiv">

				<!-- contenitore principale -->
				<div class="listViewActionsDiv row-fluid">
					<table
						class="table table-bordered blockContainer showInlineTable equalSplit">
						<thead>
							<tr>
								<th class="blockHeader" colspan="6" ng-show="ctrl.hideForm"><span
									ng-click="ctrl.hideForm=false"
									class="glyphicon glyphicon-expand"></span>&nbsp;Parametri
									Ricerca</th>
								<th class="blockHeader" colspan="6" ng-hide="ctrl.hideForm"><span
									ng-click="ctrl.hideForm=true"
									class="glyphicon glyphicon-collapse-down"></span>&nbsp;Parametri
									Ricerca</th>
							</tr>
						</thead>
						<tbody ng-hide="ctrl.hideForm">
							<tr>

								<td class="fieldLabel medium"><label
									class="muted pull-right marginRight10px"> Periodo da </label></td>
								<td>
									<div class="controls">
										<div class="input-append date" id="datepickerExecutionFrom"
											data-date-format="mm-yyyy">

											<input type="text" name="date"
												ng-model="ctrl.selectedContabilityDateFrom"
												style="width: 125px; height: 23px"> <span
												class="add-on" style="height: 23px"><i
												class="icon-th"></i></span>
										</div>
										<script type="text/javascript">
											$("#datepickerExecutionFrom")
												.datepicker(
													{
														format : 'mm-yyyy',
														language : 'it',
														viewMode : "months",
														minViewMode : "months",
														orientation : "bottom auto",
														autoclose : "true"
													});
										</script>
									</div>
								</td>

								<td class="fieldLabel medium"><label
									class="muted pull-right "> <span class="redColor"
										ng-show="form.process.$invalid">* </span>Periodo a
								</label></td>
								<td>
									<div class="controls">
										<div class="input-append date" id="datepickerExecutionTo"
											data-date-format="mm-yyyy">

											<input type="text" name="date"
												ng-model="ctrl.selectedContabilityDateTo"
												style="width: 125px; height: 23px"> <span
												class="add-on" style="height: 23px"><i
												class="icon-th"></i></span>
										</div>
										<script type="text/javascript">
											$("#datepickerExecutionTo")
												.datepicker(
													{
														format : 'mm-yyyy',
														language : 'it',
														viewMode : "months",
														minViewMode : "months",
														orientation : "bottom auto",
														autoclose : "true"
													});
										</script>
									</div>
								</td>
							<tr>
								<td class="fieldLabel medium" style="text-align: right" nowrap
									colspan="6">
									<button
										ng-hide="ctrl.selectedContabilityDateFrom==undefined||ctrl.selectedContabilityDateTo==undefined"
										class="btn addButton" ng-click="filterApply()">
										<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Applica</strong>
									</button>
								</td>

							</tr>
						</tbody>
					</table>

				</div>

				<div>
					<!-- actions and pagination -->
					<!-- actions and pagination -->
					<div class="listViewTopMenuDiv noprint">
						<div class="listViewActionsDiv row-fluid">
							<!--  -->
							<span class="btn-toolbar span4"> <span class="btn-group">

							</span>
							</span>
							<!--  -->
							<!--  -->
							<span class="btn-toolbar span4"> <span
								class="customFilterMainSpan btn-group"
								style="text-align: center;">
									<div class="pageNumbers alignTop ">
										<span class="pageNumbersText"><strong> <!-- Titolo -->
										</strong></span>
									</div>
							</span>
							</span>
							<!--  -->
							<!--  -->
							<span class="span4 btn-toolbar">
								<div class="listViewActions pull-right">

									<div class="pageNumbers alignTop ">
										<span ng-show="ctrl.totRecords > 50"> <span
											class="pageNumbersText" style="padding-right: 5px">
												Record da <strong>{{ctrl.page*50}}</strong> a <strong>{{(ctrl.page*50)+ctrl.execution.length}}</strong>
										</span>
											<button title="Precedente" class="btn"
												id="listViewPreviousPageButton" type="button"
												ng-click="navigateToPreviousPage()" ng-show="ctrl.page > 0">
												<span class="glyphicon glyphicon-chevron-left"></span>
											</button>
											<button title="Successiva" class="btn"
												id="listViewNextPageButton" type="button"
												ng-click="navigateToNextPage()"
												ng-show="ctrl.page < ctrl.pageTot">
												<span class="glyphicon glyphicon-chevron-right"></span>
											</button>
										</span>
									</div>
								</div>
								<div class="clearfix"></div>
							</span>
							<!--  -->
						</div>
					</div>


					<!-- table -->
					<div class="listViewContentDiv" id="listViewContents">

						<div class="contents-topscroll noprint">
							<div class="topscroll-div" style="width: 95%">&nbsp;</div>
						</div>

						<div class="listViewEntriesDiv contents-bottomscroll">
							<div class="bottomscroll-div"
								style="width: 95%; min-height: 80px">

								<table class="table table-bordered listViewEntriesTable">
									<thead>
										<tr class="listViewHeaders">
											<th><a href="" class="listViewHeaderValues">Data e
													Ora </a></th>
											<th><a href="" class="listViewHeaderValues">Servizio
											</a></th>
											<th><a href="" class="listViewHeaderValues">Utente</a></th>
											<th><a href="" class="listViewHeaderValues">Dettagli</a></th>
										</tr>
									</thead>
									<tbody>

										<tr class="listViewEntries" ng-repeat="item in ctrl.execution">

											<td class="listViewEntryValue ">{{formatDate(item.dataInserimento)}}</td>
											<td class="listViewEntryValue ">{{item.serviceName}}</td>
											<td class="listViewEntryValue ">{{item.userName}}</td>
											<td class="listViewEntryValue ">{{item.message}}</td>

										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>