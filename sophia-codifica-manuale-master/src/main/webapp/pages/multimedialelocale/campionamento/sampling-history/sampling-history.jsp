<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@
	include file="../../../navbar.jsp"%>

<div class="bodyContents">
	<div class="mainContainer row-fluid">

		<div id="companyLogo" class="navbar commonActionsContainer noprint">
			<div class="actionsContainer row-fluid">
				<div class="span2">
					<!--  -- >
					<span class="companyLogo"><img src="images/siae_logo_siaetrasparente.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
					<!--  -->
					<span class="companyLogo"><img src="images/logo_siae.jpg"
						title="SIAE" alt="SIAE">&nbsp;</span>
				</div>
				<span class="btn-toolbar span4">
					<div class="pageNumbers alignTop pull-right"
						style="text-align: center;">
						<span class="pageNumbersText"><strong>Storico
								Campionamento</strong></span>
					</div>
				</span>
			</div>
		</div>

		<div class="contentsDiv marginLeftZero" id="rightPanel"
			style="min-height: 80px;">
			<div class="listViewPageDiv">

				<!-- contenitore principale -->
				<div class="listViewActionsDiv row-fluid">
					<table
						class="table table-bordered blockContainer showInlineTable equalSplit">
						<thead>
							<tr>
								<th class="blockHeader" colspan="6" ng-show="ctrl.hideForm"><span
									ng-click="ctrl.hideForm=false"
									class="glyphicon glyphicon-expand"></span>&nbsp;Parametri
									Ricerca</th>
								<th class="blockHeader" colspan="6" ng-hide="ctrl.hideForm"><span
									ng-click="ctrl.hideForm=true"
									class="glyphicon glyphicon-collapse-down"></span>&nbsp;Parametri
									Ricerca</th>
							</tr>
						</thead>
						<tbody ng-hide="ctrl.hideForm">
							<tr>

								<td class="fieldLabel medium"><label
									class="muted pull-right marginRight10px">
										Contabilit&agrave da </label></td>
								<td>
									<div class="controls">
										<div class="input-append date" id="datepickerContabilityHistoryFrom"
											data-date-format="mm-yyyy">

											<input type="text" name="date"
												ng-model="ctrl.selectedContabilityDateFrom"
												style="width: 125px; height: 23px"> <span
												class="add-on" style="height: 23px"><i
												class="icon-th"></i></span>
										</div>
										<script type="text/javascript">
											$("#datepickerContabilityHistoryFrom")
												.datepicker(
													{
														format : 'mm-yyyy',
														language : 'it',
														viewMode : "months",
														minViewMode : "months",
														orientation : "bottom auto",
														autoclose : "true"
													});
										</script>
									</div>
								</td>

								<td class="fieldLabel medium"><label
									class="muted pull-right "> <span class="redColor"
										ng-show="form.process.$invalid">* </span>Contabilit&agrave a
								</label></td>
								<td>
									<div class="controls">
										<div class="input-append date" id="datepickerContabilityHistoryTo"
											data-date-format="mm-yyyy">

											<input type="text" name="date"
												ng-model="ctrl.selectedContabilityDateTo"
												style="width: 125px; height: 23px"> <span
												class="add-on" style="height: 23px"><i
												class="icon-th"></i></span>
										</div>
										<script type="text/javascript">
											$("#datepickerContabilityHistoryTo")
												.datepicker(
													{
														format : 'mm-yyyy',
														language : 'it',
														viewMode : "months",
														minViewMode : "months",
														orientation : "bottom auto",
														autoclose : "true"
													});
										</script>
									</div>
								</td>
							<tr>
								<td class="fieldLabel medium" style="text-align: right" nowrap
									colspan="6">
									<button
										ng-hide="ctrl.selectedContabilityDateFrom==undefined||ctrl.selectedContabilityDateTo==undefined"
										class="btn addButton" ng-click="filterApply()">
										<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Applica</strong>
									</button>
								</td>

							</tr>
						</tbody>
					</table>

				</div>

				<div>
					<!-- actions and pagination -->
					<!-- actions and pagination -->
					<div class="listViewTopMenuDiv noprint">
						<div class="listViewActionsDiv row-fluid">
							<!--  -->
							<span class="btn-toolbar span4"> <span class="btn-group">

							</span>
							</span>
							<!--  -->
							<!--  -->
							<span class="btn-toolbar span4"> <span
								class="customFilterMainSpan btn-group"
								style="text-align: center;">
									<div class="pageNumbers alignTop ">
										<span class="pageNumbersText"><strong> <!-- Titolo -->
										</strong></span>
									</div>
							</span>
							</span>
							<!--  -->
							<!--  -->
							<span class="span4 btn-toolbar">
								<div class="listViewActions pull-right">

									<div class="pageNumbers alignTop ">
										<span ng-show="ctrl.totRecords > 50">
											<span class="pageNumbersText" style="padding-right: 5px">
												Record da <strong>{{ctrl.page*50}}</strong> a <strong>{{(ctrl.page*50)+ctrl.sampling.length}}</strong>
										</span>
											<button title="Precedente" class="btn"
												id="listViewPreviousPageButton" type="button"
												ng-click="navigateToPreviousPage()"
												ng-show="ctrl.page > 0">
												<span class="glyphicon glyphicon-chevron-left"></span>
											</button>
											<button title="Successiva" class="btn"
												id="listViewNextPageButton" type="button"
												ng-click="navigateToNextPage()" ng-show="ctrl.page < ctrl.pageTot">
												<span class="glyphicon glyphicon-chevron-right"></span>
											</button>
										</span>
									</div>
								</div>
								<div class="clearfix"></div>
							</span>
							<!--  -->
						</div>
					</div>


					<!-- table -->
					<div class="listViewContentDiv" id="listViewContents">

						<div class="contents-topscroll noprint">
							<div class="topscroll-div" style="width: 95%">&nbsp;</div>
						</div>

						<div class="listViewEntriesDiv contents-bottomscroll">
							<div class="bottomscroll-div"
								style="width: 95%; min-height: 80px">

								<table class="table table-bordered listViewEntriesTable">
									<thead>
										<tr class="listViewHeaders">
											<th><a href="" class="listViewHeaderValues">Contabilit&agrave</a></th>
											<th><a href="" class="listViewHeaderValues">InizioLavorazione
											</a></th>
											<th><a href="" class="listViewHeaderValues">EstrazioneLotto</a></th>
											<th><a href="" class="listViewHeaderValues"></a></th>
											<th><a href="" class="listViewHeaderValues"></a></th>
											<th><a href="" class="listViewHeaderValues">Resto
													Modulo 5</a></th>
											<th><a href="" class="listViewHeaderValues">Resto
													Modulo 61</a></th>
											<th><a href="" class="listViewHeaderValues">Attiva</a></th>
											<th><a href="" class="listViewHeaderValues">Ultima
													Modifica</a></th>
											<th><a href="" class="listViewHeaderValues">Utente</a></th>
										</tr>
									</thead>
									<tbody>

										<tr class="listViewEntries" ng-repeat="item in ctrl.sampling">

											<td style="padding: 10px !important;"
												class="listViewEntryValue ">{{formatContability(item.contabilita)}}</td>
											<td style="padding: 10px !important;"
												class="listViewEntryValue ">{{formatDate(item.dataInizioLavorazione)}}</td>
											<td style="padding: 10px !important;"
												class="listViewEntryValue ">{{formatDate(item.dataEstrazioniLotto)}}</td>
											<td style="padding: 10px !important;"
												class="listViewEntryValue " data-field-type="string">
												<table border="1" style="border-color: #575e78;">
													<tr style="border-color: #575e78;">
														<td style="border-color: #575e78;" height="25"
															align="center" valign="center">BSM</td>
														<td style="border-color: #575e78;" height="25"
															align="center" valign="center">ROMA</td>
													</tr>
													<tr style="border: thin #575e78">
														<td style="border-color: #575e78;" height="25"
															align="center" valign="center">CONCERTINI</td>
														<td style="border-color: #575e78;" height="25"
															align="center" valign="center">MILANO</td>
													</tr>
												</table>
											</td>
											<td style="padding: 10px !important;"
												class="listViewEntryValue ">
												<table border="1" style="border-color: #575e78;">
													<tr style="border-color: #575e78;">
														<td style="text-align: center; border-color: #575e78;"
															height="25" width="25" align="center" valign="center">{{item.estrazioniRoma}}</td>
													</tr>
													<tr style="border: thin #575e78">
														<td style="text-align: center; border-color: #575e78;"
															height="25" width="25" align="center" valign="center">{{item.estrazioniMilano}}</td>
													</tr>
												</table>
											</td>
											<td style="padding: 10px !important;"
												class="listViewEntryValue ">
												<table border="1" style="border-color: #575e78;">
													<tr style="border-color: #575e78;">
														<td style="text-align: center; border-color: #575e78;"
															height="25" width="25" align="center" valign="center">{{item.restoRoma1}}</td>
													</tr>
													<tr style="border: thin #575e78">
														<td style="text-align: center; border-color: #575e78;"
															height="25" width="25" align="center" valign="center">{{item.restoMilano1}}</td>
													</tr>
												</table>
											</td>

											<td style="padding: 10px !important;"
												class="listViewEntryValue ">
												<table border="1" style="border-color: #575e78;">
													<tr style="border-color: #575e78;">
														<td style="text-align: center; border-color: #575e78;"
															height="25" width="25" align="center" valign="center">{{item.restoRoma2}}</td>
													</tr>
													<tr style="border: thin #575e78">
														<td style="text-align: center; border-color: #575e78;"
															height="25" width="25" align="center" valign="center">{{item.restoMilano2}}</td>
													</tr>
												</table>
											</td>
											<td class="listViewEntryValue ">{{item.flagAttivo=="Y" ?
												"ATTIVA" : "DISATTIVATA"}}</td>

											<td style="padding: 10px !important;"
												class="listViewEntryValue ">{{formatDate(item.dataOraUltimaModifica)}}</td>

											<td style="padding: 10px !important;"
												class="listViewEntryValue ">{{item.utenteUltimaModifica}}</td>

										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>