 <%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@
	include file="../../../navbar.jsp"%>

<script>
    // Gestione Progress Bar - Start

    $(function() {

        var progressbar = $( "#progressbar" ),
          progressLabel = $( ".progress-label" ),
          executeButton = $( "#executeButton" )
            .button()
            .on( "click", function() {

              $( this ).button( "option", {
                disabled: true,
                label: "Esecuzione in corso..."
              });

              var periodoContabileId = $("#periodoContabileId").val();
              
              if (periodoContabileId==null || periodoContabileId==""){

                alert("Attenzione !\nSpecificare il mese contabile da campionare.");

                $( "#executeButton" ).button( "option", {
                    disabled: false,
                    label: "Inizia campionamento"
                });

              } else {

                  //var jqxhr = $.get( "rest/performing/sendCommand?info=START_CAMPIONAMENTO&periodoContabile="+periodoContabileId, function( value ) {

                       progressLabel.text( "Loading..." );

                       $("#elaborazioneValue #totValue").text("0");
                       $("#elaborazioneValue #elabValue").text("0");
                       $("#elaborazioneValue #sospValue").text("0");
                       $("#elaborazioneValue #errValue").text("0");

                       setTimeout( progress, 2000 );

                       $("#suspendDiv").show();
                       $("#resumeDiv").hide();

                  //});

              }

            });

        progressbar.progressbar({
          value: false,
          change: function() {
            progressLabel.text( progressbar.progressbar( "value" ) + "%" );
          },
          complete: function() {
            progressLabel.text( "Complete!" );

            $( "#executeButton" ).button( "option", {
                disabled: false,
                label: "Inizia campionamento"
            });

          }
        });

        function progress() {

                var jqxhr = $.get( "rest/performing/campionamento/getAvanzamentoElaborazioneCampionamento", function( value ) {

                      var val = progressbar.progressbar( "value" ) || 0;

                      $("#progressbar").progressbar( "value", value.percentuale);

                      $("#elaborazioneValue #totValue").text(value.totalePM);
                      $("#elaborazioneValue #elabValue").text(value.totaleElaborati);
                      $("#elaborazioneValue #sospValue").text(value.totaleSospesi);
                      $("#elaborazioneValue #errValue").text(value.totaleErrori);

                      if ( val < 100 ) {
                        setTimeout( progress, 2000 );
                      }else{
                            $( "#executeButton" ).button( "option", {
                                disabled: false,
                                label: "Inizia campionamento"
                            });
                      }
                });
        }

  });

  // Gestione Progress Bar - End


  function callActivateDeactivate(status){

        var request = $.ajax({
             url: "rest/performing/campionamento/sendCommand?info="+status,
             type: "GET"
        });

        request.done(function(value) {

          if (value == "ACTIVE"){

            $("#activeArrow").show();
            $("#inactiveArrow").hide();

            $("#activateDiv").hide();
            $("#inactiveDiv").show();

            $("#periodoContabileId").attr("disabled", false);

            $("#statoProcessoDiv #statoProcesso").text("Attivo");

            $("#executeButton").button("option", {
             disabled: false
            });

            $("#elaborazioneLabel").show();
            $("#elaborazioneValue").show();

             var jqxhr = $.get( "rest/performing/campionamento/getAvanzamentoElaborazione", function( value ) {

               $("#elaborazioneValue #totValue").text(value.totalePM);
               $("#elaborazioneValue #elabValue").text(value.totaleElaborati);
               $("#elaborazioneValue #sospValue").text(value.totaleSospesi);
               $("#elaborazioneValue #errValue").text(value.totaleErrori);

            });


          } else
          if (value == "INACTIVE"){
            $("#activeArrow").hide();
            $("#inactiveArrow").show();

            $("#activateDiv").show();
            $("#inactiveDiv").hide();

            $("#periodoContabileId").attr("disabled", true);

            $("#statoProcessoDiv #statoProcesso").text("Inattivo");

            $("#executeButton").button("option", {
             disabled: true
            });

            $("#elaborazioneLabel").hide();
            $("#elaborazioneValue").hide();

          }else{
              alert(value);
          }

        });

        request.fail(function(jqXHR, textStatus) {
           alert( "Request failed: " + textStatus );
        });
   }

  function callSuspendeResume(status){

        //suspendDiv      resumeDiv
        //suspendButton   resumeButton
        //SUSPENDE        RESUME

        var request = $.ajax({
             url: "rest/performing/campionamento/sendCommand?info="+status,
             type: "GET"
        });

        request.done(function(value) {

          if (value == "SUSPENDED"){

            $("#suspendDiv").hide();
            $("#resumeDiv").show();

          } else
          if (value == "RESUMED"){

            $("#suspendDiv").show();
             $("#resumeDiv").hide();

          }else{
              alert(value);
          }

        });

        request.fail(function(jqXHR, textStatus) {
           alert( "Request failed: " + textStatus );
        });
   }
  
  $(document).ready(function(){

      var request = $.ajax({
           url: "rest/performing/campionamento/sendCommand?info=STATUS",
           type: "GET"
      });

      request.done(function(value) {
        if (value == "ACTIVE"){

          $("#activeArrow").show();
          $("#inactiveArrow").hide();

          $("#activateDiv").hide();
          $("#inactiveDiv").show();

          $("#statoProcessoDiv #statoProcesso").text("Attivo");

          $("#executeButton").attr("disabled", false);
          $("#periodoContabileId").attr("disabled", false);

          $("#executeButton").button("option", {
           disabled: false
          });

          $("#elaborazioneLabel").show();
          $("#elaborazioneValue").show();

          var jqxhr = $.get( "rest/performing/campionamento/getAvanzamentoElaborazioneCampionamento", function( value ) {

             $("#elaborazioneValue #totValue").text(value.totalePM);
             $("#elaborazioneValue #elabValue").text(value.totaleElaborati);
             $("#elaborazioneValue #sospValue").text(value.totaleSospesi);
             $("#elaborazioneValue #errValue").text(value.totaleErrori);

              //if (value.totalePM>0 && value.totaleElaborati>0 && value.){
              //    $('#executeButton').click();
              //}


          });

        } else
        if (value == "INACTIVE"){
          $("#activeArrow").hide();
          $("#inactiveArrow").show();

          $("#activateDiv").show();
          $("#inactiveDiv").hide();

          $("#periodoContabileId").attr("disabled", true);

          $("#statoProcessoDiv #statoProcesso").text("Inattivo");

          $("#executeButton").button("option", {
           disabled: true
          });

          $("#elaborazioneLabel").hide();
          $("#elaborazioneValue").hide();

        }else{
            alert(value);
        }

      }); 

      request.fail(function(jqXHR, textStatus) {
         alert( "Request failed: " + textStatus );
      });


  });
  </script>

<div class="bodyContents">
	<div class="mainContainer row-fluid">

		<div id="companyLogo" class="navbar commonActionsContainer noprint">
			<div class="actionsContainer row-fluid">
				<div class="span2">
					<!--  -- >
					<span class="companyLogo"><img src="images/siae_logo_siaetrasparente.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
					<!--  -->
					<span class="companyLogo"><img src="images/logo_siae.jpg"
						title="SIAE" alt="SIAE">&nbsp;</span>
				</div>
				<span class="btn-toolbar span4">
					<div class="pageNumbers alignTop pull-right" style="text-align: center;">
						<span class="pageNumbersText"><strong>Esecuzione
								Campionamento</strong></span>
					</div>
				</span>
			</div>
		</div>


		<form class="form-horizontal recordEditView" id="RicalcoloFormId" name="RicalcoloForm" method="post" action="${contextPath}/secure/ricalcoloEsecuzioneSave">

    </form>


        <table class="table table-bordered blockContainer showInlineTable equalSplit">
            <thead>
            <tr>
                <th class="blockHeader" colspan="4">Controllo esecuzione</th>
            </tr>
            </thead>
            <tbody style="display: table-row-group;">

            <tr>
                <td class="fieldLabel medium">
                    <label class="muted pull-right marginRight10px">
                        <!-- Contenuto -->
                        <div id="activateDiv" style="display:none" align="center">
                            <button class="btn addButton" id="startButton" onclick="callActivateDeactivate('ACTIVATE');">Start</button>
                        </div>
                        <div id="inactiveDiv" style="display:none" align="center">
                            <button class="btn addButton" id="stopButton" onclick="callActivateDeactivate('DEACTIVATE');">Stop</button>
                        </div>
                    </label>
                </td>
                <td class="fieldValue medium">
                    <div class="row-fluid">
                        <span class="span10">
                            <div class="input-append row-fluid">
                                <div class="span12 row-fluid date">
                                    <!-- Contenuto -->
                                    <div id="activeArrow" style="display:none" align="center">
                                       <img src="images/green-arrow.png" height="30" width="30" alt="Processo attivo" title="Stato processo"/>
                                    </div>
                                    <div id="inactiveArrow" style="display:none" align="center">
                                        <img src="images/red-arrow.png" height="30" width="30" alt="Processo attivo" title="Stato processo"/>
                                    </div>
                                </div>
                            </div>
                        </span>
                    </div>
                </td>

                <td class="fieldLabel medium">
                   <label class="muted pull-right marginRight10px">Stato processo</label>
                </td>
                <td class="fieldValue medium">
                    <div class="row-fluid">
                        <span class="span10">
                            <div class="input-append row-fluid">
                                <div class="span12 row-fluid date">
                                    <!-- Contenuto -->
                                    <div id="statoProcessoDiv" class="row-fluid">
                                       <span id="statoProcesso" class="span10 pull-left"></span>
                                    </div>
                                </div>
                            </div>
                        </span>
                    </div>
                </td>
            </tr>

            <tr>
                <td class="fieldLabel medium">
                    <label class="muted pull-right marginRight10px">
                        <!-- Contenuto -->
                        <button class="btn addButton" id="executeButton" data-ng-click="executeCampionamento()">Inizia campionamento</button>
                        <!--
                        <div id="suspendDiv" style="display:none" align="center">
                            <button id="suspendButton" onclick="callSuspendeResume('SUSPENDE');">Sospendi</button>
                        </div>
                        <div id="resumeDiv" style="display:none" align="center">
                            <button id="resumeButton" onclick="callSuspendeResume('RESUME');">Riprendi</button>
                        </div>
                         -->
                    </label>
                </td>
                <td class="fieldValue medium">
                    <div class="row-fluid">
                        <span class="span10">
                            <div class="input-append row-fluid">

                            <!--<select class="js-basic-single-select" name="periodoContabile" id="periodoContabileId" style="width: 220px; display: block;" required="true">
                                <option value="" class="globalSearch_module_All">- Mese contabile-</option>
                                 <c:forEach items="${resultPage.records}" var="c">  
                                    <option value="${c.id}" class="globalSearch_module_All">
                                            ${c.descrizioneContabilita}
                                    </option>
                                </c:forEach>-->
                                <select id="periodoContabileId" class="js-basic-single-select" name="periodoContabile" style="width: 220px; display: block;" required="true">
                                	<option></option>
                                	<option data-ng-repeat="record in records" id="contabilitaId">{{record.contabilita}}</option>
                                </select>
                            <!-- </select> -->

                                <div class="span12 row-fluid date">
                                    <!-- Contenuto -->
                                        <div id="progressbar"><div></div></div>
                                </div>
                            </div>
                        </span>
                    </div>
                </td>
                <td class="fieldLabel medium">
                  <div class="muted pull-right marginRight10px" id="elaborazioneLabel" style="display:none">
                    <label class="muted pull-right marginRight10px">Numero PM totali</label><br/>
                    <label class="muted pull-right marginRight10px">Numero PM elaborati</label><br/>
                  </div>
                </td>
                <td class="fieldValue medium">
                    <div id="elaborazioneValue" style="display:none" class="row-fluid">
                        <span id="totValue" class="pull-left"></span><br/>
                        <span id="elabValue" style="margin-top:2px;" class="pull-left"></span><br/>
                    </div>
                </td>
            </tr>
</tbody>
            </table>
            </div>
            </div>