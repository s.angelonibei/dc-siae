<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@
	include file="../navbar.jsp"%>

<div class="bodyContents">

	<div class="mainContainer row-fluid">

		<div id="companyLogo" class="navbar commonActionsContainer noprint">
			<div class="actionsContainer row-fluid">
				<div class="span2">
					<span class="companyLogo"><img src="images/logo_siae.jpg"
						title="SIAE" alt="SIAE">&nbsp;</span>
				</div>
				<span class="btn-toolbar span4">
					<div class="pageNumbers alignTop pull-right"
						style="text-align: center;">
						<span class="pageNumbersText"><strong>Visualizza Report</strong></span>
					</div>
				</span>
			</div>
		</div>

		<div class="contentsDiv marginLeftZero" id="rightPanel"
			style="min-height: 180px;">
			<div
				ng-hide="ctrl.dettailSelected!=null&&ctrl.dettailSelected!=undefined">
				<div class="listViewPageDiv">

					<div class="listViewTopMenuDiv noprint">
						<div class="listViewTopMenuDiv noprint">
							<div class="listViewActionsDiv row-fluid">

								<span class="btn-toolbar span4"> <span class="btn-group">

								</span>
								</span> <span class="btn-toolbar span4"> <span
									class="customFilterMainSpan btn-group"
									style="text-align: center;">
										<div class="pageNumbers alignTop "></div>
								</span>
								</span> <span class="span4 btn-toolbar">
									<div class="listViewActions pull-right">
										<!-- span class="btn-group">
										<button class="btn addButton" ng-click="showUpload($event)">
											<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;<strong>Nuovo</strong>
										</button>
									</span> -->
									</div>
									<div class="clearfix"></div>
								</span>

							</div>
						</div>
					</div>

					<!-- table -->
					<div class="listViewContentDiv" id="listViewContents">

						<!--  -- >
					 <div class="contents-topscroll noprint">
					 	<div class="topscroll-div" style="width: 95%;">&nbsp;</div>
					 </div>
<!--  -->
						<div class="listViewEntriesDiv">
							<div style="width: 100%; min-height: 400px">

								<table
									class="table table-bordered blockContainer showInlineTable ">
									<thead>
										<tr>
											<th class="blockHeader" colspan="12" ng-hide="ctrl.hideForm">
												Parametri di Ricerca</th>
										</tr>
									</thead>
							
									<tbody ng-hide="ctrl.hideForm">

										<tr>
											<td class="fieldLabel medium"><label
												class="muted pull-right marginRight10px"> <span
													class="redColor" ng-show="form.process.$invalid">*</span>Data
													invio da
											</label></td>
											<td class="fieldLabel medium" nowrap><input type="text"
												class="span9 input-small datepickerInvioReportMlFrom"
												style="width: 180px" ng-model="ctrl.selectedPeriodSendFrom">
												<script type="text/javascript">
													$(".datepickerInvioReportMlFrom")
															.datepicker(
																	{
																		format : 'dd-mm-yyyy',
																		language : 'it',
																		viewMode : "days",
																		minViewMode : "days",
																		autoclose : true,
																		todayHighlight : true
																	});
												</script> <span class="glyphicon glyphicon-erase"
												style="margin-top: 6px;"
												ng-click="ctrl.selectedPeriodSendFrom=undefined;"></span></td>

											<td class="fieldLabel medium"><label
												class="muted pull-right marginRight10px"> <span
													class="redColor" ng-show="form.process.$invalid">*</span>Data
													invio a
											</label></td>
											<td class="fieldLabel medium" nowrap><input type="text"
												class="span9 input-small datepickerInvioReportMlTo"
												style="width: 180px" ng-model="ctrl.selectedPeriodSendTo">
												<script type="text/javascript">
													$(".datepickerInvioReportMlTo")
															.datepicker(
																	{
																		format : 'dd-mm-yyyy',
																		language : 'it',
																		viewMode : "days",
																		minViewMode : "days",
																		autoclose : true,
																		todayHighlight : true
																	});
												</script> <span class="glyphicon glyphicon-erase"
												style="margin-top: 6px;"
												ng-click="ctrl.selectedPeriodSendTo=undefined;"></span></td>
												
											<td class="fieldLabel medium" nowrap colspan="4"></td>
												
										</tr>
										<tr>
											<td class="fieldLabel medium"><label
												class="muted pull-right marginRight10px"> <span
													class="redColor" ng-show="form.process.$invalid">*</span>Periodo di competenza
													da
											</label></td>
											<td class="fieldLabel medium" nowrap><input type="text"
												class="span9 input-small datepickerFromReportMl"
												style="width: 180px" ng-model="ctrl.selectedPeriodFrom">
												<script type="text/javascript">
													$(".datepickerFromReportMl")
															.datepicker(
																	{
																		format : 'dd-mm-yyyy',
																		language : 'it',
																		viewMode : "days",
																		minViewMode : "days",
																		autoclose : true,
																		todayHighlight : true
																	});
												</script> <span class="glyphicon glyphicon-erase"
												style="margin-top: 6px;"
												ng-click="ctrl.selectedPeriodFrom=undefined;"></span></td>

											<td class="fieldLabel medium"><label
												class="muted pull-right marginRight10px"> <span
													class="redColor" ng-show="form.process.$invalid">*</span>Periodo di competenza
													a
											</label></td>
											<td class="fieldLabel medium"><input type="text"
												class="span9 input-small datepickerToReportMl"
												style="width: 180px" ng-model="ctrl.selectedPeriodTo">
												<script type="text/javascript">
													$(".datepickerToReportMl")
															.datepicker(
																	{
																		format : 'dd-mm-yyyy',
																		language : 'it',
																		viewMode : "days",
																		minViewMode : "days",
																		autoclose : true,
																		todayHighlight : true
																	});
												</script> <span class="glyphicon glyphicon-erase"
												style="margin-top: 6px;"
												ng-click="ctrl.selectedPeriodTo=undefined;"></span></td>

												<td class="fieldLabel medium"><label
												class="muted pull-right "> 
													<span class="redColor" ng-show="form.process.$invalid">* </span>
													Periodo Ripartizione
												</label></td>
												<td class="fieldValue medium">
													<div class="row-fluid">
														<span class="span10"> <select name="stato"
															id="stato" ng-model="ctrl.selectedPeriodoRipartizione"
															ng-options="item for item in ctrl.ripartizioni">
														</select>
														</span>
													</div>
												</td>

										</tr>

										<tr>
											<td class="fieldLabel medium"><label
												class="muted pull-right marginRight10px"> <span
													class="redColor" ng-show="form.process.$invalid">*</span>Licenziatario
											</label></td>
											<td class="fieldValue medium">
												<div class="row-fluid">
													<span class="span10"> <md-autocomplete
															style="height:28px; border: 1px solid #ddd;"
															class="md-autocomplete"
															md-selected-item="ctrl.selectedItem"
															md-search-text-change="searchTextChange(ctrl.searchText)"
															md-search-text="ctrl.searchText"
															md-selected-item-change="selectedItemChange(item)"
															md-items="item in querySearch(ctrl.searchText)"
															md-item-text="item.nome" md-min-length="0">
														<md-item-template> <span
															md-highlight-text="ctrl.searchText"
															md-highlight-flags="^i">{{item}} </span> </md-item-template> <md-not-found>
														Nessun risultato trovato per "{{ctrl.searchText}}" </md-not-found> </md-autocomplete>
													</span>
												</div>
											</td>


											<td class="fieldLabel medium"><label
												class="muted pull-right "> <span class="redColor"
													ng-show="form.process.$invalid">* </span>Tipologia di
													servizio
											</label></td>
											<td class="fieldValue medium">
												<div class="row-fluid">
													<span class="span10"> <select name="tipologia"
														id="tipologia" ng-model="ctrl.selectedTipologia"
														ng-options="item.nome for item in ctrl.tipologie">
													</select>
													</span>
												</div>
											</td>

											<td class="fieldLabel medium"><label
												class="muted pull-right "> <span class="redColor"
													ng-show="form.process.$invalid">* </span>Stato
											</label></td>
											<td class="fieldValue medium">
												<div class="row-fluid">
													<span class="span10"> <select name="stato"
														id="stato" ng-model="ctrl.selectedStato"
														ng-options="item.nome for item in ctrl.stati">
													</select>
													</span>
												</div>
											</td>
										</tr>

										<tr>
											<td class="fieldLabel medium" style="text-align: right"
												nowrap colspan="12">

												<button
													class="btn addButton"
													ng-click="download(ctrl.selectedPeriodSendFrom,ctrl.selectedPeriodSendTo, ctrl.searchText,ctrl.selectedPeriodFrom, ctrl.selectedPeriodTo, ctrl.selectedTipologia, ctrl.selectedStato, ctrl.selectedPeriodoRipartizione)">
													<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Download
														Csv</strong>
												</button>

												<button
													class="btn addButton"
													ng-click="apply(ctrl.selectedPeriodSendFrom,ctrl.selectedPeriodSendTo, ctrl.searchText,ctrl.selectedPeriodFrom, ctrl.selectedPeriodTo, ctrl.selectedTipologia, ctrl.selectedStato, ctrl.selectedPeriodoRipartizione, 0, 50, 50)">
													<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Applica</strong>
												</button>

											</td>
										</tr>
									</tbody>
								</table>
								<br>
								<div class="listViewActions pull-right">
						 			
						 			<div class="pageNumbers alignTop ">
							 			<span ng-show="ctrl.dsrProcessConfig.hasPrev || ctrl.dsrProcessConfig.hasNext">
							 				<span class="pageNumbersText" style="padding-right:5px">
							 					Record da <strong>{{ctrl.report.first+1}}</strong> a <strong>{{ctrl.report.last}}</strong>
							 				</span>
						 					<button title="Precedente" class="btn" id="listViewPreviousPageButton" type="button" 
						 							ng-click="navigateToPreviousPage()"
						 							ng-show="ctrl.dsrProcessConfig.hasPrev">
						 						<span class="glyphicon glyphicon-chevron-left"></span>
						 					</button>
						 					<button title="Successiva" class="btn" id="listViewNextPageButton" type="button"
						 							ng-click="navigateToNextPage()"
						 							ng-show="ctrl.dsrProcessConfig.hasNext">
						 						<span class="glyphicon glyphicon-chevron-right"></span>
						 					</button>
						 				</span>
							 		</div>
						 		</div>
								<br>
								<!-- table -->
								<div class="listViewEntriesDiv contents-bottomscroll">
									<div class="bottomscroll-div"
										style="width: 95%; min-height: 80px">

										<table class="table table-bordered listViewEntriesTable ">
											<thead>
												<tr class="listViewHeaders">
													<th><a href="" class="listViewHeaderValues">Data
															Invio </a></th>
													<th><a href="" class="listViewHeaderValues">Data<br>Validazione
													</a></th>
													<th><a href="" class="listViewHeaderValues">Data<br>Codifica
													</a></th>
													<th><a href="" class="listViewHeaderValues">Licenziatario</a></th>
													<th><a href="" class="listViewHeaderValues">Codice Licenza
													</a></th>
													<th><a href="" class="listViewHeaderValues">Tipologia
															di<br>Servizio
													</a></th>
													<th><a href="" class="listViewHeaderValues">Periodo<br> competenza
													</a></th>
													<th><a href="" class="listViewHeaderValues">Modalità<br>utilizzazione
													</a></th>
													<th><a href="" class="listViewHeaderValues">Modalità<br>fruizione
													</a></th>
													<th><a href="" class="listViewHeaderValues">In<br>Abbonamento</a></th>
													<th><a href="" class="listViewHeaderValues">Stato</a></th>
													<th><a href="" class="listViewHeaderValues">%
															Scarto</a></th>
													<th><a href="" class="listViewHeaderValues">%
															Codificato</a></th>
													<th><a href="" class="listViewHeaderValues">Ripartizione </a></th>
													<th></th>
												</tr>
											</thead>
											<tbody>

												<tr class="listViewEntries "
													ng-repeat="item in ctrl.report">
													<td class="listViewEntryValue "><span
														ng-hide="item.posizioneReport==null&&item.posizioneReport==undefined"
														ng-click="downloadReport(item,'CARICATO')"
														class="	glyphicon glyphicon-cloud-download"></span> &nbsp;{{formatDate(item.dataUpload)}}</td>
													<td class="listViewEntryValue ">{{formatDate(item.dataValidazione)}}</td>
													<td class="listViewEntryValue "><span
														ng-hide="item.posizioneReportCodificato==null&&item.posizioneReportCodificato==undefined"
														ng-click="downloadReport(item,'CODIFICATO')"
														class="	glyphicon glyphicon-cloud-download"></span></span> &nbsp;{{formatDate(item.dataCodifica)}}</td>
													<td class="listViewEntryValue ">{{item.nominativoLicenziatario}}</td>
													<td class="listViewEntryValue ">{{item.licenza}}</td>
													<td class="listViewEntryValue ">{{item.servizio}}</td>
													<td class="listViewEntryValue ">{{item.origPeriodoCompetenza}}</td>
													<td class="listViewEntryValue ">{{item.modUtilizzazione.modalita}}</td>
													<td class="listViewEntryValue ">{{item.modFruizione}}</td>
													<td class="listViewEntryValue ">{{getTextFromBool(item.inAbbonamento)}}</td>
													<td class="listViewEntryValue ">{{item.statoLogico}}</td>
													<td class="listViewEntryValue ">{{getPercentualeScarto(item.validazioneRecordTotali,item.validazioneRecordValidi)}}</td>
													<td class="listViewEntryValue ">{{getPercentualeCodifica(item.codificaUtilizzazioniCodificate,item.validazioneUtilizzazioniValide)}}</td>
													<td class="listViewEntryValue ">{{item.periodoRipartizione}}</td>
													<td class="listViewEntryValue "><span
														ng-click="showDettails(item)"
														class="glyphicon glyphicon-list-alt"></span></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<!-- /table -->

							</div>
						</div>
					</div>
				</div>
			</div>
			<div
				ng-hide="ctrl.dettailSelected==null||ctrl.dettailSelected==undefined">

				<br> <br>
				<div id="top_of_dialog">
					<strong>Dettaglio Report</strong>
				</div>
				<br> <br>
				<button class="btn addButton" ng-click="back()">
					<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Indietro</strong>
				</button>
							
				<button 
					ng-hide="ctrl.dettailSelected.posizioneReport==null&&ctrl.dettailSelected.posizioneReport==undefined"
					style="margin-right: 10px;"
					class="btn addButton pull-right" ng-click="downloadReport(ctrl.dettailSelected,'CARICATO')">
					<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Scarica report originale</strong>
				</button>
				
				<button 
					ng-hide="ctrl.dettailSelected.posizioneReportCodificato==null&&ctrl.dettailSelected.posizioneReportCodificato==undefined"
					style="margin-right: 10px;"  class="btn addButton pull-right" ng-click="downloadReport(ctrl.dettailSelected,'CODIFICATO')">
					<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Scarica report codificato</strong>
				</button>
				<br> <br>
				Dati Report
				<table class="table table-bordered listViewEntriesTable">
					<tbody>

						<tr class="listViewEntries">
							<td style="color: #444;font-weight: bold;font-size: 16px!important;" class="fieldLabel ">Licenziatario</td>
							<td class="listViewEntryValue ">{{ctrl.dettailSelected.nominativoLicenziatario}}</td>

							<td style="color: #444;font-weight: bold;font-size: 16px!important;" class="fieldLabel ">Codice Licenziatario</td>
							<td class="listViewEntryValue ">{{ctrl.dettailSelected.codiceLicenziatario}}</td>
										
							<td style="color: #444;font-weight: bold;font-size: 16px!important;" class="fieldLabel ">Codice Licenza</td>
							<td class="listViewEntryValue ">{{ctrl.dettailSelected.licenza}}</td>
						</tr>
						
						<tr class="listViewEntries">
							<td style="color: #444;font-weight: bold;font-size: 16px!important;" class="fieldLabel ">Tipologia di servizio</td>
							<td class="listViewEntryValue ">{{ctrl.dettailSelected.servizio}}</td>
																		
							<td style="color: #444;font-weight: bold;font-size: 16px!important;" class="fieldLabel ">Periodo Competenza</td>
							<td class="listViewEntryValue ">{{ctrl.dettailSelected.origPeriodoCompetenza}}</td>									
							
							<td style="color: #444;font-weight: bold;font-size: 16px!important;" class="fieldLabel ">Versione Report</td>
							<td class="listViewEntryValue ">{{ctrl.dettailSelected.progressivoReport}}</td>
						</tr>
					
						
						<tr class="listViewEntries">
							<td style="color: #444;font-weight: bold;font-size: 16px!important;" class="fieldLabel ">Modalità utilizzazione</td>
							<td class="listViewEntryValue ">{{ctrl.dettailSelected.modUtilizzazione.modalita}}</td>

							<td style="color: #444;font-weight: bold;font-size: 16px!important;" class="fieldLabel ">Modalità fruizione</td>
							<td class="listViewEntryValue ">{{ctrl.dettailSelected.modFruizione}}</td>
							
							<td style="color: #444;font-weight: bold;font-size: 16px!important;"class="fieldLabel ">In Abbonamento</td>
							<td class="listViewEntryValue ">{{getTextFromBool(ctrl.dettailSelected.inAbbonamento)}}</td>
						</tr>
						
							
						<tr class="listViewEntries">
							<td style="color: #444;font-weight: bold;font-size: 16px!important;"class="fieldLabel ">Data Invio</td>
							<td class="listViewEntryValue ">{{formatDateHour(ctrl.dettailSelected.dataUpload)}}</td>

							<td style="color: #444;font-weight: bold;font-size: 16px!important;" class="fieldLabel ">Data Validazione</td>
							<td class="listViewEntryValue ">{{formatDateHour(ctrl.dettailSelected.dataValidazione)}}</td>
											
							<td style="color: #444;font-weight: bold;font-size: 16px!important;" class="fieldLabel ">Data Codifica</td>
							<td class="listViewEntryValue ">{{formatDateHour(ctrl.dettailSelected.dataCodifica)}}</td>
						</tr>
						
						<tr class="listViewEntries">	
							<td style="color: #444;font-weight: bold;font-size: 16px!important;" class="fieldLabel ">Record Totali</td>
							<td class="listViewEntryValue ">{{getTextNA(ctrl.dettailSelected.validazioneRecordTotali)}}</td>
							
							<td style="color: #444;font-weight: bold;font-size: 16px!important;"class="fieldLabel ">Record Validi</td>
							<td class="listViewEntryValue ">{{getTextNA(ctrl.dettailSelected.validazioneRecordValidi)}}</td>
							
							<td style="color: #444;font-weight: bold;font-size: 16px!important;" class="fieldLabel ">Utilizzazioni Valide</td>
							<td class="listViewEntryValue ">{{getTextNA(ctrl.dettailSelected.validazioneUtilizzazioniValide)}}</td>
						</tr>
						
						<tr class="listViewEntries">
							<td style="color: #444;font-weight: bold;font-size: 16px!important;" class="fieldLabel ">Stato</td>
							<td class="listViewEntryValue ">{{ctrl.dettailSelected.statoLogico}}</td>
						
							<td style="color: #444;font-weight: bold;font-size: 16px!important;"class="fieldLabel ">% Scarto</td>
							<td class="listViewEntryValue ">{{getPercentualeScarto(ctrl.dettailSelected.validazioneRecordTotali,ctrl.dettailSelected.validazioneRecordValidi)}}</td>
							
							<td style="color: #444;font-weight: bold;font-size: 16px!important;"class="fieldLabel "></td>
							<td class="listViewEntryValue "></td>
						</tr>
					</tbody>
					</table>
					<br> <br>
					Kpi Codifica
					<table class="table table-bordered listViewEntriesTable">
						<tbody>
					
						<tr class="listViewEntries">	
							<td style="color: #444;font-weight: bold;font-size: 16px!important;"class="fieldLabel ">N. Record Codificati</td>
							<td class="listViewEntryValue ">{{getTextNA(ctrl.dettailSelected.codificaRecordCodificati)}}</td>
							
							<td style="color: #444;font-weight: bold;font-size: 16px!important;"class="fieldLabel ">% Record Codificati </td>
							<td class="listViewEntryValue ">{{getPercentualeOpereCodificate(ctrl.dettailSelected.codificaRecordCodificati,ctrl.dettailSelected.validazioneRecordValidi)}}</td>
							
							<td style="color: #444;font-weight: bold;font-size: 16px!important;"class="fieldLabel ">N. Utilizzazioni Codificate</td>
							<td class="listViewEntryValue ">{{getTextNA(ctrl.dettailSelected.codificaUtilizzazioniCodificate)}}</td>	
							
							<td style="color: #444;font-weight: bold;font-size: 16px!important;"class="fieldLabel ">% Utilizzazioni Codificate </td>
							<td class="listViewEntryValue ">{{getPercentualeCodifica(ctrl.dettailSelected.codificaUtilizzazioniCodificate,ctrl.dettailSelected.validazioneUtilizzazioniValide)}}</td>
										
								
						</tr>
					
					
						<tr class="listViewEntries">	
							<td style="color: #444;font-weight: bold;font-size: 16px!important;" class="fieldLabel ">N. Record Codificati Regolari</td>
							<td class="listViewEntryValue ">{{getTextNA(ctrl.dettailStat.recordCodificatiRegolari+ctrl.dettailStat.recordCodificatiProvvisori)}}</td>
							
							<td style="color: #444;font-weight: bold;font-size: 16px!important;"class="fieldLabel ">% Record Codificato Regolare</td>
							<td class="listViewEntryValue ">{{calcolaPercentuale(ctrl.dettailStat.recordCodificatiRegolari+ctrl.dettailStat.recordCodificatiProvvisori,ctrl.dettailSelected.validazioneRecordValidi)}}</td>
								
							<td style="color: #444;font-weight: bold;font-size: 16px!important;"class="fieldLabel ">N. Utilizzazioni Codificate Regolari</td>
							<td class="listViewEntryValue ">{{getTextNA(ctrl.dettailStat.utilizzazioniCodificateRegolari+ctrl.dettailStat.utilizzazioniCodificateProvvisorie)}}</td>
								
							<td style="color: #444;font-weight: bold;font-size: 16px!important;"class="fieldLabel ">% Utilizzazioni Codificate Regolare</td>
							<td class="listViewEntryValue ">{{calcolaPercentuale(ctrl.dettailStat.utilizzazioniCodificateRegolari+ctrl.dettailStat.utilizzazioniCodificateProvvisorie,ctrl.dettailSelected.validazioneUtilizzazioniValide)}}</td>
						</tr>
						
						<tr class="listViewEntries">							
							<td style="color: #444;font-weight: bold;font-size: 16px!important;"class="fieldLabel ">N. Record non Codificati</td>
							<td class="listViewEntryValue ">{{getTextNA(ctrl.dettailStat.recordNonCodificati)}}</td>
							
							<td style="color: #444;font-weight: bold;font-size: 16px!important;"class="fieldLabel ">% Record non Codificati</td>
							<td class="listViewEntryValue ">{{calcolaPercentuale(ctrl.dettailStat.recordNonCodificati,ctrl.dettailSelected.validazioneRecordValidi)}}</td>
								
							<td style="color: #444;font-weight: bold;font-size: 16px!important;"class="fieldLabel ">N. Utilizzazioni non Codificate</td>
							<td class="listViewEntryValue ">{{getTextNA(ctrl.dettailStat.utilizzazioniNonCodificate)}}</td>
								
							<td style="color: #444;font-weight: bold;font-size: 16px!important;"class="fieldLabel ">% Utilizzazioni non Codificate</td>
							<td class="listViewEntryValue ">{{calcolaPercentuale(ctrl.dettailStat.utilizzazioniNonCodificate,ctrl.dettailSelected.validazioneUtilizzazioniValide)}}</td>
						</tr>
						
						<tr class="listViewEntries">		
							<td style="color: #444;font-weight: bold;font-size: 16px!important;" class="fieldLabel ">N. Record Codificati Irregolari</td>
							<td class="listViewEntryValue ">{{getTextNA(ctrl.dettailStat.recordCodificatiIrregolari)}}</td>
														
							<td style="color: #444;font-weight: bold;font-size: 16px!important;" class="fieldLabel ">% Record Codificato Irregolare</td>
							<td class="listViewEntryValue ">{{calcolaPercentuale(ctrl.dettailStat.recordCodificatiIrregolari,ctrl.dettailSelected.validazioneRecordValidi)}}</td>
								
							<td style="color: #444;font-weight: bold;font-size: 16px!important;"class="fieldLabel ">N. Utilizzazioni Codificate Irregolari</td>
							<td class="listViewEntryValue ">{{getTextNA(ctrl.dettailStat.utilizzazioniCodificateIrregolari)}}</td>
							
							<td style="color: #444;font-weight: bold;font-size: 16px!important;"class="fieldLabel ">% Utilizzazioni Codificate Irregolare</td>
							<td class="listViewEntryValue ">{{calcolaPercentuale(ctrl.dettailStat.utilizzazioniCodificateIrregolari,ctrl.dettailSelected.validazioneUtilizzazioniValide)}}</td>
						</tr>
						
						<tr class="listViewEntries">		
							<td style="color: #444;font-weight: bold;font-size: 16px!important;" class="fieldLabel ">% Polverizzazione</td>
							<td class="listViewEntryValue ">{{getTextNA(ctrl.dettailStat.percPolverizzazione)}}</td>
														
							<td style="color: #444;font-weight: bold;font-size: 16px!important;"class="fieldLabel "></td>
							<td class="listViewEntryValue "></td>
								
							<td style="color: #444;font-weight: bold;font-size: 16px!important;"class="fieldLabel "></td>
							<td class="listViewEntryValue "></td>
							
							<td style="color: #444;font-weight: bold;font-size: 16px!important;"class="fieldLabel "></td>
							<td class="listViewEntryValue "></td>
						</tr>
					</tbody>
				</table>
				
				<br> <br>
				Ripartizione
				<table class="table table-bordered listViewEntriesTable">
					<tbody>

						<tr class="listViewEntries">
							<td style="color: #444;font-weight: bold;font-size: 16px!important;" class="fieldLabel ">Ripartizione</td>
							<td class="listViewEntryValue ">{{getTextNA(ctrl.dettailSelected.periodoRipartizione)}}</td>

							<td style="color: #444;font-weight: bold;font-size: 16px!important;" class="fieldLabel ">Importo (&euro;)</td>
							<td class="listViewEntryValue ">{{getTextNA(formatNumber(ctrl.dettailSelected.importo))}}</td>
							
							<td style="color: #444;font-weight: bold;font-size: 16px!important;"class="fieldLabel "></td>
							<td class="listViewEntryValue "></td>
							
										
						</tr>
						
						<tr class="listViewEntries">
							<td style="color: #444;font-weight: bold;font-size: 16px!important;" class="fieldLabel ">Ripartito</td>
							<td class="listViewEntryValue ">{{getTextNA(checkEmptyText(ctrl.dettailSelected.dataRipartizione))}}</td>
							
							<td style="color: #444;font-weight: bold;font-size: 16px!important;" class="fieldLabel ">Data Ripartizione</td>
							<td class="listViewEntryValue ">{{getTextNA(formatDateHour(ctrl.dettailSelected.dataRipartizione))}}</td>
																		
							<td style="color: #444;font-weight: bold;font-size: 16px!important;" class="fieldLabel ">Importo Ex Art 18 (&euro;)</td>
							<td class="listViewEntryValue ">{{getTextNA(formatNumber(ctrl.dettailSelected.importoExArt18))}}</td>									
							
							
						</tr>
					
						
					</tbody>
					</table>
				<br> <br>
				<div ng-hide="checkSize()"  id="top_of_dialog">
					<strong>Storico </strong>
				</div>

				<div  ng-hide="checkSize()"  style="margin-top: 15px">
					<table class="table table-bordered listViewEntriesTable">
						<thead>
							<tr class="listViewHeaders">
								<th><a href="" class="listViewHeaderValues fixedSize">Versione<br>Report </a></th>
								<th><a href="" class="listViewHeaderValues fixedSize">Data invio </a></th>
								<th><a href="" class="listViewHeaderValues fixedSize">Data validazione</a></th>
								<th><a href="" class="listViewHeaderValues fixedSize">Data codifica</a></th>
								<th><a href="" class="listViewHeaderValues fixedSize">Record<br>Totali</a></th>
								<th><a href="" class="listViewHeaderValues fixedSize">Record<br>Validi</a></th>
								<th><a href="" class="listViewHeaderValues fixedSize">Utilizzazioni<br>Valide</a></th>
								<th><a href="" class="listViewHeaderValues fixedSize">Utilizzazioni<br>Codificate</a></th>
								<th><a href="" class="listViewHeaderValues fixedSize">Record<br>Codificati</a></th>
								<th><a href="" class="listViewHeaderValues fixedSize">Percentuale<br>Scarto</a></th>
								<th><a href="" class="listViewHeaderValues fixedSize">% Codificato<br>Utilizzazioni</a></th>
								<th><a href="" class="listViewHeaderValues fixedSize">% Codificato<br>Record</a></th>
								<th><a href="" class="listViewHeaderValues fixedSize">Stato</a></th>
							</tr>
						</thead>
						<tbody>
							<tr class="listViewEntries"
								ng-repeat="item in ctrl.storico track by item.identificativoReport">
								<td class="listViewEntryValue fixedSize">{{getTextNA(item.progressivoReport)}}</td>
								<td class="listViewEntryValue fixedSize"><span 
														ng-hide="item.posizioneReport==null&&item.posizioneReport==undefined"
														ng-click="downloadReport(item,'CARICATO')"
														class="	glyphicon glyphicon-cloud-download"></span> &nbsp;
														{{formatDateHour(item.dataUpload)}}</td>
								<td class="listViewEntryValue fixedSize">{{formatDateHour(item.dataValidazione)}}</td>
								<td class="listViewEntryValue fixedSize"><span
														ng-hide="item.posizioneReportCodificato==null&&item.posizioneReportCodificato==undefined"
														ng-click="downloadReport(item,'CODIFICATO')"
														class="	glyphicon glyphicon-cloud-download"></span></span> &nbsp;
														{{formatDateHour(item.dataCodifica)}}</td>
								<td class="listViewEntryValue fixedSize">{{getTextNA(item.validazioneRecordTotali)}}</td>
								<td class="listViewEntryValue fixedSize">{{getTextNA(item.validazioneRecordValidi)}}</td>
								<td class="listViewEntryValue fixedSize">{{getTextNA(item.validazioneUtilizzazioniValide)}}</td>
								<td class="listViewEntryValue fixedSize">{{getTextNA(item.codificaUtilizzazioniCodificate)}}</td>
								<td class="listViewEntryValue fixedSize">{{getTextNA(item.codificaRecordCodificati)}}</td>	
								<td class="listViewEntryValue fixedSize">{{getPercentualeScarto(item.validazioneRecordTotali,item.validazioneRecordValidi)}}</td>
								<td class="listViewEntryValue fixedSize">{{getPercentualeCodifica(item.codificaUtilizzazioniCodificate,item.validazioneUtilizzazioniValide)}}</td>
								<td class="listViewEntryValue fixedSize">{{getPercentualeOpereCodificate(item.codificaRecordCodificati,item.validazioneRecordValidi)}}</td>
								<td class="listViewEntryValue fixedSize">{{item.statoLogico}}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>


	</div>


</div>