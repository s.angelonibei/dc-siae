<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%@
	include file="../navbar.jsp" %>

<div class="bodyContents" >

	<div class="mainContainer row-fluid" >
	
		<div id="companyLogo" class="navbar commonActionsContainer noprint">
			<div class="actionsContainer row-fluid">
				<div class="span2">
<!--  -- >			<span class="companyLogo"><img src="images/siae_logo_siaetrasparente.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
<!--  -->			<span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
				</div>
				<span class="btn-toolbar span4">
					<div class="pageNumbers alignTop pull-right" style="text-align: center;">
		 				<span class="pageNumbersText"><strong>Gestione Processi</strong></span>
					</div>
				</span>				
			</div>
		</div>
	
		<div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px;"> 
			<div class="listViewPageDiv">

				<!-- actions and pagination -->
				<div class="listViewTopMenuDiv noprint">
					<div class="listViewActionsDiv row-fluid">

						<span class="btn-toolbar span4">
							<span class="btn-group">
<!--  -- >
								<button class="btn addButton" ng-click="showMoveToNext($event,ctrl.unidentifiedSong)">
									<span class="glyphicon glyphicon-forward"></span>&nbsp;&nbsp;<strong>Successiva</strong>
								</button>
<!--  -->
 							</span>
						</span>

						<span class="btn-toolbar span4">
							<span class="customFilterMainSpan btn-group" style="text-align: center;">
								<div class="pageNumbers alignTop ">
<!--  -- >
					 				<span class="pageNumbersText"><strong>Titolo</strong></span>
<!--  -->
								</div>
						 	</span>
						</span>

					<%--<span class="span4 btn-toolbar">--%>
							<%--<div class="listViewActions pull-right">--%>
								<%--<span class="btn-group">--%>
									<%--<button class="btn addButton" ng-click="showNewProcessConfig($event)">--%>
										<%--<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;<strong>Nuovo</strong>--%>
									<%--</button>--%>
	 							<%--</span>--%>
	 				 		<%--</div>--%>
					 		<%--<div class="clearfix"></div>--%>
					 	<%--</span>--%>

					</div>
				</div>
							
<!-- actions and pagination -->
				<div class="listViewTopMenuDiv noprint">
					<div class="listViewActionsDiv row-fluid">
<!--  -->
						<span class="btn-toolbar span4">
							<span class="btn-group">

 							</span>
						</span>
<!--  -->
<!--  -->
						<span class="btn-toolbar span4">
							<span class="customFilterMainSpan btn-group" style="text-align: center;">
								<div class="pageNumbers alignTop ">
					 				<span class="pageNumbersText"><strong><!-- Titolo --></strong></span>
								</div>
						 	</span>
						</span>
<!--  -->
<!--  -->
						<span class="span4 btn-toolbar">
							<div class="listViewActions pull-right">
					 			
					 			<div class="pageNumbers alignTop ">
						 			<span ng-show="ctrl.processList.hasPrev || ctrl.processList.hasNext">
						 				<span class="pageNumbersText" style="padding-right:5px">
						 					Record da <strong>{{ctrl.processList.first+1}}</strong> a <strong>{{ctrl.processList.last}}</strong>
						 				</span>
					 					<button title="Precedente" class="btn" id="listViewPreviousPageButton" type="button" 
					 							ng-click="navigateToPreviousPage()"
					 							ng-show="ctrl.processList.hasPrev">
					 						<span class="glyphicon glyphicon-chevron-left"></span>
					 					</button>
					 					<button title="Successiva" class="btn" id="listViewNextPageButton" type="button"
					 							ng-click="navigateToNextPage()"
					 							ng-show="ctrl.processList.hasNext">
					 						<span class="glyphicon glyphicon-chevron-right"></span>
					 					</button>
					 				</span>
						 		</div>
					 		</div>
					 		<div class="clearfix"></div>
					 	</span>
<!--  -->
					</div>
				</div>
					
							
				<!-- riga da riconoscere -->
				
				<!-- table -->
				<div class="listViewContentDiv" id="listViewContents">
					
					 <div class="contents-topscroll noprint">
					 	<div class="topscroll-div" style="width: 95%">&nbsp;</div>
					 </div>
					 					 
					 <div class="listViewEntriesDiv contents-bottomscroll">
					 	<div class="bottomscroll-div" style="width: 95%; min-height: 80px">
					 	
					 		<table class="table table-bordered listViewEntriesTable">
					 		<thead>
					 		<tr class="listViewHeaders">
					 		
					 			<th nowrap><a href="" class="listViewHeaderValues" >Nome Processo</a></th>
					 			<%--<th nowrap><a href="" class="listViewHeaderValues" >Identificazione con ID_SONG</a></th>--%>
					 			<th nowrap><a href="" class="listViewHeaderValues" >Identificazione con ISWC</a></th>
					 			<th nowrap><a href="" class="listViewHeaderValues" >Identificazione con ISRC</a></th>
					 			<th nowrap><a href="" class="listViewHeaderValues" >Identificazione con Titolo Autori</a></th>
					 			<th nowrap></th>
					 		</tr>
					 		</thead>
					 		<tbody > 
						 		<tr class="listViewEntries" ng-repeat="item in ctrl.processList.rows | orderBy:'+name'">
						 			<td class="listViewEntryValue medium" nowrap >{{item.name}}</td>									
						 			<%--<td class="listViewEntryValue medium" nowrap >{{item.identificationByIdSong == "-1" ? "No" : item.identificationByIdSong}}</td>--%>
						 			<td class="listViewEntryValue medium" nowrap >{{item.identificationByIswc == "-1" ? "No" :   item.identificationByIswc}}</td>
						 			<td class="listViewEntryValue medium" nowrap >{{item.identificationByIsrc == "-1" ? "No" :   item.identificationByIsrc}}</td>
						 			<td class="listViewEntryValue medium" nowrap >{{item.identificationByAuthorTitle == "-1" ? "No" : item.identificationByAuthorTitle}}</td>
						 			<td nowrap class="medium">
						 				<div class="actions pull-right">
<!--  -->
						 				<span class="actionImages">
						 					<a ng-click="showInfo(item)"><i title="Dettaglio" class="glyphicon glyphicon-info-sign alignMiddle"></i></a>&nbsp;
						 					<%--<a ng-click="showDeleteConfig($event,item)"><i title="Elimina" class="glyphicon glyphicon-trash alignMiddle"></i></a>&nbsp;--%>
						 				</span>
<!--  -->
						 				</div>
						 			</td>

						 		</tr>
					 		</tbody>
					 		</table>
					 		
						</div>
					</div>
				</div>
				<!-- /table -->
		
				
			</div>
		</div>
	</div>
<!-- -- >
	<pre>{{ctrl.properties}}</pre>
< !-- -->
</div>