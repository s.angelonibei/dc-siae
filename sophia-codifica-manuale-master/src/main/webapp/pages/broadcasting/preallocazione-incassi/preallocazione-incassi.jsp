<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@
	include file="../../navbar.jsp"%>

<div class="bodyContents">

	<div class="mainContainer row-fluid">

		<div id="companyLogo" class="navbar commonActionsContainer noprint">
			<div class="actionsContainer row-fluid">
				<div class="span2">
					<span class="companyLogo"><img src="images/logo_siae.jpg"
						title="SIAE" alt="SIAE">&nbsp;</span>
				</div>
				<span class="btn-toolbar span4">
					<div class="pageNumbers alignTop pull-right"
						style="text-align: center;">
						<span class="pageNumbersText"><strong>Preallocazione
								Incassi</strong></span>
					</div>
				</span>
			</div>
		</div>

		<div class="contentsDiv marginLeftZero" id="rightPanel"
			style="min-height: 180px;">
			<div
				ng-hide="ctrl.dettailSelected!=null&&ctrl.dettailSelected!=undefined">
				<div class="listViewPageDiv">

					<div class="listViewTopMenuDiv noprint">
						<div class="listViewTopMenuDiv noprint">
							<div class="listViewActionsDiv row-fluid">

								<span class="btn-toolbar span4"> <span class="btn-group">

								</span>
								</span> <span class="btn-toolbar span4"> <span
									class="customFilterMainSpan btn-group"
									style="text-align: center;">
										<div class="pageNumbers alignTop "></div>
								</span>
								</span> <span class="span4 btn-toolbar">
									<div class="listViewActions pull-right">
										<!-- span class="btn-group">
										<button class="btn addButton" ng-click="showUpload($event)">
											<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;<strong>Nuovo</strong>
										</button>
									</span> -->
									</div>
									<div class="clearfix"></div>
								</span>

							</div>
						</div>
					</div>

					<!-- table -->
					<div class="listViewContentDiv" id="listViewContents">

						<!--  -- >
					 <div class="contents-topscroll noprint">
					 	<div class="topscroll-div" style="width: 95%;">&nbsp;</div>
					 </div>
<!--  -->
						<div class="listViewEntriesDiv">
							<div style="width: 100%; min-height: 400px">

								<table
									class="table table-bordered blockContainer showInlineTable ">
									<thead>
										<tr>
											<th class="blockHeader" colspan="12" ng-hide="ctrl.hideForm">
												Verifica Canale</th>
										</tr>
									</thead>

									<tbody ng-hide="ctrl.hideForm">

										<tr>
											<td class="fieldLabel medium"><label
												class="muted pull-right marginRight10px"> <span
													class="redColor" ng-show="form.process.$invalid">*</span>Data
													Competenza da
											</label></td>
											<td class="fieldLabel medium" nowrap><input type="text"
												class="span9 input-small datepickerFromRiparto"
												style="width: 180px" ng-model="ctrl.selectedPeriodFrom">
												<script type="text/javascript">
													$(".datepickerFromRiparto")
															.datepicker(
																	{
																		format : 'dd-mm-yyyy',
																		language : 'it',
																		viewMode : "days",
																		minViewMode : "days",
																		autoclose : true,
																		todayHighlight : true
																	});
												</script> <span class="glyphicon glyphicon-erase"
												style="margin-top: 6px;"
												ng-click="ctrl.selectedPeriodFrom=undefined;"></span></td>
											<!--   <td class="fieldValue medium">

                            <div class="input-group">
							<span class="span10">
								 <select name="monthFrom" id="monthFrom" ng-model="selectedMonthFrom" ng-options="item.name for item in ctrl.months track by item.id" required></select>
							</span>
                                <span class="span10">
								 <select ng-model="yearFrom"	ng-options="n for n in [] | year_range:1900:2017" required></select>
							</span>
                            </div>


                        </td> -->

											<td class="fieldLabel medium"><label
												class="muted pull-right marginRight10px"> <span
													class="redColor" ng-show="form.process.$invalid">*</span>Data
													Competenza a
											</label></td>
											<td class="fieldLabel medium"><input type="text"
												class="span9 input-small datepickerToRiparto"
												style="width: 180px" ng-model="ctrl.selectedPeriodTo">
												<script type="text/javascript">
													$(".datepickerToRiparto")
															.datepicker(
																	{
																		format : 'dd-mm-yyyy',
																		language : 'it',
																		viewMode : "days",
																		minViewMode : "days",
																		autoclose : true,
																		todayHighlight : true
																	});
												</script> <span class="glyphicon glyphicon-erase"
												style="margin-top: 6px;"
												ng-click="ctrl.selectedPeriodTo=undefined;"></span></td>

											<!--   <td class="fieldValue medium">

                            <div class="input-group">
							<span class="span10">
								 <select name="monthTo" id="monthTo" ng-model="selectedMonthTo" ng-options="item.name for item in ctrl.months track by item.id" required></select>
							</span>
                                <span class="span10">
								 <select ng-model="yearTo"	ng-options="n for n in [] | year_range:1900:2017" required></select>
							</span>
                            </div>


                        </td> -->

											<td class="fieldLabel medium"><label
												class="muted pull-right "> <span class="redColor"
													ng-show="form.process.$invalid">* </span>Tipo di diritto
											</label></td>
											<td class="fieldValue medium">
												<div class="row-fluid">
													<span class="span10"> <select name="emittente"
														id="emittente" ng-change="" ng-model="ctrl.tipoDiritto"
														ng-options="item.nome for item in ctrl.listaTipiDiritto">
													</select>
													</span>
												</div>
											</td>

										</tr>

										<tr>
											<td class="fieldLabel medium"><label
												class="muted pull-right marginRight10px"> <span
													class="redColor" ng-show="form.process.$invalid">*</span>Tipo
													Emittente
											</label></td>
											<td class="fieldValue medium">
												<div class="row-fluid">
													<span class="span10"> <select name="tipoEmittente"
														id="tipoEmittente" ng-model="ctrl.selectedTipoBroadcaster"
														ng-options="item.nome for item in ctrl.tipiEmittenti"
														ng-change="getEmittenti(ctrl.selectedTipoBroadcaster.nome)">
													</select>
													</span>
												</div>
											</td>


											<td class="fieldLabel medium"><label
												class="muted pull-right "> <span class="redColor"
													ng-show="form.process.$invalid">* </span>Emittente
											</label></td>
											<td class="fieldValue medium">
												<div class="row-fluid">
													<span class="span10"> <select name="emittente"
														id="emittente" ng-change="getCanali(ctrl.emittente)"
														ng-model="ctrl.emittente"
														ng-options="item.nome for item in ctrl.listaEmittenti">
													</select>
													</span>
												</div>
											</td>

											<td class="fieldLabel medium"><label
												class="muted pull-right "> <span class="redColor"
													ng-show="form.process.$invalid">* </span>Canale
											</label></td>
											<td class="fieldValue medium">
												<div class="row-fluid">
													<span class="span10"> <select name="canale"
														id="canale" ng-model="ctrl.canale"
														ng-options="item.nome for item in ctrl.listaCanali">
													</select>
													</span>
												</div>
											</td>

										</tr>

										<tr>
											<td class="fieldLabel medium" style="text-align: right"
												nowrap colspan="12">
												<button
													ng-hide="
													ctrl.selectedPeriodTo==undefined||ctrl.selectedPeriodFrom==undefined"
													class="btn addButton"
													ng-click="apply(ctrl.selectedTipoBroadcaster,ctrl.emittente,ctrl.canale,ctrl.anno,ctrl.selectedPeriodFrom,ctrl.selectedPeriodTo)">
													<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Applica</strong>
												</button>

											</td>
										</tr>
									</tbody>
								</table>
								<br>

								<!-- table -->
								<div class="listViewEntriesDiv contents-bottomscroll">
									<div class="bottomscroll-div"
										style="width: 95%; min-height: 80px">

										<table class="table table-bordered listViewEntriesTable ">
											<thead>
												<tr class="listViewHeaders">
													<th><a href="" 
														class="listViewHeaderValues">Tipo<br>Emittente </a></th>
													<th><a href="" 
														class="listViewHeaderValues">Emittente </a></th>
													<th><a href=""
														class="listViewHeaderValues">Lista Canali </a></th>
													<th><a href="" 
														class="listViewHeaderValues">Tipo<br>Diritto </a></th>
													<th><a href="" 
														class="listViewHeaderValues">Inizio<br>Validità </a></th>
													<th><a href="" 
														class="listViewHeaderValues">Fine<br>Validità </a></th>
													<th><a href="" 
														class="listViewHeaderValues">Conto </a></th>
													<th></th>
												</tr>
											</thead>
											<tbody>

												<tr class="listViewEntries "
													ng-repeat="item in ctrl.conti">

													<td class="listViewEntryValue ">{{item.tipoEmittente}}</td>
													<td class="listViewEntryValue ">{{item.emittente}}</td>
													<td class="listViewEntryValue ">{{item.listaCanali}}</td>
													<td class="listViewEntryValue ">{{item.tipoDiritto}}</td>
													<td class="listViewEntryValue ">{{formatDate(item.periodoDa)}}</td>
													<td class="listViewEntryValue ">{{formatDate(item.periodoA)}}</td>
													<td class="listViewEntryValue ">{{item.conto}}</td>

													<td class="listViewEntryValue "><span
														ng-click="showConto(item)"
														class="glyphicon glyphicon-list-alt"></span></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<!-- /table -->

							</div>
						</div>
					</div>
				</div>
			</div>
			<div
				ng-hide="ctrl.dettailSelected==null||ctrl.dettailSelected==undefined">

				<br> <br>
				<div id="top_of_dialog">
					<strong>Dettaglio Carico Riparto</strong>
				</div>
				<br>
				<button class="btn addButton" ng-click="back()">
					<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Torna
						alla lista</strong>
				</button>
				<br> <br>
				<table class="table table-bordered listViewEntriesTable">
					<tbody>
						<tr>
							<td class="fieldLabel medium">Tipo Emittente</td>
							<td class="listViewEntryValue medium">{{ctrl.conto.tipoEmittente}}</td>

							<td class="fieldLabel medium">Emittente</td>
							<td class="listViewEntryValue medium">{{ctrl.conto.emittente}}</td>

						</tr>
						<tr>
							<td class="fieldLabel medium">Tipo Diritto</td>
							<td class="listViewEntryValue medium">{{ctrl.conto.tipoDiritto}}</td>

							<td class="fieldLabel medium">Utente Ultima Modifica</td>
							<td class="listViewEntryValue medium">{{ctrl.dettagliConto.utente}}</td>

						</tr>
						<tr>
							<td class="fieldLabel medium">Data da</td>
							<td class="listViewEntryValue medium">
								{{formatDate(ctrl.conto.periodoDa)}}</td>

							<td class="fieldLabel medium">Data a</td>
							<td class="listViewEntryValue medium">
								{{formatDate(ctrl.conto.periodoA)}}</td>
						</tr>
						<tr>
							<td class="fieldLabel medium">Conto</td>
							<td class="listViewEntryValue medium">{{ctrl.conto.conto}}</td>

							<td class="fieldLabel medium">Importo Conto (&euro;)</td>
							<td class="listViewEntryValue medium">{{formatNumber(ctrl.dettagliConto.codiceConto.importo)}}</td>
						</tr>

					</tbody>
				</table>
				<br> 


				<div id="top_of_dialog">
					<strong>Dettaglio Allocazioni importi</strong>
				</div>
				<br>

				<table 
					class="table table-bordered listViewEntriesTable ">
					<thead>
						<tr class="listViewHeaders">
							<th><a href="" 
								class="listViewHeaderValues">Canale </a></th>
							<th><a href="" 
								class="listViewHeaderValues">% Importi </a></th>
							<th><a href="" 
								class="listViewHeaderValues">Importo Preallocato </a></th>
							<th><a href="" 
								class="listViewHeaderValues">Importo Ripartito</a></th>
						</tr>
					</thead>
					<tbody>

						<tr class="listViewEntries"
							ng-repeat="item in ctrl.dettagliConto.dettaglioPianoDeiConti | orderBy : '+canali.nome'">

							<td class="listViewEntryValue ">{{item.canali.nome}}</td>
							<td class="fieldValue medium">
								<div class="row-fluid">
									<span class="span10"> <input type="text" ng-disabled="item.importoAllocazioneDefinitivo!=null||tem.importoAllocazioneDefinitivo!=undefined"
										class="form-control" id="name"
										placeholder="Inserisci la percentuale" ng-model="item.percentualeAllocazione"
										required>
									</span>
								</div>
							</td>
							<td class="listViewEntryValue ">{{calcolaImportoAllocazione(item)}}</td>
							<td class="listViewEntryValue ">{{formatNumber(item.importoAllocazioneDefinitivo)}}</td>

						</tr>
					</tbody>
				</table>

				<br>
				<button class="btn addButton"
					ng-click="cancelModify()">
					<span class="glyphicon glyphicon-ko"></span>&nbsp;&nbsp;<strong> Annulla </strong>
				</button>

				<button class="btn addButton"
					ng-hide="checkPercentuale()"
					ng-click="updatePreallocazioneIncassi('<%=userName%>')">
					<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Salva</strong>
				</button>
				<strong
					ng-show="checkPercentuale()">Impossibile Salvare percentuale non uguale a 100%</strong>
				<br> <br>
		
				<div ng-hide="checkSize(ctrl.storico)" >
				<div id="top_of_dialog">
					<strong>Storico modifiche</strong>
				</div>

				<div style="margin-top: 15px">
					<table class="table table-bordered listViewEntriesTable">
						<thead>
							<tr class="listViewHeaders">
								<th><a href="" class="listViewHeaderValues fixedSize">Data Ultima Modifica</a></th>
								<th><a href="" class="listViewHeaderValues fixedSize">Utente Ultima Modifica</a></th>
								<th><a href="" class="listViewHeaderValues fixedSize">Dettaglio</a></th>
							</tr>
						</thead>
						<tbody>

							<tr class="listViewEntries"
								ng-repeat="item in ctrl.storico">
								<td class="listViewEntryValue fixedSize">{{item.dataCreazione}}</td>
								<td class="listViewEntryValue fixedSize">{{item.utenteUltimaModifica}}</td>
								<td class="listViewEntryValue ">
								<span ng-click="openDettail(ctrl.conto.pianoConti,item)"
									class="glyphicon glyphicon-list-alt">
								</span></td>
							</tr>
						</tbody>
					</table>
				</div>
				</div>
			</div>
		</div>
	</div>
</div>