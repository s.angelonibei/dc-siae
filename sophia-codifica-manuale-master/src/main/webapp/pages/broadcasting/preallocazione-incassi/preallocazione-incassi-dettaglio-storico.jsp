<%@ page import="com.google.inject.Injector"%><%@
	page
	import="com.google.inject.Key"%><%@
	page
	import="com.google.inject.name.Names"%><%@
	page language="java"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	final Injector injector = (Injector) pageContext.getServletContext().getAttribute(Injector.class.getName());
	final String userName = (String) session.getAttribute("sso.user.userName");
	final String homeUrl = injector.getInstance(Key.get(String.class, Names.named("home.url")));
%>
<form name="form" novalidate>

	<div id="top_of_dialog">
		<strong>Modifica Carico Ripartizione</strong>
	</div>

		<table class="table table-bordered listViewEntriesTable">
					<tbody>
						<tr>
							<td class="fieldLabel medium">Data Creazione</td>
							<td class="listViewEntryValue medium">{{formatDate(ctrl.storico.dataCreazione)}}</td>

							<td class="fieldLabel medium">Utente Ultima Modifica</td>
							<td class="listViewEntryValue medium">{{ctrl.storico.utenteUltimaModifica}}</td>
						</tr>
					</tbody>
				</table>
	<div style="margin-top: 15px">
		<table 
			class="table table-bordered listViewEntriesTable ">
			<thead>
				<tr class="listViewHeaders">
					<th><a href="" 
						class="listViewHeaderValues">Canale </a></th>
					<th><a href="" 
						class="listViewHeaderValues">% Importi </a></th>
					<th><a href="" 
						class="listViewHeaderValues">Importo Preallocato </a></th>
					<th><a href="" 
						class="listViewHeaderValues">Importo Ripartito</a></th>
				</tr>
			</thead>
			<tbody>

				<tr class="listViewEntries"
					ng-repeat="item in ctrl.dettagliConto.dettaglioPianoDeiConti | orderBy : '+canali.nome'">
					<td class="listViewEntryValue ">{{item.canali.nome}}</td>
					<td class="listViewEntryValue ">{{item.percentualeAllocazione}}</td>
					<td class="listViewEntryValue ">{{calcolaImportoAllocazione(item)}}</td>
					<td class="listViewEntryValue ">{{formatNumber(item.importoAllocazioneDefinitivo)}}</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div style="margin-top: 15px">
		<table width="100%">
			<tr>
				<td>
					<button type="button" class="btn addButton" ng-click="cancel()">
						<span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp;<strong>Chiudi</strong>
					</button>
				</td>
			</tr>
		</table>
	</div>

</form>

<script type="text/ng-template" id="errorDialogId">

        <div id="target"  >
          <strong>{{ngDialogData}}</strong>
        </div>
    </script>

<script type="text/javascript">
	$('#top_of_dialog')[0].scrollIntoView(true);
</script>
