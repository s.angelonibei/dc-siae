<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@
	include file="../../navbar.jsp"%>
<div class="bodyContents">
	<div class="mainContainer row-fluid">
		<div id="companyLogo" class="navbar commonActionsContainer noprint">
			<div class="actionsContainer row-fluid">
				<div class="span2">
					<!--  -- >
					<span class="companyLogo"><img src="images/siae_logo_siaetrasparente.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
					<!--  -->
					<span class="companyLogo"><img src="images/logo_siae.jpg"
						title="SIAE" alt="SIAE">&nbsp;</span>
				</div>
				<span class="btn-toolbar span4">
					<div class="pageNumbers alignTop pull-right"
						style="text-align: center;">
						<span
							ng-hide="ctrl.selectedReport!=undefined&&ctrl.selectedReport!=null"
							class="pageNumbersText"> <strong>Gestione
								Utilizzazioni</strong>
						</span>
					</div>
				</span>
			</div>
		</div>

		<div
			ng-hide="ctrl.selectedReport!=undefined&&ctrl.selectedReport!=null"
			class="contentsDiv marginLeftZero" id="rightPanel"
			style="min-height: 80px;">

			<div class="listViewPageDiv">
				<!-- contenitore principale -->


				<div class="listViewActionsDiv row-fluid">
					<table
						class="table table-bordered blockContainer showInlineTable equalSplit">
						<thead>
							<tr>
								<th class="blockHeader" colspan="6" ng-show="ctrl.hideForm"><span
									ng-click="ctrl.hideForm=false"
									class="glyphicon glyphicon-expand"></span>&nbsp;Parametri
									Ricerca</th>
								<th class="blockHeader" colspan="6" ng-hide="ctrl.hideForm"><span
									ng-click="ctrl.hideForm=true"
									class="glyphicon glyphicon-collapse-down"></span>&nbsp;Parametri
									Ricerca</th>
							</tr>
						</thead>
						<tbody ng-hide="ctrl.hideForm">
							<tr>

								<td class="fieldLabel medium"><label
									class="muted pull-right "> <span class="redColor"
										ng-show="form.process.$invalid">* </span>Emittente
								</label></td>
								<td class="fieldValue medium">
									<div class="row-fluid">
										<span class="span10"> <select name="emittente"
											id="emittente" ng-change="getCanali(ctrl.fiteredBroadcaster)"
											ng-model="ctrl.fiteredBroadcaster"
											ng-options="getNameForSelect(item) for item in ctrl.emittenti"
											required>
										</select>
										</span>
									</div>
								</td>

								<td class="fieldLabel medium"><label
									class="muted pull-right "> <span class="redColor"
										ng-show="form.process.$invalid">* </span>Canale
								</label></td>
								<td class="fieldValue medium">
									<div class="row-fluid">
										<span class="span10"> <select name="canale" id="canale"
											ng-model="ctrl.fiteredChannel"
											ng-options="item.nome for item in ctrl.listaCanali" required>
										</select>
										</span>
									</div>
								</td>

								<td class="fieldLabel medium"><label
									class="muted pull-right marginRight10px"> Titolo
										Trasmissione </label></td>
								<td class="fieldValue medium">
									<div class="row-fluid">
										<span class="span10"> <input
											ng-model="ctrl.fiteredTitle">

										</span>
									</div>
								</td>
							</tr>
							<tr>
								<td class="fieldLabel medium"><label
									class="muted pull-right marginRight10px"> <span
										class="redColor" ng-show="form.process.$invalid">*</span>Inizio
										trasmissione da
								</label></td>
								<td class="fieldLabel medium" nowrap><input type="text"
									class="span9 input-small datepickerFrom" style="width: 180px"
									ng-model="ctrl.fiteredFromDate"> <script
										type="text/javascript">
										$(".datepickerFrom").datetimepicker({
									    	format: 'yyyy-mm-dd hh:ii', 
									    	language: 'it',
									    	autoclose: true,
									    	todayHighlight: true
										});
									</script> <span class="glyphicon glyphicon-erase"
									style="margin-top: 6px;"
									ng-click="ctrl.fiteredFromDate=undefined;"></span></td>
								<!--   <td class="fieldValue medium">

                            <div class="input-group">
							<span class="span10">
								 <select name="monthFrom" id="monthFrom" ng-model="selectedMonthFrom" ng-options="item.name for item in ctrl.months track by item.id" required></select>
							</span>
                                <span class="span10">
								 <select ng-model="yearFrom"	ng-options="n for n in [] | year_range:1900:2017" required></select>
							</span>
                            </div>


                        </td> -->

								<td class="fieldLabel medium"><label
									class="muted pull-right marginRight10px"> <span
										class="redColor" ng-show="form.process.$invalid">*</span>Inizio
										trasmissione a
								</label></td>
								<td class="fieldLabel medium"><input type="text"
									class="span9 input-small datepickerTo" style="width: 180px"
									ng-model="ctrl.fiteredToDate"> <script
										type="text/javascript">
										$(".datepickerTo").datetimepicker({
									    	format: 'yyyy-mm-dd hh:ii', 
									    	language: 'it',
									    	autoclose: true,
									    	todayHighlight: true
										});
									</script> <span class="glyphicon glyphicon-erase"
									style="margin-top: 6px;" ng-click="ctrl.fiteredToDate=undefined;"></span>

								</td>

								<!--   <td class="fieldValue medium">

                            <div class="input-group">
							<span class="span10">
								 <select name="monthTo" id="monthTo" ng-model="selectedMonthTo" ng-options="item.name for item in ctrl.months track by item.id" required></select>
							</span>
                                <span class="span10">
								 <select ng-model="yearTo"	ng-options="n for n in [] | year_range:1900:2017" required></select>
							</span>
                            </div>


                        </td> -->

								<td class="fieldLabel medium" nowrap colspan="4"></td>
							</tr>
							<tr>
								<td class="fieldLabel medium" style="text-align: right" nowrap
									colspan="6">
									<button ng-disabled="ctrl.fiteredBroadcaster==undefined||ctrl.fiteredBroadcaster==null||ctrl.fiteredBroadcaster==''||
														ctrl.fiteredChannel==undefined||ctrl.fiteredChannel==null||ctrl.fiteredChannel==''||
														ctrl.fiteredFromDate==undefined||ctrl.fiteredFromDate==null||ctrl.fiteredFromDate==''||
														ctrl.fiteredFromDate==undefined||ctrl.fiteredFromDate==null||ctrl.fiteredFromDate==''||
														ctrl.fiteredToDate==undefined||ctrl.fiteredToDate==null||ctrl.fiteredToDate==''"
														class="btn addButton"
										ng-click="getUtilizzazioni()">
										<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Applica</strong>
									</button>
								</td>

							</tr>
						</tbody>
					</table>

				</div>
				<div>
					<!-- actions and pagination -->
					<div class="listViewTopMenuDiv noprint">
						<div class="listViewActionsDiv row-fluid">

							<span class="btn-toolbar span4"> <span class="btn-group"></span>
							</span> <span class="btn-toolbar span4"> <span
								class="customFilterMainSpan btn-group"
								style="text-align: center;">
									<div class="pageNumbers alignTop ">
										<!--  -- >
					 				<span class="pageNumbersText"><strong>Titolo</strong></span>
<!--  -->
									</div>
							</span>
							</span> <span class="span4 btn-toolbar">
								<div class="listViewActions pull-right">
									<span class="btn-group"></span>
								</div>
								<div class="clearfix"></div>
							</span>

						</div>
					</div>


					<!-- table -->
					<div class="listViewContentDiv" id="listViewContents">

						<div class="contents-topscroll noprint">
							<div class="topscroll-div" style="width: 95%">&nbsp;</div>
						</div>

						<div class="listViewEntriesDiv contents-bottomscroll">
							<div class="bottomscroll-div"
								style="width: 95%; min-height: 80px">

								<table class="table table-bordered listViewEntriesTable">
									<thead>
										<tr class="listViewHeaders">
											<th><a href="" class="listViewHeaderValues">Titolo
													Trasmissione</a></th>
											<th><a href="" class="listViewHeaderValues">Tipo
													Trasmissione </a></th>
											<th><a href="" class="listViewHeaderValues">Orario Inizio
													Trasmissione</a></th>
											<th><a href="" class="listViewHeaderValues">Orario Fine
													Trasmissione</a></th>
											<th><a href="" class="listViewHeaderValues">Durata </a></th>
											<th><a href="" class="listViewHeaderValues">Stato </a></th>
											<th></th>
											<th></th>
										</tr>
									</thead>
									<tbody>

										<tr class="listViewEntries"
											ng-repeat="item in ctrl.utilizzazioni">

											<td class="listViewEntryValue">{{item.titoloTrasmissione}}</td>
											<td class="listViewEntryValue">{{item.genereTrasmissione}}</td>
											<td class="listViewEntryValue">{{item.orarioInizioTrasmissione}}</td>
											<td class="listViewEntryValue">{{item.orarioFineTrasmissione}}</td>
											<td class="listViewEntryValue">{{item.durataTrasmissione}}</td>
											<td class="listViewEntryValue">{{getStato(item)}}</td>
											<td class="listViewEntryValue"><img height="25"
												width="25" src="{{getImgStato(item)}}"></td>

											<td class="listViewEntryValue ">
												<div class="pull-right"
													style="text-align: center; margin-right: 12px">
													<button ng-hide="item.amountUsed === 0"
														class="btn addButton" ng-click="showUtilizzazione(item)">
														<strong>Gestisci Utilizzazione</strong>
													</button>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- /table -->

				</div>
			</div>
		</div>

		<!-- TELEVISIONE -->
		<div
			ng-show="ctrl.selectedReport!=undefined&&ctrl.selectedReport!=null&&ctrl.selectedReport.tipo!='TELEVISIONE'"
			class="contentsDiv marginLeftZero" id="rightPanel"
			style="min-height: 80px;">
			<div class="listViewPageDiv">
				<!-- contenitore principale -->
				<div class="listViewActionsDiv row-fluid">
					<span class="pageNumbersText">

						<button class="btn addButton" ng-click="back()">
							<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Indietro</strong>
						</button>
					</span>
				</div>
			</div>
			<div class="listViewTopMenuDiv noprint">
				<div class="listViewActionsDiv row-fluid">

					<span class="btn-toolbar span4"> <span class="btn-group"></span>
					</span> <span class="btn-toolbar span4"> <span
						class="customFilterMainSpan btn-group" style="text-align: center;">
							<div class="pageNumbers alignTop ">

								<span class="pageNumbersText"><strong>Dettaglio
										Utilizzazione</strong></span>

							</div>
					</span>
					</span> <span class="span4 btn-toolbar">
						<div class="listViewActions pull-right">
							<span class="btn-group"></span>
						</div>
						<div class="clearfix"></div>
					</span>

				</div>
			</div>
			<div>
				<div class="listViewContentDiv" id="listViewContents">


					<div class="listViewEntriesDiv contents-bottomscroll">
						<div class="bottomscroll-div" style="width: 95%; min-height: 80px">

							<table class="table table-bordered blockContainer showInlineTable ">
								<thead>
									<tr class="listViewHeaders">
										<th colspan="6"><a href="" class="listViewHeaderValues">Trasmissione
										</a></th>
									</tr>
								</thead>
								<tbody>
									<tr
										ng-repeat="item in ctrl.dettaglioUtilizzazione.bdcCampoDTO track by $index"
										ng-if="$index % 2 == 0">

										<td class="fieldBg medium"><label
											class="muted pull-right marginRight10px">
												{{ctrl.dettaglioUtilizzazione.bdcCampoDTO[$index].nomeCampo}} </label></td>

										<td class="fieldValueBg medium">
											<div class="row-fluid">
												<span class="span10"> <input
													ng-disabled="!ctrl.dettaglioUtilizzazione.bdcCampoDTO[$index].editabile"
													ng-model="ctrl.dettaglioUtilizzazione.bdcCampoDTO[$index].valoreCampo">
												</span>
											</div>
										</td>

										<td class="fieldBg medium"><label
											class="muted pull-right marginRight10px">
												{{ctrl.dettaglioUtilizzazione.bdcCampoDTO[$index].errori}} </label></td>


										<td class="fieldBg medium">
											<label class="muted pull-right marginRight10px">
												{{ctrl.dettaglioUtilizzazione.bdcCampoDTO[$index+1].nomeCampo}} 
											</label>
										</td>

										<td class="fieldValueBg medium">
											<div class="row-fluid" ng-hide="!ctrl.dettaglioUtilizzazione.bdcCampoDTO[$index+1]">
												<span class="span10"> 
												<input
													ng-disabled="!ctrl.dettaglioUtilizzazione.bdcCampoDTO[$index+1].editabile"
													ng-model="ctrl.dettaglioUtilizzazione.bdcCampoDTO[$index+1].valoreCampo">
												</span>
											</div>
										</td>

										<td class="fieldBg medium">
											<label
												class="muted pull-right marginRight10px">
													{{ctrl.dettaglioUtilizzazione.bdcCampoDTO[$index+1].errori}} 
											</label>
										</td>
									</tr>

								</tbody>
							</table>
							<div class="contents-topscroll noprint">
								<div class="topscroll-div" style="width: 95%">&nbsp;</div>
							</div>
							<div ng-hide="!ctrl.dettaglioUtilizzazione.showMusicDTO"class="bottomscroll-div"
								style="width: 95%; min-height: 80px">
								<!-- actions and pagination -->
								<div class="listViewTopMenuDiv noprint">
									<div class="listViewActionsDiv row-fluid">

										<span class="btn-toolbar span4"> <span
											class="btn-group"></span>
										</span> <span class="btn-toolbar span4"> <span
											class="customFilterMainSpan btn-group"
											style="text-align: center;">
												<div class="pageNumbers alignTop ">

													<span class="pageNumbersText"><strong>Utilizzazioni
															musicali associate</strong></span>

												</div>
										</span>
										</span> <span class="span4 btn-toolbar">
											<div class="listViewActions pull-right">
												<span class="btn-group"></span>
											</div>
											<div class="clearfix"></div>
										</span>
									</div>
								</div>
								<table class="table table-bordered listViewEntriesTable">
									<thead>
										<tr class="listViewHeaders">
											<th><a href="" class="listViewHeaderValues">Titolo </a></th>
											<th><a href="" class="listViewHeaderValues">Primo compositore</a></th>
											<th><a href="" class="listViewHeaderValues">Secondo compositore</a></th>
											<th><a href="" class="listViewHeaderValues">Categoria d'uso </a></th>
											<th><a href="" class="listViewHeaderValues">Durata </a></th>
										</tr>
									</thead>
									<tbody>
										<tr class="listViewEntries"
											ng-repeat="item in ctrl.dettaglioUtilizzazione.showMusicDTO">
											<td class="listViewEntryValue">{{item.title}}</td>
											<td class="listViewEntryValue">{{item.composer}}</td>
											<td class="listViewEntryValue">{{item.secondComposer}}</td>
											<td class="listViewEntryValue">{{item.musicType.name}}</td>
											<td class="listViewEntryValue">{{convertSecond(item.realDuration)}}</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- RADIO -->
	</div>
</div>