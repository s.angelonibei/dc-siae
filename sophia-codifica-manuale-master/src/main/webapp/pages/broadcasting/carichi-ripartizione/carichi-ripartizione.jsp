<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@
	include file="../../navbar.jsp"%>

<div class="bodyContents">

	<div class="mainContainer row-fluid">

		<div id="companyLogo" class="navbar commonActionsContainer noprint">
			<div class="actionsContainer row-fluid">
				<div class="span2">
					<span class="companyLogo"><img src="images/logo_siae.jpg"
						title="SIAE" alt="SIAE">&nbsp;</span>
				</div>
				<span class="btn-toolbar span4">
					<div class="pageNumbers alignTop pull-right"
						style="text-align: center;">
						<span class="pageNumbersText"><strong>Carichi di
								Ripartizione</strong></span>
					</div>
				</span>
			</div>
		</div>

		<div class="contentsDiv marginLeftZero" id="rightPanel"
			style="min-height: 180px;">
			<div
				ng-hide="ctrl.dettailSelected!=null&&ctrl.dettailSelected!=undefined">
				<div class="listViewPageDiv">

					<div class="listViewTopMenuDiv noprint">
						<div class="listViewTopMenuDiv noprint">
							<div class="listViewActionsDiv row-fluid">

								<span class="btn-toolbar span4"> <span class="btn-group">

								</span>
								</span> <span class="btn-toolbar span4"> <span
									class="customFilterMainSpan btn-group"
									style="text-align: center;">
										<div class="pageNumbers alignTop "></div>
								</span>
								</span> <span class="span4 btn-toolbar">
									<div class="listViewActions pull-right">
										<!-- span class="btn-group">
										<button class="btn addButton" ng-click="showUpload($event)">
											<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;<strong>Nuovo</strong>
										</button>
									</span> -->
									</div>
									<div class="clearfix"></div>
								</span>

							</div>
						</div>
					</div>

					<!-- table -->
					<div class="listViewContentDiv" id="listViewContents">

						<!--  -- >
					 <div class="contents-topscroll noprint">
					 	<div class="topscroll-div" style="width: 95%;">&nbsp;</div>
					 </div>
<!--  -->
						<div class="listViewEntriesDiv">
							<div style="width: 100%; min-height: 400px">

								<table
									class="table table-bordered blockContainer showInlineTable ">
									<thead>
										<tr>
											<th class="blockHeader" colspan="12" ng-hide="ctrl.hideForm">
												Verifica Canale</th>
										</tr>
									</thead>

									<tbody ng-hide="ctrl.hideForm">

										<tr>
											<td class="fieldLabel medium"><label
												class="muted pull-right marginRight10px"> <span
													class="redColor" ng-show="form.process.$invalid">*</span>Data
													da
											</label></td>
											<td class="fieldLabel medium" nowrap><input type="text"
												class="span9 input-small datepickerFromRiparto"
												style="width: 180px" ng-model="ctrl.selectedPeriodFrom">
												<script type="text/javascript">
													$(".datepickerFromRiparto")
															.datepicker(
																	{
																		format : 'dd-mm-yyyy',
																		language : 'it',
																		viewMode : "days",
																		minViewMode : "days",
																		autoclose : true,
																		todayHighlight : true
																	});
												</script> <span class="glyphicon glyphicon-erase"
												style="margin-top: 6px;"
												ng-click="ctrl.selectedPeriodFrom=undefined;"></span></td>
											<!--   <td class="fieldValue medium">

                            <div class="input-group">
							<span class="span10">
								 <select name="monthFrom" id="monthFrom" ng-model="selectedMonthFrom" ng-options="item.name for item in ctrl.months track by item.id" required></select>
							</span>
                                <span class="span10">
								 <select ng-model="yearFrom"	ng-options="n for n in [] | year_range:1900:2017" required></select>
							</span>
                            </div>


                        </td> -->

											<td class="fieldLabel medium"><label
												class="muted pull-right marginRight10px"> <span
													class="redColor" ng-show="form.process.$invalid">*</span>Data
													a
											</label></td>
											<td class="fieldLabel medium"><input type="text"
												class="span9 input-small datepickerToRiparto"
												style="width: 180px" ng-model="ctrl.selectedPeriodTo">
												<script type="text/javascript">
													$(".datepickerToRiparto")
															.datepicker(
																	{
																		minDate: new Date(),
																		format : 'dd-mm-yyyy',
																		language : 'it',
																		viewMode : "days",
																		minViewMode : "days",
																		autoclose : true,
																		todayHighlight : true
																	});
												</script> <span class="glyphicon glyphicon-erase"
												style="margin-top: 6px;"
												ng-click="ctrl.selectedPeriodTo=undefined;"></span></td>

											<!--   <td class="fieldValue medium">

                            <div class="input-group">
							<span class="span10">
								 <select name="monthTo" id="monthTo" ng-model="selectedMonthTo" ng-options="item.name for item in ctrl.months track by item.id" required></select>
							</span>
                                <span class="span10">
								 <select ng-model="yearTo"	ng-options="n for n in [] | year_range:1900:2017" required></select>
							</span>
                            </div>


                        </td> -->

											<td class="fieldLabel medium" nowrap colspan="4"></td>
										</tr>

										<tr>
											<td class="fieldLabel medium"><label
												class="muted pull-right marginRight10px"> <span
													class="redColor" ng-show="form.process.$invalid">*</span>Tipo
													Emittente
											</label></td>
											<td class="fieldValue medium">
												<div class="row-fluid">
													<span class="span10"> <select name="tipoEmittente"
														id="tipoEmittente" ng-model="ctrl.selectedTipoBroadcaster"
														ng-options="item.nome for item in ctrl.tipiEmittenti"
														ng-change="getEmittenti(ctrl.selectedTipoBroadcaster.nome)">
													</select>
													</span>
												</div>
											</td>


											<td class="fieldLabel medium"><label
												class="muted pull-right "> <span class="redColor"
													ng-show="form.process.$invalid">* </span>Emittente
											</label></td>
											<td class="fieldValue medium">
												<div class="row-fluid">
													<span class="span10"> <select name="emittente"
														id="emittente" ng-change="getCanali(ctrl.emittente)"
														ng-model="ctrl.emittente"
														ng-options="item.nome for item in ctrl.listaEmittenti">
													</select>
													</span>
												</div>
											</td>

											<td class="fieldLabel medium"><label
												class="muted pull-right "> <span class="redColor"
													ng-show="form.process.$invalid">* </span>Canale
											</label></td>
											<td class="fieldValue medium">
												<div class="row-fluid">
													<span class="span10"> <select name="canale"
														id="canale" ng-model="ctrl.canale"
														ng-options="item.nome for item in ctrl.listaCanali">
													</select>
													</span>
												</div>
											</td>

										</tr>

										<tr>
											<td class="fieldLabel medium" style="text-align: right"
												nowrap colspan="12">

												<button
													ng-hide="
													ctrl.selectedPeriodTo==undefined||ctrl.selectedPeriodFrom==undefined"
													class="btn addButton"
													ng-click="download(ctrl.selectedTipoBroadcaster,ctrl.emittente,ctrl.canale,ctrl.anno,ctrl.selectedPeriodFrom,ctrl.selectedPeriodTo)">
													<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Download
														Csv</strong>
												</button>

												<button
													ng-hide="
													ctrl.selectedPeriodTo==undefined||ctrl.selectedPeriodFrom==undefined"
													class="btn addButton"
													ng-click="apply(ctrl.selectedTipoBroadcaster,ctrl.emittente,ctrl.canale,ctrl.anno,ctrl.selectedPeriodFrom,ctrl.selectedPeriodTo)">
													<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Applica</strong>
												</button>

											</td>
										</tr>
									</tbody>
								</table>
								<br>

								<!-- table -->
								<div class="listViewEntriesDiv contents-bottomscroll">
									<div class="bottomscroll-div"
										style="width: 95%; min-height: 80px">

										<table class="table table-bordered listViewEntriesTable ">
											<thead>
												<tr class="listViewHeaders">
													<th><a href="" ng-click="sort('TipoEmittente')"  class="listViewHeaderValues">Tipo<br>Emittente 
													<span class="glyphicon glyphicon-sort"></span></a></th>
													<th><a href="" ng-click="sort('Emittente')" class="listViewHeaderValues">Emittente
													<span  class="glyphicon glyphicon-sort"></span></a></th>
													<th><a href="" ng-click="sort('Canale')" class="listViewHeaderValues">Canale
													<span  class="glyphicon glyphicon-sort"></span></a></th>
													<th><a href="" ng-click="sort('DataDa')" class="listViewHeaderValues">Inizio<br>Competenza 
													<span  class="glyphicon glyphicon-sort"></span></a></th>
													<th><a href="" ng-click="sort('DataA')" class="listViewHeaderValues">Fine<br>Competenza
													<span  class="glyphicon glyphicon-sort"></span></a></th>
													<th><a href="" ng-click="sort('Incasso')" class="listViewHeaderValues">Incasso<br>netto (&euro;)
													<span  class="glyphicon glyphicon-sort"></span></a></th>
													<th><a href="" ng-click="sort('TipoDiritto')" class="listViewHeaderValues">Tipo<br>Diritto 
													<span  class="glyphicon glyphicon-sort"></span></a></th>
													<th><a href="" ng-click="sort('Consolidamento')" class="listViewHeaderValues">Consolidamento<br>Incassi
													<span  class="glyphicon glyphicon-sort"></span></a></th>
													<th><a href="" ng-click="sort('Sospeso')" class="listViewHeaderValues">Sospeso per<br>Contenzioso
													<span  class="glyphicon glyphicon-sort"></span></a></th>
													<th><a href=""  class="listViewHeaderValues">Note<br>da leggere
													</a></th>
													<th><a href="" ng-click="sort('Report')" class="listViewHeaderValues">Report<br>Disponibile
													<span  class="glyphicon glyphicon-sort"></span></a></th>
													<th><a href="" ng-click="sort('ImportoRipartibile')" class="listViewHeaderValues">Importo<br>Ripartibile
													<span  class="glyphicon glyphicon-sort"></span></a></th>
													<th></th>
													<th></th>
													<th></th>
												</tr>
											</thead>
											<tbody>

												<tr class="listViewEntries "
													ng-repeat="item in ctrl.carichiRipartizione |orderBy : ctrl.sortType">

													<td class="listViewEntryValue ">{{item.canale.bdcBroadcasters.tipo_broadcaster}}</td>
													<td class="listViewEntryValue ">{{item.canale.bdcBroadcasters.nome}}</td>
													<td class="listViewEntryValue ">{{item.canale.nome}}</td>

													<td class="listViewEntryValue ">{{formatDate(item.inizioPeriodoCompetenza)}}</td>
													<td class="listViewEntryValue ">{{formatDate(item.finePeriodoCompetenza)}}</td>
													<td class="listViewEntryValue ">{{formatNumber(item.incassoNetto)}}</td>
													<td class="listViewEntryValue ">{{item.tipoDiritto}}</td>
													<td class="listViewEntryValue ">{{item.incasso}}</td>
													<td class="listViewEntryValue ">{{getTextFromBool(item.sospesoPerContenzioso)}}</td>
													<td class="listViewEntryValue ">{{getNoteDisponibili(item.notaCommentoUffEmittenti,item.notaCommentoDirRipartizione)}}</td>

													<td class="listViewEntryValue "
														style="padding: 15px !important;"><span
														style="float: left;">{{getTextReport(item.repoDisponibile)}}
													</span> <img style="margin-left: 10px; width: 20px; height: 20px;"
														ng-show="item.repoDisponibile&&!item.kpiValido"
														src="images/warning.png"
														title="i KPI non superano le soglie" alt="Warning">
													</td>
													
													<td class="listViewEntryValue ">{{getTextImportoRipartibile(item)}}</td>

													<td class="listViewEntryValue ">
														<button ng-hide="getTextImportoRipartibile(item)!='Si'" class="btn addButton">
															<strong>Manda in ripartizione</strong>
														</button>
													</td>
													<td class="listViewEntryValue "><span
														ng-click="editCaricoRiparto(item)"
														class="glyphicon glyphicon-edit"></span></td>

													<td class="listViewEntryValue "><span
														ng-click="showCaricoRiparto(item)"
														class="glyphicon glyphicon-list-alt"></span></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<!-- /table -->

							</div>
						</div>
					</div>
				</div>
			</div>
			<div
				ng-hide="ctrl.dettailSelected==null||ctrl.dettailSelected==undefined">

				<br> <br>
				<div id="top_of_dialog">
					<strong>Dettaglio Carico Riparto</strong>
				</div>
				<br> <br>
				<button class="btn addButton" ng-click="back()">
					<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Indietro</strong>
				</button>
				<button ng-hide="getTextImportoRipartibile(ctrl.caricoRiparto)!='Si'" class="btn addButton pull-right">
					<strong>Manda in ripartizione</strong>
				</button>
				<br> <br>
				<table class="table table-bordered listViewEntriesTable">
					<tbody>

						<tr class="listViewEntries">
							<td class="fieldLabel medium">Tipo Emittente</td>
							<td class="listViewEntryValue medium">{{ctrl.caricoRiparto.canale.bdcBroadcasters.tipo_broadcaster}}</td>

							<td class="fieldLabel medium">Emittente</td>
							<td class="listViewEntryValue medium">{{ctrl.caricoRiparto.canale.bdcBroadcasters.nome}}</td>

						</tr>
						<tr>
							<td class="fieldLabel medium">Canale</td>
							<td class="listViewEntryValue medium">{{ctrl.caricoRiparto.canale.nome}}</td>

							<td class="fieldLabel medium"></td>
							<td class="listViewEntryValue medium"></td>

						</tr>
						<tr>
							<td class="fieldLabel medium">Data da</td>
							<td class="listViewEntryValue medium">
								{{formatDate(ctrl.caricoRiparto.inizioPeriodoCompetenza)}}
							</td>

							<td class="fieldLabel medium">Data a</td>
							<td class="listViewEntryValue medium">
								{{formatDate(ctrl.caricoRiparto.finePeriodoCompetenza)}}
							</td>
						</tr>
						<tr>
							<td class="fieldLabel medium">Incasso netto (&euro;)</td>
							<td class="listViewEntryValue medium">{{formatNumber(ctrl.caricoRiparto.incassoNetto)}}</td>

							<td class="fieldLabel medium">Tipo Diritto</td>
							<td class="listViewEntryValue medium">{{ctrl.caricoRiparto.tipoDiritto}}</td>
						</tr>
						<tr>
							<td class="fieldLabel medium ">Consolidamento<br>Incassi</td>
							<td class="listViewEntryValue medium">{{ctrl.caricoRiparto.incasso}}</td>

							<td class="fieldLabel medium ">Sospeso per Contenzioso</td>
							<td class="listViewEntryValue medium">{{getTextFromBool(ctrl.caricoRiparto.sospesoPerContenzioso)}}</td>

						</tr>
						<tr>
							<td class="fieldLabel medium">Nota Commento Ufficio
								Emittenti</td>
							<td class="listViewEntryValue medium fixedSize"><span
								class="fixedSize">{{ctrl.caricoRiparto.notaCommentoUffEmittenti}}</span></td>

							<td class="fieldLabel medium ">Report Disponibile</td>
							<td class="listViewEntryValue medium"><span
														style="float: left;">{{getTextReport(ctrl.caricoRiparto.repoDisponibile)}}
													</span> <img style="margin-left: 10px; width: 20px; height: 20px;"
														ng-show="ctrl.caricoRiparto.repoDisponibile&&!ctrl.caricoRiparto.kpiValido"
														src="images/warning.png"
														title="i KPI non superano le soglie" alt="Warning">
													</td>
						</tr>
						<tr>
							<td class="fieldLabel medium">Nota Commento Direttore
								Riparizione</td>
							<td class="listViewEntryValue medium fixedSize"><span
								class="fixedSize">{{ctrl.caricoRiparto.notaCommentoDirRipartizione}}</span></td>

							<td class="fieldLabel medium ">Importo Ripartibile</td>
							<td class="listViewEntryValue medium">{{getTextImportoRipartibile(ctrl.caricoRiparto)}}</td>
						</tr>
					</tbody>
				</table>
				<br>
				<br>


				<div id="top_of_dialog">
					<strong>Kpi Relativo</strong>
				</div>
				<br>
				<div id="top_of_dialog" ng-hide="ctrl.split_items">Kpi non
					calcolati</div>

				<div id="top_of_dialog" ng-show="ctrl.split_items">
					<table ng-hide="isRadio()"
						class="table table-bordered listViewEntriesTable">
						<thead>
							<tr>
								<th class="blockHeader" colspan="5" ng-hide="ctrl.hideForm">
									Vincoli ripartizione KPI selezionato (TV)</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>% completezza palinsesto >70%</td>
								<td>% musica dichiarata >30%</td>
								<td>% Opere musicali con titolo erroneo: <30%</td>
								<td>% Opere filmiche con titolo erroneo: <30%</td>
								<td>% Autori con titolo erroneo: <30%</td>
							</tr>
						</tbody>
					</table>

					<table ng-show="isRadio()"
						class="table table-bordered listViewEntriesTable">
						<thead>
							<tr>
								<th class="blockHeader" colspan="3" ng-hide="ctrl.hideForm">
									Vincoli ripartizione KPI selezionato (RADIO)</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>% Durata brani dichiarati: >60%</td>
								<td>% Brani radio con titoli erronei: <30%</td>
								<td>% Autori radio con titoli erronei: <30%</td>
							</tr>
						</tbody>
					</table>
				</div>
				<br> <br>
				<div class="listViewEntriesDiv ">
					<div align="center" class="bottomscroll-div"
						style="width: 100%; min-height: 80px">
						<div class="Row" ng-repeat="items in ctrl.split_items">
							<div class="Column" align="center" ng-repeat="model in items">
								<ng-gauge ng-hide="model.valore==undefined||model.valore<0"
									size="120" type="full" thick="10" min="0" max="100"
									value="model.valore" append="%" cap="round"
									foreground-color="#575E77"
									background-color="rgba(155,155,155, 0.4)"> </ng-gauge>
								<div class="Row medium">{{model.kpi}}</div>
								<div class="Row medium">{{model.descKpi}}</div>


							</div>
						</div>
					</div>
				</div>

				<div id="top_of_dialog">
					<strong>Storico modifiche</strong>
				</div>

				<div style="margin-top: 15px">
					<table class="table table-bordered listViewEntriesTable">
						<thead>
							<tr class="listViewHeaders">
								<th><a href="" class="listViewHeaderValues fixedSize">Nome
										Campo </a></th>
								<th><a href="" class="listViewHeaderValues fixedSize">Vecchio
										Valore </a></th>
								<th><a href="" class="listViewHeaderValues fixedSize">Nuovo
										Valore </a></th>
								<th><a href="" class="listViewHeaderValues fixedSize">Utente
										Ultima Modifica</a></th>
								<th><a href="" class="listViewHeaderValues fixedSize">Data
										Ultima Modifica</a></th>

							</tr>
						</thead>
						<tbody>

							<tr ng-hide="checkSize()" class="listViewEntries"
								ng-repeat="item in ctrl.storico">
								<td class="listViewEntryValue fixedSize">{{item.nomeCampo}}</td>
								<td class="listViewEntryValue fixedSize">{{getTextFromBool(item.valoreOld)}}</td>
								<td class="listViewEntryValue fixedSize">{{getTextFromBool(item.valoreNew)}}</td>
								<td class="listViewEntryValue fixedSize">{{item.username}}</td>
								<td class="listViewEntryValue fixedSize">{{formatDateHour(item.dataModifica)}}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>


	</div>


</div>