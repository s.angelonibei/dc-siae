<%@ page import="com.google.inject.Injector"%><%@
	page
	import="com.google.inject.Key"%><%@
	page
	import="com.google.inject.name.Names"%><%@
	page language="java"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	final Injector injector = (Injector) pageContext.getServletContext().getAttribute(Injector.class.getName());
	final String userName = (String) session.getAttribute("sso.user.userName");
	final String homeUrl = injector.getInstance(Key.get(String.class, Names.named("home.url")));
%>
<form name="form" novalidate>

	<div id="top_of_dialog">
		<strong>Modifica Carico Ripartizione</strong>
	</div>

	<div style="margin-top: 15px">
		<table
			class="table table-bordered blockContainer showInlineTable equalSplit">
			<thead>
				<tr>
					<th class="blockHeader" colspan="2">Carico Ripartizione</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="fieldLabel medium"><label
						class="muted pull-right marginRight10px"> <span
							class="redColor" ng-show="form.process.$invalid">*</span>Consolidamento<br>Incassi
					</label></td>
					<td class="fieldValue medium">
						<div class="row-fluid">
							<span class="span10"> 
								<select name="Incasso"
									id="Incasso" ng-model="ctrl.item.incasso"
									ng-options="item for item in ctrl.incassi">
								</select>
							</span>
						</div>
					</td>
				</tr>
				
				<tr>
					<td class="fieldLabel medium"><label
						class="muted pull-right marginRight10px"> <span
							class="redColor" ng-show="form.process.$invalid">*</span>Sospeso per contenzioso
					</label></td>
					<td class="fieldValue medium">
						<div class="row-fluid">
							<span class="span10"> <input type="checkbox"
								ng-model="ctrl.item.sospesoPerContenzioso" />
							</span>
						</div>
					</td>
				</tr>
				
				<tr>
					<td class="fieldLabel medium"><label
						class="muted pull-right marginRight10px"> <span
							class="redColor" ng-show="form.process.$invalid">*</span>Importo ripartibile
					</label></td>
					<td class="fieldValue medium">
						<div class="row-fluid">
							<span class="span10"> <input type="checkbox"
								ng-model="ctrl.item.importoRipartibile" />
							</span>
						</div>
					</td>
				</tr>
				
				<tr>
					<td class="fieldLabel medium"><label
						class="muted pull-right marginRight10px"> <span
							class="redColor" ng-show="form.process.$invalid">*</span>Nota commento Uff. Emittenti<br>(massimo 200 caratteri)
					</label></td>
					<td class="fieldValue medium">
						<div class="row-fluid">
							<span class="span10">
							 <textarea id="txtArea" rows="4"  maxlength="200"
									cols="70" class="form-control span4" id="name"
									placeholder="Inserisci la nota Uff. Emittenti" ng-model="ctrl.item.notaCommentoUffEmittenti"
									 />
							
							</span>
						</div>
					</td>
				</tr>
				
				<tr>
					<td class="fieldLabel medium"><label
						class="muted pull-right marginRight10px"> <span
							class="redColor" ng-show="form.process.$invalid">*</span>Nota commento Dir. Ripartizione<br>(massimo 200 caratteri)
					</label></td>
					<td class="fieldValue medium">
						<div class="row-fluid">
							<span class="span10">
							 <textarea id="txtArea" rows="4" maxlength="200"
									cols="70" class="form-control span4" id="name"
									placeholder="Inserisci la nota Dir. Ripartizione" ng-model="ctrl.item.notaCommentoDirRipartizione"
									 />
							
							</span>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div style="margin-top: 15px">
		<table width="100%">
			<tr>
				<td>
					<button type="button" class="btn addButton" ng-click="cancel()">
						<span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp;<strong>Annulla</strong>
					</button>
				</td>
				<td align="right">
					<button ng-disabled="form.$invalid || licencesNumber() < 1"
						class="btn addButton" ng-click="showConfirmAlert('<%=userName%>')">
						<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;<strong>Modifica</strong>
					</button>
				</td>
			</tr>
		</table>
	</div>

</form>

<script type="text/ng-template" id="errorDialogId">

        <div id="target"  >
          <strong>{{ngDialogData}}</strong>
        </div>
    </script>

<script type="text/javascript">
	$('#top_of_dialog')[0].scrollIntoView(true);
</script>
