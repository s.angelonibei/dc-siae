<%@ page  contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@include file="../../navbar.jsp"%>
<div class="bodyContents">
	<div class="mainContainer row-fluid">
		<div id="companyLogo" class="navbar commonActionsContainer noprint">
			<div class="actionsContainer row-fluid">
				<div class="span2">
         <span class="companyLogo"><img src="images/logo_siae.jpg"
         title="SIAE" alt="SIAE">&nbsp;</span>
				</div>
				<span class="btn-toolbar span4">
         <div class="pageNumbers alignTop pull-right"
			  style="text-align: center;">
            <span class="pageNumbersText"><strong>News</strong></span>
         </div>
      </span>
			</div>
		</div>
		<div class="contentsDiv marginLeftZero" id="rightPanel"
			 style="min-height: 80px;">
			<div class="listViewPageDiv">
				<div class="listViewActionsDiv row-fluid">
					<span class="btn-toolbar span4"> </span> <span
						class="btn-toolbar span4"> </span>
					<span class="span4 btn-toolbar">
            <div class="listViewActions pull-right">
               <span class="btn-group pull-right">
               <button class="btn addButton" ng-click="showAddNews($event)">
               <span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;<strong>Aggiungi
               news</strong>
               </button>
               </span>
            </div>
            <div class="clearfix"></div>
         </span>
				</div>
				<!-- contenitore principale -->
				<div class="listViewActionsDiv row-fluid">
					<table
							class="table table-bordered blockContainer showInlineTable">
						<thead>
						<tr>
							<th class="blockHeader" colspan="8" ng-show="ctrl.hideForm"><span
									ng-click="ctrl.hideForm=false"
									class="glyphicon glyphicon-expand"></span>&nbsp;Parametri
								Ricerca
							</th>
							<th class="blockHeader" colspan="8" ng-hide="ctrl.hideForm"><span
									ng-click="ctrl.hideForm=true"
									class="glyphicon glyphicon-collapse-down"></span>&nbsp;Parametri
								Ricerca
							</th>
						</tr>
						</thead>
						<tbody ng-hide="ctrl.hideForm">
						<tr>
							<td class="fieldBg medium">
								<label
										class="muted pull-right marginRight10px"> Titolo </label>
							</td>
							<td class="fieldBg medium">
								<div class="row-fluid">
                        <span>
                        <input
								style="width: 180px"
								ng-model="ctrl.filteredTitle">
                        </span>
									<span class="glyphicon glyphicon-erase"
										  style="margin-top: 6px;"
										  ng-click="ctrl.filteredTitle=undefined;">
								</div>
							</td>
							<td
									class="fieldBg medium"><label
									class="muted pull-right marginRight10px"> Testo </label>
							</td>
							<td class="fieldBg medium">
								<div class="row-fluid">
                        <span>
                        <input
								style="width: 180px"
								ng-model="ctrl.filteredText">
                        </span>
									<span class="glyphicon glyphicon-erase"
										  style="margin-top: 6px;"
										  ng-click="ctrl.filteredText=undefined;"/>
								</div>
							</td>
							<td class="fieldBg medium">
								<label
										class="muted pull-right "> <span class="redColor"
																		 ng-show="form.process.$invalid">* </span>Emittente
								</label>
							</td>
							<td class="fieldBg medium">
								<div class="row-fluid" style="width: 200px" >
									<div
											style="width: 180px;float: left;"
											ng-dropdown-multiselect=""
											options="ctrl.listaEmittentiLabel"
											selected-model="ctrl.filteredBroadcaster"
											checkboxes="false"
											extra-settings="multiselectSettings">
									</div>
									<span
											class="glyphicon glyphicon-erase"
											style="margin-top: 6px;float: right;"
											ng-click="ctrl.filteredBroadcaster=[];">
                        </span>
								</div>
							</td>
							<td class="fieldBg medium"><label
									class="muted pull-right "> <span class="redColor"
																	 ng-show="form.process.$invalid">* </span>Stato
							</label>
							</td>
							<td class="fieldBg medium">
								<div class="row-fluid" style="width: 200px" >
									<select
											style="width: 180px;float: left;"
											name="stato"
											id="emittente"
											ng-model="ctrl.filteredStato"
											ng-options="item.name for item in ctrl.listaStati">
									</select>
									<span class="glyphicon glyphicon-erase"
										  style="margin-top: 6px;float: right;"
										  ng-click="ctrl.filteredStato=undefined;"/>
								</div>
							</td>
						</tr>
						<tr>
							<td class="fieldBg medium">
								<label
										class="muted pull-right marginRight10px"> <span
										class="redColor" ng-show="form.process.$invalid">*</span>Data Creazione da
								</label>
							</td>
							<td class="fieldBg medium" nowrap>
								<input type="text"
									   class="span9 input-small datepickerCreationFrom" style="width: 180px"
									   ng-model="ctrl.filteredCreationFrom">
								<script type="text/javascript">
									$(".datepickerCreationFrom")
											.datepicker(
													{
														format: 'yyyy-mm-dd',
														language: 'it',
														viewMode : "days",
														minViewMode : "days",
														orientation : "bottom auto",
														autoclose : "true"
													});
								</script>
								<span class="glyphicon glyphicon-erase"
									  style="margin-top: 6px;"
									  ng-click="ctrl.filteredCreationFrom=undefined;"></span>
							</td>
							<td class="fieldBg medium"><label
									class="muted pull-right marginRight10px"> <span
									class="redColor" ng-show="form.process.$invalid">*</span>Data Creazione a
							</label>
							</td>
							<td class="fieldBg medium" nowrap>
								<input type="text"
									   class="span9 input-small datepickerCreationTo" style="width: 180px"
									   ng-model="ctrl.filteredCreationTo">
								<script type="text/javascript">
									$(".datepickerCreationTo")
											.datepicker(
													{
														format: 'yyyy-mm-dd',
														language: 'it',
														viewMode : "days",
														minViewMode : "days",
														orientation : "bottom auto",
														autoclose : "true"
													});
								</script>
								<span class="glyphicon glyphicon-erase"
											style="margin-top: 6px;" ng-click="ctrl.filteredCreationTo=undefined;"></span>
							</td>
							<td class="fieldBg medium">
								<label
										class="muted pull-right marginRight10px"> <span
										class="redColor" ng-show="form.process.$invalid">*</span>Valida da
								</label>
							</td>
							<td class="fieldBg medium" nowrap>
								<input type="text"
									   class="span9 input-small datepickerFrom" style="width: 180px"
									   ng-model="ctrl.fiteredFromDate">
								<script type="text/javascript">
									$(".datepickerFrom")
											.datepicker(
													{
														format: 'yyyy-mm-dd',
														language: 'it',
														viewMode : "days",
														minViewMode : "days",
														orientation : "bottom auto",
														autoclose : "true"
													});
								</script>
								<span class="glyphicon glyphicon-erase"
									  style="margin-top: 6px;"
									  ng-click="ctrl.fiteredFromDate=undefined;"></span>
							</td>
							<!--   <td class="fieldValue medium">
                               <div class="input-group">
                               <span>
                               <select name="monthFrom" id="monthFrom" ng-model="selectedMonthFrom" ng-options="item.name for item in ctrl.months track by item.id" required></select>
                               </span>
                                   <span>
                               <select ng-model="yearFrom"	ng-options="n for n in [] | year_range:1900:2017" required></select>
                               </span>
                               </div>


                               </td> -->
							<td class="fieldBg medium"><label
									class="muted pull-right marginRight10px" nowrap> <span
									class="redColor" ng-show="form.process.$invalid">*</span>Valida a
							</label>
							</td>
							<td class="fieldBg medium" nowrap>
								<input type="text"
									   class="span9 input-small datepickerTo" style="width: 180px"
									   ng-model="ctrl.fiteredToDate">
								<script type="text/javascript">
									$(".datepickerTo")
											.datepicker(
													{
														format: 'yyyy-mm-dd',
														language: 'it',
														viewMode : "days",
														minViewMode : "days",
														orientation : "bottom auto",
														autoclose : "true"
													});
								</script>
								<span class="glyphicon glyphicon-erase"
									  style="margin-top: 6px;" ng-click="ctrl.fiteredToDate=undefined;"></span>
							</td>

						</tr>
						<tr>
							<td class="fieldLabel medium" style="text-align: right" nowrap
								colspan="8">
								<button class="btn addButton" ng-click="downloadCsv()">
									<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Scarica
									Csv</strong>
								</button>
								<button class="btn addButton" ng-click="filterApply()">
									<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Applica</strong>
								</button>
							</td>
						</tr>
						</tbody>
					</table>
				</div>
				<div>
					<!-- actions and pagination -->
					<!-- actions and pagination -->
					<div class="listViewTopMenuDiv noprint">
						<div class="listViewActionsDiv row-fluid">
							<!--  -->
							<span class="btn-toolbar span4"> <span class="btn-group">
               </span>
               </span>
							<!--  -->
							<!--  -->
							<span class="btn-toolbar span4">
                  <span
						  class="customFilterMainSpan btn-group"
						  style="text-align: center;">
                     <div class="pageNumbers alignTop ">
                        <span class="pageNumbersText">
                           <strong>
                              <!-- Titolo -->
                           </strong>
                        </span>
                     </div>
                  </span>
               </span>
							<!--  -->
							<!--  -->
							<span class="span4 btn-toolbar">
                  <div class="listViewActions pull-right">
                     <div class="pageNumbers alignTop ">
                        <span ng-show="ctrl.news.hasPrev || ctrl.news.hasNext">
                        <span class="pageNumbersText" style="padding-right: 5px">
                        Record da <strong>{{ctrl.news.first+1}}</strong> a <strong>{{ctrl.news.last}}</strong>
                        </span>
                        <button title="Precedente" class="btn"
								id="listViewPreviousPageButton" type="button"
								ng-click="navigateToPreviousPage()"
								ng-show="ctrl.news.hasPrev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        </button>
                        <button title="Successiva" class="btn"
								id="listViewNextPageButton" type="button"
								ng-click="navigateToNextPage()" ng-show="ctrl.news.hasNext">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        </button>
                        </span>
                     </div>
                  </div>
                  <div class="clearfix"></div>
               </span>
							<!--  -->
						</div>
					</div>
					<!-- table -->
					<div class="listViewContentDiv" id="listViewContents">
						<div class="contents-topscroll noprint">
							<div class="topscroll-div" style="width: 95%">&nbsp;</div>
						</div>
						<div class="listViewEntriesDiv contents-bottomscroll">
							<div class="bottomscroll-div"
								 style="width: 95%; min-height: 80px">
								<table class="table table-bordered listViewEntriesTable">
									<thead>
									<tr class="listViewHeaders">
										<th><a href="" 	ng-click="sort('Titolo')" class="listViewHeaderValues">Titolo <span class="glyphicon glyphicon-sort"></span></a></th>
										<th><a href="" 	ng-click="sort('Testo')" class="listViewHeaderValues">Testo <span class="glyphicon glyphicon-sort"></span></a></th>
										<th><a href="" 	ng-click="sort('DataCreazione')" class="listViewHeaderValues">Data di creazione<span class="glyphicon glyphicon-sort"></span></a></th>
										<th><a href="" 	ng-click="sort('ValidaDa')" class="listViewHeaderValues">Valida da<span class="glyphicon glyphicon-sort"></span></a></th>
										<th><a href="" 	ng-click="sort('ValidaA')" class="listViewHeaderValues">Valida a<span class="glyphicon glyphicon-sort"></span></a></th>
										<th><a href="" 	ng-click="sort('Stato')" class="listViewHeaderValues">Stato<span class="glyphicon glyphicon-sort"></span></a></th>
										<th style="text-align: center;" align="center"><a
												href="" style="text-align: center;"></a></th>
										<th style="text-align: center;" align="center"><a
												href="" style="text-align: center;">Disattiva</a></th>
									</tr>
									</thead>
									<tbody>
									<tr class="listViewEntries" ng-repeat="item in ctrl.news.rows |orderBy : ctrl.sortType">
										<td class="listViewEntryValue ">{{item.title}}</td>
										<td class="listViewEntryValue ">{{item.news}}</td>
										<td class="listViewEntryValue ">{{formatDateHour(item.insertDate)}}</td>
										<td class="listViewEntryValue ">{{formatDate(item.validFrom)}}</td>
										<td class="listViewEntryValue ">{{formatDate(item.validTo)}}</td>
										<td class="listViewEntryValue ">{{item.flagActive ?
											"ATTIVA" : "DISATTIVA"}}
										</td>
										<td class="listViewEntryValue ">
											<div align="center">
											 <span ng-hide="!item.flagActive"
												   ng-click="showUpdateNews(item)"
												   class="	glyphicon glyphicon-edit">

											 </span>
											 <span
													ng-hide="item.flagActive"
													ng-click="showNewsDettails(item)"
													class="glyphicon glyphicon-duplicate">
											 </span>
											</div>
										</td>
										<td class="listViewEntryValue " align="center">
											<div align="center">
                                 <span ng-hide="!item.flagActive"
									   ng-click="showConfirmAlert(item)"
									   class="glyphicon glyphicon-eye-close"></span>
											</div>
										</td>
									</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>