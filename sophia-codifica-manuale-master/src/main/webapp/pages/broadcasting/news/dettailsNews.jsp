<%@ page import="com.google.inject.Injector"%><%@
	page
	import="com.google.inject.Key"%><%@
	page
	import="com.google.inject.name.Names"%><%@
	page language="java"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	final Injector injector = (Injector) pageContext.getServletContext().getAttribute(Injector.class.getName());
	final String userName = (String) session.getAttribute("sso.user.userName");
	final String homeUrl = injector.getInstance(Key.get(String.class, Names.named("home.url")));
%>
<form name="form" novalidate>


	<div id="top_of_dialog">
		<strong>Riattiva News</strong>
	</div>

	<div style="margin-top: 15px">
		<table
			class="table table-bordered blockContainer showInlineTable equalSplit">
			<thead>
				<tr>
					<th class="blockHeader" colspan="2">News</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="fieldLabel medium"><label
						class="muted pull-right marginRight10px"> <span
							class="redColor" ng-show="form.process.$invalid">*</span>Titolo
					</label></td>
					<td class="fieldValue medium">
						<div class="row-fluid">
							<span class="span10"> <input type="text"
								class="form-control span4" id="name"
								placeholder="Titolo della news" ng-model="ctrl.selectedTitle"
								required>
							</span> 
						</div>
					</td>
				</tr>

				<tr>
					<td class="fieldLabel medium"><label
						class="muted pull-right marginRight10px"> <span
							class="redColor" ng-show="form.process.$invalid">*</span>Tipo
							Emittente
					</label></td>
					<td class="fieldValue medium">
						<div class="row-fluid">
							<span class="span10"> <select name="tipoEmittente"
								id="tipoEmittente" ng-model="ctrl.selectedTipoBroadcaster"
								ng-options="item.nome for item in ctrl.tipiEmittenti track by item.nome"
								ng-change="getEmittenti(ctrl.selectedTipoBroadcaster.nome)">
							</select>
							</span>
						</div>
					</td>
				</tr>

				<tr>
					<td class="fieldLabel medium"><label
						class="muted pull-right marginRight10px"> <span
							class="redColor" ng-show="form.process.$invalid">*</span>Emittente
					</label></td>
					<td class="fieldValue medium">
						<div class="row-fluid">
							<span class="span10"> <select name="emittente"
								id="emittente" ng-model="ctrl.selectedBroadcaster"
								ng-options="item.nome for item in ctrl.emittenti track by item.id"
								ng-change="search(ctrl.selectedBroadcaster.id)">
							</select>
							</span>
						</div>
					</td>
				</tr>

				<tr>
					<td class="fieldLabel medium"><label
						class="muted pull-right marginRight10px"> <span
							class="redColor" ng-show="form.process.$invalid">*</span>Utente
					</label></td>
					<td class="fieldValue medium">
						<div class="row-fluid">
							<span class="span10"> <select name="utente" id="utente"
								ng-model="ctrl.selectedUser"
								ng-options="item.username for item in ctrl.utenti track by item.username">
							</select>
							</span>
						</div>
					</td>
				</tr>

				<tr>
				<tr>
					<td class="fieldLabel medium"><label
						class="muted pull-right marginRight10px"> <span
							class="redColor" ng-show="form.process.$invalid">*</span>Testo
					</label></td>
					<td class="fieldValue medium">
						<div class="row-fluid">
							<span class="span10"> <textarea id="txtArea" rows="10"
									cols="70" class="form-control span4" id="name"
									placeholder="Testo della news" ng-model="ctrl.selectedText"
									required />
							</span>
						</div>
					</td>
				</tr>

				<tr>
					<td class="fieldLabel medium">
						<div class="control-group">
							<label class="control-label" for="licFrom"> <span
								class="redColor" ng-show="fromInvalid">*</span>Validit&agrave da
							</label>

						</div>
					</td>
					<td>
						<div class="controls">
							<div class="input-append date" id="datepickerRiattivaFrom"
								data-date-format="dd-mm-yyyy">

								<input placeholder="Oggi" type="text" name="date"
									ng-model="ctrl.selectedDateFrom"
									style="width: 173px; height: 25px"> <span
									class="add-on" style="height: 25px"><i class="icon-th"></i></span>
												
							 	<span class="glyphicon glyphicon-erase"
									style="margin-top: 6px;margin-left: 6px;"
									ng-click="ctrl.selectedDateFrom=undefined;"></span>
							</div>
							<script type="text/javascript">
								$("#datepickerRiattivaFrom")
									.datepicker(
										{
									  	  	language: 'it',
											format : 'dd-mm-yyyy',
											viewMode : "days",
											minViewMode : "days",
											orientation : "bottom auto",
											autoclose : "true"
										});
							</script>
						</div>
					</td>
				</tr>
				<tr>
					<td class="fieldLabel medium">
						<div class="control-group">
							<label class="control-label" for="licFrom"> <span
								class="redColor" ng-show="fromInvalid">*</span>Validit&agrave a
							</label>
					</td>
					<td>
						<div class="controls">
							<div class="input-append date" id="datepickerRiattivaTo"
								data-date-format="dd-mm-yyyy">

								<input placeholder="Sempre" type="text" name="date"
									ng-model="ctrl.selectedDateTo"
									style="width: 173px; height: 25px"> <span
									class="add-on" style="height: 25px"><i class="icon-th"></i></span>
												
							 	<span class="glyphicon glyphicon-erase"
									style="margin-top: 6px;margin-left: 6px;"
									ng-click="ctrl.selectedDateTo=undefined;"></span>
							</div>
							<script type="text/javascript">
								$("#datepickerRiattivaTo")
									.datepicker(
										{
									   	 	language: 'it',
											format : 'dd-mm-yyyy',
											viewMode : "days",
											minViewMode : "days",
											orientation : "bottom auto",
											autoclose : "true"
										});
							</script>
						</div>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div style="margin-top: 15px">
		<table width="100%">
			<tr>
				<td>
					<button type="button" class="btn addButton" ng-click="cancel()">
						<span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp;<strong>Annulla</strong>
					</button>
				</td>
				<td align="right">
					<button ng-disabled="form.$invalid || licencesNumber() < 1"
						class="btn addButton" ng-click="save('<%=userName%>')">
						<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;<strong>Duplica</strong>
					</button>
				</td>
			</tr>
		</table>
	</div>

</form>

<script type="text/ng-template" id="errorDialogId">

        <div id="target"  >
          <strong>{{ngDialogData}}</strong>
        </div>
    </script>

<script type="text/javascript">
	$('#top_of_dialog')[0].scrollIntoView(true);
</script>
