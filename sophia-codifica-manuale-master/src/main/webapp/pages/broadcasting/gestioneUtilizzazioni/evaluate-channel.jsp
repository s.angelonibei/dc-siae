<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@
	include file="../../navbar.jsp"%>

<div class="bodyContents">

	<div class="mainContainer row-fluid">

		<div id="companyLogo" class="navbar commonActionsContainer noprint">
			<div class="actionsContainer row-fluid">
				<div class="span2">
					<span class="companyLogo"><img src="images/logo_siae.jpg"
						title="SIAE" alt="SIAE">&nbsp;</span>
				</div>
				<span class="btn-toolbar span4">
					<div class="pageNumbers alignTop pull-right"
						style="text-align: center;">
						<span class="pageNumbersText"><strong>Monitoraggio
								Utilizzazioni</strong></span>
					</div>
				</span>
			</div>
		</div>

		<div class="contentsDiv marginLeftZero" id="rightPanel"
			style="min-height: 180px;">
			<div class="listViewPageDiv">

				<div class="listViewTopMenuDiv noprint">
					<div class="listViewTopMenuDiv noprint">
						<div class="listViewActionsDiv row-fluid">

							<span class="btn-toolbar span4"> <span class="btn-group">

							</span>
							</span> <span class="btn-toolbar span4"> <span
								class="customFilterMainSpan btn-group"
								style="text-align: center;">
									<div class="pageNumbers alignTop "></div>
							</span>
							</span> <span class="span4 btn-toolbar">
								<div class="listViewActions pull-right">
									<span class="btn-group">
										<button class="btn addButton" ng-click="showUpload($event)">
											<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;<strong>Nuovo</strong>
										</button>
									</span>
								</div>
								<div class="clearfix"></div>
							</span>

						</div>
					</div>
				</div>

				<!-- table -->
				<div class="listViewContentDiv" id="listViewContents">

					<!--  -- >
					 <div class="contents-topscroll noprint">
					 	<div class="topscroll-div" style="width: 95%;">&nbsp;</div>
					 </div>
<!--  -->
					<div class="listViewEntriesDiv">
						<div style="width: 100%; min-height: 400px">

							<table
								class="table table-bordered blockContainer showInlineTable ">
								<thead>
									<tr>
										<th class="blockHeader" colspan="12" ng-hide="ctrl.hideForm">
											Verifica Canale</th>
									</tr>
								</thead>

								<tbody ng-hide="ctrl.hideForm">
									<tr>
										<td class="fieldLabel medium"><label
											class="muted pull-right marginRight10px"> <span
												class="redColor" ng-show="form.process.$invalid">*</span>Tipo
												Emittente
										</label></td>
										<td class="fieldValue medium">
											<div class="row-fluid">
												<span class="span10"> <select name="tipoEmittente"
													id="tipoEmittente" ng-model="ctrl.selectedTipoBroadcaster"
													ng-options="item.nome for item in ctrl.tipiEmittenti"
													ng-change="getEmittenti(ctrl.selectedTipoBroadcaster.nome)">
												</select>
												</span>
											</div>
										</td>


										<td class="fieldLabel medium"><label
											class="muted pull-right "> <span class="redColor"
												ng-show="form.process.$invalid">* </span>Emittente
										</label></td>
										<td class="fieldValue medium">
											<div class="row-fluid">
												<span class="span10"> <select name="emittente"
													id="emittente" ng-change="getCanali(ctrl.emittente)"
													ng-model="ctrl.emittente"
													ng-options="item.nome for item in ctrl.listaEmittenti"
													required>
												</select>
												</span>
											</div>
										</td>

										<td class="fieldLabel medium"><label
											class="muted pull-right "> <span class="redColor"
												ng-show="form.process.$invalid">* </span>Canale
										</label></td>
										<td class="fieldValue medium">
											<div class="row-fluid">
												<span class="span10"> <select name="canale"
													id="canale" ng-model="ctrl.canale"
													ng-options="item.nome for item in ctrl.listaCanali"
													required>
												</select>
												</span>
											</div>
										</td>

									</tr>
									<tr>
										<td class="fieldLabel medium"><label
											class="muted pull-right marginRight10px"> <span
												class="redColor" ng-show="form.process.$invalid">*</span>
												Anno
										</label></td>
										<td class="fieldValue medium">
											<div class="row-fluid">
												<span class="span10"> <select name="utilization"
													id="utilization" ng-model="ctrl.anno"
													ng-options="item for item in ctrl.anni" required>
												</select>
												</span>
											</div>
										</td>

										<td class="fieldLabel medium"><label
											class="muted pull-right marginRight10px"> <span
												class="redColor" ng-show="form.process.$invalid">*</span>
												Mesi
										</label></td>
										<td class="fieldValue medium">
											<div class="input-large">
												<div class="row-fluid">
													<span class="span10">
														<div class="input-large">
															<div ng-dropdown-multiselect="" options="ctrl.mesi"
																selected-model="ctrl.mesiSelected" checkboxes="false"
																extra-settings="multiselectSettings"></div>
														</div>
													</span>
												</div>
											</div>
										</td>
										<td class="fieldLabel medium"><label
											class="muted pull-right marginRight10px"></td>
										<td class="fieldValue medium">
											<div class="input-large">
												<div class="row-fluid">
													<span class="span10"> </span>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<td class="fieldLabel medium" style="text-align: right" nowrap
											colspan="12">
											<button
												ng-hide="
													ctrl.emittente==undefined||ctrl.emittente==''||ctrl.emittente=='*'||
													ctrl.canale==undefined||ctrl.canale==''||ctrl.canale=='*'||
													ctrl.anno==undefined||ctrl.anno==''||ctrl.anno=='*'||
													ctrl.mesiSelected==undefined||!ctrl.mesiSelected.length"
												class="btn addButton"
												ng-click="download(ctrl.emittente,ctrl.canale,ctrl.anno,ctrl.mesiSelected)">
												<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Download
													File Armonizzato</strong>
											</button>
											<button
												ng-hide="
													ctrl.emittente==undefined||ctrl.emittente==''||ctrl.emittente=='*'||
													ctrl.canale==undefined||ctrl.canale==''||ctrl.canale=='*'||
													ctrl.anno==undefined||ctrl.anno==''||ctrl.anno=='*'||
													ctrl.mesiSelected==undefined||!ctrl.mesiSelected.length"
												class="btn addButton"
												ng-click="apply(ctrl.emittente,ctrl.canale,ctrl.anno,ctrl.mesiSelected)">
												<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Visualizza
													Kpi</strong>
											</button>
										</td>
									</tr>
								</tbody>
							</table>
							<!-- table -->
							<div class="listViewContentDiv" id="listViewContents">

								<div class="contents-topscroll noprint">
									<div class="topscroll-div" style="width: 95%">&nbsp;</div>
								</div>
								<div class="contents-topscroll noprint">
									<div class="topscroll-div" style="width: 95%">&nbsp;</div>
								</div>
								<div class="listViewEntriesDiv ">
								
									<div align="center" class="bottomscroll-div"
										style="width: 95%; min-height: 80px">
										<div class="Row" ng-repeat="items in ctrl.split_items">
											<div class="Column" align="center"
												ng-repeat="model in items">
												<div style="width: 400px;">
												<ng-gauge ng-hide="model.valore==undefined||model.valore<0"
													size="200" type="full" thick="15" min="0" max="100"
													value="model.valore" append="%" cap="round"
													foreground-color="#575E77"
													background-color="rgba(155,155,155, 0.4)"> 
												</ng-gauge>
												
												<div class="Row">{{model.kpi}}</div> 
												<div class="Row">{{model.descKpi}}</div>
												</div>
											</div>
										</div>

									</div>
								</div>
							</div>
							<!-- /table -->

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>