<%@ page import="com.google.inject.Injector"%><%@
	page
	import="com.google.inject.Key"%><%@
	page
	import="com.google.inject.name.Names"%><%@
	page language="java"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	final Injector injector = (Injector) pageContext.getServletContext().getAttribute(Injector.class.getName());
	final String userName = (String) session.getAttribute("sso.user.userName");
	final String homeUrl = injector.getInstance(Key.get(String.class, Names.named("home.url")));
%>
<form name="form" novalidate>


	<div id="top_of_dialog">
		<strong>Aggiungi Utente</strong>
	</div>

	<div style="margin-top: 15px">
		<div class="listViewPageDiv">
			<!-- contenitore principale -->
			<div class="listViewActionsDiv row-fluid">
				<span class="btn-toolbar span4" align="center">
					<div class="row-fluid" ngf-drop ng-model="ctrl.file"
						ngf-max-size="20MB" ngf-allow-dir="false">
						<div class="row-fluid" ng-hide="!ctrl.file"
							style="margin-bottom: 5px">
							<strong>{{ctrl.file.name}}</strong> {{ctrl.file.$error}}
							{{ctrl.file.$errorParam}}
						</div>

						<div class="row-fluid"
							style="margin-top: 20px; margin-bottom: 20px">
							Drag &amp; Drop file
							<button class="btn addButton" type="file" ngf-select name="file"
								ng-model="ctrl.file" ngf-max-size="20MB" ngf-allow-dir="false">...o
								Seleziona il file</button>
						</div>

						<div ngf-no-file-drop>File Drag/Drop is not supported for
							this browser</div>

						<img ngf-thumbnail="ctrl.file">

					</div>
			</div>
		</div>

		<div style="margin-top: 15px">
			<table width="100%">
				<tr>
					<td>
						<button type="button" class="btn addButton" ng-click="cancel()">
							<span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp;<strong>Annulla</strong>
						</button>
					</td>
					<td>
						<button ng-hide="ctrl.file==undefined" class="btn addButton" ng-click="uploadImage()">
							<span class="glyphicon glyphicon-plus"></span> &nbsp;&nbsp;<strong>Carica
								l'immagine</strong>
						</button>
					</td>
					<td>
						<button ng-hide="ctrl.file==undefined"  class="btn addButton" ng-click="deleteImage()">
							<span class="glyphicon glyphicon-trash"></span> &nbsp;&nbsp;<strong>Cancella
								l'immagine </strong>
						</button>
					</td>
				</tr>
			</table>
		</div>
</form>

<script type="text/ng-template" id="errorDialogId">

        <div id="target"  >
          <strong>{{ngDialogData}}</strong>
        </div>
    </script>

<script type="text/javascript">
	$('#top_of_dialog')[0].scrollIntoView(true);
</script>
