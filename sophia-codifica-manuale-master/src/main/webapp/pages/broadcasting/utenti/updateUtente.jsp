<%@ page import="com.google.inject.Injector"%><%@
	page
	import="com.google.inject.Key"%><%@
	page
	import="com.google.inject.name.Names"%><%@
	page language="java"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	final Injector injector = (Injector) pageContext.getServletContext().getAttribute(Injector.class.getName());
	final String userName = (String) session.getAttribute("sso.user.userName");
	final String homeUrl = injector.getInstance(Key.get(String.class, Names.named("home.url")));
%>
<form name="form" novalidate>


	<div id="top_of_dialog">
		<strong>Modifica Utente</strong>
	</div>

	<div style="margin-top: 15px">
		<table
			class="table table-bordered blockContainer showInlineTable equalSplit">
			<thead>
				<tr>
					<th class="blockHeader" colspan="2">Utente</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="fieldLabel medium"><label
						class="muted pull-right marginRight10px"> <span
							class="redColor" ng-show="form.process.$invalid">*</span>Username
					</label></td>
					<td class="fieldValue medium">
						<div class="row-fluid">
							<span class="span10"> <input type="text"
								class="form-control span4" id="name"
								placeholder="Inserisci la Username"
								ng-model="ctrl.selectedUsername" required>
							</span>
						</div>
					</td>
				</tr>

				<tr>
					<td class="fieldLabel medium"><label
						class="muted pull-right marginRight10px"> <span
							class="redColor" ng-show="form.process.$invalid">*</span>Password
					</label></td>
					<td class="fieldValue medium">
				<span class="span10"> 
						<button type="button" class="btn addButton" ng-click="regeneratePassword()">
							<span class="glyphicon glyphicon-refresh"></span>&nbsp;&nbsp;<strong>Rigenera
								Password</strong>
						</button>
						</span>
					</td>
				</tr>

				<tr>
					<td class="fieldLabel medium"><label
						class="muted pull-right marginRight10px"> <span
							class="redColor" ng-show="form.process.$invalid">*</span>Email
					</label></td>
					<td class="fieldValue medium">
						<div class="row-fluid">
							<span class="span10"> <input type="text"
								class="form-control span4" id="name"
								placeholder="Inserisci l'Email" ng-model="ctrl.selectedEmail"
								required>
							</span>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div 
		style="margin-top: 15px"
		id="top_of_dialog">
		<strong>Repertori Utente</strong>
	</div>

	<div style="margin-top: 15px">
		<table
			class="table table-bordered listViewEntriesTable">
			<thead>
				<tr class="listViewHeaders">
					<th class="blockHeader">Repertorio</th>
					<th class="blockHeader">Attivo</th>
					<th class="blockHeader">Default</th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="item in ctrl.repertori">
					
					<td class="fieldValue medium"> {{item.nome}} </td>
					
					<td class="fieldValue medium">
						
						<input 
							type="checkbox"
							ng-model="item.active"
							ng-disabled="item.nome===ctrl.selectedObj" />
								
					</td>
					
					<td class="fieldValue medium"> 
						<input 
							ng-disabled="!item.active" 
							type="radio"
							name="pageSet"
							ng-value="item.nome"
							ng-model="ctrl.selectedObj"/>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div style="margin-top: 15px">
		<table width="100%">
			<tr>
				<td>
					<button type="button" class="btn addButton" ng-click="cancel()">
						<span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp;<strong>Annulla</strong>
					</button>
				</td>
				<td align="right">
					<button ng-disabled="form.$invalid || licencesNumber() < 1"
						class="btn addButton" ng-click="save('<%=userName%>')">
						<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;<strong>Modifica</strong>
					</button>
				</td>
			</tr>
		</table>
	</div>

</form>

<script type="text/ng-template" id="errorDialogId">

        <div id="target"  >
          <strong>{{ngDialogData}}</strong>
        </div>
    </script>

<script type="text/javascript">
	$('#top_of_dialog')[0].scrollIntoView(true);
</script>
