<%@ page import="com.google.inject.Injector"%><%@
	page
	import="com.google.inject.Key"%><%@
	page
	import="com.google.inject.name.Names"%><%@
	page language="java"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	final Injector injector = (Injector) pageContext.getServletContext().getAttribute(Injector.class.getName());
	final String userName = (String) session.getAttribute("sso.user.userName");
	final String homeUrl = injector.getInstance(Key.get(String.class, Names.named("home.url")));
%>
<form name="form" novalidate>


	<div id="top_of_dialog">
		<strong>Aggiorna Configurazione</strong>
	</div>

	<div style="margin-top: 15px">
		<table
			class="table table-bordered blockContainer showInlineTable equalSplit">
			<thead>
				<tr>
					<th class="blockHeader" colspan="2">Configurazione</th>
				</tr>
			</thead>
			<tbody>
				<tr ng-show="ctrl.configuration.supportedFormat">
					<td class="fieldLabel medium"><label
						class="muted pull-right marginRight10px"> <span
							class="redColor" ng-show="form.process.$invalid">*</span>Supported Format
					</label></td>
					<td class="fieldValue medium">
						<div class="row-fluid">
							<span class="span10">
								{{ctrl.configuration.supportedFormat}}
							</span>
						</div>
					</td>
				</tr>
				<tr ng-show="ctrl.configuration.supportedFormat">
					<td class="fieldLabel medium"><label
						class="muted pull-right marginRight10px"> <span
							class="redColor" ng-show="form.process.$invalid">*</span>Expected Files
					</label></td>
					<td class="fieldValue medium">
						<div class="row-fluid">
							<span class="span10">
								{{ctrl.configuration.expectedFiles}}
							</span>
						</div>
					</td>
				</tr>
                <tr ng-show="ctrl.configuration.fileValidator">
					<td class="fieldLabel medium"><label
						class="muted pull-right marginRight10px"> <span
							class="redColor" ng-show="form.process.$invalid">*</span>File Validator
					</label></td>
					<td class="fieldValue medium">
						<div class="row-fluid">
							<span class="span10">
								{{ctrl.configuration.fileValidator}}
							</span>
						</div>
					</td>
				</tr>
			
                <tr ng-show="ctrl.configuration.parseConfigPath">
					<td class="fieldLabel medium"><label
						class="muted pull-right marginRight10px"> <span
							class="redColor" ng-show="form.process.$invalid">*</span>Parse Config Path
					</label></td>
					<td class="fieldValue medium">
						<div class="row-fluid">
							<span class="span10">
								{{ctrl.configuration.parseConfigPath}}
							</span>
						</div>
					</td>
				</tr>
                <tr ng-show="ctrl.configuration.parseStreamName">
					<td class="fieldLabel medium"><label
						class="muted pull-right marginRight10px"> <span
							class="redColor" ng-show="form.process.$invalid">*</span>Parse Stream Name
					</label></td>
					<td class="fieldValue medium">
						<div class="row-fluid">
							<span class="span10">
								{{ctrl.configuration.parseStreamName}}
							</span>
						</div>
					</td>
				</tr>
                <tr ng-show="ctrl.configuration.fileParser">
					<td class="fieldLabel medium"><label
						class="muted pull-right marginRight10px"> <span
							class="redColor" ng-show="form.process.$invalid">*</span>File Parser
					</label></td>
					<td class="fieldValue medium">
						<div class="row-fluid">
							<span class="span10">
								{{ctrl.configuration.fileParser}}
							</span>
						</div>
					</td>
				</tr>
				
                <tr ng-show="ctrl.configuration.normalizeRulePath">
					<td class="fieldLabel medium"><label
						class="muted pull-right marginRight10px"> <span
							class="redColor" ng-show="form.process.$invalid">*</span>Normalize Rule Path
					</label></td>
					<td class="fieldValue medium">
						<div class="row-fluid">
							<span class="span10">
								{{ctrl.configuration.normalizeRulePath}}
							</span>
						</div>
					</td>
				</tr>
                <tr ng-show="ctrl.configuration.validateRulePath">
					<td class="fieldLabel medium"><label
						class="muted pull-right marginRight10px"> <span
							class="redColor" ng-show="form.process.$invalid">*</span>Validate Rule Path
					</label></td>
					<td class="fieldValue medium">
						<div class="row-fluid">
							<span class="span10">
								{{ctrl.configuration.validateRulePath}}
							</span>
						</div>
					</td>
				</tr>
                <tr ng-show="ctrl.configuration.writeConfigPath">
					<td class="fieldLabel medium"><label
						class="muted pull-right marginRight10px"> <span
							class="redColor" ng-show="form.process.$invalid">*</span>Write Config Path
					</label></td>
					<td class="fieldValue medium">
						<div class="row-fluid">
							<span class="span10">
								{{ctrl.configuration.writeConfigPath}}
							</span>
						</div>
					</td>
				</tr>
                <tr ng-show="ctrl.configuration.writeConfigStream">
					<td class="fieldLabel medium"><label
						class="muted pull-right marginRight10px"> <span
							class="redColor" ng-show="form.process.$invalid">*</span>Write Config Stream
					</label></td>
					<td class="fieldValue medium">
						<div class="row-fluid">
							<span class="span10">
								{{ctrl.configuration.writeConfigStream}}
							</span>
						</div>
					</td>
				</tr>
                <tr ng-show="ctrl.configuration.writeTemplateBucket">
					<td class="fieldLabel medium"><label
						class="muted pull-right marginRight10px"> <span
							class="redColor" ng-show="form.process.$invalid">*</span>Write Template Bucket
					</label></td>
					<td class="fieldValue medium">
						<div class="row-fluid">
							<span class="span10">
								{{ctrl.configuration.writeTemplateBucket}}
							</span>
						</div>
					</td>
				</tr>
                <tr ng-show="ctrl.configuration.writeUploadBucket">
					<td class="fieldLabel medium"><label
						class="muted pull-right marginRight10px"> <span
							class="redColor" ng-show="form.process.$invalid">*</span>Write Upload Bucket
					</label></td>
					<td class="fieldValue medium">
						<div class="row-fluid">
							<span class="span10">
								{{ctrl.configuration.writeUploadBucket}}
							</span>
						</div>
					</td>
				</tr>
				
                <tr ng-show="ctrl.configuration.writeFileExtension">
					<td class="fieldLabel medium"><label
						class="muted pull-right marginRight10px"> <span
							class="redColor" ng-show="form.process.$invalid">*</span>Write File Extension
					</label></td>
					<td class="fieldValue medium">
						<div class="row-fluid">
							<span class="span10">
								{{ctrl.configuration.writeFileExtension}}
							</span>
						</div>
					</td>
				</tr>
				
			</tbody>
		</table>
	</div>

	<div style="margin-top: 15px">
		<table width="100%">
			<tr>
				<td>
					<button type="button" class="btn addButton" ng-click="cancel()">
						<span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp;<strong>Chiudi</strong>
					</button>
				</td>
			</tr>
		</table>
	</div>

</form>

<script type="text/ng-template" id="errorDialogId">

        <div id="target"  >
          <strong>{{ngDialogData}}</strong>
        </div>
    </script>

<script type="text/javascript">
	$('#top_of_dialog')[0].scrollIntoView(true);
</script>
