<%@ page import="com.google.inject.Injector"%><%@
	page
	import="com.google.inject.Key"%><%@
	page
	import="com.google.inject.name.Names"%><%@
	page language="java"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	final Injector injector = (Injector) pageContext.getServletContext().getAttribute(Injector.class.getName());
	final String userName = (String) session.getAttribute("sso.user.userName");
	final String homeUrl = injector.getInstance(Key.get(String.class, Names.named("home.url")));
%>
<form name="form" novalidate>


	<div id="top_of_dialog">
		<strong>Storico modifiche canale</strong>
	</div>

	<div style="margin-top: 15px">
		<table class="table table-bordered listViewEntriesTable">
			<thead>
				<tr class="listViewHeaders">
					<th><a href="" class="listViewHeaderValues">Nome Canale </a></th>
					<th><a href="" class="listViewHeaderValues">Generalista </a></th>
					<th><a ng-show="checkRadio()" href="" class="listViewHeaderValues">Regola speciale
					</a></th>
					<th><a href="" class="listViewHeaderValues">Data Inizio
							Validit&agrave</a></th>
					<th><a href="" class="listViewHeaderValues">Data Fine
							Validit&agrave</a></th>
					<th><a href="" class="listViewHeaderValues">Data Ultima
							Modifica</a></th>
					<th><a href="" class="listViewHeaderValues">Utente Ultima
							Modifica</a></th>

				</tr>
			</thead>
			<tbody>

				<tr ng-hide="checkSize()" class="listViewEntries"
					ng-repeat="item in ctrl.listaCanali">
					<td class="listViewEntryValue ">{{item.nome}}</td>
					<td class="listViewEntryValue"><span
						ng-hide="item.generalista==1" class="glyphicon glyphicon-remove"></span>
						<span ng-hide="item.generalista==0" class="glyphicon glyphicon-ok"></span></td>
					<td class="listViewEntryValue">
						<div ng-show="checkRadio()" >
							<span
							ng-hide="item.specialRadio==1" class="glyphicon glyphicon-remove"></span>
							<span ng-hide="item.specialRadio==0" class="glyphicon glyphicon-ok"></span>
						</div>	
					</td>
					<td class="listViewEntryValue ">{{formatDate(item.dataInizioValid)}}</td>
					<td class="listViewEntryValue ">{{formatDate(item.dataFineValid)}}</td>
					<td class="listViewEntryValue ">{{formatDate(item.dataModifica)}}</td>
					<td class="listViewEntryValue ">{{item.utenteModifica}}</td>
				</tr>

				<tr ng-hide="!checkSize()">
					<td colspan="7">Non sono state effettuate modifiche al canale
					</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div style="margin-top: 15px">
		<table width="100%">
			<tr>
				<td>
					<button type="button" class="btn addButton" ng-click="cancel()">
						<span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp;<strong>Annulla</strong>
					</button>
				</td>
			</tr>
		</table>
	</div>

</form>

<script type="text/ng-template" id="errorDialogId">

        <div id="target"  >
          <strong>{{ngDialogData}}</strong>
        </div>
    </script>

<script type="text/javascript">
	$('#top_of_dialog')[0].scrollIntoView(true);
</script>
