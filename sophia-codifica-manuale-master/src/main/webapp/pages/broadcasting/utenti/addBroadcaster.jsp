<%@ page import="com.google.inject.Injector"%><%@
	page
	import="com.google.inject.Key"%><%@
	page
	import="com.google.inject.name.Names"%><%@
	page language="java"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	final Injector injector = (Injector) pageContext.getServletContext().getAttribute(Injector.class.getName());
	final String userName = (String) session.getAttribute("sso.user.userName");
	final String homeUrl = injector.getInstance(Key.get(String.class, Names.named("home.url")));
%>
<form name="form" novalidate>


	<div id="top_of_dialog">
		<strong>Aggiungi Emittente</strong>
	</div>

	<div style="margin-top: 15px">
		<table
			class="table table-bordered blockContainer showInlineTable equalSplit">
			<thead>
				<tr>
					<th class="blockHeader" colspan="2">Emittente</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="fieldLabel medium"><label
						class="muted pull-right marginRight10px"> <span
							class="redColor" ng-show="form.process.$invalid">*</span>Nome
							Emittente
					</label></td>
					<td class="fieldValue medium">
						<div class="row-fluid">
							<span class="span10"> <input type="text"
								class="form-control span4" id="name"
								placeholder="Inserisci il nome dell'emittente"
								ng-model="ctrl.selectedName" required>
							</span>
						</div>
					</td>
				</tr>
				<tr>
					<td class="fieldLabel medium"><label
						class="muted pull-right marginRight10px"> <span
							class="redColor" ng-show="form.process.$invalid">*</span>Tipo
							Emittente
					</label></td>
					<td class="fieldValue medium">
						<div class="row-fluid">
							<span class="span10"> <select name="tipoEmittente"
								id="tipoEmittente" ng-model="ctrl.selectedType"
								ng-change="changeType()"
								ng-options="item.nome for item in ctrl.tipiEmittenti" required>
							</select>
							</span>
						</div>
					</td>
				</tr>
				<tr>
					<td class="fieldLabel medium"><label
						class="muted pull-right marginRight10px"> <span
							class="redColor" ng-show="form.process.$invalid">*</span>Configurazione Default
					</label></td>
					<td class="fieldValue medium">
						<div class="row-fluid">
							<span class="span10"> <select name="tipoConfigurazione"
								id="tipoEmittente" ng-model="ctrl.selectedConf"
								ng-options="item.nomeTemplate for item in ctrl.tipiConf"
								required>
							</select>
							</span>
						</div>
					</td>
				</tr>
				<tr>
					<td class="fieldLabel medium"><label
						class="muted pull-right marginRight10px"> <span
							class="redColor" ng-show="form.process.$invalid">*</span>Data
							Inizio Validità Configurazione
					</label></td>
					<td>
						<div class="controls">
							<div class="input-append date" id="datepickerConfigurazioneFrom"
								data-date-format="mm-yyyy">

								<input placeholder="Oggi" type="text" name="date"
									ng-model="ctrl.selectedDateConfigurazioneFrom"
									style="width: 173px; height: 25px"> <span
									class="add-on" style="height: 25px"><i class="icon-th"></i></span>

								<span class="glyphicon glyphicon-erase"
									style="margin-top: 6px; margin-left: 6px;"
									ng-click="ctrl.selectedDateConfigurazioneFrom=undefined;"></span>
							</div>
							<script type="text/javascript">
								$("#datepickerConfigurazioneFrom").datepicker({
							  	  	language: 'it',
									format : 'dd-mm-yyyy',
									viewMode : "days",
									minViewMode : "days",
									orientation : "bottom auto",
									autoclose : "true"
								});
							</script>
						</div>
					</td>
				</tr>
				<tr>
					<td class="fieldLabel medium"><label
						class="muted pull-right marginRight10px"> <span
							class="redColor" ng-show="form.process.$invalid">*</span>Data
							Fine Validità Configurazione
					</label></td>
					<td>
						<div class="controls">
							<div class="input-append date" id="datepickerConfigurazioneTo"
								data-date-format="mm-yyyy">

								<input placeholder="Nessuna" type="text" name="date"
									ng-model="ctrl.selectedDateConfigurazioneTo"
									style="width: 173px; height: 25px"> <span
									class="add-on" style="height: 25px"><i class="icon-th"></i></span>

								<span class="glyphicon glyphicon-erase"
									style="margin-top: 6px; margin-left: 6px;"
									ng-click="ctrl.selectedDateConfigurazioneTo=undefined;"></span>
							</div>
							<script type="text/javascript">
								$("#datepickerConfigurazioneTo").datepicker({
							  	  	language: 'it',
									format : 'dd-mm-yyyy',
									viewMode : "days",
									minViewMode : "days",
									orientation : "bottom auto",
									autoclose : "true"
								});
							</script>
						</div>
					</td>
				</tr>
				<tr>
                    <td class="fieldLabel medium"><label class="muted pull-right marginRight10px"> <span class="redColor ng-hide" ng-show="form.process.$invalid" aria-hidden="true" style="">*</span>Nuovo Tracciato Rai</label></td>
                    <td><input type="checkbox" ng-model="ctrl.nuovoTracciatoRai" name="newtracciato">&nbsp;</td>
                </tr>
			</tbody>
		</table>
	</div>

	<div style="margin-top: 15px">
		<table width="100%">
			<tr>
				<td>
					<button type="button" class="btn addButton" ng-click="cancel()">
						<span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp;<strong>Annulla</strong>
					</button>
				</td>
				<td align="right">
					<button ng-disabled="form.$invalid || licencesNumber() < 1"
						class="btn addButton" ng-click="save('<%=userName%>')">
						<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;<strong>Aggiungi</strong>
					</button>
				</td>
			</tr>
		</table>
	</div>

</form>

<script type="text/ng-template" id="errorDialogId">

        <div id="target"  >
          <strong>{{ngDialogData}}</strong>
        </div>
    </script>

<script type="text/javascript">
	$('#top_of_dialog')[0].scrollIntoView(true);
</script>
