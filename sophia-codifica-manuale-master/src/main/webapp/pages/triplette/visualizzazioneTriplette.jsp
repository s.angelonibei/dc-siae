<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../navbar.jsp"%>

<div class="bodyContents" ng-init="init()">

	<div class="mainContainer row-fluid">

		<div class="contents-topscroll noprint">
			<div class="topscroll-div" style="width: 95%;margin-bottom: 20px">&nbsp;</div>
		</div>

		<div class="sticky">
			<div id="companyLogo" class="navbar commonActionsContainer noprint">
				<div class="actionsContainer row-fluid">
					<div class="span2">
						<span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
					</div>
					<span class="btn-toolbar span5" style="margin-left: 0px">
						<div class="pageNumbers alignTop pull-right" style="text-align: center;">
							<span id="up" class="pageNumbersText"><strong>Gestione Anagrafica Triplette</strong></span>
						</div>
					</span>
				</div>
			</div>
		</div>

		<div class="listViewEntriesDiv">
			<div style="width: 100%; min-height: 400px">

				<div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 180px;">
					<div>
						<div class="listViewPageDiv">

							<!-- start Toolbar -->
						<div class="listViewTopMenuDiv noprint">
								<div class="listViewActionsDiv row-fluid">
								<span class="span12 btn-toolbar">
									<div class="listViewActions pull-right">
										<button title="NuovaTripletta" class="btn" id="nuovaTriplettaButton" type="button" ng-click="nuovaTripletta()">
											<span class="glyphicon glyphicon-open-file"></span>&nbsp;&nbsp;<strong>Nuova Tripletta</strong>
										</button>
									</div>
								</span>
								</div>
							</div>
							<!-- end Toolbar -->
							
							<div class="listViewEntriesDiv">
								<div style="width: 100%; min-height: 400px">

									<table class="table table-bordered blockContainer showInlineTable ">
										<thead>
											<tr>
													<th class="blockHeader" colspan="6" ng-show="ctrl.hideForm"><span ng-click="ctrl.hideForm=false" class="glyphicon glyphicon-expand"></span>&nbsp;Ricerca Triplette</th>
													<th class="blockHeader" colspan="6" ng-hide="ctrl.hideForm"><span ng-click="ctrl.hideForm=true" class="glyphicon glyphicon-collapse-down"></span>&nbsp;Ricerca Triplette</th>
											</tr>
										</thead>

										<tbody ng-hide="ctrl.hideForm">

											<tr>
												<td class="fieldLabel medium"><label class="muted pull-right marginRight10px">
													Tipo Emittente
													</label></td>
												<td class="fieldValue medium">
														<div class="row-fluid">
															<span class="span10"> <select name="tipoEmittente" id="tipoEmittente" ng-model="ctrl.selectedTipoBroadcaster"
																	ng-options="item as item.nome for item in ctrl.tipiEmittenti track by item.nome">
																</select>
															</span>
														</div>
												</td>

												<td class="fieldLabel medium"><label class="muted pull-right ">Emittente
													</label></td>
												<td class="fieldValue medium">
													<div class="row-fluid">
														<span class="span10"><select name="emittente" id="emittente" ng-change="getCanali(ctrl.emittente)"
															 ng-model="ctrl.emittente" ng-options="item as item.nome for item in ctrl.emittenti | orderBy : 'nome'| filter: (!['TUTTI',''].includes(ctrl.selectedTipoBroadcaster.nome) || '') && {tipoBroadcaster: ctrl.selectedTipoBroadcaster.nome} track by item.id">
															 <option selected="ctrl.emittente==null"></option>
															</select>
														</span>
													</div>
												</td>												

												<td class="fieldLabel medium">
													<label class="muted pull-right ">Canale</label>
												</td>
												<td class="fieldValue medium">
													<div class="row-fluid">
														<span class="span10"> <select name="canale" id="canale" ng-model="ctrl.canaleSelezionato" ng-options="item as item.nome for item in ctrl.listaCanali | orderBy : 'nome' track by item.id">
																<option selected="ctrl.canaleSelezionato=null"></option>
															</select>
														</span>
													</div>
												</td>

											</tr>

											<tr>

												<td class="fieldLabel medium"><label class="muted pull-right ">Tipo di diritto
													</label></td>
												<td class="fieldValue medium">
													<div class="row-fluid">
														<span class="span10"> <select name="tipoDirittoSelect" ng-model="ctrl.tipoDiritto" ng-options="item for item in ctrl.tipiDiritto">
																<option></option>
															</select>
														</span>
													</div>
												</td>

												<td class="fieldLabel medium"><label class="muted pull-right "> <span class="redColor" ng-show="form.process.$invalid">*
														</span>Stato
													</label></td>

												<td class="fieldValue medium">
													<div class="row-fluid">
														<span class="span10"> <select name="statoTripletta" id="statoTripletta" ng-model="ctrl.statoTripletta" ng-options="item for item in ctrl.statiTripletta">
															<option></option>
															</select>
														</span>
													</div>
												</td>

												<td class="fieldLabel medium"><label class="muted pull-right ">
														Tripletta:
													</label>
												</td>

												<td class="fieldValue medium">
													<div class="row-fluid">
														<span class="span10">
															<input style="padding-top: 3px; padding-bottom: 6px;" ng-model="ctrl.filterTripletta" />
														</span>
													</div>
												</td>

											</tr>

											<tr>
												<td class="fieldLabel medium" style="text-align: right" nowrap colspan="12">
													<button ng-hide="" class="btn addButton"
													 ng-click="filterApply()">
														<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Applica</strong>
													</button>

												</td>
											</tr>
										</tbody>
									</table>
									<!-- start toolbar tabella | paginazione -->
									<div class="listViewTopMenuDiv noprint">
										<div class="listViewActionsDiv row-fluid">
											<span class="span12 btn-toolbar">
												<div class="listViewActions pull-right">
													<div class="pageNumbers alignTop ">
														<span ng-show="ctrl.totRecords > 50">
															<span class="pageNumbersText" style="padding-right: 5px">
																Record da <strong>{{ctrl.page*50+1}}</strong> a <strong>{{(ctrl.page*50) + ctrl.sampling.length}}</strong>
																su
																<strong>{{totRecords}}</strong>
															</span>
															<button title="Precedente" class="btn" id="listViewPreviousPageButton" type="button" ng-click="navigateToPreviousPage()"
															ng-show="ctrl.page > 0">
																<span class="glyphicon glyphicon-chevron-left"></span>
															</button>
															<button title="Successiva" class="btn" id="listViewNextPageButton" type="button" ng-click="navigateToNextPage()"
															ng-show="ctrl.page < ctrl.pageTot">
																<span class="glyphicon glyphicon-chevron-right"></span>
															</button>
															<button title="Fine" class="btn" id="listViewNextPageButton" type="button" ng-click="navigateToEndPage()"
															ng-show="ctrl.page < ctrl.pageTot">
																<span class="glyphicon glyphicon-step-forward"></span>
															</button>
														</span>
													</div>
												</div>
											</span>
										</div>
									</div>
									<!-- end toolbar tabella | paginazione -->
									<!-- table -->
									<div class="listViewEntriesDiv contents-bottomscroll">
										<div class="bottomscroll-div" style="width: 95%; min-height: 80px">

											<table class="table table-bordered listViewEntriesTable ">
												<thead>

													<tr class="listViewHeaders">
														<th style="width: 15%" ng-click="orderImage('spanTripletta','tripletta')" style="cursor: pointer">Tripletta&nbsp;<span
																id="spanTripletta" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
														<th style="width: 15%" ng-click="orderImage('spanNomeEmittente','nomeEmittente')" style="cursor: pointer">Nome Emittente&nbsp;<span id="spanNomeEmittente"
																style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
														<th style="width: 10%" ng-click="orderImage('spanTipoEmittente','tipoEmittente')" style="cursor: pointer">Tipo Emittente&nbsp;<span id="spanTipoEmittente" style="font-size:80%"
																class="glyphicon glyphicon-sort"></span></th>
														<th>Canali&nbsp;</th>
														<th style="width: 6%" ng-click="orderImage('spanTipoDiritto','tipoDiritto')" style="cursor: pointer">Tipo Diritto&nbsp;<span
																id="spanTipoDiritto" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
														<th style="width: 5%" ng-click="orderImage('spanStato','stato')" style="cursor: pointer">Stato&nbsp;<span
															id="spanStato" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
														<th style="width: 5%">Azioni&nbsp;<span style="font-size:80%"></span></th>
													</tr>
												</thead>
												<tbody>
													<tr class="listViewEntries " ng-repeat="item in ctrl.sampling">
														<td class="listViewEntryValue">{{item.tripletta}}</td>
														<td class="listViewEntryValue">{{item.nomeEmittente}}</td>
														<td class="listViewEntryValue">{{item.tipoEmittente}}</td>
														<td class="listViewEntryValue">
															<span ng-show="item.longChannelList && item.canaliKey == 'canaliShorted'">{{item.canaliShorted}}...<span class="glyphicon glyphicon-collapse-down" ng-click="item.canaliKey='nomiCanali'"></span>
															</span>
															<span ng-show="item.longChannelList && item.canaliKey == 'nomiCanali'">{{item.nomiCanali}}&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-collapse-up" ng-click="item.canaliKey='canaliShorted'"></span>
															</span>
															<span ng-show="!item.longChannelList">{{item.nomiCanali}}</span>
															</span>
														</td>
														<td class="listViewEntryValue">{{item.tipoDiritto}}</td>
														<td class="listViewEntryValue">{{item.stato}}</td>
														<td class="listViewEntryValue"><span ng-click="dettaglioTripletta(item)" class="glyphicon glyphicon-list-alt"></span></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
									<!-- table -->									


								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
<!--<pre>{{ctrl.emittente | json}}</pre>-->