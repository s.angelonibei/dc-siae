<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../navbar.jsp"%>

<div class="bodyContents" ng-init="init()">

	<div class="mainContainer row-fluid">

		<div id="companyLogo" class="navbar commonActionsContainer noprint">
			<div class="actionsContainer row-fluid">
				<div class="span2">
					<span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
				</div>
				<span class="btn-toolbar span5" style="margin-left: 0px">
					<div class="pageNumbers alignTop pull-right" style="text-align: center;">
						<span id="up" class="pageNumbersText"><strong>Anagrafica Tripletta</strong></span>
					</div>
				</span>
			</div>
		</div>

		<div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 180px;">
			<div class="listViewPageDiv">
				<div class="listViewTopMenuDiv noprint">
					<div class="listViewTopMenuDiv noprint">
						<div class="listViewActionsDiv row-fluid">
							<span class="span12 btn-toolbar">
								<div class="listViewActions pull-right">
									<button title="Indietro" class="btn" id="goBackButton" type="button" ng-click="goBack()">
										<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Indietro</strong>
									</button>
								</div>
								<div class="clearfix"></div>
							</span>
						</div>
					</div>
				</div>
				<form id="triplettaForm" autocomplete="off">
				<div class="listViewContentDiv" id="listViewContents">
					<div class="listViewEntriesDiv">
						<div style="width: 100%; min-height: 400px">
							<table class="table table-bordered table-responsive blockContainer showInlineTable ">					
								<thead>
									<tr>
										<th class="blockHeader" colspan="12">{{ctrl.configs[ctrl.mode].title}}</th>
									</tr>
								</thead>
								<tbody>

									<tr>
										<td class="fieldLabel medium"><label class="muted pull-right marginRight10px">Descrizione SAP:
											</label></td>
										<td class="fieldLabel medium" colspan="5" ng-show="!editable('descrizioneSap')"><label class="muted pull-left">{{ctrl.tripletta.descrizioneSap}}
											</label></td>
										<td class="fieldValue medium" colspan="5" ng-show="editable('descrizioneSap')">
											<div class="row-fluid">
												<span class="span12">
													<div class="">
														<input name="descrizioneTripletta" style="width: 99%"
															class="ng-pristine ng-valid ng-empty ng-touched"
															type="text" maxlength="256" ng-model="ctrl.tripletta.descrizioneSap" id="descrizioneTripletta">
													</div>
												</span>
											</div>
										</td>
									</tr>

									<tr>
										<td class="fieldLabel medium span2"><label class="muted pull-right marginRight10px">Chiave di Rif.3:
											</label></td>
										<td class="fieldLabel medium span2" ng-show="!editable('chiaveRif3')"><label class="muted">{{ctrl.tripletta.chiaveRif3}}
											</label></td>
										<td class="fieldValue medium" ng-show="editable('chiaveRif3')">
											<div class="row-fluid">
												<span class="span12">
													<div class="">
														<input name="chiaveRif3" style="width: 94%"
															class="ng-pristine ng-valid ng-empty ng-touched"
															type="text" maxlength="16" 
															pattern="^[A-Za-z0-9]{5}$"
															title="Codice alfanumerico di 5 caratteri"
															ng-model="ctrl.tripletta.chiaveRif3" id="chiaveRif3" required>
													</div>
												</span>
											</div>
										</td>

										<td class="fieldLabel medium span2"><label class="muted pull-right marginRight10px">Conto:
											</label></td>
										<td class="fieldLabel medium span2" ng-show="!editable('conto')"><label class="muted">{{ctrl.tripletta.conto}}
											</label></td>
										<td class="fieldValue medium" ng-show="editable('conto')">
											<div class="row-fluid">
												<span class="span12">
													<div class="">
														<input name="conto" style="width: 94%"
															class="ng-pristine ng-valid ng-empty ng-touched"
															type="text" maxlength="16" 
															pattern="^[0-9]{8}$"
															title="numero (8 caratteri)"
															ng-model="ctrl.tripletta.conto" id="conto" required>
													</div>
												</span>
											</div>
										</td>

										<td class="fieldLabel medium span2"><label class="muted pull-right marginRight10px">Attribuzione:
											</label></td>
										<td class="fieldLabel medium span2" ng-show="!editable('attribuzione')"><label style="min-width: 218px;" class="muted">{{ctrl.tripletta.attribuzione}}
											</label></td>
										<td class="fieldValue medium" ng-show="editable('attribuzione')">
												<div class="row-fluid">
													<span class="span12">
														<div class="">
															<input name="attribuzione" style="width:95%"
																class="ng-pristine ng-valid ng-empty ng-touched"
																type="text" maxlength="16"
																pattern="^[0-9]{2,10}$"
																title="Codice Cliente SAP (numero da 2 a 10 caratteri)"																
																ng-model="ctrl.tripletta.attribuzione" id="attribuzione" required>
														</div>
													</span>
												</div>
											</td>

									</tr>



									<tr>

										<td class="fieldLabel medium"><label class="muted pull-right ">Tipo di diritto
											</label></td>
										<td class="fieldValue medium" ng-show="editable('tipoDiritto')">
											<div class="row-fluid">
												<span class="span10"> <select name="tipoDirittoSelect" ng-model="ctrl.tripletta.tipoDiritto" ng-options="item for item in ctrl.tipiDiritto" required>
														<option></option>
													</select>
												</span>
											</div>
										</td>
										<td class="fieldLabel medium" ng-show="!editable('tipoDiritto')">
											<div class="row-fluid">
													<label class="muted">{{ctrl.tripletta.tipoDiritto}}</label>
											</div>
										</td>

										<td class="fieldLabel medium"><label class="muted pull-right ">
												</span>Periodo di riferimento
											</label></td>
										<td class="fieldValue medium" ng-show="editable('rifTemporaleIncassi')">
											<div class="row-fluid">
												<span class="span10"> <select name="periodoDiRiferimentoSelect" ng-change="selectPeriodoRiferimento()" ng-model="ctrl.tripletta.rifTemporaleIncassi"
													 ng-options="item for item in ctrl.tipiRifTemporale" required>
													 <option></option>
													</select>
												</span>
											</div>
										</td>
										<td class="fieldLabel medium" ng-show="!editable('rifTemporaleIncassi')">
											<div class="row-fluid">
												<label class="muted">{{ctrl.tripletta.rifTemporaleIncassi}}</label>
											</div>
										</td>

										<td class="fieldLabel medium" nowrap>

											
											<div class="input-append muted" style="height: 23px">
												<span style="float:left">Da&nbsp;&nbsp;</span>
												<div ng-show="editable('giornoMeseDa')">
												<div class="date" id="periodoDiRiferimentoDaDP" data-date-format="dd-mm">
													<input id="periodoDiRiferimentoDa" placeholder="gg-mm" type="text" ng-model="ctrl.tripletta.giornoMeseDa" style="width:50%" required></input>
													<span class="add-on" ng-show="ctrl.tripletta.rifTemporaleIncassi!='Annuale - Solare'"><i class="icon-th"></i></span>
												</div>
												<span class="add-on" style="color: #000;" ng-click="ctrl.tripletta.giornoMeseDa=undefined;" ng-show="ctrl.tripletta.rifTemporaleIncassi!='Annuale - Solare'"><i
													class="glyphicon glyphicon-erase"></i></span>
											</div>
												<div class="row-fluid" ng-show="!editable('giornoMeseDa')">
													<label class="muted">{{ctrl.tripletta.giornoMeseDa}}</label>
												</div>
											</div>
										

										</td>

										<td class="fieldLabel medium" nowrap>
											<div class="input-append muted" style="height: 23px">
													<span style="float:left">A&nbsp;&nbsp;</span>
												<div ng-show="editable('giornoMeseA')">
													<div class="date" id="periodoDiRiferimentoADP" data-date-format="dd-mm">
														<input id="periodoDiRiferimentoA" placeholder="gg-mm" type="text" ng-model="ctrl.tripletta.giornoMeseA" style="width:50%" required></input>
														<span class="add-on" ng-show="ctrl.tripletta.rifTemporaleIncassi!='Annuale - Solare'"><i class="icon-th"></i></span>
													</div>
													<span class="add-on" style="color: #000;" ng-click="ctrl.tripletta.giornoMeseA=undefined;" ng-show="ctrl.tripletta.rifTemporaleIncassi!='Annuale - Solare'"><i
														class="glyphicon glyphicon-erase"></i></span>
											</div>
												<div class="row-fluid" ng-show="!editable('giornoMeseA')">
													<label class="muted">{{ctrl.tripletta.giornoMeseA}}</label>
												</div>													
											</div>

										</td>

									</tr>

									<tr>
										<td class="fieldLabel medium"><label class="muted pull-right marginRight10px"> <span class="redColor" ng-show="form.process.$invalid">*</span>Tipo
												Emittente
											</label></td>
										<td class="fieldValue medium" ng-show="editable('tipoEmittente')">
											<div class="row-fluid">
												<span class="span10"> <select name="tipoEmittente" id="tipoEmittente" ng-model="ctrl.selectedTipoBroadcaster"
													 ng-options="item as item.nome for item in ctrl.tipiEmittenti track by item.nome">
													</select>
												</span>
											</div>
										</td>
										<td class="fieldLabel medium" ng-show="!editable('tipoEmittente')">
											<div class="row-fluid">
													<label class="muted">{{ctrl.tripletta.tipoEmittente}}</label>
											</div>
										</td>										

										<td class="fieldLabel medium"><label class="muted pull-right ">Emittente
											</label></td>
										<td class="fieldValue medium" ng-show="editable('nomeEmittente')">
											<div class="row-fluid">
												<span class="span10"> <select name="emittente" id="emittente" ng-change="getCanali(ctrl.emittente)"
													 ng-model="ctrl.emittente" ng-options="item as item.nome for item in ctrl.emittenti| orderBy : 'nome' | filter: (!['TUTTI',''].includes(ctrl.selectedTipoBroadcaster.nome) || '') && {tipoBroadcaster: ctrl.selectedTipoBroadcaster.nome} track by item.id">
													</select>
												</span>
											</div>
										</td>
										<td class="fieldLabel medium" ng-show="!editable('nomeEmittente')">
											<div class="row-fluid">
													<label class="muted">{{ctrl.tripletta.nomeEmittente}}</label>
											</div>
										</td>										

										<td class="fieldLabel medium" ng-show="editable('canale')">
											<label class="muted pull-right ">Canale</label>
										</td>
										<td class="fieldValue medium" ng-show="editable('canale')">
											<div class="row-fluid">
												<span class="span10"> <select name="canale" id="canale" ng-model="ctrl.canaleSelezionato" ng-options="item.nome for item in ctrl.listaCanali | orderBy : 'nome' track by item.id">
													</select>
												</span>
											</div>
										</td>
										<td class="fieldLabel medium" ng-show="!editable('canale')" colspan="2"></td>
									</tr>

									<tr ng-show="ctrl.mode == 'detail'">

										<td class="fieldLabel medium">
											<label class="muted pull-right ">Data Creazione/Modifica</label>
										</td>
										<td class="fieldLabel medium">
											<div class="row-fluid">
												<span class="span10">
														<label class="muted pull-left">{{ctrl.tripletta.dataInserimento}}</label>
												</span>
											</div>
										</td>
										<td class="fieldLabel medium">
												<label class="muted pull-right ">
														Utente Ultima Modifica
												</label>
											</td>
											<td class="fieldLabel medium">
												<div class="row-fluid">
													<span class="span10">
															<label class="muted pull-left">{{ctrl.tripletta.utente || ''}}</label>
													</span>
												</div>
											</td>
										<td class="fieldLabel medium" colspan="2"></td>

									</tr>
									<tr>
										<td class="fieldLabel medium" style="text-align: right" nowrap colspan="12">
											<button class="btn addButton" ng-click="aggiungiCanale()" type="button" ng-show="editable('canale')">
												<span class="glyphicon glyphicon-plus-sign"></span>&nbsp;&nbsp;<strong>Aggiungi</strong>
											</button>
										</td>
									</tr>
								</tbody>
							</table>
							<br>
							<!-- results table start -->
							<div class="listViewEntriesDiv contents-bottomscroll">
								<div class="bottomscroll-div" style="width: 95%; min-height: 80px">

									<table class="table table-bordered listViewEntriesTable ">
										<thead>
											<tr class="listViewHeaders">
												<th><a href="" class="listViewHeaderValues">Canale</a></th>
												<!-- <th><a href="" class="listViewHeaderValues">% Preallocazione</a></th> -->
												<th><a href="" class="listViewHeaderValues">Valido Da</a></th>
												<th><a href="" class="listViewHeaderValues">Valido A</a></th>
												<th></th>
											</tr>
										</thead>
										<tbody>

											<tr class="listViewEntries " ng-repeat="item in ctrl.tripletta.canali track by $index">

												<td class="listViewEntryValue ">{{item.nomeCanale}}</td>
												<!--
												<td class="listViewEntryValue" ng-show="editable('canale')">
													<input type="text" pattern="^(100(,00)?$|^[0-9]{1,2}$|^[0-9]{1,2},[0-9]{1,2})%?$" placeholder="00,00%" id="percentage{{$index}}"
													title="Inserire una percentuale valida" ng-model="item.preallocazionePercentuale" />
												</td> -->
												<!--
												<td class="listViewEntryValue" ng-show="!editable('canale')">
													<div class="row-fluid">
															<label class="muted">{{item.preallocazionePercentuale | currency : "%" : 2}}</label>
													</div>
												</td>
												-->

												<td class="listViewEntryValue" ng-show="editable('canale')">
													<input type="text" class="span9 input-small dataCanale" style="width: 180px" ng-model="item.attivoDal">
													<span class="glyphicon glyphicon-erase" style="margin-top: 6px;" ng-click="item.attivoDal=undefined;"></span>
												</td>
												<td class="listViewEntryValue" ng-show="!editable('canale')">
													<div class="row-fluid">
															<label class="muted">{{item.attivoDal}}</label>
													</div>
												</td>

												<td class="listViewEntryValue" ng-show="editable('canale')">
													<input type="text" class="span9 input-small dataCanale" style="width: 180px" ng-model="item.attivoAl">
													<span class="glyphicon glyphicon-erase" style="margin-top: 6px;" ng-click="item.attivoAl=undefined;"></span>
												</td>
												<td class="listViewEntryValue" ng-show="!editable('canale')">
													<div class="row-fluid">
															<label class="muted">{{item.attivoAl}}</label>
													</div>
												</td>												

												<td class="listViewEntryValue" ng-show="editable('canale')"><span ng-click="eliminaCanale(item)" class="glyphicon glyphicon-trash"></span></td>
												<td class="listViewEntryValue" ng-show="!editable('canale')"></td>

												<script type="text/javascript">
													$(".dataCanale")
														.datepicker({
															format: 'dd-mm-yyyy',
															language: 'it',
															viewMode: "days",
															minViewMode: "days",
															autoclose: true,
															todayHighlight: true
														});
												</script>
											</tr>

											<tr>
												<td class="fieldLabel medium" style="text-align: right" nowrap colspan="12">
													<button class="btn addButton" ng-click="salvaTripletta('<%=userName%>')" ng-show="showSaveButton()">
														<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Salva</strong>
													</button>
													<button class="btn addButton" ng-click="modificaTripletta('<%=userName%>')" ng-show="showEditButton()">
															<span class="glyphicon glyphicon-edit"></span>&nbsp;&nbsp;<strong>Modifica</strong>
														</button>													
												</td>												
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<!-- result table end -->

						</div>
					</div>
				</div>
				</form>
			</div>

		</div>
	</div>
</div>
<!--<pre>{{ctrl.tripletta | json}}</pre> -->