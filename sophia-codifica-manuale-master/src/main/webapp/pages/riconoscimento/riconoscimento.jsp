<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="../navbar.jsp" %>

<div class="bodyContents">
  <div class="mainContainer row-fluid">
    <div id="companyLogo" class="navbar commonActionsContainer noprint">
      <div class="actionsContainer row-fluid">
        <div class="span2">
          <span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE"
                                         alt="SIAE">&nbsp;</span>
        </div>
        <span class="btn-toolbar span4">
					<div class="pageNumbers alignTop pull-right"
               style="text-align: center;">
		 				<span class="pageNumbersText"><strong>Riconoscimento</strong></span>
					</div>
				</span>
      </div>
    </div>

    <div class="contentsDiv marginLeftZero" id="rightPanel"
         style="min-height: 80px;">
      <div class="listViewPageDiv">

        <!-- actions and pagination -->
        <form name="myForm" novalidate>
          <input type="hidden" ng-model="ctrl.search.navbarTab">
          <input type="hidden" ng-model="ctrl.search.execute">
          <input type="hidden" ng-model="ctrl.search.maxrows">
          <input type="hidden" ng-model="ctrl.search.first">
          <input type="hidden" ng-model="ctrl.search.last">
          <input type="hidden" ng-model="ctrl.search.sortType">
          <input type="hidden" ng-model="ctrl.search.sortReverse">
          <input type="hidden" ng-model="ctrl.search.currentTimeMillis">
          <input type="hidden" ng-model="ctrl.search.hashId">


          <input type="hidden" ng-model="ctrl.unidentifiedSong.hashId">
          <input type="hidden" ng-model="ctrl.unidentifiedSong.title">
          <input type="hidden" ng-model="ctrl.unidentifiedSong.artists">
          <input type="hidden" ng-model="ctrl.unidentifiedSong.roles">
          <input type="hidden" ng-model="ctrl.unidentifiedSong.priority">
          <input type="hidden" ng-model="ctrl.unidentifiedSong.insertTime">
          <input type="hidden" ng-model="ctrl.unidentifiedSong.lastAutoTime">
          <input type="hidden" ng-model="ctrl.unidentifiedSong.lastManualTime">
          <input type="hidden" ng-model="ctrl.unidentifiedSong.siadaTitle">
          <input type="hidden" ng-model="ctrl.unidentifiedSong.siadaArtists">

          <input type="hidden" ng-model="ctrl.user"
                 value="<%=(String) session.getAttribute("sso.user.userName")%>">
          <div class="listViewTopMenuDiv noprint">
            <div class="listViewActionsDiv row-fluid">
              <!--  -->
              <span class="btn-toolbar span4">
							<span class="btn-group">
                  <button class="btn addButton"
                          ng-click="goToRicerca('<%=(String) session.getAttribute("sso.user.userName")%>')">
									<span
                      class="glyphicon glyphicon-search"></span>&nbsp;&nbsp;<strong>Indietro</strong>
								</button>
								<button class="btn addButton"
                        ng-click="showMoveToNext('<%=(String) session.getAttribute("sso.user.userName")%>')">
									<span
                      class="glyphicon glyphicon-forward"></span>&nbsp;&nbsp;<strong>Successiva</strong>
								</button>
 							</span>
						</span>

              <span class="span8 btn-toolbar">
							<div class="listViewActions pull-right">
								<span class="btn-group">
									<button class="btn addButton"
                          ng-click="showInsertCode('<%=(String) session.getAttribute("sso.user.userName")%>')">
										<span class="glyphicon glyphicon-pencil"></span>&nbsp;&nbsp;<strong>Inserimento Manuale</strong>
									</button>
									<button class="btn addButton"
                          ng-click="showProvisionalCode('<%=(String) session.getAttribute("sso.user.userName")%>')">
										<span class="glyphicon glyphicon-hourglass"></span>&nbsp;&nbsp;<strong>Codice Provvisorio</strong>
									</button>
									<button class="btn addButton"
                          ng-click="showUnmanagedCode('<%=(String) session.getAttribute("sso.user.userName")%>')">
										<span class="glyphicon glyphicon-thumbs-down"></span>&nbsp;&nbsp;<strong>Non Codificabile</strong>
									</button>
									<button class="btn addButton"
                          ng-click="showReject('<%=(String) session.getAttribute("sso.user.userName")%>')">
										<span class="glyphicon glyphicon-eye-close"></span>&nbsp;&nbsp;<strong>Rifiuta</strong>
									</button>
	 							</span>
	 				 		</div>
					 		<div class="clearfix"></div>
					 	</span>
            </div>
          </div>
        </form>
        <!-- riga da riconoscere -->
        <!-- table -->
        <div class="listViewContentDiv" id="listViewContents">

          <div class="contents-topscroll noprint">
            <div class="topscroll-div" style="width: 95%">&nbsp;</div>
          </div>

          <div class="listViewEntriesDiv contents-bottomscroll">
            <div class="bottomscroll-div" style="width: 95%; min-height: 80px">

              <table class="table table-bordered listViewEntriesTable">
                <thead>
                <tr class="listViewHeaders">
                  <th nowrap><a href="" class="listViewHeaderValues">Titolo</a>
                  </th>
                  <th nowrap><a href="" class="listViewHeaderValues">Artisti</a>
                  </th>
                  <th nowrap><a href="" class="listViewHeaderValues">Tipologia
                    Artisti</a></th>
                  <th nowrap><a href="" class="listViewHeaderValues">Priorit&agrave;</a>
                  </th>
                  <th nowrap></th>
                </tr>
                </thead>
                <tbody>
                <tr class="listViewEntries">

                  <td class="listViewEntryValue medium" nowrap>
                    {{ctrl.unidentifiedSong.title}}
                  </td>
                  <td class="listViewEntryValue medium">
                    {{ctrl.unidentifiedSong.artists.split('|').join(', ')}}
                  </td>
                  <td class="listViewEntryValue medium">
                    {{ctrl.unidentifiedSong.roles.split('|').join(', ')}}
                  </td>
                  <td class="listViewEntryValue medium" nowrap>
                    {{ctrl.unidentifiedSong.priority | number}}
                  </td>

                  <td nowrap class="medium">
                    <div class="actions pull-right">
                      <!--  -->
                      <span class="actionImages">
						 					<a ng-click="showInsertCode('<%=(String) session.getAttribute("sso.user.userName")%>')"><i
                          title="Inserimento Manuale"
                          class="glyphicon glyphicon-pencil alignMiddle"></i></a>&nbsp;
						 					<a ng-click="showProvisionalCode('<%=(String) session.getAttribute("sso.user.userName")%>')"><i
                          title="Codice Provvisorio"
                          class="glyphicon glyphicon-hourglass alignMiddle"></i></a>&nbsp;
						 					<a ng-click="showUnmanagedCode('<%=(String) session.getAttribute("sso.user.userName")%>')"><i
                          title="Non Codificabile"
                          class="glyphicon glyphicon-thumbs-down alignMiddle"></i></a>&nbsp;
						 					<a ng-click="showReject('<%=(String) session.getAttribute("sso.user.userName")%>')"><i
                          title="Rifiuta"
                          class="glyphicon glyphicon-eye-close alignMiddle"></i></a>&nbsp;
						 				</span>
                      <!--  -->
                    </div>
                  </td>

                </tr>
                </tbody>
              </table>

            </div>
          </div>
        </div>
        <!-- /table -->

        <!-- risultati SOLR -->
        <!-- table -->
        <div class="listViewContentDiv">

          <div class="contents-topscroll noprint">
            <div class="topscroll-div" style="width: 95%">&nbsp;</div>
          </div>

          <div class="listViewEntriesDiv contents-bottomscroll">
            <div class="bottomscroll-div" style="width: 95%; min-height: 80px">

              <table class="table table-bordered listViewEntriesTable">
                <thead>
                <tr class="listViewHeaders">
                  <th nowrap><a href="" class="listViewHeaderValues">Codice
                    Opera</a></th>
                  <th nowrap><a href="" class="listViewHeaderValues">Titolo</a>
                  </th>
                  <th nowrap><a href="" class="listViewHeaderValues">Autori</a>
                  </th>
                  <th nowrap><a href=""
                                class="listViewHeaderValues">Compositori</a>
                  </th>
                  <th nowrap><a href=""
                                class="listViewHeaderValues">Interpreti</a></th>
                  <th nowrap><a href="" class="listViewHeaderValues">Ambito</a>
                  </th>
                  <th nowrap><a href=""
                                class="listViewHeaderValues">Rilevanza</a></th>
                  <th nowrap><a href=""
                                class="listViewHeaderValues">Confidenza</a></th>
                  <th nowrap></th>
                </tr>
                </thead>
                <tbody>
                <tr class="listViewEntries"
                    ng-repeat="item in ctrl.solrResults | orderBy:'-confidenza'">

                  <td class="listViewEntryValue medium">
                    {{item.opera.codiceOpera}}
                  </td>
                  <td class="listViewEntryValue medium" nowrap>
                    {{item.opera.titoloOT}}
                  </td>
                  <td class="listViewEntryValue medium">
                    {{item.opera.autori.join(', ')}}
                  </td>
                  <td class="listViewEntryValue medium">
                    {{item.opera.compositori.join(', ')}}
                  </td>
                  <td class="listViewEntryValue medium">
                    {{item.opera.interpreti.join(', ')}}
                  </td>
                  <td class="listViewEntryValue medium">{{item.opera.ambito}}
                  </td>
                  <td class="listViewEntryValue medium">{{item.opera.rilevanza |
                    number:3}}
                  </td>
                  <td class="listViewEntryValue medium">{{item.confidenza |
                    number:3}}
                  </td>

                  <td nowrap class="medium">
                    <div class="actions pull-right">
                      <!--  -->
                      <span class="actionImages">
						 					<a ng-click="showSolrResult($event,item)"><i
                          title="Dettagli"
                          class="glyphicon glyphicon-info-sign alignMiddle"></i></a>&nbsp;
						 					<a ng-click="showChooseSolrResult($event,ctrl.unidentifiedSong,item)"><i
                          title="Conferma"
                          class="glyphicon glyphicon-check alignMiddle"></i></a>&nbsp;
						 				</span>
                      <!--  -->
                    </div>
                  </td>

                </tr>
                </tbody>
              </table>

            </div>
          </div>
        </div>
        <!-- /table -->


        <!-- elenco DSR -->
        <!-- table -->
        <div class="listViewContentDiv" id="listViewContents">

          <div class="contents-topscroll noprint">
            <div class="topscroll-div" style="width: 95%">&nbsp;</div>
          </div>

          <div class="listViewEntriesDiv contents-bottomscroll">
            <div class="bottomscroll-div" style="width: 95%; min-height: 80px">

              <table class="table table-bordered listViewEntriesTable">
                <thead>
                <tr class="listViewHeaders">
                  <th nowrap><a href="" class="listViewHeaderValues">DSR</a>
                  </th>
                  <th nowrap><a href="" class="listViewHeaderValues">Album</a>
                  </th>
                  <th nowrap><a href="" class="listViewHeaderValues">ISWC</a>
                  </th>
                  <th nowrap><a href="" class="listViewHeaderValues">ISRC</a>
                  </th>
                  <th nowrap><a href="" class="listViewHeaderValues">Utilizzazioni</a>
                  </th>
                  <th nowrap></th>
                </tr>
                </thead>
                <tbody>
                <tr class="listViewEntries"
                    ng-repeat="item in ctrl.unidentifiedSongDsr | orderBy:'-salesCount'">

                  <td class="listViewEntryValue medium">{{item.idDsr}}</td>
                  <td class="listViewEntryValue medium" nowrap>
                    {{item.albumTitle}}
                  </td>
                  <td class="listViewEntryValue medium">{{item.iswc}}</td>
                  <td class="listViewEntryValue medium">{{item.isrc}}</td>
                  <td class="listViewEntryValue medium">{{item.salesCount}}</td>

                  <td nowrap class="medium">
                    <div class="actions pull-right">
                    </div>
                  </td>

                </tr>
                </tbody>
              </table>

            </div>
          </div>
        </div>
        <!-- /table -->

      </div>
    </div>
  </div>
  <!-- -- >
        <pre>{{ctrl.properties}}</pre>
    < !-- -->
</div>