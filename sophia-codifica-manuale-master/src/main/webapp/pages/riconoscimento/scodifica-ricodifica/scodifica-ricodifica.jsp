<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <%@ include file="../../navbar.jsp" %>

        <div ng-init="init('<%=(String) session.getAttribute("sso.user.userName")%>')" class="bodyContents">

            <div class="mainContainer row-fluid">

                <div class="contents-topscroll noprint">
                    <div class="topscroll-div" style="width: 95%; margin-bottom: 20px">&nbsp;</div>
                </div>

                <div class="sticky">
                    <div id="companyLogo" class="navbar commonActionsContainer noprint">
                        <div class="actionsContainer row-fluid">
                            <div class="span2">
                                <span class="companyLogo">
                                    <img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;
                                </span>
                            </div>
                            <span class="btn-toolbar span8">
                                <span class="customFilterMainSpan btn-group" style="text-align: center;">
                                    <div class="pageNumbers alignTop ">
                                        <span class="pageNumbersText">
                                            <strong>Scodifica e ri-codifica</strong>
                                        </span>
                                    </div>
                                </span>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px;"
                    id="lthroughContainer">
                    <div class="listViewPageDiv">
                        <div class="alert alert-danger" ng-show="messages.error && (!messages.info)">
                            <button type="button" class="close" ng-click="clearMessages()" aria-hidden="true">x</button>
                            <span><strong>Errore: </strong>{{messages.error}}</span>
                        </div>
                        <div class="alert alert-success" ng-show="messages.info && (!messages.error)">
                            <button type="button" class="close" ng-click="clearMessages()" aria-hidden="true">x</button>
                            <span>{{messages.info}}</span>
                        </div>

                        <div class="flexColumnStretch">
                            <!-- buttons -->
                            <div class="flexRowCenterSpaceBtw margin-TB-22">
                                <div class="flexRow20">
                                    <button class="btn addButton"
                                        ng-click="goToRicerca()">
                                        <span
                                            class="glyphicon glyphicon-search"></span>&nbsp;&nbsp;<strong>Ricerca</strong>
                                    </button>
                                    <button ng-show="codedDataTableLength" class="btn addButton margin-left"
                                        ng-click="showMoveToNext()">
                                        <span
                                            class="glyphicon glyphicon-forward"></span>&nbsp;&nbsp;<strong>Successiva</strong>
                                    </button>
                                </div>
                                <div ng-show="codedDataTableLength" class="flexRowCenterEnd">
                                    <button class="btn addButton"
                                        ng-click="showInsertCode()">
                                        <span class="glyphicon glyphicon-pencil"></span>&nbsp;&nbsp;<strong>Inserimento Manuale</strong>
                                    </button>

                                    <button class="btn addButton margin-left"
                                        ng-click="showReject()">
                                        <span
                                            class="glyphicon glyphicon-eye-close"></span>&nbsp;&nbsp;<strong>Blacklist</strong>
                                    </button>
                                </div>
                            </div>
                            <!-- codificato  table -->
                            <div id="codificatoTable" class="margin-TB-22">
                                <h2 class="padding-2x">Opera Codificata</h2>
                                <div class="table-responsive text-nowrap horizontal-scroll">
                                    <table class="table table-bordered listViewEntriesTable">
                                        <thead>
                                            <tr>
                                                <th ng-repeat="head in codedHeaderTitles">{{head}}</th>
                                                <!-- empty column for download button -->
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr mdbTableCol ng-show="codedDataTableLength">
                                                <td class="initialSpacesText">{{codedDataTable.title}}</td>
                                                <td class="initialSpacesText">{{codedDataTable.artists.split('|').join(', ')}}</td>
                                                <td class="initialSpacesText">{{codedDataTable.roles.split('|').join(', ')}}</td>
                                                <td class="initialSpacesText">{{codedDataTable.salesCount | number}}</td>
                                                <td class="initialSpacesText">{{codedDataTable.uuid}}</td>
                                                <td>
                                                    <span class="glyphicon" ng-class="{
                                                        'glyphicon glyphicon-ok': codedDataTable.identificationType == true,
                                                      }" aria-hidden="true">
                                                    </span>
                                                </td>
                                                <td nowrap class="medium">
                                                    <div class="actions pull-right">
                                                        <span class="actionImages">
                                                            <a ng-click="showInsertCode()"><i title="Inserimento Manuale"
                                                                    class="glyphicon glyphicon-pencil alignMiddle"></i></a>&nbsp;
                                                            <a ng-click="showReject()"><i title="Blacklist"
                                                                    class="glyphicon glyphicon-eye-close alignMiddle"></i></a>&nbsp;
                                                        </span>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div ng-show="!codedDataTableLength" style="margin-top: 16px; color: hsl(240deg 41% 54%);">
                                        <div style="text-align: center;">Al momento non ci sono record.</div>
                                    </div>
                                </div>
                            </div>

                            <div class="" style="border-bottom: 2px solid #cccccc;"></div>

                            <!-- risultati SOLR -->
                            <div id="solrTable" class="margin-TB-22">
                                <h2 class="padding-2x">Proposte di codifica</h2>
                                <div class="table-responsive text-nowrap horizontal-scroll">
                                    <table class="table table-bordered listViewEntriesTable">
                                        <thead>
                                            <tr>
                                                <th ng-repeat="head in solrHeaderTitles">{{head}}</th>
                                                <!-- empty column for download button -->
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="listViewEntries"
                                                ng-repeat="item in solrResults | limitTo: solrLimit">

                                                <td class=" initialSpacesText listViewEntryValue medium">
                                                    {{item.opera.codiceOpera}}
                                                </td>
                                                <td class=" initialSpacesText listViewEntryValue medium" nowrap>
                                                    {{item.opera.titoloOT}}
                                                </td>
                                                <td class=" initialSpacesText listViewEntryValue medium">
                                                    {{item.opera.autori.join(', ')}}
                                                </td>
                                                <td class=" initialSpacesText listViewEntryValue medium">
                                                    {{item.opera.compositori.join(', ')}}
                                                </td>
                                                <td class=" initialSpacesText listViewEntryValue medium">
                                                    {{item.opera.interpreti.join(', ')}}
                                                </td>
                                                <td class=" initialSpacesText listViewEntryValue medium">{{item.opera.ambito}}
                                                </td>
                                                <td class=" initialSpacesText listViewEntryValue medium">{{item.opera.rilevanza |
                                                    number:3}}
                                                </td>
                                                <td class=" initialSpacesText listViewEntryValue medium">{{item.confidenza |
                                                    number:3}}
                                                </td>

                                                <td nowrap class="medium">
                                                    <div class="actions pull-right">
                                                        <span class="actionImages">
                                                            <a ng-click="showSolrResult(item)"><i
                                                                    title="Dettagli"
                                                                    class="glyphicon glyphicon-info-sign alignMiddle"></i></a>&nbsp;
                                                            <a
                                                                ng-click="showChooseSolrResult(item)"><i
                                                                    title="Conferma"
                                                                    class="glyphicon glyphicon-check alignMiddle"></i></a>&nbsp;
                                                        </span>
                                                    </div>
                                                </td>

                                            </tr>
                                        </tbody>
                                    </table>
                                    <div ng-show="!solrResults.length" style="margin-top: 16px; color: hsl(240deg 41% 54%);">
                                        <div style="text-align: center;">Al momento non ci sono proposte.</div>
                                    </div>
                                </div>
                            </div>
                            <div class="" style="border-bottom: 2px solid #cccccc;"></div>
                            <!-- elenco DSR -->
                            <div id="dsrTable" class="margin-TB-22">
                                <h2 class="padding-2x">Dsr</h2>
                                <div class="table-responsive text-nowrap horizontal-scroll">
                                    <table class="table table-bordered listViewEntriesTable">
                                        <thead>
                                            <tr>
                                                <th ng-repeat="head in dsrHeaderTitles">{{head}}</th>
                                                <!-- empty column for download button -->
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="listViewEntries"
                                                ng-repeat="item in unidentifiedSongDsr | orderBy:'-salesCount'">

                                                <td class=" initialSpacesText listViewEntryValue medium">{{item.idDsr}}</td>
                                                <td class=" initialSpacesText listViewEntryValue medium" nowrap>
                                                    {{item.albumTitle}}
                                                </td>
                                                <td class=" initialSpacesText listViewEntryValue medium">{{item.iswc}}</td>
                                                <td class=" initialSpacesText listViewEntryValue medium">{{item.isrc}}</td>
                                                <td class=" initialSpacesText listViewEntryValue medium">{{item.salesCount}}</td>

                                                <td nowrap class="medium">
                                                    <div class="actions pull-right">
                                                    </div>
                                                </td>

                                            </tr>
                                        </tbody>
                                    </table>
                                    <div ng-show="!unidentifiedSongDsr.length" style="margin-top: 16px; color: hsl(240deg 41% 54%);">
                                        <div style="text-align: center;">Al momento non ci sono record.</div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>