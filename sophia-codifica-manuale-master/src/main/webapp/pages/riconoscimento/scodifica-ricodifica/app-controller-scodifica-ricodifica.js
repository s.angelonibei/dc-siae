codmanApp.controller('scodifica-ricodificaCtrl', ['$scope', '$route', 'ngDialog', 'scodificaRicodificaService', 'ricercaCodificatoService', 'riconoscimentoService', '$location', 'configService',
    function ($scope, $route, ngDialog, scodificaRicodificaService, ricercaCodificatoService, riconoscimentoService, $location, configService) {

        $scope.codedHeaderTitles = ["Titolo", "Artisti", "Tipologia Artisti", "Priorita", "Codice Uuid", "Flag codifica manuale"];
        $scope.solrHeaderTitles = ["Codice Opera", "Titolo", "Autori", "Compositori", "Interpreti", "Ambito", "Rilevanza", "Confidenza"];
        $scope.dsrHeaderTitles = ["Dsr", "Album", "ISWC", "ISRC", "Utilizzazioni"];
        $scope.routeHashId = null;
        $scope.solrLimit = configService.solrLimitResult || 3;

        $scope.messages = {
            info: "",
            error: "",
            warning: ""
        };
        $scope.codedDataTable = {};
        $scope.autocompleteItems = [];
        $scope.unidentifiedSongDsr = [];

        $scope.clearMessages = function () {
            $scope.messages = {
                info: "",
                error: "",
                warning: ""
            }
        };

        $scope.warningMessage = function (message) {
            setTimeout(() => {
                $scope.messages.warning = message;
                $scope.$apply();
            }, 100);
        };

        $scope.infoMessage = function (message) {
            setTimeout(() => {
                $scope.messages.info = message;
                $scope.$apply();
            }, 100);
        };

        $scope.errorMessage = function (message) {
            setTimeout(() => {
                $scope.messages.error = message;
                $scope.$apply();
            }, 100);
        };

        $scope.goToRicerca = function () {
            $location.path('/ricercaCodificato');
        };


        $scope.showMoveToNext = function () {
            $scope.loadNextSongData();
        };

        $scope.showInsertCode = function () {
            // map to reuse popup
            const self = {
                unidentifiedSong: {
                    ...$scope.codedDataTable
                }
            };
            ngDialog.open({
                template: 'pages/riconoscimento/dialog-insert-code.html',
                plain: false,
                width: '50%',
                data: self,
                controller: ['$scope', 'ngDialog', function (scope, ngDialog) {
                    scope.ctrl = scope.ngDialogData;
                    scope.ctrl.siaeWorkCode = null;
                    scope.cancel = function () {
                        ngDialog.closeAll(false);
                    };
                    scope.save = function () {
                        console.log(scope.ctrl);
                        const insertManual = {
                            siaeWorkCode: scope.ctrl.siaeWorkCode,
                            ...scope.ctrl.unidentifiedSong
                        };
                        ngDialog.closeAll(insertManual);
                    };
                }]
            }).closePromise.then(({ value }) => {
                $scope.handleInsertManual(value);
            });
        };

        /* blacklist */
        $scope.showReject = function () {
            const data = {
                tableHeader: ["Titolo", "Artisti", "Ruoli"],
                tableData: {
                    titolo: $scope.codedDataTable.title,
                    artisti: $scope.codedDataTable.artists.split('|').join(', '),
                    ruoli: $scope.codedDataTable.roles.split('|').join(', ')
                },
                subTitle: "Si vuole inserire la seguente combinazione anagrafica nella blacklist?",
                message: "",
                actionRequired: true
            };
            ngDialog.open({
                template: 'pages/configurazione-pricing/popup/update-old-conf-confirmation.html',
                width: '60%',
                className: 'ngdialog-theme-default dialogOverflow',
                controller: ['$scope', 'ngDialog', UpdateOldConfirmationDialogController],
                data
            }).closePromise
                .then(({ value }) => {
                    if (value && value.confirm) {
                        const data = {
                            title: $scope.codedDataTable.siadaTitle,
                            artists: $scope.codedDataTable.siadaArtists,
                            uuid: $scope.codedDataTable.uuid
                        };
                        ricercaCodificatoService
                            .addToBlacklist(data)
                            .then(response => {
                                if (response) {
                                    $scope.infoMessage("Opera inserita correttamente nella Blacklist");
                                    $scope.loadNextSongData();
                                }
                            });
                    }
                });
        };

        $scope.showSolrResult = function (solrResult) {
            const self = {
                solrResult: {
                    ...solrResult
                }
            };
            ngDialog.open({
                template: 'pages/riconoscimento/dialog-solr-result-details.html',
                plain: false,
                width: '60%',
                data: self,
                controller: ['$scope', 'ngDialog', function (scope, dialog) {
                    scope.ctrl = scope.ngDialogData;
                    scope.cancel = function () {
                        dialog.closeAll(false);
                    };
                    scope.save = function () {
                        dialog.closeAll(true);
                    };
                }]
            })
        };

        $scope.showChooseSolrResult = function (solrResult) {
            const self = {
                solrResult: {
                    ...solrResult
                }
            };
            ngDialog.open({
                template: 'pages/riconoscimento/dialog-choose-solr-result.html',
                plain: false,
                width: '50%',
                data: self,
                className: 'ngdialog-theme-default ngdialog-theme-custom',
                controller: ['$scope', 'ngDialog', function (scope, ngDialog) {
                    scope.ctrl = scope.ngDialogData;
                    scope.cancel = function () {
                        ngDialog.closeAll(false);
                    };
                    scope.save = function () {
                        console.log(scope.ctrl);
                        ngDialog.closeAll(scope.ctrl);
                    };
                }]
            }).closePromise.then(({ value }) => {
                const opera = value.solrResult.opera;
                const insertManual = {
                    hashId: $scope.codedDataTable.hashId,
                    title: $scope.codedDataTable.title,
                    artists: $scope.codedDataTable.artists,
                    siadaTitle: $scope.codedDataTable.siadaTitle,
                    siadaArtists: $scope.codedDataTable.siadaArtists,
                    roles: $scope.codedDataTable.roles,
                    siaeWorkCode: opera.codiceOpera
                };
                $scope.handleInsertManual(insertManual);
            });
        };

        /* dataObj: 
        *   hashId
            title
            artists
            siadaTitle
            siadaArtists
            roles
            siaeWorkCode
        *  */
        $scope.handleInsertManual = function (dataObj) {
            if (typeof dataObj === "object") {
                ricercaCodificatoService
                    .insertManual(dataObj)
                    .then(_response => {
                        $scope.infoMessage("Codifica avvenuta con successo.");
                        $scope.loadNextSongData();
                    }, function (_error) {
                        $scope.errorMessage('Errore durante la richiesta.');
                    });
            }
        };

        $scope.callSolrSearch = function () {
            const paramsSolr = {
                title: $scope.codedDataTable.title,
                artists: $scope.codedDataTable.artists,
                roles: $scope.codedDataTable.roles
            };

            scodificaRicodificaService
                .solrSearch(paramsSolr)
                .then(({ data }) => {
                    setTimeout(() => {
                        $scope.solrResults = data;
                        $scope.$apply();
                    }, 100);
                }, (_error) => {
                    $scope.errorMessage('Qualcosa è andato storto.');
                });
        };

        $scope.getDsrData = function () {
            ricercaCodificatoService
                .getDsrByHashId($scope.codedDataTable.hashId)
                .then(({data}) => {
                    setTimeout(() => {
                        $scope.unidentifiedSongDsr = data;
                        $scope.$apply();
                    }, 100);
                }, (_error) => {
                    $scope.errorMessage('Qualcosa è andato storto.');
                });
        };

        $scope.loadCurrentSongData = function (hashId) {
            const user = $scope.user;
            scodificaRicodificaService
                .identifiedSong(hashId, user)
                .then(({data}) => {
                    $scope.loadData(data);
                }, function (_error) {
                    $scope.errorMessage('Impossibile caricare i dati.');
                });
        };

        $scope.loadNextSongData = function () {
            const params = {
                salesCount: $scope.codedDataTable.salesCount.toString(),
                user: $scope.user
            };
            const hashId = $scope.codedDataTable.hashId;
            scodificaRicodificaService
                .getNextSong(hashId, params)
                .then(({data}) => {
                    $scope.loadData(data);
                }, function (_error) {
                    $scope.errorMessage('Impossibile caricare i dati.');
                });
        };

        $scope.loadData = function (data) {
            setTimeout(() => {
                $scope.codedDataTableLength = Object.values(data).length;
                if($scope.codedDataTableLength) {
                    $scope.codedDataTable = {
                        ...data,
                        identificationType: !!data.identificationType 
                    };
                    $scope.callSolrSearch();
                    $scope.getDsrData();
                } else {
                    $scope.solrResults = [];
                    $scope.unidentifiedSongDsr = [];
                }
                $scope.$apply();
            }, 100);
        };

        $scope.init = function (user) {
            const hashId = $route.current.params ? $route.current.params.hashId : undefined;
            $scope.user = user;
            // $scope.routeHashId = hashId;
            $scope.loadCurrentSongData(hashId);
        };

        // $scope.init();

    }]);