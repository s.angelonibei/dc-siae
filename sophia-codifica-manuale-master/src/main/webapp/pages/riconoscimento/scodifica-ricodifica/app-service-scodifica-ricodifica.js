codmanApp.service('scodificaRicodificaService', ['$http', function ($http) {

    var service = this;
    const baseIdentifiedUrl = 'rest/new-load-identified';
    const solrSearch = 'rest/solr/search';
    const song = `${baseIdentifiedUrl}/song/{hashId}`;
    const nextSong = `${baseIdentifiedUrl}/nextSong/{hashId}`;

    //service.prefixUrlRequest = `${$location.protocol()}://${$location.host()}:${$location.port()}/${baseUrl}`;
    /* 
    *   Input Params
    *   title, artists, roles
    * */
    // search on solr
    service.solrSearch = function (params) {
        return $http({
            method: 'GET',
            url: solrSearch,
            params
        });
    };

    service.identifiedSong = function (hashId, user) {

        return $http({
            method: 'GET',
            url: `${song}`.replace("{hashId}", hashId),
            params: { user }
        });

    };
    /* params:
    {
        user: string,
        salesCount: string
    } */
    service.getNextSong = function (hashId, params) {
        return $http({
            method: 'GET',
            url: `${nextSong}`.replace("{hashId}", hashId),
            params
        });

    };

}]);