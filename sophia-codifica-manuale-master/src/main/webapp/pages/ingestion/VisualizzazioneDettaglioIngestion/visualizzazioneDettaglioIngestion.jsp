<%@ include file="../../navbar.jsp"%>

<div class="bodyContents" ng-init="init()">
	<div class="mainContainer row-fluid">

		<div class="contents-topscroll noprint">
			<div class="topscroll-div" style="width: 95%;margin-bottom: 20px">&nbsp;</div>
		</div>

		<div class="sticky">
			<div id="companyLogo" class="navbar commonActionsContainer noprint">
				<div class="actionsContainer row-fluid">
					<div class="span2">
						<span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
					</div>
					<span class="btn-toolbar span5">
						<div class="pageNumbers alignTop pull-right" style="text-align: center;">
							<span id="up" class="pageNumbersText"><strong>{{ctrl.pageTitle}}</strong></span>
						</div>
					</span>
				</div>
			</div>
		</div>

		<div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px;">
			<div class="listViewPageDiv">

				<!-- start Toolbar -->
				<div class="listViewTopMenuDiv noprint">
					<div class="listViewActionsDiv row-fluid">
					<span class="span6 btn-toolbar">
						<div class="listViewActions pull-left">
							<span style="padding-right: 5px">
								<strong>Dettaglio Ingestion SAP</strong>
							</span>
						</div>
					</span>						
					<span class="span6 btn-toolbar">
						<div class="listViewActions pull-right">
							<button title="Indietro" class="btn" id="goBackButton" type="button" ng-click="back()">
								<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Indietro</strong>
							</button>
						</div>
					</span>
					</div>
				</div>
				<!-- end Toolbar -->

				<!-- start file details -->
				<div class="listViewTopMenuDiv noprint">
					<div class="listViewActionsDiv row-fluid">
					<span class="span6 btn-toolbar">
						<div class="row-fluid">
							<div class="span2">Data Ingestion:</div>
							<div class="span10">{{ctrl.dataAggiornamento | date: 'dd-MM-yyyy'}}</div>
						</div>
						<div class="row-fluid pull-left">
							<div class="span2">Nome File:</div>
							<div class="span10">{{ctrl.nomeFile}}</div>
						</div>
					</span>
					<span class="span6 btn-toolbar">
						<div class="listViewActions pull-right" style="width:100%; margin-bottom: 8px;">
							<button class="btn pull-right" id="downloadFileSapButton" type="button" ng-click="downloadFileSap()">
								<span class="glyphicon glyphicon-cloud-download"></span>&nbsp;&nbsp;<strong>Scarica file SAP</strong>
							</button>
						</div>
						<div class="listViewActions pull-right">
							<button class="btn pull-right" ng-click="downloadFileCsv()">
								<span class="glyphicon glyphicon-download"></span>&nbsp;&nbsp;<strong>Dati elaborati CSV</strong>
							</button>
						</div>
					</span>
					</div>
				</div>
				<!-- end file details -->

				<!-- start toolbar tabella | paginazione -->
				<div class="listViewTopMenuDiv noprint">
					<div class="listViewActionsDiv row-fluid">
						<span class="span6 btn-toolbar">
							<div class="listViewActions pull-left">
								<div class="pageNumbers alignTop ">	
								<span class="pageNumbersText" style="padding-right: 5px">
										Ricerca Tripletta:
								</span>									
								<input style="width: 180px; padding-top: 3px; padding-bottom: 6px;" ng-model="ctrl.filterTripletta"/>
								<button class="btn" type="button" ng-click="filter()" style="padding: 5px 10px 2px 10px;">
								<span class="glyphicon glyphicon-filter"></span>
								</button>
								</div>
							</div>
						</span>
						<span class="span6 btn-toolbar">
							<div class="listViewActions pull-right">
								<div class="pageNumbers alignTop ">
									<span ng-show="ctrl.totRecords > 50">
										<span class="pageNumbersText" style="padding-right: 5px">
											Record da <strong>{{ctrl.page*50+1}}</strong> a <strong>{{(ctrl.page*50) + ctrl.sampling.length}}</strong>
											su
											<strong>{{ctrl.totRecords}}</strong>
										</span>
										<button title="Precedente" class="btn" id="listViewPreviousPageButton" type="button" ng-click="navigateToPreviousPage()"
											ng-show="ctrl.page > 0">
											<span class="glyphicon glyphicon-chevron-left"></span>
										</button>
										<button title="Successiva" class="btn" id="listViewNextPageButton" type="button" ng-click="navigateToNextPage()"
											ng-show="ctrl.page < ctrl.pageTot">
											<span class="glyphicon glyphicon-chevron-right"></span>
										</button>
										<button title="Fine" class="btn" id="listViewNextPageButton" type="button" ng-click="navigateToEndPage()"
											ng-show="ctrl.page < ctrl.pageTot">
											<span class="glyphicon glyphicon-step-forward"></span>
										</button>
									</span>
								</div>
							</div>
						</span>
					</div>
				</div>
				<!-- end toolbar tabella | paginazione -->				

				<!-- start table risultati -->
				<div class="listViewContentDiv" id="listViewContents">
					<div class="listViewEntriesDiv contents-bottomscroll">
						<div class="bottomscroll table-responsive" style="min-height: 80px">
							<table class="table table-bordered listViewEntriesTable tableFormatted">
								<thead>
									<tr class="listViewHeaders">
										<!-- PRIMA DI CAMBIARE LA CLASSE DELLO SPAN, CONTROLLARE LA LOGICA SUL CONTROLLER DI ANGULAR. -->
										<th style="width:15%">Tripletta&nbsp;<span ng-click="orderImage('spanTripletta','tripletta')"
												id="spanTripletta" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
										<th style="width:5%">Tipo&nbsp;<span ng-click="orderImage('spanTipoTripletta','tipoTripletta')"
											id="spanTipoTripletta" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
										<th>Tipo Emittente&nbsp;<span ng-click="orderImage('spanTipoEmittente','tipoEmittente')"
											id="spanTipoEmittente" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
										<th style="width:8%">Emittente&nbsp;<span ng-click="orderImage('spanEmittente','emittente')"
											id="spanEmittente" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>																							
										<th style="width:10%">Canale&nbsp;<span ng-click="orderImage('spanCanale','canale')"
											id="spanCanale" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
										<!--<th style="width:7%">Periodo&nbsp;<span ng-click="orderImage('spanPeriodo','periodo')"
											id="spanPeriodo" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>-->
										<th style="width:8%">Tipo&nbsp;Diritto&nbsp;<span ng-click="orderImage('spanTipoDiritto','tipoDiritto')"
											id="spanTipoDiritto" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
										<th style="width:9%; text-align:right;">Importo&nbsp;<span ng-click="orderImage('spanImporto','importo')"
											id="spanImporto" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
										<th style="width:8%">Periodo Da&nbsp;<span ng-click="orderImage('spanDataDa','dataDa')"
											id="spanDataDa" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
										<th style="width:7%">Periodo A&nbsp;<span ng-click="orderImage('spanDataA','dataA')"
											id="spanDataA" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
										<th>Tipo&nbsp;Errore&nbsp;<span ng-click="orderImage('spanErrore','errore')"
											id="spanErrore" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
										<th style="width:5%">Azioni&nbsp;<span style="font-size:80%"></span></th>
									</tr>
								</thead>
								<tbody>
									<tr class="listViewEntries" ng-repeat="item in ctrl.sampling">
										<td>{{item.tripletta}}</td>
										<td>{{item.tipoTripletta}}</td>
										<td>{{item.tipoEmittente}}</td>
										<td>{{item.emittente}}</td>
										<td>{{getFirstCanale(item.canale)}}
											<span ng-show="item.canale.indexOf(',') > -1">...<span class="glyphicon glyphicon-info-sign" style="color: rgba(0,180,249,1)" data-toggle="tooltip-canale" 
												title="{{getListaCanali(item.canale)}}"
												data-html="true" rel="tooltip"></span>
												<script type="text/javascript">
													$('[data-toggle="tooltip-canale"]').tooltip();
												</script>
											</span>
										</td>
										<!--<td>{{getPeriodo(item.periodo)}}</td>-->
										<td>{{item.tipoDiritto}}</td>
										<td style="text-align:right;">
											<div>
												{{item.importo | currency : '&euro;' : 2}}
											</div>
										</td>
										<td>{{item.dataDa | date: 'dd-MM-yyyy'}}</td>
										<td>{{item.dataA | date: 'dd-MM-yyyy'}}</td>
										<td>
											<div ng-show="item.stato!='OK'"><span class="glyphicon glyphicon-info-sign topRightIcon" data-toggle="tooltip" 
												title="{{getRecordSapTable(item.recordSap)}}"
												data-html="true" rel="tooltip"></span>{{item.errore}}
												<script type="text/javascript">
													$('[data-toggle="tooltip"]').tooltip();
												</script>
											</div>
										</td>
										<td><a ng-show="item.stato=='ERR_TRIPLETTA'" ng-click="goToGestioneErrore(item)"><u>Gestione Errore</u></a></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- end table risultati -->					
			</div>
		</div>
	</div>
</div>
<!-- <pre>{{ctrl | json}}</pre> -->