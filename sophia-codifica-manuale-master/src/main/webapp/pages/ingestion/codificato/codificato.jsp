<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%@
	include file="../../navbar.jsp" %>


<div class="bodyContents">

    <div class="mainContainer row-fluid">

        <div id="companyLogo" class="navbar commonActionsContainer noprint">
            <div class="actionsContainer row-fluid">
                <div class="span2">
                    <span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
                </div>
                <span class="btn-toolbar span4">
                    <div class="pageNumbers alignTop pull-right" style="text-align: center;">
                        <span class="pageNumbersText"><strong>Ingestion Codificato SOPHIA</strong></span>
                    </div>
                </span>
            </div>
        </div>

        <div class="alert alert-danger" ng-show="messages.error && (!messages.info)">
            <button type="button" class="close" ng-click="clearMessages()" aria-hidden="true">x</button>
            <span><strong>Errore: </strong>{{messages.error}}</span>
        </div>
        <div class="alert alert-success" ng-show="messages.info && (!messages.error)">
            <button type="button" class="close" ng-click="clearMessages()" aria-hidden="true">x</button>
            <span>{{messages.info}}</span>
        </div>

        <div class="contentsDiv marginLeftZero" id="codificatoContainer" style="min-height: 180px;">
            <div class="listViewPageDiv">
                <div class="listViewTopMenuDiv noprint flexRowHNoneVCenter">
                    <div class="flexRowHCenterVCenter listViewActionsDiv row-fluid">
                        <div class="row-fluid table-bordered uploadContainer textAlignCenter" ngf-drop
                            ng-model="upload.file" ngf-allow-dir="false">
                            <div class="row-fluid" ng-hide="!upload.file" style="margin-bottom: 5px">
                                <strong>{{upload.file.name}}</strong>
                                {{upload.file.$error}} {{upload.file.$errorParam}}</div>
                            <div class="row-fluid" style="margin-top: 20px; margin-bottom: 20px">Drag &amp; Drop file
                            </div>
                            <div ngf-no-file-drop>File Drag/Drop is not supported for this browser</div>
                            <div class="flexRowCenterEnd marginRightLeft4x margin-bottom-2x">
                                <button class="btn addButton floatRight marginRightLeft4x" type="file" ngf-select
                                    name="file" required ng-model="upload.file" ngf-allow-dir="false">carica
                                    file</button>
                                <span ng-if="upload.file">
                                    <button class="btn margin-bottom-x" type="button"
                                        ng-click="uploadFile(upload.file)">
                                        <span class="glyphicon glyphicon-cloud-upload"></span>
                                        Upload
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- pagination -->
                <div class="flexRowCenterEnd">
                    <div class="pageNumbers">
                        <span class="pageNumbersText" style="padding-right:5px">
                            Record da <strong>{{requestParameters.first+1}}</strong> a
                            <strong>{{requestParameters.last}}</strong>
                        </span>
                        <span ng-show="hasPrev || hasNext">
                            <button title="Precedente" class="btn" id="listViewPreviousPageButton" type="button"
                                ng-click="navigateToPreviousPage()" ng-show="hasPrev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                            </button>
                            <button title="Successiva" class="btn" id="listViewNextPageButton" type="button"
                                ng-click="navigateToNextPage()" ng-show="hasNext">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </button>
                        </span>
                    </div>
                </div>
                <!-- table -->
                <div class="flexRowHCenterVCenter margin-top-8x">
                    <div class="table-responsive text-nowrap horizontal-scroll" style="width: 90vw">
                        <table class="table table-bordered listViewEntriesTable">
                            <thead>
                                <tr class="flexRowSpaceCenter">
                                    <th class="fxFlex" ng-class="{
                                            'maxW20' : 'Nome file'== head,
                                            'maxW10' : head == 'Righe totali' || head == 'Righe elaborate',
                                            'placeToCenter': head == 'Scarti'
                                        }" ng-repeat="head in getTableHeader()">
                                        {{head}}
                                    </th>
                                </tr>
                            </thead>
                            <!-- ["Nome file", "Data caricamento file", "Data aggiornamento", "Stato ingestion", "Righe totali", "Righe elaborate"]; -->
                            <tbody>
                                <tr mdbTableCol class="flexRowSpaceCenter"
                                    ng-repeat="item in codificatoData track by $index">
                                    <td class="fxFlex maxW20">
                                        <a download="{{item.nomeFile}}" class="dotted-ward link"
                                            ng-href="downloadExtendedDSR?url={{item.pathCsv | urlEncode}}">
                                            {{item.nomeFile}}
                                        </a>
                                    </td>
                                    <td class="fxFlex ">
                                        {{item.dataCaricamento | date : 'medium'}}
                                    </td>
                                    <td class="fxFlex ">
                                        {{item.dataAggiornamento | date : 'medium'}}
                                    </td>
                                    <td class="fxFlex " class="textAlignCenter">
                                        <span ng-if="item.statoIngestion" ng-class="{
                                            'completed': item.statoIngestion.toLowerCase() == 'completato',
                                            'error': item.statoIngestion.toLowerCase().includes('errore')
                                            }" aria-hidden="true">
                                            {{item.statoIngestion}}
                                        </span>
                                        <span
                                            ng-if="item.statoIngestion && item.statoIngestion.toLowerCase().includes('errore')"
                                            class="marginRightLeft4x">
                                            <i title="{{item.descrizioneErrore}}"
                                                class="glyphicon glyphicon-warning-sign alignMiddle"></i>
                                        </span>
                                    </td>
                                    <td class="maxW10 fxFlex placeToRight">
                                        {{item.righeTotali}}
                                    </td>
                                    <td class="maxW10 fxFlex placeToRight">
                                        {{item.righeElaborate}}
                                    </td>
                                    <td class="fxFlex placeToCenter">
                                        <span ng-show="item.pathCsvScarti">
                                            <a download="{{item.scartoFileName}}"
                                                ng-href="downloadExtendedDSR?url={{item.pathCsvScarti | urlEncode}}">
                                                <i title="Download file scarto"
                                                    class="glyphicon glyphicon-cloud-download alignMiddle"></i>
                                            </a>
                                        </span>
                                    </td>

                                </tr>
                            </tbody>
                        </table>
                        <div ng-show="!codificatoData.length" style="margin-top: 16px;">
                            <div style="text-align: center;">Al momento non ci sono record.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>