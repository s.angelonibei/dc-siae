//(function(){

/**
 * CodificatoCtrl
 * 
 * @path /codificato
 */
codmanApp.controller('CodificatoCtrl', ['$scope', 'codificatoService', '$interval',
    function ($scope, codificatoService, $interval) {
        const TABLE_HEADER = ["Nome file", "Data caricamento file", "Data aggiornamento", "Stato ingestion", "Righe totali", "Righe elaborate", "Scarti"];
        $scope.messages = {
            info: "",
            error: ""
        };

        $scope.requestParameters = {
            first: 0,
            last: 50,
            maxRows: 50
        };
        $scope.codificatoData = [];

        $scope.upload = {
            file: null
        };
        $scope.onInit = function () {
            $scope.getTableList();
            $scope.startInterval();
        };

        $scope.infoMessage = function (message) {

            setTimeout(() => {
                $scope.messages.info = message;
                $scope.$apply();
            }, 100);
        };

        $scope.errorMessage = function (message) {
            setTimeout(() => {
                $scope.messages.error = message;
                $scope.$apply();
            }, 100);
        };

        $scope.clearMessages = function () {
            $scope.messages = {
                info: "",
                error: ""
            };
        }

        $scope.getTableHeader = function () {
            return TABLE_HEADER;
        };

        $scope.uploadFile = function (file) {
            codificatoService.checkElaboration()
                .then(({ alreadyElaborating: elaborating }) => {
                    if (!elaborating) {
                        codificatoService.uploadFile(file)
                            .then(_resp => {
                                $scope.infoMessage("Caricamento file riuscito.");
                                $scope.getTableList();
                            }, () => {
                                $scope.errorMessage("Errore nel caricamento del file.");
                            });
                    } else {
                        $scope.errorMessage("Elaborazione in corso, impossibile caricare il file.");
                    }
                }, () => {
                    $scope.errorMessage("Qualcosa è andato storto.");
                });
        };

        $scope.getTableList = function () {
            codificatoService.getAll($scope.requestParameters.first, $scope.requestParameters.last)
                .then(data => {
                    setTimeout(() => {
                        const newData = data.rows
                            .map(el => {
                                if (el.pathCsvScarti) {
                                    const scartoFileName = _.last(el.pathCsvScarti.split("/"));
                                    el = Object.assign(el, { scartoFileName });
                                }

                                return el.righeTotali && (el.statoIngestion.toLowerCase() === 'errore' || el.statoIngestion.toLowerCase() === 'in corso') ?
                                    { ...el, statoIngestion: el.statoIngestion + "  (" + getPercentage(el) + ")%" } :
                                    el;
                            });
                        const { hasNext, hasPrev, first, last } = data;
                        $scope.hasPrev = hasPrev;
                        $scope.hasNext = hasNext;
                        $scope.codificatoData = newData;
                        $scope.$apply();
                    }, 100);
                }, () => {
                    $scope.errorMessage("Qualcosa è andato storto.");
                });
        };

        $scope.startInterval = function () {
            if (!$scope.getTableListRequestDashboard) {
                $scope.getTableListRequestDashboard = $interval($scope.getTableList, 30000);
            }
        };

        $scope.stopInterval = function () {
            $interval.cancel($scope.getTableListRequestDashboard);
        };

        $scope.navigateToNextPage = function () {
            $scope.requestParameters.first = $scope.requestParameters.last;
            $scope.requestParameters.last = $scope.requestParameters.last + $scope.requestParameters.maxRows;
            $scope.getTableList();
          };
      
          $scope.navigateToPreviousPage = function () {
            $scope.requestParameters.last = $scope.requestParameters.first;
            $scope.requestParameters.first = Math.max($scope.requestParameters.first - $scope.requestParameters.maxRows, 0);
            $scope.getTableList();
          };
      

        $scope.onInit();

        function getPercentage(el) {
            const { righeTotali, righeElaborate } = el;
            return ((parseFloat(righeElaborate) / parseFloat(righeTotali)) * 100).toFixed(0);
        }
    }]);

