codmanApp.service('codificatoService', ['$http', 'Upload',
    function ($http, Upload) {
        let service = this;
        const baseUrl = "rest/ingestion-codificato";
        const upload = "/upload-csv";
        const getAllConfig = '/all';
        const checkElaboration = '/checkIfElaborating';
        /* {​​​​
            "ID": 27,
            "nomeFile": "HIT_output_mod.csv",
            "dataCaricamento": "2020-10-16T13:57:39.000Z",
            "statoIngestion": "COMPLETATO",
            "righeElaborate": 200,
            "righeTotali": 265,
            "descrizioneErrore": "qwdqwdwqd",
            "pathCsv": "s3://siae-dev-datalake/debug/ingestion-codificato/HIT_output_mod.csv",
            "pathCsvScarti": "efewrfwfe",
            "dataAggiornamento": "2020-10-16T14:15:45.000Z"
        }​​​​ */
        service.getAll = function (first, last) {
            const params = {
                first,
                last
            };
            return new Promise((resolve, reject) => {
                $http({
                    method: 'GET',
                    url: `${baseUrl}${getAllConfig}`,
                    params
                }).then(({ data }) => {
                    resolve(data);
                }, () => {
                    reject(data);
                });
            });
        };

        service.uploadFile = function (file) {
            return new Promise((resolve, reject) => {
                Upload.upload({
                    url:`${baseUrl}${upload}`,
                    data: {
                        file
                    }
                }).then((resp) => {
                    resolve(true);
                }, () => {
                    reject(false);
                });
            });
        };

        service.checkElaboration = function () {
            return new Promise((resolve, reject) => {
                $http({
                    method: 'GET',
                    url: `${baseUrl}${checkElaboration}`,
                }).then(({ data }) => {
                    resolve(data);
                }, () => {
                    reject(false);
                });
            });
        };

    }]);