<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../navbar.jsp"%> <!-- path has to be adjusted-->

<div class="bodyContents" ng-init="init()">
	<div class="mainContainer row-fluid">

		<div class="contents-topscroll noprint">
			<div class="topscroll-div" style="width: 95%;margin-bottom: 20px">&nbsp;</div>
		</div>

		<div class="sticky">
			<div id="companyLogo" class="navbar commonActionsContainer noprint">
				<div class="actionsContainer row-fluid">
					<div class="span2">
						<span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
					</div>
					<span class="btn-toolbar span5">
						<div class="pageNumbers alignTop pull-right" style="text-align: center;">
							<span id="up" class="pageNumbersText"><strong>{{ctrl.pageTitle}}</strong></span>
						</div>
					</span>
				</div>
			</div>
		</div>

		<div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px;">
			<div class="listViewPageDiv">

				<!-- start table inserimento campi-->
				<div class="listViewActionsDiv row-fluid">
					<table class="table table-bordered blockContainer showInlineTable equalSplit">
						<thead>
							<tr>
								<th class="blockHeader" colspan="6" ng-show="ctrl.hideForm"><span ng-click="ctrl.hideForm=false" class="glyphicon glyphicon-expand"></span>&nbsp;Parametri
									Ricerca</th>
								<th class="blockHeader" colspan="6" ng-hide="ctrl.hideForm"><span ng-click="ctrl.hideForm=true" class="glyphicon glyphicon-collapse-down"></span>&nbsp;Parametri
									Ricerca</th>
							</tr>
						</thead>
						<tbody ng-hide="ctrl.hideForm">
							<tr>
								<td class="fieldLabel medium" style="width: 16.66%"></td>
								<td class="fieldLabel medium" style="width: 16.66%"><label class="muted pull-right marginRight10px" style="padding-top: 10px">
										Data Ingestion SAP Da</label></td>
								<td style="width: 16.66%">
									<div class="controls">
										<div class="input-append date" id="datepickerDataAggiornamentoDa" data-date-format="dd-mm-yyyy">
											<input placeholder="gg-mm-aaaa" type="text" name="date" ng-model="ctrl.selectedDataAggiornamentoDa" style="width: 125px; height: 23px">
											<span class="add-on" style="height: 23px"><i class="icon-th"></i></span>
										</div>
										<script type="text/javascript">
											$("#datepickerDataAggiornamentoDa")
												.datepicker(
													{
														format: 'dd-mm-yyyy',
														language: 'it',
														viewMode : "days",
														minViewMode : "days",
														orientation : "bottom auto",
														autoclose : "true"
													});
										</script>
									</div>
								</td>
								<td class="fieldLabel medium" style="width: 16.66%"><label class="muted pull-right marginRight10px" style="padding-top: 10px">
									Data Ingestion SAP A</label></td>
								<td style="width: 16.66%">
									<div class="controls">
										<div class="input-append date" id="datepickerDataAggiornamentoA" data-date-format="dd-mm-yyyy">
											<input placeholder="gg-mm-aaaa" type="text" name="date" ng-model="ctrl.selectedDataAggiornamentoA" style="width: 125px; height: 23px">
											<span class="add-on" style="height: 23px"><i class="icon-th"></i></span>
										</div>
										<script type="text/javascript">
											$("#datepickerDataAggiornamentoA")
												.datepicker(
													{
														format: 'dd-mm-yyyy',
														language: 'it',
														viewMode : "days",
														minViewMode : "days",
														orientation : "bottom auto",
														autoclose : "true"
													});
										</script>
									</div>
								</td>
								<td class="fieldLabel medium" style="width: 16.66%"></td>
							</tr>
							<tr>
								<td class="fieldLabel medium" style="text-align: right; width: 16.66% nowrap" colspan="6">
									<button class="btn addButton" ng-click="filterApply(true)">
										<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Applica</strong>
									</button>
								</td>
							</tr>
						</tbody>
					</table>

				</div>
				<!-- end table inserimento campi -->

				<!-- start toolbar tabella | paginazione -->
				<div class="listViewTopMenuDiv noprint">
					<div class="listViewActionsDiv row-fluid">
						<span class="span6 btn-toolbar">
							<div class="listViewActions pull-left">
								<span style="padding-right: 5px">
									<button title="Ripeti Ingestion" class="btn" id="ingestionButton" type="button" ng-click="scheduleIngestion()">
										<span class="glyphicon glyphicon glyphicon-repeat" style="padding-right: 5px; font-weight: bold;"></span>&nbsp;Ripeti Ingestion
									</button>
								</span>
							</div>
						</span>
						<span class="span6 btn-toolbar">
							<div class="listViewActions pull-right">
								<div class="pageNumbers alignTop ">
									<span ng-show="ctrl.totRecords > 50">
										<span class="pageNumbersText" style="padding-right: 5px">
											Record da <strong>{{ctrl.page*50+1}}</strong> a <strong>{{(ctrl.page*50) + ctrl.sampling.length}}</strong>
											su
											<strong>{{totRecords}}</strong>
										</span>
										<button title="Precedente" class="btn" id="listViewPreviousPageButton" type="button" ng-click="navigateToPreviousPage()"
										 ng-show="ctrl.page > 0">
											<span class="glyphicon glyphicon-chevron-left"></span>
										</button>
										<button title="Successiva" class="btn" id="listViewNextPageButton" type="button" ng-click="navigateToNextPage()"
										 ng-show="ctrl.page < ctrl.pageTot">
											<span class="glyphicon glyphicon-chevron-right"></span>
										</button>
										<button title="Fine" class="btn" id="listViewNextPageButton" type="button" ng-click="navigateToEndPage()"
										 ng-show="ctrl.page < ctrl.pageTot">
											<span class="glyphicon glyphicon-step-forward"></span>
										</button>
									</span>
								</div>
							</div>
						</span>
					</div>
				</div>
				<!-- end toolbar tabella | paginazione -->

				<!-- start table risultati -->
				<div class="listViewContentDiv" id="listViewContents">

					<div class="listViewEntriesDiv contents-bottomscroll">
						<div class="bottomscroll-div" style="width: 95%; min-height: 80px">
							<table class="table table-bordered listViewEntriesTable tableFormatted">
								<thead>
									<tr class="listViewHeaders">
										<!-- PRIMA DI CAMBIARE LA CLASSE DELLO SPAN, CONTROLLARE LA LOGICA SUL CONTROLLER DI ANGULAR. -->
										<th>Data&nbsp;Ingestion&nbsp;SAP&nbsp;<span ng-click="orderImage('spanDataUltimoAggiornamento','dataAggiornamento')"
											 id="spanDataUltimoAggiornamento" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
										<th>Esito Ingestion&nbsp;<span ng-click="orderImage('spanEsitoIngestion','esitoIngestion')" id="spanEsitoIngestion"
											 style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
										<th>Tipo Errore&nbsp;<span ng-click="orderImage('spanTipoErrore','tipoErrore')" id="spanTipoErrore" style="font-size:80%"
											 class="glyphicon glyphicon-sort"></span></th>
										<th>Numero triplette OK&nbsp;<span ng-click="orderImage('spanNumeroTriplette','numTripletteOk')" id="spanNumeroTriplette"
											 style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
										<th>Numero triplette KO&nbsp;<span ng-click="orderImage('spanNumeroTripletteKO','numTripletteKo')"
											 id="spanNumeroTripletteKO" style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
										<th>Azioni&nbsp;<span style="font-size:80%"></span></th>
									</tr>
								</thead>
								<tbody>
									<tr class="listViewEntries" ng-repeat="item in ctrl.sampling">
										<td>{{item.dataAggiornamento | date: 'dd-MM-yyyy'}}</td>
										<td>{{item.esitoIngestion}}</td>
										<td>{{item.tipoErrore}}</td>
										<td>{{item.numTripletteOk}}</td>
										<td>{{item.numTripletteKo}}</td>
										<td><a ng-show="item.esitoIngestion=='LAVORATO'" ng-click="goToDettaglio(item.idIngestion)"><u>Vedi dettaglio</u></a></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- end table risultati -->
			</div>
		</div>
	</div>
</div>
<!--<pre>{{ctrl | json}}</pre>-->