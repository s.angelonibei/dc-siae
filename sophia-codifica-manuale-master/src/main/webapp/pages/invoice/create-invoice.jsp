<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../navbar.jsp"%>

<div class="bodyContents">

	<div class="mainContainer row-fluid">

		<div id="companyLogo" class="navbar commonActionsContainer noprint">
			<div class="actionsContainer row-fluid">
				<div class="span2">
					<span class="companyLogo"><img src="images/logo_siae.jpg"
						title="SIAE" alt="SIAE">&nbsp;</span>
				</div>
				<span class="btn-toolbar span4">
					<div class="pageNumbers alignTop pull-right"
						style="text-align: center;">
						<span class="pageNumbersText"><strong>Nuova
								Fattura</strong></span>
					</div>
				</span>
			</div>
		</div>

		<div class="contentsDiv marginLeftZero" id="rightPanel"
			style="min-height: 80px;">
			<div class="listViewPageDiv">
				<!-- contenitore principale -->
				<div ng-show="ctrl.hideEditing">
					<!-- actions and pagination -->
					<div class="listViewTopMenuDiv noprint">
						<div class="listViewActionsDiv row-fluid">

							<span class="btn-toolbar span4"> <span class="btn-group"></span>
							</span> <span class="btn-toolbar span4"> <span
								class="customFilterMainSpan btn-group"
								style="text-align: center;">
									<div class="pageNumbers alignTop ">
									</div>
							</span>
							</span> <span class="span4 btn-toolbar">
								<div class="listViewActions pull-right">
									<span class="btn-group"></span>
								</div>
								<div class="clearfix"></div>
							</span>

						</div>
					</div>



					<!-- actions and pagination -->
					<div class="listViewTopMenuDiv noprint">
						<div class="listViewActionsDiv row-fluid">
							<!--  -->
							<span class="btn-toolbar span4"> <span class="btn-group">

							</span>
							</span>
							<!--  -->
							<!--  -->
							<span class="btn-toolbar span4"> <span
								class="customFilterMainSpan btn-group"
								style="text-align: center;">
									<div class="pageNumbers alignTop ">
										<span class="pageNumbersText"><strong> <!-- Titolo -->
										</strong></span>
									</div>
							</span>
							</span>
							<!--  -->
							<!--  -->
							<span class="span4 btn-toolbar">
								<div class="listViewActions pull-right">

									<div class="pageNumbers alignTop ">
										<span ng-show="hasPrev() || hasNext()"> <span
											class="pageNumbersText" style="padding-right: 5px">
												Record da <strong>{{getFirst()}}</strong> a <strong>{{ctrl.last}}</strong>
										</span>
											<button title="Precedente" class="btn"
												id="listViewPreviousPageButton" type="button"
												ng-click="navigateToPreviousPage()" ng-show="hasPrev()">
												<span class="glyphicon glyphicon-chevron-left"></span>
											</button>
											<button title="Successiva" class="btn"
												id="listViewNextPageButton" type="button"
												ng-click="navigateToNextPage()" ng-show="hasNext()">
												<span class="glyphicon glyphicon-chevron-right"></span>
											</button>
										</span>
									</div>
								</div>
								<div class="clearfix"></div>
							</span>
							<!--  -->
						</div>
					</div>

					<!-- qui va il filtro-->
					<%--<filter-invoice on-filter-apply="apply(parameters)" anag-client-data="anagClientData" on-filter-dsp="filterDsp(value, values)"> </filter-invoice>--%>

					<filter-dsp-statistics
							anag-client-data="anagClientData"
							dsps-utilizations-commercial-offers-countries="dspsUtilizationsCommercialOffersCountries"
							filter-parameters="filterParameters"
							on-filter-apply="apply(parameters)"
							hide-back-claim-select="true"
							hide-client-select="false"></filter-dsp-statistics>

					<!-- table -->
					<div class="listViewContentDiv" id="listViewContents">

						<div class="contents-topscroll noprint">
							<div class="topscroll-div" style="width: 95%">&nbsp;</div>
						</div>

						<div class="listViewEntriesDiv contents-bottomscroll">
							<div class="bottomscroll-div"
								style="width: 95%; min-height: 80px">

								<table class="table table-bordered listViewEntriesTable">
									<thead>
										<tr class="listViewHeaders">
											<th><input type="checkbox"
												ng-model="allSelected"
												ng-click="selectedItem(allSelected)"
												ng-click="toggleItem(allSelected);toggleCCID()"/></th>
											<th><a href="" ng-click="sort('dspName')" class="listViewHeaderValues">DSP<span class="glyphicon glyphicon-sort{{sortBy==='dspName' ? (asc ? '-by-alphabet' : '-by-alphabet-alt') : '' }}"></span></a></th>
											<th><a href="" ng-click="sort('periodString')" class="listViewHeaderValues">Periodo<span class="glyphicon glyphicon-sort{{sortBy==='periodString' ? (asc ? '-by-alphabet' : '-by-alphabet-alt') : '' }}"></span></a></th>
											<th><a href="" ng-click="sort('country')" class="listViewHeaderValues">Territorio<span class="glyphicon glyphicon-sort{{sortBy==='country' ? (asc ? '-by-alphabet' : '-by-alphabet-alt') : '' }}"></span></a></th>
											<th><a href="" ng-click="sort('utilizationType')" class="listViewHeaderValues">Tipo di
													utilizzo<span class="glyphicon glyphicon-sort{{sortBy==='utilizationType' ? (asc ? '-by-alphabet' : '-by-alphabet-alt') : '' }}"></span></a></th>
											<th><a href="" ng-click="sort('commercialOffer')" class="listViewHeaderValues">Offerta
													commerciale<span class="glyphicon glyphicon-sort{{sortBy==='commercialOffer' ? (asc ? '-by-alphabet' : '-by-alphabet-alt') : '' }}"></span></a></th>
											<th style="text-align: right;"><a href="" ng-click="sort('totalValue')"
												class="listViewHeaderValues"> Valore (&euro;)<span class="glyphicon glyphicon-sort{{sortBy==='totalValue' ? (asc ? '-by-alphabet' : '-by-alphabet-alt') : '' }}"></span></a></th>
											<th style="text-align: right;"><a href="" ng-click="sort('invoiceAmount')"
												class="listViewHeaderValues"> Valore fatturabile
													(&euro;)<span class="glyphicon glyphicon-sort{{sortBy==='invoiceAmount' ? (asc ? '-by-alphabet' : '-by-alphabet-alt') : '' }}"></span></a></th>
											<th style="text-align: right;"><a href="" ng-click="sort('valoreResiduo')"
												class="listViewHeaderValues"> Valore fatturabile residuo
													(&euro;)<span class="glyphicon glyphicon-sort{{sortBy==='valoreResiduo' ? (asc ? '-by-alphabet' : '-by-alphabet-alt') : '' }}"></span></a></th>
                                            <th style="text-align: right;"><a href="" ng-click="sort('ccidParziale')"
                                            class="listViewHeaderValues"> CCID parzialmente abbinato
												<span class="glyphicon glyphicon-sort{{sortBy==='ccidParziale' ? (asc ? '-by-alphabet' : '-by-alphabet-alt') : '' }}"></span></a></th>
											<th></th>
										</tr>
									</thead>
									<tbody>

										<tr class="listViewEntries" ng-repeat="item in ctrl.data.rows">
											<td><input type="checkbox"
												ng-model="selectedCheckbox[item.idCCIDmetadata]"
												ng-click="selectedItem(item)" /></td>
											<td class="listViewEntryValue ">{{item.dspName}}</td>
											<td class="listViewEntryValue ">{{item.periodString}}</td>
											<td class="listViewEntryValue ">{{item.country}}</td>
											<td class="listViewEntryValue ">{{item.utilizationType}}</td>
											<td class="listViewEntryValue ">{{item.commercialOffer}}</td>
											<td class="listViewEntryValue " style="text-align: right;">{{item.totalValue | number:2}}</td>
											<td class="listViewEntryValue " style="text-align: right;">{{item.invoiceAmount | number:2}}</td>
											<td class="listViewEntryValue " style="text-align: right;">{{item.valoreResiduo | number:2}}</td>
											<td class="listViewEntryValue " style="text-align: right;">{{item.ccidParziale ? 'SI' : 'NO'}}</td>
											<td nowrap class="medium">
												<div class="actions pull-right">
													<!--  -->

													<!--  -->
												</div>
											</td>

										</tr>
									</tbody>
								</table>

							</div>
						</div>
					</div>
					<!-- /table -->

					<div style="margin-top: 15px">
						<table width="100%">
							<tr>
								<td align="right">
									<button ng-disabled="selectionIsEmpty()" class="btn addButton"
										ng-click="confirm()">
										<strong>Conferma</strong>
									</button>
								</td>
							</tr>
						</table>
					</div>

				</div>
				<!-- contenitore principale -->

				<!-- contenitore editing -->
				<div ng-hide="ctrl.hideEditing">
					<div style="margin-top: 15px">
						<div class="row-fluid">
							<div class="span6">


								<div class="span6" style="margin-top: 15px">
									<div class="control-group">
										<label class="control-label span2" for="inputDirezione"><strong>UFFICIO
												SIAE</strong></label>
										<div class="controls">
											<input type="text" class="span3" id="inputDirezione"
												value="{{ctrl.configuration.direzioneSede}}" readonly>
										</div>
									</div>
									<div class="span1"></div>
								</div>
								<div class="span6" style="margin-bottom: 15px">

									<div class="span4" ng-show="ctrl.hideInfoUfficio">
										<span ng-click="ctrl.hideInfoUfficio=false"
											class="glyphicon glyphicon-expand"></span>&nbsp;Dettaglio
										ufficio
									</div>
									<div class="span4" ng-hide="ctrl.hideInfoUfficio">
										<span ng-click="ctrl.hideInfoUfficio=true"
											class="glyphicon glyphicon-collapse-down"></span>&nbsp;Dettaglio
										ufficio
									</div>
								</div>
								<div ng-hide="ctrl.hideInfoUfficio" class="span6">
									<div class="span2">
										<div class="control-group">
											<label class="control-label span1" for="inputCanale">CANALE</label>
											<div class="controls">
												<input type="text" id="inputCanale" class="span1"
													value="{{ctrl.configuration.canale}}" readonly>
											</div>
										</div>

									</div>
									<div class="span3">
										<div class="control-group">
											<label class="control-label span1" for="inputCod">COD.</label>
											<div class="controls">
												<input type="text" id="inputCod" class="span1"
													value="{{ctrl.configuration.cod}}" readonly>
											</div>
											<div class="span1"></div>
										</div>

									</div>
									<div class="span3">
										<div class="control-group">
											<label class="control-label span1" for="inputTipo">TIPO
												DOC.</label>
											<div class="controls">
												<input type="text" id="inputTipo" class="span1"
													value="{{getDocType()}}" readonly>
											</div>
										</div>
										<div class="span1"></div>
									</div>
									<div class="span6">
										<div class="control-group">
											<label class="control-label span1" for="inputUfficio">UFFICIO</label>
											<div class="controls">
												<input type="text" class="span3" id="inputUfficio"
													value="{{ctrl.configuration.ufficio}}" readonly>
											</div>
										</div>
										<div class="span2"></div>
									</div>


								</div>
							</div>
							<div class="span6">
								<div class="span6">
									<div class="span6">
										<label class="control-label span2"><strong>CLIENTE
										</strong></label> <input type="text" class="span3" id="inputDirezione"
											value="{{ctrl.clientData.code}} - {{ctrl.selectedDsp.name | uppercase}}  "
											readonly>
									</div>
									<div class="span6" style="margin-top: 15px">

										<div class="span4" ng-show="ctrl.hideInfoCliente">
											<span ng-click="ctrl.hideInfoCliente=false"
												class="glyphicon glyphicon-expand"></span>&nbsp;Dettaglio
											cliente
										</div>
										<div class="span4" ng-hide="ctrl.hideInfoCliente">
											<span ng-click="ctrl.hideInfoCliente=true"
												class="glyphicon glyphicon-collapse-down"></span>&nbsp;Dettaglio
											cliente
										</div>
									</div>
									<div ng-hide="ctrl.hideInfoCliente">
										<div class="span6" style="margin-top: 15px">
											<div class="control-group">
												<label class="control-label span2" for="inputDenominazione">DENOMINAZIONE</label>
												<div class="controls">
													<input type="text" class="span3" id="inputDenominazione"
														value="{{ctrl.clientData.companyName}}" readonly>
												</div>
											</div>
											<div class="span1"></div>
										</div>
										<div class="span2">
											<div class="control-group">
												<label class="control-label span1" for="inputPaese">PAESE
												</label>
												<div class="controls">
													<input type="text" id="inputPaese" class="span1"
														value="{{ctrl.clientData.country}}" readonly>
												</div>
											</div>

										</div>
										<div class="span3">
											<div class="control-group">
												<label class="control-label span1" for="inputOpzione">OPZIONE
												</label>
												<div class="controls">
													<input type="text" id="inputOpzione" class="span2" value=""
														readonly>
												</div>
											</div>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div style="margin-top: 25px">
						<div class="container-fluid" style="padding-left: 0px;">
							<div class="row">
								<div class="span2">
									<label class="muted pull-left marginRight10px"> <span
										class="redColor" ng-show="form.process.$invalid">*</span>DATA
										DI COMPETENZA
									</label>
								</div>
								<div class="span3">
									<div class="input-append date" id="datepickerFrom"
										data-date-format="mm-yyyy">

										<input type="text" name="date" ng-model="dateOfpertinence"
											style="width: 173px" disabled>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div style="margin-top: 15px">
						<div class="container-fluid" style="padding-left: 0px;">
							<div class="row">
								<div class="span2">
									<label class="muted pull-left marginRight10px"> <span
										class="redColor" ng-show="form.process.$invalid">*</span>TIPO
										DI LICENZA
									</label>
								</div>
								<div class="span3">
									<select name="licence" id="licence" style="width: 210px"
										ng-model="selectedLicence" required>
										<option ng-repeat="item in ctrl.licenceList"
											value="{{item.code}}">({{item.dsp}}) - {{item.code}} - [dal
											{{item.from}} al {{item.to}}]</option>

									</select>

								</div>
							</div>
						</div>
					</div>
					<div style="margin-top: 25px; margin-bottom: 15px">
						<ul class="nav nav-tabs" id="inoviceTab">
							<li class="active"><a href="#dsr">DSR</a></li>
							<li><a href="#items">DATI CONTABILI</a></li>
						</ul>
						<div class="contents-topscroll noprint">
							<div class="topscroll-div" style="width: 95%">&nbsp;</div>
						</div>
						<div class="tab-content">
							<div class="tab-pane" id="items">
								<div>
									<!-- table -->
									<div class="listViewContentDiv" id="listViewContents">

										<div class="contents-topscroll noprint">
											<div class="topscroll-div" style="width: 95%">&nbsp;</div>
										</div>

										<div class="listViewEntriesDiv contents-bottomscroll">
											<div class="bottomscroll-div"
												style="width: 95%; min-height: 80px">

												<table class="table table-bordered listViewEntriesTable">
													<thead>
														<tr class="listViewHeaders">
															<th nowrap class="medium"><a href=""
																class="listViewHeaderValues">Descrizione</a></th>

															<th nowrap class="medium" style="text-align: right;"><a
																href="" class="listViewHeaderValues">Valore
																	fatturabile(&euro;)</a></th>

															<th nowrap class="medium" style="text-align: right;"><a
																href="" class="listViewHeaderValues">Valore
																	fatturabile residuo(&euro;)</a></th>
															<th></th>
														</tr>
													</thead>
													<tbody ui:sortable ng:model="ctrl.accountingData">

														<tr class="listViewEntries"
															ng-repeat="itemA in ctrl.accountingData"
															style="cursor: move;">
															<td nowrap class="medium" class="listViewEntryValue">{{itemA.description.label}}</td>
															<td nowrap class="medium" class="listViewEntryValue"
																style="text-align: right;">{{itemA.totFatturabile | number:2}}</td>
															<td nowrap class="medium" class="listViewEntryValue"
																style="text-align: right;">{{itemA.totFatturabileResiduo | number:2}}</td>
															<td nowrap class="medium">
																<div class="actions pull-right">
																	<!--  -->
																	<span class="actionImages"> <a
																		ng-click="showInfo(itemA)"><i title="Dettaglio"
																			class="glyphicon glyphicon-info-sign alignMiddle"></i></a>&nbsp;
																	</span>
																	<!--  -->
																</div>
															</td>
														</tr>
													</tbody>
												</table>

											</div>
										</div>
									</div>
									<!-- /table -->
								</div>
								<div style="width: 100%; margin-top: 15px">
									<h4>ANNOTAZIONI</h4>
									<textarea style="width: 60%" ng-model="annotation"></textarea>
								</div>

							</div>
							<div class="tab-pane active" id="dsr">
								<div>
									<!-- table -->
									<div class="listViewContentDiv" id="listViewContents">
										<div class="listViewEntriesDiv contents-bottomscroll">
											<div class="bottomscroll-div"
												style="width: 95%; min-height: 80px">
												<table class="table table-bordered listViewEntriesTable">
													<thead>
														<tr class="listViewHeaders">

															<th><input type="checkbox" ng-model="allSelectedDsp"
																ng-click="selectedItemDsp(allSelectedDsp)" /></th>
															<th nowrap class="medium"><a href=""
																class="listViewHeaderValues">Descrizione</a></th>
															<th><a href="" class="listViewHeaderValues">DSP</a></th>
															<th><a href="" class="listViewHeaderValues">Periodo</a></th>
															<th><a href="" class="listViewHeaderValues">Territorio</a></th>
															<th><a href="" class="listViewHeaderValues">Tipo
																	di utilizzo</a></th>
															<th><a href="" class="listViewHeaderValues">Offerta
																	commerciale</a></th>
															<th nowrap class="medium" style="text-align: right;"><a
																href="" class="listViewHeaderValues">Valore (&euro;)</a></th>

															<th style="text-align: right;"><a href=""
																class="listViewHeaderValues"> Valore fatturabile
																	(&euro;)</a></th>

															<th style="text-align: right;"><a href=""
																class="listViewHeaderValues"> Valore fatturabile
																	residuo(&euro;)</a></th>


															<th nowrap class="medium"></th>
														</tr>
													</thead>
													<tbody>

														<tr class="listViewEntries" ng-repeat="item in ctrl.items">
															<td nowrap class="medium"><input type="checkbox"
																ng-click="selectedItemDsp(item)"
																ng-model="selectedCheckboxDsp[item.idCCIDmetadata]" /></td>
															<td nowrap class="medium" class="listViewEntryValue ">{{item.description}}</td>
															<td class="listViewEntryValue ">{{item.dspName}}</td>
															<td class="listViewEntryValue ">{{item.periodString}}</td>
															<td class="listViewEntryValue ">{{item.country}}</td>
															<td class="listViewEntryValue ">{{item.utilizationType}}</td>
															<td class="listViewEntryValue ">{{item.commercialOffer}}</td>
															<td nowrap class="medium" class="listViewEntryValue "
																style="text-align: right;">{{item.totalValue | number:2}}</td>
															<td class="listViewEntryValue "
																style="text-align: right;">{{item.invoiceAmount | number:2}}</td>
															<td class="listViewEntryValue "
																style="text-align: right;">{{item.valoreResiduo | number:2}}</td>


															<td nowrap class="medium">
																<div class="actions pull-right">
																	<!--  
																	<span class="actionImages"> <a
																		ng-click="showInfo(item)"><i title="Dettaglio"
																			class="glyphicon glyphicon-info-sign alignMiddle"></i></a>&nbsp;
																	</span>
																	  -->
																</div>
															</td>

														</tr>
													</tbody>
												</table>
											</div>
											<!-- /table -->
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
					<script>
						$('#inoviceTab a').click(function(e) {
							e.preventDefault();
							$(this).tab('show');
						})
					</script>
					<div style="margin-top: 15px">
						<table width="100%">
							<tr>
								<td align="left">
									<button class="btn addButton" ng-show="ctrl.showCollapseButton"
										ng-click="collapse()">
										<strong>Raggruppa</strong>
									</button>
								</td>

								<td align="right">
									<button ng-disabled="accountingSelectionIsEmpty()"
										class="btn addButton" ng-click="saveDraft()">
										<strong>Salva Bozza</strong>
									</button>
								</td>
							</tr>
						</table>
					</div>
				</div>

			</div>
			<!-- contenitore editing -->

		</div>



	</div>
	<!-- -- >
	<pre>{{ctrl.properties}}</pre>
< !-- -->
</div>