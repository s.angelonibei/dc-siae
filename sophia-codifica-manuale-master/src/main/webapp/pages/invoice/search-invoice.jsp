<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@
	include file="../navbar.jsp"%>

<div class="bodyContents">

	<div class="mainContainer row-fluid">

		<div id="companyLogo" class="navbar commonActionsContainer noprint">
			<div class="actionsContainer row-fluid">
				<div class="span2">
					<!--  -- >			<span class="companyLogo"><img src="images/siae_logo_siaetrasparente.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
<!--  -->
					<span class="companyLogo"><img src="images/logo_siae.jpg"
						title="SIAE" alt="SIAE">&nbsp;</span>
				</div>
				<span class="btn-toolbar span4">
					<div class="pageNumbers alignTop pull-right"
						style="text-align: center;">
						<span class="pageNumbersText"><strong>Lista
								Fatture</strong></span>
					</div>
				</span>
			</div>
		</div>

		<div class="contentsDiv marginLeftZero" id="rightPanel"
			style="min-height: 80px;">
			<div class="listViewPageDiv">
				<!-- contenitore principale -->
				<div ng-show="ctrl.hideEditing">
					<!-- actions and pagination -->
					<div class="listViewTopMenuDiv noprint">
						<div class="listViewActionsDiv row-fluid">

							<span class="btn-toolbar span4"> <span class="btn-group">
									<!--  -- >
								<button class="btn addButton" ng-click="showMoveToNext($event,ctrl.unidentifiedSong)">
									<span class="glyphicon glyphicon-forward"></span>&nbsp;&nbsp;<strong>Successiva</strong>
								</button>
<!--  -->
							</span>
							</span> <span class="btn-toolbar span4"> <span
								class="customFilterMainSpan btn-group"
								style="text-align: center;">
									<div class="pageNumbers alignTop ">
										<!--  -- >
					 				<span class="pageNumbersText"><strong>Titolo</strong></span>
<!--  -->
									</div>
							</span>
							</span> <span class="span4 btn-toolbar">
								<div class="listViewActions pull-right">
									<span class="btn-group"> <!-- 		<button class="btn addButton" ng-click="showNewDSPConfig($event)">
										<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;<strong>Nuovo</strong>
									</button>  -->
									</span>
								</div>
								<div class="clearfix"></div>
							</span>

						</div>
					</div>



					<!-- actions and pagination -->
					<div class="listViewTopMenuDiv noprint">
						<div class="listViewActionsDiv row-fluid">
							<!--  -->
							<span class="btn-toolbar span4"> <span class="btn-group">

							</span>
							</span>
							<!--  -->
							<!--  -->
							<span class="btn-toolbar span4"> <span
								class="customFilterMainSpan btn-group"
								style="text-align: center;">
									<div class="pageNumbers alignTop ">
										<span class="pageNumbersText"><strong> <!-- Titolo -->
										</strong></span>
									</div>
							</span>
							</span>
							<!--  -->
							<!--  -->
							<span class="span4 btn-toolbar">
								<div class="listViewActions pull-right">

									<div class="pageNumbers alignTop ">
										<span ng-show="ctrl.values.hasPrev || ctrl.values.hasNext">
											<span class="pageNumbersText" style="padding-right: 5px">
												Record da <strong>{{ctrl.values.first+1}}</strong> a <strong>{{ctrl.values.last}}</strong>
										</span>
											<button title="Precedente" class="btn"
												id="listViewPreviousPageButton" type="button"
												ng-click="navigateToPreviousPage()"
												ng-show="ctrl.values.hasPrev">
												<span class="glyphicon glyphicon-chevron-left"></span>
											</button>
											<button title="Successiva" class="btn"
												id="listViewNextPageButton" type="button"
												ng-click="navigateToNextPage()"
												ng-show="ctrl.values.hasNext">
												<span class="glyphicon glyphicon-chevron-right"></span>
											</button>
										</span>
									</div>
								</div>
								<div class="clearfix"></div>
							</span>
							<!--  -->
						</div>
					</div>


					<!-- qui va il filtro-->
					<div ng-include src="'pages/filters/filter-invoice-search.html'"></div>


					<!-- table -->
					<div class="listViewContentDiv" id="listViewContents">

						<div class="contents-topscroll noprint">
							<div class="topscroll-div" style="width: 95%">&nbsp;</div>
						</div>

						<div class="listViewEntriesDiv contents-bottomscroll">
							<div class="bottomscroll-div"
								style="width: 95%; min-height: 80px">

								<table class="table table-bordered listViewEntriesTable">
									<thead>
										<tr class="listViewHeaders">
											<th><a href="" class="listViewHeaderValues"
												ng-click="ctrl.sortType='code'; ctrl.sortReverse=!ctrl.sortReverse">Numero
													Fattura <span
													ng-show="ctrl.sortType=='code' && !ctrl.sortReverse"
													class="glyphicon glyphicon-sort-by-attributes"></span> <span
													ng-show="ctrl.sortType=='code' && ctrl.sortReverse"
													class="glyphicon glyphicon-sort-by-attributes-alt"></span>
											</a></th>
											<th><a href="" class="listViewHeaderValues"
												ng-click="ctrl.sortType='dsp'; ctrl.sortReverse=!ctrl.sortReverse">Cliente
													<span ng-show="ctrl.sortType=='dsp' && !ctrl.sortReverse"
													class="glyphicon glyphicon-sort-by-attributes"></span> <span
													ng-show="ctrl.sortType=='dsp' && ctrl.sortReverse"
													class="glyphicon glyphicon-sort-by-attributes-alt"></span>
											</a></th>
											<th><a href="" class="listViewHeaderValues"
												ng-click="ctrl.sortType='creationTime'; ctrl.sortReverse=!ctrl.sortReverse">Data
													di creazione <span
													ng-show="ctrl.sortType=='creationTime' && !ctrl.sortReverse"
													class="glyphicon glyphicon-sort-by-attributes"></span> <span
													ng-show="ctrl.sortType=='creationTime' && ctrl.sortReverse"
													class="glyphicon glyphicon-sort-by-attributes-alt"></span>
											</a></th>
											<th><a href="" class="listViewHeaderValues" ng-click="ctrl.sortType='updateTime'; ctrl.sortReverse=!ctrl.sortReverse">Ultimo
													aggiornamento
													<span
													ng-show="ctrl.sortType=='updateTime' && !ctrl.sortReverse"
													class="glyphicon glyphicon-sort-by-attributes"></span> <span
													ng-show="ctrl.sortType=='updateTime' && ctrl.sortReverse"
													class="glyphicon glyphicon-sort-by-attributes-alt"></span>
													</a></th>
											<th><a href="" class="listViewHeaderValues"
												ng-click="ctrl.sortType='status'; ctrl.sortReverse=!ctrl.sortReverse">Stato
													<span
													ng-show="ctrl.sortType=='status' && !ctrl.sortReverse"
													class="glyphicon glyphicon-sort-by-attributes"></span> <span
													ng-show="ctrl.sortType=='status' && ctrl.sortReverse"
													class="glyphicon glyphicon-sort-by-attributes-alt"></span>
											</a></th>
											<th style="text-align: right;"><a href=""
												class="listViewHeaderValues"
												ng-click="ctrl.sortType='total'; ctrl.sortReverse=!ctrl.sortReverse">Totale
													(&euro;) <span
													ng-show="ctrl.sortType=='total' && !ctrl.sortReverse"
													class="glyphicon glyphicon-sort-by-attributes"></span> <span
													ng-show="ctrl.sortType=='total' && ctrl.sortReverse"
													class="glyphicon glyphicon-sort-by-attributes-alt"></span>
											</a></th>
											<th></th>
											<th></th>
										</tr>
									</thead>
									<tbody>

										<tr class="listViewEntries" data-toggle="popover"
											data-placement="top" title="{{item.note}}"
											data-trigger="hover"
											ng-repeat="item in ctrl.values.rows | orderBy:ctrl.sortType:ctrl.sortReverse">
											<td class="listViewEntryValue ">{{item.invoiceCode}}</td>
											<td class="listViewEntryValue ">{{item.clientData.companyName}}</td>
											<td class="listViewEntryValue ">{{
												formatDate(item.creationDate) }}</td>
											<td class="listViewEntryValue ">{{
												formatDate(item.lastUpdateDate) }}</td>
											<td class="listViewEntryValue ">{{
												getStatus(item.status) }}</td>
											<td class="listViewEntryValue " style="text-align: right;">{{item.total | number:2 }}</td>
											<td nowrap class="medium">
												<div class="actions pull-right">
													<!--  -->
													<span layout-align="center"> <a
														ng-click="showDetails(item)"><i title="Dettagli"
															class="glyphicon glyphicon-list-alt alignMiddle"></i></a>&nbsp;
														&nbsp; <span ng-if="item.status === 'BOZZA'"><a
															ng-click="showDeleteConfig(item)"><i title="Elimina"
																class="glyphicon glyphicon-trash alignMiddle"></i></a></span> <span
														ng-if="item.status === 'FATTURATO'"><a
															download="fattura_{{item.invoiceCode}}.pdf"
															ng-href="{{msInvoiceApiUrl}}invoice/download/{{item.idInvoice}}"><i
																title="Download"
																class="glyphicon glyphicon-download alignMiddle"></i></a></span>

													</span>
												
													<!--  -->
												</div>
											</td>
											<td nowrap class="medium">
												<div class="actions pull-right">
													<!--  -->
													
													<button ng-disabled="item.status === 'BOZZA'||item.status === 'RICHIESTA_FATTURAZIONE'" class="btn addButton" ng-click="doReturnInvoice(item)">
														&nbsp;&nbsp;<strong>Storna</strong>
													</button>
													<!--  -->
												</div>
											</td>

										</tr>
									</tbody>
								</table>

							</div>
						</div>
					</div>
					<!-- /table -->

				</div>
				<!-- contenitore principale -->


			</div>



		</div>
		<!-- -- >
	<pre>{{ctrl.properties}}</pre>
< !-- -->
	</div>
</div>