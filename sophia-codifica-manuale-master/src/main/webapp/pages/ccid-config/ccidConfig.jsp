<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@
	include file="../navbar.jsp" %>

<div class="bodyContents">

	<div class="mainContainer row-fluid">

		<div id="companyLogo" class="navbar commonActionsContainer noprint">
			<div class="actionsContainer row-fluid">
				<div class="span2">
					<span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
				</div>
				<span class="btn-toolbar span4">
					<div class="pageNumbers alignTop pull-right" style="text-align: center;">
						<span class="pageNumbersText"><strong>CONFIGURAZIONE CCID</strong></span>
					</div>
				</span>
			</div>
		</div>

		<div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px;">
			<div class="listViewPageDiv">
				<div class="alert alert-danger" ng-show="messages.error && (!messages.info)">
					<button type="button" class="close" ng-click="clearMessages()" aria-hidden="true">x</button>
					<span><strong>Errore: </strong>{{messages.error}}</span>
				</div>
				<div class="alert alert-success" ng-show="messages.info && (!messages.error)">
					<button type="button" class="close" ng-click="clearMessages()" aria-hidden="true">x</button>
					<span>{{messages.info}}</span>
				</div>
				<!-- actions and pagination -->
				<div class="listViewTopMenuDiv noprint">
					<div class="listViewActionsDiv row-fluid">

						<span class="btn-toolbar span4">
							<span class="btn-group">
							</span>
						</span>

						<span class="btn-toolbar span4">
							<span class="customFilterMainSpan btn-group" style="text-align: center;">
								<div class="pageNumbers alignTop ">
								</div>
							</span>
						</span>

						<span class="span4 btn-toolbar">
							<div class="listViewActions pull-right">
								<span class="btn-group">
									<button class="btn addButton" ng-click="showNewConfig()">
										<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;<strong>Nuovo</strong>
									</button>
								</span>
							</div>
							<div class="clearfix"></div>
						</span>

					</div>
				</div>

				<!-- actions and pagination -->
				<div class="listViewTopMenuDiv noprint">
					<div class="listViewActionsDiv row-fluid">
						<span class="btn-toolbar span4">
							<span class="btn-group">

							</span>
						</span>
						<span class="btn-toolbar span4">
							<span class="customFilterMainSpan btn-group" style="text-align: center;">
								<div class="pageNumbers alignTop ">
									<span class="pageNumbersText"><strong>
											<!-- Titolo --></strong></span>
								</div>
							</span>
						</span>
						<span class="span4 btn-toolbar">
							<div class="listViewActions pull-right">

								<div class="pageNumbers alignTop ">
									<span ng-show="ctrl.config.hasPrev || ctrl.config.hasNext">
										<span class="pageNumbersText" style="padding-right:5px">
											Record da <strong>{{ctrl.config.first+1}}</strong> a
											<strong>{{ctrl.config.last}}</strong>
										</span>
										<button title="Precedente" class="btn" id="listViewPreviousPageButton"
											type="button" ng-click="navigateToPreviousPage()"
											ng-show="ctrl.config.hasPrev">
											<span class="glyphicon glyphicon-chevron-left"></span>
										</button>
										<button title="Successiva" class="btn" id="listViewNextPageButton" type="button"
											ng-click="navigateToNextPage()" ng-show="ctrl.config.hasNext">
											<span class="glyphicon glyphicon-chevron-right"></span>
										</button>
									</span>
								</div>
							</div>
							<div class="clearfix"></div>
						</span>
					</div>
				</div>


				<!-- filtro -->
				<div ng-include src="'pages/filters/filter-dsp-utilization-offer-country.html'"></div>

				<!-- table -->
				<div class="listViewContentDiv" id="listViewContents">

					<div class="contents-topscroll noprint">
						<div class="topscroll-div" style="width: 95%">&nbsp;</div>
					</div>

					<div class="listViewEntriesDiv contents-bottomscroll">
						<div class="bottomscroll-div" style="width: 95%; min-height: 80px">

							<table class="table table-bordered listViewEntriesTable">
								<thead>
									<tr class="listViewHeaders">
										<th nowrap><a class="listViewHeaderValues">DSP</a></th>
										<th nowrap><a class="listViewHeaderValues">Tipo di utilizzo</a></th>
										<th nowrap><a class="listViewHeaderValues">Offerta commerciale</a></th>
										<th nowrap><a class="listViewHeaderValues">Territorio</a></th>
										<th nowrap><a class="listViewHeaderValues">Esclusione claim=0</a></th>
										<th nowrap><a class="listViewHeaderValues">Codificato</a></th>
										<th nowrap><a class="listViewHeaderValues">Codificato provvisorio</a>
										</th>
										<th nowrap><a class="listViewHeaderValues">Non codificato</a></th>
										<th nowrap><a class="listViewHeaderValues">Versione CCID</a></th>
										<th nowrap><a class="listViewHeaderValues">Parametri</a></th>
										<th nowrap><a class="listViewHeaderValues">Valuta</a></th>
										<th nowrap><a class="listViewHeaderValues">Rank</a></th>
										<th nowrap><a class="listViewHeaderValues">Formato naming CCID 14</a>
										</th>
										<th nowrap></th>
										<th nowrap></th>
										<th nowrap></th>
									</tr>
								</thead>
								<tbody>
									<tr class="listViewEntries"
										ng-repeat="item in ctrl.config.rows  | orderBy:ctrl.sortType:ctrl.sortReverse">
										<td class="listViewEntryValue medium" nowrap>{{item.dspName}}</td>
										<td class="listViewEntryValue medium" nowrap>{{item.utilizationName}}</td>
										<td class="listViewEntryValue medium" nowrap>{{item.commercialOfferName}}</td>
										<td class="listViewEntryValue medium" nowrap>{{item.countryName}}</td>
										<td class="listViewEntryValue medium" nowrap><span
												ng-show={{item.excludeClaimZero}} class="glyphicon glyphicon-ok"></span>
										</td>
										<td class="listViewEntryValue medium" nowrap><span ng-show={{item.encoded}}
												class="glyphicon glyphicon-ok"></span></td>
										<td class="listViewEntryValue medium" nowrap><span
												ng-show={{item.encodedProvisional}}
												class="glyphicon glyphicon-ok"></span></td>
										<td class="listViewEntryValue medium" nowrap><span ng-show={{item.notEncoded}}
												class="glyphicon glyphicon-ok"></span></td>
										<td class="listViewEntryValue medium" nowrap>{{item.ccdiVersionName}}</td>
										<td class="listViewEntryValue medium" nowrap>
											<div ng-repeat="(key, value) in item.ccidParams">
												{{key}}:{{value}}
											</div>
										</td>
										<td class="listViewEntryValue medium" nowrap>{{item.currency}}</td>
										<td class="listViewEntryValue medium" nowrap>{{item.rank}}</td>
										<td class="listViewEntryValue medium" nowrap>{{item.formatoNaming}}</td>
										<td nowrap class="medium">
											<div class="actions pull-right">
												<span class="actionImages">
													<a ng-click="showNewConfig(item)"><i
															title="Modifica Configurazione"
															class="glyphicon glyphicon-pencil alignMiddle"></i></a>&nbsp;
												</span>
											</div>
										</td>
										<td nowrap class="medium">
											<div ng-if="item.ccdiVersionName == 14" class="actions pull-right">
												<span class="actionImages">
													<a ng-click="showFileNamingConfig(item)"><i
															title="Configurazione nome CCID secondo standard"
															class="glyphicon glyphicon-edit alignMiddle"></i></a>&nbsp;
												</span>
											</div>
										</td>
										<td nowrap class="medium">
											<div class="actions pull-right">
												<span class="actionImages">
													<!--	<a ng-click="showInfoConfig($event,item)"><i title="Dettaglio" class="glyphicon glyphicon-info-sign alignMiddle"></i></a>&nbsp;-->
													<a ng-click="showDeleteConfig($event,item)"><i title="Elimina"
															class="glyphicon glyphicon-trash alignMiddle"></i></a>&nbsp;
												</span>
											</div>
										</td>
									</tr>
								</tbody>
							</table>

						</div>
					</div>
				</div>
				<!-- /table -->


			</div>
		</div>
	</div>
	<!-- -- >
	<pre>{{ctrl.properties}}</pre>
< !-- -->
</div>
