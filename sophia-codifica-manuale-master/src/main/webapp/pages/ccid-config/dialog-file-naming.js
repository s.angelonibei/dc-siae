/* 
 *
 * required in input:
 *   territoryCode
 *   userStartEndDate
 */
function FileNamingDialogController($scope, ngDialog) {
    const { id } = $scope.ngDialogData;
    console.log("---> ", $scope.ngDialogData);

    /*     $scope.onInit = function () {

        }

        $scope.onInit(); */

    $scope.save = function() {
        console.log($scope.ngDialogData);
        ngDialog.close(id, $scope.buildRequestObject());
    };

    $scope.cancel = function() {
        ngDialog.close(id);
    };

    $scope.elimina = function() {
        ngDialog.close(id, {eliminaFileNamingConfig: true});
    };

    $scope.buildRequestObject = function() {

        const { invoiceSender, invoiceReceiver, useType, licenseSD, typeOfClaim } = $scope.ngDialogData;
        const useStartEndDate = dialogForm.useStartEndDate.value;
        // object is compliant with api contract
        const fileNamingReq = {
            invoiceSender,
            invoiceReceiver,
            useType,
            useStartEndDate,
            licenseSD,
            typeOfClaim
        };
        // cleanup object from empty values( null undefined and empty string)
        return _.omitBy(fileNamingReq, (e) => _.isNil(e) || e === "");
    };

}
