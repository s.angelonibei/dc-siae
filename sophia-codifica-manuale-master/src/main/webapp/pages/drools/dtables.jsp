<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%@
	include file="../navbar.jsp" %>

<div class="bodyContents" >

	<div class="mainContainer row-fluid" >

		<div id="companyLogo" class="navbar commonActionsContainer noprint">
			<div class="actionsContainer row-fluid">
				<div class="span2">
<!--  -- >			<span class="companyLogo"><img src="images/siae_logo_siaetrasparente.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
-->			<span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
				</div>
				<span class="btn-toolbar span4">
					<div class="pageNumbers alignTop pull-right" style="text-align: center;">
		 				<span class="pageNumbersText"><strong>Tabelle Pricing</strong></span>
					</div>
				</span>
			</div>
		</div>

		<div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 180px;">
			<div class="listViewPageDiv">

				<div class="listViewTopMenuDiv noprint">
					<div class="listViewActionsDiv row-fluid">
					
						<span class="btn-toolbar span4">
							<span class="btn-group">
								<button class="btn addButton" ng-click="showUpload()">
									<span class="glyphicon glyphicon-cloud-upload"></span>&nbsp;&nbsp;<strong>Upload</strong>
								</button>
							 </span>
							 <span class="btn-group">
								<button class="btn addButton" ng-click="goToConfiguration()">
									<span class="glyphicon glyphicon-cloud-upload"></span>&nbsp;&nbsp;<strong>Configurazione Pricing</strong>
								</button>
 							</span>
						</span>

						<span class="btn-toolbar span4">
						</span>

						<span class="span5 btn-toolbar">
							<div class="listViewActions pull-right">
							
						 		<div class="pageNumbers alignTop ">
						 			<span ng-show="ctrl.searchResults.hasPrev || ctrl.searchResults.hasNext">
						 				<span class="pageNumbersText" style="padding-right:5px">
						 					Record da <strong>{{ctrl.searchResults.first+1}}</strong> a <strong>{{ctrl.searchResults.last}}</strong>
						 				</span>
					 					<button title="Precedente" class="btn" id="listViewPreviousPageButton" type="button"
					 							ng-click="navigateToPreviousPage()"
					 							ng-show="ctrl.searchResults.hasPrev">
					 						<span class="glyphicon glyphicon-chevron-left"></span>
					 					</button>
					 					<button title="Successiva" class="btn" id="listViewNextPageButton" type="button"
					 							ng-click="navigateToNextPage()"
					 							ng-show="ctrl.searchResults.hasNext">
					 						<span class="glyphicon glyphicon-chevron-right"></span>
					 					</button>
					 				</span>
						 		</div>
					 			
					 		</div>
					 		<div class="clearfix"></div>
					 	</span>
					</div>
				</div>

				<!-- table -->
				<div class="listViewContentDiv" id="listViewContents" >
<!--  -- >
					 <div class="contents-topscroll noprint">
					 	<div class="topscroll-div" style="width: 95%;">&nbsp;</div>
					 </div>
  -->
					 <div class="listViewEntriesDiv contents-bottomscroll">
					 	<div class="bottomscroll-div" style="width: 95%; min-height: 200px">
					 		<form name="form" ng-submit="onRicerca()">
								<table class="table table-bordered listViewEntriesTable">

								<thead>
								<tr class="listViewHeaders">
									<th nowrap><a ng-click="ctrl.sortType='idDsp'; ctrl.sortReverse=!ctrl.sortReverse" class="listViewHeaderValues" >DSP
										<span ng-show="ctrl.sortType=='idDsp' && !ctrl.sortReverse" class="glyphicon glyphicon-sort-by-alphabet"></span>
										<span ng-show="ctrl.sortType=='idDsp' && ctrl.sortReverse" class="glyphicon glyphicon-sort-by-alphabet-alt"></span>
									</a></th>
									<th nowrap><a ng-click="ctrl.sortType='idUtilizationType'; ctrl.sortReverse=!ctrl.sortReverse" class="listViewHeaderValues" >Tipo Utilizzazione
										<span ng-show="ctrl.sortType=='idUtilizationType' && !ctrl.sortReverse" class="glyphicon glyphicon-sort-by-alphabet"></span>
										<span ng-show="ctrl.sortType=='idUtilizationType' && ctrl.sortReverse" class="glyphicon glyphicon-sort-by-alphabet-alt"></span>
									</a></th>
									<th nowrap><a ng-click="ctrl.sortType='validFrom'; ctrl.sortReverse=!ctrl.sortReverse" class="listViewHeaderValues" >Data Inizio Validit&agrave;
										<span ng-show="ctrl.sortType=='validFrom' && !ctrl.sortReverse" class="glyphicon glyphicon-sort-by-attributes"></span>
										<span ng-show="ctrl.sortType=='validFrom' && ctrl.sortReverse" class="glyphicon glyphicon-sort-by-attributes-alt"></span>
									</a></th>
									<th nowrap><a ng-click="ctrl.sortType='validTo'; ctrl.sortReverse=!ctrl.sortReverse" class="listViewHeaderValues" >Data Fine Validit&agrave;
										<span ng-show="ctrl.sortType=='validTo' && !ctrl.sortReverse" class="glyphicon glyphicon-sort-by-attributes"></span>
										<span ng-show="ctrl.sortType=='validTo' && ctrl.sortReverse" class="glyphicon glyphicon-sort-by-attributes-alt"></span>
									</a></th>
									<th nowrap><span class="listViewHeaderValues" >File</span></th>
									<th nowrap></th>
								</tr>
								</thead>
								<tbody>

								<tr style="background-color: #e7e7e7 !important;">
									<td nowrap><div class="row-fluid">
											<select class="span10 listSearchContributor"
													ng-model="ctrl.idDsp" ng-change="onRicerca()">
												<option value="" >(Tutti)</option>
												<option value="{{key}}" ng-repeat="(key, value) in ctrl.decode.idDsp" >{{value}}</option>
											</select>
										</div>
									</td>
									<td nowrap><div class="row-fluid">
											<select class="span9 listSearchContributor"
													ng-model="ctrl.idUtilizationType" ng-change="onRicerca()">
												<option value="" >(Tutti)</option>
												<option value="{{key}}" ng-repeat="(key, value) in ctrl.decode.idUtilizationType" >{{value}}</option>
											</select>
										</div>
									</td>
									<td nowrap>
										<input type="text" class="span9 input-small ctrl_validFrom"
											style="width: 80px"
											ng-model="ctrl.validFrom" ng-change="onRicerca()">
										<script type="text/javascript">
											$(".ctrl_validFrom").datepicker({
												format: 'yyyy-mm-dd',
												language: 'it',
												autoclose: true,
												todayHighlight: true
											});
										</script>
										<span class="glyphicon glyphicon-erase" style="margin-top: 6px;"
											ng-click="ctrl.validFrom=null; onRicerca()"></span>
									</td>
									<td nowrap>
										<input type="text" class="span9 input-small ctrl_validTo"
											   style="width: 80px"
											   ng-model="ctrl.validTo" ng-change="onRicerca()">
										<script type="text/javascript">
											$(".ctrl_validTo").datepicker({
												format: 'yyyy-mm-dd',
												language: 'it',
												autoclose: true,
												todayHighlight: true
											});
										</script>
										<span class="glyphicon glyphicon-erase" style="margin-top: 6px;"
											  ng-click="ctrl.validTo=null; onRicerca()"></span>
									</td>
									<td nowrap>
										<input type="text" class="listSearchContributor input-medium "
												ng-model="ctrl.xlsS3Url">
									</td>
									<td nowrap>
										<div class="pull-right">
											<button class="btn" type="button" ng-click="onAzzera(); onRicerca()"><span class="glyphicon glyphicon-erase"></span></button>
											<button class="btn " type="submit"><span class="glyphicon glyphicon-search"></span></button>
										</div>
									</td>
								</tr>

								<tr class="listViewEntries" ng-repeat="item in ctrl.searchResults.rows | orderBy:ctrl.sortType:ctrl.sortReverse" >
									<td class="listViewEntryValue medium" nowrap>{{ctrl.decode.idDsp[item.idDsp]}}</td>
									<td class="listViewEntryValue medium" nowrap>{{ctrl.decode.idUtilizationType[item.idUtilizationType]}}</td>
									<td class="listViewEntryValue medium" nowrap>{{item.validFrom | date:'d MMMM yyyy'}}</td>
									<td class="listViewEntryValue medium" nowrap>{{item.validTo ? (item.validTo | date:'d MMMM yyyy') : 'In corso'}}</td>
									<td class="listViewEntryValue medium" nowrap>{{item.xlsS3Url.substr(1 + item.xlsS3Url.lastIndexOf('/'))}}</td>
									<td nowrap class="medium">
										<div class="actions pull-right">
										<span class="actionImages">
											<a download="{{item.xlsS3Url.substr(1 + item.xlsS3Url.lastIndexOf('/'))}}" ng-href="rest/droolsPricing/download/{{item.id}}"><i title="Download" class="glyphicon glyphicon-cloud-download alignMiddle"></i></a>&nbsp;
											<a ng-click="showEditData($event,item)"><i title="Modifica Date" class="glyphicon glyphicon-edit alignMiddle"></i></a>&nbsp;
											<a ng-click="showDelete($event,item)"><i title="Elimina" class="glyphicon glyphicon-trash alignMiddle"></i></a>&nbsp;
											<a ng-click="goToConfiguration(item)"><i title="Modifica Configurazione" class="glyphicon glyphicon-pencil alignMiddle"></i></a>&nbsp;
											<a ng-click="showDetails($event,item)"><i title="Dettagli" class="glyphicon glyphicon-list-alt alignMiddle"></i></a>&nbsp;
										</span>
										</div>
									</td>
								</tr>

								</tbody>
								</table>
							</form>
						</div>
					</div>
				</div>

				<div class="listViewTopMenuDiv noprint">
					<div class="listViewActionsDiv row-fluid">
					
						<span class="btn-toolbar span4">
						</span>

						<span class="btn-toolbar span4">
						</span>

						<span class="span5 btn-toolbar">
							<div class="listViewActions pull-right">
							
						 		<div class="pageNumbers alignTop ">
						 			<span ng-show="ctrl.searchResults.hasPrev || ctrl.searchResults.hasNext">
						 				<span class="pageNumbersText" style="padding-right:5px">
						 					Record da <strong>{{ctrl.searchResults.first+1}}</strong> a <strong>{{ctrl.searchResults.last}}</strong>
						 				</span>
					 					<button title="Precedente" class="btn" id="listViewPreviousPageButton" type="button"
					 							ng-click="navigateToPreviousPage()"
					 							ng-show="ctrl.searchResults.hasPrev">
					 						<span class="glyphicon glyphicon-chevron-left"></span>
					 					</button>
					 					<button title="Successiva" class="btn" id="listViewNextPageButton" type="button"
					 							ng-click="navigateToNextPage()"
					 							ng-show="ctrl.searchResults.hasNext">
					 						<span class="glyphicon glyphicon-chevron-right"></span>
					 					</button>
					 				</span>
						 		</div>
					 			
					 		</div>
					 		<div class="clearfix"></div>
					 	</span>
					</div>
				</div>

			</div>
		</div>
	</div>
<!--
	<pre>{{ctrl.transazioni}}</pre>
 -->
</div>