codmanApp.service('pricingService', ['$http', '$location', 'anagClientService',
    function ($http, $location, anagClientService) {
        let service = this;
        const baseUrl = "rest/droolsPricing";
        const getIdUtilizationType = "/idUtilizationType";
        const getIdDspUrl = '/idDsp';
        // pricing url
        const baseUrlRootingPricing = "api/ms-pricelist/pricelist";
        const baseUrlPricing = `${$location.protocol()}://${$location.host()}:${$location.port()}/${baseUrlRootingPricing}`;
        const importPricingUrl = 'importPriceList';
        const addPricingUrl = 'savePriceList';
        const updateDate ='updateDate';

        const DATE_FORMAT = 'YYYY-MM-DD';
        const SIAE_MINIMUM_ALBUM = "SIAE Minimum Album";
        const STRM_SUB = "STRM_SUB";
        const DWN_KEY = "DWN";
        /*  
            Cloud: "Cloud"
            DWN: "Download"
            MASTERLIST: "Master list"
            STRM_AD: "Streaming AD"
            STRM_GEN: "Streaming generico"
            STRM_SUB: "Streaming in abbonamento"
            YOUTUBE: "Streaming Youtube"
            YOU_AUDIO: "Youtube AudioTier"
        */
        const STREAMING_AD = buildConfiguration(["Rule Name", "Offerta Commerciale", "Territorio",
            "SIAE Pro Rata %", "Minimum Per Stream", "Minimum Currency"]);
        const STREAMING_IN_ABBONAMENTO_TERRITTORY = buildConfiguration(["Rule Name", "Offerta Commerciale", "Territorio",
            "SIAE Pro Rata %", "Minimo Abbonamento", "Minimum Currency"]);
        const STREAMING_IN_ABBONAMENTO_COUNTRY = buildConfiguration(["Rule Name", "Countries", "Offerta Commerciale",
            "SIAE Pro Rata %", "Minimo Abbonamento", "Minimum Currency"]);
        const STREAMING_GENERICO = buildConfiguration(["Rule Name", "Offerta Commerciale", "Territorio", "SIAE Pro Rata %", "Minimo Abbonamento", "Minimum Per Stream", "Minimum Currency"]);
        const STREAMING_YOUTUBE = buildConfiguration(["Rule Name", "Offerta Commerciale", "SIAE Pro Rata %", "Minimo Abbonamento", "Minimum Currency"]);
        const DOWNLOAD = buildConfiguration(["Rule Name", "Territorio", "SIAE Pro Rata %", "SIAE Minimum 1 Track", "SIAE Minimum Album", "Minimum Currency"]);
        const STREAMING_CLOUD = buildConfiguration(["Rule Name", "Offerta Commerciale", "Territorio", "SIAE Pro Rata %", "Minimo Abbonamento", "Minimum Currency"]);
        const MASTERLIST = buildConfiguration(["Rule Name", "Offerta Commerciale", "SIAE Pro Rata %", "Minimo Abbonamento", "Minimum Currency"]);
        const AUDIO_TIER = buildConfiguration(["Rule Name", "Offerta Commerciale", "Territorio", "SIAE Pro Rata %", "Minimum Per Stream", "Minimum Currency"]);

        service.configurationsTemplates = {
            Cloud: STREAMING_CLOUD,
            DWN: DOWNLOAD,
            MASTERLIST,
            STRM_AD: STREAMING_AD,
            STRM_GEN: STREAMING_GENERICO,
            STRM_SUB: STREAMING_IN_ABBONAMENTO_TERRITTORY,
            STRM_SUB_COUNTRY: STREAMING_IN_ABBONAMENTO_COUNTRY,
            YOUTUBE: STREAMING_YOUTUBE,
            YOU_AUDIO: AUDIO_TIER
        };

        service.getStrmSubKey = function () {
            return STRM_SUB;
        };

        service.getDwnKey = function () {
            return DWN_KEY;
        };

        service.getSiaeMinimumAlbum = function () {
            return SIAE_MINIMUM_ALBUM;
        };

        service.getDateFormat = function () {
            return DATE_FORMAT;
        };

        // utilizationSelected must be {key,value} of utilizationSelected
        // case STRM_SUB it must have checkbox value
        service.getConfigurationTemplate = function (utilizationSelected) {
            return new Promise((resolve) => {
                if (utilizationSelected) {
                    // take configuration based on utilizationSelected. i.g.  service.configurationsTemplates.DWN
                    const conf = (utilizationSelected.checkboxValue && utilizationSelected.checkboxValue === 'Countries') ?
                        "STRM_SUB_COUNTRY" :
                        Object.keys(service.configurationsTemplates).find(key => utilizationSelected.key === key);
                    const settings = service.configurationsTemplates[conf];
                    resolve({ ...settings, societaTutelaSelected: utilizationSelected.value });
                }
            });
        };

        service.getDspUtilizationsCommercialOffersCountries = function () {
            return new Promise((resolve, reject) => {
                if (service.dspUtilizationsCommercialOffersCountries) {
                    resolve(service.dspUtilizationsCommercialOffersCountries);
                } else {
                    anagClientService.getDspsUtilizationsCommercialOffersCountries()
                        .then((dspUtilizationsCommercialOffersCountries) => {
                            service.dspUtilizationsCommercialOffersCountries = dspUtilizationsCommercialOffersCountries;
                            resolve(dspUtilizationsCommercialOffersCountries);
                        }, () => {
                            service.dspUtilizationsCommercialOffersCountries = {};
                            reject({});
                        });
                }
            });
        }

        service.getIdUtilizationType = function () {
            return new Promise((resolve, reject) => {
                if (Object.keys(service.idUtilizationType || {}).length) {
                    resolve(service.idUtilizationType);
                } else { //load data from be
                    $http({
                        method: 'GET',
                        url: `${baseUrl}${getIdUtilizationType}`
                    }).then((response) => {
                        service.setIdUtilizationType(response.data);
                        resolve(service.idUtilizationType);
                    }, (error) => {
                        service.idUtilizationType = {};
                        reject(error);
                    });
                };
            });
        };

        service.setIdUtilizationType = function (idUtilizationType) {
            service.idUtilizationType = idUtilizationType;
        };

        service.getIdDsp = function () {
            return new Promise((resolve, reject) => {
                $http({
                    method: 'GET',
                    url: `${baseUrl}${getIdDspUrl}`
                }).then((response) => {
                    service.setIdDsp(response.data);
                    resolve(response.data);
                }, () => {
                    service.setIdDsp({});
                    reject({});
                });
            });
        };

        service.setIdDsp = function (idDspList) {
            service.idDsp = idDspList;
        };

        /* 
        * input preloadConfig: {
                id: 9
                idDsp: "googleplaypayasyougo"
                idUtilizationType: "DWN"
                validFrom: "2016-01-01T00:00:00.000Z" YYYY/MM/DD
                xlsS3Url: "s3://siae-dev-datalake/debug/rules/googleplaypayasyougo/DWN/20160101/GooglePlayDownload.xls"
            }
        *  */
        service.setPreloadConfig = function (preloadConfig) {
            service.preloadConfig = preloadConfig
        };

        service.getPreloadConfig = function () {
            return new Promise((resolve) => {
                resolve(service.preloadConfig);
            });
        };

        service.importPricing = function () {

            return new Promise((resolve, reject) => {
                if (service.preloadConfig) {
                    let params = _.omit(service.preloadConfig, ['id', 'xlsS3Url']);
                    if (service.preloadConfig.validFrom) {
                        const validFrom = moment(service.preloadConfig.validFrom).format(DATE_FORMAT);
                        const validTo = service.preloadConfig.validTo ? moment(service.preloadConfig.validTo).format(DATE_FORMAT) : "";
                        _.assignIn(params, { validFrom, validTo });
                    }
                    const p = _.omitBy(params, (e) => _.isNil(e) || e === "");
                    p.checkboxSelezionata = service.preloadConfig.checkboxSelezionata;
                    $http({
                        method: 'GET',
                        url: buildUrl(importPricingUrl),
                        // cleanup object from empty values( null undefined and empty string)
                        params: p
                    })
                        .then(({ data }) => {
                            resolve(data);
                        }, (error) => {
                            reject(error);
                        });
                } else {
                    resolve({});
                }
            });
        };

        service.addPricingConfiguration = function (body) {
            const { preloadConfig = {} } = service;
            const { nomeFileExcel = '' } = preloadConfig || {};
            const data = _.omitBy({ ...body, nomeFileExcel }, (e) => _.isNil(e) || e === "")
            return new Promise((resolve, reject) => {
                $http({
                    method: 'POST',
                    url: buildUrl(addPricingUrl),
                    data
                })
                    .then(data => {
                        resolve(data);
                    }, error => {
                        reject(error);
                    })
            });
        };

        service.updateConfigurationDates = function (idConfig, dates) {
            const { validFrom, validTo } = dates;
            return $http({
                method: 'PUT',
                url: `${baseUrl}/${updateDate}/${idConfig}`,
                params: {
                    validFrom,
                    validTo
                }
            });
        };

        
        function buildConfiguration(headerTitles) {

            const columnKeys = headerTitles.reduce((acc, v) => {
                const fixedKey = v.replace(/[^a-z]/gi, '');
                const camelFixedKey = _.camelCase(fixedKey);
                return {
                    ...acc,
                    // in order to have an array for siam minimum album
                    // [camelFixedKey]: camelFixedKey === camelSiaeMA ? [""] : ""
                    [camelFixedKey]: ""
                }
            }, []);

            return {
                headerTitles,
                columnKeys
            };
        };

        function buildUrl(url) {
            return `${baseUrlPricing}/${url}`;
        };
    }]);