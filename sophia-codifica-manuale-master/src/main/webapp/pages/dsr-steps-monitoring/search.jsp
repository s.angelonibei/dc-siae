<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%@
	include file="../navbar.jsp" %>

<div class="bodyContents" >

	<div class="mainContainer row-fluid" >
	
		<div id="companyLogo" class="navbar commonActionsContainer noprint">
			<div class="actionsContainer row-fluid">
				<div class="span2">
<!--  			<span class="companyLogo"><img src="images/siae_logo_siaetrasparente.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
  -->			<span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
				</div>
				<span class="btn-toolbar span4">
					<div class="pageNumbers alignTop pull-right" style="text-align: center;">
		 				<span class="pageNumbersText"><strong>DSR Monitoring</strong></span>
					</div>
				</span>				
			</div>
		</div>


		
		<div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 180px;">
			<div class="listViewPageDiv">
				<filter-dsp-statistics dsps-utilizations-commercial-offers-countries="dspsUtilizationsCommercialOffersCountries"
									   filter-parameters="filterParameters"
									   on-filter-apply="filterApply(parameters)"
									   hide-back-claim-select="true"
									   hide-client-select="true">
				</filter-dsp-statistics>
				

				<div class="listViewTopMenuDiv noprint">
					<div class="listViewActionsDiv row-fluid">
					
						<span class="btn-toolbar span3">
						</span>

						<span class="btn-toolbar span4">
						</span>

						<span class="span5 btn-toolbar" >
							<div class="listViewActions pull-right" style="vertical-align: bottom">
							
						 		<div class="pageNumbers alignTop ">
						 			<span ng-show="ctrl.searchResults.hasPrev || ctrl.searchResults.hasNext">
						 				<span class="pageNumbersText" style="padding-right:5px">
						 					Record da <strong>{{ctrl.searchResults.first+1}}</strong> a <strong>{{ctrl.searchResults.last}}</strong>
						 				</span>
					 					<button title="Precedente" class="btn" id="listViewPreviousPageButton" type="button"
												ng-click="navigateToPreviousPage()"
												ng-show="ctrl.searchResults.hasPrev">
					 						<span class="glyphicon glyphicon-chevron-left"></span>
					 					</button>
					 					<button title="Successiva" class="btn" id="listViewNextPageButton" type="button"
												ng-click="navigateToNextPage()"
												ng-show="ctrl.searchResults.hasNext">
					 						<span class="glyphicon glyphicon-chevron-right"></span>
					 					</button>
					 				</span>
						 		</div>
					 			
					 		</div>
					 		<div class="clearfix"></div>
					 	</span>
					</div>
				</div>

				<!-- table -->
				<div class="listViewContentDiv" id="listViewContents" >
					
					<div class="listViewEntriesDiv contents-bottomscroll">
						<div class="bottomscroll-div" style="width: 95%; min-height: 400px">

							<table class="table table-bordered listViewEntriesTable">

								<thead>
								<tr class="listViewHeaders">
									<th nowrap>DSR</th>

									<th nowrap>Data Inizio
										
									</th>

									<th nowrap>Extraction</th>
									<th nowrap>Normalization</th>
									<th nowrap>Identification</th>
									<th nowrap>Pricing and Claiming</th>
									<th nowrap>Unidentified</th>
									
									<th nowrap></th>
								</tr>
								</thead>
								<tbody>

								<tr style="background-color: #e7e7e7 !important;">
									<td nowrap><div class="row-fluid"><angucomplete-alt id="idDsr"
																						placeholder="Nome DSR"
																						pause="200"
																						selected-object="selectIdDsr"
																						remote-url="rest/dsr-steps-monitoring/searchIdDsrs?idDsr="
																						search-fields="name"
																						title-field="name"
																						minlength="1"
																						input-class=" listSearchContributor"/>
									</div>
									</td>

									<td>
										<input type="text" class="input-large ngdialog-input ctrl_insertTime"
											   style="width: 120px" ng-model="ctrl.insertTime"  />
										<script type="text/javascript">
											$(".ctrl_insertTime").datetimepicker({
												format: 'yyyy-mm-dd hh:ii',
												language: 'it',
												autoclose: true,
												todayHighlight: true
											});
										</script>
									</td>

									<td nowrap></td>
									<td nowrap></td>
									<td nowrap></td>
									<td nowrap></td>
									<td nowrap></td>
									

									<td nowrap>
										
									</td>
								</tr>

								<tr class="listViewEntries" ng-repeat="item in ctrl.searchResults.rows | orderBy:'extractQueued':ctrl.sortReverse" >
									<td class="listViewEntryValue medium" nowrap>{{item.idDsr}}</td>

									<td class="listViewEntryValue medium" nowrap>{{item.extractQueued | date:'d MMM y HH:mm:ss'}}</td>

									<td ng-show="null == decodeExtract(item)"></td>
									<td ng-show="'queued' == decodeExtract(item)"
										class="listViewEntryValue medium" nowrap>{{item.extractQueued | date:'d MMM y HH:mm:ss'}} <br/><strong>queued</strong></td>
									<td ng-show="'started' == decodeExtract(item)"
										class="listViewEntryValue medium" nowrap>{{item.extractStarted | date:'d MMM y HH:mm:ss'}} <br/><strong>started</strong></td>
									<td ng-show="'completed' == decodeExtract(item)"
										class="listViewEntryValue medium" nowrap>{{item.extractFinished | date:'d MMM y HH:mm:ss'}} <br/><strong>completed</strong>
										<img align="top" width="18" src="images/ok.png" /></td>
									<td ng-show="'failed' == decodeExtract(item)"
										class="listViewEntryValue medium" nowrap>{{item.extractFinished | date:'d MMM y HH:mm:ss'}} <br/><strong>failed</strong>
										<img align="top" width="18" src="images/ko.png" /></td>

									<td ng-show="null == decodeClean(item)"></td>
									<td ng-show="'queued' == decodeClean(item)"
										class="listViewEntryValue medium" nowrap>{{item.cleanQueued | date:'d MMM y HH:mm:ss'}} <br/><strong>queued</strong></td>
									<td ng-show="'started' == decodeClean(item)"
										class="listViewEntryValue medium" nowrap>{{item.cleanStarted | date:'d MMM y HH:mm:ss'}} <br/><strong>started</strong></td>
									<td ng-show="'completed' == decodeClean(item)"
										class="listViewEntryValue medium" nowrap>{{item.cleanFinished | date:'d MMM y HH:mm:ss'}} <br/><strong>completed</strong>
										<img align="top" width="18" src="images/ok.png" /></td>
									<td ng-show="'failed' == decodeClean(item)"
										class="listViewEntryValue medium" nowrap>{{item.cleanFinished | date:'d MMM y HH:mm:ss'}} <br/><strong>failed</strong>
										<img align="top" width="18" src="images/ko.png" /></td>

								
									<td ng-show="null == decodeIdentify(item)"></td>
									<td ng-show="'queued' == decodeIdentify(item)"
										class="listViewEntryValue medium" nowrap>{{item.identifyQueued | date:'d MMM y HH:mm:ss'}} <br/><strong>queued</strong></td>
									<td ng-show="'started' == decodeIdentify(item)"
										class="listViewEntryValue medium" nowrap>{{item.identifyStarted | date:'d MMM y HH:mm:ss'}} <br/><strong>started</strong></td>
									<td ng-show="'completed' == decodeIdentify(item)"
										class="listViewEntryValue medium" nowrap>{{item.identifyFinished | date:'d MMM y HH:mm:ss'}} <br/><strong>completed</strong>
										<img align="top" width="18" src="images/ok.png" /></td>
									<td ng-show="'failed' == decodeIdentify(item)"
										class="listViewEntryValue medium" nowrap>{{item.identifyFinished | date:'d MMM y HH:mm:ss'}} <br/><strong>failed</strong>
										<img align="top" width="18" src="images/ko.png" /></td>

									

									<td ng-show="null == decodeClaim(item)"></td>
									<td ng-show="'queued' == decodeClaim(item)"
										class="listViewEntryValue medium" nowrap>{{item.claimQueued | date:'d MMM y HH:mm:ss'}} <br/><strong>queued</strong></td>
									<td ng-show="'started' == decodeClaim(item)"
										class="listViewEntryValue medium" nowrap>{{item.claimStarted | date:'d MMM y HH:mm:ss'}} <br/><strong>started</strong></td>
									<td ng-show="'completed' == decodeClaim(item)"
										class="listViewEntryValue medium" nowrap>{{item.claimFinished | date:'d MMM y HH:mm:ss'}} <br/><strong>completed</strong>
										<img align="top" width="18" src="images/ok.png" /></td>
									<td ng-show="'failed' == decodeClaim(item)"
										class="listViewEntryValue medium" nowrap>{{item.claimFinished | date:'d MMM y HH:mm:ss'}} <br/><strong>failed</strong>
										<img align="top" width="18" src="images/ko.png" /></td>


									<td ng-show="null == decodeUniload(item)"></td>
									<td ng-show="'queued' == decodeUniload(item)"
										class="listViewEntryValue medium" nowrap>{{item.uniloadQueued | date:'d MMM y HH:mm:ss'}} <br/><strong>queued</strong></td>
									<td ng-show="'started' == decodeUniload(item)"
										class="listViewEntryValue medium" nowrap>{{item.uniloadStarted | date:'d MMM y HH:mm:ss'}} <br/><strong>started</strong></td>
									<td ng-show="'completed' == decodeUniload(item)"
										class="listViewEntryValue medium" nowrap>{{item.uniloadFinished | date:'d MMM y HH:mm:ss'}} <br/><strong>completed</strong>
										<img align="top" width="18" src="images/ok.png" /></td>
									<td ng-show="'failed' == decodeUniload(item)"
										class="listViewEntryValue medium" nowrap>{{item.uniloadFinished | date:'d MMM y HH:mm:ss'}} <br/><strong>failed</strong>
										<img align="top" width="18" src="images/ko.png" /></td>

									
									<td nowrap class="medium">
										<div class="actions pull-right">
					 				<span class="actionImages">
					 					<a ng-click="showSearch($event,item)"><i title="Mostra Messaggi" class="glyphicon glyphicon-list alignMiddle"></i></a>&nbsp;
					 				</span>
										</div>
									</td>
								</tr>

								</tbody>
							</table>

						</div>
					</div>
				</div>

				<div class="listViewTopMenuDiv noprint">
					<div class="listViewActionsDiv row-fluid">
					
						<span class="btn-toolbar span3">
						</span>

						<span class="btn-toolbar span4">
						</span>

						<span class="span5 btn-toolbar">
							<div class="listViewActions pull-right">
							
						 		<div class="pageNumbers alignTop ">
						 			<span ng-show="ctrl.searchResults.hasPrev || ctrl.searchResults.hasNext">
						 				<span class="pageNumbersText" style="padding-right:5px">
						 					Record da <strong>{{ctrl.searchResults.first+1}}</strong> a <strong>{{ctrl.searchResults.last}}</strong>
						 				</span>
					 					<button title="Precedente" class="btn" id="listViewPreviousPageButton" type="button"
												ng-click="navigateToPreviousPage()"
												ng-show="ctrl.searchResults.hasPrev">
					 						<span class="glyphicon glyphicon-chevron-left"></span>
					 					</button>
					 					<button title="Successiva" class="btn" id="listViewNextPageButton" type="button"
												ng-click="navigateToNextPage()"
												ng-show="ctrl.searchResults.hasNext">
					 						<span class="glyphicon glyphicon-chevron-right"></span>
					 					</button>
					 				</span>
						 		</div>
					 			
					 		</div>
					 		<div class="clearfix"></div>
					 	</span>
					</div>
				</div>

			</div>
		</div>
										
		

		
		
		
	</div>
<!-- 
	<pre>{{ctrl.transazioni}}</pre>
 -->
</div>