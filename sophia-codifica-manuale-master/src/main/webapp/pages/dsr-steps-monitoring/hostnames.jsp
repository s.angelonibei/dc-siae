<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%@
	include file="../navbar.jsp" %>

<div class="bodyContents" >

	<div class="mainContainer row-fluid" >
	
		<div id="companyLogo" class="navbar commonActionsContainer noprint">
			<div class="actionsContainer row-fluid">
				<div class="span2">
<!--  -- >			<span class="companyLogo"><img src="images/siae_logo_siaetrasparente.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
<!--  -->			<span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
				</div>
				<span class="btn-toolbar span4">
					<div class="pageNumbers alignTop pull-right" style="text-align: center;">
		 				<span class="pageNumbersText"><strong>EC2 Hostnames / EMR Cluster IDs</strong></span>
					</div>
				</span>				
			</div>
		</div>
	
		<div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 180px;"> 
			<div class="listViewPageDiv">

				<div class="listViewTopMenuDiv noprint">
					<div class="listViewActionsDiv row-fluid">
					
						<span class="btn-toolbar span3">
						</span>

						<span class="btn-toolbar span4">
						</span>
												 						
						<span class="btn-toolbar span5" >
					 	</span>
					</div>
				</div>

				<!-- table -->
				<div class="listViewContentDiv" id="listViewContents" >
<!--  -- >
					 <div class="contents-topscroll noprint">
					 	<div class="topscroll-div" style="width: 95%;">&nbsp;</div>
					 </div>
<!--  -->
					 <div class="listViewEntriesDiv contents-bottomscroll">
					 	<div class="bottomscroll-div" style="width: 95%; min-height: 400px">

					 		<table class="table table-bordered listViewEntriesTable">

					 		<thead>
					 		<tr class="listViewHeaders">
					 			<th nowrap><a ng-click="setSortType('hostname');" class="listViewHeaderValues" >Host/Cluster
						 			<span ng-show="ctrl.sortType=='hostname' && !ctrl.sortReverse" class="glyphicon glyphicon-sort-by-alphabet"></span>
	            					<span ng-show="ctrl.sortType=='hostname' && ctrl.sortReverse" class="glyphicon glyphicon-sort-by-alphabet-alt"></span>
            					</a></th>
					 			<th nowrap><a ng-click="setSortType('insertTime');" class="listViewHeaderValues" >Data Ora
						 			<span ng-show="ctrl.sortType=='insertTime' && !ctrl.sortReverse" class="glyphicon glyphicon-sort-by-attributes"></span>
	            					<span ng-show="ctrl.sortType=='insertTime' && ctrl.sortReverse" class="glyphicon glyphicon-sort-by-attributes-alt"></span>
            					</a></th>
						 		<th nowrap>Elapsed</th>
						 		<th nowrap>Tipo Coda</th>
						 		<th nowrap>Nome Modulo</th>
						 		<th nowrap>DSR</th>
					 			<th nowrap></th>
					 		</tr>
					 		</thead>
					 		<tbody>

					 		<tr class="listViewEntries" ng-repeat="item in ctrl.hostnames | orderBy:ctrl.sortType:ctrl.sortReverse" >
					 			<td class="listViewEntryValue medium" nowrap>{{item.hostname}}</td>
					 			<td class="listViewEntryValue medium" nowrap>{{item.insertTime | date:'d MMM y HH:mm:ss'}}</td>
					 			<td class="listViewEntryValue medium" nowrap>{{elapsedTime(item.insertTime)}}</td>
					 			<td class="listViewEntryValue medium" nowrap>{{item.queueType}}</td>
					 			<td class="listViewEntryValue medium" nowrap>{{item.serviceName}}</td>
					 			<td class="listViewEntryValue medium" nowrap>{{item.idDsr}}</td>

					 			<td nowrap class="medium">
					 				<div class="actions pull-right">
					 				<span class="actionImages">
					 					<a ng-show="'started'===item.queueType" ng-click="showKillEmrStep(item.hostname,item.applicationId)"><i title="Kill EMR Step" class="glyphicon glyphicon-thumbs-down alignMiddle"></i></a>&nbsp;
					 					<a ng-click="showSearch($event,item)"><i title="Mostra Messaggi" class="glyphicon glyphicon-list alignMiddle"></i></a>&nbsp;
					 				</span>
					 				</div>
					 			</td>
					 		</tr>

					 		</tbody>
					 		</table>
					 		
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
<!-- 
	<pre>{{ctrl.transazioni}}</pre>
 -->
</div>