<!doctype html>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html lang="it">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="application-name" content="Codifica Manuale ver.1.0">
    <title>Codifica Manuale</title>
    <link rel="shortcut icon" href="images/favicon_0.ico">

    <!-- jquery -->
    <link type="text/css" href="jquery/jquery-ui.min.css" rel="stylesheet" media="screen">
    <link type="text/css" href="jquery/jquery-ui.structure.min.css" rel="stylesheet" media="screen">
    <link type="text/css" href="jquery/jquery-ui.theme.min.css" rel="stylesheet" media="screen">
    <link type="text/css" href="jquery/jquery.datetimepicker.css" rel="stylesheet" media="screen">
    <!-- /jquery -->

    <!-- bootstrap -->
    <link type="text/css" href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <!-- WARNING: old version do not change! -->
    <link type="text/css" href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
    <link type="text/css" href="bootstrap/css/bootstrap-datepicker.min.css" rel="stylesheet" media="screen">
    <link type="text/css" href="bootstrap/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
    <!-- /bootstrap -->


    <!-- angular material -->
    <!--     <link rel="stylesheet" href="bower_components/angular-material/angular-material.min.css" /> -->
    <link rel="stylesheet" href="dep/angular-material.min.css">

    <!-- /angular material -->

    <link rel="stylesheet" href="https://unpkg.com/tableexport@5.2.0/dist/css/tableexport.min.css" />

    <!-- angucomplete-alt -->
    <link rel="stylesheet" href="css/angucomplete-alt.css" type="text/css">

    <!-- ngDialog -->
    <link rel="stylesheet" href="css/ngDialog.min.css" type="text/css">
    <link rel="stylesheet" href="css/ngDialog-theme-default.css" type="text/css">
    <!-- /ngDialog -->

    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
    <!-- WARNING: contains glyphicons! -->
    <link rel="stylesheet" href="css/toggle-switch-round.css" type="text/css">
    <link rel="stylesheet" href="css/multiselect.css" type="text/css">
    <style type="text/css">
        @media print {
            .noprint {
                display: none;
            }
        }
    </style>
    <link rel="stylesheet" href="css/statistiche-post-ripartizione.css" type="text/css">
    <link rel="stylesheet" href="css/rendiconto.css" type="text/css">
    <link rel="stylesheet" href="pages/ripartizione/lookThrough/lookThrough.css" type="text/css"></link>
    <link rel="stylesheet" href="pages/ricerca-codificato/ricerca-codificato.css" type="text/css"></link>
    <link rel="stylesheet" href="pages/ccid-config/dialog-file-naming.css" type="text/css">
    <link rel="stylesheet" href="pages/ripartizione/produzione-carichi.css" type="text/css">
    <link rel="stylesheet" href="pages/blacklist/configurazione/configurazione-blacklist.css" type="text/css">
    <link rel="stylesheet" href="pages/configurazione-pricing/configurazione-pricing.css" type="text/css">
    <link rel="stylesheet" href="pages/dashboard-dsp/dashboard-dsp.css" type="text/css">
    <link rel="stylesheet" href="pages/configurazione-pricing/templates/templates.css" type="text/css">
    <link rel="stylesheet" href="pages/ingestion/codificato/codificato.css" type="text/css">
    <link rel="stylesheet" href="pages/configurazione-caretteri/configurazione-caratteri.css" type="text/css">
    <link rel="stylesheet" href="pages/riconoscimento/scodifica-ricodifica/scodifica-ricodifica.css" type="text/css">
    <link rel="stylesheet" href="pages/gestione-conflitti/gestione-conflitti.css" type="text/css"></link>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
        integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body>
    <div id="page">

        <span us-spinner="{radius:30, width:8, length: 16}"
            style=" position: fixed;top: 50%;left: 50%;z-index: 999999;"></span>

        <div ng-view></div>

    </div>

    <table width="100%">
        <tr>
            <td width="98%"><i>Cruscotto Utilizzazioni ver-1.9.0</i></td>
            <td width="2%">&nbsp;</td>
        </tr>
    </table>
    <script src="https://unpkg.com/dayjs"></script>
    <script src="https://unpkg.com/dayjs/plugin/utc"></script>
    <script>
        dayjs.extend(dayjs_plugin_utc)
    </script>
    <script src="https://unpkg.com/dayjs/plugin/customParseFormat"></script>
    <script>
        dayjs.extend(dayjs_plugin_customParseFormat)
    </script>
    <script type="text/javascript" src="jquery/jquery.js"></script>
    <script type="text/javascript" src="jquery/jquery-ui.min.js"></script>
    <script type="text/javascript" src="jquery/jquery.datetimepicker.min.js"></script>
    <script type="text/javascript" src="jquery/lodash.min.js"></script>

    <script type="text/javascript" src="bootstrap/js/bootstrap-3.3.6.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/locales/bootstrap-datepicker.it.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/locales/bootstrap-datetimepicker.it.js"></script>

    <!-- utilities -->
    <script type="text/javascript" src="js/codman-utilities.js"></script>
    <!-- /utilities -->

    <!-- angular.js 1.6.7 -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.7/angular.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.7/angular-animate.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.7/angular-messages.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.7/angular-sanitize.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.7/angular-aria.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/2.5.0/ui-bootstrap-tpls.min.js"></script>

    <!-- /angular.js 1.6.7 -->
    <!--1.6.8 -->
    <!-- <script type="text/javascript" src="bower_components/angular-route/angular-route.min.js"></script> -->
    <!-- <script type="text/javascript" src="bower_components/angular-touch/angular-touch.min.js"></script> -->
    <script type="text/javascript"
        src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.8/angular-route.min.js"></script>
    <script type="text/javascript"
        src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.8/angular-touch.min.js"></script>
    <!-- /1.6.8 -->
    <script type="text/javascript" src="js/angular-locale_it-it.min.js"></script>
    <script type="text/javascript" src="js/angular-ui_0.4.0.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/it.js"></script>
    <script src="https://unpkg.com/angular-date-time-input@1.2.1/src/dateTimeInput.js"></script>

    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/angular-bootstrap-datetimepicker/1.1.4/js/datetimepicker.min.js"></script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/angular-bootstrap-datetimepicker/1.1.4/js/datetimepicker.templates.min.js"></script>

    <!-- <script type="text/javascript" src="bower_components/angular-material/angular-material.js"></scr -->
    <script src="dep/angular-material.js"></script>

    <script type="text/javascript" src="js/angucomplete-alt.js"></script>
    <!-- /angucomplete-alt -->
    <!-- gauge -->
    <script src="dep/angularjs-gauge.min.js"></script>
    <!-- gauge -->

    <!-- angular-loading-spinner -->
    <script type="text/javascript" src="js/spin.min.js"></script>
    <script type="text/javascript" src="js/angular-spinner.min.js"></script>
    <script type="text/javascript" src="js/angular-loading-spinner.js"></script>
    <!-- /angular-loading-spinner -->

    <!-- ng-file-upload -->
    <script type="text/javascript" src="js/ng-file-upload-shim.js"></script>
    <script type="text/javascript" src="js/ng-file-upload.min.js"></script>
    <!-- /ng-file-upload -->

    <script type="text/javascript" src="js/ngDialog.min.js"></script>

    <!-- librerie per l'export in Excel -->
    <script type="text/javascript" src="js/alsql/alasql.min.js"></script>
    <script type="text/javascript" src="js/js-xlsx/xlsx.core.min.js"></script>
    <script type="text/javascript" src="js/fileSaver/FileSaver.js"></script>
    <script type="text/javascript" src="js/fileSaver/FileSaver.min.js"></script>

    <script type="text/javascript" src="https://unpkg.com/tableexport@5.2.0/dist/js/tableexport.min.js"></script>
    <script type="text/javascript" src="js/moment/min/moment-with-locales.min.js"></script>

    <script type="text/javascript" src="js/app.js"></script>
    <script type="text/javascript" src="js/util/alert-service/util-service.js"></script>
    <script type="text/javascript" src="js/app-controller-home.js"></script>
    <script type="text/javascript" src="js/app-controller-riconoscimento.js"></script>
    <script type="text/javascript" src="js/riconoscimento/service/riconoscimentoService.js"></script>
    <script type="text/javascript" src="js/app-controller-knowledgebase.js"></script>
    <script type="text/javascript" src="js/app-controller-dsrprocessconfig.js"></script>
    <script type="text/javascript" src="js/app-controller-dspconfig.js"></script>
    <script type="text/javascript" src="js/app-controller-mandato-config.js"></script>
    <script type="text/javascript" src="js/app-controller-documentazione-mandanti.js"></script>
    <script type="text/javascript" src="js/app-controller-conf-aggi-trattenuta.js"></script>
    <script type="text/javascript" src="js/app-controller-produzione-carichi.js"></script>
    <script type="text/javascript" src="js/app-controller-lista-carichi.js"></script>
    <script type="text/javascript" src="js/app-controller-statistiche-post-ripartizione.js"></script>
    <script type="text/javascript" src="js/app-controller-ricerca.js"></script>
    <script type="text/javascript" src="js/app-controller-commercialoffers-config.js"></script>
    <script type="text/javascript" src="js/filters/app-controller-filter-dsp-utilizzazioni-offerte.js"></script>
    <script type="text/javascript" src="js/filters/app-controller-filter-dsp-country-utilization.js"></script>
    <script type="text/javascript" src="js/app-controller-processmanagement.js"></script>
    <script type="text/javascript" src="js/app-controller-anag-dsp.js"></script>
    <script type="text/javascript" src="js/app-controller-dsr-metadata.js"></script>
    <script type="text/javascript" src="js/app-controller-ccid-config.js"></script>
    <script type="text/javascript" src="pages/ccid-config/dialog-file-naming.js"></script>
    <script type="text/javascript" src="js/filters/app-controller-filter-dsp-utilization-offer-country.js"></script>
    <script type="text/javascript" src="js/app-controller-drools.js"></script>
    <script type="text/javascript" src="js/app-controller-blacklist.js"></script>
    <!-- angularjs-dropdown-multiselect lib -->
    <script type="text/javascript" src="js/filters/dropdownMultiselectModule.js"></script>

    <script type="text/javascript" src="js/filters/dateTimePickerPopUpController.js"></script>
    <script type="text/javascript" src="js/app-controller-sqs-failed-stats.js"></script>
    <script type="text/javascript" src="js/app-controller-sqs-failed-search.js"></script>
    <script type="text/javascript" src="js/app-controller-service-bus-stats.js"></script>
    <script type="text/javascript" src="js/app-controller-service-bus-search.js"></script>
    <script type="text/javascript" src="js/app-controller-create-invoice.js"></script>
    <%--<script type="text/javascript" src="js/filters/app-controller-filter-invoice.js"></script>--%>
    <script type="text/javascript" src="js/multimediale/fatturazione/component/filter-invoice.component.js"></script>
    <script type="text/javascript" src="js/app-controller-search-invoice.js"></script>
    <script type="text/javascript" src="js/filters/app-controller-filter-invoice-search.js"></script>
    <script type="text/javascript" src="js/app-controller-dsr-steps-monitoring.js"></script>
    <script type="text/javascript" src="js/app-controller-dsr-progress.js"></script>
    <script type="text/javascript" src="js/app-controller-dsr-claim.js"></script>
    <script type="text/javascript" src="js/filters/app-controller-filter-claim-config.js"></script>
    <script type="text/javascript" src="js/app-controller-sqs-hostnames.js"></script>
    <script type="text/javascript" src="js/app-controller-anag-client.js"></script>
    <script type="text/javascript" src="js/app-controller-deliver-ccid.js"></script>
    <script type="text/javascript" src="js/app-controller-configure-deliver-ccid.js"></script>
    <%--<script type="text/javascript" src="js/filters/app-controller-filter-dsp-country-sapcode.js"></script>--%>
    <script type="text/javascript"
        src="js/multimediale/fatturazione/component/filter-dsp-territorio-sapcode.component.js"></script>
    <script type="text/javascript" src="js/multimediale/fatturazione/service/anag-client.service.js"></script>
    <script type="text/javascript" src="js/app-controller-advance-payment.js"></script>
    <script type="text/javascript" src="js/app-controller-accantonamento-gestione-reclami.js"></script>
    <script type="text/javascript" src="js/multimediale/fatturazione/service/invoice.service.js"></script>
    <script type="text/javascript"
        src="js/multimediale/fatturazione/component/filter-advance-payment.component.js"></script>
    <script type="text/javascript" src="js/app-controller-update-ccid.js"></script>
    <script type="text/javascript" src="js/filters/app-controller-filter-update-ccid.js"></script>
    <script type="text/javascript" src="js/app-controller-upload-file.js"></script>
    <script type="text/javascript" src="js/app-controller-evaluate-channel.js"></script>
    <script type="text/javascript" src="js/app-controller-news.js"></script>
    <script type="text/javascript" src="js/app-controller-utenti.js"></script>
    <script type="text/javascript" src="js/app-controller-broadcasting-report.js"></script>
    <script type="text/javascript" src="js/app-controller-alter-ego.js"></script>
    <script type="text/javascript" src="js/app-controller-performing-execution-history.js"></script>
    <script type="text/javascript" src="js/app-controller-performing-sampling-config.js"></script>
    <script type="text/javascript" src="js/app-controller-performing-sampling-execution.js"></script>
    <script type="text/javascript" src="js/app-controller-performing-sampling-guide.js"></script>
    <script type="text/javascript" src="js/app-controller-performing-sampling-history.js"></script>
    <script type="text/javascript" src="js/app-controller-performing-visualizzazione-eventi.js"></script>
    <script type="text/javascript" src="js/app-controller-performing-visualizzazione-dettaglio-evento.js"></script>
    <script type="text/javascript" src="js/app-controller-performing-visualizzazione-movimenti.js"></script>
    <script type="text/javascript" src="js/app-controller-performing-visualizzazione-dettaglio-movimento.js"></script>
    <script type="text/javascript"
        src="js/app-controller-performing-visualizzazione-dettaglio-programmaMusicale.js"></script>
    <script type="text/javascript" src="js/app-controller-performing-visualizzazione-tracciamentoPM.js"></script>
    <script type="text/javascript" src="js/app-controller-performing-visualizzazione-dettaglio-fattura.js"></script>
    <script type="text/javascript" src="js/app-controller-performing-visualizzazione-fatture.js"></script>
    <script type="text/javascript" src="js/app-service-dialog-contabili-pm.js"></script>
    <script type="text/javascript" src="js/app-controller-ricercheProposte.js"></script>
    <script type="text/javascript" src="js/app-controller-ConsoleCSV.js"></script>
    <script type="text/javascript" src="js/performing/acquisizioneNDM/conf-importi-riconciliati-controller.js"></script>
    <script type="text/javascript" src="js/app-controller-reclami-upload-file.js"></script>
    <script type="text/javascript" src="js/app-controller-market-share.js"></script>

    <script type="text/javascript" src="js/app-controller-carichi-ripartizione.js"></script>
    <script type="text/javascript" src="js/app-controller-preallocazione-incassi.js"></script>
    <script type="text/javascript" src="js/app-controller-multimedialelocale.js"></script>
    <script type="text/javascript" src="js/app-controller-multimedialelocale-regolare.js"></script>
    <script type="text/javascript" src="js/app-controller-performing-visualizzazione-movimenti.js"></script>
    <script type="text/javascript" src="js/app-controller-performing-visualizzazione-dettaglio-movimento.js"></script>
    <script type="text/javascript"
        src="js/app-controller-performing-visualizzazione-dettaglio-programmaMusicale.js"></script>
    <script type="text/javascript" src="js/app-controller-performing-visualizzazione-tracciamentoPM.js"></script>
    <script type="text/javascript" src="js/app-controller-performing-visualizzazione-dettaglio-fattura.js"></script>
    <script type="text/javascript" src="js/app-controller-performing-visualizzazione-fatture.js"></script>
    <script type="text/javascript" src="js/app-service-dialog-contabili-pm.js"></script>
    <script type="text/javascript"
        src="js/app-controller-performing-visualizzazione-riconciliazioneImporti.js"></script>
    <script type="text/javascript" src="js/app-controller-performing-visualizzazione-gestioneMegaconcerti.js"></script>
    <script type="text/javascript" src="js/app-controller-performing-visualizzazione-gestioneRipartizione.js"></script>

    <script type="text/javascript" src="js/app-controller-performing-codificaManualeEsperto.js"></script>
    <script type="text/javascript" src="js/app-controller-performing-codificaManualeBase.js"></script>
    <script type="text/javascript" src="js/app-controller-performing-configurazioniSoglie.js"></script>
    <script type="text/javascript" src="js/app-controller-performing-carichiRipartizione.js"></script>
    <script type="text/javascript" src="js/app-controller-performing-regoleRipartizione.js"></script>
    <script type="text/javascript" src="js/app-controller-performing-codifica-KPICodifica.js"></script>
    <script type="text/javascript" src="js/app-controller-performing-codifica-MonitoringOperatoriCodifica.js"></script>
    <script type="text/javascript" src="js/app-controller-performing-codifica-scodifica.js"></script>
    <script type="text/javascript"
        src="pages/performing/codifica/scodifica/dettaglio-combinazione-anagrafica.controller.js"></script>
    <script type="text/javascript" src="pages/performing/codifica/modifica-codice-opera.controller.js"></script>
    <script type="text/javascript" src="js/app-controller-performing-monitoraggioCodificato.js"></script>
    <script type="text/javascript" src="js/performing/codifica/service/monitoraggioCodificatoService.js"></script>
    <script type="text/javascript" src="js/performing/codifica/service/scodificaService.js"></script>
    <script type="text/javascript" src="js/performing/cruscotti/service/programmaMusicaleService.js"></script>
    <script type="text/javascript" src="js/app-controller-performing-aggiornamenti-dati-sun.js"></script>
    <script type="text/javascript" src="js/performing/radio-in-store/service/gestioneMusicProvidersService.js"></script>
    <script type="text/javascript"
        src="js/performing/radio-in-store/controller/app-controller-performing-radioInStore-gestioneMusicProviders.js"></script>
    <script type="text/javascript" src="js/app-controller-monitoraggioPalinsesti.js"></script>

    <script type="text/javascript" src="js/app-controller-ingestion-monitoraggioIngestion.js"></script>
    <script type="text/javascript" src="js/app-controller-ingestion-dettaglioIngestion.js"></script>
    <script type="text/javascript" src="js/app-controller-triplette-gestioneTripletta.js"></script>
    <script type="text/javascript" src="js/app-controller-triplette-visualizzaTriplette.js"></script>
    <script type="text/javascript" src="js/performing/cruscotti/service/eventiPagatiService.js"></script>

    <script src="js/configurations/configurations-module.js"></script>
    <script src="js/configurations/configurations-utility-service.js"></script>
    <script src="js/configurations/configurations-service.js"></script>
    <script src="js/configurations/configurations-view-info-controller.js"></script>
    <script src="js/configurations/configurations-ranking-controller.js"></script>
    <script src="js/configurations/configurations-thresholds-controller.js"></script>

    <script src="app/multimediale/statistiche/statistics.controller.js"></script>
    <script src="app/multimediale/components/filter-dsp-statistics.component.js"></script>
    <script src="app/multimediale/statistiche/storico/storico.controller.js"></script>
    <script src="app/multimediale/backclaim/backclaim.controller.js"></script>
    <script src="app/multimediale/components/filter-dsp-country-period-offer.component.js"></script>
    <script src="app/multimediale/shared/multimediale.service.js"></script>

    <script type="text/javascript" src="js/app-controller-lavorazione-dsr.js"></script>

    <script src="js/app-controller-preparatory-activities.js"></script>
    <script src="js/app-controller-royalties-search.js"></script>
    <script src="js/app-controller-unidentified-loading.js"></script>
    <script src="js/app-controller-mail-alerts.js"></script>
    <script type="text/javascript" src="pages/ripartizione/rendiconto/app-controller-rendiconto.js"></script>
    <script type="text/javascript" src="pages/ripartizione/rendiconto/app-service-rendiconto.js"></script>
    <!-- look through  -->
    <script type="text/javascript" src="pages/ripartizione/lookThrough/app-controller-lthrough.js"></script>
    <script type="text/javascript" src="pages/ripartizione/lookThrough/app-service-lthrough.js"></script>
    <script type="text/javascript" src="pages/ripartizione/lookThrough/mock/getTipoSocieta.json"></script>

    <!-- ricerca codificato  -->
    <script type="text/javascript" src="pages/ricerca-codificato/app-service-ricerca-codificato.js"></script>
    <script type="text/javascript" src="pages/ricerca-codificato/app-controller-ricerca-codificato.js"></script>
    <script type="text/javascript" src="pages/ricerca-codificato/mock/searchCodificato.json"></script>

    <!-- conf blacklist -->
    <script type="text/javascript" src="pages/blacklist/configurazione/configurazione-blacklist.js"></script>
    <script type="text/javascript" src="pages/blacklist/app-service-blacklist.js"></script>

    <!-- conf pricing -->
    <script type="text/javascript" src="pages/configurazione-pricing/configurazione-pricing-ctrl.js"></script>
    <script type="text/javascript"
        src="pages/configurazione-pricing/templates/pricing-configuration-templates.component.js"></script>
    <script type="text/javascript" src="pages/drools/app-service-pricing.js"></script>
    <script type="text/javascript" src="pages/configurazione-pricing/popup/import-confirmation.js"></script>
    <script type="text/javascript" src="pages/configurazione-pricing/popup/update-old-conf-confirmation.js"></script>

    <!-- dashboard-dsp -->
    <script type="text/javascript" src="pages/dashboard-dsp/dashboard-dsp-ctrl.js"></script>
    <script type="text/javascript" src="pages/dashboard-dsp/app-service-dashboard-dsp.js"></script>
    <script type="text/javascript" src="pages/dashboard-dsp/configurazione/configurazione-dashboard-dsp.js"></script>
    <script type="text/javascript" src="pages/dashboard-dsp/popup/delete-confirmation.js"></script>

    <!-- ingestion codificato -->
    <script type="text/javascript" src="pages/ingestion/codificato/codificato.js"></script>
    <script type="text/javascript" src="pages/ingestion/codificato/app-service-codificato.js"></script>
    
    <!-- configurazione caratteri -->
    <script type="text/javascript" src="pages/configurazione-caretteri/configurazione-caratteri-ctrl.js"></script>
    <script type="text/javascript" src="pages/configurazione-caretteri/app-service-configurazione-caratteri.js"></script>
    <script type="text/javascript" src="pages/configurazione-caretteri/popup/delete-confirmation.js"></script>
    <script type="text/javascript" src="pages/configurazione-caretteri/popup/edit-configuration.js"></script>
    
    <!-- codifica-scodifica -->
    <script type="text/javascript" src="pages/riconoscimento/scodifica-ricodifica/app-controller-scodifica-ricodifica.js"></script>
    <script type="text/javascript" src="pages/riconoscimento/scodifica-ricodifica/app-service-scodifica-ricodifica.js"></script>

    <!-- gestione conflitti  -->
    <script type="text/javascript" src="pages/gestione-conflitti/app-controller-gestione-conflitti.js"></script>
    <script type="text/javascript" src="pages/gestione-conflitti/app-service-gestione-conflitti.js"></script>

    <script type="text/javascript">
        if (top != self) {
            top.location.href = self.location.href;
        }
    </script>
</body>

</html>
