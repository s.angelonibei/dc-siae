<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%@
	include file="../navbar.jsp" %>

<div class="bodyContents" >

	<div class="mainContainer row-fluid" >
	
		<div id="companyLogo" class="navbar commonActionsContainer noprint">
			<div class="actionsContainer row-fluid">
				<div class="span2">
<!--  -- >			<span class="companyLogo"><img src="images/siae_logo_siaetrasparente.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
<!--  -->			<span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
				</div>
				<span class="btn-toolbar span4">
					<div class="pageNumbers alignTop pull-right" style="text-align: center;">
		 				<span class="pageNumbersText"><strong>DSR METADATA</strong></span>
					</div>
				</span>				
			</div>
		</div>
	
		<div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px;"> 
			<div class="listViewPageDiv">

				<!-- actions and pagination -->
				<div class="listViewTopMenuDiv noprint">
					<div class="listViewActionsDiv row-fluid">

						<span class="btn-toolbar span4">
							<span class="btn-group">
<!--  -- >
								<button class="btn addButton" ng-click="showMoveToNext($event,ctrl.unidentifiedSong)">
									<span class="glyphicon glyphicon-forward"></span>&nbsp;&nbsp;<strong>Successiva</strong>
								</button>
<!--  -->
 							</span>
						</span>

						<span class="btn-toolbar span4">
							<span class="customFilterMainSpan btn-group" style="text-align: center;">
								<div class="pageNumbers alignTop ">
<!--  -- >
					 				<span class="pageNumbersText"><strong>Titolo</strong></span>
<!--  -->
								</div>
						 	</span>
						</span>

					<span class="span4 btn-toolbar">
							<div class="listViewActions pull-right">
								<span class="btn-group">
									<button class="btn addButton" ng-click="showNewConfig($event)">
										<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;<strong>Nuovo</strong>
									</button>
	 							</span>
	 				 		</div>
					 		<div class="clearfix"></div>
					 	</span>

					</div>
				</div>
							
<!-- actions and pagination -->
				<div class="listViewTopMenuDiv noprint">
					<div class="listViewActionsDiv row-fluid">
<!--  -->
						<span class="btn-toolbar span4">
							<span class="btn-group">

 							</span>
						</span>
<!--  -->
<!--  -->
						<span class="btn-toolbar span4">
							<span class="customFilterMainSpan btn-group" style="text-align: center;">
								<div class="pageNumbers alignTop ">
					 				<span class="pageNumbersText"><strong><!-- Titolo --></strong></span>
								</div>
						 	</span>
						</span>
<!--  -->
<!--  -->
						<span class="span4 btn-toolbar">
							<div class="listViewActions pull-right">
					 			
					 			<div class="pageNumbers alignTop ">
						 			<span ng-show="ctrl.config.hasPrev || ctrl.config.hasNext">
						 				<span class="pageNumbersText" style="padding-right:5px">
						 					Record da <strong>{{ctrl.config.first+1}}</strong> a <strong>{{ctrl.config.last}}</strong>
						 				</span>
					 					<button title="Precedente" class="btn" id="listViewPreviousPageButton" type="button" 
					 							ng-click="navigateToPreviousPage()"
					 							ng-show="ctrl.config.hasPrev">
					 						<span class="glyphicon glyphicon-chevron-left"></span>
					 					</button>
					 					<button title="Successiva" class="btn" id="listViewNextPageButton" type="button"
					 							ng-click="navigateToNextPage()"
					 							ng-show="ctrl.config.hasNext">
					 						<span class="glyphicon glyphicon-chevron-right"></span>
					 					</button>
					 				</span>
						 		</div>
					 		</div>
					 		<div class="clearfix"></div>
					 	</span>
<!--  -->
					</div>
				</div>
					
							
				<!-- eventuale filtro -->
				<div ng-include src="'pages/filters/filter-dsp-utilizzo-offerta.html'"></div>
		
				<!-- table -->
				<div class="listViewContentDiv" id="listViewContents">
					
					 <div class="contents-topscroll noprint">
					 	<div class="topscroll-div" style="width: 95%">&nbsp;</div>
					 </div>
					 					 
					 <div class="listViewEntriesDiv contents-bottomscroll">
					 	<div class="bottomscroll-div" style="width: 95%; min-height: 80px">
					 	
					 		<table class="table table-bordered listViewEntriesTable">
					 		<thead>
					 		<tr class="listViewHeaders">
					 		
					 			<th nowrap><a href="" class="listViewHeaderValues" >DSP</a></th>
					 			<th nowrap><a href="" class="listViewHeaderValues" >Tipo di utilizzo</a></th>
					 			<th nowrap><a href="" class="listViewHeaderValues" >Offerta commerciale</a></th>
					 			<th nowrap><a href="" class="listViewHeaderValues" >Formato</a></th>
					 			<th nowrap></th>
					 		</tr>
					 		</thead>
					 		<tbody > 
						 		<tr class="listViewEntries" ng-repeat="item in ctrl.config.rows">
						 			<td class="listViewEntryValue medium" nowrap >{{item.dsp}}</td>
						 			<td class="listViewEntryValue medium" nowrap >{{item.utilizationType}}</td>
						 			<td class="listViewEntryValue medium" nowrap >{{item.commercialOffer}}</td>
						 			<td class="listViewEntryValue medium" nowrap >{{item.regExRepresentation}}</td>
						 			<td nowrap class="medium">
						 				<div class="actions pull-right">
<!--  -->
						 				<span class="actionImages">
						 				<!--	<a ng-click="showInfoConfig($event,item)"><i title="Dettaglio" class="glyphicon glyphicon-info-sign alignMiddle"></i></a>&nbsp;-->
						 					<a ng-click="showDeleteConfig($event,item)"><i title="Elimina" class="glyphicon glyphicon-trash alignMiddle"></i></a>&nbsp;
						 				</span>
<!--  -->
						 				</div>
						 			</td>

						 		</tr>
					 		</tbody>
					 		</table>
					 		
						</div>
					</div>
				</div>
				<!-- /table -->
		
				
			</div>
		</div>
	</div>
<!-- -- >
	<pre>{{ctrl.properties}}</pre>
< !-- -->
</div>