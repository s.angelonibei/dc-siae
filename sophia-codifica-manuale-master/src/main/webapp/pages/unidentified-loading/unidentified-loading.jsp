<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
	<%@ include file="../navbar.jsp" %>
		<!-- path has to be adjusted-->

		<div class="bodyContents">
			<div class="mainContainer row-fluid">

				<div class="contents-topscroll noprint">
					<div class="topscroll-div" style="width: 95%;margin-bottom: 20px">&nbsp;</div>
				</div>

				<div class="sticky">
					<div id="companyLogo" class="navbar commonActionsContainer noprint">
						<div class="actionsContainer row-fluid">
							<div class="span2">
								<span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE"
										alt="SIAE">&nbsp;</span>
							</div>
							<span class="btn-toolbar span5">
								<div class="pageNumbers alignTop pull-right" style="text-align: center;">
									<span id="up" class="pageNumbersText"><strong>{{ctrl.pageTitle}}</strong></span>
								</div>
							</span>
						</div>
					</div>
				</div>

				<div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px;margin-top: 25px;">
					<div class="listViewPageDiv">
						<div class="alert alert-danger" ng-show="ctrl.messages.error && (!ctrl.messages.info)">
                            <button type="button" class="close" ng-click="clearMessages()" aria-hidden="true">x</button>
                            <span><strong>Errore: </strong>{{ctrl.messages.error}}</span>
                        </div>
                        <div class="alert alert-success" ng-show="ctrl.messages.info && (!ctrl.messages.error)">
                            <button type="button" class="close" ng-click="clearMessages()" aria-hidden="true">x</button>
                            <span>{{ctrl.messages.info}}</span>
                        </div>
				
					</div>

					<div class="listViewPageDiv">
						<div class="listViewActionsDiv row-fluid">
							<span class="span12 btn-toolbar">
								<div class="listViewActions pull-right">
									<span class="btn-group">
										<button class="btn addButton" ng-click="clearUnidentified()">
											<span class="glyphicon glyphicon-erase"></span>&nbsp;&nbsp;<strong>Elimina
												Caricamenti N.I.</strong>
										</button>
										<button class="btn addButton" ng-click="clearIdentified()">
											<span class="glyphicon glyphicon-erase"></span>&nbsp;&nbsp;<strong>Elimina Caricamenti Codificato</strong>
										</button>
									</span>
								</div>
								<div class="clearfix"></div>
							</span>
						</div>
					</div>

					<div class="listViewPageDiv">
						<div class="listViewActionsDiv row-fluid">
							<form name="myForm" ng-submit="search()" novalidate>
								<table class="table table-bordered blockContainer showInlineTable equalSplit">
									<thead>
										<tr>
											<th colspan="8">
												<a class="btn btn-link" href="javascript:void(0)"
													ng-click="ctrl.hideForm=!ctrl.hideForm">
													<span
														class="glyphicon glyphicon-{{ctrl.hideForm ? 'expand' : 'collapse-down'}}"></span>&nbsp;Parametri
													Ricerca</a>
											</th>
										</tr>
									</thead>
									<tbody ng-hide="ctrl.hideForm">
										<tr>
											<td class="fieldLabel medium">
												<label class="muted pull-right marginRight10px">DSP</label>
											</td>
											<td class="fieldValue medium">
												<div class="row-fluid">
													<span class="span10">
														<div class="input-large">
															<div ng-dropdown-multiselect="" options="ctrl.dsps"
																selected-model="ctrl.data.selectedDsps"
																checkboxes="false" events="dspEvents"
																extra-settings="ctrl.dspSettings">
															</div>
														</div>
													</span>
												</div>
											</td>

											<td class="fieldLabel medium">
												<label class="muted pull-right marginRight10px"> <span class="redColor"
														ng-show="myForm.process.$invalid">*</span>TERRITORIO</label>
											</td>
											<td class="fieldValue medium">
												<div class="row-fluid">
													<span class="span10">
														<div class="input-large">
															<div ng-dropdown-multiselect="" options="ctrl.countries"
																selected-model="ctrl.data.selectedCountries"
																checkboxes="false"
																extra-settings="ctrl.countrySettings"></div>
														</div>
													</span>
												</div>
											</td>

											<td class="fieldLabel medium">
												<label class="muted pull-right marginRight10px"> <span class="redColor"
														ng-show="myForm.process.$invalid">*</span>OFFERTA
													COMMERCIALE</label>
											</td>
											<td class="fieldValue medium">
												<div class="row-fluid">
													<span class="span10">
														<div class="input-large">
															<div ng-dropdown-multiselect=""
																options="ctrl.commercialOffers"
																selected-model="ctrl.data.selectedCommercialOffers"
																checkboxes="false"
																extra-settings="ctrl.commercialOfferSettings"></div>
														</div>
													</span>
												</div>
											</td>

											<td class="fieldLabel medium">
												<label class="muted pull-right marginRight10px">STATO</label>
											</td>
											<td class="fieldValue medium">
												<div class="row-fluid">
													<span class="span10">
														<div class="input-large">
															<div ng-dropdown-multiselect="" options="ctrl.statuses"
																selected-model="ctrl.data.selectedStatuses"
																checkboxes="false" extra-settings="multiselectSettings">
															</div>
														</div>
													</span>
												</div>
											</td>

										</tr>
										<tr>

											<td class="fieldLabel medium">
												<label class="muted pull-right marginRight10px">STATO CODIFICATO</label>
											</td>
											<td class="fieldValue medium">
												<div class="row-fluid">
													<span class="span10">
														<div class="input-large">
															<div ng-dropdown-multiselect="" options="ctrl.statuses"
																selected-model="ctrl.data.selectedCodificatoStatuses"
																checkboxes="false" extra-settings="multiselectSettings">
															</div>
														</div>
													</span>
												</div>
											</td>

											<td class="fieldLabel medium"><label
													class="muted pull-right marginRight10px">
													INIZIO PERIODO DI VALIDITA'</label>
											</td>
											<td class="fieldLabel medium" nowrap>
												<input type="text" class="span9 input-small datepickerFrom"
													style="width: 180px" ng-model="ctrl.data.selectedPeriodFrom">
												<script type="text/javascript">
													$(".datepickerFrom").datepicker({
														format: 'mm-yyyy',
														language: 'it',
														viewMode: "months",
														minViewMode: "months",
														autoclose: true,
														todayHighlight: true
													});
												</script>
												<span class="glyphicon glyphicon-erase" style="margin-top: 6px;"
													ng-click="ctrl.data.selectedPeriodFrom=undefined;"></span>
											</td>
											<td class="fieldLabel medium"><label
													class="muted pull-right marginRight10px">
													FINE PERIODO DI VALIDITA'</label>
											</td>
											<td class="fieldLabel medium" nowrap>
												<input type="text" class="span9 input-small datepickerFrom"
													style="width: 180px" ng-model="ctrl.data.selectedPeriodTo">
												<script type="text/javascript">
													$(".datepickerFrom").datepicker({
														format: 'mm-yyyy',
														language: 'it',
														viewMode: "months",
														minViewMode: "months",
														autoclose: true,
														todayHighlight: true
													});
												</script>
												<span class="glyphicon glyphicon-erase" style="margin-top: 6px;"
													ng-click="ctrl.data.selectedPeriodTo=undefined;"></span>
											</td>



											<td class="fieldLabel medium"><label
													class="muted pull-right marginRight10px">DSR
												</label></td>
											<td nowrap>
												<div class="row-fluid">
													<angucomplete-alt id="idDsr" placeholder="Nome DSR" pause="200"
														selected-object="ctrl.data.selectIdDsr"
														remote-url="rest/dsr-steps-monitoring/searchIdDsrs?idDsr="
														search-fields="name" title-field="name" minlength="1"
														input-class=" listSearchContributor" />
												</div>
											</td>

											<td class="fieldLabel medium" nowrap colspan="4">

											</td>
										</tr>

										<tr>
											<td class="fieldLabel medium" style="text-align:right" nowrap colspan="8">
												<button ng-disabled="myForm.$invalid" type="submit"
													class="btn addButton">
													<span
														class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Applica</strong>
												</button>
											</td>
										</tr>
									</tbody>
								</table>
							</form>
						</div>
					</div>

					<div class="listViewPageDiv">
						<!-- start toolbar tabella | paginazione -->
						<div class="listViewTopMenuDiv noprint" ng-if="ctrl.data.results.rows.length">
							<div class="listViewActionsDiv row-fluid">
								<span class="span12 btn-toolbar">
									<div class="listViewActions pull-right">
										<div class="pageNumbers alignTop ">
											<span >
												<span class="pageNumbersText" style="padding-right: 5px">
													Record da <strong>{{ctrl.data.results.first}}</strong> a
													<strong>{{ctrl.data.results.last}}</strong>
													su
													<strong>{{ctrl.data.results.maxrows}}</strong>
												</span>
												<button title="Precedente" class="btn" id="listViewPreviousPageButton"
													type="button" ng-click="navigateToPreviousPage()"
													ng-show="ctrl.data.results.first > 0">
													<span class="glyphicon glyphicon-chevron-left"></span>
												</button>
												<button title="Successiva" class="btn" id="listViewNextPageButton"
													type="button" ng-click="navigateToNextPage()"
													ng-show="ctrl.data.results.maxrows > ctrl.data.results.last">
													<span class="glyphicon glyphicon-chevron-right"></span>
												</button>
												<%--<button title="Fine" class="btn" id="listViewNextPageButton"
													type="button" --%>
													<%--ng-click="navigateToEndPage()"--%>
														<%--ng-show="ctrl.data.results.last <
															ctrl.data.results.maxrows">--%>
															<%--<span class="glyphicon glyphicon-step-forward">
																<%--< /button>--%>
											</span>
										</div>
									</div>
								</span>
							</div>
						</div>
					</div>
				</div>
				<!-- end toolbar tabella | paginazione -->
				<!-- table -->
				<div ng-if="ctrl.data.results.rows.length" class="listViewEntriesDiv contents-bottomscroll">
					<div class="bottomscroll-div" style="width: 95%; min-height: 80px">

						<table class="table table-bordered listViewEntriesTable ">
							<thead>
								<tr class="listViewHeaders">
									<th style="width: 7%" ng-click="sortedSearch('spanDSR','dsr')"
										style="cursor: pointer">Nome DSR&nbsp;<span id="spanDSR" style="font-size:80%"
											class="glyphicon glyphicon-sort"></span></th>
									<th style="width: 7%" ng-click="sortedSearch('spanNomeDSP','dsp')"
										style="cursor: pointer">Nome DSP&nbsp;<span id="spanNomeDSP"
											style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
									<th style="width: 7%" ng-click="sortedSearch('spanPeriodo','period')"
										style="cursor: pointer">Periodo&nbsp;<span id="spanPeriodo"
											style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
									<th style="width: 7%" ng-click="sortedSearch('spanCountry','country')"
										style="cursor: pointer">Territorio&nbsp;<span id="spanCountry"
											style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
									<th style="width: 7%"
										ng-click="sortedSearch('spanCommercialOffer','commercialOffer')"
										style="cursor: pointer">Offerta commerciale&nbsp;<span id="spanCommercialOffer"
											style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
									<th style="width: 7%" ng-click="sortedSearch('spanStatus','status')"
										style="cursor: pointer">Stato non identificato<span id="spanStatus" style="font-size:80%"
											class="glyphicon glyphicon-sort"></span></th>
									<th style="width: 7%" ng-click="sortedSearch('spanStatus','codificatoStatus')"
										style="cursor: pointer">Stato codificato<span id="spanStatus" style="font-size:80%"
											class="glyphicon glyphicon-sort"></span></th>
									<th style="width: 7%">Azioni&nbsp;</th>
								</tr>
							</thead>
							<tbody>
								<tr class="listViewEntries " ng-repeat="row in ctrl.data.results.rows">
									<td class="listViewEntryValue">{{row.dsr}}</td>
									<td class="listViewEntryValue">{{row.dspName}}</td>
									<td class="listViewEntryValue">{{formatPeriod(row.dsrPeriod)}}</td>
									<td class="listViewEntryValue">{{row.countryName}}</td>
									<td class="listViewEntryValue">{{row.commercialOfferName}}</td>
									<td class="listViewEntryValue">{{row.status.description}}</td>
									<td class="listViewEntryValue">{{row.codificatoStatus.description}}</td>
									<td class="listViewEntryValue flexRowHNoneVCenter">
										<button class="btn addButton flexRowHNoneVCenter" ng-click="submitLoadingRequest(row)">
											<span class="glyphicon glyphicon-upload"></span><strong>&nbsp;NI</strong>
										</button>
										<button class="btn addButton flexRowHNoneVCenter margin-left" ng-click="submitCodificatoRequest(row)">
											<span
												class="glyphicon glyphicon-upload"></span><strong>&nbsp;Codificato</strong>
										</button>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>


		</div>
		</div>
		<!-- <pre>{{ctrl | json}}</pre> -->