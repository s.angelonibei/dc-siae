<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../navbar.jsp"%>

<div class="bodyContents">

    <div class="row-fluid">

        <div class="contents-topscroll noprint">
            <div class="topscroll-div" style="width: 95%; margin-bottom: 20px">&nbsp;</div>
        </div>

        <div class="sticky">
            <div id="companyLogo" class="navbar commonActionsContainer noprint">
                <div class="actionsContainer row-fluid">
                    <div class="span2">
                        <span class="companyLogo">
                            <img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;
                        </span>
                    </div>
                    <span class="btn-toolbar span8">
                        <span class="customFilterMainSpan btn-group" style="text-align: center;">
                            <div class="pageNumbers alignTop ">
                                <span class="pageNumbersText">
                                    <strong style="text-transform: uppercase;">Dashboard Dsp</strong>
                                </span>
                            </div>
                        </span>
                    </span>
                </div>
            </div>
        </div>

        <md-card id="pricingContainer" md-theme="{{ showDarkTheme ? 'dark-grey' : 'default' }}" md-theme-watch>
            <md-content style="overflow: hidden;">
                <!-- MESSAGES CONTAINERS -->
                <div class="alert alert-danger" ng-show="messages.error && (!messages.info)">
                    <button type="button" class="close" ng-click="clearMessages()" aria-hidden="true">x</button>
                    <span><strong>Errore: </strong>{{messages.error}}</span>
                </div>
                <div class="alert alert-success" ng-show="messages.info && (!messages.error)">
                    <button type="button" class="close" ng-click="clearMessages()" aria-hidden="true">x</button>
                    <span>{{messages.info}}</span>
                </div>
                <div class="alert alert-warning" ng-show="messages.warning">
					<button type="button" class="close" aria-hidden="true">x</button>
					<span>{{messages.warning}}</span>
				</div>
                <!-- HEADER CONTAINER -->
                <form name="importForm" novalidate style="flex-direction: row;
                    display: flex; margin: 8px; background-color: #e7e7e7 ; border: 1px solid #ccc">

                    <div id="headerContainer pricingCustom" style="display: flex;
                        place-content: center space-evenly;
                        flex: 1 1 100%;
                        box-sizing: border-box;">
                        <md-input-container class="md-block"
                            style="flex: 1 1 100%; box-sizing: border-box; max-width: 15%;">
                            <h3>DSP</h3>
                            <md-select name="dsp" multiple ng-change="getDashboardList()" ng-model-options="{ trackBy: '$value.idDsp' }" ng-model="dspSelected"
                                aria-label="label" ng-disabled="disableHeaderForm" required md-no-asterisk>
                                <md-option ng-repeat="dsp in dspFilteredList" ng-value="{{dsp}}">
                                    {{ dsp.name }}
                                </md-option>
                            </md-select>
                            <div ng-show="importForm.dsp.$touched && importForm.dsp.$invalid"
                                class="error md-input-messages-animation">Campo obbligatorio.</div>
                        </md-input-container>
                        <md-input-container class="md-block"
                            style="flex: 1 1 100%; box-sizing: border-box; max-width: 15%;">
                            <h3>Anno</h3>
                            <md-select name="anno" ng-change="getDashboardList()" ng-model="annoSelected" aria-label="anno" required md-no-asterisk>
                                <md-option ng-repeat="anno in annoList" value="{{anno}}">
                                    {{ anno }}
                                </md-option>
                            </md-select>
                            <div ng-show="importForm.anno.$touched && importForm.anno.$invalid"
                                class="error md-input-messages-animation">Campo obbligatorio.</div>
                        </md-input-container>
                        <md-input-container class="md-block"
                            style="flex: 1 1 100%; box-sizing: border-box; max-width: 15%;">
                            <h3>Mese</h3>
                            <md-select name="mese" ng-change="getDashboardList()" ng-model="meseSelected" aria-label="meselabel" required md-no-asterisk>
                                <md-option ng-repeat="mese in meseList" value="{{mese}}">
                                    {{ mese }}
                                </md-option>
                            </md-select>
                            <div ng-show="importForm.mese.$touched && importForm.mese.$invalid"
                                class="error md-input-messages-animation">Campo obbligatorio.</div>
                        </md-input-container>
                    </div>
                </form>
                <!-- HEADER BUTTON -->
                <div style="flex-direction: column;
                        box-sizing: border-box;
                        display: flex;
                        place-content: flex-end center;
                        align-items: flex-end;
                        margin: 8px;">
                    <div>
                        <button class="btn" ng-click="goToConfiguration()">
                            <span class="glyphicon glyphicon-import"></span>
                            Configurazione
                        </button>
                        <button class="btn" ng-click="scan()">
                            <span class="glyphicon glyphicon-cd"></span>
                            Scan
                        </button>
                    </div>
                </div>
                <!-- page Container -->
                <div class="flexRowHCenterVCenter" id="pricingConfigurationContainer">
                    <div class="table-responsive text-nowrap horizontal-scroll">
                        <table class="table table-bordered listViewEntriesTable">
                            <thead>
                                <tr>
                                    <th ng-repeat="head in getTableHeader()">{{head}}</th>
                                    <!-- empty column for download button -->
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- "idDsp", "periodo", "idConfig", "dsrArrivati", "nomeFile", "offertaCommerciale", "territorio", "tipoUtilizzo"); -->
                                <tr mdbTableCol ng-repeat="item in dashboardData track by $index">
                                    <td>{{getDspFromId(item.idDsp).name}}</td>
                                    <td class="textAlignCenter">{{item.dsrArrivati}}/{{item.dsrAttesi}}</td>
                                    <td>{{item.territorio}}</td>
                                    <td>{{item.tipoUtilizzo}}</td>
                                    <td>{{getOfferingNameFromId(item.offertaCommerciale)}}</td>
                                    <td class="textAlignCenter">
                                        <span ng-if="item.semaforo" class="glyphicon glyphicon-unchecked" ng-class="{
                                            'active': item.semaforo == 'OK',
                                            'deactive': item.semaforo == 'DSR MANCANTI'
                                          }" aria-hidden="true">
                                        </span>
                                    </td>
                                    <td style="display: flex;
                                            flex-direction: row-reverse">
                                            <a ng-click="openDsrList(item)"><i title="Apri lista Dsr" class="glyphicon glyphicon-th-list alignMiddle"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div ng-show="!dashboardData.length" style="margin-top: 16px;">
                            <div style="text-align: center;">Al momento non ci sono record.</div>
                        </div>
                    </div>
                </div>
            </md-content>
        </md-card>
    </div>
</div>