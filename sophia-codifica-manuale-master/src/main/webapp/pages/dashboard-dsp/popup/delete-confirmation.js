/* 
 * input 
 * tableHeader, tableData
 */
function DeleteConfirmationDialogController($scope, ngDialog) {
    const { id } = $scope.ngDialogId;
    console.log("---> ", $scope.ngDialogData);
	const { tableData } = $scope.ngDialogData;
    $scope.data = tableData;

    $scope.getTableHeader = function () {
        const { tableHeader } = $scope.ngDialogData;
        return tableHeader;
    };

    $scope.cancel = function () {
        ngDialog.close(id);
    };

    $scope.confirm = function () {
        ngDialog.close(id, { confirm: true });
    };

}
