codmanApp.service('dashboardDspService', ['$http', '$location',
    function ($http, $location) {
        let service = this;
        const baseUrl = "rest/monitorDsr";
        const getAllConfig = "/all";
        const addConfig = '/addConfig';
        const editConfig = '/editConfig';
        const removeConfig = '/removeConfig';
        const scan = '/scan';
        const getDashboardDSP = '/getDashboardDsp';

        service.getYearsRange = function () {
            return new Promise((resolve) => {
                const years = calculateYears();
                resolve(years);
            });
        };

        service.getMonthsRange = function () {
            return new Promise((resolve) => {
                const months = calculateMonths();
                resolve(months);
            });
        };
        /*  row:[item]
            idDsp: string
            periodo: string
            idConfig: integer
            dsrArrivati: integer
            nomeFile: string
        */
        service.getDashboardConfig = function (list, periodo) {
            return new Promise((resolve, reject) => {
                $http({
                    method: 'PUT',
                    url: `${baseUrl}${getDashboardDSP}`,
                    data: { // list array of obj, periodo string
                        list,
                        periodo
                    }
                }).then(({ data }) => {
                    const { row: dashboardData } = data;
                    if (!dashboardData.length) {
                        resolve(dashboardData);
                    }
                    const unidentified = _.remove(dashboardData, (item) => { return !_.has(item, 'idConfig') });

                    const groupedUnidentifiedByIdDsp = Object.values(_.groupBy([...unidentified], 'idDsp'));
                    const fileNameUnidenfied = groupedUnidentifiedByIdDsp
                        .map(arr => arr.reduce((acc, val) => ([...acc, (val.nomeFile)]), []));
                    const mappedUnidentified = groupedUnidentifiedByIdDsp
                        .map((el, i) => {
                            const res = _.first(el);
                            return Object.assign(res, { nomeFile: fileNameUnidenfied[i] });
                        });

                    const groupedId = _.groupBy(dashboardData, 'idConfig');
                    const mappedFileNames = Object.entries(groupedId).reduce((acc, [k, v]) => ({ ...acc, [k]: v.map(val => val.nomeFile) }), {});
                    const mappedIdentified = _.uniqBy(dashboardData, "idConfig")
                        .map(el => ({ ...el, nomeFile: mappedFileNames[el.idConfig] }));
                    resolve([...mappedIdentified, ...mappedUnidentified]);
                }, () => {
                    reject([]);
                });
            });
        };

        service.getAllConfig = function () {
            return new Promise((resolve, reject) => {
                $http({
                    method: 'GET',
                    url: `${baseUrl}${getAllConfig}`
                }).then(({ data }) => {
                    resolve(data.row);
                }, () => {
                    reject([]);
                });
            });
        };

        service.getFrequenzaDsr = function () {
            return new Promise((resolve) => {
                resolve(["mensile", "trimestrale"]);
            })
        };

        service.scan = function (list, periodo) {

            return new Promise((resolve, reject) => {
                $http({
                    method: 'PUT',
                    url: `${baseUrl}${scan}`,
                    data: { // list array of obj, periodo string
                        list,
                        periodo
                    }
                })
                    .then(({ data }) => {
                        resolve(data);
                    }, (error) => {
                        reject(error);
                    });
            });
        };

        /* data: MonitorDsrDTO */
        service.addConfig = function (data) {
            return new Promise((resolve, reject) => {
                $http({
                    method: 'POST',
                    url: `${baseUrl}${addConfig}`,
                    data
                })
                    .then(({ data }) => {
                        resolve(data);
                    }, (error) => {
                        reject(error);
                    });
            });
        };

        service.editConfig = function (data, idConfig) {
            return new Promise((resolve, reject) => {
                $http({
                    method: 'POST',
                    url: `${baseUrl}${editConfig}/${idConfig}`,
                    data
                })
                    .then(({ data }) => {
                        resolve(data);
                    }, (error) => {
                        reject(error);
                    });
            });
        };

        service.removeConfig = function (idConfig) {
            return new Promise((resolve, reject) => {
                $http({
                    method: 'DELETE',
                    url: `${baseUrl}${removeConfig}/${idConfig}`
                })
                    .then(({ data }) => {
                        resolve(data);
                    }, (error) => {
                        reject(error);
                    });
            });
        };

        function calculateYears() {
            const currentYear = moment().year();
            const minus10 = _.rangeRight(-1, -11);
            const more10 = _.range(0, 11);
            return [...minus10, ...more10].map(n => currentYear + n);
        };

        function calculateMonths() {
            const months = _.range(1, 13)
                .map(n => {
                    if (n < 10) {
                        n = "0" + n;
                    }
                    return n.toString();
                })
            const quarters = ["Q1", "Q2", "Q3", "Q4"];
            return [...months, ...quarters];
        };

        function customizer(objValue, srcValue, key) {
            if (key === 'nomeFile') {
                return [objValue, srcValue];
            }
        }

        function buildUrl(url) {
            return `${baseUrlPricing}/${url}`;
        };
    }]);