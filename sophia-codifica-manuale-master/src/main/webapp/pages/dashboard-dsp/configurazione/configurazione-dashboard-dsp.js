/**
 * ConfigurazioneDashboardDspCtrl
 * 
 * @path /configurazione-dashboard-dsp
 */
codmanApp.controller('ConfigurazioneDashboardDspCtrl', ['$scope', 'ngDialog', 'pricingService', 'dashboardDspService', 'blacklistService', '$location', '$q',
    function ($scope, ngDialog, pricingService, dashboardDspService, blacklistService, $location, $q) {

        const TABLE_HEADER = ["Dsp", "Territorio", "Tipo di utilizzo", "Offerta commerciale", "Frequenza Dsr", "Dsr Attesi", "Sla invio Dsr", "Sla invio Ccid"];
        $scope.ConfigurazioneDashboardDspCtrl = [{ "idConfig": 4, "idDsp": "7digital", "territorio": "*", "tipoUtilizzo": "*", "offertaCommerciale": "*", "frequenzaDsr": "mensile", "dsrAttesi": 20 }, { "idConfig": 5, "idDsp": "7digital", "territorio": "IT", "tipoUtilizzo": "*", "offertaCommerciale": "*", "frequenzaDsr": "mensile", "dsrAttesi": 20 }, { "idConfig": 6, "idDsp": "spotify", "territorio": "*", "tipoUtilizzo": "STRM_GEN", "offertaCommerciale": "*", "frequenzaDsr": "mensile", "dsrAttesi": 1 }];
        $scope.messages = {
            info: "",
            error: "",
            warning: ""
        };

        $scope.infoMessage = function (message) {
            $scope.clearMessages();
            setTimeout(() => {
                $scope.messages.info = message;
                $scope.$apply();
            }, 100);
        };

        $scope.errorMessage = function (message) {
            $scope.clearMessages();
            setTimeout(() => {
                $scope.messages.error = message;
                $scope.$apply();
            }, 100);
        };

        $scope.warningMessage = function (message) {
            $scope.clearMessages();
            setTimeout(() => {
                $scope.messages.warning = message;
                $scope.$apply();
            }, 100);
        };

        $scope.clearMessages = function () {
            $scope.messages = {
                info: "",
                error: "",
                warning: ""
            };
        };

        $scope.goToDashboard = function () {
            $location.path("/dashboard-dsp");
        };

        $scope.scan = function () {
            console.log("scan");
            $scope.startInterval();
            const periodo = $scope.getPeriodo($scope.annoSelected, $scope.meseSelected);
            const list = $scope.getMappedDspList($scope.dspSelected);
            $scope.warningMessage("Scan iniziato");
            dashboardDspService.scan(list, periodo)
                .then(_response => {
                    $scope.stopInterval();
                    $scope.infoMessage("Scan terminato con successo");
                }, () => {
                    $scope.errorMessage("Scan fallito");
                });
        };

        $scope.openEditorDialog = function (rowSelected) {
            const dspList = [...$scope.getDspList()];
            const tipoUtilizzoList = [...$scope.getUtilizationsList()];
            const offertaCommercialeList = [...$scope.getCommercialOfferList()];

            $q.all([blacklistService.getAnagCountry(), dashboardDspService.getFrequenzaDsr()])
                .then(([anagCountry, frequenzaList]) => {
                    const territorioList = Object.entries({ ['*']: "*", ...anagCountry })
                        .map(([k, v]) => ({ code: k, value: v }))
                        .sort($scope.compare);
                    let preloadData = {
                        territorioList,
                        dspList,
                        tipoUtilizzoList,
                        offertaCommercialeList,
                        frequenzaList,
                    };
                    if (rowSelected) { // edit mode
                        const { idDsp, territorio, tipoUtilizzo,
                            offertaCommerciale, frequenzaDsr, slaInvioCcid, slaInvioDsr, dsrAttesi
                        } = rowSelected;
                        preloadData = {
                            ...preloadData,
                            dsp: dspList.find(({ idDsp: nId }) => idDsp === nId),
                            territorio: territorioList.find(({ code }) => code === territorio),
                            tipoUtilizzo: tipoUtilizzoList.find(({ idUtilizationType }) => idUtilizationType === tipoUtilizzo),
                            offertaCommerciale: offertaCommercialeList.find(({ idCommercialOffering }) => idCommercialOffering == offertaCommerciale),
                            frequenzaDsr: frequenzaList.find((frequenza) => frequenza === frequenzaDsr),
                            slaInvioDsr: slaInvioDsr,
                            slaInvioCcid: slaInvioCcid,
                            dsrAttesi: dsrAttesi,
                        };
                    }
                    ngDialog.open({
                        template: 'pages/dashboard-dsp/popup/editor-dsp.html',
                        plain: false,
                        className: 'ngdialog-theme-default ngdialog-editor-dsp-custom',
                        data: preloadData,
                        controller: ['$scope', function ($scope) {
                            console.log("---> ", $scope.ngDialogData);
                            const { id } = $scope.ngDialogData;
                            $scope.save = function () {
                                console.log($scope.ngDialogData);
                                if ($scope.dialogForm.$valid) {
                                    ngDialog.close(id, $scope.buildRequestObject());
                                }
                            };

                            $scope.cancel = function () {
                                ngDialog.close(id);
                            };

                            $scope.buildRequestObject = function () {

                                const {
                                    dsp,
                                    territorio,
                                    tipoUtilizzo,
                                    offertaCommerciale,
                                    frequenzaDsr,
                                    slaInvioDsr,
                                    slaInvioCcid,
                                    dsrAttesi
                                } = $scope.ngDialogData;
                                // object is compliant with api contract
                                const addConfiguration = {
                                    idDsp: dsp.idDsp,
                                    territorio: territorio.code,
                                    tipoUtilizzo: tipoUtilizzo.idUtilizationType,
                                    offertaCommerciale: offertaCommerciale.idCommercialOffering,
                                    frequenzaDsr,
                                    slaInvioDsr,
                                    slaInvioCcid,
                                    dsrAttesi
                                };
                                // cleanup object from empty values( null undefined and empty string)
                                return _.omitBy(addConfiguration, (e) => _.isNil(e) || e === "");
                            };

                            $scope.onDspSelection = function () {
                                const editMode = !!$scope.ngDialogData.dsp;
                                $scope.ngDialogData.filteredOffertaCommercialeList = [...$scope.ngDialogData.offertaCommercialeList];
                                $scope.ngDialogData.filteredTipoUtilizzoList = [...$scope.ngDialogData.tipoUtilizzoList];
                                if (editMode) {
                                    const selectedDsp = $scope.ngDialogData.dsp.idDsp;
                                    $scope.ngDialogData.filteredOffertaCommercialeList = [{ idCommercialOffering: "*", offering: "*" }, ...$scope.ngDialogData.filteredOffertaCommercialeList
                                        .filter(({ idDSP }) => idDSP === selectedDsp)];
                                    $scope.ngDialogData.filteredTipoUtilizzoList = [{ idUtilizationType: "*", name: "*" }, ...$scope.ngDialogData.filteredTipoUtilizzoList
                                        .filter(({ idDsp = [] }) => idDsp.find(id => id === selectedDsp))];
                                }
                            };
                            $scope.onDspSelection();

                        }]
                    }).closePromise
                        .then((body) => {
                            if (body.value && typeof body.value !== "string") {
                                const editMode = !!rowSelected;
                                if (editMode) {
                                    dashboardDspService.editConfig(body.value, rowSelected.idConfig)
                                        .then(() => {
                                            $scope.infoMessage("Configurazione settata con successo.");
                                            $scope.updateConfigurationData();
                                        }, () => {
                                            $scope.errorMessage("C'è stato un problema, riprovare.");
                                        });
                                } else {
                                    dashboardDspService.addConfig(body.value)
                                        .then(() => {
                                            $scope.infoMessage("Configurazione settata con successo.");
                                            dashboardDspService.getAllConfig()
                                                .then((dashboardData) => {
                                                    $scope.setConfigurationData(dashboardData);
                                                });
                                        }, () => {
                                            $scope.errorMessage("C'è stato un problema, riprovare.");
                                        });
                                }
                            }
                        });
                });
        };

        $scope.showDeleteConfig = function (configToRemove) {
            const { slaInvioDsr = "", slaInvioCcid = "" } = configToRemove;
            const tableData = _.merge(_.omit(configToRemove, "idDsp", "offertaCommerciale", "idConfig"), { slaInvioDsr, slaInvioCcid });
            const data = {
                tableHeader: $scope.getTableHeader(),
                tableData
            };

            return ngDialog.open({
                template: 'pages/dashboard-dsp/popup/delete-confirmation.html',
                width: '60%',
                className: 'ngdialog-theme-default dialogOverflow',
                controller: ['$scope', 'ngDialog', DeleteConfirmationDialogController],
                data
            }).closePromise;
        };

        $scope.onInit = function () {
            // to ensure both are completed
            $q.all([dashboardDspService.getAllConfig(), pricingService.getDspUtilizationsCommercialOffersCountries()])
                .then(([dashboardData, data]) => {
                    $scope.setConfigurationData(dashboardData);
                    const { dsp, utilizations, commercialOffers } = data;
                    $scope.setDspList(dsp);
                    $scope.setUtilizationsList(utilizations);
                    $scope.setCommercialOfferList(commercialOffers);
                });
        };

        $scope.setConfigurationData = function (dashboardData) {
            setTimeout(() => {
                $scope.configurazioneData = dashboardData
                    .map(conf => {
                        const dspName = $scope.getDspNameFromId(conf.idDsp);
                        const nomeOfferta = $scope.getOfferingNameFromId(conf.offertaCommerciale);
                        return { ...conf, dspName, nomeOfferta };
                    });
                $scope.$apply();
            }, 100);
        };

        $scope.editRow = function (row) {
            $scope.openEditorDialog(_.clone(row));
        };

        $scope.updateConfigurationData = function () {
            dashboardDspService.getAllConfig()
                .then(dashboardData => {
                    $scope.setConfigurationData(dashboardData);
                })
        };

        $scope.removeRow = function (item) {
            const { idConfig } = item;
            $scope.showDeleteConfig(item)
                .then(function ({ value }) {
                    if (value && typeof value !== "string") {
                        dashboardDspService.removeConfig(idConfig)
                            .then(() => {
                                $scope.updateConfigurationData();
                                $scope.infoMessage("Configurazione rimossa con successo.");
                            }, () => {
                                $scope.errorMessage("C'è stato un problema, riprovare.");
                            });
                    }
                });
        };

        $scope.getTableHeader = function () {
            return TABLE_HEADER;
        };

        $scope.getDspNameFromId = function (idDsp) {
            return ($scope.getDspList()
                .find(el => el.idDsp === idDsp) || { name: '' }).name;
        };

        $scope.getOfferingNameFromId = function (offertaCommerciale) {
            return ($scope.getCommercialOfferList()
                .find(el => el.idCommercialOffering == offertaCommerciale) || { idCommercialOffering: "*", offering: "*" }).offering;
        };

        $scope.setDspList = function (list) {
            if (!$scope.dspList) {
                $scope.dspList = [{ idDsp: "*", code: "*", name: "*" }, ...list];
            }
        };

        $scope.getDspList = function () {
            return $scope.dspList || [];
        };

        $scope.setUtilizationsList = function (list) {
            if (!$scope.utilizationsList) {
                $scope.utilizationsList = [{ idUtilizationType: "*", name: "*" }, ...list];
            }
        };

        $scope.getUtilizationsList = function () {
            return $scope.utilizationsList;
        };

        $scope.setCommercialOfferList = function (list) {
            if (!$scope.commercialOfferList) {
                $scope.commercialOfferList = [{ idCommercialOffering: "*", offering: "*" }, ...list];
            }
        };

        $scope.getCommercialOfferList = function () {
            return $scope.commercialOfferList;
        };

        $scope.onInit();
        $scope.compare = function (a, b) {
            if (a.value < b.value) {
                return -1;
            }
            if (a.value > b.value) {
                return 1;
            }
            return 0;
        }
    }
]);