<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../navbar.jsp"%>

<div class="bodyContents">

    <div class="row-fluid">

        <div class="contents-topscroll noprint">
            <div class="topscroll-div" style="width: 95%; margin-bottom: 20px">&nbsp;</div>
        </div>

        <div class="sticky">
            <div id="companyLogo" class="navbar commonActionsContainer noprint">
                <div class="actionsContainer row-fluid">
                    <div class="span2">
                        <span class="companyLogo">
                            <img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;
                        </span>
                    </div>
                    <span class="btn-toolbar span8">
                        <span class="customFilterMainSpan btn-group" style="text-align: center;">
                            <div class="pageNumbers alignTop ">
                                <span class="pageNumbersText">
                                    <strong style="text-transform: uppercase;">Configurazione Dashboard Dsp</strong>
                                </span>
                            </div>
                        </span>
                    </span>
                </div>
            </div>
        </div>

        <md-card style="margin-top:36px" id="pricingContainer" md-theme="{{ showDarkTheme ? 'dark-grey' : 'default' }}"
            md-theme-watch>
            <md-content style="overflow: hidden;">
                <!-- MESSAGES CONTAINERS -->
                <div class="alert alert-danger" ng-show="messages.error && (!messages.info)">
                    <button type="button" class="close" ng-click="clearMessages()" aria-hidden="true">x</button>
                    <span><strong>Errore: </strong>{{messages.error}}</span>
                </div>
                <div class="alert alert-success" ng-show="messages.info && (!messages.error)">
                    <button type="button" class="close" ng-click="clearMessages()" aria-hidden="true">x</button>
                    <span>{{messages.info}}</span>
                </div>
                <div class="alert alert-warning" ng-show="messages.warning">
                    <button type="button" class="close" aria-hidden="true">x</button>
                    <span>{{messages.warning}}</span>
                </div>
                <!-- HEADER CONTAINER NONE-->

                <!-- HEADER BUTTON -->
                <div style="flex-direction: column;
                    box-sizing: border-box;
                    display: flex;
                    place-content: flex-end center;
                    align-items: flex-end;
                    margin: 8px;">
                    <div>
                        <button class="btn" ng-click="editRow()">
                            <span class="glyphicon glyphicon-plus"></span>
                            Aggiungi
                        </button>
                    </div>
                </div>

                <!-- page Container -->
                <div class="flexRowHCenterVCenter" id="pricingConfigurationContainer">
                    <div class="table-responsive text-nowrap horizontal-scroll">
                        <table class="table table-bordered listViewEntriesTable">
                            <thead>
                                <tr>
                                    <th ng-repeat="head in getTableHeader()">{{head}}</th>
                                    <!-- empty column for buttons -->
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr mdbTableCol ng-repeat="item in configurazioneData track by $index">
                                    <td>{{item.dspName}}</td>
                                    <td>{{item.territorio}}</td>
                                    <td>{{item.tipoUtilizzo}}</td>
                                    <td>{{item.nomeOfferta}}</td>
                                    <td>{{item.frequenzaDsr}}</td>
                                    <td>{{item.dsrAttesi}}</td>
                                    <td>{{item.slaInvioDsr}}</td>
                                    <td>{{item.slaInvioCcid}}</td>
                                    <td>
                                        <a ng-click="editRow(item)"><i title="Modifica"
                                            class="glyphicon glyphicon-edit alignMiddle"></i></a>
                                        <a ng-click="removeRow(item)"><i title="Elimina"
                                            class="margin-left glyphicon glyphicon-trash alignMiddle"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div ng-show="!configurazioneData.length" style="margin-top: 16px;">
                            <div style="text-align: center;">Al momento non ci sono record.</div>
                        </div>
                    </div>
                </div>
            </md-content>
        </md-card>
    </div>
</div>