/**
 * DashboardDspCtrl
 * 
 * @path /dashboard-dsp
 */
codmanApp.controller('DashboardDspCtrl', ['$scope', 'ngDialog', 'pricingService', 'dashboardDspService', '$location', '$interval',
    function ($scope, ngDialog, pricingService, dashboardDspService, $location, $interval) {

        const TABLE_HEADER = ["Dsp", "Dsr Ricevuti/Attesi", "Territorio", "Tipo di utilizzo", "Offerta commerciale", "Alert"];
        $scope.configuration = {};
        $scope.messages = {
            info: "",
            error: "",
            warning: ""
        };

        $scope.$on("$destroy", () => {
            $scope.stopInterval();
        });

        $scope.startInterval = function () {
            if (!$scope.pollingRequestDashboard) {
                $scope.pollingRequestDashboard = $interval($scope.getDashboardList, 30000);
            }
            if (!$scope.pollingRequestDsp) {
                $scope.pollingRequestDsp = $interval($scope.setDspFilteredList, 30000);
            }
        };

        $scope.stopInterval = function () {
            $interval.cancel($scope.pollingRequestDsp);
            $interval.cancel($scope.pollingRequestDashboard);
        };

        $scope.infoMessage = function (message) {
            $scope.clearMessages();
            setTimeout(() => {
                $scope.messages.info = message;
                $scope.$apply();
            }, 100);
        };

        $scope.errorMessage = function (message) {
            $scope.clearMessages();
            setTimeout(() => {
                $scope.messages.error = message;
                $scope.$apply();
            }, 100);
        };

        $scope.warningMessage = function (message) {
            $scope.clearMessages();
            setTimeout(() => {
                $scope.messages.warning = message;
                $scope.$apply();
            }, 100);
        };

        $scope.clearMessages = function () {
            $scope.messages = {
                info: "",
                error: "",
                warning: ""
            };
        };

        $scope.goToConfiguration = function () {
            $location.path("/configurazione-dashboard-dsp");
        };

        $scope.scan = function () {
            console.log("scan");
            if (!$scope.importForm.$valid) {
                $scope.markFormAsTouched($scope.importForm);
                return;
            }
            $scope.startInterval();
            const periodo = $scope.getPeriodo($scope.annoSelected, $scope.meseSelected);
            const list = $scope.getMappedDspList($scope.dspSelected);
            $scope.warningMessage("Scan iniziato");
            dashboardDspService.scan(list, periodo)
                .then(_response => {
                    $scope.stopInterval();
                    $scope.getDashboardList();
                    $scope.infoMessage("Scan terminato con successo");
                }, () => {
                    $scope.errorMessage("Scan fallito");
                });
        };

        $scope.openDsrList = function (rowSelected) {
            const getDspFromId = $scope.getDspFromId;
            $scope.stopInterval();
            ngDialog.open({
                template: 'pages/dashboard-dsp/dsr-list-popup.html',
                plain: false,
                className: 'ngdialog-theme-default ngdialog-theme-custom',
                controllerAs: 'ctrl',
                data: { idDsp: rowSelected.idDsp, periodo: rowSelected.periodo, dsrList: rowSelected.nomeFile },
                controller: ['$scope', function ($scope) {
                    $scope.dspName = getDspFromId($scope.ngDialogData.idDsp).name;
                    $scope.cancel = function () {
                        ngDialog.closeAll(false);
                    };
                }]
            }).closePromise.then(function (_data) {
                $scope.startInterval();
            });
        };

        $scope.onInit = function () {
            pricingService.getDspUtilizationsCommercialOffersCountries()
                .then(({ dsp, commercialOffers }) => {
                    $scope.setDspList(dsp);
                    $scope.setDspFilteredList();
                    $scope.setCommercialOfferList(commercialOffers);
                    $scope.pollingRequestDsp = $interval($scope.setDspFilteredList, 30000);
                });

            dashboardDspService.getYearsRange()
                .then((anno) => {
                    if (!$scope.annoList) {
                        $scope.annoList = anno;
                    }
                });
            dashboardDspService.getMonthsRange()
                .then((mese) => {
                    if (!$scope.meseList) {
                        $scope.meseList = mese;
                    }
                });
        };

        $scope.setDspFilteredList = function () {
            $scope.clearMessages();
            /* TODO if scan completed, return and cancel interval polling */
            dashboardDspService.getAllConfig()
                .then((allConfig) => {
                    const grouped = _.uniqBy(allConfig, 'idDsp')
                        .map(({ idDsp, frequenzaDsr }) => ({ idDsp, frequenzaDsr }));
                    $scope.filteredFrequence = [...grouped];
                    const listDsp = grouped
                        .map(({ idDsp }) => idDsp);
                    $scope.dspFilteredList = $scope.getDspList()
                        .filter(({ idDsp }) => listDsp.find(el => el === idDsp));
                }, () => {
                    $scope.errorMessage("Qualcosa è andato storto, riprovare.");
                });
        };

        $scope.getDashboardList = function () {
        
            if ($scope.importForm.$valid) {
                if (!$scope.pollingRequestDashboard) {
                    $scope.pollingRequestDashboard = $interval($scope.getDashboardList, 30000);
                }
                const periodo = $scope.getPeriodo($scope.annoSelected, $scope.meseSelected);
                const list = $scope.getMappedDspList($scope.dspSelected);
                dashboardDspService.getDashboardConfig(list, periodo)
                    .then(dashboardData => {
                        $scope.dashboardData = dashboardData;
                    });
            }
        };

        $scope.markFormAsTouched = function (formGroup) {
            formGroup.$$controls.forEach(child => {
                child && child.$setTouched();
            });
        };

        $scope.getDspFromId = function (idDsp) {
            return $scope.getDspList()
                .find(el => el.idDsp === idDsp)
        };

        $scope.getMappedDspList = function (dspSelected) {
            return dspSelected
                .map(({ idDsp }) => {
                    const frequenzaDsr = $scope.filteredFrequence.find(el => el.idDsp === idDsp).frequenzaDsr;
                    return ({ idDsp, frequenzaDsr });
                });
        };

        $scope.getOfferingNameFromId = function (offertaCommerciale) {
            return ($scope.getCommercialOfferList()
                .find(el => el.idCommercialOffering == offertaCommerciale) || { idCommercialOffering: "*", offering: "*" }).offering;
        };

        $scope.setCommercialOfferList = function (list) {
            if (!$scope.commercialOfferList) {
                $scope.commercialOfferList = [{ idCommercialOffering: "*", offering: "*" }, ...list];
            }
        };

        $scope.getCommercialOfferList = function () {
            return $scope.commercialOfferList;
        };

        $scope.getPeriodo = function (anno, mese) {
            return `${anno}${mese}`;
        };

        $scope.setDspList = function (list) {
            if (!$scope.dspList) {
                $scope.dspList = list;
            }
        };

        $scope.getDspList = function () {
            return $scope.dspList;
        };

        $scope.getTableHeader = function () {
            return TABLE_HEADER;
        };

        $scope.onInit();

    }
]);