<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html lang="it">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="application-name" content="Codifica Manuale ver.1.0">
    <title>Codifica Manuale</title>
    <base href="../">
    <link rel="shortcut icon" href="images/favicon_0.ico">
    <link type="text/css" href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link type="text/css" href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
    <script type="text/javascript">
        if(top!=self){top.location.href=self.location.href;}
    </script>
    <!--[if lt IE 9]> <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script> <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script> <![endif]-->
</head>

<body>
<div class="container">
    <div class="row">
        <div class="span12">
            <form class="form-horizontal myForm" action="<%= request.getContextPath() %>/login" method="post">
                <input type="hidden" name="landing" value="<%= request.getContextPath() %>/#home" />
                <div class="control-group">
                    <label class="control-label" for="username">Username</label>
                    <div class="controls">
                        <input type="text" id="username" name="username" required autofocus>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="password">Password</label>
                    <div class="controls">
                        <input type="password" id="password" name="password" required>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <button type="submit" class="btn addButton"><span class="glyphicon glyphicon-log-in"></span>&nbsp;&nbsp;Login</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</body>

</html>