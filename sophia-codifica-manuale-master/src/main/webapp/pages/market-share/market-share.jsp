<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%><%@
include file="../navbar.jsp"%>
<input type="hidden" id="user" value="<%=(String) session.getAttribute("sso.user.userName")%>">


<div class="bodyContents">

<div class="mainContainer row-fluid">

    <div class="contents-topscroll noprint">
        <div class="topscroll-div" style="width: 95%; margin-bottom: 20px">&nbsp;</div>
    </div>

    <div class="sticky">
         <div id="companyLogo" class="navbar commonActionsContainer noprint">
              <div class="actionsContainer row-fluid">
                 <div class="span2">
                     <span class="companyLogo">
                        <img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;
                      </span>
                 </div>
                 <div style="text-align: center; whidth:100%;">
                    <span style="position: absolute;left: 50%; margin-left: -160px;margin-top:10px;">
                        <div class="pageNumbers alignTop pull-right" style="text-align: center;">
                            <span class="pageNumbersText">
                                <strong class="ng-binding">Market Share</strong>
                            </span>
                        </div>
                    </span>
                 </div>
               </div>
          </div>
    </div>


    <div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px;">
        <div class="listViewPageDiv">
            <!-- table inserimento campi-->

            <div class="listViewActionsDiv row-fluid">
                <!-- Campi di ricerca -->
                <form name="SearchForm" autocomplete="off" ng-submit="searchMarketShare(request)">
                    <filter-dsp-statistics
                            dsps-utilizations-commercial-offers-countries="dspsUtilizationsCommercialOffersCountries"
                            filter-parameters="filterParameters"
                            on-filter-apply="searchMarketShare(parameters)"
                            hide-back-claim-select="true"
                            hide-client-select="true"></filter-dsp-statistics>
                    <div class="alert alert-danger" ng-show="messages.error && (!messages.info)">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                        <span><strong>Errore: </strong>{{messages.error}}</span>
                    </div>
                    <div class="alert alert-success" ng-show="messages.info && (!messages.error)">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                        <span>{{messages.info}}</span>
                    </div>
                </form>

            </div>

            <div>
                <!-- actions and pagination -->
                <div class="listViewTopMenuDiv noprint" ng-show="marketShareList.rows && marketShareList.rows.length>0">
                    <div align="right" style="margin-top: 20px; margin-right: 10px;">
                        <button class="btn addButton" data-ng-click="exportExcel()" >
                            <strong>Esporta tutto in Excel</strong>
                        </button>
                    </div>
                    <div class="listViewActionsDiv row-fluid">
                        <span class="btn-toolbar span4"> 
                            <span class="btn-group">
                            </span>
                        </span>
                        <span class="btn-toolbar span4">
                            <span class="customFilterMainSpan btn-group" style="text-align: center;">
                                    <div class="pageNumbers alignTop ">
                                        <span class="pageNumbersText">
                                            <strong>Console dei Risultati</strong>
                                        </span>
                                    </div>
                            </span>
                        </span>
                            <span class="span4 btn-toolbar">
                            <div class="listViewActions pull-right">
                            <div class="pageNumbers alignTop ">
                                    <span ng-show="marketShareList.hasNext || marketShareList.hasPrev"> <span
                                            class="pageNumbersText" style="padding-right: 5px">
                                            Record da <strong>{{filterParameters.first + 1}}</strong> a <strong>{{filterParameters.first + marketShareList.rows.length}}</strong> su <strong>{{marketShareList.extra.totalResults}}</strong>
                                    </span>
                                        <button title="Precedente" class="btn"
                                                id="listViewPreviousPageButton" type="button"
                                                ng-click="navigateToPreviousPage()" ng-show="paging.currentPage > 1">
                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                        </button>
                                        <button title="Successiva" class="btn"
                                                id="listViewNextPageButton" type="button"
                                                ng-click="navigateToNextPage()"
                                                ng-show="marketShareList.hasNext">
                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                        </button>
                                        <button title="Fine" class="btn" id="listViewNextPageButton"
                                                type="button" ng-click="navigateToEndPage()"
                                                ng-show="paging.currentPage < paging.totPages">
                                            <span class="glyphicon glyphicon-step-forward"></span>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </span>
                        <!--  -->
                    </div>
                </div>
                <!-- table risultati -->
                <div class="listViewContentDiv" id="listViewContents" ng-show="marketShareList.rows && marketShareList.rows.length>0">


                    <div class="contents-topscroll noprint">
                        <div class="topscroll-div" style="width: 95%">&nbsp;</div>
                    </div>

                    <div class="listViewEntriesDiv contents-bottomscroll" >
                        <div class="bottomscroll-div" style="width: 95%; min-height: 80px;">
                            <table class="table table-bordered listViewEntriesTable tableFormatted">
                                <thead>
                                    <tr class="listViewHeaders">
                                    <!-- PRIMA DI CAMBIARE LA CLASSE DELLO SPAN, CONTROLLARE LA LOGICA SUL CONTROLLER DI ANGULAR. -->
                                        <th style="width:10%; text-align: center;">Dsp</th>
                                        <th style="width:10%; text-align: center;">Periodo</th>
                                        <th style="width:10%; text-align: center;">Territorio</th>
                                        <th style="width:10%; text-align: center;">Tipo utilizzo</th>
                                        <th style="width:10%; text-align: center;">Offerta commerciale</th>
                                        <th style="width:10%; text-align: center;">Totale utilizzazioni</th>
                                        <th style="width:20%; text-align: center;">Utilizzazioni claim SIAE</th>
                                        <th style="width:20%; text-align: center;">Market share CCID</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="listViewEntries" ng-repeat="file in marketShareList.rows">
                                        <td style="width:10%; text-align: center; font-size:16px;">{{file.dsp}}</td>
                                        <td style="width:10%; text-align: center; font-size:16px;">{{file.period}}</td>
                                        <td style="width:10%; text-align: center; font-size:16px;">{{countryLabel(file.country)}}</td>
                                        <td style="width:10%; text-align: center; font-size:16px;">{{file.utilizationType}}</td>
                                        <td style="width:10%; text-align: center; font-size:16px;">{{file.commercialOffer}}</td>
                                        <td style="width:10%; text-align: center; font-size:16px;">{{file.totUsage}}</td>
                                        <td style="width:20%; text-align: center; font-size:16px;">{{file.siaeIdentifiedVisualizations}}</td>
                                        <td style="width:20%; text-align: center; font-size:16px;">{{file.marketShare}}</td>
                                    </tr>
                                    <tr>
                                        <th style="width:10%; text-align: center;"></th>
                                        <th style="width:10%; text-align: center;"></th>
                                        <th style="width:10%; text-align: center;"></th>
                                        <th style="width:10%; text-align: center;"></th>
                                        <th style="width:10%; text-align: center;"><strong>MARKET SHARE TOT</strong></th>
                                        <th style="width:10%; text-align: center;">{{marketShareList.extra.totUsage}}</th>
                                        <th style="width:20%; text-align: center;">{{marketShareList.extra.siaeIdentifiedVisualizations}}</th>
                                        <th style="width:20%; text-align: center;">{{marketShareList.extra.marketShare}}</th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                      </div>

                </div>

                <!-- table risultati -->
        </div>
    </div>
</div>
</div>