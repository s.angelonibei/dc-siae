<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%@
	include file="../navbar.jsp" %>

<div class="bodyContents" >

	<div class="mainContainer row-fluid" >
	
		<div id="companyLogo" class="navbar commonActionsContainer noprint">
			<div class="actionsContainer row-fluid">
				<div class="span2">
<!--  -- >			<span class="companyLogo"><img src="images/siae_logo_siaetrasparente.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
<!--  -->			<span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
				</div>
				<span class="btn-toolbar span4">
					<div class="pageNumbers alignTop pull-right" style="text-align: center;">
		 				<span class="pageNumbersText"><strong>Ricerca Knowledge Base</strong></span>
					</div>
				</span>				
			</div>
		</div>
	
		<div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px;"> 
			<div class="listViewPageDiv">
					<div class="alert alert-danger" ng-show="messages.error && (!messages.warning)">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
							<span><strong>Errore: </strong>{{messages.error}}</span>
						</div>
						<div class="alert alert-warning" ng-show="messages.warning && (!messages.error)">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
							<span>{{messages.warning}}</span>
						</div>

				<!-- form ricerca -->
				<div class="listViewTopMenuDiv noprint">
					<div class="listViewActionsDiv row-fluid">

					<form name="form" ng-submit="onRicerca(request)">
					<table class="table table-bordered blockContainer showInlineTable equalSplit">
					<thead>
						<tr>
							<th class="blockHeader" colspan="6"
								ng-show="ctrl.hideForm">
								<span ng-click="ctrl.hideForm=false" class="glyphicon glyphicon-expand"></span>&nbsp;Parametri Ricerca
							</th>
							<th class="blockHeader" colspan="6" 
								ng-hide="ctrl.hideForm">
								<span ng-click="ctrl.hideForm=true" class="glyphicon glyphicon-collapse-down"></span>&nbsp;Parametri Ricerca
							</th>
						</tr>
					</thead>
					<tbody ng-hide="ctrl.hideForm">
						<tr>
								<td  class="fieldLabel samll" nowrap style="width:10%"></td>
							<td class="fieldLabel samll" nowrap style="width:15%">
										<label class="muted pull-right marginRight10px" >
											Codice UUID
										</label>
									</td>
									<td class="fieldValue large">
										<div class="row-fluid" style="width: 25%">
												<input name="id" class="input-large " type="text" min="2"
													ng-model="request.uuid" style="width: 387%">
										</div>
							</td>
							<td class="fieldLabel samll" nowrap  style="width:15%">
								<label class="muted pull-right marginRight10px">
									Codice ISWC
								</label>
							</td>
							<td class="fieldValue large" style="width: 25%">
								<div class="row-fluid" >
										<input name="id" class="input-large " type="text" min="2"
											ng-model="request.iswc" style="width: 97%">
								</div>
							</td>
							<td class="fieldLabel samll" nowrap style="width:10%"></td>
						</tr>
						<tr>
							<td  class="fieldLabel samll" nowrap style="width:10%"></td>
						<td class="fieldLabel samll" nowrap style="width:15%">
									<label class="muted pull-right marginRight10px" >
										Codice Opera
									</label>
								</td>
								<td class="fieldValue large">
									<div class="row-fluid" style="width: 25%">
											<input name="id" class="input-large " type="text" min="2"
												ng-model="request.codice" style="width: 387%">
									</div>
						</td>
						<td class="fieldLabel samll" nowrap  style="width:15%" colspan="2">
							
						</td>
						<td class="fieldLabel samll" nowrap style="border-left: none; width:10%"></td>
					</tr>
						
						
						
						<tr>
							<td class="fieldLabel samll" nowrap style="width:10%"></td>
							<td class="fieldLabel small" nowrap style="width:15%" >
								<label class="muted pull-right marginRight10px" >
									Titolo
								</label>
							</td>
							<td class="fieldValue large" style="width: 25%">
								<div class="row-fluid" >
										<input name="id" class="input-large " type="text" min="2"
											ng-model="request.title" style="width: 97%">
								</div>
							</td>
							
							<td class="fieldLabel small" nowrap style="width:15%">
								<label class="muted pull-right marginRight10px" >
									Artisti
								</label>
							</td>
							<td class="fieldValue large" style="width: 25%">
								<div class="row-fluid" >
										<input name="id" class="input-large " type="text" min="2"
											ng-model="request.artists" style="width: 97%">
								</div>
							</td>
							<td class="fieldLabel samll" nowrap style="width:10%"></td>	
						</tr>

						<tr>
							<td class="fieldLabel medium" style="text-align:right" nowrap colspan="6">
									<label class="muted pull-left marginLeft10px">Per effettuare la ricerca valorizzare Titolo  e Artisti o codice UUID o codice ISWC o codice Opera</label>

								&nbsp;
								<button class="btn btn-success" ng-click="onAzzera()" type="button"><span class="glyphicon glyphicon-erase"></span>&nbsp;<strong>Cancella</strong></button>
								<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-search"></span>&nbsp;<strong>Ricerca</strong></button>
							</td>
						</tr>
					</tbody>
					</table>
					</form>

					</div>
				</div>

					<br>
					<br>
				<!-- riga da riconoscere -->
				<!-- table -->
				<div class="listViewContentDiv" id="listViewContents" ng-show="ctrl.combana.length != 0">
					
					 <div class="contents-topscroll noprint">
					 	<div class="topscroll-div" style="width: 95%">&nbsp;</div>
					 </div>
					 					 
					 <div class="listViewEntriesDiv contents-bottomscroll">
					 	<div class="bottomscroll-div" style="width: 95%; min-height: 80px">
					 	
					 		<table class="table table-bordered listViewEntriesTable">
					 		<thead>
					 		<tr class="listViewHeaders">
								 <th style="width:15%;text-align: center" >Codice UUID</th>
								 <th style="width:10%;text-align: center">Codice Opera</th>
								 <th style="width:20%;text-align: center" >Titolo</th>
								 <th style="width:30%;text-align: center">Artisti</th>
								 <th style="width:10%;text-align: center">Codice ISWC</th>
								 <th style="width:10%;text-align: center">Grado di Confidenza</th>
					 			<th style="width:5%;text-align: center"></th>
					 		</tr>
					 		</thead>
					 		<tbody >
						 		<tr class="listViewEntries" ng-repeat="item in ctrl.combana">
						 			<td class="listViewEntryValue medium" style="width:15%;"  >{{item.codiceOpera}}</td>
						 			<td class="listViewEntryValue medium" style="width:10%;" >{{showCodiciOpera(item)}}</td>
						 			<td class="listViewEntryValue medium" style="width:20%;text-align: center;">{{item.titoloOriginale || item.titoloAlternativo}}</td>
						 			<td class="listViewEntryValue medium" style="width:30%;" >{{item.compositore || item.autore}}</td>
									 <td class="listViewEntryValue medium" style="width:10%;">{{item.iswc}}</td>
									 <td class="listViewEntryValue medium" style="width:10%; text-align: center;">{{item.gradoDiConfidenza | number:2}}&nbsp;%</td>

						 			<td nowrap class="medium">
						 				<div class="actions pull-right">
<!--  -->
						 				<span class="actionImages">
						 					<a ng-click="showDetails(item)"><i title="Dettagli" class="glyphicon glyphicon-list-alt alignMiddle"></i></a>&nbsp;
						 				</span>
<!--  -->
						 				</div>
						 			</td>

						 		</tr>
					 		</tbody>
					 		</table>
					 		
						</div>
					</div>
				</div>
				<!-- /table -->
				
			</div>
		</div>
	</div>
<!-- -- >
	<pre>{{ctrl.properties}}</pre>
< !-- -->
</div>