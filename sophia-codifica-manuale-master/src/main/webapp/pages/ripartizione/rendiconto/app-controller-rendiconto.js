codmanApp.controller('rendicontoCtrl', ['$scope', '$interval', '$filter', 'statistichePostRipartizioneService', 'rendicontoService',
    function ($scope, $interval, $filter, statistichePostRipartizioneService, rendicontoService) {

        $scope.datamodel = [];
        $scope.settings = {
            scrollableHeight: '200px',
            scrollable: true,
            enableSearch: false,
            displayProp: 'value',
            idProp: 'value',
            enableSelectedItemView: false,
            closeOnBlur: true,
        };
        $scope.pollingRequestReport;
        $scope.defaultPaging = {
            currentPage: 0,
            maxRows: 50,
            hasNext: true,
            hasPrev: false,
            firstRecord: 1,
            lastRecord: 50,
            numberOfItems: null,
            numberOfPages: null,
        }
        $scope.paging = { ...$scope.defaultPaging };
        $scope.requestReportBody = {
            posizioneSiae: "",
            idRipartizioneSiada: []
        };
        $scope.headerTitles = ["Posizione Siae",
            "Id Ripartizione",
            "Data Creazione",
            "Ultimo Aggiornamento",
            "Valore netto Dem",
            "Valore netto Drm",
            "Creato Da",
            "Stato"];
        $scope.messages = {
            info: "",
            error: "",
            warning: ""
        };
        $scope.filterTableSettings = {
            scrollableHeight: '200px',
            scrollable: true,
            enableSearch: false,
            displayProp: 'value',
            idProp: 'value',
            enableSelectedItemView: false,
            closeOnBlur: true,
        };
        // object to manage filterTable
        $scope.filterTable = {
            searchBy: "",
            searchDataModel: [],
            searchFilterOption: $scope.headerTitles
                .slice()
                .map(el => ({ value: el, label: el })),
            searchFilterSelected: []
        };
        // list of request
        $scope.richiesteList = [];

        $scope.clearMessages = function () {
            $scope.messages = {
                info: "",
                error: "",
                warning: ""
            }
        }

        $scope.warningMessage = function (message) {
            $scope.messages.warning = message;
        }

        $scope.infoMessage = function (message) {
            $scope.messages.info = message;
        }

        $scope.errorMessage = function (message) {
            $scope.messages.error = message;
        }

        $scope.init = function () {
            statistichePostRipartizioneService
                .getListIdSiada()
                .then(function (success) {
                    // map to {value: elValue} in order to display value in dropbox directive
                    $scope.idSiadaList = success.data
                        .map(el => ({ value: el, label: el }));
                }, function (error) {
                    console.log('ERRORE', error);
                    //scope.message...TODO
                });
            // get Requests List - reques is done with an interval of 30 sec to update data
            getRequestList();
            $scope.pollingRequestReport = $interval(getRequestList, 30000);

        }

        $scope.init();

        $scope.paginate = function (value) {
            var begin, end, index;
            begin = ($scope.paging.currentPage) * $scope.paging.maxRows;
            end = begin + $scope.paging.maxRows;
            index = $scope.richiesteList.indexOf(value);
            return (begin <= index && index < end);
        };

        $scope.navigateToPreviousPage = function () {
            const { currentPage } = $scope.paging;
            const hasPrev = (currentPage - 1) >= 0;
            $scope.paging = {
                ...$scope.paging,
                currentPage: hasPrev ? currentPage - 1 : currentPage,
                hasNext: true,
            };
            $scope.paging.hasPrev = ($scope.paging.currentPage - 1 >= 0);
            updateRecordLabel();
        }

        $scope.navigateToNextPage = function () {
            const { currentPage, numberOfPages } = $scope.paging;
            const hasNext = (currentPage + 1) <= numberOfPages;
            $scope.paging = {
                ...$scope.paging,
                currentPage: hasNext ? currentPage + 1 : currentPage,
                hasPrev: true,
            };
            $scope.paging.hasNext = ($scope.paging.currentPage + 1) < numberOfPages;
            updateRecordLabel();
        }

        function updateRecordLabel() {
            const { maxRows } = $scope.paging;
            $scope.paging = {
                ...$scope.paging,
                firstRecord: (($scope.paging.currentPage) * maxRows) + 1,
                lastRecord: ($scope.paging.currentPage) * maxRows + maxRows
            }
        }

        function getRequestList() {
            $scope.clearMessages();
            rendicontoService
                .getRequestList()
                .then(({ data }) => {
                    $scope.richiesteList = data.requestsList
                        .map((richiesta) => ({
                            ...richiesta,
                            dataCreazione: moment(richiesta.dataCreazione).format("YYYY-MM-DD HH:mm:ss"),
                            ultimoAggiornamento: moment(richiesta.ultimoAggiornamento).format("YYYY-MM-DD HH:mm:ss")
                        }));

                    if (!$scope.richiesteList.length) {
                        $scope.paging = { ...$scope.defaultPaging };
                        $scope.paging.numberOfItems = 0;
                        $scope.infoMessage("Al momento non c'è uno storico richieste.");
                        return;
                    }

                    $scope.paging = {
                        ...$scope.paging,
                        numberOfItems: $scope.richiesteList.length,
                        numberOfPages: ($scope.richiesteList.length / $scope.paging.maxRows) < 1 ? 1 : $scope.richiesteList.length / $scope.paging.maxRows
                    };
                }, _error => {
                    $scope.errorMessage("Si è verificato un problema con lo storico richieste, riprovare più tardi.");
                })
        }

        $scope.init = function () {
            $scope.clearMessages();

        }

        $scope.init();

        $scope.generaRendiconto = function (userName) {
            $scope.clearMessages();
            if ($scope.requestReportBody.posizioneSiae && $scope.requestReportBody.idRipartizioneSiada.length) {
                const request = {
                    idRipartizioneSiada: $scope.requestReportBody.idRipartizioneSiada.map(({ id }) => id),
                    posizioneSiae: $scope.requestReportBody.posizioneSiae,
                    richiestaDa: userName
                };
                rendicontoService.postReportRequest(request)
                    .then(_data => {
                        $scope.infoMessage("Richiesta effettuata con successo.");
                        // per aggiornare i dati della tabella.
                        getRequestList();
                    }, ({data:{message}}) => {
                        $scope.errorMessage(message || "Si è verificato un problema, riprovare più tardi.");
                    })
            } else {
                $scope.errorMessage("Compilare sia posizione siae che id ripartizione.");
            }
        }

        $scope.applicaFiltro = function () {
            // to catch a snap of event click moment
            $scope.clearMessages();
            const inputTextArrayValues = $scope.filterTable.searchDataModel.slice();
            if (!($scope.filterTable.searchFilterSelected.length) || !inputTextArrayValues.length) {
                $scope.warningMessage("Inserire uno o più filtri/valori.");
            }

            $scope.filterTable.searchBy = $scope.filterTable.searchDataModel.slice();
            $scope.setFilterDataTable();

        }

        $scope.$on("$destroy", () => {
            $interval.cancel($scope.pollingRequestReport);
        });

        $scope.setFilterDataTable = function () {
            $scope.filterDataTable = $scope.mapFilterSelectedToTableField();
        };

        /*
            map value         
                "Nome Ad" --> posizioneSiae 
                "Id Ripartizione" --> listaRipartizione
                "Data Creazione" --> dataCreazione
                "Ultimo Aggiornamento" --> ultimoAggiornamento
                "Valore netto Dem" --> valoreNettoDem
                "Valore netto Drm" --> valoreNettoDrm
                "Creato Da" --> richiestaDa
                "Stato" --> stato 
                
            output:
                {key: value ...}
        */
        $scope.mapFilterSelectedToTableField = function () {

            const inputText = $scope.filterTable.searchBy;
            const selectedFilter = $scope.filterTable.searchFilterSelected.slice() || [];

            return selectedFilter.reduce((acc, el) => {
                let keyFilter;
                switch (el.id.toLowerCase()) {
                    case "posizione siae":
                        keyFilter = { posizioneSiae: inputText };
                        break;
                    case "id ripartizione":
                        keyFilter = { listaRipartizione: inputText };
                        break;
                    case "data creazione":
                        keyFilter = { dataCreazione: inputText };
                        break;
                    case "ultimo aggiornamento":
                        keyFilter = { ultimoAggiornamento: inputText };
                        break;
                    case "valore netto dem":
                        keyFilter = { valoreNettoDem: inputText };
                        break;
                    case "valore netto drm":
                        keyFilter = { valoreNettoDrm: inputText }
                        break;
                    case "creato da":
                        keyFilter = { richiestaDa: inputText }
                        break;
                    case "stato":
                        keyFilter = { stato: inputText }
                        break;
                }
                return { ...acc, ...keyFilter };
            }, {});

        };

        $scope.criteriaMatch = function () {

            return (richiesta) => {
                const inputTextArrayValues = $scope.filterTable.searchBy;
                // need to check all key-value of richiesta
                if (!(Object.keys($scope.filterDataTable || {}).length) || !inputTextArrayValues.length) {
                    return true;
                }
                const filterDataTableKeys = Object.keys($scope.filterDataTable);

                // obj from richiesta with key of interested filter
                const searchOn = _.pick(richiesta, filterDataTableKeys);
                const toLowerCaseSearchOn = Object
                    .entries(searchOn)
                    .reduce((acc, [key, value]) => ({ ...acc, [key]: value.toLowerCase() }), {});
                // number of criteria inserted by user
                const inputTextArrayLength = inputTextArrayValues.length;
                // per tutti gli input inseriti trova corrispondenza nell'isma riga della tabella (toLowerCaseSearchOn)
                const resulted = inputTextArrayValues
                    .filter(input => _.some(filterDataTableKeys, (k) => toLowerCaseSearchOn[k].includes(input.toLowerCase())))
                    .length
                return resulted === inputTextArrayLength;
                // return _.some(inputTextArrayValues, (text) => _.some(toLowerCaseFilteredData, (el) => el.includes(text.toLowerCase())))
            };
        };


        $scope.exportToExcel = function () {

            // apply filter as data displayed 
            let dataRow = $filter('filter')($scope.richiesteList, $scope.criteriaMatch())
                .map(
                    ({ posizioneSiae, listaRipartizione, dataCreazione, ultimoAggiornamento, valoreNettoDem, valoreNettoDrm, richiestaDa, stato }) =>
                        [posizioneSiae, listaRipartizione, dataCreazione, ultimoAggiornamento, valoreNettoDem, valoreNettoDrm, richiestaDa, stato]
                );
            // remove download header title
            let headerTitles = $scope.headerTitles.slice();
            dataRow.unshift(headerTitles);
            let ws = XLSX.utils.aoa_to_sheet(dataRow);
            // crea il workbook
            let wb = XLSX.utils.book_new();
            // imposta il nome del worksheet
            const ws_name = 'Rendiconto_richieste';
            wb.SheetNames.push(ws_name);
            // inserisce il worksheet nel workbook
            wb.Sheets[ws_name] = ws;

            /* bookType can be any supported output type */
            let wopts = { bookType: 'xlsx', bookSST: true, type: 'binary' };

            let wbout = XLSX.write(wb, wopts);

            //costruisce il nome del file
            const current_datetime = new Date();
            const date = current_datetime.getDate() + "-" + (current_datetime.getMonth() + 1) + "-" + current_datetime.getFullYear()
            let buf = new ArrayBuffer(wbout.length);
            let view = new Uint8Array(buf);
            for (let i = 0; i !== wbout.length; ++i) view[i] = wbout.charCodeAt(i) & 0xFF;
            /* the saveAs call downloads a file on the local machine */
            saveAs(new Blob([buf], { type: "application/octet-stream" }), ws_name + date + ".xlsx");
        }
    }]);