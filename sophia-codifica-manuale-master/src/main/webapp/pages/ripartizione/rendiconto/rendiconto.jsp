<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../navbar.jsp"%>

<div class="bodyContents">

    <div class="mainContainer row-fluid">

        <div class="contents-topscroll noprint">
            <div class="topscroll-div" style="width: 95%; margin-bottom: 20px">&nbsp;</div>
        </div>

        <div class="sticky">
            <div id="companyLogo" class="navbar commonActionsContainer noprint">
                <div class="actionsContainer row-fluid">
                    <div class="span2">
                        <span class="companyLogo">
                            <img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;
                        </span>
                    </div>
                    <span class="btn-toolbar span8">
                        <span class="customFilterMainSpan btn-group" style="text-align: center;">
                            <div class="pageNumbers alignTop ">
                                <span class="pageNumbersText">
                                    <strong>Rendiconto</strong>
                                </span>
                            </div>
                        </span>
                    </span>
                </div>
            </div>
        </div>

        <div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px;" id="rendicontoContainer">
            <div class="listViewPageDiv">
                <!-- table inserimento campi-->
                <div class="alert alert-danger" ng-show="messages.error && (!messages.info)">
                    <button type="button" class="close" ng-click="clearMessages()" aria-hidden="true">x</button>
                    <span><strong>Errore: </strong>{{messages.error}}</span>
                </div>
                <div class="alert alert-success" ng-show="messages.info && (!messages.error)">
                    <button type="button" class="close" ng-click="clearMessages()" aria-hidden="true">x</button>
                    <span>{{messages.info}}</span>
                </div>
                <div class="listViewActionsDiv row-fluid" style="padding-top: 8px;">
                    <!-- Campi di ricerca -->

                    <div class="equalSplit table-bordered flexRowHNoneVCenter paddingLeftx8"
                        style="background-color: #f5f5f5;">
                        <span class="glyphicon glyphicon-expand paddingRightLeftx4">
                        </span>
                        <div class="blockHeader titleBlockFont " colspan="6">Generazione Rendiconto </div>
                    </div>
                    <div class="table-bordered flexRowHCenterVCenter" style="padding:8px;">
                        <div class="flexRowHCenterVCenter">
                            <div style=" font:bold;">Posizione Siae :</div>
                            <div class="fieldValue medium">
                                <input type="text" class="form-control maxWidth20"
                                    ng-model="requestReportBody.posizioneSiae">
                            </div>
                        </div>
                        <div class="flexRowHCenterVCenter maxWidth50">
                            <div>ID Ripartizione :</div>
                            <div class="fieldValue medium maxWidth20">
                                <div ng-dropdown-multiselect options="idSiadaList"
                                    selected-model="requestReportBody.idRipartizioneSiada" checkboxes="true"
                                    extra-settings="settings">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="equalSplit table-bordered"
                        style="padding: 6px 0px; background-color: #f5f5f5;margin: 0px;">

                        <div style="display:flex;justify-content:flex-end;flex: right;width: 100%;">
                            <button class="btn addButton marginRightLeftx4"
                                ng-click="generaRendiconto('<%=userName%>')">
                                <span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;
                                <strong>Genera Rendiconto</strong>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="flexColumnStretch">
                    <div class="listViewTopMenuDiv">
                        <div class="listViewActionsDiv row-fluid">
                            <span class="span12 btn-toolbar">
                                <span class="btn-toolbar" style="text-align: center;">
                                    <div>
                                        <span class="pageNumbersText"><strong></strong></span>
                                    </div>
                                </span>
                            </span>
                        </div>
                    </div>
                    <div class="alert alert-warning" ng-show="messages.warning && (!messages.error)">
                        <button type="button" class="close" ng-click="clearMessages()" aria-hidden="true">x</button>
                        <span>{{messages.warning}}</span>
                    </div>
                    <!-- Filtro Container -->
                    <div class="paddingTopBottomx8" ng-show="richiesteList.length">
                        <div class="equalSplit table-bordered" style="background-color: #f5f5f5;">
                            <span class="glyphicon glyphicon-expand" style="padding-left: 8px;">
                            </span>
                            <span style="line-height: 2;font-size: 17px;font-weight: bold;">Storico Richieste</span>
                        </div>
                        <div class="table-bordered"
                            style="cursor: pointer; flex-direction: row; box-sizing: border-box; display: flex; padding:8px;">
                            <div class="flexRowHCenterVCenter">
                                <div class="paddingRightLeftx6">Filtra per : </div>
                                <div style="width: 20vw;" ng-dropdown-multiselect
                                    options="filterTable.searchFilterOption"
                                    selected-model="filterTable.searchFilterSelected" checkboxes="true" checkAll
                                    extra-settings="filterTableSettings">
                                </div>
                            </div>
                            <form class="flexRowHCenterVCenter" name="criteriForm">
                                <!--  <div class="paddingRightLeftx6">Cerca per : </div>
                                <input type="text" class="form-control" ng-model="filterTable.searchDataModel"> -->
                                <div class="flexRowHCenterVCenter">
                                    <div class="noWrapText paddingRightLeftx6">Inserisci valori: </div>
                                    <div class="flexColumnStretch paddingTop25 maxWidth20">

                                        <md-chips class="table-bordered maxWidth20"
                                            ng-model="filterTable.searchDataModel" name="criteriName" md-max-chips="8"
                                            placeholder="Criterio di ricerca.." secondary-placeholder="+Criteri"
                                            md-add-on-blur="true" input-aria-labelledby="state">
                                        </md-chips>
                                        <div ng-show="filterTable.searchDataModel.length < 8">
                                            <p style="font-style: italic; font-size: 11px;">I criteri vanno inseriti
                                                separati da tasto 'Invio'.</p>
                                        </div>
                                        <div class="errors" ng-messages="criteriForm.criteriName.$error">
                                            <div ng-message="md-max-chips">Raggiunto il numero massimo di criteri.</div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="equalSplit table-bordered" style="background-color: #f5f5f5;margin: 0px;">

                            <div class="flexRowHCenterVCenter"
                                style="display:flex;justify-content:flex-end;flex: right;">
                                <button class="btn addButton" ng-click="applicaFiltro()" style="margin-right: 4px;">
                                    <strong>Applica Filtro</strong>
                                </button>
                                <button class="btn addButton" data-ng-click="exportToExcel()"
                                    style="margin-right: 4px;">
                                    <strong>Esporta in Excel</strong>
                                </button>
                                <div class="paddingTopBottomx8">
                                    <span class="flexRowHCenterVCenter  pull-right"
                                        ng-show="paging.hasNext || paging.hasPrev">
                                        <span class="pageNumbersText" style="padding-right: 5px">
                                            Record da <strong>{{paging.firstRecord}}</strong> a
                                            <strong>{{paging.lastRecord}}</strong>
                                        </span>
                                        <button title="Precedente" class="btn marginRightLeftx4"
                                            id="listViewPreviousPageButton" type="button"
                                            ng-click="navigateToPreviousPage()" ng-show="paging.hasPrev">
                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                        </button>
                                        <button title="Successiva" class="btn marginRightLeftx4"
                                            id="listViewNextPageButton" type="button" ng-click="navigateToNextPage()"
                                            ng-show="paging.hasNext">
                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive text-nowrap horizontal-scroll" ng-show="richiesteList.length"
                        id="rendicontoTable">
                        <table class="table table-bordered listViewEntriesTable">
                            <thead>
                                <tr>
                                    <th ng-repeat="head in headerTitles">{{head}}</th>
                                    <!-- empty column for download button -->
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr mdbTableCol
                                    ng-repeat="richiesta in richiesteList | filter:paginate | filter:criteriaMatch()">
                                    <td>{{richiesta.posizioneSiae}}</td>
                                    <td class="breakSpacesText">{{richiesta.listaRipartizione}}</td>
                                    <td>{{richiesta.dataCreazione}}</td>
                                    <td>{{richiesta.ultimoAggiornamento}}</td>
                                    <td>{{richiesta.valoreNettoDem}}</td>
                                    <td>{{richiesta.valoreNettoDrm}}</td>
                                    <td>{{richiesta.richiestaDa}}</td>
                                    <td>
                                        <span class="glyphicon" ng-class="{
                                            'glyphicon-ok-sign success-icon': richiesta.stato.toLowerCase() == 'completata',
                                            'glyphicon-remove-sign error-icon': richiesta.stato.toLowerCase() == 'errore',
                                            'glyphicon-retweet progress-icon': richiesta.stato.toLowerCase() == 'in corso',
                                            'glyphicon glyphicon-dashboard': richiesta.stato.toLowerCase() == 'da elaborare'
                                          }" aria-hidden="true">
                                        </span>
                                        {{richiesta.stato}}
                                    </td>
                                    <td class="positioning">
                                        <span data-ng-if="richiesta.pathS3Rendiconto">
                                            <a download="{{richiesta.pathS3Rendiconto}}"
                                                data-ng-href="downloadExtendedDSR?url={{richiesta.pathS3Rendiconto}}">
                                                <i title={{richiesta.pathS3Rendiconto}} class="glyphicon glyphicon-floppy-save alignMiddle"></i>
                                            </a>
                                        </span>
                                        <!-- Info on reject cause -->
                                        <span ng-show="richiesta.stato.toLowerCase() == 'errore'"
                                            class="glyphicon glyphicon-info-sign"
                                            uib-tooltip="{{richiesta.descrizioneErrore}}">
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- table risultati -->
                </div>
            </div>
        </div>
    </div>