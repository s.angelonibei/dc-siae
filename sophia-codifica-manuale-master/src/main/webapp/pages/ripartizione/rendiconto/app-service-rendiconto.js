codmanApp.service('rendicontoService', ['$http', '$q', 'configService', '$location', function ($http, $q, configService, $location) {

    var service = this;
    // for test porpose only
    // service.mock = $location.host() === 'localhost';
    const postRequestEndpoint = configService.mmReportRequestEndpoint;
    const reportRequestGetListEndpoint = configService.mmRequestReportGetRequestList;

    service.mmRequestReport = `${$location.protocol()}://${$location.host()}:${$location.port()}/${configService.mmRequestReportBasePath}`;

    /* 
    *   input parameter:
    *       Request: object -> {
    *           idRipartizioneSiada: [string],
    *           posizioneSiae: string,
    *           richiestaDa: string,
    *       }
    */
    service.postReportRequest = function (request) {
        /*         return service.mock ?
                    $q(function (resolve) {
                        resolve({
                            message: "done"
                        })
                    })
                    : $http({
                        method: 'POST',
                        url: service.mmRequestReport + postRequestEndpoint,
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        data: {
                            request
                        }
                    }); */

        const { idRipartizioneSiada, posizioneSiae, richiestaDa } = request;
        return $http({
            method: 'POST',
            url: service.mmRequestReport + postRequestEndpoint,
            headers: {
                'Content-Type': 'application/json'
            },
            data: { 
                idRipartizioneSiada,
                posizioneSiae,
                richiestaDa 
            }
        });

    }


    /*     service.downloadFile = function (id) {
            return $http({
                method: 'GET',
                responseType: 'blob',
                url: service.mmRequestReport + 'invoice/download648/' + id
            });
        } */

    service.getRequestList = function () {
        // for test porpose only
        /* return service.mock ?
            $http.get('./pages/ripartizione/rendiconto/mock/getRequestList.json')
            : $http({
                method: 'GET',
                url: service.mmRequestReport + reportRequestGetListEndpoint
            }); */

        return $http({
            method: 'GET',
            url: service.mmRequestReport + reportRequestGetListEndpoint
        });
    }

}]);