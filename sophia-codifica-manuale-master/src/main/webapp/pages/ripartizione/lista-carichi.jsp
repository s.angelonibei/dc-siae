<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%><%@
include file="../navbar.jsp"%>
<input type="hidden" id="user" value="<%=(String) session.getAttribute("sso.user.userName")%>">


<div class="bodyContents">

<div class="mainContainer row-fluid">

    <div class="contents-topscroll noprint">
        <div class="topscroll-div" style="width: 95%; margin-bottom: 20px">&nbsp;</div>
    </div>

    <div class="sticky">
         <div id="companyLogo" class="navbar commonActionsContainer noprint">
              <div class="actionsContainer row-fluid">
                 <div class="span2">
                     <span class="companyLogo">
                        <img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;
                      </span>
                 </div>
                 <span class="btn-toolbar span8"> 
                    <span class="customFilterMainSpan btn-group" style="text-align: center;">
                            <div class="pageNumbers alignTop ">
                                <span class="pageNumbersText">
                                    <strong>Lista Carichi</strong>
                                </span>
                            </div>
                    </span>
                </span>                
               </div>
          </div>
    </div>
    
    
    <div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px;">
        <div class="listViewPageDiv">
            <!-- table inserimento campi-->
            
                            <div class="listViewActionsDiv row-fluid">
                                 <!-- Campi di ricerca -->
                                    <form name="SearchForm" autocomplete="off">
                                                <table class="table table-bordered blockContainer showInlineTable equalSplit">
                                                    <thead>
                                                        <tr>
                                                            <th class="blockHeader" colspan="6"><span
                                                                class="glyphicon glyphicon-{{ctrl.hideForm ? 'collapse-down' : 'expand'}}"></span>
                                                                Parametri di Ricerca </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody >
                                                        <tr>
                                                            <td style="text-align: right; font:bold; width: 10%;">ID Ripartizione :</td>
                                                            <td class="fieldValue medium" style="width: 20%;">
                                                                    <input type="text" class="form-control" style="width:90%; margin-bottom:0%;"
                                                                        ng-model="request.idRipartizioneSiada">
                                                                </td>
                                                                <td style="text-align: right; font:bold; width: 10%;">Data Creazione :</td>
                                                                <td style="width:20%"><input type="text"
                                                                    class="input-append date datePeriod"
                                                                    style="width: 80%"
                                                                    ng-model="request.dataCreazione">                                                     
                                                                      <script type="text/javascript">
                                                                            $(".datePeriod")
                                                                                .datepicker(
                                                                                    {
                                                                                        format : 'mm-yyyy',
                                                                                        language : 'it',
                                                                                        viewMode : "months",
                                                                                        minViewMode : "months",
                                                                                        autoclose : true,
                                                                                        todayHighlight: true
                                                                                    });
                                                                        </script>
                                                                        <span class="glyphicon glyphicon-erase"
                                                                            style="margin-top: 6px;"
                                                                            ng-click="ctrl.selectedPeriodFrom=undefined;"></span>
                                                                </td>
                                                            <td style="text-align: right; font:bold; width:10%;">Stato :</td>
                                                            <td class="fieldValue medium" style="width: 20%;">
                                                                <select name="stato" style="width:90%; margin-bottom:0%;"
                                                                    id="stato" ng-model="request.stato">
                                                                    <option></option>
                                                                    <option>PRONTO</option>
                                                                    <option>ANNULLATO</option>
                                                                    <option>APPROVATO</option>
										                        </select>
                                                            </td>								
                                                        </tr>
                                                            <tr>
                                                                    <td class="fieldLabel medium" style="text-align: right" nowrap colspan="6">
                                                                        <button	class="btn addButton" ng-click="applyFilter(request)">
                                                                            <span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Applica</strong>
                                                                        </button>
                                                                        <button class="btn addButton pull-left" ng-click="goToProduzioneCarichi()">
                                                                            <span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;<strong>Nuovo Carico</strong>
                                                                        </button>
                                                                    </td>
                                        
                                                                </tr>
                                                     
                                                                <div class="alert alert-danger" ng-show="messages.error && (!messages.info)">
                                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                                                    <span><strong>Errore: </strong>{{messages.error}}</span>
                                                                </div>
                                                                <div class="alert alert-success" ng-show="messages.info && (!messages.error)">
                                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                                                    <span>{{messages.info}}</span>
                                                                </div>																				
                                                    </tbody>
                                                </table>
                                                </form>
                
                </div>

            <div>
                  <!-- actions and pagination -->
				<div class="listViewTopMenuDiv noprint">
						<div class="listViewActionsDiv row-fluid">
							<span class="span12 btn-toolbar">
								<span class="btn-toolbar span12" style="text-align: center; margin-left: 5%" >
                                    <div class="pageNumbers alignTop" >
                                        <span class="pageNumbersText"><strong>Risultati </strong></span>
									</div>
								</span>
									<div class="pageNumbers pull-right">
										<span ng-show="paging.hasNext || paging.hasPrev"> 
											<span class="pageNumbersText" style="padding-right: 5px">
												Record da <strong>{{paging.firstRecord}}</strong> a <strong>{{paging.lastRecord}}</strong>
											</span>
											<button title="Precedente" class="btn" id="listViewPreviousPageButton" type="button"
											ng-click="navigateToPreviousPage()" ng-show="paging.hasPrev">										
												<span class="glyphicon glyphicon-chevron-left"></span>
											</button>
											<button title="Successiva" class="btn" id="listViewNextPageButton" type="button"
												ng-click="navigateToNextPage()" ng-show="paging.hasNext">
												<span class="glyphicon glyphicon-chevron-right"></span>
											</button>
										</span>
									</div>
								<div class="clearfix"></div>
							</span>
							<!--  -->
						</div>
					</div>
                    
                    <!-- table risultati -->
                <div class="listViewContentDiv" id="listViewContents">                 
                    <div class="listViewEntriesDiv contents-bottomscroll" >
                        <div class="bottomscroll-div" style="width: 95%; min-height: 80px;">
                            <table class="table table-bordered listViewEntriesTable tableFormatted">
                                <thead>
                                    <tr class="listViewHeaders"> 
                                    <!-- PRIMA DI CAMBIARE LA CLASSE DELLO SPAN, CONTROLLARE LA LOGICA SUL CONTROLLER DI ANGULAR. -->
                                        <th style="width:20%; text-align: center;">ID Ripartizione SIADA</th>
                                        <th style="width:20%; text-align: center;">Versione</th>
                                        <th style="width:20%; text-align: center;">Stato</th>
                                        <th style="width:20%; text-align: center;">Data e ora creazione</th>
                                        <th style="width:20%; text-align: center;">n. CCID Ripartibili</th>
                                        <th style="width:20%; text-align: center;">Tot. valore economico</th>
                                        <th style="width:20%; text-align: center;">Scarica File Report</th>
                                        <th style="width:20%; text-align: center;">Azioni</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="listViewEntries" ng-repeat="file in getCarichiList">
                                        <td style="width:20%; text-align: center; font-size:16px;">{{file.idRipartizioneSiada}}</td>
                                        <td style="width:20%; text-align: center; font-size:16px;">{{file.versione}}</td>
                                        <td style="width:20%; text-align: center; font-size:16px;">{{file.stato}}</td>
                                        <td style="width:20%; text-align: center; font-size:16px;">{{file.dataOraCreazione | date: 'dd/MM/yyyy &nbsp;&nbsp;  HH:mm'}}</td>
                                        <td style="width:20%; text-align: center; font-size:16px;">{{file.numCcid}}</td>
                                        <td style="width:20%; text-align: center; font-size:16px;">{{file.valoreTotaleRipartizione| number: 2}} </td>
                                        <td style="width:20%; text-align: center;">
                                            <button class="btn addButton" ng-click="downloadFile(file)">
                                            <span class="glyphicon glyphicon glyphicon-download"></span></button></td>
                                            <td style="width:20%; text-align: center;">
                                                    <button ng-show="file.stato =='PRONTO'" class="btn addButton" ng-click = "confermaRipartizione(file.id)">
                                                    <span>Avvia Ripartizione</span></button></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                      </div>
                    
                </div>

                <!-- table risultati -->
        </div>
    </div>
</div>
</div>