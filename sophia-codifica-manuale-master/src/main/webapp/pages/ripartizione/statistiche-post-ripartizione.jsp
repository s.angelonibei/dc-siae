<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../navbar.jsp"%>
<!-- path has to be adjusted-->

<div class="bodyContents" ng-init="init()">
	<div class="mainContainer row-fluid">

		<div class="contents-topscroll noprint">
			<div class="topscroll-div" style="width: 95%;margin-bottom: 20px">&nbsp;</div>
		</div>

		<div class="sticky">
			<div id="companyLogo" class="navbar commonActionsContainer noprint">
				<div class="actionsContainer row-fluid">
					<div class="span2">
						<span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
					</div>
					<span class="btn-toolbar span4" style="margin-left:6%">
						<div class="pageNumbers alignTop pull-right" style="text-align: center;">
							<span id="up" class="pageNumbersText"><strong>{{pageTitle}}</strong></span>
						</div>
					</span>
				</div>
			</div>
		</div>

		<div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px;margin-top: 25px;">
			<div class="listViewPageDiv">
				<div class="alert alert-warning" ng-show="messages.warning" style="margin-top:20px">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
					<span>{{messages.warning}}</span>
				</div>
				<div class="alert alert-danger" ng-show="messages.error" style="margin-top:20px">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
					<span><strong>Attenzione: </strong>{{messages.error}}</span>
				</div>
				<div class="alert alert-success" ng-show="messages.info" style="margin-top:20px">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
					<span>{{messages.info}}</span>
				</div>

				<div class="listViewActionsDiv row-fluid">
					<!-- Campi di ricerca -->
					<form name="SearchForm" autocomplete="off">
						<table class="table table-bordered blockContainer showInlineTable equalSplit">
							<thead>
								<tr>
									<th class="blockHeader" colspan="6"><span
											class="glyphicon glyphicon-{{ctrl.hideForm ? 'collapse-down' : 'expand'}}"></span>
										Parametri di Ricerca </th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="fieldLabel medium" style="width: 5%;">
										<label class="muted pull-right marginRight10px">
											ID Ripartizione :
										</label>
									</td>
									<td class="fieldValue medium" style="width: 20%;">
										<select type="text" class="form-control" style="width:30%; margin-bottom:0%;"
											name="idSiada" id="idSiada" ng-model="idRipartizioneSiada"
											ng-options="item for item in idSiadaList">
										</select>
									</td>
								</tr>
								<tr>
									<td class="fieldLabel medium" style="text-align: right" nowrap colspan="6">
										<button class="btn addButton" ng-click="applyFilter(idRipartizioneSiada)"
											ng-show="idRipartizioneSiada">
											<span
												class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Applica</strong>
										</button>
									</td>

								</tr>

							</tbody>
						</table>
					</form>
				</div>

				<div>
					<!-- actions and pagination -->
					<div class="listViewTopMenuDiv noprint" ng-show="getStatisticheList.length != 0">
						<div class="listViewActionsDiv row-fluid">
							<span class="span12 btn-toolbar">
								<span class="btn-toolbar span12" style="text-align: center; margin-left: 5%">
									<div class="pageNumbers alignTop">
										<span class="pageNumbersText"><strong>Risultati </strong></span>
									</div>
								</span>
								<div class="pageNumbers pull-right">
									<div>
										<button class="btn addButton" data-ng-click="exportToExcel()">
											<strong>Esporta tutto in Excel</strong>
										</button>
									</div>
									<span ng-show="paging.hasNext || paging.hasPrev">
										<span class="pageNumbersText" style="padding-right: 5px">
											Record da <strong>{{paging.firstRecord}}</strong> a
											<strong>{{paging.lastRecord}}</strong>
										</span>
										<button title="Precedente" class="btn" id="listViewPreviousPageButton"
											type="button" ng-click="navigateToPreviousPage()" ng-show="paging.hasPrev">
											<span class="glyphicon glyphicon-chevron-left"></span>
										</button>
										<button title="Successiva" class="btn" id="listViewNextPageButton" type="button"
											ng-click="navigateToNextPage()" ng-show="paging.hasNext">
											<span class="glyphicon glyphicon-chevron-right"></span>
										</button>
									</span>
								</div>
								<div class="clearfix"></div>
							</span>
							<!--  -->
						</div>
					</div>
					<!-- table risultati -->
					<div class="listViewContentDiv" id="listViewContents" ng-show="getStatisticheList.length != 0">


						<div class="contents-topscroll noprint">
							<div class="topscroll-div" style="width: 95%">&nbsp;</div>
						</div>
						<!--Table-->
						<div class="table-responsive text-nowrap horizontal-scroll" id="statisticheTable">
							<!--Table-->
							<table class="table-striped table table-bordered listViewEntriesTable">

								<!--Table head-->
								<thead>
									<tr>
										<th ng-repeat="head in headerTitles">{{head}} </th>
									</tr>
								</thead>
								<!--Table head-->

								<!--Table body-->
								<tbody>
									<tr mdbTableCol ng-repeat="statistica in getStatisticheList">
										<td>{{statistica.nominativo}}</td>
										<td class="col-max-width-20">{{statistica.idDsr}}</td>
										<td>{{statistica.dspName}}</td>
										<td>{{statistica.valoreLordo | number: 2}}</td>
										<td>{{statistica.valoreAggioDem | number: 2}}</td>
										<td>{{statistica.valoreTrattenuta | number: 2}} </td>
										<td>{{statistica.valoreAggioDrm | number: 2}}</td>
										<td>{{statistica.valoreNetto | number: 2}} </td>
										<td>{{statistica.valoreNettoDem | number: 2}} </td>
										<td>{{statistica.valoreNettoDrm | number: 2}} </td>
										<td>
											<button class="btn addButton" ng-click="downloadFile(statistica)">
												<span class="glyphicon glyphicon glyphicon-download"></span>
											</button>
										</td>
									</tr>
								</tbody>
								<!--Table body-->
							</table>
						</div>
						<!--Table-->
						<!--Section: Live preview-->
					</div>

					<!-- table risultati -->
				</div>
			</div>
		</div>
	</div>
</div>
<!--<pre>{{ctrl | json}}</pre>-->