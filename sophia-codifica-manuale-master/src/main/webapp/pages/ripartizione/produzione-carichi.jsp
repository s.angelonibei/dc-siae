<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../navbar.jsp"%>
<!-- path has to be adjusted-->

<div class="bodyContents" ng-init="init()">
	<div class="mainContainer row-fluid">

		<div class="contents-topscroll noprint">
			<div class="topscroll-div" style="width: 95%;margin-bottom: 20px">&nbsp;</div>
		</div>

		<div class="sticky">
			<div id="companyLogo" class="navbar commonActionsContainer noprint">
				<div class="actionsContainer row-fluid">
					<div class="span2">
						<span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
					</div>
					<span class="btn-toolbar span8">
						<span class="customFilterMainSpan btn-group" style="text-align: center;">
							<div class="pageNumbers alignTop ">
								<span class="pageNumbersText">
									<strong>{{ctrl.pageTitle}}</strong>
								</span>
							</div>
						</span>
					</span>
				</div>
			</div>
		</div>

		<div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px;margin-top: 25px;">
			<div class="listViewPageDiv">
				<div class="alert alert-warning" ng-show="messages.warning" style="margin-top:20px">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
					<span>{{messages.warning}}</span>
				</div>
				<div class="alert alert-danger" ng-show="messages.error" style="margin-top:20px">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
					<span><strong>Attenzione: </strong>{{messages.error}}</span>
				</div>
				<div class="alert alert-success" ng-show="messages.info" style="margin-top:20px">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
					<span>{{messages.info}}</span>
				</div>

				<!-- start table inserimento campi-->
				<div style="margin-top: 15px" class="listViewTopMenuDiv noprint">
					<div class="listViewActionsDiv row-fluid">
						<%--<form name="myForm" novalidate>
            <table
                    class="table table-bordered blockContainer showInlineTable equalSplit noHoverTable">
                <thead>
                <tr>
                    <th class="blockHeader" colspan="8"><span
                            ng-click="ctrl.hideForm=!ctrl.hideForm"
                            class="glyphicon glyphicon-{{!ctrl.hideForm ? 'collapse-down' : 'expand'}}"></span>&nbsp;Parametri
                        Ricerca
                    </th>
                </tr>
                </thead>
                <tbody ng-hide="ctrl.hideForm">
                <td class="fieldLabel medium"><label
                        class="muted pull-right marginRight10px"> DSP
                </label></td>
                <td class="fieldValue medium">
                    <div class="row-fluid">
							<span class="span10">
                                <div class="input-large">
									<div ng-dropdown-multiselect
                                         options="ctrl.dspList"
                                         selected-model="ctrl.selectedDsp"
                                         checkboxes="false"
                                         extra-settings="multiselectDsp"
                                         ></div>
								</div>
							</span>
                    </div>
                </td>
                <td class="fieldLabel medium"><label
                        class="muted pull-right marginRight10px">
                        TERRITORIO
                </label></td>
                <td class="fieldValue medium">
                    <div class="row-fluid">
							<span class="span10">
								<div class="input-large">
									<div ng-dropdown-multiselect
                                         options="ctrl.countrySelectionList"
                                         selected-model="ctrl.selectedCountryModel"
                                         checkboxes="false"
                                         extra-settings="multiselectSettingsFilter"></div>
								</div>
							</span>
                    </div>
                </td>

                <td class="fieldLabel medium"><label
                        class="muted pull-right marginRight10px">TIPO DI
                    UTILIZZO
                </label></td>
                <td class="fieldValue medium">
                    <div class="row-fluid">
							<span class="span10">
								<div class="input-large">
									<div ng-dropdown-multiselect
                                         options="ctrl.utilizationSelectionList"
                                         selected-model="ctrl.selectedUtilizationModel"
                                         checkboxes="false"
                                         extra-settings="multiselectSettingsFilter"></div>
								</div>
							</span>
                    </div>
                </td>

                </tr>
                <tr>
                    <td class="fieldLabel medium" style="height: 20%;"><label
                            class="muted pull-right marginRight10px">INIZIO
                        PERIODO DI VALIDITA'
                    </label></td>
                    <td class="fieldLabel medium" nowrap style="background-color: white; height: 20%;"><input type="text"
                                                                class="span9 input-small datepickerFrom"
                                                                style="width: 180px; height: 16px; margin-bottom: 0%"
                                                                ng-model="ctrl.selectedPeriodFrom">
                        <script
                                type="text/javascript">
									$(".datepickerFrom").datepicker({
										format: 'mm-yyyy',
										language: 'it',
										viewMode: "months",
										minViewMode: "months",
										autoclose: true,
										todayHighlight: true
									});
								</script>
                        <span class="glyphicon glyphicon-erase"
                              style="margin-top: 6px;"
                              ng-click="ctrl.selectedPeriodFrom=undefined;"></span>
                    </td>
                    <td class="fieldLabel medium" style="height: 20%;"><label
                            class="muted pull-right marginRight10px">FINE
                        PERIODO DI VALIDITA'
                    </label></td>
                    <td class="fieldLabel medium" style="background-color: white; height: 20%;"><input type="text" 
                                                         class="span9 input-small datepickerTo"
                                                         style="width: 180px; height: 16px; margin-bottom: 0%;"
                                                         ng-model="ctrl.selectedPeriodTo">
						<script
						
                                type="text/javascript">
									$(".datepickerTo").datepicker({
										format: 'mm-yyyy',
										language: 'it',
										viewMode: "months",
										minViewMode: "months",
										autoclose: true,
										todayHighlight: true
									});
								</script>
                        <span class="glyphicon glyphicon-erase"
                              style="margin-top: 6px;"
                              ng-click="ctrl.selectedPeriodTo=undefined;"></span>

                    </td>
                    <td class="fieldLabel medium" style="height: 20%;"><label
                            class="muted pull-right marginRight10px">OFFERTA
                        COMMERCIALE
                    </label></td>
                    <td class="fieldValue medium" style="height: 20%;">
                        <div class="row-fluid">
							<span class="span10">
								<div class="input-large">
									<div ng-dropdown-multiselect=""
                                         options="ctrl.offerSelectionList"
                                         selected-model="ctrl.selectedOfferModel"
                                         checkboxes="false"
                                         extra-settings="multiselectSettingsFilter"></div>
								</div>
							</span>
                        </div>
                    </td>
				</tr>
				<tr>
						<td class="fieldLabel medium"><label
								class="muted pull-right marginRight10px">
								FLAG RIPARTITO
						</label></td>
						<td class="fieldLabel medium" nowrap style="background-color: white">
								<input type="checkbox"
								ng-model="ctrl.flagRipartito"
								style="margin-left: 10px">
						</td>
					<td class="fieldLabel medium"><label
							class="muted pull-right marginRight10px"> <span
							class="redColor"
							ng-show="myForm.selectedInvoiceNumber.$invalid">*</span>NUMERO
						FATTURA
					</label></td>
					<td class="fieldValue medium">
						<div class="row-fluid">
							<span class="span10">
								<div class="input-large">
									<input class="input-large  ng-pristine ng-valid ng-empty ng-touched"
										   name="selectedInvoiceNumber"
										   type="text" min="2"
										   ng-model="ctrl.invoiceCode">
								</div>
							</span>
						</div>
					</td>
						<td class="fieldLabel medium"></td>
						<td class="fieldLabel medium" ></td>
				</tr>
                <tr>
                    <td class="fieldLabel medium" style="text-align: right"
                        nowrap
                        colspan="8">
                        <button class="btn addButton"
                                ng-click="apply()">
                            <span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Applica</strong>
                        </button>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>--%>
						<filter-dsp-statistics anag-client-data="anagClientData"
							dsps-utilizations-commercial-offers-countries="dspsUtilizationsCommercialOffersCountries"
							filter-parameters="filterParameters" on-filter-apply="apply(parameters)"
							hide-back-claim-select="true" hide-client-select="true" show-flag-ripartito="true"
							show-invoice-code="true"></filter-dsp-statistics>
					</div>

				</div>
				<!-- end table inserimento campi -->
				<br>
				<br>

				<!--start table CCID selected-->
				<div class="listViewTopMenuDiv noprint" ng-show="ctrl.selectedItems.length != 0">
					<div class="listViewActionsDiv row-fluid">
						<span class="span12 btn-toolbar">
							<div class="pageNumbers alignTop" style="text-align: center;"><span
									class="pageNumbersText"><strong>CCID Selezionati </strong></span>
							</div>
						</span>
						<!--  -->
					</div>
				</div>
				<div style="height:200px; overflow:auto;" ng-show="ctrl.selectedItems.length != 0">
					<table class="table table-bordered listViewEntriesTable">
						<thead>
							<tr class="listViewHeaders">
								<th>
								</th>
								<th style="text-align: center;">DSP</th>
								<th style="text-align: center;">Territorio</th>
								<th style="text-align: center;">Periodo</th>
								<th style="text-align: center;">Tipo di utilizzo</th>
								<th style="text-align: center;">Offerta commerciale</th>
								<th style="text-align: center;">Numero fattura</th>
								<th style="text-align: center;">Valore del CCID lordo (&euro;)</th>
								<th style="text-align: center;">Valore del CCID parziale (&euro;)</th>
								<th style="text-align: center;">ID Ripartizione </th>
								<th style="text-align: center">Flag ripartito</th>
							</tr>
						</thead>
						<tbody>
							<tr class="listViewEntries" ng-repeat="ccid in ctrl.selectedItems track by $index">
								<td nowrap class="medium">
									<input type="checkbox" ng-model="ccid.checked" ng-change="removeItem(ccid);" />
								</td>
								<td nowrap class="medium" class="listViewEntryValue " style="text-align: center;">
									{{ccid.dspName}}</td>
								<td class="listViewEntryValue " style="text-align: center;">{{ccid.country}}</td>
								<td class="listViewEntryValue " style="text-align: center;">{{ccid.periodString}}</td>
								<td class="listViewEntryValue " style="text-align: center;">{{ccid.utilizationType}}
								</td>
								<td class="listViewEntryValue " style="text-align: center;">{{ccid.commercialOffer}}
								</td>
								<td nowrap class="medium" class="listViewEntryValue " style="text-align: center;">
									{{ccid.numeroFattura}}</td>
								<td class="listViewEntryValue " style="text-align: center;">
									{{ccid.totalValue | number: 2}}</td>
								<td class="listViewEntryValue " style="text-align: center;">
									{{ccid.valoreFattura | number: 2}}</td>
								<td class="listViewEntryValue " style="text-align: center;">
									{{ccid.periodoRipartizioneId}}</td>
								<td class="listViewEntryValue " style="text-align: center;">
									{{ccid.periodoRipartizioneId? 'SI':'NO'}}</td>
							</tr>
							<tr class="listViewEntries">
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td style="text-align:right;"> <button ng-show="ctrl.selectedItems.length != 0"
										class="btn addButton" ng-click="generaCarichi()"> <strong>Genera
											carichi</strong> </button> </td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- end table-->
				<!-- start table CCID -->
				<div style="display: flex;
					flex-direction: column;">
					<div class="flexRowCenterSpaceBtw">
						<strong ng-show="ctrl.dataCcid.rows && ctrl.dataCcid.rows.length" class="flexColumnReverse20">Seleziona CCID </strong>
						<div id="actionPagination" class="flexRowEnd"
							ng-show="ctrl.dataCcid.rows && ctrl.dataCcid.rows.length">
	
							<div class="flexRowCenterSpaceBtw">
								<div>Visualizza</div>
								<md-input-container class="md-block" style="text-align: center;">
									<md-select name="paginazione" ng-model="paginazioneSelected"
										ng-change="paginationSelectedEvent()">
										<md-option ng-repeat="numero in paginazioneList" value="{{numero}}">
											{{ numero }}</md-option>
									</md-select>
								</md-input-container>
								<div>righe</div>
							</div>
	
							<span ng-show="paging.hasNext || paging.hasPrev" class="flexRowCenterSpaceBtw">
								<span class="pageNumbersText" style="padding: 0px 6px 0px 0px">
									Record da <strong>{{paging.firstRecord}}</strong> a
									<strong>{{paging.lastRecord}}</strong>
								</span>
								<button title="Precedente" class="btn" id="listViewPreviousPageButton" type="button"
									ng-click="navigateToPreviousPage()" ng-show="paging.hasPrev">
									<span class="glyphicon glyphicon-chevron-left"></span>
								</button>
								<button title="Successiva" class="btn" id="listViewNextPageButton" type="button"
									ng-click="navigateToNextPage()" ng-show="paging.hasNext">
									<span class="glyphicon glyphicon-chevron-right"></span>
								</button>
							</span>
	
						</div>
					</div>
					<!-- actions and pagination -->

					<table class="table table-bordered listViewEntriesTable" ng-show="ctrl.dataCcid.rows">
						<thead>
							<tr class="listViewHeaders">
								<th>
									<input type="checkbox" ng-model="allSelected" ng-click="toggleItem(allSelected);" />
								</th>
								<th ng-click="sort('dspName')" class="listViewHeaderValues" style="text-align: center;">
									DSP
									<span
										class="glyphicon glyphicon-sort{{sortBy==='dspName' ? (asc ? '-by-alphabet' : '-by-alphabet-alt') : '' }}"></span>
								</th>
								<th ng-click="sort('country')" class="listViewHeaderValues" style="text-align: center;">
									Territorio
									<span
										class="glyphicon glyphicon-sort{{sortBy==='country' ? (asc ? '-by-alphabet' : '-by-alphabet-alt') : '' }}"></span>
								</th>
								<th ng-click="sort('periodString')" class="listViewHeaderValues"
									style="text-align: center;">Periodo
									<span
										class="glyphicon glyphicon-sort{{sortBy==='periodString' ? (asc ? '-by-alphabet' : '-by-alphabet-alt') : '' }}"></span>
								</th>
								<th ng-click="sort('utilizationType')" class="listViewHeaderValues"
									style="text-align: center;">Tipo di utilizzo
									<span
										class="glyphicon glyphicon-sort{{sortBy==='utilizationType' ? (asc ? '-by-alphabet' : '-by-alphabet-alt') : '' }}"></span>
								</th>
								<th ng-click="sort('commercialOffer')" class="listViewHeaderValues"
									style="text-align: center;">Offerta
									commerciale
									<span
										class="glyphicon glyphicon-sort{{sortBy==='commercialOffer' ? (asc ? '-by-alphabet' : '-by-alphabet-alt') : '' }}"></span>
								</th>
								<th nowrap class="medium" style="text-align: center;" ng-click="sort('numeroFattura')"
									class="listViewHeaderValues">Numero fattura
									<span
										class="glyphicon glyphicon-sort{{sortBy==='totalValue' ? (asc ? '-by-order' : '-by-order-alt') : '' }}"></span>
								</th>
								<th style="text-align: center;" ng-click="sort('invoiceAmount')"
									class="listViewHeaderValues">Valore del CCID lordo (&euro;)
									<span
										class="glyphicon glyphicon-sort{{sortBy==='invoiceAmount' ? (asc ? '-by-order' : '-by-order-alt') : '' }}"></span>
								</th>
								<th style="text-align: center;" ng-click="sort('invoiceAmount')"
									class="listViewHeaderValues">Valore del CCID parziale (&euro;)
									<span
										class="glyphicon glyphicon-sort{{sortBy==='invoiceAmount' ? (asc ? '-by-order' : '-by-order-alt') : '' }}"></span>
								</th>
								<th style="text-align: center;" ng-click="sort('valoreResiduo')"
									class="listViewHeaderValues">ID Ripartizione
									<span
										class="glyphicon glyphicon-sort{{sortBy==='valoreResiduo' ? (asc ? '-by-order' : '-by-order-alt') : '' }}"></span>
								</th>
								<th style="text-align: center" class="listViewHeaderValues">
									Flag ripartito
								</th>
							</tr>
						</thead>
						<tbody>
							<tr class="listViewEntries" ng-repeat="ccid in ctrl.dataCcid.rows track by $index">
								<td nowrap class="medium">
									<input type="checkbox" ng-model="ccid.checked" ng-change="toggleItem(ccid);" />
								</td>
								<td nowrap class="medium" class="listViewEntryValue " style="text-align: center;">
									{{ccid.dspName}}</td>
								<td class="listViewEntryValue " style="text-align: center;">{{ccid.country}}</td>
								<td class="listViewEntryValue " style="text-align: center;">{{ccid.periodString}}</td>
								<td class="listViewEntryValue " style="text-align: center;">{{ccid.utilizationType}}
								</td>
								<td class="listViewEntryValue " style="text-align: center;">{{ccid.commercialOffer}}
								</td>
								<td nowrap class="medium" class="listViewEntryValue " style="text-align: center;">
									{{ccid.numeroFattura}}</td>
								<td class="listViewEntryValue " style="text-align: center;">
									{{ccid.totalValue | number: 2}}</td>
								<td class="listViewEntryValue " style="text-align: center;">
									{{ccid.valoreFattura | number: 2}}</td>
								<td class="listViewEntryValue " style="text-align: center;">
									{{ccid.periodoRipartizioneId}}</td>
								<td class="listViewEntryValue " style="text-align: center;">
									{{ccid.periodoRipartizioneId? 'SI':'NO'}}</td>
							</tr>
						</tbody>
					</table>
				</div>

				<!-- end table CCID -->
			</div>
		</div>
	</div>
</div>
<!--<pre>{{ctrl | json}}</pre>-->