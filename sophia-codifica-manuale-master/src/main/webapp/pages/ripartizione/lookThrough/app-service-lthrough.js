codmanApp.service('lookThroughService', ['$http', 'configService', '$location', function ($http, configService, $location) {

    var service = this;
    const baseUrl = "api/ms-look-through/lookthrough";
    const getlist = "getList";
    const createRequest = "createRequest";
    const getTipoSocietaUrl = "rest/societa-tutela/getSocietaList"

    service.prefixUrlRequest = `${$location.protocol()}://${$location.host()}:${$location.port()}/${baseUrl}/`;

    /* 
       input parameter:
            idRipartizioneSiada: string
            idLabel: Array<string>,
            tipoDiritto: Array<string>, ["DEM", "DRM"]
            societa: string, -> codice da getSocietaList
            creatoDa: string
    */
    service.postReportRequest = function (request) {

        const { idRipartizioneSiada, idLabel, tipoDiritto, societa, creatoDa } = request;
        return $http({
            method: 'POST',
            url: service.prefixUrlRequest + createRequest,
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                idRipartizioneSiada,
                idLabel,
                tipoDiritto,
                societa,
                creatoDa
            }
        });

    };

    service.downloadFile = function (id) {
        return $http({
            method: 'GET',
            responseType: 'blob',
            url: service.prefixUrlRequest + 'invoice/download648/' + id
        });
    };

    /* 
        * pagination
        * params : {first: number, last: number} 
    */
    service.getRequestList = function (params) {
        // for test porpose only
        /* return new Promise((resolve, reject) => {
            $http.get('./pages/ripartizione/lookThrough/mock/getRequestList.json')
                .then((data) => {
                    resolve(data);
                });
        }); */
        return new Promise((resolve, reject) => {
            $http({
                method: 'GET',
                url: service.prefixUrlRequest + getlist,
                params
            })
                .then(function successCallback({ data }) {
                    resolve(data);
                }, function errorCallback(response) {
                    reject(response);
                });
        });
    };

    service.getTipoSocieta = function () {
        /* dto response [{
            codice,
            nominativo
            homeTerritory,
            posizioneSiae,
            codiceSiae,
            tenant
        }] */
        // for test porpose only
        // return new Promise((resolve, reject) => {
        /*  $http.get('./pages/ripartizione/lookThrough/mock/getTipoSocieta.json')
             .then((data) => {
                 resolve(data);
             }); */
        return new Promise((resolve, reject) => {
            $http({
                method: 'GET',
                url: getTipoSocietaUrl
            })
                .then(function successCallback({ data }) {
                    resolve(data);
                }, function errorCallback(response) {
                    reject(response);
                });
        });
        //})
    };

    service.getTipoDiritto = function () {
        return new Promise((resolve) => {
            resolve(["DEM", "DRM"]);
        });
    };

}]);