<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../navbar.jsp"%>

<div class="bodyContents">

    <div class="mainContainer row-fluid">

        <div class="contents-topscroll noprint">
            <div class="topscroll-div" style="width: 95%; margin-bottom: 20px">&nbsp;</div>
        </div>

        <div class="sticky">
            <div id="companyLogo" class="navbar commonActionsContainer noprint">
                <div class="actionsContainer row-fluid">
                    <div class="span2">
                        <span class="companyLogo">
                            <img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;
                        </span>
                    </div>
                    <span class="btn-toolbar span8">
                        <span class="customFilterMainSpan btn-group" style="text-align: center;">
                            <div class="pageNumbers alignTop ">
                                <span class="pageNumbersText">
                                    <strong>Generazione look-through</strong>
                                </span>
                            </div>
                        </span>
                    </span>
                </div>
            </div>
        </div>

        <div class="contentsDiv marginLeftZero" style="min-height: 80px;" id="lthroughContainer">
            <div class="listViewPageDiv">
                <div class="messagesContainer">
                    <div class="alert alert-danger" ng-show="messages.error && (!messages.info)">
                        <button type="button" class="close" ng-click="clearMessages()" aria-hidden="true">x</button>
                        <span><strong>Errore: </strong>{{messages.error}}</span>
                    </div>
                    <div class="alert alert-success" ng-show="messages.info && (!messages.error)">
                        <button type="button" class="close" ng-click="clearMessages()" aria-hidden="true">x</button>
                        <span>{{messages.info}}</span>
                    </div>
                </div>
                
                <div class="listViewActionsDiv row-fluid" style="padding-top: 8px;">
                    <!-- Campi di ricerca -->

                    <div class="equalSplit table-bordered flexRowHNoneVCenter paddingLeftx8"
                        style="background-color: #f5f5f5;">
                        <span class="glyphicon glyphicon-expand paddingRightLeftx4">
                        </span>
                        <div class="blockHeader titleBlockFont " colspan="6">Generazione look-through </div>
                    </div>
                    <div class="table-bordered flexRowCenterSpaceEvenly " style="padding:8px;">
                        <div class="flexRowHCenterVCenter">
                            <div>Tipo di Diritto :</div>
                            <div class="fieldValue medium maxWidth15 flex-100">
                                <div ng-dropdown-multiselect options="tipoDirittoList"
                                    selected-model="tipoDirittoSelected" checkboxes="true"
                                    extra-settings="settingsWithoutScroll">
                                </div>
                            </div>
                        </div>

						<div class="flexRowHCenterVCenter">
                            <div style=" font:bold;">Id Label :</div>
							<div class="fieldValue medium maxWidth15 flex-100">
                                <div ng-dropdown-multiselect options="idSiadaList"
                                    selected-model="selectedItem" checkboxes="true"
                                    extra-settings="settingsIdLabel" enable-starts-with="true" >
                                </div>
                            </div>
                            <!-- md-min-length="0" md-items="item in autocompleteItems" md-search-text-change="querySearch(searchText)" 
							<md-autocomplete id="lookThroughAC" flex required md-input-name="autocompleteField"
								md-selected-item="selectedItem" md-search-text="searchText" md-min-length="0"
								md-items="item in querySearch(searchText)"
								md-require-match>
								<md-item-template>

									<md-checkbox ng-if="item.value === -1000" aria-label="Select All" ng-checked="isChecked()"
										md-indeterminate="isIndeterminate()" ng-click="toggleAll($event)">
										<span ng-if="isChecked()">De-</span>{{item.label}}
									</md-checkbox>
									<div  ng-if="item.value !== -1000" class="demo-select-all-checkboxes" flex="100">
										<md-checkbox ng-checked="exists(item, selected)"
											ng-click="toggle(item, selected, $event)">
											{{ item.label }}
										</md-checkbox>
									</div>
								</md-item-template>
							</md-autocomplete>-->
						</div>

                        <div class="flexRowHCenterVCenter">
                            <div style=" font:bold;">Ripartizione SIADA:</div>
                            <div class="fieldValue medium flex-100">
                                <input type="text" class="form-control maxWidth15" ng-model="ripartizioneSiada">
                            </div>
                        </div>

                        <div class="flexRowHCenterVCenter">
                            <div style=" font:bold;">Tipo Societa :</div>
                            <div class="fieldValue medium maxWidth15 flex-100">
                                <div ng-dropdown-multiselect options="tipoSocietaList"
                                    selected-model="selectedTipoSocieta" checkboxes="true"
                                    extra-settings="settingsWithoutScroll">
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="equalSplit table-bordered"
                        style="padding: 6px 0px; background-color: #f5f5f5;margin: 0px;">

                        <div class="flex-100" style="display:flex;justify-content:flex-end;flex: right;width: 100%;">
                            <button class="btn addButton marginRightLeftx4"
                                ng-click="generaRendiconto('<%=userName%>')">
                                <span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;
                                <strong>Genera look-through</strong>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="flexColumnStretch">
                    <!-- pagination -->
                    <div class="paddingTopBottomx8">
                        <span class="flexRowHCenterVCenter  pull-right" ng-show="paging.hasNext || paging.hasPrev">
                            <span class="pageNumbersText" style="padding-right: 5px">
                                Record da <strong>{{paging.firstRecord}}</strong> a
                                <strong>{{paging.lastRecord}}</strong>
                            </span>
                            <button title="Precedente" class="btn marginRightLeftx4" id="listViewPreviousPageButton"
                                type="button" ng-click="navigateToPreviousPage()" ng-show="paging.hasPrev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                            </button>
                            <button title="Successiva" class="btn marginRightLeftx4" id="listViewNextPageButton"
                                type="button" ng-click="navigateToNextPage()" ng-show="paging.hasNext">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </button>
                        </span>
                    </div>
                    <div id="lookThroughTable">
                        <div class="table-responsive text-nowrap horizontal-scroll">
                            <table class="table table-bordered listViewEntriesTable">
                                <thead>
                                    <tr>
                                        <th ng-repeat="head in headerTitles">{{head}}</th>
                                        <!-- empty column for download button -->
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr mdbTableCol ng-repeat="item in lookThroughDataTable track by $index">
                                        <td>{{item.idRipartizioneSiada}}</td>
                                        <td>{{item.idLabel}}</td>
                                        <td>{{item.tipoDiritto}}</td>
                                        <td>{{item.dataCreazione}}</td>
                                        <td>{{item.ultimoAggiornamento}}</td>
                                        <td>{{item.societa}}</td>
                                        <td>{{item.creatoDa}}</td>
                                        <td>
                                            <span ng-class="{
                                                'glyphicon glyphicon-ok-sign success-icon': item.stato.toLowerCase() == 'completata',
                                                'glyphicon glyphicon-remove-sign error-icon': item.stato.toLowerCase() == 'errore',
                                                'glyphicon glyphicon-retweet progress-icon': item.stato.toLowerCase() == 'in lavorazione', 
                                                'glyphicon glyphicon glyphicon-dashboard': item.stato.toLowerCase() == 'da elaborare'
                                              }" aria-hidden="true">
                                            </span>
                                            {{item.stato}}
                                        </td>
                                        <td class="flexRowSpaceCenter">
                                            <span data-ng-if="item.fileDem">
                                                <a download="{{item.fileDem}}"
                                                    data-ng-href="downloadExtendedDSR?url={{item.fileDem}}">
                                                    <i title={{item.fileDem}}
                                                        class="glyphicon glyphicon-floppy-save alignMiddle"></i>
                                                </a>
                                            </span>
                                            <span data-ng-if="item.fileDrm">
                                                <a download="{{item.fileDrm}}"
                                                    data-ng-href="downloadExtendedDSR?url={{item.fileDrm}}">
                                                    <i title={{item.fileDrm}}
                                                        class="glyphicon glyphicon-floppy-save alignMiddle"></i>
                                                </a>
                                            </span>
                                            <!-- Info on reject cause -->
                                            <span ng-show="item.stato.toLowerCase() == 'errore'"
                                                class="glyphicon glyphicon-info-sign" uib-tooltip="{{item.errore}}">
                                            </span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div ng-show="!lookThroughDataTable.length" style="margin-top: 16px;">
                                <div style="text-align: center;">Al momento non ci sono record.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>