codmanApp.controller('lookThroughCtrl', ['$scope', '$q', '$timeout', '$interval', 'statistichePostRipartizioneService', 'lookThroughService',
    function ($scope, $q, $timeout, $interval, statistichePostRipartizioneService, lookThroughService) {
        $scope.queryParams = {
            first: 0,
            last: 50
        };
        $scope.settings = {
            scrollableHeight: '200px',
            scrollable: true,
            enableSearch: false,
            displayProp: 'value',
            idProp: 'value',
            enableSelectedItemView: false,
            closeOnBlur: true,
        };
        $scope.pollingRequestReport;
        $scope.defaultPaging = {
            // currentPage: 0,
            maxRows: 50,
            hasNext: false,
            hasPrev: false,
            firstRecord: 1,
            lastRecord: 50,
            // numberOfItems: null,
            //numberOfPages: null,
        };
        $scope.paging = { ...$scope.defaultPaging };
        $scope.selectedItem = [];
        $scope.headerTitles = ["Id ripartizione Siada",
            "Id Label",
            "Tipo Diritto",
            "Data Creazione",
            "Ultimo Aggiornamento",
            "Società",
            "Creato Da",
            "Stato"];

        $scope.messages = {
            info: "",
            error: "",
            warning: ""
        };
        // list of request
        $scope.lookThroughDataTable = [];
        $scope.autocompleteItems = [];
        $scope.selectedTipoSocieta;

        $scope.clearMessages = function () {
            $scope.messages = {
                info: "",
                error: "",
                warning: ""
            }
        };

        $scope.warningMessage = function (message) {
            $scope.messages.warning = message;
        };

        $scope.infoMessage = function (message) {
            $scope.messages.info = message;
        };

        $scope.errorMessage = function (message) {
            $scope.messages.error = message;
        };

        $scope.getSettings = function () {
            const scrollable = {
                scrollableHeight: '200px',
                scrollable: true,
            };
            const noScroll = {
                scrollableHeight: '180px',
                scrollable: false,
            };
            $scope.settingsWithoutScroll = {
                ...noScroll,
                ...$scope.settings
            };

            $scope.settingsScroll = {
                ...scrollable,
                ...$scope.settings
            };

            $scope.settingsIdLabel = {
                ...$scope.settingsWithoutScroll,
                enableSearch: true
            };

        };

        $scope.init = function () {

            $scope.searchText = null;

            $scope.getSettings();
            lookThroughService
                .getTipoSocieta()
                .then(data => {
                    // map to {value: elValue} in order to display value in dropbox directive
                    $scope.tipoSocietaList = data
                        .map(el => ({ value: el.codice, label: el.nominativo }));
                    $scope.selectedTipoSocieta = $scope.tipoSocietaList.map(({ value }) => ({ id: value }));
                }, function (error) {
                    $scope.errorMessage('Qualcosa è andato storto.');
                });

            lookThroughService
                .getTipoDiritto()
                .then((data) => {
                    // map to {value: elValue} in order to display value in dropbox directive
                    $scope.tipoDirittoList = data
                        .map(el => ({ value: el, label: el }));
                }, function (error) {
                    $scope.errorMessage('Qualcosa è andato storto.');
                });

            statistichePostRipartizioneService
                .getListIdSiada()
                .then(function (success) { //id label autocomplete
                    // map to {value: elValue} in order to display value in dropbox directive
                    $scope.idSiadaList = success.data
                        .map(el => ({ value: el, label: el }));
                }, function (error) {
                    $scope.errorMessage('Qualcosa è andato storto.');
                });
            // get Requests List - reques is done with an interval of 30 sec to update data
            getRequestList();
            $scope.pollingRequestReport = $interval(getRequestList, 30000);

        }

        $scope.init();

        $scope.navigateToNextPage = function () {
            const first = $scope.queryParams.first + $scope.paging.maxRows;
            const last = $scope.queryParams.last + $scope.paging.maxRows;
            // update first, last value
            $scope.queryParams = {
                first,
                last
            };
            getRequestList();
        };

        $scope.navigateToPreviousPage = function () {
            const first = $scope.queryParams.first - $scope.paging.maxRows;
            const last = $scope.queryParams.last - $scope.paging.maxRows;
            // update first, last value
            $scope.queryParams = {
                first,
                last
            };
            getRequestList();
        };

        function getRequestList() {
            $scope.clearMessages();
            lookThroughService
                .getRequestList($scope.queryParams)
                .then(data => {
                    setTimeout(() => {
                        const { rows, first, last, hasNext, hasPrev } = data;

                        $scope.lookThroughDataTable = rows
                            .map((richiesta) => {
                                const stato = richiesta.stato.toLowerCase() === "in corso" ?
                                    'In lavorazione' :
                                    richiesta.stato.charAt(0).toUpperCase() + richiesta.stato.slice(1).toLowerCase();

                                return {
                                    ...richiesta,
                                    stato,
                                    tipoDiritto: richiesta.tipoDiritto.toString(),
                                    idLabel: richiesta.idLabel.toString(),
                                    dataCreazione: moment(richiesta.dataCreazione).format("YYYY-MM-DD HH:mm:ss"),
                                    ultimoAggiornamento: moment(richiesta.ultimoAggiornamento).format("YYYY-MM-DD HH:mm:ss")
                                }
                            });

                        if (!$scope.lookThroughDataTable.length) {
                            $scope.paging = { ...$scope.defaultPaging };
                            // $scope.paging.numberOfItems = 0;
                            $scope.infoMessage("Al momento non c'è uno storico richieste.");
                            return;
                        }

                        $scope.paging = {
                            ...$scope.paging,
                            firstRecord: first + 1,
                            lastRecord: last,
                            hasNext,
                            hasPrev
                        };
                        $scope.$apply();
                    }, 100);
                }, _error => {
                    $scope.errorMessage("Si è verificato un problema con lo storico richieste, riprovare più tardi.");
                });
        };

        $scope.generaRendiconto = function (userName) {
            $scope.clearMessages();
            if ($scope.selectedItem.length && ($scope.tipoDirittoSelected || []).length &&
                ($scope.selectedTipoSocieta || []).length && $scope.ripartizioneSiada) {

                const idLabel = $scope.selectedItem
                    .map(({ id }) => id);

                const request = {
                    idRipartizioneSiada: $scope.ripartizioneSiada,
                    idLabel,
                    tipoDiritto: $scope.tipoDirittoSelected.map(({ id }) => id),
                    societa: $scope.selectedTipoSocieta[0].id,
                    creatoDa: userName
                };
                lookThroughService.postReportRequest(request)
                    .then(_data => {
                        $scope.infoMessage("Richiesta effettuata con successo.");
                        // per aggiornare i dati della tabella.
                        getRequestList();
                    }, ({ data: { message } }) => {
                        $scope.errorMessage(message || "Si è verificato un problema, riprovare più tardi.");
                    });
            } else {
                $scope.errorMessage("Compilare tutti i valori richiesti.");
            }
        };

        $scope.$on("$destroy", () => {
            $interval.cancel($scope.pollingRequestReport);
        });


    }]);