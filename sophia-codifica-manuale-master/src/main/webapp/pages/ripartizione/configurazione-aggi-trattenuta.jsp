<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../navbar.jsp"%>
<!-- path has to be adjusted-->

<div class="bodyContents" ng-init="init()">
	<div class="mainContainer row-fluid">

		<div class="contents-topscroll noprint">
			<div class="topscroll-div" style="width: 95%;margin-bottom: 20px">&nbsp;</div>
		</div>

		<div class="sticky">
			<div id="companyLogo" class="navbar commonActionsContainer noprint">
				<div class="actionsContainer row-fluid">
					<div class="span2">
						<span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
					</div>
					<span class="btn-toolbar span5">
						<div class="pageNumbers alignTop pull-right" style="text-align: center;">
							<span id="up" class="pageNumbersText"><strong>{{ctrl.pageTitle}}</strong></span>
						</div>
					</span>
				</div>
			</div>
		</div>

		<div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px;margin-top: 25px;">
			<div class="listViewPageDiv">
				<div class="alert alert-warning" ng-show="messages.warning" style="margin-top:20px">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
					<span>{{messages.warning}}</span>
				</div>
				<div class="alert alert-danger" ng-show="messages.error" style="margin-top:20px">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
					<span><strong>Attenzione: </strong>{{messages.error}}</span>
				</div>
				<div class="alert alert-success" ng-show="messages.info" style="margin-top:20px">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
					<span>{{messages.info}}</span>
				</div>

				<!-- start table inserimento campi-->
				<div class="listViewActionsDiv row-fluid">
					<table class="table table-bordered blockContainer showInlineTable equalSplit">
						<thead>
							<tr>
								<th class="blockHeader" colspan="6" ng-show="ctrl.hideForm"><span
										ng-click="ctrl.hideForm=false"
										class="glyphicon glyphicon-expand"></span>&nbsp;Parametri
									Ricerca</th>
								<th class="blockHeader" colspan="6" ng-hide="ctrl.hideForm"><span
										ng-click="ctrl.hideForm=true"
										class="glyphicon glyphicon-collapse-down"></span>&nbsp;Parametri
									Ricerca</th>
							</tr>
						</thead>
						<tbody ng-hide="ctrl.hideForm">
							<tr>
								<td class="fieldLabel medium" style="width: 20%"><label
										class="muted pull-right marginRight10px" style="padding-top: 10px">
										Società </label></td>
								<td style="width: 80%">
									<div class="row-fluid">
										<span class="span10">
											<select name="mandante" id="mandante" ng-model="mandante"
												ng-options="item.nominativo for item in mandanti track by item.id"
												required>
											</select>
										</span>
									</div>
								</td>
							</tr>
							<tr>
								<td class="fieldLabel medium" style="text-align: right; width: 16.66% nowrap"
									colspan="6">
									<button class="btn addButton" ng-click="filter(mandante)">
										<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Applica</strong>
									</button>
									<button class="btn addButton" ng-click="mandante = {codice:''}; filter(mandante)" ng-show="mandante.codice">
											<span class="glyphicon glyphicon-erase"></span>&nbsp;&nbsp;<strong>Cancella</strong>
										</button>
									<button class="btn addButton pull-left" ng-click="newConfiguration()">
										<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;<strong>Nuovo</strong>
									</button>
								</td>
							</tr>
						</tbody>
					</table>
				</div> <!-- end table inserimento campi -->

				<!-- start table risultati -->
				<div class="listViewContentDiv" id="listViewContents">

					<div class="listViewEntriesDiv contents-bottomscroll">
						<div class="bottomscroll-div" style="width: 95%; min-height: 80px">
							<table class="table table-responsive">
								<thead>
									<tr class="listViewHeaders">
										<!-- PRIMA DI CAMBIARE LA CLASSE DELLO SPAN, CONTROLLARE LA LOGICA SUL CONTROLLER DI ANGULAR. -->
										<th style=" text-align: center;">Società </th>
										<th style=" text-align: center;">Periodo da </th>
										<th style=" text-align: center;">Periodo a </th>
										<th style=" text-align: center;">Aggio DEM </th>
										<th style=" text-align: center;">Aggio DRM </th>
										<th style=" text-align: center;">Trattenuta DEM </th>
										<th style="text-align: center;">Azioni</th>
									</tr>
								</thead>
								<tbody ng-repeat="item in configurations| orderBy: ['key','validFrom'] track by $index">
									<tr>
										<td style="text-align: center;">
											{{showName(item.key)}}</td>
										<td style="width:16.5; text-align: center;">
											{{item.validFrom | date: 'dd-MM-yyyy'}}</td>
										<td style=" text-align: center;">
											{{item.validTo | date: 'dd-MM-yyyy'}}</td>
										<td style=" text-align: center;">{{findKey(item.settings,'threshold.aggioDEM').value| number: 2}}&nbsp;%</span></td>
										<td style=" text-align: center;">{{findKey(item.settings,'threshold.aggioDRM').value| number: 2}}&nbsp;%</span></td>
										<td style=" text-align: center;">{{findKey(item.settings,'threshold.trattenutaDEM').value| number: 2}}&nbsp;%</span></td>
										<td style="text-align: center;">
												<a ng-click="deleteConfiguration(item)"><i title="Elimina" class="glyphicon glyphicon-trash "></i></a>&nbsp;
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- end table risultati -->
			</div>
		</div>
	</div>
</div>
<!--<pre>{{ctrl | json}}</pre>-->