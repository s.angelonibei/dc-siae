/**
 * ConfigurazionePricingCtrl
 * 
 * @path /pricing
 */
codmanApp.controller('ConfigurazionePricingCtrl', ['$scope', '$q', 'pricingService', '$location', 'ngDialog',
    function ($scope, $q, pricingService, $location, ngDialog) {

        $scope.configuration = {};
        $scope.messages = {
            info: "",
            error: ""
        };

        $scope.checkbox = [{
            label: 'Territory',
            checked: false
        }, {
            label: 'Countries',
            checked: false
        }];

        $scope.defaultTemplateRadio = 2;
        $scope.dwnTemplateRadio = [{
            label: 'Template a due parametri',
            checked: true,
            value: $scope.defaultTemplateRadio
        }, {
            label: 'Template a tre parametri',
            checked: false,
            value: 3
        }];

        $scope.dwnCheckboxOptions = [{
            label: 'Offerta commerciale',
            checked: false
        }, {
            label: 'Total sales',
            checked: false
        }];
        /* will have key: value based on label-checked of dwnCheckboxOptions */
        $scope.dwnCheckboxesInfo = {
            offertaCommerciale: false,
            totalSales: false
        };

        $scope.isRadioButtonVisible = function () {
            return $scope.utilizationSelected && $scope.utilizationSelected.key === pricingService.getStrmSubKey();
        }

        $scope.showDownloadCheckboxes = function () {
            return $scope.utilizationSelected && $scope.utilizationSelected.key === pricingService.getDwnKey();
        }

        $scope.onCheckboxOptionEvent = function () {
            $scope.dwnCheckboxesInfo = $scope.dwnCheckboxOptions
                .reduce((acc, { label, checked }) => {
                    const key = _.camelCase(label);
                    return Object.assign(acc, { [key]: checked });
                }, {});
        }

        $scope.dataInizioValidita = {
            type: 'date',
            required: false,
            binding: 'applicant.expectedGraduation',
            today: new Date(),
            startView: 'month',
            label: 'Data inizio validità',
            locale: buildLocaleProvider(pricingService.getDateFormat()),
        };

        $scope.dataFineValidita = {
            type: 'date',
            required: false,
            binding: 'applicant.expectedGraduation',
            today: new Date(),
            startView: 'month',
            label: 'Data fine validità',
            locale: buildLocaleProvider(pricingService.getDateFormat()),
        };

        $scope.infoMessage = function (message) {
            setTimeout(() => {
                $scope.messages.info = message;
                $scope.$apply();
            }, 100);
        };

        $scope.errorMessage = function (message) {
            setTimeout(() => {
                $scope.messages.error = message;
                $scope.$apply();
            }, 100);
        };

        $scope.clearMessages = function () {
            $scope.messages = {
                info: "",
                error: ""
            };
        };

        $scope.onInit = function () {
            $scope.dateFormat = pricingService.getDateFormat();
            pricingService.getDspUtilizationsCommercialOffersCountries()
                .then(({ dsp }) => {
                    $scope.setDspList(dsp);
                });

            pricingService.getIdUtilizationType()
                .then(data => {
                    $scope.idUtilizationType = Object.entries(data).map(([k, v]) => ({ key: k, value: v }));
                    pricingService.getPreloadConfig()
                        .then(conf => {
                            if (conf) {
                                $scope.disableHeaderForm = true;
                                pricingService.getDspUtilizationsCommercialOffersCountries()
                                    .then(({ dsp }) => { //to ensure dspList has being loaded
                                        setTimeout(() => {
                                            $scope.setDspList(dsp);
                                            $scope.utilizationSelected = $scope.idUtilizationType.find(({ key }) => key === conf.tipoUtilizzo);
                                            $scope.dspSelected = $scope.dspList.find(dsp => dsp.idDsp === conf.idDsp);
                                            $scope.initPricingData(true);
                                            $scope.$apply();
                                        }, 100);
                                    });
                            }
                        }, _error => {
                            $scope.errorMessage("Qualcosa è andato storto.");
                        });
                }, _error => {
                    $scope.errorMessage("Qualcosa è andato storto.");
                });
        };

        $scope.onInit();

        $scope.setDspList = function (list) {
            if (!$scope.dspList) {
                $scope.dspList = list;
            }
        };


        $scope.navigateToDrool = function () {
            setTimeout(function () {
                $location.path("/droolsPricing");
                $scope.$apply();
            }, 700);
        };

        $scope.salva = function () {
            console.log($scope.configuration);
            if (!$scope.importForm.$valid) {
                $scope.markFormAsTouched($scope.importForm);
                return;
            }
            const dataToSend = _.omitBy($scope.buildBodyRequest(), (e) => _.isNil(e) || e === "");
            pricingService.addPricingConfiguration(dataToSend)
                .then((data) => {
                    $scope.infoMessage("Salvataggio avvenuto con successo.");
                    $scope.navigateToDrool();
                },
                    ({ data: errorResponse }) => {
                        const { message, status } = errorResponse;
                        if (status === 500) {
                            const { dspSelected, utilizationSelected } = $scope;
                            if (message.includes("row-duplicate")) {
                                const dataInizioValidita = $scope.dataInizioValidita.selectedDate;
                                const data = {
                                    tableHeader: ["Dsp", "Tipo Utilizzo", "Data Inizio Validita"],
                                    tableData: {
                                        dspSelected: dspSelected.name,
                                        utilizationSelected: utilizationSelected.value,
                                        dataInizioValidita
                                    },
                                    subTitle: "Impossibile creare la pricilist perche esiste gia una configurazione che ha come data inizio quella selezionata.",
                                };

                                ngDialog.open({
                                    template: 'pages/configurazione-pricing/popup/update-old-conf-confirmation.html',
                                    width: '60%',
                                    className: 'ngdialog-theme-default dialogOverflow',
                                    controller: ['$scope', 'ngDialog', UpdateOldConfirmationDialogController],
                                    data
                                });
                            }
                            else if (message.includes("row-already-exists")) {
                                const data = {
                                    tableHeader: ["Dsp", "Tipo Utilizzo"],
                                    tableData: {
                                        dspSelected: dspSelected.name,
                                        utilizationSelected: utilizationSelected.value
                                    },
                                    subTitle: "Le date della configurazione si sovrappongono con una già esistente",
                                    message: "Si vuole aggiornare la data fine validità della vecchia configurazione?",
                                    actionRequired: true
                                };

                                const idDspListString = (message.match(/\[(.*?)\]/) || ["", ""])[1]
                                    .split(",");
                                ngDialog.open({
                                    template: 'pages/configurazione-pricing/popup/update-old-conf-confirmation.html',
                                    width: '60%',
                                    className: 'ngdialog-theme-default dialogOverflow',
                                    controller: ['$scope', 'ngDialog', UpdateOldConfirmationDialogController],
                                    data
                                }).closePromise
                                    .then(({ value }) => {
                                        if (value && typeof value !== "string") {
                                            const validTo = moment(dataToSend.dataInizioValidita, pricingService.getDateFormat())
                                                .subtract(1, "days")
                                                .format(pricingService.getDateFormat());
                                            const promises = idDspListString.map(id => {
                                                return pricingService.updateConfigurationDates(id, { validTo });
                                            });
                                            $q.all(promises)
                                                .then((results) => {
                                                    console.log(results);
                                                    pricingService.addPricingConfiguration(dataToSend)
                                                        .then((data) => {
                                                            $scope.infoMessage("Salvataggi avvenuti con successo.");
                                                            $scope.navigateToDrool();
                                                        });
                                                }, (_error) => {
                                                    $scope.errorMessage("Qualcosa è andato storto.");
                                                });

                                        }
                                    });
                            } else {
                                $scope.errorMessage("Salvataggio non riuscito.");
                            }
                        }
                    });
        };

        $scope.buildBodyRequest = function () {
            // add checkbox info into request for Download utilization
            const dwnKeys = $scope.showDownloadCheckboxes() ? {
                ...$scope.dwnCheckboxesInfo,
                numParametri: $scope.dwnTemplateSelectedValue
            } : {};

            return {
                dsp: $scope.dspSelected.idDsp,
                dataInizioValidita: moment($scope.dataInizioValidita.selectedDate).format(pricingService.getDateFormat()),
                ...dwnKeys,
                dataFineValidita: $scope.dataFineValidita.selectedDate ?
                    moment($scope.dataFineValidita.selectedDate).format(pricingService.getDateFormat()) :
                    null,
                tipoUtilizzo: $scope.utilizationSelected.key,
                recordDettaglio: $scope.mapConfigurationData(),
                checkboxSelezionata: $scope.isRadioButtonVisible() ? $scope.checkboxSelectedValue : null
            }
        };

        $scope.mapConfigurationData = function () {
            const siaeMinimumAlbumKey = _.camelCase(pricingService.getSiaeMinimumAlbum());
            const recordDettaglio = $scope.configuration.slice();
            if ($scope.utilizationSelected.key === pricingService.getDwnKey()) {

                recordDettaglio.forEach((record, i) => {
                    // if utilization = dwn, need to filter record based on selected checkboxes
                    if ($scope.utilizationSelected.key === pricingService.getDwnKey()) {
                        const toExclude = Object.entries($scope.dwnCheckboxesInfo || {})
                            .filter(([key, value]) => !value)
                            .map(([key, value]) => key);
                        record = _.omit(record, toExclude);
                        recordDettaglio[i] = record;
                    }

                    const siaeMinimumAlbumList = Object.entries(record)
                        .filter(([k, _v]) => k.includes(siaeMinimumAlbumKey));
                    if (siaeMinimumAlbumList.length) {
                        // keys to exlude
                        const keyToExclude = siaeMinimumAlbumList.map(([key, _v]) => key);
                        record = _.omit(record, keyToExclude);
                        // map all values into unique {siaeMinimumAlbum : ["values"]}
                        record[siaeMinimumAlbumKey] = siaeMinimumAlbumList.map(([_k, v]) => v);
                        recordDettaglio[i] = record;
                    }
                });
            }

            return recordDettaglio;
        };

        $scope.importPricing = function () {
            $scope.markFormAsTouched($scope.importForm);
            if ($scope.importForm.$valid) {
                const { dspSelected, utilizationSelected, dataInizioValidita, dataFineValidita } = $scope;
                const configToImport = {
                    dsp: dspSelected.idDsp,
                    tipoUtilizzo: utilizationSelected.key,
                    dataInizioValidita: moment(dataInizioValidita.selectedDate).format(pricingService.getDateFormat()),
                    dataFineValidita: $scope.dataFineValidita.selectedDate ?
                        moment($scope.dataFineValidita.selectedDate).format(pricingService.getDateFormat()) :
                        null,
                };
                const data = {
                    tableHeader: ["Dsp", "Tipo Utilizzo", "Data inizio validità", "Data fine validità"],
                    tableData: configToImport
                };

                ngDialog.open({
                    template: 'pages/configurazione-pricing/popup/import-confirmation.html',
                    width: '60%',
                    className: 'ngdialog-theme-default dialogOverflow',
                    controller: ['$scope', 'ngDialog', ImportConfirmationDialogController],
                    data
                }).closePromise
                    .then(({ value }) => {
                        if (value && typeof value !== "string") {
                            if (dspSelected && utilizationSelected) {
                                const checkboxOptions = utilizationSelected.key === pricingService.getStrmSubKey() ?
                                    { checkboxSelezionata: $scope.isRadioButtonVisible() ? $scope.checkboxSelectedValue : null } :
                                    {};
                                const loadDataFrom = {
                                    idDsp: dspSelected.idDsp,
                                    tipoUtilizzo: utilizationSelected.key,
                                    ...checkboxOptions
                                }
                                pricingService.setPreloadConfig(_.omitBy(loadDataFrom, (e) => _.isNil(e) || e === ""));
                                $scope.initPricingData(false);
                            };

                        }
                    });
            }
        };

        $scope.initPricingData = function (editMode) {
            pricingService.importPricing()
                .then((pricingData) => {
                    setTimeout(() => {
                        if (Object.keys(pricingData).length) {
                            $scope.data = pricingData;
                            if (pricingData.tipoUtilizzo === pricingService.getStrmSubKey()) {
                                $scope.checkboxSelectedValue = pricingData.checkboxSelezionata;
                            } else if (pricingData.tipoUtilizzo === pricingService.getDwnKey()) {
                                $scope.dwnTemplateSelectedValue = pricingData.numParametri || $scope.defaultTemplateRadio;
                                $scope.dwnCheckboxOptions[0].checked = !!pricingData.offertaCommerciale;
                                $scope.dwnCheckboxOptions[1].checked = !!pricingData.totalSales;
                                $scope.onCheckboxOptionEvent();
                            }
                            if (editMode) {
                                $scope.dataInizioValidita.selectedDate = pricingData.dataInizioValidita ? new Date(moment(pricingData.dataInizioValidita, pricingService.getDateFormat())) : null;
                                if (pricingData.dataFineValidita) {
                                    $scope.dataFineValidita.selectedDate = new Date(moment(pricingData.dataFineValidita, pricingService.getDateFormat()));
                                }
                            } else { //import 
                                $scope.dataInizioValidita.selectedDate = new Date();
                            }
                        }
                        else {
                            $scope.infoMessage("Non è stata trovata una pricelist da importare con i parametri inseriti.");
                        }
                        $scope.$apply();
                    }, 100);
                });
        };

        $scope.markFormAsTouched = function (formController) {
            formController.$$controls.forEach(child => {
                child && child.$setTouched();
            });
        };

        $scope.onUtilizationSelection = function () {
            if ($scope.utilizationSelected.key === pricingService.getStrmSubKey()) {
                $scope.checkboxSelectedValue = $scope.checkbox[0].label;
            } else if ($scope.utilizationSelected.key === pricingService.getDwnKey()) {
                $scope.dwnTemplateSelectedValue = $scope.dwnTemplateRadio[0].value;
            }

            $scope.utilizationSelectedToBind = _.clone($scope.utilizationSelected);
        };

        function buildLocaleProvider(formatString) {
            return {
                formatDate: function (date) {
                    return date ?
                        moment(date).format(formatString) :
                        null;
                },
                parseDate: function (dateString) {
                    if (dateString) {
                        return moment(dateString).isValid() ? moment(dateString).toDate() : new Date(NaN);
                    }
                    return null;
                }
            };
        };
    }
]);