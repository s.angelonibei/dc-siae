codmanApp
    .component('pricingConfigurationTemplates', {
        templateUrl: 'pages/configurazione-pricing/templates/pricing-configuration-templates.html',
        controller: ['$scope', 'pricingService', function pricingConfigurationTeamplateCtrl($scope, pricingService) {
            const ctrl = this;
            ctrl.headerWithButton = pricingService.getSiaeMinimumAlbum();
            /* data model ->
            *   dataInizioValidita
            *   recordDettaglio: []
            *   dsp
            *   tipoUtilizzo
            *   dataFineValidita
            * */
            ctrl.$onChanges = function (changesObj) {

                const { dwnCheckboxesInfo = null,
                    utilizationSelected = null,
                    checkboxSelected = null,
                    data = null
                } = changesObj;

                if (utilizationSelected && utilizationSelected.currentValue) { // new configuration
                    ctrl.configurationList = [];
                    ctrl.setUpPageData();
                } else if (data && data.currentValue) { // update
                    ctrl.utilizationSelected = { key: data.currentValue.tipoUtilizzo };
                    ctrl.setUpPageData(data);
                } else if (checkboxSelected && checkboxSelected.currentValue) {
                    ctrl.configurationList = [];
                    ctrl.setUpPageData();
                } else if (dwnCheckboxesInfo && dwnCheckboxesInfo.currentValue) { // dwn changes
                    ctrl.setUpPageData();
                }

            };

            ctrl.$onInit = function () {
                ctrl.configurationList = [];
            };

            ctrl.setUpPageData = function (data = null) {
				const utilizationSelected = ctrl.utilizationSelected || {};
                let objConf = { ...utilizationSelected };
				const {key = ''} = utilizationSelected;
                if (key === pricingService.getStrmSubKey()) {
                    _.assign(objConf, { checkboxValue: ctrl.checkboxSelected });
                }
                pricingService.getConfigurationTemplate(objConf)
                    .then(settings => {
                        ctrl.headerTitles = _.clone(settings.headerTitles);
                        ctrl.recordKeys = _.clone(settings.columnKeys);
                        ctrl.tBodyColumns = ctrl.recordKeys ? Object.keys(ctrl.recordKeys) : [];

                        if (key === pricingService.getDwnKey()) {
                            const settingToAdd = Object.keys(_.omitBy(ctrl.dwnCheckboxesInfo, (e) => !e));
                            setTimeout(() => {
                                ctrl.tBodyColumns.splice(2, 0, ...settingToAdd);
                                ctrl.headerTitles.splice(2, 0, ...settingToAdd.map(setting => _.startCase(setting)));
                                $scope.$apply();
                            }, 100);
                        }

                        ctrl.recordDettaglio = ctrl.configurationList;

                        if (data && data.currentValue) {
                            ctrl.updateValues(data.currentValue);
                        }
                    });
            };

            ctrl.notEmptyObject = function (obj) {
                return !!Object.values(obj).find(x => x);
            };

            ctrl.showColumnButton = function (header) {
                return header === ctrl.headerWithButton;
            };

            ctrl.showConfInput = function (col) {
                return col === _.camelCase(ctrl.headerWithButton);
            };

            ctrl.updateValues = function (data) {
                const recordDettaglio = _.clone(data.recordDettaglio || []);
                // siae minimum album is an array, map as obj with diff keys
                siaeMinAlbKey = _.camelCase(ctrl.headerWithButton);
                // if sia min alb
                if (Array.isArray(recordDettaglio) && recordDettaglio.length > 0 && Object.keys(recordDettaglio[0]).includes(siaeMinAlbKey)) {
                    let maxLenght = 0;
                    recordDettaglio.forEach(record => {
                        if (record[siaeMinAlbKey].length > maxLenght) {
                            // calculate the max number of items for siae min alb
                            maxLenght = record[siaeMinAlbKey].length;
                        }
                    });
                    if (maxLenght) {
                        // for each row, add column to table and mng record
                        for (i = 0; i < maxLenght - 1; i++) {
                            ctrl.addColumn(ctrl.headerWithButton);
                        }
                        recordDettaglio.forEach(record => {
                            // split value into different columns
                            const arrayToObj = record[siaeMinAlbKey].reduce((acc, v, i) => {
                                const index = i > 0 ? i : '';
                                const key = siaeMinAlbKey + index;
                                return { [key]: v, ...acc };
                            }, {});
                            record = _.merge(record, arrayToObj);
                        });
                    }
                }
                // to copy values into html controllers
                console.log("update configurationList", recordDettaglio);
                setTimeout(() => {
                    ctrl.configurationList = _.clone(recordDettaglio);
                    ctrl.recordDettaglio = ctrl.configurationList;
                    $scope.$apply();
                }, 100);
            };

            // header is the name of column table
            ctrl.addColumn = function (header) {
                // index of header, will be used to add element next to it
                const index = ctrl.headerTitles.indexOf(header) + 1;
                ctrl.headerTitles.splice(index, 0, header);
                // add input field column
                const lastIndex = _.findLastIndex(ctrl.tBodyColumns, (x) => x.includes(_.camelCase(header)));
                // last index of minumAlbum column + 1
                const colIndex = parseInt(ctrl.tBodyColumns[lastIndex].replace(/[a-z]/gi, '') || 0) + 1;
                const newColName = `${_.camelCase(header)}${colIndex}`;
                // add new field to single Record (to add new line with all fields)
                ctrl.recordKeys.newColName = "";
                // add new field to update numbers of body columns into table
                ctrl.tBodyColumns.splice(lastIndex + 1, 0, newColName);
            };

            ctrl.addNewLine = function (row = null) {
                ctrl.configurationList.push(_.clone(row || ctrl.recordKeys));
            };

            ctrl.removeLine = function (index) {
                _.pullAt(ctrl.configurationList, index);
            };

        }],
        bindings: {
            recordDettaglio: '=', // configuration to save
            utilizationSelected: '<', // utilization from dropdown
            checkboxSelected: '<',
            dwnCheckboxesInfo: '<',
            data: '<' // data to load from importPricing
        }
    });