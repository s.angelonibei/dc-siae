<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <%@ include file="../navbar.jsp" %>

        <div class="bodyContents">

            <div class="row-fluid">

                <div class="contents-topscroll noprint">
                    <div class="topscroll-div" style="width: 95%; margin-bottom: 20px">&nbsp;</div>
                </div>

                <div class="sticky">
                    <div id="companyLogo" class="navbar commonActionsContainer noprint">
                        <div class="actionsContainer row-fluid">
                            <div class="span2">
                                <span class="companyLogo">
                                    <img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;
                                </span>
                            </div>
                            <span class="btn-toolbar span8">
                                <span class="customFilterMainSpan btn-group" style="text-align: center;">
                                    <div class="pageNumbers alignTop ">
                                        <span class="pageNumbersText">
                                            <strong style="text-transform: uppercase;">Configurazione Pricing</strong>
                                        </span>
                                    </div>
                                </span>
                            </span>
                        </div>
                    </div>
                </div>

                <md-card id="pricingContainer" md-theme="{{ showDarkTheme ? 'dark-grey' : 'default' }}" md-theme-watch>
                    <md-content style="overflow: hidden;">
                        <!-- MESSAGES CONTAINERS -->
                        <div class="alert alert-danger" ng-show="messages.error && (!messages.info)">
                            <button type="button" class="close" ng-click="clearMessages()" aria-hidden="true">x</button>
                            <span><strong>Errore: </strong>{{messages.error}}</span>
                        </div>
                        <div class="alert alert-success" ng-show="messages.info && (!messages.error)">
                            <button type="button" class="close" ng-click="clearMessages()" aria-hidden="true">x</button>
                            <span>{{messages.info}}</span>
                        </div>
                        <!-- HEADER CONTAINER -->
                        <form name="importForm" class="flexColumnCenterStart" novalidate style="background-color: #e7e7e7 ; border: 1px solid #ccc">
                            <div ng-show="showDownloadCheckboxes()" class="flexRowHNoneVCenter dwnCheckboxes">
                                <md-input-container>
                                    <md-checkbox ng-model="dwnCheckboxOptions[0].checked" ng-change="onCheckboxOptionEvent()">
                                        {{ dwnCheckboxOptions[0].label}}
                                    </md-checkbox>
                                    <md-checkbox ng-model="dwnCheckboxOptions[1].checked" ng-change="onCheckboxOptionEvent()">
                                        {{ dwnCheckboxOptions[1].label}}
                                    </md-checkbox>
                                </md-input-container>

                            </div>
                            <div id="headerContainer pricingCustom" class="flexRowCenterSpaceEvenly">
                                <md-input-container class="md-block"
                                    style="flex: 1 1 100%; box-sizing: border-box; max-width: 15%;">
                                    <h3>DSP</h3>
                                    <md-select name="dsp" ng-model-options="{ trackBy: '$value.idDsp' }"
                                        ng-model="dspSelected" aria-label="label" ng-disabled="disableHeaderForm"
                                        required md-no-asterisk>
                                        <md-option ng-repeat="dsp in dspList" ng-value="{{dsp}}">
                                            {{ dsp.name }}
                                        </md-option>
                                    </md-select>
                                    <div ng-show="importForm.dsp.$touched && importForm.dsp.$invalid"
                                        class="error md-input-messages-animation">Campo obbligatorio.</div>
                                </md-input-container>
                                <md-input-container class="md-block"
                                    style="flex: 1 1 100%; box-sizing: border-box; max-width: 15%;">
                                    <h3>Tipo Utilizzo</h3>
                                    <md-select name="societaTutela" ng-model-options="{ trackBy: '$value.key' }"
                                        ng-model="utilizationSelected" aria-label="select"
                                        ng-disabled="disableHeaderForm" ng-change="onUtilizationSelection()" required
                                        md-no-asterisk>
                                        <md-option ng-repeat="utilization in idUtilizationType"
                                            ng-value="{{utilization}}">
                                            {{ utilization.value }}
                                        </md-option>
                                    </md-select>
                                    <div ng-show="importForm.societaTutela.$touched && importForm.societaTutela.$invalid"
                                        class="error md-input-messages-animation">Campo obbligatorio.</div>
                                </md-input-container>
                                <md-input-container layout="column">
                                    <h3>Data Inizio Validit&agrave</h3>
                                    <md-datepicker ng-disabled="disableHeaderForm"
                                        ng-model="dataInizioValidita.selectedDate"
                                        ng-required="dataInizioValidita.required"
                                        md-date-locale="dataInizioValidita.locale" md-mode="day" md-open-on-focus="true"
                                        aria-label="datapickerarialabel">
                                    </md-datepicker>
                                </md-input-container>
                                <md-input-container layout="column">
                                    <h3>Data Fine Validit&agrave</h3>
                                    <md-datepicker ng-disabled="disableHeaderForm"
                                        ng-model="dataFineValidita.selectedDate" ng-required="dataFineValidita.required"
                                        md-date-locale="dataFineValidita.locale" md-mode="day" md-open-on-focus="true"
                                        aria-label="datapicker2arialabel">
                                    </md-datepicker>
                                </md-input-container>
                                <div ng-show="isRadioButtonVisible()" class="checkboxesContainer" >

                                    <md-radio-group ng-disabled="disableHeaderForm" ng-model="checkboxSelectedValue"
                                        class="flexColumnCenterStart">

                                        <md-radio-button aria-label="aria-label1" ng-value="checkbox[0].label" class="md-primary">
                                            <label for="in-front" aria-hidden="true" tabindex="-1" label
                                                ng-bind="::checkbox[0].label">
                                            </label>
                                        </md-radio-button>
                                        <md-radio-button aria-label="aria-label2" ng-value="checkbox[1].label">
                                            <label for="label-in-front" aria-hidden="true" tabindex="-1" label
                                                ng-bind="::checkbox[1].label">
                                            </label>
                                        </md-radio-button>

                                    </md-radio-group>

                                </div>
                                <div ng-show="showDownloadCheckboxes()" class="checkboxesContainer">

                                    <md-radio-group ng-model="dwnTemplateSelectedValue"
                                        class="flexColumnCenterStart">

                                        <md-radio-button aria-label="aria-label3" ng-value="dwnTemplateRadio[0].value" class="md-primary">
                                            <label for="in-front" aria-hidden="true" tabindex="-1" label
                                                ng-bind="::dwnTemplateRadio[0].label">
                                            </label>
                                        </md-radio-button>
                                        <md-radio-button aria-label="aria-label4" ng-value="dwnTemplateRadio[1].value">
                                            <label for="label-in-front" aria-hidden="true" tabindex="-1" label
                                                ng-bind="::dwnTemplateRadio[1].label">
                                            </label>
                                        </md-radio-button>

                                    </md-radio-group>

                                </div>
                            </div>
                        </form>
                        <!-- HEADER BUTTON -->
                        <div style="flex-direction: column;
                        box-sizing: border-box;
                        display: flex;
                        place-content: flex-end center;
                        align-items: flex-end;
                        margin: 8px;">
                            <div>
                                <button class="btn" type="submit" ng-click="importPricing()">
                                    <span class="glyphicon glyphicon-import"></span>
                                    Importa Pricing
                                </button>
                                <button class="btn addButton" ng-click="salva(configuration)"
                                    style="float: right; margin: 0px 8px;">
                                    <span class="glyphicon glyphicon-save"></span>
                                    Salva
                                </button>
                            </div>
                        </div>
                        <!-- page Container -->
                        <div class="flexRowHCenterVCenter" id="pricingConfigurationContainer">
                            <pricing-configuration-templates record-dettaglio="configuration"
                                utilization-selected="utilizationSelectedToBind" data="data"
                                checkbox-selected="checkboxSelectedValue" dwn-checkboxes-info="dwnCheckboxesInfo">
                            </pricing-configuration-templates>
                        </div>
                    </md-content>
                </md-card>
            </div>
        </div>