/* 
 * input 
 * tableHeader, tableData
 */
function UpdateOldConfirmationDialogController($scope, ngDialog) {
    const { id } = $scope.ngDialogId;
    const { tableData } = $scope.ngDialogData;
    $scope.data = Object.values(tableData)
        .map((value) => (value));

    $scope.getTableHeader = function () {
        const { tableHeader } = $scope.ngDialogData;
        return tableHeader;
    };

    $scope.getTableData = function () {
        return $scope.data;
    };

    $scope.getSubtitle = function () {
        const { subTitle } = $scope.ngDialogData;
        return subTitle;
    };

    $scope.getMessage = function () {
        const { message } = $scope.ngDialogData;
        return message;
    };

    $scope.getActionRequired = function () {
        const { actionRequired = false } = $scope.ngDialogData;
        return actionRequired;
    }

    $scope.cancel = function () {
        ngDialog.close(id);
    };

    $scope.confirm = function () {
        ngDialog.close(id, { confirm: true });
    };

}
