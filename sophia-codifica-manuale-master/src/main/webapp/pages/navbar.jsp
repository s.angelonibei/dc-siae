<%@ page import="com.google.inject.Injector" %><%@
	page import="com.google.inject.Key" %><%@
	page import="com.google.inject.name.Names" %><%@
	page language="java" contentType="text/html; charset=UTF-8"
    	pageEncoding="UTF-8"%><%
final Injector injector = (Injector) pageContext.getServletContext().getAttribute(Injector.class.getName());
final String userName = (String) session.getAttribute("sso.user.userName");
final String homeUrl = injector.getInstance(Key.get(String.class, Names.named("home.url")));
%>
<div>
  <md-toolbar class="md-menu-toolbar">
    <div layout="row">
      <div>
        <md-menu-bar  class="navbar navbar-fixed-top navbar-inverse noprint" >
      	  <md-icon class="flax2">
          	<a class="alignMiddle" href="<%= homeUrl %>"><img class="iconhome" src="images/home.png" alt="Home" title="Home"></a>
          </md-icon>
          <%   if (null != session.getAttribute("guMultimediale")) { %>
          <md-menu>
            <a class="flax1" ng-click="$mdMenu.open()" >
             <strong>Multimediale&nbsp;<i class="caret"></i></strong>
            </a>
			<md-menu-content class="menu-content-background">
				<%   if (null != session.getAttribute("muConfigurazione")) { %>
				<md-menu-item>
					<md-menu>
						<md-button >Attività</md-button>
						<md-menu-content class="menu-content-background">
							<%   if (null != session.getAttribute("preparatoryActivities")) { %>
							<md-menu-item>	<label class="moduleNames"><a href="#!/preparatoryActivities">Attività propedeutiche</a></label></md-menu-item>
							<%   } %>
							<%   if (true) { %>
							<md-menu-item>	<label class="moduleNames"><a href="#!/unidentifiedLoading">Caricamento non identificato e codificato</a></label></md-menu-item>
							<%   } %>
							<%   if (null != session.getAttribute("deliverCcid")) { %>
							<md-menu-item>	<label class="moduleNames"><a href="#!/deliverCcid">Invio CCID</a></label></md-menu-item>
							<%   } %>
							<%   if (null != session.getAttribute("lavorazioneDSR")) { %>
							<md-menu-item>	<label class="moduleNames"><a href="#!/lavorazioneDSR">Lavorazione DSR</a></label></md-menu-item>
							<%   } %>
							<%   if (null != session.getAttribute("dsrProgressSearch")) { %>
							<md-menu-item>	<label class="moduleNames"><a href="#!/dsrProgressSearch">DSR Progress</a></label></md-menu-item>
							<%   } %>
							<%   if (true) { %>
							<md-menu-item>	<label class="moduleNames"><a href="#!/controlloClaim">Controllo % Claim</a></label></md-menu-item>
							<%   } %>
							<%   if (true) { %>
								<md-menu-item>	<label class="moduleNames"><a href="#!/dashboard-dsp">Dashboard DSP</a></label></md-menu-item>
							<%   } %>
						</md-menu-content>
					</md-menu>
				</md-menu-item>
				<%   } %>
				 <%   if (null != session.getAttribute("muRiconoscimento")) { %>
			     <md-menu-item>
	                <md-menu>
	                  <md-button >Riconoscimento</md-button>
	                  <md-menu-content class="menu-content-background">
	                  	<%   if (null != session.getAttribute("dspStatistics")) { %>
	            			<md-menu-item>	<label class="moduleNames"><a href="#!/dspStatistics">Statistiche DSP</a></label></md-menu-item>
	                   	<%   } %>

						<%  if (null != session.getAttribute("riconoscimento")) { %>
			           	 	<md-menu-item>
								<md-menu>									
									<md-button >
										Riconoscimento
									</md-button>
									<md-menu-content class="menu-content-background">
										<md-menu-item>
											<label class="moduleNames">
												<a href="#!/riconoscimento">Non Identificato</a>
											</label>
										</md-menu-item>
										<md-menu-item>
											<label class="moduleNames">
												<a href="#!/scodifica-ricodifica">Scodifica e ri-codifica</a>
											</label>
										</md-menu-item>
									</md-menu-content>
								</md-menu>
							</md-menu-item>
			            <%   } %>
			            <%  if (null != session.getAttribute("ricerca")) { %>
			           	 	<md-menu-item>	<label class="moduleNames"><a href="#!/ricerca">Ricerca opere non identificate</a></label></md-menu-item>
						<%   } %>
						<%  if (null != session.getAttribute("ricerca")) { %>
							<md-menu-item>	<label class="moduleNames"><a href="#!/ricercaCodificato">Ricerca opere codificate</a></label></md-menu-item>
						<%   } %>
			            <%  if (null != session.getAttribute("knowledgeBase")) { %>
			           	 	<md-menu-item>	<label class="moduleNames"><a href="#!/knowledgeBase">Knowledge Base</a></label></md-menu-item>
						<%   } %>
						<%   { %>
							<md-menu-item>	<label class="moduleNames"><a href="#!/marketShare">Market Share</a></label></md-menu-item>
						<%   } %>
						<%   { %>
							<md-menu-item>	<label class="moduleNames"><a href="#!/royaltiesSearch">Schemi di Riparto</a></label></md-menu-item>
						<%   } %>		
						<%   { %>
							<md-menu-item>	<label class="moduleNames"><a href="#!/codificato">Ingestion Codificato SOPHIA</a></label></md-menu-item>
						<%   } %>					
	                  </md-menu-content>
	                </md-menu>
	             </md-menu-item>
				 <%   } %>
				 <%   if (null != session.getAttribute("muFatturazione")) { %>
	             <md-menu-item>
	                <md-menu>
	                  <md-button >Fatturazione</md-button>
	                  <md-menu-content class="menu-content-background">
						<%   if (null != session.getAttribute("anagClient")) { %>
	                  	 	<md-menu-item>	<label class="moduleNames"><a href="#!/anagClient">Gestione Clienti</a></label></md-menu-item>
	                    <%   } %>
						<%   if (null != session.getAttribute("invoices")) { %>
	                   	 	<md-menu-item>	<label class="moduleNames"><a href="#!/invoices">Ricerca fatture</a></label></md-menu-item>
	                    <%   } %>
	                    <%   if (null != session.getAttribute("createInvoice")) { %>
	                   	 	<md-menu-item>	<label class="moduleNames"><a href="#!/createInvoice">Nuova fattura</a></label></md-menu-item>
	                    <%   } %>
	                    <%   if (null != session.getAttribute("anticipi")) { %>
	                   	 	<md-menu-item>	<label class="moduleNames"><a href="#!/advancePayment">Anticipi</a></label></md-menu-item>
	                    <%   } %>
	                    <%   if (null != session.getAttribute("aggValFatt")) { %>
	                   	 	<md-menu-item>	<label class="moduleNames"><a href="#!/updateCcid">Aggiornamento valore fatturabile</a></label></md-menu-item>
	                    <%   } %>
	                </md-menu>
	             </md-menu-item>
				 <%   } %>
				 <%   if (null != session.getAttribute("muConfigurazione")) { %>
				 <md-menu-item>
	               <md-menu>
	                 <md-button >Configurazione</md-button>
	                 <md-menu-content class="menu-content-background">
						 <%   if (null != session.getAttribute("mandatoConfig")) { %>
							 <md-menu-item>	<label class="moduleNames"><a href="#!/mandatoConfig">Gestione Mandati</a></label></md-menu-item>
						 <%   } %>
	                   <%   if (null != session.getAttribute("dsrConfig")) { %>
	                  	 	<md-menu-item>	<label class="moduleNames"><a href="#!/dsrConfig">Configurazione DSR</a></label></md-menu-item>
	                   <%   } %>
	                   <%   if (null != session.getAttribute("dspConfig")) { %>
	                  	 	<md-menu-item>	<label class="moduleNames"><a href="#!/dspConfig">Configurazione DSP</a></label></md-menu-item>
	                   <%   } %>
	                   <%   if (null != session.getAttribute("commercialOffersConfig")) { %>
	                  	 	<md-menu-item>	<label class="moduleNames"><a href="#!/commercialOffersConfig">Offerte Commerciali</a></label></md-menu-item>
	                   <%   } %>
	                   <%   if (null != session.getAttribute("processManagement")) { %>
	                  	 	<md-menu-item>	<label class="moduleNames"><a href="#!/processManagement">Gestione Processi</a></label></md-menu-item>
	                   <%   } %>
	                   <%   if (null != session.getAttribute("anagDspConfig")) { %>
	                  	 	<md-menu-item>	<label class="moduleNames"><a href="#!/anagDspConfig">Gestione DSP</a></label></md-menu-item>
	                   <%   } %>
	                   <%   if (null != session.getAttribute("dsrMetadataConfig")) { %>
	                  	 	<md-menu-item>	<label class="moduleNames"><a href="#!/dsrMetadata">DSR Metadata</a></label></md-menu-item>
	                   <%   } %>
					   <%   if (null != session.getAttribute("ccidConfig")) { %>
	                  	 	<md-menu-item>	<label class="moduleNames"><a href="#!/ccidConfig">Configurazione CCID</a></label></md-menu-item>
	                   <%   } %>
	                   <%   if (null != session.getAttribute("droolsPricing")) { %>
	                  	 	<md-menu-item>	<label class="moduleNames"><a href="#!/droolsPricing">Tabelle Pricing</a></label></md-menu-item>
	                   <%   } %>
	                   <%   if (null != session.getAttribute("blacklist")) { %>
	                  	 	<md-menu-item>	<label class="moduleNames"><a href="#!/blacklist">Blacklist</a></label></md-menu-item>
					   <%   } %>
					   <%   if (null != session.getAttribute("mandatoConfig")) { %>
						<md-menu-item>	<label class="moduleNames"><a href="#!/documentazioneMandanti">Documentazione Mandanti</a></label></md-menu-item>
						<%   } %>
						<%   if (true) { %>
							<md-menu-item>	<label class="moduleNames"><a href="#!/mailAlerts">Invio Mail</a></label></md-menu-item>
						<%   } %>						
						 <%   if (null != session.getAttribute("configureDeliverCcid")) { %>
						 <md-menu-item>	<label class="moduleNames"><a href="#!/configureDeliverCcid">Destinazione CCID</a></label></md-menu-item>
						 <%   } %>
						

              		</md-menu-content>
             	   </md-menu>
     			</md-menu-item>
     			 <%   } %>
     			 <%   if (null != session.getAttribute("muMonitoraggio")) { %>
	             <md-menu-item>
	                <md-menu>
	                  <md-button >Monitoraggio</md-button>
	                  <md-menu-content class="menu-content-background">
						  
	                    <%   if (null != session.getAttribute("dsrStepsMonitoringSearch")) { %>
	                   	 	<md-menu-item>	<label class="moduleNames"><a href="#!/dsrStepsMonitoringSearch">DSR Monitoring</a></label></md-menu-item>
	                    <%   } %>
	                    <%   if (null != session.getAttribute("sqsServiceBusSearch")) { %>
	                   	 	<md-menu-item>	<label class="moduleNames"><a href="#!/serviceBusSearch">Messaggi Service Bus</a></label></md-menu-item>
	                    <%   } %>
	                    <%   if (null != session.getAttribute("sqsServiceBusStats")) { %>
	                   	 	<md-menu-item>	<label class="moduleNames"><a href="#!/serviceBusStats">Service Bus Stats</a></label></md-menu-item>
	                    <%   } %>
	                    <%   if (null != session.getAttribute("sqsFailedStats")) { %>
	                   	 	<md-menu-item>	<label class="moduleNames"><a href="#!/sqsFailedStats">SQS Failed Stats</a></label></md-menu-item>
	                    <%   } %>
	                    <%   if (null != session.getAttribute("sqsFailedSearch")) { %>
	                   	 	<md-menu-item>	<label class="moduleNames"><a href="#!/sqsFailedSearch">SQS Failed Search</a></label></md-menu-item>
	                    <%   } %>
	                    <%   if (null != session.getAttribute("sqsHostnames")) { %>
	                   	 	<md-menu-item>	<label class="moduleNames"><a href="#!/sqsHostnames">Hostnames / Cluster IDs</a></label></md-menu-item>
						<%   } %>
						<%   if (true) { %>
							<md-menu-item>	<label class="moduleNames"><a href="#!/configurazione-char">Configurazione caratteri CCID</a></label></md-menu-item>
						<%   } %>	
						
					  </md-menu-content>
                	</md-menu>
	             </md-menu-item>
				 <%   } %>
					<md-menu-item>
						 <md-menu>
							 <md-button >Gestione reclami</md-button>
							 <md-menu-content class="menu-content-background">
										 <md-menu-item>	<label class="moduleNames"><a href="#!/reclamiUploadFile">Upload file</a></label></md-menu-item>
								 </md-menu-content>
						 </md-menu>
					</md-menu-item>
					<md-menu-item>
						<md-menu>
							<md-button>Ripartizione</md-button>
							<md-menu-content class="menu-content-background">
								<md-menu-item> <label class="moduleNames"><a href="#!/configurazioneAggiTrattenuta">Configurazione Aggi e
											trattenuta</a></label></md-menu-item>
								<md-menu-item> <label class="moduleNames"><a href="#!/produzioneCarichi">
											Produzione carichi</a></label></md-menu-item>
								<md-menu-item> <label class="moduleNames"><a href="#!/listaCarichi">
											Lista Carichi</a></label></md-menu-item>
								<md-menu-item> <label class="moduleNames"><a href="#!/statistichePostRipartizione">
											Statistiche</a></label></md-menu-item>
								<md-menu-item> <label class="moduleNames"><a href="#!/rendiconto">
											Rendiconto</a></label></md-menu-item>
								<md-menu-item> <label class="moduleNames"><a href="#!/lookThrough">
									Estrazione dati per Look-Through</a></label></md-menu-item>
							</md-menu-content>
						</md-menu>
					</md-menu-item>

				<%--   if (null != session.getAttribute("muBackclaim")) { --%>
				<md-menu-item>	<label class="moduleNames"><a href="#!/multimediale/backclaim">Back-claim</a></label></md-menu-item>
				<%--  } --%>

				<md-menu-item>
					<md-menu>
						<md-button >
							<div style="max-width: 90%; white-space: normal;">
								Gestione conflitti Youtube
							</div>
						</md-button>
						<md-menu-content class="menu-content-background">
									<md-menu-item>	<label class="moduleNames"><a href="#!/gestioneConflitti">Upload file</a></label></md-menu-item>
							</md-menu-content>
					</md-menu>
			   </md-menu-item>

            </md-menu-content>
          </md-menu>
          <%   } %>

          <%   if (null != session.getAttribute("guBroadcasting")) { %>
          <md-menu>
            <a class="flax1" ng-click="$mdMenu.open()" >
             <strong>Broadcasting&nbsp;<i class="caret"></i></strong>
            </a>
			<md-menu-content class="menu-content-background">
				<%   if (null != session.getAttribute("brUtilizzazioniCanale")) { %>
				<md-menu-item>	<label class="moduleNames"><a href="#!/evaluate-channel">Monitoraggio Utilizzazioni</a></label></md-menu-item>
				<%   } %>
           	 	<%   if (null != session.getAttribute("brGestioneUtilizzazioni")) { %>
           	 	<md-menu-item>	<label class="moduleNames"><a href="#!/broadcasting-report">Gestione Utilizzazioni</a></label></md-menu-item>
           	 	<%   } %>
           	 	<%   if (null != session.getAttribute("brGestioneNews")) { %>
           	 	<md-menu-item>	<label class="moduleNames"><a href="#!/news">Gestione News</a></label></md-menu-item>
           	 	<%   } %>
           	 	<%   if (null != session.getAttribute("brGestioneBroadcaster")) { %>
           	 	<md-menu-item>	<label class="moduleNames"><a href="#!/utenti">Gestione Broadcaster</a></label></md-menu-item>
           	 	<%   } %>
           	 	<%   if (null != session.getAttribute("brCarichiRiparto")) { %>
           	 	<md-menu-item>	<label class="moduleNames"><a href="#!/carichi-ripartizione">Carichi di Ripartizione</a></label></md-menu-item>
					<%   } %>
           	 	<%   if (null != session.getAttribute("brPreallocazioneIncasso")) { %>
           	 	<md-menu-item>	<label class="moduleNames"><a href="#!/preallocazione-incassi">Preallocazione Incassi</a></label></md-menu-item>
					<%   } %>
				<%   if (null != session.getAttribute("brPreallocazioneIncasso")) { %>
           	 	<md-menu-item>	<label class="moduleNames"><a href="#!/anagraficaTriplette">Gestione Anagrafica Triplette</a></label></md-menu-item>
					<%   } %>
				<%   if (null != session.getAttribute("brPreallocazioneIncasso")) { %>
					<md-menu-item>	<label class="moduleNames"><a href="#!/monitoraggioIngestion">Monitoraggio Ingestion SAP</a></label></md-menu-item>
					<%   } %>
           	 	<%   if (null != session.getAttribute("brLoginAlterEgo")) { %>
           	 	<md-menu-item>	<label class="moduleNames"><a href="#!/alterEgo">Login Alter Ego</a></label></md-menu-item>
           	 	<%   } %>
            </md-menu-content>
          </md-menu>
          <%   } %>

            <%   if (null != session.getAttribute("guPerforming")) { %>
          <md-menu>
            <a class="flax1" ng-click="$mdMenu.open()" >
             <strong>Performing&nbsp;<i class="caret"></i></strong>
            </a>
           <md-menu-content class="menu-content-background">
           		<%   if (null != session.getAttribute("prCampionamento")) { %>
			     <md-menu-item>
	                <md-menu>
	                  <md-button >Regole</md-button>
	                  <md-menu-content class="menu-content-background">
						  <md-menu>
								  <md-button>Ripartizione</md-button> <md-menu-content class="menu-content-background">
											<md-menu-item><label class="moduleNames"><a href="#!/confImportiRiconciliati">Configurazione Importi Riconciliati</a></label></md-menu-item>
							  <%   if (null != session.getAttribute("prRegoleRipartizione")) { %>
							  <md-menu-item><label class="moduleNames"><a href="#!/regoleRipartizione">Voci Incasso</a></label></md-menu-item>
							  <% } %>
							  <%   if (null != session.getAttribute("prGestioneMegaconcerti")) { %>
							  <md-menu-item><label class="moduleNames"><a href="#!/gestioneMegaconcerti">Megaconcerti</a></label></md-menu-item>
							  <% } %>

                              <%   if (null != session.getAttribute("prGestioneMp")) { %>
							  <md-menu>
								  <md-button >Radio in Store</md-button>
								  <md-menu-content class="menu-content-background">
									  <md-menu-item>	<label class="moduleNames"><a href="#!/gestioneMusicProviders">Configura Provider</a></label></md-menu-item>
								  </md-menu-content>
							  </md-menu>
                              <% } %>

							  </md-menu-content>
						  </md-menu>

						  <md-menu>
							  <md-button>Campionamento</md-button> <md-menu-content class="menu-content-background">
							  <%   if (null != session.getAttribute("prConfigurazioneCampionamento")) { %>
							  <md-menu-item>	<label class="moduleNames"><a href="#!/configSampling">Configurare Contabilita'</a></label></md-menu-item>
							  <%   } %>
							  <%   if (null != session.getAttribute("prEsecuzioneCompionamento")) { %>
							  <md-menu-item>	<label class="moduleNames"><a href="#!/samplingExecution">Eseguire Campionamento</a></label></md-menu-item>
							  <%   } %>
							  <%   if (null != session.getAttribute("prCampionamentoFlussoGuida")) { %>
							  <md-menu-item>	<label class="moduleNames"><a href="#!/samplingGuide">Scarica file guida</a></label></md-menu-item>
							  <%   } %>

							  <md-menu>
								  <md-button >Storico</md-button>
								  <md-menu-content class="menu-content-background">
									  <%   if (null != session.getAttribute("prStoricoCampionamento")) { %>
									  <md-menu-item>	<label class="moduleNames"><a href="#!/samplingHistory">Configurazioni</a></label></md-menu-item>
									  <%   } %>
									  <%   if (null != session.getAttribute("prStoricoEsecuzioni")) { %>
									  <md-menu-item>	<label class="moduleNames"><a href="#!/executionHistory">Esecuzioni</a></label></md-menu-item>
									  <%   } %>
								  </md-menu-content>
							  </md-menu>
						  </md-menu-content>
                          </md-menu>
						<!--
								  <%   if (null != session.getAttribute("prConfigurazioniSoglie")) { %>
							  <md-menu>
								  <md-button>Codifica</md-button> <md-menu-content class="menu-content-background">
								  <md-menu-item><label class="moduleNames"><a href="#!/configurazioniSoglie">Configurare Soglie</a></label></md-menu-item>
							  </md-menu-content>
							  </md-menu>
									<% } %> -->


	                  </md-menu-content>
					 <!--
	                  <%   if (null != session.getAttribute("prGA")) { %>
	                  <md-button >GA</md-button>
	                  <md-menu-content class="menu-content-background">
	                     <%   if (null != session.getAttribute("prAcquirePM")) { %>
						<md-menu-item>	<label class="moduleNames"><a href="#!/configSampling">Acquisizione PM</a></label></md-menu-item>
		           	 	<%   } %>
	                  </md-menu-content>
					 <%   } %>
					  -->
	                </md-menu>
	             </md-menu-item>
	             <%   } %>
	             <%   if (null != session.getAttribute("prCruscotti")) { %>
	             <md-menu-item>
	             	<md-menu>
	             	  <md-button ng-click="$mdMenu.open()">Ricerca</md-button>
	             	  <md-menu-content class="menu-content-background">
	                 <%   if (null != session.getAttribute("prVisualizzazioneEventi")) { %>
	                  	<md-menu-item><label class="moduleNames"><a href="#!/visualizzazioneEventi">Eventi Pagati</a></label></md-menu-item>
	                  <% } %>
	                  <%   if (null != session.getAttribute("prVisualizzazioneMovimenti")) { %>
	                  	<md-menu-item><label class="moduleNames"><a href="#!/visualizzazioneMovimenti">Movimenti contabili</a></label></md-menu-item>
	                  <% } %>
	                 <%   if (null != session.getAttribute("prVisualizzazioneTracciamentoPM")) { %>
	                  	<md-menu-item><label class="moduleNames"><a href="#!/tracciamentoPM">Programmi Musicali</a></label></md-menu-item>
	                 <% } %>
	                 <%   if (null != session.getAttribute("prVisualizzazioneFatture")) { %>
	                  	<md-menu-item><label class="moduleNames"><a href="#!/fattureList">Fatture</a></label></md-menu-item>
	                 <% } %>
                          <%   if (null != session.getAttribute("prRiconciliazioneImporti")) { %>
                          <md-menu-item>	<label class="moduleNames"><a href="#!/riconciliazioneImporti">Importi Riconciliati</a></label></md-menu-item>
                          <%   } %>
						  <%   if (null != session.getAttribute("prAggiornamentoSun")) { %>
						  <md-menu-item>	<label class="moduleNames"><a href="#!/aggiornamentoDatiSun">Dati Aggiornati Sun</a></label></md-menu-item>
						  <%   } %>
	                  </md-menu-content>
	             	</md-menu>
	             </md-menu-item>
	             <% } %>

           	 	<%   if (null != session.getAttribute("prCodifica")) { %>
	             <md-menu-item>
	             	<md-menu>
	             	  <md-button ng-click="$mdMenu.open()">Codifica</md-button>
	             	  <md-menu-content class="menu-content-background">
	                 <%   if (null != session.getAttribute("prCodificaManualeEsperto")) { %>
	                  	<md-menu-item><label class="moduleNames"><a href="#!/codificaManualeEsperto">Esperto</a></label></md-menu-item>
	                  <% } %>
	                 <%   if (null != session.getAttribute("prCodificaManualeBase")) { %>
	                  	<md-menu-item><label class="moduleNames"><a href="#!/codificaManualeBase">Base</a></label></md-menu-item>
	                  <% } %>
                             <%   if (null != session.getAttribute("prScodifica")) { %>
						 <md-menu-item><label class="moduleNames"><a href="#!/performing/codifica/scodifica">Scodifica</a></label></md-menu-item>
                             <% } %>
	             	</md-menu>
	             </md-menu-item>
	             <% } %>
	             <!-- inizio nuova pagina -->
	             <%   if (null != session.getAttribute("prRadioInStore")) { %>
			     <md-menu-item>
	                <md-menu>

	                  <md-button >Dashboard</md-button>
	                  <md-menu-content class="menu-content-background">

							  <%   if (null != session.getAttribute("prCarichiRipartizione")) { %>
						  <md-menu>
							  <md-button>Ripartizione</md-button> <md-menu-content class="menu-content-background">
							  <md-menu-item><label class="moduleNames"><a href="#!/carichiRipartizione">Carichi</a></label></md-menu-item>
						  </md-menu-content>
						  </md-menu>
							  <% } %>

							  <%   if (null != session.getAttribute("prKPICodifica")) { %>
						  <md-menu>
							  <md-button>Codifica</md-button> <md-menu-content class="menu-content-background">
							  <md-menu-item><label class="moduleNames"><a href="#!/KPICodifica">KPI</a></label></md-menu-item>
							  <%  } %>
							  <%   if (null != session.getAttribute("prMonitoringOperatoriCodifica")) { %>
							  <md-menu-item><label class="moduleNames"><a href="#!/MonitoringOperatoriCodifica">Operatori</a></label></md-menu-item>
							  <% } %>
							  <%   if (null != session.getAttribute("prMonitoraggioCodificato")) { %>
							  <md-menu-item><label class="moduleNames"><a href="#!/monitoraggioCodificato">Matrice</a></label></md-menu-item>
						  </md-menu-content>
						  </md-menu>
							  <% } %>


						  <md-menu>
							  <md-button>Radio in Store</md-button> <md-menu-content class="menu-content-background">
							  <% } %>
							  <%   if (null != session.getAttribute("prMonitoraggioMp")) { %>
							  <md-menu-item>	<label class="moduleNames"><a href="#!/monitoraggioPalinsesti">Monitoraggio Palinsesti</a></label></md-menu-item>
							  <% } %>
						  </md-menu-content>
						  </md-menu>

	                     <%--<%   if (null != session.getAttribute("prGestioneMp")) { %>
							<md-menu-item>	<label class="moduleNames"><a href="#!/gestioneMusicProviders">Configura Provider</a></label></md-menu-item>--%>
	               		<% } if (null != session.getAttribute("prMonitoraggioKpi")) { %>
							<md-menu-item>	<label class="moduleNames"><a href="#!/rsMonitoraggioKpi">Monitoraggio Kpi</a></label></md-menu-item>
					 	<% } %>


	                  </md-menu-content>


	                </md-menu>
	             </md-menu-item>
	             <%--<% } %>--%>

	             <!-- Fine nuova pagina -->

	            </md-menu-content>
          </md-menu>
          <%--<% } %>--%>
         <%   if (null != session.getAttribute("guMultimedialeLocale")) { %>
          	<md-menu>
                <a class="flax1" ng-click="$mdMenu.open()" >
                    <strong> Multimediale Blanket&nbsp;<i class="caret"></i></strong>
                </a>
                <md-menu-content class="menu-content-background">
                    <%   if (null != session.getAttribute("muLocale")) { %>
                    <md-menu-item>
                        <label class="moduleNames"><a href="#!/multimedialeLocale">Visualizza Report</a></label>
                    </md-menu-item>
                    <%   } %>
                       <%   if (null != session.getAttribute("muLocale")) { %>
                    <md-menu-item>
                        <label class="moduleNames"><a href="#!/multimedialeLocaleRegolare">Monitoraggio Codificato Regolare</a></label>
                    </md-menu-item>
                    <%   } %>
                </md-menu-content>
            </md-menu>
					<%   } %>
					
					<%   if (null != session.getAttribute("prConfigurazioniSoglie")) { %>
          	<md-menu>
                <a class="flax1" ng-click="$mdMenu.open()" >
                    <strong> Codifica&nbsp;<i class="caret"></i></strong>
								</a>
								<% if (null != session.getAttribute("prConfigurazioniSoglie")) { %>
									<md-menu-content class="menu-content-background">
										<md-menu>
											<md-button ng-click="$mdMenu.open()">Configurazione</md-button>
											<md-menu-content class="menu-content-background">
												<md-menu-item><label class="moduleNames"><a href="#!/configurations/codifica/view-info">Informazioni
															Visualizzate</a></label></md-menu-item>
												<md-menu-item><label class="moduleNames"><a href="#!/configurations/codifica/ranking">Parametri
															Ranking</a></label>
												</md-menu-item>
												<md-menu-item><label class="moduleNames"><a href="#!/configurations/codifica/thresholds">Soglie
															Codifica</a></label></md-menu-item>
											</md-menu-content>
										</md-menu>
										<md-menu>
											<md-button ng-click="$mdMenu.open()">Ricerca proposte</md-button>
											<md-menu-content class="menu-content-background">
												<md-menu-item><label class="moduleNames"><a href="#!/RicProposte">Ricerca puntuale</a></label></md-menu-item>
												<md-menu-item><label class="moduleNames"><a href="#!/ConsoleCSV">Upload CSV </a></label>
												</md-menu-item>
											</md-menu-content>
										</md-menu>
									</md-menu-content>
									<% } %>
									
            </md-menu>
          <%   } %>					

          <md-menu class="flax">
	            <div class="navbutton" ng-click="$mdMenu.open()" >
		            <a id="headerLinksBig" class="flax pull-right headerLinksContainer">
		             	<strong><%= userName %></strong>&nbsp;<i class="caret"></i>
					</a>
					<div id="headerLinksCompact" class="navbutton">
						<span class="dropdown iconmenu">
							<a class="dropdown-toggle btn-navbar navbutton" data-toggle="dropdown" href="">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</a>
						</span>
					</div>
	           </div>
	           <md-menu-content class="menu-content-background">
	            <md-menu-item>
			       <a href="logout"><span class="glyphicon glyphicon-log-out"></span>&nbsp;&nbsp;Esci</a>
				</md-menu-item>
               </md-menu-content>
          </md-menu>
        </md-menu-bar>
      </div>
    </div>
  </md-toolbar>
  <!-- top right icons -->
</div>
