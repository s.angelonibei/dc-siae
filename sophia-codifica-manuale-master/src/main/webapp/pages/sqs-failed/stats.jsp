<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%@
	include file="../navbar.jsp" %>

<div class="bodyContents" >

	<div class="mainContainer row-fluid" >
	
		<div id="companyLogo" class="navbar commonActionsContainer noprint">
			<div class="actionsContainer row-fluid">
				<div class="span2">
<!--  -- >			<span class="companyLogo"><img src="images/siae_logo_siaetrasparente.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
<!--  -->			<span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
				</div>
				<span class="btn-toolbar span4">
					<div class="pageNumbers alignTop pull-right" style="text-align: center;">
		 				<span class="pageNumbersText"><strong>SQS Failed Statistics</strong></span>
					</div>
				</span>				
			</div>
		</div>
	
		<div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px;"> 
			<div class="listViewPageDiv">

				<!-- actions and pagination -->
				<div class="listViewTopMenuDiv noprint">
					<div class="listViewActionsDiv row-fluid">
<!--  -- >
						<span class="btn-toolbar span4">
							<span class="btn-group">

 							</span>
						</span>
<!--  -- >
						<span class="btn-toolbar span1">
							<span class="customFilterMainSpan btn-group" style="text-align: center;">
						 	</span>
						</span>
<!--  -->

						<span class="span8 btn-toolbar">
							<div class="listViewActions pull-right">
								<span class="btn-group">

	 							</span>
	 				 		</div>
					 		<div class="clearfix"></div>
					 	</span>
					</div>
				</div>
					
				<!-- riga da riconoscere -->
				<!-- table -->
				<div class="listViewContentDiv" id="listViewContents">
					
					 <div class="contents-topscroll noprint">
					 	<div class="topscroll-div" style="width: 95%">&nbsp;</div>
					 </div>
					 					 
					 <div class="listViewEntriesDiv contents-bottomscroll">
					 	<div class="bottomscroll-div" style="width: 95%; min-height: 80px">
					 	
					 	
					 	<div ng-repeat="day in ctrl.sqsFailedStats | orderBy:'-receiveDate'">
					 	
					 		<table class="table table-bordered listViewEntriesTable" >
					 			
					 		<thead>
					 		<tr class="listViewHeaders">
					 			<th nowrap><a href="" class="listViewHeaderValues" >{{day.receiveDate | date:'mediumDate'}}</a></th>
					 			<th nowrap><a href="" class="listViewHeaderValues" >Nome Coda</a></th>
					 			<th nowrap><a href="" class="listViewHeaderValues" >Numero Messaggi</a></th>
					 			<th nowrap></th>
					 		</tr>
					 		</thead>
					 		
					 		<tbody >
						 		<tr class="listViewEntries" ng-repeat="item in day.sqsFailedStats | orderBy:'-queueSize'">
						 		
						 			<td class="listViewEntryValue medium" width="20%" nowrap >{{item.receiveDate | date:'mediumDate'}}</td>
						 			<td class="listViewEntryValue medium" width="40%" nowrap ><strong>{{item.queueName}}</strong></td>
						 			<td class="listViewEntryValue medium" nowrap >{{item.queueSize | number}}</td>

						 			<td nowrap class="medium">
						 				<div class="actions pull-right">
<!--  -->
						 				<span class="actionImages">
						 					<a ng-click="showSearch($event,item)"><i title="Mostra Dettaglio" class="glyphicon glyphicon-list alignMiddle"></i></a>&nbsp;
						 				</span>
<!--  -->
						 				</div>
						 			</td>

						 		</tr>
					 		</tbody>
						</table>
						<br/>
					 	</div>
					 	

						</div>
					</div>
				</div>
				<!-- /table -->
				
			</div>
		</div>
	</div>
<!-- -- >
	<pre>{{ctrl.properties}}</pre>
< !-- -->
</div>