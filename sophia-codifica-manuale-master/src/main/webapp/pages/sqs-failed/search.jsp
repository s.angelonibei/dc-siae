<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%@
	include file="../navbar.jsp" %>

<div class="bodyContents" >

	<div class="mainContainer row-fluid" >
	
		<div id="companyLogo" class="navbar commonActionsContainer noprint">
			<div class="actionsContainer row-fluid">
				<div class="span2">
<!--  -- >			<span class="companyLogo"><img src="images/siae_logo_siaetrasparente.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
<!--  -->			<span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
				</div>
				<span class="btn-toolbar span4">
					<div class="pageNumbers alignTop pull-right" style="text-align: center;">
		 				<span class="pageNumbersText"><strong>Ricerca messaggi di errore</strong></span>
					</div>
				</span>				
			</div>
		</div>
	
		<div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px;"> 
			<div class="listViewPageDiv">

				<!-- form ricerca -->
				<div class="listViewTopMenuDiv noprint">
					<div class="listViewActionsDiv row-fluid">

					<form name="form">
					<table class="table table-bordered blockContainer showInlineTable equalSplit">
					<thead>
						<tr>
							<th class="blockHeader" colspan="4"
								ng-show="ctrl.hideForm">
								<span ng-click="ctrl.hideForm=false" class="glyphicon glyphicon-expand"></span>&nbsp;Parametri Ricerca
							</th>
							<th class="blockHeader" colspan="4" 
								ng-hide="ctrl.hideForm">
								<span ng-click="ctrl.hideForm=true" class="glyphicon glyphicon-collapse-down"></span>&nbsp;Parametri Ricerca
							</th>
						</tr>
					</thead>
					<tbody ng-hide="ctrl.hideForm">
						<tr>
							<td class="fieldLabel small"  >
								<label class="muted pull-right marginRight10px">
									Data
								</label>
							</td>
							<td class="fieldValue large">
								<div class="row-fluid" >
									<input type="text" name="receiveDate" class="input-small search_receiveDate" 
											ng-model="ctrl.receiveDate" ng-pattern="/^((\d{4})\-(\d{2})\-(\d{2}))$/" > 
										<script type="text/javascript">
										    $(".search_receiveDate").datepicker({
										    	format: 'yyyy-mm-dd', 
										    	language: 'it',
										    	autoclose: true,
										    	todayHighlight: true
										    });
										</script> 
								</div>
							</td>

							<td class="fieldLabel small" nowrap >
								<label class="muted pull-right marginRight10px">
									Nome Coda
								</label>
							</td>
							<td class="fieldValue small">
								<div class="row-fluid">
									<select class="input-large nameField"
										name="queueName"
				 						ng-model="ctrl.queueName">
					 					<option value="">(Tutte)</option>
							 			<option value="{{item}}" ng-repeat="item in ctrl.decode.queueNames" >{{item}}</option>
					 				</select>
								</div>
							</td>
						</tr>

						<tr>
							<td class="fieldLabel small"  >
								<label class="muted pull-right marginRight10px">
									JSON
								</label>
							</td>
							<td class="fieldValue large" >
								<div class="row-fluid" >
										<textarea name="json" class="input-large " type="text"  rows="3"
											ng-model="ctrl.json"></textarea>
								</div>
							</td>	

							<td class="fieldLabel medium" style="text-align: right; vertical-align: bottom" nowrap colspan="2">
								<button class="btn btn-success" ng-click="onAzzera()"><span class="glyphicon glyphicon-erase"></span>&nbsp;<strong>Cancella</strong></button>
								&nbsp;
								<button class="btn btn-success" ng-click="onRicerca()"><span class="glyphicon glyphicon-search"></span>&nbsp;<strong>Ricerca</strong></button>
							</td>

						</tr>

						<tr>
						</tr>
					</tbody>
					</table>
					</form>

					</div>
				</div>

				<!-- actions and pagination -->
				<div class="listViewTopMenuDiv noprint">
					<div class="listViewActionsDiv row-fluid">
<!--  -->
						<span class="btn-toolbar span4">
							<span class="btn-group">

 							</span>
						</span>
<!--  -->
<!--  -->
						<span class="btn-toolbar span4">
							<span class="customFilterMainSpan btn-group" style="text-align: center;">
								<div class="pageNumbers alignTop ">
					 				<span class="pageNumbersText"><strong><!-- Titolo --></strong></span>
								</div>
						 	</span>
						</span>
<!--  -->
<!--  -->
						<span class="span4 btn-toolbar">
							<div class="listViewActions pull-right">
					 			
					 			<div class="pageNumbers alignTop ">
						 			<span ng-show="ctrl.searchResults.hasPrev || ctrl.searchResults.hasNext">
						 				<span class="pageNumbersText" style="padding-right:5px">
						 					Record da <strong>{{ctrl.searchResults.first+1}}</strong> a <strong>{{ctrl.searchResults.last}}</strong>
						 				</span>
					 					<button title="Precedente" class="btn" id="listViewPreviousPageButton" type="button" 
					 							ng-click="navigateToPreviousPage()"
					 							ng-show="ctrl.searchResults.hasPrev">
					 						<span class="glyphicon glyphicon-chevron-left"></span>
					 					</button>
					 					<button title="Successiva" class="btn" id="listViewNextPageButton" type="button"
					 							ng-click="navigateToNextPage()"
					 							ng-show="ctrl.searchResults.hasNext">
					 						<span class="glyphicon glyphicon-chevron-right"></span>
					 					</button>
					 				</span>
						 		</div>
					 		</div>
					 		<div class="clearfix"></div>
					 	</span>
<!--  -->
					</div>
				</div>
					
				<!-- riga da riconoscere -->
				<!-- table -->
				<div class="listViewContentDiv" id="listViewContents">
					
					 <div class="contents-topscroll noprint">
					 	<div class="topscroll-div" style="width: 95%">&nbsp;</div>
					 </div>
					 					 
					 <div class="listViewEntriesDiv contents-bottomscroll">
					 	<div class="bottomscroll-div" style="width: 95%; min-height: 80px">
					 	
					 		<table class="table table-bordered listViewEntriesTable">
					 		<thead>
					 		<tr class="listViewHeaders">
								<th nowrap><a class="listViewHeaderValues" >#</a></th>
					 			<th nowrap><a ng-click="setSortType('receiveTime');" class="listViewHeaderValues" >Data Ricezione
					 			<span ng-show="ctrl.sortType=='receiveTime' && !ctrl.sortReverse" class="glyphicon glyphicon-sort-by-attributes"></span>
            					<span ng-show="ctrl.sortType=='receiveTime' && ctrl.sortReverse" class="glyphicon glyphicon-sort-by-attributes-alt"></span>
            					</a></th>
					 			<th nowrap><a ng-click="setSortType('queueName');" class="listViewHeaderValues" >Nome Coda
					 			<span ng-show="ctrl.sortType=='queueName' && !ctrl.sortReverse" class="glyphicon glyphicon-sort-by-alphabet"></span>
            					<span ng-show="ctrl.sortType=='queueName' && ctrl.sortReverse" class="glyphicon glyphicon-sort-by-alphabet-alt"></span>
            					</a></th>
								<th nowrap><a class="listViewHeaderValues" >UUID</a></th>
								<th nowrap><a class="listViewHeaderValues" >Sender</a></th>
					 			<th></th>
					 		</tr>
					 		</thead>
					 		<tbody >
						 		<tr class="listViewEntries" ng-repeat="item in ctrl.searchResults.rows | orderBy:ctrl.sortType:ctrl.sortReverse">
						 		
						 			<td class="listViewEntryValue medium" nowrap >{{item.id}}</td>
						 			<td class="listViewEntryValue medium" nowrap >{{item.receiveTime | date:'medium'}}</td>
						 			<td class="listViewEntryValue medium" nowrap >{{item.queueName}}</td>
						 			<td class="listViewEntryValue medium" nowrap >{{item.json.header.uuid}}</td>
						 			<td class="listViewEntryValue medium" nowrap >{{item.json.header.sender}}</td>

						 			<td nowrap class="medium">
						 				<div class="actions pull-right">
<!--  -->
						 				<span class="actionImages">
						 					<a ng-click="showSendMessage($event,item)"><i title="Invia Nuovamente" class="glyphicon glyphicon-share-alt alignMiddle"></i></a>&nbsp;
						 					<a ng-click="showDetails($event,item)"><i title="Dettagli" class="glyphicon glyphicon-list-alt alignMiddle"></i></a>&nbsp;
						 				</span>
<!--  -->
						 				</div>
						 			</td>

						 		</tr>
					 		</tbody>
					 		</table>
					 		
						</div>
					</div>
				</div>
				<!-- /table -->
				
				<!-- pagination -->
				<div class="listViewTopMenuDiv noprint">
					<div class="listViewActionsDiv row-fluid">
					
						<span class="btn-toolbar span4">
						</span>

						<span class="btn-toolbar span4">
						</span>
						
						<span class="span4 btn-toolbar">
							<div class="listViewActions pull-right">
					 			
					 			<div class="pageNumbers alignTop ">
						 			<span ng-show="ctrl.searchResults.hasPrev || ctrl.searchResults.hasNext">
						 				<span class="pageNumbersText" style="padding-right:5px">
						 					Record da <strong>{{ctrl.searchResults.first+1}}</strong> a <strong>{{ctrl.searchResults.last}}</strong>
						 				</span>
					 					<button title="Precedente" class="btn" id="listViewPreviousPageButton" type="button" 
					 							ng-click="navigateToPreviousPage()"
					 							ng-show="ctrl.searchResults.hasPrev">
					 						<span class="glyphicon glyphicon-chevron-left"></span>
					 					</button>
					 					<button title="Successiva" class="btn" id="listViewNextPageButton" type="button"
					 							ng-click="navigateToNextPage()"
					 							ng-show="ctrl.searchResults.hasNext">
					 						<span class="glyphicon glyphicon-chevron-right"></span>
					 					</button>
					 				</span>
						 		</div>

					 		</div>
					 		<div class="clearfix"></div>
					 	</span>
					</div>
				</div>
				<!-- /pagination -->
				
			</div>
		</div>
	</div>
<!-- -- >
	<pre>{{ctrl.properties}}</pre>
< !-- -->
</div>