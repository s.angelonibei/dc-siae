<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="../navbar.jsp" %>

<div class="bodyContents" data-ng-init="init()">
    <div class="mainContainer row-fluid">

        <div class="contents-topscroll noprint">
            <div class="topscroll-div" style="width: 95%;margin-bottom: 20px">&nbsp;</div>
        </div>

        <div class="sticky">
            <div id="companyLogo" class="navbar commonActionsContainer noprint">
                <div class="actionsContainer row-fluid">
                    <div class="span2">
                        <span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
                    </div>
                    <span class="btn-toolbar span5">
						<div class="pageNumbers alignTop pull-right" style="text-align: center;">
							<span id="up" class="pageNumbersText"><strong>{{ctrl.pageTitle}}</strong></span>
						</div>
					</span>
                </div>
            </div>
        </div>

        <div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px;margin-top: 25px;">
            <div class="listViewPageDiv">
                <!-- messaggi -->
                <div class="alert alert-warning" data-ng-show="ctrl.messages.warning" style="margin-top:20px">
                    <button type="button" class="close" ng-click="ctrl.messages.clear()" aria-hidden="true">x</button>
                    <span>{{ctrl.messages.warning}}</span>
                </div>
                <div class="alert alert-danger" data-ng-show="ctrl.messages.error" style="margin-top:20px">
                    <button type="button" class="close" ng-click="ctrl.messages.clear()" aria-hidden="true">x</button>
                    <span><strong>Attenzione: </strong>{{ctrl.messages.error}}</span>
                </div>
                <div class="alert alert-success" data-ng-show="ctrl.messages.info" style="margin-top:20px">
                    <button type="button" class="close" ng-click="ctrl.messages.clear()" aria-hidden="true">x</button>
                    <span>{{ctrl.messages.info}}</span>
                </div>
            </div>

            <div class="listViewPageDiv" data-ng-show="ctrl.data.status=='READY'">
                <div class="listViewTopMenuDiv noprint">
                    <div class="listViewActionsDiv row-fluid">
                        <table class="table table-bordered blockContainer showInlineTable equalSplit noHoverTable">
                            <thead>
                            <tr>
                                <th class="blockHeader" colspan="6" data-ng-show="ctrl.hideRoyaltiesSelection">
										<span data-ng-click="ctrl.hideRoyaltiesSelection=false"
                                              class="glyphicon glyphicon-expand"></span>&nbsp;Schemi di Riparto
                                    &nbsp;- Periodo selezionato: {{ctrl.data.currentRoyalties.month | itMonth}}
                                    {{ctrl.data.currentRoyalties.year}}
                                </th>
                                <th class="blockHeader" colspan="6" data-ng-hide="ctrl.hideRoyaltiesSelection">
										<span data-ng-click="ctrl.hideRoyaltiesSelection=true"
                                              class="glyphicon glyphicon-collapse-down"></span>&nbsp;Schemi di Riparto
                                    &nbsp;- Periodo selezionato: {{ctrl.data.currentRoyalties.month | itMonth}}
                                    {{ctrl.data.currentRoyalties.year}}
                                </th>
                            </tr>
                            </thead>
                            <tbody data-ng-hide="ctrl.hideRoyaltiesSelection">
                            <tr data-ng-repeat-start='royaltiesStorageBundle in ctrl.data.royaltiesStorages'>
                                <td>
                                    <button class="btn addButton" data-ng-click="viewChildren= !viewChildren">
											<span class="glyphicon glyphicon-plus"
                                                  data-ng-show="!viewChildren"></span><strong></strong>
                                        <span class="glyphicon glyphicon-minus"
                                              data-ng-show="viewChildren"></span><strong></strong>
                                    </button>
                                    Anno {{royaltiesStorageBundle.year}}
                                </td>
                            </tr>
                            <tr>
                                <td colspan='2' data-ng-class="viewChildren? '' : 'hide'">
                                    <table class="table table-bordered blockContainer showInlineTable">
                                        <tbody>
                                        <tr data-ng-repeat='royaltiesStorage in royaltiesStorageBundle.months'>
                                            <td>{{royaltiesStorage.month | itMonth }}</td>
                                            <td>
                                                <div data-ng-repeat="dump in royaltiesStorage.dumps">
                                                {{dump.dump}} (ultimo aggiornamento:&nbsp;
                                                {{dump.updatedDate | date: 'dd/MM/yyyy'}})
                                                </div>
                                            </td>
                                            <td>
                                                <button class="btn addButton"
                                                        data-ng-show="!royaltiesStorage.isActive"
                                                        data-ng-click="requestRoyaltiesStorage(royaltiesStorage)">
															<span
                                                                    class="glyphicon glyphicon-import"></span><strong>&nbsp;Richiedi</strong>
                                                </button>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr data-ng-repeat-end></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="listViewPageDiv" data-ng-show="ctrl.data.status=='READY'">
                <!-- form ricerca -->
                <div class="listViewTopMenuDiv noprint">
                    <div class="listViewActionsDiv row-fluid">

                        <form name="form" class="form-inline" data-ng-submit="search()">
                            <table class="table table-bordered blockContainer showInlineTable equalSplit">
                                <thead>
                                <tr>
                                    <th class="blockHeader" colspan="6" data-ng-show="ctrl.hideSerachParams">
											<span data-ng-click="ctrl.hideSerachParams=false"
                                                  class="glyphicon glyphicon-expand"></span>&nbsp;Parametri Ricerca
                                    </th>
                                    <th class="blockHeader" colspan="6" data-ng-hide="ctrl.hideSerachParams">
											<span data-ng-click="ctrl.hideSerachParams=true"
                                                  class="glyphicon glyphicon-collapse-down"></span>&nbsp;Parametri
                                        Ricerca
                                    </th>
                                </tr>
                                </thead>
                                <tbody data-ng-hide="ctrl.hideSerachParams">
                                <tr>
                                    <td class="fieldLabel small">
                                        <div class="row-fluid">
                                            <select name="country" id="country"
                                                    data-ng-model="ctrl.data.selectedCountry"
                                                    data-ng-options=" item.name for item in countries track by item.idCountry"
                                                    class="pull-right"
                                                    required></select>
                                            <label for="country"
                                                   class="muted col-sm-2 pull-right">Territorio</label>
                                        </div>
                                    </td>

                                    <td class="fieldLabel small">
                                        <div class="row-fluid">
                                            <select name="dsp" id="dsp"
                                                    data-ng-model="ctrl.data.selectedDsp"
                                                    data-ng-options=" item.name for item in dsp track by item.idDsp"
                                                    class="pull-right"
                                                    required></select>
                                            <label for="dsp"
                                                   class="muted col-sm-2 pull-right">DSP</label>
                                        </div>
                                    </td>

                                    <td class="fieldLabel small"></td>
                                </tr>
                                <tr>
                                    <td class="fieldLabel small">
                                        <div class="row-fluid">
                                            <input name="uuid" id="uuid" class="input-large pull-right" type="text"
                                                   min="2"
                                                   data-ng-model="ctrl.data.uuid">
                                            <label for="uuid"
                                                   class="muted col-sm-2 pull-right">Codice UUID</label>
                                        </div>
                                    </td>
                                    <td class="fieldLabel small">
                                        <div class="row-fluid">
                                            <select name="society" id="society" data-ng-model="ctrl.data.society"
                                                    class="pull-right">
                                                <option value="SIAE">SIAE</option>
                                                <option value="UCMR-ADA">UCMR-ADA</option>
                                            </select>
                                            <label for="society" class="muted col-sm-2 pull-right">
                                                Società
                                            </label>
                                        </div>
                                    </td>
                                    <td class="fieldLabel small">
                                        <div class="row-fluid">
                                            <input name="workCode" id="workCode" class="input-large pull-right"
                                                   type="text" min="2"
                                                   data-ng-model="ctrl.data.workCode">
                                            <label for="workCode"
                                                   class="muted col-sm-2 pull-right">Codice Opera</label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fieldLabel medium" style="text-align:right" nowrap colspan="6">
                                        <label class="muted pull-left marginLeft10px">Per effettuare la ricerca
                                            sono obbligatori Territorio e DSP oltre a Codice UUID o Codice Opera e Società.
                                            <div
                                                    class=""></div>
                                        </label>
                                        &nbsp;
                                        <button class="btn btn-success" data-ng-click="clearSearchForm()"
                                                type="button"><span
                                                class="glyphicon glyphicon-erase"></span>&nbsp;<strong>Cancella</strong>
                                        </button>
                                        <button type="submit" class="btn btn-success"
                                                data-ng-disabled="!(ctrl.data.selectedCountry && ctrl.data.selectedDsp &&
                                                (ctrl.data.uuid || (ctrl.data.society && ctrl.data.workCode)))"><span
                                                class="glyphicon glyphicon-search"></span>&nbsp;<strong>Ricerca</strong>
                                        </button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>

            <div class="listViewPageDiv" data-ng-show="ctrl.data.status=='READY'">
                <!-- results table -->
                <%--<span data-ng-show="ctrl.data.status=='READY' && ctrl.data.royalties.length === 0">Claim a zero</span>--%>
                <div class="listViewContentDiv" id="listViewContents" data-ng-show="ctrl.data.royalties.length != 0">

                    <div class="contents-topscroll noprint">
                        <div class="topscroll-div" style="width: 95%">&nbsp;</div>
                    </div>

                    <div class="listViewEntriesDiv contents-bottomscroll">
                        <div class="bottomscroll-div" style="width: 95%; min-height: 80px">
                            <table class="table table-bordered listViewEntriesTable">
                                <thead>
                                <tr class="listViewHeaders">
                                    <th>IPI</th>
                                    <th>Posizione Siae</th>
                                    <th>Tipo qualifica</th>
                                    <th>Tipo quota</th>
                                    <th>Quota</th>
                                    <th>Società</th>
                                    <th>Claim</th>
                                    <th>SE</th>
                                    <th>PD</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="listViewEntries" data-ng-repeat="item in ctrl.data.royalties">
                                    <td class="listViewEntryValue medium">{{item.codiceIpi}}</td>
                                    <td class="listViewEntryValue medium">{{item.posizioneSiae}}</td>
                                    <td class="listViewEntryValue medium">{{item.tipoQualifica}}</td>
                                    <td class="listViewEntryValue medium">{{item.tipoQuota}}</td>
                                    <td class="listViewEntryValue medium">{{item.percentuale | number: 2}}</td>
                                    <td class="listViewEntryValue medium">{{item.socname}}</td>
                                    <td class="listViewEntryValue medium">
                                        <span class="glyphicon glyphicon-ok" data-ng-show="!item.doNotClaim"></span>
                                        <span class="glyphicon glyphicon-remove" data-ng-show="item.doNotClaim"></span>
                                    </td>
                                    <td class="listViewEntryValue medium">
											<span class="glyphicon glyphicon-ok"
                                                  data-ng-show="item.sospesoPerEditore"></span>
                                        <span class="glyphicon glyphicon-remove"
                                              data-ng-show="!item.sospesoPerEditore"></span>
                                    </td>
                                    <td class="listViewEntryValue medium">
                                        <span class="glyphicon glyphicon-ok" data-ng-show="item.publicDomain"></span>
                                        <span class="glyphicon glyphicon-remove"
                                              data-ng-show="!item.publicDomain"></span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- results table -->
            </div>
        </div>
    </div>
</div>