<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../navbar.jsp"%>
<!-- path has to be adjusted-->

<div class="bodyContents" ng-init="init()">
	<div class="mainContainer row-fluid">

		<div class="contents-topscroll noprint">
			<div class="topscroll-div" style="width: 95%;margin-bottom: 20px">&nbsp;</div>
		</div>

		<div class="sticky">
			<div id="companyLogo" class="navbar commonActionsContainer noprint">
				<div class="actionsContainer row-fluid">
					<div class="span2">
						<span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
					</div>
					<span class="btn-toolbar span5">
						<div class="pageNumbers alignTop pull-right" style="text-align: center;">
							<span id="up" class="pageNumbersText"><strong>{{ctrl.pageTitle}}</strong></span>
						</div>
					</span>
				</div>
			</div>
		</div>

		<div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px;margin-top: 25px;">
			<div class="listViewPageDiv">

				<div class="alert alert-warning" ng-show="messages.warning" style="margin-top:20px">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
					<span>{{messages.warning}}</span>
				</div>
				<div class="alert alert-danger" ng-show="messages.error" style="margin-top:20px">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
					<span><strong>Attenzione: </strong>{{messages.error}}</span>
				</div>
				<div class="alert alert-success" ng-show="messages.info" style="margin-top:20px">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
					<span>{{messages.info}}</span>
				</div>

				<!-- start table inserimento campi-->
				<div class="listViewActionsDiv row-fluid">
					<table class="table table-bordered blockContainer showInlineTable equalSplit">
						<thead>
							<tr>
								<th class="blockHeader" colspan="6" ng-show="ctrl.hideForm"><span
										ng-click="ctrl.hideForm=false"
										class="glyphicon glyphicon-expand"></span>&nbsp;Parametri
									Ricerca</th>
								<th class="blockHeader" colspan="6" ng-hide="ctrl.hideForm"><span
										ng-click="ctrl.hideForm=true"
										class="glyphicon glyphicon-collapse-down"></span>&nbsp;Parametri
									Ricerca</th>
							</tr>
						</thead>
						<tbody ng-hide="ctrl.hideForm">
							<tr>
								<td class="fieldLabel medium" style="width: 20%"><label
										class="muted pull-right marginRight10px" style="padding-top: 10px">
										Mandante </label></td>
								<td style="width: 80%">
									<div class="row-fluid">
										<span class="span10">
											<select name="mandante" id="mandante" ng-model="mandante" ng-change="resetPaging()"
												ng-options="item.nominativo for item in mandanti track by item.id"
												required>
											</select>
										</span>
									</div>

								</td>
							</tr>
							<tr>
								<td class="fieldLabel medium" style="text-align: right; width: 16.66% nowrap"
									colspan="6">
									<button class="btn addButton" ng-click="filter(mandante)">
										<span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;<strong>Applica</strong>
									</button>
									<button class="btn addButton" ng-click="reset()"
										ng-show="mandante.codice">
										<span
											class="glyphicon glyphicon-erase"></span>&nbsp;&nbsp;<strong>Cancella</strong>
									</button>
								</td>
							</tr>
						</tbody>
					</table>

				</div>
				<!-- end table inserimento campi -->

				<!-- start toolbar tabella | paginazione -->
				<div class="listViewTopMenuDiv noprint">
					<div class="listViewActionsDiv row-fluid">
						<span class="span12 btn-toolbar">
							<div class="listViewActions pull-right">
								<div class="pageNumbers alignTop ">
									<span ng-show="paging.totRecords > paging.pageSize">
										<span class="pageNumbersText" style="padding-right: 5px">
											Record da <strong>{{paging.currentPage*paging.pageSize+1}}</strong> a
											<strong>{{(paging.currentPage*paging.pageSize) + results.length}}</strong>
											su
											<strong>{{paging.totRecords}}</strong>
										</span>
										<button title="Precedente" class="btn" id="listViewPreviousPageButton"
											type="button" ng-click="navigateToPreviousPage()" ng-show="paging.currentPage > 0">
											<span class="glyphicon glyphicon-chevron-left"></span>
										</button>
										<button title="Successiva" class="btn" id="listViewNextPageButton" type="button"
											ng-click="navigateToNextPage()" ng-show="paging.currentPage < paging.totPages">
											<span class="glyphicon glyphicon-chevron-right"></span>
										</button>
										<button title="Fine" class="btn" id="listViewNextPageButton" type="button"
											ng-click="navigateToEndPage()" ng-show="paging.currentPage < paging.totPages">
											<span class="glyphicon glyphicon-step-forward"></span>
										</button>
									</span>
								</div>
							</div>
						</span>
					</div>
				</div>
				<!-- end toolbar tabella | paginazione -->

				<!-- start table risultati -->
				<div class="listViewContentDiv" id="listViewContents">

					<div class="listViewEntriesDiv contents-bottomscroll">
						<div class="bottomscroll-div" style="width: 95%; min-height: 80px">
							<table class="table table-bordered listViewEntriesTable tableFormatted">
								<thead>
									<tr class="listViewHeaders">
										<!-- PRIMA DI CAMBIARE LA CLASSE DELLO SPAN, CONTROLLARE LA LOGICA SUL CONTROLLER DI ANGULAR. -->
										<th style="width:14%; text-align: center;">Società </th>
										<th style="width:14%; text-align: center;">Data </th>
										<th style="width:14%; text-align: center;">Stato </th>
										<th style="width:14%; text-align: center;">Opere totali </th>
										<th style="width:14%; text-align: center;">Opere acquisite </th>
										<th style="width:14%; text-align: center;">Esito
										</th>
										<th style="width:16%; text-align: center;">File </th>
									</tr>
								</thead>
								<tbody>
									<tr class="listViewEntries" ng-repeat="item in results">
										<td style="width:14%; text-align: center;">{{item.societa}}</td>
										<td style="width:14%; text-align: center;">
											{{item.dataInserimento | date: 'dd-MM-yyyy  &nbsp;&nbsp;  HH:mm'}}</td>
										<td style="width:14%; text-align: center;">{{item.stato}}</td>
										<td style="width:14%; text-align: center;">{{item.totaleOpere}}</td>
										<td style="width:14%; text-align: center;">{{item.totaleOpereAcquisite}}</td>
										<td style="width:14%; text-align: center;">{{item.esito}} &nbsp;
											<span ng-show="item.esito == 'KO'">
												<i class="fas fa-exclamation-circle" data-toggle="tooltip-error-message"
													title={{item.message}} data-html="true" rel="tooltip"></i>
												<script type="text/javascript">
													$('[data-toggle="tooltip-error-message"]').tooltip();
												</script>
											</span> </td>
										<td style="width:16%; text-align: center;"><button class="btn addButton" ng-click="downloadFile(item.id, item.fileName)">
												<span class="glyphicon glyphicon glyphicon-download"></span></button>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- end table risultati -->
			</div>
		</div>
	</div>
</div>
<!--<pre>{{ctrl | json}}</pre>-->