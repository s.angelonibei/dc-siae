<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%@
	include file="../navbar.jsp" %>

<div class="bodyContents" >

	<div class="mainContainer row-fluid" >
	
		<div id="companyLogo" class="navbar commonActionsContainer noprint">
			<div class="actionsContainer row-fluid">
				<div class="span2">
				<span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
				</div>
				<span class="btn-toolbar span5" style="margin-left: 0%">
					<div class="pageNumbers alignTop pull-right" style="text-align: center;">
		 				<span class="pageNumbersText"><strong>Gestione Mandati</strong></span>
					</div>
				</span>				
			</div>
		</div>

		<div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px;">
			<div class="listViewPageDiv">
				<div class="listViewTopMenuDiv noprint">

					<div class="alert alert-danger" ng-show="ctrl.messages.error" style="margin-top:20px">
						<button type="button" class="close" aria-hidden="true" ng-click="ctrl.messages.clear()">x</button>
						<span><strong>Attenzione: </strong>{{ctrl.messages.error}}</span>
					</div>
					<div class="alert alert-success" ng-show="ctrl.messages.info" style="margin-top:20px">
						<button type="button" class="close" aria-hidden="true" ng-click="ctrl.messages.clear()">x</button>
						<span>{{ctrl.messages.info}}</span>
					</div>

					<div class="listViewActionsDiv row-fluid">
						<span class="span12 btn-toolbar">
							<div class="listViewActions pull-right">
								<span class="btn-group">
									<button class="btn addButton" ng-click="nuovaSocietaTutela()">
										<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;<strong>Nuova Societa di
											Tutela</strong>
									</button>
								</span>
							</div>
							<div class="clearfix"></div>
						</span>
					</div>
				</div>
		
				<!-- start tabella società tutela -->
				<div class="listViewContentDiv" id="listViewContents">
		
					<div class="contents-topscroll noprint">
						<div class="topscroll-div" style="width: 95%">&nbsp;</div>
					</div>
		
					<div class="listViewEntriesDiv contents-bottomscroll">
						<div class="bottomscroll-div" style="width: 95%; min-height: 80px">
							<span class="btn-toolbar span7" style="margin-left:3.5%">
								<div class="pageNumbers alignTop pull-right" style="text-align: center;">
									 <span class="pageNumbersText"><strong>Società di Tutela </strong></span>
								</div>
							</span>
		
							<table class="table table-bordered listViewEntriesTable">
								<thead>
									<tr class="listViewHeaders">
		
										<th nowrap><a href="" class="listViewHeaderValues">Nominativo Società</a></th>
										<th nowrap><a href="" class="listViewHeaderValues">Home Territory</a></th>
										<th nowrap><a href="" class="listViewHeaderValues">Posizione SIAE</a></th>
										<th nowrap><a href="" class="listViewHeaderValues">Codice SIADA</a></th>
										<th nowrap><a href="" class="listViewHeaderValues" >Azioni</a></th>
									</tr>
								</thead>
								<tbody>
									<tr class="listViewEntries" ng-repeat="societa in ctrl.societaTutela">
										<td class="listViewEntryValue medium" nowrap>{{societa.nominativo}}</td>
										<td class="listViewEntryValue medium" nowrap>{{societa.homeTerritory.name}}</td>
										<td class="listViewEntryValue medium" nowrap>{{societa.posizioneSIAE}}</td>
										<td class="listViewEntryValue medium" nowrap>{{societa.codiceSIADA}}</td>
										<td>
											 <a ng-click="goToBlackList(societa)" title="BlackList" ><i class="fas fa-ban"></i></a>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<a  ng-show="societa.nominativo !='SIAE'"ng-click="goToDocumentazione(societa)" title="Documentazione"><i class="far fa-file-alt"></i></a>
											</span>	
										</td>
									</tr>
								</tbody>
							</table>
		
						</div>
					</div>
				</div>
				<!-- end tabella società tutela -->
			</div>
		</div>

		<div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px;">
			<div class="listViewPageDiv">
				<div class="listViewTopMenuDiv noprint">
					<div class="listViewActionsDiv row-fluid">
						<span class="span12 btn-toolbar">
							<div class="listViewActions pull-right">
								<span class="btn-group">
									<button class="btn addButton" ng-click="editMandato()">
										<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;<strong>Nuovo Mandato</strong>
									</button>
								</span>
							</div>
							<div class="clearfix"></div>
						</span>
					</div>
				</div>
		
				<!-- start tabella mandati -->
				<div class="listViewContentDiv" id="listViewContents">
		
					<div class="contents-topscroll noprint">
						<div class="topscroll-div" style="width: 95%">&nbsp;</div>
					</div>
		
					<div class="listViewEntriesDiv contents-bottomscroll">
						<div class="bottomscroll-div" style="width: 95%; min-height: 80px">
							<span class="btn-toolbar span7" style="margin-left:2%">
								<div class="pageNumbers alignTop pull-right" style="text-align: center;">
									 <span class="pageNumbersText"><strong>Mandato </strong></span>
								</div>
							</span>
		
							<table class="table table-bordered listViewEntriesTable">
								<thead>
									<tr class="listViewHeaders">		
										<th nowrap><a href="" class="listViewHeaderValues">Mandataria</a></th>
										<th nowrap><a href="" class="listViewHeaderValues">Mandante</a></th>
										<th nowrap><a href="" class="listViewHeaderValues">Data inizio mandato</a></th>
										<th nowrap><a href="" class="listViewHeaderValues">Data fine mandato</a></th>
										<th nowrap><a href="" class="listViewHeaderValues">Azioni</a></th>
									</tr>
								</thead>
								<tbody>
									<tr class="listViewEntries" ng-repeat="mandato in ctrl.mandati">
										<td class="listViewEntryValue medium" nowrap>{{mandato.mandataria.nominativo}}</td>
										<td class="listViewEntryValue medium" nowrap>{{mandato.mandante.nominativo}}</td>
										<td class="listViewEntryValue medium" nowrap>{{mandato.dataInizioMandato | date: 'dd-MM-yyyy'}}</td>
										<td class="listViewEntryValue medium" nowrap>{{mandato.dataFineMandato | date: 'dd-MM-yyyy'}}</td>
										<td>
											<a ng-click="eliminaMandato(mandato)"><i title="Elimina" class="glyphicon glyphicon-trash "></i></a>&nbsp;
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<a ng-click="editMandato(mandato)"><i title="Modifica" class="glyphicon glyphicon-edit "></i></a>&nbsp;
										</td>
									</tr>
								</tbody>
							</table>
		
						</div>
					</div>
				</div>
				<!-- end tabella mandati -->
			</div>
		</div>


		<div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px;">
				<div class="listViewPageDiv">
					<div class="listViewTopMenuDiv noprint">
						<div class="listViewActionsDiv row-fluid">
							<span class="span12 btn-toolbar">
								<div class="listViewActions pull-right">
									<span class="btn-group">
										<button class="btn addButton" ng-click="editConfigurazioneMandato()">
											<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;<strong>Nuova Configurazione Mandato</strong>
										</button>
									</span>
								</div>
								<div class="clearfix"></div>
							</span>
						</div>
					</div>
			
					<!-- start tabella mandati -->
					<div class="listViewContentDiv" id="listViewContents">
			
						<div class="contents-topscroll noprint">
							<div class="topscroll-div" style="width: 95%">&nbsp;</div>
						</div>
			
						<div class="listViewEntriesDiv contents-bottomscroll">
							<div class="bottomscroll-div" style="width: 95%; min-height: 80px">
								<span class="btn-toolbar span7" style="margin-left:5%">
									<div class="pageNumbers alignTop pull-right" style="text-align: center;">
										 <span class="pageNumbersText"><strong>Configurazione Mandato </strong></span>
									</div>
								</span>
			
								<table class="table table-bordered listViewEntriesTable">
									<thead>
										<tr class="listViewHeaders">		
											<th nowrap><a href="" class="listViewHeaderValues">Mandataria</a></th>
											<th nowrap><a href="" class="listViewHeaderValues">Mandante</a></th>
											<th nowrap><a href="" class="listViewHeaderValues">DSP</a></th>
											<th nowrap><a href="" class="listViewHeaderValues">Tipo di utilizzo</a></th>
											<th nowrap><a href="" class="listViewHeaderValues">Offerta commerciale</a></th>
											<th nowrap><a href="" class="listViewHeaderValues">Territorio</a></th>
											<th nowrap><a href="" class="listViewHeaderValues">Valida da</a></th>
											<th nowrap><a href="" class="listViewHeaderValues">Valida a</a></th>
											<th nowrap><a href="" class="listViewHeaderValues">Azioni</a></th>
										</tr>
									</thead>
									<tbody>
										<tr class="listViewEntries" ng-repeat="configurazioneMandato in ctrl.configurazioniMandato">
											<td class="listViewEntryValue medium" nowrap>{{configurazioneMandato.mandato.mandataria.nominativo}}</td>
											<td class="listViewEntryValue medium" nowrap>{{configurazioneMandato.mandato.mandante.nominativo}}</td>
											<td class="listViewEntryValue medium" nowrap>{{configurazioneMandato.dsp.name}}</td>
											<td class="listViewEntryValue medium" nowrap>{{configurazioneMandato.utilization.name}}</td>
											<td class="listViewEntryValue medium" nowrap>{{configurazioneMandato.offer.offering}}</td>
											<td class="listViewEntryValue medium" nowrap>{{configurazioneMandato.country.name}}</td>
											<td class="listViewEntryValue medium" nowrap>{{configurazioneMandato.validaDa | date: 'dd-MM-yyyy'}}</td>
											<td class="listViewEntryValue medium" nowrap>{{configurazioneMandato.validaA | date: 'dd-MM-yyyy'}}</td>
											<td>
												<a ng-click="eliminaConfigurazioneMandato(configurazioneMandato)"><i title="Elimina" class="glyphicon glyphicon-trash "></i></a>&nbsp;
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<a ng-click="editConfigurazioneMandato(configurazioneMandato)"><i title="Modifica" class="glyphicon glyphicon-edit "></i></a>&nbsp;
											</td>
										</tr>
									</tbody>
								</table>
			
							</div>
						</div>
					</div>
					<!-- end tabella mandati -->
				</div>
		</div>
	</div>
</div>
<!-- <pre>{{ctrl | json}}</pre> -->