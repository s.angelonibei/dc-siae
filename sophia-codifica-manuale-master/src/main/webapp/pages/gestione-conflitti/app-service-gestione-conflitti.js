codmanApp.service('gestioneConflittiService', ['$http', '$location', 'Upload', function ($http, $location, Upload) {

    var service = this;
    const baseUrl = "api/ms-gestione-conflitti/gestioneConflitti";
    const getlist = "getRequestList";
    const createRequest = "addRequest";
    const getSchemiRiparto = "rest/royalties-search/list-royalties-storages";

    service.prefixUrlRequest = `${$location.protocol()}://${$location.host()}:${$location.port()}/${baseUrl}/`;

    /* 
       input parameter:
            file: fileToUpload
            schemiRiparto: {
                year: "string",
                month: "string"
            },
            territori: string, "IT", "R"
            creatoDa: string
    */
     service.postReportRequest = function (request) {
        const {file} = request;
        const data = _.omit(request, 'file');
        return Upload.upload({
            url: service.prefixUrlRequest + createRequest,
            file,
            data
        }).then((resp) => {
            resolve(true);
        }, (response) => {
            reject(response);
        });/* 
        return $http({
            method: 'POST',
            url: service.prefixUrlRequest + createRequest,
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                ...request
            }
        }); */

    };

    service.downloadFile = function (id) {
        return $http({
            method: 'GET',
            responseType: 'blob',
            url: service.prefixUrlRequest + 'invoice/download648/' + id
        });
    };

    /* 
        * pagination
        * params : {first: number, last: number} 
    */
    service.getRequestList = function (params) {
        // for test porpose only
        /* return new Promise((resolve, reject) => {
            $http.get('./pages/ripartizione/lookThrough/mock/getRequestList.json')
                .then((data) => {
                    resolve(data);
                });
        }); */
        return new Promise((resolve, reject) => {
            $http({
                method: 'GET',
                url: service.prefixUrlRequest + getlist,
                params
            })
                .then(function successCallback({ data }) {
                    resolve(data);
                }, function errorCallback(response) {
                    reject(response);
                });
        });
    };

    /* dumps: [string] */
    service.getSchemiRiparto = function (params) {
        return new Promise((resolve, reject) => {
            $http({
                method: 'GET',
                url: getSchemiRiparto,
                params
            })
                .then(function successCallback({ data }) {
                    resolve(data);
                }, function errorCallback(response) {
                    reject(response);
                });
        });
    };

    service.getTerritori = function () {
        return new Promise((resolve) => {
            resolve([{
                label: 'Italia',
                code: 'IT'
            }, {
                label: 'Resto del mondo',
                code: 'R'
            }]);
        });
    };

}]);