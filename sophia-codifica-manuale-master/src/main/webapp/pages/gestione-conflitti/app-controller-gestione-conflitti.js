codmanApp.controller('gestioneConflittiCtrl', ['$scope', '$interval', 'gestioneConflittiService',
    function ($scope, $interval, gestioneConflittiService) {
        const DUMPS = ["SIAE", "IPI"];
        $scope.pollingRequestReport;
        $scope.schemiRiparto;
        $scope.ipi;
        $scope.queryParams = {
            first: 0,
            last: 50
        };
		
		$scope.search = {
			name: '',
			stato: ''
		};
        $scope.upload = {
            file: null
        };
        // todo take from service 
        $scope.territoryCheckboxes = [{
            label: 'Italia',
            checked: false,
            code: 'IT'
        }, {
            label: 'Tutti gli altri Territori (si applicherà lo schema di reparto FR)',
            checked: false,
            code: 'R'
        }];

        $scope.siaeDate;
        $scope.defaultPaging = {
            maxRows: 50,
            hasNext: false,
            hasPrev: false,
            firstRecord: 1,
            lastRecord: 50,
        };
        $scope.paging = { ...$scope.defaultPaging };
        $scope.headerTitles = ["Data Acquisizione", "Nome", "Stato", "Schemi riparto", "File intermedio", "File output"];

        $scope.messages = {
            info: "",
            error: "",
            warning: ""
        };
        // list of request
        $scope.gestioneConflittiDataTable = [];

        $scope.clearMessages = function () {
            $scope.messages = {
                info: "",
                error: "",
                warning: ""
            }
        };

        $scope.warningMessage = function (message) {
            setTimeout(() => {
                $scope.messages.warning = message;
                $scope.$apply();
            }, 100);
        };

        $scope.infoMessage = function (message) {
            setTimeout(() => {
                $scope.messages.info = message;
                $scope.$apply();
            }, 100);
        };

        $scope.errorMessage = function (message) {
            setTimeout(() => {
                $scope.messages.error = message;
                $scope.$apply();
            }, 100);
        };

        $scope.onInit = function () {

            gestioneConflittiService
                .getSchemiRiparto({
                    dumps: DUMPS
                })
                .then(({ storages = [] }) => {
                    const labelsValue = Object.entries(_.groupBy(storages, "dump"))
                        .reduce((acc, [key, values]) => ({ ...acc, [key]: _.last(values) }), {});
                    const { SIAE = {}, IPI = {} } = labelsValue;
                    const { month: mS = "", year: yS = "", updatedDate: upS = "" } = SIAE;
                    const { month: mI = "", year: yI = "", updatedDate: upI = "" } = IPI;
                    setTimeout(() => {
                        $scope.siaeDate = {
                            year: yS,
                            month: mS
                        };
                        $scope.schemiRiparto = `${mS}/${yS} - ${upS}`;
                        $scope.ipi = `${mI}/${yI} - ${upI}`;
                        $scope.$apply();
                    }, 100);

                }, function (error) {
                    $scope.errorMessage('Qualcosa è andato storto.');
                });
            // get Requests List - reques is done with an interval of 30 sec to update data
            getRequestList();
            $scope.pollingRequestReport = $interval(getRequestList, 30000);
        }

        $scope.onRefresh = function() {
			$scope.search = {
				name: '',
                stato: '',
                schemiRiparto: ''
			};
            $scope.queryParams = _.omit($scope.queryParams, Object.keys($scope.search));
            getRequestList();
        };
		$scope.onFilterSearch = function() {
            Object.assign($scope.queryParams, {
                ...$scope.search
            });
            getRequestList();
        };
		

        $scope.navigateToNextPage = function () {
            const first = $scope.queryParams.first + $scope.paging.maxRows;
            const last = $scope.queryParams.last + $scope.paging.maxRows;
            // update first, last value
            $scope.queryParams = {
                first,
                last
            };
            getRequestList();
        };

        $scope.navigateToPreviousPage = function () {
            const first = $scope.queryParams.first - $scope.paging.maxRows;
            const last = $scope.queryParams.last - $scope.paging.maxRows;
            // update first, last value
            $scope.queryParams = {
                first,
                last
            };
            getRequestList();
        };

        function getRequestList() {
            gestioneConflittiService
                .getRequestList(_.omitBy($scope.queryParams, (e) => _.isNil(e) || e === ""))
                .then(data => {
                    setTimeout(() => {
                        const { rows, first, last, hasNext, hasPrev } = data;

                        $scope.gestioneConflittiDataTable = rows
                            .map((richiesta) => {
                                const stato = richiesta.stato.toLowerCase() === "in corso" ?
                                    'In lavorazione' :
                                    richiesta.stato.charAt(0).toUpperCase() + richiesta.stato.slice(1).toLowerCase();

                                return {
                                    ...richiesta,
                                    stato,
                                    dataAcquisizione: moment(richiesta.dataAcquisizione).format("YYYY-MM-DD HH:mm:ss"),
                                    ultimoAggiornamento: moment(richiesta.ultimoAggiornamento).format("YYYY-MM-DD HH:mm:ss")
                                }
                            });

                        if (!$scope.gestioneConflittiDataTable.length) {
                            $scope.paging = { ...$scope.defaultPaging };
                            // $scope.paging.numberOfItems = 0;
                            $scope.clearMessages();
                            $scope.infoMessage("Al momento non c'è uno storico richieste.");
                            return;
                        }

                        $scope.paging = {
                            ...$scope.paging,
                            firstRecord: first + 1,
                            lastRecord: last,
                            hasNext,
                            hasPrev
                        };
                        $scope.$apply();
                    }, 100);
                }, _error => {
                    $scope.clearMessages();
                    $scope.errorMessage("Si è verificato un problema con lo storico richieste, riprovare più tardi.");
                });
        };

        $scope.generaRichiesta = function (file, userName) {
            $scope.clearMessages();
            const checkboxSelected = $scope.territoryCheckboxes
                .filter(el => el.checked)
                .reduce((acc, val) => (acc.length) ? "E" : val.code, "");
            if (file && $scope.schemiRiparto && checkboxSelected &&
                userName) {
                const { year: anno, month: mese } = $scope.siaeDate;
                const request = {
                    file,
                    anno,
                    mese,
                    territorio: checkboxSelected,
                    creatoDa: userName
                };
                gestioneConflittiService.postReportRequest(request)
                    .then(_data => {
                        $scope.infoMessage("Richiesta effettuata con successo.");
                        // per aggiornare i dati della tabella.
                        getRequestList();
                    }, ({ data: { message } }) => {
                        $scope.errorMessage(message || "Si è verificato un problema, riprovare più tardi.");
                    });
            } else {
                $scope.errorMessage("Compilare tutti i valori richiesti.");
            }
        };

        $scope.$on("$destroy", () => {
            $interval.cancel($scope.pollingRequestReport);
        });

        $scope.onInit();

    }]);