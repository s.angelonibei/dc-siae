'<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <%@ include file="../navbar.jsp" %>

        <div class="bodyContents">

            <div class="mainContainer row-fluid">

                <div class="contents-topscroll noprint">
                    <div class="topscroll-div" style="width: 95%; margin-bottom: 20px">&nbsp;</div>
                </div>

                <div class="sticky">
                    <div id="companyLogo" class="navbar commonActionsContainer noprint">
                        <div class="actionsContainer row-fluid">
                            <div class="span2">
                                <span class="companyLogo">
                                    <img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;
                                </span>
                            </div>
                            <span class="btn-toolbar span8">
                                <span class="customFilterMainSpan btn-group" style="text-align: center;">
                                    <div class="pageNumbers alignTop ">
                                        <span class="pageNumbersText">
                                            <strong>Gestione conflitti Youtube</strong>
                                        </span>
                                    </div>
                                </span>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="contentsDiv marginLeftZero" style="min-height: 80px;" id="gestioneConflittiContainer">
                    <div class="listViewPageDiv">
                        <div class="messagesContainer">
                            <div class="alert alert-danger" ng-show="messages.error && (!messages.info)">
                                <button type="button" class="close" ng-click="clearMessages()"
                                    aria-hidden="true">x</button>
                                <span><strong>Errore: </strong>{{messages.error}}</span>
                            </div>
                            <div class="alert alert-success" ng-show="messages.info && (!messages.error)">
                                <button type="button" class="close" ng-click="clearMessages()"
                                    aria-hidden="true">x</button>
                                <span>{{messages.info}}</span>
                            </div>
                        </div>

                        <div class="listViewActionsDiv row-fluid" style="padding-top: 8px;">
                            <!-- Campi di ricerca -->

                            <div class="equalSplit table-bordered flexRowHNoneVCenter paddingLeftx8"
                                style="background-color: #f5f5f5;">
                                <span class="glyphicon glyphicon-expand paddingRightLeftx4">
                                </span>
                                <div class="blockHeader titleBlockFont " colspan="6">Generazione richiesta</div>
                            </div>
                            <div class="table-bordered flexRowCenterSpaceBtw " style="padding:8px;">

                                <div class="listViewTopMenuDiv noprint flex-25">
                                    <div class="listViewActionsDiv row-fluid">
                                        <div class="row-fluid table-bordered uploadContainer textAlignCenter" ngf-drop
                                            ng-model="upload.file" ngf-allow-dir="false">
                                            <div class="row-fluid" ng-hide="!upload.file" style="margin-bottom: 5px">
                                                <strong>{{upload.file.name}}</strong>
                                                {{upload.file.$error}} {{upload.file.$errorParam}}
                                            </div>
                                            <div class="row-fluid" style="margin-top: 20px; margin-bottom: 20px">Drag
                                                &amp; Drop file
                                            </div>
                                            <div ngf-no-file-drop>File Drag/Drop is not supported for this browser</div>
                                            <div class="flexRowCenterEnd marginRightLeft4x margin-bottom-2x">
                                                <button class="btn addButton floatRight marginRightLeft4x" type="file"
                                                    ngf-select name="file" required ng-model="upload.file"
                                                    ngf-allow-dir="false">carica
                                                    file</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="flexColumnStretch">
                                    <div class="margin-2x">Territorio</div>
                                    <md-input-container class="flexColumnCenterStart">
                                        <md-checkbox ng-model="territoryCheckboxes[0].checked"
                                            ng-change="onCheckboxOptionEvent()">
                                            {{ territoryCheckboxes[0].label}}
                                        </md-checkbox>
                                        <md-checkbox ng-model="territoryCheckboxes[1].checked"
                                            ng-change="onCheckboxOptionEvent()">
                                            {{ territoryCheckboxes[1].label}}
                                        </md-checkbox>
                                    </md-input-container>
                                </div>

                                <div class="flexColumnStretch">
                                    <div class="flexRowHCenterVCenter">
                                        <div class="flex-15">Schemi riparto:</div>
                                        <div class="fieldValue medium flex-100">
                                            <input type="text" disabled class="form-control maxWidth15"
                                                ng-model="schemiRiparto">
                                        </div>
                                    </div>
                                    <div class="flexRowHCenterVCenter">
                                        <div class="flex-15">IPI:</div>
                                        <div class="fieldValue medium flex-100">
                                            <input type="text" disabled class="form-control maxWidth15" ng-model="ipi">
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="equalSplit table-bordered"
                                style="padding: 6px 0px; background-color: #f5f5f5;margin: 0px;">
                                <div class="flex-100"
                                    style="display:flex;justify-content:flex-end;flex: right;width: 100%;">
                                    <button class="btn addButton marginRightLeftx4"
                                        ng-click="generaRichiesta(upload.file, '<%=userName%>')">
                                        <span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;
                                        <strong>Genera richiesta</strong>
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="flexColumnStretch">
                            <!-- pagination -->
                            <div class="paddingTopBottomx8">
                                <span class="flexRowHCenterVCenter pull-right"
                                    ng-show="paging.hasNext || paging.hasPrev">
                                    <span class="pageNumbersText" style="padding-right: 5px">
                                        Record da <strong>{{paging.firstRecord}}</strong> a
                                        <strong>{{paging.lastRecord}}</strong>
                                    </span>
                                    <button title="Precedente" class="btn marginRightLeftx4"
                                        id="listViewPreviousPageButton" type="button"
                                        ng-click="navigateToPreviousPage()" ng-show="paging.hasPrev">
                                        <span class="glyphicon glyphicon-chevron-left"></span>
                                    </button>
                                    <button title="Successiva" class="btn marginRightLeftx4" id="listViewNextPageButton"
                                        type="button" ng-click="navigateToNextPage()" ng-show="paging.hasNext">
                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                    </button>
                                </span>
                            </div>
                            <div id="gestioneConflittiTable">
                                <div class="table-responsive text-nowrap horizontal-scroll">
                                    <table class="table table-bordered listViewEntriesTable">
                                        <thead>
                                            <tr>
                                                <th ng-repeat="head in headerTitles">{{head}}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <!-- Filter Search -->
                                            <tr>
                                                <td>

                                                </td>
                                                <td>
                                                    <div class="flexRowHCenterVCenter">
                                                        <div class="fieldValue medium flex-100">
                                                            <input type="text" class="form-control"
                                                                ng-model="search.name">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="flexRowHCenterVCenter">
                                                        <div class="fieldValue medium flex-100">
                                                            <input type="text" class="form-control"
                                                                ng-model="search.stato">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="flexRowHCenterVCenter">
                                                        <div class="fieldValue medium flex-100">
                                                            <input type="text" class="form-control"
                                                                ng-model="search.schemiRiparto">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>

                                                </td>
                                                <td>
                                                    <div class="flexRowCenterSpaceEvenly margin-top-x">
                                                        <button class="btn" ng-click="onFilterSearch()"><span
                                                                class="glyphicon glyphicon-search"></span></button>
                                                        <button class="btn" ng-click="onRefresh()"><span
                                                                class="glyphicon glyphicon-refresh"></span></button>
                                                    </div>
                                                </td>
                                            </tr>
                                            <!-- data -->
                                            <tr mdbTableCol
                                                ng-repeat="item in gestioneConflittiDataTable track by $index">
                                                <td>
                                                    <div style="padding-left: 10px">
                                                        {{item.dataAcquisizione}}
                                                    </div>
                                                </td>
                                                <td>
                                                    <div style="padding-left: 10px">
                                                        {{item.file}}
                                                    </div>
                                                </td>
                                                <td>
                                                    <div style="padding-left: 10px">
                                                        <span ng-class="{
                                                    'glyphicon glyphicon-ok-sign success-icon': item.stato.toLowerCase() == 'completata',
                                                    'glyphicon glyphicon-remove-sign error-icon': item.stato.toLowerCase() == 'errore',
                                                    'glyphicon glyphicon-retweet progress-icon': item.stato.toLowerCase() == 'in corso', 
                                                    'glyphicon glyphicon glyphicon-dashboard': item.stato.toLowerCase() == 'caricato'
                                                  }" aria-hidden="true">
                                                        </span>
                                                        {{item.stato}}
                                                    </div>
                                                </td>
                                                <td>
                                                    <div style="padding-left: 10px">
                                                        {{item.schemiRiparto}}
                                                    </div>
                                                </td>
                                                <td>
                                                    <div style="padding-left: 10px">

                                                    </div>
                                                    <span data-ng-if="item.ytStaging">
                                                        <a download="{{item.ytStaging}}"
                                                            data-ng-href="downloadExtendedDSR?url={{item.ytStaging}}">
                                                            <i title={{item.ytStaging}}
                                                                class="glyphicon glyphicon-floppy-save alignMiddle"></i>
                                                        </a>
                                                    </span>
                                                </td>
                                                <td>
                                                    <div style="padding-left: 10px">

                                                    </div>
                                                    <span data-ng-if="item.ytAudio">
                                                        <a download="{{item.ytAudio}}"
                                                            data-ng-href="downloadExtendedDSR?url={{item.ytAudio}}">
                                                            <i title={{item.ytAudio}}
                                                                class="glyphicon glyphicon-floppy-save alignMiddle"></i>
                                                        </a>
                                                    </span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div ng-show="!gestioneConflittiDataTable.length" style="margin-top: 16px;">
                                        <div style="text-align: center;">Al momento non ci sono record.</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>'