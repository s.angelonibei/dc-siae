<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../navbar.jsp"%>
<!-- path has to be adjusted-->

<div class="bodyContents">
	<div class="mainContainer row-fluid">

		<div class="contents-topscroll noprint">
			<div class="topscroll-div" style="width: 95%;margin-bottom: 20px">&nbsp;</div>
		</div>

		<div class="sticky">
			<div id="companyLogo" class="navbar commonActionsContainer noprint">
				<div class="actionsContainer row-fluid">
					<div class="span2">
						<span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
					</div>
					<span class="btn-toolbar span5">
						<div class="pageNumbers alignTop pull-right" style="text-align: center;">
							<span id="up" class="pageNumbersText"><strong>{{ctrl.pageTitle}}</strong></span>
						</div>
					</span>
				</div>
			</div>
		</div>

		<div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px;margin-top: 25px;">
			<div class="listViewPageDiv">
				<div class="alert alert-warning" ng-show="ctrl.messages.warning" style="margin-top:20px">
					<button type="button" class="close" ng-click="ctrl.messages.clear()" aria-hidden="true">x</button>
					<span>{{ctrl.messages.warning}}</span>
				</div>
				<div class="alert alert-danger" ng-show="ctrl.messages.error" style="margin-top:20px">
					<button type="button" class="close" ng-click="ctrl.messages.clear()" aria-hidden="true">x</button>
					<span><strong>Attenzione: </strong>{{ctrl.messages.error}}</span>
				</div>
				<div class="alert alert-success" ng-show="ctrl.messages.info" style="margin-top:20px">
					<button type="button" class="close" ng-click="ctrl.messages.clear()" aria-hidden="true">x</button>
					<span>{{ctrl.messages.info}}</span>
				</div>
			</div>

			<%--<div class="listViewPageDiv">--%>
			<%--<div class="listViewActionsDiv row-fluid">--%>
			<%--<span class="span12 btn-toolbar">--%>
			<%--<div class="listViewActions pull-right">--%>
			<%--<span class="btn-group">--%>
			<%--<button class="btn addButton" data-ng-click="startAllProcesses()">--%>
			<%--<span class="glyphicon glyphicon-play"></span>&nbsp;&nbsp;<strong>Avvia Tutte</strong>--%>
			<%--</button>--%>
			<%--</span>--%>
			<%--</div>--%>
			<%--<div class="clearfix"></div>--%>
			<%--</span>--%>
			<%--</div>--%>
			<%--</div>			--%>

			<div class="listViewPageDiv" data-ng-repeat="process in ctrl.processes">
				<div class="listViewActionsDiv row-fluid">
					<table class="table table-bordered blockContainer showInlineTable equalSplit noHoverTable">
						<thead>
						<tr>
							<th class="blockHeader" colspan="6" ng-show="process.collapseHistory"><span
									style="cursor: pointer;"
									data-ng-click="expandExecutions(process)"
									class="glyphicon glyphicon-expand"></span>&nbsp;<span>{{process.description}}</span>

							</th>
								<th class="blockHeader" colspan="6" ng-hide="process.collapseHistory"><span style="cursor: pointer;"
										data-ng-click="expandExecutions(process)"
										class="glyphicon glyphicon-collapse-down"></span>&nbsp;<span>{{process.description}}</span>
									<span class="pull-right">
										<button type="button" class="btn addButton" data-ng-click="periodSelection()"
												data-ng-show="process.processID === 'UPDATE_RAYALTIES'">
											<span
													class="glyphicon glyphicon-play"></span>&nbsp;&nbsp;<strong>Avvia</strong>
										</button>
										<button type="button" class="btn addButton" data-ng-click="startKb()"
												data-ng-show="process.processID === 'UPDATE_KB'">
											<span
													class="glyphicon glyphicon-play"></span>&nbsp;&nbsp;<strong>Avvia</strong>
										</button>
										<button type="button" class="btn addButton" data-ng-click="startDocumentation()"
												data-ng-show="process.processID === 'UPDATE_DOCS'">
											<span
													class="glyphicon glyphicon-play"></span>&nbsp;&nbsp;<strong>Avvia SIAE</strong>
										</button>
										<button type="button" class="btn addButton"
												data-ng-click="startUcmrAdaDocumentation()"
												data-ng-show="process.processID === 'UPDATE_DOCS'">
											<span
													class="glyphicon glyphicon-play"></span>&nbsp;&nbsp;<strong>Avvia UCMR-ADA</strong>
										</button>
									</span>
								</th>
						</tr>
						</thead>
						<tbody ng-hide="process.collapseHistory">
						<tr>
							<td style="padding: 0;">
								<table class="equalSplit noHoverTable" style="width:100%">
									<thead>
											<tr>
												<th data-ng-repeat="(key, val) in process.executions[0]">{{ key | tableHeader}}</th>
												<%--<th style="text-align: center; width: 25%;">
													Lanciato da
												</th>
												<th style="text-align: center; width: 25%;">
													Data Inizio Elaborazione
												</th>
												<th style="text-align: center; width: 25%;">
													Data Fine Elaborazione
												</th>
												<th style="text-align: center; width: 25%;">
													Esito
												</th>--%>
											</tr>
										</thead>
										<tbody data-ng-hide="process.processID === 'UPDATE_RAYALTIES' || process.processID === 'UPDATE_KB'">
										<tr data-ng-repeat="item in process.executions">
											<td data-ng-repeat="column in item">
												{{ column | columsDecoder }}
											</td>
										</tr>
										</tbody>
										<tbody data-ng-show="process.processID === 'UPDATE_RAYALTIES' || process.processID === 'UPDATE_KB'"
											   data-ng-repeat="(key, value) in process.executions | groupBy: 'id_lancio'">
										<tr>
											<td rowspan="{{value.length}}">{{key}}</td>
											<td data-ng-repeat="column in value[0]">{{column | columsDecoder }}</td>
										</tr>
										<tr data-ng-repeat="item in value" data-ng-if="!$first">
											<td data-ng-repeat="column in item">{{column | columsDecoder}}</td>
										</tr>
										</tbody>
									</table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>

        </div>
	</div>
</div>
<!-- <pre>{{ctrl | json}}</pre> -->