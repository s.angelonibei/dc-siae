<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%@
	include file="../navbar.jsp" %>

<div class="bodyContents" >

	<div class="mainContainer row-fluid" >
	
		<div id="companyLogo" class="navbar commonActionsContainer noprint">
			<div class="actionsContainer row-fluid">
				<div class="span2">
<!--  -- >			<span class="companyLogo"><img src="images/siae_logo_siaetrasparente.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
<!--  -->			<span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
				</div>
				<span class="btn-toolbar span4">
					<div class="pageNumbers alignTop pull-right" style="text-align: center;">
		 				<span class="pageNumbersText"><strong>Messaggi Service Bus</strong></span>
					</div>
				</span>				
			</div>
		</div>
	
		<div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 180px;"> 
			<div class="listViewPageDiv">

				<div class="listViewTopMenuDiv noprint">
					<div class="listViewActionsDiv row-fluid">
					
						<span class="btn-toolbar span3">
							<span class="btn-group">

 							</span>
						</span>

						<span class="btn-toolbar span4">
						</span>
												 						
						<span class="span5 btn-toolbar">
							<div class="listViewActions pull-right">
							
						 		<div class="pageNumbers alignTop ">
						 			<span ng-show="ctrl.searchResults.hasPrev || ctrl.searchResults.hasNext">
						 				<span class="pageNumbersText" style="padding-right:5px">
						 					Record da <strong>{{ctrl.searchResults.first+1}}</strong> a <strong>{{ctrl.searchResults.last}}</strong>
						 				</span>
					 					<button title="Precedente" class="btn" id="listViewPreviousPageButton" type="button" 
					 							ng-click="navigateToPreviousPage()"
					 							ng-show="ctrl.searchResults.hasPrev">
					 						<span class="glyphicon glyphicon-chevron-left"></span>
					 					</button>
					 					<button title="Successiva" class="btn" id="listViewNextPageButton" type="button"
					 							ng-click="navigateToNextPage()"
					 							ng-show="ctrl.searchResults.hasNext">
					 						<span class="glyphicon glyphicon-chevron-right"></span>
					 					</button>
					 				</span>
						 		</div>
					 			
					 		</div>
					 		<div class="clearfix"></div>
					 	</span>
					</div>
				</div>
					
				<!-- table -->
				<div class="listViewContentDiv" id="listViewContents" >
<!--  -- >
					 <div class="contents-topscroll noprint">
					 	<div class="topscroll-div" style="width: 95%;">&nbsp;</div>
					 </div>
<!--  -->
					 <div class="listViewEntriesDiv contents-bottomscroll">
					 	<div class="bottomscroll-div" style="width: 95%; min-height: 400px">
					 		
					 		<table class="table table-bordered listViewEntriesTable">

					 		<thead>
					 		<tr class="listViewHeaders">
						 		<th nowrap><a ng-click="setSortType('queueName');" class="listViewHeaderValues" >Coda
						 			<span ng-show="ctrl.sortType=='queueName' && !ctrl.sortReverse" class="glyphicon glyphicon-sort-by-attributes"></span>
	            					<span ng-show="ctrl.sortType=='queueName' && ctrl.sortReverse" class="glyphicon glyphicon-sort-by-attributes-alt"></span>
            					</a></th>
						 		<th nowrap><a ng-click="setSortType('insertTime');" class="listViewHeaderValues" >Data
						 			<span ng-show="ctrl.sortType=='insertTime' && !ctrl.sortReverse" class="glyphicon glyphicon-sort-by-attributes"></span>
	            					<span ng-show="ctrl.sortType=='insertTime' && ctrl.sortReverse" class="glyphicon glyphicon-sort-by-attributes-alt"></span>
            					</a></th>
						 		<th nowrap><a ng-click="setSortType('idDsr');" class="listViewHeaderValues" >DSR
						 			<span ng-show="ctrl.sortType=='idDsr' && !ctrl.sortReverse" class="glyphicon glyphicon-sort-by-alphabet"></span>
	            					<span ng-show="ctrl.sortType=='idDsr' && ctrl.sortReverse" class="glyphicon glyphicon-sort-by-alphabet-alt"></span>
            					</a></th>
						 		<th nowrap><a ng-click="setSortType('idDsp');" class="listViewHeaderValues" >DSP
						 			<span ng-show="ctrl.sortType=='idDsp' && !ctrl.sortReverse" class="glyphicon glyphicon-sort-by-alphabet"></span>
	            					<span ng-show="ctrl.sortType=='idDsp' && ctrl.sortReverse" class="glyphicon glyphicon-sort-by-alphabet-alt"></span>
            					</a></th>
						 		<th nowrap><a ng-click="setSortType('queueType');" class="listViewHeaderValues" >Tipo
						 			<span ng-show="ctrl.sortType=='queueType' && !ctrl.sortReverse" class="glyphicon glyphicon-sort-by-alphabet"></span>
	            					<span ng-show="ctrl.sortType=='queueType' && ctrl.sortReverse" class="glyphicon glyphicon-sort-by-alphabet-alt"></span>
            					</a></th>
						 		<th nowrap><a ng-click="setSortType('serviceName');" class="listViewHeaderValues" >Modulo
						 			<span ng-show="ctrl.sortType=='serviceName' && !ctrl.sortReverse" class="glyphicon glyphicon-sort-by-alphabet"></span>
	            					<span ng-show="ctrl.sortType=='serviceName' && ctrl.sortReverse" class="glyphicon glyphicon-sort-by-alphabet-alt"></span>
            					</a></th>
						 		<th nowrap><a ng-click="setSortType('month');" class="listViewHeaderValues" >Mese
						 			<span ng-show="ctrl.sortType=='month' && !ctrl.sortReverse" class="glyphicon glyphicon-sort-by-alphabet"></span>
	            					<span ng-show="ctrl.sortType=='month' && ctrl.sortReverse" class="glyphicon glyphicon-sort-by-alphabet-alt"></span>
            					</a></th>
						 		<th nowrap><a ng-click="setSortType('year');" class="listViewHeaderValues" >Anno
						 			<span ng-show="ctrl.sortType=='year' && !ctrl.sortReverse" class="glyphicon glyphicon-sort-by-alphabet"></span>
	            					<span ng-show="ctrl.sortType=='year' && ctrl.sortReverse" class="glyphicon glyphicon-sort-by-alphabet-alt"></span>
            					</a></th>
						 		<th nowrap><a ng-click="setSortType('country');" class="listViewHeaderValues" >Nazionalit&agrave;
						 			<span ng-show="ctrl.sortType=='country' && !ctrl.sortReverse" class="glyphicon glyphicon-sort-by-alphabet"></span>
	            					<span ng-show="ctrl.sortType=='country' && ctrl.sortReverse" class="glyphicon glyphicon-sort-by-alphabet-alt"></span>
            					</a></th>
						 		<th nowrap><a ng-click="setSortType('hostname');" class="listViewHeaderValues" >Hostname
						 			<span ng-show="ctrl.sortType=='hostname' && !ctrl.sortReverse" class="glyphicon glyphicon-sort-by-alphabet"></span>
	            					<span ng-show="ctrl.sortType=='hostname' && ctrl.sortReverse" class="glyphicon glyphicon-sort-by-alphabet-alt"></span>
            					</a></th>
					 			<th nowrap></th>
					 		</tr>
					 		</thead>
					 		<tbody>
		
					 		<tr style="background-color: #e7e7e7 !important;">
					 			<td nowrap><div class="row-fluid">
					 					<select class="span10 listSearchContributor"
					 							ng-model="ctrl.queueName" ng-change="onRicerca()">
					 						<option value="" >(Tutte)</option>
					 						<option value="{{item}}" ng-repeat="item in ctrl.decode.queueNames" >{{item}}</option>
					 					</select>
					 				</div>
					 			</td>
					 			<td nowrap>
									<input type="text" class="span9 input-small ctrl_insertTime"
										style="width: 80px"
										ng-model="ctrl.insertTime" ng-change="onRicerca()"> 
									<script type="text/javascript">
									    $(".ctrl_insertTime").datepicker({
									    	format: 'yyyy-mm-dd', 
									    	language: 'it',
									    	autoclose: true,
									    	todayHighlight: true
									    });
									</script>
									<span class="glyphicon glyphicon-erase" style="margin-top: 6px;"
										ng-click="ctrl.insertTime=null; onRicerca()"></span> 
					 			</td>
					 			<td nowrap><div class="row-fluid">
						 			<angucomplete-alt id="idDsr"
						 				initial-value="ctrl.idDsr"
										placeholder="Nome DSR"
										pause="200"
										selected-object="selectIdDsr"
										remote-url="rest/service-bus/searchIdDsrs?idDsr="
										search-fields="name"
  										title-field="name"
										minlength="1"
										input-class=" listSearchContributor"/>
					 				</div>
					 			</td>
					 			<td nowrap><div class="row-fluid">
					 					<select class="span10 listSearchContributor"
					 							ng-model="ctrl.idDsp" ng-change="onRicerca()">
					 						<option value="" >(Tutti)</option>
					 						<option value="{{key}}" ng-repeat="(key, value) in ctrl.decode.idDsps" >{{value}}</option>
					 					</select>
					 				</div>
					 			</td>
					 			<td nowrap><div class="row-fluid">
					 					<select class="span10 listSearchContributor"
					 							ng-model="ctrl.queueType" ng-change="onRicerca()">
					 						<option value="" >(Tutti)</option>
					 						<option value="{{item}}" ng-repeat="item in ctrl.decode.queueTypes" >{{item}}</option>
					 					</select>
					 				</div>
					 			</td>
					 			<td nowrap><div class="row-fluid">
					 					<select class="span10 listSearchContributor"
					 							ng-model="ctrl.serviceName" ng-change="onRicerca()">
					 						<option value="" >(Tutti)</option>
					 						<option value="{{item}}" ng-repeat="item in ctrl.decode.serviceNames" >{{item}}</option>
					 					</select>
					 				</div>
					 			</td>
					 			<td nowrap><div class="row-fluid">
					 					<select class="span10 listSearchContributor"
					 							ng-model="ctrl.month" ng-change="onRicerca()">
					 						<option value="" >(Tutti)</option>
					 						<option value="{{item.key}}" ng-repeat="item in ctrl.decode.months" >{{item.value}}</option>
					 					</select>
					 				</div>
					 			</td>
					 			<td nowrap><div class="row-fluid">
					 					<select class="span10 listSearchContributor"
					 							ng-model="ctrl.year" ng-change="onRicerca()">
					 						<option value="" >(Tutti)</option>
					 						<option value="{{item}}" ng-repeat="item in ctrl.decode.years" >{{item}}</option>
					 					</select>
					 				</div>
					 			</td>
					 			<td nowrap><div class="row-fluid">
					 				<angucomplete-alt id="country"
										placeholder="Nazionalit&agrave;"
										pause="200"
										selected-object="selectCountry"
										remote-url="rest/anagCountry/search?name="
										search-fields="name"
  										title-field="name"
										minlength="1"
										input-class="input-small listSearchContributor"/>
					 			</td>
					 			<td nowrap><div class="row-fluid">
						 			<angucomplete-alt id="hostname"
						 				initial-value="ctrl.hostname"
										placeholder="Hostname"
										pause="200"
										selected-object="selectHostname"
										remote-url="rest/service-bus/searchHostnames?hostname="
										search-fields="name"
  										title-field="name"
										minlength="1"
										input-class="input-small listSearchContributor"/>
					 				</div>
					 			</td>
					 			<td nowrap>
						 			<div class="pull-right">
						 				<button class="btn" ng-click="onAzzera(); onRicerca()"><span class="glyphicon glyphicon-erase"></span></button>
						 				&nbsp;
						 				<button class="btn " ng-click="onRicerca()"><span class="glyphicon glyphicon-search"></span></button>
						 			</div>
					 			</td>
					 		</tr>

					 		<tr class="listViewEntries" ng-repeat="item in ctrl.searchResults.rows | orderBy:ctrl.sortType:ctrl.sortReverse" >
					 			<td class="listViewEntryValue medium" nowrap>{{item.queueName}}</td>
					 			<td class="listViewEntryValue medium" nowrap>{{item.insertTime | date:'d MMM yyyy HH:mm:ss'}}</td>
					 			<td class="listViewEntryValue medium" nowrap>{{item.idDsr}}</td>
					 			<td class="listViewEntryValue medium" nowrap>{{ctrl.decode.idDsps[item.idDsp]}}</td>
					 			<td class="listViewEntryValue medium" nowrap>{{item.queueType}}</td>
					 			<td class="listViewEntryValue medium" nowrap>{{item.serviceName}}</td>
					 			<td class="listViewEntryValue medium" nowrap>{{ctrl.decode.month[item.month]}}</td>
					 			<td class="listViewEntryValue medium" nowrap>{{item.year}}</td>
					 			<td class="listViewEntryValue medium" nowrap>{{ctrl.decode.countries[item.country]}}</td>
					 			<td class="listViewEntryValue medium" nowrap>{{item.hostname}}</td>
					 			<td nowrap class="medium">
					 				<div class="actions pull-right">
					 				<span class="actionImages">
					 					<a ng-click="showDetails($event,item)"><i title="Dettagli" class="glyphicon glyphicon-list-alt alignMiddle"></i></a>&nbsp;
					 				</span>
					 				</div>
					 			</td>
					 		</tr>

					 		</tbody>
					 		</table>
					 		
						</div>
					</div>
				</div>

				<div class="listViewTopMenuDiv noprint">
					<div class="listViewActionsDiv row-fluid">
					
						<span class="btn-toolbar span3">
						</span>

						<span class="btn-toolbar span4">
						</span>
												 						
						<span class="span5 btn-toolbar">
							<div class="listViewActions pull-right">
							
						 		<div class="pageNumbers alignTop ">
						 			<span ng-show="ctrl.searchResults.hasPrev || ctrl.searchResults.hasNext">
						 				<span class="pageNumbersText" style="padding-right:5px">
						 					Record da <strong>{{ctrl.searchResults.first+1}}</strong> a <strong>{{ctrl.searchResults.last}}</strong>
						 				</span>
					 					<button title="Precedente" class="btn" id="listViewPreviousPageButton" type="button" 
					 							ng-click="navigateToPreviousPage()"
					 							ng-show="ctrl.searchResults.hasPrev">
					 						<span class="glyphicon glyphicon-chevron-left"></span>
					 					</button>
					 					<button title="Successiva" class="btn" id="listViewNextPageButton" type="button"
					 							ng-click="navigateToNextPage()"
					 							ng-show="ctrl.searchResults.hasNext">
					 						<span class="glyphicon glyphicon-chevron-right"></span>
					 					</button>
					 				</span>
						 		</div>
					 			
					 		</div>
					 		<div class="clearfix"></div>
					 	</span>
					</div>
				</div>
				
			</div>
		</div>
	</div>
<!-- 
	<pre>{{ctrl.transazioni}}</pre>
 -->
</div>