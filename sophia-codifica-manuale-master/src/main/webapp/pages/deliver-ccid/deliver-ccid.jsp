<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="../../pages/navbar.jsp" %>

<div class="bodyContents">

    <div class="mainContainer row-fluid">

        <div id="companyLogo" class="navbar commonActionsContainer noprint">
            <div class="actionsContainer row-fluid">
                <div class="span2">
                    <%--<span class="companyLogo"><img src="images/siae_logo_siaetrasparente.jpg" title="SIAE" alt="SIAE">&nbsp;</span>--%>
                    <span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
                </div>
                <span class="btn-toolbar span4">
					<div class="pageNumbers alignTop pull-right" style="text-align: center;">
		 				<span class="pageNumbersText"><strong>Invio CCID</strong></span>
					</div>
				</span>
            </div>
        </div>

        <div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px;">


            <!-- actions and pagination -->
            <!-- qui va il filtro-->
            <filter-dsp-statistics
                    dsps-utilizations-commercial-offers-countries="dspsUtilizationsCommercialOffersCountries"
                    filter-parameters="filterParameters"
                    on-filter-apply="search(parameters)"
                    hide-back-claim-select="false"
                    hide-client-select="true"
                    show-deliver-ccid-statuses="true"
                    show-zip-name="true">
            </filter-dsp-statistics>

            <div class="floatRight" style="margin-top: 20px; margin-right: 10px;">
                <button class="btn addButton" data-ng-click="retrieveConfigShareSiae()">
                    <strong>Scarica su share SIAE</strong>
                </button>
                <button class="btn addButton" data-ng-click="retrieveConfig()">
                    <strong>Invia ai provider</strong>
                </button>
            </div>

            <div class="listViewTopMenuDiv noprint">
                <div class="listViewActionsDiv row-fluid">
						<span class="btn-toolbar span4">
							<span class="btn-group">

 							</span>
						</span>
                    <span class="btn-toolbar span4">
							<span class="customFilterMainSpan btn-group" style="text-align: center;">
								<div class="pageNumbers alignTop ">
					 				<span class="pageNumbersText"><strong></strong></span>
								</div>
						 	</span>
						</span>
                    <span class="span4 btn-toolbar">
                            <div class="listViewActions pull-right">
                            <div class="pageNumbers alignTop ">
                                    <span ng-show="data.hasNext || data.hasPrev"> <span
                                            class="pageNumbersText" style="padding-right: 5px">
                                            Record da <strong>{{filterParameters.first + 1}}</strong> a <strong>{{filterParameters.first + data.rows.length}}</strong> su <strong>{{data.extra.totalResults}}</strong>
                                    </span>
                                        <button title="Precedente" class="btn"
                                                id="listViewPreviousPageButton" type="button"
                                                data-ng-click="navigateToPreviousPage()"
                                                ng-show="paging.currentPage > 1">
                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                        </button>
                                        <button title="Successiva" class="btn"
                                                id="listViewNextPageButton" type="button"
                                                data-ng-click="navigateToNextPage()"
                                                ng-show="data.hasNext">
                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </span>
                </div>
            </div>

            <!-- table -->
            <div class="listViewContentDiv" id="listViewContents">

                <div class="contents-topscroll noprint">
                    <div class="topscroll-div" style="width: 95%">&nbsp;</div>
                </div>

                <div class="listViewEntriesDiv contents-bottomscroll">
                    <div class="bottomscroll-div" style="width: 95%; min-height: 80px">

                        <table class="table table-bordered listViewEntriesTable">
                            <thead>
                            <tr class="listViewHeaders">
                                <th><label><input type="checkbox" data-ng-click="checkAll(selectAllCheck)"
                                                  data-ng-model="selectAllCheck"/></label></th>
                                <th>CCID</th>
                                <th>DSP</th>
                                <th>Periodo</th>
                                <th>Territorio</th>
                                <th>Offerta Commerciale</th>
                                <th>Zip File</th>
                                <th>Scaricato</th>
                                <th>Confermato</th>
                                <th>Consegnato</th>
                                <th>Path</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr style="outline:none;" class="clickable-stat" data-ng-repeat="item in data.rows">
                                <td><label><input type="checkbox" data-ng-model="item.checked"
                                                  data-ng-change="onChangeItemCheck(item)"/></label></td>
                                <td class="listViewEntryValue" data-ng-bind="item.idDsr"></td>
                                <td class="listViewEntryValue" data-ng-bind="item.idDsp"></td>
                                <td class="listViewEntryValue" data-ng-bind="item.period"></td>
                                <td class="listViewEntryValue" data-ng-bind="item.country"></td>
                                <td class="listViewEntryValue" data-ng-bind="item.offer"></td>
                                <td class="listViewEntryValue" data-ng-bind="item.zipfile"></td>
                                <td class="listViewEntryValue"
                                    data-ng-bind="item.scaricato ? (item.data_scarico | date:'dd/MM/yyyy HH:mm') : 'NO'"></td>
                                <td class="listViewEntryValue"
                                    data-ng-bind="item.confermato ? (item.data_conferma | date:'dd/MM/yyyy HH:mm') : 'NO'"></td>
                                <td class="listViewEntryValue"
                                    data-ng-bind="item.consegnato ? (item.data_consegna | date:'dd/MM/yyyy HH:mm') : (item.errore ? item.errore : 'NO')"></td>
                                <td class="listViewEntryValue" data-ng-bind="item.path_consegna"></td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <!-- /table -->
        </div>
    </div>
    <script type="text/ng-template" id="errorDialogId">

        <div id="target">
            <strong>{{ngDialogData}}</strong>
        </div>
    </script>
</div>
<%--<pre>{{properties}}</pre>--%>
