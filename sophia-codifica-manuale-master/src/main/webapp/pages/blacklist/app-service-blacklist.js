codmanApp.service('blacklistService', ['$http', 'configService', '$location', function ($http, configService, $location) {

    var service = this;
    const baseUrl = "api/ms-gestione-blacklist/blacklist";
    const getBlacklist = "getBlackList";
    const importBlacklist = "importBlackList";
    const getSocieta = "societa";
    const getSubEditori = "subeditori";
    const addBlacklist = "addBlacklist";

    service.baseUrlBlackList = `${$location.protocol()}://${$location.host()}:${$location.port()}/${baseUrl}`;
    service.DATE_FORMAT = "DD/MM/YYYY";

    service.getAnagCountry = function () {
        if (service.anagCountry) {
            return new Promise((resolve) => {
                resolve(service.anagCountry);
            });
        } else { //safe check to reload data from be
            return new Promise((resolve, reject) => {
                $http({
                    method: 'GET',
                    url: 'rest/anagCountry'
                })
                    .then(function successCallback({ data }) {
                        service.setAnagCountry(data);
                        resolve(service.anagCountry);
                    }, function errorCallback(response) {
                        reject(response);
                    });
            });
        }

    };

    service.setAnagCountry = function (anagCountryList) {
        service.anagCountry = anagCountryList;
    };

    service.getSocietaTutelaList = function () {
        return new Promise((resolve) => {
            if (service.societaTutela) {
                resolve(service.societaTutela);
            } else {
                $http({
                    method: 'GET',
                    url: 'rest/societa-tutela'
                })
                    .then(({ data }) => {
                        service.setSocietaTutelaList(data);
                        resolve(service.societaTutela);
                    }, (response) => {
                        reject(response);
                    });
            }
        });
    };

    service.setSocietaTutelaList = function (societaTutelaList) {
        service.societaTutela = societaTutelaList;
    };

    service.getSocieta = function () { // called each time to get new values,

        return new Promise((resolve, reject) => {
            /* res = {
                societa: ["Wordtune",
                    "Vinder",
                    "Photofeed",
                    "Photolist",
                    "Kwideo",
                    "Jayo",
                    "Twitterworks",
                    "Rhybox",
                    "Oyoloo",
                    "Skimia",
                    "Digitube",
                    "Tagcat",
                    "Pixoboo",
                    "Jayo",
                    "Layo",
                    "Skinte",
                    "Zooveo",
                    "Eimbee",
                    "Shufflester",
                    "Yambee"]
            };
            service.setSocieta(res);
            resolve(service.societa); */
            if (service.societa) {
                resolve(service.societa);
            } else {
                $http({
                    method: 'GET',
                    url: buildUrl(getSocieta)
                })
                    .then(({ data }) => {
                        service.setSocieta(data.names);
                        resolve(service.societa);
                    }, (response) => {
                        reject(response);
                    });
            }
        });
    };

    service.setSocieta = function (societaList) {
        service.societa = societaList;
    };

    service.getSubEditori = function () { // called each time to get new values

        return new Promise((resolve, reject) => {
            /*     res = {
                    subEditori: ["Maroon",
                        "Aquamarine",
                        "Khaki",
                        "Turquoise",
                        "Fuscia",
                        "Red",
                        "Violet",
                        "Aquamarine",
                        "Green",
                        "Puce"]
                };
                service.setSubEditori(res);
                resolve(service.subEditori); */
            if (service.subEditori) {
                resolve(service.subEditori);
            } else {
                $http({
                    method: 'GET',
                    url: buildUrl(getSubEditori)
                })
                    .then(({ data }) => {
                        service.setSubEditori(data.names);
                        resolve(service.subEditori);
                    }, (response) => {
                        reject(response);
                    });
            }
        });
    }

    service.setSubEditori = function (subEditoriList) {
        service.subEditori = subEditoriList;
    };

    service.getPreloadConfig = function () {
        return new Promise((resolve) => {
            resolve(service.preloadConfig);
        });
    };

    /* 
    * input preloadConfig: {
            societa*: 'societaTutelaCodice',
            territorio*: 'territoryCode',
            dataInizioValidita: 'dd/MM/yyyy',
            nomeFileCsv: 'nome.csv'
        }
    *  */
    service.setPreloadConfig = function (preloadConfig) {
        service.preloadConfig = preloadConfig;
    };

    service.importBlackList = function () {

        return new Promise((resolve, reject) => {

            /*   const recordDettaglio = [{
                  descrizione: "Injury of lumbosacral plexus",
                  tipoRegola: "CI",
                  subEditori: ["Maroon",
                      "Aquamarine",
                      "Khaki",
                      "Turquoise"],
                  societa: ["Oyoloo",
                      "Skimia",
                      "Digitube"],
                  societaEscluse: ["Shufflester", "Yambee"],
                  tipoQuotaDSP: ["CNY", "IDR", "PGK", "CZK", "CNY", "CNY", "EUR", "PHP", "CNY", "EUR"
                  ],
                  dspPeriodoList: ["118-37", "565-46",
                      "341-21", "852-14",
                      "664-89", "173-19",
                      "256-61", "280-83",
                      "153-89", "583-69"
                  ]
              }, {
                  descrizione: "Matador Cavani",
                  tipoRegola: "CI",
                  societa: ["Aquamarine", "Green"],
                  tipoQuotaDSP: ["CNY"],
                  dspPeriodoList: ["7DG-202007"]
              }];
              const response = {
                  societaDiTutela: "SIAE",
                  territorio: "ITA",
                  dataInizioValidita: "26/06/2020",
                  recordDettaglio
              }; */
            if (service.preloadConfig) { // CALL WITH PARAM
                const params = _.omit(service.preloadConfig, 'nomeFileCsv');

                $http({
                    method: 'GET',
                    url: buildUrl(importBlacklist),
                    params
                })
                    .then(({ data }) => {
                        //service.setsubEditori(data);
                        resolve(data);
                    }, (response) => {
                        reject(response);
                    });
            } else {
                resolve({});
            }

        });
    };

    service.getTipoQuotaDsp = function () {
        return new Promise((resolve) => {
            const tipoQuota = ["DRM", "DEM"];

            resolve(tipoQuota);
        });
    };

    service.getTipoRegola = function () {
        return new Promise((resolve) => {
            const tipoRegola = ["CI", "ED"];

            resolve(tipoRegola);
        });
    };

    service.getDspList = function () {
        if (service.dspList) {
            return new Promise((resolve) => {
                resolve(service.dspList);
            });
        }

        return new Promise((resolve, reject) => {
            const [first, last] = [0, 100];
            $http({
                method: 'GET',
                url: 'rest/anagDsp/all',
                params: {
                    first,
                    last
                }
            })
                .then(({ data, first, last }) => {
                    const all = { idDsp: "ALL", code: "ALL", name: "ALL" };
                    service.dspList = [all, ...data.rows];
                    resolve(service.dspList);
                }, (response) => {
                    service.dspList = [];
                    reject(response);
                });
        });
    };

    service.addBlacklist = function (body) {
        const { preloadConfig = {} } = service;
        const { nomeFileCsv = '' } = preloadConfig || {};
        const data = _.omitBy({ ...body, nomeFileCsv }, (e) => _.isNil(e) || e === "")
        return new Promise((resolve, reject) => {
            $http({
                method: 'POST',
                url: buildUrl(addBlacklist),
                data
            })
                .then(data => {
                    console.log(data)
                    resolve(data);
                }, error => {
                    reject(error);
                })
        });
    };

    function buildUrl(url) {
        return `${service.baseUrlBlackList}/${url}`;
    };
}]);