<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../navbar.jsp"%>

<div class="bodyContents">

    <div class="row-fluid">

        <div class="contents-topscroll noprint">
            <div class="topscroll-div" style="width: 95%; margin-bottom: 20px">&nbsp;</div>
        </div>

        <div class="sticky">
            <div id="companyLogo" class="navbar commonActionsContainer noprint">
                <div class="actionsContainer row-fluid">
                    <div class="span2">
                        <span class="companyLogo">
                            <img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;
                        </span>
                    </div>
                    <span class="btn-toolbar span8">
                        <span class="customFilterMainSpan btn-group" style="text-align: center;">
                            <div class="pageNumbers alignTop ">
                                <span class="pageNumbersText">
                                    <strong style="text-transform: uppercase;">Configurazione BlackList</strong>
                                </span>
                            </div>
                        </span>
                    </span>
                </div>
            </div>
        </div>

        <md-card id="blacklistContainer" md-theme="{{ showDarkTheme ? 'dark-grey' : 'default' }}" md-theme-watch>
            <md-content style="overflow: hidden;">
                <div class="alert alert-danger" ng-show="messages.error && (!messages.info)">
                    <button type="button" class="close" ng-click="clearMessages()" aria-hidden="true">x</button>
                    <span><strong>Errore: </strong>{{messages.error}}</span>
                </div>
                <div class="alert alert-success" ng-show="messages.info && (!messages.error)">
                    <button type="button" class="close" ng-click="clearMessages()" aria-hidden="true">x</button>
                    <span>{{messages.info}}</span>
                </div>
                <form name="importForm" novalidate style="flex-direction: row;
                    display: flex; margin: 8px; background-color: #e7e7e7 ; border: 1px solid #ccc">

                    <div id="headerContainer" style="display: flex;
                        place-content: center space-evenly;
                        flex: 1 1 100%;
                        box-sizing: border-box;
                        max-width: 80%;
                        padding-top: 16px">
                        <md-input-container class="md-block"
                            style="flex: 1 1 100%; box-sizing: border-box; max-width: 15%;">
                            <h3>Società tutela</h3>
                            <md-select name="societaTutela" ng-model-options="{ trackBy: '$value.codice' }"
                                ng-model="societaTutelaSelected" required md-no-asterisk>
                                <md-option ng-repeat="societa in societaTutelaList" ng-value="{{societa}}">
                                    {{ societa.nominativo }}
                                </md-option>
                            </md-select>
                            <div ng-show="importForm.societaTutela.$touched && importForm.societaTutela.$invalid"
                                class="error md-input-messages-animation">Campo obbligatorio.</div>
                        </md-input-container>
                        <md-input-container class="md-block"
                            style="flex: 1 1 100%; box-sizing: border-box; max-width: 15%;">
                            <h3>Territorio</h3>
                            <md-select name="territorio" ng-model="territorioSelected"
                                ng-disabled="!societaTutelaSelected" required md-no-asterisk>
                                <md-option
                                    ng-repeat="territorio in territorioList | filter:{code: societaTutelaSelected.homeTerritory.idCountry}"
                                    value="{{territorio.code}}">
                                    {{ territorio.value }}
                                </md-option>
                            </md-select>
                            <div ng-show="importForm.territorio.$touched && importForm.territorio.$invalid"
                                class="error md-input-messages-animation">Campo obbligatorio.</div>
                        </md-input-container>
                        <md-input-container layout="column">
                            <h3 label ng-bind="::dateFields.label"></h3>
                            <md-datepicker ng-model="dateFields.selectedDate" ng-required="dateFields.required"
                                md-date-locale="dateFields.locale" md-mode="month" md-open-on-focus="true">
                            </md-datepicker>
                        </md-input-container>
                        <md-input-container layout="column" ng-show="nomeFileCsv" style="width: 100%;">
                            <h3>Nome del File Csv</h3>
                            <div class="fieldValue medium">
                                <input disabled type="text" class="form-control maxWidth20 " ng-model="nomeFileCsv">
                            </div>
                        </md-input-container>
                    </div>
                </form>
                <div style="flex-direction: column;
                        box-sizing: border-box;
                        display: flex;
                        place-content: flex-end center;
                        align-items: flex-end;
                        margin: 8px;">
                    <div>
                        <button class="btn" type="submit" ng-click="importaBlackList()">
                            <span class="glyphicon glyphicon-import"></span>
                            Importa Blacklist
                        </button>
                        <button class="btn addButton" ng-click="salvaConfigurazione()">Salva</button>
                    </div>
                </div>
                <!-- page Container -->
                <div style=" flex-direction: row;box-sizing: border-box; display: flex;">
                    <!-- left panel -->
                    <div style="flex: 1 1 100%; box-sizing: border-box; max-width: 40%;">
                        <div class="table-responsive text-nowrap horizontal-scroll" style="margin:18px;">
                            <table class="table table-bordered listViewEntriesTable">
                                <thead>
                                    <tr>
                                        <th ng-repeat="head in tableFields.headerTitles">{{head}}</th>
                                        <!-- empty column for download button -->
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr mdbTableCol
                                        ng-repeat="configurazione in configurazione.recordDettaglio track by $index"
                                        ng-click="openDetails(configurazione)">
                                        <td>{{configurazione.descrizione}}</td>
                                        <td>{{configurazione.tipoRegola}}</td>
                                        <td style="display: flex;
                                            flex-direction: row-reverse">
                                            <div class="addButton" ng-click="rimuoviRecord($index)">
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div ng-show="!configurazione.recordDettaglio.length" style="margin-top: 16px;">
                                <div style="text-align: center;">Al momento non ci sono record.</div>
                            </div>
                        </div>
                    </div>
                    <!-- right panel -->
                    <md-card style="width: 100%;">
                        <!-- <md-card-title style="flex: 0 1 auto">
                            <md-card-title-text style="background-color: #575e78;">
                                <h3 style="text-align: center; color: white;">Record di Dettaglio</h3>
                            </md-card-title-text>
                        </md-card-title> -->
                        <div>
                            <h3 style="text-align: center;height: 35px;margin: 9px 0px 0 0;background-color: #f4f4f5;font-size: 16px;padding-top: 1p;"
                                class="table table-bordered">Record di Dettaglio</h3>
                        </div>
                        <md-content style="overflow: hidden;">
                            <form name="recordForm" novalidate class="flexColumnStartCenter" style="margin-top: 16px;"
                                ng-submit="aggiungiRecord()">
                                <!-- first row (descrizione, tipo e bottone) -->
                                <div id="rightPanelContainer" style="display: flex;
                                    place-content: start;
                                    flex: 1 1 100%;
                                    box-sizing: border-box;
                                    width: 100%;">
                                    <md-input-container class="md-block"
                                        style="flex: 1 1 100%; box-sizing: border-box; max-width: 40%;">
                                        <h3>Descrizione</h3>
                                        <div class="fieldValue medium">
                                            <input name="descrizioneController" type="text"
                                                class="form-control maxWidth20 "
                                                ng-model="configurationBlackList.descrizione" required md-no-asterisk>
                                        </div>
                                        <div ng-show="recordForm.descrizioneController.$touched && recordForm.descrizioneController.$invalid"
                                            class="error md-input-message-animation">Campo obbligatorio.</div>
                                    </md-input-container>
                                    <md-input-container class="md-block"
                                        style="flex: 1 1 100%; box-sizing: border-box; max-width: 15%;">
                                        <h3>Tipo Regola</h3>
                                        <md-select style="margin-top:5px" name="tipoRegolaController"
                                            ng-model="tipoRegolaSelected" required md-no-asterisk>
                                            <md-option ng-repeat="tipoRegola in tipoRegolaList" value="{{tipoRegola}}">
                                                {{ tipoRegola }}
                                            </md-option>
                                        </md-select>
                                        <div ng-show="recordForm.tipoRegolaController.$touched && recordForm.tipoRegolaController.$invalid"
                                            class="error md-input-message-animation">Campo obbligatorio.</div>
                                    </md-input-container>
                                    <div style="display: flex; align-items: start; flex-direction: column;">
                                        <button class="btn addButton" ng-click="nuovoRecord()"
                                            style="margin: 26px 0 0 26px">
                                            <span class="glyphicon glyphicon-erase"></span>
                                        </button>
                                    </div>
                                </div>
                                <!-- SUB EDITORI -->
                                <div id="subEditoriContainer">
                                    <md-content class="md-padding" class="flexColumnCenterStart">
                                        <h3 id="favoriteStateLabel">Sub Editori</h3>
                                        <div class="headerContainer">
                                            <md-chips style="flex: 1 1 100%;
                                                box-sizing: border-box;
                                                display: flex;
                                                place-content: center start;
                                                flex: 1 1 100%;
                                                box-sizing: border-box; border: 1px solid #e0e0e0;"
                                                ng-model="subEditoreChipSelected"
                                                md-transform-chip="transformSubEditoreChip($chip)"
                                                md-require-match="autocompleteRequireMatch"
                                                input-aria-label="Sub Editori">
                                                <md-autocomplete md-no-cache="true"
                                                    md-selected-item="subEditoreSelected"
                                                    md-search-text="subEditoreValue"
                                                    md-selected-item-change="subEditoriSelectedEvent(item)"
                                                    md-items="item in subEditoriQuerySearch(subEditoreValue)"
                                                    md-item-text="item" md-min-length="0" md-escape-options="clear"
                                                    placeholder="Ex. Siae" input-aria-labelledby="favoriteStateLabel">
                                                    <md-item-template>
                                                        <span md-highlight-text="subEditoreValue"
                                                            md-highlight-flags="^i">{{item}}</span>
                                                    </md-item-template>
                                                    <md-not-found>
                                                        <a ng-click="newSubEditore(subEditoreValue)">Creane uno
                                                            nuovo!</a>
                                                    </md-not-found>
                                                </md-autocomplete>
                                                <md-chip-template>
                                                    <span>
                                                        <strong>{{$chip}}</strong>
                                                    </span>
                                                </md-chip-template>
                                            </md-chips>
                                        </div>
                                    </md-content>
                                </div>
                                <!-- SOCIETA -->
                                <div id="societaContainer" style="display: flex;
                                    place-content: center start;
                                    flex: 1 1 100%;
                                    box-sizing: border-box;">
                                    <md-content class="md-padding" style="display: flex;
                                    flex-direction: column;
                                    place-content: center start;
                                    flex: 1 1 100%;
                                    box-sizing: border-box;">
                                        <h3 id="SocietaLabel">Società</h3>
                                        <div class="flexRowCenterStart headerContainer">
                                            <div style="display: flex;
                                            flex-direction: column;
                                            place-content: center start;
                                            flex: 1 1 100%;
                                            box-sizing: border-box;">
                                                <label>Lista Inclusioni</label>
                                                <md-chips style=" flex: 1 1 100%;
                                                box-sizing: border-box;
                                                display: flex;
                                                place-content: center start;
                                                flex: 1 1 100%;
                                                box-sizing: border-box; border: 1px solid #e0e0e0;"
                                                    ng-model="societaInclusionList"
                                                    md-transform-chip="transformSocietaChip($chip)"
                                                    md-require-match="autocompleteRequireMatch"
                                                    input-aria-label="Società">
                                                    <md-autocomplete md-no-cache="true"
                                                        md-selected-item="societaSelected" md-search-text="societaValue"
                                                        md-selected-item-change="societaSelectedEvent(item)"
                                                        md-items="item in societaQuerySearch(societaValue, societaExclusionList)"
                                                        md-item-text="item" md-min-length="0" md-escape-options="clear"
                                                        placeholder="Ex. Società" input-aria-labelledby="societaLabel">
                                                        <md-item-template>
                                                            <span md-highlight-text="societaValue"
                                                                md-highlight-flags="^i">{{item}}</span>
                                                        </md-item-template>
                                                        <md-not-found>
                                                            <a ng-click="newSocieta(societaValue)">Creane uno nuovo!</a>
                                                        </md-not-found>
                                                    </md-autocomplete>
                                                    <md-chip-template>
                                                        <span>
                                                            <strong>{{$chip}}</strong>
                                                        </span>
                                                    </md-chip-template>
                                                </md-chips>
                                            </div>
                                            <div style="display: flex;
                                            flex-direction: column;
                                            place-content: center start;
                                            flex: 1 1 100%;
                                            box-sizing: border-box;">
                                                <label>Lista Esclusioni</label>
                                                <md-chips style=" flex: 1 1 100%;
                                                                    box-sizing: border-box;
                                                                    display: flex;
                                                                    place-content: center start;
                                                                    flex: 1 1 100%;
                                                                    box-sizing: border-box; border: 1px solid #e0e0e0;"
                                                    ng-model="societaExclusionList"
                                                    md-transform-chip="transformSocietaChip($chip)"
                                                    md-require-match="autocompleteRequireMatch"
                                                    input-aria-label="Società">
                                                    <md-autocomplete md-no-cache="true"
                                                        md-selected-item="societaSelected"
                                                        md-search-text="societaEsclusioneValue"
                                                        md-selected-item-change="societaSelectedEvent(item)"
                                                        md-items="item in societaQuerySearch(societaEsclusioneValue, societaInclusionList)"
                                                        md-item-text="item" md-min-length="0" md-escape-options="clear"
                                                        placeholder="Ex. Società" input-aria-labelledby="societaLabel">
                                                        <md-item-template>
                                                            <span md-highlight-text="societaEsclusioneValue"
                                                                md-highlight-flags="^i">{{item}}</span>
                                                        </md-item-template>
                                                        <md-not-found>
                                                            <a ng-click="newSocietaEsclusioni(societaEsclusioneValue)">Creane
                                                                uno nuovo!</a>
                                                        </md-not-found>
                                                    </md-autocomplete>
                                                    <md-chip-template>
                                                        <span>
                                                            <strong>{{$chip}}</strong>
                                                        </span>
                                                    </md-chip-template>
                                                </md-chips>
                                            </div>
                                        </div>
                                    </md-content>
                                </div>
                                <!-- TIPO QUOTA -->
                                <div id="tipoQuotaContainer">
                                    <md-content class="md-padding" class="flexColumnCenterStart">
                                        <h3>Tipo quota DSP</h3>
                                        <div class="headerContainer">
                                            <md-chips style="flex: 1 1 100%;
                                            box-sizing: border-box;
                                            display: flex;
                                            place-content: center start;
                                            flex: 1 1 100%;
                                            box-sizing: border-box; border: 1px solid #e0e0e0;
                                            max-width: 20%;" ng-model="tipoQuotaChipSelected"
                                                md-require-match="autocompleteRequireMatch"
                                                input-aria-label="tipo Quota">
                                                <md-autocomplete md-no-cache="true" md-selected-item="tipoQuotaSelected"
                                                    md-search-text="tipoQuotaValue"
                                                    md-selected-item-change="tipoQuotaSelectedEvent(item)"
                                                    md-items="item in tipoQuotaQuerySearch(tipoQuotaValue)"
                                                    md-item-text="item" md-min-length="0" md-escape-options="clear"
                                                    placeholder="Ex. DEM">
                                                    <md-item-template>
                                                        <span md-highlight-text="tipoQuotaValue"
                                                            md-highlight-flags="^i">{{item}}</span>
                                                    </md-item-template>
                                                    <md-not-found>
                                                        Nessun Sub Editore trovato.
                                                    </md-not-found>
                                                </md-autocomplete>
                                                <md-chip-template>
                                                    <span>
                                                        <strong>{{$chip}}</strong>
                                                    </span>
                                                </md-chip-template>
                                            </md-chips>
                                        </div>
                                    </md-content>
                                </div>
                                <!-- DSP / PERIODO -->
                                <div class="flexRowCenterStart" style="width: 100%;">
                                    <md-content class="flexColumnCenterStart headerContainer" style="max-width: 20%;">
                                        <md-input-container class="md-block"
                                            style="flex: 1 1 100%; box-sizing: border-box;">
                                            <h3>DSP</h3>
                                            <md-select name="dsp" ng-model="dspSelected">
                                                <md-option ng-repeat="dsp in dspList" ng-value="{{dsp}}">
                                                    {{ dsp.name }}
                                                </md-option>
                                            </md-select>
                                        </md-input-container>
                                    </md-content>
                                    <md-content class="flexColumnCenterStart headerContainer" style="max-width: 35%;">
                                        <h3>{{dspDateFields.label}}</h3>
                                        <md-input-container layout="column">
                                            <md-datepicker ng-model="dspDateFields.selectedDate"
                                                ng-disabled="disableDspDate()" ng-required="dspDateFields.required"
                                                md-date-locale="dspDateFields.locale" md-mode="month"
                                                md-open-on-focus="true" md-min-date="dataFields.today">
                                            </md-datepicker>
                                        </md-input-container>
                                    </md-content>
                                    <md-content style="flex: 1 1 100%;
                                        box-sizing: border-box;
                                        flex-direction: column;
                                        place-content: center;
                                        margin-top:25px">
                                        <button class="btn addButton"
                                            ng-disbled="$scope.dspSelected !== 'ALL' && $scope.dspSelected && $scope.dspDateFields.selectedDate"
                                            ng-click="aggiungiDspPeriodo()">
                                            <span class="glyphicon glyphicon-plus"></span>
                                        </button>
                                    </md-content>
                                </div>
                                <!-- CHIPS CONTAINER -->
                                <div id="tipoQuotaContainer" ng-show="dspPeriodoChipList.length">
                                    <md-content class="md-padding" class="flexColumnCenterStart">
                                        <div class="headerContainer">
                                            <md-chips style="flex: 1 1 100%;
                                            box-sizing: border-box;
                                            display: flex;
                                            place-content: center start;
                                            flex: 1 1 100%;
                                            box-sizing: border-box; border: 1px solid #e0e0e0;"
                                                ng-model="dspPeriodoChipList" readonly="true" md-removable="true"
                                                input-aria-label="Dsp Periodo">
                                                <md-chip-template>
                                                    <span>
                                                        <strong>{{$chip}}</strong>
                                                    </span>
                                                </md-chip-template>
                                            </md-chips>
                                        </div>
                                    </md-content>
                                </div>
                                <!-- Applica bottone -->
                                <md-content class="headerContainer" style="flex-direction: row;
                                    box-sizing: border-box;
                                    display: flex;
                                    place-content: center flex-end;
                                    align-items: center;
                                    width: 100%;
                                    height: 100%;
                                    margin: 8px 0px;">
                                    <button class="btn addButton"
                                        ng-disbled="$scope.dspSelected && $scope.dspDateFields.selectedDate"
                                        type="submit">
                                        <span class="glyphicon "></span>
                                        Applica
                                    </button>
                                </md-content>
                            </form>
                        </md-content>
                    </md-card>
                </div>
            </md-content>
            <!-- <md-card-actions layout="row" layout-align="end center">
                <md-button class="btn addButton" ng-click="salvaConfigurazione()">Salva</md-button>
            </md-card-actions> -->
        </md-card>
    </div>
</div>