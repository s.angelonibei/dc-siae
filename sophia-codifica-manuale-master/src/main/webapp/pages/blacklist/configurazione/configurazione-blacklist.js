codmanApp.controller('ConfigurazioneBlacklistCtrl', ['$scope', 'blacklistService', '$location',
    function ($scope, blacklistService, $location) {

        const Y_S_HEADER = ["Rule Name", "Offerta Commerciale", "SIAE Pro Rata %", "Minimo Abbonamento", "Minimum Currency"];
        $scope.messages = {
            info: "",
            error: ""
        };

        $scope.dataInizioValidita = {
            type: 'date',
            required: true,
            today: new Date(),
            startView: 'month',
            label: 'Data inizio validità',
            locale: buildLocaleProvider("DD-MM-YYYY"),
        };
        $scope.dataFineValidita = {
            type: 'date',
            required: false,
            today: new Date(),
            startView: 'month', // day
            label: 'Data fine validità',
            locale: buildLocaleProvider("YYYYMM"),
        };
        $scope.subEditoreChipSelected = [];
        $scope.tipoQuotaChipSelected = [];
        $scope.dspPeriodoChipList = [];
        $scope.subEditoreValue = null;
        $scope.societaInclusionList = [];
        $scope.societaExclusionList = [];
        $scope.configurazioneList = [];
        $scope.dspSelectedList = [];
        $scope.autocompleteRequireMatch = true;
        /* 
            {
                societàDiTutela: "SIAE",
                territorio: "ITA",
                dataInizioValidita: "26/06/2020",
                recordDettaglio: [recordDettagio]
            } 
        */
        $scope.configurationBlackList = {
            territorio: $scope.territorioSelected
        };

        $scope.infoMessage = function (message) {

            setTimeout(() => {
                $scope.messages.info = message;
                $scope.$apply()
            }, 100);
        };

        $scope.errorMessage = function (message) {
            setTimeout(() => {
                $scope.messages.error = message;
                $scope.$apply()
            }, 100);
        };

        $scope.clearMessages = function () {
            $scope.messages = {
                info: "",
                error: ""
            };
        }

        $scope.onInit = function () {

            $scope.dateFormat = blacklistService.DATE_FORMAT;

            blacklistService.getSocietaTutelaList()
                .then(data => {
                    $scope.societaTutelaList = data;
                }, _error => {
                    $scope.errorMessage("Qualcosa è andato storto.");
                });

            blacklistService.getAnagCountry()
                .then(data => {
                    $scope.territorioList = Object.entries(data)
                        .map(([k, v]) => ({ code: k, value: v }));
                }, _error => {
                    $scope.errorMessage("Qualcosa è andato storto.");
                });

            blacklistService.getSocieta()
                .then(data => {
                    $scope.societa = data;
                }, _error => {
                    $scope.errorMessage("Qualcosa è andato storto.");
                });

            blacklistService.getSubEditori()
                .then(data => {
                    $scope.subEditori = data;
                }, _error => {
                    $scope.errorMessage("Qualcosa è andato storto.");
                });


            blacklistService.getTipoRegola()
                .then(data => {
                    $scope.tipoRegolaList = data;
                });

            initBlackListData();

            blacklistService.getDspList()
                .then((data) => {
                    $scope.dspList = data;
                }, function (error) {
                    console.log('ERRORE', error);
                    $scope.errorMessage("Qualcosa è andato storto.");
                });

            blacklistService.getPreloadConfig()
                .then((data) => {
                    $scope.dateFields.selectedDate = data && new Date(moment(data.dataInizioValidita, 'DD/MM/YYYY')) || new Date();
                });

        };

        /* Returns array of header depending on Tipo Utilizzo */
        $scope.getTableHeader = function () {
            // $scope.tipoUtilizzoSelected
            return Y_S_HEADER;
        };

        $scope.tableFields = {
            headerTitles: ["Descrizione", "Tipo Regola"]
        };

        $scope.transformToDate = function (dataInizioValidita, fromFormat) {

            return moment(dataInizioValidita, fromFormat)
        };

        $scope.openDetails = function (rowSelected) {
            console.log(rowSelected);
            const { descrizione, subEditori = [], societa, societaEscluse, tipoQuotaDSP, dspPeriodoList, tipoRegola } = rowSelected;
            $scope.configurationBlackList.descrizione = descrizione;
            $scope.tipoRegolaSelected = tipoRegola;
            $scope.subEditoreChipSelected = _.uniq(subEditori);
            $scope.societaInclusionList = _.uniq(societa);
            $scope.societaExclusionList = _.uniq(societaEscluse);
            $scope.tipoQuotaChipSelected = _.uniq(tipoQuotaDSP);
            $scope.dspPeriodoChipList = _.uniq(dspPeriodoList);
        };

        $scope.setHeaderValues = function (societaTutela, territorio, dataInizioValidita = null, nomeFileCsv) {
            $scope.nomeFileCsv = nomeFileCsv;
            $scope.societaTutelaSelected = $scope.societaTutelaList.find(({ codice }) => codice === societaTutela);
            $scope.territorioSelected = territorio;
            if (dataInizioValidita) {
                $scope.dateFields.selectedDate = new Date(dataInizioValidita);
            }
        };

        $scope.importaBlackList = function () {
            $scope.markFormAsTouched($scope.importForm);
            if ($scope.importForm.$valid) {
                const { societaTutelaSelected: societa, territorioSelected: territorio } = $scope;
                if (societa && territorio) {
                    const loadDataFrom = {
                        societa: societa.codice,
                        territorio
                    };
                    blacklistService.setPreloadConfig(loadDataFrom);
                    initBlackListData();
                }
            }
        };

        $scope.markFormAsTouched = function (formController) {
            formController.$$controls.forEach(child => {
                child && child.$setTouched();
            });
        };

        $scope.markFormAsUntouched = function (formController) {
            formController.$$controls.forEach(child => {
                child && child.$setUntouched();
            });
        };

        $scope.aggiungiRecord = function () {
            console.log("aggiungi");
            $scope.recordForm.descrizioneController.$setTouched();
            $scope.recordForm.tipoRegolaController.$setTouched();
            if (!$scope.configurazione.recordDettaglio || !$scope.configurazione.recordDettaglio.length) {
                $scope.configurazione.recordDettaglio = [];
            }

            if ($scope.recordForm.$valid) {
                const newRecord = $scope.buildNewRecord();
                const origin = $scope.configurazione.recordDettaglio
                    .map((record) => ({ ...record, descrizione: record.descrizione.toLowerCase(), tipoRegola: record.tipoRegola.toLowerCase() }));
                const findRecord = _.find(origin, { descrizione: newRecord.descrizione.toLowerCase(), tipoRegola: newRecord.tipoRegola.toLowerCase() });
                if (!findRecord) {
                    $scope.configurazione.recordDettaglio.push(newRecord);
                    $scope.infoMessage("Configurazione inserita correttamente");
                } else { // update Record
                    const index = _.findIndex(origin, { descrizione: newRecord.descrizione.toLowerCase(), tipoRegola: newRecord.tipoRegola.toLowerCase() });
                    $scope.configurazione.recordDettaglio.splice(index, 1, newRecord);
                    $scope.infoMessage("Configurazione aggiornata correttamente");
                }
            }

        };

        $scope.nuovoRecord = function () {
            const nuovo = { descrizione: null, subEditori: [], societa: [], societaEscluse: [], tipoQuotaDSP: [], dspPeriodoList: [], tipoRegola: null };
            $scope.markFormAsUntouched($scope.importForm);
            $scope.openDetails(nuovo);
        };

        $scope.rimuoviRecord = function (index) {
            console.log("posizione da rimuovere", index);
            if ($scope.configurazione && $scope.configurazione.recordDettaglio.length) {
                _.pullAt($scope.configurazione.recordDettaglio, index);
            }
        };

        // return obj of new record configuration. descrizione, tipo regola must be mandatory
        $scope.buildNewRecord = function () {
            let newRecord = {
                descrizione: $scope.configurationBlackList.descrizione,
                tipoRegola: $scope.tipoRegolaSelected,
            };

            let optionalValues = {
                subEditori: $scope.subEditoreChipSelected,
                societa: $scope.societaInclusionList,
                societaEscluse: $scope.societaExclusionList,
                tipoQuotaDSP: $scope.tipoQuotaChipSelected,
                dspPeriodoList: $scope.dspPeriodoChipList,
            };
            // cleanup object from empty values( null undefined and empty string)
            const cleanValues = _.omitBy(optionalValues, (e) => _.isNil(e) || e === "");

            return {
                ...newRecord,
                ...cleanValues
            };
        };
        $scope.newSubEditore = function (value) {
            console.log("new sub editore");
            $scope.subEditoreChipSelected.push(value);
        };

        $scope.newSocieta = function (value) {
            console.log("new sub editore");
            $scope.societaInclusionList.push(value);
        };

        $scope.newSocietaEsclusioni = function (value) {
            console.log("new sub editore");
            $scope.societaExclusionList.push(value);
        };

        $scope.searchTextChange = function (valoreTesto) {
            console.log("text changed", valoreTesto);
        };

        $scope.subEditoriSelectedEvent = function (valoreTesto) {
            console.log("text changed", valoreTesto);
        };

        $scope.societaSelectedEvent = function (valoreTesto) {
            console.log("text changed", valoreTesto);
        };

        $scope.tipoQuotaSelectedEvent = function (valoreTesto) {
            console.log("text changed", valoreTesto);
        };

        $scope.subEditoriQuerySearch = function (inputValue) {
            const valoreTesto = inputValue;
            console.log("subEditoriQuerySearch changed", valoreTesto);
            return new Promise((resolve) => {
                blacklistService.getSubEditori()
                    .then((subEditori) => {
                        const results = valoreTesto ?
                            subEditori.filter(createFilterFor(valoreTesto)) :
                            subEditori;
                        resolve(results);
                    });
            });
        };

        $scope.societaQuerySearch = function (inputValue, listToExclude) {
            const valoreTesto = inputValue;
            console.log("societaQuerySearch changed", valoreTesto);
            return new Promise((resolve) => {
                blacklistService.getSocieta()
                    .then((societa) => {
                        const societaToUse = _.without(societa, ...listToExclude);
                        const results = valoreTesto ?
                            societaToUse.filter(createFilterFor(valoreTesto)) :
                            societaToUse;
                        resolve(results);
                    });
            });
        };

        $scope.tipoQuotaQuerySearch = function (inputValue) {
            const valoreTesto = inputValue;
            console.log("societaQuerySearch changed", valoreTesto);
            return new Promise((resolve) => {
                blacklistService.getTipoQuotaDsp()
                    .then((tipoQuota) => {
                        const results = valoreTesto ?
                            tipoQuota.filter(createFilterFor(valoreTesto)) :
                            tipoQuota;
                        resolve(results);
                    });
            });
        };

        /**
            * Return the proper object when the append is called.
         */
        $scope.transformSubEditoreChip = function (chip) {
            // If it is an object, it's already a known chip
            if (angular.isObject(chip)) {
                return chip;
            }

            // Otherwise, create a new one
            return chip;
        };

        $scope.transformSocietaChip = function (chip) {
            // If it is an object, it's already a known chip
            if (angular.isObject(chip)) {
                return chip;
            }

            // Otherwise, create a new one
            return chip;
        };

        $scope.aggiungiDspPeriodo = function () {
            const dspCode = $scope.dspSelected.code;
            let chipValue = `${dspCode}`;
            if ($scope.dspDateFields.selectedDate) {
                const date = moment($scope.dspDateFields.selectedDate)
                    .format("YYYYMM");
                chipValue = `${dspCode}-${date}`;
            }
            if (!$scope.dspPeriodoChipList.includes(chipValue)) {
                $scope.dspPeriodoChipList.push(chipValue);
            }
        };

        $scope.salvaConfigurazione = function () {
            if ($scope.importForm.$valid && $scope.configurazione.recordDettaglio && $scope.configurazione.recordDettaglio.length) {
                const body = buildAddBodyRequest();
                blacklistService.addBlacklist(body)
                    .then(() => {
                        $scope.infoMessage("Configurazione salvata con successo.");
                        setTimeout(function () {
                            $location.path("/blacklist");
                            $scope.$apply();
                        }, 700);
                    }, () => {
                        $scope.errorMessage("Qualcosa è andato storto.");
                        console.log("error");
                    })
            } else {
                // error message
                $scope.errorMessage("Form invalido. Inserire tutti i campi obbligatori.");
            }
        };

        $scope.disableDspDate = function () {
            return $scope.dspSelected && $scope.dspSelected.code === "ALL";
        };


        $scope.onInit();

        function initBlackListData() {
            blacklistService.importBlackList()
                .then(data => {
                    $scope.configurazione = data;
                    if (!_.isEmpty(data)) {

                        blacklistService.getPreloadConfig()
                            .then((newConfiguration = {}) => {
                                setTimeout(function () {
                                    const { nomeFileCsv = '' } = newConfiguration;
                                    $scope.setHeaderValues(data.societaDiTutela, data.territorio, null, nomeFileCsv);
                                    if (data.recordDettaglio && data.recordDettaglio.length) {
                                        $scope.openDetails(data.recordDettaglio[0]);
                                    }
                                    $scope.$apply(); //this triggers a $digest
                                });
                            }, 500);
                    }
                }, _error => {
                    $scope.errorMessage("Qualcosa è andato storto.");
                });
        }

        function buildLocaleProvider(formatString) {
            return {
                formatDate: function (date) {
                    return date ?
                        moment(date).format(formatString) :
                        null;
                },
                parseDate: function (dateString) {
                    if (dateString) {
                        return moment(dateString).isValid() ? moment(dateString).toDate() : new Date(NaN);
                    }
                    return null;
                }
            };
        };

        function buildAddBodyRequest() {
            const recordDettaglio = buildRecordDettaglio();
            const { codice: societaDiTutela } = $scope.societaTutelaSelected;
            const territorio = $scope.territorioSelected;
            const dataInizioValidita = $scope.dateFields.selectedDate;

            return {
                societaDiTutela,
                territorio,
                dataInizioValidita: moment(dataInizioValidita).format($scope.dateFormat),
                recordDettaglio
            }
        };

        function buildRecordDettaglio() {
            const dettagli = $scope.configurazione.recordDettaglio;
            /* return [{
                descrizione: "desc",
                tipoRegola: "regola",
                subEditori: ["nomeSubEditore", ..],
                societa: ["nomeSocieta", ..],
                societaEscluse: ["nomeSocieta"],
                tipoQuotaDSP: ["string1",..],
                dspPeriodoList: ["string1",..]
            }]; */
            return dettagli;
        }

        function createFilterFor(query) {
            var lowercaseQuery = query.toLowerCase();

            return function filterFn(state) {
                return state.toLowerCase()
                    .startsWith(lowercaseQuery);
            };

        };

    }
]);