<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%@
	include file="../navbar.jsp" %>

<div class="bodyContents" >

	<div class="mainContainer row-fluid" >
	
		<div id="companyLogo" class="navbar commonActionsContainer noprint">
			<div class="actionsContainer row-fluid">
				<div class="span2">
<!--  -- >			<span class="companyLogo"><img src="images/siae_logo_siaetrasparente.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
<!--  -->			<span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
				</div>
				<span class="btn-toolbar span4">
					<div class="pageNumbers alignTop pull-right" style="text-align: center;">
		 				<span class="pageNumbersText"><strong>Blacklist</strong></span>
					</div>
				</span>				
			</div>
		</div>
	
		<div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 180px;"> 
			<div class="listViewPageDiv">

				<div class="listViewTopMenuDiv noprint">
					<div class="listViewActionsDiv row-fluid">
					
						<span class="btn-toolbar span4">
							<span class="btn-group">
								<button class="btn addButton" ng-click="showUpload()">
									<span class="glyphicon glyphicon-cloud-upload"></span>&nbsp;&nbsp;<strong>Upload</strong>
								</button>
							 </span>
							 <span class="btn-group">
								<button class="btn addButton" ng-click="goToConfiguration()">
									<span class="glyphicon"></span>&nbsp;&nbsp;<strong>Configurazione Blacklist</strong>
								</button>
 							</span>
						</span>

						<!-- <span class="btn-toolbar span4">
						</span> -->
												 						
						<span class="span8 btn-toolbar">
							<div class="listViewActions pull-right">
							
						 		<div class="pageNumbers alignTop ">
						 			<span ng-show="ctrl.searchResults.hasPrev || ctrl.searchResults.hasNext">
						 				<span class="pageNumbersText" style="padding-right:5px">
						 					Record da <strong>{{ctrl.searchResults.first+1}}</strong> a <strong>{{ctrl.searchResults.last}}</strong>
						 				</span>
					 					<button title="Precedente" class="btn" id="listViewPreviousPageButton" type="button" 
					 							ng-click="navigateToPreviousPage()"
					 							ng-show="ctrl.searchResults.hasPrev">
					 						<span class="glyphicon glyphicon-chevron-left"></span>
					 					</button>
					 					<button title="Successiva" class="btn" id="listViewNextPageButton" type="button"
					 							ng-click="navigateToNextPage()"
					 							ng-show="ctrl.searchResults.hasNext">
					 						<span class="glyphicon glyphicon-chevron-right"></span>
					 					</button>
					 				</span>
						 		</div>
					 			
					 		</div>
					 		<div class="clearfix"></div>
					 	</span>
					</div>
				</div>
					
				<!-- table -->
				<div class="listViewContentDiv" id="listViewContents" >
<!--  -- >
					 <div class="contents-topscroll noprint">
					 	<div class="topscroll-div" style="width: 95%;">&nbsp;</div>
					 </div>
<!--  -->
					 <div class="listViewEntriesDiv contents-bottomscroll">
					 	<div class="bottomscroll-div" style="width: 95%; min-height: 400px">
					 		
					 		<table class="table table-bordered listViewEntriesTable">

					 		<thead>
					 		<tr class="listViewHeaders">
								<th nowrap><a ng-click="ctrl.sortType='societaTutela'; ctrl.sortReverse=!ctrl.sortReverse" class="listViewHeaderValues">Societ&agrave; tutela
									<span ng-show="ctrl.sortType=='societaTutela' && !ctrl.sortReverse" class="glyphicon glyphicon-sort-by-alphabet"></span>
									<span ng-show="ctrl.sortType=='societaTutela' && ctrl.sortReverse" class="glyphicon glyphicon-sort-by-alphabet-alt"></span>
								</a></th>
						 		<th nowrap><a ng-click="ctrl.sortType='country'; ctrl.sortReverse=!ctrl.sortReverse" class="listViewHeaderValues" >Territorio
						 			<span ng-show="ctrl.sortType=='country' && !ctrl.sortReverse" class="glyphicon glyphicon-sort-by-alphabet"></span>
	            					<span ng-show="ctrl.sortType=='country' && ctrl.sortReverse" class="glyphicon glyphicon-sort-by-alphabet-alt"></span>
            					</a></th>
						 		<th nowrap><a ng-click="ctrl.sortType='validFrom'; ctrl.sortReverse=!ctrl.sortReverse" class="listViewHeaderValues" >Data Inizio Validit&agrave;
						 			<span ng-show="ctrl.sortType=='validFrom' && !ctrl.sortReverse" class="glyphicon glyphicon-sort-by-attributes"></span>
	            					<span ng-show="ctrl.sortType=='validFrom' && ctrl.sortReverse" class="glyphicon glyphicon-sort-by-attributes-alt"></span>
            					</a></th>
								 <th nowrap><span class="listViewHeaderValues" >File CSV S3</span></th>
					 			<th nowrap></th>
					 		</tr>
					 		</thead>
					 		<tbody>
		
							 <tr style="background-color: #e7e7e7 !important;">
								<td nowrap><div class="row-fluid">
										<angucomplete-alt id="societaTutela"
										   placeholder="Societ&agrave; Tutela"
										   pause="200"
										   selected-object="selectetSocietaTutela"
										   remote-url="rest/societa-tutela/search?field=nominativo&value="
										   search-fields="nominativo"
										   title-field="nominativo"
										   minlength="1"
										   initial-value="ctrl.codiceSocietaTutela"
										   input-class="input-small listSearchContributor"/>
										</div>
								</td>								
					 			<td nowrap><div class="row-fluid">
					 				<angucomplete-alt id="country"
										placeholder="Territorio"
										pause="200"
										selected-object="selectCountry"
										remote-url="rest/anagCountry/search?name="
										search-fields="name"
  										title-field="name"
										minlength="1"
										input-class="input-small listSearchContributor"/>
					 				</div>
					 			</td>
					 			<td nowrap>
									<input type="text" class="span9 input-small ctrl_validFrom"
										style="width: 80px"
										ng-model="ctrl.validFrom" ng-change="onRicerca()"> 
									<script type="text/javascript">
									    $(".ctrl_validFrom").datepicker({
									    	format: 'yyyy-mm-dd', 
									    	language: 'it',
									    	autoclose: true,
									    	todayHighlight: true
									    });
									</script>
									<span class="glyphicon glyphicon-erase" style="margin-top: 6px;"
										ng-click="ctrl.validFrom=null; onRicerca()"></span> 
					 			</td>
					 			<td nowrap>
						 			<input type="text" class="listSearchContributor input-medium "
											ng-model="ctrl.xlsS3Url"> 
					 			</td>
					 			<td nowrap>
						 			<div class="pull-right">
						 				<button class="btn" ng-click="onAzzera(); onRicerca()"><span class="glyphicon glyphicon-erase"></span></button>
						 				&nbsp;
						 				<button class="btn " ng-click="onRicerca()"><span class="glyphicon glyphicon-search"></span></button>
						 			</div>
					 			</td>
					 		</tr>
					 		
					 		<tr class="listViewEntries" ng-repeat="item in ctrl.searchResults.rows | orderBy:ctrl.sortType:ctrl.sortReverse" >
								<td class="listViewEntryValue medium" nowrap>{{item.societaTutela.nominativo}}</td>
					 			<td class="listViewEntryValue medium" nowrap>{{ctrl.decode.countries[item.country]}}</td>
					 			<td class="listViewEntryValue medium" nowrap>{{item.validFrom | date:'d MMMM yyyy'}}</td>
					 			<td class="listViewEntryValue medium" nowrap>{{item.xlsS3Url.substr(1 + item.xlsS3Url.lastIndexOf('/'))}}</td>
					 			<td nowrap class="medium">
					 				<div class="actions pull-right">
					 				<span class="actionImages">
					 					<a download="{{item.xlsS3Url.substr(1 + item.xlsS3Url.lastIndexOf('/'))}}" ng-href="rest/blacklist/download/{{item.id}}"><i title="Download" class="glyphicon glyphicon-cloud-download alignMiddle"></i></a>&nbsp;
										 <a ng-click="goToConfiguration(item)"><i title="Modifica" class="glyphicon glyphicon-pencil alignMiddle"></i></a>&nbsp; 
										 <a ng-click="showDelete($event,item)"><i title="Elimina" class="glyphicon glyphicon-trash alignMiddle"></i></a>&nbsp;
					 					<a ng-click="showDetails($event,item)"><i title="Dettagli" class="glyphicon glyphicon-list-alt alignMiddle"></i></a>&nbsp;
					 				</span>
					 				</div>
					 			</td>
					 		</tr>

					 		</tbody>
					 		</table>
					 		
						</div>
					</div>
				</div>

				<div class="listViewTopMenuDiv noprint">
					<div class="listViewActionsDiv row-fluid">
					
						<span class="btn-toolbar span3">
						</span>

						<span class="btn-toolbar span4">
						</span>
												 						
						<span class="span5 btn-toolbar">
							<div class="listViewActions pull-right">
							
						 		<div class="pageNumbers alignTop ">
						 			<span ng-show="ctrl.searchResults.hasPrev || ctrl.searchResults.hasNext">
						 				<span class="pageNumbersText" style="padding-right:5px">
						 					Record da <strong>{{ctrl.searchResults.first+1}}</strong> a <strong>{{ctrl.searchResults.last}}</strong>
						 				</span>
					 					<button title="Precedente" class="btn" id="listViewPreviousPageButton" type="button" 
					 							ng-click="navigateToPreviousPage()"
					 							ng-show="ctrl.searchResults.hasPrev">
					 						<span class="glyphicon glyphicon-chevron-left"></span>
					 					</button>
					 					<button title="Successiva" class="btn" id="listViewNextPageButton" type="button"
					 							ng-click="navigateToNextPage()"
					 							ng-show="ctrl.searchResults.hasNext">
					 						<span class="glyphicon glyphicon-chevron-right"></span>
					 					</button>
					 				</span>
						 		</div>
					 			
					 		</div>
					 		<div class="clearfix"></div>
					 	</span>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>