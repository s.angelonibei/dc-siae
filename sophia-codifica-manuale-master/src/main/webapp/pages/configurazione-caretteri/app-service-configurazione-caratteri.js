codmanApp.service('configurazioneCharService', ['$http',
    function ($http) {
        let service = this;
        const baseUrl = "rest/ccid/specialchar";
        const getList = "/getList";
        const save = '/save';
        const remove = '/delete';

        service.getList = function (dspList) {
            return new Promise((resolve, reject) => {
                $http({
                    method: 'GET',
                    url: `${baseUrl}${getList}`,
                    params: dspList
                }).then(({ data }) => {
                    resolve(data);
                }, (error) => {
                    reject(error);
                });
            });
        };
        /* 
        *   idDsp: string
        *   charList: string
        *   titleLength: number
        *   enabled: string
        */
        service.saveConfig = function (conf) {
            return new Promise((resolve, reject) => {
                $http({
                    method: 'POST',
                    url: `${baseUrl}${save}`,
                    data: {
                        ...conf
                    }
                }).then(({ data }) => {
                    resolve(data);
                }, (error) => {
                    reject(error);
                });
            });
        };
        service.removeConfig = function (idDsp) {
            const params = { idDsp };

            return new Promise((resolve, reject) => {
                $http({
                    method: 'DELETE',
                    url: `${baseUrl}${remove}`,
                    params
                })
                    .then(({ data }) => {
                        resolve(data);
                    }, (error) => {
                        reject(error);
                    });
            });
        };

    }]);