/* 
 * input 
 * 
 */
function EditConfigurationDialogController($scope, ngDialog) {
    const { id } = $scope.ngDialogId;
    const DEFAULT_CHARS = `; \\t | "`;
    const DEFAULT_ENABLED = true;
    const DEFAULT_LENGTH = 60;
    const TAB_CODE = 9;
    const SPACE_CODE = 32;

    $scope.onInit = function () {
        $scope.editMode = $scope.ngDialogData.editMode;
        $scope.caratteriSpeciali = DEFAULT_CHARS;
        $scope.enabled = DEFAULT_ENABLED;
        $scope.lunghezzaTitolo = DEFAULT_LENGTH;
        if ($scope.editMode) {
            $scope.caratteriSpeciali = $scope.ngDialogData.charList;
            $scope.lunghezzaTitolo = $scope.ngDialogData.titleLength;
            $scope.enabled = $scope.ngDialogData.enabled;
        }
    };

    $scope.cancel = function () {
        ngDialog.close(id);
    };

    $scope.confirm = function () {
        $scope.markFormAsTouched($scope.dialogForm);
        $scope.dialogForm.$setValidity('allowTab', true);
        if ($scope.dialogForm.$valid) {
            const {
                caratteriSpeciali,
                lunghezzaTitolo,
                enabled,
            } = $scope;
            let idDsp, dspSelected;
            if ($scope.editMode) {
                idDsp = $scope.ngDialogData.idDsp;
            } else {
                dspSelected = $scope.dspSelected;
            }
            ngDialog.close(id,
                {
                    confirm: true,
                    dspSelected,
                    idDsp,
                    caratteriSpeciali,
                    lunghezzaTitolo,
                    enabled,
                    context: $scope.editMode ? 'edit' : 'new'
                });
        }
    };

    $scope.markFormAsTouched = function (formController) {
        formController.$$controls.forEach(child => {
            child && child.$setTouched();
        });
    };

    $scope.onKeyDown = function ($event) {
        console.log($event);
        if ($event.keyCode === TAB_CODE) {
            $scope.caratteriSpeciali = `${($scope.caratteriSpeciali || "")}\\t`;
            $event.preventDefault();
            $event.stopPropagation();
        } else if ($event.keyCode === SPACE_CODE) {
			$scope.caratteriSpeciali = `${($scope.caratteriSpeciali || "")} `;
            $event.preventDefault();
            $event.stopPropagation();
		}
    };

    $scope.onInit();

}
