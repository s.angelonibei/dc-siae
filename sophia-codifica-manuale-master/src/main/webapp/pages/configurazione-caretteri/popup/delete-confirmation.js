/* 
 * input 
 * tableHeader, tableData
 */
function DeleteDialogController($scope, ngDialog) {
    const { id } = $scope.ngDialogId;
	const { tableData } = $scope.ngDialogData;
	$scope.data = Object.values(tableData)
		.map((value) => ( value ));

    $scope.getTableHeader = function () {
        const { tableHeader } = $scope.ngDialogData;
        return tableHeader;
    };

    $scope.getTableData = function () {
        return $scope.data;
    };

    $scope.cancel = function () {
        ngDialog.close(id);
    };

    $scope.confirm = function () {
        ngDialog.close(id, { confirm: true });
    };

}
