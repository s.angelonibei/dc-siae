/**
 * ConfigurazioneCharCtrl
 * 
 * @path /configurazione-char
 */
codmanApp.controller('ConfigurazioneCharCtrl', ['$scope', 'ngDialog', 'configurazioneCharService', 'pricingService',
    function ($scope, ngDialog, configurazioneCharService, pricingService) {

        const TABLE_HEADER = ["Nome Dsp", "Lista caretteri speciale ", "Lunghezza titolo consentito"];
        $scope.messages = {
            info: "",
            error: "",
            warning: ""
        };

        $scope.infoMessage = function (message) {
            $scope.clearMessages();
            setTimeout(() => {
                $scope.messages.info = message;
                $scope.$apply();
            }, 100);
        };

        $scope.errorMessage = function (message) {
            $scope.clearMessages();
            setTimeout(() => {
                $scope.messages.error = message;
                $scope.$apply();
            }, 100);
        };

        $scope.warningMessage = function (message) {
            $scope.clearMessages();
            setTimeout(() => {
                $scope.messages.warning = message;
                $scope.$apply();
            }, 100);
        };

        $scope.clearMessages = function () {
            $scope.messages = {
                info: "",
                error: "",
                warning: ""
            };
        };


        $scope.onInit = function () {
            pricingService.getDspUtilizationsCommercialOffersCountries()
                .then(({ dsp }) => {
                    $scope.setDspList(dsp);
                });
            $scope.getConfigurationList();
        };

        $scope.createConf = function () {
            const dialog = $scope.openConfigurationDialog({
                editMode: false,
                dspList: $scope.getDspList()
            });
            dialog
                .closePromise
                .then(function ({ value }) {
                    const { dspSelected, caratteriSpeciali: charList, lunghezzaTitolo: titleLength, enabled, context } = value;
                    const conf = {
                        idDsp: dspSelected.idDsp,
                        charList,
                        titleLength: parseInt(titleLength),
                        enabled: enabled.toString(),
                        context
                    };
                    $scope.invokeSave(conf);
                });

        };

        $scope.editConf = function (rowSelected) {
            const dialog = $scope.openConfigurationDialog({
                editMode: true,
                ...rowSelected
            });
            dialog
                .closePromise
                .then(function ({ value }) {
                    const { idDsp, caratteriSpeciali: charList, lunghezzaTitolo: titleLength, enabled: strEnabled, context } = value;
                    const conf = {
                        idDsp,
                        charList,
                        titleLength: parseInt(titleLength),
                        enabled: strEnabled ? "Y" : "N",
                        context
                    };
                    $scope.invokeSave(conf);
                });
        };

        $scope.openConfigurationDialog = function (data) {
            return ngDialog.open({
                template: 'pages/configurazione-caretteri/popup/edit-configuration.html',
                plain: false,
                className: 'ngdialog-theme-default flexRowHNoneVCenter',
                data,
                controller: ['$scope', 'ngDialog', EditConfigurationDialogController],
            });
        };

        $scope.invokeSave = function (conf) {
            // base64 encryption
            body = {
                ...conf,
                charList: btoa(conf.charList)
            };
            configurazioneCharService.saveConfig(body)
                .then(_response => {
                    $scope.getConfigurationList();
                    $scope.infoMessage("configurazione creata con successo");
                }, (error) => {
                    $scope.errorMessage("configurazione fallita ");
                });
        };

        $scope.deleteConf = function (item) {
            const tableData = {
                ..._.omit(item, 'enabled'),
                idDsp: $scope.getDspFromId(item.idDsp).name
            };
            const data = {
                tableHeader: TABLE_HEADER,
                tableData
            };

            ngDialog.open({
                template: 'pages/configurazione-caretteri/popup/delete-confirmation.html',
                width: '60%',
                className: 'ngdialog-theme-default dialogOverflow flexRowHNoneVCenter',
                controller: ['$scope', 'ngDialog', DeleteDialogController],
                data
            }).closePromise
                .then((deleteConfirmation) => {
                    if (deleteConfirmation) {
                        configurazioneCharService.removeConfig(item.idDsp)
                            .then((_data) => {
                                $scope.getConfigurationList();
                                $scope.infoMessage("configurazione cancellata con successo");
                            });
                    }
                });
        };

        $scope.getConfigurationList = function () {

            const dspList = {
                dspList: $scope.dspSelected ? $scope.dspSelected.map(({ idDsp }) => idDsp) : null
            };
            configurazioneCharService.getList(dspList)
                .then(({ rows = [] }) => {
                    setTimeout(() => {

                        $scope.configurationList = rows
                            .map(r => ({ 
								...r,
								enabled: r.enabled === "Y" ? true : false,
								charList: atob(r.charList)
							}));
                        $scope.$apply();
                    }, 100);
                });
        };

        $scope.getDspFromId = function (idDsp) {
            return $scope.getDspList()
                .find(el => el.idDsp === idDsp)
        };

        $scope.setDspList = function (list) {
            if (!$scope.dspList) {
                $scope.dspList = list;
            }
        };

        $scope.getDspList = function () {
            return $scope.dspList || [];
        };

        $scope.getTableHeader = function () {
            return TABLE_HEADER;
        };

        $scope.onInit();

    }
]);