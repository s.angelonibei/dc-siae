<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../navbar.jsp"%>

<div class="bodyContents">

    <div class="row-fluid">

        <div class="contents-topscroll noprint">
            <div class="topscroll-div" style="width: 95%; margin-bottom: 20px">&nbsp;</div>
        </div>

        <div class="sticky">
            <div id="companyLogo" class="navbar commonActionsContainer noprint">
                <div class="actionsContainer row-fluid">
                    <div class="span2">
                        <span class="companyLogo">
                            <img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;
                        </span>
                    </div>
                    <span class="btn-toolbar span8">
                        <span class="customFilterMainSpan btn-group" style="text-align: center;">
                            <div class="pageNumbers alignTop ">
                                <span class="pageNumbersText">
                                    <strong style="text-transform: uppercase;">Configurazione caratteri CCID</strong>
                                </span>
                            </div>
                        </span>
                    </span>
                </div>
            </div>
        </div>

        <md-card id="pricingContainer" md-theme="{{ showDarkTheme ? 'dark-grey' : 'default' }}" md-theme-watch>
            <md-content style="overflow: hidden;">
                <!-- MESSAGES CONTAINERS -->
                <div class="alert alert-danger" ng-show="messages.error && (!messages.info)">
                    <button type="button" class="close" ng-click="clearMessages()" aria-hidden="true">x</button>
                    <span><strong>Errore: </strong>{{messages.error}}</span>
                </div>
                <div class="alert alert-success" ng-show="messages.info && (!messages.error)">
                    <button type="button" class="close" ng-click="clearMessages()" aria-hidden="true">x</button>
                    <span>{{messages.info}}</span>
                </div>
                <div class="alert alert-warning" ng-show="messages.warning">
                    <button type="button" class="close" aria-hidden="true">x</button>
                    <span>{{messages.warning}}</span>
                </div>
                <!-- HEADER CONTAINER -->
                <form name="importForm" novalidate style="flex-direction: row;
                    display: flex; margin: 8px; background-color: #e7e7e7 ; border: 1px solid #ccc">

                    <div id="headerContainer pricingCustom" class="flexRowCenterSpaceEvenly">
                        <md-input-container class="md-block"
                            style="flex: 1 1 100%; box-sizing: border-box; max-width: 20%;">
                            <h3>DSP</h3>
                            <md-select name="dsp" multiple ng-change="getConfigurationList()"
                                ng-model-options="{ trackBy: '$value.idDsp' }" ng-model="dspSelected" aria-label="label"
                                ng-disabled="disableHeaderForm" md-no-asterisk>
                                <md-option ng-repeat="dsp in dspList" ng-value="{{dsp}}">
                                    {{ dsp.name }}
                                </md-option>
                            </md-select>
                        </md-input-container>
                    </div>
                </form>
                <!-- HEADER BUTTON -->
                <div class="flexColumnEndCenter margin-2x">

                    <button class="btn" ng-click="createConf()">
                        <span class="glyphicon glyphicon-plus"></span>
                        Nuova configurazione
                    </button>
                </div>
                <!-- page Container -->
                <div class="flexRowHCenterVCenter" id="configurationeCaratteriContainer">
                    <div class="table-responsive text-nowrap horizontal-scroll">
                        <table class="table table-bordered listViewEntriesTable tableW60">
                            <thead>
                                <tr>
                                    <th ng-repeat="head in getTableHeader()">{{head}}</th>
                                    <!-- empty column for buttons -->
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- "idDsp", "Lista caretteri speciale ", "Lunghezza titolo consentito"); -->
                                <tr ng-show="getDspList().length > 0" mdbTableCol
                                    ng-repeat="item in configurationList track by $index">
                                    <td>{{getDspFromId(item.idDsp).name}}</td>
                                    <td>
                                        <div style="white-space: break-spaces;">{{item.charList}}</div>
                                    </td>
                                    <td>{{item.titleLength}}</td>
                                    <!-- buttons -->
                                    <td class="flexRowCenterSpaceEvenly">
                                        <a ng-click="editConf(item)"><i title="Modifica configurazione"
                                                class="glyphicon glyphicon-edit alignMiddle"></i></a>
                                        <a ng-click="deleteConf(item)"><i title="Modifica configurazione"
                                                class="glyphicon glyphicon-remove alignMiddle"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div ng-show="!configurationList.length || getDspList().length === 0" style="margin-top: 16px;">
                            <div style="text-align: center;">Al momento non ci sono record.</div>
                        </div>
                    </div>
                </div>
            </md-content>
        </md-card>
    </div>
</div>