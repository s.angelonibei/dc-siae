<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%@
	include file="../navbar.jsp" %>

<div class="bodyContents">

	<div class="mainContainer row-fluid">

		<div id="companyLogo" class="navbar commonActionsContainer noprint">
			<div class="actionsContainer row-fluid">
				<div class="span2">
					<span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
				</div>
				<span class="btn-toolbar span5" style="margin-left: 0%">
					<div class="pageNumbers alignTop pull-right" style="text-align: center;">
						<span class="pageNumbersText"><strong>Lavorazione DSR</strong></span>
					</div>
				</span>
			</div>
		</div>

		<div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px;">
			<div class="listViewPageDiv">
				<div class="listViewTopMenuDiv noprint">

					<div class="alert alert-danger" ng-show="ctrl.messages.error" style="margin-top:20px">
						<button type="button" class="close" aria-hidden="true"
							ng-click="ctrl.messages.clear()">x</button>
						<span><strong>Attenzione: </strong>{{ctrl.messages.error}}</span>
					</div>
					<div class="alert alert-success" ng-show="ctrl.messages.info" style="margin-top:20px">
						<button type="button" class="close" aria-hidden="true"
							ng-click="ctrl.messages.clear()">x</button>
						<span>{{ctrl.messages.info}}</span>
					</div>

					<div class="listViewActionsDiv row-fluid">
						<span class="span12 btn-toolbar">
							<filter-dsp-statistics dsps-utilizations-commercial-offers-countries="dspsUtilizationsCommercialOffersCountries"
							filter-parameters="filterParameters"
							on-filter-apply="searchDsrs(parameters)"
							hide-back-claim-select="true"
							hide-client-select="true"
							show-dsr-processing-statuses="true"
							show-id-dsr="true"
							dsr-processing-statuses="dsrProcessingStatuses"
							date-from="currentPeriod"
							date-to="currentPeriod">
							</filter-dsp-statistics>
							<div class="clearfix"></div>
						</span>
					</div>
				</div>

				<div class="listViewTopMenuDiv noprint">
					<div class="listViewActionsDiv row-fluid">
						<span class="span12 btn-toolbar">
						</span>
					</div>
				</div>

				<div class="listViewEntriesDiv contents-bottomscroll">
					<div class="bottomscroll-div" style="width: 95%; min-height: 80px">

						<table class="table table-bordered listViewEntriesTable ">
							<thead>
								<tr>
									<th style="text-align: center; width:20%" ng-repeat="stat in ctrl.data.extra.stats">
										{{stat.header}}
									</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="text-align: center;" ng-repeat="stat in ctrl.data.extra.stats">
										{{stat.value}}
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>

				<!-- start toolbar tabella | paginazione -->
				<div class="listViewTopMenuDiv noprint">
					<div class="listViewActionsDiv row-fluid">
						<span class="span12 btn-toolbar">
							<div class="listViewActions pull-right">
								<div class="pageNumbers alignTop ">
									<span>
										<span class="pageNumbersText" style="padding-right: 5px">
											Record da <strong>{{ctrl.data.first}}</strong> a
											<strong>{{ctrl.data.last}}</strong>
											su
											<strong>{{ctrl.data.maxrows}}</strong>
										</span>
										<button title="Precedente" class="btn" id="listViewPreviousPageButton"
											type="button" ng-click="navigateToPreviousPage()"
											ng-show="ctrl.data.first > 0">
											<span class="glyphicon glyphicon-chevron-left"></span>
										</button>
										<button title="Successiva" class="btn" id="listViewNextPageButton" type="button"
											ng-click="navigateToNextPage()"
											ng-show="ctrl.data.maxrows > ctrl.data.last">
											<span class="glyphicon glyphicon-chevron-right"></span>
										</button>
										<%--<button title="Fine" class="btn" id="listViewNextPageButton" type="button"--%>
											<%--ng-click="navigateToEndPage()" ng-show="ctrl.data.last <= ctrl.data.maxrows">--%>
											<%--<span class="glyphicon glyphicon-step-forward"></span>--%>
										<%--</button>--%>
									</span>
								</div>
							</div>
						</span>
					</div>
				</div>
				<!-- end toolbar tabella | paginazione -->
				<div class="listViewTopMenuDiv noprint">
					<div class="listViewActionsDiv row-fluid">
						<span class="span12 btn-toolbar">
							<div class="listViewActions pull-right">
								<div class="pageNumbers alignTop ">
									<span>
										<button type="button" class="btn addButton" ng-click="emptyDsrsSelection()"
												ng-show="ctrl.selectedDsrs.length > 0">
											<span class="glyphicon glyphicon-erase"></span>&nbsp;&nbsp;<strong>
												Cancella Selezione</strong>
										</button>
										<button type="button" class="btn addButton" ng-click="checkDsrsProcessing()"
												ng-show="ctrl.selectedDsrs.length > 0">
											<span class="glyphicon glyphicon-play"></span>&nbsp;&nbsp;<strong>Avvia
												Lavorazione</strong>
										</button>
									</span>

								</div>
							</div>
						</span>
					</div>
				</div>
				<!-- table -->
				<div class="listViewEntriesDiv contents-bottomscroll">
					<div class="bottomscroll-div" style="width: 95%; min-height: 80px">

						<table class="table table-bordered listViewEntriesTable ">
							<thead>
								<tr class="listViewHeaders">
									<th>
										<span class="glyphicon glyphicon-unchecked" style="cursor: pointer"
											  ng-click="selectCurrentPage()" ng-show="!ctrl.pageSelected"
											  data-toggle="tooltip" data-placement="top"
											  title="Seleziona pagina corrente">
										</span>
										<span class="glyphicon glyphicon-check" style="cursor: pointer"
											  ng-click="deselectCurrentPage()" ng-show="ctrl.pageSelected"
											  data-toggle="tooltip" data-placement="top"
											  title="Deseleziona pagina corrente">
										</span>
									</th>
									<th style="width: 7%" ng-click="sortedSearch('spanDSR','dsr')"
										style="cursor: pointer">Nome DSR&nbsp;<span id="spanDSR" style="font-size:80%"
											class="glyphicon glyphicon-sort"></span></th>
									<th style="width: 7%" ng-click="sortedSearch('spanNomeDSP','dsp')"
										style="cursor: pointer">Nome DSP&nbsp;<span id="spanNomeDSP"
											style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
									<th style="width: 7%" ng-click="sortedSearch('spanPeriodo','period')"
										style="cursor: pointer">Periodo&nbsp;<span id="spanPeriodo"
											style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
									<th style="width: 7%"
										ng-click="sortedSearch('spanUtilizationType','utilizationType')"
										style="cursor: pointer">Tipo Utilizzo&nbsp;<span id="spanUtilizationType"
											style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
									<th style="width: 7%"
										ng-click="sortedSearch('spanCommercialOffer','commercialOffer')"
										style="cursor: pointer">Offerta commerciale&nbsp;<span id="spanCommercialOffer"
											style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
									<th style="width: 7%" ng-click="sortedSearch('spanCountry','country')"
										style="cursor: pointer">Territorio&nbsp;<span id="spanCountry"
											style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
									<th style="width: 12%"
										ng-click="sortedSearch('spanDsrDeliveryDate','dsrDeliveryDate')"
										style="cursor: pointer">Data arrivo file&nbsp;<span id="spanDsrDeliveryDate"
											style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
									<th style="width: 12%"
										ng-click="sortedSearch('spanDsrProcessingStart','spanDsrProcessingStart')"
										style="cursor: pointer">Data inizio lavorazione&nbsp;<span
											id="spanDsrProcessingStart" style="font-size:80%"
											class="glyphicon glyphicon-sort"></span></th>
									<th style="width: 12%"
										ng-click="sortedSearch('spanDsrProcessingEnd','dsrProcessingEnd')"
										style="cursor: pointer">Data fine lavorazione&nbsp;<span
											id="spanDsrProcessingEnd" style="font-size:80%"
											class="glyphicon glyphicon-sort"></span></th>
									<th style="width: 7%" ng-click="sortedSearch('spanStatus','status')"
										style="cursor: pointer">Stato&nbsp;<span id="spanStatus" style="font-size:80%"
											class="glyphicon glyphicon-sort"></span></th>
									<th style="width: 7%" ng-click="sortedSearch('spanErrorType','errorType')"
										style="cursor: pointer">Tipo Errore&nbsp;<span id="spanErrorType style="
											font-size:80%" class="glyphicon glyphicon-sort"></span></th>
									<th style="width: 7%" ng-click="sortedSearch('spanKbVersion','kbVersion')"
										style="cursor: pointer">Versione KB&nbsp;<span id="spanKbVersion"
											style="font-size:80%" class="glyphicon glyphicon-sort"></span></th>
									
								</tr>
							</thead>
							<tbody>
								<tr class="listViewEntries " ng-repeat="row in ctrl.data.rows"
									ng-click="flipDsrSelection(row)" style="cursor: pointer"
									ng-class="{true:'dsr-row selected', false:'dsr-row'}[row.selected]">
									<td class="listViewEntryValue">
										<span class="glyphicon glyphicon-check" ng-show="row.selected"></span>
									</td>
									<td class="listViewEntryValue">{{row.dsr}}</td>
									<td class="listViewEntryValue">{{row.dspName}}</td>
									<td class="listViewEntryValue">{{formatPeriod(row.dsrPeriod)}}</td>
									<td class="listViewEntryValue">{{row.utilizationName}}</td>
									<td class="listViewEntryValue">{{row.commercialOfferName}}</td>
									<td class="listViewEntryValue">{{row.countryName}}</td>
									<td class="listViewEntryValue">{{row.dsrDeliveryDate | date: 'dd-MM-yyyy HH:mm:ss'}}
									</td>
									<td class="listViewEntryValue">
										{{row.dsrProcessingStart | date: 'dd-MM-yyyy HH:mm:ss'}}</td>
									<td class="listViewEntryValue">
										{{row.dsrProcessingEnd | date: 'dd-MM-yyyy HH:mm:ss'}}</td>
									<td class="listViewEntryValue">{{row.processingStatus.description}}</td>
									<td class="listViewEntryValue">{{row.errorType}}</td>
									<td class="listViewEntryValue">{{row.kbVersion}}</td>
									
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<!-- table -->

				<!-- start bottom toolbar tabella -->
				<div class="listViewTopMenuDiv noprint">
					<div class="listViewActionsDiv row-fluid">
						<span class="span12 btn-toolbar">
							<div class="listViewActions pull-right">
								<div class="pageNumbers alignTop ">
									<span>
										<button type="button" class="btn addButton" ng-click="emptyDsrsSelection()"
											ng-show="ctrl.selectedDsrs.length > 0">
											<span class="glyphicon glyphicon-erase"></span>&nbsp;&nbsp;<strong>
												Cancella Selezione</strong>
										</button>
										<button type="button" class="btn addButton" ng-click="checkDsrsProcessing()"
											ng-show="ctrl.selectedDsrs.length > 0">
											<span class="glyphicon glyphicon-play"></span>&nbsp;&nbsp;<strong>Avvia
												Lavorazione</strong>
										</button>
									</span>

								</div>
							</div>
						</span>
					</div>
				</div>

			</div>

		</div>
	</div>
</div>
<%--<pre>{{ctrl | json}}</pre>--%>