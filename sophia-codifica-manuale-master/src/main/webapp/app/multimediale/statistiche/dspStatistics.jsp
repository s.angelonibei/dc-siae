<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
	<%@ include file="../../../pages/navbar.jsp" %>

		<div class="bodyContents">

			<div class="mainContainer row-fluid">

				<div id="companyLogo" class="navbar commonActionsContainer noprint">
					<div class="actionsContainer row-fluid">
						<div class="span2">
							<%--<span class="companyLogo"><img src="images/siae_logo_siaetrasparente.jpg" title="SIAE"
									alt="SIAE">&nbsp;</span>--%>
								<span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE"
										alt="SIAE">&nbsp;</span>
						</div>
						<span class="btn-toolbar span4">
							<div class="pageNumbers alignTop pull-right" style="text-align: center;">
								<span class="pageNumbersText"><strong>DSP - Statistiche</strong></span>
							</div>
						</span>
					</div>
				</div>

				<div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px;">
					<!-- actions and pagination -->
					<div class="messagesContainer">

						<div class="alert alert-success" ng-show="messages.info">
							<button type="button" class="close" ng-click="clearMessages()" aria-hidden="true">x</button>
							<span>{{messages.info}}</span>
						</div>
					</div>
					<!-- qui va il filtro-->
					<filter-dsp-statistics
						dsps-utilizations-commercial-offers-countries="dspsUtilizationsCommercialOffersCountries"
						filter-parameters="filterParameters" on-filter-apply="filterApply(parameters)"
						hide-back-claim-select="false" hide-client-select="true">
					</filter-dsp-statistics>
					<div ng-if="values && values.rows.length" class="flexRowEnd" style="margin-top: 20px;">
						<button class="btn addButton" data-ng-click="exportAllResultToExcel()">
							<strong>Esporta tutto in Excel</strong>
						</button>
					</div>

					<div class="listViewTopMenuDiv noprint">
						<div class="listViewActionsDiv row-fluid">
							<span class="btn-toolbar span4">
								<span class="btn-group">

								</span>
							</span>
							<span class="btn-toolbar span4">
								<span class="customFilterMainSpan btn-group" style="text-align: center;">
									<div class="pageNumbers alignTop ">
										<span class="pageNumbersText"><strong>
												<!-- Titolo -->
											</strong></span>
									</div>
								</span>
							</span>
							<span class="span4 btn-toolbar">
								<div class="listViewActions pull-right">

									<div class="pageNumbers alignTop ">
										<span data-ng-show="values.hasPrev || values.hasNext">
											<span class="pageNumbersText" style="padding-right:5px">
												Record da <strong ng-bind="values.first+1"></strong> a <strong
													ng-bind="values.last"></strong>
											</span>
											<button title="Precedente" class="btn" id="listViewPreviousPageButton"
												type="button" ng-click="navigateToPreviousPage()"
												ng-show="values.hasPrev">
												<span class="glyphicon glyphicon-chevron-left"></span>
											</button>
											<button title="Successiva" class="btn" id="listViewNextPageButton"
												type="button" ng-click="navigateToNextPage()" ng-show="values.hasNext">
												<span class="glyphicon glyphicon-chevron-right"></span>
											</button>
										</span>
									</div>
								</div>
								<div class="clearfix"></div>
							</span>
						</div>
					</div>

					<!-- table -->
					<div class="listViewContentDiv" id="listViewContents">

						<div class="contents-topscroll noprint">
							<div class="topscroll-div" style="width: 95%">&nbsp;</div>
						</div>

						<div ng-if="values && values.rows.length" class="listViewEntriesDiv contents-bottomscroll">
							<div class="bottomscroll-div" style="width: 95%; min-height: 80px">
								<table class="table table-bordered listViewEntriesTable">
									<thead>
										<tr class="listViewHeaders">
											<th></th>
											<th><a href="" class="listViewHeaderValues">DSP</a></th>
											<th style="text-align: center;">DSR</th>
											<th>Periodo</th>
											<th>Territorio</th>
											<th>Tipo di utilizzo</th>
											<th>Offerta commerciale</th>
											<th>% Claim</th>
											<!--AA aggiunta campo %claim-->
											<th>Contatore backclaim</th>
											<th>Tipo BC</th>
											<th style="text-align: right;">Totale Valore</a></th>
											<th style="text-align: right;">Totale utilizzazioni</a></th>
											<th style="text-align: right;">Identificato valore</a></th>
											<th style="text-align: right;">% Identificato utilizzazioni</a></th>
											<th style="text-align: right;">Valore claim (&euro;)</a></th>
											<th style="text-align: right;">Utilizzazioni claim SIAE</a></th>
											<th style="text-align: right;">Non identificato valore</a></th>
											<th style="text-align: right;">Non identificato visualizzazioni</a></th>
											<th style="text-align: right;">Importo CCID</a></th>

											<th></th>
										</tr>
									</thead>
									<tbody ng-repeat-start="item in values.rows | orderBy:'dsp' track by $index">
										<tr style="outline:none;" class="clickable-stat">
											<td style="vertical-align: middle;">
												<div ng-click="drilldown(item)" aria-expanded="item.expanded"
													ng-show="item.hasDrilldown"><i ng-show="!item.expanded"
														class="fa fa-plus fa-xs" aria-hidden="true"></i>
													<i ng-show="item.expanded" class="fa fa-minus fa-xs"
														aria-hidden="true"></i>
												</div>
											</td>
											<td class="listViewEntryValue" ng-bind="item.dsp"></td>
											<td class="listViewEntryValue">
												<a data-ng-bind="item.dsr"></a>
											</td>
											<td class="listViewEntryValue" data-ng-bind="item.period"></td>
											<td class="listViewEntryValue" data-ng-bind="item.country"></td>
											<td class="listViewEntryValue" data-ng-bind="item.utilizationType"></td>
											<td class="listViewEntryValue" data-ng-bind="item.commercialOffer"></td>
											<td>
												<button ng-disabled="item.percClaim != false"
													data-ng-click="showPercClaim(item)"
													ng-class="{'dot-red' : item.percClaim == false,'dot-green' : item.percClaim == true, 'dot-null' : item.percClaim == null }"></button>
											</td>
											<td style="text-align: center;">
												<a href="javascript:void(0)" data-ng-if="item.backclaim > 0"
													data-ng-click="openModal(item)" ng-bind="item.backclaim"></a>
												<span data-ng-if="item.backclaim <= 0" ng-bind="item.backclaim"></span>
											</td>
											<td class="listViewEntryValue" data-ng-bind="item.tipoBc"></td>
											<td class="listViewEntryValue"
												data-ng-bind="item.totValue | currency: item.currency : 2"
												style="text-align: right;"></td>
											<td class="listViewEntryValue" data-ng-bind="item.totUsage | number: 2"
												style="text-align: right;"></td>
											<td class="listViewEntryValue"
												data-ng-bind="item.identificatoValorePricing| number: 2"
												style="text-align: right;"></td>
											<td class="listViewEntryValue"
												data-ng-bind="item.usageIdentified| currency: '%' : 2"
												style="text-align: right;"></td>
											<td class="listViewEntryValue"
												data-ng-bind="item.siaeValueIdentified| number: 2"
												style="text-align: right;"> </td>
											<td class="listViewEntryValue"
												data-ng-bind="item.siaeIdentifiedVisualizations| number: 2"
												style="text-align: right;"></td>
											<td class="listViewEntryValue"
												data-ng-bind="item.valueNotIdentified| number: 2"
												style="text-align: right;"></td>
											<td class="listViewEntryValue"
												data-ng-bind="item.visualizationsNotIdentified| number: 0"
												style="text-align: right;"></td>
											<td class="listViewEntryValue"
												data-ng-bind="item.totalValueCcidCurrency| currency: item.ccidCurrency : 2"
												style="text-align: right;"></td>
											<td nowrap class="medium">
												<div class="actions pull-right">
													<span class="actionImages">
														<a data-ng-click="showInfo(item)"><i title="Dettaglio"
																class="glyphicon glyphicon-info-sign alignMiddle"></i></a>&nbsp;
														<span data-ng-if="item.ccidS3Path"><a
																download="{{item.ccidS3Path}}"
																data-ng-href="downloadCCID?id={{item.ccidS3Id}}"><i
																	title={{item.ccidS3Path}}
																	class="glyphicon  glyphicon-cloud-download alignMiddle"></i></a>&nbsp;</span>
														<span data-ng-if="item.extendedDsrUrl"><a
																download="{{item.extendedDsrUrl.substr(1 + item.extendedDsrUrl.lastIndexOf('/'))}}"
																data-ng-href="downloadExtendedDSR?url={{item.extendedDsrUrl | urlEncode}}"><i
																	title={{item.extendedDsrUrl}}
																	class="glyphicon glyphicon-list-alt	alignMiddle"></i></a>&nbsp;</span>
														<span><a
																data-ng-href="#!/dspStatistics/storico/{{item.dsr}}?{{filterParametersUrl}}">
																<i title="Storico"
																	class="fas fa-history alignMiddle"></i></a>&nbsp;</span>
													</span>
												</div>
											</td>
										</tr>
									</tbody>
									<tbody ng-repeat-end
										ng-class="{'collapse': !item.expanded, 'collapsing' : item.expanded}"
										aria-expanded="item.expanded">
										<tr ng-show="!item.children">
											<td class="listViewEntryValue" colspan="18">Non sono presenti statistiche
												divise per società</td>
										</tr>
										<tr ng-show="item.children && item.children.length > 0"
											ng-repeat="child in item.children">
											<td class="listViewEntryValue">{{child.societa}}</td>
											<td class="listViewEntryValue" ng-bind="item.dsp"></td>
											<td class="listViewEntryValue">
												<a data-ng-bind="item.dsr"></a>
											</td>
											<td class="listViewEntryValue" data-ng-bind="item.period"></td>
											<td class="listViewEntryValue" data-ng-bind="item.country"></td>
											<td class="listViewEntryValue" data-ng-bind="item.utilizationType"></td>
											<td class="listViewEntryValue" data-ng-bind="item.commercialOffer"></td>
											<td> </td>
											<td style="text-align: center;">
												<a href="javascript:void(0)" data-ng-if="item.backclaim > 0"
													data-ng-click="openModal(item)" ng-bind="item.backclaim"></a>
												<span data-ng-if="item.backclaim <= 0" ng-bind="item.backclaim"></span>
											</td>
											<td class="listViewEntryValue" data-ng-bind="item.tipoBc"></td>
											<td class="listViewEntryValue"
												data-ng-bind="item.totValue | currency: item.currency : 2"
												style="text-align: right;"></td>
											<td class="listViewEntryValue" data-ng-bind="item.totUsage | number: 0"
												style="text-align: right;"></td>
											<td class="listViewEntryValue"
												data-ng-bind="child.identificatoValore| number: 2"
												style="text-align: right;"></td>
											<td class="listViewEntryValue"
												data-ng-bind="child.percentualeIdentificatoUtilizzazioni| currency: '%' : 2"
												style="text-align: right;"></td>
											<td class="listViewEntryValue" data-ng-bind="child.claimValore| number: 2"
												style="text-align: right;"> </td>
											<td class="listViewEntryValue"
												data-ng-bind="child.claimUtilizzazioni| number: 2"
												style="text-align: right;"></td>
											<td class="listViewEntryValue"
												data-ng-bind="child.nonIdentificatoValore| number: 2"
												style="text-align: right;"></td>
											<td class="listViewEntryValue"
												data-ng-bind="child.nonIdentificatoUtilizzazioni| number: 0"
												style="text-align: right;"></td>
											<td class="listViewEntryValue"
												data-ng-bind="item.totalValueCcidCurrency| currency: item.ccidCurrency : 2"
												style="text-align: right;"></td>
											<td nowrap class="medium"></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- /table -->
				</div>
			</div>
		</div>
		<%--<pre>{{properties}}</pre>--%>
			</div>