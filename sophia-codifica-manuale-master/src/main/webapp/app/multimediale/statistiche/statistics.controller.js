/**
 * DspConfigCtrl
 *
 * @path /dspConfig
 */
codmanApp.controller('DspStatisticsCtrl', ['$scope', '$route', 'ngDialog', '$routeParams', '$location', '$http', 'configService', '$filter', 'dspsUtilizationsCommercialOffersCountries', '$httpParamSerializer', 'statsService',
  function ($scope, $route, ngDialog, $routeParams, $location, $http, configService, $filter, dspsUtilizationsCommercialOffersCountries, $httpParamSerializer, statsService) {
    $scope.dspsUtilizationsCommercialOffersCountries = dspsUtilizationsCommercialOffersCountries;
    $scope.filterParameters = $routeParams;

    $scope.messages = {
      info: "",
      error: "",
      warning: ""
    };

    $scope.clearMessages = function () {
      $scope.messages = {
        info: "",
        error: "",
        warning: ""
      }
    };

    $scope.infoMessage = function (message) {
      $scope.messages.info = message;
    };

    $scope.filterParameters = {
      first: $scope.filterParameters.first || 0,
      last: $scope.filterParameters.last || configService.maxRowsPerPage,
      selectedDspModel: $scope.filterParameters.selectedDspModel ? $scope.filterParameters.selectedDspModel = [].concat($scope.filterParameters.selectedDspModel).map(function (x) {
        return angular.fromJson(x)
      }) : void 0,
      selectedCountryModel: $scope.filterParameters.selectedCountryModel ? $scope.filterParameters.selectedCountryModel = [].concat($scope.filterParameters.selectedCountryModel).map(function (x) {
        return angular.fromJson(x)

      }) : void 0,
      selectedUtilizationModel: $scope.filterParameters.selectedUtilizationModel ? $scope.filterParameters.selectedUtilizationModel = [].concat($scope.filterParameters.selectedUtilizationModel).map(function (x) {
        return angular.fromJson(x)

      }) : void 0,
      selectedOfferModel: $scope.filterParameters.selectedOfferModel ? $scope.filterParameters.selectedOfferModel = [].concat($scope.filterParameters.selectedOfferModel).map(function (x) {
        return angular.fromJson(x)

      }) : void 0,
      selectedPeriodFrom: $scope.filterParameters.selectedPeriodFrom ? $scope.filterParameters.selectedPeriodFrom : void 0,
      selectedPeriodTo: $scope.filterParameters.selectedPeriodTo ? $scope.filterParameters.selectedPeriodTo : void 0,
      backclaim: $scope.filterParameters.backclaim ? $scope.filterParameters.backclaim : void 0
    };

    $scope.sheetTemplate = {
      headers: true,
      columns: [
        { columnid: 'dsp', title: 'DSP' },
        { columnid: 'period', title: 'Periodo' },
        { columnid: 'country', title: 'Territorio' },
        { columnid: 'utilizationType', title: 'Tipo di utilizzo' },
        { columnid: 'commercialOffer', title: 'Offerta commerciale' },
        { columnid: 'totValue', title: 'Totale Valore (€)' },
        { columnid: 'totUsage', title: 'Totale utilizzazioni' },
        { columnid: 'valueIdentified', title: 'Identificato valore' },
        { columnid: 'usageIdentified', title: '% Identificato utilizzazioni' },
        { columnid: 'siaeValueIdentified', title: 'Valore claim SIAE' },
        {
          columnid: 'siaeIdentifiedVisualizations',
          title: 'Utilizzazioni claim SIAE'
        },
        { columnid: 'valueNotIdentified', title: 'Non identificato valore' },
        {
          columnid: 'visualizationsNotIdentified',
          title: 'Non identificato visualizzazioni'
        },
        { columnid: 'currency', title: 'Valuta' },
        { columnid: 'totalValueCcidCurrency', title: 'Importo CCID' }
      ]
    };

    $scope.navigateToNextPage = function () {
      $scope.filterParameters.first = $scope.filterParameters.last;
      $scope.filterParameters.last = $scope.filterParameters.last + configService.maxRowsPerPage;
      $scope.getPagedResults($scope.filterParameters);
    };

    $scope.navigateToPreviousPage = function () {
      $scope.filterParameters.last = $scope.filterParameters.first;
      $scope.filterParameters.first = Math.max($scope.first - configService.maxRowsPerPage, 0);
      $scope.getPagedResults($scope.filterParameters);
    };


    // Funzione principale richiamata al caricamento della pagina
    $scope.onRefresh = function () {
      $scope.filterApply($scope.filterParameters);
    };

    $scope.exportAllResultToExcel = function () {
      $scope.getPagedResultsNoPagable(
        $scope.filterParameters
      );
    };


    $scope.formatNumber = function (number) {
      return twoDecimalRound(number);
    };

    $scope.formatInteger = function (number) {
      return zeroDecimalRound(number);
    };

    $scope.getPagedResults = function (parameters) {
      $scope.filterParameters = angular.copy(parameters);
      var periodFrom = 0;
      var yearFrom = 0;
      var periodTo = 0;
      var yearTo = 0;

      if ((parameters.selectedPeriodFrom)) {
        var from = parameters.selectedPeriodFrom.split('-');
        periodFrom = parseInt(from[0], 10);
        yearFrom = parseInt(from[1], 10);
      }

      if (parameters.selectedPeriodTo) {
        var to = parameters.selectedPeriodTo.split('-');
        periodTo = parseInt(to[0], 10);
        yearTo = parseInt(to[1], 10);
      }

      parameters.dspList = getCollectionValues(parameters.selectedDspModel);
      parameters.countryList = getCollectionValues(parameters.selectedCountryModel);
      parameters.utilizationList = getCollectionValues(parameters.selectedUtilizationModel);
      parameters.offerList = getCollectionValues(parameters.selectedOfferModel);
      parameters.monthFrom = periodFrom;
      parameters.yearFrom = yearFrom;
      parameters.monthTo = periodTo;
      parameters.yearTo = yearTo;

      const filterApplied = Object
        .entries(_.pick(parameters, ['dspList', 'countryList', 'utilizationList', 'offerList', 'monthFrom', 'yearFrom', 'monthTo', 'yearTo']))
        .find(([_k, v]) => v.length);

      if (filterApplied) { // tbd     
        $scope.clearMessages();
        $http({
          method: 'GET',
          url: 'rest/dspStatistics/all',
          params: {
            first: parameters.first || 0,
            last: parameters.last || configService.maxRowsPerPage,
            dspList: parameters.dspList ? parameters.dspList : [],
            countryList: parameters.countryList ? parameters.countryList : [],
            utilizationList: parameters.utilizationList ? parameters.utilizationList : [],
            offerList: parameters.offerList ? parameters.offerList : [],
            monthFrom: parameters.monthFrom ? parameters.monthFrom : 0,
            yearFrom: parameters.yearFrom ? parameters.yearFrom : 0,
            monthTo: parameters.monthTo ? parameters.monthTo : 0,
            yearTo: parameters.yearTo ? parameters.yearTo : 0,
            backclaim: parameters.backclaim
          }
        }).then(function successCallback(response) {
          $scope.values = response.data;
        });
      } else {
        $scope.infoMessage("Selezionare almeno un filtro per la ricerca.");
      }
    };

    $scope.getPagedResultsNoPagable = function (parameters) {
      $scope.filterParameters = angular.copy(parameters);
      var periodFrom = 0;
      var yearFrom = 0;
      var periodTo = 0;
      var yearTo = 0;

      if ((parameters.selectedPeriodFrom)) {
        var from = parameters.selectedPeriodFrom.split('-');
        periodFrom = parseInt(from[0], 10);
        yearFrom = parseInt(from[1], 10);
      }

      if (parameters.selectedPeriodTo) {
        var to = parameters.selectedPeriodTo.split('-');
        periodTo = parseInt(to[0], 10);
        yearTo = parseInt(to[1], 10);
      }

      parameters.dspList = getCollectionValues(parameters.selectedDspModel);
      parameters.countryList = getCollectionValues(parameters.selectedCountryModel);
      parameters.utilizationList = getCollectionValues(parameters.selectedUtilizationModel);
      parameters.offerList = getCollectionValues(parameters.selectedOfferModel);
      parameters.monthFrom = periodFrom;
      parameters.yearFrom = yearFrom;
      parameters.monthTo = periodTo;
      parameters.yearTo = yearTo;

      const filterApplied = Object
        .entries(_.pick(parameters, ['dspList', 'countryList', 'utilizationList', 'offerList', 'monthFrom', 'yearFrom', 'monthTo', 'yearTo']))
        .find(([_k, v]) => v.length);

      if (filterApplied) { // tbd   
        $scope.clearMessages();    
        $http({
          method: 'GET',
          url: 'rest/dspStatistics/allNotPagable',
          params: {
            dspList: parameters.dspList ? parameters.dspList : [],
            countryList: parameters.countryList ? parameters.countryList : [],
            utilizationList: parameters.utilizationList ? parameters.utilizationList : [],
            offerList: parameters.offerList ? parameters.offerList : [],
            monthFrom: parameters.monthFrom ? parameters.monthFrom : 0,
            yearFrom: parameters.yearFrom ? parameters.yearFrom : 0,
            monthTo: parameters.monthTo ? parameters.monthTo : 0,
            yearTo: parameters.yearTo ? parameters.yearTo : 0,
            backclaim: parameters.backclaim
          }
        }).then(function successCallback(response) {
          $scope.valuesExport = response.data;

          // qui bisogna mappare i risultati
          var result = [];
          $scope.valuesExport.forEach(function (item) {
            var rowItem = {
              dsp: item.dsp,
              dateFrom: item.dateFrom,
              dateTo: item.dateTo,
              country: item.country,
              utilizationType: item.utilizationType,
              commercialOffer: item.commercialOffer,
              totValue: item.totValue,
              totUsage: item.totUsage,
              valueIdentified: item.valueIdentified,
              identificatoValorePricing: item.identificatoValorePricing,
              usageIdentified: item.usageIdentified,
              siaeValueIdentified: item.siaeValueIdentified,
              siaeIdentifiedVisualizations: item.siaeIdentifiedVisualizations,
              valueNotIdentified: item.valueNotIdentified,
              visualizationsNotIdentified: item.visualizationsNotIdentified,
              currency: item.currency,
              totalValueCcidCurrency: item.totalValueCcidCurrency,
              societaTutela: item.societaTutela,
              ccidName: item.ccidName,
              tipoBc: item.tipoBc,
              ccidCurrency: item.ccidCurrency
            };
            result.push(rowItem);
          });

          var resultFiltered = $filter('orderBy')(result, 'dsp', false);

          var rowItem = [['DSP', 'Data da', 'Data a', 'Territorio', 'Tipo di utilizzo', 'Offerta commerciale', 'Totale Valore', 'Valuta', 'Totale utilizzazioni', 'Identificato valore', '% Identificato utilizzazioni', 'Valore claim (Euro)', 'Utilizzazioni claim SIAE', 'Non identificato valore', 'Non identificato visualizzazioni', 'Importo CCID', 'Valuta CCID', 'Nome CCID', 'Societa Tutela', 'Tipo BC']];
          angular.forEach(resultFiltered, function (value, key) {
            rowItem.push([value.dsp, new Date(value.dateFrom), new Date(value.dateTo), value.country, value.utilizationType, value.commercialOffer, value.totValue, value.currency, value.totUsage, value.identificatoValorePricing, value.usageIdentified, value.siaeValueIdentified, value.siaeIdentifiedVisualizations, value.valueNotIdentified, value.visualizationsNotIdentified, value.totalValueCcidCurrency, value.ccidCurrency, value.ccidName, value.societaTutela, value.tipoBc]);
          });

          // definisce il nome del worksheet
          var ws_name = "Statistiche_DSP";
          // crea il worksheet
          var ws = XLSX.utils.aoa_to_sheet(rowItem);

          // crea il workbook
          var wb = XLSX.utils.book_new();
          // imposta il nome del worksheet
          wb.SheetNames.push(ws_name);

          /*
           * Oggetto per la definizione dello stile delle colonne
           */
          ws['!cols'] = [
            { wpx: 150 },
            { wpx: 70 },
            { wpx: 70 },
            { wpx: 150 },
            { wpx: 150 },
            { wpx: 100 },
            { wpx: 100 },
            { wpx: 100 },
            { wpx: 150 },
            { wpx: 120 },
            { wpx: 130 },
            { wpx: 130 },
            { wpx: 130 },
            { wpx: 140 },
            { wpx: 140 },
            { wpx: 140 },
            { wpx: 140 },
            { wpx: 140 }
          ];

          ws = $scope.excelFormatDotDecimalNumber(ws, 5, "#,##0.00;(#,##0.00)");
          ws = $scope.excelFormatDotDecimalNumber(ws, 6, "#,##0.00;(#,##0.00)");
          ws = $scope.excelFormatDotDecimalNumber(ws, 7, "#,##0.00;(#,##0.00)");
          ws = $scope.excelFormatDotDecimalNumber(ws, 9, "#,##0.00;(#,##0.00)");
          ws = $scope.excelFormatDotDecimalNumber(ws, 10, "#,##0.00;(#,##0.00)");
          ws = $scope.excelFormatDotDecimalNumber(ws, 13, "#,##0.00;(#,##0.00)");
          ws = $scope.excelFormatDotDecimalNumber(ws, 15, "#,##0.00;(#,##0.00)");
          // inserisce il worksheet nel workbook
          wb.Sheets[ws_name] = ws;

          /* bookType can be any supported output type */
          var wopts = { bookType: 'xlsx', bookSST: true, type: 'binary' };

          var wbout = XLSX.write(wb, wopts);

          //costruisce il nome del file
          var date = $scope.formattedDate();

          /* the saveAs call downloads a file on the local machine */
          saveAs(new Blob([$scope.s2ab(wbout)], { type: "application/octet-stream" }), ws_name + date + ".xlsx");
        }, function errorCallback(response) {
          $scope.values = [];
        });
      } else {
        $scope.infoMessage("Selezionare almeno un filtro per la ricerca.");
      }
    };

    $scope.showPercClaim = function (item) {
      ngDialog.open({
        template: 'pages/dsp-statistics/dsp-statistics-perc-claim.html',
        plain: false,
        width: '78%',
        controller: ['$scope', 'ngDialog', '$http', function ($scope, ngDialog, $http) {
          $scope.ctrl = {
            item: item
          };
          $http({
            method: 'GET',
            url: 'rest/percClaim/getResultList/' + item.dsr
          }).then(function successCallback(response) {
            $scope.data = response.data;
          }, function errorCallback(response) {

          });

          $scope.close = function () {
            ngDialog.closeAll(false);
          };

        }]
      });


    };


    $scope.showInfo = function (item) {
      ngDialog.open({
        template: 'pages/dsp-statistics/dsp-statistics-info.html',
        plain: false,
        width: '60%',
        controller: ['$scope', 'ngDialog', '$http', function ($scope, ngDialog, $http) {
          $scope.ctrl = {
            item: item,
            data: null
          };
          $scope.formatNumber = function (number) {
            return twoDecimalRound(number);
          };
          $http({
            method: 'POST',
            url: 'rest/dspStatistics/anticipiAssociati',
            headers: {
              'Content-Type': 'application/json'
            },
            data: {
              dsr: item.dsr,
              dsp: item.dsp,
              country: item.country,
              year: item.year,
              periodNumber: item.periodNumber
            }
          }).then(function successCallback(response) {
            response.data.invoiceStatus = response.data.invoiceStatus.replace("_", " ");
            $scope.data = response.data;
          }, function errorCallback(response) {

          });

          $scope.close = function () {
            ngDialog.closeAll(false);
          };

        }]
      });


    };

    function getCollectionValues(collection) {
      var selectedList = [];
      if (collection) {
        for (var e in collection) {
          selectedList.push(collection[e].id ? collection[e].id : collection[e]);
        }
      }
      return selectedList;
    }

    $scope.filterApply = function (parameters) {
      /*vm.$emit('filterApply', parameters);
      // emette un evento verso il controller esterno per definire l'oggetto con i parametri
      // di ricerca utilizzati per esportare in excel tutti i risultati da un servizio non paginato.
      vm.$emit('exportParamiter', parameters);*/
      // $scope.filterParameters = parameters;
      parameters.first = 0;
      parameters.last = configService.maxRowsPerPage;
      $scope.filterParametersUrl = $httpParamSerializer(parameters);
      $scope.getPagedResults(parameters);
    };


    // cattura l'evnto emesso dal controller figlio e definisce un oggetto nello scope di questo
    // controller con i parametri utilizzati per l'esportazione di tutti i risultati della ricerca senza paginazione
    /*$scope.$on('exportParamiter', function (event, paramiters) {
      $scope.exportParameters = paramiters;
    });*/

    // $scope.onRefresh();


    /*
     * DESCRIZIONE:
     * La funzione serve per generare un baffer dato un oggetto, generalmente si utilizza
     * per permettere la creazione di un file.
     *
     * Parametri:
     * s: l'oggetto che si vuole memorizzare nel buffer
     *
     * return:
     * restituisce il buffer dell'oggetto passato come argomento.
     */
    $scope.s2ab = function (s) {
      var buf = new ArrayBuffer(s.length);
      var view = new Uint8Array(buf);
      for (var i = 0; i !== s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
      return buf;
    };

    /*
     * DESCRIZIONE:
     * La funzione si occupa della formattazione di convertire un valore che arriva al ws: worksheet in formato stringa
     * e lo converte in un formato numerico.
     *
     * Paramtri
     * ws: worksheet
     * colToFormat: indice della colonna sulla quale effettuare la formattazione
     * formatPattern: il pattern utilizzato per effettuare la formattazione
     */
    $scope.excelFormatDotDecimalNumber = function (ws, colToFormat, formatPattern) {
      /*
       * formatto la colonna per il formato decimale
       */
      var range = XLSX.utils.decode_range(ws['!ref']);
      for (var R = range.s.r; R <= range.e.r; ++R) {
        if (ws[XLSX.utils.encode_cell({ c: colToFormat, r: R })] !== undefined) {
          ws[XLSX.utils.encode_cell({ c: colToFormat, r: R })].z = formatPattern;
        }

      }

      return ws
    };

    /*
     * DESCRIZIONE:
     * La funzione si occupa della conversione del tipo di dato di una colonna
     *
     * Paramtri
     * ws: worksheet
     * colToFormat: indice della colonna sulla quale effettuare la conversione di tipo
     * typeToConvert: it nuovo tipo di dato
     */

    $scope.excelChangeColumnType = function (ws, colToFormat, typeToConvert) {
      /*
       * Cambia il tipo di dato della colonna
       */
      var range = XLSX.utils.decode_range(ws['!ref']);
      for (var R = range.s.r; R <= range.e.r; ++R) {
        if (ws[XLSX.utils.encode_cell({ c: colToFormat, r: R })] !== undefined) {
          ws[XLSX.utils.encode_cell({ c: colToFormat, r: R })].t = typeToConvert;
        }
      }

      return ws
    };

    /*
     * DESCRIZIONE:
     * La funzione prende come argomento una data e restituisce la data con una formattazione diversa
     * se la data non viene passata alla funzione il valore di default è la data corrente.
     *
     * PARAMETRI:
     * d Un oggetto di tipo Date
     *
     * RETURN:
     * la funzione restituisce la data sottoforma di stringa custom.
     */
    $scope.formattedDate = function formattedDate(d = new Date) {
      let month = String(d.getMonth() + 1);
      let day = String(d.getDate());
      const year = String(d.getFullYear());
      var hour = d.getHours();
      var minute = d.getMinutes();

      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;

      return `${day}/${month}/${year}/${hour}/${minute}`;
    };

    $scope.openModal = function (item) {
      return ngDialog.open({
        template: 'pages/dsp-statistics/statistiche-dsr-originale.html',
        plain: false,
        width: '60%',
        controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
          var ngDialogId = $scope.ngDialogId;
          $scope.ctrl = {
            ngDialogId: ngDialogId,
            totaliDsr: {
              currency: "",
              totaleValore: 0,
              totaleUtilizzazioni: 0,
              identificatoValore: 0,
              totaliPercentuale: 0,
              valoreClaim: 0,
              utilizzazioniClaim: 0,
              nonIdentificatoValore: 0,
              nonIdentificatoUtilizzazioni: 0,
              importoCcid: 0
            }
          };
          $http({
            method: 'GET',
            url: 'rest/dspStatistics/backclaim/' + item.dsr
          }).then(function successCallback(response) {
            $scope.values = response.data;
            $scope.first = response.data.first;
            $scope.last = response.data.last;
            var dsrOriginale =
              response.data.rows.find(el =>
                el.backclaim > 0
              );
            $scope.ctrl.totaliDsr.totaleValore = dsrOriginale.totValue ? dsrOriginale.totValue : 0;
            $scope.ctrl.totaliDsr.valoreClaim = dsrOriginale.siaeValueIdentified ? dsrOriginale.siaeValueIdentified : 0;
            $scope.ctrl.totaliDsr.importoCcid = dsrOriginale.totalValueCcidCurrency ? dsrOriginale.totalValueCcidCurrency : 0;
            $scope.ctrl.totaliDsr.nonIdentificatoValore = dsrOriginale.valueNotIdentified ? dsrOriginale.valueNotIdentified : 0;
            $scope.ctrl.totaliDsr.utilizzazioniClaim = dsrOriginale.siaeIdentifiedVisualizations ? dsrOriginale.siaeIdentifiedVisualizations : 0;
            $scope.ctrl.totaliDsr.identificatoValore = dsrOriginale.identificatoValorePricing ? dsrOriginale.identificatoValorePricing : 0;
            $scope.ctrl.totaliDsr.nonIdentificatoUtilizzazioni = dsrOriginale.visualizationsNotIdentified ? dsrOriginale.visualizationsNotIdentified : 0;
            $scope.ctrl.totaliDsr.currency = dsrOriginale.currency ? dsrOriginale.currency : 0;
            $scope.ctrl.totaliDsr.totaleUtilizzazioni = dsrOriginale.totUsage ? dsrOriginale.totUsage : 0;
            $scope.ctrl.totaliDsr.totaliPercentuale = dsrOriginale.usageIdentified ? dsrOriginale.usageIdentified : 0;
            response.data.rows.forEach(el => {
              if (el.backclaim === 0) {
                el.percParziale = (((((el.totUsage ? el.totUsage : 0) * (el.usageIdentified ? el.usageIdentified : 0)) / 100)) / (dsrOriginale.totUsage ? dsrOriginale.totUsage : 0)) * 100;
                $scope.ctrl.totaliDsr.totaliPercentuale = $scope.ctrl.totaliDsr.totaliPercentuale + (el.percParziale ? el.percParziale : 0);
                $scope.ctrl.totaliDsr.identificatoValore = $scope.ctrl.totaliDsr.identificatoValore + (el.identificatoValorePricing ? el.identificatoValorePricing : 0);
                $scope.ctrl.totaliDsr.valoreClaim = $scope.ctrl.totaliDsr.valoreClaim + (el.siaeValueIdentified ? el.siaeValueIdentified : 0);
                $scope.ctrl.totaliDsr.utilizzazioniClaim = $scope.ctrl.totaliDsr.utilizzazioniClaim + (el.siaeIdentifiedVisualizations ? el.siaeIdentifiedVisualizations : 0);
                $scope.ctrl.totaliDsr.nonIdentificatoValore = $scope.ctrl.totaliDsr.nonIdentificatoValore + (el.valueNotIdentified ? el.valueNotIdentified : 0);
                $scope.ctrl.totaliDsr.nonIdentificatoUtilizzazioni = $scope.ctrl.totaliDsr.nonIdentificatoUtilizzazioni + (el.visualizationsNotIdentified ? el.visualizationsNotIdentified : 0);
                $scope.ctrl.totaliDsr.importoCcid = $scope.ctrl.totaliDsr.importoCcid + (el.totalValueCcidCurrency ? el.totalValueCcidCurrency : 0);
              }
            });
            console.log(dsrOriginale);
          }, function errorCallback(response) {
            $scope.values = [];
          });

          $scope.cancel = function () {
            ngDialog.close(ngDialogId);
            $route.reload();
          };

        }]
      })
    };

    $scope.drilldown = function (item) {
      if (!item.drilledDown) {
        statsService.drilldown(item.dsr).then(
          function success(result) {
            if (result.data && result.data.length > 0) {
              item.children = result.data;
            }
            item.drilledDown = true;
          },
          function error(error) {
            item.drilledDown = false;
          }
        );
      }
      item.expanded = !item.expanded;
    };


  }]);

codmanApp.service('statsService', ['$http', function ($http) {

  var service = this;

  service.drilldown = function (idDsr) {
    return $http({
      method: 'GET',
      url: 'rest/dspStatistics/drilldown',
      headers: {
        'Content-Type': 'application/json'
      },
      params: {
        idDsr: idDsr
      }
    });

  };
}]);
