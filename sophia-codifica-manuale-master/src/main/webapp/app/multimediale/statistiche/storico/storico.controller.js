/**
 * DspConfigCtrl
 *
 * @path /dspConfig
 */
codmanApp.controller('storicoStatisticheCtrl', ['$scope', '$route', 'ngDialog', '$routeParams', '$location', '$http', 'configService', 'storico', 'MultimedialeService',
  function ($scope, $route, ngDialog, $routeParams, $location, $http, configService, storico, MultimedialeService) {

    $scope.filterParameters = $routeParams;

    $scope.storico = storico;

    $scope.onAzzera = function () {
      $scope.ctrl.execute = false;
      $scope.ctrl.first = 0;
      $scope.ctrl.last = $scope.ctrl.maxrows;
    };

    $scope.showInfo = function (item) {
      ngDialog.open({
        template: 'pages/dsp-statistics/dsp-statistics-info.html',
        plain: false,
        width: '60%',
        controller: ['$scope', 'ngDialog', '$http', function ($scope, ngDialog, $http) {
          $scope.ctrl = {
            item: item,
            data: null
          };
          $scope.formatNumber = function (number) {
            return twoDecimalRound(number);
          };
          $http({
            method: 'POST',
            url: 'rest/dspStatistics/anticipiAssociati',
            headers: {
              'Content-Type': 'application/json'
            },
            data: {
              dsr: item.dsr,
              dsp: item.dsp,
              country: item.country,
              year: item.year,
              periodNumber: item.periodNumber
            }
          }).then(function successCallback(response) {
            response.data.invoiceStatus = response.data.invoiceStatus.replace("_", " ");
            $scope.ctrl.data = response.data;
          });

          $scope.close = function () {
            ngDialog.closeAll(false);
          };

        }]
      });
    };
  }]);