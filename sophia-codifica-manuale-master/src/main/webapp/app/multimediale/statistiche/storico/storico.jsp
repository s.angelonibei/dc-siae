<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../../../pages/navbar.jsp" %>

<div class="bodyContents" >

    <div class="mainContainer row-fluid" >

        <div id="companyLogo" class="navbar commonActionsContainer noprint">
            <div class="actionsContainer row-fluid">
                <div class="span2">
                    <%--<span class="companyLogo"><img src="images/siae_logo_siaetrasparente.jpg" title="SIAE" alt="SIAE">&nbsp;</span>--%>
                    <span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE" alt="SIAE">&nbsp;</span>
                </div>
                <span class="btn-toolbar span4">
					<div class="pageNumbers alignTop pull-right" style="text-align: center;">
		 				<span class="pageNumbersText"><strong>Storico DSR</strong></span>
					</div>
				</span>
            </div>
        </div>

        <div class="contentsDiv marginLeftZero" id="rightPanel" style="min-height: 80px;">
            <div class="listViewPageDiv">

                <!-- actions and pagination -->
                <div class="listViewTopMenuDiv noprint">
                    <div class="listViewActionsDiv row-fluid">

						<span class="btn-toolbar span4">
							<span class="btn-group">
                                <%--<button class="btn addButton" data-ng-click="showMoveToNext($event,ctrl.unidentifiedSong)">
                                    <span class="glyphicon glyphicon-forward"></span>&nbsp;&nbsp;<strong>Successiva</strong>
                                </button>--%>
                            </span>
						</span>

                        <span class="btn-toolbar span4">
							<span class="customFilterMainSpan btn-group" style="text-align: center;">
								<div class="pageNumbers alignTop ">
                                    <%--<span class="pageNumbersText"><strong>Titolo</strong></span>--%>
                                </div>
						 	</span>
						</span>

                        <span class="span4 btn-toolbar">
							<div class="listViewActions pull-right">
							<span class="btn-group">
								<!-- 		<button class="btn addButton" data-ng-click="showNewDSPConfig($event)">
										<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;<strong>Nuovo</strong>
									</button>  -->
	 							</span>
	 				 		</div>
					 		<div class="clearfix"></div>
					 	</span>

                    </div>
                </div>

                <!-- actions and pagination -->
                <div class="listViewTopMenuDiv noprint">
                    <div class="listViewActionsDiv row-fluid">
						<span class="btn-toolbar span4">
							<span class="btn-group">

 							</span>
						</span>
                        <span class="btn-toolbar span4">
							<span class="customFilterMainSpan btn-group" style="text-align: center;">
								<div class="pageNumbers alignTop ">
					 				<span class="pageNumbersText"><strong><!-- Titolo --></strong></span>
								</div>
						 	</span>
						</span>
                        <span class="span4 btn-toolbar">
							<div class="listViewActions pull-right">

					 			<div class="pageNumbers alignTop ">
						 			<span data-ng-show="ctrl.values.hasPrev || ctrl.values.hasNext">
						 				<span class="pageNumbersText" style="padding-right:5px">
						 					Record da <strong>{{ctrl.values.first+1}}</strong> a <strong>{{ctrl.values.last}}</strong>
						 				</span>
					 					<button title="Precedente" class="btn" id="listViewPreviousPageButton" type="button"
                                                data-ng-click="navigateToPreviousPage()"
                                                data-ng-show="ctrl.values.hasPrev">
					 						<span class="glyphicon glyphicon-chevron-left"></span>
					 					</button>
					 					<button title="Successiva" class="btn" id="listViewNextPageButton" type="button"
                                                data-ng-click="navigateToNextPage()"
                                                data-ng-show="ctrl.values.hasNext">
					 						<span class="glyphicon glyphicon-chevron-right"></span>
					 					</button>
					 				</span>
						 		</div>
					 		</div>
					 		<div class="clearfix"></div>
					 	</span>
                    </div>
                </div>


                <!-- qui va il filtro-->
                <!-- table -->
                <div class="listViewContentDiv" id="listViewContents">

                    <div class="contents-topscroll noprint">
                        <div class="topscroll-div" style="width: 95%">&nbsp;</div>
                    </div>

                    <div class="listViewEntriesDiv contents-bottomscroll">
                        <div class="bottomscroll-div" style="width: 95%; min-height: 80px">

                            <table class="table table-bordered listViewEntriesTable">
                                <thead>
                                <tr class="listViewHeaders">
                                    <th ><a href="" class="listViewHeaderValues" >DSP</a></th>
                                    <th  style="text-align: center;" ><a href="" class="listViewHeaderValues">DSR</a></th>
                                    <th ><a href="" class="listViewHeaderValues" >Periodo</a></th>
                                    <th ><a href="" class="listViewHeaderValues" >Territorio</a></th>
                                    <th ><a href="" class="listViewHeaderValues" >Tipo di utilizzo</a></th>
                                    <th ><a href="" class="listViewHeaderValues" >Offerta commerciale</a></th>
                                    <th style="text-align: right;"><a href="" class="listViewHeaderValues" >Totale Valore</a></th>
                                    <th style="text-align: right;"><a href="" class="listViewHeaderValues" >Totale utilizzazioni</a></th>
                                    <th style="text-align: right;"><a href="" class="listViewHeaderValues" >Identificato valore</a></th>
                                    <th style="text-align: right;"><a href="" class="listViewHeaderValues" >% Identificato utilizzazioni</a></th>
                                    <th style="text-align: right;"><a href="" class="listViewHeaderValues" >Valore claim SIAE (&euro;)</a></th>
                                    <th style="text-align: right;"><a href="" class="listViewHeaderValues" >Utilizzazioni claim SIAE</a></th>
                                    <th style="text-align: right;"><a href="" class="listViewHeaderValues" >Non identificato valore</a></th>
                                    <th style="text-align: right;"><a href="" class="listViewHeaderValues" >Non identificato visualizzazioni</a></th>
                                    <th style="text-align: right;"><a href="" class="listViewHeaderValues" >Importo CCID</a></th>
                                    <th ></th>
                                </tr>
                                </thead>
                                <tbody >

                                <tr class="listViewEntries" data-ng-repeat="item in storico.rows | orderBy:'dsp' track by $index">
                                    <td class="listViewEntryValue">{{item.dsp}}</td>
                                    <td class="listViewEntryValue">{{item.dsr}}</td> 
                                    <td class="listViewEntryValue" data-ng-bind="item.period"></td>
                                    <td class="listViewEntryValue" data-ng-bind="item.country"></td>
                                    <td class="listViewEntryValue" data-ng-bind="item.utilizationType"></td>
                                    <td class="listViewEntryValue" data-ng-bind="item.commercialOffer"></td>
                                    <td class="listViewEntryValue" data-ng-bind="item.totValue | currency: item.currency : 2" style="text-align: right;"></td>
                                    <td class="listViewEntryValue" data-ng-bind="item.totUsage | number: 0" style="text-align: right;"></td>
                                    <td class="listViewEntryValue" data-ng-bind="item.identificatoValorePricing| number: 2" style="text-align: right;"></td>
                                    <td class="listViewEntryValue" data-ng-bind="item.usageIdentified| currency: '%' : 2" style="text-align: right;"></td>
                                    <td class="listViewEntryValue" data-ng-bind="item.siaeValueIdentified| number: 2" style="text-align: right;"> </td>
                                    <td class="listViewEntryValue" data-ng-bind="item.siaeIdentifiedVisualizations| number: 2" style="text-align: right;"></td>
                                    <td class="listViewEntryValue" data-ng-bind="item.valueNotIdentified| number: 2" style="text-align: right;"></td>
                                    <td class="listViewEntryValue" data-ng-bind="item.visualizationsNotIdentified| number: 0" style="text-align: right;"></td>
                                    <td class="listViewEntryValue" data-ng-bind="item.totalValueCcidCurrency| currency: item.ccidCurrency : 2" style="text-align: right;"></td>
                                    <td nowrap class="medium">
                                        <div class="actions pull-right">
                                            <!--  -->
                                            <span class="actionImages">
						 				    <a data-ng-click="showInfo(item)"><i title="Dettaglio" class="glyphicon glyphicon-info-sign alignMiddle"></i></a>&nbsp;
						 					<span data-ng-if="item.ccidS3Path"><a download="{{item.ccidS3Path}}" data-ng-href="downloadCCID?id={{item.ccidS3Id}}"><i title={{item.ccidS3Path}} class="glyphicon  glyphicon-cloud-download alignMiddle"></i></a>&nbsp;</span>

                                                <!--	<a data-ng-click="showDeleteConfig($event,item)"><i title="Elimina" class="glyphicon glyphicon-trash alignMiddle"></i></a>&nbsp; -->
						 				</span>
                                            <!--  -->
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <br>
                            <a class="btn addButton"  data-ng-href="#!/dspStatistics?{{filterParameters | urlEncodeAngular}}"> Back
                            </a>

                        </div>
                    </div>
                </div>
                <!-- /table -->
            </div>
        </div>
    </div>
    <%--<pre>{{ctrl.properties}}</pre>--%>
</div>