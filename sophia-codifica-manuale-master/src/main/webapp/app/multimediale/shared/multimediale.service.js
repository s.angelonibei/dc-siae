(function () {
  'use strict';
  angular
    .module('codmanApp')
    .service('MultimedialeService', MultimedialeService);

  MultimedialeService.$inject = ['$http', 'UtilService', 'configService', '$location'];

  function MultimedialeService($http, UtilService, configService, $location, $q) {
    var vm = this;
    vm.msInvoiceUrl = `${$location.protocol()}://${$location.host()}:${$location.port()}/${configService.msInvoiceApiPath}`;
    vm.getStoricoStatistiche = getStoricoStatistiche;

    function getStoricoStatistiche(idDsr, first, last) {
      return $http({
        method: 'GET',
        url: 'rest/dspStatistics/storico',
        params: {
          first: first ? first : 0,
          last: last ? last : 50,
          dsr: idDsr
        }
      }).then(function (response) {
        return response.data;
      }).catch(function (error) {
        UtilService.handleHttpRejection(error);
        $q.reject();
      });
    }
  }
})();
