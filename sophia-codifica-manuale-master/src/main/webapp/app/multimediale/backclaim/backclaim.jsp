<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="../../../pages/navbar.jsp" %>

<div class="row-fluid">
  <div id="companyLogo" class="commonActionsContainer noprint">
    <div class="actionsContainer row-fluid">
      <div class="span2">
        <span class="companyLogo"><img src="images/logo_siae.jpg" title="SIAE"
                                       alt="SIAE">&nbsp;</span>
      </div>
      <span class="btn-toolbar span4">
					<div class="pageNumbers alignTop pull-right"
               style="text-align: center;">
		 				<strong>Back-claim</strong>
					</div>
				</span>
    </div>
  </div>

  <div class="contentsDiv">
    <div>
      <filter-dsp-country-period-offer
          period-limits="vm.periodLimits"
          dsp-countries="vm.dspCountries"
          on-filter-apply="vm.getCcid(parameters,0)"
          commercial-offers="vm.commercialOffers">
      </filter-dsp-country-period-offer>

      <div class="listViewTopMenuDiv noprint"
           ng-if="vm.ccids && vm.ccids.rows && vm.ccids && vm.ccids.rows.length > 0">
        <div class="listViewActionsDiv row-fluid">
          <span class="btn-toolbar span4"> <span
              class="btn-group"></span></span>
          <span class="btn-toolbar span4">
                  <span class="customFilterMainSpan btn-group"
                        style="text-align: center;">
                     <div class="pageNumbers alignTop "><span
                         class="pageNumbersText"><strong> </strong></span></div>
                  </span>
               </span>
          <span class="span4 btn-toolbar">
                  <div class="listViewActions pull-right">
                     <div class="pageNumbers alignTop "><span> <span
                         class="pageNumbersText" style="padding-right: 5px">
                       Record da <strong>{{vm.ccids.currentPage*vm.ccids.maxrows+1}}</strong> a
                       <strong>{{vm.ccids.currentPage*vm.ccids.maxrows + vm.ccids.rows.length}}</strong>
                     </span>
                       <button title="Precedente" class="btn"
                               type="button"
                               ng-click="vm.previousPage()"
                               ng-if="vm.ccids.hasPrev"><span
                           class="glyphicon glyphicon-chevron-left"></span></button>
                       <button title="Successiva" class="btn"
                               type="button"
                               ng-click="vm.nextPage()"
                               ng-if="vm.ccids.hasNext"><span
                           class="glyphicon glyphicon-chevron-right"></span></button></span></div>
        </div>
        <div class="clearfix"></div>
        </span>
        </div>
      </div>

      <table class="table table-bordered listViewEntriesTable"
             ng-if="vm.ccids && vm.ccids.rows && vm.ccids.rows.length > 0">
        <thead>
        <tr class="listViewHeaders">
          <th nowrap ng-click="vm.sort('idDsr')" class="listViewHeaderValues">DSR
            <span
                class="glyphicon glyphicon-sort{{vm.filter.sortBy==='idDsr' ? (vm.filter.asc ? '-by-alphabet' : '-by-alphabet-alt') : '' }}"></span>
          </th>
          <th nowrap ng-click="vm.sort('periodString')" class="listViewHeaderValues">
            Periodo
            <span
                class="glyphicon glyphicon-sort{{vm.filter.sortBy==='periodString' ? (vm.filter.asc ? '-by-order' : '-by-order-alt') : '' }}"></span>
          </th>
          <th nowrap ng-click="vm.sort('country')" class="listViewHeaderValues">
            Territorio
            <span
                class="glyphicon glyphicon-sort{{vm.filter.sortBy==='country' ? (vm.filter.asc ? '-by-alphabet' : '-by-alphabet-alt') : '' }}"></span>
          </th>
          <th ng-click="vm.sort('utilizationType')"
              class="listViewHeaderValues">
            Tipo
            di utilizzo
            <span
                class="glyphicon glyphicon-sort{{vm.filter.sortBy==='utilizationType' ? (vm.filter.asc ? '-by-alphabet' : '-by-alphabet-alt') : '' }}"></span>
          </th>
          <th ng-click="vm.sort('commercialOffer')"
              class="listViewHeaderValues">
            Offerta commerciale
            <span
                class="glyphicon glyphicon-sort{{vm.filter.sortBy==='commercialOffer' ? (vm.filter.asc ? '-by-alphabet' : '-by-alphabet-alt') : '' }}"></span>
          </th>
          <th nowrap class="medium" style="text-align: right;"
              ng-click="vm.sort('totalValue')" class="listViewHeaderValues">
            Valore
            (&euro;)
            <span
                class="glyphicon glyphicon-sort{{vm.filter.sortBy==='totalValue' ? (vm.filter.asc ? '-by-order' : '-by-order-alt') : '' }}"></span>
          </th>
          <%--<th style="text-align: right;" ng-click="vm.sort('invoiceAmount')"--%>
              <%--class="listViewHeaderValues">Valore fatturabile (&euro;)--%>
            <%--<span--%>
                <%--class="glyphicon glyphicon-sort{{vm.filter.sortBy==='invoiceAmount' ? (vm.filter.asc ? '-by-order' : '-by-order-alt') : '' }}"></span>--%>
          <%--</th>--%>
          <%--<th style="text-align: right;" ng-click="vm.sort('valoreResiduo')"--%>
              <%--class="listViewHeaderValues">Valore fatturabile residuo (&euro;)--%>
            <%--<span--%>
                <%--class="glyphicon glyphicon-sort{{vm.filter.sortBy==='valoreResiduo' ? (vm.filter.asc ? '-by-order' : '-by-order-alt') : '' }}"></span>--%>
          <%--</th>--%>
          <%--<th style="text-align: right" class="listViewHeaderValues"--%>
              <%--ng-click="vm.sort('invoiceAmountNew')"--%>
              <%--class="listViewHeaderValues">--%>
            <%--Valore Aggiornato (&euro;) <span--%>
              <%--class="glyphicon glyphicon-sort{{vm.filter.sortBy==='invoiceAmountNew' ? (vm.filter.asc ? '-by-order' : '-by-order-alt') : '' }}"></span>--%>
          <%--</th>--%>
          <th ng-click="vm.sort('backclaim')" class="listViewHeaderValues" style="text-align: right;">
            Backclaim Eseguiti
            <span
                class="glyphicon glyphicon-sort{{vm.filter.sortBy==='backclaim' ? (vm.filter.asc ? '-by-order' : '-by-order-alt') : '' }}"></span>
          </th>
          <th nowrap ng-click="vm.sort('idBackclaimInProgress')"
              class="listViewHeaderValues">
            BackClaim
            <span
                class="glyphicon glyphicon-sort{{vm.filter.sortBy==='idBackclaimInProgress' ? (vm.filter.asc ? '-by-order' : '-by-order-alt') : '' }}"></span>
          </th>
        </tr>
        </thead>
        <tbody>
        <tr class="listViewEntries" ng-repeat="item in vm.ccids.rows">
          <td nowrap class="medium listViewEntryValue" ng-bind="item.idDsr"></td>
          <td class="listViewEntryValue" ng-bind="item.periodString"></td>
          <td class="listViewEntryValue" ng-bind="item.country"></td>
          <td class="listViewEntryValue" ng-bind="item.utilizationType"></td>
          <td class="listViewEntryValue" ng-bind="item.commercialOffer"></td>
          <td nowrap class="medium listViewEntryValue"
              style="text-align: right;" ng-bind="item.totalValue | number: 2">
          </td>
          <%--<td class="listViewEntryValue " style="text-align: right;"--%>
              <%--ng-bind="item.invoiceAmount | number: 2">--%>
          <%--</td>--%>
          <%--<td class="listViewEntryValue " style="text-align: right;"--%>
              <%--ng-bind="item.valoreResiduo | number: 2">--%>
          <%--</td>--%>
          <%--<td class="listViewEntryValue " style="text-align: right;"--%>
              <%--ng-bind="(item.invoiceAmountNew ? item.invoiceAmountNew : 0) | number: 2">--%>
          <%--</td>--%>
          <td class="listViewEntryValue " style="text-align: right;"
              ng-bind="item.backclaim"></td>
          <td style="text-align: center">
            <a ng-if="item.idBackclaimInProgress >= 2"
               href="javascript:void(0)"
               data-toggle="tooltip"
               data-placement="top"
               title="Annulla"
               ng-click="vm.voidBackclaim(item.idDsr, $index)">
              <i class="glyphicon glyphicon-remove"></i>
            </a>
            <a ng-if="!item.bcType"
               href="javascript:void(0)"
               data-toggle="tooltip"
               data-placement="top"
               title="{{item.description}}"
               ng-click="vm.startBackclaim(item.idDsr,'A', $index)">Tipo A
              <i class="glyphicon glyphicon-{{item.idBackclaimInProgress === 0 ||
                item.idBackclaimInProgress === 2 ? 'play' :
                item.idBackclaimInProgress === 1 ? 'time' :
                'exclamation-sign'}}"
                 style="{{ (item.idBackclaimInProgress === 0 || item.idBackclaimInProgress === 2 ) ? '' : 'cursor: not-allowed'}}"></i>
            </a>
            <a ng-if="!item.bcType"
               href="javascript:void(0)"
               data-toggle="tooltip"
               data-placement="top"
               title="{{item.description}}"
               ng-click="vm.startBackclaim(item.idDsr,'B',  $index)">Tipo B
              <i class="glyphicon glyphicon-{{item.idBackclaimInProgress === 0 ||
                item.idBackclaimInProgress === 2 ? 'play' :
                item.idBackclaimInProgress === 1 ? 'time' :
                'exclamation-sign'}}"
                 style="{{ (item.idBackclaimInProgress === 0 || item.idBackclaimInProgress === 2 ) ? '' : 'cursor: not-allowed'}}"></i>
            </a>
            <a ng-if="!item.bcType"
               href="javascript:void(0)"
               data-toggle="tooltip"
               data-placement="top"
               title="{{item.description}}"
               ng-click="vm.startBackclaim(item.idDsr,'C',  $index)">Tipo C
              <i class="glyphicon glyphicon-{{item.idBackclaimInProgress === 0 ||
                item.idBackclaimInProgress === 2 ? 'play' :
                item.idBackclaimInProgress === 1 ? 'time' :
                'exclamation-sign'}}"
                 style="{{ (item.idBackclaimInProgress === 0 || item.idBackclaimInProgress === 2 ) ? '' : 'cursor: not-allowed'}}"></i>
            </a>


            <a ng-if="item.bcType"
               href="javascript:void(0)"
               data-toggle="tooltip"
               data-placement="top"
               title="{{item.description}}"
               ng-click="vm.startBackclaim(item.idDsr,item.bcType,  $index)">Tipo {{item.bcType}}
              <i class="glyphicon glyphicon-{{item.idBackclaimInProgress === 0 ||
                item.idBackclaimInProgress === 2 ? 'play' :
                item.idBackclaimInProgress === 1 ? 'time' :
                'exclamation-sign'}}"
                 style="{{ (item.idBackclaimInProgress === 0 || item.idBackclaimInProgress === 2 ) ? '' : 'cursor: not-allowed'}}"></i>
            </a>
          </td>
        </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>