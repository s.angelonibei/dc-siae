(function () {
  'use strict';
  angular.module('codmanApp')
    .controller('BackclaimCtrl', BackclaimCtrl);

  BackclaimCtrl.$inject = ['UtilService', 'periodLimits', 'dspCountries', 'commercialOffers', 'InvoiceService'];

  function BackclaimCtrl(UtilService, periodLimits, dspCountries, commercialOffers, InvoiceService) {
    var vm = this;
    vm.periodLimits = angular.copy(periodLimits);
    vm.dspCountries = angular.copy(dspCountries);
    vm.getCcid = getCcid;
    vm.currentPage = 0;
    vm.startBackclaim = startBackclaim;
    vm.nextPage = nextPage;
    vm.previousPage = previousPage;
    vm.voidBackclaim = voidBackclaim;
    vm.sort = sort;
    vm.commercialOffers = angular.copy(commercialOffers);

    function getCcid(params, page) {
      vm.filter = params;
      vm.filter.monthFrom = vm.filter.periodFrom ? dayjs(vm.filter.periodFrom).month() + 1 : void 0;
      vm.filter.yearFrom = vm.filter.periodFrom ? dayjs(vm.filter.periodFrom).year() : void 0;
      vm.filter.monthTo = vm.filter.periodTo ? dayjs(vm.filter.periodTo).month() + 1 : void 0;
      vm.filter.yearTo = vm.filter.periodTo ? dayjs(vm.filter.periodTo).year() : void 0;
      vm.filter.isBackclaim = true;
      vm.filter.fatturato = void 0;

      InvoiceService.getCcid(vm.filter, page)
        .then(function (data) {
        vm.ccids = data;
      });
    }

    function sort(field) {
      vm.filter.sortBy = field;
      vm.filter.asc = !vm.filter.asc;
      getCcid(vm.filter, vm.ccids.currentPage);
    }
    function nextPage() {
      getCcid(
        vm.filter,
        vm.ccids.currentPage + 1
      );
    }

    function previousPage() {
      getCcid(
        vm.filter,
        vm.ccids.currentPage - 1
      );
    }

    function startBackclaim(idDsr,bcType, index) {
      var progress = vm.ccids.rows[index].idBackclaimInProgress;
      if (progress === 0 || progress === 2) {
        UtilService.showConfirm('Inizia ' + (progress === 2 ? 'nuovo' : '') + ' Backlaim per CCID ' + idDsr,
          'Sei sicuro di voler richiedere una ' + (progress === 2 ? 'nuova' : '') + ' procedura di Backclaim di tipo '+ bcType +' per il CCID ' + idDsr)
          .then(function (data) {
            // var sse = InvoiceService.startBackclaim(idDsr,bcType);
            InvoiceService.startBackclaim(idDsr,bcType);
            setTimeout(function(){ getCcid(vm.filter, vm.ccids.currentPage); }, 3000);

            // sse.onmessage = function (e) {
            //   console.log(e.data);
            // };
            // sse.onopen = function (e) {
            //   return console.log('open');
            // };
            // sse.onerror = function (e) {
            //   if (e.readyState === EventSource.CLOSED) {
            //     sse.close();
            //     console.log('close');
            //   } else {
            //     console.log(e);
            //   }
            // };
            //
            // sse.addEventListener('backclaimInProgress', function (event) {
            //   if (parseInt(JSON.parse(event.data).idBackclaimInProgress) >= 2) {
            //     sse.close();
            //   }
            //   getCcid(vm.filter, vm.ccids.currentPage);
            // }, false);
          });
      }

    }

    function voidBackclaim(idDsr, index) {
      UtilService.showConfirm('Annulla Backclaim ' + idDsr,
        'Sei sicuro di voler annullare la procedura di Backclaim conclusa per il CCID ' + idDsr)
        .then(function (data) {
          InvoiceService.voidBackclaim(idDsr).then(function (data) {
            UtilService.showWarning('Backclaim ' + idDsr + ' Annullato',
              'Il backclaim è stato correttamente annullato');
            getCcid(vm.filter, vm.ccids.currentPage);
          }).catch(function (error) {
            UtilService.handleHttpRejection(error);
          });
        });

    }
  }

})();
