(function () {
  'use strict';

  function filterDspCountryPeriodOfferCtrl() {
    var vm = this;
    vm.filterCommercialOffers = filterCommercialOffers;

    vm.apply = apply;
    vm.reset = reset;
    vm.updateDateValue = updateDateValue;
    vm.$onInit = $onInit;
    vm.filter = { dsp: [] };

    function $onInit() {
      vm.backupCommercialOffers = angular.copy(vm.commercialOffers);
      /*
        vm.dspCountries.dsps = vm.dspCountries.map(function (x) {
          return x.dspList[0];
        });
  
        vm.dspCountries.countries = vm.dspCountries.map(function (x) {
          return x.dspList[0];
        });*/
    }

    function reset($event) {
      $event.preventDefault();
      vm.filter = { dsp: [] };
    }

    function updateDateValue(ngModel, value) {
      vm.filter = { ...vm.filter, [ngModel]: value };
    }

    function filterCommercialOffers() {
      vm.onfilterCommercialOffers({
        idAnagClient: vm.filter.client.idAnagClient,
        commerialOffers: vm.dspCountries
      });
    }

    function apply() {
      vm.onFilterApply({ parameters: vm.filter });
    }

    vm.multiselectSettings = {
      scrollableHeight: '200px',
      scrollable: false,
      enableSearch: true,
      displayProp: 'name',
      idProp: 'idCountry',
      smartButtonMaxItems: 2
    };

    vm.multiselectDsp = {
      scrollableHeight: '200px',
      scrollable: false,
      enableSearch: true,
      displayProp: 'name',
      idProp: 'idDsp',
      smartButtonMaxItems: 2
    };

    vm.multiselectSettingsCommercialOffers = {
      scrollableHeight: '200px',
      scrollable: false,
      enableSearch: true,
      displayProp: 'offering',
      idProp: 'offering',
      smartButtonMaxItems: 2
    };

    vm.events = {
      dsp: {
        onItemSelect: onItemSelectDsp,
        onItemDeselect: onItemDeselectDsp,
        onDeselectAll: function () { 
        vm.commercialOffers = angular.copy(vm.backupCommercialOffers);
        vm.filter = {dsp:[]}; 
        },
        onSelectAll: function(){
          vm.filter = {dsp:vm.dspCountries.dsp.map(dsp => { return {'id': dsp.idDsp}})}; 
        }
      }
    };

    function onItemSelectDsp(item) {
      vm.filter.commercialOffer = [];
      vm.commercialOffers = vm.backupCommercialOffers.filter(function (e) {
        for (let i = 0; i < vm.filter.dsp.length; i++) {
          if (e.idDSP === vm.filter.dsp[i].id)
            return true;
        }
        return false;
      });

    }

    function onItemDeselectDsp(item) {
      vm.filter.commercialOffer = [];
      if (vm.filter.dsp.length === 0) {
        vm.commercialOffers = angular.copy(vm.backupCommercialOffers);
      } else
        onItemSelectDsp(item);
    }
  }

  angular
    .module('codmanApp')
    .component('filterDspCountryPeriodOffer', {
      templateUrl: 'app/multimediale/components/filter-dsp-country-period-offer.html',
      controller: filterDspCountryPeriodOfferCtrl,
      controllerAs: 'vm',
      bindings: {
        periodLimits: '<',
        dspCountries: '<',
        onFilterApply: '&',
        commercialOffers: '<',
      }
    });
})();