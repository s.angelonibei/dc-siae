(function () {
  'use strict';

  function filterStatisticsCtrl($route, ngDialog, $routeParams, $location, $http) {
    var vm = this;
    vm.$onInit = $onInit;
    vm.apply = apply;

    vm.ctrl = {
      hideForm: false,
      dspList: [],
      utilizationList: [],
      commercialOffersList: [],
      countryList: [],
      months: [
        { 'id': '1', 'name': 'Gennaio' },
        { 'id': '2', 'name': 'Febbraio' },
        { 'id': '3', 'name': 'Marzo' },
        { 'id': '4', 'name': 'Aprile' },
        { 'id': '5', 'name': 'Maggio' },
        { 'id': '6', 'name': 'Giugno' },
        { 'id': '7', 'name': 'Luglio' },
        { 'id': '8', 'name': 'Agosto' },
        { 'id': '9', 'name': 'Settembre' },
        { 'id': '10', 'name': 'Ottobre' },
        { 'id': '11', 'name': 'Novembre' },
        { 'id': '12', 'name': 'Dicembre' }
      ]
    };


    // Funzione principale richiamata al caricamento della pagina
    function $onInit() {
      vm.hideBackClaimSelect = vm.hideBackClaimSelect || false;
      vm.hideClientSelect = vm.hideClientSelect || false;
      vm.showZipName = vm.showZipName || false;

      vm.backup = angular.copy(vm.dspsUtilizationsCommercialOffersCountries);

      vm.selectedDspModel = vm.filterParameters['selectedDspModel'] ? vm.filterParameters['selectedDspModel'] : [];
      vm.selectedCountryModel = vm.filterParameters['selectedCountryModel'] ? vm.filterParameters['selectedCountryModel'] : [];
      vm.selectedUtilizationModel = vm.filterParameters['selectedUtilizationModel'] ? vm.filterParameters['selectedUtilizationModel'] : [];
      vm.selectedOfferModel = vm.filterParameters['selectedOfferModel'] ? vm.filterParameters['selectedOfferModel'] : [];
      vm.selectedPeriodFrom = vm.filterParameters['selectedPeriodFrom'] || vm.dateFrom;
      vm.selectedPeriodTo = vm.filterParameters['selectedPeriodTo'] || vm.dateTo;
      vm.selectedProcessingStatuses = vm.filterParameters['selectedProcessingStatuses'] ? vm.filterParameters['selectedProcessingStatuses'] : [];

      vm.multiselectSettings = {
        scrollableHeight: '200px',
        scrollable: false,
        enableSearch: true,
        displayProp: 'description',
        smartButtonMaxItems: 2
      };

      vm.dspSettings = angular.copy(vm.multiselectSettings);
      vm.countrySettings = angular.copy(vm.multiselectSettings);
      vm.utilizationSettings = angular.copy(vm.multiselectSettings);
      vm.commercialOfferSettings = angular.copy(vm.multiselectSettings);
      vm.processingStatusesSettings = angular.copy(vm.multiselectSettings);

      vm.dspSettings.idProp = 'idDsp';
      vm.countrySettings.idProp = 'idCountry';
      vm.countrySettings.displayProp = 'name';
      vm.utilizationSettings.idProp = 'idUtilizationType';
      vm.utilizationSettings.displayProp = 'name';
      vm.commercialOfferSettings.idProp = 'offering';
      vm.commercialOfferSettings.displayProp = 'offering';
      vm.processingStatusesSettings.idProp = 'id';

      if (vm.selectedClient !== undefined)
        onItemSelectClient(vm.selectedClient);

    }

    function apply() {
      vm.onFilterApply({ parameters: vm });
    }

    vm.events = {
      dsp: {
        onItemSelect: onItemSelectDsp,
        onItemDeselect: onItemDeselectDsp,
        onDeselectAll: function () {
          if (vm.hideClientSelect || !vm.selectedClient) {
            vm.dspsUtilizationsCommercialOffersCountries.commercialOffers = angular.copy(vm.backup.commercialOffers);
            vm.dspsUtilizationsCommercialOffersCountries.utilizations = angular.copy(vm.backup.utilizations);
          } else {
            vm.dspsUtilizationsCommercialOffersCountries.utilizations = angular.copy(vm.backup.utilizations).filter(
              utilizationType => vm.selectedClient.dspList.findIndex( el => utilizationType.idDsp.includes(el.idDsp)) != -1
            );
            vm.dspsUtilizationsCommercialOffersCountries.commercialOffers = angular.copy(vm.backup.commercialOffers).filter(
              commercialOffer => vm.selectedClient.dspList.findIndex( el => commercialOffer.idDSP == el.idDsp) != -1
            );
          }
        }
      },
      utilizationType: {
        onItemSelect: onItemSelectDsp,
        onItemDeselect: onItemDeselectDsp,
        onDeselectAll: function () {
          if (vm.hideClientSelect || !vm.selectedClient) {
            vm.dspsUtilizationsCommercialOffersCountries.utilizations = angular.copy(vm.backup.utilizations);
            if(vm.selectedDspModel && vm.selectedDspModel.length> 0){
              vm.dspsUtilizationsCommercialOffersCountries.commercialOffers = vm.backup.commercialOffers.filter(function (e) {
                for (let i = 0; i < vm.selectedDspModel.length; i++) {
                  if (e.idDSP === vm.selectedDspModel[i].id)
                    return true;
                }
                return false;                
              });
            }else {
              vm.dspsUtilizationsCommercialOffersCountries.commercialOffers = angular.copy(vm.backup.commercialOffers);
            }
          } else {
            vm.dspsUtilizationsCommercialOffersCountries.utilizations = angular.copy(vm.backup.utilizations).filter(
              utilizationType => vm.selectedClient.dspList.findIndex( el => utilizationType.idDsp.includes(el.idDsp)) != -1
            );
            vm.dspsUtilizationsCommercialOffersCountries.commercialOffers = angular.copy(vm.backup.commercialOffers).filter(
              commercialOffer => vm.selectedClient.dspList.findIndex( el => commercialOffer.idDSP == el.idDsp) != -1
            );
            if(vm.selectedDspModel && vm.selectedDspModel.length> 0){
              vm.dspsUtilizationsCommercialOffersCountries.commercialOffers = vm.dspsUtilizationsCommercialOffersCountries.commercialOffers.filter(function (e) {
                for (let i = 0; i < vm.selectedDspModel.length; i++) {
                  if (e.idDSP === vm.selectedDspModel[i].id)
                    return true;
                }
                return false;                
              });
            }
          }
        }
      }
    };

    vm.onItemSelectClient = onItemSelectClient;
    function onItemSelectClient(item) {
      vm.selectedDspModel = [];
      vm.selectedCountryModel = [];
      vm.selectedUtilizationModel = [];
      vm.selectedOfferModel = [];
      const idDsps = item.dspList.map(e => e.idDsp);
      vm.dspsUtilizationsCommercialOffersCountries.dsp = vm.backup.dsp.filter(function (e) {
        for (let i = 0; i < idDsps.length; i++) {
          if (e.idDsp === idDsps[i])
            return true;
        }
        return false;
      });
      vm.dspsUtilizationsCommercialOffersCountries.utilizations = vm.backup.utilizations.filter(function (e) {
        for (let i = 0; i < idDsps.length; i++) {
          if (e.idDsp.includes(idDsps[i]))
            return true;
        }
        return false;
      });
      vm.dspsUtilizationsCommercialOffersCountries.commercialOffers = vm.backup.commercialOffers.filter(function (e) {
        for (let i = 0; i < idDsps.length; i++) {
          if (e.idDSP === idDsps[i])
            return true;
        }
        return false;
      });
    }


    function onItemSelectDsp(item) {
      if (vm.selectedDspModel && vm.selectedDspModel.length !== 0) {
        vm.dspsUtilizationsCommercialOffersCountries.utilizations = vm.backup.utilizations.filter(function (e) {
          for (let i = 0; i < vm.selectedDspModel.length; i++) {
            if (e.idDsp.includes(vm.selectedDspModel[i].id))
              return true;
          }
          return false;
        });
        vm.dspsUtilizationsCommercialOffersCountries.commercialOffers = vm.backup.commercialOffers.filter(function (e) {
          for (let i = 0; i < vm.selectedDspModel.length; i++) {
            if (e.idDSP === vm.selectedDspModel[i].id)
              return true;
          }
          return false;
        });
      }
      if (vm.selectedUtilizationModel && vm.selectedUtilizationModel.length !== 0) {
        vm.dspsUtilizationsCommercialOffersCountries.commercialOffers = vm.dspsUtilizationsCommercialOffersCountries.commercialOffers.filter(function (e) {
          for (let i = 0; i < vm.selectedUtilizationModel.length; i++) {
            if (e.idUtilizationType === vm.selectedUtilizationModel[i].id)
              return true;
          }
          return false;
        });
      }
    }

    function onItemDeselectDsp(item) {
      if (vm.selectedDspModel && vm.selectedDspModel.length === 0) {
        vm.dspsUtilizationsCommercialOffersCountries.commercialOffers = angular.copy(vm.backup.commercialOffers);
        vm.dspsUtilizationsCommercialOffersCountries.utilizations = angular.copy(vm.backup.utilizations);
      } else
        onItemSelectDsp(item);
    }
  }

  filterStatisticsCtrl.$inject = ['$route', 'ngDialog', '$routeParams', '$location', '$http'];

  angular
    .module('codmanApp')
    .component('filterDspStatistics', {
      templateUrl: 'app/multimediale/components/filter-dsp-statistics.html',
      controller: filterStatisticsCtrl,
      controllerAs: 'vm',
      bindings: {
        dspsUtilizationsCommercialOffersCountries: '<',
        anagClientData: '<',
        filterParameters: '<',
        onFilterApply: '&',
        hideBackClaimSelect: '<',
        hideClientSelect: '<',
        showInvoiceCode: '<',
        showFlagRipartito: '<',
        dateFrom: '<',
        dateTo: '<',
        selectedClient: '<',
        showDsrProcessingStatuses: '<',
        showIdDsr: '<',
        dsrProcessingStatuses: '<',
        showDeliverCcidStatuses: '<',
        showZipName: '<'
      }
    });
})();

