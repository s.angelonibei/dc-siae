codmanApp.controller('confAggiTrattenutaCtrl', [ '$scope','$route','$interval', 'configService','societaTutela', '$routeParams','configurationsService','ngDialog',
 function($scope, $route, $interval, configService, societaTutela, $routeParams, configurationsService, ngDialog) {

	$scope.mandante ={};
	$scope.configDomain = "multimediale.aggi";
	$scope.configurations = [];

    $scope.ctrl = { pageTitle: `Configurazione aggi/trattenute`,
    societaTutela: societaTutela.data,
		};
		
	$scope.mandanti = $scope.ctrl.societaTutela;

	$scope.configurationTemplate =  {
		"domain":`${$scope.configDomain}`,
		"key":"",
		"label":"",
		"active":true,
		"topLevel":true,
		"settings":[
		   { 
			"valueType":"Decimal",
			"label": "Aggio DEM",
			"value":"0",
			"key":"threshold.aggioDEM", 
			"settingType":"key_value"
		   },
		   {  
			"valueType":"Decimal",
			"label": "Aggio DRM",
			"value":"0",
			"key":"threshold.aggioDRM", 
			"settingType":"key_value",
		 },
		 { 
		  "valueType":"Decimal",
		  "label": "Trattenuta DEM",
		  "value":"0",
		  "key":"threshold.trattenutaDEM", 
		  "settingType":"key_value"
		 }    
		]
	 };
	
	
        
	$scope.messages = {
		info: "", 
		error: ""
	};

	$scope.clearMessages = function() {
		$scope.messages = {
			info: "", 
			error: ""
		};
	};

	$scope.infoMessage = function(message) {
		$scope.messages.info = message;
	};

	$scope.errorMessage = function(message) {
		$scope.messages.error = message;
	};

	$scope.init = function(){
		$scope.clearMessages();
		configurationsService.loadConfiguration(`${$scope.configDomain}`).then(function success(results){
			$scope.configurations = results;
		}, function error(error) {
			$scope.errorMessage(" Si è verificato un errore, riprovare più tardi!");
			$('#rightPanel')[0].scrollIntoView( true );
			window.scrollBy(0, -100);

		});       
	};

	$scope.filter = function(societa){
		$scope.clearMessages();
		configurationsService.loadConfiguration(`${$scope.configDomain}`).then(function success(results){
			$scope.configurations = results;
			$scope.configurations = $scope.configurations.filter(el => (el.key == societa.codice || !societa.codice));
		}, function error(error) {
			$scope.errorMessage(" Si è verificato un errore, riprovare più tardi!");
			$('#rightPanel')[0].scrollIntoView( true );
			window.scrollBy(0, -100);
		});		
	}

	$scope.init();

	$scope.showName = function(codice){
		var name = "";
		if(codice){
			name = $scope.mandanti.filter(el => el.codice == codice);
			
		} 
		return name[0].nominativo;
	};

	
	$scope.newConfiguration = function(){
		$scope.clearMessages();
		var scope = $scope;
		return ngDialog.open({
			template : 'pages/ripartizione/dialog-crea-configurazione-aggi.html',
			plain : false,
			width : '40%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				var ngDialogId = $scope.ngDialogId;
				$scope.mandanti = scope.mandanti;
				$scope.errorMessage ="";
				$scope.configuration = _.cloneDeep(scope.configurationTemplate);
				$scope.selectedMandante = scope.mandante;
				$scope.ctrl = {
					ngDialogId : ngDialogId
				};

				$scope.cancel = function() {
					ngDialog.close(ngDialogId);
				};

				$scope.saveConfiguration = function() {
					$scope.configuration.key = $scope.selectedMandante.codice;
					$scope.configuration.label = `Configurazione Aggi e trattenuta ${$scope.configuration.key}`;
					configurationsService.saveConfiguration($scope.configuration).then(function success(result) {
					ngDialog.close(ngDialogId);
					scope.infoMessage(" Salvataggio effettuato con successo!");
					configurationsService.loadConfiguration(`${scope.configDomain}`).then(function success(results){
						scope.configurations = results;
						scope.configurations = scope.configurations.filter(el => (el.key == scope.mandante.codice || !scope.mandante.codice));
					}, function error(error) {
						$scope.errorMessage(" Si è verificato un errore, riprovare più tardi!");
						$('#rightPanel')[0].scrollIntoView( true );
						window.scrollBy(0, -100);
					});						
					}, function error(error) {
						if (error.status != 200) {
                            $scope.errorMessage =error.data.errorMessages[0];
                        }
					});
				  };
				  
			}]
		});
	};

	$scope.deleteConfiguration = function (configuration){
		$scope.clearMessages();
		var scope = $scope;
        ngDialog.openConfirm({
			template: 'pages/ripartizione/dialog-alert.html',
			plain: false,
			data: {
				configuration: configuration
			},
			controller:  function () {
				var ctrl = this;
				ctrl.title= "Elimina Configurazione";
				ctrl.message= `Confermi l'eliminazione della configurazione?`;
			},
			controllerAs: 'ctrl'
        }).then(function confirm(data) {
			configurationsService.deleteConfiguration(data.configuration).then(
				function success(response){
					var index = $scope.configurations.findIndex(el => el.id === data.configuration.id);
					$scope.configurations.splice(index,1);
				},
				function error(response){
					scope.errorMessage('Eliminazione non riuscita');
				}
			);
        }, function cancel() {
          //no op
        });
	};


	$scope.findKey = function(settings,key){
		return setting =settings.find(el => el.key == key); 
	}
	

}]);

