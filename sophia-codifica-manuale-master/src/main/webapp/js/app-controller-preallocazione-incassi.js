/**
 * DsrMetadataCtrl
 * 
 * @path /dsrMetadata
 */
codmanApp.controller('PreallocazioneIncassiCtrl', [ '$scope', '$route', 'ngDialog', '$routeParams', '$location', '$http', 'Upload', 'configService', function($scope, $route, ngDialog, $routeParams, $location, $http, Upload, configService) {

	$scope.ctrl = {
		navbarTab : 'ccidConfig',
		config : [],
		periodi :[],
		filterParameters : [],
		execute : $routeParams.execute,
		hideForm : $routeParams.hideForm,
		listaEmittenti : [],
		listaCanali : [],
		selectedTipoBroadcaster : "",
		emittente : $routeParams.emittente || 'TUTTE',
		canale : $routeParams.canale || 'TUTTI',
		anno : $routeParams.anno || '',
		selectedPeriodTo : $routeParams.selectedPeriodTo || '',
		selectedPeriodFrom : $routeParams.selectedPeriodFrom || '',
		anni : [ (new Date()).getFullYear(), (new Date()).getFullYear() - 1, (new Date()).getFullYear() - 2, (new Date()).getFullYear() - 3, (new Date()).getFullYear() - 4 , (new Date()).getFullYear() - 5 , (new Date()).getFullYear() - 6 , (new Date()).getFullYear() - 7  ],
		tipiEmittenti : [ 
			{
				"nome" : "TUTTE"
			}, {
				"nome" : "TELEVISIONE"
			}, {
				"nome" : "RADIO"
			} ],
		first : 0,
		last : 50,
		maxrow : 50,
		dettailSelected : null,
		storico	:[],
		conto:null,
		listaTipiDiritto:[ 
			{
				"nome" : "TUTTI"
			}, {
				"nome" : "DEM"
			}, {
				"nome" : "DRM"
			}]
		
	};

	$scope.multiselectSettings = {
		scrollableHeight : '200px',
		scrollable : false,
		enableSearch : false,
		showUncheckAll : true,
		searchBr : false,
		checkboxes : false
	};


	// Funzione principale richiamata al caricamento della pagina
	$scope.onRefresh = function() {
		
	};
	$scope.navigateToNextPage = function() {
		$scope.getConti($scope.ctrl.tipoDiritto,$scope.ctrl.selectedTipoBroadcaster,$scope.ctrl.emittente, $scope.ctrl.canale, $scope.ctrl.anno, $scope.ctrl.selectedPeriodFrom,$scope.ctrl.selectedPeriodTo,$scope.ctrl.last,$scope.ctrl.last + $scope.ctrl.maxrows,50)
	};


	$scope.formatNumber = function(number){
 		return twoDecimalRound(number);
	}
	
	$scope.checkSize = function(list){
		if(list!=undefined&&list!=null&&list.length>0){
			if (typeof list === 'string') {
				return true;
			}else{
				return false;
			}
		}else{
			return true;
		}
	}
	
	$scope.calcolaImportoAllocazione= function(item){
		var sommaAllocato = 0;
		for (var i = 0; i < $scope.ctrl.dettagliConto.dettaglioPianoDeiConti.length; i++) {
			if(item.importoAllocazioneDefinitivo==undefined||item.importoAllocazioneDefinitivo==null){
				sommaAllocato=sommaAllocato+Number($scope.ctrl.dettagliConto.dettaglioPianoDeiConti[i].percentualeAllocazione);
			}
		}
		if(item.importoAllocazioneDefinitivo==undefined||item.importoAllocazioneDefinitivo==null){
	 		return twoDecimalRound((((item.percentualeAllocazione*100)/sommaAllocato)*$scope.ctrl.dettagliConto.codiceConto.importo)/100);
		}else{
			return "";
		}
	}
	
	$scope.checkPercentuale= function(item){
		if(item=null||$scope.ctrl.dettagliConto==undefined||$scope.ctrl.dettagliConto.dettaglioPianoDeiConti==undefined){
			return false;
		}
		var sommaAllocato = 0;
		for (var i = 0; i < $scope.ctrl.dettagliConto.dettaglioPianoDeiConti.length; i++) {
			sommaAllocato=sommaAllocato+Number($scope.ctrl.dettagliConto.dettaglioPianoDeiConti[i].percentualeAllocazione);
		}
		if(sommaAllocato==100){
			return false;
		}else{
			return true;
		}
	}
	$scope.navigateToPreviousPage = function() {

		$scope.getConti($scope.ctrl.tipoDiritto,$scope.ctrl.selectedTipoBroadcaster,$scope.ctrl.emittente, $scope.ctrl.canale, $scope.ctrl.anno, $scope.ctrl.selectedPeriodFrom,$scope.ctrl.selectedPeriodTo,Math.max($scope.ctrl.first - $scope.ctrl.maxrows, 0),$scope.ctrl.first,50)
		
	}
	
	$scope.getNoteDisponibili = function(notaEmittenti,notaRipartimento) {
		if((notaEmittenti!=null && notaEmittenti != undefined &&notaEmittenti !=="")||
		   (notaRipartimento!=null && notaRipartimento != undefined &&notaRipartimento !=="")){
			return "Si";
		}else{
			return "No";
		}
			
	}
	
	$scope.formatDate = function(dateString) {
		var date = new Date(dateString)
		var separator = "-"
		var month = date.getMonth() + 1
		return (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + separator + (month < 10 ? "0" + month : month)
			+ separator + date.getFullYear()
	}
	
	$scope.getEmittenti = function(item) {
		$scope.ctrl.utenti = [];
		$scope.ctrl.selectedUser = "";
		var tipoBroadcaster = item
		if (tipoBroadcaster==="TUTTE") {
			tipoBroadcaster=null;
		}
		$http({
			method : 'GET',
			url : 'rest/broadcasting/emittentiFiltrati',
			params : {
				fiteredType : tipoBroadcaster
			}
		}).then(function successCallback(response) {
			$scope.ctrl.listaEmittenti = response.data;
			$scope.ctrl.listaEmittenti.unshift({
				"id" : -1,
				"nome" : "TUTTE",
				"tipo_broadcaster" : ""
			})
			$scope.ctrl.canale = {};
		}, function errorCallback(response) {
			$scope.ctrl.listaEmittenti = [];
			$scope.ctrl.emittente = {};
			$scope.ctrl.canale = {};
		});
	}
	
	$scope.getCanali = function(emittente) {
		var id =null;
		if (emittente==null||emittente==undefined||emittente.nome==="TUTTE") {
			id=null;
		}else{
			id=emittente.id
		}
		$http({
			method : 'GET',
			url : 'rest/broadcasting/canali/' + id,
			params : {

			}
		}).then(function successCallback(response) {
			$scope.ctrl.listaCanali = response.data;
			$scope.ctrl.listaCanali.unshift({
				"id" : -1,
				"nome" : "TUTTI",
				"tipo_broadcaster" : ""
			})
			$scope.ctrl.canale = {};
		}, function errorCallback(response) {
			$scope.ctrl.listaCanali = []
			$scope.ctrl.listaCanali.unshift({
				"id" : -1,
				"nome" : "TUTTI",
				"tipo_broadcaster" : ""
			})
			$scope.ctrl.canale = {};
		});

	};
	
	$scope.getListFiltered = function(item) {
		var results = item ? $scope.ctrl.listaEmittenti.filter($scope.createFilterFor(item)) : $scope.ctrl.listaEmittenti,
			deferred;

		return results;


	};

	$scope.createFilterFor = function(query) {
		var lowercaseQuery = angular.uppercase(query);

		return function filterFn(item) {
			return (item.nome.indexOf(lowercaseQuery) === 0);
		};

	}

	

	$scope.apply = function(tipoEmittente,emittente, canale, anno, dataFrom , dataTo) {
		// TODO Aggiungere la chiama al servizio che torna la lista dei carichi
		// di ripartizione
		$scope.ctrl.selectedTipoBroadcaster=tipoEmittente
		$scope.ctrl.emittente=emittente
		$scope.ctrl.canale=canale
		$scope.ctrl.anno =anno
		$scope.ctrl.selectedPeriodFrom = dataFrom
		$scope.ctrl.selectedPeriodT = dataTo
		$scope.getConti($scope.ctrl.tipoDiritto,tipoEmittente,emittente, canale, anno, dataFrom, dataTo, 0, 50, 50)
	}
	
	
	$scope.getConti = function(tipoDiritto, tipoEmittente,emittente, canale, anno,  dataFrom, dataTo, first, last, maxrow){
		var nomeEmittente=null;
		var idEmittente=null;
		var idCanale=null;
		
		if (tipoEmittente.nome==null||tipoEmittente.nome==undefined||tipoEmittente.nome==="TUTTE") {
			nomeEmittente=undefined;
		}else{
			nomeEmittente=tipoEmittente.nome
		}
		
		if (emittente==null||emittente==undefined||emittente.nome==="TUTTE") {
			idEmittente=null;
		}else{
			idEmittente=emittente.id
		}
		
		var id =null;
		if (canale==null||canale==undefined||canale.nome==="TUTTI") {
			idCanale=null;
		}else{
			idCanale=canale.id
		}
		var diritto =null;
		if (tipoDiritto==null||tipoDiritto==undefined||tipoDiritto.nome==="TUTTI") {
			diritto=null;
		}else{
			diritto=tipoDiritto.nome
		}
		$http({
			method : 'POST',
			url : 'rest/preallocazioneIncassi/getConti',
    			headers: {
                    'Content-Type': 'application/json'},
			data : {
				dateFrom:dataFrom,
				dateTo:dataTo,
				tipoDiritto:diritto,
				tipoEmittente:nomeEmittente,
				idEmittente:idEmittente,
				idCanale:idCanale,
				first: first,
				last: last,
				maxrow:maxrow,
			}
		}).then(function successCallback(response) {
			$scope.ctrl.conti = response.data.rows;
			$scope.ctrl.first = response.data.first;
			$scope.ctrl.last = response.data.last;
		
		}, function errorCallback(response) {
			$scope.ctrl.conti = []
		});
	}
	
	$scope.back= function(item) {
		$scope.ctrl.dettailSelected=null;
		$scope.ctrl.conto=null;
		$scope.ctrl.storico = [];
		$scope.ctrl.split_items=null;
		$scope.ctrl.dettagliConto=null;
		$scope.getConti($scope.ctrl.tipoDiritto,$scope.ctrl.selectedTipoBroadcaster,$scope.ctrl.emittente, $scope.ctrl.canale, $scope.ctrl.anno, $scope.ctrl.selectedPeriodFrom,$scope.ctrl.selectedPeriodTo,0,50)
	}
	$scope.cancelModify= function(item) {
		$scope.ctrl.dettagliConto=angular.copy($scope.ctrl.tmpDettagliConto);
	}
	
	$scope.updatePreallocazioneIncassi= function(username) {
		var conto=$scope.ctrl.dettagliConto;
		$http({
			method : 'POST',
			url : 'rest/preallocazioneIncassi/updatePreallocazioneIncassi',
    			headers: {
                    'Content-Type': 'application/json'},
			data : {
				idPianoDeiConti:conto.idBdcPianoDeiConti,
				conto:conto.codiceConto.codiceConto,
				utente:username,
				dettaglioPianoDeiConti:conto.dettaglioPianoDeiConti
			}
		}).then(function successCallback(response) {
			if (typeof response.data === 'string') {
				if (response.data.includes("change")) {
					$scope.showError("Errore","Impossibile salvare la modifica, non è stata variata alcuna percentuale di allocazione");
				}else{
					$scope.showError("Errore","Impossibile salvare la modifica, errore imprevisto");
				}
				
			}else{
				$scope.ctrl.tmpDettagliConto =angular.copy(response.data)
				$scope.ctrl.dettagliConto = response.data;
				$http({
					method : 'POST',
					url : 'rest/preallocazioneIncassi/getStoricoConto',
					params : {
						numeroConto : $scope.ctrl.conto.conto,
					}
				}).then(function successCallback(response) {
					$scope.ctrl.storico = response.data;
					$scope.showError("Esito","La modifica è stata eseguita con successo");
				}, function errorCallback(response) {
					$scope.ctrl.storico = [];
				});		
			}
		}, function errorCallback(response) {
			$scope.ctrl.conti = []
		});
	}
	
	$scope.showConto = function(item) {
		$scope.ctrl.dettailSelected=item;
		$scope.ctrl.conto=item;
		
		$http({
			method : 'POST',
			url : 'rest/preallocazioneIncassi/getDettaglio',
			params : {
				id : $scope.ctrl.conto.pianoConti,
			}
		}).then(function successCallback(response) {
			
			$scope.ctrl.tmpDettagliConto =angular.copy(response.data)
			$scope.ctrl.dettagliConto = response.data;
		}, function errorCallback(response) {
			$scope.ctrl.dettagliConto = [];
		});
		
		$http({
			method : 'POST',
			url : 'rest/preallocazioneIncassi/getStoricoConto',
			params : {
				numeroConto : $scope.ctrl.conto.conto,
			}
		}).then(function successCallback(response) {
			$scope.ctrl.storico = response.data;
		}, function errorCallback(response) {
			$scope.ctrl.storico = [];
		});
		
	};
	
	$scope.isRadio = function(){
		if( $scope.ctrl.conto!=null&&$scope.ctrl.conto!=undefined){
	 		return $scope.ctrl.conto.canale.bdcBroadcasters.tipo_broadcaster==="RADIO";
		}
	}
	
	$scope.formatDateHour = function(dateString) {
		if (dateString != undefined && dateString != null) {
			var date = new Date(dateString)
			var separator = "-"
			var month = date.getMonth() + 1
			return (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + separator + (month < 10 ? "0" + month : month) + separator + date.getFullYear() + " " + (date.getHours() < 10 ? "0" + date.getHours() : date.getHours()) + ":" + (date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes())

		} else {
			return "";
		}
	}
	
	
	$scope.getTextFromBool= function(bool) {
		if(bool==null||bool==="null"||bool==undefined){
			return ""
		}
		if(bool===true){
			return "Si"
		}else if(bool===false){
			return "No"
		}
		
		if(bool==="true"){
			return "Si"
		}else if(bool==="false"){
			return "No"
		} else{
			return bool
		}
	}

	$scope.getTextReport= function(repoDisponibile,kpiValido) {
		if(repoDisponibile===true){
			return "Si"
		}else if(repoDisponibile===false){
			return "No"
		}	
	}

	$scope.getTextImportoRipartibile= function(item) {	
		if(item==null||item==undefined){
			return ""
		}
		if(item.importoRipartibile==null||item.importoRipartibile==undefined){
			if(item.incasso==="CONSOLIDATO"&&item.repoDisponibile&&!sospesoPerContenzioso){
				return "Si"
			}else{
				return "No"
			}
		}
		
		if(item.importoRipartibile===true){
			return "Si"
		}else if(item.importoRipartibile===false){
			return "No"
		}
		
	
	}
	
	// funzione che renderizza il popup per l'aggiunta di una configurazione del
	// DSP
	$scope.openDettail = function(item,storico) {
		var rootScope = $scope
		var nConto = item;
		var nStorico = storico;
		ngDialog.open({
			template : 'pages/broadcasting/preallocazione-incassi/preallocazione-incassi-dettaglio-storico.jsp',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', '$http', function($scope, ngDialog, $http) {

				$scope.ctrl = {
					pianoConti:nConto,
					storico:nStorico,
					dettagliConto : [],
					rScope : rootScope
				};
				
				$scope.formatDate = function(dateString) {
					if (dateString != undefined && dateString != null) {
						var date = new Date(dateString)
						var separator = "-"
						var month = date.getMonth() + 1
						return (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + separator + (month < 10 ? "0" + month : month) + separator + date.getFullYear() + " " + (date.getHours() < 10 ? "0" + date.getHours() : date.getHours()) + ":" + (date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes())

					} else {
						return "";
					}
				}
				
				$http({
					method : 'POST',
					url : 'rest/preallocazioneIncassi/getDettaglio',
					params : {
						id : $scope.ctrl.storico.pianoConti,
					}
				}).then(function successCallback(response) {
					$scope.ctrl.dettagliConto = response.data;
				}, function errorCallback(response) {
					$scope.ctrl.dettagliConto = [];
				});
				

				$scope.formatNumber = function(number){
			 		return twoDecimalRound(number);
				}
				
				
				$scope.calcolaImportoAllocazione= function(item){
					var sommaAllocato = 0;
					for (var i = 0; i < $scope.ctrl.dettagliConto.dettaglioPianoDeiConti.length; i++) {
						if(item.importoAllocazioneDefinitivo==undefined||item.importoAllocazioneDefinitivo==null){
							sommaAllocato=sommaAllocato+Number($scope.ctrl.dettagliConto.dettaglioPianoDeiConti[i].percentualeAllocazione);
						}
					}
					if(item.importoAllocazioneDefinitivo==undefined||item.importoAllocazioneDefinitivo==null){
				 		return twoDecimalRound((((item.percentualeAllocazione*100)/sommaAllocato)*$scope.ctrl.dettagliConto.codiceConto.importo)/100);
					}else{
						return "";
					}
				}

				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
			} ]
		}).closePromise.then(function(data) {
			if (data.value === true) {
				// no op
			}
		});


	};
	
	
	$scope.showError = function(title, msgs) {
		ngDialog.open({
			template : 'pages/advance-payment/dialog-alert.html',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				$scope.ctrl = {
					title : title,
					message : msgs
				};
				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				$scope.save = function() {
					ngDialog.closeAll(true);
				};
			} ]
		}).closePromise.then(function(data) {
			// no op
		});
	};

	$scope.showAlert = function(title, message) {
		ngDialog.open({
			template : 'pages/advance-payment/dialog-alert.html',
			plain : false,
			width : '50%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				var ngDialogId = $scope.ngDialogId;
				$scope.ctrl = {
					title : title,
					message : message,
					ngDialogId : ngDialogId
				};
				$scope.cancel = function(ngDialogId) {
					ngDialog.close(ngDialogId);
				};
			} ]
		}).closePromise.then(function(data) {
			if (data.value === true) {

			}
		});
	};

	
	
	$scope.onRefresh();

} ]);


codmanApp.filter('range', function() {
	return function(input, min, max) {
		min = parseInt(min); // Make string input int
		max = parseInt(max);
		for (var i = min; i <= max; i++)
			input.push(i);
		return input;
	};
});