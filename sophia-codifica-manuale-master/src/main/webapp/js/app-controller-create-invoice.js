 /**
 * CreateInvoiceCtrl
 *
 * @path /dspConfig
 */
codmanApp.controller('CreateInvoiceCtrl', ['$scope', '$route', 'ngDialog', '$routeParams', '$location', '$http', 'configService', 'anagClientService', 'anagClientData', 'dspsUtilizationsCommercialOffersCountries',
  function ($scope, $route, ngDialog, $routeParams, $location, $http, configService, anagClientService, anagClientData, dspsUtilizationsCommercialOffersCountries) {
    $scope.anagClientData = angular.copy(anagClientData);
    $scope.dspsUtilizationsCommercialOffersCountries = angular.copy(dspsUtilizationsCommercialOffersCountries);
    $scope.filterParameters = $routeParams;

    $scope.ctrl = {
      navbarTab: 'createInvoice',
      data: [],
      filterParameters: [],
      execute: $routeParams.execute,
      hideForm: $routeParams.hideForm,
      hideInfoUfficio: true,
      hideInfoCliente: true,
      maxrows: 50,
      first: 0,
      last: 50,
      sortType: 'priority',
      sortReverse: true,
      filter: $routeParams.filter,
      selectedItems: {},
      hideEditing: true,
      items: [],
      itemsToCollapse: {},
      accountingData: [],
      showCollapseButton: true,
      configuration: {},
      selectedDsp: {},
      clientData: {},
      annotationMap: {},
      licenceList: [],
      msInvoiceApiUrl: `${$location.protocol()}://${$location.host()}:${$location.port()}/${configService.msInvoiceApiPath}`,
      currentPage: 0
    };

    $scope.filterDsp = anagClientService.filterDsp;

    $scope.dialogSaveDraftMsg = '';
    $scope.dialogSaveDraftShowNavOption = false;
    $scope.dialogSaveDraftHideConfirmButton = false;
    $scope.dialogSaveDraftShowInvoicesRight = false;
    $scope.invoiceDtoInDraft = undefined;
    $scope.requestUUID = undefined;
    $scope.activeInvoiceRequestUUID = undefined;
    $scope.activeDraftUUID = undefined;
    $scope.annotation = '';

    $scope.allSelected = false;
    $scope.selectedCheckbox = {};
    $scope.codeSelectionList = [];

    $scope.allSelectedDsp = false;
    $scope.selectedCheckboxDsp = {};
    $scope.codeSelectionListDsp = [];

    $scope.navigateToNextPage = function () {
      $scope.ctrl.first = $scope.ctrl.last + 1;
      $scope.ctrl.last += $scope.ctrl.maxrows;
      var parameters = $scope.ctrl.filterParameters;
      $scope.ctrl.currentPage = $scope.ctrl.currentPage + 1;
      $scope.getPagedResults(
        $scope.ctrl.first,
        $scope.ctrl.last,
        parameters
      );
    };

    $scope.navigateToPreviousPage = function () {
      $scope.ctrl.last = $scope.ctrl.first - 1;
      $scope.ctrl.first -= ($scope.ctrl.maxrows);
      if ($scope.ctrl.currentPage > 0) {
        $scope.ctrl.currentPage = $scope.ctrl.currentPage - 1;
      } else {
        $scope.ctrl.currentPage = 0;
      }
      if ($scope.ctrl.first === 1) {
        $scope.ctrl.first = 0;
      }
      var parameters = $scope.ctrl.filterParameters;
      $scope.getPagedResults(
        $scope.ctrl.first,
        $scope.ctrl.last,
        parameters
      );
    };
    $scope.hasNext = function () {
      return $scope.ctrl.data.hasNext;
    };


    $scope.hasPrev = function () {
      return $scope.ctrl.data.hasPrev;
    };

    $scope.getFirst = function () {
      if ($scope.ctrl.first === 0) {
        return $scope.ctrl.first + 1;
      }
      return $scope.ctrl.first;
    };

    $scope.resetPaging = function () {
      $scope.ctrl.first = 0;
      $scope.ctrl.last = $scope.ctrl.maxrows;
    };

    // Funzione principale richiamata al caricamento della pagina
    $scope.onRefresh = function () {
      createItemReasonList();
      getInvoiceConfiguration();
      /*$scope.getPagedResults(0,
        $scope.ctrl.maxrows);*/
    };

    createItemReasonList = function () {
      $http({
        method: 'GET',
        url: $scope.ctrl.msInvoiceApiUrl + 'invoice/itemReason/all'
      }).then(function successCallback(response) {
        for (x in response.data) {
          $scope.codeSelectionList.push({
            label: response.data[x].code + ' - ' + response.data[x].description,
            id: x.toString(),
            idInvoiceReason: response.data[x].idItemInvoiceReason
          });
        }
      }, function errorCallback(response) {
        $scope.codeSelectionList = [];
      });
    };

    $scope.getPagedResults = function (first, last, parameters) {
      if (parameters.selectedClient) {

        var periodFrom = 0;
        var yearFrom = 0;
        var periodTo = 0;
        var yearTo = 0;

        if ((parameters.selectedPeriodFrom)) {
          var from = parameters.selectedPeriodFrom.split('-');
          periodFrom = parseInt(from[0], 10);
          yearFrom = parseInt(from[1], 10);
        }

        if (parameters.selectedPeriodTo) {
          var to = parameters.selectedPeriodTo.split('-');
          periodTo = parseInt(to[0], 10);
          yearTo = parseInt(to[1], 10);
        }

        parameters.dspList = getCollectionValues(parameters.selectedDspModel);
        parameters.countryList = getCollectionValues(parameters.selectedCountryModel);
        parameters.utilizationList = getCollectionValues(parameters.selectedUtilizationModel);
        parameters.offerList = getCollectionValues(parameters.selectedOfferModel);

        $http({
          method: 'POST',
          url: $scope.ctrl.msInvoiceApiUrl + 'invoice/all/ccid',
          headers: {
            'Content-Type': 'application/json'
          },
          data: {
            first: first,
            last: last,
            clientId: parameters.selectedClient.idAnagClient,
            dspList: parameters.dspList ? parameters.dspList : [],
            countryList: parameters.countryList ? parameters.countryList : [],
            utilizationList: parameters.utilizationList ? parameters.utilizationList : [],
            offerList: parameters.offerList ? parameters.offerList : [],            
            monthFrom: periodFrom,
            yearFrom: yearFrom,
            monthTo: periodTo,
            yearTo: yearTo,
            sortBy: $scope.sortBy ? $scope.sortBy : 'ccidParziale',
            asc: !!$scope.asc,
            currentPage: $scope.ctrl.currentPage || 0

          }
        }).then(function successCallback(response) {
          $scope.ctrl.data = response.data;
          setPageCheckbox();
        }, function errorCallback(response) {
          $scope.ctrl.data = [];
        });
        getClientData(parameters.selectedClient.idAnagClient);
      }
    };

    function getCollectionValues(collection) {
      var selectedList = [];
      if (collection) {
        for (var e in collection) {
          selectedList.push(collection[e].id ? collection[e].id : collection[e]);
        }
      }
      return selectedList;
    }

    getInvoiceConfiguration = function () {
      $http({
        method: 'GET',
        url: $scope.ctrl.msInvoiceApiUrl + 'invoice/configuration',
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(function successCallback(response) {
        $scope.ctrl.configuration = response.data;
      }, function errorCallback(response) {
        $scope.ctrl.configuration = {};
      });
    };

    $scope.getDocType = function () {
      if ($scope.ctrl.clientData && $scope.ctrl.clientData.foreignClient) {
        if ($scope.ctrl.clientData.foreignClient) {
          return $scope.ctrl.configuration.tipoDoc.estero;
        }

        return $scope.ctrl.configuration.tipoDoc.italia;
      }
    };

    getClientData = function (client) {
      $http({
        method: 'GET',
        url: $scope.ctrl.msInvoiceApiUrl + 'invoice/clientData',
        headers: {
          'Content-Type': 'application/json'
        },
        params: {
          client: client
        }
      }).then(function successCallback(response) {
        $scope.ctrl.clientData = response.data;
        $scope.ctrl.licenceList = getLicences($scope.ctrl.clientData);
      }, function errorCallback(response) {
        $scope.ctrl.clientData = {};
        openErrorDialog('Dati cliente non trovati.', true);
      });
    };

    getLicences = function () {

      let listaLicenze = [];
      angular.forEach($scope.ctrl.clientData.dspLicenceMap, function (v, k) {
        licenzeTmp = JSON.parse(v);

        for (i in licenzeTmp.licences) {
          let licenzaVal = licenzeTmp.licences[i];
          licenzaVal.dsp = k;
          listaLicenze.push(licenzaVal);
        }
        ;

      });

      return listaLicenze;
    };

    $scope.confirm = function () {
        $scope.sortBy = undefined;
      $scope.ctrl.hideEditing = false;
      var keys = Object.keys($scope.ctrl.selectedItems);
      for (i = 0; i < keys.length; i++) {
        copy = angular.copy($scope.ctrl.selectedItems[keys[i]]);
        copy.description = copy.dspName + '-' + copy.country + '-' + copy.periodString;
        copy.collapsed = false;
        $scope.ctrl.items.push(copy);
      }
      var ret = $scope.maxPeriod();
      var day;

      if (ret.periodType === 'quarter') {
        period = ret.period * 3;
        day = $scope.lastDayOfMonth(period);
        if (period <= 9) {
          var period = '0' + period;
        } else {
          var period = period;
        }
      } else {
        day = $scope.lastDayOfMonth(ret.period);
        if (ret.period <= 9) {
          var period = '0' + ret.period;
        } else {
          var period = ret.period;
        }
      }

      $scope.dateOfpertinence = ret.year + '-' + period + '-' + day;
    };

      $scope.sort = function (field) {
          $scope.sortBy = field;
          $scope.asc = !$scope.asc;
          $scope.getPagedResults($scope.ctrl.first, $scope.ctrl.last, $scope.ctrl.filterParameters)
      };


    $scope.selectedItemDsp = function (item) {
      if ($scope.ctrl.items === undefined) {
        $scope.allSelectedDsp = false;
        return;
      }
      if (item === true) {
        // seleziona tutti gli elementi
        for (i = 0; i < $scope.ctrl.items.length; i++) {
          var value = $scope.ctrl.items[i];
          if (!$scope.ctrl.selectedDsp[value.idCCIDmetadata]) {
            addItem(value, $scope.ctrl.itemsToCollapse);
            $scope.selectedCheckboxDsp[value.idCCIDmetadata] = true;
            setMainCheckboxDsp();
          }
        }
        return;
      } else if (item === false) {
        // deseleziona tutti gli elementi
        for (i = 0; i < $scope.ctrl.items.length; i++) {
          var key = $scope.ctrl.items[i].idCCIDmetadata;
          $scope.selectedCheckboxDsp[key] = false;
          addOrRemoveSelectedItem($scope.ctrl.items[i], $scope.ctrl.itemsToCollapse);
          // $scope.ctrl.itemsToCollapse = {}
        }
        setMainCheckboxDsp();
      } else {
        // aggiungi o rimuovi elemento in base alla sua presenza o meno
        // nella lista
        addOrRemoveSelectedItem(item, $scope.ctrl.itemsToCollapse);
        setMainCheckboxDsp();
      }
    };


    $scope.selectedItem = function (item) {
      if ($scope.ctrl.data.rows === undefined) {
        $scope.allSelected = false;
        return;
      }
      if (item === true) {
        // seleziona tutti gli elementi
        for (i = 0; i < $scope.ctrl.data.rows.length; i++) {
          var value = $scope.ctrl.data.rows[i];
          if (!$scope.ctrl.selectedItems[value.idCCIDmetadata]) {
            addItem(value, $scope.ctrl.selectedItems);
            $scope.selectedCheckbox[value.idCCIDmetadata] = true;
            setMainCheckbox();
          }
        }
        return;
      } else if (item === false) {
        // deseleziona tutti gli elementi
        for (i = 0; i < $scope.ctrl.data.rows.length; i++) {
          var key = $scope.ctrl.data.rows[i].idCCIDmetadata;
          $scope.selectedCheckbox[key] = false;
          addOrRemoveSelectedItem($scope.ctrl.data.rows[i], $scope.ctrl.selectedItems);
          // $scope.ctrl.selectedItems = {}
        }
        setMainCheckbox();
      } else {
        // aggiungi o rimuovi elemento in base alla sua presenza o meno
        // nella lista
        addOrRemoveSelectedItem(item, $scope.ctrl.selectedItems);
        setMainCheckbox();
      }
    };

    setMainCheckboxDsp = function () {
      $scope.allSelectedDsp = true;
      for (var i = 0; i < $scope.ctrl.items.length; i++) {
        if (!$scope.ctrl.itemsToCollapse[$scope.ctrl.items[i].idCCIDmetadata]) {
          $scope.allSelectedDsp = false;
        }
      }
    };

    setMainCheckbox = function () {
      $scope.allSelected = true;
      for (var i = 0; i < $scope.ctrl.data.rows.length; i++) {
        if (!$scope.ctrl.selectedItems[$scope.ctrl.data.rows[i].idCCIDmetadata]) {
          $scope.allSelected = false;
        }
      }
    };

    addOrRemoveSelectedItem = function (item, map) {
      if (!map[item.idCCIDmetadata]) {
        addItem(item, map);
      } else {
        removeItem(item, map);
      }
    };

    addItem = function (item, map) {
      map[item.idCCIDmetadata] = item;
    };

    removeItem = function (item, map) {
      delete map[item.idCCIDmetadata];
    };

    setPageCheckbox = function () {
      $scope.allSelected = true;
      for (var i = 0; i < $scope.ctrl.data && $scope.ctrl.data.rows && $scope.ctrl.data.rows.length; i++) {
        if (!$scope.ctrl.selectedItems[$scope.ctrl.data.rows[i].idCCIDmetadata]) {
          $scope.allSelected = false;
        }
      }
    };

    $scope.collapse = function () {
      if ($scope.selectedLicence === undefined) {
        openErrorDialog('Selezionare una licenza', false);
        return;
      }

      if (typeof $scope.ctrl.itemsToCollapse !== 'undefined'
        && Object.keys($scope.ctrl.itemsToCollapse).length > 0) {
        if (differentCurrencies($scope.ctrl.itemsToCollapse) !== true) {
          showDialogToAddDescription();
        } else {
          openErrorDialog('Valute differenti.', false);
          return;
        }
      }
    };

    differentCurrencies = function (values) {
      var valueArr = _.values(values).map(function (item) {
        return item.currency;
      });
      var value = '';
      var differentCurrency = valueArr.some(function (item, index, array) {
        var tmp = item;
        if (value === '') {
          value = tmp;
        }
        return value != tmp;
      });
      return differentCurrency;
    };

    createNewItem = function (description) {
      var keys = Object.keys($scope.ctrl.itemsToCollapse);
      var totFatturabile = 0;
      var totFatturabileResiduo = 0;
      var firstValue = false;
      var currency = '';
      if ($scope.annotation === '') {
        firstValue = true;
        $scope.annotation += 'Licenza ' + $scope.selectedLicence + " Diritto D'autore: ";
      }
      for (i = 0; i < keys.length; i++) {
        item = $scope.ctrl.itemsToCollapse[keys[i]];
        if (currency === '') {
          currency = item.currency;
        }
        totFatturabile += item.invoiceAmount;
        totFatturabileResiduo += item.valoreResiduo;
        for (var x = 0; x < $scope.ctrl.items.length; x++) {
          if ($scope.ctrl.items[x] == item) {
            annotation = extractItemAnnotationData($scope.ctrl.items[x]);
            if (!($scope.ctrl.annotationMap.hasOwnProperty(annotation))) {
              $scope.ctrl.annotationMap[annotation] = true;
              annotationSeparator = ', ';
              if (firstValue === true) {
                annotationSeparator = '';
                firstValue = false;
              }
              $scope.annotation += annotationSeparator + annotation;
            }
            $scope.ctrl.items.splice(x, 1);
          }
        }
      }

      var descriptionFound = _.find($scope.codeSelectionList, {'id': description.id});
      newItem = {
        description: descriptionFound,
        collapsed: true,
        totFatturabile: totFatturabile,
        totFatturabileResiduo: totFatturabileResiduo,
        currency: currency,
        containedItems: angular.copy($scope.ctrl.itemsToCollapse)
      };
      $scope.ctrl.accountingData.push(newItem);
      $scope.ctrl.itemsToCollapse = {};
    };

    $scope.selectionIsEmpty = function () {
      return isObjectEmpty($scope.ctrl.selectedItems);
    };

    $scope.accountingSelectionIsEmpty = function () {
      if (typeof $scope.ctrl.accountingData !== 'undefined' && $scope.ctrl.accountingData.length > 0
        && $scope.dateOfpertinence !== undefined && ($scope.dateOfpertinence || $scope.dateOfpertinence.length > 0)) {
        return false;
      }
      return true;
    };

    isObjectEmpty = function (obj) {
      if (Object.keys(obj).length === 0 && obj.constructor === Object) {
        return true;
      }

      return false;
    };

    $scope.setDescription = function (descriptionId) {
      if (descriptionId) {
        createNewItem(descriptionId);
      }
    };

    extractItemAnnotationData = function (item) {
      var periodPrefix = '';
      if (item.periodType === 'quarter') {
        periodPrefix = 'Q';
      }
      return item.utilizationType + ' ' + item.year + ' ' + periodPrefix + item.period;
    };

    $scope.saveDraft = function () {
      if (($scope.activeDraftUUID !== undefined && $scope.activeDraftUUID !== $scope.requestUUID) ||
        $scope.activeDraftUUID === undefined) {
        var itemList = [];
        var total = 0;
        var totalValueOrigCurrency = 0;
        var currency = '';
        for (index in $scope.ctrl.accountingData) {
          var ccidList = [];
          var valoreResiduo = 0;
          var invoiceAmount = 0;
          for (x in $scope.ctrl.accountingData[index].containedItems) {
            ccidList.push($scope.ctrl.accountingData[index].containedItems[x].idCCID);
            if (currency === '') {
              currency = $scope.ctrl.accountingData[index].currency;
            }
            valoreResiduo += $scope.ctrl.accountingData[index].containedItems[x].valoreResiduo;
            invoiceAmount += $scope.ctrl.accountingData[index].containedItems[x].invoiceAmount;
            total += $scope.ctrl.accountingData[index].containedItems[x].valoreResiduo;
            totalValueOrigCurrency += $scope.ctrl.accountingData[index].containedItems[x].invoiceAmount;
          }
          var ithItem =
            {
              idDescription: $scope.ctrl.accountingData[index].description.idInvoiceReason,
              totalValue: valoreResiduo,
              totalValueOrig: invoiceAmount,
              currency: $scope.ctrl.accountingData[index].currency,
              ccidList: ccidList,
              note: ''
            };
          itemList.push(ithItem);
          valoreResiduo = 0;
          invoiceAmount = 0;
        }

        $scope.dialogSaveDraftShowNavOption = true;
        $scope.dialogSaveDraftHideConfirmButton = true;
        $scope.dialogSaveDraftMsg = 'Elaborazione richiesta';
        showDialogSaveDraft();

        $http({
          method: 'POST',
          url: $scope.ctrl.msInvoiceApiUrl + 'invoice/saveInvoiceDraft',
          headers: {
            'Content-Type': 'application/json'
          },
          data: {
            itemList: itemList,
            annotation: $scope.annotation,
            idDsp: $scope.ctrl.selectedDsp.idDsp,
            total: total,
            totalValueOrigCurrency: totalValueOrigCurrency,
            currency: currency,
            clientId: $scope.ctrl.clientData.idAnagClient,
            dateOfpertinence: $scope.dateOfpertinence

          }
        }).then(function successCallback(response) {
          $scope.invoiceDtoInDraft = response.data;
          $scope.dialogSaveDraftShowNavOption = false;
          $scope.dialogSaveDraftHideConfirmButton = false;
          $scope.dialogSaveDraftMsg = 'Bozza salvata. Inviare richiesta fattura?';
          $scope.activeDraftUUID = $scope.requestUUID;
        }, function errorCallback(response) {
          $scope.dialogSaveDraftMsg = 'Errore applicativo. Riprovare o contattare l\'assistenza';
        });
      } else {
        openErrorDialog('Richiesta effettuata precedentemente, modificare i parametri', false);
      }
    };
    $scope.formatNumber = function (number) {
      return twoDecimalRound(number);
    };

    showDialogSaveDraft = function () {
      ngDialog.open({
        template: 'pages/invoice/save-draft.html',
        plain: false,
        width: '60%',
        scope: $scope,
        closeByDocument: false,
        controller: ['$scope', 'ngDialog', '$http', function ($scope, ngDialog, $http) {
          $scope.cancel = function () {
            ngDialog.closeAll(false);
          };

          $scope.goto = function (section) {
            if (section === 'creaFattura') {
              $route.reload();
            } else if (section === 'listaFatture') {
              $location.path('/invoices');
            }

            ngDialog.closeAll(false);
          };


/*          $scope.sendInvoiceRequest = function () {

          };*/

          $scope.sendInvoiceRequest = function () {
            if (($scope.activeInvoiceRequestUUID !== undefined && $scope.activeInvoiceRequestUUID !== $scope.requestUUID) ||
              $scope.activeInvoiceRequestUUID === undefined) {
              // esegui richiesta
              $http({
                method: 'GET',
                url: $scope.ctrl.msInvoiceApiUrl + 'invoice/requestInvoice',
                params: {
                  invoiceId: $scope.invoiceDtoInDraft.idInvoice
                }
              }).then(function successCallback(response) {
                $scope.activeInvoiceRequestUUID = $scope.requestUUID;
                $scope.dialogSaveDraftShowNavOption = true;
                $scope.dialogSaveDraftMsg = 'Richiesta Inviata';
                $scope.dialogSaveDraftShowInvoicesRight = true;
              }, function errorCallback(response) {
                openErrorDialogSimple('Errore applicativo. Riprovare o contattare l\'assistenza', false);
              });
            } else {
              openErrorDialog('Richiesta effettuata precedentemente, modificare i parametri', false);
            }
          };
        }]
      }).closePromise.then(function (data) {
        if (data.value === true) {
          // no op
        }
      });
    };

    openErrorDialog = function (message, reloadPage) {
      ngDialog.open({
        template: 'pages/invoice/dataNotFound.html',
        plain: false,
        closeByDocument: false,
        width: '60%',
        scope: $scope,
        controller: ['$scope', 'ngDialog', '$http', function ($scope, ngDialog, $http) {
          $scope.ctrl = {
            message: message
          };


          $scope.close = function (section) {
            ngDialog.close();
            if (reloadPage) {
              $route.reload();
            }
          };
        }]
      }).closePromise.then(function (data) {
        if (data.value === true) {
          // no op
        }
      });
    };


    openErrorDialogSimple = function (message, reloadPage) {
      ngDialog.open({
        template: 'pages/invoice/errorDialog.html',
        plain: false,
        closeByDocument: false,
        width: '60%',
        scope: $scope,
        controller: ['$scope', 'ngDialog', '$http', function ($scope, ngDialog, $http) {
          $scope.ctrl = {
            message: message
          };


          $scope.close = function (section) {
            ngDialog.close();
            if (reloadPage) {
              $route.reload();
            }
          };
        }]
      }).closePromise.then(function (data) {
        if (data.value === true) {
          // no op
        }
      });
    };
    showDialogToAddDescription = function () {
      ngDialog.open({
        template: 'pages/invoice/set-description.html',
        plain: false,
        width: '60%',
        scope: $scope,
        controller: ['$scope', 'ngDialog', '$http', function ($scope, ngDialog, $http) {
          $scope.cancel = function () {
            ngDialog.closeAll(false);
          };

          $scope.addDescription = function () {
            $scope.setDescription($scope.selectedCodeModel);
            requestUUID = guid();
            $scope.setUUID(requestUUID);
            ngDialog.closeAll(true);
          };
          $scope.formatNumber = function (number) {
            return twoDecimalRound(number);
          };

          $scope.checkValidity = function () {
            if (_.find($scope.codeSelectionList, {'id': $scope.selectedCodeModel.id})) {
              return false;
            }
            return true;
          };
        }]
      }).closePromise.then(function (data) {
        if (data.value === true) {
          // no op
        }
      });
    };

    $scope.setUUID = function (UUID) {
      $scope.requestUUID = UUID;
    };

    $scope.setCollapse = function (value) {
      $scope.ctrl.showCollapseButton = value;
    };
    $scope.showInfo = function (item) {
      ngDialog.open({
        template: 'pages/invoice/create-invoice-info.html',
        plain: false,
        width: '60%',
        controller: ['$scope', 'ngDialog', '$http', function ($scope, ngDialog, $http) {
          $scope.ctrl = {
            detailItems: item.containedItems
          };

          $scope.formatNumber = function (number) {
            return twoDecimalRound(number);
          };
          $scope.close = function () {
            ngDialog.closeAll(false);
            $scope.ctrl.detailItems = [];
          };
        }]
      }).closePromise.then(function (data) {
        if (data.value === true) {
          // no op
        }
      });
    };


    $scope.apply = function(parameters) {
      $scope.ctrl.filterParameters = parameters;
      $scope.ctrl.first = 0;
      $scope.ctrl.last = 50;
      $scope.getPagedResults(0,
        $scope.ctrl.maxrows,
        parameters
      );
    };

    $scope.lastDayOfMonth = function (value) {
      var period = value;
      if (period === 2) {
        return 28;
      }
      if (period === 4 || period === 6 || period === 9 || period === 11) {
        return 30;
      }
      if (period === 1 || period === 3 || period === 5 || period === 7 || period === 8 || period === 10 || period === 12) {
        return 31;
      }
    };

    $scope.maxPeriod = function () {
      var ret = $scope.ctrl.items[0];

      for (var i = 0; i < $scope.ctrl.items.length; i++) {
        console.log('index: ' + i + ' year: ' + $scope.ctrl.items[i].year + ' month: ' + $scope.ctrl.items[i].period);
        if (ret.year < $scope.ctrl.items[i].year) {
          ret = $scope.ctrl.tems[i];
        }

        if (ret.period < $scope.ctrl.items[i].period && ret.year === $scope.ctrl.items[i].year) {
          ret = $scope.ctrl.items[i];
        }
      }

      return ret;
    };
    $scope.onRefresh();

    $scope.selectedCodeModel = [];
    $scope.selectedDescriptionModel = [];


    $scope.multiselectSettings = {
      scrollableHeight: '200px',
      scrollable: false,
      enableSearch: true,
      selectionLimit: 1,
      showUncheckAll: false,
      searchBr: true,
      checkboxes: false
    };
  }]);
