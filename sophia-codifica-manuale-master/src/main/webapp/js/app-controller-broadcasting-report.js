/**
 * BroadcastingReportCtrl
 * 
 * @path /broadcasting-report
 */
codmanApp.controller('BroadcastingReportCtrl', ['$scope','$route', 'ngDialog', '$routeParams', '$location', '$http', 'Upload', 'configService', function($scope,$route, ngDialog, $routeParams, $location, $http, Upload, configService) {

	$scope.ctrl = {
		navbarTab: 'utenti',
		utenti:[],
		listaEmittenti:"",
		execute: $routeParams.execute,
		fiteredBroadcaster:"",
		fiteredChannel:"",
		fiteredTitle:"",
		fiteredFromDate:"",
		fiteredToDate:""
	};

	// Funzione principale richiamata al caricamento della pagina
	$scope.onRefresh = function() {
		//$scope.getUtilizzazioni();
	};

	$scope.back = function() {
		$scope.ctrl.selectedReport=null;
		$scope.ctrl.dettaglioUtilizzazione =null;
	};


	$http({
		method: 'GET',
		url: 'rest/broadcasting/emittenti',
		params: {
			
		}
	}).then(function successCallback(response) {
		$scope.ctrl.emittenti = response.data;
	}, function errorCallback(response) {
		$scope.ctrl.listaEmittenti = [];
	});


	$scope.getNameForSelect  = function(item) {
		if(item.tipo_broadcaster=="TELEVISIONE"){
    		return item.nome +" - TV";
		}else if(item.tipo_broadcaster=="RADIO"){
    		return item.nome +" - RADIO";
		}else{
    		return item.nome;
		}
	}

	$scope.convertSecond = function (seconds) {
		if(seconds!=undefined){
		    seconds = Number(seconds);
		    var hours = Math.floor(seconds / 3600);
		    var minutes = Math.floor(seconds % 3600 / 60);
		    var seconds = Math.floor(seconds % 3600 % 60);
		    return ((hours < 10 ? "0" : "") + hours + ":" +  (minutes < 10 ? "0" : "") + minutes + ":" + (seconds < 10 ? "0" : "") + seconds); 
		}else{
			return "";
		}
	}
	
	
	$scope.getCanali = function(emittente) { 
		var emit=emittente;
		if(emit==null||emit==undefined||emit.id==undefined){
			return;
		}
		$http({
			method: 'GET',
			url: 'rest/broadcasting/canali/'+emit.id,
			params: {
				
			}
		}).then(function successCallback(response) {
			$scope.ctrl.listaCanali = response.data;
		}, function errorCallback(response) {
			$scope.ctrl.listaCanali = []
		});

	};
	
	$scope.getUtilizzazioni = function() {
		$http({
			method: 'POST',
			url: 'rest/broadcastingS3/fe/utilizationfile/listutilarmon',
			headers: {
                'Content-Type': 'application/json'},
			data:{
				broadcasters : $scope.ctrl.fiteredBroadcaster,
			    canali : $scope.ctrl.fiteredChannel,
			    titoloTrasmissione : $scope.ctrl.fiteredTitle,
			    inizioTrasmissioneFrom : $scope.ctrl.fiteredFromDate,
			    inizioTrasmissioneTo : $scope.ctrl.fiteredToDate
			}
		}).then(function successCallback(response) {
               $scope.ctrl.utilizzazioni = response.data;
		}, function errorCallback(response) {
		      $scope.ctrl.utilizzazioni = [];
		});
   
	};
	
	$scope.getError = function (item) {
		if(item.regola1==undefined&&item.regola2==undefined&&item.regola3==undefined){
	        return false;
		}else{
	        return true;
		}
    };
    
	$scope.getStato = function (item) {
		if(item.regola1==undefined&&item.regola2==undefined&&item.regola3==undefined){
	        return "Elaborato correttamente";
		}else{
	        return "Errore di elaborazione";
		}
    };
    $scope.getImgStato = function (item) {
		if(item.regola1==undefined&&item.regola2==undefined&&item.regola3==undefined){
	        return "images/ok.png";
		}else{
	        return "images/ko.png";
		}
    };
    
	$scope.encode = function (buffer) {
        var binary = '';
        var bytes = new Uint8Array(buffer);
        var len = bytes.byteLength;
        for (var i = 0; i < len; i++) {
            binary += String.fromCharCode(bytes[i]);
        }
        return window.btoa(binary);
    };
	
	$scope.showUtilizzazione = function(utilizzazione) {
		$scope.ctrl.selectedReport=utilizzazione;
		$scope.getDettaglioUtilizzazioni();
		   
	}
	$scope.getDettaglioUtilizzazioni= function(){
		$http({
			method: 'POST',
			url: 'rest/broadcastingS3/fe/utilizationfile/utilizzazioniArmonizzateDettaglio',
			data: {
				bdcBroadcasters: $scope.ctrl.fiteredBroadcaster,
				idShowSchedule:$scope.ctrl.selectedReport.id,
				error: $scope.getError($scope.ctrl.selectedReport)
			}
		}).then(function successCallback(response) {
			$scope.ctrl.dettaglioUtilizzazione = response.data;
			
		}, function errorCallback(response) {
		    $scope.ctrl.dettaglioUtilizzazione ={};
		});
	}
	

	showAlert = function(title, message) {
		ngDialog.open({
			template: 'pages/advance-payment/dialog-alert.html',
			plain: false,
			width: '50%',
			controller: ['$scope', 'ngDialog', function($scope, ngDialog) {	
				var ngDialogId=$scope.ngDialogId;
				$scope.ctrl = {
					title: title,
					message: message,
					ngDialogId: ngDialogId
				};
				$scope.cancel = function(ngDialogId) {
					ngDialog.close(ngDialogId);
				};
			}]
		}).closePromise.then(function (data) {
			if (data.value === true) {
				
			}
		});
	};
     
	
	$scope.formatDate = function(dateString){
		var date = new Date(dateString)
		var separator = "-"
		var month = date.getMonth() + 1	
		return (date.getDate() < 10 ? "0"+ date.getDate() : date.getDate())  + separator + (month < 10 ? "0"+ month : month) 
		+ separator + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes() 
	}

	
	$scope.onRefresh();
	
}]);


