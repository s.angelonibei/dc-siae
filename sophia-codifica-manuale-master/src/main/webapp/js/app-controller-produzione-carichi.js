codmanApp.controller('produzioneCarichiCtrl', ['$scope', 'ngDialog', '$routeParams', '$location', '$http',
    'configService', 'anagClientData', 'anagClientService', 'UtilService', 'accantonaContext', 'generaCarichiService', 'returnAdvancePayment', '$window', '$route', 'dspsUtilizationsCommercialOffersCountries',
    function ($scope, ngDialog, $routeParams, $location, $http, configService,
        anagClientData, anagClientService, UtilService, accantonaContext, generaCarichiService, returnAdvancePayment, $window, $route, dspsUtilizationsCommercialOffersCountries) {
        $scope.TUTTE = "Tutte";
        $scope.paging = {};
        $scope.page = 0;
        $scope.dspsUtilizationsCommercialOffersCountries = angular.copy(dspsUtilizationsCommercialOffersCountries);
        $scope.filterParameters = $routeParams;
        $scope.paginazioneList = [100, 250, 500, $scope.TUTTE];
        $scope.paginazioneSelected = $scope.paginazioneList[0];
        $scope.maxRows = $scope.paginazioneSelected;

        $scope.ctrl = {
            pageTitle: `Produzione carichi`,
            data: [],
            filterParameters: [],
            execute: $routeParams.execute,
            sortType: 'priority',
            sortReverse: true,
            filter: $routeParams.filter,
            advancePayment: null,
            selectedItems: [],
            items: [],
            anagClientData: anagClientData.data,
            msInvoiceApiUrl: `${$location.protocol()}://${$location.host()}:${$location.port()}/${configService.msInvoiceApiPath}`
        }
        $scope.ctrl.selectedDsp = [];
        $scope.ctrl.flagRipartito = false;
        $scope.ctrl.selectedCountryModel = [];
        $scope.ctrl.selectedOfferModel = [];
        $scope.ctrl.selectedUtilizationModel = [];
        $scope.asc = true;
        $scope.sortBy = '';
        $scope.arrayCcid = [];
        $scope.selectedItems = [];
        $scope.pagination = true;



        $scope.multiselectDsp = {
            scrollableHeight: '200px',
            scrollable: false,
            enableSearch: true,
            displayProp: 'name',
            idProp: 'idDsp'
        };


        $scope.utilizationsOffersCountries = function () {
            $http({
                method: 'GET',
                url: 'rest/data/dspUtilizationsOffersCountries'
            }).then(function successCallback(response) {
                $scope.ctrl.dspList = response.data.dsp;
                $scope.ctrl.utilizationList = response.data.utilizations;
                $scope.ctrl.commercialOffersList = response.data.commercialOffers;
                $scope.ctrl.countryList = response.data.countries;
                $scope.buildKeySets();

            });

        };

        $scope.utilizationsOffersCountries();


        $scope.buildKeySets = function () {
            $scope.ctrl.countrySelectionList = [];
            _.forEach($scope.ctrl.countryList, function (value) {
                $scope.ctrl.countrySelectionList.push({
                    label: value.name,
                    id: value.idCountry
                });
            });

            $scope.ctrl.utilizationSelectionList = [];
            _.forEach($scope.ctrl.utilizationList, function (value) {
                $scope.ctrl.utilizationSelectionList.push({
                    label: value.name,
                    id: value.idUtilizationType
                });
            });

            $scope.ctrl.offerSelectionList = [];
            _.forEach($scope.ctrl.commercialOffersList, function (value) {
                $scope.ctrl.offerSelectionList.push({
                    label: value.offering,
                    id: value.offering
                });
            });
        };

        $scope.apply = function (req) {
            var periodFrom, yearFrom, periodTo, yearTo;

            if ((req.selectedPeriodFrom)) {
                var from = req.selectedPeriodFrom.split('-');
                periodFrom = parseInt(from[0], 10);
                yearFrom = parseInt(from[1], 10);
            }

            if (req.selectedPeriodTo) {
                var to = req.selectedPeriodTo.split('-');
                periodTo = parseInt(to[0], 10);
                yearTo = parseInt(to[1], 10);
            }

            $scope.page = 0;
            var parameters = {
                dsp: $scope.getCollectionValues(req.selectedDspModel),
                countryList: $scope.getCollectionValues(req.selectedCountryModel),
                utilizationList: $scope.getCollectionValues(req.selectedUtilizationModel),
                offerList: $scope.getCollectionValues(req.selectedOfferModel),
                flagRipartito: req.flagRipartito,
                invoiceCode: req.invoiceCode,
                monthFrom: periodFrom,
                yearFrom: yearFrom,
                monthTo: periodTo,
                yearTo: yearTo,
                hideForm: false,
                currentPage: $scope.page,
                maxRows: $scope.maxRows
            };
            $scope.invoiceApply(parameters);
        };

        $scope.invoiceApply = function (parameters) {
            $scope.sortBy = undefined;
            angular.forEach(parameters, function (value, key) {
                $scope.ctrl.filterParameters[key] = value;
            });
            $scope.getCcid($scope.page, $scope.ctrl.filterParameters);
        };

        $scope.sort = function (field) {
            $scope.sortBy = field;
            $scope.asc = !$scope.asc;
            $scope.getCcid($scope.page, $scope.ctrl.filterParameters)
        };

        $scope.getCollectionValues = function (collection) {
            var selectedList = [];
            if (collection !== 'undefined') {
                for (var e in collection) {
                    selectedList.push(collection[e].id);
                }
            }
            return selectedList;
        };


        $scope.anagClientData = angular.copy(anagClientData);

        $scope.filterDsp = anagClientService.filterDsp;

        $scope.selectedItemsEqualNumberOfRows = selectedItemsEqualNumberOfRows;

        function selectedItemsEqualNumberOfRows() {
            return $scope.ctrl.selectedItems &&
                $scope.ctrl.dataCcid &&
                $scope.ctrl.dataCcid.rows &&
                Object.values($scope.ctrl.selectedItems).length === $scope.ctrl.dataCcid.rows.length;
        };

        // function uncheckAllSelectedAndGetCcid() {
        //
        //     // if($scope.allSelected)
        //     $scope.allSelected = false;
        //     $scope.getCcid($scope.page, $scope.ctrl.filterParameters);
        // };

        function isObjectEmpty(obj) {
            if (Object.keys(obj).length === 0 && obj.constructor === Object) {
                return true;
            }
            return false;
        }


        $scope.formatNumber = function (number) {
            return twoDecimalRound(number);
        };

        $scope.paginationSelectedEvent = function () {
            $scope.pagination = $scope.paginazioneSelected !== $scope.TUTTE; // true false based on selection
            $scope.maxRows = $scope.paginazioneSelected !== $scope.TUTTE ? // null or number based on selection
                $scope.paginazioneSelected :
                null;
            $scope.getCcid(0, $scope.ctrl.filterParameters);

        }

        $scope.multiselectSettingsFilter = {
            scrollableHeight: '200px',
            scrollable: false,
            enableSearch: true
        };

        $scope.multiselectSettings = {
            scrollableHeight: '200px',
            scrollable: false,
            enableSearch: true,
            selectionLimit: 1,
            showUncheckAll: false,
            searchBr: true,
            checkboxes: false
        };

        $scope.invoiceApply = function (parameters) {
            $scope.sortBy = undefined;
            angular.forEach(parameters, function (value, key) {
                $scope.ctrl.filterParameters[key] = value;
            });
            $scope.getCcid(0, $scope.ctrl.filterParameters)
        };


        /*function reassign(dataCcid) {
            $scope.ctrl.advancePayment = dataCcid.idAdvancePayment ? dataCcid.idAdvancePayment : void 0;
    
            angular.forEach($scope.ctrl.dataCcid.rows, function (oldData, oldKey) {
                $scope.ctrl.dataCcid.rows[oldKey].invoiceAmountNew = 0;
                angular.forEach(dataCcid.ccidList, function (updatedData, newKey) {
                    if (oldData.idCCIDmetadata === updatedData.idCCIDmetadata) {
                        $scope.ctrl.dataCcid.rows[oldKey] = updatedData;
                        $scope.ctrl.dataCcid.rows[oldKey].checked = true;
                    }
                });
            });
        }*/

        $scope.getCcid = function (pages, params) {
            $scope.ctrl.dataCcid = [];
            $scope.selectedItems = [];
            $scope.arrayCcid = [];
            $scope.allSelected = false;
            $scope.clearMessages();
            $http({
                method: 'POST',
                url: $scope.ctrl.msInvoiceApiUrl + 'invoice/all/ccidFattura',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: $scope.buildCcidFatturaBody(params, pages)
            }).then(function successCallback(response) {

                if (response.data.rows.length < 1) {
                    $scope.paging = { ...$scope.firstPaging };
                    $scope.response = [];
                    return;
                }
                $scope.paging.currentPage = response.data.currentPage;
                $scope.paging.maxrows = response.data.maxrows;
                if ($scope.paging.currentPage == 0) {
                    $scope.paging.firstRecord = 1;
                    $scope.paging.lastRecord = $scope.paging.maxrows;
                } else {
                    $scope.paging.firstRecord = (($scope.paging.currentPage) * $scope.paging.maxrows) + 1;
                    $scope.paging.lastRecord = ($scope.paging.currentPage) * $scope.paging.maxrows + response.data.rows.length;

                }
                $scope.arrayCcid = _.cloneDeep(response.data);
                $scope.ctrl.dataCcid = response.data;
                $scope.ctrl.selectedItems.forEach(function (el) {
                    var index = $scope.ctrl.dataCcid.rows.findIndex(item => el.idCcidInvoiceItem == item.idCcidInvoiceItem)
                    if (index >= 0) {
                        $scope.ctrl.dataCcid.rows[index].checked = true;
                    }

                })

                $scope.paging.hasPrev = response.data.hasPrev;
                $scope.paging.hasNext = response.data.hasNext;
            }, function errorCallback(response) {
                $scope.ctrl.dataCcid = [];
            });

        };

        $scope.buildCcidFatturaBody = function (params, pages) {
            const { pagination, maxRows } = $scope;

            const body = {
                currentPage: pages,
                maxRows,
                dspList: params.dsp ? params.dsp : [],
                countryList: (params.countryList ? params.countryList : []),
                utilizationList: (params.utilizationList ? params.utilizationList : []),
                offerList: (params.offerList ? params.offerList : []),
                invoiceCode: (params.invoiceCode ? params.invoiceCode : ''),
                flagRipartito: (params.flagRipartito ? params.flagRipartito : false),
                monthFrom: (params.monthFrom ? params.monthFrom : ''),
                yearFrom: (params.yearFrom ? params.yearFrom : ''),
                monthTo: (params.monthTo ? params.monthTo : ''),
                yearTo: (params.yearTo ? params.yearTo : ''),
                sortBy: $scope.sortBy ? $scope.sortBy : 'dspName',
                asc: $scope.asc,
                pagination
            }
            return _.omitBy(body, (e) => _.isNil(e) || e === "");
        }

        $scope.selectedCheckbox = {};
        // $scope.cCID = [];

        function addItem(item) {
            var i1 = $scope.ctrl.selectedItems.findIndex(el => el.idCcidInvoiceItem == item.idCcidInvoiceItem);
            if (i1 < 0)
                $scope.ctrl.selectedItems.push(item);
        };

        $scope.removeItem = function (item) {
            var index = $scope.ctrl.selectedItems.findIndex(el => el.idCcidInvoiceItem == item.idCcidInvoiceItem);
            $scope.ctrl.selectedItems.splice(index, 1);
            var a = $scope.arrayCcid.rows.findIndex(el => el.idCcidInvoiceItem == item.idCcidInvoiceItem);
            if (a >= 0)
                $scope.arrayCcid.rows.splice(i, 1);
            var b = $scope.ctrl.dataCcid.rows.findIndex(el => el.idCcidInvoiceItem == item.idCcidInvoiceItem);
            if (b >= 0)
                $scope.ctrl.dataCcid.rows[b].checked = false;

        }

        function addOrRemoveSelectedItem(item) {
            if (!$scope.ctrl.selectedItems.find(el => el.idCcidInvoiceItem == item.idCcidInvoiceItem)) {
                addItem(item);
            } else {
                $scope.removeItem(item);
            }
        };

        $scope.toggleItem = function (item) {
            if (!$scope.ctrl.dataCcid || !$scope.ctrl.dataCcid.rows) {
                $scope.allSelected = false;
                return;
            }
            if (item === true) {
                // seleziona tutti gli elementi
                for (var i = 0; i < $scope.ctrl.dataCcid.rows.length; i++) {
                    $scope.ctrl.dataCcid.rows[i].checked = true;
                    addItem($scope.ctrl.dataCcid.rows[i]);
                }
            } else if (item === false) {
                // deseleziona tutti gli elementi
                for (var i = 0; i < $scope.ctrl.dataCcid.rows.length; i++) {
                    $scope.ctrl.dataCcid.rows[i].checked = false;
                    $scope.removeItem($scope.ctrl.dataCcid.rows[i]);
                }
            } else {
                // aggiungi o rimuovi elemento in base alla sua presenza o meno
                // nella lista
                addOrRemoveSelectedItem(item);
            }
            // setMainCheckbox();
        };


        $scope.selectionIsEmpty = function () {
            if ($scope.ctrl && $scope.ctrl.selectedItems) {
                return !Object.keys($scope.ctrl.selectedItems).length;
            }
        };


        $scope.cancel = function () {
            // $scope.ctrl.hideEditing = false
            $scope.ctrl.data = [];
            if ($scope.ctrl.dataCcid)
                $scope.ctrl.dataCcid.rows = [];
            $scope.ctrl.advancePayment = undefined;
            $scope.ctrl.selectedItems = [];
            $scope.selectedCheckbox = {};
            angular.forEach($scope.ctrl.filterParameters, function (value, key) {
                if (key !== 'selectedClient')
                    $scope.ctrl.filterParameters[key] = undefined;
            });
        };

        isObjectEmpty = function (obj) {
            if (Object.keys(obj).length === 0 && obj.constructor === Object) {
                return true;
            }

            return false;
        };


        extractItemAnnotationData = function (item) {
            var periodPrefix = '';
            if (item.periodType === 'quarter') {
                periodPrefix = 'Q';
            }
            return item.utilizationType + ' ' + item.year + ' ' + periodPrefix + item.period;
        };


        $scope.goTo = function (url, param) {
            if (url != undefined && param != undefined) {
                $location.path(url + param);
            }
        };

        $scope.messages = {
            info: "",
            error: "",
            warning: ""
        }

        $scope.clearMessages = function () {
            $scope.messages = {
                info: "",
                error: "",
                warning: ""
            }
        }

        $scope.warningMessage = function (message) {
            $scope.messages.warning = message;
        }

        $scope.infoMessage = function (message) {
            $scope.messages.info = message;
        }

        $scope.errorMessage = function (message) {
            $scope.messages.error = message;
        }


        $scope.generaCarichi = function () {
            if ($scope.ctrl.selectedItems == 0) {
                UtilService.showAlert('Attenzione', 'Non sono stati selezionati CCID.');
                return
            }
            var scope = $scope;
            return ngDialog.open({
                template: 'pages/ripartizione/dialog-genera-carichi.html',
                plain: false,
                width: '40%',
                controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
                    var ngDialogId = $scope.ngDialogId;
                    $scope.ctrl = {
                        ngDialogId: ngDialogId
                    };

                    $scope.cancel = function () {
                        ngDialog.close(ngDialogId);
                    };

                    $scope.saveGeneraCarichi = function (idSiada) {
                        if (scope.ctrl.selectedItems && Object.keys(scope.ctrl.selectedItems).length > 0) {
                            generaCarichiService.saveGeneraCarichi(idSiada, scope.ctrl.selectedItems).then(function success(result) {
                                ngDialog.close(ngDialogId);
                                scope.goToListaCarichi();
                            }, function error(error) {
                                if (error.status != 200) {
                                    $scope.errorMessage(error.data.message);
                                    $('#rightPanel')[0].scrollIntoView(true);
                                    window.scrollBy(0, -100);
                                }
                            });
                        }
                    };
                }]
            });
        };

        $scope.goToListaCarichi = function () {
            $scope.goTo('/listaCarichi', '');
        }


        $scope.goTo = function (url, param) {
            if (url != undefined && param != undefined) {
                $location.path(url + param);
            }
        };

        $scope.clearMessages();

        $scope.navigateToPreviousPage = function () {
            $scope.page = $scope.page - 1;
            $scope.getCcid($scope.page, $scope.ctrl.filterParameters);
        }

        $scope.navigateToNextPage = function () {
            $scope.page = $scope.page + 1;
            $scope.getCcid($scope.page, $scope.ctrl.filterParameters);
        }

    }]
);

codmanApp.service('generaCarichiService', ['$http', 'configService', '$location', function ($http, configService, $location) {

    var service = this;

    service.msInvoiceApiUrl = `${$location.protocol()}://${$location.host()}:${$location.port()}/${configService.msInvoiceApiPath}`;


    service.saveGeneraCarichi = function (idSiada, ccid) {
        return $http({
            method: 'POST',
            url: service.msInvoiceApiUrl + 'invoice/genera-carico-ripartizione',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                idRipartizioneSiada: idSiada,
                ccidList: Object.values(ccid)
            }
        });

    }



}]);