codmanApp.controller('VisualizzazioneTracciamentoPMCtrl', [ '$scope', '$routeParams', '$route', '$http', '$location', 'ngDialog', '$rootScope', '$anchorScroll',
	function($scope, $routeParams, $route, $http, $location, ngDialog, $rootScope, $anchorScroll) {
	
	$scope.params = {};
	
//	if($rootScope.savedQuery != undefined && $rootScope.savedQuery != null){
//		if($rootScope.savedQuery.page != "PM"){
//			$rootScope.savedQuery = undefined;
//		}
//	}
	
	$scope.orderImage = function(name,param){
		
		//get id of html element clicked
		var id = "#" + name;
		
		//get css class of that html element for change the image of arrows
		var classe = angular.element(id).attr("class");

		//set to original position all images of arrows, on each columns.
		var element1 = document.getElementsByClassName("glyphicon glyphicon-sort-by-attributes");
		var element2 = document.getElementsByClassName("glyphicon glyphicon-sort-by-attributes-alt");
		angular.element(element1).attr("class","glyphicon glyphicon-sort");
		angular.element(element2).attr("class","glyphicon glyphicon-sort");

		if(classe != "glyphicon glyphicon-sort-by-attributes" || classe == "glyphicon glyphicon-sort"){
			
			angular.element(id).attr("class","glyphicon glyphicon-sort-by-attributes");
			
		}else{
			angular.element(id).attr("class","glyphicon glyphicon-sort-by-attributes-alt");
		}
		
		$scope.order(param);
		
	}
	
	$scope.orderBy = null;
	
	$scope.order = function(param){
		if($scope.orderBy == param.concat(" asc")){
			$scope.orderBy = param.concat(" desc");
		}else{
			$scope.orderBy = param.concat(" asc");
		}
		$scope.getSampling(false);
	}
	
	$scope.count = true;
	/*savadQuery rappresenta l'oggetto che memorizza i filtri relativi alla ricerca... quando si torna indietro tramite quest'oggetto si recuperano tutti i filtri*/
	$scope.goTo = function(url,param){
		if(url != undefined && param != undefined){
			$rootScope.savedQuery = {pagina : "PM",params: $scope.params, page: $scope.ctrl.page, totRecords : $scope.totRecords, pageTot: $scope.pageTot};
			$location.path(url + param);
		} 
	};
	
	$scope.exportExcel = function(){
		
		$location.hash('up');
		$anchorScroll();
		$location.hash('');
		
		$http({
			method: 'GET',
			url: 'rest/performing/cruscotti/exportPmList',
			params: $scope.params
		}).then( function(response) {

			var linkElement = document.createElement('a');

			try {

				var blob = new Blob([response.data], { type: 'application/csv' });
				var url = window.URL.createObjectURL(blob);
				linkElement.setAttribute('href', url);
                linkElement.setAttribute("download", 'ListaProgrammiMusicaliRicercati.csv');

                var clickEvent = new MouseEvent("click", {
                	"view": window,
                	"bubbles": true,
                	"cancelable": false
                });
                linkElement.dispatchEvent(clickEvent);

			} catch (ex) {

			}
		}, function (response) {
			$scope.showError("Errore","Impossibile fare il download dell'excel.");
		});
	}
	
	$scope.ctrl = {
		navbarTab : 'visualizzazioneTracciamentoPM',
		execute : $routeParams.execute,
		page : 0
	};
	
	$scope.navigateToNextPage = function() {
		$scope.ctrl.page=$scope.ctrl.page+1;
		$scope.getSampling(false);

	};

	$scope.navigateToPreviousPage = function() {
		$scope.ctrl.page=$scope.ctrl.page-1;
		$scope.getSampling(false);

	};
	
	$scope.navigateToEndPage = function(){
		$scope.ctrl.page = $scope.ctrl.pageTot;
		$scope.getSampling(false);
	}
	
	$scope.filterApply = function() {
		
		$scope.orderBy = null;
		
		$scope.ctrl.first = 0;
		$scope.ctrl.last = 50;
		$scope.ctrl.page = 0;
		$scope.getSampling(true);
	}
	
	$scope.getSampling = function(flag){
		
		if($scope.ctrl.selectedContabilityDateFrom == ""){
			var meseInizio = null;
			var annoInizio = null;
		}else{
			if($scope.ctrl.selectedContabilityDateFrom !== undefined){
				var meseInizio = 0;
				var annoInizio = 0;
				var from = $scope.ctrl.selectedContabilityDateFrom.split("-");
		        meseInizio = ""+parseInt(from[0],10);
		        annoInizio = ""+parseInt(from[1],10);
			}
		}
		
		if($scope.ctrl.selectedContabilityDateTo == ""){
			var meseFine = null;
			var annoFine = null;
		}else{
			if($scope.ctrl.selectedContabilityDateTo !== undefined){
				var meseFine = 0;
				var annoFine = 0;
		        var from = $scope.ctrl.selectedContabilityDateTo.split("-");
		        meseFine = ""+parseInt(from[0],10);
		        annoFine = ""+parseInt(from[1],10);
			}
		}
		
		if(annoFine < annoInizio){
			$scope.showError("Errore","Il parametro Contabilità a non può essere precedente al campo Contabilità da");
			return;
		}else if (annoFine == annoInizio){
			if(meseFine < meseInizio){
				$scope.showError("Errore","Il parametro Contabilità a non può essere precedente al campo Contabilità da");
				return;
			}
		}
		
		if($rootScope.savedQuery == undefined || $rootScope.savedQuery == null){
			
		
			$scope.params = {
					annoInizioPeriodoContabile : annoInizio,
					meseInizioPeriodoContabile : meseInizio,
					annoFinePeriodoContabile : annoFine,
					meseFinePeriodoContabile : meseFine,
					tipoDocumento: $scope.tipoDocumento,
					evento: $scope.idEvento,
					fattura: $scope.fattura,
					voceIncasso: $scope.voceIncasso,
					reversale: $scope.reversale,
					dataInizioEventoDa: $scope.dataInizioEventoDa,
					dataInizioEventoA: $scope.dataInizioEventoA,
					locale: $scope.locale,
					codiceBA: $scope.codiceBA,
					seprag: $scope.seprag,
					codiceSAPOrganizzatore: $scope.codiceSAP,
					organizzatore: $scope.ivaCF,
					direttoreEsecuzione: $scope.DE,
					permesso: $scope.permesso,
					titoloOpera: $scope.titoloOpera,
					supporto: $scope.supporto,
					tipoProgrammi: $scope.tipoProgrammi,
					gruppoPrincipale: $scope.gruppo,
					numeroPM: $scope.numeroPM,
					page: $scope.ctrl.page,
					count: flag,
					order : $scope.orderBy
				};
		}else{
			$scope.params = $rootScope.savedQuery.params;
			$rootScope.savedQuery = undefined;
		}
		
		$http({
			method: 'GET',
			url: 'rest/performing/cruscotti/tracciamentoPMLista',
			params: $scope.params
		}).then(function(response){
			if(flag == true){
				$scope.pageTotTemp = Math.floor(response.data/50);
				$scope.totRecordsTemp = response.data;
				if(response.data > 1000){
					$scope.showWarning("Warning","Il risultato prevede " + response.data + " record, è consigliabile applicare dei filtri aggiuntivi. Sicuro di voler continuare?");
				}else if (response.data == 0){ 
					$scope.fillElements(null,null,null,null,null,null);
					$scope.showError("","Non risulta nessun dato per la ricerca effettuata");
				}else{
					//aggiungere orderBy
					$rootScope.pageTot = $scope.pageTotTemp;
					$rootScope.totRecords = $scope.totRecordsTemp;
					$scope.getSampling(false);
				}
			}else{
				var currentPage = response.data.currentPage*50;
				$scope.fillElements($scope.totRecords, response.data.currentPage, currentPage, response.data.records.length, response.data.records, $scope.pageTot);
			}
			
		},function(response){
			$scope.fillElements(null,null,null,null,null,null);
			$scope.showError("","Non risulta nessun dato per la ricerca effettuata");
		})
		
	};
	
	//a seconda di come va la chiamata riempie gli oggetti che servono alla view
	$scope.fillElements = function(totRecords,page,first,last,samplings,pageTot){
		$scope.ctrl.totRecords = totRecords;
		$scope.ctrl.page = page;
		$scope.ctrl.first = first;
		$scope.ctrl.last = totRecords;
		$scope.ctrl.records = samplings;
		$scope.ctrl.pageTot = pageTot;
	}
	
	$scope.showWarning = function(title, msgs) {
		var parentScope = $scope
		ngDialog.open({
			template : 'pages/advance-payment/dialog-warning.html',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				$scope.ctrl = {
					title : title,
					message : msgs
				};
				$scope.cancel = function() {
					$rootScope.flag = false;
					ngDialog.closeAll(false);
				};
				$scope.call = function() {
					$rootScope.flag = true;
					parentScope.pageTot = parentScope.pageTotTemp;
					parentScope.totRecords = parentScope.totRecordsTemp;
					ngDialog.closeAll(true);
				};
			} ]
		}).closePromise.then(function(data) {
			// no op
			if($rootScope.flag == true){
				$scope.getSampling(false);
			}
		});
	};
	
	$scope.showError = function(title, msgs) {
		ngDialog.open({
			template : 'pages/advance-payment/dialog-alert.html',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				$scope.ctrl = {
					title : title,
					message : msgs
				};
				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				$scope.save = function() {
					ngDialog.closeAll(true);
				};
			} ]
		}).closePromise.then(function(data) {
			// no op
		});
	};
	
	if($rootScope.savedQuery != undefined && $rootScope.savedQuery != null){
		
		if($rootScope.savedQuery.pagina == "PM"){
			if($rootScope.savedQuery.params.meseInizioPeriodoContabile != undefined){
				if($rootScope.savedQuery.params.meseInizioPeriodoContabile < 10){
					$scope.ctrl.selectedContabilityDateFrom = '0'+ $rootScope.savedQuery.params.meseInizioPeriodoContabile + '-' + $rootScope.savedQuery.params.annoInizioPeriodoContabile;
				}else{
					$scope.ctrl.selectedContabilityDateFrom = $rootScope.savedQuery.params.meseInizioPeriodoContabile + '-' + $rootScope.savedQuery.params.annoInizioPeriodoContabile;
				}
			}
			if($rootScope.savedQuery.params.meseFinePeriodoContabile != undefined){
				if($rootScope.savedQuery.params.meseFinePeriodoContabile < 10){
					$scope.ctrl.selectedContabilityDateTo = '0'+ $rootScope.savedQuery.params.meseFinePeriodoContabile + '-' + $rootScope.savedQuery.params.annoFinePeriodoContabile;
				}else{
					$scope.ctrl.selectedContabilityDateTo = $rootScope.savedQuery.params.meseFinePeriodoContabile + '-' + $rootScope.savedQuery.params.annoFinePeriodoContabile;
				}
			}
			if($rootScope.savedQuery.params.tipoDocumento != undefined){
				$scope.tipoDocumento = $rootScope.savedQuery.params.tipoDocumento
			}
			
			if($rootScope.savedQuery.params.evento != undefined){
				$scope.idEvento = $rootScope.savedQuery.params.evento
			}
			
			if($rootScope.savedQuery.params.fattura != undefined){
				$scope.fattura =$rootScope.savedQuery.params.fattura
			}
			
			if($rootScope.savedQuery.params.voceIncasso != undefined){
				$scope.voceIncasso = $rootScope.savedQuery.params.voceIncasso
			}
			
			if($rootScope.savedQuery.params.reversale != undefined){
				$scope.reversale =$rootScope.savedQuery.params.reversale
			}
//			controllare data Inizio evento
			if($rootScope.savedQuery.params.dataInizioEventoDa != undefined){
				$scope.dataInizioEventoDa = $rootScope.savedQuery.params.dataInizioEventoDa
			}
			
			if($rootScope.savedQuery.params.dataInizioEventoA != undefined){
				$scope.dataInizioEventoA =$rootScope.savedQuery.params.dataInizioEventoA
			}
			
			if($rootScope.savedQuery.params.locale != undefined){
				$scope.locale = $rootScope.savedQuery.params.locale
			}
			
			if($rootScope.savedQuery.params.codiceBA != undefined){
				$scope.codiceBA =$rootScope.savedQuery.params.codiceBA
			}
			
			if($rootScope.savedQuery.params.seprag != undefined){
				$scope.seprag =$rootScope.savedQuery.params.seprag
			}
			
			if($rootScope.savedQuery.params.codiceSAPOrganizzatore != undefined){
				$scope.codiceSAP =$rootScope.savedQuery.params.codiceSAPOrganizzatore 
			}
			
			if($rootScope.savedQuery.params.organizzatore != undefined){
				$scope.ivaCF =$rootScope.savedQuery.params.organizzatore
			}
			
			if($rootScope.savedQuery.params.direttoreEsecuzione != undefined){
				$scope.DE =$rootScope.savedQuery.params.direttoreEsecuzione
			}
			
			if($rootScope.savedQuery.params.permesso != undefined){
				$scope.permesso =$rootScope.savedQuery.params.permesso
			}
			
			if($rootScope.savedQuery.params.titoloOpera != undefined){
				$scope.titoloOpera =$rootScope.savedQuery.params.titoloOpera
			}
			
			if($rootScope.savedQuery.params.supporto != undefined){
				$scope.supporto =$rootScope.savedQuery.params.supporto
			}
			
			if($rootScope.savedQuery.params.tipoProgrammi != undefined){
				$scope.tipoProgrammi =$rootScope.savedQuery.params.tipoProgrammi
			}
			
			if($rootScope.savedQuery.params.gruppoPrincipale != undefined){
				$scope.gruppo =$rootScope.savedQuery.params.gruppoPrincipale
			}
			
			if($rootScope.savedQuery.params.numeroPM != undefined){
				$scope.numeroPM =$rootScope.savedQuery.params.numeroPM
			}
//			if($rootScope.savedQuery.params.comuneLocale != undefined){
//				$scope.comuneLocale = $rootScope.savedQuery.params.comuneLocale
//			}
			if($rootScope.savedQuery.page != undefined){
				$scope.ctrl.page = $rootScope.savedQuery.page
			}
			
			if($rootScope.savedQuery.totRecords != undefined){
				$scope.totRecords = $rootScope.savedQuery.totRecords
			}
			
			if($rootScope.savedQuery.pageTot != undefined){
				$scope.pageTot = $rootScope.savedQuery.pageTot
			}
			$scope.getSampling(false);
		}else{
			$rootScope.savedQuery = undefined;
		}
	}
	
	
}])