codmanApp.controller('PerformingCodificaManualeBaseCtrl',
	[ '$scope', 'PerformingCodificaManualeBaseService', '$rootScope', '$timeout','ngDialog', '$interval', 'UtilService',
	function($scope, CMService, $rootScope, $timeout,ngDialog,$interval,UtilService) {
	$scope.user = angular.element("#headerLinksBig").text().trim();
	
	$scope.selezionato = 0;
	
	$scope.index = 0;
	
	$scope.conferma = false;

	$scope.warningTimer = false;
	
	$scope.sessioneScaduta = false;
	
	$scope.firstDialogEnter = true;

	$scope.resetTimer = function(){
		$scope.warningTimer = false;
		CMService.resetTimer($scope.user).then(function(response){
			var milliseconds = response.data;
			var secondsRound = Math.round(milliseconds.sessionTimeResidual/1000);
			var secondsFloor = Math.floor(milliseconds.sessionTimeResidual/1000);
			var minutes = Math.floor(secondsFloor/60);
			var seconds = secondsFloor - (minutes * 60);
			$scope.startTimer(minutes,seconds)
		},function(response){
			CMService.showError("Errore","Non è stato possibile aggiornare il timer")
		})
	};
	
	$scope.getTimerSessione = function(user){
		CMService.getTimerSessione(user).then(function(response){
			var milliseconds = response.data;
			var secondsRound = Math.round(milliseconds.sessionTimeResidual/1000);
			var secondsFloor = Math.floor(milliseconds.sessionTimeResidual/1000);
			var minutes = Math.floor(secondsFloor/60);
			var seconds = secondsFloor - (minutes * 60);
			$scope.startTimer(minutes,seconds)
		},function(response){
			CMService.showError("Errore","Non è stato possibile aggiornare il timer")
		})
	};
	
	$scope.startTimer = function(minutes,seconds){
			
			if (angular.isDefined($scope.start)) {
	            $interval.cancel($scope.start);
	        }
			
			$scope.minutes = minutes;
			$scope.seconds = seconds;
			
			$scope.start = $interval(function(){
				if($scope.seconds > 0){
					$scope.seconds = $scope.seconds - 1
				}else{
					$scope.minutes = $scope.minutes - 1;
					$scope.seconds = 59
				}
				
				if($scope.minutes == 1){
					$scope.warningTimer = true
				}
				
				if($scope.minutes == 0 && $scope.seconds == 0){
					$scope.warningTimer = false;
					$scope.sessioneScaduta = false;
					$scope.stopTimer()
				}
			},1000);
			
			$scope.stopTimer = function(){
				$interval.cancel($scope.start);
			}
	};
	
	$scope.deleteElements = function(combana){
		delete combana.statApprov;
		delete combana.codApprov;
		delete combana.gradoConf
	};
	
	$rootScope.$on('keypress', function (e, a) {
		if($scope.utilizzazioni.length > 0){
			$scope.$apply(function () {
		    	if($scope.firstDialogEnter){
		    		if(a.key == "1"){
			        	$scope.changeValueFromEvent(0)
			        }
			        if(a.key == "2"){
			        	$scope.changeValueFromEvent(1)
			        }
			        if(a.key == "3"){
			        	$scope.changeValueFromEvent(2)
			        }
			        if(a.key == "4"){
			        	$scope.changeValueFromEvent(3)
			        }
			        if(a.key == "5"){
			        	$scope.changeValueFromEvent(4)
			        }
			        if(a.key == "Enter"){
			        	if($scope.firstDialogEnter){
			        		if($scope.index < $scope.utilizzazioni.length-1){
				        		$scope.skippa($scope.utilizzazioni[$scope.index],e)
			        		}else{
			        			if(!$scope.conferma){
				        			$scope.sceltaProposte($scope.utilizzazioni,$scope.user)
			        			}else{
									CMService.askConfirm("Attenzione","Sei sicuro di aver identificato il giusto abbinamento?")
									.closePromise.then(function(model){
										if(model.value == true){
											CMService.sceltaProposte(opereAccettate,user).then(function(response){
												CMService.showError("Successo","L'abbinamento della proposta con le combane è avvenuto con successo")
												.closePromise.then(function(model){
													$scope.ctrl.rScope.init()
												})
											},function(response){
												if(response.data == 520){
													CMService.showError("Errore","Sessione scaduta, per continuare ricarica la pagina");
													return
												}
												CMService.showError("Errore","Non è stato possibile associare la proposta alla combana")
											})
										}
									})
								}
			        		}
			        	}
			        }
			        if(a.key == " "){
				        	if($scope.firstDialogEnter){
				        		if($scope.index > 0){
				        			$scope.tornaIndietro(e);
				        		}
				        	}
			        }
		    	}
		    });
		}
	});
	
	$scope.changeValueFromEvent = function(selezionato){
		if($scope.utilizzazioni[$scope.index].candidate[selezionato] != undefined){
			$scope.utilizzazioni[$scope.index].gradoConf = $scope.utilizzazioni[$scope.index].candidate[selezionato].gradoDiConf;
			$scope.utilizzazioni[$scope.index].codApprov = $scope.utilizzazioni[$scope.index].candidate[selezionato].codiceOpera;
			if(selezionato == 0){
				$scope.utilizzazioni[$scope.index].statApprov = 'P'
			}else{
				$scope.utilizzazioni[$scope.index].statApprov = 'A'
			}
		}
	};
	
	$scope.skippa = function(utilizzazione,event){
		if(event.pageX==0&&event.pageY==0&&event.clientX==0&&event.clientY==0&&event.screenX==0&&event.screenY==0){
			return false;
		}
		$scope.resetTimer();
		if(utilizzazione.statApprov==null||utilizzazione.statApprov==undefined||utilizzazione.statApprov==''){
			utilizzazione.statApprov = 'S'
		}
		$scope.next()
	};
	
	$scope.next = function(){
		$scope.index = $scope.index + 1;
		return false;
	};
	
	$scope.tornaIndietro = function(event){
		if(event.pageX==0&&event.pageY==0&&event.clientX==0&&event.clientY==0&&event.screenX==0&&event.screenY==0){
			return false;
		}
		$scope.index = $scope.index - 1
	};
	
	$scope.sceltaProposte = function(opereAccettate,user){

		angular.forEach(opereAccettate,function(item){
			if(item.codApprov == undefined){
				item.statApprov = 'S'
			}
		});
		$context=$scope;
		$scope.conferma=true;
		ngDialog.open({
				template : 'pages/advance-payment/dialog-storicoUtilizzazioni.html',
				preCloseCallback:function(){
					$scope.conferma=false;
				},
				plain : false,
				width : '60%',
				controller : [ '$scope', 'ngDialog', '$rootScope', function($scope, ngDialog, $rootScope) {
					var ngDialogId = $scope.ngDialogId;
					$scope.ctrl = {
						username:user,
						opere:opereAccettate,
						ngDialogId : ngDialogId,
						rScope:$context
					};
					$scope.clear = function() {
						ngDialog.closeAll(true);
					};
					$scope.cancel = function() {
						CMService.askConfirm("Attenzione","Sei sicuro di aver identificato il giusto abbinamento?")
						.closePromise.then(function(model){
							if(model.value == true){
								CMService.sceltaProposte(opereAccettate,user).then(function(response){
									CMService.showError("Successo","L'abbinamento della proposta con le combane è avvenuto con successo")
									.closePromise.then(function(model){
										$scope.ctrl.rScope.init()
									})
								},function(response){
									if(response.data == 520){
										CMService.showError("Errore","Sessione scaduta, per continuare ricarica la pagina");
										return
									}
									CMService.showError("Errore","Non è stato possibile associare la proposta alla combana")
								})
							}
						})
					}
				}]
			})
		
	};
	

	
	$scope.init = function(){
		CMService.getUtilizzazioni($scope.user).then(function(response){
			$scope.getTimerSessione($scope.user);
			$scope.utilizzazioni = response.data.codifiche;
			$scope.numeroCandidate = response.data.nCandidate;
			$scope.index = 0;
			if($scope.utilizzazioni.length < 1){
				$scope.empty = true;
				CMService.showError("Errore","Non ci sono utilizzazioni da approvare")
			} else{
				$scope.empty = false
			}
		},function(response){
			CMService.showError("Errore","Non è stato possibile ottenere le utilizzazioni")
		})
	};
	
//	$scope.annulla = function(utilizzazione){
//		CMService.askConfirm("Attenzione","Sei sicuro di voler annullare l'approvazione?")
//		.closePromise.then(function(model){
//			if(model.value == true){
//				var object = angular.copy(utilizzazione)
//				delete object.codApprov
//				object.statApprov = 'R'
//				CMService.approvaOpera(object,$scope.user).then(function(response){
//					$scope.next()
//				},function(response){
//					CMService.showError("Errore","Non è stato possibile annullare l'approvazione")
//				})
//			}
//		})
//	}
	
}]);

codmanApp.service('PerformingCodificaManualeBaseService', ['ngDialog', '$http', function(ngDialog, $http){
	
	var service = this;
	
	service.getUtilizzazioni = function(user){
		return $http({
			method:'GET',
			url:'rest/performing/codifica/manuale/base/getCodificaManualeBase',
			params: {
				user: user
			}
		})
	};
	
	service.approvaOpera = function(object,user){
		return $http({
			method:'POST',
			url: 'rest/performing/codifica/manuale/approvaOpera',
			data: {
				codificaOpereDTO: object,
				username: user
			}
		})
	};

	service.getTimerSessione = function(user){
		return $http({
			method: 'GET',
			url: 'rest/performing/codifica/manuale/getTimerSessione',
			params: {
				user: user
			}
		})
	};
	service.resetTimer = function(user){
		return $http({
			method: 'GET',
			url: 'rest/performing/codifica/manuale/refreshSession',
			params: {user:user}
		})
	};
	service.askConfirm = function(title,msgs){
		return ngDialog.open({
			template : 'pages/advance-payment/dialog-confirm.html',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', '$rootScope', function($scope, ngDialog, $rootScope) {
				var ngDialogId = $scope.ngDialogId;
				$scope.ctrl = {
					title : title,
					message : msgs,
					ngDialogId : ngDialogId
				};
				$scope.cancel = function(ngDialogId) {
					ngDialog.close(ngDialogId);
				};
				$scope.save = function() {
					ngDialog.closeAll(true);
				};
				
				$rootScope.$on('keypress', function (e, a, key) {
				    $scope.$apply(function () {
				        if(a.key == "Enter"){
				        	$scope.save()
				        }
				    });
				})
			} ]
		})
	};
	
	service.sceltaProposte = function(opere,user){
		return $http({
			method:'POST',
			url: 'rest/performing/codifica/manuale/approvaOpera',
			data: {
				codificaOpereDTO: opere,
				username: user
			}
		}) 
	};
	
	
	service.showError = function(title,msgs){
		return ngDialog.open({
			template : 'pages/advance-payment/dialog-alert.html',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', '$rootScope', function($scope, ngDialog, $rootScope) {
				var ngDialogId = $scope.ngDialogId;
				$scope.ctrl = {
					title : title,
					message : msgs,
					ngDialogId : ngDialogId
				};
				$scope.cancel = function() {
					ngDialog.closeAll(true);
				};
				
				$rootScope.$on('keypress', function (e, a, key) {
				    $scope.$apply(function () {
				        if(a.key == "Enter"){
				        	$scope.cancel()
				        }
				    });
				})
			}]
		})
	}

}]);

codmanApp.directive('keypressEvents',function ($document, $rootScope) {
    return {
        restrict: 'A',
        link: function () {
            $document.bind('keypress', function (e) {
                $rootScope.$broadcast('keypress', e);
            });
        }
    }
});




