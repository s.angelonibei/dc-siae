codmanApp.controller('preparatoryActivitiesCtrl', ['$scope', '$route', '$interval', 'configService', '$routeParams', 'ngDialog', 'preparatoryActivitiesService', 'processesSearchResponse',
	function ($scope, $route, $interval, configService, $routeParams, ngDialog, preparatoryActivitiesService, processesSearchResponse) {

		$scope.ctrl = {
			processes: processesSearchResponse.data.map(process => { return { ...process, ...{ collapseHistory: true } } }),
			messages: {
				info: "",
				error: "",
				clear: function () {
					this.info = "";
					this.error = "";
				},
				infoMessage: function (message) {
					this.info = message;
					$('#rightPanel')[0].scrollIntoView(true);
					window.scrollBy(0, -100);
				},
				errorMessage: function (message) {
					this.error = message;
					$('#rightPanel')[0].scrollIntoView(true);
					window.scrollBy(0, -100);
				}
			}
		};

		$scope.expandExecutions = (process) => {
			if (!process.collapseHistory) {
				process.collapseHistory = true;
			} else {
				process.collapseHistory = false;
				$scope.getProcessExecutions(process);
			}
		};

		$scope.startProcesses = (process) => {
			preparatoryActivitiesService.startProcesses(
				[process]).then(
					function success(response) {
						$scope.ctrl.messages.infoMessage(response.data[0].outcome.message);
					},
					function error(error) {
						$scope.ctrl.messages.errorMessage(error.data);
					}
				);
		};
		$scope.startDocumentation = (process) => {
			preparatoryActivitiesService.startDocumentation().then(
				function success(response) {
					$scope.ctrl.messages.infoMessage('Aggiornamento documentazione SIAE avviato');
				},
				function error(error) {
					$scope.ctrl.messages.errorMessage(error.data);
				}
			);
		};

		$scope.startUcmrAdaDocumentation = (process) => {
			preparatoryActivitiesService.startUcmrAdaDocumentation().then(
				function success(response) {
					$scope.ctrl.messages.infoMessage('Aggiornamento documentazione UCMR-ADA avviato');
				},
				function error(error) {
					$scope.ctrl.messages.errorMessage(error.data);
				}
			);
		};
		$scope.startKb = (process) => {
			preparatoryActivitiesService.startKb().then(
				function success(response) {
					$scope.ctrl.messages.infoMessage('Aggiornamento KB avviato');
				},
				function error(error) {
					$scope.ctrl.messages.errorMessage(error.data);
				}
			);
		};

		$scope.getProcessExecutions = (process) => {
			preparatoryActivitiesService.getProcessExecutions(process).then(
				function success(response) {
					let index = $scope.ctrl.processes.findIndex(element => element.processID == process.processID);
					if (index != -1) {
						$scope.ctrl.processes[index] = { ...process, ...response.data };
					}
				},
				function error(error) {
					$scope.ctrl.messages.errorMessage(error.data);
				}
			);
		};

		$scope.startAllProcesses = () => {
			preparatoryActivitiesService.startProcesses(
				$scope.ctrl.processes).then(
					function success(response) {
						let message = '';
						response.data.forEach(el => {
							message = message + el.outcome.message +'\n';
						});
						$scope.ctrl.messages.infoMessage(message);
					},
					function error(error) {
						$scope.ctrl.messages.errorMessage(error.data);
					}
				);
		};

		$scope.periodSelection = () => {
			ngDialog.open({
				template: 'pages/preparatory-activities/update-schemi-riparto.html',
				width: '60%',
				scope: $scope,
				controller: ['$scope', 'ngDialog', '$http', function ($scope, ngDialog, $http) {
					$scope.start = () => {
						const split = $scope.period.split('-');
						const month = split[0];
						const year = split[1];
						$http({
							method: 'POST',
							url: 'rest/preparatory-activities/start-update-royalties',
							data: {
								year: year,
								month: month
							}
						}).then((response) => {
							$scope.ctrl.messages.infoMessage('Aggiornamento schemi riparto avviato');
							$scope.closeThisDialog();
						},(error) => {
							// $scope.ctrl.messages.errorMessage('Impossibile avviare, esistono DSR in esecuzione per il periodo selezionato');
							$scope.ctrl.messages.errorMessage(error.data);
							$scope.closeThisDialog();
						});
					}
				}]
			})
		}


	}]);

codmanApp.service('preparatoryActivitiesService', ['$http', '$q', function ($http, $q) {

	var service = this;

	service.searchProcesses = function () {
		return $http({
			method: 'POST',
			url: 'rest/preparatory-activities/search',
			data: {
				tag: 'preparatoryActities'
			},
			headers: { 'Content-Type': 'application/json' }
		});
	};

	service.startProcesses = function (processes) {
		return $http({
			method: 'POST',
			url: 'rest/preparatory-activities/start-processes',
			data: {processes: processes},
			headers: {'Content-Type': 'application/json'}
		});
	};

	service.startDocumentation = function () {
		return $http({
			method: 'POST',
			url: 'rest/preparatory-activities/start-update-siae-documentation',
			headers: {'Content-Type': 'application/json'}
		});
	};

	service.startUcmrAdaDocumentation = function () {
		return $http({
			method: 'POST',
			url: 'rest/preparatory-activities/start-update-ucmr-ada-documentation',
			headers: {'Content-Type': 'application/json'}
		});
	};

	service.startKb = function () {
		return $http({
			method: 'POST',
			url: 'rest/preparatory-activities/start-update-kb',
			headers: {'Content-Type': 'application/json'}
		});
	};
	
	service.getProcessExecutions = function (request) {
		return $http({
			method: 'POST',
			url: 'rest/preparatory-activities/process-executions',
			data: request,
			headers: { 'Content-Type': 'application/json' }
		});
	};

}
]);

codmanApp.filter('tableHeader', function () {
	return function (input) {
		const header_decode = {
			data_inizio: 'Data inizio elaborazione',
			startedAt: 'Data inizio elaborazione',
			data_fine: 'Data fine elaborazione',
			finishedAt: 'Data fine elaborazione',
			esito: 'Esito',
			status: 'Esito',
			periodo: 'Periodo',
			societa: 'Società',
			data: 'Data aggiornamento',
			service: 'Processo',
			id_lancio: 'Id esecuzione'
		};
		return header_decode[input]
	};
});

codmanApp.filter('columsDecoder', function () {
	return function (input) {
		const decode = {
			tis_dump_v2: 'Territori',
			ucmr_ada_dump_v2: 'UCMR-ADA',
			siae_dump_v2: 'SIAE',
			ipi_dump_v2: 'IPI',
			ingestion_codifica_manuale: 'Codifica manuale',
			index_updater: 'Indicizzazione KB',
			ingestion_ulisse_ipi_dump: 'Opere SIAE',
			ingestion_ucmr_ada: 'Opere UCMR-ADA',
			ingestion_isrc: 'Codici ISRC',
			codici_dump: 'Codici UUID'

		};
		return decode[input] ? decode[input] : input
	};
});

codmanApp.filter('groupBy', function () {
	return _.memoize(function (items, field) {
			return _.mapValues(_.groupBy(items, field),
				list => list.map(item => _.omit(item, field)))
		}
	);
});

