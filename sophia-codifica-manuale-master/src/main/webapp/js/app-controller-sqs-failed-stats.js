//(function(){
	
	/**
	 * SqsFailedStatsCtrl
	 * 
	 * @path /riconoscimento
	 */
	codmanApp.controller('SqsFailedStatsCtrl', ['$scope', 'ngDialog', '$routeParams', '$location', '$route', '$http', function($scope, ngDialog, $routeParams, $location, $route, $http) {

		$scope.ctrl = {
			navbarTab: 'sqsFailedStats',			
			sqsFailedStats: [ ]
		};		
		
		$scope.onRefresh = function() {
			rownum = 0;
			// get next song line by priority
			$http({
				method: 'GET',
				url: 'rest/sqs-failed/stats-date'
			}).then(function successCallback(response) {
				$scope.ctrl.sqsFailedStats = response.data;
			}, function errorCallback(response) {
				$scope.ctrl.sqsFailedStats = [ ];
				if (404 == response.status) { // 404 not found
					$scope.showNotFound(null);
				}
			});
		};
		
		$scope.showNotFound = function(event) {
			ngDialog.open({
				template: 'pages/sqs-failed/dialog-not-found.html',
				plain: false,
				width: '50%',
				controller: ['$scope', 'ngDialog', function($scope, ngDialog) {
					$scope.cancel = function() {
						ngDialog.closeAll(false);
					};
					$scope.save = function() {
						ngDialog.closeAll(true);
					};
				}]
			}).closePromise.then(function (data) {
				// no op
			});
		};
		
		$scope.showSearch = function(event, sqsFailedStat) {
			$location
				.path('/sqsFailedSearch')
				.search('receiveDate', sqsFailedStat.receiveDate)
				.search('queueName', sqsFailedStat.queueName);
		};
		
		$scope.onRefresh();

}]);
	
//})();

