codmanApp.controller('VisualizzazioneDettaglioIngestionCtrl', ['$scope', '$routeParams', '$route', '$http', '$location', 'ngDialog', '$rootScope', '$anchorScroll', 'configService', 'triplettaContext', 
	function ($scope, $routeParams, $route, $http, $location, ngDialog, $rootScope, $anchorScroll, configService, triplettaContext) {

		$scope.ctrl = {
			 pageTitle: `Monitoraggio Ingestion SAP`,
			 msIngestionApiUrl: `${$location.protocol()}://${$location.host()}:${$location.port()}/${configService.msIngestionApiPath}`
			};

		$scope.orderImage = function (name, param) {

			//get id of html element clicked
			var id = "#" + name;

			//get css class of that html element for change the image of arrows
			var classe = angular.element(id).attr("class");

			//set to original position all images of arrows, on each columns.
			var element1 = document.getElementsByClassName("glyphicon glyphicon-sort-by-attributes");
			var element2 = document.getElementsByClassName("glyphicon glyphicon-sort-by-attributes-alt");
			angular.element(element1).attr("class", "glyphicon glyphicon-sort");
			angular.element(element2).attr("class", "glyphicon glyphicon-sort");

			if (classe != "glyphicon glyphicon-sort-by-attributes" || classe == "glyphicon glyphicon-sort") {

				angular.element(id).attr("class", "glyphicon glyphicon-sort-by-attributes");

			} else {
				angular.element(id).attr("class", "glyphicon glyphicon-sort-by-attributes-alt");
			}

			$scope.order(param);

		}

		$scope.order = function (param) {
			if ($scope.orderBy == param.concat(" asc")) {
				$scope.orderBy = param.concat(" desc");
			} else {
				$scope.orderBy = param.concat(" asc");
			}
			$scope.getSampling();
		}

		$scope.goTo = function (url, param) {
			if (url != undefined && param != undefined) {
				$location.path(url + param);
			}
		};

		$scope.goToGestioneErrore = function (tripletta) {
			triplettaContext.mode = 'error';
			triplettaContext.sapData = JSON.parse(tripletta.recordSap);
			triplettaContext.history.push(triplettaContext.state);
			triplettaContext.state = (function stateIIFE() {
				let orderBy = $scope.orderBy;
				let page = $scope.ctrl.page;
				let filterTripletta = $scope.ctrl.filterTripletta;
				return {
					location : $location.path(),
					reset: function(scope) {
						scope.orderBy = orderBy;
						scope.ctrl.page = page;
						scope.ctrl.filterTripletta = filterTripletta;
					}
				};
			}());
			$location.path('/gestioneTripletta');
		};

		$scope.navigateToNextPage = function () {
			$scope.ctrl.page = $scope.ctrl.page + 1;
			$scope.getSampling();

		};

		$scope.navigateToPreviousPage = function () {
			$scope.ctrl.page = $scope.ctrl.page - 1;
			$scope.getSampling();

		};

		$scope.navigateToEndPage = function () {
			$scope.ctrl.page = $scope.ctrl.pageTot;
			$scope.getSampling();
		}

		$scope.showWarning = function (title, msgs) {
			var parentScope = $scope
			ngDialog.open({
				template: 'pages/advance-payment/dialog-warning.html',
				plain: false,
				width: '60%',
				controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
					$scope.ctrl = {
						title: title,
						message: msgs
					};
					$scope.cancel = function () {
						$rootScope.flag = false;
						ngDialog.closeAll(false);
					};
					$scope.call = function () {
						$rootScope.flag = true;
						parentScope.pageTot = parentScope.pageTotTemp;
						parentScope.totRecords = parentScope.totRecordsTemp;
						ngDialog.closeAll(true);
					};
				}]
			}).closePromise.then(function (data) {
				// no op
				if ($rootScope.flag == true) {
					//aggiungere orderBy
					$scope.getSampling(false);
				}
			});
		};

		$scope.showError = function (title, msgs) {
			ngDialog.open({
				template: 'pages/advance-payment/dialog-alert.html',
				plain: false,
				width: '60%',
				controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
					$scope.ctrl = {
						title: title,
						message: msgs
					};
					$scope.cancel = function () {
						ngDialog.closeAll(false);
					};
					$scope.save = function () {
						ngDialog.closeAll(true);
					};
				}]
			}).closePromise.then(function (data) {
			});
		};

		showAlert = function (title, message) {
			ngDialog.open({
				template: 'pages/advance-payment/dialog-alert.html',
				plain: false,
				width: '50%',
				controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
					var ngDialogId = $scope.ngDialogId;
					$scope.ctrl = {
						title: title,
						message: message,
						ngDialogId: ngDialogId
					};
					$scope.cancel = function (ngDialogId) {
						ngDialog.close(ngDialogId);
					};
				}]
			}).closePromise.then(function (data) {

			});
		};

		$scope.init = function () {
			if(triplettaContext.state.location && triplettaContext.state.location.startsWith('/ingestionDetail/')){
				triplettaContext.state.reset($scope);
				if (triplettaContext.history && triplettaContext.history.length > 0) {
					triplettaContext.state = triplettaContext.history[0];
					triplettaContext.history.shift();
				}
			}else {
				$scope.orderBy = 'tripletta asc';
				$scope.ctrl.page = 0;
			}
			$scope.idIngestion = $routeParams.idIngestion;
			$scope.getSampling();
		}
		
		$scope.filter = function () {
			$scope.ctrl.page = 0;
			$scope.getSampling();
		}

		$scope.getSampling = function () {

			let _params = {
				page: $scope.ctrl.page || 0,
				count: false,
				order: $scope.orderBy
			};

			if($scope.ctrl.filterTripletta){
				_params = {..._params, tripletta: $scope.ctrl.filterTripletta};
			}
		
			$http({
				method: 'GET',
				url: $scope.ctrl.msIngestionApiUrl + 'ingestion-files/' + $scope.idIngestion,
				params: {..._params}
			}).then(function(response){
				$scope.ctrl.pageTot  = Math.floor(response.data.count / 50);
				$scope.ctrl.totRecords = response.data.count;						
				$scope.ctrl.sampling = response.data.list;
				$scope.ctrl.nomeFile = response.data.nomeFile;
				$scope.ctrl.dataAggiornamento = response.data.dataAggiornamento;
			},function(response){
				$scope.showError("","Non risulta nessun dato per il file selezionato");
			})
		}

		$scope.downloadFileSap = function() {
			$http({
				method : 'GET',
				url : $scope.ctrl.msIngestionApiUrl + `ingestion-files/${$scope.idIngestion}/input-file`,
				headers: {
						'Content-Type': 'text/plain'
				}
			}).then(function (response) {
				var contentDisposition = response.headers('Content-Disposition');
				var filename = contentDisposition.split(';')[1].split('filename')[1].split('=')[1].trim();
				saveAs(new Blob([response.data],{type:"text/plain;charset=UTF-8"}), filename);
			}, function (response) {
				$scope.showAlert("Errore","Si è verificato un errore: file non disponibile")
			});
		};

		$scope.downloadFileCsv = function() {
			$http({
				method : 'GET',
				url : $scope.ctrl.msIngestionApiUrl + `ingestion-files/${$scope.idIngestion}/output-file`,
				headers: {
						'Content-Type': 'text/plain'
				},
				params : {
					order: $scope.orderBy
				}
			}).then(function (response) {
				var contentDisposition = response.headers('Content-Disposition');
				var filename = contentDisposition.split(';')[1].split('filename')[1].split('=')[1].trim();
				saveAs(new Blob([response.data],{type:"text/plain;charset=UTF-8"}), filename);
			}, function (response) {
				$scope.showAlert("Errore","Si è verificato un errore: file non disponibile")
			});
		};

		$scope.back = function () {
			$location.path('/monitoraggioIngestion');
		}

		$scope.$on('$locationChangeSuccess',function(evt, absNewUrl, absOldUrl) {
			if(absNewUrl && absNewUrl.split('#!/') && absNewUrl.split('#!/').length > 1 ) {
				if(!absNewUrl.split('#!/')[1].match(/[i|I]ngestion/g) && triplettaContext.mode!='error'){
					triplettaContext.empty();
				}
			}
		});

		$scope.getRecordSapTable = function(recordSap) {
			let header = ``;
			let content = '';
			for (const [key, value] of Object.entries(JSON.parse(recordSap))) {
				content = content + `<div>`;
				content = content + `<span>${key}:</span>`;
				content = content + `<span>${value}</span>`;
				content = content + `</div>`;
			}
			let footer = ``;
			return header+content+footer;
		}

		$scope.getPeriodo = function (periodo) {
			if (periodo.indexOf(' ') > -1) {
				return periodo.split(' ')[1];
			} else {
				return periodo;
			}
		}

		$scope.getFirstCanale = function (canale) {
			if (canale && canale.indexOf(',') > -1) {
				return canale.split(',')[0];
			} else {
				return canale;
			}
		}

		$scope.getListaCanali = function (canali) {
			let content = '';
			if(canali) {
			for (const canale of canali.split(',')) {
				content = content + `<div>`;
				content = content + `<span>${canale}</span>`;
				content = content + `</div>`;
			}
			}
			return content;
		}		

	}]);