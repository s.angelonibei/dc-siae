(function() {
  'use strict';
  angular
    .module('codmanApp')
    .controller('VisualizzazioneRiconciliazioneImportiCtrl', VisualizzazioneRiconciliazioneImportiCtrl);

  VisualizzazioneRiconciliazioneImportiCtrl.$inject =
      ['$scope','$location','$anchorScroll','$http', 'RiconciliazioneImportiService', 'ngDialog', 'preEventiPagatiService', 'UtilService'];

  function VisualizzazioneRiconciliazioneImportiCtrl($scope, $location,$anchorScroll,$http, RiconciliazioneImportiService, ngDialog, preEventiPagatiService, UtilService) {
    $scope.isValid = UtilService.isValid;
    $scope.params = {
      fatturaValidata: 'tutti',
      megaConcerto: 'tutti',
    };

    $scope.totaliRiconciliazione = {
      totaleLordoSophia : 0,
      totaleLordoNdm : 0,
      totaleNettoSophiaAggio : 0,
      totaleNettoNdmAggio : 0,
      totaleNettoSophia : 0
    };
    
    $scope.config = {
      agenzie: preEventiPagatiService[0],
      dateLimits: preEventiPagatiService[1],
      vociIncasso: preEventiPagatiService[2],
      sedi: preEventiPagatiService[3]
    };
    $scope.user = angular.element('#headerLinksBig').text().trim();

    $scope.updateDateValue = function(ngModel, value) {
      $scope.params[ngModel] = value;
    };

    $scope.exportExcel = function(){

      $location.hash('up');
      $anchorScroll();
      $location.hash('');

      $http({
        method: 'GET',
        url: 'rest/performing/cruscotti/riconciliazioneImportiListCSV',
        params: $scope.params
      }).then( function(response) {

        var linkElement = document.createElement('a');

        try {

          var blob = new Blob([response.data], { type: 'application/csv' });
          var url = window.URL.createObjectURL(blob);
          linkElement.setAttribute('href', url);
          linkElement.setAttribute("download", 'ListaRiconciliazioneImporti.csv');

          var clickEvent = new MouseEvent("click", {
            "view": window,
            "bubbles": true,
            "cancelable": false
          });
          linkElement.dispatchEvent(clickEvent);

        } catch (ex) {

        }
      }, function (response) {
        $scope.showError("Errore","Impossibile fare il download dell'excel.");
      });
    }

    $scope.checkParams = function(params) {
      if (!params.count) { params.count = true; }

      if (params.inizioPeriodoContabile && !checkDate(params.inizioPeriodoContabile, params.finePeriodoContabile)) {
        $scope.showError('Attenzione', "La data 'Contabilità a' non può essere inferiore alla data 'Contabilità da'");
        return;
      } else if (!params.inizioPeriodoContabile && params.finePeriodoContabile) {
        params.inizioPeriodoContabile = dayjs().subtract(7, 'year').format('MM-YYYY');
      }

      if (params.inizioPeriodoFattura && !checkDate(params.inizioPeriodoFattura, params.finePeriodoFattura)) {
        $scope.showError('Attenzione', "La data 'Fattura a' non può essere inferiore alla data 'Fattura da'");
        return;
      } else if (!params.finePeriodoContabile && params.inizioPeriodoContabile) {
        params.finePeriodoContabile = dayjs().format('MM-YYYY');
      }

      $scope.filterApply(params);
    };

    $scope.filterApply = function(params) {
      $scope.saveQuery = angular.copy(params);
      if ($scope.saveQuery.inizioPeriodoContabile && $scope.saveQuery.inizioPeriodoContabile.indexOf('-') >= 0) {
        $scope.saveQuery.inizioPeriodoContabile = $scope.saveQuery.inizioPeriodoContabile.split('-')[1] + $scope.saveQuery.inizioPeriodoContabile.split('-')[0];
      }
      if ($scope.saveQuery.finePeriodoContabile && $scope.saveQuery.finePeriodoContabile.indexOf('-') >= 0) {
        $scope.saveQuery.finePeriodoContabile = $scope.saveQuery.finePeriodoContabile.split('-')[1] + $scope.saveQuery.finePeriodoContabile.split('-')[0];
      }

      if ($scope.saveQuery.count === true) {
        RiconciliazioneImportiService.getSampling($scope.saveQuery).then(function(response) {
          $scope.saveQuery.count = false;
          $scope.totRecords = response.data.recordTotali;
          $scope.totaliRiconciliazione.totaleLordoSophia = response.data.totaleLordoSophia;
          $scope.totaliRiconciliazione.totaleLordoNdm = response.data.totaleLordoNdm;
          $scope.totaliRiconciliazione.totaleNettoSophiaAggio = response.data.totaleNettoSophiaAggio;
          $scope.totaliRiconciliazione.totaleNettoNdmAggio = response.data.totaleNettoNdmAggio;
          $scope.totaliRiconciliazione.totaleNettoSophia = response.data.totaleNettoSophia;
          $scope.pageTot = Math.floor(response.data.recordTotali / 50);
          if ($scope.totRecords === 0) {
            $scope.showError('Errore', 'Non risulta nessun dato per la ricerca effettuata');
            $scope.sampling = void 0;
            $scope.totRecords = void 0;
            $scope.pageTot = void 0;
          } else if ($scope.totRecords > 1000) {
            $scope.showWarning('Attenzione', 'Il risultato prevede ' + $scope.totRecords + ' record, è consigliabile applicare dei filtri aggiuntivi. Sicuro di voler continuare?');
          } else {
            $scope.filterApply($scope.saveQuery);
          }
        }).catch(function(response) {
          $scope.showError('Errore', 'Non è stato possibile ottenere i dati dal server');
          $scope.sampling = void 0;
          $scope.totRecords = void 0;
          $scope.pageTot = void 0;
        });
      } else if (!$scope.saveQuery.count) {
        RiconciliazioneImportiService.getSampling($scope.saveQuery).then(function(response) {
          $scope.sampling = response.data;
          $scope.page = $scope.saveQuery.page ? $scope.saveQuery.page : 0;
          $scope.saveQuery.page = $scope.page;
        }).catch(function(response) {
          $scope.showError('Errore', 'Non è stato possibile ottenere dei dati dal server');
          $scope.sampling = void 0;
          $scope.totRecords = void 0;
          $scope.pageTot = void 0;
        });
      }
    };

    $scope.orderImage = function(name, param) {
      if (!$scope.saveQuery) {
        return;
      }
      // get id of html element clicked
      var id = '#' + name;

      // get css class of that html element for change the image of arrows
      var classe = angular.element(id).attr('class');

      // set to original position all images of arrows, on each columns.
      var element1 = document.getElementsByClassName('glyphicon glyphicon-sort-by-attributes');
      var element2 = document.getElementsByClassName('glyphicon glyphicon-sort-by-attributes-alt');
      angular.element(element1).attr('class', 'glyphicon glyphicon-sort');
      angular.element(element2).attr('class', 'glyphicon glyphicon-sort');

      if (classe !== 'glyphicon glyphicon-sort-by-attributes' || classe === 'glyphicon glyphicon-sort') {
        angular.element(id).attr('class', 'glyphicon glyphicon-sort-by-attributes');
      } else {
        angular.element(id).attr('class', 'glyphicon glyphicon-sort-by-attributes-alt');
      }

      $scope.order(param);
    };

    $scope.order = function(param) {
      if ($scope.orderBy === param.concat(' asc')) {
        $scope.orderBy = param.concat(' desc');
      } else {
        $scope.orderBy = param.concat(' asc');
      }
      $scope.saveQuery.order = $scope.orderBy;
      $scope.filterApply($scope.saveQuery);
    };

    $scope.navigateToStartPage = function() {
      $scope.saveQuery.page = 0;
      $scope.filterApply($scope.saveQuery);
    };

    $scope.navigateToNextPage = function() {
      $scope.saveQuery.page++;
      $scope.filterApply($scope.saveQuery);
    };

    $scope.navigateToPreviousPage = function() {
      $scope.saveQuery.page--;
      $scope.filterApply($scope.saveQuery);
    };

    $scope.navigateToEndPage = function() {
      $scope.saveQuery.page = $scope.pageTot;
      $scope.filterApply($scope.saveQuery);
    };

    // NGDIALOG QUI SOTTO

    $scope.showWarning = function(title, msgs) {
      var parentScope = $scope;
      ngDialog.open({
        template: 'pages/advance-payment/dialog-warning.html',
        plain: false,
        width: '60%',
        controller: ['$scope', 'ngDialog', function(scope, dialog) {
          scope.ctrl = {
            title: title,
            message: msgs
          };
          scope.cancel = function() {
            dialog.closeAll(false);
          };
          scope.call = function() {
            parentScope.filterApply(parentScope.saveQuery);
            dialog.closeAll(false);
          };
        }]
      }).closePromise.then(function(data) {

      });
    };

    $scope.goTo = function(url, param) {
      if (url != undefined && param != undefined) {
        $location.path(url + param);
      }
    };

    $scope.showError = function(title, msgs) {
      ngDialog.open({
        template: 'pages/advance-payment/dialog-alert.html',
        plain: false,
        width: '60%',
        controller: ['$scope', 'ngDialog', function(scope, dialog) {
          scope.ctrl = {
            title: title,
            message: msgs
          };
          scope.cancel = function() {
            dialog.closeAll(false);
          };
          scope.save = function() {
            dialog.closeAll(true);
          };
        }]
      });
    };

    // FINE NGDIALOG

    function checkDate(_from, to) {
      var comparisonFrom = dayjs(_from);
      var comparisonTo = dayjs(to);

      if (comparisonFrom.isValid()) {
        return comparisonTo.isSame(comparisonFrom) || comparisonTo.isAfter(comparisonFrom);
      }
      if (_from.length > 8) {
        comparisonFrom = _from ? dayjs(_from.split('-')[2] + _from.split('-')[1] + _from.split('-')[0]) : dayjs().subtract(7, 'year');
        comparisonTo = to ? dayjs(to.split('-')[2] + to.split('-')[1] + to.split('-')[0]) : dayjs();

        return comparisonTo.isSame(comparisonFrom) || comparisonTo.isAfter(comparisonFrom);
      }

      comparisonFrom = _from ? dayjs(_from.split('-')[1] + _from.split('-')[0]) : dayjs().subtract(7, 'year');
      comparisonTo = to ? dayjs(to.split('-')[1] + to.split('-')[0]) : dayjs();// to.split('-')[1] + to.split('-')[0];

      return comparisonTo.isSame(comparisonFrom) || comparisonTo.isAfter(comparisonFrom);// comparisonTo >= comparisonFrom;
    }
  }
})();

codmanApp.service('RiconciliazioneImportiService', function($http) {
  this.getSampling = function(params) {
    return $http({
      method: 'GET',
      url: 'rest/performing/cruscotti/riconciliazioneImportiList',
      params: params
    });
  };
});
