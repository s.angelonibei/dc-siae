/**
 * DsrMetadataCtrl
 * 
 * @path /dsrMetadata
 */
codmanApp.controller('DsrMetadataCtrl', ['$scope','$route', 'ngDialog', '$routeParams', '$location', '$http', function($scope,$route, ngDialog, $routeParams, $location, $http) {

	$scope.ctrl = {
		navbarTab: 'dsrMetadataConfig',
		config: [],
		filterParameters: [],
		execute: $routeParams.execute,
		hideForm: $routeParams.hideForm,
		maxrows: 20,
		first: $routeParams.first||0,
		last: $routeParams.last||20,
		sortType: 'priority',
		sortReverse: true,
		filter: $routeParams.filter,
		selectedDsp : $routeParams.dsp||'*',
		selectedUtilization : $routeParams.utilization||'*',
		selectedOffer : $routeParams.offer||'*'
	};		
			
	$scope.onAzzera = function() {
		$scope.ctrl.execute = false;
		$scope.ctrl.first = 0;
		$scope.ctrl.last = $scope.ctrl.maxrows;
		};
		
	$scope.navigateToNextPage = function() {
		$location.search('execute', 'true')
		.search('hideForm', $scope.ctrl.hideForm)
		.search('first', $scope.ctrl.last)
		.search('last', $scope.ctrl.last + $scope.ctrl.maxrows);
	};
	
	$scope.navigateToPreviousPage = function() {
		$location.search('execute', 'true')
			.search('hideForm', $scope.ctrl.hideForm)
			.search('first', Math.max($scope.ctrl.first - $scope.ctrl.maxrows, 0))
			.search('last', $scope.ctrl.first);
	};
	

	// Funzione principale richiamata al caricamento della pagina
	$scope.onRefresh = function() {
		// get All DSP Configuration


		if ($scope.ctrl.execute) {
			$scope.getPagedResults($scope.ctrl.first,
					$scope.ctrl.last, 		
					$scope.ctrl.selectedDsp,
					$scope.ctrl.selectedUtilization,
					$scope.ctrl.selectedOffer);
		} else {
			$scope.getPagedResults(0,
					$scope.ctrl.maxrows);
		}
		

	
	};
	
	$scope.getPagedResults = function(first, last, dsp, utilization, offer) { 
		$http({
			method: 'GET',
			url: 'rest/dsrMetadata/all',
			params: {
				first: first,
				last: last, 
				dsp : (typeof dsp !== 'undefined' ? dsp : ""),
				utilization : (typeof utilization !== 'undefined' ? utilization : ""),
				offer : (typeof offer !== 'undefined' ? offer : "")
			}
		}).then(function successCallback(response) {
			$scope.ctrl.config = response.data;
			$scope.ctrl.first = response.data.first;
			$scope.ctrl.last = response.data.last;
		}, function errorCallback(response) {
			$scope.ctrl.dspConfig = [];
		});

	};
	

	
	//funzione che renderizza il popup per l'aggiunta di una nuova configurazione 
	$scope.showNewConfig = function(event) {
		ngDialog.open({
			template: 'pages/dsr-metadata/dialog-new.html',
			plain: false,
			width: '60%',
			controller: ['$scope', 'ngDialog', '$http', '$q', function($scope, ngDialog, $http, $q) {
				
				$scope.ctrl = {
						dspList: [],
						commercialOffersList:[],
						periodTypes: [],
						countryTypes: [],
						timeStampTypes:[],
						cardinalityTypes:[],
						components: ['STRINGA','PERIODO','TERRITORIO', 'TIMESTAMP', 'NUMERO REPORT'],
						periodAlreadySet: false,
						countryAlreadySet: false,
						timeStampAlreadySet : false,
						cardinalityAlreadySet : false,
						jsonRegExConfig: []
					};
					
				
				
				var promise1 = $http({method: 'GET', url: 'rest/data/dspUtilizationsOffers'}); 
				var promise2 = $http({method: 'GET', url: 'rest/dsrMetadata/configurationValues'}); 
				$q.all([promise1, promise2]).then(function successCallback(response) {	
					

					$scope.ctrl.dspList = response[0].data.dsp;
					$scope.ctrl.utilizationList = response[0].data.utilizations;
					$scope.ctrl.commercialOffersList = response[0].data.commercialOffers;
					$scope.ctrl.periodTypes = response[1].data.periodTypes;
					$scope.ctrl.countryTypes = response[1].data.countryTypes;
					$scope.ctrl.timeStampTypes = response[1].data.timeStampTypes;
					$scope.ctrl.cardinalityTypes = response[1].data.cardinalityTypes;
					}, function errorCallback(response) {
						$scope.ctrl.dspList = [];
						$scope.ctrl.commercialOffersList = [];
						$scope.ctrl.periodTypes = [];
						$scope.ctrl.countryTypes = [];
						$scope.ctrl.timeStampTypes = [];
						$scope.ctrl.cardinalityTypes = [];
					}); 
				
				  
				$scope.filterUtilizationByDsp = function () {
				    return function (item) {
				    	if((typeof $scope.selectedDsp === 'undefined'))
				    		return false;
				    	
				      	
				    	for (var i = 0; i < $scope.ctrl.commercialOffersList.length; i++) { 
				    		  if ($scope.ctrl.commercialOffersList[i].idDSP == $scope.selectedDsp.idDsp &&
				    				  item.idUtilizationType == $scope.ctrl.commercialOffersList[i].idUtilizationType)
						        {   
						            return true;
						        }
				    	}

				    	 return false;
				      
				    };
				};
				
				$scope.filterOffersByDspAndUtilization = function () {
				    return function (item) {
				    	if( (typeof $scope.selectedDsp === 'undefined') ||  (typeof $scope.selectedUtilization === 'undefined'))
				    		return false;
				   
				    		
				    	
				    	for (var i = 0; i < $scope.ctrl.commercialOffersList.length; i++) { 
				    		  if (item.idDSP == $scope.selectedDsp.idDsp && item.idUtilizationType == $scope.selectedUtilization.idUtilizationType)
						        {   
						            return true;
						        }
						        return false;
				    	}
				      
				    };
				};
				
				
				$scope.addString = function(){
					if($scope.generatedExpression!== undefined){
						
						$scope.generatedExpression = $scope.generatedExpression + $scope.insertedString;
						$scope.expressionStringHtml =  $scope.expressionStringHtml + '<b style="color:blue">' +  $scope.insertedString + ' </b>'
					} else{
						$scope.generatedExpression = $scope.insertedString;
						$scope.expressionStringHtml = '<b style="color:blue">' + $scope.insertedString + ' </b>'
					}
					
					$scope.ctrl.jsonRegExConfig.push({'stringa' : $scope.insertedString});
					$scope.selectedParameter = '';
					$scope.insertedString = '';
				}
				
				$scope.addPeriod = function(){
					if($scope.ctrl.periodAlreadySet === false){						
						
						if($scope.generatedExpression!== undefined){	
							$scope.generatedExpression = $scope.generatedExpression + $scope.insertedPeriod.code;
							$scope.expressionStringHtml =  $scope.expressionStringHtml + '<b style="color:magenta">' +  $scope.insertedPeriod.code + ' </b>'
						} else{
							$scope.generatedExpression = $scope.insertedPeriod.code;
							$scope.expressionStringHtml = '<b style="color:magenta">' + $scope.insertedPeriod.code + ' </b>'
							
						}
						$scope.ctrl.jsonRegExConfig.push({'periodo' : $scope.insertedPeriod.code});
						$scope.ctrl.periodAlreadySet = true;
						
					}else{									 
						$scope.openErrorDialog("Formato del periodo già inserito");
					}
					
					$scope.selectedParameter = '';
					$scope.insertedPeriod = undefined;
				}
		
				
				$scope.addCountry = function(){
					if($scope.ctrl.countryAlreadySet === false){						
						
						if($scope.generatedExpression!== undefined){	
							$scope.generatedExpression = $scope.generatedExpression + $scope.insertedCountry.code;
							$scope.expressionStringHtml =  $scope.expressionStringHtml + '<b style="color:orange">' +  $scope.insertedCountry.code+ ' </b>'
						} else{
							$scope.generatedExpression = $scope.insertedCountry.code ;
							$scope.expressionStringHtml = '<b style="color:orange">' + $scope.insertedCountry.code + ' </b>'
						}
						$scope.ctrl.jsonRegExConfig.push({'territorio' : $scope.insertedCountry.code});
						$scope.ctrl.countryAlreadySet = true;
						
					}else{									 
						$scope.openErrorDialog("Formato del territorio già inserito");
					}
					
					$scope.selectedParameter = '';
					$scope.insertedCountry = undefined;
				}
	
				$scope.addCardinality = function(){
					if($scope.ctrl.cardinalityAlreadySet === false){						
						
						if($scope.generatedExpression!== undefined){	
							$scope.generatedExpression = $scope.generatedExpression + $scope.insertedCardinality.code;
							$scope.expressionStringHtml =  $scope.expressionStringHtml + '<b style="color:black">' +  $scope.insertedCardinality.code+ ' </b>'
						} else{
							$scope.generatedExpression = $scope.insertedCardinality.code ;
							$scope.expressionStringHtml = '<b style="color:black">' + $scope.insertedCardinality.code + ' </b>'
						}
						$scope.ctrl.jsonRegExConfig.push({'numberof' : $scope.insertedCardinality.code});
						$scope.ctrl.cardinalityAlreadySet = true;
						
					}else{									 
						$scope.openErrorDialog("Formato  già inserito");
					}
					
					$scope.selectedParameter = '';
					$scope.insertedCardinality = undefined;
				}
				
				$scope.addTimeStamp = function(){
					if($scope.ctrl.timeStampAlreadySet === false){						
						
						if($scope.generatedExpression!== undefined){	
							$scope.generatedExpression = $scope.generatedExpression + $scope.insertedTimeStamp.code;
							$scope.expressionStringHtml =  $scope.expressionStringHtml + '<b style="color:cyan">' +  $scope.insertedTimeStamp.code + ' </b>'
						} else{
							$scope.generatedExpression = $scope.insertedTimeStamp.code ;
							$scope.expressionStringHtml = '<b style="color:cyan">' + $scope.insertedTimeStamp.code + ' </b>'
						}
						$scope.ctrl.jsonRegExConfig.push({'timestamp' : $scope.insertedTimeStamp.code});
						$scope.ctrl.timeStampAlreadySet = true;
						
					}else{									 
						$scope.openErrorDialog("Formato del timestamp già inserito");
					}
					
					$scope.selectedParameter = '';
					$scope.insertedTimeStamp = undefined;
				}
				
				$scope.invalid = function(){
					if($scope.ctrl.periodAlreadySet === true && $scope.ctrl.countryAlreadySet === true){
						return false;
					}
					 return true;
				}
				
			    $scope.openErrorDialog = function (errorMsg) {
			        ngDialog.open({ 
			        	template: 'errorDialogId',
			        	plain: false,
						width: '40%',
			        	data: errorMsg
			        	
			        });
			    };
				
			    $scope.openConfirmationDialog = function (msg) {
			        ngDialog.open({ 
			        	template: 'confirmationDialogId',
			        	plain: false,
						width: '40%',
			        	data: msg
			        	
			        });
			    };
			    
				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				
							
				$scope.save = function() {
					
					$http({
						method: 'GET',
						url: 'rest/dsrMetadata/checkCommercialOfferAlreadySet',
						params: {
							idCommercialOffer: $scope.selectedOffer.idCommercialOffering						}
					}).then(function successCallback(response) {
						if(response.data===true){
					           ngDialog.openConfirm({
				                    template:
				                    	'<div>' + 
								   		'<strong>Configurazione già inserita per l\'offerta commerciale selezionata. Procedere comunque?</strong>' +
								   	'</div>' +
								   	'<div style="margin-top: 15px">' +
								   		'<table width="100%">' +
								   			'<tr>' +
								   				'<td>' +
								   					'<button type="button" class="btn addButton" ng-click="closeThisDialog()"><strong>No</strong></button>' + 
								   				'</td>' +
								   				'<td align="right">' +
								   					'<button class="btn addButton" ng-click="confirm()"> <strong>Si</strong></button>' +
								   				'</td>' +
								   			'</tr>' +
								   		'</table>' +
								   	'</div>',
				                    plain: true,
				                    className: 'ngdialog-theme-default'
				                }).then(function (value) {
				                    
				                	$scope.saveData();
				                }, function (value) {
				                    $scope.cancel();
				                });
						
							
						
					    
						}else{
							$scope.saveData();
						}
						
					}, function errorCallback(response) {
						$scope.openErrorDialog(response.statusText);
					});

					
				};
				
				$scope.saveData = function(){
					$http({
						method: 'POST',
						url: 'rest/dsrMetadata',
						data: {
							commercialOfferId: $scope.selectedOffer.idCommercialOffering,
							idDsp: $scope.selectedOffer.idDSP,
							regexString: $scope.generatedExpression,
							regexStringValues: JSON.stringify($scope.ctrl.jsonRegExConfig)
						}
					}).then(function successCallback(response) {
						ngDialog.closeAll(true);
						$route.reload();
					}, function errorCallback(response) {
						if(response.status == 409){
							$scope.openErrorDialog("Conflitto individuato per la configurazione: " + $scope.generatedExpression);	
						}else if(response.status == 403){
							response.status == $scope.openErrorDialog("Configurazione già presente");
						}else{
							$scope.openErrorDialog(response.statusText);	
						}
						
					});
				}
				
				
				
				
			}]
		}).closePromise.then(function (data) {
			if (data.value === true) {
				// no op
			}
		});
	};
	
	
	$scope.showDeleteConfig = function(event, configToRemove) {
		ngDialog.open({
			template: 'pages/dsr-metadata/dialog-delete.html',
			plain: false,
			width: '60%',
			controller: ['$scope', 'ngDialog', '$http', function($scope, ngDialog, $http) {
				$scope.ctrl = {
					configToRemove: configToRemove
				};
				

				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				

				$scope.deleteConfig = function() {
					$http({
						method: 'DELETE',
						url: 'rest/dsrMetadata',
						headers: {
				            'Content-Type': 'application/json'},
				        params: {
							idDsrMetadataConfig: $scope.ctrl.configToRemove.idDsrMetadataConfig    
						}
					
					}).then(function successCallback(response) {
						ngDialog.closeAll(true);
						$route.reload();
					}, function errorCallback(response) {
						$scope.openErrorDialog(response.statusText);
					});
					
				};
				
			    $scope.openErrorDialog = function (errorMsg) {
			        ngDialog.open({ 
			        	template: 'errorDialogId',
			        	plain: false,
						width: '40%',
			        	data: errorMsg
			        	
			        });
			    };
			}]
		}).closePromise.then(function (data) {
			if (data.value === true) {
				// no op
			}
		});
	};
	
		
	
	$scope.showEditConfig = function(event, configToModify) {
		ngDialog.open({
			template: 'pages/dsr-metadata/dialog-edit.html',
			plain: false,
			width: '60%',
			controller: ['$scope', 'ngDialog', '$http', function($scope, ngDialog, $http) {
				$scope.ctrl = {
					configToModify: configToModify,
					parameterList1: ['*']
				};
				

				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				
		
				$scope.editConfig = function() {
				
					$http({
						method: 'PUT',
						url: 'rest/dsrMetadata',
						headers: {
				            'Content-Type': 'application/json'},
						data: {
							
						}
					
					}).then(function successCallback(response) {
						ngDialog.closeAll(true);
						$route.reload();
					}, function errorCallback(response) {
						$scope.openErrorDialog(response.statusText);
					});
					
				};
				
			    $scope.openErrorDialog = function (errorMsg) {
			        ngDialog.open({ 
			        	template: 'errorDialogId',
			        	plain: false,
						width: '40%',
			        	data: errorMsg
			        	
			        });
			    };
			}]
		}).closePromise.then(function (data) {
			if (data.value === true) {
				// no op
			}
		});
	};
	
	 $scope.$on('filterApply', function(event, parameters) { 
		 $scope.ctrl.filterParameters = parameters;
			$location.search('execute', 'true')
			.search('hideForm', false)
			.search('first', 0)
			.search('last', $scope.ctrl.maxrows)
			.search('dsp', parameters.dsp)
			.search('utilization',parameters.utilization)
			.search('offer', parameters.offer);	 
			$scope.ctrl.hideForm = false;
		
		 });
	
	$scope.onRefresh();
	
}]);

//html filter (render text as html)
codmanApp.filter('html', ['$sce', function ($sce) { 
    return function (text) {
        return $sce.trustAsHtml(text);
    };    
}])
