/**
 * HomeCtrl
 * 
 * @path /home
 */
codmanApp.controller('HomeCtrl', ['$scope', '$location', '$routeParams',  function($scope, $location, $routeParams) {

	$scope.navigateTo = function(path) {
		$location.path(path);
	};

}]);