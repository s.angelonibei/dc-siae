//(function(){

	
	/**
	 * SqsHostnamesCtrl
	 * 
	 * @path /sqsHostnames
	 */
	codmanApp.controller('SqsHostnamesCtrl', ['$scope', 'ngDialog', '$routeParams', '$location', '$http', 'configService',
	                                                function($scope, ngDialog, $routeParams, $location, $http, configService) {

		$scope.ctrl = {
			navbarTab: 'sqsHostnames',
			timeZone: configService.timeZone||'Europe/Rome',
			sortType: 'insertTime',
			sortReverse: true,
			hostnames: []
		};

		$scope.setSortType = function(sortType) {
			if (sortType == $scope.ctrl.sortType) {
				$scope.ctrl.sortReverse = !$scope.ctrl.sortReverse;
			} else {
				$scope.ctrl.sortType = sortType;
			}
			return true;
		};
		
		$scope.showSearch = function(event, item) {
			$location
				.path('/serviceBusSearch')
				.search('insertTime', null)
				.search('sortReverse', null)
				.search('idDsr', null)
				.search('first', null)
				.search('last', null)
				.search('hostname', item.hostname);
		};
		
		$scope.elapsedTime = function(startDate) {
			return elapsedTime(startDate);
		};

		$scope.showKillEmrStep = function(clusterId, applicationId) {
			ngDialog.open({
				template: 'pages/dsr-progress/dialog-kill-emr-step.html',
				plain: false,
				width: '60%',
				controller: ['$scope', 'ngDialog', function($scope, ngDialog) {
					$scope.ctrl = {
						clusterId: clusterId,
						applicationId: applicationId,
						waitingResponse: false,
						exitValue: null,
						stdout: null,
						stderr: null
					};
					$scope.cancel = function() {
						ngDialog.closeAll(false);
					};
					$scope.save = function() {
						$scope.ctrl.waitingResponse = true;
						$scope.ctrl.exitValue = null;
						$scope.ctrl.stdout = null;
						$scope.ctrl.stderr = null;
						$http({
							method: 'POST',
							url: 'rest/aws/kill-emr-step',
							params: {
								clusterId: $scope.ctrl.clusterId,
								applicationId: $scope.ctrl.applicationId
							}
						}).then(function successCallback(response) {
							$scope.ctrl.waitingResponse = false;
							if (response.data) {
								$scope.ctrl.exitValue = response.data.exitValue;
								$scope.ctrl.stdout = response.data.stdout;
								$scope.ctrl.stderr = response.data.stderr;
							}
//							ngDialog.closeAll(true);
						}, function errorCallback(response) {
							$scope.ctrl.waitingResponse = false;
							$scope.ctrl.exitValue = null;
							$scope.ctrl.stdout = null;
							$scope.ctrl.stderr = null;
//							ngDialog.closeAll(false);
						});
					};
				}]
			}).closePromise.then(function (data) {
				if (data.value === true) {
					
				}
			});
		};
		
		$http({
			method: 'GET',
			url: 'rest/dsr-steps-monitoring/hostnames',
			params: {
				activeOnly: 'false'
			}
		}).then(function successCallback(response) {
			$scope.ctrl.hostnames = response.data;
		}, function errorCallback(response) {
			$scope.ctrl.hostnames = [];
		});

	}]);
	
//})();

