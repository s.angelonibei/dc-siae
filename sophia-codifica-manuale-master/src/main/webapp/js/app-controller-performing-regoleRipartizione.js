codmanApp.controller("RegoleRipartizioneCtrl", ["$scope", "RegoleRipartizioneService", "ngDialog", function ($scope, RRService, ngDialog) {

    $scope.user = angular.element("#headerLinksBig").text().trim();

    $scope.criterio = {
        ripartizione: {
            esclusioniUtilizzazioni: {
                durataMinima: 30
            }
        },
        valorizzazione: {}
    };

    $scope.ctrl = {};

    $scope.ricerca = {};

    RRService.getVociIncasso().then(function (response) {
        $scope.vociIncasso = response.data
    }, function (response) {
        RRService.showError("Errore", "Non è stato possibile ottenere la lista delle voci incasso")
    });


    RRService.getVociIncassoFittizie().then(function (response) {
        $scope.vociIncassoFittizie = response.data
    }, function (response) {
        RRService.showError("Errore", "Non è stato possibile ottenere la lista delle voci incasso")
    });

    RRService.getPeriodiRipartizione().then(function (response) {
        $scope.periodiRipartizione = response.data
    }, function (response) {
        RRService.showError("Errore", "Non è stato possibile ottenere la lista dei periodi di ripartizione")
    });

    
    $scope.init = function (filtri) {
        RRService.getStorico(filtri).then(function (response) {
            if (response.data.length < 1) {
                RRService.showError("Errore", "Non è stato trovata nessuna Regola per i filtri inseriti");
                $scope.storico = []
            } else {
                $scope.storico = response.data;
                angular.forEach($scope.storico, function (value) {
                    angular.forEach(value.regoleRipartizioneDTO, function (value) {
                        value.valorizzazione = JSON.parse(value.valorizzazione);
                        value.ripartizione = JSON.parse(value.ripartizione);
                        value.ripartizione.analitico = parseFloat(value.ripartizione.analitico);
                        value.ripartizione.campionario = parseFloat(value.ripartizione.campionario);
                        value.ripartizione.proporzionale = parseFloat(value.ripartizione.proporzionale);
                        value.ripartizione.campionarioProgrammi = parseFloat(value.ripartizione.campionarioProgrammi);
                        value.ripartizione.campionarioRilevazioni = parseFloat(value.ripartizione.campionarioRilevazioni)
                    })
                })
            }
        }, function (response) {
            RRService.showError("Errore", "Non è stato possibile ottenere lo storico delle Regole precedentemente salvate")
        })
    };

    $scope.search = function (filtriRicerca) {
        $scope.init(filtriRicerca)
    };
    
    $scope.getApproved = function (periodo) {
    		if($scope.periodiRipartizione!=null||$scope.periodiRipartizione!=undefined){
    			result=false;
			for (var i = 0; i < $scope.periodiRipartizione.length; i++) {
				if($scope.periodiRipartizione[i].codice==periodo){
					result= $scope.periodiRipartizione[i].dataApprovazioneRegoleValorizzazione!=null&&$scope.periodiRipartizione[i].dataApprovazioneRegoleValorizzazione!=undefined				
				}
			}
			return result;
		}
    };

    $scope.checkFilters = function (criterio, checkboxSelected) {

        var object = angular.copy(criterio);

        if (!checkboxSelected) {
            delete object.ripartizione.esclusioniUtilizzazioni.durataMinima
        }

        if(object.valorizzazione) {
            if (!object.valorizzazione.differenziaGruppiPrincipaliESpalla) {
                delete object.valorizzazione.quotaGruppiPrincipali;
                delete object.valorizzazione.quotaGruppiSpalla
            } else {
                var quotaPrincipali;
                var quotaSpalla;
                if (!object.valorizzazione.quotaGruppiPrincipali || object.valorizzazione.quotaGruppiPrincipali === null) {
                    quotaPrincipali = 0
                } else {
                    quotaPrincipali = object.valorizzazione.quotaGruppiPrincipali;
                }
                if (!object.valorizzazione.quotaGruppiSpalla || object.valorizzazione.quotaGruppiSpalla === null) {
                    quotaSpalla = 0
                } else {
                    quotaSpalla = object.valorizzazione.quotaGruppiSpalla;
                }
                var quote = quotaPrincipali + quotaSpalla;
                if (quote < 100 || quote > 100) {
                    RRService.showError("Attenzione", "La somma delle % dei gruppi deve corrispondere a 100%");
                    return false
                }
            }
        }


        if (!object.ripartizione || !angular.isObject(object.ripartizione) || object.ripartizione === {}) {
            RRService.showError("Attenzione", "Tutti i filtri sono obbligatori, a eccezione delle Esclusioni e della Suddivisione importi Principale/Spalla");
            return false
        }

        if (checkboxSelected && (!object.ripartizione.esclusioniUtilizzazioni.durataMinima || object.ripartizione.esclusioniUtilizzazioni.durataMinima < 0)) {
            RRService.showError("Attenzione", "Se selezionata l'eslusione 'Esecuzioni sotto i', inserire un valore valido");
            return false
        }

        if (object.ripartizione.campionario && (!object.ripartizione.campionarioProgrammi && !object.ripartizione.campionarioRilevazioni)) {
            RRService.showError("Attenzione", "Se selezionata la tipologia di ripartizione 'Campionario', inserire le % di incassi per Programmi e Rilevazioni");
            return false
        }

        if (object.ripartizione.campionario && (object.ripartizione.voceIncassoRilevazioni === null || !object.ripartizione.voceIncassoRilevazioni || object.ripartizione.voceIncassoRilevazioni === "")) {
            RRService.showError("Attenzione", "Se selezionata la tipologia di ripartizione 'Campionario', inserire la Voce Incasso Rilevazioni");
            return false
        }

        if (!object.periodoRipartizione || !object.voceIncasso || !object.tipologiaReport || !object.ripartizioneOS || !object.valorizzazione || !object.valorizzazione.regola) {
            RRService.showError("Attenzione", "Tutti i filtri sono obbligatori, a eccezione delle Esclusioni e della Suddivisione importi Principale/Spalla");
            return false
        }

        var totRipartizione = object.ripartizione.analitico + object.ripartizione.proporzionale + object.ripartizione.campionario;

        if (totRipartizione !== 100) {
            RRService.showError("Attenzione", "Il tipo di ripartizione è obbligatorio: Analitico, Proporzionale o Campionario");
            return false
        }

        if (object.ripartizione.campionario) {
            var campionarioProgrammi;
            var campionarioRilevazioni;
            if (!object.ripartizione.campionarioProgrammi || object.ripartizione.campionarioProgrammi === null) {
                campionarioProgrammi = 0
            } else {
                campionarioProgrammi = object.ripartizione.campionarioProgrammi;
            }
            if (!object.ripartizione.campionarioRilevazioni || object.ripartizione.campionarioRilevazioni === null) {
                campionarioRilevazioni = 0
            } else {
                campionarioRilevazioni = object.ripartizione.campionarioRilevazioni;
            }
            var totRipartizioneCampionario = campionarioProgrammi + campionarioRilevazioni;
            if (totRipartizioneCampionario < 100 || totRipartizioneCampionario > 100) {
                RRService.showError("Attenzione", "La somma delle % degli incassi del tipo Campionario, deve fare 100%");
                return false
            }
        } else {
            object.ripartizione.campionarioProgrammi = 0;
            object.ripartizione.campionarioRilevazioni = 0
        }

        return $scope.buildCorrectObject(object)
    };

    $scope.buildCorrectObject = function (object) {
        angular.forEach(object.ripartizione, function (key, value) {
            if (!key) {
                object.ripartizione[value] = 0
            }
        });
        object.ripartizione = JSON.stringify(object.ripartizione);
        object.valorizzazione = JSON.stringify(object.valorizzazione);
        return object
    };

    $scope.save = function (object, checkboxSelected) {
        var conf = $scope.checkFilters(object, checkboxSelected);
        if (!conf) {
            return
        }
        conf.user = $scope.user;

        RRService.askConfirm("Attenzione", "Sei sicuro di voler salvare la Regola?")
            .closePromise.then(function (model) {
            if (model.value) {
                RRService.save(conf).then(function (response) {
                    RRService.showError("Successo", "La Regola è stata salvata con successo");
                    ngDialog.closeAll();
                    $scope.init()
                }, function (response) {
                    if (response.status === 501) {

                        RRService.showError("Errore", "Impossibile salvare la configurazione, esiste già una regola salvata per questa (Voce Incasso,Tipo Report, Periodo di ripartizione)");
                        return
                    }
                    RRService.showError("Errore", "Non è stato possibile salvare la Regola");
                    $scope.init()
                })
            }
        })
    };

    $scope.checkDate = function (from, to) {
        if (from && to) {

            var comparisonFrom = "";
            var comparisonTo = "";

            if (from.length > 7) {
                comparisonFrom = from.split("-")[2] + from.split("-")[1] + from.split("-")[0];
                comparisonTo = to.split("-")[2] + to.split("-")[1] + to.split("-")[0];
            } else {
                comparisonFrom = from.split("-")[1] + from.split("-")[0];
                comparisonTo = to.split("-")[1] + to.split("-")[0];
            }

            if (comparisonTo < comparisonFrom) {
                return false;
            }
        }

        return true
    };

    $scope.openModelStorico = function (periodo, configurazione) {
        RRService.openModelStorico(periodo, configurazione);
    };

    $scope.openModelModifica = function (periodo, configurazione) {
        RRService.openModelModifica(periodo, configurazione, $scope.user, $scope.init, $scope.vociIncasso, $scope.vociIncassoFittizie)
    };

    $scope.deleteConfigurazione = function (periodo, configurazione) {
        RRService.askConfirm("Attenzione", "Sei sicuro di voler eliminare la Regola?")
            .closePromise.then(function (model) {
            if (model.value === true) {

                //periodo, configurazione, $scope.user, configurazione.voceIncasso, configurazione.tipologiaReport
                var object = {
                    periodoRipartizione: periodo,
                    voceIncasso: configurazione.voceIncasso,
                    tipologiaReport: configurazione.tipologiaReport,
                    user: $scope.user,
                    esclusioniUtilizzazioni: {
                        durataMinima: 30
                    }
                };

                RRService.deleteConfigurazione(object)
                    .then(function () {
                        RRService.showError("Successo", "La Regola è stata eliminata con successo");
                        $scope.init()
                    }, function () {
                        RRService.showError("Errore", "Non è stato possibile eliminare la Regola")
                    })
            }
        })
    };

    $scope.openModelNew = function () {
        RRService.openModelNew($scope.periodiRipartizione, $scope.vociIncasso, $scope.vociIncassoFittizie, $scope.save);
    };

    $scope.parseDateTime = function (date) {
        return RRService.parseDateTime(date);
    };

    $scope.parseDate = function (date) {
        return RRService.parseDate(date);
    }

}]);

codmanApp.service("RegoleRipartizioneService", ["$http", 'ngDialog', function ($http, ngDialog) {

    var service = this;

    service.getStorico = function (filtri) {
        return $http({
            method: 'GET',
            url: 'rest/performing/valorizzazione/configurazione/getConfigurazioni',
            params: filtri
        })
    };

    service.getVociIncasso = function () {
        return $http({
            method: 'GET',
            url: 'rest/performing/valorizzazione/getVociIncasso'
        })
    };

    service.getVociIncassoFittizie = function () {
        return $http({
            method: 'GET',
            url: 'rest/performing/valorizzazione/getVociIncassoFittizie'
        })
    };

    service.getPeriodiRipartizione = function () {
        return $http({
            method: 'GET',
            url: 'rest/performing/valorizzazione/getPeriodiRipartizione'
        })
    };

    service.save = function (object) {
        return $http({
            method: 'POST',
            url: 'rest/performing/valorizzazione/configurazione/addConfigurazione',
            data: object
        })
    };

    service.deleteConfigurazione = function (object) {
        return $http({
            method: 'DELETE',
            url: 'rest/performing/valorizzazione/configurazione/deleteConfigurazione',
            params: object
        })
    };

    service.showError = function (title, msgs) {
        return ngDialog.open({
            template: 'pages/advance-payment/dialog-alert.html',
            plain: false,
            width: '60%',
            controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
                var ngDialogId = $scope.ngDialogId;
                $scope.ctrl = {
                    title: title,
                    message: msgs,
                    ngDialogId: ngDialogId
                };
                $scope.cancel = function () {
                    ngDialog.close(ngDialogId);
                };
            }]
        })
    };

    service.askConfirm = function (title, msgs) {
        return ngDialog.open({
            template: 'pages/advance-payment/dialog-confirm.html',
            plain: false,
            width: '60%',
            controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
                var ngDialogId = $scope.ngDialogId;
                $scope.ctrl = {
                    title: title,
                    message: msgs,
                    ngDialogId: ngDialogId
                };
                $scope.cancel = function (ngDialogId) {
                    ngDialog.close(ngDialogId);
                };
                $scope.save = function () {
                    ngDialog.close(ngDialogId, true);
                };
            }]
        })
    };

    service.openModelStorico = function (periodo, configurazione) {
        return ngDialog.open({
            template: 'pages/advance-payment/dialog-storicoRegoleRipartizione.html',
            plain: false,
            width: '90%',
            controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {

                var ngDialogId = $scope.ngDialogId;
                $scope.ctrl = {
                    ngDialogId: ngDialogId
                };

                $scope.configurazione = angular.copy(configurazione);
                $scope.periodo = periodo;
                $scope.vuoto = false;

                service.getStoricoConfigurazione($scope.periodo, $scope.configurazione.voceIncasso, $scope.configurazione.tipologiaReport).then(function (response) {
                    $scope.storico = response.data[0].regoleRipartizioneDTO;

                    angular.forEach($scope.storico, function (value) {
                        value.valorizzazione = JSON.parse(value.valorizzazione);
                        value.ripartizione = JSON.parse(value.ripartizione);
                        value.ripartizione.analitico = parseFloat(value.ripartizione.analitico);
                        value.ripartizione.campionario = parseFloat(value.ripartizione.campionario);
                        value.ripartizione.proporzionale = parseFloat(value.ripartizione.proporzionale);
                        value.ripartizione.campionarioProgrammi = parseFloat(value.ripartizione.campionarioProgrammi);
                        value.ripartizione.campionarioRilevazioni = parseFloat(value.ripartizione.campionarioRilevazioni)
                    });

                    $scope.vuoto = false
                }, function () {
                    service.showError("Errore", "Non è stato possibile ottenere lo storico della configurazione selezionata");
                    $scope.vuoto = true
                });

                $scope.cancel = function () {
                    ngDialog.close(ngDialogId);
                };

                $scope.parseDate = function (data) {
                    return service.parseDateTime(data);
                }

            }]
        })
    };

    service.getStoricoConfigurazione = function (periodo, voceIncasso, tipoReport) {
        return $http({
            method: 'GET',
            url: 'rest/performing/valorizzazione/configurazione/getStoricoConfigurazione',
            params: {
                periodo: periodo,
                voceIncasso: voceIncasso,
                tipoReport: tipoReport
            }
        })
    };

    service.openModelModifica = function (periodo, conf, user, init, vociIncasso, vociIncassoFittizie) {
        return ngDialog.open({
            template: 'pages/advance-payment/dialog-modify-RegoleRipartizione.html',
            plain: false,
            closeByDocument: false,
            closeByEscape: false,
            width: '90%',
            controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
                var ngDialogId = $scope.ngDialogId;
                $scope.ctrl = {
                    ngDialogId: ngDialogId
                };

                $scope.cancel = function (ngDialogId) {
                    ngDialog.close(ngDialogId);
                };

                $scope.user = user;

                $scope.vociIncasso = vociIncasso;

                $scope.vociIncassoFittizie = vociIncassoFittizie;

                $scope.periodoRipartizione = periodo;

                $scope.criterio = angular.copy(conf);

                angular.forEach($scope.criterio.ripartizione, function (value, key) {
                    if (key !== 'campionarioProgrammi' && key !== 'campionarioRilevazioni' && value === 100)
                        $scope.tipoRipartizione = key
                });

                $scope.saveModify = function (object) {

                    var criterio = angular.copy(object);

                    if (object.ripartizione.esclusioniUtilizzazioni === null || !object.ripartizione.esclusioniUtilizzazioni) {
                        object.ripartizione.esclusioniUtilizzazioni = {durataMinima: null};
                    }

                    if (!$scope.checkboxSelected) {
                        object.ripartizione.esclusioniUtilizzazioni.durataMinima = null
                    }

                    if ($scope.tipoRipartizione === 'analitico') {
                        criterio.ripartizione = {
                            analitico: 100,
                            proporzionale: 0,
                            campionario: 0,
                            campionarioProgrammi: 0,
                            campionarioRilevazioni: 0,
                            esclusioniUtilizzazioni:
                            object.ripartizione.esclusioniUtilizzazioni

                        }
                    }

                    if ($scope.tipoRipartizione === 'proporzionale') {
                        criterio.ripartizione = {
                            analitico: 0,
                            proporzionale: 100,
                            campionario: 0,
                            campionarioProgrammi: 0,
                            campionarioRilevazioni: 0,
                            esclusioniUtilizzazioni:
                            object.ripartizione.esclusioniUtilizzazioni
                        }
                    }

                    if ($scope.tipoRipartizione === 'campionario') {
                        criterio.ripartizione = {
                            analitico: 0,
                            proporzionale: 0,
                            campionario: 100,
                            campionarioProgrammi: object.ripartizione.campionarioProgrammi,
                            campionarioRilevazioni: object.ripartizione.campionarioRilevazioni,
                            voceIncassoRilevazioni: object.ripartizione.voceIncassoRilevazioni,
                            esclusioniUtilizzazioni:
                            object.ripartizione.esclusioniUtilizzazioni
                        }
                    }

                    delete criterio.competenzaA;
                    delete criterio.competenzaDa;

                    criterio.periodoRipartizione = $scope.periodoRipartizione;
                    var object = $scope.checkFilters(criterio, $scope.checkboxSelected);
                    if (!object) {
                        return
                    }
                    object.user = user;
                    object.ripartizione = JSON.stringify(object.ripartizione);
                    object.valorizzazione = JSON.stringify(object.valorizzazione);
                    service.askConfirm("Attenzione", "Seri sicuro di voler salvare le modifiche?")
                        .closePromise.then(function (model) {
                        if (model.value === true) {
                            service.modificaConfigurazione(object).then(function (response) {
                                service.showError("Successo", "La configurazione è stata modificata con successo");
                                ngDialog.closeAll();
                                init()
                            }, function (response) {
                                if (response.status === 501) {
                                    service.showError("Errore", "Impossibile salvare la configurazione, esiste già una regola salvata per questa (Voce Incasso,Tipo Report, Periodo di ripartizione)");
                                    return
                                }
                                service.showError("Errore", "Non è stato possibile modificare la configurazione")
                            })
                        }
                    })
                };

                $scope.checkFilters = function (criterio, checkboxSelected) {

                    var object = angular.copy(criterio);

                    if (!checkboxSelected) {
                        delete object.ripartizione.esclusioniUtilizzazioni.durataMinima
                    }

                    if(object.valorizzazione) {
                        if (!object.valorizzazione.differenziaGruppiPrincipaliESpalla) {
                            delete object.valorizzazione.quotaGruppiPrincipali;
                            delete object.valorizzazione.quotaGruppiSpalla
                        } else {
                            var quotaPrincipali;
                            var quotaSpalla;
                            if (!object.valorizzazione.quotaGruppiPrincipali || object.valorizzazione.quotaGruppiPrincipali === null) {
                                quotaPrincipali = 0
                            } else {
                                quotaPrincipali = object.valorizzazione.quotaGruppiPrincipali;
                            }
                            if (!object.valorizzazione.quotaGruppiSpalla || object.valorizzazione.quotaGruppiSpalla === null) {
                                quotaSpalla = 0
                            } else {
                                quotaSpalla = object.valorizzazione.quotaGruppiSpalla;
                            }
                            var quote = quotaPrincipali + quotaSpalla;
                            if (quote < 100 || quote > 100) {
                                service.showError("Attenzione", "La somma delle % dei gruppi deve corrispondere a 100%");
                                return false
                            }
                        }
                    }

                    if (object.ripartizione === "") {
                        service.showError("Attenzione", "Tutti i filtri sono obbligatori, a eccezione delle Esclusioni e della Suddivisione importi Principale/Spalla");
                        return false
                    }

                    if (checkboxSelected && (!object.ripartizione.esclusioniUtilizzazioni.durataMinima || object.ripartizione.esclusioniUtilizzazioni.durataMinima < 0)) {
                        service.showError("Attenzione", "Se selezionata l'eslusione 'Esecuzioni sotto i', inserire un valore valido");
                        return false
                    }

                    if (object.ripartizione.campionario && (!object.ripartizione.campionarioProgrammi && !object.ripartizione.campionarioRilevazioni)) {
                        service.showError("Attenzione", "Se selezionata la tipologia di ripartizione 'Campionario', inserire le % di incassi per Programmi e Rilevazioni");
                        return false
                    }

                    if (object.ripartizione.campionario && (object.ripartizione.voceIncassoRilevazioni === null || !object.ripartizione.voceIncassoRilevazioni || object.ripartizione.voceIncassoRilevazioni === "")) {
                        service.showError("Attenzione", "Se selezionata la tipologia di ripartizione 'Campionario', inserire la Voce Incasso Rilevazioni");
                        return false
                    }

                    if (!object.periodoRipartizione || !object.voceIncasso || !object.tipologiaReport || !object.ripartizioneOS || !object.valorizzazione || !object.valorizzazione.regola) {
                        service.showError("Attenzione", "Tutti i filtri sono obbligatori, a eccezione delle Esclusioni e della Suddivisione importi Principale/Spalla");
                        return false
                    }

                    var totRipartizione = object.ripartizione.analitico + object.ripartizione.proporzionale + object.ripartizione.campionario;

                    if (totRipartizione != 100) {
                        service.showError("Attenzione", "Il tipo di ripartizione è obbligatorio: Analitico, Proporzionale o Campionario");
                        return false
                    }

                    if (object.ripartizione.campionario) {
                        var campionarioProgrammi;
                        var campionarioRilevazioni;
                        if (!object.ripartizione.campionarioProgrammi || object.ripartizione.campionarioProgrammi === null) {
                            campionarioProgrammi = 0
                        } else {
                            campionarioProgrammi = object.ripartizione.campionarioProgrammi;
                        }
                        if (!object.ripartizione.campionarioRilevazioni || object.ripartizione.campionarioRilevazioni === null) {
                            campionarioRilevazioni = 0
                        } else {
                            campionarioRilevazioni = object.ripartizione.campionarioRilevazioni;
                        }
                        var totRipartizioneCampionario = campionarioProgrammi + campionarioRilevazioni;
                        if (totRipartizioneCampionario < 100 || totRipartizioneCampionario > 100) {
                            service.showError("Attenzione", "La somma delle % degli incassi del tipo Campionario, deve fare 100%");
                            return false
                        }
                    } else {
                        object.ripartizione.campionarioProgrammi = 0;
                        object.ripartizione.campionarioRilevazioni = 0
                    }

                    return $scope.buildCorrectObject(object);
                };


                $scope.buildCorrectObject = function (object) {
                    angular.forEach(object.ripartizione, function (key, value) {
                        if (!key) {
                            object.ripartizione[value] = 0
                        }
                    });
                    return object
                }
            }]
        })
    };

    service.openModelNew = function (periodiRipartizione, vociIncasso, vociIncassoFittizie, save) {
        return ngDialog.open({
            template: 'pages/advance-payment/dialog-new-RegoleRipartizione.html',
            plain: false,
            closeByDocument: false,
            closeByEscape: false,
            width: '90%',
            controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
                var ngDialogId = $scope.ngDialogId;
                $scope.ctrl = {
                    ngDialogId: ngDialogId
                };

                $scope.criterio = {
                    ripartizione: {
                        esclusioniUtilizzazioni: {
                            durataMinima: 30
                        }
                    }
                };

                $scope.save = save;

                $scope.vociIncasso = vociIncasso;

                $scope.vociIncassoFittizie = vociIncassoFittizie;

                $scope.periodiRipartizione = periodiRipartizione;

                $scope.parseDateTime = function (date) {
                    return service.parseDateTime(date);
                };

                $scope.parseDate = function (date) {
                    return service.parseDate(date);
                };

                $scope.cancel = function (ngDialogId) {
                    ngDialog.close(ngDialogId);
                };


            }]
        })
    };

    service.modificaConfigurazione = function (configurazione) {
        return $http({
            method: 'PUT',
            url: 'rest/performing/valorizzazione/configurazione/updateConfigurazione',
            data: configurazione
        })
    };
}]);
