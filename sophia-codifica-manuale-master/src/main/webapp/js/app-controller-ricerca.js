(function() {
  /*
   * RicercaCtrl
   *
   * @path /ricerca
   */
  angular.module('codmanApp').controller('RicercaCtrl',
    ['$scope', 'ngDialog', '$routeParams', '$location', '$http', 'configService', 'riconoscimentoService', '$route',
      function($scope, ngDialog, $routeParams, $location, $http, configService, riconoscimentoService, $route) {
        $scope.resetForm = function(fromButton) {
          $scope.ctrl = {
            navbarTab: 'ricerca',
            execute: true, // $routeParams.execute,
            hideForm: $routeParams.hideForm,
            maxrows: configService.maxRowsPerPage,
            first: $routeParams.first || 0,
            last: $routeParams.last || configService.maxRowsPerPage,
            title: $routeParams.title,
            artists: $routeParams.artists,
            roles: $routeParams.roles,
            sortType: 'priority',
            sortReverse: true,
            currentTimeMillis: (new Date()).getTime()
          };
          if (!fromButton && $routeParams) {
            var search = $routeParams;
            Object.keys(search).forEach(function(key) {
              $scope.ctrl[key] = search[key];
            });
          }
        };

        function reloadPageWithNewParams(search) {
          Object.keys(search).forEach(function(key) {
            $location.search(key, search[key]);
          });
          $location.path($location.path());
        }

        $scope.onRicercaEx = function() {
          var search = angular.copy($scope.ctrl);
          $http({
            method: 'GET',
            url: 'rest/unidentified-song',
            params: search
          }).then(function successCallback(response) {
            $scope.unidentifiedSongs = response.data;
            $scope.ctrl.first = response.data.first;
            $scope.ctrl.last = response.data.last;
          }).catch(function errorCallback(response) {
            $scope.unidentifiedSongs = {};
          });

          return;
        };

        $scope.onAzzera = function() {
          $location.url($location.path());
          $scope.resetForm(true);
        };

        $scope.onRicerca = function() {
          $scope.ctrl.maxrows = configService.maxRowsPerPage;
          $scope.ctrl.first = 0;
          $scope.ctrl.last = configService.maxRowsPerPage;
          $scope.ctrl.sortType = 'priority';
          $scope.ctrl.sortReverse = true;
          $scope.ctrl.currentTimeMillis = (new Date()).getTime();
          $scope.onRicercaEx();
        };

        $scope.sort = function(field) {
          $scope.ctrl.sortType === field ?
            $scope.ctrl.sortReverse = !$scope.ctrl.sortReverse :
            $scope.ctrl.sortType = field;
          $scope.onRicercaEx();
        };

        $scope.gotoRiconoscimento = function(hashId, user) {
          var search = angular.copy($scope.ctrl);
          search.hashId = hashId;
          // riconoscimentoService.getOpera(hashId, user).then(function(response) {
          //   if (response) {
          Object.keys(search).forEach(function(key) {
            $location.search(key, search[key]);
          });
          $location.search('user', user);
          $location.path('/riconoscimento');
          // .search('response', angular.toJson(response));
          // } else {
          //   $scope.showNotFound();
          // }
          // }).catch(function(response) {
          //   if (response.status === 404) {
          //     $scope.showNotFound();
          //   }
          // });
        };

        $scope.navigateToNextPage = function() {
          var search = angular.copy($scope.ctrl);
          search.first = $scope.ctrl.last;
          search.last = $scope.ctrl.last + $scope.ctrl.maxrows;
          reloadPageWithNewParams(search);
        };

        $scope.navigateToPreviousPage = function() {
          var search = angular.copy($scope.ctrl);
          search.first = Math.max($scope.ctrl.first - $scope.ctrl.maxrows, 0);
          search.last = $scope.ctrl.first;
          reloadPageWithNewParams(search);
        };

        $scope.showNotFound = function() {
          ngDialog.open({
            template: 'pages/riconoscimento/dialog-not-found.html',
            plain: false,
            width: '50%',
            controller: ['$scope', 'ngDialog', function(scope, dialog) {
              scope.cancel = function() {
                dialog.closeAll(false);
              };
              scope.save = function() {
                dialog.closeAll(true);
              };
            }]
          });
        };

        $scope.resetForm(false);
        if ($scope.ctrl.execute) {
          $scope.onRicercaEx();
        }
      }]);
})();
