(function() {
  'use strict';
  angular.module('codmanApp')
    .controller('PerformingScodificaCtrl', PerformingScodificaCtrl);

  PerformingScodificaCtrl.$inject = ['scodificaService', 'ngDialog', 'UtilService'];

  function PerformingScodificaCtrl(scodificaService, ngDialog, UtilService) {
    var vm = this;

    vm.getListaScodifica = getListaScodifica;
    vm.isValid = isValid;
    vm.getDettaglioCombinazioneAnagrafica = getDettaglioCombinazioneAnagrafica;
    vm.modificaCodiceOpera = modificaCodiceOpera;
    vm.showDetail = showDetail;


    function isValid() {
      var valid = false;
      if (vm.scodificaRequest) {
        Object.keys(vm.scodificaRequest).forEach(function(b) {
          if (b !== 'pageNumber' && b !== 'export') {
            valid = valid || (!!vm.scodificaRequest[b] && (!!vm.scodificaRequest[b].trim().length > 0));
          }
        });
      }
      return valid;
    }

    function getListaScodifica(pageNumber, sendError) {
      if (!isNaN(pageNumber)) { // exporting results as CSV if pageNumber undefined
        vm.scodificaRequest.pageNumber =
                    vm.scodificaRequest.pageNumber && pageNumber ? vm.scodificaRequest.pageNumber + pageNumber : 1;
        vm.scodificaRequest.export = 'false';
      } else {
        vm.scodificaRequest.export = 'true';
      }
      scodificaService.getListaScodifica(vm.scodificaRequest, sendError)
        .then(function(data) {
          if (data && sendError !== false) {
            vm.listaScodifica = data.list;
            vm.pagination = data.pagination;
          } else if (!vm.scodificaRequest.export === 'true') {
            vm.listaScodifica = undefined;
            vm.pagination = undefined;
          }
        });
    }

    function getDettaglioCombinazioneAnagrafica(combana, user) {
      ngDialog.open({
        template: 'pages/performing/codifica/scodifica/dettaglio-combinazione-anagrafica.htm',
        plain: false,
        width: '90%',
        controller: 'DettaglioCombinazioneAnagraficaController',
        controllerAs: 'vm',
        resolve: {
          dca: function() {
            return scodificaService.getDettaglioCombinazioneAnagrafica(combana.idCombana);
          }
        },
        data: {
          combana: combana,
          user: user
        }
      }).closePromise.then(function(response) {
        if (response.value === true) {
          vm.getListaScodifica(0, false);
        }
      });
    }

    function modificaCodiceOpera(scodifica, user) {
      scodificaService.getSessioneScodifica(scodifica, user);
    }

    function showDetail(scodifica) {
      if (!!!scodifica.dettaglio) {
        scodificaService.getDettaglioScodifica(scodifica.idCombana)
          .then(function(data) {
            scodifica.dettaglio = data;
          });
      }
      scodifica.showDetail = !!!scodifica.showDetail;
    }
  }
})();
