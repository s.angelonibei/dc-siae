codmanApp.controller('ConsoleCSVCtrl', [ '$scope','$interval','Upload', 'configService','uploadMassivoService', function($scope, $interval, Upload, configService, uploadMassivoService) {

	$scope.firstPaging = {
		currentPage: 1,
		order: "insertTime desc"
	}

	$scope.messages = {
		info: "", 
		error: ""
	}

	$scope.paging = {... $scope.firstPaging};

	$scope.clearMessages = function() {
		$scope.messages = {
			info: "", 
			error: ""
		}
	}

	$scope.infoMessage = function(message) {
		$scope.messages.info = message;
	}

	$scope.errorMessage = function(message) {
		$scope.messages.error = message;
	}

	$scope.init = function(){
		$scope.clearMessages();
		uploadMassivoService.getUploadedFileList($scope.paging).then(function(response){
			if(response.data.records.length < 1){
				$scope.paging = {... $scope.firstPaginng};
				$scope.response = [];
				return;
			}
			$scope.paging.currentPage = response.data.currentPage;
			$scope.paging.totRecords = response.data.totRecords;
			$scope.paging.firstRecord = (($scope.paging.currentPage - 1) * 50) + 1;
			$scope.paging.lastRecord = ($scope.paging.currentPage -1 ) * 50 + response.data.records.length;
			$scope.paging.totPages = Math.ceil(response.data.totRecords/50);

			$scope.uploadedFileList = response.data.records;
		},function(error){
			$scope.errorMessage("Si è verificato un problema, riprovare più tardi!");
		});
	}

	$scope.navigateToPreviousPage = function () {
		$scope.paging.currentPage = $scope.paging.currentPage - 1;
		$scope.init();
	}

	$scope.navigateToNextPage = function () {
		$scope.paging.currentPage = $scope.paging.currentPage + 1;
		$scope.init();
	}

	$scope.navigateToEndPage = function () {
		$scope.paging.currentPage = $scope.paging.totPages;
		$scope.init();
	}	

	$scope.uploadFile = function(file) {
		$scope.clearMessages();
		uploadMassivoService.upload(file)
		.then(function (response) {
			$scope.cs.file = null;
			$scope.init();
			$scope.infoMessage("Il file è stato caricato correttamente.");			
		}, function (error) {
			if (error == 'INVALID_EXT') {
				$scope.errorMessage("Estensione non supportata. Le estensioni supportate sono: xls,xlsx,csv");
			} else {
				$scope.errorMessage("Il file non è stato caricato correttamente!");
			}
		});
  };

	$scope.downloadFile = function (fileUrl, fileName) {
		$scope.clearMessages();
		uploadMassivoService.downloadFile(fileUrl)
			.then(function (response) {
				var blob = new Blob([response.data], { type: "application/vnd.ms-excel;charset=UTF-8" });
				saveAs(blob, fileName);
			}, function (error) {
				$scope.errorMessage("Non è possibile scaricare il file selezionato!");
			});
	}

	$scope.init();

	$scope.getIdConfigurazione = function(message) {
		var parsed = JSON.parse(message);
		var idConfigurazione = parsed.body.configurationId;
		return idConfigurazione;
	}

}]);

codmanApp.service('uploadMassivoService', ['$http','$q', function($http,$q){

	var service = this;

	service.getUploadedFileList = function(paging){
		return $http({
			method: 'GET',
			url: 'rest/performing/codifica/ricerca-massiva/uploaded-files',
			params: {
				page: paging.currentPage,
				order: paging.order
			}
		})
	}

	service.upload = function (cs) {

		var extension = cs.file.name.slice(cs.file.name.lastIndexOf('.'))
		if (extension != '.xls' && extension != '.xlsx' && extension != '.csv') {
			return $q.reject("INVALID_EXT");;
		}

		var fd = new FormData();

		if (cs.file) {
			fd.append('file', cs.file)
			fd.append('fileName', cs.file.name);
		}

		return $http({
			method: 'POST',
			url: 'rest/performing/codifica/ricerca-massiva/upload-csv',
			data: fd,
			transformRequest: angular.identity,
			headers: { 'Content-Type': undefined }
		});
	}

	service.downloadFile = function (fileUrl) {
		var encodedURL = encodeURIComponent(fileUrl);
		return $http({
			method: 'GET',
			responseType: 'blob',
			url: 'rest/performing/codifica/ricerca-massiva/download-csv?fileUrl=' + encodedURL
		});
	}

}]);



