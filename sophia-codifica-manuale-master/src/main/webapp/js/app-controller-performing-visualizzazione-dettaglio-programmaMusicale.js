codmanApp.controller('VisualizzazionePMCtrl',
    ['$filter','$scope', '$http', '$location', 'ngDialog', '$routeParams','programmaMusicaleService', 'PerformingCodificaManualeEspertoService',
        function($filter, $scope, $http, $location, ngDialog, $routeParams, programmaMusicaleService, CMEService, pmDetail) {
            $scope.user = angular.element("#headerLinksBig").text().trim();

            $scope.opereDaApprovare = {};

            $scope.getCandidate = getCandidate;
            $scope.addOperaCandidata = addOperaCandidata;
            $scope.isValid = isValid;
            $scope.invia = invia;

            $scope.checkNumOpera = function(codiceOpera, id, tipoApprovazione) {
                if (codiceOpera && codiceOpera !== "") {
                    if (codiceOpera.length === 11) {
                        angular.element(id).css('color', 'black');
                    }else {
                        angular.element(id).css('color', 'red');
                    }
                    return codiceOpera;
                }else {
                    if (tipoApprovazione === 'A') {
                        angular.element(id).css('color', 'blue');
                    } else {
                        angular.element(id).css('color', 'red');
                    }
                }
            };

            $scope.exportOpere = function() {
                $http({
                    method: 'GET',
                    url: 'rest/performing/cruscotti/exportOpereList',
                    params: {
                        idPm: $scope.idProgrammaMusicale,
                    }
                }).then(function(response) {

                    var blob = new Blob([response.data], {type: "text/plain;charset=utf-8"});
                    saveAs(blob,'Lista Opere ID programma musicale-' + $scope.idProgrammaMusicale + '.csv');

                }, function() {
                    $scope.showError("Errore", "Impossibile fare il download dell'excel.");
                });
            };

            $scope.saveFlagCampionamento = function() {
                $scope.savePM('c', $scope.flagDaCampionare);
            };

            $scope.saveFlagGPS = function() {
                $scope.savePM('g', $scope.flagGruppoPrincipaleSpalla);
            };

            $scope.flagGruppoPS = function(incasso, flagGP) {
                if (incasso === "2244" && flagGP === "1") {
                    $scope.gruppo = flagGP;
                    $scope.gruppoTipo = "(Principale)";
                } else if (incasso === "2244" && flagGP === "0") {
                    $scope.gruppo = flagGP;
                    $scope.gruppoTipo = "(Spalla)";
                } else {
                    $scope.gruppo = flagGP;
                    $scope.gruppoTipo = "";
                }
            };
            
            $scope.getStatoPM = function(statoPm) {
            		if (!statoPm) {
            			return ""
            		}else if (statoPm==='V') {
            			return "VALIDO"
                } else {
            			return "ANNULLATO"
                }
            };
        

            $scope.removeOpera = function(id, showError, user, codiceOpera, init) {
                ngDialog.open({
                    template: 'pages/advance-payment/dialog-warning.html',
                    plain: false,
                    width: '60%',
                    controller: ['$scope', 'ngDialog', function($scope, scope) {
                        $scope.ctrl = {
                            title: "Opera",
                            ngDialogId: scope.ngDialogId
                        };

                        if (codiceOpera !== undefined && codiceOpera !== "") {
                            $scope.ctrl.message = "Sei sicuro di voler rimuovere l'opera " + codiceOpera + " selezionata?";
                        } else {
                            $scope.ctrl.message = "Sei sicuro di voler rimuovere l'opera selezionata?";
                        }

                        $scope.showError = showError;
                        $scope.id = id;
                        $scope.user = user;
                        $scope.init = init;

                        $scope.cancel = function(ngDialogId) {
                            ngDialog.closeAll(ngDialogId);
                        };

                        $scope.call = function() {
                            $http({
                                method: 'DELETE',
                                url: 'rest/performing/cruscotti/removeOpera',
                                params: {
                                    idUtilizzazione: id,
                                    userId: $scope.user
                                }
                            }).then(function() {
                                $scope.showError("Eliminazione Opera", "Opera eliminata correttamente");
                                $scope.init();

                            }, function() {
                                $scope.showError("Eliminazione Opera", "C'è stato un problema nell'eliminazione dell'opera");
                            });
                            ngDialog.closeAll(true);
                        };
                    }]
                }).closePromise.then(function(data) {

                });
            };

            $scope.idMovimento = $routeParams.idMovimento;
            $scope.idProgrammaMusicale = $routeParams.idProgrammaMusicale;
            $scope.flagCampionamento = true;
            $scope.flagGruppo = true;

            $scope.savePM = function(variable, param) {
                var params;

                if (variable === 'c') {
                    params = {
                        idPM: $scope.idProgrammaMusicale,
                        flagGruppo: null,
                        flagCampionamento: param,
                        userId: $scope.user
                    }
                } else if (variable === 'g') {
                    params = {
                        idPM: $scope.idProgrammaMusicale,
                        flagGruppo: param,
                        flagCampionamento: null,
                        userId: $scope.user
                    }
                }


                $http({
                    method: 'PUT',
                    url: 'rest/performing/cruscotti/updatePmOnDetail',
                    params: params
                }).then(function() {
                    $scope.showError("Modifica parametro", "Parametro modificato con successo");
                    $scope.init();
                }, function() {
                    $scope.showError("Modifica parametro", "Errore nel modificare il parametro, controllare se i campi sono corretti");
                })
            };

            $scope.searchCorresponding = function(user, opera, id, showError, checkNumOpera) {
                var scope = $scope;
                ngDialog.open({
                    template: 'pages/advance-payment/dialog-search-corresponding-opera.html',
                    plaing: false,
                    width: '80%',
                    controller: ['$scope', 'ngDialog', '$http', function($scope, ngDialog, $http) {
                        $scope.opera = opera;
                        $scope.user = user;
                        $scope.showError = showError;
                        $scope.id = id;
                        $scope.checkNumOpera = checkNumOpera;
                        $scope.artisti = "";

                        if ($scope.opera.compositore !== undefined && $scope.opera.compositore !== "") {
                            $scope.artisti = $scope.opera.compositore;
                        } else if ($scope.opera.autori !== undefined && $scope.opera.autori !== "") {
                            $scope.artisti = $scope.opera.autori;
                        } else if ($scope.opera.interpreti !== undefined && $scope.opera.interpreti !== "") {
                            $scope.artisti = $scope.opera.interpreti;
                        }

                        if ($scope.opera.titoloComposizione === undefined) {
                            $scope.opera.titoloComposizione = '';
                        }

                        $scope.searchCorrespondingOpera = function() {

                            var titoloComposizione = $scope.opera.titoloComposizione.replace(/ /g, "+");
                            var artisti = $scope.artisti.replace(/ /g, "+");

                            $http({
                                method: 'GET',
                                url: 'rest/performing/cruscotti/ricercaOpere',
                                params: {
                                    titoloOpera: titoloComposizione,
                                    autore: artisti,
                                    compositore: null
                                }
                            }).then(function(response) {
                                $scope.showRicerca = true;
                                $scope.samplings = response.data;
                                if (response.data.length === 0) {
                                    $scope.showError("Ricerca Opera", "La ricerca non ha riportato nessun risultato");
                                }
                            }, function() {
                                $scope.showError("Ricerca Opera", "C'è stato un problema nel ricercare l'opera");
                            })
                        };

                        $scope.replaceOpera = function(opera) {
                            opera.id = $scope.opera.id;
                            opera.idPM = $scope.opera.idProgrammaMusicale;
                            var modifiedOpera = {
                                id: $scope.opera.id,
                                idProgrammaMusicale: $scope.opera.idProgrammaMusicale,
                                codiceOpera: opera.codiceOpera,
                                titoloComposizione: opera.titolo,
                                compositore: opera.compositore,
                                compositori: opera.compositori,
                                menodi30sec: opera.menodi30sec,
                                autori: opera.autori,
                                interpreti: opera.interpreti,
                                elaboratorePubblicoDominio: opera.elaboratorePubblicoDominio,
                                combinazioneCodifica: null,
                                flagMagNonConcessa: null,
                                nettoMaturato: null,
                                dataPrimaCodifica: null,
                                tipoAccertamento: null,
                                durataEsecuzione: opera.durataEsecuzione,
                                flagCodifica: null
                            };

                            $http({
                                method: 'PUT',
                                url: 'rest/performing/cruscotti/updateOpera',
                                data: modifiedOpera
                            }).then(function() {
                                $scope.showError("Success", "Opera modificata con successo");
                                scope.init();
                                $scope.checkNumOpera(opera.codiceOpera, '#opera' + $scope.id, opera.tipoApprovazione);
                            }, function() {
                                $scope.showError("Errore", "C'è stato un problema nell'aggiornare le informazioni dell'opera");
                            });
                        };

                        $scope.cancel = function() {
                            ngDialog.close($scope.ngDialogId);
                        };
                        $scope.save = function() {
                            ngDialog.closeAll(true);
                        };
                    }]
                }).closePromise.then(function(data) {

                });
            };

            $scope.addOpera = function(user, idPM, numPM, showError) {
                ngDialog.open({
                    template: 'pages/advance-payment/dialog-add-opera.html',
                    plain: false,
                    width: '90%',
                    controller: ['$scope', 'ngDialog', '$http', function($scope, ngDialog, $http) {
                        $scope.user = user;
                        $scope.showRicerca = false;
                        $scope.idPM = idPM;
                        $scope.numPM = numPM;
                        $scope.success = false;
                        $scope.showError = showError;
                        $scope.paramsVisible = false;

                        $scope.buttonVisible = function(titoloComposizione, compositori, autori, interpreti) {
                            if (titoloComposizione != undefined && titoloComposizione != '') {
                                if ((compositori != undefined && compositori != '') || (autori != undefined && autori != '') || (interpreti != undefined && interpreti != '')) {
                                    return false;
                                }
                            }
                            return true;

                        };

                        $scope.ricercaOpera = function(titolo1, autore1, codiceOpera1) {
                            if ((titolo1 == undefined || titolo1 == "") && (autore1 == undefined || autore1 == "") && (codiceOpera1 == undefined || codiceOpera1 == "")) {
                                return $scope.showError("Error", "Inserire almeno un campo per la ricerca dell'opera");
                            }
                            if (titolo1 != undefined && titolo1 != "") {
                                var titoloComposizione = titolo1.replace(/ /g, "+");
                            }
                            if (autore1 != undefined && autore1 != "") {
                                var autore = autore1.replace(/ /g, "+");
                            }
                            if (codiceOpera1 != undefined && codiceOpera1 != "") {
                                if (new RegExp('[0-9]{11}').exec(codiceOpera1) && codiceOpera1.length < 12) {
                                    var codiceOpera = codiceOpera1;
                                } else {
                                    return $scope.showError("Error", "Il codice opera non è valido");
                                }
                            }


                            $http({
                                method: 'GET',
                                url: 'rest/performing/cruscotti/ricercaOpere',
                                params: {
                                    titoloOpera: titoloComposizione,
                                    autore: autore,
                                    codiceOpera: codiceOpera
                                }
                            }).then(function(response) {
                                if (response.data.length == 0) {
                                    $scope.showError("Ricerca Opera", "La ricerca non ha riportato nessun risultato");

                                } else {
                                    $scope.showRicerca = true;
                                    $scope.samplings = response.data;
                                }
                            }, function(response) {
                                $scope.operaAdded("Ricerca Opera", "C'è stato un problema nel ricercare l'opera", $scope);
                            })
                        };

                        $scope.saveOperaExcel = function() {

                            var fd = new FormData();

                            fd.append('Opere', $scope.excelOpere);
                            $http({
                                method: 'POST',
                                url: 'rest/performing/cruscotti/importOpere',
                                data: fd,
                                params: {
                                    idPm: $scope.idPM,
                                    numeroPm: $scope.numPM,
                                    userId: $scope.user
                                },
                                transformRequest: angular.identity,
                                headers: {
                                    'Content-Type': undefined
                                }
                            }).then(function(response) {
                                $scope.showError("Success", "Opere aggiunte con successo");
                            }, function(response) {
                                if ($scope.excelOpere == undefined) {
                                    $scope.showError("Errore", "Aggiungere un file prima di dare la conferma");
                                } else if (response.status == 500) {
                                    $scope.showError("Errore", response.data);
                                }
                            })
                        };
                        //modificare appena andrei aggiunge i campi nel backend
                        $scope.saveOperaManuale = function() {

                            if ($scope.nettoMaturato != undefined && $scope.nettoMaturato != "") {
                                if (new RegExp('[^0-9$]+').exec($scope.nettoMaturato)) {
                                    showError("Error", "Il netto maturato deve contenere solo numeri");
                                    return;
                                }
                            }

                            var durata = null;

                            if ($scope.durata != undefined && $scope.durata != "") {

                                durata = $scope.durata;

                                if (new RegExp('[^0-9:$]+').exec(durata)) {
                                    showError("Error", "La durata deve contenere solo numeri e ':'");
                                    return;
                                }

                            }

                            if ($scope.codiceOpera && $scope.codiceOpera !== "") {
                                if (!(new RegExp('[0-9]{11}').exec($scope.codiceOpera) && $scope.codiceOpera.length < 12)) {
                                    showError("Errore", "Il codice Opera non è valido. Deve essere lungo 11 caratteri e può contenere solo numeri");
                                    return;
                                }
                            }


                            if ($scope.durataInferiore30Sec && $scope.durataInferiore30Sec !== "") {
                                if (durata && durata !== "") {
                                    if ($scope.durataInferiore30Sec === 1 && durata > 30) {
                                        showError("Errore", "Il flag durata inferiore di 30 secondi non può essere positivo se la durata è maggiore di 30 secondi");
                                        return;
                                    } else if ($scope.durataInferiore30Sec === 0 && durata < 30) {
                                        showError("Errore", "Il flag durata inferiore di 30 secondi non può essere negativo se la durata è minore di 30 secondi");
                                        return;
                                    }
                                }
                            }

                            if ($scope.dataPrimaCodifica != undefined && $scope.dataPrimaCodifica != null) {
                                var day = $scope.dataPrimaCodifica.split('-')[0];
                                var month = $scope.dataPrimaCodifica.split('-')[1];
                                var year = $scope.dataPrimaCodifica.split('-')[2];
                            }

                            var opera = {
                                idProgrammaMusicale: $scope.idPM,
                                numeroProgrammaMusicale: $scope.numPM,
                                codiceOpera: $scope.codiceOpera,
                                titoloComposizione: $scope.titoloComposizione,
                                compositore: null,
                                compositori: $scope.compositori,
                                menodi30sec: $scope.durataInferiore30Sec,
                                autori: $scope.autori,
                                interpreti: $scope.interpreti,
                                combinazioneCodifica: $scope.combinazioneCodifica,
                                elaboratorePubblicoDominio: $scope.flagPD,
                                flagMagNonConcessa: $scope.flagMag,
                                nettoMaturato: $scope.nettoMaturato,
                                dataPrimaCodifica: new Date(year, month, day, 0, 0, 0, 0),
                                tipoAccertamento: $scope.tipoAccertamento,
                                durata: durata,
                                flagCodifica: $scope.flagCodifica,
                                flagPubblicoDominio: $scope.flagPD,
                                flagCedolaEsclusa: $scope.flagEscludiOpera
                            };
                            $scope.saveOpera(opera);
                        };

                        $scope.saveOperaSearch = function(sampling) {

                            var opera = {
                                idProgrammaMusicale: $scope.idPM,
                                numeroProgrammaMusicale: $scope.numPM,
                                codiceOpera: sampling.codiceOpera,
                                titoloComposizione: sampling.titolo,
                                compositore: sampling.compositore,
                                compositori: sampling.compositori,
                                durataEsecuzione: sampling.durataEsecuzione,
                                menodi30sec: sampling.menodi30sec,
                                autori: sampling.autori,
                                interpreti: sampling.interpreti,
                                elaboratorePubblicoDominio: sampling.elaboratorePubblicoDominio,
                                combinazioneCodifica: "",
                                flagMagNonConcessa: "",
                                nettoMaturato: "",
                                dataPrimaCodifica: "",
                                tipoAccertamento: "",
                                durataEsecuzione: sampling.durataEsecuzione,
                                flagCodifica: "",
                                flagCedolaEsclusa:""
                            };

                            $scope.saveOpera(opera);
                        };

                        $scope.saveOpera = function(opera) {
                            if (opera.codiceOpera == "" || opera.codiceOpera == undefined) {
                                opera.codiceOpera = null;
                            }
                            if (opera.durataEsecuzione == "" || opera.durataEsecuzione == undefined) {
                                opera.durataEsecuzione = null;
                            }
                            if (opera.menodi30sec == "" || opera.menodi30sec == undefined) {
                                opera.menodi30sec = null;
                            }
                            if (opera.interpreti == "" || opera.interpreti == undefined) {
                                opera.interpreti = null;
                            }
                            if (opera.elaboratorePubblicoDominio == "" || opera.elaboratorePubblicoDominio == undefined) {
                                opera.elaboratorePubblicoDominio = null;
                            }
                            if (opera.dataPrimaCodifica == "" || opera.dataPrimaCodifica == undefined) {
                                opera.dataPrimaCodifica = null;
                            }
                            if (opera.titoloComposizione == "" || opera.titolo == undefined) {
                                opera.titolo = null;
                            }
                            if (opera.compositori == "" || opera.compositori == undefined) {
                                opera.compositori = null;
                            }
                            if (opera.autori == "" || opera.autori == undefined) {
                                opera.autori = null;
                            }
                            if (opera.combinazioneCodifica == "" || opera.combinazioneCodifica == undefined) {
                                opera.combinazioneCodifica = null;
                            }
                            if (opera.flagMagNonConcessa == "" || opera.flagMagNonConcessa == undefined) {
                                opera.flagMagNonConcessa = null;
                            }
                            if (opera.nettoMaturato == "" || opera.nettoMaturato == undefined) {
                                opera.nettoMaturato = null;
                            }
                            if (opera.tipoAccertamento == "" || opera.tipoAccertamento == undefined) {
                                opera.tipoAccertamento = null;
                            }
                            if (opera.flagCodifica == "" || opera.flagCodifica == undefined) {
                                opera.flagCodifica = null;
                            }
                            if (opera.flagCedolaEsclusa == "" || opera.flagCedolaEsclusa == undefined) {
                                opera.flagCedolaEsclusa = null;
                            }

                            $http({
                                method: 'POST',
                                url: 'rest/performing/cruscotti/addOpera',
                                data: opera,
                                params: {
                                    userId: $scope.user
                                }
                            }).then(function(response) {
                                $scope.operaAdded("Aggiunta opera", "Opera aggiunta con successo");
                            }, function(response) {
                                $scope.operaAdded("Aggiunta opera", "C'è stato un problema, non è stato possibile aggiungere l'opera");
                            })
                        };

                        $scope.ctrl = {
                            title: "Aggiunta Opere"
                        };
                        $scope.operaAdded = function(title, msgs, scope) {
                            ngDialog.open({
                                template: 'pages/advance-payment/dialog-alert.html',
                                plain: false,
                                width: '60%',
                                controller: ['$scope', 'ngDialog', function($scope, scope) {
                                    $scope.ctrl = {
                                        title: title,
                                        message: msgs,
                                        ngDialogId: scope.ngDialogId
                                    };
                                    $scope.cancel = function(ngDialogId) {
                                        ngDialog.closeAll(ngDialogId);
                                    };
                                    $scope.save = function() {
                                        ngDialog.closeAll(true);
                                    };
                                }]
                            }).closePromise.then(function(data) {

                            });
                            ngDialog.closeAll(false);
                        };

                        $scope.cancel = function() {
                            ngDialog.closeAll(false);
                        };
                        $scope.save = function() {
                            ngDialog.closeAll(true);
                        };
                    }]
                }).closePromise.then(function(data) {
                    $scope.init();
                });
            };

            $scope.goTo = function(url, param) {
                if (url != undefined && param != undefined) {
                    $location.path(url + param);
                }
            };

            $scope.formatDurata = function(durata,durata30Sec,id){
                if(durata != undefined && durata != ""){
                    if(durata30Sec != undefined && durata30Sec != "" && id != undefined && id != ""){
                        if(durata.toString().trim() > 30 && durata30Sec == 1){
                            angular.element(id).css('color','red');
                        }
                        if(durata.toString().trim() < 30 && durata30Sec == 0){
                            angular.element(id).css('color','red');
                        }
                    }
                    // return $scope.sec2time(durata);
                    return $filter('date')(durata * 1000, 'mm:ss')
                }
            };

            $scope.formatDurataInferiore = function(durataInferiore) {
                if (durataInferiore != undefined && durataInferiore != '') {
                    if (durataInferiore == 1) {
                        return "Si";
                    }
                    if (durataInferiore == 0) {
                        return "No";
                    }
                }

            };

            $scope.formatFlagPD = function(flagPD) {
                if (flagPD != undefined && flagPD != '') {
                    if (flagPD == 1) {
                        return "Si";
                    }
                    if (flagPD == 0) {
                        return "No";
                    }
                }
            };

            $scope.modifyOpera = function(opera, durata, showError, user, init) {
                ngDialog.open({
                    template: 'pages/advance-payment/dialog-modify-opera.html',
                    plain: false,
                    width: '90%',
                    controller: ['$scope', 'ngDialog', '$http', function($scope, ngDialog, $http) {
                        $scope.opera = opera;
                        $scope.idOpera = opera.id;
                        $scope.codiceOpera = opera.codiceOpera;
                        $scope.durata = durata || '';
                        var durataOld = $scope.opera.durata;

                        if ($scope.opera.dataPrimaCodifica != null && $scope.opera.dataPrimaCodifica != undefined && $scope.opera.dataPrimaCodifica != "") {
                            var data = $scope.opera.dataPrimaCodifica.split("T")[0];
                            var anno = data.split("-")[0];
                            var mese = data.split("-")[1];
                            var giorno = data.split("-")[2];

                            $scope.dataPrimaCodifica = giorno + '-' + mese + '-' + anno;
                        }

                        if (opera.durataInferiore30sec == 1) {
                            $scope.durata30sec = "Si";
                        } else if (opera.durataInferiore30sec == 0) {
                            $scope.durata30sec = "No";
                        }

                        if (opera.flagPubblicoDominio == 1) {
                            $scope.flagPD = "Si";
                        } else if (opera.flagPubblicoDominio == 0) {
                            $scope.flagPD = "No";
                        }

                        if (opera.flagMagNonConcessa == 1) {
                            $scope.flagMagNonConcessa = "Si";
                        } else if (opera.flagMagNonConcessa == 0) {
                            $scope.flagMagNonConcessa = "No";
                        }

                        if (opera.flagCodifica == 1) {
                            $scope.flagCodifica = "Si";
                        } else if (opera.flagCodifica == 0) {
                            $scope.flagCodifica = "No";
                        }
                        if (opera.flagCedolaEsclusa == 1) {
                            $scope.flagCedolaEsclusa = "Si";
                        } else if (opera.flagCedolaEsclusa == 0) {
                            $scope.flagCedolaEsclusa = "No";
                        }

                        $scope.showError = showError;
                        $scope.user = user;
                        $scope.init = init;

                        $scope.cancel = function() {
                            ngDialog.closeAll(false);
                        };

                        $scope.save = function() {

                            if ($scope.newCodiceOpera != undefined && $scope.newCodiceOpera != "") {
                                if (new RegExp('[0-9]{11}').exec($scope.newCodiceOpera) && $scope.newCodiceOpera.length < 12) {

                                } else {
                                    showError("Errore", "Il codice Opera non è valido. Deve essere lungo 11 caratteri e può contenere solo numeri");
                                    return;
                                }
                            }

                            if ($scope.newNettoMaturato != undefined && $scope.newNettoMaturato != "") {
                                if (new RegExp('[^0-9$]+').exec($scope.newNettoMaturato)) {
                                    showError("Error", "Il netto maturato deve contenere solo numeri");
                                    return;
                                }
                            }

                            var newDurata = $scope.newDurata || '';


                            if ($scope.newDurata30sec != undefined && $scope.newDurata30sec !== "") {
                                if (newDurata != undefined && newDurata != "") {
                                    if ($scope.newDurata30sec == 1 && newDurata > 30) {
                                        $scope.showError("Errore", "Il flag durata inferiore di 30 secondi non può essere positivo se la durata è maggiore di 30 secondi");
                                        return;
                                    } else if ($scope.newDurata30sec == 0 && newDurata < 30) {
                                        $scope.showError("Errore", "Il flag durata inferiore di 30 secondi non può essere negativo se la durata è minore di 30 secondi");
                                        return;
                                    }
                                } else if (durataOld != undefined && durataOld != "") {
                                    if ($scope.newDurata30sec == 1 && durataOld > 30) {
                                        $scope.showError("Errore", "Il flag durata inferiore di 30 secondi non può essere positivo se la durata è maggiore di 30 secondi");
                                        return;
                                    } else if ($scope.newDurata30sec == 0 && durataOld < 30) {
                                        $scope.showError("Errore", "Il flag durata inferiore di 30 secondi non può essere negativo se la durata è minore di 30 secondi");
                                        return;
                                    }
                                }
                            } else if ($scope.durata30sec != undefined && $scope.durata30sec != "") {
                                if (newDurata != undefined && newDurata != "") {
                                    if ($scope.durata30sec == "Si" && newDurata > 30) {
                                        $scope.showError("Errore", "Il flag durata inferiore di 30 secondi non può essere positivo se la durata è maggiore di 30 secondi");
                                        return;
                                    } else if ($scope.durata30sec == "No" && newDurata < 30) {
                                        $scope.showError("Errore", "Il flag durata inferiore di 30 secondi non può essere negativo se la durata è minore di 30 secondi");
                                        return;
                                    }
                                } else if (durataOld != undefined && durataOld != "") {
                                    if ($scope.durata30sec == "Si" && durataOld > 30) {
                                        $scope.showError("Errore", "Il flag durata inferiore di 30 secondi non può essere positivo se la durata è maggiore di 30 secondi");
                                        return;
                                    } else if ($scope.durata30sec == "No" && durataOld < 30) {
                                        $scope.showError("Errore", "Il flag durata inferiore di 30 secondi non può essere negativo se la durata è minore di 30 secondi");
                                        return;
                                    }
                                }
                            }

                            if (newDurata == undefined || newDurata == "") {
                                newDurata = 0;
                            }
                            if ($scope.newDurata30sec == undefined || $scope.newDurata30sec == "") {
                                var newDurata30sec = null;
                            } else {
                                var newDurata30sec = $scope.newDurata30sec;
                            }
                            if ($scope.newFlagPD == undefined || $scope.newFlagPD == "") {
                                var newFlagPD = null;
                            } else {
                                var newFlagPD = $scope.newFlagPD;
                            }
                            if ($scope.newFlagCedolaEsclusa == undefined || $scope.newFlagCedolaEsclusa == "") {
                                var newFlagCedolaEsclusa = null;
                            } else {
                                var newFlagCedolaEsclusa = $scope.newFlagCedolaEsclusa;
                            }

                            var newOpera = {
                                id: $scope.idOpera
                            };

                            var valid = false;

                            if ($scope.newCodiceOpera != undefined && $scope.newCodiceOpera != "") {
                                newOpera.codiceOpera = $scope.newCodiceOpera;
                                valid = true;
                            }
                            if ($scope.newTitoloComposizione != undefined && $scope.newTitoloComposizione != "") {
                                newOpera.titoloComposizione = $scope.newTitoloComposizione;
                                valid = true;
                            }
                            if ($scope.newCompositore != undefined && $scope.newCompositore != "") {
                                newOpera.compositori = $scope.newCompositore;
                                valid = true;
                            }
                            if (newDurata != undefined && newDurata != "") {
                                newOpera.durata = newDurata;
                                valid = true;
                            }
                            if (newDurata30sec != undefined && newDurata30sec != "") {
                                newOpera.durataInferiore30sec = newDurata30sec;
                                valid = true;
                            }
                            if ($scope.newAutori != undefined && $scope.newAutori != "") {
                                newOpera.autori = $scope.newAutori;
                                valid = true;
                            }
                            if ($scope.newInterpreti != undefined && $scope.newInterpreti != "") {
                                newOpera.interpreti = $scope.newInterpreti;
                                valid = true;
                            }
                            if ($scope.newFlagCodifica != undefined && $scope.newFlagCodifica != "") {
                                newOpera.flagCodifica = $scope.newFlagCodifica;
                                valid = true;
                            }
                            if ($scope.newFlagPD != undefined && $scope.newFlagPD != "") {
                                newOpera.flagPubblicoDominio = $scope.newFlagPD;
                                valid = true;
                            }
                            if ($scope.newFlagCedolaEsclusa != undefined && $scope.newFlagCedolaEsclusa != "") {
                                newOpera.flagCedolaEsclusa = $scope.newFlagCedolaEsclusa;
                                valid = true;
                            }

                            if ($scope.newCombinazioneCodifica != undefined && $scope.newCombinazioneCodifica != "") {
                                newOpera.combinazioneCodifica = $scope.newCombinazioneCodifica;
                                valid = true;
                            }
                            if ($scope.newFlagMagNonConcessa != undefined && $scope.newFlagMagNonConcessa != "") {
                                newOpera.flagMagNonConcessa = $scope.newFlagMagNonConcessa;
                                valid = true;
                            }
                            if ($scope.newNettoMaturato != undefined && $scope.newNettoMaturato != "") {
                                newOpera.nettoMaturato = $scope.newNettoMaturato;
                                valid = true;
                            }
                            if ($scope.newDataPrimaCodifica != undefined && $scope.newDataPrimaCodifica != "") {
                                newOpera.dataPrimaCodifica = $scope.newDataPrimaCodifica;
                                valid = true;
                            }
                            if ($scope.newTipoAccertamento != undefined && $scope.newTipoAccertamento != "") {
                                newOpera.tipoAccertamento = $scope.newTipoAccertamento;
                                valid = true;
                            }

                            if (!valid) {
                                $scope.showError("Modifica Opera", "Inserire almeno un campo prima di salvare");
                                return;
                            }

                            $http({
                                method: 'PUT',
                                url: 'rest/performing/cruscotti/updateOpera',
                                params: {
                                    userId: $scope.user
                                },
                                data: newOpera
                            }).then(function(response) {
                                $scope.showError("Modifica opera", "Opera modificata con successo");
                                $scope.init();
                            }, function(response) {
                                $scope.showError("Modifica opera", "Non è stato possibile modificare l'opera");
                            });

                            ngDialog.closeAll(true);
                        };
                    }]
                }).closePromise.then(function(data) {

                });

            };

            $scope.formatHour = function(hour) {
                if (hour != null) {
                    var ora = hour.slice(0, 2) + ":" + hour.slice(2, 4);
                    return ora;
                }

            };

            $scope.formatFormato = function(formato) {
                if (formato === "DI") {
                    return "Digitale";
                } else if (formato === "A3") {
                    return "Cartaceo A3";
                } else if (formato === "A4") {
                    return "Cartaceo A4";
                }
            };

            function getCandidate(opera, user) {
                if(!$scope.codifica || !$scope.codifica[opera.idCombana]){
                    programmaMusicaleService.getCandidate(opera.idCombana, user)
                        .then(function (response) {
                            if (!$scope.codifica)
                                $scope.codifica = [];
                            if(response && response !== ''){
                                $scope.codifica[opera.idCombana] = response;
                                $scope.codifica[opera.idCombana].candidate = angular.fromJson($scope.codifica[opera.idCombana].candidate);
                            }
                            selectCandidate(opera.idCombana);
                        });
                }else{
                    selectCandidate(opera.idCombana);
                }
            }

            function selectCandidate(idCombana) {
                for (var key in $scope.codifica) {
                    $scope.codifica[key].hidden = (parseInt(key) === idCombana ? ($scope.codifica[key].hidden === undefined ? false : !$scope.codifica[key].hidden) : true);
                }
                $scope.opereDaApprovare = {};
            }

            function addOperaCandidata(candidata, opera, index) {
                !$scope.opereDaApprovare[opera.idCombana] || $scope.opereDaApprovare[opera.idCombana]['codApprov'] !== candidata.codiceOpera ?
                    $scope.opereDaApprovare[opera.idCombana] = {'codApprov': candidata.codiceOpera, 'idCodifica': $scope.codifica[opera.idCombana].idCodifica, 'statApprov': index === 0 ? 'P' : 'A'} :
                    $scope.opereDaApprovare = {};
            }

            $scope.init = function() {
                /*$http({
                    method: 'GET',
                    url: 'rest/performing/cruscotti/pmDetail',
                    params: {
                        idMovimento: $scope.idMovimento,
                        idProgrammaMusicale: $scope.idProgrammaMusicale
                    }
                })*/
                programmaMusicaleService.getPMDetail($scope.idProgrammaMusicale, $scope.idMovimento).then(function(response) {
                    $scope.opereDaApprovare = {};
                    $scope.codifica = undefined;
                    $scope.sampling = response.data;
                    $scope.eventi = response.data.eventi;
                    $scope.opere = response.data.opere;
                    $scope.movimenti = response.data.movimenti;
                    $scope.importo = response.data.importo;
                    $scope.direttoreEsecuzione = response.data.direttoreEsecuzione;
                    $scope.flagGruppoPrincipaleSpalla = response.data.flagGruppoPrincipale;
                    $scope.totaleImportoSun = 0;
                    $scope.totaleImportoRicalcolato = 0;
                    $scope.flagDaCampionare = response.data.flagDaCampionare;
                    $scope.numProgrammaMusicale = $scope.sampling.numeroProgrammaMusicale;
                    if ($scope.sampling.foglio == "DI") {
                        angular.element("#flagCampionamento").prop('disabled', true);
                        angular.element("#flagButton").prop('disabled', true);
                    }
                    if ($scope.sampling.voceIncasso != "2244") {
                        angular.element("#flagGPS").prop('disabled', true);
                        angular.element("#buttonGPS").prop('disabled', true);
                    }
                    angular.forEach($scope.movimenti, function(value, key) {
                        if (value.tipoDocumentoContabile == 221) {
                            $scope.totaleImportoRicalcolato = $scope.totaleImportoRicalcolato - value.importoValorizzato;
                            $scope.totaleImportoSun = $scope.totaleImportoSun - value.importoTotDemLordo;
                        } else if (value.tipoDocumentoContabile == 501) {
                            $scope.totaleImportoRicalcolato = $scope.totaleImportoRicalcolato + value.importoValorizzato;
                            $scope.totaleImportoSun = $scope.totaleImportoSun + value.importoTotDemLordo;
                        }

                    })
                })
            };

            function isValid(){
                return ($scope.opereDaApprovare && Object.keys($scope.opereDaApprovare).length > 0);
            }

            function invia() {
                programmaMusicaleService.askConfirm("Attenzione","Sei sicuro di aver identificato il giusto abbinamento?")
                    .closePromise.then(function(model){
                    if(model.value){
                        return CMEService.sceltaProposte(Object.values($scope.opereDaApprovare),$scope.user).then(function(){
                            $scope.showError("Successo","L'abbinamento della proposta con le combane è avvenuto con successo");
                            $scope.init();
                        },function(response){
                            if(response.data === 520){
                                $scope.showError("Errore","Non è stato possibile associare la proposta alla combana: Sessione scaduta, per continuare a codificare effettua una nuova ricerca");
                            }
                            $scope.showError("Errore","Non è stato possibile associare la proposta alla combana");
                            return false;
                        })
                    }
                })
            }

            $scope.showError = function(title, msgs) {
                ngDialog.open({
                    template: 'pages/advance-payment/dialog-alert.html',
                    plain: false,
                    width: '60%',
                    controller: ['$scope', 'ngDialog', function($scope, ngDialog) {
                        $scope.ctrl = {
                            title: title,
                            message: msgs
                        };
                        $scope.cancel = function() {
                            ngDialog.close($scope.ngDialogId);
                        };
                        $scope.save = function() {
                            ngDialog.closeAll(true);
                        };
                    }]
                }).closePromise.then(function(data) {

                });
            };
            $scope.back = function() {
                window.history.back();
            }

        }]).directive('fileModel', ['$parse', function($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function() {
                scope.$apply(function() {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);