/**
 * FilterDspCountryUtilizationCtrl
 * 
 */
codmanApp.controller('FilterDspCountryUtilizationCtrl', ['$scope','$route', 'ngDialog', '$routeParams', '$location', '$http', function($scope,$route, ngDialog, $routeParams, $location, $http) {

	$scope.ctrl = {
		hideForm: true,
		values: [ ]
	};		
			
	
	// Funzione principale richiamata al caricamento della pagina
	$scope.onRefresh = function() {
		
		if(typeof $routeParams.hideForm !== 'undefined'){			
			if(typeof $routeParams.hideForm === 'boolean'){
				$scope.ctrl.hideForm = $routeParams.hideForm;
			}else{				
				$scope.ctrl.hideForm = 	$scope.$eval($routeParams.hideForm);
			}
		}else{
			$scope.ctrl.hideForm = true;
		}
		
		$http({
			method: 'GET',
			url: 'rest/data/dspCountriesUtilizations'
		}).then(function successCallback(response) {
			$scope.ctrl.values = response.data;
			$scope.ctrl.values.dsp.splice(0, 0, {idDsp: '*', name: '*'});
			$scope.ctrl.values.countries.splice(0, 0, {idCountry: '*', name: '*'});
			$scope.ctrl.values.utilizations.splice(0, 0, {idUtilizationType: '*', name: '*'});
		}, function errorCallback(response) {
			 $scope.ctrl.values = [ ];
		});
		
		
		if($scope.ctrl.hideForm === false){
			$scope.selectedDsp = {idDsp: $routeParams.dsp};
			$scope.selectedCountry = {idCountry: $routeParams.country};
			$scope.selectedUtilization = {idUtilizationType: $routeParams.utilization};
			}
	};
			

	$scope.apply= function(selectedDsp, selectedCountry, selectedUtilization)
	{	
		var parameters={
				
				dsp : typeof selectedDsp !== 'undefined' ?  selectedDsp.idDsp : '*',				
				country : typeof selectedCountry !== 'undefined' ?  selectedCountry.idCountry : '*',
				utilization : typeof selectedUtilization !== 'undefined' ? selectedUtilization.idUtilizationType : '*'
		}
	    $scope.$emit('filterApply-DCU', parameters);
	}


	
	$scope.onRefresh();
	
}]);