/**
 * FilterInvoiceCtrl
 *
 */
codmanApp.controller('FilterInvoiceSearchCtrl', ['$scope','$route', 'ngDialog', '$routeParams', '$location', '$http', '$q', 'configService',
	function($scope,$route, ngDialog, $routeParams, $location, $http, $q, configService) {

	$scope.ctrl = {
		hideForm: false,
		dspList: [],
		clientList: [],
		statusList : [ ],
		msInvoiceApiUrl: `${$location.protocol()}://${$location.host()}:${$location.port()}/${configService.msInvoiceApiPath}`
	};


	// Funzione principale richiamata al caricamento della pagina
	$scope.onRefresh = function() {
		if(typeof $routeParams.hideForm !== 'undefined'){
			if(typeof $routeParams.hideForm === 'boolean'){
				$scope.ctrl.hideForm = $routeParams.hideForm;
			}else{
				$scope.ctrl.hideForm = 	$scope.$eval($routeParams.hideForm);
			}
		}else{
			$scope.ctrl.hideForm = true;
		}


		var promise0 = $http({method: 'GET', url: $scope.ctrl.msInvoiceApiUrl + 'anagClient/clients'});
		// var promise1 = $http({method: 'GET', url: 'rest/data/dspList'});
		var promise2 = $http({method: 'GET', url: 'rest/data/invoiceStatusCodes'});
		$q.all([promise0, promise2]).then(function successCallback(response) {
			$scope.ctrl.clientList = response[0].data;
			$scope.ctrl.statusList = response[1].data;
			
			buildKeySets();

			}, function errorCallback(response) {
				$scope.ctrl.dspList = [ ];
				$scope.ctrl.clientList = [ ];
				$scope.ctrl.statusList = [ ];
		
			}); 

	};


	$scope.filterDsp = function(anagCcid){
		$scope.dspSelectionList = [];
		$scope.selectedDspModel = [];
			angular.forEach(anagCcid.dspList, function(dsp){
				$scope.dspSelectionList.push({
					label: dsp.name,
					id:  dsp.idDsp
				});
			});


		};

	buildKeySets = function() {
		$scope.dspSelectionList = [];
		$scope.clientSelectionList = [];
		_.forEach($scope.ctrl.dspList, function(value) {

 			 $scope.dspSelectionList.push({
	    		label: value.name,
	   			id:  value.idDsp
	   		});
		});

		_.forEach($scope.ctrl.clientList, function(value) {

			$scope.clientSelectionList.push({
				label: value.companyName,
				id:  value.idAnagClient,
				dspList: value.dspList
			});
		});

		$scope.statusSelectionList = []
		_.forEach($scope.ctrl.statusList, function(value) {

 			 $scope.statusSelectionList.push({
	    		label: value.description,
	   			id:   value.code
	   		});
		});



	};


	getCollectionValues = function(collection) {
		var selectedList = []
		if(collection !== 'undefined'){

			for (var e in collection) {

		    	selectedList.push(collection[e].id)
		    }
		}
		return selectedList
	}

	$scope.apply= function(selectedClientCollection, selectedDspCollection, selectedStatusCollection, invoiceCode, selectedPeriodFrom, selectedPeriodTo )	{
		
		var dateTo = undefined
		if(selectedPeriodTo !== undefined){
			var trimmed =selectedPeriodTo.split("-")
			dateTo = new Date(parseInt(trimmed[0],10),parseInt(trimmed[1],10)-1,parseInt(trimmed[2],10),23,59,59)
		}
		var parameters={
				clientList: selectedClientCollection,
				dspList:  getCollectionValues(selectedDspCollection),
				statusList: getCollectionValues(selectedStatusCollection),
				dateFrom : selectedPeriodFrom ,
				dateTo : dateTo,
				invoiceCode:invoiceCode
		}
	    $scope.$emit('filterApply', parameters);
	}

	$scope.onRefresh();

	$scope.selectedDspModel = [];
	$scope.selectedClientModel = [];
	$scope.selectedStatusModel = [];
	
	

    $scope.multiselectSettings = {
        scrollableHeight: '200px',
        scrollable: false,
		enableSearch: true,
		smartButtonMaxItems: 2		
    };


}]);




codmanApp.filter('year_range', function() {
    	  return function(input, min, max) {
    		    min = parseInt(min); //Make string input int
    		    max = parseInt(max);
    		    for (var i=min; i<=max; i++)
    		      input.push(i);
    		    return input;
    		  };
    	});