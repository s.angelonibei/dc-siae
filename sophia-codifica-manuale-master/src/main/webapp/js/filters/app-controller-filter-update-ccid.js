/**
 * FilterInvoiceCtrl
 *
 */
codmanApp.controller('FilterUpdateCcidCtrl', ['$scope','$route', 'ngDialog', '$routeParams', '$location', '$http', '$q', 'configService', 'anagClientService',
	function($scope,$route, ngDialog, $routeParams, $location, $http, $q,configService, anagClientService) {

	$scope.filterDsp = anagClientService.filterDsp;
	$scope.ctrl = {
		hideForm: false,
		dsplist: [],
		utilizationList : [ ],
		commercialOffersList : [ ],
		countryList : [ ],
		months : [{"id":"1","name":"Gennaio"},{"id":"2","name":"Febbraio"},{"id":"3","name":"Marzo"}, {"id":"4","name":"Aprile"},
		{"id":"5","name":"Maggio"},{"id":"6","name":"Giugno"},{"id":"7","name":"Luglio"},{"id":"8","name":"Agosto"},
		{"id":"9","name":"Settembre"},{"id":"10","name":"Ottobre"},{"id":"11","name":"Novembre"},{"id":"12","name":"Dicembre"}]
	};





	// Funzione principale richiamata al caricamento della pagina
	$scope.onRefresh = function() {

		anagClientService.getClientsData().then(function (data){
			$scope.ctrl.anagClientData = angular.copy(data[0]);
		});
//		if(typeof $routeParams.hideForm !== 'undefined'){
//			if(typeof $routeParams.hideForm === 'boolean'){
//				$scope.ctrl.hideForm = $routeParams.hideForm;
//			}else{
//				$scope.ctrl.hideForm = 	$scope.$eval($routeParams.hideForm);
//			}
//		}else{
//			$scope.ctrl.hideForm = true;
//		}


		$http({
		method: 'GET',
		url: 'rest/data/dspUtilizationsOffersCountries'
		}).then(function successCallback(response) {

			$scope.ctrl.dspList =  response.data.dsp;
			$scope.ctrl.utilizationList = response.data.utilizations;
			$scope.ctrl.commercialOffersList = response.data.commercialOffers;
			$scope.ctrl.countryList = response.data.countries;

			buildKeySets();

			}, function errorCallback(response) {
				$scope.ctrl.dspList = [ ];
				$scope.ctrl.utilizationList = [ ];
				$scope.ctrl.commercialOffersList = [ ];
				$scope.ctrl.countryList = [ ];
			});

	};


	buildKeySets = function() {
				$scope.countrySelectionList = []
		_.forEach($scope.ctrl.countryList, function(value) {

 			 $scope.countrySelectionList.push({
	    		label: value.name,
	   			id:  value.idCountry
	   		});
		});

				$scope.utilizationSelectionList = []
		_.forEach($scope.ctrl.utilizationList, function(value) {

 			 $scope.utilizationSelectionList.push({
	    		label: value.name,
	   			id:  value.idUtilizationType
	   		});
		});

				$scope.offerSelectionList = []
		_.forEach($scope.ctrl.commercialOffersList, function(value) {

 			 $scope.offerSelectionList.push({
	    		label: value.offering,
	   			id:  value.offering
	   		});
		});

	};

	$scope.applyFilterDsp = function(anagCcid){
		$scope.ctrl.dspList = anagCcid.dspList;
	};


	getCollectionValues = function(collection) {
		var selectedList = []
		if(collection !== 'undefined'){

			for (var e in collection) {

		    	selectedList.push(collection[e].id)
		    }
		}
		return selectedList
	};

	$scope.apply= function(selectedClient, selectedDsp, selectedCountryCollection, selectedUtilizationCollection,
	selectedOfferCollection, selectedPeriodFrom, selectedPeriodTo)	{
	    var periodFrom = 0;
	    var yearFrom = 0;
        var periodTo = 0;
        var yearTo = 0;

	    if((selectedPeriodFrom !== undefined)){
            var from = selectedPeriodFrom.split("-");
            periodFrom = parseInt(from[0],10);
            yearFrom = parseInt(from[1],10);
	    }

	    if(selectedPeriodTo !== undefined){
            var to = selectedPeriodTo.split("-");
            periodTo = parseInt(to[0],10);
            yearTo = parseInt(to[1],10);

	    }
	    
		var parameters={
	    		client: selectedClient,
				dsp: selectedDsp,
				countryList : getCollectionValues(selectedCountryCollection),
				utilizationList : getCollectionValues(selectedUtilizationCollection),
				offerList : getCollectionValues(selectedOfferCollection),
				monthFrom : periodFrom ,
				yearFrom : yearFrom ,
				monthTo : periodTo,
				yearTo : yearTo,
				hideForm : false
		}
	    $scope.$emit('filterApply', parameters);
	}

	$scope.onRefresh();

	$scope.selectedCountryModel = [];
	$scope.selectedUtilizationModel = [];
	$scope.selectedOfferModel = [];


    $scope.multiselectSettings = {
        scrollableHeight: '200px',
        scrollable: false,
        enableSearch: true
    };


}]);




codmanApp.filter('year_range', function() {
    	  return function(input, min, max) {
    		    min = parseInt(min); //Make string input int
    		    max = parseInt(max);
    		    for (var i=min; i<=max; i++)
    		      input.push(i);
    		    return input;
    		  };
    	});