/**
 *
 * FilterDspUtilizationOfferCtrl
 * 
 */
codmanApp.controller('FilterDspUtilizationOfferCtrl', ['$scope','$route', 'ngDialog', '$routeParams', '$location', '$http', '$q', function($scope,$route, ngDialog, $routeParams, $location, $http, $q) {

	$scope.ctrl = {
		hideForm: false,
		values: [ ]
	};		

	$scope.backup = {};
	
	// Funzione principale richiamata al caricamento della pagina
	$scope.onRefresh = function() {
		if(typeof $routeParams.hideForm !== 'undefined'){			
			if(typeof $routeParams.hideForm === 'boolean'){
				$scope.ctrl.hideForm = $routeParams.hideForm;
			}else{				
				$scope.ctrl.hideForm = 	$scope.$eval($routeParams.hideForm);
			}
		}else{
			$scope.ctrl.hideForm = true;
		}
		
		var promise1 = $http({method: 'GET', url: 'rest/data/allAvailableOffers'}); 
		var promise2 = $http({method: 'GET', url: 'rest/data/dspUtilizations'}); 
		$q.all([promise1, promise2]).then(function successCallback(response) {						
			$scope.ctrl.commercialOffersList = response[0].data;
			$scope.ctrl.commercialOffersList.splice(0, 0, {idCommercialOffering: '*', offering:'*', _star: true});
			$scope.ctrl.dspList =  response[1].data.dsp; 
			$scope.ctrl.dspList.splice(0, 0, {idDsp: '*', name: '*', _star: true});
			$scope.ctrl.utilizationList = response[1].data.utilizations;
			$scope.ctrl.utilizationList.splice(0, 0, {idUtilizationType: '*', name: '*', _star: true});

			$scope.backup.dspList = angular.copy($scope.ctrl.dspList);
			$scope.backup.utilizationList = angular.copy($scope.ctrl.utilizationList);
			$scope.backup.commercialOffersList = angular.copy($scope.ctrl.commercialOffersList);
			$scope.onDspSelection();
			$scope.onUtilizationSelection();
			
			}, function errorCallback(response) {
				$scope.ctrl.dspList = [ ];
				$scope.ctrl.utilizationList = [ ];
				$scope.ctrl.commercialOffersList = [ ];
			}); 
		
		if($scope.ctrl.hideForm === false){
			$scope.selectedDsp = {idDsp: $routeParams.dsp};
			$scope.selectedUtilization = {idUtilizationType: $routeParams.utilization};
			$scope.selectedOffer = {idCommercialOffering: $routeParams.offer};
		}
	};
			

	$scope.apply= function(selectedDsp, selectedUtilization, selectedOffer)	{
		const parameters = {
			dsp: typeof selectedDsp !== 'undefined' ? selectedDsp.idDsp : '*',
			utilization: typeof selectedUtilization !== 'undefined' ? selectedUtilization.idUtilizationType : '*',
			offer: typeof selectedOffer !== 'undefined' ? selectedOffer.idCommercialOffering : '*',
			hideForm: false
		};
		$scope.$emit('filterApply', parameters);
	};

	$scope.onDspSelection = function() {
		 if (!$scope.selectedDsp || $scope.selectedDsp._star) {
			$scope.ctrl.utilizationList = angular.copy($scope.backup.utilizationList);
			$scope.ctrl.commercialOffersList = angular.copy($scope.backup.commercialOffersList);
			return;
		}

		$scope.ctrl.commercialOffersList = $scope.backup.commercialOffersList.filter(offer => offer.idDSP === $scope.selectedDsp.idDsp || offer._star);
		$scope.ctrl.utilizationList = $scope.backup.utilizationList.filter(utilization =>
			$scope.ctrl.commercialOffersList.map(offer => offer.idUtilizationType).includes(utilization.idUtilizationType) || utilization._star);
	};

	$scope.onUtilizationSelection = function() {
		if ($scope.selectedUtilization && $scope.selectedUtilization._star) {
			$scope.onDspSelection();
			return;
		}
    if($scope.selectedUtilization && $scope.selectedUtilization.idUtilizationType !='*'){
		$scope.ctrl.commercialOffersList = $scope.backup.commercialOffersList.filter(offer => offer.idDSP === $scope.selectedDsp.idDsp || offer._star);
		$scope.ctrl.commercialOffersList = $scope.ctrl.commercialOffersList.filter(offer => offer.idUtilizationType === $scope.selectedUtilization.idUtilizationType || offer._star);
		}
	};
	
	$scope.onRefresh();
	
}]);