/**
 * FilterDspUtilizationOfferCountryCtrl
 * 
 */
codmanApp.controller('FilterDspUtilizationOfferCountryCtrl', ['$scope','$route', 'ngDialog', '$routeParams', '$location', '$http', '$q', function($scope,$route, ngDialog, $routeParams, $location, $http, $q) {

	$scope.ctrl = {
		hideForm: false,
		values: [ ],
		dspList : [ ],
		utilizationList : [ ],
		commercialOffersList : [ ],
		countryList : [ ]
	};		
			
	
	// Funzione principale richiamata al caricamento della pagina
	$scope.onRefresh = function() {
		if(typeof $routeParams.hideForm !== 'undefined'){	
			if(typeof $routeParams.hideForm === 'boolean'){
				$scope.ctrl.hideForm = $routeParams.hideForm;
			}else{				
				$scope.ctrl.hideForm = 	$scope.$eval($routeParams.hideForm);
			}
		}else{
			$scope.ctrl.hideForm = true;
		}

		
		$http({
		method: 'GET',
		url: 'rest/data/dspUtilizationsOffersCountries'
		}).then(function successCallback(response) {						
			$scope.ctrl.dspList =  response.data.dsp; 
			$scope.ctrl.dspList.splice(0, 0, {idDsp: '*', name: '*'});
			$scope.ctrl.utilizationList = response.data.utilizations;
			$scope.ctrl.utilizationList.splice(0, 0, {idUtilizationType: '*', name: '*'});
			$scope.ctrl.commercialOffersList = response.data.commercialOffers;
			$scope.ctrl.commercialOffersList.splice(0, 0, {idCommercialOffering: '*', offering:'*'});
			$scope.ctrl.countryList = response.data.countries;
			$scope.ctrl.countryList.splice(0, 0, {idCountry: '*', name:'*'});

			
			}, function errorCallback(response) {
				$scope.ctrl.dspList = [ ];
				$scope.ctrl.utilizationList = [ ];
				$scope.ctrl.commercialOffersList = [ ];
				$scope.ctrl.countryList = [ ];
			}); 
		
		if($scope.ctrl.hideForm === false){
			$scope.selectedDsp = {idDsp: $routeParams.dsp};
			$scope.selectedUtilization = {idUtilizationType: $routeParams.utilization};
			$scope.selectedOffer = {idCommercialOffering: $routeParams.offer};
			$scope.selectedCountry = {idCountry: $routeParams.country};
		}else{
			
			$scope.selectedDsp = {idDsp: '*'};
			$scope.selectedUtilization = {idUtilizationType: '*'};
			$scope.selectedOffer = {idCommercialOffering: '*'};
			$scope.selectedCountry = {idCountry: '*'};
		}
	};
			
	$scope.filterUtilizationByDsp = function () {
	    return function (item) {
	    	if((typeof $scope.selectedDsp === 'undefined'))
	    		return false;
	    	
	    	if($scope.selectedDsp.idDsp === '*'){
	    		return true;
	    	}
	    	
	    	for (var i = 0; i < $scope.ctrl.commercialOffersList.length; i++) { 
	    		  if (($scope.ctrl.commercialOffersList[i].idDSP == $scope.selectedDsp.idDsp &&
	    				  item.idUtilizationType == $scope.ctrl.commercialOffersList[i].idUtilizationType) || item.idUtilizationType == '*')
			        {   
			            return true;
			        }
	    	}

	    	 return false;
	      
	    };
	};
	
	$scope.filterOffersByDspAndUtilization = function () {
	    return function (item) {
	    	    	
	    	if( (typeof $scope.selectedDsp === 'undefined') ||  (typeof $scope.selectedUtilization === 'undefined'))
	    		return false;
	   
	    		if($scope.selectedDsp.idDsp === '*' && item.idCommercialOffering !='*'){
	    			return false;
	    		}else{
	    			if ((item.idDSP == $scope.selectedDsp.idDsp && item.idUtilizationType == $scope.selectedUtilization.idUtilizationType) 
		    				  || item.idCommercialOffering =='*'){   
				            return true;
				        }else{
				        	return false;
				        }
	    		}
	      
	    };
	};

	$scope.resetOffer = function(){
		if(($scope.selectedDsp.idDsp === '*' ||  (typeof $scope.selectedUtilization !== 'undefined' && $scope.selectedUtilization.idUtilizationType === '*' )) && $scope.selectedOffer !=='*'){

    		$scope.selectedOffer = {idCommercialOffering: '*'};
    	}	
	}
	
	$scope.apply= function(selectedDsp, selectedUtilization, selectedOffer, selectedCountry)
	{	
		var parameters={
				dsp : typeof selectedDsp !== 'undefined' ?  selectedDsp.idDsp : '*',
				utilization : typeof selectedUtilization !== 'undefined' ? selectedUtilization.idUtilizationType : '*',
				offer : typeof selectedOffer !== 'undefined' ?  selectedOffer.idCommercialOffering : '*',
				country : typeof selectedCountry !== 'undefined' ?  selectedCountry.idCountry: '*',
				hideForm : false		
		}
	    $scope.$emit('filterApply', parameters);
	}


	
	$scope.onRefresh();
	
}]);