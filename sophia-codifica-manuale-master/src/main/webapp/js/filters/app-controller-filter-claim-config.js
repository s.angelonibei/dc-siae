/**
 * FilterClaimConfigCtrl
 * 
 */
codmanApp.controller('FilterClaimConfigCtrl', ['$scope', '$route', 'ngDialog', '$routeParams', '$location', '$http',
	function ($scope, $route, ngDialog, $routeParams, $location, $http) {

		var parentCtrl = $scope.ctrl;
		$scope.ctrl = _.extend({
			hideForm: true,
			values: []
		}, parentCtrl);

		// Funzione principale richiamata al caricamento della pagina
		$scope.onRefresh = function () {

			if (typeof $routeParams.hideForm !== 'undefined') {
				if (typeof $routeParams.hideForm === 'boolean') {
					$scope.ctrl.hideForm = $routeParams.hideForm;
				} else {
					$scope.ctrl.hideForm = $scope.$eval($routeParams.hideForm);
				}
			} else {
				$scope.ctrl.hideForm = true;
			}


			if ($scope.ctrl.hideForm === false) {
				$scope.selectedDsp = { idDsp: $routeParams.dsp };
				$scope.selectedCountry = { idCountry: $routeParams.country };
				$scope.selectedUtilization = { idUtilizationType: $routeParams.utilization };
				$scope.selectedOffer = { idCommercialOffering: $routeParams.offer };
				$scope.selectedUuid = $routeParams.uuid;
			}
		};

		$scope.apply = function (selectedUuid, selectedDsp, selectedCountry, selectedUtilization, selectedOffer) {
			var parameters = {
				uuid: selectedUuid || '',
				dsp: typeof selectedDsp !== 'undefined' ? selectedDsp.idDsp : '*',
				country: typeof selectedCountry !== 'undefined' ? selectedCountry.idCountry : '*',
				utilization: typeof selectedUtilization !== 'undefined' ? selectedUtilization.idUtilizationType : '*',
				offer: typeof selectedOffer !== 'undefined' ? selectedOffer.idCommercialOffering : '*',
			}
			$scope.$emit('filterApply-ClaimConfig', parameters);
		}

		$scope.onRefresh();

	}]);