/**
 * UtentiCtrl
 * 
 * @path /utenti
 */
codmanApp.controller('UtentiCtrl', [ '$scope', '$route', 'ngDialog', '$routeParams', '$location', '$http', 'Upload', 'configService', function($scope, $route, ngDialog, $routeParams, $location, $http, Upload, configService) {

	$scope.ctrl = {
		navbarTab : 'utenti',
		utenti : [],
		execute : $routeParams.execute,
		fiteredName : "",
		listaCanali : [],
		fiteredType : "",
		fiterBroadcasterName : "",
		fiterBroadcasterType : {},
		tipiEmittenti : [
			{
				"nome" : "TUTTI"
			},{
				"nome" : "TELEVISIONE"
			}, {
				"nome" : "RADIO"
					
			} ],
		sortType:'canale.nome',
		sortOrder:'canale.nome'
	};

	// Funzione principale richiamata al caricamento della pagina
	$scope.onRefresh = function() {
		$scope.getEmittenti()
	};
	$scope.back = function() {
		$scope.ctrl.selectedBroadcaster = null;
		$scope.getEmittenti()
	};
	$scope.getEmittenti = function() {
		var tipoBroadcaster=""
		if($scope.ctrl.fiterBroadcasterType.nome=="TUTTI"){
			tipoBroadcaster=""
		}else{
			tipoBroadcaster=$scope.ctrl.fiterBroadcasterType.nome;
		}
		$http({
			method : 'GET',
			url : 'rest/broadcasting/emittentiFiltrati',
			params : {
				fiteredName : $scope.ctrl.fiterBroadcasterName,
				fiteredType : tipoBroadcaster
			}
		}).then(function successCallback(response) {
			$scope.ctrl.emittenti = response.data;
		}, function errorCallback(response) {
			$scope.ctrl.emittenti = [];
		});

	};
	
	$scope.sort = function(sort) {
		if(sort==="TipoEmittente"){
			if($scope.ctrl.sortType==="+tipo_broadcaster"){
				$scope.ctrl.sortType="-tipo_broadcaster";
			}else{
				$scope.ctrl.sortType="+tipo_broadcaster";
			}
		}else if(sort==="NomeEmittente"){
			if($scope.ctrl.sortType==="+nome"){
				$scope.ctrl.sortType="-nome";
			}else{
				$scope.ctrl.sortType="+nome";
			}
		}else if(sort==="DataCreazione"){
			if($scope.ctrl.sortType==="+data_creazione"){
				$scope.ctrl.sortType="-data_creazione";
			}else{
				$scope.ctrl.sortType="+data_creazione";
			}
		}else if(sort==="Attivo"){
			if($scope.ctrl.sortType==="-data_disattivazione"){
				$scope.ctrl.sortType="+data_disattivazione";
			}else{
				$scope.ctrl.sortType="-data_disattivazione";
			}
		}else if(sort==="DataFineValidita"){
			if($scope.ctrl.sortType==="+data_disattivazione"){
				$scope.ctrl.sortType="-data_disattivazione";
			}else{
				$scope.ctrl.sortType="+data_disattivazione";
			}
		}
	}
	
	$scope.encode = function(buffer) {
		var binary = '';
		var bytes = new Uint8Array(buffer);
		var len = bytes.byteLength;
		for (var i = 0; i < len; i++) {
			binary += String.fromCharCode(bytes[i]);
		}
		return window.btoa(binary);
	};
	
	$scope.getRepertori= function(utente) {
		var repertoriSort=[];
		if(utente.repertori!=null&&utente!=undefined&&utente.repertori.length>0){
			var repertorio=""
			for (var i = 0; i < utente.repertori.length; i++) {
				repertoriSort.push(utente.repertori[i].repertorio.nome);
			}
			repertoriSort=repertoriSort.sort();
			for (var i = 0; i < repertoriSort.length; i++) {
				repertorio += repertoriSort[i] + ", ";
			}

			return repertorio.substring(0, repertorio.length - 2);
		}
	}
	
	$scope.getRepertorioDefault= function(utente) {
		if(utente.repertori!=null&&utente!=undefined&&utente.repertori.length>0){
			for (var i = 0; i < utente.repertori.length; i++) {
				if (utente.repertori[i].defaultRepertorio) {
					return utente.repertori[i].repertorio.nome;
				}
			}
		}
	}

	
	$scope.checkRadio = function() {
		if($scope.ctrl.selectedBroadcaster!=undefined&&$scope.ctrl.selectedBroadcaster.tipo_broadcaster=="RADIO"){
			return true;
		}else{
			return false;
		}
	}
	
	$scope.showEditEmittente = function(broadcaster) {
		$scope.ctrl.selectedBroadcaster = broadcaster;
		if (broadcaster.logo == null || broadcaster.logo == undefined) {
			$scope.ctrl.file = undefined
		} else {
			$scope.ctrl.file = "data:image/png;base64," + $scope.encode(broadcaster.logo);
		}
		$scope.getuserByBroadcaster();
		$scope.getCanali(broadcaster);
		$scope.getConfigurazioni(broadcaster);
		
	}
	
	$scope.getuserByBroadcaster = function() {
		$http({
			method : 'POST',
			url : 'rest/user/userByBroadcastId',
			data : {
				id : $scope.ctrl.selectedBroadcaster.id,
				nome : $scope.ctrl.selectedBroadcaster.nome,
				tipo_broadcaster : $scope.ctrl.selectedBroadcaster.tipo_broadcaster
			}
		}).then(function successCallback(response) {
			$scope.ctrl.utenti = response.data;

		}, function errorCallback(response) {
			$scope.ctrl.utenti = [];
		});
	}

	$scope.getCanali = function(emittente) {
		var emit = emittente;
		if (emit == null || emit == undefined || emit.id == undefined) {
			return;
		}
		$http({
			method : 'GET',
			url : 'rest/broadcasting/allCanali/' + emit.id,
			params : {

			}
		}).then(function successCallback(response) {
			$scope.ctrl.listaCanali = response.data;
			dataFineValid=null;
			active=false;
			for ( var canale in $scope.ctrl.listaCanali) {
				if(dataFineValid==null||dataFineValid<$scope.ctrl.listaCanali[canale].dataFineValid){
					dataFineValid=$scope.ctrl.listaCanali[canale].dataFineValid;
				}
				if($scope.ctrl.listaCanali[canale].dataFineValid==null||$scope.ctrl.listaCanali[canale].dataFineValid==undefined){
					active=true;
				}
			}
			if (active==false && new Date(dataFineValid)<new Date()) {
				$scope.ctrl.selectedBroadcaster.data_disattivazione=dataFineValid;
			}else{
				$scope.ctrl.selectedBroadcaster.data_disattivazione=null;
			}
		}, function errorCallback(response) {
			$scope.ctrl.listaCanali = []
		});

	};

	$scope.getConfigurazioni = function(emittente) {
		var emit = emittente;
		if (emit == null || emit == undefined || emit.id == undefined) {
			return;
		}
		$http({
			method : 'POST',
			url : 'rest/broadcasterConfig/activeConfig',
			params : {
				idEmittente:emit.id
			}
		}).then(function successCallback(response) {
			$scope.ctrl.listaConf = response.data;
		}, function errorCallback(response) {
			$scope.ctrl.listaCanali = []
		});

	};
	
	//funzione che renderizza il popup per l'aggiunta di una configurazione del DSP
	$scope.showAddUtente = function(utente) {
		var rootScope = $scope
		var utenteUltimaModifica=utente
		ngDialog.open({
			template : 'pages/broadcasting/utenti/addUtente.jsp',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', '$http', function($scope, ngDialog, $http) {

				$scope.ctrl = {
					sendMail:false,
					selectedUsername : "",
					selectedPassword : "",
					selectedEmail : "",
					selectedBroadcaster : {},
					selectedRole : {
						"id" : 2,
						"ruolo" : "BROADCASTER"
					},
					utenteUltimaModifica:utenteUltimaModifica,
					repertori : [],
					rScope : rootScope
				};

				$http({
					method : 'GET',
					url : 'rest/user/listaRepertori',
					params : {

					}
				}).then(function successCallback(response) {
					$scope.ctrl.repertori = response.data;
				}, function errorCallback(response) {
					$scope.ctrl.repertori = []
				});
				
				$scope.showAlert = function(title, message) {
					ngDialog.open({
						template : 'pages/advance-payment/dialog-alert.html',
						plain : false,
						width : '50%',
						controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
							var ngDialogId = $scope.ngDialogId;
							$scope.ctrl = {
								title : title,
								message : message,
								ngDialogId : ngDialogId
							};
							$scope.cancel = function(ngDialogId) {
								ngDialog.close(ngDialogId);
							};
						} ]
					}).closePromise.then(function(data) {
						if (data.value === true) {

						}
					});
				};

				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
			
				$scope.checkLowercase = function(selectedPassword) {
					if(selectedPassword!=undefined&&selectedPassword!=null&&selectedPassword.match("(?=.*[a-z])")){
						return true;
					}else{
						return false;
					}
				}
				
				$scope.checkNumber = function(selectedPassword) {
					if(selectedPassword!=undefined&&selectedPassword!=null&&selectedPassword.match("(?=.*[0-9])")){
						return true;
					}else{
						return false;
					}
				}
				
				$scope.checkUppercase = function(selectedPassword) {
					if(selectedPassword!=undefined&&selectedPassword!=null&&selectedPassword.match("(?=.*[A-Z])")){
						return true;
					}else{
						return false;
					}
				}
				
				$scope.checkSpecialChar = function(selectedPassword) {
					
					if(selectedPassword!=undefined&&selectedPassword!=null){
						var specialChars = "(?=.*[$@!%*#?&\-+_])"
					    for(i = 0; i < specialChars.length;i++){
					        if(selectedPassword.indexOf(specialChars[i]) > -1){
					            return true
					        }
					    }
					    return false;
					}else{
						return false;
					}
				}
				$scope.checkSize = function(selectedPassword) {
					if(selectedPassword!=undefined&&selectedPassword!=null&&selectedPassword.match("(.{8,})")){
						return true;
					}else{
						return false;
					}
				}
				
				//MODIFICARE I PARAMETRI		
				$scope.save = function() {
					var repertori=[];
					var hasDefault=false
					for (var i = 0; i < $scope.ctrl.repertori.length; i++) {
						if($scope.ctrl.repertori[i].nome===$scope.ctrl.selectedObj){
							$scope.ctrl.repertori[i].defaultValue=true;
							hasDefault=true;
						}else{
							$scope.ctrl.repertori[i].defaultValue=false;
						}
						repertori.push($scope.ctrl.repertori[i])
					}
					if(!hasDefault){
						$scope.showAlert("Errore", "Devi selezionare un repertorio di default");
						return;
					}
					$http({
						method : 'POST',
						url : 'rest/user/addUser',
						headers : {
							'Content-Type' : 'application/json'
						},
						data : {
							sendMail:$scope.ctrl.sendMail,
							repertori:repertori,
							bdcUtenti:{
								username : $scope.ctrl.selectedUsername,
								password : $scope.ctrl.selectedPassword,
								email : $scope.ctrl.selectedEmail,
								bdcBroadcasters : $scope.ctrl.rScope.ctrl.selectedBroadcaster,
								bdcRuoli : $scope.ctrl.selectedRole,
								utenteUltimaModifica:$scope.ctrl.utenteUltimaModifica
							}
						}
					}).then(function successCallback(response) {
						ngDialog.closeAll(false);
						$scope.ctrl.rScope.getuserByBroadcaster();
						$scope.showAlert("Esito", "Utente Aggiunto Correttamente");
					}, function errorCallback(response) {
						if(response.status==501){
							$scope.showAlert("Errore", "Non è stato possibile aggiungere l'utente, username già presente");
						}else if(response.status==502){
							$scope.showAlert("Errore", "Non è stato possibile aggiungere l'utente, email già presente");
						}else if(response.status==503){
							$scope.showAlert("Errore", "Non è stato possibile aggiungere l'utente, email non valida");
						}else if(response.status==504){
							$scope.showAlert("Errore", "Non è stato possibile aggiungere l'utente, password non valida");
						}else if(response.status==400){
							$scope.showAlert("Errore", "Non è stato possibile aggiungere l'utente, parametri inseriti non validi");
						}else{
							$scope.showAlert("Errore", "Non è stato possibile aggiungere l'utente");
						}

					});
				};
			} ]
		}).closePromise.then(function(data) {
			if (data.value === true) {
				// no op
			}
		});
	};

	$scope.deleteUser = function(item) {
		$http({
			method : 'POST',
			url : 'rest/user/deleteUser',
			headers : {
				'Content-Type' : 'application/json'
			},
			data : {
				id : item.id,
				title : item.title,
				utenti : item.nres,
				validTo : item.validTo,
				validFrom : item.validFrom,
				creator : item.creator,
				idUtente : item.idUtente,
				bdcBroadcasters : item.bdcBroadcasters
			}
		}).then(function successCallback(response) {
			$scope.getuserByBroadcaster();
		}, function errorCallback(response) {
			$scope.showAlert("Errore", "Non è stato possibile cancellare l'utente");
		});
	}

	

	$scope.showError = function(title, msgs) {
		ngDialog.open({
			template : 'pages/advance-payment/dialog-alert.html',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				$scope.ctrl = {
					title : title,
					message : msgs
				};
				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				$scope.save = function() {
					ngDialog.closeAll(true);
				};
			} ]
		}).closePromise.then(function(data) {
			// no op
		});
	};

	//funzione che renderizza il popup per l'aggiunta di una configurazione del DSP
	$scope.showUpdateUtente = function(user,item) {
		var rootScope = $scope
		var utente = item;
		var username = user;
		ngDialog.open({
			template : 'pages/broadcasting/utenti/updateUtente.jsp',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', '$http', function($scope, ngDialog, $http) {

				$scope.ctrl = {
					sendMail:false,
					selectedId : utente.id,
					selectedUsername : utente.username,
					selectedPassword : utente.password,
					selectedEmail : utente.email,
					selectedBroadcaster : utente.bdcBroadcasters,
					selectedRole : {
						"id" : 2,
						"ruolo" : "BROADCASTER"
					},
					utenteUltimaModifica:username, 
					repertori : [],
					selectedObj:"",
					rScope : rootScope
				};
				
				$http({
					method : 'GET',
					url : 'rest/user/listaRepertoriUtente',
					params : {
						idUtente:$scope.ctrl.selectedId
					}
				}).then(function successCallback(response) {
					$scope.ctrl.repertori = response.data;
					for (var i = 0; i < $scope.ctrl.repertori.length; i++) {
						if($scope.ctrl.repertori[i].defaultValue){
							$scope.ctrl.selectedObj=$scope.ctrl.repertori[i].nome;
						}
					}
				}, function errorCallback(response) {
					$scope.ctrl.repertori = []
				});
				
				$scope.showAlert = function(title, message) {
					ngDialog.open({
						template : 'pages/advance-payment/dialog-alert.html',
						plain : false,
						width : '50%',
						controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
							var ngDialogId = $scope.ngDialogId;
							$scope.ctrl = {
								title : title,
								message : message,
								ngDialogId : ngDialogId
							};
							$scope.cancel = function(ngDialogId) {
								ngDialog.close(ngDialogId);
							};
						} ]
					}).closePromise.then(function(data) {
						if (data.value === true) {

						}
					});
				};

				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				$scope.regeneratePassword = function() {

					$http({
						method : 'POST',
						url : 'rest/user/resetPassword',
						headers : {
							'Content-Type' : 'application/json'
						},
						data : {
							id : $scope.ctrl.selectedId,
							utenteUltimaModifica:$scope.ctrl.utenteUltimaModifica
						}
					}).then(function successCallback(response) {
						ngDialog.closeAll(false);
						$scope.ctrl.rScope.getuserByBroadcaster();
						$scope.showAlert("Esito", "La Password è stata rigenerata correttamente ed è stata inoltrata alla mail dell'utente");
					}, function errorCallback(response) {
						$scope.showAlert("Errore", "Non è stato possibile rigenerare la password");
					});
				}

				//MODIFICARE I PARAMETRI		
				$scope.save = function() {
					var repertori=[];
					for (var i = 0; i < $scope.ctrl.repertori.length; i++) {
						if($scope.ctrl.repertori[i].nome===$scope.ctrl.selectedObj){
							$scope.ctrl.repertori[i].defaultValue=true;
						}else{
							$scope.ctrl.repertori[i].defaultValue=false;
						}
						repertori.push($scope.ctrl.repertori[i])
					}
					$http({
						method : 'POST',
						url : 'rest/user/updateUser',
						headers : {
							'Content-Type' : 'application/json'
						},
						data : {
							sendMail:$scope.ctrl.sendMail,
							repertori: repertori,
							bdcUtenti:{
								id : $scope.ctrl.selectedId,
								username : $scope.ctrl.selectedUsername,
								password : $scope.ctrl.selectedPassword,
								email : $scope.ctrl.selectedEmail,
								bdcBroadcasters : $scope.ctrl.rScope.ctrl.selectedBroadcaster,
								bdcRuoli : $scope.ctrl.selectedRole,
								utenteUltimaModifica:username
							}
						}
					}).then(function successCallback(response) {
						ngDialog.closeAll(false);
						$scope.ctrl.rScope.getuserByBroadcaster();
						$scope.showAlert("Esito", "Utente Modificato Correttamente");
					}, function errorCallback(response) {
						if(response.status==501){
							$scope.showAlert("Errore", "Non è stata fatta nessuna modifica");
						}else if(response.status==502){
							$scope.showAlert("Errore", "Non è stato possibile modificare l'utente, username/email già presenti");
						}else if(response.status==503){
							$scope.showAlert("Errore", "Non è stato possibile modificare l'utente, email non valida");
						}else if(response.status==400){
							$scope.showAlert("Errore", "Non è stato possibile modificare l'utente, parametri inseriti non validi");
						}else{
							$scope.showAlert("Errore", "Non è stato possibile modificare l'utente");
						}
						
					});
				};
			} ]
		}).closePromise.then(function(data) {
			if (data.value === true) {
				// no op
			}
		});


	};



	//funzione che renderizza il popup per l'aggiunta di una configurazione del DSP
	$scope.showEditImage = function(item) {
		var rootScope = $scope
		ngDialog.open({
			template : 'pages/broadcasting/utenti/uploadImage.jsp',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', '$http', function($scope, ngDialog, $http) {

				$scope.ctrl = {
						selectedUsername : "",
						selectedPassword : "",
						selectedEmail : "",
						selectedBroadcaster : rootScope.ctrl.selectedBroadcaster,
						selectedRole : {
							"id" : 2,
							"ruolo" : "BROADCASTER"
						},
						rScope : rootScope,
						file : rootScope.ctrl.file,
					};
				
				$scope.uploadImage = function(item) {
					Upload.upload({
						url : 'rest/broadcasting/uploadImage',
						data : {
							file : $scope.ctrl.file,
							broadcasterId : $scope.ctrl.selectedBroadcaster.id,
						}
					}).then(function(resp) {
						$scope.ctrl.rScope.ctrl.file=$scope.ctrl.file;
						ngDialog.closeAll(true);
						$scope.showError("Esito Upload", "File Caricato con successo");
					}, function(resp) {
						ngDialog.closeAll(false);
						$scope.showError("Esito Upload", "Impossibile caricare il file, controllare la validità");
					}, function(evt) {
						var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
						logmsg('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
					});
				}

				$scope.deleteImage = function(item) {
					$http({
						method : 'GET',
						url : 'rest/broadcasting/deleteImage',
						params : {
							broadcasterId : $scope.ctrl.selectedBroadcaster.id,
						}
					}).then(function successCallback(response) {
						ngDialog.closeAll(true);
						$scope.ctrl.selectedBroadcaster.logo = undefined;
						$scope.ctrl.file = undefined
						$scope.showError("Esito", "Immagine eliminata con successo");
						$scope.ctrl.rScope.ctrl.file=$scope.ctrl.file;
					}, function errorCallback(response) {
						ngDialog.closeAll(false);
						$scope.showError("Esito", "Immagine non eliminata");
					});
				}

				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};

				$scope.showError = function(title, msgs) {
					ngDialog.open({
						template : 'pages/advance-payment/dialog-alert.html',
						plain : false,
						width : '60%',
						controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
							$scope.ctrl = {
								title : title,
								message : msgs
							};
							$scope.cancel = function() {
								ngDialog.closeAll(false);
							};
							$scope.save = function() {
								ngDialog.closeAll(true);
							};
						} ]
					}).closePromise.then(function(data) {
						// no op
					});
				};
			} ]
		}).closePromise.then(function(data) {
			if (data.value === true) {
				// no op
			}
		});
	};

	//funzione che renderizza il popup per l'aggiunta di una configurazione del DSP
	$scope.showAddCanale = function(utente) {
		var user=utente
		var rootScope = $scope
		ngDialog.open({
			template : 'pages/broadcasting/utenti/addCanale.jsp',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', '$http', function($scope, ngDialog, $http) {

				$scope.ctrl = {
					channelName : "",
					generalista : "",
					selectedDateFrom : "",
					selectedDateTo : "",
					utenteUltimaModifica:utente,
					rScope : rootScope
				};
				$scope.checkRadio = function() {
					if($scope.ctrl.rScope.ctrl.selectedBroadcaster.tipo_broadcaster=="RADIO"){
						return true;
					}else{
						return false;
					}
				}
				$scope.showAlert = function(title, message) {
					ngDialog.open({
						template : 'pages/advance-payment/dialog-alert.html',
						plain : false,
						width : '50%',
						controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
							var ngDialogId = $scope.ngDialogId;
							$scope.ctrl = {
								title : title,
								message : message,
								ngDialogId : ngDialogId
							};
							$scope.cancel = function(ngDialogId) {
								ngDialog.close(ngDialogId);
							};
						} ]
					}).closePromise.then(function(data) {
						if (data.value === true) {

						}
					});
				};

				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};

				//MODIFICARE I PARAMETRI		
				$scope.save = function(username) {
					$http({
						method : 'POST',
						url : 'rest/broadcaster/canale/addChannel',
						headers : {
							'Content-Type' : 'application/json'
						},
						data : {
							bdcBroadcastersId:$scope.ctrl.rScope.ctrl.selectedBroadcaster.id,
						    nome: $scope.ctrl.channelName,
						    generalista: $scope.ctrl.generalista,
							specialRadio: $scope.ctrl.specialRadio,
						    dataInizioValid: $scope.ctrl.selectedDateFrom,
						    dataFineValid: $scope.ctrl.selectedDateTo,
						    utenteUltimaModifica : username
						}
					}).then(function successCallback(response) {
						ngDialog.closeAll(false);
						$scope.ctrl.rScope.getCanali($scope.ctrl.rScope.ctrl.selectedBroadcaster);
						$scope.showAlert("Esito", "Canale Aggiunto Correttamente");
					}, function errorCallback(response) {
						if(response.status==409){
							$scope.showAlert("Errore", "Non è stato possibile aggiungere il canale, canale già presente nel broadcaster selezionato");
						}else if(response.status==406){
							$scope.showAlert("Errore", "Non è stato possibile aggiungere il canale, la data di fine è antecedente a quella d'inizio ");
						}else if(response.status==400){
							$scope.showAlert("Errore", "Non è stato possibile aggiungere il canale, parametri d'invio non corretti");
						}else {
							$scope.showAlert("Errore", "Non è stato possibile aggiungere il canale");
						}
					});
				};
			} ]
		}).closePromise.then(function(data) {
			if (data.value === true) {
				// no op
			}
		});
	};

	//funzione che renderizza il popup per l'aggiunta di una configurazione del DSP
	$scope.showAddBroadcaster = function(utente) {
		var user=utente
		var rootScope = $scope
		ngDialog.open({
			template : 'pages/broadcasting/utenti/addBroadcaster.jsp',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', '$http', function($scope, ngDialog, $http) {

				$scope.ctrl = {
					utenteUltimaModifica:utente,
					rScope : rootScope,
					tipiConf : null,
					tipiEmittenti:[
						{
							"nome" : "TELEVISIONE"
						}, {
							"nome" : "RADIO"
						} ],
				};
				$scope.changeType = function() {
					$http({
						method : 'GET',
						url : 'rest/broadcaster/getConfigurazioni',
						params : {
							tipoEmittente:$scope.ctrl.selectedType.nome
						}
					}).then(function successCallback(response) {
						$scope.ctrl.tipiConf=response.data
					}, function errorCallback(response) {
						
					});
				}
			

				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};

				//MODIFICARE I PARAMETRI		
				$scope.save = function(username) {
					$http({
						method : 'POST',
						url : 'rest/broadcaster/addEmittente',
						headers : {
							'Content-Type' : 'application/json'
						},
						data : {
						    nomeEmittente : $scope.ctrl.selectedName,
							tipoBroadcaster : $scope.ctrl.selectedType.nome,
							bdcBroadcasterConfigTemplate : $scope.ctrl.selectedConf,
							username : username,
							dataInizioValidita: $scope.ctrl.selectedDateConfigurazioneFrom,
							dataFineValidita: $scope.ctrl.selectedDateConfigurazioneTo,
							nuovoTracciatoRai: ($scope.ctrl.nuovoTracciatoRai) ? 'S' : 'N'
						}
					}).then(function successCallback(response) {
						ngDialog.closeAll(false);
						$scope.ctrl.rScope.getCanali($scope.ctrl.rScope.ctrl.selectedBroadcaster);
						$scope.showAlert("Esito", "Emittente Aggiunto Correttamente");
					}, function errorCallback(response) {
						if(response.status==409){
							$scope.showAlert("Errore", "Non è stato possibile aggiungere l'emittente");
						}else {
							$scope.showAlert("Errore", "Non è stato possibile aggiungere l'emittente");
						}
					});
				};
				$scope.showAlert = function(title, message) {
					ngDialog.open({
						template : 'pages/advance-payment/dialog-alert.html',
						plain : false,
						width : '50%',
						controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
							var ngDialogId = $scope.ngDialogId;
							$scope.ctrl = {
								title : title,
								message : message,
								ngDialogId : ngDialogId
							};
							$scope.cancel = function(ngDialogId) {
								ngDialog.close(ngDialogId);
							};
						} ]
					}).closePromise.then(function(data) {
						if (data.value === true) {

						}
					});
				};
			} ]
		}).closePromise.then(function(data) {
			if (data.value === true) {
				// no op
			}
		});
	};
	
	//funzione che renderizza il popup per l'aggiunta di una configurazione del DSP
	$scope.showAddConfiguration = function(utente,emittente) {
		var user=utente
		var rootScope = $scope
		ngDialog.open({
			template : 'pages/broadcasting/utenti/addConfiguration.jsp',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', '$http', function($scope, ngDialog, $http) {

				$scope.ctrl = {
					utenteUltimaModifica:utente,
					rScope : rootScope,
					tipiConf : null,
				};
			
				$http({
					method : 'GET',
					url : 'rest/broadcasting/allCanali/' + emittente.id,
					params : {

					}
				}).then(function successCallback(response) {
					$scope.ctrl.listaCanali = response.data;
					$scope.ctrl.listaCanali.unshift({
						"id" : -1,
						"nome" : "TUTTI",
						"tipo_broadcaster" : ""
					})
				}, function errorCallback(response) {
					$scope.ctrl.listaCanali = []
				});

				$http({
					method : 'GET',
					url : 'rest/broadcaster/getConfigurazioni',
					params : {
						tipoEmittente:emittente.tipo_broadcaster
					}
				}).then(function successCallback(response) {
					$scope.ctrl.tipiConf=response.data
					$scope.ctrl.selectedConf=response.data[0]
				}, function errorCallback(response) {
					
				});				
					
				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};

				//MODIFICARE I PARAMETRI		
				$scope.save = function(username) {

					var idCanale =null;
					if ($scope.ctrl.selectedCanale==null||$scope.ctrl.selectedCanale==undefined||$scope.ctrl.selectedCanale.nome==="TUTTI") {
						idCanale=null;
					}else{
						idCanale=$scope.ctrl.selectedCanale.id
					}
					
					
					$http({
						method : 'POST',
						url : 'rest/broadcasterConfig/addConfig',
						headers : {
							'Content-Type' : 'application/json'
						},
						data : {
							   idBroadcaster:emittente.id,
							   idChannel:idCanale,
							   validFrom:$scope.ctrl.selectedDateConfigurazioneFrom,
							   validTo:$scope.ctrl.selectedDateConfigurazioneTo,
							   bdcBroadcasterConfigTemplate:$scope.ctrl.selectedConf,
							   username:username
						}
					}).then(function successCallback(response) {
						$scope.ctrl.rScope.getConfigurazioni($scope.ctrl.rScope.ctrl.selectedBroadcaster);
						ngDialog.closeAll(false);
						$scope.showAlert("Esito", "Configurazione Aggiunta Correttamente");
					}, function errorCallback(response) {
						if(response.status==409){
							$scope.showAlert("Errore", "Non è stato possibile aggiungere la configurazione");
						}else {
							$scope.showAlert("Errore", "Non è stato possibile aggiungere la configurazione");
						}
					});
				};
				$scope.showAlert = function(title, message) {
					ngDialog.open({
						template : 'pages/advance-payment/dialog-alert.html',
						plain : false,
						width : '50%',
						controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
							var ngDialogId = $scope.ngDialogId;
							$scope.ctrl = {
								title : title,
								message : message,
								ngDialogId : ngDialogId
							};
							$scope.cancel = function(ngDialogId) {
								ngDialog.close(ngDialogId);
							};
						} ]
					}).closePromise.then(function(data) {
						if (data.value === true) {

						}
					});
				};
			} ]
		}).closePromise.then(function(data) {
			if (data.value === true) {
				// no op
			}
		});
	};
	
	$scope.showDettaglioConfigurazione = function(item) {
		var rootScope = $scope
		ngDialog.open({
			template : 'pages/broadcasting/utenti/infoConfiguration.jsp',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', '$http', function($scope, ngDialog, $http) {

				$scope.ctrl = {
					rScope : rootScope,
					configuration : JSON.parse(item[1].configuration.configurazione),
				};
				
				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};

			} ]
		}).closePromise.then(function(data) {
			if (data.value === true) {
				// no op
			}
		});
	};
	
	//funzione che renderizza il popup per l'aggiunta di una configurazione del DSP
	$scope.showUpdateConfiguration = function(utente, item) {
		var user=utente
		var rootScope = $scope
		ngDialog.open({
			template : 'pages/broadcasting/utenti/updateConfiguration.jsp',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', '$http', function($scope, ngDialog, $http) {

				
				$scope.ctrl = {
						utenteUltimaModifica:utente,
						rScope : rootScope,
						tipiConf : null,
						channel : item[0],
				};
				
				var dataInizio =$scope.ctrl.rScope.formatDate(item[1].validFrom)
				var dataFine =$scope.ctrl.rScope.formatDate(item[1].validTo)
				var username=utente;
				var numbers = dataInizio.match(/\d+/g);
				var numbers2 = dataFine.match(/\d+/g);
				var stringNumbers=undefined;
				var stringNumbers2=undefined;
				if(numbers!=undefined&&numbers!=null){
					stringNumbers = (numbers[0]+"-"+  numbers[1] +"-"+  numbers[2]);
				}
				if(numbers2!=undefined&&numbers2!=null){
					stringNumbers2 = (numbers2[0]+"-"+  numbers2[1] +"-"+  numbers2[2]);
				}
				
				document.getElementById('defvalue').value = $scope.ctrl.hiddenvalue;
				document.getElementById('defvalue2').value = $scope.ctrl.hiddenvalue2;

				$scope.ctrl.selectedDateConfigurazioneFrom=stringNumbers;
				$scope.ctrl.selectedDateConfigurazioneTo=stringNumbers2;
				
				$http({
					method : 'GET',
					url : 'rest/broadcaster/getConfigurazioni',
					params : {
						tipoEmittente:rootScope.ctrl.selectedBroadcaster.tipo_broadcaster
					}
				}).then(function successCallback(response) {
					$scope.ctrl.tipiConf=response.data
					for (i = 0; i < $scope.ctrl.tipiConf.length; i++) {
						if ($scope.ctrl.tipiConf[i].id == item[1].configuration.id) {
							$scope.ctrl.selectedConf = $scope.ctrl.tipiConf[i];
						}
					}
				}, function errorCallback(response) {
					
				});				
					
				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};

				//MODIFICARE I PARAMETRI		
				$scope.save = function(username) {
					$http({
						method : 'POST',
						url : 'rest/broadcasterConfig/updateConfig',
						headers : {
							'Content-Type' : 'application/json'
						},
						data : {
							   idConfiguration:item[1].id,
							   idBroadcaster:item[1].idBroadcaster,
							   idChannel: item[1].idChannel,
							   validFrom:$scope.ctrl.selectedDateConfigurazioneFrom,
							   validTo:$scope.ctrl.selectedDateConfigurazioneTo,
							   bdcBroadcasterConfigTemplate:$scope.ctrl.selectedConf,
							   username:username
						}
					}).then(function successCallback(response) {
						$scope.ctrl.rScope.getConfigurazioni($scope.ctrl.rScope.ctrl.selectedBroadcaster);
						ngDialog.closeAll(false);
						$scope.showAlert("Esito", "Configurazione Aggiornata Correttamente");
					}, function errorCallback(response) {
						if(response.status==409){
							$scope.showAlert("Errore", "Non è stato possibile aggiornare la configurazione");
						}else {
							$scope.showAlert("Errore", "Non è stato possibile aggiornare la configurazione");
						}
					});
				};
				$scope.showAlert = function(title, message) {
					ngDialog.open({
						template : 'pages/advance-payment/dialog-alert.html',
						plain : false,
						width : '50%',
						controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
							var ngDialogId = $scope.ngDialogId;
							$scope.ctrl = {
								title : title,
								message : message,
								ngDialogId : ngDialogId
							};
							$scope.cancel = function(ngDialogId) {
								ngDialog.close(ngDialogId);
							};
						} ]
					}).closePromise.then(function(data) {
						if (data.value === true) {

						}
					});
				};
			} ]
		}).closePromise.then(function(data) {
			if (data.value === true) {
				// no op
			}
		});
	};
	
	$scope.deleteConfigurazione = function( modifyUser,idConfig) {
		$http({
			method : 'POST',
			url : 'rest/broadcasterConfig/deleteConfig',
			headers : {
				'Content-Type' : 'application/json'
			},
			params : {
				   idConfig:idConfig,
				   modifyUser:modifyUser
			}
		}).then(function successCallback(response) {
			ngDialog.closeAll(false);
			$scope.getConfigurazioni($scope.ctrl.selectedBroadcaster);
			$scope.showAlert("Esito", "Configurazione Aggiornata Correttamente");
		}, function errorCallback(response) {
			if(response.status==409){
				$scope.showAlert("Errore", "Non è stato possibile aggiornare la configurazione");
			}else {
				$scope.showAlert("Errore", "Non è stato possibile aggiornare la configurazione");
			}
		});
	}
	
	
	
	
	$scope.showStoricoCanale = function(item) {
		var rootScope = $scope
		var utente = item;
		ngDialog.open({
			template : 'pages/broadcasting/utenti/storicoCanale.jsp',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', '$http', function($scope, ngDialog, $http) {

				$scope.ctrl = {
					id: item.id,
					rScope : rootScope
				};
				
				$http({
					method : 'GET',
					url : 'rest/broadcasting/storicoCanali/' + $scope.ctrl.id,
					params : {

					}
				}).then(function successCallback(response) {
					$scope.ctrl.listaCanali = response.data;
				}, function errorCallback(response) {
					$scope.ctrl.listaCanali = [];
				});
				$scope.checkRadio = function() {
					if($scope.ctrl.rScope.ctrl.selectedBroadcaster.tipo_broadcaster=="RADIO"){
						return true;
					}else{
						return false;
					}
				}
				$scope.showAlert = function(title, message) {
					ngDialog.open({
						template : 'pages/advance-payment/dialog-alert.html',
						plain : false,
						width : '70%',
						controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
							var ngDialogId = $scope.ngDialogId;
							$scope.ctrl = {
								title : title,
								message : message,
								ngDialogId : ngDialogId
							};
							$scope.cancel = function(ngDialogId) {
								ngDialog.close(ngDialogId);
							};
						} ]
					}).closePromise.then(function(data) {
						if (data.value === true) {

						}
					});
				};
				
				$scope.checkSize = function() {
					if ($scope.ctrl.listaCanali==null||$scope.ctrl.listaCanali==undefined||$scope.ctrl.listaCanali.length==0) {
						return true;
					}else{
						return false;
					}

				};
				
				$scope.formatDate = function(dateString) {
					if (dateString != undefined && dateString != null) {
						var date = new Date(dateString)
						var separator = "-"
						var month = date.getMonth() + 1
						return (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + separator + (month < 10 ? "0" + month : month) + separator + date.getFullYear() + " " + (date.getHours() < 10 ? "0" + date.getHours() : date.getHours()) + ":" + (date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes())

					} else {
						return "";
					}
				}
			
				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};

			} ]
		}).closePromise.then(function(data) {
			if (data.value === true) {
				// no op
			}
		});
	};
	//funzione che renderizza il popup per l'aggiunta di una configurazione del DSP
	$scope.showUpdateCanale = function(utente,item) {
		var rootScope = $scope
		var utente = item;
		var dataInizio =$scope.formatDate(item.dataInizioValid)
		var dataFine =$scope.formatDate(item.dataFineValid)
		var isGeneralista = item.generalista==1;
		var isSpecialRadio = item.specialRadio==1;
		var username=utente;
		var numbers = dataInizio.match(/\d+/g);
		var numbers2 = dataFine.match(/\d+/g);
		var stringNumbers=undefined;
		var stringNumbers2=undefined;
		if(numbers!=undefined&&numbers!=null){
			stringNumbers = (numbers[0]+"-"+  numbers[1] +"-"+  numbers[2]);
		}
		if(numbers2!=undefined&&numbers2!=null){
			stringNumbers2 = (numbers2[0]+"-"+  numbers2[1] +"-"+  numbers2[2]);
		}
		var stringNumbers2 = dataFine.match(/\d+/g);
		ngDialog.open({
			template : 'pages/broadcasting/utenti/updateCanale.jsp',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', '$http', function($scope, ngDialog, $http) {
				
				$scope.ctrl = {
					id: item.id,
				    utenteUltimaModifica : username,
					channelName : item.nome,
					generalista : isGeneralista,
					selectedDateFrom : dataInizio,
					selectedDateTo : dataFine,
					specialRadio : isSpecialRadio,
					hiddenvalue : stringNumbers,
					hiddenvalue2 : stringNumbers2,
					rScope : rootScope
				};
			
				document.getElementById('defvalue').value = $scope.ctrl.hiddenvalue;
				document.getElementById('defvalue2').value = $scope.ctrl.hiddenvalue2;
				$scope.checkRadio = function() {
					if($scope.ctrl.rScope.ctrl.selectedBroadcaster.tipo_broadcaster=="RADIO"){
						return true;
					}else{
						return false;
					}
				}
				$scope.formatDate = function(dateString) {
					if (dateString != undefined && dateString != null) {
						var date = new Date(dateString)
						var separator = "-"
						var month = date.getMonth() + 1
						return (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + separator + (month < 10 ? "0" + month : month) + separator + date.getFullYear() + " " + (date.getHours() < 10 ? "0" + date.getHours() : date.getHours()) + ":" + (date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes())

					} else {
						return "";
					}
				}
				$scope.showAlert = function(title, message) {
					ngDialog.open({
						template : 'pages/advance-payment/dialog-alert.html',
						plain : false,
						width : '50%',
						controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
							var ngDialogId = $scope.ngDialogId;
							$scope.ctrl = {
								title : title,
								message : message,
								ngDialogId : ngDialogId
							};
							$scope.cancel = function(ngDialogId) {
								ngDialog.close(ngDialogId);
							};
						} ]
					}).closePromise.then(function(data) {
						if (data.value === true) {

						}
					});
				};

				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};

				//MODIFICARE I PARAMETRI		
				$scope.save = function(username) {
					$http({
						method : 'POST',
						url : 'rest/broadcaster/canale/updateChannel',
						headers : {
							'Content-Type' : 'application/json'
						},
						data : {
							bdcBroadcastersId:$scope.ctrl.id,
						    nome: $scope.ctrl.channelName,
						    generalista: $scope.ctrl.generalista,
							specialRadio: $scope.ctrl.specialRadio,
						    dataInizioValid: $scope.ctrl.selectedDateFrom,
						    dataFineValid: $scope.ctrl.selectedDateTo,
						    utenteUltimaModifica : username
						}
					}).then(function successCallback(response) {
						ngDialog.closeAll(false);
						$scope.ctrl.rScope.getCanali($scope.ctrl.rScope.ctrl.selectedBroadcaster);
						$scope.showAlert("Esito", "Canale Modificato Correttamente");
					}, function errorCallback(response) {
						if(response.status==410){
							$scope.showAlert("Errore", "Non sono state apportate le modifiche al canale, nome canale già esistente");
						}else if(response.status==409){
							$scope.showAlert("Errore", "Non sono state apportate le modifiche al canale, i dati in input corrispondono a quelli attuali");
						}else if(response.status==406){
							$scope.showAlert("Errore", "Non è stato possibile aggiungere il canale, la data di fine è antecedente a quella d'inizio ");
						}else{
							$scope.showAlert("Errore", "Non sono state effettuate modifiche al canale");
						}
					});
				};
			} ]
		}).closePromise.then(function(data) {
			if (data.value === true) {
				// no op
			}
		});
	};

	showAlert = function(title, message) {
		ngDialog.open({
			template : 'pages/advance-payment/dialog-alert.html',
			plain : false,
			width : '50%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				var ngDialogId = $scope.ngDialogId;
				$scope.ctrl = {
					title : title,
					message : message,
					ngDialogId : ngDialogId
				};
				$scope.cancel = function(ngDialogId) {
					ngDialog.close(ngDialogId);
				};
			} ]
		}).closePromise.then(function(data) {
			if (data.value === true) {

			}
		});
	};

	$scope.getImgStato = function(item) {
		if (item == 1) {
			return "images/ok.png";
		} else {
			return "images/ko.png";
		}
	};

	$scope.formatDate = function(dateString) {
		if (dateString != undefined && dateString != null) {
			var date = new Date(dateString)
			var separator = "-"
			var month = date.getMonth() + 1
			return (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + separator + (month < 10 ? "0" + month : month) + separator + date.getFullYear() 

		} else {
			return "";
		}
	}


	$scope.onRefresh();

} ]);