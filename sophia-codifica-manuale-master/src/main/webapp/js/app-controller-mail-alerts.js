codmanApp.controller('mailAlertsCtrl', ['$scope', '$route', '$interval', 'configService', '$routeParams', 'ngDialog', 'mailAlertsService', 'dspsUtilizationsCommercialOffersCountries','alerts',
	function ($scope, $route, $interval, configService, $routeParams, ngDialog, mailAlertsService, dspsUtilizationsCommercialOffersCountries,alerts) {

		$scope.ctrl = {
			data: {
				alerts: alerts.data,
				dsps: dspsUtilizationsCommercialOffersCountries.dsp
			},
			pageTitle: `Invio Mail`,
			messages: {
				info: "",
				error: "",
				warning: "",
				clear: function () {
					this.info = "";
					this.error = "";
				},
				infoMessage: function (message) {
					this.info = message;
					this.scrollIntoView();
				},
				errorMessage: function (message) {
					this.error = message;
					this.scrollIntoView();
				},
				warningMessage: function (message) {
					this.warning = message;
					this.scrollIntoView();
				},
				scrollIntoView: function () {
					$('#rightPanel')[0].scrollIntoView(true);
					window.scrollBy(0, -100);
				}
			}
		};

		$scope.editAlert = function (alert) {
			var alerts = $scope.ctrl.data.alerts;
			var messages = $scope.ctrl.messages;
			var dsps = $scope.ctrl.data.dsps;
			var mapDsps = $scope.mapDsps;
			var unmapDsps = $scope.unmapDsps;
			ngDialog.open({
				template: 'pages/mail-alerts/dialog-edit-alert.html',
				plain: false,
				width: '40%',
				controller: ['$scope', 'ngDialog', 'mailAlertsService', '$http', function ($scope, ngDialog, mailAlertsService, $http) {
					$scope.ctrl = {
						dspSelectSettings: {
							scrollableHeight: '200px',
							scrollable: false,
							enableSearch: true,
							smartButtonMaxItems: 2,
							idProp: 'idDsp',
							displayProp: 'description'
						},
						alertMixin: {
							counter: 0,
							addAddress: () => {
								$scope.alert.addresses.push({ id: ++$scope.alert.counter, value: '' });
							},
							removeAddress: (address) => {
								let index = $scope.alert.addresses.findIndex(item => item.id == address.id);
								$scope.alert.addresses.splice(index, 1);
							},
							address: (id) => {
								return $scope.alert.addresses.find(item => item.id == id);
							}
						},
						dsps: dsps
					};
					if (alert) {
						$scope.mode = 'edit';
						$scope.alert = { ..._.cloneDeep(alert), ...$scope.ctrl.alertMixin };
					} else {
						$scope.mode = 'new';
						$scope.alert = {
							...{
								dsps: [],
								failedClaim: false,
								completedClaim: false,
								negativeClaim: false,
								addresses: [{ id: 0, value: '' }]
							},
							...$scope.ctrl.alertMixin
						}
					};

					$scope.cancel = function () {
						ngDialog.closeAll(false);
					};

					$scope.save = function () {
						let request = {...$scope.alert, dsps:mapDsps($scope.alert.dsps)};
						mailAlertsService.saveAlert(request).then(function success(response) {
							let toAdd = {...response.data, dsps:unmapDsps(response.data.dsps)};
							if ($scope.mode == 'new') {
								alerts.push(toAdd);
							} else {
								var index = alerts.findIndex(el => el.id === toAdd.id);
								alerts.splice(index, 1, toAdd);
							}
							ngDialog.closeAll(true);
						}, function error(error) {
							ngDialog.closeAll(true);
							messages.errorMessage(error);
						});
					};
				}]
			});
		};

		$scope.unmapDsps = function (dsps) {
			
			if(dsps)
				return dsps.map(e =>{
					return {id: e.idDsp};
				});
		};

		$scope.mapDsps = function (dsps) {
			if (!dsps) return void 0;
			let result = [];
			for (var e of dsps) {
				let found = $scope.ctrl.data.dsps.find((item) => item.idDsp == e.id);
				if (found) result.push(found);
			}
			return result;
		};

		(()=>{
			$scope.ctrl.data.alerts.forEach(function(alert, index) {
				this[index] = {...alert, dsps:$scope.unmapDsps(alert.dsps)};
			  }, $scope.ctrl.data.alerts);	
		})();

		$scope.longerThan = function (arr, propId, limit) {
			if (!arr) return void 0;
			let acc = 0;
			for (var e of arr) {
				if ((acc + e[propId].length) < limit) {
					acc = acc + e[propId].length;
				} else {
					return true;
				}
			}
			return false;
		};

		$scope.arrayShortenedJoined = function (arr, propId, limit) {
			if (!arr) return void 0;
			let result = [];
			let acc = 0;
			for (var e of arr) {
				if ((acc + e[propId].length) < limit) {
					acc = acc + e[propId].length;
					result.push(e[propId]);
				} else {
					break;
				}
			}
			return result.join(", ");
		};

		$scope.arrayJoined = function (arr, propId) {
			if (!arr) return void 0;
			let result = [];
			for (var e of arr) {
				result.push(e[propId]);
			}
			return result.join(", ");
		};


		$scope.deleteAlert = function (alert) {
			$scope.ctrl.messages.clear();
			ngDialog.openConfirm({
				template: 'pages/mail-alerts/dialog-alert.html',
				plain: false,
				data: {
					alert: alert
				},
				controller: function () {
					var ctrl = this;
					ctrl.title = "Elimina Configurazione Alert";
					ctrl.message = `Confermi l'eliminazione della configurazione?`;
				},
				controllerAs: 'ctrl'
			}).then(function confirm(data) {
				let request = {...data.alert, dsps:$scope.mapDsps(data.alert.dsps)};
				mailAlertsService.deleteAlert(request).then(
					function success(response) {
						mailAlertsService.getAlerts().then(
							function success(res){
								debugger;
								$scope.ctrl.data.alerts= res.data;

								$scope.ctrl.data.alerts.forEach(function(alert, index) {
									this[index] = {...alert, dsps:$scope.unmapDsps(alert.dsps)};
								}, $scope.ctrl.data.alerts);
							}
						)
					},
					function error(error) {
						$scope.ctrl.messages.errorMessage(error);
					}
				);
			}, function cancel() {
				//no op
			});
		};

	}]);

codmanApp.service('mailAlertsService', ['$http', '$q', function ($http, $q) {

	var service = this;

	service.saveAlert = function (alert) {
		return $http({
			method: 'POST',
			url: 'rest/mail-alerts',
			data: alert,
			headers: { 'Content-Type': 'application/json' }
		});
	};

	service.deleteAlert = function (alert) {
		return $http({
			method: 'DELETE',
			url: 'rest/mail-alerts',
			data: alert,
			headers: {'Content-Type': 'application/json'}			
		});
	};

	service.getAlerts = function () {
		return $http({
			method: 'GET',
			url: 'rest/mail-alerts'
		});
	};
}
]);