/**
 * MultimedialeLocaleCtrl
 * 
 * @path /multimedialeLocale
 */
codmanApp.controller('MultimedialeLocaleCtrl', [ '$scope', '$route', 'ngDialog', '$routeParams', '$location', '$http', 'Upload', 'configService', function($scope, $route, ngDialog, $routeParams, $location, $http, Upload, configService) {

	$scope.ctrl = {
			
		navbarTab : 'ccidConfig',
		config : [],
		filterParameters : [],
		execute : $routeParams.execute,
		hideForm : $routeParams.hideForm,
		selectedPeriodSend : $routeParams.selectedPeriodSend || '',
		selectedLicenziatario : $routeParams.selectedLicenziatario || '',
		selectedPeriodFrom : $routeParams.selectedPeriodFrom || '',
		selectedPeriodTo : $routeParams.selectedPeriodTo || '',
		selectedTipologia : $routeParams.selectedTipologia || '',
		selectedStato : $routeParams.selectedStato || '',
		selectedPeriodoRipartizione: $routeParams.selectedStato || '',
		first : 0,
		last : 50,
		maxrow : 50,
		report:[],
		dettailSelected : null,
		stati : [],
		tipologie : [],
		ripartizioni : [],
		searchText:"",
		licenziatari :[]	
	};

	$scope.newState=function (state) {
		alert("Sorry! You'll need to create a Constitution for " + state + " first!");
    }

    $scope.querySearch= function  (query) {
      var results = query ? $scope.ctrl.licenziatari.filter(  $scope.createFilterFor(query) ) : $scope.ctrl.licenziatari,
          deferred;
      if (self.simulateQuery) {
        deferred = $q.defer();
        $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
        return deferred.promise;
      } else {
        return results;
      }
    }

    $scope.searchTextChange= function (text) {

    }

    $scope.selectedItemChange = function(item) {

    }

    /**
     * Create filter function for a query string
     */
    $scope.createFilterFor= function (query) {
      var lowercaseQuery = angular.lowercase(query);

      return function filterFn(x) {
    	   	return angular.lowercase(x).includes(lowercaseQuery);
      };
    }

	$scope.multiselectSettings = {
		scrollableHeight : '200px',
		scrollable : false,
		enableSearch : false,
		showUncheckAll : true,
		searchBr : false,
		checkboxes : false
	};

	// Funzione principale richiamata al caricamento della pagina
	$scope.onRefresh = function() {
		$scope.getStati();
		$scope.getLicenziatari();
		$scope.getTipologie();
		$scope.getRipartizioni();
	};
	
	$scope.getStati = function() {
		$http({
			method : 'POST',
			url : 'rest/multimediale/locale/stati',
    			headers: {
                    'Content-Type': 'application/json'},
			data : {
			}
		}).then(function successCallback(response) {
			$scope.ctrl.stati = response.data;
			$scope.ctrl.stati.unshift({
				"nome" : "TUTTI"
			});
		}, function errorCallback(response) {
			$scope.ctrl.stati = []
		});
	};

	
	$scope.getLicenziatari = function() {
		$http({
		method : 'POST',
		url : 'rest/multimediale/locale/licenziatari',
			headers: {
                'Content-Type': 'application/json'},
		data : {
			
		}}).then(function successCallback(response) {
			$scope.ctrl.licenziatari = response.data;
		}, function errorCallback(response) {
			$scope.ctrl.tipologie = []
		});
	};

	
	$scope.getTipologie = function() {
		$http({
			method : 'POST',
			url : 'rest/multimediale/locale/tipologieServizio',
				headers: {
	                'Content-Type': 'application/json'},
			data : {
				
			}}).then(function successCallback(response) {
				$scope.ctrl.tipologie = response.data;
				$scope.ctrl.tipologie.unshift({
					"nome" : "TUTTI"
				})
			}, function errorCallback(response) {
				$scope.ctrl.tipologie = []
		});
	};
	
	$scope.getRipartizioni = function() {
		$http({
			method : 'POST',
			url : 'rest/multimediale/locale/ripartizioni',
    			headers: {
                    'Content-Type': 'application/json'},
			params : {
			}
		}).then(function successCallback(response) {
			$scope.ctrl.ripartizioni = response.data;
			$scope.ctrl.ripartizioni.unshift(
				"TUTTI"
			)
		}, function errorCallback(response) {
			$scope.ctrl.ripartizioni = []
		});
	};

	$scope.navigateToNextPage = function() {
		$scope.getReportMultimediale($scope.ctrl.dataInvioDa,$scope.ctrl.dataInvioA,$scope.ctrl.licenziatario, $scope.ctrl.selectedPeriodFrom, $scope.ctrl.selectedPeriodTo, $scope.ctrl.tipologiaServizio,$scope.ctrl.statoReport,$scope.ctrl.selectedPeriodoRipartizione,$scope.ctrl.last,$scope.ctrl.last + $scope.ctrl.maxrows,50)	 
	};

	$scope.formatNumber = function(number){
 		return twoDecimalRound(number);
	}
	
	$scope.checkEmptyText = function(dataRipartizione){
		if (dataRipartizione!=null||dataRipartizione!=undefined) {
			return "Si"
		}else{
			return "No"
		}
	}
	
	$scope.navigateToPreviousPage = function() {
		$scope.getReportMultimediale($scope.ctrl.dataInvioDa,$scope.ctrl.dataInvioA,$scope.ctrl.licenziatario, $scope.ctrl.selectedPeriodFrom, $scope.ctrl.selectedPeriodTo, $scope.ctrl.tipologiaServizio,$scope.ctrl.statoReport,$scope.ctrl.selectedPeriodoRipartizione,$scope.ctrl.last,Math.max($scope.ctrl.first - $scope.ctrl.maxrows, 0),$scope.ctrl.first,50)
	}	

	$scope.back= function(item) {
		$scope.ctrl.dettailSelected=null;
		$scope.ctrl.dettailStat=null;
	}
	
	$scope.formatDate = function(dateString) {
		if (dateString != undefined && dateString != null) {
			var date = new Date(dateString)
			var separator = "-"
			var month = date.getMonth() + 1
			return (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + separator + (month < 10 ? "0" + month : month)
				+ separator + date.getFullYear()
		} else {
			return "N/A";
		}
	}
	
	$scope.checkSize = function() {
		if($scope.ctrl.storico==null||$scope.ctrl.storico==undefined||$scope.ctrl.storico.length==0){
			return true;
		}else{
			return false;
		}
	}
	
	$scope.apply = function(dataInvioDa,dataInvioA,licenziatario, selectedPeriodFrom, selectedPeriodTo, tipologiaServizio , statoReport, periodoRipartizione) {
		$scope.ctrl.dataInvioDa=dataInvioDa
		$scope.ctrl.dataInvioA=dataInvioA
		$scope.ctrl.licenziatario=licenziatario
		$scope.ctrl.selectedPeriodFrom=selectedPeriodFrom
		$scope.ctrl.selectedPeriodTo =selectedPeriodTo
		$scope.ctrl.tipologiaServizio = tipologiaServizio
		$scope.ctrl.statoReport = statoReport
		$scope.getReportMultimediale(dataInvioDa,dataInvioA,licenziatario, selectedPeriodFrom, selectedPeriodTo, tipologiaServizio, statoReport, periodoRipartizione, 0, 50, 50)
	}
	
	$scope.download = function(dataInvioDa,dataInvioA,licenziatario, selectedPeriodFrom, selectedPeriodTo, tipologiaServizio , statoReport, periodoRipartizione) {
		$scope.ctrl.dataInvioDa=dataInvioDa
		$scope.ctrl.dataInvioA=dataInvioA
		$scope.ctrl.licenziatario=licenziatario
		$scope.ctrl.selectedPeriodFrom=selectedPeriodFrom
		$scope.ctrl.selectedPeriodTo =selectedPeriodTo
		$scope.ctrl.tipologiaServizio = tipologiaServizio
		$scope.ctrl.statoReport = statoReport
		$scope.downloadReportMultimediale(dataInvioDa,dataInvioA,licenziatario, selectedPeriodFrom, selectedPeriodTo, tipologiaServizio, statoReport, periodoRipartizione)
	}
	
	$scope.getReportMultimediale = function(dataInvioDa,dataInvioA,licenziatario, selectedPeriodFrom, selectedPeriodTo, tipologiaServizio, statoReport, periodoRipartizione, first, last, maxrow){
		
		var stato="";
		var servizio="";
		var periodo="";
		if(tipologiaServizio!=null&&tipologiaServizio!==undefined&&tipologiaServizio.nome!=="TUTTI"){
			servizio=tipologiaServizio.nome;
		}
		if(statoReport!=null&&statoReport!=undefined&&statoReport.nome!=="TUTTI"){
			stato=statoReport.nome;
		}
		if(periodoRipartizione!=null&&periodoRipartizione!=undefined&&periodoRipartizione!=="TUTTI"){
			periodo=periodoRipartizione;
		}
		
		$http({
			method : 'POST',
			url : 'rest/multimediale/locale/report',
    			headers: {
                    'Content-Type': 'application/json'},
			data : {
				dataInvioDa:dataInvioDa,
				dataInvioA:dataInvioA,
				licenziatario:licenziatario,
				dataDa:selectedPeriodFrom,
				dataA:selectedPeriodTo,
				servizio:servizio,
				statoLogico:stato,
				first: first,
				periodoRipartizione:periodo,
				last: last,
				maxrow:maxrow,
			}
		}).then(function successCallback(response) {
			if (response.data.message!=undefined||response.data.message!=null) {
				$scope.showAlert("Errore",response.data.message)
			}
			$scope.ctrl.report = response.data.rows;
			$scope.ctrl.first = response.data.first;
			$scope.ctrl.last = response.data.last;
		
		}, function errorCallback(response) {
			$scope.ctrl.report = []
		});
	}		

	$scope.downloadReportMultimediale = function(dataInvioDa,dataInvioA,licenziatario, selectedPeriodFrom, selectedPeriodTo, tipologiaServizio, statoReport, periodoRipartizione, first, last, maxrow){
		var stato="";
		var servizio="";
		if(tipologiaServizio!=null&&tipologiaServizio!==undefined&&tipologiaServizio.nome!=="TUTTI"){
			servizio=tipologiaServizio.nome;
		}
		if(statoReport!=null&&statoReport!=undefined&&statoReport.nome!=="TUTTI"){
			stato=statoReport.nome;
		}
		if(periodoRipartizione!=null&&periodoRipartizione!=undefined&&periodoRipartizione!=="TUTTI"){
			periodo=periodoRipartizione;
		}
		$http({
			method : 'POST',
			url : 'rest/multimediale/locale/downloadReport',
    			headers: {
                    'Content-Type': 'application/json'},
			data : {
				dataInvioDa:dataInvioDa,
				dataInvioA:dataInvioA,
				licenziatario:licenziatario,
				dataDa:selectedPeriodFrom,
				dataA:selectedPeriodTo,
				servizio:servizio,
				statoLogico:stato,
				first: first,
				last: last,
				maxrow:maxrow,
			}
		}).then(function successCallback(response) {

			if (response.data.message!=undefined||response.data.message!=null) {
				$scope.showAlert("Errore",response.data.message)
			}
			saveAs(new Blob([response.data],{type:"text/plain;charset=UTF-8"}), "ReportMultimediale.csv");
		}, function errorCallback(response) {
			$scope.ctrl.report = []
		});
	}


	
	$scope.showDettails = function(item) {
		$scope.ctrl.dettailSelected=item;
		if (item==undefined||item==null||item.codificaStatistiche==undefined||item.codificaStatistiche==null) {
			$scope.ctrl.dettailStat=null;
		}else{
			$scope.ctrl.dettailStat=JSON.parse(item.codificaStatistiche);
		}
		$http({
			method : 'POST',
			url : 'rest/multimediale/locale/storico',
    			headers: {
                    'Content-Type': 'application/json'},
			data : {
				periodoCompetenza:item.origPeriodoCompetenza,
				licenza:item.licenza,
				servizio:item.servizio,
				statoLogico:item.statoLogico,
	
			}
		}).then(function successCallback(response) {
			if (response.data.message!=null&&response.data.message!=null) {
				$scope.ctrl.storico = [];
			}else{
				$scope.ctrl.storico = response.data;
			}
		}, function errorCallback(response) {
			$scope.ctrl.storico = [];
		});
		
		
	};
	
	$scope.downloadReport = function(item,type) {
		var id=item.idMlReport;
		var tipo=type;
		$http({
			responseType: 'blob',
			method : 'GET',
			url : 'rest/multimediale/locale/downloadReportFile',
    			headers: {
                    'Content-Type': 'application/json'},
			params : {
				id:id,
				tipo:tipo
			}
		}).then(function successCallback(response) {
			var contentDisposition = response.headers('Content-Disposition');
			var filename =contentDisposition.split(';')[1].split('filename')[1].split('=')[1].trim();
			if(tipo==='CARICATO'){

				saveAs(new Blob([response.data],{type:"application/octet-stream"}), filename);
			}else{
				saveAs(new Blob([response.data],{type:"text/plain;charset=UTF-8"}), filename);
			}
		}, function errorCallback(response) {
			$scope.showAlert("Errore","Si è verificato un errore: Report non disponibile")
		});
		
		
	};
	
	function splitPath(DatLen,Len){
		var outSplit=[];
		while (DatLen>=Len){
		outSplit.push(Len);DatLen-=Len;
		}
		if (DatLen!=0){outSplit.push(DatLen);}
		return outSplit;
	}

	var safeSaveLoad=0;
	function safeSave(name,Data,PathLength){//Data as array             
	    // !Elem:LoadSaveDi
	    safeSaveLoad=0;
	    if (!PathLength){PathLength=100000;}
	    var buffer = new ArrayBuffer(Data.length)
	        , temp = new DataView(buffer)
	        ,n32=Math.floor(Data.length/4)
	        ,n16=Math.floor((Data.length-n32*4)/2)
	        ,n8=Data.length-n32*4-n16*2
	        ,Kkey=0, K=-1
	        ,APath=splitPath(n32,PathLength)
	    ;
	    if (APath==''){APath=[0]}

	    var RunSasa=setInterval(function(){
	        if (Kkey==0){Kkey=1;K+=1;}
	        for (var i=0;i<APath[K];i++){
	            temp.setUint32((i+K*PathLength)*4, Data[(i+K*PathLength)*4]*Math.pow(256,3)+Data[(i+K*PathLength)*4+1]*Math.pow(256,2)+Data[(i+K*PathLength)*4+2]*256+Data[(i+K*PathLength)*4+3]);
	        }
	        Kkey=0;
	        safeSaveLoad=Math.round(1000*(K+1)/APath.length)/1000;
	        if (K+1==APath.length){
	            clearInterval(RunSasa);
	            if (n16!=0){temp.setUint16(n32*4, Data[n32*4]*256+Data[n32*4+1]);}
	            if (n8!=0){temp.setUint8(n32*4+n16*2, Data[n32*4+n16*2]);}
	            saveAs(new Blob([   buffer  ], {type: "windows-1252"}), name);
	        }
	    },10);
	}
		
	$scope.getPercentualeScarto = function(validazioneRecordTotali,validazioneRecordValidi) {
		if (validazioneRecordTotali != undefined && validazioneRecordTotali != null&&validazioneRecordValidi != undefined && validazioneRecordValidi != null) {
			percentualeScarto = (validazioneRecordTotali - validazioneRecordValidi) * 100 / validazioneRecordTotali 
			return percentualeScarto.toFixed(2)+"%";
		} else {
			return "N/A";
		}
	}
	
	$scope.getPercentualeCodifica = function(codificaUtilizzazioniCodificate,validazioneUtilizzazioniValide) {
		if (codificaUtilizzazioniCodificate != undefined && codificaUtilizzazioniCodificate != null &&validazioneUtilizzazioniValide != undefined && validazioneUtilizzazioniValide != null) {
			percentualeCodifica = codificaUtilizzazioniCodificate * 100 / validazioneUtilizzazioniValide	
			return percentualeCodifica.toFixed(2)+"%";
		} else {
			return "N/A";
		}
	}
	
	$scope.getPercentualeOpereCodificate = function(codificaOpereCodificate,validazioneRecordValidi) {
		if (codificaOpereCodificate != undefined && codificaOpereCodificate != null &&validazioneRecordValidi != undefined && validazioneRecordValidi != null) {
			percentualeOpereCodificate = codificaOpereCodificate * 100 / validazioneRecordValidi	
			return percentualeOpereCodificate.toFixed(2)+"%";
		} else {
			return "N/A";
		}
	}
	
	$scope.formatDateHour = function(dateString) {
		if (dateString != undefined && dateString != null) {
			var date = new Date(dateString)
			var separator = "-"
			var month = date.getMonth() + 1
			return (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + separator + (month < 10 ? "0" + month : month) + separator + date.getFullYear() + " " + (date.getHours() < 10 ? "0" + date.getHours() : date.getHours()) + ":" + (date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes())
		} else {
			return "N/A";
		}
	}
	$scope.getTextNA= function(string) {
		if (string == undefined || string == null ||string ==="") {
			return "N/A";
		}else{
			return string;
		}
		
	}
	$scope.calcolaPercentuale= function(value,tot) {
		if (value == undefined || value == null ||value ==="") {
			return "N/A";
		}else{
			var percent= (value/tot)*100
			return twoDecimalRound(percent) + "%";
		}
		
	}
	
	
	$scope.getTextFromBool= function(bool) {
		if(bool==null||bool==="null"||bool==undefined){
			return ""
		}
		if(bool===true){
			return "Si"
		}else if(bool===false){
			return "No"
		}
		
		if(bool==="true"){
			return "Si"
		}else if(bool==="false"){
			return "No"
		} else{
			return bool
		}
	}
	
	$scope.showError = function(title, msgs) {
		ngDialog.open({
			template : 'pages/advance-payment/dialog-alert.html',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				$scope.ctrl = {
					title : title,
					message : msgs
				};
				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				$scope.save = function() {
					ngDialog.closeAll(true);
				};
			} ]
		}).closePromise.then(function(data) {
			// no op
		});
	};

	$scope.showAlert = function(title, message) {
		ngDialog.open({
			template : 'pages/advance-payment/dialog-alert.html',
			plain : false,
			width : '50%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				var ngDialogId = $scope.ngDialogId;
				$scope.ctrl = {
					title : title,
					message : message,
					ngDialogId : ngDialogId
				};
				$scope.cancel = function(ngDialogId) {
					ngDialog.close(ngDialogId);
				};
			} ]
		}).closePromise.then(function(data) {
			if (data.value === true) {

			}
		});
	};
	
	$scope.onRefresh();

} ]);


codmanApp.filter('range', function() {
	return function(input, min, max) {
		min = parseInt(min); //Make string input int
		max = parseInt(max);
		for (var i = min; i <= max; i++)
			input.push(i);
		return input;
	};
});