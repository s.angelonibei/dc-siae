/**
 * MultimedialeLocaleCtrl
 * 
 * @path /multimedialeLocale
 */
codmanApp.controller('MultimedialeLocaleRegolareCtrl', [ '$scope', '$route', 'ngDialog', '$routeParams', '$location', '$http', 'Upload', 'configService', function($scope, $route, ngDialog, $routeParams, $location, $http, Upload, configService) {

	$scope.ctrl = {		
		navbarTab : 'ccidConfig',
		config : [],
		filterParameters : [],
		execute : $routeParams.execute,
		hideForm : $routeParams.hideForm,
		selectedPeriodFrom : $routeParams.selectedPeriodFrom || '',
		selectedPeriodTo : $routeParams.selectedPeriodTo || '',
		first : 0,
		last : 50,
		maxrow : 50,
		firstR : 0,
		lastR : 50,
		maxrowR : 50,
		report:[],
		dettailSelected : null
	};


	// Funzione principale richiamata al caricamento della pagina
	$scope.onRefresh = function() {
		
	};

	$scope.navigateToNextPage = function() {
		$scope.getEsecuzioniRicodifica($scope.ctrl.dataAvvioDa,$scope.ctrl.dataAvvioA,$scope.ctrl.last,$scope.ctrl.last + $scope.ctrl.maxrows,50)	 
	};

	$scope.formatNumber = function(number){
 		return twoDecimalRound(number);
	}
	
	$scope.checkEmptyText = function(dataRipartizione){
		if (dataRipartizione!=null||dataRipartizione!=undefined) {
			return "Si"
		}else{
			return "No"
		}
	}
	
	$scope.navigateToPreviousPage = function() {
		$scope.getEsecuzioniRicodifica($scope.ctrl.dataAvvioDa,$scope.ctrl.dataAvvioA,$scope.ctrl.last,Math.max($scope.ctrl.first - $scope.ctrl.maxrows, 0),$scope.ctrl.first,50)
	}	

	$scope.back= function(item) {
		$scope.ctrl.dettailSelected=null;
		$scope.ctrl.dettailStat=null;
	}
	
	$scope.formatDate = function(dateString) {
		if (dateString != undefined && dateString != null) {
			var date = new Date(dateString)
			var separator = "-"
			var month = date.getMonth() + 1
			return (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + separator + (month < 10 ? "0" + month : month)
				+ separator + date.getFullYear()
		} else {
			return "N/A";
		}
	}
	
	$scope.checkSize = function() {
		if($scope.ctrl.storico==null||$scope.ctrl.storico==undefined||$scope.ctrl.storico.length==0){
			return true;
		}else{
			return false;
		}
	}
	
	$scope.apply = function(dataAvvioDa,dataAvvioA) {
		$scope.ctrl.dataAvvioDa=dataAvvioDa
		$scope.ctrl.dataAvvioA=dataAvvioA
		$scope.getEsecuzioniRicodifica(dataAvvioDa,dataAvvioA, 0, 50, 50)
	}
	
	$scope.getEsecuzioniRicodifica = function(dataAvvioDa,dataAvvioA, first, last, maxrow){
	
		$http({
			method : 'POST',
			url : 'rest/multimediale/ricodifica/getEsecuzioniRicodifica',
    			headers: {
                    'Content-Type': 'application/json'},
			data : {
				dataAvvioDa:dataAvvioDa,
				dataAvvioA:dataAvvioA,
				first: first,
				last: last,
				maxrow:maxrow,
			}
		}).then(function successCallback(response) {
			if (response.data.message!=undefined||response.data.message!=null) {
				$scope.showAlert("Errore",response.data.message)
			}
			$scope.ctrl.report = response.data;
			$scope.ctrl.first = response.data.first;
			$scope.ctrl.last = response.data.last;
		
		}, function errorCallback(response) {
			$scope.ctrl.report = []
		});
	}		
	
	$scope.showDettails = function(item) {
		$http({
			method : 'POST',
			url : 'rest/multimediale/ricodifica/getDettaglioEsecuzione',
    			headers: {
                    'Content-Type': 'application/json'
                    	},
			params : {
				idEsecuzione:item.idEsecuzione
			}
		}).then(function successCallback(response) {
			$scope.ctrl.dettailSelected=response.data;
			if($scope.ctrl.dettailSelected.reportAggiornati==null||$scope.ctrl.dettailSelected.reportAggiornati==undefined){
				$scope.ctrl.dettailSelected.reportAggiornati=0;
			}
			$scope.showReportEsecuzione(item.idEsecuzione,0,50)
		}, function errorCallback(response) {

		});
		
		
		
	};
	
	$scope.navigateToNextPageReportEsecuzione = function() {
		$scope.showReportEsecuzione($scope.ctrl.idEsecuzione,$scope.ctrl.lastR,$scope.ctrl.lastR + $scope.ctrl.maxrowsR,50)	 
	};
	
	$scope.navigateToPreviousPageReportEsecuzione = function() {
		$scope.showReportEsecuzione($scope.ctrl.idEsecuzione,$scope.ctrl.lastR,Math.max($scope.ctrl.firstR - $scope.ctrl.maxrowsR, 0),$scope.ctrl.firstR,50)
	}	
	
	$scope.showReportEsecuzione = function(id,first,last) {
		$http({
			method : 'POST',
			url : 'rest/multimediale/ricodifica/getReportEsecuzione',
    			headers: {
                    'Content-Type': 'application/json'
                    	},
			params : {
				idEsecuzione:id,
				first:first,
				last:last
			}
		}).then(function successCallback(response) {
			$scope.ctrl.reportRilavorati=response.data;		
		}, function errorCallback(response) {
			
		});
	};
	
	$scope.downloadReport = function(item,type) {
		var id=item.idMlReport;
		var tipo=type;
		$http({
			method : 'GET',
			url : 'rest/multimediale/locale/downloadReportFile',
    			headers: {
                    'Content-Type': 'application/json'},
			params : {
				id:id,
				tipo:tipo
			}
		}).then(function successCallback(response) {
			var contentDisposition = response.headers('Content-Disposition');
			var filename = contentDisposition.split(';')[1].split('filename')[1].split('=')[1].trim();
			saveAs(new Blob([response.data],{type:"text/plain;charset=UTF-8"}), filename);
		}, function errorCallback(response) {
			$scope.showAlert("Errore","Si è verificato un errore: Report non disponibile")
		});
		
		
	};
	
	$scope.formatDateHour = function(dateString) {
		if (dateString != undefined && dateString != null) {
			var date = new Date(dateString)
			var separator = "-"
			var month = date.getMonth() + 1
			return (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + separator + (month < 10 ? "0" + month : month) + separator + date.getFullYear() + " " + (date.getHours() < 10 ? "0" + date.getHours() : date.getHours()) + ":" + (date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes())
		} else {
			return "N/A";
		}
	}
	$scope.getTextNA= function(string) {
		if (string == undefined || string == null ||string ==="") {
			return "N/A";
		}else{
			return string;
		}
		
	}
	$scope.calcolaPercentuale= function(value,tot) {
		if (value == undefined || value == null ||value ==="") {
			return "N/A";
		}else{
			var percent= (value/tot)*100
			return twoDecimalRound(percent) + "%";
		}
	}
	$scope.getPercentualeScarto = function(validazioneRecordTotali,validazioneRecordValidi) {
		if (validazioneRecordTotali != undefined && validazioneRecordTotali != null&&validazioneRecordValidi != undefined && validazioneRecordValidi != null) {
			percentualeScarto = (validazioneRecordTotali - validazioneRecordValidi) * 100 / validazioneRecordTotali 
			return percentualeScarto.toFixed(2)+"%";
		} else {
			return "N/A";
		}
	}
	
	$scope.formatPercent = function(percent) {
		if (percent != undefined && percent != null) {
			return percent.toFixed(2)+"%";
		} else {
			return "N/A";
		}
	}
	
	
	$scope.getPercentualeCodifica = function(codificaUtilizzazioniCodificate,validazioneUtilizzazioniValide) {
		if (codificaUtilizzazioniCodificate != undefined && codificaUtilizzazioniCodificate != null &&validazioneUtilizzazioniValide != undefined && validazioneUtilizzazioniValide != null) {
			percentualeCodifica = codificaUtilizzazioniCodificate * 100 / validazioneUtilizzazioniValide	
			return percentualeCodifica.toFixed(2)+"%";
		} else {
			return "N/A";
		}
	}
	
	$scope.getTextFromBool= function(bool) {
		if(bool==null||bool==="null"||bool==undefined){
			return ""
		}
		if(bool===true){
			return "Si"
		}else if(bool===false){
			return "No"
		}
		
		if(bool==="true"){
			return "Si"
		}else if(bool==="false"){
			return "No"
		} else{
			return bool
		}
	}
	
	$scope.showError = function(title, msgs) {
		ngDialog.open({
			template : 'pages/advance-payment/dialog-alert.html',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				$scope.ctrl = {
					title : title,
					message : msgs
				};
				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				$scope.save = function() {
					ngDialog.closeAll(true);
				};
			} ]
		}).closePromise.then(function(data) {
			// no op
		});
	};

	$scope.showAlert = function(title, message) {
		ngDialog.open({
			template : 'pages/advance-payment/dialog-alert.html',
			plain : false,
			width : '50%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				var ngDialogId = $scope.ngDialogId;
				$scope.ctrl = {
					title : title,
					message : message,
					ngDialogId : ngDialogId
				};
				$scope.cancel = function(ngDialogId) {
					ngDialog.close(ngDialogId);
				};
			} ]
		}).closePromise.then(function(data) {
			if (data.value === true) {

			}
		});
	};
	
	$scope.onRefresh();

} ]);


codmanApp.filter('range', function() {
	return function(input, min, max) {
		min = parseInt(min); //Make string input int
		max = parseInt(max);
		for (var i = min; i <= max; i++)
			input.push(i);
		return input;
	};
});