codmanApp.controller('VisualizzazioneEventiCtrl', [ '$scope', '$routeParams', '$route', '$http', '$location', 'ngDialog', '$rootScope', '$anchorScroll',  
	function($scope, $routeParams, $route, $http, $location, ngDialog, $rootScope, $anchorScroll) {
	
//	$scope.formatImporto = function(importo){
//		if(importo != undefined && importo != ""){
//			
//		}
//	}
	
	$scope.params = {};
	
	$scope.orderImage = function(name,param){
		
		//get id of html element clicked
		var id = "#" + name;
		
		//get css class of that html element for change the image of arrows
		var classe = angular.element(id).attr("class");

		//set to original position all images of arrows, on each columns.
		var element1 = document.getElementsByClassName("glyphicon glyphicon-sort-by-attributes");
		var element2 = document.getElementsByClassName("glyphicon glyphicon-sort-by-attributes-alt");
		angular.element(element1).attr("class","glyphicon glyphicon-sort");
		angular.element(element2).attr("class","glyphicon glyphicon-sort");

		if(classe != "glyphicon glyphicon-sort-by-attributes" || classe == "glyphicon glyphicon-sort"){
			
			angular.element(id).attr("class","glyphicon glyphicon-sort-by-attributes");
			
		}else{
			angular.element(id).attr("class","glyphicon glyphicon-sort-by-attributes-alt");
		}
		
		$scope.order(param);
		
	}
	
	$scope.orderBy = null;
	
	$scope.order = function(param){
		if($scope.orderBy == param.concat(" asc")){
			$scope.orderBy = param.concat(" desc");
		}else{
			$scope.orderBy = param.concat(" asc");
		}
		$scope.getSampling(false);
	}
	
	$scope.exportExcel = function(){
		
		$location.hash('up');
		$anchorScroll();
		$location.hash('');
		
		$http({
			method: 'GET',
			url: 'rest/performing/cruscotti/exportEventiList',
			params: $scope.params
		}).then( function(response) {

			var linkElement = document.createElement('a');

			try {

				var blob = new Blob([response.data], { type: 'application/csv' });
				var url = window.URL.createObjectURL(blob);
				linkElement.setAttribute('href', url);
                linkElement.setAttribute("download", 'ListaEventiPagatiRicercati.csv');

                var clickEvent = new MouseEvent("click", {
                	"view": window,
                	"bubbles": true,
                	"cancelable": false
                });
                linkElement.dispatchEvent(clickEvent);

			} catch (ex) {

			}
		}, function (response) {
			$scope.showError("Errore","Impossibile fare il download dell'excel.");
		});
	}
	
	$scope.count = true;
	
	$scope.formatDate = function(date){
		if(date != undefined && date != ""){
			var data = date.split("T")[0];
			var day = data.split('-')[2];
			var month = data.split('-')[1];
			var year = data.split('-')[0];
			return day + '-' + month + '-' + year;
		}else{
			return "";
		}
	};
	
	$scope.formatHour = function(hour){
		if(hour != undefined && hour != ""){
			var hourParsed = hour.slice(0,2) + ":" + hour.slice(2,4);
			return hourParsed;
		}else{
			return '';
		}
		
		
	}
	
	$scope.goTo = function(url,param){
		if(url != undefined && param != undefined){
			$rootScope.savedQuery = {pagina : "Evento",params: $scope.params, page: $scope.ctrl.page, totRecords : $scope.totRecords, pageTot: $scope.pageTot};
			$location.path(url + param);
		}
	};
	
	$scope.navigateToNextPage = function() {
		$scope.ctrl.page=$scope.ctrl.page+1;
		$scope.getSampling(false);

	};

	$scope.navigateToPreviousPage = function() {
		$scope.ctrl.page=$scope.ctrl.page-1;
		$scope.getSampling(false);

	};
	
	$scope.navigateToEndPage = function(){
		$scope.ctrl.page = $scope.pageTot;
		$scope.getSampling(false);
	}
	
	// Funzione principale richiamata al caricamento della pagina
	$scope.onRefresh = function() {
		
	};
	
	$scope.ctrl = {
		navbarTab : 'visualizzazioneEventi',
		execute : $routeParams.execute,
		page : 0
		};
	
	$scope.filterApply = function() {
		
		$scope.orderBy = null;
		
		$scope.ctrl.first = 0;
		$scope.ctrl.last = 50;
		$scope.ctrl.page = 0;
		//aggiungere orderBy
		$scope.getSampling(true);
		
	}
	
	//aggiungere orderBy
	//il flag è usato per la count. Se è vero allora fa la count.
	$scope.getSampling = function(flag){

		if($scope.ctrl.selectedContabilityDateFrom == ""){
			var meseInizio = null;
			var annoInizio = null;
		}else{
			if($scope.ctrl.selectedContabilityDateFrom !== undefined){
				var meseInizio = 0;
				var annoInizio = 0;
				var from = $scope.ctrl.selectedContabilityDateFrom.split("-");
		        meseInizio = parseInt(from[0],10);
		        annoInizio = parseInt(from[1],10);
		        
		        console.log("selectedContabilityDateFrom prima dello spit"+$scope.ctrl.selectedContabilityDateFrom);
		        console.log(from);
		        
		        console.log("il mese di inizio è :"+meseInizio);
		        console.log("l'anno di inizio è :"+meseInizio);
			}
		}
		
		if($scope.ctrl.selectedContabilityDateTo == ""){
			var meseFine = null;
			var annoFine = null;
		}else{
			if($scope.ctrl.selectedContabilityDateTo !== undefined){
				var meseFine = 0;
				var annoFine = 0;
		        var from = $scope.ctrl.selectedContabilityDateTo.split("-");
		        meseFine = parseInt(from[0],10);
		        annoFine = parseInt(from[1],10);
		        
		        console.log(from);
		        console.log("Il mese di fine è :"+meseFine);
		        console.log("L'anno di fine è :"+annoFine);
			}
		}
		
		if(annoFine < annoInizio){
			$scope.showError("Errore","Il parametro Contabilità a non può essere precedente al campo Contabilità da");
			console.log(annoFine);
			console.log(annoInizio);
			return;
		}else if (annoFine == annoInizio){
			if(meseFine < meseInizio){
				$scope.showError("Errore","Il parametro Contabilità a non può essere precedente al campo Contabilità da");
			    
				console.log("if di ugualianza"+annoFine);
				console.log(annoInizio);
				return;
			}
		}
		
		if($rootScope.savedQuery == undefined || $rootScope.savedQuery == null){
		
			$scope.params = {
					annoInizioPeriodoContabile : annoInizio,
					meseInizioPeriodoContabile : meseInizio,
					annoFinePeriodoContabile : annoFine,
					meseFinePeriodoContabile : meseFine,
					tipoDocumento: $scope.tipoDocumento,
					presenzaMovimenti: $scope.presenzaMovimenti,
					evento: $scope.idEvento,
					fattura: $scope.fattura,
					voceIncasso: $scope.voceIncasso,
					reversale: $scope.reversale,
					dataInizioEvento: $scope.dataInizioEvento,
					dataFineEvento: $scope.dataFineEvento,
					locale: $scope.locale,
					codiceBA: $scope.codiceBA,
					seprag: $scope.seprag,
					organizzatore: $scope.ivaCF,
					titoloOpera: $scope.titoloOpera,
					numeroPM: $scope.numeroPM,
					permesso: $scope.permesso,
					codiceSAP: $scope.codiceSAP,
					direttoreEsecuzione: $scope.DE,
					fatturaValidata: $scope.fatturaValidata,
					page: $scope.ctrl.page,
					count: flag,
					order : $scope.orderBy
				}
		
		}else{
			$scope.params = $rootScope.savedQuery.params;
			$rootScope.savedQuery = undefined;
		}
		
		$http({
			method: 'GET',
			url: 'rest/performing/cruscotti/eventiPagatiList',
			params: $scope.params
		}).then(function(response){
			if(flag == true){
				$scope.pageTotTemp = Math.floor(response.data/50);
				$scope.totRecordsTemp = response.data;
				if(response.data > 1000){
					$scope.showWarning("Attenzione","Il risultato prevede " + response.data + " record, è consigliabile applicare dei filtri aggiuntivi. Sicuro di voler continuare?");
				}else if (response.data == 0){
					$scope.fillElements(null,null,null,null,null,null);
					$scope.showError("","Non risulta nessun dato per la ricerca effettuata");
				}else{
					$rootScope.pageTot = $scope.pageTotTemp;
					$rootScope.totRecords = $scope.totRecordsTemp;
					//aggiungere orderBy
					$scope.getSampling(false);
				}
			}else{
				var currentPage = response.data.currentPage*50;
				$scope.totRecords = $scope.totRecordsTemp;
				$scope.pageTot = $scope.pageTotTemp;
				$scope.fillElements($scope.totRecords, response.data.currentPage, currentPage, response.data.records.length, response.data.records, $scope.pageTot);
			}
			
		},function(response){
			$scope.fillElements(null,null,null,null,null,null);
			$scope.showError("","Non risulta nessun dato per la ricerca effettuata");
		})
	};
	
	//a seconda di come va la chiamata riempie gli oggetti che servono alla view
	$scope.fillElements = function(totRecords,page,first,last,sampling,pageTot){
		$scope.ctrl.totRecords = totRecords;
		$scope.ctrl.page = page;
		$scope.ctrl.first = first;
		$scope.ctrl.last = totRecords;
		$scope.ctrl.sampling = sampling;
		$scope.ctrl.pageTot = pageTot;
	}
	
	$scope.showWarning = function(title, msgs) {
		var parentScope = $scope
		ngDialog.open({
			template : 'pages/advance-payment/dialog-warning.html',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				$scope.ctrl = {
					title : title,
					message : msgs
				};
				$scope.cancel = function() {
					$rootScope.flag = false;
					ngDialog.closeAll(false);
				};
				$scope.call = function() {
					$rootScope.flag = true;
					parentScope.pageTot = parentScope.pageTotTemp;
					parentScope.totRecords = parentScope.totRecordsTemp;
					ngDialog.closeAll(true);
				};
			} ]
		}).closePromise.then(function(data) {
			// no op
			if($rootScope.flag == true){
				//aggiungere orderBy
				$scope.getSampling(false);
			}
		});
	};
	
	$scope.showError = function(title, msgs) {
		ngDialog.open({
			template : 'pages/advance-payment/dialog-alert.html',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				$scope.ctrl = {
					title : title,
					message : msgs
				};
				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				$scope.save = function() {
					ngDialog.closeAll(true);
				};
			} ]
		}).closePromise.then(function(data) {
		});
	};
	
	showAlert = function(title, message) {
		ngDialog.open({
			template : 'pages/advance-payment/dialog-alert.html',
			plain : false,
			width : '50%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				var ngDialogId = $scope.ngDialogId;
				$scope.ctrl = {
					title : title,
					message : message,
					ngDialogId : ngDialogId
				};
				$scope.cancel = function(ngDialogId) {
					ngDialog.close(ngDialogId);
				};
			} ]
		}).closePromise.then(function(data) {
			
		});
	};
	
	if($rootScope.savedQuery != undefined && $rootScope.savedQuery != null){
		if($rootScope.savedQuery.pagina == "Evento"){
			if($rootScope.savedQuery.params.meseInizioPeriodoContabile != undefined){
				if($rootScope.savedQuery.params.meseInizioPeriodoContabile < 10){
					$scope.ctrl.selectedContabilityDateFrom = '0'+ $rootScope.savedQuery.params.meseInizioPeriodoContabile + '-' + $rootScope.savedQuery.params.annoInizioPeriodoContabile;
				}else{
					$scope.ctrl.selectedContabilityDateFrom = $rootScope.savedQuery.params.meseInizioPeriodoContabile + '-' + $rootScope.savedQuery.params.annoInizioPeriodoContabile;
				}
			}
			if($rootScope.savedQuery.params.meseFinePeriodoContabile != undefined){
				if($rootScope.savedQuery.params.meseFinePeriodoContabile < 10){
					$scope.ctrl.selectedContabilityDateTo = '0'+ $rootScope.savedQuery.params.meseFinePeriodoContabile + '-' + $rootScope.savedQuery.params.annoFinePeriodoContabile;
				}else{
					$scope.ctrl.selectedContabilityDateTo = $rootScope.savedQuery.params.meseFinePeriodoContabile + '-' + $rootScope.savedQuery.params.annoFinePeriodoContabile;
				}
			}
			if($rootScope.savedQuery.params.tipoDocumento != undefined){
				$scope.tipoDocumento = $rootScope.savedQuery.params.tipoDocumento
			}
			
			if($rootScope.savedQuery.params.evento != undefined){
				$scope.idEvento = $rootScope.savedQuery.params.evento
			}
			
			if($rootScope.savedQuery.params.fattura != undefined){
				$scope.fattura =$rootScope.savedQuery.params.fattura
			}
			
			if($rootScope.savedQuery.params.voceIncasso != undefined){
				$scope.voceIncasso = $rootScope.savedQuery.params.voceIncasso
			}
			
			if($rootScope.savedQuery.params.reversale != undefined){
				$scope.reversale =$rootScope.savedQuery.params.reversale
			}

			if($rootScope.savedQuery.params.dataInizioEvento != undefined){
				$scope.dataInizioEvento = $rootScope.savedQuery.params.dataInizioEvento
			}
			
			if($rootScope.savedQuery.params.dataFineEvento != undefined){
				$scope.dataFineEvento =$rootScope.savedQuery.params.dataFineEvento
			}
			
			if($rootScope.savedQuery.params.locale != undefined){
				$scope.locale = $rootScope.savedQuery.params.locale
			}
			
			if($rootScope.savedQuery.params.codiceBA != undefined){
				$scope.codiceBA =$rootScope.savedQuery.params.codiceBA
			}
			
			if($rootScope.savedQuery.params.seprag != undefined){
				$scope.seprag =$rootScope.savedQuery.params.seprag
			}
			
			if($rootScope.savedQuery.params.codiceSAPOrganizzatore != undefined){
				$scope.codiceSAP =$rootScope.savedQuery.params.codiceSAPOrganizzatore 
			}
			
			if($rootScope.savedQuery.params.organizzatore != undefined){
				$scope.ivaCF =$rootScope.savedQuery.params.organizzatore
			}
			
			if($rootScope.savedQuery.params.direttoreEsecuzione != undefined){
				$scope.DE =$rootScope.savedQuery.params.direttoreEsecuzione
			}
			
			if($rootScope.savedQuery.params.permesso != undefined){
				$scope.permesso =$rootScope.savedQuery.params.permesso
			}
			
			if($rootScope.savedQuery.params.titoloOpera != undefined){
				$scope.titoloOpera =$rootScope.savedQuery.params.titoloOpera
			}
			
			if($rootScope.savedQuery.params.numeroPM != undefined){
				$scope.numeroPM =$rootScope.savedQuery.params.numeroPM
			}
			
			if($rootScope.savedQuery.params.fatturaValidata != undefined){
				$scope.fatturaValidata =$rootScope.savedQuery.params.fatturaValidata
			}
			
			if($rootScope.savedQuery.page != undefined){
				$scope.ctrl.page = $rootScope.savedQuery.page
			}
			
			if($rootScope.savedQuery.totRecords != undefined){
				$scope.totRecords = $rootScope.savedQuery.totRecords
			}
			
			if($rootScope.savedQuery.pageTot != undefined){
				$scope.pageTot = $rootScope.savedQuery.pageTot
			}
			
			$scope.getSampling(false);
		}else{
			$rootScope.savedQuery = undefined;
		}
	}
	
}]);