codmanApp.controller('RicProposteCtrl', ['$scope', 'ricProposteService', function ($scope, ricProposteService) {

	$scope.request = {};
	$scope.headers = {};
	$scope.errorMessage ="";
	$scope.warningMessage = "";

	$scope.search = function (request) {
		$scope.errorMessage ="";
		$scope.warningMessage = "";
		console.log('Search Request:', request);
		if ((request.titolo && request.compositori) ||
		((request.titolo || request.titoloAlternativo) &&
		(request.compositori || request.autori || request.interpreti))){
		ricProposteService.ricercaOpere(request).then(function (response) {
			console.log('Search Response:', response);
			$scope.results = response.data;
			$scope.headers = _.cloneDeep($scope.request);
			if(response.data.length == 0) {
			$scope.warningMessage ="Non sono stati trovati risultati!";	
			}
			
		}, function (error) {
			
			$scope.errorMessage ="Si è verificato un problema, riprovare più tardi!";
			console.log('MESSAGGIO DI ERRORE'+$scope.errorMessage);
		});}
		 else {
			$scope.warningMessage ="Non sono stati inseriti tutti i campi necessari per la ricerca!";	
			}
	};

	$scope.resetData = function () {
		$scope.request = {};
		$scope.errorMessage ="";
		$scope.warningMessage ="";
	};

	$scope.getInterpreti = function (interpreti) {
		if (interpreti && interpreti.length >= 12) {
			var newList = interpreti.split(',',12);
			content = newList.toString();
			return content;
		} else {
			return interpreti;
		}
	};

	$scope.getListaInterpreti = function (interpreti) {
		let content = '';
		if(interpreti) {
		var newList =interpreti.split(',');
		content = (newList.slice(12)).toString();
		return content;
		}
		return content;
	};

}]);

codmanApp.service('ricProposteService', ['$http', function ($http) {

	var service = this;
	service.ricercaOpereServiceUrl = 'rest/ricercaOpere/';

	service.ricercaOpere = function (data) {
		return $http({
			method: 'POST',
			url: `${service.ricercaOpereServiceUrl}`,
			data: data
		})
	};

}]);
