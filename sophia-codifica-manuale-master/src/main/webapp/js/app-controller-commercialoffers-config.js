/**
 * commercialConfigCtrl
 * 
 * @path /commercialConfig
 */
codmanApp.controller('CommercialOffersConfigCtrl', ['$scope','$route', 'ngDialog', '$routeParams', '$location', '$http', '$q', function($scope,$route, ngDialog, $routeParams, $location, $http, $q) {

	$scope.ctrl = {
		navbarTab: 'configurazioneCommercialOffers',
		commercialConfig: {},
		filterParameters: [],
		execute: $routeParams.execute,
		hideForm: $routeParams.hideForm,
		maxrows: 20,
		first: $routeParams.first||0,
		last: $routeParams.last||20,
		selectedDsp : $routeParams.dsp||'*',
		selectedUtilization : $routeParams.utilization||'*',
		selectedOffer : $routeParams.offer||'*'
	};		
			
	$scope.onAzzera = function() {
		$scope.ctrl.execute = false;
		$scope.ctrl.first = 0;
		$scope.ctrl.last = $scope.ctrl.maxrows;
		};
		
	$scope.navigateToNextPage = function() {
		$location.search('execute', 'true')
		.search('hideForm', $scope.ctrl.hideForm)
		.search('first', $scope.ctrl.last)
		.search('last', $scope.ctrl.last + $scope.ctrl.maxrows);
	};
	
	$scope.navigateToPreviousPage = function() {
		$location.search('execute', 'true')
			.search('hideForm', $scope.ctrl.hideForm)
			.search('first', Math.max($scope.ctrl.first - $scope.ctrl.maxrows, 0))
			.search('last', $scope.ctrl.first);
	};
	

	// Funzione principale richiamata al caricamento della pagina
	$scope.onRefresh = function() {
		// get all commercial offers configurations

		if ($scope.ctrl.execute) {
			$scope.getPagedResults($scope.ctrl.first,
					$scope.ctrl.last,
					$scope.ctrl.selectedDsp,
					$scope.ctrl.selectedUtilization,
					$scope.ctrl.selectedOffer);
		} else {
			$scope.getPagedResults(0,
					$scope.ctrl.maxrows);
		}
	
	};
	
	
	$scope.getPagedResults = function(first, last, dsp, utilization, offer) {
		$http({
			method: 'GET',
			url: 'rest/commercialOffers/all',
			params: {
				first: first,
				last: last,
				dsp : (typeof dsp !== 'undefined' ? dsp : ""),
				utilization : (typeof utilization !== 'undefined' ? utilization : ""),
				offer : (typeof offer !== 'undefined' ? offer : "")
			}
		}).then(function successCallback(response) {
			$scope.ctrl.commercialConfig = response.data;
			$scope.ctrl.first = response.data.first;
			$scope.ctrl.last = response.data.last;
		}, function errorCallback(response) {
			$scope.ctrl.commercialConfig = {};
		});

	};
	

	
	//funzione che renderizza il popup per l'aggiunta di una configurazione del DSP
	$scope.showNewCommercialConfig = function(event) {
		ngDialog.open({
			template: 'pages/commercial-offer-config/dialog-new-commercialconfig.html',
			plain: false,
			width: '60%',
			controller: ['$scope', 'ngDialog', '$http', function($scope, ngDialog, $http) {
				$scope.ctrl = {
					dspList : [],
					utilizationList : [],
					serviceTypeList : [],
					useTypeList : [],
					appliedTariffList : []
				};

                var promise1 = $http({method: 'GET', url: 'rest/data/dspUtilizations'});
                var promise2 = $http({method: 'GET', url: 'rest/data/ccidConfigurationData'});
                $q.all([promise1, promise2]).then(function successCallback(response) {

                        $scope.ctrl.dspList = response[0].data.dsp;
                        $scope.ctrl.utilizationList = response[0].data.utilizations;
                        $scope.ctrl.serviceTypeList = response[1].data.serviceType;
					    $scope.ctrl.useTypeList = response[1].data.useType;
					    $scope.ctrl.appliedTariffList = response[1].data.appliedTariff;

                    }, function errorCallback(response) {
                        $scope.ctrl.dspList = [ ];
                        $scope.ctrl.utilizationList = [ ];
                });

			    $scope.openErrorDialog = function (errorMsg) {
			        ngDialog.open({ 
			        	template: 'errorDialogId',
			        	plain: false,
						width: '40%',
			        	data: errorMsg
			        	
			        });
			    };
				
				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				
							
				$scope.save = function() {
					$http({
						method: 'POST',
						url: 'rest/commercialOffers',
						headers: {
				            'Content-Type': 'application/json'},
						data: {
							idDSP: $scope.selectedDspConfig.idDsp,
							idUtilizationType: $scope.selectedUtilization.idUtilizationType,
							offering: $scope.selectedOffer,
							serviceType: $scope.selectedServiceType.code,
						    useType: $scope.selectedUseType.code,
						    appliedTariff: $scope.selectedAppliedTariff.code,
						    tradingBrand: $scope.selectedTradingBrand
						}
					}).then(function successCallback(response) {
						ngDialog.closeAll(true);
						$route.reload();
					}, function errorCallback(response) {
						if(response.status == 409){
							$scope.openErrorDialog("Configurazione già presente.");	
						}else{
							$scope.openErrorDialog(response.statusText);	
						}
						
					});
				};
				
				
				
				
			}]
		}).closePromise.then(function (data) {
			if (data.value === true) {
				// no op
			}
		});
	};
	
	
	$scope.showDeleteCommercialConfig = function(event, commercialConfigToRemove) {
		ngDialog.open({
			template: 'pages/commercial-offer-config/dialog-remove-commercialconfig.html',
			plain: false,
			width: '60%',
			controller: ['$scope', 'ngDialog', '$http', function($scope, ngDialog, $http) {
				$scope.ctrl = {
					commercialConfigToRemove: commercialConfigToRemove
				};
				

				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				

				$scope.deleteCommercialConfig = function() {
					$http({
						method: 'DELETE',
						url: 'rest/commercialOffers',
						headers: {
				            'Content-Type': 'application/json'},
						data: {
							idCommercialOffering : $scope.ctrl.commercialConfigToRemove.idCommercialOffer,
							idDSP: $scope.ctrl.commercialConfigToRemove.idDsp,
							idUtilizationType: $scope.ctrl.commercialConfigToRemove.idUtilization,
							offering: $scope.ctrl.commercialConfigToRemove.offer					
						}
					
					}).then(function successCallback(response) {
						ngDialog.closeAll(true);
						$route.reload();
					}, function errorCallback(response) {
						if(response.status == 409){
                            $scope.openErrorDialog("Non è possibile eliminare la seguente configurazione perchè in uso da altri servizi");
                        }else{
                            $scope.openErrorDialog(response.statusText);
                        }
					});
					
				};
				
			    $scope.openErrorDialog = function (errorMsg) {
			        ngDialog.open({ 
			        	template: 'errorDialogId',
			        	plain: false,
						width: '40%',
			        	data: errorMsg
			        	
			        });
			    };
			}]
		}).closePromise.then(function (data) {
			if (data.value === true) {
				// no op
			}
		});
	};
	
		
	
	$scope.showEditCommercialConfig = function(event, commercialConfigToModify) {
		ngDialog.open({
			template: 'pages/commercial-offer-config/dialog-modify-commercialconfig.html',
			plain: false,
			width: '60%',
			controller: ['$scope', 'ngDialog', '$http', function($scope, ngDialog, $http) {
				$scope.ctrl = {
					commercialConfigToModify: commercialConfigToModify,
					dspList : [],
					utilizationList : [],
					serviceTypeList : [],
					useTypeList : [],
					appliedTariffList : []
				};

                var promise1 = $http({method: 'GET', url: 'rest/data/dspUtilizations'});
                var promise2 = $http({method: 'GET', url: 'rest/data/ccidConfigurationData'});
                $q.all([promise1, promise2]).then(function successCallback(response) {

                        $scope.ctrl.dspList = response[0].data.dsp;
                        $scope.ctrl.utilizationList = response[0].data.utilizations;
                        $scope.ctrl.serviceTypeList = response[1].data.serviceType;
                        $scope.ctrl.useTypeList = response[1].data.useType;
                        $scope.ctrl.appliedTariffList = response[1].data.appliedTariff;


                        $scope.selectedOffer = commercialConfigToModify.offer;
                        $scope.selectedUtilization = {idUtilizationType : commercialConfigToModify.idUtilization};
                        $scope.selectedServiceType = {code : commercialConfigToModify.ccidServiceTypeCode};
                        $scope.selectedUseType = {code : commercialConfigToModify.ccidUseTypeCode};
                        $scope.selectedAppliedTariff  = {code : commercialConfigToModify.ccidAppliedTariffCode};
                        $scope.selectedTradingBrand = commercialConfigToModify.tradingBrand

                    }, function errorCallback(response) {
                        $scope.ctrl.dspList = [ ];
                        $scope.ctrl.utilizationList = [ ];
                        $scope.ctrl.serviceTypeList = [];
                        $scope.ctrl.useTypeList = [];
                        $scope.ctrl.appliedTariffList = [];
                });


				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				

				$scope.modifycommercialConfig = function() {

					$http({
						method: 'PUT',
						url: 'rest/commercialOffers',
						headers: {
				            'Content-Type': 'application/json'},
						params: {
							previdDSP: $scope.ctrl.commercialConfigToModify.idDsp,
							previdUtilizationType: $scope.ctrl.commercialConfigToModify.idUtilization,
							prevoffering: $scope.ctrl.commercialConfigToModify.offer,
                            prevCcidServiceTypeCode: $scope.ctrl.commercialConfigToModify.ccidServiceTypeCode,
                            prevCcidUseTypeCode: $scope.ctrl.commercialConfigToModify.ccidUseTypeCode,
							prevAppliedTariffCode: $scope.ctrl.commercialConfigToModify.ccidAppliedTariffCode,
                            prevTradingBrand: $scope.ctrl.commercialConfigToModify.tradingBrand,
							newidUtilizationType: $scope.selectedUtilization.idUtilizationType,
							newoffering: $scope.selectedOffer,
							newserviceType: $scope.selectedServiceType.code,
                            newuseType: $scope.selectedUseType.code,
                            newappliedTariff: $scope.selectedAppliedTariff.code,
                            newtradingBrand: $scope.selectedTradingBrand


						}
					
					}).then(function successCallback(response) {
						ngDialog.closeAll(true);
						$route.reload();
					}, function errorCallback(response) {
						if(response.status == 409){
							$scope.openErrorDialog("Configurazione già presente.");	
						}else{
							$scope.openErrorDialog(response.statusText);	
						}
					});
					
				};
				
			    $scope.openErrorDialog = function (errorMsg) {
			        ngDialog.open({ 
			        	template: 'errorDialogId',
			        	plain: false,
						width: '40%',
			        	data: errorMsg
			        	
			        });
			    };
			}]
		}).closePromise.then(function (data) {
			if (data.value === true) {
				// no op
			}
		});
	};
	
	 $scope.$on('filterApply', function(event, parameters) { 
		 $scope.ctrl.filterParameters = parameters;
			$location.search('execute', 'true')
			.search('hideForm', false)
			.search('first', 0)
			.search('last', $scope.ctrl.maxrows)
			.search('dsp', parameters.dsp)
			.search('utilization',parameters.utilization)
			.search('offer', parameters.offer);	 
			$scope.ctrl.hideForm = false;
		
		 });
	
	$scope.onRefresh();
	

	
}]);





codmanApp.filter('range', function() {
	  return function(input, min, max) {
		    min = parseInt(min); //Make string input int
		    max = parseInt(max);
		    for (var i=min; i<=max; i++)
		      input.push(i);
		    return input;
		  };
});  

codmanApp.filter('month_range', function() {
	  return function(input, min, max) {
		    min = parseInt(min); //Make string input int
		    max = parseInt(max);
		    for (var i=min; i<=max; i++)
		      input.push(i + "M");
		    return input;
		  };
}); 

