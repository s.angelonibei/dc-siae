codmanApp.controller('PerformingCodificaManualeEspertoCtrl', [ '$scope', 'PerformingCodificaManualeEspertoService', '$interval', '$window','configService',
'configurationsUtilityService','$sce', function($scope, CMService, $interval, $window, configService,configurationsUtilityService, $sce) {
	
	$scope.user = angular.element("#headerLinksBig").text().trim();
	$scope.urlUlisse = configService.ulisseUrlCodiceOpera;

	$scope.ctrl = {
			assegnazioneAutomatica: false,
			ricercaVoce: true,
			ricercaTitoloAutore: true
	};

	$scope.warningTimer = false;

	$scope.showProposte = true;

	$scope.deleteElements = function(combana){
		delete combana.statApprov;
		delete combana.codApprov
	};

	$scope.filterApply = function(params){

		$scope.parametriFinali = angular.copy(params);

		$scope.parametriFinali.user = $scope.user;

		CMService.getCodificaManuale($scope.parametriFinali).then(function(response){
			if(response.data.length < 1){
				CMService.showError("Errore","Non ci sono altre combane da codificare per i filtri di ricerca inseriti");
				$scope.data = [];
				return
			}
			$scope.data = response.data.codifiche;
			$scope.numeroCandidate = response.data.nCandidate;
			$scope.extraFieldsToShow = response.data.extraFieldsToShow || [];
			$scope.getTimerSessione($scope.user)
		},function(response){
			CMService.showError("Errore","Non è stato possibile ottenere i dati")
		})
	};

	$scope.sceltaProposte = function(opereAccettate){
		CMService.askConfirm("Attenzione","Sei sicuro di aver identificato il giusto abbinamento?")
		.closePromise.then(function(model){
			if(model.value === true){

				angular.forEach(opereAccettate,function(item){
					if(item.codApprov === undefined){
						item.statApprov = 'S'
					}
				});

				CMService.sceltaProposte(opereAccettate,$scope.user).then(function(response){
					CMService.showError("Successo","L'abbinamento della proposta con le combane è avvenuto con successo")
					.closePromise.then(function(model){
						$scope.filterApply($scope.parametriFinali)
					})
				},function(response){
					if(response.data === 520){
						CMService.showError("Errore","Non è stato possibile associare la proposta alla combana: Sessione scaduta, per continuare a codificare effettua una nuova ricerca");
						return
					}
					CMService.showError("Errore","Non è stato possibile associare la proposta alla combana")
				})
			}
		})
	};

	$scope.openModelCodifica = function(combana){
		CMService.openModelCodifica(combana,$scope.user,$scope.filterApply,$scope.parametriFinali)
	};

	$scope.getTimerSessione = function(user){
		CMService.getTimerSessione(user).then(function(response){
			var milliseconds = response.data;
            var secondsFloor = Math.floor(milliseconds.sessionTimeResidual/1000);
			var minutes = Math.floor(secondsFloor/60);
			var seconds = secondsFloor - (minutes * 60);
			$scope.startTimer(minutes,seconds)
		},function(response){
			CMService.showError("Errore","Non è stato possibile aggiornare il timer")
		})
	};

	$scope.resetTimer = function(){
		$scope.warningTimer = false;
		CMService.resetTimer($scope.user).then(function(response){
			var milliseconds = response.data;
            var secondsFloor = Math.floor(milliseconds.sessionTimeResidual/1000);
			var minutes = Math.floor(secondsFloor/60);
			var seconds = secondsFloor - (minutes * 60);
			$scope.startTimer(minutes,seconds)
		},function(response){
			CMService.showError("Errore","Non è stato possibile aggiornare il timer")
		})
	};

	$scope.startTimer = function(minutes,seconds){

		if (angular.isDefined($scope.start)) {
            $interval.cancel($scope.start);
        }

		$scope.minutes = minutes;
		$scope.seconds = seconds;

		$scope.start = $interval(function(){
			if($scope.seconds > 0){
				$scope.seconds = $scope.seconds - 1
			}else{
				$scope.minutes = $scope.minutes - 1;
				$scope.seconds = 59
			}

			if($scope.minutes === 0){
				$scope.warningTimer = true
			}

			if($scope.minutes === 0 && $scope.seconds === 0){
				$scope.warningTimer = false;
				$scope.stopTimer()
			}
		},1000);

		$scope.stopTimer = function(){
			$interval.cancel($scope.start);
		}
	}

	$scope.goToDocumentazione = function (event, codiceOpera){
		//TODO da aggiustare con il vero link di ulisse
		$window.open(`${$scope.urlUlisse}${codiceOpera}`, '_blank');
		event.stopPropagation();
		event.preventDefault();
	}

	$scope.getMaturatoHtml = function (maturato) {
		return $sce.trustAsHtml(configurationsUtilityService.getMaturato(maturato));
	}

	$scope.getRischioHtml = function (rischio) {
		return $sce.trustAsHtml(configurationsUtilityService.getRischio(rischio));
	}

	$scope.getStatoOperaHtml = function (proposta) {
		return $sce.trustAsHtml(configurationsUtilityService.getStatoOpera(proposta));
	}



}]);

codmanApp.service('PerformingCodificaManualeEspertoService', ['ngDialog', '$http', function(ngDialog, $http){

	var service = this;

	service.checkDate = function(from, to){
		if(from !== undefined && to !== undefined){
			var comparisonFrom = from.split("-")[2] + from.split("-")[1] + from.split("-")[0];
			var comparisonTo = to.split("-")[2] + to.split("-")[1] + to.split("-")[0];
			return comparisonTo >= comparisonFrom;
		}else{
			return true;
		}
	};

	service.showError = function(title,msgs){
		return ngDialog.open({
			template : 'pages/advance-payment/dialog-alert.html',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
					var ngDialogId = $scope.ngDialogId;
					$scope.ctrl = {
						title : title,
						message : msgs,
						ngDialogId : ngDialogId
					};
					$scope.cancel = function() {
						ngDialog.close(ngDialogId);
					};
				}]
			})
		};

	service.askConfirm = function(title,msgs){
		return ngDialog.open({
			template : 'pages/advance-payment/dialog-confirm.html',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				var ngDialogId = $scope.ngDialogId;
				$scope.ctrl = {
					title : title,
					message : msgs,
					ngDialogId : ngDialogId
				};
				$scope.cancel = function(ngDialogId) {
					ngDialog.close(ngDialogId);
				};
				$scope.save = function() {
					ngDialog.closeAll(true);
				};
			} ]
		})
	};

	service.openModelCodifica = function(combana,user,init,params){
		return ngDialog.open({
			template : 'pages/advance-payment/dialog-codifica.html',
			plain : false,
			width : '90%',
			controller : [ '$scope','ngDialog', function($scope,ngDialog) {
				var vm = this;
                vm.user = $scope.ngDialogData.user;
                vm.combana = $scope.ngDialogData.combana;

				vm.cancel = function() {
					ngDialog.closeAll(false);
				};

				vm.search = function(codiceOpera){
					if(!new RegExp('^[0-9]{11}$').exec(codiceOpera)){
						service.showError("Attenzione","Il codice opera deve essere composte da 11 numeri");
						return
					}

					service.searchOpera(codiceOpera).then(function(response){
						vm.result = response.data.content[codiceOpera];
						if(!vm.result){
							service.showError("Errore","Non è stata trovata nessuna opera per il codice opera inserito");

						}
					},function(response){
						service.showError("Errore","Non è stato possibile ottenere informazioni riguardo al codice opera inserito")
					})
				};

				vm.combine = function(result){
					service.askConfirm("Attenzione","Sei sicuro di aver identificato il giusto abbinamento?")
					.closePromise.then(function(model){
						if(model.value === true){
							var object = [];
							vm.combana.statApprov = "M";
							vm.combana.codApprov = result.codiceOpera;
							object.push(vm.combana);
							service.sceltaProposte(object,vm.user).then(function(response){
								service.showError("Successo","Abbinazione avvenuta con successo");
								init(params);
								vm.cancel()
							},function(response){
								service.showError("Errore","Non è stato possibile abbinare il codice opera")
							})
						}
					})
				}
			}],
			controllerAs: 'vm',
            data:{
                combana: combana,
                user: user
            },
		})
	};

	service.resetTimer = function(user){
		return $http({
			method: 'GET',
			url: 'rest/performing/codifica/manuale/refreshSession',
			params: {user:user}
		})
	};

	service.getCodificaManuale = function(params){
		return $http({
			method:'GET',
			url: 'rest/performing/codifica/manuale/esperto/getCodificaManualeEsperto',
			params: params
		})
	};

	service.searchOpera = function(codiceOpera){
		return $http({
			method:'GET',
			url: 'rest/performing/cruscotti/ricercaOpere',
			params: {
				codiceOpera : codiceOpera
			}
		})
	};

	service.sceltaProposte = function(opere,user){
		return $http({
			method:'POST',
			url: 'rest/performing/codifica/manuale/approvaOpera',
			data: {
				codificaOpereDTO: opere,
				username: user
			}
		})
	};

	service.getTimerSessione = function(user){
		return $http({
			method: 'GET',
			url: 'rest/performing/codifica/manuale/getTimerSessione',
			params: {
				user: user
			}
		})
	}

}]);



