(function() {
    'use strict';
    angular.module('codmanApp').controller("AggiornamentoSunController", AggiornamentoSunController);


    AggiornamentoSunController.$inject = ["AggiornamentoSunService","ngDialog"]

    function AggiornamentoSunController(AggiornamentoSunService,ngDialog) {
	        var vm = this;
	        
	        vm.checkValue  = function(value) {
	        		if(value==null||value==undefined||value==''){
	        			return 0;
	        		}else{
	        			return value;
	        		}
	        }
	
	        vm.getPeriodiRipartizione = function() {
		        	AggiornamentoSunService.dateAggiornamento().then(function(response){
		    			if(response.data.length < 1){
		    				vm.dateAggiornamento = []
		    			}else{
		    				vm.dateAggiornamento = response.data
		    				
		    			}
		    		},function(response){
		    			showError("Errore","Non è stato possibile ottenere lo storico delle Regole precedentemente salvate")
		    		})
	        }
	        vm.parseDateTime = function(date) {
	            return RRService.parseDateTime(date);
	        }
	
	        vm.parseDate = function(date) {
	            return RRService.parseDate(date);    
	        }
	        
	        vm.search = function(data){
	    			vm.init(data)
	    		}
	        
	        vm.exportProgrammaMusicale = function(data){
	         	AggiornamentoSunService.exportProgrammaMusicale(data).then(function(response){
	    			if(response.data.length < 1){
		    			showError("Errore","Non sono presenti bonifiche")
	    			}else{

	    				var linkElement = document.createElement('a');
		    			try {
		    				var blob = new Blob([response.data], { type: 'application/csv' });
		    				var url = window.URL.createObjectURL(blob);
		    				linkElement.setAttribute('href', url);
	                    linkElement.setAttribute("download", 'Lista Bonifiche Programma Musicali.csv');
	                    var clickEvent = new MouseEvent("click", {
		                    	"view": window,
		                    	"bubbles": true,
		                    	"cancelable": false
	                    });
	                    linkElement.dispatchEvent(clickEvent);
		    			} catch (ex) {
			    			showError("Errore","Non è stato possibile scaricare la bonifica dei programmi musicale")
		    			}
	    			}
		    		},function(response){
		    			showError("Errore","Non è stato possibile scaricare la bonifica dei programmi musicale")
		    		})
		    	}
	        
	        vm.exportMovimentiContabili = function(data){
	         	AggiornamentoSunService.exportMovimentiContabili(data).then(function(response){
	    			if(response.data.length < 1){
		    			showError("Errore","Non sono presenti bonifiche")
	    			}else{

	    				var linkElement = document.createElement('a');
		    			try {
		    				var blob = new Blob([response.data], { type: 'application/csv' });
		    				var url = window.URL.createObjectURL(blob);
		    				linkElement.setAttribute('href', url);
	                    linkElement.setAttribute("download", 'Lista Bonifiche Movimenti Contabili.csv');
	                    var clickEvent = new MouseEvent("click", {
		                    	"view": window,
		                    	"bubbles": true,
		                    	"cancelable": false
	                    });
	                    linkElement.dispatchEvent(clickEvent);
		    			} catch (ex) {
			    			showError("Errore","Non è stato possibile scaricare la bonifica dei movimenti contabili")
		    			}
	    			}
		    		},function(response){
		    			showError("Errore","Non è stato possibile scaricare la bonifica dei movimenti contabili")
		    		})
		    	}
	        
	        vm.exportEventiPagati = function(data){
	         	AggiornamentoSunService.exportEventiPagati(data).then(function(response){
	    			if(response.data.length < 1){
		    			showError("Errore","Non sono presenti bonifiche")
	    			}else{

	    				var linkElement = document.createElement('a');
		    			try {
		    				var blob = new Blob([response.data], { type: 'application/csv' });
		    				var url = window.URL.createObjectURL(blob);
		    				linkElement.setAttribute('href', url);
	                    linkElement.setAttribute("download", 'Lista Bonifiche Eventi Pagati.csv');
	                    var clickEvent = new MouseEvent("click", {
		                    	"view": window,
		                    	"bubbles": true,
		                    	"cancelable": false
	                    });
	                    linkElement.dispatchEvent(clickEvent);
		    			} catch (ex) {
			    			showError("Errore","Non è stato possibile scaricare la bonifica degli eventi pagati")
		    			}
	    			}
		    		},function(response){
		    			showError("Errore","Non è stato possibile scaricare la bonifica degli eventi pagati")
		    		})
		    	}
	        
	        vm.exportDirettoriEsecuzione = function(data){
	         	AggiornamentoSunService.exportDirettoriEsecuzione(data).then(function(response){
	    			if(response.data.length < 1){
		    			showError("Errore","Non sono presenti bonifiche")
	    			}else{

	    				var linkElement = document.createElement('a');
		    			try {
		    				var blob = new Blob([response.data], { type: 'application/csv' });
		    				var url = window.URL.createObjectURL(blob);
		    				linkElement.setAttribute('href', url);
	                    linkElement.setAttribute("download", 'Lista Bonifiche Direttori Esecuzione.csv');
	                    var clickEvent = new MouseEvent("click", {
		                    	"view": window,
		                    	"bubbles": true,
		                    	"cancelable": false
	                    });
	                    linkElement.dispatchEvent(clickEvent);
		    			} catch (ex) {
			    			showError("Errore","Non è stato possibile scaricare la bonifica dei direttori d'esecuzione")
		    			}
	    			}
		    		},function(response){
		    			showError("Errore","Non è stato possibile scaricare la bonifica dei direttori d'esecuzione")
		    		})
		    	}
	        
	        vm.exportManifestazioni = function(data){
	         	AggiornamentoSunService.exportManifestazioni(data).then(function(response){
	    			if(response.data.length < 1){
		    			showError("Errore","Non sono presenti bonifiche")
	    			}else{

	    				var linkElement = document.createElement('a');
		    			try {
		    				var blob = new Blob([response.data], { type: 'application/csv' });
		    				var url = window.URL.createObjectURL(blob);
		    				linkElement.setAttribute('href', url);
	                    linkElement.setAttribute("download", 'Lista Bonifiche Manifestazioni.csv');
	                    var clickEvent = new MouseEvent("click", {
		                    	"view": window,
		                    	"bubbles": true,
		                    	"cancelable": false
	                    });
	                    linkElement.dispatchEvent(clickEvent);
		    			} catch (ex) {
			    			showError("Errore","Non è stato possibile scaricare la bonifica delle manifestazioni")
		    			}
	    			}
		    		},function(response){
		    			showError("Errore","Non è stato possibile scaricare la bonifica delle manifestazioni")
		    		})
		    	}
	        
	        vm.init = function(data){
		        	AggiornamentoSunService.aggiornamentiSun(data).then(function(response){
		        		if(response.data.length < 1){
		    				showError("Errore","Non è stato trovata nessuna Regola per i filtri inseriti")
		    				vm.aggiornamentiSun = []
		    			}else{
		    				vm.aggiornamentiSun = response.data
		    				
		    			}
		    		},function(response){
		    			showError("Errore","Non è stato possibile ottenere lo storico delle Regole precedentemente salvate")
		    		})
		    	}		
	        
	        function showError(title, msgs) {
	            ngDialog.open({
	                template: 'pages/advance-payment/dialog-alert.html',
	                plain: false,
	                width: '60%',
	                controller: ['$scope', 'ngDialog', function($scope, ngDialog) {
	                    $scope.ctrl = {
	                        title: title,
	                        message: msgs
	                    };
	                    $scope.cancel = function() {
	                        ngDialog.close($scope.ngDialogId);
	                    };
	                    $scope.save = function() {
	                        ngDialog.closeAll(true);
	                    };
	                }]
	            }).closePromise.then(function(data) {

	            });
	        }
	        vm.getPeriodiRipartizione();
	}
    	
    codmanApp.service('AggiornamentoSunService', ['ngDialog', '$http', 'Upload', function(ngDialog, $http, Upload) {

        var service = this;
        
        service.dateAggiornamento = function(){
			return $http({
				method: 'GET',
				url: 'rest/performing/aggiornamentiSun/periodiAggiornamenti',
				params: {}
			})
		}
        
        service.aggiornamentiSun = function(data){
	    		return $http({
	    			method: 'GET',
	    			url: 'rest/performing/aggiornamentiSun/aggiornamentiSun',
	    			params: {
	    				dataAggiornamento:data
	    			}
	    		})
    		}

        service.exportProgrammaMusicale = function(data){
	    		return $http({
	    			method: 'GET',
	    			url: 'rest/performing/aggiornamentiSun/exportProgrammaMusicale',
	    			params: {
	    				dataBonifica:data
	    			}
	    		})
    		}

        service.exportMovimentiContabili = function(data){
	    		return $http({
	    			method: 'GET',
	    			url: 'rest/performing/aggiornamentiSun/exportMovimentiContabili',
	    			params: {
	    				dataBonifica:data
	    			}
	    		})
    		}

        service.exportEventiPagati = function(data){
	    		return $http({
	    			method: 'GET',
	    			url: 'rest/performing/aggiornamentiSun/exportEventiPagati',
	    			params: {
	    				dataBonifica:data
	    			}
	    		})
    		}

        service.exportDirettoriEsecuzione = function(data){
	    		return $http({
	    			method: 'GET',
	    			url: 'rest/performing/aggiornamentiSun/exportDirettoriEsecuzione',
	    			params: {
	    				dataBonifica:data
	    			}
	    		})
    		}
        
        service.exportManifestazioni = function(data){
    		return $http({
    			method: 'GET',
    			url: 'rest/performing/aggiornamentiSun/exportManifestazioni',
    			params: {
    				dataBonifica:data
    			}
    		})
		}
        
        
    }]);
})();