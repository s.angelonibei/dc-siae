/**
 * DsrProcessConfigCtrl
 * 
 * @path /dsrConfig
 */
codmanApp.controller('DsrProcessConfigCtrl', ['$scope','$route', 'ngDialog', '$routeParams', '$location', '$http', function($scope, $route, ngDialog, $routeParams, $location, $http) {

	$scope.ctrl = {
		navbarTab: 'configurazioneDSR',
		dsrProcessConfig: {},
		execute: $routeParams.execute,
		hideForm: $routeParams.hideForm,
		maxrows: 20,
		first: $routeParams.first||0,
		last: $routeParams.last||20,
		filter: $routeParams.filter,
		selectedDsp : $routeParams.dsp||'*',
		selectedUtilization : $routeParams.utilization||'*',
		selectedCountry : $routeParams.country||'*'
	};		
			
	
	//Funzione principale richiamata al caricaento della pagina
	$scope.onRefresh = function() {
		// get All DSR Process Configuration					
		if ($scope.ctrl.execute) {
			$scope.getPagedResults($scope.ctrl.first, 
					$scope.ctrl.last,
					$scope.ctrl.selectedDsp,
					$scope.ctrl.selectedCountry,
					$scope.ctrl.selectedUtilization);
		}else{
			
			$scope.getPagedResults(0, $scope.ctrl.maxrows);			
		}
	};
	
	
	$scope.getPagedResults = function(first, last, dsp, country, utilization) {
		$http({
			method: 'GET',
			url: 'rest/dsrConfig/allConfigDetail',
			params: {
				first: first,
				last: last,
				dsp: (typeof dsp !== 'undefined' ? dsp : ""),
				country: (typeof country !== 'undefined' ? country : ""),
				utilization: (typeof utilization !== 'undefined' ? utilization : "")
			}
		}).then(function successCallback(response) {
			$scope.ctrl.dsrProcessConfig = response.data;
			$scope.ctrl.first = response.data.first;
			$scope.ctrl.last = response.data.last;
		}, function errorCallback(response) {
			$scope.ctrl.dsrProcessConfig = {};
		});

	};
	
	$scope.navigateToNextPage = function() {
		$location.search('execute', 'true')
		.search('hideForm', $scope.ctrl.hideForm)
		.search('first', $scope.ctrl.last)
		.search('last', $scope.ctrl.last + $scope.ctrl.maxrows);
	};
	
	$scope.navigateToPreviousPage = function() {
		$location.search('execute', 'true')
			.search('hideForm', $scope.ctrl.hideForm)
			.search('first', Math.max($scope.ctrl.first - $scope.ctrl.maxrows, 0))
			.search('last', $scope.ctrl.first);
	};
	
	
	
	
	
	
	//funzione che renderizza il popup per la modifica della configurazione del processo per DSR
	$scope.showNewProcessConfig = function(event, dsrProcessConfig) {
		ngDialog.open({
			template: 'pages/dsrprocessconfig/dialog-new.html',
			plain: false,
			width: '60%',
			controller: ['$scope', 'ngDialog', '$http', function($scope, ngDialog, $http) {
				$scope.ctrl = {
					dsrProcessConfig: dsrProcessConfig,
					processList : [],
					countryList : [],
					dspList : [],
					dspCountriesUtilizations : [],
					utilizationList : []
				};
				
				
				$http({
					method: 'GET',
					url: 'rest/dsrConfig/allProcess'
				}).then(function successCallback(response) {
					$scope.ctrl.processList = response.data;
				}, function errorCallback(response) {
					$scope.ctrl.processList = [ ];
				});
				
				
				
				$http({
					method: 'GET',
					url: 'rest/data/dspUtilizationsOffersCountries'
				}).then(function successCallback(response) {
					$scope.ctrl.dspCountriesUtilizations = response.data;
					
					$scope.ctrl.dspCountriesUtilizations.dsp.splice(0, 0, {idDsp: '*', name: '*'});
					$scope.ctrl.dspCountriesUtilizations.utilizations.splice(0, 0, {idUtilizationType: '*', name: '*'});
					$scope.ctrl.dspCountriesUtilizations.countries.splice(0, 0, {idCountry: '*', name: '*'});
					
				}, function errorCallback(response) {
					$scope.ctrl.dspCountriesUtilizations = [ ];
				});
				
				
				
				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				
				$scope.save = function() {
					$http({
						method: 'PUT',
						url: 'rest/dsrConfig',
						data: {
								dsp : $scope.dsp.idDsp,
								country : $scope.country.idCountry,
								utilizationType : $scope.utilizationType.idUtilizationType,
								idDsrProcess :	$scope.process,
								priority : $scope.rank		
								
								
						}
					}).then(function successCallback(response) {
						ngDialog.closeAll(true);
						$route.reload();
					}, function errorCallback(response) {
						if(response.status == 409){
							$scope.openErrorDialog("Configurazione già presente. Modifichi i valori direttamente dalla tabella");
							
						}else{
							$scope.openErrorDialog(response.statusText);	
						}
						
					});
				};
				
				$scope.filterUtilizationByDsp = function () {
				    return function (item) {
				    	if((typeof $scope.dsp === 'undefined'))
				    		return false;
				    	
				    	if($scope.dsp.idDsp === '*'){
				    		return true;
				    	}
				    	
				    	for (var i = 0; i < $scope.ctrl.dspCountriesUtilizations.commercialOffers.length; i++) { 
				    		  if (($scope.ctrl.dspCountriesUtilizations.commercialOffers[i].idDSP == $scope.dsp.idDsp &&
				    				  item.idUtilizationType ==$scope.ctrl.dspCountriesUtilizations.commercialOffers[i].idUtilizationType) || item.idUtilizationType == '*')
						        {   
						            return true;
						        }
				    	}

				    	 return false;
				      
				    };
				};
				
				$scope.openErrorDialog = function (errorMsg) {
			        ngDialog.open({ 
			        	template: 'errorDialogId',
			        	plain: false,
						width: '40%',
			        	data: errorMsg
			        	
			        });
			    };
				
			}]
		}).closePromise.then(function (data) {
			if (data.value === true) {
				// no op
			}
		});
	};

	$scope.showDeleteProcessConfig = function(event, dsrConfigToRemove) {
		ngDialog.open({
			template: 'pages/dsrprocessconfig/dialog-remove.html',
			plain: false,
			width: '60%',
			controller: ['$scope', 'ngDialog', '$http', function($scope, ngDialog, $http) {
				$scope.ctrl = {
					dsrConfigToRemove: dsrConfigToRemove
				};
				

				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				

				$scope.deleteDsrConfig = function() {
					$http({
						method: 'DELETE',
						url: 'rest/dsrConfig',
						headers: {
				            'Content-Type': 'application/json'},
						data: {
							priority : $scope.ctrl.dsrConfigToRemove.priority,
							dsp : $scope.ctrl.dsrConfigToRemove.dsp,
							country : $scope.ctrl.dsrConfigToRemove.country,
							utilizationType : $scope.ctrl.dsrConfigToRemove.utilizationType,
							idDsrProcess :	$scope.ctrl.dsrConfigToRemove.idDsrProcess
								
						}
					
					}).then(function successCallback(response) {
						ngDialog.closeAll(true);
						$route.reload();
					}, function errorCallback(response) {
						$scope.openErrorDialog(response.statusText);
					});
					
				};
				
			    $scope.openErrorDialog = function (errorMsg) {
			        ngDialog.open({ 
			        	template: 'errorDialogId',
			        	plain: false,
						width: '40%',
			        	data: errorMsg
			        	
			        });
			    };
			}]
		}).closePromise.then(function (data) {
			if (data.value === true) {
				// no op
			}
		});
	};
	
	//funzione che renderizza il popup per la modifica della configurazione del processo per DSR
	$scope.showEditProcessConfig = function(event, dsrProcessConfigToEdit) {
		ngDialog.open({
			template: 'pages/dsrprocessconfig/dialog-edit.html',
			plain: false,
			width: '60%',
			controller: ['$scope', 'ngDialog', '$http', function($scope, ngDialog, $http) {
				$scope.ctrl = {
					dsrProcessConfigToEdit: dsrProcessConfigToEdit,
					processList : []
				};
				
				
				$http({
					method: 'GET',
					url: 'rest/dsrConfig/allProcess'
				}).then(function successCallback(response) {
					$scope.ctrl.processList = response.data;
				}, function errorCallback(response) {
					$scope.ctrl.processList = [ ];
				});
				
				
				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				
				
				$scope.save = function() {
					
					$http({
						method: 'POST',
						url: 'rest/dsrConfig',
						headers: {
				            'Content-Type': 'application/json'},
						data: {
							dsp : $scope.ctrl.dsrProcessConfigToEdit.dsp,
							country : $scope.ctrl.dsrProcessConfigToEdit.country,
							utilizationType : $scope.ctrl.dsrProcessConfigToEdit.utilizationType,
							idDsrProcess :	$scope.process,
							priority : $scope.priority
						}
					
					}).then(function successCallback(response) {
						ngDialog.closeAll(true);
						$route.reload();
					}, function errorCallback(response) {
						$scope.openErrorDialog(response.statusText);
					});
					
				};
				
			    $scope.openErrorDialog = function (errorMsg) {
			        ngDialog.open({ 
			        	template: 'errorDialogId',
			        	plain: false,
						width: '40%',
			        	data: errorMsg
			        	
			        });
			    };
				
				
				
			}]
		}).closePromise.then(function (data) {
			if (data.value === true) {
				// no op
			}
		});
	};

	 $scope.$on('filterApply-DCU', function(event, parameters) { 
	
		 $scope.ctrl.filterParameters = parameters;
		 $location.search('execute', 'true')
			.search('hideForm', false)
			.search('first', 0)
			.search('last', $scope.ctrl.maxrows)
			.search('dsp', parameters.dsp)
			.search('country', parameters.country)
			.search('utilization', parameters.utilization);	 
			$scope.ctrl.hideForm = false;
		 
		 });
	
	$scope.onRefresh();
	
}]);

codmanApp.filter('rank', function() {
	  return function(input, min, max) {
		    min = parseInt(min); //Make string input int
		    max = parseInt(max);
		    for (var i=min; i<=max; i++)
		      input.push(i);
		    return input;
		  };
});  
