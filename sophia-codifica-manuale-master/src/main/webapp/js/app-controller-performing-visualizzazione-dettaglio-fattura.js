codmanApp.controller('VisualizzazioneDettaglioFatturaCtrl',['$scope', '$http', '$routeParams', '$location', '$routeParams', 'ngDialog', function($scope, $http, $routeParams, $location, $routeParams, ngDialog){
	
	$scope.user = angular.element("#headerLinksBig").text().trim();

	$scope.saveMegaConcerto = function(idEvento,flag,indice){
		
		if(flag != undefined && idEvento){
			var attivaDisattiva = "modificare"
			if(flag == true){
				var attivaDisattiva = "attivare"
			}else if(flag == false){
				var attivaDisattiva = "disattivare"
			}
			var model = $scope.showWarning("Attenzione","Sei sicuro di voler " + attivaDisattiva + " il megaconcerto per l'evento: " + idEvento + " ?");
			model.closePromise.then(function(data){
				if(data.value == true){
					$http({
						method: 'PUT',
						url: 'rest/performing/cruscotti/updateFlagMegaConcert',
						data: {
							dataFattura: $scope.sampling.dataFattura,
							manifestazione: {
								id: idEvento,
								flagMegaConcert: flag
							}
						},
						params: {
							userId: $scope.user
						}
					}).then(function(response) {
						$scope.showError("Success",response.data);
						$scope.init();
					},function(response){
						$scope.showError("Error",response.data);
						$scope.init();
					});
				}
				else{
					$scope.eventi[indice].manifestazione.flagMegaConcert = !$scope.eventi[indice].manifestazione.flagMegaConcert
				}
			})
		}
	}
	
	$scope.previstiRientratiPrincipaliSpalla = function(principali,spalla,voceIncasso){
		var stringa = "";
		
		if(voceIncasso == 2244){
			if(principali != undefined){
				stringa = stringa + principali + " (Principali) ";
			}
			if(spalla != undefined){
				stringa = stringa + spalla + " (Spalla)";
			}
			return stringa;
		}else{
			return principali;
		}
		
	}
	
	$scope.formatDate = function(date){
		if(date != null && date != undefined){
			var data = date.split("T")[0];
			var orario = date.split("T")[1];
			var anno = data.split("-")[0];
			var mese = data.split("-")[1];
			var giorno = data.split("-")[2];
			var ora = orario.split(":")[0];
			var minuti = orario.split(":")[1];
			return giorno + '-' + mese + '-' + anno + ' ' + ora + ':' + minuti;
		}
	}
	
	$scope.formatImporto = function(documento,importo,id){
		if(documento == 221){
			return '-' + importo;
		}else{
			return importo;
		}
	}
	
	var numeroFattura = $routeParams.numeroFattura;
	
	
	  $scope.exportFattura = function(numeroFattura){
		  $http({
			method: 'GET',
			responseType: 'blob',
			url: 'rest/performing/cruscotti/exportFatturaDetail',
			params: {
				numeroFattura: numeroFattura //controllare anche se vuole altri parametri
			}
		}).then( function(response) {

			var linkElement = document.createElement('a');

			try {
				var blob = new Blob([response.data], { type: "text/plain;charset=UTF-8" });
				saveAs(blob,'Fattura-'+numeroFattura+'.csv');

			} catch (ex) {
				$scope.showError("Errore","Impossibile fare il download dell'excel.");
			}
		}, function (response) {
			$scope.showError("Errore","Impossibile fare il download dell'excel.");
		});
	  }
	 
	$scope.init = function(){
		$http({
			method:'GET',
			url:'rest/performing/cruscotti/fattureDetail',
			params: {
				numeroFattura: numeroFattura
			}
		}).then(function(response){
			if(response.data.length > 0){
				$scope.showEventi = true;
			}
			$scope.sampling = response.data;
			$scope.eventi = response.data.eventiAssociati;
			$scope.riconciliazioneFattura = response.data.riconciliazioneFattura;
		},function(response){
			$scope.showError("","Non risulta nessun dato per la ricerca effettuata");
		})
	}

	$scope.riconciliaImporti = function(item){
		$http({
			method:'POST',
			url:'rest/performing/cruscotti/eventi/riconcilia-importi',
			data: item
		}).then(function(response){
			if(response.data.length > 0){
				$scope.showEventi = true;
			}
			$scope.sampling = response.data;
			$scope.eventi = response.data.eventiAssociati;
			$scope.riconciliazioneFattura = response.data.riconciliazioneFattura;
		},function(response){
			$scope.showError("","Operazione Non Riuscita!");
		});
	};

	$scope.riconciliaVociIncasso = function(item){
		$http({
			method:'POST',
			url:'rest/performing/cruscotti/eventi/riconcilia-voci-incasso',
			data: item
		}).then(function(response){
			if(response.data.length > 0){
				$scope.showEventi = true;
			}
			$scope.sampling = response.data;
			$scope.eventi = response.data.eventiAssociati;
			$scope.riconciliazioneFattura = response.data.riconciliazioneFattura;
		},function(response){
			$scope.showError("","Operazione Non Riuscita!");
		});
	};

	$scope.ripristinaRiconciliazione = function(item){
		$http({
			method:'POST',
			url:'rest/performing/cruscotti/eventi/ripristina-riconciliazione',
			data: item
		}).then(function(response){
			if(response.data.length > 0){
				$scope.showEventi = true;
			}
			$scope.sampling = response.data;
			$scope.eventi = response.data.eventiAssociati;
			$scope.riconciliazioneFattura = response.data.riconciliazioneFattura;
		},function(response){
			$scope.showError("","Operazione Non Riuscita!");
		});
	};
	
	
	$scope.goTo = function(url,param){
		if(url != undefined && param != undefined){
			$location.path(url + param);
		}
	}
	
	$scope.back = function(){
		 window.history.back();
	}
	
	$scope.showError = function(title, msgs) {
		ngDialog.open({
			template : 'pages/advance-payment/dialog-alert.html',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				$scope.ctrl = {
					title : title,
					message : msgs
				};
				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				$scope.save = function() {
					ngDialog.closeAll(true);
				};
			} ]
		}).closePromise.then(function(data) {
			// no op
		});
	};
	
	$scope.showWarning = function(title, msgs){
		return ngDialog.open({
			template : 'pages/advance-payment/dialog-warning.html',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				$scope.ctrl = {
					title : title,
					message : msgs
				};
				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				$scope.call = function() {
					ngDialog.closeAll(true);
				};
			} ]
		})
	};
	
}]);