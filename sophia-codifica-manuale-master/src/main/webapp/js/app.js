// (function(){
moment.locale('it');

var codmanApp = angular.module('codmanApp', ['ngAnimate', 'ngSanitize', 'ui.bootstrap', 'ui.dateTimeInput', 'ui.bootstrap.datetimepicker', 'ngDialog', 'ngLoadingSpinner', 'ngRoute', 'ngFileUpload', 'angularjs-dropdown-multiselect', 'angucomplete-alt', 'ui', 'ngMaterial', 'ngMessages', 'angularjs-gauge'
  ,'configurationsApp'])
  .provider('configService', function () {
    var options = {};
    this.config = function (opt) {
      angular.extend(options, opt);
    };
    this.$get = [function () {
      if (!options) {
        throw new Error('options must be configured');
      }
      return options;
    }];
  }).run(
    function ($templateCache) {
      $templateCache.put('templates/datetimepicker.html',
        '<div class="datetimepicker table-responsive">\n <table class="table table-condensed {{ data.currentView }}-view">\n <thead>\n <tr>\n <th class="left" data-ng-click="changeView(data.currentView, data.leftDate, $event)" data-ng-show="data.leftDate.selectable"><i class="glyphicon glyphicon-arrow-left"></i>\n </th>\n <th class="switch" colspan="5" data-ng-show="data.previousViewDate.selectable" data-ng-click="changeView(data.previousView, data.previousViewDate, $event)">{{ data.previousViewDate.display }}</th>\n <th class="right" data-ng-click="changeView(data.currentView, data.rightDate, $event)" data-ng-show="data.rightDate.selectable"><i class="pull-right glyphicon glyphicon-arrow-right"></i>\n </th>\n </tr>\n <tr>\n <th class="dow" data-ng-repeat="day in data.dayNames">{{ day }}</th>\n </tr>\n </thead>\n <tbody>\n <tr data-ng-if="data.currentView !== \'day\'">\n <td colspan="7">\n <span class="{{ data.currentView }}" data-ng-repeat="dateObject in data.dates" data-ng-class="{current: dateObject.current, active: dateObject.active, past: dateObject.past, future: dateObject.future, disabled: !dateObject.selectable}" data-ng-click="changeView(data.nextView, dateObject, $event)">{{ dateObject.display }}</span></td>\n </tr>\n <tr data-ng-if="data.currentView === \'day\'" data-ng-repeat="week in data.weeks">\n <td data-ng-repeat="dateObject in week.dates" data-ng-click="changeView(data.nextView, dateObject, $event)" class="day" data-ng-class="{current: dateObject.current, active: dateObject.active, past: dateObject.past, future: dateObject.future, disabled: !dateObject.selectable}">{{ dateObject.display }}</td>\n </tr>\n </tbody>\n </table>\n</div>\n');
    });


angular.element(document).ready(function () {
  $.get('rest/angularjs-config', function (configData) {
    //	    	logmsg(configData);
    angular.module('codmanApp').config(['configServiceProvider', function (configServiceProvider) {
      configServiceProvider.config(configData);
    }]);
    angular.bootstrap(document, ['codmanApp']);
  });
});

// due of https://github.com/angular-ui/ui-router/issues/2889
codmanApp.config(['$qProvider', function ($qProvider) {
  $qProvider.errorOnUnhandledRejections(false);
}]);

// Disable AJAX caching on IE
codmanApp.config(['$httpProvider',
  function ($httpProvider) {
    // initialize get if not there
    if (!$httpProvider.defaults.headers.get) {
      $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 15 May 1972 00:00:00 GMT';
    // extra
    $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
    $httpProvider.defaults.headers.get.Pragma = 'no-cache';
  }]);

codmanApp.filter('capitalize', function () {
  return function (input) {
    return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1) : '';
  };
});

codmanApp.service('scopeService', function() {
  return {
    safeApply: function ($scope, fn) {
      var phase = $scope.$root.$$phase;
      if (phase === '$apply' || phase === '$digest') {
        if (fn && typeof fn === 'function') {
          fn();
        }
      } else {
        $scope.$apply(fn);
      }
    },
  };
});

codmanApp.filter('urlEncode', [function () {
  return window.encodeURIComponent;
}]);

angular.module('codmanApp').filter('urlEncodeAngular', [ '$httpParamSerializer', 
  function ($httpParamSerializer) {
    return function (input) {
       return $httpParamSerializer(input);
    };
  }]);

codmanApp.factory('triplettaContext', function () {
  let context = {};
  context.empty = function () {
    context.state = {
      location: '',
      reset: function (scope) {
      }
    };
    context.sapData = {};
    context.tripletta = {};
    context.mode = '';
    context.history = [];
  };
  context.empty();
  return context;
});

codmanApp.factory('accantonaContext', function () {
  let context = {};
  return context;
});

codmanApp.factory('backclaimStoricoContext', function () {
  let context = {};
  return context;
});

codmanApp.factory('returnAdvancePayment', function () {
  let context = {};
  return context;
});

codmanApp.filter('datesDifference', function () {
  return function (input, date2) {
    if (!date2) {
      date2 = new Date();
    }
    return (new Date(input)).getTime() - date2.getTime();
  };
});

codmanApp.filter('secToTime', function () {
  return function (input) {
    var secNum = parseInt(input, 10); // don't forget the second param
    var hours = Math.floor(secNum / 3600);
    var minutes = Math.floor((secNum - (hours * 3600)) / 60);
    var seconds = secNum - (hours * 3600) - (minutes * 60);

    if (hours < 10) {
      hours = '0' + hours;
    }
    if (minutes < 10) {
      minutes = '0' + minutes;
    }
    if (seconds < 10) {
      seconds = '0' + seconds;
    }
    return hours + ':' + minutes + ':' + seconds;
  };
});

codmanApp.filter('parseLicences', function () {
  return function (input) {
    var licence = JSON.parse(input), resultRep = '';
    licence && licence.licences ? licence.licences.forEach(function (a, c) {
      var b = '';
      0 < c && (b = '\n ');
      resultRep += b + 'code: ' + a.code + ' da: ' + a.from + ' a: ' + a.to;
    }) : '';
    return resultRep;
  };
});

function DatePickerController(UtilService) {
  var vm = this;
  vm.randomId = UtilService.makeId(10);
  vm.updateValue = updateValue;

  function updateValue(value, format) {
    vm.onUpdate({value:value && 0 < value.trim().length && dayjs(value, format.toUpperCase()).isValid() ? dayjs.utc(value, format.toUpperCase()).toJSON() : void 0});
  }
}

DatePickerController.$inject = ['UtilService'];

var datepicker = {
  template:
    `<div class="input-append date" data-ng-attr-id="{{$ctrl.randomId}}" data-provide="datepicker" data-date-format="{{$ctrl.format}}" data-date-language="{{$ctrl.language}}" data-date-view-mode="{{$ctrl.viewMode}}" data-date-min-view-mode="{{$ctrl.minViewMode}}" data-date-orientation="{{$ctrl.orientation}}" data-date-autoclose="{{$ctrl.autoclose}}" data-date-end-date="{{$ctrl.endDate ? ($ctrl.endDate | date : $ctrl.format) : '0d'}}" data-date-start-date="{{($ctrl.startDate | date : $ctrl.format)}}"> <input data-ng-model="$ctrl.dateValue" data-ng-change="$ctrl.updateValue($ctrl.dateValue,$ctrl.format);" placeholder="{{$ctrl.placeholder ? $ctrl.placeholder : 'gg-mm-aaaa'}}" type="text" data-ng-attr-id="{{'date'+$ctrl.randomId}}" style="width: 125px; height: 23px"><span class="add-on" style="height: 23px"><i class="icon-th"></i></span></div>`,
  bindings: {
    placeholder: '@?',
    format: '@?',
    language: '@?',
    viewMode: '@?',
    minViewMode: '@?',
    orientation: '@?',
    autoclose: '@?',
    startDate: '@?',
    endDate: '@?',
    onUpdate: '&',
    dateValue: '='
  },
  controller: DatePickerController
};

codmanApp.component('datepicker', datepicker);

codmanApp.directive('countdown', ['$interval', 'dateFilter', '$filter', 'UtilService', 'ngDialog',
  function ($interval, dateFilter, $filter, UtilService, ngDialog) {
    // return the directive link function. (compile function not needed)
    return function (scope, element, attrs) {
      var data;  // date format
      var stopTime; // so that we can cancel the time updates

      // used to update the UI
      function updateTime() {
        if ($filter('datesDifference')(data) <= 0) {
          $interval.cancel(stopTime);
          UtilService.showAlert('Attenzione!', 'Sessione di codifica scaduta')
            .closePromise.then(function (response) {
            ngDialog.closeAll();
          });
          return;
        }
        element.text(dateFilter($filter('datesDifference')(data), 'mm:ss'));
      }

      // watch the expression, and update the UI on change.
      scope.$watch(attrs.countdown, function (value) {
        data = value;
        updateTime();
      });

      stopTime = $interval(updateTime, 1000);

      // listen on DOM destroy (removal) event, and cancel the next UI update
      // to prevent updating time after the DOM element was removed.
      element.on('$destroy', function () {
        $interval.cancel(stopTime);
      });
    };
  }]);

codmanApp.directive('isLocked', ['$interval', 'dateFilter', '$filter',
  function ($interval, dateFilter, $filter) {
    // return the directive link function. (compile function not needed)
    return function (scope, element, attrs) {
      var data;  // date format
      var stopTime; // so that we can cancel the time updates
      //elemet.nodeName === a && element.css('display','none');

      // used to update the UI
      function updateTime() {
        if ($filter('datesDifference')(data) <= 0) {
          $interval.cancel(stopTime);
          element.remove();
          return;
        }
      }

      // watch the expression, and update the UI on change.
      scope.$watch(attrs.isLocked, function (value) {
        data = value;
        updateTime();
      });

      stopTime = $interval(updateTime, 1000);

      // listen on DOM destroy (removal) event, and cancel the next UI update
      // to prevent updating time after the DOM element was removed.
      element.on('$destroy', function () {
        $interval.cancel(stopTime);
      });
    };
  }]);

codmanApp.directive('datepicker', function () {
  return {
    restrict: 'A',
    controller: function (scope, element, attrs) {
      element.datepicker({
        language: 'it',
        format: 'mm-yyyy',
        viewMode: 'months',
        minViewMode: 'months',
        orientation: 'bottom auto',
        autoclose: 'true',
        startDate: scope.ctrl.item.periodTo,
        onSelect: function (dateText) {
          var modelPath = $(this).attr('ng-model');
          putObject(modelPath, scope, dateText);
          scope.$apply();
        }
      }).datepicker('update', scope.ctrl.item.periodTo);
    }
  };
});

codmanApp.filter('range', function () {
  return function (input, min, max) {
    min = parseInt(min); // Make string input int
    max = parseInt(max);
    for (var i = min; i <= max; i++) {
      input.push(i);
    }
    return input;
  };
});

codmanApp.filter('month_range', function () {
  return function (input, min, max) {
    min = parseInt(min); // Make string input int
    max = parseInt(max);
    for (var i = min; i <= max; i++) {
      input.push(i + 'M');
    }
    return input;
  };
});

codmanApp.config(config);
config.$inject = ['$routeProvider'];

function config($routeProvider) {
  $routeProvider.when('/home', {
    templateUrl: 'pages/home.jsp',
    controller: 'HomeCtrl'
  }).when('/riconoscimento', {
    templateUrl: 'pages/riconoscimento/riconoscimento.jsp',
    controller: 'RiconoscimentoCtrl',
    resolve: {
      preRiconoscimentoService: preRiconoscimentoService
    }
  }).when('/scodifica-ricodifica', {
    templateUrl: 'pages/riconoscimento/scodifica-ricodifica/scodifica-ricodifica.jsp',
    controller: 'scodifica-ricodificaCtrl'
  }).when('/knowledgeBase', {
    templateUrl: 'pages/knowledge-base/ricerca.jsp',
    controller: 'KnowledgeBaseCtrl'
  }).when('/droolsPricing', {
    templateUrl: 'pages/drools/dtables.jsp',
    controller: 'DroolsPricingCtrl'
  }).when('/blacklist', {
    templateUrl: 'pages/blacklist/blacklist.jsp',
    controller: 'BlacklistCtrl',
    resolve: {
      societaTutelaList: function ($q, societaTutelaService) {
          var deferred = $q.defer();
          societaTutelaService.getAll().then(deferred.resolve, deferred.reject);
          return deferred.promise;
      }
    }    
  }).when('/configurazione-blacklist', {
    templateUrl: 'pages/blacklist/configurazione/configurazione-blacklist.jsp',
    controller: 'ConfigurazioneBlacklistCtrl' 
  }).when('/configurazione-pricing', {
    templateUrl: 'pages/configurazione-pricing/configurazione-pricing.jsp',
    controller: 'ConfigurazionePricingCtrl' 
  }).when('/dashboard-dsp', {
    templateUrl: 'pages/dashboard-dsp/dashboard-dsp.jsp',
    controller: 'DashboardDspCtrl' 
  }).when('/configurazione-dashboard-dsp', {
    templateUrl: 'pages/dashboard-dsp/configurazione/configurazione-dashboard-dsp.jsp',
    controller: 'ConfigurazioneDashboardDspCtrl' 
  }).when('/dsrConfig', {
    templateUrl: 'pages/dsrprocessconfig/dsrProcessConfig.jsp',
    controller: 'DsrProcessConfigCtrl'
  }).when('/mandatoConfig', {
    templateUrl: 'pages/mandato-config/mandatoConfig.jsp',
    controller: 'MandatoConfigCtrl',
    resolve: {
      societaTutela: function ($q, societaTutelaService) {
        var deferred = $q.defer();
        societaTutelaService.getAll().then(deferred.resolve, deferred.reject);
        return deferred.promise;
      },
      mandati: function ($q, mandatiService) {
        var deferred = $q.defer();
        mandatiService.getAll().then(deferred.resolve, deferred.reject);
        return deferred.promise;
      },
      configurazioniMandato: function ($q, configurazioneMandatoService) {
        var deferred = $q.defer();
        configurazioneMandatoService.getAll().then(deferred.resolve, deferred.reject);
        return deferred.promise;
      }      
    }    
  }).when('/documentazioneMandanti', {
    templateUrl: 'pages/mandato-config/documentazione-mandanti.jsp',
    controller: 'documentazioneMandantiCtrl',
    resolve: {
      societaTutela: function ($q, societaTutelaService) {
          var deferred = $q.defer();
          societaTutelaService.getAll().then(deferred.resolve, deferred.reject);
          return deferred.promise;
      }     
    }    
  }).when('/preparatoryActivities', {
    templateUrl: 'pages/preparatory-activities/preparatory-activities.jsp',
    controller: 'preparatoryActivitiesCtrl',
    resolve: {
      processesSearchResponse: function ($q, preparatoryActivitiesService) {
        var deferred = $q.defer();
        preparatoryActivitiesService.searchProcesses().then(deferred.resolve, deferred.reject);
        return deferred.promise;
      }
    }     
  }).when('/royaltiesSearch', {
    templateUrl: 'pages/royalties-search/royalties-search.jsp',
    controller: 'royaltiesSearchCtrl',
    resolve: {
      royaltiesStorages: function ($q, royaltiesSearchService) {
        var deferred = $q.defer();
        royaltiesSearchService.listRoyaltiesStorages().then(deferred.resolve, deferred.reject);
        return deferred.promise;
      },
      dspsUtilizationsCommercialOffersCountries: function (anagClientService) {
        return anagClientService.getDspsUtilizationsCommercialOffersCountries();
      },
      activeRoyaltiesStorage: function (royaltiesSearchService) {
        return royaltiesSearchService.activeRoyaltiesStorage()
      }
    }
  }).when('/unidentifiedLoading', {
    templateUrl: 'pages/unidentified-loading/unidentified-loading.jsp',
    controller: 'unidentifiedLoadingCtrl',
    resolve: {
      dspsUtilizationsCommercialOffersCountries: function (anagClientService) {
        return anagClientService.getDspsUtilizationsCommercialOffersCountries();
      },
      /* defaultSearchResponse: function ($q, unidentifiedLoadingService) {
        var deferred = $q.defer();
        unidentifiedLoadingService.search().then(deferred.resolve, deferred.reject);
        return deferred.promise;
      }, */
      statuses: function ($q, unidentifiedLoadingService) {
        var deferred = $q.defer();
        unidentifiedLoadingService.statuses().then(deferred.resolve, deferred.reject);
        return deferred.promise;
      }
    }  
  }).when('/mailAlerts', {
    templateUrl: 'pages/mail-alerts/mail-alerts.jsp',
    controller: 'mailAlertsCtrl',
    resolve: {
      dspsUtilizationsCommercialOffersCountries: function (anagClientService) {
        return anagClientService.getDspsUtilizationsCommercialOffersCountries();
      },
      alerts: function ($q, mailAlertsService) {
        var deferred = $q.defer();
        mailAlertsService.getAlerts().then(deferred.resolve, deferred.reject);
        return deferred.promise;
      }
    }  
  }).when('/configurazioneAggiTrattenuta', {
    templateUrl: 'pages/ripartizione/configurazione-aggi-trattenuta.jsp',
    controller: 'confAggiTrattenutaCtrl',
    resolve: {
      societaTutela: function ($q, societaTutelaService) {
          var deferred = $q.defer();
          societaTutelaService.getAll().then(deferred.resolve, deferred.reject);
          return deferred.promise;
      }      
    }   
  }).when('/produzioneCarichi', {
    templateUrl: 'pages/ripartizione/produzione-carichi.jsp',
    controller: 'produzioneCarichiCtrl',
    resolve: {
      societaTutela: function ($q, societaTutelaService) {
        var deferred = $q.defer();
        societaTutelaService.getAll().then(deferred.resolve, deferred.reject);
        return deferred.promise;
      },
      anagClientData: function (anagClientService) {
        return anagClientService.getClientsData();
      },
      dspsUtilizationsCommercialOffersCountries: function (anagClientService) {
        return anagClientService.getDspsUtilizationsCommercialOffersCountries();
      }
    }    
  }).when('/statistichePostRipartizione', {
    templateUrl: 'pages/ripartizione/statistiche-post-ripartizione.jsp',
    controller: 'statistichePostRipartizioneCtrl'    
  }).when('/rendiconto', {
    templateUrl: 'pages/ripartizione/rendiconto/rendiconto.jsp',
    controller: 'rendicontoCtrl',    
  }).when('/lookThrough', {
    templateUrl: 'pages/ripartizione/lookThrough/lookThrough.jsp',
    controller: 'lookThroughCtrl',    
  }).when('/listaCarichi', {
    templateUrl: 'pages/ripartizione/lista-carichi.jsp',
    controller: 'listaCarichiCtrl',    
  }).when('/dspConfig', {
    templateUrl: 'pages/dspconfig/dspConfig.jsp',
    controller: 'DspConfigCtrl'
  }).when('/ricerca', {
    templateUrl: 'pages/ricerca/ricerca.jsp',
    controller: 'RicercaCtrl'
  }).when('/ricercaCodificato', {
    templateUrl: 'pages/ricerca-codificato/ricerca-codificato.jsp',
    controller: 'ricercaCodificatoCtrl'
  }).when('/commercialOffersConfig', {
    templateUrl: 'pages/commercial-offer-config/commercialOfferConfig.jsp',
    controller: 'CommercialOffersConfigCtrl'
  }).when('/processManagement', {
    templateUrl: 'pages/process-management/processManagement.jsp',
    controller: 'ProcessManagementCtrl'
  }).when('/anagDspConfig', {
    templateUrl: 'pages/anag-dsp-config/anagDspConfig.jsp',
    controller: 'AnagDspCtrl'
  }).when('/dsrMetadata', {
    templateUrl: 'pages/dsr-metadata/dsrMetadataConfig.jsp',
    controller: 'DsrMetadataCtrl'
  }).when('/ccidConfig', {
    templateUrl: 'pages/ccid-config/ccidConfig.jsp',
    controller: 'CCIDConfigCtrl'
  }).when('/dspStatistics', {
    templateUrl: 'app/multimediale/statistiche/dspStatistics.jsp',
    controller: 'DspStatisticsCtrl',
    resolve: {
      dspsUtilizationsCommercialOffersCountries: function (anagClientService) {
        return anagClientService.getDspsUtilizationsCommercialOffersCountries();
      }
    }
  }).when('/sqsFailedStats', {
    templateUrl: 'pages/sqs-failed/stats.jsp',
    controller: 'SqsFailedStatsCtrl'
  }).when('/sqsFailedSearch', {
    templateUrl: 'pages/sqs-failed/search.jsp',
    controller: 'SqsFailedSearchCtrl'
  }).when('/serviceBusStats', {
    templateUrl: 'pages/service-bus/stats.jsp',
    controller: 'ServiceBusStatsCtrl'
  }).when('/serviceBusSearch', {
    templateUrl: 'pages/service-bus/search.jsp',
    controller: 'ServiceBusSearchCtrl'
  }).when('/deliverCcid', {
    templateUrl: 'pages/deliver-ccid/deliver-ccid.jsp',
    controller: 'DeliverCcidCtrl',
    resolve: {
   //   anagClientData: function (anagClientService) {
   //     return anagClientService.getClientsData();
   //   },
      dspsUtilizationsCommercialOffersCountries: function (anagClientService) {
        return anagClientService.getDspsUtilizationsCommercialOffersCountries();
      }
    }
  }).when('/configureDeliverCcid', {
    templateUrl: 'pages/deliver-ccid/configure-deliver-ccid.jsp',
    controller: 'ConfigureDeliverCcidCtrl',
    resolve: {
      dspsUtilizationsCommercialOffersCountries: function (anagClientService) {
        return anagClientService.getDspsUtilizationsCommercialOffersCountries();
      }
    }
  }).when('/createInvoice', {
    templateUrl: 'pages/invoice/create-invoice.jsp',
    controller: 'CreateInvoiceCtrl',
    resolve: {
      anagClientData: function (anagClientService) {
        return anagClientService.getClientsData();
      },
      dspsUtilizationsCommercialOffersCountries: function (anagClientService) {
        return anagClientService.getDspsUtilizationsCommercialOffersCountries();
      }
    }
  }).when('/advancePayment', {
    templateUrl: 'pages/advance-payment/advance-payment.jsp',
    controller: 'AdvancePaymentCtrl',
    resolve: {
      anagClientData: function (anagClientService) {
        return anagClientService.getClientsData();
      },
      maxRows: function (InvoiceService) {
        return InvoiceService.getMaxRows();
      },
      dspsUtilizationsCommercialOffersCountries: function (anagClientService) {
        return anagClientService.getDspsUtilizationsCommercialOffersCountries();
      }
    }
  }).when('/accantonamentoGestioneReclami', {
    templateUrl: 'pages/advance-payment/accantonamento-gestione-reclami.jsp',
    controller: 'accantonamentoGestioneReclamiCtrl',
    resolve: {
      anagClientData: function (anagClientService) {
        return anagClientService.getClientsData();
      },
      dspsUtilizationsCommercialOffersCountries: function (anagClientService) {
        return anagClientService.getDspsUtilizationsCommercialOffersCountries();
      }
    }
  }).when('/reclamiUploadFile', {
    templateUrl: 'pages/gestione-reclami/reclami-upload-file.jsp',
    controller: 'reclamiUploadFileCtrl',
    resolve: {
      anagClientData: function (anagClientService) {
        return anagClientService.getClientsData();
      }
    }
  }).when('/marketShare', {
    templateUrl: 'pages/market-share/market-share.jsp',
    controller: 'marketShareCtrl',
    resolve: {
      anagClientData: function (anagClientService) {
        return anagClientService.getClientsData();
      },
      dspsUtilizationsCommercialOffersCountries: function (anagClientService) {
        return anagClientService.getDspsUtilizationsCommercialOffersCountries();
      }
    }
  }).when('/updateCcid', {
    templateUrl: 'pages/advance-payment/update-ccid.jsp',
    controller: 'UpdateCcidCtrl',
    resolve: {
      anagClientData: function (anagClientService) {
        return anagClientService.getClientsData();
      },
      dspsUtilizationsCommercialOffersCountries: function (anagClientService) {
        return anagClientService.getDspsUtilizationsCommercialOffersCountries();
      }
    }
  }).when('/invoices', {
    templateUrl: 'pages/invoice/search-invoice.jsp',
    controller: 'SearchInvoiceCtrl'
  }).when('/dsrStepsMonitoringSearch', {
    templateUrl: 'pages/dsr-steps-monitoring/search.jsp',
    controller: 'DsrStepsMonitoringCtrl',
    resolve: {
      // anagClientData: function (anagClientService) {
      //   return anagClientService.getClientsData();
      // },
      dspsUtilizationsCommercialOffersCountries: function (anagClientService) {
        return anagClientService.getDspsUtilizationsCommercialOffersCountries();
      }
    }
  }).when('/configurazione-char', {
    templateUrl: 'pages/configurazione-caretteri/configurazione-caratteri.jsp',
    controller: 'ConfigurazioneCharCtrl' 
  }).when('/dsrProgressSearch', {
    templateUrl: 'pages/dsr-progress/search.jsp',
    controller: 'DsrProgressCtrl',
    resolve: {
      // anagClientData: function (anagClientService) {
      //   return anagClientService.getClientsData();
      // },
      dspsUtilizationsCommercialOffersCountries: function (anagClientService) {
        return anagClientService.getDspsUtilizationsCommercialOffersCountries();
      }
    }
  }).when('/controlloClaim', {
    templateUrl: 'pages/dsr-claim/search.jsp',
    controller: 'DsrClaimCtrl',
    resolve: {
      // anagClientData: function (anagClientService) {
      //   return anagClientService.getClientsData();
      // },
      dspsUtilizationsCommercialOffersCountries: function (anagClientService) {
        return anagClientService.getDspsUtilizationsCommercialOffersCountries();
      }
    }
  })
  .when('/sqsHostnames', {
    templateUrl: 'pages/dsr-progress/hostnames.jsp',
    controller: 'SqsHostnamesCtrl'
  }).when('/anagClient', {
    templateUrl: 'pages/anag-client/anagClient.jsp',
    controller: 'AnagClientCtrl',
    resolve: {
      anagClientData: function (anagClientService) {
        return anagClientService.getClientsData();
      }
    }
  }).when('/multimediale/backclaim', {
    templateUrl: 'app/multimediale/backclaim/backclaim.jsp',
    controller: 'BackclaimCtrl',
    controllerAs: 'vm',
    resolve: {
      periodLimits: function (anagClientService) {
        return anagClientService.getPeriodLimits();
      },
      dspCountries: function (anagClientService) {
        return anagClientService.getDspCountries();
      },
      commercialOffers: function (anagClientService) {
        return anagClientService.getCommercialOffers();
      }
    }
  }).when('/gestioneConflitti', {
    templateUrl: 'pages/gestione-conflitti/gestione-conflitti.jsp',
    controller: 'gestioneConflittiCtrl',    
  }).when('/evaluate-channel', {
    templateUrl: 'pages/broadcasting/gestioneUtilizzazioni/evaluate-channel.jsp',
    controller: 'EvaluateChannelCtrl'
  }).when('/news', {
    templateUrl: 'pages/broadcasting/news/news.jsp',
    controller: 'NewsCtrl'
  }).when('/utenti', {
    templateUrl: 'pages/broadcasting/utenti/utenti.jsp',
    controller: 'UtentiCtrl'
  }).when('/alterEgo', {
    templateUrl: 'pages/broadcasting/alter-ego/alter-ego.jsp',
    controller: 'AlterEgoCtrl'
  }).when('/broadcasting-report', {
    templateUrl: 'pages/broadcasting/broadcasting-report/broadcasting-report.jsp',
    controller: 'BroadcastingReportCtrl'
  }).when('/configSampling', {
    templateUrl: 'pages/performing/campionamento/sampling-config/sampling-config.jsp',
    controller: 'PerformingConfigSamplingCtrl'
  }).when('/samplingHistory', {
    templateUrl: 'pages/performing/campionamento/sampling-history/sampling-history.jsp',
    controller: 'PerformingSamplingHistoryCtrl'
  }).when('/samplingExecution', {
    templateUrl: 'pages/performing/campionamento/sampling-execution/sampling-execution.jsp',
    controller: 'PerformingSamplingExecutionCtrl'
  }).when('/executionHistory', {
    templateUrl: 'pages/performing/campionamento/execution-history/execution-history.jsp',
    controller: 'PerformingExecutionHistoryCtrl'
  }).when('/samplingGuide', {
    templateUrl: 'pages/performing/campionamento/sampling-guide/sampling-guide.jsp',
    controller: 'PerformingSamplingGuideCtrl'
  }).when('/visualizzazioneEventi', {
    templateUrl: 'pages/performing/cruscotti/VisualizzazioneEventi/visualizzazioneEventi.jsp',
    controller: 'VisualizzazioneEventiCtrl'
  }).when('/eventoDetail/:idEvento', {
    templateUrl: 'pages/performing/cruscotti/VisualizzazioneEventi/VisualizzazioneDettaglioEvento/visualizzazioneDettaglioEvento.jsp',
    controller: 'VisualizzazioneDettaglioEventoCtrl'
  }).when('/visualizzazioneMovimenti', {
    templateUrl: 'pages/performing/cruscotti/VisualizzazioneMovimenti/visualizzazioneMovimenti.jsp',
    controller: 'VisualizzazioneMovimentiCtrl'
  }).when('/movimentoDetail/:idMovimento', {
    templateUrl: 'pages/performing/cruscotti/VisualizzazioneMovimenti/VisualizzazioneDettaglioMovimento/visualizzazioneDettaglioMovimento.jsp',
    controller: 'VisualizzazioneDettaglioMovimentoCtrl'
  }).when('/pmDetail/:idProgrammaMusicale', {
    templateUrl: 'pages/performing/cruscotti/VisualizzazionePM/VisualizzazioneDettaglioPM/pmDetail.jsp',
    controller: 'VisualizzazionePMCtrl'
    /* resolve: {
            pmDetail: function (programmaMusicaleService) {
                return programmaMusicaleService.getPMDetail(idProgrammaMusicale);
            }
        }*/
  }).when('/pmDetail/:idProgrammaMusicale/:idMovimento', {
    templateUrl: 'pages/performing/cruscotti/VisualizzazionePM/VisualizzazioneDettaglioPM/pmDetail.jsp',
    controller: 'VisualizzazionePMCtrl'
  }).when('/tracciamentoPM', {
    templateUrl: 'pages/performing/cruscotti/VisualizzazionePM/tracciamentoPM.jsp',
    controller: 'VisualizzazioneTracciamentoPMCtrl'
  }).when('/fatturaDetail/:numeroFattura', {
    templateUrl: 'pages/performing/cruscotti/VisualizzazioneFatture/VisualizzazioneDettaglioFattura/visualizzazioneDettaglioFattura.jsp',
    controller: 'VisualizzazioneDettaglioFatturaCtrl'
  }).when('/fattureList', {
    templateUrl: 'pages/performing/cruscotti/VisualizzazioneFatture/visualizzazioneFatture.jsp',
    controller: 'VisualizzazioneFattureCtrl'
  }).// aggiungere altra redirect
  when('/carichi-ripartizione', {
    templateUrl: 'pages/broadcasting/carichi-ripartizione/carichi-ripartizione.jsp',
    controller: 'CarichiRipartoCtrl'
  }).when('/preallocazione-incassi', {
    templateUrl: 'pages/broadcasting/preallocazione-incassi/preallocazione-incassi.jsp',
    controller: 'PreallocazioneIncassiCtrl'
  }).when('/multimedialeLocale', {
    templateUrl: 'pages/multimedialelocale/multimedialelocale.jsp',
    controller: 'MultimedialeLocaleCtrl'
  }).when('/multimedialeLocaleRegolare', {
    templateUrl: 'pages/multimedialelocale/multimedialelocale-regolare.jsp',
    controller: 'MultimedialeLocaleRegolareCtrl'
  }).when('/movimentoDetail=:contabilita', {
    templateUrl: 'pages/performing/cruscotti/VisualizzazioneMovimenti/VisualizzazioneDettaglioMovimento/visualizzazioneDettaglioMovimento.jsp',
    controller: 'VisualizzazioneDettaglioMovimentoCtrl'
  }).when('/pmDetail=:idProgrammaMusicale', {
    templateUrl: 'pages/performing/cruscotti/pmDetail.jsp',
    controller: 'VisualizzazionePMCtrl'
  }).when('/riconciliazioneImporti', {
    templateUrl: 'pages/performing/riconciliazioneImporti/riconciliazioneImporti.jsp',
    controller: 'VisualizzazioneRiconciliazioneImportiCtrl',
    resolve: {
      preEventiPagatiService: preEventiPagatiService
    }
  }).when('/gestioneMegaconcerti', {
    templateUrl: 'pages/performing/cruscotti/GestioneMegaconcerti/GestioneMegaconcerti.jsp',
    controller: 'VisualizzazioneGestioneMegaconcertiCtrl'
  }).when('/gestioneRipartizione', {
    templateUrl: 'pages/performing/cruscotti/GestioneRipartizione/GestioneRipartizione.jsp',
    controller: 'VisualizzazioneGestioneRipartizioneCtrl'
  }).when('/codificaManualeEsperto', {
    templateUrl: 'pages/performing/codifica/CodificaManuale/CodificaManualeEsperto.jsp',
    controller: 'PerformingCodificaManualeEspertoCtrl'
  }).when('/codificaManualeBase', {
    templateUrl: 'pages/performing/codifica/CodificaManuale/CodificaManualeBase.jsp',
    controller: 'PerformingCodificaManualeBaseCtrl'
  }).when('/configurazioniSoglie', {
    templateUrl: 'pages/performing/codifica/ConfigurazioniSoglie/ConfigurazioniSoglie.jsp',
    controller: 'PerformingConfigurazioniSoglieCtrl'
  }).when('/carichiRipartizione', {
    templateUrl: 'pages/performing/cruscotti/CarichiRipartizione/CarichiRipartizione.jsp',
    controller: 'CarichiRipartizioneCtrl'
  }).when('/regoleRipartizione', {
    templateUrl: 'pages/performing/cruscotti/RegoleRipartizione/RegoleRipartizione.jsp',
    controller: 'RegoleRipartizioneCtrl'
  }).when('/aggiornamentoDatiSun', {
    templateUrl: 'pages/performing/aggiornamentodatisun/aggiornamentoDatiSun.jsp',
    controller: 'AggiornamentoSunController',
    controllerAs: 'vm'
  }).when('/KPICodifica', {
    templateUrl: 'pages/performing/codifica/KPICodifica/KPICodifica.jsp',
    controller: 'KPIController',
    controllerAs: 'vm'
  }).when('/MonitoringOperatoriCodifica', {
    templateUrl: 'pages/performing/codifica/MonitoringOperatoriCodifica/MonitoringOperatoriCodifica.jsp',
    controller: 'MonitoringOperatoriCodificaCtrl'
  }).when('/monitoraggioCodificato', {
    templateUrl: 'pages/performing/codifica/MonitoraggioCodificato/MonitoraggioCodificato.jsp',
    controller: 'PerformingMonitoraggioCodificatoCtrl',
    controllerAs: 'vm'
  }).when('/performing/codifica/scodifica', {
    templateUrl: 'pages/performing/codifica/scodifica/modificaCodiceOpera.jsp',
    controller: 'PerformingScodificaCtrl',
    controllerAs: 'vm'
  }).when('/gestioneMusicProviders', {
    templateUrl: 'pages/performing/radio-in-store/gestione-music-providers.jsp',
    controller: 'GestionePalinsestiCtrl'
  }).when('/monitoraggioPalinsesti', {
    templateUrl: 'pages/performing/radio-in-store/monitoraggioPalinsesti.jsp',
    controller: 'MonitoraggioPalinsestiCtrl'
  }).when('/monitoraggioIngestion', {
    templateUrl: 'pages/ingestion/monitoraggioIngestion.jsp',
    controller: 'MonitoraggioIngestionCtrl'
  }).when('/ingestionDetail/:idIngestion', {
    templateUrl: 'pages/ingestion/VisualizzazioneDettaglioIngestion/visualizzazioneDettaglioIngestion.jsp',
    controller: 'VisualizzazioneDettaglioIngestionCtrl'
  }).when('/anagraficaTriplette', {
    templateUrl: 'pages/triplette/visualizzazioneTriplette.jsp',
    controller: 'VisualizzaTripletteCtrl'
  }).when('/gestioneTripletta', {
    templateUrl: 'pages/triplette/modifica/gestioneTripletta.jsp',
    controller: 'gestioneTriplettaCtrl'
  }).when('/RicProposte', {
    templateUrl: 'pages/performing/codifica/RicercaProposte/Ricerche.jsp',
    controller: 'RicProposteCtrl'
  }).when('/ConsoleCSV', {
    templateUrl: 'pages/performing/codifica/RicercaProposte/ConsoleCSV.jsp',
    controller: 'ConsoleCSVCtrl'
  }).when('/confImportiRiconciliati', {
    templateUrl: 'pages/performing/acquisizioneNDM/confImportiRiconciliati.jsp',
    controller: 'confImportiRiconciliatiCtrl'
  }).when('/dspStatistics/storico/:idDsr', {
    templateUrl: 'app/multimediale/statistiche/storico/storico.jsp',
    controller: 'storicoStatisticheCtrl',
    resolve: {
      storico: function ($route, MultimedialeService) {
        return MultimedialeService.getStoricoStatistiche($route.current.params.idDsr);
      }
    }
  }).when('/lavorazioneDSR', {
    templateUrl: 'pages/lavorazione-dsr/lavorazioneDSR.jsp',
    controller: 'LavorazioneDSRCtrl',
    resolve: {
      dspsUtilizationsCommercialOffersCountries: function (anagClientService) {
        return anagClientService.getDspsUtilizationsCommercialOffersCountries();
      },
      searchResponse: function ($q, lavorazioneDsrService) {
        var deferred = $q.defer();
        lavorazioneDsrService.search().then(deferred.resolve, deferred.reject);
        return deferred.promise;
      },
      dsrProcessingStatuses: function ($q, lavorazioneDsrService) {
        var deferred = $q.defer();
        lavorazioneDsrService.statuses().then(deferred.resolve, deferred.reject);
        return deferred.promise;
      }
    }       
  }).when('/codificato', {
    templateUrl: 'pages/ingestion/codificato/codificato.jsp',
    controller: 'CodificatoCtrl' 
  }).
  // default
  otherwise({
    redirectTo: '/home'
  });
}

function preEventiPagatiService(eventiPagatiService) {
  return eventiPagatiService.getSearchConfig();
}

preEventiPagatiService.$inject = ['eventiPagatiService'];

function preRiconoscimentoService(riconoscimentoService, $route) {
  return riconoscimentoService.getOpera(
    $route.current.params ?
      $route.current.params.hashId :
      undefined,
    $route.current.params.user);
}

preRiconoscimentoService.$inject = ['riconoscimentoService', '$route'];