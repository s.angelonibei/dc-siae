/**
 * AdvancePaymentCtrl
 *
 * @path /dspConfig
 */
codmanApp.service('factoryCcidCollector', function () {
  var productList = [];
  var addProduct = function (newObj) {
    productList.push(newObj);
  };

  var getProducts = function () {
    return productList;
  };

  return {
    addProduct: addProduct,
    getProducts: getProducts
  };
});

codmanApp.controller('AdvancePaymentCtrl', ['$scope', 'ngDialog', '$routeParams', '$location', '$http', 'configService', 'anagClientData', 'anagClientService', 'UtilService', 'accantonaContext', 'returnAdvancePayment', '$window', 'dspsUtilizationsCommercialOffersCountries',
  function ($scope, ngDialog, $routeParams, $location, $http, configService, anagClientData, anagClientService, UtilService, accantonaContext, returnAdvancePayment, $window, dspsUtilizationsCommercialOffersCountries) {
    $scope.CONSUMA = 'consuma';
    $scope.RIPROPORZIONA = 'riproporziona';
    $scope.CHIUDI = 'chiudi';

    $scope.dspsUtilizationsCommercialOffersCountries = angular.copy(dspsUtilizationsCommercialOffersCountries);


    $scope.asc = true;

    $scope.filterParameters = $routeParams;

    $scope.anagClientData = angular.copy(anagClientData);
    $scope.ctrl = {
      navbarTab: 'advancePayment',
      data: [],
      filterParameters: [],
      execute: $routeParams.execute,
      sortType: 'priority',
      sortReverse: true,
      filter: $routeParams.filter,
      advancePayment: null,
      selectedItems: [],
      items: [],
      msInvoiceApiUrl: `${$location.protocol()}://${$location.host()}:${$location.port()}/${configService.msInvoiceApiPath}`
    };

    $scope.filterDsp = anagClientService.filterDsp;

    $scope.selectedItemsEqualNumberOfRows = selectedItemsEqualNumberOfRows;

    $scope.navigateToNextPage = function () {
      // $scope.ctrl.first = $scope.ctrl.last + 1;
      // $scope.ctrl.last += $scope.ctrl.maxrows;
      $scope.getPagedResults(
        $scope.ctrl.currentPage + 1,
        $scope.ctrl.filterParameters
      );
    };

    $scope.navigateToPreviousPage = function () {
      $scope.getPagedResults(
        $scope.ctrl.currentPage - 1,
        $scope.ctrl.filterParameters
      );
    };

    // $scope.hasNext = function () {
    //     return $scope.ctrl.data.hasNext;
    // };
    //
    // $scope.hasPrev = function () {
    //     return $scope.ctrl.data.hasPrev;
    // };

    // $scope.getFirst = function () {
    //     if ($scope.ctrl.first === 0) {
    //         return $scope.ctrl.first + 1;
    //     }
    //     return $scope.ctrl.first;
    // };

    function selectedItemsEqualNumberOfRows() {
      return $scope.ctrl.selectedItems &&
        $scope.ctrl.dataCcid &&
        $scope.ctrl.dataCcid.rows &&
        Object.values($scope.ctrl.selectedItems).length === $scope.ctrl.dataCcid.rows.length;
    }

    function uncheckAllSelectedAndGetCcid(page) {
      // if($scope.allSelected)
      $scope.allSelected = false;
      $scope.getCcid(page, $scope.ctrl.filterParameters);
    }

    $scope.navigateToNextCcidPage = function () {
      uncheckAllSelectedAndGetCcid($scope.ctrl.dataCcid.currentPage + 1);
    };

    $scope.navigateToPreviousCcidPage = function () {
      uncheckAllSelectedAndGetCcid($scope.ctrl.dataCcid.currentPage - 1);
    };

    $scope.getPagedResults = function (page, parameters) {
      if (parameters && parameters.selectedClient) {
        $http({
          method: 'POST',
          url: $scope.ctrl.msInvoiceApiUrl + 'importiAnticipi/getAdvancePayment',
          headers: {
            'Content-Type': 'application/json'
          },
          data: {
            currentPage: page || 0,
            dsp: parameters.dsp ? parameters.dsp.idDsp : void 0,
            idClient: parameters.selectedClient.idAnagClient,
            advancePaymentCode: parameters.advancePaymentCode ? parameters.advancePaymentCode : null,
            monthFrom: parameters.monthFrom,
            yearFrom: parameters.yearFrom,
            monthTo: parameters.monthTo,
            yearTo: parameters.yearTo,
            showStandBy: parameters.showStandBy,
            hideForm: false
          }
        }).then(function successCallback(response) {
          $scope.ctrl.data = response.data;
        }).catch(function errorCallback(response) {
          $scope.ctrl.data = [];
          UtilService.handleHttpRejection(response);
        });

        $scope.allSelected = false;
        $scope.ctrl.selectedDsp = parameters.dsp;
      }
    };

    differentCurrencies = function (values) {
      var valueArr = _.values(values).map(function (item) {
        return item.currency;
      });
      var value = '';
      var differentCurrency = valueArr.some(function (item, index, array) {
        var tmp = item;
        if (value === '') {
          value = tmp;
        }
        return value !== tmp;
      });
      return differentCurrency;
    };

    // funzione che renderizza il popup per l'aggiunta di una configurazione del DSP
    $scope.showAddNewAdvancePayment = function (event) {
      var rootScope = $scope;
      ngDialog.open({
        template: 'pages/advance-payment/dialog-add-advance-payment.html',
        plain: false,
        width: '60%',
        scope: $scope,
        controller: ['$scope', 'ngDialog', '$http', function (scope, dialog, http) {
          scope.ctrl.rScope = rootScope;

          scope.showAlert = UtilService.showAlert;

          scope.cancel = function () {
            ngDialog.closeAll(false);
          };

          scope.check = function () {
            if (!scope.amountUsed) {
              return false;
            }
            if (!scope.originalAmount) {
              return false;
            }
            return parseFloat(scope.amountUsed.replace(',', '.')) > parseFloat(scope.originalAmount.replace(',', '.'));
          };
          // MODIFICARE I PARAMETRI
          scope.save = function () {
            var amount = scope.amountUsed.replace(',', '.');
            var oAmount = scope.originalAmount.replace(',', '.');
            http({
              method: 'POST',
              url: scope.ctrl.rScope.ctrl.msInvoiceApiUrl + 'importiAnticipi/addAdvancePayment',
              headers: {
                'Content-Type': 'application/json'
              },
              data: {
                periodFrom: scope.selectedPeriodFrom,
                periodTo: scope.selectedPeriodTo,
                invoiceNumber: scope.invoiceNumber,
                originalAmount: oAmount,
                amountUsed: amount,
                idAnagClient: scope.selectedClient.idAnagClient
              }
            }).then(function successCallback(response) {
              dialog.closeAll(true);

              scope.ctrl.rScope.getPagedResults(0,
                $scope.ctrl.filterParameters
              );
              scope.ctrl.rScope.selectedDsp = scope.selectedDsp;
            }).catch(function errorCallback(response) {
              if (response.status === 409) {
                UtilService.showAlert('Errore', 'Data di inizio validità successiva alla data di fine validità.');
              } else if (response.status === 406) {
                UtilService.showAlert('Errore', 'Fattura già associata.');
              } else {
                UtilService.handleHttpRejection(response);
              }
            });
          };
        }]
      })
    };

    // funzione che renderizza il popup per l'aggiunta di una configurazione del DSP
    $scope.showUpdateAdvancePayment = function (itemSelected) {
      var rootScope = $scope;
      ngDialog.open({
        template: 'pages/advance-payment/dialog-update-advance-payment.html',
        plain: false,
        width: '60%',
        controller: ['$scope', 'ngDialog', '$http', function (scope, dialog, http) {
          scope.ctrl = {
            rScope: rootScope,
            item: itemSelected,
            dspList: [],
            selectedPeriodTo: itemSelected.periodTo
          };

          http({
            method: 'GET',
            url: 'rest/data/dspUtilizations'

          }).then(function successCallback(response) {
            scope.ctrl.dspList = response.data.dsp;
          }).catch(function errorCallback(response) {
            scope.ctrl.dspList = [];
            UtilService.handleHttpRejection(response);
          });


          scope.formatNumber = function (number) {
            return twoDecimalRound(number);
          };

          scope.showAlert = function (title, message) {
            ngDialog.open({
              template: 'pages/advance-payment/dialog-alert.html',
              plain: false,
              width: '50%',
              controller: ['$scope', 'ngDialog', function (scope2, dialog2) {
                var ngDialogId = $scope.ngDialogId;
                scope2.ctrl = {
                  title: title,
                  message: message,
                  ngDialogId: ngDialogId
                };
                scope2.cancel = function (ngDialogId) {
                  dialog2.close(ngDialogId);
                };
              }]
            })
          };

          scope.cancel = function () {
            dialog.closeAll(false);
          };

          // MODIFICARE I PARAMETRI
          scope.save = function () {
            http({
              method: 'POST',
              url: scope.ctrl.rScope.ctrl.msInvoiceApiUrl + 'importiAnticipi/updateAdvancePayment',
              headers: {
                'Content-Type': 'application/json'
              },
              data: {
                idImportoAnticipo: scope.ctrl.item.idImportoAnticipo,
                idAnagClient: $scope.ctrl.filterParameters.selectedClient.idAnagClient,
                periodFrom: scope.ctrl.item.periodFrom,
                periodTo: scope.ctrl.selectedPeriodTo,
                invoiceNumber: scope.ctrl.item.invoiceNumber,
                originalAmount: scope.ctrl.item.originalAmount,
                amountUsed: scope.ctrl.item.amountUsed
              }
            }).then(function successCallback(response) {
              dialog.closeAll(true);

              scope.ctrl.rScope.getPagedResults(0,
                50,
                $scope.ctrl.filterParameters
              );
              // scope.ctrl.rScope.filterParameters.selectedClient = scope.ctrl.item;
            }, function errorCallback(response) {
              if (response.data.message == null) {
                scope.showAlert('Errore', "Impossibile aggiornare la data dell'anticipo");
              } else {
                scope.showAlert('Errore', response.data.message);
              }
            });
          };
        }]
      })
    };

    // Mostra il pup up con i dettagli dei CCID associati all'anticipo da eliminare
    $scope.deleteDialogAnticipo = function (advancePaymant) {
      var rootScope = $scope;
      ngDialog.open({
        template: 'pages/advance-payment/dialog-delete-anticipo.html',
        plain: false,
        width: '60%',
        scope: $scope,
        controller: ['$scope', 'ngDialog', '$http', function (scope, dialog, http) {
          http({
            method: 'POST',
            url: rootScope.ctrl.msInvoiceApiUrl + 'importiAnticipi/getLinkedCcid',
            headers: {
              'Content-Type': 'application/json'
            },
            data: {
              idImportoAnticipo: advancePaymant.idImportoAnticipo,
            }
          }).then(function successCallback(response) {
            scope.ctrl.items = response.data;
          }).catch(function errorCallback(response) {
            scope.ctrl.items = [];
          });


          scope.delete = function () {
            http({
              method: 'DELETE',
              url: rootScope.ctrl.msInvoiceApiUrl + 'importiAnticipi/deleteAdvancePayment',
              headers: {
                'Content-Type': 'application/json'
              },
              data: {
                idImportoAnticipo: advancePaymant.idImportoAnticipo,
              }
            }).then(function successCallback(response) {
              dialog.closeAll(true);
              scope.ctrl.rScope.getPagedResults(0,
                $scope.ctrl.filterParameters
              );
              rootScope.selectedDsp = scope.ctrl.item;
            }, function errorCallback(response) {
              UtilService.handleHttpRejection(response);
              rootScope.ctrl.items = [];
            });
          };

          scope.ctrl = {
            rScope: rootScope,
            item: advancePaymant,
            dspList: [],
            selectedPeriodTo: advancePaymant.periodTo
          };
          scope.formatNumber = function (number) {
            return twoDecimalRound(number);
          };

          scope.cancel = function () {
            dialog.closeAll(false);
          };
        }]
      })
    };

    $scope.useAdvancePayment = function (advancePayment) {

      advancePayment.totalSelected = 0;

      $scope.ctrl.filterParameters.selectedPeriodTo = advancePayment.periodTo;
      $scope.ctrl.filterParameters.selectedPeriodFrom = advancePayment.periodFrom;
      $scope.ctrl.filterParameters.fatturato = false;
      $scope.pagina = angular.copy($scope.CONSUMA);
      $scope.ctrl.advancePayment = advancePayment;
      $scope.getCcid(0, $scope.ctrl.filterParameters);
    };

    $scope.readjustAdvancePayment = function (advancePayment) {

      if (!($scope.cCID && $scope.cCID.list)) {
        $scope.cCID = { list: [] };
      }
      $scope.cCID.total = 0;

      advancePayment.totalSelected = 0;
      $scope.ctrl.filterParameters.fatturato = true;
      $scope.ctrl.filterParameters.selectedPeriodTo = advancePayment.periodTo;
      $scope.ctrl.filterParameters.selectedPeriodFrom = advancePayment.periodFrom;
      $scope.pagina = angular.copy($scope.RIPROPORZIONA);
      $scope.ctrl.advancePayment = advancePayment;
      $scope.getCcid(0, $scope.ctrl.filterParameters);
    };

    $scope.closeAdvancePayment = function (advancePayment) {

      if (!($scope.cCID && $scope.cCID.list)) {
        $scope.cCID = { list: [] };
      }
      $scope.cCID.total = 0;
      advancePayment.totalSelected = 0;
      $scope.ctrl.filterParameters.fatturato = false;
      $scope.ctrl.filterParameters.selectedPeriodTo = advancePayment.periodTo;
      $scope.ctrl.filterParameters.selectedPeriodFrom = advancePayment.periodFrom;
      $scope.pagina = angular.copy($scope.CHIUDI);
      $scope.ctrl.advancePayment = advancePayment;
      $scope.getCcid(0, $scope.ctrl.filterParameters);
    };


    // TODO MODIFICARE I PARAMETRI DELLA CHIAMATA E RITORNO CCID <-> ANTICIPO
    $scope.showCcidInfo = function (advancePaymant) {

      var rootScope = $scope;
      ngDialog.open({
        template: 'pages/advance-payment/dialog-show-ccid-info.html',
        plain: false,
        width: '60%',
        controller: ['$scope', 'ngDialog', '$http', function (scope, dialog, http) {
          http({
            method: 'POST',
            url: rootScope.ctrl.msInvoiceApiUrl + 'importiAnticipi/getLinkedCcid',
            headers: {
              'Content-Type': 'application/json'
            },
            data: {
              idImportoAnticipo: advancePaymant.idImportoAnticipo,
              idDSP: advancePaymant.idDSP,
              periodFrom: advancePaymant.periodFrom,
              periodTo: advancePaymant.periodTo,
              invoiceNumber: advancePaymant.invoiceNumber,
              originalAmount: advancePaymant.originalAmount,
              amountUsed: advancePaymant.amountUsed
            }
          }).then(function successCallback(response) {
            scope.ctrl.items = response.data;
          }, function errorCallback(response) {
            scope.ctrl.items = [];
            UtilService.handleHttpRejection(response);

          });
          scope.ctrl = {
            item: [],
            rScope: rootScope
          };
          scope.formatNumber = function (number) {
            return twoDecimalRound(number);
          };

          scope.cancel = function () {
            dialog.closeAll(false);
          };
        }]
      })
    };


    function isObjectEmpty(obj) {
      if (Object.keys(obj).length === 0 && obj.constructor === Object) {
        return true;
      }
      return false;
    }


    $scope.formatNumber = function (number) {
      return twoDecimalRound(number);
    };

    $scope.filterApply = function (parameters) {
      $scope.ctrl.filterParameters = parameters;
      $scope.getPagedResults(0,
        parameters
      );
    };

    $scope.multiselectSettings = {
      scrollableHeight: '200px',
      scrollable: false,
      enableSearch: true,
      selectionLimit: 1,
      showUncheckAll: false,
      searchBr: true,
      checkboxes: false
    };

    $scope.invoiceApply = function (parameters) {
      $scope.sortBy = undefined;
      angular.forEach(parameters, function (value, key) {
        $scope.ctrl.filterParameters[key] = value;
      });
      $scope.getCcid(0, $scope.ctrl.filterParameters)
    };

    $scope.sort = function (field) {
      $scope.sortBy = field;
      $scope.asc = !$scope.asc;
      $scope.getCcid($scope.ctrl.dataCcid.currentPage, $scope.ctrl.filterParameters)
    };

    function sumValoreResiduoCcidSelected(map) {
      var result = 0;
      Object.keys(map).forEach(function (key) {
        result += map[key].valoreResiduo;
      });
      return result;
    }

    function reassign(dataCcid) {
      $scope.ctrl.advancePayment = dataCcid.idAdvancePayment ? dataCcid.idAdvancePayment : void 0;

      angular.forEach($scope.ctrl.dataCcid.rows, function (oldData, oldKey) {
        $scope.ctrl.dataCcid.rows[oldKey].invoiceAmountNew = 0;
        angular.forEach(dataCcid.ccidList, function (updatedData, newKey) {
          if (oldData.idCCIDmetadata === updatedData.idCCIDmetadata) {
            $scope.ctrl.dataCcid.rows[oldKey] = updatedData;
            $scope.ctrl.dataCcid.rows[oldKey].checked = true;
          }
        });
      });
    }

    function getCollectionValues(collection) {
      var selectedList = [];
      if (collection) {
        for (var e in collection) {
          selectedList.push(collection[e].id ? collection[e].id : collection[e]);
        }
      }
      return selectedList;
    }

    $scope.getCcid = function (page, params) {
      var msInvoiceApiUrl = $scope.ctrl.msInvoiceApiUrl;
      var periodFromArr = params.selectedPeriodFrom ? params.selectedPeriodFrom.split('-') : [0, 0];
      var periodToArr = params.selectedPeriodTo ? params.selectedPeriodTo.split('-') : [0, 0];
      params.dspList = getCollectionValues(params.selectedDspModel);
      params.countryList = getCollectionValues(params.selectedCountryModel);
      params.utilizationList = getCollectionValues(params.selectedUtilizationModel);
      params.offerList = getCollectionValues(params.selectedOfferModel);
      $scope.dspsUtilizationsCommercialOffersCountries = angular.copy(dspsUtilizationsCommercialOffersCountries);
      $scope.dspsUtilizationsCommercialOffersCountries.dsp =  angular.copy(params.selectedClient.dspList);
      $scope.dspsUtilizationsCommercialOffersCountries.commercialOffers = angular.copy(dspsUtilizationsCommercialOffersCountries).commercialOffers.filter(offer=> 
          $scope.dspsUtilizationsCommercialOffersCountries.dsp.map(el => el.idDsp).includes(offer.idDSP)
      );
      $scope.dspsUtilizationsCommercialOffersCountries.utilizations = $scope.dspsUtilizationsCommercialOffersCountries.utilizations.filter(util =>
           util.idDsp.some(r=> $scope.dspsUtilizationsCommercialOffersCountries.dsp.map(el => el.idDsp).includes(r))
      );
      if (params.selectedClient) {
        $http({
          method: 'POST',
          url: msInvoiceApiUrl + 'invoice/all/ccid',
          headers: {
            'Content-Type': 'application/json'
          },
          data: {
            currentPage: page || 0,
            clientId: params.selectedClient.idAnagClient,
            dspList: params.dspList ? params.dspList : [],
            countryList: params.countryList ? params.countryList : [],
            utilizationList: params.utilizationList ? params.utilizationList : [],
            offerList: params.offerList ? params.offerList : [],
            monthFrom: (params.monthFrom ? params.monthFrom : periodFromArr[0]),
            yearFrom: (params.yearFrom ? params.yearFrom : periodFromArr[1]),
            monthTo: (params.monthTo ? params.monthTo : periodToArr[0]),
            yearTo: (params.yearTo ? params.yearTo : periodToArr[1]),
            fatturato: params.fatturato,
            sortBy: $scope.sortBy ? $scope.sortBy : 'idDsr',
            asc: $scope.asc
          }
        }).then(function successCallback(response) {
          $scope.ctrl.dataCcid = response.data;
          $scope.toggleCCID();
        }, function errorCallback(response) {
          $scope.ctrl.dataCcid = [];
        });
      }
    };

    $scope.selectedCheckbox = {};
    // $scope.cCID = [];

    $scope.toggleCCID = function () {
      if (!($scope.cCID && $scope.cCID.list)) {
        $scope.cCID = { list: [] };
      }
      $scope.cCID.total = 0;
      for (var _i = 0, _a = Object.values($scope.ctrl.selectedItems); _i < _a.length; _i++) {
        var value = _a[_i];
        if (value.checked)
          $scope.cCID.total += value.invoiceAmount;
      }
      if (Object.values($scope.ctrl.selectedItems).length !== 0) {
        if ($scope.pagina === $scope.RIPROPORZIONA) {

          $http({
            method: 'POST',
            url: $scope.ctrl.msInvoiceApiUrl + 'importiAnticipi/riproporziona-fake',
            headers: {
              'Content-Type': 'application/json'
            },
            data: {
              idAdvancePayment: $scope.ctrl.advancePayment,
              ccidList: Object.values($scope.ctrl.selectedItems)
            }
          }).then(function successCallback(response) {
            reassign(response.data);
          });
        }

        if ($scope.pagina === $scope.CHIUDI) {
          $http({
            method: 'POST',
            url: $scope.ctrl.msInvoiceApiUrl + 'importiAnticipi/chiudi-anticipo-fake',
            headers: {
              'Content-Type': 'application/json'
            },
            data: {
              idAdvancePayment: $scope.ctrl.advancePayment,
              ccidList: Object.values($scope.ctrl.selectedItems)
            }
          }).then(function successCallback(response) {
            reassign(response.data);
          });
        }
      } else {
        angular.forEach($scope.ctrl.dataCcid.rows, function (oldData, oldKey) {
          $scope.ctrl.dataCcid.rows[oldKey].invoiceAmountNew = 0;
        });
      }
    };

    function addItem(item) {
      $scope.ctrl.advancePayment.totalSelected += item.valoreResiduo;
      $scope.ctrl.selectedItems[item.idCCIDmetadata] = item;
      if ($scope.pagina === $scope.CONSUMA) {
        if ($scope.ctrl.advancePayment.amountUsed - $scope.ctrl.advancePayment.totalSelected < 0) {
          UtilService.showAlert('Info', "il Ccid selezionato supera l'importo residuo dell'anticipo,per la quota parte del residuo fatturabile si dovrà produrre una fattura");
          return
        } /*else if ($scope.ctrl.advancePayment.amountUsed - $scope.ctrl.advancePayment.totalSelected < 0) {
        UtilService.showAlert('Info', "L'anticipo verrà consumato completamente");
      }*/
      }
    }

    function removeItem(item) {
      delete $scope.ctrl.selectedItems[item.idCCIDmetadata];
      $scope.ctrl.advancePayment.totalSelected -= item.valoreResiduo;
    }

    function addOrRemoveSelectedItem(item) {
      if (!$scope.ctrl.selectedItems[item.idCCIDmetadata]) {
        addItem(item);
      } else {
        removeItem(item);
      }
      // setMainCheckbox();
    }

    $scope.toggleItem = function (item) {
      if (!$scope.ctrl.dataCcid || !$scope.ctrl.dataCcid.rows) {
        $scope.allSelected = false;
        return;
      }
      if (item === true) {
        // seleziona tutti gli elementi
        var tvalue = 0;
        for (var i = 0; i < $scope.ctrl.dataCcid.rows.length; i++) {
          tvalue = tvalue + $scope.ctrl.dataCcid.rows[i].valoreResiduo;
          if (tvalue > $scope.ctrl.advancePayment.amountUsed && $scope.pagina === $scope.CONSUMA) {
            UtilService.showAlert('Errore', "Impossibile consumare l'intera lista dei CCID,il totale dei valori fatturabili supera il valore residuo dell'anticipo.");
            return;
          }
          $scope.ctrl.dataCcid.rows[i].checked = true;
          addItem($scope.ctrl.dataCcid.rows[i]);
        }
      } else if (item === false) {
        // deseleziona tutti gli elementi
        for (var i = 0; i < $scope.ctrl.dataCcid.rows.length; i++) {
          $scope.ctrl.dataCcid.rows[i].checked = false;
          removeItem($scope.ctrl.dataCcid.rows[i]);
        }
      } else {
        // aggiungi o rimuovi elemento in base alla sua presenza o meno
        // nella lista
        addOrRemoveSelectedItem(item);
      }
      // setMainCheckbox();
    };

    /*setMainCheckbox = function () {
      $scope.allSelected = Object.values($scope.ctrl.selectedItems).length === $scope.ctrl.dataCcid.rows.length;
    };*/

    differentCurrencies = function (values) {
      var valueArr = _.values(values).map(function (item) {
        return item.currency;
      });
      var value = '';
      var differentCurrency = valueArr.some(function (item, index, array) {
        var tmp = item;
        if (value === '') {
          value = tmp;
        }
        return value !== tmp;
      });
      return differentCurrency;
    };

    $scope.selectionIsEmpty = function () {
      if ($scope.ctrl && $scope.ctrl.selectedItems) {
        return !Object.keys($scope.ctrl.selectedItems).length;
      }
    };

    $scope.confirmUseAdvancePayment = function () {
      // $scope.ctrl.hideEditing = false

      if (!($scope.cCID && $scope.cCID.list)) {
        $scope.cCID = { list: [] };
      }
      $scope.cCID.total = 0;

      if ($scope.ctrl && $scope.ctrl.selectedItems && Object.keys($scope.ctrl.selectedItems).length > 0) {
        $http({
          method: 'POST',
          url: $scope.ctrl.msInvoiceApiUrl + 'importiAnticipi/useAdvancePayment',
          headers: {
            'Content-Type': 'application/json'
          },
          data: {
            idAdvancePayment: $scope.ctrl.advancePayment,
            ccidList: Object.values($scope.ctrl.selectedItems)
          }
        }).then(function successCallback(response) {
          $scope.cancel();
          UtilService.showAlert('Esito richiesta', 'Anticipo consumato correttamente');
        }).catch(function errorCallback(error) {
          UtilService.handleHttpRejection(error);
        });
      }
    };

    $scope.confirmReadjustAdvancePayment = function () {
      // $scope.ctrl.hideEditing = false
      if ($scope.ctrl && $scope.ctrl.selectedItems && Object.keys($scope.ctrl.selectedItems).length > 0) {
        $http({
          method: 'POST',
          url: $scope.ctrl.msInvoiceApiUrl + 'importiAnticipi/riproporziona',
          headers: {
            'Content-Type': 'application/json'
          },
          data: {
            idAdvancePayment: $scope.ctrl.advancePayment,
            ccidList: Object.values($scope.ctrl.selectedItems)

          }
        }).then(function successCallback(response) {
          $scope.cancel();
          UtilService.showAlert('Esito richiesta', 'Anticipo riproporzionato correttamente');
        }).catch(function errorCallback(error) {
          UtilService.handleHttpRejection(error);
        });
      }
    };

    $scope.confirmCloseAdvancePayment = function () {
      // $scope.ctrl.hideEditing = false
      if ($scope.ctrl && $scope.ctrl.selectedItems) {
        $http({
          method: 'POST',
          url: $scope.ctrl.msInvoiceApiUrl + 'importiAnticipi/chiudi-anticipo',
          headers: {
            'Content-Type': 'application/json'
          },
          data: {
            idAdvancePayment: $scope.ctrl.advancePayment,
            ccidList: Object.values($scope.ctrl.selectedItems)

          }
        }).then(function successCallback(response) {
          $scope.cancel();
          UtilService.showAlert('Esito richiesta', 'Anticipo chiuso correttamente');
        }).catch(function errorCallback(error) {
          UtilService.handleHttpRejection(error);
        });
      }
    };

    $scope.cancel = function () {
      // $scope.ctrl.hideEditing = false
      $scope.ctrl.data = [];
      if ($scope.ctrl.dataCcid)
        $scope.ctrl.dataCcid.rows = [];
      $scope.ctrl.advancePayment = undefined;
      $scope.ctrl.selectedItems = [];
      $scope.selectedCheckbox = {};
      angular.forEach($scope.ctrl.filterParameters, function (value, key) {
        if (key !== 'selectedClient')
          $scope.ctrl.filterParameters[key] = undefined;
      });
      $scope.getPagedResults(0,
        $scope.ctrl.filterParameters
      );
    };

    isObjectEmpty = function (obj) {
      if (Object.keys(obj).length === 0 && obj.constructor === Object) {
        return true;
      }

      return false;
    };


    extractItemAnnotationData = function (item) {
      var periodPrefix = '';
      if (item.periodType === 'quarter') {
        periodPrefix = 'Q';
      }
      return item.utilizationType + ' ' + item.year + ' ' + periodPrefix + item.period;
    };


    $scope.goToAccantona = function (idIngestion, ctrl) {
      $window.localStorage.removeItem('item');
      $window.localStorage.removeItem('ctrl');
      accantonaContext.context = { item: idIngestion, ctrl: ctrl };
      $window.localStorage.setItem('item', JSON.stringify(idIngestion));
      if (ctrl.rScope) {
        var ctrlCopy = _.clone(ctrl);
        delete ctrlCopy.rScope;
        $window.localStorage.setItem('ctrl', JSON.stringify(ctrlCopy));
      } else {
        $window.localStorage.setItem('ctrl', JSON.stringify(ctrl));
      }
      $scope.goTo('/accantonamentoGestioneReclami', '');

    };

    $scope.goTo = function (url, param) {
      if (url != undefined && param != undefined) {
        $location.path(url + param);
      }
    };

  }
]);


codmanApp.filter('range', function () {
  return function (input, min, max) {
    for (var i = parseInt(min); i <= parseInt(max); i++) {
      input.push(i);
    }
    return input;
  };
});

