

codmanApp.controller('VisualizzazioneDettaglioMovimentoCtrl',['$scope', '$http', '$routeParams', '$location', 'ngDialog', 'DialogContabiliPM',
	function($scope, $http, $routeParams, $location, ngDialog, DialogContabiliPM){
	
	$scope.user = angular.element("#headerLinksBig").text().trim();
	
	$scope.importoDEM = function(importo,documento){
		if(importo != undefined && documento != undefined){
			if(documento == 221){
				return '-'+importo; 
			}else{
				return importo;
			}
		}
	}
	
	$scope.idMovimento = $routeParams.idMovimento;
	
	$scope.addContabili = function(user){
		
		DialogContabiliPM.addContabili(user);
	}
	
	$scope.removeContabili = function(){
		
	}
	
	$scope.addPM = function(idEvento,user){
		
		DialogContabiliPM.addPM(idEvento,user);		
	}
	
//	$scope.removePM = function(idMovimentoContabile){
//		$http({
//			method:'PUT',
//			url:'rest/performing/cruscotti/updateidEventofromMovimentazione',
//			params: {
//				idEvento: null,
//				idMovimento: idMovimentoContabile,
//				userId: $scope.user
//			}
//		}).then(function(response){
//			$scope.showError("Success","Programma Musicale rimosso con successo");
//			$scope.init();
//		},function(response){
//			$scope.showError("Error","Non è stato possibile rimuovere il programma musicale selezionato");
//		})
//	}
	
	$scope.flagPM = true;
	
	$scope.visibleAndSave = function(pmPrevisti,pmPrevistiPrincipale,pmPrevistiSpalla,index){
		if($scope.flagPM == true){
			angular.element("#spanPM" + index).attr('class',"glyphicon glyphicon-saved");
			angular.element("#pmPrevisti" + index).prop('readonly', false);
			angular.element("#pmPrevistiPrincipale" + index).prop('readonly', false);
			angular.element("#pmPrevistiSpalla" + index).prop('readonly', false);
			$scope.flagPM = false;
		}else if($scope.flagPM == false){
			angular.element("#spanPM" + index).attr('class',"glyphicon glyphicon-pencil");
			angular.element("#pmPrevisti" + index).prop('readonly', true);
			angular.element("#pmPrevistiPrincipale" + index).prop('readonly', true);
			angular.element("#pmPrevistiSpalla" + index).prop('readonly', true);
			$scope.flagPM = true;
			
			$scope.savePM(pmPrevisti,pmPrevistiPrincipale,pmPrevistiSpalla,index);
		}
		
	}
	
	$scope.savePM = function (pmPrevisti,pmPrevistiPrincipale,pmPrevistiSpalla,index){
		
		var checkPM = {};
		var idEventoPM = $scope.dettagliPM[index].idEvento
		
		if(pmPrevisti[index] == undefined && pmPrevistiPrincipale[index] == undefined && pmPrevistiSpalla[index] == undefined){
			$scope.showError("Errore","Riempire almeno un campo");
			return;
		}
		
		$http({
			method:'GET',
			url: 'rest/performing/cruscotti/controlloPmRientrati',
			params: {
				idEvento : idEventoPM
			}
		}).then(function(response){
			checkPM = response.data;
			if(pmPrevisti[index] != undefined){
				if(pmPrevisti[index] < checkPM.PmRientrati){
					$scope.showError("Errore","Il numero dei Programmi Musicali Attesi non può essere minore dei Programmi Musicali Rientrati. Refreshare la pagina per avere gli ultimi aggiornamenti");
					return;
				}else{
					
					$http({
						method:'PUT',
						url: 'rest/performing/cruscotti/updatePmAttesiFromEventiPagati',
						params: {
							idEventoPagato : $scope.idEventoPagato,
							pmPrevisti: pmPrevisti[index],
							pmPrevistiSpalla: null,
							userId: $scope.user
						}
					}).then(function(response){
						$scope.showError("Success","Programmi Musicali Attesi modificati con successo");
						$scope.init();
					},function(response){
						$scope.showError("Errore","Non è stato possibile modificare i Programmi Musicali Attesi. Riprovare");
					})
				}
			}else{
				if(pmPrevistiPrincipale[index] < checkPM.PmRientrati || pmPrevistiSpalla[index] < checkPM.PmRientratiSpalla){
					$scope.showError("Errore","Il numero dei Programmi Musicali Attesi Principali e Spalla non può essere minore dei Programmi Musicali Rientrati Principali e Spalla. Refreshare la pagina per avere gli ultimi aggiornamenti");
					return;
				}else{
					$http({
						method:'PUT',
						url: 'rest/performing/cruscotti/updatePmAttesiFromEventiPagati',
						params: {
							idEventoPagato : $scope.idEventoPagato,
							pmPrevisti: pmPrevistiPrincipale[index],
							pmPrevistiSpalla: pmPrevistiSpalla[index],
							userId: $scope.user
						}
					}).then(function(response){
						$scope.showError("Success","Programmi Musicali Principali e Spalla Attesi modificati con successo");
						$scope.init();
					},function(response){
						$scope.showError("Errore","Non è stato possibile modificare i Programmi Musicali Principali e Spalla Attesi. Riprovare");
					})
				}
			}

		},function(response){
			$scope.showError("Errore","Non è stato possibile fare il controllo sui PM rientrati. Riprovare");
		})
	}
	
	$scope.goTo = function(url,param1,param2){
		if(url != undefined && param1 != undefined && param2 != undefined){
			$location.path(url + param1 + param2);
		}
	}
	
	$scope.init = function(){
		$http({
			method: 'GET',
			url: 'rest/performing/cruscotti/movimentiDetail',
			params: {
				movimento: $scope.idMovimento
			}
		}).then(function(response){
			$scope.samplings = response.data;
			$scope.evento = response.data[0];
			$scope.manifestazione = response.data[0].manifestazione;
			$scope.movimentazioniPM = response.data[0].movimentazioniPM;
			$scope.dettagliPM = response.data[0].dettaglioPM;
			$scope.idEventoPagato = response.data[0].id;
		},function(response){
			$scope.showError("Errore","Non risulta nessun dato per il movimento selezionato");
		});
	}
	
	$scope.PMAttesi = function(incasso,previsti,previstiSpalla){
		if(incasso == "2244"){ 
			$scope.pmShow = true;
			return previsti + "  Principale   " + previstiSpalla + "  Spalla";
		}else{
			return previsti;
		}
	};
	
	$scope.PMRientrati = function(incasso,rientrati,rientratiSpalla){
		if(incasso == "2244"){
			return rientrati + " Principale " + rientratiSpalla + " Spalla";
		}else{
			return rientrati;
		}
	};
	
	$scope.formatDate = function(date){
		if(date != null){
			var data = date.split("T")[0];
			return data;
		}
		
	};
	
	$scope.formatHour = function(hour){
		if(hour != null){
			var ora = hour.slice(0,2) + ":" + hour.slice(2,4);
			return ora;
		}
		
	}
	
	$scope.numeroPM = function(incasso,flagGP,numPM){
		if(incasso == "2244" && flagGP == "1"){
			return numPM + " (Principale)";
		}else if (incasso == "2244" && flagGP == "0"){
			return numPM + " (Spalla)";
		}else {
			return numPM;
		}
	}
	
	$scope.showError = function(title, msgs) {
		ngDialog.open({
			template : 'pages/advance-payment/dialog-alert.html',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				$scope.ctrl = {
					title : title,
					message : msgs
				};
				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				$scope.save = function() {
					ngDialog.closeAll(true);
				};
			} ]
		}).closePromise.then(function(data) {
			// no op
		});
	};
	
	showAlert = function(title, message) {
		ngDialog.open({
			template : 'pages/advance-payment/dialog-alert.html',
			plain : false,
			width : '50%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				var ngDialogId = $scope.ngDialogId;
				$scope.ctrl = {
					title : title,
					message : message,
					ngDialogId : ngDialogId
				};
				$scope.cancel = function(ngDialogId) {
					ngDialog.close(ngDialogId);
				};
			} ]
		}).closePromise.then(function(data) {
			if (data.value === true) {

			}
		});
	};
	
	$scope.back = function(){
		 window.history.back();
	}
	
	
}]);