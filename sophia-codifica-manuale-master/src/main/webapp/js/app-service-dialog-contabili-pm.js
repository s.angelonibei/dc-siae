codmanApp.service('DialogContabiliPM',['ngDialog', function(ngDialog){
	
	this.addContabili = function(user){
		ngDialog.open({
			template : 'pages/advance-payment/dialog-add-contabilita.html',
			plain : false,
			width : '90%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				$scope.user = user;
				$scope.showRicerca = false;
				$scope.fatturaTrovata = {};
				
				$scope.showPerNumero = function(numeroFattura){
					if(numeroFattura != undefined && numeroFattura != ""){
						$scope.fatturaTrovata = {};
						$scope.fatturaTrovata = {numeroFattura: numeroFattura,seprag: "prenderlo dalla chiamata", reversale: "prenderlo dalla chiamata"};
						$scope.showRicerca = true;
					}else{
						$scope.showRicerca = false;
					}
				}
				
				$scope.showPerSR = function(seprag,reversale){
					if(seprag != undefined && seprag != "" && reversale != undefined && reversale != ""){
						$scope.fatturaTrovata = {};
						$scope.fatturaTrovata = {numeroFattura: "prenderlo dalla chiamata" ,seprag: seprag, reversale: reversale};
						$scope.showRicerca = true;
					}else{
						$scope.showRicerca = false;
					}
				}
				
				$scope.addFattura = function(){
					
				}
				
				$scope.ctrl = {
					title : "Aggiunta Informazione Contabile"
				};
				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				$scope.save = function() {
					ngDialog.closeAll(true);
				};
			} ]
		}).closePromise.then(function(data) {
			// no op
		});
	};
	
	this.addPM = function(idEvento,user,scope){
		
		ngDialog.open({
			template : 'pages/advance-payment/dialog-add-pm.html',
			plain : false,
			width : '90%',
			controller : [ '$scope', 'ngDialog', '$location', '$http', 'DialogContabiliPM', function($scope, ngDialog, $location, $http, DialogContabiliPM) {
				
				var scopePrecedente = scope;
				
				$scope.ctrl = {
					navbarTab : 'dialog-add-pm',
					page : 0
				};
				
				$scope.showVociIncasso = false;
				
				$scope.navigateToNextPage = function() {
					$scope.ctrl.page=$scope.ctrl.page+1;
					$scope.visiblePM($scope.numeroPM);

				};

				$scope.navigateToPreviousPage = function() {
					$scope.ctrl.page=$scope.ctrl.page-1;
					$scope.visiblePM($scope.numeroPM);

				};
				
				$scope.evento = idEvento;
				$scope.user = user;	
				
				$scope.visiblePMRicercato = false;
				$scope.pmRicercati = [];
				
				$scope.assignIdPM = function(numPM,idProgrammaMusicale){
					$scope.numPM = numPM;
					$scope.idProgrammaMusicale = idProgrammaMusicale;
					$scope.showVociIncasso = true;
				}
				
				//obtain vociIncasso list
				
				$http({
					method: 'GET',
					url: 'rest/performing/cruscotti/getVociIncassoPerIdEvento',
					params: {
						idEvento: $scope.evento
					}
				}).then(function(response){
					$scope.listaVociIncasso = response.data;
				},function(response){
										
				})
				
				$scope.ctrl.first = 0;
				$scope.ctrl.last = 50;
				$scope.ctrl.page = 0;
				
				$scope.visiblePM = function(numPM){
					
					var params = {};
					
					params.page = $scope.ctrl.page;
					
					if(numPM != undefined && numPM != ""){
						params.numeroPm = numPM;
					} 
					
					$http({
						method: 'GET',
						url: 'rest/performing/cruscotti/listaPmDisponibili',
						params: params
					}).then(function(response){
						$scope.ctrl.totRecords = response.data.totRecords;
						$scope.ctrl.page = response.data.currentPage;
						$scope.ctrl.first = response.data.currentPage*50;
						$scope.ctrl.last = response.data.length;
						$scope.ctrl.pageTot = Math.floor(response.data.totRecords/50);
						$scope.visiblePMRicercato = true;
						$scope.pmRicercati = response.data.records;
					},function(response){
						$scope.pmRicercati = {};
						
					})
				};
				
				$scope.ctrl = {
					title : "Aggiunta programma musicale"
				};
				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				$scope.save = function(user,idProgrammaMusicale,evento,voceIncassoSelected) {
					
					if(idProgrammaMusicale == undefined || idProgrammaMusicale == ""){
						DialogContabiliPM.showError("Aggiunta Programma Musicale","Inserire il programma musicale prima di salvare");
						return;
					}
					
					if(voceIncassoSelected == undefined || voceIncassoSelected == ""){
						DialogContabiliPM.showError("Aggiunta Programma Musicale","Inserire la voce incasso prima di salvare");
						return;
					}
					
					$http({
						method:'PUT',
						url:'rest/performing/cruscotti/aggancioPm',
						params: {
							userId: user,
							idEvento: evento,
							voceIncasso: voceIncassoSelected,
							numeroPm: $scope.numPM
						}
					}).then(function(response){
						DialogContabiliPM.showError("Aggiunta Programma Musicale","Programma Musicale associato all'evento con successo. Controllare e in caso modificare il numero dei Programmi Musicali Attesi");
						scopePrecedente.init();
					},function(response){
						DialogContabiliPM.showError("Aggiunta Programma Musicale","Errore durante l'associazione del Programma Musicale all'evento");
					})
					
					ngDialog.closeAll(true);

				};
			} ]
		});
		
	};
	
	this.showError = function(title, msgs) {
		ngDialog.open({
			template : 'pages/advance-payment/dialog-alert.html',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				$scope.ctrl = {
					title : title,
					message : msgs
				};
				$scope.cancel = function() {
					ngDialog.close($scope.ngDialogId); 
				};
				$scope.save = function() {
					ngDialog.closeAll(true);
				};
			} ]
		}).closePromise.then(function(data) {
			// no op
		});
	};
	
}]);