
/**
 * ConfigureDeliverCcidCtrl
 *
 * @path /deliver-ccid/configure-deliver-ccid.jsp
 */
codmanApp.controller('ConfigureDeliverCcidCtrl', ['$scope','$route', 'ngDialog', '$routeParams', '$location', '$http', function($scope,$route, ngDialog, $routeParams, $location, $http) {

    $scope.ctrl = {
        navbarTab: 'Destinazione CCID',
        dspConfig: [],
        filterParameters: [],
        execute: $routeParams.execute,
        hideForm: $routeParams.hideForm,
        maxrows: 20,
        first: $routeParams.first||0,
        last: $routeParams.last||20,
        sortType: 'priority',
        sortReverse: true,
        filter: $routeParams.filter,
        selectedDsp : $routeParams.dsp||'*',
        selectedUtilization : $routeParams.utilization||'*',
        selectedOffer : $routeParams.offer||'*'
    };

    $scope.onAzzera = function() {
        $scope.ctrl.execute = false;
        $scope.ctrl.first = 0;
        $scope.ctrl.last = $scope.ctrl.maxrows;
    };

    $scope.navigateToNextPage = function() {
        $location.search('execute', 'true')
            .search('hideForm', $scope.ctrl.hideForm)
            .search('first', $scope.ctrl.last)
            .search('last', $scope.ctrl.last + $scope.ctrl.maxrows);
    };

    $scope.navigateToPreviousPage = function() {
        $location.search('execute', 'true')
            .search('hideForm', $scope.ctrl.hideForm)
            .search('first', Math.max($scope.ctrl.first - $scope.ctrl.maxrows, 0))
            .search('last', $scope.ctrl.first);
    };


    // Funzione principale richiamata al caricamento della pagina
    $scope.onRefresh = function() {
        // get All DSP Configuration

        if ($scope.ctrl.execute) {
            $scope.getPagedResults($scope.ctrl.first,
                $scope.ctrl.last,
                $scope.ctrl.selectedDsp,
                $scope.ctrl.selectedUtilization,
                $scope.ctrl.selectedOffer);
        } else {
            $scope.getPagedResults(0,
                $scope.ctrl.maxrows);
        }

    };


    $scope.getPagedResults = function(first, last, dsp, utilization, offer) {
        $http({
            method: 'GET',
            url: 'rest/ccid-destination/allConfig',
            params: {
                first: first,
                last: last,
                dsp : (typeof dsp !== 'undefined' ? dsp : ""),
                utilization : (typeof utilization !== 'undefined' ? utilization : ""),
                offer : (typeof offer !== 'undefined' ? offer : "")
            }
        }).then(function successCallback(response) {
            $scope.ctrl.dspConfig = response.data;
            $scope.ctrl.first = response.data.first;
            $scope.ctrl.last = response.data.last;
        }, function errorCallback(response) {
            $scope.ctrl.dspConfig = [];
        });

    };



    //funzione che renderizza il popup per l'aggiunta di una configurazione del DSP
    $scope.showNewDSPConfig = function(event, dspConfig) {
        ngDialog.open({
            template: 'pages/deliver-ccid/dialog-new-destination.html',
            plain: false,
            width: '60%',
            controller: ['$scope', 'ngDialog', '$http', '$q', function($scope, ngDialog, $http, $q) {
                $scope.ctrl = {
                    dspConfig: dspConfig,
                    commercialOffersList : [],
                    dspList : [],
                    utilizationList : []
                };



                var promise1 = $http({method: 'GET', url: 'rest/ccid-destination/allAvailableOffers'});
                var promise2 = $http({method: 'GET', url: 'rest/data/dspUtilizations'});
                $q.all([promise1, promise2]).then(function successCallback(response) {
                    $scope.ctrl.commercialOffersList = response[0].data;
                    $scope.ctrl.commercialOffersList.splice(0, 0, {idCommercialOffering: '*', offering:'*'});
                    $scope.ctrl.dspList =  response[1].data.dsp;
                    $scope.ctrl.utilizationList = response[1].data.utilizations;
                    $scope.ctrl.utilizationList.splice(0,0,{name: '*', idUtilizationType: '*' });
                }, function errorCallback(response) {
                    $scope.ctrl.dspList = [ ];
                    $scope.ctrl.utilizationList = [ ];
                    $scope.ctrl.commercialOffersList = [ ];
                });


                // funzione che resetta il selettore opzionale relativo alla frequenza di ricezione del DSR
                $scope.clearOptional= function() {
                    $scope.showOptional = false;
                    $scope.frequenzaDSROpzionale = "";

                };

                $scope.filterOffersByDspAndUtilization = function () {
                    return function (item) {
                        if( (typeof $scope.selectedDsp === 'undefined') ||  (typeof $scope.selectedUtilization === 'undefined'))
                            return false;
                        if(item.offering == '*')
                            return true;


                        for (var i = 0; i < $scope.ctrl.commercialOffersList.length; i++) {
                            if ((item.idDSP == $scope.selectedDsp.idDsp && item.idUtilizationType == $scope.selectedUtilization.idUtilizationType)
                                || (item.idDSP == $scope.selectedDsp.idDsp &&  $scope.selectedUtilization.idUtilizationType == '*'))
                            {
                                return true;
                            }
                            return false;
                        }

                    };
                };

                $scope.openErrorDialog = function (errorMsg) {
                    ngDialog.open({
                        template: 'errorDialogId',
                        plain: false,
                        width: '40%',
                        data: errorMsg

                    });
                };

                $scope.cancel = function() {
                    ngDialog.closeAll(false);
                };


                $scope.save = function() {
                    var frequenzaDSROpt =$scope.frequenzaDSROpzionale!=null && $scope.frequenzaDSROpzionale!="" ? " + " + $scope.frequenzaDSROpzionale : "" ;
                    $http({
                        method: 'POST',
                        url: 'rest/ccid-destination',
                        data: {
                            idDsp: $scope.selectedDsp.idDsp,
                            idUtilizationType: $scope.selectedUtilization.idUtilizationType,
                            idCommercialOffer: $scope.selectedOffer.idCommercialOffering,
                            url: $scope.path,
                            rank: $scope.rank
                        }
                    }).then(function successCallback(response) {
                        ngDialog.closeAll(true);
                        $route.reload();
                    }, function errorCallback(response) {
                        if(response.status == 409){
                            $scope.openErrorDialog("Configurazione già presente");
                        }else{
                            $scope.openErrorDialog(response.statusText);
                        }

                    });
                };




            }]
        }).closePromise.then(function (data) {
            if (data.value === true) {
                // no op
            }
        });
    };


    $scope.showDeleteConfig = function(event, dspConfigToRemove) {
        ngDialog.open({
            template: 'pages/deliver-ccid/dialog-remove-destination.html',
            plain: false,
            width: '60%',
            controller: ['$scope', 'ngDialog', '$http', function($scope, ngDialog, $http) {
                $scope.ctrl = {
                    dspConfigToRemove: dspConfigToRemove
                };


                $scope.cancel = function() {
                    ngDialog.closeAll(false);
                };


                $scope.deleteDSPConfig = function() {
                    $http({
                        method: 'DELETE',
                        url: 'rest/ccid-destination',
                        headers: {
                            'Content-Type': 'application/json'},
                        data: {
                            id: $scope.ctrl.dspConfigToRemove.id
                        }

                    }).then(function successCallback(response) {
                        ngDialog.closeAll(true);
                        $route.reload();
                    }, function errorCallback(response) {
                        $scope.openErrorDialog(response.statusText);
                    });

                };

                $scope.openErrorDialog = function (errorMsg) {
                    ngDialog.open({
                        template: 'errorDialogId',
                        plain: false,
                        width: '40%',
                        data: errorMsg

                    });
                };
            }]
        }).closePromise.then(function (data) {
            if (data.value === true) {
                // no op
            }
        });
    };



    $scope.showEditDSPConfig = function(event, dspConfigToModify) {
        ngDialog.open({
            template: 'pages/ccid-destination/dialog-modify-dspconfig.html',
            plain: false,
            width: '60%',
            controller: ['$scope', 'ngDialog', '$http', function($scope, ngDialog, $http) {
                $scope.ctrl = {
                    dspConfigToModify: dspConfigToModify
                };


                $scope.cancel = function() {
                    ngDialog.closeAll(false);
                };

                // funzione che resetta il selettore opzionale relativo alla frequenza di ricezione del DSR
                $scope.clearOptional= function() {
                    $scope.showOptional = false;
                    $scope.frequenzaDSROpzionale = "";

                };


                $scope.openErrorDialog = function (errorMsg) {
                    ngDialog.open({
                        template: 'errorDialogId',
                        plain: false,
                        width: '40%',
                        data: errorMsg

                    });
                };
            }]
        }).closePromise.then(function (data) {
            if (data.value === true) {
                // no op
            }
        });
    };

    $scope.$on('filterApply', function(event, parameters) {
        $scope.ctrl.filterParameters = parameters;
        $location.search('execute', 'true')
            .search('hideForm', false)
            .search('first', 0)
            .search('last', $scope.ctrl.maxrows)
            .search('dsp', parameters.dsp)
            .search('utilization',parameters.utilization)
            .search('offer', parameters.offer);
        $scope.ctrl.hideForm = false;

    });

    $scope.onRefresh();



}]);





codmanApp.filter('range', function() {
    return function(input, min, max) {
        min = parseInt(min); //Make string input int
        max = parseInt(max);
        for (var i=min; i<=max; i++)
            input.push(i);
        return input;
    };
});

codmanApp.filter('month_range', function() {
    return function(input, min, max) {
        min = parseInt(min); //Make string input int
        max = parseInt(max);
        for (var i=min; i<=max; i++)
            input.push(i + "M");
        return input;
    };
}); 

