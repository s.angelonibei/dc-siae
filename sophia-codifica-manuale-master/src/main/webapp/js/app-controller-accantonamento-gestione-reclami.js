angular.module('codmanApp').controller('accantonamentoGestioneReclamiCtrl',['$scope', 'ngDialog', '$routeParams', '$location', '$http', 
'configService', 'anagClientData', 'anagClientService', 'UtilService', 'accantonaContext','accantonamentoService', 'returnAdvancePayment','$window', '$route', 'dspsUtilizationsCommercialOffersCountries',
function ($scope, ngDialog, $routeParams, $location, $http, configService, 
    anagClientData, anagClientService, UtilService, accantonaContext, accantonamentoService, returnAdvancePayment,$window, $route, dspsUtilizationsCommercialOffersCountries) {
    $scope.item = JSON.parse($window.localStorage.getItem('item'));
    $scope.ctrl =JSON.parse($window.localStorage.getItem('ctrl')); //selected 
    $scope.ctrl.advancePayment = $scope.item;
    $scope.ctrl.selectedDsp = [] ;
    $scope.ctrl.selectedCountryModel = [];
    $scope.ctrl.selectedOfferModel = [];
    $scope.ctrl.selectedUtilizationModel = [];
    $scope.ctrl.selectedPeriodFrom = $scope.ctrl.advancePayment.periodFrom;
    $scope.ctrl.selectedPeriodTo = $scope.ctrl.advancePayment.periodTo;
    $scope.dspsUtilizationsCommercialOffersCountries = angular.copy(dspsUtilizationsCommercialOffersCountries);
    $scope.filterParameters = $routeParams;
   

    $scope.multiselectDsp = {
        scrollableHeight: '200px',
        scrollable: false,
        enableSearch: true,
        displayProp: 'name',
        idProp: 'idDsp'
      };
   

    $scope.utilizationsOffersCountries = function() {
        $http({
          method: 'GET',
          url: 'rest/data/dspUtilizationsOffersCountries'
        }).then(function successCallback(response) {
          $scope.ctrl.utilizationList = response.data.utilizations;
          $scope.ctrl.commercialOffersList = response.data.commercialOffers;
          $scope.ctrl.countryList = response.data.countries;
          $scope.buildKeySets();
          
        });
        
      };
   

   $scope.buildKeySets = function() {
    $scope.ctrl.countrySelectionList = [];
    _.forEach($scope.ctrl.countryList, function(value) {
        $scope.ctrl.countrySelectionList.push({
        label: value.name,
        id: value.idCountry
      });
    });

    $scope.ctrl.utilizationSelectionList = [];
    _.forEach($scope.ctrl.utilizationList, function(value) {
        $scope.ctrl.utilizationSelectionList.push({
        label: value.name,
        id: value.idUtilizationType
      });
    });

    $scope.ctrl.offerSelectionList = [];
    _.forEach($scope.ctrl.commercialOffersList, function(value) {
        $scope.ctrl.offerSelectionList.push({
        label: value.offering,
        id: value.offering
      });
    });
  };

  $scope.apply = function(request)	{
    var periodFrom, yearFrom, periodTo, yearTo;

    if ((request.selectedPeriodFrom)) {
      var from = request.selectedPeriodFrom.split('-');
      periodFrom = parseInt(from[0], 10);
      yearFrom = parseInt(from[1], 10);
    }

    if (request.selectedPeriodTo) {
      var to = request.selectedPeriodTo.split('-');
      periodTo = parseInt(to[0], 10);
      yearTo = parseInt(to[1], 10);
    }

    var parameters = {
      selectedClient: request.selectedClient,
      dsp: $scope.getCollectionValues(request.selectedDspModel),
      countryList: $scope.getCollectionValues(request.selectedCountryModel),
      utilizationList: $scope.getCollectionValues(request.selectedUtilizationModel),
      offerList: $scope.getCollectionValues(request.selectedOfferModel),
      monthFrom: periodFrom,
      yearFrom: yearFrom,
      monthTo: periodTo,
      yearTo: yearTo,
      hideForm: false
    };
    $scope.invoiceApply(parameters);
  };

  $scope.invoiceApply = function (parameters) {
    $scope.sortBy = undefined;
    angular.forEach(parameters, function (value, key) {
        $scope.ctrl.filterParameters[key] = value;
    });
    $scope.getCcid(0, 50, $scope.ctrl.filterParameters)
};

  $scope.getCollectionValues = function(collection) {
    var selectedList = [];
    if (collection !== 'undefined') {
      for (var e in collection) {
        selectedList.push(collection[e].id);
      }
    }
    return selectedList;
  };

  

  

    $scope.asc = true;

    $scope.anagClientData = angular.copy(anagClientData);

    $scope.filterDsp = anagClientService.filterDsp;
    

    $scope.selectedItemsEqualNumberOfRows = selectedItemsEqualNumberOfRows;

    $scope.navigateToNextPage = function () {
        $scope.ctrl.first = $scope.ctrl.last + 1;
        $scope.ctrl.last += $scope.ctrl.maxrows;
        $scope.getPagedResults(
            $scope.ctrl.first,
            $scope.ctrl.last,
            $scope.ctrl.filterParameters
        );
    };

    $scope.navigateToPreviousPage = function () {
        $scope.ctrl.last = $scope.ctrl.first - 1;
        $scope.ctrl.first -= ($scope.ctrl.maxrows);
        if ($scope.ctrl.first === 1) {
            $scope.ctrl.first = 0;
        }
        $scope.getPagedResults(
            $scope.ctrl.first,
            $scope.ctrl.last,
            $scope.ctrl.filterParameters
        );
    };

    $scope.hasNext = function () {
        return $scope.ctrl.data.hasNext;
    };

    $scope.hasPrev = function () {
        return $scope.ctrl.data.hasPrev;
    };

    $scope.getFirst = function () {
        if ($scope.ctrl.first === 0) {
            return $scope.ctrl.first + 1;
        }
        return $scope.ctrl.first;
    };

    function selectedItemsEqualNumberOfRows() {
        return $scope.ctrl.selectedItems &&
            $scope.ctrl.dataCcid &&
            $scope.ctrl.dataCcid.rows &&
            Object.values($scope.ctrl.selectedItems).length === $scope.ctrl.dataCcid.rows.length;
    };

    function uncheckAllSelectedAndGetCcid() {
        
        // if($scope.allSelected)
        $scope.allSelected = false;
        $scope.getCcid($scope.ctrl.ccidFirst, $scope.ctrl.ccidLast, $scope.ctrl.filterParameters);
    };

    $scope.navigateToNextCcidPage = function () {
        
        $scope.ctrl.ccidFirst = $scope.ctrl.ccidLast + 1;
        $scope.ctrl.ccidLast += $scope.ctrl.ccidMaxrows;
        uncheckAllSelectedAndGetCcid();
    };

    $scope.navigateToPreviousCcidPage = function () {
        
        $scope.ctrl.ccidLast = $scope.ctrl.ccidFirst - 1;
        $scope.ctrl.ccidFirst -= ($scope.ctrl.ccidMaxrows);
        if ($scope.ctrl.ccidFirst === 1) {
            $scope.ctrl.ccidFirst = 0;
        }
        uncheckAllSelectedAndGetCcid();
    };

    $scope.getFirstCcid = function () {
        if ($scope.ctrl.ccidFirst === 0) {
            return $scope.ctrl.ccidFirst + 1;
        }
        return $scope.ctrl.ccidFirst;
    };

    $scope.hasNextCcid = function () {
        if (!$scope.ctrl.dataCcid) {
            return false;
        }
        return $scope.ctrl.dataCcid.hasNext;
    };


    $scope.hasPrevCcid = function () {
        if (!$scope.ctrl.dataCcid) {
            return false;
        }
        return $scope.ctrl.dataCcid.hasPrev;
    };


    // Funzione principale richiamata al caricamento della pagina
    //$scope.onRefresh = function () {
      //  $scope.getPagedResults(0, $scope.ctrl.maxrows);
    //};

    
    $scope.getPagedResults = function (page, parameters) {
        var result = {};
            

            $scope.allSelected = false;
           
        return result;
    };

    differentCurrencies = function (values) {
        var valueArr = _.values(values).map(function (item) {
            return item.currency;
        });
        var value = '';
        var differentCurrency = valueArr.some(function (item, index, array) {
            var tmp = item;
            if (value === '') {
                value = tmp;
            }
            return value !== tmp;
        });
        return differentCurrency;
    };


    // TODO Lista ccid reclami associati
    $scope.showCcidReclamiInfo = function (advancePayment) {
            var rootScope = $scope.ctrl;
            ngDialog.open({
                template: 'pages/advance-payment/dialog-show-ccid-info.html',
                plain: false,
                width: '60%',
                controller: ['$scope', 'ngDialog', '$http', function (scope, dialog, http) {
                    http({
                        method: 'POST',
                        url: rootScope.msInvoiceApiUrl + 'importiAnticipi/getLinkedCcid',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        data: {
                            idImportoAnticipo: advancePayment.idImportoAnticipo,
                            idDSP: advancePayment.idDSP,
                            periodFrom: advancePayment.periodFrom,
                            periodTo: advancePayment.periodTo,
                            invoiceNumber: advancePayment.invoiceNumber,
                            originalAmount: advancePayment.originalAmount,
                            amountUsed: advancePayment.amountUsed
                        }
                    }).then(function successCallback(response) {
                        scope.ctrl.items = response.data;
                    }, function errorCallback(response) {
                        scope.ctrl.items = [];
                        UtilService.handleHttpRejection(response);

                    });
                    scope.ctrl = {
                        item: [],
                        rScope: rootScope
                    };
                    scope.formatNumber = function (number) {
                        return twoDecimalRound(number);
                    };

                    scope.cancel = function () {
                        dialog.closeAll(false);
                    };
                }]
            })
        };



    function isObjectEmpty(obj) {
        if (Object.keys(obj).length === 0 && obj.constructor === Object) {
            return true;
        }
        return false;
    }


    $scope.formatNumber = function (number) {
        return twoDecimalRound(number);
    };

    $scope.filterApply = function (parameters) {
        $scope.ctrl.filterParameters = parameters;
        $scope.getPagedResults(0,
            $scope.ctrl.maxrows,
            parameters
        );
    };

    //$scope.onRefresh();


    $scope.multiselectSettingsFilter = {
        scrollableHeight: '200px',
        scrollable: false,
        enableSearch: true
    };
   


    $scope.multiselectSettings = {
        scrollableHeight: '200px',
        scrollable: false,
        enableSearch: true,
        selectionLimit: 1,
        showUncheckAll: false,
        searchBr: true,
        checkboxes: false
    };

    $scope.invoiceApply = function (parameters) {
        $scope.sortBy = undefined;
        angular.forEach(parameters, function (value, key) {
            $scope.ctrl.filterParameters[key] = value;
        });
        $scope.getCcid(0, 50, $scope.ctrl.filterParameters)
    };

    $scope.sort = function (field) {
        
        $scope.sortBy = field;
        $scope.asc = !$scope.asc;
        $scope.getCcid($scope.ctrl.ccidFirst, $scope.ctrl.ccidLast, $scope.ctrl.filterParameters)
    };

    function sumValoreResiduoCcidSelected(map) {
        var result = 0;
        Object.keys(map).forEach(function (key) {
            result += map[key].valoreResiduo;
        });
        return result;
    }

    function reassign(dataCcid) {
        $scope.ctrl.advancePayment = dataCcid.idAdvancePayment ? dataCcid.idAdvancePayment : void 0;

        angular.forEach($scope.ctrl.dataCcid.rows, function (oldData, oldKey) {
            $scope.ctrl.dataCcid.rows[oldKey].invoiceAmountNew = 0;
            angular.forEach(dataCcid.ccidList, function (updatedData, newKey) {
                if (oldData.idCCIDmetadata === updatedData.idCCIDmetadata) {
                    $scope.ctrl.dataCcid.rows[oldKey] = updatedData;
                    $scope.ctrl.dataCcid.rows[oldKey].checked = true;
                }
            });
        });
    }

    // get ccid reclami
    $scope.getCcid = function (first, last, params) {
        $scope.clearMessages();
        var periodFromArr = $scope.item.periodFrom.split('-');
        var periodToArr = $scope.item.periodTo.split('-');
        var dspEdited = params.dsp ? params.dsp.map(el => el.replace('(','').replace(')','')) : [];
            $http({
                method: 'POST',
                url: $scope.ctrl.msInvoiceApiUrl + 'invoice/all/ccidReclami',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    first: first,
                    last: last,
                    clientId: params.selectedClient.idAnagClient,
                    dspList: dspEdited ? dspEdited : [],
                    countryList: (params.countryList ? params.countryList : []),
                    utilizationList: (params.utilizationList ? params.utilizationList : []),
                    offerList: (params.offerList ? params.offerList : []),
                    monthFrom: (params.monthFrom ? params.monthFrom :  periodFromArr[0] ),
                    yearFrom: (params.yearFrom ? params.yearFrom :  periodFromArr[1] ),
                    monthTo: (params.monthTo ? params.monthTo : periodToArr[0] ),
                    yearTo: (params.yearTo ? params.yearTo : periodToArr[1] ),
                    fatturato: params.fatturato,
                    sortBy: $scope.sortBy ? $scope.sortBy : 'idDsr',
                    asc: $scope.asc
                }
            }).then(function successCallback(response) {
                $scope.ctrl.dataCcid = response.data;
                console.log($scope.ctrl.dataCcid);
                $scope.toggleCCID();
            }, function errorCallback(response) {
                $scope.ctrl.dataCcid = [];
            });
        
    };


    $scope.selectedCheckbox = {};
    // $scope.cCID = [];

    $scope.toggleCCID = function () {
        $scope.clearMessages();
        if (!($scope.cCID && $scope.cCID.list)) {
            $scope.cCID = {list: []};
        }
        $scope.cCID.total = 0;
        for (var _i = 0, _a = Object.values($scope.ctrl.selectedItems); _i < _a.length; _i++) {
            var value = _a[_i];
            if (value.checked)
                $scope.cCID.total += value.invoiceAmount;
        }
        if (Object.values($scope.ctrl.selectedItems).length !== 0) {

            if ($scope.pagina === $scope.CHIUDI) {
                $http({
                    method: 'POST',
                    url: $scope.ctrl.msInvoiceApiUrl + 'importiAnticipi/chiudi-anticipo-fake',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: {
                        idAdvancePayment: $scope.ctrl.advancePayment,
                        ccidList: Object.values($scope.ctrl.selectedItems)
                    }
                }).then(function successCallback(response) {
                    reassign(response.data);
                });
            }
        } else {
            angular.forEach($scope.ctrl.dataCcid.rows, function (oldData, oldKey) {
                $scope.ctrl.dataCcid.rows[oldKey].invoiceAmountNew = 0;
            });
        }
    };

    function addItem(item) {
        $scope.ctrl.advancePayment.totalSelected += item.valoreResiduo;
        $scope.ctrl.selectedItems[item.idCCIDmetadata] = item;
        if ($scope.ctrl.advancePayment.amountUsed - $scope.ctrl.advancePayment.totalSelected < 0) {
            UtilService.showAlert('Info', "il Ccid selezionato supera l'importo residuo dell'anticipo,per la quota parte del residuo fatturabile si dovrà produrre una fattura");
            return
        }
    };

    function removeItem(item) {
        delete $scope.ctrl.selectedItems[item.idCCIDmetadata];
        $scope.ctrl.advancePayment.totalSelected -= item.valoreResiduo;
    };

    function addOrRemoveSelectedItem(item) {
        if (!$scope.ctrl.selectedItems[item.idCCIDmetadata]) {
            addItem(item);
        } else {
            removeItem(item);
        }
    };

    $scope.toggleItem = function (item) {
        if (!$scope.ctrl.dataCcid || !$scope.ctrl.dataCcid.rows) {
            $scope.allSelected = false;
            return;
        }
        if (item === true) {
            // seleziona tutti gli elementi
            var tvalue = 0;
            for (var i = 0; i < $scope.ctrl.dataCcid.rows.length; i++) {
                tvalue = tvalue + $scope.ctrl.dataCcid.rows[i].valoreResiduo;
                if (tvalue > $scope.ctrl.advancePayment.amountUsed) {
                    UtilService.showAlert('Errore', "Impossibile consumare l'intera lista dei CCID,il totale dei valori fatturabili supera il valore residuo dell'anticipo.");
                    return;
                }
                $scope.ctrl.dataCcid.rows[i].checked = true;
                addItem($scope.ctrl.dataCcid.rows[i]);
            }
        } else if (item === false) {
            // deseleziona tutti gli elementi
            for (var i = 0; i < $scope.ctrl.dataCcid.rows.length; i++) {
                $scope.ctrl.dataCcid.rows[i].checked = false;
                removeItem($scope.ctrl.dataCcid.rows[i]);
            }
        } else {
            // aggiungi o rimuovi elemento in base alla sua presenza o meno
            // nella lista
            addOrRemoveSelectedItem(item);
        }
        // setMainCheckbox();
    };


    differentCurrencies = function (values) {
        var valueArr = _.values(values).map(function (item) {
            return item.currency;
        });
        var value = '';
        var differentCurrency = valueArr.some(function (item, index, array) {
            var tmp = item;
            if (value === '') {
                value = tmp;
            }
            return value !== tmp;
        });
        return differentCurrency;
    };

    $scope.selectionIsEmpty = function () {
        if ($scope.ctrl && $scope.ctrl.selectedItems) {
            return !Object.keys($scope.ctrl.selectedItems).length;
        }
    };


    $scope.cancel = function () {
        // $scope.ctrl.hideEditing = false
        $scope.ctrl.data = [];
        if ($scope.ctrl.dataCcid)
            $scope.ctrl.dataCcid.rows = [];
        $scope.ctrl.advancePayment = undefined;
        $scope.ctrl.selectedItems = [];
        $scope.selectedCheckbox = {};
        angular.forEach($scope.ctrl.filterParameters, function (value, key) {
            if (key !== 'selectedClient')
                $scope.ctrl.filterParameters[key] = undefined;
        });
        $scope.getPagedResults(0,
            $scope.ctrl.maxrows,
            $scope.ctrl.filterParameters
        );
    };

    isObjectEmpty = function (obj) {
        if (Object.keys(obj).length === 0 && obj.constructor === Object) {
            return true;
        }

        return false;
    };


    extractItemAnnotationData = function (item) {
        var periodPrefix = '';
        if (item.periodType === 'quarter') {
            periodPrefix = 'Q';
        }
        return item.utilizationType + ' ' + item.year + ' ' + periodPrefix + item.period;
    };

    $scope.openModal = function(item){
        $scope.clearMessages();
        if(!$scope.item.percentuale){
            $scope.item.percentuale = "0";
        }  
        var itemOuter = $scope.item;
        
        return ngDialog.open({ 
            template : 'pages/advance-payment/dialog-crea-accantonamento.html',
            plain : false,
            width : '50%',
            controller : [ '$scope', 'ngDialog', 'accantonamentoService', function($scope, ngDialog, accantonamentoService) {
                var ngDialogId = $scope.ngDialogId;
                $scope.errorMessage ="";
                $scope.ctrl = {
                    ngDialogId : ngDialogId
                };

                $scope.item = item;
                if (item.importoAccantonato != null) {
                    $scope.tipo = "Modifica";
                } else {
                    $scope.tipo = "Crea nuovo";
                }

                $scope.saveAccantona = function() {
                    $scope.errorMessage ="";
                    accantonamentoService.saveAccantona($scope.item).then(function success(result){
                        itemOuter.importoAccantonatoUtilizzabile = result.data.importoAccantonatoUtilizzabile;
                        itemOuter.amountUsed = result.data.amountUsed;
                        $window.localStorage.setItem('item',JSON.stringify($scope.item));
                        ngDialog.close(ngDialogId);
                    }, function error(error) {
                        if (error.status != 200) {
                            $scope.errorMessage =error.data.errorMessage;
                            $route.reload();

                        }  
                    });    
                };

               
                $scope.cancel = function() {
                    ngDialog.close(ngDialogId);
                    $route.reload();
                };
                  
            }]
        })
    }

    $scope.addCcidReclamo = function(fattura) {
        $scope.clearMessages();      
        if ($scope.ctrl && $scope.ctrl.selectedItems && Object.keys($scope.ctrl.selectedItems).length > 0) {
        accantonamentoService.addCcidReclamo(fattura, $scope.ctrl.selectedItems).then(function success(result){
            $scope.refreshData($scope.item);                  
            $('#rightPanel')[0].scrollIntoView( true );
            window.scrollBy(0, -100);
        }, function error(error){
            if (error.status != 200) {
                $scope.errorMessage(error.data.message);
                $('#rightPanel')[0].scrollIntoView( true );
                window.scrollBy(0, -100);
            }

        }); }
        
    };
    

    $scope.refreshData = function(item){
        var anticipiList = {};
        accantonamentoService.getResults(0, $scope.ctrl.filterParameters).then(function success(result){
            anticipiList = result.data;
            $scope.item = anticipiList.rows.find(element => element.invoiceNumber == item.invoiceNumber);
            $window.localStorage.setItem('item',JSON.stringify($scope.item));
            $route.reload();
        }, function error(error){

        });      
    };



    $scope.goToAnticipi = function(ctrl) {
        returnAdvancePayment.context = ctrl;
        $scope.goTo('/advancePayment','');
    };
    
    $scope.goTo = function (url, param) {
        if (url != undefined && param != undefined) {
            $location.path(url + param);
        }
    };

    $scope.messages = {
        info: "", 
        error: ""
    }
   
    $scope.clearMessages = function() {
        $scope.messages = {
            info: "", 
            error: ""
        }
    }

    $scope.infoMessage = function(message) {
        $scope.messages.info = message;
    }

    $scope.errorMessage = function(message) {
        $scope.messages.error = message;
    }

    $scope.init = function(params){
        var dsp = [];
        if (params.selectedClient.dspList !== 'undefined') {
          for (var e in params.selectedClient.dspList) {
            dsp.push(params.selectedClient.dspList[e].idDsp);
          }
        }
        value = {
            selectedClient: params.selectedClient,
            dsp: dsp
        }
        $scope.getCcid($scope.ctrl.ccidFirst|| 0, $scope.ctrl.ccidLast || 50, value);
    };

    $scope.init($scope.ctrl.filterParameters);

   
    
    $scope.clearMessages();
}]
);

codmanApp.service('accantonamentoService', ['$http','$q', 'configService', '$location', function($http,$q, configService, $location){

    var service = this;

    service.msInvoiceApiUrl = `${$location.protocol()}://${$location.host()}:${$location.port()}/${configService.msInvoiceApiPath}`;


	service.saveAccantona = function (fattura) {
		return $http({
			method: 'POST',
			url: service.msInvoiceApiUrl + 'importiAnticipi/addInvoiceAccantonamento' ,
			headers: {
                'Content-Type': 'application/json'
              },
            data: fattura
		});
    }

    service.addCcidReclamo = function(fattura, ccid){
        return $http({
			method: 'POST',
			url: service.msInvoiceApiUrl + 'importiAnticipi/useReclamoPayment' ,
			headers: {
                'Content-Type': 'application/json'
              },
              data: {
                idAdvancePayment: fattura,
                ccidList: Object.values(ccid)
            }
		});

    }

    service.getResults = function(page, parameters){
        return $http({
            method: 'POST',
            url: service.msInvoiceApiUrl + 'importiAnticipi/getAdvancePayment',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                currentPage: page  || 0,
                dsp: parameters.dsp ? parameters.dsp.idDsp : void 0,
                idClient: parameters.selectedClient.idAnagClient,
                advancePaymentCode: parameters.advancePaymentCode? parameters.advancePaymentCode: void 0,
                showStandBy: parameters.showStandBy,
                hideForm: false
            }
        }); 
        
    };



}]);