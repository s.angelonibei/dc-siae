codmanApp.controller('unidentifiedLoadingCtrl', ['$scope', 'unidentifiedLoadingService', 'dspsUtilizationsCommercialOffersCountries', 'statuses',
	function ($scope, unidentifiedLoadingService, dspsUtilizationsCommercialOffersCountries, statuses) {

		$scope.multiselectSettings = {
			scrollableHeight: '200px',
			scrollable: false,
			enableSearch: true,
			displayProp: 'description',
			smartButtonMaxItems: 2
		};

		$scope.ctrl = {
			pageSize: 75,
			request: unidentifiedLoadingService.emptySearchRequest(),
			dsps: dspsUtilizationsCommercialOffersCountries.dsp.slice(0),
			commercialOffers: dspsUtilizationsCommercialOffersCountries.commercialOffers.slice(0),
			countries: dspsUtilizationsCommercialOffersCountries.countries.slice(0),
			statuses: statuses.data.statuses,
			data: {
				selectedDsps: [],
				selectedCountries: [],
				selectedCommercialOffers: [],
				selectedStatuses: [],
				selectedCodificatoStatuses: [],
				selectIdDsr: void 0,
				results: []
			},
			dspSettings: { ...$scope.multiselectSettings, idProp: 'idDsp' },
			countrySettings: { ...$scope.multiselectSettings, idProp: 'idCountry', displayProp: 'name' },
			commercialOfferSettings: { ...$scope.multiselectSettings, idProp: 'offering', displayProp: 'offering' },
			pageTitle: `Caricamento Non identificato e codificato`,
			messages: {
				info: "",
				error: ""
			}
		};

		$scope.clearMessages = function () {
			$scope.ctrl.messages = {
				info: "",
				error: ""
			}
		};

		$scope.infoMessage = function (message) {
			setTimeout(() => {
				$scope.ctrl.messages.info = message;
				$scope.$apply();
			}, 100);
		};

		$scope.errorMessage = function (message) {
			setTimeout(() => {
				$scope.ctrl.messages.error = message;
				$scope.$apply();
			}, 100);
		};



		$scope.dspEvents = {
			onItemSelect: () => {
				if ($scope.ctrl.data.selectedDsps.length == 0)
					$scope.ctrl.commercialOffers = dspsUtilizationsCommercialOffersCountries.commercialOffers.slice(0);
				else
					$scope.ctrl.commercialOffers = dspsUtilizationsCommercialOffersCountries.commercialOffers.filter(offer => $scope.ctrl.data.selectedDsps.findIndex(s => s.id == offer.idDSP) != -1);
			},
			onItemDeselect: (item) => {
				if ($scope.ctrl.data.selectedDsps.length == 0) {
					$scope.ctrl.commercialOffers = dspsUtilizationsCommercialOffersCountries.commercialOffers.slice(0);
				} else {
					$scope.ctrl.commercialOffers = dspsUtilizationsCommercialOffersCountries.commercialOffers.filter(offer => $scope.ctrl.data.selectedDsps.findIndex(s => s.id == offer.idDSP) != -1);
				}
			},
			onSelectAll: function () {
				$scope.ctrl.data.selectedDsps = dspsUtilizationsCommercialOffersCountries.dsp.slice(0).map(dsp => { return { 'id': dsp.idDsp } });
			},
			onDeselectAll: function () {
				$scope.ctrl.data.selectedDsps = [];
				$scope.ctrl.commercialOffers = dspsUtilizationsCommercialOffersCountries.commercialOffers.slice(0);
			}
		};

		$scope.formatPeriod = function (dsrPeriod) {
			if (dsrPeriod) {
				if (dsrPeriod.periodType == 'month') {
					return `${['Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov', 'Dic'][dsrPeriod.period - 1]} ${dsrPeriod.year}`;
				} else {
					return `${['Gen - Mar', 'Apr - Giu', 'Lug - Set', 'Ott - Dic'][dsrPeriod.period - 1]} ${dsrPeriod.year}`;
				}
			}
		};

		$scope.sortedSearch = function (name, param) {

			//get id of html element clicked
			var id = "#" + name;

			//get css class of that html element for change the image of arrows
			var classe = angular.element(id).attr("class");

			//set to original position all images of arrows, on each columns.
			var element1 = document.getElementsByClassName("glyphicon glyphicon-sort-by-attributes");
			var element2 = document.getElementsByClassName("glyphicon glyphicon-sort-by-attributes-alt");
			angular.element(element1).attr("class", "glyphicon glyphicon-sort");
			angular.element(element2).attr("class", "glyphicon glyphicon-sort");

			if (classe != "glyphicon glyphicon-sort-by-attributes" || classe == "glyphicon glyphicon-sort") {
				angular.element(id).attr("class", "glyphicon glyphicon-sort-by-attributes");
			} else {
				angular.element(id).attr("class", "glyphicon glyphicon-sort-by-attributes-alt");
			}

			$scope.ctrl.request.sortBy = `${param}`;

			if ($scope.ctrl.request.order == "DESC") {
				$scope.ctrl.request.order = "ASC";
			} else {
				$scope.ctrl.request.order = "DESC";
			}

			$scope.searchWithCachedFilters();

		};

		$scope.searchWithCachedFilters = function () {
			unidentifiedLoadingService.search($scope.ctrl.request).then(function successCallback(response) {
				$scope.ctrl.data.results = response.data;
			}).catch(function (e) {
				$scope.errorMessage('Operazione non riuscita');
			});
		};

		$scope.navigateToPreviousPage = function () {
			$scope.ctrl.request.first = Math.max($scope.ctrl.request.first - $scope.ctrl.pageSize, 0);
			$scope.ctrl.request.last = $scope.ctrl.request.last - $scope.ctrl.pageSize;
			$scope.searchWithCachedFilters();
		};

		$scope.navigateToNextPage = function () {
			$scope.ctrl.request.first = $scope.ctrl.request.first + $scope.ctrl.pageSize;
			$scope.ctrl.request.last = $scope.ctrl.request.last + $scope.ctrl.pageSize;
			$scope.searchWithCachedFilters();
		};

		$scope.navigateToEndPage = function () {
			$scope.ctrl.request.first = $scope.ctrl.data.results.maxrows - $scope.ctrl.pageSize;
			$scope.ctrl.request.last = $scope.ctrl.data.results.maxrows;
			$scope.searchWithCachedFilters();
		};

		$scope.search = function () {
			var monthFrom = 0;
			var yearFrom = 0;
			var monthTo = 0;
			var yearTo = 0;

			if ($scope.ctrl.data.selectedPeriodFrom) {
				var from = $scope.ctrl.data.selectedPeriodFrom.split('-');
				monthFrom = parseInt(from[0], 10);
				yearFrom = parseInt(from[1], 10);
			}
			if ($scope.ctrl.data.selectedPeriodTo) {
				var from = $scope.ctrl.data.selectedPeriodTo.split('-');
				monthTo = parseInt(from[0], 10);
				yearTo = parseInt(from[1], 10);
			}

			$scope.ctrl.request = {
				...$scope.ctrl.request,
				...{
					dspList: $scope.ctrl.data.selectedDsps && $scope.ctrl.data.selectedDsps.map(item => item.id) || [],
					countryList: $scope.ctrl.data.selectedCountries && $scope.ctrl.data.selectedCountries.map(item => item.id) || [],
					offerList: $scope.ctrl.data.selectedCommercialOffers && $scope.ctrl.data.selectedCommercialOffers.map(item => item.id) || [],
					dsr: $scope.ctrl.data.selectIdDsr && $scope.ctrl.data.selectIdDsr.originalObject.name || '',
					monthFrom: monthFrom,
					yearFrom: yearFrom,
					monthTo: monthTo,
					yearTo: yearTo,
					statuses: $scope.ctrl.data.selectedStatuses,
					codificatoStatuses: $scope.ctrl.data.selectedCodificatoStatuses
				}
			};

			const filterApplied = Object
				.entries(_.pick($scope.ctrl.request, ['dspList', 'countryList', 'dsr', 'offerList', 'monthFrom', 'yearFrom', 'monthTo', 'yearTo', 'statuses', 'codificatoStatuses']))
				.find(([_k, v]) => v.length);
			if(filterApplied){
				unidentifiedLoadingService.search($scope.ctrl.request)
					.then(function successCallback(response) {
					$scope.ctrl.data.results = response.data;
				}).catch(function (e) {
					$scope.errorMessage('Operazione non riuscita');
				});
			} else {
				$scope.infoMessage("Selezionare almeno un filtro")
			}
		};

		$scope.submitLoadingRequest = function (item) {
			unidentifiedLoadingService.submitLoadingRequest(
				{
					dsrs: [item]
				}).then(
					function success(response) {
						$scope.infoMessage(response.data.message);
					},
					function error(error) {
						$scope.errorMessage(error);
					}
				);
		};

		$scope.submitCodificatoRequest = function (item) {
			unidentifiedLoadingService.submitCodificatoRequest({ dsrs: [item] }).then(response => {
				$scope.infoMessage(response.data.message);
			}, error => {
				$scope.errorMessage(error);
			});
		};

		$scope.clearUnidentified = function () {
			unidentifiedLoadingService.clearUnidentified().then(
				function success(response) {
					$scope.infoMessage(response.data.message);
				},
				function error(error) {
					$scope.errorMessage(error);
				}
			);
		};

		$scope.clearIdentified = function () {
			unidentifiedLoadingService.clearIdentified()
				.then(response => {
					$scope.infoMessage(response.data.message);
				}, error => {
					$scope.errorMessage(error);
				});
		};

	}]);

codmanApp.service('unidentifiedLoadingService', ['$http', '$q', 'configService', function ($http, $q, configService) {

	var service = this;

	service.statuses = function (request) {
		if (!request) request = this.emptySearchRequest();
		return $http({
			method: 'GET',
			url: 'rest/unidentified-loading/statuses'
		});
	};

	service.search = function (request) {
		if (!request) request = this.emptySearchRequest();
		return $http({
			method: 'POST',
			url: 'rest/unidentified-loading/search',
			data: request
		});
	};

	service.submitLoadingRequest = function (request) {
		return $http({
			method: 'POST',
			url: 'rest/unidentified-loading/submit-request',
			data: request
		});
	};

	service.submitCodificatoRequest = function (request) {
		return $http({
			method: 'POST',
			url: 'rest/identified-loading/submit-load-identified',
			data: request
		});
	};

	service.clearIdentified = function (request) {
		return $http({
			method: 'POST',
			url: 'rest/identified-loading/clear-load-identified'
		});
	};


	service.clearUnidentified = function (request) {
		return $http({
			method: 'POST',
			url: 'rest/unidentified-loading/clear-unidentified'
		});
	};

	service.emptySearchRequest = function (pageSize) {
		// var currentTime = new Date();
		return {
			dspList: [],
			countryList: [],
			offerList: [],
			// month: currentTime.getMonth() + 1,
			// year: currentTime.getFullYear(),
			// statuses: [{ id: 1, description: 'Arrivato' }],
			first: 0,
			last: pageSize || configService.maxRowsPerPage,
			sortBy: 'dsr',
			order: 'DESC'
		};
	};
}
]);