(function () {
  'use strict';
  angular
    .module('codmanApp')
    .controller('AnagClientCtrl', ['$scope', '$route', 'ngDialog', '$routeParams', '$location', '$http', 'configService', 'anagClientData', 'anagClientService',
      function ($scope, $route, ngDialog, $routeParams, $location, $http, configService, anagClientData, anagClientService) {
        $scope.anagClientData = angular.copy(anagClientData);
        $scope.ctrl = {
          navbarTab: 'anagClient',
          rows: [],
          filterParameters: {},
          execute: $routeParams.execute,
          maxrows: 20,
          first: $routeParams.first || 0,
          last: $routeParams.last || 20,
          sortType: 'priority',
          sortReverse: true,
          msInvoiceApiUrl: `${$location.protocol()}://${$location.host()}:${$location.port()}/${configService.msInvoiceApiPath}`
        };

        $scope.filterDsp = anagClientService.filterDsp;

        $scope.navigateToNextPage = function () {
          $scope.getPagedResults($scope.ctrl.last, $scope.ctrl.last + $scope.ctrl.maxrows,
            $scope.ctrl.filterParameters.client,
            $scope.ctrl.filterParameters.dsp,
            $scope.ctrl.filterParameters.country,
            $scope.ctrl.filterParameters.sapCode);
        };

        $scope.navigateToPreviousPage = function () {
          $scope.getPagedResults(Math.max($scope.ctrl.first - $scope.ctrl.maxrows, 0), $scope.ctrl.first,
            $scope.ctrl.filterParameters.client,
            $scope.ctrl.filterParameters.dsp,
            $scope.ctrl.filterParameters.country,
            $scope.ctrl.filterParameters.sapCode);
        };

        $scope.onRefresh = function () {
          $scope.getPagedResults(0,
            $scope.ctrl.maxrows);
        };

        $scope.getPagedResults = function (first, last, client, dsp, country, sapCode) {
          $http({
            method: 'GET',
            url: $scope.ctrl.msInvoiceApiUrl + 'anagClient/all',
            params: {
              first: first,
              last: last,
              clientId: (typeof client !== 'undefined' ? client : ''),
              dsp: (typeof dsp !== 'undefined' ? dsp : ''),
              country: (typeof country !== 'undefined' ? country : ''),
              sapCode: (typeof sapCode !== 'undefined' ? sapCode : '')
            }
          }).then(function successCallback(response) {
            $scope.ctrl.rows = response.data.rows;
            $scope.ctrl.first = response.data.first;
            $scope.ctrl.last = response.data.last;
            $scope.ctrl.hasNext = response.data.hasNext;
            $scope.ctrl.hasPrev = response.data.hasPrev;
          }, function errorCallback(response) {
            $scope.ctrl.rows = [];
          });
        };


        // funzione che renderizza il popup per l'aggiunta di una nuova configurazione
        $scope.showNewConfig = function () {
          ngDialog.open({
              template: 'pages/anag-client/dialog-new.html',
              plain: false,
              width: '60%',
              scope: $scope,
              controllerAs: 'vm',
              controller: ['$scope', 'ngDialog', '$http', function (scope, dialog, http) {
                var vm = this;
                // scope.ctrl.msInvoiceApiUrl =`${$location.protocol()}://${$location.host()}:${$location.port()}/${configService.msInvoiceApiPath}`
                vm.anagClientData = angular.copy(anagClientData);
                vm.values = vm.anagClientData[1];
                vm.values.anagClient = vm.anagClientData[0];
                vm.licence = {};
                vm.licence.licences = [];
                vm.hideLicenceList = true;
                vm.filterDsp = filterDsp;
                /*var promise1 = http({method: 'GET', url: 'rest/data/countries'});
                var promise2 = http({method: 'GET', url: 'rest/data/dspList'});
                $q.all([promise1, promise2]).then(function successCallback(response) {
                  scope.ctrl.countryList = response[0].data;
                  scope.ctrl.dspList = response[1].data;
                }, function errorCallback(response) {
                  scope.ctrl.dspList = [];
                  scope.ctrl.countryList = [];
                });*/

                function filterDsp(b) {
                  var a = !0;
                  (function() {
                    vm.selectedClient && vm.selectedClient.dspList.forEach(function(c) {
                      a = c.idDsp !== b.idDsp;
                    });
                  })();
                  return a;
                }

                vm.openErrorDialog = function (errorMsg) {
                  dialog.open({
                    template: 'errorDialogId',
                    plain: false,
                    width: '40%',
                    data: errorMsg

                  });
                };

                vm.cancel = function () {
                  dialog.closeAll(false);
                };

                vm.hideLicList = function (val) {
                  vm.hideLicenceList = val;
                  vm.automaticRenew = false;
                  vm.selectedPeriodFrom = undefined;
                  vm.selectedPeriodTo = undefined;
                  vm.licenceCode = undefined;
                };
                vm.addClient = function () {
                  var params = {
                    licenceCode: vm.licenceCode,
                    selectedPeriodFrom: vm.selectedPeriodFrom,
                    selectedPeriodTo: vm.selectedPeriodTo,
                    automaticRenew: vm.automaticRenew,
                    codeInvalid: vm.codeInvalid,
                    fromInvalid: vm.fromInvalid,
                    toInvalid: vm.toInvalid,
                    hideLicenceList: vm.hideLicenceList
                  };

                  scope.confirmLicence(params, vm.licence.licences);
                  vm.licenceCode = params.licenceCode;
                  vm.selectedPeriodFrom = params.selectedPeriodFrom;
                  vm.selectedPeriodTo = params.selectedPeriodTo;
                  vm.codeInvalid = params.codeInvalid;
                  vm.fromInvalid = params.fromInvalid;
                  vm.toInvalid = params.toInvalid;
                  vm.automaticRenew = params.automaticRenew;
                  vm.hideLicenceList = params.hideLicenceList;
                };
                vm.eraseLicence = function (index) {
                  vm.licence.licences = vm.removeLicence(index, vm.licence.licences);
                };

                vm.setAutomaticRenew = function () {
                  if (vm.automaticRenew === true) {
                    vm.disableTo = true;
                    return;
                  }
                  vm.disableTo = false;
                };

                vm.licencesNumber = function () {
                  if (!vm.licence.licences) {
                    return 0;
                  }
                  return vm.licence.licences.length;
                };

                vm.getLicence = function (item) {
                  return 'code: ' + item.code + ' da: ' + item.from + ' a: ' + item.to;
                };
                vm.save = function () {
                  var foreignClient = 1;

                  if (vm.selectedCountry && vm.selectedCountry.idCountry === 'ITA') {
                    foreignClient = 0;
                  }

                  var data = {
                    idDsp: vm.selectedDsp.idDsp,
                    licence: angular.toJson(vm.licence)
                  };

                  if(vm.newClient){
                    data.dspName = vm.selectedDsp.dspName;
                    data.companyName = vm.selectedProviderName;
                    data.country = vm.selectedCountry.idCountry;
                    data.code = vm.selectedCode;
                    data.vatCode = vm.selectedVatCode;
                    data.vatDescription = vm.selectedVatDescription;
                    data.foreignClient = foreignClient;
                  }else{
                    data.idAnagClient = vm.selectedClient.idAnagClient;
                  }

                  http({
                    method: 'POST',
                    url: $scope.ctrl.msInvoiceApiUrl + 'anagClient',
                    data: data
                  }).then(function successCallback(response) {
                    dialog.closeAll(true);
                    $route.reload();
                  }, function errorCallback(response) {
                    if (response.status === 409) {
                      vm.openErrorDialog('Configurazione già presente. Modificare i valori direttamente  dal menu di modifica');
                    } else {
                      vm.openErrorDialog(response.statusText);
                    }
                  });
                };
              }]
            }
          );
        };


        $scope.showDeleteConfig = function (event, configToRemove) {
          ngDialog.open({
            template: 'pages/anag-client/dialog-delete.html',
            plain: false,
            width: '60%',
            scope: $scope,
            controller: ['$scope', 'ngDialog', '$http', function (scope, dialog, http) {
              scope.ctrl.configToRemove = configToRemove;
              scope.cancel = function () {
                dialog.closeAll(false);
              };

              scope.deleteConfig = function () {
                http({
                  method: 'DELETE',
                  url: $scope.ctrl.msInvoiceApiUrl + 'anagClient',
                  headers: {
                    'Content-Type': 'application/json'
                  },
                  params: {
                    idClientDsp: scope.ctrl.configToRemove.idClientDsp
                  }

                }).then(function successCallback(response) {
                  dialog.closeAll(true);
                  $route.reload();
                }, function errorCallback(response) {
                  scope.openErrorDialog(response.statusText);
                });
              };

              scope.getLicenses = function (licenceString) {
                scope.getLicensesList(licenceString);
              };

              scope.openErrorDialog = function (errorMsg) {
                dialog.open({
                  template: 'errorDialogId',
                  plain: false,
                  width: '40%',
                  data: errorMsg

                });
              };
            }]
          });
        };


        $scope.showEditConfig = function (event, configToModify) {
          ngDialog.open({
            template: 'pages/anag-client/dialog-edit.html',
            plain: false,
            width: '60%',
            scope: $scope,
            controller: ['$scope', 'ngDialog', '$http', function (scope, dialog, http) {
              scope.ctrl.configToModify = configToModify;

              http({
                method: 'GET',
                url: 'rest/data/countries'
              }).then(function successCallback(response) {
                scope.ctrl.countries = response.data;
                scope.hideLicenceList = false;
                scope.dspName = configToModify.dspName;
                scope.code = configToModify.code;
                scope.companyName = configToModify.companyName;
                scope.country = {idCountry: configToModify.country};
                scope.vatCode = configToModify.vatCode;
                scope.vatDescription = configToModify.vatDescription;
                scope.licence = JSON.parse(configToModify.licence);
                scope.licence.licences.forEach(function (item, index) {
                  item.index = index;
                });
              }, function errorCallback(response) {
                scope.openErrorDialog('errore caricamento dati');
              });

              scope.confirm = function () {
                var params = {
                  licenceCode: scope.licenceCode,
                  selectedPeriodFrom: scope.selectedPeriodFrom,
                  selectedPeriodTo: scope.selectedPeriodTo,
                  automaticRenew: scope.automaticRenew,
                  codeInvalid: scope.codeInvalid,
                  fromInvalid: scope.fromInvalid,
                  toInvalid: scope.toInvalid,
                  hideLicenceList: scope.hideLicenceList
                };

                scope.confirmLicence(params, scope.licence.licences);
                scope.licenceCode = params.licenceCode;
                scope.selectedPeriodFrom = params.selectedPeriodFrom;
                scope.selectedPeriodTo = params.selectedPeriodTo;
                scope.codeInvalid = params.codeInvalid;
                scope.fromInvalid = params.fromInvalid;
                scope.toInvalid = params.toInvalid;
                scope.automaticRenew = params.automaticRenew;
                scope.hideLicenceList = params.hideLicenceList;
              };

              scope.cancel = function () {
                dialog.closeAll(false);
              };

              scope.getLicenceRep = function (item) {
                return 'code: ' + item.code + ' da: ' + item.from + ' a: ' + item.to;
              };

              scope.eraseLicence = function (index) {
                scope.licence.licences = scope.removeLicence(index, scope.licence.licences);
              };

              scope.hideLicList = function () {
                scope.automaticRenew = false;
                scope.selectedPeriodFrom = undefined;
                scope.selectedPeriodTo = undefined;
                scope.licenceCode = undefined;
                scope.hideLicenceList = false;
              };

              scope.addNewLicence = function () {
                scope.hideLicenceList = true;
              };

              scope.editConfig = function () {
                scope.licence.licences.forEach(function (item) {
                  delete item.index;
                });

                var foreignClientUpd = 1;

                if (scope.country && scope.country.idCountry === 'ITA') {
                  foreignClientUpd = 0;
                }

                http({
                  method: 'PUT',
                  url: $scope.ctrl.msInvoiceApiUrl + 'anagClient',
                  headers: {
                    'Content-Type': 'application/json'
                  },
                  data: {
                    prevCode: scope.ctrl.configToModify.code,
                    prevCompanyName: scope.ctrl.configToModify.companyName,
                    prevCountry: scope.ctrl.configToModify.country,
                    prevVatDescription: scope.ctrl.configToModify.vatDescription,
                    prevVatCode: scope.ctrl.configToModify.vatCode,
                    prevLicences: scope.ctrl.configToModify.licence,
                    idClientDsp: scope.ctrl.configToModify.idClientDsp,
                    idAnagClient: scope.ctrl.configToModify.idAnagClient,
                    idDsp: scope.ctrl.configToModify.idDsp,
                    code: scope.code,
                    companyName: scope.companyName,
                    country: scope.country.idCountry,
                    vatDescription: scope.vatDescription,
                    foreignClient: foreignClientUpd,
                    vatCode: scope.vatCode,
                    licence: angular.toJson(scope.licence)
                  }


                }).then(function successCallback(response) {
                  dialog.closeAll(true);
                  $route.reload();
                }, function errorCallback(response) {
                  $scope.openErrorDialog(response.statusText);
                });
              };

              scope.openErrorDialog = function (errorMsg) {
                dialog.open({
                  template: 'errorDialogId',
                  plain: false,
                  width: '40%',
                  data: errorMsg

                });
              };
            }]
          });
        };


        $scope.confirmLicence = function (payload, licences) {
          payload.codeInvalid = !payload.licenceCode;
          payload.fromInvalid = !payload.selectedPeriodFrom;
          payload.toInvalid = !payload.selectedPeriodTo && (!payload.automaticRenew || payload.automaticRenew === false);


          if (!payload.codeInvalid && !payload.fromInvalid && !payload.toInvalid) {
            var newItem = {
              index: licences.length + 1,
              code: payload.licenceCode,
              from: payload.selectedPeriodFrom,
              to: payload.automaticRenew ? 'automatic renewal' : payload.selectedPeriodTo
            };

            licences.push(newItem);
            payload.licenceCode = undefined;
            payload.selectedPeriodFrom = undefined;
            payload.selectedPeriodTo = undefined;
            payload.hideLicenceList = false;
            payload.automaticRenew = false;
            return;
          }

          payload.hideLicenceList = true;
        };

        $scope.removeLicence = function (index, licences) {
          licences = _.reject(licences, function (item) {
            return item.index === index;
          });
          licences.forEach(function (item, index) {
            item.index = index;
          });

          return licences;
        };
        // $scope.$on('filterApply', function (event, parameters) {
        $scope.filterApply = function (parameters) {
          $scope.ctrl.filterParameters = parameters;
          $scope.getPagedResults(0, $scope.ctrl.maxrows, parameters.client, parameters.dsp, parameters.country, parameters.sapCode);
        };
        // });

        $scope.onRefresh();
      }])
  ;
})();
