(function() {
  'use strict';
  angular
    .module('codmanApp')
    .controller('PerformingMonitoraggioCodificatoCtrl', PerformingMonitoraggioCodificatoCtrl);

  PerformingMonitoraggioCodificatoCtrl.$inject = ['monitoraggiCodificaService', 'RegoleRipartizioneService', 'UtilService'];

  function PerformingMonitoraggioCodificatoCtrl(monitoraggiCodificaService, RRService, UtilService) {
    var vm = this;
    vm.search = search;
    vm.init = init;
    vm.model = {};
    vm.monitoraggio = {};
    vm.periodoRipartizione = {};
    vm.getPeriodiRipartizione = getPeriodiRipartizione;

    function search() {
      monitoraggiCodificaService.getMonitoraggioCodificato(vm.model).then(function(response) {
        if (response.status === 204) {
          UtilService.showError('Attenzione', 'Non sono stati trovate valorizzazioni per il periodo selezionato');
          return false;
        }
        vm.monitoraggio = angular.fromJson(response.data.valori);
      }).catch(function(response) {
        UtilService.showError('Errore', "C'è stato un problema, riprovare in un secondo momento");
      });
    }

    function init() {
      // vm.search();
      vm.getPeriodiRipartizione();
    }

    function getPeriodiRipartizione() {
      return RRService.getPeriodiRipartizione().then(function(response) {
        vm.periodoRipartizione = response.data;
        return response.data;
      }, function() {
        UtilService.showError('Errore', 'Non è stato possibile ottenere la lista dei periodi di ripartizione');
      });
    }

    vm.init();
  }
})();
