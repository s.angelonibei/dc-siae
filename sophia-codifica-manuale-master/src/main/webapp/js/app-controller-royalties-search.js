codmanApp.controller('royaltiesSearchCtrl', ['$scope', '$route', '$interval', 'configService', '$routeParams', 'ngDialog', 'royaltiesSearchService', 'royaltiesStorages', '$q','dspsUtilizationsCommercialOffersCountries','activeRoyaltiesStorage',
	function ($scope, $route, $interval, configService, $routeParams, ngDialog, royaltiesSearchService, royaltiesStorages, $q, dspsUtilizationsCommercialOffersCountries, activeRoyaltiesStorage) {

		$scope.countries = dspsUtilizationsCommercialOffersCountries.countries;
		$scope.dsp = dspsUtilizationsCommercialOffersCountries.dsp;

		$scope.ctrl = {
			pageTitle: `Schemi di Riparto`,
			data: {
				status: royaltiesStorages.data.status,
				statusMessage: royaltiesStorages.data.statusMessage,
				royaltiesStorages: !royaltiesStorages.data.storages ? [] : royaltiesStorages.data.storages.reduce((royaltiesByYear, item) => {
					let royaltiesGroup = royaltiesByYear.find(royaltiesGroup => royaltiesGroup.year == item.year);
					if (!royaltiesGroup) {
						royaltiesByYear.push({
							year: item.year,
							months: [{month: item.month, dumps: [item]}]
						});
					} else {
						let royaltiesGroupMonth = royaltiesGroup.months.find(royaltiesByYearMonth => royaltiesByYearMonth.month === item.month);
						if (!royaltiesGroupMonth) {
							royaltiesGroup.months.push({
								month: item.month,
								dumps: [item]
							})
						} else
							royaltiesGroupMonth.dumps.push(item)
					}
					return royaltiesByYear;
				}, []),
				currentRoyalties: activeRoyaltiesStorage.data,
				royalties: []
			},
			hideRoyaltiesSelection: true,
			messages: {
				info: "",
				error: "",
				warning: "",
				clear: function () {
					this.info = "";
					this.error = "";
				},
				infoMessage: function (message) {
					this.info = message;
					// this.scrollIntoView();
				},
				errorMessage: function (message) {
					this.error = message;
					// this.scrollIntoView();
				},
				warningMessage: function (message) {
					this.warning = message;
					// this.scrollIntoView();
				},
				// scrollIntoView: function () {
				// 	$('#rightPanel')[0].scrollIntoView(true);
				// 	window.scrollBy(0, -100);
				// }
			}
		};

		(() => {
			if ($scope.ctrl.data.status != 'READY') {
				$scope.ctrl.messages.warningMessage($scope.ctrl.data.statusMessage);
				// $scope.ctrl.data.royalties = [];
			}
		})();


		$scope.requestRoyaltiesStorage = function (royaltiesStorage) {
			royaltiesSearchService.requestRoyaltiesStorage(royaltiesStorage.dumps[0]).then(
				(response) => {
					$scope.ctrl.data.status = response.data.status;
					$scope.ctrl.data.statusMessage = response.data.statusMessage;
					$scope.ctrl.messages.warningMessage($scope.ctrl.data.statusMessage);
				},
				(error) => {
					$scope.ctrl.messages.errorMessage(error);
					// $scope.ctrl.data.royalties = [];
				
				}
			);
		};

		$scope.clearSearchForm = () => {
			$scope.ctrl.data.selectedCountry = void 0;
			$scope.ctrl.data.uuid = void 0;
			$scope.ctrl.data.workCode = void 0;
			$scope.$broadcast('angucomplete-alt:clearInput');
			$scope.ctrl.data.royalties = [];
		};

		$scope.search = () => {
			royaltiesSearchService.searchRoyalties(
				{
					country: $scope.ctrl.data.selectedCountry,
					uuid: $scope.ctrl.data.uuid,
					workCode: $scope.ctrl.data.workCode,
					society: $scope.ctrl.data.society,
					anagDsp: $scope.ctrl.data.selectedDsp
				}
			).then((response) => {
				$scope.ctrl.data.status = response.data.status;
				$scope.ctrl.data.statusMessage = response.data.statusMessage;
				if (response.data.status == 'READY') {
					$scope.ctrl.data.royalties = response.data.royalties;
				} else {
					$scope.ctrl.messages.warningMessage($scope.ctrl.data.statusMessage);
					$scope.ctrl.data.royalties = [];
				}
			},
				(error) => {
					$scope.ctrl.messages.errorMessage(error.data.statusMessage);
					$scope.ctrl.data.royalties = [];
					
				}
			);
		};

	}]);

codmanApp.service('royaltiesSearchService', ['$http', '$q', function ($http, $q) {

	var service = this;

	service.searchRoyalties = function (request) {
		return $http({
			method: 'POST',
			url: 'rest/royalties-search',
			data: request,
			headers: { 'Content-Type': 'application/json' }
		});
		
	};

	service.listRoyaltiesStorages = function () {
		return $http({
			method: 'GET',
			url: 'rest/royalties-search/list-royalties-storages'
		});
	};

	service.requestRoyaltiesStorage = function (request) {
		return $http({
			method: 'POST',
			url: 'rest/royalties-search/request-royalties-storage',
			data: request,
			headers: { 'Content-Type': 'application/json' }
		});
	};

	service.activeRoyaltiesStorage = function () {
		return $http({
			method: 'GET',
			url: 'rest/royalties-search/active-royalties-storage',
		})
	}
}
]);

codmanApp.filter('itMonth', function () {
	return function (input) {
		return `${[
			'Gennaio',
			'Febbraio',
			'Marzo',
			'Aprile',
			'Maggio',
			'Giugno',
			'Luglio',
			'Agosto',
			'Settembre',
			'Ottobre',
			'Novembre',
			'Dicembre'][input - 1]}`;
	};
});