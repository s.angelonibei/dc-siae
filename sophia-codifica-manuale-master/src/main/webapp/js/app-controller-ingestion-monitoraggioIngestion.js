codmanApp.controller('MonitoraggioIngestionCtrl', ['$scope', '$routeParams', '$route', '$http', '$location', 'ngDialog', '$rootScope', '$anchorScroll', 'configService', 'triplettaContext',
	function ($scope, $routeParams, $route, $http, $location, ngDialog, $rootScope, $anchorScroll, configService, triplettaContext) {

		$scope.ctrl = { pageTitle: `Monitoraggio Ingestion SAP`,
		msIngestionApiUrl: `${$location.protocol()}://${$location.host()}:${$location.port()}/${configService.msIngestionApiPath}`
		};

		$scope.orderImage = function (name, param) {

			//get id of html element clicked
			var id = "#" + name;

			//get css class of that html element for change the image of arrows
			var classe = angular.element(id).attr("class");

			//set to original position all images of arrows, on each columns.
			var element1 = document.getElementsByClassName("glyphicon glyphicon-sort-by-attributes");
			var element2 = document.getElementsByClassName("glyphicon glyphicon-sort-by-attributes-alt");
			angular.element(element1).attr("class", "glyphicon glyphicon-sort");
			angular.element(element2).attr("class", "glyphicon glyphicon-sort");

			if (classe != "glyphicon glyphicon-sort-by-attributes" || classe == "glyphicon glyphicon-sort") {

				angular.element(id).attr("class", "glyphicon glyphicon-sort-by-attributes");

			} else {
				angular.element(id).attr("class", "glyphicon glyphicon-sort-by-attributes-alt");
			}

			$scope.order(param);

		}

		$scope.order = function (param) {
			if ($scope.orderBy == param.concat(" asc")) {
				$scope.orderBy = param.concat(" desc");
			} else {
				$scope.orderBy = param.concat(" asc");
			}
			$scope.getSampling(false);
		}

		$scope.filterApply = function () {
			$scope.ctrl.page = 0;
			$scope.getSampling(true);
		}

		$scope.goToDettaglio = function(idIngestion) {
			triplettaContext.state = (function stateIIFE() {
				let orderBy = $scope.orderBy;
				let page = $scope.ctrl.page;
				let dataAggiornamentoA = $scope.ctrl.selectedDataAggiornamentoA;
				let dataAggiornamentoDa = $scope.ctrl.selectedDataAggiornamentoDa;
				return {
					location : $location.path(),
					reset: function(scope) {
						scope.orderBy = orderBy;
						scope.ctrl.page = page;
						scope.ctrl.selectedDataAggiornamentoA = dataAggiornamentoA;
						scope.ctrl.selectedDataAggiornamentoDa = dataAggiornamentoDa;
					}
				};
			}());
			$scope.goTo('/ingestionDetail/', idIngestion);
		}

		$scope.goTo = function (url, param) {
			if (url != undefined && param != undefined) {
				$location.path(url + param);
			}
		};

		$scope.navigateToNextPage = function () {
			$scope.ctrl.page = $scope.ctrl.page + 1;
			$scope.getSampling(false);

		};

		$scope.navigateToPreviousPage = function () {
			$scope.ctrl.page = $scope.ctrl.page - 1;
			$scope.getSampling(false);

		};

		$scope.navigateToEndPage = function () {
			$scope.ctrl.page = $scope.pageTot;
			$scope.getSampling(false);
		}

		//il flag è usato per la count. Se è vero allora fa la count.
		$scope.getSampling = function (flag) {

			$scope.params = {
				fromDate: $scope.ctrl.selectedDataAggiornamentoDa,
				toDate: $scope.ctrl.selectedDataAggiornamentoA,
				page: $scope.ctrl.page || 0,
				count: flag,
				order: $scope.orderBy
			}

			$http({
				method: 'GET',
				url: $scope.ctrl.msIngestionApiUrl + 'ingestion-files',
				params: $scope.params
			}).then(function (response) {
				if (flag == true) {
					$scope.pageTotTemp = Math.floor(response.data.count / 50);
					$scope.totRecordsTemp = response.data.count;
					if (response.data.count > 1000) {
						$scope.showWarning("Attenzione", "Il risultato prevede " + response.data + " record, è consigliabile applicare dei filtri aggiuntivi. Sicuro di voler continuare?");
					} else if (response.data.count == 0) {
						$scope.fillElements(null, null, null, null, null, null);
						$scope.showError("", "Non risulta nessun dato per la ricerca effettuata");
					} else {
						$rootScope.pageTot = $scope.pageTotTemp;
						$rootScope.totRecords = $scope.totRecordsTemp;
						$scope.getSampling(false);
					}
				} else {
					$scope.fillElements($scope.totRecords, $scope.ctrl.page, 0, response.data.list.length, response.data.list, $scope.pageTot);
				}

			}, function (response) {
				$scope.fillElements(null, null, null, null, null, null);
				$scope.showError("", "Non risulta nessun dato per la ricerca effettuata");
			})
		};

		//a seconda di come va la chiamata riempie gli oggetti che servono alla view
		$scope.fillElements = function (totRecords, page, first, last, sampling, pageTot) {
			$scope.ctrl.totRecords = totRecords;
			$scope.ctrl.page = page;
			$scope.ctrl.first = first;
			$scope.ctrl.last = totRecords;
			$scope.ctrl.sampling = sampling;
			$scope.ctrl.pageTot = pageTot;
		}

		$scope.showWarning = function (title, msgs) {
			var parentScope = $scope
			ngDialog.open({
				template: 'pages/advance-payment/dialog-warning.html',
				plain: false,
				width: '60%',
				controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
					$scope.ctrl = {
						title: title,
						message: msgs
					};
					$scope.cancel = function () {
						$rootScope.flag = false;
						ngDialog.closeAll(false);
					};
					$scope.call = function () {
						$rootScope.flag = true;
						parentScope.pageTot = parentScope.pageTotTemp;
						parentScope.totRecords = parentScope.totRecordsTemp;
						ngDialog.closeAll(true);
					};
				}]
			}).closePromise.then(function (data) {
				// no op
				if ($rootScope.flag == true) {
					//aggiungere orderBy
					$scope.getSampling(false);
				}
			});
		};

		$scope.showError = function (title, msgs) {
			ngDialog.open({
				template: 'pages/advance-payment/dialog-alert.html',
				plain: false,
				width: '60%',
				controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
					$scope.ctrl = {
						title: title,
						message: msgs
					};
					$scope.cancel = function () {
						ngDialog.closeAll(false);
					};
					$scope.save = function () {
						ngDialog.closeAll(true);
					};
				}]
			}).closePromise.then(function (data) {
			});
		};

		$scope.showAlert = function (title, message) {
			ngDialog.open({
				template: 'pages/advance-payment/dialog-alert.html',
				plain: false,
				width: '50%',
				controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
					var ngDialogId = $scope.ngDialogId;
					$scope.ctrl = {
						title: title,
						message: message,
						ngDialogId: ngDialogId
					};
					$scope.cancel = function (ngDialogId) {
						ngDialog.close(ngDialogId);
					};
				}]
			}).closePromise.then(function (data) {

			});
		};

		$scope.init = function () {
			if(triplettaContext.state.location && triplettaContext.state.location ==='/monitoraggioIngestion'){
				triplettaContext.state.reset($scope);
				triplettaContext.empty();
			}else {
				$scope.ctrl.selectedDataAggiornamentoDa = '';	
				$scope.ctrl.selectedDataAggiornamentoA = '';	
				$scope.orderBy = 'dataAggiornamento desc';
				$scope.ctrl.page = 0;
			}
			$scope.getSampling(true);
		}

		$scope.$on('$locationChangeSuccess',function(evt, absNewUrl, absOldUrl) {
			if(absNewUrl && absNewUrl.split('#!/') && absNewUrl.split('#!/').length > 1 ) {
				if(!absNewUrl.split('#!/')[1].match(/[i|I]ngestion/g)){
					triplettaContext.empty();
				}
			}
		});

		$scope.scheduleIngestion = function () {
			$http({
				method: 'GET',
				url: $scope.ctrl.msIngestionApiUrl + 'ingestion/init',
			}).then(function (response) {
				if (response.data && response.data.errorMessage) {
					$scope.showAlert('Errore',response.data.errorMessage);
				} else {
					$scope.showError('','Elaborazione richiesta con successo');
				}
			}, function (error) {
				$scope.showError('', "Non è stato possibile richiedere l'ingestion");
			})
		}

	}]);