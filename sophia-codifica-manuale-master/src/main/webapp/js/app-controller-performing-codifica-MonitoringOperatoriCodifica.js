codmanApp.controller('MonitoringOperatoriCodificaCtrl', ['$scope', 'PerformingMonitoringService', "RegoleRipartizioneService", function ($scope, MOService, RRService) {

    $scope.user = angular.element("#headerLinksBig").text().trim();
    $scope.mo = {};
    $scope.paging = {
        currentPage: 1,
        order: "inizioPeriodoCompetenza desc"
    };

    RRService.getPeriodiRipartizione().then(function (response) {
        $scope.periodiRipartizione = response.data
    }, function (response) {
        RRService.showError("Errore", "Non è stato possibile ottenere la lista dei periodi di ripartizione")
    });

    $scope.navigateToPreviousPage = function () {
        $scope.paging.currentPage = $scope.paging.currentPage - 1;
        $scope.init($scope.mo)
    };

    $scope.navigateToNextPage = function () {
        $scope.paging.currentPage = $scope.paging.currentPage + 1;
        $scope.init($scope.mo)
    };

    $scope.navigateToEndPage = function () {
        $scope.paging.currentPage = $scope.paging.totPages;
        $scope.init($scope.mo)
    };

    /*Chiamata da applicare per effettuare la ricerca all'interno di un periodo definito dalla variabile mo*/
    $scope.search = function () {
        $scope.init()
    };

    $scope.init = function () {
        /*creare servizio getListaMonitoring che fa ritornare la lista di oggetti al fine di popolare la tabella nella pagina*/
        MOService.getListaMonitoring($scope.mo.idPeriodoRipartizione, $scope.paging).then(function (response) {

            if (!response.data) {
                $scope.paging = {
                    currentPage: 1,
                    order: "codiceConfigurazione asc"
                };
                MOService.showError("Attenzione", "Non sono stati trovate codifiche per il periodo selezionato");
                $scope.records = [];
                return
            }
            $scope.records = response.data.riepilogoCodificaManualeResponse;
            $scope.paging.currentPage = response.data.currentPage;
            $scope.paging.totRecords = response.data.totRecords;
            $scope.paging.firstRecord = (($scope.paging.currentPage - 1) * 50) + 1;
            $scope.paging.lastRecord = $scope.paging.currentPage * 50;
            $scope.paging.totPages = Math.ceil(response.data.totRecords / 50);
        }).catch(function () {
            MOService.showError("Errore", "Non è stato possibile ottenere le configurazioni delle soglie")
        });
    };

    $scope.parseDateTime = function (date) {
        return MOService.parseDateTime(date)
    };

    $scope.parseDate = function (date) {
        return MOService.parseDate(date)
    }

    $scope.init();
}]);

codmanApp.service('PerformingMonitoringService', ['ngDialog', '$http', function (ngDialog, $http) {

    var service = this;

    service.getListaMonitoring = function (idPeriodoRipertizione, paging) {
        return $http({
            method: 'GET',
            url: 'rest/performing/codifica/manuale/riepilogo',
            params: {
                inizioPeriodoCompetenza: idPeriodoRipertizione,
                page: paging.currentPage,
            }
        })
    };

    service.checkDate = function (from, to) {
        if (from && to) {

            var comparisonFrom = "";
            var comparisonTo = "";

            if (from.length > 7) {
                comparisonFrom = from.split("-")[2] + from.split("-")[1] + from.split("-")[0];
                comparisonTo = to.split("-")[2] + to.split("-")[1] + to.split("-")[0];
            } else {
                comparisonFrom = from.split("-")[1] + from.split("-")[0];
                comparisonTo = to.split("-")[1] + to.split("-")[0];
            }

            if (comparisonTo < comparisonFrom) {
                return false;
            }
        }

        return true
    };

    service.showError = function (title, msgs) {
        return ngDialog.open({
            template: 'pages/advance-payment/dialog-alert.html',
            plain: false,
            width: '60%',
            controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
                var ngDialogId = $scope.ngDialogId;
                $scope.ctrl = {
                    title: title,
                    message: msgs,
                    ngDialogId: ngDialogId
                };
                $scope.cancel = function () {
                    ngDialog.close(ngDialogId);
                };
            }]
        })
    };

    service.parseDateTime = function (date) {

        if (!date) {
            return ""
        }

        var year = date.split("-")[0];
        var month = date.split("-")[1];
        var day = date.split("-")[2].split("T")[0];

        var minutes = date.split("-")[2].split("T")[1].split(".")[0];

        return day + "-" + month + "-" + year + " " + minutes
    };

    service.parseDate = function (date) {

        if (!date) {
            return ""
        }

        var year = date.split("-")[0];
        var month = date.split("-")[1];
        var day = date.split("-")[2].split("T")[0];

        return day + "-" + month + "-" + year
    }


}]);
