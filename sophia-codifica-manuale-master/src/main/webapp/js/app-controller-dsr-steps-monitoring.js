//(function(){


/**
 * DsrProgressCtrl
 *
 * @path /dsrProgressSearch
 */
codmanApp.controller('DsrStepsMonitoringCtrl', ['$scope', '$route','ngDialog', '$routeParams', '$location', '$http', '$interval', 'configService','dspsUtilizationsCommercialOffersCountries','$httpParamSerializer','configService',
	function($scope, $route, ngDialog, $routeParams, $location, $http, $interval, configService,dspsUtilizationsCommercialOffersCountries,$httpParamSerializer, configService) {

		$scope.dspsUtilizationsCommercialOffersCountries = angular.copy(dspsUtilizationsCommercialOffersCountries);
		$scope.filterParameters = $routeParams;


		$scope.filterParameters = {
			first: $scope.filterParameters.first || 0,
			last: $scope.filterParameters.last || configService.maxRowsPerPage,
			selectedDspModel: $scope.filterParameters.selectedDspModel ? $scope.filterParameters.selectedDspModel = [].concat($scope.filterParameters.selectedDspModel).map(function (x) {
				return angular.fromJson(x)
			}) : void 0,
			selectedCountryModel: $scope.filterParameters.selectedCountryModel ? $scope.filterParameters.selectedCountryModel = [].concat($scope.filterParameters.selectedCountryModel).map(function (x) {
				return angular.fromJson(x)

			}) : void 0,
			selectedUtilizationModel: $scope.filterParameters.selectedUtilizationModel ? $scope.filterParameters.selectedUtilizationModel = [].concat($scope.filterParameters.selectedUtilizationModel).map(function (x) {
				return angular.fromJson(x)

			}) : void 0,
			selectedOfferModel: $scope.filterParameters.selectedOfferModel ? $scope.filterParameters.selectedOfferModel = [].concat($scope.filterParameters.selectedOfferModel).map(function (x) {
				return angular.fromJson(x)

			}) : void 0,
			selectedPeriodFrom: $scope.filterParameters.selectedPeriodFrom ? $scope.filterParameters.selectedPeriodFrom : void 0,
			selectedPeriodTo: $scope.filterParameters.selectedPeriodTo ? $scope.filterParameters.selectedPeriodTo : void 0,
			backclaim: $scope.filterParameters.backclaim ? $scope.filterParameters.backclaim : void 0
		};


		$scope.ctrl = {
			navbarTab: 'dsrStepsMonitoringSearch',
			// filterParameters: [],
			maxrows: configService.maxRowsPerPage,
			first: $routeParams.first||0,
			last: $routeParams.last||configService.maxRowsPerPage||50,
			idDsr: $routeParams.idDsr,
			dataInizio: $routeParams.insertTime,
			timeZone: configService.timeZone||'Europe/Rome',
			sortReverse: true,
			searchResults: {},
			hostnames: [],
			decode: {
				idDsrs: []
			}
		};

		// var someDate = new Date();
		// var numberOfDaysToSub = 2;
		// someDate.setDate(someDate.getDate() - numberOfDaysToSub);
		//
		// var dd = someDate.getDate();
		// var mm = someDate.getMonth() + 1;
		// var y = someDate.getFullYear();
		//
		// $scope.ctrl.insertTime = y+ "-"+mm+"-"+dd+" 00:00";


		$scope.decodeExtract = function(item) {
			if (item.extractFinished) {
				return 'OK' === item.extractStatus ? 'completed' : 'failed';
			}
			if (item.extractStarted) return 'started';
			if (item.extractQueued) return 'queued';
			return null;
		};

		$scope.decodeClean = function(item) {
			if (item.cleanFinished) {
				return 'OK' === item.cleanStatus ? 'completed' : 'failed';
			}
			if (item.cleanStarted) return 'started';
			if (item.cleanQueued) return 'queued';
			return null;
		};


		$scope.decodePrice = function(item) {
			if (item.priceFinished) {
				return 'OK' === item.priceStatus ? 'completed' : 'failed';
			}
			if (item.priceStarted) return 'started';
			if (item.priceQueued) return 'queued';
			return null;
		};

		$scope.decodeHypercube = function(item) {
			if (item.hypercubeFinished) {
				return 'OK' === item.hypercubeStatus ? 'completed' : 'failed';
			}
			if (item.hypercubeStarted) return 'started';
			if (item.hypercubeQueued) return 'queued';
			return null;
		};

		$scope.decodeIdentify = function(item) {
			if (item.identifyFinished) {
				return 'OK' === item.identifyStatus ? 'completed' : 'failed';
			}
			if (item.identifyStarted) return 'started';
			if (item.identifyQueued) return 'queued';
			return null;
		};

		$scope.decodeUniload = function(item) {
			if (item.uniloadFinished) {
				return 'OK' === item.uniloadStatus ? 'completed' : 'failed';
			}
			if (item.uniloadStarted) return 'started';
			if (item.uniloadQueued) return 'queued';
			return null;
		};

		$scope.decodeClaim = function(item) {
			if (item.claimFinished) {
				return 'OK' === item.claimStatus ? 'completed' : 'failed';
			}
			if (item.claimStarted) return 'started';
			if (item.claimQueued) return 'queued';
			return null;
		};

		$scope.onAzzera = function() {
			$scope.ctrl.first = 0;
			$scope.ctrl.last = $scope.ctrl.maxrows;
			$scope.ctrl.idDsr = null;
			$scope.$broadcast('angucomplete-alt:clearInput');
		};

		$scope.selectIdDsr = function(ref) {
			if (ref) {
				$scope.ctrl.idDsr = ref.originalObject.name;
			} else {
				$scope.ctrl.idDsr = null;
			}
			$scope.onRicerca();
		};

		$scope.toggleSortReverse = function() {
			$scope.ctrl.sortReverse = !$scope.ctrl.sortReverse;
			$scope.onRicerca();
		};

		$scope.onRicerca = function() {
			$scope.onRicercaEx(0, $scope.ctrl.maxrows);
			$scope.refreshHostnames();
		};

		$scope.filterApply = function (parameters) {
			parameters.first = 0;
			parameters.last = configService.maxRowsPerPage;
			$scope.filterParametersUrl = $httpParamSerializer(parameters);
			$scope.getPagedResults(parameters);
		};

		$scope.onRicercaEx = function(first, last,parameters) {
			$http({
				method: 'GET',
				url: 'rest/dsr-steps-monitoring/search',
				params: {
					first: first,
					last: last,
					dataInizio: $scope.ctrl.insertTime,
					timeZone: $scope.ctrl.timeZone,
					sortReverse: $scope.ctrl.sortReverse,
					idDsr: $scope.ctrl.idDsr
				}
			}).then(function successCallback(response) {
				$scope.ctrl.searchResults = response.data;
				$scope.ctrl.first = response.data.first;
				$scope.ctrl.last = response.data.last;
				// if (response.data.extra.referenceDate) {
				// 	$scope.ctrl.insertTime = response.data.extra.referenceDate;
				// }
			}, function errorCallback(response) {
				$scope.ctrl.searchResults = { };
			});
		};

		$scope.getPagedResults = function(parameters) {

			var periodFrom = 0;
			var yearFrom = 0;
			var periodTo = 0;
			var yearTo = 0;

			if ((parameters.selectedPeriodFrom)) {
				var from = parameters.selectedPeriodFrom.split('-');
				periodFrom = parseInt(from[0], 10);
				yearFrom = parseInt(from[1], 10);
			}

			if (parameters.selectedPeriodTo) {
				var to = parameters.selectedPeriodTo.split('-');
				periodTo = parseInt(to[0], 10);
				yearTo = parseInt(to[1], 10);
			}

			parameters.dspList = getCollectionValues(parameters.selectedDspModel);
			parameters.countryList = getCollectionValues(parameters.selectedCountryModel);
			parameters.utilizationList = getCollectionValues(parameters.selectedUtilizationModel);
			parameters.offerList = getCollectionValues(parameters.selectedOfferModel);
			parameters.monthFrom = periodFrom;
			parameters.yearFrom = yearFrom;
			parameters.monthTo = periodTo;
			parameters.yearTo = yearTo;


			$http({
				method: 'GET',
				url: 'rest/dsr-steps-monitoring/searchNew',
				params: {
					first: parameters.first || 0,
					last: parameters.last || configService.maxRowsPerPage,
					dataInizio: $scope.ctrl.insertTime,
					timeZone: $scope.ctrl.timeZone,
					sortReverse: $scope.ctrl.sortReverse,
					idDsr: $scope.ctrl.idDsr,
					dspList: parameters.dspList ? parameters.dspList : [],
					countryList: parameters.countryList ? parameters.countryList : [],
					utilizationList: parameters.utilizationList ? parameters.utilizationList : [],
					offerList: parameters.offerList ? parameters.offerList : [],
					monthFrom: parameters.monthFrom ? parameters.monthFrom : 0,
					yearFrom: parameters.yearFrom ? parameters.yearFrom : 0,
					monthTo: parameters.monthTo ? parameters.monthTo : 0,
					yearTo: parameters.yearTo ? parameters.yearTo : 0,
					backclaim: parameters.backclaim
				}
			}).then(function successCallback(response) {
				$scope.ctrl.searchResults = response.data.data;
				$scope.ctrl.searchResults.extra = response.data.extra;
				// $scope.ctrl.first = response.data.first;
				// $scope.ctrl.last = response.data.last;
				// if (response.data.extra.referenceDate) {
				// 	$scope.ctrl.insertTime = response.data.extra.referenceDate;
				// }
			}, function errorCallback(response) {
				$scope.ctrl.searchResults = { };
			});
			function getCollectionValues(collection) {
				var selectedList = [];
				if (collection) {
					for (var e in collection) {
						selectedList.push(collection[e].id ? collection[e].id : collection[e]);
					}
				}
				return selectedList;
			}

		};

		$scope.refreshHostnames = function() {
			$http({
				method: 'GET',
				url: 'rest/dsr-steps-monitoring/hostnames',
				params: {
					activeOnly: 'true'
				}
			}).then(function successCallback(response) {
				$scope.ctrl.hostnames = response.data;
			}, function errorCallback(response) {
				$scope.ctrl.hostnames = [];
			});
		};

		$scope.showSearch = function(event, item) {
			$location
				.path('/serviceBusSearch')
				.search('insertTime', null)
				.search('sortReverse', null)
				.search('first', null)
				.search('last', null)
				.search('idDsr', item.idDsr);
		};

		$scope.showSearchHostname = function(event, item) {
			$location
				.path('/serviceBusSearch')
				.search('insertTime', null)
				.search('sortReverse', null)
				.search('idDsr', null)
				.search('first', null)
				.search('last', null)
				.search('hostname', item.hostname);
		};

		$scope.showSearchServiceType = function(serviceName, queueType) {
			$location
				.path('/serviceBusSearch')
				.search('insertTime', null)
				.search('sortReverse', null)
				.search('idDsr', null)
				.search('first', null)
				.search('last', null)
				.search('serviceName', serviceName)
				.search('queueType', queueType);
		};

		$scope.navigateToCurrentPage = function() {
			$scope.filterApply($scope.filterParameters);

			// $location
			// 	.search('first', 0==$scope.ctrl.first?null:$scope.ctrl.first)
			// 	.search('last', $scope.ctrl.maxrows==$scope.ctrl.last?null:$scope.ctrl.last)
			// 	.search('insertTime', $scope.ctrl.insertTime||null)
			// 	.search('sortReverse', $scope.ctrl.sortReverse?'true':null)
			// 	.search('idDsr', $scope.ctrl.idDsr||null);
		};



		$scope.navigateToNextPage = function () {
			$scope.filterParameters.first = parseInt($scope.filterParameters.last);
			$scope.filterParameters.last = parseInt($scope.filterParameters.last) + configService.maxRowsPerPage;
			$scope.getPagedResults($scope.filterParameters);
		};

		$scope.navigateToPreviousPage = function () {
			$scope.filterParameters.last = parseInt($scope.filterParameters.first);
			$scope.filterParameters.first = Math.max($scope.filterParameters.first - configService.maxRowsPerPage, 0);
			$scope.getPagedResults($scope.filterParameters);
		};

		$scope.showAlert = function(title, message) {
			ngDialog.open({
				template: 'pages/dsr-progress/dialog-alert.html',
				plain: false,
				width: '50%',
				controller: ['$scope', 'ngDialog', function($scope, ngDialog) {
					$scope.ctrl = {
						title: title,
						message: message
					};
					$scope.cancel = function() {
						ngDialog.closeAll(false);
					};
					$scope.save = function() {
						ngDialog.closeAll(true);
					};
				}]
			}).closePromise.then(function (data) {
				if (data.value === true) {

				}
			});
		};

		$scope.showKillEmrStep = function(clusterId, applicationId) {
			ngDialog.open({
				template: 'pages/dsr-progress/dialog-kill-emr-step.html',
				plain: false,
				width: '60%',
				controller: ['$scope', 'ngDialog', function($scope, ngDialog) {
					$scope.ctrl = {
						clusterId: clusterId,
						applicationId: applicationId,
						waitingResponse: false,
						exitValue: null,
						stdout: null,
						stderr: null
					};
					$scope.cancel = function() {
						ngDialog.closeAll(false);
					};
					$scope.save = function() {
						$scope.ctrl.waitingResponse = true;
						$scope.ctrl.exitValue = null;
						$scope.ctrl.stdout = null;
						$scope.ctrl.stderr = null;
						$http({
							method: 'POST',
							url: 'rest/aws/kill-emr-step',
							params: {
								clusterId: $scope.ctrl.clusterId,
								applicationId: $scope.ctrl.applicationId
							}
						}).then(function successCallback(response) {
							$scope.ctrl.waitingResponse = false;
							if (response.data) {
								$scope.ctrl.exitValue = response.data.exitValue;
								$scope.ctrl.stdout = response.data.stdout;
								$scope.ctrl.stderr = response.data.stderr;
							}
//							ngDialog.closeAll(true);
						}, function errorCallback(response) {
							$scope.ctrl.waitingResponse = false;
							$scope.ctrl.exitValue = null;
							$scope.ctrl.stdout = null;
							$scope.ctrl.stderr = null;
//							ngDialog.closeAll(false);
						});
					};
				}]
			}).closePromise.then(function (data) {
				if (data.value === true) {

				}
			});
		};

		
		//
		// // autorefresh every 30 seconds
		// var promiseHostnames = $interval(function() {
		// 	$scope.refreshHostnames();
		// }, 30000);

		
		$scope.filterApply($scope.filterParameters);
		// $scope.refreshHostnames();

	}]);

//})();

