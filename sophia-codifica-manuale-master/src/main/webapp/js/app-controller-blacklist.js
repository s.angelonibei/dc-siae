//(function(){

/**
 * BlacklistCtrl
 * 
 * @path /blacklist
 */
codmanApp.controller('BlacklistCtrl', ['$scope', 'ngDialog', '$routeParams', '$location', '$route', '$http', 'Upload', 'configService', 'societaTutelaList', 'blacklistService',
	function ($scope, ngDialog, $routeParams, $location, $route, $http, Upload, configService, societaTutelaList, blacklistService) {

		yyyymmdd = function (date, sep) {
			if (date) {
				var dt = new Date(date);
				var mm = dt.getMonth() + 1;
				var dd = dt.getDate();
				return [dt.getFullYear(),
				(mm > 9 ? '' : '0') + mm,
				(dd > 9 ? '' : '0') + dd
				].join(sep);
			}
		};

		$scope.ctrl = {
			navbarTab: 'blacklist',
			maxrows: configService.maxRowsPerPage,
			first: $routeParams.first || 0,
			last: $routeParams.last || configService.maxRowsPerPage,
			country: $routeParams.country,
			codiceSocietaTutela: $routeParams.codiceSocietaTutela,
			validFrom: yyyymmdd($routeParams.validFrom, '-'),
			xlsS3Url: $routeParams.xlsS3Url,
			sortType: 'validFrom',
			sortReverse: false,
			decode: {
				countries: {}
			},
			searchResults: {},
			societaTutelaList: societaTutelaList.data
		};
		blacklistService.setSocietaTutelaList(societaTutelaList.data);

		$scope.selectCountry = function (ref) {
			if (ref) {
				$scope.ctrl.country = ref.originalObject.idCountry;
			} else {
				$scope.ctrl.country = null;
			}
			$scope.onRicerca();
		};

		$scope.selectetSocietaTutela = function (ref) {
			if (ref) {
				$scope.ctrl.codiceSocietaTutela = ref.originalObject.codice;
			} else {
				$scope.ctrl.codiceSocietaTutela = null;
			}
			$scope.onRicerca();
		};

		$scope.onAzzera = function () {
			$scope.ctrl.first = 0;
			$scope.ctrl.last = $scope.ctrl.maxrows;
			$scope.ctrl.country = null;
			$scope.ctrl.validFrom = null;
			$scope.ctrl.xlsS3Url = null;
			$scope.$broadcast('angucomplete-alt:clearInput');
		};

		$scope.onRicerca = function () {
			$scope.onRicercaEx(0, $scope.ctrl.maxrows);
		};

		$scope.onRicercaEx = function (first, last) {
			$http({
				method: 'GET',
				url: 'rest/blacklist/search',
				params: {
					first: first,
					last: last,
					country: $scope.ctrl.country,
					validFrom: $scope.ctrl.validFrom,
					xlsS3Url: $scope.ctrl.xlsS3Url,
					codiceSocietaTutela: $scope.ctrl.codiceSocietaTutela
				}
			}).then(function successCallback(response) {
				$scope.ctrl.searchResults = response.data;
				$scope.ctrl.first = response.data.first;
				$scope.ctrl.last = response.data.last;
			}, function errorCallback(response) {
				$scope.ctrl.searchResults = {};
			});

		};

		$scope.navigateToNextPage = function () {
			$location
				.search('first', $scope.ctrl.last)
				.search('last', $scope.ctrl.last + $scope.ctrl.maxrows)
				.search('country', $scope.ctrl.country || null)
				.search('validFrom', $scope.ctrl.validFrom || null)
				.search('xlsS3Url', $scope.ctrl.xlsS3Url || null);
		};

		$scope.navigateToPreviousPage = function () {
			$location
				.search('first', Math.max($scope.ctrl.first - $scope.ctrl.maxrows, 0))
				.search('last', $scope.ctrl.first)
				.search('country', $scope.ctrl.country || null)
				.search('validFrom', $scope.ctrl.validFrom || null)
				.search('xlsS3Url', $scope.ctrl.xlsS3Url || null);
		};

		$scope.showUpload = function (event) {
			var scope = $scope;
			var countries = [];
			angular.forEach(scope.ctrl.decode.countries, function (value, key) {
				countries.push({
					code: key,
					name: value
				});
			});
			ngDialog.open({
				template: 'pages/blacklist/dialog-upload.html',
				plain: false,
				width: '50%',
				controller: ['$scope', 'ngDialog', '$http', function ($scope, ngDialog, $http) {
					$scope.ctrl = {
						countries: countries,
						validFrom: null,
						file: null,
						societaTutela: null,
						societaTutelaList: scope.ctrl.societaTutelaList
					};
					$scope.cancel = function () {
						ngDialog.closeAll(false);
					};
					$scope.save = function () {
						if ($scope.form.file.$valid && $scope.ctrl.file) {
							Upload.upload({
								url: 'rest/blacklist/upload',
								data: {
									file: $scope.ctrl.file,
									country: $scope.ctrl.country.code,
									validFrom: $scope.ctrl.validFrom,
									codiceSocietaTutela: $scope.ctrl.societaTutela.codice
								}
							}).then(function (resp) {
								ngDialog.closeAll(true);
							}, function (resp) {
								ngDialog.closeAll(false);
								scope.showError(null, resp.data);
							}, function (evt) {
								var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
								logmsg('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
							});
						}
					};
				}]
			}).closePromise.then(function (data) {
				if (data.value === true) {
					$route.reload();
				}
			});
		};

		$scope.showError = function (event, msgs) {
			ngDialog.open({
				template: 'pages/blacklist/dialog-error.html',
				plain: false,
				width: '60%',
				controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
					$scope.ctrl = {
						msgs: msgs
					};
					$scope.cancel = function () {
						ngDialog.closeAll(false);
					};
					$scope.save = function () {
						ngDialog.closeAll(true);
					};
				}]
			}).closePromise.then(function (data) {
				// no op
			});
		};

		$scope.showDelete = function (event, item) {
			var scope = $scope;
			ngDialog.open({
				template: 'pages/blacklist/dialog-delete.html',
				plain: false,
				width: '60%',
				controller: ['$scope', 'ngDialog', '$http', function ($scope, ngDialog, $http) {
					$scope.ctrl = {
						id: item.id,
						country: item.country,
						validFrom: item.validFrom,
						xlsS3Url: item.xlsS3Url,
						decode: scope.ctrl.decode
					};
					$scope.cancel = function () {
						ngDialog.closeAll(false);
					};
					$scope.save = function () {
						$http({
							method: 'DELETE',
							url: 'rest/blacklist/delete/' + $scope.ctrl.id
						}).then(function successCallback(response) {
							ngDialog.closeAll(true);
						}, function errorCallback(response) {
							ngDialog.closeAll(false);
						});
					};
				}]
			}).closePromise.then(function (data) {
				if (data.value === true) {
					$route.reload();
				}
			});
		};

		$scope.showDetails = function (event, item) {
			var scope = $scope;
			ngDialog.open({
				template: 'pages/blacklist/dialog-details.html',
				plain: false,
				width: '60%',
				controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
					$scope.ctrl = {
						id: item.id,
						country: item.country,
						validFrom: item.validFrom,
						xlsS3Url: item.xlsS3Url,
						decode: scope.ctrl.decode,
						societaTutela: item.societaTutela
					};
					$scope.cancel = function () {
						ngDialog.closeAll(false);
					};
					$scope.save = function () {
						ngDialog.closeAll(true);
					};
				}]
			}).closePromise.then(function (data) {
				if (data.value === true) {
					// no op
				}
			});
		};
		blacklistService.getAnagCountry()
			/* $http({
				method: 'GET',
				url: 'rest/anagCountry'
			}) */
			.then(function successCallback(response) {
				$scope.ctrl.decode.countries = response;
			}, function errorCallback(response) {
				$scope.ctrl.decode.countries = {};
			});

		$scope.onRicercaEx($scope.ctrl.first, $scope.ctrl.last);

		$scope.goToConfiguration = function (item = null) {
			console.log(item);
			if (item) {
				const { validFrom, societaTutela: { codice: societa }, country: territorio, xlsS3Url } = item;
				const splitPath = xlsS3Url.split('/') || [];
				const loadDataFrom = {
					societa,
					territorio,
					dataInizioValidita: moment(validFrom).format('DD/MM/YYYY'),
					nomeFileCsv: splitPath[splitPath.length -1]
				};
				blacklistService.setPreloadConfig(loadDataFrom);
			} else { // new config
				blacklistService.setPreloadConfig(null); // to reset data
			}
			$location.path("/configurazione-blacklist");
		}

	}]);

