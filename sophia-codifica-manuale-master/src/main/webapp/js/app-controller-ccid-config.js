/**
 * DsrMetadataCtrl
 * 
 * @path /dsrMetadata
 */
codmanApp.controller('CCIDConfigCtrl', ['$scope', '$route', 'ngDialog', '$routeParams', '$location', '$http', function ($scope, $route, ngDialog, $routeParams, $location, $http) {

    $scope.ctrl = {
        navbarTab: 'ccidConfig',
        config: [],
        filterParameters: [],
        execute: $routeParams.execute,
        hideForm: $routeParams.hideForm,
        maxrows: 20,
        first: $routeParams.first || 0,
        last: $routeParams.last || 20,
        filter: $routeParams.filter,
        selectedDsp: $routeParams.dsp || '*',
        selectedUtilization: $routeParams.utilization || '*',
        selectedOffer: $routeParams.offer || '*',
        selectedCountry: $routeParams.country || '*'
    };

    $scope.messages = {
        info: "",
        error: ""
    };

    $scope.namingEnum = {
        default: 'Default',
        standard: 'Secondo standard'
    };

    $scope.fileNamingData = {};

    $scope.infoMessage = function (message) {
        $scope.messages.info = message;
    };

    $scope.errorMessage = function (message) {
        $scope.messages.error = message;
    };

    $scope.getFileNamingData = function () {
        return new Promise((resolve, _reject) => {
            resolve({ useStartEndDateList: $scope.useStartEndDateList });
        });
    }

    $scope.clearMessages = function () {
        $scope.messages = {
            info: "",
            error: ""
        };
    }

    $scope.namingEnum = {
        default: 'Default',
        standard: 'Secondo standard'
    };
    $scope.useStartEndDateList = ["", "yyyy", "yyyy-mm", "yyyy-mm-dd", "yyyy-Qq", "yyyy-Www", "yyyy-mm-dd--yyyy-mm-dd"];

    $scope.fileNamingData = {};

    $scope.getFileNamingData = function () {
        return new Promise((resolve, _reject) => {
            resolve({ useStartEndDateList: $scope.useStartEndDateList });
        });
    }


    $scope.navigateToNextPage = function () {
        $location.search('execute', 'true')
            .search('hideForm', $scope.ctrl.hideForm)
            .search('first', $scope.ctrl.last)
            .search('last', $scope.ctrl.last + $scope.ctrl.maxrows);
    };

    $scope.navigateToPreviousPage = function () {
        $location.search('execute', 'true')
            .search('hideForm', $scope.ctrl.hideForm)
            .search('first', Math.max($scope.ctrl.first - $scope.ctrl.maxrows, 0))
            .search('last', $scope.ctrl.first);
    };

    // Funzione principale richiamata al caricamento della pagina
    $scope.onRefresh = function () {
        // get All DSP Configuration
        if ($scope.ctrl.execute) {
            $scope.getPagedResults($scope.ctrl.first,
                $scope.ctrl.last,
                $scope.ctrl.selectedDsp,
                $scope.ctrl.selectedUtilization,
                $scope.ctrl.selectedOffer,
                $scope.ctrl.selectedCountry);
        } else {
            $scope.getPagedResults(0,
                $scope.ctrl.maxrows);
        }
    };

    $scope.getPagedResults = function (first, last, dsp, utilization, offer, country) {
        /*  $scope.mock = true;
         const promise = $scope.mock ?
             $http.get('./js/mock/dspStatistics-all.json') : */
        const promise = $http({
            method: 'GET',
            url: 'rest/ccidConfig/all',
            params: {
                first: first,
                last: last,
                dsp: (typeof dsp !== 'undefined' ? dsp : ""),
                utilization: (typeof utilization !== 'undefined' ? utilization : ""),
                offer: (typeof offer !== 'undefined' ? offer : ""),
                country: (typeof country !== 'undefined' ? country : "")
            }
        });

        return promise
            .then(function successCallback({ data }) {
                data.rows = data.rows.map(row => ({
                    ...row,
                    formatoNaming: row.fileNaming && Object.keys(row.fileNaming).length ?
                        $scope.namingEnum.standard : $scope.namingEnum.default
                }));
                $scope.ctrl.config = data;
                $scope.ctrl.first = data.first;
                $scope.ctrl.last = data.last;
            }, function errorCallback(_error) {
                $scope.errorMessage("Si è verificato un problema, riprovare più tardi.");
                $scope.ctrl.config = [];
            });

    };

    $scope.showFileNamingConfig = function (item) {

        $scope.getFileNamingData()
            .then(({ useStartEndDateList }) => {
                $scope.fileNamingData = {
                    useStartEndDateList,
                    ...item.fileNaming,
                    hasFileNamingConfig: _.has(item || {}, 'fileNaming')
                };
                const dialog =
                    ngDialog.open({
                        template: 'pages/ccid-config/dialog-file-naming.html',
                        controller: ['$scope', 'ngDialog', FileNamingDialogController],
                        data: $scope.fileNamingData
                    });
                dialog.closePromise
                    .then(({ value }) => {
                        if (value.eliminaFileNamingConfig) {
                            const { idCCIDConfig } = item;
                            $scope.showDeleteConfig(null, { ...item, eliminaFileNamingConfig: true })
                            //$scope.deleteFileNamingRequest(idCCIDConfig);
                        } else {
                            const { idCCIDConfig } = item;
                            $scope.addFileNamingRequest(value, idCCIDConfig);
                        }
                    });
            }, (_error) => {
                $scope.errorMessage("Si è verificato un problema, riprovare più tardi.");
            });
    }

    $scope.addFileNamingRequest = function (fileNamingBody, idCCIDConfig) {
        if (typeof fileNamingBody === "object") {
            const path = 'rest/ccidConfig/addFileNaming/{idCCIDConfig}';
            $http({
                method: 'POST',
                url: path.replace('{idCCIDConfig}', idCCIDConfig),
                data: fileNamingBody
            })
                .then((_response) => {
                    $scope.infoMessage("Configurazione file naming avvenuta con successo.");
                    $scope.onRefresh();
                }, (_error) => {
                    $scope.errorMessage("Si è verificato un problema, riprovare più tardi.");
                });
        }
    };

    $scope.deleteFileNamingRequest = function (idCCIDConfig) {
        const path = 'rest/ccidConfig/deleteFileNaming/{idCCIDConfig}';
        $http({
            method: 'DELETE',
            url: path.replace('{idCCIDConfig}', idCCIDConfig),
        })
            .then((_response) => {
                $scope.infoMessage("Configurazione file naming eliminata con successo.");
                $scope.onRefresh();
            }, (_error) => {
                $scope.errorMessage("Si è verificato un problema, riprovare più tardi.");
            });
    };



    //funzione che renderizza il popup per l'aggiunta di una nuova configurazione 
    $scope.showNewConfig = function (item = null) {
        console.log(item);
        ngDialog.open({
            template: 'pages/ccid-config/dialog-new.html',
            plain: false,
            width: '60%',
            controller: ['$scope', 'ngDialog', '$http', '$q', function ($scope, ngDialog, $http, $q) {

                $scope.ctrl = {
                    dspList: [],
                    commercialOffersList: [],
                    utilizationList: [],
                    countryList: [],
                    ccidVersionList: [],
                    currencyList: [],
                    ccidVersionParams: {}
                };
                $scope.encoded = true;
                $scope.encodedProvisional = true;
                $scope.notEncoded = false;
                $scope.excludeClaimZero = false;
                $scope.item = item;

                var promise1 = $http({ method: 'GET', url: 'rest/data/dspUtilizationsOffersCountries' });
                var promise2 = $http({ method: 'GET', url: 'rest/ccidConfig/configurationValues' });
                $q.all([promise1, promise2]).then(function successCallback(response) {


                    $scope.ctrl.dspList = response[0].data.dsp;
                    $scope.ctrl.dspList.splice(0, 0, { idDsp: '*', name: '*' });
                    $scope.ctrl.utilizationList = response[0].data.utilizations;
                    $scope.ctrl.utilizationList.splice(0, 0, { idUtilizationType: '*', name: '*' });
                    $scope.ctrl.commercialOffersList = response[0].data.commercialOffers;
                    $scope.ctrl.commercialOffersList.splice(0, 0, { idCommercialOffering: '*', offering: '*' });
                    $scope.ctrl.countryList = response[0].data.countries;
                    $scope.ctrl.countryList.splice(0, 0, { idCountry: '*', name: '*' });

                    $scope.ctrl.ccidVersionList = response[1].data.ccidVersionList;

                    //for $scope.ctrl.ccidVersionList

                    //if [0] is null set editable true
                    for (let idx in $scope.ctrl.ccidVersionList) {
                        for (var idx2 in $scope.ctrl.ccidVersionList[idx].paramsMap) {
                            if (!$scope.ctrl.ccidVersionList[idx].paramsMap[idx2][0].value) {
                                $scope.ctrl.ccidVersionList[idx].paramsMap[idx2][0].editable = true;
                            }
                        }
                    }

                    $scope.ctrl.currencyList = response[1].data.currencyList;

                    if ($scope.item) {
                        updateModel();
                    }

                }, function errorCallback(_response) {
                    $scope.ctrl.dspList = [];
                    $scope.ctrl.utilizationList = [];
                    $scope.ctrl.commercialOffersList = [];
                    $scope.ctrl.countryList = [];
                    $scope.ctrl.ccidVersionList = [];
                    $scope.ctrl.countryList = [];
                });

                $scope.getTitle = function () {
                    return $scope.item ? 'Modifica configurazione' : 'Nuova configurazione';
                }

                $scope.filterUtilizationByDsp = function () {
                    return function (item) {
                        if ((typeof $scope.selectedDsp === 'undefined'))
                            return false;

                        if ($scope.selectedDsp.idDsp === '*') {
                            return true;
                        }

                        for (var i = 0; i < $scope.ctrl.commercialOffersList.length; i++) {
                            if (($scope.ctrl.commercialOffersList[i].idDSP == $scope.selectedDsp.idDsp &&
                                item.idUtilizationType == $scope.ctrl.commercialOffersList[i].idUtilizationType) || item.idUtilizationType == '*') {
                                return true;
                            }
                        }

                        return false;

                    };
                };

                $scope.filterOffersByDspAndUtilization = function () {
                    return function (item) {
                        if ((typeof $scope.selectedDsp === 'undefined') || (typeof $scope.selectedUtilization === 'undefined'))
                            return false;

                        if ($scope.selectedDsp.idDsp === '*' && item.idCommercialOffering != '*') {
                            return false;
                        } else {
                            if ((item.idDSP == $scope.selectedDsp.idDsp && item.idUtilizationType == $scope.selectedUtilization.idUtilizationType) ||
                                item.idCommercialOffering == '*') {
                                return true;
                            } else {
                                return false;
                            }
                        }

                    };
                };


                $scope.openErrorDialog = function (errorMsg) {
                    ngDialog.open({
                        template: 'errorDialogId',
                        plain: false,
                        width: '40%',
                        data: errorMsg

                    });
                };

                $scope.openConfirmationDialog = function (msg) {
                    ngDialog.open({
                        template: 'confirmationDialogId',
                        plain: false,
                        width: '40%',
                        data: msg

                    });
                };

                $scope.cancel = function () {
                    ngDialog.closeAll(false);
                };


                $scope.save = function () {
                    if (!$scope.item) {
                        $http({
                            method: 'GET',
                            url: 'rest/ccidConfig/checkConfigAlreadySet',
                            params: {
                                idDsp: $scope.selectedDsp.idDsp,
                                idUtilization: $scope.selectedUtilization.idUtilizationType,
                                idCommercialOffer: $scope.selectedOffer.idCommercialOffering,
                                idCountry: $scope.selectedCountry.idCountry
                            }
                        }).then(function successCallback(response) {
                            if (response.data === true) {
                                $scope.openErrorDialog("Configurazione già presente");
                            } else {
                                $scope.saveData();
                            }

                        }, function errorCallback(response) {
                            $scope.openErrorDialog(response.statusText);
                        });
                    } else {
                        const { idCCIDConfig } = item;
                        const ccidAnagParams = buildBodyConfiguration();
                        const url = 'rest/ccidConfig/editCCIDConfig/{idCCIDConfig}'.replace('{idCCIDConfig}', idCCIDConfig);
                        $http({
                            method: 'POST',
                            url,
                            data: {
                                idDsp: $scope.selectedDsp.idDsp,
                                idUtilizationType: $scope.selectedUtilization.idUtilizationType,
                                idCommercialOffers: $scope.selectedOffer.idCommercialOffering,
                                idCountry: $scope.selectedCountry.idCountry,
                                encoded: $scope.encoded,
                                encodedProvisional: $scope.encodedProvisional,
                                notEncoded: $scope.notEncoded,
                                excludeClaimZero: $scope.excludeClaimZero,
                                idCCIDVersion: $scope.selectedCCIDVersion.idCCIDVersion,
                                rank: $scope.rank,
                                currency: $scope.selectedCurrency,
                                ccidAnagParams
                            }
                        })
                            .then(() => {
                                ngDialog.closeAll(true);
                                $route.reload();
                            })

                    }


                };

                $scope.saveData = function () {
                    const ccidAnagParams = buildBodyConfiguration()
                    $http({
                        method: 'POST',
                        url: 'rest/ccidConfig',
                        data: {
                            idDsp: $scope.selectedDsp.idDsp,
                            idUtilizationType: $scope.selectedUtilization.idUtilizationType,
                            idCommercialOffers: $scope.selectedOffer.idCommercialOffering,
                            idCountry: $scope.selectedCountry.idCountry,
                            encoded: $scope.encoded,
                            encodedProvisional: $scope.encodedProvisional,
                            notEncoded: $scope.notEncoded,
                            excludeClaimZero: $scope.excludeClaimZero,
                            idCCIDVersion: $scope.selectedCCIDVersion.idCCIDVersion,
                            rank: $scope.rank,
                            currency: $scope.selectedCurrency,
                            ccidAnagParams
                        }
                    }).then(function successCallback(response) {
                        ngDialog.closeAll(true);
                        $route.reload();
                    }, function errorCallback(response) {
                        $scope.openErrorDialog(response.statusText);

                    });
                };

                $scope.objLength = function (obj) {
                    return Object.keys(obj).length;
                };

                $scope.setCCIDVersionDefaults = function (selected) {
                    let ccidVersion = $scope.ctrl.ccidVersionList.find(element => element.idCCIDVersion === selected.idCCIDVersion);
                    if (ccidVersion.paramsMap && Object.keys(ccidVersion.paramsMap).length > 1) {
                        for (let param of Object.keys(ccidVersion.paramsMap)) {
                            let paramDefault = ccidVersion.paramsMap[param].find(element => element.isDefault);
                            $scope.ctrl.ccidVersionParams[param] = paramDefault;
                        }
                    } else {
                        $scope.ctrl.ccidVersionParams = {};
                    }
                };

                function buildBodyConfiguration() {
                    let ccidAnagParams = [];
                    Object.keys($scope.ctrl.ccidVersionParams)
                        .forEach(key => {
                            let anagCcidConfigDTO = {};
                            const { idCCIDParam, value, editable } = $scope.ctrl.ccidVersionParams[key];
                            anagCcidConfigDTO.id = idCCIDParam;
                            anagCcidConfigDTO.value = value;
                            anagCcidConfigDTO.editable = editable;
                            ccidAnagParams.push(anagCcidConfigDTO);
                        });
                    return ccidAnagParams;
                };
                function updateModel() {
                    /*  ITEM:
                        ccdiVersion: 1
                        ccdiVersionName: "13"
                        ccidParams: {}
                        commercialOfferName: "*"
                        countryName: "*"
                        currency: "EUR"
                        dspName: "7 digital"
                        encoded: true
                        encodedProvisional: false
                        excludeClaimZero: false
                        formatoNaming: "Default"
                        idCCIDConfig: 13
                        idCommercialOffers: "*"
                        idCountry: "*"
                        idDsp: "7digital"
                        idUtilizationType: "*"
                        notEncoded: false
                        rank: 2
                        utilizationName: "*"
                    " */
                    console.log('update models');
                    const { item } = $scope;
                    $scope.selectedDsp = $scope.ctrl.dspList.find(dsp => dsp.name === item.dspName);
                    $scope.selectedUtilization = $scope.ctrl.utilizationList
                        .filter(utilization => $scope.filterUtilizationByDsp()(utilization))
                        .find(utilization => utilization.name === item.utilizationName);
                    $scope.selectedOffer = $scope.ctrl.commercialOffersList
                        .filter(offer => $scope.filterOffersByDspAndUtilization()(offer))
                        .find(offer => offer.offering === item.commercialOfferName);
                    $scope.selectedCountry = $scope.ctrl.countryList.find(country => country.name === item.countryName)
                    $scope.encoded = item.encoded;
                    $scope.encodedProvisional = item.encodedProvisional;
                    $scope.notEncoded = item.notEncoded;
                    $scope.excludeClaimZero = item.excludeClaimZero;
                    $scope.selectedCCIDVersion = $scope.ctrl.ccidVersionList.find(ccidVersion => ccidVersion.name === item.ccdiVersionName);
                    $scope.selectedCurrency = $scope.ctrl.currencyList.find(currency => currency === item.currency);
                    $scope.rank = item.rank;
                    if ($scope.selectedCCIDVersion.paramsMap) {
                        /* TODO CHECK  */
                        $scope.setCCIDVersionDefaults($scope.selectedCCIDVersion);
                        Object.entries($scope.selectedCCIDVersion.paramsMap)
                            .forEach(([key, value]) => {
                                const { editable } = value[0];
                                if (item.ccidParams[key]) {
                                    $scope.ctrl.ccidVersionParams[key].value = item.ccidParams[key];
                                }
                                /* if (editable) {
                                } else {
                                    ctrl.ccidVersionParams[key] = item.ccidParams[key];
                                } */
                            }, $scope);
                    }
                    //ccid 14 params todo



                }
            }]
        }).closePromise.then(function (data) {
            if (data.value === true) {
                // no op
            }
        });
    };


    $scope.showDeleteConfig = function (event, configToRemove) {

        ngDialog.open({
            template: 'pages/ccid-config/dialog-delete.html',
            plain: false,
            width: '60%',
            controller: ['$scope', 'ngDialog', '$http', function ($scope, ngDialog, $http) {
                $scope.ctrl = {
                    configToRemove: configToRemove
                };


                $scope.cancel = function () {
                    ngDialog.closeAll(false);
                };


                $scope.deleteConfig = function () {
                    // rimuovi fileNamingConf
                    if ($scope.ctrl.configToRemove.eliminaFileNamingConfig) {
                        ngDialog.close($scope.ngDialogId, $scope.ctrl.configToRemove)
                    } else {
                        $http({
                            method: 'DELETE',
                            url: 'rest/ccidConfig',
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            params: {
                                idCCIDConfig: $scope.ctrl.configToRemove.idCCIDConfig
                            }

                        })
                            .then(function successCallback(response) {
                                ngDialog.closeAll(true);
                                $route.reload();
                            }, function errorCallback(response) {
                                $scope.openErrorDialog(response.statusText);
                            });
                    }

                };

                $scope.openErrorDialog = function (errorMsg) {
                    ngDialog.open({
                        template: 'errorDialogId',
                        plain: false,
                        width: '40%',
                        data: errorMsg

                    });
                };
            }]
        }).closePromise.then(function ({value}) {
            if (value.eliminaFileNamingConfig) {
                const { idCCIDConfig } = value;
                $scope.deleteFileNamingRequest(idCCIDConfig);
            }
        });
    };



    $scope.$on('filterApply', function (event, parameters) {
        $scope.ctrl.filterParameters = parameters;
        $location.search('execute', 'true')
            .search('hideForm', false)
            .search('first', 0)
            .search('last', $scope.ctrl.maxrows)
            .search('dsp', parameters.dsp)
            .search('utilization', parameters.utilization)
            .search('offer', parameters.offer)
            .search('country', parameters.country)
            .search('hideForm', parameters.hideForm);
        $scope.ctrl.hideForm = false;

    });

    $scope.onRefresh();

}]);


codmanApp.filter('range', function () {
    return function (input, min, max) {
        min = parseInt(min); //Make string input int
        max = parseInt(max);
        for (var i = min; i <= max; i++)
            input.push(i);
        return input;
    };
});  
