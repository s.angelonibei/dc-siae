//(function(){
	
	/**
	 * ServiceBusStatsCtrl
	 * 
	 * @path /service-bus
	 */
	codmanApp.controller('ServiceBusStatsCtrl', ['$scope', 'ngDialog', '$routeParams', '$location', '$route', '$http', function($scope, ngDialog, $routeParams, $location, $route, $http) {

		$scope.ctrl = {
			navbarTab: 'serviceBusStats',			
			serviceBusStats: [ ]
		};		
		
		$scope.onRefresh = function() {
			rownum = 0;
			// get next song line by priority
			$http({
				method: 'GET',
				url: 'rest/service-bus/stats-date'
			}).then(function successCallback(response) {
				$scope.ctrl.serviceBusStats = response.data;
			}, function errorCallback(response) {
				$scope.ctrl.serviceBusStats = [ ];
				if (404 == response.status) { // 404 not found
					$scope.showNotFound(null);
				}
			});
		};
		
		$scope.showNotFound = function(event) {
			ngDialog.open({
				template: 'pages/service-bus/dialog-not-found.html',
				plain: false,
				width: '50%',
				controller: ['$scope', 'ngDialog', function($scope, ngDialog) {
					$scope.cancel = function() {
						ngDialog.closeAll(false);
					};
					$scope.save = function() {
						ngDialog.closeAll(true);
					};
				}]
			}).closePromise.then(function (data) {
				// no op
			});
		};
		
		$scope.showSearch = function(event, sqsFailedStat) {
			$location
				.path('/serviceBusSearch')
				.search('insertTime', sqsFailedStat.insertTime)
				.search('queueName', sqsFailedStat.queueName);
		};
		
		$scope.onRefresh();

}]);
	
//})();

