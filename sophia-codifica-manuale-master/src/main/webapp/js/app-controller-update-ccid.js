/**
 * InvoiceAdvancePaymentCtrl
 * 
 * @path /dspConfig
 */
codmanApp.controller('UpdateCcidCtrl', ['$scope','$route', 'ngDialog', '$routeParams', '$location', '$http', 'configService', 'dspsUtilizationsCommercialOffersCountries', 'anagClientData',
	function($scope,$route, ngDialog, $routeParams, $location, $http,configService, dspsUtilizationsCommercialOffersCountries, anagClientData) {

	$scope.dspsUtilizationsCommercialOffersCountries = angular.copy(dspsUtilizationsCommercialOffersCountries);
	$scope.filterParameters = $routeParams;

	$scope.anagClientData = angular.copy(anagClientData);

	$scope.ctrl = {
		navbarTab: 'advancePayment',
		filterParameters: [],
		execute: $routeParams.execute,
		maxrows: 50,
		first:0,
		last: 50,
		sortType: 'priority',
		sortReverse: true,
		filter: $routeParams.filter,
		selectedItems: {},
		hideEditing: true,
		items:[],
		selectedDsp: {},
		advancePayments: {},
		msInvoiceApiUrl: `${$location.protocol()}://${$location.host()}:${$location.port()}/${configService.msInvoiceApiPath}`
		
	};

    $scope.selectedCheckbox = {}
    
    $scope.getFirst = function() {
		if ($scope.ctrl.first==0) {
			return $scope.ctrl.first+1;
		}
		return $scope.ctrl.first;
	};
    $scope.navigateToNextPage = function() {
		$scope.ctrl.first = $scope.ctrl.last + 1
		$scope.ctrl.last += $scope.ctrl.maxrows 
		var parameters= $scope.ctrl.filterParameters;
		$scope.getPagedResults($scope.ctrl.first, $scope.ctrl.last,
						parameters.client,
       					parameters.dsp,
       					parameters.countryList,
       					parameters.utilizationList,
       					parameters.offerList,
       					parameters.monthFrom,
       					parameters.yearFrom,
       					parameters.monthTo,
       					parameters.yearTo
       					);
	};

	$scope.navigateToPreviousPage = function() {
        $scope.ctrl.last = $scope.ctrl.first -1
        $scope.ctrl.first  -= ($scope.ctrl.maxrows)
        if($scope.ctrl.first==1){
            $scope.ctrl.first=0;
        }
		var parameters= $scope.ctrl.filterParameters;
		$scope.getPagedResults($scope.ctrl.first, $scope.ctrl.last,
							parameters.client,
        					parameters.dsp,
        					parameters.countryList,
        					parameters.utilizationList,
        					parameters.offerList,
        					parameters.monthFrom,
        					parameters.yearFrom,
        					parameters.monthTo,
        					parameters.yearTo
        					);

	};

	$scope.resetPaging = function() {
		$scope.ctrl.first  = 0 		
		$scope.ctrl.last = $scope.ctrl.maxrows
	};

	// Funzione principale richiamata al caricamento della pagina
	$scope.onRefresh = function() {
		$scope.getPagedResults(0, $scope.ctrl.maxrows)
	};
	

	$scope.hasNext = function(){
		if($scope.ctrl.data==undefined){
			return false
		}
		return  $scope.ctrl.data.hasNext;
	};

	
	$scope.hasPrev = function(){
		if($scope.ctrl.data==undefined){
			return false
		}
		return  $scope.ctrl.data.hasPrev;
	};
	

	
//	createAdvancePaymentList = function(){
//		$http({
//			method: 'GET',
//			url: 'rest/advancepayment/AdvancePaymentList'
//		}).then(function successCallback(response) {
//			for(x in response.data){			
//				$scope.codeSelectionList.push({
//					label: response.data[x].code + ' - ' + response.data[x].description,
//					id:  x.toString(),
//					idInvoiceReason: response.data[x].idItemInvoiceReason
//				});
//			}
//		}, function errorCallback(response) {
//			 $scope.codeSelectionList=[]
//		});
//		
//	}

	$scope.getPagedResults = function(first, last ,client, dsp, countryList, utilizationList, offerList,
			monthFrom, yearFrom, monthTo, yearTo) {

		        if(client !== undefined && null !== client){

		        		$http({
		        			method: 'POST',
		        			url: $scope.ctrl.msInvoiceApiUrl + 'invoice/all/ccid',
		        			headers: {
		                        'Content-Type': 'application/json'},
		        			data: {
		        				first: first,
		        				last: last,
								clientId: (typeof client !== 'undefined' ? client.idAnagClient: 0) ,
								dspList: ( typeof dsp !== 'undefined' ? dsp : []) ,
		        				countryList : (typeof countryList !== 'undefined' ? countryList : []) ,
		        				utilizationList : (typeof utilizationList !== 'undefined' ? utilizationList : []),
		        				offerList : (typeof offerList !== 'undefined' ? offerList : []),
		        				monthFrom : (typeof monthFrom !== 'undefined' ? monthFrom : 0),
		        				yearFrom : (typeof yearFrom !== 'undefined' ? yearFrom : 0),
		        				monthTo : (typeof monthTo !== 'undefined' ? monthTo : 0),
		        				yearTo : (typeof yearTo !== 'undefined' ? yearTo : 0)
		        			}
		        		}).then(function successCallback(response) {
		        			$scope.ctrl.data = response.data;
		        			
		        		}, function errorCallback(response) {
							$scope.ctrl.data = [];
						});
		        		
		        		$scope.ctrl.selectedClient = client;
		        		
		        		
		        }

			};


	
	$scope.confirm = function(){
        //$scope.ctrl.hideEditing = false
		$scope.getPagedResults(0,
					$scope.ctrl.maxrows,$scope.ctrl.selectedDsp,2);
	}

	$scope.selectedItem = function(item){
	    	if($scope.ctrl.data.rows === undefined){
	    		$scope.allSelected = false
	        setMainCheckbox()
	    		return
	    	}
        if(item === true){
            // seleziona tutti gli elementi
            for(i=0; i< $scope.ctrl.data.rows.length; i++ ){
            var value = $scope.ctrl.data.rows[i]
            if(!$scope.ctrl.selectedItems[value.idCCIDmetadata]){
                    addItem(value, $scope.ctrl.selectedItems)
                    $scope.selectedCheckbox[value.idCCIDmetadata]=true
                    setMainCheckbox()
                }
            }
            return
        }else if(item === false){
        // deseleziona tutti gli elementi
            $scope.ctrl.selectedItems = {}
            for(i=0; i< $scope.ctrl.data.rows.length; i++ ){
                var key = $scope.ctrl.data.rows[i].idCCIDmetadata
                $scope.selectedCheckbox[key]=false
            }
            setMainCheckbox()
        }else{
            // aggiungi o rimuovi elemento in base alla sua presenza o meno
			// nella lista
            addOrRemoveSelectedItem(item,$scope.ctrl.selectedItems)
            setMainCheckbox()
        }

	};

	addItem = function(item,map){
        map[item.idCCIDmetadata] = item

	}

	removeItem = function(item, map){
        delete map[item.idCCIDmetadata]
	}

	setMainCheckbox = function(){
        if(Object.keys($scope.ctrl.selectedItems).length !== $scope.ctrl.data.rows.length){
            $scope.allSelected = false
        }
        if(Object.keys($scope.ctrl.selectedItems).length === $scope.ctrl.data.rows.length){
            $scope.allSelected = true
        }
	}

	addOrRemoveSelectedItem = function (item,map){
        if(!map[item.idCCIDmetadata]){
            addItem(item, map)
        }else{
            removeItem(item, map)
           }
	};

	differentCurrencies = function(values){
	    	 var valueArr = _.values(values).map(function(item){ return item.currency });
	    	 var value = ''
	    	 var differentCurrency = valueArr.some(function(item, index, array){ 
	    	 var tmp = item      
	    	    if(value === '')
	    	    		value = tmp
	    	    return value != tmp 
	    	 });
	    	 return differentCurrency 
	}
     


	$scope.selectionIsEmpty = function(){
    		return isObjectEmpty($scope.ctrl.selectedItems)    	
	}
    
    
	isObjectEmpty = function(obj){
	    	if(Object.keys(obj).length === 0 && obj.constructor === Object){
	    		return true
	    	}
	    	
	    	return false
	}
   

	extractItemAnnotationData = function(item){
	    	 var periodPrefix = ''
	    	 if(item.periodType === 'quarter'){
	    		 periodPrefix = 'Q'
	    	 }
	    	 return item.utilizationType + ' ' + item.year + ' ' + periodPrefix + item.period
	}
     
  
	$scope.formatNumber = function(number){
 		return twoDecimalRound(number);
	}
 
    
      

	$scope.setUUID = function(UUID){
    	 	$scope.requestUUID = UUID	
	}
     
	
	$scope.edit = function(item){

        ngDialog.open({
            template: 'pages/advance-payment/dialog-update-ccid.html',
            plain: false,
            width: '60%',
            scope: $scope,
            controller: ['$scope','ngDialog', '$http', function($scope, ngDialog, $http) {
            	var msInvoiceApiUrl = $scope.ctrl.msInvoiceApiUrl
            		$scope.ctrl = {
            			item: item
    				};
                $scope.cancel = function() {
                    ngDialog.closeAll(false);
                };
	            	$scope.formatNumber = function(number){
	             	return twoDecimalRound(number);
	            	}
                $scope.save = function(usableAmount){
                	var amount=usableAmount.replace(",",".");
                	
                	$http({
            			method: 'POST',
            			url: msInvoiceApiUrl + 'invoice/all/updateccid',
            			headers: {
                            'Content-Type': 'application/json'},
            			data: {
            				idCCIDMetadata: item.idCCIDmetadata,
            				invoiceAmaunt: amount
            			}
            		}).then(function successCallback(response) {
                      ngDialog.closeAll(false);
                      $scope.showAlert("Esito Richiesta","Valore fatturabile aggiornato correttamente")
            		}, function errorCallback(response) {
            			// $scope.ctrl.data.rows = [];
            			if(response!=null&&response.data!=null&&response.data.message!=null){
                     	$scope.showAlert("Esito Richiesta",response.data.message)
            			}else{
            				$scope.showAlert("Esito Richiesta","Errore imprevisto, riprovare più tardi")
            			}
            		});
                	
                };
             	$scope.formatNumber = function(number){
             		return twoDecimalRound(number);
            	};
                
                $scope.checkValidity = function(){
               	 if(_.find($scope.codeSelectionList, { 'id': $scope.selectedCodeModel.id})){
               		 return false
               	 }
               	 return true
                };

        	}]
        		}).closePromise.then(function (data) {
        			 $scope.getPagedResults(0,
     						$scope.ctrl.maxrows,
     						$scope.ctrl.filterParameters.client,
     						$scope.ctrl.filterParameters.dsp,
     						$scope.ctrl.filterParameters.countryList,
     						$scope.ctrl.filterParameters.utilizationList,
     						$scope.ctrl.filterParameters.offerList,
     						$scope.ctrl.filterParameters.monthFrom,
     						$scope.ctrl.filterParameters.yearFrom,
     						$scope.ctrl.filterParameters.monthTo,
     						$scope.ctrl.filterParameters.yearTo
     						);
        		});
	};

	$scope.setUUID = function(UUID){
    	 	$scope.requestUUID = UUID	
	}
     
	$scope.showAlert = function(title, message) {
		ngDialog.open({
			template: 'pages/advance-payment/dialog-alert.html',
			plain: false,
			width: '50%',
			controller: ['$scope', 'ngDialog', function($scope, ngDialog) {	
				var ngDialogId=$scope.ngDialogId;
				$scope.ctrl = {
					title: title,
					message: message,
					ngDialogId: ngDialogId
				};
				$scope.cancel = function(ngDialogId) {
					ngDialog.close(ngDialogId);
				};
			}]
		}).closePromise.then(function (data) {
			if (data.value === true) {
				
			}
		});
	};

		 function getCollectionValues(collection) {
			var selectedList = [];
			if (collection !== 'undefined') {
				for (var e in collection) {
					selectedList.push(collection[e].id);
				}
			}
			return selectedList;
		}

	$scope.apply = function (req) {
		var periodFrom = 0;
		var yearFrom = 0;
		var periodTo = 0;
		var yearTo = 0;

		if((req.selectedPeriodFrom !== undefined)){
			var from = req.selectedPeriodFrom.split("-");
			periodFrom = parseInt(from[0],10);
			yearFrom = parseInt(from[1],10);
		}

		if(req.selectedPeriodTo !== undefined){
			var to = req.selectedPeriodTo.split("-");
			periodTo = parseInt(to[0],10);
			yearTo = parseInt(to[1],10);

		}

		var parameters={
			client: req.selectedClient,
			dsp: getCollectionValues(req.selectedDspModel),
			countryList : getCollectionValues(req.selectedCountryModel),
			utilizationList : getCollectionValues(req.selectedUtilizationModel),
			offerList : getCollectionValues(req.selectedOfferModel),
			monthFrom : periodFrom ,
			yearFrom : yearFrom ,
			monthTo : periodTo,
			yearTo : yearTo,
			hideForm : false
		};
		$scope.ctrl.filterParameters = parameters;
		$scope.getPagedResults(0,
			$scope.ctrl.maxrows,
			parameters.client,
			parameters.dsp,
			parameters.countryList,
			parameters.utilizationList,
			parameters.offerList,
			parameters.monthFrom,
			parameters.yearFrom,
			parameters.monthTo,
			parameters.yearTo
		);
	};
	
	$scope.$on('filterApply', function(event, parameters) {
		 $scope.ctrl.filterParameters = parameters;
		 $scope.getPagedResults(0,
					$scope.ctrl.maxrows,
					parameters.client,
					parameters.dsp,
					parameters.countryList,
					parameters.utilizationList,
					parameters.offerList,
					parameters.monthFrom,
					parameters.yearFrom,
					parameters.monthTo,
					parameters.yearTo
					);


	});
	$scope.onRefresh();
	$scope.selectedCodeModel = [];
	$scope.selectedDescriptionModel = [];
	$scope.multiselectSettings = {
        scrollableHeight: '200px',
        scrollable: false,
        enableSearch: true,
        selectionLimit: 1,
        showUncheckAll: false,
        searchBr: true,
        checkboxes: false
	};

}]);



codmanApp.filter('range', function() {
	  return function(input, min, max) {
		    min = parseInt(min); // Make string input int
		    max = parseInt(max);
		    for (var i=min; i<=max; i++)
		      input.push(i);
		    return input;
		  };
});

codmanApp.filter('month_range', function() {
	  return function(input, min, max) {
		    min = parseInt(min); // Make string input int
		    max = parseInt(max);
		    for (var i=min; i<=max; i++)
		      input.push(i + "M");
		    return input;
		  };
});

