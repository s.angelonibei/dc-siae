(function() {
  /*
         * RiconoscimentoCtrl
         *
         * @path /riconoscimento
         */
  angular.module('codmanApp').controller('RiconoscimentoCtrl', ['$scope', 'ngDialog', '$routeParams', '$location', '$route', '$http', 'riconoscimentoService', 'preRiconoscimentoService', 'UtilService',
    function($scope, ngDialog, $routeParams, $location, $route, $http, riconoscimentoService, preRiconoscimentoService, UtilService) {
      $scope.ctrl = {
        //   navbarTab: 'riconoscimento'
      };

      function reloadPageWithNewParams() {
        $scope.ctrl.unidentifiedSong = {};
        $scope.ctrl.unidentifiedSongDsr = [];
        $scope.ctrl.solrResults = [];
        var search = $routeParams;
        Object.keys(search).forEach(function(key) {
          $location.search(key, search[key]);
        });
        $location.path($location.path());
      }

      function searchDsrAndSolr() {
        $scope.ctrl.navbarTab = 'riconoscimento';
        $http({
          method: 'GET',
          url: 'rest/unidentifiedSongDsr/byHashId/' + $scope.ctrl.unidentifiedSong.hashId
        }).then(function(response) {
          $scope.ctrl.unidentifiedSongDsr = response.data;
        }, function(response) {
          $scope.ctrl.unidentifiedSongDsr = [];
        });
        // search on solr
        $http({
          method: 'GET',
          url: 'rest/solr/search',
          params: {
            title: $scope.ctrl.unidentifiedSong.title,
            artists: $scope.ctrl.unidentifiedSong.artists,
            roles: $scope.ctrl.unidentifiedSong.roles
          }
        }).then(function(response) {
          $scope.ctrl.solrResults = response.data;
        }).catch(function(response) {
          $scope.ctrl.solrResults = [];
        });
      }

      $scope.onRefresh = function() {
        window.addEventListener('unload', function(e) {
          navigator.sendBeacon(riconoscimentoService.UNLOCK_OPERE);
        }, false);
        $scope.ctrl.unidentifiedSong = preRiconoscimentoService;
        $scope.ctrl.search = $routeParams;
        searchDsrAndSolr();
      };

      $scope.showKbIdentifyError = function(identifyResult) {
        ngDialog.open({
          template: 'pages/riconoscimento/dialog-kb-identify-error.html',
          plain: false,
          width: '60%',
          scope: $scope,
          controller: ['$scope', 'ngDialog', function(scope, dialog) {
            scope.ctrl.identifyResult = identifyResult;
            scope.cancel = function() {
              dialog.closeAll(false);
            };
            scope.save = function() {
              dialog.closeAll(true);
            };
          }]
        });
      };

      $scope.goToRicerca = function(user) {
        var search = $routeParams;
        Object.keys(search).forEach(function(key) {
          $location.search(key, search[key]);
        });
        $location.path('/ricerca');
      };

      $scope.showMoveToNext = function(user) {
        var params = {
          unidentifiedSong: $scope.ctrl.unidentifiedSong
        };
        Object.keys($scope.ctrl.search).forEach(function(key) {
          params[key] = $scope.ctrl.search[key];
        });

        $http({
          method: 'GET',
          url: 'rest/unidentified-song',
          params: params
        }).then(function successCallback(response) {
          $scope.ctrl.unidentifiedSong = response.data;
          $scope.ctrl.search.hashId = response.data.hashId;
          reloadPageWithNewParams()
        });
      };

      $scope.showReject = function(user) {
        ngDialog.open({
          template: 'pages/riconoscimento/dialog-reject-song.html',
          plain: false,
          width: '50%',
          scope: $scope,
          controller: ['$scope', 'ngDialog', '$http', function(scope, dialog, http) {
            scope.cancel = function() {
              dialog.closeAll(false);
            };
            scope.save = function() {
              http({
                method: 'PATCH',
                url: 'rest/unidentified-song/reject',
                data: scope.ctrl.unidentifiedSong
              }).then(function successCallback(response) {
                dialog.closeAll(true);
              }).catch(function(response) {
                dialog.closeAll(true);
              });
            };
          }]
        }).closePromise.then(function(data) {
          if (data.value === true) {
            $scope.showMoveToNext();
          }
        });
      };

      $scope.showSolrResult = function(event, solrResult) {
        ngDialog.open({
          template: 'pages/riconoscimento/dialog-solr-result-details.html',
          plain: false,
          width: '60%',
          scope: $scope,
          controller: ['$scope', 'ngDialog', function(scope, dialog) {
            scope.ctrl.solrResult = solrResult;
            scope.cancel = function() {
              dialog.closeAll(false);
            };
            scope.save = function() {
              dialog.closeAll(true);
            };
          }]
        })
      };

      $scope.showInsertCode = function(user) {
        ngDialog.open({
          template: 'pages/riconoscimento/dialog-insert-code.html',
          plain: false,
          width: '50%',
          scope: $scope,
          controller: ['$scope', 'ngDialog', '$http', function(scope, dialog, http) {
            scope.cancel = function() {
              ngDialog.closeAll(false);
            };
            scope.save = function() {
              http({
                method: 'PUT',
                url: 'rest/identifiedSong/manual',
                data: {
                  hashId: $scope.ctrl.unidentifiedSong.hashId,
                  title: $scope.ctrl.unidentifiedSong.title,
                  artists: $scope.ctrl.unidentifiedSong.artists,
                  siadaTitle: $scope.ctrl.unidentifiedSong.siadaTitle,
                  siadaArtists: $scope.ctrl.unidentifiedSong.siadaArtists,
                  roles: $scope.ctrl.unidentifiedSong.roles,
                  siaeWorkCode: scope.ctrl.siaeWorkCode
                }
              }).then(function successCallback(response) {
                dialog.closeAll(response);
              }).catch(function errorCallback(response) {
                dialog.closeAll(response);
              });
            };
          }]
        }).closePromise.then(function(response) {
          if (response.value && response.value.data && response.value.data.status === 'OK') {
            $scope.showMoveToNext();
          } else if (response.value && response.value.data && response.value.data.status === 'KO') {
            UtilService.handleHttpRejection(response, undefined, true);
          }
        });
      };

      $scope.showChooseSolrResult = function(event, unidentifiedSong, solrResult) {
        ngDialog.open({
          template: 'pages/riconoscimento/dialog-choose-solr-result.html',
          plain: false,
          width: '50%',
          scope: $scope,
          controller: ['$scope', 'ngDialog', '$http', function(scope, dialog, http) {
            scope.ctrl.solrResult = solrResult;
            scope.cancel = function() {
              dialog.closeAll(false);
            };
            scope.save = function() {
              http({
                method: 'PUT',
                url: 'rest/identifiedSong/manual',
                data: {
                  hashId: $scope.ctrl.unidentifiedSong.hashId,
                  title: $scope.ctrl.unidentifiedSong.title,
                  artists: $scope.ctrl.unidentifiedSong.artists,
                  siadaTitle: $scope.ctrl.unidentifiedSong.siadaTitle,
                  siadaArtists: $scope.ctrl.unidentifiedSong.siadaArtists,
                  roles: $scope.ctrl.unidentifiedSong.roles,
                  siaeWorkCode: scope.ctrl.solrResult.opera.codiceOpera
                }
              }).then(function successCallback(response) {
                dialog.closeAll(response);
              }).catch(function errorCallback(response) {
                dialog.closeAll(response);
              });
            };
          }]
        }).closePromise.then(function(response) {
          if (response.value && response.value.data && response.value.data.status === 'OK') {
            $scope.showMoveToNext();
          } else if (response.value && response.value.data && response.value.data.status === 'KO') {
            UtilService.handleHttpRejection(response, undefined, true);
          }
        });
      };

      $scope.showProvisionalCode = function(user) {
        ngDialog.open({
          template: 'pages/riconoscimento/dialog-provisional-code.html',
          plain: false,
          width: '50%',
          scope: $scope,
          controller: ['$scope', 'ngDialog', '$http', function(scope, dialog, http) {
            scope.cancel = function() {
              dialog.closeAll(false);
            };
            scope.save = function() {
              http({
                method: 'PUT',
                url: 'rest/identifiedSong/provisional',
                data: {
                  hashId: $scope.ctrl.unidentifiedSong.hashId,
                  title: $scope.ctrl.unidentifiedSong.title,
                  artists: $scope.ctrl.unidentifiedSong.artists,
                  siadaTitle: $scope.ctrl.unidentifiedSong.siadaTitle,
                  siadaArtists: $scope.ctrl.unidentifiedSong.siadaArtists,
                  roles: $scope.ctrl.unidentifiedSong.roles
                }
              }).then(function successCallback(response) {
                dialog.closeAll(response);
              }).catch(function errorCallback(response) {
                dialog.closeAll(response);
              });
            };
          }]
        }).closePromise.then(function(response) {
          if (response.value && response.value.data && response.value.data.status === 'OK') {
            $scope.showMoveToNext();
          } else if (response.value && response.value.data && response.value.data.status === 'KO') {
            UtilService.handleHttpRejection(response, undefined, true);
          }
        });
      };

      $scope.showUnmanagedCode = function(user) {
        ngDialog.open({
          template: 'pages/riconoscimento/dialog-unmanaged-code.html',
          plain: false,
          width: '50%',
          scope: $scope,
          controller: ['$scope', 'ngDialog', '$http', function(scope, dialog, http) {
            scope.cancel = function() {
              dialog.closeAll(false);
            };
            scope.save = function() {
              http({
                method: 'PUT',
                url: 'rest/identifiedSong/manual',
                data: {
                  hashId: $scope.ctrl.unidentifiedSong.hashId,
                  title: $scope.ctrl.unidentifiedSong.title,
                  artists: $scope.ctrl.unidentifiedSong.artists,
                  siadaTitle: $scope.ctrl.unidentifiedSong.siadaTitle,
                  siadaArtists: $scope.ctrl.unidentifiedSong.siadaArtists,
                  roles: $scope.ctrl.unidentifiedSong.roles,
                  siaeWorkCode: 'NOT_MANAGED'
                }
              }).then(function successCallback(response) {
                dialog.closeAll(response);
              }).catch(function errorCallback(response) {
                dialog.closeAll(response);
              });
            };
          }]
        }).closePromise.then(function(response) {
          if (response.value && response.value.data && response.value.data.status === 'OK') {
            $scope.showMoveToNext();
          } else if (response.value && response.value.data && response.value.data.status === 'KO') {
            UtilService.handleHttpRejection(response, undefined, true);
          }
        });
      };

      $scope.showNotFound = function() {
        ngDialog.open({
          template: 'pages/riconoscimento/dialog-not-found.html',
          plain: false,
          width: '50%',
          controller: ['$scope', 'ngDialog', function(scope, dialog) {
            scope.cancel = function() {
              dialog.closeAll(false);
            };
            scope.save = function() {
              dialog.closeAll(true);
            };
          }]
        });
      };

      $scope.onRefresh();
    }]);
})();

