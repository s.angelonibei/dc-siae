(function () {
  angular
    .module('codmanApp')
    .service('riconoscimentoService', riconoscimentoService);

  riconoscimentoService.$inject = ['$http', 'UtilService', '$routeParams', '$q'];

  function riconoscimentoService($http, UtilService, $routeParams, $q) {
    var vm = this;
    vm.getOpera = getOpera;
    vm.unlockOpere = unlockOpere;
    vm.ROOT_URL = 'rest/unidentified-song/';
    vm.UNLOCK_OPERE = vm.ROOT_URL + 'unlock';

    function getOpera(hashId, user) {
      var newHashId = hashId;
      if (!hashId) {
        newHashId = $routeParams ? $routeParams.hashId : newHashId;
      }
      return $http({
        method: 'GET',
        url: vm.ROOT_URL + newHashId,
        params: {
          user: (user ? user : '')
        }
      }).then(getOperaComplete)
        .catch(getOperaFailed);

      function getOperaComplete(response) {
        if (response.status !== 204) {
          return response.data;
        }
        UtilService.showAlert('Errore!', 'Un altro utente sta già lavorando su quest\'opera.');
        return $q.reject(response);
      }

      function getOperaFailed(response) {
        if (response.status !== 500) {
          UtilService.showAlert('Errore!', 'Un altro utente sta già lavorando su quest\'opera.');
          return $q.reject(response);
        }
        UtilService.showAlert('Attenzione!', 'Non sono presenti opere da codificare.');
        return $q.reject(response);
      }
    }

    function unlockOpere(user) {
      return $http({
        method: 'GET',
        url: vm.UNLOCK_OPERE,
        params: {
          user: (user ? user : '')
        }
      }).then(unlockOpereComplete())
        .catch(unlockOpereFailed);

      function unlockOpereComplete(response) {
        if (response.status !== 204) {
          return response.data;
        }
        return $q.reject(response);
      }

      function unlockOpereFailed(response) {
        UtilService.showAlert('Errore!', 'Errore inatteso, riprovare in un secondo momento');
        return $q.reject(response);
      }
    }
  }
}());
