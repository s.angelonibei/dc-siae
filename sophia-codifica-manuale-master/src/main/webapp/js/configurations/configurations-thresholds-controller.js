angular.module('configurationsApp').controller('configurationsCodificaThresholdsCtrl',
    ['configurationsService', '$scope','configurations','configurationHistory', function(configurationsService, $scope, configurations, configurationHistory) {

        $scope.pageTitle="Configurazione Soglie di Codifica"
        $scope.thresholdsLabel="Configurazione Soglie"

        $scope.messages = {
            info: "", 
            error: ""
        }

        $scope.clearMessages = function() {
            $scope.messages = {
                info: "", 
                error: ""
            }
        }
    
        $scope.infoMessage = function(message) {
            $scope.messages.info = message;
        }
    
        $scope.errorMessage = function(message) {
            $scope.messages.error = message;
        }

        $scope.historyData = {
            label: "Storico Configurazioni",
            configurations: configurationHistory
        };


        $scope.configuration = configurations[0];
        $scope.currentConfiguration = _.cloneDeep($scope.configuration);

        $scope.showHistoryConfiguration = function (configId) {
            $scope.clearMessages();
            configurationsService.loadConfigurationById(configId).then(function success(result) {
                $scope.configuration = result;
                $scope.collapseConf = false;
            }, function error(error) {
                // TODO aprire modale con error
                console.error('Error', error);
            });
            $('#panel')[0].scrollIntoView( true );
            window.scrollBy(0, -100);
        }
        
        $scope.showConfiguration = function (id) {
            $scope.clearMessages();
            for (let c of $scope.activeConfigurations.configurations) {
                if (c.id === id) {
                    $scope.configuration = c;
                    break;
                }
            }
            $('#panel')[0].scrollIntoView( true );
            window.scrollBy(0, -100);
        }

        $scope.returnToCurrentConf = function () {
            $scope.clearMessages();
            $scope.configuration = _.cloneDeep($scope.currentConfiguration);
            $('#panel')[0].scrollIntoView( true );
            window.scrollBy(0, -100);
        }

  
        $scope.saveConfiguration = function () {
            $scope.clearMessages(); 
            configurationsService.saveConfiguration($scope.configuration).then(function success(result) {
                $scope.configuration = result;
                $scope.currentConfiguration = $scope.configuration;
                configurationsService.loadConfigurationHistory('history/codifica/thresholds').then(
                    function success(result) {
                        $scope.historyData.configurations = result;
                        $scope.collapseHistory = false;
                    }, function error(error) {
                        console.error('Error', error);
                    });
            }, function error(error) {
                if (error.status != 400) {
                    $scope.errorMessage(" Si è verificato un errore, riprovare più tardi!");
                    $('#panel')[0].scrollIntoView( true );
                    window.scrollBy(0, -100);

                } else {
                    $scope.configuration = error.data;
                    $scope.currentConfiguration = _.cloneDeep($scope.configuration);
                    $scope.errorMessage(" Dati non validi!");
                    $('#panel')[0].scrollIntoView( true );
                    window.scrollBy(0, -100);
                }
            });
        }

        $scope.formatDate = function (dateStr) {
            return moment(dateStr).format('LLL');
        }

        $scope.saveAsNew = function () {
            $scope.clearMessages();
            $scope.configuration.id = $scope.currentConfiguration.id;
            $scope.configuration.validTo = null;
            $scope.configuration.active = true;
            configurationsService.saveConfiguration($scope.configuration).then(function success(result) {
                $scope.configuration = result;
                $scope.currentConfiguration = $scope.configuration;
                configurationsService.loadConfigurationHistory('history/codifica/thresholds').then(
                    function success(result) {
                        $scope.historyData.configurations = result;
                        $scope.collapseHistory = false;
                    }, function error(error) {
                        console.error('Error', error);
                    });
            }, function error(error) {
                if (error.status != 400) {
                    $scope.errorMessage(" Si è verificato un errore, riprovare più tardi!");
                    $('#panel')[0].scrollIntoView( true );
                    window.scrollBy(0, -100);

                } else {
                    $scope.configuration = error.data;
                    $scope.currentConfiguration = _.cloneDeep($scope.configuration);
                    $scope.errorMessage(" Dati non validi!");
                    $('#panel')[0].scrollIntoView( true );
                    window.scrollBy(0, -100);
                }
            });
        }
    }
    ]);