angular.module('configurationsApp').factory('configurationsService',
    ['$http', '$q', 'urls',
        function ($http, $q, urls) {
 
            var factory = {
                loadConfiguration: loadConfiguration,
                saveConfiguration: saveConfiguration,
                loadConfigurationHistory: loadConfigurationHistory,
                loadConfigurationById: loadConfigurationById,
                adaptSettings: adaptSettings,
                deleteConfiguration: deleteConfiguration
            };
 
            return factory;
 
            function loadConfiguration(path) {
                var deferred = $q.defer();
                $http.get(`${urls.SERVICE_API}/${path}`)
                    .then(
                        function (response) {
                            response.data.forEach(config => {
                                adaptSettings(config);
                            });
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                            console.error('Error while fetching!',errResponse);
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }
  
            function saveConfiguration(configuration) {
                console.log('Creating ...', configuration);
                var deferred = $q.defer();
                $http.post(urls.SERVICE_API, configuration)
                    .then(
                        function (response) {
                            console.log('Creations successful!',response.data);
                            adaptSettings(response.data);
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                            adaptSettings(errResponse.data);
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

            function loadConfigurationById(id) {
                var deferred = $q.defer();
                $http.get(`${urls.SERVICE_API}/${id}`)
                    .then(
                        function (response) {
                            adaptSettings(response.data);
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                            console.error('Error while fetching!',errResponse);
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }            

            function loadConfigurationHistory(path) {
                var deferred = $q.defer();
                $http.get(`${urls.SERVICE_API}/${path}`)
                    .then(
                        function (response) {
                            response.data.forEach(config => {
                                adaptSettings(config);
                            });
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                            console.error('Error while fetching!',errResponse);
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

            function adaptSettings(configuration) {
                if(configuration.settings) {
                    configuration.settings.forEach(setting => {
                        if(setting.valueType=='Boolean'){
                            setting.value = setting.value.toLowerCase() == 'true' || setting.value.toLowerCase() == '1' ? true : false; 
                        }
                        if(setting.valueType=='Integer'){
                            setting.value = parseInt(setting.value, 10);
                        }
                        if(setting.valueType=='Decimal'){
                            setting.value = parseFloat(setting.value);
                        }
                    });
                }
                if(configuration.configurations) {
                    configuration.configurations.forEach(config => {
                        adaptSettings(config);
                    });
                }
            }

            function deleteConfiguration(configuration) {
                return $http({
                    method: 'DELETE',
                    url: `${urls.SERVICE_API}`,
                    data: configuration,
                    headers: {'Content-Type': 'application/json'}
                });
            }


 
        }
    ]);