
var app = angular.module('configurationsApp', []);

app.constant('urls', {
    SERVICE_API: '/cruscottoUtilizzazioni/rest/performing/configurations'
});

app.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/configurations/codifica/view-info', {
        templateUrl: 'pages/configurations/codifica/view-info.jsp',
        controller: 'configurationsCodificaViewInfoCtrl',
        controllerAs: 'ctrl',
        resolve: {
            configurations: function ($q, configurationsService) {
                var deferred = $q.defer();
                configurationsService.loadConfiguration('codifica/view-info').then(deferred.resolve, deferred.reject);
                return deferred.promise;
            },
            readOnlyConfigurations: function ($q, configurationsService) {
                var deferred = $q.defer();
                configurationsService.loadConfiguration('codifica/ranking').then(deferred.resolve, deferred.reject);
                return deferred.promise;
            },
            configurationHistory: function ($q, configurationsService) {
                var deferred = $q.defer();
                configurationsService.loadConfigurationHistory('history/codifica/view-info').then(deferred.resolve, deferred.reject);
                return deferred.promise;
            }
        }
    })
    .when('/configurations/codifica/ranking', {
        templateUrl: 'pages/configurations/codifica/ranking.jsp',
        controller: 'configurationsCodificaRankingCtrl',
        controllerAs: 'ctrl',
        resolve: {
            configurations: function ($q, configurationsService) {
                var deferred = $q.defer();
                configurationsService.loadConfiguration('codifica/ranking').then(deferred.resolve, deferred.reject);
                return deferred.promise;
            },
            configurationHistory: function ($q, configurationsService) {
                var deferred = $q.defer();
                configurationsService.loadConfigurationHistory('history/codifica/ranking').then(deferred.resolve, deferred.reject);
                return deferred.promise;
            }
        }
    }).when('/configurations/codifica/thresholds', {
        templateUrl: 'pages/configurations/codifica/thresholds.jsp',
        controller: 'configurationsCodificaThresholdsCtrl',
        controllerAs: 'ctrl',
        resolve: {
            configurations: function ($q, configurationsService) {
                var deferred = $q.defer();
                configurationsService.loadConfiguration('codifica/thresholds').then(deferred.resolve, deferred.reject);
                // deferred.resolve("dummy");
                return deferred.promise;
            },
            configurationHistory: function ($q, configurationsService) {
                var deferred = $q.defer();
               configurationsService.loadConfigurationHistory('history/codifica/thresholds').then(deferred.resolve, deferred.reject);
                return deferred.promise;
            }

        }
    });
    
    
    
}]);