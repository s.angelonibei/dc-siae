angular.module('configurationsApp').controller('configurationsCodificaViewInfoCtrl',
    ['configurationsService', '$scope', 'configurations', 'configurationHistory', 'readOnlyConfigurations','configurationsUtilityService', 
    '$sce', function (configurationsService, $scope, configurations, configurationHistory, readOnlyConfigurations, configurationsUtilityService, $sce) {

        $scope.pageTitle = "Informazioni Visualizzate";
        $scope.thresholdsLabel = "Configurazione Soglie";

        $scope.messages = {
            info: "", 
            error: ""
        }

        $scope.clearMessages = function() {
            $scope.messages = {
                info: "", 
                error: ""
            }
        }
    
        $scope.infoMessage = function(message) {
            $scope.messages.info = message;
        }
    
        $scope.errorMessage = function(message) {
            $scope.messages.error = message;
        }

        $scope.historyData = {
            label: "Storico Configurazioni",
            configurations: configurationHistory
        };

        (function () {
            try {
                $scope.periodiMaturato = { ...readOnlyConfigurations[0].settings.find(s => s.key == 'relevance_flag').thresholds.find(t => t.key === 'relevance_period')};
                $scope.periodiRischio = { ...readOnlyConfigurations[0].settings.find(s => s.key == 'risk_flag').thresholds.find(t => t.key === 'risk_period')};
            }
            catch (e) {
                console.log('Cannot initialize read only properties', e);
            }
        })();     

        $scope.configuration = configurations[0];
        $scope.currentConfiguration = _.cloneDeep($scope.configuration);

        $scope.showHistoryConfiguration = function (config_id) {
            $scope.clearMessages();
            configurationsService.loadConfigurationById(config_id).then(function success(result) {
                $scope.configuration = result;
                $scope.collapseConf = false;                
            }, function error(error) {
                // TODO aprire modale con errore
                console.error('Error', error);
            });
            $('#panel')[0].scrollIntoView( true );
            window.scrollBy(0, -100);
        }

        $scope.returnToCurrentConf = function () {
            $scope.clearMessages();
            $scope.configuration = _.cloneDeep($scope.currentConfiguration);
            $('#panel')[0].scrollIntoView( true );
            window.scrollBy(0, -100);
        }

        $scope.saveConfiguration = function () {
            $scope.clearMessages();
            configurationsService.saveConfiguration($scope.configuration).then(function success(result) {
                $scope.configuration = result;
                $scope.currentConfiguration = $scope.configuration;
                configurationsService.loadConfigurationHistory('history/codifica/view-info').then(
                    function success(result) {
                        $scope.historyData.configurations= result;
                        $scope.collapseHistory = false;
                    }, function error(error) {
                        console.error('Error', error);
                    });
            }, function error(error) {
                if (error.status != 400) {
                    $scope.errorMessage(" Si è verificato un errore, riprovare più tardi!");
                    $('#panel')[0].scrollIntoView( true );
                    window.scrollBy(0, -100);

                } else {
                    $scope.configuration = error.data;
                    $scope.currentConfiguration = _.cloneDeep($scope.configuration);
                    $scope.errorMessage(" Dati non validi!");
                    $('#panel')[0].scrollIntoView( true );
                    window.scrollBy(0, -100);
                }
            });
        }

        $scope.adjustLastThreshold = function (index, thresholds){
            if(index == thresholds.length - 2){
                thresholds[index+1].value = thresholds[index].value;
            }
        }

        $scope.formatDate = function (dateStr) {
            return moment(dateStr).format('LLL');
        }

        $scope.saveAsNew = function () {
            $scope.clearMessages();
            $scope.configuration.id = $scope.currentConfiguration.id;
            $scope.configuration.validTo = null;
            $scope.configuration.active = true;
            configurationsService.saveConfiguration($scope.configuration).then(function success(result) {
                $scope.configuration = result;
                $scope.currentConfiguration = $scope.configuration;
                configurationsService.loadConfigurationHistory('history/codifica/view-info').then(
                    function success(result) {
                        $scope.historyData.configurations = result;
                        $scope.collapseHistory = false;
                    }, function error(error) {
                        console.error('Error', error);
                    });
            }, function error(error) {
                if (error.status != 400) {
                    $scope.errorMessage(" Si è verificato un errore, riprovare più tardi!");
                    $('#panel')[0].scrollIntoView( true );
                    window.scrollBy(0, -100);

                } else {
                    $scope.configuration = error.data;
                    $scope.currentConfiguration = _.cloneDeep($scope.configuration);
                    $scope.errorMessage(" Dati non validi!");
                    $('#panel')[0].scrollIntoView( true );
                    window.scrollBy(0, -100);
                }
            });
        }

        $scope.getMaturatoHtml = function (maturato) {
            return $sce.trustAsHtml(configurationsUtilityService.getMaturato(maturato));
        }

        $scope.getRischioHtml = function (rischio) {
            return $sce.trustAsHtml(configurationsUtilityService.getRischio(rischio));
        }

    }
    ]);