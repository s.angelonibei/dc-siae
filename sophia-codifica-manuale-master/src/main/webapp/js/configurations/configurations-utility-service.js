angular.module("configurationsApp").factory("configurationsUtilityService", [
"$filter", function($filter) {
    var factory = {
      getMaturato: getMaturato,
      getRischio: getRischio,
      getStatoOpera: getStatoOpera
    };

    return factory;

    function getMaturato(maturato) {
      switch (maturato) {
        case "€":
          return "<i class=\"fas fa-euro-sign\"></i>";
          break;
        case "€€":
          return "<i class=\"fas fa-euro-sign\"></i><i class=\"fas fa-euro-sign\"></i>";
          break;
        case "€€€":
          return "<i class=\"fas fa-euro-sign\"></i><i class=\"fas fa-euro-sign\"></i><i class=\"fas fa-euro-sign\"></i>";
          break;
        case "€€€€":
          return "<i class=\"fas fa-euro-sign\"></i><i class=\"fas fa-euro-sign\"></i><i class=\"fas fa-euro-sign\"></i><i class=\"fas fa-euro-sign\"></i>";
          break;
        default:
          return $filter('currency')(maturato,'&euro;',2);
      }
    }

    function getRischio(rischio) {
        switch (rischio) {
          case "!":
            return "<i class=\"fas fa-exclamation-triangle\" style=\"color:#39E506;\"></i>";
            break;
          case "!!":
            return "<i class=\"fas fa-exclamation-triangle\" style=\"color:#F9F516;\"></i>";
            break;
          case "!!!":
            return "<i class=\"fas fa-exclamation-triangle\" style=\"color:#F31111;\"></i>";
            break;
          default:
            return $filter('number')(rischio,2);
        }
      }

      function getStatoOpera(proposta) {
        var result = '';
        if (proposta.statoOpera && proposta.statoOpera == 'Regolare' && proposta.flagPD == '1') {
            result="<i class=\"far fa-circle\" data-toggle=\"tooltip-stato-opera\" title=\"Regolare e di Pubblico Dominio\" data-html=\"true\" rel=\"tooltip\"></i>"+
            "<script type=\"text/javascript\">$('[data-toggle=\"tooltip-stato-opera\"]').tooltip();</script>"; 
            }
        else if(proposta.statoOpera && proposta.statoOpera == 'Regolare' && proposta.flagEL == '1') { 
            result = "<i class=\"fas fa-circle\" style=\"color:yellow\" data-toggle=\"tooltip-stato-opera\" title=\"Regolare ed Elaborata\" data-html=\"true\" rel=\"tooltip\"></i>"+
            "<script type=\"text/javascript\">$('[data-toggle=\"tooltip-stato-opera\"]').tooltip();</script>"; 
            }
        else if(proposta.statoOpera && proposta.statoOpera == 'Regolare') {
            result="<i class=\"fas fa-circle\" style=\"color:#39E506\" data-toggle=\"tooltip-stato-opera\" title=\"Regolare\" data-html=\"true\" rel=\"tooltip\"></i>"+
            "<script type=\"text/javascript\">$('[data-toggle=\"tooltip-stato-opera\"]').tooltip();</script>"; 
        }
        else if(proposta.statoOpera && proposta.statoOpera == 'Irregolare') {
            result = "<i class=\"fas fa-circle\" style=\"color:red\" data-toggle=\"tooltip-stato-opera\" title=\"Irregolare\" data-html=\"true\" rel=\"tooltip\"></i>"+
            "<script type=\"text/javascript\">$('[data-toggle=\"tooltip-stato-opera\"]').tooltip();</script>"; 
        }
            return result;
        }
      
  }
]);
