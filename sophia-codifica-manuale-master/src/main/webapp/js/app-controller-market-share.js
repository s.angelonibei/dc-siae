codmanApp.controller('marketShareCtrl',
	['$http', '$scope', '$routeParams', '$interval','Upload', 'configService','uploadReclamiService','anagClientData', 'anagClientService', 'dspsUtilizationsCommercialOffersCountries',
		function($http ,$scope, $routeParams, $interval, Upload, configService, uploadReclamiService, anagClientData, anagClientService, dspsUtilizationsCommercialOffersCountries) {

	$scope.dspsUtilizationsCommercialOffersCountries = angular.copy(dspsUtilizationsCommercialOffersCountries);
	$scope.filterParameters = $routeParams;

	$scope.anagClientData = angular.copy(anagClientData);
	$scope.request={};
	$scope.request.selectedCountryModel = [];
    $scope.request.selectedOfferModel = [];
	$scope.request.selectedUtilizationModel = [];
	$scope.request.selectedDsp = [] ;
	$scope.marketShareList = {};
	$scope.exportExcelData = {};

	$scope.firstPaging = {
		currentPage: 1,
		order: "insertTime desc"
	};

	$scope.messages = {
		info: "",
		error: ""
	};

	$scope.paging = {... $scope.firstPaging};

	$scope.clearMessages = function() {
		$scope.messages = {
			info: "",
			error: ""
		}
	};

	$scope.infoMessage = function(message) {
		$scope.messages.info = message;
	};

	$scope.errorMessage = function(message) {
		$scope.messages.error = message;
	};

	$scope.init = function(){
		$scope.clearMessages();

	};

	$scope.navigateToPreviousPage = function () {
		$scope.paging.currentPage = $scope.paging.currentPage - 1;
		$scope.init();
        $scope.filterParameters.first = $scope.filterParameters.first - configService.maxRowsPerPage;
        $scope.filterParameters.last = $scope.filterParameters.last - configService.maxRowsPerPage;
        $scope.searchMarketShare($scope.filterParameters)
	};

	$scope.navigateToNextPage = function () {
		$scope.paging.currentPage = $scope.paging.currentPage + 1;
		$scope.init();
		$scope.filterParameters.first = $scope.filterParameters.first + configService.maxRowsPerPage;
		$scope.filterParameters.last = $scope.filterParameters.last + configService.maxRowsPerPage;
		$scope.searchMarketShare($scope.filterParameters)
	};

	$scope.navigateToEndPage = function () {
		$scope.paging.currentPage = $scope.paging.totPages;
		$scope.init();
	};

	$scope.init();


	$scope.utilizationsOffersCountries = function() {
        $http({
          method: 'GET',
          url: 'rest/data/dspUtilizationsOffersCountries'
        }).then(function successCallback(response) {
          $scope.utilizationList = response.data.utilizations;
          $scope.commercialOffersList = response.data.commercialOffers;
          $scope.countryList = response.data.countries;
          $scope.buildKeySets();

        });

      };

   $scope.utilizationsOffersCountries();
   $scope.clearMessages();


   $scope.buildKeySets = function() {
    $scope.countrySelectionList = [];
    _.forEach($scope.countryList, function(value) {
        $scope.countrySelectionList.push({
        label: value.name,
        id: value.idCountry
      });
	});

    $scope.utilizationSelectionList = [];
    _.forEach($scope.utilizationList, function(value) {
        $scope.utilizationSelectionList.push({
        label: value.name,
        id: value.idUtilizationType
      });
    });

    $scope.offerSelectionList = [];
    _.forEach($scope.commercialOffersList, function(value) {
        $scope.offerSelectionList.push({
        label: value.offering,
        id: value.offering
      });
    });
  };

  $scope.multiselectDsp = {
	scrollableHeight: '200px',
	scrollable: false,
	enableSearch: true,
	displayProp: 'name',
	idProp: 'idDsp'
  };

  $scope.multiselectSettingsFilter = {
	scrollableHeight: '200px',
	scrollable: false,
	enableSearch: true
};

	$scope.countryLabel = function(id){
		const country = $scope.countryList.find(element => element.idCountry == id);
		return country.name;
	};

  $scope.searchMarketShare = function(request,
									  exportAll = false,
									  successCallBack = function(response) { $scope.marketShareList = response.data; }){
	$scope.filterParameters = angular.copy(request);
	$scope.clearMessages();
	var monthFrom, yearFrom, monthTo, yearTo;
	var dspList, countryList, utilizationList, offerList;

    if ((request.selectedPeriodFrom)) {
      var from = request.selectedPeriodFrom.split('-');
      monthFrom = parseInt(from[0], 10);
      yearFrom = parseInt(from[1], 10);
    }

    if (request.selectedPeriodTo) {
      var to = request.selectedPeriodTo.split('-');
      monthTo = parseInt(to[0], 10);
      yearTo = parseInt(to[1], 10);
	}
	$scope.filterParameters.first = $scope.filterParameters.first || 0;
	$scope.filterParameters.last = $scope.filterParameters.last || configService.maxRowsPerPage;
	  $http({
		method: 'GET',
		url: 'rest/dspStatistics/allMarketShare',
		params : {
			first: request.first || 0,
			last: request.last || configService.maxRowsPerPage,
			dspList: $scope.getCollectionValues(request.selectedDspModel),
			countryList: $scope.getCollectionValues(request.selectedCountryModel),
			utilizationList: $scope.getCollectionValues(request.selectedUtilizationModel),
			offerList: $scope.getCollectionValues(request.selectedOfferModel),
			monthFrom: monthFrom? monthFrom : '0',
			yearFrom: yearFrom? yearFrom : '0',
			monthTo: monthTo? monthTo : '0',
			yearTo: yearTo? yearTo : '0',
			exportAll: exportAll
		  }
	}).then(successCallBack,
		  function errorCallback(response) {
		$scope.ctrl.searchResults = {};
	});
  };

  $scope.getCollectionValues = function(collection) {
    var selectedList = [];
    if (collection !== 'undefined') {
      for (const e in collection) {
        selectedList.push(collection[e].id);
      }
    }
    return selectedList;
  };

  $scope.exportExcel = function () {
  	$scope.searchMarketShare($scope.filterParameters, true,
  	function(response) {
  		const rowItem = [['DSP', 'Data da', 'Data a', 'Territorio', 'Tipo di utilizzo', 'Offerta commerciale', 'Totale utilizzazioni', 'Utilizzazioni claim SIAE', 'Market share CCID']];

		angular.forEach(response.data.rows, function (value, key) {
			rowItem.push([value.dsp, new Date(value.dateFrom), new Date(value.dateTo), value.country, value.utilizationType, value.commercialOffer, value.totUsage, value.siaeIdentifiedVisualizations, value.marketShare]);
		});

		const wb = XLSX.utils.book_new();
		wb.SheetNames.push("Market share");
		
		const totUsageTotalCell = {f: 'SUM(G2:G' + rowItem.length + ')'};
		const siaeIdentifiedVisualizationTotalCell = {f: 'SUM(H2:H' + rowItem.length + ')'};
		const marketShareTotalCell = {f: 'SUM(H2:H' + rowItem.length + ')/SUM(' + 'G2:G' + rowItem.length + ')'};

		const totUsageTotalCellRef = XLSX.utils.encode_cell({r: rowItem.length, c: 6});
		const siaeIdentifiedVisualizationTotalCellRef = XLSX.utils.encode_cell({r: rowItem.length, c: 7});
		const marketShareTotalCellRef = XLSX.utils.encode_cell({r: rowItem.length, c: 8});

		const ws = XLSX.utils.aoa_to_sheet(rowItem);
		wb.Sheets["Market share"] = ws;

		ws[totUsageTotalCellRef] = totUsageTotalCell;
		ws[siaeIdentifiedVisualizationTotalCellRef] = siaeIdentifiedVisualizationTotalCell;
		ws[marketShareTotalCellRef] = marketShareTotalCell;

		ws['!ref'] = XLSX.utils.encode_range({
			s: {c: 0, r: 0},
			e: marketShareTotalCellRef
		});

		const wopts = {bookType: 'xlsx', bookSST: true, type: 'binary'};

		const wbout = XLSX.write(wb, wopts);

		saveAs(new Blob([$scope.s2ab(wbout)], {type: "application/octet-stream"}), 'market_share_' + Math.round((new Date()).getTime() / 1000) + ".xlsx");
	});
  };

  $scope.s2ab = function (s) {
	const buf = new ArrayBuffer(s.length);
	const view = new Uint8Array(buf);
	for (let i = 0; i !== s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
  	return buf;
  };


}]);

