codmanApp.controller('LavorazioneDSRCtrl', ['$scope', '$route', 'ngDialog', '$routeParams', '$location', '$http', 'configService', '$filter', 'dspsUtilizationsCommercialOffersCountries', 'lavorazioneDsrService', 'searchResponse', 'dsrProcessingStatuses',
	function ($scope, $route, ngDialog, $routeParams, $location, $http, configService, $filter, dspsUtilizationsCommercialOffersCountries, lavorazioneDsrService, searchResponse, dsrProcessingStatuses) {
		$scope.dspsUtilizationsCommercialOffersCountries = dspsUtilizationsCommercialOffersCountries;
		$scope.dsrProcessingStatuses = dsrProcessingStatuses.data.statuses;
		$scope.filterParameters = { ...$routeParams, ...{ selectedProcessingStatuses: [{ id: 'ARRIVED', description: 'Arrivato',code:'ARRIVED' }] } };
		$scope.currentTime = new Date();
		// $scope.currentPeriod = (($scope.currentTime.getMonth() + 1) < 10 ? '0' + ($scope.currentTime.getMonth() + 1) : ($scope.currentTime.getMonth() + 1)) + '-' + $scope.currentTime.getFullYear();

		$scope.ctrl = {
			data: searchResponse.data,
			selectedDsrs: [],
			pageSelected: false,
			pageSize: configService.maxRowsPerPage,
			request: lavorazioneDsrService.emptySearchRequest(),
			messages: {
				info: "",
				error: "",
				clear: function () {
					this.info = "";
					this.error = "";
				},
				infoMessage: function (message) {
					this.info = message;
					$('#rightPanel')[0].scrollIntoView(true);
					window.scrollBy(0, -100);
				},
				errorMessage: function (message) {
					this.error = message;
					$('#rightPanel')[0].scrollIntoView(true);
					window.scrollBy(0, -100);
				}
			}
		};

		$scope.flipDsrSelection = function (item) {
			if (!item.processingStatus.restartable) return;
			if (item.selected) {
				this.deselectDsr(item);
			} else {
				this.selectDsr(item);
			}
		};

		$scope.selectDsr = function (item) {
			if (!item.processingStatus.restartable) return;
			item.selected = true;
			$scope.ctrl.selectedDsrs.push(item);
		};

		$scope.deselectDsr = function (item) {
			if (!item.processingStatus.restartable) return;
			item.selected = false;
			let index = $scope.ctrl.selectedDsrs.findIndex(e => item.dsr == e.dsr);
			if (index != -1) $scope.ctrl.selectedDsrs.splice(index, 1);
		};

		$scope.emptyDsrsSelection = function () {
			$scope.ctrl.selectedDsrs.forEach((item) => {
				item.selected = false;
			});
			$scope.ctrl.selectedDsrs = [];
			$scope.ctrl.pageSelected = false;
		};

		$scope.selectCurrentPage = function () {
			$scope.ctrl.data.rows.forEach((dsr) => {
				this.selectDsr(dsr);
			});
			$scope.ctrl.pageSelected = true;
		};

		$scope.deselectCurrentPage = function () {
			$scope.ctrl.data.rows.forEach((dsr) => {
				this.deselectDsr(dsr);
			});
			$scope.ctrl.pageSelected = false;
		};

		$scope.checkDsrsProcessing = function () {
			let found = $scope.ctrl.selectedDsrs.find((item) => item.processingStatus.restartNeedConfirmation);
			if (found) {
				this.askForProcessingConfirmation();
			} else {
				this.submitDsrsProcessing();
			}
		};

		$scope.formatPeriod = function (dsrPeriod) {
			if (dsrPeriod) {
				if (dsrPeriod.periodType == 'month') {
					let period =`${['Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov', 'Dic'][dsrPeriod.period - 1]} ${dsrPeriod.year}` ;
					if(period == 'undefined undefined'){
						return '';
					}
					else return  period;
				}
				 else {
					let period =`${['Gen - Mar', 'Apr - Giu', 'Lug - Set', 'Ott - Dic'][dsrPeriod.period - 1]} ${dsrPeriod.year}` ;
					if(period == 'undefined undefined'){
						return '';
					}
					else return  period;
				}
			}
		};

		$scope.submitDsrsProcessing = function () {
			lavorazioneDsrService.submitDsrsProcessing(
				{
					dsrs: $scope.ctrl.selectedDsrs
				}).then(
					function success(response) {
						$scope.emptyDsrsSelection();
						$scope.ctrl.messages.infoMessage(response.data.message);
						$scope.searchDsrsWithCachedFilters();
					},
					function error(response) {
						$scope.ctrl.messages.errorMessage(response.data.error);
						$scope.searchDsrsWithCachedFilters();
					}
				);
		};

		$scope.askForProcessingConfirmation = function () {
			$scope.ctrl.messages.clear();
			ngDialog.openConfirm({
				template: 'pages/lavorazione-dsr/dialog-alert.html',
				plain: false,
				data: {
				},
				controller: function () {
					var ctrl = this;
					ctrl.title = "Attenzione";
					ctrl.message = `Si sta richiedendo di rielaborare uno o più DSR, tutti i CCID precedentemente prodotti verranno sovrascritti!`;
				},
				controllerAs: 'ctrl'
			}).then(function confirm(data) {
				$scope.submitDsrsProcessing();
			}, function cancel() {
				//no op
			});
		};

		$scope.sortedSearch = function (name, param) {

			//get id of html element clicked
			var id = "#" + name;

			//get css class of that html element for change the image of arrows
			var classe = angular.element(id).attr("class");

			//set to original position all images of arrows, on each columns.
			var element1 = document.getElementsByClassName("glyphicon glyphicon-sort-by-attributes");
			var element2 = document.getElementsByClassName("glyphicon glyphicon-sort-by-attributes-alt");
			angular.element(element1).attr("class", "glyphicon glyphicon-sort");
			angular.element(element2).attr("class", "glyphicon glyphicon-sort");

			if (classe != "glyphicon glyphicon-sort-by-attributes" || classe == "glyphicon glyphicon-sort") {
				angular.element(id).attr("class", "glyphicon glyphicon-sort-by-attributes");
			} else {
				angular.element(id).attr("class", "glyphicon glyphicon-sort-by-attributes-alt");
			}

			$scope.ctrl.request.sortBy = `${param}`;

			if ($scope.ctrl.request.order == "DESC") {
				$scope.ctrl.request.order = "ASC";
			} else {
				$scope.ctrl.request.order = "DESC";
			}

			$scope.searchDsrsWithCachedFilters();

		};

		$scope.checkSelected = () => {
			$scope.ctrl.data.rows.forEach(item => item.selected = $scope.ctrl.selectedDsrs.map(it => it.dsr).includes(item.dsr));
			$scope.ctrl.pageSelected= $scope.ctrl.data.rows.map(item => item.selected).findIndex(check => !check) === -1
		};

		$scope.searchDsrsWithCachedFilters = function () {
			lavorazioneDsrService.search($scope.ctrl.request).then(function successCallback(response) {
				$scope.ctrl.data = response.data;
				//check selected
				$scope.checkSelected();
			
			}).catch(function (e) {
				$scope.ctrl.messages.errorMessage('Operazione non riuscita');
			});
		};

		$scope.navigateToPreviousPage = function () {
			$scope.ctrl.request.first = $scope.ctrl.request.first - $scope.ctrl.pageSize;
			$scope.ctrl.request.last = $scope.ctrl.request.last - $scope.ctrl.pageSize;
			$scope.searchDsrsWithCachedFilters();
		};

		$scope.navigateToNextPage = function () {
			$scope.ctrl.request.first = $scope.ctrl.request.first + $scope.ctrl.pageSize;
			$scope.ctrl.request.last = $scope.ctrl.request.last + $scope.ctrl.pageSize;
			$scope.searchDsrsWithCachedFilters();
		};

		$scope.navigateToEndPage = function () {
			$scope.ctrl.request.first = $scope.ctrl.data.maxrows - $scope.ctrl.pageSize;
			$scope.ctrl.request.last = $scope.ctrl.data.maxrows;
			$scope.searchDsrsWithCachedFilters();
		};

		$scope.searchDsrs = function (parameters) {
			var monthFrom = 0;
			var yearFrom = 0;
			var monthTo = 0;
			var yearTo = 0;
			$scope.ctrl.request.first = 0;
			$scope.ctrl.request.last = $scope.ctrl.pageSize;
			$scope.ctrl.pageSelected= false;
			$scope.emptyDsrsSelection();

			if ((parameters.selectedPeriodFrom)) {
				var from = parameters.selectedPeriodFrom.split('-');
				monthFrom = parseInt(from[0], 10);
				yearFrom = parseInt(from[1], 10);
			}

			if (parameters.selectedPeriodTo) {
				var to = parameters.selectedPeriodTo.split('-');
				monthTo = parseInt(to[0], 10);
				yearTo = parseInt(to[1], 10);
			}

			$scope.ctrl.request = {
				...$scope.ctrl.request,
				...{
					dspList: this.getCollectionValues(parameters.selectedDspModel),
					countryList: this.getCollectionValues(parameters.selectedCountryModel),
					utilizationList: this.getCollectionValues(parameters.selectedUtilizationModel),
					offerList: this.getCollectionValues(parameters.selectedOfferModel),
					monthFrom: monthFrom,
					yearFrom: yearFrom,
					monthTo: monthTo,
					yearTo: yearTo,
					processingStatuses: parameters.selectedProcessingStatuses,
					idDsr: parameters.idDsr
				}
			}
				;

			lavorazioneDsrService.search($scope.ctrl.request).then(function successCallback(response) {
				$scope.ctrl.data = response.data;
			}).catch(function (e) {
				$scope.ctrl.messages.errorMessage('Operazione non riuscita');
			});
		};

		$scope.getCollectionValues = function getCollectionValues(collection) {
			var selectedList = [];
			if (collection) {
				for (var e in collection) {
					selectedList.push(collection[e].id ? collection[e].id : collection[e]);
				}
			}
			return selectedList;
		};

	}]);

codmanApp.service('lavorazioneDsrService', ['$http', '$q', 'configService', function ($http, $q, configService) {

	var service = this;

	service.statuses = function (request) {
		if (!request) request = this.emptySearchRequest();
		return $http({
			method: 'GET',
			url: 'rest/dsr-processing/processing-statuses'
		});
	};

	service.search = function (request) {
		if (!request) request = this.emptySearchRequest();
		return $http({
			method: 'POST',
			url: 'rest/dsr-processing/search',
			data: request
		});
	};

	service.submitDsrsProcessing = function (request) {
		return $http({
			method: 'POST',
			url: 'rest/dsr-processing/submit-dsrs-processing',
			data: request
		});
	};

	service.emptySearchRequest = function (pageSize) {
		var currentTime = new Date();
		return {
			dspList: [],
			countryList: [],
			utilizationList: [],
			offerList: [],
			// monthFrom: currentTime.getMonth() + 1,
			// yearFrom: currentTime.getFullYear(),
			// monthTo: currentTime.getMonth() + 1,
			// yearTo: currentTime.getFullYear(),
			processingStatuses: [{ id: 'ARRIVED', description: 'Arrivato', code: 'ARRIVED' }],
			first: 0,
			last: pageSize || configService.maxRowsPerPage,
			sortBy: 'dsrDeliveryDate',
			order: 'DESC'
		};
	};
}
]);