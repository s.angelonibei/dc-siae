(function() {
    'use strict';
    angular.module('codmanApp').controller("KPIController", KPIController);


    KPIController.$inject = ["KPIService", "RegoleRipartizioneService"]

    function KPIController(KPIService, RRService) {
        var vm = this;

        vm.ricerca = {};
        vm.periodoRipartizione = {};
        vm.vociIncasso = {};
        vm.tipoReport="";
        
        vm.checkValue  = function(value) {
        		if(value==null||value==undefined||value==''){
        			return 0;
        		}else{
        			return value;
        		}
        }
        
        vm.getVociIncasso = function() {
            return RRService.getVociIncasso().then(function(response) {
            	vm.vociIncasso = response.data
                return response.data
            }, function(response) {
                RRService.showError("Errore", "Non è stato possibile ottenere la lista delle voci incasso")
            })
        }
        
        vm.getValue = function(codice) {
	    		for(var i=0;i<vm.periodoRipartizione.length;i++) {
	    			if(codice==vm.periodoRipartizione[i].codice){
	    				
	    				return codice +" ("+vm.formatDate(vm.periodoRipartizione[i].competenzeDa)+"/"+vm.formatDate(vm.periodoRipartizione[i].competenzeA)+")";
	    			}
	
	    		}
        }
        
        vm.formatDate = function(dateString) {
	    		var date = new Date(dateString)
	    		var separator = "-"
	    		var month = date.getMonth() + 1
	    		return (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + separator + (month < 10 ? "0" + month : month)
	    			+ separator + date.getFullYear()
	    	}

        vm.getPeriodiRipartizione = function() {
            return RRService.getPeriodiRipartizione().then(function(response) {
            	vm.periodoRipartizione = response.data
                return response.data
            }, function(response) {
                RRService.showError("Errore", "Non è stato possibile ottenere la lista dei periodi di ripartizione")
            })
        }

        vm.parseDateTime = function(date) {
            return RRService.parseDateTime(date);
        }

        vm.parseDate = function(date) {
            return RRService.parseDate(date);
        }

        vm.getVociIncasso();
        vm.getPeriodiRipartizione();
        
        
        
        vm.search = function(filtriRicerca){
    		vm.init(filtriRicerca)
    	}
        
        vm.init = function(filtri){
        	KPIService.kpiRecord(filtri).then(function(response){
    			if(response.data.length < 1){
    				RRService.showError("Errore","Non è stato trovata nessuna Regola per i filtri inseriti")
    				vm.storico = []
    			}else{
    				vm.storico = response.data
    				
    			}
    		},function(response){
    			RRService.showError("Errore","Non è stato possibile ottenere lo storico delle Regole precedentemente salvate")
    		})
    	}
        
    }


    codmanApp.service('KPIService', ['ngDialog', '$http', 'Upload', function(ngDialog, $http, Upload) {

        var service = this;
        
        service.kpiRecord = function(filtri){
    		return $http({
    			method: 'GET',
    			url: 'rest/performing/codifica/kpiRecord',
    			params: filtri
    		})
    	}

    }])
})();