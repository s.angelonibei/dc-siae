//(function(){
	
	/**
	 * KnowledgeBaseCtrl
	 * 
	 * @path /kb
	 */
	codmanApp.controller('KnowledgeBaseCtrl', ['$scope', 'ngDialog', '$routeParams', '$location', '$http', 'configService','ricercaKBService',
	                                           function($scope, ngDialog, $routeParams, $location, $http, configService, ricercaKBService) {

		$scope.ctrl = {
			navbarTab: 'knowledgeBase',
			execute: true, // $routeParams.execute,
			hideForm: $routeParams.hideForm,
			combana: []
		};	
		
		$scope.request = {
			title: $routeParams.title,
			artists: $routeParams.artists,
			iswc: undefined,
			uuid: undefined
		}
		
		$scope.onAzzera = function() {
			$scope.clearMessages();
			$scope.ctrl.execute = true; // false;
			$scope.request.iswc = null;
			$scope.request.uuid = null;
			$scope.request.title = null;
			$scope.request.artists = null;
			$scope.request.codice = null;
		};


		$scope.messages = {
            info: "", 
			error: "",
			warning: ""
        }

        $scope.clearMessages = function() {
            $scope.messages = {
                info: "", 
				error: "",
				warning:""
            }
        }
    
        $scope.infoMessage = function(message) {
            $scope.messages.info = message;
        }
    
        $scope.errorMessage = function(message) {
            $scope.messages.error = message;
		}
		
		$scope.warningMessage = function(message) {
            $scope.messages.warning = message;
        }

		
		$scope.onRicerca = function(request) {
		$scope.clearMessages();
		if (($scope.request.title && $scope.request.artists) ||
		$scope.request.iswc  || $scope.request.uuid || $scope.request.codice){
			ricercaKBService.ricercaOpere(request).then(function (response) {
			console.log('Search Response:', response);
			$scope.ctrl.combana = response.data;
			$scope.headers = _.cloneDeep($scope.ctrl);
			if(response.data.length == 0) {
			$scope.warningMessage("Non sono stati trovati risultati!");	
			}
			
		}, function (error) {
			
			$scope.errorMessage("Si è verificato un problema, riprovare più tardi!");
		});}
		 else {
			$scope.ctrl.combana = [];
			$scope.warningMessage("Non sono stati inseriti tutti i campi necessari per la ricerca!");	
			}	
		};
		

		$scope.showCodiciOpera= function(combana){
			$scope.clearMessages();
			var codici = '';
			if (combana.codiceAda && !combana.codiceSiae){
				codici = `ADA :${combana.codiceAda}`;	

			} else if (!combana.codiceAda && combana.codiceSiae){
				codici = `SIAE :${combana.codiceSiae}`;	
				
			} else if (combana.codiceAda && combana.codiceSiae){
				codici = `SIAE :${combana.codiceSiae}
				ADA :${combana.codiceAda}`;			
			} 
			
			return codici;
		}
		
		$scope.showDetails = function(combana) {
			$scope.clearMessages();
			var scope = $scope;
			ngDialog.open({
				template: 'pages/knowledge-base/dialog-details.html',
				plain: false,
				width: '60%',
				controller: ['$scope', 'ngDialog', '$http', function($scope, ngDialog, $http) {
					$scope.ctrl = {
						combana: combana
					};
					$scope.showCodiciOpera = function(combana) {
						return scope.showCodiciOpera(combana);
					};					
					
					$scope.cancel = function() {
						ngDialog.closeAll(false);
					};
					$scope.save = function() {
						ngDialog.closeAll(true);
					};

					$scope.showArtists= function(data){
						return data.split(',').map(el => el.substring(0,el.indexOf('['))).join(', ');
					};
				}]
			});
		};

		

	}]);


codmanApp.service('ricercaKBService', ['$http', function ($http) {

	var service = this;
	service.ricercaOpereServiceUrl = 'rest/ricercaOpereMultimediale';

	service.ricercaOpere = function (data) {
		return $http({
			method: 'POST',
			url: `${service.ricercaOpereServiceUrl}`,
			data: data
		})
	};

}]);