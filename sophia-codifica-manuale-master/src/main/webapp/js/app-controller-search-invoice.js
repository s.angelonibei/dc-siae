/**
 * CreateInvoiceCtrl
 *
 * @path /dspConfig
 */
codmanApp.controller('SearchInvoiceCtrl', ['$scope','$route', 'ngDialog', '$routeParams', '$location', '$http', 'configService',
	function($scope,$route, ngDialog, $routeParams, $location, $http, configService) {

	$scope.msInvoiceApiUrl =`${$location.protocol()}://${$location.host()}:${$location.port()}/${configService.msInvoiceApiPath}`;

	$scope.ctrl = {
		navbarTab: 'createInvoice',
		values: [],
		filterParameters: [],
		execute: $routeParams.execute,
		hideForm: $routeParams.hideForm,
		hideInfoUfficio: true,
		hideInfoCliente: true,
		maxrows: 50,
		first: $routeParams.first||0,
		last: $routeParams.last||50,
		sortType: 'priority',
		sortReverse: true,
		filter: $routeParams.filter,
		selectedItems: {},
		hideEditing: true,
		items:[],
		itemsToCollapse:{},
		accountingData: [],
		showCollapseButton: true,
		configuration: {},
		detailItems:[],
		statusMap: {"BOZZA": "BOZZA","RICHIESTA_FATTURA": "RICHIESTA FATTURA", "FATTURATO": "FATTURATO"},
	};
	
    $scope.allSelected = false;
    $scope.selectedCheckbox = {}
    $scope.codeSelectionList=[]
    $scope.annotation= ""
    	
	$scope.onAzzera = function() {
		$scope.ctrl.execute = false;
		$scope.ctrl.first = 0;
		$scope.ctrl.last = $scope.ctrl.maxrows;
		};

	$scope.navigateToNextPage = function() {
		$scope.getPagedResults($scope.ctrl.last, $scope.ctrl.last + $scope.ctrl.maxrows,
				 $scope.ctrl.filterParameters.dspList,
				 $scope.ctrl.filterParameters.statusList,
				 $scope.ctrl.filterParameters.nvoiceCode,
				 $scope.ctrl.filterParameters.dateFrom,
				 $scope.ctrl.filterParameters.dateTo)
		 
	};

	$scope.navigateToPreviousPage = function() {
		$scope.getPagedResults(Math.max($scope.ctrl.first - $scope.ctrl.maxrows, 0), $scope.ctrl.first,
				 $scope.ctrl.filterParameters.dspList,
				 $scope.ctrl.filterParameters.statusList,
				 $scope.ctrl.filterParameters.invoiceCode,
				 $scope.ctrl.filterParameters.dateFrom,
				 $scope.ctrl.filterParameters.dateTo)
			};


	// Funzione principale richiamata al caricamento della pagina
	$scope.onRefresh = function() {
		
		createItemReasonList()
		$scope.getPagedResults(0, $scope.ctrl.maxrows)

	};
	
	createItemReasonList = function(){
		$http({
			method: 'GET',
			url: $scope.msInvoiceApiUrl + 'invoice/itemReason/all'
		}).then(function successCallback(response) {
			for(x in response.data){			
				$scope.codeSelectionList.push({
					label: response.data[x].code + ' - ' + response.data[x].description,
					id:  x.toString()
				});
			}
		}, function errorCallback(response) {
			 $scope.codeSelectionList=[]
		});
		
	}

	$scope.getPagedResults = function(first, last ,client,dspList,statusList,invoiceCode,  dateFrom, dateTo) {

        		$http({
        			method: 'POST',
        			url:  $scope.msInvoiceApiUrl +  'invoice/all/invoice',
        			headers: {
                        'Content-Type': 'application/json'},
        			data: {
        				first: first,
        				last: last,
								clientList: client ,
        				dspList: (typeof dspList !== 'undefined' ? dspList : []) ,
        				statusList: (typeof statusList !== 'undefined' ? statusList : []),
        				invoiceCode : invoiceCode,
        				dateFrom : dateFrom,
        				dateTo : dateTo 
        				
        				}
        		}).then(function successCallback(response) {
        			$scope.ctrl.values.rows = response.data.rows;
        			$scope.ctrl.first = response.data.first
        			$scope.ctrl.last = response.data.last
        			$scope.ctrl.values = response.data
        		}, function errorCallback(response) {
        			$scope.ctrl.values.rows = []
        		});
		
      

	};

	$scope.formatNumber = function(number){
		return twoDecimalRound(number);
	}
	
	$scope.formatDate = function(dateString){
		var date = new Date(dateString)
		var separator = "-"
		var month = date.getMonth() + 1
		return (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + separator + (month < 10 ? "0" + month : month) + separator + date.getFullYear() + " " + (date.getHours() < 10 ? "0" + date.getHours() : date.getHours()) + ":" + (date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes())

	}
	
     $scope.showDetails = function(item){

    		ngDialog.open({
    			template: 'pages/invoice/search-invoice-info.html',
    			plain: false,
    			width: '60%',
    			backdrop: 'static',
    			closeByDocument: false,
    			scope: $scope,
    			controller: ['$scope', 'ngDialog', '$http', function($scope, ngDialog, $http) {
    				$scope.ctrl = {
    						detailItems: item.containedItems,
    						accountingData:[],
    						item: item,
    						waitingInvoiceRequest: false,
    						errorMessage: 'Errore applicativo. Riprovare o contattare l\'assistenza'
    				};	
    				
    				$http({
    					method: 'GET',
						url: $scope.msInvoiceApiUrl + 'invoice/all/items',
    					params: {
    						invoiceId: item.idInvoice
    						}
    				}).then(function successCallback(response) {
    					$scope.ctrl.accountingData = response.data
    					$scope.ctrl.first = response.data.first
    					$scope.ctrl.last = response.data.last
    				}, function errorCallback(response) {
    					$scope.ctrl.accountingData = []
    					$scope.ctrl.detailItems = []
    				});	
    				

    				$scope.formatNumber = function(number){
    					return twoDecimalRound(number);
    				}
    				
    				$scope.formatInteger = function(number){
    					return zeroDecimalRound(number);
    				}
    				
    			   	$scope.showItemDetail = function(item){
    		    		$scope.showDetail = true
    		    		$scope.detailName = item.description
    		    		$scope.ctrl.detailItems = item.invoiceDetailList    		    	
    		    		};
    				
    				$scope.back = function(){
    					$scope.showDetail = false
    					$scope.ctrl.detailItems = []
    				};
    				
    				$scope.getInvoiceStatus = function(){
    					return $scope.ctrl.item.status
    				};
    				
		          $scope.sendInvoiceRequest = function(){
		        	  $scope.ctrl.waitingInvoiceRequest = true
		        		$http({
		        			method: 'GET',
							url: $scope.msInvoiceApiUrl +  'invoice/requestInvoice',
		        			params: {
		        				invoiceId: $scope.ctrl.item.idInvoice
		        				}
		        		}).then(function successCallback(response) {
		        			
		       			 $scope.ctrl.item.status = 'RICHIESTA_FATTURA'
        				 $scope.getPagedResults($scope.ctrl.first !== undefined ? $scope.ctrl.first : 0,
		         					$scope.ctrl.last   !== undefined ? $scope.ctrl.last :50,
		         					$scope.ctrl.params !== undefined ? $scope.ctrl.params.dspList : null,
		         					$scope.ctrl.params !== undefined ? $scope.ctrl.params.statusList : null,
		         					$scope.ctrl.params !== undefined ? $scope.ctrl.params.invoiceCode : null,		
		         					$scope.ctrl.params !== undefined ? $scope.ctrl.params.dateFrom : null,
		         					$scope.ctrl.params !== undefined ? $scope.ctrl.params.dateTo : null
		         					);
        				
		        			$scope.ctrl.waitingInvoiceRequest= false
		        		}, function errorCallback(response) {
		        			
		        			openErrorDialog($scope.ctrl.errorMessage, false)
		        			$scope.ctrl.waitingInvoiceRequest = false
		        		});
	                 };
	                 
	                 
	             openErrorDialog = function(message, reloadPage){
              	  	 ngDialog.open({
        	             template: 'pages/invoice/dataNotFound.html',
        	             plain: false,
        	             width: '60%',
        	             scope: $scope,
        	             controller: ['$scope','ngDialog', '$http', function($scope, ngDialog, $http) {
        	            	 
        	            	 $scope.ctrl = {
        	     					message: message
        	     				};
        	                 
        	                 $scope.close = function(section){  			         
        			           	ngDialog.closeAll(false);
        			           if(reloadPage){        			        	   
        			        	   $route.reload()
        			        	   resetItem()
        			           }

        	                 }
        	                

        	         			}]
        	         		}).closePromise.then(function (data) {
        	         			if (data.value === true) {
        	         				// no op
        	         			}
        	         		});
	             }    
	             
	             $scope.checkInvoice = function(){
	            		$http({
		        			method: 'GET',
							url:  $scope.msInvoiceApiUrl +  'invoice/checkInvoice',
		        			params: {
		        				invoiceId: $scope.ctrl.item.idInvoice
		        				}
		        		}).then(function successCallback(response) {
		        			
		        			var item = response.data
		        			if(item.status === 'FATTURATO'){
		        				 $scope.ctrl.item = item
		        				 $scope.getPagedResults($scope.ctrl.first !== undefined ? $scope.ctrl.first : 0,
				         					$scope.ctrl.last !== undefined ? $scope.ctrl.last :50,
				         					$scope.ctrl.params !== undefined ? $scope.ctrl.params.dspList : null,
				         					$scope.ctrl.params!== undefined ? $scope.ctrl.params.statusList : null,
				         					$scope.ctrl.params!== undefined ? $scope.ctrl.params.invoiceCode : null,		
				         					$scope.ctrl.params !== undefined ? $scope.ctrl.params.dateFrom : null,
				         					$scope.ctrl.params !== undefined ? $scope.ctrl.params.dateTo : null
				         					);
		        				}
		        			
		        			}, function errorCallback(response) {
		        				//TODO: gestire avviso utente
		        		});	 
	             
	             };
	             
	                 resetItem = function(){
	             		$http({
		        			method: 'GET',
							url:  $scope.msInvoiceApiUrl +  'invoice/',
		        			params: {
		        				invoiceId: $scope.ctrl.item.idInvoice
		        				}
		        		}).then(function successCallback(response) {
		        			$scope.ctrl.item = response.data
		        		}, function errorCallback(response) {
		        
		        		});
	                	 
	                 }
    				$scope.close = function() {
    					ngDialog.closeAll(false)
    					$scope.detailName = ''
    					$scope.ctrl.detailItems = []
    				};

    			}]
    		}).closePromise.then(function (data) {
    			if (data.value === true) {
    				// no op
    			}
    		});


    	};
     
    	$scope.showDeleteConfig = function(item){
			let msInvoiceApiUrl =  $scope.ctrl.msInvoiceApiUrl;
    		ngDialog.open({
    			template: 'pages/invoice/dialog-remove-invoice.html',
    			plain: false,
    			width: '60%',
    			scope: $scope,
    			controller: ['$scope', 'ngDialog', '$http', function($scope, ngDialog, $http) {
    				$scope.ctrl = {
    					itemToRemove: item,
    					maxrows: $scope.ctrl.maxrows,
    					params : $scope.ctrl.filterParameters
    				};
    				

    				$scope.cancel = function() {
    					ngDialog.closeAll(false);
    				};
    				

    				$scope.formatN = function(number){
    					return $scope.formatNumber(number)
    				}
    			
    				$scope.formatD = function(date){
    					return $scope.formatDate(date)
    				}
    				
				   $scope.getS = function(status){
					  return $scope.getStatus(status) 
				   }
    				
    				$scope.deleteItem = function() {
    					$http({
    						method: 'DELETE',
							url:  $scope.msInvoiceApiUrl +  'invoice',
    						headers: {
    				            'Content-Type': 'application/json'},
    						data: { 
    							idInvoice: $scope.ctrl.itemToRemove.idInvoice	
    							}
    					
    					}).then(function successCallback(response) {
    						
    						 ngDialog.closeAll(false);
    						 $scope.getPagedResults(0,
    				         					$scope.ctrl.maxrows,
    				         					$scope.ctrl.params !== undefined ? $scope.ctrl.params.dspList : null,
    				         					$scope.ctrl.params!== undefined ? $scope.ctrl.params.statusList : null,
    				         					$scope.ctrl.params!== undefined ? $scope.ctrl.params.invoiceCode : null,
    				         					$scope.ctrl.params !== undefined ? $scope.ctrl.params.dateFrom : null,
    				         					$scope.ctrl.params !== undefined ? $scope.ctrl.params.dateTo : null
    				         					);
    					
    					}, function errorCallback(response) {
    						
    						$scope.openErrorDialog(response.statusText);
    					});
    					
    				};
    				
    			    $scope.openErrorDialog = function (errorMsg) {
    			        ngDialog.open({ 
    			        	template: 'errorDialogId',
    			        	plain: false,
    						width: '40%',
    			        	data: errorMsg
    			        	
    			        });
    			    };
    			}]
    		}).closePromise.then(function (data) {
    			if (data.value === true) {
    				// no op
    			}
    		});
    	};
 
     $scope.getStatus = function(status){
    	 return  $scope.ctrl.statusMap[status]
    	 
     }
	 $scope.$on('filterApply', function(event, parameters) {
		 $scope.ctrl.filterParameters = parameters;
		 $scope.getPagedResults(0,
         					$scope.ctrl.maxrows,
         					parameters.clientList,
         					parameters.dspList,
         					parameters.statusList,
         					parameters.invoiceCode,
         					parameters.dateFrom,
         					parameters.dateTo
         					);


		 });

	$scope.onRefresh();
	
	$scope.selectedCodeModel = [];
	$scope.selectedDescriptionModel = [];


    $scope.multiselectSettings = {
        scrollableHeight: '200px',
        scrollable: false,
        enableSearch: true,
        selectionLimit: 1,
        showUncheckAll: false,
        searchBr: true,
        checkboxes: false
    };

    $scope.doReturnInvoice = function(item){

    	ngDialog.open({
    		template: 'pages/invoice/dialog-return-invoice.html',
    		plain: false,
    		width: '60%',
    		scope: $scope,
    		controller: ['$scope', 'ngDialog', '$http', function($scope, ngDialog, $http) {
    			$scope.ctrl = {
    					itemToRemove: item,
    					detailItems: item.containedItems,
    					message:"Sei sicuro di voler Stornare la fattura?"
    			};	
    			
    		 	$scope.formatNumber = function(number){
    		 		return twoDecimalRound(number);
    			};
    			$scope.confirm = function() {
    				
    				$http({
						method: 'DELETE',
						url: $scope.msInvoiceApiUrl + 'invoice/deleteInvoice',
						headers: {
				            'Content-Type': 'application/json'},
						data: { 
							idInvoice : $scope.ctrl.itemToRemove.idInvoice	
						}
					
					}).then(function successCallback(response) {
						
						 ngDialog.closeAll(false);
						 $scope.getPagedResults(0,
								 $scope.ctrl.maxrows === undefined ? 50 : $scope.ctrl.maxrows,
				         					$scope.ctrl.params !== undefined ? $scope.ctrl.params.clientList : null,
				         					$scope.ctrl.params !== undefined ? $scope.ctrl.params.dspList : null,
				         					$scope.ctrl.params!== undefined ? $scope.ctrl.params.statusList : null,
				         					$scope.ctrl.params!== undefined ? $scope.ctrl.params.invoiceCode : null,
				         					$scope.ctrl.params !== undefined ? $scope.ctrl.params.dateFrom : null,
				         					$scope.ctrl.params !== undefined ? $scope.ctrl.params.dateTo : null
				         					);
						 
					
					}, function errorCallback(response) {
						
						$scope.openErrorDialog(response.statusText);
					});
    				
    			};
    			$scope.cancel = function() {
    				ngDialog.closeAll(false);
    				
    			};

    		}]
    	}).closePromise.then(function (data) {
    		if (data.value === true) {
    			// no op
    		}
    	});


    };
}]);


codmanApp.filter('range', function() {
	  return function(input, min, max) {
		    min = parseInt(min); //Make string input int
		    max = parseInt(max);
		    for (var i=min; i<=max; i++)
		      input.push(i);
		    return input;
		  };
});

codmanApp.filter('month_range', function() {
	  return function(input, min, max) {
		    min = parseInt(min); //Make string input int
		    max = parseInt(max);
		    for (var i=min; i<=max; i++)
		      input.push(i + "M");
		    return input;
		  };
});

