codmanApp.controller('VisualizzazioneDettaglioEventoCtrl',['$scope','$http', '$routeParams', 'ngDialog', '$location', 'DialogContabiliPM',
	function($scope, $http, $routeParams, ngDialog, $location, DialogContabiliPM){
	
	$scope.user = angular.element("#headerLinksBig").text().trim();
	
	$scope.pmShow = [];

	$scope.importoDEM = function(importo,documento){
		if(importo != undefined && documento != undefined){
			if(documento == 221){
				return '-' + importo; 
			}else{
				return importo;
			}
		}
	}

	$scope.importoMovimentazione = function(importo,tipoDocumentoContabile){
		if(importo != undefined && importo != ""){
			if(tipoDocumentoContabile != undefined && tipoDocumentoContabile != ""){
				if(tipoDocumentoContabile == 221){
					return '-' + importo;
				}else{
					return importo;
				}
			}
		}
	}
	
	$scope.addContabili = function(user){
		
		DialogContabiliPM.addContabili(user);
	}
	
	$scope.removeContabili = function(){
		
	}
	
	$scope.flagPM = true;
	
	$scope.addPM = function(idEvento,user){
		DialogContabiliPM.addPM(idEvento,user,$scope);
	}
	
	$scope.removePM = function(idMovimentoContabile,user){
		var scope= $scope;
		ngDialog.open({
			template : 'pages/advance-payment/dialog-warning.html',
			plain : false,
			width : '70%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				$scope.ctrl = {
					rScope:scope,
					title : "Warning",
					message : "sei sicuro di voler rimuovere il programma musicale selezionato, dall'evento ?"
				};
				$scope.user = user;
				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				$scope.call = function() {
					
					$http({
						method:'PUT',
						url:'rest/performing/cruscotti/sgancioPm',
						params: {
							idMovimentoContabile: idMovimentoContabile
						}
					}).then(function(response){
						$scope.ctrl.rScope.showError("Success","Programma Musicale rimosso con successo. Controllare e in caso modificare il numero dei Programmi Musicali Attesi");
						$scope.ctrl.rScope.init();
					},function(response){
						rScope.showError("Error","Non è stato possibile rimuovere il programma musicale selezionato: " + response.data);
					});
					ngDialog.closeAll(true);
				};
			} ]
		}).closePromise.then(function(data) {

		});
		
	}
	
	//vedere se in un eventoDetail ci possono essere più voci incasso (quindi più sezioni pmAttesi / Rientrati)TODO
	
	$scope.visibleAndSave = function(pmPrevisti,pmPrevistiPrincipale,pmPrevistiSpalla,index){
		if($scope.flagPM == true){
			angular.element("#spanPM" + index).attr('class',"glyphicon glyphicon-saved");
			angular.element("#pmPrevisti" + index).prop('readonly', false);
			angular.element("#pmPrevistiPrincipale" + index).prop('readonly', false);
			angular.element("#pmPrevistiSpalla" + index).prop('readonly', false);
			$scope.flagPM = false;
		}else if($scope.flagPM == false){
			angular.element("#spanPM" + index).attr('class',"glyphicon glyphicon-pencil");
			angular.element("#pmPrevisti" + index).prop('readonly', true);
			angular.element("#pmPrevistiPrincipale" + index).prop('readonly', true);
			angular.element("#pmPrevistiSpalla" + index).prop('readonly', true);
			$scope.flagPM = true;
			
			$scope.savePM(pmPrevisti,pmPrevistiPrincipale,pmPrevistiSpalla,index);
		}
		
	}
	
	$scope.savePM = function (pmPrevisti,pmPrevistiPrincipale,pmPrevistiSpalla,index){
		
		var checkPM = {};
		var idEventoPM = $scope.dettagliPM[index].idEvento
		
		if( (!pmPrevisti || !pmPrevisti[index] || !pmPrevisti[index]!="") && (!pmPrevistiPrincipale || !pmPrevistiPrincipale[index] || !pmPrevistiPrincipale[index]!="")&& (!pmPrevistiSpalla || !pmPrevistiSpalla[index] || pmPrevistiSpalla[index]!="")){
			$scope.showError("Errore","Riempire almeno un campo");
			return;
		}
		
		$http({
			method:'GET',
			url: 'rest/performing/cruscotti/controlloPmRientrati',
			params: {
				idEvento : idEventoPM
			}
		}).then(function(response){
			checkPM = response.data;
			if(pmPrevisti && pmPrevisti[index] != undefined){
				if(pmPrevisti[index] < checkPM.PmRientrati){
					$scope.showError("Errore","Il numero dei Programmi Musicali Attesi non può essere minore dei Programmi Musicali Rientrati. Refreshare la pagina per avere gli ultimi aggiornamenti");

				}else{
					$http({
						method:'PUT',
						url: 'rest/performing/cruscotti/updatePmAttesiFromEventiPagati',
						params: {
							idEventoPagato : $scope.samplings[index].id,
							pmPrevisti: pmPrevisti[index],
							pmPrevistiSpalla: 0,
							userId: $scope.user
						}
					}).then(function(response){
						$scope.showError("Success","Programmi Musicali Attesi modificati con successo");
						$scope.init();
					},function(response){
						$scope.showError("Errore","Non è stato possibile modificare i Programmi Musicali Attesi. Riprovare");
					})
				}
			}else{
				if((!pmPrevistiPrincipale && pmPrevistiPrincipale[index] < checkPM.PmRientrati) || (!pmPrevistiSpalla &&  pmPrevistiSpalla[index] < checkPM.PmRientratiSpalla)){
					$scope.showError("Errore","Il numero dei Programmi Musicali Attesi Principali e Spalla non può essere minore dei Programmi Musicali Rientrati Principali e Spalla. Refreshare la pagina per avere gli ultimi aggiornamenti");

				}else{
					$http({
						method:'PUT',
						url: 'rest/performing/cruscotti/updatePmAttesiFromEventiPagati',
						params: {
							idEventoPagato : $scope.idEventoPagato,
							pmPrevisti: pmPrevistiPrincipale[index],
							pmPrevistiSpalla: pmPrevistiSpalla[index],
							userId: $scope.user
						}
					}).then(function(response){
						$scope.showError("Success","Programmi Musicali Principali e Spalla Attesi modificati con successo");
						$scope.init();
					},function(response){
						$scope.showError("Errore","Non è stato possibile modificare i Programmi Musicali Principali e Spalla Attesi. Riprovare");
					})
				}
			}

		},function(response){
			$scope.showError("Errore","Non è stato possibile fare il controllo sui PM rientrati. Riprovare");
		})
	}
	
	$scope.idEvento = $routeParams.idEvento;
	
	$scope.goTo = function(url,param1,param2){
		if(url != undefined && param1 != undefined && param2 != undefined){
			$location.path(url + param1 + param2);
		}
	};
	
	$scope.init = function(){
		
		$http({
			method: 'GET',
			url: 'rest/performing/cruscotti/eventoDetail',
			params: {
				evento: $scope.idEvento
			}
		}).then(function(response){
			$scope.samplings = response.data;
			$scope.evento = response.data[0];
			$scope.manifestazione = response.data[0].manifestazione;
			$scope.movimentazioniPM = response.data[0].movimentazioniPM;
			$scope.dettagliPM = response.data[0].dettaglioPM;		
			$scope.idEventoPagato = response.data[0].id;
			if($scope.idEventoPagato != undefined){
				$scope.idAssente = false;
			}else{
				$scope.idAssente = true;
			}
		},function(response){
			$scope.showError("","Non risulta nessun dato per l'evento selezionato");
		})
	}
	
	$scope.PMAttesi = function(incasso,previsti,previstiSpalla,index){
		if(previsti != undefined && previstiSpalla != undefined){
			if(incasso == "2244"){
				$scope.pmShow[index] = true;
				return previsti + "  Principale   " + previstiSpalla + "  Spalla";
			}else{
				return previsti;
			}
		}
	};

	$scope.PMRientrati = function(incasso,rientrati,rientratiSpalla){
		if(incasso == "2244"){
			return rientrati + " Principale " + rientratiSpalla + " Spalla";
		}else{
			return rientrati;
		}
	};
	
	$scope.formatDate = function(date){
		if(date != null && date != undefined){
			var data = date.split("T")[0];
			var anno = data.split("-")[0];
			var mese = data.split("-")[1];
			var giorno = data.split("-")[2];
			return giorno + '-' + mese + '-' + anno;
		}
		
	};
	
	$scope.formatHour = function(hour){
		if(hour != null){
			var ora = hour.slice(0,2) + ":" + hour.slice(2,4);
			return ora;
		}
		
	}
	
	$scope.numeroPM = function(incasso,flagGP,numPM){
		if(incasso == "2244" && flagGP == "1"){
			return numPM + " (Principale)";
		}else if (incasso == "2244" && flagGP == "0"){
			return numPM + " (Spalla)";
		}else {
			return numPM;
		}
	}
	
	$scope.showError = function(title, msgs) {
		ngDialog.open({
			template : 'pages/advance-payment/dialog-alert.html',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				$scope.ctrl = {
					title : title,
					message : msgs
				};
				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				$scope.save = function() {
					ngDialog.closeAll(true);
				};
			} ]
		}).closePromise.then(function(data) {

		});
	};
	
	$scope.showAlert = function(title, message) {
		ngDialog.open({
			template : 'pages/advance-payment/dialog-alert.html',
			plain : false,
			width : '50%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				var ngDialogId = $scope.ngDialogId;
				$scope.ctrl = {
					title : title,
					message : message,
					ngDialogId : ngDialogId
				};
				$scope.cancel = function(ngDialogId) {
					ngDialog.close(ngDialogId);
				};
			} ]
		}).closePromise.then(function(data) {
			
		});
	};
	
	$scope.back = function(){
		 window.history.back();
	}
	
}]);