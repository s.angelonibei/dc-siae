codmanApp.controller('DeliverCcidCtrl',
    ['$scope', '$http', '$route', '$routeParams', 'ngDialog', '$location', 'configService', 'dspsUtilizationsCommercialOffersCountries',
        function ($scope, $http, $route, $routeParams, ngDialog, $location, configService, dspsUtilizationsCommercialOffersCountries) {
            $scope.filterParameters = $routeParams;
            //$scope.anagClientData = angular.copy(anagClientData);
            $scope.dspsUtilizationsCommercialOffersCountries = angular.copy(dspsUtilizationsCommercialOffersCountries);

            $scope.firstPaging = {
                currentPage: 1
            };

            $scope.selectedCcid = [];

            $scope.paging = { ...$scope.firstPaging };

            $scope.navigateToPreviousPage = function () {
                $scope.paging.currentPage = $scope.paging.currentPage - 1;
                $scope.filterParameters.first = $scope.filterParameters.first - configService.maxRowsPerPage;
                $scope.filterParameters.last = $scope.filterParameters.last - configService.maxRowsPerPage;
                $scope.callSearchService($scope.filterParameters, (response) => {
                    $scope.data = response.data;
                    $scope.checkSelected()
                });
            };

            $scope.navigateToNextPage = function () {
                $scope.paging.currentPage = $scope.paging.currentPage + 1;
                $scope.filterParameters.first = $scope.filterParameters.first + configService.maxRowsPerPage;
                $scope.filterParameters.last = $scope.filterParameters.last + configService.maxRowsPerPage;
                $scope.callSearchService($scope.filterParameters, (response) => {
                    $scope.data = response.data;
                    $scope.checkSelected()
                });
            };

            $scope.checkSelected = () => {
                $scope.data.rows.forEach(item => item.checked = $scope.selectedCcid.includes(item.idDsr));
                $scope.selectAllCheck = $scope.data.rows.map(item => item.checked).findIndex(check => !check) === -1
            };

            $scope.search = (request) => {
                $scope.selectedCcid = [];
                $scope.callSearchService(request);
            };


            $scope.callSearchService = (request, successCallback = (response) => {
                $scope.selectAllCheck = false;
                $scope.data = response.data
            }) => {
                $scope.filterParameters = angular.copy(request);
                let monthFrom, yearFrom, monthTo, yearTo;

                if ((request.selectedPeriodFrom)) {
                    const from = request.selectedPeriodFrom.split('-');
                    monthFrom = parseInt(from[0], 10);
                    yearFrom = parseInt(from[1], 10);
                }

                if (request.selectedPeriodTo) {
                    const to = request.selectedPeriodTo.split('-');
                    monthTo = parseInt(to[0], 10);
                    yearTo = parseInt(to[1], 10);
                }
                $scope.filterParameters.first = $scope.filterParameters.first || 0;
                $scope.filterParameters.last = $scope.filterParameters.last || configService.maxRowsPerPage;
                $http({
                    method: 'GET',
                    url: 'rest/deliverCCID/search',
                    params: {
                        first: request.first || 0,
                        last: request.last || configService.maxRowsPerPage,
                        dspList: $scope.getCollectionValues(request.selectedDspModel),
                        countryList: $scope.getCollectionValues(request.selectedCountryModel),
                        utilizationList: $scope.getCollectionValues(request.selectedUtilizationModel),
                        offerList: $scope.getCollectionValues(request.selectedOfferModel),
                        monthFrom: monthFrom ? monthFrom : '0',
                        yearFrom: yearFrom ? yearFrom : '0',
                        monthTo: monthTo ? monthTo : '0',
                        yearTo: yearTo ? yearTo : '0',
                        backclaim: request.backclaim,
                        status: request.selectedDeliverCcidStatus,
                        zipName: request.zipName
                    }
                }).then(successCallback)
            };

            $scope.checkAll = function (selectAllCheck) {
                $scope.data.rows.forEach(item => {
                    item.checked = selectAllCheck;

                    if (selectAllCheck) {
                        if (!$scope.selectedCcid.includes(item.idDsr))
                            $scope.selectedCcid.push(item.idDsr);
                    } else {
                        if (configService.debug)
                            console.log($scope.selectedCcid.indexOf(item.idDsr));
                        $scope.selectedCcid.splice($scope.selectedCcid.indexOf(item.idDsr), 1)
                    }

                });
                if (configService.debug)
                    console.log($scope.selectedCcid)
            };

            $scope.onChangeItemCheck = function (item) {
                if (item.checked) {
                    if (!$scope.selectedCcid.includes(item.idDsr))
                        $scope.selectedCcid.push(item.idDsr);
                } else {
                    if (configService.debug)
                        console.log($scope.selectedCcid.indexOf(item.idDsr));
                    $scope.selectedCcid.splice($scope.selectedCcid.indexOf(item.idDsr), 1)
                }
                if (configService.debug)
                    console.log($scope.selectedCcid)
            };

            $scope.retrieveConfig = () => {
                if ($scope.selectedCcid.length === 0) {
                    $scope.errorDialog('selezionare almeno un ccid');
                    return
                }

                $http({
                    method: 'POST',
                    url: 'rest/deliverCCID/retrieveConfig',
                    data: { ccid: $scope.selectedCcid }
                }).then(function (response) {
                    if (response.data.hasOwnProperty('error')) {
                        $scope.errorDialog(response.data.error)
                    } else if (response.data.length > 1) {
                        $scope.errorDialog('trovate più configurazioni per i ccid selezionati')
                    } else if (response.data.length === 0) {
                        $scope.errorDialog('nessuna configurazione trovata per i ccid selezionati')
                    } else {
                        $scope.dialog = ngDialog.open({
                            template: 'pages/deliver-ccid/dialog-deliver-ccid.html',
                            width: '60%',
                            scope: $scope,
                            data: {
                                deliverToDsp: true,
                                idDsp: $scope.data.rows.find(({ idDsr }) => $scope.selectedCcid[0] === idDsr).idDsp
                            },
                            controller: ['$scope', '$http', function ($scope, $http) {
                                $scope.item = response.data[0];
                                $scope.placeholderZipName = $scope.ngDialogData.idDsp + moment().format("YYYYMMDD-HHMMSS");

                                $scope.consegnati = $scope.item.ccid
                                    .filter(ccid => ccid.consegnato)
                                    .map(ccid => ccid.idDsr)
                                    .join('<br/>');

                                $scope.send = function () {
                                    let data = {
                                        ccid: $scope.item.ccid.map(item => item.idDsr),
                                        ftpPath: $scope.inputPath || $scope.item.ftpPath,
                                        zip: $scope.zipFlag,
                                    };
                                    if ($scope.zipFlag) {
                                        data = Object.assign({
                                            zipName: $scope.zipName || $scope.placeholderZipName
                                        }, data);
                                    }
                                    $http({
                                        method: 'POST',
                                        url: 'rest/deliverCCID/deliverToDsp',
                                        data
                                    }).then(() => {
                                        $scope.closeThisDialog();
                                        $scope.search($scope.filterParameters);
                                        $scope.errorDialog('invio riuscito')
                                    }, (response) => $scope.errorDialog(response.data.error))
                                }

                            }]
                        })
                    }
                });

            };

            $scope.retrieveConfigShareSiae = () => {
                if ($scope.selectedCcid.length === 0) {
                    $scope.errorDialog('selezionare almeno un ccid');
                    return
                }

                $http({
                    method: 'POST',
                    url: 'rest/deliverCCID/retrieveConfigShareSiae',
                    data: { ccid: $scope.selectedCcid }
                }).then(function (response) {

                    $scope.dialog = ngDialog.open({
                        template: 'pages/deliver-ccid/dialog-deliver-ccid.html',
                        width: '60%',
                        scope: $scope,
                        controller: ['$scope', 'ngDialog', '$http', function ($scope, ngDialog, $http) {
                            $scope.item = response.data[0];

                            $scope.send = function () {
                                $http({
                                    method: 'POST',
                                    url: 'rest/deliverCCID/deliverToShareSiae',
                                    data: {
                                        ccid: $scope.selectedCcid,
                                        ftpPath: $scope.inputPath
                                    }
                                }).then(() => {
                                    $scope.closeThisDialog();
                                    $scope.search($scope.filterParameters);
                                    $scope.errorDialog('invio riuscito')
                                }, (response) => $scope.errorDialog(response.data.error))
                            }

                        }]
                    })

                });

            };

            $scope.errorDialog = function (message) {
                ngDialog.open({
                    template: 'errorDialogId',
                    plain: false,
                    width: '40%',
                    data: message
                })
            };

            $scope.getCollectionValues = function (collection) {
                const selectedList = [];
                if (collection !== 'undefined') {
                    for (const e in collection) {
                        selectedList.push(collection[e].id);
                    }
                }
                return selectedList;
            };
        }]);