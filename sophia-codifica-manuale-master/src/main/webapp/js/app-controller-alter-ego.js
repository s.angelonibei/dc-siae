/**
 * AlterEgoCtrl
 * 
 * @path /utenti
 */
codmanApp.controller('AlterEgoCtrl', [ '$scope', '$route', 'ngDialog', '$routeParams', '$location', '$http', 'Upload', 'configService', function($scope, $route, ngDialog, $routeParams, $location, $http, Upload, configService) {

	$scope.ctrl = {
		navbarTab : 'alterEgo',
		utenti : [],
		execute : $routeParams.execute,
		fiteredName : "",
		fiteredType : "",
		fiterBroadcasterName : "",
		fiterBroadcasterType : {},
		tipiEmittenti : [
			{
				"nome" : "TUTTI"
			},{
				"nome" : "TELEVISIONE"
			}, {
				"nome" : "RADIO"
			} ],
	};

	// Funzione principale richiamata al caricamento della pagina
	$scope.onRefresh = function() {
		$scope.getEmittenti()
	};
	$scope.back = function() {
		$scope.ctrl.selectedBroadcaster = null;
		$scope.getEmittenti()
	};
	
	$scope.getEmittenti = function() {
		var tipoBroadcaster=""
		if($scope.ctrl.fiterBroadcasterType.nome=="TUTTI"){
			tipoBroadcaster=""
		}else{
			tipoBroadcaster=$scope.ctrl.fiterBroadcasterType.nome;
		}
		$http({
			method : 'GET',
			url : 'rest/broadcasting/emittentiFiltrati',
			params : {
				fiteredName : $scope.ctrl.fiterBroadcasterName,
				fiteredType : tipoBroadcaster
			}
		}).then(function successCallback(response) {
			$scope.ctrl.emittenti = response.data;
		}, function errorCallback(response) {
			$scope.ctrl.emittenti = [];
		});

	};

	$scope.encode = function(buffer) {
		var binary = '';
		var bytes = new Uint8Array(buffer);
		var len = bytes.byteLength;
		for (var i = 0; i < len; i++) {
			binary += String.fromCharCode(bytes[i]);
		}
		return window.btoa(binary);
	};
	
	$scope.getRepertori= function(utente) {
		var repertoriSort=[];
		if(utente.repertori!=null&&utente!=undefined&&utente.repertori.length>0){
			var repertorio=""
			for (var i = 0; i < utente.repertori.length; i++) {
				repertoriSort.push(utente.repertori[i].repertorio.nome);
			}
			repertoriSort=repertoriSort.sort();
			for (var i = 0; i < repertoriSort.length; i++) {
				repertorio += repertoriSort[i] + ", ";
			}

			return repertorio.substring(0, repertorio.length - 2);
		}
	}
	
	$scope.getRepertorioDefault= function(utente) {
		if(utente.repertori!=null&&utente!=undefined&&utente.repertori.length>0){
			for (var i = 0; i < utente.repertori.length; i++) {
				if (utente.repertori[i].defaultRepertorio) {
					return utente.repertori[i].repertorio.nome;
				}
			}
		}
	}

	
	$scope.checkRadio = function() {
		if($scope.ctrl.selectedBroadcaster!=undefined&&$scope.ctrl.selectedBroadcaster.tipo_broadcaster=="RADIO"){
			return true;
		}else{
			return false;
		}
	}
	
	$scope.showEditEmittente = function(broadcaster,repertori) {
		$scope.ctrl.selectedBroadcaster = broadcaster;
		$scope.getuserByBroadcaster(repertori);

	}
	
	$scope.getuserByBroadcaster = function(repertori) {
		$http({
			method : 'POST',
			url : 'rest/user/listaUtentiRepertori',
			data : {
				bdcBroadcasters : $scope.ctrl.selectedBroadcaster,
				repertori:repertori
			}
		}).then(function successCallback(response) {
			$scope.ctrl.utenti = response.data;

		}, function errorCallback(response) {
			$scope.ctrl.utenti = [];
		});
	}

	$scope.showError = function(title, msgs) {
		ngDialog.open({
			template : 'pages/advance-payment/dialog-alert.html',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				$scope.ctrl = {
					title : title,
					message : msgs
				};
				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				$scope.save = function() {
					ngDialog.closeAll(true);
				};
			} ]
		}).closePromise.then(function(data) {
			// no op
		});
	};
	
	showAlert = function(title, message) {
		ngDialog.open({
			template : 'pages/advance-payment/dialog-alert.html',
			plain : false,
			width : '50%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				var ngDialogId = $scope.ngDialogId;
				$scope.ctrl = {
					title : title,
					message : message,
					ngDialogId : ngDialogId
				};
				$scope.cancel = function(ngDialogId) {
					ngDialog.close(ngDialogId);
				};
			} ]
		}).closePromise.then(function(data) {
			if (data.value === true) {

			}
		});
	};

	$scope.getImgStato = function(item) {
		if (item == 1) {
			return "images/ok.png";
		} else {
			return "images/ko.png";
		}
	};
	
	$scope.loginAlterEgo = function (username,item,url) {
		
		$http({
			method : 'POST',
			url : 'rest/userToken/generateUserToken',
			data : {
				bdcUserId : item.id,
				username : username,
			}
		}).then(function successCallback(response) {
			showLoginConfirm(username,item,url,response)
		}, function errorCallback(response) {

		});
			  
	}
	
	showLoginConfirm = function(username,item,url,response) {
		ngDialog.open({
			template : 'pages/advance-payment/dialog-warning.html',
			plain : false,
			width : '50%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				var ngDialogId = $scope.ngDialogId;
				$scope.ctrl = {
					title : "Conferma Accesso",
					message : "Vuoi accedere come "+item.username+" ?",
					ngDialogId : ngDialogId
				};
				$scope.cancel = function() {
					ngDialog.close(ngDialogId);
				};
				$scope.call= function(){
					ngDialog.close(ngDialogId);
					var winName=response.data.token;       
					var form = document.createElement("form");
					form.setAttribute("method", "POST");
					form.setAttribute("action", url);
					form.setAttribute("target",winName);  
					var input = document.createElement('input');
					input.type = 'hidden';
					input.name = 'token';
					input.value = response.data.token;
					form.appendChild(input);
					document.body.appendChild(form);      
					form.target = winName;
					form.submit();                 
					document.body.removeChild(form);
					$scope.formatDate = function(dateString) {
						if (dateString != undefined && dateString != null) {
							var date = new Date(dateString)
							var separator = "-"
							var month = date.getMonth() + 1
							return (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + separator + (month < 10 ? "0" + month : month) + separator + date.getFullYear() 

						} else {
							return "";
						}
					}

				}
			} ]
		}).closePromise.then(function(data) {
			if (data.value === true) {

			}
		});
	};
	
	

	$scope.onRefresh();

} ]);