codmanApp.controller('reclamiUploadFileCtrl', ['$http', '$scope', '$interval', 'Upload', 'configService', 'uploadReclamiService', 'anagClientData', 'anagClientService', function($http, $scope, $interval, Upload, configService, uploadReclamiService, anagClientData, anagClientService) {

    $scope.anagClientData = angular.copy(anagClientData);
    $scope.selectedCountryModel = [];
    $scope.selectedOfferModel = [];
    $scope.selectedUtilizationModel = [];
    $scope.selectedDsp = [];

    $scope.firstPaging = {
        currentPage: 1,
        order: "insertTime desc"
    }

    $scope.messages = {
        info: "",
        error: ""
    }

    $scope.paging = {...$scope.firstPaging };

    $scope.clearMessages = function() {
        $scope.messages = {
            info: "",
            error: ""
        }
    }

    $scope.infoMessage = function(message) {
        $scope.messages.info = message;
    }

    $scope.errorMessage = function(message) {
        $scope.messages.error = message;
    }

    $scope.init = function() {
        $scope.clearMessages();
        uploadReclamiService.getUploadedFileReclamiList()
            .then(function(response) {
                $scope.uploadedFileList = response.data;
                $scope.uploadedFileList.forEach(function(file) {
                    file.nameNew = _.clone(file.fileName || "");
                    file.nameNew = file.nameNew.substring(1 + file.nameNew.lastIndexOf('/'));
                });
                console.log($scope.uploadedFileList.data);

            }, function(error) {
                $scope.errorMessage("Si è verificato un problema, riprovare più tardi!");
            });
    }

    $scope.navigateToPreviousPage = function() {
        $scope.paging.currentPage = $scope.paging.currentPage - 1;
        $scope.init();
    }

    $scope.navigateToNextPage = function() {
        $scope.paging.currentPage = $scope.paging.currentPage + 1;
        $scope.init();
    }

    $scope.navigateToEndPage = function() {
        $scope.paging.currentPage = $scope.paging.totPages;
        $scope.init();
    }

    $scope.uploadFile = function(file, request, user) {
        $scope.clearMessages();
        uploadReclamiService.upload(file, request, user)
            .then(function(response) {
                $scope.cs.file = null;
                $scope.init();
                $scope.infoMessage("Il file è stato caricato correttamente.");
            }, function(error) {
                if (error == 'INVALID_EXT') {
                    $scope.errorMessage("Estensione non supportata. Le estensioni supportate sono: xls,xlsx,csv");
                } else {
                    $scope.errorMessage("Il file non è stato caricato correttamente!");
                }
            });
    };

    $scope.downloadFile = function(fileUrl) {
        $scope.clearMessages();
        uploadReclamiService.downloadFile(fileUrl)
            .then(function(response) {
                var blob = new Blob([response.data], { type: "application/vnd.ms-excel;charset=UTF-8" });
                saveAs(blob, fileUrl.substring(fileUrl.lastIndexOf('/') + 1));
            }, function(error) {
                $scope.errorMessage("Non è possibile scaricare il file selezionato!");
            });
    }

    $scope.init();



    $scope.utilizationsOffersCountries = function() {
        $http({
            method: 'GET',
            url: 'rest/data/dspUtilizationsOffersCountries'
        }).then(function successCallback(response) {
            $scope.utilizationList = response.data.utilizations;
            $scope.commercialOffersList = response.data.commercialOffers;
            $scope.countryList = response.data.countries;
            $scope.buildKeySets();

        });

    };

    $scope.utilizationsOffersCountries();


    $scope.buildKeySets = function() {
        $scope.countrySelectionList = [];
        _.forEach($scope.countryList, function(value) {
            $scope.countrySelectionList.push({
                label: value.name,
                id: value.code
            });
        });

        $scope.utilizationSelectionList = [];
        _.forEach($scope.utilizationList, function(value) {
            $scope.utilizationSelectionList.push({
                label: value.name,
                id: value.idUtilizationType
            });
        });

        $scope.offerSelectionList = [];
        _.forEach($scope.commercialOffersList, function(value) {
            $scope.offerSelectionList.push({
                label: value.offering,
                id: value.offering
            });
        });
    };

    $scope.filterCommercialOffersByDsp = function() {
        return function(offer) {
            if ($scope.request !== undefined &&
                $scope.request.dsp !== undefined &&
                $scope.request.dsp.idDsp !== undefined)
                return offer.idDSP === $scope.request.dsp.idDsp;
            else
                return true;
        }

        //return $scope.commercialOffersList.filter(offer => offer.idDSP === $scope.request.dsp.idDsp)
    }




}]);

codmanApp.service('uploadReclamiService', ['$http', '$q', 'configService', '$location', function($http, $q, configService, $location) {

    var service = this;

    service.msInvoiceApiUrl = `${$location.protocol()}://${$location.host()}:${$location.port()}/${configService.msInvoiceApiPath}`;



    service.getUploadedFileReclamiList = function() {
        return $http({
            method: 'GET',
            url: service.msInvoiceApiUrl + 'reclamo/getReclami'
        });
    };

    service.upload = function(cs, params, user) {

        var extension = cs.file.name.slice(cs.file.name.lastIndexOf('.'));
        let { dsp: { idDsp = '' } } = params;
        if (extension != '.xls' && extension != '.xlsx' && extension != '.csv') {
            return $q.reject("INVALID_EXT");
        }
        if (idDsp.match(/[()]/)) {
            idDsp = idDsp.replace('(', '').replace(')', '');
        }

        var fd = new FormData();

        var reclamo = {
            commercialOffer: params.commercialOffer.offering ? params.commercialOffer.offering : '',
            idDsp,
            country: params.country.id ? params.country.id : '',
            utente: user,
            period: params.period ? params.period.replace('-', '') : '',
            periodType: "YYYYMM",
            year: params.period.substring(0, 4),
            fileName: cs.file.name
        };

        if (cs.file) {
            fd.append('file', cs.file);
            fd.append('reclamo', JSON.stringify(reclamo));
        }

        return $http({
            method: 'POST',
            url: service.msInvoiceApiUrl + 'reclamo/addReclamo',
            headers: { 'Content-Type': undefined },
            transformRequest: angular.identity,
            data: fd
        });
    };


    service.downloadFile = function(fileUrl) {
        var encodedURL = encodeURIComponent(fileUrl);
        return $http({
            method: 'GET',
            responseType: 'blob',
            url: service.msInvoiceApiUrl + 'reclamo/downloadinput?fileUrl=' + encodedURL
        });
    };

}]);