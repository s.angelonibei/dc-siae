(function() {
  angular
    .module('codmanApp')
    .service('eventiPagatiService', eventiPagatiService);

  eventiPagatiService.$inject = ['$http'];

  function eventiPagatiService($http) {
    this.getSearchConfig = getSearchConfig;
    this.getAgenzie = getAgenzie;
    this.getDateLimits = getDateLimits;
    this.getCodiciVoceIncasso = getCodiciVoceIncasso;
    this.getSedi = getSedi;

    function getSearchConfig() {
      var promises = [
        getAgenzie(),
        getDateLimits(),
        getCodiciVoceIncasso(),
        getSedi()
      ];

      return Promise.all(promises);
    }

    function getAgenzie(search) {
      return $http({
        method: 'GET',
        url: 'rest/eventi-pagati',
        params: {
          agenzie: search ? search : ''
        }

      }).then(getAgenzieComplete);


      function getAgenzieComplete(response) {
        if (response.status !== 204) {
          return response.data;
        }
      }
    }

    function getDateLimits() {
      return $http({
        method: 'GET',
        url: 'rest/eventi-pagati/date-limits'

      }).then(getDateLimitsComplete);


      function getDateLimitsComplete(response) {
        if (response.status !== 204) {
          return response.data;
        }
      }
    }

    function getCodiciVoceIncasso(search) {
      return $http({
        method: 'GET',
        url: 'rest/eventi-pagati/voci-incasso',
        params: {
          query: search ? search : ''
        }
      }).then(getCodiciVoceIncassoComplete);


      function getCodiciVoceIncassoComplete(response) {
        if (response.status !== 204) {
          return response.data;
        }
      }
    }

    function getSedi(search) {
      return $http({
        method: 'GET',
        url: 'rest/eventi-pagati/sedi',
        params: {
          query: search ? search : ''
        }
      }).then(getSediComplete);


      function getSediComplete(response) {
        if (response.status !== 204) {
          return response.data;
        }
      }
    }
  }
}());
