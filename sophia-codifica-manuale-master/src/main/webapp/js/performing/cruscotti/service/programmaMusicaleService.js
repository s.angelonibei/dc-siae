(function () {
  'use strict';
  angular
    .module('codmanApp')
    .service('programmaMusicaleService', programmaMusicaleService);

  programmaMusicaleService.$inject = ['$http', 'ngDialog', 'UtilService'];

  function programmaMusicaleService($http, ngDialog, UtilService) {
    this.getCandidate = getCandidate;
    this.askConfirm = askConfirm;
    this.getPMDetail = getPMDetail;

    function getPMDetail(idProgrammaMusicale, idMovimento) {
      return $http({
        method: 'GET',
        url: 'rest/performing/cruscotti/pmDetail',
        params: {
          idProgrammaMusicale: idProgrammaMusicale,
          idMovimento: idMovimento
        }
      }).then(getPMDetailComplete)
        .catch(function (response) {
          UtilService.handleHttpRejection(response, "Non risulta nessun dato per il programma musicale selezionato")
        });

      function getPMDetailComplete(response) {
        // $scope.opereDaApprovare = {};
        // $scope.codifica = undefined;
        // $scope.sampling = response.data;
        // $scope.eventi = response.data.eventi;
        // $scope.opere = response.data.opere;
        // $scope.movimenti = response.data.movimenti;
        // $scope.importo = response.data.importo;
        // $scope.direttoreEsecuzione = response.data.direttoreEsecuzione;
        // $scope.flagGruppoPrincipaleSpalla = response.data.flagGruppoPrincipale;
        // $scope.totaleImportoSun = 0;
        // $scope.totaleImportoRicalcolato = 0;
        // $scope.flagDaCampionare = response.data.flagDaCampionare;
        // $scope.numProgrammaMusicale = $scope.sampling.numeroProgrammaMusicale;
        // if ($scope.sampling.foglio === "DI") {
        //     angular.element("#flagCampionamento").prop('disabled', true);
        //     angular.element("#flagButton").prop('disabled', true);
        // }
        // if ($scope.sampling.voceIncasso !== "2244") {
        //     angular.element("#flagGPS").prop('disabled', true);
        //     angular.element("#buttonGPS").prop('disabled', true);
        // }
        // angular.forEach($scope.movimenti, function(value, key) {
        //     if (value.tipoDocumentoContabile == 221) {
        //         $scope.totaleImportoRicalcolato = $scope.totaleImportoRicalcolato - value.importoValorizzato;
        //         $scope.totaleImportoSun = $scope.totaleImportoSun - value.importoTotDemLordo;
        //     } else if (value.tipoDocumentoContabile == 501) {
        //         $scope.totaleImportoRicalcolato = $scope.totaleImportoRicalcolato + value.importoValorizzato;
        //         $scope.totaleImportoSun = $scope.totaleImportoSun + value.importoTotDemLordo;
        //     }
        //
        // })
        if (response.status !== 204) {
          return response;
        }
        showError('Errore', 'Non risulta nessun dato per il programma musicale selezionato');
      }
    }

    function getCandidate(idCombana, utente) {
      return $http({
        method: 'GET',
        url: 'rest/performing/cruscotti/utilizzazione/' + idCombana,
        params: {
          user: utente
        }
      }).then(getCandidateComplete)
        .catch(getCandidateFailed);

      function getCandidateComplete(response) {
        if (response.status !== 204) {
          return response.data;
        }
        showError('Attenzione!', "Non ci sono candidate disponibile al momento per l'" +
          'opera selezionate, riprovare in un secondo momento');
      }
    }

    function getCandidateFailed(e) {
      var newMessage = 'Si è verificato un problema riprovare in un secondo momento';
      showError('Errore', newMessage);
      if (e.data && e.data.description) {
        newMessage = newMessage + '\n' + e.data.description;
      }
      e.data.description = newMessage;
      logger.error(newMessage);
      return $q.reject(e);
    }

    function showError(title, msgs) {
      ngDialog.open({
        template: 'pages/advance-payment/dialog-alert.html',
        plain: false,
        width: '60%',
        controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
          $scope.ctrl = {
            title: title,
            message: msgs
          };
          $scope.cancel = function () {
            ngDialog.close($scope.ngDialogId);
          };
          $scope.save = function () {
            ngDialog.closeAll(true);
          };
        }]
      });
    }

    function askConfirm(title, msgs) {
      return ngDialog.open({
        template: 'pages/advance-payment/dialog-confirm.html',
        plain: false,
        width: '60%',
        controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
          var ngDialogId = $scope.ngDialogId;
          $scope.ctrl = {
            title: title,
            message: msgs,
            ngDialogId: ngDialogId
          };
          $scope.cancel = function (ngDialogId) {
            ngDialog.close(ngDialogId);
          };
          $scope.save = function () {
            ngDialog.closeAll(true);
          };
        }]
      });
    }
  }
})();
