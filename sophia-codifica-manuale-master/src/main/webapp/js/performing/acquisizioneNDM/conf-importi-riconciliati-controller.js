   angular.module('codmanApp').controller('confImportiRiconciliatiCtrl',
    ['configurationsService', '$scope','ngDialog', function (configurationsService, $scope, ngDialog) {

        $scope.pageTitle = "Configurazione Importi Riconciliati";
        $scope.configDomain = "performing.soglie-ndm";
        $scope.configurations = [];

        $scope.messages = {
            info: "", 
            error: ""
        };

        $scope.codVoceIncasso = '';

        $scope.clearMessages = function() {
            $scope.messages = {
                info: "", 
                warning: "",
                error: ""
            };
        };
    
        $scope.warningMessage = function(message) {
            $scope.messages.warning = message;
        };
    
        $scope.errorMessage = function(message) {
            $scope.messages.error = message;
        };

        $scope.infoMessage = function(message) {
            $scope.messages.info = message;
        };
       
        $scope.configurationTemplate =  {  
            "domain":`${$scope.configDomain}`,
            "key":"",
            "active":true,
            "topLevel":true,
            "settings":[
               {  
                  "valueType":"Decimal",
                  "value":"0",
                  "key":"threshold.value", 
                  "settingType":"key_value",
               },
               { 
                "valueType":"Boolean",
                "value":false,
                "key":"threshold.percentage", 
                "settingType":"key_value"
               }    
            ]
         };

        $scope.searchConfiguration = function (voceIncasso) {
            $scope.clearMessages();
            configurationsService.loadConfiguration(`${$scope.configDomain}/${voceIncasso?voceIncasso:''}`).then(function success(results){
                $scope.configurations = results;
                if (results.length == 0) {
                    $scope.warningMessage("Non esiste una configurazione per la voce incasso inserita");
                    $('#rightPanel')[0].scrollIntoView( true );
                    window.scrollBy(0, -100);   
                }
            }, function error(error) {
                console.log('error', error);
                $scope.errorMessage(" Si è verificato un errore, riprovare più tardi!");
                $('#rightPanel')[0].scrollIntoView( true );
                window.scrollBy(0, -100);

            });         
        };

        $scope.getValueSettingIndex = function(configuration) {
            var index = configuration.settings.findIndex(function(element) {
                return element.key ==='threshold.value';
            });
            return index;    
        };

        $scope.getTypeSettingIndex = function(configuration) {
            var index = configuration.settings.findIndex(function(element) {
                return element.key ==='threshold.percentage';
            });
            return index;
        };

        $scope.upsertConfiguration = function(configuration){
            var scope = $scope;
            return ngDialog.open({
                template : 'pages/performing/acquisizioneNDM/dialog-crea-soglia-riconciliazione.html',
                plain : false,
                width : '40%',
                controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
                    var ngDialogId = $scope.ngDialogId;

                    if (configuration && configuration.key) {
                        $scope.tipo = "Modifica";
                        $scope.configuration = _.cloneDeep(configuration);
                    } else {
                        $scope.tipo = "Crea nuova";
                        $scope.configuration = _.cloneDeep(scope.configurationTemplate);
                    }

                    $scope.ctrl = {
                        ngDialogId : ngDialogId,
                        typeSetting: $scope.configuration.settings[scope.getTypeSettingIndex($scope.configuration)],
                        valueSetting: $scope.configuration.settings[scope.getValueSettingIndex($scope.configuration)]
                    };

                    $scope.cancel = function() {
                        ngDialog.close(ngDialogId);
                    };

                    $scope.saveConfiguration = function() {
                        scope.clearMessages();
                        $scope.configuration.label = `Configurazione Soglia NDM voce incasso ${$scope.configuration.key}`;
                        configurationsService.saveConfiguration($scope.configuration).then(function success(result) {
                            var index = scope.configurations.findIndex(function(element) {
                                return element.key === result.key;
                            });
                            if(index === -1){
                                scope.codVoceIncasso = result.key;
                                scope.configurations = [result];
                            } else {
                                scope.configurations.splice(index, 1, result);
                            }
                            scope.infoMessage(" Salvataggio effettuato con successo!");
                        }, function error(error) {
                            if (error.status != 400) {
                                scope.errorMessage(" Si è verificato un errore, riprovare più tardi!");
                                $('#rightPanel')[0].scrollIntoView( true );
                                window.scrollBy(0, -100);
        
                            } else {
                                console.log('error', error.data);
                                var message = error.data.errorMessages[0] || '';
                                if (error.data.settings) {
                                    message = message.concat(error.data.settings.reduce(function (acc, setting) {
                                        return acc + (setting.errorMessages[0] || '');
                                    }, ''));
                                }
                                scope.errorMessage(message);
                                $('#rightPanel')[0].scrollIntoView( true );
                                window.scrollBy(0, -100);
                            }
                        });
                        ngDialog.close(ngDialogId);
                      };
                      
                }]
            });
        };

    }
]);