(function () {
    'use strict';
    angular
        .module('codmanApp')
        .service('gestioneMusicProvidersService', gestioneMusicProvidersService);

    gestioneMusicProvidersService.$inject = ['$http','UtilService'];

    function gestioneMusicProvidersService($http,UtilService) {
        var service = {
            getMusicProviders: getMusicProviders,
            getPalinsesti: getPalinsesti,
            getUserByMusicProvider: getUserByMusicProvider,
            getUtilizationFiles: getUtilizationFiles,
            downloadUtilizationFile: downloadUtilizationFile,
            scaricaFileArmonizzato: scaricaFileArmonizzato,
        };
        return service;

        function getMusicProviders(nameFilter){
            return $http({
                method: 'GET',
                url: 'rest/performing/radioInStore/musicProviders',
                params: {
                    nome: nameFilter
                }
            }).then(function (response) {
                return [].concat(response.data.musicProviderDTO);
            });
        }

        function getPalinsesti(musicProvider) {
            return $http({
                method: 'GET',
                url: 'rest/performing/radioInStore/musicProviders/' + musicProvider.idMusicProvider + '/palinsesti',
            }).then(function (response) {
                return [].concat(response.data.perfPalinsesto);
            })
        }

        function getUserByMusicProvider(musicProvider) {
            return $http({
                method: 'GET',
                url: 'rest/performing/radioInStore/utenti/' + musicProvider.idMusicProvider,
            }).then(function (response) {
                return [].concat(response.data.perfRsUtente);
            });
        }

        function getUtilizationFiles(musicProvider,palinsesto,anno,mesiSelected) {
            var mes = [];
            for (var i = 0; i < mesiSelected.length; i++) {
                mes.push(mesiSelected[i].id)
            }

            return $http({
                method: 'GET',
                url: 'rest/performing/radioInStore/utilizationFiles',
                params: {
                    'idMusicProvider': musicProvider ? musicProvider.idMusicProvider : undefined,
                    'idPalinsesto': palinsesto ? palinsesto.idPalinsesto : undefined,
                    'anno': anno,
                    'mesi': mes
                }
            }).then(function (response) {
                return response.status === 204 ?
                    UtilService.showAlert('Attenzione!','Nessun risultato trovato per la selezione effettuata') :
                    [].concat(response.data.perfRsUtilizationFile);
            })
        }

        function downloadUtilizationFile(id) {
            return $http({
                method: 'GET',
                url: 'rest/performing/radioInStore/utilizationFiles/' + id,
            }).then(function (response) {
                return [].concat(response.data.perfRsUtilizationFile);
            })
        }
        function scaricaFileArmonizzato(musicProvider,palinsesto,anno,mesiSelected) {
            var mes = [];
            for (var i = 0; i < mesiSelected.length; i++) {
                mes.push(mesiSelected[i].id)
            }
            return $http({
                method: 'POST',
                url: 'rest/performing/radioInStore/armonizzato',
                data: {
                    'musicProvider': {'idMusicProvider': musicProvider},
                    'palinsesti': [{'idPalinsesto': palinsesto.idPalinsesto, 'nome': palinsesto.nome}],
                    'anno': anno,
                    'mesi': mes
                }
            }).then(function (response) {
                return response.status === 204 ?
                    UtilService.showAlert('Attenzione!','Nessun risultato trovato per la selezione effettuata') :
                    response
            })
        }
    }
})();