/**
 * UtentiCtrl
 *
 * @path /utenti
 */
codmanApp.controller('GestionePalinsestiCtrl', ['$scope', '$route', 'ngDialog', '$routeParams', '$location', '$http', 'gestioneMusicProvidersService',
    function ($scope, $route, ngDialog, $routeParams, $location, $http, gestioneMusicProvidersService) {

    $scope.ctrl = {
        navbarTab: 'utenti',
        utenti: [],
        execute: $routeParams.execute,
        fiteredName: "",
        palinsesti: [],
        filterMusicProviderName: "",
        sortType: 'musicProvider.nome',
        sortOrder: 'musicProvider.nome'
    };

    // Funzione principale richiamata al caricamento della pagina
    $scope.onRefresh = function () {
        $scope.getMusicProviders($scope.ctrl.filterMusicProviderName)
    };
    $scope.back = function () {
        $scope.ctrl.selectedMusicProvider = null;
        $scope.getMusicProviders($scope.ctrl.filterMusicProviderName)
    };
    $scope.getMusicProviders = function (nameFilter) {
        gestioneMusicProvidersService.getMusicProviders(nameFilter)
            .then(function (response) {
                $scope.ctrl.musicProvider = response;
        });

    };

    $scope.sort = function (sort) {
        if (sort === "NomeMusicProvider") {
            if ($scope.ctrl.sortType === "+nome") {
                $scope.ctrl.sortType = "-nome";
            } else {
                $scope.ctrl.sortType = "+nome";
            }
        } else if (sort === "DataCreazione") {
            if ($scope.ctrl.sortType === "+data_creazione") {
                $scope.ctrl.sortType = "-data_creazione";
            } else {
                $scope.ctrl.sortType = "+data_creazione";
            }
        } else if (sort === "Attivo") {
            if ($scope.ctrl.sortType === "-data_disattivazione") {
                $scope.ctrl.sortType = "+data_disattivazione";
            } else {
                $scope.ctrl.sortType = "-data_disattivazione";
            }
        } else if (sort === "DataFineValidita") {
            if ($scope.ctrl.sortType === "+data_disattivazione") {
                $scope.ctrl.sortType = "-data_disattivazione";
            } else {
                $scope.ctrl.sortType = "+data_disattivazione";
            }
        }
    };

    $scope.showMusicProvider = function (musicProvider) {
        $scope.ctrl.selectedMusicProvider = musicProvider;
        $scope.getUserByMusicProvider(musicProvider);
        $scope.getPalinsesti(musicProvider);
    };

    $scope.getUserByMusicProvider = function (musicProvider) {
        gestioneMusicProvidersService.getUserByMusicProvider(musicProvider)
            .then(function (response) {
                $scope.ctrl.utenti = response;
            });
    };

    $scope.getPalinsesti = function (musicProvider) {
        gestioneMusicProvidersService.getPalinsesti(musicProvider)
            .then(function (response) {
                $scope.ctrl.palinsesti = response;
                $scope.ctrl.selectedMusicProvider.dataFineValidita=null;
                var active=false;
                for (var palinsesto in $scope.ctrl.palinsesti) {
					var now = new Date();
					now.setHours(0,0,0,0);
					if (new Date($scope.ctrl.palinsesti[palinsesto].dataFineValidita) < now) {
						if($scope.ctrl.selectedMusicProvider.dataFineValidita==null){
							if(!active){
								$scope.ctrl.selectedMusicProvider.dataFineValidita=$scope.ctrl.palinsesti[palinsesto].dataFineValidita;
							}
						}else{
							if(new Date($scope.ctrl.selectedMusicProvider.dataFineValidita) < new Date($scope.ctrl.palinsesti[palinsesto].dataFineValidita)){
								if(!active){
									$scope.ctrl.selectedMusicProvider.dataFineValidita=$scope.ctrl.palinsesti[palinsesto].dataFineValidita;
								}
							}
						}
						$scope.ctrl.palinsesti[palinsesto].active = false;
					} else {
					   $scope.ctrl.palinsesti[palinsesto].active = true;
					   $scope.ctrl.selectedMusicProvider.dataFineValidita=null;
					   active=true
					}
                }

            });
    };


    //funzione che renderizza il popup per l'aggiunta di una configurazione del DSP
    $scope.showAddUtente = function (utente) {
        var rootScope = $scope;
        var utenteUltimaModifica = utente;
        ngDialog.open({
            template: 'pages/performing/radio-in-store/addUtente.jsp',
            plain: false,
            width: '60%',
            controller: ['$scope', 'ngDialog', '$http', function ($scope, ngDialog, $http) {

                $scope.ctrl = {
                    sendMail: false,
                    selectedUsername: "",
                    selectedPassword: "",
                    selectedEmail: "",
                    selectedMusicProvider: {},
                    utenteUltimaModifica: utenteUltimaModifica,
                    rScope: rootScope
                };


                $scope.showAlert = function (title, message) {
                    ngDialog.open({
                        template: 'pages/advance-payment/dialog-alert.html',
                        plain: false,
                        width: '50%',
                        controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
                            var ngDialogId = $scope.ngDialogId;
                            $scope.ctrl = {
                                title: title,
                                message: message,
                                ngDialogId: ngDialogId
                            };
                            $scope.cancel = function (ngDialogId) {
                                ngDialog.close(ngDialogId);
                            };
                        }]
                    }).closePromise.then(function (data) {
                        if (data.value === true) {

                        }
                    });
                };

                $scope.cancel = function () {
                    ngDialog.closeAll(false);
                };

                $scope.checkLowercase = function (selectedPassword) {
                    if (selectedPassword != undefined && selectedPassword != null && selectedPassword.match("(?=.*[a-z])")) {
                        return true;
                    } else {
                        return false;
                    }
                };

                $scope.checkNumber = function (selectedPassword) {
                    if (selectedPassword != undefined && selectedPassword != null && selectedPassword.match("(?=.*[0-9])")) {
                        return true;
                    } else {
                        return false;
                    }
                };

                $scope.checkUppercase = function (selectedPassword) {
                    if (selectedPassword != undefined && selectedPassword != null && selectedPassword.match("(?=.*[A-Z])")) {
                        return true;
                    } else {
                        return false;
                    }
                };

                $scope.checkSpecialChar = function (selectedPassword) {

                    if (selectedPassword != undefined && selectedPassword != null) {
                        var specialChars = "(?=.*[$@!%*#?&\-+_])";
                        for (i = 0; i < specialChars.length; i++) {
                            if (selectedPassword.indexOf(specialChars[i]) > -1) {
                                return true
                            }
                        }
                        return false;
                    } else {
                        return false;
                    }
                };
                $scope.checkSize = function (selectedPassword) {
                    if (selectedPassword != undefined && selectedPassword != null && selectedPassword.match("(.{8,})")) {
                        return true;
                    } else {
                        return false;
                    }
                };

                $scope.save = function () {
                    var hasDefault = false;
                    $http({
                        method: 'PUT',
                        url: 'rest/performing/radioInStore/utente',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        data: {
                            sendMail: $scope.ctrl.sendMail,
                            username: $scope.ctrl.selectedUsername,
                            password: $scope.ctrl.selectedPassword,
                            email: $scope.ctrl.selectedEmail,
                            idMusicProvider: $scope.ctrl.rScope.ctrl.selectedMusicProvider.idMusicProvider,
                            utenteUltimaModifica: $scope.ctrl.utenteUltimaModifica
                        }
                    }).then(function successCallback(response) {
                        ngDialog.closeAll(false);
                        $scope.ctrl.rScope.getuserByMusicProvider();
                        $scope.showAlert("Esito", "Utente Aggiunto Correttamente");
                    }, function errorCallback(response) {
                        if (response.status == 501) {
                            $scope.showAlert("Errore", "Non è stato possibile aggiungere l'utente, username già presente");
                        } else if (response.status == 502) {
                            $scope.showAlert("Errore", "Non è stato possibile aggiungere l'utente, email già presente");
                        } else if (response.status == 503) {
                            $scope.showAlert("Errore", "Non è stato possibile aggiungere l'utente, email non valida");
                        } else if (response.status == 504) {
                            $scope.showAlert("Errore", "Non è stato possibile aggiungere l'utente, password non valida");
                        } else if (response.status == 400) {
                            $scope.showAlert("Errore", "Non è stato possibile aggiungere l'utente, parametri inseriti non validi");
                        } else {
                            $scope.showAlert("Errore", "Non è stato possibile aggiungere l'utente");
                        }

                    });
                };
            }]
        }).closePromise.then(function (data) {
            if (data.value === true) {
                // no op
            }
        });
    };

    $scope.deleteUser = function (item) {
        $http({
            method: 'POST',
            url: 'rest/performing/radioInStore/deleteUser',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                id: item.id,
                title: item.title,
                utenti: item.nres,
                validTo: item.validTo,
                validFrom: item.validFrom,
                creator: item.creator,
                idUtente: item.idUtente,
                bdcMusicProviders: item.bdcMusicProviders
            }
        }).then(function successCallback(response) {
            $scope.getuserByMusicProvider();
        }, function errorCallback(response) {
            $scope.showAlert("Errore", "Non è stato possibile cancellare l'utente");
        });
    };


    $scope.showError = function (title, msgs) {
        ngDialog.open({
            template: 'pages/advance-payment/dialog-alert.html',
            plain: false,
            width: '60%',
            controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
                $scope.ctrl = {
                    title: title,
                    message: msgs
                };
                $scope.cancel = function () {
                    ngDialog.closeAll(false);
                };
                $scope.save = function () {
                    ngDialog.closeAll(true);
                };
            }]
        }).closePromise.then(function (data) {
            // no op
        });
    };

    //funzione che renderizza il popup per l'aggiunta di una configurazione del DSP
    $scope.showUpdateUtente = function (user, item) {
        var rootScope = $scope;
        var utente = item;
        var username = user;
        ngDialog.open({
            template: 'pages/performing/radio-in-store/updateUtente.jsp',
            plain: false,
            width: '60%',
            controller: ['$scope', 'ngDialog', '$http', function ($scope, ngDialog, $http) {

                $scope.ctrl = {
                    sendMail: false,
                    selectedId: utente.id,
                    selectedUsername: utente.username,
                    selectedPassword: utente.password,
                    selectedEmail: utente.email,
                    selectedMusicProvider: utente.bdcMusicProviders,
                    selectedRole: {
                        "id": 2,
                        "ruolo": "BROADCASTER"
                    },
                    utenteUltimaModifica: username,
                    selectedObj: "",
                    rScope: rootScope
                };

                $scope.showAlert = function (title, message) {
                    ngDialog.open({
                        template: 'pages/advance-payment/dialog-alert.html',
                        plain: false,
                        width: '50%',
                        controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
                            var ngDialogId = $scope.ngDialogId;
                            $scope.ctrl = {
                                title: title,
                                message: message,
                                ngDialogId: ngDialogId
                            };
                            $scope.cancel = function (ngDialogId) {
                                ngDialog.close(ngDialogId);
                            };
                        }]
                    }).closePromise.then(function (data) {
                        if (data.value === true) {

                        }
                    });
                };

                $scope.cancel = function () {
                    ngDialog.closeAll(false);
                };
                $scope.regeneratePassword = function () {

                    $http({
                        method: 'POST',
                        url: 'rest/performing/radioInStore/resetPassword',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        data: {
                            id: $scope.ctrl.selectedId,
                            utenteUltimaModifica: $scope.ctrl.utenteUltimaModifica
                        }
                    }).then(function successCallback(response) {
                        ngDialog.closeAll(false);
                        $scope.ctrl.rScope.getuserByMusicProvider();
                        $scope.showAlert("Esito", "La Password è stata rigenerata correttamente ed è stata inoltrata alla mail dell'utente");
                    }, function errorCallback(response) {
                        $scope.showAlert("Errore", "Non è stato possibile rigenerare la password");
                    });
                };

                //MODIFICARE I PARAMETRI
                $scope.save = function () {

                    $http({
                        method: 'POST',
                        url: 'rest/performing/radioInStore/utente',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        data: {
                            id: $scope.ctrl.selectedId,
                            username: $scope.ctrl.selectedUsername,
                            email: $scope.ctrl.selectedEmail,
                            utenteUltimaModifica: username
                        }
                    }).then(function successCallback(response) {
                        ngDialog.closeAll(false);
                        $scope.ctrl.rScope.getuserByMusicProvider();
                        $scope.showAlert("Esito", "Utente Modificato Correttamente");
                    }, function errorCallback(response) {
                        if (response.status == 501) {
                            $scope.showAlert("Errore", "Non è stata fatta nessuna modifica");
                        } else if (response.status == 502) {
                            $scope.showAlert("Errore", "Non è stato possibile modificare l'utente, username/email già presenti");
                        } else if (response.status == 503) {
                            $scope.showAlert("Errore", "Non è stato possibile modificare l'utente, email non valida");
                        } else if (response.status == 400) {
                            $scope.showAlert("Errore", "Non è stato possibile modificare l'utente, parametri inseriti non validi");
                        } else {
                            $scope.showAlert("Errore", "Non è stato possibile modificare l'utente");
                        }

                    });
                };
            }]
        }).closePromise.then(function (data) {
            if (data.value === true) {
                // no op
            }
        });


    };


    //funzione che renderizza il popup per l'aggiunta di una configurazione del DSP
    $scope.showAddMusicProvider = function (utente) {
        var user = utente;
        var rootScope = $scope;
        ngDialog.open({
            template: 'pages/performing/radio-in-store/addMusicProvider.jsp',
            plain: false,
            width: '60%',
            controller: ['$scope', 'ngDialog', '$http', function ($scope, ngDialog, $http) {

                $scope.ctrl = {
                    utenteUltimaModifica: utente,
                    rScope: rootScope
                };

                $scope.cancel = function () {
                    ngDialog.closeAll(false);
                };

                //MODIFICARE I PARAMETRI
                $scope.save = function (username) {
                    $http({
                        method: 'POST',
                        url: 'rest/performing/radioInStore/musicProvider',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        data: {
                            nome: $scope.ctrl.selectedName,
                            username: username,
                            dataInizioValidita: $scope.ctrl.selectedDateMPFrom,
                            dataFineValidita: $scope.ctrl.selectedDateMPTo
                        }
                    }).then(function successCallback(response) {
                        ngDialog.closeAll(false);
                        rootScope.getMusicProviders();
                        $scope.showAlert("Esito", "Music Provider Aggiunto Correttamente");
                    }, function errorCallback(response) {
                        if (response.status == 409) {
                            $scope.showAlert("Errore", "Non è stato possibile aggiungere l'musicProvider");
                        } else {
                            $scope.showAlert("Errore", "Non è stato possibile aggiungere l'musicProvider");
                        }
                    });
                };
                $scope.showAlert = function (title, message) {
                    ngDialog.open({
                        template: 'pages/advance-payment/dialog-alert.html',
                        plain: false,
                        width: '50%',
                        controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
                            var ngDialogId = $scope.ngDialogId;
                            $scope.ctrl = {
                                title: title,
                                message: message,
                                ngDialogId: ngDialogId
                            };
                            $scope.cancel = function (ngDialogId) {
                                ngDialog.close(ngDialogId);
                            };
                        }]
                    }).closePromise.then(function (data) {
                        if (data.value === true) {

                        }
                    });
                };
            }]
        }).closePromise.then(function (data) {
            if (data.value === true) {
                // no op
            }
        });
    };


    //funzione che renderizza il popup per l'aggiunta di una configurazione del DSP
    $scope.showAddPalinsesto = function (utente) {
        var user = utente;
        var rootScope = $scope;
        ngDialog.open({
            //template : 'pages/performing/radio-in-store/addPalinsesto.jsp',
            template: 'pages/performing/radio-in-store/addPalinsesto.jsp',
            plain: false,
            width: '60%',
            controller: ['$scope', 'ngDialog', '$http', function ($scope, ngDialog, $http) {

                $scope.ctrl = {
                    codiceDitta: "",
                    selectedDateFrom: "",
                    selectedDateTo: "",
                    utenteUltimaModifica: utente,
                    rScope: rootScope
                };

                $scope.showAlert = function (title, message) {
                    ngDialog.open({
                        template: 'pages/advance-payment/dialog-alert.html',
                        plain: false,
                        width: '50%',
                        controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
                            var ngDialogId = $scope.ngDialogId;
                            $scope.ctrl = {
                                title: title,
                                message: message,
                                ngDialogId: ngDialogId
                            };
                            $scope.cancel = function (ngDialogId) {
                                ngDialog.close(ngDialogId);
                            };
                        }]
                    }).closePromise.then(function (data) {
                        if (data.value === true) {

                        }
                    });
                };

                $scope.cancel = function () {
                    ngDialog.closeAll(false);
                };

                //backend da sviluppare
                $scope.save = function (username) {

                    $http({
                        method: 'POST',
                        url: 'rest/performing/radioInStore/palinsesto',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        data: {
                            idMusicProvider: $scope.ctrl.rScope.ctrl.selectedMusicProvider.idMusicProvider,
                            nome: $scope.ctrl.nome,
                            codiceDitta: $scope.ctrl.codiceDitta,
                            dataInizioValidita: $scope.ctrl.selectedDateFrom,
                            dataFineValidita: $scope.ctrl.selectedDateTo,
                            utenteUltimaModifica: username
                        }
                    }).then(function successCallback(response) {
                        ngDialog.closeAll(false);
                        $scope.ctrl.rScope.getPalinsesti($scope.ctrl.rScope.ctrl.selectedMusicProvider);
                        $scope.showAlert("Esito", "Palinsesto Aggiunto Correttamente");
                    }, function errorCallback(response) {
                        if (response.status == 409) {
                            $scope.showAlert("Errore", "Non è stato possibile aggiungere il palinsesto, palinsesto già presente nel musicProvider selezionato");
                        } else if (response.status == 406) {
                            $scope.showAlert("Errore", "Non è stato possibile aggiungere il palinsesto, la data di fine è antecedente a quella d'inizio ");
                        } else if (response.status == 400) {
                            $scope.showAlert("Errore", "Non è stato possibile aggiungere il palinsesto, parametri d'invio non corretti");
                        } else {
                            $scope.showAlert("Errore", "Non è stato possibile aggiungere il palinsesto");
                        }
                    });
                };
            }]
        }).closePromise.then(function (data) {
            if (data.value === true) {
                // no op
            }
        });
    };

    //funzione che renderizza il popup per l'aggiunta di una configurazione del DSP
    $scope.showUpdatePalinsesto = function (utente, item) {
        var rootScope = $scope;
        var utente = utente;
        var dataInizio = $scope.formatDate(item.dataInizioValidita);
        var dataFine = $scope.formatDate(item.dataFineValidita);

        ngDialog.open({
            // template : 'pages/performing/radio-in-store/updatePalinsesto.jsp',
            template: 'pages/performing/radio-in-store/modificaPalinsesto.jsp',
            plain: false,
            width: '60%',
            controller: ['$scope', 'ngDialog', '$http', function ($scope, ngDialog, $http) {

                $scope.ctrl = {
                    idPalinsesto: item.idPalinsesto,
                    idMusicProvider: rootScope.ctrl.selectedMusicProvider.idMusicProvider,
                    utenteUltimaModifica: utente,
                    nome: item.nome,
                    codiceDitta: item.codiceDitta,
                    selectedDateFrom: dataInizio,
                    selectedDateTo: dataFine,
                    rScope: rootScope
                };

                $scope.formatDate = function (dateString) {
                    if (dateString != undefined && dateString != null) {
                        var date = new Date(dateString);
                        var separator = "-";
                        var month = date.getMonth() + 1;
                        return (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + separator + (month < 10 ? "0" + month : month) + separator + date.getFullYear() + " " + (date.getHours() < 10 ? "0" + date.getHours() : date.getHours()) + ":" + (date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes())
                    } else {
                        return "";
                    }
                };

                $scope.showAlert = function (title, message) {
                    ngDialog.open({
                        template: 'pages/advance-payment/dialog-alert.html',
                        plain: false,
                        width: '50%',
                        controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
                            var ngDialogId = $scope.ngDialogId;
                            $scope.ctrl = {
                                title: title,
                                message: message,
                                ngDialogId: ngDialogId
                            };
                            $scope.cancel = function (ngDialogId) {
                                ngDialog.close(ngDialogId);
                            };
                        }]
                    }).closePromise.then(function (data) {
                        if (data.value === true) {

                        }
                    });
                };

                $scope.cancel = function () {
                    ngDialog.closeAll(false);
                };

                //MODIFICARE I PARAMETRI
                $scope.save = function (username) {
                    $http({
                        method: 'PUT',
                        url: 'rest/performing/radioInStore/palinsesto',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        data: {
                            codiceDitta: $scope.ctrl.codiceDitta,
                            idMusicProvider: $scope.ctrl.idMusicProvider,
                            idPalinsesto: $scope.ctrl.idPalinsesto,
                            nome: $scope.ctrl.nome,
                            dataInizioValidita: $scope.ctrl.selectedDateFrom,
                            dataFineValidita: $scope.ctrl.selectedDateTo,
                            utenteUltimaModifica: username
                        }
                    }).then(function successCallback(response) {
                        ngDialog.closeAll(false);
                        $scope.ctrl.rScope.getPalinsesti($scope.ctrl.rScope.ctrl.selectedMusicProvider);
                        $scope.showAlert("Esito", "Palinsesto Modificato Correttamente");
                    }, function errorCallback(response) {
                        if (response.status == 410) {
                            $scope.showAlert("Errore", "Non sono state apportate le modifiche al palinsesto, nome palinsesto già esistente");
                        } else if (response.status == 409) {
                            $scope.showAlert("Errore", "Non sono state apportate le modifiche al palinsesto, i dati in input corrispondono a quelli attuali");
                        } else if (response.status == 406) {
                            $scope.showAlert("Errore", "Non è stato possibile aggiungere il palinsesto, la data di fine è antecedente a quella d'inizio ");
                        } else {
                            $scope.showAlert("Errore", "Non sono state effettuate modifiche al palinsesto");
                        }
                    });
                };
            }]
        }).closePromise.then(function (data) {
            if (data.value === true) {
                // no op
            }
        });
    };

    $scope.showStoricoPalinsesti = function (item) {
        var rootScope = $scope;
        var utente = item;
        ngDialog.open({
            // template : 'pages/performing/radio-in-store/storicoPalinsesto.jsp',
            template: 'pages/performing/radio-in-store/storicoPalinsesti.jsp',
            plain: false,
            width: '60%',
            controller: ['$scope', 'ngDialog', '$http', function ($scope, ngDialog, $http) {

                $scope.ctrl = {
                    idPalinsesto: item.idPalinsesto,
                    rScope: rootScope
                };

                $http({
                    method: 'GET',
                    url: 'rest/performing/radioInStore/storicoPalinsesti/' + $scope.ctrl.idPalinsesto,
                    params: {}
                }).then(function successCallback(response) {
                    if (Array.isArray(response.data.perfPalinsestoStorico)) {
                        $scope.ctrl.palinsesti = response.data.perfPalinsestoStorico;
                    } else {
                        $scope.ctrl.palinsesti = [response.data.perfPalinsestoStorico];
                    }
                }, function errorCallback(response) {
                    $scope.ctrl.palinsesti = [];
                });

                $scope.showAlert = function (title, message) {
                    ngDialog.open({
                        template: 'pages/advance-payment/dialog-alert.html',
                        plain: false,
                        width: '70%',
                        controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
                            var ngDialogId = $scope.ngDialogId;
                            $scope.ctrl = {
                                title: title,
                                message: message,
                                ngDialogId: ngDialogId
                            };
                            $scope.cancel = function (ngDialogId) {
                                ngDialog.close(ngDialogId);
                            };
                        }]
                    }).closePromise.then(function (data) {
                        if (data.value === true) {

                        }
                    });
                };

                $scope.checkSize = function () {
                    if ($scope.ctrl.palinsesti == null || $scope.ctrl.palinsesti == undefined || $scope.ctrl.palinsesti.length == 0) {
                        return true;
                    } else {
                        return false;
                    }

                };

                $scope.formatDate = function (dateString) {
                    if (dateString != undefined && dateString != null) {
                        var date = new Date(dateString);
                        var separator = "-";
                        var month = date.getMonth() + 1;
                        return (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + separator + (month < 10 ? "0" + month : month) + separator + date.getFullYear() + " " + (date.getHours() < 10 ? "0" + date.getHours() : date.getHours()) + ":" + (date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes())

                    } else {
                        return "";
                    }
                };

                $scope.cancel = function () {
                    ngDialog.closeAll(false);
                };

            }]
        }).closePromise.then(function (data) {
            if (data.value === true) {
                // no op
            }
        });
    };
    showAlert = function (title, message) {
        ngDialog.open({
            template: 'pages/advance-payment/dialog-alert.html',
            plain: false,
            width: '50%',
            controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
                var ngDialogId = $scope.ngDialogId;
                $scope.ctrl = {
                    title: title,
                    message: message,
                    ngDialogId: ngDialogId
                };
                $scope.cancel = function (ngDialogId) {
                    ngDialog.close(ngDialogId);
                };
            }]
        }).closePromise.then(function (data) {
            if (data.value === true) {

            }
        });
    };

    $scope.getImgStato = function (item) {
        if (item == 1) {
            return "images/ok.png";
        } else {
            return "images/ko.png";
        }
    };

    $scope.formatDate = function (dateString) {
        if (dateString != undefined && dateString != null) {
            var date = new Date(dateString);
            var separator = "-";
            var month = date.getMonth() + 1;
            return (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + separator + (month < 10 ? "0" + month : month) + separator + date.getFullYear()

        } else {
            return "";
        }
    };


    $scope.onRefresh();

}]);