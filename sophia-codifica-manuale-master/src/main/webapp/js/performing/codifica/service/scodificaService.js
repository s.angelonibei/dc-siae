(function() {
  'use strict';
  angular
    .module('codmanApp')
    .service('scodificaService', scodificaService);

  scodificaService.$inject = ['$http', 'UtilService', 'ngDialog'];

  function scodificaService($http, UtilService, ngDialog) {
    this.getListaScodifica = getListaScodifica;
    this.getDettaglioCombinazioneAnagrafica = getDettaglioCombinazioneAnagrafica;
    this.getListaUtilizzazioni = getListaUtilizzazioni;
    this.eliminaCodiceOpera = eliminaCodiceOpera;
    this.getDettaglioScodifica = getDettaglioScodifica;
    this.getSessioneScodifica = getSessioneScodifica;

    function getListaScodifica(model, sendError) {
      return $http({
        method: 'GET',
        url: 'rest/performing/scodifica',
        params: model
      }).then(getListaScodificaComplete, UtilService.handleHttpRejection);

      function getListaScodificaComplete(response) {
        if (response.status !== 204) {
          if (model.export === 'true') {
            // saveAs(new Blob([response.data],{type:response.headers('content-type')}), response.headers('content-disposition').split('"')[1]);
            UtilService.saveFile(response);
          } else {
            return response.data;
          }
        } else if (sendError === undefined || sendError === true) {
          UtilService.showAlert('Attenzione!', 'Non esistono risultati per la selezione effettuata');
        } else {
          return response.data;
        }
      }
    }

    function getDettaglioScodifica(idCombana) {
      return $http({
        method: 'GET',
        url: 'rest/performing/scodifica/dettaglio/' + idCombana
      }).then(getDettaglioScodificaComplete, UtilService.handleHttpRejection);

      function getDettaglioScodificaComplete(response) {
        if (response.status !== 204) {
          return response.data.list[0];
        }
        UtilService.showAlert('Attenzione!', 'Non esistono risultati per la selezione effettuata');
      }
    }

    function getDettaglioCombinazioneAnagrafica(idCombana) {
      return $http({
        method: 'GET',
        url: 'rest/performing/scodifica/combane/' + idCombana
      }).then(getDettaglioCombinazioneAnagraficaComplete, UtilService.handleHttpRejection);

      function getDettaglioCombinazioneAnagraficaComplete(response) {
        var newListDettaglioImporti = [];
        var totaleArea = 0;
        var i = 0;
        var totali = [];
        var _loop_1 = function(item) {
          totaleArea += item.importiMaturati ? item.importiMaturati : 0;
          var index = newListDettaglioImporti.findIndex(function(e) { return e.area === item.area; });
          addTotale(item);
          if (index > -1) {
            newListDettaglioImporti[index].dettaglioImporto.push(item);
          } else {
            newListDettaglioImporti.push({ area: item.area, dettaglioImporto: [item] });
          }
          if ((i !== (response.data.list.length - 1) && item.codVoceIncasso !== response.data.list[i + 1].codVoceIncasso) || i === (response.data.list.length - 1)) {
            totaleItem = JSON.parse(JSON.stringify(item));
            totaleItem.periodoRipartizione = 'Totale';
            totaleItem.importiMaturati = totaleArea;
            index = newListDettaglioImporti.findIndex(function(e) { return e.area === item.area; });
            newListDettaglioImporti[index].dettaglioImporto.push(totaleItem);
            addTotale(totaleItem);
            totaleArea = 0;
          }
          i++;
        };
        var totaleItem;
        for (var _i = 0, _a = response.data.list; _i < _a.length; _i++) {
          var item = _a[_i];
          _loop_1(item);
        }
        function addTotale(item) {
          var totale = totali.find(function(e) { return e.periodoRipartizione === item.periodoRipartizione; });
          totale ?
            totali[totali.findIndex(function(e) { return e === totale; })].importiMaturati += (item.importiMaturati ? item.importiMaturati : 0) :
            totali.push({
              codVoceIncasso: undefined,
              area: 'Totale',
              importiMaturati: item.importiMaturati,
              periodoRipartizione: item.periodoRipartizione
            });
        }
        totali.push(totali.splice(totali.findIndex(function(e) { return e.periodoRipartizione === 'Totale'; }), 1)[0]);
        newListDettaglioImporti.push({ area: 'Totale', dettaglioImporto: totali });
        return newListDettaglioImporti;
      }
    }

    function getSessioneScodifica(combana, utente) {
      if (combana.idCodifica) {
        return $http({
          method: 'POST',
          url: 'rest/performing/scodifica/combane/' + combana.idCombana + '/sessioni-codifica',
          params: {
            utente: utente,
            idCodifica: combana.idCodifica
          }
        }).then(getSessioneScodificaComplete, getSessioneScodificaFailed);
      }
      UtilService.showAlert('Attenzione!', 'La combana selezionata non ha una codifica associata');


      function getSessioneScodificaComplete(response) {
        return ngDialog.open({
          template: 'pages/advance-payment/dialog-codifica.html',
          plain: false,
          width: '90%',
          controller: 'ModificaCodiceOperaController',
          data: {
            combana: combana,
            utente: utente,
            sessione: response.data

          },
          controllerAs: 'vm'
        });
      }

      function getSessioneScodificaFailed(error) {
        if (error.status === 409 ) {
          UtilService.showAlert('Attenzione!', 'la combana selezionata è già in lavorazione da un altro utente');
        } else {
          UtilService.handleHttpRejection(error);
        }
      }
    }

    function eliminaCodiceOpera(model) {
      return $http({
        method: 'PATCH',
        url: 'rest/performing/scodifica/' + model.idCodifica,
        params: {
          idCombana: model.idCombana,
          codiceOpera: model.codiceOperaApprovato
        }
      }).then(eliminaCodiceOperaComplete, UtilService.handleHttpRejection);

      function eliminaCodiceOperaComplete(response) {
        UtilService.showError('Successo', 'Eliminazione codice avvenuta con successo');
        return response;
      }
    }

    function getListaUtilizzazioni(idCombana) {
      return $http({
        method: 'GET',
        url: 'rest/performing/scodifica/combane/' + idCombana + '/utilizzazioni'
      }).then(getListaUtilizzazioniComplete, UtilService.handleHttpRejection);

      function getListaUtilizzazioniComplete(response) {
        if (response.status !== 204) {
          // saveAs(new Blob([response.data],{type:response.headers('content-type')}), response.headers('content-disposition').split('"')[1]);
          UtilService.saveFile(response);
        } else {
          UtilService.showAlert('Attenzione!', 'Non esistono risultati per la selezione effettuata');
        }
      }
    }
  }
})();
