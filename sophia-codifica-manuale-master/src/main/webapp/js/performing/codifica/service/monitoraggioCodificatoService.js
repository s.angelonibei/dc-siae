(function() {
  'use strict';
  angular
    .module('codmanApp')
    .service('monitoraggiCodificaService', monitoraggiCodificaService);

  monitoraggiCodificaService.$inject = ['$http'];

  function monitoraggiCodificaService($http) {
    this.getMonitoraggioCodificato = function(model) {
      return $http({
        method: 'GET',
        url: 'rest/performing/codifica/manuale/monitoraggio',
        params: model
      });
    };
  }
})();
