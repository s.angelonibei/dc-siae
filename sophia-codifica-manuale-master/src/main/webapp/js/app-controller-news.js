/**
 * NewsCtrl
 *
 * @path /news
 */
codmanApp.controller('NewsCtrl', [ '$scope', '$route', 'ngDialog', '$routeParams', '$location', '$http', function($scope, $route, ngDialog, $routeParams, $location, $http) {


    $scope.navigateToNextPage = function () {
        $scope.ctrl.first = $scope.ctrl.last;
        $scope.ctrl.last = $scope.ctrl.last + $scope.ctrl.maxrows;
        $scope.getNews();

    };

    $scope.navigateToPreviousPage = function () {
        $scope.ctrl.last = $scope.ctrl.first;
        $scope.ctrl.first = Math.max($scope.ctrl.first - $scope.ctrl.maxrows, 0);
        $scope.getNews();

    };

    $scope.multiselectSettings = {
        scrollableHeight: '200px',
        scrollable: false,
        enableSearch: true
    };

    $scope.ctrl = {
        listaStati: [
            {
                "id": "-1",
                "name": "Tutte",
                "flagActive": -2
            },
            {
                "id": "1",
                "name": "Attiva",
                "flagActive": 1
            },
            {
                "id": "0",
                "name": "Disattiva",
                "flagActive": 0
            }],
        navbarTab: 'news',
        news: [],
        execute: $routeParams.execute,
        filteredBroadcaster: [],
        first: 0,
        last: 50,
        maxrows: 50,
        sortType: "+title"
    };

    $scope.sort = function (sort) {
        if (sort === "Titolo") {
            if ($scope.ctrl.sortType === "+title") {
                $scope.ctrl.sortType = "-title";
            } else {
                $scope.ctrl.sortType = "+title";
            }
        } else if (sort === "Testo") {
            if ($scope.ctrl.sortType === "+news") {
                $scope.ctrl.sortType = "-news";
            } else {
                $scope.ctrl.sortType = "+news";
            }
        } else if (sort === "DataCreazione") {
            if ($scope.ctrl.sortType === "+insertDate") {
                $scope.ctrl.sortType = "-insertDate";
            } else {
                $scope.ctrl.sortType = "+insertDate";
            }
        } else if (sort === "ValidaDa") {
            if ($scope.ctrl.sortType === "-validFrom") {
                $scope.ctrl.sortType = "+validFrom";
            } else {
                $scope.ctrl.sortType = "-validFrom";
            }
        } else if (sort === "ValidaA") {
            if ($scope.ctrl.sortType === "+validTo") {
                $scope.ctrl.sortType = "-validTo";
            } else {
                $scope.ctrl.sortType = "+validTo";
            }
        } else if (sort === "Stato") {
            if ($scope.ctrl.sortType === "+flagActive") {
                $scope.ctrl.sortType = "-flagActive";
            } else {
                $scope.ctrl.sortType = "+flagActive";
            }
        }
    };

    // Funzione principale richiamata al caricamento della pagina
    $scope.onRefresh = function () {
        $scope.ctrl.last = 50;
        $scope.getNews();
        $scope.getEmittenti();
    };

    $scope.getEmittenti = function () {
        $http({
            method: 'GET',
            url: 'rest/broadcasting/emittenti',
            params: {}
        }).then(function successCallback(response) {
            $scope.ctrl.listaEmittenti = response.data;
            $scope.ctrl.listaEmittentiLabel = [];
            for (var i in $scope.ctrl.listaEmittenti) {
                item = {};
                item["id"] = $scope.ctrl.listaEmittenti[i].id;// Primary Key;
                item["label"] = $scope.ctrl.listaEmittenti[i].nome;// Any
                // Description
                // Field;
                $scope.ctrl.listaEmittentiLabel.push(item);
            }
        }, function errorCallback(response) {
            $scope.ctrl.listaEmittenti = [];
            $scope.ctrl.listaEmittentiLabel = [];
        });

    };


    $scope.getNameForSelect = function (item) {
        if (item.tipo_broadcaster == "TELEVISIONE") {
            return item.nome + " - TV";
        } else if (item.tipo_broadcaster == "RADIO") {
            return item.nome + " - RADIO";
        } else {
            return item.nome;
        }
    };

    $scope.formatDate = function (dateString) {
        var date = new Date(dateString);
        var separator = "-";
        var month = date.getMonth() + 1;
        return (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + separator + (month < 10 ? "0" + month : month)
            + separator + date.getFullYear()
    };

    $scope.formatDateHour = function (dateString) {
        if (dateString != undefined && dateString != null) {
            var date = new Date(dateString);
            var separator = "-";
            var month = date.getMonth() + 1;
            return (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + separator + (month < 10 ? "0" + month : month) + separator + date.getFullYear() + " " + (date.getHours() < 10 ? "0" + date.getHours() : date.getHours()) + ":" + (date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes())

        } else {
            return "";
        }
    };

    $scope.filterApply = function () {
        $scope.ctrl.first = 0;
        $scope.ctrl.last = 50;
        $scope.ctrl.news = {};
        $scope.getNews();
    };

    $scope.downloadCsv = function () {
        $scope.ctrl.first = 0;
        $scope.ctrl.last = 50;
        $scope.ctrl.news = {};
        $scope.downloadNews();
    };


    $scope.downloadNews = function () {
        var idBroadcaster = [];
        for (var i in $scope.ctrl.filteredBroadcaster) {
            idBroadcaster.push($scope.ctrl.filteredBroadcaster[i].id);
        }
    };

    $scope.downloadCsv = function () {
        $scope.ctrl.first = 0;
        $scope.ctrl.last = 50;
        $scope.downloadNews();
    };


    $scope.downloadNews = function () {
        var idBroadcaster = [];
        for (var i in $scope.ctrl.filteredBroadcaster) {
            idBroadcaster.push($scope.ctrl.filteredBroadcaster[i].id);
        }

        var stato = "-1";
        if ($scope.ctrl.filteredStato != undefined) {
            stato = $scope.ctrl.filteredStato.id;
        }
        $http({
            method: 'POST',
            url: 'rest/news/downloadAllnews',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                titolo: $scope.ctrl.filteredTitle,
                testo: $scope.ctrl.filteredText,
                validaDa: $scope.ctrl.fiteredFromDate,
                validaA: $scope.ctrl.fiteredToDate,
                dataCreazioneDa : $scope.ctrl.filteredCreationFrom,
                dataCreazioneA : $scope.ctrl.filteredCreationTo,
                testo: $scope.ctrl.filteredText,
                flagActive: stato,
                idBroadcaster: idBroadcaster,
                first: $scope.ctrl.first,
                last: $scope.ctrl.last
            }
        }).then(function successCallback(response) {

            if (typeof response == 'string') {
                $scope.showError("Errore", response);
            } else {
                saveAs(new Blob([response.data],{type:"text/plain;charset=UTF-8"}), "Elenco-News.csv");
            }

        }, function errorCallback(response) {
            $scope.showError("Errore", "Errore generico");
        });

    };


    $scope.getNews = function () {
        var idBroadcaster = [];
        for (var i in $scope.ctrl.filteredBroadcaster) {
            idBroadcaster.push($scope.ctrl.filteredBroadcaster[i].id);
        }

        var stato = "-1";
        if ($scope.ctrl.filteredStato != undefined) {
            stato = $scope.ctrl.filteredStato.id;
        }

        $http({
            method: 'POST',
            url: 'rest/news/allnews',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                titolo: $scope.ctrl.filteredTitle,
                testo: $scope.ctrl.filteredText,
                validaDa: $scope.ctrl.fiteredFromDate,
                validaA: $scope.ctrl.fiteredToDate,
                dataCreazioneDa : $scope.ctrl.filteredCreationFrom,
                dataCreazioneA : $scope.ctrl.filteredCreationTo,
                testo: $scope.ctrl.filteredText,
                flagActive: stato,
                idBroadcaster: idBroadcaster,
                first: $scope.ctrl.first,
                last: $scope.ctrl.last
            }
        }).then(function successCallback(response) {
            $scope.ctrl.first = response.data.first;
            $scope.ctrl.last = response.data.last;
            $scope.ctrl.news = response.data;

        }, function errorCallback(response) {
            $scope.ctrl.news = [];
        });

    };


    $scope.showAddNews = function (event) {
        var rootScope = $scope;
        ngDialog.open({
            template: 'pages/broadcasting/news/addNews.jsp',
            plain: false,
            width: '60%',
            controller: ['$scope', 'ngDialog', '$http', function ($scope, ngDialog, $http) {

                $scope.ctrl = {
                    selectedBroadcaster: "",
                    selectedUser: "",
                    rScope: rootScope,
                    emittenti: [],
                    utenti: [],
                    tipiEmittenti: [
                        {
                            "nome": "TUTTI"
                        }, {
                            "nome": "TELEVISIONE"
                        }, {
                            "nome": "RADIO"
                        }],
                };

                $scope.getEmittenti = function (item) {
                    $scope.ctrl.utenti = [];
                    $scope.ctrl.selectedUser = "";
                    var tipoBroadcaster = item;
                    $http({
                        method: 'GET',
                        url: 'rest/broadcasting/emittentiFiltrati',
                        params: {
                            fiteredType: tipoBroadcaster
                        }
                    }).then(function successCallback(response) {
                        $scope.ctrl.emittenti = response.data;
                        $scope.ctrl.emittenti.unshift({
                            "id": -1,
                            "nome": "TUTTI",
                            "tipo_broadcaster": ""
                        })
                    }, function errorCallback(response) {
                        $scope.ctrl.emittenti = [];
                    });
                };

                $scope.search = function (id) {
                    $http({
                        method: 'POST',
                        url: 'rest/user/userByBroadcastId',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        data: {
                            id: $scope.ctrl.selectedBroadcaster.id,
                            nome: $scope.ctrl.selectedBroadcaster.nome,
                            tipo_broadcaster: $scope.ctrl.selectedBroadcaster.tipo_broadcaster
                        }
                    }).then(function successCallback(response) {
                        $scope.ctrl.utenti = response.data;
                        $scope.ctrl.utenti.unshift({
                            "id": -1,
                            "username": "TUTTI",
                            "email": "",
                            "password": "",
                            "bdcBroadcasters": {
                                "id": -1,
                                "nome": "",
                                "tipo_broadcaster": ""
                            },
                            "bdcRuoli": {
                                "id": -1,
                                "ruolo": ""
                            }
                        });

                    }, function errorCallback(response) {
                        $scope.ctrl.utenti = [];
                    });
                };


                $scope.showAlert = function (title, message) {
                    ngDialog.open({
                        template: 'pages/advance-payment/dialog-alert.html',
                        plain: false,
                        width: '50%',
                        controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
                            var ngDialogId = $scope.ngDialogId;
                            $scope.ctrl = {
                                title: title,
                                message: message,
                                ngDialogId: ngDialogId
                            };
                            $scope.cancel = function (ngDialogId) {
                                ngDialog.close(ngDialogId);
                            };
                        }]
                    }).closePromise.then(function (data) {
                        if (data.value === true) {

                        }
                    });
                };

                $scope.cancel = function () {
                    ngDialog.closeAll(false);
                };


                // MODIFICARE I PARAMETRI
                $scope.save = function (username) {
                    var user = username;
                    var broadcaster;
                    var idUtente;
                    if ($scope.ctrl.selectedBroadcaster != undefined && $scope.ctrl.selectedBroadcaster.id >= 0) {
                        broadcaster = $scope.ctrl.selectedBroadcaster;
                    }
                    if ($scope.ctrl.selectedUser != undefined && $scope.ctrl.selectedUser.id >= 0) {
                        idUtente = $scope.ctrl.selectedUser.id;
                    }

                    if ($scope.ctrl.selectedDateTo != undefined) {
                        var numbers = $scope.ctrl.selectedDateTo.match(/\d+/g);
                        var dateValidTo = new Date(numbers[2], numbers[1] - 1, numbers[0]);
                    }
                    if ($scope.ctrl.selectedDateFrom != undefined) {
                        var numbers2 = $scope.ctrl.selectedDateFrom.match(/\d+/g);
                        var dateValidFrom = new Date(numbers2[2], numbers2[1] - 1, numbers2[0]);
                    }


                    $http({
                        method: 'POST',
                        url: 'rest/news/addNews',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        data: {
                            title: $scope.ctrl.selectedTitle,
                            news: $scope.ctrl.selectedText,
                            validTo: dateValidTo,
                            validFrom: dateValidFrom,
                            creator: user,
                            idUtente: idUtente,
                            bdcBroadcasters: broadcaster
                        }
                    }).then(function successCallback(response) {
                        ngDialog.closeAll(false);
                        $scope.ctrl.rScope.onRefresh();
                        $scope.showAlert("Esito", "News Aggiunta Correttamente");
                    }, function errorCallback(response) {
                        if (response.status == 409) {
                            $scope.showAlert("Errore", "Configurazione già presente.");
                        } else {
                            $scope.showAlert("Errore", response.data);
                        }

                    });
                };
            }]
        }).closePromise.then(function (data) {
            if (data.value === true) {
                // no op
            }
        });
    };

    $scope.deactivationNews = function (item) {
        $http({
            method: 'POST',
            url: 'rest/news/deactivateNews',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                id: item.id,
                title: item.title,
                news: item.nres,
                validTo: item.validTo,
                validFrom: item.validFrom,
                creator: item.creator,
                idUtente: item.idUtente,
                bdcBroadcasters: item.bdcBroadcasters
            }
        }).then(function successCallback(response) {
            $scope.onRefresh();
        }, function errorCallback(response) {
            $scope.ctrl.emittenti = [];
        });
    };

    $scope.activationNews = function (item) {
        $http({
            method: 'POST',
            url: 'rest/news/activateNews',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                id: item.id,
                title: item.title,
                news: item.nres,
                validTo: item.validTo,
                validFrom: item.validFrom,
                creator: item.creator,
                idUtente: item.idUtente,
                bdcBroadcasters: item.bdcBroadcasters
            }
        }).then(function successCallback(response) {
            $scope.onRefresh();
        }, function errorCallback(response) {
            $scope.ctrl.emittenti = [];
        });
    };

    // funzione che renderizza il popup per l'aggiunta di una configurazione del
    // DSP
    $scope.showUpdateNews = function (item) {
        var rootScope = $scope;
        var news = item;
        selectedDateFromFormat = $scope.formatDate(news.validFrom);
        selectedDateToFormat = $scope.formatDate(news.validTo);
        ngDialog.open({
            template: 'pages/broadcasting/news/updateNews.jsp',
            plain: false,
            width: '60%',
            controller: ['$scope', 'ngDialog', '$http', function ($scope, ngDialog, $http) {

                $scope.ctrl = {
                    id: news.id,
                    selectedTitle: news.title,
                    selectedText: news.news,
                    selectedDateFrom: selectedDateFromFormat,
                    selectedDateTo: selectedDateToFormat,
                    rScope: rootScope,
                    emittenti: [],
                    utenti: [],
                    tipiEmittenti: [
                        {
                            "nome": "TUTTI"
                        }, {
                            "nome": "TELEVISIONE"
                        }, {
                            "nome": "RADIO"
                        }],
                };


                $scope.getEmittenti = function (item) {
                    $scope.ctrl.utenti = [];
                    $scope.ctrl.selectedUser = "";
                    var tipoBroadcaster = item;
                    $http({
                        method: 'GET',
                        url: 'rest/broadcasting/emittentiFiltrati',
                        params: {
                            fiteredType: tipoBroadcaster
                        }
                    }).then(function successCallback(response) {
                        $scope.ctrl.emittenti = response.data;
                        $scope.ctrl.selectedBroadcaster = news.bdcBroadcasters;
                        $scope.ctrl.emittenti.unshift({
                            "id": -1,
                            "nome": "TUTTI",
                            "tipo_broadcaster": ""
                        });
                        if ($scope.ctrl.selectedBroadcaster != null) {
                            $scope.search($scope.ctrl.selectedBroadcaster.id);
                        } else {
                            $scope.ctrl.selectedBroadcaster = {
                                "id": -1,
                                "nome": "TUTTI",
                                "tipo_broadcaster": ""
                            };
                            $scope.search(-1);
                        }
                    }, function errorCallback(response) {
                        $scope.ctrl.emittenti = [];
                    });
                };

                $scope.search = function (id) {
                    $http({
                        method: 'POST',
                        url: 'rest/user/userByBroadcastId',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        data: {
                            id: $scope.ctrl.selectedBroadcaster.id,
                            nome: $scope.ctrl.selectedBroadcaster.nome,
                            tipo_broadcaster: $scope.ctrl.selectedBroadcaster.tipo_broadcaster
                        }
                    }).then(function successCallback(response) {
                        $scope.ctrl.utenti = response.data;
                        $scope.ctrl.utenti.unshift({
                            "id": -1,
                            "username": "TUTTI",
                            "email": "",
                            "password": "",
                            "bdcBroadcasters": {
                                "id": -1,
                                "nome": "",
                                "tipo_broadcaster": ""
                            },
                            "bdcRuoli": {
                                "id": -1,
                                "ruolo": ""
                            }
                        });
                        for (i = 0; i < $scope.ctrl.utenti.length; i++) {
                            if ($scope.ctrl.utenti[i].id == news.idUtente) {
                                $scope.ctrl.selectedUser = $scope.ctrl.utenti[i];
                            }
                        }
                        if ($scope.ctrl.selectedUser == undefined) {
                            $scope.ctrl.selectedUser = {
                                "id": -1,
                                "username": "TUTTI",
                                "email": "",
                                "password": "",
                                "bdcBroadcasters": {
                                    "id": -1,
                                    "nome": "",
                                    "tipo_broadcaster": ""
                                },
                                "bdcRuoli": {
                                    "id": -1,
                                    "ruolo": ""
                                }
                            }

                        }

                    }, function errorCallback(response) {
                        $scope.ctrl.utenti = [];
                    });
                };


                $scope.showAlert = function (title, message) {
                    ngDialog.open({
                        template: 'pages/advance-payment/dialog-alert.html',
                        plain: false,
                        width: '50%',
                        controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
                            var ngDialogId = $scope.ngDialogId;
                            $scope.ctrl = {
                                title: title,
                                message: message,
                                ngDialogId: ngDialogId
                            };
                            $scope.cancel = function (ngDialogId) {
                                ngDialog.close(ngDialogId);
                            };
                        }]
                    }).closePromise.then(function (data) {
                        if (data.value === true) {

                        }
                    });
                };

                $scope.cancel = function () {
                    ngDialog.closeAll(false);
                };

                $scope.formatDate = function (dateString) {
                    var date = new Date(dateString);
                    var separator = "-";
                    var month = date.getMonth() + 1;
                    return (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + separator + (month < 10 ? "0" + month : month)
                        + separator + date.getFullYear()
                };

                for (i = 0; i < $scope.ctrl.tipiEmittenti.length; i++) {
                    if (news.bdcBroadcasters != null && news.bdcBroadcasters != undefined && $scope.ctrl.tipiEmittenti[i].nome == news.bdcBroadcasters.tipo_broadcaster) {
                        $scope.ctrl.selectedTipoBroadcaster = $scope.ctrl.tipiEmittenti[i];
                        $scope.getEmittenti($scope.ctrl.selectedTipoBroadcaster.nome);
                    }
                }
                if ($scope.ctrl.selectedTipoBroadcaster == undefined) {
                    $scope.ctrl.selectedTipoBroadcaster = {
                        "nome": "TUTTI"
                    };
                    $scope.ctrl.emittenti.unshift({
                        "id": -1,
                        "nome": "TUTTI",
                        "tipo_broadcaster": ""
                    });
                    $scope.ctrl.selectedBroadcaster = {
                        "id": -1,
                        "nome": "TUTTI",
                        "tipo_broadcaster": ""
                    };
                    $scope.ctrl.utenti.unshift({
                        "id": -1,
                        "username": "TUTTI",
                        "email": "",
                        "password": "",
                        "bdcBroadcasters": {
                            "id": -1,
                            "nome": "",
                            "tipo_broadcaster": ""
                        },
                        "bdcRuoli": {
                            "id": -1,
                            "ruolo": ""
                        }
                    });

                    $scope.ctrl.selectedUser = {
                        "id": -1,
                        "username": "TUTTI",
                        "email": "",
                        "password": "",
                        "bdcBroadcasters": {
                            "id": -1,
                            "nome": "",
                            "tipo_broadcaster": ""
                        },
                        "bdcRuoli": {
                            "id": -1,
                            "ruolo": ""
                        }
                    }


                }

                // MODIFICARE I PARAMETRI
                $scope.save = function (username) {
                    var user = username;
                    var broadcaster;
                    var idUtente;
                    var dateValidTo;
                    var dateValidFrom;
                    if ($scope.ctrl.selectedBroadcaster != undefined && $scope.ctrl.selectedBroadcaster.id >= 0) {
                        broadcaster = $scope.ctrl.selectedBroadcaster;
                    }
                    if ($scope.ctrl.selectedUser != undefined && $scope.ctrl.selectedUser.id >= 0) {
                        idUtente = $scope.ctrl.selectedUser.id;
                    }

                    if ($scope.ctrl.selectedDateTo != undefined) {
                        var numbers = $scope.ctrl.selectedDateTo.match(/\d+/g);
                        dateValidTo = new Date(numbers[2], numbers[1] - 1, numbers[0]);
                    }
                    if ($scope.ctrl.selectedDateFrom != undefined) {
                        var numbers = $scope.ctrl.selectedDateFrom.match(/\d+/g);
                        dateValidFrom = new Date(numbers[2], numbers[1] - 1, numbers[0]);
                    }
                    $http({
                        method: 'POST',
                        url: 'rest/news/updateNews',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        data: {
                            id: $scope.ctrl.id,
                            title: $scope.ctrl.selectedTitle,
                            news: $scope.ctrl.selectedText,
                            validTo: dateValidTo,
                            validFrom: dateValidFrom,
                            creator: user,
                            idUtente: idUtente,
                            bdcBroadcasters: broadcaster
                        }
                    }).then(function successCallback(response) {
                        ngDialog.closeAll(false);
                        $scope.ctrl.rScope.onRefresh();
                        $scope.showAlert("Esito", "News Modificata Correttamente");
                    }, function errorCallback(response) {
                        if (response.status == 409) {
                            $scope.showAlert("Errore", "Configurazione già presente.");
                        } else {
                            $scope.showAlert("Errore", response.data);
                        }

                    });


                };
            }]
        }).closePromise.then(function (data) {
            if (data.value === true) {
                // no op
            }
        });


    };

    // funzione che renderizza il popup per l'aggiunta di una configurazione del
    // DSP
    $scope.showNewsDettails = function (item) {
        var rootScope = $scope;
        var news = item;
        // selectedDateFromFormat = $scope.formatDate(news.validFrom);
        selectedDateFromFormat = null;
        selectedDateToFormat = null;
        ngDialog.open({
            template: 'pages/broadcasting/news/dettailsNews.jsp',
            plain: false,
            width: '60%',
            controller: ['$scope', 'ngDialog', '$http', function ($scope, ngDialog, $http) {

                $scope.ctrl = {
                    id: news.id,
                    selectedTitle: news.title,
                    selectedText: news.news,
                    selectedDateFrom: selectedDateFromFormat,
                    selectedDateTo: selectedDateToFormat,
                    rScope: rootScope,
                    emittenti: [],
                    utenti: [],
                    tipiEmittenti: [{
                        "nome": "TUTTI"
                    }, {
                        "nome": "TELEVISIONE"
                    }, {
                        "nome": "RADIO"
                    }],
                };


                $scope.getEmittenti = function (item) {
                    $scope.ctrl.utenti = [];
                    $scope.ctrl.selectedUser = undefined;
                    var tipoBroadcaster = item;
                    $http({
                        method: 'GET',
                        url: 'rest/broadcasting/emittentiFiltrati',
                        params: {
                            fiteredType: tipoBroadcaster
                        }
                    }).then(function successCallback(response) {
                        $scope.ctrl.emittenti = response.data;
                        $scope.ctrl.selectedBroadcaster = news.bdcBroadcasters;
                        $scope.ctrl.emittenti.unshift({
                            "id": -1,
                            "nome": "TUTTI",
                            "tipo_broadcaster": ""
                        });
                        if ($scope.ctrl.selectedBroadcaster != null) {
                            $scope.search($scope.ctrl.selectedBroadcaster.id);
                        } else {
                            $scope.ctrl.selectedBroadcaster = {
                                "id": -1,
                                "nome": "TUTTI",
                                "tipo_broadcaster": ""
                            };
                            $scope.search(-1);
                        }
                    }, function errorCallback(response) {
                        $scope.ctrl.emittenti = [];
                    });
                };
                $scope.search = function (id) {
                    $http({
                        method: 'POST',
                        url: 'rest/user/userByBroadcastId',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        data: {
                            id: $scope.ctrl.selectedBroadcaster.id,
                            nome: $scope.ctrl.selectedBroadcaster.nome,
                            tipo_broadcaster: $scope.ctrl.selectedBroadcaster.tipo_broadcaster
                        }
                    }).then(function successCallback(response) {
                        $scope.ctrl.utenti = response.data;
                        $scope.ctrl.utenti.unshift({
                            "id": -1,
                            "username": "TUTTI",
                            "email": "",
                            "password": "",
                            "bdcBroadcasters": {
                                "id": -1,
                                "nome": "",
                                "tipo_broadcaster": ""
                            },
                            "bdcRuoli": {
                                "id": -1,
                                "ruolo": ""
                            }
                        });
                        for (i = 0; i < $scope.ctrl.utenti.length; i++) {
                            if ($scope.ctrl.utenti[i].id == news.idUtente) {
                                $scope.ctrl.selectedUser = $scope.ctrl.utenti[i];
                            }
                        }
                        if ($scope.ctrl.selectedUser == undefined) {
                            $scope.ctrl.selectedUser = {
                                "id": -1,
                                "username": "TUTTI",
                                "email": "",
                                "password": "",
                                "bdcBroadcasters": {
                                    "id": -1,
                                    "nome": "",
                                    "tipo_broadcaster": ""
                                },
                                "bdcRuoli": {
                                    "id": -1,
                                    "ruolo": ""
                                }
                            }

                        }
                    }, function errorCallback(response) {
                        $scope.ctrl.utenti = [];
                    });
                };


                $scope.showAlert = function (title, message) {
                    ngDialog.open({
                        template: 'pages/advance-payment/dialog-alert.html',
                        plain: false,
                        width: '50%',
                        controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
                            var ngDialogId = $scope.ngDialogId;
                            $scope.ctrl = {
                                title: title,
                                message: message,
                                ngDialogId: ngDialogId
                            };
                            $scope.cancel = function (ngDialogId) {
                                ngDialog.close(ngDialogId);
                            };
                        }]
                    }).closePromise.then(function (data) {
                        if (data.value === true) {

                        }
                    });
                };

                $scope.cancel = function () {
                    ngDialog.closeAll(false);
                };

                $scope.formatDate = function (dateString) {
                    var date = new Date(dateString);
                    var separator = "-";
                    var month = date.getMonth() + 1;
                    return (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + separator + (month < 10 ? "0" + month : month)
                        + separator + date.getFullYear()
                };
                for (i = 0; i < $scope.ctrl.tipiEmittenti.length; i++) {
                    if (news.bdcBroadcasters != null && news.bdcBroadcasters != undefined && $scope.ctrl.tipiEmittenti[i].nome == news.bdcBroadcasters.tipo_broadcaster) {
                        $scope.ctrl.selectedTipoBroadcaster = $scope.ctrl.tipiEmittenti[i];
                        $scope.getEmittenti($scope.ctrl.selectedTipoBroadcaster.nome);
                    }
                }
                // MODIFICARE I PARAMETRI
                $scope.save = function (username) {
                    var user = username;
                    var broadcaster;
                    var idUtente;
                    var dateValidTo;
                    var dateValidFrom;
                    if ($scope.ctrl.selectedBroadcaster && $scope.ctrl.selectedBroadcaster.id >= 0) {
                        broadcaster = $scope.ctrl.selectedBroadcaster;
                    }
                    if ($scope.ctrl.selectedUser && $scope.ctrl.selectedUser.id >= 0) {
                        idUtente = $scope.ctrl.selectedUser.id;
                    }

                    if ($scope.ctrl.selectedDateTo) {
                        var numbers = $scope.ctrl.selectedDateTo.match(/\d+/g);
                        dateValidTo = new Date(numbers[2], numbers[1] - 1, numbers[0]);
                    }
                    if ($scope.ctrl.selectedDateFrom) {
                        var numbers = $scope.ctrl.selectedDateFrom.match(/\d+/g);
                        dateValidFrom = new Date(numbers[2], numbers[1] - 1, numbers[0]);
                    }
                    $http({
                        method: 'POST',
                        url: 'rest/news/addNews',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        data: {
                            id: $scope.ctrl.id,
                            title: $scope.ctrl.selectedTitle,
                            news: $scope.ctrl.selectedText,
                            validTo: dateValidTo,
                            validFrom: dateValidFrom,
                            creator: user,
                            idUtente: idUtente,
                            bdcBroadcasters: broadcaster
                        }
                    }).then(function successCallback(response) {
                        ngDialog.closeAll(false);
                        $scope.ctrl.rScope.onRefresh();
                        $scope.showAlert("Esito", "News Duplicata Correttamente");
                    }, function errorCallback(response) {
                        if (response.status == 409) {
                            $scope.showAlert("Errore", "Configurazione già presente.");
                        } else {
                            if(response.data)
                                $scope.showAlert("Errore", response.data);
                        }

                    });


                };
            }]
        }).closePromise.then(function (data) {
            if (data.value === true) {
                // no op
            }
        });


    };
    $scope.onRefresh();

    showAlert = function (title, message) {
        ngDialog.open({
            template: 'pages/advance-payment/dialog-alert.html',
            plain: false,
            width: '50%',
            controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
                var ngDialogId = $scope.ngDialogId;
                $scope.ctrl = {
                    title: title,
                    message: message,
                    ngDialogId: ngDialogId
                };
                $scope.cancel = function (ngDialogId) {
                    ngDialog.close(ngDialogId);
                };
            }]
        }).closePromise.then(function (data) {
            if (data.value === true) {

            }
        });
    };


    $scope.showConfirmAlert = function (news) {
        var rootScope = $scope;
        ngDialog.open({
            template: 'pages/dialog/dialog-confirm-alert.html',
            plain: false,
            width: '50%',
            controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
                var ngDialogId = $scope.ngDialogId;
                $scope.ctrl = {
                    title: "Info",
                    message: "Sei sicuro di voler disattivare la news?",
                    ngDialogId: ngDialogId,
                    item: news,
                    rScope: rootScope
                };
                $scope.cancel = function (ngDialogId) {
                    ngDialog.close(ngDialogId);
                };
                $scope.confirm = function (ngDialogId) {
                    $scope.ctrl.rScope.deactivationNews($scope.ctrl.item);
                    ngDialog.close(ngDialogId);
                };
            }]
        }).closePromise.then(function (data) {
            if (data.value === true) {

            }
        });
    };


    $scope.$on('filterApply', function (event, parameters) {
        $scope.ctrl.filterParameters = parameters;
        $scope.getPagedResults(0,
            $scope.ctrl.maxrows,
            parameters.dsp,
            parameters.invoiceNumber,
            parameters.monthFrom,
            parameters.yearFrom,
            parameters.monthTo,
            parameters.yearTo
        );
    });
}]);