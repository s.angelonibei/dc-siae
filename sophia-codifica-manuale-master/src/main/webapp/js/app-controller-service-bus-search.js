//(function(){
	
	/**
	 * ServiceBusSearchCtrl
	 * 
	 * @path /serviceBusSearch
	 */
	codmanApp.controller('ServiceBusSearchCtrl', ['$scope', 'ngDialog', '$routeParams', '$location', '$http', 'configService',
	                                              function($scope, ngDialog, $routeParams, $location, $http, configService) {

		yyyymmdd = function(date, sep) {
			if (date) {
				var dt = new Date(date);
				var mm = dt.getMonth() + 1;
				var dd = dt.getDate();
				return [dt.getFullYear(),
				        (mm>9 ? '' : '0') + mm,
				        (dd>9 ? '' : '0') + dd
				        ].join(sep);
			}
		};
		
		$scope.ctrl = {
			navbarTab: 'serviceBusSearch',
			execute: true, // $routeParams.execute,
			hideForm: $routeParams.hideForm,
			maxrows: configService.maxRowsPerPage,
			first: $routeParams.first||0,
			last: $routeParams.last||configService.maxRowsPerPage,
			insertTime: yyyymmdd($routeParams.insertTime, '-'),
			queueName: $routeParams.queueName,
			queueType: $routeParams.queueType,
			serviceName: $routeParams.serviceName,
			sender: $routeParams.sender,
			hostname: $routeParams.hostname,
			idDsp: $routeParams.idDsp,
			idDsr: $routeParams.idDsr,
			year: $routeParams.year,
			month: $routeParams.month,
			country: $routeParams.country,
			json: $routeParams.json,
			sortType: 'insertTime',
			sortReverse: true,
			searchResults: {},
			decode: {
				queueNames: [],
				idDsrs: [],
				idDsps: {},
				queueTypes: [],
				serviceNames: [],
				months: [
					{key:'01', value:'Gennaio'},
					{key:'02', value:'Febbraio'},
					{key:'03', value:'Marzo'},
					{key:'04', value:'Aprile'},
					{key:'05', value:'Maggio'},
					{key:'06', value:'Giugno'},
					{key:'07', value:'Luglio'},
					{key:'08', value:'Agosto'},
					{key:'09', value:'Settembre'},
					{key:'10', value:'Ottobre'},
					{key:'11', value:'Novembre'},
					{key:'12', value:'Dicembre'},
					{key:'Q1', value:'1° Trimestre'},
					{key:'Q2', value:'2° Trimestre'},
					{key:'Q3', value:'3° Trimestre'},
					{key:'Q4', value:'4° Trimestre'}
				],
				month: {
					'01':'Gennaio', '02':'Febbraio', '03':'Marzo', '04':'Aprile',
					'05':'Maggio', '06':'Giugno', '07':'Luglio', '08':'Agosto',
					'09':'Settembre', '10':'Ottobre', '11':'Novembre', '12':'Dicembre',
					'Q1':'1° Trimestre', 'Q2':'2° Trimestre', 'Q3':'3° Trimestre', 'Q4':'4° Trimestre'
				},
				years: ['2014', '2015', '2016', '2017', '2018', '2019', '2020'],
				countries: {}
			}
		};
		
		$scope.selectIdDsr = function(ref) {
			if (ref) {
				$scope.ctrl.idDsr = ref.originalObject.name;
			} else {
				$scope.ctrl.idDsr = null;
			}
			$scope.onRicerca();
		};

		$scope.selectCountry = function(ref) {
			if (ref) {
				$scope.ctrl.country = ref.originalObject.idCountry;
			} else {
				$scope.ctrl.country = null;
			}
			$scope.onRicerca();
		};
		
		$scope.selectHostname = function(ref) {
			if (ref) {
				$scope.ctrl.hostname = ref.originalObject.name;
			} else {
				$scope.ctrl.hostname = null;
			}
			$scope.onRicerca();
		};

		$scope.onAzzera = function() {
			$scope.ctrl.execute = true; // false;
			$scope.ctrl.first = 0;
			$scope.ctrl.last = $scope.ctrl.maxrows;
			$scope.ctrl.insertTime = null;
			$scope.ctrl.queueName = null;
			$scope.ctrl.queueType = null;
			$scope.ctrl.serviceName = null;
			$scope.ctrl.sender = null;
			$scope.ctrl.hostname = null;
			$scope.ctrl.idDsp = null;
			$scope.ctrl.idDsr = null;
			$scope.ctrl.year = null;
			$scope.ctrl.month = null;
			$scope.ctrl.country = null;
			$scope.ctrl.json = null;
			$scope.$broadcast('angucomplete-alt:clearInput');
		};

		$scope.setSortType = function(sortType) {
			$scope.ctrl.sortType = sortType;
			$scope.ctrl.sortReverse = !$scope.ctrl.sortReverse;
			$scope.onRicerca();
		};
		
		$scope.onRicerca = function() {
			$scope.onRicercaEx(0, $scope.ctrl.maxrows);
		};
		
		$scope.onRicercaEx = function(first, last) {
			$http({
				method: 'GET',
				url: 'rest/service-bus/search',
				params: {
					first: first,
					last: last,
					sortType: $scope.ctrl.sortType,
					sortReverse: $scope.ctrl.sortReverse,
					insertTime: $scope.ctrl.insertTime,
					queueName: $scope.ctrl.queueName,
					queueType: $scope.ctrl.queueType,
					serviceName: $scope.ctrl.serviceName,
					sender: $scope.ctrl.sender,
					hostname: $scope.ctrl.hostname,
					idDsp: $scope.ctrl.idDsp,
					idDsr: $scope.ctrl.idDsr,
					year: $scope.ctrl.year,
					month: $scope.ctrl.month,
					country: $scope.ctrl.country,
					json: $scope.ctrl.json
				}
			}).then(function successCallback(response) {
				$scope.ctrl.searchResults = response.data;
				$scope.ctrl.first = response.data.first;
				$scope.ctrl.last = response.data.last;
				// string to JSON object
				if ($scope.ctrl.searchResults.rows) {
					for (i = 0, len = $scope.ctrl.searchResults.rows.length; i < len; i ++) {
						var row = $scope.ctrl.searchResults.rows[i];
						try {
							row.json = JSON.parse(row.messageJson);
							row.jsonError = null;
						} catch(err) {
							row.json = null;
							row.jsonError = err.name + ': ' + err.message;
						}
					}
				}
			}, function errorCallback(response) {
				$scope.ctrl.searchResults = {};
			});
		};
		
		$scope.navigateToNextPage = function() {
			$location
				.search('execute', 'true')
				.search('hideForm', $scope.ctrl.hideForm)
				.search('first', $scope.ctrl.last)
				.search('last', $scope.ctrl.last + $scope.ctrl.maxrows)
				.search('sortType', $scope.ctrl.sortType||null)
				.search('sortReverse', $scope.ctrl.sortReverse||null)
				.search('insertTime', $scope.ctrl.insertTime||null)
				.search('queueName', $scope.ctrl.queueName||null)
				.search('queueType', $scope.ctrl.queueType||null)
				.search('serviceName', $scope.ctrl.serviceName||null)
				.search('sender', $scope.ctrl.sender||null)
				.search('hostname', $scope.ctrl.hostname||null)
				.search('idDsp', $scope.ctrl.idDsp||null)
				.search('idDsr', $scope.ctrl.idDsr||null)
				.search('year', $scope.ctrl.year||null)
				.search('month', $scope.ctrl.month||null)
				.search('country', $scope.ctrl.country||null)
				.search('json', $scope.ctrl.json||null);
		};

		$scope.navigateToPreviousPage = function() {
			$location
				.search('execute', 'true')
				.search('hideForm', $scope.ctrl.hideForm)
				.search('first', Math.max($scope.ctrl.first - $scope.ctrl.maxrows, 0))
				.search('last', $scope.ctrl.first)
				.search('sortType', $scope.ctrl.sortType||null)
				.search('sortReverse', $scope.ctrl.sortReverse||null)
				.search('insertTime', $scope.ctrl.insertTime||null)
				.search('queueName', $scope.ctrl.queueName||null)
				.search('queueType', $scope.ctrl.queueType||null)
				.search('serviceName', $scope.ctrl.serviceName||null)
				.search('sender', $scope.ctrl.sender||null)
				.search('hostname', $scope.ctrl.hostname||null)
				.search('idDsp', $scope.ctrl.idDsp||null)
				.search('idDsr', $scope.ctrl.idDsr||null)
				.search('year', $scope.ctrl.year||null)
				.search('month', $scope.ctrl.month||null)
				.search('country', $scope.ctrl.country||null)
				.search('json', $scope.ctrl.json||null);
		};
		
		$scope.showAlert = function(title, message) {
			ngDialog.open({
				template: 'pages/service-bus/dialog-alert.html',
				plain: false,
				width: '50%',
				controller: ['$scope', 'ngDialog', function($scope, ngDialog) {
					$scope.ctrl = {
						title: title,
						message: message
					};
					$scope.cancel = function() {
						ngDialog.closeAll(false);
					};
					$scope.save = function() {
						ngDialog.closeAll(true);
					};
				}]
			}).closePromise.then(function (data) {
				if (data.value === true) {
					
				}
			});
		};
		
		$scope.showDetails = function(event, serviceBus) {
			decode = $scope.ctrl.decode;
			ngDialog.open({
				template: 'pages/service-bus/dialog-details.html',
				plain: false,
				width: '70%',
				controller: ['$scope', 'ngDialog', function($scope, ngDialog) {
					$scope.ctrl = {
						serviceBus: serviceBus,
						decode: decode
					};
					$scope.cancel = function() {
						ngDialog.closeAll(false);
					};
					$scope.save = function() {
						ngDialog.closeAll(true);
					};
				}]
			}).closePromise.then(function (data) {
				if (data.value === true) {
					
				}
			});
		};
		
		$scope.showSendMessage = function(event, serviceBus) {
			ngDialog.open({
				template: 'pages/service-bus/dialog-send-message.html',
				plain: false,
				width: '70%',
				controller: ['$scope', 'ngDialog', '$http', function($scope, ngDialog, $http) {
					$scope.ctrl = {
						serviceBus: serviceBus,
						force: false
					};
					$scope.cancel = function() {
						ngDialog.closeAll(false);
					};
					$scope.save = function() {
						$http({
							method: 'POST',
							url: 'rest/service-bus/' + ($scope.ctrl.force ? 'force-send' : 'send-again'),
							data: $scope.ctrl.serviceBus
						}).then(function successCallback(response) {
							ngDialog.closeAll(true);
						}, function errorCallback(response) {
							ngDialog.closeAll(false);
						});
					};
				}]
			}).closePromise.then(function (data) {
				if (data.value === true) {
					$scope.showAlert('Messaggio inviato', 'Messaggio inviato correttamente');
				}
			});
		};
		
		$http({
			method: 'GET',
			url: 'rest/service-bus/queue-names'
		}).then(function successCallback(response) {
			$scope.ctrl.decode.queueNames = response.data;
		}, function errorCallback(response) {
			$scope.ctrl.decode.queueNames = [];
		});

		$http({
			method: 'GET',
			url: 'rest/service-bus/queue-types'
		}).then(function successCallback(response) {
			$scope.ctrl.decode.queueTypes = response.data;
		}, function errorCallback(response) {
			$scope.ctrl.decode.queueTypes = [];
		});

//		$http({
//			method: 'GET',
//			url: 'rest/service-bus/iddsrs'
//		}).then(function successCallback(response) {
//			$scope.ctrl.decode.idDsrs = response.data;
//		}, function errorCallback(response) {
//			$scope.ctrl.decode.idDsrs = [];
//		});

		$http({
			method: 'GET',
			url: 'rest/service-bus/iddsps'
		}).then(function successCallback(response) {
			$scope.ctrl.decode.idDsps = response.data;
		}, function errorCallback(response) {
			$scope.ctrl.decode.idDsps = {};
		});
		
		$http({
			method: 'GET',
			url: 'rest/service-bus/service-names'
		}).then(function successCallback(response) {
			$scope.ctrl.decode.serviceNames = response.data;
		}, function errorCallback(response) {
			$scope.ctrl.decode.serviceNames = [];
		});

		$http({
			method: 'GET',
			url: 'rest/anagCountry'
		}).then(function successCallback(response) {
			$scope.ctrl.decode.countries = response.data;
		}, function errorCallback(response) {
			$scope.ctrl.decode.countries = {};
		});

		if ($scope.ctrl.execute) {
			$scope.onRicercaEx($scope.ctrl.first, $scope.ctrl.last);
		}

	}]);
	
//})();

