/**
 * PerformingExecutionHistoryCtrl
 * 
 * @path /news
 */
codmanApp.controller('PerformingExecutionHistoryCtrl', [ '$scope', '$route', 'ngDialog', '$routeParams', '$location', '$http', function($scope, $route, ngDialog, $routeParams, $location, $http) {


	$scope.navigateToNextPage = function() {
		$scope.ctrl.page=$scope.ctrl.page+1;
		$scope.getExecution();

	};

	$scope.navigateToPreviousPage = function() {
		$scope.ctrl.page=$scope.ctrl.page-1;
		$scope.getExecution();

	};

	$scope.ctrl = {
		navbarTab : 'executionHistory',
		execute : $routeParams.execute,
		filteredPeriodFrom : {},
		filteredPeriodTo : {},
		annoInizioPeriodoContabile : "",
		meseInizioPeriodoContabile : "",
		annoFinePeriodoContabile : "",
		meseFinePeriodoContabile : "",
		page : 0
	};

	// Funzione principale richiamata al caricamento della pagina
	$scope.onRefresh = function() {
		
	};


	$scope.formatContability = function(dateString) {
		//		var year = dateString.substr(0, 4);
		//		var month = dateString.substr(4, 6);
		//		return month+"/"+year;
		return dateString;
	}

	$scope.formatDate = function(dateString) {
		var date = new Date(dateString)
		var separator = "-"
		var month = date.getMonth() + 1
		return (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + separator + (month < 10 ? "0" + month : month)
			+ separator + date.getFullYear()
	}

	$scope.filterApply = function() {
		$scope.ctrl.first = 0;
		$scope.ctrl.last = 50;
		$scope.ctrl.page = 0;
		$scope.ctrl.execution = {};
		$scope.getExecution();
	}

	$scope.getExecution = function() {
		var periodFrom = 0;
	    var yearFrom = 0;
        var periodTo = 0;
        var yearTo = 0;

	    if(($scope.ctrl.selectedContabilityDateFrom !== undefined)){
            var from = $scope.ctrl.selectedContabilityDateFrom.split("-");
            periodFrom = ""+parseInt(from[0],10);
            yearFrom = ""+parseInt(from[1],10);
	    }

	    if($scope.ctrl.selectedContabilityDateTo !== undefined){
            var to = $scope.ctrl.selectedContabilityDateTo.split("-");
            periodTo =""+ parseInt(to[0],10);
            yearTo = ""+parseInt(to[1],10);

	    }
		$http({
			method : 'GET',
			url : 'rest/performing/campionamento/campionamentoStoricoEsecuzioniLista',
			params : {
				annoInizioPeriodoContabile : yearFrom,
				meseInizioPeriodoContabile : periodFrom,
				annoFinePeriodoContabile : yearTo,
				meseFinePeriodoContabile : periodTo,
				page : $scope.ctrl.page
			}
		}).then(function successCallback(response) {
			$scope.ctrl.totRecords=response.data.totRecords;
			$scope.ctrl.page=response.data.currentPage;
			$scope.ctrl.first = response.data.currentPage*50;
			$scope.ctrl.last = response.data.records.length;
			$scope.ctrl.pageTot=Math.floor(response.data.totRecords/50);
			$scope.ctrl.execution = response.data.records;

		}, function errorCallback(response) {
			$scope.ctrl.execution = [];
		});


	};

	$scope.onRefresh();

	showAlert = function(title, message) {
		ngDialog.open({
			template : 'pages/advance-payment/dialog-alert.html',
			plain : false,
			width : '50%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				var ngDialogId = $scope.ngDialogId;
				$scope.ctrl = {
					title : title,
					message : message,
					ngDialogId : ngDialogId
				};
				$scope.cancel = function(ngDialogId) {
					ngDialog.close(ngDialogId);
				};
			} ]
		}).closePromise.then(function(data) {
			if (data.value === true) {

			}
		});
	};

} ]);