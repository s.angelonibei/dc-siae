/**
 * PerformingConfigSamplingCtrl
 * 
 * @path /configSave
 */
codmanApp.controller('PerformingConfigSamplingCtrl', [ '$scope', '$route', 'ngDialog', '$routeParams', '$location', '$http', function($scope, $route, ngDialog, $routeParams, $location, $http) {


	$scope.ctrl = {
		navbarTab : 'campionamentoConfigurazione',
		execute : $routeParams.execute
	};

	// Funzione principale richiamata al caricamento della pagina
	$scope.onRefresh = function() {
		
	};

	$scope.formatDate = function(dateString) {
		var date = new Date(dateString)
		var separator = "-"
		var month = date.getMonth() + 1
		return (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + separator + (month < 10 ? "0" + month : month)
			+ separator + date.getFullYear()
	}

	$scope.checkStatusButton= function(){
		var estrattiRoma = $scope.ctrl.bsm1 + ";" + $scope.ctrl.bsm2 + ";" + $scope.ctrl.bsm3 + ";" + $scope.ctrl.bsm4 + ";" + $scope.ctrl.bsm5 + ";"
		var arrayRoma = estrattiRoma.split(";",5);
		var estrattiMilano = $scope.ctrl.concertini1 + ";" + $scope.ctrl.concertini2 + ";" + $scope.ctrl.concertini3 + ";" + $scope.ctrl.concertini4 + ";" + $scope.ctrl.concertini5
		var arrayMilano = estrattiMilano.split(";",5);
		var mesePeriodoContabilita;
		var annoPeriodoContabilita;
		var dataInizioLavorazione;
		var dataEstrazioneLotto;

		$scope.formalError = false;
		
		if( ($scope.ctrl.selectedContabilityDate !== undefined) ) {
			var from = $scope.ctrl.selectedContabilityDate.split("-");
			mesePeriodoContabilita = "" + parseInt(from[0], 10);
			annoPeriodoContabilita = "" + parseInt(from[1], 10);
		}

		if ($scope.ctrl.selectedWorkingDate !== undefined) {
			var a = $scope.ctrl.selectedWorkingDate.split("-");
			dataInizioLavorazione = a.join('/');

		}
		if ($scope.ctrl.selectedDateTo !== undefined) {
			var a = $scope.ctrl.selectedDateTo.split("-");
			dataEstrazioneLotto = a.join('/');

		}
		
		if($scope.ctrl.concertini1  == $scope.ctrl.concertini2  || $scope.ctrl.concertini1  == $scope.ctrl.concertini3  || $scope.ctrl.concertini1  == $scope.ctrl.concertini4  || $scope.ctrl.concertini1  == $scope.ctrl.concertini5  ||
			$scope.ctrl.concertini2  == $scope.ctrl.concertini3  || $scope.ctrl.concertini2  == $scope.ctrl.concertini4  || $scope.ctrl.concertini2  == $scope.ctrl.concertini5  ||
			$scope.ctrl.concertini3  == $scope.ctrl.concertini4  || $scope.ctrl.concertini3  == $scope.ctrl.concertini5  ||
			$scope.ctrl.concertini4  == $scope.ctrl.concertini5 ){
			 $scope.formalError = true;
		}
		
		if($scope.ctrl.bsm1  == $scope.ctrl.bsm2  || $scope.ctrl.bsm1  == $scope.ctrl.bsm3  || $scope.ctrl.bsm1  == $scope.ctrl.bsm4  || $scope.ctrl.bsm1  == $scope.ctrl.bsm5  ||
				$scope.ctrl.bsm2  == $scope.ctrl.bsm3  || $scope.ctrl.bsm2  == $scope.ctrl.bsm4  || $scope.ctrl.bsm2  == $scope.ctrl.bsm5  ||
				$scope.ctrl.bsm3  == $scope.ctrl.bsm4  || $scope.ctrl.bsm3  == $scope.ctrl.bsm5  ||
				$scope.ctrl.bsm4  == $scope.ctrl.bsm5 ){
				 $scope.formalError = true;
		}
		
		angular.forEach(arrayRoma,function(num){
			 if (num=="undefined"||num<1 || num>90){
				 $scope.formalError = true;
			 }
		})

			 
		angular.forEach(arrayMilano,function(num){
			 if (num=="undefined"||num<1 || num>90){
				 $scope.formalError = true;
			 }
		})
		if(mesePeriodoContabilita==undefined){
			 $scope.formalError = true;
		}
		 
		if(annoPeriodoContabilita==undefined){
			 $scope.formalError = true;
		}
		if(dataInizioLavorazione==undefined){
			 $scope.formalError = true;
		}
		
		if(dataEstrazioneLotto==undefined){
			 $scope.formalError = true;
		}

		return $scope.formalError;
			 
		
	}
	
	$scope.saveConfiguration = function(item) {
		var estrattiRoma = $scope.ctrl.bsm1 + ";" + $scope.ctrl.bsm2 + ";" + $scope.ctrl.bsm3 + ";" + $scope.ctrl.bsm4 + ";" + $scope.ctrl.bsm5 + ";"
		var estrattiMilano = $scope.ctrl.concertini1 + ";" + $scope.ctrl.concertini2 + ";" + $scope.ctrl.concertini3 + ";" + $scope.ctrl.concertini4 + ";" + $scope.ctrl.concertini5
		var mesePeriodoContabilita;
		var annoPeriodoContabilita;
		var dataInizioLavorazione;
		var dataEstrazioneLotto;

		if( ($scope.ctrl.selectedContabilityDate !== undefined) ) {
			var from = $scope.ctrl.selectedContabilityDate.split("-");
			mesePeriodoContabilita = "" + parseInt(from[0], 10);
			annoPeriodoContabilita = "" + parseInt(from[1], 10);
		}

		if ($scope.ctrl.selectedWorkingDate !== undefined) {
			var a = $scope.ctrl.selectedWorkingDate.split("-");
			dataInizioLavorazione = a.join('/');

		}
		if ($scope.ctrl.selectedDateTo !== undefined) {
			var a = $scope.ctrl.selectedDateTo.split("-");
			dataEstrazioneLotto = a.join('/');

		}
		
		var resto5bsm=document.getElementById('resto5bsmId').value
		var resto5concertini=document.getElementById('resto5concertiniId').value
		var resto61bsm=document.getElementById('resto61bsmId').value
		var resto61concertini=document.getElementById('resto61concertiniId').value
		var username=item;
		$http({
			method : 'POST',
			url : 'rest/performing/campionamento/campionamentoConfigurazioneSave',
			headers : {
				'Content-Type' : 'application/json'
			},
			data : {
				mesePeriodoContabilita : mesePeriodoContabilita,
				annoPeriodoContabilita : annoPeriodoContabilita,
				dataInizioLavorazione : dataInizioLavorazione,
				dataEstrazioneLotto : dataEstrazioneLotto,
				estrazioniRoma : estrattiRoma,
				estrazioniMilano : estrattiMilano,
				restoRoma1 : resto5bsm,
				restoMilano1 : resto5concertini,
				restoRoma2 : resto61bsm,
				restoMilano2 : resto61concertini,
				username : username
			}
		}).then(function successCallback(response) {
			ngDialog.closeAll(false);
			$scope.showAlert("Esito", "Configurazione Aggiunta Correttamente");
		}, function errorCallback(response) {
			if (response.status == 409) {
				$scope.showAlert("Errore", "Configurazione già presente.");
			} else {
				$scope.showAlert("Errore", response.statusText);
			}

		});
	}



	$scope.onRefresh();

	$scope.showAlert = function(title, message) {
		ngDialog.open({
			template : 'pages/advance-payment/dialog-alert.html',
			plain : false,
			width : '50%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				var ngDialogId = $scope.ngDialogId;
				$scope.ctrl = {
					title : title,
					message : message,
					ngDialogId : ngDialogId
				};
				$scope.cancel = function(ngDialogId) {
					ngDialog.close(ngDialogId);
				};
			} ]
		}).closePromise.then(function(data) {
			if (data.value === true) {

			}
		});
	};

} ]);