/**
 * AnagDspCtrl
 * 
 * @path /anagDsp
 */
codmanApp.controller('AnagDspCtrl', ['$scope','$route', 'ngDialog', '$routeParams', '$location', '$http', function($scope,$route, ngDialog, $routeParams, $location, $http) {

	$scope.ctrl = {
		navbarTab: 'anagDspConfig',
		dspConfig: [],
		filterParameters: [],
		execute: $routeParams.execute,
		hideForm: $routeParams.hideForm,
		maxrows: 20,
		first: $routeParams.first||0,
		last: $routeParams.last||20,
		sortType: 'priority',
		sortReverse: true,
		filter: $routeParams.filter,
		selectedDsp : $routeParams.dsp||'*',
		selectedUtilization : $routeParams.utilization||'*',
		selectedOffer : $routeParams.offer||'*'
	};		
			
	$scope.onAzzera = function() {
		$scope.ctrl.execute = false;
		$scope.ctrl.first = 0;
		$scope.ctrl.last = $scope.ctrl.maxrows;
		};
		
	$scope.navigateToNextPage = function() {
		$location.search('execute', 'true')
		.search('hideForm', $scope.ctrl.hideForm)
		.search('first', $scope.ctrl.last)
		.search('last', $scope.ctrl.last + $scope.ctrl.maxrows);
	};
	
	$scope.navigateToPreviousPage = function() {
		$location.search('execute', 'true')
			.search('hideForm', $scope.ctrl.hideForm)
			.search('first', Math.max($scope.ctrl.first - $scope.ctrl.maxrows, 0))
			.search('last', $scope.ctrl.first);
	};
	

	// Funzione principale richiamata al caricamento della pagina
	$scope.onRefresh = function() {
		// get All DSP Configuration


		if ($scope.ctrl.execute) {
			$scope.getPagedResults($scope.ctrl.first,
					$scope.ctrl.last /*, // da abilitare con i corretti parametri in caso di filtro		
					$scope.ctrl.selectedDsp,
					$scope.ctrl.selectedUtilization,
					$scope.ctrl.selectedOffer*/);
		} else {
			$scope.getPagedResults(0,
					$scope.ctrl.maxrows);
		}
		

	
	};
	
	
//	$scope.getPagedResults = function(first, last, dsp, utilization, offer) { da abilitare con i corretti parametri in caso di filtro
		$scope.getPagedResults = function(first, last) {
		$http({
			method: 'GET',
			url: 'rest/anagDsp/all',
			params: {
				first: first,
				last: last /*, da abilitare con i corretti parametri in caso di filtro
				dsp : (typeof dsp !== 'undefined' ? dsp : ""),
				utilization : (typeof utilization !== 'undefined' ? utilization : ""),
				offer : (typeof offer !== 'undefined' ? offer : "") */
			}
		}).then(function successCallback(response) {
			$scope.ctrl.dspConfig = response.data;
			$scope.ctrl.first = response.data.first;
			$scope.ctrl.last = response.data.last;
		}, function errorCallback(response) {
			$scope.ctrl.dspConfig = [];
		});

	};
	

	
	//funzione che renderizza il popup per l'aggiunta di una nuova configurazione 
	$scope.showNewConfig = function(event) {
		ngDialog.open({
			template: 'pages/anag-dsp-config/dialog-new.html',
			plain: false,
			width: '60%',
			controller: ['$scope', 'ngDialog', '$http', '$q', function($scope, ngDialog, $http, $q) {
				$scope.ctrl = {
						
				};
				
				
//				decommentare e chiamare i necessari servizi per la valorizzatione di eventuali menu a tendina				
//				var promise1 = $http({method: 'GET', url: 'rest/dspConfig/allAvailableOffers'}); 
//				var promise2 = $http({method: 'GET', url: 'rest/data/dspUtilizations'}); 
//				$q.all([promise1, promise2]).then(function successCallback(response) {						
//					$scope.ctrl.commercialOffersList = response[0].data;
//					$scope.ctrl.commercialOffersList.splice(0, 0, {idCommercialOffering: '*', offering:'*'});
//					$scope.ctrl.dspList =  response[1].data.dsp; 
//					$scope.ctrl.utilizationList = response[1].data.utilizations;
//					$scope.ctrl.utilizationList.splice(0,0,{name: '*', idUtilizationType: '*' });
//					}, function errorCallback(response) {
//						$scope.ctrl.dspList = [ ];
//						$scope.ctrl.utilizationList = [ ];
//						$scope.ctrl.commercialOffersList = [ ];
//					}); 
//
				  
				
			    $scope.openErrorDialog = function (errorMsg) {
			        ngDialog.open({ 
			        	template: 'errorDialogId',
			        	plain: false,
						width: '40%',
			        	data: errorMsg
			        	
			        });
			    };
				
				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				
							
				$scope.save = function() {
					$http({
						method: 'POST',
						url: 'rest/anagDsp',
						data: {
							idDsp: $scope.idDsp,
							code: $scope.code,
							name: $scope.name,
							description: $scope.description,
							ftpSourcePath: $scope.ftpSourcePath
							
						}
					}).then(function successCallback(response) {
						ngDialog.closeAll(true);
						$route.reload();
					}, function errorCallback(response) {
						if(response.status == 409){
							$scope.openErrorDialog("Configurazione già presente. Modifichi i valori direttamente dalla tabella");	
						}else{
							$scope.openErrorDialog(response.statusText);	
						}
						
					});
				};
				
				
				
				
			}]
		}).closePromise.then(function (data) {
			if (data.value === true) {
				// no op
			}
		});
	};
	
	
	$scope.showDeleteConfig = function(event, configToRemove) {
		ngDialog.open({
			template: 'pages/anag-dsp-config/dialog-delete.html',
			plain: false,
			width: '60%',
			controller: ['$scope', 'ngDialog', '$http', function($scope, ngDialog, $http) {
				$scope.ctrl = {
					configToRemove: configToRemove
				};
				

				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				

				$scope.deleteConfig = function() {
					$http({
						method: 'DELETE',
						url: 'rest/anagDsp',
						headers: {
				            'Content-Type': 'application/json'},
						data: {
							idDsp: $scope.ctrl.configToRemove.idDsp,
							code: $scope.ctrl.configToRemove.code,
							name: $scope.ctrl.configToRemove.name,
							description: $scope.ctrl.configToRemove.description,
							ftpSourcePath: $scope.ctrl.configToRemove.ftpSourcePath
						}
					
					}).then(function successCallback(response) {
						ngDialog.closeAll(true);
						$route.reload();
					}, function errorCallback(response) {
						$scope.openErrorDialog(response.statusText);
					});
					
				};
				
			    $scope.openErrorDialog = function (errorMsg) {
			        ngDialog.open({ 
			        	template: 'errorDialogId',
			        	plain: false,
						width: '40%',
			        	data: errorMsg
			        	
			        });
			    };
			}]
		}).closePromise.then(function (data) {
			if (data.value === true) {
				// no op
			}
		});
	};
	
		
	
	$scope.showEditConfig = function(event, configToModify) {
		ngDialog.open({
			template: 'pages/anag-dsp-config/dialog-edit.html',
			plain: false,
			width: '60%',
			controller: ['$scope', 'ngDialog', '$http', function($scope, ngDialog, $http) {
				$scope.ctrl = {
					configToModify: configToModify
				};
				
				$scope.code = configToModify.code;
				$scope.name= configToModify.name
				$scope.description = configToModify.description
				$scope.ftpSourcePath = configToModify.ftpSourcePath

				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				
		
				$scope.editConfig = function() {
				
					$http({
						method: 'PUT',
						url: 'rest/anagDsp',
						headers: {
				            'Content-Type': 'application/json'},				
				            params: {
				            	
								prevcode: $scope.ctrl.configToModify.code,
								prevname: $scope.ctrl.configToModify.name,
								prevdescription: $scope.ctrl.configToModify.description,
								prevftpSourcePath: $scope.ctrl.configToModify.ftpSourcePath, 
				            	idDsp: $scope.ctrl.configToModify.idDsp,
								code: $scope.code,
								name: $scope.name,
								description: $scope.description,
								ftpSourcePath: $scope.ftpSourcePath
								
							}
						
					
					}).then(function successCallback(response) {
						ngDialog.closeAll(true);
						$route.reload();
					}, function errorCallback(response) {
						$scope.openErrorDialog(response.statusText);
					});
					
				};
				
			    $scope.openErrorDialog = function (errorMsg) {
			        ngDialog.open({ 
			        	template: 'errorDialogId',
			        	plain: false,
						width: '40%',
			        	data: errorMsg
			        	
			        });
			    };
			}]
		}).closePromise.then(function (data) {
			if (data.value === true) {
				// no op
			}
		});
	};
	
//	 $scope.$on('filterApply', function(event, parameters) { //decommentare in caso di utilizzo filtro assegnando i parametri adeguati
//		 $scope.ctrl.filterParameters = parameters;
//			$location.search('execute', 'true')
//			.search('hideForm', false)
//			.search('first', 0)
//			.search('last', $scope.ctrl.maxrows)
//			.search('dsp', parameters.dsp)
//			.search('utilization',parameters.utilization)
//			.search('offer', parameters.offer);	 
//			$scope.ctrl.hideForm = false;
//		
//		 });
	
	$scope.onRefresh();
	
}]);


