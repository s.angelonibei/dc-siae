//(function(){
	
	function logmsg(message) {
		if (window.console && console.log) {
	        console.log(message);
	    }		
	};
	
	function cloneObject(obj) {
		if (null == obj || "object" != typeof obj) return obj;
		var copy = obj.constructor();
		for (var attr in obj) {
			if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
		}
		return copy;
	};

	function dd_MM_yyyyDate(str) {
		if (!!str) {
			return new Date(str.substring(6,10), str.substring(3,5) - 1, str.substring(0,2));
		}
	}

	function dd_MM_yyyyString(date) {
		if (!!date) {
			date = new Date(date);
			var yyyy = date.getFullYear();
			var MM = date.getMonth() < 9 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1); // getMonth() is zero-based
			var dd  = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
			return "".concat(dd).concat("/").concat(MM).concat("/").concat(yyyy);
		}
	};

	function dd_MM_yyyy_HH_mmDate(str) {
		if (!!str) {
			var date = new Date(str.substring(6,10), str.substring(3,5) - 1, str.substring(0,2));
			date.setHours(str.substring(11,13), str.substring(14,16), 0, 0);
			return date;
		}
	}

	function dd_MM_yyyy_HH_mmString(date) {
		if (!!date) {
			date = new Date(date);
			var yyyy = date.getFullYear();
			var MM = date.getMonth() < 9 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1); // getMonth() is zero-based
			var dd  = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
			var hh = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
			var mm = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
			return "".concat(dd).concat("/").concat(MM).concat("/").concat(yyyy)
						.concat(" ").concat(hh).concat(":").concat(mm);
		}
	};

	function yyyyMMddDate(str) {
		if (!!str) {
			return new Date(str.substring(0,4), str.substring(4,6) - 1, str.substring(6,8));
		}
		return null;
	}

	function yyyyMMddHHmmssDate(str) {
		if (!!str) {
			var date = new Date(str.substring(0,4), str.substring(4,6) - 1, str.substring(6,8));
			date.setHours(str.substring(8,10), str.substring(10,12), str.substring(12,14), 0);
			return date;
		}
	}

	function yyyyMMddString(date) {
		if (!!date) {
			date = new Date(date);
			var yyyy = date.getFullYear();
			var MM = date.getMonth() < 9 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1); // getMonth() is zero-based
			var dd  = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
			return "".concat(yyyy).concat(MM).concat(dd);
		}
	};

	function yyyyMMddHHmmssString(date) {
		if (!!date) {
			date = new Date(date);
			var yyyy = date.getFullYear();
			var MM = date.getMonth() < 9 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1); // getMonth() is zero-based
			var dd  = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
			var hh = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
			var mm = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
			var ss = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
			return "".concat(yyyy).concat(MM).concat(dd).concat(hh).concat(mm).concat(ss);
		}
	};

	function decodeMonth(mese) {
		if (!!mese) {
			switch (Number(mese)) {
				case 1: return "Gennaio";
				case 2: return "Febbraio";
				case 3: return "Marzo";
				case 4: return "Aprile";
				case 5: return "Maggio";
				case 6: return "Giugno";
				case 7: return "Luglio";
				case 8: return "Agosto";
				case 9: return "Settembre";
				case 10: return "Ottobre";
				case 11: return "Novembre";
				case 12: return "Dicembre";
			}
		}
	};
	
	function stringToDateYYYY_MM_DD_hh_mm(str) {
	    if(!/^(\d){4}-(\d){2}-(\d){2} (\d){2}\:(\d){2}$/.test(str)) {
	    	return null;
	    }
		var date = new Date(str.substring(0,4), str.substring(5,7) - 1, str.substring(8,10));
		date.setHours(str.substring(11,13), str.substring(14,16), 0, 0);
		return date;
	};

	function elapsedTime(startTimeStr) {
		var startTime = new Date(startTimeStr);
		// later record end time
	    var endTime = new Date();
		// time difference in ms
	    var timeDiff = endTime - startTime;
		// strip the miliseconds
	    timeDiff /= 1000;
	    // get seconds
	    var seconds = Math.round(timeDiff % 60);
	    // remove seconds from the date
	    timeDiff = Math.floor(timeDiff / 60);
	    // get minutes
	    var minutes = Math.round(timeDiff % 60);
	    // remove minutes from the date
	    timeDiff = Math.floor(timeDiff / 60);
	    // get hours
	    var hours = Math.round(timeDiff % 24);
	    // remove hours from the date
	    timeDiff = Math.floor(timeDiff / 24);
	    // the rest of timeDiff is number of days
	    var days = timeDiff;
	    // format result
	    if (days > 0) {
		    return days + "d " + hours + "h " + minutes + "m " + seconds + "s";	    	
		} else if (hours > 0) {
		    return hours + "h " + minutes + "m " + seconds + "s";	    	
		} else if (minutes > 0) {
		    return minutes + "m " + seconds + "s";	    	
		} else if (seconds > 0) {
		    return seconds + "s";	    	
		} else {
		    return timeDiff + "ms";
	    }
	};
	
	function elapsedTimeMs(startTimeStr) {
		var startTime = new Date(startTimeStr);
		// later record end time
	    var endTime = new Date();
		// time difference in ms
	    var timeDiff = endTime - startTime;
	    return timeDiff;
	}

	 function twoDecimalRound (number){
		if(number !== undefined){
			
		return number.toLocaleString('it',{
				minimumFractionDigits: 2,
		        maximumFractionDigits: 2
		     
		    })
		}
		return ""
	}
	 
	function zeroDecimalRound (number){
			if(number !== undefined){
				
			return number.toLocaleString('it',{
					maximumFractionDigits: 0
			    })
			}
			return ""
		}
	 
	function formatNumberToLocale(number){
		number.toLocaleString('it')
	} 
	 /**
	  * Generates a GUID string.
	  * @returns {String} The generated GUID.
	  * @example af8a8416-6e18-a307-bd9c-f2c947bbb3aa
	  * Sometimes the Math.random() function will return shorter
	  * number (for example 0.4363), due to zeros at the end 
	  * (from the example above, actually the number is 0.4363000000000000).
	  * That’s why we need to append to this string "0000000000"
	  * (a string with nine zeros) and then cutting it off with substr().
	  *	The reason of adding exactly nine zeros is because of the worse
	  * case scenario, which is when the Math.random() 
	  * function will return exactly 0 or 1 (probability of 1/10^16 for each one of them).
	  *  That’s why we needed to add nine zeros to it 
	  *  ("0"+"000000000" or "1"+"0000000000"), and then cutting 
	  *  it off from the second index (3rd character) with a
	  *  length of eight characters. For the rest of the cases, 
	  *  the addition of zeros will not harm the result because 
	  *  it is cutting it off anyway.
	  */
	 function guid() {
	     function _p8(s) {
	         var p = (Math.random().toString(16)+"000000000").substr(2,8);
	         return s ? "-" + p.substr(0,4) + "-" + p.substr(4,4) : p ;
	     }
	     return _p8() + _p8(true) + _p8(true) + _p8();
	 }
	
//})();

