(function () {
  'use strict';
  angular
    .module('codmanApp')
    .service('UtilService', UtilService);

  UtilService.$inject = ['$rootScope', 'ngDialog'];

  function UtilService($rootScope, ngDialog) {
    var vm = this;

    vm.exportTable = exportTable;
    vm.showConfirm = showConfirm;
    vm.saveFile = saveFile;
    vm.isValid = isValid;
    vm.makeId = makeId;

    function showConfirm(title, msgs) {
      return ngDialog.openConfirm({
        template: 'js/util/alert-service/dialog-warning-template.htm',
        plain: false,
        width: '60%',
        controller: ['$scope', function ($scope) {
          $scope.ctrl = {
            title: title,
            message: msgs
          };
          $scope.cancel = function () {
            this.closeThisDialog();
          };
          $scope.call = function () {
            this.confirm();
          };
        }]
      })
    };

    vm.showWarning = function (title, msgs) {
      return ngDialog.open({
        template: 'js/util/alert-service/dialog-warning-template.htm',
        plain: false,
        width: '60%',
        controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
          $scope.ctrl = {
            title: title,
            message: msgs
          };
          $scope.cancel = function () {
            $scope.flag = false;
            ngDialog.closeAll(false);
          };
          $scope.call = function () {
            $scope.flag = true;
            ngDialog.closeAll(true);
          };
        }]
      });
    };

    vm.showError = function (title, msgs) {
      return ngDialog.open({
        template: 'js/util/alert-service/dialog-alert-template.htm',
        plain: false,
        width: '60%',
        controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
          $scope.ctrl = {
            title: title,
            message: msgs
          };
          $scope.cancel = function () {
            ngDialog.closeAll(false);
          };
          $scope.save = function () {
            ngDialog.closeAll(true);
          };
        }]
      })
    };

    vm.showAlert = function (title, message) {
      return ngDialog.open({
        template: 'js/util/alert-service/dialog-alert-template.htm',
        plain: false,
        width: '50%',
        controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
          var ngDialogId = $scope.ngDialogId;
          $scope.ctrl = {
            title: title,
            message: message,
            ngDialogId: ngDialogId
          };
          $scope.cancel = function (ngDialogId) {
            ngDialog.close(ngDialogId);
          };
        }]
      });
    };

    vm.showToolTip = function () {
      $("#myModal").draggable({
        handle: ".modal-header"
      }).modal();
    };

    vm.handleHttpRejection = function (error, message, silent) {
      if (!silent) {
        var GENERIC_ERROR_MESSAGE = 'Si e\' verificato un errore, riporavare piu\' tardi';
        switch (error.status) {
          case 404:
          case 500:
          case 400:
            vm.showAlert("Attenzione!", message ? message : (error.data && error.data.message ? error.data.message : GENERIC_ERROR_MESSAGE));
            break;
          default:
            vm.showAlert('Errore!', GENERIC_ERROR_MESSAGE);
        }
      }
      console.error('XHR Failed:' + error.data && error.data.message ? error.data.message : '');
      console.error('ERROR DATA:' + error.data ? angular.toJson(error.data) : angular.toJson(error));
    };

    function exportTable(id) {
      TableExport.prototype.formatConfig.csv.separator = ';';
      var ExportButtons = document.getElementById(id);

      var instance = new TableExport(ExportButtons, {
        formats: ['csv'],
        exportButtons: false,
      });

      var exportData = instance.getExportData()[id]['csv'];
      //                   // data          // mime              // name              // extension
      //instance.export2file(exportData.data, exportData.mimeType, exportData.filename, exportData.fileExtension);
      vm.saveFile(exportData.data, exportData.mimeType, exportData.filename + exportData.fileExtension);

    }

    function saveFile(response, contentType, contentDisposition) {
      const data =
        new Blob([decodeURIComponent(
          encodeURI('\ufeff' + ((!!response.data) ? response.data.toString() : response)))],
          {type: contentType ? contentType : response.headers('content-type')});
      const a = document.createElement('a');
      const fileURL = URL.createObjectURL(data);
      a.href = fileURL;
      a.download = contentDisposition ? contentDisposition : response.headers('content-disposition').split('"')[1];
      window.document.body.appendChild(a);
      a.click();
      window.document.body.removeChild(a);
      URL.revokeObjectURL(fileURL);
    }

    function isValid(object) {
      var valid = !1;
      if (object) {
        for (var key in object) {
          if ("pageNumber" !== key && "export" !== key && "boolean" !== typeof object[key] && (valid = "object" === typeof object[key] ? valid || isValid(object[key]) : valid || !!object[key] && 0 < object[key].trim().length), valid) {
            break;
          }
        }
      }
      return valid;
    }

    function makeId(length) {
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

      for (var i = 0; i < length; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

      return text;
    }
  }
})();