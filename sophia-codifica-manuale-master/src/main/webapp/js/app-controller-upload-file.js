/**
 * DsrMetadataCtrl
 * 
 * @path /dsrMetadata
 */
codmanApp.controller('UploadFileCtrl', ['$scope','$route', 'ngDialog', '$routeParams', '$location', '$http', function($scope,$route, ngDialog, $routeParams, $location, $http) {

	$scope.ctrl = {
		navbarTab: 'ccidConfig',
		config: [],
		filterParameters: [],
		execute: $routeParams.execute,
		hideForm: $routeParams.hideForm,
		maxrows: 20,
		first: $routeParams.first||0,
		last: $routeParams.last||20,
		filter: $routeParams.filter,
		selectedDsp : $routeParams.dsp||'*',
		selectedUtilization : $routeParams.utilization||'*',
		selectedOffer : $routeParams.offer||'*',
		selectedCountry : $routeParams.country||'*'
	};		
			

	$scope.submit = function() {
	      if ($scope.form.file.$valid && $scope.file) {
	        $scope.upload($scope.file);
	      }
	    };

	    // upload on file select or drop
	    $scope.upload = function (file) {
	        Upload.upload({
	            url: 'upload/url',
	            data: {file: file, 'username': $scope.username}
	        }).then(function (resp) {
	            console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
	        }, function (resp) {
	            console.log('Error status: ' + resp.status);
	        }, function (evt) {
	            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
	            console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
	        });
	    };
	    // for multiple files:
	    $scope.uploadFiles = function (files) {
	      if (files && files.length) {
	        for (var i = 0; i < files.length; i++) {
	        	//  Upload.upload({..., data: {file: files[i]}, ...})...;
	        }
	        // or send them all together for HTML5 browsers:
	        //      Upload.upload({..., data: {file: files}, ...})...;
	      }
	    }
	
}]);


codmanApp.filter('range', function() {
	  return function(input, min, max) {
		    min = parseInt(min); //Make string input int
		    max = parseInt(max);
		    for (var i=min; i<=max; i++)
		      input.push(i);
		    return input;
		  };
});  
