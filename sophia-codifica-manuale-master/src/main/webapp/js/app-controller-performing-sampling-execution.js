/**
 * PerformingSamplingExecutionCtrl
 * 
 * @path /samplingExecution
 */
codmanApp.controller('PerformingSamplingExecutionCtrl', [ '$scope', '$route', 'ngDialog', '$routeParams', '$location', '$http', function($scope, $route, ngDialog, $routeParams, $location, $http) {
	
	$http({
		method : 'GET',
		url : 'rest/performing/campionamento/campionamentoEsecuzione'
	}).then(function successCallback(response) {
		
		$scope.records = [];
		
		angular.forEach(response.data.records,function(record){
			
			//ottengo la data, convertendo la risposta in string
			var date = JSON.stringify(record.contabilita);
			//parso la data ed estraggo il mese in formato decimale
			var month_num = date.substring(4,6);
			
			//creo una data temporanea con il mese precedentemente ottenuto
			var date_temp = new Date(month_num+"/1/2000"),
			 
		    locale = "it-IT",
			//estraggo il mese in formato stringa
		    month = date_temp.toLocaleString(locale, { month: "long" });
			$scope.records.push({id:record.id,contabilita:month + " " + date.substring(0,4)});
			
		});
		/*
		if (response.data == "ACTIVE"){

            angular.element("#activeArrow").show();
            angular.element("#inactiveArrow").hide();

            angular.element("#activateDiv").hide();
            angular.element("#inactiveDiv").show();

            angular.element("#periodoContabileId").attr("disabled", false);

            angular.element("#statoProcessoDiv #statoProcesso").text("Attivo");

            angular.element("#executeButton").button("option", {
             disabled: false
            });

            angular.element("#elaborazioneLabel").show();
            angular.element("#elaborazioneValue").show()
            
            var jqxhr = $http({method:'GET',url:'performing/getAvanzamentoElaborazioneCampionamento'}).then(function(response) {

               $("#elaborazioneValue #totValue").text(response.data.totalePM);
               $("#elaborazioneValue #elabValue").text(response.data.totaleElaborati);
               $("#elaborazioneValue #sospValue").text(response.data.totaleSospesi);
               $("#elaborazioneValue #errValue").text(response.data.totaleErrori);
               
            },function(response){console.log("error: " + response.data)});


        } else
        if (value == "INACTIVE"){
          $("#activeArrow").hide();
          $("#inactiveArrow").show();

          $("#activateDiv").show();
          $("#inactiveDiv").hide();

          $("#periodoContabileId").attr("disabled", true);

          $("#statoProcessoDiv #statoProcesso").text("Inattivo");

          $("#executeButton").button("option", {
           disabled: true
          });

          $("#elaborazioneLabel").hide();
          $("#elaborazioneValue").hide();

        }else{
            alert(value);
        }
	*/}, function errorCallback(response) {

		$scope.showError("", "Non risulta nessun dato per il periodo selezionato");
		
	});
	
	$scope.showError = function(title, msgs) {
		ngDialog.open({
			template : 'pages/advance-payment/dialog-alert.html',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				$scope.ctrl = {
					title : title,
					message : msgs
				};
				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				$scope.save = function() {
					ngDialog.closeAll(true);
				};
			} ]
		}).closePromise.then(function(data) {
			// no op
		});
	};
	
	$scope.executeCampionamento = function(){
		
		var mesi = ["gennaio","febbraio","marzo","aprile","maggio","giugno","luglio","agosto",
  		  "settembre","ottobre","dicembre"];
		
		angular.forEach($scope.records,function(record){
			if(record.contabilita == angular.element("#periodoContabileId").val()){
				$http({
					method : 'GET',
					url : 'rest/performing/campionamento/sendCommand?info=START_CAMPIONAMENTO&periodoContabile='+record.id
				}).then(function successCallback(response) {
					
					console.log("success: " + response.data);
					
				}, function errorCallback(response) {
					
					console.log("error: " + response.data);
					
				});
			}
		})
		
		
	}
	

	// Funzione principale richiamata al caricamento della pagina
	$scope.onRefresh = function() {
	};

	$scope.onRefresh();

	showAlert = function(title, message) {
		ngDialog.open({
			template : 'pages/advance-payment/dialog-alert.html',
			plain : false,
			width : '50%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				var ngDialogId = $scope.ngDialogId;
				$scope.ctrl = {
					title : title,
					message : message,
					ngDialogId : ngDialogId
				};
				$scope.cancel = function(ngDialogId) {
					ngDialog.close(ngDialogId);
				};
			} ]
		}).closePromise.then(function(data) {
			if (data.value === true) {

			}
		});
	};

	$scope.$on('filterApply', function(event, parameters) {
		$scope.ctrl.filterParameters = parameters;
		$scope.getPagedResults(0,
			$scope.ctrl.maxrows,
			parameters.dsp,
			parameters.invoiceNumber,
			parameters.monthFrom,
			parameters.yearFrom,
			parameters.monthTo,
			parameters.yearTo
		);
	});

} ]);