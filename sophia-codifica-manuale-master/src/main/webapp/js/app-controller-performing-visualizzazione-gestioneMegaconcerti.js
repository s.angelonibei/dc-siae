codmanApp.controller('VisualizzazioneGestioneMegaconcertiCtrl',['$scope', 'ngDialog', 'GestioneMegaconcertiService', '$rootScope', function($scope, ngDialog, GestioneMegaconcertiService, $rootScope){
	
	$scope.user = angular.element("#user").val();
	
	$scope.params = {
		inizioPeriodoValidazione : '',
		finePeriodoValidazione: '',
		codiceRegola: '',
		order: '',
		page: 0
	}
	
	$scope.orderBy = null;
	
	$scope.filterApply = function(params){
		$scope.saveQuery = angular.copy(params);
		$scope.getResult($scope.saveQuery,true);
	}
	
	$scope.orderImage = function(name,param){
		if($scope.saveQuery == undefined){
			return;
		}
		//get id of html element clicked
		var id = "#" + name;
		
		//get css class of that html element for change the image of arrows
		var classe = angular.element(id).attr("class");

		//set to original position all images of arrows, on each columns.
		var element1 = document.getElementsByClassName("glyphicon glyphicon-sort-by-attributes");
		var element2 = document.getElementsByClassName("glyphicon glyphicon-sort-by-attributes-alt");
		angular.element(element1).attr("class","glyphicon glyphicon-sort");
		angular.element(element2).attr("class","glyphicon glyphicon-sort");

		if(classe != "glyphicon glyphicon-sort-by-attributes" || classe == "glyphicon glyphicon-sort"){
			
			angular.element(id).attr("class","glyphicon glyphicon-sort-by-attributes");
			
		}else{
			angular.element(id).attr("class","glyphicon glyphicon-sort-by-attributes-alt");
		}
		
		$scope.order(param);
		
	}
	
	
	
	$scope.order = function(param){
		if($scope.orderBy == param.concat(" asc")){
			$scope.orderBy = param.concat(" desc");
		}else{
			$scope.orderBy = param.concat(" asc");
		}
		$scope.saveQuery.order = $scope.orderBy;
		$scope.getResult($scope.saveQuery,false);
	}
	
	$scope.navigateToNextPage = function() {
		$scope.saveQuery.page=$scope.saveQuery.page+1;
		$scope.getResult($scope.saveQuery,false);

	};

	$scope.navigateToPreviousPage = function() {
		$scope.saveQuery.page=$scope.saveQuery.page-1;
		$scope.getResult($scope.saveQuery,false);

	};
	
	$scope.navigateToEndPage = function(){
		$scope.saveQuery.page = $scope.sampling.pages.length - 1;
		$scope.getResult($scope.saveQuery,false);
	};
	
	$scope.getResult = function(params,count){
		GestioneMegaconcertiService.getListaConfigurazioniAttive(params).then(function(response){
			if(response.data.totRecords > 1000 && count){
				$scope.showWarning("Attenzione","Il risultato prevede " + response.data.totRecords + " record, è consigliabile applicare dei filtri aggiuntivi. Sicuro di voler continuare?",response.data)
			}else if(response.data.totRecords == 0){
				$scope.showError("Errore","Non risulta nessun dato per la ricerca effettuata");
				$scope.sampling = {}
			}else{
				$scope.sampling = response.data;
			}
		},function(response){
			
		})
	}
	
	$scope.openModel = function(modifica,mcOld){
		var user = $scope.user;
		var parseDate = $scope.parseDate;
		var showError = $scope.showError;
		var checkDate = $scope.checkDate;
		var convertFlagCapMultiplo = $scope.convertFlagCapMultiplo;
		var getResult = $scope.getResult;
		var params = $scope.saveQuery;
		var extractTime = $scope.extractTime;
		var checkNum = $scope.checkNum;
		var askConfirm = $scope.askConfirm;
		ngDialog.open({
			template : 'pages/advance-payment/dialog-MC.html',
			plain : false,
			width : '90%',
			controller : [ '$scope', 'GestioneMegaconcertiService', function($scope, GestioneMegaconcertiService) {
				
				$scope.modifica = modifica;
				$scope.parseDate = parseDate;
				$scope.extractTime = extractTime;
				$scope.checkNum = checkNum;
				
				if(modifica == true){
					
					GestioneMegaconcertiService.getAggioAttivo(mcOld.id).then(function(response){
						$scope.mc = response.data
						$scope.mc.inizioPeriodoValidazione = $scope.parseDate($scope.mc.inizioPeriodoValidazione);
						
						if($scope.mc.finePeriodoValidazione){
							$scope.mc.finePeriodoValidazione = $scope.parseDate($scope.mc.finePeriodoValidazione);
						}else if(!$scope.mc.finePeriodoValidazione){
							$scope.mc.finePeriodoValidazione = "in corso"
						}
						if($scope.mc.flagCapMultiplo){
							$scope.mc.flagCapMultiplo = 'true';
						}else{
							$scope.mc.flagCapMultiplo = 'false';
						}
						$scope.hideForm = true;
						$scope.tipo = "Modifica";
						$scope.mc.utenteUltimaModifica = user;
						GestioneMegaconcertiService.getConfigurationHistory($scope.mc.codiceRegola).then(function(response){
							$scope.storico = response.data;
						},function(response){

						})
					},function(response){
						showError("Errore","Non è stato possibile ottenere informazioni dettagliate riguardo alla configurazione: " + mc.codiceRegola)
					})
				}else if (modifica == false){
					$scope.tipo = "Creazione";
				}
				
				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				
				$scope.save = function(mc) {
					
						var data = angular.copy(mc)
						data.flagCapMultiplo = convertFlagCapMultiplo(data.flagCapMultiplo)
						if(!data.finePeriodoValidazione){
							data.finePeriodoValidazione = undefined
							mc.finePeriodoValidazione = "in corso"
						}else{
							if(!checkDate(data.inizioPeriodoValidazione,data.finePeriodoValidazione)){
								showError("Errore","La data 'Valido a' non può essere antecedente alla data 'Valido da'")
								return;
							};
						}
						
						if(data.soglia.toString().indexOf(",") >= 0 || data.aggioSottosoglia.toString().indexOf(",") >= 0 || data.aggioSoprasoglia.toString().indexOf(",") >= 0 || data.valoreCap.toString().indexOf(",") >= 0){
							showError("Errore","I valori decimali devono essere scritti con il punto.")
							return;
						}
						
						if(!checkNum(data.soglia) || !checkNum(data.aggioSottosoglia) || !checkNum(data.aggioSoprasoglia) || !checkNum(data.valoreCap)){
							showError("Errore","I parametri, a parte il Tipo CAP, devono essere numeri")
							return;
						}
						
						GestioneMegaconcertiService.getCountRecordsAffected(data).then(function(response){
							if(response.data > 0){
								var message = "Il salvataggio della configurazione comporterà la modifica di " + response.data + " eventi. Sei sicuro di voler continuare?"
							}else if (response.data == 0){
								var message = "Il salvataggio della configurazione non influenzerà alcun evento attuale. Sei sicuro di voler continuare?"
							}
							askConfirm("Attenzione",message)
							.closePromise.then(function(model){
								if(model.value == true){
									data.utenteCreazione = user;
									GestioneMegaconcertiService.createConfiguration(data).then(function(response){
										showError("Successo","Creazione avvenuta con successo")
										getResult(params,false)
										$scope.cancel()
									},function(response){
										showError("Errore","Non è stato possibile salvare la configurazione: " + response.data)
										$scope.cancel()
									})
								}
							})
						},function(response){
							showError("Errore","Non è stato possibile controllare il numero di eventi influenzati da questa configurazione")
						})
				}
				
			  $scope.saveModify = function(mc){
			  
			  		var data = angular.copy(mc)
			  
					data.flagCapMultiplo = convertFlagCapMultiplo(data.flagCapMultiplo)
					
					if(!data.inizioPeriodoValidazione || !data.soglia || !data.aggioSottosoglia || !data.aggioSoprasoglia || !data.valoreCap || data.flagCapMultiplo.length == 0){
						showError("Errore","Tutti i parametri, a parte la data fine validazione, devono essere valorizzati")
						return;
					}
					
					if(!data.finePeriodoValidazione || data.finePeriodoValidazione == "in corso"){
						data.finePeriodoValidazione = undefined
						mc.finePeriodoValidazione = "in corso"
					}else{
						if(!checkDate(data.inizioPeriodoValidazione,data.finePeriodoValidazione)){
							showError("Errore","La data 'Valido a' non può essere antecedente alla data 'Valido da'")
							return;
						};
					}
					
					if(data.soglia.toString().indexOf(",") >= 0 || data.aggioSottosoglia.toString().indexOf(",") >= 0 || data.aggioSoprasoglia.toString().indexOf(",") >= 0 || data.valoreCap.toString().indexOf(",") >= 0){
						showError("Errore","I valori decimali devono essere scritti con il punto.")
						return;
					}
					
					if(!checkNum(data.soglia) || !checkNum(data.aggioSottosoglia) || !checkNum(data.aggioSoprasoglia) || !checkNum(data.valoreCap)){
						showError("Errore","I parametri, a parte il Tipo CAP e le date, devono essere numeri")
						return;
					}
					
					data.flagCapMultiplo = convertFlagCapMultiplo(data.flagCapMultiplo)
			  
			  		GestioneMegaconcertiService.getCountRecordsAffected(data).then(function(response){
					if(response.data > 0){
						var message = "Il salvataggio della configurazione comporterà la modifica di " + response.data + " eventi. Sei sicuro di voler continuare?"
					}else if (response.data == 0){
						var message = "Il salvataggio della configurazione non influenzerà alcun evento attuale. Sei sicuro di voler continuare?"
					}
					askConfirm("Attenzione",message)
					.closePromise.then(function(model){
						if(model.value == true){
							GestioneMegaconcertiService.editConfiguration(data).then(function(response){
								showError("Successo","Modifica avvenuta con successo")
								getResult(params,false)
								$scope.cancel()
							},function(response){
								showError("Errore","Non è stato possibile salvare la configurazione: " + response.data)
							})
						}
					})
					},function(response){
						showError("Errore","Non è stato possibile controllare il numero di eventi influenzati da questa configurazione")
					})
			  }
			} ]
		})
		
	}
	
	//NGDIALOG QUI SOTTO
	
	$scope.checkNum = function(param){
		return !isNaN(parseFloat(param)) && isFinite(param)
	}
	
	$scope.showError = function(title, msgs) {
		ngDialog.open({
			template : 'pages/advance-payment/dialog-alert.html',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				var ngDialogId = $scope.ngDialogId;
				$scope.ctrl = {
					title : title,
					message : msgs,
					ngDialogId : ngDialogId
				};
				$scope.cancel = function(ngDialogId) {
					ngDialog.close(ngDialogId);
				};
			} ]
		}).closePromise.then(function(data) {
		});
	};
	
	$scope.showWarning = function(title, msgs, data) {
		var parentScope = $scope
		ngDialog.open({
			template : 'pages/advance-payment/dialog-warning.html',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				$scope.ctrl = {
					title : title,
					message : msgs
				};
				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				$scope.call = function() {
					parentScope.sampling = data
					ngDialog.closeAll(true);
				};
			} ]
		})
	};
	
	$scope.askConfirm = function(title, msgs) {
		return ngDialog.open({
			template : 'pages/advance-payment/dialog-warning.html',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				$scope.ctrl = {
					title : title,
					message : msgs
				};
				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				$scope.call = function() {
					ngDialog.closeAll(true);
				};
			} ]
		})
	};
	
	// FINE NGDIALOG
	
	$scope.checkDate = function(from,to){
		if(from.length > 7){
			var comparisonFrom = from.split("-")[2] + from.split("-")[1] + from.split("-")[0];
			var comparisonTo = to.split("-")[2] + to.split("-")[1] + to.split("-")[0];
		}else{
			var comparisonFrom = from.split("-")[1] + from.split("-")[0];
			var comparisonTo = to.split("-")[1] + to.split("-")[0];
		}
		
		
		if(comparisonTo < comparisonFrom){
			return false;
		}else{
			return true;
		}
	}
	
	$scope.parseDate = function(data){
		var anno = data.split("-")[0];
		var mese = data.split("-")[1];
		var giorno = data.split("-")[2].split("T")[0];
		return giorno + "-" + mese + "-" + anno;
	}
	
	$scope.extractTime = function(data){
		var time = data.split("T")[1];
		var hours = time.split(":")[0];
		var minutes = time.split(":")[1];
		var seconds = time.split(":")[2].split(".")[0];
		
		return ' ' + hours + ":" + minutes + ":" + seconds;
	}
	
	$scope.convertFlagCapMultiplo = function(flagCapMultiplo){
		if(flagCapMultiplo == 'true'){
			return true;
		}
		if(flagCapMultiplo == 'false'){
			return false;
		}
		if(flagCapMultiplo){
			return 'true';
		}else{
			return 'false';
		}
		
	}
	
	$scope.saveQuery = {order:'inizioPeriodoValidazione desc'};
	$scope.orderImage('spanValidoDa','inizioPeriodoValidazione');
	
}]);

codmanApp.service('GestioneMegaconcertiService',function($http){
	
	this.getListaConfigurazioniAttive = function(params){
		return $http({
			method: 'GET',
			url: 'rest/performing/cruscotti/getListaAggiAttivi',
			params: params
		})
	}
	
	this.createConfiguration = function(mc){
		return $http({
			method: 'POST',
			url: 'rest/performing/cruscotti/addConfigurazioneNuovoAggio',
			data: {
				inizioPeriodoValidazione: mc.inizioPeriodoValidazione,
				finePeriodoValidazione: mc.finePeriodoValidazione,
				soglia: mc.soglia,
				aggioSottosoglia: mc.aggioSottosoglia,
				aggioSoprasoglia: mc.aggioSoprasoglia,
				valoreCap: mc.valoreCap,
				flagCapMultiplo: mc.flagCapMultiplo,
				utenteCreazione: mc.utenteCreazione,
				codiceRegola: mc.codiceRegola,
			}
		})
	}
	
	this.getConfigurationHistory = function(codiceRegola){
		return $http({
			method: 'GET',
			url: 'rest/performing/cruscotti/getStoricoAggio',
			params: {
				codiceRegola: codiceRegola
			}
		})
	}
	
	this.getAggioAttivo = function(id){
		return $http({
			method: 'GET',
			url: 'rest/performing/cruscotti/getAggioAttivo',
			params: {
				id: id
			}
		})
	}
	
	this.getCountRecordsAffected = function(mc){
		return $http({
			method: 'GET',
			url: 'rest/performing/cruscotti/getEventiToEditOnAggio',
			params: {
				inizioPeriodoValidazione: mc.inizioPeriodoValidazione,
				finePeriodoValidazione: mc.finePeriodoValidazione,
				id: mc.id
			}
		})
	}
	
	this.editConfiguration = function(mc){
		return $http({
			method: 'PUT',
			url: 'rest/performing/cruscotti/updateAggio',
			data: mc
		})
	}
	
})