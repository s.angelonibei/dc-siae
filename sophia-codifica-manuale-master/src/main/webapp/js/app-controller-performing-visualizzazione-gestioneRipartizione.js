codmanApp.controller('VisualizzazioneGestioneRipartizioneCtrl',['$scope', 'GestioneRipartizioneService', 'ngDialog', function($scope, GestioneRipartizioneService, ngDialog){
	
	$scope.user = angular.element("#headerLinksBig").text().trim();
	
	$scope.params = {
		selectedContabilityDateFrom : '',
		selectedContabilityDateTo: '',
		voceIncasso: '',
		voceRipartizione: '',
//		count: true, CASO SENZA COUNT
		order: '',
		page: 0
	}
	
	$scope.orderBy = null;
	$scope.sampling = {};
	
	$scope.checkParams = function(params){
		
		if(!checkDate(params.selectedContabilityDateFrom,params.selectedContabilityDateTo)){
			$scope.showError("Attenzione","La data 'Contabilità a' non può essere inferiore alla data 'Contabilità da'");
			return;
		}
		
		$scope.filterApply();
	}
	
	$scope.filterApply = function(){
		$scope.saveQuery = angular.copy($scope.params);
		$scope.getResult($scope.params);
	}
	
	$scope.orderImage = function(name,param){
		if($scope.saveQuery == undefined){
			return;
		}
		//get id of html element clicked
		var id = "#" + name;
		
		//get css class of that html element for change the image of arrows
		var classe = angular.element(id).attr("class");

		//set to original position all images of arrows, on each columns.
		var element1 = document.getElementsByClassName("glyphicon glyphicon-sort-by-attributes");
		var element2 = document.getElementsByClassName("glyphicon glyphicon-sort-by-attributes-alt");
		angular.element(element1).attr("class","glyphicon glyphicon-sort");
		angular.element(element2).attr("class","glyphicon glyphicon-sort");

		if(classe != "glyphicon glyphicon-sort-by-attributes" || classe == "glyphicon glyphicon-sort"){
			
			angular.element(id).attr("class","glyphicon glyphicon-sort-by-attributes");
			
		}else{
			angular.element(id).attr("class","glyphicon glyphicon-sort-by-attributes-alt");
		}
		
		$scope.order(param);
		
	}
	
	
	
	$scope.order = function(param){
		if($scope.orderBy == param.concat(" asc")){
			$scope.orderBy = param.concat(" desc");
		}else{
			$scope.orderBy = param.concat(" asc");
		}
		$scope.saveQuery.order = $scope.orderBy;
//		$scope.saveQuery.count = false; CASO SENZA COUNT
		$scope.getResult($scope.saveQuery);
	}
	
	$scope.navigateToNextPage = function() {
		$scope.saveQuery.page=$scope.saveQuery.page+1;
//		$scope.saveQuery.count = false; CASO SENZA COUNT
		$scope.getResult($scope.saveQuery);

	};

	$scope.navigateToPreviousPage = function() {
		$scope.saveQuery.page=$scope.saveQuery.page-1;
//		$scope.saveQuery.count = false; CASO SENZA COUNT
		$scope.getResult($scope.saveQuery);

	};
	
	$scope.navigateToEndPage = function(){
		$scope.saveQuery.page = $scope.sampling.pageTot;
//		$scope.saveQuery.count = false; CASO SENZA COUNT
		$scope.getResult($scope.saveQuery);
	};
	
	$scope.getResult = function(params){
		
		GestioneRipartizioneService.getSampling(params)/*.then(function(response){
		
		CASO SENZA COUNT
		
		if(response.data.totRecords > 1000){
			$scope.showWarning("Attenzione","Il risultato prevede " + response.data.totRecords + " record, è consigliabile applicare dei filtri aggiuntivi. Sicuro di voler continuare?");
		}else{
			$scope.sampling = response.data;
		}
		},function(response){
			$scope.showError("","Non risulta nessun dato per la ricerca effettuata");
		})*/
		
		/*CASO CON COUNT
		if(params.count == true && response.data > 1000){
			$scope.showWarning("Attenzione","Il risultato prevede " + response.data + " record, è consigliabile applicare dei filtri aggiuntivi. Sicuro di voler continuare?");
		}else{
			$scope.sampling = response.data;
		}
		},function(response){
			$scope.showError("","Non risulta nessun dato per la ricerca effettuata");
		})*/
	}
	
	
	
	//NGDIALOG QUI SOTTO
	
	$scope.showWarning = function(title, msgs) {
		var parentScope = $scope;
		ngDialog.open({
			template : 'pages/advance-payment/dialog-warning.html',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				$scope.ctrl = {
					title : title,
					message : msgs
				};
				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				$scope.call = function() {
//					parentScope.params.count = false; CASO SENZA COUNT
//					parentScope.getResult(parentScope.params); CASO SENZA COUNT
					
					parentScope.sampling = parentScope.risposta;
					ngDialog.closeAll(false);
				};
			} ]
		}).closePromise.then(function(data) {

		});
	};
	
	$scope.showError = function(title, msgs) {
		ngDialog.open({
			template : 'pages/advance-payment/dialog-alert.html',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				$scope.ctrl = {
					title : title,
					message : msgs
				};
				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				$scope.save = function() {
					ngDialog.closeAll(true);
				};
			} ]
		}).closePromise.then(function(data) {
		});
	};
	
	// FINE NGDIALOG
	
	var checkDate = function(from,to){
		var comparisonFrom = from.split("-")[1] + from.split("-")[0];
		var comparisonTo = to.split("-")[1] + to.split("-")[0];
		
		if(comparisonTo < comparisonFrom){
			return false;
		}else{
			return true;
		}
	}
	
}]);

codmanApp.service('GestioneRipartizioneService',function($http){
	
//	this.getSampling = function(params){
//		return $http({
//			method: 'GET',
//			url: 'rest/performing/cruscotti/gestioneRipartizione', corretto?
//			params: {
//				//aggiungere gli altri a seconda di come viene costruito il servizio back-end
//				count: params.count,
//				order : params.order
//			}
//		})
//	}
	
})