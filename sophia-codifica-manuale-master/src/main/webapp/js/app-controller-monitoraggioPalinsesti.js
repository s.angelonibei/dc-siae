/**
 * DsrMetadataCtrl
 *
 * @path /dsrMetadata
 */
codmanApp.controller('MonitoraggioPalinsestiCtrl', ['$scope', '$route', 'ngDialog', '$routeParams', '$location', '$http', 'Upload', 'gestioneMusicProvidersService',
    function ($scope, $route, ngDialog, $routeParams, $location, $http, Upload, gestioneMusicProvidersService) {

        $scope.listUtilizzazioni=[];
        $scope.ctrl = {
            navbarTab: 'ccidConfig',
            config: [],
            filterParameters: [],
            execute: $routeParams.execute,
            hideForm: $routeParams.hideForm,
            musicProviders: [],
            palinsesti: [],
            listaGauge: [],
            mesiSelected: [],
            mesi: [
                {
                    id: '1',
                    label: ' Gennaio '
                },
                {
                    id: '2',
                    label: ' Febbraio '
                },
                {
                    id: '3',
                    label: ' Marzo '
                },
                {
                    id: '4',
                    label: ' Aprile '
                },
                {
                    id: '5',
                    label: ' Maggio '
                },
                {
                    id: '6',
                    label: ' Giugno '
                },
                {
                    id: '7',
                    label: ' Luglio '
                },
                {
                    id: '8',
                    label: ' Agosto '
                },
                {
                    id: '9',
                    label: ' Settembre '
                },
                {
                    id: '10',
                    label: ' Ottobre '
                },
                {
                    id: '11',
                    label: ' Novembre '
                },
                {
                    id: '12',
                    label: ' Dicembre '
                }],
            anni: [(new Date()).getFullYear(), (new Date()).getFullYear() - 1, (new Date()).getFullYear() - 2, (new Date()).getFullYear() - 3, (new Date()).getFullYear() - 4, (new Date()).getFullYear() - 5, (new Date()).getFullYear() - 6, (new Date()).getFullYear() - 7],

        };

        $scope.multiselectSettings = {
            scrollableHeight: '200px',
            scrollable: false,
            enableSearch: false,
            showUncheckAll: true,
            searchBr: false,
            checkboxes: false
        };


        // Funzione principale richiamata al caricamento della pagina
        $scope.onRefresh = function () {
            $scope.getMusicProvider();
        };
//TODO
        $scope.downloadUtilizzazioni = function (id) {


            gestioneMusicProvidersService.download(id).then(function(response){

                var linkElement = document.createElement('a');

//        			try {
                var blob = new Blob([response.data], { type: response.type });
                saveAs(blob,conf.nomeOriginaleFile);

//        			} catch (ex) {
//        				CSService.showError("Errore","Impossibile fare il download dell'excel. File non trovato");
//        			}

            },function(response){
                if(response.status == 404){
                    CSService.showError("Errore","Impossibile fare il download dell'excel. File non trovato");
                    return
                }
                CSService.showError("Errore","Impossibile fare il download dell'excel.");
            })



        };

        //TODO
        $scope.downloadArmonizzato = function (item) {

        };


        $scope.getMusicProvider = function () {
            gestioneMusicProvidersService.getMusicProviders('')
                .then(function (response) {
                    $scope.ctrl.musicProviders = response;
                });

        };
        $scope.getPalinsesti = function (musicProvider) {
            gestioneMusicProvidersService.getPalinsesti(musicProvider)
                .then(function successCallback(response) {
                    $scope.ctrl.palinsesti = response;
                    for (var palinsesto in $scope.ctrl.palinsesti) {
                        $scope.ctrl.palinsesti[palinsesto].active = !$scope.ctrl.palinsesti[palinsesto].dataFineValidita;
                    }
                });
        };

        $scope.getListFiltered = function (item) {
            return item ?
                $scope.ctrl.musicProviders.filter($scope.createFilterFor(item)) :
                $scope.ctrl.musicProviders;
        };

        $scope.createFilterFor = function (query) {
            var lowercaseQuery = angular.uppercase(query);

            return function filterFn(item) {
                return (item.nome.indexOf(lowercaseQuery) === 0);
            };

        };

        $scope.apply = function (musicProvider, palinsesto, anno, mesiSelected) {
            gestioneMusicProvidersService.getUtilizationFiles(musicProvider, palinsesto, anno, mesiSelected)
                .then(function successCallback(response) {
                    $scope.listUtilizzazioni = response;
                });
        };

        $scope.scaricaFileArmonizzato = function (musicProvider, palinsesto, anno, mesiSelected) {
            gestioneMusicProvidersService.scaricaFileArmonizzato(musicProvider, palinsesto, anno, mesiSelected)
                .then(function successCallback(response) {
                    saveAs(new Blob([response.data],{type:"text/plain;charset=UTF-8"}), response.headers('content-disposition').split('"')[1]);
                });
        };


        $scope.showUpload = function () {
            var scope = $scope;
            ngDialog.open({
                template: 'pages/performing/gestioneUtilizzazioni/dialog-upload.html',
                plain: false,
                width: '50%',
                controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
                    $scope.ctrl = {
                        tipoUpload: "Singolo",
                        tipiUpload: ['Singolo'/*, 'Massivo' */],
                        musicProviders: [],
                        palinsesti: [],
                        listaGauge: [],
                        musicProvider: $routeParams.musicProvider || '*',
                        palinsesto: $routeParams.palinsesto || '*',
                        anno: $routeParams.anno || '',
                        mese: $routeParams.mese || '*',
                        mesi: [
                            {
                                id: '1',
                                label: ' Gennaio '
                            },
                            {
                                id: '2',
                                label: ' Febbraio '
                            },
                            {
                                id: '3',
                                label: ' Marzo '
                            },
                            {
                                id: '4',
                                label: ' Aprile '
                            },
                            {
                                id: '5',
                                label: ' Maggio '
                            },
                            {
                                id: '6',
                                label: ' Giugno '
                            },
                            {
                                id: '7',
                                label: ' Luglio '
                            },
                            {
                                id: '8',
                                label: ' Agosto '
                            },
                            {
                                id: '9',
                                label: ' Settembre '
                            },
                            {
                                id: '10',
                                label: ' Ottobre '
                            },
                            {
                                id: '11',
                                label: ' Novembre '
                            },
                            {
                                id: '12',
                                label: ' Dicembre '
                            }],
                        anni: [(new Date()).getFullYear(), (new Date()).getFullYear() - 1, (new Date()).getFullYear() - 2, (new Date()).getFullYear() - 3, (new Date()).getFullYear() - 4, (new Date()).getFullYear() - 5, (new Date()).getFullYear() - 6, (new Date()).getFullYear() - 7],

                    };

                    $scope.init = function () {
                        gestioneMusicProvidersService.getMusicProviders('')
                            .then(function (response) {
                                $scope.ctrl.musicProviders = response;
                            });
                    };


                    $scope.cancel = function () {
                        ngDialog.closeAll(false);
                    };

                    $scope.save = function () {
                        if ($scope.form.file.$valid && $scope.ctrl.file) {
                            if ($scope.ctrl.tipoUpload == 'Singolo') {
                                //UPLOAD SINGOLO
                                Upload.upload({
                                    url: 'rest/performing/radioInStore/utilizationfiles',
                                    data: {
                                        file: $scope.ctrl.file,
                                        idUtente: 1,
                                        idMusicProvider: $scope.ctrl.musicProvider.id,
                                        idPalinsesto: $scope.ctrl.palinsesto.id,
                                        anno: $scope.ctrl.anno,
                                        mese: $scope.ctrl.mese
                                    }
                                }).then(function (resp) {
                                    ngDialog.closeAll(true);
                                    scope.showError("Esito Upload", "File Caricato con successo");
                                }, function (resp) {
                                    ngDialog.closeAll(false);
                                    scope.showError("Esito Upload", "Impossibile caricare il file, controllare la validità");
                                }, function (evt) {
                                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                                    logmsg('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                                });
                            } else {
                                //UPLOAD MASSIVO
                                Upload.upload({
                                    url: 'rest/broadcastingS3/fe/utilizationfile/upload',
                                    data: {
                                        file: $scope.ctrl.file,
                                        idUtente: 0,
                                        idMusicProvider: $scope.ctrl.musicProvider.id,
                                    }
                                }).then(function (resp) {
                                    ngDialog.closeAll(true);
                                    scope.showError("Esito Upload", "File Caricato con successo");
                                }, function (resp) {
                                    ngDialog.closeAll(false);
                                    scope.showError("Esito Upload", "Impossibile caricare il file, controllare la validità");
                                }, function (evt) {
                                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                                    logmsg('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                                });
                            }


                        }
                    };

                    $scope.init();
                }]
            }).closePromise.then(function (data) {
                if (data.value === true) {
                    $route.reload();
                }
            });
        };
        $scope.showError = function (title, msgs) {
            ngDialog.open({
                template: 'pages/advance-payment/dialog-alert.html',
                plain: false,
                width: '60%',
                controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
                    $scope.ctrl = {
                        title: title,
                        message: msgs
                    };
                    $scope.cancel = function () {
                        ngDialog.closeAll(false);
                    };
                    $scope.save = function () {
                        ngDialog.closeAll(true);
                    };
                }]
            }).closePromise.then(function (data) {
                // no op
            });
        };

        $scope.$on('filterApply', function (event, parameters) {
            $scope.ctrl.filterParameters = parameters;
            $location.search('execute', 'true')
                .search('hideForm', false)
                .search('first', 0)
                .search('last', $scope.ctrl.maxrows)
                .search('dsp', parameters.dsp)
                .search('utilization', parameters.utilization)
                .search('offer', parameters.offer)
                .search('country', parameters.country)
                .search('hideForm', parameters.hideForm);
            $scope.ctrl.hideForm = false;

        });

        $scope.onRefresh();

    }]);


codmanApp.filter('range', function () {
    return function (input, min, max) {
        min = parseInt(min); //Make string input int
        max = parseInt(max);
        for (var i = min; i <= max; i++)
            input.push(i);
        return input;
    };
});