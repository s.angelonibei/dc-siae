// (function(){

/**
 * DroolsPricingCtrl
 *
 * @path /droolsPricing
 */
codmanApp.controller('DroolsPricingCtrl', ['$scope', 'ngDialog', '$routeParams', '$location', '$route', '$http', 'Upload', 'configService', 'UtilService', 'pricingService',
    function ($scope, ngDialog, $routeParams, $location, $route, $http, Upload, configService, UtilService, pricingService) {
        yyyymmdd = function (date, sep) {
            if (date) {
                var dt = new Date(date);
                var mm = dt.getMonth() + 1;
                var dd = dt.getDate();
                return [dt.getFullYear(),
                (mm > 9 ? '' : '0') + mm,
                (dd > 9 ? '' : '0') + dd
                ].join(sep);
            }
        };

        $scope.ctrl = {
            navbarTab: 'droolsPricing',
            maxrows: configService.maxRowsPerPage,
            first: $routeParams.first || 0,
            last: $routeParams.last || configService.maxRowsPerPage,
            idDsp: $routeParams.idDsp,
            idUtilizationType: $routeParams.idUtilizationType,
            validFrom: yyyymmdd($routeParams.validFrom, '-'),
            validTo: yyyymmdd($routeParams.validTo, '-'),
            xlsS3Url: $routeParams.xlsS3Url,
            sortType: 'idDsp',
            sortReverse: false,
            decode: {},
            searchResults: {}
        };

        $scope.onAzzera = function () {
            $scope.ctrl.first = 0;
            $scope.ctrl.last = $scope.ctrl.maxrows;
            $scope.ctrl.idDsp = null;
            $scope.ctrl.idUtilizationType = null;
            $scope.ctrl.validFrom = null;
            $scope.ctrl.validTo = null;
            $scope.ctrl.xlsS3Url = null;
        };

        $scope.onRicerca = function () {
            $scope.onRicercaEx(0, $scope.ctrl.maxrows);
        };

        $scope.onRicercaEx = function (first, last) {
            $http({
                method: 'GET',
                url: 'rest/droolsPricing/search',
                params: {
                    first: first,
                    last: last,
                    idDsp: $scope.ctrl.idDsp,
                    idUtilizationType: $scope.ctrl.idUtilizationType,
                    validFrom: $scope.ctrl.validFrom,
                    validTo: $scope.ctrl.validTo,
                    xlsS3Url: $scope.ctrl.xlsS3Url
                }
            }).then(function successCallback(response) {
                $scope.ctrl.searchResults = response.data;
                $scope.ctrl.first = response.data.first;
                $scope.ctrl.last = response.data.last;
            }, function errorCallback(response) {
                $scope.ctrl.searchResults = {};
            });
        };

        $scope.navigateToNextPage = function () {
            $location
                .search('first', $scope.ctrl.last)
                .search('last', $scope.ctrl.last + $scope.ctrl.maxrows)
                .search('idDsp', $scope.ctrl.idDsp || null)
                .search('idUtilizationType', $scope.ctrl.idUtilizationType || null)
                .search('validFrom', $scope.ctrl.validFrom || null)
                .search('validTo', $scoper.ctrl.validTo || null)
                .search('xlsS3Url', $scope.ctrl.xlsS3Url || null);
        };

        $scope.navigateToPreviousPage = function () {
            $location
                .search('first', Math.max($scope.ctrl.first - $scope.ctrl.maxrows, 0))
                .search('last', $scope.ctrl.first)
                .search('idDsp', $scope.ctrl.idDsp || null)
                .search('idUtilizationType', $scope.ctrl.idUtilizationType || null)
                .search('validFrom', $scope.ctrl.validFrom || null)
                .search('validTo', $scope.ctrl.validTo || null)
                .search('xlsS3Url', $scope.ctrl.xlsS3Url || null);
        };

        $scope.goToConfiguration = function (item) {
            if(item) {
                const splitPath = (item.xlsS3Url || '').split('/') || [];
                const nomeFileExcel =  splitPath[splitPath.length -1];
                item["nomeFileExcel"] = nomeFileExcel;
                item["tipoUtilizzo"] = item.idUtilizationType;
                item = _.omit(item, 'idUtilizationType');
            }
            pricingService.setPreloadConfig(item || null);
            $location.path("/configurazione-pricing");
        };

        $scope.showUpload = function (event) {
            var scope = $scope;
            ngDialog.open({
                template: 'pages/drools/dialog-upload.html',
                plain: false,
                width: '50%',
                controller: ['$scope', 'ngDialog', '$http', function ($scope, ngDialog, $http) {
                    $scope.ctrl = {
                        idDsp: null,
                        idUtilizationType: null,
                        validFrom: null,
                        validTo: null,
                        file: null,
                        decode: scope.ctrl.decode
                    };
                    $scope.cancel = function () {
                        ngDialog.closeAll(false);
                    };
                    $scope.save = function () {
                        if ($scope.form.file.$valid && $scope.ctrl.file) {
                            Upload.upload({
                                url: 'rest/droolsPricing/upload',
                                data: {
                                    file: $scope.ctrl.file,
                                    idDsp: $scope.ctrl.idDsp,
                                    idUtilizationType: $scope.ctrl.idUtilizationType,
                                    validFrom: $scope.ctrl.validFrom,
                                    validTo: $scope.ctrl.validTo || ""
                                }
                            }).then(function (resp) {
                                ngDialog.closeAll(true);
                            }, function (resp) {
                                ngDialog.closeAll(false);
                                scope.showError(null, resp.data);
                            }, function (evt) {
                                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                                logmsg('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                            });
                        }
                    };
                }]
            }).closePromise.then(function (data) {
                if (data.value === true) {
                    $route.reload();
                }
            });
        };

        $scope.showError = function (event, msgs) {
            ngDialog.open({
                template: 'pages/drools/dialog-error.html',
                plain: false,
                width: '60%',
                controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
                    $scope.ctrl = {
                        msgs: msgs
                    };
                    $scope.cancel = function () {
                        ngDialog.closeAll(false);
                    };
                    $scope.save = function () {
                        ngDialog.closeAll(true);
                    };
                }]
            }).closePromise.then(function (data) {
                // no op
            });
        };

        $scope.showEditData = function (event, configToModify) {
            ngDialog.open({
                template: 'pages/drools/dialog-edit.html',
                plain: false,
                width: '60%',
                scope: $scope,
                controller: ['$scope', 'ngDialog', '$http', function (scope, dialog, http) {
                    scope.cancel = function () {
                        dialog.closeAll(false);
                    };


                    scope.editConfig = function () {
                        http({
                            method: 'PUT',
                            url: 'rest/droolsPricing/' + configToModify.id,
                            params: scope.params
                        }).then(function successCallback(response) {
                            dialog.closeAll(true);
                            scope.onRicerca();
                        }, function errorCallback(response) {
                            ngDialog.closeAll(false);
                            scope.showError(null, response.data);
                        });
                    };
                }]
            });
        };

        $scope.showDelete = function (event, item) {
            var scope = $scope;
            ngDialog.open({
                template: 'pages/drools/dialog-delete.html',
                plain: false,
                width: '60%',
                controller: ['$scope', 'ngDialog', '$http', function ($scope, ngDialog, $http) {
                    $scope.ctrl = {
                        id: item.id,
                        idDsp: item.idDsp,
                        idUtilizationType: item.idUtilizationType,
                        validFrom: item.validFrom,
                        validTo: item.validTo,
                        xlsS3Url: item.xlsS3Url,
                        decode: scope.ctrl.decode
                    };
                    $scope.cancel = function () {
                        ngDialog.closeAll(false);
                    };
                    $scope.save = function () {
                        $http({
                            method: 'DELETE',
                            url: 'rest/droolsPricing/delete/' + $scope.ctrl.id
                        }).then(function successCallback(response) {
                            ngDialog.closeAll(true);
                        }, function errorCallback(response) {
                            ngDialog.closeAll(false);
                        });
                    };
                }]
            }).closePromise.then(function (data) {
                if (data.value === true) {
                    $route.reload();
                }
            });
        };

        $scope.showDetails = function (event, item) {
            var scope = $scope;
            ngDialog.open({
                template: 'pages/drools/dialog-details.html',
                plain: false,
                width: '60%',
                controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
                    $scope.ctrl = {
                        id: item.id,
                        idDsp: item.idDsp,
                        idUtilizationType: item.idUtilizationType,
                        validFrom: item.validFrom,
                        validTo: item.validTo,
                        xlsS3Url: item.xlsS3Url,
                        decode: scope.ctrl.decode
                    };
                    $scope.cancel = function () {
                        ngDialog.closeAll(false);
                    };
                    $scope.save = function () {
                        ngDialog.closeAll(true);
                    };
                }]
            }).closePromise.then(function (data) {
                if (data.value === true) {
                    // no op
                }
            });
        };
        pricingService.getIdDsp()
            .then((idDspList) => {
                $scope.ctrl.decode.idDsp = idDspList;
            }, (response) => {
                $scope.ctrl.decode.idDsp = {};
            });

        pricingService.getIdUtilizationType()
            .then(response => {
                $scope.ctrl.decode.idUtilizationType = response;
            }, _error => {
                $scope.ctrl.decode.idUtilizationType = {};
            });

        $scope.onRicercaEx($scope.ctrl.first, $scope.ctrl.last);
    }]);

// })();

