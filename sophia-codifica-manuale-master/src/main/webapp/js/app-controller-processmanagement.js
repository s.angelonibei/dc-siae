/**
 * DspConfigCtrl
 * 
 * @path /dspConfig
 */
codmanApp.controller('ProcessManagementCtrl', ['$scope','$route', 'ngDialog', '$routeParams', '$location', '$http',
                                               function($scope,$route, ngDialog, $routeParams, $location, $http) {

	$scope.ctrl = {
		navbarTab: 'processManagement',
		processList: {},
		filterParameters: [],
		execute: $routeParams.execute,
		hideForm: $routeParams.hideForm,
		maxrows: 20,
		first: $routeParams.first||0,
		last: $routeParams.last||20
	};		
	
			
	$scope.onAzzera = function() {
		$scope.ctrl.execute = false;
		$scope.ctrl.first = 0;
		$scope.ctrl.last = $scope.ctrl.maxrows;
		};
		
	$scope.navigateToNextPage = function() {
		$location.search('execute', 'true')
		.search('hideForm', $scope.ctrl.hideForm)
		.search('first', $scope.ctrl.last)
		.search('last', $scope.ctrl.last + $scope.ctrl.maxrows);
	};
	
	$scope.navigateToPreviousPage = function() {
		$location.search('execute', 'true')
			.search('hideForm', $scope.ctrl.hideForm)
			.search('first', Math.max($scope.ctrl.first - $scope.ctrl.maxrows, 0))
			.search('last', $scope.ctrl.first);
	};
	

	// Funzione principale richiamata al caricamento della pagina
	$scope.onRefresh = function() {
		// get All DSP Configuration

		if ($scope.ctrl.execute) {
			$scope.getPagedResults($scope.ctrl.first,
					$scope.ctrl.last);
		} else {
			$scope.getPagedResults(0,
					$scope.ctrl.maxrows);
		}
	
	};
	
	
	$scope.getPagedResults = function(first, last) {
		$http({
			method: 'GET',
			url: 'rest/dsrConfig/allManagedProcesses',
			params: {
				first: first,
				last: last
			}
		}).then(function successCallback(response) {
			$scope.ctrl.processList = response.data;
			$scope.ctrl.first = response.data.first;
			$scope.ctrl.last = response.data.last;
		}, function errorCallback(response) {
			$scope.ctrl.processList = {};
		});

	};
	

	
	$scope.showNewProcessConfig = function(event) {
		ngDialog.open({
			template: 'pages/process-management/process-management-new.html',
			plain: false,
			width: '60%',
			controller: ['$scope', 'ngDialog', '$http', function($scope, ngDialog, $http) {
				$scope.ctrl = {
						range : ['No',1,2,3,4]
				};
				
				$scope.idSongStep = $scope.ctrl.range[0];	
				$scope.iswcStep = $scope.ctrl.range[0];	 
				$scope.isrcStep = $scope.ctrl.range[0];	 
				$scope.titoloAutoriStep = $scope.ctrl.range[0];	 
			
				
			    $scope.openErrorDialog = function (errorMsg) {
			        ngDialog.open({ 
			        	template: 'errorDialogId',
			        	plain: false,
						width: '40%',
			        	data: errorMsg
			        	
			        });
			    };
			    
			    $scope.selectedItemChanged = function(caller) {

					switch (caller) {
					case "idSong":
						$scope.idSongStep != 'No' ?	$scope.idSongSwitch = true : $scope.idSongSwitch = false;
						break;
					case "iswc":
						$scope.iswcStep != 'No' ?	$scope.iswcSwitch = true : $scope.iswcSwitch = false;
						break;
					case "isrc":
						$scope.isrcStep != 'No' ?	$scope.isrcSwitch = true : $scope.isrcSwitch = false;
						break;
					case "titoloAutori":
						$scope.titoloAutoriStep != 'No' ?	$scope.titoloAutoriSwitch = true : $scope.titoloAutoriSwitch = false;
						break;
					default:
						console.log("Not valid selection " + expr);
					}
				}; 
				
				$scope.onSwichChanged = function(caller){
					switch (caller) {
					case "idSong":
						$scope.idSongSwitch == false ? $scope.idSongStep = 'No' : $scope.idSongStep = 1;
						break;
					case "iswc":
						$scope.iswcSwitch == false ? $scope.iswcStep = 'No' : $scope.iswcStep = 1;
						break;
					case "isrc":
						$scope.isrcSwitch == false ? $scope.isrcStep = 'No' : $scope.isrcStep = 1;
						break;
					case "titoloAutori":
						$scope.titoloAutoriSwitch == false ? $scope.titoloAutoriStep = 'No' : $scope.titoloAutoriStep = 1; 
						break;
					default:
						console.log("Not valid selection " + expr);
					}
				};
				
				$scope.noneSelected = function(){
					var check = false;
					$scope.idSongStep == 'No' && $scope.iswcStep == 'No'
						&& $scope.isrcStep == 'No' && $scope.titoloAutoriStep == 'No' ? check = true : check = false;
				return check;
				};
				
				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				
				function ProcessConfiguration(steps){
					this.ISRC = steps["ISRC"]
					this.ISWC = steps["ISWC"];
					this.IDSONG = steps["IDSONG"];
					this.TIT_AUT = steps["TITAUT"];
				};			
				$scope.save = function() {
					// selezione valori impostati dall'utente
					var values = [];
					var x = 0;
					if($scope.idSongStep  != 'No')
						values[x++] = {name: "IDSONG", position: $scope.idSongStep};
					if($scope.iswcStep != 'No')
						values[x++] ={name : "ISWC", position: $scope.iswcStep};
					if($scope.isrcStep != 'No')
						values[x++] = {name: "ISRC", position: $scope.isrcStep};
					if($scope.titoloAutoriStep != 'No')
						values[x++] = {name: "TITAUT", position: $scope.titoloAutoriStep};
					values.sort(function(a, b){return a.position - b.position});	
					
					
					//controlli di validita'
					if(values[0].position != 1){
						$scope.openErrorDialog("Configurazione non valida. Gli step devono partire da 1");
						return;
					}
					var validConfiguration = true;
					for (i = 0 ; i < values.length-1; i++) { 
					    if((values[i+1].position - values[i].position) != 1 ){
					    	validConfiguration = false;
					    	break;
					    }
					}
					if(!validConfiguration){
						$scope.openErrorDialog("Configurazione non valida. Verificare ordine step");
						return;
					}
					
					//preparazione valori da inserire nel DB
					var id_dsr_process = "";
					var process_description = "";
					var map = [];
					var x;
					for(x in values){
						if(x == 0){
							id_dsr_process = id_dsr_process.concat(values[x].name);
							if(values[x].name === "TITAUT"){
								process_description = process_description.concat("Titolo Autori");
							}else{								
								process_description = process_description.concat(values[x].name);
							}
						}else{							
							id_dsr_process = id_dsr_process.concat("_" + values[x].name);
							if(values[x].name === "TITAUT"){
								process_description = process_description.concat(", Titolo Autori");
							}else{								
								process_description = process_description.concat(", " + values[x].name);
							}
						}
						map[values[x].name] = values[x].position;
					}
					var stepNames = ["ISRC","ISWC","IDSONG", "TITAUT"];
					var maxStepSetted = values[values.length - 1].position;
					
					var y;
					var configuration = [];
					for(y in stepNames){
						
						var priority;
						var status;
						if(map[stepNames[y]]){
							priority = map[stepNames[y]];
							status = true;
						}else{
							priority = ++maxStepSetted;
							status = false;
						}
						
						configuration[stepNames[y]] = {
								weight: priority,
								enabled:status
						};
						
					}
					
					var processConfiguration = new ProcessConfiguration(configuration);
					
					
					$http({
						method: 'POST',
						url: 'rest/dsrConfig/allManagedProcesses',
						data: {
							idDsrProcess: id_dsr_process ,
							name: $scope.processName,
							description: process_description ,
							configuration: JSON.stringify(processConfiguration) 
						}
					}).then(function successCallback(response) {
						ngDialog.closeAll(true);
						$route.reload();
					}, function errorCallback(response) {
						if(response.status == 409){
							$scope.openErrorDialog("Configurazione già presente.");	
						}else{
							$scope.openErrorDialog(response.statusText);	
						}
						
					});
				};
				
				
				
				
			}]
		}).closePromise.then(function (data) {
			if (data.value === true) {
				// no op
			}
		});
	};
	
	$scope.showInfo = function(config){
		ngDialog.open({
			template: 'pages/process-management/process-management-info.html',
			plain: false,
			width: '60%',
			controller: ['$scope', 'ngDialog', '$http', function($scope, ngDialog, $http) {
				$scope.ctrl = {
						config: config
				};	
				
				
				$scope.close = function() {
					ngDialog.closeAll(false);
				};

			}]
		}).closePromise.then(function (data) {
			if (data.value === true) {
				// no op
			}
		});
	

	}
	
	
	
	
	
	
	$scope.showDeleteConfig = function(event, config) {
		ngDialog.open({
			template: 'pages/process-management/process-management-delete.html',
			plain: false,
			width: '60%',
			controller: ['$scope', 'ngDialog', '$http', function($scope, ngDialog, $http) {
				$scope.ctrl = {
					config: config
				};
				

				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				

				$scope.deleteConfig = function() {
					$http({
						method: 'DELETE',
						url: 'rest/dsrConfig/allManagedProcesses',
						headers: {
				            'Content-Type': 'application/json'},
						params: {
							idDsrProcess: config.idDsrProcess						
						}
					
					}).then(function successCallback(response) {
						ngDialog.closeAll(true);
						$route.reload();
					}, function errorCallback(response) {
						if(response.status == 409){
							$scope.openErrorDialog("Si sta eliminando un processo  in uso da una configurazione DSR: eliminare prima la configurazione. ");	
						}else{
							$scope.openErrorDialog(response.statusText);	
						}
						
					});
					
				};
				
			    $scope.openErrorDialog = function (errorMsg) {
			        ngDialog.open({ 
			        	template: 'errorDialogId',
			        	plain: false,
						width: '40%',
			        	data: errorMsg
			        	
			        });
			    };
			}]
		}).closePromise.then(function (data) {
			if (data.value === true) {
				// no op
			}
		});
	};
	
		

								 
	 	$scope.onRefresh();
	

	
}]);




