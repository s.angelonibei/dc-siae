codmanApp.controller('MandatoConfigCtrl', ['$scope', '$route', 'ngDialog', '$routeParams', '$location', '$http', 'societaTutela', 'mandati', 'mandatiService', 'configurazioneMandatoService', 'configurazioniMandato', '$filter', function ($scope, $route, ngDialog, $routeParams, $location, $http, societaTutela, mandati, mandatiService, configurazioneMandatoService, configurazioniMandato, $filter) {

	$scope.ctrl = {
		societaTutela: societaTutela.data,
		mandati: mandati.data.filter( m => m.mandante.codice !='SIAE'),
		configurazioniMandato: configurazioniMandato.data.filter( m => m.mandato.mandante.codice !='SIAE'),
		messages : {
			info: "",
			error: "",
			clear : function (){
				this.info = "";
				this.error = "";
			},
			infoMessage: function (message){
				this.info = message;
                $('#rightPanel')[0].scrollIntoView( true );
                window.scrollBy(0, -100);				
			},
			errorMessage: function (message){
				this.error = message;
				$('#rightPanel')[0].scrollIntoView( true );
                window.scrollBy(0, -100);
			}
		}
	};

	$scope.nuovaSocietaTutela = function () {
		var societaTutela = $scope.ctrl.societaTutela;
		ngDialog.open({
			template: 'pages/mandato-config/dialog-edit-societa-tutela.html',
			plain: false,
			width: '60%',
			controller: ['$scope', 'ngDialog', 'countryListService', 'societaTutelaService', function ($scope, ngDialog, countryListService, societaTutelaService) {

				$scope.ctrl = {};

				countryListService.getCountriesList().then(function success(response) {
					$scope.ctrl.countries = response.data;
				}, function error(error) {
					console.error('errror', error);
					$scope.ctrl.countries = [];
				});

				$scope.cancel = function () {
					ngDialog.closeAll(false);
				};

				$scope.save = function () {
					societaTutelaService.salvaSocieta(
						{
							nominativo: $scope.ctrl.nominativo,
							codice: $scope.ctrl.codice,
							homeTerritory: $scope.ctrl.homeTerritory,
							dataInizioMandato: $scope.ctrl.dataInizioMandato,
							dataFineMandato: $scope.ctrl.dataFineMandato,
							posizioneSIAE: $scope.ctrl.posizioneSIAE,
							codiceSIADA: $scope.ctrl.codiceSIADA
						}
						).then(function success(response) {
						societaTutela.push(response.data);
						ngDialog.closeAll(true);
					}, function error(error) {
						if (error.status == 400) {
							console.error('Errore di Validazione');
						} else {
							console.error('Mostrare errore di Salvataggio');
						}
					});
				};
			}]
		});
	};

	$scope.editMandato = function (mandato) {
		var societaTutela = $scope.ctrl.societaTutela;
		var mandati = $scope.ctrl.mandati;
		ngDialog.open({
			template: 'pages/mandato-config/dialog-edit-mandato.html',
			plain: false,
			width: '60%',
			controller: ['$scope', 'ngDialog', 'mandatiService', function ($scope, ngDialog, mandatiService) {

				if (mandato) {
					$scope.mode = 'edit';
					$scope.id = mandato.id;
					$scope.mandataria = mandato.mandataria;
					$scope.mandante = mandato.mandante;
					$scope.dataInizioMandato = $filter('date')(mandato.dataInizioMandato,'dd-MM-yyyy');
					$scope.dataFineMandato = $filter('date')(mandato.dataFineMandato,'dd-MM-yyyy');
					$scope.mandatarie = [$scope.mandataria];
					$scope.mandanti = [$scope.mandante];
				} else {
					$scope.mode = 'new';
					$scope.mandatarie = societaTutela.filter(el => el.tenant == '1');
					$scope.mandanti = societaTutela.filter(el => el.tenant != '1');
				}

				$scope.cancel = function () {
					ngDialog.closeAll(false);
				};

				$scope.save = function () {
					mandatiService.salvaMandato(
						{
							id: $scope.id,
							mandataria: $scope.mandataria,
							mandante: $scope.mandante,
							dataInizioMandato: moment($scope.dataInizioMandato, 'DD-MM-YYYY').format('YYYY-MM-DD'),
							dataFineMandato: moment($scope.dataFineMandato, 'DD-MM-YYYY').format('YYYY-MM-DD')
						}
						).then(function success(response) {
						if($scope.mode == 'new'){
							mandati.push(response.data);
						}else {
							var index = mandati.findIndex(el => el.id === $scope.id);
							mandati.splice(index, 1, response.data);
						}
						ngDialog.closeAll(true);
					}, function error(error) {
						if (error.status == 400) {
							console.error('Errore di Validazione');
						} else {
							console.error('Mostrare errore di Salvataggio');
						}
					});
				};
			}]
		});
	};

	$scope.eliminaMandato = function (mandato){
		$scope.ctrl.messages.clear();
        ngDialog.openConfirm({
			template: 'pages/mandato-config/dialog-alert.html',
			plain: false,
			data: {
				mandato: mandato
			},
			controller:  function () {
				var ctrl = this;
				ctrl.title= "Elimina Mandato";
				ctrl.message= `Confermi l'eliminazione della configurazione?`;
			},
			controllerAs: 'ctrl'
        }).then(function confirm(data) {
			mandatiService.elminaMandato(data.mandato).then(
				function success(response){
					var index = $scope.ctrl.mandati.findIndex(el => el.id === data.mandato.id);
					$scope.ctrl.mandati.splice(index,1);
				},
				function error(response){
					$scope.ctrl.messages.errorMessage('Eliminazione non riuscita. Esistono configurazioni attive per questo Mandato?');
				}
			);
        }, function cancel() {
          //no op
        });
	};

	$scope.editConfigurazioneMandato = function (configurazioneMandato) {
		var mandati = $scope.ctrl.mandati;
		var configurazioniMandato = $scope.ctrl.configurazioniMandato;
		ngDialog.open({
			template: 'pages/mandato-config/dialog-edit-configurazione-mandato.html',
			plain: false,
			width: '60%',
			controller: ['$scope', 'ngDialog', 'configurazioneMandatoService', function ($scope, ngDialog, configurazioneMandatoService) {
				$scope.ctrl = {};

				if (configurazioneMandato) {
					$scope.mode = 'edit';
					$scope.id = configurazioneMandato.id;
					$scope.selectedMandato= configurazioneMandato.mandato;
					$scope.selectedDsp = configurazioneMandato.dsp;
					$scope.selectedUtilization = configurazioneMandato.utilization;
					$scope.selectedOffer = configurazioneMandato.offer;
					$scope.selectedCountry = configurazioneMandato.country;
					$scope.ctrl.mandati= [configurazioneMandato.mandato];
					$scope.validaDa = $filter('date')(configurazioneMandato.validaDa,'dd-MM-yyyy');
					$scope.validaA = $filter('date')(configurazioneMandato.validaA,'dd-MM-yyyy');
				} else {
					$scope.mode = 'new';
					$scope.ctrl.mandati= mandati;
				}

				$http({method: 'GET', url: 'rest/data/dspUtilizationsOffersCountries'})
					.then(function success(response) {
						$scope.ctrl.dspList = response.data.dsp;
						$scope.ctrl.dspList.splice(0, 0, { idDsp: '*', name: '*' });
						$scope.ctrl.utilizationList = response.data.utilizations;
						$scope.ctrl.utilizationList.splice(0, 0, { idUtilizationType: '*', name: '*' });
						$scope.ctrl.commercialOffersList = response.data.commercialOffers;
						$scope.ctrl.commercialOffersList.splice(0, 0, { idCommercialOffering: -1, offering: '*' });
						$scope.ctrl.countries = response.data.countries;
						$scope.ctrl.countries.splice(0, 0, { idCountry: '*', name: '*' });
						if($scope.selectedMandato){
							$scope.selectMandato();		
						}
					}, function error(response) {
						$scope.ctrl.dspList = [];
						$scope.ctrl.utilizationList = [];
						$scope.ctrl.commercialOffersList = [];
						$scope.ctrl.countries = [];
						if($scope.selectedMandato){
							$scope.selectMandato();		
						}
					}); 

					$scope.selectMandato = function () {
						$scope.validaDa = $filter('date')($scope.selectedMandato.dataInizioMandato, 'dd-MM-yyyy');
						$scope.validaA = $filter('date')($scope.selectedMandato.dataFineMandato, 'dd-MM-yyyy');
						$(".ctrl_validFrom").datepicker("destroy");
						$(".ctrl_validFrom").datepicker({
							format: 'dd-mm-yyyy',
							language: 'it',
							autoclose: true,
							todayHighlight: true,
							startDate: $filter('date')($scope.selectedMandato.dataInizioMandato, 'dd-MM-yyyy'),
							endDate: $filter('date')($scope.selectedMandato.dataFineMandato, 'dd-MM-yyyy')
						});
						$(".ctrl_validTo").datepicker("destroy");
						$(".ctrl_validTo").datepicker({
							format: 'dd-mm-yyyy',
							language: 'it',
							autoclose: true,
							todayHighlight: true,
							startDate: $filter('date')($scope.selectedMandato.dataInizioMandato, 'dd-MM-yyyy'),
							endDate: $filter('date')($scope.selectedMandato.dataFineMandato, 'dd-MM-yyyy')
						});
					};
					
				

				$scope.changeDataInizioValidita = function (){
					if(!$scope.validaDa){
						$scope.validaDa = $filter('date')($scope.selectedMandato.dataInizioMandato, 'dd-MM-yyyy');
					}
					if($scope.validaDa &&
						moment($scope.validaDa, 'DD-MM-YYYY') < moment($filter('date')($scope.selectedMandato.dataInizioMandato, 'dd-MM-yyyy'), 'DD-MM-YYYY')){
						$scope.validaDa = $filter('date')($scope.selectedMandato.dataInizioMandato, 'dd-MM-yyyy');
					}
				};

				$scope.changeDataFineValidita = function (){					
					if(!$scope.validaA){
						$scope.validaA = $filter('date')($scope.selectedMandato.dataFineMandato, 'dd-MM-yyyy');
					}
					if($scope.validaA &&
						moment($scope.validaA, 'DD-MM-YYYY') > moment($filter('date')($scope.selectedMandato.dataFineMandato, 'dd-MM-yyyy'), 'DD-MM-YYYY')){
						$scope.validaA = $filter('date')($scope.selectedMandato.dataFineMandato, 'dd-MM-yyyy');
					}
				};				
					
				$scope.filterUtilizationByDsp = function () {
					return function (item) {
						if ((typeof $scope.selectedDsp === 'undefined'))
							return false;
						if ($scope.selectedDsp.idDsp === '*') {
							return true;
						}
						for (var i = 0; i < $scope.ctrl.commercialOffersList.length; i++) {
							if (($scope.ctrl.commercialOffersList[i].idDSP == $scope.selectedDsp.idDsp &&
								item.idUtilizationType == $scope.ctrl.commercialOffersList[i].idUtilizationType) || item.idUtilizationType == '*') {
								return true;
							}
						}
						return false;
					};
				};

				$scope.filterOffersByDspAndUtilization = function () {
					return function (item) {
						if ((typeof $scope.selectedDsp === 'undefined') || (typeof $scope.selectedUtilization === 'undefined'))
							return false;

						if ($scope.selectedDsp.idDsp === '*' && item.idCommercialOffering != -1) {
							return false;
						} else {
							if ((item.idDSP == $scope.selectedDsp.idDsp && item.idUtilizationType == $scope.selectedUtilization.idUtilizationType) || item.idCommercialOffering == -1) {
								return true;
							} else {
								return false;
							}
						}
					};
				};					

				$scope.cancel = function () {
					ngDialog.closeAll(false);
				};

				$scope.save = function () {
					configurazioneMandatoService.salvaConfigurazioneMandato(
						{
							id: $scope.id,
							mandato: $scope.selectedMandato,
							dsp: $scope.selectedDsp,
							utilization: $scope.selectedUtilization,
							offer: $scope.selectedOffer,
							country: $scope.selectedCountry,
							validaDa: moment($scope.validaDa, 'DD-MM-YYYY').format('YYYY-MM-DD'),
							validaA: moment($scope.validaA, 'DD-MM-YYYY').format('YYYY-MM-DD')							
						}
						).then(function success(response) {
						if($scope.mode == 'new'){
							configurazioniMandato.push(response.data);
						}else {
							var index = configurazioniMandato.findIndex(el => el.id === $scope.id);
							configurazioniMandato.splice(index, 1, response.data);
						}
						ngDialog.closeAll(true);
					}, function error(error) {
						if (error.status == 400) {
							console.error('Errore di Validazione');
						} else {
							console.error('Mostrare errore di Salvataggio');
						}
					});
				};
			}]
		});
	};

	$scope.eliminaConfigurazioneMandato = function (configurazioneMandato){
		$scope.ctrl.messages.clear();
        ngDialog.openConfirm({
			template: 'pages/mandato-config/dialog-alert.html',
			plain: false,
			data: {
				configurazioneMandato: configurazioneMandato
			},
			controller:  function () {
				var ctrl = this;
				ctrl.title= "Elimina Mandato";
				ctrl.message= `Confermi l'eliminazione della configurazione?`;
			},
			controllerAs: 'ctrl'
        }).then(function confirm(data) {
			configurazioneMandatoService.elminaConfigurazioneMandato(data.configurazioneMandato).then(
				function success(response){
					var index = $scope.ctrl.configurazioniMandato.findIndex(el => el.id === data.configurazioneMandato.id);
					$scope.ctrl.configurazioniMandato.splice(index,1);
				},
				function error(repsonse){
					$scope.ctrl.messages.errorMessage('Operazione non riuscita');
				}
			);
        }, function cancel() {
          //no op
        });
	};

	$scope.goToBlackList = function (societa) {
		if (societa && societa.codice) {
			$location.path('/blacklist').search({ codiceSocietaTutela: societa.codice });
		} else {
			$location.path('/blacklist');
		}
	};

	$scope.goToDocumentazione = function (societa) {
		if (societa && societa.codice) {
			$location.path('/documentazioneMandanti').search({ codiceSocietaTutela: societa.codice });
		} else {
			$location.path('/documentazioneMandanti');
		}
	};

}]);

codmanApp.service('countryListService', ['$http', function ($http) {

	var service = this;

	service.getCountriesList = function () {
		return $http({
			method: 'GET',
			url: 'rest/data/countries'
		});
	};
}
]);

codmanApp.service('societaTutelaService', ['$http', '$q', function ($http, $q) {

	var service = this;

	service.salvaSocieta = function (societa) {
		return $http({
			method: 'POST',
			url: 'rest/societa-tutela',
			data: societa
		});
	};

	service.getAll = function () {
		return $http({
			method: 'GET',
			url: 'rest/societa-tutela'
		});
	};
}
]);

codmanApp.service('mandatiService', ['$http', '$q', function ($http, $q) {

	var service = this;

	service.salvaMandato = function (mandato) {
		return $http({
			method: 'POST',
			url: 'rest/mandato',
			data: mandato
		});
	};

	service.elminaMandato = function (mandato) {
		return $http({
			method: 'DELETE',
			url: 'rest/mandato',
			data: mandato,
			headers: {'Content-Type': 'application/json'}
		});
	};	

	service.getAll = function () {
		return $http({
			method: 'GET',
			url: 'rest/mandato'
		});
	};	
}
]);


codmanApp.service('configurazioneMandatoService', ['$http', '$q', function ($http, $q) {

	var service = this;

	service.salvaConfigurazioneMandato = function (mandato) {
		return $http({
			method: 'POST',
			url: 'rest/mandato-configurazione',
			data: mandato
		});
	};

	service.elminaConfigurazioneMandato = function (mandato) {
		return $http({
			method: 'DELETE',
			url: 'rest/mandato-configurazione',
			data: mandato,
			headers: {'Content-Type': 'application/json'}
		});
	};	

	service.getAll = function () {
		return $http({
			method: 'GET',
			url: 'rest/mandato-configurazione'
		});
	};	
}
]);