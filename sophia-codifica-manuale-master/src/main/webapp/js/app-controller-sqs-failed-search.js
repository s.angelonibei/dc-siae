//(function(){
	
	/**
	 * SqsFailedSearchCtrl
	 * 
	 * @path /sqsFailedSearch
	 */
	codmanApp.controller('SqsFailedSearchCtrl', ['$scope', 'ngDialog', '$routeParams', '$location', '$http', 'configService',
	                                             function($scope, ngDialog, $routeParams, $location, $http, configService) {

		yyyymmdd = function(date, sep) {
			if (date) {
				var dt = new Date(date);
				var mm = dt.getMonth() + 1;
				var dd = dt.getDate();
				return [dt.getFullYear(),
				        (mm>9 ? '' : '0') + mm,
				        (dd>9 ? '' : '0') + dd
				        ].join(sep);
			}
		};
			
		$scope.ctrl = {
			navbarTab: 'sqsFailedSearch',
			execute: true, // $routeParams.execute,
			hideForm: $routeParams.hideForm,
			maxrows: configService.maxRowsPerPage,
			first: $routeParams.first||0,
			last: $routeParams.last||configService.maxRowsPerPage,
			receiveDate: yyyymmdd($routeParams.receiveDate, '-'),
			queueName: $routeParams.queueName,
			json: $routeParams.json,
			sortType: 'receiveTime',
			sortReverse: true,
			searchResults: {},
			decode: {
				queueNames: []
			}
		};
				
		$scope.onAzzera = function() {
			$scope.ctrl.execute = true; // false;
			$scope.ctrl.first = 0;
			$scope.ctrl.last = $scope.ctrl.maxrows;
			$scope.ctrl.receiveDate = null;
			$scope.ctrl.queueName = null;
			$scope.ctrl.json = null;
		};

		$scope.setSortType = function(sortType) {
			$scope.ctrl.sortType = sortType;
			$scope.ctrl.sortReverse = !$scope.ctrl.sortReverse;
			$scope.onRicerca();
		};

		$scope.onRicerca = function() {
			$scope.onRicercaEx(0, $scope.ctrl.maxrows);
		};
		
		$scope.onRicercaEx = function(first, last) {
			$http({
				method: 'GET',
				url: 'rest/sqs-failed/search',
				params: {
					first: first,
					last: last,
					sortType: $scope.ctrl.sortType,
					sortReverse: $scope.ctrl.sortReverse,
					receiveDate: $scope.ctrl.receiveDate,
					queueName: $scope.ctrl.queueName,
					json: $scope.ctrl.json
				}
			}).then(function successCallback(response) {
				$scope.ctrl.searchResults = response.data;
				$scope.ctrl.first = response.data.first;
				$scope.ctrl.last = response.data.last;
				// string to JSON object
				for (i = 0, len = $scope.ctrl.searchResults.rows.length; i < len; i ++) {
					var row = $scope.ctrl.searchResults.rows[i];
					try {
						row.json = JSON.parse(row.messageJson);
						row.jsonError = null;
					} catch(err) {
						row.json = null;
						row.jsonError = err.name + ': ' + err.message;
					}
				}
			}, function errorCallback(response) {
				$scope.ctrl.searchResults = {};
			});
		};
		
		$scope.navigateToNextPage = function() {
			$location
				.search('execute', 'true')
				.search('hideForm', $scope.ctrl.hideForm)
				.search('first', $scope.ctrl.last)
				.search('last', $scope.ctrl.last + $scope.ctrl.maxrows)
				.search('sortType', $scope.ctrl.sortType||null)
				.search('sortReverse', $scope.ctrl.sortReverse||null)
				.search('receiveDate', $scope.ctrl.receiveDate||null)
				.search('queueName', $scope.ctrl.queueName||null)
				.search('json', $scope.ctrl.json||null);
		};

		$scope.navigateToPreviousPage = function() {
			$location
				.search('execute', 'true')
				.search('hideForm', $scope.ctrl.hideForm)
				.search('first', Math.max($scope.ctrl.first - $scope.ctrl.maxrows, 0))
				.search('last', $scope.ctrl.first)
				.search('sortType', $scope.ctrl.sortType||null)
				.search('sortReverse', $scope.ctrl.sortReverse||null)
				.search('receiveDate', $scope.ctrl.receiveDate||null)
				.search('queueName', $scope.ctrl.queueName||null)
				.search('json', $scope.ctrl.json||null);
		};
		
		$scope.showAlert = function(title, message) {
			ngDialog.open({
				template: 'pages/sqs-failed/dialog-alert.html',
				plain: false,
				width: '50%',
				controller: ['$scope', 'ngDialog', function($scope, ngDialog) {
					$scope.ctrl = {
						title: title,
						message: message
					};
					$scope.cancel = function() {
						ngDialog.closeAll(false);
					};
					$scope.save = function() {
						ngDialog.closeAll(true);
					};
				}]
			}).closePromise.then(function (data) {
				if (data.value === true) {
					
				}
			});
		};
		
		$scope.showDetails = function(event, sqsFailed) {
			ngDialog.open({
				template: 'pages/sqs-failed/dialog-details.html',
				plain: false,
				width: '70%',
				controller: ['$scope', 'ngDialog', function($scope, ngDialog) {
					$scope.ctrl = {
						sqsFailed: sqsFailed
					};
					$scope.cancel = function() {
						ngDialog.closeAll(false);
					};
					$scope.save = function() {
						ngDialog.closeAll(true);
					};
				}]
			}).closePromise.then(function (data) {
				if (data.value === true) {
					
				}
			});
		};
		
		$scope.showSendMessage = function(event, sqsFailed) {
			ngDialog.open({
				template: 'pages/sqs-failed/dialog-send-message.html',
				plain: false,
				width: '70%',
				controller: ['$scope', 'ngDialog', '$http', function($scope, ngDialog, $http) {
					$scope.ctrl = {
						sqsFailed: sqsFailed,
						force: false
					};
					$scope.cancel = function() {
						ngDialog.closeAll(false);
					};
					$scope.save = function() {
						$http({
							method: 'POST',
							url: 'rest/sqs-failed/' + ($scope.ctrl.force ? 'force-send' : 'send-again'),
							data: $scope.ctrl.sqsFailed
						}).then(function successCallback(response) {
							ngDialog.closeAll(true);
						}, function errorCallback(response) {
							ngDialog.closeAll(false);
						});
					};
				}]
			}).closePromise.then(function (data) {
				if (data.value === true) {
					$scope.showAlert('Messaggio inviato', 'Messaggio inviato correttamente');
				}
			});
		};
		
		$http({
			method: 'GET',
			url: 'rest/sqs-failed/queue-names'
		}).then(function successCallback(response) {
			$scope.ctrl.decode.queueNames = response.data;
		}, function errorCallback(response) {
			$scope.ctrl.decode.queueNames = [];
		});
		
		if ($scope.ctrl.execute) {
			$scope.onRicercaEx($scope.ctrl.first, $scope.ctrl.last);
		}

	}]);
	
//})();

