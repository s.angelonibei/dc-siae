codmanApp.controller('gestioneTriplettaCtrl', ['$scope', '$routeParams', '$route', '$http', '$location', 'ngDialog', '$rootScope', '$anchorScroll', 'configService', 'triplettaContext', 
	function ($scope, $routeParams, $route, $http, $location, ngDialog, $rootScope, $anchorScroll, configService, triplettaContext) {

		$scope.ctrl = {
			msIngestionApiUrl: `${$location.protocol()}://${$location.host()}:${$location.port()}/${configService.msIngestionApiPath}`,
			tipiEmittenti: [{
				'nome': 'TUTTI'
			}, {
				'nome': 'TELEVISIONE'
			}, {
				'nome': 'RADIO'
			}],
			tipiDiritto: ['DEM','DRM'],
			tipiRifTemporale: ['Annuale - Solare', 'Annuale - Commerciale'],
			emittenti: [],
			canaleSelezionato: {},
			configs : {
				detail: {
					title: 'Dettaglio Tripletta',
					readOnlyFields: ['descrizioneSap','chiaveRif3','conto', 'attribuzione','tipoDiritto','rifTemporaleIncassi',
					'giornoMeseDa','giornoMeseA','tipoEmittente','nomeEmittente','canale']
				},
				new: {
					title: 'Nuova Tripletta',
					readOnlyFields: []
				},
				error: {
					title: 'Nuova Tripletta',
					readOnlyFields: ['chiaveRif3','conto', 'attribuzione']
				},
				edit: {
					title: 'Modifica Tripletta',
					readOnlyFields: ['tipoEmittente','nomeEmittente']
				}
			}			
		};

		$scope.goTo = function (url, param) {
			if (url != undefined && param != undefined) {
				$location.path(url + param);
			}
		};

		$scope.goBack = function () {
			triplettaContext.mode = '';
			triplettaContext.sapData = '';
			$scope.goTo(triplettaContext.state.location,'');
		}

		$scope.showAlert = function (title, message, callback) {
			ngDialog.open({
				template: 'pages/advance-payment/dialog-alert.html',
				plain: false,
				width: '50%',
				controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
					var ngDialogId = $scope.ngDialogId;
					$scope.ctrl = {
						title: title,
						message: message,
						ngDialogId: ngDialogId
					};
					$scope.cancel = function (ngDialogId) {
						ngDialog.close(ngDialogId);
					};
				}]
			}).closePromise.then(function (data) {
				if(callback){
					callback();
				}
			});
		};

		$scope.getEmittenti = function () {
			$http({
				method: 'GET',
				url: $scope.ctrl.msIngestionApiUrl + 'broadcasters'
			}).then(function(response){
				$scope.ctrl.emittenti = response.data;
			},function(error){
				$scope.showAlert('Errore','Errore recupero emittenti');
			})
		}

		$scope.getCanali = function(emittente) {
			if (!emittente) {
				return;
			}
			$http({
				method : 'GET',
				url: `${$scope.ctrl.msIngestionApiUrl}broadcasters/${emittente.id}/canali`,
				params : {
	
				}
			}).then(function successCallback(response) {
				$scope.ctrl.listaCanali = response.data;
				$scope.ctrl.canale = {};
			}, function errorCallback(response) {
				$scope.ctrl.listaCanali = [];
				$scope.ctrl.canale = {};
			});
		};

		$scope.aggiungiCanale = function () {
			if ($scope.ctrl.canaleSelezionato.id) {
				let canale = {
					idCanale: $scope.ctrl.canaleSelezionato.id,
					nomeCanale: $scope.ctrl.canaleSelezionato.nome,
					attivoDal: moment().format('DD-MM-YYYY'),
					//preallocazionePercentuale : $scope.ctrl.tripletta.canali.length === 0 ? '100,00' : '0,00',
					attivoAl: ''
				};
				$scope.ctrl.tripletta.canali.push(canale);
				let singoloVsVasca = $scope.singoloVsVasca();
				$scope.ctrl.tripletta.canali.forEach(canale => {
					canale.singoloVsVasca = singoloVsVasca;
					}
				);
			}
		}

		$scope.eliminaCanale = function(canaleDaElimare) {
			let index = $scope.ctrl.tripletta.canali.indexOf(canaleDaElimare);
			if (index != -1) {
				$scope.ctrl.tripletta.canali.splice(index,1);
			}
			let singoloVsVasca = $scope.singoloVsVasca();
			$scope.ctrl.tripletta.canali.forEach(canale => {
				canale.singoloVsVasca = singoloVsVasca;
				}
			);			
		}

		$scope.showSaveButton = function() {
			if ($scope.ctrl.tripletta && $scope.ctrl.tripletta.canali) {
				return $scope.ctrl.mode != 'detail' && $scope.ctrl.tripletta.canali.length > 0;
			} 
			return false;
		}

		$scope.showEditButton = function() {
			if ($scope.ctrl.tripletta && $scope.ctrl.tripletta.canali) {
				return $scope.ctrl.mode == 'detail';
			} 
			return false;
		}

		$scope.selectPeriodoRiferimento = function() {
			if($scope.ctrl.tripletta.rifTemporaleIncassi === 'Annuale - Solare'){
				$scope.ctrl.tripletta.giornoMeseDa = '01-01';
				$scope.ctrl.tripletta.giornoMeseA = '31-12';
				$('#periodoDiRiferimentoDa').prop('readonly', true);
				$('#periodoDiRiferimentoA').prop('readonly', true);
				$('#' + 'periodoDiRiferimentoA'+'DP').datepicker("destroy");
				$('#' + 'periodoDiRiferimentoDa'+'DP').datepicker("destroy");
			}else {
				$('#periodoDiRiferimentoDa').prop('readonly', false);
				$('#periodoDiRiferimentoA').prop('readonly', false);
				$scope.initPeriodoRiferimento('periodoDiRiferimentoDa');
				$scope.initPeriodoRiferimento('periodoDiRiferimentoA');
			}
		}

		$scope.init = function() {

			if(triplettaContext.mode === 'new') {
				$scope.ctrl.mode = 'new';
				$scope.ctrl.tripletta = $scope.emptyTripletta();
				$scope.setupForUpdate();
			}			

			if(triplettaContext.mode === 'error') {
				$scope.ctrl.tripletta = $scope.emptyTripletta();
				$scope.ctrl.mode = 'error';
				$scope.ctrl.tripletta.chiaveRif3 = triplettaContext.sapData.chiaveRif3;
				$scope.ctrl.tripletta.conto = triplettaContext.sapData.conto;
				if (triplettaContext.sapData.attribuzione) {
					$scope.ctrl.tripletta.attribuzione = triplettaContext.sapData.attribuzione.split(' ')[0];
				}
				$scope.ctrl.tripletta.descrizioneSap = triplettaContext.sapData.testo;
				$scope.ricercaTripletta($scope.ctrl.tripletta.conto, $scope.ctrl.tripletta.chiaveRif3,$scope.ctrl.tripletta.attribuzione);
			}

			if(triplettaContext.mode === 'edit') {
				$scope.ctrl.mode = 'edit';
				if($scope.ctrl.tripletta.giornoMeseDa && $scope.ctrl.tripletta.giornoMeseDa.match(/([0-9]{2})/gi)) {
					$scope.ctrl.tripletta.giornoMeseDa = $scope.ctrl.tripletta.giornoMeseDa.match(/([0-9]{2})/gi).join('-');
				}
				if($scope.ctrl.tripletta.giornoMeseA && $scope.ctrl.tripletta.giornoMeseA.match(/([0-9]{2})/gi)) {
					$scope.ctrl.tripletta.giornoMeseA = $scope.ctrl.tripletta.giornoMeseA.match(/([0-9]{2})/gi).join('-');
				}
				if($scope.ctrl.tripletta.canali) {
					$scope.ctrl.tripletta.canali.forEach(canale => {
						let attivoDal = canale.attivoDal || '';
						canale.attivoDal = attivoDal.replace(/\//gi, '-');
						let attivoAl = canale.attivoAl || '';
						canale.attivoAl = attivoAl.replace(/\//gi, '-');
					});
				}
				$scope.setupForUpdate();
				$scope.selectPeriodoRiferimento();
				$scope.getCanali({id: $scope.ctrl.tripletta.idEmittente});
			}

			if(triplettaContext.mode === 'detail') {
				$scope.ctrl.mode = 'detail';
				if(triplettaContext.tripletta.id){
					$scope.dettaglioTripletta(triplettaContext.tripletta.id);
				}
			}
	
		}

		$scope.ricercaTripletta = function (conto, chiaveRif3, attribuzione) {

			$scope.params = {
				idEmittente: '',
				idCanale: '',
				tipoEmittente: '',
				tipoDiritto: '',
				stato: '',
				page: 0,
				count: false,
				order: 'tripletta asc',
				tripletta: `${conto}${chiaveRif3}${attribuzione}`,
				canali: true
			}

			$http({
				method: 'GET',
				url: $scope.ctrl.msIngestionApiUrl + 'triplette',
				params: $scope.params
			}).then(function (response) {
				if (response.data && response.data.list && response.data.list.length == 1) {
					$scope.ctrl.tripletta = response.data.list[0];
					if($scope.ctrl.tripletta.giornoMeseDa && $scope.ctrl.tripletta.giornoMeseDa.match(/([0-9]{2})/gi)) {
						$scope.ctrl.tripletta.giornoMeseDa = $scope.ctrl.tripletta.giornoMeseDa.match(/([0-9]{2})/gi).join('-');
					}
					if($scope.ctrl.tripletta.giornoMeseA && $scope.ctrl.tripletta.giornoMeseA.match(/([0-9]{2})/gi)) {
						$scope.ctrl.tripletta.giornoMeseA = $scope.ctrl.tripletta.giornoMeseA.match(/([0-9]{2})/gi).join('-');
					}
					if($scope.ctrl.tripletta.canali) {
						$scope.ctrl.tripletta.canali.forEach(canale => {
							let attivoDal = canale.attivoDal || '';
							canale.attivoDal = attivoDal.replace(/\//gi, '-');
							let attivoAl = canale.attivoAl || '';
							canale.attivoAl = attivoAl.replace(/\//gi, '-');
						});
					}
					$scope.setupForUpdate();
					$scope.selectPeriodoRiferimento();
					$scope.ctrl.selectedTipoBroadcaster = {nome: $scope.ctrl.tripletta.tipoEmittente};
					$scope.ctrl.emittente = {id: $scope.ctrl.tripletta.idEmittente};
					$scope.getCanali({id: $scope.ctrl.tripletta.idEmittente});
					if($scope.ctrl.tripletta.tipoDiritto) $scope.ctrl.configs.error.readOnlyFields.push('tipoDiritto');
					if($scope.ctrl.tripletta.rifTemporaleIncassi) $scope.ctrl.configs.error.readOnlyFields.push('rifTemporaleIncassi');					
				} else {
					$scope.setupForUpdate();
				}
			}, function (error) {
				$scope.setupForUpdate();
			});
		};

		$scope.setupForUpdate = function () {
			$scope.initPeriodoRiferimento('periodoDiRiferimentoDa');
			$scope.initPeriodoRiferimento('periodoDiRiferimentoA');
			$scope.getEmittenti();			
		}

		$scope.emptyTripletta = function () {
			return {
				tipoDiritto: undefined,
				rifTemporaleIncassi: undefined,
				giornoMeseDa: '',
				giornoMeseA: '',
				chiaveRif3: '',
				conto: '',
				attribuzione: '',
				descrizioneSap: '',
				dataInserimento: '',
				utente: '',
				canali: []
			};
		}

		$scope.editable = function (field) {
			try {
				return !$scope.ctrl.configs[$scope.ctrl.mode].readOnlyFields.includes(field);
			} catch (e) {
				return false;
			}
		}

		$scope.initPeriodoRiferimento = function (controlId) {
			$('#' + controlId+'DP')
				.datepicker({
					format: 'dd-mm',
					language: 'it',
					viewMode: 'days',
					minViewMode: 'days',
					autoclose: true,
					todayHighlight: true
				});
		}

		$scope.dettaglioTripletta = function(id) {
			$http({
				method : 'GET',
				url: `${$scope.ctrl.msIngestionApiUrl}triplette/${id}`
			}).then(function success(response) {
				$scope.ctrl.tripletta = response.data;
			}, function error(error) {
				$scope.showAlert('Errore','Non è stato possibile recuperare i dati');
			});
		}		

		$scope.salvaTripletta = function(username) {
			if (!$scope.triplettaValida()) return;
			let request = $scope.buildRequest(username);		
			$http({
				method : 'POST',
				url: $scope.ctrl.msIngestionApiUrl + 'triplette',
				data : {...request}
			}).then(function success(response) {
				triplettaContext.mode = 'detail';
				triplettaContext.tripletta.id = response.data.tripletta.id;
				$scope.showAlert('','Salvataggio effettuato con successo', function () {	
					$scope.init();
				});
			}, function error(error) {
				$scope.showAlert('Errore','Non è stato possibile effettuare il salvataggio');
			});
		}

		$scope.modificaTripletta = function(username) {
			$scope.ctrl.user = username;
			$scope.ctrl.mode = 'edit';
			triplettaContext.mode = 'edit';
			$scope.init();
		}

		$scope.triplettaValida = function () {
			/*
			$scope.ctrl.tripletta.canali.forEach((canale, index) => {
				let value = canale.preallocazionePercentuale + '';
				value = parseInt(value.replace(/\,|\%/gi, ''), 10);
				if (value === 0) {
					$("#percentage" + index)[0].setCustomValidity("Il valore non può essere 0");
					return false;
				} else {
					$("#percentage" + index)[0].setCustomValidity("");
				}
			});
			*/
			if (!$("#triplettaForm")[0].checkValidity()) {
				let errorOnReadOnlyField = false;
				for (let field of $scope.ctrl.configs[$scope.ctrl.mode].readOnlyFields) { 
					let valid = $(`#${field}`)[0].checkValidity();
					if (!valid) {
						errorOnReadOnlyField = true;
						$scope.showAlert('Errore', `Impossibile salvare: il campo ${field} non è valido`);
						break;
					}
				}
				if(!errorOnReadOnlyField) {
					$("#triplettaForm")[0].reportValidity();
				}
				return false;									
			}
			/*
			let percs = [];
			$scope.ctrl.tripletta.canali.forEach(canale => {
				let str = canale.preallocazionePercentuale + '';
				str = str.replace(/\%/gi, '');
				str = str.replace(/\,/gi, '.');
				percs.push(parseFloat(str));
			});
			let sum = percs.reduce((a, b) => a + b, 0);
			if (sum < 100.00) {
				$scope.showAlert('Errore', 'La somma delle allocazioni è inferiore a 100');
				return false;
			}
			if (sum > 100.00) {
				$scope.showAlert('Errore', 'La somma delle allocazioni è superiore a 100');
				return false;
			}
			*/
			return true;
		}

		$scope.buildRequest = function (username) {
			let request = JSON.parse(JSON.stringify($scope.ctrl.tripletta));
			let giornoMeseDa = request.giornoMeseDa;
			request.giornoMeseDa = giornoMeseDa.split('-').join('/').concat('/yyyy');
			let giornoMeseA = request.giornoMeseA;
			request.giornoMeseA = giornoMeseA.split('-').join('/').concat($scope.rifTemporaleAnnoSolare() ? '/yyyy' : '/yyyy+1');
			request.dataInserimento = moment().format('DD/MM/YYYY');
			request.utente = username;
			request.singoloVsVasca = $scope.singoloVsVasca();
			request.canali.forEach(canale => {
				let attivoDal = canale.attivoDal || '';
				canale.attivoDal = attivoDal.replace(/-/gi, '/');
				let attivoAl = canale.attivoAl || '';
				canale.attivoAl = attivoAl.replace(/-/gi, '/')
				//let preallocazionePercentuale = canale.preallocazionePercentuale + '';
				//canale.preallocazionePercentuale = parseFloat(preallocazionePercentuale.replace(/\%/gi, '').replace(/\,/gi, '.'));
			});
			// console.log('Save Tripletta Request:', JSON.stringify(request, null, '\t'));
			return request;
		}

		$scope.rifTemporaleAnnoSolare = function() {
			return $scope.ctrl.tripletta.rifTemporaleIncassi === 'Annuale - Solare';
		}

		$scope.singoloVsVasca = function() {
			return $scope.ctrl.tripletta.canali.length > 1 ? 'Vasca' :'Singolo';
		}

	}]);