
codmanApp.controller('PerformingConfigurazioniSoglieCtrl', [ '$scope', 'PerformingConfigurazioniSoglieService', function($scope, CSService) {
	
	$scope.user = angular.element("#headerLinksBig").text().trim();
	
	$scope.cs = {}
	
	$scope.paging = {
			currentPage: 1,
			order: "inizioPeriodoCompetenza desc"
	}
	
	$scope.navigateToPreviousPage = function(){
		$scope.paging.currentPage = $scope.paging.currentPage - 1
		$scope.init($scope.cs)
	}
	
	$scope.navigateToNextPage = function(){
		$scope.paging.currentPage = $scope.paging.currentPage + 1
		$scope.init($scope.cs)
	}
	
	$scope.navigateToEndPage = function(){
		$scope.paging.currentPage = $scope.paging.totPages
		$scope.init($scope.cs)
	}
	
	$scope.search = function(cs){
		if(!CSService.checkDate(cs.inizioPeriodoCompetenza,cs.finePeriodoCompetenza)){
			CSService.showError("Attenzione","La data di fine periodo non può essere antecedente alla data di inizio periodo")
			return
		}
		
		$scope.init(cs)
		
	}
	
	$scope.init = function(cs){
		CSService.getListaConfigurazioni(cs,$scope.paging).then(function(response){
			
			if(response.data.records.length < 1){
				$scope.paging = {
						currentPage: 1,
						order: "codiceConfigurazione asc"
				}
				CSService.showError("Attenzione","Non è stato trovata nessuna configurazione")
				$scope.response = []
				return
			}
			$scope.paging.currentPage = response.data.currentPage
			$scope.paging.totRecords = response.data.totRecords
			$scope.paging.firstRecord = (($scope.paging.currentPage - 1) * 50) + 1
			$scope.paging.lastRecord = $scope.paging.currentPage * 50
			$scope.paging.totPages = Math.ceil(response.data.totRecords/50)
			
			$scope.response = response.data.records
		},function(response){
			CSService.showError("Errore","Non è stato possibile ottenere le configurazioni delle soglie")
		})
	}
	
	$scope.openModelStorico = function(codiceConfigurazione){
		CSService.openModelStorico(codiceConfigurazione);
	}
	
	$scope.openModelModifica = function(conf){
		CSService.openModelModifica(conf,$scope.user,$scope.init)
	}
	
	$scope.openModelUpload = function(user){
		CSService.openModelUpload(user,$scope.init)
	}
	
	$scope.downloadConfigurazione = function(conf){
		
		CSService.exportConfigurazione(conf.id).then(function(response){
			
			var linkElement = document.createElement('a');
			
			try {
				var blob = new Blob([response.data], { type: "application/vnd.ms-excel;charset=UTF-8" });
				saveAs(blob,conf.nomeOriginaleFile);

			} catch (ex) {
				CSService.showError("Errore","Impossibile fare il download dell'excel. File non trovato");
			}
			
		},function(response){
			if(response.status == 404){
				CSService.showError("Errore","Impossibile fare il download dell'excel. File non trovato");
				return
			}
			CSService.showError("Errore","Impossibile fare il download dell'excel.");
		})
		
	}
	
	$scope.parseDateTime = function(date){
		return CSService.parseDateTime(date)
	}
	
	$scope.parseDate = function(date){
		return CSService.parseDate(date)
	}
	
}]);

codmanApp.service('PerformingConfigurazioniSoglieService', ['ngDialog', '$http', 'Upload', function(ngDialog, $http, Upload){

	var service = this
	
	service.getListaConfigurazioni = function(cs,paging){
		return $http({
			method: 'GET',
			url: 'rest/performing/codifica/soglie/getConfigurazioniSoglie',
			params: {
				inizioPeriodoCompetenza: cs.inizioPeriodoCompetenza,
				finePeriodoCompetenza: cs.finePeriodoCompetenza,
				page: paging.currentPage,
				order: paging.order
			}
		})
	}
	
	service.getConfigurazione = function(id){
		return $http({
			method: 'GET',
			url: 'rest/performing/codifica/soglie/getConfigurazioneSoglie/' + id
		})
	}
	
	service.exportConfigurazione = function(id){
		return $http({
			method: 'GET',
			responseType: 'blob',
			url: 'rest/performing/codifica/soglie/exportConfigurazioneSoglie/'+id
		})
	}
	
	service.checkDate = function(from,to){
		if(from && to){
			
			var comparisonFrom = ""
			var comparisonTo = ""
			
			if(from.length > 7){
				comparisonFrom = from.split("-")[2] + from.split("-")[1] + from.split("-")[0];
				comparisonTo = to.split("-")[2] + to.split("-")[1] + to.split("-")[0];
			}else{
				comparisonFrom = from.split("-")[1] + from.split("-")[0];
				comparisonTo = to.split("-")[1] + to.split("-")[0];
			}
			
			if(comparisonTo < comparisonFrom){
				return false;
			}
		}
		
		return true
	}
	
	service.modificaConfigurazione = function(cs,user){
 		
 		var fd = new FormData()
 		
 		if(cs.file != undefined){
 			fd.append('file', cs.file)
 			var extension = cs.file.name.slice(cs.file.name.lastIndexOf('.'))
			if(extension != '.xls' && extension != '.xlsx'){
				service.showError("Attenzione","Il file non è nel formato corretto. I formati corretti sono .xls e .xlsx")
				return false
			}
 			cs.nomeFile = cs.file.name
 		}
         if(cs.id != undefined){
        	 
         }else{
         	service.showError("Errore","Non è stato possibile modificare la configurazione")
         	return
         }
         if(cs.inizioPeriodoCompetenza != undefined && cs.inizioPeriodoCompetenza != ""){
        	 
         }else{
         	service.showError("Attenzione","Il campo 'Periodo Competenza da' non può essere vuoto")
         	return
         }
         var blob = new Blob([JSON.stringify(cs)], { type: "application/json"});
         fd.append('configurazioneSoglie', blob);
         fd.append('user',user);
         return $http({
 			method:'PUT',
 			url: 'rest/performing/codifica/soglie/updateConfiguration',
 			data: fd,
 			transformRequest: angular.identity,
 			headers: {'Content-Type': undefined}
 		})
         
	}
	
	service.openModelModifica = function(conf,user, init){
		return ngDialog.open({
			template: 'pages/advance-payment/dialog-modificaConfigurazioneSoglie.html',
			plain : false,
			width : '80%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				var ngDialogId = $scope.ngDialogId;
				$scope.ctrl = {
					ngDialogId : ngDialogId
				};
				
				$scope.cancel = function(ngDialogId) {
					ngDialog.close(ngDialogId);
				};
				
				service.getConfigurazione(conf.id).then(function(response){
					$scope.cs = response.data
					$scope.cs.inizioPeriodoCompetenza = service.parseDateTime(response.data.inizioPeriodoCompetenza).split(" ")[0]
					$scope.cs.finePeriodoCompetenza = service.parseDateTime(response.data.finePeriodoCompetenza).split(" ")[0]
					$scope.inizioCompare = $scope.cs.inizioPeriodoCompetenza
					$scope.fineCompare = $scope.cs.finePeriodoCompetenza
				},function(response){
					service.showError("Errore","Impossibile ottenere la configurazione selezionata")
				})
				
				$scope.user = user
				
				$scope.saveModify = function(cs, user){
					if(cs.file == undefined && $scope.cs.inizioPeriodoCompetenza == $scope.inizioCompare && $scope.cs.finePeriodoCompetenza == $scope.fineCompare){
						service.showError("Errore","Prima di salvare, modificare una data o caricare un file")
						return
					}
					
					service.askConfirm("Attenzione","Sei sicuro di voler salvare le modifiche?")
					.closePromise.then(function(model){
						if(model.value == true){
							service.modificaConfigurazione(cs,user).then(function(response){
								service.showError("Successo","La configurazione è stata modificata con successo")
								init({})
							},function(response){
								service.showError("Errore","Non è stato possibile modificare la configurazione: " + response.data)
							})
						}
					})
				}
			}]
		})
	}
	
	service.getStorico = function(codiceConfigurazione){
		return $http({
			method:'GET',
			url: 'rest/performing/codifica/soglie/getStoricoConfigurazioneSoglie/' + codiceConfigurazione
		})
	}
	
	service.openModelStorico = function(codiceConfigurazione){
		return ngDialog.open({
			template : 'pages/advance-payment/dialog-storicoConfigurazioniSoglie.html',
			plain : false,
			width : '90%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				var ngDialogId = $scope.ngDialogId;
				$scope.ctrl = {
					ngDialogId : ngDialogId
				};
				
				$scope.cancel = function() {
					ngDialog.close(ngDialogId);
				};
				
				$scope.codiceConfigurazione = codiceConfigurazione
				
				service.getStorico($scope.codiceConfigurazione).then(function(response){
					if(response.data.length < 1){
						$scope.vuoto = true
						return
					}
					$scope.storico = response.data
				},function(response){
					$scope.vuoto = true
					return
				})
				
				$scope.parseDate = function(date){
					return service.parseDate(date)
				}
				
				$scope.parseDateTime = function(date){
					return service.parseDateTime(date)
				}
				
				$scope.downloadConfigurazioneStorico = function(conf){					
					
					service.exportConfigurazione(conf.id).then(function(response){
						var linkElement = document.createElement('a');

						try {
							var blob = new Blob([response.data], { type: "application/vnd.ms-excel;charset=UTF-8" });
							saveAs(blob,'Configurazione-' + conf.id + '.xls');

						} catch (ex) {
							service.showError("Errore","Impossibile fare il download dell'excel.");
						}
					},function(response){
						service.showError("Errore","Impossibile fare il download dell'excel.")
					})
				}
			}]
		})
	}
	
	service.upload = function(cs){
		
		var extension = cs.file.name.slice(cs.file.name.lastIndexOf('.'))
		if(extension != '.xls' && extension != '.xlsx'){
			service.showError("Attenzione","Il file non è nel formato corretto. I formati corretti sono .xls e .xlsx")
			return false
		}
		
		var fd = new FormData();
		
		if(cs.file != undefined){
			fd.append('file', cs.file)
			cs.nomeFile = cs.file.name
		}
		
		if(cs.inizioPeriodoCompetenza != undefined && cs.inizioPeriodoCompetenza != ""){
        	fd.append('inizioPeriodoCompetenza',cs.inizioPeriodoCompetenza)
        }else{
        	service.showError("Attenzione","Il campo 'Periodo Competenza da' non può essere vuoto")
        	return
        }
        
        var blob = new Blob([JSON.stringify(cs)], { type: "application/json"});
        fd.append('configurazioneSoglie', blob);
        
        return $http({
			method:'POST',
			url: 'rest/performing/codifica/soglie/addConfiguration',
			data: fd,
			transformRequest: angular.identity,
			headers: {'Content-Type': undefined}
		})
	}
	
	service.openModelUpload = function(user,init){
		return ngDialog.open({
			template : 'pages/advance-payment/dialog-uploadConfigurazioniSoglie.html',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				var ngDialogId = $scope.ngDialogId;
				$scope.ctrl = {
					ngDialogId : ngDialogId
				};
				
				$scope.cancel = function() {
					ngDialog.close(ngDialogId);
				};
				
				$scope.upload = function(cs){
					
					if(!service.checkDate(cs.inizioPeriodoCompetenza,cs.finePeriodoCompetenza)){
						service.showError("Attenzione","La data di fine periodo non può essere antecedente alla data di inizio periodo")
						return
					}
					
					cs.utenteCaricamentoFile = user
					
					service.upload(cs).then(function(response){
						service.showError("Successo","Caricamento avvenuto con successo")
						init({})
						$scope.cancel();
					},function(response){
						service.showError("Errore","Non è stato possibile effettuare l'update: " + response.data)
					})
				}
			}]
		})
	}
	
	service.showError = function(title,msgs){
		return ngDialog.open({
			template : 'pages/advance-payment/dialog-alert.html',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				var ngDialogId = $scope.ngDialogId;
				$scope.ctrl = {
					title : title,
					message : msgs,
					ngDialogId : ngDialogId
				};
				$scope.cancel = function() {
					ngDialog.close(ngDialogId);
				};
			}]
		})
	}
	
	service.askConfirm = function(title,msgs){
		return ngDialog.open({
			template : 'pages/advance-payment/dialog-confirm.html',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				var ngDialogId = $scope.ngDialogId;
				$scope.ctrl = {
					title : title,
					message : msgs,
					ngDialogId : ngDialogId
				};
				$scope.cancel = function(ngDialogId) {
					ngDialog.close(ngDialogId);
				};
				$scope.save = function() {
					ngDialog.closeAll(true);
				};
			} ]
		})
	}
	
	service.parseDateTime = function(date){
		
		if(!date){
			return ""
		}
		
		var year = date.split("-")[0]
		var month = date.split("-")[1]
		var day = date.split("-")[2].split("T")[0]
		
		var minutes = date.split("-")[2].split("T")[1].split(".")[0]
		
		return day + "-" + month + "-" + year + " " + minutes
	}
	
	service.parseDate = function(date){
		
		if(!date){
			return ""
		}
		
		var year = date.split("-")[0]
		var month = date.split("-")[1]
		var day = date.split("-")[2].split("T")[0]
		
		return day + "-" + month + "-" + year
	}

}])
