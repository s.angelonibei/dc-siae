/**
 * DsrProcessConfigCtrl
 * 
 * @path /dsrConfig
 */
codmanApp.controller('DsrClaimCtrl', ['$scope', '$route', 'ngDialog', '$routeParams', '$location', '$http', '$q', 'dspsUtilizationsCommercialOffersCountries',
	function ($scope, $route, ngDialog, $routeParams, $location, $http, $q, dspsUtilizationsCommercialOffersCountries) {

		var commercialOffers = dspsUtilizationsCommercialOffersCountries.commercialOffers;
		var countries = dspsUtilizationsCommercialOffersCountries.countries;
		var dsp = dspsUtilizationsCommercialOffersCountries.dsp;
		var utilizations = dspsUtilizationsCommercialOffersCountries.utilizations;

		addStartOption(commercialOffers, 'idCommercialOffering', 'offering');
		addStartOption(countries, 'idCountry', 'name');
		addStartOption(dsp, 'idDsp', 'name');
		addStartOption(utilizations, 'idUtilizationType', 'name');

		$scope.ctrl = {
			navbarTab: 'controlloClaim',
			dsrProcessConfig: {},
			execute: $routeParams.execute,
			hideForm: $routeParams.hideForm,
			maxrows: 20,
			first: $routeParams.first || 0,
			last: $routeParams.last || 20,
			filter: $routeParams.filter,
			selectedDsp: $routeParams.dsp || '*',
			selectedUtilization: $routeParams.utilization || '*',
			selectedCountry: $routeParams.country || '*',
			selectedOffer: $routeParams.offer || '*',
			selectedUuid: $routeParams.uuid || '',
			commercialOffers: commercialOffers,
			countries: countries,
			dsp: dsp,
			utilizations: utilizations
		};

		$scope.filterUtilizationByDsp = filterUtilizationByDsp;
		$scope.filterOffersByDspAndUtilization = filterOffersByDspAndUtilization;


		//Funzione principale richiamata al caricaento della pagina
		$scope.onRefresh = function () {
			// get All DSR Process Configuration					
			if ($scope.ctrl.execute) {
				$scope.getPagedResults($scope.ctrl.first,
					$scope.ctrl.last,
					$scope.ctrl.selectedUuid,
					$scope.ctrl.selectedDsp,
					$scope.ctrl.selectedCountry,
					$scope.ctrl.selectedUtilization,
					$scope.ctrl.selectedOffer);
			} else {

				$scope.getPagedResults(0, $scope.ctrl.maxrows);
			}
		};


		$scope.getPagedResults = function (first, last, uuid, dsp, country, utilization, offer) {
			$http({
				method: 'GET',
				url: 'rest/percClaim/getConf',
				params: {
					first: first,
					last: last,
					uuid: uuid || "",
					dsp: (typeof dsp !== 'undefined' ? dsp : ""),
					country: (typeof country !== 'undefined' ? country : ""),
					utilization: (typeof utilization !== 'undefined' ? utilization : ""),
					offer: offer || ""
				}
			}).then(function successCallback(response) {
				$scope.ctrl.dsrProcessConfig = response.data;
				$scope.ctrl.first = response.data.first;
				$scope.ctrl.last = response.data.last;
			}, function errorCallback(response) {
				$scope.ctrl.dsrProcessConfig = {};
			});

		};

		$scope.navigateToNextPage = function () {
			$location.search('execute', 'true')
				.search('hideForm', $scope.ctrl.hideForm)
				.search('first', $scope.ctrl.last)
				.search('last', $scope.ctrl.last + $scope.ctrl.maxrows);
		};

		$scope.navigateToPreviousPage = function () {
			$location.search('execute', 'true')
				.search('hideForm', $scope.ctrl.hideForm)
				.search('first', Math.max($scope.ctrl.first - $scope.ctrl.maxrows, 0))
				.search('last', $scope.ctrl.first);
		};

		//funzione che renderizza il popup per la modifica della configurazione del processo per DSR
		$scope.showNewProcessConfig = function (event, claimConfig) {
			var options = {
				claimConfig: claimConfig || null,
				api: 'rest/percClaim/saveConf',
				method: 'POST',
				actionLabel: 'Aggiungi'
			}
			openDialog(event, options);
		};

		function openDialog(event, options) {
			console.log($scope.ctrl);
			ngDialog.open({
				template: 'pages/dsr-claim/dialog-new.html',
				plain: false,
				width: '40%',
				controller: ['$scope', 'ngDialog', '$http', '$filter', function ($scope, ngDialog, $http, $filter) {
					var claimConfig = options.claimConfig;
					$scope.ctrl = {
						claimConfig: claimConfig,
						commercialOffersList: commercialOffers,
						countriesList: countries,
						dspList: dsp,
						utilizationsList: utilizations
					};

					$scope.actionLabel = options.actionLabel;

					// numeri da 0 a 100 e max due decimali
					$scope.from0To100 = /^(?:100(?:\.00?)?|\d?\d(?:\.\d\d?)?)$/;


					if (claimConfig) {
						$scope.id = claimConfig.id;
						$scope.codiceUUID = claimConfig.codiceUUID;

						$scope.dsp = getDspById(claimConfig.dspId);

						$scope.country = getCountryById(claimConfig.countryId);

						$scope.utilizationType = getUtilizzationById(claimConfig.utilizationTypeId);

						$scope.drm = claimConfig.percDrm;
						$scope.dem = claimConfig.percDem;

						$scope.selectedOffer = getOffersById(claimConfig.offerId);

						$scope.periodoDa = $filter('mmyyyy')(claimConfig.validFrom);
						$scope.periodoA = $filter('mmyyyy')(claimConfig.validTo);
					}

					$scope.cancel = function () {
						ngDialog.closeAll(false);
					};

					$scope.save = function () {

						var validFrom = '1-' + $scope.periodoDa;
						var dd = moment($scope.periodoA, 'MM-YYYY').endOf('month').format('DD');
						var validTo = dd + '-' + $scope.periodoA;

						var request = {
							id: $scope.id,
							codiceUUID: $scope.codiceUUID,
							dspId: $scope.dsp.idDsp,
							countryId: $scope.country.idCountry,
							utilizationTypeId: $scope.utilizationType.idUtilizationType,
							percDrm: $scope.drm,
							percDem: $scope.dem,
							offerId: $scope.selectedOffer.idCommercialOffering,
							validFrom: validFrom,
							validTo: validTo
						};

						console.log($scope);
						// return;

						$http({
							method: options.method,
							url: options.api,
							data: request
						}).then(function successCallback(response) {
							ngDialog.closeAll(true);
							$route.reload();
						}, function errorCallback(response) {
							if (response.status == 409) {
								$scope.openErrorDialog("Claim già presente");

							} else {
								$scope.openErrorDialog(response && response.statusText || "Errore Imprevisto");
							}

						});
					};

					$scope.filterUtilizationByDsp = filterUtilizationByDsp;


					$scope.filterOffersByDspAndUtilization = filterOffersByDspAndUtilization;


					$scope.openErrorDialog = function (errorMsg) {
						ngDialog.open({
							template: 'errorDialogId',
							plain: false,
							width: '40%',
							data: errorMsg

						});
					};

				}]
			}).closePromise.then(function (data) {
				if (data.value === true) {
					// no op
				}
			});
		}

		$scope.showDeleteProcessConfig = function (event, claimConfig) {
			ngDialog.open({
				template: 'pages/dsr-claim/dialog-remove.html',
				plain: false,
				width: '87%',
				controller: ['$scope', 'ngDialog', '$http', function ($scope, ngDialog, $http) {
					$scope.ctrl = {
						claimConfig: claimConfig
					};

					$scope.cancel = function () {
						ngDialog.closeAll(false);
					};

					$scope.deleteDsrConfig = function () {
						$http({
							method: 'DELETE',
							url: 'rest/percClaim/removeConf',
							headers: {
								'Content-Type': 'application/json'
							},
							data: {
								id: claimConfig.id
							}

						}).then(function successCallback(response) {
							ngDialog.closeAll(true);
							$route.reload();
						}, function errorCallback(response) {
							$scope.openErrorDialog(response && response.statusText || "Errore Imprevisto");
						});

					};

					$scope.openErrorDialog = function (errorMsg) {
						ngDialog.open({
							template: 'errorDialogId',
							plain: false,
							width: '40%',
							data: errorMsg

						});
					};
				}]
			}).closePromise.then(function (data) {
				if (data.value === true) {
					// no op
				}
			});
		};

		//funzione che renderizza il popup per la modifica della configurazione del processo per DSR
		$scope.showEditProcessConfig = function (event, claimConfig) {

			var options = {
				claimConfig: claimConfig || null,
				api: 'rest/percClaim/updateConf',
				method: 'PUT',
				actionLabel: 'Modifica'
			}
			console.log(event, claimConfig);
			openDialog(event, options);
		};

		$scope.$on('filterApply-ClaimConfig', function (event, parameters) {

			$scope.ctrl.filterParameters = parameters;
			console.log($routeParams);
			$location.search('execute', 'true')
				.search('hideForm', false)
				.search('first', 0)
				.search('last', $scope.ctrl.maxrows)
				.search('uuid', parameters.uuid)
				.search('dsp', parameters.dsp)
				.search('country', parameters.country)
				.search('utilization', parameters.utilization)
				.search('offer', parameters.offer);
			$scope.ctrl.hideForm = false;

			// se ci sono differenze, basta cambiare la query string per il reload
			// diversamente, viene forzato il reload
			if (!checkDiff(parameters)) {
				$scope.onRefresh();
			}
		});

		$scope.onRefresh();

		function checkDiff(parameters) {
			if (parameters.uuid !== $routeParams.uuid
				|| parameters.dsp !== $routeParams.dsp
				|| parameters.country !== $routeParams.country
				|| parameters.utilization !== $routeParams.utilization
				|| parameters.offer !== $routeParams.offer) {
				return true;
			}
			return false;
		}

		function getDspById(dspName) {
			return dsp.find(function (item) {
				return item.idDsp === dspName;
			});
		}

		function getCountryById(territorio) {
			return countries.find(function (item) {
				return item.idCountry === territorio;
			})
		}

		function getUtilizzationById(tipoUtilizzo) {
			return utilizations.find(function (item) {
				return item.idUtilizationType === tipoUtilizzo;
			});
		}

		function getOffersById(offertaCommerciale) {
			return commercialOffers.find(function (item) {
				return String(item.idCommercialOffering) === offertaCommerciale;
			});
		}

		function addStartOption(arr, id, descr) {
			if (_.isArray(arr)) {
				var obj = {};
				obj[id] = '*';
				obj[descr] = '*';
				arr.unshift(obj);
			}
		}

		function filterUtilizationByDsp(dsp) {
			return function (item) {

				var selectedDsp = dsp;

				if (!selectedDsp) {
					return true;
				}

				if (selectedDsp.idDsp === '*') {
					return true;
				}

				if (item.idUtilizationType === "*") {
					return true;
				}

				var idDsp = item.idDsp;
				if (_.isArray(idDsp)) {
					var sItem = idDsp.find(function (aDsp) {
						return aDsp === selectedDsp.idDsp;
					})

					return sItem ? true : false;
				}

				return true;
			};
		}

		function filterOffersByDspAndUtilization(dsp, utilization) {
			return function (item) {
				var selectedDsp = dsp;
				var selectedUtilization = utilization;

				if (!selectedDsp || !selectedUtilization) {
					return true;
				}

				if (item.idCommercialOffering === '*') {
					return true;
				}

				var check1 = !selectedDsp || selectedDsp.idDsp === "*" ? true : item.idDSP === selectedDsp.idDsp;
				var check2 = !selectedUtilization || selectedUtilization.idUtilizationType === "*" ? true
					: item.idUtilizationType === selectedUtilization.idUtilizationType;

				return check1 && check2;
			};
		}

	}]);

codmanApp.filter('mmyyyy', function () {
	return function (input) {
		if (!input) {
			return input;
		}

		var tokens = input.split('-');
		if (tokens.length !== 3) {
			return input;
		}

		tokens.shift();
		return tokens.join('-');
	};
});

codmanApp.directive('percValidation', function () {
	return {
		require: 'ngModel',
		restrict: 'A',
		link: function (scope, elem, attr, ngModel) {

			ngModel.$validators.percValidation = function (modelValue, viewValue) {
				var value = modelValue || viewValue;

				console.log(scope.drm, attr['compareWith']);

				// return true;
				if (!value) {
					return true;
				}

				console.log(scope);

				var nValue = value * 100;
				var compareWith = attr['compareWith'];
				var nOtherValue = Number(compareWith) * 100;
				console.log(nValue, nOtherValue, nValue + nOtherValue);
				if ((nValue + nOtherValue) === 10000) {
					console.log('valido');
					return true;
				}
				console.log('NON valido');
				return false;
			}

			attr.$observe('compareWith', function (value) {
				ngModel.$validate();
			});
		}
	}
});

codmanApp.directive('dateValidation', function () {
	return {
		require: 'ngModel',
		restrict: 'A',
		link: function (scope, elem, attr, ngModel) {
			console.log('liiiinn');

			var m = attr['dateModel'];
			var dataDa = attr['dateFrom'];
			var dataA = attr['dateTo'];

			ngModel.$validators.dateValidation = function (modelValue, viewValue) {
				/* var m = scope.$eval(attr['dateModel']);
				var dataDa = scope.$eval(attr['dateFrom']);
				var dataA = scope.$eval(attr['dateTo']); */

				console.log(m, dataDa, dataA);

				var value = modelValue || viewValue;
				if (!value) {
					return false;
				}

				var isValid = moment(value, 'MM-YYYY', true).isValid();
				if (isValid === false) {
					return false;
				}

				if (m === 'min' && scope[dataA] && moment(scope[dataA], 'MM-YYYY', true).isValid()) {
					var check = moment(value, 'MM-YYYY').isSameOrBefore(moment(scope[dataA], 'MM-YYYY'));
					return check;
				}

				if (m === 'max' && scope[dataDa] && moment(scope[dataDa], 'MM-YYYY', true).isValid()) {
					var check = moment(scope[dataDa], 'MM-YYYY').isSameOrBefore(moment(value, 'MM-YYYY'));
					return check;
				}


				return true;
			};

			if (m === 'max') {
				attr.$observe('dateFromValue', function (value) {
					//do something with the new Time
					var a = scope[dataA]
					ngModel.$validate();
					scope[dataA] = a;
					ngModel.$validate();
					if (value) {
						ngModel.$dirty = true;
					}
				});
			}


			if (m === 'min') {
				attr.$observe('dateToValue', function (value) {
					var da = scope[dataDa]
					ngModel.$validate();
					scope[dataDa] = da;
					ngModel.$validate();
					if (value) {
						ngModel.$dirty = true;
					}
				});
			}
		}
	};
});