codmanApp.controller('VisualizzazioneFattureCtrl',['$scope', '$http', '$routeParams', '$location', 'ngDialog', '$rootScope', function($scope, $http, $routeParams, $location, ngDialog, $rootScope){
	
	$scope.count = true;
	
	$scope.params = {};
	
	$scope.orderImage = function(name,param){
		
		//get id of html element clicked
		var id = "#" + name;
		
		//get css class of that html element for change the image of arrows
		var classe = angular.element(id).attr("class");

		//set to original position all images of arrows, on each columns.
		var element1 = document.getElementsByClassName("glyphicon glyphicon-sort-by-attributes");
		var element2 = document.getElementsByClassName("glyphicon glyphicon-sort-by-attributes-alt");
		angular.element(element1).attr("class","glyphicon glyphicon-sort");
		angular.element(element2).attr("class","glyphicon glyphicon-sort");

		if(classe != "glyphicon glyphicon-sort-by-attributes" || classe == "glyphicon glyphicon-sort"){
			
			angular.element(id).attr("class","glyphicon glyphicon-sort-by-attributes");
			
		}else{
			angular.element(id).attr("class","glyphicon glyphicon-sort-by-attributes-alt");
		}
		
		$scope.order(param);
		
	}
	
	$scope.order = function(param){
		if($scope.orderBy == param.concat(" asc")){
			$scope.orderBy = param.concat(" desc");
		}else{
			$scope.orderBy = param.concat(" asc");
		}
		$scope.getSampling(false);
	}
	
	/*$scope.exportExcel = function(){
		$http({
			method: 'GET',
			url: 'rest/performing/cruscotti/exportEventiList',
			params: $scope.params
		}).then( function(response) {

			var linkElement = document.createElement('a');

			try {

				var blob = new Blob([response.data], { type: 'application/csv' });
				var url = window.URL.createObjectURL(blob);
				linkElement.setAttribute('href', url);
                linkElement.setAttribute("download", 'ListaFattureRicercati.csv');

                var clickEvent = new MouseEvent("click", {
                	"view": window,
                	"bubbles": true,
                	"cancelable": false
                });
                linkElement.dispatchEvent(clickEvent);

			} catch (ex) {

			}
		}, function (response) {
			$scope.showError("Errore","Impossibile fare il download dell'excel.");
		});
	}*/
	
	$scope.formatDate = function(date){
		if(date != undefined){
			var data = date.split("T")[0];
			var day = data.split('-')[2];
			var month = data.split('-')[1];
			var year = data.split('-')[0];
			return day + '-' + month + '-' + year;
			
		}
	};
	
	$scope.navigateToNextPage = function() {
		$scope.ctrl.page=$scope.ctrl.page+1;
		$scope.getSampling(false);

	};

	$scope.navigateToPreviousPage = function() {
		$scope.ctrl.page=$scope.ctrl.page-1;
		$scope.getSampling(false);

	};
	
	$scope.navigateToEndPage = function(){
		$scope.ctrl.page = $scope.ctrl.pageTot;
		$scope.getSampling(false);
	}
	
	// Funzione principale richiamata al caricamento della pagina
	$scope.onRefresh = function() {
		
	};
	
	$scope.ctrl = {
		navbarTab : 'visualizzazioneEventi',
		execute : $routeParams.execute,
		page : 0
		};
	
	$scope.filterApply = function() {
		
		var element1 = document.getElementsByClassName("glyphicon glyphicon-sort-by-attributes");
		var element2 = document.getElementsByClassName("glyphicon glyphicon-sort-by-attributes-alt");
		angular.element(element1).attr("class","glyphicon glyphicon-sort");
		angular.element(element2).attr("class","glyphicon glyphicon-sort");
		
		$scope.ctrl.first = 0;
		$scope.ctrl.last = 50;
		$scope.ctrl.page = 0;
		$scope.orderBy = null;
		$scope.getSampling(true);
		
	}
	
	//il flag è usato per la count. Se è vero allora fa la count.
	$scope.getSampling = function(flag){
		
		if($rootScope.savedQuery == undefined || $rootScope.savedQuery == null){
		
			$scope.params = {
					tipoDocumento: $scope.tipoDocumento,
					idEvento: $scope.idEvento,
					fattura: $scope.fattura,
					voceIncasso: $scope.voceIncasso,
					reversale: $scope.reversale,
					dataEventoDa: $scope.dataEventoDa,
					dataEventoA: $scope.dataEventoA,
					seprag: $scope.seprag,
					organizzatore: $scope.ivaCF,
					codiceSap: $scope.codiceSAP,
					sede: $scope.sede,
					agenzia: $scope.agenzia,
					dataFatturaDa: $scope.dataFatturaDa,
					dataFatturaA: $scope.dataFatturaA,
					fatturaValidata: $scope.fatturaValidata,
					order : $scope.orderBy,
					page: $scope.ctrl.page,
					fatturaValidata: $scope.fatturaValidata,
					importo: $scope.importo, // importo sun ovvero importo totale dem lordo pm
					count: flag
			}
			
		}else{
			$scope.params = $rootScope.savedQuery.params;
//			$scope.countData = $rootScope.savedQuery.countData;
//			$scope.ctrl.page = $rootScope.savedQuery.page;
			$rootScope.savedQuery = undefined;
		}
		
        //aggiungere gli altri campi alla chiamata e modificare page!
//		$http($rootScope.savedQuery).then(function(response){
		$http({
			method: 'GET',
			url: 'rest/performing/cruscotti/fattureList',
			params: $scope.params
		}).then(function(response){	
			if(flag == true){
				$scope.countDataTemp = response.data;
				$scope.pageTotTemp = Math.floor(response.data/50);
				$scope.totRecordsTemp = response.data;
				if(response.data > 1000){
					$scope.showWarning("Warning","Il risultato prevede " + response.data + " record, è consigliabile applicare dei filtri aggiuntivi. Sicuro di voler continuare?");
				}else if (response.data == 0){
					$scope.fillElements(null,null,null,null,null,null);
					$scope.showError("","Non risulta nessun dato per la ricerca effettuata");
				}else{
					$rootScope.pageTot = $scope.pageTotTemp;
					$rootScope.totRecords = $scope.totRecordsTemp;
					$scope.getSampling(false);
				}
			}else{
				var currentPage = response.data.currentPage*50;
				$scope.totRecords = $scope.countDataTemp;
				$scope.pageTot = $scope.pageTotTemp;
				$scope.fillElements($scope.totRecords, response.data.currentPage, currentPage, response.data.records.length, response.data.records, $scope.pageTot);
			}
			
		},function(response){
			$scope.fillElements(null,null,null,null,null,null);
			$scope.showError("","Non risulta nessun dato per la ricerca effettuata");
		})
	};
	
	//a seconda di come va la chiamata riempie gli oggetti che servono alla view
	$scope.fillElements = function(totRecords,page,first,last,samplings,pageTot){
		$scope.ctrl.totRecords = totRecords;
		$scope.ctrl.page = page;
		$scope.ctrl.first = first;
		$scope.ctrl.last = totRecords;
		$scope.ctrl.samplings = samplings;
		$scope.ctrl.pageTot = pageTot;
	}
	
	$scope.showWarning = function(title, msgs) {
		var parentScope = $scope
		ngDialog.open({
			template : 'pages/advance-payment/dialog-warning.html',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				$scope.ctrl = {
					title : title,
					message : msgs
				};
				$scope.cancel = function() {
					$rootScope.flag = false;
					ngDialog.closeAll(false);
				};
				$scope.call = function() {
					$rootScope.flag = true;
					parentScope.countData = parentScope.countDataTemp
					parentScope.pageTot = parentScope.pageTotTemp
					parentScope.totRecords = parentScope.totRecordsTemp
					ngDialog.closeAll(true);
				};
			} ]
		}).closePromise.then(function(data) {
			// no op
			if($rootScope.flag == true){
				//aggiungere orderBy
				$scope.getSampling(false);
			}
		});
	};
	
	$scope.showError = function(title, msgs) {
		ngDialog.open({
			template : 'pages/advance-payment/dialog-alert.html',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				$scope.ctrl = {
					title : title,
					message : msgs
				};
				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				$scope.save = function() {
					ngDialog.closeAll(true);
				};
			} ]
		}).closePromise.then(function(data) {
		});
	};
	
	$scope.showAlert = function(title, message) {
		ngDialog.open({
			template : 'pages/advance-payment/dialog-alert.html',
			plain : false,
			width : '50%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				var ngDialogId = $scope.ngDialogId;
				$scope.ctrl = {
					title : title,
					message : message,
					ngDialogId : ngDialogId
				};
				$scope.cancel = function(ngDialogId) {
					ngDialog.close(ngDialogId);
				};
			} ]
		}).closePromise.then(function(data) {
			if (data.value === true) {

			}
		});
	};
	
	$scope.goTo = function(url,param){
		
		if(url != undefined && param != undefined){
			$rootScope.savedQuery = {pagina : "Fattura",params: $scope.params, countData : $scope.countData, page : $scope.ctrl.page};
			$location.path(url + param);
		}
	}
	
	$scope.goToFattura = function(url,param){
		if(url != undefined && param != undefined){
			$rootScope.savedQuery = {pagina : "Fattura",params: $scope.params, countData : $scope.countData, page : $scope.ctrl.page};
			$location.path(url + param);
		}
	}
	
	if($rootScope.savedQuery != undefined && $rootScope.savedQuery != null){
		if($rootScope.savedQuery.pagina == "Fattura"){
			if($rootScope.savedQuery.params.tipoDocumento != undefined){
				$scope.tipoDocumento = $rootScope.savedQuery.params.tipoDocumento
			}
			
			if($rootScope.savedQuery.params.evento != undefined){
				$scope.idEvento = $rootScope.savedQuery.params.evento
			}
			
			if($rootScope.savedQuery.params.fattura != undefined){
				$scope.fattura =$rootScope.savedQuery.params.fattura
			}
			
			if($rootScope.savedQuery.params.voceIncasso != undefined){
				$scope.voceIncasso = $rootScope.savedQuery.params.voceIncasso
			}
			
			if($rootScope.savedQuery.params.reversale != undefined){
				$scope.reversale =$rootScope.savedQuery.params.reversale
			}
//			controllare data Inizio evento
			if($rootScope.savedQuery.params.dataInizioEvento != undefined){
				$scope.dataInizioEvento = $rootScope.savedQuery.params.dataInizioEvento
			}
			
			if($rootScope.savedQuery.params.seprag != undefined){
				$scope.seprag =$rootScope.savedQuery.params.seprag
			}
			
			if($rootScope.savedQuery.params.codiceSAPOrganizzatore != undefined){
				$scope.codiceSAP =$rootScope.savedQuery.params.codiceSAPOrganizzatore 
			}
			
			if($rootScope.savedQuery.params.organizzatore != undefined){
				$scope.ivaCF =$rootScope.savedQuery.params.organizzatore
			}
			
			if($rootScope.savedQuery.params.sede != undefined){
				$scope.sede =$rootScope.savedQuery.params.sede
			}
			
			if($rootScope.savedQuery.params.agenzia != undefined){
				$scope.agenzia =$rootScope.savedQuery.params.agenzia
			}
			
			if($rootScope.savedQuery.params.dataFattura != undefined){
				$scope.dataFattura =$rootScope.savedQuery.params.dataFattura
			}
			
			if($rootScope.savedQuery.params.importo != undefined){
				$scope.importo =$rootScope.savedQuery.params.importo
			}
			
			if($rootScope.savedQuery.params.fatturaValidata != undefined){
				$scope.fatturaValidata =$rootScope.savedQuery.params.fatturaValidata
			}
			
			if($rootScope.savedQuery.page != undefined){
				$scope.ctrl.page = $rootScope.savedQuery.page
			}
			
			if($rootScope.savedQuery.countData != undefined){
				$scope.totRecords = $rootScope.savedQuery.countData
				$scope.pageTot = Math.floor($rootScope.savedQuery.countData/50);
			}
			
			$scope.getSampling(false);
		}else{
			$rootScope.savedQuery = undefined;
		}
	}
	
}]);