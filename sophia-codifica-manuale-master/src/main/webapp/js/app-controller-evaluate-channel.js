/**
 * DsrMetadataCtrl
 * 
 * @path /dsrMetadata
 */
codmanApp.controller('EvaluateChannelCtrl', [ '$scope', '$route', 'ngDialog', '$routeParams', '$location', '$http', 'Upload', function($scope, $route, ngDialog, $routeParams, $location, $http, Upload) {

	$scope.ctrl = {
		navbarTab : 'ccidConfig',
		config : [],
		filterParameters : [],
		execute : $routeParams.execute,
		hideForm : $routeParams.hideForm,
		listaEmittenti : [],
		listaCanali : [],
		listaGauge : [],
		mesiSelected : [],
		emittente : $routeParams.emittente || '*',
		canale : $routeParams.canale || '*',
		anno : $routeParams.anno || '',
		mesi : [
			{
				id : '1',
				label : ' Gennaio '
			},
			{
				id : '2',
				label : ' Febbraio '
			},
			{
				id : '3',
				label : ' Marzo '
			},
			{
				id : '4',
				label : ' Aprile '
			},
			{
				id : '5',
				label : ' Maggio '
			},
			{
				id : '6',
				label : ' Giugno '
			},
			{
				id : '7',
				label : ' Luglio '
			},
			{
				id : '8',
				label : ' Agosto '
			},
			{
				id : '9',
				label : ' Settembre '
			},
			{
				id : '10',
				label : ' Ottobre '
			},
			{
				id : '11',
				label : ' Novembre '
			},
			{
				id : '12',
				label : ' Dicembre '
			} ],
		anni : [ (new Date()).getFullYear(), (new Date()).getFullYear() - 1, (new Date()).getFullYear() - 2, (new Date()).getFullYear() - 3, (new Date()).getFullYear() - 4 , (new Date()).getFullYear() - 5 , (new Date()).getFullYear() - 6 , (new Date()).getFullYear() - 7  ],
		tipiEmittenti : [ {
			"nome" : "TELEVISIONE"
		}, {
			"nome" : "RADIO"
		} ],
	};

	$scope.multiselectSettings = {
		scrollableHeight : '200px',
		scrollable : false,
		enableSearch : false,
		showUncheckAll : true,
		searchBr : false,
		checkboxes : false
	};


	// Funzione principale richiamata al caricamento della pagina
	$scope.onRefresh = function() {
		
	};


	$scope.getNameForSelect = function(item) {
		if (item.tipo_broadcaster === "TELEVISIONE") {
			return item.nome + " - TV";
		} else if (item.tipo_broadcaster === "RADIO") {
			return item.nome + " - RADIO";
		} else {
			return item.nome;
		}
	}
	$scope.getEmittenti = function(item) {
		$scope.ctrl.utenti = [];
		$scope.ctrl.selectedUser = "";
		var tipoBroadcaster = item;
		$http({
			method : 'GET',
			url : 'rest/broadcasting/emittentiFiltrati',
			params : {
				fiteredType : tipoBroadcaster
			}
		}).then(function successCallback(response) {
			$scope.ctrl.listaEmittenti = response.data;
		}, function errorCallback(response) {
			$scope.ctrl.listaEmittenti = [];
		});
	}

	$scope.getListFiltered = function(item) {
		var results = item ? $scope.ctrl.listaEmittenti.filter($scope.createFilterFor(item)) : $scope.ctrl.listaEmittenti,
			deferred;

		return results;


	};

	$scope.createFilterFor = function(query) {
		var lowercaseQuery = angular.uppercase(query);

		return function filterFn(item) {
			return (item.nome.indexOf(lowercaseQuery) === 0);
		};

	}


	$scope.apply = function(emittente, canale, anno, mesiSelected) {
		var emit = emittente;
		var can = canale;
		var ann = anno;
		var mes = [];
		for (i = 0; i < mesiSelected.length; i++) {
			mes.push(mesiSelected[i].id)
		}
		$http({
			method : 'POST',
			url : 'rest/broadcastingKPI/fe/monitoraggio/kpi',
			headers : {

			},
			data : {
				"RequestData" : {
					"emittente" : {
						"id" : emit.id
					},
					"canali" : [ can.id ],
					"anno" : ann,
					"mesi" : mes,
					"tipoCanali" : can.tipoCanale,
					"platform":"BKO"
				}
			}
		}).then(function successCallback(response) {
			$scope.ctrl.listaGauge = response.data.ResponseData.output.listaKPI;
			$scope.ctrl.split_items=[]
			var cont=0;
			var arrSubItem=[];
			for (i = 0; i < $scope.ctrl.listaGauge.length; i++) {
				if(cont==3){
					$scope.ctrl.split_items.push(arrSubItem)
					arrSubItem=[];
					cont=0;
				}
				arrSubItem.push($scope.ctrl.listaGauge[i])
				cont++;
			}
			$scope.ctrl.split_items.push(arrSubItem)
		}, function errorCallback(response) {
			$scope.ctrl.listaGauge = []
		});


	}

	$scope.download = function(emittente, canale, anno, mesiSelected) {
		var emit = emittente;
		var can = canale;
		var ann = anno;
		var mes = [];
		for (i = 0; i < mesiSelected.length; i++) {
			mes.push(mesiSelected[i].id)
		}
		
		$http({
			method : 'POST',
			url : 'rest/broadcastingS3/fe/utilizationfile/downloadFileArmonizato',
			headers : {

			},
			data : {
				"canali" : [ can ],
				"anno" : ann,
				"mesi" : mes,
				"tipoCanali" : can.tipoCanale,
				"broadcasters":emittente
			}
		}).then(function successCallback(response) {

			if(typeof response=='string'){
				$scope.showError("Errore",response);
			}else{
				saveAs(new Blob([response.data],{type:response.headers('content-type')}), response.headers('content-disposition').split('"')[1]);
			}
		}, function errorCallback(response) {
			$scope.showError("","Questa selezione non ha prodotto risultati");
		});

	}
	
	$scope.getCanali = function(emittente) {
		var emit = emittente;
		if (emit == null || emit == undefined || emit.id == undefined) {
			return;
		}
		$http({
			method : 'GET',
			url : 'rest/broadcasting/canali/' + emit.id,
			params : {

			}
		}).then(function successCallback(response) {
			$scope.ctrl.listaCanali = response.data;
		}, function errorCallback(response) {
			$scope.ctrl.listaCanali = []
		});

	};


	$scope.showUpload = function(event) {
		var scope = $scope;
		ngDialog.open({
			template : 'pages/broadcasting/gestioneUtilizzazioni/dialog-upload.html',
			plain : false,
			width : '50%',
			controller : [ '$scope', 'ngDialog', '$http', function($scope, ngDialog, $http) {
				$scope.ctrl = {
					tipoUpload : "Singolo",
					tipiUpload : [ 'Singolo', 'Massivo' ],
					listaEmittenti : [],
					listaCanali : [],
					listaGauge : [],
					emittente : $routeParams.emittente || '*',
					canale : $routeParams.canale || '*',
					anno : $routeParams.anno || '',
					mese : $routeParams.mese || '*',
					mesi : [
						{
							id : '1',
							label : ' Gennaio '
						},
						{
							id : '2',
							label : ' Febbraio '
						},
						{
							id : '3',
							label : ' Marzo '
						},
						{
							id : '4',
							label : ' Aprile '
						},
						{
							id : '5',
							label : ' Maggio '
						},
						{
							id : '6',
							label : ' Giugno '
						},
						{
							id : '7',
							label : ' Luglio '
						},
						{
							id : '8',
							label : ' Agosto '
						},
						{
							id : '9',
							label : ' Settembre '
						},
						{
							id : '10',
							label : ' Ottobre '
						},
						{
							id : '11',
							label : ' Novembre '
						},
						{
							id : '12',
							label : ' Dicembre '
						} ],
						anni : [ (new Date()).getFullYear(), (new Date()).getFullYear() - 1, (new Date()).getFullYear() - 2, (new Date()).getFullYear() - 3, (new Date()).getFullYear() - 4 , (new Date()).getFullYear() - 5 , (new Date()).getFullYear() - 6 , (new Date()).getFullYear() - 7  ],
						
				};

				$scope.getNameForSelect = function(item) {
					if (item.tipo_broadcaster == "TELEVISIONE") {
						return item.nome + " - TV";
					} else if (item.tipo_broadcaster == "RADIO") {
						return item.nome + " - RADIO";
					} else {
						return item.nome;
					}
				}

				$http({
					method : 'GET',
					url : 'rest/broadcasting/emittenti',
					params : {

					}
				}).then(function successCallback(response) {
					$scope.ctrl.listaEmittenti = response.data;
				}, function errorCallback(response) {
					$scope.ctrl.listaEmittenti = [];
				});

				$scope.getCanali = function(emittente) {
					var emit = emittente;
					if (emit == null || emit == undefined || emit.id == undefined) {
						return;
					}
					$http({
						method : 'GET',
						url : 'rest/broadcasting/canali/' + emit.id,
						params : {

						}
					}).then(function successCallback(response) {
						$scope.ctrl.listaCanali = response.data;
					}, function errorCallback(response) {
						$scope.ctrl.listaCanali = []
					});

				};

				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				$scope.save = function() {
					if ($scope.form.file.$valid && $scope.ctrl.file) {
						if ($scope.ctrl.tipoUpload == 'Singolo') {
							//UPLOAD SINGOLO
							Upload.upload({
								url : 'rest/broadcastingS3/fe/utilizationfile/upload',
								data : {
									file : $scope.ctrl.file,
									idUtente : 0,
									idBroadcaster : $scope.ctrl.emittente.id,
									idCanale : $scope.ctrl.canale.id,
									anno : $scope.ctrl.anno,
									mese : $scope.ctrl.mese
								}
							}).then(function(resp) {
								ngDialog.closeAll(true);
								scope.showError("Esito Upload", "File Caricato con successo");
							}, function(resp) {
								ngDialog.closeAll(false);
								scope.showError("Esito Upload", "Impossibile caricare il file, controllare la validità");
							}, function(evt) {
								var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
								logmsg('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
							});
						} else {
							//UPLOAD MASSIVO
							Upload.upload({
								url : 'rest/broadcastingS3/fe/utilizationfile/upload',
								data : {
									file : $scope.ctrl.file,
									idUtente : 0,
									idBroadcaster : $scope.ctrl.emittente.id,
								}
							}).then(function(resp) {
								ngDialog.closeAll(true);
								scope.showError("Esito Upload", "File Caricato con successo");
							}, function(resp) {
								ngDialog.closeAll(false);
								scope.showError("Esito Upload", "Impossibile caricare il file, controllare la validità");
							}, function(evt) {
								var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
								logmsg('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
							});
						}



					}
				};
			} ]
		}).closePromise.then(function(data) {
			if (data.value === true) {
				$route.reload();
			}
		});
	};
	$scope.showError = function(title, msgs) {
		ngDialog.open({
			template : 'pages/advance-payment/dialog-alert.html',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				$scope.ctrl = {
					title : title,
					message : msgs
				};
				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				$scope.save = function() {
					ngDialog.closeAll(true);
				};
			} ]
		}).closePromise.then(function(data) {
			// no op
		});
	};

	$scope.$on('filterApply', function(event, parameters) {
		$scope.ctrl.filterParameters = parameters;
		$location.search('execute', 'true')
			.search('hideForm', false)
			.search('first', 0)
			.search('last', $scope.ctrl.maxrows)
			.search('dsp', parameters.dsp)
			.search('utilization', parameters.utilization)
			.search('offer', parameters.offer)
			.search('country', parameters.country)
			.search('hideForm', parameters.hideForm);
		$scope.ctrl.hideForm = false;

	});

	$scope.onRefresh();

} ]);


codmanApp.filter('range', function() {
	return function(input, min, max) {
		min = parseInt(min); //Make string input int
		max = parseInt(max);
		for (var i = min; i <= max; i++)
			input.push(i);
		return input;
	};
});