/**
 * DsrMetadataCtrl
 * 
 * @path /dsrMetadata
 */
codmanApp.controller('CarichiRipartoCtrl', [ '$scope', '$route', 'ngDialog', '$routeParams', '$location', '$http', 'Upload', 'configService', function($scope, $route, ngDialog, $routeParams, $location, $http, Upload, configService) {

	$scope.ctrl = {
		navbarTab : 'ccidConfig',
		config : [],
		periodi :[],
		filterParameters : [],
		execute : $routeParams.execute,
		hideForm : $routeParams.hideForm,
		listaEmittenti : [],
		listaCanali : [],
		selectedTipoBroadcaster : "",
		emittente : $routeParams.emittente || 'TUTTE',
		canale : $routeParams.canale || 'TUTTI',
		anno : $routeParams.anno || '',
		selectedPeriodTo : $routeParams.selectedPeriodTo || '',
		selectedPeriodFrom : $routeParams.selectedPeriodFrom || '',
		anni : [ (new Date()).getFullYear(), (new Date()).getFullYear() - 1, (new Date()).getFullYear() - 2, (new Date()).getFullYear() - 3, (new Date()).getFullYear() - 4 , (new Date()).getFullYear() - 5 , (new Date()).getFullYear() - 6 , (new Date()).getFullYear() - 7  ],
		tipiEmittenti : [ 
			{
				"nome" : "TUTTE"
			}, {
				"nome" : "TELEVISIONE"
			}, {
				"nome" : "RADIO"
			} ],
		first : 0,
		last : 50,
		maxrow : 50,
		dettailSelected : null,
		storico	:[],
		caricoRiparto:null,
		sortType:'canale.nome',
		sortOrder:'canale.nome'
		
	};

	$scope.multiselectSettings = {
		scrollableHeight : '200px',
		scrollable : false,
		enableSearch : false,
		showUncheckAll : true,
		searchBr : false,
		checkboxes : false
	};


	// Funzione principale richiamata al caricamento della pagina
	$scope.onRefresh = function() {
		
	};
	$scope.navigateToNextPage = function() {
		$scope.getCarichiRiparto($scope.ctrl.selectedTipoBroadcaster,$scope.ctrl.emittente, $scope.ctrl.canale, $scope.ctrl.anno, $scope.ctrl.selectedPeriodFrom,$scope.ctrl.selectedPeriodTo,$scope.ctrl.last,$scope.ctrl.last + $scope.ctrl.maxrows,50)
		 
	};


	$scope.formatNumber = function(number){
 		return twoDecimalRound(number);
	}
	
	$scope.navigateToPreviousPage = function() {

		$scope.getCarichiRiparto($scope.ctrl.selectedTipoBroadcaster,$scope.ctrl.emittente, $scope.ctrl.canale, $scope.ctrl.anno, $scope.ctrl.selectedPeriodFrom,$scope.ctrl.selectedPeriodTo,Math.max($scope.ctrl.first - $scope.ctrl.maxrows, 0),$scope.ctrl.first,50)
		
	}
		
	$scope.sort = function(sort) {
		if(sort==="TipoEmittente"){
			if($scope.ctrl.sortType==="+canale.bdcBroadcasters.tipo_broadcaster"){
				$scope.ctrl.sortType="-canale.bdcBroadcasters.tipo_broadcaster";
			}else{
				$scope.ctrl.sortType="+canale.bdcBroadcasters.tipo_broadcaster";
			}
		}else if(sort==="Emittente"){
			if($scope.ctrl.sortType==="+canale.bdcBroadcasters.nome"){
				$scope.ctrl.sortType="-canale.bdcBroadcasters.nome";
			}else{
				$scope.ctrl.sortType="+canale.bdcBroadcasters.nome";
			}
		}else if(sort==="Canale"){
			if($scope.ctrl.sortType==="+canale.nome"){
				$scope.ctrl.sortType="-canale.nome";
			}else{
				$scope.ctrl.sortType="+canale.nome";
			}
		}else if(sort==="DataDa"){
			if($scope.ctrl.sortType==="+inizioPeriodoCompetenza"){
				$scope.ctrl.sortType="-inizioPeriodoCompetenza";
			}else{
				$scope.ctrl.sortType="+inizioPeriodoCompetenza";
			}
		}else if(sort==="DataA"){
			if($scope.ctrl.sortType==="+finePeriodoCompetenza"){
				$scope.ctrl.sortType="-finePeriodoCompetenza";
			}else{
				$scope.ctrl.sortType="+finePeriodoCompetenza";
			}
		}else if(sort==="Incasso"){
			if($scope.ctrl.sortType==="+incassoNetto"){
				$scope.ctrl.sortType="-incassoNetto";
			}else{
				$scope.ctrl.sortType="+incassoNetto";
			}
		}else if(sort==="TipoDiritto"){
			if($scope.ctrl.sortType==="+tipoDiritto"){
				$scope.ctrl.sortType="-tipoDiritto";
			}else{
				$scope.ctrl.sortType="+tipoDiritto";
			}
		}else if(sort==="Consolidamento"){
			if($scope.ctrl.sortType==="+incasso"){
				$scope.ctrl.sortType="-incasso";
			}else{
				$scope.ctrl.sortType="+incasso";
			}
		}else if(sort==="Sospeso"){
			if($scope.ctrl.sortType==="-sospesoPerContenzioso"){
				$scope.ctrl.sortType="+sospesoPerContenzioso";
			}else{
				$scope.ctrl.sortType="-sospesoPerContenzioso";
			}
		}else if(sort==="Report"){
			if($scope.ctrl.sortType==="-repoDisponibile"){
				$scope.ctrl.sortType="+repoDisponibile";
			}else{
				$scope.ctrl.sortType="-repoDisponibile";
			}
		}else if(sort==="ImportoRipartibile"){
			if($scope.ctrl.sortType==="-importoRipartibile"){
				$scope.ctrl.sortType="+importoRipartibile";
			}else{
				$scope.ctrl.sortType="-importoRipartibile";
			}
		}
	}
	
	$scope.getNoteDisponibili = function(notaEmittenti,notaRipartimento) {
		if((notaEmittenti!=null && notaEmittenti != undefined &&notaEmittenti !=="")||
		   (notaRipartimento!=null && notaRipartimento != undefined &&notaRipartimento !=="")){
			return "Si";
		}else{
			return "No";
		}
			
	}
	
	$scope.formatDate = function(dateString) {
		var date = new Date(dateString)
		var separator = "-"
		var month = date.getMonth() + 1
		return (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + separator + (month < 10 ? "0" + month : month)
			+ separator + date.getFullYear()
	}
	
	$scope.getEmittenti = function(item) {
		$scope.ctrl.utenti = [];
		$scope.ctrl.selectedUser = "";
		var tipoBroadcaster = item
		if (tipoBroadcaster==="TUTTE") {
			tipoBroadcaster=null;
		}
		$http({
			method : 'GET',
			url : 'rest/broadcasting/emittentiFiltrati',
			params : {
				fiteredType : tipoBroadcaster
			}
		}).then(function successCallback(response) {
			$scope.ctrl.listaEmittenti = response.data;
			$scope.ctrl.listaEmittenti.unshift({
				"id" : -1,
				"nome" : "TUTTE",
				"tipo_broadcaster" : ""
			})
			$scope.ctrl.canale = {};
		}, function errorCallback(response) {
			$scope.ctrl.listaEmittenti = [];
			$scope.ctrl.emittente = {};
			$scope.ctrl.canale = {};
		});
	}
	
	$scope.getCanali = function(emittente) {
		var id =null;
		if (emittente==null||emittente==undefined||emittente.nome==="TUTTE") {
			id=null;
		}else{
			id=emittente.id
		}
		$http({
			method : 'GET',
			url : 'rest/broadcasting/canali/' + id,
			params : {

			}
		}).then(function successCallback(response) {
			$scope.ctrl.listaCanali = response.data;
			$scope.ctrl.listaCanali.unshift({
				"id" : -1,
				"nome" : "TUTTI",
				"tipo_broadcaster" : ""
			})
			$scope.ctrl.canale = {};
		}, function errorCallback(response) {
			$scope.ctrl.listaCanali = []
			$scope.ctrl.listaCanali.unshift({
				"id" : -1,
				"nome" : "TUTTI",
				"tipo_broadcaster" : ""
			})
			$scope.ctrl.canale = {};
		});

	};
	
	$scope.getListFiltered = function(item) {
		var results = item ? $scope.ctrl.listaEmittenti.filter($scope.createFilterFor(item)) : $scope.ctrl.listaEmittenti,
			deferred;

		return results;


	};

	$scope.createFilterFor = function(query) {
		var lowercaseQuery = angular.uppercase(query);

		return function filterFn(item) {
			return (item.nome.indexOf(lowercaseQuery) === 0);
		};

	}

	

	$scope.apply = function(tipoEmittente,emittente, canale, anno, dataFrom , dataTo) {
		//TODO Aggiungere la chiama al servizio che torna la lista dei carichi di ripartizione
		$scope.ctrl.selectedTipoBroadcaster=tipoEmittente
		$scope.ctrl.emittente=emittente
		$scope.ctrl.canale=canale
		$scope.ctrl.anno =anno
		$scope.ctrl.selectedPeriodFrom = dataFrom
		$scope.ctrl.selectedPeriodT = dataTo
		$scope.getCarichiRiparto(tipoEmittente,emittente, canale, anno, dataFrom, dataTo, 0, 50, 50)
	}
	
	
	$scope.download = function(tipoEmittente, emittente, canale, anno, dataFrom, dataTo, first, last, maxrow){
		var nomeEmittente=null;
		var idEmittente=null;
		var idCanale=null;
		
		if (tipoEmittente.nome==null||tipoEmittente.nome==undefined||tipoEmittente.nome==="TUTTE") {
			nomeEmittente=undefined;
		}else{
			nomeEmittente=tipoEmittente.nome
		}
		
		if (emittente==null||emittente==undefined||emittente.nome==="TUTTE") {
			idEmittente=null;
		}else{
			idEmittente=emittente.id
		}
		
		var id =null;
		if (canale==null||canale==undefined||canale.nome==="TUTTI") {
			idCanale=null;
		}else{
			idCanale=canale.id
		}
		
		$http({
			method : 'POST',
			url : 'rest/gestioneRipartizione/downloadRipartizioni',
    			headers: {
                    'Content-Type': 'application/json'},
			data : {
				dateFrom:dataFrom,
				dateTo:dataTo,
				tipoEmittente:nomeEmittente,
				idEmittente:idEmittente,
				idCanale:idCanale,
				first: first,
				last: last,
				maxrow:maxrow,
			}
		}).then(function successCallback(response) {
			saveAs(new Blob([response.data],{type:"application/octet-stream"}),"Ripartizioni "+dataFrom+"/"+dataTo+".csv");
		}, function errorCallback(response) {
			$scope.ctrl.carichiRipartizione = []
		});
	}
	
	
	$scope.getCarichiRiparto = function(tipoEmittente,emittente, canale, anno,  dataFrom, dataTo, first, last, maxrow){
		var nomeEmittente=null;
		var idEmittente=null;
		var idCanale=null;
		
		if (dataFrom==null||dataFrom==undefined||dataFrom===""||dataTo==null||dataTo==undefined||dataTo==="") {
			$scope.showAlert("", "Per effettuare la ricerca è obbligatorio inserire un periodo valido");
			return
		}
		
		if (tipoEmittente.nome==null||tipoEmittente.nome==undefined||tipoEmittente.nome==="TUTTE") {
			nomeEmittente=undefined;
		}else{
			nomeEmittente=tipoEmittente.nome
		}
		
		if (emittente==null||emittente==undefined||emittente.nome==="TUTTE") {
			idEmittente=null;
		}else{
			idEmittente=emittente.id
		}
		
		var id =null;
		if (canale==null||canale==undefined||canale.nome==="TUTTI") {
			idCanale=null;
		}else{
			idCanale=canale.id
		}
		
		$http({
			method : 'POST',
			url : 'rest/gestioneRipartizione/getRipartizioni',
    			headers: {
                    'Content-Type': 'application/json'},
			data : {
				dateFrom:dataFrom,
				dateTo:dataTo,
				tipoEmittente:nomeEmittente,
				idEmittente:idEmittente,
				idCanale:idCanale,
				first: first,
				last: last,
				maxrow:maxrow,
			}
		}).then(function successCallback(response) {
			$scope.ctrl.carichiRipartizione = response.data.rows;
			$scope.ctrl.first = response.data.first;
			$scope.ctrl.last = response.data.last;
		
		}, function errorCallback(response) {
			$scope.ctrl.carichiRipartizione = []
		});
	}
	
	
	//funzione che renderizza il popup per l'aggiunta di una configurazione del DSP
	$scope.editCaricoRiparto = function(item) {
		var rootScope = $scope
		var caricoRiparto =  angular.copy(item);
		
		ngDialog.open({
			template : 'pages/broadcasting/carichi-ripartizione/updateCaricoRipartizione.jsp',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', '$http', function($scope, ngDialog, $http) {
				
				$scope.getImportoRipartibile= function(item) {		
					if(item==null||item==undefined){
						return false
					}
					if(item.importoRipartibile==null||item.importoRipartibile==undefined){
						if(item.incasso==="CONSOLIDATO"&&item.repoDisponibile&&!sospesoPerContenzioso){
							return true
						}else{
							return false
						}
					}else{
						return item.importoRipartibile
					}
				}
				
				$scope.ctrl = { 
					lastImportoRipartibile : item.importoRipartibile,
					incassi: ["CONSOLIDATO","PARZIALE"],
					item: caricoRiparto,
					rootScope: rootScope
				};
				
				$scope.showAlert = function(title, message) {
					ngDialog.open({
						template : 'pages/advance-payment/dialog-alert.html',
						plain : false,
						width : '50%',
						controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
							var ngDialogId = $scope.ngDialogId;
							$scope.ctrl = {
								title : title,
								message : message,
								ngDialogId : ngDialogId
							};
							$scope.cancel = function(ngDialogId) {
								ngDialog.close(ngDialogId);
							};
						} ]
					}).closePromise.then(function(data) {
						if (data.value === true) {

						}
					});
				};

				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
	
				$scope.showConfirmAlert = function(username) {
					var rootScope = $scope
					ngDialog.open({
						template : 'pages/dialog/dialog-confirm-alert.html',
						plain : false,
						width : '50%',
						controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
							var ngDialogId = $scope.ngDialogId;
							$scope.ctrl = {
								title : "Info",
								message : "Sei sicuro di voler apportare la modifica?",
								ngDialogId : ngDialogId,
								item : username,
								rScope : rootScope
							};
							$scope.cancel = function(ngDialogId) {
								ngDialog.close(ngDialogId);
							};
							$scope.confirm = function(ngDialogId) {
								$scope.ctrl.rScope.save ($scope.ctrl.item);
								ngDialog.close(ngDialogId);
							};
						} ]
					}).closePromise.then(function(data) {
						if (data.value === true) {

						}
					});
				};
				
				$scope.save = function(username) {
					if($scope.ctrl.lastImportoRipartibile==caricoRiparto.importoRipartibile){
						caricoRiparto.importoRipartibile=null;
					}
					$http({
						method : 'POST',
						url : 'rest/gestioneRipartizione/updateRipartizioni',
						headers : {
							'Content-Type' : 'application/json'
						},
						data : {
							username : username,
							caricoRipartizione : caricoRiparto
						}
					}).then(function successCallback(response) {
						ngDialog.closeAll(false);
						item=caricoRiparto
						$scope.ctrl.rootScope.getCarichiRiparto($scope.ctrl.rootScope.ctrl.selectedTipoBroadcaster,$scope.ctrl.rootScope.ctrl.emittente, $scope.ctrl.rootScope.ctrl.canale, $scope.ctrl.rootScope.ctrl.anno, $scope.ctrl.rootScope.ctrl.selectedPeriodFrom, $scope.ctrl.rootScope.ctrl.selectedPeriodTo,0,50,50)
						$scope.showAlert("Esito", "Ripartizione Modificata Correttamente");
					}, function errorCallback(response) {
						if (response.status == 409) {
							$scope.showAlert("Errore", "Ripartizione non modificata.");
						} else {
							$scope.showAlert("Errore", response.statusText);
						}

					});


				};
			} ]
		}).closePromise.then(function(data) {
			if (data.value === true) {
				// no op
			}
		});


	};

	$scope.back= function(item) {
		$scope.ctrl.dettailSelected=null;
		$scope.ctrl.caricoRiparto=null;
		$scope.ctrl.storico = [];
		$scope.ctrl.split_items=null;
	}
		

	$scope.showCaricoRiparto = function(item) {
		$scope.ctrl.dettailSelected=item;
		$scope.ctrl.caricoRiparto=item;

		$http({
			method : 'GET',
			url : 'rest/gestioneRipartizione/getStorico',
			params : {
				idCaricoRipartizione : $scope.ctrl.caricoRiparto.id,
			}
		}).then(function successCallback(response) {
			$scope.ctrl.storico = response.data;
			if(	$scope.ctrl.caricoRiparto.kpi!=null&&	$scope.ctrl.caricoRiparto.kpi!=undefined){
				$scope.ctrl.listaGauge = 	$scope.ctrl.caricoRiparto.kpi;
				$scope.ctrl.split_items=[]
				var cont=0;
				var arrSubItem=[];
				for (i = 0; i < $scope.ctrl.listaGauge.length; i++) {
					if(cont==3){
						$scope.ctrl.split_items.push(arrSubItem)
						arrSubItem=[];
						cont=0;
					}
					arrSubItem.push($scope.ctrl.listaGauge[i])
					cont++;
				}
				$scope.ctrl.split_items.push(arrSubItem)
			}
		}, function errorCallback(response) {
			$scope.ctrl.storico = [];
		});
		
		
	};
	
	$scope.isRadio = function(){
		if( $scope.ctrl.caricoRiparto!=null&&$scope.ctrl.caricoRiparto!=undefined){
	 		return $scope.ctrl.caricoRiparto.canale.bdcBroadcasters.tipo_broadcaster==="RADIO";
		}
	}
	
	$scope.formatDateHour = function(dateString) {
		if (dateString != undefined && dateString != null) {
			var date = new Date(dateString)
			var separator = "-"
			var month = date.getMonth() + 1
			return (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + separator + (month < 10 ? "0" + month : month) + separator + date.getFullYear() + " " + (date.getHours() < 10 ? "0" + date.getHours() : date.getHours()) + ":" + (date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes())

		} else {
			return "";
		}
	}
	
	
	$scope.getTextFromBool= function(bool) {
		if(bool==null||bool==="null"||bool==undefined){
			return ""
		}
		if(bool===true){
			return "Si"
		}else if(bool===false){
			return "No"
		}
		
		if(bool==="true"){
			return "Si"
		}else if(bool==="false"){
			return "No"
		} else{
			return bool
		}
	}

	$scope.getTextReport= function(repoDisponibile,kpiValido) {
		if(repoDisponibile===true){
			return "Si"
		}else if(repoDisponibile===false){
			return "No"
		}	
	}

	$scope.getTextImportoRipartibile= function(item) {	
		if(item==null||item==undefined){
			return ""
		}
		if(item.importoRipartibile==null||item.importoRipartibile==undefined){
			if(item.incasso==="CONSOLIDATO"&&item.repoDisponibile&&!sospesoPerContenzioso){
				return "Si"
			}else{
				return "No"
			}
		}
		
		if(item.importoRipartibile===true){
			return "Si"
		}else if(item.importoRipartibile===false){
			return "No"
		}
		
	
	}
	
	
	$scope.showError = function(title, msgs) {
		ngDialog.open({
			template : 'pages/advance-payment/dialog-alert.html',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				$scope.ctrl = {
					title : title,
					message : msgs
				};
				$scope.cancel = function() {
					ngDialog.closeAll(false);
				};
				$scope.save = function() {
					ngDialog.closeAll(true);
				};
			} ]
		}).closePromise.then(function(data) {
			// no op
		});
	};

	$scope.showAlert = function(title, message) {
		ngDialog.open({
			template : 'pages/advance-payment/dialog-alert.html',
			plain : false,
			width : '50%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				var ngDialogId = $scope.ngDialogId;
				$scope.ctrl = {
					title : title,
					message : message,
					ngDialogId : ngDialogId
				};
				$scope.cancel = function(ngDialogId) {
					ngDialog.close(ngDialogId);
				};
			} ]
		}).closePromise.then(function(data) {
			if (data.value === true) {

			}
		});
	};

	
	
	$scope.onRefresh();

} ]);


codmanApp.filter('range', function() {
	return function(input, min, max) {
		min = parseInt(min); //Make string input int
		max = parseInt(max);
		for (var i = min; i <= max; i++)
			input.push(i);
		return input;
	};
});