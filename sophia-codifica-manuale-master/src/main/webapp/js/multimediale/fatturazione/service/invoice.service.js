(function () {
  'use strict';
  angular
    .module('codmanApp')
    .service('InvoiceService', InvoiceService);

  InvoiceService.$inject = ['$http', 'UtilService', 'configService', '$location'];

  function InvoiceService($http, UtilService, configService, $location) {
    var vm = this;
    vm.msInvoiceUrl = `${$location.protocol()}://${$location.host()}:${$location.port()}/${configService.msInvoiceApiPath}`;
    vm.getMaxRows = getMaxRows;
    vm.getCcid = getCcid;
    vm.startBackclaim = startBackclaim;
    vm.voidBackclaim = voidBackclaim;

    function getMaxRows() {
      return $http({
        method: 'GET',
        url: `${vm.msInvoiceUrl}invoice/max-rows`,
        responseType: 'text/plain'
      }).then(function successCallback(response) {
        return response.data;
      });
    }

    function getCcid(params, page) {
      var countryList = [];
      var dspList = [];
      var offerList = [];

      angular.forEach(params.country, el => {
        countryList.push(el.id);
      });

      angular.forEach(params.dsp, el => {
        dspList.push(el.id);
      });

      angular.forEach(params.commercialOffer, el => {
        offerList.push(el.id);
      });

      return $http({
        method: 'POST',
        url: `${vm.msInvoiceUrl}invoice/all/ccid`,
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          currentPage: page,
          clientId: params.client ? params.client.idAnagClient : void 0,
          dspList: dspList,
          countryList: countryList,
          utilizationList: params.utilizationList,
          offerList: params.offerList ? params.offerList : offerList,
          monthFrom: params.monthFrom,
          yearFrom: params.yearFrom,
          monthTo: params.monthTo,
          yearTo: params.yearTo,
          fatturato: params.fatturato,
          backclaim: params.backclaim,
          isBackclaim: params.isBackclaim,
          sortBy: params.sortBy,
          asc: params.asc
        }
      }).then(function successCallback(response) {
        return response.data;
      });

    }

    function startBackclaim(idDsr, bcType) {
      return new EventSource(`${vm.msInvoiceUrl}multimediale/backclaim/${idDsr}/${bcType}`);
    }

    function voidBackclaim(idDsr) {
      return $http.delete(`${vm.msInvoiceUrl}multimediale/backclaim/${idDsr}`)
        .then(function (response) {
          return response.data;
        });
    }
  }
})();
