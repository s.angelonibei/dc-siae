(function () {
  'use strict';
  angular
    .module('codmanApp')
    .service('anagClientService', anagClientService);

  anagClientService.$inject = ['$http', 'UtilService', 'ngDialog', '$location', 'configService'];

  function anagClientService($http, UtilService, ngDialog, $location, configService) {
    var vm = this;
    vm.MS_INVOICE_API_URL = $location.protocol() + '://' + $location.host() + ':' + $location.port() + '/' + configService.msInvoiceApiPath;

    vm.getClientsData = getClientsData;
    vm.getClients = getClients;
    vm.getDspCountries = getDspCountries;
    vm.filterDsp = filterDsp;
    vm.getPeriodLimits = getPeriodLimits;
    vm.getCommercialOffers = getCommercialOffers;
    vm.getDspsUtilizationsCommercialOffersCountries = getDspsUtilizationsCommercialOffersCountries;

    function getClientsData() {
      var promises = [
        getClients(),
        getDspCountries()
      ];
      return Promise.all(promises);
    }

    function filterDsp(idAnagClient, dsps) {
      vm.getClients(idAnagClient)
        .then(function (data) {
          if (data.length === 1) {
            dsps.dspOld = angular.copy(dsps.dsp);
            dsps.dsp = data[0].dspList;
          } else {
            dsps.dsp = angular.copy(dsps.dspOld);
          }
        });
    }

    function getClients(clientId) {
      return $http({
        method: 'GET',
        url: vm.MS_INVOICE_API_URL + 'anagClient/clients' + (clientId ? '/' + clientId : '')
      }).then(function successCallback(response) {
        return response.data;
      })
    }

    function getDspCountries() {
      return $http({
        method: 'GET',
        url: 'rest/data/dspCountries'
      }).then(function successCallback(response) {
        return response.data;
      })
    }

    function getPeriodLimits() {
      return $http({
        method: 'GET',
        url: 'rest/dsrMetadata/period-limits'
      }).then(function successCallback(response) {
        return response.data;
      })
    }

    function getCommercialOffers() {
     return $http({
       method: 'GET',
       url: 'rest/data/dspUtilizationsOffersCountries'
     }).then(function successCallback(response) {
        return response.data.commercialOffers;
     });
    }

    function getDspsUtilizationsCommercialOffersCountries() {
      return $http({
        method: 'GET',
        url: 'rest/data/dspUtilizationsOffersCountries'
      }).then(function successCallback(response) {
        return response.data;
      });
    }

  }
})();
