(function() {
  'use strict';
  function FilterDspCountrySapcodeCtrl() {
    var vm = this;
    vm.$onInit = onInit;
    vm.filterDsp = filterDsp;

    function onInit() {
      vm.values = vm.anagClientData[1];
      vm.values.anagClient = vm.anagClientData[0];
    }

    function filterDsp(value, values) {
      vm.onFilterDsp({value: value, values: values});
    }

    vm.apply = function() {
      var parameters = {
        client: vm.selectedClient ? vm.selectedClient.idAnagClient : '',
        dsp: vm.selectedDsp ? vm.selectedDsp.idDsp : '',
        country: vm.selectedCountry ? vm.selectedCountry.idCountry : '',
        sapCode: vm.selectedSapCode
      };
      vm.onFilterApply({parameters: parameters});
      // vm.$emit('filterApply', parameters);
    };

    vm.reset = function() {
      vm.selectedClient = {};
      vm.selectedDsp = {};
      vm.selectedCountry = {};
      vm.selectedSapCode = '';
    };
  }

  angular
    .module('codmanApp')
    .component('filterDspTerritorioSapcode', {
      templateUrl: 'pages/filters/filter-dsp-territorio-sapcode.html',
      controller: FilterDspCountrySapcodeCtrl,
      controllerAs: 'vm',
      bindings: {
        onFilterApply: '&',
        anagClientData: '<',
        onFilterDsp: '&'
      }
    });
})();
