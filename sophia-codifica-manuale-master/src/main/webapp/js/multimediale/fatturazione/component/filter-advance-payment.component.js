(function() {
  'use strict';

  function FilterAdvancePaymentCtrl() {
    var vm = this;
    vm.$onInit = onInit;
    vm.filterDsp = filterDsp;

    vm.ctrl = {
      months: [{'id': '1', 'name': 'Gennaio'}, {
        'id': '2',
        'name': 'Febbraio'
      }, {'id': '3', 'name': 'Marzo'}, {'id': '4', 'name': 'Aprile'},
      {'id': '5', 'name': 'Maggio'}, {
        'id': '6',
        'name': 'Giugno'
      }, {'id': '7', 'name': 'Luglio'}, {'id': '8', 'name': 'Agosto'},
      {'id': '9', 'name': 'Settembre'}, {
        'id': '10',
        'name': 'Ottobre'
      }, {'id': '11', 'name': 'Novembre'}, {'id': '12', 'name': 'Dicembre'}]
    };

    // Funzione principale richiamata al caricamento della pagina

    function filterDsp(value, values) {
      vm.onFilterDsp({value: value, values: values});
    }

    function onInit() {
      vm.values = vm.anagClientData[1];
      vm.values.anagClient = vm.anagClientData[0];
    }

    vm.getCollectionValues = function(collection) {
      var selectedList = [];
      if (collection) {
        for (var e in collection) {
          selectedList.push(collection[e].id);
        }
      }
      return selectedList;
    };

    vm.apply = function() {
      var periodFrom, yearFrom, periodTo, yearTo;

      if ((vm.selectedPeriodFrom)) {
        var from = vm.selectedPeriodFrom.split('-');
        periodFrom = parseInt(from[0], 10);
        yearFrom = parseInt(from[1], 10);
      }

      if (vm.selectedPeriodTo) {
        var to = vm.selectedPeriodTo.split('-');
        periodTo = parseInt(to[0], 10);
        yearTo = parseInt(to[1], 10);
      }

      var parameters = {
        dsp: vm.selectedDsp,
        selectedClient: vm.selectedClient,
        advancePaymentCode: vm.selectedInvoiceNumber,
        monthFrom: periodFrom,
        yearFrom: yearFrom,
        monthTo: periodTo,
        yearTo: yearTo,
        showStandBy: vm.showStandBy,
        hideForm: false
      };
      vm.onFilterApply({parameters: parameters});
    };
  }

  angular
    .module('codmanApp')
    .component('filterAdvancePayment', {
      templateUrl: 'pages/filters/filter-advance-payment.html',
      controller: FilterAdvancePaymentCtrl,
      controllerAs: 'vm',
      bindings: {
        anagClientData: '<',
        selectedClient: '<',
        onFilterApply: '&',
        onFilterDsp: '&',
      }
    });
})();
