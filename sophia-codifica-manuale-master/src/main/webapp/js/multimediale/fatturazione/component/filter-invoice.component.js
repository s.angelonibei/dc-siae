(function() {
  'use strict';

  function FilterInvoiceCtrl($http) {
    var vm = this;
    vm.$onInit = onInit;
    vm.filterDsp = filterDsp;
    vm.apply = apply;

    vm.selectedCountryModel = [];
    vm.selectedUtilizationModel = [];
    vm.selectedOfferModel = [];
    vm.selectedDsp = [];

    function filterDsp(value, values) {
      vm.onFilterDsp({value: value, values: values});
    }

    function buildKeySets() {
      vm.countrySelectionList = [];
      _.forEach(vm.countryList, function(value) {
        vm.countrySelectionList.push({
          label: value.name,
          id: value.idCountry
        });
      });

      vm.utilizationSelectionList = [];
      _.forEach(vm.utilizationList, function(value) {
        vm.utilizationSelectionList.push({
          label: value.name,
          id: value.idUtilizationType
        });
      });

      vm.offerSelectionList = [];
      _.forEach(vm.commercialOffersList, function(value) {
        vm.offerSelectionList.push({
          label: value.offering,
          id: value.offering
        });
      });
    }

    vm.months = [{'id': '1', 'name': 'Gennaio'}, {'id': '2', 'name': 'Febbraio'}, {'id': '3', 'name': 'Marzo'}, {'id': '4', 'name': 'Aprile'},
      {'id': '5', 'name': 'Maggio'}, {'id': '6', 'name': 'Giugno'}, {'id': '7', 'name': 'Luglio'}, {'id': '8', 'name': 'Agosto'},
      {'id': '9', 'name': 'Settembre'}, {'id': '10', 'name': 'Ottobre'}, {'id': '11', 'name': 'Novembre'}, {'id': '12', 'name': 'Dicembre'}];

    // Funzione principale richiamata al caricamento della pagina
    function onInit() {
      if (vm.selectedClient) {
        vm.hideClient = true;
      }

      if(vm.dateFrom){
        vm.selectedPeriodFrom = vm.dateFrom;
      }
      if(vm.dateTo){
        vm.selectedPeriodTo = vm.dateTo;
      }
      $http({
        method: 'GET',
        url: 'rest/data/dspUtilizationsOffersCountries'
      }).then(function successCallback(response) {
        vm.utilizationList = response.data.utilizations;
        vm.commercialOffersList = response.data.commercialOffers;
        vm.countryList = response.data.countries;

        buildKeySets();
      });
    }

    function getCollectionValues(collection) {
      var selectedList = [];
      if (collection !== 'undefined') {
        for (var e in collection) {
          selectedList.push(collection[e].id);
        }
      }
      return selectedList;
    }

    function getCollectionValuesIdDsp(collection) {
      var selectedList = [];
      if (collection !== 'undefined') {
        for (var e in collection) {
          selectedList.push(collection[e].idDsp);
        }
      }
      return selectedList;
    }

    function apply()	{
      var periodFrom, yearFrom, periodTo, yearTo;

      if ((vm.selectedPeriodFrom)) {
        var from = vm.selectedPeriodFrom.split('-');
        periodFrom = parseInt(from[0], 10);
        yearFrom = parseInt(from[1], 10);
      }

      if (vm.selectedPeriodTo) {
        var to = vm.selectedPeriodTo.split('-');
        periodTo = parseInt(to[0], 10);
        yearTo = parseInt(to[1], 10);
      }

      var parameters = {
        selectedClient: vm.selectedClient,
        dsp: getCollectionValues(vm.selectedDsp),
        countryList: getCollectionValues(vm.selectedCountryModel),
        utilizationList: getCollectionValues(vm.selectedUtilizationModel),
        offerList: getCollectionValues(vm.selectedOfferModel),
        monthFrom: periodFrom,
        yearFrom: yearFrom,
        monthTo: periodTo,
        yearTo: yearTo,
        hideForm: false
      };
      vm.onFilterApply({parameters: parameters});
    }

    vm.multiselectSettings = {
      scrollableHeight: '200px',
      scrollable: false,
      enableSearch: true
    };

    vm.multiselectDsp = {
      scrollableHeight: '200px',
      scrollable: false,
      enableSearch: true,
      displayProp: 'name',
      idProp: 'idDsp'
    };
  }

  FilterInvoiceCtrl.$inject = ['$http'];

  angular
    .module('codmanApp')
    .component('filterInvoice', {
      templateUrl: 'pages/filters/filter-invoice.html',
      controller: FilterInvoiceCtrl,
      controllerAs: 'vm',
      bindings: {
        anagClientData: '<',
        selectedClient: '<',
        onFilterApply: '&',
        onFilterDsp: '&',
        dateFrom: '<',
        dateTo: '<'
      }
    });
})();
