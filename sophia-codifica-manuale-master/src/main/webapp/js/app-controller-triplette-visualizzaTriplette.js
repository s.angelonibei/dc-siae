codmanApp.controller('VisualizzaTripletteCtrl', ['$scope', '$routeParams', '$route', '$http', '$location', 'ngDialog', '$rootScope', '$anchorScroll', 'configService', 'triplettaContext',
	function ($scope, $routeParams, $route, $http, $location, ngDialog, $rootScope, $anchorScroll, configService, triplettaContext) {

		$scope.ctrl = {
			msIngestionApiUrl: `${$location.protocol()}://${$location.host()}:${$location.port()}/${configService.msIngestionApiPath}`,
			tipiEmittenti: [{
				'nome': 'TUTTI'
			}, {
				'nome': 'TELEVISIONE'
			}, {
				'nome': 'RADIO'
			}],
			tipiDiritto: ['DEM', 'DRM'],
			statiTripletta: ['VALIDA','DISATTIVA'],
			emittenti: [],
			canaleSelezionato: null,
			filterTripletta: ''
		};

		$scope.nuovaTripletta = function () {
			triplettaContext.mode = 'new';
			triplettaContext.tripletta = {};
			$scope.setState();
			$scope.goTo('/gestioneTripletta', '');
		}

		$scope.dettaglioTripletta = function (tripletta) {
			triplettaContext.tripletta = tripletta;
			triplettaContext.mode = 'detail';
			$scope.setState();
			$scope.goTo('/gestioneTripletta', '');
		}

		$scope.setState = function () {
			triplettaContext.state = (function stateIIFE() {
				let orderBy = $scope.orderBy;
				let page = $scope.ctrl.page;
				let selectedTipoBroadcaster = $scope.ctrl.selectedTipoBroadcaster;
				let emittente = $scope.ctrl.emittente;
				let canaleSelezionato = $scope.ctrl.canaleSelezionato;
				let listaCanali = $scope.ctrl.listaCanali;
				let tipoDiritto = $scope.ctrl.tipoDiritto;
				let statoTripletta = $scope.ctrl.statoTripletta;
				let filterTripletta = $scope.ctrl.filterTripletta;
				return {
					location : $location.path(),
					reset: function(scope) {
						scope.orderBy = orderBy;
						scope.ctrl.page = page;
						scope.ctrl.selectedTipoBroadcaster = selectedTipoBroadcaster;
						scope.ctrl.emittente = emittente;
						scope.ctrl.canaleSelezionato = canaleSelezionato;
						scope.ctrl.listaCanali = listaCanali;
						scope.ctrl.tipoDiritto = tipoDiritto;
						scope.ctrl.statoTripletta =statoTripletta;
						scope.ctrl.filterTripletta = filterTripletta;
					}
				};
			}());			
		}

		$scope.goTo = function (url, param) {
			if (url != undefined && param != undefined) {
				$location.path(url + param);
			}
		};		

		$scope.showAlert = function (title, message) {
			ngDialog.open({
				template: 'pages/advance-payment/dialog-alert.html',
				plain: false,
				width: '50%',
				controller: ['$scope', 'ngDialog', function ($scope, ngDialog) {
					var ngDialogId = $scope.ngDialogId;
					$scope.ctrl = {
						title: title,
						message: message,
						ngDialogId: ngDialogId
					};
					$scope.cancel = function (ngDialogId) {
						ngDialog.close(ngDialogId);
					};
				}]
			}).closePromise.then(function (data) {});
		};

		$scope.getEmittenti = function () {
			$http({
				method: 'GET',
				url: $scope.ctrl.msIngestionApiUrl + 'broadcasters'
			}).then(function (response) {
				$scope.ctrl.emittenti = response.data;
			}, function (error) {
				$scope.showAlert('', 'Errore recupero emittenti');
			})
		}

		$scope.getCanali = function (emittente) {
			if (!emittente) {
				$scope.ctrl.listaCanali = [];
				$scope.ctrl.canaleSelezionato = null;
				return;
			}
			$http({
				method: 'GET',
				url: `${$scope.ctrl.msIngestionApiUrl}broadcasters/${emittente.id}/canali`,
				params: {

				}
			}).then(function successCallback(response) {
				$scope.ctrl.listaCanali = response.data;
				$scope.ctrl.canaleSelezionato = null;
			}, function errorCallback(response) {
				$scope.ctrl.listaCanali = [];
				$scope.ctrl.canaleSelezionato = null;
			});
		};

		$scope.init = function () {
			if(triplettaContext.state.location && triplettaContext.state.location.startsWith('/anagraficaTriplette')){
				triplettaContext.state.reset($scope);
				if (triplettaContext.history && triplettaContext.history.length > 0) {
					triplettaContext.state = triplettaContext.history[0];
					triplettaContext.history.shift();
				}
				$scope.getSampling(true);
			}
			$scope.getEmittenti();
		}

		$scope.orderImage = function (name, param) {

			//get id of html element clicked
			var id = "#" + name;

			//get css class of that html element for change the image of arrows
			var classe = angular.element(id).attr("class");

			//set to original position all images of arrows, on each columns.
			var element1 = document.getElementsByClassName("glyphicon glyphicon-sort-by-attributes");
			var element2 = document.getElementsByClassName("glyphicon glyphicon-sort-by-attributes-alt");
			angular.element(element1).attr("class", "glyphicon glyphicon-sort");
			angular.element(element2).attr("class", "glyphicon glyphicon-sort");

			if (classe != "glyphicon glyphicon-sort-by-attributes" || classe == "glyphicon glyphicon-sort") {

				angular.element(id).attr("class", "glyphicon glyphicon-sort-by-attributes");

			} else {
				angular.element(id).attr("class", "glyphicon glyphicon-sort-by-attributes-alt");
			}

			$scope.order(param);

		}

		$scope.order = function (param) {
			if ($scope.orderBy == param.concat(" asc")) {
				$scope.orderBy = param.concat(" desc");
			} else {
				$scope.orderBy = param.concat(" asc");
			}
			$scope.getSampling(false);
		}

		$scope.filterApply = function () {
			$scope.ctrl.page = 0;
			$scope.getSampling(true);
		}

		$scope.navigateToNextPage = function () {
			$scope.ctrl.page = $scope.ctrl.page + 1;
			$scope.getSampling(false);

		};

		$scope.navigateToPreviousPage = function () {
			$scope.ctrl.page = $scope.ctrl.page - 1;
			$scope.getSampling(false);

		};

		$scope.navigateToEndPage = function () {
			$scope.ctrl.page = $scope.pageTot;
			$scope.getSampling(false);
		}

		//il flag è usato per la count. Se è vero allora fa la count.
		$scope.getSampling = function (flag) {

			$scope.params = {
				idEmittente: ($scope.ctrl.emittente && $scope.ctrl.emittente.id) ? $scope.ctrl.emittente.id : '',
				idCanale: ($scope.ctrl.canaleSelezionato && $scope.ctrl.canaleSelezionato.id) ? $scope.ctrl.canaleSelezionato.id : '',
				tipoEmittente: ($scope.ctrl.selectedTipoBroadcaster && $scope.ctrl.selectedTipoBroadcaster.nome && $scope.ctrl.selectedTipoBroadcaster.nome != 'TUTTI')? $scope.ctrl.selectedTipoBroadcaster.nome : '',
				tipoDiritto: $scope.ctrl.tipoDiritto ? $scope.ctrl.tipoDiritto : '' ,
				stato: $scope.ctrl.statoTripletta? $scope.ctrl.statoTripletta : '' ,
				page: $scope.ctrl.page || 0,
				count: flag,
				order: $scope.orderBy || 'tripletta asc',
				tripletta: $scope.ctrl.filterTripletta || ''
			}

			$http({
				method: 'GET',
				url: $scope.ctrl.msIngestionApiUrl + 'triplette',
				params: $scope.params
			}).then(function (response) {
				if (flag == true) {
					$scope.pageTotTemp = Math.floor(response.data.count / 50);
					$scope.totRecordsTemp = response.data.count;
					if (response.data.count > 1000) {
						$scope.showWarning("Attenzione", "Il risultato prevede " + response.data + " record, è consigliabile applicare dei filtri aggiuntivi. Sicuro di voler continuare?");
					} else if (response.data.count == 0) {
						$scope.fillElements(null, null, null, null, null, null);
						$scope.showAlert("", "Non risulta nessun dato per la ricerca effettuata");
					} else {
						$rootScope.pageTot = $scope.pageTotTemp;
						$rootScope.totRecords = $scope.totRecordsTemp;
						$scope.getSampling(false);
					}
				} else {
					$scope.fillElements($scope.totRecords, $scope.ctrl.page, 0, response.data.list.length, response.data.list, $scope.pageTot);
				}

			}, function (response) {
				$scope.fillElements(null, null, null, null, null, null);
				$scope.showAlert("", "Non risulta nessun dato per la ricerca effettuata");
			})
		};

		//a seconda di come va la chiamata riempie gli oggetti che servono alla view
		$scope.fillElements = function (totRecords, page, first, last, sampling, pageTot) {
			$scope.ctrl.totRecords = totRecords;
			$scope.ctrl.page = page || 0;
			$scope.ctrl.first = first;
			$scope.ctrl.last = totRecords;
			$scope.ctrl.sampling = $scope.adaptCanali(sampling);
			$scope.ctrl.pageTot = pageTot;
		}

		$scope.adaptCanali = function (records) {
			if (records && records.length > 0) {
				for (let record of records) {
					let canali = record.nomiCanali;
					if (canali) {
						if (canali.length > 80) {
							let canaliStrBuffer = '';
							for (const canale of canali.split(',')) {
								if (!canaliStrBuffer) {
									canaliStrBuffer = canale.trim();
								} else
								if (canaliStrBuffer.length + canale.length < 80) {
									canaliStrBuffer = canaliStrBuffer + ', ' + canale.trim();
								}
							}
							record = Object.assign(record, {longChannelList: true, canaliShorted: canaliStrBuffer, canaliKey: 'canaliShorted'});
						} else {
							record = Object.assign(record, {longChannelList: false, canaliKey: 'nomiCanali'});
						}
					}
				}
			}
			return records;
		}

	}
]);