codmanApp.controller("CarichiRipartizioneCtrl", ["$scope", "CarichiRipartizioneService", function($scope, CRService){

	/*
	 * Funzionamento pagina (almeno quello che dovrebbe essere):
	 * - al caricamento faccio la chiamata per ottenere i periodi di ripartizione e i carichiRipartizione per il periodo attuale
	 * - mi salvo il codicePeriodo (es. 182) del periodo attuale
	 * - se si sta visualizzando il periodo attuale allora è possibile selezionare gli input per scegliere quale voce incasso ripartire
	 *  ed è possibile cliccare il pulsante approva (visibile in questo caso),
	 *  sennò gli input non saranno cliccabili ma rappresenteranno la scelta precedentemente fatta (ovvero quando quel periodo era quello in corso),
	 *  ovvero alcuni saranno riempiti con l'immagine verde e alcuni vuoti, a seconda di quelli selezionati quando ripartiti,
	 *  mentre il pulsante non sarà visibile.
	 *  
	 */

	
	CRService.getInfoValorizzatore().then(function(response){
		if (!response.status === 204)
			$scope.infoValorizzatore = response.data
	},function(response){
		CRService.showError("Errore","Impossibile ottenere lo stato del valorizzatore")
	})
	
	$scope.carichiRipartizione = {}
	
	$scope.user = angular.element("#headerLinksBig").text().trim();
	
	CRService.getPeriodiRipartizione().then(function(response){
		$scope.periodiRipartizione = response.data
	},function(response){
		CRService.showError("Errore","Impossibile ottenere i periodi di ripartizione")
	})
	
	$scope.chiamata = function(){
		$scope.init($scope.codicePeriodo)
	}
	
	$scope.getPeriodoCompetenza = function(idPeriodo){
		for (var i = 0; i < $scope.periodiRipartizione.length; i++) {
			if($scope.periodiRipartizione[i].idPeriodoRipartizione==idPeriodo){
				return $scope.periodiRipartizione[i].codice;
			}
		}
	}
	$scope.getPeriodoCompetenzaCompleto = function(codice){
		if($scope.periodiRipartizione!=null||$scope.periodiRipartizione!=undefined){
			for (var i = 0; i < $scope.periodiRipartizione.length; i++) {
				if($scope.periodiRipartizione[i].codice==codice){
					return $scope.periodiRipartizione[i].codice +" ("+$scope.parseDate($scope.periodiRipartizione[i].competenzeDa) +" / "+ $scope.parseDate($scope.periodiRipartizione[i].competenzeA) + " )"	
				}
			}
		}
	}
	
	$scope.CarichiVuoti = function(carichiRipartizione,carryOver){
		if(	carichiRipartizione.length == 0 && Object.keys(carryOver).length == 0){
			return true
		}else{
			return false;
		}
	}
	
	$scope.init = function(codicePeriodo){
		$scope.carichiRipartizione =[];
		$scope.carryOver=[];
		CRService.getCarichiRipartizione(codicePeriodo).then(function(response){
			$scope.carichiRipartizione = response.data.carichiRipartizione
			$scope.carryOver = response.data.carryOver
			//ottengo dalla chiamata il periodo attuale e me lo salvo per gestire l'uso degli input e 
			//del pulsante (che possono essere cliccati SOLO SE stiamo nel periodo in corso). 
			//Mentre si scrive questo commento il periodo in corso è il secondo semestre del 2018 (attualmente i periodi vengono divisi per semestri, è variabile!)
			if(!$scope.codicePeriodoInCorso){
				$scope.codicePeriodoInCorso = response.data.codicePeriodo
				$scope.codicePeriodo=response.data.codicePeriodo;
				$scope.ripartiti=response.data.periodiRipartiti;
			}
		},function(response){
			CRService.showError("Errore","Impossibile ottenere i carichi di ripartizione")
		})
	}
	
	$scope.approva = function(carichiRipartizione,carryOver){
		
		CRService.approva(user,carichiRipartizione,carryOver).then(function(response){
			CRService.showError("Successo","Il carico di ripartizione per il periodo " + $scope.codicePeriodoInCorso + " è stato approvato con successo")
			location.reload();
		},function(response){
			CRService.showError("Errore","Non è stato possibile approvare il carico di ripartizione del periodo: " + $scope.codicePeriodoInCorso)
		})
	}
	
	$scope.parseDate = function(date){
		return CRService.parseDate(date);
	}
	
}])

codmanApp.service("CarichiRipartizioneService", ["$http", 'ngDialog', function($http, ngDialog){
	
	var service = this
	
	service.getPeriodiRipartizione = function(){
		return $http({
			method: 'GET',
			url: 'rest/performing/valorizzazione/getPeriodiRipartizione'
		})
	}
	
	service.getCarichiRipartizione = function(codicePeriodo){
		return $http({
			method: 'GET',
			url: 'rest/performing/valorizzazione/getCarichiRipartizione',
			params: {
				codicePeriodo: codicePeriodo
			}
		})
	}
	
	service.getInfoValorizzatore = function(){
		return $http({
			method: 'GET',
			url: 'rest/performing/valorizzazione/getInfoValorizzatore'
		})
	}
	
	service.approva = function(user,carichiRipartizione,carryOver){
		var listCarryOver=[];
		angular.forEach(carryOver, function(value, key) {
			angular.forEach(value, function(val, k) {
				listCarryOver.push(val);
			});
		});
		
		return $http({
			method: 'PUT',
			url: 'rest/performing/valorizzazione/approvazionePeriodoRipartizione',
			data: {
				user:user.value,
				carichiRipartizione: carichiRipartizione,
				carryOver: listCarryOver
			}
		})
	}
	
	service.showError = function(title,msgs){
		return ngDialog.open({
			template : 'pages/advance-payment/dialog-alert.html',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				var ngDialogId = $scope.ngDialogId;
				$scope.ctrl = {
					title : title,
					message : msgs,
					ngDialogId : ngDialogId
				};
				$scope.cancel = function() {
					ngDialog.close(ngDialogId);
				};
			}]
		})
	}
	
	service.askConfirm = function(title,msgs){
		return ngDialog.open({
			template : 'pages/advance-payment/dialog-confirm.html',
			plain : false,
			width : '60%',
			controller : [ '$scope', 'ngDialog', function($scope, ngDialog) {
				var ngDialogId = $scope.ngDialogId;
				$scope.ctrl = {
					title : title,
					message : msgs,
					ngDialogId : ngDialogId
				};
				$scope.cancel = function(ngDialogId) {
					ngDialog.close(ngDialogId);
				};
				$scope.save = function() {
					ngDialog.closeAll(true);
				};
			} ]
		})
	}
	
	service.parseDate = function(date){
		
		if(!date){
			return ""
		}
		
		var year = date.split("-")[0]
		var month = date.split("-")[1]
		var day = date.split("-")[2].split("T")[0]
		
		return day + "-" + month + "-" + year
	}
	
}])
