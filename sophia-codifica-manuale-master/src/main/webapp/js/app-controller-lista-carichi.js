codmanApp.controller('listaCarichiCtrl', [ '$scope','$route','$interval', 'configService', '$routeParams','configurationsService','ngDialog', 'avvioRipartizioneService','$location','$filter',
 function($scope, $route, $interval, configService, $routeParams, configurationsService, ngDialog, avvioRipartizioneService, $location,$filter) {

	$scope.paging = {};
	$scope.first = 0;
	$scope.last = 50;
	$scope.maxRows = 10;
    $scope.page = 0;
	$scope.filter = {
		year: void 0,
		month:  void 0,
		stato: void 0,
		idRipartizioneSiada:void 0 
	} 

	$scope.messages = {
        info: "", 
        error: "",
        warning: ""
    }
   
    $scope.clearMessages = function() {
        $scope.messages = {
            info: "", 
            error: "",
            warning: ""
        }
    }

    $scope.warningMessage = function(message){
        $scope.messages.warning = message;
    }

    $scope.infoMessage = function(message) {
        $scope.messages.info = message;
    }

    $scope.errorMessage = function(message) {
        $scope.messages.error = message;
    }

	$scope.init = function(){
		$scope.clearMessages();
		avvioRipartizioneService.getCarichiList($scope.page, $scope.maxRows, $scope.filter).then(function(response){
			if(response.data.rows.length < 1){
				$scope.paging = {... $scope.firstPaging};
				$scope.response = [];
				$scope.getCarichiList = [];
				$scope.filter = {
					year: void 0,
					month:  void 0,
					stato: void 0,
					idRipartizioneSiada:void 0 
				}
				return;
			}
			$scope.paging.currentPage = response.data.currentPage;
                $scope.paging.maxrows = response.data.maxrows;
                if ($scope.paging.currentPage == 0) {
                    $scope.paging.firstRecord = 1;
                    $scope.paging.lastRecord = $scope.paging.maxrows;
                } else {
                    $scope.paging.firstRecord = (($scope.paging.currentPage) * $scope.paging.maxrows) + 1;
                    $scope.paging.lastRecord = ($scope.paging.currentPage) * $scope.paging.maxrows + response.data.rows.length;
                }
			$scope.getCarichiList = response.data.rows;
			$scope.paging.hasPrev = response.data.hasPrev;
			$scope.paging.hasNext = response.data.hasNext;
		},function(error){
			$scope.errorMessage("Si è verificato un problema, riprovare più tardi!");
		});
	}

	$scope.applyFilter = function(request){
		$scope.clearMessages();
		var month, year;
		if ((request.dataCreazione)) {
		  var date = request.dataCreazione.split('-');
		  $scope.filter.month = parseInt(date[0], 10);
		  $scope.filter.year = parseInt(date[1], 10);
		}
		$scope.filter.stato = request.stato? request.stato : void 0;
		$scope.filter.idRipartizioneSiada = request.idRipartizioneSiada? request.idRipartizioneSiada : void 0;
		$scope.init();
	}

	$scope.init();

    $scope.downloadFile = function (file) {
		$scope.clearMessages();
		avvioRipartizioneService.downloadFile(file.id)
			.then(function (response) {
				var header = response.headers();
				var blob = new Blob([response.data], { type: "application/vnd.ms-excel;charset=UTF-8" });
				saveAs(blob, header['content-disposition'].replace('attachment;filename=',''));
			}, function (error) {
				$scope.errorMessage("Non è possibile scaricare il file selezionato!");
			});
	}

	$scope.goToProduzioneCarichi = function(){
		$scope.goTo('/produzioneCarichi','');
	}

   
    $scope.goTo = function (url, param) {
        if (url != undefined && param != undefined) {
            $location.path(url + param);
        }
	};
	
	$scope.navigateToPreviousPage = function () {
		$scope.page = $scope.page - 1;
		$scope.init();
	}

	$scope.navigateToNextPage = function () {
		$scope.page = $scope.page + 1;
		$scope.init();
	}


	$scope.confermaRipartizione = function (id){
		$scope.clearMessages();
		var scope = $scope;
        ngDialog.openConfirm({
			template: 'pages/ripartizione/dialog-alert.html',
			plain: false,
			data: {
				id: id
			},
			controller:  function () {
				var ctrl = this;
				ctrl.title= "Avvia Ripartizione";
				ctrl.message= `Confermi la ripartizione del carico selezionato?`;
			},
			controllerAs: 'ctrl'
        }).then(function confirm(data) {
			avvioRipartizioneService.avviaRipartizione(id).then(
				function success(response){
					console.log('RESPONSE', response);
					scope.init();
					scope.infoMessage('La ripartizione è stata avviata con successo');
					$('#rightPanel')[0].scrollIntoView( true );
                    window.scrollBy(0, -100);
				
				},
				function error(response){
					scope.errorMessage(response.data.message);
					$('#rightPanel')[0].scrollIntoView( true );
                    window.scrollBy(0, -100);
				}
			);
        }, function cancel() {
          //no op
        });
	};



}]);

codmanApp.service('avvioRipartizioneService', ['$http','$q', 'configService','$location', function($http,$q,configService,$location){

	var service = this;

    service.msInvoiceApiUrl = `${$location.protocol()}://${$location.host()}:${$location.port()}/${configService.msInvoiceApiPath}`;


    service.getCarichiList = function(pages, maxRows, filter){
        return $http({
			method: 'POST',
			url: service.msInvoiceApiUrl + 'invoice/carico/list' ,
			headers: {
                'Content-Type': 'application/json'
              },
			  data: {  
				currentPage: pages,
                maxRows: maxRows,
			 	year: filter.year?filter.year : void 0,
				month: filter.month?filter.month : void 0,
			 	stato: filter.stato?filter.stato : void 0,
			 	idRipartizioneSiada: filter.idRipartizioneSiada?filter.idRipartizioneSiada : void 0  
			 }
		});

    }



	service.downloadFile = function (id) {
		return $http({
			method: 'GET',
			responseType: 'blob',
			url: service.msInvoiceApiUrl + 'invoice/carico/'+id
		});
	}

	service.avviaRipartizione = function(id){
        return $http({
			method: 'POST',
			url: service.msInvoiceApiUrl + 'invoice/avvia-ripartizione/'+id ,
			headers: {
                'Content-Type': 'application/json'
              }
		});

    }

}]);