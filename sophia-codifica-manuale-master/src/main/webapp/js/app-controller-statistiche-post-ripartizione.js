codmanApp.controller('statistichePostRipartizioneCtrl', ['$scope', '$route', '$interval', 'configService', '$routeParams', 'configurationsService', 'ngDialog', 'statistichePostRipartizioneService', '$location', '$filter',
    function($scope, $route, $interval, configService, $routeParams, configurationsService, ngDialog, statistichePostRipartizioneService, $location, $filter) {

        $scope.pageTitle = `Statistiche`;
        $scope.idRipartizioneSiada = '';
        $scope.paging = {};
        $scope.first = 0;
        $scope.last = 50;
        $scope.maxRows = 10;
        $scope.page = 0;
        $scope.getStatisticheList = [];

        $scope.headerTitles = ["Società di riferimento",
            "Nome CCID",
            "Nome DSP",
            "Valore lordo (€)",
            "Valore aggio DEM (€)",
            "Valore trattenuta DEM (€)",
            "Valore aggio DRM (€)",
            "Valore netto (€)",
            "Valore netto DEM (€)",
            "Valore netto DRM (€)",
            "Download"
        ];


        $scope.init = function() {
            statistichePostRipartizioneService.getListIdSiada().then(function(success) {
                console.log(success.data);
                $scope.idSiadaList = success.data;
            }, function(error) {
                console.log('ERRORE', error);
            });
        }

        $scope.init();

        $scope.messages = {
            info: "",
            error: "",
            warning: ""
        }

        $scope.clearMessages = function() {
            $scope.messages = {
                info: "",
                error: "",
                warning: ""
            }
        }

        $scope.warningMessage = function(message) {
            $scope.messages.warning = message;
            $("#rightPanel")[0].scrollIntoView(true);
            window.scrollBy(0, -100);
        };

        $scope.infoMessage = function(message) {
            $scope.messages.info = message;
            $("#rightPanel")[0].scrollIntoView(true);
            window.scrollBy(0, -100);
        };

        $scope.errorMessage = function(message) {
            $scope.messages.error = message;
            $("#rightPanel")[0].scrollIntoView(true);
            window.scrollBy(0, -100);
        };


        $scope.applyFilter = function(id) {
            $scope.clearMessages();
            statistichePostRipartizioneService.getStatisticheList(id, true, $scope.page, $scope.maxRows)
                .then(function(response) {
                    if (response.data.rows.length < 1) {
                        $scope.paging = {...$scope.firstPaging };
                        $scope.response = [];
                        $scope.getStatisticheList = [];
                        return;
                    }
                    $scope.paging.currentPage = response.data.currentPage;
                    $scope.paging.maxrows = response.data.maxrows;
                    if ($scope.paging.currentPage == 0) {
                        $scope.paging.firstRecord = 1;
                        $scope.paging.lastRecord = $scope.paging.maxrows;
                    } else {
                        $scope.paging.firstRecord = (($scope.paging.currentPage) * $scope.paging.maxrows) + 1;
                        $scope.paging.lastRecord = ($scope.paging.currentPage) * $scope.paging.maxrows + response.data.rows.length;
                    }
                    $scope.getStatisticheList = response.data.rows;
                    $scope.paging.hasPrev = response.data.hasPrev;
                    $scope.paging.hasNext = response.data.hasNext;
                }, function(error) {
                    $scope.errorMessage("Si è verificato un problema, riprovare più tardi!");
                });

        }



        $scope.downloadFile = function(file) {
            $scope.clearMessages();
            var fileName = 'file648_' + file.idRipartizioneSiada + '_' + file.nominativo + '_' + file.idDsr + '.csv';
            statistichePostRipartizioneService.downloadFile(file.id)
                .then(function(response) {
                    var header = response.headers();
                    if (response.data.size != 0) {
                        var blob = new Blob([response.data], { type: "application/vnd.ms-excel;charset=UTF-8" });
                        console.log('response', response);
                        saveAs(blob, fileName);
                    } else {
                        $scope.warningMessage("La produzione del file 648 è ancora in corso!");
                    }

                }, function(error) {
                    $scope.warningMessage("La produzione del file 648 è ancora in corso!");
                });
        }


        $scope.navigateToPreviousPage = function() {
            $scope.page = $scope.page - 1;
            $scope.applyFilter($scope.idRipartizioneSiada);
        }

        $scope.navigateToNextPage = function() {
            $scope.page = $scope.page + 1;
            $scope.applyFilter($scope.idRipartizioneSiada);
        }

        $scope.exportToExcel = function() {

            statistichePostRipartizioneService.getStatisticheList($scope.idRipartizioneSiada, false)
                .then(({ data }) => {
                    let dataRow = data
                        .map(
                            ({ nominativo, idDsr, dspName, valoreLordo, valoreAggioDem, valoreTrattenuta, valoreAggioDrm, valoreNetto, valoreNettoDem, valoreNettoDrm }) => [nominativo, idDsr, dspName, valoreLordo, valoreAggioDem, valoreTrattenuta, valoreAggioDrm, valoreNetto, valoreNettoDem, valoreNettoDrm]
                        );
                    // remove download header title
                    let headerTitles = $scope.headerTitles.slice();
                    headerTitles.splice(headerTitles.length - 1);
                    dataRow.unshift(headerTitles);
                    let ws = XLSX.utils.aoa_to_sheet(dataRow);
                    // crea il workbook
                    let wb = XLSX.utils.book_new();
                    // imposta il nome del worksheet
                    const ws_name = 'Statistiche_post_ripartizione';
                    wb.SheetNames.push(ws_name);
                    // inserisce il worksheet nel workbook
                    wb.Sheets[ws_name] = ws;

                    /* bookType can be any supported output type */
                    let wopts = { bookType: 'xlsx', bookSST: true, type: 'binary' };

                    let wbout = XLSX.write(wb, wopts);

                    //costruisce il nome del file
                    const current_datetime = new Date();
                    const date = current_datetime.getDate() + "-" + (current_datetime.getMonth() + 1) + "-" + current_datetime.getFullYear()
                    let buf = new ArrayBuffer(wbout.length);
                    let view = new Uint8Array(buf);
                    for (let i = 0; i !== wbout.length; ++i) view[i] = wbout.charCodeAt(i) & 0xFF;
                    /* the saveAs call downloads a file on the local machine */
                    saveAs(new Blob([buf], { type: "application/octet-stream" }), ws_name + date + ".xlsx");
                });
        }

    }
]);

codmanApp.service('statistichePostRipartizioneService', ['$http', '$q', 'configService', '$location', function($http, $q, configService, $location) {

    var service = this;

    service.msInvoiceApiUrl = `${$location.protocol()}://${$location.host()}:${$location.port()}/${configService.msInvoiceApiPath}`;


    service.getStatisticheList = function(idRipartizioneSiada, paginationActive, currentPage = null, maxRows = null) {
        let payload = {
            idRipartizioneSiada: idRipartizioneSiada ? idRipartizioneSiada : '',
            paginationActive
        };
        if (typeof currentPage === "number" && typeof maxRows === "number") {
            payload = {
                ...payload,
                currentPage,
                maxRows
            };
        }
        return $http({
            method: 'POST',
            url: service.msInvoiceApiUrl + 'invoice/statistica-ripartizione',
            headers: {
                'Content-Type': 'application/json'
            },
            data: payload
        });

    }



    service.downloadFile = function(id) {
        return $http({
            method: 'GET',
            responseType: 'blob',
            url: service.msInvoiceApiUrl + 'invoice/download648/' + id
        });
    }

    service.getListIdSiada = function() {
        return $http({
            method: 'GET',
            url: service.msInvoiceApiUrl + 'invoice/ripartizione/all'
        });
    }

}]);