codmanApp.controller('documentazioneMandantiCtrl', [ '$scope','$interval','Upload', 'configService','mandatiDocumentationService','societaTutela', '$routeParams', function($scope, $interval, Upload, configService, mandatiDocumentationService, societaTutela, $routeParams) {

	$scope.mandante = {};

	$scope.ctrl = {
		pageTitle: `Ingestion documentazione Mandanti`,
		societaTutela: societaTutela.data,
	};

	$scope.mandanti = $scope.ctrl.societaTutela.filter(el => el.tenant != '1');

	$scope.resetPage =false;
		       
	$scope.paging = {
		currentPage: 0,
		pageSize: 50
	};

	$scope.messages = {
		info: "", 
		error: ""
	};

	$scope.clearMessages = function() {
		$scope.messages = {
			info: "", 
			warning: "",
			error: ""
		};
	};

	$scope.warningMessage = function(message) {
		$scope.messages.warning = message;
	};

	$scope.errorMessage = function(message) {
		$scope.messages.error = message;
	};

	$scope.infoMessage = function(message) {
		$scope.messages.info = message;
	};

	$scope.init = function(){
		$scope.clearMessages();
		if($scope.resetPage){
			$scope.paging = {currentPage: 0,pageSize: 50, totRecords:0};
			$scope.resetPage =false;
		}
		mandatiDocumentationService.getDocumentationList($scope.mandante.codice, $scope.paging).then(function(response){
			if(!response.data.rows){
				$scope.paging = {currentPage: 0, pageSize: 50, totRecords:0};
				$scope.results = [];
				$scope.paging.totRecords = 0;
				return;
			}
			$scope.paging.totRecords = response.data.maxrows;
			$scope.paging.totPages = Math.ceil(response.data.maxrows/$scope.paging.pageSize)-1;
			$scope.results = response.data.rows;
		},function(error){
			$scope.errorMessage("Si è verificato un problema, riprovare più tardi!");
		});
	};

	$scope.navigateToPreviousPage = function () {
		$scope.paging.currentPage = $scope.paging.currentPage - 1;
		$scope.init();
	};

	$scope.navigateToNextPage = function () {
		$scope.paging.currentPage = $scope.paging.currentPage + 1;
		$scope.init();
	};

	$scope.navigateToEndPage = function () {
		$scope.paging.currentPage = $scope.paging.totPages;
		$scope.init();
	};	

	$scope.downloadFile = function (id, path) {
		$scope.clearMessages();
		var fileName = path.substring(1+ path.lastIndexOf('/'));
		mandatiDocumentationService.downloadFile(id)
			.then(function (response) {
				var blob = new Blob([response.data], { type: "application/vnd.ms-excel;charset=UTF-8" });
				saveAs(blob, fileName);
			}, function (error) {
				$scope.errorMessage("Non è possibile scaricare il file selezionato!");
			});
	};

	$scope.filter = function(){
		$scope.init();
	};

	$scope.reset = function(){
		$scope.mandante = {};
		$scope.paging = {currentPage: 0,pageSize: 50, totRecords:0};
		$scope.results = [];
		$scope.init();
	};

	$scope.resetPaging = function (){
		$scope.resetPage=true;
	};

	if($routeParams.codiceSocietaTutela){
		var mandanteFromAnag =  $scope.ctrl.societaTutela.filter(el => el.codice == $routeParams.codiceSocietaTutela);
		$scope.mandante = mandanteFromAnag[0];
	};

}]);

codmanApp.service('mandatiDocumentationService', ['$http','$q', function($http,$q){

	var service = this;

	service.getDocumentationList = function(codiceSocieta, paging){
		return $http({
			method: 'GET',
			url: 'rest/societa-tutela/search-documentation',
			params: {
				codiceSocieta: codiceSocieta,
				pageSize: paging.pageSize || 50,
				pageNumber: paging.currentPage || 0
			}
		});
	};
	
	service.downloadFile = function (id) {
		return $http({
			method: 'GET',
			responseType: 'blob',
			url: `rest/societa-tutela/download-documentation/${id}`
		});
	};

}]);