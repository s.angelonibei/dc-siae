/**
 * EsitiAutenticazione.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.siae.aas;

public class EsitiAutenticazione implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected EsitiAutenticazione(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _OK = "OK";
    public static final java.lang.String _PasswordErrata = "PasswordErrata";
    public static final java.lang.String _PasswordScaduta = "PasswordScaduta";
    public static final java.lang.String _AccountBloccato = "AccountBloccato";
    public static final java.lang.String _AccountTemporaneamenteBloccato = "AccountTemporaneamenteBloccato";
    public static final java.lang.String _AccountInesistente = "AccountInesistente";
    public static final java.lang.String _ErroreGenerico = "ErroreGenerico";
    public static final EsitiAutenticazione OK = new EsitiAutenticazione(_OK);
    public static final EsitiAutenticazione PasswordErrata = new EsitiAutenticazione(_PasswordErrata);
    public static final EsitiAutenticazione PasswordScaduta = new EsitiAutenticazione(_PasswordScaduta);
    public static final EsitiAutenticazione AccountBloccato = new EsitiAutenticazione(_AccountBloccato);
    public static final EsitiAutenticazione AccountTemporaneamenteBloccato = new EsitiAutenticazione(_AccountTemporaneamenteBloccato);
    public static final EsitiAutenticazione AccountInesistente = new EsitiAutenticazione(_AccountInesistente);
    public static final EsitiAutenticazione ErroreGenerico = new EsitiAutenticazione(_ErroreGenerico);
    public java.lang.String getValue() { return _value_;}
    public static EsitiAutenticazione fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        EsitiAutenticazione enumeration = (EsitiAutenticazione)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static EsitiAutenticazione fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EsitiAutenticazione.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://aas.siae.it/", "EsitiAutenticazione"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
