/**
 * PuntoPeriferico.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.siae.aas;

public class PuntoPeriferico  implements java.io.Serializable {
    private it.siae.aas.TipoPuntoPeriferico tipo;

    private java.lang.String codiceSeprag;

    private java.lang.String descrizione;

    public PuntoPeriferico() {
    }

    public PuntoPeriferico(
           it.siae.aas.TipoPuntoPeriferico tipo,
           java.lang.String codiceSeprag,
           java.lang.String descrizione) {
           this.tipo = tipo;
           this.codiceSeprag = codiceSeprag;
           this.descrizione = descrizione;
    }


    /**
     * Gets the tipo value for this PuntoPeriferico.
     * 
     * @return tipo
     */
    public it.siae.aas.TipoPuntoPeriferico getTipo() {
        return tipo;
    }


    /**
     * Sets the tipo value for this PuntoPeriferico.
     * 
     * @param tipo
     */
    public void setTipo(it.siae.aas.TipoPuntoPeriferico tipo) {
        this.tipo = tipo;
    }


    /**
     * Gets the codiceSeprag value for this PuntoPeriferico.
     * 
     * @return codiceSeprag
     */
    public java.lang.String getCodiceSeprag() {
        return codiceSeprag;
    }


    /**
     * Sets the codiceSeprag value for this PuntoPeriferico.
     * 
     * @param codiceSeprag
     */
    public void setCodiceSeprag(java.lang.String codiceSeprag) {
        this.codiceSeprag = codiceSeprag;
    }


    /**
     * Gets the descrizione value for this PuntoPeriferico.
     * 
     * @return descrizione
     */
    public java.lang.String getDescrizione() {
        return descrizione;
    }


    /**
     * Sets the descrizione value for this PuntoPeriferico.
     * 
     * @param descrizione
     */
    public void setDescrizione(java.lang.String descrizione) {
        this.descrizione = descrizione;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PuntoPeriferico)) return false;
        PuntoPeriferico other = (PuntoPeriferico) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.tipo==null && other.getTipo()==null) || 
             (this.tipo!=null &&
              this.tipo.equals(other.getTipo()))) &&
            ((this.codiceSeprag==null && other.getCodiceSeprag()==null) || 
             (this.codiceSeprag!=null &&
              this.codiceSeprag.equals(other.getCodiceSeprag()))) &&
            ((this.descrizione==null && other.getDescrizione()==null) || 
             (this.descrizione!=null &&
              this.descrizione.equals(other.getDescrizione())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTipo() != null) {
            _hashCode += getTipo().hashCode();
        }
        if (getCodiceSeprag() != null) {
            _hashCode += getCodiceSeprag().hashCode();
        }
        if (getDescrizione() != null) {
            _hashCode += getDescrizione().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PuntoPeriferico.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://aas.siae.it/", "PuntoPeriferico"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://aas.siae.it/", "Tipo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://aas.siae.it/", "TipoPuntoPeriferico"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codiceSeprag");
        elemField.setXmlName(new javax.xml.namespace.QName("http://aas.siae.it/", "CodiceSeprag"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descrizione");
        elemField.setXmlName(new javax.xml.namespace.QName("http://aas.siae.it/", "Descrizione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
