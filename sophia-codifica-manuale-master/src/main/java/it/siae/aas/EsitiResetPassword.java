/**
 * EsitiResetPassword.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.siae.aas;

public class EsitiResetPassword implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected EsitiResetPassword(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _OK = "OK";
    public static final java.lang.String _AccountInesistente = "AccountInesistente";
    public static final java.lang.String _RispostaSegretaAssente = "RispostaSegretaAssente";
    public static final java.lang.String _RispostaSegretaErrata = "RispostaSegretaErrata";
    public static final java.lang.String _EmailAssente = "EmailAssente";
    public static final java.lang.String _InvioEmailErrato = "InvioEmailErrato";
    public static final java.lang.String _ErroreGenerico = "ErroreGenerico";
    public static final java.lang.String _InputNonValido = "InputNonValido";
    public static final EsitiResetPassword OK = new EsitiResetPassword(_OK);
    public static final EsitiResetPassword AccountInesistente = new EsitiResetPassword(_AccountInesistente);
    public static final EsitiResetPassword RispostaSegretaAssente = new EsitiResetPassword(_RispostaSegretaAssente);
    public static final EsitiResetPassword RispostaSegretaErrata = new EsitiResetPassword(_RispostaSegretaErrata);
    public static final EsitiResetPassword EmailAssente = new EsitiResetPassword(_EmailAssente);
    public static final EsitiResetPassword InvioEmailErrato = new EsitiResetPassword(_InvioEmailErrato);
    public static final EsitiResetPassword ErroreGenerico = new EsitiResetPassword(_ErroreGenerico);
    public static final EsitiResetPassword InputNonValido = new EsitiResetPassword(_InputNonValido);
    public java.lang.String getValue() { return _value_;}
    public static EsitiResetPassword fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        EsitiResetPassword enumeration = (EsitiResetPassword)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static EsitiResetPassword fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EsitiResetPassword.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://aas.siae.it/", "EsitiResetPassword"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
