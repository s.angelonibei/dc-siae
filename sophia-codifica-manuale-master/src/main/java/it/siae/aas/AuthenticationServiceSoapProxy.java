package it.siae.aas;

public class AuthenticationServiceSoapProxy implements it.siae.aas.AuthenticationServiceSoap {
  private String _endpoint = null;
  private it.siae.aas.AuthenticationServiceSoap authenticationServiceSoap = null;
  
  public AuthenticationServiceSoapProxy() {
    _initAuthenticationServiceSoapProxy();
  }
  
  public AuthenticationServiceSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initAuthenticationServiceSoapProxy();
  }
  
  private void _initAuthenticationServiceSoapProxy() {
    try {
      authenticationServiceSoap = (new it.siae.aas.AuthenticationServiceLocator()).getAuthenticationServiceSoap();
      if (authenticationServiceSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)authenticationServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)authenticationServiceSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (authenticationServiceSoap != null)
      ((javax.xml.rpc.Stub)authenticationServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public it.siae.aas.AuthenticationServiceSoap getAuthenticationServiceSoap() {
    if (authenticationServiceSoap == null)
      _initAuthenticationServiceSoapProxy();
    return authenticationServiceSoap;
  }
  
  public it.siae.aas.EsitiResetPassword resetPassword(java.lang.String userName, java.lang.String ip, java.lang.String operatore, java.lang.String securityPassword) throws java.rmi.RemoteException{
    if (authenticationServiceSoap == null)
      _initAuthenticationServiceSoapProxy();
    return authenticationServiceSoap.resetPassword(userName, ip, operatore, securityPassword);
  }
  
  public it.siae.aas.EsitiCambioPassword changePassword(java.lang.String userName, java.lang.String oldPassword, java.lang.String newPassword, java.lang.String ip, java.lang.String operatore, java.lang.String securityPassword) throws java.rmi.RemoteException{
    if (authenticationServiceSoap == null)
      _initAuthenticationServiceSoapProxy();
    return authenticationServiceSoap.changePassword(userName, oldPassword, newPassword, ip, operatore, securityPassword);
  }
  
  public it.siae.aas.EsitiAutenticazione login(java.lang.String userName, java.lang.String password) throws java.rmi.RemoteException{
    if (authenticationServiceSoap == null)
      _initAuthenticationServiceSoapProxy();
    return authenticationServiceSoap.login(userName, password);
  }
  
  public it.siae.aas.UtenteBackOffice getUserData(java.lang.String userName) throws java.rmi.RemoteException{
    if (authenticationServiceSoap == null)
      _initAuthenticationServiceSoapProxy();
    return authenticationServiceSoap.getUserData(userName);
  }
  
  public java.lang.String[] getUserGroups(java.lang.String userName, java.lang.String appCode) throws java.rmi.RemoteException{
    if (authenticationServiceSoap == null)
      _initAuthenticationServiceSoapProxy();
    return authenticationServiceSoap.getUserGroups(userName, appCode);
  }
  
  public java.lang.String[] getGroupMembers(java.lang.String applicationGroup, java.lang.String subApplicationGroup) throws java.rmi.RemoteException{
    if (authenticationServiceSoap == null)
      _initAuthenticationServiceSoapProxy();
    return authenticationServiceSoap.getGroupMembers(applicationGroup, subApplicationGroup);
  }
  
  public int getSecurityLevel(java.lang.String applicationGroup, java.lang.String applicationSubGroup) throws java.rmi.RemoteException{
    if (authenticationServiceSoap == null)
      _initAuthenticationServiceSoapProxy();
    return authenticationServiceSoap.getSecurityLevel(applicationGroup, applicationSubGroup);
  }
  
  public java.lang.String getLdapPasswordExpirationTime(java.lang.String userName) throws java.rmi.RemoteException{
    if (authenticationServiceSoap == null)
      _initAuthenticationServiceSoapProxy();
    return authenticationServiceSoap.getLdapPasswordExpirationTime(userName);
  }
  
  public it.siae.aas.UniqueLoginResponse loginSSO(java.lang.String userName, java.lang.String password, java.lang.String ip) throws java.rmi.RemoteException{
    if (authenticationServiceSoap == null)
      _initAuthenticationServiceSoapProxy();
    return authenticationServiceSoap.loginSSO(userName, password, ip);
  }
  
  public void expireToken(java.lang.String authenticationToken, java.lang.String ip) throws java.rmi.RemoteException{
    if (authenticationServiceSoap == null)
      _initAuthenticationServiceSoapProxy();
    authenticationServiceSoap.expireToken(authenticationToken, ip);
  }
  
  public void refreshToken(java.lang.String authenticationToken, java.lang.String ip) throws java.rmi.RemoteException{
    if (authenticationServiceSoap == null)
      _initAuthenticationServiceSoapProxy();
    authenticationServiceSoap.refreshToken(authenticationToken, ip);
  }
  
  public java.lang.String checkToken(java.lang.String authenticationToken, java.lang.String applicationCode, java.lang.String ip) throws java.rmi.RemoteException{
    if (authenticationServiceSoap == null)
      _initAuthenticationServiceSoapProxy();
    return authenticationServiceSoap.checkToken(authenticationToken, applicationCode, ip);
  }
  
  
}