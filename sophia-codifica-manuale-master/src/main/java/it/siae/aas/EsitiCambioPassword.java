/**
 * EsitiCambioPassword.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.siae.aas;

public class EsitiCambioPassword implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected EsitiCambioPassword(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _OK = "OK";
    public static final java.lang.String _VecchiaPasswordErrata = "VecchiaPasswordErrata";
    public static final java.lang.String _NuovaPasswordNonAmmissibile = "NuovaPasswordNonAmmissibile";
    public static final java.lang.String _AccountBloccato = "AccountBloccato";
    public static final java.lang.String _AccountTemporaneamenteBloccato = "AccountTemporaneamenteBloccato";
    public static final java.lang.String _AccountInesistente = "AccountInesistente";
    public static final java.lang.String _InputNonValido = "InputNonValido";
    public static final java.lang.String _ErroreGenerico = "ErroreGenerico";
    public static final java.lang.String _NuovaPasswordNonConfermata = "NuovaPasswordNonConfermata";
    public static final EsitiCambioPassword OK = new EsitiCambioPassword(_OK);
    public static final EsitiCambioPassword VecchiaPasswordErrata = new EsitiCambioPassword(_VecchiaPasswordErrata);
    public static final EsitiCambioPassword NuovaPasswordNonAmmissibile = new EsitiCambioPassword(_NuovaPasswordNonAmmissibile);
    public static final EsitiCambioPassword AccountBloccato = new EsitiCambioPassword(_AccountBloccato);
    public static final EsitiCambioPassword AccountTemporaneamenteBloccato = new EsitiCambioPassword(_AccountTemporaneamenteBloccato);
    public static final EsitiCambioPassword AccountInesistente = new EsitiCambioPassword(_AccountInesistente);
    public static final EsitiCambioPassword InputNonValido = new EsitiCambioPassword(_InputNonValido);
    public static final EsitiCambioPassword ErroreGenerico = new EsitiCambioPassword(_ErroreGenerico);
    public static final EsitiCambioPassword NuovaPasswordNonConfermata = new EsitiCambioPassword(_NuovaPasswordNonConfermata);
    public java.lang.String getValue() { return _value_;}
    public static EsitiCambioPassword fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        EsitiCambioPassword enumeration = (EsitiCambioPassword)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static EsitiCambioPassword fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EsitiCambioPassword.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://aas.siae.it/", "EsitiCambioPassword"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
