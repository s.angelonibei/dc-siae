/**
 * AuthenticationServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.siae.aas;

public interface AuthenticationServiceSoap extends java.rmi.Remote {
    public it.siae.aas.EsitiResetPassword resetPassword(java.lang.String userName, java.lang.String ip, java.lang.String operatore, java.lang.String securityPassword) throws java.rmi.RemoteException;
    public it.siae.aas.EsitiCambioPassword changePassword(java.lang.String userName, java.lang.String oldPassword, java.lang.String newPassword, java.lang.String ip, java.lang.String operatore, java.lang.String securityPassword) throws java.rmi.RemoteException;
    public it.siae.aas.EsitiAutenticazione login(java.lang.String userName, java.lang.String password) throws java.rmi.RemoteException;
    public it.siae.aas.UtenteBackOffice getUserData(java.lang.String userName) throws java.rmi.RemoteException;
    public java.lang.String[] getUserGroups(java.lang.String userName, java.lang.String appCode) throws java.rmi.RemoteException;
    public java.lang.String[] getGroupMembers(java.lang.String applicationGroup, java.lang.String subApplicationGroup) throws java.rmi.RemoteException;
    public int getSecurityLevel(java.lang.String applicationGroup, java.lang.String applicationSubGroup) throws java.rmi.RemoteException;
    public java.lang.String getLdapPasswordExpirationTime(java.lang.String userName) throws java.rmi.RemoteException;
    public it.siae.aas.UniqueLoginResponse loginSSO(java.lang.String userName, java.lang.String password, java.lang.String ip) throws java.rmi.RemoteException;
    public void expireToken(java.lang.String authenticationToken, java.lang.String ip) throws java.rmi.RemoteException;
    public void refreshToken(java.lang.String authenticationToken, java.lang.String ip) throws java.rmi.RemoteException;
    public java.lang.String checkToken(java.lang.String authenticationToken, java.lang.String applicationCode, java.lang.String ip) throws java.rmi.RemoteException;
}
