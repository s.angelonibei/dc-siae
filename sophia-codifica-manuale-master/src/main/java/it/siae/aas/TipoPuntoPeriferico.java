/**
 * TipoPuntoPeriferico.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.siae.aas;

public class TipoPuntoPeriferico implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected TipoPuntoPeriferico(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _DG = "DG";
    public static final java.lang.String _Mandataria = "Mandataria";
    public static final java.lang.String _Filiale = "Filiale";
    public static final java.lang.String _Agenzia = "Agenzia";
    public static final java.lang.String _Sede = "Sede";
    public static final TipoPuntoPeriferico DG = new TipoPuntoPeriferico(_DG);
    public static final TipoPuntoPeriferico Mandataria = new TipoPuntoPeriferico(_Mandataria);
    public static final TipoPuntoPeriferico Filiale = new TipoPuntoPeriferico(_Filiale);
    public static final TipoPuntoPeriferico Agenzia = new TipoPuntoPeriferico(_Agenzia);
    public static final TipoPuntoPeriferico Sede = new TipoPuntoPeriferico(_Sede);
    public java.lang.String getValue() { return _value_;}
    public static TipoPuntoPeriferico fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        TipoPuntoPeriferico enumeration = (TipoPuntoPeriferico)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static TipoPuntoPeriferico fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TipoPuntoPeriferico.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://aas.siae.it/", "TipoPuntoPeriferico"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
