/**
 * UtenteBackOffice.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.siae.aas;

public class UtenteBackOffice  extends it.siae.aas.UtenteBase  implements java.io.Serializable {
    private it.siae.aas.PuntoPeriferico puntoPeriferico;

    public UtenteBackOffice() {
    }

    public UtenteBackOffice(
           java.lang.String userName,
           java.lang.String tokenID,
           java.lang.String denominazione,
           java.lang.String email,
           it.siae.aas.EsitiAutenticazione statoPassword,
           it.siae.aas.PuntoPeriferico puntoPeriferico) {
        super(
            userName,
            tokenID,
            denominazione,
            email,
            statoPassword);
        this.puntoPeriferico = puntoPeriferico;
    }


    /**
     * Gets the puntoPeriferico value for this UtenteBackOffice.
     * 
     * @return puntoPeriferico
     */
    public it.siae.aas.PuntoPeriferico getPuntoPeriferico() {
        return puntoPeriferico;
    }


    /**
     * Sets the puntoPeriferico value for this UtenteBackOffice.
     * 
     * @param puntoPeriferico
     */
    public void setPuntoPeriferico(it.siae.aas.PuntoPeriferico puntoPeriferico) {
        this.puntoPeriferico = puntoPeriferico;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UtenteBackOffice)) return false;
        UtenteBackOffice other = (UtenteBackOffice) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.puntoPeriferico==null && other.getPuntoPeriferico()==null) || 
             (this.puntoPeriferico!=null &&
              this.puntoPeriferico.equals(other.getPuntoPeriferico())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getPuntoPeriferico() != null) {
            _hashCode += getPuntoPeriferico().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UtenteBackOffice.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://aas.siae.it/", "UtenteBackOffice"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("puntoPeriferico");
        elemField.setXmlName(new javax.xml.namespace.QName("http://aas.siae.it/", "PuntoPeriferico"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://aas.siae.it/", "PuntoPeriferico"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
