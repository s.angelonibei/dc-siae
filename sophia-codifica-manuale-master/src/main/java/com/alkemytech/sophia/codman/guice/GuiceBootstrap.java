package com.alkemytech.sophia.codman.guice;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import com.alkemytech.sophia.common.emr.EMRService;
import com.alkemytech.sophia.common.s3.S3Service;
import com.alkemytech.sophia.common.sqs.SQSService;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceServletContextListener;

public class GuiceBootstrap extends GuiceServletContextListener {

	private Injector injector;
		
	@Override
	protected Injector getInjector() {
		return injector;
	}

	 @Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		final ServletContext servletContext = servletContextEvent.getServletContext();
		injector = Guice.createInjector(new ApplicationServletModule(servletContext.getInitParameter("configuration")));
		servletContext.setAttribute(Injector.class.getName(), injector);
		// startup service(s)
		injector.getInstance(S3Service.class).startup();
		injector.getInstance(SQSService.class).startup();
		injector.getInstance(EMRService.class).startup();
	}

	@Override
	public void contextDestroyed(ServletContextEvent servletContextEvent) {
		final ServletContext servletContext = servletContextEvent.getServletContext();
		servletContext.removeAttribute(Injector.class.getName());
		super.contextDestroyed(servletContextEvent);
		// shutdown service(s)
		injector.getInstance(S3Service.class).shutdown();
		injector.getInstance(SQSService.class).shutdown();
		injector.getInstance(EMRService.class).shutdown();
		injector = null;
		// force garbage collection
		System.gc();
	}
	    
}
