package com.alkemytech.sophia.codman.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.GsonBuilder;

@XmlRootElement
@Entity(name="SophiaCaOpera")
@Table(name="sophia_ca_opera")
@IdClass(SophiaCaOperaId.class)
public class SophiaCaOpera {

	@Id
	@Column(name="codice_opera", nullable=false)
	private String codiceOpera;
	
	@Column(name="title", nullable=true)
	private String title;

	@Id
	@Column(name="hash_title", nullable=false)
	private String hashTitle;

	@Column(name="artists_names", nullable=true)
	private String artistsNames;
	
	@Column(name="artists_ipi", nullable=true)
	private String artistsIpi;
	
	@Column(name="an_origin_vector", nullable=true)
	private String anOriginVector;

	@Column(name="ai_origin_vector", nullable=true)
	private String aiOriginVector;
	
	@Column(name="an_idutil_src_matrix", nullable=true)
	private String anIdutilSrcMatrix;

	@Column(name="ai_idutil_src_matrix", nullable=true)
	private String aiIdutilSrcMatrix;

	@Column(name="an_iddsr_source_matrix", nullable=true)
	private String anIddsrSourceMatrix;

	@Column(name="ai_iddsr_source_matrix", nullable=true)
	private String aiIddsrSourceMatrix;
	
	@Column(name="kb_version", nullable=true)
	private Integer kbVersion;
	
	@Column(name="valid", nullable=true)
	private Integer valid;

	public String getCodiceOpera() {
		return codiceOpera;
	}

	public void setCodiceOpera(String codiceOpera) {
		this.codiceOpera = codiceOpera;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getHashTitle() {
		return hashTitle;
	}

	public void setHashTitle(String hashTitle) {
		this.hashTitle = hashTitle;
	}

	public String getArtistsNames() {
		return artistsNames;
	}

	public void setArtistsNames(String artistsNames) {
		this.artistsNames = artistsNames;
	}

	public String getArtistsIpi() {
		return artistsIpi;
	}

	public void setArtistsIpi(String artistsIpi) {
		this.artistsIpi = artistsIpi;
	}

	public String getAnOriginVector() {
		return anOriginVector;
	}

	public void setAnOriginVector(String anOriginVector) {
		this.anOriginVector = anOriginVector;
	}

	public String getAiOriginVector() {
		return aiOriginVector;
	}

	public void setAiOriginVector(String aiOriginVector) {
		this.aiOriginVector = aiOriginVector;
	}

	public String getAnIdutilSrcMatrix() {
		return anIdutilSrcMatrix;
	}

	public void setAnIdutilSrcMatrix(String anIdutilSrcMatrix) {
		this.anIdutilSrcMatrix = anIdutilSrcMatrix;
	}

	public String getAiIdutilSrcMatrix() {
		return aiIdutilSrcMatrix;
	}

	public void setAiIdutilSrcMatrix(String aiIdutilSrcMatrix) {
		this.aiIdutilSrcMatrix = aiIdutilSrcMatrix;
	}

	public String getAnIddsrSourceMatrix() {
		return anIddsrSourceMatrix;
	}

	public void setAnIddsrSourceMatrix(String anIddsrSourceMatrix) {
		this.anIddsrSourceMatrix = anIddsrSourceMatrix;
	}

	public String getAiIddsrSourceMatrix() {
		return aiIddsrSourceMatrix;
	}

	public void setAiIddsrSourceMatrix(String aiIddsrSourceMatrix) {
		this.aiIddsrSourceMatrix = aiIddsrSourceMatrix;
	}

	public Integer getKbVersion() {
		return kbVersion;
	}

	public void setKbVersion(Integer kbVersion) {
		this.kbVersion = kbVersion;
	}

	public Integer getValid() {
		return valid;
	}

	public void setValid(Integer valid) {
		this.valid = valid;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting().create().toJson(this);
	}
	
}
