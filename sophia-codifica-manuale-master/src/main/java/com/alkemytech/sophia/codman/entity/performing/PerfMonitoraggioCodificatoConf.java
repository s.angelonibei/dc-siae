package com.alkemytech.sophia.codman.entity.performing;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PERF_MONITORAGGIO_CODIFICATO_CONF database table.
 * 
 */
@Entity
@Table(name="PERF_MONITORAGGIO_CODIFICATO_CONF")
@NamedQuery(name="PerfMonitoraggioCodificatoConf.findAll", query="SELECT p FROM PerfMonitoraggioCodificatoConf p")
public class PerfMonitoraggioCodificatoConf implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID_MONITORAGGIO_CODIFICATO_CONF", unique=true, nullable=false)
	private int idMonitoraggioCodificatoConf;

	@Column(name="CONFIGURAZIONE", nullable=false)
	private String configurazione;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_FINE_VALIDITA")
	private Date dataFineValidita;

	//bi-directional many-to-one association to PerfMonitoraggioCodificato
	@JsonManagedReference
	@OneToMany(mappedBy="perfMonitoraggioCodificatoConf")
	private List<PerfMonitoraggioCodificato> perfMonitoraggioCodificatos;

	public PerfMonitoraggioCodificatoConf() {
	}

	public int getIdMonitoraggioCodificatoConf() {
		return this.idMonitoraggioCodificatoConf;
	}

	public void setIdMonitoraggioCodificatoConf(int idMonitoraggioCodificatoConf) {
		this.idMonitoraggioCodificatoConf = idMonitoraggioCodificatoConf;
	}

	public String getConfigurazione() {
		return this.configurazione;
	}

	public void setConfigurazione(String configurazione) {
		this.configurazione = configurazione;
	}

	public Date getDataFineValidita() {
		return this.dataFineValidita;
	}

	public void setDataFineValidita(Date dataFineValidita) {
		this.dataFineValidita = dataFineValidita;
	}

	public List<PerfMonitoraggioCodificato> getPerfMonitoraggioCodificatos() {
		return this.perfMonitoraggioCodificatos;
	}

	public void setPerfMonitoraggioCodificatos(List<PerfMonitoraggioCodificato> perfMonitoraggioCodificatos) {
		this.perfMonitoraggioCodificatos = perfMonitoraggioCodificatos;
	}

	public PerfMonitoraggioCodificato addPerfMonitoraggioCodificato(PerfMonitoraggioCodificato perfMonitoraggioCodificato) {
		getPerfMonitoraggioCodificatos().add(perfMonitoraggioCodificato);
		perfMonitoraggioCodificato.setPerfMonitoraggioCodificatoConf(this);

		return perfMonitoraggioCodificato;
	}

	public PerfMonitoraggioCodificato removePerfMonitoraggioCodificato(PerfMonitoraggioCodificato perfMonitoraggioCodificato) {
		getPerfMonitoraggioCodificatos().remove(perfMonitoraggioCodificato);
		perfMonitoraggioCodificato.setPerfMonitoraggioCodificatoConf(null);

		return perfMonitoraggioCodificato;
	}

}