package com.alkemytech.sophia.codman.dto;

import lombok.Data;

import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@Produces("application/json")
@XmlRootElement
public class LoadIdentifiedBlackListDTO {

    private String title;
    private String artists;
    private Integer siaeWorkCode;
    private String uuid;
}
