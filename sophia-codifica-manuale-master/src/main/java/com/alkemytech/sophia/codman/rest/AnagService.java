package com.alkemytech.sophia.codman.rest;

import com.alkemytech.sophia.codman.dto.AnagUtilizationTypeIdDspDTO;
import com.alkemytech.sophia.codman.dto.AnagUtilizationTypeIdDspResultSet;
import com.alkemytech.sophia.codman.dto.BasicAnagDTO;
import com.alkemytech.sophia.codman.entity.AnagCountry;
import com.alkemytech.sophia.codman.entity.AnagDsp;
import com.alkemytech.sophia.codman.entity.AnagUtilizationType;
import com.alkemytech.sophia.codman.entity.CommercialOffers;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.alkemytech.sophia.codman.invoice.repository.InvoiceConfiguration;
import com.alkemytech.sophia.codman.invoice.repository.InvoiceStatus;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.yaml.snakeyaml.Yaml;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Singleton
@Path("data")
public class AnagService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Provider<EntityManager> provider;
	private final Gson gson;

	@Inject
	protected AnagService(@McmdbDataSource Provider<EntityManager> provider, Gson gson) {
		super();
		this.provider = provider;
		this.gson = gson;
	}

	@GET
	@Path("countries")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response countries() {
		EntityManager entityManager = null;
		List<AnagCountry> result;
		try {
			entityManager = provider.get();
			final Query q = entityManager.createQuery("select x from AnagCountry x order by x.name");
			result = (List<AnagCountry>) q.getResultList();
			return Response.ok(gson.toJson(result)).build();
		} catch (Exception e) {
			logger.error("countries", e);
		}
		return Response.status(500).build();
	}

	@GET
	@Path("dspList")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response dspList() {
		EntityManager entityManager = null;
		List<AnagDsp> result;
		try {
			entityManager = provider.get();
			final Query q = entityManager.createQuery("select x from AnagDsp x order by x.name");
			result = (List<AnagDsp>) q.getResultList();
			return Response.ok(gson.toJson(result)).build();
		} catch (Exception e) {
			logger.error("dspList", e);
		}
		return Response.status(500).build();
	}

	@GET
	@Path("utilizations")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response utilizations() {
		EntityManager entityManager = null;
		List<AnagUtilizationType> result;
		try {
			entityManager = provider.get();
			final Query q = entityManager.createQuery("select x from AnagUtilizationType x order by x.name");
			result = (List<AnagUtilizationType>) q.getResultList();
			return Response.ok(gson.toJson(result)).build();
		} catch (Exception e) {
			logger.error("utilizations", e);
		}
		return Response.status(500).build();
	}
	
	@GET
	@Path("dspCountries")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response dspCountries() {
		Map<String, List> result = new HashMap<>();

		EntityManager entityManager;

		entityManager = provider.get();
		result.put("dsp", entityManager.createQuery("select x from AnagDsp x order by x.idDsp", AnagDsp.class).getResultList());
		result.put("countries", entityManager.createQuery("select x from AnagCountry x order by x.idCountry", AnagCountry.class).getResultList());

		return Response.ok(gson.toJson(result)).build();

	}

	@GET
	@Path("dspCountriesUtilizations")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response dspCountriesUtilizations() {
		Map<String, List<?>> result = new HashMap<>();

		EntityManager entityManager = null;
		List<AnagUtilizationType> resultUtilizations;
		List<AnagDsp> resultDsp;
		List<AnagCountry> resultCountries;

		try {
			entityManager = provider.get();
			final Query q1 = entityManager.createQuery("select x from AnagDsp x order by x.idDsp");
			resultDsp = (List<AnagDsp>) q1.getResultList();
			result.put("dsp", resultDsp);

			final Query q2 = entityManager.createQuery("select x from AnagCountry x order by x.idCountry");
			resultCountries = (List<AnagCountry>) q2.getResultList();
			result.put("countries", resultCountries);

			final Query q3 = entityManager
					.createQuery("select x from AnagUtilizationType x order by x.idUtilizationType");
			resultUtilizations = (List<AnagUtilizationType>) q3.getResultList();
			result.put("utilizations", resultUtilizations);

		} catch (Exception e) {
			logger.error("dspCountriesUtilizations", e);
			return Response.status(500).build();
		}

		return Response.ok(gson.toJson(result)).build();

	}

	@GET
	@Path("dspUtilizations")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response dspUtilizations() {
		Map<String, List<?>> result = new HashMap<>();

		EntityManager entityManager = null;
		List<AnagUtilizationType> resultUtilizations;
		List<AnagDsp> resultDsp;

		try {
			entityManager = provider.get();
			final Query q1 = entityManager.createQuery("select x from AnagDsp x order by x.idDsp");
			resultDsp = (List<AnagDsp>) q1.getResultList();
			result.put("dsp", resultDsp);

			final Query q3 = entityManager
					.createQuery("select x from AnagUtilizationType x order by x.idUtilizationType");
			resultUtilizations = (List<AnagUtilizationType>) q3.getResultList();
			result.put("utilizations", resultUtilizations);

		} catch (Exception e) {
			logger.error("dspUtilizations", e);
			return Response.status(500).build();
		}

		return Response.ok(gson.toJson(result)).build();

	}

	@GET
	@Path("dspUtilizationsOffers")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response dspUtilizationsOffers() {
		Map<String, List<?>> result = new HashMap<>();

		EntityManager entityManager = null;
		List<AnagUtilizationType> resultUtilizations;
		List<AnagDsp> resultDsp;
		List<CommercialOffers> resultOffers;

		try {
			entityManager = provider.get();
			final Query q1 = entityManager.createQuery("select x from AnagDsp x order by x.idDsp");
			resultDsp = (List<AnagDsp>) q1.getResultList();
			result.put("dsp", resultDsp);

			final Query q2 = entityManager
					.createQuery("select x from AnagUtilizationType x order by x.idUtilizationType");
			resultUtilizations = (List<AnagUtilizationType>) q2.getResultList();
			result.put("utilizations", resultUtilizations);

			final Query q3 = entityManager.createQuery("select x from CommercialOffers x order by x.idDSP");
			resultOffers = (List<CommercialOffers>) q3.getResultList();
			result.put("commercialOffers", resultOffers);

		} catch (Exception e) {
			logger.error("dspUtilizationsOffers", e);
			return Response.status(500).build();
		}

		return Response.ok(gson.toJson(result)).build();

	}

	@GET
	@Path("allAvailableOffers")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response allAvailableOffers() {
		EntityManager entityManager = null;
		List<CommercialOffers> result;
		try {
			entityManager = provider.get();
			final Query q = entityManager.createQuery("select x from CommercialOffers x order by x.offering");
			result = (List<CommercialOffers>) q.getResultList();
			return Response.ok(gson.toJson(result)).build();
		} catch (Exception e) {
			logger.error("allAvailableOffers", e);
		}
		return Response.status(500).build();
	}

	@GET
	@Path("dspUtilizationsOffersCountries")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response dspUtilizationsOffersCountries() {
		Map<String, List<?>> result = new HashMap<>();

		EntityManager entityManager = null;
		List<AnagUtilizationTypeIdDspDTO> resultUtilizations;
		List<AnagDsp> resultDsp;
		List<CommercialOffers> resultOffers;
		List<AnagCountry> resultCountries;

		try {
			entityManager = provider.get();
			final Query q1 = entityManager.createQuery("select x from AnagDsp x order by x.name");
			resultDsp = (List<AnagDsp>) q1.getResultList();
			result.put("dsp", resultDsp);

			MultiValueMap<AnagUtilizationTypeIdDspDTO, String> utilizationTypeDsp = new LinkedMultiValueMap<>();
            final Query q2 = entityManager.createNativeQuery(
                    "select ANAG_UTILIZATION_TYPE.ID_UTILIZATION_TYPE idUtilizationType, " +
							"ANAG_UTILIZATION_TYPE.NAME name, " +
							"ANAG_UTILIZATION_TYPE.DESCRIPTION description, " +
							"ANAG_DSP.IDDSP idDsp " +
                    "from COMMERCIAL_OFFERS " +
                    "join ANAG_UTILIZATION_TYPE on COMMERCIAL_OFFERS.ID_UTIILIZATION_TYPE = ANAG_UTILIZATION_TYPE.ID_UTILIZATION_TYPE " +
                    "join ANAG_DSP on COMMERCIAL_OFFERS.IDDSP = ANAG_DSP.IDDSP " +
                    "group by ANAG_UTILIZATION_TYPE.ID_UTILIZATION_TYPE, ANAG_UTILIZATION_TYPE.NAME, ANAG_UTILIZATION_TYPE.DESCRIPTION, ANAG_DSP.IDDSP " +
                    "order by ANAG_UTILIZATION_TYPE.NAME", "AnagUtilizationTypeIdDspResultSet");
			List<AnagUtilizationTypeIdDspResultSet> resultUtilizationsDsp = q2.getResultList();
			for (AnagUtilizationTypeIdDspResultSet obj : resultUtilizationsDsp) {
				utilizationTypeDsp.add(new AnagUtilizationTypeIdDspDTO(obj.getIdUtilizationType(), obj.getName(), obj.getDescription()), obj.getIdDsp());
			}
			resultUtilizations = new ArrayList<>();

			for (Map.Entry<AnagUtilizationTypeIdDspDTO, List<String>> entry : utilizationTypeDsp.entrySet()) {
				AnagUtilizationTypeIdDspDTO key = entry.getKey();
				List<String> value = entry.getValue();
				resultUtilizations.add(new AnagUtilizationTypeIdDspDTO()
						.setIdUtilizationType(key.getIdUtilizationType())
						.setName(key.getName())
						.setDescription(key.getDescription())
						.setIdDsp(value));
			}
			result.put("utilizations", resultUtilizations);
			final Query q3 = entityManager.createQuery("select x from CommercialOffers x order by x.offering");
			resultOffers = (List<CommercialOffers>) q3.getResultList();
			result.put("commercialOffers", resultOffers);

			final Query q4 = entityManager.createQuery("select x from AnagCountry x order by x.name");
			resultCountries = (List<AnagCountry>) q4.getResultList();
			result.put("countries", resultCountries);

		} catch (Exception e) {
			logger.error("dspUtilizationsOffersCountries", e);
			return Response.status(500).build();
		}

		return Response.ok(gson.toJson(result)).build();

	}

	@GET
	@Path("ccidConfigurationData")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response ccidConfigurationData() {
		Map<String, List<?>> result = new HashMap<>();

		EntityManager entityManager = null;

		try {
			entityManager = provider.get();
			final Query q1 = entityManager.createNativeQuery("select * from CCID_SERVICE_TYPE");
			result.put("serviceType", getBasicAnagDtoList(q1.getResultList()));

			final Query q2 = entityManager.createNativeQuery("select * from CCID_USE_TYPE");
			result.put("useType", getBasicAnagDtoList(q2.getResultList()));

			final Query q3 = entityManager.createNativeQuery("select * from CCID_APPLIED_TARIFF");
			result.put("appliedTariff", getBasicAnagDtoList(q3.getResultList()));

		} catch (Exception e) {
			logger.error("ccidConfigurationData", e);
			return Response.status(500).build();
		}

		return Response.ok(gson.toJson(result)).build();

	}

	private List<BasicAnagDTO> getBasicAnagDtoList(List<Object[]> result) {
		List<BasicAnagDTO> dtoList = new ArrayList<>();
		if (result != null && !result.isEmpty()) {
			for (Object[] o : result) {
				dtoList.add(new BasicAnagDTO(o));
			}
		}
		return dtoList;
	}

	@GET
	@Path("invoiceStatusCodes")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response invoiceStatusCodes() {
		Yaml yaml = new Yaml();
		InvoiceConfiguration result;
		List<InvoiceStatus> statusList = new ArrayList<>();
		try {
			InputStream in = getClass().getResourceAsStream("/yaml/invoiceConfig.yml");
			result = yaml.loadAs(in, InvoiceConfiguration.class);
			if (result != null) {
				Map<String, String> statusMap = result.getInvoiceStatus();
				for (String key : statusMap.keySet()) {
					statusList.add(new InvoiceStatus(key, statusMap.get(key)));
				}

				return Response.ok(gson.toJson(statusList)).build();

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return Response.status(500).build();

	}

}
