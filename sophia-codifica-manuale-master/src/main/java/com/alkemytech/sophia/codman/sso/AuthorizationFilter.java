package com.alkemytech.sophia.codman.sso;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.name.Named;

public class AuthorizationFilter implements Filter {

	private final Logger logger = LoggerFactory.getLogger(getClass());

//	private final Properties configuration;
	private final Map<String, Pattern> patterns;

	private FilterConfig config;

	@Inject
	public AuthorizationFilter(@Named("configuration") Properties configuration) {
		super();
//		this.configuration = configuration;
		this.patterns = new HashMap<String, Pattern>();
	}

	@Override
	public void init(FilterConfig config) throws ServletException {
		this.config = config;
		try (final InputStream in = getClass().getResourceAsStream("/authorization.properties")) {
			final Properties properties = new Properties();
			properties.load(in);
			logger.debug("init: properties {}", properties);
			for (Entry<Object, Object> property : properties.entrySet()) {
				patterns.put(property.getKey().toString(), 
						Pattern.compile(property.getValue().toString()));
			}
	    } catch (IOException e) {
	    	throw new ServletException(e);
		}
	}

	@Override
	public void destroy() {
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		final HttpServletRequest httpRequest = (HttpServletRequest) request;
		final HttpServletResponse httpResponse = (HttpServletResponse) response;
		final HttpSession httpSession = httpRequest.getSession(false);

		final String contextPath = config.getServletContext().getContextPath();
//		logger.debug("doFilter: contextPath [" + contextPath + "]");
		String requestUri = httpRequest.getRequestURI();
//		logger.debug("doFilter: requestUri [" + requestUri + "]");
		requestUri = requestUri.substring(contextPath.length());
//		logger.debug("doFilter: context path free requestUri [" + requestUri + "]");

		for (Entry<String, Pattern> pattern : patterns.entrySet()) {
//			logger.debug("doFilter: pattern: " + pattern.getValue());
			if (pattern.getValue().matcher(requestUri).matches()) {
				if (null == httpSession || null == httpSession.getAttribute(pattern.getKey())) {
					// interrupt filter chain and redirect request
					httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED);
					return;
				}
			}
		}

		// pass the request along the filter chain
		chain.doFilter(request, response);

	}

}
