package com.alkemytech.sophia.codman.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.codman.dto.ConfigurazioneMandatoDTO;
import com.alkemytech.sophia.codman.mandato.service.IConfigurazioneMandatoService;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
@Path("mandato-configurazione")
public class AnagConfigurazioneMandatoController {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private Gson gson;
	
	private IConfigurazioneMandatoService configurazioneMandatoService;
	
	@Inject
	protected AnagConfigurazioneMandatoController(IConfigurazioneMandatoService configurazioneMandatoService, Gson gson) {
		super();
		this.configurazioneMandatoService = configurazioneMandatoService;
		this.gson = gson;
	}

	@GET
	@Produces("application/json")
	public Response getAll() {
		try {
			List<ConfigurazioneMandatoDTO> results = configurazioneMandatoService.getAll();
			return Response.ok(gson.toJson(results)).build();
		} catch (Exception e) {
			logger.error("Errore recupero mandati", e);
			return Response.status(500).build();
		}
	}
	
	@GET
	@Path("{id}")
	@Produces("application/json")
	public Response find(@PathParam("id") Integer id) {
		try {
			ConfigurazioneMandatoDTO result = configurazioneMandatoService.get(id);
			return Response.ok(gson.toJson(result)).build();
		} catch (Exception e) {
			logger.error(String.format("Errore recupero configurazioneMandato: %d", id), e);
			return Response.status(500).build();
		}
	}

	@POST
	@Produces("application/json")
	public Response save(ConfigurazioneMandatoDTO configurazioneMandato) {
		try {
			ConfigurazioneMandatoDTO result = configurazioneMandatoService.save(configurazioneMandato);
			return Response.ok(gson.toJson(result)).build();
		} 
		catch (IllegalArgumentException e) {
			logger.error(String.format("Errore di Validazione: %s", gson.toJson(configurazioneMandato)), e);
			return Response.status(400).build();
		}
		catch (Exception e) {
			logger.error(String.format("Errore salvataggio: %s", gson.toJson(configurazioneMandato)), e);
			return Response.status(500).build();
		}
	}

	@DELETE
	@Produces("application/json")
	@Consumes("application/json")
	public Response delete(ConfigurazioneMandatoDTO configurazioneMandato) {
		try {
			configurazioneMandatoService.delete(configurazioneMandato);
			return Response.ok(gson.toJson(configurazioneMandato)).build();
		}
		catch (Exception e) {
			logger.error(String.format("Errore nell'eliminazione del configurazioneMandato: %s", gson.toJson(configurazioneMandato)), e);
			return Response.status(500).build();
		}
	}

}
