package com.alkemytech.sophia.codman.entity.performing.dao;

import com.alkemytech.sophia.codman.dto.NdmVoceFatturaDateLimitsDTO;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.util.List;

public class EventiPagatiDAO implements IEventiPagatiDAO {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private Provider<EntityManager> provider;

    @Inject
    public EventiPagatiDAO(Provider<EntityManager> provider) {
        this.provider = provider;
    }

    @Override
    public List<String> getAgenzie(String agenzie) {
        EntityManager entityManager = provider.get();
        String query = "select e.agenzia from PerfEventiPagati e " +
                "where upper(e.agenzia) like upper(:agenzie) " +
                "group by  e.agenzia " +
                "order by  e.agenzia";

        return entityManager.createQuery(query, String.class)
                .setParameter("agenzie", agenzie != null ? "%" + agenzie + "%" : "%")
                .getResultList();
    }

    @Override
    public List<String> getVociIncasso(String search) {
        EntityManager entityManager = provider.get();
        String query = "select e.voceIncasso from PerfEventiPagati e " +
                "where upper(e.voceIncasso) like upper(:voceIncasso) " +
                "group by  e.voceIncasso " +
                "order by  e.voceIncasso";

        return entityManager.createQuery(query, String.class)
                .setParameter("voceIncasso", search != null ? "%" + search + "%" : "%")
                .getResultList();

    }

    @Override
    public NdmVoceFatturaDateLimitsDTO getDateLimits() {
        EntityManager entityManager = provider.get();
        String query = "select new " +
                "com.alkemytech.sophia.codman.dto.NdmVoceFatturaDateLimitsDTO(" +
                "max(e.dataContabileFattura), " +
                "min(e.dataContabileFattura), " +
                "max(e.dataFattura), " +
                "min(e.dataFattura) " +
                ") from PerfNdmVoceFattura e ";

        return entityManager.createQuery(query, NdmVoceFatturaDateLimitsDTO.class)
                .getSingleResult();
    }

    @Override
    public List<String> getSedi(String search) {
        EntityManager entityManager = provider.get();
        String query = "select e.sede from PerfEventiPagati e " +
                "where upper(e.sede) like upper(:sede) " +
                "group by  e.sede " +
                "order by  e.sede";

        return entityManager.createQuery(query, String.class)
                .setParameter("sede", search != null ? "%" + search + "%" : "%")
                .getResultList();
    }
}

