package com.alkemytech.sophia.codman.rest.performing.serviceimpl.config.utils;

import com.alkemytech.sophia.codman.entity.Configuration;

public interface ConfigurationValidator {
	
	public int validate(Configuration c);
	
	public boolean supports(Configuration c);

}
