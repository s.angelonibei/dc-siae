package com.alkemytech.sophia.codman.service.dsrmetadata.timestamp;

public interface TimeStamp {
	
	public String getCode();
	
	public String getDescription();
	
	public String getRegex();
}
