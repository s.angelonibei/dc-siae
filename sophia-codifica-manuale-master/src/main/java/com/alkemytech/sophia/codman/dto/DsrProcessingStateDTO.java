package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DsrProcessingStateDTO implements Serializable{
	
	public static enum STATUS {
		ERROR,
		COMPLETED,
		ARRIVED,
		PROCESSING
	}
	
	private static final long serialVersionUID = 1L;

	private String id;

	private String description;
	
	private boolean restartable;
	
	private boolean restartNeedConfirmation;
	
	private String code;
	
	public DsrProcessingStateDTO() {
		super();
	}

	public DsrProcessingStateDTO(String id, String description, boolean restartable, boolean restartNeedConfirmation,String code) {
		super();
		this.id = id;
		this.description = description;
		this.restartable = restartable;
		this.restartNeedConfirmation = restartNeedConfirmation;
		this.code=code;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isRestartable() {
		return restartable;
	}

	public void setRestartable(boolean restartable) {
		this.restartable = restartable;
	}

	public boolean isRestartNeedConfirmation() {
		return restartNeedConfirmation;
	}

	public void setRestartNeedConfirmation(boolean restartNeedConfirmation) {
		this.restartNeedConfirmation = restartNeedConfirmation;
	}
	
	public static DsrProcessingStateDTO statusFromEnum(STATUS status){
		
		switch (status) {
			case ERROR:
				return new DsrProcessingStateDTO("ERROR","Errore",true,false,"ERROR");
			case COMPLETED:
				return new DsrProcessingStateDTO("COMPLETED","Completato",true,true,"COMPLETED");
			case ARRIVED:
				return new DsrProcessingStateDTO("ARRIVED","Arrivato",true,false,"ARRIVED");
			case PROCESSING:
				return new DsrProcessingStateDTO("PROCESSING","In lavorazione",false,false,"PROCESSING");
		}
			
		
		return null;
	}

}