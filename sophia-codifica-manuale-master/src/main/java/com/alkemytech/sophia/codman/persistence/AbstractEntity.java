package com.alkemytech.sophia.codman.persistence;

import java.io.Serializable;

@SuppressWarnings("serial")
public abstract class AbstractEntity<PK> implements Serializable {

	public abstract PK getId();
	
}
