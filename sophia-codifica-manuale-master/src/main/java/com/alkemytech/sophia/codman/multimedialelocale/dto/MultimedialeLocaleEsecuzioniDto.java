package com.alkemytech.sophia.codman.multimedialelocale.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Date;

public class MultimedialeLocaleEsecuzioniDto {
	private Long idEsecuzione;
	private Date dataAvvio;
	private Long reportDaRiprocessare;
	private BigDecimal reportRiprocessati;
	private String ripartizione;

	public MultimedialeLocaleEsecuzioniDto() {
		super();
	}

	public MultimedialeLocaleEsecuzioniDto(Object[] record) {
		if (record.length == 5) {
			idEsecuzione = (Long) (record[0]);
			dataAvvio = ((Timestamp) record[1]);
			reportDaRiprocessare = (Long) (record[2]);
			reportRiprocessati = ((BigDecimal)record[3]);
			ripartizione = (String) (record[4]);
		}
	}

	public Long getIdEsecuzione() {
		return idEsecuzione;
	}

	public void setIdEsecuzione(Long idEsecuzione) {
		this.idEsecuzione = idEsecuzione;
	}

	public Date getDataAvvio() {
		return dataAvvio;
	}

	public void setDataAvvio(Date dataAvvio) {
		this.dataAvvio = dataAvvio;
	}

	public Long getReportDaRiprocessare() {
		return reportDaRiprocessare;
	}

	public void setReportDaRiprocessare(Long reportDaRiprocessare) {
		this.reportDaRiprocessare = reportDaRiprocessare;
	}

	public BigDecimal getReportRiprocessati() {
		return reportRiprocessati;
	}

	public void setReportRiprocessati(BigDecimal reportRiprocessati) {
		this.reportRiprocessati = reportRiprocessati;
	}

	public String getRipartizione() {
		return ripartizione;
	}

	public void setRipartizione(String ripartizione) {
		this.ripartizione = ripartizione;
	}

}
