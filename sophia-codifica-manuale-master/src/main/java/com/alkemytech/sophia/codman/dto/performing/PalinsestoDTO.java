package com.alkemytech.sophia.codman.dto.performing;

import java.math.BigInteger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PalinsestoDTO {

	private BigInteger idPalinsesto;
	private Boolean attivo;
	private String codiceDitta;
	private String dataCreazione;
	private String dataInizioValidita;
	private String dataFineValidita;
	private String dataUltimaModifica;
	private String nome;
	private String utenteUltimaModifica;
	private BigInteger idMusicProvider;

			
	public PalinsestoDTO() {
		super();
	}

	public PalinsestoDTO(BigInteger idPalinsesto, Boolean attivo, String codiceDitta, String dataCreazione,
			String dataInizioValidita, String dataFineValidita, String dataUltimaModifica, String nome,
			String utenteUltimaModifica, BigInteger idMusicProvider) {
		super();
		this.idPalinsesto = idPalinsesto;
		this.attivo = attivo;
		this.codiceDitta = codiceDitta;
		this.dataCreazione = dataCreazione;
		this.dataInizioValidita = dataInizioValidita;
		this.dataFineValidita = dataFineValidita;
		this.dataUltimaModifica = dataUltimaModifica;
		this.nome = nome;
		this.utenteUltimaModifica = utenteUltimaModifica;
		this.idMusicProvider = idMusicProvider;
	}

	public BigInteger getIdPalinsesto() {
		return idPalinsesto;
	}

	public void setIdPalinsesto(BigInteger idPalinsesto) {
		this.idPalinsesto = idPalinsesto;
	}

	public Boolean getAttivo() {
		return attivo;
	}

	public void setAttivo(Boolean attivo) {
		this.attivo = attivo;
	}

	public String getCodiceDitta() {
		return codiceDitta;
	}

	public void setCodiceDitta(String codiceDitta) {
		this.codiceDitta = codiceDitta;
	}

	public String getDataCreazione() {
		return dataCreazione;
	}

	public void setDataCreazione(String dataCreazione) {
		this.dataCreazione = dataCreazione;
	}

	public String getDataInizioValidita() {
		return dataInizioValidita;
	}

	public void setDataInizioValidita(String dataInizioValidita) {
		this.dataInizioValidita = dataInizioValidita;
	}

	public String getDataFineValidita() {
		return dataFineValidita;
	}

	public void setDataFineValidita(String dataFineValidita) {
		this.dataFineValidita = dataFineValidita;
	}

	public String getDataUltimaModifica() {
		return dataUltimaModifica;
	}

	public void setDataUltimaModifica(String dataUltimaModifica) {
		this.dataUltimaModifica = dataUltimaModifica;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getUtenteUltimaModifica() {
		return utenteUltimaModifica;
	}

	public void setUtenteUltimaModifica(String utenteUltimaModifica) {
		this.utenteUltimaModifica = utenteUltimaModifica;
	}

	public BigInteger getIdMusicProvider() {
		return idMusicProvider;
	}

	public void setIdMusicProvider(BigInteger idMusicProvider) {
		this.idMusicProvider = idMusicProvider;
	}

}
