
package com.alkemytech.sophia.codman.rest.performing.service;

import com.alkemytech.sophia.codman.dto.PerfEventiPagatiDTO;
import com.alkemytech.sophia.codman.dto.PerfUtilizzazioniDTO;
import com.alkemytech.sophia.codman.dto.ProgrammaMusicaleDTO;
import com.alkemytech.sophia.codman.dto.performing.AggiMcDTO;
import com.alkemytech.sophia.codman.dto.performing.FatturaDetailDTO;
import com.alkemytech.sophia.codman.entity.performing.*;
import com.alkemytech.sophia.codman.dto.performing.PerfCodificaDTO;
import com.alkemytech.sophia.codman.dto.performing.RiconciliazioneImportiDTO;
import com.alkemytech.sophia.codman.entity.performing.security.ReportPage;

import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by idilello on 7/11/16.
 */
public interface IProgrammaMusicaleService {

    List<String> getTipoProgrammi();

    ReportPage getFlussoGuida(Long contabilitaIniziale, Long contabilitaFinale, Integer page);

    List<ProgrammaMusicale> getTotFlussoGuida(Long contabilitaIniziale, Long contabilitaFinale);
    
    ReportPage getFlussoRientrati(Long contabilitaIniziale, Long contabilitaFinale, Integer page);

    int storeFileContant(String filePathName);

    long getCountTracciamentoPM(String dataIniziale, String dataFinale, Long numeroPM, Integer page, String tipoDocumento, String seprag, String idEvento,
                                String fattura, String reversale, String voceIncasso, String permesso, String locale, String codiceBALocale,
                                String codiceSAPOrganizzatore, String organizzatore, String titoloOpera, String dataInizioEvento, String dataFineEvento,
                                String direttoreEsecuzione, String supporto, String tipoProgrammi, String gruppoPrincipale);
    
    ReportPage getTracciamentoPM(String dataIniziale, String dataFinale, Long numeroPM, Integer page, String tipoDocumento, String seprag, String idEvento,
                                 String fattura, String reversale, String voceIncasso, String permesso, String locale, String codiceBALocale,
                                 String codiceSAPOrganizzatore, String organizzatore, String titoloOpera, String dataInizioEvento, String dataFineEvento,
                                 String direttoreEsecuzione, String supporto, String tipoProgrammi, String gruppoPrincipale, String order);

    ReportPage getListaEventiPagati(PerfEventiPagatiDTO perfEventiPagatiDTO, String inizioPeriodoContabile, String finePeriofoContabile, String organizzatore,
                                    String direttoreEsecuzione, String permesso, String numeroPM, String titoloOpera, String codiceSAP,
                                    String dataInizioEvento, String dataFineEvento, String fatturaValidata, String order);

    long getCountListaEventiPagati(PerfEventiPagatiDTO perfEventiPagatiDTO, String inizioPeriodoContabile, String finePeriofoContabile,
                                   String organizzatore, String direttoreEsecuzione, String permesso, String numeroPM, String titoloOpera, String codiceSAP,
                                   String dataInizioEvento, String dataFineEvento, String fatturaValidata);

    List<EventiPagati> getEventiByFattura(String numeroFattura);
    
    List<EventiPagati> getEvento(Long eventoId);

    long getCountListaMovimentazioni(Long inizioPeriodoContabile, Long finePeriodoContabile, Long evento, String fattura, Long reversale, String voceIncasso,
                                     Date dataInizioEvento, Date dataFineEvento, String seprag, String tipoDocumento, String locale, String sapOrganizzatore,
                                     String tipoSupporto, String tipoProgramma, String direttoreEsecuzione, String titoloOpera, String numeroPM, String permesso,
                                     Integer page, String codiceBALocale, String organizzatore);
    
    ReportPage getListaMovimentazioni(Long inizioPeriodoContabile, Long finePeriodoContabile, Long evento, String fattura, Long reversale, String voceIncasso,
                                      Date dataInizioEvento, Date dataFineEvento, String seprag, String tipoDocumento, String locale, String sapOrganizzatore,
                                      String tipoSupporto, String tipoProgramma, String direttoreEsecuzione, String titoloOpera, String numeroPM, String permesso,
                                      Integer page, String codiceBALocale, String organizzatore, String order);

    ReportPage getListaAggregatiSiada(Long inizioPeriodoContabile, Long finePeriodoContabile, String voceIncasso, String tipoSupporto, String tipoProgramma, Long numeroPM, Integer page);

    ReportPage getListaAggregatiEventi(Long inizioPeriodoContabile, Long finePeriodoContabile, String idPuntoTerritoriale);

    List<ReportPage> getListaAggregatiMovimenti(Long inizioPeriodoContabile, Long finePeriodoContabile, String idPuntoTerritoriale, Integer page, String correntiArretrati);

    List<EventiPagati> getDettagliMovimento(Long movimentoId);

    ProgrammaMusicale getDettaglioPM(Long idProgrammaMusicale, Long idMovimento);
    
    void updatePmOnDetail(Long idPM, String flagCampionamento, String flagGruppo, Double durata, String durataMeno30Sec, String flagPD, Long idOpera);

    ProgrammaMusicale getDettaglioPM(Long idProgrammaMusicale);

    Integer updateContabilitaOriginale(Integer nuovaContabilita, List<Object> listaMovimenti);
    
    void updatePMAttesiInEventiPagati(Long pmPrevisti, Long pmPrevistiSpalla, Long idEvento);
    
    void updatePMAttesiInMovimenti(Integer pmPrevisti, Integer pmPrevistiSpalla, Long idMovimento);
    
    long getCountListaFatture(EventiPagati eventoPagato, MovimentoContabile movimentoContabile, String fatturaValidata);
    
    ReportPage getListaFatture(EventiPagati eventoPagato, MovimentoContabile movimentoContabile, String fatturaValidata, Integer page, String order);
    
    List<EventiPagati> getDettaglioFattureOnEvento(String numeroFattura);
    
    List<MovimentoContabile> getDettaglioFattureOnMovimento(String numeroFattura);

    FatturaDetailDTO getFatturaDetail(String numeroFattura);
    
    Double fattureImportoTotByIdEvento(Double idEvento);
    
    Integer addUtilizzazione(Utilizzazione utilizzazione);
    
    ReportPage getListaPmDisponibili(String numeroPm, Integer page);

	void sgancioPm(Long idPm);
	
	void sgancioPm(Long idPm, String voceIncasso, Long idEvento);

	void aggancioPm(String numeroPm, String voceIncasso, Long idEvento);
	
	void removeOpera(Long idUtilizzazione);
	
	void addPm(ProgrammaMusicale programmaMusicale);
	
	void updatePm(ProgrammaMusicaleDTO programmaMusicaleDto);
	
	void updateOpera(PerfUtilizzazioniDTO perfUtilizzazioniAddDTO);

	Integer controlloPmRientrati(Long idEvento);
	
	Utilizzazione getOpera(Long idOpera);
	
	MovimentoContabile getMovimento(Long idMovimento);

	List<ProgrammaMusicale> getListaProgrammiMusicali(String dataIniziale, String dataFinale, Long numeroPM, String tipoDocumento, String seprag, String idEvento,
                                                      String fattura, String reversale, String voceIncasso, String permesso, String locale, String codiceBALocale,
                                                      String codiceSAPOrganizzatore, String organizzatore, String titoloOpera, String dataInizioEvento, String dataFineEvento,
                                                      String direttoreEsecuzione, String supporto, String tipoProgrammi, String gruppoPrincipale, String order, boolean allList);
	
	List<ProgrammaMusicale> getListaProgrammiMusicali(String dataIniziale, String dataFinale, Long numeroPM, String tipoDocumento, String seprag, String idEvento,
                                                      String fattura, String reversale, String voceIncasso, String permesso, String locale, String codiceBALocale,
                                                      String codiceSAPOrganizzatore, String organizzatore, String titoloOpera, String dataInizioEvento, String dataFineEvento,
                                                      String direttoreEsecuzione, String supporto, String tipoProgrammi, String gruppoPrincipale, String order);
	
	List<MovimentoContabile> getListaMovimenti(Long inizioPeriodoContabile, Long finePeriodoContabile, Long evento, String fattura, Long reversale, String voceIncasso,
                                               Date dataInizioEvento, Date dataFineEvento, String seprag, String tipoDocumento, String locale, String sapOrganizzatore,
                                               String tipoSupporto, String tipoProgramma, String gruppoPrincipale, String titoloOpera, String numeroPM, String permesso,
                                               Integer page, String codiceBALocale, String organizzatore, String order);
	
	List<MovimentoContabile> getListaMovimenti(Long inizioPeriodoContabile, Long finePeriodoContabile, Long evento, String fattura, Long reversale, String voceIncasso,
                                               Date dataInizioEvento, Date dataFineEvento, String seprag, String tipoDocumento, String locale, String sapOrganizzatore,
                                               String tipoSupporto, String tipoProgramma, String gruppoPrincipale, String titoloOpera, String numeroPM, String permesso,
                                               Integer page, String codiceBALocale, String organizzatore, String order, boolean allList);
	

    List<EventiPagati> getListaEvPagati(PerfEventiPagatiDTO perfEventiPagatiDTO, String inizioPeriodoContabile, String finePeriofoContabile, String organizzatore,
                                        String direttoreEsecuzione, String permesso, String numeroPM, String titoloOpera, String codiceSAP,
                                        String dataInizioEvento, String dataFineEvento, String fatturaValidata, String order);
    
    void addOpera(Utilizzazione utilizzazione);
    
    EventiPagati getEventoPagato(Long idEventoPagato);
//	public List<Utilizzazione> getOpereDisponibili():

    List<String> getVociIncassoPerIdEvento(Long idEvento);

    List<Long> getIdMovimentiDaSganciare(Long idPm, String voceIncasso, Long idEvento);
    
    List<String> addConfigurazioneAggioMc(AggiMcDTO aggio);

    List<AggiMc> getListaAggiAttivi(String inizioPeriodoValidazione, String finePeriodoValidazione, String codiceRegola, String order);

    AggiMc getAggioAttivo(Long id);

    List<String> getValidazioneDate(String inizioPeriodoValidazione, String finePeriodoValidazione);

    List<String> getValidazioneDateOnUpdate(String inizioPeriodoValidazione, String finePeriodoValidazione, String codiceOpera);

    List<String> updateAggi(AggiMcDTO aggio);
    
    List<String> updateDataAggi(AggiMcDTO aggio);

    List<String> updateParametriAggi(AggiMcDTO aggio);

    List<AggiMc> getStoricoAggio(String codiceRegola);
    


    ReportPage getRiconciliazioneImportiList(String voceIncasso, String agenzia, String sede, String seprag, String singolaFattura,
                                             String inizioPeriodoContabile, String finePeriodoContabile, Date inizioPeriodoFattura, Date finePeriodoFattura, Integer page, String order, String fatturaValidata, String megaConcerto);

    TotaleRiconciliazioneImporti getCountRiconciliazioneImportiList(String voceIncasso, String agenzia, String sede, String seprag, String singolaFattura,
                                            String inizioPeriodoContabile, String finePeriodoContabile, Date inizioPeriodoFattura, Date finePeriodoFattura, String fatturaValidata, String megaConcerto);
    
    String updateFlagMegaConcert(FatturaDetailDTO fatturaDetailDTO);
    
    List<Long> getEventiToEditOnAggio(AggiMcDTO aggio);

    PerfCodificaDTO getCodifica(Long idCombana, String user);
    
    
    public FatturaDetailDTO riconciliaImporti(RiconciliazioneImportiDTO riconciliazioneImporti, String username);
    
    public FatturaDetailDTO riconciliaVociIncasso(RiconciliazioneImportiDTO riconciliazioneImporti, String username);
    
    public FatturaDetailDTO ripristinaRiconciliazione(RiconciliazioneImportiDTO riconciliazioneImporti, String username);
    
}
