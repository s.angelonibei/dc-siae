package com.alkemytech.sophia.codman.rest.performing.service;


import java.util.Date;
import java.util.List;

import com.alkemytech.sophia.codman.dto.performing.BonificaDTO;
import com.alkemytech.sophia.codman.dto.performing.csv.BonificaDirettoreListToCsv;
import com.alkemytech.sophia.codman.dto.performing.csv.BonificaEventoPagatoListToCsv;
import com.alkemytech.sophia.codman.dto.performing.csv.BonificaManifestazioneListToCsv;
import com.alkemytech.sophia.codman.dto.performing.csv.BonificaMovimentoContabileListToCsv;
import com.alkemytech.sophia.codman.dto.performing.csv.BonificaProgrammaMusicaleListToCsv;

public interface IAggiornamentoSunService {


    public List<Date> getDateAggiornameto();

    public List<BonificaDTO> getAggiornamenti(String data);

	public List<BonificaProgrammaMusicaleListToCsv> getProgrammiMusicale(String dataBonifica);

	public List<BonificaEventoPagatoListToCsv> getEventiPagati(String dataBonifica);

	public List<BonificaMovimentoContabileListToCsv> getMovimentiContabili(String dataBonifica);

	public List<BonificaManifestazioneListToCsv> getManifestazioni(String dataBonifica);

	public List<BonificaDirettoreListToCsv> getDirettoriEsecuzione(String dataBonifica);
}

