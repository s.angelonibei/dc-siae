package com.alkemytech.sophia.codman.rest;

import com.alkemytech.sophia.codman.dto.enumaration.IngestionCodificatoStatus;
import com.alkemytech.sophia.codman.entity.IdentifiedSong;
import com.alkemytech.sophia.codman.entity.MmIngestionCodificato;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.alkemytech.sophia.codman.guice.UnidentifiedDataSource;
import com.alkemytech.sophia.codman.multimedialelocale.entity.UnidentifiedSong;
import com.alkemytech.sophia.common.s3.S3Service;
import com.amazonaws.services.s3.AmazonS3URI;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.QuoteMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.*;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Singleton
@Path("ingestion-codificato")
public class IngestionCodificatoService {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final Properties configuration;
    private final Provider<EntityManager> provider;
    private final Provider<EntityManager> unidentifiedProvider;
    private final Gson gson;
    private final S3Service s3Service;
    private final IdentifiedSongService identifiedSongService;

    private static final String SUFFIX = "_scarti";
    private static final String EXT = ".csv";


    @Inject
    public IngestionCodificatoService(@Named("configuration") Properties configuration,
                                      @McmdbDataSource Provider<EntityManager> provider,
                                      @UnidentifiedDataSource Provider<EntityManager> unidentifiedProvider,
                                      Gson gson, S3Service s3Service,
                                      IdentifiedSongService identifiedSongService) {
        super();
        this.configuration = configuration;
        this.provider = provider;
        this.unidentifiedProvider = unidentifiedProvider;
        this.gson = gson;
        this.s3Service = s3Service;
        this.identifiedSongService = identifiedSongService;
    }

    @GET
    @Path("all")
    public Response getAll(@DefaultValue("0") @QueryParam("first") int first,
                           @DefaultValue("50") @QueryParam("last") int last) {
        final EntityManager entityManager = provider.get();
        Query q = entityManager.createNamedQuery("MmIngestionCodificato.all").setFirstResult(first)
                .setMaxResults(1 + last - first);

        List<MmIngestionCodificato> resultList = q.getResultList();

        //Map<String, List<?>> result = new HashMap<>();
        //result.put("row", resultList);

        final PagedResult pagedResult = new PagedResult();
        if (null != resultList) {
            final int maxrows = last - first;
            final boolean hasNext = resultList.size() > maxrows;
            pagedResult.setRows((hasNext ? resultList.subList(0, maxrows) : resultList)).setMaxrows(maxrows).setFirst(first)
                    .setLast(hasNext ? last : first + resultList.size()).setHasNext(hasNext).setHasPrev(first > 0);
        } else {
            pagedResult.setMaxrows(0).setFirst(0).setLast(0).setHasNext(false).setHasPrev(false);
        }

        return Response.ok(gson.toJson(pagedResult)).build();
    }

    @POST
    @Path("upload-csv")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadCsv(@FormDataParam("file") InputStream uploadedInputStream,
                              @FormDataParam("file") FormDataContentDisposition fileDetail) throws IOException {

        final AmazonS3URI s3URI = new AmazonS3URI(configuration.getProperty("ingestionCodificato.s3Path"));
        final String uploadDir = configuration.getProperty("csv.download.directory");
        final File file = File.createTempFile("_ingestion_" + System.currentTimeMillis(), ".csv", new File(uploadDir));
        file.deleteOnExit();
        final JsonObject json = new JsonObject();
        final String exception;
        final String fileName = fileDetail.getFileName();

        final MmIngestionCodificato fileIngestion = new MmIngestionCodificato();
        final Timestamp timestamp;

        if (saveToFile(uploadedInputStream, file)) {
            if (s3Service.upload(s3URI.getBucket(), s3URI.getKey() + fileName, file)) {
                final EntityManager em = provider.get();
                timestamp = new Timestamp(System.currentTimeMillis());
                fileIngestion.setNomeFile(fileDetail.getFileName());
                fileIngestion.setStatoIngestion(IngestionCodificatoStatus.INCORSO.getName());
                fileIngestion.setRigheTotali(0);
                fileIngestion.setRigheElaborate(0);
                fileIngestion.setDataCaricamento(timestamp);
                fileIngestion.setDataAggiornamento(timestamp);
                fileIngestion.setPathCsv(s3URI + fileName);

                try {
                    em.getTransaction().begin();
                    em.persist(fileIngestion);
                    em.getTransaction().commit();

                } catch (Exception e) {

                    if (em.getTransaction().isActive()) {
                        em.getTransaction().rollback();
                    }
                    json.addProperty("Error", e.toString());
                    exception = e.toString();
                    logger.error("Impossibile caricare il file", e);

                    fileIngestion.setStatoIngestion(IngestionCodificatoStatus.ERRORE.getName());
                    fileIngestion.setDescrizioneErrore(exception);

                    em.getTransaction().begin();
                    em.persist(fileIngestion);
                    em.getTransaction().commit();

                    return Response.ok(gson.toJson(json)).build();
                }

                logger.debug("transfer of file " + fileDetail.getFileName() + " completed");
                json.addProperty("result", "File caricato correttamente");

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            elaborateCsv(file, fileName, s3URI);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
                return Response.ok(gson.toJson(json)).build();
            }
        }
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }

    public void elaborateCsv(File csv, String fileName, AmazonS3URI s3URI) throws IOException {

        final Integer maxThread = Integer.parseInt(configuration.getProperty("ingestionCodificato.thread_count"));
        final Integer executorTimeout = Integer.parseInt(configuration.getProperty("ingestionCodificato.executor_shutdown"));
        final String uploadDir = configuration.getProperty("csv.download.directory");
        final List<String[]> records;
        final EntityManager em = provider.get();
        final MmIngestionCodificato fileIngestion = (MmIngestionCodificato) em.createNamedQuery("MmIngestionCodificato.ByNameAndTime")
                .setParameter("nomeFile", fileName)
                .setMaxResults(1)
                .getSingleResult();

        int idx = fileName.indexOf(EXT);
        String scartiFileName = fileName.substring(0, idx);
        scartiFileName = scartiFileName.concat(SUFFIX);
        final File fileScarti = File.createTempFile(scartiFileName, EXT, new File(uploadDir));
        fileScarti.deleteOnExit();

        try {
            FileReader fileToRead = new FileReader(csv);
            CSVReader csvReader = new CSVReaderBuilder(fileToRead)
                    .withSkipLines(1)
                    .build();
            logger.debug("Csv loaded: " + csv.getName());

            records = csvReader.readAll();
            logger.debug("loaded all records: " + records.size());
            fileToRead.close();
            csvReader.close();

            fileIngestion.setRigheTotali(records.size());
            em.getTransaction().begin();
            em.merge(fileIngestion);
            em.getTransaction().commit();

            int partitionSize = records.size() / maxThread;

            if (records.size() <= maxThread){
                 partitionSize = records.size();
            }
            ExecutorService es = Executors.newCachedThreadPool();
            for (final List<String[]> partitions : Lists.partition(records, partitionSize)) {

                es.execute(new Runnable() {
                    @Override
                    public void run() {
                        logger.debug("Starting elaborateRecords on new Thread " + Thread.currentThread() + " " + partitions.size() + " files");
                        int elaborated = elaborateRecords(partitions, fileScarti);
                        fileIngestion.setRigheElaborate(fileIngestion.getRigheElaborate() + elaborated);
                        entityMerge(fileIngestion);
                        logger.debug("elaborated: " + fileIngestion.getRigheElaborate());
                        logger.debug("Ho aggiunto " + partitions.size() + "elaborati sul db");
                    }
                });
            }
            es.shutdown();
            es.awaitTermination(executorTimeout, TimeUnit.MINUTES);

            if (fileIngestion.getRigheElaborate() < fileIngestion.getRigheTotali()){
                fileIngestion.setStatoIngestion(IngestionCodificatoStatus.ERRORE.getName());
                fileIngestion.setDescrizioneErrore("Non tutti i record sono stati elaborati con successo");
            } else {
                fileIngestion.setStatoIngestion(IngestionCodificatoStatus.COMPLETATO.getName());
            }

            em.getTransaction().begin();
            em.merge(fileIngestion);
            em.getTransaction().commit();

        } catch (Exception e) {
            logger.error("elaborate csv failed: " + e);
            fileIngestion.setStatoIngestion(IngestionCodificatoStatus.ERRORE.getName());
            fileIngestion.setDescrizioneErrore(e.toString());
            em.getTransaction().begin();
            em.merge(fileIngestion);
            em.getTransaction().commit();
        } finally {
            if (fileScarti.length() > 0) {
                if (s3Service.upload(s3URI.getBucket(), s3URI.getKey() + scartiFileName + EXT, fileScarti)) {
                    logger.debug("file scarti salvato su s3");
                    fileIngestion.setPathCsvScarti(s3URI + scartiFileName + EXT);

                    em.getTransaction().begin();
                    em.merge(fileIngestion);
                    em.getTransaction().commit();
                }
            }
            csv.delete();
            fileScarti.delete();
        }
    }

    public int elaborateRecords(List<String[]> partition, File file) {
        EntityManager unidentifiedEm = unidentifiedProvider.get();
        boolean isValid;
        int count = 0;
        String error = "";

        for (String[] strings : partition) {

            try {

                String hashId = strings[3];
                String siaeWorkCode = strings[2];

                isValid = hashId.length() == 40 && siaeWorkCode.length() == 11;

                UnidentifiedSong unidentifiedSong = unidentifiedEm.find(UnidentifiedSong.class, hashId);

                if (unidentifiedSong != null && isValid) {
                    count++;

                    IdentifiedSong identifiedSong = new IdentifiedSong();
                    identifiedSong.setHashId(hashId);
                    identifiedSong.setTitle(unidentifiedSong.getTitle());
                    identifiedSong.setArtists(unidentifiedSong.getArtists());
                    identifiedSong.setSiadaTitle(unidentifiedSong.getSiadaTitle());
                    identifiedSong.setSiadaArtists(unidentifiedSong.getSiadaArtists());
                    identifiedSong.setRoles(unidentifiedSong.getRoles());
                    identifiedSong.setSiaeWorkCode(siaeWorkCode);

                    identifiedSongService.manual(identifiedSong);
                } else {
                    if(!isValid) {
                        error = "Error: Lunghezza HashID o SiaeWorkCode non validi";
                    } else {
                        error = "Error: HashID non trovato su UNIDENTIFIED_SONG";
                    }
                    writeToCsv(strings, file, error);
                }
            } catch (Exception e){
                writeToCsv(strings, file, "Error: record non conforme al tracciato ");
            }

        }
        return count;
    }

    @GET
    @Path("checkIfElaborating")
    @Produces("application/json")
    public Response checkIfElaborating() {

        EntityManager em = provider.get();
        JsonObject jsonResponse = new JsonObject();

        Object result = em.createNamedQuery("MmIngestionCodificato.checkElaborating")
                .getResultList()
                .get(0);

        jsonResponse.addProperty("alreadyElaborating", Integer.parseInt(String.valueOf(result)) > 0);

        return Response.ok(gson.toJson(jsonResponse)).build();
    }

    private void writeToCsv(String[] recordScartato, File file, String error) {
        try {
            final FileWriter writer = new FileWriter(file, true);
            final BufferedWriter bufferedWriter = new BufferedWriter(writer);
            final CSVPrinter printer = new CSVPrinter(bufferedWriter, CSVFormat.EXCEL.withDelimiter(';').withQuoteMode(QuoteMode.MINIMAL));

            recordScartato[recordScartato.length - 1] = recordScartato[recordScartato.length - 1] + ";" + error;
            //Utilizzato printrecord perchè il semplice 'print' non è compatibilità con l'append
            printer.printRecord(recordScartato);
            printer.flush();
            printer.close();
            bufferedWriter.close();
            writer.close();

        } catch (Exception e) {
            logger.error("error while writing to csv");
        }
    }


    private boolean saveToFile(InputStream in, File file) {
        OutputStream out = null;
        try {
            int count = 0;
            final byte[] bytes = new byte[1024];
            out = new FileOutputStream(file);
            while (-1 != (count = in.read(bytes))) {
                out.write(bytes, 0, count);
            }
            out.close();
            out = null;
            return true;
        } catch (IOException e) {
            logger.error("saveToFile", e);
        } finally {
            if (null != out) {
                try {
                    out.close();
                } catch (IOException e) {
                    logger.error("saveToFile", e);
                }
            }
        }
        return false;
    }

    private void entityMerge(MmIngestionCodificato fileIngestion){
        EntityManager em = provider.get();
        try {
            em.getTransaction().begin();
            em.merge(fileIngestion);
            em.getTransaction().commit();
        } catch (Exception e){
            if (em.getTransaction().isActive())
                em.getTransaction().rollback();
            logger.error("entityMerge", e);
        }
    }
}
