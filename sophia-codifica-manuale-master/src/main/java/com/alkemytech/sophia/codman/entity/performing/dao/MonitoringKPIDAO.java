package com.alkemytech.sophia.codman.entity.performing.dao;

import com.alkemytech.sophia.codman.entity.PeriodoRipartizione;
import com.alkemytech.sophia.codman.entity.performing.InformazioneKPICodifica;
import com.alkemytech.sophia.codman.rest.performing.Constants;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.http.util.TextUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository("monitoringKPIDAO")
public class MonitoringKPIDAO implements IMonitoringKPIDAO {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject
    private Provider<EntityManager> provider;

    public MonitoringKPIDAO(Provider<EntityManager> provider) {
        // super();
        this.provider = provider;
    }

    @Override
    public List<InformazioneKPICodifica> getRecordKPI(Long idPeriodoRipartizione, String voceIncasso,String tipologiaReport) {

        // List<InformazioneKPICodifica> listaRecord=new ArrayList<>();

        EntityManager em = provider.get();


//        PeriodoRipartizione p = (PeriodoRipartizione) em.createQuery(
//				"SELECT x from PeriodoRipartizione x "
//						+ "	where x.repertorio='MUSICA' "
//						+ " and x.ambito='PERFORMING' "
//						+ " and x.dataApprovazioneCarichiRiparto IS NULL "
//						+ " order by x.competenzeDa asc ")
//				.setMaxResults(1).getSingleResult();
//        if (idPeriodoRipartizione!=null&&idPeriodoRipartizione.equals(p.getIdPeriodoRipartizione())) {
//        		idPeriodoRipartizione=null;
//		}
        String query = "SELECT  " +
                "    CODICE_VOCE_INCASSO VOCE_INCASSO, " +
                "    NUMERO_COMBANA, " +
                "    NUMERO_UTILIZZAZIONI, " +
                "    pr.COMPETENZE_DA periodoRipartizioneDa, " +
                "    pr.COMPETENZE_A periodoRipartizioneA, " +
                "    pr.CODICE codicePeriodo, " +
                "    NR_UTIL_CODIFICA_AUTOMATICA, " +
                "    NR_UTIL_CODIFICA_MANUALE, " +
                "    NR_UTIL_DA_CODIFICARE_MANUALMENTE, " +
                "    NR_CODIFICATO_DA_GIRARE, " +
                "    VALORE_ECONOMICO_CODIFICATO, " +
                "    VALORE_ECONOMICO_NON_CODIFICATO " +
                "FROM " +
                "    PERF_MONITORAGGIO_KPI " +
                "        LEFT JOIN " +
                "    PERIODO_RIPARTIZIONE pr ON PERF_MONITORAGGIO_KPI.PERIODO_RIPARTIZIONE_ID = pr.ID_PERIODO_RIPARTIZIONE " +
                " WHERE ";

        if (idPeriodoRipartizione != null ) {
        	   query += " PERF_MONITORAGGIO_KPI.PERIODO_RIPARTIZIONE_ID = "+idPeriodoRipartizione;

		} else {query += " PERF_MONITORAGGIO_KPI.PERIODO_RIPARTIZIONE_ID IS NULL ";}
        
        if ( !TextUtils.isEmpty(voceIncasso)) {
     	  query += " AND CODICE_VOCE_INCASSO = '"+voceIncasso+"'";
		}
        
//        if (!TextUtils.isEmpty(tipologiaReport)) {
//        	  if (Constants.PM_CARTACEO.equals(tipologiaReport)) {
//        		 query += " AND pm.SUPPORTO_PM <>'DI' ";
//        	  }else {
//             query += " AND pm.SUPPORTO_PM ='DI' ";
//        	  }
//		}

        query += " GROUP BY CODICE_VOCE_INCASSO ";

        List<Object[]> listaRecord =
                em.createNativeQuery(query).getResultList();

        List<InformazioneKPICodifica> informazioneKPICodificas = new ArrayList<>();

        
        for (Object[] codifica : listaRecord) {
            InformazioneKPICodifica informazioneKPICodifica = new InformazioneKPICodifica((String) codifica[0], (Long) codifica[1], (Long) codifica[2], (Date) codifica[3], (Date) codifica[4], (String) codifica[5], (Long) codifica[6], (Long) codifica[7], (Long) codifica[8], (Long) codifica[9], (BigDecimal) codifica[10], (BigDecimal) codifica[11]);
//            if (TextUtils.isEmpty(informazioneKPICodifica.getCodicePeriodo())) {
//            		informazioneKPICodifica.setCodicePeriodo(p.getCodice());
//			}
            informazioneKPICodificas.add(informazioneKPICodifica);
        }

        return informazioneKPICodificas;
    }

    private void setQuery(String periodoRipartizione, Long voceIncasso, Long buffer) {

    }

}