package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.codman.entity.AnagCountry;

@XmlRootElement 
public class SocietaTutelaDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer id;
	
	private String codice;
	
	private String nominativo;
	
	private AnagCountry homeTerritory;
	
	private String posizioneSIAE;
	
	private String codiceSIADA;
	
	private Integer tenant;

	public String getNominativo() {
		return nominativo;
	}

	public void setNominativo(String nominativo) {
		this.nominativo = nominativo;
	}

	public AnagCountry getHomeTerritory() {
		return homeTerritory;
	}

	public void setHomeTerritory(AnagCountry homeTerritory) {
		this.homeTerritory = homeTerritory;
	}

	public String getPosizioneSIAE() {
		return posizioneSIAE;
	}

	public void setPosizioneSIAE(String posizioneSIAE) {
		this.posizioneSIAE = posizioneSIAE;
	}

	public String getCodiceSIADA() {
		return codiceSIADA;
	}

	public void setCodiceSIADA(String codiceSIADA) {
		this.codiceSIADA = codiceSIADA;
	}

	public String getCodice() {
		return codice;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	public Integer getTenant() {
		return tenant;
	}

	public void setTenant(Integer tenant) {
		this.tenant = tenant;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}