package com.alkemytech.sophia.codman.multimedialelocale.entity;

import javax.persistence.*;
import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.codman.multimedialelocale.dto.CodificaStatistiche;
import com.google.gson.Gson;
import com.opencsv.bean.CsvBindByPosition;

import java.sql.Timestamp;
import java.util.Date;

@Produces("application/json")
@XmlRootElement
@Entity(name = "MultimedialeLocale")
@Table(name = "ML_REPORT")
@NamedQueries({ @NamedQuery(name = "MultimedialeLocale.GetAll", query = "SELECT x FROM MultimedialeLocale x") })
public class MultimedialeLocale {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_ML_REPORT", nullable = false)
	private Integer idMlReport;

	@Column(name = "LICENZA")
	private String licenza;

	@Column(name = "CODICE_LICENZIATARIO")
	private String codiceLicenziatario;

	@Column(name = "NOMINATIVO_LICENZIATARIO")
	private String nominativoLicenziatario;

	@Column(name = "SERVIZIO")
	private String servizio;

	@Column(name = "ORIG_PERIODO_COMPETENZA")
	private String origPeriodoCompetenza;

	@Column(name = "PERIODO_COMPETENZA_DATA_DA")
	private Date periodoCompetenzaDataDa;

	@Column(name = "PERIODO_COMPETENZA_DATA_A")
	private Date periodoCompetenzaDataA;

	@OneToOne
	@JoinColumn(name = "MOD_UTILIZZAZIONE")
	private ModalitaUtilizzazione modUtilizzazione;

	@Column(name = "MOD_FRUIZIONE")
	private String modFruizione;

	@Column(name = "IN_ABBONAMENTO")
	private Boolean inAbbonamento;

	@Column(name = "POSIZIONE_REPORT")
	private String posizioneReport;

	@Column(name = "IDENTIFICATIVO_REPORT")
	private String identificativoReport;

	@Column(name = "PROGRESSIVO_REPORT")
	private Integer progressivoReport;

	@Column(name = "STATO")
	private String stato;

	@Column(name = "STATO_LOGICO")
	private String statoLogico;

	@Column(name = "USA_CODICE_OPERA")
	private Boolean usaCodiceOpera;

	@Column(name = "DATA_UPLOAD")
	private Date dataUpload;

	@Column(name = "POSIZIONE_REPORT_NORMALIZZATO")
	private String posizioneReportNormalizzato;

	@Column(name = "DATA_VALIDAZIONE")
	private Date dataValidazione;

	@Column(name = "VALIDAZIONE_RECORD_TOTALI")
	private Integer validazioneRecordTotali;

	@Column(name = "VALIDAZIONE_RECORD_VALIDI")
	private Integer validazioneRecordValidi;

	@Column(name = "VALIDAZIONE_UTILIZZAZIONI_VALIDE")
	private Integer validazioneUtilizzazioniValide;

	@Column(name = "DATA_CODIFICA")
	private Date dataCodifica;

	@Column(name = "POSIZIONE_REPORT_CODIFICATO")
	private String posizioneReportCodificato;

	@Column(name = "CODIFICA_RECORD_CODIFICATI")
	private Integer codificaRecordCodificati;

	@Column(name = "CODIFICA_UTILIZZAZIONI_CODIFICATE")
	private Integer codificaUtilizzazioniCodificate;

	@Column(name = "ATTIVO")
	private boolean attivo;

	@Column(name = "CODIFICA_INFO")
	private String codificaInfo;

	@Column(name = "SOGLIA_VALIDAZIONE")
	private Double sogliaValidazione;

	@Column(name = "SOGLIA_CODIFICA")
	private Double sogliaCodifica;

	@Column(name = "CODIFICA_STATISTICHE")
	private String codificaStatistiche;

	@Column(name = "RIPARTIZIONE")
	private String periodoRipartizione;
	
	@Column(name = "IMPORTO_NETTO")
	private Double importo;

	@Column(name = "DATA_RIPARTIZIONE")
	private Date dataRipartizione;

	@Column(name = "IMPORTO_EX_ART_18")
	private Double importoExArt18;

	public MultimedialeLocale() {
		super();
	}

	public String getCodificaInfo() {
		return codificaInfo;
	}

	public void setCodificaInfo(String codificaInfo) {
		this.codificaInfo = codificaInfo;
	}

	public Double getSogliaValidazione() {
		return sogliaValidazione;
	}

	public void setSogliaValidazione(Double sogliaValidazione) {
		this.sogliaValidazione = sogliaValidazione;
	}

	public Double getSogliaCodifica() {
		return sogliaCodifica;
	}

	public void setSogliaCodifica(Double sogliaCodifica) {
		this.sogliaCodifica = sogliaCodifica;
	}

	public boolean isAttivo() {
		return attivo;
	}

	public void setAttivo(boolean attivo) {
		this.attivo = attivo;
	}

	public String getStato() {
		return stato;
	}

	public void setStato(String stato) {
		this.stato = stato;
	}

	public Boolean getUsaCodiceOpera() {
		return usaCodiceOpera;
	}

	public void setUsaCodiceOpera(Boolean usaCodiceOpera) {
		this.usaCodiceOpera = usaCodiceOpera;
	}

	public Integer getIdMlReport() {
		return idMlReport;
	}

	public void setIdMlReport(Integer idMlReport) {
		this.idMlReport = idMlReport;
	}

	public String getLicenza() {
		return licenza;
	}

	public void setLicenza(String licenza) {
		this.licenza = licenza;
	}

	public String getCodiceLicenziatario() {
		return codiceLicenziatario;
	}

	public void setCodiceLicenziatario(String codiceLicenziatario) {
		this.codiceLicenziatario = codiceLicenziatario;
	}

	public String getNominativoLicenziatario() {
		return nominativoLicenziatario;
	}

	public void setNominativoLicenziatario(String nominativoLicenziatario) {
		this.nominativoLicenziatario = nominativoLicenziatario;
	}

	public String getServizio() {
		return servizio;
	}

	public void setServizio(String servizio) {
		this.servizio = servizio;
	}

	public Date getPeriodoCompetenzaDataDa() {
		return periodoCompetenzaDataDa;
	}

	public void setPeriodoCompetenzaDataDa(Date periodoCompetenzaDataDa) {
		this.periodoCompetenzaDataDa = periodoCompetenzaDataDa;
	}

	public Date getPeriodoCompetenzaDataA() {
		return periodoCompetenzaDataA;
	}

	public void setPeriodoCompetenzaDataA(Date periodoCompetenzaDataA) {
		this.periodoCompetenzaDataA = periodoCompetenzaDataA;
	}

	public ModalitaUtilizzazione getModUtilizzazione() {
		return modUtilizzazione;
	}

	public void setModUtilizzazione(ModalitaUtilizzazione modUtilizzazione) {
		this.modUtilizzazione = modUtilizzazione;
	}

	public String getModFruizione() {
		return modFruizione;
	}

	public void setModFruizione(String modFruizione) {
		this.modFruizione = modFruizione;
	}

	public Boolean getInAbbonamento() {
		return inAbbonamento;
	}

	public void setInAbbonamento(Boolean inAbbonamento) {
		this.inAbbonamento = inAbbonamento;
	}

	public String getPosizioneReport() {
		return posizioneReport;
	}

	public void setPosizioneReport(String posizioneReport) {
		this.posizioneReport = posizioneReport;
	}

	public String getIdentificativoReport() {
		return identificativoReport;
	}

	public void setIdentificativoReport(String identificativoReport) {
		this.identificativoReport = identificativoReport;
	}

	public Integer getProgressivoReport() {
		return progressivoReport;
	}

	public void setProgressivoReport(Integer progressivoReport) {
		this.progressivoReport = progressivoReport;
	}

	public String getStatoLogico() {
		return statoLogico;
	}

	public void setStatoLogico(String statoLogico) {
		this.statoLogico = statoLogico;
	}

	public String getOrigPeriodoCompetenza() {
		return origPeriodoCompetenza;
	}

	public void setOrigPeriodoCompetenza(String origPeriodoCompetenza) {
		this.origPeriodoCompetenza = origPeriodoCompetenza;
	}

	public Date getDataUpload() {
		return dataUpload;
	}

	public void setDataUpload(Date dataUpload) {
		this.dataUpload = dataUpload;
	}

	public String getPosizioneReportNormalizzato() {
		return posizioneReportNormalizzato;
	}

	public void setPosizioneReportNormalizzato(String posizioneReportNormalizzato) {
		this.posizioneReportNormalizzato = posizioneReportNormalizzato;
	}

	public Date getDataValidazione() {
		return dataValidazione;
	}

	public void setDataValidazione(Date dataValidazione) {
		this.dataValidazione = dataValidazione;
	}

	public Integer getValidazioneRecordTotali() {
		return validazioneRecordTotali;
	}

	public void setValidazioneRecordTotali(Integer validazioneRecordTotali) {
		this.validazioneRecordTotali = validazioneRecordTotali;
	}

	public Integer getValidazioneRecordValidi() {
		return validazioneRecordValidi;
	}

	public void setValidazioneRecordValidi(Integer validazioneRecordValidi) {
		this.validazioneRecordValidi = validazioneRecordValidi;
	}

	public Integer getValidazioneUtilizzazioniValide() {
		return validazioneUtilizzazioniValide;
	}

	public void setValidazioneUtilizzazioniValide(Integer validazioneUtilizzazioniValide) {
		this.validazioneUtilizzazioniValide = validazioneUtilizzazioniValide;
	}

	public Date getDataCodifica() {
		return dataCodifica;
	}

	public void setDataCodifica(Date dataCodifica) {
		this.dataCodifica = dataCodifica;
	}

	public String getPosizioneReportCodificato() {
		return posizioneReportCodificato;
	}

	public void setPosizioneReportCodificato(String posizioneReportCodificato) {
		this.posizioneReportCodificato = posizioneReportCodificato;
	}

	public Integer getCodificaRecordCodificati() {
		return codificaRecordCodificati;
	}

	public void setCodificaRecordCodificati(Integer codificaRecordCodificati) {
		this.codificaRecordCodificati = codificaRecordCodificati;
	}

	public Integer getCodificaUtilizzazioniCodificate() {
		return codificaUtilizzazioniCodificate;
	}

	public void setCodificaUtilizzazioniCodificate(Integer codificaUtilizzazioniCodificate) {
		this.codificaUtilizzazioniCodificate = codificaUtilizzazioniCodificate;
	}

	public String getCodificaStatistiche() {
		return codificaStatistiche;
	}

	public void setCodificaStatistiche(String codificaStatistiche) {
		this.codificaStatistiche = codificaStatistiche;
	}

	public String getPeriodoRipartizione() {
		return periodoRipartizione;
	}

	public void setPeriodoRipartizione(String periodoRipartizione) {
		this.periodoRipartizione = periodoRipartizione;
	}

	public Double getImporto() {
		return importo;
	}

	public void setImporto(Double importo) {
		this.importo = importo;
	}

	public Date getDataRipartizione() {
		return dataRipartizione;
	}

	public void setDataRipartizione(Date dataRipartizione) {
		this.dataRipartizione = dataRipartizione;
	}

	public Double getImportoExArt18() {
		return importoExArt18;
	}

	public void setImportoExArt18(Double importoExArt18) {
		this.importoExArt18 = importoExArt18;
	}


	
}
