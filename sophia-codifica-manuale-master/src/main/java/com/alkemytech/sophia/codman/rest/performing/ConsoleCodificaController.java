package com.alkemytech.sophia.codman.rest.performing;

import static com.alkemytech.sophia.codman.utils.ConfigurationsUtils.getOverrides;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.collections.CollectionUtils;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.codman.dto.performing.CodificaListDTO;
import com.alkemytech.sophia.codman.dto.performing.CodificaOpereRequestDTO;
import com.alkemytech.sophia.codman.dto.performing.ConfigurazioneSoglieDTO;
import com.alkemytech.sophia.codman.dto.performing.MonitoraggioCodificatoResponse;
import com.alkemytech.sophia.codman.dto.performing.PerfCodificaMassivaDTO;
import com.alkemytech.sophia.codman.dto.performing.RiepilogoCodificaManualeResponse;
import com.alkemytech.sophia.codman.entity.Configuration;
import com.alkemytech.sophia.codman.entity.ThresholdSet;
import com.alkemytech.sophia.codman.entity.performing.ConfigurazioniSoglie;
import com.alkemytech.sophia.codman.entity.performing.InformazioneKPICodifica;
import com.alkemytech.sophia.codman.entity.performing.PerfCodificaMassiva;
import com.alkemytech.sophia.codman.entity.performing.dao.IConfigurationDAO;
import com.alkemytech.sophia.codman.entity.performing.security.ReportMonitoringKPI;
import com.alkemytech.sophia.codman.entity.performing.security.ReportPage;
import com.alkemytech.sophia.codman.rest.performing.service.ICodificaService;
import com.alkemytech.sophia.codman.rest.performing.service.ImonitoringKPIService;
import com.alkemytech.sophia.common.s3.S3Service;
import com.alkemytech.sophia.common.sqs.SQSService;
import com.amazonaws.services.s3.AmazonS3URI;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.sun.jersey.multipart.FormDataParam;


@Singleton
@Path("performing/codifica")
public class ConsoleCodificaController {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());

    private ICodificaService codificaService;
    private ImonitoringKPIService monitoringKPIService;
    private Gson gson;
    private String pathFileS3;
    private final S3Service s3Service;
    private Integer limitCodifichePerforming;
    private final SQSService sqsService;
    private IConfigurationDAO configurationDAO;
    private String version;
    private String compression;
    private String configUrl;
    private String startedQueue;
    private String completedQueue;
    private String failedQueue;
    private String toProcessQueue;
    private String enviroment;
    private String sender;
    private String serviceBus;
    private String inputFilesS3Path;
    private String outputFilesS3Path;
    private String queueStatus;
      
    @Inject
    public ConsoleCodificaController(
                                    ICodificaService codificaService,
                                    ImonitoringKPIService monitoringKPIService,
                                    @Named("codifica.configurazioneSoglie.path") String pathFileS3,
                                    Gson gson,
                                    S3Service s3Service,
                                    @Named("performing.limit.codifiche") Integer limitCodifichePerforming,
                                    SQSService sqsService,
                                    IConfigurationDAO configurationDAO,
                                    @Named("upload_massivo.version") String version,
                                    @Named("upload_massivo.compression") String compression,
                                    @Named("upload_massivo.configUrl") String configUrl,
                                    @Named("upload_massivo.startedQueue") String startedQueue,
                                    @Named("upload_massivo.completedQueue") String completedQueue,
                                    @Named("upload_massivo.failedQueue") String failedQueue,
                                    @Named("upload_massivo.sqs.to_process_queue") String toProcessQueue,
                                    @Named("default.environment_sqs") String enviroment,
                                    @Named("upload_massivo.sender") String sender,
                                    @Named("upload_massivo.service_bus") String serviceBus,
                                    @Named("upload_massivo.input.bucket.s3") String inputFilesPath,
                                    @Named("upload_massivo.output.bucket.s3") String outputFilesPath,
                                    @Named("upload_massivo.queue.status") String queueStatus

    ) {
        super();
        this.codificaService = codificaService;
        this.gson = gson;
        this.pathFileS3 = pathFileS3;
        this.s3Service = s3Service;
        this.sqsService = sqsService;
        this.limitCodifichePerforming = limitCodifichePerforming;
        this.monitoringKPIService=monitoringKPIService;
        this.configurationDAO=configurationDAO;
        this.version=version;
        this.compression=compression;
        this.configUrl=configUrl;
        this.startedQueue=enviroment+startedQueue;
        this.completedQueue=enviroment+completedQueue;
        this.failedQueue=enviroment+failedQueue;
        this.toProcessQueue=enviroment+toProcessQueue;
        this.enviroment=enviroment;
        this.sender=sender;
        this.serviceBus= enviroment+serviceBus;
        this.inputFilesS3Path = inputFilesPath;
        this.outputFilesS3Path = outputFilesPath;
        this.queueStatus=queueStatus;
    }

    @GET
    @Path("getKpiCodifica")
    public Response getKpiCodifica(@QueryParam(value = "periodoRipartizione") String periodoRipartizione,
                                   @QueryParam(value = "voceIncasso") String voceIncasso) {

        String response = "Hello world";
        return Response.ok(gson.toJson(response)).build();

    }

    //pagine Codifica Manuale

    @GET
    @Path("manuale/esperto/getCodificaManualeEsperto")
    public Response getCodificaManuale(
            @QueryParam(value = "fonte") String fonte,
            @QueryParam(value = "voce") String voce,
            @QueryParam(value = "dataInizioEvento") String dataInizioEvento,
            @QueryParam(value = "dataFineEvento") String dataFineEvento,
            @QueryParam(value = "dataInizioFattura") String dataInizioFattura,
            @QueryParam(value = "dataFineFattura") String dataFineFattura,
            @QueryParam(value = "titolo") String titolo,
            @QueryParam(value = "nominativo") String nominativo,
            @QueryParam(value = "user") String user
    ) {

        CodificaListDTO response = codificaService.getListCodificaEsperto(
                voce,
                fonte,
                titolo,
                nominativo,
                user, limitCodifichePerforming);
        return Response.ok(gson.toJson(response)).build();
    }

    @GET
    @Path("manuale/base/getCodificaManualeBase")
    public Response getCodificaManuale(@QueryParam(value = "user") String user) {
        CodificaListDTO response = codificaService.getListCodificaBase(user, limitCodifichePerforming);
        return Response.ok(gson.toJson(response)).build();
    }

    @POST
    @Path("manuale/approvaOpera")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response approvaOpera(CodificaOpereRequestDTO codificaOpereRequestDTO) {
        codificaService.getApprovaOpera(codificaOpereRequestDTO);
        return Response.ok().build();
    }

    @GET
    @Path("manuale/getTimerSessione")
    public Response getTimerSessione(@QueryParam(value = "user") String user) {
        TimerSessionResponse response = codificaService.getSessionTimer(user);
        return Response.ok(gson.toJson(response)).build();
    }

    @GET
    @Path("manuale/refreshSession")
    public Response refreshSession(@QueryParam(value = "user") String user) {
        TimerSessionResponse response = codificaService.refreshSession(user);
        return Response.ok(gson.toJson(response)).build();
    }


    //pathS3   s3://siae-sophia-datalake/dev/pm-conf
    //pagina Configurazioni Soglie
    @GET
    @Path("soglie/getConfigurazioniSoglie")
    public Response getConfigurazioniSoglie(
            @QueryParam(value = "inizioPeriodoCompetenza") String inizioPeriodoCompetenza,
            @QueryParam(value = "finePeriodoCompetenza") String finePeriodoCompetenza,
            @QueryParam(value = "order") String order,
            @QueryParam(value = "page") Integer page) {

        if (inizioPeriodoCompetenza != null && inizioPeriodoCompetenza.equals("")) {
            inizioPeriodoCompetenza = null;
        }
        if (inizioPeriodoCompetenza != null && inizioPeriodoCompetenza.equals("")) {
            inizioPeriodoCompetenza = null;
        }
        if (order != null && order.equals("")) {
            order = null;
        }
        if (page == null || page < 1) {
            page = 1;
        }

        Integer totRecords = codificaService.countListaConfigurazioniSoglieAttivi(inizioPeriodoCompetenza, finePeriodoCompetenza);
        List<ConfigurazioniSoglie> configurazioniSoglieAttivi = codificaService.getListaConfigurazioniSoglieAttivi(inizioPeriodoCompetenza, finePeriodoCompetenza, order, page);

        ReportPage reportPage = new ReportPage(configurazioniSoglieAttivi, page);

        reportPage.setTotRecords(totRecords);

        return Response.ok(gson.toJson(reportPage)).build();
    }

    @GET
    @Path("soglie/getConfigurazioneSoglie/{id}")
    public Response getConfigurazioneSoglie(@PathParam("id") Long id) {

        if (id == null || id < 1) {
            return Response.ok(gson.toJson("Impossibile ottenere i dati riguardo alla configurazione")).build();
        }
        ConfigurazioniSoglie configurazioniSoglie = codificaService.getConfigurazioneSoglie(id);
        if (null == configurazioniSoglie) {
            return Response.status(Status.NOT_FOUND).build();
        }

        return Response.ok(gson.toJson(configurazioniSoglie)).build();

    }

    //download configurazione per id
    @GET
    @Path("soglie/exportConfigurazioneSoglie/{id}")
    //download generale configurazione soglie per id
    public Response exportConfigurazioneSoglie(@PathParam("id") Long id) {

        final ConfigurazioniSoglie configurazioniSoglie = codificaService.getConfigurazioneSoglie(id);

        if (null == configurazioniSoglie) {
            return Response.status(Status.NOT_FOUND).build();
        }
        final AmazonS3URI s3Uri = new AmazonS3URI(configurazioniSoglie.getPathFile());
        final ByteArrayOutputStream out = new ByteArrayOutputStream();

        if (s3Service.download(s3Uri.getBucket(), s3Uri.getKey(), out)) {

            String type = "";
            String extension = s3Uri.getKey().substring(s3Uri.getKey().lastIndexOf('.'));
            if (extension.equals(".xls")) {
                type = "application/vnd.ms-excel";
            } else if (extension.equals(".xlsx")) {
                type = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            }

            return Response.ok()
                    .type(type)
                    .entity(out.toByteArray())
                    .build();
        } else {
            return Response.status(Status.NOT_FOUND).build();
        }

    }

    //getStoricoConfigurazionePerCodice
    @GET
    @Path("soglie/getStoricoConfigurazioneSoglie/{codiceConfigurazione}")
    //ottiene lo storico tramite il codiceConfigurazione
    public Response getStoricoConfigurazione(@PathParam("codiceConfigurazione") Long codiceConfigurazione) {


        if (codiceConfigurazione == null || codiceConfigurazione == 0) {
            return Response.ok(gson.toJson("Codice configurazione non valido")).build();
        }

        List<ConfigurazioniSoglie> configurazioniSoglieAttivi = new ArrayList<>();

        configurazioniSoglieAttivi = codificaService.getStoricoConfigurazioneSoglie(codiceConfigurazione);

        return Response.ok(gson.toJson(configurazioniSoglieAttivi)).build();
    }

    //updateConfigurazioneFromXls
    @PUT
    @Path("soglie/updateConfiguration")
    //modifica configurazione soglie tramite file
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response updateConfiguration(@FormDataParam("file") InputStream file,
                                        @FormDataParam("configurazioneSoglie") ConfigurazioneSoglieDTO configurazioneSoglieDTO, @FormDataParam("user") String user) {
        List<String> response = codificaService.updateConfigurazioneSoglie(configurazioneSoglieDTO, user, file, s3Service, pathFileS3);
        if (response.isEmpty())
            return Response.ok().build();
        else
            return Response.status(500).entity(gson.toJson(response)).build();
    }


    @POST
    @Path("soglie/addConfiguration")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response addConfiguration(@FormDataParam("file") InputStream file,
                                     @FormDataParam("configurazioneSoglie") ConfigurazioneSoglieDTO configurazioneSoglieDTO) {
        List<String> response = codificaService.addConfigurazioneSoglie(configurazioneSoglieDTO, file, s3Service, pathFileS3);
        if (response.isEmpty())
            return Response.ok().build();
        else
            return Response.status(500).entity(gson.toJson(response)).build();
    }

    @GET
    @Path("manuale/riepilogo")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRiepilogo(@QueryParam("inizioPeriodoCompetenza") Long idPeriodoRipertizione,
                                 @QueryParam("page") int page,
                                 @QueryParam("order") String order) {

        List<RiepilogoCodificaManualeResponse>
                riepilogoCodificaManualeResponseList =
                codificaService.getRiepilogoCodificaManualeResponseList(idPeriodoRipertizione,
                        page,
                        order);

        return Response.ok(new GenericEntity<List<RiepilogoCodificaManualeResponse>>(riepilogoCodificaManualeResponseList){}).build();
    }

    @GET
    @Path("manuale/monitoraggio")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMonitoraggioCodificato(@QueryParam("idPeriodoRipartizione") Long idPeriodoRipartizione) {

//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        //TODO: implementing real service

        MonitoraggioCodificatoResponse monitoraggioCodificatoResponse = codificaService.getMonitoraggioCodificato(idPeriodoRipartizione,0,"asc");

        /*List<ValoreEconomico> valoreEconomicoList = new ArrayList<>();
		valoreEconomicoList.add(new ValoreEconomico(null, new BigDecimal(1)));
		valoreEconomicoList.add(new ValoreEconomico(new BigDecimal(1), new BigDecimal(10)));
		valoreEconomicoList.add(new ValoreEconomico(new BigDecimal(10), new BigDecimal(50)));
		valoreEconomicoList.add(new ValoreEconomico(new BigDecimal(50), new BigDecimal(100)));
		valoreEconomicoList.add(new ValoreEconomico(new BigDecimal(100), new BigDecimal(500)));
		valoreEconomicoList.add(new ValoreEconomico(new BigDecimal(500), null));

		monitoraggioCodificatoResponse.setValoreEconomicoList(valoreEconomicoList);

        LivelloSomiglianza livelloSomiglianza = new LivelloSomiglianza(null, (byte) 100);

        List<LivelloSomiglianza> livelloSomiglianzaList = new ArrayList();

        for (int i = 95; i >= 60; i -= 5) {
            int j = 0;
            if (i != 100) {
                livelloSomiglianza = new LivelloSomiglianza((byte) (i - 5), (byte) (i));
            }
            livelloSomiglianzaList.add(livelloSomiglianza);
            monitoraggioCodificatoResponse.getMonitoraggioCodifiche().put(livelloSomiglianza.toString(), new HashMap<String, List<NumeroCombane>>());
            for (ValoreEconomico valoreEconomico : valoreEconomicoList) {
                if(monitoraggioCodificatoResponse.getMonitoraggioCodifiche().get(livelloSomiglianza.toString()).get(valoreEconomico.toString())==null){
                    monitoraggioCodificatoResponse.getMonitoraggioCodifiche().get(livelloSomiglianza.toString()).put(valoreEconomico.toString(), new ArrayList<NumeroCombane>());
                }
                monitoraggioCodificatoResponse.getMonitoraggioCodifiche().get(livelloSomiglianza.toString()).get(valoreEconomico.toString()).add(new NumeroCombane(ThreadLocalRandom.current().nextInt(j, j + 100), ThreadLocalRandom.current().nextInt(j + 101, j + 200)));
                j += 5;
            }
        }*/

        if(monitoraggioCodificatoResponse == null)
            return Response.noContent().build();

        GsonBuilder gsonBuilder = new GsonBuilder();

//        monitoraggioCodificatoResponse.setLivelloSomiglianzaList(livelloSomiglianzaList);

//        Type monitoraggioCodificatoResponseType = new TypeToken<MonitoraggioCodificatoResponse>() {}.getType();
//        JsonSerializer<MonitoraggioCodificatoResponse> serializer = new LinkedHashMapOfLinkedHashMapsSerializer(); // implementation detail

        gson = gsonBuilder
//                .registerTypeAdapter(monitoraggioCodificatoResponseType, serializer)
                .enableComplexMapKeySerialization()
                .serializeNulls()
                .create();

//        Type monitoraggioCodificatoResponseType = new TypeToken<LinkedHashMap<LivelloSomiglianza, LinkedHashMap<ValoreEconomico, ArrayList<NumeroCombane>>>>() {}.getType();

//        return Response.ok(new GenericEntity<MonitoraggioCodificatoResponse>(monitoraggioCodificatoResponse){}).build();
        return Response.ok(gson.toJson(monitoraggioCodificatoResponse)).build();
    }
    
    @GET
	@Path("kpiRecord")
	public Response getValorizzazioniConfigurazione(
			@QueryParam(value = "periodoRipartizione") Long periodoRipartizione,
			@QueryParam(value = "voceIncasso") String voceIncasso, @QueryParam(value = "tipologiaReport") String tipologiaReport) {

	    	List<InformazioneKPICodifica> record=new ArrayList<>();
	    	Set<String> periodi=new HashSet<>();
	    
	    try {
	    	
	    	record=monitoringKPIService.getRecordMonitoring(periodoRipartizione, voceIncasso,tipologiaReport);
	    	
	    	
	    	for(InformazioneKPICodifica item : record) {
	    		periodi.add(item.getCodicePeriodo());

                BigDecimal percCodAut = BigDecimal.valueOf(0);
                BigDecimal percCodMan = BigDecimal.valueOf(0);
                BigDecimal percCodDaAppr = BigDecimal.valueOf(0);
                BigDecimal percCodScart = BigDecimal.valueOf(0);

	    		if (item.getNumeroUtilizzazioni() != null && item.getNumeroCodificateAutomatica()!=null) {
                    percCodAut = BigDecimal.valueOf(100 * item.getNumeroCodificateAutomatica()).divide(BigDecimal.valueOf(item.getNumeroUtilizzazioni()),8, RoundingMode.HALF_UP);
                    item.setPercentCodificatoAutomatico(percCodAut);
                } else item.setPercentCodificatoAutomatico(percCodAut);
	    		
	    		if (item.getNumeroUtilizzazioni() != null && item.getNumeroCodificateManuale()!=null){
                    percCodMan = BigDecimal.valueOf(
                            100 * item.getNumeroCodificateManuale()).divide(BigDecimal.valueOf(item.getNumeroUtilizzazioni()),8, RoundingMode.HALF_UP);
                    item.setPercentCodificatoManuale(percCodMan);
                } else item.setPercentCodificatoManuale(percCodMan);
	    		
	    		if (item.getNumeroUtilizzazioni() != null && item.getNumeroNonCodificateDaApprovare()!=null){
                    percCodDaAppr = BigDecimal.valueOf(100 * item.getNumeroNonCodificateDaApprovare()).divide(BigDecimal.valueOf(item.getNumeroUtilizzazioni()),8, RoundingMode.HALF_UP);
                    item.setPercentNonCodificatoCombDaApprovare(percCodDaAppr);
                } else item.setPercentNonCodificatoCombDaApprovare(percCodDaAppr);
	    		
	    		if (item.getNumeroUtilizzazioni() != null && item.getNumeroNonCodificateScartate()!=null) {
                    percCodScart = BigDecimal.valueOf(100 * item.getNumeroNonCodificateScartate()).divide(BigDecimal.valueOf(item.getNumeroUtilizzazioni()),8, RoundingMode.HALF_UP);
                    item.setPercentNonCodificatoCombDaAScartate(percCodScart);
                } else item.setPercentNonCodificatoCombDaAScartate(percCodScart);
            }
	    	
	/*
	 * BigDecimal  puntiFinali=utilizzazioneDTO.getPunti()
							  .add(programmaMusicaleDTO.getPuntiEsclusi()
									  .divide(new BigDecimal(programmaMusicaleDTO.getUtilizzazioni().size())
											  .subtract(programmaMusicaleDTO.getnUtilizzazioniEscluse())));
					
					utilizzazioneDTO.setPuntiFinali(puntiFinali);
	 * 
	 * */
	    	ReportMonitoringKPI report= new ReportMonitoringKPI(record,periodi);   // per return.. gestire periodi
		  

		return Response.ok(gson.toJson(report)).build();
			
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return Response.status(500).build();


	}

	@POST
	@Path("ricerca-massiva/upload-csv")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response addCsv(@FormDataParam("file") InputStream file, @FormDataParam("fileName") String fileName, @Context HttpServletRequest request) {
		try {
			
	    String uuid=UUID.randomUUID().toString();
	    String justFileName = fileName.substring(0, fileName.lastIndexOf("."));
	    String extension = fileName.substring(fileName.lastIndexOf("."));
	    fileName = justFileName + "-" + uuid +extension +".gz";
		
		ByteArrayOutputStream baos = getByteArrayOutputStream(file);
		InputStream inputFile = new ByteArrayInputStream(baos.toByteArray()); 
		InputStream outputFile = new ByteArrayInputStream(baos.toByteArray()); 

		String inputFilePath = codificaService.uploadFile(fileName, inputFile, s3Service, inputFilesS3Path);
		if(inputFilePath==null) return Response.status(500).entity("Errore Caricaneto File").build();
		String outputFilePath = codificaService.uploadFile(fileName, outputFile, s3Service, outputFilesS3Path);
		if(outputFilePath==null) return Response.status(500).entity("Errore Caricaneto File").build();
    	
        HttpSession session = request.getSession();
        String utente = (String)session.getAttribute("sso.user.userName");
        
		List<Configuration> configurations = configurationDAO.getConfiguration("codifica", "ranking");
		if (CollectionUtils.isEmpty(configurations)) {
			return Response.status(500).entity("Configurazione Ranking Non Trovata!").build();
		}
		Configuration ranking = configurations.get(0);
		Map<String, Object> overrides = new HashMap<String, Object>();
		getOverrides(ranking.getSettings(), overrides);
		
		List<Configuration> configurationsViewInfo = configurationDAO.getConfiguration("codifica", "view-info");
		if (CollectionUtils.isEmpty(configurationsViewInfo)) {
			return Response.status(500).entity("Configurazione Informazioni Visualizzate Non Trovata!").build();
		}
		Configuration maturato = configurationsViewInfo.get(0).getInnerConfiguration("maturato");
		Configuration rischio = configurationsViewInfo.get(0).getInnerConfiguration("rischio");
		
		getOverrides(maturato.getSetting("maturato.thresholdset", ThresholdSet.class).getThresholds(), overrides);
		getOverrides(rischio.getSetting("rischio.thresholdset", ThresholdSet.class).getThresholds(), overrides);

		JSONObject configuration = new JSONObject();
		for (Map.Entry<String, Object> entry : overrides.entrySet()) {
			configuration.put(entry.getKey(), entry.getValue());
		}
		
        // send sqs message
        final JsonObject message = new JsonObject();
        final JsonObject body = new JsonObject();
        final JsonObject header = new JsonObject();
        
        JsonParser parser = new JsonParser();
        JsonElement config = parser.parse(configuration.toString());
        body.add("configuration", config);

        final JsonArray items = new JsonArray();
        final JsonObject item = new JsonObject();
        item.addProperty("inputUrl", "s3:/"+inputFilePath);
        item.addProperty("outputUrl", "s3:/"+outputFilePath);
        items.add(item);

        body.addProperty("configurationId", ranking.getId());
        body.addProperty("version", version);
        body.addProperty("compression", compression);
        body.addProperty("configUrl", configUrl);
        body.add("items", items);
        if (!Strings.isNullOrEmpty(startedQueue)) {
            body.addProperty("startedQueue", startedQueue);
        }
        if (!Strings.isNullOrEmpty(completedQueue)) {
            body.addProperty("completedQueue", completedQueue);
        }
        if (!Strings.isNullOrEmpty(failedQueue)) {
            body.addProperty("failedQueue", failedQueue);
        }

        String timestamp=new Date().getTime()+"";
        header.addProperty("timestamp",timestamp);
        header.addProperty("uuid",uuid);
        header.addProperty("queue",toProcessQueue);
        header.addProperty("sender",sender);
        header.addProperty("process","massivo");

        message.add("header",header);
        message.add("body",body);

        logger.info(gson.toJson(message));
        if("ON".equalsIgnoreCase(queueStatus)) {
        	sqsService.sendMessage(serviceBus,gson.toJson(message));
        }

        PerfCodificaMassiva perfCodificaMassiva = new PerfCodificaMassiva();
        perfCodificaMassiva.setInsertTime(new Timestamp(System.currentTimeMillis()));
        perfCodificaMassiva.setState("In Elaborazione");
			perfCodificaMassiva.setFileInput(inputFilePath);
			perfCodificaMassiva.setFileOutput(outputFilePath);
        perfCodificaMassiva.setReceivedMessage(gson.toJson(message));
        perfCodificaMassiva.setUtente(utente);
        perfCodificaMassiva.setToken(uuid);
        perfCodificaMassiva.setFileName(fileName);

        codificaService.saveCodificaMassiva(perfCodificaMassiva);

        if (perfCodificaMassiva.getId() != null)
            return Response.ok().build();
        else
            return Response.status(500).entity("Errore Salvataggio").build();
		} catch(Exception e) {
			return Response.status(500).entity("Errore Upload Csv").build();
		}
    }

	protected ByteArrayOutputStream getByteArrayOutputStream(InputStream file) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int len;
		while ((len = file.read(buffer)) > -1 ) {
		    baos.write(buffer, 0, len);
		}
		baos.flush();
		return baos;
	}
	    
	@GET
	@Path("ricerca-massiva/uploaded-files")
	public Response getUploadedFiles(@QueryParam(value = "order") String order, @QueryParam(value = "page") Integer page) {
		try {
			if (order != null && order.equals("")) {
				order = null;
			}
			if (page == null || page < 1) {
				page = 1;
			}

			Integer totRecords = codificaService.getUploadedFilesCount();

			List<PerfCodificaMassiva> uploadedFiles = codificaService.getUploadedFiles(order, page);

			List<PerfCodificaMassivaDTO> uploadedFileDTOs = new ArrayList<PerfCodificaMassivaDTO>(uploadedFiles.size());

			for (PerfCodificaMassiva pcm : uploadedFiles) {
				PerfCodificaMassivaDTO dto = new PerfCodificaMassivaDTO();
				if (pcm.getId() != null) dto.setId(pcm.getId());
				if (pcm.getReceivedMessage() != null) dto.setReceivedMessage(pcm.getReceivedMessage());
				if (pcm.getState() != null) dto.setState(pcm.getState());
				if (pcm.getInsertTime() != null) dto.setInsertTime(pcm.getInsertTime());
				if (pcm.getEndWorkTime() != null) dto.setEndWorkTime(pcm.getEndWorkTime());
				if (pcm.getUtente() != null) dto.setUtente(pcm.getUtente());
				if (pcm.getFileName() != null) dto.setFileName(pcm.getFileName());
				if (pcm.getFileInput() != null) dto.setFileInput(pcm.getFileInput());
				if (pcm.getFileOutput() != null) dto.setFileOutput(pcm.getFileOutput());
				if (pcm.getToken() != null) dto.setToken(pcm.getToken());
				if (pcm.getErrorMessage() !=null) dto.setErrorMessage(pcm.getErrorMessage());
				uploadedFileDTOs.add(dto);
			}

			ReportPage reportPage = new ReportPage(uploadedFiles, page);

			reportPage.setTotRecords(totRecords);
			
			Gson gson = new GsonBuilder()
			.disableHtmlEscaping()
			.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
			.create();

			return Response.ok(gson.toJson(reportPage)).build();
		} catch (Exception e) {
			logger.error("Errore Uploaded Files", e);
			return Response.status(500).build();
		}
	}
    
    @GET
    @Path("ricerca-massiva/download-csv")
    public Response downloadFileCsv(@QueryParam("fileUrl") String fileUrl) {

        if (null == fileUrl) {
            return Response.status(Status.NOT_FOUND).build();
        }
        
        fileUrl = "https://s3-eu-west-1.amazonaws.com" +fileUrl;
        
        final AmazonS3URI s3Uri = new AmazonS3URI(fileUrl);
        final ByteArrayOutputStream out = new ByteArrayOutputStream();

        if (s3Service.download(s3Uri.getBucket(), s3Uri.getKey(), out)) {

            return Response.ok()
                    .entity(out.toByteArray())
                    .build();
        } else {
            return Response.status(Status.NOT_FOUND).build();
        }

    }
    
}

