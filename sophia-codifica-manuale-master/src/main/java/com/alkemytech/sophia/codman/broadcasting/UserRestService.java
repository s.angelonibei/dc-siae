package com.alkemytech.sophia.codman.broadcasting;

import com.alkemytech.sophia.broadcasting.dto.BdcUtenteDto;
import com.alkemytech.sophia.broadcasting.dto.BdcUtentiReperioRequestDto;
import com.alkemytech.sophia.broadcasting.dto.RepertorioDTO;
import com.alkemytech.sophia.broadcasting.model.BdcBroadcasters;
import com.alkemytech.sophia.broadcasting.model.BdcRepertorio;
import com.alkemytech.sophia.broadcasting.model.BdcUtenti;
import com.alkemytech.sophia.broadcasting.model.BdcUtentiRepertorio;
import com.alkemytech.sophia.broadcasting.service.interfaces.BdcUtentiService;
import com.alkemytech.sophia.codman.utils.EmailUtils;
import com.amazonaws.util.CollectionUtils;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.util.*;

/**
 * Created by Alessandro Ravà on 12/12/2017.
 */
@Singleton
@Path("user")
public class UserRestService {

	private final static String NO_RESULTS = "No results found";
	private final static String USERNAME_ALREDY_EXIST = "Username già presente";
	private final static String EMAIL_ALREDY_EXIST = "Email già presente";
	private final static String USERNAME_NOT_EXIST = "Utente non esistente";

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final String fromEmail;
	private final String password;
	private final String smtp;
	private final String nameSender;
	private final Integer port;
	private final BdcUtentiService bdcUtentiService;
	private Gson gson;
	static char[] charsNumber = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
	static char[] charsUpper = {'Q', 'W', 'E', 'R', 'T', 'Z', 'U', 'I', 'O', 'P', 'A',
	        'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'Y', 'X', 'C', 'V', 'B', 'N', 'M' };
	static char[] charsSpecial = {'!', '£', '$', '%', '<', '>', '@', '#'};
	static char[] chars = { 'q', 'w', 'e', 'r', 't', 'z', 'u', 'i', 'o', 'p', 'a', 's',
	        'd', 'f', 'g', 'h', 'j', 'k', 'l', 'y', 'x', 'c', 'v', 'b', 'n', 'm'};
	
	private static String randomString(int length) {
	    StringBuilder stringBuilder = new StringBuilder();
	    stringBuilder.append(charsNumber[new Random().nextInt(charsNumber.length)]);
	    stringBuilder.append(charsSpecial[new Random().nextInt(charsSpecial.length)]);
	    stringBuilder.append(charsUpper[new Random().nextInt(charsUpper.length)]);
	    for (int i = 0; i < length-3; i++) {
	        stringBuilder.append(chars[new Random().nextInt(chars.length)]);
	    }
	    return stringBuilder.toString();
	}
	@Inject
	public UserRestService(@Named("broadcasting_mail_from") String fromEmail,
			@Named("broadcasting_mail_pass") String password, @Named("broadcasting_mail_smtp_host") String smtp,
			@Named("broadcasting_mail_smtp_port") Integer port,
			@Named("broadcasting.mail.name.sender") String nameSender, BdcUtentiService bdcUtentiService, Gson gson) {
		this.fromEmail = fromEmail;
		this.password = password;
		this.smtp = smtp;
		this.port = port;
		this.bdcUtentiService = bdcUtentiService;
		this.nameSender = nameSender;
		this.gson = gson;

	}

	@GET
	@Path("allUser")
	@Produces("application/json")
	public Response findAll() {
		try {
			List<BdcUtenti> result = bdcUtentiService.findAll();
			if (!CollectionUtils.isNullOrEmpty(result)) {
				return Response.ok(gson.toJson(result)).build();
			} else {
				return Response.ok(gson.toJson(NO_RESULTS)).build();
			}
		} catch (Exception e) {
			logger.error("findAll", e);
		}
		return Response.status(500).build();
	}

	@GET
	@Path("userById")
	@Produces("application/json")
	public Response userById(@QueryParam("id") Integer id) {
		try {
			BdcUtenti result = bdcUtentiService.findById(id);
			if (null != result) {
				return Response.ok(gson.toJson(result)).build();
			} else {
				return Response.ok(gson.toJson(NO_RESULTS)).build();
			}
		} catch (Exception e) {
			logger.error("find", e);
		}
		return Response.status(500).build();
	}

	@GET
	@Path("{id}")
	@Produces("application/json")
	public Response userById2(@PathParam("id") Integer id) {
		return this.userById(id);
	}


	@POST
	@Path("userByBroadcastId")
	@Produces("application/json")
	public Response userByBroadcastId(BdcBroadcasters bdcBroadcasters) {
		try {
			List<BdcUtenti> result = bdcUtentiService.findAllByBroadcaster(bdcBroadcasters);
			if (null != result) {
				return Response.ok(gson.toJson(result)).build();
			} else {
				return Response.ok(gson.toJson(NO_RESULTS)).build();
			}
		} catch (Exception e) {
			logger.error("find", e);
		}
		return Response.status(500).build();
	}

	@POST
	@Path("addUser")
	@Produces("application/json")
	public Response addUser(BdcUtenteDto bdcUtenti) {
		if (bdcUtenti.getBdcUtenti()==null||bdcUtenti.getRepertori()==null||bdcUtenti.getRepertori().size()<=0) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		if(!EmailUtils.isValidEmailAddress(bdcUtenti.getBdcUtenti().getEmail())){
			return Response.status(503).build();
		}
		boolean hasRepertorio=false;
		for (RepertorioDTO repertorio:bdcUtenti.getRepertori()) {
			if (repertorio.isDefaultValue()) {
				if (!repertorio.isActive()) {
					return Response.status(Status.BAD_REQUEST).build();	
				}
				hasRepertorio=true;			
			}			
		}
		if (!hasRepertorio) {
			return Response.status(Status.BAD_REQUEST).build();	
		}
		BdcUtenti utente=bdcUtenti.getBdcUtenti();
		if (this.bdcUtentiService.findByUsername(utente.getUsername()) != null) {
			return Response.status(501).build();
		}

		if (this.bdcUtentiService.findByEmail(utente.getEmail()) != null) {
			return Response.status(502).build();
		}
		if(!bdcUtenti.getBdcUtenti().getPassword().matches("(?=.*[a-z])")&&
				bdcUtenti.getBdcUtenti().getPassword().matches("(?=.*[0-9])")&&
				bdcUtenti.getBdcUtenti().getPassword().matches("(?=.*[A-Z])")&&
				bdcUtenti.getBdcUtenti().getPassword().matches("(?=.*[^\\A-Za-z0-9])")&&
				bdcUtenti.getBdcUtenti().getPassword().matches("(.{8,})")){
			return Response.status(504).build();
		}
		String pass=  bdcUtenti.getBdcUtenti().getPassword();
		utente.setDataCreazione(new Date());
		utente.setDataUltimaModifica(new Date());
		PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		utente.setPassword(passwordEncoder.encode(utente.getPassword()));

		bdcUtentiService.signup(utente,bdcUtenti.getRepertori());
		if (bdcUtenti.isSendMail()) {
			Properties props = new Properties();
			props.put("mail.smtp.host", smtp);
			props.put("mail.smtp.port", port);
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			Authenticator auth = new Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(fromEmail, password);
				}
			};
			Session session = Session.getInstance(props, auth);
			String testoMail = "La tua utenza è stata creata, la password per accedere alla piattaforma broadcaster è : "+ pass;
			EmailUtils.sendEmail(utente.getBdcBroadcasters(), utente, nameSender, session, utente.getEmail(), null, null,
					"Creazione Utenza", testoMail);
		}
		
		return Response.ok().build();
	}

	@POST
	@Path("updateUser")
	@Produces("application/json")
	public Response updateUser(BdcUtenteDto bdcUtenti) {
		if (bdcUtenti.getBdcUtenti()==null||bdcUtenti.getRepertori()==null||bdcUtenti.getRepertori().size()<=0) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		if(!EmailUtils.isValidEmailAddress(bdcUtenti.getBdcUtenti().getEmail())){
			return Response.status(503).build();
		}
		
		boolean hasRepertorio=false;
		for (RepertorioDTO repertorio:bdcUtenti.getRepertori()) {
			if (repertorio.isDefaultValue()) {
				if (!repertorio.isActive()) {
					return Response.status(Status.BAD_REQUEST).build();	
				}
				hasRepertorio=true;			
			}			
		}
		if (!hasRepertorio) {
			return Response.status(Status.BAD_REQUEST).build();	
		}
		BdcUtenti utente=bdcUtenti.getBdcUtenti();
		utente.setDataUltimaModifica(new Date());
		int esito=bdcUtentiService.updateUser(utente,bdcUtenti.getRepertori());
		if(esito==200) {
			return Response.ok().build();
		}else {
			return Response.status(esito).build();
		}
	}

	@POST
	@Path("resetPassword")
	@Produces("application/json")
	public Response resetPassword(BdcUtenti bdcUtenti) {
		Properties props = new Properties();
		props.put("mail.smtp.host", smtp);
		props.put("mail.smtp.port", port);
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		Authenticator auth = new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(fromEmail, password);
			}
		};
		Session session = Session.getInstance(props, auth);
		String newPass =randomString(8);
		
		String testoMail = "La tua nuova password per accedere alla piattaforma broadcaster è stata rigenerata : "+ newPass.toString();
		BdcUtenti bdcUtente=bdcUtentiService.findById(bdcUtenti.getId());
		
		PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		bdcUtente.setPassword(passwordEncoder.encode(newPass.toString()));
		bdcUtente.setUtenteUltimaModifica(bdcUtenti.getUtenteUltimaModifica());
		bdcUtente.setDataUltimaModifica(new Date());
		bdcUtentiService.changePassword(bdcUtente);
		EmailUtils.sendEmail(bdcUtente.getBdcBroadcasters(), bdcUtente, nameSender, session, bdcUtente.getEmail(), null, null,
				"Recupero Password", testoMail);
		return Response.ok().build();
	}

	@POST
	@Path("deleteUser")
	@Produces("application/json")
	public Response deleteUser(BdcUtenti bdcUtente) {
		try {
			bdcUtentiService.deleteUser(bdcUtente);
		} catch (Exception e) {
			return Response.status(500).build();
		}
		return Response.ok().build();
	}
	
	@GET
	@Path("listaRepertori")
	@Produces("application/json")
	public Response repertori() {
		try {
			List<BdcRepertorio> repertori = bdcUtentiService.findRepertori();
			ArrayList<RepertorioDTO> repertoriDto= new ArrayList<>();
			for(BdcRepertorio bdcRepertorio:repertori) {
				repertoriDto.add(new RepertorioDTO(bdcRepertorio));
			}
			return Response.ok(gson.toJson(repertori)).build();
		} catch (Exception e) {
			return Response.status(500).build();
		}
	}
	
	@GET
	@Path("listaRepertoriUtente")
	@Produces("application/json")
	public Response repertoriUtente(@QueryParam(value = "idUtente") int userId) {
		try {
			List<BdcUtentiRepertorio> repertoriUtente = bdcUtentiService.findRepertoriByUserId(userId);
			List<BdcRepertorio> repertori = bdcUtentiService.findRepertori();
			ArrayList<RepertorioDTO> repertoriDto= new ArrayList<>();
			for(BdcRepertorio bdcRepertorio:repertori) {
				repertoriDto.add(new RepertorioDTO(bdcRepertorio));
			}
			for (BdcUtentiRepertorio bdcUtentiRepertorio:repertoriUtente) {
				for(RepertorioDTO repertorioDTO:repertoriDto) {
					if (bdcUtentiRepertorio.getRepertorio().getNome().equals(repertorioDTO.getNome())) {
						repertorioDTO.setActive(true);
						if (bdcUtentiRepertorio.getDefaultRepertorio()) {
							repertorioDTO.setDefaultValue(true);
						}
					}
				}
			}
			return Response.ok(gson.toJson(repertoriDto)).build();
		} catch (Exception e) {
			return Response.status(500).build();
		}
	}
	
	@POST
	@Path("listaUtentiRepertori")
	@Produces("application/json")
	public Response utentiByRepertori(BdcUtentiReperioRequestDto bdcUtentiReperioRequestDto) {
		try {
			List<BdcUtenti> utenti = bdcUtentiService.findByRepertori(bdcUtentiReperioRequestDto.getBdcBroadcasters(),bdcUtentiReperioRequestDto.getRepertori());
			return Response.ok(gson.toJson(utenti)).build();
		} catch (Exception e) {
			return Response.status(500).build();
		}
	}

	@GET
	@Path("roles")
	@Produces("application/json")
	public Response roles(@Context HttpServletRequest request) {
		HttpSession session = request.getSession();

		Enumeration e = (Enumeration) (session.getAttributeNames());

		JsonObject jObject = new JsonObject();
		//Il JSESSIONID è valido
		jObject.addProperty("authentication",true);
		jObject.addProperty("user",(String)session.getAttribute("sso.user.userName"));
		List<String> result = new ArrayList<>();

		JsonArray jArray = new JsonArray();
		while ( e.hasMoreElements())
		{
			Object attributeName;
			if((attributeName = e.nextElement())!=null)
			{
				jArray.add((String)attributeName);
			}
		}
		jObject.add("permissions",jArray);
		return Response.ok(jObject.toString()).build();

	}


}
