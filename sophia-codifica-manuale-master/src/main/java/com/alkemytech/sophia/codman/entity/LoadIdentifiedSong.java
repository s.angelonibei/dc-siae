package com.alkemytech.sophia.codman.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "LoadIdentifiedSong")
@Table(name = "load_identified_song")
@Data
@NamedQueries({
        @NamedQuery(name = "LoadIdentifiedSong.findByIdAndLastManualTimeLessThan", query = "select lis FROM LoadIdentifiedSong lis where lis.hashId = :hashId"),
        @NamedQuery(name = "LoadIdentifiedSong.updateByUsernameAndHashId", query = "update LoadIdentifiedSong lis set lis.username = null, lis.lastManualTime = 0 where lis.username = :username and lis.hashId <> :hashId and lis.lastManualTime < :lastManualTime and lis.lastAutoTime < :lastAutoTime"),
})
@NamedQuery(name = "LoadIdentifiedSong.findAll", query = "SELECT u FROM LoadIdentifiedSong u")
public class LoadIdentifiedSong implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "hash_id", insertable = false, nullable = false)
    private String hashId;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "artists", nullable = false)
    private String artists;

    @Column(name = "roles")
    private String roles;

    @Column(name = "sales_count", nullable = false)
    private Long salesCount;

    @Column(name = "insert_time", nullable = false)
    private Long insertTime;

    @Column(name = "last_auto_time", nullable = false)
    private Long lastAutoTime = 0L;

    @Column(name = "last_manual_time", nullable = false)
    private Long lastManualTime = 0L;

    @Column(name = "siada_title")
    private String siadaTitle;

    @Column(name = "siada_artists")
    private String siadaArtists;

    @Column(name = "uuid")
    private String uuid;

    @Column(name = "username")
    private String username;

    
}