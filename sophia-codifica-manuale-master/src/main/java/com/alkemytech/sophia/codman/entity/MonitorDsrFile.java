package com.alkemytech.sophia.codman.entity;

import lombok.Data;
import org.eclipse.persistence.annotations.CascadeOnDelete;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "MonitorDsrFile")
@Data
@Table(name = "MM_MONITOR_DSR_FILE")
@CascadeOnDelete
@NamedQuery(name = "MonitorDsrFile.unidentified", query = "select x from MonitorDsrFile x where x.offertaCommerciale is null and x.idDsp = :idDsp")
public class MonitorDsrFile implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", insertable = false, nullable = false)
    private Integer ID;

    @Column(name = "ID_DSP", nullable = false)
    private String idDsp;

    @Column(name = "PERIODO", nullable = false)
    private String periodo;

    @Column(name = "NOME_FILE", nullable = false)
    private String nomeFile;

    @Column(name = "TERRITORIO")
    private String territorio;

    @Column(name = "OFFERTA_COMMERCIALE")
    private String offertaCommerciale;

    @Column(name = "TIPO_UTILIZZO")
    private String tipoUtilizzo;

    
}