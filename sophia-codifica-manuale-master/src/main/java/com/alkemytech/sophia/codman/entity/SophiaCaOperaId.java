package com.alkemytech.sophia.codman.entity;

import java.io.Serializable;

import com.google.gson.GsonBuilder;

@SuppressWarnings("serial")
public class SophiaCaOperaId implements Serializable {
	
	String codiceOpera;
	String hashTitle;

	public SophiaCaOperaId() {
		super();
	}
	
	public SophiaCaOperaId(String codiceOpera, String hashTitle) {
		super();
		this.codiceOpera = codiceOpera;
		this.hashTitle = hashTitle;
	}
	
	public String getCodiceOpera() {
		return codiceOpera;
	}

	public void setCodiceOpera(String codiceOpera) {
		this.codiceOpera = codiceOpera;
	}

	public String getHashTitle() {
		return hashTitle;
	}

	public void setHashTitle(String hashTitle) {
		this.hashTitle = hashTitle;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codiceOpera == null) ? 0 : codiceOpera.hashCode());
		result = prime * result + ((hashTitle == null) ? 0 : hashTitle.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SophiaCaOperaId other = (SophiaCaOperaId) obj;
		if (codiceOpera == null) {
			if (other.codiceOpera != null)
				return false;
		} else if (!codiceOpera.equals(other.codiceOpera))
			return false;
		if (hashTitle == null) {
			if (other.hashTitle != null)
				return false;
		} else if (!hashTitle.equals(other.hashTitle))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting().create().toJson(this);
	}
	
}
