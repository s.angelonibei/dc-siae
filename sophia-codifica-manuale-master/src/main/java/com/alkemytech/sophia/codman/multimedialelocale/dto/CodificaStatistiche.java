
package com.alkemytech.sophia.codman.multimedialelocale.dto;

import java.util.List;

import javax.persistence.Transient;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CodificaStatistiche {

    @SerializedName("puntiDemSiae")
    @Expose
    private Double puntiDemSiae;
    @SerializedName("puntiDrmSiae")
    @Expose
    private Double puntiDrmSiae;
    @SerializedName("puntiExArt18")
    @Expose
    private Double puntiExArt18;
    @SerializedName("recordTotali")
    @Expose
    private Integer recordTotali;
    @SerializedName("puntiDemNonSoci")
    @Expose
    private Double puntiDemNonSoci;
    @SerializedName("puntiDrmNonSoci")
    @Expose
    private Double puntiDrmNonSoci;
    @SerializedName("recordNonValidi")
    @Expose
    private Integer recordNonValidi;
    @SerializedName("recordNonCodificati")
    @Expose
    private Integer recordNonCodificati;
    @SerializedName("utilizzazioniTotali")
    @Expose
    private Integer utilizzazioniTotali;
    @SerializedName("puntiDemRimanentiCasi")
    @Expose
    private Double puntiDemRimanentiCasi;
    @SerializedName("puntiDrmRimanentiCasi")
    @Expose
    private Double puntiDrmRimanentiCasi;
    @SerializedName("recordPubblicoDominio")
    @Expose
    private Integer recordPubblicoDominio;
    @SerializedName("puntiDemAngloAmericani")
    @Expose
    private Double puntiDemAngloAmericani;
    @SerializedName("puntiDrmAngloAmericani")
    @Expose
    private Double puntiDrmAngloAmericani;
    @SerializedName("recordSchemaNonTrovato")
    @Expose
    private Integer recordSchemaNonTrovato;
    @SerializedName("puntiDemPubblicoDominio")
    @Expose
    private Double puntiDemPubblicoDominio;
    @SerializedName("puntiDrmPubblicoDominio")
    @Expose
    private Double puntiDrmPubblicoDominio;
    @SerializedName("repertoriAngloAmericani")
    @Expose
    private List<RepertoriAngloAmericani> repertoriAngloAmericani = null;
    @SerializedName("recordCodificatiRegolari")
    @Expose
    private Integer recordCodificatiRegolari;
    @SerializedName("recordCodificatiProvvisori")
    @Expose
    private Integer recordCodificatiProvvisori;
    @SerializedName("recordCodificatiIrregolari")
    @Expose
    private Integer recordCodificatiIrregolari;
    @SerializedName("utilizzazioniNonCodificate")
    @Expose
    private Integer utilizzazioniNonCodificate;
    @SerializedName("utilizzazioniPubblicoDominio")
    @Expose
    private Integer utilizzazioniPubblicoDominio;
    @SerializedName("utilizzazioniSchemaNonTrovato")
    @Expose
    private Integer utilizzazioniSchemaNonTrovato;
    @SerializedName("utilizzazioniCodificateRegolari")
    @Expose
    private Integer utilizzazioniCodificateRegolari;
    @SerializedName("utilizzazioniCodificateIrregolari")
    @Expose
    private Integer utilizzazioniCodificateIrregolari;
    @SerializedName("utilizzazioniCodificateProvvisorie")
    @Expose
    private Integer utilizzazioniCodificateProvvisorie;
    @Transient
    @SerializedName("percPolverizzazione")
    @Expose
    private String percPolverizzazione;
    public Double getPuntiDemSiae() {
        return puntiDemSiae;
    }

    public void setPuntiDemSiae(Double puntiDemSiae) {
        this.puntiDemSiae = puntiDemSiae;
    }

    public Double getPuntiDrmSiae() {
        return puntiDrmSiae;
    }

    public void setPuntiDrmSiae(Double puntiDrmSiae) {
        this.puntiDrmSiae = puntiDrmSiae;
    }

    public Double getPuntiExArt18() {
        return puntiExArt18;
    }

    public void setPuntiExArt18(Double puntiExArt18) {
        this.puntiExArt18 = puntiExArt18;
    }

    public Integer getRecordTotali() {
        return recordTotali;
    }

    public void setRecordTotali(Integer recordTotali) {
        this.recordTotali = recordTotali;
    }

    public Double getPuntiDemNonSoci() {
        return puntiDemNonSoci;
    }

    public void setPuntiDemNonSoci(Double puntiDemNonSoci) {
        this.puntiDemNonSoci = puntiDemNonSoci;
    }

    public Double getPuntiDrmNonSoci() {
        return puntiDrmNonSoci;
    }

    public void setPuntiDrmNonSoci(Double puntiDrmNonSoci) {
        this.puntiDrmNonSoci = puntiDrmNonSoci;
    }

    public Integer getRecordNonValidi() {
        return recordNonValidi;
    }

    public void setRecordNonValidi(Integer recordNonValidi) {
        this.recordNonValidi = recordNonValidi;
    }

    public Integer getRecordNonCodificati() {
        return recordNonCodificati;
    }

    public void setRecordNonCodificati(Integer recordNonCodificati) {
        this.recordNonCodificati = recordNonCodificati;
    }

    public Integer getUtilizzazioniTotali() {
        return utilizzazioniTotali;
    }

    public void setUtilizzazioniTotali(Integer utilizzazioniTotali) {
        this.utilizzazioniTotali = utilizzazioniTotali;
    }

    public Double getPuntiDemRimanentiCasi() {
        return puntiDemRimanentiCasi;
    }

    public void setPuntiDemRimanentiCasi(Double puntiDemRimanentiCasi) {
        this.puntiDemRimanentiCasi = puntiDemRimanentiCasi;
    }

    public Double getPuntiDrmRimanentiCasi() {
        return puntiDrmRimanentiCasi;
    }

    public void setPuntiDrmRimanentiCasi(Double puntiDrmRimanentiCasi) {
        this.puntiDrmRimanentiCasi = puntiDrmRimanentiCasi;
    }

    public Integer getRecordPubblicoDominio() {
        return recordPubblicoDominio;
    }

    public void setRecordPubblicoDominio(Integer recordPubblicoDominio) {
        this.recordPubblicoDominio = recordPubblicoDominio;
    }

    public Double getPuntiDemAngloAmericani() {
        return puntiDemAngloAmericani;
    }

    public void setPuntiDemAngloAmericani(Double puntiDemAngloAmericani) {
        this.puntiDemAngloAmericani = puntiDemAngloAmericani;
    }

    public Double getPuntiDrmAngloAmericani() {
        return puntiDrmAngloAmericani;
    }

    public void setPuntiDrmAngloAmericani(Double puntiDrmAngloAmericani) {
        this.puntiDrmAngloAmericani = puntiDrmAngloAmericani;
    }

    public Integer getRecordSchemaNonTrovato() {
        return recordSchemaNonTrovato;
    }

    public void setRecordSchemaNonTrovato(Integer recordSchemaNonTrovato) {
        this.recordSchemaNonTrovato = recordSchemaNonTrovato;
    }

    public Double getPuntiDemPubblicoDominio() {
        return puntiDemPubblicoDominio;
    }

    public void setPuntiDemPubblicoDominio(Double puntiDemPubblicoDominio) {
        this.puntiDemPubblicoDominio = puntiDemPubblicoDominio;
    }

    public Double getPuntiDrmPubblicoDominio() {
        return puntiDrmPubblicoDominio;
    }

    public void setPuntiDrmPubblicoDominio(Double puntiDrmPubblicoDominio) {
        this.puntiDrmPubblicoDominio = puntiDrmPubblicoDominio;
    }

    public List<RepertoriAngloAmericani> getRepertoriAngloAmericani() {
        return repertoriAngloAmericani;
    }

    public void setRepertoriAngloAmericani(List<RepertoriAngloAmericani> repertoriAngloAmericani) {
        this.repertoriAngloAmericani = repertoriAngloAmericani;
    }

    public Integer getRecordCodificatiRegolari() {
        return recordCodificatiRegolari;
    }

    public void setRecordCodificatiRegolari(Integer recordCodificatiRegolari) {
        this.recordCodificatiRegolari = recordCodificatiRegolari;
    }

    public Integer getRecordCodificatiProvvisori() {
		return recordCodificatiProvvisori;
	}

	public void setRecordCodificatiProvvisori(Integer recordCodificatiProvvisori) {
		this.recordCodificatiProvvisori = recordCodificatiProvvisori;
	}

	public Integer getRecordCodificatiIrregolari() {
        return recordCodificatiIrregolari;
    }

    public void setRecordCodificatiIrregolari(Integer recordCodificatiIrregolari) {
        this.recordCodificatiIrregolari = recordCodificatiIrregolari;
    }

    public Integer getUtilizzazioniNonCodificate() {
        return utilizzazioniNonCodificate;
    }

    public void setUtilizzazioniNonCodificate(Integer utilizzazioniNonCodificate) {
        this.utilizzazioniNonCodificate = utilizzazioniNonCodificate;
    }

    public Integer getUtilizzazioniPubblicoDominio() {
        return utilizzazioniPubblicoDominio;
    }

    public void setUtilizzazioniPubblicoDominio(Integer utilizzazioniPubblicoDominio) {
        this.utilizzazioniPubblicoDominio = utilizzazioniPubblicoDominio;
    }

    public Integer getUtilizzazioniSchemaNonTrovato() {
        return utilizzazioniSchemaNonTrovato;
    }

    public void setUtilizzazioniSchemaNonTrovato(Integer utilizzazioniSchemaNonTrovato) {
        this.utilizzazioniSchemaNonTrovato = utilizzazioniSchemaNonTrovato;
    }

    public Integer getUtilizzazioniCodificateRegolari() {
        return utilizzazioniCodificateRegolari;
    }

    public void setUtilizzazioniCodificateRegolari(Integer utilizzazioniCodificateRegolari) {
        this.utilizzazioniCodificateRegolari = utilizzazioniCodificateRegolari;
    }

    public Integer getUtilizzazioniCodificateIrregolari() {
        return utilizzazioniCodificateIrregolari;
    }

    public void setUtilizzazioniCodificateIrregolari(Integer utilizzazioniCodificateIrregolari) {
        this.utilizzazioniCodificateIrregolari = utilizzazioniCodificateIrregolari;
    }

	public Integer getUtilizzazioniCodificateProvvisorie() {
		return utilizzazioniCodificateProvvisorie;
	}

	public void setUtilizzazioniCodificateProvvisorie(Integer utilizzazioniCodificateProvvisorie) {
		this.utilizzazioniCodificateProvvisorie = utilizzazioniCodificateProvvisorie;
	}

	public String getPercPolverizzazione() {
		return percPolverizzazione;
	}

	public void setPercPolverizzazione(String percPolverizzazione) {
		this.percPolverizzazione = percPolverizzazione;
	}

}
