package com.alkemytech.sophia.codman.rest.performing.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.alkemytech.sophia.codman.entity.Configuration;
import com.alkemytech.sophia.codman.entity.Setting;
import com.alkemytech.sophia.codman.entity.performing.Circoscrizione;
import com.alkemytech.sophia.codman.entity.performing.Configurazione;
import com.alkemytech.sophia.codman.entity.performing.dao.IConfigurationDAO;
import com.alkemytech.sophia.codman.rest.performing.Constants;
import com.alkemytech.sophia.codman.rest.performing.service.IConfigurationService;
import com.alkemytech.sophia.codman.rest.performing.service.ITraceService;
import com.alkemytech.sophia.codman.rest.performing.serviceimpl.config.utils.ConfigurationValidatorService;
import com.google.inject.Inject;

@Service("configurationService")
public class ConfigurationService implements IConfigurationService{

	IConfigurationDAO configurationDAO;

	private ConfigurationValidatorService configurationValidatorService;

	private ITraceService traceService;

	@Inject
	public ConfigurationService(IConfigurationDAO configurationDAO) {
		this.configurationDAO = configurationDAO;
		this.configurationValidatorService = new ConfigurationValidatorService(configurationDAO);
	}

    public String getParameter(String parameterName){
        return configurationDAO.getParameter(parameterName);
    }


    public void insertParameter(String parameterName, String parameterValue){
         configurationDAO.insertParameter(parameterName, parameterValue);
    }

    public Configurazione getParameterObject(String parameterName){
        return configurationDAO.getParameterObject(parameterName);
    }

    public void insertParameterObject(Configurazione configurazione){
        configurationDAO.insertParameterObject(configurazione);
    }

    public List<Circoscrizione> getCircoscrizioni(){
        return configurationDAO.getCircoscrizioni();
    }

    public void updateConfigurazione(String annoFinePeriodoRipartizione, String meseFinePeriodoRipartizione,
                                     String annoInizioPeriodoContabile, String meseInizioPeriodoContabile,
                                     String annoFinePeriodoContabile, String meseFinePeriodoContabile,
                                     String tipologiaPM, String campionaDigitali){

        if (meseFinePeriodoRipartizione.length()==1)
            meseFinePeriodoRipartizione="0"+meseFinePeriodoRipartizione;
        if (meseInizioPeriodoContabile.length()==1)
            meseInizioPeriodoContabile="0"+meseInizioPeriodoContabile;
        if (meseFinePeriodoContabile.length()==1)
            meseFinePeriodoContabile="0"+meseFinePeriodoContabile;

        configurationDAO.insertParameter(Constants.INIZIO_PERIODO_RICALCOLO, annoInizioPeriodoContabile+meseInizioPeriodoContabile);
        configurationDAO.insertParameter(Constants.FINE_PERIODO_RICALCOLO, annoFinePeriodoContabile+meseFinePeriodoContabile);
        configurationDAO.insertParameter(Constants.FINE_PERIODO_RIPARTIZIONE, annoFinePeriodoRipartizione+meseFinePeriodoRipartizione);
        configurationDAO.insertParameter(Constants.TIPOLOGIA_PM, tipologiaPM);
        configurationDAO.insertParameter(Constants.CAMPIONAMENTO_DIGITALI, campionaDigitali);


//        traceService.trace("Ricalcolo", this.getClass().getName(), "updateConfigurazione", "Aggiornata configurazione", Constants.INFO);
    }
    
    @Override
    public List<Configuration> getConfiguration(String domain, String key) {
    	List<Configuration> configurations = configurationDAO.getConfiguration(domain, key);
		return configurations;
	}
    
    @Override
    public Configuration getConfiguration(long id) {
    	Configuration configuration = configurationDAO.getConfiguration(id);
		return configuration;
	}
    
	@Override
	public List<Configuration> getConfigurationHistory(String domain, String key, boolean settings) {
		List<Configuration> history = configurationDAO.getConfigurationHistory(domain, key);
		if (!settings) {
			List<Setting> emptySetting = new ArrayList<Setting>();
			List<Configuration> emptyConfiguration = new ArrayList<Configuration>();
			for (Configuration configuration : history) {
				configuration.setSettings(emptySetting);
				configuration.setConfigurations(emptyConfiguration);
			}
		} else {
			for (Configuration configuration : history) {
				configuration.getSettings();
			}
		}
		return history;
	}
    
    @Override
    public Configuration saveConfiguration(Configuration configuration) {
    	int errors = configurationValidatorService.validate(configuration);
		if (errors > 0)
			throw new IllegalArgumentException("Validation Error");
    	Configuration saved = configurationDAO.saveConfiguration(configuration);
		return saved;
	}

	@Override
	public void delete(Configuration configuration) {
		configurationDAO.deleteConfiguration(configuration);
		
		
	}    

}
