package com.alkemytech.sophia.codman.performing.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class UtilizzazioneDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer idUtilizzazione;
	private String autori;
	private String codiceOpera;
	private String combinazioneCodifica;
	private String compositore;
	private Date dataIns;
	private Date dataPrimaCodifica;
	private BigInteger durata;
	private String durataInferiore30sec;
	private String flagCedolaEsclusa;
	private String flagCodifica;
	private String flagMagNonConcessa;
	private String flagPubblicoDominio;
	private BigInteger idProgrammaMusicale;
	private String interpreti;
	private BigDecimal nettoMaturato;
	private BigInteger numeroProgrammaMusicale;
	private BigDecimal puntiOriginali;
	private BigDecimal puntiRicalcolati;
	private String tipoAccertamento;
	private String titoloComposizione;
	private CombanaDTO combanaDTO;

	public UtilizzazioneDTO() {
	}

	public Integer getIdUtilizzazione() {
		return this.idUtilizzazione;
	}

	public void setIdUtilizzazione(Integer idUtilizzazione) {
		this.idUtilizzazione = idUtilizzazione;
	}

	public String getAutori() {
		return this.autori;
	}

	public void setAutori(String autori) {
		this.autori = autori;
	}

	public String getCodiceOpera() {
		return this.codiceOpera;
	}

	public void setCodiceOpera(String codiceOpera) {
		this.codiceOpera = codiceOpera;
	}

	public String getCombinazioneCodifica() {
		return this.combinazioneCodifica;
	}

	public void setCombinazioneCodifica(String combinazioneCodifica) {
		this.combinazioneCodifica = combinazioneCodifica;
	}

	public String getCompositore() {
		return this.compositore;
	}

	public void setCompositore(String compositore) {
		this.compositore = compositore;
	}

	public Date getDataIns() {
		return this.dataIns;
	}

	public void setDataIns(Date dataIns) {
		this.dataIns = dataIns;
	}

	public Date getDataPrimaCodifica() {
		return this.dataPrimaCodifica;
	}

	public void setDataPrimaCodifica(Date dataPrimaCodifica) {
		this.dataPrimaCodifica = dataPrimaCodifica;
	}

	public BigInteger getDurata() {
		return this.durata;
	}

	public void setDurata(BigInteger durata) {
		this.durata = durata;
	}

	public String getDurataInferiore30sec() {
		return this.durataInferiore30sec;
	}

	public void setDurataInferiore30sec(String durataInferiore30sec) {
		this.durataInferiore30sec = durataInferiore30sec;
	}

	public String getFlagCedolaEsclusa() {
		return this.flagCedolaEsclusa;
	}

	public void setFlagCedolaEsclusa(String flagCedolaEsclusa) {
		this.flagCedolaEsclusa = flagCedolaEsclusa;
	}

	public String getFlagCodifica() {
		return this.flagCodifica;
	}

	public void setFlagCodifica(String flagCodifica) {
		this.flagCodifica = flagCodifica;
	}

	public String getFlagMagNonConcessa() {
		return this.flagMagNonConcessa;
	}

	public void setFlagMagNonConcessa(String flagMagNonConcessa) {
		this.flagMagNonConcessa = flagMagNonConcessa;
	}

	public String getFlagPubblicoDominio() {
		return this.flagPubblicoDominio;
	}

	public void setFlagPubblicoDominio(String flagPubblicoDominio) {
		this.flagPubblicoDominio = flagPubblicoDominio;
	}

	public BigInteger getIdProgrammaMusicale() {
		return this.idProgrammaMusicale;
	}

	public void setIdProgrammaMusicale(BigInteger idProgrammaMusicale) {
		this.idProgrammaMusicale = idProgrammaMusicale;
	}

	public String getInterpreti() {
		return this.interpreti;
	}

	public void setInterpreti(String interpreti) {
		this.interpreti = interpreti;
	}

	public BigDecimal getNettoMaturato() {
		return this.nettoMaturato;
	}

	public void setNettoMaturato(BigDecimal nettoMaturato) {
		this.nettoMaturato = nettoMaturato;
	}

	public BigInteger getNumeroProgrammaMusicale() {
		return this.numeroProgrammaMusicale;
	}

	public void setNumeroProgrammaMusicale(BigInteger numeroProgrammaMusicale) {
		this.numeroProgrammaMusicale = numeroProgrammaMusicale;
	}

	public BigDecimal getPuntiOriginali() {
		return this.puntiOriginali;
	}

	public void setPuntiOriginali(BigDecimal puntiOriginali) {
		this.puntiOriginali = puntiOriginali;
	}

	public BigDecimal getPuntiRicalcolati() {
		return this.puntiRicalcolati;
	}

	public void setPuntiRicalcolati(BigDecimal puntiRicalcolati) {
		this.puntiRicalcolati = puntiRicalcolati;
	}

	public String getTipoAccertamento() {
		return this.tipoAccertamento;
	}

	public void setTipoAccertamento(String tipoAccertamento) {
		this.tipoAccertamento = tipoAccertamento;
	}

	public String getTitoloComposizione() {
		return this.titoloComposizione;
	}

	public void setTitoloComposizione(String titoloComposizione) {
		this.titoloComposizione = titoloComposizione;
	}

	public CombanaDTO getCombanaDTO() {
		return this.combanaDTO;
	}

	public void setCombanaDTO(CombanaDTO combanaDTO) {
		this.combanaDTO = combanaDTO;
	}

}