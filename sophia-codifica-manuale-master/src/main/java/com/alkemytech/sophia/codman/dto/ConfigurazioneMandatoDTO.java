package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.codman.entity.AnagCountry;
import com.alkemytech.sophia.codman.entity.AnagDsp;

@XmlRootElement 
public class ConfigurazioneMandatoDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer id;
	
	private MandatoDTO mandato;
		
	private AnagDsp dsp;
	
	private UtilizationTypeDTO utilization;
	
	private AnagCountry country;
	
	private CommercialOffersConfigDTO offer;
	
	private Date validaDa;
	
	private Date validaA;

	public AnagDsp getDsp() {
		return dsp;
	}

	public void setDsp(AnagDsp dsp) {
		this.dsp = dsp;
	}

	public UtilizationTypeDTO getUtilization() {
		return utilization;
	}

	public void setUtilization(UtilizationTypeDTO utilization) {
		this.utilization = utilization;
	}

	public AnagCountry getCountry() {
		return country;
	}

	public void setCountry(AnagCountry country) {
		this.country = country;
	}

	public CommercialOffersConfigDTO getOffer() {
		return offer;
	}

	public void setOffer(CommercialOffersConfigDTO offer) {
		this.offer = offer;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public MandatoDTO getMandato() {
		return mandato;
	}

	public void setMandato(MandatoDTO mandato) {
		this.mandato = mandato;
	}

	public Date getValidaDa() {
		return validaDa;
	}

	public void setValidaDa(Date validaDa) {
		this.validaDa = validaDa;
	}

	public Date getValidaA() {
		return validaA;
	}

	public void setValidaA(Date validaA) {
		this.validaA = validaA;
	}

}