package com.alkemytech.sophia.codman.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.codman.entity.performing.security.ReportPage;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PerformingFlussoGuidaResponseDTO {

	private ReportPage reportPage;
	private String downloadPath;
	private Integer page;
	
	
	
	public PerformingFlussoGuidaResponseDTO() {
		super();
	}



	public PerformingFlussoGuidaResponseDTO(ReportPage reportPage, String downloadPath, Integer page) {
		super();
		this.reportPage = reportPage;
		this.downloadPath = downloadPath;
		this.page = page;
	}



	public ReportPage getReportPage() {
		return reportPage;
	}



	public void setReportPage(ReportPage reportPage) {
		this.reportPage = reportPage;
	}



	public String getDownloadPath() {
		return downloadPath;
	}



	public void setDownloadPath(String downloadPath) {
		this.downloadPath = downloadPath;
	}



	public Integer getPage() {
		return page;
	}



	public void setPage(Integer page) {
		this.page = page;
	}

	
}
