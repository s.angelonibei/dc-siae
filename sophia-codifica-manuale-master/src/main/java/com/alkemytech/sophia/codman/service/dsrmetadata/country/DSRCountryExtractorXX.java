package com.alkemytech.sophia.codman.service.dsrmetadata.country;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.google.inject.Provider;

public class DSRCountryExtractorXX implements DsrCountryExtractor{

	private final Provider<EntityManager> provider;
	private String regex = "[A-Z]{2}" ;
	private String code = "XX";
	private String description = "XX" ;
	
	public DSRCountryExtractorXX(Provider<EntityManager> provider) {
		super();
		this.provider = provider;
	}
	
	@Override
	public String extractDsrCountryCode(String country) {
		EntityManager entityManager = null;
		String result;
		try {
			entityManager = provider.get();
			final Query q = entityManager.createQuery("select x.idCountry from AnagCountry x where x.code = :country");
			q.setParameter("country", country);
			result = (String) q.getSingleResult();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String getRegex() {
		return regex;
	}

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public String getDescription() {
		return description;
	}

}
