package com.alkemytech.sophia.codman.entity.performing.dao;

import com.alkemytech.sophia.codman.dto.PerfEventiPagatiDTO;
import com.alkemytech.sophia.codman.dto.PerfUtilizzazioniDTO;
import com.alkemytech.sophia.codman.dto.ProgrammaMusicaleDTO;
import com.alkemytech.sophia.codman.dto.performing.AggiMcDTO;
import com.alkemytech.sophia.codman.dto.performing.FatturaDetailDTO;
import com.alkemytech.sophia.codman.dto.performing.PerfCodificaDTO;
import com.alkemytech.sophia.codman.dto.performing.RiconciliazioneImportiDTO;
import com.alkemytech.sophia.codman.entity.performing.*;
import com.alkemytech.sophia.codman.entity.performing.security.AggregatoVoce;
import com.alkemytech.sophia.codman.entity.performing.security.ReportPage;

import java.sql.Connection;
import java.util.Date;
import java.util.List;

/**
 * Created by idilello on 7/11/16.
 */
public interface IProgrammaMusicaleDAO {

    void insertMovimentazioneLogisticaPM(MovimentazioneLogisticaPM movimentazioneLogisticaPM);

    List<ProgrammaMusicale> getProgrammaMusicale(Long numeroProgrammaMusicale);

    void  updatePMRientrati(ProgrammaMusicale programmaMusicale);

    MovimentazioneLogisticaPM getUltimaMovimentazionePM(Long idProgrammaMusicale);

    long getCountTracciamentoPM(String dataIniziale, String dataFinale, Long numeroPM, String tipoDocumento, String seprag, String idEvento,
                                String fattura, String reversale, String voceIncasso, String permesso, String locale, String codiceBALocale,
                                String codiceSAPOrganizzatore, String organizzatore, String titoloOpera, String dataInizioEvento, String dataFineEvento,
                                String direttoreEsecuzione, String supporto, String tipoProgrammi, String gruppoPrincipale);

    List<ProgrammaMusicale> getTracciamentoPM(String dataIniziale, String dataFinale, Long numeroPM, String tipoDocumento, String seprag, String idEvento,
                                              String fattura, String reversale, String voceIncasso, String permesso, String locale, String codiceBALocale,
                                              String codiceSAPOrganizzatore, String organizzatore, String titoloOpera, String dataInizioEvento, String dataFineEvento,
                                              String direttoreEsecuzione, String supporto, String tipoProgrammi, String gruppoPrincipale, String order, Integer page);

    List<ProgrammaMusicale> getAllTracciamentoPM(String dataIniziale, String dataFinale, Long numeroPM, String tipoDocumento, String seprag, String idEvento,
                                                 String fattura, String reversale, String voceIncasso, String permesso, String locale, String codiceBALocale,
                                                 String codiceSAPOrganizzatore, String organizzatore, String titoloOpera, String dataInizioEvento, String dataFineEvento,
                                                 String direttoreEsecuzione, String supporto, String tipoProgrammi, String gruppoPrincipale, String order);
    
    long getCountListaEventiPagati(String inizioPeriofoContabile, String finePeriofoContabile, PerfEventiPagatiDTO perfEventiPagatiDTO, String organizzatore,
                                   String direttoreEsecuzione, String permesso, String numeroPM, String titoloOpera, String codiceSAP,
                                   String dataInizioEvento, String dataFineEvento, String fatturaValidata);
    
    List<EventiPagati> getListaEventiPagati(String inizioPeriofoContabile, String finePeriofoContabile, PerfEventiPagatiDTO perfEventiPagatiDTO, String organizzatore,
                                            String direttoreEsecuzione, String permesso, String numeroPM, String titoloOpera, String codiceSAP,
                                            String dataInizioEvento, String dataFineEvento, String fatturaValidata, String order);
    
    List<EventiPagati> getListaEventiPagati(String inizioPeriodoContabile, String finePeriodoContabile, PerfEventiPagatiDTO perfEventiPagatiDTO,
                                            String organizzatore, String direttoreEsecuzione, String permesso, String numeroPM, String titoloOpera, String codiceSAP,
                                            String dataInizioEvento, String dataFineEvento, String fatturaValidata, String order, boolean allList);
    
    List<EventiPagati> getEventiByFattura(String numeroFattura);
    
    List<EventiPagati> getEvento(Long eventoId);
    
//    public List<EventiPagati> getEvento(List<Manifestazione> eventi);

    List<MovimentoContabile> getMovimentazioniPerEvento(Long eventoId);

    List<ImportoRicalcolato> getImportiPMAssegnatiPerEvento(Long idEvento);

    List<ImportoRicalcolato> getImportiTotaliPerEvento(Long idEvento);

    List<ImportoRicalcolato> getImportiRicalcolatiPerMovimentazione(Long idMovimento158);

    List<String> getTipoProgrammi();
    
//    public EventiPagati getEventiPagatiByIdEvento(Long idEvento);

    long getCountListaMovimentazioni(Long inizioPeriodoContabile, Long finePeriodoContabile, Long evento, String fattura, Long reversale, String voceIncasso,
                                     Date dataInizioEvento, Date dataFineEvento, String seprag, String tipoDocumento, String locale, String sapOrganizzatore,
                                     String tipoSupporto, String tipoProgramma, String direttoreEsecuzione, String titoloOpera, String numeroPM, String permesso,
                                     Integer page, String codiceBALocale, String organizzatore);

    List<MovimentoContabile> getListaMovimentazioni(Long inizioPeriodoContabile, Long finePeriodoContabile, Long evento, String fattura, Long reversale, String voceIncasso,
                                                    Date dataInizioEvento, Date dataFineEvento, String seprag, String tipoDocumento, String locale, String sapOrganizzatore,
                                                    String tipoSupporto, String tipoProgramma, String direttoreEsecuzione, String titoloOpera, String numeroPM, String permesso,
                                                    Integer page, String codiceBALocale, String organizzatore, String order);
    
    List<MovimentoContabile> getListaMovimentazioni(Long inizioPeriodoContabile, Long finePeriodoContabile, Long evento, String fattura, Long reversale, String voceIncasso,
                                                    Date dataInizioEvento, Date dataFineEvento, String seprag, String tipoDocumento, String locale, String sapOrganizzatore,
                                                    String tipoSupporto, String tipoProgramma, String direttoreEsecuzione, String titoloOpera, String numeroPM, String permesso,
                                                    Integer page, String codiceBALocale, String organizzatore, String order, boolean allList);

    List<AggregatoVoce> getListaDocumentiEventi(Long inizioPeriodoContabile, Long finePeriodoContabile, String idPuntoTerritoriale);

    List<AggregatoVoce> getListaEventi(Long inizioPeriodoContabile, Long finePeriodoContabile, String idPuntoTerritoriale);

    MovimentoContabile getMovimentoContabile(Long idMovimento);

    List<MovimentoContabile> getMovimentiContabile(Long idProgrammaMusicale);

    ProgrammaMusicale getProgrammaMusicaleById(Long idProgrammaMusicale);
    
    Utilizzazione getOperaById(Long idOpera);
    
    void updateProgrammaMusicaleOnDetail(Long idPM, String flagCampionamento, String flagGruppo);
    
    void updateOperaOnDetail(Double durata, String durataMeno30Sec, String flagPD, Long idOpera);

    Manifestazione getManifestazione(Long idEvento);

    List<Utilizzazione> getUtilizzazioni(Long idProgrammaMusicale);

    DirettoreEsecuzione getDirettoreEsecuzione(Long idProgrammaMusicale);

    Cartella getCartellaById(Long idCartella);

    List<Manifestazione> getManifestazioni(List<MovimentoContabile> listaMovimenti);

    ReportPage getListaAggregatiSiada(Long inizioPeriodoContabile, Long finePeriodoContabile, String voceIncasso, String tipoSupporto, String tipoProgramma, Long numeroPM, Integer page);

    List<AggregatoVoce>  getImportoNettiPMCorrenti(Long inizioPeriodoContabile, Long finePeriodoContabile, String idPuntoTerritoriale);

    List<AggregatoVoce> getImportoProgrammatoCorrenti(Long inizioPeriodoContabile, Long finePeriodoContabile, String idPuntoTerritoriale);

    List<AggregatoVoce> getNumeroPMCorrenti(Long inizioPeriodoContabile, Long finePeriodoContabile, String idPuntoTerritoriale);

    List<AggregatoVoce> getNumeroCedoleCorrenti(Long inizioPeriodoContabile, Long finePeriodoContabile, String idPuntoTerritoriale);

    List<AggregatoVoce>  getImportoNettiPMArretrati(Long inizioPeriodoContabile, Long finePeriodoContabile, String idPuntoTerritoriale);

    List<AggregatoVoce> getNumeroPMArretrati(Long inizioPeriodoContabile, Long finePeriodoContabile, String idPuntoTerritoriale);

    List<AggregatoVoce> getNumeroCedoleArretrati(Long inizioPeriodoContabile, Long finePeriodoContabile, String idPuntoTerritoriale);

    Double getImportoPMSUNAggregato(Long idProgrammaMusicale);

    Double getImportoPMSIADAAggregato(Long idProgrammaMusicale);

    List<ImportoRicalcolato> getImportiPMRicalcolati(Long idProgrammaMusicale);

    Long getMovimentiContabili(Long periodoContabileInizio, Long periodoContabileFine, String supportoPm);

    Integer updateContabilitaOriginale(Integer nuovaContabilita, List<Object> listaMovimenti);
    
    long getCountListaFatturePerEventiPagati(EventiPagati eventoPagato, String fatturaValidata);
    
    List<EventiPagati> getListaFatturePerEventiPagati(EventiPagati eventoPagato, String fatturaValidata, String order, Integer page);
    
    List<MovimentoContabile> getListaFatturePerMovimentiContabili(MovimentoContabile movimentiContabili);

    List<EventiPagati> getDettaglioFattureOnEvento(String numeroFattura);

    List<MovimentoContabile> getDettaglioFattureOnMovimento(String numeroFattura);
    
    Double getImportoTotaleOfEvento(Double idEvento);
    
    Integer addUtilizzazione(Utilizzazione utilizzazione);
    
    List<MovimentoContabile> getListaPmDisponibili(String numeroPm);
    

    void sgancioPm(List<MovimentoContabile> movimenti);
    
    void sgancioPm(Long idMovimentoContabile);
    
    void aggancioPm(String numeroPm, String voceIncasso, Long idEvento);
    
    List<MovimentoContabile> getMovimentiByPmAndVoceIncasso(Long idPm, String voceIncasso, Long idEvento);

    Integer getVociIncassoForIdPm(Long idPm);
    
	void removeOpera(Long idUtilizzazione);
	
	void addPm(ProgrammaMusicale programmaMusicale);
	
	void updatePm(ProgrammaMusicaleDTO programmaMusicaleDto);
	
	void updateOpera(PerfUtilizzazioniDTO perfUtilizzazioniAddDTO);
	
	TrattamentoPM getTrattamentoPM(String voceIncasso);
	
	void updatePMAttesiInEventiPagati(Long pmPrevisti, Long pmPrevistiSpalla, Long idEvento);
	
	void updatePMAttesiInMovimenti(Integer pmPrevisti, Integer pmPrevistiSpalla, Long idMovimento);

	Integer controlloPmRientrati(Long idEvento);

	void addOpera(Utilizzazione utilizzazione);
	
	EventiPagati getEventoPagato(Long idEventoPagato);
	
	void updateFlagCampionamentoOnMovimentiAssociati(Long idMovimento, String flagGruppo);
	
	List<Long> retrieveIdMovimentiForProgrammiMusicali(Long idPm);
	
    List<String> getVociIncassoPerIdEvento(Long idEvento);

    void removeMovimentiAssociationWithPm(Long idMovimento);

    void updateMostRecentMovimentoOnRiaggancio(Long movimentoMaggiore, Long idPm, EventiPagati eventoSuVoce);

    EventiPagati getEventoByVoceIncasso(String voceIncasso, Long idEvento);
    
    void addConfigurazioneAggioMc(AggiMc aggio);
    
    List<AggiMc> getListaAggiAttivi(String inizioPeriodoValidazione, String finePeriodoValidazione, String codiceRegola, String order);
    
    AggiMc getAggioAttivo(Long id);

    AggiMc aggioIsInCorso();
    
    List<String> getValidazioneDate(String inizioPeriodoValidazione, String finePeriodoValidazione);
    
    List<String> getValidazioneDateOnUpdate(String inizioPeriodoValidazione, String finePeriodoValidazione, String codiceRegola);
    
    List<String> getValidazioneCodiceRegola(String codiceRegola);

    List<AggiMc> getStoricoAggio(String codiceRegola);

    void updateAggio(AggiMcDTO aggio);
    
    void updateDataAggio(Date inizioPeriodoValidazione, Date finePeriodoValidazione, Long id, String utenteUltimaModifica);
    
    void updateParametriAggio(AggiMcDTO aggio);

    AggiMc getAggioMcById(Long id);
    
    AggiMc getDataInizioConfigurazioneAggioMax();

    List<String> getCodiciRegolaAfterDate(String inizioPeriodoValidazione);

    ReportPage getRiconciliazioneImportiList(String voceIncasso, String agenzia, String sede, String seprag, String singolaFattura,
                                             String inizioPeriodoContabile, String finePeriodoContabile, Date inizioPeriodoFattura, Date finePeriodoFattura, String order, Integer page, String fatturaValidata, String megaConcerto);


    TotaleRiconciliazioneImporti getCountRiconciliazioneImportiList(String voceIncasso, String agenzia, String sede, String seprag, String singolaFattura,
                                            String inizioPeriodoContabile, String finePeriodoContabile, Date inizioPeriodoFattura, Date finePeriodoFattura, String fatturaValidata, String megaConcerto);
    
    void updateFlagMegaConcert(FatturaDetailDTO fatturaDetailDTO);

    NDMVoceFattura getNdmVoceFattura(String voceIncasso, String numeroFattura);
    

    List<RiconciliazioneImportiDTO> getRiconciliazioneFattura(String numeroFattura);
    
    Connection getConfiguration();
    
    List<Long> getEventiToEditOnAggio(AggiMcDTO aggio);
    
    AggiMc getAggioByDate(Date dataFattura);

    PerfCodificaDTO getCodifica(Long idCombana, String utente);
    
    
    public void riconciliaImporti(RiconciliazioneImportiDTO riconciliazioneImporti, String username);
    
    public void riconciliaVociIncasso(RiconciliazioneImportiDTO riconciliazioneImporti, String username);
    
    public void ripristinaRiconciliazione(RiconciliazioneImportiDTO riconciliazioneImporti, String username);
    
}