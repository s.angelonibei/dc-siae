package com.alkemytech.sophia.codman.entity;

import com.alkemytech.sophia.codman.persistence.AbstractEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.google.gson.annotations.Expose;

import javax.persistence.*;
import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement
@Entity(name="AnagCCIDConfigParam")
@Table(name="ANAG_CCID_CONFIG_PARAM")
public class AnagCCIDConfigParam implements Serializable {

	private static final long serialVersionUID = 1L;

	@Expose
	@Id
	@Column(name="ID_ANAG_CCID_CONFIG_PARAM", nullable=false)
	private Long idAnagCcidConfigParam;


	@ManyToOne
	@JoinColumn(name="ID_CCID_CONFIG")
	private CCIDConfig ccidConfig;



	@ManyToOne
	@JoinColumn(name="ID_CCID_PARAM")
	private AnagCCIDParam anagCCIDParams;

	@Expose
	@Column(name="VALUE")
	private String value;


	public Long getIdAnagCcidConfigParam() {
		return idAnagCcidConfigParam;
	}

	public void setIdAnagCcidConfigParam(Long idAnagCcidConfigParam) {
		this.idAnagCcidConfigParam = idAnagCcidConfigParam;
	}

	public CCIDConfig getCcidConfig() {
		return ccidConfig;
	}

	public void setCcidConfig(CCIDConfig ccidConfig) {
		this.ccidConfig = ccidConfig;
	}

	public AnagCCIDParam getAnagCCIDParams() {
		return anagCCIDParams;
	}

	public void setAnagCCIDParams(AnagCCIDParam anagCCIDParam) {
		this.anagCCIDParams = anagCCIDParam;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
	