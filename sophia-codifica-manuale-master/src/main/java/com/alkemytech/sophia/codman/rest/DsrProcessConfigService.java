package com.alkemytech.sophia.codman.rest;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.codman.dto.AnagDsrProcessDTO;
import com.alkemytech.sophia.codman.dto.DsrProcessConfigDTO;
import com.alkemytech.sophia.codman.dto.DsrProcessDTO;
import com.alkemytech.sophia.codman.dto.ManagedProcessDTO;
import com.alkemytech.sophia.codman.entity.AnagDsrProcess;
import com.alkemytech.sophia.codman.entity.DsrProcessConfig;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

@Singleton
@Path("dsrConfig")
public class DsrProcessConfigService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Provider<EntityManager> provider;
	private final Gson gson;

	@Inject
	protected DsrProcessConfigService(@McmdbDataSource Provider<EntityManager> provider, Gson gson) {
		super();
		this.provider = provider;
		this.gson = gson;
	}

	@GET
	@Path("allConfig")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response findAllConfig() {
		EntityManager em = null;
		List<DsrProcessConfig> result;
		try {
			em = provider.get();
			final Query q = em.createQuery("select x from DsrProcessConfig x order by x.priority asc");
			result = (List<DsrProcessConfig>) q.getResultList();
			return Response.ok(gson.toJson(result)).build();
		} catch (Exception e) {
			logger.error("findAllConfig", e);
		}
		return Response.status(500).build();
	}

	@GET
	@Path("allConfigDetail")
	@Produces("application/json")
	@SuppressWarnings({ "unchecked" })
	public Response findAllConfigDetail(@DefaultValue("0") @QueryParam("first") int first,
			@DefaultValue("50") @QueryParam("last") int last,
			@DefaultValue("") @QueryParam("dsp") String dsp,
			@DefaultValue("") @QueryParam("country") String country,
			@DefaultValue("") @QueryParam("utilization") String utilization
			) {
		EntityManager em = null;
		List<Object[]> result;
		try {
			em = provider.get();
			final String sql = "select process_config.ID_DSR_PROCESS_CONFIG, process_config.PRIORITY, process_config.IDDSP, coalesce(dsp.NAME, '*') as DSP_NAME, "
					+ " process_config.IDUTILIZATION, coalesce(utilization.NAME, '*') as UTILIZATION_NAME, process_config.IDCOUNTRY, coalesce( country.NAME, '*') as COUNTRY_NAME, process_config.ID_DSR_PROCESS, process_config.PROCESS_NAME, "
					+ " process_config.PROCESS_DESCRIPTION, process_config.PROCESS_CONFIGURATION from "
					+ "  (select c.ID_DSR_PROCESS_CONFIG as ID_DSR_PROCESS_CONFIG, 	c.PRIORITY AS PRIORITY, 	c.DSP as IDDSP, "
					+ "		c.UTILIZATION_TYPE AS IDUTILIZATION , 	c.COUNTRY as IDCOUNTRY, p.ID_DSR_PROCESS as ID_DSR_PROCESS, "
					+ "		p.NAME as PROCESS_NAME,	p.DESCRIPTION as PROCESS_DESCRIPTION , p.CONFIGURATION as PROCESS_CONFIGURATION   from "
					+ "		DSR_PROCESS_CONFIG c, " + "     ANAG_DSR_PROCESS p " + "where "
					+ "		c.ID_DSR_PROCESS = p.ID_DSR_PROCESS " 
					+ (!dsp.isEmpty() && !dsp.equals("*") ? "	and (c.DSP = ? or c.DSP ='*') " : "") 
					+ (!utilization.isEmpty() && !utilization.equals("*") ? "	and (c.UTILIZATION_TYPE = ? or c.UTILIZATION_TYPE ='*') " : "") 
					+ (!country.isEmpty() && !country.equals("*") ? "	and (c.COUNTRY = ? or c.COUNTRY ='*') " : "")
					+ (!dsp.isEmpty() && !dsp.equals("*") && !utilization.isEmpty() && !utilization.equals("*") ? " and 'VALID_UTILIZATION' = (SELECT distinct 'VALID_UTILIZATION' FROM COMMERCIAL_OFFERS o WHERE o.ID_UTIILIZATION_TYPE = ? and o.IDDSP = ? ) " : "")
					+ (!dsp.isEmpty() && !dsp.equals("*") && !utilization.isEmpty() && utilization.equals("*") ? " and 'VALID_UTILIZATION' = (SELECT distinct 'VALID_UTILIZATION' FROM COMMERCIAL_OFFERS o WHERE (o.ID_UTIILIZATION_TYPE = c.UTILIZATION_TYPE and o.IDDSP = ?) or c.UTILIZATION_TYPE='*' ) " : "")
					+ " ) process_config "
					+ " left join  (select * from ANAG_DSP) dsp on  process_config.IDDSP = dsp.IDDSP "
                    + " left join (select * from ANAG_COUNTRY ) country on  process_config.IDCOUNTRY = country.ID_COUNTRY "        
                    + " left join (select * from ANAG_UTILIZATION_TYPE ) utilization on  process_config.IDUTILIZATION = utilization.ID_UTILIZATION_TYPE "
					+ " order by process_config.PRIORITY desc";
			final Query q = em.createNativeQuery(sql).setFirstResult(first).setMaxResults(1 + last - first);

			int i= 1;
			if(!dsp.isEmpty() && !dsp.equals("*")){
				q.setParameter(i++, dsp);
			}
			if(!utilization.isEmpty() && !utilization.equals("*") ){
				q.setParameter(i++, utilization);
			}
			if(!country.isEmpty() && !country.equals("*")){
				q.setParameter(i++, country);
			}
			
			if(!dsp.isEmpty() && !dsp.equals("*") && !utilization.isEmpty() && !utilization.equals("*")){
				q.setParameter(i++, utilization);
				q.setParameter(i++, dsp);
				
			}
			
			if(!dsp.isEmpty() && !dsp.equals("*") && !utilization.isEmpty() && utilization.equals("*")){
				q.setParameter(i++, dsp);
				
			}
			
			result = (List<Object[]>) q.getResultList();
			List<DsrProcessConfigDTO> dtoList = new ArrayList<DsrProcessConfigDTO>();

			final PagedResult pagedResult = new PagedResult();
			if (null != result) {
				final int maxrows = last - first;
				final boolean hasNext = result.size() > maxrows;
				for (Object[] p : (hasNext ? result.subList(0, maxrows) : result)) {
					dtoList.add(new DsrProcessConfigDTO(p));
				}
				pagedResult.setRows(dtoList)
					.setMaxrows(maxrows)
					.setFirst(first)
					.setLast(hasNext ? last : first + result.size())
					.setHasNext(hasNext)
					.setHasPrev(first > 0);
			} else {
				pagedResult.setMaxrows(0)
					.setFirst(0)
					.setLast(0)
					.setHasNext(false)
					.setHasPrev(false);
			}
			return Response.ok(gson.toJson(pagedResult)).build();
	
		} catch (Exception e) {
			logger.error("findAllConfigDetail", e);
		}
		return Response.status(500).build();
	}

	@GET
	@Path("allProcess")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response findAllProcess() {
		EntityManager em = null;
		List<AnagDsrProcess> result;
		try {
			em = provider.get();
			final Query q = em.createQuery("select x from AnagDsrProcess x order by x.name asc");
			result = (List<AnagDsrProcess>) q.getResultList();
			return Response.ok(gson.toJson(result)).build();
		} catch (Exception e) {
			logger.error("findAllProcess", e);
		}
		return Response.status(500).build();
	}

	@GET
	@Path("getProcess/{id}")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response findProcess(@PathParam("id") String id) {
		EntityManager em = null;
		List<AnagDsrProcess> result;
		try {
			em = provider.get();
			final Query q = em
					.createQuery("select x from AnagDsrProcess x where x.idDsrProcess = :id order by x.name asc");
			q.setParameter("id", id);
			result = (List<AnagDsrProcess>) q.getResultList();
			return Response.ok(gson.toJson(result)).build();
		} catch (Exception e) {
			logger.error("findProcess", e);
		}
		return Response.status(500).build();
	}

	@GET
	@Path("allManagedProcesses")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response findAllManagedProcess(
			@DefaultValue("0") @QueryParam("first") int first,
			@DefaultValue("50") @QueryParam("last") int last) {
		EntityManager em = null;
		List<AnagDsrProcess> result;
		try {
			em = provider.get();
			final Query q = em.createQuery("select x from AnagDsrProcess x order by x.name asc").setFirstResult(first) // offset
			         .setMaxResults(1 + last - first);
			result = (List<AnagDsrProcess>) q.getResultList();
			
			final PagedResult pagedResult = new PagedResult();
			if (null != result) {				
				final int maxrows = last - first;
				final boolean hasNext = result.size() > maxrows;
				List<ManagedProcessDTO> managedProcessList = new ArrayList<>();
				for(AnagDsrProcess process : (hasNext ? result.subList(0, maxrows) : result)){
					managedProcessList.add(new ManagedProcessDTO(process));
				}
				
				pagedResult.setRows(managedProcessList).setMaxrows(maxrows).setFirst(first)
						.setLast(hasNext ? last : first + result.size()).setHasNext(hasNext).setHasPrev(first > 0);
			} else {
				pagedResult.setMaxrows(0).setFirst(0).setLast(0).setHasNext(false).setHasPrev(false);
			}
			return Response.ok(gson.toJson(pagedResult)).build();
		} catch (Exception e) {
			logger.error("findAllManagedProcess", e);
		}
		return Response.status(500).build();
	
	}
	
	@POST
	@Path("allManagedProcesses")
	public Response addManagedProcess(AnagDsrProcessDTO config) {
		
		Status status = Status.INTERNAL_SERVER_ERROR;
		final EntityManager entityManager = provider.get();
		AnagDsrProcess dsrProcessEntity = new AnagDsrProcess();
		dsrProcessEntity.setIdDsrProcess(config.getIdDsrProcess());
		dsrProcessEntity.setName(config.getName());
		dsrProcessEntity.setConfiguration(config.getConfiguration());
		dsrProcessEntity.setDescription(config.getDescription());
		try {
			entityManager.getTransaction().begin();
			entityManager.persist(dsrProcessEntity);


			entityManager.getTransaction().commit();
			return Response.ok().build();
		} catch (Exception e) {
			logger.error("addManagedProcess", e);

			if (e.getCause() != null && e.getCause().getCause() != null
					&& e.getCause().getCause() instanceof MySQLIntegrityConstraintViolationException) {
				status = Status.CONFLICT;
			}

			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}

		}

		return Response.status(status).build();
	}
	
	@DELETE
	@Path("allManagedProcesses")
	public Response deleteManagedProcess(@QueryParam("idDsrProcess") String id_dsr_process) {
		final EntityManager entityManager = provider.get();
		Status status = Status.INTERNAL_SERVER_ERROR;
		try {
			entityManager.getTransaction().begin();
			final Query query = entityManager.createQuery("delete from AnagDsrProcess x where x.idDsrProcess = :id_dsr_process");
			query.setParameter("id_dsr_process", id_dsr_process);

			query.executeUpdate();
			entityManager.getTransaction().commit();
			return Response.ok().build();
		} catch (Exception e) {
			logger.error("deleteManagedProcess", e);

			if (e.getCause() != null && e.getCause().getCause() != null
					&& e.getCause().getCause() instanceof MySQLIntegrityConstraintViolationException) {
				status = Status.CONFLICT;
			}
			
			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
		}
		return Response.status(status).build();
	}
	
	
	@PUT
	@Path("")
	public Response put(DsrProcessDTO config) {
		Status status = Status.INTERNAL_SERVER_ERROR;
		final EntityManager entityManager = provider.get();
		DsrProcessConfig dsrProcessConfig = new DsrProcessConfig();
				dsrProcessConfig.setIdDsrProcess(config.getIdDsrProcess());
				dsrProcessConfig.setDsp(config.getDsp());
				dsrProcessConfig.setCountry(config.getCountry());
				dsrProcessConfig.setUtilizationType(config.getUtilizationType());
				dsrProcessConfig.setPriority(config.getPriority());
		try {
			entityManager.getTransaction().begin();
			entityManager.persist(dsrProcessConfig);

			// TODO log user activity

			entityManager.getTransaction().commit();
			return Response.ok().build();
		} catch (Exception e) {
			logger.error("put", e);
			if (e.getCause() != null && e.getCause().getCause() != null
					&& e.getCause().getCause() instanceof MySQLIntegrityConstraintViolationException) {
				status = Status.CONFLICT;
			}
			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}

		}
		return Response.status(status).build();
	}

	@DELETE
	@Path("")
	public Response delete(DsrProcessDTO config) {
		final EntityManager entityManager = provider.get();
		try {
			entityManager.getTransaction().begin();
			final Query query = entityManager.createQuery("delete from DsrProcessConfig x where x.dsp = :dsp "
					+ "and x.utilizationType = :utilizationType  and x.country = :country");
			query.setParameter("dsp", config.getDsp());
			query.setParameter("utilizationType", config.getUtilizationType());
			query.setParameter("country", config.getCountry());

			query.executeUpdate();
			entityManager.getTransaction().commit();
			return Response.ok().build();
		} catch (Exception e) {
			logger.error("delete", e);
			entityManager.getTransaction().rollback();
		}
		return Response.status(500).build();
	}

	@POST
	@Path("")
	public Response update(DsrProcessDTO config) {
		final EntityManager entityManager = provider.get();
		try {
			entityManager.getTransaction().begin();

			final Query query = entityManager
					.createQuery("update DsrProcessConfig x set x.idDsrProcess= :idDsrProcess, "
							+ "x.priority = :priority where x.dsp = :dsp "
							+ "and x.utilizationType = :utilizationType  and x.country = :country");

			query.setParameter("dsp", config.getDsp());
			query.setParameter("utilizationType", config.getUtilizationType());
			query.setParameter("country", config.getCountry());
			query.setParameter("idDsrProcess", config.getIdDsrProcess());
			query.setParameter("priority", config.getPriority());

			query.executeUpdate();
			entityManager.getTransaction().commit();
			return Response.ok().build();
		} catch (Exception e) {
			logger.error("update", e);
			entityManager.getTransaction().rollback();
		}
		return Response.status(500).build();
	}

}
