package com.alkemytech.sophia.codman.rest;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.codman.blacklist.BlacklistValidator;
import com.alkemytech.sophia.codman.entity.Blacklist;
import com.alkemytech.sophia.codman.entity.SocietaTutela;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.alkemytech.sophia.common.s3.S3Service;
import com.amazonaws.services.s3.AmazonS3URI;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

@Singleton
@Path("blacklist")
public class BlacklistService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final String uploadDirectory;
	private final String s3Bucket;
	private final String s3KeyPrefix;
	private final Provider<EntityManager> provider;
	private final Gson gson;
	private final S3Service s3Service;
	private final BlacklistValidator blacklistValidator;
	private final MetadataService metadataService;
	
	@Inject
	protected BlacklistService(@Named("blacklist.upload.directory") String uploadDirectory,
			@Named("blacklist.s3.bucket") String s3Bucket,
			@Named("blacklist.s3.key_prefix") String s3KeyPrefix,
			@McmdbDataSource Provider<EntityManager> provider, 
			Gson gson, S3Service s3Service,
			BlacklistValidator blacklistValidator,
			MetadataService metadataService) {
		super();
		this.uploadDirectory = uploadDirectory;
		this.s3Bucket = s3Bucket;
		this.s3KeyPrefix = s3KeyPrefix;		
		this.provider = provider;
		this.gson = gson;
		this.s3Service = s3Service;
		this.blacklistValidator = blacklistValidator;
		this.metadataService = metadataService;
	}

	@GET
	@Produces("application/json")
	public Response findAll() {
		final EntityManager entityManager = provider.get();
		final List<Blacklist> result = entityManager
				.createQuery("select x from Blacklist x", Blacklist.class)
				.getResultList();
		return Response.ok(gson.toJson(result)).build();
	}
	
	@GET
	@Path("{id}")
	@Produces("application/json")
	public Response findById(@PathParam("id") Long id) {
		final EntityManager entityManager = provider.get();
		final Blacklist blacklist = entityManager
				.find(Blacklist.class, id);
		if (null == blacklist) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.ok(gson.toJson(blacklist)).build();
	}

	@GET
	@Path("download/{id}")
	@Produces("application/vnd.ms-excel")
	public Response downloadById(@PathParam("id") Long id) {
		final EntityManager entityManager = provider.get();
		final Blacklist blacklist = entityManager
				.find(Blacklist.class, id);
		if (null == blacklist) {
			return Response.status(Status.NOT_FOUND).build();
		}
		final AmazonS3URI s3Uri = new AmazonS3URI(blacklist.getXlsS3Url());
		final ByteArrayOutputStream out = new ByteArrayOutputStream();
		s3Service.download(s3Uri.getBucket(), s3Uri.getKey(), out);
		return Response.ok()
				.type("text/csv")
				.entity(out.toByteArray())
				.build();
	}

	@DELETE
	@Path("delete/{id}")
	@Produces("application/json")
	public Response delete(@PathParam("id") Long id) {
		logger.debug("delete: id {}", id);
		final EntityManager entityManager = provider.get();
		entityManager.getTransaction().begin();
		final Blacklist blacklist = entityManager.find(Blacklist.class, id);
		entityManager.remove(blacklist);
		entityManager.getTransaction().commit();
		final AmazonS3URI s3Uri = new AmazonS3URI(blacklist.getXlsS3Url());
		logger.debug("delete: s3Uri {}", s3Uri);
		s3Service.delete(s3Uri.getBucket(), s3Uri.getKey());
		return Response.ok(gson.toJson(blacklist)).build();
	}
	
	@GET
	@Path("search")
	@Produces("application/json")
	public Response search(@QueryParam("country") String country,
			@QueryParam("validFrom") String validFrom, 
			@QueryParam("xlsS3Url") String xlsS3Url,
			@QueryParam("codiceSocietaTutela") String codiceSocietaTutela,
			@DefaultValue("0") @QueryParam("first") int first,
			@DefaultValue("50") @QueryParam("last") int last) {
		final EntityManager entityManager = provider.get();
		try {
			String separator = " where ";
			final StringBuffer sql = new StringBuffer()
				.append("select * from BLACKLIST");
			if (!StringUtils.isEmpty(country)) {
				sql.append(separator).append("COUNTRY = ?");
				separator = " and ";
			}
			if (!StringUtils.isEmpty(validFrom)) {
				sql.append(separator).append("VALID_FROM = ?");
				separator = " and ";
			}
			if (!StringUtils.isEmpty(xlsS3Url)) {
				sql.append(separator).append("XLS_S3_URL like ?");
				separator = " and ";
			}
			if (!StringUtils.isEmpty(codiceSocietaTutela)) {
				sql.append(separator).append("CODICE_SOCIETA_TUTELA like ?");
				separator = " and ";
			}			
			sql.append(" limit ?, ?");
			int i = 1;
			final Query query = (Query) entityManager
				.createNativeQuery(sql.toString(), Blacklist.class);
			if (!StringUtils.isEmpty(country)) {
				query.setParameter(i ++, country);
			}
			if (!StringUtils.isEmpty(validFrom)) {
				query.setParameter(i ++, new SimpleDateFormat("yyyy-MM-dd").parse(validFrom));
			}
			if (!StringUtils.isEmpty(xlsS3Url)) {
				query.setParameter(i ++, "%" + xlsS3Url + "%");
			}
			if (!StringUtils.isEmpty(codiceSocietaTutela)) {
				query.setParameter(i ++, "%" + codiceSocietaTutela + "%");
			}			
			query.setParameter(i ++, first);
			query.setParameter(i ++, 1 + last - first);
			@SuppressWarnings("unchecked")
			final List<Blacklist> results =
				(List<Blacklist>) query.getResultList();
			final PagedResult result = new PagedResult();
			if (null != results && !results.isEmpty()) {
				final int maxrows = last - first;
				final boolean hasNext = results.size() > maxrows;
				result.setRows(!hasNext ? results : results.subList(0, maxrows))
					.setMaxrows(maxrows)
					.setFirst(first)
					.setLast(hasNext ? last : first + results.size())
					.setHasNext(hasNext)
					.setHasPrev(first > 0);
			} else {
				result.setMaxrows(0)
					.setFirst(0)
					.setLast(0)
					.setHasNext(false)
					.setHasPrev(false);
			}
			return Response.ok(gson.toJson(result)).build();
		} catch (Exception e) {
			logger.error("search", e);
		}
		return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR).build();
	}
	
	@POST
	@Path("upload")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces("application/json")
	public Response upload(@FormDataParam("country") String country,
			@FormDataParam("validFrom") String validFrom,
			@FormDataParam("codiceSocietaTutela") String codiceSocietaTutela,
	        @FormDataParam("file") InputStream uploadedInputStream,
	        @FormDataParam("file") FormDataContentDisposition fileDetail) {		
		final File file = new File(uploadDirectory, UUID.randomUUID().toString());
		logger.debug("upload: file {}", file);
		try {
			final Date validFromDate = new SimpleDateFormat("yyyy-MM-dd").parse(validFrom);
			logger.debug("upload: validFromDate {}", validFromDate);
			if (saveToFile(uploadedInputStream, file)) { // save to temporary local file
				final List<String> errors = blacklistValidator.validate(file); // validate file
				logger.debug("upload: errors {}", errors);
				if (null == errors || errors.isEmpty()) {
					final String xlsS3Url = "s3://" + s3Bucket + "/" + s3KeyPrefix +
							"/" + codiceSocietaTutela +
							"/" + country + 
							"/" + new SimpleDateFormat("yyyyMMdd").format(validFromDate) + 
//							"/" + UUID.randomUUID() + // avoid overwrites
							"/" + fileDetail.getFileName();
					logger.debug("upload: xlsS3Url {}", xlsS3Url);
					final AmazonS3URI s3Uri = new AmazonS3URI(xlsS3Url);
					logger.debug("upload: s3Uri {}", s3Uri);
					if (s3Service.upload(s3Uri.getBucket(), s3Uri.getKey(), file)) { // upload to s3
						// save to database
						final Blacklist blacklist = new Blacklist();
						blacklist.setCountry(Strings.isNullOrEmpty(country) ? "ITA" : country);
						blacklist.setValidFrom(validFromDate);
						blacklist.setXlsS3Url(xlsS3Url);
						final EntityManager entityManager = provider.get();		
						SocietaTutela societaTutela = entityManager.createNamedQuery("SocietaTutela.findByCodice", SocietaTutela.class)
		                .setParameter("codice", codiceSocietaTutela)
		                .getSingleResult();
						blacklist.setSocietaTutela(societaTutela);
						entityManager.getTransaction().begin();
						entityManager.persist(blacklist);
						entityManager.getTransaction().commit();
						return Response.ok(gson.toJson(blacklist)).build();
					}
				} else {
					return Response.status(Status.BAD_REQUEST)
							.entity(gson.toJson(errors)).build();
				}
			}
		} catch (Exception e) {
			logger.error("upload", e);
		} finally {
			file.delete();
		}
		return Response.status(Status.INTERNAL_SERVER_ERROR).build();
	}
	
	private boolean saveToFile(InputStream in, File file) {
        OutputStream out = null;
	    try {
	        int count = 0;
	        final byte[] bytes = new byte[1024];
	        out = new FileOutputStream(file);
	        while (-1 != (count = in.read(bytes))) {
	            out.write(bytes, 0, count);
	        }
	        out.close();
	        out = null;
	        return true;
	    } catch (IOException e) {
	        logger.error("saveToFile", e);
	    } finally {
	    	if (null != out) {
		        try {
					out.close();
				} catch (IOException e) {
					logger.error("saveToFile", e);
				}
	    	}
	    }
        return false;
	}
	
	@GET
	@Path("csv4dsr/{idDsr}")
	@Produces("application/json")
	public Response getXlsS3url(@PathParam("idDsr") String idDsr) {
		return metadataService.getByIdDsr(idDsr);
	}

//	@GET
//	@Path("csv4dsr/{idDsr}")
//	@Produces("application/json")
//	public Response getXlsS3url(@PathParam("idDsr") String idDsr) {
//		try {
//			logger.debug("getXlsS3url: idDsr {}", idDsr);
//			final EntityManager entityManager = provider.get();
//			// DSR_METADATA
//			final DsrMetadata dsrMetadata = entityManager
//					.find(DsrMetadata.class, idDsr);
//			logger.debug("getXlsS3url: dsrMetadata {}", dsrMetadata);
//			if (null == dsrMetadata) {
//				return Response.status(Status.NOT_FOUND).build();
//			}
//			final String year = String.format("%04d", dsrMetadata.getYear());
//			final String month = "month".equalsIgnoreCase(dsrMetadata.getPeriodType()) ?
//					String.format("%02d", dsrMetadata.getPeriod()) :
//						String.format("%02d", dsrMetadata.getPeriod() * 3 - 2);
//
//			// BLACKLIST
//			String sql = new StringBuffer()
//				.append("select XLS_S3_URL")
//				.append(" from BLACKLIST")
//				.append(" where COUNTRY = '" + dsrMetadata.getCountry() + "'")
//				.append("   and VALID_FROM <= STR_TO_DATE('").append(year + "-" + month).append("', '%Y-%m')")
//				.append("   order by VALID_FROM desc")
//				.append("   limit 1").toString();
//			@SuppressWarnings("unchecked")
//			final List<String> xlsS3Urls = entityManager
//				.createNativeQuery(sql)
//				.getResultList();
//			logger.debug("getXlsS3url: xlsS3Urls {}", xlsS3Urls);
//			final String xlsS3Url = null == xlsS3Urls ||
//					xlsS3Urls.isEmpty() ? null : xlsS3Urls.get(0);
//			logger.debug("getXlsS3url: xlsS3Url {}", xlsS3Url);
//			// format response json
//			final Map<String, String> result = new HashMap<String, String>();
//			result.put("idDsr", idDsr);
//			if (!Strings.isNullOrEmpty(xlsS3Url)) {
//				result.put("blacklistS3Url", xlsS3Url);
//			}
//			result.put("country", dsrMetadata.getCountry());
//			if (null != dsrMetadata.getServiceCode()) {
//				result.put("serviceCode", dsrMetadata.getServiceCode().toString());
//			}
//			result.put("currency", dsrMetadata.getCurrency());
//			result.put("year", year);
//			result.put("month", month);
//			return Response.ok(gson.toJson(result)).build();
//		} catch (Exception e) {
//			logger.error("getXlsS3url", e);
//		}
//		return Response.status(Status.INTERNAL_SERVER_ERROR).build();
//	}

//	public static void main(String[] args) throws Exception {
//		logger.debug("" + BlacklistService
//				.getBlacklistFileErrors(new File("/Users/roberto/Downloads/priced_lines (1).csv")));
//	}
	
}
