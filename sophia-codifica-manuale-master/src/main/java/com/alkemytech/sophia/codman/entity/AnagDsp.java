package com.alkemytech.sophia.codman.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.codman.dto.AnagUtilizationTypeIdDspResultSet;
import com.alkemytech.sophia.codman.persistence.AbstractEntity;
import com.google.gson.GsonBuilder;

@XmlRootElement
@Entity(name="AnagDsp")
@Table(name="ANAG_DSP")
@NamedQueries({@NamedQuery(name="AnagDsp.GetAll", query="SELECT dsp FROM AnagDsp dsp")})
@SuppressWarnings("serial")



public class AnagDsp extends AbstractEntity<String> {

	@Id
	@Column(name="IDDSP", nullable=false)
	private String idDsp;
	
	@Column(name="CODE", nullable=false)
	private String code;
	
	@Column(name="NAME")
	private String name;
	
	@Column(name="DESCRIPTION", nullable=false)
	private String description;
	
	@Column(name="FTP_SOURCE_PATH")
	private String ftpSourcePath;
	
	@Override
	public String getId() {
		return getIdDsp();
	}

	public String getIdDsp() {
		return idDsp;
	}

	public void setIdDsp(String idDsp) {
		this.idDsp = idDsp;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFtpSourcePath() {
		return ftpSourcePath;
	}

	public void setFtpSourcePath(String ftpSourcePath) {
		this.ftpSourcePath = ftpSourcePath;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}
}