package com.alkemytech.sophia.codman.broadcasting;

import com.alkemytech.sophia.broadcasting.model.BdcUtenti;
import com.alkemytech.sophia.broadcasting.service.interfaces.BdcBroadcastersService;
import com.alkemytech.sophia.broadcasting.service.interfaces.BdcUtentiService;
import com.alkemytech.sophia.codman.utils.EmailUtils;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.BodyPartEntity;
import com.sun.jersey.multipart.FormDataBodyPart;
import com.sun.jersey.multipart.FormDataParam;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@Singleton
@Path("mail")
public class BroadcastingMailSender {

    private final String fromEmail;
    private final String password;
    private final String toEmail;
    private final String smtp;
    private final String nameSender;
    private final Integer port;
    private final BdcUtentiService bdcUtentiService;
    private final BdcBroadcastersService bdcBroadcastersService;

    @Inject
    public BroadcastingMailSender(@Named("broadcasting_mail_from")String fromEmail,
                                  @Named("broadcasting_mail_pass") String password,
                                  @Named("broadcasting_mail_to") String toEmail,
                                  @Named("broadcasting_mail_smtp_host") String smtp,
                                  @Named("broadcasting_mail_smtp_port") Integer port,
                                  @Named("broadcasting.mail.name.sender") String nameSender,
                                  BdcUtentiService bdcUtentiService,
                                  BdcBroadcastersService bdcBroadcastersService
                                  )
    {
        super();
        this.fromEmail=fromEmail;
        this.password=password;
        this.toEmail=toEmail;
        this.smtp=smtp;
        this.port=port;
        this.bdcUtentiService=bdcUtentiService;
        this.bdcBroadcastersService=bdcBroadcastersService;
        this.nameSender=nameSender;
    }

    @POST
    @Path("send")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response sendMail(
                             @FormDataParam("file") List<FormDataBodyPart> bodyParts,
                             @FormDataParam("oggetto") String oggetto,
                             @FormDataParam("testoMail") String testoMail,
                             @FormDataParam("idUtente") Integer idUtente,
                             @FormDataParam("idBroadcaster") Integer idBroadcaster
                             ) {

        Properties props = new Properties();
        props.put("mail.smtp.host", smtp);
        props.put("mail.smtp.port", port);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        Authenticator auth = new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(fromEmail, password);
            }
        };
        Session session = Session.getInstance(props, auth);
        List<InputStream>inputStreams = new ArrayList<>();
        List<FormDataContentDisposition>formDataContentDispositions = new ArrayList<>();
        if (bodyParts!=null) {
            for (int i = 0; i < bodyParts.size(); i++) {
                inputStreams.add(((BodyPartEntity) bodyParts.get(i).getEntity()).getInputStream());
                formDataContentDispositions.add(bodyParts.get(i).getFormDataContentDisposition());
            }
        }
		BdcUtenti bdcUtente=bdcUtentiService.findById(idUtente);
        EmailUtils.sendEmail(bdcUtente.getBdcBroadcasters(), bdcUtente,nameSender, session, toEmail, inputStreams, formDataContentDispositions, oggetto, testoMail);
        return Response.ok().build();
    }

}
