package com.alkemytech.sophia.codman.guice;

import com.alkemytech.sophia.codman.provider.JooqProvider;
import com.google.inject.Key;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.inject.persist.PersistFilter;
import org.jooq.DSLContext;

import javax.persistence.EntityManager;
import java.util.Properties;

public class UnidentifiedJpaModule extends AbstractJpaModule {

    public static final String UNIT_NAME = "unidentified";
    public static final Key<PersistFilter> FILTER_KEY = Key.get(PersistFilter.class, UnidentifiedDataSource.class);

    public UnidentifiedJpaModule(Properties configuration) {
        super(configuration, UNIT_NAME);
    }

    @Override
    protected void configure() {
        // install jpa module
        super.configure();
        final Provider<EntityManager> entityManagerProvider = binder().getProvider(EntityManager.class);
        bind(EntityManager.class)
                .annotatedWith(UnidentifiedDataSource.class)
                .toProvider(entityManagerProvider);
        expose(EntityManager.class)
                .annotatedWith(UnidentifiedDataSource.class);
        bind(FILTER_KEY).to(PersistFilter.class);
        expose(FILTER_KEY);

        bind(DSLContext.class).annotatedWith(UnidentifiedDataSource.class).toProvider(JooqProvider.class).in(Singleton.class);
        expose(DSLContext.class)
                .annotatedWith(UnidentifiedDataSource.class);
    }
}
