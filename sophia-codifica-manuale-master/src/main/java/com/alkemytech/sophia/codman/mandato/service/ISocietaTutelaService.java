package com.alkemytech.sophia.codman.mandato.service;

import java.io.ByteArrayOutputStream;
import java.util.List;

import com.alkemytech.sophia.codman.dto.SocietaTutelaDTO;
import com.alkemytech.sophia.codman.rest.PagedResult;

public interface ISocietaTutelaService {

	public SocietaTutelaDTO get(Integer id);

	public List<SocietaTutelaDTO> getAll();

	public SocietaTutelaDTO save(SocietaTutelaDTO societa);

	public int delete(SocietaTutelaDTO societa);

	public List<SocietaTutelaDTO> search(String field, String value);

	public PagedResult documentation(String codiceSocieta, Integer pageNumber, Integer pageSize, String order);

	public ByteArrayOutputStream downloadById(Integer id);

    List<SocietaTutelaDTO> getAllByTenant0();

}
