package com.alkemytech.sophia.codman.dto;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Set;

@XmlRootElement
public class DeliverCcidDTO {

    private Set<String> ccid;
    private String ftpPath;
    private boolean zip;
    private String zipName;

    public String getFtpPath() {
        return ftpPath;
    }

    public DeliverCcidDTO setFtpPath(String ftpPath) {
        this.ftpPath = ftpPath;
        return this;
    }

    public Set<String> getCcid() {
        return ccid;
    }

    public DeliverCcidDTO setCcid(Set<String> ccid) {
        this.ccid = ccid;
        return this;
    }

    public String getZipName() {
        return zipName;
    }

    public void setZipName(String zipName) {
        this.zipName = zipName;
    }

    public boolean isZip() {
        return zip;
    }

    public void setZip(boolean zip) {
        this.zip = zip;
    }

    public DeliverCcidDTO() {
    }

}
