package com.alkemytech.sophia.codman.multimedialelocale.dto;

public class MlIntegrazioneResponse {
	private int idMlReport;
	
	public MlIntegrazioneResponse() {
		super();
	}
	
	public MlIntegrazioneResponse(int idMlReport) {
		super();
		this.idMlReport = idMlReport;
	}

	public int getIdMlReport() {
		return idMlReport;
	}

	public void setIdMlReport(int idMlReport) {
		this.idMlReport = idMlReport;
	}
	
}
