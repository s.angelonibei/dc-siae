package com.alkemytech.sophia.codman.rest;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.alkemytech.sophia.codman.dto.ClaimConfigDTO;
import com.alkemytech.sophia.codman.dto.ClaimResultDTO;
import com.alkemytech.sophia.codman.entity.*;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

@Singleton
@Path("percClaim")
public class PercClaimService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final Provider<EntityManager> provider;
    private final Gson gson;

    @Inject
    protected PercClaimService(
            @McmdbDataSource Provider<EntityManager> provider,
            Gson gson) {
        super();
        this.provider = provider;
        this.gson = gson;
    }

    @GET
    @Path("getResultList/{id_dsr}")
    @Produces("application/json")
    public Response findById(@PathParam("id_dsr") String dsr) {
        final EntityManager entityManager = provider.get();

        List<ClaimResultDTO> dtoList = new ArrayList<>();
        List<MMPercClaimResult> results = entityManager.createQuery("SELECT a, b FROM MMPercClaimResult a, ClaimConfig b where a.idConfig = b.id and a.esito = 'KO' and a.dsr = :value1")
                .setParameter("value1", dsr).getResultList();

        for (Object r : results) {
            ClaimResultDTO dto = this.fromResultToClaimResultDto((Object[]) r);
            dtoList.add(dto);
        }

        if (null == results) {
            return Response.status(Status.NOT_FOUND).build();
        }
        return Response.ok(gson.toJson(dtoList)).build();
    }


    @GET
    @Path("getConf")
    @Produces("application/json")
    @SuppressWarnings("unchecked")
    public Response all(@DefaultValue("0") @QueryParam("first") int first,
                        @DefaultValue("50") @QueryParam("last") int last,
                        @DefaultValue("") @QueryParam("uuid") String codiceUUID,
                        @DefaultValue("") @QueryParam("dsp") String dsp,
                        @DefaultValue("") @QueryParam("country") String country,
                        @DefaultValue("") @QueryParam("utilization") String utilization,
                        @DefaultValue("") @QueryParam("offer") String offer) {

        // this.logger.info(dsp);
        // this.logger.info(utilization);
        // this.logger.info(offer);

        EntityManager entityManager = null;
        List<ClaimConfigDTO> dtoList = new ArrayList<>();
        try {
            entityManager = provider.get();

            StringBuilder sql = new StringBuilder();

            sql.append("SELECT cc, ad, ac, aut, co from " +
                    "ClaimConfig cc " +
                    "LEFT JOIN AnagDsp ad ON cc.dspId = ad.idDsp " +
                    "LEFT JOIN AnagCountry ac ON cc.countryId = ac.idCountry " +
                    "LEFT JOIN AnagUtilizationType aut ON cc.utilizationTypeId = aut.idUtilizationType " +
                    "LEFT JOIN CommercialOffers co ON cc.offerId = co.idCommercialOffering WHERE 1=1");
            // sql.append("SELECT a, o from ClaimConfig a LEFT JOIN CommercialOffers o ON a.offerId = o.idCommercialOffering WHERE 1 = 1");

            if (StringUtils.isNotEmpty(codiceUUID)) {
                sql.append(" and lower(cc.codiceUUID) like lower(concat('%', :uuid, '%'))");
            }

            if (this.isNotEmpty(dsp)) {
                sql.append(" and cc.dspId = :dsp");
            }

            if (this.isNotEmpty(country)) {
                sql.append(" and cc.countryId = :country");
            }
            if (this.isNotEmpty(utilization)) {
                sql.append(" and cc.utilizationTypeId = :tipoUtilizzo");
            }

            if (this.isNotEmpty(offer)) {
                // sql.append(" and o.idCommercialOffering = :offertaCommerciale");
                sql.append(" and cc.offerId = :offertaCommerciale");
            }

            sql.append(" ORDER BY cc.lastUpdate DESC");

            final Query q = entityManager.createQuery(sql.toString());

            if (StringUtils.isNotEmpty(codiceUUID)) {
                q.setParameter("uuid", codiceUUID);
            }

            if (this.isNotEmpty(dsp)) {
                q.setParameter("dsp", dsp);
            }

            if (this.isNotEmpty(country)) {
                q.setParameter("country", country);
            }

            if (this.isNotEmpty(utilization)) {
                q.setParameter("tipoUtilizzo", utilization);
            }

            if (this.isNotEmpty(offer)) {
                q.setParameter("offertaCommerciale", offer);
            }

            q.setFirstResult(first).setMaxResults(1 + last - first);

            List<Object> result = (List<Object>) q.getResultList();
            final PagedResult pagedResult = new PagedResult();
            if (null != result) {
                for (Object r : result) {
                    ClaimConfigDTO dto = this.fromResultToClaimConfigDto((Object[]) r);
                    dtoList.add(dto);
                }

                final int maxrows = last - first;
                final boolean hasNext = result.size() > maxrows;
                pagedResult.setRows(dtoList).setMaxrows(maxrows).setFirst(first)
                        .setLast(hasNext ? last : first + result.size()).setHasNext(hasNext).setHasPrev(first > 0);

            } else {
                pagedResult.setMaxrows(0).setFirst(0).setLast(0).setHasNext(false).setHasPrev(false);
            }

            return Response.ok(gson.toJson(pagedResult)).build();

        } catch (Exception e) {
            logger.error("all", e);
            e.printStackTrace();
        }
        return Response.status(500).build();
    }

    @POST
    @Path("/saveConf")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response save(ClaimConfigDTO claimConfigDTO) {
        EntityManager entityManager = provider.get();
        ClaimConfig entity = this.toClaimConfigEntity(claimConfigDTO);
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(entity);
            entityManager.getTransaction().commit();
            return Response.ok(gson.toJson(entity)).build();
        } catch (Exception e) {
            logger.error("add", e);
            Status status = Status.INTERNAL_SERVER_ERROR;
            if (e.getCause() != null && e.getCause().getCause() != null
                    && e.getCause().getCause() instanceof MySQLIntegrityConstraintViolationException) {
                status = Status.CONFLICT;
            }

            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            return Response.status(status).build();
        }
    }

    @PUT
    @Path("/updateConf")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(ClaimConfigDTO claimConfigDTO) {
        EntityManager entityManager = provider.get();
        try {
            ClaimConfig entity = entityManager.find(ClaimConfig.class, claimConfigDTO.getId());
            this.toClaimConfigEntity(entity, claimConfigDTO);
            entityManager.getTransaction().begin();
            entityManager.merge(entity);
            entityManager.getTransaction().commit();
            return Response.ok(gson.toJson(entity)).build();
        } catch (Exception e) {

            logger.error("put", e);
            Status status = Status.INTERNAL_SERVER_ERROR;
            if (e.getCause() != null && e.getCause().getCause() != null
                    && e.getCause().getCause() instanceof MySQLIntegrityConstraintViolationException) {
                status = Status.CONFLICT;
            }

            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
            return Response.status(status).build();
        }
    }

    @DELETE
    @Path("/removeConf")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(ClaimConfigDTO claimConfigDTO) {
        EntityManager entityManager = provider.get();
        try {
            ClaimConfig entity = entityManager.find(ClaimConfig.class, claimConfigDTO.getId());
            entityManager.getTransaction().begin();
            entityManager.remove(entity);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
        }
        return Response.ok().build();
    }

    private boolean isNotEmpty(String value) {
        return StringUtils.isNotEmpty(value) && !value.equals("*");
    }

    private ClaimConfigDTO fromResultToClaimConfigDto(Object[] aResult) {
        ClaimConfig cc = (ClaimConfig) aResult[0];
        AnagDsp ad = (AnagDsp) aResult[1];
        AnagCountry ac = (AnagCountry) aResult[2];
        AnagUtilizationType aut = (AnagUtilizationType) aResult[3];
        CommercialOffers co = (CommercialOffers) aResult[4];
        ClaimConfigDTO dto = this.toClaimConfigDto(cc, ad, ac, aut, co);
        return dto;
    }

    private ClaimConfigDTO toClaimConfigDto(ClaimConfig cc, AnagDsp ad, AnagCountry ac, AnagUtilizationType aut, CommercialOffers co) {
        ClaimConfigDTO dto = new ClaimConfigDTO();
        if (cc != null) {
            dto.setId(cc.getId());
            dto.setCodiceUUID(cc.getCodiceUUID());
            dto.setDspId(cc.getDspId());
            dto.setCountryId(cc.getCountryId());
            dto.setUtilizationTypeId(cc.getUtilizationTypeId());
            dto.setOfferId(cc.getOfferId());

            dto.setOfferId(cc.getOfferId());
            dto.setPercDem(cc.getPercDem());
            dto.setPercDrm(cc.getPercDrm());

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String validFrom = null;
            String validTo = null;
            String lastUpdate = null;

            validFrom = this.formatDate(cc.getValidFrom(), sdf);
            validTo = this.formatDate(cc.getValidTo(), sdf);
            lastUpdate = this.formatDate(cc.getLastUpdate(), sdf);

            dto.setValidFrom(validFrom);
            dto.setValidTo(validTo);
            dto.setLastUpdate(lastUpdate);

            if (cc.getDspId().equals("*")) {
                dto.setDspName(cc.getDspId());
            }

            if (cc.getCountryId().equals("*")) {
                dto.setCountryName(cc.getCountryId());
            }

            if (cc.getUtilizationTypeId().equals("*")) {
                dto.setUtilizationTypeName(cc.getUtilizationTypeId());
            }

            if (cc.getOfferId().equals("*")) {
                dto.setOfferName(cc.getOfferId());
            }
        }

        if (ad != null) {
            dto.setDspName(ad.getName());
        }

        if (ac != null) {
            dto.setCountryName(ac.getName());
        }

        if (aut != null) {
            dto.setUtilizationTypeName(aut.getName());
        }

        if (co != null) {
            dto.setOfferName(co.getOffering());
        }

        return dto;
    }

    private ClaimConfig toClaimConfigEntity(ClaimConfigDTO dto) {
        ClaimConfig entity = new ClaimConfig();
        return this.toClaimConfigEntity(entity, dto);
    }

    private ClaimConfig toClaimConfigEntity(ClaimConfig entity, ClaimConfigDTO dto) {
        entity.setId(dto.getId());
        entity.setCodiceUUID(dto.getCodiceUUID());
        entity.setDspId(dto.getDspId());
        entity.setCountryId(dto.getCountryId());
        entity.setOfferId(dto.getOfferId());
        entity.setUtilizationTypeId(dto.getUtilizationTypeId());
        entity.setPercDem(dto.getPercDem());
        entity.setPercDrm(dto.getPercDrm());

        Date validFrom = null;
        Date validTo = null;
        Date lastUpdate = null;

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        validFrom = this.parseDate(dto.getValidFrom(), sdf);
        validTo = this.parseDate(dto.getValidTo(), sdf);
        lastUpdate = this.parseDate(dto.getLastUpdate(), sdf);

        entity.setValidFrom(validFrom);
        entity.setValidTo(validTo);
        entity.setLastUpdate(lastUpdate);
        return entity;
    }

    private String formatDate(Date date, SimpleDateFormat sdf) {
        if (date == null) {
            return null;
        }
        return sdf.format(date);
    }

    private Date parseDate(String dateStr, SimpleDateFormat sdf) {
        if (dateStr == null) {
            return null;
        }

        try {
            return sdf.parse(dateStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private ClaimResultDTO fromResultToClaimResultDto(Object[] aResult) {
        MMPercClaimResult r = (MMPercClaimResult) aResult[0];
        ClaimConfig c = (ClaimConfig) aResult[1];
        ClaimResultDTO dto = new ClaimResultDTO();

        dto.setDsr(r.getDsr());

        dto.setSalesTransactionId(r.getSalesTransactionId());

        dto.setCodiceUUID(c.getCodiceUUID());

        dto.setPercDemRes(r.getPercDemRes());
        dto.setPercDrmRes(r.getPercDrmRes());

        dto.setPercDemConf(r.getPercDemConf());
        dto.setPercDrmConf(r.getPercDrmConf());

        dto.setEsito(r.getEsito());

        return dto;
    }

}
