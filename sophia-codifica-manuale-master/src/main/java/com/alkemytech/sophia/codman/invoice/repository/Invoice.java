package com.alkemytech.sophia.codman.invoice.repository;

import com.alkemytech.sophia.codman.persistence.AbstractEntity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@XmlRootElement
@Entity(name = "Invoice")
@Table(name = "INVOICE")
@NamedQueries({ @NamedQuery(name = "Invoice.GetAll", query = "SELECT x FROM Invoice x") })
@SuppressWarnings("serial")

public class Invoice extends AbstractEntity<String> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_INVOICE", nullable = false)
	private Integer idInvoice;

	@Column(name = "INVOICE_CODE")
	private String invoiceCode;
	
	@Column(name = "CLIENT_ID", nullable = false)
	private Integer clientId;

	@Column(name = "CREATION_DATE", nullable = false)
	private Date creationDate;

	@Column(name = "LAST_UPDATE_DATE", nullable = false)
	private Date lastUpdateDate;

	@Column(name = "USER_ID")
	private String userID;

	@Column(name = "IDDSP", nullable = false)
	private String idDsp;

	@Column(name = "TOTAL_INVOICE", nullable = false)
	private BigDecimal total;

	@Column(name = "VAT")
	private Float vat;

	@Column(name = "NOTE")
	private String note;

	@Column(name = "STATUS", nullable = false)
	private String status;

	@Column(name = "PDF_PATH")
	private String pdfPath;

	@Column(name = "DATE_OF_PERTINENCE", nullable = false)
	private Date dateOfPertinence;
	
	@Column(name = "INVOICE_TYPE", nullable = false)
	private String invoiceType;
	
	@Column(name = "REQUEST_ID")
	private String requestID;

	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "PDF", columnDefinition = "BLOB")
	private byte[] pdf;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_INVOICE", referencedColumnName = "ID_INVOICE")
	private List<InvoiceItem> invoiceItems;

	
	
	public Invoice() {
		super();
	}



	public Invoice(Integer idInvoice, String invoiceCode, Integer clientId, Date creationDate, Date lastUpdateDate,
			String userID, String idDsp, BigDecimal total, Float vat, String note, String status, String pdfPath,
			Date dateOfPertinence, String invoiceType, String requestID, byte[] pdf, List<InvoiceItem> invoiceItems) {
		super();
		this.idInvoice = idInvoice;
		this.invoiceCode = invoiceCode;
		this.clientId = clientId;
		this.creationDate = creationDate;
		this.lastUpdateDate = lastUpdateDate;
		this.userID = userID;
		this.idDsp = idDsp;
		this.total = total;
		this.vat = vat;
		this.note = note;
		this.status = status;
		this.pdfPath = pdfPath;
		this.dateOfPertinence = dateOfPertinence;
		this.invoiceType = invoiceType;
		this.requestID = requestID;
		this.pdf = pdf;
		this.invoiceItems = invoiceItems;
	}
	
	
	@Override
	public String getId() {
		return getIdInvoice().toString();
	}

	public Integer getIdInvoice() {
		return idInvoice;
	}

	public void setIdInvoice(Integer idInvoice) {
		this.idInvoice = idInvoice;
	}

	public Integer getClientId() {
		return clientId;
	}

	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getIdDsp() {
		return idDsp;
	}

	public void setIdDsp(String idDsp) {
		this.idDsp = idDsp;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public Float getVat() {
		return vat;
	}

	public void setVat(Float vat) {
		this.vat = vat;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPdfPath() {
		return pdfPath;
	}

	public void setPdfPath(String pdfPath) {
		this.pdfPath = pdfPath;
	}

	
	public Date getDateOfPertinence() {
		return dateOfPertinence;
	}

	public void setDateOfPertinence(Date dateOfPertinence) {
		this.dateOfPertinence = dateOfPertinence;
	}

	public String getInvoiceType() {
		return invoiceType;
	}

	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType;
	}

	public String getRequestID() {
		return requestID;
	}

	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}

	public byte[] getPdf() {
		return pdf;
	}

	public void setPdf(byte[] pdf) {
		this.pdf = pdf;
	}

	public String getInvoiceCode() {
		return invoiceCode;
	}

	public void setInvoiceCode(String invoiceCode) {
		this.invoiceCode = invoiceCode;
	}

	public void addInvoiceItem(InvoiceItem item) {
		item.setInvoice(this);
		if(invoiceItems == null){			
			invoiceItems = new ArrayList();
		}
		invoiceItems.add(item);

	}

	public List<InvoiceItem> getInvoiceItems() {
		return invoiceItems;
	}

}