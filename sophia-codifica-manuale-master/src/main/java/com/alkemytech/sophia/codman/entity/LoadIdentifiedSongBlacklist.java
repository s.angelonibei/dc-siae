package com.alkemytech.sophia.codman.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "load_identified_song_blacklist")
@Data
public class LoadIdentifiedSongBlacklist implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "blacklist_id", insertable = false, nullable = false)
    private Integer blacklistId;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "artists", nullable = false)
    private String artists;

    @Column(name = "codice_opera")
    private Integer codiceOpera;

    @Column(name = "codice_uuid", nullable = false)
    private String codiceUuid;

    
}