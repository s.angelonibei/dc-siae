package com.alkemytech.sophia.codman.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

@Singleton
@SuppressWarnings("serial")
public class SimpleLogoutServlet extends HttpServlet {

	private final String ssoAuthLoginUrl;
		
	@Inject
	protected SimpleLogoutServlet(@Named("sso.auth.login.url") String ssoAuthLoginUrl) {
		super();
		this.ssoAuthLoginUrl = ssoAuthLoginUrl;
	}
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// invalidate session and authentication token
		final HttpSession session = request.getSession(false);
		if (null != session) {
			// remove attributes
			session.removeAttribute("sso.user.userName");
			session.removeAttribute("sso.user.group");
			// invalidate session
			session.invalidate();
		}
		
		// redirect to login page
		response.sendRedirect(ssoAuthLoginUrl);
		
	}
	
}
