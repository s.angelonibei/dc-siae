package com.alkemytech.sophia.codman.entity;

import java.io.Serializable;

import com.google.gson.GsonBuilder;

@SuppressWarnings("serial")
public class SqsDsrProgressId implements Serializable {
	
	private String idDsr;
	private String serviceName;
	private String queueType;

	public SqsDsrProgressId() {
		super();
	}
	
	public SqsDsrProgressId(String idDsr, String serviceName, String queueType) {
		super();
		this.idDsr = idDsr;
		this.serviceName = serviceName;
		this.queueType = queueType;
	}
	
	public String getIdDsr() {
		return idDsr;
	}

	public void setIdDsr(String idDsr) {
		this.idDsr = idDsr;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getQueueType() {
		return queueType;
	}

	public void setQueueType(String queueType) {
		this.queueType = queueType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idDsr == null) ? 0 : idDsr.hashCode());
		result = prime * result
				+ ((queueType == null) ? 0 : queueType.hashCode());
		result = prime * result
				+ ((serviceName == null) ? 0 : serviceName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SqsDsrProgressId other = (SqsDsrProgressId) obj;
		if (idDsr == null) {
			if (other.idDsr != null)
				return false;
		} else if (!idDsr.equals(other.idDsr))
			return false;
		if (queueType == null) {
			if (other.queueType != null)
				return false;
		} else if (!queueType.equals(other.queueType))
			return false;
		if (serviceName == null) {
			if (other.serviceName != null)
				return false;
		} else if (!serviceName.equals(other.serviceName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting().create().toJson(this);
	}
	
}
