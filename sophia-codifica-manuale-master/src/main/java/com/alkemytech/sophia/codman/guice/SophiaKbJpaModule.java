package com.alkemytech.sophia.codman.guice;

import com.alkemytech.sophia.codman.provider.JooqProvider;
import com.google.inject.Key;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.inject.persist.PersistFilter;
import org.jooq.DSLContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.util.Properties;

public class SophiaKbJpaModule extends AbstractJpaModule {

    public static final String UNIT_NAME = "sophia_kb";
    public static final Key<PersistFilter> FILTER_KEY = Key.get(PersistFilter.class, SophiaKbDataSource.class);
    private final Logger logger = LoggerFactory.getLogger(getClass());

    public SophiaKbJpaModule(Properties configuration) {
        super(configuration, UNIT_NAME);
    }

    @Override
    protected void configure() {
        super.configure();
        final Provider<EntityManager> entityManagerProvider = binder().getProvider(EntityManager.class);
        bind(EntityManager.class)
                .annotatedWith(SophiaKbDataSource.class)
                .toProvider(entityManagerProvider);
        expose(EntityManager.class)
                .annotatedWith(SophiaKbDataSource.class);
        bind(FILTER_KEY).to(PersistFilter.class);
        expose(FILTER_KEY);

        bind(DSLContext.class).annotatedWith(SophiaKbDataSource.class).toProvider(JooqProvider.class).in(Singleton.class);
        expose(DSLContext.class)
                .annotatedWith(SophiaKbDataSource.class);
    }

}
