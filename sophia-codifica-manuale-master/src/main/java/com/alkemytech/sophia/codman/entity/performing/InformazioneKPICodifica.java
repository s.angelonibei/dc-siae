package com.alkemytech.sophia.codman.entity.performing;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


public class InformazioneKPICodifica implements Serializable {
	
	private String voceIncasso;
	private Long numeroCombana;
	private Long numeroUtilizzazioni;
	private Date competenzeDa;
	private Date competenzeA;
	private String codicePeriodo;
	private Long numeroCodificateAutomatica;
	private Long numeroCodificateManuale;
	private Long numeroNonCodificateDaApprovare;
	private Long numeroNonCodificateScartate;
	private BigDecimal valoreEconomicoNonCodificato;
	private BigDecimal valoreEconomicoCodificato;
	private BigDecimal percentCodificatoAutomatico;
	private BigDecimal percentCodificatoManuale;
	private BigDecimal percentNonCodificatoCombDaApprovare;
	private BigDecimal percentNonCodificatoCombDaAScartate;

	public InformazioneKPICodifica(String voceIncasso, Long numeroCombana, Long numeroUtilizzazioni,
			Date competenzeDa, Date competenzeA, String codicePeriodo, Long numeroCodificateAutomatica,
			Long numeroCodificateManuale, Long numeroNonCodificateDaApprovare, Long numeroNonCodificateScartate,
			BigDecimal valoreEconomicoNonCodificato, BigDecimal valoreEconomicoCodificato) {
		super();
		this.voceIncasso = voceIncasso;
		this.numeroCombana = numeroCombana;
		this.numeroUtilizzazioni = numeroUtilizzazioni;
		this.competenzeDa = competenzeDa;
		this.competenzeA = competenzeA;
		this.codicePeriodo = codicePeriodo;
		this.numeroCodificateAutomatica = numeroCodificateAutomatica;
		this.numeroCodificateManuale = numeroCodificateManuale;
		this.numeroNonCodificateDaApprovare = numeroNonCodificateDaApprovare;
		this.numeroNonCodificateScartate = numeroNonCodificateScartate;
		this.valoreEconomicoNonCodificato = valoreEconomicoNonCodificato;
		this.valoreEconomicoCodificato = valoreEconomicoCodificato;
	}
	
	public String getVoceIncasso() {
		return voceIncasso;
	}
	public void setVoceIncasso(String voceIncasso) {
		this.voceIncasso = voceIncasso;
	}
	public Long getNumeroCombana() {
		return numeroCombana;
	}
	public void setNumeroCombana(Long numeroCombana) {
		this.numeroCombana = numeroCombana;
	}
	public Long getNumeroUtilizzazioni() {
		return numeroUtilizzazioni;
	}
	public void setNumeroUtilizzazioni(Long numeroUtilizzazioni) {
		this.numeroUtilizzazioni = numeroUtilizzazioni;
	}
	public Date getCompetenzeDa() {
		return competenzeDa;
	}
	public void setCompetenzeDa(Date competenzeDa) {
		this.competenzeDa = competenzeDa;
	}
	public Date getCompetenzeA() {
		return competenzeA;
	}
	public void setCompetenzeA(Date competenzeA) {
		this.competenzeA = competenzeA;
	}
	public String getCodicePeriodo() {
		return codicePeriodo;
	}
	public void setCodicePeriodo(String codicePeriodo) {
		this.codicePeriodo = codicePeriodo;
	}
	public Long getNumeroCodificateAutomatica() {
		return numeroCodificateAutomatica;
	}
	public void setNumeroCodificateAutomatica(Long numeroCodificateAutomatica) {
		this.numeroCodificateAutomatica = numeroCodificateAutomatica;
	}
	public Long getNumeroCodificateManuale() {
		return numeroCodificateManuale;
	}
	public void setNumeroCodificateManuale(Long numeroCodificateManuale) {
		this.numeroCodificateManuale = numeroCodificateManuale;
	}
	public Long getNumeroNonCodificateDaApprovare() {
		return numeroNonCodificateDaApprovare;
	}
	public void setNumeroNonCodificateDaApprovare(Long numeroNonCodificateDaApprovare) {
		this.numeroNonCodificateDaApprovare = numeroNonCodificateDaApprovare;
	}
	public Long getNumeroNonCodificateScartate() {
		return numeroNonCodificateScartate;
	}
	public void setNumeroNonCodificateScartate(Long numeroNonCodificateScartate) {
		this.numeroNonCodificateScartate = numeroNonCodificateScartate;
	}
	public BigDecimal getValoreEconomicoNonCodificato() {
		return valoreEconomicoNonCodificato;
	}
	public void setValoreEconomicoNonCodificato(BigDecimal valoreEconomicoNonCodificato) {
		this.valoreEconomicoNonCodificato = valoreEconomicoNonCodificato;
	}
	public BigDecimal getValoreEconomicoCodificato() {
		return valoreEconomicoCodificato;
	}
	public void setValoreEconomicoCodificato(BigDecimal valoreEconomicoCodificato) {
		this.valoreEconomicoCodificato = valoreEconomicoCodificato;
	}
	public BigDecimal getPercentCodificatoAutomatico() {
		return percentCodificatoAutomatico;
	}
	public void setPercentCodificatoAutomatico(BigDecimal percentCodificatoAutomatico) {
		this.percentCodificatoAutomatico = percentCodificatoAutomatico;
	}
	public BigDecimal getPercentCodificatoManuale() {
		return percentCodificatoManuale;
	}
	public void setPercentCodificatoManuale(BigDecimal percentCodificatoManuale) {
		this.percentCodificatoManuale = percentCodificatoManuale;
	}
	public BigDecimal getPercentNonCodificatoCombDaApprovare() {
		return percentNonCodificatoCombDaApprovare;
	}
	public void setPercentNonCodificatoCombDaApprovare(BigDecimal percentNonCodificatoCombDaApprovare) {
		this.percentNonCodificatoCombDaApprovare = percentNonCodificatoCombDaApprovare;
	}
	public BigDecimal getPercentNonCodificatoCombDaAScartate() {
		return percentNonCodificatoCombDaAScartate;
	}
	public void setPercentNonCodificatoCombDaAScartate(BigDecimal percentNonCodificatoCombDaAScartate) {
		this.percentNonCodificatoCombDaAScartate = percentNonCodificatoCombDaAScartate;
	}
}
