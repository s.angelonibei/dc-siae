package com.alkemytech.sophia.codman.gestioneperiodi;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.http.util.TextUtils;

import com.alkemytech.sophia.broadcasting.dto.Periodo;

public class GestionePeriodiImpl implements GestionePeriodiInterface {

	@Override
	public List<Periodo> getPeriodi(Integer anno, String ambito) {

		List<Periodo> listaPeriodi = new ArrayList<Periodo>();
		GregorianCalendar primoSemIn = new GregorianCalendar();
		primoSemIn.setLenient(false);
		primoSemIn.set(GregorianCalendar.YEAR, anno);
		primoSemIn.set(GregorianCalendar.MONTH, GregorianCalendar.JANUARY);
		primoSemIn.set(GregorianCalendar.DATE, 1);
		primoSemIn.set(GregorianCalendar.HOUR_OF_DAY, 0);
		primoSemIn.set(GregorianCalendar.MINUTE, 0);
		primoSemIn.set(GregorianCalendar.SECOND, 0);
		primoSemIn.set(GregorianCalendar.MILLISECOND, 0);

		GregorianCalendar primoSemFi = new GregorianCalendar();
		primoSemFi.setLenient(false);
		primoSemFi.set(GregorianCalendar.YEAR, anno);
		primoSemFi.set(GregorianCalendar.MONTH, GregorianCalendar.JUNE);
		primoSemFi.set(GregorianCalendar.DATE, 30);
		primoSemFi.set(GregorianCalendar.HOUR_OF_DAY, 0);
		primoSemFi.set(GregorianCalendar.MINUTE, 0);
		primoSemFi.set(GregorianCalendar.SECOND, 0);
		primoSemFi.set(GregorianCalendar.MILLISECOND, 0);

		GregorianCalendar secondoSemIn = new GregorianCalendar();
		secondoSemIn.setLenient(false);
		secondoSemIn.set(GregorianCalendar.YEAR, anno);
		secondoSemIn.set(GregorianCalendar.MONTH, GregorianCalendar.JULY);
		secondoSemIn.set(GregorianCalendar.DATE, 1);
		secondoSemIn.set(GregorianCalendar.HOUR_OF_DAY, 0);
		secondoSemIn.set(GregorianCalendar.MINUTE, 0);
		secondoSemIn.set(GregorianCalendar.SECOND, 0);
		secondoSemIn.set(GregorianCalendar.MILLISECOND, 0);

		GregorianCalendar secondoSemFin = new GregorianCalendar();
		secondoSemFin.setLenient(false);
		secondoSemFin.set(GregorianCalendar.YEAR, anno);
		secondoSemFin.set(GregorianCalendar.MONTH, GregorianCalendar.DECEMBER);
		secondoSemFin.set(GregorianCalendar.DATE, 31);
		secondoSemFin.set(GregorianCalendar.HOUR_OF_DAY, 0);
		secondoSemFin.set(GregorianCalendar.MINUTE, 0);
		secondoSemFin.set(GregorianCalendar.SECOND, 0);
		secondoSemFin.set(GregorianCalendar.MILLISECOND, 0);

		listaPeriodi.add(new Periodo(1, "1° SEMESTRE", "RIPARTIZIONE", new Date(primoSemIn.getTimeInMillis()),
				new Date(primoSemFi.getTimeInMillis())));
		listaPeriodi.add(new Periodo(2, "2° SEMESTRE", "RIPARTIZIONE", new Date(secondoSemIn.getTimeInMillis()),
				new Date(secondoSemFin.getTimeInMillis())));
		return listaPeriodi;

	}

	@Override
	public Periodo beutyfyDate(String periodo) throws Exception {
		
		
		

		if (!TextUtils.isEmpty(periodo)) {
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			Periodo per = new Periodo();
			if (periodo.matches("20[1-9][0-9]S[1-2]{1}")) {
				String[] annoMese = periodo.split("S");
				Integer anno = Integer.parseInt(annoMese[0]);
				Integer nPeriod = Integer.parseInt(annoMese[1]);
				if (nPeriod.equals(1)) {
					per.setDataInizioPeriodo(dateFormat.parse("01/1/" + anno));
					per.setDataFinePeriodo(dateFormat.parse("30/6/" + anno));
				} else if (nPeriod.equals(2)) {
					per.setDataInizioPeriodo(dateFormat.parse("01/7/" + anno));
					per.setDataFinePeriodo(dateFormat.parse("31/12/" + anno));
				}

			} else if (periodo.matches("20[1-9][0-9]Q[1-4]{1}")) {
				String[] annoMese = periodo.split("Q");
				Integer anno = Integer.parseInt(annoMese[0]);
				Integer nPeriod = Integer.parseInt(annoMese[1]);
				if (nPeriod.equals(1)) {
					per.setDataInizioPeriodo(dateFormat.parse("01/1/" + anno));
					per.setDataFinePeriodo(dateFormat.parse("31/3/" + anno));
				} else if (nPeriod.equals(2)) {
					per.setDataInizioPeriodo(dateFormat.parse("01/4/" + anno));
					per.setDataFinePeriodo(dateFormat.parse("30/6/" + anno));
				} else if (nPeriod.equals(3)) {
					per.setDataInizioPeriodo(dateFormat.parse("01/7/" + anno));
					per.setDataFinePeriodo(dateFormat.parse("30/9/" + anno));
				} else if (nPeriod.equals(4)) {
					per.setDataInizioPeriodo(dateFormat.parse("01/10/" + anno));
					per.setDataFinePeriodo(dateFormat.parse("31/12/" + anno));
				}

			} else if (periodo.matches("20[1-9][0-9]")) {
				per.setDataInizioPeriodo(dateFormat.parse("01/1/" + periodo));
				per.setDataFinePeriodo(dateFormat.parse("31/12/" + periodo));
			}else {
				return null;
			}

			return per;
		} else {
			return null;
		}
	}

}
