package com.alkemytech.sophia.codman.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.GsonBuilder;

@XmlRootElement
@Entity(name="Blacklist")
@Table(name="BLACKLIST")
public class Blacklist {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID", nullable=false)
	private Long id;

	@Column(name="COUNTRY", nullable=false)
	private String country;
	
	@Column(name="VALID_FROM", nullable=false)
	private Date validFrom;

	@Column(name="XLS_S3_URL", nullable=false)
	private String xlsS3Url;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "CODICE_SOCIETA_TUTELA", referencedColumnName = "CODICE")
	private SocietaTutela societaTutela;
	    

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public String getXlsS3Url() {
		return xlsS3Url;
	}

	public void setXlsS3Url(String xlsS3Url) {
		this.xlsS3Url = xlsS3Url;
	}
	
	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}

	public SocietaTutela getSocietaTutela() {
		return societaTutela;
	}

	public void setSocietaTutela(SocietaTutela societaTutela) {
		this.societaTutela = societaTutela;
	}
}
