package com.alkemytech.sophia.codman.dto;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Map;

@XmlRootElement (name="pagination")
@XmlAccessorType(XmlAccessType.FIELD)
public class PaginationDTO implements Serializable {
    private int pageNumber = 1;
    private int rows;
    private int lastPage = 1;
    private int pageSize = 10;

    public PaginationDTO() {
    }

    public PaginationDTO(EntityManager em, String query, Map<String, Object> parameters, int pageSize, int pageNumber) {
        TypedQuery<Integer> typedQuery = em.createQuery(query, Integer.class);
        for (String key: parameters.keySet()) {
            typedQuery.setParameter(key, parameters.get(key));
        }
        try {
            this.rows = typedQuery.getSingleResult();
        }catch (NoResultException e){
            this.rows = 0;
        }
        this.lastPage = Math.round(rows / pageSize) + 1;
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
    }

    public PaginationDTO(int pageNumber, int pageSize) {
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
    }

    public PaginationDTO(int pageNumber, int rows, int lastPage, int pageSize) {
        this.pageNumber = pageNumber;
        this.rows = rows;
        this.lastPage = lastPage;
        this.pageSize = pageSize;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public int getLastPage() {
        return lastPage;
    }

    public void setLastPage(int lastPage) {
        this.lastPage = lastPage;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
