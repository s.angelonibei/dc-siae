package com.alkemytech.sophia.codman.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity(name = "LoadIdentifiedSongDsr")
@Data
@Table(name = "load_identified_song_dsr")
public class LoadIdentifiedSongDsr implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_util", insertable = false, nullable = false)
    private String idUtil;

    @Column(name = "hash_id", nullable = false)
    private String hashId;

    @Column(name = "id_dsr")
    private String idDsr;

    @Column(name = "album_title")
    private String albumTitle;

    @Column(name = "proprietary_id")
    private String proprietaryId;

    @Column(name = "isrc")
    private String isrc;

    @Column(name = "iswc")
    private String iswc;

    @Column(name = "sales_count", nullable = false)
    private Long salesCount = 0L;

    @Column(name = "dsp")
    private String dsp;

    @Column(name = "insert_time", nullable = false)
    private Long insertTime;

    
}