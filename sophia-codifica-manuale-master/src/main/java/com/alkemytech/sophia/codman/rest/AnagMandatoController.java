package com.alkemytech.sophia.codman.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.codman.dto.MandatoDTO;
import com.alkemytech.sophia.codman.mandato.service.IMandatoService;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
@Path("mandato")
public class AnagMandatoController {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private Gson gson;
	
	private IMandatoService mandatoService;
	
	@Inject
	protected AnagMandatoController(IMandatoService mandatoService, Gson gson) {
		super();
		this.mandatoService = mandatoService;
		this.gson = gson;
	}

	@GET
	@Produces("application/json")
	public Response getAll() {
		try {
			List<MandatoDTO> results = mandatoService.getAll();
			return Response.ok(gson.toJson(results)).build();
		} catch (Exception e) {
			logger.error("Errore recupero mandati", e);
			return Response.status(500).build();
		}
	}
	
	@GET
	@Path("{id}")
	@Produces("application/json")
	public Response find(@PathParam("id") Integer id) {
		try {
			MandatoDTO result = mandatoService.get(id);
			return Response.ok(gson.toJson(result)).build();
		} catch (Exception e) {
			logger.error(String.format("Errore recupero mandato: %d", id), e);
			return Response.status(500).build();
		}
	}

	@POST
	@Produces("application/json")
	public Response save(MandatoDTO mandato) {
		try {
			MandatoDTO result = mandatoService.save(mandato);
			return Response.ok(gson.toJson(result)).build();
		} 
		catch (IllegalArgumentException e) {
			logger.error(String.format("Errore di Validazione: %s", gson.toJson(mandato)), e);
			return Response.status(400).build();
		}
		catch (Exception e) {
			logger.error(String.format("Errore salvataggio: %s", gson.toJson(mandato)), e);
			return Response.status(500).build();
		}
	}

	@DELETE
	@Produces("application/json")
	@Consumes("application/json")
	public Response delete(MandatoDTO mandato) {
		try {
			mandatoService.delete(mandato);
			return Response.ok(gson.toJson(mandato)).build();
		}
		catch (Exception e) {
			logger.error(String.format("Errore nell'eliminazione del mandato: %s", gson.toJson(mandato)), e);
			return Response.status(500).build();
		}
	}

}
