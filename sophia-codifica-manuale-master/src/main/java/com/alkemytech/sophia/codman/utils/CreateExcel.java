package com.alkemytech.sophia.codman.utils;

import com.alkemytech.sophia.codman.entity.performing.*;
import com.alkemytech.sophia.codman.entity.performing.security.AggregatoVoce;
import com.alkemytech.sophia.codman.entity.performing.security.ReportPage;
import com.alkemytech.sophia.codman.rest.performing.ReportCache;
import com.alkemytech.sophia.codman.rest.performing.service.IProgrammaMusicaleService;
import com.alkemytech.sophia.codman.rest.performing.service.ITraceService;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

//import org.apache.poi.xssf.usermodel.SXSSFCell;
//import org.apache.poi.xssf.usermodel.SXSSFRow;
//import org.apache.poi.xssf.usermodel.SXSSFSheet;
//import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * Created by cimo on 17/10/17.
 */
@Service("createExcel")
public class CreateExcel implements ICreateExcel{


    // Costruisci titolo report
    // Costruisci filtri:
    //        - Nome Filtro 1: Valore Filtro 1
    //        - Nome Filtro 2: Valore Filtro 2
    //        - Nome Filtro 3: Valore Filtro 3
    //  Costruisce intestazione
    //  Lista Dati


    @Autowired
    private ITraceService traceService;

    @Autowired
    private IProgrammaMusicaleService programmaMusicaleService;

    private void output(String date, String terminal) {

        int rowCount = 0;
        int columnCount = 0;

        SXSSFWorkbook workbook = new SXSSFWorkbook();
        SXSSFSheet sheet = workbook.createSheet("Data");

        SXSSFRow row = sheet.createRow(rowCount);

        rowCount++;
        SXSSFCell cell = row.createCell(columnCount);
        cell.setCellValue(date);

        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue(terminal);


        try {
            FileOutputStream outputStream = new FileOutputStream("Data.xlsx");
            //workbook.write(outputStream);
            workbook.write(System.out);
            //System.out.println(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public SXSSFWorkbook createReportListaEventi(){

            SXSSFWorkbook workbook = new SXSSFWorkbook();

            SXSSFSheet sheet = workbook.createSheet("Data");

            ReportPage reportPage = ReportCache.getReportForExport(ReportCache.REPORT_LISTA_EVENTI);

            int rowCount = 0;
            int columnCount = 0;

            // Intestazione Report
            SXSSFRow row = sheet.createRow(rowCount);
            SXSSFCell cell = row.createCell(columnCount);
            cell.setCellValue("Evento");
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue("ContabilitÃ ");
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue("Fattura");
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue("Voce incasso");
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue("Data Inizio");
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue("Ora Inizio");
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue("Locale");
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue("Tipo Documento");
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue("Importo");

            EventiPagati evento = null;
            List<Object> entrys = reportPage.getAllRecords();
            for (Object e:entrys){
                evento = (EventiPagati)e;
                rowCount++;
                columnCount=0;

                row = sheet.createRow(rowCount);
                cell = row.createCell(columnCount);
                cell.setCellValue(evento.getIdEvento());
                columnCount++;
                cell = row.createCell(columnCount);
                cell.setCellValue(evento.getContabilita());
                columnCount++;
                cell = row.createCell(columnCount);
                cell.setCellValue(evento.getNumeroFattura());
                columnCount++;
                cell = row.createCell(columnCount);
                cell.setCellValue(evento.getVoceIncasso());
                columnCount++;
                cell = row.createCell(columnCount);
                cell.setCellValue(evento.getDataInizioEventoStr());
                columnCount++;
                cell = row.createCell(columnCount);
                cell.setCellValue(evento.getOraInizioEvento());
                columnCount++;
                cell = row.createCell(columnCount);
                cell.setCellValue(evento.getDenominazioneLocale());
                columnCount++;
                cell = row.createCell(columnCount);
                cell.setCellValue(evento.getTipoDocumentoContabile());
                columnCount++;
                cell = row.createCell(columnCount);
                cell.setCellValue(evento.getImportDem());

            }

        return workbook;

    }


    public SXSSFWorkbook createReportListaMovimenti(){

        SXSSFWorkbook workbook = new SXSSFWorkbook();

        SXSSFSheet sheet = workbook.createSheet("Data");

        ReportPage reportPage = ReportCache.getReportForExport(ReportCache.REPORT_LISTA_MOVIMENTI);

        int rowCount = 0;
        int columnCount = 0;

        // Intestazione Report
        SXSSFRow row = sheet.createRow(rowCount);
        SXSSFCell cell = row.createCell(columnCount);
        cell.setCellValue("ContabilitÃ ");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Voce incasso");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Evento");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Fattura");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Reversale");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Tipo Documento");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Numero Programma Musicale");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Importo SUN");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Permesso");

        MovimentoContabile movimento = null;
        List<Object> entrys = reportPage.getAllRecords();
        for (Object e:entrys){
            movimento = (MovimentoContabile)e;
            rowCount++;
            columnCount=0;

            row = sheet.createRow(rowCount);
            cell = row.createCell(columnCount);
            cell.setCellValue(movimento.getContabilita());
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(movimento.getVoceIncasso());
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(movimento.getEvento().getDataInizioEventoStr()+" "+movimento.getEvento().getOraInizioEvento()+"-"+movimento.getEvento().getOraFineEvento());
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(movimento.getNumeroFattura());
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(movimento.getReversale());

            columnCount++;
            cell = row.createCell(columnCount);
            if (movimento.getTipoDocumentoContabile()!=null && movimento.getTipoDocumentoContabile().equalsIgnoreCase("501"))
              cell.setCellValue("Mod. 501 (Entrata)");
            else
              cell.setCellValue("Mod. 221 (Uscita)");

            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(movimento.getNumProgrammaMusicale());
            columnCount++;
            cell = row.createCell(columnCount);
            if (movimento.getImportoTotDem()!=null)
             cell.setCellValue(movimento.getImportoTotDem());
            else
             cell.setCellValue("");
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(movimento.getNumeroPermesso());



        }

        return workbook;

    }



    public SXSSFWorkbook createReportAggregatoIncasso(){

        SXSSFWorkbook workbook = new SXSSFWorkbook();

        SXSSFSheet sheet = workbook.createSheet("Data");

        ReportPage reportPage = ReportCache.getReportForExport(ReportCache.REPORT_AGGREGATO_INCASSO);

        int rowCount = 0;
        int columnCount = 0;

        // Intestazione Report
        SXSSFRow row = sheet.createRow(rowCount);
        SXSSFCell cell = row.createCell(columnCount);
        cell.setCellValue("Voce Incasso");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Numero Documenti (Entrate)");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Importo (Entrate)");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Numero Documenti (Uscite)");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Importo (Uscite)");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Numero Eventi");

        AggregatoVoce aggregato = null;
        List<Object> entrys = reportPage.getAllRecords();
        for (Object e:entrys){
            aggregato = (AggregatoVoce)e;
            rowCount++;
            columnCount=0;

            row = sheet.createRow(rowCount);
            cell = row.createCell(columnCount);
            cell.setCellValue(aggregato.getVoceIncasso());
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(aggregato.getNumeroDocumentiEntrata());
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(aggregato.getImportoEntrate());
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(aggregato.getNumeroDocumentiUscita());
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(aggregato.getImportoUscite());
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(aggregato.getNumeroEventi());


        }

        return workbook;

    }


    public SXSSFWorkbook createReportAggregatoMovimenti(){

        SXSSFWorkbook workbook = new SXSSFWorkbook();

        List<ReportPage> reportPage = ReportCache.getReportListForExport(ReportCache.REPORT_AGGREGATO_MOVIMENTI);

        ReportPage listaImportiCorrenti = reportPage.get(0);
        ReportPage listaCedolePMCorrenti= reportPage.get(1);
        ReportPage listaImportiArretrati = reportPage.get(2);
        ReportPage listaCedolePMArretrati = reportPage.get(3);


        // Sheet Programmi Musicali Correnti
        SXSSFSheet sheet = workbook.createSheet("Programmi Musicali Correnti");

        int rowCount = 0;
        int columnCount = 0;


        SXSSFRow row = sheet.createRow(rowCount);
        SXSSFCell cell = row.createCell(columnCount);
        cell.setCellValue("Voce Incasso");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Importo Netto Incassi");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Importo Non Programmato");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Importo Sospesi");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Importo Annullati");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Importo Programmato");

        AggregatoVoce aggregato = null;
        List<Object> entrys = listaImportiCorrenti.getRecords();
        for (Object e:entrys){
            aggregato = (AggregatoVoce)e;
            rowCount++;
            columnCount=0;

            row = sheet.createRow(rowCount);
            cell = row.createCell(columnCount);
            cell.setCellValue(aggregato.getVoceIncasso());
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(aggregato.getImporto());
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(aggregato.getImporto566());
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(aggregato.getImportoSospesi());
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(aggregato.getImportoAnnullati());
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(aggregato.getImportoProgrammato());
        }


        // Sheet Cedole Programmi Musicali Correnti
        sheet = workbook.createSheet("Cedole Programmi Musicali Correnti");

        rowCount = 0;
        columnCount = 0;

        row = sheet.createRow(rowCount);
        cell = row.createCell(columnCount);
        cell.setCellValue("Voce Incasso");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Numero Programmi Musicali");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Numero Cedole");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Importo Programmato");

        aggregato = null;
        entrys = listaCedolePMCorrenti.getRecords();
        for (Object e:entrys){
            aggregato = (AggregatoVoce)e;
            rowCount++;
            columnCount=0;

            row = sheet.createRow(rowCount);
            cell = row.createCell(columnCount);
            cell.setCellValue(aggregato.getVoceIncasso());
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(aggregato.getNumeroPM());
            columnCount++;
            cell = row.createCell(columnCount);
            if (aggregato.getNumeroCedole()!=null)
              cell.setCellValue(aggregato.getNumeroCedole());
            else
                cell.setCellValue("");
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(aggregato.getImporto());
        }


        // Sheet Programmi Musicali Arretrati
        sheet = workbook.createSheet("Programmi Musicali Arretrati");

        rowCount = 0;
        columnCount = 0;

        row = sheet.createRow(rowCount);
        cell = row.createCell(columnCount);
        cell.setCellValue("Voce Incasso");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Importo Sospesi");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Importo Annullati");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Importo Programmato");

        aggregato = null;
        entrys = listaImportiArretrati.getRecords();
        for (Object e:entrys){
            aggregato = (AggregatoVoce)e;
            rowCount++;
            columnCount=0;

            row = sheet.createRow(rowCount);
            cell = row.createCell(columnCount);
            cell.setCellValue(aggregato.getVoceIncasso());
            columnCount++;
            cell = row.createCell(columnCount);
            if (aggregato.getImportoSospesi()!=null)
             cell.setCellValue(aggregato.getImportoSospesi());
            else
             cell.setCellValue("");
            columnCount++;
            cell = row.createCell(columnCount);
            if (aggregato.getImportoAnnullati()!=null)
             cell.setCellValue(aggregato.getImportoAnnullati());
            else
             cell.setCellValue("");
            columnCount++;
            cell = row.createCell(columnCount);
            if (aggregato.getImportoProgrammato()!=null)
             cell.setCellValue(aggregato.getImportoProgrammato());
            else
             cell.setCellValue("");
        }



        // Sheet Cedole Programmi Musicali Arretrati
        sheet = workbook.createSheet("Cedole Programmi Musicali Arretrati");

        rowCount = 0;
        columnCount = 0;

        row = sheet.createRow(rowCount);
        cell = row.createCell(columnCount);
        cell.setCellValue("Voce Incasso");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Numero Programmi Musicali");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Numero Cedole");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Importo Programmato");

        aggregato = null;
        entrys = listaCedolePMArretrati.getRecords();
        for (Object e:entrys){
            aggregato = (AggregatoVoce)e;
            rowCount++;
            columnCount=0;

            row = sheet.createRow(rowCount);
            cell = row.createCell(columnCount);
            cell.setCellValue(aggregato.getVoceIncasso());
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(aggregato.getNumeroPM());
            columnCount++;
            cell = row.createCell(columnCount);
            if (aggregato.getNumeroCedole()!=null)
             cell.setCellValue(aggregato.getNumeroCedole());
            else
            cell.setCellValue("");
            columnCount++;
            cell = row.createCell(columnCount);
            if (aggregato.getImporto()!=null)
                cell.setCellValue(aggregato.getImporto());
            else
                cell.setCellValue("");
        }


        return workbook;

    }


    public SXSSFWorkbook createReportImportiRicalcolati(String reportName){

        SXSSFWorkbook workbook = new SXSSFWorkbook();

        SXSSFSheet sheet = workbook.createSheet("Importi Ricalcolati");

        ReportPage reportPage = ReportCache.getReportForExport(reportName);

        int rowCount = 0;
        int columnCount = 0;

        // Intestazione Report
        SXSSFRow row = sheet.createRow(rowCount);
        SXSSFCell cell = row.createCell(columnCount);
        cell.setCellValue("Evento");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Voce Incasso");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Fattura");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Numero Programma");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Numero Cedole");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Durata Cedole");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Tipo Documento");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Numero PM previsti");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Importo PM SUN");
        columnCount++;

        if (reportName.equalsIgnoreCase(ReportCache.REPORT_IMPORTI_RICALCOLATI)) {
            cell = row.createCell(columnCount);
            cell.setCellValue("Importo PM ricalcolato");
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue("Importo Unitario");
            columnCount++;
        }

        cell = row.createCell(columnCount);
        cell.setCellValue("Importo Evento");
        columnCount++;

        if (reportName.equalsIgnoreCase(ReportCache.REPORT_IMPORTI_SOSPESI)) {
            cell = row.createCell(columnCount);
            cell.setCellValue("Motivazione Sospensione");
        }

        if (reportName.equalsIgnoreCase(ReportCache.REPORT_IMPORTI_ERRORE)) {
            cell = row.createCell(columnCount);
            cell.setCellValue("Errore");
        }


        MovimentoContabile movimento = null;
        List<Object> entrys = reportPage.getAllRecords();
        for (Object e:entrys){
            movimento = (MovimentoContabile)e;
            rowCount++;
            columnCount=0;

            row = sheet.createRow(rowCount);
            cell = row.createCell(columnCount);
            cell.setCellValue(movimento.getIdEvento());
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(movimento.getVoceIncasso());
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(movimento.getNumeroFattura());
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(movimento.getNumProgrammaMusicale());
            columnCount++;
            cell = row.createCell(columnCount);
            if (movimento.getTotaleCedole()!=null)
             cell.setCellValue(movimento.getTotaleCedole());
            else
             cell.setCellValue("");
            columnCount++;
            cell = row.createCell(columnCount);
            if (movimento.getTotaleDurataCedole()!=null)
             cell.setCellValue(movimento.getTotaleDurataCedole());
            else
             cell.setCellValue("");
            columnCount++;
            cell = row.createCell(columnCount);
            if (movimento.getTipoDocumentoContabile()!=null && movimento.getTipoDocumentoContabile().equalsIgnoreCase("501"))
              cell.setCellValue("Mod. 501 (Entrata)");
            else
              cell.setCellValue("Mod. 221 (Uscita)");
            columnCount++;
            cell = row.createCell(columnCount);
            if (movimento.getNumeroPmPrevisti()!=null)
             cell.setCellValue(movimento.getNumeroPmPrevisti());
            else
             cell.setCellValue("");
            columnCount++;
            cell = row.createCell(columnCount);
            if (movimento.getNumeroPmPrevisti()!=null)
             cell.setCellValue(movimento.getImportoTotDem());
            else
             cell.setCellValue("");
            columnCount++;

            if (reportName.equalsIgnoreCase(ReportCache.REPORT_IMPORTI_RICALCOLATI)) {
                cell = row.createCell(columnCount);
                cell.setCellValue(movimento.getImportoRicalcolato());
                columnCount++;
                cell = row.createCell(columnCount);
                if (movimento.getImportoRicalcolatoSingolaCedola()!=null)
                 cell.setCellValue(movimento.getImportoRicalcolatoSingolaCedola());
                else
                 cell.setCellValue("");
                columnCount++;
            }

            cell = row.createCell(columnCount);
            cell.setCellValue(movimento.getImportoEvento());
            columnCount++;

            if (reportName.equalsIgnoreCase(ReportCache.REPORT_IMPORTI_SOSPESI)) {
                cell = row.createCell(columnCount);
                cell.setCellValue(movimento.getMotivazioneSospensione());
            }

            if (reportName.equalsIgnoreCase(ReportCache.REPORT_IMPORTI_ERRORE)) {
                cell = row.createCell(columnCount);
                cell.setCellValue(movimento.getMotivazioneSospensione());
            }

        }

        return workbook;

    }


    public SXSSFWorkbook createReportTracciamentoPM(){

        SXSSFWorkbook workbook = new SXSSFWorkbook();

        SXSSFSheet sheet = workbook.createSheet("Tracciamento PM");

        ReportPage reportPage = ReportCache.getReportForExport(ReportCache.REPORT_TRACCIAMENTO_PM);

        int rowCount = 0;
        int columnCount = 0;

        // Intestazione Report
        SXSSFRow row = sheet.createRow(rowCount);
        SXSSFCell cell = row.createCell(columnCount);
        cell.setCellValue("Numero Programma");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Data");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Stato");


        ProgrammaMusicale programma = null;
        List<Object> entrys = reportPage.getAllRecords();
        for (Object e:entrys){
            programma = (ProgrammaMusicale)e;
            rowCount++;
            columnCount=0;

            row = sheet.createRow(rowCount);
            cell = row.createCell(columnCount);
            if (programma.getNumeroProgrammaMusicale()!=null)
             cell.setCellValue(programma.getNumeroProgrammaMusicale());
            else
              cell.setCellValue("");
            columnCount++;
            cell = row.createCell(columnCount);
            if (programma.getDataPassaggioStato()!=null)
             cell.setCellValue(programma.getDataPassaggioStato());
            else
             cell.setCellValue("");
            columnCount++;
            cell = row.createCell(columnCount);
            if (programma.getStato()!=null)
              cell.setCellValue(programma.getStato());
            else
              cell.setCellValue("");
        }

        return workbook;

    }

    public SXSSFWorkbook createReportAggregatoSIADA(){

        SXSSFWorkbook workbook = new SXSSFWorkbook();

        SXSSFSheet sheet = workbook.createSheet("Dati");

        ReportPage reportPage = ReportCache.getReportForExport(ReportCache.REPORT_VERIFICA_PM_SIADA);

        int rowCount = 0;
        int columnCount = 0;

        // Intestazione Report
        SXSSFRow row = sheet.createRow(rowCount);
        SXSSFCell cell = row.createCell(columnCount);
        cell.setCellValue("Data inoltro a SIADA");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Mese Contabile");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Voce Incasso");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Voce Incasso SIADA");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Numero Programma Musicale");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Numero Fattura");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("ID Evento");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Inizio Evento");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Fine Evento");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Importo");

        MovimentoSiada movimento = null;
        List<Object> entrys = reportPage.getAllRecords();
        for (Object e:entrys){
            movimento = (MovimentoSiada)e;
            rowCount++;
            columnCount=0;

            row = sheet.createRow(rowCount);
            cell = row.createCell(columnCount);
            cell.setCellValue(movimento.getDataElaborazioneStr());
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(movimento.getMeseContabile());
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(movimento.getVoceIncasso());
            columnCount++;
            cell = row.createCell(columnCount);
            if (movimento.getVoceIncassoSiada()!=null)
             cell.setCellValue(movimento.getVoceIncassoSiada());
            else
             cell.setCellValue("");
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(movimento.getNumeroProgrammaMusicale());
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(movimento.getNumeroFattura());
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(movimento.getIdEvento());
            columnCount++;
            cell = row.createCell(columnCount);
            if (movimento.getDataInizioEventoStr()!=null)
                cell.setCellValue(movimento.getDataInizioEventoStr());
            else
                cell.setCellValue("");
            columnCount++;
            cell = row.createCell(columnCount);
            if (movimento.getDataFineEventoStr()!=null)
             cell.setCellValue(movimento.getDataFineEventoStr());
            else
             cell.setCellValue("");
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(movimento.getImporto());
        }

        return workbook;

    }


    public SXSSFWorkbook createReportDettaglioPM(Long idPm){

        SXSSFWorkbook workbook = new SXSSFWorkbook();

        ProgrammaMusicale programmaMusicale = programmaMusicale = programmaMusicaleService.getDettaglioPM(idPm);

        SXSSFSheet sheet = workbook.createSheet("Dettaglio Programma Musicale");

        int rowCount = 0;
        int columnCount = 0;

        // Intestazione Report
        SXSSFRow row = sheet.createRow(rowCount);
        SXSSFCell cell = row.createCell(columnCount);
        cell.setCellValue("Riepilogo Programma Musicale");
        rowCount++;
        rowCount++;
        columnCount=0;
        row = sheet.createRow(rowCount);
        cell = row.createCell(columnCount);
        cell.setCellValue("Numero Programma");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue(programmaMusicale.getNumeroProgrammaMusicale());
        columnCount++;
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Voce Incasso");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue(programmaMusicale.getVoceIncasso());
        rowCount++;
        columnCount=0;
        row = sheet.createRow(rowCount);
        cell = row.createCell(columnCount);
        cell.setCellValue("Tipo supporto");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue(programmaMusicale.getSupportoPm());
        columnCount++;
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Tipo");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue(programmaMusicale.getTipoPm());
        rowCount++;
        columnCount=0;
        row = sheet.createRow(rowCount);
        cell = row.createCell(columnCount);
        cell.setCellValue("Flag campionamento");
        columnCount++;
        cell = row.createCell(columnCount);
        if (programmaMusicale.getCodiceCampionamento()!=null)
          cell.setCellValue("Si");
        else
            cell.setCellValue("");
        columnCount++;
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Codice campionamento");
        columnCount++;
        cell = row.createCell(columnCount);
        if (programmaMusicale.getCodiceCampionamento()!=null)
          cell.setCellValue(programmaMusicale.getCodiceCampionamento());
        else
          cell.setCellValue("");
        rowCount++;
        columnCount=0;
        row = sheet.createRow(rowCount);
        cell = row.createCell(columnCount);
        cell.setCellValue("Risultato calcolo campionamento");
        columnCount++;
        cell = row.createCell(columnCount);
        if (programmaMusicale.getRisultatoCalcoloResto()!=null)
         cell.setCellValue(programmaMusicale.getRisultatoCalcoloResto());
        else
         cell.setCellValue("");
        columnCount++;
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Totale durata cedole");
        columnCount++;
        cell = row.createCell(columnCount);
        if (programmaMusicale.getTotaleDurataCedole()!=null)
         cell.setCellValue(programmaMusicale.getTotaleDurataCedole());
        else
         cell.setCellValue("");
        rowCount++;
        columnCount=0;
        row = sheet.createRow(rowCount);
        cell = row.createCell(columnCount);
        cell.setCellValue("Totale cedole");
        columnCount++;
        cell = row.createCell(columnCount);
        if (programmaMusicale.getTotaleCedole()!=null)
         cell.setCellValue(programmaMusicale.getTotaleCedole());
        else
            cell.setCellValue("");
        columnCount++;
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Programma foglio segue di:");
        columnCount++;
        cell = row.createCell(columnCount);
        if (programmaMusicale.getPmRiferimento()!=null)
         cell.setCellValue(programmaMusicale.getPmRiferimento());
        else
            cell.setCellValue("");
        rowCount++;
        columnCount=0;
        row = sheet.createRow(rowCount);
        cell = row.createCell(columnCount);
        cell.setCellValue("Data rientro");
        columnCount++;
        cell = row.createCell(columnCount);
        if (programmaMusicale.getDataRientroPM()!=null)
         cell.setCellValue(programmaMusicale.getDataRientroPM());
        else
         cell.setCellValue("");
        columnCount++;
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Direttore Esecuzione");
        columnCount++;
        cell = row.createCell(columnCount);
        if (programmaMusicale.getDirettoreEsecuzione()!=null && programmaMusicale.getDirettoreEsecuzione().getDirettoreEsecuzione()!=null)
         cell.setCellValue(programmaMusicale.getDirettoreEsecuzione().getDirettoreEsecuzione());
        else
         cell.setCellValue("");
        rowCount++;
        rowCount++;
        columnCount=0;


        row = sheet.createRow(rowCount);
        cell = row.createCell(columnCount);
        cell.setCellValue("Riepilogo Evento");
        rowCount++;
        rowCount++;

        columnCount=0;
        row = sheet.createRow(rowCount);
        cell = row.createCell(columnCount);
        cell.setCellValue("Seprag punto territoriale");
        columnCount++;
        cell = row.createCell(columnCount);
        if (programmaMusicale.getCartella()!=null && programmaMusicale.getCartella().getSeprag()!=null && programmaMusicale.getCartella().getDescrizionePuntoTerritoriale()!=null)
          cell.setCellValue(programmaMusicale.getCartella().getSeprag()+" - "+programmaMusicale.getCartella().getDescrizionePuntoTerritoriale());
        else
           cell.setCellValue("");
        columnCount++;
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Locale");
        columnCount++;
        cell = row.createCell(columnCount);
        if (programmaMusicale.getEventi()!=null && programmaMusicale.getEventi().size()>0)
          cell.setCellValue(programmaMusicale.getEventi().get(0).getDenominazioneLocale());
        else
          cell.setCellValue("");

        rowCount++;
        columnCount=0;
        row = sheet.createRow(rowCount);
        cell = row.createCell(columnCount);
        cell.setCellValue("Codice locale");
        columnCount++;
        cell = row.createCell(columnCount);
        if (programmaMusicale.getEventi()!=null)
         cell.setCellValue(programmaMusicale.getEventi().get(0).getCodiceLocale());
        else
            cell.setCellValue("");
        columnCount++;
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("LocalitÃ ");
        columnCount++;
        cell = row.createCell(columnCount);
        if (programmaMusicale.getEventi()!=null && programmaMusicale.getEventi().size()>0)
         cell.setCellValue(programmaMusicale.getEventi().get(0).getDenominazioneLocalita());
        else
            cell.setCellValue("");

        rowCount++;
        columnCount=0;
        row = sheet.createRow(rowCount);
        cell = row.createCell(columnCount);
        cell.setCellValue("Provincia");
        columnCount++;
        cell = row.createCell(columnCount);
        if (programmaMusicale.getEventi()!=null && programmaMusicale.getEventi().size()>0)
         cell.setCellValue(programmaMusicale.getEventi().get(0).getSiglaProvinciaLocale());
        else
          cell.setCellValue("");

        rowCount++;



        for (int i=0; i<programmaMusicale.getEventi().size(); i++){
            rowCount++;
            columnCount=0;
            row = sheet.createRow(rowCount);
            cell = row.createCell(columnCount);
            cell.setCellValue("Data inizio evento");
            columnCount++;
            cell = row.createCell(columnCount);
            if (programmaMusicale.getEventi().get(i).getDataInizioEventoStr()!=null)
             cell.setCellValue(programmaMusicale.getEventi().get(i).getDataInizioEventoStr());
            else
             cell.setCellValue("");
            columnCount++;
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue("Data fine evento");
            columnCount++;
            cell = row.createCell(columnCount);
            if (programmaMusicale.getEventi().get(i).getDataFineEventoStr()!=null)
             cell.setCellValue(programmaMusicale.getEventi().get(i).getDataFineEventoStr());
            else
             cell.setCellValue("");

            rowCount++;
            columnCount=0;
            row = sheet.createRow(rowCount);

            cell = row.createCell(columnCount);
            cell.setCellValue("Ora inizio evento");
            columnCount++;
            cell = row.createCell(columnCount);
            if (programmaMusicale.getEventi().get(i).getOraInizioEvento()!=null)
             cell.setCellValue(programmaMusicale.getEventi().get(i).getOraInizioEvento());
            else
             cell.setCellValue("");
            columnCount++;
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue("Ora fine evento");
            columnCount++;
            cell = row.createCell(columnCount);
            if (programmaMusicale.getEventi().get(i).getOraFineEvento()!=null)
              cell.setCellValue(programmaMusicale.getEventi().get(i).getOraFineEvento());
            else
              cell.setCellValue("");
        }

        rowCount++;
        rowCount++;
        columnCount=0;

        row = sheet.createRow(rowCount);
        row = sheet.createRow(rowCount);
        cell = row.createCell(columnCount);
        cell.setCellValue("Riepilogo Movimenti");
        rowCount++;
        rowCount++;
        columnCount=0;
        row = sheet.createRow(rowCount);
        cell = row.createCell(columnCount);
        cell.setCellValue("Periodo rientro");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Evento");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Voce incasso");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Numero fattura");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Numero reversale");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Data reversale");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Numero PM");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Importo SUN");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Importo Ricalcolato");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Importo Manuale");
        MovimentoContabile movimento=null;

        for (int i=0; i<programmaMusicale.getMovimenti().size(); i++){
            movimento = programmaMusicale.getMovimenti().get(i);
            rowCount++;
            columnCount=0;
            row = sheet.createRow(rowCount);
            cell = row.createCell(columnCount);
            cell.setCellValue(movimento.getContabilita());
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(movimento.getEvento().getDataInizioEventoStr()+" "+movimento.getEvento().getOraInizioEvento());
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(movimento.getVoceIncasso());
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(movimento.getNumeroFattura());
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(movimento.getReversale());
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(movimento.getDataReversaleStr());
            columnCount++;
            cell = row.createCell(columnCount);
            if (movimento.getVoceIncasso().equalsIgnoreCase("2244") && movimento.getFlagGruppoPrincipale()=='1') {
                cell.setCellValue(movimento.getNumProgrammaMusicale() + " (Principale)");
            }
            if (movimento.getVoceIncasso().equalsIgnoreCase("2244") && movimento.getFlagGruppoPrincipale()=='0') {
                cell.setCellValue(movimento.getNumProgrammaMusicale() + " (Spalla)");
            }
            if (!movimento.getVoceIncasso().equalsIgnoreCase("2244")) {
                cell.setCellValue(movimento.getNumProgrammaMusicale());
            }
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(movimento.getImportoTotDem());
            columnCount++;
            cell = row.createCell(columnCount);
            if (movimento.getImportiRicalcolati()!=null && movimento.getImportiRicalcolati().size()>0) {
                cell.setCellValue(movimento.getImportiRicalcolati().get(0).getImporto());
            }
            columnCount++;
            cell = row.createCell(columnCount);
            if (movimento.getImportoManuale()!=null)
             cell.setCellValue(movimento.getImportoManuale());

        }

        rowCount++;
        columnCount=0;
        row = sheet.createRow(rowCount);
        cell = row.createCell(columnCount);
        cell.setCellValue("TOTALE");
        columnCount=columnCount+7;
        cell = row.createCell(columnCount);
        if (programmaMusicale.getImporto().getImportoAggregatoSUN()!=null) {
            cell.setCellValue(programmaMusicale.getImporto().getImportoAggregatoSUN());
        }else{
            cell.setCellValue("0");
        }
        columnCount++;
        cell = row.createCell(columnCount);
        if (programmaMusicale.getImporto()!=null && programmaMusicale.getImporto().getImportoTotaleRicalcolato()!=null) {
            cell.setCellValue(programmaMusicale.getImporto().getImportoTotaleRicalcolato());
        }else{
            cell.setCellValue("0");
        }

        rowCount++;
        rowCount++;
        columnCount=0;

        row = sheet.createRow(rowCount);
        cell = row.createCell(columnCount);
        cell.setCellValue("Lista Opere");
        rowCount++;
        rowCount++;
        columnCount=0;
        row = sheet.createRow(rowCount);
        cell = row.createCell(columnCount);
        cell.setCellValue("Codice");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Titolo");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Compositore");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Durata");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Durata meno 30 sec");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Importo Singola Cedola");

        Utilizzazione opera=null;

        for (int i=0; i<programmaMusicale.getOpere().size(); i++){
            opera = programmaMusicale.getOpere().get(i);
            rowCount++;
            columnCount=0;
            row = sheet.createRow(rowCount);
            cell = row.createCell(columnCount);
            if (opera.getCodiceOpera()!=null)
              cell.setCellValue(opera.getCodiceOpera());
            else
              cell.setCellValue("");
            columnCount++;
            cell = row.createCell(columnCount);
            if (opera.getTitoloComposizione()!=null)
              cell.setCellValue(opera.getTitoloComposizione());
            else
              cell.setCellValue("");
            columnCount++;
            cell = row.createCell(columnCount);
            if (opera.getCompositore()!=null)
             cell.setCellValue(opera.getCompositore());
            else
             cell.setCellValue("");
            columnCount++;
            cell = row.createCell(columnCount);
            if (opera.getDurata()!=null)
              cell.setCellValue(opera.getDurata());
            else
              cell.setCellValue("");
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(movimento.getReversale());
            columnCount++;
            cell = row.createCell(columnCount);
            //TODO: see if needs
//            if (opera.getDurataInferiore60sec()!=null)
//              cell.setCellValue(opera.getDurataInferiore60sec());
//            else
//              cell.setCellValue("");
            columnCount++;
            cell = row.createCell(columnCount);
            if (opera.getImportoCedola()!=null)
              cell.setCellValue(opera.getImportoCedola());
            else
              cell.setCellValue("");
        }


        return workbook;

    }


    public SXSSFWorkbook createReportDettaglioMovimento(Long idMovimento){

        SXSSFWorkbook workbook = new SXSSFWorkbook();

        SXSSFSheet sheet = workbook.createSheet("Dati");

        List<EventiPagati> eventoPagato = programmaMusicaleService.getDettagliMovimento(idMovimento);

        /*
        List<EventiPagati> eventoPagato = new ArrayList<EventiPagati>();

        EventiPagati evento = new EventiPagati();
        evento.setIdEvento(252525L);
        evento.setSeprag("1700081");
        evento.setDenominazioneLocale("Cicerone Local");
        evento.setCodiceBaLocale("1234");
        evento.setCodiceSpeiLocale("09333");
        evento.setDataInizioEvento(new Date());
        evento.setOraInizioEvento("22:00");

        evento.setContabilita(201611);
        evento.setVoceIncasso("2265");
        evento.setNumeroFattura("4525134534635");
        evento.setImportDem(100.50);
        evento.setTipoDocumentoContabile("501");

        Manifestazione manifestazione=new Manifestazione();
        manifestazione.setDataFineEvento(new Date());
        manifestazione.setOraFineEvento("23:59");
        manifestazione.setSiglaProvinciaLocale("LT");

        List<DettaglioPM> dettagliPMs = new ArrayList<DettaglioPM>();
        DettaglioPM dettaglioPM = new DettaglioPM();
        dettaglioPM.setVoceIncasso("2265");
        dettaglioPM.setPmPrevisti(4L);
        dettaglioPM.setPmPrevistiSpalla(0L);
        dettaglioPM.setPmRientrati(3L);
        dettaglioPM.setPmRientratiSpalla(0L);
        dettaglioPM.setImportoDisponibile(20D);
        dettaglioPM.setImportoAssegnato(80.50);
        dettagliPMs.add(dettaglioPM);
        evento.setDettaglioPM(dettagliPMs);
        evento.setManifestazione(manifestazione);

        List<MovimentoContabile> movimentiPMs = new ArrayList<MovimentoContabile>();
        MovimentoContabile movimentoContabile = new MovimentoContabile();
        movimentoContabile.setContabilita(201611);
        movimentoContabile.setVoceIncasso("2365");
        movimentoContabile.setNumeroFattura(54525345543L);
        movimentoContabile.setNumProgrammaMusicale("9171635261");
        movimentoContabile.setNumeroPermesso("20173243234");
        movimentoContabile.setImportoTotDem(100.5);
        movimentoContabile.setImportoRicalcolato(100D);
        movimentiPMs.add(movimentoContabile);
        evento.setMovimentazioniPM(movimentiPMs);

        eventoPagato.add(evento);

       */

        int rowCount = 0;
        int columnCount = 0;

        // Intestazione Report
        SXSSFRow row = sheet.createRow(rowCount);
        SXSSFCell cell = row.createCell(columnCount);
        cell.setCellValue("Informazioni Evento");
        rowCount++;
        rowCount++;
        columnCount=0;
        row = sheet.createRow(rowCount);
        cell = row.createCell(columnCount);
        cell.setCellValue("ID Evento");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue(eventoPagato.get(0).getIdEvento());
        columnCount++;
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Seprag punto territoriale");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue(eventoPagato.get(0).getSeprag());
        rowCount++;
        columnCount=0;
        row = sheet.createRow(rowCount);
        cell = row.createCell(columnCount);
        cell.setCellValue("Locale");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue(eventoPagato.get(0).getDenominazioneLocale());
        columnCount++;
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Codice BA");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue(eventoPagato.get(0).getCodiceBaLocale());
        rowCount++;
        columnCount=0;
        row = sheet.createRow(rowCount);
        cell = row.createCell(columnCount);
        cell.setCellValue("Codice SPEI");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue(eventoPagato.get(0).getCodiceSpeiLocale());
        columnCount++;
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Inizio evento");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue(eventoPagato.get(0).getDataInizioEventoStr()+" "+eventoPagato.get(0).getOraInizioEvento());

        if (eventoPagato.get(0).getManifestazione()!=null){
            rowCount++;
            columnCount=0;
            row = sheet.createRow(rowCount);
            cell = row.createCell(columnCount);
            cell.setCellValue("Fine evento");
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(eventoPagato.get(0).getManifestazione().getDataFineEventoStr()+" "+eventoPagato.get(0).getManifestazione().getOraFineEvento());
            columnCount++;
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue("LocalitÃ ");
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(eventoPagato.get(0).getManifestazione().getDenominazioneLocalita());
            rowCount++;
            columnCount=0;
            row = sheet.createRow(rowCount);
            cell = row.createCell(columnCount);
            cell.setCellValue("Provincia");
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(eventoPagato.get(0).getManifestazione().getSiglaProvinciaLocale());
        }

        rowCount++;
        rowCount++;
        columnCount=0;
        row = sheet.createRow(rowCount);
        cell = row.createCell(columnCount);
        cell.setCellValue("Informazioni Contabili");
        rowCount++;
        rowCount++;
        columnCount=0;
        row = sheet.createRow(rowCount);

        cell = row.createCell(columnCount);
        cell.setCellValue("Periodo pagamento");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Voce incasso");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Numero fattura");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Importo DEM");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Tipologia documento");
        rowCount++;
        columnCount=0;

        for (EventiPagati e: eventoPagato){
            row = sheet.createRow(rowCount);

            cell = row.createCell(columnCount);
            if (e.getContabilita()!=null) {
                cell.setCellValue(e.getContabilita());
            }else{
                cell.setCellValue("");
            }
            columnCount++;
            cell = row.createCell(columnCount);
            if (e.getVoceIncasso()!=null) {
                cell.setCellValue(e.getVoceIncasso());
            }else{
                cell.setCellValue("");
            }
            columnCount++;
            cell = row.createCell(columnCount);
            if (e.getNumeroFattura()!=null) {
                cell.setCellValue(e.getNumeroFattura());
            }else{
                cell.setCellValue("");
            }
            columnCount++;
            cell = row.createCell(columnCount);
            if (e.getImportDem()!=null)
             cell.setCellValue(e.getImportDem());
            else
                cell.setCellValue("");
            columnCount++;
            cell = row.createCell(columnCount);
            if (e.getTipoDocumentoContabile()!=null && e.getTipoDocumentoContabile().equalsIgnoreCase("501")) {
                cell.setCellValue("Mod. 501 (Entrata)");
            }else
            if (e.getTipoDocumentoContabile()!=null && e.getTipoDocumentoContabile().equalsIgnoreCase("221")) {
                cell.setCellValue("Mod. 221 (Uscita)");
            }

            rowCount++;
        }

        rowCount++;
        columnCount=0;
        row = sheet.createRow(rowCount);
        cell = row.createCell(columnCount);
        cell.setCellValue("Programmi Musicali attesi/rientrati");
        rowCount++;
        rowCount++;
        columnCount=0;
        row = sheet.createRow(rowCount);

        cell = row.createCell(columnCount);
        cell.setCellValue("Voce incasso");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("PM Attesi");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("PM Rientrati");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Importo DEM Residuo");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Importo assegnato");
        rowCount++;
        columnCount=0;

        List<DettaglioPM> dettagliPM =eventoPagato.get(0).getDettaglioPM();

        for (DettaglioPM p: dettagliPM){
            row = sheet.createRow(rowCount);

            cell = row.createCell(columnCount);
            cell.setCellValue(p.getVoceIncasso());
            columnCount++;
            cell = row.createCell(columnCount);
            if (p.getVoceIncasso().equalsIgnoreCase("2244")) {
                cell.setCellValue(p.getPmPrevisti()+" (Principale) "+p.getPmPrevistiSpalla()+" (Spalla)");
            }else
            if (!p.getVoceIncasso().equalsIgnoreCase("2244")) {
                cell.setCellValue(p.getPmPrevisti());
            }
            columnCount++;
            cell = row.createCell(columnCount);
            if (p.getVoceIncasso().equalsIgnoreCase("2244")) {
                cell.setCellValue(p.getPmRientrati()+" (Principale) "+p.getPmRientratiSpalla()+" (Spalla)");
            }else
            if (!p.getVoceIncasso().equalsIgnoreCase("2244")) {
                cell.setCellValue(p.getPmRientrati());
            }
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(p.getImportoDisponibile());
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(p.getImportoAssegnato());

            rowCount++;
        }

        rowCount++;
        columnCount=0;
        row = sheet.createRow(rowCount);
        cell = row.createCell(columnCount);
        cell.setCellValue("Dettaglio movimentazioni Programmi Musicali");
        rowCount++;
        rowCount++;
        columnCount=0;

        row = sheet.createRow(rowCount);

        cell = row.createCell(columnCount);
        cell.setCellValue("Mese Rendicontazione");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Voce incasso");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Numero fattura");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Numero");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Permesso");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Importo SUN");
        columnCount++;
        cell = row.createCell(columnCount);
        cell.setCellValue("Importo");
        rowCount++;


        List<MovimentoContabile> movimentiPM =eventoPagato.get(0).getMovimentazioniPM();

        for (MovimentoContabile m: movimentiPM){

            columnCount=0;
            row = sheet.createRow(rowCount);

            cell = row.createCell(columnCount);
            cell.setCellValue(m.getContabilita());
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(m.getVoceIncasso());
            columnCount++;
            cell = row.createCell(columnCount);
            cell.setCellValue(m.getNumeroFattura());
            columnCount++;
            cell = row.createCell(columnCount);
            if (m.getVoceIncasso().equalsIgnoreCase("2244") && m.getFlagGruppoPrincipale().equals("1")){
                cell.setCellValue(m.getNumProgrammaMusicale()+" (Principale)");
            }else
            if (m.getVoceIncasso().equalsIgnoreCase("2244") && m.getFlagGruppoPrincipale().equals("0")){
                cell.setCellValue(m.getNumProgrammaMusicale()+" (Spalla)");
            }else
            if (!m.getVoceIncasso().equalsIgnoreCase("2244")){
                cell.setCellValue(m.getNumProgrammaMusicale());
            }
            columnCount++;
            cell = row.createCell(columnCount);
            if (m.getNumeroPermesso()!=null)
             cell.setCellValue(m.getNumeroPermesso());
            else
             cell.setCellValue("");
            columnCount++;
            cell = row.createCell(columnCount);
            if (m.getImportoTotDem()!=null)
             cell.setCellValue(m.getImportoTotDem());
            else
             cell.setCellValue("");
            columnCount++;
            cell = row.createCell(columnCount);
            if (m.getImportiRicalcolati()!=null && m.getImportiRicalcolati().size()>0)
             cell.setCellValue(m.getImportiRicalcolati().get(0).getImporto());
            else
              cell.setCellValue("");

            rowCount++;
        }


        return workbook;

    }

    private String setValue(String value){
       if (value!=null)
           return value;
        else
           return "";
    }

    public void exportToExcel(String idReport, OutputStream outputStream) {

        try {

            SXSSFWorkbook workbook = null;

            if (idReport != null && idReport.equalsIgnoreCase(ReportCache.REPORT_LISTA_EVENTI)) {

                workbook = createReportListaEventi();

            }else
            if (idReport != null && idReport.equalsIgnoreCase(ReportCache.REPORT_LISTA_MOVIMENTI)) {

                workbook = createReportListaMovimenti();

            }else
            if (idReport != null && idReport.equalsIgnoreCase(ReportCache.REPORT_AGGREGATO_INCASSO)) {

                workbook = createReportAggregatoIncasso();

            }else
            if (idReport != null && idReport.equalsIgnoreCase(ReportCache.REPORT_AGGREGATO_MOVIMENTI)) {

                workbook = createReportAggregatoMovimenti();

            }else
            if (idReport != null && idReport.equalsIgnoreCase(ReportCache.REPORT_IMPORTI_RICALCOLATI)) {

                workbook = createReportImportiRicalcolati(ReportCache.REPORT_IMPORTI_RICALCOLATI);

            }else
            if (idReport != null && idReport.equalsIgnoreCase(ReportCache.REPORT_IMPORTI_SOSPESI)) {

                workbook = createReportImportiRicalcolati(ReportCache.REPORT_IMPORTI_SOSPESI);

            }else
            if (idReport != null && idReport.equalsIgnoreCase(ReportCache.REPORT_IMPORTI_ERRORE)) {

                workbook = createReportImportiRicalcolati(ReportCache.REPORT_IMPORTI_ERRORE);

            }else
            if (idReport != null && idReport.equalsIgnoreCase(ReportCache.REPORT_TRACCIAMENTO_PM)) {

                workbook = createReportTracciamentoPM();

            }else
            if (idReport != null && idReport.equalsIgnoreCase(ReportCache.REPORT_VERIFICA_PM_SIADA)) {

                workbook = createReportAggregatoSIADA();

            }

            workbook.write(outputStream);

        }catch(Exception e){

            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);

//            traceService.trace("Cruscotti", this.getClass().getName(), "exportToExcel", sw.toString(), Constants.ERROR);
        }
    }


    public void exportToExcelDetail(String idReport, String detailType, Long detailObjectId, OutputStream outputStream) {

        try {

            SXSSFWorkbook workbook = null;

            // Dettaglio PM
            if (detailType.equalsIgnoreCase("PM")) {

                workbook = createReportDettaglioPM(detailObjectId);

            }else
            // Dettaglio Movimento
            if (detailType.equalsIgnoreCase("MOV")) {

                workbook = createReportDettaglioMovimento(detailObjectId);

            }

            workbook.write(outputStream);

        }catch(Exception e){

            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);

//            traceService.trace("Cruscotti", this.getClass().getName(), "exportToExcel", sw.toString(), Constants.ERROR);
        }
    }

    public static void main(String[] args){
        CreateExcel createExcel = new CreateExcel();
        SXSSFWorkbook workbook = createExcel.createReportDettaglioMovimento(null);

        try {
            FileOutputStream outputStream = new FileOutputStream("/home/cimo/Temp/Dettaglio_Movimento.xlsx");
            workbook.write(outputStream);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }




}