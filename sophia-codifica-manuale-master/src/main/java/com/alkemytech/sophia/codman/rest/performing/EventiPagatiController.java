package com.alkemytech.sophia.codman.rest.performing;

import com.alkemytech.sophia.codman.rest.performing.service.EventiPagatiService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

@Singleton
@Path("eventi-pagati")
public class EventiPagatiController {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private Gson gson;
	private EventiPagatiService eventiPagatiService;

	@Inject
	protected EventiPagatiController(EventiPagatiService eventiPagatiService) {
		this.gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create();
		this.eventiPagatiService = eventiPagatiService;
	}

	@GET
	public String getList(@QueryParam(value = "agenzie") String agenzie) {
		return (gson).toJson(eventiPagatiService.getAgenzie(agenzie));
	}

	@GET
	@Path("voci-incasso")
	public String getVociIncasso(@QueryParam(value = "query") String search) {
		return (gson).toJson(eventiPagatiService.getVociIncasso(search));
	}

	@GET
	@Path("sedi")
	public String getSedi(@QueryParam(value = "query") String search) {
		return (gson).toJson(eventiPagatiService.getSedi(search));
	}

	@GET
	@Path("date-limits")
	public String getDateLimits() {
		return (gson).toJson(eventiPagatiService.getDateLimits());
	}
}