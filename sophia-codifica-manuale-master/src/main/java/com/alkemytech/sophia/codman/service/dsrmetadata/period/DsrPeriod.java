package com.alkemytech.sophia.codman.service.dsrmetadata.period;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DsrPeriod {
		
	public static final String MENSILE = "month";
	public static final String TRIMESTRALE = "quarter";
	
	private Date periodStartDate;
	private Date periodEndDate;
	
	private String year;
	private  String period;
	
	private String periodType;

//	private String startDateRepresentationYYYYMMDD;
	
//	private String endDateRepresentationYYYYMMDD;
	
	public Date getPeriodStartDate() {
		return periodStartDate;
	}

	public void setPeriodStartDate(Date periodStartDate) {
		this.periodStartDate = periodStartDate;
	}

	public Date getPeriodEndDate() {
		return periodEndDate;
	}

	public void setPeriodEndDate(Date periodEndDate) {
		this.periodEndDate = periodEndDate;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getPeriodType() {
		return periodType;
	}

	public void setPeriodType(String periodType) {
		this.periodType = periodType;
	}
	
	@Override
	public String toString(){
		SimpleDateFormat sdf  = new SimpleDateFormat("dd-MM-yyyy");
		
		StringBuffer sb = new StringBuffer();
		sb.append("[ PERIOD_TYPE: " + periodType );
		sb.append(" - YEAR: " + year );
		sb.append(" - PERIOD: " + period );
		sb.append(" - START_DATE: " + sdf.format(periodStartDate) );
		sb.append(" - END_DATE: " + sdf.format(periodEndDate) );
		sb.append(" ]");
		
		return sb.toString();
	}

	public String getStartDateRepresentationYYYYMMDD() {
		  Calendar cal = Calendar.getInstance();
		    cal.setTime(periodStartDate);
		    int year = cal.get(Calendar.YEAR);
		    int month = cal.get(Calendar.MONTH);
		    int day = cal.get(Calendar.DAY_OF_MONTH);
		    month++;
		return year + "" + (month < 10? "0"+ month: month) + "" + (day < 10 ? "0" + day : day);
	}



	public String getEndDateRepresentationYYYYMMDD() {
		Calendar cal = Calendar.getInstance();
	    cal.setTime(periodEndDate);
	    int year = cal.get(Calendar.YEAR);
	    int month = cal.get(Calendar.MONTH);
	    int day = cal.get(Calendar.DAY_OF_MONTH);
	    month++;
	    return year + "" + (month < 10? "0"+ month: month) + "" + (day < 10 ? "0" + day : day);
	}


	
	
}
