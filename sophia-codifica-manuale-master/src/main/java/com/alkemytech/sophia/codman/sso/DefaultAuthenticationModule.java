package com.alkemytech.sophia.codman.sso;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.common.net.IPv4Mask;
import com.alkemytech.sophia.common.tools.StringTools;

public class DefaultAuthenticationModule {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final String ssoDefaultUser;
	private final List<String> ssoDefaultGroups;
	private final IPv4Mask[] ssoDefaultNetmasks;
	private final Set<String> ssoDefaultAddresses;
	private final String[] ssoDefaultHttpHeaders;

	public DefaultAuthenticationModule(Properties configuration) {
		super();
		this.ssoDefaultUser = configuration.getProperty("sso.default.user");
		this.ssoDefaultGroups = Arrays.asList(StringTools.parseCsv(configuration.getProperty("sso.default.groups"), ",", true));
		this.ssoDefaultNetmasks = parseIPv4MasksCsv(configuration.getProperty("sso.default.netmasks"));
		this.ssoDefaultAddresses = parseAddressesCsv(configuration.getProperty("sso.default.addresses"));
		this.ssoDefaultHttpHeaders = StringTools.parseCsv(configuration.getProperty("sso.default.http_headers"), ",", true);
		logger.debug("<init>: user {}, groups {}, netmasks {}, addresses {}, headers {}",
				ssoDefaultUser, ssoDefaultGroups, ssoDefaultNetmasks, ssoDefaultAddresses, ssoDefaultHttpHeaders);
	}
	
	public IPv4Mask[] getSsoDefaultNetmasks() {
		return ssoDefaultNetmasks;
	}

	public Set<String> getSsoDefaultAddresses() {
		return ssoDefaultAddresses;
	}

	public String[] getSsoDefaultHttpHeaders() {
		return ssoDefaultHttpHeaders;
	}

	public String getSsoDefaultUser() {
		return ssoDefaultUser;
	}

	public List<String> getSsoDefaultGroups() {
		return ssoDefaultGroups;
	}

	private IPv4Mask[] parseIPv4MasksCsv(String ipv4MasksCsv) {
		final String[] csvItems = StringTools.parseCsv(ipv4MasksCsv, ",", true);
		if (null != csvItems) {
			final ArrayList<IPv4Mask> ipv4Masks = new ArrayList<IPv4Mask>();
			for (String csvItem : csvItems) {
				try {
					ipv4Masks.add(IPv4Mask.getIPv4Mask(csvItem));
				} catch (Exception e) {
					logger.debug("parseIPv4MasksCsv: invalid IPv4 netmask [{}]", csvItem);
				}
			}
			return ipv4Masks.toArray(new IPv4Mask[ipv4Masks.size()]);
		}
		return null;
	}
	
	private Set<String> parseAddressesCsv(String addressesCsv) {
		if (StringTools.isNullOrEmpty(addressesCsv)) {
			return null;
		}
		return new HashSet<String>(Arrays.asList(StringTools.parseCsv(addressesCsv, ",", true)));
	}
	
	private boolean isDefaultIP(String remoteAddr) {
		logger.debug("isDefaultIP: remoteAddr [{}]", remoteAddr);
		if (null != ssoDefaultAddresses) {
			if (ssoDefaultAddresses.contains(remoteAddr)) {
				return true;
			}
		}
		if (null != ssoDefaultNetmasks) {
			for (IPv4Mask ssoDefaultNetmask : ssoDefaultNetmasks) {
				try {
					if (ssoDefaultNetmask.matches(remoteAddr)) {
						return true;
					}
				} catch (Exception e) {
					// swallow
				}
			}
		}
		return false;
	}
	
	public boolean isDefaultIP(HttpServletRequest httpRequest) throws UnknownHostException {

		//aggiunto per verifica del path dei ruoli della JSESSIONID.
		//anche se chiamato in localhost devo sapere se il cookie è buono.
		try {
			if (httpRequest.getRequestURI().endsWith("/rest/user/roles"))
				return false;
		}catch(Exception e){}

		if (null != ssoDefaultHttpHeaders) {
			for (String headerName : ssoDefaultHttpHeaders) {
				String headerValue = httpRequest.getHeader(headerName);
				if (!StringTools.isNullOrEmpty(headerValue)) {
					logger.debug("isDefaultIP: {} [{}]", headerName, headerValue);
					final int comma = headerValue.lastIndexOf(',');
					if (-1 != comma) {
						headerValue = headerValue.substring(comma + 1).trim();
					}
					if (isDefaultIP(headerValue)) {
						return true;
					}
				}
			}
		}
		return isDefaultIP(httpRequest.getRemoteAddr());
	}
	
}
