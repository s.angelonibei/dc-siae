package com.alkemytech.sophia.codman.entity.performing.dao;

import com.alkemytech.sophia.codman.entity.performing.CampionamentoConfig;
import com.alkemytech.sophia.codman.entity.performing.ProgrammaMusicale;
import com.alkemytech.sophia.codman.entity.performing.TracciamentoApplicativo;
import com.alkemytech.sophia.codman.entity.performing.security.ReportPage;
import com.alkemytech.sophia.codman.rest.performing.Constants;
import com.alkemytech.sophia.codman.utils.DateUtils;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.persist.UnitOfWork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by idilello on 6/9/16.
 */
public class CampionamentoDAO implements ICampionamentoDAO{

	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	private Provider<EntityManager> provider;
	
    private UnitOfWork unitOfWork;
	
    @Inject
	public CampionamentoDAO(Provider<EntityManager> provider){
        this.provider = provider;
    }


     public CampionamentoConfig getCampionamentoConfig(Long contabilita){

         String query = "select c from CampionamentoConfig c where c.contabilita=:contabilita and c.flagAttivo='Y'";

         List<CampionamentoConfig> results = (List<CampionamentoConfig>)provider.get().createQuery(query)
                 .setParameter("contabilita",contabilita)
                 .getResultList();

         if (results!=null && results.size()>0)
             return results.get(0);
         else
             return null;

     }

    public CampionamentoConfig getCampionamentoConfigById(Long idCampionamentoConfig){

        String query = "select c from CampionamentoConfig c where c.id=:idCampionamentoConfig and c.flagAttivo='Y'";

        List<CampionamentoConfig> results = (List<CampionamentoConfig>)provider.get().createQuery(query)
                .setParameter("idCampionamentoConfig",idCampionamentoConfig)
                .getResultList();

        if (results!=null && results.size()>0)
            return results.get(0);
        else
            return null;

    }

    public void insertParametriCampionamento(CampionamentoConfig campionamentoConfig){
    		EntityManager entityManager=provider.get();

        CampionamentoConfig configurazionePrecedeente = getCampionamentoConfig(campionamentoConfig.getContabilita());

        if (configurazionePrecedeente!=null){
			entityManager.getTransaction().begin();
            configurazionePrecedeente.setFlagAttivo("N");
            entityManager.persist(configurazionePrecedeente);
			entityManager.getTransaction().commit();		
        }

        campionamentoConfig.setDataOraUltimaModifica(new Date());
        campionamentoConfig.setFlagAttivo("Y");

        try {
			entityManager.getTransaction().begin();
	        entityManager.persist(campionamentoConfig);
			entityManager.getTransaction().commit();			
		} catch (Exception ex) {
			logger.error("persist", ex);

			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
		}
    }


    public ReportPage getFlussoGuida(Long contabilitaIniziale, Long contabilitaFinale, Integer page){
         return getFlussoPM(contabilitaIniziale, contabilitaFinale, null, page);
    }

    public ReportPage getFlussoRientrati(Long contabilitaIniziale, Long contabilitaFinale, Integer page){
        return getFlussoPM(contabilitaIniziale, contabilitaFinale, Constants.INVIATO_A_NEED, page);
    }

    @Override
	public List<ProgrammaMusicale> getTotFlussoGuida(Long contabilitaIniziale, Long contabilitaFinale) {
        return getFlussoPM(contabilitaIniziale, contabilitaFinale);
	}


	private ReportPage getFlussoPM(Long contabilitaIniziale, Long contabilitaFinale, String statoPM, Integer page){

		EntityManager entityManager=provider.get();
        String query = null;

        List<ProgrammaMusicale> listaPM = new ArrayList<ProgrammaMusicale>();

        ReportPage reportPage = new ReportPage(listaPM, listaPM.size(), page);

        int recordCount=0;

        query = "select p.numero_Programma_Musicale,p.flag_Da_Campionare,p.pm_Riferimento,p.codice_voce_Incasso,p.id_Programma_Musicale, p.RISULTATO_CALCOLO_RESTO,p.codice_campionamento, l.stato \n" + 
        		"from \n" + 
        		" (select distinct(p.numero_Programma_Musicale), p.flag_Da_Campionare,p.pm_Riferimento,m.codice_voce_Incasso,p.id_Programma_Musicale,p.RISULTATO_CALCOLO_RESTO,p.codice_campionamento \n" + 
        		"  from PERF_PROGRAMMA_MUSICALE p, PERF_MOVIMENTO_CONTABILE m \n" + 
        		"  where m.contabilita>=? and m.contabilita<=?\n" + 
        		"  and m.id_Programma_Musicale=p.id_Programma_Musicale " +
        		"  and p.FLAG_DA_CAMPIONARE is not null \n" + 
        		" ) p left join PERF_MOVIMENTAZIONE_LOGISTICA_PM l on p.id_Programma_Musicale=l.id_Programma_Musicale \n" + 
        		" and l.flag_attivo=? and l.flag_escluso_invio_need=?;";


        Query q = entityManager.createNativeQuery(query);


        List<Object[]> results = (List<Object[]>)q
                            .setParameter(1,contabilitaIniziale)
                            .setParameter(2,contabilitaFinale)
                            .setParameter(3,'Y')
                            .setParameter(4,'N')
                            .getResultList();

        if (results != null && results.size() > 0) {

            recordCount = results.size();

            int startRecord = 0;
            int endRecord = recordCount;

            if (page > 0) {

                startRecord = (page) * ReportPage.NUMBER_RECORD_PER_PAGE;
                endRecord = 0;
                if (recordCount > ReportPage.NUMBER_RECORD_PER_PAGE) {
                    endRecord = startRecord + ReportPage.NUMBER_RECORD_PER_PAGE;
                    if (endRecord > recordCount)
                        endRecord = recordCount - 1;
                } else {
                    endRecord = recordCount;
                }

            }

            ProgrammaMusicale programma = null;
            String posizioneLogisticaPM = null;
            Character flag=null;
            for (int i = startRecord; i < endRecord; i++) {

                posizioneLogisticaPM = null;

                programma = new ProgrammaMusicale();

                programma.setNumeroProgrammaMusicale(((Long)results.get(i)[0]));
                if (results.get(i)[1]!=null) {
                    flag = ((String)results.get(i)[1]).charAt(0);
                    programma.setFlagDaCampionare(flag);
                }
                if (results.get(i)[2]!=null)
                programma.setPmRiferimento((String)results.get(i)[2]);
                programma.setVoceIncasso((String) results.get(i)[3]);
                programma.setId(((Long) results.get(i)[4]));
                if (results.get(i)[5]!=null)
                    programma.setRisultatoCalcoloResto(((Long) results.get(i)[5]));
                if (results.get(i)[6]!=null)
                    programma.setCodiceCampionamento(((Long) results.get(i)[6]));
                if (results.get(i)[7]!=null)
                    posizioneLogisticaPM = (String) results.get(i)[7];

                // Il PM non ha nessuno stato per cui può essere inviato a NEED
                if ( (statoPM==null && posizioneLogisticaPM==null) ||
                     (statoPM!=null && posizioneLogisticaPM!=null && posizioneLogisticaPM.equalsIgnoreCase(statoPM)))
                  listaPM.add(programma);
            }

            //recordCount = listaPM.size();

            reportPage = new ReportPage(listaPM, recordCount, page);

        }

        return reportPage ;

    }

	
	private List<ProgrammaMusicale> getFlussoPM(Long contabilitaIniziale, Long contabilitaFinale){

		EntityManager entityManager=provider.get();
        String query = null;

        List<ProgrammaMusicale> listaPM = new ArrayList<ProgrammaMusicale>();

        query = "select p.numero_Programma_Musicale,p.flag_Da_Campionare,p.pm_Riferimento,p.codice_voce_Incasso,p.id_Programma_Musicale, p.RISULTATO_CALCOLO_RESTO,p.codice_campionamento, l.stato \n" + 
        		"from \n" + 
        		" (select distinct(p.numero_Programma_Musicale), p.flag_Da_Campionare,p.pm_Riferimento,m.codice_voce_Incasso,p.id_Programma_Musicale,p.RISULTATO_CALCOLO_RESTO,p.codice_campionamento \n" + 
        		"  from PERF_PROGRAMMA_MUSICALE p, PERF_MOVIMENTO_CONTABILE m \n" + 
        		"  where m.contabilita>=? and m.contabilita<=?\n" + 
        		"  and m.id_Programma_Musicale=p.id_Programma_Musicale " +
        		"  and p.FLAG_DA_CAMPIONARE is not null \n" + 
        		" ) p left join PERF_MOVIMENTAZIONE_LOGISTICA_PM l on p.id_Programma_Musicale=l.id_Programma_Musicale \n" + 
        		" and l.flag_attivo=? and l.flag_escluso_invio_need=?;";


        Query q = entityManager.createNativeQuery(query);


        List<Object[]> results = (List<Object[]>)q
                            .setParameter(1,contabilitaIniziale)
                            .setParameter(2,contabilitaFinale)
                            .setParameter(3,'Y')
                            .setParameter(4,'N')
                            .getResultList();

        if (results != null && results.size() > 0) {

            ProgrammaMusicale programma = null;
            String posizioneLogisticaPM = null;
            Character flag=null;
            for (int i = 0; i < results.size(); i++) {

                posizioneLogisticaPM = null;

                programma = new ProgrammaMusicale();

                programma.setNumeroProgrammaMusicale(((Long)results.get(i)[0]));
                if (results.get(i)[1]!=null) {
                    flag = ((String)results.get(i)[1]).charAt(0);
                    programma.setFlagDaCampionare(flag);
                }
                if (results.get(i)[2]!=null)
                programma.setPmRiferimento((String)results.get(i)[2]);
                programma.setVoceIncasso((String) results.get(i)[3]);
                programma.setId(((Long) results.get(i)[4]));
                if (results.get(i)[5]!=null)
                    programma.setRisultatoCalcoloResto(((Long) results.get(i)[5]));
                if (results.get(i)[6]!=null)
                    programma.setCodiceCampionamento(((Long) results.get(i)[6]));
                if (results.get(i)[7]!=null)
                    posizioneLogisticaPM = (String) results.get(i)[7];
                  listaPM.add(programma);
            }

            //recordCount = listaPM.size();


        }

        return listaPM ;

    }
	
    public ReportPage getCampionamenti(Long contabilitaIniziale, Long contabilitaFinale, Integer page){

		EntityManager entityManager=provider.get();
        String query = null;

        List<CampionamentoConfig> results = new ArrayList<CampionamentoConfig>();

        ReportPage reportPage = new ReportPage(results, results.size(), page);

        int recordCount=0;

        query = "select c from CampionamentoConfig c where c.contabilita>=:contabilitaIniziale and c.contabilita<=:contabilitaFinale order by c.contabilita, c.flagAttivo desc";

        Query q = entityManager.createQuery(query);

        results = (List<CampionamentoConfig>)q
                .setParameter("contabilitaIniziale",contabilitaIniziale)
                .setParameter("contabilitaFinale",contabilitaFinale)
                .getResultList();

        if (results != null && results.size() > 0) {

            recordCount = results.size();

            int startRecord = (page-1) * ReportPage.NUMBER_RECORD_PER_PAGE;
            int endRecord = 0;
            if (recordCount>ReportPage.NUMBER_RECORD_PER_PAGE) {
                endRecord = startRecord + ReportPage.NUMBER_RECORD_PER_PAGE;
                if (endRecord > recordCount)
                    endRecord = recordCount -1;
            }else {
                endRecord = recordCount;
            }

            reportPage = new ReportPage(results, recordCount, page);

        }

        return reportPage ;

    }

    public ReportPage getCampionamentiEsecuzione(Long contabilitaIniziale, Long contabilitaFinale, Integer page){

		EntityManager entityManager=provider.get();
        String query = null;

        List<CampionamentoConfig> results = new ArrayList<CampionamentoConfig>();

        ReportPage reportPage = new ReportPage(results, results.size(), page);

        int recordCount=0;

        query = "select c from CampionamentoConfig c where c.contabilita>=:contabilitaIniziale and c.contabilita<=:contabilitaFinale and c.flagAttivo='Y' order by c.contabilita, c.flagAttivo desc";

        Query q = entityManager.createQuery(query);

        results = (List<CampionamentoConfig>)q
                .setParameter("contabilitaIniziale",contabilitaIniziale)
                .setParameter("contabilitaFinale",contabilitaFinale)
                .getResultList();

        if (results != null && results.size() > 0) {

            recordCount = results.size();

            int startRecord = (page-1) * ReportPage.NUMBER_RECORD_PER_PAGE;
            int endRecord = 0;
            if (recordCount>ReportPage.NUMBER_RECORD_PER_PAGE) {
                endRecord = startRecord + ReportPage.NUMBER_RECORD_PER_PAGE;
                if (endRecord > recordCount)
                    endRecord = recordCount -1;
            }else {
                endRecord = recordCount;
            }

            reportPage = new ReportPage(results, recordCount, page);

        }

        return reportPage ;

    }

    public ReportPage getStoricoEsecuzioni(Long periodoIniziale, Long periodoFinale, Integer page){

		EntityManager entityManager=provider.get();
        String query = null;
        String serviceName="Campionamento";
        String methodName="/campionamentoEsecuzione";

        String periodoInizialeStr = periodoIniziale.toString()+"01";
        String periodoFinaleStr = periodoFinale.toString()+"01";

        Date initialData = DateUtils.stringToDate(periodoInizialeStr,"yyyyMMdd");
        Date finalData = DateUtils.stringToDate(periodoFinaleStr,"yyyyMMdd");


        List<TracciamentoApplicativo> results = new ArrayList<TracciamentoApplicativo>();

        ReportPage reportPage = new ReportPage(results, results.size(), page);

        int recordCount=0;

        query = "select c from TracciamentoApplicativo c "+
                "where c.serviceName=:serviceName and c.methodName=:methodName "+
                "and c.dataInserimento>=:periodoIniziale and c.dataInserimento<=:periodoFinale "+
                "order by c.dataInserimento desc";

        Query q = entityManager.createQuery(query);

        results = (List<TracciamentoApplicativo>)q
                .setParameter("serviceName",serviceName)
                .setParameter("methodName",methodName)
                .setParameter("periodoIniziale",initialData)
                .setParameter("periodoFinale",finalData)
                .getResultList();

        if (results != null && results.size() > 0) {

            recordCount = results.size();

            int startRecord = (page-1) * ReportPage.NUMBER_RECORD_PER_PAGE;
            int endRecord = 0;
            if (recordCount>ReportPage.NUMBER_RECORD_PER_PAGE) {
                endRecord = startRecord + ReportPage.NUMBER_RECORD_PER_PAGE;
                if (endRecord > recordCount)
                    endRecord = recordCount -1;
            }else {
                endRecord = recordCount;
            }

            reportPage = new ReportPage(results, recordCount, page);

        }

        return reportPage ;


    }

    public List<TracciamentoApplicativo> getEsecuzioniCampionamentoEngine(Long periodoIniziale, Long periodoFinale){

		EntityManager entityManager=provider.get();
        String query = null;
        String serviceName = "CampionamentoInstance";
        String methodName = "startCampionamento";
        String message = "Periodo contabile da:";

        String periodoInizialeStr = periodoIniziale.toString()+"01";
        String periodoFinaleStr = periodoFinale.toString()+"01";

        Date initialData = DateUtils.stringToDate(periodoInizialeStr,"yyyyMMdd");
        Date finalData = DateUtils.stringToDate(periodoFinaleStr,"yyyyMMdd");

        List<TracciamentoApplicativo> results = new ArrayList<TracciamentoApplicativo>();

        query = "select c from TracciamentoApplicativo c "+
                "where c.serviceName=:serviceName and c.methodName=:methodName "+
                "and c.dataInserimento>=:periodoIniziale and c.dataInserimento<=:periodoFinale "+
                "and c.message like :message "+
                "order by c.dataInserimento desc";

        Query q = entityManager.createQuery(query);

        results = (List<TracciamentoApplicativo>)q
                .setParameter("serviceName",serviceName)
                .setParameter("methodName",methodName)
                .setParameter("periodoIniziale",initialData)
                .setParameter("periodoFinale",finalData)
                .setParameter("message", "%"+message+"%")
                .getResultList();

        return results;

    }


    public List<TracciamentoApplicativo> getEsecuzioniCampionamentoStart(Long periodoIniziale, Long periodoFinale){

		EntityManager entityManager=provider.get();
        String query = null;
        String serviceName="Campionamento";
        String methodName="/campionamentoEsecuzione";

        String periodoInizialeStr = periodoIniziale.toString()+"01";
        String periodoFinaleStr = periodoFinale.toString()+"01";

        Date initialData = DateUtils.stringToDate(periodoInizialeStr,"yyyyMMdd");
        Date finalData = DateUtils.stringToDate(periodoFinaleStr,"yyyyMMdd");

        List<TracciamentoApplicativo> results = new ArrayList<TracciamentoApplicativo>();


        query = "select c from TracciamentoApplicativo c "+
                "where c.serviceName=:serviceName and c.methodName=:methodName "+
                "and c.dataInserimento>=:periodoIniziale and c.dataInserimento<=:periodoFinale "+
                "order by c.dataInserimento desc";

        Query q = entityManager.createQuery(query);

        results = (List<TracciamentoApplicativo>)q
                .setParameter("serviceName",serviceName)
                .setParameter("methodName",methodName)
                .setParameter("periodoIniziale",initialData)
                .setParameter("periodoFinale",finalData)
                .getResultList();

        return results;

    }

}

