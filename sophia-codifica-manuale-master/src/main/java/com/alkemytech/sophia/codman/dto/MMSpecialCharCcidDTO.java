package com.alkemytech.sophia.codman.dto;

import lombok.Data;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Data
@XmlRootElement
public class MMSpecialCharCcidDTO implements Serializable {

    private String  context;
    private String idDsp;
    private String charList;
    private Long titleLength;
    private String enabled;

}
