package com.alkemytech.sophia.codman.multimedialelocale.dto;

import java.io.Serializable;
import java.util.Date;

public class PeriodLimits implements Serializable {
    private Date minPeriod;
    private Date maxPeriod;

    public PeriodLimits() {
    }

    public PeriodLimits(Date minPeriod, Date maxPeriod) {
        this.minPeriod = minPeriod;
        this.maxPeriod = maxPeriod;
    }

    public Date getMinPeriod() {
        return minPeriod;
    }

    public void setMinPeriod(Date minPeriod) {
        this.minPeriod = minPeriod;
    }

    public Date getMaxPeriod() {
        return maxPeriod;
    }

    public void setMaxPeriod(Date maxPeriod) {
        this.maxPeriod = maxPeriod;
    }
}
