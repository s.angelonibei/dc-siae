package com.alkemytech.sophia.codman.dto.performing.csv;

import java.util.Date;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

public class BonificaProgrammaMusicaleListToCsv {

	@CsvBindByPosition(position = 0)
	@CsvBindByName
	private String origineRecord;

	@CsvBindByPosition(position = 1)
	@CsvBindByName
	private Long idProgrammaMusicale;

	@CsvBindByPosition(position = 2)
	@CsvBindByName
	private Long nProgrammaMusicale;

	@CsvBindByPosition(position = 3)
	@CsvBindByName
	private String tipoPm;

	@CsvBindByPosition(position = 4)
	@CsvBindByName
	private String supportoPm;

	@CsvBindByPosition(position = 5)
	@CsvBindByName
	private String foglio;

	@CsvBindByPosition(position = 6)
	@CsvBindByName
	private Date dataAssegnazione;

	@CsvBindByPosition(position = 7)
	@CsvBindByName
	private Date dataRestituzione;

	@CsvBindByPosition(position = 8)
	@CsvBindByName
	private Date dataCompilazione;

	@CsvBindByPosition(position = 9)
	@CsvBindByName
	private String statoPm;

	@CsvBindByPosition(position = 10)
	@CsvBindByName
	private Date dataAnnullamento;

	@CsvBindByPosition(position = 11)
	@CsvBindByName
	private String foglioSegueDi;

	@CsvBindByPosition(position = 12)
	@CsvBindByName
	private String pmRiferimento;

	@CsvBindByPosition(position = 13)
	@CsvBindByName
	private String causaAnnullamento;

	@CsvBindByPosition(position = 14)
	@CsvBindByName
	private String statoWeb;

	@CsvBindByPosition(position = 15)
	@CsvBindByName
	private Long totaleCedole;

	@CsvBindByPosition(position = 16)
	@CsvBindByName
	private Long totaleDurataCedole;

	@CsvBindByPosition(position = 17)
	@CsvBindByName
	private Long gioranteTrattenimento;

	@CsvBindByPosition(position = 18)
	@CsvBindByName
	private String flagGruppoPrincipale;

	public BonificaProgrammaMusicaleListToCsv() {
		super();
	}

	public BonificaProgrammaMusicaleListToCsv(Object[] row) {
		super();
		this.origineRecord = (String) row[0];
		this.idProgrammaMusicale = (Long) row[1];
		this.nProgrammaMusicale = (Long) row[2];
		this.tipoPm = (String) row[3];
		this.supportoPm = (String) row[4];
		this.foglio = (String) row[5];
		this.dataAssegnazione = (Date) row[6];
		this.dataRestituzione = (Date) row[7];
		this.dataCompilazione = (Date) row[8];
		this.statoPm = (String) row[9];
		this.dataAnnullamento = (Date) row[10];
		this.foglioSegueDi = (String) row[11];
		this.pmRiferimento = (String) row[12];
		this.causaAnnullamento = (String) row[13];
		this.statoWeb = (String) row[14];
		this.totaleCedole = (Long) row[15];
		this.totaleDurataCedole = (Long) row[16];
		this.gioranteTrattenimento = (Long) row[17];
		this.flagGruppoPrincipale = (String) row[18];
	}

	public String getOrigineRecord() {
		return origineRecord;
	}

	public void setOrigineRecord(String origineRecord) {
		this.origineRecord = origineRecord;
	}

	public Long getIdProgrammaMusicale() {
		return idProgrammaMusicale;
	}

	public void setIdProgrammaMusicale(Long idProgrammaMusicale) {
		this.idProgrammaMusicale = idProgrammaMusicale;
	}

	public Long getnProgrammaMusicale() {
		return nProgrammaMusicale;
	}

	public void setnProgrammaMusicale(Long nProgrammaMusicale) {
		this.nProgrammaMusicale = nProgrammaMusicale;
	}

	public String getTipoPm() {
		return tipoPm;
	}

	public void setTipoPm(String tipoPm) {
		this.tipoPm = tipoPm;
	}

	public String getSupportoPm() {
		return supportoPm;
	}

	public void setSupportoPm(String supportoPm) {
		this.supportoPm = supportoPm;
	}

	public String getFoglio() {
		return foglio;
	}

	public void setFoglio(String foglio) {
		this.foglio = foglio;
	}

	public Date getDataAssegnazione() {
		return dataAssegnazione;
	}

	public void setDataAssegnazione(Date dataAssegnazione) {
		this.dataAssegnazione = dataAssegnazione;
	}

	public Date getDataRestituzione() {
		return dataRestituzione;
	}

	public void setDataRestituzione(Date dataRestituzione) {
		this.dataRestituzione = dataRestituzione;
	}

	public Date getDataCompilazione() {
		return dataCompilazione;
	}

	public void setDataCompilazione(Date dataCompilazione) {
		this.dataCompilazione = dataCompilazione;
	}

	public String getStatoPm() {
		return statoPm;
	}

	public void setStatoPm(String statoPm) {
		this.statoPm = statoPm;
	}

	public Date getDataAnnullamento() {
		return dataAnnullamento;
	}

	public void setDataAnnullamento(Date dataAnnullamento) {
		this.dataAnnullamento = dataAnnullamento;
	}

	public String getFoglioSegueDi() {
		return foglioSegueDi;
	}

	public void setFoglioSegueDi(String foglioSegueDi) {
		this.foglioSegueDi = foglioSegueDi;
	}

	public String getPmRiferimento() {
		return pmRiferimento;
	}

	public void setPmRiferimento(String pmRiferimento) {
		this.pmRiferimento = pmRiferimento;
	}

	public String getCausaAnnullamento() {
		return causaAnnullamento;
	}

	public void setCausaAnnullamento(String causaAnnullamento) {
		this.causaAnnullamento = causaAnnullamento;
	}

	public String getStatoWeb() {
		return statoWeb;
	}

	public void setStatoWeb(String statoWeb) {
		this.statoWeb = statoWeb;
	}

	public Long getTotaleCedole() {
		return totaleCedole;
	}

	public void setTotaleCedole(Long totaleCedole) {
		this.totaleCedole = totaleCedole;
	}

	public Long getTotaleDurataCedole() {
		return totaleDurataCedole;
	}

	public void setTotaleDurataCedole(Long totaleDurataCedole) {
		this.totaleDurataCedole = totaleDurataCedole;
	}

	public Long getGioranteTrattenimento() {
		return gioranteTrattenimento;
	}

	public void setGioranteTrattenimento(Long gioranteTrattenimento) {
		this.gioranteTrattenimento = gioranteTrattenimento;
	}

	public String getFlagGruppoPrincipale() {
		return flagGruppoPrincipale;
	}

	public void setFlagGruppoPrincipale(String flagGruppoPrincipale) {
		this.flagGruppoPrincipale = flagGruppoPrincipale;
	}

	public String[] getMappingStrategy() {
		return new String[] { "Origine Record", "Id Programma Musicale", "Numero Programma Musicale", "Tipo Pm",
				"Supporto Pm", "Foglio", "Data Assegnazione", "Data Restituzione", "Data Compilazione", "Stato Pm",
				"Data Annullamento", "Foglio Segue di", "Pm Riferimento", "Causa Annullamento", "Stato Web",
				"Totale Cedole", "Totale Durata Cedole", "Giornate Trattenimento", "Flag Gruppo Principale" };
	}

}
