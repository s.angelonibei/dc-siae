package com.alkemytech.sophia.codman.multimedialelocale.repository;

import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.alkemytech.sophia.codman.multimedialelocale.dto.PeriodLimits;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.jooq.DSLContext;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

import java.sql.Date;

import static org.jooq.impl.DSL.*;

@Repository("DsrMetadataRepository")
public class DsrMetadataRepository implements IDsrMetadataRepository {
    private Provider<EntityManager> provider;
    private DSLContext jooq;

    @Inject
    public DsrMetadataRepository(Provider<EntityManager> provider,
                                 @McmdbDataSource DSLContext jooq) {
        this.provider = provider;
        this.jooq = jooq;
    }

    @Override
    public PeriodLimits getPeriodLimits(){
        return jooq.select(
                concat(concat(min(field("year")).cast(String.class),"-"),concat(lpad(min(field("period")).cast(String.class),2,'0').cast(String.class),"-01")).cast(Date.class).as("min_period"),
                concat(concat(max(field("year")).cast(String.class),"-"),concat(lpad(max(field("period")).cast(String.class),2,'0').cast(String.class),"-31")).cast(Date.class).as("max_period")
        ).from("DSR_METADATA")
        .where(field("year").isNotNull().and(field("period").isNotNull().and(field("period_type").eq("month"))))
        .fetchOneInto(PeriodLimits.class);
    }
}
