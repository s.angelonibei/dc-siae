package com.alkemytech.sophia.codman.rest.performing.service;

import javax.servlet.http.HttpServletRequest;

public interface ITraceService {

	public void trace(String serviceName, String className, String methodName, String message, String logLevel, String username);
}
