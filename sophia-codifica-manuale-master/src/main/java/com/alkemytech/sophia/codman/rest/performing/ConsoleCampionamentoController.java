package com.alkemytech.sophia.codman.rest.performing;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Date;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.codman.dto.PerfCampionamentoSaveDTO;
import com.alkemytech.sophia.codman.dto.PerformingFlussoGuidaDto;
import com.alkemytech.sophia.codman.dto.PerformingFlussoGuidaResponseDTO;
import com.alkemytech.sophia.codman.entity.performing.CampionamentoConfig;
import com.alkemytech.sophia.codman.entity.performing.InformazioniPM;
import com.alkemytech.sophia.codman.entity.performing.ProgrammaMusicale;
import com.alkemytech.sophia.codman.entity.performing.security.ReportPage;
import com.alkemytech.sophia.codman.rest.performing.service.ICampionamentoService;
import com.alkemytech.sophia.codman.rest.performing.service.IConfigurationService;
import com.alkemytech.sophia.codman.rest.performing.service.IConsoleRicalcoloService;
import com.alkemytech.sophia.codman.rest.performing.service.IProgrammaMusicaleService;
import com.alkemytech.sophia.codman.rest.performing.service.ITraceService;
import com.alkemytech.sophia.codman.utils.AccountingUtils;
import com.alkemytech.sophia.codman.utils.DateUtils;
import com.alkemytech.sophia.common.beantocsv.CustomMappingStrategy;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

@Singleton
@Path("performing/campionamento")
public class ConsoleCampionamentoController {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private ICampionamentoService campionamentoService;
	private ITraceService traceService;
	private IConsoleRicalcoloService consoleRicalcoloService;
	private IConfigurationService configurationService;
	private IProgrammaMusicaleService programmaMusicaleService;
	private Gson gson;

	@Inject
	protected ConsoleCampionamentoController(Gson gson, ICampionamentoService campionamentoService,
			ITraceService traceService, IConsoleRicalcoloService consoleRicalcoloService,
			IConfigurationService configurationService, IProgrammaMusicaleService programmaMusicaleService) {
		super();
		this.gson = gson;
		this.campionamentoService = campionamentoService;
		this.traceService = traceService;
		this.consoleRicalcoloService = consoleRicalcoloService;
		this.configurationService = configurationService;
		this.programmaMusicaleService = programmaMusicaleService;
	}

	@GET
	@Path("campionamentoConfigurazione")
	public Response campionamentoConfigurazione() {

		// traceService.trace("Campionamento", this.getClass().getName(),
		// "/campionamentoConfigurazione",
		// "Accesso menu Configurazione", Constants.CLICK);
		return Response.ok().build();
	}

	@GET
	@Path("campionamentoStorico")
	public Response campionamentoStorico() {
		// traceService.trace("Campionamento", this.getClass().getName(),
		// "/campionamentoStorico",
		// "Accesso visualizzazione parametri storici di campionamento",
		// Constants.CLICK);
		return Response.ok().build();
	}

	@POST
	@Path("campionamentoConfigurazioneSave")
	public Response campionamentoConfigurazioneSave(PerfCampionamentoSaveDTO perfCampionamentoSaveDTO) {

		CampionamentoConfig campionamentoCnfg = new CampionamentoConfig();

		campionamentoCnfg.setContabilita(Long.parseLong(perfCampionamentoSaveDTO.getAnnoPeriodoContabilita()
				+ (perfCampionamentoSaveDTO.getMesePeriodoContabilita().length() == 2
						? perfCampionamentoSaveDTO.getMesePeriodoContabilita()
						: "0" + perfCampionamentoSaveDTO.getMesePeriodoContabilita())));

		campionamentoCnfg.setDataEstrazioniLotto(DateUtils
				.stringToDate(perfCampionamentoSaveDTO.getDataEstrazioneLotto(), DateUtils.DATE_ITA_FORMAT_SLASH));
		campionamentoCnfg.setDataInizioLavorazione(DateUtils
				.stringToDate(perfCampionamentoSaveDTO.getDataInizioLavorazione(), DateUtils.DATE_ITA_FORMAT_SLASH));
		campionamentoCnfg.setEstrazioniRoma(perfCampionamentoSaveDTO.getEstrazioniRoma());
		campionamentoCnfg.setEstrazioniMilano(perfCampionamentoSaveDTO.getEstrazioniMilano());

		campionamentoCnfg.setRestoRoma1(Long.parseLong(perfCampionamentoSaveDTO.getRestoRoma1())); // Resto Modulo 5
		campionamentoCnfg.setRestoRoma2(Long.parseLong(perfCampionamentoSaveDTO.getRestoRoma2())); // Resto Modulo 61

		campionamentoCnfg.setRestoMilano1(Long.parseLong(perfCampionamentoSaveDTO.getRestoMilano1())); // Resto Modulo 5
		campionamentoCnfg.setRestoMilano2(Long.parseLong(perfCampionamentoSaveDTO.getRestoMilano2())); // Resto Modulo
																										// 61

		campionamentoCnfg.setUtenteUltimaModifica(perfCampionamentoSaveDTO.getUsername());

		try {
			campionamentoService.insertParametriCampionamento(campionamentoCnfg);
			return Response.ok().build();
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return Response.status(500).build();
	}

	@GET
	@Path("campionamentoStoricoLista")
	public Response campionamentoStoricoLista(
			@QueryParam("annoInizioPeriodoContabile") String annoInizioPeriodoContabile,
			@QueryParam("meseInizioPeriodoContabile") String meseInizioPeriodoContabile,
			@QueryParam("annoFinePeriodoContabile") String annoFinePeriodoContabile,
			@QueryParam("meseFinePeriodoContabile") String meseFinePeriodoContabile, @QueryParam("page") Integer page) {

		// traceService.trace("Campionamento", this.getClass().getName(),
		// "/campionamentoStoricoLista",
		// "Visualizzazione parametri storici di campionamento", Constants.CLICK);

		String inizioPeriodoContabile;
		String finePeriodoContabile;

		if (annoInizioPeriodoContabile == null || meseInizioPeriodoContabile == null || annoFinePeriodoContabile == null
				|| meseFinePeriodoContabile == null) {
			return Response.ok(gson.toJson("{\"message\":\"E' necessario selezionare un criterio di ricerca\""))
					.build();
		} else {

			if (meseInizioPeriodoContabile.length() == 1)
				inizioPeriodoContabile = annoInizioPeriodoContabile + "0" + meseInizioPeriodoContabile;
			else
				inizioPeriodoContabile = annoInizioPeriodoContabile + meseInizioPeriodoContabile;

			if (meseFinePeriodoContabile.length() == 1)
				finePeriodoContabile = annoFinePeriodoContabile + "0" + meseFinePeriodoContabile;
			else
				finePeriodoContabile = annoFinePeriodoContabile + meseFinePeriodoContabile;

			ReportPage resultPage = campionamentoService.getCampionamenti(Long.parseLong(inizioPeriodoContabile),
					Long.parseLong(finePeriodoContabile), page);
			if (resultPage==null||resultPage.getRecords()==null||resultPage.getRecords().size()==0) {
				return Response.status(500).entity("Non risulta nessun dato per il periodo selezionato").build();
			}
			return Response.ok(gson.toJson(resultPage)).build();
		}

	}

	@GET
	@Path("campionamentoStoricoEsecuzioni")
	public Response campionamentoStoricoEsecuzioni() {

		// traceService.trace("Campionamento", this.getClass().getName(),
		// "/campionamentoStoricoEsecuzioni",
		// "Accesso visualizzazione storico esecuzioni", Constants.CLICK);

		return Response.ok().build();
	}

	@GET
	@Path("campionamentoStoricoEsecuzioniLista")
	public Response campionamentoStoricoEsecuzioniLista(
			@QueryParam("annoInizioPeriodoContabile") String annoInizioPeriodoContabile,
			@QueryParam("meseInizioPeriodoContabile") String meseInizioPeriodoContabile,
			@QueryParam("annoFinePeriodoContabile") String annoFinePeriodoContabile,
			@QueryParam("meseFinePeriodoContabile") String meseFinePeriodoContabile, @QueryParam("page") Integer page) {

		// traceService.trace("Campionamento", this.getClass().getName(),
		// "/campionamentoStoricoEsecuzioniLista",
		// "Visualizzazione storico esecuzioni", Constants.CLICK);

		String inizioPeriodoContabile;
		String finePeriodoContabile;

		if (annoInizioPeriodoContabile == null || meseInizioPeriodoContabile == null || annoFinePeriodoContabile == null
				|| meseFinePeriodoContabile == null) {
			return Response.ok(gson.toJson("{\"message\":\"E' necessario selezionare un criterio di ricerca\""))
					.build();
		} else {

			if (meseInizioPeriodoContabile.length() == 1)
				inizioPeriodoContabile = annoInizioPeriodoContabile + "0" + meseInizioPeriodoContabile;
			else
				inizioPeriodoContabile = annoInizioPeriodoContabile + meseInizioPeriodoContabile;

			if (meseFinePeriodoContabile.length() == 1)
				finePeriodoContabile = annoFinePeriodoContabile + "0" + meseFinePeriodoContabile;
			else
				finePeriodoContabile = annoFinePeriodoContabile + meseFinePeriodoContabile;

			ReportPage resultPage = campionamentoService.getStoricoEsecuzioni(Long.parseLong(inizioPeriodoContabile),
					Long.parseLong(finePeriodoContabile), page);

			if (resultPage==null||resultPage.getRecords()==null||resultPage.getRecords().size()==0) {
				return Response.status(500).entity("Non risulta nessun dato per il periodo selezionato").build();
			}
			return Response.ok(gson.toJson(resultPage)).build();
		}

	}

	@GET
	@Path("campionamentoEsecuzione")
	public Response ricalcoloEsecuzione() {

		// traceService.trace("Campionamento", this.getClass().getName(),
		// "/campionamentoEsecuzione",
		// "Accesso esecuzione algoritmo di campionamento", Constants.CLICK, );

		String endDate = DateUtils.dateToString(new Date(), "yyyyMM");
		String startDate = DateUtils.dateToString(DateUtils.stringToDate("01/01/2016", DateUtils.DATE_ITA_FORMAT_SLASH),
				"yyyyMM");

		ReportPage resultPage = campionamentoService.getCampionamentiEsecuzione(Long.parseLong(startDate),
				Long.parseLong(endDate), 0);

		return Response.ok(gson.toJson(resultPage)).build();
	}

	@GET
	@Path("getAvanzamentoElaborazioneCampionamento")
	@Produces("application/json")
	public Response getAvanzamentoElaborazioneCampionamento() {

		try {

			InformazioniPM informazioni = new InformazioniPM();

			informazioni = consoleRicalcoloService.getAvanzamentoElaborazioneCampionamento();

			return Response.ok(gson.toJson(informazioni)).build();

		} catch (Exception e) {
			return Response.status(500).build();
		}

	}

	@GET
	@Path("campionamentoFlussoGuida")
	public Response campionamentoFlussoGuida() {

		// traceService.trace("Campionamento", this.getClass().getName(),
		// "/campionamentoFlussoGuida",
		// "Accesso esportazione flusso guida", Constants.CLICK);

		String currentYear = DateUtils.dateToString(new Date(), "yyyy");
		String currentMonth = DateUtils.dateToString(new Date(), "MM");
		String finePeriodoRicalcolo = configurationService.getParameter("FINE_PERIODO_RIPARTIZIONE");

		Long annoFinePeriodoRicalcolo = Long.parseLong(finePeriodoRicalcolo.substring(0, 4));
		Long meseFinePeriodoRicalcolo = Long.parseLong(finePeriodoRicalcolo.substring(4, 6));
		//
		// if (meseFinePeriodoRicalcolo == 12) {
		// model.addAttribute("meseInizioPeriodoContabile", "01");
		// model.addAttribute("annoInizioPeriodoContabile", annoFinePeriodoRicalcolo +
		// 1);
		// } else {
		// if (meseFinePeriodoRicalcolo < 9)
		// model.addAttribute("meseInizioPeriodoContabile", "0" +
		// meseFinePeriodoRicalcolo);
		// else
		// model.addAttribute("meseInizioPeriodoContabile", meseFinePeriodoRicalcolo);
		//
		// model.addAttribute("annoInizioPeriodoContabile", annoFinePeriodoRicalcolo);
		// }
		//
		// model.addAttribute("meseFinePeriodoContabile", currentMonth);
		// model.addAttribute("annoFinePeriodoContabile", currentYear);

		return Response.ok().build();
	}

	@POST
	@Path("viewFlussoGuida")
	@Produces("application/json")
	public Response viewFlussoGuida(PerformingFlussoGuidaDto performingFlussoGuidaDto) {

		// traceService.trace("Campionamento", this.getClass().getName(),
		// "/viewFlussoGuida",
		// "Visualizzazione flusso guida", Constants.CLICK);

		Long inizioPeriodoContabile = AccountingUtils.normalizePeriodoContabile(
				performingFlussoGuidaDto.getMeseInizioPeriodoContabile(),
				performingFlussoGuidaDto.getAnnoInizioPeriodoContabile());
		Long finePeriodoContabile = AccountingUtils.normalizePeriodoContabile(
				performingFlussoGuidaDto.getMeseFinePeriodoContabile(),
				performingFlussoGuidaDto.getAnnoFinePeriodoContabile());

		// String downloadPath = servletContext.getRealPath("/") + "/download/";

		ReportPage resultPage = programmaMusicaleService.getFlussoGuida(inizioPeriodoContabile, finePeriodoContabile,
				performingFlussoGuidaDto.getPage());

		// downloadPath = servletContext.getRealPath("/") + "/download/" +
		// resultPage.getFileName();

		PerformingFlussoGuidaResponseDTO performingFlussoGuidaResponseDTO = new PerformingFlussoGuidaResponseDTO();
		// performingFlussoGuidaResponseDTO.setDownloadPath(downloadPath);
		performingFlussoGuidaResponseDTO.setReportPage(resultPage);
		// model.addAttribute("downloadPath", downloadPath);
		// model.addAttribute("resultPage", resultPage);

		// ADD DOWNLOAD PATH
		if (resultPage==null||resultPage.getRecords()==null||resultPage.getRecords().size()==0) {
			return Response.status(500).entity("Non risulta nessun dato per il periodo selezionato").build();
		}
		return Response.ok(gson.toJson(resultPage)).build();
	}

	@POST
	@Path("downloadCSVFlussoGuida")
	@Produces("application/csv")
	public Response downloadCSVFlussoGuida(PerformingFlussoGuidaDto performingFlussoGuidaDto) throws IOException {

		String fileName = "FlussoGuida_" + DateUtils.dateToString(new Date(), DateUtils.DATETIME_ITA_FORMAT_SLASH);

		Long inizioPeriodoContabile = AccountingUtils.normalizePeriodoContabile(
				performingFlussoGuidaDto.getMeseInizioPeriodoContabile(),
				performingFlussoGuidaDto.getAnnoInizioPeriodoContabile());
		Long finePeriodoContabile = AccountingUtils.normalizePeriodoContabile(
				performingFlussoGuidaDto.getMeseFinePeriodoContabile(),
				performingFlussoGuidaDto.getAnnoFinePeriodoContabile());

		File file = File.createTempFile(fileName, ".csv");
		FileWriter writer = new FileWriter(file);

		List<ProgrammaMusicale> resultPage = programmaMusicaleService.getTotFlussoGuida(inizioPeriodoContabile, finePeriodoContabile);

		try {
			if (resultPage.size() != 0) {
				StreamingOutput so = null;
				CustomMappingStrategy<ProgrammaMusicale> customMappingStrategy = new CustomMappingStrategy<>(
						new ProgrammaMusicale().getMappingStrategy());
				customMappingStrategy.setType(ProgrammaMusicale.class);
				so = getStreamingOutput(resultPage, customMappingStrategy);
				Response response = Response.ok(so, "text/csv").header("Content-Disposition", "attachment; filename=\""
						+ "Flusso Contabile" + inizioPeriodoContabile + "-" + finePeriodoContabile + ".csv\"").build();
				return response;
			} else {
				return Response.status(500).entity("Non risulta nessun dato per il periodo selezionato").build();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			return Response.status(500).entity("Non risulta nessun dato per il periodo selezionato").build();
		} finally {
		}
	}

	/*
	 * @POST
	 * 
	 * @Path("fileupload") public Response processUpload(@RequestParam
	 * MultipartFile file) throws IOException {
	 * 
	 * traceService.trace("Campionamento", this.getClass().getName(), "/fileupload",
	 * "Upload flusso PM rientrati", Constants.CLICK);
	 * 
	 * String fileName = file.getOriginalFilename();
	 * 
	 * if (!file.isEmpty()) { try { byte[] bytes = file.getBytes();
	 * 
	 * File dir = new File("tmpFiles"); if (!dir.exists()) dir.mkdirs();
	 * 
	 * // Create the file on server File serverFile = new File(dir.getAbsolutePath()
	 * + File.separator + fileName);
	 * 
	 * BufferedOutputStream stream = new BufferedOutputStream(new
	 * FileOutputStream(serverFile)); stream.write(bytes); stream.close();
	 * 
	 * int numPMcaricati = programmaMusicaleService
	 * .storeFileContant(dir.getAbsolutePath() + File.separator + fileName);
	 * 
	 * if (numPMcaricati < 0) return Response.ok(gson.
	 * toJson("{\"message\":\"Problemi durante il caricamento del file. Verificare il formato\""
	 * )) .build(); else return
	 * Response.ok(gson.toJson("{\"message\":\"Acquisiti correttamente " +
	 * numPMcaricati + " programmi musicali presenti nel file\"")) .build();
	 * 
	 * 
	 * } catch (Exception e) { e.printStackTrace(); return Response.ok(gson.
	 * toJson("{\"message\":\"Errore durante il caricamento del file\"")) .build();
	 * } } else { return
	 * Response.ok(gson.toJson("{\"message\":\"File di dimensioni pari a 0\""))
	 * .build(); } }
	 */

	// parte di un'altro controller

	@Path("sendCommand")
	@GET
	@Produces("application/json")
	public Response sendCommand(@QueryParam("info") String info,
			@QueryParam("periodoContabile") String periodoContabile) {

		Response response = Response.ok("non avviato").build();

		try {

			// Acquisisce lo stato del processo di backend (ACTIVE/INACTIVE)
			if (info != null && info.equalsIgnoreCase(Constants.STATUS)) {

				String status = consoleRicalcoloService.checkStatus();

				response = Response.ok(gson.toJson(status)).build();
			} else if (info != null && info.equalsIgnoreCase(Constants.ACTIVATE)) {

				// traceService.trace("Ricalcolo", this.getClass().getName(), "/sendCommand",
				// "Inviato comando: "+Constants.ACTIVATE, Constants.CLICK);

				// Esegue lo script sheel per far partire il processo remoto
				String status = consoleRicalcoloService.startProcess();

				response = Response.ok(gson.toJson(status)).build();

			} else if (info != null && info.equalsIgnoreCase(Constants.DEACTIVATE)) {

				// traceService.trace("Ricalcolo", this.getClass().getName(), "/sendCommand",
				// "Inviato comando: "+Constants.DEACTIVATE, Constants.CLICK);

				String status = consoleRicalcoloService.deactivate();

				response = Response.ok(gson.toJson(status)).build();
			} else if (info != null && info.equalsIgnoreCase(Constants.START_RICALCOLO)) {

				// traceService.trace("Ricalcolo", this.getClass().getName(), "/sendCommand",
				// "Inviato comando: "+Constants.START_RICALCOLO, Constants.CLICK);

				String status = consoleRicalcoloService.startRicalcolo();

				response = Response.ok(gson.toJson(status)).build();
			} else if (info != null && info.equalsIgnoreCase(Constants.START_CAMPIONAMENTO)) {

				// traceService.trace("Ricalcolo", this.getClass().getName(), "/sendCommand",
				// "Inviato comando: "+Constants.START_CAMPIONAMENTO, Constants.CLICK);

				String status;
				if (periodoContabile != null && !periodoContabile.equalsIgnoreCase("")) {

					CampionamentoConfig parametriConfigurazione = campionamentoService
							.getCampionamentoConfigById(Long.parseLong(periodoContabile));

					configurationService.insertParameter("INIZIO_PERIODO_CAMPIONAMENTO",
							parametriConfigurazione.getContabilita().toString());
					configurationService.insertParameter("FINE_PERIODO_CAMPIONAMENTO",
							parametriConfigurazione.getContabilita().toString());

					status = consoleRicalcoloService.startCampionamento();

				} else {
					status = "KO";
				}
				response = Response.ok(gson.toJson(status)).build();
			}

		} catch (Exception e) {
			// response = Response.ok("info: " + info + " periodoContabile: " +
			// periodoContabile).build();
			response = Response.ok(gson.toJson("INACTIVE")).build();
			/*
			 * response=Utility.jsonNOK(e.getMessage());
			 * 
			 * StringWriter sw = new StringWriter(); PrintWriter pw = new PrintWriter(sw);
			 * e.printStackTrace(pw);
			 * 
			 * traceService.trace("Ricalcolo", this.getClass().getName(), "/sendCommand",
			 * sw.toString(), Constants.ERROR);
			 */
		}

		return response;
	}

	private <T> StreamingOutput getStreamingOutput(final List<T> obj, final CustomMappingStrategy mappingStrategy) {
		StreamingOutput so = new StreamingOutput() {
			@Override
			public void write(OutputStream outputStream) throws IOException, WebApplicationException {
				OutputStreamWriter osw = new OutputStreamWriter(outputStream);

				StatefulBeanToCsv beanToCsv = new StatefulBeanToCsvBuilder(osw).withMappingStrategy(mappingStrategy)
						.withSeparator(';').build();
				try {
					beanToCsv.write(obj);
					osw.flush();
					outputStream.flush();
				} catch (CsvDataTypeMismatchException e) {
					logger.error(e.getMessage());
				} catch (CsvRequiredFieldEmptyException e) {
					logger.error(e.getMessage());
				} finally {
					if (osw != null)
						osw.close();
				}
			}
		};
		return so;
	}

}
