package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;

public class UtilizationTypeDTO implements Serializable {

	public UtilizationTypeDTO() {

	}

	private static final long serialVersionUID = -6248355468161554809L;

	private String idUtilizationType;

	private String name;

	private String description;

	public String getIdUtilizationType() {
		return idUtilizationType;
	}

	public void setIdUtilizationType(String idUtilizationType) {
		this.idUtilizationType = idUtilizationType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}