package com.alkemytech.sophia.codman.entity;

import com.google.gson.GsonBuilder;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlRootElement
@Entity(name="DroolsPricing")
@Table(name="DROOLS_PRICING")
public class DroolsPricing {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID", nullable=false)
	private Long id;

	@Column(name="IDDSP", nullable=false)
	private String idDsp;
	
	@Column(name="ID_UTILIZATION_TYPE", nullable=false)
	private String idUtilizationType;

	@Column(name="VALID_FROM", nullable=false)
	private Date validFrom;

	@Column(name="VALID_TO")
	private Date validTo;

	@Column(name="XLS_S3_URL", nullable=false)
	private String xlsS3Url;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIdDsp() {
		return idDsp;
	}

	public void setIdDsp(String idDsp) {
		this.idDsp = idDsp;
	}

	public String getIdUtilizationType() {
		return idUtilizationType;
	}

	public void setIdUtilizationType(String idUtilizationType) {
		this.idUtilizationType = idUtilizationType;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public String getXlsS3Url() {
		return xlsS3Url;
	}

	public void setXlsS3Url(String xlsS3Url) {
		this.xlsS3Url = xlsS3Url;
	}
	
	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}
}
