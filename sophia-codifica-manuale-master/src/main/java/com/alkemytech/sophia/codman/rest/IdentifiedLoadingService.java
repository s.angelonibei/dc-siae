package com.alkemytech.sophia.codman.rest;

import com.alkemytech.sophia.codman.dto.UnidentifiedLoadingSearchResultDTO;
import com.alkemytech.sophia.codman.dto.UnidentifiedLoadingSubmitRequestDTO;
import com.alkemytech.sophia.codman.dto.UnidentifiedLoadingSubmitResponseDTO;
import com.alkemytech.sophia.codman.guice.UnidentifiedDataSource;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Singleton
@Path("identified-loading")
public class IdentifiedLoadingService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private Gson gson;
    private SQS sqs;
    private Properties configuration;

    private final Provider<EntityManager> uni;


    @Inject
    protected IdentifiedLoadingService(@UnidentifiedDataSource Provider<EntityManager> uni,
                                            SQS sqs,
                                            Gson gson,
                                            @Named("configuration") Properties configuration) {
        super();
        this.sqs=sqs;
        this.uni=uni;
        this.gson = gson;
        this.configuration=configuration;
    }

    @POST
    @Path("/submit-load-identified")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response submitLoadingCodificati(UnidentifiedLoadingSubmitRequestDTO request) throws IOException {
        try {
            UnidentifiedLoadingSubmitResponseDTO response = new UnidentifiedLoadingSubmitResponseDTO();
            for(UnidentifiedLoadingSearchResultDTO dto : request.getDsrs()) {

                String period="";
                if(dto.getDsrPeriod().getPeriodType().equals("quarter"))
                    period = "Q" + dto.getDsrPeriod().getPeriod();
                else
                    period = String.format("%02d", Integer.parseInt(dto.getDsrPeriod().getPeriod()));


                JsonObject messageBody = new JsonObject();
                messageBody.addProperty("idDsr",dto.getDsr());
                messageBody.addProperty("dsp",dto.getDspPath());
                messageBody.addProperty("year",dto.getDsrPeriod().getYear());
                messageBody.addProperty("month",period);
                messageBody.addProperty("country",dto.getCountryName());
//				messageBody.addProperty("checkAlreadyProcessed","false");
                messageBody.addProperty("idUtilizationType",dto.getUtilizationType());

                SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration, "newcodificati.sqs");
                logger.debug("Sending message to servicebus: " + messageBody.toString());
                sqsMessagePump.sendToProcessMessage(messageBody, false);

            }
            response.setStatus("OK");
            response.setMessage("Caricamento del codificato richiesto.");
            response.setDsrs(request.getDsrs());
            return Response.ok(gson.toJson(response)).build();
        } catch (Exception e) {
            logger.error("Search Request Exception", e);
            return Response.status(500).build();
        }
    }

    @POST
    @Path("/clear-load-identified")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response submitRequestIdentified() throws IOException {
        try {

            logger.error("CANCELLAZIONE CODIFICATO!");

            EntityManager em = uni.get();

            em.getTransaction().begin();

            em.createNativeQuery("TRUNCATE TABLE load_identified_song").executeUpdate();
            em.createNativeQuery("TRUNCATE TABLE load_identified_song_dsr").executeUpdate();


            em.getTransaction().commit();


            Map<String,Object> response = new HashMap<String, Object>();
            response.put("status", "OK");
            response.put("message", "Codificato eliminato con successo");



            return Response.ok(gson.toJson(response)).build();
        } catch (Exception e) {
            logger.error("Search Request Exception", e);
            return Response.status(500).build();
        }
    }
}
