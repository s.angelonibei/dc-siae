package com.alkemytech.sophia.codman.performing.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class UtilizzazioniAssegnatePKDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer idCodifica;
	private Integer idSessioneCodifica;

	public UtilizzazioniAssegnatePKDTO() {
	}

	public Integer getIdCodifica() {
		return idCodifica;
	}

	public void setIdCodifica(Integer idCodifica) {
		this.idCodifica = idCodifica;
	}

	public Integer getIdSessioneCodifica() {
		return idSessioneCodifica;
	}

	public void setIdSessioneCodifica(Integer idSessioneCodifica) {
		this.idSessioneCodifica = idSessioneCodifica;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof UtilizzazioniAssegnatePKDTO)) {
			return false;
		}
		UtilizzazioniAssegnatePKDTO castOther = (UtilizzazioniAssegnatePKDTO)other;
		return 
			this.idCodifica.equals(castOther.idCodifica)
			&& this.idSessioneCodifica.equals(castOther.idSessioneCodifica);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.idCodifica.hashCode();
		hash = hash * prime + this.idSessioneCodifica.hashCode();
		
		return hash;
	}
}