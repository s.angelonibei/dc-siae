package com.alkemytech.sophia.codman.entity.performing;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "PERF_CARTELLA")
public class Cartella  implements Serializable {

    private Long id;
    private String codiceSede;
    private String codiceProvincia;
    private String unitaTerritoriale;
    private String descrizionePuntoTerritoriale;
    private Integer contabilita;
    private Integer contabilitaOrg;

    @Id
    @SequenceGenerator(name="CARTELLA_GEN",sequenceName = "CARTELLA_SEQ",allocationSize=1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CARTELLA_GEN")
    @Column(name = "ID_CARTELLA", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "CODICE_SEDE")
    public String getCodiceSede() {
        return codiceSede;
    }

    public void setCodiceSede(String codiceSede) {
        this.codiceSede = codiceSede;
    }

    @Column(name = "CODICE_PROVINCIA")
    public String getCodiceProvincia() {
        return codiceProvincia;
    }

    public void setCodiceProvincia(String codiceProvincia) {
        this.codiceProvincia = codiceProvincia;
    }

    @Column(name = "UNITA_TERRITORIALE")
    public String getUnitaTerritoriale() {
        return unitaTerritoriale;
    }

    public void setUnitaTerritoriale(String unitaTerritoriale) {
        this.unitaTerritoriale = unitaTerritoriale;
    }

    @Column(name = "CONTABILITA")
    public Integer getContabilita() {
        return contabilita;
    }

    public void setContabilita(Integer contabilita) {
        this.contabilita = contabilita;
    }

    @Column(name = "CONTABILITA_ORIG")
    public Integer getContabilitaOrg() {
        return contabilitaOrg;
    }

    public void setContabilitaOrg(Integer contabilitaOrg) {
        this.contabilitaOrg = contabilitaOrg;
    }

    @Transient
    public String getSeprag(){
      return codiceSede+codiceProvincia+unitaTerritoriale;
    }

    @Transient
    public String getDescrizionePuntoTerritoriale() {
        return descrizionePuntoTerritoriale;
    }

    public void setDescrizionePuntoTerritoriale(String descrizionePuntoTerritoriale) {
        this.descrizionePuntoTerritoriale = descrizionePuntoTerritoriale;
    }

}
