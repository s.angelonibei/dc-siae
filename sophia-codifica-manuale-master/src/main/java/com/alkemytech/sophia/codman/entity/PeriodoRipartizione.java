package com.alkemytech.sophia.codman.entity;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name="PERIODO_RIPARTIZIONE")
public class PeriodoRipartizione {

	 	@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
	    @Column(name="ID_PERIODO_RIPARTIZIONE", nullable=false)
	    private Long idPeriodoRipartizione;

	    @Column(name="CODICE")
	    private String codice;
	    
	    @Column(name="REPERTORIO")
	    private String repertorio;
	    
	    @Column(name="AMBITO")
	    private String ambito;
	    
	    @Column(name="COMPETENZE_DA")
	    private Date competenzeDa;
	    
	    @Column(name="COMPETENZE_A")
	    private Date competenzeA;
	    
	    @Column(name="DATA_APPROVAZIONE_REGOLE_VALORIZZAZIONE")
	    private Date dataApprovazioneRegoleValorizzazione;
	    
	    @Column(name="APPROVATORE_REGOLE_VALORIZZAZIONE")
	    private String approvatoreRegoleValorizzazione;
	    
	    @Column(name="DATA_APPROVAZIONE_CARICHI_RIPARTO")
	    private Date dataApprovazioneCarichiRiparto;
	    
	    @Column(name="APPROVATORE_CARICHI_RIPARTO")
	    private String approvatoreCarichiRiparto;
	    
	    @Column(name="STATO_APPROVAZIONE_CARICHI_RIPARTO")
	    private Boolean statoApprovazioneCarichiRiparto;
	    
	    public PeriodoRipartizione() {
	    	super();
	    }

		public PeriodoRipartizione(Long idPeriodoRipartizione, String codice, String repertorio, String ambito,
				Date competenzeDa, Date competenzeA, Date dataApprovazioneRegoleValorizzazione,
				String approvatoreRegoleValorizzazione, Date dataApprovazioneCarichiRiparto,
				String approvatoreCarichiRiparto, Boolean statoApprovazioneCarichiRiparto) {
			super();
			this.idPeriodoRipartizione = idPeriodoRipartizione;
			this.codice = codice;
			this.repertorio = repertorio;
			this.ambito = ambito;
			this.competenzeDa = competenzeDa;
			this.competenzeA = competenzeA;
			this.dataApprovazioneRegoleValorizzazione = dataApprovazioneRegoleValorizzazione;
			this.approvatoreRegoleValorizzazione = approvatoreRegoleValorizzazione;
			this.dataApprovazioneCarichiRiparto = dataApprovazioneCarichiRiparto;
			this.approvatoreCarichiRiparto = approvatoreCarichiRiparto;
			this.statoApprovazioneCarichiRiparto = statoApprovazioneCarichiRiparto;
		}

		public Long getIdPeriodoRipartizione() { 
			return idPeriodoRipartizione;
		}

		public void setIdPeriodoRipartizione(Long idPeriodoRipartizione) {
			this.idPeriodoRipartizione = idPeriodoRipartizione;
		}

		public String getCodice() {
			return codice;
		}

		public void setCodice(String codice) {
			this.codice = codice;
		}

		public String getRepertorio() {
			return repertorio;
		}

		public void setRepertorio(String repertorio) {
			this.repertorio = repertorio;
		}

		public String getAmbito() {
			return ambito;
		}

		public void setAmbito(String ambito) {
			this.ambito = ambito;
		}

		public Date getCompetenzeDa() {
			return competenzeDa;
		}

		public void setCompetenzeDa(Date competenzeDa) {
			this.competenzeDa = competenzeDa;
		}

		public Date getCompetenzeA() {
			return competenzeA;
		}

		public void setCompetenzeA(Date competenzeA) {
			this.competenzeA = competenzeA;
		}

		public Date getDataApprovazioneRegoleValorizzazione() {
			return dataApprovazioneRegoleValorizzazione;
		}

		public void setDataApprovazioneRegoleValorizzazione(Date dataApprovazioneRegoleValorizzazione) {
			this.dataApprovazioneRegoleValorizzazione = dataApprovazioneRegoleValorizzazione;
		}

		public String getApprovatoreRegoleValorizzazione() {
			return approvatoreRegoleValorizzazione;
		}

		public void setApprovatoreRegoleValorizzazione(String approvatoreRegoleValorizzazione) {
			this.approvatoreRegoleValorizzazione = approvatoreRegoleValorizzazione;
		}

		public Date getDataApprovazioneCarichiRiparto() {
			return dataApprovazioneCarichiRiparto;
		}

		public void setDataApprovazioneCarichiRiparto(Date dataApprovazioneCarichiRiparto) {
			this.dataApprovazioneCarichiRiparto = dataApprovazioneCarichiRiparto;
		}

		public String getApprovatoreCarichiRiparto() {
			return approvatoreCarichiRiparto;
		}

		public void setApprovatoreCarichiRiparto(String approvatoreCarichiRiparto) {
			this.approvatoreCarichiRiparto = approvatoreCarichiRiparto;
		}

		public Boolean getStatoApprovazioneCarichiRiparto() {
			return statoApprovazioneCarichiRiparto;
		}

		public void setStatoApprovazioneCarichiRiparto(Boolean statoApprovazioneCarichiRiparto) {
			this.statoApprovazioneCarichiRiparto = statoApprovazioneCarichiRiparto;
		}
		public int getIntCompetenzeDa(){
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
			return Integer.parseInt(sdf.format(competenzeDa));
		}

		public int getIntCompetenzeA(){
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
			return Integer.parseInt(sdf.format(competenzeDa));
		}

	    
}
