package com.alkemytech.sophia.codman.entity.performing;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "PERF_DIRETTORE_ESECUZIONE")
public class DirettoreEsecuzione  implements Serializable {


    private Long id;
    private Long idProgrammaMusicale;
    private Long numeroProgrammaMusicale;
    private String direttoreEsecuzione;
    private String posizioneSIAE;
    private String indirizzo;
    private String cap;
    private String comune;
    private String siglaProvincia;
    private String codiceSap;
    private String codiceFiscale;
    private String sesso;
    private String comuneNascita;
    private String siglaProvinciaNascita;
    private Date dataNascita;
    private Character flagMancanzaFirma;
    private Character flagPresenzaFirmaPrivacy;


    @Id
    @Column(name = "ID_DIRETTORE_ESECUZIONE", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    @Column(name = "ID_PROGRAMMA_MUSICALE")
    public Long getIdProgrammaMusicale() {
        return idProgrammaMusicale;
    }

    public void setIdProgrammaMusicale(Long idProgrammaMusicale) {
        this.idProgrammaMusicale = idProgrammaMusicale;
    }

    @Column(name = "NUMERO_PROGRAMMA_MUSICALE")
    public Long getNumeroProgrammaMusicale() {
        return numeroProgrammaMusicale;
    }

    public void setNumeroProgrammaMusicale(Long numeroProgrammaMusicale) {
        this.numeroProgrammaMusicale = numeroProgrammaMusicale;
    }

    @Column(name = "DIRETTORE_ESECUZIONE")
    public String getDirettoreEsecuzione() {
        return direttoreEsecuzione;
    }

    public void setDirettoreEsecuzione(String direttoreEsecuzione) {
        this.direttoreEsecuzione = direttoreEsecuzione;
    }


    @Column(name = "POSIZIONE_SIAE")
    public String getPosizioneSIAE() {
        return posizioneSIAE;
    }

    public void setPosizioneSIAE(String posizioneSIAE) {
        this.posizioneSIAE = posizioneSIAE;
    }

    @Column(name = "INDIRIZZO")
    public String getIndirizzo() {
        return indirizzo;
    }

    public void setIndirizzo(String indirizzo) {
        this.indirizzo = indirizzo;
    }

    @Column(name = "CAP")
    public String getCap() {
        return cap;
    }

    public void setCap(String cap) {
        this.cap = cap;
    }


    @Column(name = "COMUNE")
    public String getComune() {
        return comune;
    }

    public void setComune(String comune) {
        this.comune = comune;
    }

    @Column(name = "SIGLA_PROVINCIA")
    public String getSiglaProvincia() {
        return siglaProvincia;
    }

    public void setSiglaProvincia(String siglaProvincia) {
        this.siglaProvincia = siglaProvincia;
    }

    @Column(name = "CODICE_SAP")
    public String getCodiceSap() {
        return codiceSap;
    }

    public void setCodiceSap(String codiceSap) {
        this.codiceSap = codiceSap;
    }



    @Column(name = "CODICE_FISCALE")
    public String getCodiceFiscale() {
        return codiceFiscale;
    }

    public void setCodiceFiscale(String codiceFiscale) {
        this.codiceFiscale = codiceFiscale;
    }

    @Column(name = "SESSO")
    public String getSesso() {
        return sesso;
    }

    public void setSesso(String sesso) {
        this.sesso = sesso;
    }

    @Column(name = "COMUNE_NASCITA")
    public String getComuneNascita() {
        return comuneNascita;
    }

    public void setComuneNascita(String comuneNascita) {
        this.comuneNascita = comuneNascita;
    }

    @Column(name = "SIGLA_PROVINCIA_NASCITA")
    public String getSiglaProvinciaNascita() {
        return siglaProvinciaNascita;
    }

    public void setSiglaProvinciaNascita(String siglaProvinciaNascita) {
        this.siglaProvinciaNascita = siglaProvinciaNascita;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_NASCITA")
    public Date getDataNascita() {
        return dataNascita;
    }

    public void setDataNascita(Date dataNascita) {
        this.dataNascita = dataNascita;
    }

    @Column(name = "FLAG_MANCANZA_FIRMA")
    public Character getFlagMancanzaFirma() {
        return flagMancanzaFirma;
    }

    public void setFlagMancanzaFirma(Character flagMancanzaFirma) {
        this.flagMancanzaFirma = flagMancanzaFirma;
    }

    @Column(name = "FLAG_PRESENZA_FIRMA_PRIVACY")
    public Character getFlagPresenzaFirmaPrivacy() {
        return flagPresenzaFirmaPrivacy;
    }

    public void setFlagPresenzaFirmaPrivacy(Character flagPresenzaFirmaPrivacy) {
        this.flagPresenzaFirmaPrivacy = flagPresenzaFirmaPrivacy;
    }
}
