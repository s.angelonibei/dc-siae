package com.alkemytech.sophia.codman.service.dsrmetadata.timestamp;

public class TimeStampV1 implements TimeStamp {

	private String regex = "[0-9]{8}T[0-9]{6}";
	private String code = "YYYYMMDDTHHmmss";
	private String description = "YYYYMMDDTHHmmss";

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String getRegex() {
		return regex;
	}

	@Override
	public String getCode() {
		// TODO Auto-generated method stub
		return code;
	}

}
