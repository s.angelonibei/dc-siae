package com.alkemytech.sophia.codman.dto.performing.csv;

import java.util.Date;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

public class BonificaManifestazioneListToCsv {

	@CsvBindByPosition(position = 0)
	@CsvBindByName
	private String origineRecord;
	@CsvBindByPosition(position = 1)
	@CsvBindByName
	private Long idEvento;
	@CsvBindByPosition(position = 2)
	@CsvBindByName
	private Date dataInizioEvento;
	@CsvBindByPosition(position = 3)
	@CsvBindByName
	private Date dataFineEvento;
	@CsvBindByPosition(position = 4)
	@CsvBindByName
	private String oraInizio;
	@CsvBindByPosition(position = 5)
	@CsvBindByName
	private String oraFine;
	@CsvBindByPosition(position = 6)
	@CsvBindByName
	private String codiceLocale;
	@CsvBindByPosition(position = 7)
	@CsvBindByName
	private String denominazioneLocale;
	@CsvBindByPosition(position = 8)
	@CsvBindByName
	private String codiceComune;
	@CsvBindByPosition(position = 9)
	@CsvBindByName
	private String comuneLocale;
	@CsvBindByPosition(position = 10)
	@CsvBindByName
	private String provinciaLocale;
	@CsvBindByPosition(position = 11)
	@CsvBindByName
	private String regioneLocale;
	@CsvBindByPosition(position = 12)
	@CsvBindByName
	private String codiceBaLocale;
	@CsvBindByPosition(position = 13)
	@CsvBindByName
	private String capLocale;
	@CsvBindByPosition(position = 14)
	@CsvBindByName
	private String siglaLocale;
	@CsvBindByPosition(position = 15)
	@CsvBindByName
	private String spazioLocale;
	@CsvBindByPosition(position = 16)
	@CsvBindByName
	private String genereLocale;
	@CsvBindByPosition(position = 17)
	@CsvBindByName
	private String denominazioneLocalita;
	@CsvBindByPosition(position = 18)
	@CsvBindByName
	private String codiceSapOrganizzatore;
	@CsvBindByPosition(position = 19)
	@CsvBindByName
	private String partitaIvaOrganizzatore;
	@CsvBindByPosition(position = 20)
	@CsvBindByName
	private String codiceFiscaleOrganizzatore;
	@CsvBindByPosition(position = 21)
	@CsvBindByName
	private String nomeOrganizzatore;
	@CsvBindByPosition(position = 22)
	@CsvBindByName
	private String comuneOrganizzatore;
	@CsvBindByPosition(position = 23)
	@CsvBindByName
	private String provinciaOrganizzatore;
	@CsvBindByPosition(position = 24)
	@CsvBindByName
	private String titolo;
	@CsvBindByPosition(position = 25)
	@CsvBindByName
	private String titoloOriginale;

	public BonificaManifestazioneListToCsv(Object[] row) { // controllare 28 invece di 27
		super();
		
		
		this.origineRecord = (String) row[0];
		this.idEvento = (Long) row[1];
		this.dataInizioEvento = (Date) row[2];
		this.dataFineEvento = (Date) row[3];
		this.oraInizio = (String) row[4];
		this.oraFine = (String) row[5];
		this.codiceLocale = (String) row[6];
		this.denominazioneLocale = (String) row[7];
		this.codiceComune = (String) row[8];
		this.comuneLocale = (String) row[9];
		this.provinciaLocale = (String) row[10];
		this.regioneLocale = (String) row[11];
		this.codiceBaLocale = (String) row[12];
		this.capLocale = (String) row[13];
		this.siglaLocale = (String) row[14];
		this.spazioLocale = (String) row[15];
		this.genereLocale = (String) row[16];
		this.denominazioneLocalita = (String) row[17];
		this.codiceSapOrganizzatore = (String) row[18];
		this.partitaIvaOrganizzatore = (String) row[19];
		this.codiceFiscaleOrganizzatore = (String) row[20];
		this.nomeOrganizzatore = (String) row[21];
		this.comuneOrganizzatore = (String) row[22];
		this.provinciaOrganizzatore = (String) row[23];
		this.titolo = (String) row[24];
		this.titoloOriginale = (String) row[25];
	}

	public BonificaManifestazioneListToCsv() {
		super();
	}

	public String getOrigineRecord() {
		return origineRecord;
	}

	public void setOrigineRecord(String origineRecord) {
		this.origineRecord = origineRecord;
	}

	public Long getIdEvento() {
		return idEvento;
	}

	public void setIdEvento(Long idEvento) {
		this.idEvento = idEvento;
	}

	public Date getDataInizioEvento() {
		return dataInizioEvento;
	}

	public void setDataInizioEvento(Date dataInizioEvento) {
		this.dataInizioEvento = dataInizioEvento;
	}

	public Date getDataFineEvento() {
		return dataFineEvento;
	}

	public void setDataFineEvento(Date dataFineEvento) {
		this.dataFineEvento = dataFineEvento;
	}

	public String getOraInizio() {
		return oraInizio;
	}

	public void setOraInizio(String oraInizio) {
		this.oraInizio = oraInizio;
	}

	public String getOraFine() {
		return oraFine;
	}

	public void setOraFine(String oraFine) {
		this.oraFine = oraFine;
	}

	public String getCodiceLocale() {
		return codiceLocale;
	}

	public void setCodiceLocale(String codiceLocale) {
		this.codiceLocale = codiceLocale;
	}

	public String getDenominazioneLocale() {
		return denominazioneLocale;
	}

	public void setDenominazioneLocale(String denominazioneLocale) {
		this.denominazioneLocale = denominazioneLocale;
	}

	public String getCodiceComune() {
		return codiceComune;
	}

	public void setCodiceComune(String codiceComune) {
		this.codiceComune = codiceComune;
	}

	public String getComuneLocale() {
		return comuneLocale;
	}

	public void setComuneLocale(String comuneLocale) {
		this.comuneLocale = comuneLocale;
	}

	public String getProvinciaLocale() {
		return provinciaLocale;
	}

	public void setProvinciaLocale(String provinciaLocale) {
		this.provinciaLocale = provinciaLocale;
	}

	public String getRegioneLocale() {
		return regioneLocale;
	}

	public void setRegioneLocale(String regioneLocale) {
		this.regioneLocale = regioneLocale;
	}

	public String getCodiceBaLocale() {
		return codiceBaLocale;
	}

	public void setCodiceBaLocale(String codiceBaLocale) {
		this.codiceBaLocale = codiceBaLocale;
	}

	public String getCapLocale() {
		return capLocale;
	}

	public void setCapLocale(String capLocale) {
		this.capLocale = capLocale;
	}

	public String getSiglaLocale() {
		return siglaLocale;
	}

	public void setSiglaLocale(String siglaLocale) {
		this.siglaLocale = siglaLocale;
	}

	public String getSpazioLocale() {
		return spazioLocale;
	}

	public void setSpazioLocale(String spazioLocale) {
		this.spazioLocale = spazioLocale;
	}

	public String getGenereLocale() {
		return genereLocale;
	}

	public void setGenereLocale(String genereLocale) {
		this.genereLocale = genereLocale;
	}

	public String getDenominazioneLocalita() {
		return denominazioneLocalita;
	}

	public void setDenominazioneLocalita(String denominazioneLocalita) {
		this.denominazioneLocalita = denominazioneLocalita;
	}

	public String getCodiceSapOrganizzatore() {
		return codiceSapOrganizzatore;
	}

	public void setCodiceSapOrganizzatore(String codiceSapOrganizzatore) {
		this.codiceSapOrganizzatore = codiceSapOrganizzatore;
	}

	public String getPartitaIvaOrganizzatore() {
		return partitaIvaOrganizzatore;
	}

	public void setPartitaIvaOrganizzatore(String partitaIvaOrganizzatore) {
		this.partitaIvaOrganizzatore = partitaIvaOrganizzatore;
	}

	public String getCodiceFiscaleOrganizzatore() {
		return codiceFiscaleOrganizzatore;
	}

	public void setCodiceFiscaleOrganizzatore(String codiceFiscaleOrganizzatore) {
		this.codiceFiscaleOrganizzatore = codiceFiscaleOrganizzatore;
	}

	public String getNomeOrganizzatore() {
		return nomeOrganizzatore;
	}

	public void setNomeOrganizzatore(String nomeOrganizzatore) {
		this.nomeOrganizzatore = nomeOrganizzatore;
	}

	public String getComuneOrganizzatore() {
		return comuneOrganizzatore;
	}

	public void setComuneOrganizzatore(String comuneOrganizzatore) {
		this.comuneOrganizzatore = comuneOrganizzatore;
	}

	public String getProvinciaOrganizzatore() {
		return provinciaOrganizzatore;
	}

	public void setProvinciaOrganizzatore(String provinciaOrganizzatore) {
		this.provinciaOrganizzatore = provinciaOrganizzatore;
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	public String getTitoloOriginale() {
		return titoloOriginale;
	}

	public void setTitoloOriginale(String titoloOriginale) {
		this.titoloOriginale = titoloOriginale;
	}

	public String[] getMappingStrategy() {
		return new String[] { 
				
				"Origine Record", 
				"Id evento",
				"Data inizio evento", 
				"Data fine evento", 
				"Ora inizio",
				"Ora fine",
				"Codice locale", 
				"Denominazione locale",
				"Codice comune",
				"Comune Locale",
				"Provincia locale", 
				"Regione locale", 
				"Codice Ba Locale", 
				"Cap locale",
				"Sigla Locale",
				"Spazio locale",
				"Genere Locale", 
				"Denominazione localita", 
				"Codice sap organizzatore", 
				"Partita iva organizzatore",
				"Codice fiscale organizzatore",
				"Nome organizzatore", 
				"Comune organizzatore", 
				"Provincia organizzatore",
				"Titolo", 
				"Titolo originale"
		};
	}

}
