package com.alkemytech.sophia.codman.utils;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;
import java.util.Properties;

public class DbConnectionPool {
    private static HikariDataSource ds;


    public static DataSource getConnection(Properties properties) {
        if (ds == null) {
            HikariConfig config = new HikariConfig();
            config.setJdbcUrl(properties.getProperty("javax.persistence.jdbc.url"));
            config.setDriverClassName(properties.getProperty("javax.persistence.jdbc.driver"));
            config.setUsername(properties.getProperty("javax.persistence.jdbc.user"));
            config.setPassword(properties.getProperty("javax.persistence.jdbc.password"));
            config.addDataSourceProperty("cachePrepStmts", "true");
            config.addDataSourceProperty("prepStmtCacheSize", "250");
            config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
            ds = new HikariDataSource(config);
            return ds;


        } else {

            return ds;
        }

    }

}