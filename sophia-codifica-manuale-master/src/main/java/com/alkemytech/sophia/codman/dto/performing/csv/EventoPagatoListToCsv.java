package com.alkemytech.sophia.codman.dto.performing.csv;

import java.text.DecimalFormat;
import java.util.Date;


import com.alkemytech.sophia.codman.utils.DateUtils;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

public class EventoPagatoListToCsv {

    @CsvBindByPosition(position = 0)
    @CsvBindByName
	private Long evento;
    @CsvBindByPosition(position = 1)
    @CsvBindByName
	private String permesso;
    @CsvBindByPosition(position = 2)
    @CsvBindByName
	private Integer contabilita;
    @CsvBindByPosition(position = 3)
    @CsvBindByName
	private String fattura;
    @CsvBindByPosition(position = 4)
    @CsvBindByName
	private String voceIncasso;
    @CsvBindByPosition(position = 5)
    @CsvBindByName
	private String dataInizio;
    @CsvBindByPosition(position = 6)
    @CsvBindByName
	private String oraInizio;
    @CsvBindByPosition(position = 7)
    @CsvBindByName
	private String locale;
    @CsvBindByPosition(position = 8)
    @CsvBindByName
	private String organizzatore;
    @CsvBindByPosition(position = 9)
    @CsvBindByName
	private String tipoDocumento;
    @CsvBindByPosition(position = 10)
    @CsvBindByName
	private String importo;
    @CsvBindByPosition(position = 11)
    @CsvBindByName
	private String quadratura;
    @CsvBindByPosition(position = 12)
    @CsvBindByName
	private String importoAggio;
    @CsvBindByPosition(position = 13)
    @CsvBindByName
	private String importoExArt;
    @CsvBindByPosition(position = 14)
    @CsvBindByName
	private String importoNetto;
	
	
	public EventoPagatoListToCsv(Long evento, String permesso, Integer contabilita, String fattura, String voceIncasso,
			Date dataInizio, String oraInizio, String locale, String organizzatore, String tipoDocumento,
			Double importo, String quadratura, Double importoAggio, Double importoExArt) {
		super();
		this.evento = evento;
		this.permesso = permesso;
		this.contabilita = contabilita;
		this.fattura = fattura;
		this.voceIncasso = voceIncasso;
		
		if(dataInizio != null) {
			this.dataInizio = DateUtils.dateToString(dataInizio, DateUtils.DATE_ITA_FORMAT_DASH);
		}
		
		this.oraInizio = oraInizio;
		this.locale = locale;
		this.organizzatore = organizzatore;
		this.tipoDocumento = tipoDocumento;
		this.importo = importo+"";
		
		
		
		if(this.dataInizio!= null) {
		}
		this.quadratura = ("1".equals(quadratura)? "Effettuata":"Non Effettuata");
		
		DecimalFormat df = new DecimalFormat("0.00");
		String importoDem = null;
	
		if (importoAggio!=null) {
			this.importoAggio =  df.format(importoAggio);
		}else {
			importoAggio=0.0;
		}
		if (importoExArt!=null) {
			this.importoExArt = df.format(importoExArt);
		}else {
			importoExArt=0.0;
		}
		try {
			double totale = (importo-importoExArt-importoAggio);
			importoNetto=df.format(totale);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public EventoPagatoListToCsv() {
		super();
	}


	public Long getEvento() {
		return evento;
	}

	public void setEvento(Long evento) {
		this.evento = evento;
	}

	public String getPermesso() {
		return permesso;
	}

	public void setPermesso(String permesso) {
		this.permesso = permesso;
	}

	public Integer getContabilita() {
		return contabilita;
	}

	public void setContabilita(Integer contabilita) {
		this.contabilita = contabilita;
	}

	public String getFattura() {
		return fattura;
	}

	public void setFattura(String fattura) {
		this.fattura = fattura;
	}

	public String getVoceIncasso() {
		return voceIncasso;
	}

	public void setVoceIncasso(String voceIncasso) {
		this.voceIncasso = voceIncasso;
	}

	public String getDataInizio() {
		return dataInizio;
	}

	public void setDataInizio(String dataInizio) {
		this.dataInizio = dataInizio;
	}

	public String getOraInizio() {
		return oraInizio;
	}

	public void setOraInizio(String oraInizio) {
		this.oraInizio = oraInizio;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getOrganizzatore() {
		return organizzatore;
	}

	public void setOrganizzatore(String organizzatore) {
		this.organizzatore = organizzatore;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getImporto() {
		return importo;
	}

	public void setImporto(String importo) {
		this.importo = importo;
	}

	public String getQuadratura() {
		return quadratura;
	}

	public void setQuadratura(String quadratura) {
		this.quadratura = quadratura;
	}

	public String getImportoAggio() {
		return importoAggio;
	}

	public void setImportoAggio(String importoAggio) {
		this.importoAggio = importoAggio;
	}

	public String getImportoExArt() {
		return importoExArt;
	}

	public void setImportoExArt(String importoExArt) {
		this.importoExArt = importoExArt;
	}

	public String getImportoNetto() {
		return importoNetto;
	}

	public void setImportoNetto(String importoNetto) {
		importoNetto = importoNetto;
	}

	public String[] getMappingStrategy() {
        return new String[]{
        		"Evento",
        		"Permesso",
        		"Contabilita",
    			"Fattura",
        		"Voce Incasso",
        		"Data Inizio",
        		"Ora Inizio",
        		"Locale-Spazio",
        		"Organizzatore",
        		"Tipo Documento",
        		"Importo",
        		"Quadratura Ndm",
        		"Importo Aggio",
        		"Importo Ex.Articolo 18",
        		"Importo Netto",
        };
	}

}
