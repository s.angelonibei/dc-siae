package com.alkemytech.sophia.codman.multimedialelocale.dto;

import java.util.ArrayList;

public class FieldDto {
	private String nome;

	public FieldDto() {
		super();
	}
	public FieldDto(String nome) {
		super();
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public static ArrayList<FieldDto> getStati() {
		ArrayList<FieldDto> fields = new ArrayList<>();
		FieldDto caricato = new FieldDto("CARICATO");
		FieldDto scartato = new FieldDto("SCARTATO");
		FieldDto lavorabile = new FieldDto("LAVORABILE");
		FieldDto accettato = new FieldDto("ACCETTATO");
		FieldDto rifiutato = new FieldDto("RIFIUTATO");
		fields.add(caricato);
		fields.add(scartato);
		fields.add(lavorabile);
		fields.add(accettato);
		fields.add(rifiutato);
		return fields;
	}
	
	public static ArrayList<FieldDto> getTipologieServizio() {
		ArrayList<FieldDto> fields = new ArrayList<>();
		FieldDto streaming = new FieldDto("Stream");
		FieldDto download = new FieldDto("Download");
		fields.add(streaming);
		fields.add(download);
		return fields;
	}
	
	public static boolean containsServizio(String servizio) {
		boolean contains=false;
		for (FieldDto fieldDto:getTipologieServizio()) {
			if (fieldDto.getNome().equals(servizio)) {
				return true;
			}
		}
		
		return contains;
	}
	
}
