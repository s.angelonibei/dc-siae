package com.alkemytech.sophia.codman.entity;

import com.alkemytech.sophia.codman.dto.MMSpecialCharCcidDTO;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement
@Entity
@Data
@Table(name = "MM_SPECIALCHAR_CCID")
public class MmSpecialCharCcid implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "IDDSP", nullable = false)
    private String idDsp;

    @Column(name = "CHAR_LIST", nullable = false)
    private String charList;

    @Column(name = "TITLE_LENGTH", nullable = false)
    private Long titleLength;

    @Column(name = "ENABLED", nullable = false)
    private String enabled;

    public MmSpecialCharCcid() {
    }

    public MmSpecialCharCcid(MMSpecialCharCcidDTO dto) {
        this.idDsp = dto.getIdDsp();
        this.charList = dto.getCharList();
        this.titleLength = dto.getTitleLength();
        this.enabled = dto.getEnabled().equalsIgnoreCase("TRUE") || dto.getEnabled().equalsIgnoreCase("Y") ? "Y" : "N";
    }

}
