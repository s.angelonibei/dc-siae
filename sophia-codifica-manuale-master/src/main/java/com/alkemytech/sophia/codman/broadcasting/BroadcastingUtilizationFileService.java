package com.alkemytech.sophia.codman.broadcasting;

import com.alkemytech.sophia.broadcasting.dto.*;
import com.alkemytech.sophia.broadcasting.enums.Repertorio;
import com.alkemytech.sophia.broadcasting.enums.Stato;
import com.alkemytech.sophia.broadcasting.model.BdcInformationFileEntity;
import com.alkemytech.sophia.broadcasting.model.TipoBroadcaster;
import com.alkemytech.sophia.broadcasting.service.interfaces.*;
import com.alkemytech.sophia.codman.utils.PATCH;
import com.alkemytech.sophia.common.beantocsv.CustomMappingStrategy;
import com.alkemytech.sophia.common.s3.S3Service;
import com.amazonaws.services.s3.AmazonS3URI;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import com.sun.jersey.multipart.FormDataBodyPart;
import com.sun.jersey.multipart.FormDataParam;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.StreamingOutput;
import java.io.*;
import java.util.*;


@Singleton
@Path("broadcastingS3")
public class BroadcastingUtilizationFileService {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final String s3Bucket;
    private final String s3KeyPrefix;
    private final String fileArmonizzatoTV;
    private final String fileArmonizzatoRadio;
//    private final String fileArmonizzatoTVNuovoTracciatoRai;
//    private final String fileArmonizzatoRadioNuovoTracciatoRai;
    private final S3Service s3Service;
    private final InformationFileEntityService informationFileEntityService;
    private final BdcBroadcastersService bdcBroadcastersService;
    private final BdcUtentiService bdcUtentiService;
    private final BroadcasterConfigService broadcasterConfigService;
    private final BdcCanaliService bdcCanaliService;
    private final BdcFileArmonizzatoService bdcFileArmonizzatoService;
    private final Gson gson;

    @Inject
    public BroadcastingUtilizationFileService(@Named("broadcasting.s3.bucket") String s3Bucket, S3Service s3Service,
                                              InformationFileEntityService informationFileEntityService, BdcBroadcastersService bdcBroadcastersService,
                                              BdcUtentiService bdcUtentiService, BroadcasterConfigService broadcasterConfigService,
                                              BdcCanaliService bdcCanaliService, BdcFileArmonizzatoService bdcFileArmonizzatoService,
                                              @Named("file_armonizzato_tv") String fileArmonizzatoTV,
                                              @Named("file_armonizzato_radio") String fileArmonizzatoRadio,
//                                              @Named("file_armonizzato_tv_NT") String fileArmonizzatoTVNuovoTracciatoRai,
//                                              @Named("file_armonizzato_radio_NT") String fileArmonizzatoRadioNuovoTracciatoRai,
                                              @Named("broadcasting.s3.key_prefix") String s3KeyPrefix, Gson gson,
                                              @Named("csv.download.directory") String csvTmpDirectory) {
        super();
        this.s3Bucket = s3Bucket;
        this.s3Service = s3Service;
        this.s3KeyPrefix = s3KeyPrefix;
        this.fileArmonizzatoTV = fileArmonizzatoTV;
        this.fileArmonizzatoRadio = fileArmonizzatoRadio;
//        this.fileArmonizzatoTVNuovoTracciatoRai = fileArmonizzatoTVNuovoTracciatoRai;
//        this.fileArmonizzatoRadioNuovoTracciatoRai = fileArmonizzatoRadioNuovoTracciatoRai;
        this.gson = gson;
        this.informationFileEntityService = informationFileEntityService;
        this.bdcBroadcastersService = bdcBroadcastersService;
        this.bdcUtentiService = bdcUtentiService;
        this.bdcCanaliService = bdcCanaliService;
        this.broadcasterConfigService = broadcasterConfigService;
        this.bdcFileArmonizzatoService = bdcFileArmonizzatoService;
    }

    @POST
    @Path("fe/utilizationfile/upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Response broadcastingFileUpload(@FormDataParam("file") List<FormDataBodyPart> bodyParts,
                                           @FormDataParam("idBroadcaster") Integer idBroadcaster,
                                           @FormDataParam("idCanale") Integer idCanale,
                                           @FormDataParam("anno") Integer anno,
                                           @FormDataParam("mese") Integer mese,
                                           @FormDataParam("idUtente") Integer idUtente) {

        List<String> err = new ArrayList<>();
        Boolean stepOne = false;
        ResponseInformationFileInsertDTO response = new ResponseInformationFileInsertDTO();
        ResponseDataInformationFileDTO responseDataInformationFileDTO = new ResponseDataInformationFileDTO();

        for (FormDataBodyPart fdbp : bodyParts) {
            if (!((idCanale == null && anno == null && mese == null)||(idCanale != null && anno != null && mese != null))) {
                err.add("Il format del caricamento SINGOLO prevede l'inserimento dei valori nei campi canale, anno e mese; mentre "
                        + "il format del caricamento MASSIVO prevede che i campi canale, anno e mese vengano lasciati vuoti.");
                EsitoDTO esito = new EsitoDTO();
                esito.setCodice("10");
                esito.setDescrizioneEsito("KO");
                response.setEsito(esito);
                responseDataInformationFileDTO.setOutput(err.toString());
                response.setResponseData(responseDataInformationFileDTO);
                return Response.ok(response).build();
            }
            String[] nameFileParts = fdbp.getFormDataContentDisposition().getFileName().split("\\.(?=[^\\.]+$)");
            String nameFile = nameFileParts[0] + "_" + Calendar.getInstance().getTimeInMillis() + "." + nameFileParts[1];
            final String percorso = "s3://" + s3Bucket + "/" + s3KeyPrefix + "/"
                    + (bdcBroadcastersService.findById(idBroadcaster).getNome()).replace(" ", "") + "/" + nameFile;
            logger.debug("upload: percorso {}", percorso);
            final AmazonS3URI s3Uri = new AmazonS3URI(percorso);
            logger.debug("upload: s3Uri {}", s3Uri);
            SearchBroadcasterConfigDTO searchBroadcasterConfigDTO = new SearchBroadcasterConfigDTO();
            searchBroadcasterConfigDTO.setIdBroadcaster(idBroadcaster);
            // searchBroadcasterConfigDTO.setIdChannel(idCanale);
            BdcConfigDTO bdcConfigDto = broadcasterConfigService.findActive(searchBroadcasterConfigDTO);
            if (!nameFileParts[1].matches(bdcConfigDto.getSupportedFormat())) {
                String erroreFormato = fdbp.getFormDataContentDisposition().getFileName() + ": formato file non supportato";
                err.add(erroreFormato);
                stepOne = true;
            }
            if (!stepOne && s3Service.upload(s3Uri.getBucket(), s3Uri.getKey(), fdbp.getEntityAs(InputStream.class), fdbp.getFormDataContentDisposition().getFileName())) {
                final BdcInformationFileEntity bdcInformationFileEntity = new BdcInformationFileEntity();
                bdcInformationFileEntity.setEmittente(bdcBroadcastersService.findById(idBroadcaster));
                bdcInformationFileEntity.setIdUtente(bdcUtentiService.findById(idUtente));
                if (idCanale != null) {
                    bdcInformationFileEntity.setCanale(bdcCanaliService.findById(idCanale));
                }
                bdcInformationFileEntity.setNomeFile(fdbp.getFormDataContentDisposition().getFileName());
                bdcInformationFileEntity.setAnno(anno);
                bdcInformationFileEntity.setMese(mese);
                bdcInformationFileEntity.setDataUpload(Calendar.getInstance().getTime());
                bdcInformationFileEntity.setPercorso(percorso);
                if (idCanale == null && anno == null && mese == null) {
                    bdcInformationFileEntity.setTipoUpload("MASSIVO");
                } else {
                    bdcInformationFileEntity.setTipoUpload("SINGOLO");
                }
                bdcInformationFileEntity.setStato(Stato.DA_ELABORARE);
                Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
                Set<ConstraintViolation<BdcInformationFileEntity>> violations = validator
                        .validate(bdcInformationFileEntity);
                if (!violations.isEmpty()) {
                    List<String> errorLog = new ArrayList<>();
                    for (ConstraintViolation<BdcInformationFileEntity> violation : violations) {
                        errorLog.add(violation.getPropertyPath() + " " + violation.getMessage());
                    }
                }
                try {
                    informationFileEntityService.save(bdcInformationFileEntity);
                    err.add(fdbp.getFormDataContentDisposition().getFileName() + ": upload del file avvenuto con successo");
                } catch (Exception e) {
                    logger.error(e.getMessage());
                    // entityManager.getTransaction().rollback();
                    List<String> log = new ArrayList<>();
                    for (StackTraceElement ste : e.getStackTrace()) {
                        log.add(ste.toString());
                    }
                }
            }
            stepOne=false;
        }
        EsitoDTO esito = new EsitoDTO();
        esito.setCodice("00");
        esito.setDescrizioneEsito("OK");
        response.setEsito(esito);
        responseDataInformationFileDTO.setOutput(err.toString());
        response.setResponseData(responseDataInformationFileDTO);
        return Response.ok(response).build();
    }

    @POST
    @Path("fe/utilizationfile/minimalUpload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Response broadcastingMinimalFileUpload(@FormDataParam("fileContent") InputStream uploadedInputStream,
                                           @FormDataParam("fileName") String fileName,
                                           @FormDataParam("idBroadcaster") Integer idBroadcaster, @FormDataParam("idCanale") Integer idCanale,
                                           @FormDataParam("anno") Integer anno, @FormDataParam("mese") Integer mese,
                                           @FormDataParam("idUtente") Integer idUtente) {
        final String[] parts = (fileName).split("\\.(?=[^\\.]+$)");
        final String nomeFile = parts[0] + "_" + Calendar.getInstance().getTimeInMillis() + "." + parts[1];
        final String percorso = "s3://" + s3Bucket + "/" + s3KeyPrefix + "/"
                + (bdcBroadcastersService.findById(idBroadcaster).getNome()).replace(" ", "") + "/" + nomeFile;
        logger.debug("upload: percorso {}", percorso);
        final AmazonS3URI s3Uri = new AmazonS3URI(percorso);
        logger.debug("upload: s3Uri {}", s3Uri);

        SearchBroadcasterConfigDTO searchBroadcasterConfigDTO = new SearchBroadcasterConfigDTO();
        searchBroadcasterConfigDTO.setIdBroadcaster(idBroadcaster);
        // searchBroadcasterConfigDTO.setIdChannel(idCanale);
        BdcConfigDTO bdcConfigDto = broadcasterConfigService.findActive(searchBroadcasterConfigDTO);
        ResponseInformationFileInsertDTO response = new ResponseInformationFileInsertDTO();
        if (!parts[1].matches(bdcConfigDto.getSupportedFormat())) {
            EsitoDTO esito = new EsitoDTO();
            esito.setCodice("10");
            esito.setDescrizioneEsito("Formato file non supportato");
            response.setEsito(esito);
            String responseString = gson.toJson(response);
            logger.error(responseString);
            return Response.ok(responseString).build();
        }
        if (s3Service.upload(s3Uri.getBucket(), s3Uri.getKey(), uploadedInputStream, fileName)) {
            ResponseDataInformationFileDTO responseDataInformationFileDTO = new ResponseDataInformationFileDTO();
            final BdcInformationFileEntity bdcInformationFileEntity = new BdcInformationFileEntity();
            bdcInformationFileEntity.setEmittente(bdcBroadcastersService.findById(idBroadcaster));
            bdcInformationFileEntity.setIdUtente(bdcUtentiService.findById(idUtente));
            if (idCanale != null) {
                bdcInformationFileEntity.setCanale(bdcCanaliService.findById(idCanale));
            }
            bdcInformationFileEntity.setNomeFile(fileName);
            bdcInformationFileEntity.setAnno(anno);
            bdcInformationFileEntity.setMese(mese);
            bdcInformationFileEntity.setDataUpload(Calendar.getInstance().getTime());
            bdcInformationFileEntity.setPercorso(percorso);
            if (idCanale == null && anno == null && mese == null) {
                bdcInformationFileEntity.setTipoUpload("MASSIVO");
            } else if (idCanale == null || anno == null || mese == null) {
                List<String> err = new ArrayList<>();
                err.add("Il format del caricamento SINGOLO prevede l'inserimento dei valori nei campi canale, anno e mese; mentre "
                        + "il format del caricamento MASSIVO prevede che i campi canale, anno e mese vengano lasciati vuoti.");
                EsitoDTO esito = new EsitoDTO();
                esito.setCodice("10");
                esito.setDescrizioneEsito("Input error");
                esito.setLog(err);
                response.setEsito(esito);
                String responseString = gson.toJson(response);
                logger.error(responseString);
                return Response.ok(responseString).build();
            } else {
                bdcInformationFileEntity.setTipoUpload("SINGOLO");
            }
            bdcInformationFileEntity.setStato(Stato.DA_ELABORARE);
            Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
            Set<ConstraintViolation<BdcInformationFileEntity>> violations = validator
                    .validate(bdcInformationFileEntity);
            if (!violations.isEmpty()) {
                List<String> errorLog = new ArrayList<>();
                for (ConstraintViolation<BdcInformationFileEntity> violation : violations) {
                    errorLog.add(violation.getPropertyPath() + " " + violation.getMessage());
                }
                EsitoDTO esito = new EsitoDTO();
                esito.setCodice("10");
                esito.setDescrizioneEsito("Input error");
                esito.setLog(errorLog);
                response.setEsito(esito);
                String responseString = gson.toJson(response);
                logger.error(responseString);
                return Response.ok(responseString).build();
            }
            try {
                informationFileEntityService.save(bdcInformationFileEntity);
            } catch (Exception e) {
                logger.error(e.getMessage());
                // entityManager.getTransaction().rollback();
                EsitoDTO esitoDTO = new EsitoDTO();
                esitoDTO.setCodice("500");
                esitoDTO.setDescrizioneEsito("KO");
                List<String> log = new ArrayList<>();
                for (StackTraceElement ste : e.getStackTrace()) {
                    log.add(ste.toString());
                }
                esitoDTO.setLog(log);
                response.setEsito(esitoDTO);
                return Response.status(500).entity(response).build();
            }
            EsitoDTO esito = new EsitoDTO();
            esito.setCodice("00");
            esito.setDescrizioneEsito("OK");
            response.setEsito(esito);
            responseDataInformationFileDTO.setOutput("Upload file e persistenza in tabella avvenuti con successo");
            response.setResponseData(responseDataInformationFileDTO);
            return Response.ok(response).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
    
    @GET
    @Path("fe/utilizationfile/download/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response downloadById(@PathParam("id") Integer id) {
        informationFileEntityService.findById(id);
        if (null == id) {
            return Response.status(Status.NOT_FOUND).build();
        }
        final AmazonS3URI s3Uri = new AmazonS3URI(informationFileEntityService.findById(id).getPercorso());

        StreamingOutput stream = new StreamingOutput() {
            @Override
            public void write(OutputStream outputStream) throws IOException, WebApplicationException {
                InputStream is =s3Service.download(s3Uri.getBucket(), s3Uri.getKey());
                int nextByte = 0;
                while((nextByte  = is.read()) != -1 ){
                    outputStream.write(nextByte);
                }
                outputStream.flush();
                outputStream.close();
                is.close();
            }
        };

        return Response.ok(stream)
                .type(MediaType.APPLICATION_OCTET_STREAM)
                .header("Content-Disposition", "attachment; filename=\"" + informationFileEntityService.findById(id).getNomeFile() + "\"")
                .build();
    }

    @POST
    @Path("fe/utilizationfile/downloadFileArmonizato")
    @Produces("text/csv")
    public Response downloadFileArmonizato(FileArmonizzatoRequestDTO fileArmonizzatoRequestDTO) {

        if (null == fileArmonizzatoRequestDTO || fileArmonizzatoRequestDTO.getCanali() == null
                || fileArmonizzatoRequestDTO.getCanali().size() == 0 || null == fileArmonizzatoRequestDTO.getAnno()
                || null == fileArmonizzatoRequestDTO.getMesi() || fileArmonizzatoRequestDTO.getMesi().size() == 0) {
            return Response.status(Status.BAD_REQUEST).build();
        }

        StreamingOutput so;
        try {
            if (fileArmonizzatoRequestDTO.getBroadcasters().getTipo_broadcaster().toString().contains("TELEVISIONE")) {
                if(fileArmonizzatoRequestDTO.getBroadcasters().getNuovoTracciato().equalsIgnoreCase("S")){
                    final List<FileNewTracciatoArmonizzatoTvDTO> armonizzatoDTOs = bdcFileArmonizzatoService.getFileArmonizzatoNewTracciato(
                            fileArmonizzatoRequestDTO.getBroadcasters(), fileArmonizzatoRequestDTO.getCanali(),
                            fileArmonizzatoRequestDTO.getAnno(), fileArmonizzatoRequestDTO.getMesi());

                    if (armonizzatoDTOs.size() != 0) {
                        logger.info("TV - S "+armonizzatoDTOs.size());
                        CustomMappingStrategy<FileNewTracciatoArmonizzatoTvDTO> customMappingStrategy = new CustomMappingStrategy<>(new FileNewTracciatoArmonizzatoTvDTO().getMappingStrategy());
                        customMappingStrategy.setType(FileNewTracciatoArmonizzatoTvDTO.class);
                        so = getStreamingOutput(armonizzatoDTOs, customMappingStrategy);
                    } else {
                        return Response.status(500)
                                .entity("Questa selezione non ha prodotto risultati per il Download Armonizzato TV")
                                .build();
                    }
                }else {
                    final List<FileArmonizzatoTvDTO> armonizzatoDTOs = bdcFileArmonizzatoService.getFileArmonizzato(
                            fileArmonizzatoRequestDTO.getBroadcasters(), fileArmonizzatoRequestDTO.getCanali(),
                            fileArmonizzatoRequestDTO.getAnno(), fileArmonizzatoRequestDTO.getMesi());

                    if (armonizzatoDTOs.size() != 0) {
                        logger.info("TV - N "+armonizzatoDTOs.size());
                        CustomMappingStrategy<FileArmonizzatoTvDTO> customMappingStrategy = new CustomMappingStrategy<>(new FileArmonizzatoTvDTO().getMappingStrategy());
                        customMappingStrategy.setType(FileArmonizzatoTvDTO.class);
                        so = getStreamingOutput(armonizzatoDTOs, customMappingStrategy);
                    } else {
                        return Response.status(500)
                                .entity("Questa selezione non ha prodotto risultati per il Download Armonizzato TV")
                                .build();
                    }
                }

            } else {
                if(fileArmonizzatoRequestDTO.getBroadcasters().getNuovoTracciato().equalsIgnoreCase("S")){
                    List<FileNewTracciatoArmonizzatoRadioDTO> armonizzatoDTOs = bdcFileArmonizzatoService.getFileArmonizzatoRadioNewTracciato(
                            fileArmonizzatoRequestDTO.getBroadcasters(), fileArmonizzatoRequestDTO.getCanali(),
                            fileArmonizzatoRequestDTO.getAnno(), fileArmonizzatoRequestDTO.getMesi());
                    if (armonizzatoDTOs.size() != 0) {
                        logger.info("RADIO - S "+armonizzatoDTOs.size());
                        CustomMappingStrategy<FileNewTracciatoArmonizzatoRadioDTO> customMappingStrategy = new CustomMappingStrategy<>(new FileNewTracciatoArmonizzatoRadioDTO().getMappingStrategy());
                        customMappingStrategy.setType(FileNewTracciatoArmonizzatoRadioDTO.class);
                        so = getStreamingOutput(armonizzatoDTOs, customMappingStrategy);
                    } else {
                        return Response.status(500)
                                .entity("Questa selezione non ha prodotto risultati per il Download Armonizzato Radio")
                                .build();
                    }
                }else {
                    List<FileArmonizzatoRadioDTO> armonizzatoDTOs = bdcFileArmonizzatoService.getFileArmonizzatoRadio(
                            fileArmonizzatoRequestDTO.getBroadcasters(), fileArmonizzatoRequestDTO.getCanali(),
                            fileArmonizzatoRequestDTO.getAnno(), fileArmonizzatoRequestDTO.getMesi());
                    if (armonizzatoDTOs.size() != 0) {
                        logger.info("RADIO - N "+armonizzatoDTOs.size());
                        CustomMappingStrategy<FileArmonizzatoRadioDTO> customMappingStrategy = new CustomMappingStrategy<>(new FileArmonizzatoRadioDTO().getMappingStrategy());
                        customMappingStrategy.setType(FileArmonizzatoRadioDTO.class);
                        so = getStreamingOutput(armonizzatoDTOs, customMappingStrategy);
                    } else {
                        return Response.status(500)
                                .entity("Questa selezione non ha prodotto risultati per il Download Armonizzato Radio")
                                .build();
                    }
                }
            }
            return Response.ok(so)
                    .type("text/csv")
                    .header("Content-Disposition", "attachment; filename=\"" + fileArmonizzatoRequestDTO.getHarmonizedFileName() + ".csv\"")
                    .build();
        } catch (Exception e) {
            logger.error(e.getMessage());
            return Response.status(500).entity(e.getStackTrace()).build();
        } finally {
        }
    }

    @GET
    @Path("fe/utilizationfile/download/armonizzato/{tipo_broadcaster}/{nuovoTracciato}")
    @Produces("application/vnd.ms-excel")
    public Response getFile(@PathParam("tipo_broadcaster") String tipoBroadcasters,@PathParam("nuovoTracciato") String nuovoTracciato) {
        File file;
        AmazonS3URI s3Uri;
        ByteArrayOutputStream out;
        if (TipoBroadcaster.TELEVISIONE.toString().equals(tipoBroadcasters)) {
//            if(nuovoTracciato.equalsIgnoreCase("S")){
//                file = new File(fileArmonizzatoTVNuovoTracciatoRai);
//                s3Uri = new AmazonS3URI(fileArmonizzatoTVNuovoTracciatoRai);
//            }else{
                file = new File(fileArmonizzatoTV);
                s3Uri = new AmazonS3URI(fileArmonizzatoTV);
//            }
        } else if (TipoBroadcaster.RADIO.toString().equals("RADIO")) {
//            if(nuovoTracciato.equalsIgnoreCase("S")){
//                file = new File(fileArmonizzatoRadioNuovoTracciatoRai);
//                s3Uri = new AmazonS3URI(fileArmonizzatoRadioNuovoTracciatoRai);
//            }else {
                file = new File(fileArmonizzatoRadio);
                s3Uri = new AmazonS3URI(fileArmonizzatoRadio);
//            }
        } else {
            return Response.ok(gson.toJson(String.valueOf("Selezione non disponibile"))).build();
        }
        out = new ByteArrayOutputStream();
        s3Service.download(s3Uri.getBucket(), s3Uri.getKey(), out);
        return Response.ok().type("text/csv").entity(out.toByteArray())
                .header("Content-Disposition", "attachment; filename=" + file.getName()).build();
    }

    @PATCH
    @Path("fe/utilizationfile/changestatus")
    @Produces("application/json")
    public Response updateFileStatus(RequestInformationFileInsertDTO requestInformationFileInsertDTO) {
        ResponseInformationFileInsertDTO response = new ResponseInformationFileInsertDTO();
        ResponseDataInformationFileDTO responseDataInformationFileDTO = new ResponseDataInformationFileDTO();
        response.setHeader(requestInformationFileInsertDTO.getHeader());

        if (requestInformationFileInsertDTO.getRequestData().getStato().equals("DA_ELABORARE")) {
            return Response.status(500).entity("Impossibile settare lo stato DA_ELABORARE").build();
        }

        try {
            BdcInformationFileEntity entity = informationFileEntityService
                    .findById(requestInformationFileInsertDTO.getRequestData().getId());
            entity.setStato(requestInformationFileInsertDTO.getRequestData().getStato());
            entity.setDataProcessamento(Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTime());
            informationFileEntityService.save(entity);
        } catch (Exception e) {
            logger.error(e.getMessage());
            EsitoDTO esitoDTO = new EsitoDTO();
            esitoDTO.setCodice("500");
            esitoDTO.setDescrizioneEsito("KO");
            List<String> log = new ArrayList<>();
            log.add("Errore input id");
            esitoDTO.setLog(log);
            response.setEsito(esitoDTO);
            return Response.status(500).entity(response).build();
        }
        EsitoDTO esito = new EsitoDTO();
        esito.setCodice("00");
        esito.setDescrizioneEsito("OK");
        response.setEsito(esito);
        responseDataInformationFileDTO.setOutput("Update stato e data elaborazione avvenuto con successo");
        response.setResponseData(responseDataInformationFileDTO);
        return Response.ok(response).build();
    }

    @POST
    @Path("fe/utilizationfile/listfile")
    @Produces("application/json")
    public Response listUtilizationFile(BdcRequestDataDTO request) {
        BdcResponseListFileDTO response = new BdcResponseListFileDTO();
        response.setHeader(request.getHeader());
        BdcResponseDataDTO bdcResponseDataDTO = new BdcResponseDataDTO();
        try {
            if (request.getRequestData().getEmittente().equals(null)
                    && StringUtils.isEmpty(request.getRequestData().getStato())) {
                bdcResponseDataDTO.setOutput(informationFileEntityService.getListaFiles(
                        request.getRequestData().getEmittente(), Stato.valueOf(request.getRequestData().getStato())));
            } else {
                bdcResponseDataDTO.setOutput(informationFileEntityService.getListaFiles(
                        request.getRequestData().getEmittente(), Stato.valueOf(request.getRequestData().getStato())));
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            EsitoDTO esitoDTO = new EsitoDTO();
            esitoDTO.setCodice("500");
            esitoDTO.setDescrizioneEsito("KO");
            List<String> log = new ArrayList<>();
            log.add("Errore input stato.");
            esitoDTO.setLog(log);
            response.setEsito(esitoDTO);
            return Response.status(500).entity(response).build();
        }
        EsitoDTO esito = new EsitoDTO();
        esito.setCodice("00");
        esito.setDescrizioneEsito("OK");
        response.setEsito(esito);
        response.setResponseData(bdcResponseDataDTO);
        return Response.ok(gson.toJson(response)).build();
    }

    @POST
    @Path("fe/utilizationfile/listfilebroadcaster")
    @Produces("application/json")
    public Response listUtilizationFileBdc(BdcRequestDataDTO request) {
//        GsonBuilder builder = new GsonBuilder();
//        builder.excludeFieldsWithoutExposeAnnotation();
//        Gson gson = builder.create();
        BdcResponseListFileDTO response = new BdcResponseListFileDTO();
        response.setHeader(request.getHeader());
        BdcResponseDataDTO bdcResponseDataDTO = new BdcResponseDataDTO();
        try {
            if (request.getRequestData().getEmittente() != null) {
                bdcResponseDataDTO.setOutput(
                        informationFileEntityService.getListaFilesBroadcaster(request.getRequestData().getEmittente(), Repertorio.valueOfDescription( request.getRequestData().getRepertorio())));
            } else {
                bdcResponseDataDTO.setOutput(new ArrayList<BdcInformationFileEntity>());
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            EsitoDTO esitoDTO = new EsitoDTO();
            esitoDTO.setCodice("500");
            esitoDTO.setDescrizioneEsito("KO");
            List<String> log = new ArrayList<>();
            log.add("Errore input broadcaster.");
            esitoDTO.setLog(log);
            response.setEsito(esitoDTO);
            return Response.status(500).entity(response).build();
        }
        EsitoDTO esito = new EsitoDTO();
        esito.setCodice("00");
        esito.setDescrizioneEsito("OK");
        response.setEsito(esito);
        response.setResponseData(bdcResponseDataDTO);
        return Response.ok(gson.toJson(response)).build();
    }
   
    @POST
    @Path("fe/utilizationfile/listutilarmon")
    @Produces("application/json")
    public Response listUtilArmon(RequestUtilizzazioniArmonizzateDTO requestUtilizzazioniArmonizzateDTO) {


        List<UtilizzazioniArmonizzatoDTO> fileArmonizzatoTvDTO = bdcFileArmonizzatoService.getUtilizzazioniArmonizzate(
                requestUtilizzazioniArmonizzateDTO.getBroadcasters(),
                requestUtilizzazioniArmonizzateDTO.getCanali(),
                requestUtilizzazioniArmonizzateDTO.getTitoloTrasmissione(),
                requestUtilizzazioniArmonizzateDTO.getInizioTrasmissioneFrom(),
                requestUtilizzazioniArmonizzateDTO.getInizioTrasmissioneTo());

        return Response.ok(gson.toJson(fileArmonizzatoTvDTO)).build();
    }


    @POST
    @Path("fe/utilizationfile/utilizzazioniArmonizzateDettaglio")
    @Produces("application/json")
    public Response dettaglio(BdcRequestUtilizzazioniDettaglioDTO bdcRequestUtilizzazioniDettaglioDTO) {

        BdcReportDTO result = new BdcReportDTO();
        result.setBdcCampoDTO(
                bdcFileArmonizzatoService.getScheduleField(bdcRequestUtilizzazioniDettaglioDTO.getBdcBroadcasters(),
                        bdcRequestUtilizzazioniDettaglioDTO.getIdShowSchedule(),
                        bdcRequestUtilizzazioniDettaglioDTO.isError()));
        if (!bdcRequestUtilizzazioniDettaglioDTO.isError()) {
            result.setShowMusicDTO(bdcFileArmonizzatoService.getShowMusic(bdcRequestUtilizzazioniDettaglioDTO.getBdcBroadcasters(),
                    bdcRequestUtilizzazioniDettaglioDTO.getIdShowSchedule(),
                    bdcRequestUtilizzazioniDettaglioDTO.isError()));
        }
        return Response.ok(gson.toJson(result)).build();
    }


    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("fe/utilizationfile/cancelutilizationrecords")
    @Produces(MediaType.APPLICATION_JSON)
    public Response cancelUtilizationRecors(IdFileRequestDTO request) {
        Boolean ok = informationFileEntityService.cancelUtilizationRecords(request.getId());
        if (ok) {
            BdcInformationFileEntity bdcInformationFileEntity = informationFileEntityService.findById(request.getId());
            return Response.ok(gson.toJson(bdcInformationFileEntity)).build();
        } else
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("fe/utilizationfile/confirmutilizationrecords")
    @Produces(MediaType.APPLICATION_JSON)
    public Response confirmUtilizationRecords(IdFileRequestDTO request) {
        Boolean ok = informationFileEntityService.confirmUtilizationRecords(request.getId());
        if (ok) {
            BdcInformationFileEntity bdcInformationFileEntity = informationFileEntityService.findById(request.getId());
            return Response.ok(gson.toJson(bdcInformationFileEntity)).build();
        } else
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
    }


    private <T> StreamingOutput getStreamingOutput(final List<T> obj, final CustomMappingStrategy mappingStrategy) {
        StreamingOutput so = new StreamingOutput() {
            @Override
            public void write(OutputStream outputStream) throws IOException, WebApplicationException {
                OutputStreamWriter osw = new OutputStreamWriter(outputStream);

                StatefulBeanToCsv beanToCsv =
                        new StatefulBeanToCsvBuilder(osw)
                                .withMappingStrategy(mappingStrategy)
                                .withSeparator(';')
                                .build();
                try {
                    beanToCsv.write(obj);
                    osw.flush();
                    outputStream.flush();
                } catch (CsvDataTypeMismatchException e) {
                    logger.error(e.getMessage());
                } catch (CsvRequiredFieldEmptyException e) {
                    logger.error(e.getMessage());
                } finally {
                    if (osw != null)
                        osw.close();
                }
            }
        };
        return so;
    }
}
