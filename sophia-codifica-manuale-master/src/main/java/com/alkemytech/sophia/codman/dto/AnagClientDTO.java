package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.codman.entity.AnagClient;
import com.alkemytech.sophia.codman.persistence.AbstractEntity;

@Produces("application/json")
@XmlRootElement
public class AnagClientDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4914905532583323965L;

	private Integer idAnagClient;
	private String code;
	private String idDsp;
	private String dspName;
	private String companyName;
	private String country;
	private String vatCode;
	private String licence;
	private String vatDescription;
	private Boolean foreignClient;
	private Date startDate;
	private Date endDate;

	public AnagClientDTO() {

	}

	public AnagClientDTO(AnagClient entity) {

		this.idAnagClient = entity.getIdAnagClient();
		this.code = entity.getCode();
		this.idDsp = entity.getIdDsp();
		this.companyName = entity.getCompanyName();
		this.country = entity.getCountry();
		this.vatCode = entity.getVatCode();
		this.licence = entity.getLicence();
		this.vatDescription = entity.getVatDescription();
		this.foreignClient = entity.getForeignClient();
		this.startDate = entity.getStartDate();
		this.endDate = entity.getEndDate();

	}

	public Integer getIdAnagClient() {
		return idAnagClient;
	}

	public void setIdAnagClient(Integer idAnagClient) {
		this.idAnagClient = idAnagClient;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getIdDsp() {
		return idDsp;
	}

	public void setIdDsp(String idDsp) {
		this.idDsp = idDsp;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getVatCode() {
		return vatCode;
	}

	public void setVatCode(String vatCode) {
		this.vatCode = vatCode;
	}

	public String getLicence() {
		return licence;
	}

	public void setLicence(String licence) {
		this.licence = licence;
	}

	public String getVatDescription() {
		return vatDescription;
	}

	public void setVatDescription(String vatDescription) {
		this.vatDescription = vatDescription;
	}

	public Boolean getForeignClient() {
		return foreignClient;
	}

	public void setForeignClient(Boolean foreignClient) {
		this.foreignClient = foreignClient;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getDspName() {
		return dspName;
	}

	public void setDspName(String dspName) {
		this.dspName = dspName;
	}
	
	
}
