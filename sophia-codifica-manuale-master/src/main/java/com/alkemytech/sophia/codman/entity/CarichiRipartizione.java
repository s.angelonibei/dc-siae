package com.alkemytech.sophia.codman.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "PERF_CARICO_RIPARTIZIONE")
public class CarichiRipartizione implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_PERF_CARICO_RIPARTIZIONE")
	private Long idPerfCaricoRipartizione;

	@Column(name = "ID_PERIODO_RIPARTIZIONE")
	private Long idPeriodoRipartizione;

	@Column(name = "COD_VOCE_INCASSO")
	private String voceIncasso;

	@Column(name = "ID_RUN_VALORIZZAZIONE")
	private Long idRunValorizzazione;

	@Column(name = "VALORE_INCASSO_NETTO")
	private Double valoreIncassoNetto;

	@Column(name = "VALORE_PM_MANCANTI")
	private Double valorePmMancanti;

	@Column(name = "VALORE_PM_ANNULLATI")
	private Double valorePmAnnullati;

	@Column(name = "VALORE_CARICO_RIPARTIZIONE")
	private Double valoreCaricoRipartizione;

	@Column(name = "VALORE_NON_CODIFICATO")
	private Double valoreNonCodificato;

	@Column(name = "VALORE_BOLLETTINI_IRREGOLARI")
	private Double valoreBollettiniIrregolari;

	@Column(name = "VALORE_SOSPENSI")
	private Double valoreSospesi;

	@Column(name = "VALORE_RIPARTIBILE")
	private Double valoreRipartibile;

	@Column(name = "FLAG_IN_RIPARTIZIONE")
	private Boolean flagInRipartizione;
	
	@Column(name = "ID_PERIODO_RIPARTIZIONE_COMPETENZA")
	private Long idPeriodoRipartizioneCompetenza;

	
	public CarichiRipartizione() {
		super();
	}
	

	public CarichiRipartizione(Long idPerfCaricoRipartizione, Long idPeriodoRipartizione, String voceIncasso,
			Long idRunValorizzazione, Double valoreIncassoNetto, Double valorePmMancanti, Double valorePmAnnullati,
			Double valoreCaricoRipartizione, Double valoreNonCodificato, Double valoreBollettiniIrregolari,
			Double valoreSospesi, Double valoreRipartibile, Boolean flagInRipartizione, Long idPeriodoRipartizioneCompetenza) {
		super();
		this.idPerfCaricoRipartizione = idPerfCaricoRipartizione;
		this.idPeriodoRipartizione = idPeriodoRipartizione;
		this.voceIncasso = voceIncasso;
		this.idRunValorizzazione = idRunValorizzazione;
		this.valoreIncassoNetto = valoreIncassoNetto;
		this.valorePmMancanti = valorePmMancanti;
		this.valorePmAnnullati = valorePmAnnullati;
		this.valoreCaricoRipartizione = valoreCaricoRipartizione;
		this.valoreNonCodificato = valoreNonCodificato;
		this.valoreBollettiniIrregolari = valoreBollettiniIrregolari;
		this.valoreSospesi = valoreSospesi;
		this.valoreRipartibile = valoreRipartibile;
		this.flagInRipartizione = flagInRipartizione;
		this.idPeriodoRipartizioneCompetenza=idPeriodoRipartizioneCompetenza;
	}


	public Long getIdPerfCaricoRipartizione() {
		return idPerfCaricoRipartizione;
	}
	
	public void setIdPerfCaricoRipartizione(Long idPerfCaricoRipartizione) {
		this.idPerfCaricoRipartizione = idPerfCaricoRipartizione;
	}
	
	public Long getIdPeriodoRipartizione() {
		return idPeriodoRipartizione;
	}
	
	public void setIdPeriodoRipartizione(Long idPeriodoRipartizione) {
		this.idPeriodoRipartizione = idPeriodoRipartizione;
	}
	
	public String getVoceIncasso() {
		return voceIncasso;
	}
	
	public void setVoceIncasso(String voceIncasso) {
		this.voceIncasso = voceIncasso;
	}
	
	public Long getIdRunValorizzazione() {
		return idRunValorizzazione;
	}
	
	public void setIdRunValorizzazione(Long idRunValorizzazione) {
		this.idRunValorizzazione = idRunValorizzazione;
	}
	
	public Double getValoreIncassoNetto() {
		return valoreIncassoNetto;
	}
	
	public void setValoreIncassoNetto(Double valoreIncassoNetto) {
		this.valoreIncassoNetto = valoreIncassoNetto;
	}
	
	public Double getValorePmMancanti() {
		return valorePmMancanti;
	}
	
	public void setValorePmMancanti(Double valorePmMancanti) {
		this.valorePmMancanti = valorePmMancanti;
	}
	
	public Double getValorePmAnnullati() {
		return valorePmAnnullati;
	}
	
	public void setValorePmAnnullati(Double valorePmAnnullati) {
		this.valorePmAnnullati = valorePmAnnullati;
	}
	
	public Double getValoreCaricoRipartizione() {
		return valoreCaricoRipartizione;
	}
	
	public void setValoreCaricoRipartizione(Double valoreCaricoRipartizione) {
		this.valoreCaricoRipartizione = valoreCaricoRipartizione;
	}
	
	public Double getValoreBollettiniIrregolari() {
		return valoreBollettiniIrregolari;
	}
	
	public void setValoreBollettiniIrregolari(Double valoreBollettiniIrregolari) {
		this.valoreBollettiniIrregolari = valoreBollettiniIrregolari;
	}
	
	public Double getValoreSospesi() {
		return valoreSospesi;
	}
	
	public void setValoreSospesi(Double valoreSospesi) {
		this.valoreSospesi = valoreSospesi;
	}
	
	public Double getValoreRipartibile() {
		return valoreRipartibile;
	}
	
	public void setValoreRipartibile(Double valoreRipartibile) {
		this.valoreRipartibile = valoreRipartibile;
	}
	
	public Boolean isFlagInRipartizione() {
		return flagInRipartizione;
	}
	
	public void setFlagInRipartizione(Boolean flagInRipartizione) {
		this.flagInRipartizione = flagInRipartizione;
	}


	public Double getValoreNonCodificato() {
		return valoreNonCodificato;
	}


	public void setValoreNonCodificato(Double valoreNonCodificato) {
		this.valoreNonCodificato = valoreNonCodificato;
	}


	public Boolean getFlagInRipartizione() {
		return flagInRipartizione;
	}


	public Long getIdPeriodoRipartizioneCompetenza() {
		return idPeriodoRipartizioneCompetenza;
	}


	public void setIdPeriodoRipartizioneCompetenza(Long idPeriodoRipartizioneCompetenza) {
		this.idPeriodoRipartizioneCompetenza = idPeriodoRipartizioneCompetenza;
	}



}
