package com.alkemytech.sophia.codman.service.dsrmetadata.timestamp;

public class TimeStampV2 implements TimeStamp {

	private String regex = "[0-9]{14}";
	private String code = "YYYYMMDDHHmmss";
	private String description = "YYYYMMDDHHmmss";

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String getRegex() {
		return regex;
	}

	@Override
	public String getCode() {
		// TODO Auto-generated method stub
		return code;
	}

}