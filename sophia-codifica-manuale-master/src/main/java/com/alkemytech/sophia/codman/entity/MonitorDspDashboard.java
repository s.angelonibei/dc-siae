package com.alkemytech.sophia.codman.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity(name = "MonitorDspDashboard")
@Table(name = "MM_MONITOR_DSP_DASHBOARD", uniqueConstraints = @UniqueConstraint(columnNames = {"ID_DSP", "PERIODO", "ID_CONFIG"}))
@NamedQueries({@NamedQuery(name = "MonitorDspDashboard.getDashboardDsp", query = "select x from MonitorDspDashboard x where x.idDsp = :idDsp and x.periodo = :periodo"),
@NamedQuery(name = "MonitorDspDashboard.getUnidentified", query = "select x from MonitorDspDashboard x where x.idDsp = :idDsp and x.idConfig is null"),
@NamedQuery(name = "MonitorDspDashboard.doesRecordExist", query = "select x from MonitorDspDashboard x where x.idDsp = :idDsp and x.idConfig = :idConfig and x.periodo = :periodo")})
public class MonitorDspDashboard implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", insertable = false, nullable = false)
    private Integer id;

    @Column(name = "ID_DSP", nullable = false)
    private String idDsp;

    @Column(name = "PERIODO", nullable = false)
    private String periodo;

    @Column(name = "ID_CONFIG")
    private Integer idConfig;

    @Column(name = "DSR_ARRIVATI")
    private Integer dsrArrivati = 0;
}