package com.alkemytech.sophia.codman.entity.performing.dao;

import com.alkemytech.sophia.codman.dto.performing.CodificaDto;
import com.alkemytech.sophia.codman.dto.performing.CodificaOpereRequestDTO;
import com.alkemytech.sophia.codman.dto.performing.MonitoraggioCodificatoResponse;
import com.alkemytech.sophia.codman.dto.performing.RiepilogoCodificaManualeResponse;
import com.alkemytech.sophia.codman.entity.performing.ConfigurazioniSoglie;
import com.alkemytech.sophia.codman.rest.performing.TimerSessionResponse;

import java.util.List;

public interface ICodificaDAO {

	public void addConfigurazioneSoglie(ConfigurazioniSoglie configurazione);
	
	public void updateConfigurazioneSoglie(ConfigurazioniSoglie configurazione);
	
	public ConfigurazioniSoglie configurazioneIsInCorso();
	
	public ConfigurazioniSoglie getConfigurazioneSoglieById(Long id);
	
	public List<Long> getCodiciRegolaAfterDate(String inizioPeriodoCompetenza);

	public List<Long> getValidazioneDateOnUpdate(String inizioPeriodoCompetenza, String finePeriodoCompetenza, Long codiceConfigurazione);
	
	public List<ConfigurazioniSoglie> getListaConfigurazioniSoglieAttivi(String inizioPeriodoValidazione, String finePeriodoValidazione, String order, Integer page);
	
	public Integer countListaConfigurazioniSoglieAttivi(String inizioPeriodoValidazione, String finePeriodoValidazione);
	
	public List<CodificaDto> getListCodificaEsperto(String voceIncasso, String fonte, String titolo, String nominativo,String username, Integer limitCodifichePerforming);
	
	public List<CodificaDto> getListCodificaBase(String username, Integer limitCodifichePerforming);

	public List<ConfigurazioniSoglie> getStoricoConfigurazioneSoglie(Long codiceConfigurazione);

	public Long getMaxCodiceRegolaOnConfigurazioneSoglie();

    public List<Long> getValidazioneDate(String inizioPeriodoCompetenza, String finePeriodoCompetenza);

	public ConfigurazioniSoglie exportConfigurazioneSoglie(Long id);

	public ConfigurazioniSoglie getConfigurazioneSoglie(Long id);

	public void approvaOpera(CodificaOpereRequestDTO codificaOpereRequestDTO);

	public TimerSessionResponse getSessionTimer(String username);

	TimerSessionResponse refreshSession(String username);

    List<RiepilogoCodificaManualeResponse> getRiepilogoCodificaManualeResponseList(Long idPeriodoRipertizione, int page, String order);

    void endSessioneCodifica(String username);

    void refreshSessioneCodifica(String username, Long idCodifica);

    List<CodificaDto> refreshSessioneCodifica(String username, List<Object[]> codifiche);

    MonitoraggioCodificatoResponse getMonitoraggioCodificato(Long idPeriodoRipartizione, int page, String order);

}
