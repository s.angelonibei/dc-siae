package com.alkemytech.sophia.codman.multimedialelocale.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MultimedialeLocaleRequestDto {

	private String dataInvioDa;
	private String dataInvioA;
	private String dataDa;
	private String dataA;
	private String licenziatario;
	private String servizio;
	private String statoLogico;
	private String periodoRipartizione;
	private Integer first;
	private Integer last;
	private Integer maxrow;

	public MultimedialeLocaleRequestDto() {
		super();
	}

	public MultimedialeLocaleRequestDto(String dataInvioDa, String dataInvioA, String dataDa, String dataA,
			String licenziatario, String servizio, String statoLogico, String periodoRipartizione, Integer first, Integer last, Integer maxrow) {
		super();
		this.dataInvioDa = dataInvioDa;
		this.dataInvioA = dataInvioA;
		this.dataDa = dataDa;
		this.dataA = dataA;
		this.licenziatario = licenziatario;
		this.servizio = servizio;
		this.statoLogico = statoLogico;
		this.periodoRipartizione = periodoRipartizione;
		this.first = first;
		this.last = last;
		this.maxrow = maxrow;
	}

	public String getDataInvioDa() {
		return dataInvioDa;
	}

	public void setDataInvioDa(String dataInvioDa) {
		this.dataInvioDa = dataInvioDa;
	}

	public String getDataInvioA() {
		return dataInvioA;
	}

	public void setDataInvioA(String dataInvioA) {
		this.dataInvioA = dataInvioA;
	}

	public String getDataDa() {
		return dataDa;
	}

	public void setDataDa(String dataDa) {
		this.dataDa = dataDa;
	}

	public String getDataA() {
		return dataA;
	}

	public void setDataA(String dataA) {
		this.dataA = dataA;
	}

	public String getLicenziatario() {
		return licenziatario;
	}

	public void setLicenziatario(String licenziatario) {
		this.licenziatario = licenziatario;
	}

	public String getServizio() {
		return servizio;
	}

	public void setServizio(String servizio) {
		this.servizio = servizio;
	}

	public String getStatoLogico() {
		return statoLogico;
	}

	public void setStatoLogico(String statoLogico) {
		this.statoLogico = statoLogico;
	}

	public String getPeriodoRipartizione() {
		return periodoRipartizione;
	}

	public void setPeriodoRipartizione(String periodoRipartizione) {
		this.periodoRipartizione = periodoRipartizione;
	}

	public Integer getFirst() {
		return first;
	}

	public void setFirst(Integer first) {
		this.first = first;
	}

	public Integer getLast() {
		return last;
	}

	public void setLast(Integer last) {
		this.last = last;
	}

	public Integer getMaxrow() {
		return maxrow;
	}

	public void setMaxrow(Integer maxrow) {
		this.maxrow = maxrow;
	}
	

}
