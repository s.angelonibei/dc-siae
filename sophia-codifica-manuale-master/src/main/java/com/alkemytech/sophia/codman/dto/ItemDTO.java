
package com.alkemytech.sophia.codman.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.util.Date;

@XmlAccessorType(XmlAccessType.FIELD)
public class ItemDTO implements Serializable
{

    @Expose
    @XmlElement(name = "ID")
    @SerializedName("ID")
    private String id;
    @Expose
    @XmlElement(name = "TimeStamp")
    @SerializedName("TimeStamp")
    private Date timeStamp;
    @Expose
    @XmlElement(name = "Riferimento")
    @SerializedName("Riferimento")
    private String riferimento;
    private final static long serialVersionUID = -5601989329005698625L;

    public String getID() {
        return id;
    }

    public void setID(String iD) {
        this.id = iD;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getRiferimento() {
        return riferimento;
    }

    public void setRiferimento(String riferimento) {
        this.riferimento = riferimento;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("timeStamp", timeStamp).append("riferimento", riferimento).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(riferimento).append(timeStamp).append(id).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ItemDTO) == false) {
            return false;
        }
        ItemDTO rhs = ((ItemDTO) other);
        return new EqualsBuilder().append(riferimento, rhs.riferimento).append(timeStamp, rhs.timeStamp).append(id, rhs.id).isEquals();
    }

}
