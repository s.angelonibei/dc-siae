package com.alkemytech.sophia.codman.rest;

import com.alkemytech.sophia.codman.dto.DeliverCcidDTO;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.alkemytech.sophia.codman.utils.Periods;
import com.alkemytech.sophia.codman.utils.QueryUtils;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.config.ResultType;
import org.eclipse.persistence.sessions.Record;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.math.BigInteger;
import java.util.*;

import static org.apache.commons.collections4.CollectionUtils.isEmpty;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

@Singleton
@Path("deliverCCID")
@SuppressWarnings("unchecked")
public class DeliverCCIDService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final Provider<EntityManager> provider;
    private final Gson gson;
    private final SQS sqs;
    private final Properties configuration;

    @Inject
    protected DeliverCCIDService(@McmdbDataSource Provider<EntityManager> provider,
                                 Gson gson, SQS sqs,
                                 @Named("configuration") Properties configuration) {
        super();
        this.provider = provider;
        this.gson = gson;
        this.sqs = sqs;
        this.configuration = configuration;
    }

    @GET
    @Path("search")
    public Response search(@DefaultValue("0") @QueryParam("first") int first,
                           @DefaultValue("50") @QueryParam("last") int last,
                           @QueryParam("dspList") List<String> dspList,
                           @QueryParam("countryList") List<String> countryList,
                           @QueryParam("utilizationList") List<String> utilizationList,
                           @QueryParam("offerList") List<String> offerList,
                           @QueryParam("zipName") String zipName,
                           @DefaultValue("-1") @QueryParam("backclaim") int backclaim,
                           @DefaultValue("0") @QueryParam("monthFrom") int monthFrom,
                           @DefaultValue("0") @QueryParam("yearFrom") int yearFrom,
                           @DefaultValue("0") @QueryParam("monthTo") int monthTo,
                           @DefaultValue("0") @QueryParam("yearTo") int yearTo,
                           @DefaultValue("-1") @QueryParam("status") int status) {
        String sql = "select " +
                "       DM.IDDSR AS idDsr,\n" +
                "       DM.IDDSP AS idDsp,\n" +
                "       IF(DM.PERIOD_TYPE = 'month',\n" +
                "          concat(ELT(DM.PERIOD, 'Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu',\n" +
                "                     'Lug', 'Ago', 'Set', 'Ott', 'Nov', 'Dic'), ' ', DM.YEAR),\n" +
                "          concat(ELT(DM.PERIOD, 'Gen - Mar', 'Apr - Giu', 'Lug - Set',\n" +
                "                     'Ott - Dic'), ' ', DM.YEAR)) AS period ,\n" +
                "       AC.NAME AS country,\n" +
                "       CO.OFFERING AS offer,\n" +
                "       MIC.SCARICATO AS scaricato,\n" +
                "       MIC.CONFERMATO AS confermato,\n" +
                "       MIC.CONSEGNATO AS consegnato,\n" +
                "       MIC.DATA_SCARICO AS data_scarico,\n" +
                "       MIC.DATA_CONFERMA AS data_conferma,\n" +
                "       MIC.DATA_CONSEGNA AS data_consegna,\n" +
                "       MIC.PATH_SCARICO AS path_scarico,\n" +
                "       MIC.PATH_CONSEGNA AS path_consegna,\n" +
                "       MIC.ERRORE_CONSEGNA  AS errore, \n" +
                "       MIC.ZIPFILE AS zipfile         \n" +
                "from CCID_METADATA CM\n" +
                "join DSR_METADATA DM on CM.ID_DSR = DM.IDDSR\n" +
                "join COMMERCIAL_OFFERS CO on DM.SERVICE_CODE = CO.ID_COMMERCIAL_OFFERS\n" +
                "join ANAG_COUNTRY AC on DM.COUNTRY = AC.ID_COUNTRY\n" +
                "join ANAG_UTILIZATION_TYPE AUT on CO.ID_UTIILIZATION_TYPE = AUT.ID_UTILIZATION_TYPE\n" +
                "left join MM_INVIO_CCID MIC on CM.ID_CCID_METADATA = MIC.ID_CCID_METADATA\n" +
                "join ANAG_DSP DSP ON DSP.IDDSP = DM.IDDSP\n" +
                "join CCID_S3_PATH CS3P on DM.IDDSR = CS3P.IDDSR\n" +
                "where 1=1\n";

        String orderBy = "\norder by DM.IDDSP, DM.IDDSR";

        StringBuilder conditions = new StringBuilder();
        if (isNotEmpty(dspList))
            conditions.append(String.format("and DSP.IDDSP in (%s)\n", QueryUtils.getValuesCollection(dspList)));

        if (isNotEmpty(countryList))
            conditions.append(String.format("and AC.ID_COUNTRY in (%s)\n", QueryUtils.getValuesCollection(countryList)));

        if (isNotEmpty(utilizationList))
            conditions.append(String.format("and AUT.ID_UTILIZATION_TYPE in (%s)\n", QueryUtils.getValuesCollection(utilizationList)));

        if (isNotEmpty(offerList))
            conditions.append(String.format("and CO.OFFERING in (%s)\n", QueryUtils.getValuesCollection(offerList)));

        if (StringUtils.isNotEmpty(zipName))
            conditions.append(String.format("and MIC.ZIPFILE LIKE (%s)\n", "'%"+zipName+"%'"));

        switch (backclaim) {
            case 1:
                conditions.append("and (DM.IDDSR regexp '_BC_A_[0-9]{4}$' or DM.IDDSR regexp '_BC[0-9]{4}$')\n");
                break;
            case 2:
                conditions.append("and DM.IDDSR regexp '_BC_B_[0-9]{4}$' \n");
                break;
            case 3:
                conditions.append("and DM.IDDSR regexp '_BC_C1_[0-9]{4}$' \n");
                break;
            case 4:
                conditions.append("and DM.IDDSR regexp '_BC_C2_[0-9]{4}$' \n");
                break;
        }

        switch (status) {
            case 1:
                conditions.append("and SCARICATO\n");
                break;
            case 2:
                conditions.append("and CONFERMATO\n");
                break;
            case 3:
                conditions.append("and CONSEGNATO\n");
                break;
        }

        int defaultMonthFrom = 0;
        int defaultYearFrom = 2000;
        Date date = new Date();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        int defaultMonthTo = calendar.get(Calendar.MONTH);
        int defaultYearTo = calendar.get(Calendar.YEAR);

        if (monthFrom > 0 || monthTo > 0) {
            if (monthFrom > 0) {
                defaultMonthFrom = monthFrom;
                defaultYearFrom = yearFrom;
            }

            if (monthTo > 0) {
                defaultMonthTo = monthTo;
                defaultYearTo = yearTo;

            }
            Map<Integer, List<Integer>> quarters = Periods.extractCorrespondingQuarters(defaultMonthFrom, defaultYearFrom, defaultMonthTo, defaultYearTo);

            conditions.append(" and (");
            int y = 0;
            for (int key : quarters.keySet()) {
                if (y++ != 0) {
                    conditions.append(" or ");
                }
                conditions.append(" (DM.PERIOD_TYPE='quarter' and DM.PERIOD in (")
                        .append(StringUtils.join(quarters.get(key), ','))
                        .append(") and DM.YEAR = ").append(key).append(" )");
            }

            if (quarters.keySet().size() > 0)
                conditions.append("\nor\n");

            conditions.append(String.format("\n(DM.PERIOD_TYPE='month' " +
                            "and STR_TO_DATE(CONCAT(DM.YEAR,'-', DM.PERIOD , '-01'), '%%Y-%%m-%%d') >= '%s-%s-01'\n" +
                            "and STR_TO_DATE(CONCAT(DM.YEAR,'-', DM.PERIOD , '-01'), '%%Y-%%m-%%d') <= '%s-%s-01' )\n",
                    defaultYearFrom,
                    defaultMonthFrom,
                    defaultYearTo,
                    defaultMonthTo))
                    .append(" )\n");
        }

        EntityManager entityManager = provider.get();
        try {
            Query query = entityManager.createNativeQuery(sql + conditions.toString() + orderBy)
                    .setHint(QueryHints.RESULT_TYPE, ResultType.Map)
                    .setFirstResult(first)
                    .setMaxResults(1 + last - first);

            Query countQuery = entityManager.createNativeQuery("select count(0) from (" + sql + conditions.toString() + ") a");
            long count = (long) countQuery.getSingleResult();
            List<Record> result = query.getResultList();
            final PagedResult pagedResult = new PagedResult();
            if (null != result) {
                final int maxrows = last - first;
                final boolean hasNext = result.size() > maxrows;

                pagedResult.setFirst(first)
                        .setLast(hasNext ? last : first + result.size())
                        .setRows(hasNext ? result.subList(0, maxrows) : result)
                        .setHasNext(hasNext)
                        .setHasPrev(first > 0)
                        .setMaxrows(maxrows);
            } else {
                pagedResult.setMaxrows(0).setFirst(0).setLast(0).setHasNext(false).setHasPrev(false);
            }

            JsonObject extra = new JsonObject();
            extra.addProperty("totalResults", count);
            pagedResult.setExtra(extra);

            return Response.ok(gson.toJson(pagedResult)).build();
        } catch (Exception e) {
            logger.error("", e);
        }
        return Response.status(500).build();
    }

    @POST
    @Path("retrieveConfig")
    public Response retrieveConfig(DeliverCcidDTO request) {
        Set<String> ccidList = request.getCcid();

        if (isEmpty(ccidList))
            return Response.status(Response.Status.BAD_REQUEST).build();

        List<String> binds = new ArrayList<>(ccidList.size());
        for (String s : ccidList) {
            binds.add("?");
        }

        String sql = "select DM.IDDSR idDsr,\n" +
                "       AUT.ID_UTILIZATION_TYPE utilizationType,\n" +
                "       CO.OFFERING commercialOffer,\n" +
                "       CCID_S3_PATH.S3_PATH ccidS3Path,\n" +
                "       MMIC.HOST ftpHost,\n" +
                "       MMIC.PORT ftpPort,\n" +
                "       MDIC.PATH ftpPath,\n" +
                "       MIC.CONSEGNATO consegnato,\n" +
                "       max(MDIC.`RANK`)\n" +
                "from DSR_METADATA DM\n" +
                "         join CCID_S3_PATH on DM.IDDSR = CCID_S3_PATH.IDDSR\n" +
                "         join ANAG_DSP AD ON AD.IDDSP = DM.IDDSP\n" +
                "         join COMMERCIAL_OFFERS CO on DM.SERVICE_CODE = CO.ID_COMMERCIAL_OFFERS\n" +
                "         join ANAG_UTILIZATION_TYPE AUT on CO.ID_UTIILIZATION_TYPE = AUT.ID_UTILIZATION_TYPE\n" +
                "         join MM_MODALITA_INVIO_CCID MMIC on MMIC.IDDSP = AD.IDDSP\n" +
                "         join MM_DESTINAZIONE_INVIO_CCID MDIC on (MDIC.IDDSP = AD.IDDSP) AND\n" +
                "                                                 (MDIC.UTILIZATION_TYPE = AUT.ID_UTILIZATION_TYPE OR\n" +
                "                                                  MDIC.UTILIZATION_TYPE = '*') AND\n" +
                "                                                 (MDIC.COMMERCIAL_OFFERS = CO.ID_COMMERCIAL_OFFERS OR\n" +
                "                                                  MDIC.COMMERCIAL_OFFERS = '*')\n" +
                "         join CCID_METADATA CM ON CM.ID_DSR = DM.IDDSR\n" +
                "         left join MM_INVIO_CCID MIC ON MIC.ID_CCID_METADATA = CM.ID_CCID_METADATA\n" +
                "where DM.IDDSR in (" + StringUtils.join(binds, ",") + ")\n" +
                "group by DM.IDDSR, AUT.NAME, CO.ID_COMMERCIAL_OFFERS, CO.OFFERING, CCID_S3_PATH.S3_PATH, MMIC.HOST, MMIC.PORT, MDIC.PATH, MIC.CONSEGNATO";
        EntityManager entityManager = provider.get();

        try {
            Query query = entityManager.createNativeQuery(sql)
                    .setHint(QueryHints.RESULT_TYPE, ResultType.Map);

            int i = 0;
            for (String ccid : ccidList) {
                query.setParameter(++i, ccid);
            }

            List<Record> resultList = query.getResultList();
            Map<String, JsonObject> configs = new HashMap<>();
            JsonArray response = new JsonArray();
            for (Record record : resultList) {
                String ftpHost = (String) record.get("ftpHost");
                String ftpPort = (String) record.get("ftpPort");
                String ftpPath = (String) record.get("ftpPath");
                String idDsr = (String) record.get("idDsr");
                boolean consegnato = BooleanUtils.toBooleanDefaultIfNull((Boolean) record.get("consegnato"), false);
                if (configs.containsKey(ftpHost + ftpPort + ftpPath)) {
                    JsonObject element = configs.get(ftpHost + ftpPort + ftpPath);
                    JsonObject ccid = new JsonObject();
                    ccid.addProperty("idDsr", idDsr);
                    ccid.addProperty("consegnato", consegnato);
                    element.getAsJsonArray("ccid").add(ccid);
                } else {
                    JsonObject element = new JsonObject();
                    element.addProperty("ftpHost", ftpHost);
                    element.addProperty("ftpPort", ftpPort);
                    element.addProperty("ftpPath", ftpPath);
                    JsonArray ccidArray = new JsonArray();
                    JsonObject ccid = new JsonObject();
                    ccid.addProperty("idDsr", idDsr);
                    ccid.addProperty("consegnato", consegnato);
                    ccidArray.add(ccid);
                    element.add("ccid", ccidArray);
                    configs.put(ftpHost + ftpPort + ftpPath, element);
                }
                ccidList.remove(idDsr);
            }

            if (isNotEmpty(ccidList)) {
                JsonObject error = new JsonObject();
                error.addProperty("error", "nessuna configurazione trovata per i seguenti ccid: " + ccidList);
                return Response.ok(error.toString()).build();
            } else {
                for (JsonObject value : configs.values())
                    response.add(value);
                return Response.ok(response.toString()).build();
            }
        } catch (Exception e) {
            logger.error("retrieveConfig", e);
        }
        return Response.status(500).build();
    }

    @POST
    @Path("deliverToDsp")
    public Response deliverToDsp(DeliverCcidDTO request) {
        String sql = "select " +
                "       MMIC.IDDSP idDsp,\n" +
                "       MMIC.HOST ftpHost,\n" +
                "       MMIC.PORT ftpPort,\n" +
                "       MDIC.PATH ftpPath,\n" +
                "       MMIC.PROTOCOL protocol,\n" +
                "       MMIC.USER user,\n" +
                "       MMIC.PWD pwd,\n" +
                "       MMIC.CERT_PATH certPath,\n" +
                "       max(MDIC.`RANK`)\n" +
                "from DSR_METADATA DM\n" +
                "         join CCID_S3_PATH on DM.IDDSR = CCID_S3_PATH.IDDSR\n" +
                "         join ANAG_DSP AD ON AD.IDDSP = DM.IDDSP\n" +
                "         join COMMERCIAL_OFFERS CO on DM.SERVICE_CODE = CO.ID_COMMERCIAL_OFFERS\n" +
                "         join ANAG_UTILIZATION_TYPE AUT on CO.ID_UTIILIZATION_TYPE = AUT.ID_UTILIZATION_TYPE\n" +
                "         join MM_MODALITA_INVIO_CCID MMIC on MMIC.IDDSP = AD.IDDSP\n" +
                "         join MM_DESTINAZIONE_INVIO_CCID MDIC on (MDIC.IDDSP = AD.IDDSP) AND\n" +
                "                                                 (MDIC.UTILIZATION_TYPE = AUT.ID_UTILIZATION_TYPE OR\n" +
                "                                                  MDIC.UTILIZATION_TYPE = '*') AND\n" +
                "                                                 (MDIC.COMMERCIAL_OFFERS = CO.ID_COMMERCIAL_OFFERS OR\n" +
                "                                                  MDIC.COMMERCIAL_OFFERS = '*')\n" +
                "where DM.IDDSR in (%s)\n" +
                "group by MMIC.IDDSP, MMIC.HOST, MMIC.PORT, MDIC.PATH, MMIC.PROTOCOL, MMIC.USER, MMIC.PWD, MMIC.CERT_PATH";

        EntityManager entityManager = provider.get();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            JsonObject messageBody = new JsonObject();
            Record config = (Record) entityManager.createNativeQuery(String.format(sql, QueryUtils.getValuesCollection(request.getCcid())))
                    .setHint(QueryHints.RESULT_TYPE, ResultType.Map)
                    .getSingleResult();

            List<Record> ccidS3Paths = entityManager.createNativeQuery(String.format("select CCID_METADATA.ID_DSR, CCID_METADATA.ID_CCID_METADATA, CCID_S3_PATH.S3_PATH\n" +
                    "from CCID_METADATA join CCID_S3_PATH on CCID_METADATA.ID_DSR = CCID_S3_PATH.IDDSR\n" +
                    "where ID_DSR in (%s)", QueryUtils.getValuesCollection(request.getCcid())))
                    .setHint(QueryHints.RESULT_TYPE, ResultType.Map)
                    .getResultList();

            String ftpPath = StringUtils.isEmpty(request.getFtpPath()) ? (String) config.get("ftpPath") : request.getFtpPath();
            messageBody.addProperty("dsp", (String) config.get("idDsp"));
            messageBody.addProperty("host", (String) config.get("ftpHost"));
            messageBody.addProperty("port", (String) config.get("ftpPort"));
            messageBody.addProperty("path", ftpPath);
            messageBody.addProperty("protocol", (String) config.get("protocol"));
            messageBody.addProperty("user", (String) config.get("user"));
            messageBody.addProperty("pwd", (String) config.get("pwd"));
            messageBody.addProperty("cert_path", (String) config.get("certPath"));
            messageBody.addProperty("tipoTrasferimento", "CONSEGNA");
            messageBody.addProperty("zip",request.isZip());
            messageBody.addProperty("zipName",request.getZipName());
            messageBody.add("ccid", new JsonArray());

            transaction.begin();
            for (Record ccidS3Path : ccidS3Paths) {
                String idDsr = (String) ccidS3Path.get("ID_DSR");
                Long idCcidMetadata = (Long) ccidS3Path.get("ID_CCID_METADATA");
                String s3Path = (String) ccidS3Path.get("S3_PATH");


                Record invioCcid = null;
                try {
                    invioCcid = (Record) entityManager.createNativeQuery("select ID, ID_CCID_METADATA, CONSEGNATO FROM MM_INVIO_CCID WHERE ID_CCID_METADATA = " + idCcidMetadata)
                            .setHint(QueryHints.RESULT_TYPE, ResultType.Map)
                            .getSingleResult();
                } catch (NoResultException ignored) {
                }

                if (invioCcid == null) {
                    entityManager.createNativeQuery("insert into MM_INVIO_CCID (ID_CCID_METADATA, CONFERMATO, DATA_CONFERMA, PATH_CONSEGNA) " +
                            "VALUES (?, true, now(), ?)")
                            .setParameter(1, idCcidMetadata)
                            .setParameter(2, ftpPath)
                            .executeUpdate();
                } else {
                    entityManager.createNativeQuery("update MM_INVIO_CCID set CONFERMATO = TRUE, DATA_CONFERMA = NOW(), ID = LAST_INSERT_ID(ID), " +
                            "PATH_CONSEGNA = ?, CONSEGNATO = false, DATA_CONSEGNA = NULL WHERE ID_CCID_METADATA = ?")
                            .setParameter(1, ftpPath)
                            .setParameter(2, idCcidMetadata)
                            .executeUpdate();
                }

                BigInteger id = (BigInteger) entityManager.createNativeQuery("SELECT LAST_INSERT_ID()").getSingleResult();
                JsonObject ccid = new JsonObject();
                ccid.addProperty("idDsr", idDsr);
                ccid.addProperty("id", id);
                ccid.addProperty("path_s3", s3Path);
                messageBody.getAsJsonArray("ccid").add(ccid);
            }

            SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration, "deliver_ccid.sqs");
            sqsMessagePump.sendToProcessMessage(messageBody, false);

            transaction.commit();
        } catch (NoResultException e) {
            if (transaction.isActive())
                transaction.rollback();
            JsonObject error = new JsonObject();
            error.addProperty("error", "nessuna configurazione trovata per i ccid selezionati");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(error.toString()).build();
        } catch (NonUniqueResultException e) {
            if (transaction.isActive())
                transaction.rollback();
            JsonObject error = new JsonObject();
            error.addProperty("error", "trovate più configurazioni per i ccid selezionati");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(error.toString()).build();
        } catch (IllegalStateException e) {
            if (transaction.isActive())
                transaction.rollback();
            JsonObject error = new JsonObject();
            error.addProperty("error", e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(error.toString()).build();
        } catch (Exception e) {
            if (transaction.isActive())
                transaction.rollback();
            JsonObject error = new JsonObject();
            error.addProperty("error", "invio non riuscito");
            logger.error("deliverToDsp", e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(error.toString()).build();
        }

        return Response.ok().build();
    }

    @POST
    @Path("retrieveConfigShareSiae")
    public Response retrieveConfigShareSiae(DeliverCcidDTO request) {
        String host = configuration.getProperty("deliver_ccid.ftp_siae.host");
        String port = configuration.getProperty("deliver_ccid.ftp_siae.port");
        String path = configuration.getProperty("deliver_ccid.ftp_siae.default_path");

        JsonObject config = new JsonObject();
        config.addProperty("ftpHost", host);
        config.addProperty("ftpPort", port);
        config.addProperty("ftpPath", path);

        JsonArray result = new JsonArray();
        result.add(config);
        return Response.ok(result.toString()).build();
    }

    @POST
    @Path("deliverToShareSiae")
    public Response deliverToShareSiae(DeliverCcidDTO request) {
        final String protocol = configuration.getProperty("deliver_ccid.ftp_siae.protocol");
        final String host = configuration.getProperty("deliver_ccid.ftp_siae.host");
        final String port = configuration.getProperty("deliver_ccid.ftp_siae.port");
        final String username = configuration.getProperty("deliver_ccid.ftp_siae.username");
        final String password = configuration.getProperty("deliver_ccid.ftp_siae.password");
        final String path = StringUtils.isEmpty(request.getFtpPath()) ? configuration.getProperty("deliver_ccid.ftp_siae.default_path") : request.getFtpPath();

        EntityManager entityManager = provider.get();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            JsonObject messageBody = new JsonObject();

            List<Record> ccidS3Paths = entityManager.createNativeQuery(String.format("select CCID_METADATA.ID_DSR, CCID_METADATA.ID_CCID_METADATA, CCID_S3_PATH.S3_PATH\n" +
                    "from CCID_METADATA join CCID_S3_PATH on CCID_METADATA.ID_DSR = CCID_S3_PATH.IDDSR\n" +
                    "where ID_DSR in (%s)", QueryUtils.getValuesCollection(request.getCcid())))
                    .setHint(QueryHints.RESULT_TYPE, ResultType.Map)
                    .getResultList();

            //messageBody.addProperty("dsp", (String) config.get("idDsp"));
            messageBody.addProperty("host", host);
            messageBody.addProperty("port", port);
            messageBody.addProperty("path", path);
            messageBody.addProperty("protocol", protocol);
            messageBody.addProperty("user", username);
            messageBody.addProperty("pwd", password);
//            messageBody.addProperty("cert_path", (String) config.get("certPath"));
            messageBody.addProperty("tipoTrasferimento", "SCARICO");
            messageBody.add("ccid", new JsonArray());

            transaction.begin();
            for (Record ccidS3Path : ccidS3Paths) {
                String idDsr = (String) ccidS3Path.get("ID_DSR");
                String s3Path = (String) ccidS3Path.get("S3_PATH");
                Long idCcidMetadata = (Long) ccidS3Path.get("ID_CCID_METADATA");


                entityManager.createNativeQuery(String.format("insert into MM_INVIO_CCID set ID_CCID_METADATA = %s\n" +
                                "on duplicate key update SCARICATO = false, DATA_SCARICO = null, ID = LAST_INSERT_ID(ID)",
                        idCcidMetadata)).executeUpdate();
                BigInteger id = (BigInteger) entityManager.createNativeQuery("SELECT LAST_INSERT_ID()").getSingleResult();
                JsonObject ccid = new JsonObject();
                ccid.addProperty("idDsr", idDsr);
                ccid.addProperty("id", id);
                ccid.addProperty("path_s3", s3Path);
                messageBody.getAsJsonArray("ccid").add(ccid);
            }

            SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration, "deliver_ccid.sqs");
            sqsMessagePump.sendToProcessMessage(messageBody, false);

            transaction.commit();
        } catch (Exception e) {
            if (transaction.isActive())
                transaction.rollback();
            JsonObject error = new JsonObject();
            error.addProperty("error", "invio non riuscito");
            logger.error("deliverToDsp", e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(error.toString()).build();
        }

        return Response.ok().build();
    }


}