package com.alkemytech.sophia.codman.rest;

import com.alkemytech.sophia.codman.dto.DsrMetadataConfigDTO;
import com.alkemytech.sophia.codman.dto.DsrMetadataDTO;
import com.alkemytech.sophia.codman.dto.FormatMetadataDTO;
import com.alkemytech.sophia.codman.dto.JsonRegexConfigDTO;
import com.alkemytech.sophia.codman.entity.DsrMetadataConfig;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.alkemytech.sophia.codman.multimedialelocale.repository.IDsrMetadataRepository;
import com.alkemytech.sophia.codman.service.dsrmetadata.cardinality.BaseCardinality;
import com.alkemytech.sophia.codman.service.dsrmetadata.cardinality.Cardinality;
import com.alkemytech.sophia.codman.service.dsrmetadata.country.DSRCountryExtractorXX;
import com.alkemytech.sophia.codman.service.dsrmetadata.country.DSRCountryExtractorXXX;
import com.alkemytech.sophia.codman.service.dsrmetadata.country.DsrCountryExtractor;
import com.alkemytech.sophia.codman.service.dsrmetadata.country.DsrCountryExtractorXXLowerCase;
import com.alkemytech.sophia.codman.service.dsrmetadata.period.*;
import com.alkemytech.sophia.codman.service.dsrmetadata.timestamp.TimeStamp;
import com.alkemytech.sophia.codman.service.dsrmetadata.timestamp.TimeStampV1;
import com.alkemytech.sophia.codman.service.dsrmetadata.timestamp.TimeStampV2;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Singleton
@Path("dsrMetadata")
public class DsrMetadataService {

	private static final String COMP_KEY_PERIODO = "periodo";
	private static final String COMP_KEY_TERRITORIO = "territorio";
	private static final String COMP_KEY_STRINGA = "stringa";
	private static final String COMP_KEY_TIMESTAMP = "timestamp";
	private static final String COMP_KEY_CARDINALITY = "numberof";

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Provider<EntityManager> provider;
	private final Gson gson;
	private final IDsrMetadataRepository dsrMetadataRepository;

	private Map<String, DsrCountryExtractor> countryCodeToExtractorsMap;
	private Map<String, DsrPeriodExtractor> periodCodeToExtractorsMap;
	private Map<String, TimeStamp> timeStampCodeToRegex;
	private Map<String, Cardinality> cardinalityCodeToRegex;

	@Inject
	protected DsrMetadataService(@McmdbDataSource Provider<EntityManager> provider, Gson gson, IDsrMetadataRepository dsrMetadataRepository) {
		super();
		this.provider = provider;
		this.gson = gson;
		this.dsrMetadataRepository = dsrMetadataRepository;
		setRegexMap();
	}

	@GET
	@Path("all")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response all(@DefaultValue("0") @QueryParam("first") int first,
						@DefaultValue("50") @QueryParam("last") int last,
						@DefaultValue("") @QueryParam("dsp") String dsp,
						@DefaultValue("") @QueryParam("utilization") String utilization,
						@DefaultValue("") @QueryParam("offer") String offer) {
		EntityManager entityManager = null;
		List<Object[]> result;
		try {
			entityManager = provider.get();
			final Query q = entityManager
					.createQuery(
							"select x.idDsrMetadataConfig , x.idCommercialOffer , d.idDsp , d.name , u.idUtilizationType , "
									+ " u.name , c.offering ,  x.regex , x.regexConfig , "
									+ " x.regexRepresentation , x.countryCode , x.periodCode , x.startIndexCountry , "
									+ " x.endIndexCountry , x.startIndexPeriod , x.endIndexPeriod , d.code from DsrMetadataConfig x, CommercialOffers c ,  AnagDsp d, AnagUtilizationType u "
									+ " where x.idCommercialOffer = c.idCommercialOffering and c.idDSP = d.idDsp and c.idUtilizationType = u.idUtilizationType "
									+((!dsp.isEmpty() && !dsp.equals("*")) ? "  and d.idDsp = :dsp  " : "")
									+ ((!utilization.isEmpty() && !utilization.equals("*")) ? " and u.idUtilizationType = :utilization " : ""  )
									+(!offer.isEmpty() && !offer.equals("*") ? " and c.idCommercialOffering = :offer " : "")
									+ " order by d.name, u.name, c.offering")
					.setFirstResult(first) // offset
					.setMaxResults(1 + last - first);

			if (!dsp.isEmpty() && !dsp.equals("*")) {
				q.setParameter("dsp", dsp);
			}
			if (!utilization.isEmpty() && !utilization.equals("*")) {
				q.setParameter("utilization", utilization);
			}
			if (!offer.isEmpty() && !offer.equals("*")) {
				q.setParameter("offer", Integer.parseInt(offer));
			}

			result = (List<Object[]>) q.getResultList();

			List<DsrMetadataConfigDTO> dtoList = new ArrayList<>();
			final PagedResult pagedResult = new PagedResult();
			if (null != result) {
				final int maxrows = last - first;
				final boolean hasNext = result.size() > maxrows;
				for (Object[] o : (hasNext ? result.subList(0, maxrows) : result)) {
					dtoList.add(new DsrMetadataConfigDTO(o));
				}

				pagedResult.setRows(dtoList).setMaxrows(maxrows).setFirst(first)
						.setLast(hasNext ? last : first + result.size()).setHasNext(hasNext).setHasPrev(first > 0);

			} else {
				pagedResult.setMaxrows(0).setFirst(0).setLast(0).setHasNext(false).setHasPrev(false);
			}

			return Response.ok(gson.toJson(pagedResult)).build();
		} catch (Exception e) {
			logger.error("all", e);
		}
		return Response.status(500).build();
	}

	@GET
	@Path("configurationValues")
	@Produces("application/json")
	public Response configurationValues() {
		// EntityManager entityManager = null;
		Map<String, List<?>> result = new HashMap<>();
		try {

			// entityManager = provider.get();

			ArrayList<FormatMetadataDTO> periods = new ArrayList<>();

			for (String key : periodCodeToExtractorsMap.keySet()) {
				FormatMetadataDTO p = new FormatMetadataDTO();
				p.setCode(periodCodeToExtractorsMap.get(key).getCode());
				p.setDescription(periodCodeToExtractorsMap.get(key).getDescription());
				periods.add(p);
			}

			ArrayList<FormatMetadataDTO> countries = new ArrayList<>();

			for (String key : countryCodeToExtractorsMap.keySet()) {
				FormatMetadataDTO c = new FormatMetadataDTO();
				c.setCode(countryCodeToExtractorsMap.get(key).getCode());
				c.setDescription(countryCodeToExtractorsMap.get(key).getDescription());
				countries.add(c);
			}

			ArrayList<FormatMetadataDTO> timeStamps = new ArrayList<>();

			for (String key : timeStampCodeToRegex.keySet()) {
				FormatMetadataDTO t = new FormatMetadataDTO();
				t.setCode(timeStampCodeToRegex.get(key).getCode());
				t.setDescription(timeStampCodeToRegex.get(key).getDescription());
				timeStamps.add(t);
			}

			ArrayList<FormatMetadataDTO> cardinalities = new ArrayList<>();

			for (String key : cardinalityCodeToRegex.keySet()) {
				FormatMetadataDTO ca = new FormatMetadataDTO();
				ca.setCode(cardinalityCodeToRegex.get(key).getCode());
				ca.setDescription(cardinalityCodeToRegex.get(key).getDescription());
				cardinalities.add(ca);
			}

			result.put("periodTypes", periods);
			result.put("countryTypes", countries);
			result.put("timeStampTypes", timeStamps);
			result.put("cardinalityTypes", cardinalities);


			return Response.ok(gson.toJson(result)).build();
		} catch (Exception e) {
			logger.error("configurationValues", e);
		}
		return Response.status(500).build();
	}

	@POST
	@Path("")
	@SuppressWarnings("unchecked")
	public Response add(JsonRegexConfigDTO configDTO) {

		Status status = Status.INTERNAL_SERVER_ERROR;
		final EntityManager entityManager = provider.get();
		try {
			Gson gson = new Gson();
			ArrayList<HashMap<String, String>> compList = gson.fromJson(configDTO.getRegexStringValues(),
					new TypeToken<ArrayList<HashMap<String, String>>>() {
					}.getType());

			String regEx = "";
			int currCharNum = 0;
			String countryCode = "";
			String periodCode = "";
			int startIndexCountry = 0;
			int endIndexCountry = 0;
			int startIndexPeriod = 0;
			int endIndexPeriod = 0;

			for (HashMap<String, String> comp : compList) {
				for (String key : comp.keySet()) {
					switch (key) {
						case COMP_KEY_STRINGA: {
							regEx += comp.get(key);
							currCharNum += comp.get(key).length();
							break;
						}
						case COMP_KEY_TERRITORIO: {
							String terrExtractCode = comp.get(key);
							regEx += countryCodeToExtractorsMap.get(terrExtractCode).getRegex();
							countryCode = countryCodeToExtractorsMap.get(terrExtractCode).getCode();
							currCharNum += terrExtractCode.length();
							startIndexCountry = currCharNum - terrExtractCode.length();
							endIndexCountry = currCharNum - 1;
							break;
						}
						case COMP_KEY_PERIODO: {
							String periodExtractCode = comp.get(key);
							regEx += periodCodeToExtractorsMap.get(periodExtractCode).getRegex();
							periodCode = periodCodeToExtractorsMap.get(periodExtractCode).getCode();
							currCharNum += periodExtractCode.length();
							startIndexPeriod = currCharNum - periodExtractCode.length();
							endIndexPeriod = currCharNum - 1;
							break;
						}
						case COMP_KEY_TIMESTAMP: {
							String timeStampExtractCode = comp.get(key);
							regEx += timeStampCodeToRegex.get(timeStampExtractCode).getRegex();
							currCharNum += timeStampExtractCode.length();
							break;
						}
						case COMP_KEY_CARDINALITY: {
							String cardinalityExtractCode = comp.get(key);
							regEx += cardinalityCodeToRegex.get(cardinalityExtractCode).getRegex();
							currCharNum += cardinalityExtractCode.length();
							break;
						}
						default:
							break;
					}
				}
			}

			final Query query = entityManager.createQuery("select d.ftpSourcePath, m.regex "
					+ " from AnagDsp d, CommercialOffers c, DsrMetadataConfig m " + " where d.idDsp = c.idDSP "
					+ " and c.idCommercialOffering = m.idCommercialOffer " + " and m.regex =:regex "
					+ " and d.ftpSourcePath = (select x.ftpSourcePath from AnagDsp x where x.idDsp = :idDsp)");
			query.setParameter("regex", regEx);
			query.setParameter("idDsp", configDTO.getIdDsp());
			List<Object[]> result3 = (List<Object[]>) query.getResultList();
			if (result3 != null && !result3.isEmpty()) {
				status = Status.CONFLICT;
				return Response.status(status).build();
			}

			entityManager.getTransaction().begin();
			DsrMetadataConfig config = new DsrMetadataConfig();
			config.setIdCommercialOffer(configDTO.getCommercialOfferId());
			config.setRegex(regEx);
			config.setRegexConfig(configDTO.getRegexStringValues());
			config.setRegexRepresentation(configDTO.getRegexString());
			config.setCountryCode(countryCode);
			config.setPeriodCode(periodCode);
			config.setStartIndexCountry(startIndexCountry);
			config.setEndIndexCountry(endIndexCountry);
			config.setStartIndexPeriod(startIndexPeriod);
			config.setEndIndexPeriod(endIndexPeriod);

			entityManager.persist(config);
			entityManager.getTransaction().commit();

			return Response.ok().build();
		} catch (Exception e) {
			logger.error("add", e);

			if (e.getCause() != null && e.getCause().getCause() != null
					&& e.getCause().getCause() instanceof MySQLIntegrityConstraintViolationException) {
				status = Status.FORBIDDEN;
			}

			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}

		}

		return Response.status(status).build();
	}


	@DELETE
	@Path("")
	public Response delete(@QueryParam("idDsrMetadataConfig") int idDsrMetadataConfig) {
		final EntityManager entityManager = provider.get();
		try {
			entityManager.getTransaction().begin();
			final Query query = entityManager
					.createQuery("delete from DsrMetadataConfig where idDsrMetadataConfig = :idDsrMetadataConfig");
			query.setParameter("idDsrMetadataConfig", idDsrMetadataConfig);
			query.executeUpdate();
			entityManager.getTransaction().commit();
			return Response.ok().build();
		} catch (Exception e) {
			logger.error("delete", e);
			entityManager.getTransaction().rollback();
		}
		return Response.status(500).build();
	}

	@GET
	@Path("period-limits")
	public Response getPeriodLimits() {
		return Response.ok(gson.toJson(dsrMetadataRepository.getPeriodLimits())).build();
	}

	// @GET
	// @Path("extractDsrMetadata")
	// @Produces("application/json")
	// @SuppressWarnings("unchecked")
	// public Response extractMetadata(@QueryParam("dsp") String dsp,
	// @QueryParam("dsrName") String dsrName,
	// @QueryParam("returnError") Boolean returnError) {
	// // Fake implementation
	// DsrMetadataDTO metadata = new DsrMetadataDTO();
	//
	// if (returnError != null && true == returnError) {
	// metadata.setStatus("KO");
	// metadata.setErrorMessage("Nessuna configurazione disponibile per il DSR
	// indicato");
	// return Response.ok(gson.toJson(metadata)).build();
	// }
	//
	// if (null == dsp) {
	// metadata.setStatus("KO");
	// metadata.setErrorMessage("Manca il parametro 'dsp'");
	// return Response.ok(gson.toJson(metadata)).build();
	// }
	//
	// if (null == dsrName) {
	// metadata.setStatus("KO");
	// metadata.setErrorMessage("Manca il parametro 'dsrName'");
	// return Response.ok(gson.toJson(metadata)).build();
	// }
	//
	// metadata.setStatus("OK");
	//
	// metadata.setDsp(dsp);
	// metadata.setCommercialOffer("fake commercial offer");
	// metadata.setCountry("IT");
	// metadata.setUtilizationType("Download");
	// metadata.setPeriodType("mensile");
	// metadata.setPeriodStartDate("20160401");
	// metadata.setPeriodEndDate("20160430");
	//
	// return Response.ok(gson.toJson(metadata)).build();
	//
	// // return Response.status(500).build();
	// }

	private void setRegexMap() {
		countryCodeToExtractorsMap = new HashMap<>();
		periodCodeToExtractorsMap = new HashMap<>();
		timeStampCodeToRegex = new HashMap<>();
		cardinalityCodeToRegex = new HashMap<>();
		// Country codes
		DSRCountryExtractorXX c1 = new DSRCountryExtractorXX(provider);
		countryCodeToExtractorsMap.put(c1.getCode(),c1);
		DSRCountryExtractorXXX c2 = new DSRCountryExtractorXXX(provider);
		countryCodeToExtractorsMap.put(c2.getCode(),c2 );
		DsrCountryExtractorXXLowerCase c3 = new DsrCountryExtractorXXLowerCase(provider);
		countryCodeToExtractorsMap.put(c3.getCode(),c3);

		// Period codes
		DSRPeriodExtractorYYYYMM p1 = new DSRPeriodExtractorYYYYMM();
		periodCodeToExtractorsMap.put(p1.getCode(), p1);
		DSRPeriodExtractorYYYYMMv2 p2 = new DSRPeriodExtractorYYYYMMv2();
		periodCodeToExtractorsMap.put(p2.getCode(), p2);
		DSRPeriodExtractorYYYYMMv3 p3 = new DSRPeriodExtractorYYYYMMv3();
		periodCodeToExtractorsMap.put(p3.getCode(), p3);
		DSRPeriodExtractorYYYYMMDD_YYYYMMDD p4 = new DSRPeriodExtractorYYYYMMDD_YYYYMMDD();
		periodCodeToExtractorsMap.put(p4.getCode(), p4);
		DSRPeriodExtractorYYYYQX p5 = new DSRPeriodExtractorYYYYQX();
		periodCodeToExtractorsMap.put(p5.getCode(), p5);
		DSRPeriodExtractorYYYY_QX p6 = new DSRPeriodExtractorYYYY_QX();
		periodCodeToExtractorsMap.put(p6.getCode(), p6);
		DSRPeriodExtractorYYYY_minus_QX p7  = new DSRPeriodExtractorYYYY_minus_QX();
		periodCodeToExtractorsMap.put(p7.getCode(), p7);
		DSRPeriodExtractorYYYYTX p8  = new DSRPeriodExtractorYYYYTX();
		periodCodeToExtractorsMap.put(p8.getCode(), p8);
		DSRPeriodExtractorYYYYMM_YYYYMM p9  = new DSRPeriodExtractorYYYYMM_YYYYMM();
		periodCodeToExtractorsMap.put(p9.getCode(), p9);
		DsrPeriodExtractorXth_Qtr_YYYY p10 = new DsrPeriodExtractorXth_Qtr_YYYY();
		periodCodeToExtractorsMap.put(p10.getCode(), p10);
		DSRPeriodExtractorYYYYMMDD_YYYYMMDD_v2 p11 = new DSRPeriodExtractorYYYYMMDD_YYYYMMDD_v2();
		periodCodeToExtractorsMap.put(p11.getCode(), p11);

		//Time Stamp codes
		TimeStampV1 ts1 = new TimeStampV1();
		timeStampCodeToRegex.put(ts1.getCode(), ts1 );
		TimeStampV2 ts2 = new TimeStampV2();
		timeStampCodeToRegex.put(ts2.getCode(), ts2 );

		//Cardinality codes
		BaseCardinality ca1 = new BaseCardinality();
		cardinalityCodeToRegex.put(ca1.getCode(), ca1);
	}
	
	
	
	

	@GET
	@Path("extractDsrMetadata")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response extractMetadata(@QueryParam("dsp") String dsp, @QueryParam("dsrName") String dsrName) {

		DsrMetadataDTO metadata = new DsrMetadataDTO();
		
		
		if (null == dsp) {
			metadata.setStatus("KO");
			metadata.setErrorMessage("Manca il parametro 'dsp'");
			return Response.ok(gson.toJson(metadata)).build();
		}

		if (null == dsrName) {
			metadata.setStatus("KO");
			metadata.setErrorMessage("Manca il parametro 'dsrName'");
			return Response.ok(gson.toJson(metadata)).build();
		}

		metadata = extractMetadataQuery(dsp,dsrName,provider.get());

		return Response.ok(gson.toJson(metadata)).build();
	}

	@GET
	@Path("saveStorico/{idDsr}")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response saveStorico(@PathParam("idDsr") String idDsr) {
		EntityManager entityManager = null;
		entityManager = provider.get();
		entityManager.getTransaction().begin();
		entityManager.createNativeQuery("INSERT INTO MM_STATISTICHE_STORICO\n" +
				"  (\n" +
				"   DSP ,DSR ,DATA_RICEZIONE_DSR ,PERIOD_TYPE ,PERIOD ,YEAR ,PERIODO ,TERRITORIO ,TIPO_UTILIZZO ,\n" +
				"   OFFERTA_COMMERCIALE ,TOTALE_VALORE ,CURRENCY ,TOTALE_UTILIZZAZIONI ,IDENTIFICATO ,ID_CCID_PATH ,\n" +
				"   IDDSR ,S3_PATH ,ID_CCID ,ID_DSR ,TOTAL_VALUE ,ID_CCID_METADATA ,CURRENCY2 ,CCID_VERSION ,\n" +
				"   CCID_ENCODED ,CCID_ENCODED_PROVISIONAL ,CCID_NOT_ENCODED ,INVOICE_STATUS ,INVOICE_ITEM ,\n" +
				"   VALORE_FATTURABILE ,SUM_USE_QUANTITY ,SUM_USE_QUANTITY_MATCHED ,SUM_USE_QUANTITY_UNMATCHED ,\n" +
				"   SUM_AMOUNT_LICENSOR ,SUM_AMOUNT_PAI ,SUM_AMOUNT_UNMATCHED ,SUM_USE_QUANTITY_SIAE ,VALORE_RESIDUO ,\n" +
				"   QUOTA_FATTURA ,IDENTIFICATO_VALORE_PRICING ,TOTAL_VALUE_CCID_CURRENCY\n" +
				"   )\n" +
				"   select *\n" +
				" from (select IF(ANAG_DSP.NAME IS NULL, '', ANAG_DSP.NAME) AS DSP,\n" +
				"        IF(M1.IDDSR IS NULL, '', M1.IDDSR) AS DSR,\n" +
				"\t\t\t\tIF(steps_monitoring.TIMESTAMP IS NULL , ' ',\n" +
				"\t\t\t\t  DATE_FORMAT( CONVERT_TZ(from_unixtime(steps_monitoring.TIMESTAMP,'%Y-%m-%d %H:%i'),'+00:00','+01:00') ,'%d-%m-%Y %H:%i') )\n" +
				"    AS DATA_RICEZIONE_DSR,\n" +
				"        M1.PERIOD_TYPE,\n" +
				"\t\t\t\tM1.PERIOD,\n" +
				"        M1.YEAR,\n" +
				"\t\t(CASE\n" +
				"        WHEN M1.PERIOD_TYPE = 'month' THEN\n" +
				"\t\t\t\tconcat(ELT(M1.PERIOD, 'Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov','Dic') , ' ', M1.YEAR)\n" +
				"        ELSE concat(ELT(M1.PERIOD\n" +
				"           , 'Gen - Mar'\n" +
				"           , 'Apr - Giu'\n" +
				"           , 'Lug - Set'\n" +
				"           , 'Ott - Dic')\n" +
				"           , ' '\n" +
				"           , M1.YEAR) END) AS PERIODO,\n" +
				"     IF(M1.COUNTRY IS NULL, ' ', M1.COUNTRY) AS TERRITORIO,\n" +
				"     IF(ANAG_UTILIZATION_TYPE.NAME IS NULL, ' ', ANAG_UTILIZATION_TYPE.NAME) AS TIPO_UTILIZZO,\n" +
				"     IF(COMMERCIAL_OFFERS.OFFERING IS NULL, ' ', COMMERCIAL_OFFERS.OFFERING) as OFFERTA_COMMERCIALE,\n" +
				"     IF(M1.TOTAL_VALUE IS NULL, ' ', ROUND(M1.TOTAL_VALUE, 2)) as TOTALE_VALORE,\n" +
				"     M1.CURRENCY as 'CURRENCY',\n" +
				"     IF(M1.SALES_LINES_NUM IS NULL, ' ', M1.SALES_LINES_NUM) as TOTALE_UTILIZZAZIONI,\n" +
				"     IF(S1.SALES_LINE_RECOGNIZED IS NULL OR (M1.SALES_LINES_NUM IS NULL OR M1.SALES_LINES_NUM = 0), ' ',\n" +
				"     round((S1.SALES_LINE_RECOGNIZED * 100) / M1.SALES_LINES_NUM, 2)) as  IDENTIFICATO\n" +
				" from\n" +
				"  DSR_METADATA M1 left join\n" +
				"  steps_monitoring on steps_monitoring.IDDSR=M1.IDDSR,\n" +
				"  DSR_STATISTICS S1,\n" +
				"  COMMERCIAL_OFFERS,\n" +
				"  ANAG_UTILIZATION_TYPE,\n" +
				"  ANAG_DSP\n" +
				" where S1.IDDSR = M1.IDDSR\n" +
				"  and M1.SERVICE_CODE = COMMERCIAL_OFFERS.ID_COMMERCIAL_OFFERS\n" +
				"  and COMMERCIAL_OFFERS.ID_UTIILIZATION_TYPE = ANAG_UTILIZATION_TYPE.ID_UTILIZATION_TYPE\n" +
				"  and COMMERCIAL_OFFERS.IDDSP = ANAG_DSP.IDDSP\n" +
				" order by ANAG_DSP.NAME, steps_monitoring.TIMESTAMP\n" +
				" desc) y\n" +
				"  left join CCID_S3_PATH\n" +
				"  on y.DSR = CCID_S3_PATH.IDDSR\n" +
				"  left join CCID_METADATA\n" +
				"  on y.DSR = CCID_METADATA.ID_DSR\n" +
				"  where y.dsr='"+idDsr+"' ")
				.executeUpdate();

		entityManager.getTransaction().commit();

		return Response.ok().build();
	}

	@GET
	@Path("checkCommercialOfferAlreadySet")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response checkCommercialOfferAlreadySet(@QueryParam("idCommercialOffer") int idCommercialOffer) {
		EntityManager entityManager = null;
		List<Object> result;
		try {

			entityManager = provider.get();
			Query query = entityManager.createQuery(
					" select x.idCommercialOffer "
							+ " from DsrMetadataConfig x "
							+ " where x.idCommercialOffer= :commercialOffer");

			query.setParameter("commercialOffer", idCommercialOffer);
			result = query.getResultList();

			if(result != null && !result.isEmpty()){
				return Response.ok(gson.toJson(true)).build();
			}
			return Response.ok(gson.toJson(false)).build();
		} catch (Exception e) {
			logger.error("checkCommercialOfferAlreadySet", e);
		}
		return Response.status(500).build();
	}

	public DsrMetadataDTO extractMetadataQuery(String dsp, String dsrName, EntityManager entityManager){
		DsrMetadataDTO metadata = new DsrMetadataDTO();



		List<Object[]> result;
		try {
//			entityManager = provider.get();
			String sqlString = "select x.idDsrMetadataConfig , x.idCommercialOffer , d.idDsp , d.name , u.idUtilizationType , "
					+ " u.name , c.offering ,  x.regex , x.regexConfig , "
					+ " x.regexRepresentation , x.countryCode , x.periodCode , x.startIndexCountry , "
					+ " x.endIndexCountry , x.startIndexPeriod , x.endIndexPeriod , d.code from DsrMetadataConfig x, CommercialOffers c ,  AnagDsp d, AnagUtilizationType u "
					+ " where d.idDsp = c.idDSP and  c.idCommercialOffering = x.idCommercialOffer and c.idUtilizationType = u.idUtilizationType and d.ftpSourcePath = :ftpSourcePath";

			final Query q = entityManager.createQuery(sqlString);
			q.setParameter("ftpSourcePath", dsp);

			result = (List<Object[]>) q.getResultList();

			if (result != null) {
				for (Object[] o : result) {
					DsrMetadataConfigDTO tmp = new DsrMetadataConfigDTO(o);
					if (dsrName.matches(tmp.getRegEx())) {
						String periodString = dsrName.substring(tmp.getStartIndexPeriod(), tmp.getEndIndexPeriod() + 1);
						DsrPeriod dsrPeriod = periodCodeToExtractorsMap.get(tmp.getPeriodTypeExtractorCode())
								.extractDsrPeriod(periodString);

						if(dsrPeriod == null){
							throw new Exception("Errore estrazione periodo");
						}

						String countryString = dsrName.substring(tmp.getStartIndexCountry(),
								tmp.getEndIndexCountry() + 1);
						String dsrCountry = countryCodeToExtractorsMap.get(tmp.getCountryExtractorCode())
								.extractDsrCountryCode(countryString);

						// ANAG_COUNTRY
						if(dsrCountry == null){
							throw new Exception("Errore estrazione territorio");
						}

						Object result2;
						String sqlString2 ="select x.code from AnagCountry x where x.idCountry = :country";
						final Query q2 = entityManager.createQuery(sqlString2);
						q2.setParameter("country", dsrCountry);
						result2 =  q2.getSingleResult();
						String territory = (String) result2;

						if(territory == null){
							throw new Exception("Errore estrazione territorio");
						}
//						metadata.setStatus("OK");
//						metadata.setDsp(tmp.getDsp());
//						metadata.setCommercialOffer(tmp.getCommercialOffer());
//						metadata.setIdCommercialOffer(tmp.getIdCommercialOffer());
//						metadata.setCountry(dsrCountry);
//						metadata.setTerritory(territory);
//						metadata.setUtilizationType(tmp.getUtilizationType());
//						metadata.setIdUtilizationType(tmp.getIdUtilizationType());
//						metadata.setPeriodType(dsrPeriod.getPeriodType());
//						metadata.setPeriod(Integer.parseInt(dsrPeriod.getPeriod()));
//						metadata.setYear(Integer.parseInt(dsrPeriod.getYear()));
//						metadata.setPeriodStartDate(dsrPeriod.getStartDateRepresentationYYYYMMDD());
//						metadata.setPeriodEndDate(dsrPeriod.getEndDateRepresentationYYYYMMDD());
//						metadata.setDspCode(tmp.getDspCode());
//						return Response.ok(gson.toJson(metadata)).build();

						// invoice status
						final boolean invoiced = MetadataService.isInvoiced(entityManager, dsrName);
//                        logger.debug("extractMetadata: invoiced {}", invoiced);

						// fill-in result DTO
						metadata.setStatus("OK");
						// FIX  INC0016168 - CHG0034229 - MANCANZA STATISTICHE RIPARTIZIONE
//						metadata.setDsp(tmp.getDsp());
						metadata.setDsp(tmp.getIdDsp());
						metadata.setCommercialOffer(tmp.getCommercialOffer());
						metadata.setIdCommercialOffer(tmp.getIdCommercialOffer());
						metadata.setCountry(dsrCountry);
						metadata.setTerritory(territory);
						metadata.setUtilizationType(tmp.getUtilizationType());
						metadata.setIdUtilizationType(tmp.getIdUtilizationType());
						metadata.setPeriodType(dsrPeriod.getPeriodType());
						metadata.setPeriod(Integer.parseInt(dsrPeriod.getPeriod()));
						metadata.setYear(Integer.parseInt(dsrPeriod.getYear()));
						metadata.setPeriodStartDate(dsrPeriod.getStartDateRepresentationYYYYMMDD());
						metadata.setPeriodEndDate(dsrPeriod.getEndDateRepresentationYYYYMMDD());
						metadata.setDspCode(tmp.getDspCode());
						metadata.setInvoiced(invoiced);
						return metadata;
					}
				}
			}

		} catch (Exception e) {
//			logger.error("extractMetadata", e);
		}

		metadata.setStatus("KO");
		metadata.setErrorMessage("Nessuna configurazione disponibile per il DSR indicato");
		return metadata;
	}

}
