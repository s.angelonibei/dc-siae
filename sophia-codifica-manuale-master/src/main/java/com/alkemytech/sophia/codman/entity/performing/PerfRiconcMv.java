package com.alkemytech.sophia.codman.entity.performing;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PERF_RICONC_MV database table.
 * 
 */
@Entity
@Table(name="PERF_RICONC_MV")
@NamedQuery(name="PerfRiconcMv.findAll", query="SELECT p FROM PerfRiconcMv p")
public class PerfRiconcMv implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_PERF_RICONC_MV")
	private Long idPerfRiconcMv;

	@Column(name="AGENZIA")
	private String agenzia;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_CONTABILE_FATTURA")
	private Date dataContabileFattura;

	@Column(name="LORDO_NDM")
	private BigDecimal lordoNdm;

	@Column(name="LORDO_SOPHIA")
	private BigDecimal lordoSophia;

	@Column(name="NETTO_NDM")
	private BigDecimal nettoNdm;

	@Column(name="NETTO_SOPHIA")
	private BigDecimal nettoSophia;

	@Column(name="NETTO_SOPHIA_ART_18")
	private BigDecimal nettoSophiaArt18;

	@Column(name="NUMERO_FATTURA")
	private String numeroFattura;

	@Column(name="PERC_AGGIO_NDM")
	private BigDecimal percAggioNdm;

	@Column(name="PERIODO")
	private Integer periodo;

	@Column(name="SEDE")
	private String sede;

	@Column(name="SEMAFORO")
	private boolean semaforo;

	@Column(name="SEPRAG")
	private String seprag;
	
	@Column(name = "FLAG_MEGACONCERT")
	private boolean flagMegaConcert;

	@Column(name="VOCE_INCASSO")
	private String voceIncasso;

	public PerfRiconcMv() {
	}

	public Long getIdPerfRiconcMv() {
		return this.idPerfRiconcMv;
	}

	public void setIdPerfRiconcMv(Long idPerfRiconcMv) {
		this.idPerfRiconcMv = idPerfRiconcMv;
	}

	public String getAgenzia() {
		return this.agenzia;
	}

	public void setAgenzia(String agenzia) {
		this.agenzia = agenzia;
	}

	public Date getDataContabileFattura() {
		return this.dataContabileFattura;
	}

	public void setDataContabileFattura(Date dataContabileFattura) {
		this.dataContabileFattura = dataContabileFattura;
	}

	public BigDecimal getLordoNdm() {
		return this.lordoNdm;
	}

	public void setLordoNdm(BigDecimal lordoNdm) {
		this.lordoNdm = lordoNdm;
	}

	public BigDecimal getLordoSophia() {
		return this.lordoSophia;
	}

	public void setLordoSophia(BigDecimal lordoSophia) {
		this.lordoSophia = lordoSophia;
	}

	public BigDecimal getNettoNdm() {
		return this.nettoNdm;
	}

	public void setNettoNdm(BigDecimal nettoNdm) {
		this.nettoNdm = nettoNdm;
	}

	public BigDecimal getNettoSophia() {
		return this.nettoSophia;
	}

	public void setNettoSophia(BigDecimal nettoSophia) {
		this.nettoSophia = nettoSophia;
	}

	public BigDecimal getNettoSophiaArt18() {
		return this.nettoSophiaArt18;
	}

	public void setNettoSophiaArt18(BigDecimal nettoSophiaArt18) {
		this.nettoSophiaArt18 = nettoSophiaArt18;
	}

	public String getNumeroFattura() {
		return this.numeroFattura;
	}

	public void setNumeroFattura(String numeroFattura) {
		this.numeroFattura = numeroFattura;
	}

	public BigDecimal getPercAggioNdm() {
		return this.percAggioNdm;
	}

	public void setPercAggioNdm(BigDecimal percAggioNdm) {
		this.percAggioNdm = percAggioNdm;
	}

	public Integer getPeriodo() {
		return this.periodo;
	}

	public void setPeriodo(Integer periodo) {
		this.periodo = periodo;
	}

	public String getSede() {
		return this.sede;
	}

	public void setSede(String sede) {
		this.sede = sede;
	}

	public boolean getSemaforo() {
		return this.semaforo;
	}

	public void setSemaforo(boolean semaforo) {
		this.semaforo = semaforo;
	}

	public String getSeprag() {
		return this.seprag;
	}

	public void setSeprag(String seprag) {
		this.seprag = seprag;
	}

	public String getVoceIncasso() {
		return this.voceIncasso;
	}

	public void setVoceIncasso(String voceIncasso) {
		this.voceIncasso = voceIncasso;
	}

	public boolean isFlagMegaConcert() {
		return flagMegaConcert;
	}

	public void setFlagMegaConcert(boolean flagMegaConcert) {
		this.flagMegaConcert = flagMegaConcert;
	}
}