package com.alkemytech.sophia.codman.dto.performing;

import java.util.HashMap;
import java.util.List;

public class CarichiRipartizioneResponse {
	private String periodo;
	private String codicePeriodo;
	private List<CarichiRipartizioneDTO> carichiRipartizione;
	private HashMap<Long, List<CarichiRipartizioneDTO>> carryOver;
	private Boolean periodiRipartiti;


	public CarichiRipartizioneResponse(String periodo, String codicePeriodo,
			List<CarichiRipartizioneDTO> carichiRipartizione, HashMap<Long, List<CarichiRipartizioneDTO>> carryOver,
			Boolean periodiRipartiti) {
		super();
		this.periodo = periodo;
		this.codicePeriodo = codicePeriodo;
		this.carichiRipartizione = carichiRipartizione;
		this.carryOver = carryOver;
		this.periodiRipartiti = periodiRipartiti;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	
	public String getCodicePeriodo() {
		return codicePeriodo;
	}

	public void setCodicePeriodo(String codicePeriodo) {
		this.codicePeriodo = codicePeriodo;
	}

	public List<CarichiRipartizioneDTO> getCarichiRipartizione() {
		return carichiRipartizione;
	}

	public void setCarichiRipartizione(List<CarichiRipartizioneDTO> carichiRipartizione) {
		this.carichiRipartizione = carichiRipartizione;
	}

	public Boolean getPeriodiRipartiti() {
		return periodiRipartiti;
	}

	public void setPeriodiRipartiti(Boolean periodiRipartiti) {
		this.periodiRipartiti = periodiRipartiti;
	}

	public HashMap<Long, List<CarichiRipartizioneDTO>> getCarryOver() {
		return carryOver;
	}

	public void setCarryOver(HashMap<Long, List<CarichiRipartizioneDTO>> carryOver) {
		this.carryOver = carryOver;
	}



}
