package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;


public class BasicAnagDTO implements Serializable {

    /**
	 *
	 */
	private static final long serialVersionUID = 1657496305345141903L;

    private String code;
    private String description;

    public BasicAnagDTO(){}
    public BasicAnagDTO(Object[] obj) {

        if (null != obj && obj.length == 2) {
            try {
                code = (String) obj[0];
                description = (String) obj[1];

            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
