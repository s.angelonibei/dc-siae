package com.alkemytech.sophia.codman.rest.performing.serviceimpl;

import com.alkemytech.sophia.codman.dto.NdmVoceFatturaDateLimitsDTO;
import com.alkemytech.sophia.codman.entity.performing.dao.IEventiPagatiDAO;
import com.alkemytech.sophia.codman.rest.performing.service.EventiPagatiService;
import com.google.inject.Inject;

import java.util.List;

public class EventiPagatiServiceImpl implements EventiPagatiService {
    IEventiPagatiDAO eventiPagatiDAO;

    @Inject
    public EventiPagatiServiceImpl(IEventiPagatiDAO eventiPagatiDAO) {
        this.eventiPagatiDAO = eventiPagatiDAO;
    }

    @Override
    public List<String> getAgenzie(String agenzie) {
        return eventiPagatiDAO.getAgenzie(agenzie);
    }

    @Override
    public List<String> getVociIncasso(String search) {
        return eventiPagatiDAO.getVociIncasso(search);
    }

    @Override
    public NdmVoceFatturaDateLimitsDTO getDateLimits() {
        return eventiPagatiDAO.getDateLimits();
    }

    @Override
    public List<String> getSedi(String search) {
        return eventiPagatiDAO.getSedi(search);
    }
}
