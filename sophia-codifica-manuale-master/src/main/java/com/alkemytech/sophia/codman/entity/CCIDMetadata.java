package com.alkemytech.sophia.codman.entity;


import com.alkemytech.sophia.codman.invoice.repository.InvoiceItemToCcid;
import com.alkemytech.sophia.codman.persistence.AbstractEntity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

@XmlRootElement
@Entity(name="CCIDMetadata")
@Table(name="CCID_METADATA")
@NamedQueries({@NamedQuery(name="CCIDMetadata.GetAll", query="SELECT x FROM CCIDMetadata x")})
@SuppressWarnings("serial")
public class CCIDMetadata extends AbstractEntity<String> {

    @Id
    @Column(name = "ID_CCID_METADATA", nullable = false)
    private BigInteger idCCIDMetadata;

    @Column(name = "ID_CCID", nullable = false)
    private String idCcid;

    @Column(name = "ID_DSR", nullable = false)
    private String idDsr;

    @Column(name = "TOTAL_VALUE", nullable = false)
    private BigDecimal totalValue;

    @Column(name = "CURRENCY")
    private String currency;

    @Column(name = "CCID_VERSION")
    private String ccidVersion;

    @Column(name = "CCID_ENCODED")
    private Boolean ccidEncoded;

    @Column(name = "CCID_ENCODED_PROVISIONAL")
    private Boolean ccidEncodedProvisional;

    @Column(name = "CCID_NOT_ENCODED")
    private Boolean ccidNotEncoded;
    
    @Column(name = "INVOICE_ITEM")
    private Integer invoiceItem;

    @Column(name="INVOICE_STATUS")
    private String invoiceStatus;
        
    @Column(name="VALORE_RESIDUO")
    private BigDecimal valoreResiduo;
    
    @Column(name="VALORE_FATTURABILE")
    private BigDecimal valoreFatturabile;
    
    @Column(name="QUOTA_FATTURA")
    private BigDecimal quotaFattura; 

    @Column(name="SUM_USE_QUANTITY")
    private BigInteger sumUseQuantity;

    @Column(name="SUM_USE_QUANTITY_MATCHED")
    private BigInteger sumUseQuantityMatched;

    @Column(name="SUM_USE_QUANTITY_UNMATCHED")
    private BigInteger sumUseQuantityUnmatched;

    @Column(name="SUM_AMOUNT_LICENSOR")
    private BigDecimal sumAmountLicensor;

    @Column(name="SUM_AMOUNT_PAI")
    private BigDecimal sumAmountPai;
    
    @Column(name="SUM_AMOUNT_UNMATCHED")
    private BigDecimal sumAmountUnmatched;
    
    @Column(name="SUM_USE_QUANTITY_SIAE")
    private BigDecimal sumUseQuantitySiae;
    
    @Column(name="IDENTIFICATO_VALORE_PRICING")
    private BigDecimal identificatoValorePricing;
    
    @OneToMany
	@JoinColumn(name="ID_CCID_METADATA")
	private List<InvoiceItemToCcid> invoiceItemToCcid;
    
    @Override
    public String getId() {
        return idCCIDMetadata.toString();
    }

    public BigInteger getIdCCIDMetadata() {
        return idCCIDMetadata;
    }

    public void setIdCCIDMetadata(BigInteger idCCIDMetadata) {
        this.idCCIDMetadata = idCCIDMetadata;
    }

    public String getIdCcid() {
        return idCcid;
    }

    public void setIdCcid(String idCcid) {
        this.idCcid = idCcid;
    }

    public String getIdDsr() {
        return idDsr;
    }

    public void setIdDsr(String idDsr) {
        this.idDsr = idDsr;
    }

    public BigDecimal getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(BigDecimal totalValue) {
        this.totalValue = totalValue;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCcidVersion() {
        return ccidVersion;
    }

    public void setCcidVersion(String ccidVersion) {
        this.ccidVersion = ccidVersion;
    }

    public Boolean getCcidEncoded() {
        return ccidEncoded;
    }

    public void setCcidEncoded(Boolean ccidEncoded) {
        this.ccidEncoded = ccidEncoded;
    }

    public Boolean getCcidEncodedProvisional() {
        return ccidEncodedProvisional;
    }

    public void setCcidEncodedProvisional(Boolean ccidEncodedProvisional) {
        this.ccidEncodedProvisional = ccidEncodedProvisional;
    }

    public Boolean getCcidNotEncoded() {
        return ccidNotEncoded;
    }

    public void setCcidNotEncoded(Boolean ccidNotEncoded) {
        this.ccidNotEncoded = ccidNotEncoded;
    }

	public Integer getInvoiceItem() {
		return invoiceItem;
	}

	public void setInvoiceItem(Integer invoiceItem) {
		this.invoiceItem = invoiceItem;
	}

	public String getInvoiceStatus() {
		return invoiceStatus;
	}

	public void setInvoiceStatus(String invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}
    

	public BigDecimal getValoreResiduo() {
		return valoreResiduo;
	}

	public void setValoreResiduo(BigDecimal valoreResiduo) {
		this.valoreResiduo = valoreResiduo;
	}

	public BigDecimal getValoreFatturabile() {
		return valoreFatturabile;
	}

	public void setValoreFatturabile(BigDecimal valoreFatturabile) {
		this.valoreFatturabile = valoreFatturabile;
	}

	public BigDecimal getQuotaFattura() {
		return quotaFattura;
	}

	public void setQuotaFattura(BigDecimal quotaFattura) {
		this.quotaFattura = quotaFattura;
	}
	public BigInteger getSumUseQuantity() {
		return sumUseQuantity;
	}

	public void setSumUseQuantity(BigInteger sumUseQuantity) {
		this.sumUseQuantity = sumUseQuantity;
	}

	public BigInteger getSumUseQuantityMatched() {
		return sumUseQuantityMatched;
	}

	public void setSumUseQuantityMatched(BigInteger sumUseQuantityMatched) {
		this.sumUseQuantityMatched = sumUseQuantityMatched;
	}

	public BigInteger getSumUseQuantityUnmatched() {
		return sumUseQuantityUnmatched;
	}

	public void setSumUseQuantityUnmatched(BigInteger sumUseQuantityUnmatched) {
		this.sumUseQuantityUnmatched = sumUseQuantityUnmatched;
	}

	public BigDecimal getSumAmountLicensor() {
		return sumAmountLicensor;
	}

	public void setSumAmountLicensor(BigDecimal sumAmountLicensor) {
		this.sumAmountLicensor = sumAmountLicensor;
	}

	public BigDecimal getSumAmountPai() {
		return sumAmountPai;
	}

	public void setSumAmountPai(BigDecimal sumAmountPai) {
		this.sumAmountPai = sumAmountPai;
	}

	public BigDecimal getSumAmountUnmatched() {
		return sumAmountUnmatched;
	}

	public void setSumAmountUnmatched(BigDecimal sumAmountUnmatched) {
		this.sumAmountUnmatched = sumAmountUnmatched;
	}

	public BigDecimal getSumUseQuantitySiae() {
		return sumUseQuantitySiae;
	}

	public void setSumUseQuantitySiae(
			BigDecimal sumUseQuantitySiae) {
		this.sumUseQuantitySiae = sumUseQuantitySiae;
	}

	public BigDecimal getIdentificatoValorePricing() {
		return identificatoValorePricing;
	}

	public void setIdentificatoValorePricing(BigDecimal identificatoValorePricing) {
		this.identificatoValorePricing = identificatoValorePricing;
	}

	public List<InvoiceItemToCcid> getInvoiceItemToCcid() {
		return invoiceItemToCcid;
	}

	public void setInvoiceItemToCcid(List<InvoiceItemToCcid> invoiceItemToCcid) {
		this.invoiceItemToCcid = invoiceItemToCcid;
	}



	
}
