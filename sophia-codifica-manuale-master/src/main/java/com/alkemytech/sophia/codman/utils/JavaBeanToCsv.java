package com.alkemytech.sophia.codman.utils;

import com.opencsv.bean.MappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.StreamingOutput;
import java.io.*;
import java.sql.Timestamp;
import java.util.List;

public class JavaBeanToCsv {
    private static final Logger logger = LoggerFactory.getLogger(JavaBeanToCsv.class);

    public static File writeCsv(List beans, String path) {
        try {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            String fileName = "export_" + timestamp + "_.csv";
            File file = new File(path + File.pathSeparator + fileName);
            Writer writer = new FileWriter(file);
            StatefulBeanToCsv beanToCsv = new StatefulBeanToCsvBuilder<>(writer).build();
            beanToCsv.write(beans);
            writer.close();
            return file;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return null;

    }

    public static <T> StreamingOutput getStreamingOutput(final List<T> objectList, final MappingStrategy mappingStrategy, final Class clazz) {
        return new StreamingOutput() {
            @Override
            public void write(OutputStream outputStream) throws IOException, WebApplicationException {
                OutputStreamWriter osw = new OutputStreamWriter(outputStream);

                StatefulBeanToCsvBuilder statefulBeanToCsvBuilder = new StatefulBeanToCsvBuilder(osw)
                        .withSeparator(';');

                if (mappingStrategy != null) {
                    statefulBeanToCsvBuilder.withMappingStrategy(mappingStrategy);
                } else {
                    CustomMappingStrategy<T> strategy = new CustomMappingStrategy<>();
                    strategy.setType(clazz);
                    statefulBeanToCsvBuilder.withMappingStrategy(strategy);
                }

                StatefulBeanToCsv beanToCsv = statefulBeanToCsvBuilder.build();

                try {
                    beanToCsv.write(objectList);
                    osw.flush();
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                } finally {
                    if (osw != null)
                        osw.close();
                }
            }
        };
    }

    public static <T> StreamingOutput getStreamingOutput(final T obj, final MappingStrategy mappingStrategy) {
        return new StreamingOutput() {
            @Override
            public void write(OutputStream outputStream) throws IOException {
                OutputStreamWriter osw = new OutputStreamWriter(outputStream);

                StatefulBeanToCsvBuilder statefulBeanToCsvBuilder = new StatefulBeanToCsvBuilder(osw)
                        .withSeparator(';');

                if (mappingStrategy != null) {
                    statefulBeanToCsvBuilder.withMappingStrategy(mappingStrategy);
                }

                StatefulBeanToCsv beanToCsv = statefulBeanToCsvBuilder.build();

                try {
                    beanToCsv.write(obj);
                    osw.flush();
                    outputStream.flush();
                } catch (CsvRequiredFieldEmptyException e) {
                    logger.error(e.getMessage(), e);
                } catch (CsvDataTypeMismatchException e) {
                    logger.error(e.getMessage(), e);
                } finally {
                    if (osw != null)
                        osw.close();
                }
            }
        };
    }
}