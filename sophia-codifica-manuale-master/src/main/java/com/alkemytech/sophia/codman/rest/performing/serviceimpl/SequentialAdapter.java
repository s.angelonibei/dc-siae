package com.alkemytech.sophia.codman.rest.performing.serviceimpl;

import java.util.LinkedList;

public class SequentialAdapter<T> {
	
	private LinkedList<Modifier<T>> modifications = new LinkedList<Modifier<T>>();

	public void process(T type) {
		for (Modifier<T> modifier : getModifications()) {
			modifier.apply(type);
		}
	}
	
	public SequentialAdapter<T> addModifier(Modifier<T> modifier) {
		getModifications().addLast(modifier);
		return this;
	}

	public LinkedList<Modifier<T>> getModifications() {
		return modifications;
	}

	public void setModifications(LinkedList<Modifier<T>> modifications) {
		this.modifications = modifications;
	}
	

}
