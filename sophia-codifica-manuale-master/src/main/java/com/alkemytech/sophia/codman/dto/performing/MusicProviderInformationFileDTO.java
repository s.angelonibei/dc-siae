package com.alkemytech.sophia.codman.dto.performing;

import java.util.Date;

public class MusicProviderInformationFileDTO
{
    private String nomeFile;
    private Integer idMusicProvider;
    private String emittente;
    private Integer idPalinsesto;
    private String canale;
    private Date dataUpload;
    private String tipoUpload;

    public MusicProviderInformationFileDTO() {
    }

    public MusicProviderInformationFileDTO(String nomeFile, Integer idEmittente, String emittente,
                                           Integer idPalinsesto, String canale,
                                           Date dataUpload, String tipoUpload) {
        this.nomeFile = nomeFile;
        this.idMusicProvider = idEmittente;
        this.emittente = emittente;
        this.idPalinsesto = idPalinsesto;
        this.canale = canale;
        this.dataUpload = dataUpload;
        this.tipoUpload = tipoUpload;
    }

    public String getNomeFile() {
        return nomeFile;
    }

    public void setNomeFile(String nomeFile) {
        this.nomeFile = nomeFile;
    }

    public Integer getIdMusicProvider() {
        return idMusicProvider;
    }

    public void setIdMusicProvider(Integer idMusicProvider) {
        this.idMusicProvider = idMusicProvider;
    }

    public String getEmittente() {
        return emittente;
    }

    public void setEmittente(String emittente) {
        this.emittente = emittente;
    }

    public Integer getIdPalinsesto() {
        return idPalinsesto;
    }

    public void setIdPalinsesto(Integer idPalinsesto) {
        this.idPalinsesto = idPalinsesto;
    }

    public String getCanale() {
        return canale;
    }

    public void setCanale(String canale) {
        this.canale = canale;
    }

    public Date getDataUpload() {
        return dataUpload;
    }

    public void setDataUpload(Date dataUpload) {
        this.dataUpload = dataUpload;
    }

    public String getTipoUpload() {
        return tipoUpload;
    }

    public void setTipoUpload(String tipoUpload) {
        this.tipoUpload = tipoUpload;
    }
}
