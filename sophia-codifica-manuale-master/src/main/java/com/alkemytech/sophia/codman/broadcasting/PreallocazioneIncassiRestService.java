package com.alkemytech.sophia.codman.broadcasting;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.http.util.TextUtils;

import com.alkemytech.sophia.broadcasting.dto.BdcContiDTO;
import com.alkemytech.sophia.broadcasting.dto.BdcUpdatePianoContiRequestDTO;
import com.alkemytech.sophia.broadcasting.dto.ContiRequestDTO;
import com.alkemytech.sophia.broadcasting.model.BdcDettaglioPianoDeiConti;
import com.alkemytech.sophia.broadcasting.model.BdcPianoDeiConti;
import com.alkemytech.sophia.broadcasting.service.interfaces.BdcPreallocazioneIncassoService;
import com.alkemytech.sophia.codman.dto.BdcPianoContiResultDTO;
import com.alkemytech.sophia.codman.rest.PagedResult;
import com.amazonaws.util.CollectionUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
@Path("preallocazioneIncassi")
public class PreallocazioneIncassiRestService {
	private Gson gson;
	private BdcPreallocazioneIncassoService bdcPreallocazioneIncassoService;
	private final static String NO_RESULTS = "{\"message\":\"No results found\"}";
	private final static String NOT_CHANGE = "{\"message\":\"No change has found\"}";

	@Inject
	public PreallocazioneIncassiRestService(BdcPreallocazioneIncassoService bdcPreallocazioneIncassoService,
			Gson gson) {
		this.gson = gson;
		this.bdcPreallocazioneIncassoService = bdcPreallocazioneIncassoService;
	}

	@POST
	@Path("getConti")
	@Produces("application/json")
	public Response getRipartizioni(ContiRequestDTO contiRequestDTO) {
		try {
			Date dataInizio = null;
			Date dataFine = null;
			if (!TextUtils.isEmpty(contiRequestDTO.getDateFrom())) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
				dataInizio = sdf.parse(contiRequestDTO.getDateFrom());
			}
			if (!TextUtils.isEmpty(contiRequestDTO.getDateTo())) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
				dataFine = sdf.parse(contiRequestDTO.getDateTo());
			}
			if (!TextUtils.isEmpty(contiRequestDTO.getDateFrom()) && !TextUtils.isEmpty(contiRequestDTO.getDateTo())) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
				dataInizio = sdf.parse(contiRequestDTO.getDateFrom());
				dataFine = sdf.parse(contiRequestDTO.getDateTo());
				if (dataInizio.compareTo(dataFine) == 1) {
					return Response.status(Status.NOT_ACCEPTABLE).build();
				}
			}

			List<BdcContiDTO> result = bdcPreallocazioneIncassoService.findAll(dataInizio, dataFine,
					contiRequestDTO.getTipoEmittente(), contiRequestDTO.getIdEmittente(), contiRequestDTO.getIdCanale(),
					contiRequestDTO.getTipoDiritto(), contiRequestDTO.getFirst(), contiRequestDTO.getLast(),
					contiRequestDTO.getMaxrow());
			if (!CollectionUtils.isNullOrEmpty(result)) {
				final PagedResult pagedResult = new PagedResult();
				if (null != result && !result.isEmpty()) {
					final int maxrows = contiRequestDTO.getLast() - contiRequestDTO.getFirst();
					final boolean hasNext = result.size() > maxrows;
					pagedResult.setRows(!hasNext ? result : result.subList(0, maxrows)).setMaxrows(maxrows)
							.setFirst(contiRequestDTO.getFirst())
							.setLast(hasNext ? contiRequestDTO.getLast() : contiRequestDTO.getFirst() + result.size())
							.setHasNext(hasNext).setHasPrev(contiRequestDTO.getFirst() > 0);
				} else {
					pagedResult.setMaxrows(0).setFirst(0).setLast(0).setHasNext(false).setHasPrev(false);
				}
				return Response.ok(gson.toJson(pagedResult)).build();
			} else {
				return Response.ok(gson.toJson(NO_RESULTS)).build();
			}
		} catch (Exception e) {
			return Response.status(500).build();
		}
	}

	@POST
	@Path("getDettaglio")
	@Produces("application/json")
	public Response getDettails(@QueryParam(value = "id") Integer idBdcPianoDeiConti) {
		try {
			BdcPianoDeiConti result = bdcPreallocazioneIncassoService
					.getPianoContoDettails(idBdcPianoDeiConti);
			BdcPianoContiResultDTO bdcPianoContiResultDTO= new BdcPianoContiResultDTO();
			bdcPianoContiResultDTO.setDettaglioPianoDeiConti(result.getDettaglioPianoDeiConti());
			bdcPianoContiResultDTO.setAttivo(result.getAttivo());
			bdcPianoContiResultDTO.setCodiceConto(result.getCodiceConto());
			bdcPianoContiResultDTO.setDataCreazione(result.getDataCreazione());
			bdcPianoContiResultDTO.setDescrizione(result.getDescrizione());
			bdcPianoContiResultDTO.setIdBdcPianoDeiConti(result.getIdBdcPianoDeiConti());
			bdcPianoContiResultDTO.setUtente(result.getUtente());
			if (result!=null) {
				Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
				return Response.ok(gson.toJson(bdcPianoContiResultDTO)).build();
			} else {
				return Response.ok(gson.toJson(NO_RESULTS)).build();
			}
			
		} catch (Exception e) {
			return Response.status(500).build();
		}
	}
	
	@POST
	@Path("getStoricoConto")
	@Produces("application/json")
	public Response getStoricoConti(@QueryParam(value = "numeroConto") String numeroConto) {
		try {
			List<BdcContiDTO> result = bdcPreallocazioneIncassoService.getStoricoConto(numeroConto);
			if (!CollectionUtils.isNullOrEmpty(result)) {
				return Response.ok(gson.toJson(result)).build();
			} else {
				return Response.ok(gson.toJson(NO_RESULTS)).build();
			}
		} catch (Exception e) {
			return Response.status(500).build();
		}
	}

	
	@POST
	@Path("updatePreallocazioneIncassi")
	@Produces("application/json")
	public Response updatePreallocazioneIncassi(BdcUpdatePianoContiRequestDTO bdcUpdatePianoContiRequestDTO) {
		try {
			BdcPianoDeiConti result = bdcPreallocazioneIncassoService
					.updatePianoConti(bdcUpdatePianoContiRequestDTO);
			BdcPianoContiResultDTO bdcPianoContiResultDTO= new BdcPianoContiResultDTO();
			bdcPianoContiResultDTO.setDettaglioPianoDeiConti(result.getDettaglioPianoDeiConti());
			bdcPianoContiResultDTO.setAttivo(result.getAttivo());
			bdcPianoContiResultDTO.setCodiceConto(result.getCodiceConto());
			bdcPianoContiResultDTO.setDataCreazione(result.getDataCreazione());
			bdcPianoContiResultDTO.setDescrizione(result.getDescrizione());
			bdcPianoContiResultDTO.setIdBdcPianoDeiConti(result.getIdBdcPianoDeiConti());
			bdcPianoContiResultDTO.setUtente(result.getUtente());
			if (result!=null) {
				Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
				return Response.ok(gson.toJson(bdcPianoContiResultDTO)).build();
			} else {
				return Response.status(500).build();
			}
		} catch (IllegalArgumentException e) {
			return Response.ok(gson.toJson(NOT_CHANGE)).build();
		} catch (Exception e) {
			return Response.status(500).build();
		}
	}


}
