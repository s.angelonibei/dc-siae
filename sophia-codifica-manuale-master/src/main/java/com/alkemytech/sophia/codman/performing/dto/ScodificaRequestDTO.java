package com.alkemytech.sophia.codman.performing.dto;

import com.alkemytech.sophia.codman.dto.PaginationDTO;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.StringUtils;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ScodificaRequestDTO extends ScodificaDTO implements Serializable {
    private String nominativo;
    private Date ripartizioniRiferimentoDa;
    private Date ripartizioniRiferimentoA;
    @SerializedName("pagination")
    private PaginationDTO paginationDTO;
    private String user;
    private Long numeroProgrammaMusicale;

    public ScodificaRequestDTO() {
    }

    public ScodificaRequestDTO(Long idCombana, String titolo, String nominativo, String codiceOperaApprovato, String ripartizioniRiferimentoDa, String ripartizioniRiferimentoA, PaginationDTO paginationDTO, String user, Long numeroProgrammaMusicale) throws ParseException {
        super(idCombana, titolo, codiceOperaApprovato);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        this.nominativo = nominativo;
        this.ripartizioniRiferimentoDa = StringUtils.isNotEmpty(ripartizioniRiferimentoDa) ? sdf.parse(ripartizioniRiferimentoDa) : null;
        this.ripartizioniRiferimentoA = StringUtils.isNotEmpty(ripartizioniRiferimentoA) ? sdf.parse(ripartizioniRiferimentoA) : null;
        this.paginationDTO = paginationDTO;
        this.user = user;
        this.numeroProgrammaMusicale = numeroProgrammaMusicale;
    }

    public ScodificaRequestDTO(Long idCombana, String codiceCombana, String titolo, String nominativo, String codiceOperaApprovato, String ripartizioniRiferimentoDa, String ripartizioniRiferimentoA, PaginationDTO paginationDTO, String user, Long numeroProgrammaMusicale) throws ParseException {
        super(idCombana, codiceCombana, titolo, codiceOperaApprovato);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        this.nominativo = nominativo;
        this.ripartizioniRiferimentoDa = StringUtils.isNotEmpty(ripartizioniRiferimentoDa) ? sdf.parse(ripartizioniRiferimentoDa) : null;
        this.ripartizioniRiferimentoA = StringUtils.isNotEmpty(ripartizioniRiferimentoA) ? sdf.parse(ripartizioniRiferimentoA) : null;
        this.paginationDTO = paginationDTO;
        this.user = user;
        this.numeroProgrammaMusicale = numeroProgrammaMusicale;
    }

    public ScodificaRequestDTO(Long idCombana, String codiceCombana, String titolo, String compositori, String autori, String interpreti, String codiceOperaApprovato, String nominativo, String ripartizioniRiferimentoDa, String ripartizioniRiferimentoA, PaginationDTO paginationDTO, String user, Long numeroProgrammaMusicale) throws ParseException {
        super(idCombana, codiceCombana, titolo, compositori, autori, interpreti, codiceOperaApprovato);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        this.nominativo = nominativo;
        this.ripartizioniRiferimentoDa = StringUtils.isNotEmpty(ripartizioniRiferimentoDa) ? sdf.parse(ripartizioniRiferimentoDa) : null;
        this.ripartizioniRiferimentoA = StringUtils.isNotEmpty(ripartizioniRiferimentoA) ? sdf.parse(ripartizioniRiferimentoA) : null;
        this.paginationDTO = paginationDTO;
        this.user = user;
        this.numeroProgrammaMusicale = numeroProgrammaMusicale;
    }

    public String getNominativo() {
        return nominativo;
    }

    public void setNominativo(String nominativo) {
        this.nominativo = nominativo;
    }

    public Date getRipartizioniRiferimentoDa() {
        return ripartizioniRiferimentoDa;
    }

    public void setRipartizioniRiferimentoDa(Date ripartizioniRiferimentoDa) {
        this.ripartizioniRiferimentoDa = ripartizioniRiferimentoDa;
    }

    public Date getRipartizioniRiferimentoA() {
        return ripartizioniRiferimentoA;
    }

    public void setRipartizioniRiferimentoA(Date ripartizioniRiferimentoA) {
        this.ripartizioniRiferimentoA = ripartizioniRiferimentoA;
    }

    public PaginationDTO getPaginationDTO() {
        return paginationDTO;
    }

    public void setPaginationDTO(PaginationDTO paginationDTO) {
        this.paginationDTO = paginationDTO;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Long getNumeroProgrammaMusicale() {
        return numeroProgrammaMusicale;
    }

    public void setNumeroProgrammaMusicale(Long numeroProgrammaMusicale) {
        this.numeroProgrammaMusicale = numeroProgrammaMusicale;
    }
}
