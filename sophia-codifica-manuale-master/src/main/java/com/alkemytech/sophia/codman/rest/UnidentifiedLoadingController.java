package com.alkemytech.sophia.codman.rest;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.alkemytech.sophia.codman.dto.*;
import com.alkemytech.sophia.codman.entity.DsrProcessConfig;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.alkemytech.sophia.codman.guice.UnidentifiedDataSource;
import com.alkemytech.sophia.codman.service.dsrmetadata.period.DsrPeriod;
import com.alkemytech.sophia.codman.utils.Periods;
import com.alkemytech.sophia.codman.utils.QueryUtils;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.google.gson.JsonObject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.config.ResultType;
import org.eclipse.persistence.sessions.Record;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
@Path("unidentified-loading")
public class UnidentifiedLoadingController {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	static final UnidentifiedLoadingStateDTO[] loadingStatuses = {
			new UnidentifiedLoadingStateDTO("Disponibile","Disponibile")
			,new UnidentifiedLoadingStateDTO("Caricato","Caricato")
			,new UnidentifiedLoadingStateDTO("Failed","Fallito")
	};

	private Gson gson;
	private SQS sqs;
	private Properties configuration;
	static List<UnidentifiedLoadingSearchResultDTO> data;

	private final Provider<EntityManager> uni;
	private final Provider<EntityManager> mcmdb;
	
	

	@Inject
	protected UnidentifiedLoadingController(@McmdbDataSource Provider<EntityManager> mcmdb,
											@UnidentifiedDataSource Provider<EntityManager> uni,
											SQS sqs,
											Gson gson,
											@Named("configuration") Properties configuration) {
		super();
		this.sqs=sqs;
		this.uni=uni;
		this.mcmdb=mcmdb;
		this.gson = gson;
		this.configuration=configuration;
	}
	
	@GET
	@Path("/statuses")
	@Produces(MediaType.APPLICATION_JSON)
	public Response statuses() throws IOException {
		try {
			Map<String,Object> response = new HashMap<String, Object>();
			List<UnidentifiedLoadingStateDTO> statuses = new ArrayList<UnidentifiedLoadingStateDTO>();
			for(UnidentifiedLoadingStateDTO item: loadingStatuses) {
				statuses.add(item);
			}
			response.put("statuses", statuses);
			return Response.ok(gson.toJson(response)).build();
		} catch (Exception e) {
			logger.error("Search Request Exception", e);
			return Response.status(500).build();
		}
	}	

	@POST
	@Path("/search")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response search(UnidentifiedSearchRequestDTO request) throws IOException {
		try {
		//////////////////////////////////////////////////////////
			List<UnidentifiedLoadingSearchResultDTO> subList = new ArrayList<UnidentifiedLoadingSearchResultDTO>();
//			List<DsrProcessingDTO> subList = data.subList(request.getFirst(), Math.min(request.getLast(),data.size()));
//				List<DsrProcessingDTO> subList = new ArrayList<>();
				EntityManager em = null;
				List<DsrProcessConfig> result;
				em = mcmdb.get();


				boolean checkPeriod = false;
				Map<Integer, List<Integer>> quarters = new HashMap<>();
				String inCondition = "";

				Integer defaultMonthFrom = 0;
				Integer defaultYearFrom = 2000;
				Date date = new Date();
				Calendar calendar = new GregorianCalendar();
				calendar.setTime(date);
				Integer defaultMonthTo = calendar.get(Calendar.MONTH);
				Integer defaultYearTo = calendar.get(Calendar.YEAR);

				if (request.getMonthFrom() > 0 || request.getMonthTo() > 0) {
					checkPeriod = true;

					if (request.getMonthFrom() > 0) {
						defaultMonthFrom = request.getMonthFrom();
						defaultYearFrom = request.getYearFrom();
					}

					if (request.getMonthTo() > 0) {
						defaultMonthTo = request.getMonthTo();
						defaultYearTo = request.getYearTo();

					}
					quarters = Periods.extractCorrespondingQuarters(defaultMonthFrom, defaultYearFrom, defaultMonthTo, defaultYearTo);

					int y = 0;
					for (int key : quarters.keySet()) {
						if (y++ != 0) {
							inCondition += " or ";
						}
						inCondition += " (M1.PERIOD_TYPE='quarter' and M1.PERIOD in ("
								+ StringUtils.join(quarters.get(key), ',') + ") and M1.YEAR = " + key + " )";

					}
					if (!inCondition.isEmpty()) {
						inCondition = " ( " + inCondition + " )";
					}

				}

				List<String> listaStati = new ArrayList<>();
				for (UnidentifiedLoadingStateDTO processingStatus : request.getStatuses()) {
					if(processingStatus.getId() != null)
						listaStati.add(processingStatus.getId());
				}

				
				String bcLikeCondition = "";


				String selectCountSql="select count(*) TOTAL,\n" +
						"       sum(case when STATUS= 'ERROR' then 1 else 0 end) ERROR,\n" +
						"       sum(case when STATUS= 'COMPLETED' then 1 else 0 end) COMPLETED,\n" +
						"       sum(case when STATUS= 'ARRIVED' then 1 else 0 end) ARRIVED,\n" +
						"       sum(case when STATUS= 'PROCESSING' then 1 else 0 end) PROCESSING ";

				String selectSql = ""
						+ "SELECT  " +
						" y.STATUS, "
						+ " y.UNI_STATUS, "
						+ " y.COD_STATUS, "
						+ "y.FILE_PATH, "
						+ "y.INSERT_TIME, "
						+ "y.DSP, "
						+ "y.DSP_PATH, "
						+ "y.DSR, "
						+ "y.`DATA RICEZIONE DSR`, "
						+ "y.PERIOD_TYPE, "
						+ "y.PERIOD, "
						+ "y.YEAR, "
						+ "y.PERIODO, "
						+ "y.TERRITORIO, "
						+ "y.`TIPO UTILIZZO`, "
						+ "y.`OFFERTA COMMERCIALE`, "
						+ "y.`DATA PRODUZIONE CCID`, "
						+ "y.EXTENDED_DSR_URL, "
						+ "y.DRILLDOWN, "
						+ "y.DATA_INIZIO, "
						+ "y.IDENTIFY_KB_VERSION, "
						+ "y.ERROR_MESSAGE, "
						+ "y.DATA_FINE, "
						+ "CCID_S3_PATH.ID_CCID_PATH, "
						+ "CCID_S3_PATH.IDDSR, "
						+ "CCID_S3_PATH.S3_PATH, "
						+ "CCID_METADATA.ID_CCID, "
						+ "CCID_METADATA.ID_DSR, "
						+ "CCID_METADATA.ID_CCID_METADATA, "
						+ "CCID_METADATA.CCID_VERSION, "
						+ "CCID_METADATA.CCID_ENCODED, "
						+ "CCID_METADATA.CCID_ENCODED_PROVISIONAL, "
						+ "CCID_METADATA.CCID_NOT_ENCODED, "
						+ "CCID_METADATA.INVOICE_STATUS, "
						+ "CCID_METADATA.INVOICE_ITEM, "
						+ "CCID_METADATA.VALORE_FATTURABILE, "
						+ "CCID_METADATA.SUM_USE_QUANTITY, "
						+ "CCID_METADATA.SUM_USE_QUANTITY_MATCHED, "
						+ "CCID_METADATA.SUM_USE_QUANTITY_UNMATCHED, "
						+ "CCID_METADATA.SUM_AMOUNT_LICENSOR, "
						+ "CCID_METADATA.SUM_AMOUNT_PAI, "
						+ "CCID_METADATA.SUM_AMOUNT_UNMATCHED, "
						+ "CCID_METADATA.SUM_USE_QUANTITY_SIAE, "
						+ "CCID_METADATA.VALORE_RESIDUO, "
						+ "CCID_METADATA.QUOTA_FATTURA, "
						+ "CCID_METADATA.IDENTIFICATO_VALORE_PRICING, "
						+ "CCID_METADATA.TOTAL_VALUE_CCID_CURRENCY ";



				String fromSql =  " from "
						+ "(select "
						+ " case when FILE.LAST_START_TIME > now() - interval 5 minute then 'PROCESSING' " +
						" 			when (EXTRACT_STATUS = 'KO'\n" +
						"                  or CLEAN_STATUS = 'KO'\n" +
						"                  or IDENTIFY_STATUS = 'KO'\n" +
						"                  or CLAIM_STATUS = 'KO') then 'ERROR'\n" +
						"          when (CLAIM_STATUS = 'OK') then 'COMPLETED'\n" +
						"          when (DSR_STEPS_MONITORING.IDDSR is null) then 'ARRIVED'\n" +
						"          else 'PROCESSING' END as STATUS, " +
						" case when CODLOAD_STATUS = 'OK' then 'Caricato'\n" +
						"              when CODLOAD_STATUS = 'KO' then 'Failed'\n" +
						"              when CODLOAD_STATUS is null then 'Disponibile' END as COD_STATUS, "+
						" case when UNILOAD_STATUS = 'OK' then 'Caricato'\n" +
						"              when UNILOAD_STATUS = 'KO' then 'Failed'\n" +
						"              when UNILOAD_STATUS is null then 'Disponibile' END as UNI_STATUS, "
						+ " FILE.FILE_PATH, "
						+ " FILE.INSERT_TIME, "
						+ "IF(ANAG_DSP.NAME IS NULL , '', ANAG_DSP.NAME ) AS DSP, "
						+ "FILE.IDDSR AS DSR, "
						+ "IF (DSR_STEPS_MONITORING.EXTRACT_QUEUED IS NULL,' ',DATE_FORMAT (CONVERT_TZ (DSR_STEPS_MONITORING.EXTRACT_QUEUED,'+00:00','+01:00'),'%d-%m-%Y %H:%i')) AS 'DATA RICEZIONE DSR', "
						+ "M1.PERIOD_TYPE, "
						+ "M1.PERIOD, "
						+ "M1.YEAR, "
						+ "ANAG_DSP.FTP_SOURCE_PATH DSP_PATH, "
						+ "COALESCE(DSR_STEPS_MONITORING.EXTRACT_QUEUED,DSR_STEPS_MONITORING.CLEAN_QUEUED ) DATA_INIZIO, "
						+ "DSR_STEPS_MONITORING.CLAIM_FINISHED DATA_FINE, "
						+ "DSR_STEPS_MONITORING.ERROR_MESSAGE, "
						+ "DSR_STEPS_MONITORING.IDENTIFY_KB_VERSION, "
						+ "(CASE "
						+ "WHEN M1.PERIOD_TYPE = 'month' THEN "
						+ "concat(ELT(M1.PERIOD, 'Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov','Dic') , ' ', M1.YEAR) "
						+ "ELSE concat(ELT(M1.PERIOD, 'Gen - Mar', 'Apr - Giu', 'Lug - Set', 'Ott - Dic') , ' ', M1.YEAR)       END) AS PERIODO, "
						+ "IF(M1.COUNTRY IS NULL , ' ', M1.COUNTRY ) AS 'TERRITORIO', "
						+ "IF(ANAG_UTILIZATION_TYPE.NAME IS NULL , ' ', ANAG_UTILIZATION_TYPE.NAME) AS 'TIPO UTILIZZO', "
						+ "IF(COMMERCIAL_OFFERS.OFFERING IS NULL , ' ', COMMERCIAL_OFFERS.OFFERING) as 'OFFERTA COMMERCIALE', "
						+ "M1.CURRENCY as 'CURRENCY', "
						+ "M1.BACKCLAIM, "
						+ "IF (DSR_STEPS_MONITORING.CLAIM_FINISHED IS NULL,' ',DATE_FORMAT (CONVERT_TZ (DSR_STEPS_MONITORING.CLAIM_FINISHED,'+00:00','+01:00'),'%d-%m-%Y %H:%i')) AS 'DATA PRODUZIONE CCID', "
						+ "M1.EXTENDED_DSR_URL, "
						+ "IF((SELECT COUNT(ID) FROM CCID_METADATA_DETAILS d WHERE M1.IDDSR = d.ID_DSR) > 0, true,false) AS DRILLDOWN "
//						+ "from "
						+ "from DSR_METADATA_FILE FILE " +
						"   join "
						+ " DSR_METADATA M1 on M1.IDDSR= FILE.IDDSR "
						+ " join DSR_STEPS_MONITORING on DSR_STEPS_MONITORING.IDDSR=M1.IDDSR  "
						+ " join COMMERCIAL_OFFERS on  M1.SERVICE_CODE = COMMERCIAL_OFFERS.ID_COMMERCIAL_OFFERS "
						+ " join ANAG_UTILIZATION_TYPE on  COMMERCIAL_OFFERS.ID_UTIILIZATION_TYPE = ANAG_UTILIZATION_TYPE.ID_UTILIZATION_TYPE "
						+ " join ANAG_DSP on  COMMERCIAL_OFFERS.IDDSP = ANAG_DSP.IDDSP "
//						+ "where DSR_STEPS_MONITORING.IDDSR=M1.IDDSR and S1.IDDSR = M1.IDDSR "
//						+ "and M1.SERVICE_CODE = COMMERCIAL_OFFERS.ID_COMMERCIAL_OFFERS "
//						+ "and COMMERCIAL_OFFERS.ID_UTIILIZATION_TYPE = ANAG_UTILIZATION_TYPE.ID_UTILIZATION_TYPE "
//						+ "and COMMERCIAL_OFFERS.IDDSP = ANAG_DSP.IDDSP "
						+ " where 1=1 "
						+ (!request.getDspList().isEmpty() ? "and COMMERCIAL_OFFERS.IDDSP in ( " + QueryUtils.getValuesCollection(request.getDspList()) + " )" : "")
						+ (!request.getCountryList().isEmpty() ? "and M1.COUNTRY in ( " + QueryUtils.getValuesCollection(request.getCountryList()) + " )" : "")
						+ (!request.getOfferList().isEmpty() ? "and COMMERCIAL_OFFERS.OFFERING in ( " + QueryUtils.getValuesCollection(request.getOfferList()) + " )" : "")
						+ (!inCondition.isEmpty() ? "and (" + inCondition + " or " : "")
						+ (checkPeriod && inCondition.isEmpty() ? " and " : "")
						+ (checkPeriod ? " (PERIOD_TYPE='month' and  STR_TO_DATE(CONCAT(M1.YEAR,'-', M1.PERIOD , '-01'), '%Y-%m-%d') >= '" + defaultYearFrom + "-" + defaultMonthFrom + "-01' and STR_TO_DATE(CONCAT(M1.YEAR,'-', M1.PERIOD , '-01'), '%Y-%m-%d') <= '" + defaultYearTo + "-" + defaultMonthTo + "-01' ) " : "")
						+ (checkPeriod && !quarters.isEmpty() ? ")" : "")
//						+ (backclaim != -1 ? " and (M1.IDDSR " + (backclaim == 1 ? " LIKE '%_BC0%' or M1.IDDSR LIKE '%_BC_A_%')  " : "LIKE '"+bcLikeCondition+"') ") : " ")
						+ " ) y "
						+ "left join CCID_S3_PATH "
						+ "on y.DSR = CCID_S3_PATH.IDDSR "
						+ "left join CCID_METADATA "
						+ "on y.DSR = CCID_METADATA.ID_DSR " +
						" where STATUS='COMPLETED' "
						+ ((!listaStati.isEmpty()) ? " AND y.UNI_STATUS in ( " + QueryUtils.getValuesCollection(listaStati) + " )" : "")
						+ ((!listaStati.isEmpty()) ? " AND y.COD_STATUS in ( " + QueryUtils.getValuesCollection(listaStati) + " )" : "");



				String orderSql =  " ORDER BY "+ request.getDecodedSort();

				final Query qCount = em.createNativeQuery(selectCountSql + fromSql);
				Record countRecord = (Record) qCount.setHint(QueryHints.RESULT_TYPE, ResultType.Map).getSingleResult();

				final Query q = em.createNativeQuery(selectSql+fromSql+orderSql ).setFirstResult(request.getFirst()) // offset
						.setMaxResults(1 + request.getLast() - request.getFirst());

				List<Record> ccidS3Paths = q.setHint(QueryHints.RESULT_TYPE, ResultType.Map).getResultList();
				List<UnidentifiedLoadingSearchResultDTO> dtoList = new ArrayList<UnidentifiedLoadingSearchResultDTO>();
				final PagedResult pagedResult = new PagedResult();

				


				for (Record r : ccidS3Paths) {
					UnidentifiedLoadingSearchResultDTO data = new UnidentifiedLoadingSearchResultDTO();
					data.setDspName((String) r.get("DSP"));
					data.setDspPath((String) r.get("DSP_PATH"));
					data.setDsr((String) r.get("DSR"));
//					data.setDsrFilePath((String) r.get("FILE_PATH"));
					DsrPeriod periodo = new DsrPeriod();
					periodo.setPeriod(((Integer) r.get("PERIOD")) != null ? ((Integer) r.get("PERIOD")).toString(): null);
					periodo.setYear(((Integer) r.get("YEAR")) != null ? ((Integer) r.get("YEAR")).toString(): null);
					periodo.setPeriodType((String) r.get("PERIOD_TYPE"));
					data.setDsrPeriod(periodo);
					data.setUtilizationName((String) r.get("TIPO UTILIZZO"));
					data.setCommercialOfferName((String) r.get("OFFERTA COMMERCIALE"));
					data.setCountryName((String)r.get("TERRITORIO"));
					data.setStatus(new UnidentifiedLoadingStateDTO((String)r.get("UNI_STATUS")));

					//R1
					data.setCodificatoStatus(new UnidentifiedLoadingStateDTO((String) r.get("COD_STATUS")));
					//


//					data.setDsrDeliveryDate((Date)r.get("INSERT_TIME"));
//					data.setDsrProcessingStart((Date)r.get("DATA_INIZIO"));
//					data.setDsrProcessingEnd((Date)r.get("DATA_FINE"));

//					data.setKbVersion(((Integer) r.get("IDENTIFY_KB_VERSION")) != null ? ((Integer) r.get("IDENTIFY_KB_VERSION")).toString(): null);
//					data.setErrorType((String)r.get("ERROR_MESSAGE"));


//					data.setProcessingStatus(DsrProcessingStateDTO.statusFromEnum(DsrProcessingStateDTO.STATUS.valueOf((String)r.get("STATUS"))));

					subList.add(data);
				}


//				StatDTO s1 = new StatDTO("Arrivati", ((BigDecimal) countRecord.get("ARRIVED")) != null ? ((BigDecimal) countRecord.get("ARRIVED")).toString(): null);
//				StatDTO s3 = new StatDTO("In Lavorazione", ((BigDecimal) countRecord.get("PROCESSING")) != null ? ((BigDecimal) countRecord.get("PROCESSING")).toString(): null);
//				StatDTO s2 = new StatDTO("Completati", ((BigDecimal) countRecord.get("COMPLETED")) != null ? ((BigDecimal) countRecord.get("COMPLETED")).toString(): null);
//				StatDTO s4 = new StatDTO("In Errore", ((BigDecimal) countRecord.get("ERROR")) != null ? ((BigDecimal) countRecord.get("ERROR")).toString(): null);
//				StatDTO s5 = new StatDTO("Totale", ((Long) countRecord.get("TOTAL")) != null ? ((Long) countRecord.get("TOTAL")).toString(): null);
//				List<StatDTO> stats = Arrays.asList(s1, s2, s3, s4, s5);
//				Map<String, Object> extra = new HashMap<String, Object>();
//				extra.put("stats", stats);
//				PagedResult page = new PagedResult();
//				page.setExtra(extra);
//				page.setRows(subList);
//				page.setMaxrows(((Long) countRecord.get("TOTAL")).intValue());
//				page.setFirst(request.getFirst());
//				page.setLast(request.getLast());
//				page.setHasNext(request.getLast() < (Long) countRecord.get("TOTAL"));
//				page.setHasPrev(request.getFirst() > 0);
//				return Response.ok(gson.toJson(page)).build();
			
			
			
			///////////////////////////////////////////////////////
//			data.subList(request.getFirst(), Math.min(request.getLast(),data.size()));
			PagedResult page = new PagedResult();
			page.setRows(subList);
			page.setMaxrows(((Long) countRecord.get("TOTAL")).intValue());
			page.setFirst(request.getFirst());
			page.setLast(request.getFirst()+ccidS3Paths.size());
			page.setHasNext(request.getLast() < (Long) countRecord.get("TOTAL"));
			page.setHasPrev(request.getFirst() > 0);
			return Response.ok(gson.toJson(page)).build();
		} catch (Exception e) {
			logger.error("Search Request Exception", e);
			return Response.status(500).build();
		}
	}

	@POST
	@Path("/submit-request")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response submitRequest(UnidentifiedLoadingSubmitRequestDTO request) throws IOException {
		try {
			UnidentifiedLoadingSubmitResponseDTO response = new UnidentifiedLoadingSubmitResponseDTO();
			for(UnidentifiedLoadingSearchResultDTO dto : request.getDsrs()) {
//				dto.setStatus(new UnidentifiedLoadingStateDTO(2l,"Caricato"));

				
				String period="";
				if(dto.getDsrPeriod().getPeriodType().equals("quarter"))
					period = "Q" + dto.getDsrPeriod().getPeriod();
				else
					period = String.format("%02d", Integer.parseInt(dto.getDsrPeriod().getPeriod()));
				

				JsonObject messageBody = new JsonObject();
				messageBody.addProperty("idDsr",dto.getDsr());
				messageBody.addProperty("dsp",dto.getDspPath());
				messageBody.addProperty("year",dto.getDsrPeriod().getYear());
				messageBody.addProperty("month",period);
				messageBody.addProperty("country",dto.getCountryName());
//				messageBody.addProperty("checkAlreadyProcessed","false");
				messageBody.addProperty("idUtilizationType",dto.getUtilizationType());

				SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration, "newunidentified.sqs");
				sqsMessagePump.sendToProcessMessage(messageBody, false);
				
			}
			response.setStatus("OK");
			response.setMessage("Caricamento del non identificato richiesto.");
			response.setDsrs(request.getDsrs());
			return Response.ok(gson.toJson(response)).build();
		} catch (Exception e) {
			logger.error("Search Request Exception", e);
			return Response.status(500).build();
		}
	}	

	@POST
	@Path("/clear-unidentified")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response submitRequest() throws IOException {
		try {

			logger.error("CANCELLAZIONE NON IDENTIFICATO!");
			
			EntityManager em = uni.get();
			
			em.getTransaction().begin();
			
			em.createNativeQuery("TRUNCATE TABLE unidentified_song;").executeUpdate();
			em.createNativeQuery("TRUNCATE TABLE unidentified_song_dsr;").executeUpdate();
			
			
			em.getTransaction().commit();
			
			
			Map<String,Object> response = new HashMap<String, Object>();
			response.put("status", "OK");
			response.put("message", "Non identificato eliminato con successo");
			
			
			
			return Response.ok(gson.toJson(response)).build();
		} catch (Exception e) {
			logger.error("Search Request Exception", e);
			return Response.status(500).build();
		}
	}
}
