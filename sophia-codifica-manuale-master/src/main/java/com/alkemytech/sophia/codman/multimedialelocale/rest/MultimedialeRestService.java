package com.alkemytech.sophia.codman.multimedialelocale.rest;

import com.alkemytech.sophia.broadcasting.dto.Periodo;
import com.alkemytech.sophia.codman.gestioneperiodi.GestionePeriodiInterface;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.alkemytech.sophia.codman.multimedialelocale.dto.FieldDto;
import com.alkemytech.sophia.codman.multimedialelocale.dto.MlIntegrazioneResponse;
import com.alkemytech.sophia.codman.multimedialelocale.dto.MultimedialeLocaleIngestionRequestDto;
import com.alkemytech.sophia.codman.multimedialelocale.entity.ModalitaUtilizzazione;
import com.alkemytech.sophia.codman.multimedialelocale.entity.MultimedialeLocale;
import com.alkemytech.sophia.codman.utils.Response;
import com.alkemytech.sophia.common.s3.S3Service;
import com.amazonaws.services.s3.AmazonS3URI;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import org.apache.http.util.TextUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response.Status;

@Singleton
@Path("integrazione")
public class MultimedialeRestService {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Gson gson;
	private final Provider<EntityManager> provider;
	private final GestionePeriodiInterface gestionePeriodi;
	private final S3Service s3Service;

	@Inject
	public MultimedialeRestService(S3Service s3Service, @McmdbDataSource Provider<EntityManager> provider, Gson gson,
			GestionePeriodiInterface gestionePeriodi) {
		super();
		this.provider = provider;
		this.gson = gson;
		this.gestionePeriodi = gestionePeriodi;
		this.s3Service = s3Service;
	}

	@POST
	@Path("multimediale/locale/report")
	@Produces("application/json")
	public javax.ws.rs.core.Response multimedialeLocaleOnTable(
			MultimedialeLocaleIngestionRequestDto multimedialeLocaleDto) {
		EntityManager entityManager = provider.get();
		Periodo periodo;
		final MultimedialeLocale multimedialeLocale = new MultimedialeLocale();
		try {
			if (TextUtils.isEmpty(multimedialeLocaleDto.getLicenza())) {
				return javax.ws.rs.core.Response.status(Status.BAD_REQUEST)
						.entity(Response.MULTIMEDIALE_LOCALE_LICENZA_MANDATORY).build();
			}
			if (TextUtils.isEmpty(multimedialeLocaleDto.getCodiceLicenziatario())) {
				return javax.ws.rs.core.Response.status(Status.BAD_REQUEST)
						.entity(Response.MULTIMEDIALE_LOCALE_CODICE_LICENZIATARIO_MANDATORY).build();
			}
			if (TextUtils.isEmpty(multimedialeLocaleDto.getServizio())) {
				return javax.ws.rs.core.Response.status(Status.BAD_REQUEST)
						.entity(Response.MULTIMEDIALE_LOCALE_SERVIZIO_MANDATORY).build();
			}
			if (TextUtils.isEmpty(multimedialeLocaleDto.getPeriodoCompetenza())) {
				return javax.ws.rs.core.Response.status(Status.BAD_REQUEST)
						.entity(Response.MULTIMEDIALE_LOCALE_PERIODO_COMPETENZA_MANDATORY).build();
			}
			if (null == multimedialeLocaleDto.getModUtilizzazione()) {
				return javax.ws.rs.core.Response.status(Status.BAD_REQUEST)
						.entity(Response.MULTIMEDIALE_LOCALE_MOD_UTILIZZAZIONE_MANDATORY).build();
			}
			if (TextUtils.isEmpty(multimedialeLocaleDto.getModFruizione())) {
				// return
				// javax.ws.rs.core.Response.status(Status.BAD_REQUEST).entity(Response.MULTIMEDIALE_LOCALE_MOD_FRUIZIONE_MANDATORY).build();
			} else {
				if (!getModalitaFruizione().contains(multimedialeLocaleDto.getModFruizione())) {
					return javax.ws.rs.core.Response.status(Status.BAD_REQUEST)
							.entity(Response.MULTIMEDIALE_LOCALE_MOD_FRUIZIONE_NOT_VALID).build();
				}
			}
			if (null == multimedialeLocaleDto.getInAbbonamento()) {
				return javax.ws.rs.core.Response.status(Status.BAD_REQUEST)
						.entity(Response.MULTIMEDIALE_LOCALE_ABBONAMENTO_MANDATORY).build();
			}
			if (TextUtils.isEmpty(multimedialeLocaleDto.getPosizioneReport())) {
				return javax.ws.rs.core.Response.status(Status.BAD_REQUEST)
						.entity(Response.MULTIMEDIALE_LOCALE_POSIZIONE_REPORT_MANDATORY).build();
			}
			if (multimedialeLocaleDto.getIdentificativoReport() == null) {
				return javax.ws.rs.core.Response.status(Status.BAD_REQUEST)
						.entity(Response.MULTIMEDIALE_LOCALE_IDENTIFICATIVO_REPORT_MANDATORY).build();
			}

			if (multimedialeLocaleDto.getProgressivoReport() == null) {
				return javax.ws.rs.core.Response.status(Status.BAD_REQUEST)
						.entity(Response.MULTIMEDIALE_LOCALE_PROGRESSIVO_REPORT_MANDATORY).build();
			}

			if (!FieldDto.containsServizio(multimedialeLocaleDto.getServizio())) {
				return javax.ws.rs.core.Response.status(Status.BAD_REQUEST)
						.entity(Response.MULTIMEDIALE_LOCALE_SERVIZIO_NOT_EXIST).build();
			}

			try {
				periodo = gestionePeriodi.beutyfyDate(multimedialeLocaleDto.getPeriodoCompetenza());
				if (periodo == null) {
					return javax.ws.rs.core.Response.status(Status.BAD_REQUEST)
							.entity(Response.MULTIMEDIALE_LOCALE_FORMATO_PERIODO_COMPETENZA_NOT_VALID).build();
				}
			} catch (Exception e) {
				return javax.ws.rs.core.Response.status(Status.BAD_REQUEST)
						.entity(Response.MULTIMEDIALE_LOCALE_FORMATO_PERIODO_COMPETENZA_NOT_VALID).build();
			}
			try {
				AmazonS3URI amazonS3URI = new AmazonS3URI(multimedialeLocaleDto.getPosizioneReport());
				if (!s3Service.doesObjectExist(amazonS3URI.getBucket(), amazonS3URI.getKey())) {
					return javax.ws.rs.core.Response.status(Status.BAD_REQUEST)
							.entity(Response.MULTIMEDIALE_LOCALE_REPORT_NOT_EXIST).build();
				}
			} catch (Exception e) {
				return javax.ws.rs.core.Response.status(Status.BAD_REQUEST)
						.entity(Response.MULTIMEDIALE_LOCALE_REPORT_NOT_EXIST).build();
			}
			multimedialeLocale.setLicenza(multimedialeLocaleDto.getLicenza());
			multimedialeLocale.setCodiceLicenziatario(multimedialeLocaleDto.getCodiceLicenziatario());
			if (TextUtils.isEmpty(multimedialeLocaleDto.getNominativoLicenziatario())) {
				multimedialeLocale.setNominativoLicenziatario(null);
			} else {
				multimedialeLocale.setNominativoLicenziatario(multimedialeLocaleDto.getNominativoLicenziatario());
			}
			multimedialeLocale.setServizio(multimedialeLocaleDto.getServizio());
			multimedialeLocale.setOrigPeriodoCompetenza(multimedialeLocaleDto.getPeriodoCompetenza());
			multimedialeLocale.setPeriodoCompetenzaDataDa(periodo.getDataInizioPeriodo());
			multimedialeLocale.setPeriodoCompetenzaDataA(periodo.getDataFinePeriodo());
			try {
				ModalitaUtilizzazione modalitaUtilizzazione = (ModalitaUtilizzazione) entityManager
						.createQuery("SELECT x FROM ModalitaUtilizzazione x WHERE x.id=:id")
						.setParameter("id", multimedialeLocaleDto.getModUtilizzazione()).getSingleResult();
				multimedialeLocale.setModUtilizzazione(modalitaUtilizzazione);
			} catch (Exception e) {
				return javax.ws.rs.core.Response.status(Status.BAD_REQUEST)
						.entity(Response.MULTIMEDIALE_LOCALE_MOD_UTILIZZAZIONE_NOT_EXIST).build();
			}
			multimedialeLocale.setModFruizione(multimedialeLocaleDto.getModFruizione());
			multimedialeLocale.setInAbbonamento(multimedialeLocaleDto.getInAbbonamento());
			multimedialeLocale.setPosizioneReport(multimedialeLocaleDto.getPosizioneReport());
			multimedialeLocale.setProgressivoReport(multimedialeLocaleDto.getProgressivoReport());
			multimedialeLocale.setIdentificativoReport(multimedialeLocaleDto.getIdentificativoReport());
			multimedialeLocale.setStatoLogico(Response.MULTIMEDIALE_LOCALE_STATO_CARICAMENTO);
			multimedialeLocale.setStato(Response.MULTIMEDIALE_LOCALE_STATO_CARICAMENTO);
			multimedialeLocale.setUsaCodiceOpera(multimedialeLocaleDto.isUsaCodiceOpera());
			multimedialeLocale.setDataUpload(new Date());
			entityManager.getTransaction().begin();

			try {
				MultimedialeLocale multimediale = null;
				multimediale = (MultimedialeLocale) entityManager.createQuery(
						"SELECT x FROM  MultimedialeLocale x WHERE x.identificativoReport=:identificativoReport")
						.setParameter("identificativoReport", multimedialeLocaleDto.getIdentificativoReport())
						.getSingleResult();
				if (multimediale != null) {
					return javax.ws.rs.core.Response.status(Status.BAD_REQUEST)
							.entity(Response.MULTIMEDIALE_LOCALE_IDENTIFICATIVO_REPORT_ALREADY_EXIST).build();
				}

			} catch (NoResultException e) {
				// TODO: handle exception
			} catch (Exception e) {
				return javax.ws.rs.core.Response.status(Status.INTERNAL_SERVER_ERROR).entity("Errore imprevisto")
						.build();
			}

			try {
				MultimedialeLocale multimediale = null;
				multimediale = (MultimedialeLocale) entityManager
						.createQuery("SELECT x FROM  MultimedialeLocale x WHERE " + "x.attivo=true AND "
								+ "x.licenza=:licenza AND " + "x.servizio=:servizio AND "
								+ "x.modUtilizzazione.id =:modUtilizzazione AND "
								+ "x.origPeriodoCompetenza=:origPeriodoCompetenza")
						.setParameter("licenza", multimedialeLocaleDto.getLicenza())
						.setParameter("servizio", multimedialeLocaleDto.getServizio())
						.setParameter("modUtilizzazione", multimedialeLocaleDto.getModUtilizzazione())
						.setParameter("origPeriodoCompetenza", multimedialeLocaleDto.getPeriodoCompetenza())
						.getSingleResult();
				if (multimediale != null) {
					if (multimediale.getProgressivoReport() >= multimedialeLocaleDto.getProgressivoReport()) {
						return javax.ws.rs.core.Response.status(Status.BAD_REQUEST)
								.entity(Response.MULTIMEDIALE_LOCALE_REPORT_ALREADY_EXIST).build();
					} else {
						multimediale.setAttivo(false);
					}
				}
			} catch (NoResultException e) {
				// TODO: handle exception
			} catch (Exception e) {
				return javax.ws.rs.core.Response.status(Status.INTERNAL_SERVER_ERROR).entity("Errore imprevisto")
						.build();
			}
			multimedialeLocale.setAttivo(true);
			entityManager.persist(multimedialeLocale);
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
		}
		MlIntegrazioneResponse mlIntegrazioneResponse = new MlIntegrazioneResponse(multimedialeLocale.getIdMlReport());
		return javax.ws.rs.core.Response.ok(gson.toJson(mlIntegrazioneResponse)).build();
	}

	public ArrayList<String> getModalitaFruizione() {
		ArrayList<String> modalitaFruizione = new ArrayList<>();
		modalitaFruizione.add("Pagamento");
		modalitaFruizione.add("Gratuito");
		return modalitaFruizione;
	}
}
