package com.alkemytech.sophia.codman.performing.dto;

import com.alkemytech.sophia.codman.dto.ResponseDTO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class SessioneCodificaDTO extends ResponseDTO implements Serializable {
    private String idSessioneCodifica;
    private Date dataFineValid;
    private Date dataInizioValid;
    private String utente;
    private List<UtilizzazioniAssegnateDTO> utilizzazioniAssegnateDTOList;

    public SessioneCodificaDTO(String idSessioneCodifica,
                               Date dataFineValid,
                               Date dataInizioValid,
                               String utente) {
        this.idSessioneCodifica = idSessioneCodifica;
        this.dataFineValid = dataFineValid;
        this.dataInizioValid = dataInizioValid;
        this.utente = utente;
    }

    public SessioneCodificaDTO() {
    }

    public String getIdSessioneCodifica() {
        return idSessioneCodifica;
    }

    public void setIdSessioneCodifica(String idSessioneCodifica) {
        this.idSessioneCodifica = idSessioneCodifica;
    }

    public Date getDataFineValid() {
        return dataFineValid;
    }

    public void setDataFineValid(Date dataFineValid) {
        this.dataFineValid = dataFineValid;
    }

    public Date getDataInizioValid() {
        return dataInizioValid;
    }

    public void setDataInizioValid(Date dataInizioValid) {
        this.dataInizioValid = dataInizioValid;
    }

    public String getUtente() {
        return utente;
    }

    public void setUtente(String utente) {
        this.utente = utente;
    }

    public List<UtilizzazioniAssegnateDTO> getUtilizzazioniAssegnateDTOList() {
        return utilizzazioniAssegnateDTOList;
    }

    public void setUtilizzazioniAssegnateDTOList(List<UtilizzazioniAssegnateDTO> utilizzazioniAssegnateDTOList) {
        this.utilizzazioniAssegnateDTOList = utilizzazioniAssegnateDTOList;
    }


}
