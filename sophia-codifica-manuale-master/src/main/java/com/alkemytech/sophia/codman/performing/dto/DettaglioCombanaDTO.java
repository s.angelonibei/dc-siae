package com.alkemytech.sophia.codman.performing.dto;


import com.alkemytech.sophia.codman.utils.DateUtils;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;
import com.opencsv.bean.CsvNumber;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class DettaglioCombanaDTO implements Serializable {

    @CsvBindByPosition(position = 0)
    @CsvBindByName(column = "ID Combana")
    private Long idCombana;

    @CsvBindByPosition(position = 1)
    @CsvBindByName(column = "Area")
    private String area;

    @CsvBindByPosition(position = 2)
    @CsvBindByName(column = "Codice Voce Incasso")
    private String codVoceIncasso;

    @CsvBindByPosition(position = 3)
    @CsvBindByName(column = "Periodo Ripartizione")
    private String periodoRipartizione;

    @CsvBindByPosition(position = 4)
    @CsvBindByName(column = "Codice Opera")
    private String codiceOpera;

    @CsvBindByPosition(position = 5)
    @CsvBindByName(column = "Titolo Composizione")
    private String titoloComposizione;


    @CsvBindByPosition(position = 6)
    @CsvBindByName(column = "Compositore")
    private String compositore;

    @CsvBindByPosition(position = 7)
    @CsvBindByName(column = "Autori")
    private String autori;

    @CsvBindByPosition(position = 8)
    @CsvBindByName(column = "Interpreti")
    private String interpreti;

    @CsvBindByPosition(position = 9)
    @CsvBindByName(column = "Durata")
    private String durata;

    @CsvBindByPosition(position = 10)
    @CsvBindByName(column = "N. Programma Musicale")
    private BigInteger numeroProgrammaMusicale;

    @CsvBindByPosition(position = 11, locale = "it-IT")
    @CsvBindByName(column = "Importi Maturati", locale = "it-IT")
    @CsvNumber("#.###0,00¤")
    private BigDecimal importiMaturati;

    private Date competenzeDa;

    private Date competenzeA;

    public DettaglioCombanaDTO() {
    }

    public DettaglioCombanaDTO(Long idCombana, String periodoRipartizione,
                               Date competenzeDa,
                               Date competenzeA,
                               String codVoceIncasso,
                               String area,
                               BigDecimal importiMaturati,
                               String titoloComposizione,
                               String autori,
                               String compositore,
                               String interpreti,
                               BigInteger durata,
                               String codiceOpera,
                               BigInteger numeroProgrammaMusicale) {
        this.idCombana = idCombana;
        this.codVoceIncasso = codVoceIncasso;
        this.area = area;
        this.importiMaturati = importiMaturati;
        this.periodoRipartizione = periodoRipartizione;
        this.competenzeDa = competenzeDa;
        this.competenzeA = competenzeA;
        this.titoloComposizione = titoloComposizione;
        this.autori = autori;
        this.compositore = compositore;
        this.interpreti = interpreti;
        this.durata = DateUtils.formatDurationInSeconds(durata);
        this.codiceOpera = codiceOpera;
        this.numeroProgrammaMusicale = numeroProgrammaMusicale;
    }

    public String getCodVoceIncasso() {
        return codVoceIncasso;
    }

    public void setCodVoceIncasso(String codVoceIncasso) {
        this.codVoceIncasso = codVoceIncasso;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public BigDecimal getImportiMaturati() {
        return importiMaturati;
    }

    public void setImportiMaturati(BigDecimal importiMaturati) {
        this.importiMaturati = importiMaturati;
    }

    public String getPeriodoRipartizione() {
        return periodoRipartizione;
    }

    public void setPeriodoRipartizione(String periodoRipartizione) {
        this.periodoRipartizione = periodoRipartizione;
    }

    public Date getCompetenzeDa() {
        return competenzeDa;
    }

    public void setCompetenzeDa(Date competenzeDa) {
        this.competenzeDa = competenzeDa;
    }

    public Date getCompetenzeA() {
        return competenzeA;
    }

    public void setCompetenzeA(Date competenzeA) {
        this.competenzeA = competenzeA;
    }

    public String getTitoloComposizione() {
        return titoloComposizione;
    }

    public void setTitoloComposizione(String titoloComposizione) {
        this.titoloComposizione = titoloComposizione;
    }

    public String getAutori() {
        return autori;
    }

    public void setAutori(String autori) {
        this.autori = autori;
    }

    public String getCompositore() {
        return compositore;
    }

    public void setCompositore(String compositore) {
        this.compositore = compositore;
    }

    public String getDurata() {
        return durata;
    }

    public void setDurata(String durata) {
        this.durata = durata;
    }

    public String getCodiceOpera() {
        return codiceOpera;
    }

    public void setCodiceOpera(String codiceOpera) {
        this.codiceOpera = codiceOpera;
    }

    public BigInteger getNumeroProgrammaMusicale() {
        return numeroProgrammaMusicale;
    }

    public void setNumeroProgrammaMusicale(BigInteger numeroProgrammaMusicale) {
        this.numeroProgrammaMusicale = numeroProgrammaMusicale;
    }

    public String getInterpreti() {
        return interpreti;
    }

    public void setInterpreti(String interpreti) {
        this.interpreti = interpreti;
    }

    public Long getIdCombana() {
        return idCombana;
    }

    public void setIdCombana(Long idCombana) {
        this.idCombana = idCombana;
    }
}
