package com.alkemytech.sophia.codman.entity.performing;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import java.math.BigInteger;


/**
 * The persistent class for the PERF_EVENTI_PAGATI database table.
 * 
 */
@Entity
@Table(name="PERF_EVENTI_PAGATI")
@NamedQuery(name="PerfEventiPagati.findAll", query="SELECT p FROM PerfEventiPagati p")
public class PerfEventiPagati implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID_EVENTO_PAGATO", unique=true, nullable=false)
	private String idEventoPagato;

	@Column(name="AGENZIA", length=50)
	private String agenzia;

	@Column(name="CODICE_BA_LOCALE", length=20)
	private String codiceBaLocale;

	@Column(name="CODICE_RAGGRUPPAMENTO", length=50)
	private String codiceRaggruppamento;

	@Column(name="CODICE_SPEI_LOCALE", length=20)
	private String codiceSpeiLocale;

	@Column(name="CONTABILITA")
	private BigInteger contabilita;

	@Column(name="CONTABILITA_ORIG")
	private BigInteger contabilitaOrig;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_INIZIO_EVENTO")
	private Date dataInizioEvento;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_INS")
	private Date dataIns;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_REVERSALE")
	private Date dataReversale;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_RIPARTIZIONE")
	private Date dataRipartizione;

	@Column(name="DENOMINAZIONE_LOCALE", length=110)
	private String denominazioneLocale;

	@Column(name="ID_EVENTO")
	private BigInteger idEvento;

	@Column(name="ID_PERIODO_RIPARTIZIONE")
	private BigInteger idPeriodoRipartizione;

	@Column(name="IMPORTO_AGGIO", precision=10, scale=15)
	private BigDecimal importoAggio;

	@Column(name="IMPORTO_DEM", precision=10, scale=7)
	private BigDecimal importoDem;

	@Column(name="IMPORTO_DEM_NETTO", precision=10, scale=7)
	private BigDecimal importoDemNetto;

	@Column(name="IMPORTO_EX_ART_18", precision=10, scale=15)
	private BigDecimal importoExArt18;

	@Column(name="NUMERO_FATTURA", length=20)
	private String numeroFattura;

	@Column(name="NUMERO_PM_ATTESI")
	private BigInteger numeroPmAttesi;

	@Column(name="NUMERO_PM_ATTESI_SPALLA")
	private BigInteger numeroPmAttesiSpalla;

	@Column(name="NUMERO_REVERSALE", length=20)
	private String numeroReversale;

	@Column(name="ORA_INIZIO_EVENTO", length=5)
	private String oraInizioEvento;

	@Column(name="PERMESSO", length=19)
	private String permesso;

	@Column(name="PM_ANNULLATI")
	private BigInteger pmAnnullati;

	@Column(name="PM_NON_RIENTRATI")
	private BigInteger pmNonRientrati;

	@Column(name="QUADRATURA_NDM", length=1)
	private String quadraturaNdm;

	@Column(name="SEDE", length=50)
	private String sede;

	@Column(name="SEPRAG", length=10)
	private String seprag;

	@Column(name="TIPO_DOCUMENTO_CONTABILE", length=20)
	private String tipoDocumentoContabile;

	@Column(name="VALORE_ANNULLATO", precision=10, scale=15)
	private BigDecimal valoreAnnullato;

	@Column(name="VALORE_NON_CODIFICATO", precision=10, scale=15)
	private BigDecimal valoreNonCodificato;

	@Column(name="VALORE_NON_RIENTRATO", precision=10, scale=15)
	private BigDecimal valoreNonRientrato;

	@Column(name="VOCE_INCASSO", length=5)
	private String voceIncasso;

	public PerfEventiPagati() {
	}

	public String getIdEventoPagato() {
		return this.idEventoPagato;
	}

	public void setIdEventoPagato(String idEventoPagato) {
		this.idEventoPagato = idEventoPagato;
	}

	public String getAgenzia() {
		return this.agenzia;
	}

	public void setAgenzia(String agenzia) {
		this.agenzia = agenzia;
	}

	public String getCodiceBaLocale() {
		return this.codiceBaLocale;
	}

	public void setCodiceBaLocale(String codiceBaLocale) {
		this.codiceBaLocale = codiceBaLocale;
	}

	public String getCodiceRaggruppamento() {
		return this.codiceRaggruppamento;
	}

	public void setCodiceRaggruppamento(String codiceRaggruppamento) {
		this.codiceRaggruppamento = codiceRaggruppamento;
	}

	public String getCodiceSpeiLocale() {
		return this.codiceSpeiLocale;
	}

	public void setCodiceSpeiLocale(String codiceSpeiLocale) {
		this.codiceSpeiLocale = codiceSpeiLocale;
	}

	public BigInteger getContabilita() {
		return this.contabilita;
	}

	public void setContabilita(BigInteger contabilita) {
		this.contabilita = contabilita;
	}

	public BigInteger getContabilitaOrig() {
		return this.contabilitaOrig;
	}

	public void setContabilitaOrig(BigInteger contabilitaOrig) {
		this.contabilitaOrig = contabilitaOrig;
	}

	public Date getDataInizioEvento() {
		return this.dataInizioEvento;
	}

	public void setDataInizioEvento(Date dataInizioEvento) {
		this.dataInizioEvento = dataInizioEvento;
	}

	public Date getDataIns() {
		return this.dataIns;
	}

	public void setDataIns(Date dataIns) {
		this.dataIns = dataIns;
	}

	public Date getDataReversale() {
		return this.dataReversale;
	}

	public void setDataReversale(Date dataReversale) {
		this.dataReversale = dataReversale;
	}

	public Date getDataRipartizione() {
		return this.dataRipartizione;
	}

	public void setDataRipartizione(Date dataRipartizione) {
		this.dataRipartizione = dataRipartizione;
	}

	public String getDenominazioneLocale() {
		return this.denominazioneLocale;
	}

	public void setDenominazioneLocale(String denominazioneLocale) {
		this.denominazioneLocale = denominazioneLocale;
	}

	public BigInteger getIdEvento() {
		return this.idEvento;
	}

	public void setIdEvento(BigInteger idEvento) {
		this.idEvento = idEvento;
	}

	public BigInteger getIdPeriodoRipartizione() {
		return this.idPeriodoRipartizione;
	}

	public void setIdPeriodoRipartizione(BigInteger idPeriodoRipartizione) {
		this.idPeriodoRipartizione = idPeriodoRipartizione;
	}

	public BigDecimal getImportoAggio() {
		return this.importoAggio;
	}

	public void setImportoAggio(BigDecimal importoAggio) {
		this.importoAggio = importoAggio;
	}

	public BigDecimal getImportoDem() {
		return this.importoDem;
	}

	public void setImportoDem(BigDecimal importoDem) {
		this.importoDem = importoDem;
	}

	public BigDecimal getImportoDemNetto() {
		return this.importoDemNetto;
	}

	public void setImportoDemNetto(BigDecimal importoDemNetto) {
		this.importoDemNetto = importoDemNetto;
	}

	public BigDecimal getImportoExArt18() {
		return this.importoExArt18;
	}

	public void setImportoExArt18(BigDecimal importoExArt18) {
		this.importoExArt18 = importoExArt18;
	}

	public String getNumeroFattura() {
		return this.numeroFattura;
	}

	public void setNumeroFattura(String numeroFattura) {
		this.numeroFattura = numeroFattura;
	}

	public BigInteger getNumeroPmAttesi() {
		return this.numeroPmAttesi;
	}

	public void setNumeroPmAttesi(BigInteger numeroPmAttesi) {
		this.numeroPmAttesi = numeroPmAttesi;
	}

	public BigInteger getNumeroPmAttesiSpalla() {
		return this.numeroPmAttesiSpalla;
	}

	public void setNumeroPmAttesiSpalla(BigInteger numeroPmAttesiSpalla) {
		this.numeroPmAttesiSpalla = numeroPmAttesiSpalla;
	}

	public String getNumeroReversale() {
		return this.numeroReversale;
	}

	public void setNumeroReversale(String numeroReversale) {
		this.numeroReversale = numeroReversale;
	}

	public String getOraInizioEvento() {
		return this.oraInizioEvento;
	}

	public void setOraInizioEvento(String oraInizioEvento) {
		this.oraInizioEvento = oraInizioEvento;
	}

	public String getPermesso() {
		return this.permesso;
	}

	public void setPermesso(String permesso) {
		this.permesso = permesso;
	}

	public BigInteger getPmAnnullati() {
		return this.pmAnnullati;
	}

	public void setPmAnnullati(BigInteger pmAnnullati) {
		this.pmAnnullati = pmAnnullati;
	}

	public BigInteger getPmNonRientrati() {
		return this.pmNonRientrati;
	}

	public void setPmNonRientrati(BigInteger pmNonRientrati) {
		this.pmNonRientrati = pmNonRientrati;
	}

	public String getQuadraturaNdm() {
		return this.quadraturaNdm;
	}

	public void setQuadraturaNdm(String quadraturaNdm) {
		this.quadraturaNdm = quadraturaNdm;
	}

	public String getSede() {
		return this.sede;
	}

	public void setSede(String sede) {
		this.sede = sede;
	}

	public String getSeprag() {
		return this.seprag;
	}

	public void setSeprag(String seprag) {
		this.seprag = seprag;
	}

	public String getTipoDocumentoContabile() {
		return this.tipoDocumentoContabile;
	}

	public void setTipoDocumentoContabile(String tipoDocumentoContabile) {
		this.tipoDocumentoContabile = tipoDocumentoContabile;
	}

	public BigDecimal getValoreAnnullato() {
		return this.valoreAnnullato;
	}

	public void setValoreAnnullato(BigDecimal valoreAnnullato) {
		this.valoreAnnullato = valoreAnnullato;
	}

	public BigDecimal getValoreNonCodificato() {
		return this.valoreNonCodificato;
	}

	public void setValoreNonCodificato(BigDecimal valoreNonCodificato) {
		this.valoreNonCodificato = valoreNonCodificato;
	}

	public BigDecimal getValoreNonRientrato() {
		return this.valoreNonRientrato;
	}

	public void setValoreNonRientrato(BigDecimal valoreNonRientrato) {
		this.valoreNonRientrato = valoreNonRientrato;
	}

	public String getVoceIncasso() {
		return this.voceIncasso;
	}

	public void setVoceIncasso(String voceIncasso) {
		this.voceIncasso = voceIncasso;
	}

}