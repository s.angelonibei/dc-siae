package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;

import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.GsonBuilder;

@Produces("application/json")
@XmlRootElement 
@SuppressWarnings("serial")
public class IdentifiedSongDsrDTO implements Serializable {
	
	private String idUtil;
	private String idDsr;
	private String proprietaryId;
	private String albumTitle;
	private String isrc;
	private String iswc;
	private String dsp;
		
	public String getIdUtil() {
		return idUtil;
	}

	public void setIdUtil(String idUtil) {
		this.idUtil = idUtil;
	}

	public String getIdDsr() {
		return idDsr;
	}

	public void setIdDsr(String idDsr) {
		this.idDsr = idDsr;
	}

	public String getProprietaryId() {
		return proprietaryId;
	}

	public void setProprietaryId(String proprietaryId) {
		this.proprietaryId = proprietaryId;
	}

	public String getAlbumTitle() {
		return albumTitle;
	}

	public void setAlbumTitle(String albumTitle) {
		this.albumTitle = albumTitle;
	}

	public String getIsrc() {
		return isrc;
	}

	public void setIsrc(String isrc) {
		this.isrc = isrc;
	}

	public String getIswc() {
		return iswc;
	}

	public void setIswc(String iswc) {
		this.iswc = iswc;
	}

	public String getDsp() {
		return dsp;
	}

	public void setDsp(String dsp) {
		this.dsp = dsp;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}

}
