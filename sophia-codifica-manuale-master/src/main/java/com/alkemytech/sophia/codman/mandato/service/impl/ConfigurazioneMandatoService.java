package com.alkemytech.sophia.codman.mandato.service.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.codman.dto.CommercialOffersConfigDTO;
import com.alkemytech.sophia.codman.dto.ConfigurazioneMandatoDTO;
import com.alkemytech.sophia.codman.dto.MandatoDTO;
import com.alkemytech.sophia.codman.dto.SocietaTutelaDTO;
import com.alkemytech.sophia.codman.dto.UtilizationTypeDTO;
import com.alkemytech.sophia.codman.entity.AnagCountry;
import com.alkemytech.sophia.codman.entity.AnagDsp;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.alkemytech.sophia.codman.mandato.service.IConfigurazioneMandatoService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

@Singleton
public class ConfigurazioneMandatoService implements IConfigurazioneMandatoService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private Provider<EntityManager> provider;

	@Inject
	public ConfigurazioneMandatoService(@McmdbDataSource Provider<EntityManager> provider) {
		super();
		this.provider = provider;
	}

	@Override
	public ConfigurazioneMandatoDTO get(Integer id) {
		EntityManager em = provider.get();
		String mandatoSQL = new StringBuffer().append(mandatoSQL())
				.append(" WHERE mc.ID = ?").toString();
        Query query = em.createNativeQuery(mandatoSQL);
        query.setParameter(1, id);
        Object[] data = (Object[]) query.getSingleResult();
		ConfigurazioneMandatoDTO dto = new ConfigurazioneMandatoDTO();
		dto.setId((Integer) data[0]);
		fillMandato(dto, data);
		fillDsp(dto, data);
		fillUtilization(dto, data);
		fillCommercialOffer(dto, data);
		fillCountry(dto, data);
		dto.setValidaDa((Date) data[6]);
		dto.setValidaA((Date) data[7]);
		return dto;
	}	

	@Override
	public List<ConfigurazioneMandatoDTO> getAll() {
		EntityManager em = provider.get();
		String mandatoSQL = new StringBuffer().append(mandatoSQL()).append(" ORDER BY mc.VALIDA_DA DESC").toString();
		Query query = em.createNativeQuery(mandatoSQL);
		List<Object[]> resultList = query.getResultList();
		List<ConfigurazioneMandatoDTO> results = new ArrayList<ConfigurazioneMandatoDTO>();
		for (Object[] data : resultList) {
			ConfigurazioneMandatoDTO dto = new ConfigurazioneMandatoDTO();
			dto.setId((Integer) data[0]);
			fillMandato(dto, data);
			fillDsp(dto, data);
			fillUtilization(dto, data);
			fillCommercialOffer(dto, data);
			fillCountry(dto, data);
			dto.setValidaDa((Date) data[6]);
			dto.setValidaA((Date) data[7]);
			results.add(dto);
		}
		return results;
	}

	@Override
	public ConfigurazioneMandatoDTO save(ConfigurazioneMandatoDTO configurazioneMandato) {
		EntityManager em = provider.get();
		if (configurazioneMandato.getId() == null) {                        
            String sqlInsertMandatoConfigurazione = new StringBuffer()
    				.append("INSERT INTO MANDATO_CONFIGURAZIONE ")
    				.append("( ID_MANDATO")
    				.append(", DSP")
    				.append(", TIPO_UTILIZZO")
    				.append(", OFFERTA_COMMERCIALE")
    				.append(", TERRITORIO")
    				.append(", VALIDA_DA")
    				.append(", VALIDA_A")
    				.append(") values ( ?, ?, ?, ?, ?, ?, ?)")
    				.toString();
            Query insertMandatoConfigurazione = em.createNativeQuery(sqlInsertMandatoConfigurazione);
            insertMandatoConfigurazione.setParameter(1, configurazioneMandato.getMandato().getId());
            insertMandatoConfigurazione.setParameter(2, configurazioneMandato.getDsp().getId());
            insertMandatoConfigurazione.setParameter(3, configurazioneMandato.getUtilization().getIdUtilizationType());
			insertMandatoConfigurazione.setParameter(4,
					configurazioneMandato.getOffer().getIdCommercialOffering() != -1 ? configurazioneMandato.getOffer().getIdCommercialOffering() : "*");
            insertMandatoConfigurazione.setParameter(5, configurazioneMandato.getCountry().getIdCountry());
            insertMandatoConfigurazione.setParameter(6, new java.sql.Date(configurazioneMandato.getValidaDa().getTime()));
            if(configurazioneMandato.getValidaA()!=null) {
            	insertMandatoConfigurazione.setParameter(7, new java.sql.Date(configurazioneMandato.getValidaA().getTime()));
            }else {
            	insertMandatoConfigurazione.setParameter(7, null);
            }
            em.getTransaction().begin();
            insertMandatoConfigurazione.executeUpdate();
            BigInteger id = (BigInteger) em.createNativeQuery("SELECT LAST_INSERT_ID()").getSingleResult();
            em.flush();
            em.getTransaction().commit();
            em.clear();		
            configurazioneMandato.setId(id.intValue());
            return configurazioneMandato;
		} else {
            String sqlInsertMandatoConfigurazione = new StringBuffer()
    				.append("UPDATE MANDATO_CONFIGURAZIONE SET")
    				.append(" DSP = ?2")
    				.append(", TIPO_UTILIZZO = ?3")
    				.append(", OFFERTA_COMMERCIALE = ?4")
    				.append(", TERRITORIO = ?5")
    				.append(", VALIDA_DA = ?6")
    				.append(", VALIDA_A = ?7")
    				.append(" WHERE ID = ?1")
    				.toString();
            Query insertMandatoConfigurazione = em.createNativeQuery(sqlInsertMandatoConfigurazione);
            insertMandatoConfigurazione.setParameter(1, configurazioneMandato.getId());
            insertMandatoConfigurazione.setParameter(2, configurazioneMandato.getDsp().getId());
            insertMandatoConfigurazione.setParameter(3, configurazioneMandato.getUtilization().getIdUtilizationType());
			insertMandatoConfigurazione.setParameter(4,
					configurazioneMandato.getOffer().getIdCommercialOffering() != -1 ? configurazioneMandato.getOffer().getIdCommercialOffering() : "*");
            insertMandatoConfigurazione.setParameter(5, configurazioneMandato.getCountry().getIdCountry());
            insertMandatoConfigurazione.setParameter(6, configurazioneMandato.getValidaDa());
            insertMandatoConfigurazione.setParameter(7, configurazioneMandato.getValidaA());
            em.getTransaction().begin();
            insertMandatoConfigurazione.executeUpdate();
            em.flush();
            em.getTransaction().commit();
            em.clear();			            
			return configurazioneMandato;
		}
	}

	@Override
	public void delete(ConfigurazioneMandatoDTO configurazioneMandatoDTO) {
		EntityManager em = provider.get();
		try {
			em.getTransaction().begin();
			final String sql = new StringBuffer().append("DELETE FROM MANDATO_CONFIGURAZIONE ").append("WHERE ID = ? ").toString();
			Query query = em.createNativeQuery(sql);
			query.setParameter(1, configurazioneMandatoDTO.getId());
			query.executeUpdate();
			em.flush();
            em.getTransaction().commit();
            em.clear();			
		} catch (Exception ex) {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			throw ex;
		}
	}

	protected String mandatoSQL() {
		String sql = "SELECT mc.ID AS ID_MANDATO_CONFIGURAZIONE, " + // 0
				"             mc.ID_MANDATO, " + // 1
				"             mc.DSP, " + // 2
				"             mc.TIPO_UTILIZZO, " + // 3
				"             mc.OFFERTA_COMMERCIALE, " + // 4
				"             mc.TERRITORIO, " + // 5
				"             mc.VALIDA_DA, " + // 6
				"             mc.VALIDA_A, " + // 7
				"             m.DATA_INIZIO_MANDATO, " + // 8
				"             m.DATA_FINE_MANDATO, " + // 9
				"             mandataria.ID AS ID_MANDATARIA, " + // 10
				"             mandataria.CODICE AS CODICE_MANDATARIA, " + // 11
				"             mandataria.NOMINATIVO AS NOMINATIVO_MANDATARIA, " + // 12
				"             mandataria.HOME_TERRITORY AS HOME_TERRITORY_MANDATARIA, " + // 13
				"             mandataria.POSIZIONE_SIAE AS POSIZIONE_SIAE_MANDATARIA, " + // 14
				"             mandataria.CODICE_SIADA AS CODICE_SIADA_MANDATARIA, " + // 15
				"             mandataria.TENANT AS TENANT_MANDATARIA, " + // 16
				"             mandante.ID AS ID_MANDANTE, " + // 17
				"             mandante.CODICE AS CODICE_MANDANTE, " + // 18
				"             mandante.NOMINATIVO AS NOMINATIVO_MANDANTE, " + // 19
				"             mandante.HOME_TERRITORY AS HOME_TERRITORY_MANDANTE, " + // 20
				"             mandante.POSIZIONE_SIAE AS POSIZIONE_SIAE_MANDANTE, " + // 21
				"             mandante.CODICE_SIADA AS CODICE_SIADA_MANDANTE, " + // 22
				"             mandante.TENANT AS TENANT_MANDANTE, " + // 23
				"             dsp.IDDSP, " + // 24
				"             dsp.CODE, " + // 25
				"             dsp.NAME, " + // 26
				"             dsp.DESCRIPTION, " + // 27
				"             dsp.FTP_SOURCE_PATH, " + // 28
				"             u.ID_UTILIZATION_TYPE, " + // 29
				"             u.NAME, " + // 30
				"             u.DESCRIPTION, " + // 31
				"             o.ID_COMMERCIAL_OFFERS, " + // 32
				"             o.IDDSP, " + // 33
				"             o.ID_UTIILIZATION_TYPE, " + // 34
				"             o.OFFERING, " + // 35
				"             o.CCID_SERVICE_TYPE_CODE, " + // 36
				"             o.CCID_USE_TYPE_CODE, " + // 37
				"             o.CCID_APPLIED_TARIFF_CODE, " + // 38
				"             o.TRADING_BRAND, " + // 39
				"             c.ID_COUNTRY, " + // 40
				"             c.CODE, " + // 41
				"             c.ISO_CODE, " + // 42
				"             c.NAME " + // 43
				"      FROM MANDATO_CONFIGURAZIONE mc " + "        JOIN MANDATO m ON mc.ID_MANDATO = m.ID "
				+ "        JOIN SOCIETA_TUTELA mandataria ON m.ID_MANDATARIA = mandataria.ID "
				+ "        JOIN SOCIETA_TUTELA mandante ON m.ID_MANDANTE = mandante.ID "
				+ "        LEFT JOIN ANAG_DSP dsp ON mc.DSP=dsp.IDDSP "
				+ "        LEFT JOIN ANAG_UTILIZATION_TYPE u ON mc.TIPO_UTILIZZO=u.ID_UTILIZATION_TYPE "
				+ "        LEFT JOIN COMMERCIAL_OFFERS o ON mc.OFFERTA_COMMERCIALE=o.ID_COMMERCIAL_OFFERS "
				+ "        LEFT JOIN ANAG_COUNTRY c ON mc.TERRITORIO=c.ID_COUNTRY ";
		return sql;
	}

	private void fillMandato(ConfigurazioneMandatoDTO dto, Object[] data) {
		MandatoDTO m = new MandatoDTO();
		m.setId((Integer) data[1]);
		m.setDataInizioMandato((Date) data[8]);
		m.setDataFineMandato((Date) data[9]);
		fillMandante(m, data);
		fillMandataria(m, data);
		dto.setMandato(m);
	}

	private void fillMandataria(MandatoDTO dto, Object[] data) {
		SocietaTutelaDTO m = new SocietaTutelaDTO();
		m.setId((Integer) data[10]);
		m.setCodice((String) data[11]);
		m.setNominativo((String) data[12]);
		AnagCountry c = new AnagCountry();
		c.setIdCountry((String) data[13]);
		m.setHomeTerritory(c);
		m.setPosizioneSIAE((String) data[14]);
		m.setCodiceSIADA((String) data[15]);
		m.setTenant((Integer) data[16]);
		dto.setMandataria(m);
	}

	private void fillMandante(MandatoDTO dto, Object[] data) {
		SocietaTutelaDTO m = new SocietaTutelaDTO();
		m.setId((Integer) data[17]);
		m.setCodice((String) data[18]);
		m.setNominativo((String) data[19]);
		AnagCountry c = new AnagCountry();
		c.setIdCountry((String) data[20]);
		m.setHomeTerritory(c);
		m.setPosizioneSIAE((String) data[21]);
		m.setCodiceSIADA((String) data[22]);
		m.setTenant((Integer) data[23]);
		dto.setMandante(m);
	}

	private void fillDsp(ConfigurazioneMandatoDTO dto, Object[] data) {
		AnagDsp dsp = new AnagDsp();
		if ("*".equalsIgnoreCase((String) data[2])) {
			dsp.setIdDsp("*");
			dsp.setName("*");
		} else {
			dsp.setIdDsp((String) data[24]);
			dsp.setCode((String) data[25]);
			dsp.setName((String) data[26]);
			dsp.setDescription((String) data[27]);
			dsp.setFtpSourcePath((String) data[28]);
		}
		dto.setDsp(dsp);
	}

	private void fillUtilization(ConfigurazioneMandatoDTO dto, Object[] data) {
		UtilizationTypeDTO u = new UtilizationTypeDTO();
		if ("*".equalsIgnoreCase((String) data[3])) {
			u.setIdUtilizationType("*");
			u.setName("*");
			u.setDescription("*");
		} else {
			u.setIdUtilizationType((String) data[29]);
			u.setName((String) data[30]);
			u.setDescription((String) data[31]);
		}
		dto.setUtilization(u);
	}

	private void fillCommercialOffer(ConfigurazioneMandatoDTO dto, Object[] data) {
		CommercialOffersConfigDTO o = new CommercialOffersConfigDTO();
		if ("*".equalsIgnoreCase((String) data[4])) {
			o.setIdCommercialOffering(-1);
			o.setIdDSP("*");
			o.setIdUtilizationType("*");
			o.setOffering("*");
			o.setServiceType("*");
			o.setUseType("*");
			o.setAppliedTariff("*");
			o.setTradingBrand("*");
		} else {
			o.setIdCommercialOffering((Integer) data[32]);
			o.setIdDSP((String) data[33]);
			o.setIdUtilizationType((String) data[34]);
			o.setOffering((String) data[35]);
			o.setServiceType((String) data[36]);
			o.setUseType((String) data[37]);
			o.setAppliedTariff((String) data[38]);
			o.setTradingBrand((String) data[39]);
		}
		dto.setOffer(o);
	}

	private void fillCountry(ConfigurazioneMandatoDTO dto, Object[] data) {
		AnagCountry c = new AnagCountry();
		if ("*".equalsIgnoreCase((String) data[5])) {
			c.setIdCountry("*");
			c.setCode("*");
			c.setIsoCode("*");
			c.setName("*");
		} else {
			c.setIdCountry((String) data[40]);
			c.setCode((String) data[41]);
			c.setIsoCode((String) data[42]);
			c.setName((String) data[43]);
		}
		dto.setCountry(c);
	}

}
