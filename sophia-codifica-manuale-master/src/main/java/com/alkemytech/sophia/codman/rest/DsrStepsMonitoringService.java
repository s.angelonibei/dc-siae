package com.alkemytech.sophia.codman.rest;

import java.text.SimpleDateFormat;
import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.alkemytech.sophia.codman.dto.DsrMonitoringDTO;
import com.alkemytech.sophia.codman.utils.Periods;
import com.alkemytech.sophia.codman.utils.QueryUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.codman.dto.DsrProgressDTO;
import com.alkemytech.sophia.codman.entity.DsrStepsMonitoring;
import com.alkemytech.sophia.codman.entity.SqsDsrProgress;
import com.alkemytech.sophia.codman.entity.SqsHostname;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.alkemytech.sophia.common.tools.StringTools;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

@Singleton
@Path("dsr-steps-monitoring")
public class DsrStepsMonitoringService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Properties configuration;
	private final Provider<EntityManager> provider;
	private final Gson gson;

	@Inject
	protected DsrStepsMonitoringService(@Named("configuration") Properties configuration,
			@McmdbDataSource Provider<EntityManager> provider, Gson gson) {
		super();
		this.configuration = configuration;
		this.provider = provider;
		this.gson = gson;
	}

	@GET
	@Produces("application/json")
	public Response findAll() {
		final EntityManager entityManager = provider.get();
		final List<DsrStepsMonitoring> result = entityManager
				.createQuery("select x from DsrStepsMonitoring x", DsrStepsMonitoring.class)
				.setMaxResults(32768)
				.getResultList();
		return Response.ok(gson.toJson(result)).build();
	}

	@GET
	@Path("iddsrs")
	@Produces("application/json")
	public Response getIdDsrs() {
		final EntityManager entityManager = provider.get();
		final List<String> result = entityManager
				.createQuery("select distinct x.idDsr from DsrStepsMonitoring x order by x.idDsr", String.class)
				.getResultList();
		try {
			result.remove(null);
		} catch (NullPointerException e) { }
		return Response.ok(gson.toJson(result)).build();
	}

	@GET
	@Path("searchIdDsrs")
	@Produces("application/json")
	public Response searchIdDsrs(@QueryParam("idDsr") String idDsr) {
		final EntityManager entityManager = provider.get();
		final List<String> idDsrs = entityManager
//				.createQuery("select x.idDsr from DsrMetadata x where x.idDsr like :idDsr order by x.idDsr", String.class)
				.createQuery("select distinct x.idDsr from DsrStepsMonitoring x where x.idDsr like :idDsr order by x.idDsr", String.class)
				.setParameter("idDsr", "%" + idDsr + "%")
				.getResultList();
		try {
			idDsrs.remove(null);
		} catch (NullPointerException e) { }
		final List<Map<String, String>> result = new ArrayList<Map<String,String>>(idDsrs.size());
		for (String name : idDsrs) {
			final Map<String, String> idAsMap = new HashMap<String, String>(1);
			idAsMap.put("name", name);
			result.add(idAsMap);
		}
		return Response.ok(gson.toJson(result)).build();
	}

	@GET
	@Path("searchNew")
	@Produces("application/json")
	public Response search(@QueryParam("idDsr") String iddsr,
						   @QueryParam("sortType") String sortType,
						   @QueryParam("sortReverse") String sortReverse,
						   @DefaultValue("0") @QueryParam("first") int first,
						   @DefaultValue("50") @QueryParam("last") int last,
						   @QueryParam("dspList") List<String> dspList,
						   @QueryParam("countryList") List<String> countryList,
						   @QueryParam("utilizationList") List<String> utilizationList,
						   @QueryParam("offerList") List<String> offerList,
//		   @DefaultValue("-1") @QueryParam("backclaim") int backclaim,
						   @DefaultValue("0") @QueryParam("monthFrom") int monthFrom,
						   @DefaultValue("0") @QueryParam("yearFrom") int yearFrom,
						   @DefaultValue("0") @QueryParam("monthTo") int monthTo,
						   @DefaultValue("0") @QueryParam("yearTo") int yearTo,
						   @QueryParam("insertTime") String insertTime,
						   @QueryParam("dataInizio") String dataInizio){

		
		boolean checkPeriod = false;
		Map<Integer, List<Integer>> quarters = new HashMap<>();
		String inCondition = "";

		Integer defaultMonthFrom = 0;
		Integer defaultYearFrom = 2000;
		Date date = new Date();
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		Integer defaultMonthTo = calendar.get(Calendar.MONTH);
		Integer defaultYearTo = calendar.get(Calendar.YEAR);

		if (monthFrom > 0 || monthTo > 0) {
			checkPeriod = true;

			if (monthFrom > 0) {
				defaultMonthFrom = monthFrom;
				defaultYearFrom = yearFrom;
			}

			if (monthTo > 0) {
				defaultMonthTo = monthTo;
				defaultYearTo = yearTo;

			}
			quarters = Periods.extractCorrespondingQuarters(defaultMonthFrom, defaultYearFrom, defaultMonthTo, defaultYearTo);

			int y = 0;
			for (int key : quarters.keySet()) {
				if (y++ != 0) {
					inCondition += " or ";
				}
				inCondition += " (M1.PERIOD_TYPE='quarter' and M1.PERIOD in ("
						+ StringUtils.join(quarters.get(key), ',') + ") and M1.YEAR = " + key + " )";

			}
			if (!inCondition.isEmpty()) {
				inCondition = " ( " + inCondition + " )";
			}

		}
		Calendar calendarLastUpdate = null;
		try {
			if (!StringUtils.isEmpty(insertTime)) {

				calendarLastUpdate = Calendar.getInstance();
				final Calendar calendarTZ = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
				final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");
				dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
				calendarTZ.setTime(dateFormat.parse(insertTime));
				calendarTZ.set(Calendar.SECOND, 0);
				calendarTZ.set(Calendar.MILLISECOND, 0);
				calendarLastUpdate.setTimeInMillis(calendarTZ.getTimeInMillis());
				logger.debug("progress: where insertTime {}", calendarLastUpdate.getTime());

			}
		}catch (Exception e){e.printStackTrace();}

		Calendar calendardataInizio = null;
		try {
			if (!StringUtils.isEmpty(dataInizio)) {

				calendardataInizio = Calendar.getInstance();
				final Calendar calendarTZ = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
				final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");
				dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
				calendarTZ.setTime(dateFormat.parse(dataInizio));
				calendarTZ.set(Calendar.SECOND, 0);
				calendarTZ.set(Calendar.MILLISECOND, 0);
				calendardataInizio.setTimeInMillis(calendarTZ.getTimeInMillis());
				logger.debug("progress: where insertTime {}", calendardataInizio.getTime());

			}
		}catch (Exception e){e.printStackTrace();}


		final EntityManager entityManager = provider.get();
		try {
			String separator = " and ";
			
			final StringBuffer sqlSelect = new StringBuffer().append("select DSR_STEPS_MONITORING.* ");
			
			final StringBuffer sqlCount = new StringBuffer()
					.append(" select \n" +
							" count(*),\n" +
							"       count(EXTRACT_STATUS) EXTRACT_TOTAL,\n" +
							"       sum( CASE WHEN  EXTRACT_QUEUED is not null THEN 1 ELSE 0 END) EXTRACT_QUEUE,\n" +
							"       sum( CASE WHEN  EXTRACT_STARTED is not null THEN 1 ELSE 0 END) EXTRACT_STARTED,\n" +
							"       sum( CASE WHEN  EXTRACT_STATUS='OK' THEN 1 ELSE 0 END) EXTRACT_OK,\n" +
							"       sum( CASE WHEN  EXTRACT_STATUS='KO' THEN 1 ELSE 0 END) EXTRACT_KO,\n" +
							"       count(CLEAN_STATUS) CLEAN_TOTAL,\n" +
							"            sum( CASE WHEN  CLEAN_QUEUED is not null THEN 1 ELSE 0 END) CLEAN_QUEUE,\n" +
							"       sum( CASE WHEN  CLEAN_STARTED is not null THEN 1 ELSE 0 END) CLEAN_STARTED,\n" +
							"       sum( CASE WHEN  CLEAN_STATUS='OK' THEN 1 ELSE 0 END) CLEAN_OK,\n" +
							"       sum( CASE WHEN  CLEAN_STATUS='KO' THEN 1 ELSE 0 END) CLEAN_KO,\n" +
							"       count(IDENTIFY_STATUS) IDENTIFY_TOTAL,\n" +
							"       sum( CASE WHEN  IDENTIFY_QUEUED is not null THEN 1 ELSE 0 END) IDENTIFY_QUEUE,\n" +
							"       sum( CASE WHEN  IDENTIFY_STARTED is not null THEN 1 ELSE 0 END) IDENTIFY_STARTED,\n" +
							"       sum( CASE WHEN  IDENTIFY_STATUS='OK' THEN 1 ELSE 0 END) IDENTIFY_OK,\n" +
							"       sum( CASE WHEN  IDENTIFY_STATUS='KO' THEN 1 ELSE 0 END) IDENTIFY_KO,\n" +
							"       count(CLAIM_STATUS) CLAIM_TOTAL,\n" +
							"       sum( CASE WHEN  CLAIM_QUEUED is not null THEN 1 ELSE 0 END) CLAIM_QUEUE,\n" +
							"       sum( CASE WHEN  CLAIM_STARTED is not null THEN 1 ELSE 0 END) CLAIM_STARTED,\n" +
							"       sum( CASE WHEN  CLAIM_STATUS='OK' THEN 1 ELSE 0 END) CLAIM_OK,\n" +
							"       sum( CASE WHEN  CLAIM_STATUS='KO' THEN 1 ELSE 0 END) CLAIM_KO,\n" +
							"              count(UNILOAD_STATUS) UNILOAD_TOTAL,\n" +
							"       sum( CASE WHEN  UNILOAD_QUEUED is not null THEN 1 ELSE 0 END) UNILOAD_QUEUE,\n" +
							"       sum( CASE WHEN  UNILOAD_STARTED is not null THEN 1 ELSE 0 END) UNILOAD_STARTED,\n" +
							"       sum( CASE WHEN  UNILOAD_STATUS='OK' THEN 1 ELSE 0 END) UNILOAD_OK,\n" +
							"       sum( CASE WHEN  UNILOAD_STATUS='KO' THEN 1 ELSE 0 END) UNILOAD_KO ");
			
			
			final StringBuffer sql = new StringBuffer()
					.append("  " +
							" from DSR_STEPS_MONITORING" +
							" left join DSR_METADATA M1 on DSR_STEPS_MONITORING.IDDSR = M1.IDDSR " +
							" left join COMMERCIAL_OFFERS on M1.SERVICE_CODE = COMMERCIAL_OFFERS.ID_COMMERCIAL_OFFERS " + 
							" left join ANAG_DSP on COMMERCIAL_OFFERS.IDDSP = ANAG_DSP.IDDSP" +
							" where  1=1 " 
							+ (!dspList.isEmpty() ? "and COMMERCIAL_OFFERS.IDDSP in ( " + QueryUtils.getValuesCollection(dspList) + " )" : "")
							+ (!countryList.isEmpty() ? "and M1.COUNTRY in ( " + QueryUtils.getValuesCollection(countryList) + " )" : "")
							+ (!utilizationList.isEmpty() ? "and COMMERCIAL_OFFERS.ID_UTIILIZATION_TYPE in ( " + QueryUtils.getValuesCollection(utilizationList) + " )" : "")
							+ (!offerList.isEmpty() ? "and COMMERCIAL_OFFERS.OFFERING in ( " + QueryUtils.getValuesCollection(offerList) + " )" : "")
							+ (!inCondition.isEmpty() ? "and (" + inCondition + " or " : "")
							+ (checkPeriod && inCondition.isEmpty() ? " and " : "")
							+ (checkPeriod ? " (PERIOD_TYPE='month' and  STR_TO_DATE(CONCAT(M1.YEAR,'-', M1.PERIOD , '-01'), '%Y-%m-%d') >= '" + defaultYearFrom + "-" + defaultMonthFrom + "-01' and STR_TO_DATE(CONCAT(M1.YEAR,'-', M1.PERIOD , '-01'), '%Y-%m-%d') <= '" + defaultYearTo + "-" + defaultMonthTo + "-01' ) " : "")
							+ (checkPeriod && !quarters.isEmpty() ? ") " : " "));

			if(calendarLastUpdate != null){
				sql.append(separator).append("LAST_UPDATE > ?");
			}
			if(calendardataInizio != null){
				sql.append(separator).append("EXTRACT_QUEUED > ?");
			}
			
			
			if (!StringUtils.isEmpty(iddsr)) {
				sql.append(separator).append("IDDSR like ?");
				separator = " and ";
			}
			if (StringTools.isNullOrEmpty(sortType)) {
				sortType = "lastUpdate";
			}
			final String ascOrDesc = "true".equalsIgnoreCase(sortReverse) ? "DESC" : "ASC";
			if ("lastUpdate".equalsIgnoreCase(sortType)) {
				sql.append(" order by LAST_UPDATE ")
						.append(ascOrDesc);
			} else if ("idDsr".equalsIgnoreCase(sortType)) {
				sql.append(" order by IDDSR ")
						.append(ascOrDesc);

			} else if ("extractQueued".equalsIgnoreCase(sortType)) {
				sql.append(" order by EXTRACT_QUEUED ")
						.append(ascOrDesc);
			} else if ("extractStarted".equalsIgnoreCase(sortType)) {
				sql.append(" order by EXTRACT_STARTED ")
						.append(ascOrDesc);
			} else if ("extractFinished".equalsIgnoreCase(sortType)) {
				sql.append(" order by EXTRACT_FINISHED ")
						.append(ascOrDesc);

			} else if ("cleanQueued".equalsIgnoreCase(sortType)) {
				sql.append(" order by CLEAN_QUEUED ")
						.append(ascOrDesc);
			} else if ("cleanStarted".equalsIgnoreCase(sortType)) {
				sql.append(" order by CLEAN_STARTED ")
						.append(ascOrDesc);
			} else if ("cleanFinished".equalsIgnoreCase(sortType)) {
				sql.append(" order by CLEAN_FINISHED ")
						.append(ascOrDesc);

			} else if ("priceQueued".equalsIgnoreCase(sortType)) {
				sql.append(" order by PRICE_QUEUED ")
						.append(ascOrDesc);
			} else if ("priceStarted".equalsIgnoreCase(sortType)) {
				sql.append(" order by PRICE_STARTED ")
						.append(ascOrDesc);
			} else if ("priceFinished".equalsIgnoreCase(sortType)) {
				sql.append(" order by PRICE_FINISHED ")
						.append(ascOrDesc);

			} else if ("hypercubeQueued".equalsIgnoreCase(sortType)) {
				sql.append(" order by HYPERCUBE_QUEUED ")
						.append(ascOrDesc);
			} else if ("hypercubeStarted".equalsIgnoreCase(sortType)) {
				sql.append(" order by HYPERCUBE_STARTED ")
						.append(ascOrDesc);
			} else if ("hypercubeFinished".equalsIgnoreCase(sortType)) {
				sql.append(" order by HYPERCUBE_FINISHED ")
						.append(ascOrDesc);

			} else if ("identifyQueued".equalsIgnoreCase(sortType)) {
				sql.append(" order by IDENTIFY_QUEUED ")
						.append(ascOrDesc);
			} else if ("identifyStarted".equalsIgnoreCase(sortType)) {
				sql.append(" order by DENTIFY_STARTED ")
						.append(ascOrDesc);
			} else if ("identifyFinished".equalsIgnoreCase(sortType)) {
				sql.append(" order by DENTIFY_FINISHED ")
						.append(ascOrDesc);

			} else if ("uniloadQueued".equalsIgnoreCase(sortType)) {
				sql.append(" order by UNILOAD_QUEUED ")
						.append(ascOrDesc);
			} else if ("uniloadStarted".equalsIgnoreCase(sortType)) {
				sql.append(" order by UNILOAD_STARTED ")
						.append(ascOrDesc);
			} else if ("uniloadFinished".equalsIgnoreCase(sortType)) {
				sql.append(" order by UNILOAD_FINISHED ")
						.append(ascOrDesc);

			} else if ("claimQueued".equalsIgnoreCase(sortType)) {
				sql.append(" order by CLAIM_QUEUED ")
						.append(ascOrDesc);
			} else if ("claimStarted".equalsIgnoreCase(sortType)) {
				sql.append(" order by CLAIM_STARTED ")
						.append(ascOrDesc);
			} else if ("claimFinished".equalsIgnoreCase(sortType)) {
				sql.append(" order by CLAIM_FINISHED ")
						.append(ascOrDesc);

			} else if ("prelearnQueued".equalsIgnoreCase(sortType)) {
				sql.append(" order by PRELEARN_QUEUED ")
						.append(ascOrDesc);
			} else if ("prelearnStarted".equalsIgnoreCase(sortType)) {
				sql.append(" order by PRELEARN_STARTED ")
						.append(ascOrDesc);
			} else if ("prelearnFinished".equalsIgnoreCase(sortType)) {
				sql.append(" order by PRELEARN_FINISHED ")
						.append(ascOrDesc);

			} else if ("learnQueued".equalsIgnoreCase(sortType)) {
				sql.append(" order by LEARN_QUEUED ")
						.append(ascOrDesc);
			} else if ("learnStarted".equalsIgnoreCase(sortType)) {
				sql.append(" order by LEARN_STARTED ")
						.append(ascOrDesc);
			} else if ("learnFinished".equalsIgnoreCase(sortType)) {
				sql.append(" order by LEARN_FINISHED ")
						.append(ascOrDesc);

			}
			
			int i = 1;
			final Query query = (Query) entityManager
					.createNativeQuery(sqlSelect.toString() + sql.toString() + " limit ?, ?", DsrStepsMonitoring.class);

			if(calendarLastUpdate != null){
				query.setParameter(i ++, calendarLastUpdate);
			}

			if(calendardataInizio != null){
				query.setParameter(i ++, calendardataInizio);
			}
			
			if (!StringUtils.isEmpty(iddsr)) {
				query.setParameter(i ++, "%" + iddsr + "%");
			}
			query.setParameter(i ++, first);
			query.setParameter(i ++, 1 + last - first);

			final List<DsrStepsMonitoring> results =
					(List<DsrStepsMonitoring>) query.getResultList();
			final PagedResult result = new PagedResult();
			if (null != results && !results.isEmpty()) {
				final int maxrows = last - first;
				final boolean hasNext = results.size() > maxrows;
				result.setRows(!hasNext ? results : results.subList(0, maxrows))
						.setMaxrows(maxrows)
						.setFirst(first)
						.setLast(hasNext ? last : first + results.size())
						.setHasNext(hasNext)
						.setHasPrev(first > 0);
			} else {
				result.setMaxrows(0)
						.setFirst(0)
						.setLast(0)
						.setHasNext(false)
						.setHasPrev(false);
			}

			final Query query2 = (Query) entityManager
					.createNativeQuery(sqlCount.toString() + sql.toString());

			i = 1;

			if(calendarLastUpdate != null){
				query2.setParameter(i ++, calendarLastUpdate);
			}
			if(calendardataInizio != null){
				query2.setParameter(i ++, calendardataInizio);
			}
			
			if (!StringUtils.isEmpty(iddsr)) {
				query2.setParameter(i ++, "%" + iddsr + "%");
			}
//			query2.setParameter(i ++, first);
//			query2.setParameter(i ++, 1 + last - first);
			
			Object[] resultCount = (Object[])query2.getSingleResult();
			
			Map<String,String> extra = new HashMap();
			
			int j = 0;

			extra.put("TOTAL",resultCount[j]!= null ? resultCount[j++]+"" : "0");
			extra.put("EXTRACT_TOTAL",resultCount[j]!= null ? resultCount[j++]+"" : "0");
			extra.put("EXTRACT_QUEUED",resultCount[j]!= null ? resultCount[j++]+"" : "0");
			extra.put("EXTRACT_STARTED",resultCount[j]!= null ? resultCount[j++]+"" : "0");
			extra.put("EXTRACT_OK",resultCount[j]!= null ? resultCount[j++]+"" : "0");
			extra.put("EXTRACT_KO",resultCount[j]!= null ? resultCount[j++]+"" : "0");
			extra.put("CLEAN_TOTAL",resultCount[j]!= null ? resultCount[j++]+"" : "0");
			extra.put("CLEAN_QUEUED",resultCount[j]!= null ? resultCount[j++]+"" : "0");
			extra.put("CLEAN_STARTED",resultCount[j]!= null ? resultCount[j++]+"" : "0");
			extra.put("CLEAN_OK",resultCount[j]!= null ? resultCount[j++]+"" : "0");
			extra.put("CLEAN_KO",resultCount[j]!= null ? resultCount[j++]+"" : "0");
			extra.put("IDENTIFY_TOTAL",resultCount[j]!= null ? resultCount[j++]+"" : "0");
			extra.put("IDENTIFY_QUEUED",resultCount[j]!= null ? resultCount[j++]+"" : "0");
			extra.put("IDENTIFY_STARTED",resultCount[j]!= null ? resultCount[j++]+"" : "0");
			extra.put("IDENTIFY_OK",resultCount[j]!= null ? resultCount[j++]+"" : "0");
			extra.put("IDENTIFY_KO",resultCount[j]!= null ? resultCount[j++]+"" : "0");
			extra.put("CLAIM_TOTAL",resultCount[j]!= null ? resultCount[j++]+"" : "0");
			extra.put("CLAIM_QUEUED",resultCount[j]!= null ? resultCount[j++]+"" : "0");
			extra.put("CLAIM_STARTED",resultCount[j]!= null ? resultCount[j++]+"" : "0");
			extra.put("CLAIM_OK",resultCount[j]!= null ? resultCount[j++]+"" : "0");
			extra.put("CLAIM_KO",resultCount[j]!= null ? resultCount[j++]+"" : "0");
			extra.put("UNILOAD_TOTAL",resultCount[j]!= null ? resultCount[j++]+"" : "0");
			extra.put("UNILOAD_QUEUED",resultCount[j]!= null ? resultCount[j++]+"" : "0");
			extra.put("UNILOAD_STARTED",resultCount[j]!= null ? resultCount[j++]+"" : "0");
			extra.put("UNILOAD_OK",resultCount[j]!= null ? resultCount[j++]+"" : "0");
			extra.put("UNILOAD_KO",resultCount[j]!= null ? resultCount[j++]+"" : "0");
			
		
			DsrMonitoringDTO resultWithExtra = new DsrMonitoringDTO();
			resultWithExtra.setData(result);
			resultWithExtra.setExtra(extra);
			return Response.ok(gson.toJson(resultWithExtra)).build();
		} catch (Exception e) {
			logger.error("search", e);
		}
		return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR).build();
	}

	@GET
	@Path("search")
	@Produces("application/json")
	public Response search(@QueryParam("idDsr") String iddsr,
			@QueryParam("sortType") String sortType,
			@QueryParam("sortReverse") String sortReverse,
			@DefaultValue("0") @QueryParam("first") int first,
			@DefaultValue("50") @QueryParam("last") int last) {
		final EntityManager entityManager = provider.get();
		try {
			String separator = " where ";
			final StringBuffer sql = new StringBuffer()
				.append("select * from DSR_STEPS_MONITORING");
			if (!StringUtils.isEmpty(iddsr)) {
				sql.append(separator).append("IDDSR like ?");
				separator = " and ";
			}
			if (StringTools.isNullOrEmpty(sortType)) {
				sortType = "lastUpdate";
			}
			final String ascOrDesc = "true".equalsIgnoreCase(sortReverse) ? "DESC" : "ASC";
			if ("lastUpdate".equalsIgnoreCase(sortType)) {
				sql.append(" order by LAST_UPDATE ")
					.append(ascOrDesc);
			} else if ("idDsr".equalsIgnoreCase(sortType)) {
				sql.append(" order by IDDSR ")
					.append(ascOrDesc);
				
			} else if ("extractQueued".equalsIgnoreCase(sortType)) {
				sql.append(" order by EXTRACT_QUEUED ")
					.append(ascOrDesc);
			} else if ("extractStarted".equalsIgnoreCase(sortType)) {
				sql.append(" order by EXTRACT_STARTED ")
					.append(ascOrDesc);
			} else if ("extractFinished".equalsIgnoreCase(sortType)) {
				sql.append(" order by EXTRACT_FINISHED ")
					.append(ascOrDesc);

			} else if ("cleanQueued".equalsIgnoreCase(sortType)) {
				sql.append(" order by CLEAN_QUEUED ")
					.append(ascOrDesc);
			} else if ("cleanStarted".equalsIgnoreCase(sortType)) {
				sql.append(" order by CLEAN_STARTED ")
					.append(ascOrDesc);
			} else if ("cleanFinished".equalsIgnoreCase(sortType)) {
				sql.append(" order by CLEAN_FINISHED ")
					.append(ascOrDesc);

			} else if ("priceQueued".equalsIgnoreCase(sortType)) {
				sql.append(" order by PRICE_QUEUED ")
					.append(ascOrDesc);
			} else if ("priceStarted".equalsIgnoreCase(sortType)) {
				sql.append(" order by PRICE_STARTED ")
					.append(ascOrDesc);
			} else if ("priceFinished".equalsIgnoreCase(sortType)) {
				sql.append(" order by PRICE_FINISHED ")
					.append(ascOrDesc);

			} else if ("hypercubeQueued".equalsIgnoreCase(sortType)) {
				sql.append(" order by HYPERCUBE_QUEUED ")
					.append(ascOrDesc);
			} else if ("hypercubeStarted".equalsIgnoreCase(sortType)) {
				sql.append(" order by HYPERCUBE_STARTED ")
					.append(ascOrDesc);
			} else if ("hypercubeFinished".equalsIgnoreCase(sortType)) {
				sql.append(" order by HYPERCUBE_FINISHED ")
					.append(ascOrDesc);

			} else if ("identifyQueued".equalsIgnoreCase(sortType)) {
				sql.append(" order by IDENTIFY_QUEUED ")
					.append(ascOrDesc);
			} else if ("identifyStarted".equalsIgnoreCase(sortType)) {
				sql.append(" order by DENTIFY_STARTED ")
					.append(ascOrDesc);
			} else if ("identifyFinished".equalsIgnoreCase(sortType)) {
				sql.append(" order by DENTIFY_FINISHED ")
					.append(ascOrDesc);

			} else if ("uniloadQueued".equalsIgnoreCase(sortType)) {
				sql.append(" order by UNILOAD_QUEUED ")
					.append(ascOrDesc);
			} else if ("uniloadStarted".equalsIgnoreCase(sortType)) {
				sql.append(" order by UNILOAD_STARTED ")
					.append(ascOrDesc);
			} else if ("uniloadFinished".equalsIgnoreCase(sortType)) {
				sql.append(" order by UNILOAD_FINISHED ")
					.append(ascOrDesc);

			} else if ("claimQueued".equalsIgnoreCase(sortType)) {
				sql.append(" order by CLAIM_QUEUED ")
					.append(ascOrDesc);
			} else if ("claimStarted".equalsIgnoreCase(sortType)) {
				sql.append(" order by CLAIM_STARTED ")
					.append(ascOrDesc);
			} else if ("claimFinished".equalsIgnoreCase(sortType)) {
				sql.append(" order by CLAIM_FINISHED ")
					.append(ascOrDesc);
				
			} else if ("prelearnQueued".equalsIgnoreCase(sortType)) {
				sql.append(" order by PRELEARN_QUEUED ")
					.append(ascOrDesc);
			} else if ("prelearnStarted".equalsIgnoreCase(sortType)) {
				sql.append(" order by PRELEARN_STARTED ")
					.append(ascOrDesc);
			} else if ("prelearnFinished".equalsIgnoreCase(sortType)) {
				sql.append(" order by PRELEARN_FINISHED ")
					.append(ascOrDesc);
				
			} else if ("learnQueued".equalsIgnoreCase(sortType)) {
				sql.append(" order by LEARN_QUEUED ")
					.append(ascOrDesc);
			} else if ("learnStarted".equalsIgnoreCase(sortType)) {
				sql.append(" order by LEARN_STARTED ")
					.append(ascOrDesc);
			} else if ("learnFinished".equalsIgnoreCase(sortType)) {
				sql.append(" order by LEARN_FINISHED ")
					.append(ascOrDesc);

			}
			sql.append(" limit ?, ?");
			int i = 1;
			final Query query = (Query) entityManager
				.createNativeQuery(sql.toString(), DsrStepsMonitoring.class);

			if (!StringUtils.isEmpty(iddsr)) {
				query.setParameter(i ++, "%" + iddsr + "%");
			}
			query.setParameter(i ++, first);
			query.setParameter(i ++, 1 + last - first);
			@SuppressWarnings("unchecked")
			final List<DsrStepsMonitoring> results =
				(List<DsrStepsMonitoring>) query.getResultList();
			final PagedResult result = new PagedResult();
			if (null != results && !results.isEmpty()) {
				final int maxrows = last - first;
				final boolean hasNext = results.size() > maxrows;
				result.setRows(!hasNext ? results : results.subList(0, maxrows))
					.setMaxrows(maxrows)
					.setFirst(first)
					.setLast(hasNext ? last : first + results.size())
					.setHasNext(hasNext)
					.setHasPrev(first > 0);
			} else {
				result.setMaxrows(0)
					.setFirst(0)
					.setLast(0)
					.setHasNext(false)
					.setHasPrev(false);
			}
			return Response.ok(gson.toJson(result)).build();
		} catch (Exception e) {
			logger.error("search", e);
		}
		return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR).build();
	}

	@GET
	@Path("progress")
	@Produces("application/json")
	public Response progress(@QueryParam("idDsr") String idDsr,
			@QueryParam("insertTime") String insertTime,
			@DefaultValue("UTC") @QueryParam("timeZone") String timeZone,
			@DefaultValue("true") @QueryParam("sortReverse") String sortReverse,
			@DefaultValue("0") @QueryParam("first") int first,
			@DefaultValue("50") @QueryParam("last") int last) {
		final EntityManager entityManager = provider.get();
		try {			
			final StringBuffer sql = new StringBuffer()
				.append("select x from SqsDsrProgress x")
				.append(" where x.insertTime >= :insertTime");
			if (!StringTools.isNullOrEmpty(idDsr)) {
				sql.append(" and x.idDsr = :idDsr");
			}
			final Query query = entityManager
				.createQuery(sql.toString(), SqsDsrProgress.class);
			logger.debug("progress: insertTime {}", insertTime);
			if (!StringUtils.isEmpty(insertTime)) {
				final Calendar calendar = Calendar.getInstance();
				final Calendar calendarTZ = Calendar.getInstance(TimeZone.getTimeZone(timeZone));
				final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");
				dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
				calendarTZ.setTime(dateFormat.parse(insertTime));
				calendarTZ.set(Calendar.SECOND, 0);
				calendarTZ.set(Calendar.MILLISECOND, 0);
				calendar.setTimeInMillis(calendarTZ.getTimeInMillis());
				logger.debug("progress: where insertTime {}", calendar.getTime());
				query.setParameter("insertTime", calendar.getTime());
			} else {
				final Calendar calendar = Calendar.getInstance();
				final Calendar calendarTZ = Calendar.getInstance(TimeZone.getTimeZone(timeZone));
				final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");
				dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
				calendarTZ.add(Calendar.DATE, Integer
						.parseInt(configuration.getProperty("rs.dsrStepsMonitoring.defaultIntervalDays", "-1")));
				calendarTZ.set(Calendar.SECOND, 0);
				calendarTZ.set(Calendar.MILLISECOND, 0);
				calendar.setTimeInMillis(calendarTZ.getTimeInMillis());
				logger.debug("progress: where insertTime {}", calendar.getTime());
				query.setParameter("insertTime", calendar.getTime());
				insertTime = dateFormat.format(calendarTZ.getTime());
			}
			if (!StringTools.isNullOrEmpty(idDsr)) {
				query.setParameter("idDsr", idDsr);
			}
			@SuppressWarnings("unchecked")
			final List<SqsDsrProgress> sqsDsrProgresses = query.getResultList();
//			logger.debug("progress: sqsDsrProgresses {}", sqsDsrProgresses);
			// build aggregated objects map
			final Map<String, DsrProgressDTO> dsrProgressMap = new HashMap<String, DsrProgressDTO>();
			if (null != sqsDsrProgresses) for (SqsDsrProgress sqsDsrProgress : sqsDsrProgresses) {
				DsrProgressDTO dsrProgress = dsrProgressMap.get(sqsDsrProgress.getIdDsr());
				if (null == dsrProgress) {
					dsrProgress = new DsrProgressDTO();
					dsrProgress.setIdDsr(sqsDsrProgress.getIdDsr());
					dsrProgressMap.put(sqsDsrProgress.getIdDsr(), dsrProgress);
				}
				dsrProgress.updateWith(sqsDsrProgress);
			}
//			logger.debug("progress: dsrProgressMap {}", dsrProgressMap);
			// build ordered list
			List<DsrProgressDTO> dsrProgressList = new ArrayList<DsrProgressDTO>(dsrProgressMap.values());
			// remove items with missing {env}_to_process_extraction_and_validation time
			Iterator<DsrProgressDTO> iterator = dsrProgressList.iterator();
			while (iterator.hasNext()) {
				if (null == iterator.next().getExtractQueued()) {
					iterator.remove();
				}
			}
			// sort list
			if ("true".equalsIgnoreCase(sortReverse)) {
				Collections.sort(dsrProgressList, new Comparator<DsrProgressDTO>() {
					@Override
					public int compare(DsrProgressDTO o1, DsrProgressDTO o2) {
						return - o1.getExtractQueued().compareTo(o2.getExtractQueued());
					}			
				});
			} else {
				Collections.sort(dsrProgressList, new Comparator<DsrProgressDTO>() {
					@Override
					public int compare(DsrProgressDTO o1, DsrProgressDTO o2) {
						return o1.getExtractQueued().compareTo(o2.getExtractQueued());
					}			
				});
			}
//			logger.debug("progress: dsrProgressList {}", dsrProgressList);
			// overall stats
			int queuedCount = 0;
			int startedCount = 0;
			int completedCount = 0;
			int failedCount = 0;
			// queues stats
			QueueStats extract = new QueueStats();
			QueueStats clean = new QueueStats();
			QueueStats price = new QueueStats();
			QueueStats identify = new QueueStats();
			QueueStats uniload = new QueueStats();
			QueueStats hypercube = new QueueStats();
			QueueStats prelearn = new QueueStats();
			QueueStats learn = new QueueStats();
			QueueStats claim = new QueueStats();
			// loop on items
			for (DsrProgressDTO dsrProgress : dsrProgressList) {
				if (null != dsrProgress.getExtractQueued()) queuedCount ++;
				if (null != dsrProgress.getExtractStarted()) startedCount ++;
				if (dsrProgress.isCompleted()) completedCount ++;
				if (dsrProgress.hasFailures()) failedCount ++;
				
				if (null != dsrProgress.getExtractQueued()) extract.queued ++;
				if (null != dsrProgress.getCleanQueued()) clean.queued ++;
				if (null != dsrProgress.getPriceQueued()) price.queued ++;
				if (null != dsrProgress.getIdentifyQueued()) identify.queued ++;
				if (null != dsrProgress.getUniloadQueued()) uniload.queued ++;
				if (null != dsrProgress.getHypercubeQueued()) hypercube.queued ++;
				if (null != dsrProgress.getPrelearnQueued()) prelearn.queued ++;
				if (null != dsrProgress.getLearnQueued()) learn.queued ++;
				if (null != dsrProgress.getClaimQueued()) claim.queued ++;

				if (null != dsrProgress.getExtractStarted()) extract.started ++;
				if (null != dsrProgress.getCleanStarted()) clean.started ++;
				if (null != dsrProgress.getPriceStarted()) price.started ++;
				if (null != dsrProgress.getIdentifyStarted()) identify.started ++;
				if (null != dsrProgress.getUniloadStarted()) uniload.started ++;
				if (null != dsrProgress.getHypercubeStarted()) hypercube.started ++;
				if (null != dsrProgress.getPrelearnStarted()) prelearn.started ++;
				if (null != dsrProgress.getLearnStarted()) learn.started ++;
				if (null != dsrProgress.getClaimStarted()) claim.started ++;

				if (null != dsrProgress.getExtractCompleted()) extract.completed ++;
				if (null != dsrProgress.getCleanCompleted()) clean.completed ++;
				if (null != dsrProgress.getPriceCompleted()) price.completed ++;
				if (null != dsrProgress.getIdentifyCompleted()) identify.completed ++;
				if (null != dsrProgress.getUniloadCompleted()) uniload.completed ++;
				if (null != dsrProgress.getHypercubeCompleted()) hypercube.completed ++;
				if (null != dsrProgress.getPrelearnCompleted()) prelearn.completed ++;
				if (null != dsrProgress.getLearnCompleted()) learn.completed ++;
				if (null != dsrProgress.getClaimCompleted()) claim.completed ++;
				
				if (null != dsrProgress.getExtractFailed()) extract.failed ++;
				if (null != dsrProgress.getCleanFailed()) clean.failed ++;
				if (null != dsrProgress.getPriceFailed()) price.failed ++;
				if (null != dsrProgress.getIdentifyFailed()) identify.failed ++;
				if (null != dsrProgress.getUniloadFailed()) uniload.failed ++;
				if (null != dsrProgress.getHypercubeFailed()) hypercube.failed ++;
				if (null != dsrProgress.getPrelearnFailed()) prelearn.failed ++;
				if (null != dsrProgress.getLearnFailed()) learn.failed ++;
				if (null != dsrProgress.getClaimFailed()) claim.failed ++;
			}
			final int totalCount = dsrProgressList.size();
			final int waitingCount = totalCount - startedCount;
			final int runningCount = startedCount - completedCount;
//			logger.debug("progress: sorted dsrProgressList {}", dsrProgressList);
			// skip first items
			if (first >= dsrProgressList.size()) {
				dsrProgressList.clear();
			} else {
				dsrProgressList = dsrProgressList.subList(first, dsrProgressList.size());
			}
			// result
			Map<String, Object> extra = new HashMap<String, Object>();
			extra.put("referenceDate", insertTime);
			extra.put("totalCount", totalCount);
			extra.put("waitingCount", waitingCount);
			extra.put("runningCount", runningCount);			
			extra.put("queuedCount", queuedCount);
			extra.put("startedCount", startedCount);
			extra.put("completedCount", completedCount);
			extra.put("failedCount", failedCount);
			extra.put("extract", extract);
			extra.put("clean", clean);
			extra.put("price", price);
			extra.put("identify", identify);
			extra.put("uniload", uniload);
			extra.put("hypercube", hypercube);
			extra.put("prelearn", prelearn);
			extra.put("learn", learn);
			extra.put("claim", claim);
			final PagedResult result = new PagedResult();
			if (null != dsrProgressList && !dsrProgressList.isEmpty()) {
				final int maxrows = last - first;
				final boolean hasNext = dsrProgressList.size() > maxrows;
				result.setRows(!hasNext ? dsrProgressList : dsrProgressList.subList(0, maxrows))
					.setMaxrows(maxrows)
					.setFirst(first)
					.setLast(hasNext ? last : first + dsrProgressList.size())
					.setHasNext(hasNext)
					.setHasPrev(first > 0)
					.setExtra(extra);
			} else {
				result.setMaxrows(0)
					.setFirst(0)
					.setLast(0)
					.setHasNext(false)
					.setHasPrev(false)
					.setExtra(extra);
			}
			return Response.ok(gson.toJson(result)).build();
		} catch (Exception e) {
			logger.error("progress", e);
		}			
		return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR).build();
	}
	
	class QueueStats {
		public int queued;
		public int started;
		public int completed;
		public int failed;
		QueueStats() {
			queued = started = completed = failed = 0;
		}
	}
	
//	private boolean happensBefore(Date d1, Date d2) {
//		if (null == d1 || null == d2) {
//			return false;
//		}
//		return d1.compareTo(d2) < 0;
//	}

	@GET
	@Path("hostnames")
	@Produces("application/json")
	public Response hostnames(@DefaultValue("true") @QueryParam("activeOnly") String activeOnly) {
		final EntityManager entityManager = provider.get();
		try {
			final StringBuffer sql = new StringBuffer()
				.append("select x from SqsHostname x");
			if ("true".equalsIgnoreCase(activeOnly)) {
				sql.append(", ActiveClusters y")
				.append(" where y.idCluster = x.hostname");
			}
			final List<SqsHostname> results = entityManager
					.createQuery(sql.toString(), SqsHostname.class)
					.getResultList();
			return Response.ok(gson.toJson(results)).build();
		} catch (Exception e) {
			logger.error("hostnames", e);
		}
		return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR).build();
	}
	
	@GET
	@Path("invoiced/{idDsr}")
	@Produces("application/json")
	public Response invoiced(@PathParam("idDsr") String idDsr) {
		final EntityManager entityManager = provider.get();
		try {
			final boolean invoiced = MetadataService.isInvoiced(entityManager, idDsr);
			logger.debug("invoiced: invoiced {}", invoiced);
			return Response.ok(new StringBuilder()
				.append("{ ")
				.append("\"invoiced\": ").append(invoiced)
				.append(" }")
				.toString()).build();
		} catch (Exception e) {
			logger.error("invoiced", e);
		}
		return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR).build();
	}
	
}
