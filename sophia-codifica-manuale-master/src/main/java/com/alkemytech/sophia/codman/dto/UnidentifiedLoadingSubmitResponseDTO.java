package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UnidentifiedLoadingSubmitResponseDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String status;
	
	private String message;

	private List<UnidentifiedLoadingSearchResultDTO> dsrs;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<UnidentifiedLoadingSearchResultDTO> getDsrs() {
		return dsrs;
	}

	public void setDsrs(List<UnidentifiedLoadingSearchResultDTO> dsrs) {
		this.dsrs = dsrs;
	}

}
