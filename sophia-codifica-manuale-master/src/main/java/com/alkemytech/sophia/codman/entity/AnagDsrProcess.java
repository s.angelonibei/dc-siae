package com.alkemytech.sophia.codman.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.codman.persistence.AbstractEntity;

@XmlRootElement
@Entity(name="AnagDsrProcess")
@Table(name="ANAG_DSR_PROCESS")
@NamedQueries({@NamedQuery(name="AnagDsrProcess.GetAll", query="SELECT adp FROM AnagDsrProcess adp")})
@SuppressWarnings("serial")
public class AnagDsrProcess extends AbstractEntity<String> {

	@Id
	@Column(name="ID_DSR_PROCESS", nullable=false)
	private String idDsrProcess;
	
	@Column(name="NAME", nullable=false)
	private String name;
	
	@Column(name="DESCRIPTION", nullable=false)
	private String description;
	
	@Column(name="CONFIGURATION", columnDefinition="json")
	private String configuration;
	
	@Override
	public String getId() {
		return getIdDsrProcess();
	}

	public String getIdDsrProcess() {
		return idDsrProcess;
	}

	public void setIdDsrProcess(String idDsrProcess) {
		this.idDsrProcess = idDsrProcess;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getConfiguration() {
		return configuration;
	}

	public void setConfiguration(String configuration) {
		this.configuration = configuration;
	}


}
