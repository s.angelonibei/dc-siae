package com.alkemytech.sophia.codman.dto.performing;

import java.io.Serializable;

public class LivelloSomiglianza implements Serializable {
    private Integer id;
    private Byte inferiore;
    private Byte superiore;
    private String header;

    public LivelloSomiglianza(Integer id, Byte inferiore, Byte superiore) {
        this.id = id;
        this.inferiore = inferiore;
        this.superiore = superiore;
        this.header = toString();
    }

    public LivelloSomiglianza() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Byte getInferiore() {
        return inferiore;
    }

    public void setInferiore(Byte inferiore) {
        this.inferiore = inferiore;
    }

    public Byte getSuperiore() {
        return superiore;
    }

    public void setSuperiore(Byte superiore) {
        this.superiore = superiore;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getHeader() {
        return toString();
    }

    @Override
    public String toString() {
        return  inferiore == null ? "LS \u2264 " + superiore + "%" :
        	    superiore == null ? inferiore + "% < LS" :
                inferiore + "% < LS \u2264 " + superiore + "%";
    }
}
