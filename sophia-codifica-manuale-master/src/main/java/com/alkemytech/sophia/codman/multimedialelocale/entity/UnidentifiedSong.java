package com.alkemytech.sophia.codman.multimedialelocale.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Objects;

@XmlRootElement
@Entity
@Table(name = "unidentified_song")
@NamedNativeQuery(name = "UnidentifiedSong.findAllNative",
        query = "select hash_id, title, artists, roles, priority, insert_time, last_auto_time, last_manual_time, siada_title, siada_artists, username",
        resultClass = UnidentifiedSong.class
)
@NamedQueries({
        @NamedQuery(name = "UnidentifiedSong.findByIdAndLastManualTimeLessThan", query = "select us FROM UnidentifiedSong us where us.hashId = :hashId and (us.lastManualTime < :lastManualTime or (us.username = :username or us.username is null))"),
        @NamedQuery(name = "UnidentifiedSong.updateByUsernameAndHashId", query = "update UnidentifiedSong us set us.username = null, us.lastManualTime = 0 where us.username = :username and us.hashId <> :hashId and us.lastManualTime < :lastManualTime and us.lastAutoTime < :lastAutoTime"),
        @NamedQuery(name = "UnidentifiedSong.updateByUsername", query = "update UnidentifiedSong us set us.username = null, us.lastManualTime = 0 where us.username = :username and us.lastManualTime < :lastManualTime and us.lastAutoTime < :lastAutoTime"),
})
@NamedQuery(name = "UnidentifiedSong.findAll", query = "SELECT u FROM UnidentifiedSong u")
public class UnidentifiedSong {
    
    private String hashId;
    
    private String title;
    
    private String artists;
    
    private String roles;
    
    private Long priority;
    
    private Long insertTime;
    
    private Long lastAutoTime;
    
    private Long lastManualTime;
    
    private String siadaTitle;
    
    private String siadaArtists;

    private String username;

    public static String getNativeFindAll(){
        return "select hash_id, title, artists, roles, priority, insert_time, last_auto_time, last_manual_time, siada_title, siada_artists, username from unidentified_song ";
    }

    @Id
    @Column(name = "hash_id")
    public String getHashId() {
        return hashId;
    }

    public void setHashId(String hashId) {
        this.hashId = hashId;
    }

    @Basic
    @Column(name = "title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "artists")
    public String getArtists() {
        return artists;
    }

    public void setArtists(String artists) {
        this.artists = artists;
    }

    @Basic
    @Column(name = "roles")
    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    @Basic
    @Column(name = "priority")
    public Long getPriority() {
        return priority;
    }

    public void setPriority(Long priority) {
        this.priority = priority;
    }

    @Basic
    @Column(name = "insert_time")
    public Long getInsertTime() {
        return insertTime;
    }

    public void setInsertTime(Long insertTime) {
        this.insertTime = insertTime;
    }

    @Basic
    @Column(name = "last_auto_time")
    public Long getLastAutoTime() {
        return lastAutoTime;
    }

    public void setLastAutoTime(Long lastAutoTime) {
        this.lastAutoTime = lastAutoTime;
    }

    @Basic
    @Column(name = "last_manual_time")
    public Long getLastManualTime() {
        return lastManualTime;
    }

    public void setLastManualTime(Long lastManualTime) {
        this.lastManualTime = lastManualTime;
    }

    @Basic
    @Column(name = "siada_title")
    public String getSiadaTitle() {
        return siadaTitle;
    }

    public void setSiadaTitle(String siadaTitle) {
        this.siadaTitle = siadaTitle;
    }

    @Basic
    @Column(name = "siada_artists")
    public String getSiadaArtists() {
        return siadaArtists;
    }

    public void setSiadaArtists(String siadaArtists) {
        this.siadaArtists = siadaArtists;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UnidentifiedSong song = (UnidentifiedSong) o;
        return Objects.equals(hashId, song.hashId) &&
                Objects.equals(title, song.title) &&
                Objects.equals(artists, song.artists) &&
                Objects.equals(roles, song.roles) &&
                Objects.equals(priority, song.priority) &&
                Objects.equals(insertTime, song.insertTime) &&
                Objects.equals(lastAutoTime, song.lastAutoTime) &&
                Objects.equals(lastManualTime, song.lastManualTime) &&
                Objects.equals(siadaTitle, song.siadaTitle) &&
                Objects.equals(siadaArtists, song.siadaArtists) &&
                Objects.equals(username, song.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(hashId, title, artists, roles, priority, insertTime, lastAutoTime, lastManualTime, siadaTitle, siadaArtists, username);
    }

    @Basic
    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
