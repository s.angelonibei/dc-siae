package com.alkemytech.sophia.codman.rest;

import com.alkemytech.sophia.codman.guice.UnidentifiedDataSource;
import com.alkemytech.sophia.codman.multimedialelocale.dto.Search;
import com.alkemytech.sophia.codman.multimedialelocale.entity.UnidentifiedSong;
import com.alkemytech.sophia.codman.sso.SsoSessionAttrs;
import com.alkemytech.sophia.codman.utils.PATCH;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.google.inject.servlet.RequestScoped;
import org.apache.commons.lang.StringUtils;
import org.jooq.DSLContext;
import org.jooq.SelectQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.jooq.impl.DSL.*;

@RequestScoped
@Path("unidentified-song")
public class UnidentifiedSongService {

    private final Provider<EntityManager> provider;
    private final Gson gson;
    private final long lockDurationMinutes;
    private final long rejectDurationDays;
    private final long skipDurationSeconds;
    private static Logger logger = LoggerFactory.getLogger(UnidentifiedSongService.class);
    private DSLContext jooq;
    private HttpSession session;

    @Inject
    protected UnidentifiedSongService(@UnidentifiedDataSource Provider<EntityManager> provider, Gson gson,
                                      @Named("unidentified.lock_duration_minutes") long lockDurationMinutes,
                                      @Named("unidentified.reject_duration_days") long rejectDurationDays,
                                      @Named("unidentified.skip_duration_seconds") long skipDurationSeconds,
                                      @UnidentifiedDataSource DSLContext jooq,
                                      HttpSession session) {
        super();
        this.provider = provider;
        this.jooq = jooq;
        this.gson = gson;
        this.lockDurationMinutes = lockDurationMinutes;
        this.rejectDurationDays = rejectDurationDays;
        this.skipDurationSeconds = skipDurationSeconds;
        this.session = session;
    }

	/*@GET
	@Path("next")
	@Produces("application/json")
	public Response findNext(@QueryParam("title") String title,
							 @QueryParam("artists") String artists,
							 @QueryParam("roles") String roles,
							 @DefaultValue("0") @QueryParam("first") int first,
							 @DefaultValue("50") @QueryParam("last") int last,
							 @QueryParam("user") String user) {
		final EntityManager entityManager = provider.get();
		try {
			entityManager.getTransaction().begin();
			final UnidentifiedSong unidentifiedSong = (UnidentifiedSong) entityManager
				.createNativeQuery(new StringBuffer()
				.append(UnidentifiedSong.getNativeFindAll())
				.append(" where last_manual_time < ?")
				.append(" and last_auto_time < ?")
				.append(" order by priority desc limit 1 for update")
				.toString(), UnidentifiedSong.class)
				.setParameter(1, System.currentTimeMillis())
				.setParameter(2, Long.MAX_VALUE) // not yet identified automatically
				.getSingleResult();

			logger.info("opera {} bloccata da {}", unidentifiedSong.getHashId(), user);
			if (null != unidentifiedSong) {
				unidentifiedSong.setLastManualTime(System.currentTimeMillis() +
						TimeUnit.MINUTES.toMillis(lockDurationMinutes));
				entityManager.merge(unidentifiedSong);
				entityManager.getTransaction().commit();
				return Response.ok(gson.toJson(unidentifiedSong)).build();
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			entityManager.getTransaction().rollback();
		}
		return Response.status(500).build();
	}*/

    @GET
    @Path("{hashId}")
    @Produces("application/json")
    public Response byHashId(@PathParam("hashId") String hashId,
                             @QueryParam("user") String user,
                             @QueryParam("title") String title,
                             @QueryParam("artists") String artists,
                             @QueryParam("roles") String roles,
                             @DefaultValue("0") @QueryParam("first") int first,
                             @DefaultValue("50") @QueryParam("last") int last) {
        final EntityManager entityManager = provider.get();
        try {

            if (StringUtils.isEmpty(user)) {
                user = (String) session.getAttribute(SsoSessionAttrs.USERNAME);
            }
            if ("undefined".equals(hashId) || StringUtils.isEmpty(hashId)) {
                SelectQuery<?> select = getRecords();
                select.addConditions(field("username").eq(user).or(field("username").isNull()));
                select.addOrderBy(field("priority").desc());
                select.addLimit(1);
                hashId = select.fetchInto(UnidentifiedSong.class).get(0).getHashId();
            }
            entityManager.getTransaction().begin();
            final UnidentifiedSong unidentifiedSong = entityManager.createNamedQuery("UnidentifiedSong.findByIdAndLastManualTimeLessThan", UnidentifiedSong.class).
                    setParameter("hashId", hashId)
                    .setParameter("lastManualTime", System.currentTimeMillis())
                    .setParameter("username", user)
                    .setLockMode(LockModeType.PESSIMISTIC_WRITE)
                    .getSingleResult();
            if (null != unidentifiedSong) {
                logger.info("opera {} bloccata da {}", unidentifiedSong.getHashId(), user);

//			final UnidentifiedSong unidentifiedSong = (UnidentifiedSong) entityManager
//				.createNativeQuery(new StringBuilder()
//				.append(UnidentifiedSong.getNativeFindAll())
//				.append(" where hash_id = ?")
//				.append(" and last_manual_time < ?")
//				.append(" for update")
//				.toString(), UnidentifiedSong.class)
//				.setParameter(1, hashId)
//				.setParameter(2, System.currentTimeMillis())
//				.getSingleResult();

                unidentifiedSong.setLastManualTime(System.currentTimeMillis() +
                        TimeUnit.MINUTES.toMillis(lockDurationMinutes));
                unidentifiedSong.setUsername(user);
                entityManager.merge(unidentifiedSong);
                entityManager.flush();

                entityManager.getTransaction().commit();
                entityManager.getTransaction().begin();

                entityManager
                        .createNamedQuery("UnidentifiedSong.updateByUsernameAndHashId", UnidentifiedSong.class)
                        .setParameter("hashId", hashId)
                        .setParameter("username", user)
                        .setParameter("lastManualTime", System.currentTimeMillis())
                        .setParameter("lastAutoTime", System.currentTimeMillis())
                        .executeUpdate();
            }
            entityManager.getTransaction().commit();
            return Response.ok(gson.toJson(unidentifiedSong)).build();
        } catch (NoResultException e) {
            entityManager.getTransaction().rollback();
            return Response.status(HttpServletResponse.SC_NOT_FOUND).build(); // 404
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            entityManager.getTransaction().rollback();
        }
        return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR).build(); // 500
    }

    @GET
    @Produces("application/json")
    public Response search(
            @QueryParam("search") String search,
            @QueryParam("unidentifiedSong") String unidentifiedSong,
            @QueryParam("artists") String artists,
            @QueryParam("currentTimeMillis") Long currentTimeMillis,
            @QueryParam("execute") Boolean execute,
            @QueryParam("maxrows") Integer maxrows,
            @QueryParam("navbarTab") String navbarTab,
            @QueryParam("roles") String roles,
            @QueryParam("sortReverse") Boolean sortReverse,
            @QueryParam("sortType") String sortType,
            @QueryParam("title") String title,
            @DefaultValue("0") @QueryParam("first") int first,
            @DefaultValue("50") @QueryParam("last") int last,
            @QueryParam("user") String user) {
//		if (StringUtils.isEmpty(title) && StringUtils.isEmpty(artists) && StringUtils.isEmpty(artists)) {
//			return Response.status(HttpServletResponse.SC_BAD_REQUEST).build();
//		}
        Search searchObj;
        UnidentifiedSong unidentifiedSongObj = null;
        if (StringUtils.isNotEmpty(search))
            searchObj = gson.fromJson(search, Search.class);
        else {
            searchObj = new Search();
            searchObj.setArtists(artists);
            searchObj.setCurrentTimeMillis(currentTimeMillis);
            searchObj.setExecute(execute);
            searchObj.setFirst(first);
            searchObj.setLast(last);
            searchObj.setMaxrows(maxrows);
            searchObj.setNavbarTab(navbarTab);
            searchObj.setRoles(roles);
            searchObj.setSortReverse(sortReverse);
            searchObj.setSortType(sortType);
            searchObj.setTitle(title);
        }
        if (StringUtils.isNotEmpty(unidentifiedSong))
            unidentifiedSongObj = gson.fromJson(unidentifiedSong, UnidentifiedSong.class);
        try {
            return getResponse(searchObj, searchObj.getFirst(), searchObj.getLast(), unidentifiedSongObj, user);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR).build();
    }

    private Response getResponse(Search search,
                                 int first,
                                 int last,
                                 UnidentifiedSong unidentifiedSong,
                                 String user) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        SelectQuery<?> select = getRecords();
//        final StringBuilder sql = new StringBuilder()
//                .append(UnidentifiedSong.getNativeFindAll())
//                .append(" where last_manual_time < ?")
//                .append(" and last_auto_time < ?");
        if (StringUtils.isNotEmpty(search.getTitle())) {
            select.addConditions(field("title").like("%" + search.getTitle() + "%"));
        }
        if (StringUtils.isNotEmpty(search.getArtists())) {
            select.addConditions(field("artists").like("%" + search.getArtists() + "%"));
        }
        if (StringUtils.isNotEmpty(search.getRoles())) {
            select.addConditions(field("roles").like("%" + search.getRoles() + "%"));
        }
        if (StringUtils.isNotEmpty(search.getSortType())) {
            if (search.getSortReverse().booleanValue())
                select.addOrderBy(field(search.getSortType()).desc());
            else
                select.addOrderBy(field(search.getSortType()).asc());
            if (unidentifiedSong != null)
                select.addSeekAfter(val(UnidentifiedSong.class.getMethod("get" + ("" + search.getSortType().charAt(0)).toUpperCase() + search.getSortType().substring(1)).invoke(unidentifiedSong)));
        }
//        if (unidentifiedSong != null)
//            sql.append(" and priority < ?");
//        int i = 1;
//        final Query query = entityManager
//                .createNativeQuery(sql.toString(), UnidentifiedSong.class)
//                .setParameter(i++, Long.MAX_VALUE) // not yet identified manually
//                .setParameter(i++, Long.MAX_VALUE); // not yet identified automatically
//        if (!StringUtils.isEmpty(search.getTitle())) {
//            query.setParameter(i++, "%" + search.getTitle() + "%");
//        }
//        if (!StringUtils.isEmpty(search.getArtists())) {
//            query.setParameter(i++, "%" + search.getArtists() + "%");
//        }
//        if (!StringUtils.isEmpty(search.getRoles())) {
//            query.setParameter(i++, "%" + search.getRoles() + "%");
//        }
        /*f (!"title".equals(search.getSortType())) {
            select.addOrderBy(field("title"));
        }
        if (!"artists".equals(search.getSortType())) {
            select.addOrderBy(field("artists"));
        }
        if (!"roles".equals(search.getSortType())) {
            select.addOrderBy(field("roles"));
        }*/
        select.addOrderBy(field("hash_id"));
        if (unidentifiedSong != null) {
//            select.addSeekAfter(val(unidentifiedSong.getTitle()));
//            select.addSeekAfter(val(unidentifiedSong.getArtists()));
//            select.addSeekAfter(val(unidentifiedSong.getRoles()));
            select.addSeekAfter(val(unidentifiedSong.getHashId()));
            select.addConditions(field("username").eq(user).or(field("username").isNull()));
            select.addLimit(1);
            return Response.ok(gson.toJson(select.fetchInto(UnidentifiedSong.class).get(0))).build();
        } else {
//			query.setParameter(i ++, first);
//			query.setParameter(i ++, 1 + last - first);

            select.addLimit(first, (1 + last - first));
            List<UnidentifiedSong> results = select.fetchInto(UnidentifiedSong.class);
//            List<UnidentifiedSong> results = (List<UnidentifiedSong>) entityManager.createNativeQuery(select.getSQL())
//                    .setFirstResult(first)
//                    .setMaxResults(1 + last - first)
//                    .getResultList();
            final PagedResult result = new PagedResult();
            if (null != results && !results.isEmpty()) {
                final int maxrows = last - first;
                final boolean hasNext = results.size() > maxrows;
                result.setRows(!hasNext ? results : results.subList(0, maxrows))
                        .setMaxrows(maxrows)
                        .setFirst(first)
                        .setLast(hasNext ? last : first + results.size())
                        .setHasNext(hasNext)
                        .setHasPrev(first > 0);
            } else {
                result.setMaxrows(0)
                        .setFirst(0)
                        .setLast(0)
                        .setHasNext(false)
                        .setHasPrev(false);
            }
            return Response.ok(gson.toJson(result)).build();
        }
    }

    private SelectQuery<?> getRecords() {
        DSLContext dslContext = jooq;

        SelectQuery<?> select = dslContext.select(field("hash_id"),
                field("title"),
                field("artists"),
                field("roles"),
                field("priority"),
                field("insert_time"),
                field("last_auto_time"),
                field("last_manual_time"),
                field("siada_title"),
                field("siada_artists"),
                field("username"))
                .from(table("unidentified_song"))
//                .orderBy(field("priority").desc())
                .getQuery();

        select.addConditions(field("last_manual_time").lessThan(Long.MAX_VALUE));
        select.addConditions(field("last_auto_time").lessThan(Long.MAX_VALUE));
        return select;
    }

    @POST
    @Path("skip")
    public Response skip(UnidentifiedSong unidentifiedSong) {
        if (null == unidentifiedSong || null == unidentifiedSong.getHashId()) {
            return Response.ok().build();
        }
        final EntityManager entityManager = provider.get();
        try {
            entityManager.getTransaction().begin();
            unidentifiedSong.setLastManualTime(System.currentTimeMillis() +
                    TimeUnit.SECONDS.toMillis(skipDurationSeconds));
            entityManager.merge(unidentifiedSong);

            logger.info("opera {} bloccata da {}", unidentifiedSong.getHashId());

            entityManager.getTransaction().commit();
            return Response.ok().build();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            entityManager.getTransaction().rollback();
        }
        return Response.status(500).build();
    }

    @PATCH
    @Path("reject")
    public Response reject(UnidentifiedSong unidentifiedSong) {
        final EntityManager entityManager = provider.get();
        try {
            entityManager.getTransaction().begin();
            unidentifiedSong.setLastManualTime(System.currentTimeMillis() +
                    TimeUnit.DAYS.toMillis(rejectDurationDays));
            entityManager.merge(unidentifiedSong);

            entityManager.getTransaction().commit();
            logger.info("opera {} rigettata da {}", unidentifiedSong.getHashId(), (String) session.getAttribute(SsoSessionAttrs.USERNAME));
            return Response.ok().build();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            entityManager.getTransaction().rollback();
        }
        return Response.status(500).build();
    }

    @POST
    @Path("unlock")
    public Response unlock(String user){
        final EntityManager entityManager = provider.get();
        if(StringUtils.isEmpty(user)){
            user = (String) session.getAttribute(SsoSessionAttrs.USERNAME);
        }
        try {
            entityManager.getTransaction().begin();
            entityManager
                    .createNamedQuery("UnidentifiedSong.updateByUsername", UnidentifiedSong.class)
//                    .setParameter("hashId", unidentifiedSong.getHashId())
                    .setParameter("username", user)
                    .setParameter("lastManualTime", System.currentTimeMillis())
                    .setParameter("lastAutoTime", System.currentTimeMillis())
                    .executeUpdate();
            entityManager.getTransaction().commit();
            logger.info("opere sbloccate da {}", user);
            return Response.ok().build();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            entityManager.getTransaction().rollback();
        }
        return Response.status(500).build();
    }

}
