package com.alkemytech.sophia.codman.dto.performing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;
import java.util.Objects;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class UploadRequest implements Serializable {
  private Short anno;
  private Short mese;
  private BigInteger idUtente;
  private BigInteger idBroadcaster;
  private BigInteger idCanale;
  private BigInteger idMusicProvider;
  private BigInteger idPalinsesto;
  private List<String> filenames;
  private List<String> keys;

  public Short getAnno() {
    return anno;
  }

  public void setAnno(Short anno) {
    this.anno = anno;
  }

  public Short getMese() {
    return mese;
  }

  public void setMese(Short mese) {
    this.mese = mese;
  }

  public BigInteger getIdUtente() {
    return idUtente;
  }

  public void setIdUtente(BigInteger idUtente) {
    this.idUtente = idUtente;
  }

  public BigInteger getIdBroadcaster() {
    return idBroadcaster;
  }

  public void setIdBroadcaster(BigInteger idBroadcaster) {
    this.idBroadcaster = idBroadcaster;
  }

  public BigInteger getIdCanale() {
    return idCanale;
  }

  public void setIdCanale(BigInteger idCanale) {
    this.idCanale = idCanale;
  }

  public BigInteger getIdMusicProvider() {
    return idMusicProvider;
  }

  public void setIdMusicProvider(BigInteger idMusicProvider) {
    this.idMusicProvider = idMusicProvider;
  }

  public BigInteger getIdPalinsesto() {
    return idPalinsesto;
  }

  public void setIdPalinsesto(BigInteger idPalinsesto) {
    this.idPalinsesto = idPalinsesto;
  }

  public List<String> getFilenames() {
    return filenames;
  }

  public void setFilenames(List<String> filenames) {
    this.filenames = filenames;
  }

  public List<String> getKeys() {
    return keys;
  }

  public void setKeys(List<String> keys) {
    this.keys = keys;
  }

  public UploadRequest(Short anno, Short mese, BigInteger idUtente, BigInteger idBroadcaster, BigInteger idCanale, BigInteger idMusicProvider, BigInteger idPalinsesto, List<String> filenames, List<String> keys) {
    this.anno = anno;
    this.mese = mese;
    this.idUtente = idUtente;
    this.idBroadcaster = idBroadcaster;
    this.idCanale = idCanale;
    this.idMusicProvider = idMusicProvider;
    this.idPalinsesto = idPalinsesto;
    this.filenames = filenames;
    this.keys = keys;
  }

  public UploadRequest() {
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    UploadRequest that = (UploadRequest) o;
    return Objects.equals(anno, that.anno) &&
            Objects.equals(mese, that.mese) &&
            Objects.equals(idUtente, that.idUtente) &&
            Objects.equals(idBroadcaster, that.idBroadcaster) &&
            Objects.equals(idCanale, that.idCanale) &&
            Objects.equals(idMusicProvider, that.idMusicProvider) &&
            Objects.equals(idPalinsesto, that.idPalinsesto) &&
            Objects.equals(filenames, that.filenames) &&
            Objects.equals(keys, that.keys);
  }

  @Override
  public int hashCode() {
    return Objects.hash(anno, mese, idUtente, idBroadcaster, idCanale, idMusicProvider, idPalinsesto, filenames, keys);
  }

  @Override
  public String toString() {
    return "UploadRequest{" +
            "anno=" + anno +
            ", mese=" + mese +
            ", idUtente=" + idUtente +
            ", idBroadcaster=" + idBroadcaster +
            ", idCanale=" + idCanale +
            ", idMusicProvider=" + idMusicProvider +
            ", idPalinsesto=" + idPalinsesto +
            ", filenames=" + filenames +
            ", keys=" + keys +
            '}';
  }
}
