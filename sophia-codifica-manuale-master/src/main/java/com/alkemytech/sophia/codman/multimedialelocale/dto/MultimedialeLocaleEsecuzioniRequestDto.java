package com.alkemytech.sophia.codman.multimedialelocale.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MultimedialeLocaleEsecuzioniRequestDto {

	private String dataAvvioDa;
	private String dataAvvioA;
	private Integer first;
	private Integer last;
	private Integer maxrow;

	public MultimedialeLocaleEsecuzioniRequestDto() {
		super();
	}

	public String getDataAvvioDa() {
		return dataAvvioDa;
	}

	public void setDataAvvioDa(String dataAvvioDa) {
		this.dataAvvioDa = dataAvvioDa;
	}

	public String getDataAvvioA() {
		return dataAvvioA;
	}

	public void setDataAvvioA(String dataAvvioA) {
		this.dataAvvioA = dataAvvioA;
	}

	public Integer getFirst() {
		return first;
	}

	public void setFirst(Integer first) {
		this.first = first;
	}

	public Integer getLast() {
		return last;
	}

	public void setLast(Integer last) {
		this.last = last;
	}

	public Integer getMaxrow() {
		return maxrow;
	}

	public void setMaxrow(Integer maxrow) {
		this.maxrow = maxrow;
	}

}
