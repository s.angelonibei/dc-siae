package com.alkemytech.sophia.codman.dto.performing;


import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class CarichiRipartizioneCarryOver {
	private String periodoRipartizione;
	private String periodoRipartizioneDa;
	private String periodoRipartizioneA;
	private List<CarichiRipartizioneDTO> carryOver;
	
	
	
	public CarichiRipartizioneCarryOver(String periodoRipartizione, String periodoRipartizioneDa,
			String periodoRipartizioneA, List<CarichiRipartizioneDTO> carryOver) {
		super();
		this.periodoRipartizione = periodoRipartizione;
		this.periodoRipartizioneDa = periodoRipartizioneDa;
		this.periodoRipartizioneA = periodoRipartizioneA;
		this.carryOver = carryOver;
	}
	public String getPeriodoRipartizione() {
		return periodoRipartizione;
	}
	public void setPeriodoRipartizione(String periodoRipartizione) {
		this.periodoRipartizione = periodoRipartizione;
	}
	public String getPeriodoRipartizioneDa() {
		return periodoRipartizioneDa;
	}
	public void setPeriodoRipartizioneDa(String periodoRipartizioneDa) {
		this.periodoRipartizioneDa = periodoRipartizioneDa;
	}
	public String getPeriodoRipartizioneA() {
		return periodoRipartizioneA;
	}
	public void setPeriodoRipartizioneA(String periodoRipartizioneA) {
		this.periodoRipartizioneA = periodoRipartizioneA;
	}
	public List<CarichiRipartizioneDTO> getCarryOver() {
		return carryOver;
	}
	public void setCarryOver(List<CarichiRipartizioneDTO> carryOver) {
		this.carryOver = carryOver;
	}
	

}
