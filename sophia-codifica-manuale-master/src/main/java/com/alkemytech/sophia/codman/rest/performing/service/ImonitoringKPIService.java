package com.alkemytech.sophia.codman.rest.performing.service;

import java.util.List;

import com.alkemytech.sophia.codman.dto.performing.MonitoringKPIDTO;
import com.alkemytech.sophia.codman.entity.performing.ConfigurazioniSoglie;
import com.alkemytech.sophia.codman.entity.performing.InformazioneKPICodifica;

public interface ImonitoringKPIService {

	
	public List<InformazioneKPICodifica> getRecordMonitoring(Long periodoRipartizione, String voceIncasso, String tipologiaReport);

}
