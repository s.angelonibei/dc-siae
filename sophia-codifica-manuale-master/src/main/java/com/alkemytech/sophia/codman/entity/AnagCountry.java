package com.alkemytech.sophia.codman.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.codman.persistence.AbstractEntity;
import com.google.gson.GsonBuilder;

@XmlRootElement
@Entity(name="AnagCountry")
@Table(name="ANAG_COUNTRY")
@NamedQueries({@NamedQuery(name="AnagCountry.GetAll", query="SELECT country FROM AnagCountry country"),
	@NamedQuery(name="AnagCountry.GetByCode", query="SELECT country FROM AnagCountry country WHERE country.idCountry = :code")})
@SuppressWarnings("serial")
 

public class AnagCountry extends AbstractEntity<String> {

	@Id
	@Column(name="ID_COUNTRY", nullable=false)
	private String idCountry;
	
	@Column(name="CODE", nullable=false)
	private String code;
	
	@Column(name="ISO_CODE", nullable=false)
	private String isoCode;
	
	@Column(name="NAME")
	private String name;
	
	@Override
	public String getId() {
		return getIdCountry();
	}

	public String getIdCountry() {
		return idCountry;
	}

	public void setIdCountry(String idCountry) {
		this.idCountry = idCountry;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getIsoCode() {
		return isoCode;
	}

	public void setIsoCode(String isoCode) {
		this.isoCode = isoCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}
}