package com.alkemytech.sophia.codman.entity.performing;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Entity(name="PerfVoceIncasso")
@Table(name="PERF_VOCE_INCASSO")
public class PerfVoceIncasso {

    @Id
    @Column(name="COD_VOCE_INCASSO", nullable=false)
    private String codVoceIncasso;

    @Column(name="DESCRIZIONE")
    private String descrizione;

    @Column(name="COD_VOCE_IVA")
    private String codVoceIva;

    @Column(name="COD_VOCE_MORA")
    private String codVoceMora;

    @Column(name="PERCENT_MORA")
    private Integer percentMora;

    @Column(name="COD_INTERESSI_MORA")
    private String codInteressiMora;

    @Column(name="BASE_CALC_INTER_MORA")
    private Character baseCalcInterMora;

    @Column(name="FLAG_BOLLO")
    private Character flagBollo;

    @Column(name="COD_POSIZIONE_IVA")
    private Character codPosizioneIva;

    @Column(name="FLAG_ENTR_USC")
    private Character flagEntrUsc;

    @Column(name="ID_TIPO_MODELLO")
    private Integer idTipoModello;

    @Column(name="COD_IMPONIBILE_IVA")
    private String codImponibileIva;

    @Column(name="ID_SOPRACONTO")
    private Integer idSopraconto;

    @Column(name="TIPO_VOCE")
    private Character tipoVoce;

    @Column(name="FLAG_PM")
    private Boolean flagPm;

    @Column(name="COD_VOCE_240")
    private String codVoce240;

    @Column(name="COD_VOCE_48M")
    private String codVoce48m;

    @Column(name="FLAG_OC")
    private Boolean flagOC;

    @Column(name="DESCRIZIONE_BREVE")
    private String descrizioneBreve;

    @Column(name="FLAG_PRIVACY")
    private Boolean flagPrivacy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="DATA_CESSAZIONE_VALIDITA")
    private Date dataCessazioneValidita;

    @Column(name="TIPO_PM")
    private String tipoPM;

    @Column(name="COD_VOCE_LIRICA")
    private String codVoceLirica;

    @Column(name="ABILITA_DIRITTI_SEGRETERIA")
    private Boolean abilitaDirittiSegreteria;

    @Column(name="FLAG_VOCE_DEPOSITO")
    private Boolean flagVoceDeposito;

    @Column(name="FLAG_VOCE_ORDINE_E_CONTO")
    private Boolean flagVoceOrdineEConto;

    @Column(name="FLAG_VOCE_VERSAMENTO")
    private Boolean flagVoceVersamento;

    @Column(name="FLAG_AUTORALE")
    private Boolean flagAutorale;

    @Column(name="COD_VOCE_SOSTITUTIVA")
    private String codVoceSostitutiva;

    @Column(name="FLAG_ALLEGATO")
    private Boolean flagAllegato;

    @Column(name="FLAG_STORNO_INTERNO")
    private Boolean flagStornoInterno;

    @Column(name="FLAG_NCA")
    private Boolean flagNCA;

    @Column(name="FLAG_ENABLE_QUIETANZA_MANUALE")
    private Boolean flagEnableQuietanzaManuale;

    @Column(name="FLAG_DIRITTO_CONNESSO")
    private Boolean flagDirittoConnesso;
    
    @Column(name="FLAG_VOCE_RIPARTIZIONE_FITTIZIA")
    private Boolean flagVoceRipartizioneFittizia;
    
    public PerfVoceIncasso() {
        super();
    }

    public PerfVoceIncasso(String codVoceIncasso, String descrizione, String codVoceIva, String codVoceMora, Integer percentMora, String codInteressiMora, Character baseCalcInterMora, Character flagBollo, Character codPosizioneIva, Character flagEntrUsc, Integer idTipoModello, String codImponibileIva, Integer idSopraconto, Character tipoVoce, Boolean flagPm, String codVoce240, String codVoce48m, Boolean flagOC, String descrizioneBreve, Boolean flagPrivacy, Date dataCessazioneValidita, String tipoPM, String codVoceLirica, Boolean abilitaDirittiSegreteria, Boolean flagVoceDeposito, Boolean flagVoceOrdineEConto, Boolean flagVoceVersamento, Boolean flagAutorale, String codVoceSostitutiva, Boolean flagAllegato, Boolean flagStornoInterno, Boolean flagNCA, Boolean flagEnableQuietanzaManuale, Boolean flagDirittoConnesso) {
        this.codVoceIncasso = codVoceIncasso;
        this.descrizione = descrizione;
        this.codVoceIva = codVoceIva;
        this.codVoceMora = codVoceMora;
        this.percentMora = percentMora;
        this.codInteressiMora = codInteressiMora;
        this.baseCalcInterMora = baseCalcInterMora;
        this.flagBollo = flagBollo;
        this.codPosizioneIva = codPosizioneIva;
        this.flagEntrUsc = flagEntrUsc;
        this.idTipoModello = idTipoModello;
        this.codImponibileIva = codImponibileIva;
        this.idSopraconto = idSopraconto;
        this.tipoVoce = tipoVoce;
        this.flagPm = flagPm;
        this.codVoce240 = codVoce240;
        this.codVoce48m = codVoce48m;
        this.flagOC = flagOC;
        this.descrizioneBreve = descrizioneBreve;
        this.flagPrivacy = flagPrivacy;
        this.dataCessazioneValidita = dataCessazioneValidita;
        this.tipoPM = tipoPM;
        this.codVoceLirica = codVoceLirica;
        this.abilitaDirittiSegreteria = abilitaDirittiSegreteria;
        this.flagVoceDeposito = flagVoceDeposito;
        this.flagVoceOrdineEConto = flagVoceOrdineEConto;
        this.flagVoceVersamento = flagVoceVersamento;
        this.flagAutorale = flagAutorale;
        this.codVoceSostitutiva = codVoceSostitutiva;
        this.flagAllegato = flagAllegato;
        this.flagStornoInterno = flagStornoInterno;
        this.flagNCA = flagNCA;
        this.flagEnableQuietanzaManuale = flagEnableQuietanzaManuale;
        this.flagDirittoConnesso = flagDirittoConnesso;
    }

    public String getCodVoceIncasso() {
        return codVoceIncasso;
    }

    public void setCodVoceIncasso(String codVoceIncasso) {
        this.codVoceIncasso = codVoceIncasso;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public String getCodVoceIva() {
        return codVoceIva;
    }

    public void setCodVoceIva(String codVoceIva) {
        this.codVoceIva = codVoceIva;
    }

    public String getCodVoceMora() {
        return codVoceMora;
    }

    public void setCodVoceMora(String codVoceMora) {
        this.codVoceMora = codVoceMora;
    }

    public Integer getPercentMora() {
        return percentMora;
    }

    public void setPercentMora(Integer percentMora) {
        this.percentMora = percentMora;
    }

    public String getCodInteressiMora() {
        return codInteressiMora;
    }

    public void setCodInteressiMora(String codInteressiMora) {
        this.codInteressiMora = codInteressiMora;
    }

    public Character getBaseCalcInterMora() {
        return baseCalcInterMora;
    }

    public void setBaseCalcInterMora(Character baseCalcInterMora) {
        this.baseCalcInterMora = baseCalcInterMora;
    }

    public Character getFlagBollo() {
        return flagBollo;
    }

    public void setFlagBollo(Character flagBollo) {
        this.flagBollo = flagBollo;
    }

    public Character getCodPosizioneIva() {
        return codPosizioneIva;
    }

    public void setCodPosizioneIva(Character codPosizioneIva) {
        this.codPosizioneIva = codPosizioneIva;
    }

    public Character getFlagEntrUsc() {
        return flagEntrUsc;
    }

    public void setFlagEntrUsc(Character flagEntrUsc) {
        this.flagEntrUsc = flagEntrUsc;
    }

    public Integer getIdTipoModello() {
        return idTipoModello;
    }

    public void setIdTipoModello(Integer idTipoModello) {
        this.idTipoModello = idTipoModello;
    }

    public String getCodImponibileIva() {
        return codImponibileIva;
    }

    public void setCodImponibileIva(String codImponibileIva) {
        this.codImponibileIva = codImponibileIva;
    }

    public Integer getIdSopraconto() {
        return idSopraconto;
    }

    public void setIdSopraconto(Integer idSopraconto) {
        this.idSopraconto = idSopraconto;
    }

    public Character getTipoVoce() {
        return tipoVoce;
    }

    public void setTipoVoce(Character tipoVoce) {
        this.tipoVoce = tipoVoce;
    }

    public Boolean getFlagPm() {
        return flagPm;
    }

    public void setFlagPm(Boolean flagPm) {
        this.flagPm = flagPm;
    }

    public String getCodVoce240() {
        return codVoce240;
    }

    public void setCodVoce240(String codVoce240) {
        this.codVoce240 = codVoce240;
    }

    public String getCodVoce48m() {
        return codVoce48m;
    }

    public void setCodVoce48m(String codVoce48m) {
        this.codVoce48m = codVoce48m;
    }

    public Boolean getFlagOC() {
        return flagOC;
    }

    public void setFlagOC(Boolean flagOC) {
        this.flagOC = flagOC;
    }

    public String getDescrizioneBreve() {
        return descrizioneBreve;
    }

    public void setDescrizioneBreve(String descrizioneBreve) {
        this.descrizioneBreve = descrizioneBreve;
    }

    public Boolean getFlagPrivacy() {
        return flagPrivacy;
    }

    public void setFlagPrivacy(Boolean flagPrivacy) {
        this.flagPrivacy = flagPrivacy;
    }

    public Date getDataCessazioneValidita() {
        return dataCessazioneValidita;
    }

    public void setDataCessazioneValidita(Date dataCessazioneValidita) {
        this.dataCessazioneValidita = dataCessazioneValidita;
    }

    public String getTipoPM() {
        return tipoPM;
    }

    public void setTipoPM(String tipoPM) {
        this.tipoPM = tipoPM;
    }

    public String getCodVoceLirica() {
        return codVoceLirica;
    }

    public void setCodVoceLirica(String codVoceLirica) {
        this.codVoceLirica = codVoceLirica;
    }

    public Boolean getAbilitaDirittiSegreteria() {
        return abilitaDirittiSegreteria;
    }

    public void setAbilitaDirittiSegreteria(Boolean abilitaDirittiSegreteria) {
        this.abilitaDirittiSegreteria = abilitaDirittiSegreteria;
    }

    public Boolean getFlagVoceDeposito() {
        return flagVoceDeposito;
    }

    public void setFlagVoceDeposito(Boolean flagVoceDeposito) {
        this.flagVoceDeposito = flagVoceDeposito;
    }

    public Boolean getFlagVoceOrdineEConto() {
        return flagVoceOrdineEConto;
    }

    public void setFlagVoceOrdineEConto(Boolean flagVoceOrdineEConto) {
        this.flagVoceOrdineEConto = flagVoceOrdineEConto;
    }

    public Boolean getFlagVoceVersamento() {
        return flagVoceVersamento;
    }

    public void setFlagVoceVersamento(Boolean flagVoceVersamento) {
        this.flagVoceVersamento = flagVoceVersamento;
    }

    public Boolean getFlagAutorale() {
        return flagAutorale;
    }

    public void setFlagAutorale(Boolean flagAutorale) {
        this.flagAutorale = flagAutorale;
    }

    public String getCodVoceSostitutiva() {
        return codVoceSostitutiva;
    }

    public void setCodVoceSostitutiva(String codVoceSostitutiva) {
        this.codVoceSostitutiva = codVoceSostitutiva;
    }

    public Boolean getFlagAllegato() {
        return flagAllegato;
    }

    public void setFlagAllegato(Boolean flagAllegato) {
        this.flagAllegato = flagAllegato;
    }

    public Boolean getFlagStornoInterno() {
        return flagStornoInterno;
    }

    public void setFlagStornoInterno(Boolean flagStornoInterno) {
        this.flagStornoInterno = flagStornoInterno;
    }

    public Boolean getFlagNCA() {
        return flagNCA;
    }

    public void setFlagNCA(Boolean flagNCA) {
        this.flagNCA = flagNCA;
    }

    public Boolean getFlagEnableQuietanzaManuale() {
        return flagEnableQuietanzaManuale;
    }

    public void setFlagEnableQuietanzaManuale(Boolean flagEnableQuietanzaManuale) {
        this.flagEnableQuietanzaManuale = flagEnableQuietanzaManuale;
    }

    public Boolean getFlagDirittoConnesso() {
        return flagDirittoConnesso;
    }

    public void setFlagDirittoConnesso(Boolean flagDirittoConnesso) {
        this.flagDirittoConnesso = flagDirittoConnesso;
    }

    public Boolean getFlagVoceRipartizioneFittizia() {
		return flagVoceRipartizioneFittizia;
	}

	public void setFlagVoceRipartizioneFittizia(Boolean flagVoceRipartizioneFittizia) {
		this.flagVoceRipartizioneFittizia = flagVoceRipartizioneFittizia;
	}

	@Override
    public String toString() {
        return "PerfVoceIncasso{" +
                "codVoceIncasso='" + codVoceIncasso + '\'' +
                ", descrizione='" + descrizione + '\'' +
                ", codVoceIva='" + codVoceIva + '\'' +
                ", codVoceMora='" + codVoceMora + '\'' +
                ", percentMora=" + percentMora +
                ", codInteressiMora='" + codInteressiMora + '\'' +
                ", baseCalcInterMora=" + baseCalcInterMora +
                ", flagBollo=" + flagBollo +
                ", codPosizioneIva=" + codPosizioneIva +
                ", flagEntrUsc=" + flagEntrUsc +
                ", idTipoModello=" + idTipoModello +
                ", codImponibileIva='" + codImponibileIva + '\'' +
                ", idSopraconto=" + idSopraconto +
                ", tipoVoce=" + tipoVoce +
                ", flagPm=" + flagPm +
                ", codVoce240='" + codVoce240 + '\'' +
                ", codVoce48m='" + codVoce48m + '\'' +
                ", flagOC=" + flagOC +
                ", descrizioneBreve='" + descrizioneBreve + '\'' +
                ", flagPrivacy=" + flagPrivacy +
                ", dataCessazioneValidita=" + dataCessazioneValidita +
                ", tipoPM='" + tipoPM + '\'' +
                ", codVoceLirica='" + codVoceLirica + '\'' +
                ", abilitaDirittiSegreteria=" + abilitaDirittiSegreteria +
                ", flagVoceDeposito=" + flagVoceDeposito +
                ", flagVoceOrdineEConto=" + flagVoceOrdineEConto +
                ", flagVoceVersamento=" + flagVoceVersamento +
                ", flagAutorale=" + flagAutorale +
                ", codVoceSostitutiva='" + codVoceSostitutiva + '\'' +
                ", flagAllegato=" + flagAllegato +
                ", flagStornoInterno=" + flagStornoInterno +
                ", flagNCA=" + flagNCA +
                ", flagEnableQuietanzaManuale=" + flagEnableQuietanzaManuale +
                ", flagDirittoConnesso=" + flagDirittoConnesso +
                '}';
    }
}
