package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.codman.entity.SqsDsrProgress;
import com.google.gson.GsonBuilder;

@Produces("application/json")
@XmlRootElement 
@SuppressWarnings("serial")
public class DsrProgressDTO  implements Serializable {
	
	private String idDsr;

	private Date extractQueued;
	private Date extractStarted;
	private Date extractCompleted;
	private Date extractFailed;
	
	private Date cleanQueued;
	private Date cleanStarted;
	private Date cleanCompleted;
	private Date cleanFailed;

	private Date priceQueued;
	private Date priceStarted;
	private Date priceCompleted;
	private Date priceFailed;
	
	private Date hypercubeQueued;
	private Date hypercubeStarted;
	private Date hypercubeCompleted;
	private Date hypercubeFailed;

	private Date identifyQueued;
	private Date identifyStarted;
	private Date identifyCompleted;
	private Date identifyFailed;

	private Date uniloadQueued;
	private Date uniloadStarted;
	private Date uniloadCompleted;
	private Date uniloadFailed;

	private Date claimQueued;
	private Date claimStarted;
	private Date claimCompleted;
	private Date claimFailed;
	
	private Date prelearnQueued;
	private Date prelearnStarted;
	private Date prelearnCompleted;
	private Date prelearnFailed;

	private Date learnQueued;
	private Date learnStarted;
	private Date learnCompleted;
	private Date learnFailed;
	
	public boolean isCompleted() {
		return null != extractCompleted &&
				null != cleanCompleted &&
				null != priceCompleted &&
				null != identifyCompleted &&
				null != uniloadCompleted &&
				null != hypercubeCompleted &&
				null != prelearnCompleted &&
				null != learnCompleted &&
				null != claimCompleted;
	}

	public boolean hasFailures() {
		return null != extractFailed ||
				null != cleanFailed ||
				null != priceFailed ||
				null != identifyFailed ||
				null != uniloadFailed ||
				null != hypercubeFailed ||
				null != prelearnFailed ||
				null != learnFailed ||
				null != claimFailed;
	}

	public void updateWith(SqsDsrProgress sqsDsrProgress) {
		if ("to_process".equalsIgnoreCase(sqsDsrProgress.getQueueType())) {
			if ("extraction_and_validation".equalsIgnoreCase(sqsDsrProgress.getServiceName())) {
				final Date ref = extractQueued = sqsDsrProgress.getInsertTime();
				// overall cut-off time
				extractStarted = nullIfBefore(extractStarted, ref);
				extractCompleted = nullIfBefore(extractCompleted, ref);
				extractFailed = nullIfBefore(extractFailed, ref);
				
				cleanQueued = nullIfBefore(cleanQueued, ref);
				cleanStarted = nullIfBefore(cleanStarted, ref);
				cleanCompleted = nullIfBefore(cleanCompleted, ref);
				cleanFailed = nullIfBefore(cleanFailed, ref);
				
				priceQueued = nullIfBefore(priceQueued, ref);
				priceStarted = nullIfBefore(priceStarted, ref);
				priceCompleted = nullIfBefore(priceCompleted, ref);
				priceFailed = nullIfBefore(priceFailed, ref);
				
				hypercubeQueued = nullIfBefore(hypercubeQueued, ref);
				hypercubeStarted = nullIfBefore(hypercubeStarted, ref);
				hypercubeCompleted = nullIfBefore(hypercubeCompleted, ref);
				hypercubeFailed = nullIfBefore(hypercubeFailed, ref);
				
				identifyQueued = nullIfBefore(identifyQueued, ref);
				identifyStarted = nullIfBefore(identifyStarted, ref);
				identifyCompleted = nullIfBefore(identifyCompleted, ref);
				identifyFailed = nullIfBefore(identifyFailed, ref);
				
				uniloadQueued = nullIfBefore(uniloadQueued, ref);
				uniloadStarted = nullIfBefore(uniloadStarted, ref);
				uniloadCompleted = nullIfBefore(uniloadCompleted, ref);
				uniloadFailed = nullIfBefore(uniloadFailed, ref);
				
				claimQueued = nullIfBefore(claimQueued, ref);
				claimStarted = nullIfBefore(claimStarted, ref);
				claimCompleted = nullIfBefore(claimCompleted, ref);
				claimFailed = nullIfBefore(claimFailed, ref);
				
				prelearnQueued = nullIfBefore(prelearnQueued, ref);
				prelearnStarted = nullIfBefore(prelearnStarted, ref);
				prelearnCompleted = nullIfBefore(prelearnCompleted, ref);
				prelearnFailed = nullIfBefore(prelearnFailed, ref);
				
				learnQueued = nullIfBefore(learnQueued, ref);
				learnStarted = nullIfBefore(learnStarted, ref);
				learnCompleted = nullIfBefore(learnCompleted, ref);
				learnFailed = nullIfBefore(learnFailed, ref);
			} else if ("cleaning_and_normalization".equalsIgnoreCase(sqsDsrProgress.getServiceName())) {
				final Date ref = cleanQueued = nullIfBeforeIgnoreNull(sqsDsrProgress.getInsertTime(), extractQueued);
				// cleaning_and_normalization cut-off time
				cleanStarted = nullIfBefore(cleanStarted, ref);
				cleanCompleted = nullIfBefore(cleanCompleted, ref);
				cleanFailed = nullIfBefore(cleanFailed, ref);
			} else if ("identification".equalsIgnoreCase(sqsDsrProgress.getServiceName())) {
				final Date ref = identifyQueued = nullIfBeforeIgnoreNull(sqsDsrProgress.getInsertTime(), extractQueued);
				// identification cut-off time
				identifyStarted = nullIfBefore(identifyStarted, ref);
				identifyCompleted = nullIfBefore(identifyCompleted, ref);
				identifyFailed = nullIfBefore(identifyFailed, ref);
			} else if ("pricing".equalsIgnoreCase(sqsDsrProgress.getServiceName())) {
				final Date ref = priceQueued = nullIfBeforeIgnoreNull(sqsDsrProgress.getInsertTime(), extractQueued);
				// pricing cut-off time
				priceStarted = nullIfBefore(priceStarted, ref);
				priceCompleted = nullIfBefore(priceCompleted, ref);
				priceFailed = nullIfBefore(priceFailed, ref);
			} else if ("unidentified".equalsIgnoreCase(sqsDsrProgress.getServiceName())) {
				final Date ref = uniloadQueued = nullIfBeforeIgnoreNull(sqsDsrProgress.getInsertTime(), extractQueued);
				// unidentified cut-off time
				uniloadStarted = nullIfBefore(uniloadStarted, ref);
				uniloadCompleted = nullIfBefore(uniloadCompleted, ref);
				uniloadFailed = nullIfBefore(uniloadFailed, ref);
			} else if ("hypercube_slice".equalsIgnoreCase(sqsDsrProgress.getServiceName())) {
				final Date ref = hypercubeQueued = nullIfBeforeIgnoreNull(sqsDsrProgress.getInsertTime(), extractQueued);
				// hypercube_slice cut-off time
				hypercubeQueued = nullIfBefore(hypercubeQueued, ref);
				hypercubeStarted = nullIfBefore(hypercubeStarted, ref);
				hypercubeCompleted = nullIfBefore(hypercubeCompleted, ref);
			} else if ("pre_learning".equalsIgnoreCase(sqsDsrProgress.getServiceName())) {
				final Date ref = prelearnQueued = nullIfBeforeIgnoreNull(sqsDsrProgress.getInsertTime(), extractQueued);
				// pre_learning cut-off time
				prelearnStarted = nullIfBefore(prelearnStarted, ref);
				prelearnCompleted = nullIfBefore(prelearnCompleted, ref);
				prelearnFailed = nullIfBefore(prelearnFailed, ref);
			} else if ("learning".equalsIgnoreCase(sqsDsrProgress.getServiceName())) {
				final Date ref = learnQueued = nullIfBeforeIgnoreNull(sqsDsrProgress.getInsertTime(), extractQueued);
				// learning cut-off time
				learnStarted = nullIfBefore(learnStarted, ref);
				learnCompleted = nullIfBefore(learnCompleted, ref);
				learnFailed = nullIfBefore(learnFailed, ref);
			} else if ("claim".equalsIgnoreCase(sqsDsrProgress.getServiceName())) {
				final Date ref = claimQueued = nullIfBeforeIgnoreNull(sqsDsrProgress.getInsertTime(), extractQueued);
				// claim cut-off time
				claimStarted = nullIfBefore(claimStarted, ref);
				claimCompleted = nullIfBefore(claimCompleted, ref);
				claimFailed = nullIfBefore(claimFailed, ref);
			}
		} else if ("started".equalsIgnoreCase(sqsDsrProgress.getQueueType())) {
			if ("extraction_and_validation".equalsIgnoreCase(sqsDsrProgress.getServiceName())) {
				final Date ref = extractStarted = nullIfBeforeIgnoreNull(sqsDsrProgress.getInsertTime(), extractQueued);
				// extraction_and_validation cut-off time
				extractCompleted = nullIfBefore(extractCompleted, ref);
				extractFailed = nullIfBefore(extractFailed, ref);
			} else if ("cleaning_and_normalization".equalsIgnoreCase(sqsDsrProgress.getServiceName())) {
				final Date ref = cleanStarted = nullIfBeforeIgnoreNull(sqsDsrProgress.getInsertTime(), cleanQueued);
				// extraction_and_validation cut-off time
				cleanCompleted = nullIfBefore(cleanCompleted, ref);
				cleanFailed = nullIfBefore(cleanFailed, ref);
			} else if ("identification".equalsIgnoreCase(sqsDsrProgress.getServiceName())) {
				final Date ref = identifyStarted = nullIfBeforeIgnoreNull(sqsDsrProgress.getInsertTime(), identifyQueued);
				// identification cut-off time
				identifyCompleted = nullIfBefore(identifyCompleted, ref);
				identifyFailed = nullIfBefore(identifyFailed, ref);
			} else if ("pricing".equalsIgnoreCase(sqsDsrProgress.getServiceName())) {
				final Date ref = priceStarted = nullIfBeforeIgnoreNull(sqsDsrProgress.getInsertTime(), priceQueued);
				// pricing cut-off time
				priceCompleted = nullIfBefore(priceCompleted, ref);
				priceFailed = nullIfBefore(priceFailed, ref);
			} else if ("unidentified".equalsIgnoreCase(sqsDsrProgress.getServiceName())) {
				final Date ref = uniloadStarted = nullIfBeforeIgnoreNull(sqsDsrProgress.getInsertTime(), uniloadQueued);
				// unidentified cut-off time
				uniloadCompleted = nullIfBefore(uniloadCompleted, ref);
				uniloadFailed = nullIfBefore(uniloadFailed, ref);
			} else if ("hypercube_slice".equalsIgnoreCase(sqsDsrProgress.getServiceName())) {
				final Date ref = hypercubeStarted = nullIfBeforeIgnoreNull(sqsDsrProgress.getInsertTime(), hypercubeQueued);
				// hypercube_slice cut-off time
				hypercubeStarted = nullIfBefore(hypercubeStarted, ref);
				hypercubeCompleted = nullIfBefore(hypercubeCompleted, ref);
			} else if ("pre_learning".equalsIgnoreCase(sqsDsrProgress.getServiceName())) {
				final Date ref = prelearnStarted = nullIfBeforeIgnoreNull(sqsDsrProgress.getInsertTime(), prelearnQueued);
				// pre_learning cut-off time
				prelearnCompleted = nullIfBefore(prelearnCompleted, ref);
				prelearnFailed = nullIfBefore(prelearnFailed, ref);
			} else if ("learning".equalsIgnoreCase(sqsDsrProgress.getServiceName())) {
				final Date ref = learnStarted = nullIfBeforeIgnoreNull(sqsDsrProgress.getInsertTime(), learnQueued);
				// learning cut-off time
				learnCompleted = nullIfBefore(learnCompleted, ref);
				learnFailed = nullIfBefore(learnFailed, ref);
			} else if ("claim".equalsIgnoreCase(sqsDsrProgress.getServiceName())) {
				final Date ref = claimStarted = nullIfBeforeIgnoreNull(sqsDsrProgress.getInsertTime(), claimQueued);
				// claim cut-off time
				claimCompleted = nullIfBefore(claimCompleted, ref);
				claimFailed = nullIfBefore(claimFailed, ref);
			}
		} else if ("completed".equalsIgnoreCase(sqsDsrProgress.getQueueType())) {
			if ("extraction_and_validation".equalsIgnoreCase(sqsDsrProgress.getServiceName())) {
				extractCompleted = nullIfBeforeIgnoreNull(sqsDsrProgress.getInsertTime(), extractStarted);
			} else if ("cleaning_and_normalization".equalsIgnoreCase(sqsDsrProgress.getServiceName())) {
				cleanCompleted = nullIfBeforeIgnoreNull(sqsDsrProgress.getInsertTime(), cleanStarted);
			} else if ("identification".equalsIgnoreCase(sqsDsrProgress.getServiceName())) {
				identifyCompleted = nullIfBeforeIgnoreNull(sqsDsrProgress.getInsertTime(), identifyStarted);
			} else if ("pricing".equalsIgnoreCase(sqsDsrProgress.getServiceName())) {
				priceCompleted = nullIfBeforeIgnoreNull(sqsDsrProgress.getInsertTime(), priceStarted);
			} else if ("unidentified".equalsIgnoreCase(sqsDsrProgress.getServiceName())) {
				uniloadCompleted = nullIfBeforeIgnoreNull(sqsDsrProgress.getInsertTime(), uniloadStarted);
			} else if ("hypercube_slice".equalsIgnoreCase(sqsDsrProgress.getServiceName())) {
				hypercubeCompleted = nullIfBeforeIgnoreNull(sqsDsrProgress.getInsertTime(), hypercubeStarted);
			} else if ("pre_learning".equalsIgnoreCase(sqsDsrProgress.getServiceName())) {
				prelearnCompleted = nullIfBeforeIgnoreNull(sqsDsrProgress.getInsertTime(), prelearnStarted);
			} else if ("learning".equalsIgnoreCase(sqsDsrProgress.getServiceName())) {
				learnCompleted = nullIfBeforeIgnoreNull(sqsDsrProgress.getInsertTime(), learnStarted);
			} else if ("claim".equalsIgnoreCase(sqsDsrProgress.getServiceName())) {
				claimCompleted = nullIfBeforeIgnoreNull(sqsDsrProgress.getInsertTime(), claimStarted);
			}
		} else if ("failed".equalsIgnoreCase(sqsDsrProgress.getQueueType())) {
			if ("extraction_and_validation".equalsIgnoreCase(sqsDsrProgress.getServiceName())) {
				extractFailed = nullIfBeforeIgnoreNull(sqsDsrProgress.getInsertTime(), extractStarted);
			} else if ("cleaning_and_normalization".equalsIgnoreCase(sqsDsrProgress.getServiceName())) {
				cleanFailed = nullIfBeforeIgnoreNull(sqsDsrProgress.getInsertTime(), cleanStarted);
			} else if ("identification".equalsIgnoreCase(sqsDsrProgress.getServiceName())) {
				identifyFailed = nullIfBeforeIgnoreNull(sqsDsrProgress.getInsertTime(), identifyStarted);
			} else if ("pricing".equalsIgnoreCase(sqsDsrProgress.getServiceName())) {
				priceFailed = nullIfBeforeIgnoreNull(sqsDsrProgress.getInsertTime(), priceStarted);
			} else if ("unidentified".equalsIgnoreCase(sqsDsrProgress.getServiceName())) {
				uniloadFailed = nullIfBeforeIgnoreNull(sqsDsrProgress.getInsertTime(), uniloadStarted);
			} else if ("hypercube_slice".equalsIgnoreCase(sqsDsrProgress.getServiceName())) {
				hypercubeFailed = nullIfBeforeIgnoreNull(sqsDsrProgress.getInsertTime(), hypercubeStarted);
			} else if ("pre_learning".equalsIgnoreCase(sqsDsrProgress.getServiceName())) {
				prelearnFailed = nullIfBeforeIgnoreNull(sqsDsrProgress.getInsertTime(), prelearnStarted);
			} else if ("learning".equalsIgnoreCase(sqsDsrProgress.getServiceName())) {
				learnFailed = nullIfBeforeIgnoreNull(sqsDsrProgress.getInsertTime(), learnStarted);
			} else if ("claim".equalsIgnoreCase(sqsDsrProgress.getServiceName())) {
				claimFailed = nullIfBeforeIgnoreNull(sqsDsrProgress.getInsertTime(), claimStarted);
			}
		}
	}
	
//	private Date nullIfBefore(Date d1, Date d2) {
//		return null == d1 || null == d2 ? null : (d1.compareTo(d2) < 0 ? null : d1);
//	}
	
//	private Date nullIfBeforeIgnoreNull(Date d1, Date d2) {
//		return null == d1 ? null : (null == d2 ? d1 : (d1.compareTo(d2) < 0 ? null : d1));
//	}
	
	private static final long TIMEOUT = TimeUnit.MINUTES.toMillis(1);
	
	private Date nullIfBefore(Date d1, Date d2) {
		return null == d1 || null == d2 ? null : (d1.getTime() + TIMEOUT < d2.getTime() ? null : d1);
	}

	private Date nullIfBeforeIgnoreNull(Date d1, Date d2) {
		return null == d1 ? null : (null == d2 ? d1 : (d1.getTime() + TIMEOUT < d2.getTime() ? null : d1));
	}

	public String getIdDsr() {
		return idDsr;
	}
	public void setIdDsr(String idDsr) {
		this.idDsr = idDsr;
	}
	public Date getExtractQueued() {
		return extractQueued;
	}
	public void setExtractQueued(Date extractQueued) {
		this.extractQueued = extractQueued;
	}
	public Date getExtractStarted() {
		return extractStarted;
	}
	public void setExtractStarted(Date extractStarted) {
		this.extractStarted = extractStarted;
	}
	public Date getExtractCompleted() {
		return extractCompleted;
	}
	public void setExtractCompleted(Date extractCompleted) {
		this.extractCompleted = extractCompleted;
	}
	public Date getExtractFailed() {
		return extractFailed;
	}
	public void setExtractFailed(Date extractFailed) {
		this.extractFailed = extractFailed;
	}
	public Date getCleanQueued() {
		return cleanQueued;
	}
	public void setCleanQueued(Date cleanQueued) {
		this.cleanQueued = cleanQueued;
	}
	public Date getCleanStarted() {
		return cleanStarted;
	}
	public void setCleanStarted(Date cleanStarted) {
		this.cleanStarted = cleanStarted;
	}
	public Date getCleanCompleted() {
		return cleanCompleted;
	}
	public void setCleanCompleted(Date cleanCompleted) {
		this.cleanCompleted = cleanCompleted;
	}
	public Date getCleanFailed() {
		return cleanFailed;
	}
	public void setCleanFailed(Date cleanFailed) {
		this.cleanFailed = cleanFailed;
	}
	public Date getPriceQueued() {
		return priceQueued;
	}
	public void setPriceQueued(Date priceQueued) {
		this.priceQueued = priceQueued;
	}
	public Date getPriceStarted() {
		return priceStarted;
	}
	public void setPriceStarted(Date priceStarted) {
		this.priceStarted = priceStarted;
	}
	public Date getPriceCompleted() {
		return priceCompleted;
	}
	public void setPriceCompleted(Date priceCompleted) {
		this.priceCompleted = priceCompleted;
	}
	public Date getPriceFailed() {
		return priceFailed;
	}
	public void setPriceFailed(Date priceFailed) {
		this.priceFailed = priceFailed;
	}
	public Date getHypercubeQueued() {
		return hypercubeQueued;
	}
	public void setHypercubeQueued(Date hypercubeQueued) {
		this.hypercubeQueued = hypercubeQueued;
	}
	public Date getHypercubeStarted() {
		return hypercubeStarted;
	}
	public void setHypercubeStarted(Date hypercubeStarted) {
		this.hypercubeStarted = hypercubeStarted;
	}
	public Date getHypercubeCompleted() {
		return hypercubeCompleted;
	}
	public void setHypercubeCompleted(Date hypercubeCompleted) {
		this.hypercubeCompleted = hypercubeCompleted;
	}
	public Date getHypercubeFailed() {
		return hypercubeFailed;
	}
	public void setHypercubeFailed(Date hypercubeFailed) {
		this.hypercubeFailed = hypercubeFailed;
	}
	public Date getIdentifyQueued() {
		return identifyQueued;
	}
	public void setIdentifyQueued(Date identifyQueued) {
		this.identifyQueued = identifyQueued;
	}
	public Date getIdentifyStarted() {
		return identifyStarted;
	}
	public void setIdentifyStarted(Date identifyStarted) {
		this.identifyStarted = identifyStarted;
	}
	public Date getIdentifyCompleted() {
		return identifyCompleted;
	}
	public void setIdentifyCompleted(Date identifyCompleted) {
		this.identifyCompleted = identifyCompleted;
	}
	public Date getIdentifyFailed() {
		return identifyFailed;
	}
	public void setIdentifyFailed(Date identifyFailed) {
		this.identifyFailed = identifyFailed;
	}
	public Date getUniloadQueued() {
		return uniloadQueued;
	}
	public void setUniloadQueued(Date uniloadQueued) {
		this.uniloadQueued = uniloadQueued;
	}
	public Date getUniloadStarted() {
		return uniloadStarted;
	}
	public void setUniloadStarted(Date uniloadStarted) {
		this.uniloadStarted = uniloadStarted;
	}
	public Date getUniloadCompleted() {
		return uniloadCompleted;
	}
	public void setUniloadCompleted(Date uniloadCompleted) {
		this.uniloadCompleted = uniloadCompleted;
	}
	public Date getUniloadFailed() {
		return uniloadFailed;
	}
	public void setUniloadFailed(Date uniloadFailed) {
		this.uniloadFailed = uniloadFailed;
	}
	public Date getClaimQueued() {
		return claimQueued;
	}
	public void setClaimQueued(Date claimQueued) {
		this.claimQueued = claimQueued;
	}
	public Date getClaimStarted() {
		return claimStarted;
	}
	public void setClaimStarted(Date claimStarted) {
		this.claimStarted = claimStarted;
	}
	public Date getClaimCompleted() {
		return claimCompleted;
	}
	public void setClaimCompleted(Date claimCompleted) {
		this.claimCompleted = claimCompleted;
	}
	public Date getClaimFailed() {
		return claimFailed;
	}
	public void setClaimFailed(Date claimFailed) {
		this.claimFailed = claimFailed;
	}
	public Date getPrelearnQueued() {
		return prelearnQueued;
	}
	public void setPrelearnQueued(Date prelearnQueued) {
		this.prelearnQueued = prelearnQueued;
	}
	public Date getPrelearnStarted() {
		return prelearnStarted;
	}
	public void setPrelearnStarted(Date prelearnStarted) {
		this.prelearnStarted = prelearnStarted;
	}
	public Date getPrelearnCompleted() {
		return prelearnCompleted;
	}
	public void setPrelearnCompleted(Date prelearnCompleted) {
		this.prelearnCompleted = prelearnCompleted;
	}
	public Date getPrelearnFailed() {
		return prelearnFailed;
	}
	public void setPrelearnFailed(Date prelearnFailed) {
		this.prelearnFailed = prelearnFailed;
	}
	public Date getLearnQueued() {
		return learnQueued;
	}
	public void setLearnQueued(Date learnQueued) {
		this.learnQueued = learnQueued;
	}
	public Date getLearnStarted() {
		return learnStarted;
	}
	public void setLearnStarted(Date learnStarted) {
		this.learnStarted = learnStarted;
	}
	public Date getLearnCompleted() {
		return learnCompleted;
	}
	public void setLearnCompleted(Date learnCompleted) {
		this.learnCompleted = learnCompleted;
	}
	public Date getLearnFailed() {
		return learnFailed;
	}
	public void setLearnFailed(Date learnFailed) {
		this.learnFailed = learnFailed;
	}
	
	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}
	
}
