package com.alkemytech.sophia.codman.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.GsonBuilder;

@XmlRootElement
@Entity(name="RolePermission")
@Table(name="role_permission")
@NamedQueries({@NamedQuery(name="RolePermission.GetAll", query="SELECT x FROM RolePermission x")})
public class RolePermission {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id", nullable=false)
	private Integer id;

	@Column(name="role", nullable=false)
	private String role;
	
	@Column(name="permission", nullable=false)
	private String permission;

	public RolePermission() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getPermission() {
		return permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}
	
	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting().create().toJson(this);
	}

}
