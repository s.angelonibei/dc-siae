package com.alkemytech.sophia.codman.rest.performing.serviceimpl;

import java.util.List;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.codman.entity.performing.CampionamentoConfig;
import com.alkemytech.sophia.codman.entity.performing.TracciamentoApplicativo;
import com.alkemytech.sophia.codman.entity.performing.dao.ICampionamentoDAO;
import com.alkemytech.sophia.codman.entity.performing.security.ReportPage;
import com.alkemytech.sophia.codman.rest.performing.service.ICampionamentoService;
import com.alkemytech.sophia.codman.utils.DateUtils;
import com.google.inject.Inject;
public class CampionamentoService implements ICampionamentoService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private ICampionamentoDAO campionamentoDAO;

	@Inject
	public CampionamentoService(ICampionamentoDAO campionamentoDAO){
		this.campionamentoDAO=campionamentoDAO;
	}
	
	public void insertParametriCampionamento(CampionamentoConfig campionamentoCnfg) {
		campionamentoDAO.insertParametriCampionamento(campionamentoCnfg);

	}

	public ReportPage getCampionamenti(Long contabilitaIniziale, Long contabilitaFinale, Integer page) {
		return campionamentoDAO.getCampionamenti(contabilitaIniziale, contabilitaFinale, page);
	}
	
	public ReportPage getCampionamentiEsecuzione(Long contabilitaIniziale, Long contabilitaFinale, Integer page) {
		return campionamentoDAO.getCampionamentiEsecuzione(contabilitaIniziale, contabilitaFinale, page);
	}

	
	public CampionamentoConfig getCampionamentoConfigById(Long idCampionamentoConfig) {
		return campionamentoDAO.getCampionamentoConfigById(idCampionamentoConfig);
	}

	public ReportPage getStoricoEsecuzioni(Long periodoIniziale, Long periodoFinale, Integer page) {

		TracciamentoApplicativo tracciamento = null;

		int recordCount = 0;
		long dateDiff = -1;
		long currentDateDiff = -1;

		// Parametri dell'esecuzione
		List<TracciamentoApplicativo> esecuzioniEngine = campionamentoDAO
				.getEsecuzioniCampionamentoEngine(periodoIniziale, periodoFinale);

		// Utenza dell'esecuzione
		List<TracciamentoApplicativo> esecuzioniStart = campionamentoDAO
				.getEsecuzioniCampionamentoStart(periodoIniziale, periodoFinale);

		// Cerca gli start equivalenti per acquisire l'utente che ha lanciato il motore
		for (int i = 0; i < esecuzioniEngine.size(); i++) {
			dateDiff = -1;
			tracciamento = null;

			for (int j = 0; j < esecuzioniStart.size(); j++) {

				currentDateDiff = Math.abs(esecuzioniEngine.get(i).getDataInserimento().getTime()
						- esecuzioniStart.get(j).getDataInserimento().getTime());

				if (currentDateDiff < dateDiff || dateDiff < 0) {
					dateDiff = currentDateDiff;
					tracciamento = esecuzioniStart.get(j);
				}
			}

			if (tracciamento != null) {
				esecuzioniEngine.get(i).setUserName(tracciamento.getUserName());
			}
		}

		ReportPage reportPage = new ReportPage(esecuzioniEngine, esecuzioniEngine.size(), page);

		if (esecuzioniEngine != null && esecuzioniEngine.size() > 0) {

			recordCount = esecuzioniEngine.size();

			int startRecord = (page - 1) * ReportPage.NUMBER_RECORD_PER_PAGE;
			int endRecord = 0;
			if (recordCount > ReportPage.NUMBER_RECORD_PER_PAGE) {
				endRecord = startRecord + ReportPage.NUMBER_RECORD_PER_PAGE;
				if (endRecord > recordCount)
					endRecord = recordCount - 1;
			} else {
				endRecord = recordCount;
			}

			reportPage = new ReportPage(esecuzioniEngine, recordCount, page);

		}

		return reportPage;
	}

}
