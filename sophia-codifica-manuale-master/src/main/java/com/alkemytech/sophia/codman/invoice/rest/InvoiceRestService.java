package com.alkemytech.sophia.codman.invoice.rest;

import com.alkemytech.sophia.codman.dto.*;
import com.alkemytech.sophia.codman.entity.CCIDMetadata;
import com.alkemytech.sophia.codman.entity.performing.security.ReportPage;
import com.alkemytech.sophia.codman.invoice.repository.*;
import com.alkemytech.sophia.codman.invoice.service.InvoiceServiceInterface;
import com.alkemytech.sophia.codman.rest.PagedResult;
import com.alkemytech.sophia.invoice.integration.model.InvoiceResponse;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Singleton
@Path("invoice")
public class InvoiceRestService {

	private final Gson gson;
	private final InvoiceServiceInterface invoiceService;
	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Inject
	protected InvoiceRestService(Gson gson, InvoiceServiceInterface invoiceService) {
		super();
		this.gson = gson;
		this.invoiceService = invoiceService;
	}

	@POST
	@Path("all/ccid")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response allCCID(AvailableCCIDSearchParameters dto) {
		List<InvoiceCCIDDTO> res = invoiceService.payableCCID(dto);
		final PagedResult pagedResult = new PagedResult();
		if (!res.isEmpty()) {
			int maxrows = ReportPage.NUMBER_RECORD_PER_PAGE;
			boolean hasNext = res.size() >= maxrows;
			pagedResult.setRows(res).setMaxrows(maxrows).setFirst(dto.getFirst())
					.setLast(hasNext ? dto.getLast() : dto.getFirst() + res.size()).setHasNext(hasNext)
					.setHasPrev(dto.getFirst() > 0);
		} else {
			pagedResult.setMaxrows(0).setFirst(0).setLast(0).setHasNext(false).setHasPrev(false);
		}
		return Response.ok(gson.toJson(pagedResult)).build();
	}

	@POST
	@Path("all/updateccid")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response updateCcid(UpdateCcidParameter dto) {

		// boolean res = invoiceService.updateCCid(dto);
		CCIDMetadata ccidMetadata = invoiceService.getCcidMetadata(dto);

		// if(ccidMetadata != null && ccidMetadata.getTotalValue() != null &&
		// (ccidMetadata.getTotalValue().compareTo( new BigDecimal(
		// dto.getInvoiceAmaunt() ) ) <= 0) ){
		// return Response.status(500).entity("{\"message\":\"Il nuovo Valore
		// fatturabile deve essere minore o uguale al valore corrente\"}").build();
		// }

		List<InvoiceItemToCcid> listJoinAnticipi = invoiceService.getInvoiceItemToCcid(ccidMetadata);
		if (listJoinAnticipi.size() != 0) {
			return Response.status(500).entity(
					"{\"message\":\"Il valore fatturabile non può essere aggiornato. Il CCID è collegato a uno o piu anticipi\"}")
					.build();
		}

		if (invoiceService.updateCCid(dto)) {
			return Response.ok(gson.toJson(true)).build();
		} else {
			return Response.ok(gson.toJson(new ArrayList<InvoiceCCIDDTO>())).build();
		}
	}

	@POST
	@Path("all/invoice")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response getInvoices(InvoiceSearchParameters searchParameters) {

		List<InvoiceDTO> res = invoiceService.searchInvoice(searchParameters);
		final PagedResult pagedResult = new PagedResult();
		if (!res.isEmpty()) {
			int maxrows = ReportPage.NUMBER_RECORD_PER_PAGE;
			boolean hasNext = res.size() > maxrows;
			pagedResult.setRows(hasNext ? res.subList(0, maxrows) : res).setMaxrows(maxrows)
					.setFirst(searchParameters.getFirst())
					.setLast(hasNext ? searchParameters.getLast() : searchParameters.getFirst() + res.size())
					.setHasNext(hasNext).setHasPrev(searchParameters.getFirst() > 0);
		} else {
			pagedResult.setMaxrows(0).setFirst(0).setLast(0).setHasNext(false).setHasPrev(false);
		}
		return Response.ok(gson.toJson(pagedResult)).build();

	}

	@GET
	@Path("configuration")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response getConfiguration() {

		try {
			InvoiceConfiguration result = invoiceService.getConfiguration();
			if (result != null) {
				return Response.ok(gson.toJson(result)).build();
			}
		} catch (Exception e) {
			logger.error("invoice configuration", e);
		}
		return Response.status(500).build();
	}

	@GET
	@Path("itemReason/all")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response getItemReasons() {

		try {
			List<InvoiceItemReasonDTO> result = invoiceService.getInvoiceItemReasons();
			if (result != null) {
				return Response.ok(gson.toJson(result)).build();
			}
		} catch (Exception e) {
			logger.error("list itemReason", e);
		}
		return Response.status(500).build();
	}

	@GET
	@Path("requestInvoice")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response requestInvoice(@DefaultValue("0") @QueryParam("invoiceId") int invoiceId) {
		InvoiceResponse response = invoiceService.callInvoiceService(invoiceId);
		if (response != null) {
			return Response.ok(gson.toJson(response)).build();
		}

		return Response.status(500).build();
	}

	@POST
	@Path("saveInvoiceDraft")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response saveInvoiceDraft(InvoiceDraftDto dto) {
		InvoiceDTO result = invoiceService.newInvoice(dto);
		if (result != null) {
			return Response.ok(gson.toJson(result)).build();
		}

		return Response.status(500).build();

	}

	@DELETE
	@Path("deleteInvoice")
	public Response delete(InvoiceDTO invoiceDTO) {
		try {
			invoiceService.removeInvoice(invoiceDTO.getIdInvoice());
			return Response.status(200).build();
		} catch (Exception e) {
			return Response.status(500).build();
		}
	}

	@DELETE
	@Path("")
	public Response deleteInvoice(InvoiceDTO invoiceDTO) {
		try {
			invoiceService.removeInvoiceBozza(invoiceDTO.getIdInvoice());
			return Response.status(200).build();
		} catch (Exception e) {
			return Response.status(500).build();
		}
	}

	@GET
	@Path("all/items")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response getItems(@DefaultValue("0") @QueryParam("invoiceId") Integer invoiceId) {
		try {
			List<InvoiceItemDto> result = invoiceService.getInvoiceItems(invoiceId);
			if (result != null) {
				return Response.ok(gson.toJson(result)).build();
			}
		} catch (Exception e) {
			logger.error("list invoice items", e);
		}
		return Response.status(500).build();
	}

	@GET
	@Path("clientData")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response clientData(@DefaultValue("") @QueryParam("dsp") String dsp) {
		try {
			AnagClientDTO result = invoiceService.getAnagClient(dsp);
			if (result != null) {
				return Response.ok(gson.toJson(result)).build();
			}
		} catch (Exception e) {
			logger.error("clientData", e);
		}
		return Response.status(500).build();
	}

	@GET
	@Path("")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response getInvoice(@DefaultValue("0") @QueryParam("invoiceId") Integer invoiceId) {
		try {
			InvoiceDTO result = invoiceService.getInvoice(invoiceId);
			if (result != null) {
				return Response.ok(gson.toJson(result)).build();
			}
		} catch (Exception e) {
			logger.error("getInvoice", e);
		}
		return Response.status(500).build();
	}

	@GET
	@Path("checkInvoice")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response checkInvoice(@DefaultValue("0") @QueryParam("invoiceId") Integer invoiceId) {
		try {
			InvoiceDTO result = invoiceService.checkInvoice(invoiceId);
			if (result != null) {
				return Response.ok(gson.toJson(result)).build();
			}
		} catch (Exception e) {
			logger.error("getInvoice", e);
		}
		return Response.status(500).build();
	}

	@GET
	@Path("download/{invoiceId}")
	@Produces("application/octet-stream")
	public Response downloadById(@PathParam("invoiceId") Integer invoiceId) {
		byte[] blob = invoiceService.downloadInvoice(invoiceId);
		if (blob != null) {
			return Response.ok().type("application/octet-stream").entity(blob).build();

		}
		return Response.status(500).build();
	}
}
