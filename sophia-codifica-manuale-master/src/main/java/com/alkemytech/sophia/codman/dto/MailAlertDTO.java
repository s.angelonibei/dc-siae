package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MailAlertDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private long id;

	private List<DspDTO> dsps;

	private boolean failedClaim;

	private boolean completedClaim;

	private boolean negativeClaim;

	private List<MailAddressDTO> addresses;
	
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public List<DspDTO> getDsps() {
		return dsps;
	}

	public void setDsps(List<DspDTO> dsps) {
		this.dsps = dsps;
	}

	public boolean isFailedClaim() {
		return failedClaim;
	}

	public void setFailedClaim(boolean failedClaim) {
		this.failedClaim = failedClaim;
	}

	public boolean isCompletedClaim() {
		return completedClaim;
	}

	public void setCompletedClaim(boolean completedClaim) {
		this.completedClaim = completedClaim;
	}

	public boolean isNegativeClaim() {
		return negativeClaim;
	}

	public void setNegativeClaim(boolean negativeClaim) {
		this.negativeClaim = negativeClaim;
	}

	public List<MailAddressDTO> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<MailAddressDTO> addresses) {
		this.addresses = addresses;
	}

}
