package com.alkemytech.sophia.codman.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="SOCIETA_TUTELA")
@NamedQueries({
	@NamedQuery(name = "SocietaTutela.findByCodice", query = "SELECT s FROM SocietaTutela s WHERE s.codice = :codice")
})
public class SocietaTutela implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	protected Integer id;
		
	@Column(name = "CODICE")
	private String codice;
	
	@Column(name = "NOMINATIVO")
	private String nominativo;
	
	@Column(name = "HOME_TERRITORY")
	private String homeTerritoryCode;
	
	@Column(name = "POSIZIONE_SIAE")
	private String posizioneSIAE;
	
	@Column(name = "CODICE_SIADA")
	private String codiceSIADA;
	
	@Column(name = "TENANT")
	private Integer tenant;

	public SocietaTutela() {
		super();
	}

	public SocietaTutela(String codice, String nominativo, String homeTerritoryCode, String posizioneSIAE, String codiceSIADA,
			Integer tenant) {
		super();
		this.codice = codice;
		this.nominativo = nominativo;
		this.homeTerritoryCode = homeTerritoryCode;
		this.posizioneSIAE = posizioneSIAE;
		this.codiceSIADA = codiceSIADA;
		this.tenant = tenant;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodice() {
		return codice;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	public String getNominativo() {
		return nominativo;
	}

	public void setNominativo(String nominativo) {
		this.nominativo = nominativo;
	}

	public String getHomeTerritoryCode() {
		return homeTerritoryCode;
	}

	public void setHomeTerritoryCode(String homeTerritoryCode) {
		this.homeTerritoryCode = homeTerritoryCode;
	}

	public String getPosizioneSIAE() {
		return posizioneSIAE;
	}

	public void setPosizioneSIAE(String posizioneSIAE) {
		this.posizioneSIAE = posizioneSIAE;
	}

	public String getCodiceSIADA() {
		return codiceSIADA;
	}

	public void setCodiceSIADA(String codiceSIADA) {
		this.codiceSIADA = codiceSIADA;
	}

	public Integer getTenant() {
		return tenant;
	}

	public void setTenant(Integer tenant) {
		this.tenant = tenant;
	}

}
