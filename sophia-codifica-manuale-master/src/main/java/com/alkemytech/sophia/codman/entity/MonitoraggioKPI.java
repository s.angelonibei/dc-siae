package com.alkemytech.sophia.codman.entity;

import com.alkemytech.sophia.broadcasting.dto.KPI;
import com.alkemytech.sophia.broadcasting.model.BdcBroadcasters;
import com.alkemytech.sophia.broadcasting.model.BdcCanali;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;

@XmlRootElement
public class MonitoraggioKPI {
    @NotNull
    private BdcBroadcasters emittente;
    @NotNull
    private BdcCanali canale;
    @NotNull
    @Min( value = 1900)
    private int anno;
    @NotNull
    private List<Short> mesi;

    private java.util.Date aggiornatoAl;

    private List<KPI> listaKPI;

    public BdcBroadcasters getEmittente() {
        return emittente;
    }

    public void setEmittente(BdcBroadcasters emittente) {
        this.emittente = emittente;
    }

    public BdcCanali getCanale() {
        return canale;
    }

    public void setCanale(BdcCanali canale) {
        this.canale = canale;
    }

    public int getAnno() {
        return anno;
    }

    public void setAnno(int anno) {
        this.anno = anno;
    }

    public List<Short> getMesi() {
        return mesi;
    }

    public void setMesi(List<Short> mesi) {
        this.mesi = mesi;
    }

    public Date getAggiornatoAl() {
        return aggiornatoAl;
    }

    public void setAggiornatoAl(Date aggiornatoAl) {
        this.aggiornatoAl = aggiornatoAl;
    }

    public List<KPI> getListaKPI() {
        return listaKPI;
    }

    public void setListaKPI(List<KPI> listaKPI) {
        this.listaKPI = listaKPI;
    }
}
