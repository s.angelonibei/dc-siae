package com.alkemytech.sophia.codman.service.dsrmetadata.period;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DSRPeriodExtractorYYYYMMv3 implements DsrPeriodExtractor{
	
	private String regex = "[0-9]{4}_[0-9]{2}";
	private String code = "YYYY_MM";
	private String description = "YYYY_MM" ;
	
	@Override
	public DsrPeriod extractDsrPeriod(String periodString) {
				
		try {
			String year = periodString.substring(0,4);
			String month = periodString.substring(5,7);
			
			DsrPeriod dsrPeriod = new DsrPeriod();
			dsrPeriod.setPeriodType(DsrPeriod.MENSILE);
			dsrPeriod.setYear(year);
			dsrPeriod.setPeriod(month);
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy_MM");
			
			String dateToParse  = periodString + "_01";
			
			Date periodStartDate = dateFormat.parse(dateToParse);

			dsrPeriod.setPeriodStartDate(periodStartDate);
			
			Calendar c = Calendar.getInstance();
			c.setTime(periodStartDate);
			c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
			
			Date periodEndDate = c.getTime();
			dsrPeriod.setPeriodEndDate(periodEndDate);
			
			return dsrPeriod;
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}

	@Override
	public String getRegex() {
		return regex;
	}

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public String getDescription() {
		return description;
	}

}
