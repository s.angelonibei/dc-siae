package com.alkemytech.sophia.codman.rest;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.gson.JsonArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.codman.dto.DspDTO;
import com.alkemytech.sophia.codman.entity.AnagDsp;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

@Singleton
@Path("anagDsp")
public class AnagDspService {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Provider<EntityManager> provider;
	private final Gson gson;

	@Inject
	protected AnagDspService(@McmdbDataSource Provider<EntityManager> provider, Gson gson) {
		super();
		this.provider = provider;
		this.gson = gson;
	}

	@GET
	@Path("search")
	@Produces("application/json")
	public Response search(@QueryParam("name") String name) {
		EntityManager entityManager = provider.get();
		List<AnagDsp> resultList = entityManager.createQuery("select x from AnagDsp x where lower(x.name) like lower(:name)  order by x.name", AnagDsp.class)
				.setParameter("name",  name + "%")
				.getResultList();

		return Response.ok(gson.toJson(resultList)).build();
	}

	@GET
	@Path("all")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response all(@DefaultValue("0") @QueryParam("first") int first,
			@DefaultValue("50") @QueryParam("last") int last) {
		EntityManager entityManager = null;
		List<AnagDsp> result;
		try {
			entityManager = provider.get();
			final Query q = entityManager.createQuery("select x from AnagDsp x order by x.name").setFirstResult(first) // offset
					.setMaxResults(1 + last - first);
			;
			result = (List<AnagDsp>) q.getResultList();

			final PagedResult pagedResult = new PagedResult();
			if (null != result) {
				final int maxrows = last - first;
				final boolean hasNext = result.size() > maxrows;
				pagedResult.setRows((hasNext ? result.subList(0, maxrows) : result)).setMaxrows(maxrows).setFirst(first)
						.setLast(hasNext ? last : first + result.size()).setHasNext(hasNext).setHasPrev(first > 0);
			} else {
				pagedResult.setMaxrows(0).setFirst(0).setLast(0).setHasNext(false).setHasPrev(false);
			}

			return Response.ok(gson.toJson(pagedResult)).build();
		} catch (Exception e) {
			logger.error("all", e);
		}
		return Response.status(500).build();
	}

	@POST
	@Path("")
	public Response add(DspDTO config) {

		Status status = Status.INTERNAL_SERVER_ERROR;
		final EntityManager entityManager = provider.get();
		AnagDsp dspEntity = new AnagDsp();
		dspEntity.setIdDsp(config.getIdDsp());
		dspEntity.setCode(config.getCode());
		dspEntity.setName(config.getName());
		dspEntity.setDescription(config.getDescription());
		dspEntity.setFtpSourcePath(config.getFtpSourcePath());
		try {
			entityManager.getTransaction().begin();
			entityManager.persist(dspEntity);
			entityManager.getTransaction().commit();

			return Response.ok().build();
		} catch (Exception e) {
			logger.error("add", e);

			if (e.getCause() != null && e.getCause().getCause() != null
					&& e.getCause().getCause() instanceof MySQLIntegrityConstraintViolationException) {
				status = Status.CONFLICT;
			}

			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}

		}

		return Response.status(status).build();
	}

	@PUT
	@Path("")
	public Response update(@DefaultValue("") @QueryParam("prevcode") String prevCode,
			@DefaultValue("") @QueryParam("prevname") String prevName,
			@DefaultValue("") @QueryParam("prevdescription") String prevDescription,
			@DefaultValue("") @QueryParam("prevftpSourcePath") String prevFtpSourcePath,
			@DefaultValue("") @QueryParam("idDsp") String idDsp, 
			@DefaultValue("") @QueryParam("code") String code,
			@DefaultValue("") @QueryParam("name") String name,
			@DefaultValue("") @QueryParam("description") String description,
			@DefaultValue("") @QueryParam("ftpSourcePath") String ftpSourcePath) {

		if (prevCode.equals(code) && prevName.equals(name) && prevDescription.equals(description)
				&& prevFtpSourcePath.equals(ftpSourcePath)) {
			return Response.ok().build();
		}

		final EntityManager entityManager = provider.get();
		try {
			entityManager.getTransaction().begin();

			final Query query = entityManager
					.createQuery("update AnagDsp x set " 
							+ (!prevCode.equals(code) ? " x.code = :code " : "")
							+ (!prevCode.equals(code) && !prevName.equals(name) ? " , " : "")
							+ (!prevName.equals(name) ? " x.name = :name " : "")
							+ ((!prevCode.equals(code) || !prevName.equals(name)) 
									&& !prevDescription.equals(description) ? " , " : "")
							+ (!prevDescription.equals(description) ? " x.description = :description " : "")
							+ ((!prevCode.equals(code) || !prevName.equals(name) ||
									!prevDescription.equals(description)) &&  !prevFtpSourcePath.equals(ftpSourcePath) ? " , ": "")
							+ (!prevFtpSourcePath.equals(ftpSourcePath) ? " x.ftpSourcePath = :ftpSourcePath " : "")
							+ "  where x.idDsp = :idDsp ");

			query.setParameter("idDsp", idDsp);

			if (!prevCode.equals(code)) {
				query.setParameter("code", code);
			}
			if (!prevName.equals(name)) {
				query.setParameter("name", name);
			}
			if (!prevDescription.equals(description)) {
				query.setParameter("description", description);
			}
			if (!prevFtpSourcePath.equals(ftpSourcePath)) {
				query.setParameter("ftpSourcePath", ftpSourcePath);
			}
			query.executeUpdate();
			entityManager.getTransaction().commit();
			return Response.ok().build();
		} catch (Exception e) {
			logger.error("update", e);
			entityManager.getTransaction().rollback();
		}
		return Response.status(500).build();
	}

	@DELETE
	@Path("")
	public Response delete(DspDTO config) {
		final EntityManager entityManager = provider.get();
		try {
			entityManager.getTransaction().begin();
			final Query query = entityManager.createQuery("delete from AnagDsp x where x.idDsp = :idDsp ");
			query.setParameter("idDsp", config.getIdDsp());
			query.executeUpdate();
			entityManager.getTransaction().commit();
			return Response.ok().build();
		} catch (Exception e) {
			logger.error("delete", e);
			entityManager.getTransaction().rollback();
		}
		return Response.status(500).build();
	}

}
