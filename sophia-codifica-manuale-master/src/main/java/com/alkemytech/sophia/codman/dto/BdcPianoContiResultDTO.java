package com.alkemytech.sophia.codman.dto;

import java.util.Date;
import java.util.List;

import com.alkemytech.sophia.broadcasting.model.BdcConto;
import com.alkemytech.sophia.broadcasting.model.BdcDettaglioPianoDeiConti;
import com.google.gson.annotations.Expose;

public class BdcPianoContiResultDTO {
	@Expose
	private List<BdcDettaglioPianoDeiConti> dettaglioPianoDeiConti;
	@Expose
	private Long idBdcPianoDeiConti;
	@Expose
	private Date dataCreazione;
	@Expose
	private Boolean attivo;
	@Expose
	private String utente;
	@Expose
	private String descrizione;
	@Expose
	private BdcConto codiceConto;

	public List<BdcDettaglioPianoDeiConti> getDettaglioPianoDeiConti() {
		return dettaglioPianoDeiConti;
	}

	public void setDettaglioPianoDeiConti(List<BdcDettaglioPianoDeiConti> dettaglioPianoDeiConti) {
		this.dettaglioPianoDeiConti = dettaglioPianoDeiConti;
	}

	public Long getIdBdcPianoDeiConti() {
		return idBdcPianoDeiConti;
	}

	public void setIdBdcPianoDeiConti(Long idBdcPianoDeiConti) {
		this.idBdcPianoDeiConti = idBdcPianoDeiConti;
	}

	public Date getDataCreazione() {
		return dataCreazione;
	}

	public void setDataCreazione(Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}

	public Boolean getAttivo() {
		return attivo;
	}

	public void setAttivo(Boolean attivo) {
		this.attivo = attivo;
	}

	public String getUtente() {
		return utente;
	}

	public void setUtente(String utente) {
		this.utente = utente;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public BdcConto getCodiceConto() {
		return codiceConto;
	}

	public void setCodiceConto(BdcConto codiceConto) {
		this.codiceConto = codiceConto;
	}

}
