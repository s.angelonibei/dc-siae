package com.alkemytech.sophia.codman.entity.performing;

import com.alkemytech.sophia.codman.performing.dto.ScodificaResponseDTO;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PERF_CODIFICA database table.
 *
 */
@Entity
@Table(name="PERF_CODIFICA")
@NamedQuery(name="PerfCodifica.findAll", query="SELECT p FROM PerfCodifica p")
@SqlResultSetMappings({
		@SqlResultSetMapping(
				name = "ListaScodificaMapping",
				classes = {
						@ConstructorResult(targetClass = ScodificaResponseDTO.class,
								columns = {
										@ColumnResult(name = "idCombana", type = Long.class),
										@ColumnResult(name = "idCodifica", type = Long.class),
										@ColumnResult(name = "codiceCombana", type = String.class),
										@ColumnResult(name = "titolo", type = String.class),
										@ColumnResult(name = "compositori", type = String.class),
										@ColumnResult(name = "autori", type = String.class),
										@ColumnResult(name = "interpreti", type = String.class),
										@ColumnResult(name = "codiceOperaApprovato", type = String.class),
										@ColumnResult(name = "valoreEconomico", type = BigDecimal.class),
										@ColumnResult(name = "nUtilizzazioni", type = BigDecimal.class),
								}
						)
				}),
		@SqlResultSetMapping(
				name = "DetailScodificaMapping",
				classes = {
						@ConstructorResult(targetClass = ScodificaResponseDTO.class,
								columns = {
										@ColumnResult(name = "dataCreazione", type = Date.class),
										@ColumnResult(name = "dataCodifica", type = Date.class),
										@ColumnResult(name = "dataApprovazione", type = Date.class),
										@ColumnResult(name = "descrizione", type = String.class),
										@ColumnResult(name = "tipoApprovazione", type = String.class),
										@ColumnResult(name = "utenteCodifica", type = String.class),
										@ColumnResult(name = "tipoAzione", type = String.class),
										@ColumnResult(name = "confidenza", type = BigDecimal.class),
								}
						)
				})
})
public class PerfCodifica implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID_CODIFICA", unique=true, nullable=false)
	private Long idCodifica;

	@Column(name="CANDIDATE", nullable=false)
	private String candidate;

	@Column(name="CODICE_OPERA_APPROVATO", length=45)
	private String codiceOperaApprovato;

	@Column(name="CODICE_OPERA_SUGGERITO", nullable=false, length=20)
	private String codiceOperaSuggerito;

	@Column(name="CONFIDENZA", nullable=false, precision=10, scale=7)
	private BigDecimal confidenza;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_APPROVAZIONE")
	private Date dataApprovazione;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_CODIFICA", nullable=false)
	private Date dataCodifica;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_FINE_VALIDITA")
	private Date dataFineValidita;

	@Column(name="DIST_SEC_CANDIDATA", nullable=false, precision=10, scale=7)
	private BigDecimal distSecCandidata;

	@Column(name="N_UTILIZZAZIONI")
	private BigInteger nUtilizzazioni;

	@Column(name="PREZIOSA")
	private Boolean preziosa;

	@Column(name="STATO_APPROVAZIONE", length=1)
	private String statoApprovazione;

	@Column(name="TIPO_APPROVAZIONE", length=1)
	private String tipoApprovazione;

	@Column(name="VALORE_ECONOMICO", precision=10, scale=7)
	private BigDecimal valoreEconomico;

	//bi-directional many-to-one association to PerfUtilizzazione
	@JsonManagedReference
	@OneToMany(mappedBy="perfCodifica")
	private List<PerfUtilizzazione> perfUtilizzaziones;

	//bi-directional many-to-one association to PerfUtilizzazioniAssegnate
	@JsonManagedReference
	@OneToMany(mappedBy="perfCodifica")
	private List<PerfUtilizzazioniAssegnate> perfUtilizzazioniAssegnates;

	//bi-directional many-to-one association to PerfCombana
	@JsonBackReference
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_COMBANA", nullable=false)
	private PerfCombana perfCombana;

	public PerfCodifica() {
	}

	public Long getIdCodifica() {
		return this.idCodifica;
	}

	public void setIdCodifica(Long idCodifica) {
		this.idCodifica = idCodifica;
	}

	public String getCandidate() {
		return this.candidate;
	}

	public void setCandidate(String candidate) {
		this.candidate = candidate;
	}

	public String getCodiceOperaApprovato() {
		return this.codiceOperaApprovato;
	}

	public void setCodiceOperaApprovato(String codiceOperaApprovato) {
		this.codiceOperaApprovato = codiceOperaApprovato;
	}

	public String getCodiceOperaSuggerito() {
		return this.codiceOperaSuggerito;
	}

	public void setCodiceOperaSuggerito(String codiceOperaSuggerito) {
		this.codiceOperaSuggerito = codiceOperaSuggerito;
	}

	public BigDecimal getConfidenza() {
		return this.confidenza;
	}

	public void setConfidenza(BigDecimal confidenza) {
		this.confidenza = confidenza;
	}

	public Date getDataApprovazione() {
		return this.dataApprovazione;
	}

	public void setDataApprovazione(Date dataApprovazione) {
		this.dataApprovazione = dataApprovazione;
	}

	public Date getDataCodifica() {
		return this.dataCodifica;
	}

	public void setDataCodifica(Date dataCodifica) {
		this.dataCodifica = dataCodifica;
	}

	public Date getDataFineValidita() {
		return this.dataFineValidita;
	}

	public void setDataFineValidita(Date dataFineValidita) {
		this.dataFineValidita = dataFineValidita;
	}

	public BigDecimal getDistSecCandidata() {
		return this.distSecCandidata;
	}

	public void setDistSecCandidata(BigDecimal distSecCandidata) {
		this.distSecCandidata = distSecCandidata;
	}

	public BigInteger getNUtilizzazioni() {
		return this.nUtilizzazioni;
	}

	public void setNUtilizzazioni(BigInteger nUtilizzazioni) {
		this.nUtilizzazioni = nUtilizzazioni;
	}

	public Boolean getPreziosa() {
		return this.preziosa;
	}

	public void setPreziosa(Boolean preziosa) {
		this.preziosa = preziosa;
	}

	public String getStatoApprovazione() {
		return this.statoApprovazione;
	}

	public void setStatoApprovazione(String statoApprovazione) {
		this.statoApprovazione = statoApprovazione;
	}

	public String getTipoApprovazione() {
		return this.tipoApprovazione;
	}

	public void setTipoApprovazione(String tipoApprovazione) {
		this.tipoApprovazione = tipoApprovazione;
	}

	public BigDecimal getValoreEconomico() {
		return this.valoreEconomico;
	}

	public void setValoreEconomico(BigDecimal valoreEconomico) {
		this.valoreEconomico = valoreEconomico;
	}

	public List<PerfUtilizzazione> getPerfUtilizzaziones() {
		return this.perfUtilizzaziones;
	}

	public void setPerfUtilizzaziones(List<PerfUtilizzazione> perfUtilizzaziones) {
		this.perfUtilizzaziones = perfUtilizzaziones;
	}

	public PerfUtilizzazione addPerfUtilizzazione(PerfUtilizzazione perfUtilizzazione) {
		getPerfUtilizzaziones().add(perfUtilizzazione);
		perfUtilizzazione.setPerfCodifica(this);

		return perfUtilizzazione;
	}

	public PerfUtilizzazione removePerfUtilizzazione(PerfUtilizzazione perfUtilizzazione) {
		getPerfUtilizzaziones().remove(perfUtilizzazione);
		perfUtilizzazione.setPerfCodifica(null);

		return perfUtilizzazione;
	}

	public List<PerfUtilizzazioniAssegnate> getPerfUtilizzazioniAssegnates() {
		return this.perfUtilizzazioniAssegnates;
	}

	public void setPerfUtilizzazioniAssegnates(List<PerfUtilizzazioniAssegnate> perfUtilizzazioniAssegnates) {
		this.perfUtilizzazioniAssegnates = perfUtilizzazioniAssegnates;
	}

	public PerfUtilizzazioniAssegnate addPerfUtilizzazioniAssegnate(PerfUtilizzazioniAssegnate perfUtilizzazioniAssegnate) {
		getPerfUtilizzazioniAssegnates().add(perfUtilizzazioniAssegnate);
		perfUtilizzazioniAssegnate.setPerfCodifica(this);

		return perfUtilizzazioniAssegnate;
	}

	public PerfUtilizzazioniAssegnate removePerfUtilizzazioniAssegnate(PerfUtilizzazioniAssegnate perfUtilizzazioniAssegnate) {
		getPerfUtilizzazioniAssegnates().remove(perfUtilizzazioniAssegnate);
		perfUtilizzazioniAssegnate.setPerfCodifica(null);

		return perfUtilizzazioniAssegnate;
	}

	public PerfCombana getPerfCombana() {
		return this.perfCombana;
	}

	public void setPerfCombana(PerfCombana perfCombana) {
		this.perfCombana = perfCombana;
	}

}