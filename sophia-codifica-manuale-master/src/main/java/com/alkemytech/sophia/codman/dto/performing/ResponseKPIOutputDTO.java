package com.alkemytech.sophia.codman.dto.performing;

import com.alkemytech.sophia.broadcasting.dto.KPI;

import java.util.Date;
import java.util.List;

public class ResponseKPIOutputDTO {
    private Date lastUpdate;
    private int kpiAttesi;
    private List<KPI> listaKPI;
    private List<MusicProviderInformationFileDTO> fileDaElaborare;

    public ResponseKPIOutputDTO(Date lastUpdate, int kpiAttesi, List<KPI> listaKPI, List<MusicProviderInformationFileDTO> fileDaElaborare) {
        this.lastUpdate = lastUpdate;
        this.kpiAttesi = kpiAttesi;
        this.listaKPI = listaKPI;
        this.fileDaElaborare = fileDaElaborare;
    }

    public ResponseKPIOutputDTO() {
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public int getKpiAttesi() {
        return kpiAttesi;
    }

    public void setKpiAttesi(int kpiAttesi) {
        this.kpiAttesi = kpiAttesi;
    }

    public List<KPI> getListaKPI() {
        return listaKPI;
    }

    public void setListaKPI(List<KPI> listaKPI) {
        this.listaKPI = listaKPI;
    }

    public List<MusicProviderInformationFileDTO> getFileDaElaborare() {
        return fileDaElaborare;
    }

    public void setFileDaElaborare(List<MusicProviderInformationFileDTO> fileDaElaborare) {
        this.fileDaElaborare = fileDaElaborare;
    }
}
