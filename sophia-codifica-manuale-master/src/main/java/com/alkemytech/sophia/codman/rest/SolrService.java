package com.alkemytech.sophia.codman.rest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.alkemytech.sophia.kb.KbClientImpl;
import com.alkemytech.sophia.solr.SearchResult;
import com.alkemytech.sophia.solr.SolrClient;
import com.alkemytech.sophia.solr.SolrClientFactory;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

@Singleton
@Path("solr")
public class SolrService {

	private final KbClientImpl kbClient;
	//private final SolrClient solr;
	private final Gson gson;
	private final int maxResults;
	
	@Inject
	protected SolrService(Gson gson,
			@Named("ricerca.opere.service.mt.url") String urlKb,
			@Named("codifica.pesoTitolo") double pesoCampoTitolo,
			@Named("codifica.pesoCompositore") double pesoCampoCompositore,
			@Named("codifica.pesoAutore") double pesoCampoAutore,
			@Named("codifica.pesoInterprete") double pesoCampoInterprete,
			@Named("codifica.maxResults") int maxResults,
			@Named("codifica.discardConfidenzaMinoreDi") double discardConfidenzaMinoreDi,
			@Named("codifica.includeRilevanzaMaggioreDi") double includeRilevanzaMaggioreDi,
			@Named("codifica.includeScoreMaggioreDi") double includeScoreMaggioreDi,
			@Named("codifica.nazionalita") String nazionalita,
			@Named("codifica.tipologiaArtista") String tipologiaArtista,
			@Named("codifica.minGradoConfidenzaRicercaEsatta") double minGradoConfidenzaRicercaEsatta,
			@Named("solr.server.collection") String solrCollection,
			@Named("solr.server.zkHost") String zkHosts,
			@Named("solr.client.zkClientTimeout") int zkClientTimeout,
			@Named("solr.client.zkConnectTimeout") int zkConnectTimeout,
			@Named("solr.client.httpMaxConnections") int httpMaxConnections,
			@Named("solr.highlight.prefix") String highlightPrefix,
			@Named("solr.highlight.postfix") String highlightPostfix,
			@Named("solr.highlight.snippets") int highlightSnippets) {
		super();
//		this.solr = SolrClientFactory.getInstance(pesoCampoTitolo, pesoCampoCompositore, pesoCampoAutore, 
//				pesoCampoInterprete, maxResults, discardConfidenzaMinoreDi, includeRilevanzaMaggioreDi, includeScoreMaggioreDi,
//				nazionalita, tipologiaArtista, minGradoConfidenzaRicercaEsatta, solrCollection, zkHosts, zkClientTimeout,
//				zkConnectTimeout, httpMaxConnections, highlightPrefix, highlightPostfix, highlightSnippets);
		this.gson = gson;
		this.maxResults = maxResults;
		this.kbClient = new KbClientImpl(httpMaxConnections,urlKb);
	}

	@GET
	@Path("search")
	@Produces("application/json")
	public Response search(@QueryParam("title") String title, @QueryParam("artists") String artists, @QueryParam("roles") String roles) {
		final List<SearchResult> results = kbClient.searchOpereCodifica(title, artists, roles);
//		if (null != nofuzzy && !nofuzzy.isEmpty()) {
//			results.addAll(nofuzzy);
//		}
//		if (null == nofuzzy || nofuzzy.isEmpty() || !solr.checkGradoConfidenzaMinimo(nofuzzy)) {
//			final List<SearchResult> fuzzy = solr.searchOpereCodificaFuzzy(title, artists, roles);
//			if (null != fuzzy && !fuzzy.isEmpty()) {
//				fuzzy.removeAll(nofuzzy);
//				results.addAll(fuzzy);
//			}
//		}
//		Collections.sort(results, new Comparator<SearchResult>() {
//			@Override
//			public int compare(SearchResult o1, SearchResult o2) {
//				return - Double.compare(o1.getConfidenza(), o2.getConfidenza()); // descending order
//			}
//		});
		return Response.ok(gson.toJson(results.size() > maxResults ? results.subList(0, maxResults) : results)).build();
	}

}
