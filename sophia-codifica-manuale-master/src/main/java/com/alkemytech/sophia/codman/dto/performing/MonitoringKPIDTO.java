package com.alkemytech.sophia.codman.dto.performing;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class MonitoringKPIDTO {
	
private String periodo;
	
	private Date competenzaDa;

	private Date competenzaA;

	private List<RecordMonitoringKPIDTO> RecordMonitoringKPIDTO;
}
