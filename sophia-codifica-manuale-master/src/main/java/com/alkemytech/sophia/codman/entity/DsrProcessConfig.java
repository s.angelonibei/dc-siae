package com.alkemytech.sophia.codman.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.codman.persistence.AbstractEntity;

@XmlRootElement
@Entity(name="DsrProcessConfig")
@Table(name="DSR_PROCESS_CONFIG")
@NamedQueries({@NamedQuery(name="DsrProcessConfig.GetAll", query="SELECT dpc FROM DsrProcessConfig dpc")})
@SuppressWarnings("serial")
public class DsrProcessConfig extends AbstractEntity<Long> {

	@Id
	@Column(name="ID_DSR_PROCESS_CONFIG", nullable=false)
	private Long idDsrProcessConfig;
	
	@Column(name="PRIORITY")
	private Long priority;
	
	@Column(name="DSP", nullable=false)
	private String dsp;
	
	@Column(name="UTILIZATION_TYPE", nullable=false)
	private String utilizationType;
	
	@Column(name="COUNTRY", nullable=false)
	private String country;
	
	@Column(name="ID_DSR_PROCESS", nullable=false)
	private String idDsrProcess;	
	
	
	@Override
	public Long getId() {
		return getIdDsrProcessConfig();
	}

	public Long getIdDsrProcessConfig() {
		return idDsrProcessConfig;
	}

	public void setIdDsrProcessConfig(Long idDsrProcessConfig) {
		this.idDsrProcessConfig = idDsrProcessConfig;
	}

	public Long getPriority() {
		return priority;
	}

	public void setPriority(Long priority) {
		this.priority = priority;
	}

	public String getDsp() {
		return dsp;
	}

	public void setDsp(String dsp) {
		this.dsp = dsp;
	}

	public String getUtilizationType() {
		return utilizationType;
	}

	public void setUtilizationType(String utilizationType) {
		this.utilizationType = utilizationType;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getIdDsrProcess() {
		return idDsrProcess;
	}

	public void setIdDsrProcess(String idDsrProcess) {
		this.idDsrProcess = idDsrProcess;
	}
	
	
	
}
