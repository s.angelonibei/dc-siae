package com.alkemytech.sophia.codman.entity;

import com.alkemytech.sophia.codman.persistence.AbstractEntity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlRootElement
@Entity
@Table(name="MM_INGESTION_DOCUMENTAZIONE_MANDANTI")
public class IngestionDocumentazioneMandati extends AbstractEntity<Integer> {

	private static final long serialVersionUID = 1L;
	
    @Id
    @Column(name = "ID", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
    
    @Column(name = "DATA_INSERIMENTO", nullable = false)
	private Date dataInserimento;

	@Column(name = "STATO", nullable = false)
	private String stato;

	@Column(name = "SOCIETA", nullable = false)
	private String societa;

	@Column(name = "ESITO", nullable = false)
	private String esito;

	@Column(name = "FILE_LOCATION", nullable = false)
	private String fileLocation;

	@Column(name = "FILE_NAME", nullable = false)
	private String fileName;

	@Column(name = "TOTALE_OPERE", nullable = false)
	private Integer totaleOpere;

	@Column(name = "TOTALE_OPERE_ACQUISITE", nullable = false)
	private Integer totaleOpereAcquisite;
	
	@Column(name = "MESSAGE", nullable = false)
	private String message;

	@Override
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;

	}

	public Date getDataInserimento() {
		return dataInserimento;
	}

	public void setDataInserimento(Date dataInserimento) {
		this.dataInserimento = dataInserimento;
	}

	public String getStato() {
		return stato;
	}

	public void setStato(String stato) {
		this.stato = stato;
	}

	public String getSocieta() {
		return societa;
	}

	public void setSocieta(String societa) {
		this.societa = societa;
	}

	public String getEsito() {
		return esito;
	}

	public void setEsito(String esito) {
		this.esito = esito;
	}

	public String getFileLocation() {
		return fileLocation;
	}

	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Integer getTotaleOpere() {
		return totaleOpere;
	}

	public void setTotaleOpere(Integer totaleOpere) {
		this.totaleOpere = totaleOpere;
	}

	public Integer getTotaleOpereAcquisite() {
		return totaleOpereAcquisite;
	}

	public void setTotaleOpereAcquisite(Integer totaleOpereAcquisite) {
		this.totaleOpereAcquisite = totaleOpereAcquisite;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
