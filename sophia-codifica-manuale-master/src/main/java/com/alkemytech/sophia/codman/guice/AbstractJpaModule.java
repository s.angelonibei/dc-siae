package com.alkemytech.sophia.codman.guice;

import com.alkemytech.sophia.codman.provider.DataSourceProvider;
import com.google.inject.PrivateModule;
import com.google.inject.Singleton;
import com.google.inject.name.Names;
import com.google.inject.persist.jpa.JpaPersistModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public abstract class AbstractJpaModule extends PrivateModule {
    public final Properties configuration;
    private String unitName;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public AbstractJpaModule(Properties configuration,String unitName) {
        super();
        this.configuration = configuration;
        this.unitName = unitName;
    }

    protected Properties loadConfig() {
//        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        final Properties properties = new Properties();
//        final InputStream input = classLoader.getResourceAsStream(configuration.getProperty("jpa." + unitName + ".config"));
        final File file = new File(configuration.getProperty("jpa." + unitName + ".config"));
        final InputStream input;
        try  {
            if (file.exists()){
                input = new FileInputStream(file);
            } else {
                input = this.getClass().getResourceAsStream("/" + configuration.getProperty("jpa." + unitName + ".config"));
            }
            properties.load(input);
            logger.debug("loadConfig: properties {}", properties);
            Names.bindProperties(binder(), properties);
        } catch (IOException e) {
            logger.error("loadConfig", e);
        }
        return properties;
    }

    protected void configure() {
        final JpaPersistModule jpaPersistModule = new JpaPersistModule(unitName);
        jpaPersistModule.properties(loadConfig());
        install(jpaPersistModule);
        // expose entity manager
        bind(DataSource.class).toProvider(DataSourceProvider.class).in(Singleton.class);
    }
}
