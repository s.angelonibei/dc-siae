package com.alkemytech.sophia.codman.service.dsrmetadata.period;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DSRPeriodExtractorYYYYMMDD_YYYYMMDD_v2 implements DsrPeriodExtractor{
	
	private String regex = "[0-9]{4}-[0-9]{2}-[0-9]{2}--[0-9]{4}-[0-9]{2}-[0-9]{2}";
	private String code = "YYYY-MM-DD--YYYY-MM-DD";
	private String description = "YYYY-MM-DD--YYYY-MM-DD" ;
	
	@Override
	public DsrPeriod extractDsrPeriod(String periodString) {

		try {
			String startDate = periodString.substring(0, 10);
			String endDate = periodString.substring(12, 22);

			String startYear = startDate.substring(0, 4);
			String startMonth = startDate.substring(5, 7);

			String endYear = endDate.substring(0, 4);
			String endMonth = endDate.substring(5, 7);


			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

			Date periodStartDate = dateFormat.parse(startDate);
			Date periodEndDate = dateFormat.parse(endDate);


			DsrPeriod dsrPeriod = new DsrPeriod();
			dsrPeriod.setYear(startYear);
			if (startYear.equals(endYear) && startMonth.equals(endMonth)) {
				dsrPeriod.setPeriodType(DsrPeriod.MENSILE);
				dsrPeriod.setPeriod(startMonth);
			} else {
				dsrPeriod.setPeriodType(DsrPeriod.TRIMESTRALE);
				if ("01".equals(startMonth) && "03".equals(endMonth)) {
					dsrPeriod.setPeriod("1");
				} else if ("04".equals(startMonth) && "06".equals(endMonth)) {
					dsrPeriod.setPeriod("2");
				} else if ("07".equals(startMonth) && "09".equals(endMonth)) {
					dsrPeriod.setPeriod("3");
				} else if ("10".equals(startMonth) && "12".equals(endMonth)) {
					dsrPeriod.setPeriod("4");
				}else{
					return null;
				}
			}

			dsrPeriod.setPeriodStartDate(periodStartDate);
			dsrPeriod.setPeriodEndDate(periodEndDate);

			return dsrPeriod;

		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
		
	}

	@Override
	public String getRegex() {
		return regex;
	}

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public String getDescription() {
		return description;
	}

}
