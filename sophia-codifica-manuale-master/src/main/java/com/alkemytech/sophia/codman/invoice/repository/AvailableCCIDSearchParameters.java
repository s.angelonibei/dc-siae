package com.alkemytech.sophia.codman.invoice.repository;

import java.util.List;

import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;

import java.io.Serializable;

@Produces("application/json")
@XmlRootElement
public class AvailableCCIDSearchParameters implements Serializable  {

  	private static final long serialVersionUID = 6932794204175966625L;

  	Integer first;
    Integer last;
    String dsp;
    List<String> countryList;
    List<String> utilizationList;
    List<String>  offerList;
    Integer monthFrom;
    Integer yearFrom;
    Integer monthTo;
    Integer yearTo;
    
    public AvailableCCIDSearchParameters(){
    	
    }

    public Integer getFirst() {
        return first;
    }

    public void setFirst(Integer first) {
        this.first = first;
    }

    public Integer getLast() {
        return last;
    }

    public void setLast(Integer last) {
        this.last = last;
    }

    public String getDsp() {
        return dsp;
    }

    public void setDsp(String dsp) {
        this.dsp = dsp;
    }

    public List<String> getCountryList() {
        return countryList;
    }

    public void setCountryList(List<String> countryList) {
        this.countryList = countryList;
    }

    public List<String> getUtilizationList() {
        return utilizationList;
    }

    public void setUtilizationList(List<String> utilizationList) {
        this.utilizationList = utilizationList;
    }

    public List<String> getOfferList() {
        return offerList;
    }

    public void setOfferList(List<String> offerList) {
        this.offerList = offerList;
    }

    public Integer getMonthFrom() {
        return monthFrom;
    }

    public void setMonthFrom(Integer monthFrom) {
        this.monthFrom = monthFrom;
    }

    public Integer getYearFrom() {
        return yearFrom;
    }

    public void setYearFrom(Integer yearFrom) {
        this.yearFrom = yearFrom;
    }

    public Integer getMonthTo() {
        return monthTo;
    }

    public void setMonthTo(Integer monthTo) {
        this.monthTo = monthTo;
    }

    public Integer getYearTo() {
        return yearTo;
    }

    public void setYearTo(Integer yearTo) {
        this.yearTo = yearTo;
    }
}
