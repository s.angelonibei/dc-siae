package com.alkemytech.sophia.codman.entity.performing.dao;

import com.alkemytech.sophia.codman.dto.performing.MusicProviderDTO;
import com.alkemytech.sophia.codman.dto.performing.PalinsestoDTO;
import com.alkemytech.sophia.performing.model.PerfRsUtilizationFile;

import java.text.ParseException;
import java.util.List;

public interface IRadioInStoreDAO {

	MusicProviderDTO getMusicProviderById(Long id);

	List<MusicProviderDTO> getMusicProviderListByName(String nome);

	List<MusicProviderDTO> getMusicProviderList();

	List<com.alkemytech.sophia.performing.model.PerfPalinsesto> getPalinsestiList();

	List<com.alkemytech.sophia.performing.model.PerfPuntoVendita> getPuntiVenditaList();

	List<com.alkemytech.sophia.performing.model.PerfPalinsesto> getPalinsestiListByMp(Long id, Long idPalinsesto);

	List<com.alkemytech.sophia.performing.model.PerfPuntoVendita> getPuntiVenditaByPalinsesto(Long id, Long idPalinsesto);

	List<com.alkemytech.sophia.performing.model.PerfRsUtente> getUsers(Long idMusicProvider);
	
	Integer insertMusicProvider(MusicProviderDTO musicProviderDTO);

	Integer updateMusicProvider(MusicProviderDTO musicProvider);

	Integer updatePalinsesto(PalinsestoDTO palinsesto);

	Integer insertPalinsesto(PalinsestoDTO palinsesto);

    PerfRsUtilizationFile save(PerfRsUtilizationFile rsUtilizationFile) throws ParseException;
}
