package com.alkemytech.sophia.codman.performing.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class CombanaDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer idCombana;
	private String autori;
	private String codiceCombana;
	private String compositori;
	private Date dataCreazione;
	private String interpreti;
	private boolean lavorabile;
	private String titolo;
	private FontePrimaUtilizzazioneDTO fontePrimaUtilizzazioneDTO;
	private UtilizzazioneDTO utilizzazioneDTO;

	//bi-directional many-to-one association to PerfUtilizzazione

	public CombanaDTO() {
	}

	public Integer getIdCombana() {
		return this.idCombana;
	}

	public void setIdCombana(Integer idCombana) {
		this.idCombana = idCombana;
	}

	public String getAutori() {
		return this.autori;
	}

	public void setAutori(String autori) {
		this.autori = autori;
	}

	public String getCodiceCombana() {
		return this.codiceCombana;
	}

	public void setCodiceCombana(String codiceCombana) {
		this.codiceCombana = codiceCombana;
	}

	public String getCompositori() {
		return this.compositori;
	}

	public void setCompositori(String compositori) {
		this.compositori = compositori;
	}

	public Date getDataCreazione() {
		return this.dataCreazione;
	}

	public void setDataCreazione(Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}

	public String getInterpreti() {
		return this.interpreti;
	}

	public void setInterpreti(String interpreti) {
		this.interpreti = interpreti;
	}

	public boolean getLavorabile() {
		return this.lavorabile;
	}

	public void setLavorabile(boolean lavorabile) {
		this.lavorabile = lavorabile;
	}

	public String getTitolo() {
		return this.titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}
	public FontePrimaUtilizzazioneDTO getFontePrimaUtilizzazioneDTO() {
		return this.fontePrimaUtilizzazioneDTO;
	}

	public void setFontePrimaUtilizzazioneDTO(FontePrimaUtilizzazioneDTO fontePrimaUtilizzazioneDTO) {
		this.fontePrimaUtilizzazioneDTO = fontePrimaUtilizzazioneDTO;
	}

	public UtilizzazioneDTO getUtilizzazioneDTO() {
		return utilizzazioneDTO;
	}

	public void setUtilizzazioneDTO(UtilizzazioneDTO utilizzazioneDTO) {
		this.utilizzazioneDTO = utilizzazioneDTO;
	}
}