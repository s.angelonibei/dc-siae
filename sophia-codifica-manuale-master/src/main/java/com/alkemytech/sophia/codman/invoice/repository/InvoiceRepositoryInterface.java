package com.alkemytech.sophia.codman.invoice.repository;

import com.alkemytech.sophia.codman.dto.InvoiceDTO;

import java.util.List;

public interface InvoiceRepositoryInterface {

    public InvoiceDTO newInvoice(InvoiceDTO newInvoice);

    public List<InvoiceDTO> searchInvoice(InvoiceSearchParameters searchParameters);

    public void updateInvoice(InvoiceDTO invoice);


}
