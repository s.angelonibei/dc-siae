package com.alkemytech.sophia.codman.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("key_value")
public class KeyValueProperty extends Setting {
	
	private static final long serialVersionUID = 1L;

	/*i18n?*/
	@Column(name="SETTING_LABEL")
	protected String label;

	@Column(name="VALUE_TYPE")
	protected String valueType;
	
	@Column(name="VALUE")
	protected String value;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getValueType() {
		return valueType;
	}

	public void setValueType(String valueType) {
		this.valueType = valueType;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public boolean isTrue() {
		return valueType.equals("Boolean") && (Boolean.parseBoolean(getValue()) || "1".equals(getValue()));
	}

}
