package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class StatDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String header;
	
	private String value;
	
	private String valueType;
	
	public StatDTO() {
		super();
	}

	public StatDTO(String header, String value) {
		super();
		this.header = header;
		this.value = value;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getValueType() {
		return valueType;
	}

	public void setValueType(String valueType) {
		this.valueType = valueType;
	}
	
	
}
