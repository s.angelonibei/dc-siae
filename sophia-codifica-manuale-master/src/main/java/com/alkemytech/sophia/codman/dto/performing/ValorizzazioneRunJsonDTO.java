package com.alkemytech.sophia.codman.dto.performing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ValorizzazioneRunJsonDTO {

	public ValorizzazioneRunJsonDTO() {
		super();
	}
	
	@Override
	public String toString() {
		return "";
	}
	
}
