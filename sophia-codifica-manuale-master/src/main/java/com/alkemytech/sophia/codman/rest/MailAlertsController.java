package com.alkemytech.sophia.codman.rest;

import java.math.BigInteger;
import java.util.*;

import javax.persistence.EntityManager;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.alkemytech.sophia.codman.dto.DspDTO;
import com.alkemytech.sophia.codman.dto.MailAddressDTO;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.google.inject.Provider;
import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.config.ResultType;
import org.eclipse.persistence.sessions.Record;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.codman.dto.MailAlertDTO;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
@Path("mail-alerts")
public class MailAlertsController {
	
	private static final int CLAIM_ERRORE=1;
	private static final int CLAIM_TERMINATO=2;
	private static final int CLAIM_NEGATIVO=3;
	

	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final Provider<EntityManager> provider;
	
	private Gson gson;
	
	private static int counter = 1;

	@Inject
	protected MailAlertsController(@McmdbDataSource Provider<EntityManager> provider,Gson gson) {
		super();
		this.provider = provider;
		this.gson = gson;
	}

	@GET
	@Produces("application/json")
	public Response getAlerts() {
		try {
			
			EntityManager em = provider.get();
			
			String selectSql = "select ID_LANCIO, GROUP_CONCAT(distinct IDDSP) IDDSP,min(EMAIL_LIST) EMAIL_LIST ,GROUP_CONCAT(distinct TIPO) TIPO from MM_NOTIFICA_EMAIL group by ID_LANCIO;";

			List<Record> resultList = em.createNativeQuery(selectSql).setHint(QueryHints.RESULT_TYPE, ResultType.Map).getResultList();

			
			List<MailAlertDTO> listaMail = new ArrayList<>();
			
			for (Record r : resultList) {
				MailAlertDTO mail = new MailAlertDTO();
				String idLancio = (String)r.get("ID_LANCIO");
				String idDspList = (String)r.get("IDDSP");
				String tipoList = (String)r.get("TIPO");
				String emailList = (String)r.get("EMAIL_LIST");

				List<DspDTO> dspList= new ArrayList<>();
				for (String s : Arrays.asList(idDspList.split(","))) {
					DspDTO d = new DspDTO();
					d.setCode(s);
					d.setDescription(s);
					d.setIdDsp(s);
					d.setName(s);
					dspList.add(d);
				}
				
				mail.setDsps(dspList);

				List<MailAddressDTO> mailList= new ArrayList<>();
				for (String s : Arrays.asList(emailList.split(","))) {
					MailAddressDTO d = new MailAddressDTO();
					d.setValue(s);
					mailList.add(d);
				}
				mail.setAddresses(mailList);

				List<String> listaTipo =  Arrays.asList(tipoList.split(","));
				
				if(listaTipo.contains(CLAIM_TERMINATO+""))
					mail.setCompletedClaim(true);
				if(listaTipo.contains(CLAIM_ERRORE+""))
					mail.setFailedClaim(true);
				if(listaTipo.contains(CLAIM_NEGATIVO+""))
					mail.setNegativeClaim(true);


				mail.setId(Long.parseLong(idLancio));
				listaMail.add(mail);
				
			}
			
			return Response.ok(gson.toJson(listaMail)).build();
		} catch (Exception e) {
			logger.error("Errore...", e);
			return Response.status(500).build();
		}
	}

	@POST
	@Produces("application/json")
	@Consumes("application/json")
	public Response saveAlert(MailAlertDTO alert) {
		try {

			List<String> emails = new ArrayList<>();
			
			for (MailAddressDTO address : alert.getAddresses()) {
				if(StringUtils.isNotBlank(address.getValue()))
					emails.add(address.getValue().trim());
			}
			
			String mailList = StringUtils.join(emails.toArray(), ";");
			long  idLancio = System.currentTimeMillis() / 1000L;

			EntityManager em = provider.get();
			em.getTransaction().begin();
			String insertSql = "INSERT INTO MM_NOTIFICA_EMAIL (ID_LANCIO,IDDSP,EMAIL_LIST,TIPO) VALUES (?,?,?,?)";


			//inserisco una notifica per dsp-tipo (raggruppero in select)
			for (DspDTO dsp : alert.getDsps()) {
				
				if(alert.isCompletedClaim()){
					em.createNativeQuery(insertSql)
							.setParameter(1,idLancio)
							.setParameter(2,dsp.getIdDsp())
							.setParameter(3,mailList)
							.setParameter(4,CLAIM_TERMINATO)
							.executeUpdate();
				}
				if(alert.isFailedClaim()){
					em.createNativeQuery(insertSql)
							.setParameter(1,idLancio)
							.setParameter(2,dsp.getIdDsp())
							.setParameter(3,mailList)
							.setParameter(4,CLAIM_ERRORE)
							.executeUpdate();
				}
				if(alert.isNegativeClaim()){
					em.createNativeQuery(insertSql)
							.setParameter(1,idLancio)
							.setParameter(2,dsp.getIdDsp())
							.setParameter(3,mailList)
							.setParameter(4,CLAIM_NEGATIVO)
							.executeUpdate();
				}

			}
			
			em.getTransaction().commit();
			alert.setId(idLancio);
			
			return Response.ok(gson.toJson(alert)).build();
		} catch (Exception e) {
			logger.error(String.format("Errore ..."), e);
			return Response.status(500).build();
		}
	}
	
	@DELETE
	@Produces("application/json")
	@Consumes("application/json")
	public Response deleteAlert(MailAlertDTO alert) {
		try {
		
			EntityManager em = provider.get();
			
			em.getTransaction().begin();
			
			String deleteSql="DELETE FROM MM_NOTIFICA_EMAIL WHERE ID_LANCIO=?";
			
			em.createNativeQuery(deleteSql)
					.setParameter(1,alert.getId())
					.executeUpdate();
			
			
			em.getTransaction().commit();
			
			
			return Response.ok().build();
		}
		catch (Exception e) {
			logger.error(String.format("Errore nell'eliminazione dell'alert: %s", gson.toJson(alert)), e);
			return Response.status(500).build();
		}
	}
	
	

}
