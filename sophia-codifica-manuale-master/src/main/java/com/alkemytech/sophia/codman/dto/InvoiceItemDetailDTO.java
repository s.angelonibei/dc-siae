package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;

@Produces("application/json")
@XmlRootElement
public class InvoiceItemDetailDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4561343223522059897L;
	private String dspName;
	private String periodString;
	private String country;
	private String utilizationType;
	private String commercialOffer;
	private BigDecimal totalInvoiceValue;
	private BigDecimal totalValue;
	private BigDecimal totalValueOrigCurrency;
	private String currency;

	
	public InvoiceItemDetailDTO() {

	}

	public InvoiceItemDetailDTO(Object[] obj) {

		if (null != obj && obj.length == 16) {
			try {
				this.dspName = (String) obj[9];
				this.periodString = (String) obj[10];
				this.country = (String) obj[11];
				this.utilizationType = (String) obj[12];
				this.commercialOffer = (String) obj[13];
				this.totalInvoiceValue = (BigDecimal) obj[2];
				this.totalValue = (BigDecimal) obj[3];
				this.currency = (String) obj[5];

			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	public String getDspName() {
		return dspName;
	}

	public void setDspName(String dspName) {
		this.dspName = dspName;
	}

	public String getPeriodString() {
		return periodString;
	}

	public void setPeriodString(String periodString) {
		this.periodString = periodString;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getUtilizationType() {
		return utilizationType;
	}

	public void setUtilizationType(String utilizationType) {
		this.utilizationType = utilizationType;
	}

	public String getCommercialOffer() {
		return commercialOffer;
	}

	public void setCommercialOffer(String commercialOffer) {
		this.commercialOffer = commercialOffer;
	}

	public BigDecimal getTotalValue() {
		return totalValue;
	}

	public void setTotalValue(BigDecimal totalValue) {
		this.totalValue = totalValue;
	}

	public BigDecimal getTotalValueOrigCurrency() {
		return totalValueOrigCurrency;
	}

	public void setTotalValueOrigCurrency(BigDecimal totalValueOrigCurrency) {
		this.totalValueOrigCurrency = totalValueOrigCurrency;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public BigDecimal getTotalInvoiceValue() {
		return totalInvoiceValue;
	}

	public void setTotalInvoiceValue(BigDecimal totalInvoiceValue) {
		this.totalInvoiceValue = totalInvoiceValue;
	}


}
