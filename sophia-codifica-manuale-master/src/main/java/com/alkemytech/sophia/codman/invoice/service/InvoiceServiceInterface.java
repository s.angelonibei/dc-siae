package com.alkemytech.sophia.codman.invoice.service;

import com.alkemytech.sophia.codman.dto.AnagClientDTO;
import com.alkemytech.sophia.codman.dto.InvoiceCCIDDTO;
import com.alkemytech.sophia.codman.dto.InvoiceDTO;
import com.alkemytech.sophia.codman.dto.InvoiceDraftDto;
import com.alkemytech.sophia.codman.dto.InvoiceItemDto;
import com.alkemytech.sophia.codman.dto.InvoiceItemReasonDTO;
import com.alkemytech.sophia.codman.entity.CCIDMetadata;
import com.alkemytech.sophia.codman.invoice.repository.AvailableCCIDSearchParameters;
import com.alkemytech.sophia.codman.invoice.repository.InvoiceConfiguration;
import com.alkemytech.sophia.codman.invoice.repository.InvoiceItemToCcid;
import com.alkemytech.sophia.codman.invoice.repository.InvoiceSearchParameters;
import com.alkemytech.sophia.codman.invoice.repository.UpdateCcidParameter;
import com.alkemytech.sophia.invoice.integration.model.InvoiceResponse;

import java.io.ByteArrayOutputStream;
import java.util.List;

public interface InvoiceServiceInterface {

    public InvoiceDTO newInvoice(InvoiceDraftDto dto);

    public List<InvoiceDTO> searchInvoice(InvoiceSearchParameters searchParameters);

    public InvoiceDTO confirmInvoice(InvoiceDTO dto);

    public void removeInvoice(Integer idInvoice);
    
    public void removeInvoiceBozza(Integer idInvoice);

    public InvoiceDTO updateInvoice(InvoiceDTO dto);

    public List<InvoiceCCIDDTO> payableCCID(AvailableCCIDSearchParameters searchParameters);
    
    public List<InvoiceCCIDDTO> availableCCID(AvailableCCIDSearchParameters searchParameters);

	public boolean updateCCid(UpdateCcidParameter dto);
	
    public InvoiceConfiguration getConfiguration();
    
    public List<InvoiceItemReasonDTO> getInvoiceItemReasons();
    
    public InvoiceResponse callInvoiceService(int invoiceId);
    
    public List<InvoiceItemDto> getInvoiceItems(Integer invoiceId);

	public AnagClientDTO getAnagClient(String dsp);
	
	public InvoiceDTO getInvoice(Integer id);

	
	public InvoiceDTO checkInvoice(Integer id);
	
	public byte[] downloadInvoice(Integer id);
	
	public CCIDMetadata getCcidMetadata(UpdateCcidParameter dto);

	public List<InvoiceItemToCcid> getInvoiceItemToCcid(CCIDMetadata ccidMetadata);
	
	

	
}
