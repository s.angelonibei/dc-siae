package com.alkemytech.sophia.codman.entity.performing;

import java.io.Serializable;
import java.util.Objects;

public class UtilizzazioniAssegnatePK implements Serializable {
    //default serial version id, required for serializable classes.
    private static final long serialVersionUID = 1L;

    private Long idCodifica;

    private Long idSessioneCodifica;

    public UtilizzazioniAssegnatePK() {
    }

    public Long getIdCodifica() {
        return idCodifica;
    }

    public void setIdCodifica(Long idCodifica) {
        this.idCodifica = idCodifica;
    }

    public Long getIdSessioneCodifica() {
        return idSessioneCodifica;
    }

    public void setIdSessioneCodifica(Long idSessioneCodifica) {
        this.idSessioneCodifica = idSessioneCodifica;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UtilizzazioniAssegnatePK that = (UtilizzazioniAssegnatePK) o;
        return Objects.equals(idCodifica, that.idCodifica) &&
                Objects.equals(idSessioneCodifica, that.idSessioneCodifica);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idCodifica, idSessioneCodifica);
    }
}