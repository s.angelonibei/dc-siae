package com.alkemytech.sophia.codman.entity.performing;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;


/**
 * The persistent class for the PERF_MOVIMENTO_CONTABILE database table.
 * 
 */
@Entity
@Table(name="PERF_MOVIMENTO_CONTABILE")
@NamedQuery(name="PerfMovimentoContabile.findAll", query="SELECT p FROM PerfMovimentoContabile p")
public class PerfMovimentoContabile implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID_MOVIMENTO_CONTABILE", unique=true, nullable=false)
	private BigDecimal idMovimentoContabile;

	@Column(name="AGENZIA", length=50)
	private String agenzia;

	@Column(name="CODICE_RAGGRUPPAMENTO", length=50)
	private String codiceRaggruppamento;

	@Column(name="CODICE_VOCE_INCASSO", length=4)
	private String codiceVoceIncasso;

	@Column(name="CONTABILITA")
	private BigInteger contabilita;

	@Column(name="CONTABILITA_ORIG")
	private BigInteger contabilitaOrig;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_INS", nullable=false)
	private Date dataIns;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_ORA_ULTIMA_MODIFICA")
	private Date dataOraUltimaModifica;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_REVERSALE")
	private Date dataReversale;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_RIENTRO")
	private Date dataRientro;

	@Column(name="FLAG_GRUPPO_PRINCIPALE", length=1)
	private String flagGruppoPrincipale;

	@Column(name="FLAG_NO_MAGGIORAZIONE", length=1)
	private String flagNoMaggiorazione;

	@Column(name="FLAG_SOSPESO", length=1)
	private String flagSospeso;

	@Column(name="ID_EVENTO")
	private BigInteger idEvento;

	@Column(name="ID_PROGRAMMA_MUSICALE")
	private BigInteger idProgrammaMusicale;

	@Column(name="IMPORTO_DEM_TOTALE", precision=10, scale=7)
	private BigDecimal importoDemTotale;

	@Column(name="IMPORTO_MANUALE", precision=10, scale=7)
	private BigDecimal importoManuale;

	@Column(name="IMPORTO_VALORIZZATO", precision=10, scale=7)
	private BigDecimal importoValorizzato;

	@Column(name="NUMERO_FATTURA")
	private BigInteger numeroFattura;

	@Column(name="NUMERO_PERMESSO", length=50)
	private String numeroPermesso;

	@Column(name="NUMERO_PM", length=20)
	private String numeroPm;

	@Column(name="NUMERO_PM_PREVISTI")
	private BigInteger numeroPmPrevisti;

	@Column(name="NUMERO_PM_PREVISTI_SPALLA")
	private BigInteger numeroPmPrevistiSpalla;

	@Column(name="NUMERO_REVERSALE")
	private BigInteger numeroReversale;

	@Column(name="PM_RIPARTITO_SEMESTRE_PREC", length=1)
	private String pmRipartitoSemestrePrec;

	@Column(name="PUNTI_PROGRAMMA_MUSICALE", precision=10, scale=7)
	private BigDecimal puntiProgrammaMusicale;

	@Column(name="SEDE", length=50)
	private String sede;

	@Column(name="TIPO_DOCUMENTO_CONTABILE", length=3)
	private String tipoDocumentoContabile;

	@Column(name="TIPOLOGIA_MOVIMENTO", length=20)
	private String tipologiaMovimento;

	@Column(name="TOTALE_DEM_LORDO_PM", precision=10, scale=7)
	private BigDecimal totaleDemLordoPm;

	@Column(name="UTENTE_ULTIMA_MODIFICA", length=50)
	private String utenteUltimaModifica;

	@Column(name="ID_PERIODO_RIPARTIZIONE")
	private Long idPeriodoRipartizione;

	public PerfMovimentoContabile() {
	}

	public BigDecimal getIdMovimentoContabile() {
		return this.idMovimentoContabile;
	}

	public void setIdMovimentoContabile(BigDecimal idMovimentoContabile) {
		this.idMovimentoContabile = idMovimentoContabile;
	}

	public String getAgenzia() {
		return this.agenzia;
	}

	public void setAgenzia(String agenzia) {
		this.agenzia = agenzia;
	}

	public String getCodiceRaggruppamento() {
		return this.codiceRaggruppamento;
	}

	public void setCodiceRaggruppamento(String codiceRaggruppamento) {
		this.codiceRaggruppamento = codiceRaggruppamento;
	}

	public String getCodiceVoceIncasso() {
		return this.codiceVoceIncasso;
	}

	public void setCodiceVoceIncasso(String codiceVoceIncasso) {
		this.codiceVoceIncasso = codiceVoceIncasso;
	}

	public BigInteger getContabilita() {
		return this.contabilita;
	}

	public void setContabilita(BigInteger contabilita) {
		this.contabilita = contabilita;
	}

	public BigInteger getContabilitaOrig() {
		return this.contabilitaOrig;
	}

	public void setContabilitaOrig(BigInteger contabilitaOrig) {
		this.contabilitaOrig = contabilitaOrig;
	}

	public Date getDataIns() {
		return this.dataIns;
	}

	public void setDataIns(Date dataIns) {
		this.dataIns = dataIns;
	}

	public Date getDataOraUltimaModifica() {
		return this.dataOraUltimaModifica;
	}

	public void setDataOraUltimaModifica(Date dataOraUltimaModifica) {
		this.dataOraUltimaModifica = dataOraUltimaModifica;
	}

	public Date getDataReversale() {
		return this.dataReversale;
	}

	public void setDataReversale(Date dataReversale) {
		this.dataReversale = dataReversale;
	}

	public Date getDataRientro() {
		return this.dataRientro;
	}

	public void setDataRientro(Date dataRientro) {
		this.dataRientro = dataRientro;
	}

	public String getFlagGruppoPrincipale() {
		return this.flagGruppoPrincipale;
	}

	public void setFlagGruppoPrincipale(String flagGruppoPrincipale) {
		this.flagGruppoPrincipale = flagGruppoPrincipale;
	}

	public String getFlagNoMaggiorazione() {
		return this.flagNoMaggiorazione;
	}

	public void setFlagNoMaggiorazione(String flagNoMaggiorazione) {
		this.flagNoMaggiorazione = flagNoMaggiorazione;
	}

	public String getFlagSospeso() {
		return this.flagSospeso;
	}

	public void setFlagSospeso(String flagSospeso) {
		this.flagSospeso = flagSospeso;
	}

	public BigInteger getIdEvento() {
		return this.idEvento;
	}

	public void setIdEvento(BigInteger idEvento) {
		this.idEvento = idEvento;
	}

	public BigInteger getIdProgrammaMusicale() {
		return this.idProgrammaMusicale;
	}

	public void setIdProgrammaMusicale(BigInteger idProgrammaMusicale) {
		this.idProgrammaMusicale = idProgrammaMusicale;
	}

	public BigDecimal getImportoDemTotale() {
		return this.importoDemTotale;
	}

	public void setImportoDemTotale(BigDecimal importoDemTotale) {
		this.importoDemTotale = importoDemTotale;
	}

	public BigDecimal getImportoManuale() {
		return this.importoManuale;
	}

	public void setImportoManuale(BigDecimal importoManuale) {
		this.importoManuale = importoManuale;
	}

	public BigDecimal getImportoValorizzato() {
		return this.importoValorizzato;
	}

	public void setImportoValorizzato(BigDecimal importoValorizzato) {
		this.importoValorizzato = importoValorizzato;
	}

	public BigInteger getNumeroFattura() {
		return this.numeroFattura;
	}

	public void setNumeroFattura(BigInteger numeroFattura) {
		this.numeroFattura = numeroFattura;
	}

	public String getNumeroPermesso() {
		return this.numeroPermesso;
	}

	public void setNumeroPermesso(String numeroPermesso) {
		this.numeroPermesso = numeroPermesso;
	}

	public String getNumeroPm() {
		return this.numeroPm;
	}

	public void setNumeroPm(String numeroPm) {
		this.numeroPm = numeroPm;
	}

	public BigInteger getNumeroPmPrevisti() {
		return this.numeroPmPrevisti;
	}

	public void setNumeroPmPrevisti(BigInteger numeroPmPrevisti) {
		this.numeroPmPrevisti = numeroPmPrevisti;
	}

	public BigInteger getNumeroPmPrevistiSpalla() {
		return this.numeroPmPrevistiSpalla;
	}

	public void setNumeroPmPrevistiSpalla(BigInteger numeroPmPrevistiSpalla) {
		this.numeroPmPrevistiSpalla = numeroPmPrevistiSpalla;
	}

	public BigInteger getNumeroReversale() {
		return this.numeroReversale;
	}

	public void setNumeroReversale(BigInteger numeroReversale) {
		this.numeroReversale = numeroReversale;
	}

	public String getPmRipartitoSemestrePrec() {
		return this.pmRipartitoSemestrePrec;
	}

	public void setPmRipartitoSemestrePrec(String pmRipartitoSemestrePrec) {
		this.pmRipartitoSemestrePrec = pmRipartitoSemestrePrec;
	}

	public BigDecimal getPuntiProgrammaMusicale() {
		return this.puntiProgrammaMusicale;
	}

	public void setPuntiProgrammaMusicale(BigDecimal puntiProgrammaMusicale) {
		this.puntiProgrammaMusicale = puntiProgrammaMusicale;
	}

	public String getSede() {
		return this.sede;
	}

	public void setSede(String sede) {
		this.sede = sede;
	}

	public String getTipoDocumentoContabile() {
		return this.tipoDocumentoContabile;
	}

	public void setTipoDocumentoContabile(String tipoDocumentoContabile) {
		this.tipoDocumentoContabile = tipoDocumentoContabile;
	}

	public String getTipologiaMovimento() {
		return this.tipologiaMovimento;
	}

	public void setTipologiaMovimento(String tipologiaMovimento) {
		this.tipologiaMovimento = tipologiaMovimento;
	}

	public BigDecimal getTotaleDemLordoPm() {
		return this.totaleDemLordoPm;
	}

	public void setTotaleDemLordoPm(BigDecimal totaleDemLordoPm) {
		this.totaleDemLordoPm = totaleDemLordoPm;
	}

	public String getUtenteUltimaModifica() {
		return this.utenteUltimaModifica;
	}

	public void setUtenteUltimaModifica(String utenteUltimaModifica) {
		this.utenteUltimaModifica = utenteUltimaModifica;
	}

	public Long getIdPeriodoRipartizione() {
		return idPeriodoRipartizione;
	}

	public void setIdPeriodoRipartizione(Long idPeriodoRipartizione) {
		this.idPeriodoRipartizione = idPeriodoRipartizione;
	}
}