package com.alkemytech.sophia.codman.rest;

import com.alkemytech.sophia.codman.dto.Monitoraggio648DTO;
import com.alkemytech.sophia.codman.entity.MmMonitoraggio648;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.sql.Array;
import java.sql.Timestamp;
import java.util.*;

@Singleton
@Path("monitoraggio648")
public class Monitoraggio648Service {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final Provider<EntityManager> provider;
    private final Gson gson;

    @Inject
    protected Monitoraggio648Service(@McmdbDataSource Provider<EntityManager> provider, Gson gson) {
        this.provider = provider;
        this.gson = gson;
    }

    @POST
    @Path("save/{idDsr}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response save(String monitoraggioRow, @PathParam("idDsr") String idDsr){

        String[] params = monitoraggioRow.split("&");

        HashMap<String, String> map = new HashMap<>();

        for (String param: params) {
            String[] keyValue = param.split("=");
            map.put(keyValue[0], keyValue[1]);
        }

        Monitoraggio648DTO dto = new Monitoraggio648DTO();
        if (map.containsKey("idDsp"))
            dto.setIdDsp(map.get("idDsp"));
        if (map.containsKey("society"))
            dto.setSociety(map.get("society"));
        if(map.containsKey("stato"))
            dto.setStato(map.get("stato"));
        if(map.containsKey("errorDesc"))
            try {
                dto.setErrorDescription(URLDecoder.decode(map.get("errorDesc"), StandardCharsets.UTF_8.toString()));
            } catch (Exception e){
                logger.debug("save", e);
            }
        if(map.containsKey("dataInserimento"))
            dto.setDataInserimento(Long.parseLong(map.get("dataInserimento")));
        if(map.containsKey("dataAggiornamento"))
            dto.setDataAggiornamento(Long.parseLong(map.get("dataAggiornamento")));
        if(map.containsKey("valoreDem"))
            dto.setValoreDem(Float.parseFloat(map.get("valoreDem")));
        if(map.containsKey("valoreDrm"))
            dto.setValoreDrm(Float.parseFloat(map.get("valoreDrm")));

        EntityManager em = provider.get();
        //MmMonitoraggio648 oldMonitor = em.find(MmMonitoraggio648.class, idDsr);
        List<MmMonitoraggio648> oldMonitorList = em.createNamedQuery("MmMonitoraggio.findByDsrSocieta", MmMonitoraggio648.class)
                .setParameter("IDDSR", idDsr)
                .getResultList();
        MmMonitoraggio648 oldMonitor = null;

        if (!oldMonitorList.isEmpty())
            oldMonitor = oldMonitorList.get(0);

        try {
            MmMonitoraggio648 monitor = new MmMonitoraggio648();
            monitor.setIDDSR(idDsr);
            if (dto.getIdDsp() != null)
                monitor.setIDDSP(dto.getIdDsp());
            if (dto.getSociety() != null) {
                monitor.setSOCIETA(dto.getSociety());
            } else {
                em.getTransaction().begin();
                em.createNamedQuery("MmMonitoraggio.updateOnFailed")
                        .setParameter("stato", dto.getStato())
                        .setParameter("dataAggiornamento", new Timestamp(dto.getDataAggiornamento()))
                        .setParameter("errDescr", dto.getErrorDescription())
                        .setParameter("IDDSR", idDsr)
                .executeUpdate();
                em.getTransaction().commit();
                return Response.ok().build();
            }
            if (dto.getStato() != null)
                monitor.setSTATO(dto.getStato());
            if (dto.getDataInserimento() != null) {
                monitor.setDataInserimento(new Timestamp(dto.getDataInserimento()));
            } else {
                if (oldMonitor != null && oldMonitor.getDataInserimento() != null)
                    monitor.setDataInserimento(oldMonitor.getDataInserimento());
            }
            if (dto.getDataAggiornamento() != null)
                monitor.setDataAggiornamento(new Timestamp(dto.getDataAggiornamento()));
            if (dto.getValoreDem() != null) {
                monitor.setValoreClaimDem(dto.getValoreDem());
            } else {
                monitor.setValoreClaimDem(0.0f);
            }
            if (dto.getErrorDescription() != null){
                monitor.setErrDescr(dto.getErrorDescription());
            }
            if (dto.getValoreDrm() != null) {
                monitor.setValoreClaimDrm(dto.getValoreDrm());
            } else {
                monitor.setValoreClaimDrm(0.0f);
            }

            em.getTransaction().begin();
            em.merge(monitor);
            em.getTransaction().commit();

            return Response.ok().build();

        } catch (Exception e){
            if (em.getTransaction().isActive())
                em.getTransaction().rollback();

            logger.error("save ", e);
        }

        return Response.status(500).build();
    }
}
