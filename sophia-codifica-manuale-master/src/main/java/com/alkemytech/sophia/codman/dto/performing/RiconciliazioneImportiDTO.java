package com.alkemytech.sophia.codman.dto.performing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class RiconciliazioneImportiDTO {

    private Long contabilita;
    private String voceIncasso;
    private String semaforo;
    private BigDecimal percentualeAggioNDM;
    private BigDecimal totaleLordoSophia;
    private BigDecimal totaleLordoNDM;
    private BigDecimal nettoSophia;
    private BigDecimal nettoNDM;
    private BigDecimal importoExArt;
    private String numeroFattura;
    private BigDecimal totaleNettoSophia;
    private BigDecimal totaleNettoNDM;
    private BigDecimal totaleSophiaExArt;
    private BigDecimal deltaAssoluto;
    private BigDecimal deltaPercentuale;
    private String forzaturaNDM;
    
    private boolean riconciliabilePerImporto;
    private boolean riconciliabilePerVoceIncasso;
    private String voceIncassoSophia;

    public static final Map<String, String> fields;

    static {
        Map<String, String> newFields = new HashMap<>();
        newFields.put("periodo","Periodo Contabile (mese/anno)");
        newFields.put("numeroFattura","Numero Fattura");
        newFields.put("voceIncasso","Voce Incasso");
        newFields.put("lordoSophia","Totale Lordo Sophia (al netto dell'iva)");
        newFields.put("lordoNdm","Totale Lordo NDM (al netto dell'iva)");
        newFields.put("semaforo","Semaforo OK/KO");
        newFields.put("nettoSophia","Totale Sophia netto Aggio (al netto di iva e aggio)");
        newFields.put("nettoNdm","Totale NDM netto Aggio (al netto di iva e aggio)");
        newFields.put("percAggioNdm","% Aggio NDM");
        newFields.put("nettoSophiaArt","Totale Sophia netto (al netto di iva, aggio e 5%)");

        fields = Collections.unmodifiableMap(newFields);
    }

    public RiconciliazioneImportiDTO() {
    }

    public RiconciliazioneImportiDTO(Long contabilita, String voceIncasso, String semaforo,
                                     BigDecimal percentualeAggioNDM, BigDecimal totaleLordoSophia, BigDecimal totaleLordoNDM,
                                     BigDecimal nettoSophia, BigDecimal nettoNDM, BigDecimal importoExArt, String numeroFattura) {
        super();
        this.contabilita = contabilita;
        this.voceIncasso = voceIncasso;
        this.semaforo = semaforo;
        this.percentualeAggioNDM = percentualeAggioNDM;
        this.totaleLordoSophia = totaleLordoSophia;
        this.totaleLordoNDM = totaleLordoNDM;
        this.nettoSophia = nettoSophia;
        this.nettoNDM = nettoNDM;
        this.importoExArt = importoExArt;
        this.numeroFattura = numeroFattura;

        if(this.totaleLordoNDM != null && this.nettoNDM != null) {
            calculateTotaleNettoNdm();
        }
        if(this.totaleLordoSophia != null && this.nettoSophia != null) {
            calculateTotaleNettoSophia();
            if(this.importoExArt != null)
                calculateTotaleSophiaExArt();
        }
    }

    public Long getContabilita() {
        return contabilita;
    }

    public void setContabilita(Long contabilita) {
        this.contabilita = contabilita;
    }

    public String getVoceIncasso() {
        return voceIncasso;
    }

    public void setVoceIncasso(String voceIncasso) {
        this.voceIncasso = voceIncasso;
    }

    public String getSemaforo() {
        return semaforo;
    }

    public void setSemaforo(String semaforo) {
        this.semaforo = semaforo;
    }

    public BigDecimal getPercentualeAggioNDM() {
        return percentualeAggioNDM;
    }

    public void setPercentualeAggioNDM(BigDecimal percentualeAggioNDM) {
        this.percentualeAggioNDM = percentualeAggioNDM;
    }

    public BigDecimal getTotaleLordoSophia() {
        return totaleLordoSophia;
    }

    public void setTotaleLordoSophia(BigDecimal totaleLordoSophia) {
        this.totaleLordoSophia = totaleLordoSophia;
    }

    public BigDecimal getTotaleLordoNDM() {
        return totaleLordoNDM;
    }

    public void setTotaleLordoNDM(BigDecimal totaleLordoNDM) {
        this.totaleLordoNDM = totaleLordoNDM;
    }

    public BigDecimal getNettoSophia() {
        return nettoSophia;
    }

    public void setNettoSophia(BigDecimal nettoSophia) {
        this.nettoSophia = nettoSophia;
    }

    public BigDecimal getNettoNDM() {
        return nettoNDM;
    }

    public void setNettoNDM(BigDecimal nettoNDM) {
        this.nettoNDM = nettoNDM;
    }

    public BigDecimal getImportoExArt() {
        return importoExArt;
    }

    public void setImportoExArt(BigDecimal importoExArt) {
        this.importoExArt = importoExArt;
    }

    public String getNumeroFattura() {
        return numeroFattura;
    }

    public void setNumeroFattura(String numeroFattura) {
        this.numeroFattura = numeroFattura;
    }

    public BigDecimal getTotaleNettoSophia() {
        return totaleNettoSophia;
    }

    public void setTotaleNettoSophia(BigDecimal totaleNettoSophia) {
        this.totaleNettoSophia = totaleNettoSophia;
    }

    public BigDecimal getTotaleNettoNDM() {
        return totaleNettoNDM;
    }

    public void setTotaleNettoNDM(BigDecimal totaleNettoNDM) {
        this.totaleNettoNDM = totaleNettoNDM;
    }

    public BigDecimal getTotaleSophiaExArt() {
        return totaleSophiaExArt;
    }

    public void setTotaleSophiaExArt(BigDecimal totaleSophiaExArt) {
        this.totaleSophiaExArt = totaleSophiaExArt;
    }

    public BigDecimal getDeltaAssoluto() {
        return deltaAssoluto;
    }

    public void setDeltaAssoluto(BigDecimal deltaAssoluto) {
        this.deltaAssoluto = deltaAssoluto;
    }

    public BigDecimal getDeltaPercentuale() {
        return deltaPercentuale;
    }

    public void setDeltaPercentuale(BigDecimal deltaPercentuale) {
        this.deltaPercentuale = deltaPercentuale;
    }

    private void calculateTotaleNettoSophia() {
        this.totaleNettoSophia = this.totaleLordoSophia.subtract(this.nettoSophia);
    }

    private void calculateTotaleNettoNdm() {
        this.totaleNettoNDM = this.totaleLordoNDM.subtract(this.nettoNDM);
    }

    private void calculateTotaleSophiaExArt() {
        this.totaleSophiaExArt = this.totaleLordoSophia.subtract(this.nettoSophia).subtract(this.importoExArt);
    }

	public String getForzaturaNDM() {
		return forzaturaNDM;
	}

	public void setForzaturaNDM(String forzaturaNDM) {
		this.forzaturaNDM = forzaturaNDM;
	}

	public boolean isRiconciliabilePerImporto() {
		return riconciliabilePerImporto;
	}

	public void setRiconciliabilePerImporto(boolean riconciliabilePerImporto) {
		this.riconciliabilePerImporto = riconciliabilePerImporto;
	}

	public boolean isRiconciliabilePerVoceIncasso() {
		return riconciliabilePerVoceIncasso;
	}

	public void setRiconciliabilePerVoceIncasso(boolean riconciliabilePerVoceIncasso) {
		this.riconciliabilePerVoceIncasso = riconciliabilePerVoceIncasso;
	}

	public String getVoceIncassoSophia() {
		return voceIncassoSophia;
	}

	public void setVoceIncassoSophia(String voceIncassoSophia) {
		this.voceIncassoSophia = voceIncassoSophia;
	}
}
