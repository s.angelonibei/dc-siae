package com.alkemytech.sophia.codman.sso;

public interface SsoSessionAttrs {

	public static final String USERNAME = "sso.user.userName";
	public static final String GROUPS = "sso.user.groups";
	public static final String TOKEN_ID = "sso.user.tokenID";
	public static final String STATO_PASSWORD = "sso.user.statoPassword";
	public static final String EMAIL = "sso.user.email";
	public static final String DENOMINAZIONE = "sso.user.denominazione";
	public static final String PP_TIPO = "sso.user.puntoPeriferico.tipo";
	public static final String PP_SEPRAG = "sso.user.puntoPeriferico.codiceSeprag";
	public static final String PP_DESCRIZIONE = "sso.user.puntoPeriferico.descrizione";
	public static final String LAST_CHECK = "sso.user.lastCheckTokenMillis";

}
