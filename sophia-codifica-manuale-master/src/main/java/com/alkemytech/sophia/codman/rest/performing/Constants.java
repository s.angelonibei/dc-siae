package com.alkemytech.sophia.codman.rest.performing;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by idilello on 6/9/16.
 */
public class Constants {

	public static Map<String, Object> CONFIGURATIONS  = new HashMap<String, Object>();

    public static final String VOCE_2267 = "2267";
    public static final String VOCE_2244 = "2244";

    public static final String PM_CARTACEO="CARTACEO";

    // Quote Gruppo Principale e Gruppo Spalla
    public static final Double QUOTA_GRUPPO_PRINCIPALE = 0.85D;
    public static final Double QUOTA_GRUPPO_SPALLA = 0.15D;

    // Motivazioni di sospensione (non ricalcolo importo) per PM
    public static final String RIPARTITO_PERIODO_PRECEDENTE = "Esistono Programmi Musicali già ripartiti in periodi precedenti";
    public static final String IMPORTO_MODIFICATO_MANUALMENTE = "Importo modificato manualmente";
    public static final String VOCE_INCASSO_2267 = "Voce Incasso 2267";
    public static final String EVENTO_IMPORTO_INESISTENTE = "Importo pagato per Evento/Voce Incasso non trovato";
    public static final String EVENTO_VOCE_UNICO_PM_ATTESO = "Unico Programma Musicale per Evento/Voce Incasso";
    public static final String ERRORE= "ERRORE";
    public static final String SOSPESI= "SOSPESI";

    // Tipologia di documenti di incasso usati per calcolare gli importi delle singole coppie Evento/Voce
    public static final String DOCUMENTO_ENTRATE = "501";
    public static final String DOCUMENTO_USCITE = "221";

    // Errori durante l'esecuzione
    public static final String SUCCESS_ERROR_CODE="0";
    public static final String SUCCESS_ERROR_DESCR="Success";
    public static final String PERIODO_RICALCOLO_ERRATO_ERROR_CODE="-1";
    public static final String PERIODO_RICALCOLO_ERRATO_ERROR_DESCR="L'inizio del periodo di ricalcolo deve essere successivo alla fine dell'ultimo periodo di ripartizione";
    public static final String ECCEZIONE_ERROR_CODE="-100";

    public static final String INIZIO_PERIODO_RICALCOLO="INIZIO_PERIODO_RICALCOLO";
    public static final String FINE_PERIODO_RICALCOLO="FINE_PERIODO_RICALCOLO";
    public static final String FINE_PERIODO_RIPARTIZIONE="FINE_PERIODO_RIPARTIZIONE";
    public static final String TIPOLOGIA_PM="TIPOLOGIA_PM";
    public static final String CAMPIONAMENTO_DIGITALI="CAMPIONAMENTO_DIGITALI";
    public static final String START_COMMAND="START_COMMAND";
    public static final String PROCESSO1_LISTENER="PROCESSO1_LISTENER";
    public static final String START_ENGINE_URL="START_ENGINE_URL";

    public static final String NUMERO_ENGINE="NUMERO_ENGINE";
    public static final String ENGINE_ADDRESS="ENGINE_ADDRESS";
    public static final String ENGINE_INTERVAL="ENGINE_INTERVAL";
    public static final String AGGREGATE_INTERVAL="AGGREGATE_INTERVAL";

    // Comandi interrogazione processi di backend di calcolo
    public static final String STATUS="STATUS";
    public static final String DEACTIVATE="DEACTIVATE";
    public static final String ACTIVATE="ACTIVATE";

    // Comandi per Valorizzazione
    public static final String START_RICALCOLO="START_RICALCOLO";
    public static final String INFORMAZIONI_ELABORAZIONE="INFORMAZIONI_ELABORAZIONE";
    public static final String SUSPEND_RICALCOLO="SUSPEND_RICALCOLO";
    public static final String RESUME_RICALCOLO="RESUME_RICALCOLO";

    // Comandi per Campionamento
    public static final String START_CAMPIONAMENTO="START_CAMPIONAMENTO";
    public static final String SUSPEND_CAMPIONAMENTO="SUSPEND_CAMPIONAMENTO";
    public static final String RESUME_CAMPIONAMENTO="RESUME_CAMPIONAMENTO";
    public static final String INFORMAZIONI_ELABORAZIONE_CAMPIONAMENTO="INFORMAZIONI_ELABORAZIONE_CAMPIONAMENTO";

    // Comandi per Aggregazione
    public static final String START_AGGREGAZIONE="START_AGGREGAZIONE";
    public static final String INFORMAZIONI_ELABORAZIONE_AGGREGAZIONE="INFORMAZIONI_ELABORAZIONE_AGGREGAZIONE";

    // Stati movimentazione di magazzino dei PM
    public static final String ENTRATO_VALORIZZATORE="ENTRATO_VALORIZZATORE";
    public static final String INVIATO_A_NEED="INVIATO_NEED";
    public static final String RITORNATO_DA_NEED="RITORNATO_DA_NEED";
    public static final String ANNULLATO_DA_ORG="ANNULLATO_DA_ORG";

    // Informazioni necessarie per il trace
    public static final String APPID="VALORIZZATORE_WEB";
    public static final String ERROR="ERROR";
    public static final String INFO="INFO";
    public static final String CLICK="CLICK";
    public static final String ADD="ADD";
    public static final String UPDATE="UPDATE";
    public static final String DELETE="DELETE";
}