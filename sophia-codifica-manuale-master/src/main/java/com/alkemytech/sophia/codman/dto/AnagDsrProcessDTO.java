package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;

import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;

@Produces("application/json")
@XmlRootElement 
public class AnagDsrProcessDTO implements Serializable {


	private static final long serialVersionUID = -6427878972496306849L;
	
	private String idDsrProcess;
	private String name;
	private String description;
	private String configuration;
	
	
	
	public AnagDsrProcessDTO() {

	}
	public String getIdDsrProcess() {
		return idDsrProcess;
	}
	public void setIdDsrProcess(String idDsrProcess) {
		this.idDsrProcess = idDsrProcess;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getConfiguration() {
		return configuration;
	}
	public void setConfiguration(String configuration) {
		this.configuration = configuration;
	}
	

}
