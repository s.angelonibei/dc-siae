package com.alkemytech.sophia.codman.dto;

import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Produces("application/json")
@XmlRootElement 
public class AnagCcidConfigDTO implements Serializable {

	private static final long serialVersionUID = 7790620238873674977L;

	public AnagCcidConfigDTO(){
		
	}

	private Long id;
	private String value;
	private boolean editable;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}
}
