package com.alkemytech.sophia.codman.rest;

import com.alkemytech.sophia.codman.dto.MMSpecialCharCcidDTO;
import com.alkemytech.sophia.codman.entity.MmSpecialCharCcid;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.alkemytech.sophia.codman.utils.QueryUtils;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.xml.bind.DatatypeConverter;
import java.util.ArrayList;
import java.util.List;

import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

@Singleton
@Path("ccid/specialchar")
public class CCIDSpecialCharService {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final String charListDefault = "3B 09 7C 22";
    private final String tabToAscii = "09";
    private final Provider<EntityManager> provider;
    private final Gson gson;
    private final MetadataService metadataService;

    @Inject
    protected CCIDSpecialCharService(@McmdbDataSource Provider<EntityManager> provider,
                                     Gson gson, MetadataService metadataService) {
        super();
        this.provider = provider;
        this.gson = gson;
        this.metadataService = metadataService;
    }

    @GET
    @Path("getSpecialCharConfig/{idDsp}")
    @Produces("application/json")
    public Response getListConfigurations(@PathParam("idDsp") String idDsp) {

        EntityManager em = provider.get();
        MmSpecialCharCcid mscc = new MmSpecialCharCcid();
        try {
            String sql = "SELECT IDDSP,CHAR_LIST,TITLE_LENGTH,ENABLED from MM_SPECIALCHAR_CCID WHERE ENABLED = 'Y' AND IDDSP = '" + idDsp + "'";
            Query q = em.createNativeQuery(sql);
            List<Object[]> o = (List<Object[]>) q.getResultList();
            String str = null;
            for (Object[] ob : o) {
                mscc.setIdDsp((String) ob[0]);
                mscc.setCharList(this.asciiToString((String) ob[1]));
                mscc.setTitleLength(Long.valueOf((Integer) ob[2]));
                mscc.setEnabled((String) ob[3]);
            }
        } catch (Exception e) {
            logger.error("", e);
        }
        return Response.ok(gson.toJson(mscc)).build();
    }

    @GET
    @Path("getList")
    @Produces("application/json")
    public Response getListConfigurations(@QueryParam("dspList") List<String> dspList, @DefaultValue("0") @QueryParam("first") int first,
                                          @DefaultValue("50") @QueryParam("last") int last) {

        EntityManager em = provider.get();
        List<MmSpecialCharCcid> list = new ArrayList<>();
        try {
            String sql = "SELECT IDDSP,CHAR_LIST,TITLE_LENGTH,ENABLED from MM_SPECIALCHAR_CCID WHERE 1=1 ";
            StringBuilder conditions = new StringBuilder();
            if (dspList != null && isNotEmpty(dspList)) {
                conditions.append(String.format(" AND IDDSP in (%s) ", QueryUtils.getValuesCollection(dspList)));
            }
            Query q = em.createNativeQuery(sql + conditions.toString());
            List<Object[]> objects = (List<Object[]>) q.getResultList();
            String str = null;
            for (Object[] o : objects) {
                MmSpecialCharCcid mscc = new MmSpecialCharCcid();
                mscc.setIdDsp((String) o[0]);
                mscc.setCharList((String) o[1]);
                mscc.setTitleLength(Long.valueOf((Integer) o[2]));
                mscc.setEnabled((String) o[3]);
                list.add(mscc);
            }
            for (MmSpecialCharCcid m : list) {
                m.setCharList(this.asciiToString(m.getCharList()));
            }
        } catch (Exception e) {
            logger.error("", e);
        }
        final PagedResult result = new PagedResult();
        if (null != list && !list.isEmpty()) {
            final int maxrows = last - first;
            final boolean hasNext = list.size() > maxrows;
            result.setRows(!hasNext ? list : list.subList(0, maxrows))
                    .setMaxrows(maxrows)
                    .setFirst(first)
                    .setLast(hasNext ? last : first + list.size())
                    .setHasNext(hasNext)
                    .setHasPrev(first > 0);
        } else {
            result.setMaxrows(0)
                    .setFirst(0)
                    .setLast(0)
                    .setHasNext(false)
                    .setHasPrev(false);
        }
        return Response.ok(gson.toJson(result)).build();
    }

    @POST
    @Path("save")
    public Response saveConfigurations(MMSpecialCharCcidDTO dto) {

        EntityManager em = provider.get();
        //Converto i caratteri speciali in ASCII CODE per il salvataggio sul DB
        dto.setCharList(this.stringToAscii(dto.getCharList()));
        //
        try {
            em.getTransaction().begin();
            em.merge(new MmSpecialCharCcid(dto));
            em.getTransaction().commit();
            return Response.status(Response.Status.OK).entity(dto).build();
        } catch (Exception e) {
            em.getTransaction().rollback();
            logger.error("", e);
        }
        return Response.status(Response.Status.NOT_MODIFIED).entity("SAVE FAILED").build();
    }

    @DELETE
    @Path("delete")
    @Produces("application/json")
    public Response deleteListConfigurations(@QueryParam("idDsp") String dsp) {
        EntityManager em = provider.get();
        try {
            em.getTransaction().begin();
            final String sql = new StringBuffer().append("DELETE FROM MCMDB_debug.MM_SPECIALCHAR_CCID ").append("WHERE IDDSP = ? ").toString();
            Query query = em.createNativeQuery(sql);
            query.setParameter(1, dsp);
            query.executeUpdate();
            em.flush();
            em.getTransaction().commit();
            em.clear();
            return Response.ok(gson.toJson(dsp)).build();
        } catch (Exception ex) {
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
            logger.error(String.format("Errore nell'eliminazione del configurazione : %s", gson.toJson(dsp)), ex);
            return Response.status(500).build();
        }
    }

    private String asciiToString(String ascii) {
        String stri = "";
        String[] arr = ascii.split(" ");
        for (String s : arr) {
            if (s.length() > 2) {
                for (int i = 0; i < s.length(); i += 2) {
                    if (i + 2 <= s.length()) {
                        if (s.substring(i, i + 2).equalsIgnoreCase("09")) {
                            stri += "\\t";
                        } else {
                            int hex = Integer.parseInt(s.substring(i, i + 2), 16);
                            char c = (char) hex;
                            stri += c;
                        }
                    }
                }
                stri += " ";
            } else {
                if (s.equalsIgnoreCase("09")) {
                    stri += "\\t" + " ";
                } else {
                    int hex = Integer.parseInt(s, 16);
                    char c = (char) hex;
                    stri += c + " ";
                }
            }
        }
        return stri.substring(0, stri.length() - 1);
    }

    private String stringToAscii(String charlist) {
        String results = "";
        try{
            String c = charlist.replace("\\t", "\t");
            String[] subSets = c.split(" ");
            for (String sub : subSets) {
                byte[] bytes = sub.getBytes("US-ASCII");
                for (byte b : bytes) {
                    results +=  String.format("%02X", b);
                }
                results += " ";
            }
        } catch (Exception e){
             logger.error("", e);
        }
        // results = results.replace("20", " ");
        return results;
    }

}