package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;

public class FormatMetadataDTO implements Serializable {


	private static final long serialVersionUID = -18311251112421433L;
	String code;
	String description;
	
	public FormatMetadataDTO() {
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
}
