package com.alkemytech.sophia.codman.entity.performing;

//TODO:see if this import needs migration
//import it.siae.valorizzatore.utility.Utility;

import com.alkemytech.sophia.codman.utils.AccountingUtils;
import com.alkemytech.sophia.codman.utils.DateUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by idilello on 6/7/16.
 */
@Entity
@Table(name = "PERF_EVENTI_PAGATI")
public class EventiPagati implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
    private Long idEvento;
    private String permesso;
    private Integer contabilita;
    private String numeroFattura;
    private String voceIncasso;
    private Date dataInizioEvento;
    private String oraInizioEvento;
    private String denominazioneLocale;
    private String tipoDocumentoContabile;
    private Double importDem=0D;;
    private Double importDemNetto=0D;;
    private String seprag;
    private Integer contabilitaOrg;
    private String reversale;
    private String codiceBaLocale;
    private String codiceSpeiLocale;
    private Long numeroPmTotaliAttesi;
    private Long numeroPmTotaliAttesiSpalla;
    private Date dataIns;
    private String sede;
    private String agenzia;
    private Date dataReversale;
    private Manifestazione manifestazione;
    private List<MovimentoContabile> movimentazioniPM = new ArrayList<MovimentoContabile>();
    private List<DettaglioPM> dettaglioPM;
    private NDMVoceFattura ndmVoceFattura;
    private Double importoAggio=0D;;
    private String quadraturaNDM;
    private String forzaturaNDM;
    private Double importoExArt=0D;;
    
    private Double importoSophiaNetto;
    private Double importoSophiaNettoExArt;
    private BigDecimal aggioSottosoglia;
    private BigDecimal aggioSoprasoglia;

    private Date dataFatturaDa;
    private Date dataFatturaA;
    private Date dataEventoDa;
    private Date dataEventoA;

    @Id
//    @SequenceGenerator(name="EVENTI_PAGATI",sequenceName = "EVENTI_PAGATI_SEQ",allocationSize=1)
//    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="EVENTI_PAGATI")
    @Column(name = "ID_EVENTO_PAGATO", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    @Column(name = "SEPRAG")
    public String getSeprag() {
        return seprag;
    }

    public void setSeprag(String seprag) {
        this.seprag = seprag;
    }

    @Column(name = "CONTABILITA")
    public Integer getContabilita() {
        return contabilita;
    }

    public void setContabilita(Integer contabilita) {
        this.contabilita = contabilita;
    }

    @Column(name = "CONTABILITA_ORIG")
    public Integer getContabilitaOrg() {
        return contabilitaOrg;
    }

    public void setContabilitaOrg(Integer contabilitaOrg) {
        this.contabilitaOrg = contabilitaOrg;
    }


    @Column(name = "NUMERO_REVERSALE")
    public String getReversale() {
        return reversale;
    }

    public void setReversale(String reversale) {
        this.reversale = reversale;
    }


    @Column(name = "NUMERO_FATTURA")
    public String getNumeroFattura() {
        return numeroFattura;
    }

    public void setNumeroFattura(String numeroFattura) {
        this.numeroFattura = numeroFattura;
    }

    @Column(name = "VOCE_INCASSO")
    public String getVoceIncasso() {
        return voceIncasso;
    }

    public void setVoceIncasso(String voceIncasso) {
        this.voceIncasso = voceIncasso;
    }

    @Column(name = "TIPO_DOCUMENTO_CONTABILE")
    public String getTipoDocumentoContabile() {
        return tipoDocumentoContabile;
    }

    public void setTipoDocumentoContabile(String tipoDocumentoContabile) {
        this.tipoDocumentoContabile = tipoDocumentoContabile;
    }


    @Column(name = "DENOMINAZIONE_LOCALE")
    public String getDenominazioneLocale() {
        return denominazioneLocale;
    }

    public void setDenominazioneLocale(String denominazioneLocale) {
        this.denominazioneLocale = denominazioneLocale;
    }

    @Column(name = "CODICE_BA_LOCALE")
    public String getCodiceBaLocale() {
        return codiceBaLocale;
    }

    public void setCodiceBaLocale(String codiceBaLocale) {
        this.codiceBaLocale = codiceBaLocale;
    }

    @Column(name = "CODICE_SPEI_LOCALE")
    public String getCodiceSpeiLocale() {
        return codiceSpeiLocale;
    }

    public void setCodiceSpeiLocale(String codiceSpeiLocale) {
        this.codiceSpeiLocale = codiceSpeiLocale;
    }


    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_INIZIO_EVENTO")
    public Date getDataInizioEvento() {
        return dataInizioEvento;
    }

    public void setDataInizioEvento(Date dataInizioEvento) {
        this.dataInizioEvento = dataInizioEvento;
    }

    @Column(name = "ORA_INIZIO_EVENTO")
    public String getOraInizioEvento() {
        return oraInizioEvento;
    }

    public void setOraInizioEvento(String oraInizioEvento) {
        this.oraInizioEvento = oraInizioEvento;
    }

    @Column(name = "NUMERO_PM_ATTESI")
    public Long getNumeroPmTotaliAttesi() {
        if (numeroPmTotaliAttesi==null)
            numeroPmTotaliAttesi=1L;
        return numeroPmTotaliAttesi;
    }

    public void setNumeroPmTotaliAttesi(Long numeroPmTotaliAttesi) {
        this.numeroPmTotaliAttesi = numeroPmTotaliAttesi;
    }

    @Column(name = "IMPORTO_DEM")
    public Double getImportDem() {
        if (importDem != null)
        	//TODO: Utilility.round how to migrate?
           return AccountingUtils.round(importDem,7);
        else
           return importDem;
    }

    public void setImportDem(Double importDem) {
        this.importDem = importDem;
    }
    

    @Column(name = "IMPORTO_DEM_NETTO")
    public Double getImportDemNetto() {
		return importDemNetto;
	}

	public void setImportDemNetto(Double importDemNetto) {
		this.importDemNetto = importDemNetto;
	}

	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_INS")
    public Date getDataIns() {
        return dataIns;
    }

    public void setDataIns(Date dataIns) {
        this.dataIns = dataIns;
    }

    @Column(name = "ID_EVENTO")
    public Long getIdEvento() {
        return idEvento;
    }

    public void setIdEvento(Long idEvento) {
        this.idEvento = idEvento;
    }


    @Column(name = "NUMERO_PM_ATTESI_SPALLA")
    public Long getNumeroPmTotaliAttesiSpalla() {
        if (numeroPmTotaliAttesiSpalla==null)
            numeroPmTotaliAttesiSpalla=1L;
        return numeroPmTotaliAttesiSpalla;
    }

    public void setNumeroPmTotaliAttesiSpalla(Long numeroPmTotaliAttesiSpalla) {
        this.numeroPmTotaliAttesiSpalla = numeroPmTotaliAttesiSpalla;
    }

	@Temporal(TemporalType.DATE)
    @Column(name = "DATA_REVERSALE")
    public Date getDataReversale() {
		return dataReversale;
	}

	public void setDataReversale(Date dataReversale) {
		this.dataReversale = dataReversale;
	}

	@Transient
    public String getDataInizioEventoStr(){
//        return Utility.dateString(dataInizioEvento, Utility.OracleFormat_D);
        return DateUtils.dateToString(dataInizioEvento, DateUtils.DATE_ITA_FORMAT_SLASH);
    }

    @Transient
    public Manifestazione getManifestazione() {
        return manifestazione;
    }

    public void setManifestazione(Manifestazione manifestazione) {
        this.manifestazione = manifestazione;
    }

    @Transient
    public List<MovimentoContabile> getMovimentazioniPM() {
        return movimentazioniPM;
    }

    public void setMovimentazioniPM(List<MovimentoContabile> movimentazioniPM) {
        this.movimentazioniPM = movimentazioniPM;
    }

    @Transient
    public List<DettaglioPM> getDettaglioPM() {
        return dettaglioPM;
    }

    public void setDettaglioPM(List<DettaglioPM> dettaglioPM) {
        this.dettaglioPM = dettaglioPM;
    }

    public String getImportoDemFormatted() {
        return AccountingUtils.displayFormatted(importDem);
    }

    @Column(name = "PERMESSO")
	public String getPermesso() {
		return permesso;
	}

	public void setPermesso(String permesso) {
		this.permesso = permesso;
	}

    @Column(name = "SEDE")
	public String getSede() {
		return sede;
	}

	public void setSede(String sede) {
		this.sede = sede;
	}

    @Column(name = "AGENZIA")
	public String getAgenzia() {
		return agenzia;
	}

	public void setAgenzia(String agenzia) {
		this.agenzia = agenzia;
	}

    @Transient
	public NDMVoceFattura getNdmVoceFattura() {
		return ndmVoceFattura;
	}

	public void setNdmVoceFattura(NDMVoceFattura ndmVoceFattura) {
		this.ndmVoceFattura = ndmVoceFattura;
	}

    @Column(name = "IMPORTO_AGGIO")
	public Double getImportoAggio() {
		return importoAggio;
	}

	public void setImportoAggio(Double importoAggio) {
		this.importoAggio = importoAggio;
	}

    @Column(name = "QUADRATURA_NDM")
	public String getQuadraturaNDM() {
		return quadraturaNDM;
	}

	public void setQuadraturaNDM(String quadraturaNDM) {
		this.quadraturaNDM = quadraturaNDM;
	}

    @Column(name = "IMPORTO_EX_ART_18")
	public Double getImportoExArt() {
		return importoExArt;
	}

	public void setImportoExArt(Double importoExArt) {
		this.importoExArt = importoExArt;
	}

	public Double getImportoSophiaNetto() {
		return importoSophiaNetto;
	}

	public void setImportoSophiaNetto() {
		this.importoSophiaNetto = importDem-importoAggio;
				if(this.importoExArt !=null)
					this.importoSophiaNetto=importDem-importoAggio-importoExArt;
	}

	@Transient
    public Date getDataFatturaDa() {
        return dataFatturaDa;
    }

    public void setDataFatturaDa(Date dataFatturaDa) {
        this.dataFatturaDa = dataFatturaDa;
    }

    @Transient
    public Date getDataFatturaA() {
        return dataFatturaA;
    }

    public void setDataFatturaA(Date dataFatturaA) {
        this.dataFatturaA = dataFatturaA;
    }

    @Transient
    public Date getDataEventoDa() {
        return dataEventoDa;
    }

    public void setDataEventoDa(Date dataEventoDa) {
        this.dataEventoDa = dataEventoDa;
    }

    @Transient
    public Date getDataEventoA() {
        return dataEventoA;
    }

    public void setDataEventoA(Date dataEventoA) {
        this.dataEventoA = dataEventoA;
    }

    public Double getImportoSophiaNettoExArt() {
		return importoSophiaNettoExArt;
	}

	public void setImportoSophiaNettoExArt() {
		this.importoSophiaNettoExArt = importDem-importoAggio-importoExArt;
	}

    @Transient
	public BigDecimal getAggioSottosoglia() {
		return aggioSottosoglia;
	}

	public void setAggioSottosoglia(BigDecimal aggioSottosoglia) {
		this.aggioSottosoglia = aggioSottosoglia;
	}

    @Transient
	public BigDecimal getAggioSoprasoglia() {
		return aggioSoprasoglia;
	}

	public void setAggioSoprasoglia(BigDecimal aggioSoprasoglia) {
		this.aggioSoprasoglia = aggioSoprasoglia;
	}
	@Column(name = "FORZATURA_NDM")
	public String getForzaturaNDM() {
		return forzaturaNDM;
	}

	public void setForzaturaNDM(String forzaturaNDM) {
		this.forzaturaNDM = forzaturaNDM;
	}

}
