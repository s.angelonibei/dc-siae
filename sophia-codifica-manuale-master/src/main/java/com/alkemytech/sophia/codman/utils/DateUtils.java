package com.alkemytech.sophia.codman.utils;

import org.joda.time.Duration;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by Alessandro Russo on 28/11/2017.
 */
public class DateUtils {
    //DATE FORMAT
    public static final String DATE_ENG_FORMAT_SLASH        = "MM/dd/yyyy";
    public static final String DATE_ENG_FORMAT_DASH         = "MM-dd-yyyy";
    public static final String DATE_ITA_FORMAT_SLASH        = "dd/MM/yyyy";
    public static final String DATE_ITA_FORMAT_DASH         = "dd-MM-yyyy";

    //DATETIME FORMAT
    public static final String DATETIME_ENG_FORMAT_SLASH    = "MM/dd/yyyy HH:mm:ss";
    public static final String DATETIME_ENG_FORMAT_DASH     = "MM-dd-yyyy HH:mm:ss";
    public static final String DATETIME_ITA_FORMAT_SLASH    = "dd/MM/yyyy HH:mm:ss";
    public static final String DATETIME_ITA_FORMAT_DASH     = "dd-MM-yyyy HH:mm:ss";
    public static final String DATETIME_MYSQL_FORMAT        = "yyyy-MM-dd HH:mm:ss";

    //TIME FORMAT
    public static final String TIME_SIMPLE_FORMAT           = "HH:mm";
    public static final String TIME_COMPLETE_FORMAT         = "HH:mm:ss";

    public static String dateToString (Date date, String format) {
        DateFormat df = new SimpleDateFormat(format);
        return df.format(date);
    }

    public static Date stringToDate (String dateString, String format) {
        DateFormat df = new SimpleDateFormat(format);
        try {
            return df.parse(dateString);
        } catch (ParseException e) {
            return null;
        }
    }

    public static Date manageDate (Date inputDate, int quantity, int timeUnit){
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(inputDate);
        switch (timeUnit){
            case Calendar.SECOND:
                calendar.add(Calendar.SECOND, quantity);
                break;
            case Calendar.MINUTE:
                calendar.add(Calendar.MINUTE, quantity);
                break;
            case Calendar.HOUR_OF_DAY:
                calendar.add(Calendar.HOUR_OF_DAY, quantity);
                break;
            case Calendar.DAY_OF_MONTH:
                calendar.add(Calendar.DAY_OF_MONTH, quantity);
                break;
        }
        return calendar.getTime();
    }

    public static String formatDurationInSeconds(BigInteger seconds){
        Duration duration = new Duration((seconds != null ? seconds.multiply(BigInteger.valueOf(1000)).longValue() : Duration.ZERO)); // in milliseconds
        PeriodFormatter formatter = new PeriodFormatterBuilder()
                .printZeroAlways()
                .minimumPrintedDigits(2)
                .appendHours()
                .appendSeparator(":")
                .printZeroAlways()
                .minimumPrintedDigits(2)
                .appendMinutes()
                .appendSeparator(":")
                .printZeroAlways()
                .minimumPrintedDigits(2)
                .appendSeconds().toFormatter();
        return formatter.print(duration.toPeriod());
    }

    public static void main (String[] args) throws Exception{
        Date test = new Date();
        System.out.println("INPUT DATE -> "+dateToString(test, DATETIME_ITA_FORMAT_SLASH));
//        int quantity = -1;
//        int timeUnit = Calendar.DAY_OF_MONTH;
//        Date ret = manageDate(test, quantity, timeUnit);
//        System.out.println("OUTPUT DATE -> "+dateToString(ret, DATETIME_ITA_FORMAT_SLASH));
        System.out.println("OUTPUT DATE -> "+dateToString(test, TIME_SIMPLE_FORMAT));
    }

}
