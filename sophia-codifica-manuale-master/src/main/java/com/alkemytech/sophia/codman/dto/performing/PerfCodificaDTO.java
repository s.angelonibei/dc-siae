package com.alkemytech.sophia.codman.dto.performing;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PerfCodificaDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long idCodifica;

	private String candidate;

	private String codiceOperaApprovato;

	private String codiceOperaSuggerito;

	private BigDecimal confidenza;

	private Date dataApprovazione;

	private Date dataCodifica;

	private Date dataFineValidita;

	private BigDecimal distSecCandidata;

	private BigInteger nUtilizzazioni;

	private Boolean preziosa;

	private String statoApprovazione;

	private String tipoApprovazione;

	private BigDecimal valoreEconomico;

	public PerfCodificaDTO() {
	}

	public PerfCodificaDTO(Long idCodifica, String candidate, String codiceOperaApprovato, String codiceOperaSuggerito, BigDecimal confidenza, Date dataApprovazione, Date dataCodifica, Date dataFineValidita, BigDecimal distSecCandidata, BigInteger nUtilizzazioni, Boolean preziosa, String statoApprovazione, String tipoApprovazione, BigDecimal valoreEconomico) {
		this.idCodifica = idCodifica;
		this.candidate = candidate;
		this.codiceOperaApprovato = codiceOperaApprovato;
		this.codiceOperaSuggerito = codiceOperaSuggerito;
		this.confidenza = confidenza;
		this.dataApprovazione = dataApprovazione;
		this.dataCodifica = dataCodifica;
		this.dataFineValidita = dataFineValidita;
		this.distSecCandidata = distSecCandidata;
		this.nUtilizzazioni = nUtilizzazioni;
		this.preziosa = preziosa;
		this.statoApprovazione = statoApprovazione;
		this.tipoApprovazione = tipoApprovazione;
		this.valoreEconomico = valoreEconomico;
	}

	public Long getIdCodifica() {
		return idCodifica;
	}

	public void setIdCodifica(Long idCodifica) {
		this.idCodifica = idCodifica;
	}

	public String getCandidate() {
		return candidate;
	}

	public void setCandidate(String candidate) {
		this.candidate = candidate;
	}

	public String getCodiceOperaApprovato() {
		return codiceOperaApprovato;
	}

	public void setCodiceOperaApprovato(String codiceOperaApprovato) {
		this.codiceOperaApprovato = codiceOperaApprovato;
	}

	public String getCodiceOperaSuggerito() {
		return codiceOperaSuggerito;
	}

	public void setCodiceOperaSuggerito(String codiceOperaSuggerito) {
		this.codiceOperaSuggerito = codiceOperaSuggerito;
	}

	public BigDecimal getConfidenza() {
		return confidenza;
	}

	public void setConfidenza(BigDecimal confidenza) {
		this.confidenza = confidenza;
	}

	public Date getDataApprovazione() {
		return dataApprovazione;
	}

	public void setDataApprovazione(Date dataApprovazione) {
		this.dataApprovazione = dataApprovazione;
	}

	public Date getDataCodifica() {
		return dataCodifica;
	}

	public void setDataCodifica(Date dataCodifica) {
		this.dataCodifica = dataCodifica;
	}

	public Date getDataFineValidita() {
		return dataFineValidita;
	}

	public void setDataFineValidita(Date dataFineValidita) {
		this.dataFineValidita = dataFineValidita;
	}

	public BigDecimal getDistSecCandidata() {
		return distSecCandidata;
	}

	public void setDistSecCandidata(BigDecimal distSecCandidata) {
		this.distSecCandidata = distSecCandidata;
	}

	public BigInteger getnUtilizzazioni() {
		return nUtilizzazioni;
	}

	public void setnUtilizzazioni(BigInteger nUtilizzazioni) {
		this.nUtilizzazioni = nUtilizzazioni;
	}

	public Boolean getPreziosa() {
		return preziosa;
	}

	public void setPreziosa(Boolean preziosa) {
		this.preziosa = preziosa;
	}

	public String getStatoApprovazione() {
		return statoApprovazione;
	}

	public void setStatoApprovazione(String statoApprovazione) {
		this.statoApprovazione = statoApprovazione;
	}

	public String getTipoApprovazione() {
		return tipoApprovazione;
	}

	public void setTipoApprovazione(String tipoApprovazione) {
		this.tipoApprovazione = tipoApprovazione;
	}

	public BigDecimal getValoreEconomico() {
		return valoreEconomico;
	}

	public void setValoreEconomico(BigDecimal valoreEconomico) {
		this.valoreEconomico = valoreEconomico;
	}
}