package com.alkemytech.sophia.codman.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.codman.dto.AnagClientDTO;
import com.alkemytech.sophia.codman.dto.AnagClientModifiedDTO;
import com.alkemytech.sophia.codman.dto.DspDTO;
import com.alkemytech.sophia.codman.entity.AnagClient;
import com.alkemytech.sophia.codman.entity.AnagDsp;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
// import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

@Singleton
@Path("anagClient")
public class AnagClientService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Provider<EntityManager> provider;
	private final Gson gson;

	@Inject
	protected AnagClientService(@McmdbDataSource Provider<EntityManager> provider, Gson gson) {
		super();
		this.provider = provider;
		this.gson = gson;
	}

	@GET
	@Path("all")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response all(@DefaultValue("0") @QueryParam("first") int first,
			@DefaultValue("50") @QueryParam("last") int last, @DefaultValue("") @QueryParam("dsp") String dsp,
			@DefaultValue("") @QueryParam("country") String country,
			@DefaultValue("") @QueryParam("sapCode") String sapCode) {
		EntityManager entityManager = null;
		List<AnagClientDTO> result;

		List<String> conditions = new ArrayList<>();
		if (!dsp.isEmpty()) {
			conditions.add(" x.idDsp = \'" + dsp + "\'");
		}
		if (!country.isEmpty()) {
			conditions.add(" x.country = \'" + country + "\'");
		}
		if(!sapCode.isEmpty()){
			conditions.add(" x.code = \'" + sapCode + "\'");	
		}
		
		String whereCondition = "";
		for (String cond : conditions) {
			if (whereCondition.isEmpty()) {
				whereCondition += " where " + cond;
			} else {
				whereCondition += " and " + cond;
			}
		}

		try {
			entityManager = provider.get();
			final Query q = entityManager.createQuery("select x from AnagClient x" + whereCondition).setFirstResult(first) // offset
					.setMaxResults(1 + last - first);

			List<AnagClient> clientEntities = (List<AnagClient>) q.getResultList();

			final Query q2 = entityManager.createQuery("select x from AnagDsp x");

			List<AnagDsp> dspEntities = (List<AnagDsp>) q2.getResultList();
			Map<String, AnagDsp> anagDspMap = new HashMap<>();
			for (AnagDsp anagDsp : dspEntities) {
				anagDspMap.put(anagDsp.getIdDsp(), anagDsp);
			}

			result = getClientsDto(clientEntities, anagDspMap);

			final PagedResult pagedResult = new PagedResult();
			if (null != result) {
				final int maxrows = last - first;
				final boolean hasNext = result.size() > maxrows;
				pagedResult.setRows((hasNext ? result.subList(0, maxrows) : result)).setMaxrows(maxrows).setFirst(first)
						.setLast(hasNext ? last : first + result.size()).setHasNext(hasNext).setHasPrev(first > 0);
			} else {
				pagedResult.setMaxrows(0).setFirst(0).setLast(0).setHasNext(false).setHasPrev(false);
			}

			return Response.ok(gson.toJson(pagedResult)).build();
		} catch (Exception e) {
			logger.error("all", e);
		}
		return Response.status(500).build();
	}

	private List<AnagClientDTO> getClientsDto(List<AnagClient> clientEntities, Map<String, AnagDsp> anagDspMap) {
		List<AnagClientDTO> result = new ArrayList<>();
		for (AnagClient anagClient : clientEntities) {
			AnagClientDTO dto = new AnagClientDTO(anagClient);
			dto.setDspName(anagDspMap.containsKey(anagClient.getIdDsp())
					? anagDspMap.get(anagClient.getIdDsp()).getName() : "");
			result.add(dto);
		}
		return result;
	}

	@POST
	@Path("")
	public Response add(AnagClientDTO dto) {

		Status status = Status.INTERNAL_SERVER_ERROR;
		final EntityManager entityManager = provider.get();

		try {
			final Query q = entityManager.createQuery("select x from AnagClient x where x.idDsp =:dsp ");
			q.setParameter("dsp", dto.getIdDsp());
			q.getSingleResult();
			status = Status.CONFLICT;
			return Response.status(status).build();

		} catch (Exception e) {
			AnagClient clientEntity = new AnagClient();
			clientEntity.setCode(dto.getCode());
			clientEntity.setCompanyName(dto.getCompanyName());
			clientEntity.setCountry(dto.getCountry());
			clientEntity.setForeignClient(dto.getForeignClient());
			clientEntity.setIdDsp(dto.getIdDsp());
			clientEntity.setLicence(dto.getLicence());
			clientEntity.setVatCode(dto.getVatCode());
			clientEntity.setVatDescription(dto.getVatDescription());
			clientEntity.setStartDate(new Date());

			try {
				entityManager.getTransaction().begin();
				entityManager.persist(clientEntity);
				entityManager.getTransaction().commit();
				return Response.ok().build();

			} catch (Exception ex) {
				logger.error("persist", ex);

				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}

				return Response.status(status).build();
			}

		}

	}

	@PUT
	@Path("")
	public Response update(AnagClientModifiedDTO dto) {

		if (dto.getPrevCode().equals(dto.getCode()) && dto.getPrevCompanyName().equals(dto.getCompanyName())
				&& dto.getPrevCountry().equals(dto.getCountry()) && dto.getPrevVatCode().equals(dto.getVatCode())
				&& dto.getPrevLicences().equals(dto.getLicences())
				&& ((dto.getPrevVatDescription() == null && dto.getVatDescription() == null)
						|| (dto.getPrevVatDescription() != null
								&& dto.getPrevVatDescription().equals(dto.getVatDescription())))) {
			return Response.ok().build();
		}

		final EntityManager entityManager = provider.get();
		try {

			List<String> updateConditions = new ArrayList<>();
			boolean codeChanged = !dto.getPrevCode().equals(dto.getCode());
			boolean companyNameChanged = !dto.getPrevCompanyName().equals(dto.getCompanyName());
			boolean countryChanged = !dto.getPrevCountry().equals(dto.getCountry());
			boolean vatCodeChanged = !dto.getPrevVatCode().equals(dto.getVatCode());
			boolean licencesChanged = !dto.getPrevLicences().equals(dto.getLicences());
			boolean vatDescriptionChanged = (dto.getPrevVatDescription() == null && dto.getVatDescription() != null)
					|| (dto.getPrevVatDescription() != null
							&& !dto.getPrevVatDescription().equals(dto.getVatDescription()));

			updateConditions.add(codeChanged ? " x.code = :code " : "");
			updateConditions.add(companyNameChanged ? " x.companyName = :companyName " : "");
			updateConditions.add(countryChanged ? " x.country = :country " : "");
			updateConditions.add(
					countryChanged && dto.getCountry().equals("ITA") ? "x.foreignClient = 0 " : "x.foreignClient = 1");
			updateConditions.add(vatCodeChanged ? " x.vatCode = :vatCode " : "");
			updateConditions.add(vatDescriptionChanged ? " x.vatDescription = :vatDescription " : "");
			updateConditions.add(licencesChanged ? " x.licence = :licence " : "");

			String updateString = "";
			for (String condition : updateConditions) {
				updateString += !updateString.isEmpty() && !condition.isEmpty() ? ", " : "";
				updateString += condition;
			}

			entityManager.getTransaction().begin();

			final Query query = entityManager
					.createQuery("update AnagClient x set " + updateString + "  where x.idDsp = :idDsp ");

			query.setParameter("idDsp", dto.getIdDsp());

			if (codeChanged) {
				query.setParameter("code", dto.getCode());
			}
			if (companyNameChanged) {
				query.setParameter("companyName", dto.getCompanyName());
			}
			if (countryChanged) {
				query.setParameter("country", dto.getCountry());
			}
			if (vatCodeChanged) {
				query.setParameter("vatCode", dto.getVatCode());
			}

			if (vatDescriptionChanged) {
				query.setParameter("vatDescription", dto.getVatDescription());
			}
			if (licencesChanged) {
				query.setParameter("licence", dto.getLicences());
			}

			query.executeUpdate();
			entityManager.getTransaction().commit();

			return Response.ok().build();

		} catch (Exception e) {
			logger.error("update", e);
			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
		}
		return Response.status(500).build();
	}

	@DELETE
	@Path("")
	public Response delete(@QueryParam("idAnagClient") int idAnagClient) {
		final EntityManager entityManager = provider.get();
		try {
			AnagClient client = entityManager.find(AnagClient.class, idAnagClient);
			entityManager.getTransaction().begin();
			entityManager.remove(client);
			entityManager.getTransaction().commit();

			return Response.ok().build();

		} catch (Exception e) {
			logger.error("delete", e);
			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
		}
		return Response.status(500).build();
	}

}
