package com.alkemytech.sophia.codman.guice;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;

import com.alkemytech.sophia.codman.blacklist.BlacklistValidator;
import com.alkemytech.sophia.codman.blacklist.RegexBlacklistValidator;
import com.alkemytech.sophia.codman.invoice.repository.InvoiceRepository;
import com.alkemytech.sophia.codman.invoice.repository.InvoiceRepositoryInterface;
import com.alkemytech.sophia.codman.invoice.service.InvoiceService;
import com.alkemytech.sophia.codman.invoice.service.InvoiceServiceInterface;
import com.alkemytech.sophia.codman.servlet.LogoutServlet;
import com.alkemytech.sophia.codman.servlet.S3CCIDDownloadServlet;
import com.alkemytech.sophia.codman.servlet.S3ExtendedDSRDownloadServlet;
import com.alkemytech.sophia.codman.servlet.SimpleLoginServlet;
import com.alkemytech.sophia.codman.servlet.SimpleLogoutServlet;
import com.alkemytech.sophia.codman.sso.AasAuthenticationFilter;
import com.alkemytech.sophia.codman.sso.AuthorizationFilter;
import com.alkemytech.sophia.codman.sso.NoAuthenticationFilter;
import com.alkemytech.sophia.codman.sso.SimpleAuthenticationFilter;
import com.alkemytech.sophia.common.config.ConfigurationLoader;
import com.alkemytech.sophia.common.emr.EMRService;
import com.alkemytech.sophia.common.s3.S3Service;
import com.alkemytech.sophia.common.sqs.SQSService;
import com.alkemytech.sophia.common.tools.Log4j2Tools;
import com.alkemytech.sophia.commons.aws.SQS;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.Scopes;
import com.google.inject.name.Names;
import com.google.inject.servlet.ServletModule;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;

public class ApplicationServletModule extends ServletModule {
	
	private final String configuration;
	
	protected ApplicationServletModule(String configuration) {
		super();
		this.configuration = configuration;
	}

	@Override
	protected void configureServlets() {
		super.configureServlets();
		final Properties properties;
		// properties
		if(configuration != null) {
			properties = new ConfigurationLoader()
					.withFilePath(configuration).load();
		} else {
			properties = new ConfigurationLoader()
					.withResourceName("configuration.properties")
					.load();
		}
		Names.bindProperties(binder(), properties);
		bind(Properties.class)
			.annotatedWith(Names.named("configuration"))
			.toInstance(properties);

		// default time zone
		TimeZone.setDefault(TimeZone.getTimeZone(properties.getProperty("defaultTimezone", "UTC")));
		
		// initialize log4j
		Log4j2Tools.initialize(properties);

		// common
		bind(Gson.class).toInstance(new GsonBuilder()
			.disableHtmlEscaping()
			.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
//			.setPrettyPrinting()
			.create());
		bind(BlacklistValidator.class)
			.to(RegexBlacklistValidator.class)
			.asEagerSingleton();
		
		// dao(s)
//		bind(new TypeLiteral<GenericDAO<FuzzyMonitoring, String>>() { })
//			.to(new TypeLiteral<FuzzyMonitoringDAO>() { });

		// jpa		
//		install(new JpaPersistModule("default"));
//		requireBinding(EntityManager.class);
//		filter("/*").through(PersistFilter.class);

		// jpa with multiple databases
		install(new McmdbJpaModule(properties));
		install(new SophiaKbJpaModule(properties));
		install(new UnidentifiedJpaModule(properties));
		filter("/*").through(McmdbJpaModule.FILTER_KEY);
		filter("/*").through(SophiaKbJpaModule.FILTER_KEY);
		filter("/*").through(UnidentifiedJpaModule.FILTER_KEY);
		
		// authentication & authorization
		if ("true".equalsIgnoreCase(properties.getProperty("sso.aas.enabled"))) {
			bind(AasAuthenticationFilter.class).asEagerSingleton();
			filterRegex("^((?!" +
				"^/bootstrap/.+|" +
				"^/css/.+|" +
				"^/images/.+|" +
				"^/js/.+" +
				").)*$").through(AasAuthenticationFilter.class);
		} else if ("true".equalsIgnoreCase(properties.getProperty("sso.auth.enabled"))) {
			bind(SimpleAuthenticationFilter.class).asEagerSingleton();
			filterRegex("^((?!" +
				"^/rest/integrazione/.+|"+
				"^/bootstrap/.+|" +
				"^/css/.+|" +
				"^/images/.+|" +
				"^/js/.+|" +
				"^/login|" + // login service
				"^/pages/login.jsp" + // login form
				").)*$").through(SimpleAuthenticationFilter.class);

			bind(NoAuthenticationFilter.class).asEagerSingleton();
			filterRegex("^/rest/integrazione/.+$").through(NoAuthenticationFilter.class);			
			
		} else {
			bind(NoAuthenticationFilter.class).asEagerSingleton();
			filterRegex("^((?!" +
							"^/bootstrap/.+|" +
							"^/css/.+|" +
							"^/images/.+|" +
							"^/js/.+" +
							").)*$").through(NoAuthenticationFilter.class);			
		}
		bind(AuthorizationFilter.class).asEagerSingleton();
		filter("/*").through(AuthorizationFilter.class);

		// jersey servlet (rest)
		final Map<String, String> params = new HashMap<String, String>();
		params.put(PackagesResourceConfig.PROPERTY_PACKAGES,
						"com.alkemytech.sophia.codman.rest," +
						"com.alkemytech.sophia.codman.invoice.rest," +
						"com.alkemytech.sophia.codman.broadcasting," +
						"com.alkemytech.sophia.codman.gestioneperiodi," +
						"com.alkemytech.sophia.codman.multimedialelocale ");
		// list of packages separated by commas
		serve("/rest/*").with(GuiceContainer.class, params);

		// AWS service(s)
		bind(S3Service.class).asEagerSingleton();
		bind(SQSService.class).asEagerSingleton();
		bind(InvoiceServiceInterface.class).to(InvoiceService.class).asEagerSingleton();
		bind(InvoiceRepositoryInterface.class).to(InvoiceRepository.class).asEagerSingleton();
		bind(EMRService.class).asEagerSingleton();

		bind(S3CCIDDownloadServlet.class).in(Scopes.SINGLETON);
		serve("/downloadCCID").with(S3CCIDDownloadServlet.class);
		bind(S3ExtendedDSRDownloadServlet.class).in(Scopes.SINGLETON);
		serve("/downloadExtendedDSR").with(S3ExtendedDSRDownloadServlet.class);

		// servlet(s)
		if ("true".equalsIgnoreCase(properties.getProperty("sso.aas.enabled"))) {
			bind(LogoutServlet.class).in(Scopes.SINGLETON);
			serve("/logout").with(LogoutServlet.class);
		} else if ("true".equalsIgnoreCase(properties.getProperty("sso.auth.enabled"))) {
			bind(SimpleLoginServlet.class).in(Scopes.SINGLETON);
			serve("/login").with(SimpleLoginServlet.class);
			bind(SimpleLogoutServlet.class).in(Scopes.SINGLETON);
			serve("/logout").with(SimpleLogoutServlet.class);
		}

		bind(SQS.class).toInstance(new SQS(properties).startup());
	}
}
