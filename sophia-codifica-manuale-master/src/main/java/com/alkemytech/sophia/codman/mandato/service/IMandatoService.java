package com.alkemytech.sophia.codman.mandato.service;

import java.util.List;

import com.alkemytech.sophia.codman.dto.MandatoDTO;

public interface IMandatoService {
	
	public MandatoDTO get(Integer id);

    public List<MandatoDTO> getAll();
    
    public MandatoDTO save(MandatoDTO mandato);
    
    public void delete(MandatoDTO mandato);	

}
