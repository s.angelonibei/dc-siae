package com.alkemytech.sophia.codman.broadcasting;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.broadcasting.model.BdcRuoli;
import com.alkemytech.sophia.broadcasting.service.interfaces.BdcRuoliService;
import com.amazonaws.util.CollectionUtils;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Created by Alessandro Ravà on 14/12/2017.
 */
@Singleton
@Path("ruoli")
public class RuoliRestService {


    private final static String NO_RESULTS = "No results found";

    private final Logger logger = LoggerFactory.getLogger(getClass());
	private BdcRuoliService service;
	private Gson gson;

	@Inject
	public RuoliRestService(BdcRuoliService service, Gson gson) {
		this.service = service;
		this.gson = gson;
	}

	@GET
	@Path("allRole")
	@Produces("application/json")
	public Response findRole() {
		try {
			List<BdcRuoli> result = service.findAll();
			if (!CollectionUtils.isNullOrEmpty(result)) {
				return Response.ok(gson.toJson(result)).build();
			} else {
				return Response.ok(gson.toJson(NO_RESULTS)).build();
			}
		} catch (Exception e) {
			logger.error("findAll", e);
		}
		return Response.status(500).build();
	}
}
