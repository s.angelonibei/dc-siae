package com.alkemytech.sophia.codman.dto.performing;

import com.google.gson.Gson;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class CodificaDto {

	private Long idCodifica;

	private Long idCombana;

	private String codiceOperaSuggerito;

	private Date dataCodifica;

	private Double distSecCandidata;

	private Double confidenza;

	private Double valoreEconomico;

	private List<CombanaDTO> candidate;

	private String tipoApprovazione;

	private Date dataApprovazione;

	private String statoApprovazione;

	private String codiceOperaApprovato;

	private Date dataFineValidita;

	private Long nUtilizzazioni;

	private Boolean preziosa;

	private String titolo;

	private String autori;

	private String compositori;

	private String interpreti;

	public CodificaDto() {
		super();
	}

	public CodificaDto(Object[] row) {
		if (row.length == 19) {
			idCodifica = (Long) row[0];
			idCombana = (Long) row[1];
			codiceOperaSuggerito = (String) row[2];
			dataCodifica = (Date) row[3];
			distSecCandidata = ((BigDecimal) row[4]).doubleValue();
			confidenza = ((BigDecimal) row[5]).doubleValue();
			valoreEconomico = ((BigDecimal) row[6]).doubleValue();
			candidate = fillCombanaDTO((String) row[7]);
			tipoApprovazione = (String) row[8];
			dataApprovazione = (Date) row[9];
			statoApprovazione = (String) row[10];
			codiceOperaApprovato = (String) row[11];
			dataFineValidita = (Date) row[12];
			nUtilizzazioni = (Long) row[13];
			preziosa = (Boolean) row[14];
			titolo = (String) row[15];
			compositori = (String) row[16];
			autori = (String) row[17];
			interpreti = (String) row[18];
		}
	}

	public Long getIdCodifica() {
		return idCodifica;
	}

	public void setIdCodifica(Long idCodifica) {
		this.idCodifica = idCodifica;
	}

	public Long getIdCombana() {
		return idCombana;
	}

	public void setIdCombana(Long idCombana) {
		this.idCombana = idCombana;
	}

	public String getCodiceOperaSuggerito() {
		return codiceOperaSuggerito;
	}

	public void setCodiceOperaSuggerito(String codiceOperaSuggerito) {
		this.codiceOperaSuggerito = codiceOperaSuggerito;
	}

	public Date getDataCodifica() {
		return dataCodifica;
	}

	public void setDataCodifica(Date dataCodifica) {
		this.dataCodifica = dataCodifica;
	}

	public Double getDistSecCandidata() {
		return distSecCandidata;
	}

	public void setDistSecCandidata(Double distSecCandidata) {
		this.distSecCandidata = distSecCandidata;
	}

	public Double getConfidenza() {
		return confidenza;
	}

	public void setConfidenza(Double confidenza) {
		this.confidenza = confidenza;
	}

	public Double getValoreEconomico() {
		return valoreEconomico;
	}

	public void setValoreEconomico(Double valoreEconomico) {
		this.valoreEconomico = valoreEconomico;
	}

	public List<CombanaDTO> getCandidate() {
		return candidate;
	}

	public void setCandidate(List<CombanaDTO> candidate) {
		this.candidate = candidate;
	}

	public String getTipoApprovazione() {
		return tipoApprovazione;
	}

	public void setTipoApprovazione(String tipoApprovazione) {
		this.tipoApprovazione = tipoApprovazione;
	}

	public Date getDataApprovazione() {
		return dataApprovazione;
	}

	public void setDataApprovazione(Date dataApprovazione) {
		this.dataApprovazione = dataApprovazione;
	}

	public String getStatoApprovazione() {
		return statoApprovazione;
	}

	public void setStatoApprovazione(String statoApprovazione) {
		this.statoApprovazione = statoApprovazione;
	}

	public String getCodiceOperaApprovato() {
		return codiceOperaApprovato;
	}

	public void setCodiceOperaApprovato(String codiceOperaApprovato) {
		this.codiceOperaApprovato = codiceOperaApprovato;
	}

	public Date getDataFineValidita() {
		return dataFineValidita;
	}

	public void setDataFineValidita(Date dataFineValidita) {
		this.dataFineValidita = dataFineValidita;
	}

	public Long getnUtilizzazioni() {
		return nUtilizzazioni;
	}

	public void setnUtilizzazioni(Long nUtilizzazioni) {
		this.nUtilizzazioni = nUtilizzazioni;
	}

	public Boolean getPreziosa() {
		return preziosa;
	}

	public void setPreziosa(Boolean preziosa) {
		this.preziosa = preziosa;
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	public String getAutori() {
		return autori;
	}

	public void setAutori(String autori) {
		this.autori = autori;
	}

	public String getInterpreti() {
		return interpreti;
	}

	public void setInterpreti(String interpreti) {
		this.interpreti = interpreti;
	}

	public String getCompositori() {
		return compositori;
	}

	public void setCompositori(String compositori) {
		this.compositori = compositori;
	}

	private List<CombanaDTO> fillCombanaDTO(String canditate) {
		Gson gson= new Gson();
		List<CombanaDTO> current = new ArrayList<CombanaDTO>();
		try {
			CombanaDTO[] combane =gson.fromJson(canditate, CombanaDTO[].class);
			current= Arrays.asList(combane);
			return current;
		} catch (Exception e) {
			return null;
		}
	}
}
