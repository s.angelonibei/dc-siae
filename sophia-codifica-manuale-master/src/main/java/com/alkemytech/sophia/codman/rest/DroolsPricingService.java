package com.alkemytech.sophia.codman.rest;

import com.alkemytech.sophia.codman.dto.DroolsPricingDTO;
import com.alkemytech.sophia.codman.entity.AnagDsp;
import com.alkemytech.sophia.codman.entity.AnagUtilizationType;
import com.alkemytech.sophia.codman.entity.DroolsPricing;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.alkemytech.sophia.common.s3.S3Service;
import com.amazonaws.services.s3.AmazonS3URI;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;
import org.apache.commons.lang.StringUtils;
import org.jooq.DSLContext;
import org.jooq.Record5;
import org.jooq.SelectQuery;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.Message;
import org.kie.api.builder.Results;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.io.*;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.jooq.impl.DSL.*;

@Singleton
@Path("droolsPricing")
public class DroolsPricingService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final String uploadDirectory;
    private final String s3Bucket;
    private final String s3KeyPrefix;
    private final Provider<EntityManager> provider;
    private final Gson gson;
    private final S3Service s3Service;
    private final MetadataService metadataService;
    private final DSLContext jooq;

    @Inject
    protected DroolsPricingService(@Named("drools.upload.directory") String uploadDirectory,
                                   @Named("drools.s3.bucket") String s3Bucket,
                                   @Named("drools.s3.key_prefix") String s3KeyPrefix,
                                   @McmdbDataSource Provider<EntityManager> provider,
                                   Gson gson, S3Service s3Service,
                                   MetadataService metadataService,
                                   @McmdbDataSource DSLContext jooq
    ) {
        super();
        this.uploadDirectory = uploadDirectory;
        this.s3Bucket = s3Bucket;
        this.s3KeyPrefix = s3KeyPrefix;
        this.provider = provider;
        this.gson = gson;
        this.s3Service = s3Service;
        this.metadataService = metadataService;
        this.jooq = jooq;

    }

    @GET
    @Produces("application/json")
    public Response findAll() {
        final EntityManager entityManager = provider.get();
        final List<DroolsPricing> result = entityManager
                .createQuery("select x from DroolsPricing x", DroolsPricing.class)
                .setMaxResults(32768)
                .getResultList();
        return Response.ok(gson.toJson(result)).build();
    }

    @GET
    @Path("{id}")
    @Produces("application/json")
    public Response findById(@PathParam("id") Long id) {
        final EntityManager entityManager = provider.get();
        final DroolsPricing droolsPricing = entityManager
                .find(DroolsPricing.class, id);
        if (null == droolsPricing) {
            return Response.status(Status.NOT_FOUND).build();
        }
        return Response.ok(gson.toJson(droolsPricing)).build();
    }

    @GET
    @Path("idDsp")
    @Produces("application/json")
    public Response getIdDsp() {
        final EntityManager entityManager = provider.get();
        final List<AnagDsp> anagDsps = entityManager
                .createQuery("select x from AnagDsp x order by x.name", AnagDsp.class)
                .getResultList();
        Map<String, String> result = new LinkedHashMap<String, String>();
        if (null != anagDsps) for (AnagDsp anagDsp : anagDsps) {
            result.put(anagDsp.getIdDsp(), anagDsp.getName());
        }
        return Response.ok(gson.toJson(result)).build();
    }

    @GET
    @Path("idUtilizationType")
    @Produces("application/json")
    public Response getIdUtilizationType() {
        final EntityManager entityManager = provider.get();
        final List<AnagUtilizationType> anagUtilizationTypes = entityManager
                .createQuery("select x from AnagUtilizationType x order by x.name", AnagUtilizationType.class)
                .getResultList();
        Map<String, String> result = new LinkedHashMap<String, String>();
        if (null != anagUtilizationTypes) for (AnagUtilizationType anagUtilizationType : anagUtilizationTypes) {
            result.put(anagUtilizationType.getIdUtilizationType(), anagUtilizationType.getName());
        }
        return Response.ok(gson.toJson(result)).build();
    }

    @GET
    @Path("download/{id}")
    @Produces("application/vnd.ms-excel")
    public Response downloadById(@PathParam("id") Long id) {
        final EntityManager entityManager = provider.get();
        final DroolsPricing droolsPricing = entityManager
                .find(DroolsPricing.class, id);
        if (null == droolsPricing) {
            return Response.status(Status.NOT_FOUND).build();
        }
        final AmazonS3URI s3Uri = new AmazonS3URI(droolsPricing.getXlsS3Url());
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        s3Service.download(s3Uri.getBucket(), s3Uri.getKey(), out);
        return Response.ok()
                .type("application/vnd.ms-excel")
                .entity(out.toByteArray())
                .build();
    }

    @DELETE
    @Path("delete/{id}")
    @Produces("application/json")
    public Response delete(@PathParam("id") Long id) {
        logger.debug("delete: id {}", id);
        final EntityManager entityManager = provider.get();
        entityManager.getTransaction().begin();
        final DroolsPricing droolsPricing = entityManager.find(DroolsPricing.class, id);
        entityManager.remove(droolsPricing);
        entityManager.getTransaction().commit();
        final AmazonS3URI s3Uri = new AmazonS3URI(droolsPricing.getXlsS3Url());
        logger.debug("delete: s3Uri {}", s3Uri);
        s3Service.delete(s3Uri.getBucket(), s3Uri.getKey());
        return Response.ok(gson.toJson(droolsPricing)).build();
    }

    @GET
    @Path("search")
    @Produces("application/json")
    public Response search(@QueryParam("idDsp") String idDsp,
                           @QueryParam("idUtilizationType") String idUtilizationType,
                           @QueryParam("validFrom") String validFrom,
                           @QueryParam("validTo") String validTo,
                           @QueryParam("xlsS3Url") String xlsS3Url,
                           @DefaultValue("0") @QueryParam("first") int first,
                           @DefaultValue("50") @QueryParam("last") int last) {
        final EntityManager entityManager = provider.get();
        try {
            String separator = " where ";
            final StringBuffer sql = new StringBuffer()
                    .append("select * from DROOLS_PRICING");
            if (!StringUtils.isEmpty(idDsp)) {
                sql.append(separator).append("IDDSP = ?");
                separator = " and ";
            }
            if (!StringUtils.isEmpty(idUtilizationType)) {
                sql.append(separator).append("ID_UTILIZATION_TYPE = ?");
                separator = " and ";
            }
            if (!StringUtils.isEmpty(validFrom)) {
                sql.append(separator).append("VALID_FROM = ?");
                separator = " and ";
            }
            if (!StringUtils.isEmpty(validTo)) {
                sql.append(separator).append("VALID_TO = ?");
                separator = " and ";
            }
            if (!StringUtils.isEmpty(xlsS3Url)) {
                sql.append(separator).append("XLS_S3_URL like ?");
                separator = " and ";
            }
            sql.append(" limit ?, ?");
            int i = 1;
            final Query query = (Query) entityManager
                    .createNativeQuery(sql.toString(), DroolsPricing.class);
            if (!StringUtils.isEmpty(idDsp)) {
                query.setParameter(i++, idDsp);
            }
            if (!StringUtils.isEmpty(idUtilizationType)) {
                query.setParameter(i++, idUtilizationType);
            }
            if (!StringUtils.isEmpty(validFrom)) {
                query.setParameter(i++, new SimpleDateFormat("yyyy-MM-dd").parse(validFrom));
            }
            if (!StringUtils.isEmpty(validTo)) {
                query.setParameter(i++, new SimpleDateFormat("yyyy-MM-dd").parse(validTo));
            }
            if (!StringUtils.isEmpty(xlsS3Url)) {
                query.setParameter(i++, "%" + xlsS3Url + "%");
            }
            query.setParameter(i++, first);
            query.setParameter(i++, 1 + last - first);
            @SuppressWarnings("unchecked") final List<DroolsPricing> results =
                    (List<DroolsPricing>) query.getResultList();
            final PagedResult result = new PagedResult();
            if (null != results && !results.isEmpty()) {
                final int maxrows = last - first;
                final boolean hasNext = results.size() > maxrows;
                result.setRows(!hasNext ? results : results.subList(0, maxrows))
                        .setMaxrows(maxrows)
                        .setFirst(first)
                        .setLast(hasNext ? last : first + results.size())
                        .setHasNext(hasNext)
                        .setHasPrev(first > 0);
            } else {
                result.setMaxrows(0)
                        .setFirst(0)
                        .setLast(0)
                        .setHasNext(false)
                        .setHasPrev(false);
            }
            return Response.ok(gson.toJson(result)).build();
        } catch (Exception e) {
            logger.error("search", e);
        }
        return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR).build();
    }

    @POST
    @Path("upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces("application/json")
    public Response upload(@FormDataParam("idDsp") String idDsp,
                           @FormDataParam("idUtilizationType") String idUtilizationType,
                           @FormDataParam("validFrom") String validFrom,
                           @FormDataParam("validTo") String validTo,
                           @FormDataParam("file") InputStream uploadedInputStream,
                           @FormDataParam("file") FormDataContentDisposition fileDetail) {
        final File file = new File(uploadDirectory, UUID.randomUUID().toString());
        logger.debug("upload: file {}", file);
        try {
            Date validToDate = null;
            final Date validFromDate = new SimpleDateFormat("yyyy-MM-dd").parse(validFrom);
            if (!StringUtils.isEmpty(validTo)) {
                validToDate = new SimpleDateFormat("yyyy-MM-dd").parse(validTo);
            }
            logger.debug("upload: validFromDate {}", validFromDate);
            logger.debug("upload: validToDate {}", validToDate);
            if (validToDate != null && validToDate.compareTo(validFromDate) <= 0) {
                return Response.status(Status.BAD_REQUEST).entity(gson.toJson(Arrays.asList("Date non valide."))).build();
            }
            if (saveToFile(uploadedInputStream, file)) { // save to temporary local file
                final List<String> errors = getDecisionTableErrors(file);

                final List<DroolsPricingDTO> errorsdate = validationDate(null, idDsp, idUtilizationType, validFromDate, validToDate);

				if (!errorsdate.isEmpty() && validToDate == null){
					return Response.status(Status.BAD_REQUEST).entity(gson.toJson(Arrays.asList("Le seguenti date non sono valide: " + validFrom))).build();
				}else if(!errorsdate.isEmpty()){
				    return Response.status(Status.BAD_REQUEST).entity(gson.toJson(Arrays.asList("Le seguenti date non sono valide: " + validFrom + " - " +validTo))).build();
                }

                final List<String> errorsDrools = getDecisionTableErrors(file); // validate file with drools

                logger.debug("upload: errors {}", errors);
                if (null == errorsDrools || errorsDrools.isEmpty()) {
                    final String xlsS3Url = "s3://" + s3Bucket + "/" + s3KeyPrefix +
                            "/" + idDsp.replace('(', '_').replace(')', '_').replace(' ', '_') +
                            "/" + idUtilizationType +
                            "/" + new SimpleDateFormat("yyyyMMdd").format(validFromDate) +
//							"/" + UUID.randomUUID() + // avoid overwrites
                            "/" + fileDetail.getFileName();
                    logger.debug("upload: xlsS3Url {}", xlsS3Url);
                    final AmazonS3URI s3Uri = new AmazonS3URI(xlsS3Url);
                    logger.debug("upload: s3Uri {}", s3Uri);
                    if (s3Service.upload(s3Uri.getBucket(), s3Uri.getKey(), file)) { // upload to s3
                        // save to database
                        final DroolsPricing droolsPricing = new DroolsPricing();
                        droolsPricing.setIdDsp(idDsp);
                        droolsPricing.setIdUtilizationType(idUtilizationType);
                        droolsPricing.setValidFrom(validFromDate);
                        droolsPricing.setValidTo(validToDate);
                        droolsPricing.setXlsS3Url(xlsS3Url);
                        final EntityManager entityManager = provider.get();
                        entityManager.getTransaction().begin();
                        entityManager.persist(droolsPricing);
                        entityManager.getTransaction().commit();
                        return Response.ok(gson.toJson(droolsPricing)).build();
                    }
                } else {
                    return Response.status(Status.BAD_REQUEST)
                            .entity(gson.toJson(errors)).build();
                }
            }
        } catch (Exception e) {
            logger.error("upload", e);
        } finally {
            file.delete();
        }
        return Response.status(Status.INTERNAL_SERVER_ERROR).build();
    }

    @PUT
    @Path("{id}")
    public Response update(@PathParam("id") Long id, @QueryParam("validFrom") String validFrom, @QueryParam("validTo") String validTo) throws ParseException {
        final EntityManager entityManager = provider.get();
        DroolsPricing droolsPricing = entityManager.find(DroolsPricing.class, id);

            final Date validFromDate = new SimpleDateFormat("yyyy-MM-dd").parse(validFrom);
            Date validToDate = null;
            if(StringUtils.isNotEmpty(validTo))
                validToDate = new SimpleDateFormat("yyyy-MM-dd").parse(validTo);

       List<DroolsPricingDTO> invalidationDate = validationDate(id, droolsPricing.getIdDsp(), droolsPricing.getIdUtilizationType(), validFromDate, validToDate);

        if (!invalidationDate.isEmpty() && validToDate == null) {
            return Response.status(Status.BAD_REQUEST).entity(gson.toJson(Arrays.asList("Le seguenti date non sono valide: " + validFrom))).build();
        } else if (!invalidationDate.isEmpty()) {
            return Response.status(Status.BAD_REQUEST).entity(gson.toJson(Arrays.asList("Le seguenti date non sono valide: " + validFrom + " - " + validTo))).build();
        }

        entityManager.getTransaction().begin();
        droolsPricing.setValidFrom(validFromDate);
        droolsPricing.setValidTo(validToDate);
        entityManager.getTransaction().commit();

        return Response.ok().build();

    }

    @PUT
    @Path("updateDate/{id}")
    public Response updateDate(@PathParam("id") Long id, @QueryParam("validFrom") String validFrom, @QueryParam("validTo") String validTo) throws ParseException {
        final EntityManager entityManager = provider.get();
        DroolsPricing droolsPricing = entityManager.find(DroolsPricing.class, id);
        logger.debug("----->", validTo);
        Date validFromDate = StringUtils.isNotEmpty(validFrom) ? 
                new SimpleDateFormat("yyyy-MM-dd").parse(validFrom) :
                null;
        Date validToDate = StringUtils.isNotEmpty(validTo) ? 
                new SimpleDateFormat("yyyy-MM-dd").parse(validTo) :
                null;

        entityManager.getTransaction().begin();
        if(validFromDate != null) {
            droolsPricing.setValidFrom(validFromDate);
        }
        if(validToDate != null) {
            droolsPricing.setValidTo(validToDate);
        }
        entityManager.getTransaction().commit();

        return Response.ok().build();
    }

    private List<DroolsPricingDTO> validationDate(Long id, String idDsp, String idUtilizationType, Date validFromDate, Date validToDate) {

        DSLContext dslContext = jooq;

        //è colpa dell'antenato di Galiffa 10000000010000l = year: 2286 Millis to Date
        final long timeAgo = 10000000010000l;

        if (validToDate == null)
            validToDate = new Date(10000000010000l);

        SelectQuery<Record5<String, String, String, Object, Object>> selectData = dslContext.select(
                field("ID", String.class),
                field("IDDSP", String.class).as("idDsp"),
                field("ID_UTILIZATION_TYPE", String.class).as("idUtilizationType"),
                field("VALID_FROM").as("validFrom"),
                field("VALID_TO").as("validTo"))
                .from(table("DROOLS_PRICING"))
                .where(field("IDDSP").eq(idDsp))
                .and(field("ID_UTILIZATION_TYPE").eq(idUtilizationType))
                .and(field("VALID_FROM").le(new Timestamp(validToDate.getTime())))
                .and(coalesce(field("VALID_TO"), new Timestamp(timeAgo)).ge(new Timestamp(validFromDate.getTime()))).getQuery();

        if(id!=null)
            selectData.addConditions(field("ID", Long.class).ne(id));

        List<DroolsPricingDTO> droolsPricingDTOS = selectData.fetchInto(DroolsPricingDTO.class);
        return droolsPricingDTOS;

    }

    private boolean saveToFile(InputStream in, File file) {
        OutputStream out = null;
        try {
            int count = 0;
            final byte[] bytes = new byte[1024];
            out = new FileOutputStream(file);
            while (-1 != (count = in.read(bytes))) {
                out.write(bytes, 0, count);
            }
            out.close();
            out = null;
            return true;
        } catch (IOException e) {
            logger.error("saveToFile", e);
        } finally {
            if (null != out) {
                try {
                    out.close();
                } catch (IOException e) {
                    logger.error("saveToFile", e);
                }
            }
        }
        return false;
    }

    private List<String> getDecisionTableErrors(File file) throws FileNotFoundException {
        InputStream in = null;
        try {
            in = new FileInputStream(file);
            final KieServices kieServices = KieServices.Factory.get();
            final KieFileSystem kieFileSystem = kieServices.newKieFileSystem();
            kieFileSystem.write("src/main/resources/dtable.xls",
                    kieServices.getResources().newInputStreamResource(in));
            final KieBuilder kieBuilder = kieServices.newKieBuilder(kieFileSystem).buildAll();
            final Results results = kieBuilder.getResults();
            if (!results.hasMessages(Message.Level.ERROR)) {
                return null;
            } else {
                final List<Message> messages = results.getMessages();
                if (null != messages && !messages.isEmpty()) {
                    final List<String> errors = new ArrayList<String>(messages.size());
                    for (Message message : messages) {
                        errors.add(message.getText() + " (line " + message.getLine() + ", column " + message.getColumn() + ")");
                    }
                    return errors;
                }
            }
        } catch (Exception e) {
            logger.error("getDecisionTableErrors", e);
            final StringWriter out = new StringWriter();
            e.printStackTrace(new PrintWriter(out));
            return Arrays.asList("Bad decision table XLS file", e.getMessage(), out.toString());
        } finally {
            if (null != in) {
                try {
                    in.close();
                } catch (IOException e) {
                    logger.error("getDecisionTableErrors", e);
                }
            }
        }
        return Arrays.asList("Bad decision table XLS file");
    }

    @GET
    @Path("xls4dsr/{idDsr}")
    @Produces("application/json")
    public Response getXlsS3url(@PathParam("idDsr") String idDsr) {
        return metadataService.getByIdDsr(idDsr);
    }

//	@GET
//	@Path("xls4dsr/{idDsr}")
//	@Produces("application/json")
//	public Response getXlsS3url(@PathParam("idDsr") String idDsr) {
//		logger.debug("getXlsS3url: idDsr {}", idDsr);
//		final EntityManager entityManager = provider.get();
//		// DSR_METADATA
//		final DsrMetadata dsrMetadata = entityManager
//				.find(DsrMetadata.class, idDsr);
//		logger.debug("getXlsS3url: dsrMetadata {}", dsrMetadata);
//		if (null == dsrMetadata) {
//			return Response.status(Status.NOT_FOUND).build();
//		}
//		// COMMERCIAL_OFFERS
//		final CommercialOffers commercialOffers = entityManager
//				.find(CommercialOffers.class, dsrMetadata.getServiceCode());
//		logger.debug("getXlsS3url: commercialOffers {}", commercialOffers);
//		if (null == commercialOffers) {
//			return Response.status(Status.NOT_FOUND).build();
//		}
//		// DROOLS_PRICING
//		final String year = String.format("%04d", dsrMetadata.getYear());
//		final String month = "month".equalsIgnoreCase(dsrMetadata.getPeriodType()) ?
//				String.format("%02d", dsrMetadata.getPeriod()) :
//					String.format("%02d", dsrMetadata.getPeriod() * 3 - 2);
//		String sql = new StringBuffer()
//			.append("select A.XLS_S3_URL XLS_S3_URL")
//			.append(" from DROOLS_PRICING A, COMMERCIAL_OFFERS B")
//			.append(" where B.ID_COMMERCIAL_OFFERS = ").append(dsrMetadata.getServiceCode())
//			.append("   and A.IDDSP = B.IDDSP")
//			.append("   and A.ID_UTILIZATION_TYPE = B.ID_UTIILIZATION_TYPE")
//			.append("   and A.VALID_FROM <= STR_TO_DATE('").append(year + "-" + month).append("', '%Y-%m')")
//			.append("   order by A.VALID_FROM desc")
//			.append("   limit 1").toString();
//		@SuppressWarnings("unchecked")
//		final List<String> xlsS3Urls = entityManager.createNativeQuery(sql).getResultList();
//		logger.debug("getXlsS3url: xlsS3Urls {}", xlsS3Urls);
//		if (null == xlsS3Urls || xlsS3Urls.isEmpty()) {
//			return Response.status(Status.NOT_FOUND)
////					.entity(gson.toJson(dsrMetadata))
//					.build();
//		}
//		final Map<String, String> result = new HashMap<String, String>();
//		result.put("idDsr", idDsr);
//		result.put("idCommercialOffering", "" + commercialOffers.getIdCommercialOffering());
//		result.put("idUtilizationType", "" + commercialOffers.getIdUtilizationType());
//		result.put("offering", "" + commercialOffers.getOffering());
//		result.put("serviceCode", "" + dsrMetadata.getServiceCode());
//		if (!Strings.isNullOrEmpty(dsrMetadata.getCurrency())) {
//			result.put("currency", "" + dsrMetadata.getCurrency());
//		}
//		if (!Strings.isNullOrEmpty(dsrMetadata.getCountry())) {
//			result.put("country", "" + dsrMetadata.getCountry());
//		}
//		result.put("periodType", "" + dsrMetadata.getPeriodType());
//		result.put("period", "" + dsrMetadata.getPeriod());
//		result.put("salesLinesNum", "" + dsrMetadata.getSalesLinesNum());
//		result.put("totalValue", "" + dsrMetadata.getTotalValue());
//		result.put("subscriptionsNum", "" + dsrMetadata.getSubscriptionsNum());
//		result.put("year", year);
//		result.put("month", month);
//		result.put("xlsS3Url", xlsS3Urls.get(0));
//		return Response.ok(gson.toJson(result)).build();
//	}

//	public static void main(String[] args) throws Exception {
//		logger.debug("" + DroolsPricingService
//				.getDecisionTableErrors(new File("/Users/roberto/Development/SIAE-SOPHIA/eclipse_workspace/sophia-pricing/res/GooglePlaySubscriptions.xls")));
//	}

}
