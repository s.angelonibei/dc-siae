package com.alkemytech.sophia.codman.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.GsonBuilder;

@XmlRootElement
@Entity(name="CurrencyRate")
@Table(name="currency_rate")
@IdClass(CurrencyRateId.class)
public class CurrencyRate {

	@Id
	@Column(name="year", nullable=false)
	private String year;

	@Id
	@Column(name="month", nullable=false)
	private String month;

	@Id
	@Column(name="src_currency", nullable=false)
	private String srcCurrency;
	
	@Id
	@Column(name="dst_currency", nullable=false)
	private String dstCurrency;

	@Column(name="rate", nullable=false, precision=20, scale=10)
	private BigDecimal rate;

	
	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getSrcCurrency() {
		return srcCurrency;
	}

	public void setSrcCurrency(String srcCurrency) {
		this.srcCurrency = srcCurrency;
	}

	public String getDstCurrency() {
		return dstCurrency;
	}

	public void setDstCurrency(String dstCurrency) {
		this.dstCurrency = dstCurrency;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}
}
