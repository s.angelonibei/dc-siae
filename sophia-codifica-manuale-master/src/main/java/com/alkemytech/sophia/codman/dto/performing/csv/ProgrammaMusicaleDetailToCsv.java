package com.alkemytech.sophia.codman.dto.performing.csv;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

public class ProgrammaMusicaleDetailToCsv {

    @CsvBindByPosition(position = 0)
    @CsvBindByName
	private Long numeroPm;
    @CsvBindByPosition(position = 1)
    @CsvBindByName
	private String voceIncasso;
    @CsvBindByPosition(position = 2)
    @CsvBindByName
	private String tipoSupporto;
    @CsvBindByPosition(position = 3)
    @CsvBindByName
	private String tipoDocumento;
    @CsvBindByPosition(position = 4)
    @CsvBindByName
	private Character flagDaCampionare;
    @CsvBindByPosition(position = 5)
    @CsvBindByName
	private Long codCampionamento;
    @CsvBindByPosition(position = 6)
    @CsvBindByName
	private Long risultatoCalcoloResto;
    @CsvBindByPosition(position = 7)
    @CsvBindByName
	private Double totaleDurataCedole;
    @CsvBindByPosition(position = 8)
    @CsvBindByName
	private Long totaleCedole;
    @CsvBindByPosition(position = 9)
    @CsvBindByName
	private String foglioSegueDi;
    @CsvBindByPosition(position = 10)
    @CsvBindByName
	private String durataRientro;
    @CsvBindByPosition(position = 11)
    @CsvBindByName
	private String direttoreEsecuzione;
    @CsvBindByPosition(position = 12)
    @CsvBindByName
	private String complesso;
    @CsvBindByPosition(position = 13)
    @CsvBindByName
	private Character flagGruppo;
	

	public ProgrammaMusicaleDetailToCsv() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public ProgrammaMusicaleDetailToCsv(Long numeroPm, String voceIncasso, String tipoSupporto, String tipoDocumento,
			Character flagDaCampionare, Long codCampionamento, Long risultatoCalcoloResto, Double totaleDurataCedole,
			Long totaleCedole, String foglioSegueDi, String durataRientro, String direttoreEsecuzione,
			String complesso, Character flagGruppo) {
		super();
		this.numeroPm = numeroPm;
		this.voceIncasso = voceIncasso;
		this.tipoSupporto = tipoSupporto;
		this.tipoDocumento = tipoDocumento;
		this.flagDaCampionare = flagDaCampionare;
		this.codCampionamento = codCampionamento;
		this.risultatoCalcoloResto = risultatoCalcoloResto;
		this.totaleDurataCedole = totaleDurataCedole;
		this.totaleCedole = totaleCedole;
		this.foglioSegueDi = foglioSegueDi;
		this.durataRientro = durataRientro;
		this.direttoreEsecuzione = direttoreEsecuzione;
		this.complesso = complesso;
		this.flagGruppo = flagGruppo;
	}
	

	public Long getNumeroPm() {
		return numeroPm;
	}



	public void setNumeroPm(Long numeroPm) {
		this.numeroPm = numeroPm;
	}



	public String getVoceIncasso() {
		return voceIncasso;
	}



	public void setVoceIncasso(String voceIncasso) {
		this.voceIncasso = voceIncasso;
	}



	public String getTipoSupporto() {
		return tipoSupporto;
	}



	public void setTipoSupporto(String tipoSupporto) {
		this.tipoSupporto = tipoSupporto;
	}



	public String getTipoDocumento() {
		return tipoDocumento;
	}



	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}



	public Character getFlagDaCampionare() {
		return flagDaCampionare;
	}



	public void setFlagDaCampionare(Character flagDaCampionare) {
		this.flagDaCampionare = flagDaCampionare;
	}



	public Long getCodCampionamento() {
		return codCampionamento;
	}



	public void setCodCampionamento(Long codCampionamento) {
		this.codCampionamento = codCampionamento;
	}



	public Long getRisultatoCalcoloResto() {
		return risultatoCalcoloResto;
	}



	public void setRisultatoCalcoloResto(Long risultatoCalcoloResto) {
		this.risultatoCalcoloResto = risultatoCalcoloResto;
	}



	public Double getTotaleDurataCedole() {
		return totaleDurataCedole;
	}



	public void setTotaleDurataCedole(Double totaleDurataCedole) {
		this.totaleDurataCedole = totaleDurataCedole;
	}



	public Long getTotaleCedole() {
		return totaleCedole;
	}



	public void setTotaleCedole(Long totaleCedole) {
		this.totaleCedole = totaleCedole;
	}



	public String getFoglioSegueDi() {
		return foglioSegueDi;
	}



	public void setFoglioSegueDi(String foglioSegueDi) {
		this.foglioSegueDi = foglioSegueDi;
	}



	public String getDurataRientro() {
		return durataRientro;
	}



	public void setDurataRientro(String durataRientro) {
		this.durataRientro = durataRientro;
	}



	public String getDirettoreEsecuzione() {
		return direttoreEsecuzione;
	}



	public void setDirettoreEsecuzione(String direttoreEsecuzione) {
		this.direttoreEsecuzione = direttoreEsecuzione;
	}



	public String getComplesso() {
		return complesso;
	}



	public void setComplesso(String complesso) {
		this.complesso = complesso;
	}



	public Character getFlagGruppo() {
		return flagGruppo;
	}



	public void setFlagGruppo(Character flagGruppo) {
		this.flagGruppo = flagGruppo;
	}



	public String[] getMappingStrategy() {
        return new String[]{
        		"Numero programma musicale",
        		"Voce incasso",
        		"Tipo supporto",
    			"Tipo documento",
        		"Flag da campionare",
        		"Codice campionamento",
        		"Risultato calcolo resto",
        		"Totale durata cedole",
        		"Totale cedole",
        		"Foglio segue di",
        		"Data Rientro",
        		"Direttore Esecuzione",
        		"Complesso",
        		"Flag Gruppo Principale/Gruppo Spalla"
        };
	}
}
