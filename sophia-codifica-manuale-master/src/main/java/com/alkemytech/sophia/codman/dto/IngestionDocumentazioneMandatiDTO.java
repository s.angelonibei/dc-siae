package com.alkemytech.sophia.codman.dto;

import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Produces("application/json")
@XmlRootElement
public class IngestionDocumentazioneMandatiDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
    private Date dataInserimento;
    private String stato;
    private String societa;
    private String esito;
    private String fileLocation;
    private String fileName;
    private Integer totaleOpere;
    private Integer totaleOpereAcquisite;
    private String message;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDataInserimento() {
        return dataInserimento;
    }

    public void setDataInserimento(Date dataInserimento) {
        this.dataInserimento = dataInserimento;
    }

    public String getStato() {
        return stato;
    }

    public void setStato(String stato) {
        this.stato = stato;
    }

    public String getSocieta() {
        return societa;
    }

    public void setSocieta(String societa) {
        this.societa = societa;
    }

    public String getEsito() {
        return esito;
    }

    public void setEsito(String esito) {
        this.esito = esito;
    }

    public String getFileLocation() {
        return fileLocation;
    }

    public void setFileLocation(String fileLocation) {
        this.fileLocation = fileLocation;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Integer getTotaleOpere() {
        return totaleOpere;
    }

    public void setTotaleOpere(Integer totaleOpere) {
        this.totaleOpere = totaleOpere;
    }

    public Integer getTotaleOpereAcquisite() {
        return totaleOpereAcquisite;
    }

    public void setTotaleOpereAcquisite(Integer totaleOpereAcquisite) {
        this.totaleOpereAcquisite = totaleOpereAcquisite;
    }

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
