package com.alkemytech.sophia.codman.entity.performing.view;

/**
 * Created by idilello on 11/25/16.
 */
public class BreadCrumb {

    private String label;
    private String url;

    public BreadCrumb(String label, String url){
        this.label = label;
        this.url = url;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
