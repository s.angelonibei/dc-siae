package com.alkemytech.sophia.codman.entity.performing.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.alkemytech.sophia.codman.entity.Configuration;
import com.alkemytech.sophia.codman.entity.Setting;
import com.alkemytech.sophia.codman.entity.performing.Circoscrizione;
import com.alkemytech.sophia.codman.entity.performing.Configurazione;
import com.alkemytech.sophia.codman.rest.performing.Constants;
import com.google.inject.Inject;
import com.google.inject.Provider;

/**
 * Created by idilello on 6/9/16.
 */

public class ConfigurationDAO implements IConfigurationDAO {

    private Provider<EntityManager> provider;

    @Inject
    public ConfigurationDAO(Provider<EntityManager> provider) {
        this.provider = provider;
    }


    public List<Circoscrizione> getCircoscrizioni() {
        EntityManager entityManager = provider.get();
        if (Constants.CONFIGURATIONS.get("circoscrizioni") != null) {

            return (List<Circoscrizione>) Constants.CONFIGURATIONS.get("circoscrizioni");

        } else {

            String query = "select c from Circoscrizione c where c.id<>'9999999' order by c.denominazione asc";

            List<Circoscrizione> results = (List<Circoscrizione>) entityManager.createQuery(query)
                    .getResultList();

            return results;
        }

    }


    public String getParameter(String parameterName) {

        EntityManager entityManager = provider.get();
        if (Constants.CONFIGURATIONS.get(parameterName) != null) {

            Configurazione parametro = (Configurazione) Constants.CONFIGURATIONS.get(parameterName);

            return parametro.getParameterValue();

        } else {

            String query = "select c from Configurazione c where c.parameterName=:parameterName and c.flagAttivo='Y'";

            List<Configurazione> results = (List<Configurazione>) entityManager.createQuery(query)
                    .setParameter("parameterName", parameterName)
                    .getResultList();

            if (results != null && results.size() > 0)
                return results.get(0).getParameterValue();
            else
                return null;
        }

    }


    public void insertParameter(String parameterName, String parameterValue) {

        EntityManager entityManager = provider.get();
        Configurazione configurazione;


        try {
            String query = "select c from Configurazione c where c.parameterName=:parameterName and c.flagAttivo='Y'";

            List<Configurazione> results = (List<Configurazione>) entityManager.createQuery(query)
                    .setParameter("parameterName", parameterName)
                    .getResultList();

            if (results != null && results.size() > 0) {

                entityManager.getTransaction().begin();
                configurazione = results.get(0);

                configurazione.setParameterValue(parameterValue);
                configurazione.setDataOraUltimaModifica(new Date());

                entityManager.persist(configurazione);
                entityManager.flush();
                entityManager.getTransaction().commit();

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void insertParameterObject(Configurazione configurazione) {

        EntityManager entityManager = provider.get();
        try {

            Configurazione conf = getParameterObject(configurazione.getParameterName());

            if (conf != null) {
                entityManager.merge(configurazione);
            } else {
                entityManager.persist(configurazione);
            }

            entityManager.flush();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Configurazione getParameterObject(String parameterName) {

        EntityManager entityManager = provider.get();
        if (Constants.CONFIGURATIONS.get(parameterName) != null) {

            Configurazione parametro = (Configurazione) Constants.CONFIGURATIONS.get(parameterName);

            return parametro;

        } else {

            String query = "select c from Configurazione c where c.parameterName=:parameterName and c.flagAttivo='Y'";

            List<Configurazione> results = (List<Configurazione>) entityManager.createQuery(query)
                    .setParameter("parameterName", parameterName)
                    .getResultList();

            if (results != null && results.size() > 0)
                return results.get(0);
            else
                return null;
        }

    }

    public String getLogicalServerAddress(String phisycalAddress) {

        String logicalAddress = null;
        String mapLogicalPhisycal = null;

        if (Constants.CONFIGURATIONS.get("CLUSTER") != null) {

            Configurazione configurazione = (Configurazione) Constants.CONFIGURATIONS.get("CLUSTER");

            mapLogicalPhisycal = configurazione.getParameterValue();

        } else {

            mapLogicalPhisycal = getParameter("CLUSTER");

        }

        // AR147-127.0.1.1:7001,localhost-127.0.0.1:7001
        if (mapLogicalPhisycal != null) {

            String[] addresses = mapLogicalPhisycal.split(",");
            String address = null;

            for (int i = 0; i < addresses.length; i++) {
                address = addresses[i].trim();
                if (address.startsWith(phisycalAddress)) {
                    String[] splittedAddress = address.split("!");
                    logicalAddress = splittedAddress[1];
                    break;
                }
            }
        }

        return logicalAddress;
    }


    @Override
    public List<Configuration> getConfiguration(String domain, String key) {
        EntityManager em = provider.get();
        TypedQuery<Configuration> query = em.createNamedQuery("Configuration.getConfigurations", Configuration.class);
        query.setParameter("domain", domain);
        query.setParameter("key", key);
        List<Configuration> results = query.getResultList();
        return results;
    }

    @Override
    public Configuration getConfiguration(long id) {
        EntityManager em = provider.get();
        TypedQuery<Configuration> query = em.createNamedQuery("Configuration.findConfiguration", Configuration.class);
        query.setParameter("id", id);
        Configuration result = query.getSingleResult();
        return result;
    }

    @Override
    public List<Configuration> getConfigurationHistory(String domain, String key) {
        EntityManager em = provider.get();
        TypedQuery<Configuration> query = em.createNamedQuery("Configuration.historyConfigurations", Configuration.class);
        query.setParameter("domain", domain);
        query.setParameter("key", key);
        List<Configuration> results = query.getResultList();
        return results;
    }

    @Override
    public Configuration saveConfiguration(Configuration configuration) {
        if (configuration.getId() != null) {
            Configuration current = getConfiguration(configuration.getId());
            deactivate(current);
            current.setEditedBy(configuration.getCreatedBy());
            adjustForSaving(configuration);
            EntityManager em = provider.get();
            em.getTransaction().begin();
            em.persist(current);
            em.persist(configuration);
            em.flush();
            em.getTransaction().commit();
            return configuration;
        } else {
            EntityManager em = provider.get();
            em.getTransaction().begin();
            configuration.setCreated(new Date());
            if (configuration.getValidFrom() == null) {
            	configuration.setValidFrom(new Date());
            }            
            configuration.setActive(true);   
            if (configuration.getRelatedConfiguration() != null) {
            	em.merge(configuration.getRelatedConfiguration());
            }
            em.persist(configuration);
            em.flush();
            em.getTransaction().commit();
            return configuration;
        }
    }

    protected void deactivate(Configuration configuration) {
        configuration.setActive(false);
        configuration.setValidTo(new Date());
        configuration.setModified(new Date());
        List<Configuration> configurations = configuration.getConfigurations();
        if (configurations.isEmpty()) {
            return;
        }
        for (Configuration inner : configurations) {
            deactivate(inner);
        }
    }
    
    protected void adjustForSaving(Configuration configuration) {
        configuration.setId(null);
        configuration.setValidFrom(new Date());
        configuration.setActive(true);
        List<Setting> settings = configuration.getSettings();
        for (Setting setting : settings) {
            setting.setId(null);
        }
        List<Configuration> configurations = configuration.getConfigurations();
        if (configurations.isEmpty()) {
            return;
        }
        for (Configuration inner : configurations) {
            adjustForSaving(inner);
        }
    }


	@Override
	public void deleteConfiguration(Configuration configuration) {
		EntityManager em = provider.get();
		em.getTransaction().begin();
		try {
			Configuration toDelete = em.merge(configuration);
			em.remove(toDelete);
			em.getTransaction().commit();
		} catch (Exception e) {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			throw e;
		}

	}

}

