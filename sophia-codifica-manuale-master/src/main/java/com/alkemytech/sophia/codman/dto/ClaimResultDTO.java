package com.alkemytech.sophia.codman.dto;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@SuppressWarnings("serial")
@Data
public class ClaimResultDTO implements Serializable {

    private String dsr;
    private String codiceUUID;
    private String salesTransactionId;
    private Float percDemRes;
    private Float percDrmRes;
    private Float percDemConf;
    private Float percDrmConf;
    private String esito;
}
