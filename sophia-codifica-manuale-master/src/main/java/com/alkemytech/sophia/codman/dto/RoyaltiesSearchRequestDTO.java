package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.codman.entity.AnagCountry;
import com.alkemytech.sophia.codman.entity.AnagDsp;

@XmlRootElement
public class RoyaltiesSearchRequestDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private AnagDsp anagDsp;
	private AnagCountry country;
	private String uuid;
	private String workCode;
	private String society;

	public AnagCountry getCountry() {
		return country;
	}

	public void setCountry(AnagCountry country) {
		this.country = country;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getWorkCode() {
		return workCode;
	}

	public void setWorkCode(String workCode) {
		this.workCode = workCode;
	}

	public String getSociety() {
		return society;
	}

	public void setSociety(String society) {
		this.society = society;
	}

	public AnagDsp getAnagDsp() {
		return anagDsp;
	}

	public void setAnagDsp(AnagDsp anagDsp) {
		this.anagDsp = anagDsp;
	}
}
