package com.alkemytech.sophia.codman.dto.performing;

import com.alkemytech.sophia.codman.entity.performing.PerfMonitoraggioCodificato;

import java.io.Serializable;

public class MonitoraggioCodifcatoTuple implements Serializable {
    private PerfMonitoraggioCodificato monitoraggioCodificatoInferiore;
    private PerfMonitoraggioCodificato monitoraggioCodificatoSuperiore;

    public MonitoraggioCodifcatoTuple(PerfMonitoraggioCodificato monitoraggioCodificatoInferiore, PerfMonitoraggioCodificato monitoraggioCodificatoSuperiore) {
        this.monitoraggioCodificatoInferiore = monitoraggioCodificatoInferiore;
        this.monitoraggioCodificatoSuperiore = monitoraggioCodificatoSuperiore;
    }

    public PerfMonitoraggioCodificato getMonitoraggioCodificatoInferiore() {
        return monitoraggioCodificatoInferiore;
    }

    public void setMonitoraggioCodificatoInferiore(PerfMonitoraggioCodificato monitoraggioCodificatoInferiore) {
        this.monitoraggioCodificatoInferiore = monitoraggioCodificatoInferiore;
    }

    public PerfMonitoraggioCodificato getMonitoraggioCodificatoSuperiore() {
        return monitoraggioCodificatoSuperiore;
    }

    public void setMonitoraggioCodificatoSuperiore(PerfMonitoraggioCodificato monitoraggioCodificatoSuperiore) {
        this.monitoraggioCodificatoSuperiore = monitoraggioCodificatoSuperiore;
    }
}
