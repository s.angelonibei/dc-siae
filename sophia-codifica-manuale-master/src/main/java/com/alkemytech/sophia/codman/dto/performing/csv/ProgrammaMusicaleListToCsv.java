package com.alkemytech.sophia.codman.dto.performing.csv;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;



public class ProgrammaMusicaleListToCsv {

    @CsvBindByPosition(position = 0)
    @CsvBindByName
	private Integer contabilita;
    @CsvBindByPosition(position = 1)
    @CsvBindByName
	private String voceIncasso;
    @CsvBindByPosition(position = 2)
    @CsvBindByName
    private Long idEvento;
    @CsvBindByPosition(position = 3)
    @CsvBindByName
    private String numeroPermesso;
    @CsvBindByPosition(position = 4)
    @CsvBindByName
    private Long numeroFattura;
    @CsvBindByPosition(position = 5)
    @CsvBindByName
	private Long reversale;
    @CsvBindByPosition(position = 6)
    @CsvBindByName
	private String tipoDocumento;
    @CsvBindByPosition(position = 7)
    @CsvBindByName
	private Long numeroPm;
    @CsvBindByPosition(position = 8)
    @CsvBindByName
	private String importOriginario;
    @CsvBindByPosition(position = 9)
    @CsvBindByName
	private String importoRicalcolato;

	public ProgrammaMusicaleListToCsv(Integer contabilita, String voceIncasso, Long idEvento, String numeroPermesso, Long numeroFattura,
			Long reversale, String tipoDocumento, Long numeroPm, String importOriginario,
			String importoRicalcolato) {
		super();
		this.contabilita = contabilita;
		this.voceIncasso = voceIncasso;
		this.idEvento = idEvento;
		this.numeroPermesso = numeroPermesso;
		this.numeroFattura = numeroFattura;
		this.reversale = reversale;
		this.tipoDocumento = tipoDocumento;
		this.numeroPm = numeroPm;
		this.importOriginario = importOriginario;
		this.importoRicalcolato = importoRicalcolato;
	}

	public ProgrammaMusicaleListToCsv() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Integer getContabilita() {
		return contabilita;
	}

	public void setContabilita(Integer contabilita) {
		this.contabilita = contabilita;
	}

	public String getVoceIncasso() {
		return voceIncasso;
	}

	public void setVoceIncasso(String voceIncasso) {
		this.voceIncasso = voceIncasso;
	}

	public Long getIdEvento() {
		return idEvento;
	}

	public void setIdEvento(Long idEvento) {
		this.idEvento = idEvento;
	}

	public String getNumeroPermesso() {
		return numeroPermesso;
	}

	public void setNumeroPermesso(String numeroPermesso) {
		this.numeroPermesso = numeroPermesso;
	}

	public Long getReversale() {
		return reversale;
	}

	public void setReversale(Long reversale) {
		this.reversale = reversale;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public Long getNumeroPm() {
		return numeroPm;
	}

	public void setNumeroPm(Long numeroPm) {
		this.numeroPm = numeroPm;
	}

	public String getImportOriginario() {
		return importOriginario;
	}

	public void setImportOriginario(String importOriginario) {
		this.importOriginario = importOriginario;
	}

	public String getImportoRicalcolato() {
		return importoRicalcolato;
	}

	public void setImportoRicalcolato(String importoRicalcolato) {
		this.importoRicalcolato = importoRicalcolato;
	}

	public Long getNumeroFattura() {
		return numeroFattura;
	}

	public void setNumeroFattura(Long numeroFattura) {
		this.numeroFattura = numeroFattura;
	}

	public String[] getMappingStrategy() {
        return new String[]{

        		"Contabilita",
        		"Voce incasso",
        		"Evento",
    			"Permesso",
        		"Fattura",
        		"Reversale",
        		"Tipo Documento",
        		"Numero P.M.",
        		"Importo Originario",
        		"Importo Ricalcolato"
        };
	}
}
