package com.alkemytech.sophia.codman.rest.performing;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.alkemytech.sophia.codman.entity.Configuration;
import com.alkemytech.sophia.codman.rest.performing.service.IConfigurationService;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
@Path("performing/configurations")
public class ConfigurationsController {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private IConfigurationService configurationService;
	
	private Gson gson;

	@Inject
	protected ConfigurationsController(Gson gson,
			IConfigurationService configurationService) {
		this.gson = gson;
		this.configurationService = configurationService;
	}

	@GET
    @Path("/{domain}{no-key: (/)?}{key: ((?<=/)[\\w|\\-]+)?}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getConfiguration(@PathParam("domain") String domain, @PathParam("key") String key) {
		List<Configuration> configurations = configurationService.getConfiguration(domain, key);
		if (configurations == null) {
			return Response.status(HttpStatus.SC_NOT_FOUND).build();
		}
		return Response.ok(gson.toJson(configurations)).build();
	}
	
	@GET
	@Path("/history/{domain}/{key}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getConfigurationHistory(@PathParam("domain") String domain, @PathParam("key") String key, 
			@QueryParam(value = "settings" ) boolean settings) {
		List<Configuration> history = configurationService.getConfigurationHistory(domain, key, settings);
		if (history == null) {
			return Response.status(HttpStatus.SC_NOT_FOUND).build();
		}
		return Response.ok(gson.toJson(history)).build();
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getConfiguration(@PathParam("id") Long id) {
		Configuration configuration = configurationService.getConfiguration(id);
		if (configuration == null) {
			return Response.status(HttpStatus.SC_NOT_FOUND).build();
		}
		return Response.ok(gson.toJson(configuration)).build();
	}	
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveConfiguration(Configuration configuration, @Context HttpServletRequest request) {
		HttpSession session = request.getSession();
		String username = (String)session.getAttribute("sso.user.userName");
		configuration.setCreatedBy(username);		
		try {
			Configuration saved = configurationService.saveConfiguration(configuration);
			if (saved == null) {
				return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).build();
			}
			return Response.ok(gson.toJson(saved)).build();
		} catch (IllegalArgumentException e) {
			return Response.status(400).entity(gson.toJson(configuration)).build();
		} catch (Exception e) {
			return Response.status(500).entity(gson.toJson(configuration)).build();
		}
	}
	
	@DELETE
	@Produces("application/json")
	@Consumes("application/json")
	public Response delete(Configuration configuration) {
		try {
			configurationService.delete(configuration);
			return Response.ok(gson.toJson(configuration)).build();
		}
		catch (Exception e) {
			logger.error(String.format("Errore nell'eliminazione della configurazione: %s", gson.toJson(configuration)), e);
			return Response.status(500).build();
		}
	}

}
