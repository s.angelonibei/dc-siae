package com.alkemytech.sophia.codman.entity.performing;

import org.apache.commons.lang.StringUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Entity
@Table(name = "PERF_UTILIZZAZIONE")
public class Utilizzazione implements Serializable {

    @Id
    @Column(name = "ID_UTILIZZAZIONE", unique = true, nullable = false)
    private Long id;

    @Column(name = "ID_PROGRAMMA_MUSICALE")
    private Long idProgrammaMusicale;

    @Column(name = "CODICE_OPERA")
    private String codiceOpera;

    @Column(name = "TITOLO_COMPOSIZIONE")
    private String titoloComposizione;

    @Column(name = "COMPOSITORE")
    private String compositore;

    @Column(name = "NUMERO_PROGRAMMA_MUSICALE")
    private Long numeroProgrammaMusicale;

    @Column(name = "DURATA")
    private Double durata;

    @Column(name = "DURATA_INFERIORE_30SEC")
    private boolean durataInferiore30sec;

    @Column(name = "AUTORI")
    private String autori;

    @Column(name = "INTERPRETI")
    private String interpreti;

    @Column(name = "FLAG_CODIFICA")
    private String flagCodifica;

    @Column(name = "COMBINAZIONE_CODIFICA")
    private String combinazioneCodifica;

    @Column(name = "FLAG_PUBBLICO_DOMINIO")
    private String flagPubblicoDominio;

    @Column(name = "FLAG_MAG_NON_CONCESSA")
    private String flagMagNonConcessa;

    @Column(name = "TIPO_ACCERTAMENTO")
    private String tipoAccertamento;

    @Column(name = "NETTO_MATURATO")
    private Double nettoMaturato;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_PRIMA_CODIFICA")
    private Date dataPrimaCodifica;

    @Column(name = "PUNTI_ORIGINALI")
    private Double puntiOriginali;

    @Column(name = "PUNTI_RICALCOLATI")
    private Double puntiRicalcolati;

    @Column(name = "ID_COMBANA")
    private Long idCombana;

    @Column(name = "ID_CODIFICA")
    private Long idCodifica;

    @Transient
    private Double nettoOriginario;

    @Transient
    private Double importoValorizzato;

    @Transient
    private Double importoCedola;

    @Column(name = "FLAG_CEDOLA_ESCLUSA")
    private String flagCedolaEsclusa;

    @Transient
    private String tipoApprovazione;


    public Utilizzazione() {
        super();
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Long getIdProgrammaMusicale() {
        return idProgrammaMusicale;
    }

    public void setIdProgrammaMusicale(Long idProgrammaMusicale) {
        this.idProgrammaMusicale = idProgrammaMusicale;
    }


    public Long getNumeroProgrammaMusicale() {
        return numeroProgrammaMusicale;
    }

    public void setNumeroProgrammaMusicale(Long numeroProgrammaMusicale) {
        this.numeroProgrammaMusicale = numeroProgrammaMusicale;
    }


    public String getCompositore() {
        return compositore;
    }

    public void setCompositore(String compositore) {
        this.compositore = compositore;
    }

    public String getTitoloComposizione() {
        return titoloComposizione;
    }

    public void setTitoloComposizione(String titoloComposizione) {
        this.titoloComposizione = titoloComposizione;
    }

    public String getDurataInferiore30sec() {
        return durataInferiore30sec ? "1" : "0";
    }

    public boolean isDurataInferiore30sec() {
        return durataInferiore30sec;
    }

    public void setDurataInferiore30sec(String durataInferiore30sec) {
        this.durataInferiore30sec =
                StringUtils.isNotEmpty(durataInferiore30sec) &&
                        durataInferiore30sec.equalsIgnoreCase("1");
    }

    public void setDurataInferiore30sec(boolean isDurataInferiore30sec) {
        this.durataInferiore30sec = isDurataInferiore30sec;
    }

    public Double getDurata() {
        return durata;
    }

    public void setDurata(Double durata) {
        this.durata = durata;
    }

    public String getCodiceOpera() {
        return codiceOpera;
    }

    public void setCodiceOpera(String codiceOpera) {
        this.codiceOpera = codiceOpera;
    }

    public Double getPuntiOriginali() {
        return puntiOriginali;
    }

    public void setPuntiOriginali(Double puntiOriginali) {
        this.puntiOriginali = puntiOriginali;
    }


    public Double getImportoCedola() {
        return importoCedola;
    }

    public void setImportoCedola(Double importoCedola) {
        this.importoCedola = importoCedola;
    }

    public String getFlagPubblicoDominio() {
        return flagPubblicoDominio;
    }

    public void setFlagPubblicoDominio(String flagPubblicoDominio) {
        this.flagPubblicoDominio = flagPubblicoDominio;
    }

    public String getAutori() {
        return autori;
    }

    public void setAutori(String autori) {
        this.autori = autori;
    }

    public String getInterpreti() {
        return interpreti;
    }

    public void setInterpreti(String interpreti) {
        this.interpreti = interpreti;
    }

    public String getFlagCodifica() {
        return flagCodifica;
    }

    public void setFlagCodifica(String flagCodifica) {
        this.flagCodifica = flagCodifica;
    }

    public String getCombinazioneCodifica() {
        return combinazioneCodifica;
    }

    public void setCombinazioneCodifica(String combinazioneCodifica) {
        this.combinazioneCodifica = combinazioneCodifica;
    }

    public String getFlagMagNonConcessa() {
        return flagMagNonConcessa;
    }

    public void setFlagMagNonConcessa(String flagMagNonConcessa) {
        this.flagMagNonConcessa = flagMagNonConcessa;
    }

    public String getTipoAccertamento() {
        return tipoAccertamento;
    }

    public void setTipoAccertamento(String tipoAccertamento) {
        this.tipoAccertamento = tipoAccertamento;
    }

    public Double getNettoMaturato() {
        return nettoMaturato;
    }

    public void setNettoMaturato(Double nettoMaturato) {
        this.nettoMaturato = nettoMaturato;
    }


    public Date getDataPrimaCodifica() {
        return dataPrimaCodifica;
    }

    public void setDataPrimaCodifica(Date dataPrimaCodifica) {
        this.dataPrimaCodifica = dataPrimaCodifica;
    }


    public Long getIdCombana() {
        return idCombana;
    }

    public void setIdCombana(Long idCombana) {
        this.idCombana = idCombana;
    }


    public Long getIdCodifica() {
        return idCodifica;
    }

    public void setIdCodifica(Long idCodifica) {
        this.idCodifica = idCodifica;
    }


    public Double getPuntiRicalcolati() {
        return puntiRicalcolati;
    }

    public void setPuntiRicalcolati(Double puntiRicalcolati) {
        this.puntiRicalcolati = puntiRicalcolati;
    }


    public Double getNettoOriginario() {
        return nettoOriginario;
    }

    public void setNettoOriginario(Double nettoOriginario) {
        this.nettoOriginario = nettoOriginario;
    }


    public Double getImportoValorizzato() {
        return importoValorizzato;
    }

    public void setImportoValorizzato(Double importoValorizzato) {
        this.importoValorizzato = importoValorizzato;
    }

    public String getFlagCedolaEsclusa() {
        return flagCedolaEsclusa;
    }

    public void setFlagCedolaEsclusa(String flagCedolaEsclusa) {
        this.flagCedolaEsclusa = flagCedolaEsclusa;
    }

    public String getTipoApprovazione() {
        return tipoApprovazione;
    }

    public void setTipoApprovazione(String tipoApprovazione) {
        this.tipoApprovazione = tipoApprovazione;
    }
}
