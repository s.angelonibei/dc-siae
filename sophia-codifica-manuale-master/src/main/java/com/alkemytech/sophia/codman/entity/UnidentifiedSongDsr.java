package com.alkemytech.sophia.codman.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.alkemytech.sophia.codman.persistence.AbstractEntity;
import com.google.gson.GsonBuilder;

/*

 */
@Entity(name="UnidentifiedSongDsr")
@Table(name="unidentified_song_dsr")
@NamedQueries({@NamedQuery(name="UnidentifiedSongDsr.GetAll", query="SELECT x FROM UnidentifiedSongDsr x")})
@SuppressWarnings("serial")
public class UnidentifiedSongDsr extends AbstractEntity<String> {

	@Id
	@Column(name="id_util", nullable=false)
	private String idUtil;

	@Column(name="hash_id", nullable=false)
	private String hashId;
	
	@Column(name="id_dsr", nullable=false)
	private String idDsr;

	@Column(name="album_title")
	private String albumTitle;

	@Column(name="proprietary_id")
	private String proprietaryId;

	@Column(name="isrc")
	private String isrc;

	@Column(name="iswc")
	private String iswc;

	@Column(name="sales_count", nullable=false)
	private Long salesCount;

	@Column(name="dsp")
	private String dsp;

	@Column(name="insert_time", nullable=false)
	private Long insertTime;
	
	
	@Override
	public String getId() {
		return hashId;
	}
	
	public String getHashId() {
		return hashId;
	}
	public void setHashId(String hashId) {
		this.hashId = hashId;
	}
	public Long getInsertTime() {
		return insertTime;
	}
	public void setInsertTime(Long insertTime) {
		this.insertTime = insertTime;
	}

	public String getIdUtil() {
		return idUtil;
	}

	public void setIdUtil(String idUtil) {
		this.idUtil = idUtil;
	}

	public String getIdDsr() {
		return idDsr;
	}

	public void setIdDsr(String idDsr) {
		this.idDsr = idDsr;
	}

	public String getAlbumTitle() {
		return albumTitle;
	}

	public void setAlbumTitle(String albumTitle) {
		this.albumTitle = albumTitle;
	}

	public String getProprietaryId() {
		return proprietaryId;
	}

	public void setProprietaryId(String proprietaryId) {
		this.proprietaryId = proprietaryId;
	}

	public String getIsrc() {
		return isrc;
	}

	public void setIsrc(String isrc) {
		this.isrc = isrc;
	}

	public String getIswc() {
		return iswc;
	}

	public void setIswc(String iswc) {
		this.iswc = iswc;
	}

	public Long getSalesCount() {
		return salesCount;
	}

	public void setSalesCount(Long salesCount) {
		this.salesCount = salesCount;
	}

	public String getDsp() {
		return dsp;
	}

	public void setDsp(String dsp) {
		this.dsp = dsp;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}
}
