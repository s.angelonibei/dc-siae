package com.alkemytech.sophia.codman.entity;

import javax.persistence.*;
import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.codman.persistence.AbstractEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.google.gson.annotations.Expose;

import java.util.List;

@XmlRootElement
@Entity(name="AnagCCIDParam")
@Table(name="ANAG_CCID_PARAM")
@Produces("application/json")
public class AnagCCIDParam extends AbstractEntity<Long> {

	private static final long serialVersionUID = 1L;

    @Expose
	@Id
	@Column(name="ID_CCID_PARAM", nullable=false)
	private Long idCCIDParam;

    @Expose
	@Column(name="ID_CCID_VERSION", nullable=false)
	private Integer idCCIDVersion;

    @Expose
	@Column(name="NAME", nullable=false)
	private String name;

    @Expose
	@Column(name="VALUE")
	private String value;
	
	@Expose
	@Column(name="IS_DEFAULT")
	private Boolean isDefault;


    @Expose
	@OneToMany(mappedBy = "anagCCIDParams")
	private List<AnagCCIDConfigParam> anagCCIDConfigParamList;
		
	@Override
	public Long getId() {
		return idCCIDParam;
	}

	public Long getIdCCIDParam() {
		return idCCIDParam;
	}

	public void setIdCCIDParam(Long idCCIDParam) {
		this.idCCIDParam = idCCIDParam;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Boolean getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idCCIDParam == null) ? 0 : idCCIDParam.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AnagCCIDParam other = (AnagCCIDParam) obj;
		if (idCCIDParam == null) {
			if (other.idCCIDParam != null)
				return false;
		} else if (!idCCIDParam.equals(other.idCCIDParam))
			return false;
		return true;
	}

	public Integer getIdCCIDVersion() {
		return idCCIDVersion;
	}

	public void setIdCCIDVersion(Integer idCCIDVersion) {
		this.idCCIDVersion = idCCIDVersion;
	}
}
	