package com.alkemytech.sophia.codman.rest.performing.serviceimpl;

import java.util.List;

import com.alkemytech.sophia.codman.dto.performing.CarichiRipartizioneResponse;
import com.alkemytech.sophia.codman.dto.performing.CarichiRipartizioniRequest;
import com.alkemytech.sophia.codman.dto.performing.ConfigurazioniPeriodoDTO;
import com.alkemytech.sophia.codman.dto.performing.PerfValorizzazioneDTO;
import com.alkemytech.sophia.codman.dto.performing.ValorizzazioneRunDTO;
import com.alkemytech.sophia.codman.entity.PeriodoRipartizione;
import com.alkemytech.sophia.codman.entity.performing.PerfVoceIncasso;
import com.alkemytech.sophia.codman.entity.performing.dao.ValorizzazioneServiceDAO;
import com.alkemytech.sophia.codman.rest.performing.service.ValorizzazioneService;
import com.google.inject.Inject;

/**
 * Created by idilello on 6/21/16.
 */
public class ValorizzazioneServiceImpl implements ValorizzazioneService{
   
	private ValorizzazioneServiceDAO valorizzazioneService;

	@Inject
	public ValorizzazioneServiceImpl(ValorizzazioneServiceDAO valorizzazioneService){
		this.valorizzazioneService=valorizzazioneService;
	}

	@Override
	public Integer addConfigurazione(PerfValorizzazioneDTO perfValorizzazioneDTO) {
		return valorizzazioneService.addConfigurazione(perfValorizzazioneDTO);
	}

	@Override
	public List<ConfigurazioniPeriodoDTO> getConfigurazioni(String periodoRipartizione, String ripartizione,
			String tipologiaReport, String regola, String voceIncasso) {
		return valorizzazioneService.getConfigurazioni( periodoRipartizione,  ripartizione,
				 tipologiaReport,  regola,  voceIncasso);
	}

	@Override
	public List<PerfVoceIncasso> getVociIncasso() {
		return valorizzazioneService.getVociIncasso();
	}
	
	@Override
	public List<PerfVoceIncasso> getVociIncassoFittizie() {
		return valorizzazioneService.getVociIncassoFittizie();
	}
	
	@Override
	public List<PeriodoRipartizione> getPeriodiRipartizione() {
		return valorizzazioneService.getPeriodiRipartizione();
	}

	@Override
	public Integer updateConfigurazione(PerfValorizzazioneDTO perfValorizzazioneDTO) {
		return valorizzazioneService.updateConfigurazione(perfValorizzazioneDTO);
	}

	@Override
	public List<ConfigurazioniPeriodoDTO> getStoricoConfigurazione(String periodo, String voceIncasso,
			String tipoReport) {
		return valorizzazioneService.getStoricoConfigurazione( periodo,  voceIncasso,
				 tipoReport);
	
	}

	@Override
	public CarichiRipartizioneResponse getCarichiRipartizione(String codicePeriodo) {
		return valorizzazioneService.getCarichiRipartizione(codicePeriodo);
	}


	@Override
	public void deleteValorizzazioneConfigurazione(String periodoRipartizione, String voceIncasso, String tipologiaReport, String user) {
		valorizzazioneService.deleteValorizzazioneConfigurazione(periodoRipartizione, voceIncasso, tipologiaReport, user);
	}

	@Override
	public Integer approvaRipartizioni(CarichiRipartizioniRequest carichiRipartizioniRequest) {
		return valorizzazioneService.approvaRipartizioni(carichiRipartizioniRequest);
	}

	@Override
	public ValorizzazioneRunDTO getValorizzazioneInfo() {
		return valorizzazioneService.getValorizzazioneInfo();
	}



}
