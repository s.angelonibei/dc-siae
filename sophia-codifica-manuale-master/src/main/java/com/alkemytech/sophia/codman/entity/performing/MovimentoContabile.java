package com.alkemytech.sophia.codman.entity.performing;

//import it.siae.valorizzatore.utility.Utility;

import javax.persistence.*;

import com.alkemytech.sophia.codman.utils.AccountingUtils;
import com.alkemytech.sophia.codman.utils.DateUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by idilello on 6/7/16.
 */
@Entity
@Table(name = "PERF_MOVIMENTO_CONTABILE")
public class MovimentoContabile  implements Serializable {

    private Long idMovimentoContabile;
//    private Long idCartella;
    private Long idEvento;
    private Long idProgrammaMusicale;
    private String tipologiaMovimento;
    private Double importoManuale;
    private Double importoTotDemLordo;
    private Double importoTotDem;
    private Character flagRripartitoSemestrePrecedente;
    private Date dataOraUltimaModifica;
    private String voceIncasso;
    private String numProgrammaMusicale;
    private Long reversale;
    private Integer contabilita;
    private Double contabilitaOrg;
    private Character flagSospeso;
    private Character flagNoMaggiorazione;
    private Long numeroFattura;
    private Integer numeroPmPrevisti;
    private Integer numeroPmPrevistiSpalla;
    private String tipoDocumentoContabile;
    private String utenteUltimaModifica;
    private Date dataIns;
    private Character flagGruppoPrincipale;
    private String numeroPermesso;
    private Double importoRicalcolato;
    private Double importoRicalcolatoSingolaCedola;
    private Date dataReversale;
    private Date dataRientroPM;

    private Long totaleCedole;
    private Double totaleDurataCedole;
    private Double importoEvento;
    private String motivazioneSospensione;
    private List<ImportoRicalcolato> importiRicalcolati;
    private Manifestazione evento;
    private Integer importoOrig;
    private Integer sganciabile;
	private BigDecimal importoValorizzato;
	private BigDecimal puntiProgrammaMusicale;


	
    @Id
    @SequenceGenerator(name="MOVIMENTO_CONTABILE",sequenceName = "MOVIMENTO_CONTABILE_SEQ",allocationSize=1)
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="MOVIMENTO_CONTABILE")
    @Column(name = "ID_MOVIMENTO_CONTABILE", unique = true, nullable = false)
    public Long getId() {
        return idMovimentoContabile;
    }

    public void setId(Long id) {
        this.idMovimentoContabile = id;
    }


//    @Column(name = "ID_CARTELLA",nullable = false)
//    public Long getIdCartella() {
//        return idCartella;
//    }
//
//    public void setIdCartella(Long idCartella) {
//        this.idCartella = idCartella;
//    }

    @Column(name = "ID_EVENTO")
    public Long getIdEvento() {
        return idEvento;
    }

    public void setIdEvento(Long idEvento) {
        this.idEvento = idEvento;
    }

    @Column(name = "ID_PROGRAMMA_MUSICALE",nullable = false)
    public Long getIdProgrammaMusicale() {
        return idProgrammaMusicale;
    }

    public void setIdProgrammaMusicale(Long idProgrammaMusicale) {
        this.idProgrammaMusicale = idProgrammaMusicale;
    }


    @Column(name = "TIPOLOGIA_MOVIMENTO")
    public String getTipologiaMovimento() {
        return tipologiaMovimento;
    }

    public void setTipologiaMovimento(String tipologiaMovimento) {
        this.tipologiaMovimento = tipologiaMovimento;
    }

    @Column(name = "IMPORTO_MANUALE")
    public Double getImportoManuale() {
        return importoManuale;
    }

    public void setImportoManuale(Double importoManuale) {
        this.importoManuale = importoManuale;
    }

    @Column(name = "TOTALE_DEM_LORDO_PM")
    public Double getImportoTotDemLordo() {
        return importoTotDemLordo;
    }

    public void setImportoTotDemLordo(Double importoTotDemLordo) {
        this.importoTotDemLordo = importoTotDemLordo;
    }


    @Column(name = "IMPORTO_DEM_TOTALE")
    public Double getImportoTotDem() {
        return importoTotDem;
    }

    public void setImportoTotDem(Double importoTotDem) {
        this.importoTotDem = importoTotDem;
    }


    @Column(name = "PM_RIPARTITO_SEMESTRE_PREC")
    public Character getFlagRipartitoSemestrePrecedente() {
        if (flagRripartitoSemestrePrecedente==null)
            flagRripartitoSemestrePrecedente = 'N';

        return flagRripartitoSemestrePrecedente;
    }

    public void setFlagRipartitoSemestrePrecedente(Character flagRripartitoSemestrePrecedente) {
        this.flagRripartitoSemestrePrecedente = flagRripartitoSemestrePrecedente;
    }


    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_ORA_ULTIMA_MODIFICA")
    public Date getDataOraUltimaModifica() {
        return dataOraUltimaModifica;
    }

    public void setDataOraUltimaModifica(Date dataOraUltimaModifica) {
        this.dataOraUltimaModifica = dataOraUltimaModifica;
    }

    @Column(name = "CODICE_VOCE_INCASSO")
    public String getVoceIncasso() {
        return voceIncasso;
    }

    public void setVoceIncasso(String voceIncasso) {
        this.voceIncasso = voceIncasso;
    }


    @Column(name = "NUMERO_PM")
    public String getNumProgrammaMusicale() {
        return numProgrammaMusicale;
    }

    public void setNumProgrammaMusicale(String numProgrammaMusicale) {
        this.numProgrammaMusicale = numProgrammaMusicale;
    }

    @Column(name = "NUMERO_REVERSALE")
    public Long getReversale() {
        return reversale;
    }


    public void setReversale(Long reversale) {
        this.reversale = reversale;
    }

    @Column(name = "CONTABILITA")
    public Integer getContabilita() {
        return contabilita;
    }

    public void setContabilita(Integer contabilita) {
        this.contabilita = contabilita;
    }

    @Column(name = "CONTABILITA_ORIG")
    public Double getContabilitaOrg() {
        return contabilitaOrg;
    }

    public void setContabilitaOrg(Double contabilitaOrg) {
        this.contabilitaOrg = contabilitaOrg;
    }

    @Column(name = "FLAG_SOSPESO")
    public Character getFlagSospeso() {
        return flagSospeso;
    }

    public void setFlagSospeso(Character flagSospeso) {
        this.flagSospeso = flagSospeso;
    }

    @Column(name = "FLAG_NO_MAGGIORAZIONE")
    public Character getFlagNoMaggiorazione() {
        return flagNoMaggiorazione;
    }

    public void setFlagNoMaggiorazione(Character flagNoMaggiorazione) {
        this.flagNoMaggiorazione = flagNoMaggiorazione;
    }

    @Column(name = "NUMERO_FATTURA")
    public Long getNumeroFattura() {
        return numeroFattura;
    }

    public void setNumeroFattura(Long numeroFattura) {
        this.numeroFattura = numeroFattura;
    }

    @Column(name = "NUMERO_PM_PREVISTI")
    public Integer getNumeroPmPrevisti() {
        if (numeroPmPrevisti==null)
            numeroPmPrevisti=1;
        return numeroPmPrevisti;
    }


    public void setNumeroPmPrevisti(Integer numeroPmPrevisti) {
        this.numeroPmPrevisti = numeroPmPrevisti;
    }

    @Column(name = "NUMERO_PM_PREVISTI_SPALLA")
    public Integer getNumeroPmPrevistiSpalla() {
        if (numeroPmPrevistiSpalla==null)
            numeroPmPrevistiSpalla=1;

        return numeroPmPrevistiSpalla;
    }

    public void setNumeroPmPrevistiSpalla(Integer numeroPmPrevistiSpalla) {
        this.numeroPmPrevistiSpalla = numeroPmPrevistiSpalla;
    }

    @Column(name = "TIPO_DOCUMENTO_CONTABILE")
    public String getTipoDocumentoContabile() {
        return tipoDocumentoContabile;
    }

    public void setTipoDocumentoContabile(String tipoDocumentoContabile) {
        this.tipoDocumentoContabile = tipoDocumentoContabile;
    }

    @Column(name = "UTENTE_ULTIMA_MODIFICA")
    public String getUtenteUltimaModifica() {
        return utenteUltimaModifica;
    }

    public void setUtenteUltimaModifica(String utenteUltimaModifica) {
        this.utenteUltimaModifica = utenteUltimaModifica;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_INS")
    public Date getDataIns() {
        return dataIns;
    }

    public void setDataIns(Date dataIns) {
        this.dataIns = dataIns;
    }


    @Column(name = "FLAG_GRUPPO_PRINCIPALE")
    public Character getFlagGruppoPrincipale() {
        if (flagGruppoPrincipale!=null && flagGruppoPrincipale.equals(0))
            flagGruppoPrincipale='0';

        if (flagGruppoPrincipale!=null && flagGruppoPrincipale.equals(1))
            flagGruppoPrincipale='1';

        if (flagGruppoPrincipale==null)
            flagGruppoPrincipale='1';

        return flagGruppoPrincipale;
    }

    public void setFlagGruppoPrincipale(Character flagGruppoPrincipale) {
        this.flagGruppoPrincipale = flagGruppoPrincipale;
    }

    @Column(name = "NUMERO_PERMESSO")
    public String getNumeroPermesso() {
        return numeroPermesso;
    }

    public void setNumeroPermesso(String numeroPermesso) {
        this.numeroPermesso = numeroPermesso;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_REVERSALE")
    public Date getDataReversale() {
        return dataReversale;
    }

    public void setDataReversale(Date dataReversale) {
        this.dataReversale = dataReversale;
    }

	@Column(name="IMPORTO_VALORIZZATO", precision=10, scale=7)    
    public BigDecimal getImportoValorizzato() {
		return importoValorizzato;
	}

	public void setImportoValorizzato(BigDecimal importoValorizzato) {
		this.importoValorizzato = importoValorizzato;
	}

	@Column(name="PUNTI_PROGRAMMA_MUSICALE", precision=10, scale=7)
	public BigDecimal getPuntiProgrammaMusicale() {
		return puntiProgrammaMusicale;
	}

	public void setPuntiProgrammaMusicale(BigDecimal puntiProgrammaMusicale) {
		this.puntiProgrammaMusicale = puntiProgrammaMusicale;
	}

	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_RIENTRO")
    public Date getDataRientroPM() {
        return dataRientroPM;
    }

    public void setDataRientroPM(Date dataRientroPM) {
        this.dataRientroPM = dataRientroPM;
    }

    @Transient
    public String getDataRientroPMStr() {
        if (this.dataRientroPM != null)
//        	return Utility.dateString(this.dataRientroPM, Utility.OracleFormat_D);
        	return DateUtils.dateToString(this.dataRientroPM,DateUtils.DATE_ITA_FORMAT_SLASH);
        else
          return "";
    }

    @Transient
    public String getDataReversaleStr() {
        if (this.dataReversale != null)
//            return Utility.dateString(this.dataReversale, Utility.OracleFormat_D);
        	return DateUtils.dateToString(this.dataReversale,DateUtils.DATE_ITA_FORMAT_SLASH);
        else
            return "";
    }

    @Transient
    public Double getImportoRicalcolato() {
        return importoRicalcolato;
    }

    public void setImportoRicalcolato(Double importoRicalcolato) {

        this.importoRicalcolato = importoRicalcolato;
    }

    @Transient
    public Double getImportoRicalcolatoSingolaCedola() {
        return importoRicalcolatoSingolaCedola;
    }

    public void setImportoRicalcolatoSingolaCedola(Double importoRicalcolatoSingolaCedola) {
        this.importoRicalcolatoSingolaCedola = importoRicalcolatoSingolaCedola;
    }

    @Transient
    public Long getTotaleCedole() {
        return totaleCedole;
    }

    public void setTotaleCedole(Long totaleCedole) {
        this.totaleCedole = totaleCedole;
    }

    @Transient
    public Double getTotaleDurataCedole() {
        return totaleDurataCedole;
    }

    public void setTotaleDurataCedole(Double totaleDurataCedole) {
        this.totaleDurataCedole = totaleDurataCedole;
    }

    @Transient
    public Double getImportoEvento() {
        return importoEvento;
    }

    public void setImportoEvento(Double importoEvento) {
        this.importoEvento = importoEvento;
    }

    @Transient
    public String getMotivazioneSospensione() {
        return motivazioneSospensione;
    }

    public void setMotivazioneSospensione(String motivazioneSospensione) {
        this.motivazioneSospensione = motivazioneSospensione;
    }

    @Transient
    public List<ImportoRicalcolato> getImportiRicalcolati() {
        return importiRicalcolati;
    }

    public void setImportiRicalcolati(List<ImportoRicalcolato> importiRicalcolati) {
        this.importiRicalcolati = importiRicalcolati;
    }

    @Transient
    public Manifestazione getEvento() {
        return evento;
    }

    public void setEvento(Manifestazione evento) {
        this.evento = evento;
    }

    @Transient
    public String getImportoTotDemFormatted() {
//        return Utility.displayFormatted(importoTotDem);
        return AccountingUtils.displayFormatted(importoTotDem);
    }

    @Transient
    public String getImportoManualeFormatted() {
//        return Utility.displayFormatted(importoManuale);
        return AccountingUtils.displayFormatted(importoManuale);
    }

    @Transient
    public String getImportoTotDemLordoFormatted() {
//        return Utility.displayFormatted(importoTotDemLordo);
        return AccountingUtils.displayFormatted(importoTotDemLordo);
    }

    @Transient
    public String getImportoEventoFormatted() {
//        return Utility.displayFormatted(importoEvento);
        return AccountingUtils.displayFormatted(importoEvento);
    }

    @Transient
    public String getImportoRicalcolatoFormatted() {
//        return Utility.displayFormatted(importoRicalcolato);
        return AccountingUtils.displayFormatted(importoRicalcolato);
    }

    @Transient
    public String getImportoRicalcolatoSingolaCedolaFormatted() {
//        return Utility.displayFormatted(importoRicalcolatoSingolaCedola);
        return AccountingUtils.displayFormatted(importoRicalcolatoSingolaCedola);
    }

    @Transient
	public Integer getImportoOrig() {
		return importoOrig;
	}

	public void setImportoOrig(Integer importoOrig) {
		this.importoOrig = importoOrig;
	}

    @Transient
	public Integer getSganciabile() {
		return sganciabile;
	}

	public void setSganciabile(Integer sganciabile) {
		this.sganciabile = sganciabile;
	}

    
}
