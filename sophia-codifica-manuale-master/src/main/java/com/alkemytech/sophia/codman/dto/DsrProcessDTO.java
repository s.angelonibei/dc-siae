package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;

import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;

@Produces("application/json")
@XmlRootElement 

public class DsrProcessDTO  implements Serializable{

	private static final long serialVersionUID = -1675962788160728725L;

	private Long idDsrProcessConfig;
	private Long priority;
	private String dsp;
	private String utilizationType;
	private String country;
	private String idDsrProcess;	
	
	public DsrProcessDTO(){
		
	}

	public Long getIdDsrProcessConfig() {
		return idDsrProcessConfig;
	}

	public void setIdDsrProcessConfig(Long idDsrProcessConfig) {
		this.idDsrProcessConfig = idDsrProcessConfig;
	}

	public Long getPriority() {
		return priority;
	}

	public void setPriority(Long priority) {
		this.priority = priority;
	}

	public String getDsp() {
		return dsp;
	}

	public void setDsp(String dsp) {
		this.dsp = dsp;
	}

	public String getUtilizationType() {
		return utilizationType;
	}

	public void setUtilizationType(String utilizationType) {
		this.utilizationType = utilizationType;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getIdDsrProcess() {
		return idDsrProcess;
	}

	public void setIdDsrProcess(String idDsrProcess) {
		this.idDsrProcess = idDsrProcess;
	}
	
	
	
}
