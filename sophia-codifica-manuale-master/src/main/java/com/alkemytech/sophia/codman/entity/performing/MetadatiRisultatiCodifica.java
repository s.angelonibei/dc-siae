package com.alkemytech.sophia.codman.entity.performing;

import java.io.Serializable;
import java.math.BigDecimal;

public class MetadatiRisultatiCodifica implements Serializable {

    private Double sogliaConf;
    private Double distSecond;
    private Double ecoValue;
    private String azione;

    public MetadatiRisultatiCodifica() {
    }

    public MetadatiRisultatiCodifica(Double sogliaConf, Double distSecond, Double ecoValue, String azione) {
        this.sogliaConf = sogliaConf;
        this.distSecond = distSecond;
        this.ecoValue = ecoValue;
        this.azione = azione;
    }

    public Double getSogliaConf() {
        sogliaConf.toString().replace(',', '.');
        return sogliaConf;
    }

    public void setSogliaConf(Double sogliaConf) {
        this.sogliaConf = sogliaConf;
    }

    public Double getDistSecond() {
        distSecond.toString().replace(',', '.');
        return distSecond;
    }

    public void setDistSecond(Double distSecond) {
        this.distSecond = distSecond;
    }

    public Double getEcoValue() {
        ecoValue.toString().replace(',', '.');
        return ecoValue;
    }

    public void setEcoValue(Double ecoValue) {
        this.ecoValue = ecoValue;
    }

    public String getAzione() {
        return azione;
    }

    public void setAzione(String azione) {
        this.azione = azione;
    }
}
