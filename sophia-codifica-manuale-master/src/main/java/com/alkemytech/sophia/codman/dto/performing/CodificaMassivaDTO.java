package com.alkemytech.sophia.codman.dto.performing;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class CodificaMassivaDTO {

	private String receivedMessage;
	private Boolean pending;
	private Timestamp insertTime;
	private Timestamp lastUpdateTime;
	private String utente;
	private String fileInput;
	private String fileOutput;


	public CodificaMassivaDTO() {
		super();
	}

	public CodificaMassivaDTO(String receivedMessage, Boolean pending, Timestamp insertTime,
							  Timestamp lastUpdateTime,String utente, String fileInput, String fileOutput) {

		this.receivedMessage = receivedMessage;
		this.pending = pending;
		this.insertTime = insertTime;
		this.lastUpdateTime = lastUpdateTime;
		this.utente = utente;
		this.fileInput = fileInput;
		this.fileOutput = fileOutput;
	}

	public String getReceivedMessage() {
		return receivedMessage;
	}

	public void setReceivedMessage(String receivedMessage) {
		this.receivedMessage = receivedMessage;
	}

	public Boolean getPending() {
		return pending;
	}

	public void setPending(Boolean pending) {
		this.pending = pending;
	}

	public Timestamp getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(Timestamp insertTime) {
		this.insertTime = insertTime;
	}

	public Timestamp getLastUpdateTime() {
		return lastUpdateTime;
	}

	public void setLastUpdateTime(Timestamp lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

	public String getUtente() {
		return utente;
	}

	public void setUtente(String utente) {
		this.utente = utente;
	}

	public String getFileInput() {
		return fileInput;
	}

	public void setFileInput(String fileInput) {
		this.fileInput = fileInput;
	}

	public String getFileOutput() {
		return fileOutput;
	}

	public void setFileOutput(String fileOutput) {
		this.fileOutput = fileOutput;
	}
}
