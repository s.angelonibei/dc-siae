package com.alkemytech.sophia.codman.entity.performing;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity @IdClass(UtilizzazioniAssegnatePK.class)
@Table(name = 	"PERF_UTILIZZAZIONI_ASSEGNATE")
public class UtilizzazioniAssegnate {

    @Id
	@Column(name = "ID_CODIFICA")
	private Long idCodifica;

    @Id
	@Column(name = "ID_SESSIONE_CODIFICA")
	private Long idSessioneCodifica;

	@Column(name = "TIPO_AZIONE")
	private String tipoCodifica;

	public UtilizzazioniAssegnate() {
		super();
	}

	public UtilizzazioniAssegnate(Long idCodifica, Long idSessioneCodifica, String tipoCodifica) {
		super();
		this.idCodifica = idCodifica;
		this.idSessioneCodifica = idSessioneCodifica;
		this.tipoCodifica = tipoCodifica;
	}

	public Long getIdCodifica() {
		return idCodifica;
	}

	public void setIdCodifica(Long idCodifica) {
		this.idCodifica = idCodifica;
	}

	public Long getIdSessioneCodifica() {
		return idSessioneCodifica;
	}

	public void setIdSessioneCodifica(Long idSessioneCodifica) {
		this.idSessioneCodifica = idSessioneCodifica;
	}

	public String getTipoCodifica() {
		return tipoCodifica;
	}

	public void setTipoCodifica(String tipoCodifica) {
		this.tipoCodifica = tipoCodifica;
	}
}
