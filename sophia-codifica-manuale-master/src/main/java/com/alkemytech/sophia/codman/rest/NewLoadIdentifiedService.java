package com.alkemytech.sophia.codman.rest;

import com.alkemytech.sophia.codman.dto.IdentifiedSongDTO;
import com.alkemytech.sophia.codman.dto.IdentifiedSongDsrDTO;
import com.alkemytech.sophia.codman.dto.LoadIdentifiedBlackListDTO;
import com.alkemytech.sophia.codman.dto.LoadIdentifiedSongDTO;
import com.alkemytech.sophia.codman.entity.IdentifiedSong;
import com.alkemytech.sophia.codman.entity.LoadIdentifiedSong;
import com.alkemytech.sophia.codman.entity.LoadIdentifiedSongBlacklist;
import com.alkemytech.sophia.codman.entity.LoadIdentifiedSongDsr;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.alkemytech.sophia.codman.guice.UnidentifiedDataSource;
import com.alkemytech.sophia.codman.multimedialelocale.entity.UnidentifiedSong;
import com.alkemytech.sophia.codman.sso.SsoSessionAttrs;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import org.apache.commons.lang.StringUtils;
import org.jooq.DSLContext;
import org.jooq.SelectQuery;
import org.jooq.impl.DSL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.jooq.impl.DSL.*;

@Path("new-load-identified")
public class NewLoadIdentifiedService {

    private final Provider<EntityManager> provider;
    private final Provider<EntityManager> mcmdbProvider;
    private final KnowledgeBaseService knowledgeBaseService;
    private final Gson gson;
    private final long lockDurationMinutes;
    private static Logger logger = LoggerFactory.getLogger(NewLoadIdentifiedService.class);
    private DSLContext jooq;
    private HttpSession session;

    @Inject
    protected NewLoadIdentifiedService(@UnidentifiedDataSource Provider<EntityManager> provider,
                                       @McmdbDataSource Provider<EntityManager> mcmdbProvider,
                                       KnowledgeBaseService knowledgeBaseService,
                                       Gson gson,
                                       @Named("unidentified.lock_duration_minutes") long lockDurationMinutes,
                                       @UnidentifiedDataSource DSLContext jooq,
                                       HttpSession session) {
        super();
        this.provider = provider;
        this.mcmdbProvider = mcmdbProvider;
        this.knowledgeBaseService = knowledgeBaseService;
        this.jooq = jooq;
        this.gson = gson;
        this.lockDurationMinutes = lockDurationMinutes;
        this.session = session;
    }

    @GET
    @Path("song/{hashid}")
    @Produces("application/json")
    public Response searchByHashId(@PathParam("hashid") String hashId,
                                   @QueryParam("user") String user) {
        final EntityManager entityManager = provider.get();
        try {
            SelectQuery<?> select = getRecords();
            if (StringUtils.isEmpty(user)) {
                user = (String) session.getAttribute(SsoSessionAttrs.USERNAME);
            }
            if ("undefined".equals(hashId) || StringUtils.isEmpty(hashId)) {
                //select.addConditions(field("username").eq(user).or(field("username").isNull()));
                select.addLimit(1);
                hashId = select.fetchInto(LoadIdentifiedSongDTO.class).get(0).getHashId();
            }
            entityManager.getTransaction().begin();
            /*final LoadIdentifiedSong loadIdentifiedSong = entityManager.createNamedQuery("LoadIdentifiedSong.findByIdAndLastManualTimeLessThan", LoadIdentifiedSong.class).
                    setParameter("hashId", hashId)
                    //.setParameter("lastManualTime", System.currentTimeMillis())
                    //.setParameter("username", user)
                    .setLockMode(LockModeType.PESSIMISTIC_WRITE)
                    .getSingleResult();
            */
            select.addConditions(field("hash_id").eq(hashId));
            LoadIdentifiedSongDTO loadIdentifiedSongDTO = select.fetchInto(LoadIdentifiedSongDTO.class).get(0);

            if (null != loadIdentifiedSongDTO) {
                logger.info("opera {} bloccata da {}", loadIdentifiedSongDTO.getHashId(), user);
                LoadIdentifiedSong loadIdentifiedSong = entityManager.find(LoadIdentifiedSong.class, hashId);
                loadIdentifiedSong.setLastManualTime(System.currentTimeMillis()  +
                       TimeUnit.MINUTES.toMillis(lockDurationMinutes));
                loadIdentifiedSong.setUsername(user);
                entityManager.merge(loadIdentifiedSong);
                entityManager.flush();

                entityManager.getTransaction().commit();
                entityManager.getTransaction().begin();

                entityManager
                        .createNamedQuery("LoadIdentifiedSong.updateByUsernameAndHashId", LoadIdentifiedSong.class)
                        .setParameter("hashId", hashId)
                        .setParameter("username", user)
                        .setParameter("lastManualTime", System.currentTimeMillis())
                        .setParameter("lastAutoTime", System.currentTimeMillis())
                        .executeUpdate();
            }
            entityManager.getTransaction().commit();
            return Response.ok(gson.toJson(loadIdentifiedSongDTO)).build();
        } catch (
                NoResultException e) {
            //LoadIdentifiedSong result = new LoadIdentifiedSong();
            LoadIdentifiedSongDTO result = new LoadIdentifiedSongDTO();
            entityManager.getTransaction().rollback();
            return Response.ok(gson.toJson(result)).build();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            entityManager.getTransaction().rollback();
        }
        return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR).build(); // 500
    }

    @GET
    @Path("nextSong/{hashid}")
    public Response getNextSong(@PathParam("hashid") String hashid,
                                @QueryParam("user") String user,
                                @QueryParam("salesCount") String salesCount){
        if (hashid != null) {
            try {
                SelectQuery<?> select = getRecords();
                //select.addOrderBy(field("sales_count").desc());
                select.addOrderBy(field("hash_id"));
                select.addSeekAfter(val(salesCount), val(hashid));
                //select.addConditions(field("hash_id").greaterThan(hashid));
                //select.addConditions(field("username").eq(user).or(field("username").isNull()));
                select.addLimit(1);
                //Controllo su e restituire oggetto vuoto
                String nextHashId = select.fetchInto(LoadIdentifiedSongDTO.class).get(0).getHashId();
                if (nextHashId != null || !nextHashId.isEmpty()) {
                    return searchByHashId(nextHashId, user);
                }
            } catch (Exception IndexOutException){
                return Response.ok(gson.toJson(new LoadIdentifiedBlackListDTO())).build();
            }
        }
        return Response.status(500).build();
    }


    @GET
    @Path("getDsrByHashId/{hashId}")
    @Produces("application/json")
    @SuppressWarnings("unchecked")
    public Response dsrByHash(@PathParam("hashId") String hashId) {
        EntityManager entityManager = null;
        List<LoadIdentifiedSongDsr> result;
        try {
            entityManager = provider.get();
            entityManager.getTransaction().begin();
            final Query q = entityManager.createQuery("select x from LoadIdentifiedSongDsr x where x.hashId = :hashId order by x.salesCount desc");
            q.setParameter("hashId", hashId);
            result = (List<LoadIdentifiedSongDsr>) q.getResultList();
            return Response.ok(gson.toJson(result)).build();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.status(500).build();
    }


    @GET
    @Path("searchCodificato")
    @Produces("application/json")
    public Response searchCodificato(
            @QueryParam("uuid") String uuid,
            @QueryParam("artists") String artists,
            @QueryParam("title") String title,
            @DefaultValue("0") @QueryParam("first") int first,
            @DefaultValue("50") @QueryParam("last") int last) {

        try {
            SelectQuery<?> select = getRecords();

            if (StringUtils.isNotEmpty(title)) {
                select.addConditions(field("title").like("%" + title + "%"));
            }
            if (StringUtils.isNotEmpty(artists)) {
                select.addConditions(field("artists").like("%" + artists + "%"));
            }
            if (StringUtils.isNotEmpty(uuid)) {
                select.addConditions(field("uuid").eq(uuid));
            }
            select.addOrderBy(field("sales_count").desc());
            select.addLimit(first, (1 + last - first));


            List<LoadIdentifiedSongDTO> results = select.fetchInto(LoadIdentifiedSongDTO.class);

            final PagedResult result = new PagedResult();
            if (null != results && !results.isEmpty()) {
                final int maxrows = last - first;
                final boolean hasNext = results.size() > maxrows;
                result.setRows(!hasNext ? results : results.subList(0, maxrows))
                        .setMaxrows(maxrows)
                        .setFirst(first)
                        .setLast(hasNext ? last : first + results.size())
                        .setHasNext(hasNext)
                        .setHasPrev(first > 0);
            } else {
                result.setMaxrows(0)
                        .setFirst(0)
                        .setLast(0)
                        .setHasNext(false)
                        .setHasPrev(false);
            }
            return Response.ok(gson.toJson(result)).build();
        } catch (Exception e) {
            return Response.status(500).build();
        }
    }

    @PUT
    @Path("insertManual")
    public Response insertManual(IdentifiedSong identifiedSong){
        EntityManager em = provider.get();
        EntityManager mcmdb = mcmdbProvider.get();
        try {
            LoadIdentifiedSong songToBlackList = em.find(LoadIdentifiedSong.class, identifiedSong.getHashId());

            if (songToBlackList != null) {

                Query q = mcmdb.createNativeQuery("select * from load_identified_song_blacklist x" +
                        " where x.title = ?" +
                        " and x.artists = ?" +
                        " and x.codice_uuid = ?");
                q.setParameter(1, identifiedSong.getSiadaTitle());
                q.setParameter(2, identifiedSong.getSiadaArtists());
                q.setParameter(3, songToBlackList.getUuid());

                List<LoadIdentifiedSongBlacklist> blacklistExtract = q.getResultList();

                if(blacklistExtract != null && blacklistExtract.isEmpty()) {
                    LoadIdentifiedSongBlacklist lisb = new LoadIdentifiedSongBlacklist();
                    lisb.setTitle(identifiedSong.getSiadaTitle());
                    lisb.setArtists(identifiedSong.getSiadaArtists());
                    //if (identifiedSong.getSiaeWorkCode() != null)
                    //lisb.setCodiceOpera(Integer.parseInt(identifiedSong.getSiaeWorkCode()));
                    lisb.setCodiceUuid(songToBlackList.getUuid());

                    mcmdb.getTransaction().begin();
                    mcmdb.persist(lisb);
                    mcmdb.getTransaction().commit();
                }
            }
        } catch (Exception e){
            if (mcmdb.getTransaction().isActive())
                mcmdb.getTransaction().rollback();
            logger.error("insertManual: ", e);
        }
        identifiedSong.setIdentificationType("manual");

        return identify(identifiedSong);
    }

    @PUT
    @Path("identify")
    @Produces("application/json")
    public Response identify(IdentifiedSong identifiedSong) {

        final EntityManager em = provider.get();
        try {

            // create data transfer object
            final IdentifiedSongDTO identifiedSongDTO = createDTO(em, identifiedSong);
            if (null == identifiedSongDTO.getDsrs()) {
                logger.debug("identify: nessuna utilizzazione legata all'opera identificata [" +
                        identifiedSong.getSiaeWorkCode() + "][" + identifiedSong.getTitle() + "]");
                return Response.status(HttpServletResponse.SC_NOT_FOUND)
                        .entity(gson.toJson(RestUtils.asMap("status", "KO",
                                "errorMessage", "no utilization found for given work"))).build();
            }

            // update knowledge base
            final Response response = knowledgeBaseService.identify(identifiedSongDTO);

            // search & insert or update identified_song
            em.getTransaction().begin();
            final IdentifiedSong alreadyIdentifiedSong = em
                    .find(IdentifiedSong.class, identifiedSong.getHashId());
            if (null == alreadyIdentifiedSong) { // not found
                identifiedSong.setInsertTime(System.currentTimeMillis());
                if (HttpServletResponse.SC_OK == response.getStatus()) {
                    identifiedSong.setSophiaUpdateTime(System.currentTimeMillis());
                }
                em.persist(identifiedSong);
            } else { // already exists
                if (HttpServletResponse.SC_OK == response.getStatus()) {
                    alreadyIdentifiedSong.setSophiaUpdateTime(System.currentTimeMillis());
                }
                alreadyIdentifiedSong.setSiaeWorkCode(identifiedSong.getSiaeWorkCode());
                em.merge(alreadyIdentifiedSong);
            }

            // search & update identified_song
            final LoadIdentifiedSong loadIdentifiedSong = em
                    .find(LoadIdentifiedSong.class, identifiedSong.getHashId());
            if (null != loadIdentifiedSong) {
                if ("manual".equalsIgnoreCase(identifiedSong.getIdentificationType())) {
                    loadIdentifiedSong.setLastManualTime(Long.MAX_VALUE);
                } else {
                    loadIdentifiedSong.setLastAutoTime(Long.MAX_VALUE);
                }
                em.merge(loadIdentifiedSong);
            }
            em.getTransaction().commit();

            return response;

        } catch (Exception e) {
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
            logger.error("identify", e);
            return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR)
                    .entity(gson.toJson(RestUtils.asMap("status", "KO",
                            "errorMessage", "unespected error",
                            "stackTrace", RestUtils.asString(e)))).build();
        }

    }

    private IdentifiedSongDTO createDTO(EntityManager entityManager, IdentifiedSong identifiedSong) {

        final IdentifiedSongDTO identifiedSongDTO = new IdentifiedSongDTO();
        identifiedSongDTO.setHashId(identifiedSong.getHashId());
        identifiedSongDTO.setTitle(identifiedSong.getTitle());
        identifiedSongDTO.setArtists(identifiedSong.getArtists());
        identifiedSongDTO.setSiadaTitle(identifiedSong.getSiadaTitle());
        identifiedSongDTO.setSiadaArtists(identifiedSong.getSiadaArtists());
        identifiedSongDTO.setRoles(identifiedSong.getRoles());
        identifiedSongDTO.setSiaeWorkCode(identifiedSong.getSiaeWorkCode());
        identifiedSongDTO.setIdentificationType(identifiedSong.getIdentificationType());

        final List<LoadIdentifiedSongDsr> dsrRefs = entityManager
                .createQuery("select x from LoadIdentifiedSongDsr x where x.hashId = :hashId",
                        LoadIdentifiedSongDsr.class)
                .setParameter("hashId", identifiedSong.getHashId())
                .getResultList();
        if (null != dsrRefs && !dsrRefs.isEmpty()) {
            final List<IdentifiedSongDsrDTO> identifiedSongDsrDTOs = new ArrayList<>(dsrRefs.size());
            for (LoadIdentifiedSongDsr dsrRef : dsrRefs) {
                IdentifiedSongDsrDTO identifiedSongDsrDTO = new IdentifiedSongDsrDTO();
                identifiedSongDsrDTO.setIdUtil(dsrRef.getIdUtil());
                identifiedSongDsrDTO.setIdDsr(dsrRef.getIdDsr());
                identifiedSongDsrDTO.setProprietaryId(dsrRef.getProprietaryId());
                identifiedSongDsrDTO.setAlbumTitle(dsrRef.getAlbumTitle());
                identifiedSongDsrDTO.setIsrc(dsrRef.getIsrc());
                identifiedSongDsrDTO.setIswc(dsrRef.getIswc());
                identifiedSongDsrDTO.setDsp(dsrRef.getDsp());
                identifiedSongDsrDTOs.add(identifiedSongDsrDTO);
            }
            identifiedSongDTO.setDsrs(identifiedSongDsrDTOs);
        }

        return identifiedSongDTO;
    }

    @POST
    @Path("addToBlacklist")
    public Response addToBlackList(LoadIdentifiedBlackListDTO blackListDTO){
        EntityManager mcmdb = mcmdbProvider.get();

        Query q = mcmdb.createNativeQuery("select * from load_identified_song_blacklist x" +
                " where x.title = ?" +
                " and x.artists = ?" +
                " and x.codice_uuid = ?");
        q.setParameter(1, blackListDTO.getTitle());
        q.setParameter(2, blackListDTO.getArtists());
        q.setParameter(3, blackListDTO.getUuid());

        try {
            List<LoadIdentifiedSongBlacklist> blacklistExtract = q.getResultList();
            if (blacklistExtract != null && blacklistExtract.isEmpty()) {
                LoadIdentifiedSongBlacklist lisb = new LoadIdentifiedSongBlacklist();
                lisb.setTitle(blackListDTO.getTitle());
                lisb.setArtists(blackListDTO.getArtists());
                if (blackListDTO.getSiaeWorkCode() != null) {
                    lisb.setCodiceOpera(blackListDTO.getSiaeWorkCode());
                }
                lisb.setCodiceUuid(blackListDTO.getUuid());

                mcmdb.getTransaction().begin();
                mcmdb.persist(lisb);
                mcmdb.getTransaction().commit();
            }

            return Response.ok().build();
        } catch (Exception e){
            logger.error("Error while saving blacklist", e);
            if (mcmdb.getTransaction().isActive())
                mcmdb.getTransaction().rollback();
        }
        return Response.status(500).build();
    }

    private SelectQuery<?> getRecords() {
        DSLContext dslContext = jooq;
        SelectQuery<?> select = dslContext.select(field("hash_id").as("hashId"),
                field("title").as("title"),
                field("artists").as("artists"),
                field("roles").as("roles"),
                field("sales_count").as("salesCount"),
                field("insert_time").as("insertTime"),
                field("last_auto_time").as("lastAutoTime"),
                field("last_manual_time").as("lastManualTime"),
                field("siada_title").as("siadaTitle"),
                field("siada_artists").as("siadaArtists"),
                field("uuid").as("uuid"),
                field("username").as("username"),
                field(
                        select(field("identification_type"))
                        .from(table("identified_song"))
                        .where(field("load_identified_song.hash_id").eq(field("identified_song.hash_id")))
                ).as("identificationType"))
                .from(table("load_identified_song"))
                .orderBy(field("sales_count").desc())
                .getQuery();

        //select.addConditions(field("last_manual_time").lessThan(Long.MAX_VALUE));
        //select.addConditions(field("last_auto_time").lessThan(Long.MAX_VALUE));
        return select;
    }
}
