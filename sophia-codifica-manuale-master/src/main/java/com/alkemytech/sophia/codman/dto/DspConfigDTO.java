package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;

import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;

@Produces("application/json")
@XmlRootElement 
public class DspConfigDTO implements Serializable {
	
	private static final long serialVersionUID = -5577154905953439204L;

	private Integer idDspConfig;
	private String idDsp;
	private String dspName;
	private String idUtilization;
	private String utilizationName;
	private String idOffer;
	private String offerName;
	private String frequency;
	private Integer receiving;
	private Integer sending;

	public DspConfigDTO() {
	}

	public DspConfigDTO(Object[] obj) {

		if (null != obj && obj.length == 10) {
			try {
				idDspConfig = (Integer) obj[0];
				idDsp = (String) obj[1];
				dspName = (String) obj[2];
				idUtilization = (String) obj[3];
				utilizationName = (String) obj[4];
				idOffer = (String) obj[5];
				offerName = (String) obj[6];
				frequency = (String) obj[7];
				receiving = (Integer) obj[8];
				sending = (Integer) obj[9];

			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	public Integer getIdDspConfig() {
		return idDspConfig;
	}

	public void setIdDspConfig(Integer idDspConfig) {
		this.idDspConfig = idDspConfig;
	}

	public String getIdDsp() {
		return idDsp;
	}

	public void setIdDsp(String idDsp) {
		this.idDsp = idDsp;
	}

	public String getDspName() {
		return dspName;
	}

	public void setDspName(String dspName) {
		this.dspName = dspName;
	}

	public String getIdUtilization() {
		return idUtilization;
	}

	public void setIdUtilization(String idUtilization) {
		this.idUtilization = idUtilization;
	}

	public String getUtilizationName() {
		return utilizationName;
	}

	public void setUtilizationName(String utilizationName) {
		this.utilizationName = utilizationName;
	}

	public String getIdOffer() {
		return idOffer;
	}

	public void setIdOffer(String idOffer) {
		this.idOffer = idOffer;
	}

	public String getOfferName() {
		return offerName;
	}

	public void setOfferName(String offerName) {
		this.offerName = offerName;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public Integer getReceiving() {
		return receiving;
	}

	public void setReceiving(Integer receiving) {
		this.receiving = receiving;
	}

	public Integer getSending() {
		return sending;
	}

	public void setSending(Integer sending) {
		this.sending = sending;
	}



}
