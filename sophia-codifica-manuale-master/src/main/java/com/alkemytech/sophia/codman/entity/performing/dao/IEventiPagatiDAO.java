package com.alkemytech.sophia.codman.entity.performing.dao;

import com.alkemytech.sophia.codman.dto.NdmVoceFatturaDateLimitsDTO;

import java.util.List;

public interface IEventiPagatiDAO {
	List<String> getAgenzie(String agenzie);

    List<String> getVociIncasso(String search);

    NdmVoceFatturaDateLimitsDTO getDateLimits();

    List<String> getSedi(String search);
}
