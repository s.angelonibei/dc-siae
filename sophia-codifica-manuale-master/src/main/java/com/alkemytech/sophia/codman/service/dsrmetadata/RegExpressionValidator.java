package com.alkemytech.sophia.codman.service.dsrmetadata;

import com.alkemytech.sophia.codman.service.dsrmetadata.period.DSRPeriodExtractorYYYYMM;
import com.alkemytech.sophia.codman.service.dsrmetadata.period.DSRPeriodExtractorYYYYMMDD_YYYYMMDD;
import com.alkemytech.sophia.codman.service.dsrmetadata.period.DSRPeriodExtractorYYYYQX;
import com.alkemytech.sophia.codman.service.dsrmetadata.period.DSRPeriodExtractorYYYYTX;
import com.alkemytech.sophia.codman.service.dsrmetadata.period.DSRPeriodExtractorYYYY_QX;
import com.alkemytech.sophia.codman.service.dsrmetadata.period.DSRPeriodExtractorYYYY_minus_QX;
import com.alkemytech.sophia.codman.service.dsrmetadata.period.DsrPeriod;
import com.alkemytech.sophia.codman.service.dsrmetadata.period.DsrPeriodExtractor;
import com.alkemytech.sophia.codman.service.dsrmetadata.period.DsrPeriodExtractorXth_Qtr_YYYY;

public class RegExpressionValidator {
	
	public static void main(String[] args) {
		final String fileName = 
//				"spotify_siae_PL_201509_premium_dsr";
//				"spotify_siae_PL_20151001_20151231_premium_dsr";
//				"spotify_siae_PL_2015Q4_premium_dsr";
//				"spotify_siae_PL_2015T4_premium_dsr";
//				"spotify_siae_PL_2015_Q4_premium_dsr";
//				"spotify_siae_PL_2015-Q4_premium_dsr";
				"spotify_siae_PL_4th_Qtr_2016_premium_dsr";
		//String fileName ="spotify_siae_[A-Z]{2}_[0-9]{6}_premium_dsr";
		String SPOTIFY_PATTERN ="spotify_siae_[A-Z]{2}_[0-9]{6}_premium_dsr";
		String SPOTIFY_PATTERN_2 ="spotify_siae_[A-Z]{2}_[0-9]{8}_[0-9]{8}_premium_dsr";
		String SPOTIFY_PATTERN_3 ="spotify_siae_[A-Z]{2}_[0-9]{4}Q[1-4]{1}_premium_dsr";
		String SPOTIFY_PATTERN_4 ="spotify_siae_[A-Z]{2}_[0-9]{4}T[1-4]{1}_premium_dsr";
		String SPOTIFY_PATTERN_5 ="spotify_siae_[A-Z]{2}_[0-9]{4}_Q[1-4]{1}_premium_dsr";
		String SPOTIFY_PATTERN_6 ="spotify_siae_[A-Z]{2}_[0-9]{4}-Q[1-4]{1}_premium_dsr";
		String SPOTIFY_PATTERN_7 ="spotify_siae_[A-Z]{2}_[1-4]{1}(st|nd|rd|th)_Qtr_[0-9]{4}_premium_dsr";
		
		if(fileName.matches(SPOTIFY_PATTERN)){
			System.out.println("MATCH - SPOTIFY");
			
			String periodString = fileName.substring(16,22);
			
			DsrPeriodExtractor extractor = new DSRPeriodExtractorYYYYMM();
			
			DsrPeriod dsrPeriod = extractor.extractDsrPeriod(periodString);
			System.out.println(dsrPeriod.toString());
			
			
		}else if(fileName.matches(SPOTIFY_PATTERN_2)){
			System.out.println("MATCH - SPOTIFY_2");
			
			String periodString = fileName.substring(16,33);
			
			DsrPeriodExtractor extractor = new DSRPeriodExtractorYYYYMMDD_YYYYMMDD();
			
			DsrPeriod dsrPeriod = extractor.extractDsrPeriod(periodString);
			System.out.println(dsrPeriod.toString());
			
			
		}else if(fileName.matches(SPOTIFY_PATTERN_3)){
			System.out.println("MATCH - SPOTIFY_3");
			
			String periodString = fileName.substring(16,22);
			
			DsrPeriodExtractor extractor = new DSRPeriodExtractorYYYYQX();
			
			DsrPeriod dsrPeriod = extractor.extractDsrPeriod(periodString);
			System.out.println(dsrPeriod.toString());
			
			
		}else if(fileName.matches(SPOTIFY_PATTERN_4)){
			System.out.println("MATCH - SPOTIFY_4");
			
			String periodString = fileName.substring(16,22);
			
			DsrPeriodExtractor extractor = new DSRPeriodExtractorYYYYTX();
			
			DsrPeriod dsrPeriod = extractor.extractDsrPeriod(periodString);
			System.out.println(dsrPeriod.toString());
			
			
		}else if(fileName.matches(SPOTIFY_PATTERN_5)){
			System.out.println("MATCH - SPOTIFY_5");
			
			String periodString = fileName.substring(16,23);
			
			DsrPeriodExtractor extractor = new DSRPeriodExtractorYYYY_QX();
			
			DsrPeriod dsrPeriod = extractor.extractDsrPeriod(periodString);
			System.out.println(dsrPeriod.toString());
			
			
		}else if(fileName.matches(SPOTIFY_PATTERN_6)){
			System.out.println("MATCH - SPOTIFY_6");
			
			String periodString = fileName.substring(16,23);
			
			DsrPeriodExtractor extractor = new DSRPeriodExtractorYYYY_minus_QX();
			
			DsrPeriod dsrPeriod = extractor.extractDsrPeriod(periodString);
			System.out.println(dsrPeriod.toString());
			
			
		}else if(fileName.matches(SPOTIFY_PATTERN_7)){
			System.out.println("MATCH - SPOTIFY_7");
			
			String periodString = fileName.substring(16,28);
			
			DsrPeriodExtractor extractor = new DsrPeriodExtractorXth_Qtr_YYYY();
			
			DsrPeriod dsrPeriod = extractor.extractDsrPeriod(periodString);
			System.out.println(dsrPeriod.toString());
			
			
		}
		
		
		else{
			System.out.println("NO MATCH");
		}
		

	}

}
