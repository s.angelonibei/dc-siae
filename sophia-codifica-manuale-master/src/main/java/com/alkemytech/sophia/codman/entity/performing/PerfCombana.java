package com.alkemytech.sophia.codman.entity.performing;

import com.alkemytech.sophia.codman.entity.FontePrimaUtilizzazione;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PERF_COMBANA database table.
 * 
 */
@Entity
@Table(name="PERF_COMBANA")
@NamedQuery(name="PerfCombana.findAll", query="SELECT p FROM PerfCombana p")
public class PerfCombana implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID_COMBANA", unique=true, nullable=false)
	private Long idCombana;

	@Column(name="AUTORI", length=500)
	private String autori;

	@Column(name="CODICE_COMBANA", length=45)
	private String codiceCombana;

	@Column(name="COMPOSITORI", length=500)
	private String compositori;

	@Column(name="INTERPRETI", length=500)
	private String interpreti;

	@Column(name="LAVORABILE")
	private Boolean lavorabile;

	@Column(name="TITOLO", length=500)
	private String titolo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DATA_CREAZIONE")
	private Date dataCreazione;

	@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_FONTE_PRIMA_UTILIZZAZIONE")
	private FontePrimaUtilizzazione fontePrimaUtilizzazione;

	//bi-directional many-to-one association to PerfCodificaDTO
	@JsonManagedReference
	@OneToMany(mappedBy="perfCombana")
	private List<PerfCodifica> perfCodificas;

	//bi-directional many-to-one association to PerfUtilizzazione
	@JsonManagedReference
	@OneToMany(mappedBy="perfCombana" )
	private List<PerfUtilizzazione> perfUtilizzaziones;

	public PerfCombana() {
	}

	public Long getIdCombana() {
		return idCombana;
	}

	public void setIdCombana(Long idCombana) {
		this.idCombana = idCombana;
	}

	public Boolean getLavorabile() {
		return lavorabile;
	}

	public void setLavorabile(Boolean lavorabile) {
		this.lavorabile = lavorabile;
	}

	public String getAutori() {
		return this.autori;
	}

	public void setAutori(String autori) {
		this.autori = autori;
	}

	public String getCodiceCombana() {
		return this.codiceCombana;
	}

	public void setCodiceCombana(String codiceCombana) {
		this.codiceCombana = codiceCombana;
	}

	public String getCompositori() {
		return this.compositori;
	}

	public void setCompositori(String compositori) {
		this.compositori = compositori;
	}

	public String getInterpreti() {
		return this.interpreti;
	}

	public void setInterpreti(String interpreti) {
		this.interpreti = interpreti;
	}

	public String getTitolo() {
		return this.titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	public List<PerfCodifica> getPerfCodificas() {
		return this.perfCodificas;
	}

	public void setPerfCodificas(List<PerfCodifica> perfCodificas) {
		this.perfCodificas = perfCodificas;
	}

	public PerfCodifica addPerfCodifica(PerfCodifica perfCodifica) {
		getPerfCodificas().add(perfCodifica);
		perfCodifica.setPerfCombana(this);

		return perfCodifica;
	}

	public PerfCodifica removePerfCodifica(PerfCodifica perfCodifica) {
		getPerfCodificas().remove(perfCodifica);
		perfCodifica.setPerfCombana(null);

		return perfCodifica;
	}

	public List<PerfUtilizzazione> getPerfUtilizzaziones() {
		return this.perfUtilizzaziones;
	}

	public void setPerfUtilizzaziones(List<PerfUtilizzazione> perfUtilizzaziones) {
		this.perfUtilizzaziones = perfUtilizzaziones;
	}

	public PerfUtilizzazione addPerfUtilizzazione(PerfUtilizzazione perfUtilizzazione) {
		getPerfUtilizzaziones().add(perfUtilizzazione);
		perfUtilizzazione.setPerfCombana(this);

		return perfUtilizzazione;
	}

	public PerfUtilizzazione removePerfUtilizzazione(PerfUtilizzazione perfUtilizzazione) {
		getPerfUtilizzaziones().remove(perfUtilizzazione);
		perfUtilizzazione.setPerfCombana(null);

		return perfUtilizzazione;
	}

	public FontePrimaUtilizzazione getFontePrimaUtilizzazione() {
		return fontePrimaUtilizzazione;
	}

	public void setFontePrimaUtilizzazione(FontePrimaUtilizzazione fontePrimaUtilizzazioneByIdFontePrimaUtilizzazione) {
		this.fontePrimaUtilizzazione = fontePrimaUtilizzazioneByIdFontePrimaUtilizzazione;
	}

	public Date getDataCreazione() {
		return dataCreazione;
	}

	public void setDataCreazione(Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}

}