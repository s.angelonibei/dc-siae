package com.alkemytech.sophia.codman.entity.performing.dao;

import java.util.List;

import com.alkemytech.sophia.codman.dto.performing.ConfigurazioniPeriodoDTO;
import com.alkemytech.sophia.codman.dto.performing.MonitoringKPIDTO;
import com.alkemytech.sophia.codman.entity.performing.InformazioneKPICodifica;

public interface IMonitoringKPIDAO {
	List<InformazioneKPICodifica> getRecordKPI(Long periodoRipartizione, String voceIncasso, String tipologiaReport);
}
