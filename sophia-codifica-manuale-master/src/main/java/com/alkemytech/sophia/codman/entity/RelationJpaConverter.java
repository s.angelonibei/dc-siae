package com.alkemytech.sophia.codman.entity;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class RelationJpaConverter implements AttributeConverter<Relation, String> {

	@Override
	public String convertToDatabaseColumn(Relation attribute) {
		if(attribute==null) return null;
		return attribute.toString();
	}

	@Override
	public Relation convertToEntityAttribute(String dbData) {
		if(dbData==null) return null;
		return Relation.valueOf(dbData);
	}

}