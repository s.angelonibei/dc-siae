package com.alkemytech.sophia.codman.entity.performing;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PERF_VALORIZZAZIONE_RUN database table.
 * 
 */
@Entity
@Table(name="PERF_VALORIZZAZIONE_RUN")
@NamedQuery(name="PerfValorizzazioneRun.findAll", query="SELECT p FROM PerfValorizzazioneRun p")
public class PerfValorizzazioneRun implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID_VALORIZZAZIONE_RUN", unique=true, nullable=false)
	private Long idValorizzazioneRun;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_FINE")
	private Date dataFine;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_INIZIO", nullable=false)
	private Date dataInizio;

	@Column(name="INFO_RUN")
	private String infoRun;

	//bi-directional many-to-one association to PerfMonitoraggioCodificato
	@JsonManagedReference
	@OneToMany(mappedBy="perfValorizzazioneRun")
	private List<PerfMonitoraggioCodificato> perfMonitoraggioCodificatos;

	//bi-directional many-to-one association to PeriodoRipartizione
	@JsonBackReference
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PERIODO_RIPARTIZIONE")
	private PerfPeriodoRipartizione perfPeriodoRipartizione;

	public PerfValorizzazioneRun() {
	}

	public PerfValorizzazioneRun(Date dataFine, Date dataInizio, String infoRun, List<PerfMonitoraggioCodificato> perfMonitoraggioCodificatos, PerfPeriodoRipartizione perfPeriodoRipartizione) {
		this.dataFine = dataFine;
		this.dataInizio = dataInizio;
		this.infoRun = infoRun;
		this.perfMonitoraggioCodificatos = perfMonitoraggioCodificatos;
		this.perfPeriodoRipartizione = perfPeriodoRipartizione;
	}

	public Long getIdValorizzazioneRun() {
		return idValorizzazioneRun;
	}

	public void setIdValorizzazioneRun(Long idValorizzazioneRun) {
		this.idValorizzazioneRun = idValorizzazioneRun;
	}

	public Date getDataFine() {
		return dataFine;
	}

	public void setDataFine(Date dataFine) {
		this.dataFine = dataFine;
	}

	public Date getDataInizio() {
		return dataInizio;
	}

	public void setDataInizio(Date dataInizio) {
		this.dataInizio = dataInizio;
	}

	public String getInfoRun() {
		return infoRun;
	}

	public void setInfoRun(String infoRun) {
		this.infoRun = infoRun;
	}

	public List<PerfMonitoraggioCodificato> getPerfMonitoraggioCodificatos() {
		return perfMonitoraggioCodificatos;
	}

	public void setPerfMonitoraggioCodificatos(List<PerfMonitoraggioCodificato> perfMonitoraggioCodificatos) {
		this.perfMonitoraggioCodificatos = perfMonitoraggioCodificatos;
	}

	public PerfPeriodoRipartizione getPerfPeriodoRipartizione() {
		return perfPeriodoRipartizione;
	}

	public void setPerfPeriodoRipartizione(PerfPeriodoRipartizione periodoRipartizione) {
		this.perfPeriodoRipartizione = periodoRipartizione;
	}
}