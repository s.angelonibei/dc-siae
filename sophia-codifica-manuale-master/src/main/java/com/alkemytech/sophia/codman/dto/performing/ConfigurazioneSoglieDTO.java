package com.alkemytech.sophia.codman.dto.performing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ConfigurazioneSoglieDTO {

	private Long id;
	private Long codiceConfigurazione;
	private String inizioPeriodoCompetenza;
	private String finePeriodoCompetenza;
	private String utenteCaricamentoFile;
	private String dataCaricamentoFile;
	private String utenteUltimaModifica;
	private String dataUltimaModifica;
	private String nomeFile;
	private boolean flagAttivo;
	
	public ConfigurazioneSoglieDTO() {
		super();
	}

	public ConfigurazioneSoglieDTO(Long codiceConfigurazione, String inizioPeriodoCompetenza, String finePeriodoCompetenza,
			String utenteCaricamentoFile) {
		super();
		this.codiceConfigurazione = codiceConfigurazione;
		this.inizioPeriodoCompetenza = inizioPeriodoCompetenza;
		this.finePeriodoCompetenza = finePeriodoCompetenza;
		this.utenteCaricamentoFile = utenteCaricamentoFile;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCodiceConfigurazione() {
		return codiceConfigurazione;
	}

	public void setCodiceConfigurazione(Long codiceConfigurazione) {
		this.codiceConfigurazione = codiceConfigurazione;
	}

	public String getInizioPeriodoCompetenza() {
		return inizioPeriodoCompetenza;
	}

	public void setInizioPeriodoCompetenza(String inizioPeriodoCompetenza) {
		this.inizioPeriodoCompetenza = inizioPeriodoCompetenza;
	}

	public String getFinePeriodoCompetenza() {
		return finePeriodoCompetenza;
	}

	public void setFinePeriodoCompetenza(String finePeriodoCompetenza) {
		this.finePeriodoCompetenza = finePeriodoCompetenza;
	}

	public String getUtenteCaricamentoFile() {
		return utenteCaricamentoFile;
	}

	public void setUtenteCaricamentoFile(String utenteCaricamentoFile) {
		this.utenteCaricamentoFile = utenteCaricamentoFile;
	}

	public String getDataCaricamentoFile() {
		return dataCaricamentoFile;
	}

	public void setDataCaricamentoFile(String dataCaricamentoFile) {
		this.dataCaricamentoFile = dataCaricamentoFile;
	}

	public String getUtenteUltimaModifica() {
		return utenteUltimaModifica;
	}

	public void setUtenteUltimaModifica(String utenteUltimaModifica) {
		this.utenteUltimaModifica = utenteUltimaModifica;
	}

	public String getDataUltimaModifica() {
		return dataUltimaModifica;
	}

	public void setDataUltimaModifica(String dataUltimaModifica) {
		this.dataUltimaModifica = dataUltimaModifica;
	}

	public String getNomeFile() {
		return nomeFile;
	}

	public void setNomeFile(String nomeFile) {
		this.nomeFile = nomeFile;
	}

	public boolean isFlagAttivo() {
		return flagAttivo;
	}

	public void setFlagAttivo(boolean flagAttivo) {
		this.flagAttivo = flagAttivo;
	}

	@Override
	public String toString() {
		return "ConfigurazioneSoglieDTO [codiceConfigurazione=" + codiceConfigurazione + ", inizioPeriodoCompetenza="
				+ inizioPeriodoCompetenza + ", finePeriodoCompetenza=" + finePeriodoCompetenza
				+ ", utenteCaricamentoFile=" + utenteCaricamentoFile + ", dataCaricamentoFile=" + dataCaricamentoFile
				+ ", utenteUltimaModifica=" + utenteUltimaModifica + ", dataUltimaModifica=" + dataUltimaModifica
				+ ", nomeFile=" + nomeFile + ", flagAttivo=" + flagAttivo + "]";
	}
	
	
	
}
