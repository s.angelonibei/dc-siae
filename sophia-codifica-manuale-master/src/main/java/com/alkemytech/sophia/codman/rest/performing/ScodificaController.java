package com.alkemytech.sophia.codman.rest.performing;

import com.alkemytech.sophia.codman.dto.PaginationDTO;
import com.alkemytech.sophia.codman.dto.ResponseDTO;
import com.alkemytech.sophia.codman.performing.dto.DettaglioCombanaDTO;
import com.alkemytech.sophia.codman.performing.dto.ScodificaRequestDTO;
import com.alkemytech.sophia.codman.performing.dto.ScodificaResponseDTO;
import com.alkemytech.sophia.codman.performing.dto.SessioneCodificaDTO;
import com.alkemytech.sophia.codman.rest.performing.service.IScodificaService;
import com.alkemytech.sophia.codman.utils.JavaBeanToCsv;
import com.alkemytech.sophia.codman.utils.PATCH;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.apache.commons.collections.CollectionUtils;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.ParseException;
import java.util.GregorianCalendar;

@Singleton
@Path("performing/scodifica")
public class ScodificaController {
    private IScodificaService scodificaService;
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create();


    @Inject
    public ScodificaController(IScodificaService scodificaService) {
        this.scodificaService = scodificaService;
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON, "text/csv"})
    public Response getListaScodifica(@QueryParam("idCombana") Long idCombana,
                                      @QueryParam("titolo") String titolo,
                                      @QueryParam("nominativo") String nominativo,
                                      @QueryParam("codiceOperaApprovato") String codiceOperaApprovato,
                                      @QueryParam("ripartizioniRiferimentoDa") String ripartizioniRiferimentoDa,
                                      @QueryParam("ripartizioniRiferimentoA") String ripartizioniRiferimentoA,
                                      @QueryParam("user") String user,
                                      @QueryParam("numeroProgrammaMusicale") Long numeroProgrammaMusicale,
                                      @DefaultValue("1") @QueryParam("pageNumber") int pageNumber,
                                      @DefaultValue("10") @QueryParam("pageSize") int pageSize,
                                      @QueryParam("export") boolean export) throws ParseException {
        if (!export) {
            PaginationDTO paginationDTO = new PaginationDTO(pageNumber, pageSize);
            ScodificaRequestDTO scodificaRequestDTO = new ScodificaRequestDTO(idCombana, titolo, nominativo, codiceOperaApprovato, ripartizioniRiferimentoDa, ripartizioniRiferimentoA, paginationDTO, user, numeroProgrammaMusicale);

            ResponseDTO responseDTO = scodificaService.getListaScodifica(scodificaRequestDTO);

            if (CollectionUtils.isEmpty(responseDTO.getList())) {
                return Response.noContent().build();
            }

            return Response.ok(gson.toJson(responseDTO)).build();
        } else {
            ScodificaRequestDTO scodificaRequestDTO = new ScodificaRequestDTO(idCombana, titolo, nominativo, codiceOperaApprovato, ripartizioniRiferimentoDa, ripartizioniRiferimentoA, null, user, numeroProgrammaMusicale);

            ResponseDTO responseDTO = scodificaService.getListaScodificaForExport(scodificaRequestDTO);

            if (CollectionUtils.isEmpty(responseDTO.getList())) {
                return Response.noContent().build();
            }

//            CustomMappingStrategy<ScodificaResponseDTO> customMappingStrategy = new CustomMappingStrategy<>();
//            customMappingStrategy.setColumnMapping(ScodificaResponseDTO.getMappingStrategy());
//            customMappingStrategy.setType(ScodificaResponseDTO.class);

            return Response.ok(JavaBeanToCsv.getStreamingOutput(responseDTO.getList(), null, ScodificaResponseDTO.class))
                    .type("text/csv")
                    .header("Content-Disposition", "attachment; filename=\"lista_scodifica_" + GregorianCalendar.getInstance().getTimeInMillis() + ".csv\"")
                    .build();
        }
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON, "text/csv"})
    @Path("combane/{idCombana:(([0-9]*)?)}/utilizzazioni")
    public Response getListaUtilizzazioni(@PathParam("idCombana") Long idCombana) {
        ResponseDTO responseDTO = scodificaService.getListaUtilizzazioniForExport(idCombana);

        if (CollectionUtils.isEmpty(responseDTO.getList())) {
            return Response.noContent().build();
        }
        return Response.ok(JavaBeanToCsv.getStreamingOutput(responseDTO.getList(), null, DettaglioCombanaDTO.class))
                .type("text/csv")
                .header("Content-Disposition", "attachment; filename=\"lista_utilizzazioni_combana_n_" + idCombana +"_"+ GregorianCalendar.getInstance().getTimeInMillis() + ".csv\"")
                .build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("dettaglio/{idCombana:(([0-9]*)?)}")
    public Response getDettaglioScodifica(@PathParam("idCombana") Long idCombana) {
        ResponseDTO responseDTO = scodificaService.getDettaglioScodifica(idCombana);
        if (CollectionUtils.isEmpty(responseDTO.getList())) {
            return Response.noContent().build();
        }
        return Response.ok(gson.toJson(responseDTO)).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("combane{p:/?}{id:(([0-9]*)?)}")
    public Response getDettaglioCombana(@PathParam("id") Long id) {
        return Response.ok(gson.toJson(scodificaService.getDettaglioCombana(id))).build();
    }

    @PATCH
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{p:/?}{id:(([0-9]*)?)}")
    public Response deleteCodiceOpera(@PathParam("id") Long idCodifica,
                                      @QueryParam("idCombana") Long idCombana,
                                      @QueryParam("codiceOpera") String codiceOpera) {
        scodificaService.deleteCodiceOpera(idCodifica, idCombana, codiceOpera);
        return Response.noContent().build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("combane/{idCombana:(([0-9]*)?)}/sessioni-codifica")
    public Response getSessionScodifica(@QueryParam("utente")String utente,
                                        @QueryParam("idCodifica")Long idCodifica,
                                        @PathParam("idCombana") Long idCombana) {
        SessioneCodificaDTO sessioneCodificaDTO = scodificaService.getSessioneScodifica(idCombana, idCodifica, utente);
        if(sessioneCodificaDTO == null){
            return Response.status(Response.Status.CONFLICT).build();
        }
        return Response.status(Response.Status.CREATED).entity(gson.toJson(sessioneCodificaDTO)).build();
    }
}
