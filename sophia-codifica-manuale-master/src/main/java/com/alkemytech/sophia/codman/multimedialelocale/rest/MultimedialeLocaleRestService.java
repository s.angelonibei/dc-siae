package com.alkemytech.sophia.codman.multimedialelocale.rest;

import com.alkemytech.sophia.codman.entity.performing.security.ReportPage;
import com.alkemytech.sophia.codman.gestioneperiodi.GestionePeriodiInterface;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.alkemytech.sophia.codman.multimedialelocale.dto.*;
import com.alkemytech.sophia.codman.multimedialelocale.entity.MultimedialeLocale;
import com.alkemytech.sophia.codman.rest.PagedResult;
import com.alkemytech.sophia.common.beantocsv.CustomMappingStrategy;
import com.alkemytech.sophia.common.s3.S3Service;
import com.amazonaws.services.s3.AmazonS3URI;
import com.amazonaws.util.CollectionUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.apache.http.util.TextUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Singleton
@Path("multimediale")
public class MultimedialeLocaleRestService {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final Gson gson;
    private final Provider<EntityManager> provider;
    private final GestionePeriodiInterface gestionePeriodi;
    private final static String NO_RESULTS = "{\"message\":\"Nessun dato presente per la ricerca effettuata\"}";
    private final static String DATA_INVIO_ERRATA = "{\"message\":\"le date d'invio inserite non sono valide\"}";
    private final static String DATA_COMPETENZA_ERRATA = "{\"message\":\"le date di competenza inserite non sono valide\"}";
    private final S3Service s3Service;

    @Inject
    public MultimedialeLocaleRestService(@McmdbDataSource Provider<EntityManager> provider, Gson gson,
                                         GestionePeriodiInterface gestionePeriodi, S3Service s3Service) {
        super();
        this.provider = provider;
        this.gson = gson;
        this.gestionePeriodi = gestionePeriodi;
        this.s3Service = s3Service;
    }

    @POST
    @Path("locale/stati")
    @Produces("application/json")
    public Response getStati() {
        return Response.ok(gson.toJson(FieldDto.getStati())).build();
    }

    @POST
    @Path("locale/tipologieServizio")
    @Produces("application/json")
    public Response getTipologiaServizio() {
        return Response.ok(gson.toJson(FieldDto.getTipologieServizio())).build();
    }

    @POST
    @Path("locale/licenziatari")
    @Produces("application/json")
    public Response getLicenziatari() {
        EntityManager entityManager = provider.get();
        List<String> strings = entityManager
                .createNativeQuery("Select distinct NOMINATIVO_LICENZIATARIO From ML_REPORT").getResultList();

        return Response.ok(gson.toJson(strings)).build();
    }

    @POST
    @Path("locale/ripartizioni")
    @Produces("application/json")
    public Response getRipartizioni() {
        EntityManager entityManager = provider.get();
        try {
            List<String> resultList = entityManager.createNativeQuery("Select DISTINCT RIPARTIZIONE from ML_REPORT where RIPARTIZIONE is not null").getResultList();
            ArrayList<String> result = new ArrayList<>();
            for (String record : resultList) {
                result.add(record);
            }
            return Response.ok(gson.toJson(result)).build();
        } catch (Exception e) {
            return Response.status(Status.NOT_FOUND).build();
        }
    }

    @POST
    @Path("locale/report")
    @Produces("application/json")
    public Response multimedialeLocaleOnTable(MultimedialeLocaleRequestDto multimedialeLocaleRequestDto) {

        EntityManager entityManager = provider.get();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

        CriteriaQuery<MultimedialeLocale> query = criteriaBuilder.createQuery(MultimedialeLocale.class);
        Root<MultimedialeLocale> multimedialeRoot = query.from(MultimedialeLocale.class);

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        List<Predicate> predicates = new ArrayList<>();

        predicates.add(criteriaBuilder.equal(multimedialeRoot.get("attivo"), true));
        try {
            if (!TextUtils.isEmpty(multimedialeLocaleRequestDto.getDataDa())) {
                predicates
                        .add(criteriaBuilder.greaterThanOrEqualTo(multimedialeRoot.<Date>get("periodoCompetenzaDataDa"),
                                sdf.parse(multimedialeLocaleRequestDto.getDataDa())));
            }

            if (!TextUtils.isEmpty(multimedialeLocaleRequestDto.getDataA())) {
                predicates.add(criteriaBuilder.lessThanOrEqualTo(multimedialeRoot.<Date>get("periodoCompetenzaDataA"),
                        sdf.parse(multimedialeLocaleRequestDto.getDataA())));
            }

            if (!TextUtils.isEmpty(multimedialeLocaleRequestDto.getDataDa())
                    && !TextUtils.isEmpty(multimedialeLocaleRequestDto.getDataA())) {
                if (sdf.parse(multimedialeLocaleRequestDto.getDataA())
                        .before(sdf.parse(multimedialeLocaleRequestDto.getDataDa()))) {
                    return Response.ok(DATA_COMPETENZA_ERRATA).build();
                }
            }

        } catch (Exception e) {

        }

        try {
            if (!TextUtils.isEmpty(multimedialeLocaleRequestDto.getDataInvioDa())
                    && !TextUtils.isEmpty(multimedialeLocaleRequestDto.getDataInvioA())) {
                if (sdf.parse(multimedialeLocaleRequestDto.getDataInvioA())
                        .before(sdf.parse(multimedialeLocaleRequestDto.getDataInvioDa()))) {
                    return Response.ok(DATA_INVIO_ERRATA).build();
                }
            }

            if (!TextUtils.isEmpty(multimedialeLocaleRequestDto.getDataInvioDa())) {
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(multimedialeRoot.<Date>get("dataUpload"),
                        sdf.parse(multimedialeLocaleRequestDto.getDataInvioDa())));
            }

            if (!TextUtils.isEmpty(multimedialeLocaleRequestDto.getDataInvioA())) {
                predicates.add(criteriaBuilder.lessThanOrEqualTo(multimedialeRoot.<Date>get("dataUpload"),
                        sdf.parse(multimedialeLocaleRequestDto.getDataInvioA())));
            }

        } catch (Exception e) {

        }

        if (!TextUtils.isEmpty(multimedialeLocaleRequestDto.getLicenziatario())) {
            predicates.add(criteriaBuilder.like(multimedialeRoot.<String>get("nominativoLicenziatario"), "%" + multimedialeLocaleRequestDto.getLicenziatario() + "%"));
        }
        if (!TextUtils.isEmpty(multimedialeLocaleRequestDto.getStatoLogico())) {
            predicates.add(criteriaBuilder.equal(multimedialeRoot.get("statoLogico"),
                    multimedialeLocaleRequestDto.getStatoLogico()));
        }
        if (!TextUtils.isEmpty(multimedialeLocaleRequestDto.getServizio())) {
            predicates.add(criteriaBuilder.equal(multimedialeRoot.get("servizio"),
                    multimedialeLocaleRequestDto.getServizio()));
        }
        if (!TextUtils.isEmpty(multimedialeLocaleRequestDto.getPeriodoRipartizione())) {
            predicates.add(criteriaBuilder.equal(multimedialeRoot.get("periodoRipartizione"),
                    multimedialeLocaleRequestDto.getPeriodoRipartizione()));
        }

        query.where(predicates.toArray(new Predicate[]{}))
                .orderBy(criteriaBuilder.desc(multimedialeRoot.get("dataUpload")));

        int first = (multimedialeLocaleRequestDto.getFirst() > 0 ? multimedialeLocaleRequestDto.getFirst() - 1 : 0);
        int last = (multimedialeLocaleRequestDto.getLast() > 0 ? multimedialeLocaleRequestDto.getLast() : 50);

        List<MultimedialeLocale> result = (List<MultimedialeLocale>) entityManager.createQuery(query)
                .setFirstResult(first)
                .setMaxResults(ReportPage.NUMBER_RECORD_PER_PAGE)
                .getResultList();

        for (MultimedialeLocale multimedialeLocale : result) {
            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            CodificaStatistiche jsonObject = gson.fromJson(multimedialeLocale.getCodificaStatistiche(),
                    CodificaStatistiche.class);
            if (jsonObject != null) {
                jsonObject.setPercPolverizzazione(String.format("%.2f", MlUtil.getPercPolverizzazione(multimedialeLocale)) + "%");
            }
            multimedialeLocale.setCodificaStatistiche(gson.toJson(jsonObject));
        }
        if (!CollectionUtils.isNullOrEmpty(result)) {
            final PagedResult pagedResult = new PagedResult();
            if (null != result && !result.isEmpty()) {
                final int maxrows = multimedialeLocaleRequestDto.getLast() - multimedialeLocaleRequestDto.getFirst();
                final boolean hasNext = result.size() > maxrows;
                pagedResult.setRows(!hasNext ? result : result.subList(0, maxrows)).setMaxrows(maxrows)
                        .setFirst(multimedialeLocaleRequestDto.getFirst())
                        .setLast(hasNext ? multimedialeLocaleRequestDto.getLast()
                                : multimedialeLocaleRequestDto.getFirst() + result.size())
                        .setHasNext(hasNext).setHasPrev(multimedialeLocaleRequestDto.getFirst() > 0);
            } else {
                pagedResult.setMaxrows(0).setFirst(0).setLast(0).setHasNext(false).setHasPrev(false);
            }
            return Response.ok(gson.toJson(pagedResult)).build();
        } else {
            return Response.ok(NO_RESULTS).build();
        }

    }

    @POST
    @Path("locale/downloadReport")
    @Produces("application/json")
    public Response downloadReport(MultimedialeLocaleRequestDto multimedialeLocaleRequestDto) {

        EntityManager entityManager = provider.get();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

        CriteriaQuery<MultimedialeLocale> query = criteriaBuilder.createQuery(MultimedialeLocale.class);
        Root<MultimedialeLocale> multimedialeRoot = query.from(MultimedialeLocale.class);

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        List<Predicate> predicates = new ArrayList<>();

        predicates.add(criteriaBuilder.equal(multimedialeRoot.get("attivo"), true));
        try {
            if (!TextUtils.isEmpty(multimedialeLocaleRequestDto.getDataDa())
                    && !TextUtils.isEmpty(multimedialeLocaleRequestDto.getDataA())) {
                predicates
                        .add(criteriaBuilder.greaterThanOrEqualTo(multimedialeRoot.<Date>get("periodoCompetenzaDataDa"),
                                sdf.parse(multimedialeLocaleRequestDto.getDataDa())));
                predicates.add(criteriaBuilder.lessThanOrEqualTo(multimedialeRoot.<Date>get("periodoCompetenzaDataA"),
                        sdf.parse(multimedialeLocaleRequestDto.getDataA())));
            }

        } catch (Exception e) {

        }
        try {
            if (!TextUtils.isEmpty(multimedialeLocaleRequestDto.getDataInvioDa())
                    && !TextUtils.isEmpty(multimedialeLocaleRequestDto.getDataInvioA())) {
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(multimedialeRoot.<Date>get("dataUpload"),
                        sdf.parse(multimedialeLocaleRequestDto.getDataInvioDa())));
                predicates.add(criteriaBuilder.lessThanOrEqualTo(multimedialeRoot.<Date>get("dataUpload"),
                        sdf.parse(multimedialeLocaleRequestDto.getDataInvioA())));

            }
        } catch (Exception e) {

        }
        if (!TextUtils.isEmpty(multimedialeLocaleRequestDto.getLicenziatario())) {
            predicates.add(criteriaBuilder.like(multimedialeRoot.<String>get("nominativoLicenziatario"), "%" + multimedialeLocaleRequestDto.getLicenziatario() + "%"));
        }
        if (!TextUtils.isEmpty(multimedialeLocaleRequestDto.getStatoLogico())) {
            predicates.add(criteriaBuilder.equal(multimedialeRoot.get("statoLogico"),
                    multimedialeLocaleRequestDto.getStatoLogico()));
        }
        if (!TextUtils.isEmpty(multimedialeLocaleRequestDto.getServizio())) {
            predicates.add(criteriaBuilder.equal(multimedialeRoot.get("servizio"),
                    multimedialeLocaleRequestDto.getServizio()));
        }
        if (!TextUtils.isEmpty(multimedialeLocaleRequestDto.getPeriodoRipartizione())) {
            predicates.add(criteriaBuilder.equal(multimedialeRoot.get("periodoRipartizione"),
                    multimedialeLocaleRequestDto.getPeriodoRipartizione()));
        }

        query.where(predicates.toArray(new Predicate[]{}))
                .orderBy(criteriaBuilder.desc(multimedialeRoot.get("dataUpload")));

        List<MultimedialeLocale> result = (List<MultimedialeLocale>) entityManager.createQuery(query).getResultList();
        if (!CollectionUtils.isNullOrEmpty(result)) {
            ArrayList<MultimedialeLocaleCsvDto> csvRecords = new ArrayList<>();
            for (MultimedialeLocale ml : result) {
                csvRecords.add(new MultimedialeLocaleCsvDto(ml));
            }
            try {
                if (csvRecords.size() != 0) {
                    StreamingOutput so = null;

                    CustomMappingStrategy<MultimedialeLocaleCsvDto> customMappingStrategy = new CustomMappingStrategy<>(
                            new MultimedialeLocaleCsvDto().getMappingStrategy());
                    customMappingStrategy.setType(MultimedialeLocaleCsvDto.class);
                    so = getStreamingOutput(csvRecords, customMappingStrategy);
                    Response response = Response.ok(so, "text/csv")
                            .header("Content-Disposition", "attachment; filename=\"" + "Report Multimediale.csv\"")
                            .build();
                    return response;
                } else {
                    return Response.status(500).entity("Non risulta nessun dato per il periodo selezionato").build();
                }
            } catch (Exception e) {
                return Response.status(500).entity("Non risulta nessun dato per il periodo selezionato").build();
            } finally {

            }
        } else {
            return Response.ok(NO_RESULTS).build();
        }

    }

    private <MultimedialeLocaleCsvDto> StreamingOutput getStreamingOutput(final List<MultimedialeLocaleCsvDto> obj,
                                                                          final CustomMappingStrategy mappingStrategy) {
        StreamingOutput so = new StreamingOutput() {
            @Override
            public void write(OutputStream outputStream) throws IOException, WebApplicationException {
                OutputStreamWriter osw = new OutputStreamWriter(outputStream);

                StatefulBeanToCsv<MultimedialeLocaleCsvDto> beanToCsv = new StatefulBeanToCsvBuilder<MultimedialeLocaleCsvDto>(
                        osw).withMappingStrategy(mappingStrategy).withSeparator(';').build();
                try {
                    beanToCsv.write(obj);
                    osw.flush();
                    outputStream.flush();
                } catch (CsvDataTypeMismatchException e) {
                } catch (CsvRequiredFieldEmptyException e) {
                } finally {
                    if (osw != null)
                        osw.close();
                }
            }
        };
        return so;
    }

    @POST
    @Path("locale/storico")
    @Produces("application/json")
    public Response getStorico(MultimedialeStoricoRequestDto multimedialeLocaleRequestDto) {

        EntityManager entityManager = provider.get();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

        CriteriaQuery<MultimedialeLocale> query = criteriaBuilder.createQuery(MultimedialeLocale.class);
        Root<MultimedialeLocale> multimedialeRoot = query.from(MultimedialeLocale.class);
        List<Predicate> predicates = new ArrayList<>();

        try {
            predicates.add(criteriaBuilder.equal(multimedialeRoot.get("attivo"), false));

            predicates.add(criteriaBuilder.equal(multimedialeRoot.get("origPeriodoCompetenza"),
                    multimedialeLocaleRequestDto.getPeriodoCompetenza()));
            predicates.add(
                    criteriaBuilder.equal(multimedialeRoot.get("licenza"), multimedialeLocaleRequestDto.getLicenza()));
            predicates.add(criteriaBuilder.equal(multimedialeRoot.get("servizio"),
                    multimedialeLocaleRequestDto.getServizio()));
            query.where(predicates.toArray(new Predicate[]{}))
                    .orderBy(criteriaBuilder.desc(multimedialeRoot.get("dataUpload")));

            List<MultimedialeLocale> result = (List<MultimedialeLocale>) entityManager.createQuery(query)
                    .getResultList();
            if (!CollectionUtils.isNullOrEmpty(result)) {
                return Response.ok(gson.toJson(result)).build();
            } else {
                return Response.ok(NO_RESULTS).build();
            }
        } catch (Exception e) {
            return Response.ok(NO_RESULTS).build();
        }

    }

    @GET
    @Path("locale/downloadReportFile")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response downloadById(@QueryParam(value = "id") Integer id, @QueryParam(value = "tipo") MultilocaleFileType tipo) {

        EntityManager entityManager = provider.get();
        if (null == tipo) {
            return Response.status(Status.BAD_REQUEST).build();
        }
        if (null == id) {
            return Response.status(Status.BAD_REQUEST).build();
        }
        try {
            MultimedialeLocale locale = (MultimedialeLocale) entityManager
                    .createQuery("Select x From MultimedialeLocale x where x.idMlReport=:idMlReport")
                    .setParameter("idMlReport", id).getSingleResult();

            String posizioneReport = null;
            if (tipo.equals(MultilocaleFileType.CODIFICATO)) {
                posizioneReport = locale.getPosizioneReportCodificato();
            } else if (tipo.equals(MultilocaleFileType.CARICATO)) {
                posizioneReport = locale.getPosizioneReport();
            } else if (tipo.equals(MultilocaleFileType.NORMALIZZATO)) {
                posizioneReport = locale.getPosizioneReportNormalizzato();
            } else {
                return Response.status(Status.NOT_FOUND).build();
            }
            if (TextUtils.isEmpty(posizioneReport)) {
                return Response.status(Status.NOT_FOUND).build();
            }
            final AmazonS3URI s3Uri = new AmazonS3URI(posizioneReport);

            StreamingOutput stream = new StreamingOutput() {
                @Override
                public void write(OutputStream outputStream) throws IOException, WebApplicationException {
                    InputStream is = s3Service.download(s3Uri.getBucket(), s3Uri.getKey());
                    int nextByte = 0;
                    while ((nextByte = is.read()) != -1) {
                        outputStream.write(nextByte);
                    }
                    outputStream.flush();
                    outputStream.close();
                    is.close();
                }
            };
            int index = posizioneReport.lastIndexOf("/");
            String fileName = posizioneReport.substring(index + 1);
            if (tipo.equals(MultilocaleFileType.CODIFICATO)) {
                fileName = "Codificato-" + fileName;
            }
            return Response.ok(stream)
                    .type(MediaType.APPLICATION_OCTET_STREAM)
                    .header("Content-Disposition", "attachment; filename=" + fileName).
                            build();
        } catch (Exception e) {
            return Response.status(Status.NOT_FOUND).build();
        }
    }
}
