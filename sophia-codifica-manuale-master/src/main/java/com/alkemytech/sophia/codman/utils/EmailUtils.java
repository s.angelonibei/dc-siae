package com.alkemytech.sophia.codman.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;

import javax.activation.DataHandler;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import com.alkemytech.sophia.broadcasting.model.BdcBroadcasters;
import com.alkemytech.sophia.broadcasting.model.BdcUtenti;
import com.sun.jersey.core.header.FormDataContentDisposition;

public class EmailUtils {

	 public static boolean isValidEmailAddress(String email) {
         String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
         java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
         java.util.regex.Matcher m = p.matcher(email);
         return m.matches();
	 }

	 
	public static void sendEmail(
			BdcBroadcasters bdcBroadcasters, 
			BdcUtenti bdcUtenti,
			String nameSender,
			Session session, 
			String toEmail,
			List<InputStream> uploadedInputStream,
			List<FormDataContentDisposition> fileDetail, 
			String oggetto, 
			String testoMail) {

		try {
			MimeMessage msg = new MimeMessage(session);
			msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
			msg.addHeader("format", "flowed");
			msg.addHeader("Content-Transfer-Encoding", "8bit");
			msg.setFrom(new InternetAddress("fromEmail", nameSender));
			msg.setSubject(bdcBroadcasters.getNome() + " - "
					+ bdcBroadcasters.getTipo_broadcaster() + " - " + oggetto, "UTF-8");
			msg.setSentDate(new Date());
			msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setText(testoMail);
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);
			int i = 0;
			if (null != uploadedInputStream) {
				for (InputStream is : uploadedInputStream) {
					messageBodyPart = new MimeBodyPart();
					ByteArrayDataSource byteArrayDataSource = new ByteArrayDataSource(is, "application/octet-stream");
					messageBodyPart.setDataHandler(new DataHandler(byteArrayDataSource));
					messageBodyPart.setFileName(fileDetail.get(i).getFileName());
					multipart.addBodyPart(messageBodyPart);
					i++;
				}
			}

			msg.setContent(multipart);
			Transport.send(msg);
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
