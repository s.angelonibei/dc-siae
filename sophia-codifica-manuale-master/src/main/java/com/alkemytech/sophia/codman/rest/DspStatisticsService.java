package com.alkemytech.sophia.codman.rest;

import com.alkemytech.sophia.codman.dto.*;
import com.alkemytech.sophia.codman.entity.CCIDMetadata;
import com.alkemytech.sophia.codman.entity.ImportiAnticipo;
import com.alkemytech.sophia.codman.entity.InfoCcidStatistics;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.alkemytech.sophia.codman.invoice.repository.Invoice;
import com.alkemytech.sophia.codman.invoice.repository.InvoiceItemToCcid;
import com.alkemytech.sophia.codman.utils.Periods;
import com.alkemytech.sophia.codman.utils.QueryUtils;
import com.alkemytech.sophia.common.tools.StringTools;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import org.apache.commons.lang.StringUtils;
import org.jooq.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.jooq.impl.DSL.*;

@Singleton
@Path("dspStatistics")
public class DspStatisticsService {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final Provider<EntityManager> provider;
    private final Gson gson;
    private DSLContext jooq;

    @Inject
    protected DspStatisticsService(@McmdbDataSource Provider<EntityManager> provider, Gson gson, @McmdbDataSource DSLContext jooq) {
        super();
        this.provider = provider;
        this.gson = gson;
        this.jooq = jooq;
    }

    @GET
    @Path("all")
    @Produces("application/json")
    @SuppressWarnings("unchecked")
    public Response all(@DefaultValue("0") @QueryParam("first") int first,
                        @DefaultValue("50") @QueryParam("last") int last,
                        @QueryParam("dspList") List<String> dspList,
                        @QueryParam("countryList") List<String> countryList,
                        @QueryParam("utilizationList") List<String> utilizationList,
                        @QueryParam("offerList") List<String> offerList,
                        @DefaultValue("-1") @QueryParam("backclaim") int backclaim,
                        @DefaultValue("0") @QueryParam("monthFrom") int monthFrom,
                        @DefaultValue("0") @QueryParam("yearFrom") int yearFrom,
                        @DefaultValue("0") @QueryParam("monthTo") int monthTo,
                        @DefaultValue("0") @QueryParam("yearTo") int yearTo) {

        boolean checkPeriod = false;
        Map<Integer, List<Integer>> quarters = new HashMap<>();
        String inCondition = "";

        Integer defaultMonthFrom = 0;
        Integer defaultYearFrom = 2000;
        Date date = new Date();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        Integer defaultMonthTo = calendar.get(Calendar.MONTH);
        ;
        Integer defaultYearTo = calendar.get(Calendar.YEAR);
        ;

        if (monthFrom > 0 || monthTo > 0) {
            checkPeriod = true;

            if (monthFrom > 0) {
                defaultMonthFrom = monthFrom;
                defaultYearFrom = yearFrom;
            }

            if (monthTo > 0) {
                defaultMonthTo = monthTo;
                defaultYearTo = yearTo;

            }
            quarters = Periods.extractCorrespondingQuarters(defaultMonthFrom, defaultYearFrom, defaultMonthTo, defaultYearTo);

            int y = 0;
            for (int key : quarters.keySet()) {
                if (y++ != 0) {
                    inCondition += " or ";
                }
                inCondition += " (M1.PERIOD_TYPE='quarter' and M1.PERIOD in ("
                        + StringUtils.join(quarters.get(key), ',') + ") and M1.YEAR = " + key + " )";

            }
            if (!inCondition.isEmpty()) {
                inCondition = " ( " + inCondition + " )";
            }

        }

        EntityManager entityManager = null;
        List<Object[]> result;

        String bcLikeCondition = "";
        if (backclaim == 1)
            bcLikeCondition = "%_BC_A_%";
        else if (backclaim == 2)
            bcLikeCondition = "%_BC_B_%";
        else if (backclaim == 3)
            bcLikeCondition = "%_BC_C1_%";
        else if (backclaim == 4)
            bcLikeCondition = "%_BC_C2_%";
        try {

            entityManager = provider.get();
            String sql = ""
                    + "SELECT  "
                    + "y.DSP, "
                    + "y.DSR, "
                    + "y.`DATA RICEZIONE DSR`, "
                    + "y.PERIOD_TYPE, "
                    + "y.PERIOD, "
                    + "y.YEAR, "
                    + "y.PERIODO, "
                    + "y.TERRITORIO, "
                    + "y.`TIPO UTILIZZO`, "
                    + "y.`OFFERTA COMMERCIALE`, "
                    + "y.`TOTALE VALORE (€)`, "
                    + "y.CURRENCY, "
                    + "y.`TOTALE UTILIZZAZIONI`, "
                    + "y.`% IDENTIFICATO UTILIZZAZIONI`, "
                    + "y.BACKCLAIM, "
                    + "y.`DATA PRODUZIONE CCID`, "
                    + "y.EXTENDED_DSR_URL, "
                    + "y.DRILLDOWN, "
                    + "CCID_S3_PATH.ID_CCID_PATH, "
                    + "CCID_S3_PATH.IDDSR, "
                    + "CCID_S3_PATH.S3_PATH, "
                    + "CCID_METADATA.ID_CCID, "
                    + "CCID_METADATA.ID_DSR, "
                    + "CCID_METADATA.TOTAL_VALUE, "
                    + "CCID_METADATA.ID_CCID_METADATA, "
                    + "CCID_METADATA.CURRENCY, "
                    + "CCID_METADATA.CCID_VERSION, "
                    + "CCID_METADATA.CCID_ENCODED, "
                    + "CCID_METADATA.CCID_ENCODED_PROVISIONAL, "
                    + "CCID_METADATA.CCID_NOT_ENCODED, "
                    + "CCID_METADATA.INVOICE_STATUS, "
                    + "CCID_METADATA.INVOICE_ITEM, "
                    + "CCID_METADATA.VALORE_FATTURABILE, "
                    + "CCID_METADATA.SUM_USE_QUANTITY, "
                    + "CCID_METADATA.SUM_USE_QUANTITY_MATCHED, "
                    + "CCID_METADATA.SUM_USE_QUANTITY_UNMATCHED, "
                    + "CCID_METADATA.SUM_AMOUNT_LICENSOR, "
                    + "CCID_METADATA.SUM_AMOUNT_PAI, "
                    + "CCID_METADATA.SUM_AMOUNT_UNMATCHED, "
                    + "CCID_METADATA.SUM_USE_QUANTITY_SIAE, "
                    + "CCID_METADATA.VALORE_RESIDUO, "
                    + "CCID_METADATA.QUOTA_FATTURA, "
                    + "CCID_METADATA.IDENTIFICATO_VALORE_PRICING, "
                    + "CCID_METADATA.TOTAL_VALUE_CCID_CURRENCY, "
                    + "       case when  (y.DSR like '%_BC_A_%' or y.DSR like '%_BC0%') then  'A' " +
                    "           when y.DSR like '%_BC_B_%' then 'B' " +
                    "           when y.DSR like '%_BC_C1_%' then 'C1' " +
                    "           when y.DSR like '%_BC_C2_%' then 'C2' " +
                    "           else '' end  as tipoBc, "
                    + "PERC_CLAIM_RESULT.PERC_CLAIM "
                    + "from "
                    + "(select "
                    + "IF(ANAG_DSP.NAME IS NULL , '', ANAG_DSP.NAME ) AS DSP, "
                    + "IF (M1.IDDSR IS NULL, '', M1.IDDSR) AS DSR, "
                    + "IF (DSR_STEPS_MONITORING.EXTRACT_QUEUED IS NULL,' ',DATE_FORMAT (CONVERT_TZ (DSR_STEPS_MONITORING.EXTRACT_QUEUED,'+00:00','+01:00'),'%d-%m-%Y %H:%i')) AS 'DATA RICEZIONE DSR', "
                    + "M1.PERIOD_TYPE, "
                    + "M1.PERIOD, "
                    + "M1.YEAR, "
                    + "(CASE "
                    + "WHEN M1.PERIOD_TYPE = 'month' THEN "
                    + "concat(ELT(M1.PERIOD, 'Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov','Dic') , ' ', M1.YEAR) "
                    + "ELSE concat(ELT(M1.PERIOD, 'Gen - Mar', 'Apr - Giu', 'Lug - Set', 'Ott - Dic') , ' ', M1.YEAR)       END) AS PERIODO, "
                    + "IF(M1.COUNTRY IS NULL , ' ', M1.COUNTRY ) AS 'TERRITORIO', "
                    + "IF(ANAG_UTILIZATION_TYPE.NAME IS NULL , ' ', ANAG_UTILIZATION_TYPE.NAME) AS 'TIPO UTILIZZO', "
                    + "IF(COMMERCIAL_OFFERS.OFFERING IS NULL , ' ', COMMERCIAL_OFFERS.OFFERING) as 'OFFERTA COMMERCIALE', "
                    + "coalesce (BC_STAT.TOTALE_VALORE ,IF(M1.TOTAL_VALUE IS NULL , ' ', ROUND(M1.TOTAL_VALUE , 2) )) as 'TOTALE VALORE (€)', "
                    + "M1.CURRENCY as 'CURRENCY', "
                    + "coalesce (BC_STAT.TOTALE_UTILIZZAZIONI, IF(M1.SALES_LINES_NUM IS NULL , ' ', M1.SALES_LINES_NUM )) as 'TOTALE UTILIZZAZIONI', "
                    + "coalesce ( IF(S1.SALES_LINE_RECOGNIZED IS NULL OR (BC_STAT.TOTALE_UTILIZZAZIONI IS NULL OR BC_STAT.TOTALE_UTILIZZAZIONI = 0), null , round((S1.SALES_LINE_RECOGNIZED * 100) / BC_STAT.TOTALE_UTILIZZAZIONI , 2))," +
                    " IF(S1.SALES_LINE_RECOGNIZED IS NULL OR (M1.SALES_LINES_NUM IS NULL OR M1.SALES_LINES_NUM = 0), ' ', round((S1.SALES_LINE_RECOGNIZED * 100) / M1.SALES_LINES_NUM , 2))) as '% IDENTIFICATO UTILIZZAZIONI', "
                    + "M1.BACKCLAIM, "
                    + "IF (DSR_STEPS_MONITORING.CLAIM_FINISHED IS NULL,' ',DATE_FORMAT (CONVERT_TZ (DSR_STEPS_MONITORING.CLAIM_FINISHED,'+00:00','+01:00'),'%d-%m-%Y %H:%i')) AS 'DATA PRODUZIONE CCID', "
                    + "M1.EXTENDED_DSR_URL, "
                    + "IF((SELECT COUNT(ID) FROM CCID_METADATA_DETAILS d WHERE M1.IDDSR = d.ID_DSR) > 0, true,false) AS DRILLDOWN "
                    + "from "
                    + "DSR_METADATA M1 "
                    + " left join MM_BACKCLAIM_STATISTICS BC_STAT "
                    + " on BC_STAT.ID_DSR = M1.IDDSR, "
                    + "DSR_STEPS_MONITORING, "
                    + "DSR_STATISTICS S1, "
                    + "COMMERCIAL_OFFERS, "
                    + "ANAG_UTILIZATION_TYPE, "
                    + "ANAG_DSP "
                    + "where DSR_STEPS_MONITORING.IDDSR=M1.IDDSR and S1.IDDSR = M1.IDDSR "
                    + "and M1.SERVICE_CODE = COMMERCIAL_OFFERS.ID_COMMERCIAL_OFFERS "
                    + "and COMMERCIAL_OFFERS.ID_UTIILIZATION_TYPE = ANAG_UTILIZATION_TYPE.ID_UTILIZATION_TYPE "
                    + "and COMMERCIAL_OFFERS.IDDSP = ANAG_DSP.IDDSP "
                    + (!dspList.isEmpty() ? "and COMMERCIAL_OFFERS.IDDSP in ( " + QueryUtils.getValuesCollection(dspList) + " )" : "")
                    + (!countryList.isEmpty() ? "and M1.COUNTRY in ( " + QueryUtils.getValuesCollection(countryList) + " )" : "")
                    + (!utilizationList.isEmpty() ? "and COMMERCIAL_OFFERS.ID_UTIILIZATION_TYPE in ( " + QueryUtils.getValuesCollection(utilizationList) + " )" : "")
                    + (!offerList.isEmpty() ? "and COMMERCIAL_OFFERS.OFFERING in ( " + QueryUtils.getValuesCollection(offerList) + " )" : "")
                    + (!inCondition.isEmpty() ? "and (" + inCondition + " or " : "")
                    + (checkPeriod && inCondition.isEmpty() ? " and " : "")
                    + (checkPeriod ? " (PERIOD_TYPE='month' and  STR_TO_DATE(CONCAT(M1.YEAR,'-', M1.PERIOD , '-01'), '%Y-%m-%d') >= '" + defaultYearFrom + "-" + defaultMonthFrom + "-01' and STR_TO_DATE(CONCAT(M1.YEAR,'-', M1.PERIOD , '-01'), '%Y-%m-%d') <= '" + defaultYearTo + "-" + defaultMonthTo + "-01' ) " : "")
                    + (checkPeriod && !quarters.isEmpty() ? ")" : "")
                    + (backclaim != -1 ? " and (M1.IDDSR " + (backclaim == 1 ? " LIKE '%_BC0%' or M1.IDDSR LIKE '%_BC_A_%')  " : "LIKE '" + bcLikeCondition + "') ") : " ")
                    + " ) y "
                    + "left join CCID_S3_PATH "
                    + "on y.DSR = CCID_S3_PATH.IDDSR "
                    + "left join CCID_METADATA "
                    + "on y.DSR = CCID_METADATA.ID_DSR "
                    + "left join (select "
                    + "ID_DSR, CASE "
                    + "WHEN sum( "
                    + "		CASE "
                    + "				WHEN esito = 'KO' THEN 1 "
                    + "				WHEN esito = 'OK' THEN 0 "
                    + "		END) > 0 THEN 'False' "
                    + "WHEN sum( "
                    + "		CASE "
                    + "				WHEN esito = 'KO' THEN 1 "
                    + "				WHEN esito = 'OK' THEN 0 "
                    + "		END) = 0 THEN 'True' "
                    + "ELSE NULL "
                    + "END as 'PERC_CLAIM' "
                    + "from MM_PERC_CLAIM_RESULT "
                    + "GROUP BY ID_DSR) PERC_CLAIM_RESULT "
                    + "on y.DSR = PERC_CLAIM_RESULT.ID_DSR "
                    + "ORDER BY DSP, DSR, STR_TO_DATE(`DATA RICEZIONE DSR`, '%d-%m-%Y %H:%i') DESC";

            final Query q = entityManager.createNativeQuery(sql).setFirstResult(first) // offset
                    .setMaxResults(1 + last - first);

            result = (List<Object[]>) q.getResultList();
            List<DspStatiticsDTO> dtoList = new ArrayList<DspStatiticsDTO>();
            final PagedResult pagedResult = new PagedResult();
            if (null != result) {
                final int maxrows = last - first;
                final boolean hasNext = result.size() > maxrows;
                for (Object[] obj : (hasNext ? result.subList(0, maxrows) : result)) {

                    DspStatiticsDTO dto = new DspStatiticsDTO();

                    dto.setDsp((String) obj[0]);
                    dto.setDsr((String) obj[1]);
                    dto.setDsrDeliveryDate((String) obj[2]);
                    dto.setPeriodType((String) obj[3]);
                    dto.setPeriodNumber((Integer) obj[4]);
                    dto.setYear((Integer) obj[5]);
                    dto.setPeriod((String) obj[6]);
                    dto.setCountry((String) obj[7]);
                    dto.setUtilizationType((String) obj[8]);
                    dto.setCommercialOffer((String) obj[9]);
//                dto.setTotValue((BigDecimal)(obj[10]));
                    dto.setTotValue(obj[10] != null ? new BigDecimal((String) obj[10]) : null);
                    dto.setCurrency((String) obj[11]);
//                dto.setTotUsage((BigDecimal)(obj[12]));
                    dto.setTotUsage(obj[12] != null ? new BigDecimal((String) obj[12]) : null);
                    dto.setBackclaim((Long) obj[14]);
                    dto.setCcidProductionDate((String) obj[15]);
                    dto.setExtendedDsrUrl((String) obj[16]);
                    dto.setHasDrilldown((Integer) obj[17]);

                    dto.setUsageIdentified(toFLoat(obj[13]));
                    dto.setSiaeValueIdentified(obj[36] != null ? (BigDecimal) obj[36] : null);
                    dto.setCcidCurrency((String) obj[25]);

                    dto.setValueIdentified(obj[36] != null ? (BigDecimal) obj[36] : null);
                    dto.setSiaeIdentifiedVisualizations(obj[39] != null ? (BigDecimal) obj[39] : null);
                    dto.setValueNotIdentified(obj[38] != null ? (BigDecimal) obj[38] : null);
                    dto.setVisualizationsNotIdentified((Long) obj[35]);

                    dto.setCcidS3Id((Integer) obj[18]);
                    dto.setCcidS3Path((String) obj[19]);
                    dto.setValoreFatturabile((BigDecimal) obj[32]);
                    dto.setIdentificatoValorePricing((BigDecimal) obj[42]);
                    dto.setTotalValueCcidCurrency((BigDecimal) obj[43]);
                    dto.setTipoBc((String) obj[44]);

                    dto.setPercClaim(obj[45] != null ? Boolean.parseBoolean((String)obj[45]) : null);

                    dtoList.add(dto);
                }
                pagedResult.setRows(dtoList).setMaxrows(maxrows).setFirst(first)
                        .setLast(hasNext ? last : first + result.size()).setHasNext(hasNext).setHasPrev(first > 0);
            } else {
                pagedResult.setMaxrows(0).setFirst(0).setLast(0).setHasNext(false).setHasPrev(false);
            }
            return Response.ok(gson.toJson(pagedResult)).build();
        } catch (Exception e) {
            logger.error("all", e);
        }
        return Response.status(500).build();
    }


    @GET
    @Path("backclaim/{idDsr}")
    @Produces("application/json")
    @SuppressWarnings("unchecked")
    public Response backclaim(@PathParam("idDsr") String idDsr) {
        EntityManager entityManager;
        List<Object[]> result;
        try {
            entityManager = provider.get();

            String sql = "select * from "
                    + "(select "
                    + "IF(ANAG_DSP.NAME IS NULL , '', ANAG_DSP.NAME ) AS DSP, "
                    + "IF (M1.IDDSR IS NULL, '', M1.IDDSR) AS DSR, "
                    + "IF(steps_monitoring.TIMESTAMP IS NULL , ' ', DATE_FORMAT( CONVERT_TZ(from_unixtime(steps_monitoring.TIMESTAMP,'%Y-%m-%d %H:%i'),'+00:00','+01:00') ,'%d-%m-%Y %H:%i') ) AS 'DATA RICEZIONE DSR', "
                    + "M1.PERIOD_TYPE, "
                    + "M1.PERIOD, "
                    + "M1.YEAR, "
                    + "(CASE "
                    + "WHEN M1.PERIOD_TYPE = 'month' THEN "
                    + "concat(ELT(M1.PERIOD, 'Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov','Dic') , ' ', M1.YEAR) "
                    + "ELSE concat(ELT(M1.PERIOD, 'Gen - Mar', 'Apr - Giu', 'Lug - Set', 'Ott - Dic') , ' ', M1.YEAR)       END) AS PERIODO, "
                    + "IF(M1.COUNTRY IS NULL , ' ', M1.COUNTRY ) AS 'TERRITORIO', "
                    + "IF(ANAG_UTILIZATION_TYPE.NAME IS NULL , ' ', ANAG_UTILIZATION_TYPE.NAME) AS 'TIPO UTILIZZO', "
                    + "IF(COMMERCIAL_OFFERS.OFFERING IS NULL , ' ', COMMERCIAL_OFFERS.OFFERING) as 'OFFERTA COMMERCIALE', "
                    + "IF(M1.TOTAL_VALUE IS NULL , ' ', ROUND(M1.TOTAL_VALUE , 2) ) as 'TOTALE VALORE (€)', "
                    + "M1.CURRENCY as 'CURRENCY', "
                    + "IF(M1.SALES_LINES_NUM IS NULL , ' ', M1.SALES_LINES_NUM ) as 'TOTALE UTILIZZAZIONI', "
                    + "IF(S1.SALES_LINE_RECOGNIZED IS NULL OR (M1.SALES_LINES_NUM IS NULL OR M1.SALES_LINES_NUM = 0), ' ', round((S1.SALES_LINE_RECOGNIZED * 100) / M1.SALES_LINES_NUM , 2)) as '% IDENTIFICATO UTILIZZAZIONI', "
                    + "M1.BACKCLAIM "
                    + "from "
                    + "DSR_METADATA M1,"
                    + "steps_monitoring, "
                    + "DSR_STATISTICS S1, "
                    + "COMMERCIAL_OFFERS, "
                    + "ANAG_UTILIZATION_TYPE, "
                    + "ANAG_DSP "
                    + "where steps_monitoring.IDDSR=M1.IDDSR and S1.IDDSR = M1.IDDSR "
                    + "and M1.SERVICE_CODE = COMMERCIAL_OFFERS.ID_COMMERCIAL_OFFERS "
                    + "and COMMERCIAL_OFFERS.ID_UTIILIZATION_TYPE = ANAG_UTILIZATION_TYPE.ID_UTILIZATION_TYPE "
                    + "and COMMERCIAL_OFFERS.IDDSP = ANAG_DSP.IDDSP "
                    + "and (M1.IDDSR = ?1 "
                    + "or (M1.IDDSR like ?2 and M1.IDDSR not like ?3)) "
                    + "order by ANAG_DSP.NAME, steps_monitoring.TIMESTAMP desc) y "
                    + "left join CCID_S3_PATH "
                    + "on y.DSR = CCID_S3_PATH.IDDSR "
                    + "left join CCID_METADATA "
                    + "on y.DSR = CCID_METADATA.ID_DSR";

            final Query q = entityManager.createNativeQuery(sql).setFirstResult(0) // offset
                    .setMaxResults(1 + 50 - 0)
                    .setParameter(1, idDsr)
                    .setParameter(2, idDsr + "%_BC%")
                    .setParameter(3, idDsr + "%_BC_C1%");

//            int i = 1;
//            if (checkPeriod) {
//                q.setParameter(i++, yearFrom);
//                q.setParameter(i++, monthFrom);
//                q.setParameter(i++, yearTo);
//                q.setParameter(i, monthTo);
//            }
            result = (List<Object[]>) q.getResultList();
            List<DspStatiticsDTO> dtoList = new ArrayList<>();
            final PagedResult pagedResult = new PagedResult();
            if (null != result) {
                final int maxrows = 50 - 0;
                final boolean hasNext = result.size() > maxrows;
                for (Object[] p : (hasNext ? result.subList(0, maxrows) : result)) {
                    dtoList.add(new DspStatiticsDTO(p));
                }
                pagedResult.setRows(dtoList).setMaxrows(maxrows).setFirst(0)
                        .setLast(hasNext ? 50 : 0 + result.size()).setHasNext(hasNext).setHasPrev(0 > 0);
            } else {
                pagedResult.setMaxrows(0).setFirst(0).setLast(0).setHasNext(false).setHasPrev(false);
            }
            return Response.ok(gson.toJson(pagedResult)).build();
        } catch (Exception e) {
            logger.error("all", e);
        }
        return Response.status(500).build();
    }

    //


    @GET
    @Path("storico")
    @Produces(MediaType.APPLICATION_JSON)
    public Response storico(@DefaultValue("0") @QueryParam("first") int first,
                            @DefaultValue("50") @QueryParam("last") int last,
                            @QueryParam("dsr") String dsr) {


        EntityManager entityManager;
        List<Object[]> result;
        entityManager = provider.get();

        result = getBaseStatisticsQuery(dsr).fetch().into(Object[].class);

//        result = entityManager.createNativeQuery(sql).getResultList();


        String sql = "select DSP ,DSR ,DATA_RICEZIONE_DSR ,PERIOD_TYPE ,PERIOD ," +
                " YEAR ,PERIODO ,TERRITORIO ,TIPO_UTILIZZO ,OFFERTA_COMMERCIALE ,TOTALE_VALORE ," +
                " CURRENCY ,TOTALE_UTILIZZAZIONI ,IDENTIFICATO ,ID_CCID_PATH ,IDDSR ,S3_PATH ,ID_CCID ,ID_DSR ," +
                " TOTAL_VALUE ,ID_CCID_METADATA ,CURRENCY2 ,CCID_VERSION ,CCID_ENCODED ,CCID_ENCODED_PROVISIONAL ," +
                " CCID_NOT_ENCODED ,INVOICE_STATUS ,INVOICE_ITEM ,VALORE_FATTURABILE ,SUM_USE_QUANTITY ,SUM_USE_QUANTITY_MATCHED ," +
                " SUM_USE_QUANTITY_UNMATCHED ,SUM_AMOUNT_LICENSOR ,SUM_AMOUNT_PAI ,SUM_AMOUNT_UNMATCHED ,SUM_USE_QUANTITY_SIAE ," +
                " VALORE_RESIDUO ,QUOTA_FATTURA ,IDENTIFICATO_VALORE_PRICING ,TOTAL_VALUE_CCID_CURRENCY " +
                "  from MM_STATISTICHE_STORICO " +
                "  where dsr='" + dsr + "' order by id desc ";

        final Query q = entityManager.createNativeQuery(sql)
                .setFirstResult(first) // offset
                .setMaxResults(1 + last - first);

//            int i = 1;
//            if (checkPeriod) {
//                q.setParameter(i++, yearFrom);
//                q.setParameter(i++, monthFrom);
//                q.setParameter(i++, yearTo);
//                q.setParameter(i, monthTo);
//            }
        result.addAll(q.getResultList());
        if (!result.isEmpty()) {
            List<DspStatiticsDTO> dtoList = new ArrayList<>();
            final PagedResult pagedResult = new PagedResult();
            final int maxrows = last - first;
            final boolean hasNext = result.size() > maxrows;
            for (Object[] p : (hasNext ? result.subList(0, maxrows) : result)) {
                dtoList.add(new DspStatiticsDTO(p));
            }
            pagedResult.setRows(dtoList).setMaxrows(maxrows).setFirst(first)
                    .setLast(hasNext ? last : first + result.size()).setHasNext(hasNext).setHasPrev(first > 0);
            return Response.ok(gson.toJson(pagedResult)).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(gson.toJson(new MessageDTO(com.alkemytech.sophia.codman.utils.Response.MULTILMEDIALE_STATISTCHE_STORICO_NOT_EXIST))).build();
        }
    }


    @GET
    @Path("allMarketShare")
    @Produces("application/json")
    @SuppressWarnings("unchecked")
    public Response allMarketShare(@DefaultValue("0") @QueryParam("first") int first,
                                   @DefaultValue("50") @QueryParam("last") int last,
                                   @QueryParam("dspList") List<String> dspList,
                                   @QueryParam("countryList") List<String> countryList,
                                   @QueryParam("utilizationList") List<String> utilizationList,
                                   @QueryParam("offerList") List<String> offerList,
                                   @DefaultValue("-1") @QueryParam("backclaim") int backclaim,
                                   @DefaultValue("0") @QueryParam("monthFrom") int monthFrom,
                                   @DefaultValue("0") @QueryParam("yearFrom") int yearFrom,
                                   @DefaultValue("0") @QueryParam("monthTo") int monthTo,
                                   @DefaultValue("0") @QueryParam("yearTo") int yearTo,
                                   @DefaultValue("false") @QueryParam("exportAll") boolean exportAll) throws Exception {

        boolean checkPeriod = false;
        Map<Integer, List<Integer>> quarters = new HashMap<>();
        StringBuilder inCondition = new StringBuilder();

        int defaultMonthFrom = 0;
        int defaultYearFrom = 2000;
        Date date = new Date();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        int defaultMonthTo = calendar.get(Calendar.MONTH);
        int defaultYearTo = calendar.get(Calendar.YEAR);

        if (monthFrom > 0 || monthTo > 0) {
            checkPeriod = true;

            if (monthFrom > 0) {
                defaultMonthFrom = monthFrom;
                defaultYearFrom = yearFrom;
            }

            if (monthTo > 0) {
                defaultMonthTo = monthTo;
                defaultYearTo = yearTo;

            }
            quarters = Periods.extractCorrespondingQuarters(defaultMonthFrom, defaultYearFrom, defaultMonthTo, defaultYearTo);

            int y = 0;
            for (int key : quarters.keySet()) {
                if (y++ != 0) {
                    inCondition.append(" or ");
                }
                inCondition.append(" (M1.PERIOD_TYPE='quarter' and M1.PERIOD in (")
                        .append(StringUtils.join(quarters.get(key), ','))
                        .append(") and M1.YEAR = ")
                        .append(key)
                        .append(" )");

            }
            if (inCondition.length() > 0) {
                inCondition = new StringBuilder(" ( " + inCondition + " )");
            }

        }

        EntityManager entityManager;
        List<Object[]> result;
        try {

            entityManager = provider.get();

            String sql = "SELECT\n" +
                    "  IF(ANAG_DSP.NAME IS NULL, '', ANAG_DSP.NAME)  AS DSP,\n" +
                    "  M1.PERIOD_TYPE,\n" +
                    "  M1.PERIOD,\n" +
                    "  M1.YEAR,\n" +
                    "  (CASE WHEN M1.PERIOD_TYPE = 'month' THEN concat(ELT(M1.PERIOD, 'Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov', 'Dic'), ' ',M1.YEAR) ELSE concat(ELT(M1.PERIOD, 'Gen - Mar', 'Apr - Giu', 'Lug - Set', 'Ott - Dic'), ' ', M1.YEAR) END) AS PERIODO,\n" +
                    "  IF(M1.COUNTRY IS NULL, ' ',M1.COUNTRY) AS TERRITORIO,\n" +
                    "  IF(ANAG_UTILIZATION_TYPE.NAME IS NULL, ' ',ANAG_UTILIZATION_TYPE.NAME) AS TIPO_UTILIZZO,\n" +
                    "  IF(COMMERCIAL_OFFERS.OFFERING IS NULL, ' ',COMMERCIAL_OFFERS.OFFERING) AS OFFERTA_COMMERCIALE,\n" +
                    "  SUM(IF(M1.SALES_LINES_NUM IS NULL, 0,M1.SALES_LINES_NUM)) AS TOTALE_UTILIZZAZIONI,\n" +
                    "  SUM(IF(CCID_METADATA.SUM_USE_QUANTITY_SIAE IS NULL, 0,CCID_METADATA.SUM_USE_QUANTITY_SIAE))  AS TOTALE_UTILIZZAZIONI_CLAIM_SIAE,\n" +
                    "  SUM(IF(CCID_METADATA.SUM_USE_QUANTITY_SIAE IS NULL, 0,CCID_METADATA.SUM_USE_QUANTITY_SIAE)) / SUM(IF(M1.SALES_LINES_NUM IS NULL, ' ',M1.SALES_LINES_NUM)) AS MARKET_SHARE_SIAE,\n " +
                    "  IF(M1.PERIOD_TYPE = 'month', MAKEDATE(M1.YEAR, 1) + INTERVAL M1.PERIOD month - INTERVAL 1 month," +
                    "                 MAKEDATE(M1.YEAR, 1) + INTERVAL M1.PERIOD QUARTER - INTERVAL 1 Quarter) AS 'DATA A',\n" +
                    "  IF(M1.PERIOD_TYPE = 'month', MAKEDATE(M1.YEAR, 1) + INTERVAL M1.PERIOD month - INTERVAL 1 DAY, " +
                    "                 MAKEDATE(M1.YEAR, 1) + INTERVAL M1.PERIOD QUARTER - INTERVAL 1 DAY) AS 'DATA DA'" +
                    "FROM\n" +
                    "  DSR_METADATA M1\n" +
                    "  JOIN steps_monitoring ON steps_monitoring.IDDSR = M1.IDDSR\n" +
                    "  JOIN DSR_STATISTICS S1 ON S1.IDDSR = M1.IDDSR\n" +
                    "  JOIN COMMERCIAL_OFFERS ON M1.SERVICE_CODE = COMMERCIAL_OFFERS.ID_COMMERCIAL_OFFERS\n" +
                    "  JOIN ANAG_UTILIZATION_TYPE ON COMMERCIAL_OFFERS.ID_UTIILIZATION_TYPE = ANAG_UTILIZATION_TYPE.ID_UTILIZATION_TYPE\n" +
                    "  JOIN ANAG_DSP ON COMMERCIAL_OFFERS.IDDSP = ANAG_DSP.IDDSP\n" +
                    "  LEFT JOIN CCID_S3_PATH ON IF(M1.IDDSR IS NULL, '', M1.IDDSR) = CCID_S3_PATH.IDDSR\n" +
                    "  LEFT JOIN CCID_METADATA ON IF(M1.IDDSR IS NULL, '', M1.IDDSR) = CCID_METADATA.ID_DSR\n" +
                    " WHERE 1=1 " +
                    (!dspList.isEmpty() ? "and COMMERCIAL_OFFERS.IDDSP in ( " + QueryUtils.getValuesCollection(dspList) + " )" : "")
                    + (!countryList.isEmpty() ? "and M1.COUNTRY in ( " + QueryUtils.getValuesCollection(countryList) + " )" : "")
                    + (!utilizationList.isEmpty() ? "and COMMERCIAL_OFFERS.ID_UTIILIZATION_TYPE in ( " + QueryUtils.getValuesCollection(utilizationList) + " )" : "")
                    + (!offerList.isEmpty() ? "and COMMERCIAL_OFFERS.OFFERING in ( " + QueryUtils.getValuesCollection(offerList) + " )" : "")
                    + ((inCondition.length() > 0) ? "and (" + inCondition + " or " : "")
                    + (checkPeriod && (inCondition.length() == 0) ? " and " : "")
                    + (checkPeriod ? " (PERIOD_TYPE='month' and  STR_TO_DATE(CONCAT(M1.YEAR,'-', M1.PERIOD , '-01'), '%Y-%m-%d') >= '" + defaultYearFrom + "-" + defaultMonthFrom + "-01' and STR_TO_DATE(CONCAT(M1.YEAR,'-', M1.PERIOD , '-01'), '%Y-%m-%d') <= '" + defaultYearTo + "-" + defaultMonthTo + "-01' ) " : "")
                    + (checkPeriod && !quarters.isEmpty() ? ")" : "")
                    //escludo tutti i backclaim
                    + "and not (M1.IDDSR regexp '_BC_(A|B|C1|C2)_[0-9]{4}$' or M1.IDDSR regexp '_BC[0-9]{4}$')" +
                    "GROUP BY\n" +
                    "  IF(ANAG_DSP.NAME IS NULL, '', ANAG_DSP.NAME),\n" +
                    "  M1.PERIOD_TYPE,\n" +
                    "  M1.PERIOD,\n" +
                    "  M1.YEAR,\n" +
                    "  (CASE WHEN M1.PERIOD_TYPE = 'month'\n" +
                    "    THEN concat(\n" +
                    "        ELT(M1.PERIOD, 'Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov', 'Dic'), ' ',\n" +
                    "        M1.YEAR)\n" +
                    "   ELSE concat(ELT(M1.PERIOD, 'Gen - Mar', 'Apr - Giu', 'Lug - Set', 'Ott - Dic'), ' ', M1.YEAR) END),\n" +
                    "  IF(M1.COUNTRY IS NULL, ' ',M1.COUNTRY),\n" +
                    "  IF(ANAG_UTILIZATION_TYPE.NAME IS NULL, ' ', ANAG_UTILIZATION_TYPE.NAME),\n" +
                    "  IF(COMMERCIAL_OFFERS.OFFERING IS NULL, ' ', COMMERCIAL_OFFERS.OFFERING),\n" +
                    "  IF(M1.PERIOD_TYPE = 'month', MAKEDATE(M1.YEAR, 1) + INTERVAL M1.PERIOD month - INTERVAL 1 month, " +
                    "                 MAKEDATE(M1.YEAR, 1) + INTERVAL M1.PERIOD QUARTER - INTERVAL 1 Quarter),\n" +
                    "  IF(M1.PERIOD_TYPE = 'month', MAKEDATE(M1.YEAR, 1) + INTERVAL M1.PERIOD month - INTERVAL 1 DAY, " +
                    "                 MAKEDATE(M1.YEAR, 1) + INTERVAL M1.PERIOD QUARTER - INTERVAL 1 DAY) " +
                    "ORDER BY ANAG_DSP.NAME, steps_monitoring.TIMESTAMP DESC";

            String sqlTotali = "SELECT " +
                    "  count(*)," +
                    "  SUM(t.TOTALE_UTILIZZAZIONI)," +
                    "  SUM(t.TOTALE_UTILIZZAZIONI_CLAIM_SIAE)," +
                    "  SUM(t.TOTALE_UTILIZZAZIONI_CLAIM_SIAE) / SUM(t.TOTALE_UTILIZZAZIONI) " +
                    "FROM (" + sql + ") t";


            final Query q = entityManager.createNativeQuery(sql);
            if (!exportAll)
                q.setFirstResult(first) // offset
                        .setMaxResults(1 + last - first);

            final Query qTotali = entityManager.createNativeQuery(sqlTotali);

            result = (List<Object[]>) q.getResultList();
            final Object[] totali = (Object[]) qTotali.getSingleResult();
            List<MarketShareDTO> dtoList = new ArrayList<>();

            final PagedResult pagedResult = new PagedResult();
            if (null != result) {
                final int maxrows = last - first;
                final boolean hasNext = result.size() > maxrows;
                for (Object[] p : exportAll ? result : (hasNext ? result.subList(0, maxrows) : result)) {
                    MarketShareDTO marketShareDTO = new MarketShareDTO(p);
                    dtoList.add(marketShareDTO);
                }
                Map<String, Object> extra = new HashMap<String, Object>() {{
                    put("totalResults", totali[0]);
                    put("totUsage", totali[1]);
                    put("siaeIdentifiedVisualizations", totali[2]);
                    put("marketShare", totali[3]);
                }};

                pagedResult.setExtra(gson.toJsonTree(extra, new TypeToken<HashMap<String, Object>>() {
                }.getType()));
                pagedResult.setRows(dtoList).setMaxrows(maxrows).setFirst(first)
                        .setLast(hasNext ? last : first + result.size()).setHasNext(hasNext).setHasPrev(first > 0);
            } else {
                pagedResult.setMaxrows(0).setFirst(0).setLast(0).setHasNext(false).setHasPrev(false);
            }
            return Response.ok(gson.toJson(pagedResult)).build();
        } catch (Exception e) {

            logger.error("all", e);
            throw new Exception(e);
        }

    }


    @GET
    @Path("allNotPagable")
    @Produces("application/json")
    @SuppressWarnings("unchecked")
    public Response allNotPagable(
            @QueryParam("dspList") List<String> dspList,
            @QueryParam("countryList") List<String> countryList,
            @QueryParam("utilizationList") List<String> utilizationList,
            @QueryParam("offerList") List<String> offerList,
            @DefaultValue("-1") @QueryParam("backclaim") int backclaim,
            @DefaultValue("0") @QueryParam("monthFrom") int monthFrom,
            @DefaultValue("0") @QueryParam("yearFrom") int yearFrom,
            @DefaultValue("0") @QueryParam("monthTo") int monthTo,
            @DefaultValue("0") @QueryParam("yearTo") int yearTo) {

        boolean checkPeriod = false;
        Map<Integer, List<Integer>> quarters = new HashMap<>();
        String inCondition = "";

        Integer defaultMonthFrom = 0;
        Integer defaultYearFrom = 2000;
        Date date = new Date();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        Integer defaultMonthTo = calendar.get(Calendar.MONTH);
        ;
        Integer defaultYearTo = calendar.get(Calendar.YEAR);
        ;

        if (monthFrom > 0 || monthTo > 0) {
            checkPeriod = true;

            if (monthFrom > 0) {
                defaultMonthFrom = monthFrom;
                defaultYearFrom = yearFrom;
            }

            if (monthTo > 0) {
                defaultMonthTo = monthTo;
                defaultYearTo = yearTo;

            }
            quarters = Periods.extractCorrespondingQuarters(defaultMonthFrom, defaultYearFrom, defaultMonthTo, defaultYearTo);

            int y = 0;
            for (int key : quarters.keySet()) {
                if (y++ != 0) {
                    inCondition += " or ";
                }
                inCondition += " (M1.PERIOD_TYPE='quarter' and M1.PERIOD in ("
                        + StringUtils.join(quarters.get(key), ',') + ") and M1.YEAR = " + key + " )";

            }
            if (!inCondition.isEmpty()) {
                inCondition = " ( " + inCondition + " )";
            }

        }

        EntityManager entityManager = null;
        List<Object[]> result;

        String bcLikeCondition = "";
        if (backclaim == 1)
            bcLikeCondition = "%_BC_A_%";
        else if (backclaim == 2)
            bcLikeCondition = "%_BC_B_%";
        else if (backclaim == 3)
            bcLikeCondition = "%_BC_C1_%";
        else if (backclaim == 4)
            bcLikeCondition = "%_BC_C2_%";

        try {

            entityManager = provider.get();

            String sql = "SELECT y.DSP, " +
                    "       y.DSR, " +
                    "       y.`DATA RICEZIONE DSR`, " +
                    "       y.PERIOD_TYPE, " +
                    "       y.PERIOD, " +
                    "       y.YEAR, " +
                    "       '' as PERIODO," +
                    "       y.TERRITORIO, " +
                    "       y.`TIPO UTILIZZO`, " +
                    "       y.`OFFERTA COMMERCIALE`, " +
                    "       y.`TOTALE VALORE (€)`, y.CURRENCY, y.`TOTALE UTILIZZAZIONI`, " +
                    "       y.`% IDENTIFICATO UTILIZZAZIONI`, " +
                    "       y.BACKCLAIM, " +
                    "       y.`DATA PRODUZIONE CCID`, " +
                    "       y.EXTENDED_DSR_URL, " +
                    "       y.SOCIETA_TUTELA, " +
                    "       CCID_S3_PATH.ID_CCID_PATH, " +
                    "       CCID_S3_PATH.IDDSR, " +
                    "       CCID_S3_PATH.S3_PATH, " +
                    "       CCID_METADATA.ID_CCID, " +
                    "       CCID_METADATA.ID_DSR, " +
                    "       CCID_METADATA.TOTAL_VALUE, " +
                    "       CCID_METADATA.ID_CCID_METADATA, " +
                    "       CCID_METADATA.CURRENCY, " +
                    "       CCID_METADATA.CCID_VERSION, " +
                    "       CCID_METADATA.CCID_ENCODED, " +
                    "       CCID_METADATA.CCID_ENCODED_PROVISIONAL, " +
                    "       CCID_METADATA.CCID_NOT_ENCODED, " +
                    "       CCID_METADATA.INVOICE_STATUS, " +
                    "       CCID_METADATA.INVOICE_ITEM, " +
                    "       CCID_METADATA.VALORE_FATTURABILE, " +
                    "       CCID_METADATA.SUM_USE_QUANTITY, " +
                    "       CCID_METADATA.SUM_USE_QUANTITY_MATCHED, " +
                    "       CCID_METADATA.SUM_USE_QUANTITY_UNMATCHED, " +
                    "       CCID_METADATA.SUM_AMOUNT_LICENSOR, " +
                    "       CCID_METADATA.SUM_AMOUNT_PAI, " +
                    "       CCID_METADATA.SUM_AMOUNT_UNMATCHED, " +
                    "       CCID_METADATA.SUM_USE_QUANTITY_SIAE, " +
                    "       CCID_METADATA.VALORE_RESIDUO, " +
                    "       CCID_METADATA.QUOTA_FATTURA, " +
                    "       CCID_METADATA.IDENTIFICATO_VALORE_PRICING, " +
                    "       CCID_METADATA.TOTAL_VALUE_CCID_CURRENCY, " +
                    "       case when  (y.DSR like '%_BC_A_%' or y.DSR like '%_BC0%') then  'TIPO A'\n" +
                    "           when y.DSR like '%_BC_B_%' then 'TIPO B'\n" +
                    "           when y.DSR like '%_BC_C1_%' then 'TIPO C1'\n" +
                    "           when y.DSR like '%_BC_C2_%' then 'TIPO C2'\n" +
                    "           else '' end  as tipoBc, " +
                    "       y.`DATA A`, " +
                    "       y.`DATA DA` " +
                    "FROM (SELECT IF (ANAG_DSP.NAME IS NULL,'',ANAG_DSP.NAME) AS DSP, " +
                    "             IF (M1.IDDSR IS NULL,'',M1.IDDSR) AS DSR, " +
                    "             IF (DSR_STEPS_MONITORING.EXTRACT_QUEUED IS NULL,' ',DATE_FORMAT (CONVERT_TZ (DSR_STEPS_MONITORING.EXTRACT_QUEUED,'+00:00','+01:00'),'%d-%m-%Y %H:%i')) AS 'DATA RICEZIONE DSR', " +
                    "             M1.PERIOD_TYPE, " +
                    "             M1.PERIOD, " +
                    "             M1.YEAR, " +
                    "             IF(M1.PERIOD_TYPE = 'month', MAKEDATE(M1.YEAR, 1) + INTERVAL M1.PERIOD month - INTERVAL 1 month, " +
                    "                            MAKEDATE(M1.YEAR, 1) + INTERVAL M1.PERIOD QUARTER - INTERVAL 1 Quarter) AS 'DATA A', " +
                    "             IF(M1.PERIOD_TYPE = 'month', MAKEDATE(M1.YEAR, 1) + INTERVAL M1.PERIOD month - INTERVAL 1 DAY, " +
                    "                            MAKEDATE(M1.YEAR, 1) + INTERVAL M1.PERIOD QUARTER - INTERVAL 1 DAY) AS 'DATA DA', " +
                    "             IF (M1.COUNTRY IS NULL,' ',M1.COUNTRY) AS 'TERRITORIO', " +
                    "             IF (ANAG_UTILIZATION_TYPE.NAME IS NULL,' ',ANAG_UTILIZATION_TYPE.NAME) AS 'TIPO UTILIZZO', " +
                    "             IF (COMMERCIAL_OFFERS.OFFERING IS NULL,' ',COMMERCIAL_OFFERS.OFFERING) AS 'OFFERTA COMMERCIALE', " +
                    "           coalesce (BC_STAT.TOTALE_VALORE ,IF(M1.TOTAL_VALUE IS NULL , ' ', ROUND(M1.TOTAL_VALUE , 2) )) as 'TOTALE VALORE (€)', " +
                    "           M1.CURRENCY as 'CURRENCY', " +
                    "           coalesce (BC_STAT.TOTALE_UTILIZZAZIONI, IF(M1.SALES_LINES_NUM IS NULL , ' ', M1.SALES_LINES_NUM )) as 'TOTALE UTILIZZAZIONI', " +
                    "           coalesce ( IF(S1.SALES_LINE_RECOGNIZED IS NULL OR (BC_STAT.TOTALE_UTILIZZAZIONI IS NULL OR BC_STAT.TOTALE_UTILIZZAZIONI = 0), null , round((S1.SALES_LINE_RECOGNIZED * 100) / BC_STAT.TOTALE_UTILIZZAZIONI , 2))," +
                    "           IF(S1.SALES_LINE_RECOGNIZED IS NULL OR (M1.SALES_LINES_NUM IS NULL OR M1.SALES_LINES_NUM = 0), ' ', round((S1.SALES_LINE_RECOGNIZED * 100) / M1.SALES_LINES_NUM , 2))) as '% IDENTIFICATO UTILIZZAZIONI', " +
                    "             M1.BACKCLAIM, " +
                    "             IF (DSR_STEPS_MONITORING.CLAIM_FINISHED IS NULL,' ',DATE_FORMAT (CONVERT_TZ (DSR_STEPS_MONITORING.CLAIM_FINISHED,'+00:00','+01:00'),'%d-%m-%Y %H:%i')) AS 'DATA PRODUZIONE CCID', " +
                    "             M1.EXTENDED_DSR_URL, " +
                    //"             COALESCE((SELECT GROUP_CONCAT(d.SOCIETA) FROM CCID_METADATA_DETAILS d WHERE M1.IDDSR = d.ID_DSR),'SIAE') AS SOCIETA_TUTELA " +
                    "            if(CCID_METADATA_DETAILS.ID is null, 'SIAE', 'TOTALE')             AS SOCIETA_TUTELA" +
                    "      FROM DSR_METADATA M1 " +
                    "JOIN DSR_STEPS_MONITORING ON DSR_STEPS_MONITORING.IDDSR = M1.IDDSR " +
                    "           JOIN DSR_STATISTICS S1 ON S1.IDDSR = M1.IDDSR " +
                    "           JOIN COMMERCIAL_OFFERS ON M1.SERVICE_CODE = COMMERCIAL_OFFERS.ID_COMMERCIAL_OFFERS " +
                    "           JOIN ANAG_UTILIZATION_TYPE ON COMMERCIAL_OFFERS.ID_UTIILIZATION_TYPE = ANAG_UTILIZATION_TYPE.ID_UTILIZATION_TYPE " +
                    "           JOIN ANAG_DSP ON COMMERCIAL_OFFERS.IDDSP = ANAG_DSP.IDDSP " +
                    "            left join CCID_METADATA_DETAILS on CCID_METADATA_DETAILS.ID_DSR = M1.IDDSR " +
                    "           left join MM_BACKCLAIM_STATISTICS BC_STAT  on BC_STAT.ID_DSR = M1.IDDSR " +
                    "      WHERE DSR_STEPS_MONITORING.IDDSR = M1.IDDSR " +
                    "      AND   S1.IDDSR = M1.IDDSR " +
                    "      AND   M1.SERVICE_CODE = COMMERCIAL_OFFERS.ID_COMMERCIAL_OFFERS " +
                    "      AND   COMMERCIAL_OFFERS.ID_UTIILIZATION_TYPE = ANAG_UTILIZATION_TYPE.ID_UTILIZATION_TYPE " +
                    "      AND   COMMERCIAL_OFFERS.IDDSP = ANAG_DSP.IDDSP "
                    + (!dspList.isEmpty() ? "and COMMERCIAL_OFFERS.IDDSP in ( " + QueryUtils.getValuesCollection(dspList) + " )" : "")
                    + (!countryList.isEmpty() ? "and M1.COUNTRY in ( " + QueryUtils.getValuesCollection(countryList) + " )" : "")
                    + (!utilizationList.isEmpty() ? "and COMMERCIAL_OFFERS.ID_UTIILIZATION_TYPE in ( " + QueryUtils.getValuesCollection(utilizationList) + " )" : "")
                    + (!offerList.isEmpty() ? "and COMMERCIAL_OFFERS.OFFERING in ( " + QueryUtils.getValuesCollection(offerList) + " )" : "")
                    + (!inCondition.isEmpty() ? "and (" + inCondition + " or " : "")
                    + (checkPeriod && inCondition.isEmpty() ? " and " : "")
                    + (checkPeriod ? " (PERIOD_TYPE='month' and  STR_TO_DATE(CONCAT(M1.YEAR,'-', M1.PERIOD , '-01'), '%Y-%m-%d') >= '" + defaultYearFrom + "-" + defaultMonthFrom + "-01' and STR_TO_DATE(CONCAT(M1.YEAR,'-', M1.PERIOD , '-01'), '%Y-%m-%d') <= '" + defaultYearTo + "-" + defaultMonthTo + "-01' ) " : "")
                    + (backclaim != -1 ? " and (M1.IDDSR " + (backclaim == 1 ? " LIKE '%_BC0%' or M1.IDDSR LIKE '%_BC_A_%')  " : "LIKE '" + bcLikeCondition + "') ") : " ")
                    + (checkPeriod && !quarters.isEmpty() ? ")" : "") + " ) y " +
                    "  LEFT JOIN CCID_S3_PATH ON y.DSR = CCID_S3_PATH.IDDSR " +
                    "  LEFT JOIN CCID_METADATA ON y.DSR = CCID_METADATA.ID_DSR " +
                    "UNION " +
                    "SELECT y.DSP, " +
                    "       y.DSR, " +
                    "       y.`DATA RICEZIONE DSR`, " +
                    "       y.PERIOD_TYPE, " +
                    "       y.PERIOD, " +
                    "       y.YEAR, " +
                    "       '' as PERIODO, " +
                    "       y.TERRITORIO, " +
                    "       y.`TIPO UTILIZZO`, " +
                    "       y.`OFFERTA COMMERCIALE`, " +
                    "       y.`TOTALE VALORE (€)`, y.CURRENCY, y.`TOTALE UTILIZZAZIONI`, " +
                    "       cmd.PERCENTUALE_IDENTIFICATO_UTILIZZAZIONI, " +
                    "       y.BACKCLAIM, " +
                    "       y.`DATA PRODUZIONE CCID`, " +
                    "       y.EXTENDED_DSR_URL, " +
                    "       cmd.SOCIETA, " +
                    "       CCID_S3_PATH.ID_CCID_PATH, " +
                    "       CCID_S3_PATH.IDDSR, " +
                    "       CCID_S3_PATH.S3_PATH, " +
                    "       CCID_METADATA.ID_CCID, " +
                    "       CCID_METADATA.ID_DSR, " +
                    "       CCID_METADATA.TOTAL_VALUE, " +
                    "       CCID_METADATA.ID_CCID_METADATA, " +
                    "       CCID_METADATA.CURRENCY, " +
                    "       CCID_METADATA.CCID_VERSION, " +
                    "       CCID_METADATA.CCID_ENCODED, " +
                    "       CCID_METADATA.CCID_ENCODED_PROVISIONAL, " +
                    "       CCID_METADATA.CCID_NOT_ENCODED, " +
                    "       CCID_METADATA.INVOICE_STATUS, " +
                    "       CCID_METADATA.INVOICE_ITEM, " +
                    "       CCID_METADATA.VALORE_FATTURABILE, " +
                    "       CCID_METADATA.SUM_USE_QUANTITY, " +
                    "       CCID_METADATA.SUM_USE_QUANTITY_MATCHED, " +
                    "       cmd.NON_IDENTIFICATO_UTILIZZAZIONI, " +
                    "       cmd.CLAIM_VALORE, " +
                    "       CCID_METADATA.SUM_AMOUNT_PAI, " +
                    "       cmd.NON_IDENTIFICATO_VALORE, " +
                    "       cmd.CLAIM_UTILIZZAZIONI, " +
                    "       CCID_METADATA.VALORE_RESIDUO, " +
                    "       CCID_METADATA.QUOTA_FATTURA, " +
                    "       cmd.IDENTIFICATO_VALORE, " +
                    "       CCID_METADATA.TOTAL_VALUE_CCID_CURRENCY, " +
                    "       case when  (y.DSR like '%_BC_A_%' or y.DSR like '%_BC0%') then  'TIPO A' " +
                    "           when y.DSR like '%_BC_B_%' then 'TIPO B' " +
                    "           when y.DSR like '%_BC_C1_%' then 'TIPO C1' " +
                    "           when y.DSR like '%_BC_C2_%' then 'TIPO C2' " +
                    "           else '' end  as tipoBc, " +
                    "       y.`DATA A`, " +
                    "       y.`DATA DA` " +
                    "FROM (SELECT IF (ANAG_DSP.NAME IS NULL,'',ANAG_DSP.NAME) AS DSP, " +
                    "             IF (M1.IDDSR IS NULL,'',M1.IDDSR) AS DSR, " +
                    "             IF (DSR_STEPS_MONITORING.EXTRACT_QUEUED IS NULL,' ',DATE_FORMAT (CONVERT_TZ (DSR_STEPS_MONITORING.EXTRACT_QUEUED,'+00:00','+01:00'),'%d-%m-%Y %H:%i')) AS 'DATA RICEZIONE DSR', " +
                    "             M1.PERIOD_TYPE, " +
                    "             M1.PERIOD, " +
                    "             M1.YEAR, " +
                    "             IF(M1.PERIOD_TYPE = 'month', MAKEDATE(M1.YEAR, 1) + INTERVAL M1.PERIOD month - INTERVAL 1 month, " +
                    "                            MAKEDATE(M1.YEAR, 1) + INTERVAL M1.PERIOD QUARTER - INTERVAL 1 Quarter) AS 'DATA A', " +
                    "             IF(M1.PERIOD_TYPE = 'month', MAKEDATE(M1.YEAR, 1) + INTERVAL M1.PERIOD month - INTERVAL 1 DAY, " +
                    "                            MAKEDATE(M1.YEAR, 1) + INTERVAL M1.PERIOD QUARTER - INTERVAL 1 DAY) AS 'DATA DA', " +
                    "             IF (M1.COUNTRY IS NULL,' ',M1.COUNTRY) AS 'TERRITORIO', " +
                    "             IF (ANAG_UTILIZATION_TYPE.NAME IS NULL,' ',ANAG_UTILIZATION_TYPE.NAME) AS 'TIPO UTILIZZO', " +
                    "             IF (COMMERCIAL_OFFERS.OFFERING IS NULL,' ',COMMERCIAL_OFFERS.OFFERING) AS 'OFFERTA COMMERCIALE', " +
                    "           coalesce (BC_STAT.TOTALE_VALORE ,IF(M1.TOTAL_VALUE IS NULL , ' ', ROUND(M1.TOTAL_VALUE , 2) )) as 'TOTALE VALORE (€)', " +
                    "           M1.CURRENCY as 'CURRENCY', " +
                    "           coalesce (BC_STAT.TOTALE_UTILIZZAZIONI, IF(M1.SALES_LINES_NUM IS NULL , ' ', M1.SALES_LINES_NUM )) as 'TOTALE UTILIZZAZIONI', " +
                    "           coalesce ( IF(S1.SALES_LINE_RECOGNIZED IS NULL OR (BC_STAT.TOTALE_UTILIZZAZIONI IS NULL OR BC_STAT.TOTALE_UTILIZZAZIONI = 0), null , round((S1.SALES_LINE_RECOGNIZED * 100) / BC_STAT.TOTALE_UTILIZZAZIONI , 2))," +
                    "           IF(S1.SALES_LINE_RECOGNIZED IS NULL OR (M1.SALES_LINES_NUM IS NULL OR M1.SALES_LINES_NUM = 0), ' ', round((S1.SALES_LINE_RECOGNIZED * 100) / M1.SALES_LINES_NUM , 2))) as '% IDENTIFICATO UTILIZZAZIONI', " +
                    "             M1.BACKCLAIM, " +
                    "             IF (DSR_STEPS_MONITORING.CLAIM_FINISHED IS NULL,' ',DATE_FORMAT (CONVERT_TZ (DSR_STEPS_MONITORING.CLAIM_FINISHED,'+00:00','+01:00'),'%d-%m-%Y %H:%i')) AS 'DATA PRODUZIONE CCID', " +
                    "             M1.EXTENDED_DSR_URL " +
                    "      FROM   " +
                    " DSR_METADATA M1 " +
                    " left join MM_BACKCLAIM_STATISTICS BC_STAT " +
                    " on BC_STAT.ID_DSR = M1.IDDSR, " +
                    "           DSR_STEPS_MONITORING, " +
                    "           DSR_STATISTICS S1, " +
                    "           COMMERCIAL_OFFERS, " +
                    "           ANAG_UTILIZATION_TYPE, " +
                    "           ANAG_DSP " +
                    "      WHERE DSR_STEPS_MONITORING.IDDSR = M1.IDDSR " +
                    "      AND   S1.IDDSR = M1.IDDSR " +
                    "      AND   M1.SERVICE_CODE = COMMERCIAL_OFFERS.ID_COMMERCIAL_OFFERS " +
                    "      AND   COMMERCIAL_OFFERS.ID_UTIILIZATION_TYPE = ANAG_UTILIZATION_TYPE.ID_UTILIZATION_TYPE " +
                    "      AND   COMMERCIAL_OFFERS.IDDSP = ANAG_DSP.IDDSP "
                    + (!dspList.isEmpty() ? "and COMMERCIAL_OFFERS.IDDSP in ( " + QueryUtils.getValuesCollection(dspList) + " )" : "")
                    + (!countryList.isEmpty() ? "and M1.COUNTRY in ( " + QueryUtils.getValuesCollection(countryList) + " )" : "")
                    + (!utilizationList.isEmpty() ? "and COMMERCIAL_OFFERS.ID_UTIILIZATION_TYPE in ( " + QueryUtils.getValuesCollection(utilizationList) + " )" : "")
                    + (!offerList.isEmpty() ? "and COMMERCIAL_OFFERS.OFFERING in ( " + QueryUtils.getValuesCollection(offerList) + " )" : "")
                    + (!inCondition.isEmpty() ? "and (" + inCondition + " or " : "")
                    + (checkPeriod && inCondition.isEmpty() ? " and " : "")
                    + (checkPeriod ? " (PERIOD_TYPE='month' and  STR_TO_DATE(CONCAT(M1.YEAR,'-', M1.PERIOD , '-01'), '%Y-%m-%d') >= '" + defaultYearFrom + "-" + defaultMonthFrom + "-01' and STR_TO_DATE(CONCAT(M1.YEAR,'-', M1.PERIOD , '-01'), '%Y-%m-%d') <= '" + defaultYearTo + "-" + defaultMonthTo + "-01' ) " : "")
                    + (backclaim != -1 ? " and (M1.IDDSR " + (backclaim == 1 ? " LIKE '%_BC0%' or M1.IDDSR LIKE '%_BC_A_%')  " : "LIKE '" + bcLikeCondition + "') ") : " ")
                    + (checkPeriod && !quarters.isEmpty() ? ")" : "") + " ) y" +
                    "  LEFT JOIN CCID_S3_PATH ON y.DSR = CCID_S3_PATH.IDDSR " +
                    "  LEFT JOIN CCID_METADATA ON y.DSR = CCID_METADATA.ID_DSR " +
                    "  JOIN CCID_METADATA_DETAILS cmd ON cmd.ID_DSR = y.DSR " +
                    "ORDER BY DSP, " +
                    "         DSR, " +
                    "         STR_TO_DATE(`DATA RICEZIONE DSR`,'%d-%m-%Y %H:%i') DESC";

            final Query q = entityManager.createNativeQuery(sql);

            result = (List<Object[]>) q.getResultList();
            List<DspStatiticsDTO> dtoList = new ArrayList<>();

            for (Object[] obj : result) {

                DspStatiticsDTO dto = new DspStatiticsDTO();

                dto.setDsp((String) obj[0]);
                dto.setDsr((String) obj[1]);
                dto.setDsrDeliveryDate((String) obj[2]);
                dto.setPeriodType((String) obj[3]);
                dto.setPeriodNumber((Integer) obj[4]);
                dto.setYear((Integer) obj[5]);
                dto.setPeriod((String) obj[6]);
                dto.setCountry((String) obj[7]);
                dto.setUtilizationType((String) obj[8]);
                dto.setCommercialOffer((String) obj[9]);
//                dto.setTotValue((BigDecimal)(obj[10]));
                dto.setTotValue(obj[10] != null ? new BigDecimal((String) obj[10]) : null);
                dto.setCurrency((String) obj[11]);
//                dto.setTotUsage((BigDecimal)(obj[12]));
                dto.setTotUsage(obj[12] != null ? new BigDecimal((String) obj[12]) : null);
                dto.setUsageIdentified(toFLoat(obj[13]));
                dto.setBackclaim((Long) obj[14]);
                dto.setCcidProductionDate((String) obj[15]);
                dto.setExtendedDsrUrl((String) obj[16]);

                dto.setSocietaTutela((String) obj[17]);

                dto.setSiaeValueIdentified(obj[36] != null ? (BigDecimal) obj[36] : null);
                dto.setCcidCurrency((String) obj[25]);

                dto.setValueIdentified(obj[36] != null ? (BigDecimal) obj[36] : null);
                dto.setSiaeIdentifiedVisualizations(obj[39] != null ? (BigDecimal) obj[39] : null);
                dto.setValueNotIdentified(obj[38] != null ? (BigDecimal) obj[38] : null);
                dto.setVisualizationsNotIdentified((Long) obj[35]);

                dto.setCcidS3Id((Integer) obj[18]);
                dto.setCcidS3Path((String) obj[19]);
                dto.setValoreFatturabile((BigDecimal) obj[32]);
                dto.setIdentificatoValorePricing((BigDecimal) obj[42]);
                dto.setTotalValueCcidCurrency((BigDecimal) obj[43]);
                dto.setTipoBc((String) obj[44]);
                dto.setDateFrom((Date) obj[45]);
                dto.setDateTo((Date) obj[46]);

                dto.setCcidCurrency((String) obj[25]);


                try {
                    String justFileName = dto.getCcidS3Path().substring(dto.getCcidS3Path().lastIndexOf("/") + 1, dto.getCcidS3Path().length());
                    dto.setCcidName(justFileName);
                } catch (Exception e) {
                    logger.debug("Cannot extract CCID name", e.getMessage());
                }

                dtoList.add(dto);
            }

            return Response.ok(gson.toJson(dtoList)).build();
        } catch (Exception e) {
            logger.error("all", e);
        }
        return Response.status(500).build();
    }

    @POST
    @Path("anticipiAssociati")
    @Produces("application/json")
    @SuppressWarnings("unchecked")
    public Response getAnticipi(InfoDspStatisticsDTO dto) {

        EntityManager entityManager = null;
        CCIDMetadata ccidMetadata = null;
        Invoice invoice = null;
        List<Invoice> invoices = new ArrayList<>();
        List<ImportiAnticipo> anticipiDisponibiliList = new ArrayList<>();
        List<InfoCcidStatisticsDTO> resultListDTO = new ArrayList<>();
        SimpleDateFormat simpledateformat = new SimpleDateFormat("dd-MM-yyyy");
        entityManager = provider.get();
        ccidMetadata = (CCIDMetadata) entityManager.createQuery("Select x From CCIDMetadata x WHERE x.idDsr=:idDsr").setParameter("idDsr", dto.getDsr()).getSingleResult();
        try {
            if (ccidMetadata != null) {

                for (InvoiceItemToCcid item : ccidMetadata.getInvoiceItemToCcid()) {
                    if (!invoiceDuplicateContain(invoices, item.getInvoiceItem().getInvoice())) {
                        invoices.add(item.getInvoiceItem().getInvoice());
                    }
                }

                for (Invoice i : invoices) {
                    if (i.getInvoiceType().equals("AUTOMATICA")) {
                        invoice = i;
                    } else {
                        try {
                            ImportiAnticipo importiAnticipo = (ImportiAnticipo) entityManager.createQuery("SELECT x FROM ImportiAnticipo x where x.idInvoice = :idInvoice").setParameter("idInvoice", i.getIdInvoice()).getSingleResult();
                            BigDecimal totalValue = new BigDecimal(0);
                            for (InvoiceItemToCcid invoiceItemsToCcid : i.getInvoiceItems().get(0).getInvoiceItemToCcid()) {
                                if (ccidMetadata.getIdCCIDMetadata().equals(invoiceItemsToCcid.getIdCcidMedata().getIdCCIDMetadata())) {
                                    totalValue = totalValue.add(invoiceItemsToCcid.getValore());
                                }
                            }
                            InfoCcidStatistics ccidStatisticsDTO = new InfoCcidStatistics(
                                    importiAnticipo.getIdImportoAnticipo(), importiAnticipo.getPeriodoPertinenzaInizio(), importiAnticipo.getPeriodoPertinenzaFine(),
                                    importiAnticipo.getNumeroFattura(), importiAnticipo.getImportoOriginale(), importiAnticipo.getImportoUtilizzabile(), importiAnticipo.getIdDsp(),
                                    totalValue);
                            resultListDTO.add(new InfoCcidStatisticsDTO(ccidStatisticsDTO));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }

            }


            // verifica se esistono anticipi con dei residui
            String periodoAnticipi = "01-" + "0" + dto.getPeriodNumber() + "-" + dto.getYear();
            Date periodoAnticipiDate = simpledateformat.parse(periodoAnticipi);
            anticipiDisponibiliList = entityManager.createQuery("SELECT x FROM ImportiAnticipo x WHERE x.importoUtilizzabile != 0 AND x.idDsp=:idDsp AND :periodoAnticipo BETWEEN x.periodoPertinenzaInizio AND x.periodoPertinenzaFine")
                    .setParameter("periodoAnticipo", periodoAnticipiDate).setParameter("idDsp", dto.getDsp())
                    .getResultList();


            // definisce una variabilie BigDecimal per la somma dato che è un oggetto immutabile
            BigDecimal totaleQuota = new BigDecimal(0);

            // somma le quote di tutti gli anticipi
            for (InfoCcidStatisticsDTO infoCcidStatisticsDTO : resultListDTO) {
                totaleQuota = totaleQuota.add(infoCcidStatisticsDTO.getQuotaAnticipo());
            }

            // prepara l'oggetto da restituire.
            DettaglioDspStatisticsDTO result = new DettaglioDspStatisticsDTO();
            if (ccidMetadata != null) {
                result.setInvoiceStatus(ccidMetadata.getInvoiceStatus());
            } else {
                result.setInvoiceStatus("N/A");
            }
            result.setListAnticipo(resultListDTO);
            result.setTotaleQuoteAnticipo(totaleQuota);

            if (invoice != null && invoice.getInvoiceCode() != null) {
                result.setInvoiceCode(invoice.getInvoiceCode());
                result.setTotaleFatturato(invoice.getTotal());
            }

            if (anticipiDisponibiliList != null && anticipiDisponibiliList.size() != 0) {
                result.setFoundAnticipi(true);
            } else {
                result.setFoundAnticipi(false);
            }


            return Response.ok(gson.toJson(result)).build();

        } catch (Exception e) {
            e.printStackTrace();
        }


        return Response.status(500).build();
    }

    private boolean invoiceDuplicateContain(List<Invoice> invoices, Invoice invoice) {
        for (Invoice invoice2 : invoices) {
            if (invoice.getIdInvoice().equals(invoice2.getIdInvoice())) {
                return true;
            }
        }
        return false;
    }

    private SelectJoinStep<Record> getBaseStatisticsQuery(String idDsr) {
        SelectQuery selectQuery =
                jooq.select(
                        field("ANAG_DSP.NAME").as("DSP"),
                        field("M1.IDDSR").as("DSR"),
                        field("IF(steps_monitoring.TIMESTAMP IS NULL , '', DATE_FORMAT( CONVERT_TZ(from_unixtime(steps_monitoring.TIMESTAMP,'%Y-%m-%d %H:%i'),'+00:00','+01:00') ,'%d-%m-%Y %H:%i') )").as("DATA_RICEZIONE_DSR"),
                        field("M1.PERIOD_TYPE").as("PERIOD_TYPE"),
                        field("M1.PERIOD").as("PERIOD"),
                        field("M1.YEAR").as("YEAR"),
                        field("(CASE WHEN M1.PERIOD_TYPE = 'month' THEN concat(ELT(M1.PERIOD, 'Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov','Dic') , ' ', M1.YEAR) ELSE concat(ELT(M1.PERIOD, 'Gen - Mar', 'Apr - Giu', 'Lug - Set', 'Ott - Dic') , ' ', M1.YEAR)       END)").as("PERIODO"),
                        field("M1.COUNTRY").as("TERRITORIO"),
                        field("ANAG_UTILIZATION_TYPE.NAME").as("TIPO_UTILIZZO"),
                        field("COMMERCIAL_OFFERS.OFFERING").as("OFFERTA_COMMERCIALE"),
                        field("M1.TOTAL_VALUE").as("TOTALE_VALORE"),
                        field("M1.CURRENCY").as("CURRENCY"),
                        field("M1.SALES_LINES_NUM").as("TOTALE_UTILIZZAZIONI"),
                        when(field("S1.SALES_LINE_RECOGNIZED").isNull().or(field("S1.SALES_LINE_RECOGNIZED").isNull().or(field("M1.SALES_LINES_NUM").eq(0))), "").otherwise(field("S1.SALES_LINE_RECOGNIZED").cast(Long.class).multiply(100).divide(field("M1.SALES_LINES_NUM").cast(Long.class)).cast(String.class)).as("IDENTIFICATO")
                )
                        .from(table("DSR_METADATA").asTable("M1"),
                                table("steps_monitoring"),
                                table("DSR_STATISTICS").asTable("S1"),
                                table("COMMERCIAL_OFFERS"),
                                table("ANAG_UTILIZATION_TYPE"),
                                table("ANAG_DSP"))
                        .where(field("steps_monitoring.IDDSR").eq(field("M1.IDDSR"))
                                .and(field("S1.IDDSR").eq(field("M1.IDDSR")))
                                .and(field("M1.SERVICE_CODE").eq(field("COMMERCIAL_OFFERS.ID_COMMERCIAL_OFFERS")))
                                .and(field("COMMERCIAL_OFFERS.ID_UTIILIZATION_TYPE").eq(field("ANAG_UTILIZATION_TYPE.ID_UTILIZATION_TYPE")))
                                .and(field("COMMERCIAL_OFFERS.IDDSP").eq(field("ANAG_DSP.IDDSP")))).getQuery();

        if (StringUtils.isNotEmpty(idDsr)) {
            selectQuery.addConditions(field("M1.IDDSR", String.class).eq(idDsr));
        }

        selectQuery
                .addOrderBy(field("ANAG_DSP.NAME").asc(), field("steps_monitoring.TIMESTAMP").desc());

        Table nested = table(selectQuery).asTable("a")
                .leftOuterJoin(table("CCID_S3_PATH").asTable("b"))
                .on(field("DSR").eq(field("IDDSR")))
                .leftOuterJoin(table("CCID_METADATA").asTable("c"))
                .on(field("DSR").eq(field("ID_DSR")));

        return jooq.select(
                field("a.DSP"),
                field("a.DSR"),
                field("a.DATA_RICEZIONE_DSR"),
                field("a.PERIOD_TYPE"),
                field("a.PERIOD"),
                field("a.YEAR"),
                field("a.PERIODO"),
                field("a.TERRITORIO"),
                field("a.TIPO_UTILIZZO"),
                field("a.OFFERTA_COMMERCIALE"),
                field("a.TOTALE_VALORE"),
                field("a.CURRENCY"),
                field("a.TOTALE_UTILIZZAZIONI"),
                field("a.IDENTIFICATO"),
                field("b.ID_CCID_PATH"),
                field("b.IDDSR"),
                field("b.S3_PATH"),
                field("c.ID_CCID"),
                field("c.ID_DSR"),
                field("c.TOTAL_VALUE"),
                field("c.ID_CCID_METADATA"),
                field("c.CURRENCY"),
                field("c.CCID_VERSION"),
                field("c.CCID_ENCODED"),
                field("c.CCID_ENCODED_PROVISIONAL"),
                field("c.CCID_NOT_ENCODED"),
                field("c.INVOICE_STATUS"),
                field("c.INVOICE_ITEM"),
                field("c.VALORE_FATTURABILE"),
                field("c.SUM_USE_QUANTITY"),
                field("c.SUM_USE_QUANTITY_MATCHED"),
                field("c.SUM_USE_QUANTITY_UNMATCHED"),
                field("c.SUM_AMOUNT_LICENSOR"),
                field("c.SUM_AMOUNT_PAI"),
                field("c.SUM_AMOUNT_UNMATCHED"),
                field("c.SUM_USE_QUANTITY_SIAE"),
                field("c.VALORE_RESIDUO"),
                field("c.QUOTA_FATTURA"),
                field("c.IDENTIFICATO_VALORE_PRICING"),
                field("c.TOTAL_VALUE_CCID_CURRENCY")
        ).from(nested);
    }


    @GET
    @Path("drilldown")
    @Produces("application/json")
    @SuppressWarnings("unchecked")
    public Response drilldown(@QueryParam("idDsr") String idDsr) {
        String drilldownSql = "SELECT ID," // 0
                + "SOCIETA," // 1
                + "IDENTIFICATO_VALORE," // 2
                + "IDENTIFICATO_UTILIZZAZIONI," // 3
                + "PERCENTUALE_IDENTIFICATO_UTILIZZAZIONI," // 4
                + "CLAIM_VALORE," // 5
                + "CLAIM_UTILIZZAZIONI," // 6
                + "NON_IDENTIFICATO_VALORE," // 7
                + "NON_IDENTIFICATO_UTILIZZAZIONI," // 8
                + "ID_DSR FROM CCID_METADATA_DETAILS " // 9
                + "WHERE ID_DSR= ?";
        try {
            EntityManager entityManager = provider.get();
            final Query q = entityManager.createNativeQuery(drilldownSql);
            q.setParameter(1, idDsr);

            List<Object[]> result = (List<Object[]>) q.getResultList();
            List<DrillDownDspStatiticsDTO> dtoList = new ArrayList<DrillDownDspStatiticsDTO>();

            for (Object[] p : result) {
                DrillDownDspStatiticsDTO e = new DrillDownDspStatiticsDTO();
                e.setSocieta((String) p[1]);
                e.setIdentificatoValore(((BigDecimal) p[2]));
                e.setIdentificatoUtilizzazioni(new BigDecimal(((Long) p[3])));
                e.setPercentualeIdentificatoUtilizzazioni((BigDecimal) p[4]);
                e.setClaimValore(((BigDecimal) p[5]));
                e.setClaimUtilizzazioni(new BigDecimal(((Long) p[6])));
                e.setNonIdentificatoValore(((BigDecimal) p[7]));
                e.setNonIdentificatoUtilizzazioni(new BigDecimal(((Long) p[8])));
                dtoList.add(e);
            }
            return Response.ok(gson.toJson(dtoList)).build();
        } catch (Exception e) {
            logger.error("all", e);
            return Response.status(500).build();
        }
    }

    public static Float toFLoat(Object obj) {
        try {
            String str = (String) obj;
            BigDecimal bd = new BigDecimal(str);
            BigDecimal scaled = bd.setScale(2, RoundingMode.HALF_UP);
            str = scaled.toPlainString().trim();
            if (!StringTools.isNullOrEmpty(str)) {
                return Float.valueOf(str);
            }
        } catch (Exception e) {
        }
        return null;
    }

}
