package com.alkemytech.sophia.codman.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.GsonBuilder;

@XmlRootElement
@Entity(name="SophiaIsrcOpera")
@Table(name="sophia_isrc_opera")
public class SophiaIsrcOpera {

	@Id
	@Column(name="isrc", nullable=false)
	private String isrc;
	
	@Column(name="codice_opera", nullable=true)
	private String codiceOpera;
	
	@Column(name="origin", nullable=true)
	private String origin;

	@Column(name="idutil_source", nullable=false)
	private String idutilSource;

	@Column(name="iddsr_source", nullable=true)
	private String iddsrSource;
	
	@Column(name="kb_version", nullable=true)
	private Integer kbVersion;
	
	@Column(name="valid", nullable=true)
	private Integer valid;

	public String getIsrc() {
		return isrc;
	}

	public void setIsrc(String isrc) {
		this.isrc = isrc;
	}

	public String getCodiceOpera() {
		return codiceOpera;
	}

	public void setCodiceOpera(String codiceOpera) {
		this.codiceOpera = codiceOpera;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getIdutilSource() {
		return idutilSource;
	}

	public void setIdutilSource(String idutilSource) {
		this.idutilSource = idutilSource;
	}

	public String getIddsrSource() {
		return iddsrSource;
	}

	public void setIddsrSource(String iddsrSource) {
		this.iddsrSource = iddsrSource;
	}

	public Integer getKbVersion() {
		return kbVersion;
	}

	public void setKbVersion(Integer kbVersion) {
		this.kbVersion = kbVersion;
	}

	public Integer getValid() {
		return valid;
	}

	public void setValid(Integer valid) {
		this.valid = valid;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting().create().toJson(this);
	}

}
