package com.alkemytech.sophia.codman.rest;

import com.alkemytech.sophia.codman.dto.IdentifiedSongDTO;
import com.alkemytech.sophia.codman.dto.IdentifiedSongDsrDTO;
import com.alkemytech.sophia.codman.entity.IdentifiedSong;
import com.alkemytech.sophia.codman.entity.UnidentifiedSongDsr;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.alkemytech.sophia.codman.guice.UnidentifiedDataSource;
import com.alkemytech.sophia.codman.multimedialelocale.entity.UnidentifiedSong;
import com.alkemytech.sophia.codman.sso.SsoSessionAttrs;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.servlet.RequestScoped;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@RequestScoped
@Path("identifiedSong")
public class IdentifiedSongService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Gson gson;
	private final Provider<EntityManager> mcmdbProvider;
	private final Provider<EntityManager> unidentifiedProvider;
	private final KnowledgeBaseService knowledgeBaseService;
	private final HttpServletRequest request;

	@Inject
	protected IdentifiedSongService(@UnidentifiedDataSource Provider<EntityManager> unidentifiedProvider,
									@McmdbDataSource Provider<EntityManager> mcmdbProvider,
									KnowledgeBaseService knowledgeBaseService,
									Gson gson,
									HttpServletRequest request) {
		super();
		this.unidentifiedProvider = unidentifiedProvider;
		this.mcmdbProvider = mcmdbProvider;
		this.knowledgeBaseService = knowledgeBaseService;
		this.gson = gson;
		this.request = request;
	}

	@PUT
	@Path("provisional")
	@Produces("application/json")
	public Response provisional(IdentifiedSong identifiedSong) {
		
		final EntityManager mcmdbEM = mcmdbProvider.get();
		try {
			
			// create new provisional code
			mcmdbEM.getTransaction().begin();
			mcmdbEM.createNativeQuery("UPDATE codice_provvisorio SET id=LAST_INSERT_ID(id+1)")
					.executeUpdate();
			final BigInteger workCode = (BigInteger) mcmdbEM
					.createNativeQuery("SELECT LAST_INSERT_ID()")
					.getSingleResult();
			logger.debug("provisional: workCode " + workCode);
			final String siaeWorkCode = String.format("P%010d", workCode.longValue());
			logger.debug("provisional: siaeWorkCode " + siaeWorkCode);

			mcmdbEM
					.createNamedQuery("UnidentifiedSong.updateByUsernameAndHashId", UnidentifiedSong.class)
					.setParameter("hashId", identifiedSong.getHashId())
					.setParameter("username", request.getSession().getAttribute(SsoSessionAttrs.USERNAME))
					.setParameter("lastManualTime", Long.MAX_VALUE)
					.setParameter("lastAutoTime", Long.MAX_VALUE)
					.executeUpdate();

			mcmdbEM.getTransaction().commit();

			identifiedSong.setSiaeWorkCode(siaeWorkCode);

		} catch (Exception e) {
			if (mcmdbEM.getTransaction().isActive()) {
				mcmdbEM.getTransaction().rollback();
			}
			logger.error("provisional", e);
			return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR)
					.entity(gson.toJson(RestUtils.asMap("status", "KO",
							"errorMessage", "unespected error",
							"stackTrace", RestUtils.asString(e)))).build();
		}
		
		identifiedSong.setIdentificationType("manual");
		return identify(identifiedSong);
	}
	
	@PUT
	@Path("automatic")
	@Produces("application/json")
	public Response automatic(IdentifiedSong identifiedSong) {
		identifiedSong.setIdentificationType("automatic");
		return identify(identifiedSong);
	}
	
	@PUT
	@Path("manual")
	@Produces("application/json")
	public Response manual(IdentifiedSong identifiedSong) {

		//final EntityManager entityManager = mcmdbProvider.get();

		identifiedSong.setIdentificationType("manual");

//		entityManager.getTransaction().begin();

//		entityManager
//			.createNamedQuery("UnidentifiedSong.updateByUsernameAndHashId", UnidentifiedSong.class)
//			.setParameter("hashId", identifiedSong.getHashId())
//			.setParameter("username", request.getSession().getAttribute(SsoSessionAttrs.USERNAME))
//			.executeUpdate();

//		entityManager.getTransaction().commit();

		return identify(identifiedSong);
	}
	
	@PUT
	@Path("identify")
	@Produces("application/json")
	public Response identify(IdentifiedSong identifiedSong) {

		final EntityManager unidentifiedEM = unidentifiedProvider.get();
		try {

			// create data transfer object
			final IdentifiedSongDTO identifiedSongDTO = createDTO(unidentifiedEM, identifiedSong);
			if (null == identifiedSongDTO.getDsrs()) {
				logger.debug("identify: nessuna utilizzazione legata all'opera identificata [" +
						identifiedSong.getSiaeWorkCode() + "][" + identifiedSong.getTitle() + "]");
				return Response.status(HttpServletResponse.SC_NOT_FOUND)
						.entity(gson.toJson(RestUtils.asMap("status", "KO",
								"errorMessage", "no utilization found for given work"))).build();
			}

			// update knowledge base
			final Response response = knowledgeBaseService.identify(identifiedSongDTO);

			// search & insert or update identified_song
			unidentifiedEM.getTransaction().begin();
			final IdentifiedSong alreadyIdentifiedSong = unidentifiedEM
					.find(IdentifiedSong.class, identifiedSong.getHashId());
			if (null == alreadyIdentifiedSong) { // not found
				identifiedSong.setInsertTime(System.currentTimeMillis());
				if (HttpServletResponse.SC_OK == response.getStatus()) {
					identifiedSong.setSophiaUpdateTime(System.currentTimeMillis());
				}
				unidentifiedEM.persist(identifiedSong);
			} else { // already exists
				if (HttpServletResponse.SC_OK == response.getStatus()) {
					alreadyIdentifiedSong.setSophiaUpdateTime(System.currentTimeMillis());
				}
				alreadyIdentifiedSong.setSiaeWorkCode(identifiedSong.getSiaeWorkCode());
				unidentifiedEM.merge(alreadyIdentifiedSong);
			}
			
			// search & update unidentified_song
			final UnidentifiedSong unidentifiedSong = unidentifiedEM
					.find(UnidentifiedSong.class, identifiedSong.getHashId());
			if (null != unidentifiedSong) {
				if ("manual".equalsIgnoreCase(identifiedSong.getIdentificationType())) {
					unidentifiedSong.setLastManualTime(Long.MAX_VALUE);
				} else {
					unidentifiedSong.setLastAutoTime(Long.MAX_VALUE);
				}
				unidentifiedEM.merge(unidentifiedSong);
			}
			unidentifiedEM.getTransaction().commit();

			return response;

		} catch (Exception e) {
			if (unidentifiedEM.getTransaction().isActive()) {
				unidentifiedEM.getTransaction().rollback();
			}
			logger.error("identify", e);
			return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR)
					.entity(gson.toJson(RestUtils.asMap("status", "KO",
							"errorMessage", "unespected error",
							"stackTrace", RestUtils.asString(e)))).build();
		}

	}
	
	private IdentifiedSongDTO createDTO(EntityManager entityManager, IdentifiedSong identifiedSong) {
		
		final IdentifiedSongDTO identifiedSongDTO = new IdentifiedSongDTO();
		identifiedSongDTO.setHashId(identifiedSong.getHashId());
		identifiedSongDTO.setTitle(identifiedSong.getTitle());
		identifiedSongDTO.setArtists(identifiedSong.getArtists());
		identifiedSongDTO.setSiadaTitle(identifiedSong.getSiadaTitle());
		identifiedSongDTO.setSiadaArtists(identifiedSong.getSiadaArtists());
		identifiedSongDTO.setRoles(identifiedSong.getRoles());
		identifiedSongDTO.setSiaeWorkCode(identifiedSong.getSiaeWorkCode());
		identifiedSongDTO.setIdentificationType(identifiedSong.getIdentificationType());
		
		final List<UnidentifiedSongDsr> dsrRefs = entityManager
				.createQuery("select x from UnidentifiedSongDsr x where x.hashId = :hashId",
						UnidentifiedSongDsr.class)
				.setParameter("hashId", identifiedSong.getHashId())
				.getResultList();
		if (null != dsrRefs && !dsrRefs.isEmpty()) {
			final List<IdentifiedSongDsrDTO> identifiedSongDsrDTOs = new ArrayList<>(dsrRefs.size());
			for (UnidentifiedSongDsr dsrRef : dsrRefs) {
				IdentifiedSongDsrDTO identifiedSongDsrDTO = new IdentifiedSongDsrDTO();
				identifiedSongDsrDTO.setIdUtil(dsrRef.getIdUtil());
				identifiedSongDsrDTO.setIdDsr(dsrRef.getIdDsr());
				identifiedSongDsrDTO.setProprietaryId(dsrRef.getProprietaryId());
				identifiedSongDsrDTO.setAlbumTitle(dsrRef.getAlbumTitle());
				identifiedSongDsrDTO.setIsrc(dsrRef.getIsrc());
				identifiedSongDsrDTO.setIswc(dsrRef.getIswc());
				identifiedSongDsrDTO.setDsp(dsrRef.getDsp());			
				identifiedSongDsrDTOs.add(identifiedSongDsrDTO);
			}
			identifiedSongDTO.setDsrs(identifiedSongDsrDTOs);
		}		
		
		return identifiedSongDTO;
	}
	

}
