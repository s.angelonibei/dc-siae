package com.alkemytech.sophia.codman.performing.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class CodificaDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer idCodifica;
	private String candidate;
	private String codiceOperaApprovato;
	private String codiceOperaSuggerito;
	private BigDecimal confidenza;
	private Date dataApprovazione;
	private Date dataCodifica;
	private Date dataFineValidita;
	private BigDecimal distSecCandidata;
	private BigInteger nUtilizzazioni;
	private boolean preziosa;
	private String statoApprovazione;
	private String tipoApprovazione;
	private BigDecimal valoreEconomico;
	private CombanaDTO combanaDTO;
	private UtilizzazioneDTO utilizzazioneDTO;

	public CodificaDTO() {
	}

	public Integer getIdCodifica() {
		return this.idCodifica;
	}

	public void setIdCodifica(Integer idCodifica) {
		this.idCodifica = idCodifica;
	}

	public String getCandidate() {
		return this.candidate;
	}

	public void setCandidate(String candidate) {
		this.candidate = candidate;
	}

	public String getCodiceOperaApprovato() {
		return this.codiceOperaApprovato;
	}

	public void setCodiceOperaApprovato(String codiceOperaApprovato) {
		this.codiceOperaApprovato = codiceOperaApprovato;
	}

	public String getCodiceOperaSuggerito() {
		return this.codiceOperaSuggerito;
	}

	public void setCodiceOperaSuggerito(String codiceOperaSuggerito) {
		this.codiceOperaSuggerito = codiceOperaSuggerito;
	}

	public BigDecimal getConfidenza() {
		return this.confidenza;
	}

	public void setConfidenza(BigDecimal confidenza) {
		this.confidenza = confidenza;
	}

	public Date getDataApprovazione() {
		return this.dataApprovazione;
	}

	public void setDataApprovazione(Date dataApprovazione) {
		this.dataApprovazione = dataApprovazione;
	}

	public Date getDataCodifica() {
		return this.dataCodifica;
	}

	public void setDataCodifica(Date dataCodifica) {
		this.dataCodifica = dataCodifica;
	}

	public Date getDataFineValidita() {
		return this.dataFineValidita;
	}

	public void setDataFineValidita(Date dataFineValidita) {
		this.dataFineValidita = dataFineValidita;
	}

	public BigDecimal getDistSecCandidata() {
		return this.distSecCandidata;
	}

	public void setDistSecCandidata(BigDecimal distSecCandidata) {
		this.distSecCandidata = distSecCandidata;
	}

	public BigInteger getNUtilizzazioni() {
		return this.nUtilizzazioni;
	}

	public void setNUtilizzazioni(BigInteger nUtilizzazioni) {
		this.nUtilizzazioni = nUtilizzazioni;
	}

	public boolean getPreziosa() {
		return this.preziosa;
	}

	public void setPreziosa(boolean preziosa) {
		this.preziosa = preziosa;
	}

	public String getStatoApprovazione() {
		return this.statoApprovazione;
	}

	public void setStatoApprovazione(String statoApprovazione) {
		this.statoApprovazione = statoApprovazione;
	}

	public String getTipoApprovazione() {
		return this.tipoApprovazione;
	}

	public void setTipoApprovazione(String tipoApprovazione) {
		this.tipoApprovazione = tipoApprovazione;
	}

	public BigDecimal getValoreEconomico() {
		return this.valoreEconomico;
	}

	public void setValoreEconomico(BigDecimal valoreEconomico) {
		this.valoreEconomico = valoreEconomico;
	}

	public CombanaDTO getCombanaDTO() {
		return this.combanaDTO;
	}

	public void setCombanaDTO(CombanaDTO combanaDTO) {
		this.combanaDTO = combanaDTO;
	}

	public UtilizzazioneDTO getUtilizzazioneDTO() {
		return utilizzazioneDTO;
	}

	public void setUtilizzazioneDTO(UtilizzazioneDTO utilizzazioneDTO) {
		this.utilizzazioneDTO = utilizzazioneDTO;
	}
}