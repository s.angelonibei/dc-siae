package com.alkemytech.sophia.codman.entity.performing.dao;

import com.alkemytech.sophia.codman.entity.performing.EsecuzioneRicalcolo;
import com.alkemytech.sophia.codman.entity.performing.MovimentoContabile;
import com.alkemytech.sophia.codman.entity.performing.TracciamentoApplicativo;
import com.alkemytech.sophia.codman.entity.performing.security.ReportPage;

import java.util.List;

/**
 * Created by idilello on 6/14/16.
 */
public interface IEsecuzioneDAO {

    public List<EsecuzioneRicalcolo> getEsecuzioni();

    public ReportPage getListaPM(Long idEsecuzioneRicalcolo, Long idEvento, String numeroFattura, String voceIncasso, String numeroProgrammaMusicale, String inizioPeriodoContabile, String finePeriodoContabile, Integer page);

    public ReportPage getListaPMSospesi(Long idEsecuzioneRicalcolo, Long idEvento, String numeroFattura, String voceIncasso, String numeroProgrammaMusicale, String inizioPeriodoContabile, String finePeriodoContabile, Integer page,String type);

    public EsecuzioneRicalcolo findEsecuzioneByContabilitaSiada(Long contabilitaSiada);

    public EsecuzioneRicalcolo findEsecuzioneById(Long idEsecuzioneRicalcolo);

    public int setDataCongelamentoEsecuzione(Long elaborazioneSIADAId);

    public void trace(TracciamentoApplicativo tracciamentoApplicativo);
    
    public void deleteMovimentoSiada();

    public Long getNewEsecuzioneRicalcoloId();
    
    public Long getNumeroMovimentazioniAggregate(EsecuzioneRicalcolo esecuzione);
}
