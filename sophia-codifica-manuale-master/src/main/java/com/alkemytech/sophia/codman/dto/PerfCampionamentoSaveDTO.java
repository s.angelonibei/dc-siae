package com.alkemytech.sophia.codman.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PerfCampionamentoSaveDTO {

	private String mesePeriodoContabilita;
	private String annoPeriodoContabilita;
	private String dataInizioLavorazione;
	private String dataEstrazioneLotto;
	private String estrazioniRoma;
	private String estrazioniMilano;
	private String restoRoma1;
	private String restoMilano1;
	private String restoRoma2;
	private String restoMilano2;
	private String username;
	
	
	
	
	public PerfCampionamentoSaveDTO() {
		super();
	}
	public PerfCampionamentoSaveDTO(String mesePeriodoContabilita, String annoPeriodoContabilita,
			String dataInizioLavorazione, String dataEstrazioneLotto, String estrazioniRoma, String estrazioniMilano,
			String restoRoma1, String restoMilano1, String restoRoma2, String restoMilano2, String username) {
		super();
		this.mesePeriodoContabilita = mesePeriodoContabilita;
		this.annoPeriodoContabilita = annoPeriodoContabilita;
		this.dataInizioLavorazione = dataInizioLavorazione;
		this.dataEstrazioneLotto = dataEstrazioneLotto;
		this.estrazioniRoma = estrazioniRoma;
		this.estrazioniMilano = estrazioniMilano;
		this.restoRoma1 = restoRoma1;
		this.restoMilano1 = restoMilano1;
		this.restoRoma2 = restoRoma2;
		this.restoMilano2 = restoMilano2;
		this.username = username;
	}
	public String getMesePeriodoContabilita() {
		return mesePeriodoContabilita;
	}
	public void setMesePeriodoContabilita(String mesePeriodoContabilita) {
		this.mesePeriodoContabilita = mesePeriodoContabilita;
	}
	public String getAnnoPeriodoContabilita() {
		return annoPeriodoContabilita;
	}
	public void setAnnoPeriodoContabilita(String annoPeriodoContabilita) {
		this.annoPeriodoContabilita = annoPeriodoContabilita;
	}
	public String getDataInizioLavorazione() {
		return dataInizioLavorazione;
	}
	public void setDataInizioLavorazione(String dataInizioLavorazione) {
		this.dataInizioLavorazione = dataInizioLavorazione;
	}
	public String getDataEstrazioneLotto() {
		return dataEstrazioneLotto;
	}
	public void setDataEstrazioneLotto(String dataEstrazioneLotto) {
		this.dataEstrazioneLotto = dataEstrazioneLotto;
	}
	public String getEstrazioniRoma() {
		return estrazioniRoma;
	}
	public void setEstrazioniRoma(String estrazioniRoma) {
		this.estrazioniRoma = estrazioniRoma;
	}
	public String getEstrazioniMilano() {
		return estrazioniMilano;
	}
	public void setEstrazioniMilano(String estrazioniMilano) {
		this.estrazioniMilano = estrazioniMilano;
	}
	public String getRestoRoma1() {
		return restoRoma1;
	}
	public void setRestoRoma1(String restoRoma1) {
		this.restoRoma1 = restoRoma1;
	}
	public String getRestoMilano1() {
		return restoMilano1;
	}
	public void setRestoMilano1(String restoMilano1) {
		this.restoMilano1 = restoMilano1;
	}
	public String getRestoRoma2() {
		return restoRoma2;
	}
	public void setRestoRoma2(String restoRoma2) {
		this.restoRoma2 = restoRoma2;
	}
	public String getRestoMilano2() {
		return restoMilano2;
	}
	public void setRestoMilano2(String restoMilano2) {
		this.restoMilano2 = restoMilano2;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	
}
