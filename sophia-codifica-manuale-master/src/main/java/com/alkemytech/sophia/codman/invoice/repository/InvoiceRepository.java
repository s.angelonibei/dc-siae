package com.alkemytech.sophia.codman.invoice.repository;


import com.alkemytech.sophia.codman.dto.InvoiceDTO;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

import javax.persistence.EntityManager;
import java.util.List;

@Singleton
public class InvoiceRepository implements InvoiceRepositoryInterface {

    private final Provider<EntityManager> provider;

    @Inject
    protected InvoiceRepository(@McmdbDataSource Provider<EntityManager> provider) {
        super();
        this.provider = provider;

    }

    @Override
    public InvoiceDTO newInvoice(InvoiceDTO newInvoice) {
        return null;
    }

    @Override
    public List<InvoiceDTO> searchInvoice(InvoiceSearchParameters searchParameters) {
        return null;
    }

    @Override
    public void updateInvoice(InvoiceDTO invoice) {

    }
}
