package com.alkemytech.sophia.codman.entity;
import java.util.ArrayList;
import java.util.List;

public class AdapterSettings {
			
	private long id;
	
	private String key;
	
	private Integer ordinal;
	
	private String settingType;	

	private String label;
	
	private String valueType;
	
	private String value;
		
	private String relation;

	private String caption;
	
	private List<AdapterSettings> thresholds = new ArrayList<AdapterSettings>();	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Integer getOrdinal() {
		return ordinal;
	}

	public void setOrdinal(Integer ordinal) {
		this.ordinal = ordinal;
	}

	public String getSettingType() {
		return settingType;
	}

	public void setSettingType(String settingType) {
		this.settingType = settingType;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getValueType() {
		return valueType;
	}

	public void setValueType(String valueType) {
		this.valueType = valueType;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public List<AdapterSettings> getThresholds() {
		return thresholds;
	}

	public void setThresholds(List<AdapterSettings> thresholds) {
		this.thresholds = thresholds;
	}
	
}