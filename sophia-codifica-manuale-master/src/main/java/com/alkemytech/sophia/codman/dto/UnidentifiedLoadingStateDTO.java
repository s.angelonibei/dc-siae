package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UnidentifiedLoadingStateDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;

	private String description;
	
	public UnidentifiedLoadingStateDTO(String id, String description) {
		super();
		this.id = id;
		this.description = description;
	}

	public UnidentifiedLoadingStateDTO(String id) {
		super();
		this.id = id;
		this.description = id;
	}

	public UnidentifiedLoadingStateDTO() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}