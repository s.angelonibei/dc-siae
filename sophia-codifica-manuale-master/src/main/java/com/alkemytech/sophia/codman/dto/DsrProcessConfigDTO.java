package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;

public class DsrProcessConfigDTO implements Serializable{

	private static final long serialVersionUID = 7324047010705416496L;
	
	private Integer idDsrProcessConfig;
	private Integer priority;
	private String dsp;
	private String dspName;
	private String utilizationType;
	private String utilizationName;
	private String country;
	private String countryName;
	private String idDsrProcess;
	private String processName;
	private String processDescription;
	private String configuration;
	
	
	public DsrProcessConfigDTO(){}
	
	public DsrProcessConfigDTO(Object[] obj){
		
		if(null != obj && obj.length == 12){
			try{
				idDsrProcessConfig = (Integer)obj[0];
				priority = (Integer)obj[1];
				dsp = (String)obj[2];
				dspName = (String)obj[3];
				utilizationType = (String)obj[4];
				utilizationName = (String)obj[5];
				country = (String)obj[6];
				countryName = (String)obj[7];
				idDsrProcess = (String)obj[8];
				processName = (String)obj[9];
				processDescription = (String)obj[10];
				configuration = (String)obj[11];
			}catch(Throwable e){
				e.printStackTrace();
			}
		}
	}
	
	public Integer getIdDsrProcessConfig() {
		return idDsrProcessConfig;
	}
	public void setIdDsrProcessConfig(Integer idDsrProcessConfig) {
		this.idDsrProcessConfig = idDsrProcessConfig;
	}
	public Integer getPriority() {
		return priority;
	}
	public void setPriority(Integer priority) {
		this.priority = priority;
	}
	public String getDsp() {
		return dsp;
	}
	public void setDsp(String dsp) {
		this.dsp = dsp;
	}
	
	public String getDspName() {
		return dspName;
	}

	public void setDspName(String dspName) {
		this.dspName = dspName;
	}

	public String getUtilizationType() {
		return utilizationType;
	}
	public void setUtilizationType(String utilizationType) {
		this.utilizationType = utilizationType;
	}
	
	public String getUtilizationName() {
		return utilizationName;
	}

	public void setUtilizationName(String utilizationName) {
		this.utilizationName = utilizationName;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	
	public String getIdDsrProcess() {
		return idDsrProcess;
	}
	public void setIdDsrProcess(String idDsrProcess) {
		this.idDsrProcess = idDsrProcess;
	}
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public String getProcessDescription() {
		return processDescription;
	}
	public void setProcessDescription(String processDescription) {
		this.processDescription = processDescription;
	}
	public String getConfiguration() {
		return configuration;
	}
	public void setConfiguration(String configuration) {
		this.configuration = configuration;
	}

}
