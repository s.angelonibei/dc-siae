package com.alkemytech.sophia.codman.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Data
@Table(name = "MM_INGESTION_CODIFICATI")
@NamedQueries({@NamedQuery(name = "MmIngestionCodificato.all", query = "select x from MmIngestionCodificato x order by x.dataCaricamento desc"),
@NamedQuery(name = "MmIngestionCodificato.checkElaborating", query = "select count(x) from MmIngestionCodificato x where x.statoIngestion = 'IN CORSO'"),
@NamedQuery(name = "MmIngestionCodificato.ByNameAndTime", query = "select x from MmIngestionCodificato x where x.nomeFile = :nomeFile order by x.dataCaricamento desc")})
public class MmIngestionCodificato implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID_FILE", nullable = false)
    private Integer ID;

    @Column(name = "NOME_FILE", nullable = false)
    private String nomeFile;

    @Column(name = "DATA_CARICAMENTO")
    private Timestamp dataCaricamento;

    @Column(name = "STATO_INGESTION")
    private String statoIngestion;

    @Column(name = "RIGHE_ELABORATE")
    private Integer righeElaborate = 0;

    @Column(name = "RIGHE_TOTALI")
    private Integer righeTotali;

    @Column(name = "DESCRIZIONE_ERRORE")
    private String descrizioneErrore;

    @Column(name = "PATH_S3_CSV")
    private String pathCsv;

    @Column(name = "PATH_S3_CSV_SCARTI")
    private String pathCsvScarti;

    @Column(name = "DATA_AGGIORNAMENTO")
    private Timestamp dataAggiornamento;
    
}