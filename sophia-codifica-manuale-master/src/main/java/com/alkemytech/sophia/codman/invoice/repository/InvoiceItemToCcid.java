package com.alkemytech.sophia.codman.invoice.repository;


import com.alkemytech.sophia.codman.entity.CCIDMetadata;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;

@XmlRootElement
@Entity
@Table(name = "INVOICE_ITEM_TO_CCID")
public class InvoiceItemToCcid {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "ID_CCID_METADATA", referencedColumnName = "ID_CCID_METADATA")
	private CCIDMetadata ccidMedata;

	@ManyToOne
	@JoinColumn(name = "ID_INVOICE_ITEM", referencedColumnName = "ID_ITEM")
	private InvoiceItem invoiceItem;

	@Column(name="VALORE", nullable=false)
	private BigDecimal valore;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public CCIDMetadata getIdCcidMedata() {
		return ccidMedata;
	}

	public void setCcidMedata(CCIDMetadata ccidMedata) {
		this.ccidMedata = ccidMedata;
	}

	public InvoiceItem getInvoiceItem() {
		return invoiceItem;
	}

	public void setInvoiceItem(InvoiceItem invoiceItem) {
		this.invoiceItem = invoiceItem;
	}

	public BigDecimal getValore() {
		return valore;
	}

	public void setValore(BigDecimal valore) {
		this.valore = valore;
	}
	
	
}
