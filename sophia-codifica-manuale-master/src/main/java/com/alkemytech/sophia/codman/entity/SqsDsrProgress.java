package com.alkemytech.sophia.codman.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.GsonBuilder;

@XmlRootElement
@Entity(name="SqsDsrProgress")
@Table(name="SQS_DSR_PROGRESS")
@IdClass(SqsDsrProgressId.class)
public class SqsDsrProgress {
	
	@Id
	@Column(name="IDDSR", nullable=false)
	private String idDsr;
	
	@Id
	@Column(name="SERVICE_NAME", nullable=false)
	private String serviceName;

	@Id
	@Column(name="QUEUE_TYPE", nullable=false)
	private String queueType;

	@Column(name="INSERT_TIME", nullable=false)
	private Date insertTime;
	
	@Column(name="NUMBER", nullable=false)
	private Integer number;
	
	public String getIdDsr() {
		return idDsr;
	}

	public void setIdDsr(String idDsr) {
		this.idDsr = idDsr;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getQueueType() {
		return queueType;
	}

	public void setQueueType(String queueType) {
		this.queueType = queueType;
	}

	public Date getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(Date insertTime) {
		this.insertTime = insertTime;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}
	
}
