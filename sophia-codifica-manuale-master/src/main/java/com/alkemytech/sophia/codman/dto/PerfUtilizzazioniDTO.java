package com.alkemytech.sophia.codman.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PerfUtilizzazioniDTO {

	private Long id;
	private Long idProgrammaMusicale;
	private String codiceOpera;
	private String titoloComposizione;
	private String compositore;
	private String compositori;
	private Long numeroProgrammaMusicale;
	private Double durata;
	private String durataInferiore30sec;
	private String autori;
	private String interpreti;
	private String flagCodifica;
	private String flagPubblicoDominio;
	private String flagCedolaEsclusa;
	private String combinazioneCodifica;
	private String flagMagNonConcessa; 
	private Double nettoMaturato; 
	private String dataPrimaCodifica;
	private String tipoAccertamento; 
	private Double nettoOriginario;
	
	
	
	public PerfUtilizzazioniDTO() {
		super();
	}

	public PerfUtilizzazioniDTO(Long id, Long idProgrammaMusicale, String codiceOpera, String titolo, String compositore,
			String compositori, Long numeroProgrammaMusicale, Double durataEsecuzione, String menodi30sec,
			String autori, String interpreti, String elaboratorePubblicoDominio, String combinazionecodifica,
			String flagMagNonConcessa, Double nettoMaturato, String dataPrimaCodifica, String tipoAccertamento,
			String flagCodifica, Double nettoOriginario,String flagCedolaEsclusa) {
		super();
		this.id = id;
		this.idProgrammaMusicale = idProgrammaMusicale;
		this.codiceOpera = codiceOpera;
		this.titoloComposizione = titolo;
		this.compositore = compositore;
		this.compositori = compositori;
		this.numeroProgrammaMusicale = numeroProgrammaMusicale;
		this.durata = durataEsecuzione;
		this.durataInferiore30sec = menodi30sec;
		this.autori = autori;
		this.interpreti = interpreti;
		this.flagPubblicoDominio = elaboratorePubblicoDominio;
		this.combinazioneCodifica = combinazionecodifica;
		this.flagMagNonConcessa = flagMagNonConcessa;
		this.nettoMaturato = nettoMaturato;
		this.dataPrimaCodifica = dataPrimaCodifica;
		this.tipoAccertamento = tipoAccertamento;
		this.flagCodifica = flagCodifica;
		this.nettoOriginario=nettoOriginario;
		this.flagCedolaEsclusa=flagCedolaEsclusa;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdProgrammaMusicale() {
		return idProgrammaMusicale;
	}

	public void setIdProgrammaMusicale(Long idProgrammaMusicale) {
		this.idProgrammaMusicale = idProgrammaMusicale;
	}

	public String getCodiceOpera() {
		return codiceOpera;
	}

	public void setCodiceOpera(String codiceOpera) {
		this.codiceOpera = codiceOpera;
	}

	public String getTitolo() {
		return titoloComposizione;
	}

	public void setTitolo(String titolo) {
		this.titoloComposizione = titolo;
	}

	public String getCompositore() {
		return compositore;
	}

	public void setCompositore(String compositore) {
		this.compositore = compositore;
	}

	public String getCompositori() {
		return compositori;
	}

	public void setCompositori(String compositori) {
		this.compositori = compositori;
	}

	public Long getNumeroProgrammaMusicale() {
		return numeroProgrammaMusicale;
	}

	public void setNumeroProgrammaMusicale(Long numeroProgrammaMusicale) {
		this.numeroProgrammaMusicale = numeroProgrammaMusicale;
	}

	public Double getDurataEsecuzione() {
		return durata;
	}

	public void setDurataEsecuzione(Double durataEsecuzione) {
		this.durata = durataEsecuzione;
	}

	public String getMenodi30sec() {
		return durataInferiore30sec;
	}

	public void setMenodi30sec(String menodi30sec) {
		this.durataInferiore30sec = menodi30sec;
	}

	public String getAutori() {
		return autori;
	}

	public void setAutori(String autori) {
		this.autori = autori;
	}

	public String getInterpreti() {
		return interpreti;
	}

	public void setInterpreti(String interpreti) {
		this.interpreti = interpreti;
	}

	public String getElaboratorePubblicoDominio() {
		return flagPubblicoDominio;
	}

	public void setElaboratorePubblicoDominio(String elaboratorePubblicoDominio) {
		this.flagPubblicoDominio = elaboratorePubblicoDominio;
	}

	public String getCombinazioneCodifica() {
		return combinazioneCodifica;
	}

	public void setCombinazioneCodifica(String combinazionecodifica) {
		this.combinazioneCodifica = combinazionecodifica;
	}

	public String getFlagMagNonConcessa() {
		return flagMagNonConcessa;
	}

	public void setFlagMagNonConcessa(String flagMagNonConcessa) {
		this.flagMagNonConcessa = flagMagNonConcessa;
	}

	public Double getNettomaturato() {
		return nettoMaturato;
	}

	public void setNettomaturato(Double nettomaturato) {
		this.nettoMaturato = nettomaturato;
	}

	public String getDataPrimaCodifica() {
		return dataPrimaCodifica;
	}

	public void setDataPrimaCodifica(String dataPrimaCodifica) {
		this.dataPrimaCodifica = dataPrimaCodifica;
	}

	public String getTipoAccertamento() {
		return tipoAccertamento;
	}

	public void setTipoAccertamento(String tipoAccertamento) {
		this.tipoAccertamento = tipoAccertamento;
	}

	public String getFlagCodifica() {
		return flagCodifica;
	}

	public void setFlagCodifica(String flagCodifica) {
		this.flagCodifica = flagCodifica;
	}

	public Double getNettoMaturato() {
		return nettoOriginario;
	}

	public void setNettoMaturato(Double nettoOriginario) {
		this.nettoOriginario = nettoOriginario;
	}

	public String getTitoloComposizione() {
		return titoloComposizione;
	}

	public void setTitoloComposizione(String titoloComposizione) {
		this.titoloComposizione = titoloComposizione;
	}

	public Double getDurata() {
		return durata;
	}

	public void setDurata(Double durata) {
		this.durata = durata;
	}

	public String getDurataInferiore30sec() {
		return durataInferiore30sec;
	}

	public void setDurataInferiore30sec(String durataInferiore30sec) {
		this.durataInferiore30sec = durataInferiore30sec;
	}

	public String getFlagPubblicoDominio() {
		return flagPubblicoDominio;
	}

	public void setFlagPubblicoDominio(String flagPubblicoDominio) {
		this.flagPubblicoDominio = flagPubblicoDominio;
	}

	public String getFlagCedolaEsclusa() {
		return flagCedolaEsclusa;
	}

	public void setFlagCedolaEsclusa(String flagCedolaEsclusa) {
		this.flagCedolaEsclusa = flagCedolaEsclusa;
	}

	public Double getNettoOriginario() {
		return nettoOriginario;
	}

	public void setNettoOriginario(Double nettoOriginario) {
		this.nettoOriginario = nettoOriginario;
	}

	@Override
	public String toString() {
		return "PerfUtilizzazioniDTO [idProgrammaMusicale=" + idProgrammaMusicale 
				+ ", codiceOpera=" + codiceOpera 
				+ ", titolo=" + titoloComposizione 
				+ ", compositore=" + compositore 
				+ ", compositori=" + compositori
				+ ", numeroProgrammaMusicale=" + numeroProgrammaMusicale 
				+ ", durataEsecuzione=" + durata
				+ ", menodi30sec=" + durataInferiore30sec + ", autori=" + autori + ", interpreti=" + interpreti
				+ ", elaboratorePubblicoDominio=" + flagPubblicoDominio + ", combinazionecodifica="
				+ combinazioneCodifica + ", flagMagNonConcessa=" + flagMagNonConcessa + ", nettoMaturato="
				+ nettoMaturato + ", dataPrimaCodifica=" + dataPrimaCodifica + ", tipoAccertamento=" + tipoAccertamento
				+ ", flagCodifica=" + flagCodifica + ", nettoOriginario=" + nettoOriginario + "]";
	}

	
}
