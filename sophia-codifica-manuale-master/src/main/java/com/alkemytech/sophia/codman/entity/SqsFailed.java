package com.alkemytech.sophia.codman.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity(name="SqsFailed")
@Table(name="SQS_FAILED")
public class SqsFailed {
	
	@Id
	@GeneratedValue
	@Column(name="ID", nullable=false)
	private Long id;
	
	@Column(name="QUEUE_NAME", nullable=false)
	private String queueName;
	
	@Column(name="RECEIVE_TIME", nullable=false)
	private Date receiveTime;
	
	@Column(name="MESSAGE_JSON", nullable=true)
	private String messageJson;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getQueueName() {
		return queueName;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

	public Date getReceiveTime() {
		return receiveTime;
	}

	public void setReceiveTime(Date receiveTime) {
		this.receiveTime = receiveTime;
	}

	public String getMessageJson() {
		return messageJson;
	}

	public void setMessageJson(String messageJson) {
		this.messageJson = messageJson;
	}
	
}
