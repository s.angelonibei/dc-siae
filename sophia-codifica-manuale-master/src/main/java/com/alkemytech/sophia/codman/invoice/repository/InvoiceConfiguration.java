package com.alkemytech.sophia.codman.invoice.repository;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import org.yaml.snakeyaml.Yaml;

public class InvoiceConfiguration {

	
	private String direzioneSede;
	private String canale;
	private String cod;
	private Map<String,String> tipoDoc;
	private String ufficio;
	private Map<String,String> invoiceStatus;
	private String esbPath;
	
	public String getDirezioneSede() {
		return direzioneSede;
	}
	public void setDirezioneSede(String direzioneSede) {
		this.direzioneSede = direzioneSede;
	}
	public String getCanale() {
		return canale;
	}
	public void setCanale(String canale) {
		this.canale = canale;
	}
	public String getCod() {
		return cod;
	}
	public void setCod(String cod) {
		this.cod = cod;
	}
	
	public Map<String, String> getTipoDoc() {
		return tipoDoc;
	}
	public void setTipoDoc(Map<String, String> tipoDoc) {
		this.tipoDoc = tipoDoc;
	}
	public String getUfficio() {
		return ufficio;
	}
	public void setUfficio(String ufficio) {
		this.ufficio = ufficio;
	}
	public Map<String, String> getInvoiceStatus() {
		return invoiceStatus;
	}
	public void setInvoiceStatus(Map<String, String> invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}
	public String getEsbPath() {
		return esbPath;
	}
	public void setEsbPath(String esbPath) {
		this.esbPath = esbPath;
	}
	
}
