package com.alkemytech.sophia.codman.rest.performing.serviceimpl;

import com.alkemytech.sophia.codman.dto.performing.MusicProviderDTO;
import com.alkemytech.sophia.codman.dto.performing.PalinsestoDTO;
import com.alkemytech.sophia.codman.dto.performing.RsUtenteDTO;
import com.alkemytech.sophia.codman.entity.performing.dao.RadioInStoreDAO;
import com.alkemytech.sophia.codman.rest.performing.service.IRadioInStoreService;
import com.alkemytech.sophia.performing.model.*;
import com.google.inject.Inject;

import java.util.List;

public class RadioInStoreService implements IRadioInStoreService {

	private RadioInStoreDAO radioInStoreDAO;

	@Inject
	public RadioInStoreService(RadioInStoreDAO radioInStoreDAO) {
		this.radioInStoreDAO = radioInStoreDAO;
	}

	@Override
	public MusicProviderDTO getMusicProviderById(Long id) {
		return radioInStoreDAO.getMusicProviderById(id);
	}

	@Override
	public List<MusicProviderDTO> getMusicProviderList() {
		return radioInStoreDAO.getMusicProviderList();
	}

	@Override
	public List<MusicProviderDTO> getMusicProviderListByName(String nome) {
		return radioInStoreDAO.getMusicProviderListByName(nome);
	}

	@Override
	public List<PerfPalinsesto> getPalinsestiListByMp(Long id, Long idPalinsesto) {
		return radioInStoreDAO.getPalinsestiListByMp(id, idPalinsesto);
	}

	@Override
	public List<PerfPalinsesto> getPalinsestiList() {
		return radioInStoreDAO.getPalinsestiList();
	}

	@Override
	public List<PerfPuntoVendita> getPuntiVenditaByPalinsesto(Long idPalinsesto, Long id) {
		return radioInStoreDAO.getPuntiVenditaByPalinsesto(id, idPalinsesto);
	}

	@Override
	public List<PerfRsUtente> getUsers(Long idMusicProvider) {
		return radioInStoreDAO.getUsers(idMusicProvider);
	}

	@Override
	public Integer insertMusicProvider(MusicProviderDTO musicProviderDTO) {
		return radioInStoreDAO.insertMusicProvider(musicProviderDTO);
	}

	@Override
	public Integer insertRsUtente(RsUtenteDTO rsUtenteDTO) {

		return radioInStoreDAO.insertRsUtente(rsUtenteDTO);

	}

	@Override
	public Integer updateRsUtente(RsUtenteDTO rsUtenteDTO) {
		return radioInStoreDAO.updateRsUtente(rsUtenteDTO);
	}

	@Override
	public Integer insertPalinsesto(PalinsestoDTO palinsestoDTO) {
		return radioInStoreDAO.insertPalinsesto(palinsestoDTO);
	}

	@Override
	public Integer updateMusicProvider(MusicProviderDTO musicProviderDTO) {
		return radioInStoreDAO.updateMusicProvider(musicProviderDTO);
	}
	@Override
	public Integer updatePalinsesto(PalinsestoDTO palinsestoDTO) {
		return radioInStoreDAO.updatePalinsesto(palinsestoDTO);
	}

	@Override
	public List<PerfPalinsestoStorico> getStoricoPalinsesto(Long idPalinsesto) {	
		return radioInStoreDAO.getStoricoPalinsesto(idPalinsesto);
	}

	@Override
	public List<PerfRsUtilizationFile> getUtilizationFile(Long idMusicProvider, Long idPalinsesto, Long anno,
                                                          List<Long> mesi) {
		return radioInStoreDAO.getUtilizationFile(idMusicProvider,idPalinsesto,anno,mesi);
	}

	@Override
	public PerfRsUtilizationFile save(PerfRsUtilizationFile rsUtilizationFile) {
		return radioInStoreDAO.save(rsUtilizationFile);
	}
}
