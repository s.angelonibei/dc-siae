package com.alkemytech.sophia.codman.sso;

import com.alkemytech.sophia.codman.entity.RolePermission;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.alkemytech.sophia.common.tools.StringTools;
import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import it.siae.aas.UtenteBackOffice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

@Singleton
public class AasAuthenticationFilter extends DefaultAuthenticationModule implements Filter {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Provider<EntityManager> entityManagerProvider;
	private final String ssoToken;
	private final String ssoLoginUrl;
	private final String ssoWsEndpoint;
	private final String ssoApplicationCode;

	@Inject
	protected AasAuthenticationFilter(@Named("configuration") Properties configuration,
			@McmdbDataSource Provider<EntityManager> entityManagerProvider,
			@Named("sso.aas.token") String ssoToken,
			@Named("sso.aas.ws.endpoint") String ssoWsEndpoint,
			@Named("sso.aas.login.url") String ssoLoginUrl,
			@Named("sso.aas.application.code") String ssoApplicationCode) {
		super(configuration);
		this.entityManagerProvider = entityManagerProvider;
		this.ssoToken = ssoToken;
		this.ssoLoginUrl = ssoLoginUrl;
		this.ssoWsEndpoint = ssoWsEndpoint;
		this.ssoApplicationCode = ssoApplicationCode;
	}
		
	@Override
	public void init(FilterConfig config) throws ServletException {

	}

	@Override
	public void destroy() {
		
	}

	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		final HttpServletRequest httpRequest = (HttpServletRequest) request;
		final HttpServletResponse httpResponse = (HttpServletResponse) response;
		
		if (isDefaultIP(httpRequest)) {
			
			HttpSession httpSession = httpRequest.getSession(false);
			if (null == httpSession || // session not found
					StringTools.isNullOrEmpty((String) httpSession.getAttribute(SsoSessionAttrs.USERNAME))) { // username not found
				
				// create and initialize session
				httpSession = httpRequest.getSession(true);
				httpSession.setAttribute(SsoSessionAttrs.DENOMINAZIONE, getSsoDefaultUser());
				httpSession.setAttribute(SsoSessionAttrs.EMAIL, getSsoDefaultUser() + "@siae.it");
				httpSession.setAttribute(SsoSessionAttrs.PP_TIPO, "");
				httpSession.setAttribute(SsoSessionAttrs.PP_SEPRAG, "");
				httpSession.setAttribute(SsoSessionAttrs.PP_DESCRIZIONE, "");
				httpSession.setAttribute(SsoSessionAttrs.STATO_PASSWORD, "");
				httpSession.setAttribute(SsoSessionAttrs.TOKEN_ID, "");
				httpSession.setAttribute(SsoSessionAttrs.USERNAME, getSsoDefaultUser());
				httpSession.setAttribute(SsoSessionAttrs.GROUPS, getSsoDefaultGroups());
				httpSession.setAttribute(SsoSessionAttrs.LAST_CHECK, Long.valueOf(System.currentTimeMillis()));
				
				// inject user permission(s) into session
				final EntityManager entityManager = entityManagerProvider.get();
				final List<RolePermission> permissions = (List<RolePermission>) entityManager
					.createQuery("select x from RolePermission x where x.role in :role", RolePermission.class)
					.setParameter("role", getSsoDefaultGroups())
					.getResultList();
				for (RolePermission permission : permissions) {
					httpSession.setAttribute(permission.getPermission(), permission.getPermission());
				}

			} else { // session found
			
				final Long lastCheckTokenMillis = (Long) httpSession.getAttribute(SsoSessionAttrs.LAST_CHECK);
				if (null == lastCheckTokenMillis ||
						System.currentTimeMillis() - lastCheckTokenMillis > TimeUnit.MINUTES.toMillis(10)) {

					// update session
					httpSession.setAttribute(SsoSessionAttrs.LAST_CHECK, Long.valueOf(System.currentTimeMillis()));

				}
				
			}
			
		} else {
			
			// Documentazione Integrazione Unique Login (AASWS)
			//
			// Considerato il wsdl del servizio di login centralizzato “AuthenticationService.wsdl”, ricordare
			// che, nel caso in questione, i metodi di interesse sono i seguenti:
			// • CheckToken: per la verifica della validità del token di autenticazione
			// • GetUserData: per il recupero dei dati utente (compreso il seprag)
			// • GetUserGroups: per recuperare la lista dei sottogruppi applicativi (ldap) a cui appartiene
			//   l’utente. 
			//
			// A grandi linee il flusso del processo di autenticazione sarà il seguente: [...]
			
			HttpSession httpSession = httpRequest.getSession(false);
			if (null == httpSession || // session not found
					StringTools.isNullOrEmpty((String) httpSession.getAttribute(ssoToken))) { // authentication token not found
				
				// • Se arriva una richiesta da parte di un utente non autenticato, si verifica la presenza 
				//   del token di autenticazione in query string (nome parametro “SessionID”). [...]
				final String authenticationToken = httpRequest.getParameter(ssoToken);
				logger.debug("doFilter: authenticationToken [" + authenticationToken + "]");

				// • Se il parametro non è presente deve essere fatta una redirect verso il portale Intranet 
				//   Menusiae (per dipendenti) oppure extranet www.servizi.siae (per Mandatarie), passando un
				//   parametro in query string, denominato “ReturnURL” e valorizzato con un codice che assegneremo
				//   all’applicativo (es: http://www.servizi.siae?ReturnURL=CODICE). [...]
				
				if (Strings.isNullOrEmpty(authenticationToken)) {
					// interrupt filter chain and redirect request
					httpResponse.sendRedirect(ssoLoginUrl);
					return;
				}
				
				final AasAuthenticationWS aasAuthentication = 
						new AasAuthenticationWS(ssoWsEndpoint);
				
				// • Se invece il parametro “SessionID” è presente, viene richiamato il metodo CheckToken del
				//   web service per verificarne la validità (a questo metodo va passato anche il suddetto codice
				//   per motivi di log e l’ip dell’utente per motivi di sicurezza). [...]

				String userName = null;
				try {
					userName = aasAuthentication.checkToken(authenticationToken, 
							ssoApplicationCode, httpRequest.getRemoteAddr());
				} catch (Exception e) {
					logger.error("doFilter", e);
				}
				logger.debug("doFilter: userName [" + userName + "]");
				if (Strings.isNullOrEmpty(userName)) {
					logger.debug("doFilter: invalid authentication token");
					// interrupt filter chain and redirect request
					httpResponse.sendRedirect(ssoLoginUrl);
					return;
				}
				
				// • Se il token è valido si ottiene in risposta lo userName. A questo punto si può invocare il
				//   metodo GetUserData per recuperare le informazioni utente ed il metodo GetUserGroups per 
				//   verificare, nell’ambito dell’applicazione, l’appartenenza ad un sottogruppo applicativo. [...]

				UtenteBackOffice user = null;
				List<String> groups = null;
				try {
					user = aasAuthentication.getUserData(userName);
					groups = aasAuthentication.getUserGroups(userName, ssoApplicationCode);
				} catch (Exception e) {
					logger.error("doFilter", e);
				}
				logger.debug("doFilter: user [" + user + "]");
				logger.debug("doFilter: groups [" + groups + "]");
				if (null == user || null == groups || groups.isEmpty()) {
					logger.debug("doFilter: error retrieving user data");
					// interrupt filter chain and redirect request
					httpResponse.sendRedirect(ssoLoginUrl);
					return;
				}
				
				// create and initialize session
				httpSession = httpRequest.getSession(true);
				httpSession.setAttribute(SsoSessionAttrs.DENOMINAZIONE, user.getDenominazione());
				httpSession.setAttribute(SsoSessionAttrs.EMAIL, user.getEmail());
				httpSession.setAttribute(SsoSessionAttrs.PP_TIPO, user.getPuntoPeriferico().getTipo());
				httpSession.setAttribute(SsoSessionAttrs.PP_SEPRAG, user.getPuntoPeriferico().getCodiceSeprag());
				httpSession.setAttribute(SsoSessionAttrs.PP_DESCRIZIONE, user.getPuntoPeriferico().getDescrizione());
				httpSession.setAttribute(SsoSessionAttrs.STATO_PASSWORD, user.getStatoPassword());
				httpSession.setAttribute(SsoSessionAttrs.TOKEN_ID, user.getTokenID());
				httpSession.setAttribute(SsoSessionAttrs.USERNAME, user.getUserName());
				httpSession.setAttribute(SsoSessionAttrs.GROUPS, groups);
				httpSession.setAttribute(SsoSessionAttrs.LAST_CHECK, Long.valueOf(System.currentTimeMillis()));
				httpSession.setAttribute(ssoToken, authenticationToken);
				
				// inject user permission(s) into session
				final EntityManager entityManager = entityManagerProvider.get();
				final List<RolePermission> permissions = (List<RolePermission>) entityManager
					.createQuery("select x from RolePermission x where x.role in :role", RolePermission.class)
					.setParameter("role", groups)
					.getResultList();
				for (RolePermission permission : permissions) {
					httpSession.setAttribute(permission.getPermission(), permission.getPermission());
				}

			} else { // session found
			
				// • Sarebbe anche necessario implementare una procedura di refresh del token. Per fare 
				//   questo è possibile eseguire il CheckToken con una certa cadenza, ad esempio nei nostri
				//   applicativi eseguiamo un CheckToken ogni 10 minuti. [...]
				
				final Long lastCheckTokenMillis = (Long) httpSession.getAttribute(SsoSessionAttrs.LAST_CHECK);
				if (null == lastCheckTokenMillis ||
						System.currentTimeMillis() - lastCheckTokenMillis > TimeUnit.MINUTES.toMillis(10)) {
					logger.debug("doFilter: refreshing authentication token after " + 
							(null == lastCheckTokenMillis ? 0 : System.currentTimeMillis() - lastCheckTokenMillis));

					final AasAuthenticationWS aasAuthentication = 
							new AasAuthenticationWS(ssoWsEndpoint);

					final String authenticationToken = (String) httpSession.getAttribute(ssoToken);
					String userName = null;
					try {
						userName = aasAuthentication.checkToken(authenticationToken, 
								ssoApplicationCode, httpRequest.getRemoteAddr());
					} catch (Exception e) {
						logger.error("doFilter", e);
					}
					logger.debug("doFilter: userName [" + userName + "]");
					if (null == userName || !userName.equals(httpSession.getAttribute(SsoSessionAttrs.USERNAME))) {
						logger.debug("doFilter: invalid authentication token");
						// invalidate session
						httpSession.invalidate();
						// interrupt filter chain and redirect request
						httpResponse.sendRedirect(ssoLoginUrl);
						return;
					}

					// update session
					httpSession.setAttribute(SsoSessionAttrs.LAST_CHECK, Long.valueOf(System.currentTimeMillis()));
				}
				
			}
			
			// • In fase di logout deve essere chiusa la sessione applicativa e l’utente deve essere rimandato
			//   sulla home page del portale intranet.
			//
			// Inoltre sarebbe opportuno rendere configurabili gli URL da richiamare ed il nome dei parametri
			// (“SessionID” e “ReturnURL”). [...]

		}
				
		// pass the request along the filter chain
		chain.doFilter(request, response);
	}

}
