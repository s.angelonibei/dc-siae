package com.alkemytech.sophia.codman.dto.performing;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ConfigurazioniPeriodoDTO {

	private String periodo;
	
	private Date competenzaDa;

	private Date competenzaA;
	
	private String dataValidita;
	
	private List<RegolaRipartizioneDTO> regoleRipartizioneDTO;

	public ConfigurazioniPeriodoDTO() {
		super();
	}

	public ConfigurazioniPeriodoDTO(String periodo, Date competenzaDa, Date competenzaA, String dataValidita,
			List<RegolaRipartizioneDTO> regoleRipartizioneDTO) {
		super();
		this.periodo = periodo;
		this.competenzaDa = competenzaDa;
		this.competenzaA = competenzaA;
		this.dataValidita = dataValidita;
		this.regoleRipartizioneDTO = regoleRipartizioneDTO;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public String getDataValidita() {
		return dataValidita;
	}

	public void setDataValidita(String dataValidita) {
		this.dataValidita = dataValidita;
	}


	public List<RegolaRipartizioneDTO> getRegoleRipartizioneDTO() {
		return regoleRipartizioneDTO;
	}


	public void setRegoleRipartizioneDTO(List<RegolaRipartizioneDTO> regoleRipartizioneDTO) {
		this.regoleRipartizioneDTO = regoleRipartizioneDTO;
	}

	public Date getCompetenzaDa() {
		return competenzaDa;
	}

	public void setCompetenzaDa(Date competenzaDa) {
		this.competenzaDa = competenzaDa;
	}

	public Date getCompetenzaA() {
		return competenzaA;
	}

	public void setCompetenzaA(Date competenzaA) {
		this.competenzaA = competenzaA;
	}


}
