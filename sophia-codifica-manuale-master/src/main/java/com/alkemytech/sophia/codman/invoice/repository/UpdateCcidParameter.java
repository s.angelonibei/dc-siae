package com.alkemytech.sophia.codman.invoice.repository;

import java.util.List;

import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;

import java.io.Serializable;

@Produces("application/json")
@XmlRootElement
public class UpdateCcidParameter implements Serializable  {

  	private static final long serialVersionUID = 6932794204175966625L;

    String idCCIDMetadata;
    String invoiceAmaunt;
    
    public UpdateCcidParameter(){
    	
    }

	public String getIdCCIDMetadata() {
		return idCCIDMetadata;
	}



	public void setIdCCIDMetadata(String idCCIDMetadata) {
		this.idCCIDMetadata = idCCIDMetadata;
	}

	public String getInvoiceAmaunt() {
		return invoiceAmaunt;
	}

	public void setInvoiceAmaunt(String invoiceAmaunt) {
		this.invoiceAmaunt = invoiceAmaunt;
	}
   
}
