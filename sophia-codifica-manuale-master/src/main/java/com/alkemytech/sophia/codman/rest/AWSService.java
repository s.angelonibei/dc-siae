package com.alkemytech.sophia.codman.rest;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.codman.entity.ActiveClusters;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.alkemytech.sophia.common.emr.EMRService;
import com.alkemytech.sophia.common.tools.StringTools;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

@Singleton
@Path("aws")
public class AWSService {	
	
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Properties configuration;
	private final Provider<EntityManager> provider;
	private final Gson gson;
	private final EMRService emrService;

	@Inject
	protected AWSService(@Named("configuration") Properties configuration,
			@McmdbDataSource Provider<EntityManager> provider,
			Gson gson, EMRService emrService) {
		super();
		this.configuration = configuration;
		this.provider = provider;
		this.gson = gson;
		this.emrService = emrService;
	}
	
	@GET
	@Path("active-emr-clusters")
	@Produces("application/json")
	public Response getActiveEmrClusters(@QueryParam("useType")
			@DefaultValue("HYPERCUBE") String useType) {
		try {
			final List<JsonObject> activeEmrClusters = new ArrayList<JsonObject>();
			final EntityManager entityManager = provider.get();
			
			// load active cluster(s) from database
			final List<ActiveClusters> activeClusters = entityManager
					.createQuery("select x from ActiveClusters x, ClusterUse y"
							+ " where x.idCluster = y.idCluster"
							+ "   and y.useType = :useType", ActiveClusters.class)
					.setParameter("useType", useType)
					.getResultList();
			logger.debug("getActiveEmrClusters: activeClusters {}", activeClusters);
			if (null == activeClusters || activeClusters.isEmpty()) {
				return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR)
						.entity(gson.toJson(RestUtils.asMap("status", "KO",
								"errorMessage", "no active clusters"))).build();
			}
			
			for (ActiveClusters activeCluster : activeClusters) {
				final String dnsName = emrService.getDnsName(activeCluster.getIdCluster());
				if (!Strings.isNullOrEmpty(dnsName)) {
					final JsonObject jsonObject = (JsonObject) gson.toJsonTree(activeCluster);
					jsonObject.addProperty("dnsName", dnsName);
					jsonObject.addProperty("hiveJdbcUrl", "jdbc:hive2://" + dnsName + ":10000/default");
					jsonObject.addProperty("hiveJdbcUser", "hadoop");
					jsonObject.addProperty("hiveJdbcPassword", "");
					activeEmrClusters.add(jsonObject);
				}
			}
						
			final JsonObject result = RestUtils.asJsonObject("status", "OK");
			result.add("activeEmrClusters", gson.toJsonTree(activeEmrClusters));
			return Response.ok(gson.toJson(result)).build();
		} catch (Exception e) {			
			logger.error("getActiveEmrClusters", e);
			return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR)
					.entity(gson.toJson(RestUtils.asMap("status", "KO",
							"errorMessage", "unespected error",
							"stackTrace", RestUtils.asString(e)))).build();
		}
	}
	
	@POST
	@Path("kill-emr-step")
	@Produces("application/json")
	public Response killEmrStep(@QueryParam("clusterId") String clusterId,
			@QueryParam("applicationId") String applicationId) {
		try {
			logger.debug("killEmrStep: clusterId {}", clusterId);
			logger.debug("killEmrStep: applicationId {}", applicationId);
			// script path
			final String scriptPath = configuration.getProperty("rs.awsService.killEmrStepScriptPath");
			if (StringTools.isNullOrEmpty(scriptPath)) {
				throw new IllegalStateException("missing script path configuration");
			}
			logger.debug("killEmrStep: scriptPath {}", scriptPath);
			final File scriptFile = StringTools
					.isNullOrEmpty(scriptPath) ? null : new File(scriptPath);
			logger.debug("killEmrStep: scriptFile {}", scriptFile.getCanonicalPath());
			// working directory
			final String workingDirPath = configuration.getProperty("rs.awsService.killEmrStepWorkingDir");
			logger.debug("killEmrStep: workingDirPath {}", workingDirPath);
			final File workingDirFile = StringTools
					.isNullOrEmpty(workingDirPath) ? null : new File(workingDirPath);
			logger.debug("killEmrStep: workingDirFile {}", workingDirFile.getCanonicalPath());
			// master dns name
			final String dnsName = emrService.getDnsName(clusterId);
			if (StringTools.isNullOrEmpty(dnsName)) {
				throw new IllegalStateException("unable to retrieve master dns name");
			}
			logger.debug("killEmrStep: dnsName {}", dnsName);
			// exec
			final String[] cmdarray = { scriptFile.getCanonicalPath(), dnsName, applicationId };
			final Process process = Runtime.getRuntime().exec(cmdarray, null, workingDirFile.getCanonicalFile());
//			final Process process = Runtime.getRuntime().exec(cmdarray, null, null);
			final int exitValue = process.waitFor();
			final String stdout = StringTools.toString(process.getInputStream(), "UTF-8");
			final String stderr = StringTools.toString(process.getErrorStream(), "UTF-8");
			logger.debug("killEmrStep: stdout {}", stdout);
			logger.debug("killEmrStep: stderr {}", stderr);
			logger.debug("killEmrStep: exitValue {}", exitValue);
			return Response.ok(gson.toJson(RestUtils.asMap("status", "OK",
					"stdout", stdout,
					"stderr", stderr,
					"exitValue", Integer.toString(exitValue)))).build();
		} catch (Exception e) {			
			logger.error("killEmrStep", e);
			return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR)
					.entity(gson.toJson(RestUtils.asMap("status", "KO",
							"errorMessage", "unespected error",
							"stackTrace", RestUtils.asString(e)))).build();
		}
	}
	
}
