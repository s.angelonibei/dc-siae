

package com.alkemytech.sophia.codman.entity;

import com.alkemytech.sophia.codman.dto.AnagUtilizationTypeIdDspResultSet;
import com.alkemytech.sophia.codman.persistence.AbstractEntity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity(name="AnagUtilizationType")
@Table(name="ANAG_UTILIZATION_TYPE")
@NamedQueries({@NamedQuery(name="AnagUtilizationType.GetAll", query="SELECT x FROM AnagUtilizationType x"),
				@NamedQuery(name="AnagUtilizationType.GetByIdDsp", query="select new com.alkemytech.sophia.codman.dto.DropdownDTO(x.idUtilizationType, x.name) from AnagUtilizationType x where x.idUtilizationType in( select distinct y.idUtilizationType from CommercialOffers y where y.idDSP in :idDsps)")})
@SuppressWarnings("serial")
@SqlResultSetMapping(
		name = "AnagUtilizationTypeIdDspResultSet",
		classes = @ConstructorResult(
				targetClass = AnagUtilizationTypeIdDspResultSet.class,
				columns = {
						@ColumnResult(name = "idUtilizationType"),
						@ColumnResult(name = "name"),
						@ColumnResult(name = "description"),
						@ColumnResult(name = "idDsp")}))

public class AnagUtilizationType extends AbstractEntity<String> {

	@Id
	@Column(name="ID_UTILIZATION_TYPE", nullable=false)
	@XmlElement(name = "id")
	private String idUtilizationType;
	
	@Column(name="NAME")
	private String name;
	
	@Column(name="DESCRIPTION", nullable=false)
	private String description;
	
	@Override
	public String getId() {
		return getIdUtilizationType();
	}

	public String getIdUtilizationType() {
		return idUtilizationType;
	}

	public void setIdUtilizationType(String idUtilizationType) {
		this.idUtilizationType = idUtilizationType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}