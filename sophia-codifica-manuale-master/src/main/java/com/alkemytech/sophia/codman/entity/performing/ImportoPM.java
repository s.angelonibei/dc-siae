package com.alkemytech.sophia.codman.entity.performing;

import com.alkemytech.sophia.codman.utils.AccountingUtils;

import javax.persistence.Transient;
import java.util.List;

/**
 * Created by idilello on 11/11/16.
 */
public class ImportoPM {

    private Double importoAggregatoSUN;
    private Double importoTotaleRicalcolato;
    private List<ImportoRicalcolato> importiRicalcolati;

    public Double getImportoAggregatoSUN() {
        return importoAggregatoSUN;
    }

    public void setImportoAggregatoSUN(Double importoAggregatoSUN) {
        this.importoAggregatoSUN = importoAggregatoSUN;
    }

    public Double getImportoTotaleRicalcolato() {
        return importoTotaleRicalcolato;
    }

    public void setImportoTotaleRicalcolato(Double importoTotaleRicalcolato) {
        this.importoTotaleRicalcolato = importoTotaleRicalcolato;
    }

    public List<ImportoRicalcolato> getImportiRicalcolati() {
        return importiRicalcolati;
    }

    public void setImportiRicalcolati(List<ImportoRicalcolato> importiRicalcolati) {
        this.importiRicalcolati = importiRicalcolati;
    }

    public String getImportoTotaleRicalcolatoFormatted() {
        return AccountingUtils.displayFormatted(importoTotaleRicalcolato);
    }

    public String getImportoAggregatoSUNFormatted() {
        return AccountingUtils.displayFormatted(importoAggregatoSUN);
    }

}
