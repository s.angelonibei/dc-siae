package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UnidentifiedLoadingSubmitRequestDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<UnidentifiedLoadingSearchResultDTO> dsrs;

	public List<UnidentifiedLoadingSearchResultDTO> getDsrs() {
		return dsrs;
	}

	public void setDsrs(List<UnidentifiedLoadingSearchResultDTO> dsrs) {
		this.dsrs = dsrs;
	}

}
