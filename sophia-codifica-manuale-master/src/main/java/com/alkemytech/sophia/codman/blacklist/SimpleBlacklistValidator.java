package com.alkemytech.sophia.codman.blacklist;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

public class SimpleBlacklistValidator implements BlacklistValidator {

	private static final String PATTERN = "(\"([^\"]*)\"|[^,]*)(,|$)";
	
	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Inject
	protected SimpleBlacklistValidator() {
		super();
	}
	
	@Override
	public List<String> validate(File file) {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(file));
			String line;
			for (int i = 0; null != (line = reader.readLine()); i ++) {
                if (!line.matches(PATTERN)){
        			return Arrays.asList("Malformed CSV line " + i + ": [" + line + "]");
                }
            }
			return null;
		} catch (Exception e) {
			logger.error("validate", e);
			final StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			return Arrays.asList("Bad blacklist CSV file", e.getMessage(), out.toString());
		} finally {
			if (null != reader) {
				try {
					reader.close();
				} catch (IOException e) {
					logger.error("validate", e);
				}
			}
		}
	}

}
