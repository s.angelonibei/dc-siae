package com.alkemytech.sophia.codman.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity(name="SqsServiceBusStatsDate")
@Table(name="SQS_SERVICE_BUS")
@IdClass(SqsServiceBusStatsDateId.class)
@NamedNativeQuery(
	name="SqsServiceBusStatsDate.FindAll", 
	query="select DATE(INSERT_TIME) INSERT_TIME, QUEUE_NAME, count(*) QUEUE_SIZE from SQS_SERVICE_BUS where INSERT_TIME > (CURRENT_TIMESTAMP - INTERVAL 30 DAY) group by DATE(INSERT_TIME), QUEUE_NAME order by INSERT_TIME DESC, QUEUE_NAME", 
	resultClass=SqsServiceBusStatsDate.class)
public class SqsServiceBusStatsDate {

	@Id
	@Column(name="INSERT_TIME", nullable=false)
	private Date insertTime;

	@Id
	@Column(name="QUEUE_NAME", nullable=false)
	private String queueName;
	
	@Column(name="QUEUE_SIZE", nullable=false)
	private Long queueSize;
	
	public Date getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(Date insertTime) {
		this.insertTime = insertTime;
	}

	public String getQueueName() {
		return queueName;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

	public Long getQueueSize() {
		return queueSize;
	}

	public void setQueueSize(Long queueSize) {
		this.queueSize = queueSize;
	}
	
}
