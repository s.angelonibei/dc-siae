package com.alkemytech.sophia.codman.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import com.alkemytech.sophia.codman.entity.DettailImportiAnticipo;


public class DettailImportiAnticipoDTO {

	private Integer idDettailImportiAnticipo;
	private Integer idImportoAnticipo;
	private Date periodoPertinenzaInizio;
	private Date periodoPertinenzaFine;
	private String numeroFattura;
	private BigDecimal importoOriginale;
	private BigDecimal importoUtilizzabile;
	private String idDsp;
	private Integer idJoinAnticipiCcid;
	private String idCcid;
	private BigDecimal quotaAnticipo;
    private BigInteger idCCIDMetadata;
    private String idDsr;
    private BigDecimal totalValue;
    private String currency;
    private String ccidVersion;
    private Boolean ccidEncoded;
    private Boolean ccidEncodedProvisional;
    private Boolean ccidNotEncoded;
    private Integer invoiceItem;
    private String invoiceStatus;
    private BigDecimal valoreFatturabile;
    
    
    
	public DettailImportiAnticipoDTO( DettailImportiAnticipo entity) {
		
		init(entity);
		
	}
	
	public Integer getIdDettailImportiAnticipo() {
		return idDettailImportiAnticipo;
	}
	public void setIdDettailImportiAnticipo(Integer idDettailImportiAnticipo) {
		this.idDettailImportiAnticipo = idDettailImportiAnticipo;
	}
	public Integer getIdImportoAnticipo() {
		return idImportoAnticipo;
	}
	public void setIdImportoAnticipo(Integer idImportoAnticipo) {
		this.idImportoAnticipo = idImportoAnticipo;
	}
	public Date getPeriodoPertinenzaInizio() {
		return periodoPertinenzaInizio;
	}
	public void setPeriodoPertinenzaInizio(Date periodoPertinenzaInizio) {
		this.periodoPertinenzaInizio = periodoPertinenzaInizio;
	}
	public Date getPeriodoPertinenzaFine() {
		return periodoPertinenzaFine;
	}
	public void setPeriodoPertinenzaFine(Date periodoPertinenzaFine) {
		this.periodoPertinenzaFine = periodoPertinenzaFine;
	}
	public String getNumeroFattura() {
		return numeroFattura;
	}
	public void setNumeroFattura(String numeroFattura) {
		this.numeroFattura = numeroFattura;
	}
	public BigDecimal getImportoOriginale() {
		return importoOriginale;
	}
	public void setImportoOriginale(BigDecimal importoOriginale) {
		this.importoOriginale = importoOriginale;
	}
	public BigDecimal getImportoUtilizzabile() {
		return importoUtilizzabile;
	}
	public void setImportoUtilizzabile(BigDecimal importoUtilizzabile) {
		this.importoUtilizzabile = importoUtilizzabile;
	}
	public String getIdDsp() {
		return idDsp;
	}
	public void setIdDsp(String idDsp) {
		this.idDsp = idDsp;
	}
	public Integer getIdJoinAnticipiCcid() {
		return idJoinAnticipiCcid;
	}
	public void setIdJoinAnticipiCcid(Integer idJoinAnticipiCcid) {
		this.idJoinAnticipiCcid = idJoinAnticipiCcid;
	}
	public String getIdCcid() {
		return idCcid;
	}
	public void setIdCcid(String idCcid) {
		this.idCcid = idCcid;
	}
	public BigDecimal getQuotaAnticipo() {
		return quotaAnticipo;
	}
	public void setQuotaAnticipo(BigDecimal quotaAnticipo) {
		this.quotaAnticipo = quotaAnticipo;
	}
	public BigInteger getIdCCIDMetadata() {
		return idCCIDMetadata;
	}
	public void setIdCCIDMetadata(BigInteger idCCIDMetadata) {
		this.idCCIDMetadata = idCCIDMetadata;
	}
	public String getIdDsr() {
		return idDsr;
	}
	public void setIdDsr(String idDsr) {
		this.idDsr = idDsr;
	}
	public BigDecimal getTotalValue() {
		return totalValue;
	}
	public void setTotalValue(BigDecimal totalValue) {
		this.totalValue = totalValue;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getCcidVersion() {
		return ccidVersion;
	}
	public void setCcidVersion(String ccidVersion) {
		this.ccidVersion = ccidVersion;
	}
	public Boolean getCcidEncoded() {
		return ccidEncoded;
	}
	public void setCcidEncoded(Boolean ccidEncoded) {
		this.ccidEncoded = ccidEncoded;
	}
	public Boolean getCcidEncodedProvisional() {
		return ccidEncodedProvisional;
	}
	public void setCcidEncodedProvisional(Boolean ccidEncodedProvisional) {
		this.ccidEncodedProvisional = ccidEncodedProvisional;
	}
	public Boolean getCcidNotEncoded() {
		return ccidNotEncoded;
	}
	public void setCcidNotEncoded(Boolean ccidNotEncoded) {
		this.ccidNotEncoded = ccidNotEncoded;
	}
	public Integer getInvoiceItem() {
		return invoiceItem;
	}
	public void setInvoiceItem(Integer invoiceItem) {
		this.invoiceItem = invoiceItem;
	}
	public String getInvoiceStatus() {
		return invoiceStatus;
	}
	public void setInvoiceStatus(String invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}
	public BigDecimal getValoreFatturabile() {
		return valoreFatturabile;
	}
	public void setValoreFatturabile(BigDecimal valoreFatturabile) {
		this.valoreFatturabile = valoreFatturabile;
	}
    
	private void init(DettailImportiAnticipo entity) {
		this.idDettailImportiAnticipo= entity.getIdDettailImportiAnticipo();
		this.idImportoAnticipo = entity.getIdImportoAnticipo();
		this.periodoPertinenzaInizio = entity.getPeriodoPertinenzaInizio();
		this.periodoPertinenzaFine = entity.getPeriodoPertinenzaFine();
		this.numeroFattura = entity.getNumeroFattura();
		this.importoOriginale = entity.getImportoOriginale();
		this.importoUtilizzabile = entity.getImportoUtilizzabile();
		this.idDsp = entity.getIdDsp();
		this.idJoinAnticipiCcid = entity.getIdJoinAnticipiCcid();
		this.idCcid = entity.getIdCcid();
		this.quotaAnticipo = entity.getQuotaAnticipo();
		this.idCCIDMetadata = entity.getIdCCIDMetadata();
		this.idDsr = entity.getIdDsr();
		this.totalValue = entity.getTotalValue();
		this.currency = entity.getCurrency();
		this.ccidVersion = entity.getCcidVersion();
		this.ccidEncoded = entity.getCcidEncoded();
		this.ccidEncodedProvisional = entity.getCcidEncodedProvisional();
		this.ccidNotEncoded = entity.getCcidNotEncoded();
		this.invoiceItem = entity.getInvoiceItem();
		this.invoiceStatus = entity.getInvoiceStatus();
		this.valoreFatturabile = entity.getValoreFatturabile();
		
	}
    
}
