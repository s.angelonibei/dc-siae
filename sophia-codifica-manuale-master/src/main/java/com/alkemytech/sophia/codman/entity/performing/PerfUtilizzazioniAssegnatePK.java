package com.alkemytech.sophia.codman.entity.performing;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * The primary key class for the PERF_UTILIZZAZIONI_ASSEGNATE database table.
 * 
 */
@Embeddable
public class PerfUtilizzazioniAssegnatePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="ID_CODIFICA", insertable=false, updatable=false, unique=true, nullable=false)
	private Integer idCodifica;

	@Column(name="ID_SESSIONE_CODIFICA", insertable=false, updatable=false, unique=true, nullable=false)
	private Integer idSessioneCodifica;

	public PerfUtilizzazioniAssegnatePK() {
	}
	public Integer getIdCodifica() {
		return this.idCodifica;
	}
	public void setIdCodifica(Integer idCodifica) {
		this.idCodifica = idCodifica;
	}
	public Integer getIdSessioneCodifica() {
		return this.idSessioneCodifica;
	}
	public void setIdSessioneCodifica(Integer idSessioneCodifica) {
		this.idSessioneCodifica = idSessioneCodifica;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof PerfUtilizzazioniAssegnatePK)) {
			return false;
		}
		PerfUtilizzazioniAssegnatePK castOther = (PerfUtilizzazioniAssegnatePK)other;
		return 
			this.idCodifica.equals(castOther.idCodifica)
			&& this.idSessioneCodifica.equals(castOther.idSessioneCodifica);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.idCodifica.hashCode();
		hash = hash * prime + this.idSessioneCodifica.hashCode();
		
		return hash;
	}
}