package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;
import java.util.List;

public class ConfDspUtilizationDTO implements Serializable{

	private static final long serialVersionUID = 2519300344176150249L;

	private String dsp;
	private List<ConfUtilizationDTO> confUtilizationDTOList;
	
	
	public ConfDspUtilizationDTO(String dsp, List<ConfUtilizationDTO> confUtilizationDTOList) {
		super();
		this.dsp = dsp;
		this.confUtilizationDTOList = confUtilizationDTOList;
	}
	
	
	public String getDsp() {
		return dsp;
	}
	public void setDsp(String dsp) {
		this.dsp = dsp;
	}
	public List<ConfUtilizationDTO> getConfUtilizationDTOList() {
		return confUtilizationDTOList;
	}
	public void setConfUtilizationDTOList(List<ConfUtilizationDTO> confUtilizationDTOList) {
		this.confUtilizationDTOList = confUtilizationDTOList;
	}
	
}
