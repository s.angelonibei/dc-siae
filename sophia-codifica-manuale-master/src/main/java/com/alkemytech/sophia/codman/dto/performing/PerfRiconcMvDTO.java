package com.alkemytech.sophia.codman.dto.performing;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PerfRiconcMvDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private BigDecimal lordoNdm;
	private BigDecimal lordoSophia;
	private BigDecimal nettoNdm;
	private BigDecimal nettoSophia;
	private BigDecimal nettoSophiaArt;
	private String numeroFattura;
	private BigDecimal percAggioNdm;
	private Integer periodo;
	private boolean semaforo;
	private String voceIncasso;
	private String sede;
	private String agenzia;
	private String seprag;

	public PerfRiconcMvDTO() {
	}

	public PerfRiconcMvDTO(BigDecimal lordoNdm, BigDecimal lordoSophia, BigDecimal nettoNdm, BigDecimal nettoSophia, BigDecimal nettoSophiaArt, String numeroFattura, BigDecimal percAggioNdm, Integer periodo, boolean semaforo, String voceIncasso) {
		this.lordoNdm = lordoNdm;
		this.lordoSophia = lordoSophia;
		this.nettoNdm = nettoNdm;
		this.nettoSophia = nettoSophia;
		this.nettoSophiaArt = nettoSophiaArt;
		this.numeroFattura = numeroFattura;
		this.percAggioNdm = percAggioNdm;
		this.periodo = periodo;
		this.semaforo = semaforo;
		this.voceIncasso = voceIncasso;
	}

	public BigDecimal getLordoNdm() {
		return lordoNdm;
	}

	public void setLordoNdm(BigDecimal lordoNdm) {
		this.lordoNdm = lordoNdm;
	}

	public BigDecimal getLordoSophia() {
		return lordoSophia;
	}

	public void setLordoSophia(BigDecimal lordoSophia) {
		this.lordoSophia = lordoSophia;
	}

	public BigDecimal getNettoNdm() {
		return nettoNdm;
	}

	public void setNettoNdm(BigDecimal nettoNdm) {
		this.nettoNdm = nettoNdm;
	}

	public BigDecimal getNettoSophia() {
		return nettoSophia;
	}

	public void setNettoSophia(BigDecimal nettoSophia) {
		this.nettoSophia = nettoSophia;
	}

	public BigDecimal getNettoSophiaArt() {
		return nettoSophiaArt;
	}

	public void setNettoSophiaArt(BigDecimal nettoSophiaArt) {
		this.nettoSophiaArt = nettoSophiaArt;
	}

	public String getNumeroFattura() {
		return numeroFattura;
	}

	public void setNumeroFattura(String numeroFattura) {
		this.numeroFattura = numeroFattura;
	}

	public BigDecimal getPercAggioNdm() {
		return percAggioNdm;
	}

	public void setPercAggioNdm(BigDecimal percAggioNdm) {
		this.percAggioNdm = percAggioNdm;
	}

	public Integer getPeriodo() {
		return periodo;
	}

	public void setPeriodo(Integer periodo) {
		this.periodo = periodo;
	}

	public boolean isSemaforo() {
		return semaforo;
	}

	public void setSemaforo(boolean semaforo) {
		this.semaforo = semaforo;
	}

	public String getVoceIncasso() {
		return voceIncasso;
	}

	public void setVoceIncasso(String voceIncasso) {
		this.voceIncasso = voceIncasso;
	}

	public String getSede() {
		return sede;
	}

	public void setSede(String sede) {
		this.sede = sede;
	}

	public String getAgenzia() {
		return agenzia;
	}

	public void setAgenzia(String agenzia) {
		this.agenzia = agenzia;
	}

	public String getSeprag() {
		return seprag;
	}

	public void setSeprag(String seprag) {
		this.seprag = seprag;
	}
}