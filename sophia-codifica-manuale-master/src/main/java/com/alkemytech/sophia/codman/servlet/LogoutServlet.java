package com.alkemytech.sophia.codman.servlet;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.codman.sso.AasAuthenticationWS;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

@Singleton
@SuppressWarnings("serial")
public class LogoutServlet extends HttpServlet {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final String ssoAasToken;
	private final String ssoAasWsEndpoint;
	private final String ssoAasLoginUrl;
		
	@Inject
	protected LogoutServlet(@Named("sso.aas.token") String ssoAasToken,
			@Named("sso.aas.ws.endpoint") String ssoAasWsEndpoint,
			@Named("sso.aas.login.url") String ssoAasLoginUrl) {
		super();
		this.ssoAasToken = ssoAasToken;
		this.ssoAasWsEndpoint = ssoAasWsEndpoint;
		this.ssoAasLoginUrl = ssoAasLoginUrl;
	}
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// invalidate session and authentication token
		final HttpSession session = request.getSession(false);
		if (null != session) {
			final AasAuthenticationWS aasAuthentication = new AasAuthenticationWS(ssoAasWsEndpoint);
			try {
				final String authenticationToken = (String)
						session.getAttribute(ssoAasToken);
				aasAuthentication.expireToken(authenticationToken, request.getRemoteAddr());
				Thread.sleep(TimeUnit.SECONDS.toMillis(1));
			} catch (Exception e) {
				logger.error("service", e);
			}
			// remove authentication token
			session.removeAttribute(ssoAasToken);
			// invalidate session
			session.invalidate();
		}
		
		// redirect to login page
		response.sendRedirect(ssoAasLoginUrl);
		
	}
	
}
