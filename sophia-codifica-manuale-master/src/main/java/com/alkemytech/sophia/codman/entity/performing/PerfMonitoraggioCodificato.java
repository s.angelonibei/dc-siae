package com.alkemytech.sophia.codman.entity.performing;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the PERF_MONITORAGGIO_CODIFICATO database table.
 * 
 */
@Entity
@Table(name="PERF_MONITORAGGIO_CODIFICATO")
@NamedQuery(name="PerfMonitoraggioCodificato.findAll", query="SELECT p FROM PerfMonitoraggioCodificato p")
public class PerfMonitoraggioCodificato implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID_MONITORAGGIO_CODIFICATO", unique=true, nullable=false)
	private int idMonitoraggioCodificato;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_CREAZIONE", nullable=false)
	private Date dataCreazione;

	@Column(name="VALORI", nullable=false)
	private String valori;

	//bi-directional many-to-one association to PerfValorizzazioneRun
	@JsonBackReference
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_VALORIZZAZIONE_RUN", nullable=false)
	private PerfValorizzazioneRun perfValorizzazioneRun;

	@Column(name = "ID_VALORIZZAZIONE_RUN", insertable = false, updatable = false)
	private Long idValorizzazioneRun;

	public Long getIdValorizzazioneRun() {
		return idValorizzazioneRun;
	}

	public void setIdValorizzazioneRun(Long idValorizzazioneRun) {
		this.idValorizzazioneRun = idValorizzazioneRun;
	}

	//bi-directional many-to-one association to PerfMonitoraggioCodificatoConf
	@JsonBackReference
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_MONITORAGGIO_CODIFICATO_CONF", nullable=false)
	private PerfMonitoraggioCodificatoConf perfMonitoraggioCodificatoConf;

	public PerfMonitoraggioCodificato() {
	}

	public int getIdMonitoraggioCodificato() {
		return this.idMonitoraggioCodificato;
	}

	public void setIdMonitoraggioCodificato(int idMonitoraggioCodificato) {
		this.idMonitoraggioCodificato = idMonitoraggioCodificato;
	}

	public Date getDataCreazione() {
		return this.dataCreazione;
	}

	public void setDataCreazione(Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}

	public String getValori() {
		return this.valori;
	}

	public void setValori(String valori) {
		this.valori = valori;
	}

	public PerfValorizzazioneRun getPerfValorizzazioneRun() {
		return this.perfValorizzazioneRun;
	}

	public void setPerfValorizzazioneRun(PerfValorizzazioneRun perfValorizzazioneRun) {
		this.perfValorizzazioneRun = perfValorizzazioneRun;
	}

	public PerfMonitoraggioCodificatoConf getPerfMonitoraggioCodificatoConf() {
		return this.perfMonitoraggioCodificatoConf;
	}

	public void setPerfMonitoraggioCodificatoConf(PerfMonitoraggioCodificatoConf perfMonitoraggioCodificatoConf) {
		this.perfMonitoraggioCodificatoConf = perfMonitoraggioCodificatoConf;
	}

}