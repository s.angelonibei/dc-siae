package com.alkemytech.sophia.codman.mandato.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.codman.dto.MandatoDTO;
import com.alkemytech.sophia.codman.dto.SocietaTutelaDTO;
import com.alkemytech.sophia.codman.entity.AnagCountry;
import com.alkemytech.sophia.codman.entity.Mandato;
import com.alkemytech.sophia.codman.entity.SocietaTutela;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.alkemytech.sophia.codman.mandato.service.IMandatoService;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

@Singleton
public class MandatoService implements IMandatoService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private Provider<EntityManager> provider;

	@Inject
	public MandatoService(@McmdbDataSource Provider<EntityManager> provider) {
		super();
		this.provider = provider;
	}

	@Override
	public MandatoDTO get(Integer id) {
		EntityManager em = provider.get();
		Mandato entity = em.find(Mandato.class, id);
		MandatoDTO dto = toDTO(em, entity);
		return dto;
	}

	protected MandatoDTO toDTO(EntityManager em, Mandato entity) {
		MandatoDTO dto = new MandatoDTO();
		dto.setId(entity.getId());
		SocietaTutela entityMandante = entity.getMandante();
		SocietaTutelaDTO mandante = new SocietaTutelaDTO();
		mandante.setId(entityMandante.getId());
		mandante.setCodice(entityMandante.getCodice());
		mandante.setNominativo(entityMandante.getNominativo());
		mandante.setPosizioneSIAE(entityMandante.getPosizioneSIAE());
		mandante.setCodiceSIADA(entityMandante.getCodiceSIADA());
		mandante.setTenant(entityMandante.getTenant());
		AnagCountry mandanteHomeTerritory = em.createNamedQuery("AnagCountry.GetByCode", AnagCountry.class)
				.setParameter("code", entityMandante.getHomeTerritoryCode()).getSingleResult();
		mandante.setHomeTerritory(mandanteHomeTerritory);

		SocietaTutela entityMandataria = entity.getMandataria();
		SocietaTutelaDTO mandataria = new SocietaTutelaDTO();
		mandataria.setId(entityMandataria.getId());
		mandataria.setCodice(entityMandataria.getCodice());
		mandataria.setNominativo(entityMandataria.getNominativo());
		mandataria.setPosizioneSIAE(entityMandataria.getPosizioneSIAE());
		mandataria.setCodiceSIADA(entityMandataria.getCodiceSIADA());
		mandataria.setTenant(entityMandataria.getTenant());
		AnagCountry mandatariaHomeTerritory = em.createNamedQuery("AnagCountry.GetByCode", AnagCountry.class)
				.setParameter("code", entityMandataria.getHomeTerritoryCode()).getSingleResult();
		mandataria.setHomeTerritory(mandatariaHomeTerritory);

		dto.setMandante(mandante);
		dto.setMandataria(mandataria);
		dto.setDataInizioMandato(entity.getDataInizioMandato());
		dto.setDataFineMandato(entity.getDataFineMandato());
		return dto;
	}

	protected Mandato toEntity(EntityManager em, MandatoDTO dto) {
		Mandato entity = new Mandato();
		entity.setId(dto.getId());
		SocietaTutelaDTO mandante = dto.getMandante();
		SocietaTutela mandanteEntity = new SocietaTutela();
		mandanteEntity.setId(mandante.getId());
		mandanteEntity.setCodice(mandante.getCodice());
		mandanteEntity.setNominativo(mandante.getNominativo());
		mandanteEntity.setPosizioneSIAE(mandante.getPosizioneSIAE());
		mandanteEntity.setCodiceSIADA(mandante.getCodiceSIADA());
		mandanteEntity.setTenant(mandante.getTenant());
		mandanteEntity.setHomeTerritoryCode(mandante.getHomeTerritory().getCode());

		SocietaTutelaDTO mandataria = dto.getMandataria();
		SocietaTutela mandatariaEntity = new SocietaTutela();
		mandatariaEntity.setId(mandataria.getId());
		mandatariaEntity.setCodice(mandataria.getCodice());
		mandatariaEntity.setNominativo(mandataria.getNominativo());
		mandatariaEntity.setPosizioneSIAE(mandataria.getPosizioneSIAE());
		mandatariaEntity.setCodiceSIADA(mandataria.getCodiceSIADA());
		mandatariaEntity.setTenant(mandataria.getTenant());
		mandatariaEntity.setHomeTerritoryCode(mandataria.getHomeTerritory().getCode());

		entity.setMandante(mandanteEntity);
		entity.setMandataria(mandatariaEntity);
		entity.setDataInizioMandato(dto.getDataInizioMandato());
		entity.setDataFineMandato(dto.getDataFineMandato());
		return entity;
	}

	@Override
	public List<MandatoDTO> getAll() {
		EntityManager em = provider.get();
		List<Mandato> entities = em.createNamedQuery("Mandato.GetAll", Mandato.class).getResultList();
		List<MandatoDTO> results = new ArrayList<MandatoDTO>();
		for (Mandato entity : entities) {
			MandatoDTO dto = toDTO(em, entity);
			results.add(dto);
		}
		return results;
	}

	@Override
	public MandatoDTO save(MandatoDTO mandato) {
		EntityManager em = provider.get();
		Mandato entity = toEntity(em, mandato);
		try {
			em.getTransaction().begin();
			Mandato saved = em.merge(entity);
			em.flush();
			em.getTransaction().commit();
			mandato.setId(saved.getId());
			return mandato;
		} catch (Exception ex) {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			throw ex;
		}
	}

	@Override
	public void delete(MandatoDTO mandato) {
		EntityManager em = provider.get();
		try {
			em.getTransaction().begin();
			Mandato entity = em.find(Mandato.class, mandato.getId());
			em.remove(entity);
			em.flush();
			em.getTransaction().commit();
			em.clear();
		} catch (Exception ex) {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			throw ex;
		}
	}

}
