package com.alkemytech.sophia.codman.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.GsonBuilder;

@Entity(name="DsrMetadata")
@Table(name="DSR_METADATA")
@XmlRootElement
public class DsrMetadata {

	@Id
	@Column(name="IDDSR", nullable=false)
	private String idDsr;

	@Column(name="IDDSP", nullable=false)
	private String idDsp;

	@Column(name="SALES_LINES_NUM", nullable=true)
	private BigDecimal salesLinesNum;
	
	@Column(name="TOTAL_VALUE", nullable=true)
	private BigDecimal totalValue;

	@Column(name="SUBSCRIPTIONS_NUM", nullable=true)
	private Integer subscriptionsNum;
	
	@Column(name="CREATION_TIMESTAMP", nullable=false)
	private String creationTimestamp;

	@Column(name="SERVICE_CODE", nullable=true)
	private Integer serviceCode;

	@Column(name="PERIOD_TYPE", nullable=false)
	private String periodType;

	@Column(name="PERIOD", nullable=true)
	private Integer period;

	@Column(name="YEAR", nullable=true)
	private Integer year;

	@Column(name="COUNTRY", nullable=false)
	private String country;

	@Column(name="CURRENCY", nullable=false)
	private String currency;

	public String getIdDsr() {
		return idDsr;
	}

	public void setIdDsr(String idDsr) {
		this.idDsr = idDsr;
	}

	public String getIdDsp() {
		return idDsp;
	}

	public void setIdDsp(String idDsp) {
		this.idDsp = idDsp;
	}

	public BigDecimal getSalesLinesNum() {
		return salesLinesNum;
	}

	public void setSalesLinesNum(BigDecimal salesLinesNum) {
		this.salesLinesNum = salesLinesNum;
	}

	public BigDecimal getTotalValue() {
		return totalValue;
	}

	public void setTotalValue(BigDecimal totalValue) {
		this.totalValue = totalValue;
	}

	public Integer getSubscriptionsNum() {
		return subscriptionsNum;
	}

	public void setSubscriptionsNum(Integer subscriptionsNum) {
		this.subscriptionsNum = subscriptionsNum;
	}

	public String getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(String creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Integer getServiceCode() {
		return serviceCode;
	}

	public void setServiceCode(Integer serviceCode) {
		this.serviceCode = serviceCode;
	}

	public String getPeriodType() {
		return periodType;
	}

	public void setPeriodType(String periodType) {
		this.periodType = periodType;
	}

	public Integer getPeriod() {
		return period;
	}

	public void setPeriod(Integer period) {
		this.period = period;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}
	
}

