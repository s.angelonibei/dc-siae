package com.alkemytech.sophia.codman.entity.performing.security;

//import it.siae.valorizzatore.utility.Utility;

import com.alkemytech.sophia.codman.utils.AccountingUtils;

/**
 * Created by idilello on 10/17/16.
 */
public class AggregatoVoce implements Comparable<AggregatoVoce>{

    // Informazioni necessarie per il report Aggregato Movimenti
    public String voceIncasso;
    public String tipologiaMovimento;
    public String tipoDocumento;

    public Double importo = 0.0D;
    public Double importo566 = 0.0D;
    public Double importoProgrammato = 0.0D;
    public Double importoSospesi = 0.0D;
    public Double importoAnnullati = 0.0D;

    // Informazioni necessarie per il report Aggregato Eventi
    public Long numeroDocumentiEntrata = 0L;
    public Long numeroDocumentiUscita = 0L;
    public Long numeroDocumenti = 0L;
    public Double importoEntrate = 0.0D;
    public Double importoUscite = 0.0D;
    public Long numeroEventi = 0L;
    public Long numeroPM = 0L;
    public Long numeroCedole = 0L;

    public String getVoceIncasso() {
        return voceIncasso;
    }

    public void setVoceIncasso(String voceIncasso) {
        this.voceIncasso = voceIncasso;
    }

    public String getTipologiaMovimento() {
        return tipologiaMovimento;
    }

    public void setTipologiaMovimento(String tipologiaMovimento) {
        this.tipologiaMovimento = tipologiaMovimento;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public Double getImporto() {

        if (importo != null)
            return AccountingUtils.round(importo,7);
        else
            return importo;

    }

    public void setImporto(Double importo) {
        this.importo = importo;
    }

    public Long getNumeroDocumentiEntrata() {
        return numeroDocumentiEntrata;
    }

    public void setNumeroDocumentiEntrata(Long numeroDocumentiEntrata) {
        this.numeroDocumentiEntrata = numeroDocumentiEntrata;
    }

    public Long getNumeroDocumentiUscita() {
        return numeroDocumentiUscita;
    }

    public void setNumeroDocumentiUscita(Long numeroDocumentiUscita) {
        this.numeroDocumentiUscita = numeroDocumentiUscita;
    }

    public Long getNumeroDocumenti() {
        return numeroDocumenti;
    }

    public void setNumeroDocumenti(Long numeroDocumenti) {
        this.numeroDocumenti = numeroDocumenti;
    }

    public Long getNumeroEventi() {
        return numeroEventi;
    }

    public void setNumeroEventi(Long numeroEventi) {
        this.numeroEventi = numeroEventi;
    }

    public Double getImporto566() {
        if (importo566 != null)
            return AccountingUtils.round(importo566,7);
        else
            return importo566;

    }

    public void setImporto566(Double importo566) {
        this.importo566 = importo566;
    }

    public Double getImportoProgrammato() {
        if (importoProgrammato != null)
            return AccountingUtils.round(importoProgrammato,7);
        else
            return importoProgrammato;
    }

    public void setImportoProgrammato(Double importoProgrammato) {
        this.importoProgrammato = importoProgrammato;
    }

    public Double getImportoSospesi() {

        if (importoSospesi != null)
            return AccountingUtils.round(importoSospesi,7);
        else
            return importoSospesi;

    }

    public void setImportoSospesi(Double importoSospesi) {
        this.importoSospesi = importoSospesi;
    }

    public Double getImportoAnnullati() {

        if (importoAnnullati != null)
            return AccountingUtils.round(importoAnnullati,7);
        else
            return importoAnnullati;

    }

    public void setImportoAnnullati(Double importoAnnullati) {
        this.importoAnnullati = importoAnnullati;
    }

    public Double getImportoEntrate() {

        if (importoEntrate != null)
            return AccountingUtils.round(importoEntrate,7);
        else
            return importoEntrate;

    }

    public void setImportoEntrate(Double importoEntrate) {
        this.importoEntrate = importoEntrate;
    }

    public Double getImportoUscite() {

        if (importoUscite != null)
            return AccountingUtils.round(importoUscite,7);
        else
            return importoUscite;

    }

    public void setImportoUscite(Double importoUscite) {
        this.importoUscite = importoUscite;
    }

    public Long getNumeroPM() {
        return numeroPM;
    }

    public void setNumeroPM(Long numeroPM) {
        this.numeroPM = numeroPM;
    }

    public Long getNumeroCedole() {
        return numeroCedole;
    }

    public void setNumeroCedole(Long numeroCedole) {
        this.numeroCedole = numeroCedole;
    }

    public int compareTo(AggregatoVoce a) {
        return Integer.parseInt(this.voceIncasso) - Integer.parseInt(a.voceIncasso);
    }

    public String getImportoFormatted() {
//        return Utility.displayFormatted(importo);
        return AccountingUtils.displayFormatted(importo);
    }
    public String getImporto566Formatted() {
//        return Utility.displayFormatted(importo566);
        return AccountingUtils.displayFormatted(importo566);
    }
    public String getImportoProgrammatoFormatted() {
//        return Utility.displayFormatted(importoProgrammato);
        return AccountingUtils.displayFormatted(importoProgrammato);
    }
    public String getImportoSospesiFormatted() {
//        return Utility.displayFormatted(importoSospesi);
        return AccountingUtils.displayFormatted(importoSospesi);
    }
    public String getImportoAnnullatiFormatted() {
//        return Utility.displayFormatted(importoAnnullati);
        return AccountingUtils.displayFormatted(importoAnnullati);
    }
    public String getImportoEntrateFormatted() {
//        return Utility.displayFormatted(importoEntrate);
        return AccountingUtils.displayFormatted(importoEntrate);
    }
    public String getImportoUsciteFormatted() {
//        return Utility.displayFormatted(importoUscite);
        return AccountingUtils.displayFormatted(importoUscite);
    }

}
