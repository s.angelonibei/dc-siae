package com.alkemytech.sophia.codman.rest;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.alkemytech.sophia.codman.entity.CurrencyRate;
import com.alkemytech.sophia.codman.entity.CurrencyRateId;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

/**
 * YAHOO
 * http://www.jarloo.com/yahoo_finance/
 * http://download.finance.yahoo.com/d/quotes.csv?f=p&s=EURUSD=X
 *
 * BCE
 * https://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml
 * 
 * FIXER.IO
 * http://api.fixer.io/latest
 * 
 */
@Singleton
@Path("currency")
public class CurrencyRateService {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());

	static abstract class RatesHandler extends DefaultHandler {
		public abstract List<CurrencyRate> getRates();
	}

	static class BceHandler extends RatesHandler {

		private final int year;
		private final int month;		
		private final String referenceTime;
		private final Map<String, Average> rates;
		private boolean validTime;
		private int depth;
		
		static class Average {
			BigDecimal value = BigDecimal.ZERO;
			int samples = 0;
		}
		
		public BceHandler(int year, int month) {
			super();
			this.year = year;
			this.month = month;
			this.referenceTime = String.format("%04d-%02d", year, month);
			this.rates = new HashMap<String, Average>();
			this.validTime = false;
			this.depth = -1;
		}

		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
	        if ("Cube".equalsIgnoreCase(qName)) {
	        	depth ++;
	        	if (1 == depth) {
		            final String time = attributes.getValue("time");
		            if (!StringUtils.isEmpty(time)) {
		            	validTime = time.startsWith(referenceTime);
		            } else {
		            	validTime = false;
		            }
	        	} else if (2 == depth && validTime) {
		            final String currency = attributes.getValue("currency");
		            final String rate = attributes.getValue("rate");
		            if (!StringUtils.isEmpty(currency) && !StringUtils.isEmpty(rate)) {
	            		Average average = rates.get(currency);
	            		if (null == average) {
	            			average = new Average();
	            		}
	            		average.samples ++;
	            		average.value = average.value.add(new BigDecimal(rate));
            			rates.put(currency, average);
		            }
	        	}
	        }
		}
		
		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException {
	        if ("Cube".equalsIgnoreCase(qName)) {
				depth --;
	        }
		}
		
		public List<CurrencyRate> getRates() {
//			final GregorianCalendar calendar = new GregorianCalendar(year, month, 1);
//			final int daysInMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
//			System.out.println("daysInMonth " + daysInMonth);
			final List<CurrencyRate> result = new ArrayList<CurrencyRate>();
			for (Map.Entry<String, Average> entry : rates.entrySet()) {
				final Average average = entry.getValue();
//				System.out.println(entry.getKey() + " average.samples " + average.samples);
				if (average.samples >= 10 && average.samples <= 31) {
					final CurrencyRate currencyRate = new CurrencyRate();
					currencyRate.setYear(String.format("%04d", year));
					currencyRate.setMonth(String.format("%02d", month));
					currencyRate.setSrcCurrency("EUR");
					currencyRate.setDstCurrency(entry.getKey());
					currencyRate.setRate(entry.getValue().value
							.divide(new BigDecimal(entry.getValue().samples), 20, RoundingMode.HALF_UP));
					result.add(currencyRate);
				}
			}
			return result;
		}
		
	}
	
	static class BceHistoryHandler extends RatesHandler {

		private final int year;
		private final int month;		
		private final String referenceTime;
		private final Map<String, Average> rates;
		
		private String currency;
		
		static class Average {
			BigDecimal value = BigDecimal.ZERO;
			int samples = 0;
		}
		
		public BceHistoryHandler(int year, int month) {
			super();
			this.year = year;
			this.month = month;
			this.referenceTime = String.format("%04d-%02d", year, month);
			this.rates = new HashMap<String, Average>();
			this.currency = null;
		}

		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
	        if ("Series".equalsIgnoreCase(qName)) {
	            final String freq = attributes.getValue("FREQ");
	            final String currency = attributes.getValue("CURRENCY");
	            final String currencyDenom = attributes.getValue("CURRENCY_DENOM");
	            if ("D".equals(freq) &&
	            		"EUR".equals(currencyDenom) &&
	            		!StringUtils.isEmpty(currency)) {
	            	this.currency = currency;
	            }
	        } else if (null != currency && "Obs".equalsIgnoreCase(qName)) {
	            final String timePeriod = attributes.getValue("TIME_PERIOD");
	            final String obsValue = attributes.getValue("OBS_VALUE");
	            final String obsStatus = attributes.getValue("OBS_STATUS");
	            if (null != timePeriod && timePeriod.startsWith(referenceTime) &&
	            		!StringUtils.isEmpty(obsValue) &&
	            		"A".equals(obsStatus)) {
            		Average average = rates.get(currency);
            		if (null == average) {
            			average = new Average();
            		}
            		average.samples ++;
            		average.value = average.value.add(new BigDecimal(obsValue));
        			rates.put(currency, average);
	            }
	        }
		}
		
		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException {
	        if ("Series".equalsIgnoreCase(qName)) {
				currency = null;
	        }
		}
		
		public List<CurrencyRate> getRates() {
//			final GregorianCalendar calendar = new GregorianCalendar(year, month, 1);
//			final int daysInMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
//			System.out.println("daysInMonth " + daysInMonth);
			final List<CurrencyRate> result = new ArrayList<CurrencyRate>();
			for (Map.Entry<String, Average> entry : rates.entrySet()) {
				final Average average = entry.getValue();
//				System.out.println(entry.getKey() + " average.samples " + average.samples);
				if (average.samples >= 10 && average.samples <= 31) {
					final CurrencyRate currencyRate = new CurrencyRate();
					currencyRate.setYear(String.format("%04d", year));
					currencyRate.setMonth(String.format("%02d", month));
					currencyRate.setSrcCurrency("EUR");
					currencyRate.setDstCurrency(entry.getKey());
					currencyRate.setRate(entry.getValue().value
							.divide(new BigDecimal(entry.getValue().samples), 20, RoundingMode.HALF_UP));
					result.add(currencyRate);
				}
			}
			return result;
		}
		
	}
	
	private final Provider<EntityManager> provider;
	private final Gson gson;
	@SuppressWarnings("unused")
	private final String[] currencyCodes;
	
	@Inject
	protected CurrencyRateService(@McmdbDataSource Provider<EntityManager> provider, Gson gson,
			@Named("currency.codes") String currencyCodes) {
		super();
		this.provider = provider;
		this.gson = gson;
		this.currencyCodes = currencyCodes.split(",\\s*");
	}
	
	private List<CurrencyRate> getBceRates(int year, int month, String url, RatesHandler handler) {
		final List<CurrencyRate> result = new ArrayList<CurrencyRate>();
		logger.debug("getBceRates: reference time " + String.format("%04d-%02d", year, month));
		final HttpClient httpClient = HttpClientBuilder.create().build();
		try {
			final HttpResponse httpResponse = httpClient.execute(new HttpGet(url));
			if (200 == httpResponse.getStatusLine().getStatusCode()) {
				try (final InputStream in = httpResponse.getEntity().getContent()) {
					final SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
					final SAXParser saxParser = saxParserFactory.newSAXParser();
					saxParser.parse(in, handler);
					result.addAll(handler.getRates());
				}
			}
		} catch (IOException | ParserConfigurationException | SAXException e) {
			logger.error("getBceRates", e);
		}
		return result;
	}
	
	
	private List<CurrencyRate> getBceRates(int year, int month) {
		return getBceRates(year, month, "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml?" +
				Long.toHexString(System.currentTimeMillis()), new BceHandler(year, month));
	}
	
	private List<CurrencyRate> getBceHistoryRates(int year, int month) {
		return getBceRates(year, month, "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-sdmx.xml?" +
				Long.toHexString(System.currentTimeMillis()), new BceHistoryHandler(year, month));
	}

	public static void main(String[] args) throws Exception {
		
		
		System.out.println(Long.toHexString(System.currentTimeMillis()));
	}

//		File file = new File("/Users/roberto/Development/SIAE-SOPHIA/doc/2017_04_06_eurofxref-hist.xml");
//		File file = new File("/Users/roberto/Development/SIAE-SOPHIA/doc/2017_09_02_eurofxref-hist.xml");
//
//		for (int year = 2017; year <= 2017; year ++) {
//			for (int month = 4; month <= 4; month ++) {
//				try (final InputStream in = new FileInputStream(file)) {
//					final SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
//					final SAXParser saxParser = saxParserFactory.newSAXParser();
//					final BceHandler handler = new BceHandler(year, month);
//					saxParser.parse(in, handler);
//					for (CurrencyRate rate : handler.getRates()) {						
//						System.out.print("insert into MCMDB_debug.currency_rate (year, month, src_currency, dst_currency, rate)");
//						System.out.println("values ('" + rate.getYear() + "','" + rate.getMonth() + "','" + 
//								rate.getSrcCurrency() + "','" + rate.getDstCurrency() + "'," + rate.getRate() + ");");
//						System.out.print("insert into MCMDB_debug.currency_rate (year, month, src_currency, dst_currency, rate)");
//						System.out.println("values ('" + rate.getYear() + "','" + rate.getMonth() + "','" + 
//								rate.getDstCurrency() + "','" + rate.getSrcCurrency() + "'," + 
//								BigDecimal.ONE.divide(rate.getRate(), 20, RoundingMode.HALF_UP) + ");");
//					}
//				}
//
//			}			
//		}
//
//	}	

//	@SuppressWarnings("unused")
//	private List<CurrencyRate> getYahooFinanceRates() {
//		final List<CurrencyRate> result = new ArrayList<CurrencyRate>(currencyCodes.length);
//		final HttpClient httpClient = HttpClientBuilder.create().build();
//		final String srcCurrency = "EUR";
//		for (String dstCurrency : currencyCodes) {
//			try {
//				final HttpResponse httpResponse = httpClient
//						.execute(new HttpGet("http://download.finance.yahoo.com/d/quotes.csv?f=p&s=" + srcCurrency + dstCurrency + "=X"));
//				if (200 == httpResponse.getStatusLine().getStatusCode()) {
//					final CurrencyRate currencyRate = new CurrencyRate();
//					currencyRate.setSrcCurrency(srcCurrency);
//					currencyRate.setDstCurrency(dstCurrency);
//					currencyRate.setRate(new BigDecimal(EntityUtils
//							.toString(httpResponse.getEntity()).trim().replace(',', '.')));
//					result.add(currencyRate);
//				}
//			} catch (IOException | NumberFormatException e) {
//				System.err.println("error for currency " + dstCurrency);
//			}
//		}
//		return result;
//	}
	
//	@SuppressWarnings("unused")
//	private List<CurrencyRate> getFixerRates() {
//		final List<CurrencyRate> result = new ArrayList<CurrencyRate>();
//		final HttpClient httpClient = HttpClientBuilder.create().build();
//		try {
//			final HttpResponse httpResponse = httpClient.execute(new HttpGet("http://api.fixer.io/latest"));
//			if (200 == httpResponse.getStatusLine().getStatusCode()) {
//				try (final BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()))) {
//					final Gson gson = new GsonBuilder().create();
//					final JsonObject json = gson.fromJson(reader, JsonObject.class);
//					final String srcCurrency = json.get("base").getAsString();
//					final JsonObject rates = json.getAsJsonObject("rates");
//					for (Entry<String, JsonElement> entry : rates.entrySet()) {
//						final CurrencyRate currencyRate = new CurrencyRate();
//						currencyRate.setSrcCurrency(srcCurrency);
//						currencyRate.setDstCurrency(entry.getKey());
//						currencyRate.setRate(entry.getValue().getAsBigDecimal());
//						result.add(currencyRate);
//					}
//				}
//			}
//		} catch (JsonSyntaxException | JsonIOException | UnsupportedOperationException | IOException e) {
//			logger.error("", e);
//		}
//		return result;
//	}

	@GET
	@Produces("application/json")
	public Response findAll() {
		final EntityManager entityManager = provider.get();
		final List<CurrencyRate> result = entityManager
				.createQuery("select x from CurrencyRate x", CurrencyRate.class)
				.getResultList();
		return Response.ok(gson.toJson(result)).build();
	}

	@GET
	@Path("refresh")
	@Produces("application/json")
	public Response refresh(@QueryParam("year") Integer year, @QueryParam("month") Integer month) {
		try {
			final List<CurrencyRate> result = new ArrayList<CurrencyRate>();
			final EntityManager entityManager = provider.get();
			final boolean history = (null != year && year >= 1999 && year <= 2999 &&
					null != month && month >= 1 && month <= 12);
			if (!history) {
				final Calendar now = GregorianCalendar.getInstance();
				month = 1 + now.get(Calendar.MONTH);
				year = now.get(Calendar.YEAR);
				if (1 == month) {
					month = 12;
					year --;
				} else {
					month --;
				}
			}
			logger.debug("refresh: reference date " + String.format("%04d-%02d", year, month));
			logger.debug("refresh: history " + history);
			entityManager.getTransaction().begin();
			for (CurrencyRate currencyRate : (history ? getBceHistoryRates(year, month) : getBceRates(year, month))) {
				result.add(upsert(entityManager, currencyRate.getYear(), currencyRate.getMonth(),
						currencyRate.getSrcCurrency(), currencyRate.getDstCurrency(), currencyRate.getRate()));
				result.add(upsert(entityManager, currencyRate.getYear(), currencyRate.getMonth(),
						currencyRate.getDstCurrency(), currencyRate.getSrcCurrency(),
							BigDecimal.ONE.divide(currencyRate.getRate(), 20, RoundingMode.HALF_UP)));
			}
			entityManager.getTransaction().commit();
			// quarter averages
			if ((3 == month || 6 == month || 9 == month || 12 == month)) {
				entityManager.getTransaction().begin();
				final Map<String, CurrencyRate> quarterRates = new HashMap<String, CurrencyRate>();
				for (int i = 0; i < 3; i ++) {
					final List<CurrencyRate> currencyRates = entityManager
							.createQuery(new StringBuffer()
								.append("select x from CurrencyRate x")
								.append(" where x.year = :year")
								.append(" and x.month = :month")
								.toString(), CurrencyRate.class)
							.setParameter("year", String.format("%04d", year))
							.setParameter("month", String.format("%02d", month - i))
							.getResultList();
					// sum quarter rates by src_currency & dsr_currency
					final Map<String, CurrencyRate> currencyRateSums = new HashMap<String, CurrencyRate>();
					for (CurrencyRate currencyRate : currencyRates) {
						final String key = currencyRate.getSrcCurrency() + "-" + currencyRate.getDstCurrency();
						CurrencyRate currencyRateSum = quarterRates.get(key);
						if (null == currencyRateSum) {
							if (0 == i) {
								// create a copy to detach from persistence context
								currencyRateSum = new CurrencyRate();
								currencyRateSum.setYear(currencyRate.getYear());
								currencyRateSum.setMonth(currencyRate.getMonth());
								currencyRateSum.setSrcCurrency(currencyRate.getSrcCurrency());
								currencyRateSum.setDstCurrency(currencyRate.getDstCurrency());
								currencyRateSum.setRate(currencyRate.getRate());
								currencyRateSums.put(key, currencyRateSum);
							}
						} else {
							currencyRateSum.setRate(currencyRate.getRate().add(currencyRateSum.getRate()));
							currencyRateSums.put(key, currencyRateSum);
						}
					}
					quarterRates.clear();
					quarterRates.putAll(currencyRateSums);
				}
				// insert average rates
				final BigDecimal three = new BigDecimal(3.0);
				final String quarter =  String.format("Q%d", month / 3);
				for (CurrencyRate currencyRate : quarterRates.values()) {
					upsert(entityManager, currencyRate.getYear(),quarter,
							currencyRate.getSrcCurrency(), currencyRate.getDstCurrency(), 
							currencyRate.getRate().divide(three, 20, RoundingMode.HALF_UP));
				}
				entityManager.getTransaction().commit();
			}
			return Response.ok(gson.toJson(result)).build();
		} catch (Exception e) {
			logger.error("refresh", e);
		}
		return Response.status(500).build();
	}
	
	private CurrencyRate upsert(EntityManager entityManager, String year, String month, String srcCurrency, String dstCurrency, BigDecimal rate) {
		CurrencyRate currencyRate = entityManager
				.find(CurrencyRate.class, new CurrencyRateId(year, month, srcCurrency, dstCurrency));
		if (null == currencyRate) {
			currencyRate = new CurrencyRate();
			currencyRate.setYear(year);
			currencyRate.setMonth(month);
			currencyRate.setSrcCurrency(srcCurrency);
			currencyRate.setDstCurrency(dstCurrency);
			currencyRate.setRate(rate);
			entityManager.persist(currencyRate);
			logger.debug("upsert: insert " + year+ " " + month + " " + srcCurrency + " --> " + dstCurrency + " = " + rate);
		} else {
			currencyRate.setRate(rate);
			entityManager.merge(currencyRate);
			logger.debug("upsert: update " + year+ " " + month + " " + srcCurrency + " --> " + dstCurrency + " = " + rate);
		}
		// create a copy to detach from persistence context
		currencyRate = new CurrencyRate();
		currencyRate.setYear(year);
		currencyRate.setMonth(month);
		currencyRate.setSrcCurrency(srcCurrency);
		currencyRate.setDstCurrency(dstCurrency);
		currencyRate.setRate(rate);
		return currencyRate;
	}
	
	@GET
	@Path("convert")
	@Produces("application/json")
	public Response convert(@QueryParam("year") @DefaultValue("") String year,
			@QueryParam("month") @DefaultValue("") String month,
			@QueryParam("srcCurrency") String srcCurrency,
			@QueryParam("srcValue") String srcValue,
			@QueryParam("dstCurrency") String dstCurrency) {
		final EntityManager entityManager = provider.get();
		try {
			final Calendar now = GregorianCalendar.getInstance();
			if (StringUtils.isEmpty(year)) {
				year = String.format("%04d", now.get(Calendar.YEAR));
			}
			if (StringUtils.isEmpty(month)) {
				month = String.format("%02d", 1 + now.get(Calendar.MONTH));
			}
			final CurrencyRate currencyRate = entityManager
				.createQuery(new StringBuffer()
					.append("select x from CurrencyRate x")
					.append(" where x.year = :year")
					.append(" and x.month = :month")
					.append(" and x.srcCurrency = :srcCurrency")
					.append(" and x.dstCurrency = :dstCurrency")
					.toString(), CurrencyRate.class)
				.setParameter("year", year)
				.setParameter("month", month)
				.setParameter("srcCurrency", srcCurrency)
				.setParameter("dstCurrency", dstCurrency)
				.getSingleResult();
			if (null != currencyRate) {
				Map<String, String> result = new HashMap<String, String>();
				result.put("year", year);
				result.put("month", month);
				result.put("srcCurrency", srcCurrency);
				result.put("srcValue", srcValue);
				result.put("dstCurrency", dstCurrency);
				result.put("dstValue", new BigDecimal(srcValue.replace(',', '.'))
						.multiply(currencyRate.getRate()).toString());
				return Response.ok(gson.toJson(result)).build();
			}
		} catch (Exception e) {
			logger.error("convert", e);
		}
		return Response.status(500).build();
	}

}
