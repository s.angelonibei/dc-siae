package com.alkemytech.sophia.codman.rest;
import com.alkemytech.sophia.codman.dto.AnagCcidConfigDTO;
import com.alkemytech.sophia.codman.dto.AnagCcidFilenameConfigDTO;
import com.alkemytech.sophia.codman.dto.enumaration.UseStartEndDate;
import com.alkemytech.sophia.codman.entity.*;
import com.google.gson.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.codman.dto.CCIDConfigDTO;
import com.alkemytech.sophia.codman.dto.CCIDDTO;
import com.alkemytech.sophia.codman.dto.enumaration.UseStartEndDate;
import com.alkemytech.sophia.codman.entity.*;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.google.gson.GsonBuilder;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.util.*;

import static com.alkemytech.sophia.codman.rest.performing.Constants.STATUS;
import static javax.swing.JOptionPane.ERROR_MESSAGE;

@Singleton
@Path("ccidConfig")
public class CCIDConfigurationService {
	
	private static final String eurCurrency = "EUR";
	private static final String origCurrency = "ORIG";

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Provider<EntityManager> provider;
	private final Gson gson;
	private final MetadataService metadataService;

	@Inject
	protected CCIDConfigurationService(@McmdbDataSource Provider<EntityManager> provider,
			Gson gson, MetadataService metadataService) {
		super();
		this.provider = provider;
		this.gson = gson;
		this.metadataService = metadataService;
	}
	
	
	@GET
	@Path("all")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response all(@DefaultValue("0") @QueryParam("first") int first,
			@DefaultValue("50") @QueryParam("last") int last,
			@DefaultValue("") @QueryParam("dsp") String dsp,
			@DefaultValue("") @QueryParam("utilization") String utilization,
			@DefaultValue("") @QueryParam("offer") String offer,
			@DefaultValue("") @QueryParam("country") String country) {

		EntityManager entityManager = null;
		List<Object[]> result;
		try {
			boolean dspSetted = !dsp.isEmpty() && !dsp.equals("*");
			boolean utilizationSetted = !utilization.isEmpty() && !utilization.equals("*");
			boolean offerSetted = !offer.isEmpty() && !offer.equals("*");
			boolean countrySetted = !country.isEmpty() && !country.equals("*");
			String sqlString = " select  x.ID_CCID_CONFIG, "
					 +" x.IDDSP, "
					 + " (IF(x.IDDSP='*', '*', (SELECT NAME FROM ANAG_DSP WHERE IDDSP=x.IDDSP))) as DSP_NAME, "
					 + " x.ID_UTILIZATION_TYPE, "
					 + " (IF(x.ID_UTILIZATION_TYPE='*', '*', (SELECT NAME FROM ANAG_UTILIZATION_TYPE WHERE ANAG_UTILIZATION_TYPE.ID_UTILIZATION_TYPE =x.ID_UTILIZATION_TYPE))) as UTILIZATION_NAME, "
					 + " x.ID_COMMERCIAL_OFFERS, "
					 + " (IF(x.ID_COMMERCIAL_OFFERS='*', '*', (SELECT OFFERING FROM COMMERCIAL_OFFERS WHERE COMMERCIAL_OFFERS.ID_COMMERCIAL_OFFERS =x.ID_COMMERCIAL_OFFERS))) as OFFER_NAME, "
					 + " x.ID_COUNTRY, "
					 + " (IF(x.ID_COUNTRY='*', '*', (SELECT NAME FROM ANAG_COUNTRY WHERE ANAG_COUNTRY.ID_COUNTRY=x.ID_COUNTRY))) as COUNTRY_NAME, "
					 + " x.ENCODED, "
					 + " x.ENCODED_PROVISIONAL, "
					 + " x.NOT_ENCODED, "
					 + " x.CCID_VERSION, "
					 + " (SELECT NAME FROM ANAG_CCID_VERSION a WHERE  a.ID_CCID_VERSION = x.CCID_VERSION ) as CCID_VERSION_NAME, "
					 + " x.RANK ,"
					 + " x.CURRENCY, "
					 + " x.EXCLUDE_CLAIM_0"
					 + " from CCID_CONFIG x "
					 + (dspSetted || utilizationSetted || offerSetted || countrySetted ? " where " : "")
					 + (dspSetted ? "  (x.IDDSP = ?  or x.IDDSP = '*') " : "")
					 + (utilizationSetted && dspSetted ? " and  " : ""  )
					 + (utilizationSetted ? " (x.ID_UTILIZATION_TYPE = ? OR x.ID_UTILIZATION_TYPE = '*')  " : ""  )
					 + (offerSetted && (dspSetted || utilizationSetted)  ? " and " : "")
					 + (offerSetted  ? " (x.ID_COMMERCIAL_OFFERS = ? or x.ID_COMMERCIAL_OFFERS = '*')  " : "")
					 + (offerSetted && dspSetted && !utilizationSetted  ?  " "
					 		+ "AND (SELECT c.ID_COMMERCIAL_OFFERS FROM COMMERCIAL_OFFERS c where c.ID_COMMERCIAL_OFFERS = ?"
					 		+ " AND c.IDDSP = ? AND c.ID_UTIILIZATION_TYPE = x.ID_UTILIZATION_TYPE ) = ? "
					 		+ "OR (x.IDDSP = ? AND (SELECT c.IDDSP FROM COMMERCIAL_OFFERS c where c.ID_COMMERCIAL_OFFERS = ? )=x.IDDSP "
						 		+ " AND x.ID_UTILIZATION_TYPE = '*' AND x.ID_COMMERCIAL_OFFERS = '*' )"
					 		+ "OR  (x.IDDSP ='*' AND x.ID_COMMERCIAL_OFFERS = '*' AND x.ID_UTILIZATION_TYPE = '*')" : "")
					 + (offerSetted && dspSetted  && utilizationSetted  ?  " "
					 		+ "AND ( (SELECT c.ID_COMMERCIAL_OFFERS FROM COMMERCIAL_OFFERS c where c.ID_COMMERCIAL_OFFERS = ?"
						 		+ " AND c.IDDSP = ? AND c.ID_UTIILIZATION_TYPE = ?) = ? "
						 		+ " OR (x.IDDSP = ? AND (SELECT c.ID_UTIILIZATION_TYPE FROM COMMERCIAL_OFFERS c where c.ID_COMMERCIAL_OFFERS = ? )=x.ID_UTILIZATION_TYPE "
						 		+ " AND x.ID_COMMERCIAL_OFFERS = '*' AND x.ID_UTILIZATION_TYPE = '*') ) " : "")
					 + (offerSetted && !dspSetted && utilizationSetted ?  " AND ((SELECT c.IDDSP FROM COMMERCIAL_OFFERS c where c.ID_COMMERCIAL_OFFERS = ?"
						 		+ " AND c.ID_UTIILIZATION_TYPE = ?) = x.IDDSP"
						 		+ " OR (x.IDDSP='*' AND (SELECT c.ID_UTIILIZATION_TYPE FROM COMMERCIAL_OFFERS c where c.ID_COMMERCIAL_OFFERS = ? AND c.ID_UTIILIZATION_TYPE = ?)=x.ID_UTILIZATION_TYPE) "
						 		+ " OR  (x.IDDSP ='*' AND x.ID_COMMERCIAL_OFFERS = '*' AND x.ID_UTILIZATION_TYPE = '*' AND 'VALID_OFFER' = (SELECT 'VALID_OFFER' FROM COMMERCIAL_OFFERS c WHERE c.ID_UTIILIZATION_TYPE= ? and c.ID_COMMERCIAL_OFFERS = ? )))" : "")
					 + (offerSetted && !dspSetted && !utilizationSetted ? 
					 		 "	AND (((SELECT c.IDDSP FROM COMMERCIAL_OFFERS c where c.ID_COMMERCIAL_OFFERS = ?) = x.IDDSP "
					 		+ " AND (SELECT c.ID_UTIILIZATION_TYPE FROM COMMERCIAL_OFFERS c where c.ID_COMMERCIAL_OFFERS = ?)=x.ID_UTILIZATION_TYPE "
					 		+ " AND x.ID_COMMERCIAL_OFFERS='*' ) "
					 		+ " OR((SELECT c.IDDSP FROM COMMERCIAL_OFFERS c where c.ID_COMMERCIAL_OFFERS = ?) = x.IDDSP AND x.ID_UTILIZATION_TYPE = '*' "
					 		+ " AND  x.ID_COMMERCIAL_OFFERS='*' ) "
					 		+ " OR (x.IDDSP='*' AND (SELECT c.ID_UTIILIZATION_TYPE FROM COMMERCIAL_OFFERS c where c.ID_COMMERCIAL_OFFERS = ? )=x.ID_UTILIZATION_TYPE "
					 		+ " AND x.ID_COMMERCIAL_OFFERS='*') "
					 		+ " OR  x.ID_COMMERCIAL_OFFERS = ? "
					 		+ " OR  (x.IDDSP ='*' AND x.ID_COMMERCIAL_OFFERS = '*' AND x.ID_UTILIZATION_TYPE = '*'))" : "")
					 + (!offerSetted && utilizationSetted ? " "
					 		+ " AND ((x.IDDSP IN (SELECT c.IDDSP FROM COMMERCIAL_OFFERS c where c.ID_UTIILIZATION_TYPE= ? ) "
					 		+ " 	OR  (x.IDDSP = '*' AND x.ID_UTILIZATION_TYPE = ?)"
					 		+ "		OR (x.IDDSP = '*' AND x.ID_UTILIZATION_TYPE = '*' AND x.ID_COMMERCIAL_OFFERS = '*' "
					 		+(!offerSetted && utilizationSetted && dspSetted ? " AND 'VALID_CONFIG' = (SELECT distinct 'VALID_CONFIG' FROM COMMERCIAL_OFFERS c where c.IDDSP = ? "
					 				+ " AND c.ID_UTIILIZATION_TYPE = ? )  " : "")
					 		+ " )) "
					 		+ " ) " : "")
					 + (dspSetted && !utilizationSetted && !offerSetted ? " AND ('VALID_UTILIZATION' = (SELECT distinct 'VALID_UTILIZATION' FROM COMMERCIAL_OFFERS c WHERE c.ID_UTIILIZATION_TYPE=  x.ID_UTILIZATION_TYPE and c.IDDSP = ? ) OR  x.ID_UTILIZATION_TYPE = '*') ": "" )
					 + (countrySetted  && (dspSetted || utilizationSetted || offerSetted) ? " and " : "")
					 + (countrySetted ? " (x.ID_COUNTRY = ? or x.ID_COUNTRY = '*')  " : "")					 
					 + " order by DSP_NAME,UTILIZATION_NAME, OFFER_NAME, COUNTRY_NAME, x.RANK desc ";


			entityManager = provider.get();
			final Query q = entityManager
					.createNativeQuery(sqlString)
									.setFirstResult(first) // offset
									.setMaxResults(1 + last - first);
			int i = 1;
			if (dspSetted) {
				q.setParameter(i++, dsp);
			}
			if (utilizationSetted) {
				q.setParameter(i++, utilization);
			}
			if (offerSetted) {
				q.setParameter(i++, offer);
				if(dspSetted && !utilizationSetted ){
					q.setParameter(i++, offer);	
					q.setParameter(i++, dsp);
					q.setParameter(i++, offer);	
					q.setParameter(i++, dsp);
					q.setParameter(i++, offer);	
				}else if(dspSetted && utilizationSetted){
					q.setParameter(i++, offer);	
					q.setParameter(i++, dsp);
					q.setParameter(i++, utilization);
					q.setParameter(i++, offer);
					q.setParameter(i++, dsp);
					q.setParameter(i++, offer);
				}else if(!dspSetted  && utilizationSetted){
					q.setParameter(i++, offer);	
					q.setParameter(i++, utilization);
					q.setParameter(i++, offer);
					q.setParameter(i++, utilization);
					q.setParameter(i++, utilization);
					q.setParameter(i++, offer);
					
				}else if(!dspSetted  && !utilizationSetted){
					q.setParameter(i++, offer);	
					q.setParameter(i++, offer);
					q.setParameter(i++, offer);					
					q.setParameter(i++, offer);					
					q.setParameter(i++, offer);					
	
				}
			}else{
				if( utilizationSetted){
					q.setParameter(i++, utilization);
					q.setParameter(i++, utilization);

					if(dspSetted){
						q.setParameter(i++, dsp);
						q.setParameter(i++, utilization);
					}
				}else{
					q.setParameter(i++,dsp);
				}
				
				
			
			}
			if (countrySetted) {
				q.setParameter(i++, country);
			}
							
			result = (List<Object[]>) q.getResultList();

			List<CCIDConfigDTO> dtoList = new ArrayList<>();
			final PagedResult pagedResult = new PagedResult();
			if (null != result) {
				final int maxrows = last - first;
				final boolean hasNext = result.size() > maxrows;
				for (Object[] o : (hasNext ? result.subList(0, maxrows) : result)) {
					CCIDConfigDTO e = new CCIDConfigDTO(o);
					e.setCcidParams(getCCIDConfigParam(e.getIdCCIDConfig()));
					if (e.getCcdiVersion() == 2)
					e.setfileNaming(getCCIDFilename(e.getIdCCIDConfig()));
					dtoList.add(e);
				}

				pagedResult.setRows(dtoList).setMaxrows(maxrows).setFirst(first)
						.setLast(hasNext ? last : first + result.size()).setHasNext(hasNext).setHasPrev(first > 0);

			} else {
				pagedResult.setMaxrows(0).setFirst(0).setLast(0).setHasNext(false).setHasPrev(false);
			}

			return Response.ok(gson.toJson(pagedResult)).build();
		} catch (Exception e) {
			logger.error("", e);
		}
		return Response.status(500).build();
	}
	
	protected Map<String,String> getCCIDConfigParam(Integer idCCIDConfig){
		Map<String, String> params = new HashMap<String, String>();
		String sqlString = "select p.NAME, COALESCE(cp.VALUE, p.VALUE) from ANAG_CCID_CONFIG_PARAM  cp JOIN ANAG_CCID_PARAM p on cp.ID_CCID_PARAM=p.ID_CCID_PARAM WHERE cp.ID_CCID_CONFIG = ? ";
		EntityManager entityManager = provider.get();
		final Query q = entityManager
				.createNativeQuery(sqlString);
		q.setParameter(1, idCCIDConfig);
		List<Object[]> results = (List<Object[]>) q.getResultList();
		for(Object[] result: results) {
			String key = (String)result[0];
			String value = result[1] != null ? (String) result[1] : "";
			params.put(key,value);
		}
		return params;
	}

	protected Map<String, String> getCCIDFilename(Integer idCCIDConfig) {
		Map<String, String> filename = null;
		Integer useStartEndDatePosition = 3;
		List<String> keys = new ArrayList(Arrays.asList("invoiceSender", "invoiceReceiver", "useType", "useStartEndDate", "licenseSd", "typeOfClaim"));
		String sqlString = "select INVOICE_SENDER, INVOICE_RECEIVER, USE_TYPE, USE_START_END, LICENSE_SD, TYPE_OF_CLAIM from MM_ANAG_CCID_FILENAME_CONFIG ac where ID_CCID_CONFIG = ? ";
		EntityManager entityManager = provider.get();
		final Query q = entityManager.createNativeQuery(sqlString);
		q.setParameter(1, idCCIDConfig);
		if (!q.getResultList().isEmpty()) {
			filename = new HashMap<>();
			List<Object[]> rows = (List<Object[]>) q.getResultList();
			for (Object[] row : rows) {
				for (int i = 0; i < row.length; i++) {
					if (row[i] != null) {
						String valueToSet = row[i].toString();
						if(i == useStartEndDatePosition){
							valueToSet = UseStartEndDate.findValueBykey(valueToSet);
						}
						filename.put(keys.get(i), valueToSet);
					}
				}
			}
		}
		return filename;
	}

	@POST
	@Path("addFileNaming/{idCCIDConfig}")
	public Response addFilename(AnagCcidFilenameConfigDTO dto, @PathParam("idCCIDConfig") Integer idCCIDConfig){
		final EntityManager entityManager = provider.get();
		Query query = entityManager.createNativeQuery("SELECT acfc.ID_CCID_CONFIG FROM MM_ANAG_CCID_FILENAME_CONFIG acfc where ID_CCID_CONFIG = ?");
		query.setParameter(1, idCCIDConfig);

		AnagCCIDFilenameConfig filename = new AnagCCIDFilenameConfig();
		filename.setIdCCIDConfig(idCCIDConfig);
		filename.setInvoiceSender(dto.getInvoiceSender());
		filename.setInvoiceReceiver(dto.getInvoiceReceiver());
		filename.setUseType(dto.getUseType());
		if (dto.getUseStartEndDate() != null) {
			logger.debug("addFileName %s", UseStartEndDate.find(dto.getUseStartEndDate()).toString());
			filename.setUseStartEndDate(UseStartEndDate.find(dto.getUseStartEndDate()));
		}
		filename.setLicenseSD(dto.getLicenseSD());
		filename.setTypeOfClaim(dto.getTypeOfClaim());

		if (query.getResultList().isEmpty()) {

			try {
				entityManager.getTransaction().begin();
				entityManager.persist(filename);
				entityManager.getTransaction().commit();
				return Response.ok().build();
			} catch (Exception e) {
				logger.error("addFileName", e);

				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
			}

		} else {
			try {
				entityManager.getTransaction().begin();
				Query update = entityManager.createNativeQuery("update MM_ANAG_CCID_FILENAME_CONFIG acfc SET INVOICE_SENDER = ?, INVOICE_RECEIVER = ?, USE_TYPE = ?, USE_START_END = ?, LICENSE_SD = ?, TYPE_OF_CLAIM = ? where ID_CCID_CONFIG = ?");
				update.setParameter(1, filename.getInvoiceSender());
				update.setParameter(2, filename.getInvoiceReceiver());
				update.setParameter(3, filename.getUseType());
				if (filename.getUseStartEndDate() != null) {
					update.setParameter(4, filename.getUseStartEndDate().name());
				} else {
					update.setParameter(4, null);
				}
				update.setParameter(5, filename.getLicenseSD());
				update.setParameter(6, filename.getTypeOfClaim());
				update.setParameter(7, filename.getIdCCIDConfig());
				update.executeUpdate();
				entityManager.getTransaction().commit();
				return Response.ok().build();
			} catch (Exception e){
				logger.error("addFileName", e);

				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
			}
		}
		return Response.status(500).build();
	}

	@DELETE
	@Path("deleteFileNaming/{idCCIDConfig}")
	public Response restoreFileNaming(@PathParam("idCCIDConfig") Integer idCCIDConfig) {
		final EntityManager entityManager = provider.get();
		try {
			entityManager.getTransaction().begin();
			final Query query = entityManager
					.createNativeQuery("delete from MM_ANAG_CCID_FILENAME_CONFIG where ID_CCID_CONFIG = ?");
			query.setParameter(1, idCCIDConfig);
			query.executeUpdate();
			entityManager.getTransaction().commit();
			return Response.ok().build();
		} catch (Exception e) {
			logger.error("delete filenaming", e);
			entityManager.getTransaction().rollback();
		}
		return Response.status(500).build();
	}
	
	@GET
	@Path("getFileNameConfig/{idDsr}")
	@Produces("application/json")
	public Response getConfigByDsr(@PathParam("idDsr") String idDsr) {
		EntityManager entityManager = provider.get();
		Map<String, Object> result = new HashMap<>();

		final DsrMetadata dsrMetadata = entityManager
				.find(DsrMetadata.class, idDsr);
		if (null == dsrMetadata) {
			result.put("message", "DSR_METADATA record not found");
			return Response.ok(gson.toJson(result)).build();
		}

		final CommercialOffers commercialOffers = entityManager
				.find(CommercialOffers.class, dsrMetadata.getServiceCode());
		if (null == commercialOffers) {
			result.put("message", "COMMERCIAL_OFFERS record not found");
			return Response.ok(gson.toJson(result)).build();
		}

		final AnagDsp anagDsp = entityManager
				.find(AnagDsp.class, commercialOffers.getIdDSP());
		if (null == anagDsp) {
			result.put("message", "ANAG_DSP record not found");
			return Response.ok(gson.toJson(result)).build();
		}

		final Query q = entityManager.createNativeQuery("select x.IDDSP" +
				", x.ID_UTILIZATION_TYPE" +
				", x.ID_COMMERCIAL_OFFERS" +
				", x.ID_COUNTRY" +
				", x.ENCODED" +
				", x.ENCODED_PROVISIONAL" +
				", x.NOT_ENCODED" +
				", x.CCID_VERSION" +
				", x.CURRENCY" +
				", x.EXCLUDE_CLAIM_0" +
				", x.ID_CCID_CONFIG" +
				" from CCID_CONFIG x" +
				" where (x.IDDSP = ? or x.IDDSP = '*')" +
				" and (x.ID_UTILIZATION_TYPE = ? or  x.ID_UTILIZATION_TYPE = '*')" +
				" and (x.ID_COMMERCIAL_OFFERS = ? or x.ID_COMMERCIAL_OFFERS = '*')" +
				" and (x.ID_COUNTRY= ? or x.ID_COUNTRY = '*')" +
				" order by x.rank desc");
		q.setParameter(1, anagDsp.getIdDsp());
		q.setParameter(2, commercialOffers.getIdUtilizationType());
		q.setParameter(3, Integer.toString(commercialOffers.getIdCommercialOffering()));
		q.setParameter(4, dsrMetadata.getCountry());

		final List<Object[]> ccidConfigs = q.getResultList();
		if (null == ccidConfigs || ccidConfigs.isEmpty()) {
			result.put("message", "CCID_CONF record not found");
			return Response.ok(gson.toJson(result)).build();
		}
		final Object[] ccidConfig = ccidConfigs.get(0);
		final Integer ccidConfigId = (Integer) ccidConfig[10];

		List<String> keysConfig = new ArrayList(Arrays.asList("idDsp", "idUtilizationType",
				"idCommercialOffer", "idCountry", "encoded",
				"encodedProvisional", "notEncoded", "ccidVersion", "currency", "excludeClaim0Column1", "idCcidConfig"));

		List<String> keysFilename = new ArrayList<>(Arrays.asList("invoiceSender", "invoiceReceiver","useType", "useStartEndDate", "licenseSd", "typeOfClaim"));

		if (ccidConfig.length > 0) {
			JsonObject config = new JsonObject();
			JsonObject filename = null;
			for (int i = 0; i < ccidConfig.length; i++) {
				if (ccidConfig[i] != null) {
					config.addProperty(keysConfig.get(i), String.valueOf(ccidConfig[i]));
				}
			}

			Query queryFilename = entityManager.createNativeQuery("select af.INVOICE_SENDER" +
					", af.INVOICE_RECEIVER" +
					", af.USE_TYPE" +
					", af.USE_START_END" +
					", af.LICENSE_SD" +
					", af.TYPE_OF_CLAIM" +
					" from MM_ANAG_CCID_FILENAME_CONFIG af" +
					" where af.ID_CCID_CONFIG = ?");
			queryFilename.setParameter(1, ccidConfigId);
			List<Object[]> ccidFilename = queryFilename.getResultList();

			if (!ccidFilename.isEmpty()) {
				filename = new JsonObject();
				for (Object[] row: ccidFilename) {
					for (int i = 0; i < row.length; i++) {
						if (row[i] != null) {
							filename.addProperty(keysFilename.get(i), String.valueOf(row[i]));
						}
					}
				}
			}
			JsonElement json = gson.toJsonTree(result);
			json.getAsJsonObject().add("configuration", config);
			if (filename != null) {
				config.add("filename", filename);
			}
			return Response.ok(gson.toJson(json)).build();
		}
		return Response.status(500).build();
	}

	@GET
	@Path("isDsrBackClaimed/{idDsr}")
	@Produces("application/json")
	public Response isDsrBackClaimed(@PathParam("idDsr") String idDsr){
		EntityManager entityManager = provider.get();
		JsonObject result = new JsonObject();
		Query q = entityManager.createNativeQuery("SELECT" +
				" dsr.BACKCLAIM" +
				", dsr.BC_TYPE" +
				" FROM DSR_METADATA dsr" +
				" WHERE dsr.IDDSR = ?");
		q.setParameter(1, idDsr);
		List<Object[]> queryResult = q.getResultList();
		if (queryResult == null || queryResult.isEmpty()){
			result.addProperty("message", "DSR_METADATA record not found");
			return Response.ok(gson.toJson(result)).build();
		}

		if (!queryResult.isEmpty()){
			List<String> resultKeys = Arrays.asList("backclaim", "bc_type");
			Object[] backclaim = queryResult.get(0);
			for(int i = 0; i < backclaim.length; i++)
			if (backclaim[i] != null){
				result.addProperty(resultKeys.get(i), String.valueOf(backclaim[i]));
			}

			return Response.ok(gson.toJson(result)).build();
		}

		return Response.status(500).build();
	}


	@POST
	@Path("")
	public Response put(CCIDDTO dto) {
		Status status = Status.INTERNAL_SERVER_ERROR;
		final EntityManager entityManager = provider.get();
		CCIDConfig config = new CCIDConfig();
		config.setIdDsp(dto.getIdDsp());
		config.setIdUtilizationType(dto.getIdUtilizationType());
		config.setIdCommercialOffers(dto.getIdCommercialOffers());
		config.setIdCountry(dto.getIdCountry());
		config.setEncoded(dto.getEncoded());
		config.setEncodedProvisional(dto.getEncodedProvisional());
		config.setNotEncoded(dto.getNotEncoded());
		config.setCcdiVersion(dto.getIdCCIDVersion());
		config.setRank(dto.getRank());
		config.setCurrency(dto.getCurrency());
		config.setExcludeClaimZero(dto.getExcludeClaimZero());
		List<AnagCcidConfigDTO> ccidAnagParams = dto.getCcidAnagParams();

		if (ccidAnagParams != null && !ccidAnagParams.isEmpty()) {
			List<AnagCCIDConfigParam> params = new ArrayList<AnagCCIDConfigParam>(ccidAnagParams.size());
			for(AnagCcidConfigDTO key:ccidAnagParams) {
				AnagCCIDConfigParam param = new AnagCCIDConfigParam();
				param.setCcidConfig(config);
				param.setValue(key.getValue());
				param.setAnagCCIDParams(entityManager.find(AnagCCIDParam.class,key.getId()));
				params.add(param);


			}
			config.setAnagCCIDConfigParamList(params);
		}
		
		try {
			entityManager.getTransaction().begin();
			entityManager.persist(config);
			entityManager.getTransaction().commit();
			return Response.ok().build();
		} catch (Exception e) {
			logger.error("all", e);
			
			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}

		}

		return Response.status(status).build();
	}

	@POST
	@Path("editCCIDConfig/{idCCIDConfig}")
	public Response updateConfig(CCIDDTO dto, @PathParam("idCCIDConfig") int idCCIDConfig){
		EntityManager entityManager = provider.get();
		try {
			entityManager.getTransaction().begin();
			Query q = entityManager.createNativeQuery("update CCID_CONFIG cf " +
					" SET IDDSP = ?," +
					" ID_UTILIZATION_TYPE = ?," +
					" ID_COMMERCIAL_OFFERS = ?," +
					" ID_COUNTRY = ?," +
					" ENCODED = ?," +
					" ENCODED_PROVISIONAL = ?," +
					" NOT_ENCODED = ?," +
					" CCID_VERSION = ?," +
					" RANK = ?," +
					" CURRENCY = ?," +
					" EXCLUDE_CLAIM_0 = ?" +
					" where ID_CCID_CONFIG = ?");
			q.setParameter(1, dto.getIdDsp());
			q.setParameter(2, dto.getIdUtilizationType());
			q.setParameter(3, dto.getIdCommercialOffers());
			q.setParameter(4, dto.getIdCountry());
			q.setParameter(5, dto.getEncoded());
			q.setParameter(6, dto.getEncodedProvisional());
			q.setParameter(7, dto.getNotEncoded());
			q.setParameter(8, dto.getIdCCIDVersion());
			q.setParameter(9, dto.getRank());
			q.setParameter(10, dto.getCurrency());
			q.setParameter(11, dto.getExcludeClaimZero());
			q.setParameter(12, idCCIDConfig);
			q.executeUpdate();

			List<AnagCcidConfigDTO> ccidAnagParams = dto.getCcidAnagParams();
			if (ccidAnagParams!= null && !ccidAnagParams.isEmpty()) {
				for (AnagCcidConfigDTO param : ccidAnagParams) {
					Query params = entityManager.createNativeQuery("update ANAG_CCID_CONFIG_PARAM ap" +
							" set ap.VALUE = ?" +
							", ap.ID_CCID_PARAM = ?" +
							" where ID_CCID_CONFIG = ?");
					params.setParameter(1, param.getValue());
					params.setParameter(2, param.getId().intValue());
					params.setParameter(3, idCCIDConfig);
					params.executeUpdate();
				}
			}
			entityManager.getTransaction().commit();
			return Response.ok().build();
		} catch (Exception e){
			logger.error("update ccidConfig", e);

			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
		}
		return Response.status(500).build();
	}
	
	@DELETE
	@Path("")
	public Response delete(@QueryParam("idCCIDConfig") int idCCIDConfig) {
		final EntityManager entityManager = provider.get();
		try {
			entityManager.getTransaction().begin();
			final Query query = entityManager
					.createQuery("delete from CCIDConfig where idCCIDConfig = :idCCIDConfig");
			query.setParameter("idCCIDConfig", idCCIDConfig);
			query.executeUpdate();
			entityManager.getTransaction().commit();
			return Response.ok().build();
		} catch (Exception e) {
			logger.error("delete", e);
			entityManager.getTransaction().rollback();
		}
		return Response.status(500).build();
	}

	@GET
	@Path("getCCIDConfiguration")
	@Produces("application/json")
	public Response getCCIDConfiguration(@QueryParam("dsp") String dsp, @QueryParam("utilization") String utilization,
			@QueryParam("offer") String offer, @QueryParam("country") String country,@QueryParam("idDsr") String idDsr) {
		return metadataService.getByIdDsr(idDsr);
	}

//	@GET
//	@Path("getCCIDConfiguration")
//	@Produces("application/json")
//	@SuppressWarnings("unchecked")
//	public Response getCCIDConfiguration(@QueryParam("dsp") String dsp, @QueryParam("utilization") String utilization,
//			@QueryParam("offer") String offer, @QueryParam("country") String country,@QueryParam("idDsr") String idDsr) {
//
//		CCIDServiceDTO ccidServiceDTO = new CCIDServiceDTO();
//		
//		if (null == dsp) {
//			ccidServiceDTO.setStatus("KO");
//			ccidServiceDTO.setErrorMessage("Manca il parametro 'dsp'");
//			return Response.ok(gson.toJson(ccidServiceDTO)).build();
//		}
//		
//		if (null == utilization) {
//			ccidServiceDTO.setStatus("KO");
//			ccidServiceDTO.setErrorMessage("Manca il parametro 'utilization'");
//			return Response.ok(gson.toJson(ccidServiceDTO)).build();
//		}
//		
//		if (null == offer) {
//			ccidServiceDTO.setStatus("KO");
//			ccidServiceDTO.setErrorMessage("Manca il parametro 'offer'");
//			return Response.ok(gson.toJson(ccidServiceDTO)).build();
//		}
//		
//		if (null == country) {
//			ccidServiceDTO.setStatus("KO");
//			ccidServiceDTO.setErrorMessage("Manca il parametro 'country'");
//			return Response.ok(gson.toJson(ccidServiceDTO)).build();
//		}
//
//		if (null == idDsr) {
//			ccidServiceDTO.setStatus("KO");
//			ccidServiceDTO.setErrorMessage("Manca il parametro 'idDsr'");
//			return Response.ok(gson.toJson(ccidServiceDTO)).build();
//		}
//		
//		EntityManager entityManager = null;
//		List<Object[]> result;
//		Object result2 = null;
//		BigDecimal conversionRate = new BigDecimal(1);
//
//		
//		try {
//			String sqlString = "	select x.encoded, x.encodedProvisional, x.notEncoded, y.name, x.currency, z.currency, "
//					+ " z.periodType, z.period, z.year, z.country, c.ccidServiceTypeCode, c.ccidUseTypeCode, "
//					+ " c.ccidAppliedTariffCode, c.tradingBrand, d.name "
//					+ " from CCIDConfig x , AnagCCIDVersion y, DsrMetadata z, CommercialOffers c, AnagDsp d "
//					+ " where "
//					+ " (x.idDsp = :idDsp or x.idDsp = '*') " 
//					+ " and "
//					+ " (x.idUtilizationType = :idUtilizationType or  x.idUtilizationType = '*') " 
//					+ " and "
//					+ " (x.idCommercialOffers = :idCommercialOffers or x.idCommercialOffers = '*') " 
//					+ " and "
//					+ " (x.idCountry= :idCountry or x.idCountry = '*') " 
//					+ " and x.ccdiVersion = y.idCCIDVersion"
//					+ " and z.idDsr = :idDsr "
//					+ " and c.idCommercialOffering = :offer "
//					+ " and d.idDsp = :idDsp "
//					+ " order by x.rank desc";
//
//			
//		
//			
//			entityManager = provider.get();
//			final Query q = entityManager.createQuery(sqlString);
//			q.setParameter("idDsp", dsp);
//			q.setParameter("idUtilizationType", utilization);
//			q.setParameter("idCommercialOffers", offer);
//			q.setParameter("offer", Integer.valueOf(offer));
//			q.setParameter("idCountry", country);
//			q.setParameter("idDsr", idDsr);
//			
//			result = (List<Object[]>) q.getResultList();
//		
//	
//			
//			String ccidConfigurationCurrency =  (String)result.get(0)[4];
//			String dsrMetadataCurrency = (String)result.get(0)[5];
//			String periodType = (String)result.get(0)[6];
//			Integer period = (Integer)result.get(0)[7];
//			Integer year = (Integer)result.get(0)[8];
//			String dsrMetadataCountry =(String)result.get(0)[9];
//			String ccidServiceTypeCode =(String)result.get(0)[10];
//			String ccidUseTypeCode =(String)result.get(0)[11];
//			String ccidAppliedTariffCode =(String)result.get(0)[12];
//			String tradingBrand =(String)result.get(0)[13];
//			String receiver = (String)result.get(0)[14];
//
//			boolean conversionRateNeeded = ccidConfigurationCurrency.equals("EUR") && !dsrMetadataCurrency.equals("EUR");
//				if(conversionRateNeeded){
//				//in questo caso calcoliamo il conversion rate, negli altri casi è 1 (ORIG -> qualsiasi valuta
//				// il conversion rate è 1 perché voglio mantenere la valuta originale senza
//				// fare alcuna conversione. Se invece ho il caso EUR -> EUR non necessito di fare converisoni
//				// essendo uguale  la valuta configurata a quella del DSR )
//
//				String sqlString2 = "select c.rate from CurrencyRate c where c.year = :year and c.month = :month "
//						+ " and c.srcCurrency = :srcCurrency and c.dstCurrency= :dstCurrency";
//				final Query q2 = entityManager.createQuery(sqlString2);
//				q2.setParameter("year",year.toString());
//				q2.setParameter("month", getPeriodFormat(periodType,period));
//				q2.setParameter("srcCurrency", dsrMetadataCurrency);
//				q2.setParameter("dstCurrency", ccidConfigurationCurrency);
//
//				 result2 = q2.getSingleResult();
//				 if (result2 != null) {
//						conversionRate = (BigDecimal) result2;
//						
//					}
//				
//			}
//
//			// calcolo dei valori di rightSplit
//			final String yearString = String.format("%04d", year);
//			final String monthString = "month".equalsIgnoreCase(periodType) ?
//					String.format("%02d", period) :
//					String.format("%02d", period * 3 - 2);
//			// RIGHT_SPLIT
//			final List<RightSplit> rightSplits = entityManager
//					.createQuery("select x from RightSplit x where x.country = :country and x.validFrom <= :validFrom", RightSplit.class)
//					.setParameter("country", dsrMetadataCountry)
//					.setParameter("validFrom", new SimpleDateFormat("yyyy-MM-dd")
//							.parse(yearString + "-" + monthString + "-01"))
//					.getResultList();
//
//			RightSplit rightSplit = rightSplits.get(0);
//
//			boolean validResult = result != null && !result.isEmpty();
//			boolean validConversionRate = !conversionRateNeeded || (conversionRateNeeded && result2 !=null);
//			boolean validRightSplit = rightSplits != null  && !rightSplits.isEmpty();
//
//			if (validResult && validConversionRate && validRightSplit ) {
//				
//				ccidServiceDTO.setStatus("OK");	
//				ccidServiceDTO.setEncoded((Boolean)result.get(0)[0]);
//				ccidServiceDTO.setEncodedProvisional((Boolean)result.get(0)[1]);
//				ccidServiceDTO.setNotEncoded((Boolean)result.get(0)[2]);
//				ccidServiceDTO.setCcidVersion((String)result.get(0)[3]);
//				ccidServiceDTO.setCcidCurrency((String)result.get(0)[4]);
//				ccidServiceDTO.setDsrCurrency(dsrMetadataCurrency);
//				ccidServiceDTO.setConversionRate(conversionRate);
//				ccidServiceDTO.setTradingBrand(tradingBrand);
//				ccidServiceDTO.setServiceType(ccidServiceTypeCode);
//				ccidServiceDTO.setUseType(ccidUseTypeCode);
//				ccidServiceDTO.setAppliedTariff(ccidAppliedTariffCode);
//				ccidServiceDTO.setSender("SIAE");
//				ccidServiceDTO.setReceiver(receiver);
//				DateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");			
//				ccidServiceDTO.setCcidId(df.format(new Date()));
//				ccidServiceDTO.setWorkCodeType("SIAE_WORK_CODE");
//				ccidServiceDTO.setDemSplit(rightSplit.getDem());
//				ccidServiceDTO.setDrmSplit(rightSplit.getDrm());
//
//				return Response.ok(gson.toJson(ccidServiceDTO)).build();
//			}
//
//
//		} catch (Exception e) {
//			logger.error("", e);
//		}
//
//		ccidServiceDTO.setStatus("KO");
//		ccidServiceDTO.setErrorMessage("Nessuna configurazione disponibile per i parametri indicati");
//		return Response.ok(gson.toJson(ccidServiceDTO)).build();
//	}

	
	
	
	@GET
	@Path("configurationValues")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response configurationValues() {

		EntityManager entityManager = null;
		Map<String, List<?>> result = new HashMap<>();
		List<AnagCCIDVersion> ccidVersionList;
		List<String> currencyList = new ArrayList<>();
		currencyList.add(eurCurrency);
		currencyList.add(origCurrency);
		
		try {

			entityManager = provider.get();
			Query query = entityManager.createQuery("select x from AnagCCIDVersion x order by x.name");

			ccidVersionList = (List<AnagCCIDVersion>) query.getResultList();
			for (AnagCCIDVersion ccid : ccidVersionList) {
				ccid.setParamsMap(new HashMap<String, List<AnagCCIDParam>>());
				for(AnagCCIDParam param :ccid.getParams()){
					if(!ccid.getParamsMap().containsKey((param.getName()))){
						ccid.getParamsMap().put(param.getName(), new ArrayList<AnagCCIDParam>());
					}
					ccid.getParamsMap().get(param.getName()).add(param);
				}
			}
			result.put("ccidVersionList", ccidVersionList);
			result.put("currencyList", currencyList);
			return Response.ok(new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(result)).build();

		} catch (Exception e) {
			logger.error("configurationValues", e);
		}
		return Response.status(500).build();

	}
	

	
	@GET
	@Path("checkConfigAlreadySet")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response checkConfigAlreadySet(@DefaultValue("") @QueryParam("idDsp") String idDsp,
			@DefaultValue("") @QueryParam("idUtilization") String idUtilization,
			@DefaultValue("") @QueryParam("idCommercialOffer") String idCommercialOffer,
			@DefaultValue("") @QueryParam("idCountry") String idCountry) {
		 EntityManager entityManager = null;
		List<Object> result;
		try {

			 entityManager = provider.get();
			 Query query = entityManager.createQuery(
					  " select x from CCIDConfig x "
			 		+ " where "
			 		+ " x.idDsp= :dsp and  x.idUtilizationType = :utilization and  x.idCommercialOffers = :offer and x.idCountry = :country");
			 
			
			 query.setParameter("dsp", idDsp);
			 query.setParameter("utilization", idUtilization);
			 query.setParameter("offer", idCommercialOffer);
			 query.setParameter("country", idCountry);
			 
			result = query.getResultList();
			
			if(result != null && !result.isEmpty()){
				return Response.ok(gson.toJson(true)).build();
			}
			return Response.ok(gson.toJson(false)).build();
		} catch (Exception e) {
			logger.error("checkConfigAlreadySet", e);
		}
		return Response.status(500).build();
	}

//	private String getPeriodFormat(String periodType, Integer period){
//		if(periodType.equals("quarter")){
//			return "Q" + period;
//		}else{
//			if(Integer.valueOf(period)<10){
//			return "0" + period;
//			}else{
//				return period.toString();
//			}
//		}
//	}


}