package com.alkemytech.sophia.codman.entity.performing;

import java.math.BigDecimal;

public class TotaleRiconciliazioneImporti {

    private Long recordTotali;
    private BigDecimal totaleLordoSophia = new BigDecimal(0);
    private BigDecimal totaleLordoNdm = new BigDecimal(0);
    private BigDecimal totaleNettoSophiaAggio = new BigDecimal(0);
    private BigDecimal totaleNettoNdmAggio = new BigDecimal(0);
    private BigDecimal totaleNettoSophia = new BigDecimal(0);

    public Long getRecordTotali() {
        return recordTotali;
    }

    public void setRecordTotali(Long recordTotali) {
        this.recordTotali = recordTotali;
    }

    public BigDecimal getTotaleLordoSophia() {
        return totaleLordoSophia;
    }

    public void setTotaleLordoSophia(BigDecimal totaleLordoSophia) {
        this.totaleLordoSophia = totaleLordoSophia;
    }

    public BigDecimal getTotaleLordoNdm() {
        return totaleLordoNdm;
    }

    public void setTotaleLordoNdm(BigDecimal totaleLordoNdm) {
        this.totaleLordoNdm = totaleLordoNdm;
    }

    public BigDecimal getTotaleNettoSophiaAggio() {
        return totaleNettoSophiaAggio;
    }

    public void setTotaleNettoSophiaAggio(BigDecimal totaleNettoSophiaAggio) {
        this.totaleNettoSophiaAggio = totaleNettoSophiaAggio;
    }

    public BigDecimal getTotaleNettoNdmAggio() {
        return totaleNettoNdmAggio;
    }

    public void setTotaleNettoNdmAggio(BigDecimal totaleNettoNdmAggio) {
        this.totaleNettoNdmAggio = totaleNettoNdmAggio;
    }

    public BigDecimal getTotaleNettoSophia() {
        return totaleNettoSophia;
    }

    public void setTotaleNettoSophia(BigDecimal totaleNettoSophia) {
        this.totaleNettoSophia = totaleNettoSophia;
    }
}
