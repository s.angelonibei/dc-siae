package com.alkemytech.sophia.codman.rest.performing;

import com.alkemytech.sophia.codman.dto.performing.*;
import com.alkemytech.sophia.codman.entity.PeriodoRipartizione;
import com.alkemytech.sophia.codman.entity.performing.PerfVoceIncasso;
import com.alkemytech.sophia.codman.rest.performing.service.ValorizzazioneService;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Singleton
@Path("performing/valorizzazione")
public class ConsoleValorizzazioneController {

	private ValorizzazioneService valorizzazioneService;
	private Gson gson;
	private HttpServletRequest request;

	public static final String UTF8_BOM = "\uFEFF";

	@Inject
	protected ConsoleValorizzazioneController(HttpServletRequest request, Gson gson,
			ValorizzazioneService valorizzazioneService) {
		super();
		this.request = request;
		this.gson = gson;
		this.valorizzazioneService = valorizzazioneService;

	}

	@POST
	@Path("configurazione/addConfigurazione")
	public Response addValorizzazioneConfigurazione(PerfValorizzazioneDTO campionamentoConfig) { // all'interno
																									// dell'oggetto è
																									// presente anche la
																									// proprietà user.
		// al momento della query aggiungere anche un timestamp per la data ultima
		// modifica
		Integer result = valorizzazioneService.addConfigurazione(campionamentoConfig);
		if (result == 200) {
			return Response.ok().build();
		} else {
			return Response.status(result).build();
		}
	}

	@PUT
	@Path("configurazione/updateConfigurazione")
	public Response updateValorizzazioneConfigurazione(PerfValorizzazioneDTO campionamentoConfig) { // all'interno
																									// dell'oggetto è
																									// presente anche la
																									// proprietà user.
		// al momento della query aggiungere anche un timestamp per la data ultima
		// modifica
		Integer result = valorizzazioneService.updateConfigurazione(campionamentoConfig);
		if (result == 200) {
			return Response.ok().build();
		} else {
			return Response.status(result).build();
		}
	}

	@DELETE
	@Path("configurazione/deleteConfigurazione")
	public Response deleteValorizzazioneConfigurazione(
			@QueryParam(value = "periodoRipartizione") String periodoRipartizione,
			@QueryParam(value = "voceIncasso") String voceIncasso,
			@QueryParam(value = "tipologiaReport") String tipologiaReport, @QueryParam(value = "user") String user) {

		try {
			valorizzazioneService.deleteValorizzazioneConfigurazione(periodoRipartizione, voceIncasso, tipologiaReport,
					user);
			return Response.ok().build();
		} catch (Exception e) {
			System.out.println("ERROR " + e);
			return Response.status(500).build();
		}

	}

	@GET
	@Path("configurazione/getConfigurazioni")
	public Response getValorizzazioniConfigurazione(
			@QueryParam(value = "periodoRipartizione") String periodoRipartizione,
			@QueryParam(value = "ripartizione") String ripartizione,
			@QueryParam(value = "tipologiaReport") String tipologiaReport, @QueryParam(value = "regola") String regola,
			@QueryParam(value = "voceIncasso") String voceIncasso) {

		return Response.ok(gson.toJson(valorizzazioneService.getConfigurazioni(periodoRipartizione, ripartizione,
				tipologiaReport, regola, voceIncasso))).build();
	}

	@GET
	@Path("configurazione/getStoricoConfigurazione")
	public Response getStoricoConfigurazione(@QueryParam(value = "periodo") String periodo,
			@QueryParam(value = "voceIncasso") String voceIncasso,
			@QueryParam(value = "tipoReport") String tipoReport) {

		List<ConfigurazioniPeriodoDTO> listaVociIncasso = valorizzazioneService.getStoricoConfigurazione(periodo,
				voceIncasso, tipoReport);

		// traceService.trace("Valorizzazione", this.getClass().getName(),
		// "/ValorizzazioneConfigurazione",
		// "Accesso menu Configurazione", Constants.CLICK);
		return Response.ok(gson.toJson(listaVociIncasso)).build();
	}

	@GET
	@Path("getVociIncasso")
	public Response getVociIncasso() {
		List<PerfVoceIncasso> listaVociIncasso = valorizzazioneService.getVociIncasso();

		// traceService.trace("Valorizzazione", this.getClass().getName(),
		// "/ValorizzazioneConfigurazione",
		// "Accesso menu Configurazione", Constants.CLICK);
		return Response.ok(gson.toJson(listaVociIncasso)).build();
	}

	@GET
	@Path("getVociIncassoFittizie")
	public Response getVociIncassoFittizie() {
		List<PerfVoceIncasso> listaVociIncasso = valorizzazioneService.getVociIncassoFittizie();

		// traceService.trace("Valorizzazione", this.getClass().getName(),
		// "/ValorizzazioneConfigurazione",
		// "Accesso menu Configurazione", Constants.CLICK);
		return Response.ok(gson.toJson(listaVociIncasso)).build();
	}

	@GET
	@Path("getPeriodiRipartizione")
	public Response getPeriodiRipartizione() {
		List<PeriodoRipartizione> listaPeriodiRipartizione = valorizzazioneService.getPeriodiRipartizione();

		// traceService.trace("Valorizzazione", this.getClass().getName(),
		// "/ValorizzazioneConfigurazione",
		// "Accesso menu Configurazione", Constants.CLICK);
		return Response.ok(gson.toJson(listaPeriodiRipartizione)).build();
	}

	@GET
	@Path("getCarichiRipartizione")
	public Response getCarichiRipartizione(@QueryParam(value = "codicePeriodo") String codicePeriodo) {
		CarichiRipartizioneResponse carichiRipartizioneResponse = valorizzazioneService
				.getCarichiRipartizione(codicePeriodo);
		return Response.ok(gson.toJson(carichiRipartizioneResponse)).build();
	}

	@GET
	@Path("getInfoValorizzatore")
	public Response getInfoValorizzatore() {
		try{
			ValorizzazioneRunDTO valorizzazioneRunDTO = valorizzazioneService.getValorizzazioneInfo();
			return Response.ok(gson.toJson(valorizzazioneRunDTO)).build();
		}catch (NoResultException e){
			return Response.noContent().build();
		}
	}

	@PUT
	@Path("approvazionePeriodoRipartizione")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response approvazionePeriodoRipartizione(CarichiRipartizioniRequest carichiRipartizioniRequest) {
		Integer result = valorizzazioneService.approvaRipartizioni(carichiRipartizioniRequest);
		if (result == 200) {
			return Response.ok().build();
		} else {
			return Response.status(result).build();
		}
	}

}