package com.alkemytech.sophia.codman.mandato.service.impl;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.ws.rs.PathParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.codman.dto.IngestionDocumentazioneMandatiDTO;
import com.alkemytech.sophia.codman.dto.SocietaTutelaDTO;
import com.alkemytech.sophia.codman.entity.AnagCountry;
import com.alkemytech.sophia.codman.entity.IngestionDocumentazioneMandati;
import com.alkemytech.sophia.codman.entity.SocietaTutela;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.alkemytech.sophia.codman.mandato.service.ISocietaTutelaService;
import com.alkemytech.sophia.codman.rest.PagedResult;
import com.alkemytech.sophia.common.s3.S3Service;
import com.amazonaws.services.s3.AmazonS3URI;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

@Singleton
public class SocietaTutelaService implements ISocietaTutelaService {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private Provider<EntityManager> provider;
	private Provider<S3Service> s3ServiceProvider;
	
	@Inject
	public SocietaTutelaService(@McmdbDataSource Provider<EntityManager> provider, Provider<S3Service> s3ServiceProvider) {
		super();
		this.provider = provider;
		this.s3ServiceProvider = s3ServiceProvider;
	}		
	
	@Override
	public SocietaTutelaDTO get(Integer id) {
		EntityManager em = provider.get();
		SocietaTutela st = em.find(SocietaTutela.class, id );
		AnagCountry country = em.find(AnagCountry.class, st.getHomeTerritoryCode());
		SocietaTutelaDTO dto = new SocietaTutelaDTO();
		dto.setId(st.getId());
		dto.setCodice(st.getCodice());
		dto.setNominativo(st.getNominativo());
		dto.setPosizioneSIAE(st.getPosizioneSIAE());
		dto.setCodiceSIADA(st.getCodiceSIADA());
		dto.setTenant(st.getTenant());		
		dto.setHomeTerritory(country);
		return dto;
	}

	@Override
	public List<SocietaTutelaDTO> getAll() {
		EntityManager em = provider.get();
		String sql = "SELECT ID, " +
	             "       CODICE, " +
	             "       NOMINATIVO, " +
	             "       POSIZIONE_SIAE, " +
	             "       CODICE_SIADA, " +
	             "       TENANT, " +
	             "       ID_COUNTRY, " +
	             "       CODE, " +
	             "       ISO_CODE, " +
	             "       NAME " +
	             "FROM SOCIETA_TUTELA st " +
	             "  JOIN ANAG_COUNTRY c ON st.HOME_TERRITORY = c.ID_COUNTRY";
		Query query = em.createNativeQuery(sql);
		List<Object[]> resultList = query.getResultList();
		List<SocietaTutelaDTO> results = new ArrayList<SocietaTutelaDTO>();
		for (Object[] data : resultList) {
			SocietaTutelaDTO dto = new SocietaTutelaDTO();
			dto.setId((Integer)data[0]);
			dto.setCodice((String)data[1]);
			dto.setNominativo((String)data[2]);
			dto.setPosizioneSIAE((String)data[3]);
			dto.setCodiceSIADA((String)data[4]);
			dto.setTenant((Integer)data[5]);
			AnagCountry country = new AnagCountry();
			country.setIdCountry((String)data[6]);
			country.setCode((String)data[7]);
			country.setIsoCode((String)data[8]);
			country.setName((String)data[9]);
			dto.setHomeTerritory(country);
			results.add(dto);
		}
		return results;
	}

	@Override
	public SocietaTutelaDTO save(SocietaTutelaDTO societa) {
        
		SocietaTutela st = new SocietaTutela(societa.getCodice(), societa.getNominativo(), societa.getHomeTerritory().getIdCountry(),
				societa.getPosizioneSIAE(), societa.getCodiceSIADA(), societa.getTenant() != null ? societa.getTenant() : 0);
        EntityManager em = provider.get();
        try {
            em.getTransaction().begin();            
    		em.persist(st);
            em.flush();
            em.getTransaction().commit();
            em.clear();
            societa.setId(st.getId());
    		return societa;
        } catch (Exception ex) {
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
            throw ex;
        }
	}

	@Override
	public int delete(SocietaTutelaDTO societa) {
		EntityManager em = provider.get();
		SocietaTutela st = new SocietaTutela();
		st.setId(societa.getId());
		try {
			em.getTransaction().begin();
			em.remove(st);
			em.flush();
            em.getTransaction().commit();
            em.clear();			
			return 1;
		} catch (Exception ex) {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			throw ex;
		}
	}

	@Override
	public List<SocietaTutelaDTO> search(String field, String value) {
		StringBuffer jql = new StringBuffer().append("SELECT s FROM SocietaTutela s");
		if (field != null && value != null) {
			jql.append(" WHERE ")
			.append("LOWER(s.").append(field).append(") ")
			.append("LIKE LOWER(CONCAT('%','").append(value).append("','%'))");
		}
		TypedQuery<SocietaTutela> query = provider.get().createQuery(jql.toString(),SocietaTutela.class);
		List<SocietaTutela> entities = query.getResultList();
		List<SocietaTutelaDTO> results = new ArrayList<SocietaTutelaDTO>();
		for (SocietaTutela societaTutela : entities) {
			SocietaTutelaDTO dto = new SocietaTutelaDTO();
			dto.setId(societaTutela.getId());
			dto.setCodice(societaTutela.getCodice());
			dto.setNominativo(societaTutela.getNominativo());
			dto.setPosizioneSIAE(societaTutela.getPosizioneSIAE());
			dto.setCodiceSIADA(societaTutela.getCodiceSIADA());
			dto.setTenant(societaTutela.getTenant());
			AnagCountry country = new AnagCountry();
			country.setIdCountry(societaTutela.getHomeTerritoryCode());
			dto.setHomeTerritory(country);
			results.add(dto);
		}
		return results;
	}

	@Override
	public PagedResult documentation(String codiceSocietaTutela, Integer pageNumber, Integer pageSize, String order) {
		StringBuffer jqlCount = new StringBuffer().append("SELECT count(s) FROM IngestionDocumentazioneMandati s");
		StringBuffer jql = new StringBuffer().append("SELECT s FROM IngestionDocumentazioneMandati s");
		if (codiceSocietaTutela != null ) {
			jql.append(" WHERE s.societa='").append(codiceSocietaTutela).append("'");
			jqlCount.append(" WHERE s.societa='").append(codiceSocietaTutela).append("'");
		}
		if (order!=null) {
			jql.append(" ORDER BY ").append(order).append("");
		}
		int totalCount = ((Long) provider.get().createQuery(jqlCount.toString()).getSingleResult()).intValue();
		TypedQuery<IngestionDocumentazioneMandati> query = provider.get().createQuery(jql.toString(),IngestionDocumentazioneMandati.class);
		query.setFirstResult(pageNumber * pageSize);
		query.setMaxResults(pageSize);
		List<IngestionDocumentazioneMandati> entities = query.getResultList();
		List<IngestionDocumentazioneMandatiDTO> results = new ArrayList<IngestionDocumentazioneMandatiDTO>();
		for (IngestionDocumentazioneMandati ingestionDocumentazioneMandati : entities) {
			IngestionDocumentazioneMandatiDTO dto = new IngestionDocumentazioneMandatiDTO();
			dto.setId(ingestionDocumentazioneMandati.getId());
			dto.setDataInserimento(ingestionDocumentazioneMandati.getDataInserimento());
			dto.setStato(ingestionDocumentazioneMandati.getStato());
			dto.setEsito(ingestionDocumentazioneMandati.getEsito());
			dto.setFileLocation(ingestionDocumentazioneMandati.getFileLocation());
			dto.setFileName(ingestionDocumentazioneMandati.getFileName());
			dto.setSocieta(ingestionDocumentazioneMandati.getSocieta());
			dto.setTotaleOpere(ingestionDocumentazioneMandati.getTotaleOpere());
			dto.setTotaleOpereAcquisite(ingestionDocumentazioneMandati.getTotaleOpereAcquisite());
			dto.setMessage(ingestionDocumentazioneMandati.getMessage());
			results.add(dto);
		}

		final PagedResult pagedResult = new PagedResult();
		if (null != results && results.size() > 0) {
			pagedResult.setRows(results);
			pagedResult.setMaxrows(totalCount).setFirst(pageNumber * 50).setLast(pageNumber * 50 + results.size())
					.setHasNext(pageNumber * 50 + results.size() < totalCount).setHasPrev(pageNumber > 1);
		} else {
			pagedResult.setMaxrows(0).setFirst(0).setLast(0).setHasNext(false).setHasPrev(false);
		}		
		
		return pagedResult;
	}

	@Override
	public ByteArrayOutputStream downloadById(@PathParam("id") Integer id) {
		final EntityManager entityManager = provider.get();
		final IngestionDocumentazioneMandati ingestionDocumentazioneMandati = entityManager
				.find(IngestionDocumentazioneMandati.class, id);
		if (null == ingestionDocumentazioneMandati) {
			throw new RuntimeException();
		}
		final AmazonS3URI s3Uri = new AmazonS3URI("s3://siae-sophia-datalake" + ingestionDocumentazioneMandati.getFileLocation());
		final ByteArrayOutputStream out = new ByteArrayOutputStream();
		s3ServiceProvider.get().download(s3Uri.getBucket(), s3Uri.getKey(), out);
		return out;
	}

	@Override
	public List<SocietaTutelaDTO> getAllByTenant0() {
		EntityManager em = provider.get();
		String sql = "SELECT s" +
				" FROM SocietaTutela s " +
				" JOIN AnagCountry c ON s.homeTerritoryCode = c.idCountry " +
				" WHERE s.tenant = 0";
		TypedQuery<SocietaTutela> query = provider.get().createQuery(sql.toString(),SocietaTutela.class);
		List<SocietaTutela> entities = query.getResultList();
		List<SocietaTutelaDTO> results = new ArrayList<SocietaTutelaDTO>();
		for (SocietaTutela societaTutela : entities) {

			SocietaTutelaDTO dto = new SocietaTutelaDTO();
			dto.setId(societaTutela.getId());
			dto.setCodice(societaTutela.getCodice());
			dto.setNominativo(societaTutela.getNominativo());
			dto.setPosizioneSIAE(societaTutela.getPosizioneSIAE());
			dto.setCodiceSIADA(societaTutela.getCodiceSIADA());
			dto.setTenant(societaTutela.getTenant());

			AnagCountry country = new AnagCountry();
			country.setIdCountry(societaTutela.getHomeTerritoryCode());
			dto.setHomeTerritory(country);
			results.add(dto);
		}
		return results;
	}
}
