package com.alkemytech.sophia.codman.entity.performing;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PERF_SESSIONE_CODIFICA database table.
 * 
 */
@Entity
@Table(name="PERF_SESSIONE_CODIFICA")
@NamedQuery(name="PerfSessioneCodifica.findAll", query="SELECT p FROM PerfSessioneCodifica p")
public class PerfSessioneCodifica implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID_SESSIONE_CODIFICA", unique=true, nullable=false)
	private String idSessioneCodifica;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_FINE_VALID", nullable=false)
	private Date dataFineValid;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATA_INIZIO_VALID", nullable=false, columnDefinition="datetime DEFAULT CURRENT_TIMESTAMP NOT NULL")
	private Date dataInizioValid;

	@Column(name="UTENTE", nullable=false, length=45)
	private String utente;

	//bi-directional many-to-one association to PerfUtilizzazioniAssegnate
	@JsonManagedReference
	@OneToMany(mappedBy="perfSessioneCodifica")
	private List<PerfUtilizzazioniAssegnate> perfUtilizzazioniAssegnates;

	public PerfSessioneCodifica() {
	}

	public String getIdSessioneCodifica() {
		return this.idSessioneCodifica;
	}

	public void setIdSessioneCodifica(String idSessioneCodifica) {
		this.idSessioneCodifica = idSessioneCodifica;
	}

	public Date getDataFineValid() {
		return this.dataFineValid;
	}

	public void setDataFineValid(Date dataFineValid) {
		this.dataFineValid = dataFineValid;
	}

	public Date getDataInizioValid() {
		return this.dataInizioValid;
	}

	public void setDataInizioValid(Date dataInizioValid) {
		this.dataInizioValid = dataInizioValid;
	}

	public String getUtente() {
		return this.utente;
	}

	public void setUtente(String utente) {
		this.utente = utente;
	}

	public List<PerfUtilizzazioniAssegnate> getPerfUtilizzazioniAssegnates() {
		return this.perfUtilizzazioniAssegnates;
	}

	public void setPerfUtilizzazioniAssegnates(List<PerfUtilizzazioniAssegnate> perfUtilizzazioniAssegnates) {
		this.perfUtilizzazioniAssegnates = perfUtilizzazioniAssegnates;
	}

	public PerfUtilizzazioniAssegnate addPerfUtilizzazioniAssegnate(PerfUtilizzazioniAssegnate perfUtilizzazioniAssegnate) {
		getPerfUtilizzazioniAssegnates().add(perfUtilizzazioniAssegnate);
		perfUtilizzazioniAssegnate.setPerfSessioneCodifica(this);

		return perfUtilizzazioniAssegnate;
	}

	public PerfUtilizzazioniAssegnate removePerfUtilizzazioniAssegnate(PerfUtilizzazioniAssegnate perfUtilizzazioniAssegnate) {
		getPerfUtilizzazioniAssegnates().remove(perfUtilizzazioniAssegnate);
		perfUtilizzazioniAssegnate.setPerfSessioneCodifica(null);

		return perfUtilizzazioniAssegnate;
	}

}