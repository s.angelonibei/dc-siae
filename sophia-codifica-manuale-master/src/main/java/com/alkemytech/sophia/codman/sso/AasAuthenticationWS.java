package com.alkemytech.sophia.codman.sso;

import it.siae.aas.AuthenticationServiceSoap;
import it.siae.aas.AuthenticationServiceSoapProxy;
import it.siae.aas.UtenteBackOffice;

import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.List;

public class AasAuthenticationWS {

	/** Web Service port */
	private AuthenticationServiceSoap webServicePort;
	
//	/** Binding provider */
//	private BindingProvider bindingProvider;
	
	/** <init> */
	public AasAuthenticationWS(String endpoint) {
		super();
		final AuthenticationServiceSoapProxy webService = new AuthenticationServiceSoapProxy(endpoint);
		webServicePort = webService.getAuthenticationServiceSoap();
//		bindingProvider = (BindingProvider) webServicePort;
//		bindingProvider.getRequestContext()
//				.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpoint);
	}

	/** checkToken 
	 * @throws RemoteException */
	public String checkToken(String authenticationToken, String applicationCode, String ip) throws RemoteException {
		return webServicePort.checkToken(authenticationToken, applicationCode, ip);
	}
	
	/** expireToken 
	 * @throws RemoteException */
	public void expireToken(String authenticationToken, String ip) throws RemoteException {
		webServicePort.expireToken(authenticationToken, ip);
	}

	/** getUserData 
	 * @throws RemoteException */
	public UtenteBackOffice getUserData(String userName) throws RemoteException {
		return webServicePort.getUserData(userName);
	}
	
	/** getUserGroups 
	 * @throws RemoteException */
	public List<String> getUserGroups(String userName, String applicationCode) throws RemoteException {		
		return Arrays.asList(webServicePort.getUserGroups(userName, applicationCode));
	}

}
