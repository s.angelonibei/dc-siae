package com.alkemytech.sophia.codman.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class NdmVoceFatturaDateLimitsDTO {
	private Date maxDataContabileFattura;
	private Date minDataContabileFattura;
	private Date maxDataFattura;
	private Date minDataFattura;

	public NdmVoceFatturaDateLimitsDTO() {
	}

	public NdmVoceFatturaDateLimitsDTO(Date maxDataContabileFattura, Date minDataContabileFattura, Date maxDataFattura, Date minDataFattura) {
		this.maxDataContabileFattura = maxDataContabileFattura;
		this.minDataContabileFattura = minDataContabileFattura;
		this.maxDataFattura = maxDataFattura;
		this.minDataFattura = minDataFattura;
	}

	public Date getMaxDataContabileFattura() {
		return maxDataContabileFattura;
	}

	public void setMaxDataContabileFattura(Date maxDataContabileFattura) {
		this.maxDataContabileFattura = maxDataContabileFattura;
	}

	public Date getMinDataContabileFattura() {
		return minDataContabileFattura;
	}

	public void setMinDataContabileFattura(Date minDataContabileFattura) {
		this.minDataContabileFattura = minDataContabileFattura;
	}

	public Date getMaxDataFattura() {
		return maxDataFattura;
	}

	public void setMaxDataFattura(Date maxDataFattura) {
		this.maxDataFattura = maxDataFattura;
	}

	public Date getMinDataFattura() {
		return minDataFattura;
	}

	public void setMinDataFattura(Date minDataFattura) {
		this.minDataFattura = minDataFattura;
	}
}
