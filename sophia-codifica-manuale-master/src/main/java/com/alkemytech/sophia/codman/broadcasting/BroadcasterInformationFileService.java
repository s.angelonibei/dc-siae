package com.alkemytech.sophia.codman.broadcasting;

import com.alkemytech.sophia.broadcasting.model.BdcInformationFileEntity;
import com.alkemytech.sophia.broadcasting.service.interfaces.InformationFileEntityService;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

/**
 * Created by Alessandro Russo on 27/11/2017.
 */
@Singleton
@Path("informationfile")
public class BroadcasterInformationFileService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private InformationFileEntityService service;
    private Gson gson;

    @Inject
    public BroadcasterInformationFileService(InformationFileEntityService service, Gson gson) {
        this.service = service;
        this.gson = gson;
    }

    @POST
    @Produces("application/json")
    public Response save(BdcInformationFileEntity bdcInformationFileEntity) {
        try {
            return Response.ok(gson.toJson(service.save(bdcInformationFileEntity))).build();
        }
        catch (Exception e) {
            logger.error(e.getMessage());
        }
        return Response.status(500).build();
    }
}
