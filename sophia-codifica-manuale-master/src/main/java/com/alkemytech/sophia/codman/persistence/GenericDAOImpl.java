package com.alkemytech.sophia.codman.persistence;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import javax.persistence.EntityManager;

import com.google.inject.Inject;

public class GenericDAOImpl<E extends AbstractEntity<?>, PK> implements GenericDAO<E, PK> {

	private final EntityManager entityManager;
	private Class<E> entityClass;
	
	@Inject
	protected GenericDAOImpl(EntityManager entityManager) {
		super();
		this.entityManager = entityManager;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<E> findAll() {
		return entityManager.createNamedQuery(getEntityClass().getSimpleName() + ".GetAll").getResultList();
	}

	@Override
	@SuppressWarnings("unchecked")
	public PK insert(E entity) {
		entityManager.persist(entity);
		return (PK) entity.getId();
	}

	@Override
	public E find(PK id) {
		return entityManager.find(getEntityClass(), id);
	}

	@Override
	public void update(E entity) {
		entityManager.merge(entity);
	}

	@Override
	public void delete(E entity) {
		entityManager.remove(entity);
	}

	@Override
	@SuppressWarnings("unchecked")
	public Class<E> getEntityClass() {
		if (entityClass == null) {
			final Type type = getClass().getGenericSuperclass();
			if (type instanceof ParameterizedType) {
				ParameterizedType paramType = (ParameterizedType) type;
				entityClass = (Class<E>) paramType.getActualTypeArguments()[0];
			} else {
				throw new IllegalArgumentException("Could not guess entity class by reflection");
			}
		}
		return entityClass;
	}

	@Override
	public EntityManager getEntityManager() {
		return entityManager;
	}

}
