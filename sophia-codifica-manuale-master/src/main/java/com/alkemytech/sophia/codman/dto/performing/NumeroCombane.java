package com.alkemytech.sophia.codman.dto.performing;

import java.io.Serializable;
import java.math.BigInteger;

public class NumeroCombane implements Serializable {
    private BigInteger secondaMaggiore;
    private BigInteger secondaMinore;

    public NumeroCombane(BigInteger secondaMaggiore, BigInteger secondaMinore) {
        this.secondaMaggiore = secondaMaggiore;
        this.secondaMinore = secondaMinore;
    }

    public NumeroCombane() {
    }

    public NumeroCombane(int secondaMaggiore, int secondaMinore) {
        this.secondaMaggiore = new BigInteger(""+secondaMaggiore);
        this.secondaMinore = new BigInteger(""+secondaMinore);
    }

    public BigInteger getSecondaMaggiore() {
        return secondaMaggiore;
    }

    public void setSecondaMaggiore(BigInteger secondaMaggiore) {
        this.secondaMaggiore = secondaMaggiore;
    }

    public BigInteger getSecondaMinore() {
        return secondaMinore;
    }

    public void setSecondaMinore(BigInteger secondaMinore) {
        this.secondaMinore = secondaMinore;
    }
}
