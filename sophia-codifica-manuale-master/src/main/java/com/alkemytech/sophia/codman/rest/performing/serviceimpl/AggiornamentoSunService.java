package com.alkemytech.sophia.codman.rest.performing.serviceimpl;


import com.alkemytech.sophia.codman.dto.performing.BonificaDTO;
import com.alkemytech.sophia.codman.dto.performing.csv.BonificaDirettoreListToCsv;
import com.alkemytech.sophia.codman.dto.performing.csv.BonificaEventoPagatoListToCsv;
import com.alkemytech.sophia.codman.dto.performing.csv.BonificaManifestazioneListToCsv;
import com.alkemytech.sophia.codman.dto.performing.csv.BonificaMovimentoContabileListToCsv;
import com.alkemytech.sophia.codman.dto.performing.csv.BonificaProgrammaMusicaleListToCsv;
import com.alkemytech.sophia.codman.entity.performing.dao.IAggiornamentoSunDAO;
import com.alkemytech.sophia.codman.rest.performing.service.IAggiornamentoSunService;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.*;

@Service("aggiornamentoSunService")
public class AggiornamentoSunService implements IAggiornamentoSunService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private IAggiornamentoSunDAO aggiornamentoSunDao;
	private final Properties configuration;

	@Inject
	public AggiornamentoSunService(IAggiornamentoSunDAO aggiornamentoSunDao, Properties configuration) {
		this.configuration = configuration;
		this.aggiornamentoSunDao = aggiornamentoSunDao;
		
	}
	
	@Override
	public List<Date> getDateAggiornameto() {
		return aggiornamentoSunDao.getDateAggiornameto();
	}
	@Override
	public List<BonificaDTO> getAggiornamenti(String data) {
		return aggiornamentoSunDao.getAggiornamenti(data);
	}

	@Override
	public  List<BonificaProgrammaMusicaleListToCsv> getProgrammiMusicale(String dataBonifica) {
		return aggiornamentoSunDao.getProgrammiMusicale(dataBonifica);
	}

	@Override
	public List<BonificaEventoPagatoListToCsv> getEventiPagati(String dataBonifica) {
		return aggiornamentoSunDao.getEventiPagati(dataBonifica);
	}
	
	@Override
	public List<BonificaMovimentoContabileListToCsv> getMovimentiContabili(String dataBonifica) {
		return aggiornamentoSunDao.getMovimentiContabili(dataBonifica);
	}

	@Override
	public List<BonificaManifestazioneListToCsv> getManifestazioni(String dataBonifica) {
		return aggiornamentoSunDao.getManifestazioni(dataBonifica);
	}

	@Override
	public List<BonificaDirettoreListToCsv> getDirettoriEsecuzione(String dataBonifica) {
		return aggiornamentoSunDao.getDirettoriEsecuzione(dataBonifica);
	}
}
