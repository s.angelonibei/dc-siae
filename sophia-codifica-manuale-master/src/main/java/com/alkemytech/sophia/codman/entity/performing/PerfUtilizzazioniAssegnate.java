package com.alkemytech.sophia.codman.entity.performing;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.io.Serializable;


/**
 * The persistent class for the PERF_UTILIZZAZIONI_ASSEGNATE database table.
 * 
 */
@Entity
@Table(name="PERF_UTILIZZAZIONI_ASSEGNATE")
@NamedQuery(name="PerfUtilizzazioniAssegnate.findAll", query="SELECT p FROM PerfUtilizzazioniAssegnate p")
public class PerfUtilizzazioniAssegnate implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private PerfUtilizzazioniAssegnatePK id;

	@Column(name="TIPO_AZIONE", length=1)
	private String tipoAzione;

	//bi-directional many-to-one association to PerfCodificaDTO
	@JsonBackReference
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_CODIFICA", nullable=false)
	private PerfCodifica perfCodifica;

	//bi-directional many-to-one association to PerfSessioneCodificaì
	@JsonBackReference
	@ManyToOne(fetch=FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JoinColumn(name="ID_SESSIONE_CODIFICA", nullable=false)
	private PerfSessioneCodifica perfSessioneCodifica;

	public PerfUtilizzazioniAssegnate() {
	}

	public PerfUtilizzazioniAssegnatePK getId() {
		return this.id;
	}

	public void setId(PerfUtilizzazioniAssegnatePK id) {
		this.id = id;
	}

	public String getTipoAzione() {
		return this.tipoAzione;
	}

	public void setTipoAzione(String tipoAzione) {
		this.tipoAzione = tipoAzione;
	}

	public PerfCodifica getPerfCodifica() {
		return this.perfCodifica;
	}

	public void setPerfCodifica(PerfCodifica perfCodifica) {
		this.perfCodifica = perfCodifica;
	}

	public PerfSessioneCodifica getPerfSessioneCodifica() {
		return this.perfSessioneCodifica;
	}

	public void setPerfSessioneCodifica(PerfSessioneCodifica perfSessioneCodifica) {
		this.perfSessioneCodifica = perfSessioneCodifica;
	}

}