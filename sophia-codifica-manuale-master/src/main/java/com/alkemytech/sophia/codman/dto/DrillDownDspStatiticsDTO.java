package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DrillDownDspStatiticsDTO implements Serializable {

	private static final long serialVersionUID = -1l;
	
	private String societa;
	private BigDecimal identificatoValore;
	private BigDecimal identificatoUtilizzazioni;
	private BigDecimal percentualeIdentificatoUtilizzazioni;
	private BigDecimal claimValore;
	private BigDecimal claimUtilizzazioni;
	private BigDecimal nonIdentificatoValore;
	private BigDecimal nonIdentificatoUtilizzazioni;
	
	public BigDecimal getIdentificatoValore() {
		return identificatoValore;
	}
	public void setIdentificatoValore(BigDecimal identificatoValore) {
		this.identificatoValore = identificatoValore;
	}
	public BigDecimal getIdentificatoUtilizzazioni() {
		return identificatoUtilizzazioni;
	}
	public void setIdentificatoUtilizzazioni(BigDecimal identificatoUtilizzazioni) {
		this.identificatoUtilizzazioni = identificatoUtilizzazioni;
	}
	public BigDecimal getPercentualeIdentificatoUtilizzazioni() {
		return percentualeIdentificatoUtilizzazioni;
	}
	public void setPercentualeIdentificatoUtilizzazioni(BigDecimal percentualeIdentificatoUtilizzazioni) {
		this.percentualeIdentificatoUtilizzazioni = percentualeIdentificatoUtilizzazioni;
	}
	public BigDecimal getClaimValore() {
		return claimValore;
	}
	public void setClaimValore(BigDecimal claimValore) {
		this.claimValore = claimValore;
	}
	public BigDecimal getClaimUtilizzazioni() {
		return claimUtilizzazioni;
	}
	public void setClaimUtilizzazioni(BigDecimal claimUtilizzazioni) {
		this.claimUtilizzazioni = claimUtilizzazioni;
	}
	public BigDecimal getNonIdentificatoValore() {
		return nonIdentificatoValore;
	}
	public void setNonIdentificatoValore(BigDecimal nonIdentificatoValore) {
		this.nonIdentificatoValore = nonIdentificatoValore;
	}
	public BigDecimal getNonIdentificatoUtilizzazioni() {
		return nonIdentificatoUtilizzazioni;
	}
	public void setNonIdentificatoUtilizzazioni(BigDecimal nonIdentificatoUtilizzazioni) {
		this.nonIdentificatoUtilizzazioni = nonIdentificatoUtilizzazioni;
	}
	public String getSocieta() {
		return societa;
	}
	public void setSocieta(String societa) {
		this.societa = societa;
	}
	
}
