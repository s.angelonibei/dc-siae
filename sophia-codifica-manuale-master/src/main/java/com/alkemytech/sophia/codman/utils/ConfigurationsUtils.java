package com.alkemytech.sophia.codman.utils;

import java.util.List;
import java.util.Map;

import com.alkemytech.sophia.codman.entity.KeyValueProperty;
import com.alkemytech.sophia.codman.entity.Setting;
import com.alkemytech.sophia.codman.entity.ThresholdSet;

public class ConfigurationsUtils {
	
	public static void getOverrides(List<Setting> settings, Map<String, Object> overrides) {
		for (Setting setting : settings) {
			if (setting.getSettingType().equals("key_value") || setting.getSettingType().equals("threshold")) {
				KeyValueProperty p = (KeyValueProperty) setting;
				overrides.put(p.getKey(), getValue(p));
			}
			if (setting.getSettingType().equals("threshold_set")) {
				ThresholdSet p = (ThresholdSet) setting;
				overrides.put(p.getKey(), getValue(p));
				getOverrides(p.getThresholds(), overrides);
			}
		}
	}

	public static Object getValue(KeyValueProperty p) {
		if (p.getValueType().equalsIgnoreCase("Decimal")) {
			return Double.parseDouble(p.getValue());
		} else if (p.getValueType().equalsIgnoreCase("Integer")) {
			return Integer.parseInt(p.getValue());
		} else 	if(p.getValueType().equalsIgnoreCase("Boolean")) {
			if(Boolean.parseBoolean(p.getValue()) || "1".equals(p.getValue())) {
				return 1;
			}else {
				return 0;
			}
		} else {
			return p.getValue();
		}
	}

}
