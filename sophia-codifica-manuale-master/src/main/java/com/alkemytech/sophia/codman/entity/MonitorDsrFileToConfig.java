package com.alkemytech.sophia.codman.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity(name = "MonitorDsrFileToConfig")
@Table(name = "MM_MONITOR_DSR_FILE_TO_CONFIG")
@NamedQuery(name = "MonitorDsrFileToConfig.searchForIdConfig", query = "select x from MonitorDsrFileToConfig x where x.idConfig = :idConfig")
public class MonitorDsrFileToConfig implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID_FILE", nullable = false)
    private Integer idFile;

    @Id
    @Column(name = "ID_CONFIG", nullable = false)
    private Integer idConfig;

    
}