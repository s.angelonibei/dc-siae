package com.alkemytech.sophia.codman.entity.performing.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.http.util.TextUtils;

import com.alkemytech.sophia.codman.dto.performing.BonificaDTO;
import com.alkemytech.sophia.codman.dto.performing.csv.BonificaDirettoreListToCsv;
import com.alkemytech.sophia.codman.dto.performing.csv.BonificaEventoPagatoListToCsv;
import com.alkemytech.sophia.codman.dto.performing.csv.BonificaManifestazioneListToCsv;
import com.alkemytech.sophia.codman.dto.performing.csv.BonificaMovimentoContabileListToCsv;
import com.alkemytech.sophia.codman.dto.performing.csv.BonificaProgrammaMusicaleListToCsv;
import com.google.inject.Inject;
import com.google.inject.Provider;


public class AggiornamentoSunDAO implements IAggiornamentoSunDAO{
    private Provider<EntityManager> provider;

    @Inject
    public AggiornamentoSunDAO(Provider<EntityManager> provider) {
        this.provider = provider;
    }


	@Override
	public List<Date> getDateAggiornameto() {
		ArrayList<Date> date= new ArrayList<>();
		try {
			EntityManager em = provider.get();
			String s="SELECT DISTINCT DATE(DATA_BONIFICA) \n" + 
					"FROM ( \n" + 
					"SELECT DISTINCT \n" + 
					"    (DATA_BONIFICA)  \n" + 
					"FROM \n" + 
					"    PERF_MANIFESTAZIONE_BONIFICA  \n" + 
					"UNION ALL SELECT DISTINCT \n" + 
					"    (DATA_BONIFICA) \n" + 
					"FROM \n" + 
					"    PERF_MOVIMENTO_CONTABILE_BONIFICA  \n" + 
					"UNION ALL SELECT DISTINCT \n" + 
					"    (DATA_BONIFICA) \n" + 
					"FROM \n" + 
					"    PERF_DIRETTORE_ESECUZIONE_BONIFICA  \n" + 
					"UNION ALL SELECT DISTINCT \n" + 
					"    (DATA_BONIFICA) \n" + 
					"FROM \n" + 
					"    PERF_EVENTI_PAGATI_BONIFICA  \n" + 
					"UNION ALL SELECT DISTINCT \n" + 
					"    (DATA_BONIFICA) \n" + 
					"FROM \n" + 
					"    PERF_PROGRAMMA_MUSICALE_BONIFICA pm) AS DATA_BONIFICA_TOTALE \n" + 
					"    order by DATA_BONIFICA DESC; ";
		
			Query q = em.createNativeQuery(s);
			
			List<Object[]> result = q.getResultList();
			for (Object row : result) {
				date.add((Date) row);
			}	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return date;
	}

	@Override
	public List<BonificaDTO> getAggiornamenti(String data) {

		EntityManager em = provider.get();
		String s="SELECT \n" + 
				"DATA_BONIFICA,\n" + 
				"    COUNT(DATA_BONIFICA) AS RECORD_TOTALI,\n" + 
				"    \n" + 
				"    SUM(\n" + 
				"		IF(RECORD_ELABORATO in (0,3),  IF(TIPO = 'MANIFESTAZIONE',1,0),0)\n" + 
				"	)AS RECORD_NON_AGGIORNATI_MANIFESTAZIONE,\n" + 
				"    \n" + 
				"	SUM(\n" + 
				"		IF(RECORD_ELABORATO in (0,3), IF(TIPO = 'MOVIMENTO_CONTABILE',1,0),0)\n" + 
				"	)AS RECORD_NON_AGGIORNATI_MOVIMENTO_CONTABILE,\n" + 
				"    \n" + 
				"	SUM(\n" + 
				"		IF(RECORD_ELABORATO in (0,3),  IF(TIPO = 'DIRETTORE',1,0),0)\n" + 
				"	)AS RECORD_NON_AGGIORNATI_DIRETTORE,\n" + 
				"    \n" + 
				"    SUM(\n" + 
				"		IF(RECORD_ELABORATO in (0,3),  IF(TIPO = 'EVENTI_PAGATI',1,0),0)\n" + 
				"	)AS RECORD_NON_AGGIORNATI_EVENTI_PAGATI,\n" + 
				"    \n" + 
				"    \n" + 
				"    SUM(\n" + 
				"		IF(RECORD_ELABORATO in (0,3),  IF(TIPO = 'PROGRAMMA_MUSICALE',1,0),0)\n" + 
				"	)AS RECORD_NON_AGGIORNATI_PROGRAMMA_MUSICALE,\n" + 
				"    \n" + 
				"    SUM(IF(RECORD_ELABORATO = 1, 1, 0)) AS RECORD_INSERITI,\n" + 
				"    SUM(IF(RECORD_ELABORATO = 2, 1, 0)) AS RECORD_AGGIORNATI,\n" + 
				"    SUM(IF(RECORD_ELABORATO = 4, 1, 0)) AS RECORD_IDENTICI\n" + 
				"FROM\n" + 
				"    (SELECT \n" + 
				"        DATA_BONIFICA, RECORD_ELABORATO,'MANIFESTAZIONE' as TIPO\n" + 
				"    FROM\n" + 
				"        PERF_MANIFESTAZIONE_BONIFICA UNION ALL SELECT \n" + 
				"        DATA_BONIFICA, RECORD_ELABORATO,'MOVIMENTO_CONTABILE' as TIPO\n" + 
				"    FROM\n" + 
				"        PERF_MOVIMENTO_CONTABILE_BONIFICA UNION ALL SELECT \n" + 
				"        DATA_BONIFICA, RECORD_ELABORATO,'DIRETTORE' as TIPO\n" + 
				"    FROM\n" + 
				"        PERF_DIRETTORE_ESECUZIONE_BONIFICA UNION ALL SELECT \n" + 
				"        DATA_BONIFICA, RECORD_ELABORATO,'EVENTI_PAGATI' as TIPO\n" + 
				"    FROM\n" + 
				"        PERF_EVENTI_PAGATI_BONIFICA UNION ALL SELECT \n" + 
				"        DATA_BONIFICA, RECORD_ELABORATO,'PROGRAMMA_MUSICALE' as TIPO\n" + 
				"    FROM\n" + 
				"        PERF_PROGRAMMA_MUSICALE_BONIFICA pm) AS DATA_BONIFICA_TOTALE\n";
				if (!TextUtils.isEmpty(data)) {
					s=s+" Where DATE(DATA_BONIFICA)=DATE(?1)"; 
				}
				s=s+"    group by DATA_BONIFICA\n" + 
				" ORDER BY DATA_BONIFICA;";
	
		Query q = em.createNativeQuery(s);
		if (!TextUtils.isEmpty(data)) {
			q.setParameter(1, data);
		}
		ArrayList<BonificaDTO> bonifiche= new ArrayList<>();
		List<Object[]> result = q.getResultList();
		for (Object[] row : result) {
			bonifiche.add(new BonificaDTO(row));
		}
		
		return bonifiche;
	}



	@Override
	public List<BonificaProgrammaMusicaleListToCsv> getProgrammiMusicale(String dataBonifica) {

		EntityManager em = provider.get();
		String s="(SELECT\n" + 
				"'SUN' AS ORIGINE_RECORD,\n" + 
				"ID_PROGRAMMA_MUSICALE,\n" + 
				"NUMERO_PROGRAMMA_MUSICALE,\n" + 
				"TIPO_PM,\n" + 
				"SUPPORTO_PM,\n" + 
				"FOGLIO,\n" + 
				"DATA_ASSEGNAZIONE,\n" + 
				"DATA_RESTITUZIONE,\n" + 
				"DATA_COMPILAZIONE,\n" + 
				"STATO_PM,\n" + 
				"DATA_ANNULLAMENTO,\n" + 
				"FOGLIO_SEGUE_DI,\n" + 
				"PM_RIFERIMENTO,\n" + 
				"CAUSA_ANNULLAMENTO,\n" + 
				"STATO_WEB,\n" + 
				"TOTALE_CEDOLE,\n" + 
				"TOTALE_DURATA_CEDOLE,\n" + 
				"GIORNATE_TRATTENIMENTO,\n" + 
				"FLAG_GRUPPO_PRINCIPALE\n" + 
				"FROM\n" + 
				"PERF_PROGRAMMA_MUSICALE_BONIFICA\n" + 
				"WHERE DATE(DATA_BONIFICA)=DATE(?1)\n" + 
				"AND RECORD_ELABORATO IN (0,3))\n" + 
				"UNION ALL\n" + 
				"(SELECT\n" + 
				"'SOPHIA',\n" + 
				"ID_PROGRAMMA_MUSICALE,\n" + 
				"NUMERO_PROGRAMMA_MUSICALE,\n" + 
				"TIPO_PM,\n" + 
				"SUPPORTO_PM,\n" + 
				"FOGLIO,\n" + 
				"DATA_ASSEGNAZIONE,\n" + 
				"DATA_RESTITUZIONE,\n" + 
				"DATA_COMPILAZIONE,\n" + 
				"STATO_PM,\n" + 
				"DATA_ANNULLAMENTO,\n" + 
				"FOGLIO_SEGUE_DI,\n" + 
				"PM_RIFERIMENTO,\n" + 
				"CAUSA_ANNULLAMENTO,\n" + 
				"STATO_WEB,\n" + 
				"TOTALE_CEDOLE,\n" + 
				"TOTALE_DURATA_CEDOLE,\n" + 
				"GIORNATE_TRATTENIMENTO,\n" + 
				"FLAG_GRUPPO_PRINCIPALE\n" + 
				"FROM\n" + 
				"PERF_PROGRAMMA_MUSICALE\n" + 
				"where ID_PROGRAMMA_MUSICALE in\n" + 
				"(SELECT\n" + 
				"ID_PROGRAMMA_MUSICALE\n" + 
				"FROM\n" + 
				"PERF_PROGRAMMA_MUSICALE_BONIFICA\n" + 
				"WHERE DATE(DATA_BONIFICA)=DATE(?1)\n" + 
				"AND RECORD_ELABORATO IN (0,3)))\n" + 
				"order by ID_PROGRAMMA_MUSICALE;";
	
		Query q = em.createNativeQuery(s);
		if (!TextUtils.isEmpty(dataBonifica)) {
			q.setParameter(1, dataBonifica);
		}
		ArrayList<BonificaProgrammaMusicaleListToCsv> bonifiche= new ArrayList<>();
		List<Object[]> result = q.getResultList();
		for (Object[] row : result) {
			bonifiche.add(new BonificaProgrammaMusicaleListToCsv(row));
		}
		
		return bonifiche;
	}


	@Override
	public List<BonificaEventoPagatoListToCsv> getEventiPagati(String dataBonifica) {

		EntityManager em = provider.get();
		String s=
				"(SELECT \n" + 
				"'SUN' AS ORIGINE_RECORD,\n" + 
				"ID_EVENTO,\n" + 
				"VOCE_INCASSO,\n" + 
				"SEPRAG,\n" + 
				"NUMERO_REVERSALE,\n" + 
				"IMPORTO_DEM,\n" + 
				"CONTABILITA,\n" + 
				"TIPO_DOCUMENTO_CONTABILE,\n" + 
				"NUMERO_FATTURA,\n" + 
				"DENOMINAZIONE_LOCALE,\n" + 
				"CODICE_BA_LOCALE,\n" + 
				"CODICE_SPEI_LOCALE,\n" + 
				"DATA_INIZIO_EVENTO,\n" + 
				"ORA_INIZIO_EVENTO,\n" + 
				"NUMERO_PM_ATTESI,\n" + 
				"NUMERO_PM_ATTESI_SPALLA,\n" + 
				"CODICE_RAGGRUPPAMENTO,\n" + 
				"SEDE,\n" + 
				"AGENZIA,\n" + 
				"PERMESSO,\n" + 
				"DATA_REVERSALE\n" + 
				"FROM PERF_EVENTI_PAGATI_BONIFICA\n" + 
				"WHERE DATE(DATA_BONIFICA)=DATE(?1)\n" + 
				"AND RECORD_ELABORATO IN (0,3))\n" + 
				"UNION ALL\n" + 
				"(SELECT \n" + 
				"'SOPHIA',\n" + 
				"ID_EVENTO,\n" + 
				"VOCE_INCASSO,\n" + 
				"SEPRAG,\n" + 
				"NUMERO_REVERSALE,\n" + 
				"IMPORTO_DEM,\n" + 
				"CONTABILITA,\n" + 
				"TIPO_DOCUMENTO_CONTABILE,\n" + 
				"NUMERO_FATTURA,\n" + 
				"DENOMINAZIONE_LOCALE,\n" + 
				"CODICE_BA_LOCALE,\n" + 
				"CODICE_SPEI_LOCALE,\n" + 
				"DATA_INIZIO_EVENTO,\n" + 
				"ORA_INIZIO_EVENTO,\n" + 
				"NUMERO_PM_ATTESI,\n" + 
				"NUMERO_PM_ATTESI_SPALLA,\n" + 
				"CODICE_RAGGRUPPAMENTO,\n" + 
				"SEDE,\n" + 
				"AGENZIA,\n" + 
				"PERMESSO,\n" + 
				"DATA_REVERSALE\n" + 
				"FROM PERF_EVENTI_PAGATI\n" + 
				"WHERE ID_EVENTO IN ( SELECT \n" + 
				"ID_EVENTO FROM PERF_EVENTI_PAGATI_BONIFICA\n" + 
				"WHERE DATE(DATA_BONIFICA)=DATE(?1)\n" + 
				"AND RECORD_ELABORATO IN (0,3)))\n" + 
				"order by ID_EVENTO;";
	
		Query q = em.createNativeQuery(s);
		if (!TextUtils.isEmpty(dataBonifica)) {
			q.setParameter(1, dataBonifica);
		}
		
		ArrayList<BonificaEventoPagatoListToCsv> bonifiche= new ArrayList<>();
		List<Object[]> result = q.getResultList();
		for (Object[] row : result) {
			bonifiche.add(new BonificaEventoPagatoListToCsv(row));
		}
		
		return bonifiche;
	}


	@Override
	public List<BonificaMovimentoContabileListToCsv> getMovimentiContabili(String dataBonifica) {

		EntityManager em = provider.get();
		String s="(SELECT\n" + 
				"'SUN' AS ORIGINE_RECORD,\n" + 
				"ID_PROGRAMMA_MUSICALE,\n" + 
				"ID_EVENTO,\n" + 
				"NUMERO_PM,\n" + 
				"TIPOLOGIA_MOVIMENTO,\n" + 
				"IMPORTO_MANUALE,\n" + 
				"CODICE_VOCE_INCASSO,\n" + 
				"NUMERO_PERMESSO,\n" + 
				"TOTALE_DEM_LORDO_PM,\n" + 
				"NUMERO_REVERSALE,\n" + 
				"FLAG_GRUPPO_PRINCIPALE,\n" + 
				"NUMERO_FATTURA,\n" + 
				"IMPORTO_DEM_TOTALE,\n" + 
				"NUMERO_PM_PREVISTI,\n" + 
				"NUMERO_PM_PREVISTI_SPALLA,\n" + 
				"DATA_REVERSALE,\n" + 
				"DATA_RIENTRO,\n" + 
				"CONTABILITA,\n" + 
				"TIPO_DOCUMENTO_CONTABILE,\n" + 
				"CODICE_RAGGRUPPAMENTO,\n" + 
				"SEDE,\n" + 
				"AGENZIA\n" + 
				"FROM\n" + 
				"PERF_MOVIMENTO_CONTABILE_BONIFICA\n" + 
				"WHERE DATE(DATA_BONIFICA)=DATE(?1)\n" + 
				"AND RECORD_ELABORATO IN (0,3))\n" + 
				"UNION ALL\n" + 
				"(SELECT \n" + 
				"'SOPHIA',\n" + 
				"ID_PROGRAMMA_MUSICALE,\n" + 
				"ID_EVENTO,\n" + 
				"NUMERO_PM,\n" + 
				"TIPOLOGIA_MOVIMENTO,\n" + 
				"IMPORTO_MANUALE,\n" + 
				"CODICE_VOCE_INCASSO,\n" + 
				"NUMERO_PERMESSO,\n" + 
				"TOTALE_DEM_LORDO_PM,\n" + 
				"NUMERO_REVERSALE,\n" + 
				"FLAG_GRUPPO_PRINCIPALE,\n" + 
				"NUMERO_FATTURA,\n" + 
				"IMPORTO_DEM_TOTALE,\n" + 
				"NUMERO_PM_PREVISTI,\n" + 
				"NUMERO_PM_PREVISTI_SPALLA,\n" + 
				"DATA_REVERSALE,\n" + 
				"DATA_RIENTRO,\n" + 
				"CONTABILITA,\n" + 
				"TIPO_DOCUMENTO_CONTABILE,\n" + 
				"CODICE_RAGGRUPPAMENTO,\n" + 
				"SEDE,\n" + 
				"AGENZIA\n" + 
				"FROM \n" + 
				"PERF_MOVIMENTO_CONTABILE\n" + 
				"WHERE (ID_PROGRAMMA_MUSICALE,ID_EVENTO) IN (SELECT \n" + 
				"ID_PROGRAMMA_MUSICALE,\n" + 
				"ID_EVENTO\n" + 
				"FROM PERF_MOVIMENTO_CONTABILE_BONIFICA\n" + 
				"WHERE DATE(DATA_BONIFICA)=DATE(?1)\n" + 
				"AND RECORD_ELABORATO IN (0,3)))\n" + 
				"order by ID_PROGRAMMA_MUSICALE,\n" + 
				"ID_EVENTO ;";
	
		Query q = em.createNativeQuery(s);
		if (!TextUtils.isEmpty(dataBonifica)) {
			q.setParameter(1, dataBonifica);
		}
		ArrayList<BonificaMovimentoContabileListToCsv> bonifiche= new ArrayList<>();
		List<Object[]> result = q.getResultList();
		for (Object[] row : result) {
			bonifiche.add(new BonificaMovimentoContabileListToCsv(row));
		}
		
		return bonifiche;
	}


	@Override
	public List<BonificaManifestazioneListToCsv> getManifestazioni(String dataBonifica) {

		EntityManager em = provider.get();
		String s="(SELECT \n" + 
				"'SUN' AS ORIGINE_RECORD,\n" + 
				"ID_EVENTO,\n" + 
				"DATA_INIZIO_EVENTO,\n" + 
				"DATA_FINE_EVENTO,\n" + 
				"ORA_INIZIO,\n" + 
				"ORA_FINE,\n" + 
				"CODICE_LOCALE,\n" + 
				"DENOMINAZIONE_LOCALE,\n" + 
				"CODICE_COMUNE,\n" + 
				"COMUNE_LOCALE,\n" + 
				"PROVINCIA_LOCALE,\n" + 
				"REGIONE_LOCALE,\n" + 
				"CODICE_BA_LOCALE,\n" + 
				"CAP_LOCALE,\n" + 
				"SIGLA_LOCALE,\n" + 
				"SPAZIO_LOCALE,\n" + 
				"GENERE_LOCALE,\n" + 
				"DENOMINAZIONE_LOCALITA,\n" + 
				"CODICE_SAP_ORGANIZZATORE,\n" + 
				"PARTITA_IVA_ORGANIZZATORE,\n" + 
				"CODICE_FISCALE_ORGANIZZATORE,\n" + 
				"NOME_ORGANIZZATORE,\n" + 
				"COMUNE_ORGANIZZATORE,\n" + 
				"PROVINCIA_ORGANIZZATORE,\n" + 
				"TITOLO,\n" + 
				"TITOLO_ORIGINALE\n" + 
				"FROM \n" + 
				"PERF_MANIFESTAZIONE_BONIFICA\n" + 
				"WHERE DATE(DATA_BONIFICA)=DATE(?1)\n" + 
				"AND RECORD_ELABORATO IN (0,3))\n" + 
				"UNION ALL\n" + 
				"(SELECT \n" + 
				"'SOPHIA',\n" + 
				"ID_EVENTO,\n" + 
				"DATA_INIZIO_EVENTO,\n" + 
				"DATA_FINE_EVENTO,\n" + 
				"ORA_INIZIO,\n" + 
				"ORA_FINE,\n" + 
				"CODICE_LOCALE,\n" + 
				"DENOMINAZIONE_LOCALE,\n" + 
				"CODICE_COMUNE,\n" + 
				"COMUNE_LOCALE,\n" + 
				"PROVINCIA_LOCALE,\n" + 
				"REGIONE_LOCALE,\n" + 
				"CODICE_BA_LOCALE,\n" + 
				"CAP_LOCALE,\n" + 
				"SIGLA_LOCALE,\n" + 
				"SPAZIO_LOCALE,\n" + 
				"GENERE_LOCALE,\n" + 
				"DENOMINAZIONE_LOCALITA,\n" + 
				"CODICE_SAP_ORGANIZZATORE,\n" + 
				"PARTITA_IVA_ORGANIZZATORE,\n" + 
				"CODICE_FISCALE_ORGANIZZATORE,\n" + 
				"NOME_ORGANIZZATORE,\n" + 
				"COMUNE_ORGANIZZATORE,\n" + 
				"PROVINCIA_ORGANIZZATORE,\n" + 
				"TITOLO,\n" + 
				"TITOLO_ORIGINALE\n" + 
				"FROM \n" + 
				"PERF_MANIFESTAZIONE\n" + 
				"WHERE ID_EVENTO IN (SELECT ID_EVENTO\n" + 
				"FROM PERF_MANIFESTAZIONE_BONIFICA\n" + 
				"WHERE DATE(DATA_BONIFICA)=DATE(?1)\n" + 
				"AND RECORD_ELABORATO IN (0,3)))\n" + 
				"order by ID_EVENTO;";
		Query q = em.createNativeQuery(s);
		if (!TextUtils.isEmpty(dataBonifica)) {
			q.setParameter(1, dataBonifica);
		}
		ArrayList<BonificaManifestazioneListToCsv> bonifiche= new ArrayList<>();
		List<Object[]> result = q.getResultList();
		for (Object[] row : result) {
			bonifiche.add(new BonificaManifestazioneListToCsv(row));
		}
		
		return bonifiche;
	}


	@Override
	public List<BonificaDirettoreListToCsv> getDirettoriEsecuzione(String dataBonifica) {

		EntityManager em = provider.get();
		String s="\n" + 
				"(SELECT\n" + 
				"'SUN' AS ORIGINE_RECORD,\n" + 
				"ID_PROGRAMMA_MUSICALE, \n" + 
				"SIGLA_PROVINCIA_NASCITA,\n" + 
				"SIGLA_PROVINCIA,\n" + 
				"SESSO,\n" + 
				"POSIZIONE_SIAE,\n" + 
				"NUMERO_PROGRAMMA_MUSICALE,\n" + 
				"INDIRIZZO,\n" + 
				"DIRETTORE_ESECUZIONE,\n" + 
				"DATA_NASCITA,\n" + 
				"COMUNE_NASCITA,\n" + 
				"COMUNE,\n" + 
				"CODICE_FISCALE,\n" + 
				"CIVICO_RESIDENZA,\n" + 
				"CAP\n" + 
				"FROM PERF_DIRETTORE_ESECUZIONE_BONIFICA\n" + 
				"WHERE DATE(DATA_BONIFICA)=DATE(?1)\n" + 
				"AND RECORD_ELABORATO IN (0,3))\n" + 
				"UNION ALL\n" + 
				"(SELECT\n" + 
				"'SOPHIA',\n" + 
				"ID_PROGRAMMA_MUSICALE,\n" + 
				"SIGLA_PROVINCIA_NASCITA,\n" + 
				"SIGLA_PROVINCIA,\n" + 
				"SESSO,\n" + 
				"POSIZIONE_SIAE,\n" + 
				"NUMERO_PROGRAMMA_MUSICALE,\n" + 
				"INDIRIZZO,\n" + 
				"DIRETTORE_ESECUZIONE,\n" + 
				"DATA_NASCITA,\n" + 
				"COMUNE_NASCITA,\n" + 
				"COMUNE,\n" + 
				"CODICE_FISCALE,\n" + 
				"CIVICO_RESIDENZA,\n" + 
				"CAP\n" + 
				"FROM PERF_DIRETTORE_ESECUZIONE\n" + 
				"WHERE ID_PROGRAMMA_MUSICALE IN (SELECT \n" + 
				"ID_PROGRAMMA_MUSICALE\n" + 
				"FROM \n" + 
				"PERF_DIRETTORE_ESECUZIONE_BONIFICA\n" + 
				"WHERE DATE(DATA_BONIFICA)=DATE(?1)\n" + 
				"AND RECORD_ELABORATO IN (0,3)))\n" + 
				"order by ID_PROGRAMMA_MUSICALE;";
	
		Query q = em.createNativeQuery(s);
		if (!TextUtils.isEmpty(dataBonifica)) {
			q.setParameter(1, dataBonifica);
		}
		ArrayList<BonificaDirettoreListToCsv> bonifiche= new ArrayList<>();
		List<Object[]> result = q.getResultList();
		for (Object[] row : result) {
			bonifiche.add(new BonificaDirettoreListToCsv(row));
		}
		
		return bonifiche;
	}
}
