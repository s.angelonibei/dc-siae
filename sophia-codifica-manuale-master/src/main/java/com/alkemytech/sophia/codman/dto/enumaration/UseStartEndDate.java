package com.alkemytech.sophia.codman.dto.enumaration;

import java.util.HashMap;
import java.util.Map;

public enum UseStartEndDate {
    yyyy("yyyy"),
    yyyy_mm("yyyy-mm"),
    yyyy_mm_dd("yyyy-mm-dd"),
    yyyy_qq("yyyy-Qq"),
    yyyy_www("yyyy-Www"),
    yyyy_mm_dd__yyyy_mm_dd("yyyy-mm-dd--yyyy-mm-dd");

    private static class Holder {
        static Map<String, UseStartEndDate> MAP = new HashMap<>();
    }

    UseStartEndDate(String s) {
        Holder.MAP.put(s, this);
    }

    public static UseStartEndDate find(String val) {
        UseStartEndDate t = Holder.MAP.get(val);
        if(t == null) {
            throw new IllegalStateException(String.format("Unsupported type %s.", val));
        }
        return t;
    }

    public static String findValueBykey(String key) {
        for(Map.Entry<String, UseStartEndDate> set : Holder.MAP.entrySet()){
            if(set.getValue().name().equals(key)){
                return set.getKey();
            }
        }
        throw new IllegalStateException(String.format("Unsupported type %s.", key));
    }

}
