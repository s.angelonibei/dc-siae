package com.alkemytech.sophia.codman.entity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OrderBy;

import com.google.common.collect.Iterables;
import com.google.common.collect.Iterators;

@Entity
@DiscriminatorValue("threshold_set")
public class ThresholdSet extends KeyValueProperty {

	private static final long serialVersionUID = 1L;
	@ManyToMany(cascade = CascadeType.PERSIST)
	@JoinTable(name = "PERF_SETTING_THRESHOLDS", joinColumns = { @JoinColumn(name = "THRESHOLD_SET_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "THRESHOLDS_ID") })
	@OrderBy("ordinal ASC")
	private List<Setting> thresholds = new ArrayList<Setting>();

	public List<Setting> getThresholds() {
		return thresholds;
	}

	public void setThresholds(List<Setting> thresholds) {
		this.thresholds = thresholds;
	}

    @Override
    public Iterator<Setting> iterator() {
    	Iterator<Setting> iterator = super.iterator();
    	Iterator<Setting> set = Iterables.concat(thresholds).iterator();
    	return Iterators.concat(iterator,set);
    }


}
