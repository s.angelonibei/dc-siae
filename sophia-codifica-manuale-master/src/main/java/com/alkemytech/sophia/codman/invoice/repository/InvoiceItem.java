package com.alkemytech.sophia.codman.invoice.repository;


import com.alkemytech.sophia.codman.persistence.AbstractEntity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
@Entity(name = "InvoiceItem")
@Table(name = "INVOICE_ITEM")
@NamedQueries({@NamedQuery(name = "InvoiceItem.GetAll", query = "SELECT x FROM InvoiceItem x")})
@SuppressWarnings("serial")

public class InvoiceItem extends AbstractEntity<String> {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "ID_ITEM", nullable = false)
    private Integer idItem;

    @ManyToOne
    @JoinColumn(name="ID_INVOICE")
    private Invoice invoice;

    @JoinColumn(name="DESCRIPTION")
    private InvoiceItemReason description;

    @Column(name = "INVOICE_POSITION")
    private Integer invoicePosition;

    @Column(name = "TOTAL")
    private BigDecimal total;

    @Column(name = "NOTE")
    private String note;

    @Column(name = "TOTAL_ORIG_CURRENCY")
    private BigDecimal totalOrigCurrency;
    
    @Column(name = "ORIG_CURRENCY")
    private String origCurrency;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name="ID_INVOICE_ITEM")
	private List<InvoiceItemToCcid> invoiceItemToCcid;

    @Override
    public String getId() {
        return getIdItem().toString();
    }

    @Transient
    private List<String> invoiceItemListId = new ArrayList<String>();
    
    public Integer getIdItem() {
        return idItem;
    }

    public void setIdItem(Integer idItem) {
        this.idItem = idItem;
    }

    public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	public InvoiceItemReason getDescription() {
		return description;
	}

	public void setDescription(InvoiceItemReason description) {
		this.description = description;
	}

	public Integer getInvoicePosition() {
        return invoicePosition;
    }

    public void setInvoicePosition(Integer invoicePosition) {
        this.invoicePosition = invoicePosition;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }


	public List<String> getInvoiceItemListId() {
		return invoiceItemListId;
	}

	public void setInvoiceItemListId(List<String> invoiceItemListId) {
		this.invoiceItemListId = invoiceItemListId;
	}

	public BigDecimal getTotalOrigCurrency() {
		return totalOrigCurrency;
	}

	public void setTotalOrigCurrency(BigDecimal totalOrigCurrency) {
		this.totalOrigCurrency = totalOrigCurrency;
	}

	public String getOrigCurrency() {
		return origCurrency;
	}

	public void setOrigCurrency(String origCurrency) {
		this.origCurrency = origCurrency;
	}

	public List<InvoiceItemToCcid> getInvoiceItemToCcid() {
		return invoiceItemToCcid;
	}

	public void setInvoiceItemToCcid(List<InvoiceItemToCcid> invoiceItemToCcid) {
		this.invoiceItemToCcid = invoiceItemToCcid;
	}


}
