package com.alkemytech.sophia.codman.rest.performing.service;

import com.alkemytech.sophia.codman.entity.Configuration;
import com.alkemytech.sophia.codman.entity.performing.Circoscrizione;
import com.alkemytech.sophia.codman.entity.performing.Configurazione;

import java.util.List;

/**
 * Created by idilello on 7/27/16.
 */
public interface IConfigurationService {

    public String getParameter(String parameterName);

    public void insertParameter(String parameterName, String parameterValue);

    public Configurazione getParameterObject(String parameterName);

    public void insertParameterObject(Configurazione configurazione);

    public List<Circoscrizione> getCircoscrizioni();

    public void updateConfigurazione(String annoFinePeriodoRipartizione, String meseFinePeriodoRipartizione,
                                     String annoInizioPeriodoContabile, String meseInizioPeriodoContabile,
                                     String annoFinePeriodoContabile, String meseFinePeriodoContabile,
                                     String tipologiaPM, String campionaDigitali);
    
    public List<Configuration> getConfiguration(String domain, String key);
    
    public Configuration getConfiguration(long id);
    
    public List<Configuration> getConfigurationHistory(String domain, String key, boolean settings);

	public Configuration saveConfiguration(Configuration configuration);

	public void delete(Configuration configuration);

}
