package com.alkemytech.sophia.codman.rest;

import com.alkemytech.sophia.codman.dto.MonitorDspListDTO;
import com.alkemytech.sophia.codman.dto.MonitorDsrDTO;
import com.alkemytech.sophia.codman.entity.*;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.alkemytech.sophia.common.s3.S3Service;
import com.amazonaws.services.s3.AmazonS3URI;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.*;

@Singleton
@Path("monitorDsr")
public class MonitorDsrService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final Properties configuration;
    private final Provider<EntityManager> provider;
    private final Gson gson;
    private final S3Service s3Service;

    private final static String EXT = ".gz";
    private final static String SLASHDELIMITER = "/";
    private final static String DOTDELIMITER = ".";
    private final static String QUARTER = "Q";
    private final static String BACKCLAIM = "_BC_";
    private final static String MENSILE = "mensile";
    private final static String TRIMESTRALE = "trimestrale";
    private final static String SEMAFORO = "semaforo";

    @Inject
    protected MonitorDsrService(@Named("configuration") Properties configuration,
                                @McmdbDataSource Provider<EntityManager> provider, Gson gson, S3Service s3Service) {
        super();
        this.configuration = configuration;
        this.provider = provider;
        this.gson = gson;
        this.s3Service = s3Service;
    }

    @PUT
    @Path("getDashboardDsp")
    public Response getDashboardDsp(MonitorDspListDTO dspList){
            EntityManager entityManager = provider.get();
            List<JsonObject> jsonList = new ArrayList<>();
            Map<String, List<JsonObject>> result = new HashMap<>();
            List<String> periodToConvert;

            for (MonitorDsrDTO dsp : dspList.getList()) {

                periodToConvert = matchCorrectPeriod(dspList.getPeriodo(), dsp.getFrequenzaDsr());
                StringBuffer query = new StringBuffer();
                StringBuffer queryU = new StringBuffer();

                    query.append("SELECT mmdd.*,")
                            .append(" mmdf.NOME_FILE, mmdg.OFFERTA_COMMERCIALE, mmdg.TERRITORIO, mmdg.TIPO_UTILIZZO, mmdg.DSR_ATTESI")
                            .append(" from MM_MONITOR_DSP_DASHBOARD mmdd, MM_MONITOR_DSR_FILE_TO_CONFIG mmg, MM_MONITOR_DSR_FILE mmdf, MM_MONITOR_DSR_CONFIG mmdg")
                            .append(" where mmdd.ID_CONFIG = mmg.ID_CONFIG")
                            .append(" and mmg.ID_FILE = mmdf.ID")
                            .append(" and mmdd.ID_CONFIG = mmdg.ID_CONFIG")
                            .append(" and mmdf.ID_DSP = mmdd.ID_DSP")
                            .append(" and mmdd.ID_DSP = '" + dsp.getIdDsp() + "'")
                            .append(" and mmdd.PERIODO = '" + dspList.getPeriodo() + "'")
                            .append(" and mmdf.PERIODO in (");
                        if (periodToConvert.size() == 1) {
                            query.append(periodToConvert.get(0));
                        } else {
                            query.append(periodToConvert.get(0) + ",")
                                    .append(periodToConvert.get(1) + ",")
                                    .append(periodToConvert.get(2));
                        }
                        query.append(")");

                    Query q = entityManager.createNativeQuery(query.toString());
                    List<Object[]> resultList = q.getResultList();
                    logger.info("Returned Identified:" + resultList.toString());


                    queryU.append("SELECT mmdd.ID, mmdd.ID_DSP, mmdd.PERIODO, mmdd.DSR_ARRIVATI, mmdf.NOME_FILE, mmdf.OFFERTA_COMMERCIALE, mmdf.TERRITORIO, mmdf.TIPO_UTILIZZO")
                            .append(" from MM_MONITOR_DSP_DASHBOARD mmdd, MM_MONITOR_DSR_FILE mmdf")
                            .append(" where mmdd.ID_DSP = mmdf.ID_DSP")
                            .append(" and mmdf.OFFERTA_COMMERCIALE is null")
                            .append(" and mmdd.ID_DSP = '" + dsp.getIdDsp() +"'")
                            .append(" and mmdd.PERIODO = '" + dspList.getPeriodo() + "'")
                            .append(" and mmdf.PERIODO in (");
                        if (periodToConvert.size() == 1) {
                            queryU.append(periodToConvert.get(0));
                        } else {
                            queryU.append(periodToConvert.get(0) + ",")
                                    .append(periodToConvert.get(1) + ",")
                                    .append(periodToConvert.get(2));
                        }
                        queryU.append(")")
                                .append(" group by mmdf.NOME_FILE");

                    Query qU = entityManager.createNativeQuery(queryU.toString());
                    List<Object[]> resultListUnidentified = qU.getResultList();
                    logger.info("Returned Unidentified:" + resultListUnidentified.toString());

                    resultList.addAll(resultListUnidentified);

                    List<String> keys = Arrays.asList("id", "idDsp", "periodo", "idConfig", "dsrArrivati", "nomeFile", "offertaCommerciale", "territorio", "tipoUtilizzo", "dsrAttesi");
                    List<String> keysUnidentified = Arrays.asList("id", "idDsp", "periodo", "dsrArrivati", "nomeFile");

                    int dsrArrivati, dsrAttesi;
                    boolean isIdentified = true;

                    for (Object[] row : resultList){

                        JsonObject json = new JsonObject();

                        if (row.length == 10) {
                            for (int i = 0; i < row.length; i++) {
                                if (row[i] != null) {
                                    json.addProperty(keys.get(i), String.valueOf(row[i]));
                                }
                            }
                        } else {
                            isIdentified = false;
                            for (int i = 0; i < row.length; i++){
                                if (row[i] != null){
                                    if (i == 3){
                                        json.addProperty(keysUnidentified.get(i), String.valueOf(resultListUnidentified.size()));
                                    } else {
                                        json.addProperty(keysUnidentified.get(i), String.valueOf(row[i]));
                                    }
                                }
                            }
                        }
                        dsrArrivati = isIdentified ? (int) row[4] : (int) row[3];
                        dsrAttesi = isIdentified ? (int) row[9] : 0;


                        if (isIdentified) {

                            if (dsrArrivati >= dsrAttesi) {
                                json.addProperty(SEMAFORO, "OK");
                            } else {
                                json.addProperty(SEMAFORO, "DSR MANCANTI");
                            }
                        }

                        jsonList.add(json);
                    }
            }
            result.put("row", jsonList);
            return Response.ok(gson.toJson(result)).build();
    }

    @GET
    @Path("all")
    public Response getAllConfigs() {
        final EntityManager entityManager = provider.get();
        Query q = entityManager.createNamedQuery("MonitorDsrConfig.all");
        List<MonitorDsrConfig> resultList = q.getResultList();
        Map<String, List<?>> result = new HashMap<>();
        result.put("row", resultList);

        return Response.ok(gson.toJson(result)).build();
    }

    @GET
    @Path("getConfig/{idConfig}")
    public Response getConfiguration(@PathParam("idConfig") Integer idConfig){
        final EntityManager entityManager = provider.get();
        Query q = entityManager.createNativeQuery("select * from MM_MONITOR_DSR_CONFIG mdc" +
                " where mdc.ID_CONFIG = ?");
        q.setParameter(1, idConfig);
        List<String> keys = Arrays.asList("idConfig", "idDsp", "territorio", "tipoUtilizzo", "offertaCommerciale",
                "frequenzaDsr", "slaInvioDsr", "slaInvioCcid", "dsrAttesi");
        List<Object[]> configurations = q.getResultList();
        if (!configurations.isEmpty()){
            JsonObject result = new JsonObject();
            for (Object[] row: configurations) {
                for (int i = 0; i < row.length; i++){
                    if (row[i] != null){
                        result.addProperty(keys.get(i), String.valueOf(row[i]));
                    }
                }
            }
            return Response.ok(gson.toJson(result)).build();
        }
        return Response.status(500).build();
    }

    @POST
    @Path("addConfig")
    public Response addConfiguration(MonitorDsrDTO dto){
        final EntityManager entityManager = provider.get();
        MonitorDsrConfig config = new MonitorDsrConfig();
        config.setIdDsp(dto.getIdDsp());
        config.setTerritorio(dto.getTerritorio());
        config.setTipoUtilizzo(dto.getTipoUtilizzo());
        config.setOffertaCommerciale(dto.getOffertaCommerciale());
        config.setFrequenzaDsr(dto.getFrequenzaDsr());
        if (dto.getSlaInvioDsr() != null)
            config.setSlaInvioDsr(dto.getSlaInvioDsr());
        if (dto.getSlaInvioCcid() != null)
            config.setSlaInvioCcid(dto.getSlaInvioCcid());
        config.setDsrAttesi(dto.getDsrAttesi());

        try {
            entityManager.getTransaction().begin();
            entityManager.persist(config);
            entityManager.getTransaction().commit();
            return Response.ok().build();
        } catch (Exception e) {
            logger.error("addConfig", e);
            if (entityManager.getTransaction().isActive())
                entityManager.getTransaction().rollback();
        }
        return Response.status(500).build();
    }

    @POST
    @Path("editConfig/{idConfig}")
    public Response editConfiguration(@PathParam("idConfig") Integer idConfig, MonitorDsrDTO dto){
        final EntityManager entityManager = provider.get();
        try{
            Query update = entityManager.createNamedQuery("MonitorDspConfig.edit");
            update.setParameter("idDsp", dto.getIdDsp());
            update.setParameter("territorio", dto.getTerritorio());
            update.setParameter("tipoUtilizzo", dto.getTipoUtilizzo());
            update.setParameter("offertaCommerciale", dto.getOffertaCommerciale());
            update.setParameter("frequenzaDsr", dto.getFrequenzaDsr());
            if (dto.getSlaInvioDsr() != null) {
                update.setParameter("slaInvioDsr", dto.getSlaInvioDsr());
            } else {
                update.setParameter("slaInvioDsr", null);
            }
            if (dto.getSlaInvioCcid() != null) {
                update.setParameter("slaInvioCcid", dto.getSlaInvioCcid());
            } else {
                update.setParameter("slaInvioCcid", null);
            }
            update.setParameter("dsrAttesi", dto.getDsrAttesi());
            update.setParameter("idConfig", idConfig);
            entityManager.getTransaction().begin();
            update.executeUpdate();
            entityManager.getTransaction().commit();
            return Response.ok().build();

        } catch (Exception e){
            logger.error("editConfig", e);
            if (entityManager.getTransaction().isActive())
                entityManager.getTransaction().rollback();
        }
        return Response.status(500).build();
    }

    @DELETE
    @Path("removeConfig/{idConfig}")
    public Response deleteConfiguration(@PathParam("idConfig") Integer idConfig){
        final EntityManager entityManager = provider.get();
        try{
            MonitorDsrConfig config = entityManager.find(MonitorDsrConfig.class, idConfig);

            Query deleteToConfig = entityManager.createQuery("delete from MonitorDsrFileToConfig where idConfig = :idConfig");
            deleteToConfig.setParameter("idConfig", idConfig);

            Query deleteDashboard = entityManager.createQuery("delete from MonitorDspDashboard where (idConfig = :idConfig or (idConfig is null and idDsp = :idDsp))");
            deleteDashboard.setParameter("idConfig", idConfig);
            deleteDashboard.setParameter("idDsp", config.getIdDsp());

            entityManager.getTransaction().begin();
            deleteDashboard.executeUpdate();
            deleteToConfig.executeUpdate();
            entityManager.remove(config);
            entityManager.getTransaction().commit();

            return Response.ok().build();
        } catch (Exception e){
            logger.error("deleteConfig", e);
            entityManager.getTransaction().rollback();
        }
        return Response.status(500).build();
    }

    @PUT
    @Path("scan")
    public Response scan(MonitorDspListDTO dspList) {
        final EntityManager entityManager = provider.get();

        try {

            for (MonitorDsrDTO dsp : dspList.getList()) {

                List<String> periods = matchCorrectPeriod(dspList.getPeriodo(), dsp.getFrequenzaDsr());
                Integer totalFileArrived = 0;

                for (String periodoDaCercare : periods) {

                    Query selectDspPath = entityManager.createNativeQuery("select FTP_SOURCE_PATH from ANAG_DSP" +
                            " where IDDSP = ?");
                    selectDspPath.setParameter(1, dsp.getIdDsp());
                    List<String> pathList = selectDspPath.getResultList();
                    if (pathList.isEmpty())
                        continue;

                    String path = pathList.get(0);
                    String s3Path = configuration.getProperty("dspDashboard_s3_path");
                    s3Path = s3Path.replace("{ftp-source-path}", path);
                    s3Path = s3Path.replace("{periodo}", periodoDaCercare);


                    Query deleteBeforeCalculate = entityManager.createQuery("select x from MonitorDsrFile x where x.idDsp = :idDsp and x.periodo = :periodo");
                    deleteBeforeCalculate.setParameter("idDsp", dsp.getIdDsp());
                    deleteBeforeCalculate.setParameter("periodo", periodoDaCercare);
                    List<MonitorDsrFile> filesToDdelete = deleteBeforeCalculate.getResultList();
                    if (!filesToDdelete.isEmpty()) {
                        for (MonitorDsrFile file : filesToDdelete) {
                            Query deleteToConfig = entityManager.createQuery("delete from MonitorDsrFileToConfig x where x.idFile = :idFile");
                            deleteToConfig.setParameter("idFile", file.getID());

                            entityManager.getTransaction().begin();
                            deleteToConfig.executeUpdate();
                            entityManager.remove(file);
                            entityManager.getTransaction().commit();
                        }
                    }


                    AmazonS3URI s3uri = new AmazonS3URI(s3Path);
                    List<S3ObjectSummary> listObjectSummaries = s3Service.listObjects(s3uri.getBucket(), s3uri.getKey());

                    if (!listObjectSummaries.isEmpty()) {

                        List<String> filteredS3Name = new ArrayList<>();
                        for (S3ObjectSummary s3obj : listObjectSummaries) {
                            if (s3obj.getKey().endsWith(EXT) && !s3obj.getKey().contains(BACKCLAIM)) {
                                int from = s3obj.getKey().lastIndexOf(SLASHDELIMITER);
                                int to = s3obj.getKey().indexOf(DOTDELIMITER);
                                filteredS3Name.add(s3obj.getKey().substring(from + 1, to));
                            }
                        }

                        Query selectTris = entityManager.createQuery("select x from DsrMetadataConfig x group by x.regex");
                        List<DsrMetadataConfig> metadataConfigs = selectTris.getResultList();

                        for (String s3Name : filteredS3Name) {
                            DsrMetadataConfig matchedDsr = null;
                            for (DsrMetadataConfig metadata : metadataConfigs) {
                                if (s3Name.matches(metadata.getRegex())) {
                                    matchedDsr = metadata;
                                }
                            }
                            if (matchedDsr != null) {

                                String territorio = s3Name.substring(matchedDsr.getStartIndexCountry(), matchedDsr.getEndIndexCountry() + 1);
                                MonitorDsrFile dsrFile = new MonitorDsrFile();
                                dsrFile.setIdDsp(dsp.getIdDsp());
                                dsrFile.setNomeFile(s3Name);
                                CommercialOffers commercialOffers = entityManager.find(CommercialOffers.class, matchedDsr.getIdCommercialOffer());
                                dsrFile.setOffertaCommerciale(commercialOffers.getOffering());
                                dsrFile.setTipoUtilizzo(commercialOffers.getIdUtilizationType());
                                dsrFile.setTerritorio(territorio);
                                dsrFile.setPeriodo(periodoDaCercare);

                                entityManager.getTransaction().begin();
                                entityManager.persist(dsrFile);
                                entityManager.getTransaction().commit();

                                Query queryTerritory = entityManager.createNativeQuery("select * from ANAG_COUNTRY ac where ac.CODE = ?")
                                        .setParameter(1, territorio);
                                List<Object[]> countryList = queryTerritory.getResultList();
                                Object[] country = countryList.get(0);
                                String countryString = (String) country[0];


                                Query selectDsr = entityManager.createQuery("select x" +
                                        " from MonitorDsrConfig x" +
                                        " where" +
                                        " (x.idDsp = :idDsp or x.idDsp = '*')" +
                                        " and (x.offertaCommerciale = :offertaCommerciale or x.offertaCommerciale = '*')" +
                                        " and (x.tipoUtilizzo = :tipoUtilizzo or x.tipoUtilizzo = '*')" +
                                        " and (x.territorio = :territorio or x.territorio = '*')");
                                selectDsr.setParameter("idDsp", dsp.getIdDsp());
                                selectDsr.setParameter("offertaCommerciale", String.valueOf(matchedDsr.getIdCommercialOffer()));
                                selectDsr.setParameter("tipoUtilizzo", dsrFile.getTipoUtilizzo());
                                selectDsr.setParameter("territorio", countryString);

                                List<MonitorDsrConfig> result = selectDsr.getResultList();


                                for (MonitorDsrConfig config : result){
                                    MonitorDsrFileToConfig toConfig = new MonitorDsrFileToConfig();
                                    toConfig.setIdFile(dsrFile.getID());
                                    toConfig.setIdConfig(config.getIdConfig());

                                    entityManager.getTransaction().begin();
                                    entityManager.persist(toConfig);
                                    entityManager.getTransaction().commit();
                                }

                            } else {
                                MonitorDsrFile unidentifiedFile = new MonitorDsrFile();
                                unidentifiedFile.setIdDsp(dsp.getIdDsp());
                                unidentifiedFile.setNomeFile(s3Name);
                                unidentifiedFile.setPeriodo(periodoDaCercare);

                                entityManager.getTransaction().begin();
                                entityManager.persist(unidentifiedFile);
                                entityManager.getTransaction().commit();
                            }
                        }

                        Query selectToInsert = entityManager.createNamedQuery("MonitorDsrConfig.searchForDsp");
                        selectToInsert.setParameter("idDsp", dsp.getIdDsp());
                        List<MonitorDsrConfig> resultDsrConfig = selectToInsert.getResultList();

                        for (MonitorDsrConfig configuration : resultDsrConfig) {

                            StringBuffer query = new StringBuffer();
                            query.append("SELECT mmdftc.*")
                                    .append(" FROM MM_MONITOR_DSR_FILE_TO_CONFIG mmdftc, MM_MONITOR_DSR_FILE mmdf")
                                    .append(" where mmdftc.ID_CONFIG = " + configuration.getIdConfig())
                                    .append(" and mmdftc.ID_FILE = mmdf.ID")
                                    .append(" and mmdf.PERIODO in ('")
                                    .append(periodoDaCercare)
                                    .append("')");
                            Query totalFileArrivedQ = entityManager.createNativeQuery(query.toString());

                            List<MonitorDsrFileToConfig> filesArrived = totalFileArrivedQ.getResultList();
                            totalFileArrived = totalFileArrived + filesArrived.size();

                            Query selectDashExist = entityManager.createNamedQuery("MonitorDspDashboard.doesRecordExist");
                            selectDashExist.setParameter("idDsp", configuration.getIdDsp());
                            selectDashExist.setParameter("idConfig", configuration.getIdConfig());
                            selectDashExist.setParameter("periodo", dspList.getPeriodo());
                            List<MonitorDspDashboard> resultList = selectDashExist.getResultList();

                            MonitorDspDashboard dspDash = null;
                            if (!resultList.isEmpty()) {
                                dspDash = resultList.get(0);
                            }

                            if (dspDash == null) {
                                MonitorDspDashboard dspDashboard = new MonitorDspDashboard();
                                dspDashboard.setIdConfig(configuration.getIdConfig());
                                dspDashboard.setIdDsp(configuration.getIdDsp());
                                dspDashboard.setPeriodo(dspList.getPeriodo());
                                dspDashboard.setDsrArrivati(totalFileArrived);

                                entityManager.getTransaction().begin();
                                entityManager.persist(dspDashboard);
                                entityManager.getTransaction().commit();
                            } else {
                                dspDash.setIdConfig(configuration.getIdConfig());
                                dspDash.setIdDsp(configuration.getIdDsp());
                                dspDash.setPeriodo(dspList.getPeriodo());
                                dspDash.setDsrArrivati(totalFileArrived);

                                entityManager.getTransaction().begin();
                                entityManager.merge(dspDash);
                                entityManager.getTransaction().commit();
                            }
                        }

                        Query clearDashUnidentified = entityManager.createNativeQuery("delete from MM_MONITOR_DSP_DASHBOARD " +
                                " where ID_DSP = ? and ID_CONFIG IS NULL");
                        clearDashUnidentified.setParameter(1, dsp.getIdDsp());

                        entityManager.getTransaction().begin();
                        clearDashUnidentified.executeUpdate();
                        entityManager.getTransaction().commit();

                        Query selectUnidentified = entityManager.createNamedQuery("MonitorDsrFile.unidentified");
                        selectUnidentified.setParameter("idDsp", dsp.getIdDsp());
                        List<MonitorDsrFile> unidentifiedList = selectUnidentified.getResultList();


                        if (!unidentifiedList.isEmpty()){
                            MonitorDspDashboard dspDashboardUnidentified = new MonitorDspDashboard();
                            dspDashboardUnidentified.setIdDsp(dsp.getIdDsp());
                            dspDashboardUnidentified.setPeriodo(dspList.getPeriodo());
                            dspDashboardUnidentified.setDsrArrivati(unidentifiedList.size());

                            entityManager.getTransaction().begin();
                            entityManager.merge(dspDashboardUnidentified);
                            entityManager.getTransaction().commit();
                        }
                    }
                }
            }
            return Response.ok().build();
        } catch (Exception e) {
            logger.error("scan", e);

            if (entityManager.getTransaction().isActive())
                entityManager.getTransaction().rollback();

        }
        return Response.status(500).build();
    }


    //Utils
    public List<String> matchCorrectPeriod(String periodoToConvert, String frequency) {
        List<String> periods = new ArrayList<>();
        boolean isQuarter = periodoToConvert.contains(QUARTER);

        if (isQuarter) {

            int idxStart = periodoToConvert.indexOf(QUARTER);
            String year = periodoToConvert.substring(0, idxStart);
            String quarter = periodoToConvert.substring(idxStart);

            if (frequency.equals(MENSILE)) {

                if (quarter.equals("Q1"))
                    periods.add(year + "01");
                if (quarter.equals("Q2"))
                    periods.add(year + "04");
                if (quarter.equals("Q3"))
                    periods.add(year + "07");
                if (quarter.equals("Q4"))
                    periods.add(year + "10");

            } else {

                if (quarter.equals("Q1")) {
                    periods.add(year + "01");
                    periods.add(year + "02");
                    periods.add(year + "03");
                }
                if (quarter.equals("Q2")) {
                    periods.add(year + "04");
                    periods.add(year + "05");
                    periods.add(year + "06");
                }
                if (quarter.equals("Q3")) {
                    periods.add(year + "07");
                    periods.add(year + "08");
                    periods.add(year + "09");
                }
                if (quarter.equals("Q4")) {
                    periods.add(year + "10");
                    periods.add(year + "11");
                    periods.add(year + "12");
                }
            }
        }  else {
            periods.add(periodoToConvert);
        }
        return periods;
    }
}
