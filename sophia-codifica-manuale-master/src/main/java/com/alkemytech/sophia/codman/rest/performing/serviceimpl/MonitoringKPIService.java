package com.alkemytech.sophia.codman.rest.performing.serviceimpl;

import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alkemytech.sophia.codman.dto.performing.ConfigurazioniPeriodoDTO;
import com.alkemytech.sophia.codman.dto.performing.MonitoringKPIDTO;
import com.alkemytech.sophia.codman.entity.performing.InformazioneKPICodifica;
import com.alkemytech.sophia.codman.entity.performing.dao.ICodificaDAO;
import com.alkemytech.sophia.codman.entity.performing.dao.IMonitoringKPIDAO;
import com.alkemytech.sophia.codman.rest.performing.service.ICodificaService;
import com.alkemytech.sophia.codman.rest.performing.service.ImonitoringKPIService;
import com.google.inject.Inject;

@Service("MonitoringKPIService")
public class MonitoringKPIService implements ImonitoringKPIService {

	
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private IMonitoringKPIDAO monitoringKPIDAO;

	@Inject
	public MonitoringKPIService(IMonitoringKPIDAO monitoringKPIDAO) {
		this.monitoringKPIDAO = monitoringKPIDAO;
	}
	
	
	@Override
	public List<InformazioneKPICodifica> getRecordMonitoring(Long periodoRipartizione, String voceIncasso, String tipologiaReport) {
		return monitoringKPIDAO.getRecordKPI(periodoRipartizione,voceIncasso,tipologiaReport);
	}
		
}
