package com.alkemytech.sophia.codman.rest;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.codman.entity.AnagDsp;
import com.alkemytech.sophia.codman.entity.SqsServiceBus;
import com.alkemytech.sophia.codman.entity.SqsServiceBusStats;
import com.alkemytech.sophia.codman.entity.SqsServiceBusStatsDate;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

@Singleton
@Path("service-bus")
public class ServiceBusService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Provider<EntityManager> provider;
	private final Gson gson;

	@Inject
	protected ServiceBusService(@McmdbDataSource Provider<EntityManager> provider, Gson gson) {
		super();
		this.provider = provider;
		this.gson = gson;
	}

	@GET
	@Produces("application/json")
	public Response findAll() {
		final EntityManager entityManager = provider.get();
		final List<SqsServiceBus> result = entityManager
				.createQuery("select x from SqsServiceBus x", SqsServiceBus.class)
				.setMaxResults(32768)
				.getResultList();
		return Response.ok(gson.toJson(result)).build();
	}

	@GET
	@Path("{id}")
	@Produces("application/json")
	public Response findByQueue(@PathParam("id") Long id) {
		final EntityManager entityManager = provider.get();
		final SqsServiceBus result = entityManager
				.find(SqsServiceBus.class, id);
		if (null == result) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.ok(gson.toJson(result)).build();
	}

	@GET
	@Path("stats")
	@Produces("application/json")
	public Response getStats() {
		final EntityManager entityManager = provider.get();
		final List<SqsServiceBusStats> result = entityManager
				.createNamedQuery("SqsServiceBusStats.FindAll", SqsServiceBusStats.class)
				.getResultList();
		return Response.ok(gson.toJson(result)).build();
	}

	@GET
	@Path("stats-date")
	@Produces("application/json")
	public Response getStatsDate() {
		final EntityManager entityManager = provider.get();
		final List<SqsServiceBusStatsDate> stats = entityManager
				.createNamedQuery("SqsServiceBusStatsDate.FindAll", SqsServiceBusStatsDate.class)
				.getResultList();
		final List<Map<String, Object>> result = new ArrayList<Map<String,Object>>();
		Date date = null;
		List<SqsServiceBusStatsDate> sqsServiceBusStats = null;
		for (SqsServiceBusStatsDate sqsServiceBusStat : stats) {
			if (!sqsServiceBusStat.getInsertTime().equals(date)) {
				final Map<String, Object> resultItem = new HashMap<String, Object>();
				resultItem.put("insertTime", date = sqsServiceBusStat.getInsertTime());
				resultItem.put("serviceBusStats", sqsServiceBusStats = new ArrayList<SqsServiceBusStatsDate>());
				result.add(resultItem);
			}
			sqsServiceBusStats.add(sqsServiceBusStat);
		}
		return Response.ok(gson.toJson(result)).build();
	}

	@GET
	@Path("queue-names")
	@Produces("application/json")
	public Response getQueueNames() {
		final EntityManager entityManager = provider.get();
		final List<String> result = entityManager
				.createQuery("select distinct x.queueName from SqsServiceBus x order by x.queueName", String.class)
				.getResultList();
		try {
			result.remove(null);
		} catch (NullPointerException e) { }
		return Response.ok(gson.toJson(result)).build();
	}

	@GET
	@Path("queue-types")
	@Produces("application/json")
	public Response getQueueTypes() {
		final EntityManager entityManager = provider.get();
		final List<String> result = entityManager
				.createQuery("select distinct x.queueType from SqsServiceBus x order by x.queueType", String.class)
				.getResultList();
		try {
			result.remove(null);
		} catch (NullPointerException e) { }
		return Response.ok(gson.toJson(result)).build();
	}

	@GET
	@Path("service-names")
	@Produces("application/json")
	public Response getServiceNames() {
		final EntityManager entityManager = provider.get();
		final List<String> result = entityManager
				.createQuery("select distinct x.serviceName from SqsServiceBus x order by x.serviceName", String.class)
				.getResultList();
		try {
			result.remove(null);
		} catch (NullPointerException e) { }
		return Response.ok(gson.toJson(result)).build();
	}

	@GET
	@Path("senders")
	@Produces("application/json")
	public Response getSenders() {
		final EntityManager entityManager = provider.get();
		final List<String> result = entityManager
				.createQuery("select distinct x.sender from SqsServiceBus x order by x.sender", String.class)
				.getResultList();
		try {
			result.remove(null);
		} catch (NullPointerException e) { }
		return Response.ok(gson.toJson(result)).build();
	}

	@GET
	@Path("iddsrs")
	@Produces("application/json")
	public Response getIdDsrs() {
		final EntityManager entityManager = provider.get();
		final List<String> result = entityManager
				.createQuery("select distinct x.idDsr from SqsServiceBus x order by x.idDsr", String.class)
				.getResultList();
		try {
			result.remove(null);
		} catch (NullPointerException e) { }
		return Response.ok(gson.toJson(result)).build();
	}

	@GET
	@Path("searchIdDsrs")
	@Produces("application/json")
	public Response searchIdDsrs(@QueryParam("idDsr") String idDsr) {
		final EntityManager entityManager = provider.get();
		final List<String> idDsrs = entityManager
//				.createQuery("select x.idDsr from DsrMetadata x where x.idDsr like :idDsr order by x.idDsr", String.class)
				.createQuery("select distinct x.idDsr from SqsServiceBus x where x.idDsr like :idDsr order by x.idDsr", String.class)
				.setParameter("idDsr", "%" + idDsr + "%")
				.getResultList();
		try {
			idDsrs.remove(null);
		} catch (NullPointerException e) { }
		final List<Map<String, String>> result = new ArrayList<Map<String,String>>(idDsrs.size());
		for (String name : idDsrs) {
			final Map<String, String> idAsMap = new HashMap<String, String>(1);
			idAsMap.put("name", name);
			result.add(idAsMap);
		}
		return Response.ok(gson.toJson(result)).build();
	}

	@GET
	@Path("searchHostnames")
	@Produces("application/json")
	public Response searchHostnames(@QueryParam("hostname") String hostname) {
		final EntityManager entityManager = provider.get();
		final List<String> hostnames = entityManager
				.createQuery("select distinct x.hostname from SqsServiceBus x where x.hostname like :hostname order by x.hostname", String.class)
				.setParameter("hostname", "%" + hostname + "%")
				.getResultList();
		try {
			hostnames.remove(null);
		} catch (NullPointerException e) { }
		final List<Map<String, String>> result = new ArrayList<Map<String,String>>(hostnames.size());
		for (String name : hostnames) {
			final Map<String, String> hostnameAsMap = new HashMap<String, String>(1);
			hostnameAsMap.put("name", name);
			result.add(hostnameAsMap);
		}
		return Response.ok(gson.toJson(result)).build();
	}
	
	@GET
	@Path("iddsps")
	@Produces("application/json")
	public Response getIdDsps() {
		final EntityManager entityManager = provider.get();
		final List<AnagDsp> anagDsps = entityManager
				.createQuery("select x from AnagDsp x order by x.name", AnagDsp.class)
				.getResultList();
		Map<String, String> result = new LinkedHashMap<String, String>();
		if (null != anagDsps) for (AnagDsp anagDsp : anagDsps) {
			result.put(anagDsp.getIdDsp(), anagDsp.getName());
		}
		try {
			result.remove(null);
		} catch (NullPointerException e) { }
		return Response.ok(gson.toJson(result)).build();
	}

	@GET
	@Path("search")
	@Produces("application/json")
	public Response search(@QueryParam("queueName") String queueName,
			@QueryParam("insertTime") String insertTime,
			@QueryParam("queueType") String queueType,
			@QueryParam("serviceName") String serviceName, 
			@QueryParam("sender") String sender, 
			@QueryParam("hostname") String hostname, 
			@QueryParam("idDsp") String iddsp,
			@QueryParam("idDsr") String iddsr,
			@QueryParam("year") String year,
			@QueryParam("month") String month,
			@QueryParam("country") String country,
			@QueryParam("sortType") String sortType,
			@QueryParam("sortReverse") String sortReverse,
			@DefaultValue("0") @QueryParam("first") int first,
			@DefaultValue("50") @QueryParam("last") int last) {
		final EntityManager entityManager = provider.get();
		try {
			String separator = " where ";
			final StringBuffer sql = new StringBuffer()
				.append("select * from SQS_SERVICE_BUS");
			if (!StringUtils.isEmpty(queueName)) {
				sql.append(separator).append("QUEUE_NAME = ?");
				separator = " and ";
			}
			if (!StringUtils.isEmpty(insertTime)) {
				sql.append(separator).append("INSERT_TIME >= ? and INSERT_TIME < ?");
				separator = " and ";
			}
			if (!StringUtils.isEmpty(queueType)) {
				sql.append(separator).append("QUEUE_TYPE = ?");
				separator = " and ";
			}
			if (!StringUtils.isEmpty(serviceName)) {
				sql.append(separator).append("SERVICE_NAME = ?");
				separator = " and ";
			}
			if (!StringUtils.isEmpty(sender)) {
				sql.append(separator).append("SENDER = ?");
				separator = " and ";
			}
			if (!StringUtils.isEmpty(hostname)) {
				sql.append(separator).append("HOSTNAME = ?");
				separator = " and ";
			}
			if (!StringUtils.isEmpty(iddsp)) {
				sql.append(separator).append("IDDSP = ?");
				separator = " and ";
			}
			if (!StringUtils.isEmpty(iddsr)) {
				sql.append(separator).append("IDDSR = ?");
				separator = " and ";
			}
			if (!StringUtils.isEmpty(year)) {
				sql.append(separator).append("YEAR = ?");
				separator = " and ";
			}
			if (!StringUtils.isEmpty(month)) {
				sql.append(separator).append("MONTH = ?");
				separator = " and ";
			}
			if (!StringUtils.isEmpty(country)) {
				sql.append(separator).append("COUNTRY = ?");
				separator = " and ";
			}
			final String ascOrDesc = "true".equalsIgnoreCase(sortReverse) ? "DESC" : "ASC";
			if ("insertTime".equalsIgnoreCase(sortType)) {
				sql.append(" order by INSERT_TIME ")
					.append(ascOrDesc);
			} else if ("queueName".equalsIgnoreCase(sortType)) {
				sql.append(" order by QUEUE_NAME ")
					.append(ascOrDesc);
			} else if ("queueType".equalsIgnoreCase(sortType)) {
				sql.append(" order by QUEUE_TYPE ")
					.append(ascOrDesc);
			} else if ("serviceName".equalsIgnoreCase(sortType)) {
				sql.append(" order by SERVICE_NAME ")
					.append(ascOrDesc);
			} else if ("sender".equalsIgnoreCase(sortType)) {
				sql.append(" order by SENDER ")
					.append(ascOrDesc);
			} else if ("hostname".equalsIgnoreCase(sortType)) {
				sql.append(" order by HOSTNAME ")
					.append(ascOrDesc);
			} else if ("idDsp".equalsIgnoreCase(sortType)) {
				sql.append(" order by IDDSP ")
					.append(ascOrDesc);
			} else if ("idDsr".equalsIgnoreCase(sortType)) {
				sql.append(" order by IDDSR ")
					.append(ascOrDesc);
			} else if ("year".equalsIgnoreCase(sortType)) {
				sql.append(" order by YEAR ")
					.append(ascOrDesc);
			} else if ("MONTH".equalsIgnoreCase(sortType)) {
				sql.append(" order by MONTH ")
					.append(ascOrDesc);
			} else if ("country".equalsIgnoreCase(sortType)) {
				sql.append(" order by COUNTRY ")
					.append(ascOrDesc);
			}
			sql.append(" limit ?, ?");
			int i = 1;
			final Query query = (Query) entityManager
				.createNativeQuery(sql.toString(), SqsServiceBus.class);
			if (!StringUtils.isEmpty(queueName)) {
				query.setParameter(i ++, queueName);
			}
			if (!StringUtils.isEmpty(insertTime)) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(insertTime));
				calendar.set(Calendar.HOUR, 0);
				calendar.set(Calendar.MINUTE, 0);
				calendar.set(Calendar.SECOND, 0);
				calendar.set(Calendar.MILLISECOND, 0);
				query.setParameter(i ++, calendar.getTime());
				calendar.add(Calendar.HOUR, 24);
				query.setParameter(i ++, calendar.getTime());
			}
			if (!StringUtils.isEmpty(queueType)) {
				query.setParameter(i ++, queueType);
			}
			if (!StringUtils.isEmpty(serviceName)) {
				query.setParameter(i ++, serviceName);
			}
			if (!StringUtils.isEmpty(sender)) {
				query.setParameter(i ++, sender);
			}
			if (!StringUtils.isEmpty(hostname)) {
				query.setParameter(i ++, hostname);
			}
			if (!StringUtils.isEmpty(iddsp)) {
				query.setParameter(i ++, iddsp);
			}
			if (!StringUtils.isEmpty(iddsr)) {
				query.setParameter(i ++, iddsr);
			}
			if (!StringUtils.isEmpty(year)) {
				query.setParameter(i ++, year);
			}
			if (!StringUtils.isEmpty(month)) {
				query.setParameter(i ++, month);
			}
			if (!StringUtils.isEmpty(country)) {
				query.setParameter(i ++, country);
			}
			query.setParameter(i ++, first);
			query.setParameter(i ++, 1 + last - first);
			@SuppressWarnings("unchecked")
			final List<SqsServiceBus> results =
				(List<SqsServiceBus>) query.getResultList();
			final PagedResult result = new PagedResult();
			if (null != results && !results.isEmpty()) {
				final int maxrows = last - first;
				final boolean hasNext = results.size() > maxrows;
				result.setRows(!hasNext ? results : results.subList(0, maxrows))
					.setMaxrows(maxrows)
					.setFirst(first)
					.setLast(hasNext ? last : first + results.size())
					.setHasNext(hasNext)
					.setHasPrev(first > 0);
			} else {
				result.setMaxrows(0)
					.setFirst(0)
					.setLast(0)
					.setHasNext(false)
					.setHasPrev(false);
			}
			return Response.ok(gson.toJson(result)).build();
		} catch (Exception e) {
			logger.error("search", e);
		}
		return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR).build();
	}

}
