package com.alkemytech.sophia.codman.service.dsrmetadata.country;

public interface DsrCountryExtractor {

	public String extractDsrCountryCode(String country);
	
	public String getCode();
	
	public String getDescription();
	
	public String getRegex();
}
