package com.alkemytech.sophia.codman.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UpdateRoyaltiesRequestDTO {

    private String month;
    private String year;

    public String getMonth() {
        return month;
    }

    public UpdateRoyaltiesRequestDTO setMonth(String month) {
        this.month = month;
        return this;
    }

    public String getYear() {
        return year;
    }

    public UpdateRoyaltiesRequestDTO setYear(String year) {
        this.year = year;
        return this;
    }
}
