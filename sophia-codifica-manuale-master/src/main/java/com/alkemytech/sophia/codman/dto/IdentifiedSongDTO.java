package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;
import java.util.List;

import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.GsonBuilder;

@Produces("application/json")
@XmlRootElement 
@SuppressWarnings("serial")
public class IdentifiedSongDTO implements Serializable {
	
	private String hashId;
	private String title;
	private String artists;
	private String siadaTitle;
	private String siadaArtists;
	private String roles;
	private String siaeWorkCode;
	private String identificationType;
	private List<IdentifiedSongDsrDTO> dsrs;
	
	public String getHashId() {
		return hashId;
	}

	public void setHashId(String hashId) {
		this.hashId = hashId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getArtists() {
		return artists;
	}

	public void setArtists(String artists) {
		this.artists = artists;
	}

	public String getSiadaTitle() {
		return siadaTitle;
	}

	public void setSiadaTitle(String siadaTitle) {
		this.siadaTitle = siadaTitle;
	}

	public String getSiadaArtists() {
		return siadaArtists;
	}

	public void setSiadaArtists(String siadaArtists) {
		this.siadaArtists = siadaArtists;
	}

	public String getRoles() {
		return roles;
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}

	public String getSiaeWorkCode() {
		return siaeWorkCode;
	}

	public void setSiaeWorkCode(String siaeWorkCode) {
		this.siaeWorkCode = siaeWorkCode;
	}

	public String getIdentificationType() {
		return identificationType;
	}

	public void setIdentificationType(String identificationType) {
		this.identificationType = identificationType;
	}

	public List<IdentifiedSongDsrDTO> getDsrs() {
		return dsrs;
	}

	public void setDsrs(List<IdentifiedSongDsrDTO> dsrs) {
		this.dsrs = dsrs;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}
	
}
