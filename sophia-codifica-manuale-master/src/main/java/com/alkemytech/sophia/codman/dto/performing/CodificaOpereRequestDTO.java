package com.alkemytech.sophia.codman.dto.performing;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class CodificaOpereRequestDTO{

    private List<CodificaOpereDTO> codificaOpereDTO;
    private String username;

    public CodificaOpereRequestDTO() {
        super();
    }

    public CodificaOpereRequestDTO(List<CodificaOpereDTO> codificaOpereDTO, String username) {
        this.codificaOpereDTO = codificaOpereDTO;
        this.username = username;
    }

    public List<CodificaOpereDTO> getCodificaOpereDTO() {
        return codificaOpereDTO;
    }

    public void setCodificaOpereDTO(List<CodificaOpereDTO> codificaOpereDTO) {
        this.codificaOpereDTO = codificaOpereDTO;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "CodificaOpereRequestDTO{" +
                "codificaOpereDTO=" + codificaOpereDTO +
                ", username='" + username + '\'' +
                '}';
    }
}
