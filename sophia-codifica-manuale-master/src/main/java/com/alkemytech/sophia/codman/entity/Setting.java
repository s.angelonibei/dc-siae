package com.alkemytech.sophia.codman.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@Entity
@Table(name = "PERF_SETTING")
@DiscriminatorColumn(name = "setting_type")
@XmlJavaTypeAdapter(SettingsAdapter.class)
public class Setting implements Serializable, Comparable<Setting>, Iterable<Setting> {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SETTING_ID")
	protected Long id;

	@Column(name = "SETTING_KEY")
	protected String key;

	@Column(name = "ORDINAL")
	protected Integer ordinal;

	@Column(name = "SETTING_TYPE")
	protected String settingType;
	
	@Transient
	private List<String> errorMessages = new ArrayList<String>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Integer getOrdinal() {
		return ordinal;
	}

	public void setOrdinal(Integer ordinal) {
		this.ordinal = ordinal;
	}

	public String getSettingType() {
		return settingType;
	}

	public void setSettingType(String settingType) {
		this.settingType = settingType;
	}

	@Override
	public int compareTo(Setting o) {
		return this.getOrdinal().compareTo(o.getOrdinal());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Setting other = (Setting) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public Iterator<Setting> iterator() {
		return new Iterator<Setting>() {
			
			boolean visited = false;

			@Override
			public boolean hasNext() {
				return !visited;
			}

			@Override
			public Setting next() {
				visited = true;
				return Setting.this;
			}
			
		    public void remove() {
		        throw new UnsupportedOperationException("remove");
		    }			
			
		};
	}

	public List<String> getErrorMessages() {
		return errorMessages;
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

}
