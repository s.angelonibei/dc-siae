package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;
import java.util.Date;

import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;

@Produces("application/json")
@XmlRootElement
public class AnagClientModifiedDTO implements Serializable  {


	private String prevCode;
	private String prevCompanyName;
	private String prevCountry;
	private String prevVatDescription;
	private String prevVatCode;
	private String prevLicences;
	private String code;
	private String idDsp;
	private String companyName;
	private String country;
	private String vatCode;
	private String vatDescription;
	private String licences;
	
	public String getPrevCode() {
		return prevCode;
	}
	public void setPrevCode(String prevCode) {
		this.prevCode = prevCode;
	}
	public String getPrevCompanyName() {
		return prevCompanyName;
	}
	public void setPrevCompanyName(String prevCompanyName) {
		this.prevCompanyName = prevCompanyName;
	}
	public String getPrevCountry() {
		return prevCountry;
	}
	public void setPrevCountry(String prevCountry) {
		this.prevCountry = prevCountry;
	}
	public String getPrevVatCode() {
		return prevVatCode;
	}
	public void setPrevVatCode(String prevVatCode) {
		this.prevVatCode = prevVatCode;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getIdDsp() {
		return idDsp;
	}
	public void setIdDsp(String idDsp) {
		this.idDsp = idDsp;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getVatCode() {
		return vatCode;
	}
	public void setVatCode(String vatCode) {
		this.vatCode = vatCode;
	}
	public String getPrevVatDescription() {
		return prevVatDescription;
	}
	public void setPrevVatDescription(String prevVatDescription) {
		this.prevVatDescription = prevVatDescription;
	}
	public String getVatDescription() {
		return vatDescription;
	}
	public void setVatDescription(String vatDescription) {
		this.vatDescription = vatDescription;
	}
	public String getPrevLicences() {
		return prevLicences;
	}
	public void setPrevLicences(String prevLicences) {
		this.prevLicences = prevLicences;
	}
	public String getLicences() {
		return licences;
	}
	public void setLicences(String licences) {
		this.licences = licences;
	}
}
