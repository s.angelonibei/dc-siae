package com.alkemytech.sophia.codman.rest;

import com.alkemytech.sophia.codman.entity.*;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.*;

@Singleton
@Path("metadata")
public class MetadataService {

	private static final String STATUS = "status";
	private static final String STATUS_OK = "OK";
	private static final String STATUS_KO = "KO";
	private static final String ERROR_MESSAGE = "errorMessage";

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Provider<EntityManager> provider;
	private final Gson gson;

	@Inject
	protected MetadataService(@Named("configuration") Properties configuration,
			@McmdbDataSource Provider<EntityManager> provider, Gson gson) {
		super();
		this.provider = provider;
		this.gson = gson;
	}

	private void putValue(Map<String, Object> map, String key, String value) {
		if (null != value) {
			map.put(key, value);
		}
	}
	private void putValue(Map<String, Object> map, String key, Boolean value) {
		if (null != value) {
			map.put(key, value);
		}
	}
	private void putValue(Map<String, Object> map, String key, Integer value) {
		if (null != value) {
			map.put(key, value);
		}
	}
	private void putValue(Map<String, Object> map, String key, Long value) {
		if (null != value) {
			map.put(key, value);
		}
	}
	private void putValue(Map<String, Object> map, String key, BigInteger value) {
		if (null != value) {
			map.put(key, value);
		}
	}
	private void putValue(Map<String, Object> map, String key, BigDecimal value) {
		if (null != value) {
			if (BigDecimal.ZERO.equals(value)) {
				map.put(key, new BigDecimal("0.0"));
			} else {
				String text = value.setScale(20, RoundingMode.HALF_EVEN).toString();
				if ("0E-20".equalsIgnoreCase(text)) {
					map.put(key, new BigDecimal("0.0"));
				} else {
					while (text.endsWith("0")) {
						text = text.substring(0, text.length() - 1);
					}
					if (text.endsWith(".")) {
						text = text + "0";
					}
					map.put(key, new BigDecimal(text));
				}
			}
		}
	}

	private boolean isBisestile(int year) {
	    return (year % 4 == 0) && (!(year % 100 == 0) || (year % 400 == 0));
	}

	@GET
	@Path("{idDsr}")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response getByIdDsr(@PathParam("idDsr") String idDsr) {
		logger.debug("getByIdDsr: idDsr {}", idDsr);
		final Map<String, Object> result = new HashMap<String, Object>();
		try {
			final EntityManager entityManager = provider.get();

			// DSR_METADATA
			final DsrMetadata dsrMetadata = entityManager
					.find(DsrMetadata.class, idDsr);
			logger.debug("getByIdDsr: dsrMetadata {}", dsrMetadata);
			if (null == dsrMetadata) {
				putValue(result, STATUS, STATUS_KO);
				putValue(result, ERROR_MESSAGE, "DSR_METADATA record not found");
				return Response.ok(gson.toJson(result)).build();
			}
			final String year = null == dsrMetadata.getYear() ? null : String.format("%04d", dsrMetadata.getYear());
			final String month = null == dsrMetadata.getPeriod() ? null :
					"quarter".equalsIgnoreCase(dsrMetadata.getPeriodType()) ?
							String.format("Q%d", dsrMetadata.getPeriod()) : String.format("%02d", dsrMetadata.getPeriod());
			putValue(result, "idDsr", idDsr);
			putValue(result, "country", dsrMetadata.getCountry());
			putValue(result, "currency", dsrMetadata.getCurrency());
			putValue(result, "serviceCode", dsrMetadata.getServiceCode());
			putValue(result, "salesLineNum", dsrMetadata.getSalesLinesNum());
			putValue(result, "subscriptionsNum", dsrMetadata.getSubscriptionsNum());
			putValue(result, "totalValue", dsrMetadata.getTotalValue());
			putValue(result, "periodType", dsrMetadata.getPeriodType());
			putValue(result, "period", dsrMetadata.getPeriod());
			putValue(result, "year", year);
			putValue(result, "month", month);

			putValue(result, "dsrCurrency", dsrMetadata.getCurrency()); // WARNING: for backward compatibility

			if (Strings.isNullOrEmpty(dsrMetadata.getCountry())) {
				putValue(result, ERROR_MESSAGE, "DSR_METADATA.COUNTRY is null or empty");
			}
			if (Strings.isNullOrEmpty(dsrMetadata.getCurrency())) {
				putValue(result, ERROR_MESSAGE, "DSR_METADATA.CURRENCY is null or empty");
			}
			if (null == dsrMetadata.getServiceCode()) {
				putValue(result, ERROR_MESSAGE, "DSR_METADATA.SERVICE_CODE is null");
			}
			if (Strings.isNullOrEmpty(dsrMetadata.getPeriodType())) {
				putValue(result, ERROR_MESSAGE, "DSR_METADATA.PERIOD_TYPE is null or empty");
			}
			if (null == dsrMetadata.getPeriod()) {
				putValue(result, ERROR_MESSAGE, "DSR_METADATA.PERIOD is null");
			}
			if (result.containsKey(ERROR_MESSAGE)) {
				putValue(result, STATUS, STATUS_KO);
				return Response.ok(gson.toJson(result)).build();
			}

			// ANAG_COUNTRY
			try {
				final String territory = entityManager
						.createQuery("select x.code from AnagCountry x where x.idCountry = :country", String.class)
						.setParameter("country", dsrMetadata.getCountry())
						.getSingleResult();
				putValue(result, "territory", territory);
			} catch (NoResultException | NonUniqueResultException e) {
				logger.error("getByIdDsr", e);
				final StringWriter out = new StringWriter();
				e.printStackTrace(new PrintWriter(out));
				putValue(result, STATUS, STATUS_KO);
				putValue(result, ERROR_MESSAGE, "ANAG_COUNTRY record not found");
				putValue(result, "stackTrace", out.toString());
				return Response.ok(gson.toJson(result)).build();
			}

			// COMMERCIAL_OFFERS
			final CommercialOffers commercialOffers = entityManager
					.find(CommercialOffers.class, dsrMetadata.getServiceCode());
			logger.debug("getByIdDsr: commercialOffers {}", commercialOffers);
			if (null == commercialOffers) {
				putValue(result, STATUS, STATUS_KO);
				putValue(result, ERROR_MESSAGE, "COMMERCIAL_OFFERS record not found");
				return Response.ok(gson.toJson(result)).build();
			}
			putValue(result, "idUtilizationType", commercialOffers.getIdUtilizationType());
			putValue(result, "commercialOffer", commercialOffers.getOffering());
			putValue(result, "idCommercialOffer", commercialOffers.getIdCommercialOffering());
			putValue(result, "appliedTariff", commercialOffers.getCcidAppliedTariffCode());
			putValue(result, "serviceType", commercialOffers.getCcidServiceTypeCode());
			putValue(result, "useType", commercialOffers.getCcidUseTypeCode());
			putValue(result, "tradingBrand", commercialOffers.getTradingBrand());

			putValue(result, "offering", commercialOffers.getOffering()); // WARNING: for backward compatibility
			putValue(result, "idCommercialOffering", commercialOffers.getIdCommercialOffering()); // WARNING: for backward compatibility

			// ANAG_UTILIZATION_TYPE
			final AnagUtilizationType anagUtilizationType = entityManager
					.find(AnagUtilizationType.class, commercialOffers.getIdUtilizationType());
			logger.debug("getByIdDsr: anagUtilizationType {}", anagUtilizationType);
			if (null == anagUtilizationType) {
				putValue(result, STATUS, STATUS_KO);
				putValue(result, ERROR_MESSAGE, "ANAG_UTILIZATION_TYPE record not found");
				return Response.ok(gson.toJson(result)).build();
			}
			putValue(result, "utilizationType", anagUtilizationType.getName());

			// ANAG_DSP
			final AnagDsp anagDsp = entityManager
					.find(AnagDsp.class, commercialOffers.getIdDSP());
			logger.debug("getByIdDsr: anagDsp {}", anagDsp);
			if (null == anagDsp) {
				putValue(result, STATUS, STATUS_KO);
				putValue(result, ERROR_MESSAGE, "ANAG_DSP record not found");
				return Response.ok(gson.toJson(result)).build();
			}
			putValue(result, "dsp", anagDsp.getFtpSourcePath());
			putValue(result, "idDsp", anagDsp.getIdDsp());
			putValue(result, "dspCode", anagDsp.getCode());
			putValue(result, "dspName", anagDsp.getName());
			putValue(result, "dspFtpPath", anagDsp.getFtpSourcePath());
			putValue(result, "receiver", anagDsp.getDescription());

			// period
			final String startMonth = null == dsrMetadata.getPeriod() || null == dsrMetadata.getPeriodType() ? null :
				"month".equalsIgnoreCase(dsrMetadata.getPeriodType()) ?
					String.format("%02d", dsrMetadata.getPeriod()) :
						String.format("%02d", dsrMetadata.getPeriod() * 3 - 2); // 1, 4, 7, 10
			final String endMonth = null == dsrMetadata.getPeriod() || null == dsrMetadata.getPeriodType() ? null :
				"month".equalsIgnoreCase(dsrMetadata.getPeriodType()) ?
					String.format("%02d", dsrMetadata.getPeriod()) :
						String.format("%02d", dsrMetadata.getPeriod() * 3); // 3, 6, 9, 12
			final String endDay = "11".equals(endMonth) || "04".equals(endMonth) || "06".equals(endMonth) || "09".equals(endMonth) ? "30" : // trenta giorni conta novembre con april giugno e settembre 
						"02".equals(endMonth) ? (isBisestile(dsrMetadata.getYear()) ? "29" : "28") : // di 28 ce n'è uno...
							null != endMonth ? "31" : null; // tutti gli altri ne han 31
			final String periodStartDate = null == year || null == startMonth ? null : year + startMonth + "01";
			final String periodEndDate = null == year || null == endMonth || null == endDay ? null : year + endMonth + endDay;
			putValue(result, "periodStartDate", periodStartDate);
			putValue(result, "periodEndDate", periodEndDate);
			putValue(result, "periodStartMonth", startMonth);
			putValue(result, "periodEndMonth", endMonth);
			putValue(result, "periodEndDay", endDay);

			// WARNING: hyper-cube slice builder additions
			putValue(result, "sliceDspCode",
					"ITA".equalsIgnoreCase(dsrMetadata.getCountry()) ?
							anagDsp.getCode() : "ALL");
			putValue(result, "sliceMonth", startMonth);

			// WARNING: claim additions
			if ("month".equalsIgnoreCase(dsrMetadata.getPeriodType())) {
				putValue(result, "claimPeriodType", "M");
			} else if ("quarter".equalsIgnoreCase(dsrMetadata.getPeriodType())) {
				putValue(result, "claimPeriodType", "T");
			} else if ("semester".equalsIgnoreCase(dsrMetadata.getPeriodType())) {
				putValue(result, "claimPeriodType", "S");
			} else if ("annual".equalsIgnoreCase(dsrMetadata.getPeriodType())) {
				putValue(result, "claimPeriodType", "A");
			}
			putValue(result, "claimUtilizationType", "O");

			// DROOLS_PRICING
			final List<String> dtableS3Urls = entityManager
				.createNativeQuery(new StringBuffer()
					.append("select A.XLS_S3_URL XLS_S3_URL")
					.append(" from DROOLS_PRICING A, COMMERCIAL_OFFERS B")
					.append(" where B.ID_COMMERCIAL_OFFERS = ").append(dsrMetadata.getServiceCode())
					.append("   and A.IDDSP = B.IDDSP")
					.append("   and A.ID_UTILIZATION_TYPE = B.ID_UTIILIZATION_TYPE")
					.append("   and DATE_FORMAT(A.VALID_FROM, '%Y-%m') <= ")
							.append("'" + year + "-" + startMonth + "'")
					.append(" order by A.VALID_FROM desc")
					.append(" limit 1").toString())
				.getResultList();
			logger.debug("getByIdDsr: dtableS3Urls {}", dtableS3Urls);
			if (null == dtableS3Urls || dtableS3Urls.isEmpty()) {
				putValue(result, STATUS, STATUS_KO);
				putValue(result, ERROR_MESSAGE, "DROOLS_PRICING record not found");
				return Response.ok(gson.toJson(result)).build();
			}
			final String dtableS3Url = dtableS3Urls.get(0);
			logger.debug("getByIdDsr: dtableS3Url {}", dtableS3Url);
			putValue(result, "dtableS3Url", dtableS3Url);

			putValue(result, "xlsS3Url", dtableS3Url); // WARNING: for backward compatibility

			// RIGHT_SPLIT
			final List<RightSplit> rightSplits = entityManager
					.createQuery(new StringBuffer()
						.append("select x from RightSplit x")
						.append(" where x.country = :country")
						.append("   and x.idUtilizationType = :idUtilizationType")
						.append("   and x.validFrom <= :validFrom")
						.append(" order by x.validFrom desc")
						.toString(), RightSplit.class)
					.setParameter("country", dsrMetadata.getCountry())
					.setParameter("idUtilizationType", commercialOffers.getIdUtilizationType())
					.setParameter("validFrom", new SimpleDateFormat("yyyy-MM-dd")
							.parse(year + "-" + startMonth + "-01"))
					.getResultList();
			logger.debug("getByIdDsr: rightSplits {}", rightSplits);
			if (null == rightSplits || rightSplits.isEmpty()) {
				putValue(result, STATUS, STATUS_KO);
				putValue(result, ERROR_MESSAGE, "RIGHT_SPLIT record not found");
				return Response.ok(gson.toJson(result)).build();
			}
			final RightSplit rightSplit = rightSplits.get(0);
			logger.debug("getByIdDsr: rightSplit {}", rightSplit);
			putValue(result, "drmSplit", rightSplit.getDrm());
			putValue(result, "demSplit", rightSplit.getDem());

			// BLACKLIST
			final List<String> blacklistS3Urls = entityManager
				.createNativeQuery(new StringBuffer()
					.append("select XLS_S3_URL")
					.append(" from BLACKLIST")
					.append(" where COUNTRY = '" + dsrMetadata.getCountry() + "'")
					.append("   and CODICE_SOCIETA_TUTELA = 'SIAE'") // per retrocompatibilità
					.append("   and VALID_FROM <= STR_TO_DATE('").append(year + "-" + startMonth).append("', '%Y-%m')")
					.append("   order by VALID_FROM desc")
					.append("   limit 1").toString())
					.getResultList();
			logger.debug("getByIdDsr: blacklistS3Urls {}", blacklistS3Urls);
			if (null == blacklistS3Urls || blacklistS3Urls.isEmpty()) {
				if ("ITA".equalsIgnoreCase(dsrMetadata.getCountry())) {
					putValue(result, STATUS, STATUS_KO);
					putValue(result, ERROR_MESSAGE, "BLACKLIST record not found");
					return Response.ok(gson.toJson(result)).build();
				}
			} else {
				final String blacklistS3Url = blacklistS3Urls.get(0);
				logger.debug("getByIdDsr: blacklistS3Url {}", blacklistS3Url);
				putValue(result, "blacklistS3Url", blacklistS3Url);
			}

			// CCID_CONFIG
			final List<Object[]> ccidConfigs = entityManager
					.createQuery(new StringBuffer()
						.append("select x.encoded")
						.append(", x.encodedProvisional")
						.append(", x.notEncoded")
						.append(", y.name")
						.append(", x.currency")
						.append(", x.idCCIDConfig")
						.append(" from CCIDConfig x, AnagCCIDVersion y")
						.append(" where (x.idDsp = :idDsp or x.idDsp = '*')")
						.append(" and (x.idUtilizationType = :idUtilizationType or  x.idUtilizationType = '*')")
						.append(" and (x.idCommercialOffers = :idCommercialOffers or x.idCommercialOffers = '*') ")
						.append(" and (x.idCountry= :idCountry or x.idCountry = '*')")
						.append(" and x.ccdiVersion = y.idCCIDVersion")
						.append(" order by x.rank desc")
					.toString())
					.setParameter("idDsp", anagDsp.getIdDsp())
					.setParameter("idUtilizationType", commercialOffers.getIdUtilizationType())
					.setParameter("idCommercialOffers", Integer.toString(commercialOffers.getIdCommercialOffering()))
					.setParameter("idCountry", dsrMetadata.getCountry())
					.getResultList();
			if (null == ccidConfigs || ccidConfigs.isEmpty()) {
				putValue(result, STATUS, STATUS_KO);
				putValue(result, ERROR_MESSAGE, "CCID_CONFIG record not found");
				return Response.ok(gson.toJson(result)).build();
			}
			final Object[] ccidConfig = ccidConfigs.get(0);
			logger.debug("getByIdDsr: ccidConfig {}", Arrays.toString(ccidConfig));
			final Boolean ccidConfigEncoded = (Boolean) ccidConfig[0];
			final Boolean ccidConfigEncodedProvisional = (Boolean) ccidConfig[1];
			final Boolean ccidConfigNotEncoded = (Boolean) ccidConfig[2];
			final String ccidConfigVersion = (String) ccidConfig[3];
			final String ccidConfigCurrency = (String) ccidConfig[4];
			final Integer idCCIDConfig = (Integer) ccidConfig[5];
			putValue(result, "ccidEncoded", ccidConfigEncoded);
			putValue(result, "ccidEncodedProvisional", ccidConfigEncodedProvisional);
			putValue(result, "ccidNotEncoded", ccidConfigNotEncoded);
			putValue(result, "ccidVersion", ccidConfigVersion);
			putValue(result, "ccidCurrency", ccidConfigCurrency);


			final List<CCIDConfig> CCIDConfigs = entityManager
					.createQuery(new StringBuffer()
						.append("select c ")
						.append("from CCIDConfig c ")
						.append("where c.idCCIDConfig = :idCCIDConfig ").toString())
					.setParameter("idCCIDConfig", idCCIDConfig).getResultList();

			if (!CCIDConfigs.isEmpty()) {
				List<AnagCCIDConfigParam> anagCCIDConfigParams = CCIDConfigs.get(0).getAnagCCIDConfigParamList();

				for (AnagCCIDConfigParam param : anagCCIDConfigParams) {
					putValue(result, param.getAnagCCIDParams().getName(), param.getValue() == null? param.getAnagCCIDParams().getValue() : param.getValue() );
				}
			}

			putValue(result, "encoded", ccidConfigEncoded); // WARNING: for backward compatibility
			putValue(result, "encodedProvisional", ccidConfigEncodedProvisional); // WARNING: for backward compatibility
			putValue(result, "notEncoded", ccidConfigNotEncoded); // WARNING: for backward compatibility

			// CURRENCY_RATE
			BigDecimal conversionRate = BigDecimal.ONE;
			if ("EUR".equalsIgnoreCase(ccidConfigCurrency) &&
					!"EUR".equalsIgnoreCase(dsrMetadata.getCurrency())) {
				final List<CurrencyRate> currencyRates = entityManager
						.createQuery(new StringBuffer()
							.append("select x from CurrencyRate x")
							.append(" where x.year = :year")
							.append(" and x.month = :month")
							.append(" and x.srcCurrency = :srcCurrency")
							.append(" and x.dstCurrency = :dstCurrency")
							.toString(), CurrencyRate.class)
						.setParameter("year", year)
						.setParameter("month", month)
						.setParameter("srcCurrency", dsrMetadata.getCurrency())
						.setParameter("dstCurrency", ccidConfigCurrency)
						.getResultList();
				logger.debug("getByIdDsr: currencyRates {}", currencyRates);
				if (null == currencyRates || currencyRates.isEmpty()) {
					putValue(result, STATUS, STATUS_KO);
					putValue(result, ERROR_MESSAGE, "CURRENCY_RATE record not found");
					return Response.ok(gson.toJson(result)).build();
				}
				conversionRate = currencyRates.get(0).getRate();
				logger.debug("getByIdDsr: conversionRate {}",  conversionRate);
			}
			putValue(result, "conversionRate", conversionRate);

			// CCID_UNIQUE_ID_HISTORY
			BigInteger ccidId = null;
			Long headerCcidID = null;
			CcidUniqueIdHistory ccidUniqueIdHistory = entityManager
					.find(CcidUniqueIdHistory.class, idDsr);
			logger.debug("getByIdDsr: ccidUniqueIdHistory {}",  ccidUniqueIdHistory);
			if (null != ccidUniqueIdHistory) {
				ccidId = ccidUniqueIdHistory.getIdCcid();
				headerCcidID = ccidUniqueIdHistory.getId();
			}

			// CCID_UNIQUE_ID
			if (null == ccidId || null == headerCcidID) {
				try {
					// begin transaction
					entityManager.getTransaction().begin();
					// CCID_UNIQUE_ID nextval
					entityManager.createNativeQuery("UPDATE CCID_UNIQUE_ID SET ID=LAST_INSERT_ID(ID+1)")
							.executeUpdate();
					final BigInteger id = (BigInteger) entityManager
							.createNativeQuery("SELECT LAST_INSERT_ID()")
							.getSingleResult();
					headerCcidID = id.longValue();
					ccidId = id.multiply(new BigInteger("1000000000000")); // up to 1 tera rows per DSR
					// insert into CCID_UNIQUE_ID_HISTORY
					ccidUniqueIdHistory = new CcidUniqueIdHistory();
					ccidUniqueIdHistory.setIdDsr(idDsr);
					ccidUniqueIdHistory.setId(headerCcidID);
					ccidUniqueIdHistory.setIdCcid(ccidId);
					entityManager.persist(ccidUniqueIdHistory);
					// commit transaction
					entityManager.getTransaction().commit();
				} catch (NoResultException | NonUniqueResultException e) {
					entityManager.getTransaction().rollback(); // rollback transaction
					logger.error("getByIdDsr", e);
					final StringWriter out = new StringWriter();
					e.printStackTrace(new PrintWriter(out));
					putValue(result, STATUS, STATUS_KO);
					putValue(result, ERROR_MESSAGE, "CCID_UNIQUE_ID nextval error");
					putValue(result, "stackTrace", out.toString());
					return Response.ok(gson.toJson(result)).build();
				}
			}
			logger.debug("getByIdDsr: ccidId {}",  ccidId);
			putValue(result, "ccidId", ccidId);
			putValue(result, "headerCcidID", headerCcidID); // added on 12.07.2017

			String mandatoConfig = "SELECT mandante.tenant, " +
		             "       mandante.CODICE, " +
		             "       country.CODE, " +
		             "       bl.XLS_S3_URL " +
		             "FROM MANDATO_CONFIGURAZIONE c " +
		             "  JOIN MANDATO m ON m.ID = c.ID_MANDATO " +
		             "  JOIN SOCIETA_TUTELA mandataria ON m.ID_MANDATARIA = mandataria.ID " +
		             "  JOIN SOCIETA_TUTELA mandante ON m.ID_MANDANTE = mandante.ID " +
		             "  JOIN ANAG_COUNTRY country ON mandante.HOME_TERRITORY = country.ID_COUNTRY " +
		             "  LEFT JOIN (SELECT bl1.* " +
		             "             FROM (SELECT bl.* " +
		             "                   FROM BLACKLIST bl " +
		             "                   WHERE DATE_SUB(bl.VALID_FROM, INTERVAL 1 DAY) <= STR_TO_DATE(?1,'%Y-%m')) bl1 " +
		             "               LEFT JOIN (SELECT bl.* " +
		             "                          FROM BLACKLIST bl " +
		             "                          WHERE DATE_SUB(bl.VALID_FROM, INTERVAL 1 DAY) <= STR_TO_DATE(?1,'%Y-%m')) bl2 " +
		             "                      ON bl1.CODICE_SOCIETA_TUTELA = bl2.CODICE_SOCIETA_TUTELA " +
		             "                     AND bl1.VALID_FROM < bl2.VALID_FROM " +
		             "             WHERE bl2.VALID_FROM IS NULL) AS bl " +
		             "         ON (mandante.CODICE = bl.CODICE_SOCIETA_TUTELA) " +
		             "WHERE (c.DSP = ?2 OR c.DSP = '*') " +
		             "AND   (c.TIPO_UTILIZZO = ?3 OR c.TIPO_UTILIZZO = '*') " +
		             "AND   (c.OFFERTA_COMMERCIALE = ?4 OR c.OFFERTA_COMMERCIALE = '*') " +
		             "AND   (c.TERRITORIO = ?5 OR c.TERRITORIO = '*') " +
		             "AND   ((STR_TO_DATE(?1,'%Y-%m') >= DATE_SUB(c.VALIDA_DA, INTERVAL 1 SECOND)) AND (c.VALIDA_A IS NULL OR STR_TO_DATE(?1,'%Y-%m') <= DATE_SUB(c.VALIDA_A, INTERVAL -1 SECOND))) " +
		             "GROUP BY mandante.CODICE, " +
		             "         mandante.HOME_TERRITORY, " +
		             "         bl.XLS_S3_URL ";

			Query q = entityManager.createNativeQuery(mandatoConfig)
			.setParameter(1, year + "-" + startMonth)
			.setParameter(2, anagDsp.getIdDsp())
			.setParameter(3, commercialOffers.getIdUtilizationType())
			.setParameter(4, Integer.toString(commercialOffers.getIdCommercialOffering()))
			.setParameter(5, dsrMetadata.getCountry());
			List<Object[]> configs = q.getResultList();

			JsonObject tenant = new JsonObject();
			JsonArray mandators = new JsonArray();
			for (int i = 0; i < configs.size(); i++) {
				Integer isTenant = (Integer) configs.get(i)[0];
				if (isTenant == 1) {
					tenant.addProperty("society", (String) configs.get(i)[1]);
					tenant.addProperty("territory", (String) configs.get(i)[2]);
					tenant.addProperty("black_list_url", (String) configs.get(i)[3]);
				} else {
					JsonObject mandator = new JsonObject();
					mandator.addProperty("society", (String) configs.get(i)[1]);
					mandator.addProperty("territory", (String) configs.get(i)[2]);
					mandator.addProperty("black_list_url", (String) configs.get(i)[3]);
					mandators.add(mandator);
				}
			}

			// invoice status
			final boolean invoiced = isInvoiced(entityManager, idDsr);
			logger.debug("getByIdDsr: invoiced {}", invoiced);
			putValue(result, "invoiced", invoiced);

			// fixed fields
			putValue(result, "sender", "SIAE");
			putValue(result, "workCodeType", "SIAE_WORK_CODE");

			// that's all folks!
			putValue(result, STATUS, STATUS_OK);

			JsonElement json = gson.toJsonTree(result);
			json.getAsJsonObject().add("tenant",tenant);
			json.getAsJsonObject().add("mandators",mandators);

			return Response.ok(gson.toJson(json)).build();

		} catch (Exception e) {
			logger.error("getByIdDsr", e);
			final StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			putValue(result, STATUS, STATUS_KO);
			putValue(result, ERROR_MESSAGE, e.getMessage());
			putValue(result, "stackTrace", out.toString());
			return Response.ok(gson.toJson(result)).build();
		}
	}

	protected static boolean isInvoiced(EntityManager entityManager, String idDsr) {
		final StringBuffer sql = new StringBuffer()
				.append(" SELECT ")
				.append("   IF (COUNT(*) > 0 , 'true', 'false') AS FATTURATO ")
				.append(" FROM INVOICE_ITEM_TO_CCID ")
				.append("   LEFT JOIN CCID_METADATA ")
				.append("     ON INVOICE_ITEM_TO_CCID.ID_CCID_METADATA = CCID_METADATA.ID_CCID_METADATA ")
				.append("  WHERE CCID_METADATA.ID_DSR = '").append(idDsr).append("'");
		final String invoiced = (String) entityManager
				.createNativeQuery(sql.toString())
				.getSingleResult();
		return "true".equalsIgnoreCase(invoiced);
	}
}
