package com.alkemytech.sophia.codman.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.GsonBuilder;

@XmlRootElement
@Entity(name="ActiveClusters")
@Table(name="ACTIVE_CLUSTERS")
public class ActiveClusters {

	@Id
	@Column(name="IDCLUSTER", nullable=false)
	private String idCluster;

	@Column(name="NAME", nullable=false)
	private String name;

	public String getIdCluster() {
		return idCluster;
	}

	public void setIdCluster(String idCluster) {
		this.idCluster = idCluster;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}
}
