package com.alkemytech.sophia.codman.multimedialelocale.repository;

import com.alkemytech.sophia.codman.multimedialelocale.dto.PeriodLimits;

public interface IDsrMetadataRepository {
    PeriodLimits getPeriodLimits();
}
