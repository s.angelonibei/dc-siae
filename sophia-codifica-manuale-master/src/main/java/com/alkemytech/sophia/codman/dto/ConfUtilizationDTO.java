package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;
import java.util.List;

public class ConfUtilizationDTO implements Serializable{

	private static final long serialVersionUID = 2950766515317118354L;
	
	private String utilizationType;
	private List<String> offerinfgList;
	
	public ConfUtilizationDTO(String utilizationType, List<String> offerinfgList) {
		super();
		this.utilizationType = utilizationType;
		this.offerinfgList = offerinfgList;
	}
	
	public String getUtilizationType() {
		return utilizationType;
	}
	public void setUtilizationType(String utilizationType) {
		this.utilizationType = utilizationType;
	}
	public List<String> getOfferinfgList() {
		return offerinfgList;
	}
	public void setOfferinfgList(List<String> offerinfgList) {
		this.offerinfgList = offerinfgList;
	}
	
}
