package com.alkemytech.sophia.codman.broadcasting;

import com.alkemytech.sophia.broadcasting.dto.BdcConfigDTO;
import com.alkemytech.sophia.broadcasting.dto.BdcConfigurationDTO;
import com.alkemytech.sophia.broadcasting.dto.SearchBroadcasterConfigDTO;
import com.alkemytech.sophia.broadcasting.model.BdcBroadcasterConfig;
import com.alkemytech.sophia.broadcasting.service.interfaces.BroadcasterConfigService;
import com.amazonaws.util.CollectionUtils;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.servlet.RequestParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Alessandro Russo on 27/11/2017.
 */
@Singleton
@Path("broadcasterConfig")
public class BroadcasterConfigRestService {

	private final static String NO_RESULTS = "No results found";

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private BroadcasterConfigService service;
	private Gson gson;

	@Inject
	public BroadcasterConfigRestService(BroadcasterConfigService service, Gson gson) {
		this.service = service;
		this.gson = gson;
	}

	@GET
	@Path("allConfig")
	@Produces("application/json")
	public Response findAll() {
		try {
			List<BdcBroadcasterConfig> result = service.findAll();
			if (!CollectionUtils.isNullOrEmpty(result)) {
				return Response.ok(gson.toJson(result)).build();
			} else {
				return Response.ok(gson.toJson(NO_RESULTS)).build();
			}
		} catch (Exception e) {
			logger.error("findAll", e);
		}
		return Response.status(500).build();
	}

	@POST
	@Path("activeConfigAt")
	@Produces("application/json")
	public Response findActive(@RequestParameters SearchBroadcasterConfigDTO dto) {
		try {
			BdcConfigDTO result = service.findActive(dto);
			if (result != null) {
				return Response.ok(gson.toJson(result)).build();
			} else {
				return Response.ok(gson.toJson(NO_RESULTS)).build();
			}
		} catch (Exception e) {
			logger.error("findAll", e);
		}
		return Response.status(500).build();
	}

	@POST
	@Path("activeConfig")
	@Produces("application/json")
	public Response findAllActive(@QueryParam(value = "idEmittente") Integer idEmittente) {
		try {
			List<BdcBroadcasterConfig> result = service.findBroadcasterActivConfiguration(idEmittente);
			if (result != null) {
				return Response.ok(gson.toJson(result)).build();
			} else {
				return Response.ok(gson.toJson(NO_RESULTS)).build();
			}
		} catch (Exception e) {
			logger.error("findAll", e);
		}
		return Response.status(500).build();
	}

	@POST
	@Path("addConfig")
	@Produces("application/json")
	public Response addConfiguration(BdcConfigurationDTO bdcConfigurationDTO) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		Date inizioValid = null;
		Date fineValid = null;
		try {
			inizioValid = sdf.parse(bdcConfigurationDTO.getValidFrom());
		} catch (Exception e) {
			inizioValid = new Date();
		}
		try {
			fineValid = sdf.parse(bdcConfigurationDTO.getValidTo());
		} catch (Exception e) {

		}
		try {
			Integer result = service.createConfig(bdcConfigurationDTO.getIdBroadcaster(),
					bdcConfigurationDTO.getIdChannel(), inizioValid, fineValid,
					bdcConfigurationDTO.getBdcBroadcasterConfigTemplate(),bdcConfigurationDTO.getUsername());
			if (result==200) {
				return Response.ok(gson.toJson(result)).build();
			} else {
				return Response.status(result).build();
			}
		} catch (Exception e) {
			logger.error("findAll", e);
		}
		return Response.status(500).build();
	}
	

	@POST
	@Path("updateConfig")
	@Produces("application/json")
	public Response updateConfiguration(BdcConfigurationDTO bdcConfigurationDTO) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		Date inizioValid = null;
		Date fineValid = null;
		try {
			inizioValid = sdf.parse(bdcConfigurationDTO.getValidFrom());
		} catch (Exception e) {
			inizioValid = new Date();
		}
		try {
			fineValid = sdf.parse(bdcConfigurationDTO.getValidTo());
		} catch (Exception e) {

		}
		try {
			Integer result = service.updateConfig(bdcConfigurationDTO.getIdConfiguration(),inizioValid, fineValid,bdcConfigurationDTO.getUsername(),
					bdcConfigurationDTO.getBdcBroadcasterConfigTemplate());
			if (result==200) {
				return Response.ok(gson.toJson(result)).build();
			} else {
				return Response.status(result).build();
			}
		} catch (Exception e) {
			logger.error("findAll", e);
		}
		return Response.status(500).build();
	}

	@POST
	@Path("deleteConfig")
	@Produces("application/json")
	public Response deleteConfiguration(@QueryParam(value = "idConfig") Integer idConfig,@QueryParam(value = "modifyUser") String modifyUser) {
		try {
			Integer result = service.deleteConfig(idConfig, modifyUser);
			if (result==200) {
				return Response.ok(gson.toJson(result)).build();
			} else {
				return Response.status(result).build();
			}
		} catch (Exception e) {
			logger.error("deleteConfig", e);
		}
		return Response.status(500).build();
		}
}
