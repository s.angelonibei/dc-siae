package com.alkemytech.sophia.codman.entity.performing;

import java.io.Serializable;

/**
 * Created by idilello on 6/14/16.
 */
public class InformazioniPM implements Serializable{

    private Long totalePM=0L;
    private Long totaleElaborati=0L;
    private Long totaleSospesi=0L;
    private Long totaleErrori=0L;
    private String error="-1";
    private Long percentuale=2L;

    private Long suspende=0L;
    private Long terminate=0L;

    public Long getTotalePM() {
        return totalePM;
    }

    public void setTotalePM(Long totalePM) {
        this.totalePM = totalePM;
    }

    public Long getTotaleElaborati() {
        return totaleElaborati;
    }

    public void setTotaleElaborati(Long totaleElaborati) {
        this.totaleElaborati = totaleElaborati;
    }

    public Long getTotaleSospesi() {
        return totaleSospesi;
    }

    public void setTotaleSospesi(Long totaleSospesi) {
        this.totaleSospesi = totaleSospesi;
    }

    public Long getTotaleErrori() {
        return totaleErrori;
    }

    public void setTotaleErrori(Long totaleErrori) {
        this.totaleErrori = totaleErrori;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Long getPercentuale() {
        return percentuale;
    }

    public void setPercentuale(Long percentuale) {
        this.percentuale = percentuale;
    }

    public Long getSuspende() {
        return suspende;
    }

    public void setSuspende(Long suspende) {
        this.suspende = suspende;
    }

    public Long getTerminate() {
        return terminate;
    }

    public void setTerminate(Long terminate) {
        this.terminate = terminate;
    }
}
