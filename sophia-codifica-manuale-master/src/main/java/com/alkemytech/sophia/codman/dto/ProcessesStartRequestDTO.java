package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ProcessesStartRequestDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<ProcessMetaDataDTO> processes;

	public List<ProcessMetaDataDTO> getProcesses() {
		return processes;
	}

	public void setProcesses(List<ProcessMetaDataDTO> processes) {
		this.processes = processes;
	}

}
