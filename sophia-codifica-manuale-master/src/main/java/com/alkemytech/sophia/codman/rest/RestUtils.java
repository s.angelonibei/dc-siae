package com.alkemytech.sophia.codman.rest;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.JsonObject;

public abstract class RestUtils {

	public static JsonObject asJsonObject(String...items) {
		final JsonObject jsonObject = new JsonObject();
		if (null != items) for (int i = 1; i <= items.length; i += 2) {
			jsonObject.addProperty(items[i - 1], items[i]);
		}
		return jsonObject;
	}
	
	public static Map<String, String> asMap(String...items) {
		final Map<String, String> result = new HashMap<String, String>();
		if (null != items) for (int i = 1; i <= items.length; i += 2) {
			result.put(items[i - 1], items[i]);
		}
		return result;
	}
	
	public static String asString(Throwable e) {
		final StringWriter out = new StringWriter();
		e.printStackTrace(new PrintWriter(out));
		return out.toString();
	}
	
}
