package com.alkemytech.sophia.codman.entity;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.codman.persistence.AbstractEntity;

@XmlRootElement
@Entity(name="DettailImportiAnticipo")
public class DettailImportiAnticipo extends AbstractEntity<String>{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	

	@Id
	private Integer idDettailImportiAnticipo;
	private Integer idImportoAnticipo;
	private Date periodoPertinenzaInizio;
	private Date periodoPertinenzaFine;
	private String numeroFattura;
	private BigDecimal importoOriginale;
	private BigDecimal importoUtilizzabile;
	private String idDsp;
	private Integer idJoinAnticipiCcid;
	private String idCcid;
	private BigDecimal quotaAnticipo;
    private BigInteger idCCIDMetadata;
    private String idDsr;
    private BigDecimal totalValue;
    private String currency;
    private String ccidVersion;
    private Boolean ccidEncoded;
    private Boolean ccidEncodedProvisional;
    private Boolean ccidNotEncoded;
    private Integer invoiceItem;
    private String invoiceStatus;
    private BigDecimal valoreFatturabile;
	
    public DettailImportiAnticipo(){}
	
	public DettailImportiAnticipo( Integer idImportoAnticipo,
			Date periodoPertinenzaInizio, Date periodoPertinenzaFine, String numeroFattura, BigDecimal importoOriginale,
			BigDecimal importoUtilizzabile, String idDsp,BigDecimal quotaAnticipo,String idCcid, 
			String idDsr, BigDecimal valoreFatturabile, BigDecimal totalValue
			) {
		
		this.idImportoAnticipo=idImportoAnticipo;
		this.periodoPertinenzaInizio=periodoPertinenzaInizio;
		this.periodoPertinenzaFine=periodoPertinenzaFine;
		this.numeroFattura=numeroFattura;
		this.importoOriginale=importoOriginale;
		this.importoUtilizzabile=importoUtilizzabile;
		this.idDsp=idDsp;
		this.quotaAnticipo=quotaAnticipo;
		this.idCcid=idCcid;
		this.idDsr=idDsr;
		this.valoreFatturabile=valoreFatturabile;
		this.totalValue=totalValue;
		
	}	
    
	@Override
	public String getId() {
		return getIdDettailImportiAnticipo().toString();
	}

	public Integer getIdDettailImportiAnticipo() {
		return idDettailImportiAnticipo;
	}

	public void setIdDettailImportiAnticipo(Integer idDettailImportiAnticipo) {
		this.idDettailImportiAnticipo = idDettailImportiAnticipo;
	}

	public Integer getIdImportoAnticipo() {
		return idImportoAnticipo;
	}

	public void setIdImportoAnticipo(Integer idImportoAnticipo) {
		this.idImportoAnticipo = idImportoAnticipo;
	}

	public Date getPeriodoPertinenzaInizio() {
		return periodoPertinenzaInizio;
	}

	public void setPeriodoPertinenzaInizio(Date periodoPertinenzaInizio) {
		this.periodoPertinenzaInizio = periodoPertinenzaInizio;
	}

	public Date getPeriodoPertinenzaFine() {
		return periodoPertinenzaFine;
	}

	public void setPeriodoPertinenzaFine(Date periodoPertinenzaFine) {
		this.periodoPertinenzaFine = periodoPertinenzaFine;
	}

	public String getNumeroFattura() {
		return numeroFattura;
	}

	public void setNumeroFattura(String numeroFattura) {
		this.numeroFattura = numeroFattura;
	}

	public BigDecimal getImportoOriginale() {
		return importoOriginale;
	}

	public void setImportoOriginale(BigDecimal importoOriginale) {
		this.importoOriginale = importoOriginale;
	}

	public BigDecimal getImportoUtilizzabile() {
		return importoUtilizzabile;
	}

	public void setImportoUtilizzabile(BigDecimal importoUtilizzabile) {
		this.importoUtilizzabile = importoUtilizzabile;
	}

	public String getIdDsp() {
		return idDsp;
	}

	public void setIdDsp(String idDsp) {
		this.idDsp = idDsp;
	}

	public Integer getIdJoinAnticipiCcid() {
		return idJoinAnticipiCcid;
	}

	public void setIdJoinAnticipiCcid(Integer idJoinAnticipiCcid) {
		this.idJoinAnticipiCcid = idJoinAnticipiCcid;
	}

	public String getIdCcid() {
		return idCcid;
	}

	public void setIdCcid(String idCcid) {
		this.idCcid = idCcid;
	}

	public BigDecimal getQuotaAnticipo() {
		return quotaAnticipo;
	}

	public void setQuotaAnticipo(BigDecimal quotaAnticipo) {
		this.quotaAnticipo = quotaAnticipo;
	}

	public BigInteger getIdCCIDMetadata() {
		return idCCIDMetadata;
	}

	public void setIdCCIDMetadata(BigInteger idCCIDMetadata) {
		this.idCCIDMetadata = idCCIDMetadata;
	}

	public String getIdDsr() {
		return idDsr;
	}

	public void setIdDsr(String idDsr) {
		this.idDsr = idDsr;
	}

	public BigDecimal getTotalValue() {
		return totalValue;
	}

	public void setTotalValue(BigDecimal totalValue) {
		this.totalValue = totalValue;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getCcidVersion() {
		return ccidVersion;
	}

	public void setCcidVersion(String ccidVersion) {
		this.ccidVersion = ccidVersion;
	}

	public Boolean getCcidEncoded() {
		return ccidEncoded;
	}

	public void setCcidEncoded(Boolean ccidEncoded) {
		this.ccidEncoded = ccidEncoded;
	}

	public Boolean getCcidEncodedProvisional() {
		return ccidEncodedProvisional;
	}

	public void setCcidEncodedProvisional(Boolean ccidEncodedProvisional) {
		this.ccidEncodedProvisional = ccidEncodedProvisional;
	}

	public Boolean getCcidNotEncoded() {
		return ccidNotEncoded;
	}

	public void setCcidNotEncoded(Boolean ccidNotEncoded) {
		this.ccidNotEncoded = ccidNotEncoded;
	}

	public Integer getInvoiceItem() {
		return invoiceItem;
	}

	public void setInvoiceItem(Integer invoiceItem) {
		this.invoiceItem = invoiceItem;
	}

	public String getInvoiceStatus() {
		return invoiceStatus;
	}

	public void setInvoiceStatus(String invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}

	public BigDecimal getValoreFatturabile() {
		return valoreFatturabile;
	}

	public void setValoreFatturabile(BigDecimal valoreFatturabile) {
		this.valoreFatturabile = valoreFatturabile;
	}

}
