package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ProcessSearchDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String tag;

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

}
