package com.alkemytech.sophia.codman.blacklist;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

public class RegexBlacklistValidator implements BlacklistValidator {

	private static final String COL0_REGEX = ".*";
	private static final String COL1_REGEX = "CI|ED";
	private static final String COL2_CI_REGEX = "([0-9]{1,20})?(\\:[0-9]{1,20})*";
	private static final String COL2_ED_REGEX = "([A-Z\\-]{1,20})?";
	private static final String COL3_CI_REGEX = "(\\!SIAE)|(([A-Z\\-]{1,20})(\\:[A-Z\\-]{1,20})*)";
	private static final String COL3_ED_REGEX = ".*";
	private static final String COL4_REGEX = "(DEM)|(DRM)|(DEM\\:DRM)|(DRM\\:DEM)";
	private static final String COL5_REGEX = "(ALL)|(([0-9A-Z]{3}\\-[0-9]{6})(\\:[0-9A-Z]{3}\\-[0-9]{6})*)";
	
	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Inject
	protected RegexBlacklistValidator() {
		super();
	}
	
	@Override
	public List<String> validate(File file) {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(file));
			List<String> result = null;
			String line;
			for (int i = 1; null != (line = reader.readLine()); i ++) {
				final String error = validate(i, line);
				if (null != error) {
					if (null == result) {
						result = new LinkedList<String>();
					}
					result.add(error);
					if (result.size() > 20) {
						result.add("Too many errors...");
						break;
					}
				}
            }
			return result;
		} catch (Exception e) {
			logger.error("validate", e);
			final StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			return Arrays.asList("Bad blacklist CSV file", e.getMessage(), out.toString());
		} finally {
			if (null != reader) {
				try {
					reader.close();
				} catch (IOException e) {
					logger.error("validate", e);
				}
			}
		}
	}
	
	private String validate(int rownum, String line) {
		final String[] columns = line.split(";");
		if (!columns[0].matches(COL0_REGEX)) {
			return "Invalid line " + rownum + " column 1: \"" + columns[0] + "\"";
		}
		if (!columns[1].matches(COL1_REGEX)) {
			return "Invalid line " + rownum + " column 2: \"" + columns[1] + "\"";
		}
		if ("CI".equals(columns[1])) {
			if (!columns[2].matches(COL2_CI_REGEX)) {
				return "Invalid line " + rownum + " column 3: \"" + columns[2] + "\"";
			}
			if (!columns[3].matches(COL3_CI_REGEX)) {
				return "Invalid line " + rownum + " column 4: \"" + columns[3] + "\"";
			}
		} else if ("ED".equals(columns[1])) {
			if (!columns[2].matches(COL2_ED_REGEX)) {
				return "Invalid line " + rownum + " column 3: \"" + columns[2] + "\"";
			}
			if (!columns[3].matches(COL3_ED_REGEX)) {
				return "Invalid line " + rownum + " column 4: \"" + columns[3] + "\"";
			}
		} else {
			return "Invalid line " + rownum + " column 2: \"" + columns[1] + "\"";
		}
		if (!columns[4].matches(COL4_REGEX)) {
			return "Invalid line " + rownum + " column 5: \"" + columns[4] + "\"";
		}
		if (!columns[5].matches(COL5_REGEX)) {
			return "Invalid line " + rownum + " column 6: \"" + columns[5] + "\"";
		}
		return null;
	}

//	public static void main(String[] args) {
//	
//		System.out.println("COL0_REGEX");
//		System.out.println("EMI Anglo-American repertoire".matches(COL0_REGEX));
//		System.out.println("".matches(COL0_REGEX));
//		
//		System.out.println("COL1_REGEX");
//		System.out.println("CI".matches(COL1_REGEX));
//		System.out.println("ED".matches(COL1_REGEX));
//		System.out.println("ZZ".matches(COL1_REGEX));
//
//		System.out.println("COL2_CI_REGEX");
//		System.out.println("".matches(COL2_CI_REGEX));
//		System.out.println("00035943081".matches(COL2_CI_REGEX));
//		System.out.println("00221851397:00038970942".matches(COL2_CI_REGEX));
//		System.out.println("00221851397;00038970942".matches(COL2_CI_REGEX));
//		System.out.println("AA221851397:00038970942".matches(COL2_CI_REGEX));
//
//		System.out.println("COL2_ED_REGEX");
//		System.out.println("".matches(COL2_ED_REGEX));
//		System.out.println("HDS-ZAMP".matches(COL2_ED_REGEX));
//		System.out.println("SGAE".matches(COL2_ED_REGEX));
//		System.out.println("HDS_ZAMP".matches(COL2_ED_REGEX));
//
//		System.out.println("COL3_CI_REGEX");
//		System.out.println("".matches(COL3_CI_REGEX));
//		System.out.println("PRS:IMRO:ASCAP:BMI:SESAC:SOCAN:APRA:AMRA:SAMRO:NM".matches(COL3_CI_REGEX));
//		System.out.println("!SIAE".matches(COL3_CI_REGEX));
//		System.out.println("!SIAE:IMRO".matches(COL3_CI_REGEX));
//		System.out.println("10432:ABCD".matches(COL3_CI_REGEX));
//
//		System.out.println("COL4_REGEX");
//		System.out.println("".matches(COL4_REGEX));
//		System.out.println("DEM".matches(COL4_REGEX));
//		System.out.println("DRM".matches(COL4_REGEX));
//		System.out.println("DEM:DRM".matches(COL4_REGEX));
//		System.out.println("dem".matches(COL4_REGEX));
//
//		System.out.println("COL5_REGEX");
//		System.out.println("".matches(COL5_REGEX));
//		System.out.println("ALL".matches(COL5_REGEX));
//		System.out.println("GOO-201303:GOS-201303:YOM-201310:YOU-201310:ITU-201308:ITR-201308".matches(COL5_REGEX));
//		System.out.println("GOO-201303".matches(COL5_REGEX));
//		System.out.println("GOO-201404:GOS-201404:YOM-201310:YOU-201310:ITU-201507:ITR-201507:AMA-201507:DEE-201507:RHA-201507:VEV-201507:XBO-201507:7DG-201507:SPO-201601".matches(COL5_REGEX));
//		System.out.println("ALL:GOO-201303".matches(COL5_REGEX));
//		System.out.println("ALLALL".matches(COL5_REGEX));
//
//	}
	
}

