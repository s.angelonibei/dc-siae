package com.alkemytech.sophia.codman.entity.performing;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;


@XmlRootElement
@Entity
@Table(name = "PERF_CODIFICA")
	public class Codifica {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name = "ID_CODIFICA")
	private Long idCodifica;

	@Column(name = "ID_COMBANA")
	private Long idCombana;

	@Column(name = "CODICE_OPERA_SUGGERITO")
	private String codiceOperaSuggerito;

	@Column(name = "DATA_CODIFICA")
	private Date dataCodifica;

	@Column(name = "DIST_SEC_CANDIDATA")
	private Double distSecCandidata;

	@Column(name = "CONFIDENZA")
	private Double confidenza;

	@Column(name = "VALORE_ECONOMICO")
	private Double valoreEconomico;

	@Column(name = "CANDIDATE")
	private String candidate;

	@Column(name = "TIPO_APPROVAZIONE")
	private String tipoApprovazione;

	@Column(name = "DATA_APPROVAZIONE")
	private Date dataApprovazione;

	@Column(name = "STATO_APPROVAZIONE")
	private String statoApprovazione;

	@Column(name = "CODICE_OPERA_APPROVATO")
	private String codiceOperaApprovato;

	@Column(name = "DATA_FINE_VALIDITA")
	private Date dataFineValidita;

	@Column(name = "N_UTILIZZAZIONI")
	private Long nUtilizzazioni;

	@Column(name = "PREZIOSA")
	private Boolean preziosa;

	public Codifica() {
		super();
	}

	public Codifica(Long idCodifica, Long idCombana, String codiceOperaSuggerito, Date dataCodifica, Double distSecCandidata, Double confidenza, Double valoreEconomico, String candidate, String tipoApprovazione, Date dataApprovazione, String statoApprovazione, String codiceOperaApprovato, Date dataFineValidita, Long nUtilizzazioni, Boolean preziosa) {
		super();
		this.idCodifica = idCodifica;
		this.idCombana = idCombana;
		this.codiceOperaSuggerito = codiceOperaSuggerito;
		this.dataCodifica = dataCodifica;
		this.distSecCandidata = distSecCandidata;
		this.confidenza = confidenza;
		this.valoreEconomico = valoreEconomico;
		this.candidate = candidate;
		this.tipoApprovazione = tipoApprovazione;
		this.dataApprovazione = dataApprovazione;
		this.statoApprovazione = statoApprovazione;
		this.codiceOperaApprovato = codiceOperaApprovato;
		this.dataFineValidita = dataFineValidita;
		this.nUtilizzazioni = nUtilizzazioni;
		this.preziosa = preziosa;
	}

	public Long getIdCodifica() {
		return idCodifica;
	}

	public void setIdCodifica(Long idCodifica) {
		this.idCodifica = idCodifica;
	}

	public Long getIdCombana() {
		return idCombana;
	}

	public void setIdCombana(Long idCombana) {
		this.idCombana = idCombana;
	}

	public String getCodiceOperaSuggerito() {
		return codiceOperaSuggerito;
	}

	public void setCodiceOperaSuggerito(String codiceOperaSuggerito) {
		this.codiceOperaSuggerito = codiceOperaSuggerito;
	}

	public Date getDataCodifica() {
		return dataCodifica;
	}

	public void setDataCodifica(Date dataCodifica) {
		this.dataCodifica = dataCodifica;
	}

	public Double getDistSecCandidata() {
		return distSecCandidata;
	}

	public void setDistSecCandidata(Double distSecCandidata) {
		this.distSecCandidata = distSecCandidata;
	}

	public Double getConfidenza() {
		return confidenza;
	}

	public void setConfidenza(Double confidenza) {
		this.confidenza = confidenza;
	}

	public Double getValoreEconomico() {
		return valoreEconomico;
	}

	public void setValoreEconomico(Double valoreEconomico) {
		this.valoreEconomico = valoreEconomico;
	}

	public String getCandidate() {
		return candidate;
	}

	public void setCandidate(String candidate) {
		this.candidate = candidate;
	}

	public String getTipoApprovazione() {
		return tipoApprovazione;
	}

	public void setTipoApprovazione(String tipoApprovazione) {
		this.tipoApprovazione = tipoApprovazione;
	}

	public Date getDataApprovazione() {
		return dataApprovazione;
	}

	public void setDataApprovazione(Date dataApprovazione) {
		this.dataApprovazione = dataApprovazione;
	}

	public String getStatoApprovazione() {
		return statoApprovazione;
	}

	public void setStatoApprovazione(String statoApprovazione) {
		this.statoApprovazione = statoApprovazione;
	}

	public String getCodiceOperaApprovato() {
		return codiceOperaApprovato;
	}

	public void setCodiceOperaApprovato(String codiceOperaApprovato) {
		this.codiceOperaApprovato = codiceOperaApprovato;
	}

	public Date getDataFineValidita() {
		return dataFineValidita;
	}

	public void setDataFineValidita(Date dataFineValidita) {
		this.dataFineValidita = dataFineValidita;
	}

	public Long getnUtilizzazioni() {
		return nUtilizzazioni;
	}

	public void setnUtilizzazioni(Long nUtilizzazioni) {
		this.nUtilizzazioni = nUtilizzazioni;
	}

	public Boolean getPreziosa() {
		return preziosa;
	}

	public void setPreziosa(Boolean preziosa) {
		this.preziosa = preziosa;
	}
}
