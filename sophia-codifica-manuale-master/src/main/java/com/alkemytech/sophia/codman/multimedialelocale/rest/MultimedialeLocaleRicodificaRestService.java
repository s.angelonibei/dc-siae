package com.alkemytech.sophia.codman.multimedialelocale.rest;

import com.alkemytech.sophia.codman.entity.performing.security.ReportPage;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.alkemytech.sophia.codman.multimedialelocale.dto.DettagliEsecuzione;
import com.alkemytech.sophia.codman.multimedialelocale.dto.MultimedialeLocaleEsecuzioniDto;
import com.alkemytech.sophia.codman.multimedialelocale.dto.MultimedialeLocaleEsecuzioniRequestDto;
import com.alkemytech.sophia.codman.multimedialelocale.dto.PeriodiRilavorati;
import com.alkemytech.sophia.codman.multimedialelocale.entity.MultimedialeLocale;
import com.alkemytech.sophia.codman.rest.PagedResult;
import com.amazonaws.util.CollectionUtils;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.Predicate;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

@Singleton
@Path("multimediale/ricodifica")
public class MultimedialeLocaleRicodificaRestService {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Gson gson;
	private final Provider<EntityManager> provider;
	private final static String NO_RESULTS = "{\"message\":\"Nessun dato presente per la ricerca effettuata\"}";
	private final static String DATA_INVIO_ERRATA = "{\"message\":\"le date d'invio inserite non sono valide\"}";
	private final static String DATA_COMPETENZA_ERRATA = "{\"message\":\"le date di competenza inserite non sono valide\"}";

	@Inject
	public MultimedialeLocaleRicodificaRestService(@McmdbDataSource Provider<EntityManager> provider, Gson gson) {
		super();
		this.provider = provider;
		this.gson = gson;
	}

	
	@POST
	@Path("getEsecuzioniRicodifica")
	@Produces("application/json")
	public Response getEsecuzioniRicodifica(MultimedialeLocaleEsecuzioniRequestDto multimedialeLocaleEsecuzioniRequestDto) {

		EntityManager entityManager = provider.get();
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
		List<Predicate> predicates = new ArrayList<>();

		String query="\n" + 
				"SELECT \n" + 
				"	r.ID_ML_RIPROCESSAMENTO,\n" + 
				"	r.DATA_AVVIO,\n" + 
				"    count(*) as REPORT_DA_RIPROCESSARE,\n" + 
				"    SUM(CASE WHEN m.CODIFICA_STATISTICHE IS null THEN 0 ELSE 1 END) AS REPORT_RIPROCESSATI,\n" + 
				"    group_concat(distinct m.ORIG_PERIODO_COMPETENZA order by m.ORIG_PERIODO_COMPETENZA asc separator ', ') as PERIODI_COMPETENZA\n" + 
				"FROM \n" + 
				"	ML_RIPROCESSAMENTO r, \n" + 
				"    ML_REPORT m\n" + 
				"where \n" + 
				"	r.ID_ML_RIPROCESSAMENTO = m.RIPROCESSAMENTO\n";
		try {
			if (multimedialeLocaleEsecuzioniRequestDto.getDataAvvioDa()!=null) {
				query = query+"AND r.DATA_AVVIO >= '"+sdf1.format(sdf.parse(multimedialeLocaleEsecuzioniRequestDto.getDataAvvioDa()))+" 00:00:00'";
			}	
		}catch (Exception e) {

		}
		try {
			if (multimedialeLocaleEsecuzioniRequestDto.getDataAvvioA()!=null) {
				query = query+"AND r.DATA_AVVIO <= '"+sdf1.format(sdf.parse(multimedialeLocaleEsecuzioniRequestDto.getDataAvvioA()))+" 23:59:59'";
			}	
		}catch (Exception e) {

		}	
		
		query=query+"group by r.ID_ML_RIPROCESSAMENTO\n" + 
				"order by r.DATA_AVVIO desc";

		int first = (multimedialeLocaleEsecuzioniRequestDto.getFirst() > 0 ? multimedialeLocaleEsecuzioniRequestDto.getFirst()-1 : 0);
		int last = (multimedialeLocaleEsecuzioniRequestDto.getLast() > 0 ? multimedialeLocaleEsecuzioniRequestDto.getLast() : 50);
		
		List<Object[]> result = (List<Object[]>) entityManager.createNativeQuery(query)
				.setFirstResult(first)
				.setMaxResults(ReportPage.NUMBER_RECORD_PER_PAGE)
				.getResultList();
		
		List<MultimedialeLocaleEsecuzioniDto> esecuzioni = new ArrayList<>();
		for(Object[] record:result) {
			esecuzioni.add(new MultimedialeLocaleEsecuzioniDto(record));
		}
	
		if (!CollectionUtils.isNullOrEmpty(esecuzioni)) {
			final PagedResult pagedResult = new PagedResult();
			if (null != esecuzioni && !esecuzioni.isEmpty()) {
				final int maxrows = multimedialeLocaleEsecuzioniRequestDto.getLast() - multimedialeLocaleEsecuzioniRequestDto.getFirst();
				final boolean hasNext = esecuzioni.size() > maxrows;
				pagedResult.setRows(!hasNext ? esecuzioni : esecuzioni.subList(0, maxrows)).setMaxrows(maxrows)
						.setFirst(multimedialeLocaleEsecuzioniRequestDto.getFirst())
						.setLast(hasNext ? multimedialeLocaleEsecuzioniRequestDto.getLast()
								: multimedialeLocaleEsecuzioniRequestDto.getFirst() + esecuzioni.size())
						.setHasNext(hasNext).setHasPrev(multimedialeLocaleEsecuzioniRequestDto.getFirst() > 0);
			} else {
				pagedResult.setMaxrows(0).setFirst(0).setLast(0).setHasNext(false).setHasPrev(false);
			}
			return Response.ok(gson.toJson(pagedResult)).build();
		} else {
			return Response.ok(NO_RESULTS).build();
		}

	}

	@POST
	@Path("getDettaglioEsecuzione")
	@Produces("application/json")
	public Response getDettaglioEsecuzione(@QueryParam(value = "idEsecuzione") Integer idEsecuzione) {
		EntityManager entityManager = provider.get();
		String query ="\n" + 
				"select \n" + 
				"	DATA_RICEZIONE_DOCUMENTAZIONE\n" + 
				"from \n" + 
				"	ML_RIPROCESSAMENTO\n" + 
				"where \n" + 
				"	ID_ML_RIPROCESSAMENTO = "+idEsecuzione+";\n" + 
				"";
		String query2="SELECT \n" + 
				"	p.PERIODO,\n" + 
				"	p.DATA_RICEZIONE_MINICUBO,\n" + 
				"    count(*) as REPORT_DA_RIPROCESSARE,\n" + 
				"    SUM(CASE WHEN r.CODIFICA_STATISTICHE IS null THEN 0 ELSE 1 END) AS REPORT_RIPROCESSATI\n" + 
				"FROM \n" + 
				"	ML_RIPROCESSAMENTO_PERIODO p, \n" + 
				"    ML_REPORT r\n" + 
				"where \n" + 
				"	p.ID_ML_RIPROCESSAMENTO = r.RIPROCESSAMENTO\n" + 
				"    and p.PERIODO = r.ORIG_PERIODO_COMPETENZA\n" + 
				"    and p.ID_ML_RIPROCESSAMENTO = "+idEsecuzione+"\n" + 
				"group by p.PERIODO\n" + 
				"order by p.DATA_FINE_PERIODO desc;\n" + 
				"";
		String query3="SELECT\n" + 
				"	COUNT(*) as REPORT_DA_RIPROCESSARE,\n" + 
				"	SUM(CASE WHEN n.CODIFICA_STATISTICHE IS null THEN 0 ELSE 1 END) AS REPORT_RIPROCESSATI,\n" + 
				"    SUM(v.VALIDAZIONE_RECORD_VALIDI) RECORD_DA_RIPROCESSARE,\n" + 
				"    SUM(CASE WHEN n.CODIFICA_STATISTICHE IS null THEN 0 ELSE n.VALIDAZIONE_RECORD_VALIDI END) AS RECORD_RIPROCESSATI,\n" + 
				"    SUM(v.VALIDAZIONE_UTILIZZAZIONI_VALIDE) UTILIZZAZIONI_DA_RIPROCESSARE,\n" + 
				"    SUM(CASE WHEN n.CODIFICA_STATISTICHE IS null THEN 0 ELSE n.VALIDAZIONE_UTILIZZAZIONI_VALIDE END) AS UTILIZZAZIONI_RIPROCESSATE,\n" + 
				"    \n" + 
				"    SUM(CASE WHEN n.CODIFICA_RECORD_CODIFICATI IS null THEN 0 ELSE n.CODIFICA_RECORD_CODIFICATI END) AS RECORD_CODIFICATI,\n" + 
				"    SUM(CASE WHEN n.CODIFICA_UTILIZZAZIONI_CODIFICATE IS null THEN 0 ELSE n.CODIFICA_UTILIZZAZIONI_CODIFICATE END) AS UTILIZZAZIONI_CODIFICATE,\n" + 
				"    \n" + 
				"    SUM(CASE WHEN n.CODIFICA_STATISTICHE IS null THEN 0 ELSE n.CODIFICA_STATISTICHE->'$.recordCodificatiRegolari' END) AS RECORD_CODIFICATI_REGOLARI,\n" + 
				"    SUM(CASE WHEN n.CODIFICA_STATISTICHE IS null THEN 0 ELSE n.CODIFICA_STATISTICHE->'$.utilizzazioniCodificateRegolari' END) AS UTILIZZAZIONI_CODIFICATE_REGOLARI,   \n" + 
				"	SUM(\n" + 
				"		CASE WHEN n.CODIFICA_UTILIZZAZIONI_CODIFICATE IS null \n" + 
				"			THEN 0 \n" + 
				"			ELSE (\n" + 
				"				CASE WHEN v.CODIFICA_UTILIZZAZIONI_CODIFICATE IS null \n" + 
				"					THEN 1\n" + 
				"					ELSE (\n" + 
				"						CASE WHEN n.CODIFICA_UTILIZZAZIONI_CODIFICATE <> v.CODIFICA_UTILIZZAZIONI_CODIFICATE \n" + 
				"							THEN 1\n" + 
				"							ELSE 0\n" + 
				"						END\n" + 
				"					)\n" + 
				"				END\n" + 
				"            )\n" + 
				"		END\n" + 
				"	) AS REPORT_AGGIORNATI\n" + 
				"from \n" + 
				"    ML_REPORT n LEFT JOIN ML_REPORT v on n.REPORT_PADRE = v.ID_ML_REPORT\n" + 
				"where\n" + 
				"	n.RIPROCESSAMENTO = "+idEsecuzione+";\n" + 
				"";

		List<PeriodiRilavorati> periodiRilavorati=new ArrayList<PeriodiRilavorati>();
		DettagliEsecuzione dettagliEsecuzione= new DettagliEsecuzione();	
	
		try {
			Object recordDataAcquisizione=entityManager.createNativeQuery(query).getSingleResult();	
			dettagliEsecuzione.setDataAcquisizioneDocumentazione((Timestamp)recordDataAcquisizione);
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			List<Object[]> recordPeriodo = (List<Object[]>) entityManager.createNativeQuery(query2).getResultList();	
			for(Object[] record:recordPeriodo) {
				periodiRilavorati.add(new PeriodiRilavorati(record));
			}
			dettagliEsecuzione.setPeriodiRilavorati(periodiRilavorati);
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			Object[] dettaglio = (Object[]) entityManager.createNativeQuery(query3).getSingleResult();	
			dettagliEsecuzione.setReportDaRiprocessare((Long) dettaglio[0]);
			dettagliEsecuzione.setReportRiprocessati((BigDecimal) dettaglio[1]);
			dettagliEsecuzione.setRecordDaRiprocessare((BigDecimal) dettaglio[2]);
			dettagliEsecuzione.setRecordRiprocessati((BigDecimal) dettaglio[3]);
			dettagliEsecuzione.setUtilizzazioniDaRiprocessare((BigDecimal) dettaglio[4]);
			dettagliEsecuzione.setUtilizzazioniRiprocessati((BigDecimal) dettaglio[5]);
			dettagliEsecuzione.setRecordCodificati((BigDecimal) dettaglio[6]);
			dettagliEsecuzione.setUtilizzazioniCodificate((BigDecimal) dettaglio[7]);
			dettagliEsecuzione.setRecordCodificatiRegolari((Double) dettaglio[8]);
			dettagliEsecuzione.setUtilizzazioniCodificateRegolari((Double) dettaglio[9]);
			dettagliEsecuzione.setReportAggiornati((BigDecimal) dettaglio[10]);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return Response.ok(gson.toJson(dettagliEsecuzione)).build();
	}
	

	@POST
	@Path("getReportEsecuzione")
	@Produces("application/json")
	public Response getReportEsecuzione(@QueryParam(value = "idEsecuzione") Integer idEsecuzione,@QueryParam(value = "first") Integer first,@QueryParam(value = "last") Integer last) {
		
		EntityManager entityManager = provider.get();
		String query ="SELECT\n" + 
				"    n.DATA_CODIFICA,\n" + 
				"    n.NOMINATIVO_LICENZIATARIO,\n" + 
				"    n.LICENZA,\n" + 
				"    n.SERVIZIO,\n" + 
				"    n.ORIG_PERIODO_COMPETENZA,\n" + 
				"    mu.MODALITA,\n" + 
				"    n.MOD_FRUIZIONE,\n" + 
				"    n.IN_ABBONAMENTO,\n" + 
				"    n.STATO_LOGICO,\n" + 
				"    v.VALIDAZIONE_UTILIZZAZIONI_VALIDE AS TOTALE_UTILIZZAZIONI,\n" + 
				"    v.CODIFICA_UTILIZZAZIONI_CODIFICATE AS VECCHIO_CODIFICATO_UTILIZZAZIONI,\n" + 
				"    n.CODIFICA_UTILIZZAZIONI_CODIFICATE AS CODIFICATO_UTILIZZAZIONI,\n" + 
				"    n.RIPARTIZIONE\n" + 
				"from \n" + 
				"	ML_MODALITA_UTILIZZAZIONE mu join\n" + 
				"    ML_REPORT n on mu.ID = n.MOD_UTILIZZAZIONE\n" + 
				"    LEFT JOIN ML_REPORT v on n.REPORT_PADRE = v.ID_ML_REPORT\n" + 
				"where\n" + 
				"	n.RIPROCESSAMENTO = " + idEsecuzione + "\n" + 
				"order by n.ORIG_PERIODO_COMPETENZA;";
		
		List<Object[]> result = (List<Object[]>) entityManager.createNativeQuery(query)
				.setFirstResult(first-1)
				.setMaxResults(ReportPage.NUMBER_RECORD_PER_PAGE)
				.getResultList();
		
		List<MultimedialeLocaleDto> reportList= new ArrayList<>();
		for(Object[] record:result) {
			reportList.add(new MultimedialeLocaleDto(record));
		}
		if (!CollectionUtils.isNullOrEmpty(reportList)) {
			final PagedResult pagedResult = new PagedResult();
			if (null != reportList && !reportList.isEmpty()) {
				final int maxrows = last - first;
				final boolean hasNext = reportList.size() > maxrows;
				pagedResult.setRows(!hasNext ? reportList : reportList.subList(0, maxrows)).setMaxrows(maxrows)
						.setFirst(first)
						.setLast(hasNext ? last
								: first + reportList.size())
						.setHasNext(hasNext).setHasPrev(first > 0);
			} else {
				pagedResult.setMaxrows(0).setFirst(0).setLast(0).setHasNext(false).setHasPrev(false);
			}
			return Response.ok(gson.toJson(pagedResult)).build();
		} else {
			return Response.ok(NO_RESULTS).build();
		}
		
	}
	
}
