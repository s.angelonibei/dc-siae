package com.alkemytech.sophia.codman.rest.performing.serviceimpl;

import java.util.Arrays;
import java.util.List;

import com.alkemytech.sophia.codman.dto.performing.CombanaDTO;

public class CodificaBaseCombanaAdapter extends SequentialAdapter<CombanaDTO> {

	public CodificaBaseCombanaAdapter() {
		addModifier(new Modifier<CombanaDTO>() {
			@Override
			public void apply(CombanaDTO combana) {
				combana.setMaturato(null);
			}
			@Override
			public List<String> getApplicationFields() {
				return Arrays.asList();
			}
		}).addModifier(new Modifier<CombanaDTO>() {
			@Override
			public void apply(CombanaDTO combana) {
				combana.setRischio(null);
			}
			@Override
			public List<String> getApplicationFields() {
				return Arrays.asList();
			}
		}).addModifier(new Modifier<CombanaDTO>() {
			@Override
			public void apply(CombanaDTO combana) {
				combana.setStatoOpera(null);
			}
			@Override
			public List<String> getApplicationFields() {
				return Arrays.asList();
			}
		})
		;
	}
}
