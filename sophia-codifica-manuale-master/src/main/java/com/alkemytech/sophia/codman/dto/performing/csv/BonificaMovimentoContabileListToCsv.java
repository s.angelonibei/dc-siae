package com.alkemytech.sophia.codman.dto.performing.csv;

import java.math.BigDecimal;
import java.util.Date;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

public class BonificaMovimentoContabileListToCsv {
	@CsvBindByPosition(position = 0)
	@CsvBindByName
	private String origineRecord;
	@CsvBindByPosition(position = 1)
	@CsvBindByName
	private Long idProgrammaMusicale;
	@CsvBindByPosition(position = 2)
	@CsvBindByName
	private String numeroPm;
	@CsvBindByPosition(position = 3)
	@CsvBindByName
	private Long idEvento;
	@CsvBindByPosition(position = 4)
	@CsvBindByName
	private String tipologiaMovimento;
	@CsvBindByPosition(position = 5)
	@CsvBindByName
	private BigDecimal importoManuale;
	@CsvBindByPosition(position = 6)
	@CsvBindByName
	private String codiceVoceIncasso;
	@CsvBindByPosition(position = 7)
	@CsvBindByName
	private String numeroPermesso;
	@CsvBindByPosition(position = 8)
	@CsvBindByName
	private BigDecimal totaleDemLordoPm;
	@CsvBindByPosition(position = 9)
	@CsvBindByName
	private Long numeroReversale;
	@CsvBindByPosition(position = 10)
	@CsvBindByName
	private String flagGruppoPrincipale;
	@CsvBindByPosition(position = 11)
	@CsvBindByName
	private Long numeroFattura;
	@CsvBindByPosition(position = 12)
	@CsvBindByName
	private BigDecimal importoDemTotale;
	@CsvBindByPosition(position = 13)
	@CsvBindByName
	private Long numeroPmPrevisti;
	@CsvBindByPosition(position = 14)
	@CsvBindByName
	private Long numeroPmPrevistiSpalla;
	@CsvBindByPosition(position = 15)
	@CsvBindByName
	private Date dataReversale;
	@CsvBindByPosition(position = 16)
	@CsvBindByName
	private Date dataRientro;
	@CsvBindByPosition(position = 17)
	@CsvBindByName
	private Long contabilita;
	@CsvBindByPosition(position = 18)
	@CsvBindByName
	private String tipoDocumentoContabile;
	@CsvBindByPosition(position = 19)
	@CsvBindByName
	private String codiceRaggruppamento;
	@CsvBindByPosition(position = 20)
	@CsvBindByName
	private String sede;
	@CsvBindByPosition(position = 21)
	@CsvBindByName
	private String agenzia;

	public BonificaMovimentoContabileListToCsv() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	public BonificaMovimentoContabileListToCsv(Object[] row) {
		super();
		this.origineRecord = (String) row[0];
		this.idProgrammaMusicale = (Long) row[1];
		this.idEvento = (Long) row[2];
		this.numeroPm = (String) row[3];
		this.tipologiaMovimento = (String) row[4];
		this.importoManuale = (BigDecimal) row[5];
		this.codiceVoceIncasso = (String) row[6];
		this.numeroPermesso = (String) row[7];
		this.totaleDemLordoPm = (BigDecimal) row[8];
		this.numeroReversale = (Long) row[9];
		this.flagGruppoPrincipale = (String) row[10];
		this.numeroFattura = (Long) row[11];
		this.importoDemTotale = (BigDecimal) row[12];
		this.numeroPmPrevisti = (Long) row[13];
		this.numeroPmPrevistiSpalla = (Long) row[14];
		this.dataReversale = (Date) row[15];
		this.dataRientro = (Date) row[16];
		this.contabilita = (Long) row[17];
		this.tipoDocumentoContabile = (String) row[18];
		this.codiceRaggruppamento = (String) row[19];
		this.sede = (String) row[20];
		this.agenzia = (String) row[21];
	}

	
	public String getOrigineRecord() {
		return origineRecord;
	}

	public void setOrigineRecord(String origineRecord) {
		this.origineRecord = origineRecord;
	}

	public Long getIdProgrammaMusicale() {
		return idProgrammaMusicale;
	}

	public void setIdProgrammaMusicale(Long idProgrammaMusicale) {
		this.idProgrammaMusicale = idProgrammaMusicale;
	}

	public String getNumeroPm() {
		return numeroPm;
	}

	public void setNumeroPm(String numeroPm) {
		this.numeroPm = numeroPm;
	}

	public Long getIdEvento() {
		return idEvento;
	}

	public void setIdEvento(Long idEvento) {
		this.idEvento = idEvento;
	}

	public String getTipologiaMovimento() {
		return tipologiaMovimento;
	}

	public void setTipologiaMovimento(String tipologiaMovimento) {
		this.tipologiaMovimento = tipologiaMovimento;
	}

	public BigDecimal getImportoManuale() {
		return importoManuale;
	}

	public void setImportoManuale(BigDecimal importoManuale) {
		this.importoManuale = importoManuale;
	}

	public String getCodiceVoceIncasso() {
		return codiceVoceIncasso;
	}

	public void setCodiceVoceIncasso(String codiceVoceIncasso) {
		this.codiceVoceIncasso = codiceVoceIncasso;
	}

	public String getNumeroPermesso() {
		return numeroPermesso;
	}

	public void setNumeroPermesso(String numeroPermesso) {
		this.numeroPermesso = numeroPermesso;
	}

	public BigDecimal getTotaleDemLordoPm() {
		return totaleDemLordoPm;
	}

	public void setTotaleDemLordoPm(BigDecimal totaleDemLordoPm) {
		this.totaleDemLordoPm = totaleDemLordoPm;
	}

	public Long getNumeroReversale() {
		return numeroReversale;
	}

	public void setNumeroReversale(Long numeroReversale) {
		this.numeroReversale = numeroReversale;
	}

	public String getFlagGruppoPrincipale() {
		return flagGruppoPrincipale;
	}

	public void setFlagGruppoPrincipale(String flagGruppoPrincipale) {
		this.flagGruppoPrincipale = flagGruppoPrincipale;
	}

	public Long getNumeroFattura() {
		return numeroFattura;
	}

	public void setNumeroFattura(Long numeroFattura) {
		this.numeroFattura = numeroFattura;
	}

	public BigDecimal getImportoDemTotale() {
		return importoDemTotale;
	}

	public void setImportoDemTotale(BigDecimal importoDemTotale) {
		this.importoDemTotale = importoDemTotale;
	}

	public Long getNumeroPmPrevisti() {
		return numeroPmPrevisti;
	}

	public void setNumeroPmPrevisti(Long numeroPmPrevisti) {
		this.numeroPmPrevisti = numeroPmPrevisti;
	}

	public Long getNumeroPmPrevistiSpalla() {
		return numeroPmPrevistiSpalla;
	}

	public void setNumeroPmPrevistiSpalla(Long numeroPmPrevistiSpalla) {
		this.numeroPmPrevistiSpalla = numeroPmPrevistiSpalla;
	}

	public Date getDataReversale() {
		return dataReversale;
	}

	public void setDataReversale(Date dataReversale) {
		this.dataReversale = dataReversale;
	}

	public Date getDataRientro() {
		return dataRientro;
	}

	public void setDataRientro(Date dataRientro) {
		this.dataRientro = dataRientro;
	}

	public Long getContabilita() {
		return contabilita;
	}

	public void setContabilita(Long contabilita) {
		this.contabilita = contabilita;
	}

	public String getTipoDocumentoContabile() {
		return tipoDocumentoContabile;
	}

	public void setTipoDocumentoContabile(String tipoDocumentoContabile) {
		this.tipoDocumentoContabile = tipoDocumentoContabile;
	}

	public String getCodiceRaggruppamento() {
		return codiceRaggruppamento;
	}

	public void setCodiceRaggruppamento(String codiceRaggruppamento) {
		this.codiceRaggruppamento = codiceRaggruppamento;
	}

	public String getSede() {
		return sede;
	}

	public void setSede(String sede) {
		this.sede = sede;
	}

	public String getAgenzia() {
		return agenzia;
	}

	public void setAgenzia(String agenzia) {
		this.agenzia = agenzia;
	}

	public String[] getMappingStrategy() {
		return new String[] { "Origine Record", "ID Programma Musicale", "Numero Pm", "ID Evento",
				"Tipologia Movimento", "Importo Manuale", "Codice Voce Incasso", "Numero Permesso",
				"Totale Dem Lordo Pm", "Numero Reversale", "Flag Gruppo Principale", "Numero Fattura",
				"Importo Dem Totale", "Numero Pm Previsti", "Numero Pm Previsti Spalla", "Data Reversale",
				"Data Rientro", "Contabilita'", "Tipo Documento Contabile", "Codice Raggruppamento", "Sede", "Agenzia"};
	}
}