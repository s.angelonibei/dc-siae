package com.alkemytech.sophia.codman.dto.performing;

import java.io.Serializable;

public class MonitoraggioCodificatoResponse implements Serializable {
    private String valori;

    public MonitoraggioCodificatoResponse() {
    }

    public MonitoraggioCodificatoResponse(String valori) {
        this.valori = valori;
    }

    public String getValori() {
        return valori;
    }

    public void setValori(String valori) {
        this.valori = valori;
    }
}
