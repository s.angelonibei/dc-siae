package com.alkemytech.sophia.codman.service.dsrmetadata.country;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.google.inject.Provider;

public class DSRCountryExtractorXXX implements DsrCountryExtractor{

	private final Provider<EntityManager> provider;
	private String regex = "[A-Z]{3}" ;
	private String code = "XXX";
	private String description = "XXX" ;
	
	public DSRCountryExtractorXXX(Provider<EntityManager> provider) {
		super();
		this.provider = provider;
	}
	
	@Override
	public String extractDsrCountryCode(String country) {
		EntityManager entityManager = null;
		String result;
		try {
			entityManager = provider.get();
			final Query q = entityManager.createQuery("select x.idCountry from AnagCountry x where x.idCountry = :idCountry");
			q.setParameter("idCountry", country);
			result = (String) q.getSingleResult();
			return result;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public String getRegex() {
		return regex;
	}

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return description;
	}

	
}
