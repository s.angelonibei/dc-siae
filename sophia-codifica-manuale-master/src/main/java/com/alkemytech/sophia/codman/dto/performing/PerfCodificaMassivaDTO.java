package com.alkemytech.sophia.codman.dto.performing;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PerfCodificaMassivaDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String receivedMessage;
	private String state;
	private Date insertTime;
	private Date endWorkTime;
	private String utente;
	private String fileName;
	private String fileInput;
	private String fileOutput;
	private String token;
	private String errorMessage;
		
	public PerfCodificaMassivaDTO(Long id, String receivedMessage, String state, Timestamp insertTime, Timestamp endWorkTime,
			String utente, String fileName, String fileInput, String fileOutput, String token) {
		super();
		this.id = id;
		this.receivedMessage = receivedMessage;
		this.state = state;
		this.insertTime = new Date(insertTime.getTime());
		this.endWorkTime = new Date(endWorkTime.getTime());
		this.utente = utente;
		this.fileName = fileName;
		this.fileInput = fileInput;
		this.fileOutput = fileOutput;
		this.token = token;
	}
	
	public PerfCodificaMassivaDTO() {
		super();
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getReceivedMessage() {
		return receivedMessage;
	}
	
	public void setReceivedMessage(String receivedMessage) {
		this.receivedMessage = receivedMessage;
	}
	
	public String getState() {
		return state;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	
	public Date getInsertTime() {
		return insertTime;
	}
	
	public void setInsertTime(Date insertTime) {
		this.insertTime = insertTime;
	}
	
	public Date getEndWorkTime() {
		return endWorkTime;
	}
	
	public void setEndWorkTime(Date endWorkTime) {
		this.endWorkTime = endWorkTime;
	}
	
	public String getUtente() {
		return utente;
	}
	
	public void setUtente(String utente) {
		this.utente = utente;
	}
	
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileInput() {
		return fileInput;
	}
	
	public void setFileInput(String fileInput) {
		this.fileInput = fileInput;
	}
	
	public String getFileOutput() {
		return fileOutput;
	}
	
	public void setFileOutput(String fileOutput) {
		this.fileOutput = fileOutput;
	}
	
	public String getToken() {
		return token;
	}
	
	public void setToken(String token) {
		this.token = token;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}