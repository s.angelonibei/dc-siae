package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class CCIDConfigDTO implements Serializable{


	private static final long serialVersionUID = -8125740677030318968L;

	private Integer idCCIDConfig;
	private String idDsp;
	private String dspName;
	private String idUtilizationType;
	private String utilizationName;
	private String idCommercialOffers;
	private String commercialOfferName;
	private String idCountry;
	private String countryName;
	private Boolean encoded;
	private Boolean encodedProvisional;
	private Boolean notEncoded;
	private Integer ccdiVersion;
	private String ccdiVersionName;
	private Integer rank;
	private String currency;
	private Map<String,String> ccidParams = new HashMap<String, String>();
	private Map<String, String> fileNaming;
	private Boolean excludeClaimZero;
	
	
	public CCIDConfigDTO(){
		
	}

	public CCIDConfigDTO(Object[] obj) {

		if (null != obj && obj.length == 17) {
			try {
				
				idCCIDConfig = (Integer) obj[0];
				idDsp = (String) obj[1];
				dspName = (String) obj[2];
				idUtilizationType = (String) obj[3];
				utilizationName = (String) obj[4];
				idCommercialOffers = (String) obj[5];
				commercialOfferName =  (String) obj[6];
				idCountry = (String) obj[7];
				countryName = (String) obj[8];
				encoded = (Boolean) obj[9];
				encodedProvisional = (Boolean) obj[10];
				notEncoded = (Boolean) obj[11];
				ccdiVersion = (Integer) obj[12];
				ccdiVersionName = (String) obj[13];
				rank = (Integer) obj[14];
				currency = (String) obj[15];
				excludeClaimZero = (Boolean) obj[16];

			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}
	
	public Integer getIdCCIDConfig() {
		return idCCIDConfig;
	}

	public void setIdCCIDConfig(Integer idCCIDConfig) {
		this.idCCIDConfig = idCCIDConfig;
	}

	public String getIdDsp() {
		return idDsp;
	}

	public void setIdDsp(String idDsp) {
		this.idDsp = idDsp;
	}

	public String getDspName() {
		return dspName;
	}

	public void setDspName(String dspName) {
		this.dspName = dspName;
	}

	public String getIdUtilizationType() {
		return idUtilizationType;
	}

	public void setIdUtilizationType(String idUtilizationType) {
		this.idUtilizationType = idUtilizationType;
	}

	public String getUtilizationName() {
		return utilizationName;
	}

	public void setUtilizationName(String utilizationName) {
		this.utilizationName = utilizationName;
	}

	public String getIdCommercialOffers() {
		return idCommercialOffers;
	}

	public void setIdCommercialOffers(String idCommercialOffers) {
		this.idCommercialOffers = idCommercialOffers;
	}

	public String getCommercialOfferName() {
		return commercialOfferName;
	}

	public void setCommercialOfferName(String commercialOfferName) {
		this.commercialOfferName = commercialOfferName;
	}

	public String getIdCountry() {
		return idCountry;
	}

	public void setIdCountry(String idCountry) {
		this.idCountry = idCountry;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public Boolean getEncoded() {
		return encoded;
	}

	public void setEncoded(Boolean encoded) {
		this.encoded = encoded;
	}

	public Boolean getEncodedProvisional() {
		return encodedProvisional;
	}

	public void setEncodedProvisional(Boolean encodedProvisional) {
		this.encodedProvisional = encodedProvisional;
	}

	public Boolean getNotEncoded() {
		return notEncoded;
	}

	public void setNotEncoded(Boolean notEncoded) {
		this.notEncoded = notEncoded;
	}

	public Integer getCcdiVersion() {
		return ccdiVersion;
	}

	public void setCcdiVersion(Integer ccdiVersion) {
		this.ccdiVersion = ccdiVersion;
	}

	public String getCcdiVersionName() {
		return ccdiVersionName;
	}

	public void setCcdiVersionName(String ccdiVersionName) {
		this.ccdiVersionName = ccdiVersionName;
	}

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Map<String,String> getCcidParams() {
		return ccidParams;
	}

	public void setCcidParams(Map<String,String> ccidParams) {
		this.ccidParams = ccidParams;
	}

	public Map<String, String> getfileNaming() {
		return this.fileNaming;
	}

	public void setfileNaming(Map<String, String> fileNaming) {
		this.fileNaming = fileNaming;
	}

	public Boolean getExcludeClaimZero() {
		return excludeClaimZero;
	}

	public void setExcludeClaimZero(Boolean excludeClaimZero) {
		this.excludeClaimZero = excludeClaimZero;
	}
}
