package com.alkemytech.sophia.codman.invoice.repository;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonFormat;

@Produces("application/json")
@XmlRootElement 
public class InvoiceSearchParameters implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7456433437053826513L;
	Integer first;
	Integer last;
	List<String> dspList;
	List<String> statusList;
	String invoiceCode;
	Date dateFrom;	
	Date dateTo;
	
	public InvoiceSearchParameters(){
		
	}

	public Integer getFirst() {
		return first;
	}

	public void setFirst(Integer first) {
		this.first = first;
	}

	public Integer getLast() {
		return last;
	}

	public void setLast(Integer last) {
		this.last = last;
	}
	
	public List<String> getDspList() {
		return dspList;
	}

	public void setDspList(List<String> dspList) {
		this.dspList = dspList;
	}

	public List<String> getStatusList() {
		return statusList;
	}

	public void setStatusList(List<String> statusList) {
		this.statusList = statusList;
	}

	public String getInvoiceCode() {
		return invoiceCode;
	}

	public void setInvoiceCode(String invoiceCode) {
		this.invoiceCode = invoiceCode;
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	
}
