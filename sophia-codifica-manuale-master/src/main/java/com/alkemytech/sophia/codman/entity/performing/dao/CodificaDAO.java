package com.alkemytech.sophia.codman.entity.performing.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.collections.CollectionUtils;
import org.apache.http.util.TextUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.codman.dto.performing.CodificaDto;
import com.alkemytech.sophia.codman.dto.performing.CodificaOpereDTO;
import com.alkemytech.sophia.codman.dto.performing.CodificaOpereRequestDTO;
import com.alkemytech.sophia.codman.dto.performing.MonitoraggioCodificatoResponse;
import com.alkemytech.sophia.codman.dto.performing.RiepilogoCodificaManualeResponse;
import com.alkemytech.sophia.codman.entity.performing.Codifica;
import com.alkemytech.sophia.codman.entity.performing.ConfigurazioniSoglie;
import com.alkemytech.sophia.codman.entity.performing.SessioneCodifica;
import com.alkemytech.sophia.codman.entity.performing.UtilizzazioniAssegnate;
import com.alkemytech.sophia.codman.entity.performing.security.ReportPage;
import com.alkemytech.sophia.codman.rest.performing.TimerSessionResponse;
import com.alkemytech.sophia.codman.utils.DateUtils;
import com.google.inject.Inject;
import com.google.inject.Provider;

public class CodificaDAO implements ICodificaDAO {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private Provider<EntityManager> provider;

    @Inject
    public CodificaDAO(Provider<EntityManager> provider) {
        this.provider = provider;
    }

    public void addConfigurazioneSoglie(ConfigurazioniSoglie configurazione) {

        EntityManager entityManager = provider.get();

        try {
            entityManager.getTransaction().begin();
            entityManager.persist(configurazione);
            entityManager.flush();
            entityManager.getTransaction().commit();
            entityManager.clear();
        } catch (Exception ex) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
        }
    }

    public void updateConfigurazioneSoglie(ConfigurazioniSoglie configurazione) {

        EntityManager entityManager = provider.get();
        ConfigurazioniSoglie confSoglie = entityManager.find(ConfigurazioniSoglie.class, configurazione.getId());

        try {
            entityManager.getTransaction().begin();
            confSoglie.setInizioPeriodoCompetenza(configurazione.getInizioPeriodoCompetenza());
            confSoglie.setFinePeriodoCompetenza(configurazione.getFinePeriodoCompetenza());
            confSoglie.setUtenteCaricamentoFile(configurazione.getUtenteCaricamentoFile());
            confSoglie.setDataCaricamentoFile(configurazione.getDataCaricamentoFile());
            confSoglie.setDataUltimaModifica(configurazione.getDataUltimaModifica());
            confSoglie.setUtenteUltimaModifica(configurazione.getUtenteUltimaModifica());
            confSoglie.setPathFile(configurazione.getPathFile());
            confSoglie.setNomeOriginaleFile(configurazione.getNomeOriginaleFile());

            entityManager.getTransaction().commit();
            entityManager.clear();
        } catch (Exception ex) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
        }
    }

    public ConfigurazioniSoglie configurazioneIsInCorso() {
        EntityManager entityManager = provider.get();

        String query = "select cs from ConfigurazioniSoglie cs where cs.flagAttivo=1 and cs.finePeriodoCompetenza is null";
        Query q = entityManager.createQuery(query);

        List<ConfigurazioniSoglie> result = (List<ConfigurazioniSoglie>) q.getResultList();

        entityManager.clear();

        if (result != null && result.size() > 0) {
            return result.get(0);
        } else {
            return null;
        }
    }

    public ConfigurazioniSoglie getConfigurazioneSoglieById(Long id) {

        EntityManager entityManager = provider.get();
        ConfigurazioniSoglie config = entityManager.find(ConfigurazioniSoglie.class, id);

        entityManager.clear();

        return config;
    }

    public List<Long> getCodiciRegolaAfterDate(String inizioPeriodoCompetenza) {
        EntityManager entityManager = provider.get();

        String query = "select cs.codiceConfigurazione from ConfigurazioniSoglie cs " + "where cs.flagAttivo='1' and "
                + "(cs.inizioPeriodoCompetenza>=:inizioPeriodoCompetenza "
                + "or cs.finePeriodoCompetenza>=:inizioPeriodoCompetenza) ";
        Query q = entityManager.createQuery(query);
        q.setParameter("inizioPeriodoCompetenza",
                DateUtils.stringToDate(inizioPeriodoCompetenza, DateUtils.DATE_ITA_FORMAT_DASH));

        List<Long> result = (List<Long>) q.getResultList();

        return result;
    }

    public List<Long> getValidazioneDateOnUpdate(String inizioPeriodoCompetenza, String finePeriodoCompetenza,
                                                 Long codiceConfigurazione) {
        EntityManager entityManager = provider.get();

        String query = "select distinct cs.codiceConfigurazione from ConfigurazioniSoglie cs "
                + "where cs.flagAttivo='1' " + "and (" + "( "
                + "		(cs.inizioPeriodoCompetenza between :inizioPeriodoCompetenza and :finePeriodoCompetenza) "
                + "		or "
                + "		(cs.finePeriodoCompetenza between :inizioPeriodoCompetenza and :finePeriodoCompetenza) " + ") "
                + "or ( " + "		cs.inizioPeriodoCompetenza<=:inizioPeriodoCompetenza " + "		and "
                + "		cs.finePeriodoCompetenza>=:finePeriodoCompetenza " + ")" + ") "
                + "and cs.codiceConfigurazione != :codiceConfigurazione ";
        Query q = entityManager.createQuery(query);

        q.setParameter("inizioPeriodoCompetenza",
                DateUtils.stringToDate(inizioPeriodoCompetenza, DateUtils.DATE_ITA_FORMAT_DASH));
        if (finePeriodoCompetenza != null || finePeriodoCompetenza.equals(""))
            q.setParameter("finePeriodoCompetenza",
                    DateUtils.stringToDate(finePeriodoCompetenza, DateUtils.DATE_ITA_FORMAT_DASH));
        else
            q.setParameter("finePeriodoCompetenza", null);

        q.setParameter("codiceConfigurazione", codiceConfigurazione);
        List<Long> result = (List<Long>) q.getResultList();

        return result;
    }

    public Integer countListaConfigurazioniSoglieAttivi(String inizioPeriodoValidazione,
                                                        String finePeriodoValidazione) {

        EntityManager entityManager = provider.get();

        String query = "select count(cs.id) from ConfigurazioniSoglie cs " + "where cs.flagAttivo=1 " + "and ("
                + "		("
                + "			(:inizioPeriodoCompetenza is null or cs.finePeriodoCompetenza>=:inizioPeriodoCompetenza) "
                + "			or " + "			cs.finePeriodoCompetenza is null) " + "		and"
                + "		(:finePeriodoCompetenza is null or cs.inizioPeriodoCompetenza<=:finePeriodoCompetenza) " + ")";

        Query q = entityManager.createQuery(query);

        if (inizioPeriodoValidazione != null)
            q.setParameter("inizioPeriodoCompetenza",
                    DateUtils.stringToDate(inizioPeriodoValidazione, DateUtils.DATE_ITA_FORMAT_DASH));
        else
            q.setParameter("inizioPeriodoCompetenza", null);

        if (finePeriodoValidazione != null)
            q.setParameter("finePeriodoCompetenza",
                    DateUtils.stringToDate(finePeriodoValidazione, DateUtils.DATE_ITA_FORMAT_DASH));
        else
            q.setParameter("finePeriodoCompetenza", null);

        Long resultNumber = (Long) q.getResultList().get(0);

        return resultNumber.intValue();

    }

    public List<ConfigurazioniSoglie> getListaConfigurazioniSoglieAttivi(String inizioPeriodoValidazione,
                                                                         String finePeriodoValidazione, String order, Integer page) {

        EntityManager entityManager = provider.get();

        String query = "select cs from ConfigurazioniSoglie cs " + "where cs.flagAttivo=1 " + "and (" + "		("
                + "			(:inizioPeriodoCompetenza is null or cs.finePeriodoCompetenza>=:inizioPeriodoCompetenza) "
                + "			or " + "			cs.finePeriodoCompetenza is null) " + "		and"
                + "		(:finePeriodoCompetenza is null or cs.inizioPeriodoCompetenza<=:finePeriodoCompetenza) " + ")";
        if (order != null) {
            query = query + " order by cs." + order;
        }
        Query q = entityManager.createQuery(query);

        if (inizioPeriodoValidazione != null)
            q.setParameter("inizioPeriodoCompetenza",
                    DateUtils.stringToDate(inizioPeriodoValidazione, DateUtils.DATE_ITA_FORMAT_DASH));
        else
            q.setParameter("inizioPeriodoCompetenza", null);

        if (finePeriodoValidazione != null)
            q.setParameter("finePeriodoCompetenza",
                    DateUtils.stringToDate(finePeriodoValidazione, DateUtils.DATE_ITA_FORMAT_DASH));
        else
            q.setParameter("finePeriodoCompetenza", null);

        int startRecord;

        if (page == 1) {
            startRecord = (page - 1) * ReportPage.NUMBER_RECORD_PER_PAGE;
        } else {
            startRecord = ((page - 1) * ReportPage.NUMBER_RECORD_PER_PAGE) + 1;
        }

        int endRecord = ReportPage.NUMBER_RECORD_PER_PAGE;

        List<ConfigurazioniSoglie> result = (List<ConfigurazioniSoglie>) q.setFirstResult(startRecord)
                .setMaxResults(endRecord).getResultList();

        return result;

    }

    public List<ConfigurazioniSoglie> getStoricoConfigurazioneSoglie(Long codiceConfigurazione) {

        EntityManager entityManager = provider.get();

        String query = "select cs from ConfigurazioniSoglie cs " + "where cs.flagAttivo=0 "
                + "and cs.codiceConfigurazione=:codiceConfigurazione " + "order by cs.dataUltimaModifica desc";

        Query q = entityManager.createQuery(query);

        q.setParameter("codiceConfigurazione", codiceConfigurazione);

        List<ConfigurazioniSoglie> result = (List<ConfigurazioniSoglie>) q.getResultList();

        return result;
    }

    public Long getMaxCodiceRegolaOnConfigurazioneSoglie() {

        EntityManager entityManager = provider.get();

        String query = "select max(cs.codiceConfigurazione) from ConfigurazioniSoglie cs ";

        Query q = entityManager.createQuery(query);

        Long resultNumber = (Long) q.getResultList().get(0);

        return resultNumber;
    }

    public List<Long> getValidazioneDate(String inizioPeriodoCompetenza, String finePeriodoCompetenza) {
        EntityManager entityManager = provider.get();

        String query = "select distinct cs.codiceConfigurazione from ConfigurazioniSoglie cs " + "where (( "
                + "		(cs.inizioPeriodoCompetenza between :inizioPeriodoCompetenza and :finePeriodoCompetenza) "
                + "		or "
                + "		(cs.finePeriodoCompetenza between :inizioPeriodoCompetenza and :finePeriodoCompetenza) " + ") "
                + "or ( " + "		cs.inizioPeriodoCompetenza<=:inizioPeriodoCompetenza " + "		and "
                + "		cs.finePeriodoCompetenza>=:finePeriodoCompetenza " + ")) " + "and cs.flagAttivo='1' ";
        Query q = entityManager.createQuery(query);

        q.setParameter("inizioPeriodoCompetenza",
                DateUtils.stringToDate(inizioPeriodoCompetenza, DateUtils.DATE_ITA_FORMAT_DASH));
        q.setParameter("finePeriodoCompetenza",
                DateUtils.stringToDate(finePeriodoCompetenza, DateUtils.DATE_ITA_FORMAT_DASH));
        List<Long> result = (List<Long>) q.getResultList();

        return result;
    }

    public ConfigurazioniSoglie exportConfigurazioneSoglie(Long id) {

        EntityManager entityManager = provider.get();

        String query = "select cs from ConfigurazioniSoglie cs " + "where cs.id = :id";

        Query q = entityManager.createQuery(query);
        q.setParameter("id", id);
        if (q.getResultList().size() > 0) {
            return (ConfigurazioniSoglie) q.getResultList().get(0);
        } else {
            return null;
        }

        // return entityManager.find(ConfigurazioniSoglie.class, id);

    }

    public ConfigurazioniSoglie getConfigurazioneSoglie(Long id) {

        EntityManager entityManager = provider.get();

        String query = "select cs from ConfigurazioniSoglie cs " + "where cs.id = :id";

        Query q = entityManager.createQuery(query);
        q.setParameter("id", id);
        if (q.getResultList().size() > 0) {
            return (ConfigurazioniSoglie) q.getResultList().get(0);
        } else {
            return null;
        }
    }    

    @Override
    public List<CodificaDto> getListCodificaEsperto(String voceIncasso, String fonte, String titolo, String nominativo,
                                                    String username, Integer limitCodifichePerforming) {
        StringBuffer query = new StringBuffer();
        EntityManager entityManager = provider.get();

        endSessioneCodifica(username);

        List<Object[]> codifiche = null;
        if (!TextUtils.isEmpty(voceIncasso)) {
            query = new StringBuffer("SELECT " +
                    "    c.*, cb.TITOLO, cb.COMPOSITORI, cb.AUTORI, cb.INTERPRETI " +
                    " FROM " +
                    "    (SELECT " +
                    "        *" +
                    "    FROM " +
                    "        PERF_CODIFICA cod " +
                    "    WHERE " +
                    "        NOT EXISTS( SELECT " +
                    "                * " +
                    "            FROM " +
                    "                PERF_CODIFICA cod1 " +
                    "            WHERE " +
                    "                cod1.ID_CODIFICA <> cod.ID_CODIFICA " +
                    "                    AND cod1.ID_COMBANA = cod.ID_COMBANA " +
                    "                    AND cod1.DATA_CODIFICA > cod.DATA_CODIFICA)) c, " +
                    "    PERF_COMBANA cb " +
                    " WHERE " +
                    "    NOT EXISTS( SELECT " +
                    "            ua.ID_CODIFICA " +
                    "        FROM " +
                    "            PERF_SESSIONE_CODIFICA sc, " +
                    "            PERF_UTILIZZAZIONI_ASSEGNATE ua " +
                    "        WHERE" +
                    "            (sc.DATA_FINE_VALID > NOW() " +
                    "                OR sc.DATA_FINE_VALID IS NULL) " +
                    "                AND sc.ID_SESSIONE_CODIFICA = ua.ID_SESSIONE_CODIFICA " +
                    "                AND ua.ID_CODIFICA = c.ID_CODIFICA) " +
                    "        AND NOT EXISTS( SELECT " +
                    "            ID_CODIFICA " +
                    "        FROM" +
                    "            PERF_UTILIZZAZIONI_ASSEGNATE, " +
                    "            PERF_SESSIONE_CODIFICA " +
                    "        WHERE" +
                    "            PERF_UTILIZZAZIONI_ASSEGNATE.ID_SESSIONE_CODIFICA = PERF_SESSIONE_CODIFICA.ID_SESSIONE_CODIFICA " +
                    "                AND PERF_SESSIONE_CODIFICA.UTENTE = '" + username + "'" +
                    "                AND ( DATE(DATA_FINE_VALID) > DATE(NOW() - INTERVAL 7 DAY) ) " +
                    "                AND TIPO_AZIONE IS NOT NULL" +
                    "                AND PERF_UTILIZZAZIONI_ASSEGNATE.ID_CODIFICA = c.ID_CODIFICA) " +
                    "        AND cb.ID_COMBANA = c.ID_COMBANA " +
                    "        AND c.TIPO_APPROVAZIONE = 'M' " +
                    "        AND c.DATA_FINE_VALIDITA IS NULL " +
                    "        AND c.CODICE_OPERA_APPROVATO IS NULL " +
                    "        AND (" +
                    " Select distinct u.ID_COMBANA From PERF_UTILIZZAZIONE u " +
                    " INNER JOIN PERF_PROGRAMMA_MUSICALE p on p.ID_PROGRAMMA_MUSICALE = u.ID_PROGRAMMA_MUSICALE " +
                    " LEFT JOIN PERF_MOVIMENTO_CONTABILE m on m.ID_PROGRAMMA_MUSICALE = p.ID_PROGRAMMA_MUSICALE " +
                    " WHERE m.CODICE_VOCE_INCASSO = ?1   AND c.ID_COMBANA = u.ID_COMBANA " +
                    "     )   " +
                    " ORDER BY  c.VALORE_ECONOMICO DESC, c.N_UTILIZZAZIONI DESC " +
                    " LIMIT " + limitCodifichePerforming);

            codifiche = entityManager.createNativeQuery(query.toString()).setParameter(1, voceIncasso).getResultList();

        } else if (!TextUtils.isEmpty(titolo) || !TextUtils.isEmpty(nominativo)) {
            query = new StringBuffer("SELECT " +
                    "    c.*, cb.TITOLO, cb.COMPOSITORI, cb.AUTORI, cb.INTERPRETI" +
                    " FROM " +
                    "    (SELECT " +
                    "        *" +
                    "    FROM" +
                    "        PERF_CODIFICA cod" +
                    "    WHERE" +
                    "        NOT EXISTS( SELECT " +
                    "                *" +
                    "            FROM" +
                    "                PERF_CODIFICA cod1" +
                    "            WHERE" +
                    "                cod1.ID_CODIFICA <> cod.ID_CODIFICA" +
                    "                    AND cod1.ID_COMBANA = cod.ID_COMBANA" +
                    "                    AND cod1.DATA_CODIFICA > cod.DATA_CODIFICA)) c," +
                    "    PERF_COMBANA cb" +
                    " WHERE" +
                    "    NOT EXISTS( SELECT " +
                    "            ua.ID_CODIFICA" +
                    "        FROM" +
                    "            PERF_SESSIONE_CODIFICA sc," +
                    "            PERF_UTILIZZAZIONI_ASSEGNATE ua" +
                    "        WHERE" +
                    "            (sc.DATA_FINE_VALID > NOW()" +
                    "                OR sc.DATA_FINE_VALID IS NULL)" +
                    "                AND sc.ID_SESSIONE_CODIFICA = ua.ID_SESSIONE_CODIFICA" +
                    "                AND ua.ID_CODIFICA = c.ID_CODIFICA)" +
                    "        AND NOT EXISTS( SELECT " +
                    "            ID_CODIFICA" +
                    "        FROM" +
                    "            PERF_UTILIZZAZIONI_ASSEGNATE," +
                    "            PERF_SESSIONE_CODIFICA" +
                    "        WHERE" +
                    "            PERF_UTILIZZAZIONI_ASSEGNATE.ID_SESSIONE_CODIFICA = PERF_SESSIONE_CODIFICA.ID_SESSIONE_CODIFICA" +
                    "                AND PERF_SESSIONE_CODIFICA.UTENTE = '" + username + "'" +
                    "                AND ( DATE(DATA_FINE_VALID) > DATE(NOW() - INTERVAL 7 DAY) )" +
                    "                AND TIPO_AZIONE IS NOT NULL" +
                    "                AND PERF_UTILIZZAZIONI_ASSEGNATE.ID_CODIFICA = c.ID_CODIFICA)" +
                    "        AND cb.ID_COMBANA = c.ID_COMBANA" +
                    "        AND c.TIPO_APPROVAZIONE = 'M'" +
                    "        AND c.DATA_FINE_VALIDITA IS NULL" +
                    "        AND c.CODICE_OPERA_APPROVATO IS NULL ");

            if (!TextUtils.isEmpty(titolo)) {
                query.append(" and cb.TITOLO like ?1 ");
            }
            if (!TextUtils.isEmpty(nominativo)) {
                query.append(
                        " and (cb.COMPOSITORI like ?2 OR cb.AUTORI like ?2) ");
            }

            query.append("order by c.VALORE_ECONOMICO DESC, c.N_UTILIZZAZIONI desc limit " + limitCodifichePerforming + ";");
            Query q = entityManager.createNativeQuery(query.toString());
            if (!TextUtils.isEmpty(titolo)) {
                q.setParameter(1, "%" + titolo + "%");
            }
            if (!TextUtils.isEmpty(nominativo)) {
                q.setParameter(2, "%" + nominativo + "%");
            }
            codifiche = q.getResultList();
        } else {
            query = new StringBuffer("SELECT " +
                    "    c.*, cb.TITOLO, cb.COMPOSITORI, cb.AUTORI, cb.INTERPRETI " +
                    "FROM " +
                    "    (SELECT " +
                    "        * " +
                    "    FROM " +
                    "        PERF_CODIFICA cod " +
                    "    WHERE " +
                    "        NOT EXISTS( SELECT " +
                    "                * " +
                    "            FROM " +
                    "                PERF_CODIFICA cod1 " +
                    "            WHERE " +
                    "                cod1.ID_CODIFICA <> cod.ID_CODIFICA " +
                    "                    AND cod1.ID_COMBANA = cod.ID_COMBANA " +
                    "                    AND cod1.DATA_CODIFICA > cod.DATA_CODIFICA)) c," +
                    "    PERF_COMBANA cb " +
                    " WHERE " +
                    "    NOT EXISTS( SELECT " +
                    "            ua.ID_CODIFICA " +
                    "        FROM" +
                    "            PERF_SESSIONE_CODIFICA sc, " +
                    "            PERF_UTILIZZAZIONI_ASSEGNATE ua " +
                    "        WHERE" +
                    "            (sc.DATA_FINE_VALID > NOW() " +
                    "                OR sc.DATA_FINE_VALID IS NULL) " +
                    "                AND sc.ID_SESSIONE_CODIFICA = ua.ID_SESSIONE_CODIFICA " +
                    "                AND ua.ID_CODIFICA = c.ID_CODIFICA) " +
                    "        AND NOT EXISTS( SELECT " +
                    "            ID_CODIFICA " +
                    "        FROM" +
                    "            PERF_UTILIZZAZIONI_ASSEGNATE, " +
                    "            PERF_SESSIONE_CODIFICA " +
                    "        WHERE " +
                    "            PERF_UTILIZZAZIONI_ASSEGNATE.ID_SESSIONE_CODIFICA = PERF_SESSIONE_CODIFICA.ID_SESSIONE_CODIFICA " +
                    "                AND PERF_SESSIONE_CODIFICA.UTENTE = '" + username + "' " +
                    "                AND ( DATE(DATA_FINE_VALID) > DATE(NOW() - INTERVAL 7 DAY) ) " +
                    "                AND TIPO_AZIONE IS NOT NULL " +
                    "                AND PERF_UTILIZZAZIONI_ASSEGNATE.ID_CODIFICA = c.ID_CODIFICA) " +
                    "        AND cb.ID_COMBANA = c.ID_COMBANA " +
                    "        AND c.TIPO_APPROVAZIONE = 'M' " +
                    "        AND c.DATA_FINE_VALIDITA IS NULL " +
                    "        AND c.CODICE_OPERA_APPROVATO IS NULL" +
                    " ORDER BY c.VALORE_ECONOMICO DESC , c.N_UTILIZZAZIONI DESC" +
                    " LIMIT " + limitCodifichePerforming + "; ");
            Query q = entityManager.createNativeQuery(query.toString());
            codifiche = q.getResultList();
        }

        // AGGIORNO LA SESSIONE
        return refreshSessioneCodifica(username, codifiche);

    }

    public List<CodificaDto> refreshSessioneCodifica(String username, List<Object[]> codifiche) {
        EntityManager entityManager = provider.get();
        List<CodificaDto> cList = new ArrayList<>();
        Calendar calendar = new GregorianCalendar();
        calendar.add(Calendar.MINUTE, 15);
        entityManager.getTransaction().begin();

        SessioneCodifica sessioneCodifica = startSessioneCodifica(username, entityManager, calendar);

        cList.addAll(insertIntoUtilizzazione(codifiche, entityManager, sessioneCodifica));

        entityManager.getTransaction().commit();

        return cList;
    }

    private List<CodificaDto> insertIntoUtilizzazione(List<Object[]> codifiche, EntityManager entityManager, SessioneCodifica sessioneCodifica) {
        List<CodificaDto> cList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(codifiche)) {
            StringBuilder stringBuffer = new StringBuilder("INSERT IGNORE INTO PERF_UTILIZZAZIONI_ASSEGNATE ")
                    .append("(ID_CODIFICA,ID_SESSIONE_CODIFICA) VALUES ")
                    .append("(?1,?2)");
            String query2 = stringBuffer.toString();
            for (Object[] row : codifiche) {
                CodificaDto codificaDto = new CodificaDto(row);
                cList.add(codificaDto);
                entityManager.createNativeQuery(query2)
                        .setParameter(1, codificaDto.getIdCodifica())
                        .setParameter(2, sessioneCodifica.getIdSessione())
                        .executeUpdate();
            }
//            String query2 = stringBuffer.toString();
//            query2 = query2.substring(0, query2.length() - 1);

        }
        return cList;
    }

    public void refreshSessioneCodifica(String username, Long idCodifica) {
        EntityManager entityManager = provider.get();
        SessioneCodifica sessioneCodifica;
        Calendar calendar = new GregorianCalendar();
        calendar.add(Calendar.MINUTE, 15);
        entityManager.getTransaction().begin();

        sessioneCodifica = startSessioneCodifica(username, entityManager, calendar);

        String query = "INSERT IGNORE INTO PERF_UTILIZZAZIONI_ASSEGNATE (ID_CODIFICA, ID_SESSIONE_CODIFICA) " +
                "VALUES (?1, ?2)";

        entityManager.createNativeQuery(query)
                .setParameter(1, idCodifica)
                .setParameter(2, sessioneCodifica.getIdSessione())
                .executeUpdate();

        entityManager.getTransaction().commit();
    }

    private SessioneCodifica startSessioneCodifica(String username, EntityManager entityManager, Calendar calendar) {
        SessioneCodifica sessioneCodifica;
        try {
            sessioneCodifica = (SessioneCodifica) entityManager.createQuery(
                    "Select x From SessioneCodifica x where x.dataFineValidita>=:dataFineValidita and x.utente =:utente")
                    .setParameter("dataFineValidita", new Date()).setParameter("utente", username).getResultList()
                    .get(0);
            sessioneCodifica.setDataInizioValidita(new Date());
            sessioneCodifica.setDataFineValidita(calendar.getTime());
            sessioneCodifica.setUtente(username);
            entityManager.merge(sessioneCodifica);
            entityManager.flush();
        } catch (Exception e) {
            sessioneCodifica = catchSessioneCodificaException(username, entityManager, calendar);
        }
        return sessioneCodifica;
    }

    private SessioneCodifica catchSessioneCodificaException(String username, EntityManager entityManager, Calendar calendar) {
        SessioneCodifica sessioneCodifica;
        sessioneCodifica = new SessioneCodifica();
        sessioneCodifica.setDataInizioValidita(new Date());
        sessioneCodifica.setDataFineValidita(calendar.getTime());
        sessioneCodifica.setUtente(username);
        entityManager.persist(sessioneCodifica);
        entityManager.flush();
        return sessioneCodifica;
    }

    public void endSessioneCodifica(String username) {
        EntityManager em = provider.get();

        try {
            SessioneCodifica sessioneCodifica = (SessioneCodifica) em.createQuery(
                    "Select x From SessioneCodifica x where x.dataFineValidita>=:dataFineValidita and x.utente =:utente")
                    .setParameter("dataFineValidita", new Date()).setParameter("utente", username).getResultList()
                    .get(0);

            em.getTransaction().begin();
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(sessioneCodifica.getDataInizioValidita());
            sessioneCodifica.setDataFineValidita(calendar.getTime());
            sessioneCodifica.setUtente(username);
            em.merge(sessioneCodifica);
            em.flush();
            em.getTransaction().commit();
        } catch (Exception e) {
            //XXX: non esistono sessioni attive;
        }
    }

    @Override
    public List<CodificaDto> getListCodificaBase(String username, Integer limitCodifichePerforming) {
        EntityManager entityManager = provider.get();
        endSessioneCodifica(username);
        StringBuffer query = new StringBuffer("SELECT " +
                "    c.*, cb.TITOLO, cb.COMPOSITORI, cb.AUTORI, cb.INTERPRETI" +
                " FROM" +
                "    (SELECT " +
                "        *" +
                "    FROM " +
                "        PERF_CODIFICA cod" +
                "    WHERE" +
                "        NOT EXISTS( SELECT " +
                "                *" +
                "            FROM" +
                "                PERF_CODIFICA cod1" +
                "            WHERE" +
                "                cod1.ID_CODIFICA <> cod.ID_CODIFICA" +
                "                    AND cod1.ID_COMBANA = cod.ID_COMBANA" +
                "                    AND cod1.DATA_CODIFICA > cod.DATA_CODIFICA)) c," +
                "    PERF_COMBANA cb" +
                " WHERE " +
                "    NOT EXISTS( SELECT " +
                "            ua.ID_CODIFICA" +
                "        FROM" +
                "            PERF_SESSIONE_CODIFICA sc," +
                "            PERF_UTILIZZAZIONI_ASSEGNATE ua" +
                "        WHERE" +
                "            (sc.DATA_FINE_VALID > NOW()" +
                "                OR sc.DATA_FINE_VALID IS NULL)" +
                "                AND sc.ID_SESSIONE_CODIFICA = ua.ID_SESSIONE_CODIFICA" +
                "                AND ua.ID_CODIFICA = c.ID_CODIFICA)" +
                "        AND NOT EXISTS( SELECT " +
                "            ID_CODIFICA" +
                "        FROM" +
                "            PERF_UTILIZZAZIONI_ASSEGNATE," +
                "            PERF_SESSIONE_CODIFICA" +
                "        WHERE" +
                "            PERF_UTILIZZAZIONI_ASSEGNATE.ID_SESSIONE_CODIFICA = PERF_SESSIONE_CODIFICA.ID_SESSIONE_CODIFICA" +
                "                AND PERF_SESSIONE_CODIFICA.UTENTE = '" + username + "'" +
                "                AND ( DATE(DATA_FINE_VALID) > DATE(NOW() - INTERVAL 7 DAY) )" +
                "                AND TIPO_AZIONE IS NOT NULL" +
                "                AND PERF_UTILIZZAZIONI_ASSEGNATE.ID_CODIFICA = c.ID_CODIFICA)" +
                "        AND cb.ID_COMBANA = c.ID_COMBANA" +
                "        AND c.TIPO_APPROVAZIONE = 'M'" +
                "        AND c.DATA_FINE_VALIDITA IS NULL" +
                "        AND c.CODICE_OPERA_APPROVATO IS NULL" +
                " ORDER BY c.VALORE_ECONOMICO , c.N_UTILIZZAZIONI DESC" +
                " LIMIT " + limitCodifichePerforming);


        List<CodificaDto> cList = new ArrayList<>();
        List<Object[]> codifiche = entityManager.createNativeQuery(query.toString()).getResultList();

        // AGGIORNO LA SESSIONE
        SessioneCodifica sessioneCodifica = null;
        Calendar calendar = new GregorianCalendar();
        calendar.add(Calendar.MINUTE, 15);

        entityManager.getTransaction().begin();
        try {
            sessioneCodifica = (SessioneCodifica) entityManager.createQuery(
                    "Select x From SessioneCodifica x where x.dataFineValidita>=:dataFineValidita and x.utente =:username")
                    .setParameter("dataFineValidita", new Date()).setParameter("username", username).getResultList()
                    .get(0);
            sessioneCodifica.setDataInizioValidita(new Date());
            sessioneCodifica.setDataFineValidita(calendar.getTime());
            sessioneCodifica.setUtente(username);
            entityManager.merge(sessioneCodifica);
            entityManager.flush();
        } catch (Exception e) {
            sessioneCodifica = catchSessioneCodificaException(username, entityManager, calendar);
        }
        try {
            cList.addAll(insertIntoUtilizzazione(codifiche, entityManager, sessioneCodifica));
            entityManager.getTransaction().commit();
            return cList;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            return null;
        }
    }

    @Override
    public void approvaOpera(CodificaOpereRequestDTO codificaOpereRequestDTO) {
        EntityManager entityManager = provider.get();
        entityManager.getTransaction().begin();
        SessioneCodifica sessioneCodifica = null;
        Calendar calendar = new GregorianCalendar();
        calendar.add(Calendar.MINUTE, 15);

        List<SessioneCodifica> sessioneCodificaList = entityManager.createQuery(
                "Select x From SessioneCodifica x where x.dataFineValidita>=:dataFineValidita and x.utente =:utente")
                .setParameter("dataFineValidita", new Date())
                .setParameter("utente", codificaOpereRequestDTO.getUsername()).getResultList();


        if (CollectionUtils.isNotEmpty(sessioneCodificaList)) {
            sessioneCodifica = (SessioneCodifica) entityManager.createQuery(
                    "Select x From SessioneCodifica x where x.dataFineValidita>=:dataFineValidita and x.utente =:utente")
                    .setParameter("dataFineValidita", new Date())
                    .setParameter("utente", codificaOpereRequestDTO.getUsername()).getResultList().get(0);
            sessioneCodifica.setDataInizioValidita(new Date());
            sessioneCodifica.setDataFineValidita(calendar.getTime());
            sessioneCodifica.setUtente(codificaOpereRequestDTO.getUsername());
            entityManager.merge(sessioneCodifica);
        }

        for (CodificaOpereDTO codificaOpereDTO : codificaOpereRequestDTO.getCodificaOpereDTO()) {
            if ((codificaOpereDTO.getStatApprov() != null && codificaOpereDTO.getStatApprov().equals("A") || (codificaOpereDTO.getStatApprov().equals("P"))
                    || (codificaOpereDTO.getStatApprov().equals("M")
                    || (codificaOpereDTO.getStatApprov().equals("R"))))) {
                String query = "select c from Codifica c where c.idCodifica = :id";
                Query q = entityManager.createQuery(query);
                q.setParameter("id", codificaOpereDTO.getIdCodifica());
                Codifica codifica = (Codifica) q.getSingleResult();

                codifica.setDataApprovazione(new Date());
                codifica.setStatoApprovazione(codificaOpereDTO.getStatApprov());
                codifica.setCodiceOperaApprovato(codificaOpereDTO.getCodApprov());
                codifica.setPreziosa(codificaOpereDTO.getStatApprov().equals("M"));

                String query2 = "update Utilizzazione u set u.idCodifica = :idCodifica, u.codiceOpera = :codiceOpera "
                        + "where u.idCombana = :idCombana " + "and u.idCodifica is null "
                        + "and u.codiceOpera is null";
                Query q1 = entityManager.createQuery(query2);

                q1.setParameter("idCombana", codifica.getIdCombana());
                q1.setParameter("idCodifica", codifica.getIdCodifica());
                q1.setParameter("codiceOpera", codifica.getCodiceOperaApprovato());
                q1.executeUpdate();
                entityManager.merge(codifica);

                updateSessioneCodifica(entityManager, sessioneCodifica, codificaOpereDTO);

            } else if (codificaOpereDTO.getStatApprov().equals("S")) {
                updateSessioneCodifica(entityManager, sessioneCodifica, codificaOpereDTO);
            }
        }
        entityManager.flush();
        entityManager.getTransaction().commit();
    }

    private void updateSessioneCodifica(EntityManager entityManager, SessioneCodifica sessioneCodifica, CodificaOpereDTO codificaOpereDTO) {
        if (sessioneCodifica != null) {
            String query = "select ua from UtilizzazioniAssegnate ua where ua.idCodifica = :id and ua.idSessioneCodifica = :idSessioneCodifica";
            Query q = entityManager.createQuery(query);
            q.setParameter("id", codificaOpereDTO.getIdCodifica());
            q.setParameter("idSessioneCodifica", sessioneCodifica.getIdSessione());
            List<UtilizzazioniAssegnate> utilizzazioniAssegnateList = q.getResultList();
            for (UtilizzazioniAssegnate utilizzazioniAssegnate : utilizzazioniAssegnateList) {
                utilizzazioniAssegnate.setTipoCodifica(codificaOpereDTO.getStatApprov());
                entityManager.merge(utilizzazioniAssegnate);
            }
        }
    }

    @Override
    public TimerSessionResponse getSessionTimer(String username) {
        EntityManager entityManager = provider.get();
        // AGGIORNO LA SESSIONE
        TimerSessionResponse response = new TimerSessionResponse();
        try {
            SessioneCodifica sessioneCodifica = (SessioneCodifica) entityManager.createQuery(
                    "Select x From SessioneCodifica x where x.dataFineValidita>=:dataFineValidita and x.utente =:username")
                    .setParameter("dataFineValidita", new Date()).setParameter("username", username).getResultList()
                    .get(0);
            response.setSessionTimeResidual(sessioneCodifica.getDataFineValidita().getTime() - new Date().getTime());
        } catch (Exception e) {
            Calendar calendar = new GregorianCalendar();
            calendar.add(Calendar.MINUTE, 15);
            response.setSessionTimeResidual(calendar.getTime().getTime() - new Date().getTime());
        }
        return response;
    }

    @Override
    public TimerSessionResponse refreshSession(String username) {
        EntityManager entityManager = provider.get();
        // AGGIORNO LA SESSIONE
        entityManager.getTransaction().begin();
        Calendar calendar = new GregorianCalendar();
        calendar.add(Calendar.MINUTE, 15);
        TimerSessionResponse response = new TimerSessionResponse();
        try {
            SessioneCodifica sessioneCodifica = (SessioneCodifica) entityManager.createQuery(
                    "Select x From SessioneCodifica x where x.dataFineValidita>=:dataFineValidita and x.utente =:username")
                    .setParameter("dataFineValidita", new Date()).setParameter("username", username).getResultList()
                    .get(0);
            sessioneCodifica.setDataFineValidita(calendar.getTime());
            entityManager.persist(sessioneCodifica);
            entityManager.flush();
        } catch (Exception e) {
            SessioneCodifica sessioneCodifica = new SessioneCodifica();
            sessioneCodifica.setDataInizioValidita(new Date());
            sessioneCodifica.setDataFineValidita(calendar.getTime());
            sessioneCodifica.setUtente(username);
            entityManager.persist(sessioneCodifica);
            entityManager.flush();
        }
        response.setSessionTimeResidual(calendar.getTime().getTime() - new Date().getTime());
        entityManager.getTransaction().commit();
        return response;
    }

    @Override
    public List<RiepilogoCodificaManualeResponse> getRiepilogoCodificaManualeResponseList(Long idPeriodoRipartizione, int page, String order) {
        EntityManager entityManager = provider.get();
        String sb = ""
                + "select new  com.alkemytech.sophia.codman.dto.performing.RiepilogoCodificaManualeResponse(" +
                "sc.utente, Count(u.idUtilizzazione), "
                + "Sum(( u.puntiRicalcolati / mc.puntiProgrammaMusicale * mc.importoValorizzato ))) "
                + "from PerfSessioneCodifica sc, PerfUtilizzazioniAssegnate ua, "
                + "PerfUtilizzazione u, PerfCodifica co, PerfMovimentoContabile mc left join "
                + "PerfEventiPagati ep on ep.idEvento = mc.idEvento AND ep.voceIncasso = mc.codiceVoceIncasso "
                + "WHERE sc.idSessioneCodifica = ua.perfSessioneCodifica.idSessioneCodifica AND "
                + "co.idCodifica = ua.perfCodifica.idCodifica AND u.perfCombana.idCombana = co.perfCombana.idCombana "
                + "AND mc.idProgrammaMusicale = u.idProgrammaMusicale AND ua.tipoAzione IN ( 'P' "
                + ", 'A', 'M' ) ";
        if (idPeriodoRipartizione == null)
            sb += "AND ep.idPeriodoRipartizione is null ";
        else
            sb += "AND ep.idPeriodoRipartizione = :idPeriodoRipartizione ";
        sb += "group by sc.utente, ep.idPeriodoRipartizione";
        TypedQuery<RiepilogoCodificaManualeResponse> q = entityManager.createQuery(sb, RiepilogoCodificaManualeResponse.class);

        if (idPeriodoRipartizione != null)
            return q.setParameter("idPeriodoRipartizione", idPeriodoRipartizione).getResultList();
        return q.getResultList();
    }

    @Override
    public MonitoraggioCodificatoResponse getMonitoraggioCodificato(Long idPeriodoRipartizione, int page, String order) {
        EntityManager entityManager = provider.get();
        String query;

        if(idPeriodoRipartizione == null)
            query = "select new com.alkemytech.sophia.codman.dto.performing.MonitoraggioCodificatoResponse(" +
                    " mc.valori) from PerfMonitoraggioCodificato mc  " +
                    " where (mc.perfMonitoraggioCodificatoConf.dataFineValidita is null or " +
                    "  mc.perfMonitoraggioCodificatoConf.dataFineValidita > current_timestamp )" +
                    "  and mc.idValorizzazioneRun is null " +
                    "  order by mc.idMonitoraggioCodificato  desc " ;
        else
            query = "select new com.alkemytech.sophia.codman.dto.performing.MonitoraggioCodificatoResponse(" +
                " mc.valori) from PerfMonitoraggioCodificato mc " +
                " where (mc.perfMonitoraggioCodificatoConf.dataFineValidita is null or " +
                "  mc.perfMonitoraggioCodificatoConf.dataFineValidita > current_timestamp )" +
                "  and mc.perfValorizzazioneRun.perfPeriodoRipartizione.dataApprovazioneCarichiRiparto is null " +
                "and mc.perfValorizzazioneRun.perfPeriodoRipartizione.repertorio = 'MUSICA' " +
                "and mc.perfValorizzazioneRun.perfPeriodoRipartizione.ambito = 'PERFORMING'" +
                "and mc.perfValorizzazioneRun.idValorizzazioneRun = (select max(vr.idValorizzazioneRun) " +
                "from PerfValorizzazioneRun vr where vr.infoRun like '%\"result\": \"OK\"%' " +
                    "and vr.perfPeriodoRipartizione.idPeriodoRipartizione = :idPeriodoRipartizione ) order by mc.perfValorizzazioneRun.perfPeriodoRipartizione.competenzeDa desc";


        try {

            if (idPeriodoRipartizione != null) {
                MonitoraggioCodificatoResponse response = entityManager.createQuery(query, MonitoraggioCodificatoResponse.class)
                        .setParameter("idPeriodoRipartizione", idPeriodoRipartizione)
                        .getSingleResult();
                return response;
            }
            MonitoraggioCodificatoResponse response = entityManager.createQuery(query, MonitoraggioCodificatoResponse.class)
                    .setMaxResults(1)
                    .getSingleResult();
            return response;
        } catch (NoResultException e) {
            logger.error(e.getMessage(), e);
            return null;
        }

        /*String sb = "select new com.alkemytech.sophia.codman.dto.performing.LivelloSomiglianza(mc.idLivelloSomiglianza, mc.inferiore, mc.superiore) " +
                "from PerfLivelloSomiglianza mc order by mc.inferiore desc, mc.superiore desc";
        TypedQuery<LivelloSomiglianza> q = entityManager.createQuery(sb, LivelloSomiglianza.class);

        List<LivelloSomiglianza> livelloSomiglianzaList = q.getResultList();

        sb = "select new com.alkemytech.sophia.codman.dto.performing.ValoreEconomico(mc.idValoreEconomico, mc.inferiore, mc.superiore)" +
                "from PerfValoreEconomico mc order by mc.inferiore, mc.superiore";
        TypedQuery<ValoreEconomico> q2 = entityManager.createQuery(sb, ValoreEconomico.class);

        List<ValoreEconomico> valoreEconomicoList = q2.getResultList();

        sb = "select new com.alkemytech.sophia.codman.dto.performing.MonitoraggioCodifcatoTuple(mc1,mc2) " +
                "from PerfMonitoraggioCodificato mc1,PerfMonitoraggioCodificato mc2 " +
                "where mc1.perfLivelloSomiglianza=mc2.perfLivelloSomiglianza " +
                "and mc1.perfValoreEconomico=mc2.perfValoreEconomico " +
                "and mc1.perfValorizzazioneRun=mc2.perfValorizzazioneRun " +
                "and mc1.tipoSoglia=false " +
                "and mc2.tipoSoglia=true " +
                "and mc1.perfValorizzazioneRun.idValorizzazioneRun = " +
                "(select max(vr.idValorizzazioneRun) from PerfValorizzazioneRun vr)  " +
                "order by mc1.perfValorizzazioneRun.idValorizzazioneRun desc, " +
                "mc1.perfValoreEconomico.idValoreEconomico,mc1.perfLivelloSomiglianza.idLivelloSomiglianza";

        TypedQuery<MonitoraggioCodifcatoTuple> q3 = entityManager.createQuery(sb, MonitoraggioCodifcatoTuple.class);

        MonitoraggioCodificatoResponse monitoraggioCodificatoResponse = new MonitoraggioCodificatoResponse();
        monitoraggioCodificatoResponse.setValoreEconomicoList(valoreEconomicoList);
        monitoraggioCodificatoResponse.setLivelloSomiglianzaList(livelloSomiglianzaList);

        for(LivelloSomiglianza livelloSomiglianza :livelloSomiglianzaList){
            monitoraggioCodificatoResponse.getMonitoraggioCodifiche().put(livelloSomiglianza.getId(), new HashMap<Integer, List<NumeroCombane>>());

            for(ValoreEconomico valoreEconomico :valoreEconomicoList){
                if(monitoraggioCodificatoResponse.getMonitoraggioCodifiche().get(livelloSomiglianza.getId()).get(valoreEconomico.getId())==null){
                    monitoraggioCodificatoResponse.getMonitoraggioCodifiche().get(livelloSomiglianza.getId()).put(valoreEconomico.getId(), new ArrayList<NumeroCombane>());
                }
                for(MonitoraggioCodifcatoTuple monitoraggioCodifcatoTuple :q3.getResultList()){
                    if(monitoraggioCodificatoResponse.getValoreSoglia() == null) {
                        monitoraggioCodificatoResponse.setValoreSoglia(monitoraggioCodifcatoTuple.getMonitoraggioCodificatoInferiore().getValoreSoglia());
                    }
                    if(monitoraggioCodifcatoTuple.getMonitoraggioCodificatoInferiore().getPerfValoreEconomico().getIdValoreEconomico().equals(valoreEconomico.getId())
                    && monitoraggioCodifcatoTuple.getMonitoraggioCodificatoInferiore().getPerfLivelloSomiglianza().getIdLivelloSomiglianza().equals(livelloSomiglianza.getId())){
                        monitoraggioCodificatoResponse.getMonitoraggioCodifiche().get(livelloSomiglianza.getId()).get(valoreEconomico.getId()).add(
                                new NumeroCombane(monitoraggioCodifcatoTuple.getMonitoraggioCodificatoSuperiore().getNumeroCombane(), monitoraggioCodifcatoTuple.getMonitoraggioCodificatoInferiore().getNumeroCombane())
                        );
                    }
                }
            }
        }*/
    }

    /*public static void main(String []args) throws IOException {
        int entityCount = 50;
        int batchSize = 25;

        EntityManagerFactory entityManagerFactory;

        Properties properties = new Properties();
//        final JpaPersistModule jpaPersistModule = new JpaPersistModule("mcmdb");
        properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("jpa.mcmdb.properties"));
        entityManagerFactory = Persistence.createEntityManagerFactory("mcmdb", properties);
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction entityTransaction = entityManager.getTransaction();

        String query = "select ls from PerfLivelloSomiglianza ls";

        List<PerfLivelloSomiglianza> livelloSomiglianzas = ((TypedQuery<PerfLivelloSomiglianza>) entityManager.createQuery(query)).getResultList();

        query = "select ve from PerfValoreEconomico ve";

        List<PerfValoreEconomico> valoreEconomicos = ((TypedQuery<PerfValoreEconomico>) entityManager.createQuery(query)).getResultList();

        query = "select vr from PerfValorizzazioneRun vr";

        List<PerfValorizzazioneRun> valorizzazioneRuns = ((TypedQuery<PerfValorizzazioneRun>) entityManager.createQuery(query)).getResultList();

        try {
//            fillUpMonitoraggioCodificato(batchSize, entityManager, entityTransaction, livelloSomiglianzas, valoreEconomicos, valorizzazioneRuns);
        } catch (RuntimeException e) {
            if (entityTransaction.isActive()) {
                entityTransaction.rollback();
            }
            throw e;
        } finally {
            entityManager.close();
        }
    }*/

//    private static void fillUpMonitoraggioCodificato(int batchSize, EntityManager entityManager, EntityTransaction entityTransaction, List<PerfLivelloSomiglianza> livelloSomiglianzas, List<PerfValoreEconomico> valoreEconomicos, List<PerfValorizzazioneRun> valorizzazioneRuns) {
//        entityTransaction.begin();
//        int i = 0;
//        for(PerfValorizzazioneRun valorizzazioneRun: valorizzazioneRuns) {
//            for (PerfLivelloSomiglianza livelloSomiglianza : livelloSomiglianzas) {
//
//                for (PerfValoreEconomico valoreEconomico : valoreEconomicos) {
//                    if (i > 0 && i % batchSize == 0) {
//                        entityTransaction.commit();
//                        entityTransaction.begin();
//
//                        entityManager.clear();
//                    }
//                    PerfMonitoraggioCodificato perfMonitoraggioCodificato = new PerfMonitoraggioCodificato();
//                    perfMonitoraggioCodificato.setPerfLivelloSomiglianza(livelloSomiglianza);
//                    perfMonitoraggioCodificato.setPerfValoreEconomico(valoreEconomico);
//                    perfMonitoraggioCodificato.setTipoSoglia(false);
//                    perfMonitoraggioCodificato.setPerfValorizzazioneRun(valorizzazioneRun);
//                    perfMonitoraggioCodificato.setNumeroCombane((int) (Math.random() * 100));
//                    perfMonitoraggioCodificato.setValoreEconomico(new BigDecimal(Math.random() * 1000));
//                    perfMonitoraggioCodificato.setValoreSoglia((int) (Math.random() * 100));
//                    entityManager.persist(perfMonitoraggioCodificato);
//
//                    perfMonitoraggioCodificato = new PerfMonitoraggioCodificato();
//                    perfMonitoraggioCodificato.setPerfLivelloSomiglianza(livelloSomiglianza);
//                    perfMonitoraggioCodificato.setPerfValoreEconomico(valoreEconomico);
//                    perfMonitoraggioCodificato.setTipoSoglia(true);
//                    perfMonitoraggioCodificato.setPerfValorizzazioneRun(valorizzazioneRun);
//                    perfMonitoraggioCodificato.setNumeroCombane((int) (Math.random() * 100));
//                    perfMonitoraggioCodificato.setValoreEconomico(new BigDecimal(Math.random() * 1000));
//                    perfMonitoraggioCodificato.setValoreSoglia((int) (Math.random() * 100));
//                    entityManager.persist(perfMonitoraggioCodificato);
//                    i++;
//
//                }
//
//            }
//        }
//
//        entityTransaction.commit();
//    }
}

