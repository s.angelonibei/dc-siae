package com.alkemytech.sophia.codman.rest.performing.service;

import com.alkemytech.sophia.codman.dto.performing.*;
import com.alkemytech.sophia.codman.entity.performing.ConfigurazioniSoglie;
import com.alkemytech.sophia.codman.entity.performing.PerfCodificaMassiva;
import com.alkemytech.sophia.codman.rest.performing.TimerSessionResponse;
import com.alkemytech.sophia.common.s3.S3Service;

import java.io.InputStream;
import java.util.List;

public interface ICodificaService {
    List<String> addConfigurazioneSoglie(ConfigurazioneSoglieDTO confSoglia, InputStream file, S3Service s3Service, String pathFileS3);

    List<String> updateConfigurazioneSoglie(ConfigurazioneSoglieDTO confSoglia, String user, InputStream file, S3Service s3Service, String pathFileS3);

    List<String> getValidazioneDate(String inizioPeriodoValidazione, String finePeriodoValidazione);

    List<String> getValidazioneDateOnUpdate(String inizioPeriodoValidazione, String finePeriodoValidazione, Long codiceRegola);

    List<ConfigurazioniSoglie> getListaConfigurazioniSoglieAttivi(String inizioPeriodoValidazione, String finePeriodoValidazione, String order, Integer page);

    Integer countListaConfigurazioniSoglieAttivi(String inizioPeriodoValidazione, String finePeriodoValidazione);

    List<ConfigurazioniSoglie> getStoricoConfigurazioneSoglie(Long codiceConfigurazione);

    ConfigurazioniSoglie getConfigurazioneSoglie(Long id);

    CodificaListDTO getListCodificaEsperto(String voceIncasso, String Fonte, String titolo, String nominativo, String username, Integer limitCodifichePerforming);
     
    CodificaListDTO getListCodificaBase(String username, Integer limitCodifichePerforming);

    TimerSessionResponse getSessionTimer(String username);

    void getApprovaOpera(CodificaOpereRequestDTO codificaOpereRequestDTO);

    TimerSessionResponse refreshSession(String username);

    List<RiepilogoCodificaManualeResponse> getRiepilogoCodificaManualeResponseList(Long idPeriodoRipertizione,
                                                                                        int page,
                                                                                        String order);

    MonitoraggioCodificatoResponse getMonitoraggioCodificato(Long idPeriodoRipartizione, int page, String order);

    PerfCodificaMassiva saveCodificaMassiva(PerfCodificaMassiva perfCodificaMassiva);

	public String uploadFile(String fileName, InputStream is, S3Service s3Service, String pathFileS3);

	public Integer getUploadedFilesCount();

	public List<PerfCodificaMassiva> getUploadedFiles(String order, Integer page);
}
