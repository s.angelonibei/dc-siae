package com.alkemytech.sophia.codman.entity.performing;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "PERF_AGGI_MC")
public class AggiMc  implements Serializable{

	private Long id;
	private Date inizioPeriodoValidazione;
	private Date finePeriodoValidazione;
	private BigDecimal soglia;
	private BigDecimal aggioSottosoglia;
	private BigDecimal aggioSoprasoglia;
	private BigDecimal valoreCap;
	private boolean flagCapMultiplo;
	private Date dataCreazione = Calendar.getInstance().getTime();
	private String utenteCreazione;
	private String codiceRegola;
	private String tipoUltimaModifica;
	private String utenteUltimaModifica;
	private Date dataUltimaModifica;
	private boolean flagAttivo;
	
	
	@Id
    @Column(name = "ID_AGGIO_MC", unique = true, nullable = false)
	@SequenceGenerator(name="PERF_AGGI_MC",sequenceName = "PERF_AGGI_MC_SEQ",allocationSize=1)
	@GeneratedValue(strategy= GenerationType.SEQUENCE, generator="PERF_AGGI_MC")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
    @Column(name = "INIZIO_PERIODO_VALIDAZIONE")
	public Date getInizioPeriodoValidazione() {
		return inizioPeriodoValidazione;
	}
	public void setInizioPeriodoValidazione(Date inizioPeriodoValidazione) {
		this.inizioPeriodoValidazione = inizioPeriodoValidazione;
	}

    @Column(name = "FINE_PERIODO_VALIDAZIONE")
	public Date getFinePeriodoValidazione() {
		return finePeriodoValidazione;
	}
	public void setFinePeriodoValidazione(Date finePeriodoValidazione) {
		this.finePeriodoValidazione = finePeriodoValidazione;
	}

    @Column(name = "SOGLIA")
	public BigDecimal getSoglia() {
		return soglia;
	}
	public void setSoglia(BigDecimal soglia) {
		this.soglia = soglia;
	}

    @Column(name = "AGGIO_SOTTOSOGLIA")
	public BigDecimal getAggioSottosoglia() {
		return aggioSottosoglia;
	}
	public void setAggioSottosoglia(BigDecimal aggioSottosoglia) {
		this.aggioSottosoglia = aggioSottosoglia;
	}
    @Column(name = "AGGIO_SOPRASOGLIA")
	public BigDecimal getAggioSoprasoglia() {
		return aggioSoprasoglia;
	}
	public void setAggioSoprasoglia(BigDecimal aggioSoprasoglia) {
		this.aggioSoprasoglia = aggioSoprasoglia;
	}
    @Column(name = "VALORE_CAP")
	public BigDecimal getValoreCap() {
		return valoreCap;
	}
	public void setValoreCap(BigDecimal valoreCap) {
		this.valoreCap = valoreCap;
	}
    @Column(name = "FLAG_CAP_MULTIPLO")
	public boolean getFlagCapMultiplo() {
		return flagCapMultiplo;
	}
	public void setFlagCapMultiplo(boolean flagCapMultiplo) {
		this.flagCapMultiplo = flagCapMultiplo;
	}
    @Column(name = "DATA_CREAZIONE")
	public Date getDataCreazione() {
		return dataCreazione;
	}
	public void setDataCreazione(Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}
    @Column(name = "UTENTE_CREAZIONE")
	public String getUtenteCreazione() {
		return utenteCreazione;
	}
	public void setUtenteCreazione(String utenteCreazione) {
		this.utenteCreazione = utenteCreazione;
	}
    @Column(name = "CODICE_REGOLA")
	public String getCodiceRegola() {
		return codiceRegola;
	}
	public void setCodiceRegola(String codiceRegola) {
		this.codiceRegola = codiceRegola;
	}
    @Column(name = "TIPO_ULTIMA_MODIFICA")
	public String getTipoUltimaModifica() {
		return tipoUltimaModifica;
	}
	public void setTipoUltimaModifica(String tipoUltimaModifica) {
		this.tipoUltimaModifica = tipoUltimaModifica;
	}
    @Column(name = "UTENTE_ULTIMA_MODIFICA")
	public String getUtenteUltimaModifica() {
		return utenteUltimaModifica;
	}
	public void setUtenteUltimaModifica(String utenteUltimaModifica) {
		this.utenteUltimaModifica = utenteUltimaModifica;
	}
    @Column(name = "DATA_ULTIMA_MODIFICA")
	public Date getDataUltimaModifica() {
		return dataUltimaModifica;
	}
	public void setDataUltimaModifica(Date dataUltimaModifica) {
		this.dataUltimaModifica = dataUltimaModifica;
	}
    @Column(name = "FLAG_ATTIVO")
	public boolean getFlagAttivo() {
		return flagAttivo;
	}
	public void setFlagAttivo(boolean flagAttivo) {
		this.flagAttivo = flagAttivo;
	}

	
}
