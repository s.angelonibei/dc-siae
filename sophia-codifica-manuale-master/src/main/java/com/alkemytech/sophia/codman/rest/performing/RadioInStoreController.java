package com.alkemytech.sophia.codman.rest.performing;

import com.alkemytech.sophia.broadcasting.consts.Consts;
import com.alkemytech.sophia.broadcasting.dto.FileArmonizzatoRequestDTO;
import com.alkemytech.sophia.broadcasting.dto.IndicatoreDTO;
import com.alkemytech.sophia.broadcasting.dto.KPI;
import com.alkemytech.sophia.broadcasting.dto.performing.RSKPI;
import com.alkemytech.sophia.broadcasting.dto.performing.RSMonitoraggioKPIOutputDTO;
import com.alkemytech.sophia.broadcasting.dto.performing.RSRequestMonitoraggioKPIDTO;
import com.alkemytech.sophia.broadcasting.enums.Stato;
import com.alkemytech.sophia.broadcasting.service.performing.RsIndicatoreService;
import com.alkemytech.sophia.broadcasting.service.performing.RsInformationFileService;
import com.alkemytech.sophia.broadcasting.service.performing.RsKpiService;
import com.alkemytech.sophia.codman.dto.performing.MusicProviderDTO;
import com.alkemytech.sophia.codman.dto.performing.PalinsestoDTO;
import com.alkemytech.sophia.codman.dto.performing.RsUtenteDTO;
import com.alkemytech.sophia.codman.dto.performing.UploadRequest;
import com.alkemytech.sophia.codman.rest.performing.service.FileArmonizzatoService;
import com.alkemytech.sophia.codman.rest.performing.service.IRadioInStoreService;
import com.alkemytech.sophia.common.beantocsv.CustomMappingStrategy;
import com.alkemytech.sophia.common.s3.S3Service;
import com.alkemytech.sophia.performing.dto.FileArmonizzatoDTO;
import com.alkemytech.sophia.performing.model.*;
import com.amazonaws.services.s3.AmazonS3URI;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.http.HttpStatus;
import org.apache.http.util.TextUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.NoResultException;
import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Singleton
@Path("performing/radioInStore")
public class RadioInStoreController {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final String s3Bucket;
    private final String s3KeyPrefix;
    private final S3Service s3Service;
    private final FileArmonizzatoService fileArmonizzatoService;
    private final IRadioInStoreService iRadioInStoreService;
	private final RsKpiService kpiService;
	private final RsInformationFileService rsInformationFileEntityService;
	private final RsIndicatoreService rsIndicatoreService;
	private final Gson gson;

    //
    @Inject
    protected RadioInStoreController(@Named("broadcasting.s3.bucket") String s3Bucket,
                                     @Named("broadcasting.s3.key_prefix") String s3KeyPrefix,
                                     S3Service s3Service,
                                     FileArmonizzatoService fileArmonizzatoService,
                                     IRadioInStoreService iRadioInStoreService,
                                     RsKpiService kpiService,
                                     RsIndicatoreService rsIndicatoreService,
                                     RsInformationFileService rsInformationFileEntityService) {
        super();
        this.s3Bucket = s3Bucket;
        this.s3KeyPrefix = s3KeyPrefix;
        this.s3Service = s3Service;
        this.iRadioInStoreService = iRadioInStoreService;
        this.kpiService = kpiService;
        this.rsIndicatoreService = rsIndicatoreService;
        this.fileArmonizzatoService = fileArmonizzatoService;
        this.rsInformationFileEntityService = rsInformationFileEntityService;
        this.gson = new Gson();
    }

    @GET
    @Path("musicProviders{p:/?}{id:(([0-9]*)?)}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMusicProviderById(@PathParam("id") Long id, @QueryParam(value = "nome") String nome) {
        try {
            if (id != null) {
                return Response.ok(new GenericEntity<MusicProviderDTO>(iRadioInStoreService.getMusicProviderById(id)) {
                }).build();
            }
            if (!TextUtils.isEmpty(nome)) {
                return Response.ok(new GenericEntity<List<MusicProviderDTO>>(
                        iRadioInStoreService.getMusicProviderListByName(nome)) {
                }).build();
            }
            return Response.ok(new GenericEntity<List<MusicProviderDTO>>(iRadioInStoreService.getMusicProviderList()) {
            }).build();
        } catch (NoResultException e) {
            return Response.noContent().build();
        } catch (Exception e) {
            return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).entity(e).build();
        }
    }

    @GET
    @Path("musicProviders/{id}/palinsesti{p:/?}{idPalinsesto:(([0-9]*)?)}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPalinsestiListByMp(@PathParam("id") Long id, @PathParam("idPalinsesto") Long idPalinsesto) {
        try {
            return Response.ok(new GenericEntity<List<PerfPalinsesto>>(
                    iRadioInStoreService.getPalinsestiListByMp(id, idPalinsesto)) {
            }).build();
        } catch (NoResultException e) {
            return Response.noContent().build();
        } catch (Exception e) {
            return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).entity(e).build();
        }
    }

    @GET
    @Path("palinsesti")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPalinsestiList() {
        try {
            return Response.ok(new GenericEntity<List<PerfPalinsesto>>(iRadioInStoreService.getPalinsestiList()) {
            }).build();
        } catch (NoResultException e) {
            return Response.noContent().build();
        } catch (Exception e) {
            return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).entity(e).build();
        }
    }

    @GET
    @Path("palinsesti/{idPalinsesto}/puntivendita/{p:/?}{id:(([0-9]*)?)}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPuntiVenditaByPalinsesto(@PathParam("id") Long id,
                                                @PathParam("idPalinsesto") Long idPalinsesto) {
        try {
            return Response.ok(new GenericEntity<List<PerfPuntoVendita>>(
                    iRadioInStoreService.getPuntiVenditaByPalinsesto(id, idPalinsesto)) {
            }).build();
        } catch (NoResultException e) {
            return Response.noContent().build();
        } catch (Exception e) {
            return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).entity(e).build();
        }
    }

    @GET
    @Path("utenti{p:/?}{idMusicProvider:(([0-9]*)?)}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsers(@PathParam("idMusicProvider") Long idMusicProvider) {
        try {
            return Response.ok(new GenericEntity<List<PerfRsUtente>>(iRadioInStoreService.getUsers(idMusicProvider)) {
            }).build();
        } catch (NoResultException e) {
            return Response.noContent().build();
        } catch (Exception e) {
            return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).entity(e).build();
        }
    }

    @PUT
    @Path("utente")
    @Produces(MediaType.APPLICATION_JSON)
    public Response insertRsUtente(RsUtenteDTO rsUtenteDTO) {
        try {
            Integer result = iRadioInStoreService.insertRsUtente(rsUtenteDTO);
            if (result == 200) {
                return Response.status(HttpStatus.SC_CREATED).build();
            } else {
                return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).build();
            }
        } catch (NoResultException e) {
            return Response.noContent().build();
        } catch (Exception e) {
            return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).entity(e).build();
        }
    }

    @POST
    @Path("utente")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateRsUtente(RsUtenteDTO rsUtenteDTO) {
        try {
            Integer result = iRadioInStoreService.updateRsUtente(rsUtenteDTO);
            if (result == 200) {
                return Response.status(HttpStatus.SC_CREATED).build();
            } else {
                return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).build();
            }
        } catch (NoResultException e) {
            return Response.noContent().build();
        } catch (Exception e) {
            return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).entity(e).build();
        }
    }

    @POST
    @Path("musicProvider")
    @Produces(MediaType.APPLICATION_JSON)
    public Response insertMusicProvider(MusicProviderDTO musicProviderDTO) {
        try {
            Integer result = iRadioInStoreService.insertMusicProvider(musicProviderDTO);
            if (result == 200) {
                return Response.status(HttpStatus.SC_CREATED).build();
            } else {
                return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).build();
            }
        } catch (NoResultException e) {
            return Response.noContent().build();
        } catch (Exception e) {
            return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).entity(e).build();
        }
    }

    @PUT
    @Path("musicProvider")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateMusicProvider(MusicProviderDTO musicProviderDTO) {
        try {
            Integer result = iRadioInStoreService.updateMusicProvider(musicProviderDTO);
            if (result == 200) {
                return Response.status(HttpStatus.SC_CREATED).build();
            } else {
                return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).build();
            }
        } catch (NoResultException e) {
            return Response.noContent().build();
        } catch (Exception e) {
            return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).entity(e).build();
        }
    }

    @POST
    @Path("palinsesto")
    @Produces(MediaType.APPLICATION_JSON)
    public Response insertPalinsesto(PalinsestoDTO palinsestoDTO) {
        try {
            Integer result = iRadioInStoreService.insertPalinsesto(palinsestoDTO);
            if (result == 200) {
                return Response.status(HttpStatus.SC_CREATED).build();
            } else {
                return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).build();
            }
        } catch (NoResultException e) {
            return Response.noContent().build();
        } catch (Exception e) {
            return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).entity(e).build();
        }

    }

    @PUT
    @Path("palinsesto")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updatePalinsesto(PalinsestoDTO palinsestoDTO) {
        try {
            Integer result = iRadioInStoreService.updatePalinsesto(palinsestoDTO);
            if (result == 200) {
                return Response.status(HttpStatus.SC_CREATED).build();
            } else {
                return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).build();
            }
        } catch (NoResultException e) {
            return Response.noContent().build();
        } catch (Exception e) {
            return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).entity(e).build();
        }
    }

    @GET
    @Path("storicoPalinsesti/{idPalinsesto}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getStoricoPalinsesti(@PathParam("idPalinsesto") Long idPalinsesto) {
        try {
            return Response.ok(new GenericEntity<List<PerfPalinsestoStorico>>(
                    iRadioInStoreService.getStoricoPalinsesto(idPalinsesto)) {
            }).build();
        } catch (NoResultException e) {
            return Response.noContent().build();
        } catch (Exception e) {
            return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).entity(e).build();
        }
    }

    @GET
    @Path("utilizationFiles")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUtilizationFile(
            @QueryParam("idMusicProvider") Long idMusicProvider,
            @QueryParam(value = "idPalinsesto") Long idPalinsesto, @QueryParam(value = "anno") Long anno,
            @QueryParam(value = "mesi") List<Long> mesi) {
        try {
            List<PerfRsUtilizationFile> utilizzazioni =
                    iRadioInStoreService.getUtilizationFile(idMusicProvider, idPalinsesto, anno, mesi);
            return utilizzazioni.size() > 0 ?
                    Response.ok(new GenericEntity<List<PerfRsUtilizationFile>>(utilizzazioni) {
                    }).build() :
                    Response.noContent().build();
        } catch (NoResultException e) {
            return Response.noContent().build();
        } catch (Exception e) {
            return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).entity(e).build();
        }
    }

    /*@POST
    @Path("utilizationfiles")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Response radioInStoreFileUpload(@FormDataParam("file") List<FormDataBodyPart> bodyParts,
                                           @FormDataParam("idPalinsesto") BigInteger idPalinsesto,
                                           @FormDataParam("anno") Short anno,
                                           @FormDataParam("mese") Short mese,
                                           @FormDataParam("idUtente") BigInteger idUtente,
                                           @FormDataParam("idMusicProvider") BigInteger idMusicProvider) {

        for (FormDataBodyPart fdbp : bodyParts) {
            if (parametrsNotValid(idPalinsesto, anno, mese)) {
                return Response.status(Response.Status.BAD_REQUEST).entity(
                        "Il format del caricamento SINGOLO prevede l'inserimento dei valori nei campi palinsesto, anno e mese; mentre "
                                + "il format del caricamento MASSIVO prevede che i campi palinsesto, anno e mese vengano lasciati vuoti.").build();
            }
            String[] nameFileParts = fdbp.getFormDataContentDisposition().getFileName().split("\\.(?=[^\\.]+$)");
            String nameFile = nameFileParts[0] + "_" + Calendar.getInstance().getTimeInMillis() + "." + nameFileParts[1];
            final String percorso = String.format("s3://%s/%s/%s", s3Bucket, s3KeyPrefix, (iRadioInStoreService.getMusicProviderById(idMusicProvider.longValue()).getNome()).replace(" ", "") + "/" + nameFile);
            logger.debug("upload: percorso {}", percorso);
            final AmazonS3URI s3Uri = new AmazonS3URI(percorso);
            logger.debug("upload: s3Uri {}", s3Uri);
            saveRsUtilizationFile(idPalinsesto, anno, mese, idUtente, idMusicProvider, fdbp, percorso, s3Uri);
        }
        return Response.status(Response.Status.CREATED).build();
    }*/

    @POST
    @Path("utilizationFiles")
    @Produces(MediaType.APPLICATION_JSON)
    public Response radioInStoreFileUploadWithoutFiles(UploadRequest uploadRequest) {

        int i = 0;
        for (String filename : uploadRequest.getFilenames()) {
            final String percorso = String.format("s3://%s/%s/%s", s3Bucket, s3KeyPrefix, uploadRequest.getKeys().get(i));
            saveRsUtilizationFileWithoutFiles(
                    uploadRequest.getIdPalinsesto(),
                    uploadRequest.getAnno(),
                    uploadRequest.getMese(),
                    uploadRequest.getIdUtente(),
                    uploadRequest.getIdMusicProvider(), filename, percorso);
            i++;
        }
        return Response.status(Response.Status.CREATED).build();
    }

	/*private boolean parametrsNotValid(BigInteger idPalinsesto, Short anno, Short mese) {
		return !((idPalinsesto == null && anno == null && mese == null)
				|| (idPalinsesto != null && anno != null && mese != null));
	}

    private void saveRsUtilizationFile(BigInteger idPalinsesto, Short anno, Short mese, BigInteger idUtente, BigInteger idMusicProvider, FormDataBodyPart fdbp, String percorso, AmazonS3URI s3Uri) {
        if (s3Service.upload(s3Uri.getBucket(), s3Uri.getKey(), fdbp.getEntityAs(InputStream.class), fdbp.getFormDataContentDisposition().getFileName())) {
            final PerfRsUtilizationFile rsUtilizationFile = new PerfRsUtilizationFile();

            rsUtilizationFile.getMusicProvider().setIdMusicProvider(idMusicProvider);
            PerfRsUtente utente = new PerfRsUtente();
            utente.setId(idUtente);
            PerfPalinsesto palinsesto=new PerfPalinsesto();
            palinsesto.setIdPalinsesto(idPalinsesto);

             rsUtilizationFile.setUtente(utente);
             rsUtilizationFile.setPalinsesto(palinsesto);

//            rsUtilizationFile.setIdMusicProvider(idMusicProvider);
//            rsUtilizationFile.setIdUtente(idUtente);
//            rsUtilizationFile.setIdPalinsesto(idPalinsesto);
            setRsUtilizationFile(idPalinsesto, anno, mese, percorso, rsUtilizationFile, fdbp.getFormDataContentDisposition().getFileName());
            rsUtilizationFile.setStato(Stato.DA_ELABORARE.toString());
            iRadioInStoreService.save(rsUtilizationFile);
        }
    }*/

    private void setRsUtilizationFile(BigInteger idPalinsesto, Short anno, Short mese, String percorso, PerfRsUtilizationFile rsUtilizationFile, String fileName) {
        rsUtilizationFile.setNomeFile(fileName);
        rsUtilizationFile.setAnno(anno);
        rsUtilizationFile.setMese(mese);
        rsUtilizationFile.setPercorso(percorso);
        if (idPalinsesto == null && anno == null && mese == null) {
            rsUtilizationFile.setTipoUpload("MASSIVO");
        } else {
            rsUtilizationFile.setTipoUpload("SINGOLO");
        }
    }

    private void saveRsUtilizationFileWithoutFiles(BigInteger idPalinsesto, Short anno, Short mese, BigInteger idUtente, BigInteger idMusicProvider, String filename, String percorso) {
        final PerfRsUtilizationFile rsUtilizationFile = new PerfRsUtilizationFile();
        rsUtilizationFile.setPerfMusicProvider(new PerfMusicProvider());
        rsUtilizationFile.getPerfMusicProvider().setIdMusicProvider(idMusicProvider);
        PerfRsUtente utente = new PerfRsUtente();
        utente.setId(idUtente);
        PerfPalinsesto palinsesto = new PerfPalinsesto();
        palinsesto.setIdPalinsesto(idPalinsesto);
        rsUtilizationFile.setPerfRsUtente(utente);
        rsUtilizationFile.setPerfPalinsesto(palinsesto);
//        rsUtilizationFile.setIdMusicProvider(idMusicProvider);
//        rsUtilizationFile.setIdUtente(idUtente);
//        rsUtilizationFile.setIdPalinsesto(idPalinsesto);
        setRsUtilizationFile(idPalinsesto, anno, mese, percorso, rsUtilizationFile, filename);
        rsUtilizationFile.setStato(Stato.DA_ELABORARE.toString());
        iRadioInStoreService.save(rsUtilizationFile);
    }

    @GET
    @Path("utilizationFiles/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response downloadById(@PathParam("id") Integer id) {
        final AmazonS3URI s3Uri = new AmazonS3URI(rsInformationFileEntityService.findRsUtilizationById(id).getPercorso());

        StreamingOutput stream = getStreamingOutputFromS3(s3Uri);

        return Response.ok(stream).type(MediaType.APPLICATION_OCTET_STREAM)
                .header("Content-Disposition",
                        "attachment; filename=\"" + rsInformationFileEntityService.findRsUtilizationById(id).getNomeFile() + "\"")
                .build();
    }

    private StreamingOutput getStreamingOutputFromS3(final AmazonS3URI s3Uri) {
        return new StreamingOutput() {
            @Override
            public void write(OutputStream outputStream) throws IOException, WebApplicationException {
                InputStream is = s3Service.download(s3Uri.getBucket(), s3Uri.getKey());
                int nextByte = 0;
                while ((nextByte = is.read()) != -1) {
                    outputStream.write(nextByte);
                }
                outputStream.flush();
                outputStream.close();
                is.close();
            }
        };
    }

    @POST
    @Path("armonizzato")
    @Produces("text/csv")
    public Response getFileArmonizzato(FileArmonizzatoRequestDTO fileArmonizzatoRequestDTO) {

        if (isRequestValid(fileArmonizzatoRequestDTO))
            return Response.status(Response.Status.BAD_REQUEST).build();

        StreamingOutput so;

        try {

            final List<FileArmonizzatoDTO> armonizzatoDTOs = fileArmonizzatoService.getFileArmonizzato(
                     fileArmonizzatoRequestDTO.getMusicProvider(), fileArmonizzatoRequestDTO.getPalinsesti(),
                    fileArmonizzatoRequestDTO.getAnno(), fileArmonizzatoRequestDTO.getMesi());

            if (!CollectionUtils.isEmpty(armonizzatoDTOs)) {
                final CustomMappingStrategy<FileArmonizzatoDTO> customMappingStrategy = new CustomMappingStrategy<>(new FileArmonizzatoDTO().getMappingStrategy());
                customMappingStrategy.setType(FileArmonizzatoDTO.class);
                so = new StreamingOutput() {
                    @Override
                    public void write(OutputStream outputStream) throws IOException, WebApplicationException {
                        OutputStreamWriter osw = new OutputStreamWriter(outputStream);

                        StatefulBeanToCsv beanToCsv = new StatefulBeanToCsvBuilder(osw).withMappingStrategy(customMappingStrategy)
                                .withSeparator(';').build();
                        try {
                            beanToCsv.write(armonizzatoDTOs);
                            osw.flush();
                            outputStream.flush();
                        } catch (CsvDataTypeMismatchException e) {
                        } catch (CsvRequiredFieldEmptyException e) {
                        } finally {
                            if (osw != null)
                                osw.close();
                        }
                    }
                };
            } else {
                return Response.noContent().build();
            }
            return Response.ok(so)
                    .type("text/csv")
                    .header("Content-Disposition", "attachment; filename=\"" + fileArmonizzatoRequestDTO.getHarmonizedFileName() + ".csv\"")
                    .build();
        } catch (Exception e) {
            logger.error(e.getMessage());
            return Response.status(500).entity(e.getMessage()).build();
        }
    }

    private boolean isRequestValid(FileArmonizzatoRequestDTO fileArmonizzatoRequestDTO) {
        return null == fileArmonizzatoRequestDTO || fileArmonizzatoRequestDTO.getPalinsesti() == null
                || CollectionUtils.isEmpty(fileArmonizzatoRequestDTO.getPalinsesti()) || null == fileArmonizzatoRequestDTO.getAnno()
                || null == fileArmonizzatoRequestDTO.getMesi() || CollectionUtils.isEmpty(fileArmonizzatoRequestDTO.getMesi());
    }

    @POST
    @Path("kpis")
    @Produces("application/json")
    public Response getKPI(RSRequestMonitoraggioKPIDTO monitoraggioKPI) {
        RSMonitoraggioKPIOutputDTO monitoraggioKPIOutputDTO = new RSMonitoraggioKPIOutputDTO();

        List<RSKPI> listaKPI = new ArrayList<RSKPI>();
        if (monitoraggioKPI != null &&
                !CollectionUtils.isEmpty(monitoraggioKPI.getPalinsesti()) &&
                !CollectionUtils.isEmpty(monitoraggioKPI.getMesi())
        ) {
            Date lastKpiUpdate = null;
            HashMap<String, IndicatoreDTO> mapIndicatori = new HashMap<String, IndicatoreDTO>();
            for (IndicatoreDTO indicatore : rsIndicatoreService.listaInd(monitoraggioKPI)) {
                mapIndicatori.put(indicatore.getIndicatore(), indicatore);
                if (lastKpiUpdate == null || indicatore.getLastUpdate().after(lastKpiUpdate)) {
                    lastKpiUpdate = indicatore.getLastUpdate();
                }
            }

            monitoraggioKPIOutputDTO.setKpiAttesi(Consts.KPIATTESIRADIO);

            listaKPI.add(kpiService.calcolaKpiRecordScartati(mapIndicatori));
            listaKPI.add(kpiService.calcolaKpiRecordInRitardo(mapIndicatori));

            monitoraggioKPIOutputDTO.setLastUpdate(lastKpiUpdate);
            monitoraggioKPIOutputDTO.setListaKPI(listaKPI);
            monitoraggioKPIOutputDTO.setFileDaElaborare(rsInformationFileEntityService.fileDTOS(monitoraggioKPI));

            return Response.ok(gson.toJson(monitoraggioKPIOutputDTO)).build();
        } else {
            monitoraggioKPIOutputDTO.setKpiAttesi(Consts.KPIATTESIRADIO);
            listaKPI.add(new RSKPI(true, KPI.KPI_RECORD_ACCETTATI, KPI.DESC_KPI_RECORD_ACCETTATI, new BigDecimal(0)));
            listaKPI.add(new RSKPI(true, KPI.KPI_RECORD_IN_TEMPO, KPI.DESC_KPI_RECORD_IN_TEMPO, new BigDecimal(0)));



        }
        monitoraggioKPIOutputDTO.setLastUpdate(new Date());
        monitoraggioKPIOutputDTO.setListaKPI(listaKPI);
        // monitoraggioKPIOutputDTO.setFileDaElaborare(bdcInformationFileDTOService.fileDTOS(monitoraggioKPI));
        return Response.ok(new Gson().toJson(monitoraggioKPIOutputDTO)).build();
    }
}
