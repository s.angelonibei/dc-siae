package com.alkemytech.sophia.codman.multimedialelocale.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Search {

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("artists")
    @Expose
    private String artists;

    @SerializedName("roles")
    @Expose
    private String roles;

    @SerializedName("navbarTab")
    @Expose
    private String navbarTab;
    @SerializedName("execute")
    @Expose
    private Boolean execute;
    @SerializedName("maxrows")
    @Expose
    private Integer maxrows;
    @SerializedName("first")
    @Expose
    private Integer first;
    @SerializedName("last")
    @Expose
    private Integer last;
    @SerializedName("sortType")
    @Expose
    private String sortType;
    @SerializedName("sortReverse")
    @Expose
    private Boolean sortReverse;
    @SerializedName("currentTimeMillis")
    @Expose
    private Long currentTimeMillis;
    @SerializedName("hashId")
    @Expose
    private String hashId;

    public String getNavbarTab() {
        return navbarTab;
    }

    public void setNavbarTab(String navbarTab) {
        this.navbarTab = navbarTab;
    }

    public Boolean getExecute() {
        return execute;
    }

    public void setExecute(Boolean execute) {
        this.execute = execute;
    }

    public Integer getMaxrows() {
        return maxrows;
    }

    public void setMaxrows(Integer maxrows) {
        this.maxrows = maxrows;
    }

    public Integer getFirst() {
        return first;
    }

    public void setFirst(Integer first) {
        this.first = first;
    }

    public Integer getLast() {
        return last;
    }

    public void setLast(Integer last) {
        this.last = last;
    }

    public String getSortType() {
        return sortType;
    }

    public void setSortType(String sortType) {
        this.sortType = sortType;
    }

    public Boolean getSortReverse() {
        return sortReverse;
    }

    public void setSortReverse(Boolean sortReverse) {
        this.sortReverse = sortReverse;
    }

    public Long getCurrentTimeMillis() {
        return currentTimeMillis;
    }

    public void setCurrentTimeMillis(Long currentTimeMillis) {
        this.currentTimeMillis = currentTimeMillis;
    }

    public String getHashId() {
        return hashId;
    }

    public void setHashId(String hashId) {
        this.hashId = hashId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtists() {
        return artists;
    }

    public void setArtists(String artists) {
        this.artists = artists;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }
}