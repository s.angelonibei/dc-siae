package com.alkemytech.sophia.codman.multimedialelocale.rest;

import java.sql.Timestamp;

public class MultimedialeLocaleDto {
	private Timestamp dataCodifica;
	private String nominativoLicenziatario;
	private String licenza;
	private String servizio;
	private String origPeriodoCompetenza;
	private String modUtilizzazione;
	private String modFruizione;
	private Boolean inAbbonamento;
	private String statoLogico;
	private Double vecchioCodificatoUtilizzazioni;
	private Double codificatoUtilizzazioni;
	private String ripartizione;
	public MultimedialeLocaleDto() {
		// TODO Auto-generated constructor stub
	}
	public MultimedialeLocaleDto(Object[] record) {
		
		dataCodifica=((Timestamp)record[0]);
		nominativoLicenziatario=((String)record[1]);
		licenza=((String)record[2]);
		servizio=((String)record[3]);
		origPeriodoCompetenza=((String)record[4]);
		modUtilizzazione=(String)record[5];
		modFruizione=((String)record[6]);
		inAbbonamento=((Boolean)record[7]);
		statoLogico=((String)record[8]);
		if(((Integer)record[10])!=null) {
			vecchioCodificatoUtilizzazioni=(((Integer)record[10]).doubleValue()/((Integer)record[9]).doubleValue()*100.0);
		}
		if(((Integer)record[11])!=null) {
			codificatoUtilizzazioni=(((Integer)record[11]).doubleValue()/((Integer)record[9]).doubleValue()*100.0);
		}
		ripartizione=((String)record[12]);    
	}
	public Timestamp getDataCodifica() {
		return dataCodifica;
	}
	public void setDataCodifica(Timestamp dataCodifica) {
		this.dataCodifica = dataCodifica;
	}
	public String getNominativoLicenziatario() {
		return nominativoLicenziatario;
	}
	public void setNominativoLicenziatario(String nominativoLicenziatario) {
		this.nominativoLicenziatario = nominativoLicenziatario;
	}
	public String getLicenza() {
		return licenza;
	}
	public void setLicenza(String licenza) {
		this.licenza = licenza;
	}
	public String getServizio() {
		return servizio;
	}
	public void setServizio(String servizio) {
		this.servizio = servizio;
	}
	public String getOrigPeriodoCompetenza() {
		return origPeriodoCompetenza;
	}
	public void setOrigPeriodoCompetenza(String origPeriodoCompetenza) {
		this.origPeriodoCompetenza = origPeriodoCompetenza;
	}
	public String getModUtilizzazione() {
		return modUtilizzazione;
	}
	public void setModUtilizzazione(String modUtilizzazione) {
		this.modUtilizzazione = modUtilizzazione;
	}
	public String getModFruizione() {
		return modFruizione;
	}
	public void setModFruizione(String modFruizione) {
		this.modFruizione = modFruizione;
	}
	public Boolean getInAbbonamento() {
		return inAbbonamento;
	}
	public void setInAbbonamento(Boolean inAbbonamento) {
		this.inAbbonamento = inAbbonamento;
	}
	public String getStatoLogico() {
		return statoLogico;
	}
	public void setStatoLogico(String statoLogico) {
		this.statoLogico = statoLogico;
	}
	public Double getVecchioCodificatoUtilizzazioni() {
		return vecchioCodificatoUtilizzazioni;
	}
	public void setVecchioCodificatoUtilizzazioni(Double vecchioCodificatoUtilizzazioni) {
		this.vecchioCodificatoUtilizzazioni = vecchioCodificatoUtilizzazioni;
	}
	public Double getCodificatoUtilizzazioni() {
		return codificatoUtilizzazioni;
	}
	public void setCodificatoUtilizzazioni(Double codificatoUtilizzazioni) {
		this.codificatoUtilizzazioni = codificatoUtilizzazioni;
	}
	public String getRipartizione() {
		return ripartizione;
	}
	public void setRipartizione(String ripartizione) {
		this.ripartizione = ripartizione;
	}

}
