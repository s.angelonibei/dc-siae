package com.alkemytech.sophia.codman.utils;

import org.apache.commons.lang3.StringUtils;
import org.jooq.Field;
import org.jooq.impl.SQLDataType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Date;
import java.util.Arrays;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.inline;

public class QueryUtils {

    private final static Logger logger = LoggerFactory.getLogger(QueryUtils.class);

    public static String getValuesCollection(Iterable<String> list) {
        String result = "";
        String valueSeparator = " , ";
        for (String e : list) {
            result = result.concat("\"").concat(e).concat("\"").concat(valueSeparator);
        }
        result = result.substring(0, result.lastIndexOf(valueSeparator));

        return result;
    }

	public static Field<String> dateFormat(Field<Date> field, String format) {
		return field("date_format({0}, {1})", SQLDataType.VARCHAR, field, inline(format));
	}
	
	public static String whiteListOrderBy(String toParseString, Class<?> entity, String entityAlias) {
		if(StringUtils.isEmpty(toParseString)) return "";
		try {
			String sort = " asc";
			String[] splits = toParseString.trim().split("\\s+");
			if (splits.length == 2 && Arrays.asList("asc", "desc").contains(splits[1].toLowerCase())) {
				sort = " " + splits[1];
			}
			if(splits.length > 0) {
				for (java.lang.reflect.Field f : entity.getDeclaredFields()) {
					if(f.getName().equals(splits[0])) {
						return " order by " + entityAlias + "." + splits[0] + sort; 
					}
				}
			}
		} catch (Exception e) {
			logger.error("Error validating order by clause:", e);
		}
		return "";
	}

}
