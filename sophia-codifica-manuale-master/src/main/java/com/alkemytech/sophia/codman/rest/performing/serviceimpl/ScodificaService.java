package com.alkemytech.sophia.codman.rest.performing.serviceimpl;

import com.alkemytech.sophia.codman.dto.ResponseDTO;
import com.alkemytech.sophia.codman.performing.dto.SessioneCodificaDTO;
import com.alkemytech.sophia.codman.entity.performing.dao.ScodificaDAO;
import com.alkemytech.sophia.codman.performing.dto.ScodificaRequestDTO;
import com.alkemytech.sophia.codman.rest.performing.service.IScodificaService;
import com.google.inject.Inject;

public class ScodificaService implements IScodificaService {
    private ScodificaDAO scodificaDAO;

    @Inject
    public ScodificaService(ScodificaDAO scodificaDAO) {
        this.scodificaDAO = scodificaDAO;
    }

    public ResponseDTO getListaScodifica(ScodificaRequestDTO scodificaRequestDTO){
        return scodificaDAO.getListaScodifica(scodificaRequestDTO);
    }

    @Override
    public ResponseDTO getDettaglioScodifica(Long idCombana) {
        return scodificaDAO.getDettaglioScodifica(idCombana);
    }

    @Override
    public ResponseDTO getDettaglioCombana(Long idCombana) {
        return scodificaDAO.getDettaglioCombana(idCombana);
    }

    @Override
    public void deleteCodiceOpera(Long idCodifica, Long idCombana, String codiceOpera) {
        scodificaDAO.deleteCodiceOpera(idCodifica, idCombana, codiceOpera);
    }

    @Override
    public ResponseDTO getListaScodificaForExport(ScodificaRequestDTO scodificaRequestDTO) {
        return scodificaDAO.getListaScodificaForExport(scodificaRequestDTO);
    }

    @Override
    public ResponseDTO getListaUtilizzazioniForExport(Long idCombana) {
        return scodificaDAO.getListaUtilizzazioniForExport(idCombana);
    }

    @Override
    public SessioneCodificaDTO getSessioneScodifica(Long idCombana, Long idCodifica, String utente) {
        return scodificaDAO.getSessioneScodifica(idCombana, idCodifica, utente);
    }
}
