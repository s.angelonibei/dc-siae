package com.alkemytech.sophia.codman.servlet;

import com.alkemytech.sophia.codman.entity.CCIDS3Path;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.alkemytech.sophia.common.s3.S3Service;
import com.amazonaws.services.s3.AmazonS3URI;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import org.apache.http.HttpHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Singleton
@SuppressWarnings("serial")
public class S3CCIDDownloadServlet extends HttpServlet {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final S3Service s3Service;
    private final Provider<EntityManager> provider;


    @Inject
    protected S3CCIDDownloadServlet(S3Service s3Service, @McmdbDataSource Provider<EntityManager> provider) {
        super();
        this.s3Service = s3Service;
        this.provider = provider;
    }

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) {

        try {

            final EntityManager entityManager = provider.get();
            final CCIDS3Path ccidS3Path = entityManager
                    .find(CCIDS3Path.class, Integer.parseInt(request.getParameter("id")));
            if (null == ccidS3Path || null == ccidS3Path.getS3Path() || ccidS3Path.getS3Path().isEmpty()) {
                throw new IllegalArgumentException();
            }

            final AmazonS3URI s3Uri = new AmazonS3URI(ccidS3Path.getS3Path());
            response.addHeader(HttpHeaders.CONTENT_TYPE, "text/csv");
            String url = ccidS3Path.getS3Path();
            String fileName = url !=  null? url.substring(url.lastIndexOf("/")+1, url.length()) : ccidS3Path.getIdDsr();           
            response.addHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
            s3Service.download(s3Uri.getBucket(), s3Uri.getKey(), response.getOutputStream());

        } catch (Exception e) {
            logger.error("S3CCIDDownloadServlet", e);
        }

    }

}
