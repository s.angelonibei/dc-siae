package com.alkemytech.sophia.codman.rest.performing.service;

import com.alkemytech.sophia.codman.dto.ResponseDTO;
import com.alkemytech.sophia.codman.performing.dto.SessioneCodificaDTO;
import com.alkemytech.sophia.codman.performing.dto.ScodificaRequestDTO;

public interface IScodificaService {
    ResponseDTO getListaScodifica(ScodificaRequestDTO scodificaRequestDTO);

    ResponseDTO getDettaglioScodifica(Long idCombana);

    ResponseDTO getDettaglioCombana(Long idCombana);
    void deleteCodiceOpera(Long idCodifica, Long idCombana, String codiceOpera);

    ResponseDTO getListaScodificaForExport(ScodificaRequestDTO scodificaRequestDTO);
    ResponseDTO getListaUtilizzazioniForExport(Long idCombana);

    SessioneCodificaDTO getSessioneScodifica(Long idCombana, Long idCodifica, String utente);
}
