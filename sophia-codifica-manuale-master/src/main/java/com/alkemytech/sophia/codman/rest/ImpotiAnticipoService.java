package com.alkemytech.sophia.codman.rest;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.codman.dto.AnagClientDTO;
import com.alkemytech.sophia.codman.dto.CcidInfoDTO;
import com.alkemytech.sophia.codman.dto.DspDTO;
import com.alkemytech.sophia.codman.dto.ImportiAnticipoDTO;
import com.alkemytech.sophia.codman.dto.InvoiceCCIDDTO;
import com.alkemytech.sophia.codman.dto.InvoiceDTO;
import com.alkemytech.sophia.codman.dto.InvoiceDraftDto;
import com.alkemytech.sophia.codman.dto.InvoiceItemDto;
import com.alkemytech.sophia.codman.dto.SearchImportiAnticipo;
import com.alkemytech.sophia.codman.dto.UsaAnticipoDTO;
import com.alkemytech.sophia.codman.entity.AnagClient;
import com.alkemytech.sophia.codman.entity.CCIDMetadata;
import com.alkemytech.sophia.codman.entity.DettailImportiAnticipo;
import com.alkemytech.sophia.codman.entity.ImportiAnticipo;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.alkemytech.sophia.codman.invoice.repository.Invoice;
import com.alkemytech.sophia.codman.invoice.repository.InvoiceItem;
import com.alkemytech.sophia.codman.invoice.repository.InvoiceItemReason;
import com.alkemytech.sophia.codman.invoice.repository.InvoiceItemToCcid;
import com.alkemytech.sophia.codman.utils.InvoiceStatus;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

@Singleton
@Path("importiAnticipi")
public class ImpotiAnticipoService {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Provider<EntityManager> provider;
	private final Gson gson;

	@Inject
	protected ImpotiAnticipoService(@McmdbDataSource Provider<EntityManager> provider, Gson gson) {
		super();
		this.provider = provider;
		this.gson = gson;
	}

	@GET
	@Path("getAll")
	@Produces("application/json")
	public Response getAll() {
		return Response.ok(gson.toJson("hello world")).build();
	}

	@DELETE
	@Path("deleteAdvancePayment")
	public Response deleteAnticipo(ImportiAnticipoDTO dataDTO) {
		EntityManager entityManager = provider.get();
		Map<InvoiceItemToCcid, CCIDMetadata> mapping = new HashMap<>();

		// recupera l'anticipo
		ImportiAnticipo importiAnticipo = (ImportiAnticipo) entityManager
				.createQuery("SELECT x FROM ImportiAnticipo x WHERE x.idImportoAnticipo = :idImportoAnticipo")
				.setParameter("idImportoAnticipo", dataDTO.getIdImportoAnticipo()).getSingleResult();
		Invoice invoice = (Invoice) entityManager.createQuery("Select x from Invoice x where x.idInvoice = :idInvoice")
				.setParameter("idInvoice", importiAnticipo.getIdInvoice()).getSingleResult();
		// se l'anticipo non è nullo viene eliminato
		if (importiAnticipo != null) {
			try {
				// recupera tutte le quote dell'anticipo che si vuole eliminare
				for (InvoiceItemToCcid joinAnticipiCcid : invoice.getInvoiceItems().get(0).getInvoiceItemToCcid()) {
					// associa a ogni quota il suo ccid
					mapping.put(joinAnticipiCcid, joinAnticipiCcid.getIdCcidMedata());
				}

				// Vengono riassegnate tutte le quote al ccid reincrementando il valore residuo
				// fatturabile
				for (Map.Entry<InvoiceItemToCcid, CCIDMetadata> entry : mapping.entrySet()) {

					InvoiceItemToCcid anticipo = entry.getKey();
					CCIDMetadata ccidMetadata = entry.getValue();

					BigDecimal quota = anticipo.getValore();
					BigDecimal valoreResiduo = ccidMetadata.getValoreResiduo();

					ccidMetadata.setValoreResiduo(valoreResiduo.add(quota));
					ccidMetadata.setInvoiceStatus(InvoiceStatus.DA_FATTURARE.toString());
					mapping.put(anticipo, ccidMetadata);

				}

				// apre la transazione
				entityManager.getTransaction().begin();

				for (Map.Entry<InvoiceItemToCcid, CCIDMetadata> entry : mapping.entrySet()) {
					entityManager.merge(entry.getValue());
				}

				for (Map.Entry<InvoiceItemToCcid, CCIDMetadata> entry : mapping.entrySet()) {
					entityManager.remove(entry.getKey());
				}

				// elimina l'anticipo
				entityManager.remove(importiAnticipo);
				// salva la modifica nel db
				removeInvoice(invoice.getIdInvoice(), entityManager);
				entityManager.getTransaction().commit();
				return Response.ok().build();
			} catch (Exception e) {
				e.printStackTrace();

				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				return Response.status(Response.Status.CONFLICT)
						.entity("{\"message\":\"L'anticipo non può essere eliminato dato che è collegato a un CCID\"}")
						.build();
			}
		}
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
	}

	public void removeInvoice(Integer idInvoice, EntityManager entityManager) {
		try {
			Invoice invoice = (Invoice) entityManager
					.createQuery("Select x from Invoice x where x.idInvoice = :idInvoice")
					.setParameter("idInvoice", idInvoice).getSingleResult();

			for (InvoiceItem invoiceItem : invoice.getInvoiceItems()) {

				List<InvoiceItemToCcid> invoiceItemToCcids = entityManager
						.createQuery("Select x from InvoiceItemToCcid x where x.invoiceItem.idItem = :invoiceItem")
						.setParameter("invoiceItem", invoiceItem.getIdItem()).getResultList();

				List<CCIDMetadata> metadataList = (List<CCIDMetadata>) entityManager
						.createQuery("select x from CCIDMetadata x where x.invoiceItem = :invoiceItemId")
						.setParameter("invoiceItemId", invoiceItem.getIdItem()).getResultList();
				for (CCIDMetadata metadata : metadataList) {
					metadata.setInvoiceStatus("DA_FATTURARE");
					metadata.setInvoiceItem(null);
					BigDecimal tmpValoreResiduo = new BigDecimal(0);
					for (InvoiceItemToCcid invoiceItemToCcid : invoiceItemToCcids) {
						if (invoiceItemToCcid.getIdCcidMedata().equals(metadata.getIdCCIDMetadata())) {
							tmpValoreResiduo.add(invoiceItemToCcid.getValore());
						}

					}
					// reimposta il valore_residuo con la somma di sestesso + quota Fattura
					// Si somma quota fattura con valore residuo perchè potrebbe essere stato
					// stornato anche un anticipo

					metadata.setValoreResiduo(tmpValoreResiduo);
					// reimposta il valore quota_fattura a 0 dato che si sta eliminando la fattura
					metadata.setQuotaFattura(new BigDecimal("0"));

				}
				entityManager.createQuery("delete from InvoiceItemToCcid x where x.invoiceItem.idItem = :invoiceItem")
						.setParameter("invoiceItem", invoiceItem.getIdItem()).executeUpdate();
				entityManager.remove(invoiceItem);
			}

			entityManager.createQuery("delete from InvoiceLog x where x.idInvoice = :idInvoice")
					.setParameter("idInvoice", idInvoice).executeUpdate();
			entityManager.remove(invoice);

		} catch (Exception e) {

			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
			logger.error("delete", e);
			throw (e);
		}

	}

	@DELETE
	@Path("deleteAll")
	public Response deleteAll() {
		return Response.ok().build();
	}

	@POST
	@Path("updateAdvancePayment")
	public Response update(ImportiAnticipoDTO dataDTO) {

		String PATTERN_DATE_FORMAT = "dd-MM-yyyy";

		SimpleDateFormat dateFormat = new SimpleDateFormat(PATTERN_DATE_FORMAT);

		EntityManager entityManager = provider.get();
		Query query = entityManager
				.createQuery("SELECT x FROM ImportiAnticipo x where x.idImportoAnticipo = :idImportoAnticipo")
				.setParameter("idImportoAnticipo", dataDTO.getIdImportoAnticipo());
		List<ImportiAnticipo> listImportiAnticipo = query.getResultList();
		ImportiAnticipo importoanticipo = listImportiAnticipo.get(0);

		if (importoanticipo != null) {

			Date periodoPertinenzaFine = importoanticipo.getPeriodoPertinenzaFine();

			String newDate = "01" + "-" + dataDTO.getPeriodTo().split("-")[0] + "-"
					+ dataDTO.getPeriodTo().split("-")[1];
			try {

				Date newPeriodoPertinenzaFine = dateFormat.parse(newDate);

				if (newPeriodoPertinenzaFine.after(periodoPertinenzaFine)
						|| newPeriodoPertinenzaFine.equals(periodoPertinenzaFine)) {
					// aggiorno il valore
					importoanticipo.setPeriodoPertinenzaFine(newPeriodoPertinenzaFine);
					// apre la transazione
					entityManager.getTransaction().begin();
					// aggiorna l'entity con la nuova data pertinenza fine
					entityManager.merge(importoanticipo);
					// salva la modifica nel db
					entityManager.getTransaction().commit();

					return Response.ok().build();
				} else {
					// errore.
					return Response.status(Response.Status.CONFLICT).entity(
							"{\"message\":\"La data pertinenza fine deve essere superiore a quella corrente.\"}")
							.build();
				}

			} catch (ParseException e) {

				if (entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				e.printStackTrace();
			}

		}
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

	}

	@POST
	@Path("addAdvancePayment")
	public Response add(ImportiAnticipoDTO dataDTO) {

		EntityManager entityManager = null;

		entityManager = provider.get();

		List<ImportiAnticipo> importiAnticipi = entityManager
				.createQuery("Select x from ImportiAnticipo x where x.numeroFattura=:numeroFattura")
				.setParameter("numeroFattura", dataDTO.getInvoiceNumber()).getResultList();

		if (importiAnticipi != null && importiAnticipi.size() > 0) {
			return Response.status(406).build();
		}

		ImportiAnticipo entity = new ImportiAnticipo();
		entity.setIdDsp(dataDTO.getIdDSP());
		entity.setImportoOriginale(dataDTO.getOriginalAmount());
		entity.setImportoUtilizzabile(dataDTO.getAmountUsed());
		entity.setNumeroFattura(dataDTO.getInvoiceNumber());

		String[] arrPeriodFrom = dataDTO.getPeriodFrom().split("-");
		GregorianCalendar gcalendarFrom = new GregorianCalendar(Integer.parseInt(arrPeriodFrom[1]),
				Integer.parseInt(arrPeriodFrom[0]) - 1, 1, 0, 0, 0);
		Date dateFrom = gcalendarFrom.getTime();
		entity.setPeriodoPertinenzaInizio(dateFrom);

		String[] arrPeriodTo = dataDTO.getPeriodTo().split("-");
		GregorianCalendar gcalendarTo = new GregorianCalendar(Integer.parseInt(arrPeriodTo[1]),
				Integer.parseInt(arrPeriodTo[0]) - 1, 1, 0, 0, 0);
		Date dateTo = gcalendarTo.getTime();
		entity.setPeriodoPertinenzaFine(dateTo);

		if (dateFrom.after(dateTo)) {
			return Response.status(409).build();
		}
		entityManager.getTransaction().begin();
		InvoiceDTO invoiceDTO = new InvoiceDTO();
		AnagClientDTO anagClientDTO = new AnagClientDTO();
		DspDTO dspDTO = new DspDTO();
		List<InvoiceItemDto> invoiceItems = new ArrayList<>();

		InvoiceItemDto invoiceItem = new InvoiceItemDto();
		invoiceItem.setCurrency("EUR");
		// invoiceItem.setIdInvoice(idInvoice);
		invoiceItem.setInvoicePosition(1);
		invoiceItem.setTotalValue(new BigDecimal(0));
		invoiceItem.setTotalValueOrig(dataDTO.getOriginalAmount());
		invoiceItems.add(invoiceItem);
		dspDTO.setIdDsp(dataDTO.getIdDSP());
		anagClientDTO.setIdDsp(dataDTO.getIdDSP());
		invoiceDTO.setClientData(anagClientDTO);
		invoiceDTO.setCreationDate(new Date());
		invoiceDTO.setDsp(dspDTO);
		invoiceDTO.setIdDsp(dataDTO.getIdDSP());
		invoiceDTO.setInvoiceCode(dataDTO.getInvoiceNumber());
		invoiceDTO.setInvoiceItems(invoiceItems);
		invoiceDTO.setLastUpdateDate(new Date());
		invoiceDTO.setStatus("FATTURATO");
		invoiceDTO.setTotal(dataDTO.getOriginalAmount());
		InvoiceDTO item = newInvoice(invoiceDTO, entityManager);
		if (item == null) {
			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
			return Response.status(500).build();
		} else {
			entity.setIdInvoice(invoiceDTO.getIdInvoice());
		}
		entityManager.persist(entity);
		entityManager.getTransaction().commit();
		return Response.ok(gson.toJson(dataDTO)).build();
	}

	@POST
	@Path("getAdvancePayment")
	public Response get(SearchImportiAnticipo dataDTO) {
		SimpleDateFormat simpledateformat = new SimpleDateFormat("dd-MM-yyyy");
		List<ImportiAnticipo> resultQuery = null;
		List<ImportiAnticipoDTO> result = new ArrayList<ImportiAnticipoDTO>();
		EntityManager entityManager = null;
		try {

			List<Predicate> predicates = new ArrayList<Predicate>();

			entityManager = provider.get();
			CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<ImportiAnticipo> createQuery = criteriaBuilder.createQuery(ImportiAnticipo.class);
			Root<ImportiAnticipo> importiAnticipo = createQuery.from(ImportiAnticipo.class);

			// recupero il metamodell dell'entity
			Metamodel metaModel = entityManager.getMetamodel();
			EntityType<ImportiAnticipo> ImportiAnticipo_ = metaModel.entity(ImportiAnticipo.class);

			// filtra per dsp selezionato
			if (dataDTO.getDsp() != null && dataDTO.getDsp() != "") {
				javax.persistence.criteria.Path<String> dsp = importiAnticipo.get("idDsp");
				Predicate predicateDsp = criteriaBuilder.equal(dsp, dataDTO.getDsp());
				predicates.add(predicateDsp);
			}

			// filtra per numero fattura
			if (dataDTO.getAdvancePaymentCode() != null && !dataDTO.getAdvancePaymentCode().equals(0)) {
				javax.persistence.criteria.Path<Integer> numeroFattura = importiAnticipo.get("numeroFattura");
				Predicate predicateNumeroFattura = criteriaBuilder.equal(numeroFattura,
						dataDTO.getAdvancePaymentCode());
				predicates.add(predicateNumeroFattura);
			}

			// filtra per periodo inizio validita
			if (dataDTO.getMonthFrom() != null && dataDTO.getYearFrom() != null && dataDTO.getMonthFrom() != 0
					&& dataDTO.getYearFrom() != 0) {
				String dateFrom = "01" + "-" + dataDTO.getMonthFrom() + "-" + dataDTO.getYearFrom();
				Date parsedDateFrom = simpledateformat.parse(dateFrom);
				javax.persistence.criteria.Path<Date> datefromentity = importiAnticipo.get("periodoPertinenzaInizio");
				Predicate predicateDateFrom = criteriaBuilder.greaterThanOrEqualTo(datefromentity, parsedDateFrom);
				predicates.add(predicateDateFrom);
			}

			// filtra per periodo inizio validita
			if (dataDTO.getMonthTo() != null && dataDTO.getYearTo() != null && dataDTO.getMonthTo() != 0
					&& dataDTO.getYearTo() != 0) {
				String dateFrom = "01" + "-" + dataDTO.getMonthTo() + "-" + dataDTO.getYearTo();
				Date parsedDateTo = simpledateformat.parse(dateFrom);
				javax.persistence.criteria.Path<Date> datetoentity = importiAnticipo.get("periodoPertinenzaFine");
				Predicate predicateDateTo = criteriaBuilder.lessThanOrEqualTo(datetoentity, parsedDateTo);
				predicates.add(predicateDateTo);
			}

			createQuery.where(predicates.toArray(new Predicate[0]));

			createQuery.select(importiAnticipo);
			TypedQuery<ImportiAnticipo> q = entityManager.createQuery(createQuery).setFirstResult(dataDTO.getFirst())
					.setMaxResults(1 + dataDTO.getLast() - dataDTO.getFirst());
			resultQuery = q.getResultList();

			final PagedResult pagedResult = new PagedResult();

			if (resultQuery != null) {

				int maxrows = dataDTO.getLast() - dataDTO.getFirst();
				boolean hasNext = resultQuery.size() > maxrows;
				for (ImportiAnticipo importiAnticipoDTO : resultQuery) {
					result.add(new ImportiAnticipoDTO(importiAnticipoDTO));
				}

				pagedResult.setRows(result).setMaxrows(maxrows).setFirst(dataDTO.getFirst())
						.setLast(hasNext ? dataDTO.getLast() : dataDTO.getFirst() + resultQuery.size())
						.setHasNext(hasNext).setHasPrev(dataDTO.getFirst() > 0);

			} else {
				pagedResult.setMaxrows(0).setFirst(0).setLast(0).setHasNext(false).setHasPrev(false);
			}
			return Response.ok(gson.toJson(pagedResult)).build();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return Response.status(500).build();

	}

	@POST
	@Path("useAdvancePayment")
	public Response useAdvancePayment(UsaAnticipoDTO dataDTO) {
		List<ImportiAnticipo> resultQuery = null;
		List<CCIDMetadata> resultQueryCcidMetadata = new ArrayList<>();
		List<Predicate> predicates = new ArrayList<Predicate>();
		EntityManager entityManager = null;
		SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy");
		// anticipo da utilizzare per i ccid selezionati
		ImportiAnticipoDTO anticipo = dataDTO.getIdAdvancePayment();
		// lista dei ccid per i quali si vuole utilizzare un anticipo
		List<CcidInfoDTO> ccidinfoList = dataDTO.getCcidList();
		// lista degli anticipi associati a uno o piu ccid
		List<InvoiceItemToCcid> invoiceItemToCcids = new ArrayList<InvoiceItemToCcid>();
		BigDecimal newUsedVAlue = null;
		BigDecimal ccidResidualValue = null;
		Map<BigInteger, BigDecimal> listValoreFatturabile = new HashMap<BigInteger, BigDecimal>();

		entityManager = provider.get();
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<ImportiAnticipo> createQuery = criteriaBuilder.createQuery(ImportiAnticipo.class);
		Root<ImportiAnticipo> importiAnticipo = createQuery.from(ImportiAnticipo.class);

		if (dataDTO.getIdAdvancePayment().getIdImportoAnticipo() != null) {
			javax.persistence.criteria.Path<String> idImportoAnticipo = importiAnticipo.get("idImportoAnticipo");
			Predicate predicateDsp = criteriaBuilder.equal(idImportoAnticipo,
					dataDTO.getIdAdvancePayment().getIdImportoAnticipo());
			predicates.add(predicateDsp);
		}

		createQuery.where(predicates.toArray(new Predicate[0]));

		createQuery.select(importiAnticipo);
		TypedQuery<ImportiAnticipo> q = entityManager.createQuery(createQuery);
		resultQuery = q.getResultList();
		if (resultQuery.size() == 1) {
			BigDecimal tmpAnticipo = resultQuery.get(0).getImportoUtilizzabile();
			if (tmpAnticipo.compareTo(new BigDecimal(0)) == 0) {
				return Response.status(422).build();
			}
		}

		Invoice invoice = (Invoice) entityManager.createQuery("Select x FROM Invoice x where x.idInvoice=:idInvoice")
				.setParameter("idInvoice", resultQuery.get(0).getIdInvoice()).getSingleResult();
		// CCIDMetadata ccidMedata = (CCIDMetadata) entityManager.createQuery("Select x
		// FROM CCIDMetadata x where
		// x.idCCIDMetadata=:idCCIDMetadata").setParameter("idCCIDMetadata",
		// resultQuery.get(0).getIdInvoice()).getSingleResult();

		for (CcidInfoDTO ccidInfoDTO : ccidinfoList) {

			// valori necessari all'aggiornameto dell'anticipo
			BigDecimal ccidvalue = ccidInfoDTO.getValoreResiduo();
			BigDecimal usedvalue = anticipo.getAmountUsed();
			BigDecimal quotaAnticio = null;

			// il valore disponibile nell'anticipo è maggiroe o uguale del valore del ccid
			if (usedvalue.compareTo(ccidvalue) >= 0) {
				newUsedVAlue = usedvalue.subtract(ccidvalue);
				quotaAnticio = usedvalue.subtract(newUsedVAlue);
				ccidResidualValue = new BigDecimal(0);

				// il valore disponibile nell'anticipo è inferiore al valore del ccid
			} else if (usedvalue.compareTo(ccidvalue) < 0 && usedvalue.compareTo(new BigDecimal(0)) > 0) {
				ccidResidualValue = ccidvalue.subtract(usedvalue);
				quotaAnticio = usedvalue;
				newUsedVAlue = new BigDecimal(0);

			} else {
				break;
			}

			// aggiorna il DTO anticipo
			anticipo.setAmountUsed(newUsedVAlue);
			// crea il DTO che rappresenta la relazione tra un ccid e un anticipo

			// crea il DTO che rappresenta la relazione tra un ccid e un anticipo
			InvoiceItemToCcid invoiceItemToCcid = new InvoiceItemToCcid();
			CCIDMetadata ccidMedata = (CCIDMetadata) entityManager
					.createQuery("Select x FROM CCIDMetadata x where x.idCCIDMetadata=:idCCIDMetadata")
					.setParameter("idCCIDMetadata", ccidInfoDTO.getIdCCIDmetadata()).getSingleResult();
			invoiceItemToCcid.setCcidMedata(ccidMedata);
			for (InvoiceItem item : invoice.getInvoiceItems()) {
				invoiceItemToCcid.setInvoiceItem(item);
			}
			invoiceItemToCcid.setValore(quotaAnticio);
			invoiceItemToCcids.add(invoiceItemToCcid);

			listValoreFatturabile.put(new BigInteger(String.valueOf(ccidInfoDTO.getIdCCIDmetadata())),
					ccidResidualValue);

		}
		for (Map.Entry<BigInteger, BigDecimal> map : listValoreFatturabile.entrySet()) {
			CriteriaBuilder criteriaBuilderCCidMetadata = entityManager.getCriteriaBuilder();
			CriteriaQuery<CCIDMetadata> createQueryCCIDMetadata = criteriaBuilder.createQuery(CCIDMetadata.class);
			Root<CCIDMetadata> ccidMetadata = createQuery.from(CCIDMetadata.class);

			javax.persistence.criteria.Path<BigInteger> idCcidMetadata = ccidMetadata.get("idCCIDMetadata");
			Predicate predicateCcidMetadata = criteriaBuilderCCidMetadata.equal(idCcidMetadata, map.getKey());

			createQueryCCIDMetadata.where(predicateCcidMetadata);
			createQueryCCIDMetadata.select(ccidMetadata);
			TypedQuery<CCIDMetadata> ccidQuery = entityManager.createQuery(createQueryCCIDMetadata);
			List<CCIDMetadata> listCcidMetadata = ccidQuery.getResultList();
			CCIDMetadata tmpCcidmetadata = listCcidMetadata.get(0);
			tmpCcidmetadata.setValoreResiduo(map.getValue());
			if (map.getValue().compareTo(new BigDecimal(0)) == 0) {
				tmpCcidmetadata.setInvoiceStatus(InvoiceStatus.FATTURATO.toString());
			}
			resultQueryCcidMetadata.add(tmpCcidmetadata);
		}
		// List<CCIDMetadata> entity = entityManager.createQuery("SELECT a FROM
		// CCIDMetadata a WHERE a.idCCIDMetadata = :ID").setParameter("ID",
		// 30).getResultList();

		try {
			// inizio della transazione
			entityManager.getTransaction().begin();

			// cicla tutte le associazioni tra anticipi e ccid per persisterli nel db
			for (InvoiceItemToCcid dto : invoiceItemToCcids) {
				entityManager.persist(dto);
			}

			// aggiorna l'entity relativa agli anticipi
			ImportiAnticipo entityIA = (ImportiAnticipo) entityManager
					.createQuery("SELECT x FROM ImportiAnticipo x where x.idImportoAnticipo = :idImportoAnticipo")
					.setParameter("idImportoAnticipo", dataDTO.getIdAdvancePayment().getIdImportoAnticipo())
					.getSingleResult();

			entityIA.setImportoOriginale(anticipo.getOriginalAmount());
			entityIA.setImportoUtilizzabile(anticipo.getAmountUsed());

			entityManager.merge(entityIA);

			for (CCIDMetadata ccidMetadata : resultQueryCcidMetadata) {
				entityManager.merge(ccidMetadata);
			}

			entityManager.getTransaction().commit();

		} catch (Exception e) {

			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
			e.printStackTrace();
		}

		return Response.ok().build();
	}

	public InvoiceDTO newInvoice(InvoiceDTO dto, EntityManager entityManager) {

		List<Object[]> result;
		List<InvoiceCCIDDTO> dtoList = new ArrayList<>();
		try {
			AnagClient anagClient = (AnagClient) entityManager
					.createQuery("SELECT x FROM AnagClient x WHERE x.idDsp =:id").setParameter("id", dto.getIdDsp())
					.getSingleResult();
			Invoice invoice = new Invoice();
			Date date = new Date();
			invoice.setCreationDate(date);
			invoice.setClientId(anagClient.getIdAnagClient());
			invoice.setDateOfPertinence(new Date());
			invoice.setLastUpdateDate(date);
			invoice.setIdDsp(dto.getIdDsp());
			invoice.setTotal(dto.getTotal());
			invoice.setStatus("FATTURATO");
			invoice.setInvoiceType("ANTICIPO");
			int position = 1;

			for (InvoiceItemDto itemDao : dto.getInvoiceItems()) {
				InvoiceItem itemEntity = new InvoiceItem();
				InvoiceItemReason description = (InvoiceItemReason) entityManager
						.createQuery("SELECT x FROM InvoiceItemReason x WHERE x.idItemInvoiceReason =:id")
						.setParameter("id", 5).getSingleResult();
				itemEntity.setDescription(description);

				itemEntity.setNote(itemDao.getNote());
				itemEntity.setInvoicePosition(position);

				itemEntity.setTotal(itemDao.getTotalValue());
				itemEntity.setTotalOrigCurrency(itemDao.getTotalValueOrig());
				itemEntity.setOrigCurrency(itemDao.getCurrency());
				itemEntity.setInvoice(invoice);
				itemEntity.setDescription(description);
				invoice.addInvoiceItem(itemEntity);
				entityManager.persist(invoice);
				entityManager.persist(itemEntity);
				entityManager.flush();

				if (itemDao.getCcidList() != null && itemDao.getCcidList().size() > 0) {
					List<CCIDMetadata> ccids = entityManager
							.createQuery("SELECT x FROM CCIDMetadata x WHERE x.idCcid IN :idCcids")
							.setParameter("idCcids", itemDao.getCcidList()).getResultList();
					for (CCIDMetadata ccidMedata : ccids) {
						InvoiceItemToCcid invoiceItemToCcid = new InvoiceItemToCcid();
						invoiceItemToCcid.setCcidMedata(ccidMedata);
						invoiceItemToCcid.setInvoiceItem(itemEntity);
						invoiceItemToCcid.setValore(ccidMedata.getValoreResiduo());
						entityManager.persist(invoiceItemToCcid);
						ccidMedata.setQuotaFattura(new BigDecimal("0"));
						ccidMedata.setValoreResiduo(new BigDecimal(0));
						entityManager.merge(ccidMedata);
						entityManager.flush();
					}
				}
				position++;
			}
			dto.setIdInvoice(invoice.getIdInvoice());

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("create invoice", e);
			return null;
		}

		return dto;
	}

	@SuppressWarnings("unchecked")
	@POST
	@Path("getLinkedCcid")
	public Response getLinkedCcid(ImportiAnticipoDTO dto) {
		EntityManager entityManager = null;
		logger.info("getLinkedCcid: " + dto.toString());
		List<DettailImportiAnticipo> resultlist = new ArrayList<>();

		try {
			// recupera istanza dell'entity manager
			entityManager = provider.get();

			ImportiAnticipo importiAnticipo = (ImportiAnticipo) entityManager
					.createQuery("SELECT x FROM ImportiAnticipo x where x.idImportoAnticipo = :idImportoAnticipo")
					.setParameter("idImportoAnticipo", dto.getIdImportoAnticipo()).getSingleResult();
			Invoice invoice = (Invoice) entityManager
					.createQuery("Select x from Invoice x where x.idInvoice = :idInvoice")
					.setParameter("idInvoice", importiAnticipo.getIdInvoice()).getSingleResult();

			for (InvoiceItemToCcid invoiceItemToCcid : invoice.getInvoiceItems().get(0).getInvoiceItemToCcid()) {

				DettailImportiAnticipo dettailImportiAnticipo = new DettailImportiAnticipo(
						importiAnticipo.getIdImportoAnticipo(), importiAnticipo.getPeriodoPertinenzaInizio(),
						importiAnticipo.getPeriodoPertinenzaFine(), importiAnticipo.getNumeroFattura(),
						importiAnticipo.getImportoOriginale(), importiAnticipo.getImportoUtilizzabile(),
						importiAnticipo.getIdDsp(), invoiceItemToCcid.getValore(),
						invoiceItemToCcid.getIdCcidMedata().getIdCcid(), invoiceItemToCcid.getIdCcidMedata().getIdDsr(),
						invoiceItemToCcid.getIdCcidMedata().getValoreFatturabile(),
						invoiceItemToCcid.getIdCcidMedata().getTotalValue());
				resultlist.add(dettailImportiAnticipo);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return Response.ok(gson.toJson(resultlist)).build();
	}
}
