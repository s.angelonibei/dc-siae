package com.alkemytech.sophia.codman.entity;

import com.alkemytech.sophia.codman.entity.performing.PerfCombana;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "FONTE_PRIMA_UTILIZZAZIONE")
public class FontePrimaUtilizzazione {

    @Id
    @Column(name = "ID_FONTE_PRIMA_UTILIZZAZIONE")
    private int idFontePrimaUtilizzazione;

    @Column(name = "DESCRIZIONE")
    private String descrizione;

    @JsonManagedReference
    @OneToMany(mappedBy="fontePrimaUtilizzazione")
    private List<PerfCombana> perfCombanas;

    public int getIdFontePrimaUtilizzazione() {
        return idFontePrimaUtilizzazione;
    }

    public void setIdFontePrimaUtilizzazione(int idFontePrimaUtilizzazione) {
        this.idFontePrimaUtilizzazione = idFontePrimaUtilizzazione;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public List<PerfCombana> getPerfCombanas() {
        return perfCombanas;
    }

    public void setPerfCombanas(List<PerfCombana> perfCombanas) {
        this.perfCombanas = perfCombanas;
    }
}
