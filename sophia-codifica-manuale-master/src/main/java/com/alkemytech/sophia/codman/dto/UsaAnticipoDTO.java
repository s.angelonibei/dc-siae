package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;
import java.util.List;

import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;

@Produces("application/json")
@XmlRootElement
public class UsaAnticipoDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private ImportiAnticipoDTO idAdvancePayment;
	private List<CcidInfoDTO> ccidList;
	
	
	public ImportiAnticipoDTO getIdAdvancePayment() {
		return idAdvancePayment;
	}
	public void setIdAdvancePayment(ImportiAnticipoDTO idAdvancePayment) {
		this.idAdvancePayment = idAdvancePayment;
	}
	public List<CcidInfoDTO> getCcidList() {
		return ccidList;
	}
	public void setCcidList(List<CcidInfoDTO> ccidList) {
		this.ccidList = ccidList;
	}
	
	
	
}
