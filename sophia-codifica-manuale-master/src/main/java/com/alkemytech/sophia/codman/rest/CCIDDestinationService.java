package com.alkemytech.sophia.codman.rest;

import com.alkemytech.sophia.codman.dto.DestinazioneCcidDTO;
import com.alkemytech.sophia.codman.dto.DspConfigDTO;
import com.alkemytech.sophia.codman.entity.CommercialOffers;
import com.alkemytech.sophia.codman.entity.DspConfig;
import com.alkemytech.sophia.codman.entity.MMDestinazioneCCID;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.util.ArrayList;
import java.util.List;

@Singleton
@Path("ccid-destination")
public class CCIDDestinationService {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Provider<EntityManager> provider;
	private final Gson gson;

	@Inject
	protected CCIDDestinationService(@McmdbDataSource Provider<EntityManager> provider, Gson gson) {
		super();
		this.provider = provider;
		this.gson = gson;
	}

	@GET
	@Path("allConfig")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response all(@DefaultValue("0") @QueryParam("first") int first,
			@DefaultValue("50") @QueryParam("last") int last, @DefaultValue("") @QueryParam("dsp") String dsp,
			@DefaultValue("") @QueryParam("utilization") String utilization,
			@DefaultValue("") @QueryParam("offer") String offer) {

		EntityManager entityManager = null;
		List<Object[]> result;
		try {

            entityManager = provider.get();
		    
			String sql = "SELECT * FROM " +
			"(SELECT MM_DESTINAZIONE_INVIO_CCID.ID,\n " +
					"		c.HOST," +
					" 		c.USER, " +
					"       MM_DESTINAZIONE_INVIO_CCID.IDDSP                                                                                       IDDSP,\n" +
					"       ANAG_DSP.NAME                                                                                                       as DSP_NAME,\n" +
					"       MM_DESTINAZIONE_INVIO_CCID.UTILIZATION_TYPE                                                                         as ID_UTILIZATION,\n" +
					"       (IF(MM_DESTINAZIONE_INVIO_CCID.UTILIZATION_TYPE = '*', '*', (SELECT NAME\n" +
					"                                                                    FROM ANAG_UTILIZATION_TYPE\n" +
					"                                                                    WHERE ANAG_UTILIZATION_TYPE.ID_UTILIZATION_TYPE =\n" +
					"                                                                          MM_DESTINAZIONE_INVIO_CCID.UTILIZATION_TYPE)))   as UTILIZATION_NAME,\n" +
                    "       MM_DESTINAZIONE_INVIO_CCID.COMMERCIAL_OFFERS   as ID_OFFER,\n" +
					"       (IF(MM_DESTINAZIONE_INVIO_CCID.COMMERCIAL_OFFERS = '*', '*', (SELECT OFFERING\n" +
					"                                                                     FROM COMMERCIAL_OFFERS\n" +
					"                                                                     WHERE COMMERCIAL_OFFERS.ID_COMMERCIAL_OFFERS =\n" +
                    "                                                                           MM_DESTINAZIONE_INVIO_CCID.COMMERCIAL_OFFERS))) as OFFER_NAME,\n" +
                   
					"       MM_DESTINAZIONE_INVIO_CCID.PATH,\n" +
					"       MM_DESTINAZIONE_INVIO_CCID.RANK\n" +
					"FROM ANAG_DSP,\n" +
					"     MM_DESTINAZIONE_INVIO_CCID" +
					"     left join MM_MODALITA_INVIO_CCID c on MM_DESTINAZIONE_INVIO_CCID.IDDSP = c.IDDSP  \n" +
					"WHERE MM_DESTINAZIONE_INVIO_CCID.IDDSP = ANAG_DSP.IDDSP) x "
					+ ((!dsp.isEmpty() && !dsp.equals("*")) || (!utilization.isEmpty() && !utilization.equals("*"))
							|| (!offer.isEmpty() && !offer.equals("*")) ? " where " : "")

					+ (!dsp.isEmpty() && !dsp.equals("*") ? " x.IDDSP = ? " : "")
					+ (!dsp.isEmpty() && !dsp.equals("*") && !utilization.isEmpty() && !utilization.equals("*")
							? " and " : "")
					
					+ (!utilization.isEmpty() && !utilization.equals("*")
							? "  ( ID_UTILIZATION = ? or (x.ID_UTILIZATION = '*'  and " +
                         " IDDSP in (select distinct IDDSP from COMMERCIAL_OFFERS where COMMERCIAL_OFFERS.ID_UTIILIZATION_TYPE = ?))) " : "")
					+ (((!utilization.isEmpty() && !utilization.equals("*")) ||(!dsp.isEmpty() && !dsp.equals("*"))) && !offer.isEmpty() && !offer.equals("*")
							? " and " : "")
					+ (!offer.isEmpty() && !offer.equals("*") ? "  ( ID_OFFER = ? or (ID_OFFER='*'  and " +
                         " IDDSP = (select  IDDSP from COMMERCIAL_OFFERS where ID_COMMERCIAL_OFFERS = ?)  and " +
                         " (ID_UTILIZATION = (select  ID_UTIILIZATION_TYPE from COMMERCIAL_OFFERS where ID_COMMERCIAL_OFFERS = ?) or ID_UTILIZATION = '*')))   " : "")
					+ "order by x. DSP_NAME, x.UTILIZATION_NAME, x.OFFER_NAME";


			
			final Query q = entityManager.createNativeQuery(sql).setFirstResult(first) // offset
					.setMaxResults(1 + last - first);
			
			int i = 1;
			if (!dsp.isEmpty() && !dsp.equals("*")) {
				q.setParameter(i++, dsp);
			}
			if (!utilization.isEmpty() && !utilization.equals("*")) {
				q.setParameter(i++, utilization);
				q.setParameter(i++, utilization);
			}
			if (!offer.isEmpty() && !offer.equals("*")) {
				q.setParameter(i++, offer);
				q.setParameter(i++, offer);
				q.setParameter(i++, offer);
			}
			
			result = (List<Object[]>) q.getResultList();
			List<DestinazioneCcidDTO> dtoList = new ArrayList<DestinazioneCcidDTO>();
			final PagedResult pagedResult = new PagedResult();
			if (null != result) {
				final int maxrows = last - first;
				final boolean hasNext = result.size() > maxrows;
				for (Object[] p : (hasNext ? result.subList(0, maxrows) : result)) {
					dtoList.add(new DestinazioneCcidDTO(p));
				}
				pagedResult.setRows(dtoList).setMaxrows(maxrows).setFirst(first)
						.setLast(hasNext ? last : first + result.size()).setHasNext(hasNext).setHasPrev(first > 0);
			} else {
				pagedResult.setMaxrows(0).setFirst(0).setLast(0).setHasNext(false).setHasPrev(false);
			}
			return Response.ok(gson.toJson(pagedResult)).build();
		} catch (Exception e) {
			logger.error("all", e);
		}
		return Response.status(500).build();
	}

	@POST
	@Path("")
	@Produces("application/json")
	public Response add(DestinazioneCcidDTO config) {
		
		
		
		
		
		MMDestinazioneCCID dspConfig = new MMDestinazioneCCID();
		dspConfig.setIdDSP(config.getIdDsp());
		dspConfig.setUtilizationType(config.getIdUtilizationType() == null ? "*" : config.getIdUtilizationType());
		dspConfig.setCommercialOffers(config.getIdCommercialOffer() == null ? "*" : config.getIdCommercialOffer());
		dspConfig.setPath(config.getUrl() == null ? "" : config.getUrl());
		dspConfig.setRank(config.getRank()+"");
		
		Status status = Status.INTERNAL_SERVER_ERROR;
		final EntityManager entityManager = provider.get();
		try {
			entityManager.getTransaction().begin();
			entityManager.persist(dspConfig);

			entityManager.getTransaction().commit();
			return Response.ok().build();
		} catch (Exception e) {
			logger.error("add", e);

			if (e.getCause() != null && e.getCause().getCause() != null
					&& e.getCause().getCause() instanceof MySQLIntegrityConstraintViolationException) {
				status = Status.CONFLICT;
			}

			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}

		}

		return Response.status(status).build();
	}

	@DELETE
	@Path("")
	public Response delete(DestinazioneCcidDTO config) {
		
		final EntityManager entityManager = provider.get();
		try {
			entityManager.getTransaction().begin();
			final Query query = entityManager.createNativeQuery("delete from MM_DESTINAZIONE_INVIO_CCID where ID = ? ");
			query.setParameter(1, config.getId());
			
			query.executeUpdate();
			entityManager.getTransaction().commit();
			return Response.ok().build();
		} catch (Exception e) {
			logger.error("delete", e);
			entityManager.getTransaction().rollback();
		}
		return Response.status(500).build();
	}

	
	
	@GET
	@Path("allAvailableOffers")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response allAvailableOffers() {
		EntityManager entityManager = null;
		List<CommercialOffers> result;
		try {
			entityManager = provider.get();
			final Query q = entityManager.createQuery(
					"select x from CommercialOffers x");
			result = (List<CommercialOffers>) q.getResultList();
			return Response.ok(gson.toJson(result)).build();
		} catch (Exception e) {
			logger.error("allAvailableOffers", e);
		}
		return Response.status(500).build();
	}

}
