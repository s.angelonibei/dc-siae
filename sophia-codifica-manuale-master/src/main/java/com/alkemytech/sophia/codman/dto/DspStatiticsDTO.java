package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.common.tools.StringTools;

@Produces("application/json")
@XmlRootElement
public class DspStatiticsDTO implements Serializable {

	private static final long serialVersionUID = 1911294021205283360L;

	private static final Logger logger = LoggerFactory.getLogger(DspStatiticsDTO.class);

	private String dsp;
	private String dsr;
	private String dsrDeliveryDate;
	private String ccidProductionDate;
	private String periodType;
	private Integer periodNumber;
	private Integer year;
	private String period;
	private String country;
	private String utilizationType;
	private String commercialOffer;
	private BigDecimal totValue;
	private String currency;
	private BigDecimal totUsage;
	private BigDecimal valueIdentified;
	private Float usageIdentified;
	private BigDecimal siaeValueIdentified;
	private BigDecimal siaeIdentifiedVisualizations;
	private BigDecimal valueNotIdentified;
	private Long visualizationsNotIdentified;
	private Integer ccidS3Id;
	private String ccidS3Path;
	private BigDecimal valoreFatturabile;
	private BigDecimal identificatoValorePricing;
	private BigDecimal totalValueCcidCurrency;
	private String ccidCurrency;
	private Long backclaim;
	private String societaTutela;
	private String ccidName;
	private String extendedDsrUrl;
	private String tipoBc;
	private Date dateFrom;
	private Date dateTo;

	private Boolean percClaim;

	private boolean hasDrilldown;
	
	public DspStatiticsDTO() {};

	public DspStatiticsDTO(Object[] obj) {
		if (null != obj && obj.length == 40) {
			try {
				dsp = (String) obj[0];
				dsr = (String) obj[1];
				dsrDeliveryDate = (String) obj[2];
				periodType = (String) obj[3];
				periodNumber = (Integer) obj[4];
				year = (Integer) obj[5];
				period = (String) obj[6];
				country = (String) obj[7];
				utilizationType = (String) obj[8];
				commercialOffer = (String) obj[9];
				totValue = (BigDecimal)(obj[10]);
				currency = (String) obj[11];
				totUsage = (BigDecimal)(obj[12]);

				usageIdentified = toFLoat(obj[13]);
				siaeValueIdentified = obj[32] != null ? (BigDecimal) obj[32] : null;
				ccidCurrency = (String) obj[21];

				valueIdentified = obj[32] != null ? (BigDecimal) obj[32] : null;
				siaeIdentifiedVisualizations = obj[35] != null ? (BigDecimal) obj[35] : null;
				valueNotIdentified = obj[34] != null ? (BigDecimal) obj[34] : null;
				visualizationsNotIdentified = (Long) obj[31];

				ccidS3Id = (Integer) obj[14];
				ccidS3Path = (String) obj[15];
				valoreFatturabile = (BigDecimal) obj[28];
				identificatoValorePricing = (BigDecimal) obj[38];
				totalValueCcidCurrency = (BigDecimal) obj[39];

			} catch (Throwable e) {
				logger.error("<init>", e);
			}
		} if (null != obj && obj.length == 41) {
			try {
				dsp = (String) obj[0];
				dsr = (String) obj[1];
				dsrDeliveryDate = (String) obj[2];
				periodType = (String) obj[3];
				periodNumber = (Integer) obj[4];
				year = (Integer) obj[5];
				period = (String) obj[6];
				country = (String) obj[7];
				utilizationType = (String) obj[8];
				commercialOffer = (String) obj[9];
				totValue = (BigDecimal)(obj[10]);
				currency = (String) obj[11];
				totUsage = (BigDecimal)(obj[12]);
				backclaim = (Long) obj[14];

				usageIdentified = toFLoat(obj[13]);
				siaeValueIdentified = obj[33] != null ? (BigDecimal) obj[33] : null;
				ccidCurrency = (String) obj[22];

				valueIdentified = obj[33] != null ? (BigDecimal) obj[33] : null;
				siaeIdentifiedVisualizations = obj[36] != null ? (BigDecimal) obj[36] : null;
				valueNotIdentified = obj[35] != null ? (BigDecimal) obj[35] : null;
				visualizationsNotIdentified = (Long) obj[32];

				ccidS3Id = (Integer) obj[15];
				ccidS3Path = (String) obj[16];
				valoreFatturabile = (BigDecimal) obj[29];
				identificatoValorePricing = (BigDecimal) obj[39];
				totalValueCcidCurrency = (BigDecimal) obj[40];
			} catch (Throwable e) {
				logger.error("<init>", e);
			}
		}		
		if (null != obj && obj.length == 41) {
			try {
				dsp = (String) obj[0];
				dsr = (String) obj[1];
				dsrDeliveryDate = (String) obj[2];
				periodType = (String) obj[3];
				periodNumber = (Integer) obj[4];
				year = (Integer) obj[5];
				period = (String) obj[6];
				country = (String) obj[7];
				utilizationType = (String) obj[8];
				commercialOffer = (String) obj[9];
				totValue = (BigDecimal)(obj[10]);
				currency = (String) obj[11];
				totUsage = (BigDecimal)(obj[12]);

				usageIdentified = toFLoat(obj[13]);
				ccidProductionDate =(String) obj[15];
				siaeValueIdentified = obj[33] != null ? (BigDecimal) obj[33] : null;
				ccidCurrency = (String) obj[22];

				valueIdentified = obj[33] != null ? (BigDecimal) obj[33] : null;
				siaeIdentifiedVisualizations = obj[36] != null ? (BigDecimal) obj[36] : null;
				valueNotIdentified = obj[35] != null ? (BigDecimal) obj[35] : null;
				visualizationsNotIdentified = (Long) obj[32];

				ccidS3Id = (Integer) obj[15];
				ccidS3Path = (String) obj[16];
				valoreFatturabile = (BigDecimal) obj[29];
				identificatoValorePricing = (BigDecimal) obj[39];
				totalValueCcidCurrency = (BigDecimal) obj[40];

			} catch (Throwable e) {
				logger.error("<init>", e);
			}
		} if (null != obj && obj.length == 42) {
			try {
				dsp = (String) obj[0];
				dsr = (String) obj[1];
				dsrDeliveryDate = (String) obj[2];
				periodType = (String) obj[3];
				periodNumber = (Integer) obj[4];
				year = (Integer) obj[5];
				period = (String) obj[6];
				country = (String) obj[7];
				utilizationType = (String) obj[8];
				commercialOffer = (String) obj[9];
				totValue = (BigDecimal)(obj[10]);
				currency = (String) obj[11];
				totUsage = (BigDecimal)(obj[12]);
				backclaim = (Long) obj[14];
				ccidProductionDate =(String) obj[15];

				usageIdentified = toFLoat(obj[13]);
				siaeValueIdentified = obj[34] != null ? (BigDecimal) obj[34] : null;
				ccidCurrency = (String) obj[23];

				valueIdentified = obj[34] != null ? (BigDecimal) obj[34] : null;
				siaeIdentifiedVisualizations = obj[37] != null ? (BigDecimal) obj[37] : null;
				valueNotIdentified = obj[36] != null ? (BigDecimal) obj[36] : null;
				visualizationsNotIdentified = (Long) obj[33];

				ccidS3Id = (Integer) obj[16];
				ccidS3Path = (String) obj[17];
				valoreFatturabile = (BigDecimal) obj[30];
				identificatoValorePricing = (BigDecimal) obj[40];
				totalValueCcidCurrency = (BigDecimal) obj[41];
			} catch (Throwable e) {
				logger.error("<init>", e);
			}
		}
	}

	public static Float toFLoat(Object obj) {
		try {
			String str = (String) obj;
			str = str.trim();
			if (!StringTools.isNullOrEmpty(str)) {
				return Float.valueOf(str);
			}
		} catch (Exception e) {
		}
		return null;
	}

	public String getDsp() {
		return dsp;
	}

	public void setDsp(String dsp) {
		this.dsp = dsp;
	}

	public String getDsr() {
		return dsr;
	}

	public void setDsr(String dsr) {
		this.dsr = dsr;
	}

	public String getDsrDeliveryDate() {
		return dsrDeliveryDate;
	}

	public void setDsrDeliveryDate(String dsrDeliveryDate) {
		this.dsrDeliveryDate = dsrDeliveryDate;
	}

	public String getPeriodType() {
		return periodType;
	}

	public void setPeriodType(String periodType) {
		this.periodType = periodType;
	}

	public Integer getPeriodNumber() {
		return periodNumber;
	}

	public void setPeriodNumber(Integer periodNumber) {
		this.periodNumber = periodNumber;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getUtilizationType() {
		return utilizationType;
	}

	public void setUtilizationType(String utilizationType) {
		this.utilizationType = utilizationType;
	}

	public String getCommercialOffer() {
		return commercialOffer;
	}

	public void setCommercialOffer(String commercialOffer) {
		this.commercialOffer = commercialOffer;
	}

	public BigDecimal getTotValue() {
		return totValue;
	}

	public String getTipoBc() {
		return tipoBc;
	}

	public void setTipoBc(String tipoBc) {
		this.tipoBc = tipoBc;
	}

	public void setTotValue(BigDecimal totValue) {
		this.totValue = totValue;
	}

	public BigDecimal getTotUsage() {
		return totUsage;
	}

	public void setTotUsage(BigDecimal totUsage) {
		this.totUsage = totUsage;
	}

	public BigDecimal getValueIdentified() {
		return valueIdentified;
	}

	public void setValueIdentified(BigDecimal valueIdentified) {
		this.valueIdentified = valueIdentified;
	}

	public Float getUsageIdentified() {
		return usageIdentified;
	}

	public void setUsageIdentified(Float usageIdentified) {
		this.usageIdentified = usageIdentified;
	}

	public BigDecimal getSiaeValueIdentified() {
		return siaeValueIdentified;
	}

	public void setSiaeValueIdentified(BigDecimal siaeValueIdentified) {
		this.siaeValueIdentified = siaeValueIdentified;
	}

	public BigDecimal getSiaeIdentifiedVisualizations() {
		return siaeIdentifiedVisualizations;
	}

	public void setSiaeIdentifiedVisualizations(BigDecimal siaeIdentifiedVisualizations) {
		this.siaeIdentifiedVisualizations = siaeIdentifiedVisualizations;
	}

	public BigDecimal getValueNotIdentified() {
		return valueNotIdentified;
	}

	public void setValueNotIdentified(BigDecimal valueNotIdentified) {
		this.valueNotIdentified = valueNotIdentified;
	}

	public Long getVisualizationsNotIdentified() {
		return visualizationsNotIdentified;
	}

	public void setVisualizationsNotIdentified(Long visualizationsNotIdentified) {
		this.visualizationsNotIdentified = visualizationsNotIdentified;
	}

	public Integer getCcidS3Id() {
		return ccidS3Id;
	}

	public void setCcidS3Id(Integer ccidS3Id) {
		this.ccidS3Id = ccidS3Id;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Logger getLogger() {
		return logger;
	}

	public String getCcidS3Path() {
		return ccidS3Path;
	}

	public void setCcidS3Path(String ccidS3Path) {
		this.ccidS3Path = ccidS3Path;
	}

	public BigDecimal getValoreFatturabile() {
		return valoreFatturabile;
	}

	public void setValoreFatturabile(BigDecimal valoreFatturabile) {
		this.valoreFatturabile = valoreFatturabile;
	}

	public BigDecimal getIdentificatoValorePricing() {
		return identificatoValorePricing;
	}

	public void setIdentificatoValorePricing(BigDecimal identificatoValorePricing) {
		this.identificatoValorePricing = identificatoValorePricing;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public BigDecimal getTotalValueCcidCurrency() {
		return totalValueCcidCurrency;
	}

	public void setTotalValueCcidCurrency(BigDecimal totalValueCcidCurrency) {
		this.totalValueCcidCurrency = totalValueCcidCurrency;
	}

	public String getCcidCurrency() {
		return ccidCurrency;
	}

	public void setCcidCurrency(String ccidCurrency) {
		this.ccidCurrency = ccidCurrency;
	}

	public Long getBackclaim() {
		return backclaim;
	}

	public void setBackclaim(Long backclaim) {
		this.backclaim = backclaim;
	}

	public String getCcidProductionDate() {
		return ccidProductionDate;
	}

	public void setCcidProductionDate(String ccidProductionDate) {
		this.ccidProductionDate = ccidProductionDate;
	}

	public String getSocietaTutela() {
		return societaTutela;
	}

	public void setSocietaTutela(String societaTutela) {
		this.societaTutela = societaTutela;
	}

	public String getCcidName() {
		return ccidName;
	}

	public void setCcidName(String ccidName) {
		this.ccidName = ccidName;
	}

	public String getExtendedDsrUrl() {
		return extendedDsrUrl;
	}

	public void setExtendedDsrUrl(String extendedDsrUrl) {
		this.extendedDsrUrl = extendedDsrUrl;
	}

	public Boolean isPercClaim() {
		return percClaim;
	}

	public void setPercClaim(Boolean percClaim) {
		this.percClaim = percClaim;
	}

	public boolean isHasDrilldown() {
		return hasDrilldown;
	}

	public void setHasDrilldown(boolean hasDrilldown) {
		this.hasDrilldown = hasDrilldown;
	}
	
	public void setHasDrilldown(Integer hasDrilldown) {
		this.hasDrilldown = hasDrilldown.compareTo(1)==0;
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public DspStatiticsDTO setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
		return this;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public DspStatiticsDTO setDateTo(Date dateTo) {
		this.dateTo = dateTo;
		return this;
	}
}
