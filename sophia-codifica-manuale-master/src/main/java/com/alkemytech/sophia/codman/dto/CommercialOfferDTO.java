package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;

public class CommercialOfferDTO implements Serializable {

	private static final long serialVersionUID = 135320687253368125L;

	private Integer idCommercialOffer;
	private String idDsp;
	private String dspName;
	private String idUtilization;
	private String utilizationName;
	private String offer;
	private String ccidServiceTypeCode;
	private String ccidServiceTypeDescription;
	private String ccidUseTypeCode;
	private String ccidUseTypeDescription;
	private String ccidAppliedTariffCode;
	private String ccidAppliedTariffDescription;
	private String tradingBrand;

	public CommercialOfferDTO() {
	}

	public CommercialOfferDTO(Object[] obj) {

		if (null != obj && obj.length == 13) {
			try {
				idCommercialOffer = (Integer) obj[0];
				idDsp = (String) obj[1];
				dspName = (String) obj[2];
				idUtilization = (String) obj[3];
				utilizationName = (String) obj[4];
				offer = (String) obj[5];
				ccidServiceTypeCode = (String) obj[6];
				ccidServiceTypeDescription = (String) obj[7] ;
				ccidUseTypeCode = (String) obj[8];
				ccidUseTypeDescription = (String) obj[9];
				ccidAppliedTariffCode = (String) obj[10];
				ccidAppliedTariffDescription = (String) obj[11];
				tradingBrand = (String) obj[12];
				

			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}
	
	public Integer getIdCommercialOffer() {
		return idCommercialOffer;
	}
	public void setIdCommercialOffer(Integer idCommercialOffer) {
		this.idCommercialOffer = idCommercialOffer;
	}
	public String getIdDsp() {
		return idDsp;
	}
	public void setIdDsp(String idDsp) {
		this.idDsp = idDsp;
	}
	public String getDspName() {
		return dspName;
	}
	public void setDspName(String dspName) {
		this.dspName = dspName;
	}
	public String getIdUtilization() {
		return idUtilization;
	}
	public void setIdUtilization(String idUtilization) {
		this.idUtilization = idUtilization;
	}
	public String getUtilizationName() {
		return utilizationName;
	}
	public void setUtilizationName(String utilizationName) {
		this.utilizationName = utilizationName;
	}
	public String getOffer() {
		return offer;
	}
	public void setOffer(String offer) {
		this.offer = offer;
	}

	public String getCcidServiceTypeCode() { return ccidServiceTypeCode; }

	public void setCcidServiceTypeCode(String ccidServiceTypeCode) { this.ccidServiceTypeCode = ccidServiceTypeCode; }

	public String getCcidServiceTypeDescription() { return ccidServiceTypeDescription; 	}

	public void setCcidServiceTypeDescription(String ccidServiceTypeDescription) { this.ccidServiceTypeDescription = ccidServiceTypeDescription; }

	public String getCcidUseTypeCode() { return ccidUseTypeCode; }

	public void setCcidUseTypeCode(String ccidUseTypeCode) { this.ccidUseTypeCode = ccidUseTypeCode; }

	public String getCcidUseTypeDescription() { return ccidUseTypeDescription; }

	public void setCcidUseTypeDescription(String ccidUseTypeDescription) { this.ccidUseTypeDescription = ccidUseTypeDescription; }

	public String getCcidAppliedTariffCode() { return ccidAppliedTariffCode; }

	public void setCcidAppliedTariffCode(String ccidAppliedTariffCode) { this.ccidAppliedTariffCode = ccidAppliedTariffCode; }

	public String getCcidAppliedTariffDescription() { return ccidAppliedTariffDescription; }

	public void setCcidAppliedTariffDescription(String ccidAppliedTariffDescription) { this.ccidAppliedTariffDescription = ccidAppliedTariffDescription; }

	public String getTradingBrand() { return tradingBrand; }

	public void setTradingBrand(String tradingBrand) { this.tradingBrand = tradingBrand; }
}
