package com.alkemytech.sophia.codman.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.GsonBuilder;

@XmlRootElement
@Entity(name="ClusterUse")
@Table(name="CLUSTER_USE")
public class ClusterUse {

	@Id
	@Column(name="IDCLUSTER", nullable=false)
	private String idCluster;

	@Column(name="USE_TYPE", nullable=false)
	private String useType;

	public String getIdCluster() {
		return idCluster;
	}

	public void setIdCluster(String idCluster) {
		this.idCluster = idCluster;
	}

	public String getUseType() {
		return useType;
	}

	public void setUseType(String useType) {
		this.useType = useType;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}
}
