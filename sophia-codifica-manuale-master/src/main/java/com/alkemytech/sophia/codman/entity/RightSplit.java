package com.alkemytech.sophia.codman.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.GsonBuilder;

@XmlRootElement
@Entity(name="RightSplit")
@Table(name="RIGHT_SPLIT")
public class RightSplit {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID", nullable=false)
	private Long id;

	@Column(name="COUNTRY", nullable=false)
	private String country;
	
	@Column(name="ID_UTILIZATION_TYPE", nullable=false)
	private String idUtilizationType;

	@Column(name="VALID_FROM", nullable=false)
	private Date validFrom;

	@Column(name="DEM", nullable=false)
	private BigDecimal dem;

	@Column(name="DRM", nullable=false)
	private BigDecimal drm;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getIdUtilizationType() {
		return idUtilizationType;
	}

	public void setIdUtilizationType(String idUtilizationType) {
		this.idUtilizationType = idUtilizationType;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public BigDecimal getDem() {
		return dem;
	}

	public void setDem(BigDecimal dem) {
		this.dem = dem;
	}

	public BigDecimal getDrm() {
		return drm;
	}

	public void setDrm(BigDecimal drm) {
		this.drm = drm;
	}
	
	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}
}
