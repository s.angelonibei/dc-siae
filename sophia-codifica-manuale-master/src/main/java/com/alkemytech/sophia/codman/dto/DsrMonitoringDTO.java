package com.alkemytech.sophia.codman.dto;

import com.alkemytech.sophia.codman.rest.PagedResult;

import java.util.List;
import java.util.Map;

public class DsrMonitoringDTO {
    
    private PagedResult data;
    
    private Map<String,String> extra;


    public PagedResult getData() {
        return data;
    }

    public void setData(PagedResult data) {
        this.data = data;
    }

    public Map<String, String> getExtra() {
        return extra;
    }

    public void setExtra(Map<String, String> extra) {
        this.extra = extra;
    }
}
