package com.alkemytech.sophia.codman.rest.performing.service;

import com.alkemytech.sophia.performing.model.PerfMusicProvider;
import com.alkemytech.sophia.performing.model.PerfPalinsesto;
import com.alkemytech.sophia.performing.dto.FileArmonizzatoDTO;

import java.util.List;

public interface FileArmonizzatoService {
    List<FileArmonizzatoDTO> getFileArmonizzato (PerfMusicProvider musicProvider, List<PerfPalinsesto> palinsesti, Integer anno, List<Integer> mesi);
}
