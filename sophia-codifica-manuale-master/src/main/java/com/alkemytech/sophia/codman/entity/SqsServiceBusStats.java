package com.alkemytech.sophia.codman.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity(name="SqsServiceBusStats")
@Table(name="SQS_SERVICE_BUS")
@NamedNativeQuery(
	name="SqsServiceBusStats.FindAll", 
	query="select QUEUE_NAME, count(*) QUEUE_SIZE from SQS_SERVICE_BUS where INSERT_TIME > (CURRENT_TIMESTAMP - INTERVAL 30 DAY)  group by QUEUE_NAME order by QUEUE_NAME", 
	resultClass=SqsServiceBusStats.class)
public class SqsServiceBusStats {

	@Id
	@Column(name="QUEUE_NAME", nullable=false)
	private String queueName;
	
	@Column(name="QUEUE_SIZE", nullable=false)
	private Long queueSize;

	public String getQueueName() {
		return queueName;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

	public Long getQueueSize() {
		return queueSize;
	}

	public void setQueueSize(Long queueSize) {
		this.queueSize = queueSize;
	}
	
}
