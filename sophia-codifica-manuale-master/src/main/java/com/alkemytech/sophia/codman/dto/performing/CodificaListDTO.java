package com.alkemytech.sophia.codman.dto.performing;

import java.util.ArrayList;
import java.util.List;

public class CodificaListDTO {
	private String nCandidate;
	private List<CodificaDto> codifiche;
	private List<String> extraFieldsToShow = new ArrayList<String>();
	
	public CodificaListDTO() {
		super();
	}

	public CodificaListDTO(String nCandidate, List<CodificaDto> codifiche) {
		super();
		this.nCandidate = nCandidate;
		this.codifiche = codifiche;
	}

	public String getnCandidate() {
		return nCandidate;
	}

	public void setnCandidate(String nCandidate) {
		this.nCandidate = nCandidate;
	}

	public List<CodificaDto> getCodifiche() {
		return codifiche;
	}

	public void setCodifiche(List<CodificaDto> codifiche) {
		this.codifiche = codifiche;
	}

	public List<String> getExtraFieldsToShow() {
		return extraFieldsToShow;
	}

	public void setExtraFieldsToShow(List<String> extraFieldsToShow) {
		this.extraFieldsToShow = extraFieldsToShow;
	}
	
}
