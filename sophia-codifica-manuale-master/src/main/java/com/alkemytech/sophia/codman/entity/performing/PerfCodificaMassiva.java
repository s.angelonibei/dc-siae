package com.alkemytech.sophia.codman.entity.performing;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "PERF_CODIFICA_MASSIVA")
public class PerfCodificaMassiva {

	private Long id;
    private String receivedMessage;
    private String state;
    private Timestamp insertTime;
    private Timestamp endWorkTime;
    private String utente;
    private String fileName;
    private String fileInput;
    private String fileOutput;
    private String token;
    private String errorMessage;


    @Id
    @Column(name="ID", nullable=false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "RECEIVED_MESSAGE")
    public String getReceivedMessage() {
        return receivedMessage;
    }

    public void setReceivedMessage(String receivedMessage) {
        this.receivedMessage = receivedMessage;
    }

    @Basic
    @Column(name = "STATE")
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Basic
    @Column(name = "INSERT_TIME")
    public Timestamp getInsertTime() {
        return insertTime;
    }

    public void setInsertTime(Timestamp insertTime) {
        this.insertTime = insertTime;
    }

    @Basic
    @Column(name = "END_WORK_TIME")
    public Timestamp getEndWorkTime() {
        return endWorkTime;
    }

    public void setEndWorkTime(Timestamp endWorkTime) {
        this.endWorkTime = endWorkTime;
    }

    @Basic
    @Column(name = "UTENTE")
    public String getUtente() {
        return utente;
    }

    public void setUtente(String utente) {
        this.utente = utente;
    }
    
    @Basic
    @Column(name = "FILE_NAME")
    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Basic
    @Column(name = "FILE_INPUT")
    public String getFileInput() {
        return fileInput;
    }

    public void setFileInput(String fileInput) {
        this.fileInput = fileInput;
    }

    @Basic
    @Column(name = "FILE_OUTPUT")
    public String getFileOutput() {
        return fileOutput;
    }

    public void setFileOutput(String fileOutput) {
        this.fileOutput = fileOutput;
    }

    @Basic
    @Column(name = "TOKEN")
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token= token;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PerfCodificaMassiva that = (PerfCodificaMassiva) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(receivedMessage, that.receivedMessage) &&
                Objects.equals(state, that.state) &&
                Objects.equals(insertTime, that.insertTime) &&
                Objects.equals(endWorkTime, that.endWorkTime) &&
                Objects.equals(utente, that.utente) &&
                Objects.equals(fileName, that.fileName) &&
                Objects.equals(fileInput, that.fileInput) &&
                Objects.equals(fileOutput, that.fileOutput)&&
                Objects.equals(token, that.token) ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, receivedMessage, state, insertTime, endWorkTime, utente, fileName, fileInput, fileOutput,token);
    }


    @Basic
    @Column(name = "ERROR_MESSAGE")
	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}