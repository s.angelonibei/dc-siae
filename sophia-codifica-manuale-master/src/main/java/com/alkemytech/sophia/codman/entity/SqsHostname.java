package com.alkemytech.sophia.codman.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.GsonBuilder;

@XmlRootElement
@Entity(name="SqsHostname")
@Table(name="SQS_HOSTNAME")
public class SqsHostname {
	
	@Id
	@Column(name="HOSTNAME", nullable=false)
	private String hostname;
	
	@Column(name="QUEUE_TYPE", nullable=true)
	private String queueType;

	@Column(name="SERVICE_NAME", nullable=true)
	private String serviceName;

	@Column(name="IDDSR", nullable=true)
	private String idDsr;

	@Column(name="INSERT_TIME", nullable=false)
	private Date insertTime;

	@Column(name="APPLICATION_ID", nullable=true)
	private String applicationId;

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String getQueueType() {
		return queueType;
	}

	public void setQueueType(String queueType) {
		this.queueType = queueType;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getIdDsr() {
		return idDsr;
	}

	public void setIdDsr(String idDsr) {
		this.idDsr = idDsr;
	}

	public Date getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(Date insertTime) {
		this.insertTime = insertTime;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}
	
}
