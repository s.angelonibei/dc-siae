package com.alkemytech.sophia.codman.dto;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

@Data
@XmlRootElement
public class MonitorDspListDTO implements Serializable {

    @SerializedName("list")
    private List<MonitorDsrDTO> list;

    @SerializedName("periodo")
    private String periodo;

}
