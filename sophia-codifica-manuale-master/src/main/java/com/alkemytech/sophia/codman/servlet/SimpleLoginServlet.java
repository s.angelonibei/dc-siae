package com.alkemytech.sophia.codman.servlet;

import com.alkemytech.sophia.codman.entity.RolePermission;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.alkemytech.sophia.codman.multimedialelocale.entity.UserRole;
import com.alkemytech.sophia.codman.sso.SsoSessionAttrs;
import com.alkemytech.sophia.common.tools.StringTools;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@Singleton
@SuppressWarnings("serial")
public class SimpleLoginServlet extends HttpServlet {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Provider<EntityManager> provider;
	private final String ssoAuthLoginUrl;
		
	@Inject
	protected SimpleLoginServlet(@Named("configuration") Properties configuration,
			@McmdbDataSource Provider<EntityManager> provider,
			@Named("sso.auth.login.url") String ssoAuthLoginUrl) {
		super();
		this.provider = provider;
		this.ssoAuthLoginUrl = ssoAuthLoginUrl;
	}
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		final String username = request.getParameter("username");
		logger.debug("service: username [" + username + "]");
		final String password = request.getParameter("password");
		logger.debug("service: password [" + password + "]");
		final String landing = request.getParameter("landing");
		logger.debug("service: landing [" + landing + "]");

		try {

			final EntityManager entityManager = provider.get();
			
			// lookup user
			final UserRole userRole = entityManager.find(UserRole.class, username);
			if (null == userRole) {
				logger.debug("service: user not found");
				response.sendRedirect(ssoAuthLoginUrl);
				return;
			}
			
			// check password
			boolean authenticated = false;
			try {
				if (userRole.getPassword().equalsIgnoreCase(Hex
						.encodeHexString(MessageDigest.getInstance("MD5").digest(password.getBytes("UTF-8"))))) {
					authenticated = true;
				}
			} catch (Exception e) {
				logger.error("", e);
			}
			if (!authenticated) {
				logger.debug("service: bad password");
				response.sendRedirect(ssoAuthLoginUrl);
				return;
			}

			// load permissions for group(s)
			if (StringTools.isNullOrEmpty(userRole.getRoles())) {
				logger.debug("service: user does't belong to any group");
				response.sendRedirect(ssoAuthLoginUrl);
				return;				
			}
			final String[] roles = userRole.getRoles().split(",");
			final List<String> groups = new ArrayList<String>(roles.length);
			for (String role : roles) {
				groups.add(role.trim());
			}
			logger.debug("service: groups " + groups);
			
			// create and initialize session
			final HttpSession httpSession = request.getSession(true);
			httpSession.setAttribute(SsoSessionAttrs.USERNAME, username);
			httpSession.setAttribute(SsoSessionAttrs.EMAIL, username + "@siae.it");
			httpSession.setAttribute(SsoSessionAttrs.PP_TIPO, "");
			httpSession.setAttribute(SsoSessionAttrs.PP_SEPRAG, "");
			httpSession.setAttribute(SsoSessionAttrs.PP_DESCRIZIONE, "");
			httpSession.setAttribute(SsoSessionAttrs.STATO_PASSWORD, "");
			httpSession.setAttribute(SsoSessionAttrs.TOKEN_ID, "");
			httpSession.setAttribute(SsoSessionAttrs.USERNAME, username);
			httpSession.setAttribute(SsoSessionAttrs.GROUPS, groups);			
			httpSession.setAttribute(SsoSessionAttrs.LAST_CHECK, Long.valueOf(System.currentTimeMillis()));
			
			// inject user permission(s) into session
			final List<RolePermission> permissions = (List<RolePermission>) entityManager
				.createQuery("select x from RolePermission x where x.role in :role", RolePermission.class)
				.setParameter("role", groups)
				.getResultList();
			logger.debug("service: permissions " + permissions);
			for (RolePermission permission : permissions) {
				httpSession.setAttribute(permission.getPermission(), permission.getPermission());
			}

			response.sendRedirect(landing);
			return;

		} catch (Exception e) {
			logger.error("service", e);
		}
		
		// redirect to login page
		response.sendRedirect(ssoAuthLoginUrl);
	}
	
}
