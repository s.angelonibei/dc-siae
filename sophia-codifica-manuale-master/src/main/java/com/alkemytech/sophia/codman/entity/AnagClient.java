package com.alkemytech.sophia.codman.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.codman.persistence.AbstractEntity;

	@XmlRootElement
	@Entity(name = "AnagClient")
	@Table(name = "ANAG_CLIENT")
	@NamedQueries({@NamedQuery(name = "AnagClient.GetAll", query = "SELECT x FROM AnagClient x")})
	@SuppressWarnings("serial")

	public class AnagClient extends AbstractEntity<String> {
		
	    @Id
	    @Column(name = "ID_ANAG_CLIENT", nullable = false)
	    private Integer idAnagClient;

	    @Column(name = "CODE", nullable = false)
	    private String code;

	    @Column(name = "IDDSP", nullable = false)
	    private String idDsp;

	    @Column(name = "COMPANY_NAME ")
	    private String companyName;
	    
	    @Column(name = "COUNTRY")
	    private String country;
	    
	    @Column(name = "VAT_CODE")
	    private String vatCode;
	    	    
	    @Column(name = "LICENCE")
	    private String licence;
	    
	    @Column(name = "VAT_DESCRIPTION")
	    private String vatDescription;
	    
	    @Column(name = "FOREIGN_CLIENT")
	    private Boolean foreignClient;
	    
	    @Column(name = "START_DATE", nullable = false)
	    private Date startDate;

	    @Column(name = "END_DATE")
	    private Date endDate;
	    
	    
	    @Override
	    public String getId() {
	    	return idAnagClient.toString();
	    }


		public Integer getIdAnagClient() {
			return idAnagClient;
		}


		public void setIdAnagClient(Integer idAnagClient) {
			this.idAnagClient = idAnagClient;
		}


		public String getCode() {
			return code;
		}


		public void setCode(String code) {
			this.code = code;
		}


		public String getIdDsp() {
			return idDsp;
		}


		public void setIdDsp(String idDsp) {
			this.idDsp = idDsp;
		}


		public String getCompanyName() {
			return companyName;
		}


		public void setCompanyName(String companyName) {
			this.companyName = companyName;
		}


		public String getCountry() {
			return country;
		}


		public void setCountry(String country) {
			this.country = country;
		}


		public String getVatCode() {
			return vatCode;
		}


		public void setVatCode(String vatCode) {
			this.vatCode = vatCode;
		}


		public String getLicence() {
			return licence;
		}


		public void setLicence(String licence) {
			this.licence = licence;
		}


		public String getVatDescription() {
			return vatDescription;
		}


		public void setVatDescription(String vatDescription) {
			this.vatDescription = vatDescription;
		}


		public Boolean getForeignClient() {
			return foreignClient;
		}


		public void setForeignClient(Boolean foreignClient) {
			this.foreignClient = foreignClient;
		}


		public Date getStartDate() {
			return startDate;
		}


		public void setStartDate(Date startDate) {
			this.startDate = startDate;
		}


		public Date getEndDate() {
			return endDate;
		}


		public void setEndDate(Date endDate) {
			this.endDate = endDate;
		}
	    
	    
	}
 
