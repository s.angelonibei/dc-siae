package com.alkemytech.sophia.codman.mandato.service;

import java.util.List;

import com.alkemytech.sophia.codman.dto.ConfigurazioneMandatoDTO;

public interface IConfigurazioneMandatoService {
	
	public ConfigurazioneMandatoDTO get(Integer id);

    public List<ConfigurazioneMandatoDTO> getAll();
    
    public ConfigurazioneMandatoDTO save(ConfigurazioneMandatoDTO configurazioneMandato);
    
    public void delete(ConfigurazioneMandatoDTO configurazioneMandato);	

}
