package com.alkemytech.sophia.codman.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PerformingFlussoGuidaDto {

	private String meseInizioPeriodoContabile;
	private String annoInizioPeriodoContabile;
	private	String meseFinePeriodoContabile;
	private String annoFinePeriodoContabile;
	private Integer page;
	
	
	
	public PerformingFlussoGuidaDto() {
		super();
	}



	public PerformingFlussoGuidaDto(String meseInizioPeriodoContabile, String annoInizioPeriodoContabile,
			String meseFinePeriodoContabile, String annoFinePeriodoContabile, Integer page) {
		super();
		this.meseInizioPeriodoContabile = meseInizioPeriodoContabile;
		this.annoInizioPeriodoContabile = annoInizioPeriodoContabile;
		this.meseFinePeriodoContabile = meseFinePeriodoContabile;
		this.annoFinePeriodoContabile = annoFinePeriodoContabile;
		this.page = page;
	}



	public String getMeseInizioPeriodoContabile() {
		return meseInizioPeriodoContabile;
	}



	public void setMeseInizioPeriodoContabile(String meseInizioPeriodoContabile) {
		this.meseInizioPeriodoContabile = meseInizioPeriodoContabile;
	}



	public String getAnnoInizioPeriodoContabile() {
		return annoInizioPeriodoContabile;
	}



	public void setAnnoInizioPeriodoContabile(String annoInizioPeriodoContabile) {
		this.annoInizioPeriodoContabile = annoInizioPeriodoContabile;
	}



	public String getMeseFinePeriodoContabile() {
		return meseFinePeriodoContabile;
	}



	public void setMeseFinePeriodoContabile(String meseFinePeriodoContabile) {
		this.meseFinePeriodoContabile = meseFinePeriodoContabile;
	}



	public String getAnnoFinePeriodoContabile() {
		return annoFinePeriodoContabile;
	}



	public void setAnnoFinePeriodoContabile(String annoFinePeriodoContabile) {
		this.annoFinePeriodoContabile = annoFinePeriodoContabile;
	}



	public Integer getPage() {
		return page;
	}



	public void setPage(Integer page) {
		this.page = page;
	}
	
	
	
	

}
