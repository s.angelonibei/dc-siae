package com.alkemytech.sophia.codman.dto;

public class GenericResponse {
	private String errorCode;
	private String message;
	
	
	public GenericResponse() {
		super();
	}

	public GenericResponse(String errorCode, String message) {
		super();
		this.errorCode = errorCode;
		this.message = message;
	}
	
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	
}
