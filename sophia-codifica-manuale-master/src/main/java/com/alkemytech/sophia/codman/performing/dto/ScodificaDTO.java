package com.alkemytech.sophia.codman.performing.dto;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ScodificaDTO implements Serializable {
    @CsvBindByPosition(position = 0)
    @CsvBindByName(column = "Codice Combana")
    private Long idCombana;

    private String codiceCombana;

    @CsvBindByPosition(position = 1)
    @CsvBindByName(column = "Titolo")
    private String titolo;

    @CsvBindByPosition(position = 2)
    @CsvBindByName(column = "Compositori")
    private String compositori;

    @CsvBindByPosition(position = 3)
    @CsvBindByName(column ="Autori")
    private String autori;

    @CsvBindByPosition(position = 4)
    @CsvBindByName( column = "Interpreti")
    private String interpreti;

    @CsvBindByPosition(position = 5)
    @CsvBindByName(column = "Codice Opera")
    private String codiceOperaApprovato;

    public ScodificaDTO() {
    }

    public ScodificaDTO(Long idCombana, String codiceCombana, String titolo, String compositori, String autori, String interpreti, String codiceOperaApprovato) {
        this.idCombana = idCombana;
        this.codiceCombana = codiceCombana;
        this.titolo = titolo;
        this.compositori = compositori;
        this.autori = autori;
        this.interpreti = interpreti;
        this.codiceOperaApprovato = codiceOperaApprovato;
    }

    public ScodificaDTO(Long idCombana, String codiceCombana, String titolo, String codiceOperaApprovato) {
        this.idCombana = idCombana;
        this.codiceCombana = codiceCombana;
        this.titolo = titolo;
        this.codiceOperaApprovato = codiceOperaApprovato;
    }

    public ScodificaDTO(Long idCombana, String titolo, String codiceOperaApprovato) {
        this.idCombana = idCombana;
        this.titolo = titolo;
        this.codiceOperaApprovato = codiceOperaApprovato;
    }

    public ScodificaDTO(Long idCombana, String titolo, String compositori, String autori, String interpreti, String codiceOperaApprovato) {
        this.idCombana = idCombana;
        this.titolo = titolo;
        this.compositori = compositori;
        this.autori = autori;
        this.interpreti = interpreti;
        this.codiceOperaApprovato = codiceOperaApprovato;
    }

    public String getCodiceCombana() {
        return codiceCombana;
    }

    public void setCodiceCombana(String codiceCombana) {
        this.codiceCombana = codiceCombana;
    }

    public String getTitolo() {
        return titolo;
    }

    public void setTitolo(String titolo) {
        this.titolo = titolo;
    }

    public String getCompositori() {
        return compositori;
    }

    public void setCompositori(String compositori) {
        this.compositori = compositori;
    }

    public String getAutori() {
        return autori;
    }

    public void setAutori(String autori) {
        this.autori = autori;
    }

    public String getInterpreti() {
        return interpreti;
    }

    public void setInterpreti(String interpreti) {
        this.interpreti = interpreti;
    }

    public String getCodiceOperaApprovato() {
        return codiceOperaApprovato;
    }

    public void setCodiceOperaApprovato(String codiceOperaApprovato) {
        this.codiceOperaApprovato = codiceOperaApprovato;
    }

    public Long getIdCombana() {
        return idCombana;
    }

    public void setIdCombana(Long idCombana) {
        this.idCombana = idCombana;
    }
}
