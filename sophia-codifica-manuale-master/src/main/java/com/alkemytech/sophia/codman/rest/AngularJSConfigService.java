package com.alkemytech.sophia.codman.rest;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

@Singleton
@Path("angularjs-config")
public class AngularJSConfigService {

	private final Gson gson;
	private final Properties configuration;

	@Inject
	protected AngularJSConfigService(Gson gson,
			@Named("configuration") Properties configuration) {
		super();
		this.gson = gson;
		this.configuration = configuration;
	}

	@GET
	@Produces("application/json")
	public Response findAll() {
		final Map<String, Object> result = new HashMap<>();
		result.put("debug", Boolean.valueOf(configuration.getProperty("debug")));
		result.put("maxRowsPerPage", Integer.valueOf(configuration.getProperty("maxRowsPerPage")));
		result.put("msIngestionApiPath", configuration.getProperty("ms.ingestion.api.path"));
		result.put("msInvoiceApiPath", configuration.getProperty("ms.invoice.api.path"));
		result.put("ulisseUrlCodiceOpera", configuration.getProperty("ulisse.url.codice.opera"));
		result.put("mmReportRequestEndpoint", configuration.getProperty("mm.report.api.postreportRequest"));
		result.put("mmRequestReportBasePath", configuration.getProperty("mm.report.api.basePath"));
		result.put("mmRequestReportGetRequestList", configuration.getProperty("mm.report.api.getList"));
		result.put("solrLimitResult", configuration.getProperty("solr.result.limit"));
		return Response.ok(gson.toJson(result)).build();
	}
	
}
