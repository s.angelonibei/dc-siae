package com.alkemytech.sophia.codman.dto.performing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import java.util.Date;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PerfValorizzazioneDTO {

    private String ripartizioneOS;
    private String periodoRipartizione;
    private String voceIncasso;
    private String tipologiaReport;
    private String valorizzazione;
    private String ripartizione;
    private String user;
    private Date dataUltimaModifica;

    public PerfValorizzazioneDTO() {
        super();
    }

    public PerfValorizzazioneDTO(String ripartizioneOS, String periodoRipartizione, String voceIncasso, String tipologiaReport, String valorizzazione, String ripartizione, String user, Date dataUltimaModifica) {
        this.ripartizioneOS = ripartizioneOS;
        this.periodoRipartizione = periodoRipartizione;
        this.voceIncasso = voceIncasso;
        this.tipologiaReport = tipologiaReport;
        this.valorizzazione = valorizzazione;
        this.ripartizione = ripartizione;
        this.user = user;
        this.dataUltimaModifica = dataUltimaModifica;
    }

    public String getRipartizioneOS() {
        return ripartizioneOS;
    }

    public void setRipartizioneOS(String ripartizioneOS) {
        this.ripartizioneOS = ripartizioneOS;
    }

    public String getPeriodoRipartizione() {
        return periodoRipartizione;
    }

    public void setPeriodoRipartizione(String periodoRipartizione) {
        this.periodoRipartizione = periodoRipartizione;
    }

    public String getVoceIncasso() {
        return voceIncasso;
    }

    public void setVoceIncasso(String voceIncasso) {
        this.voceIncasso = voceIncasso;
    }

    public String getTipologiaReport() {
        return tipologiaReport;
    }

    public void setTipologiaReport(String tipologiaReport) {
        this.tipologiaReport = tipologiaReport;
    }

    public String getValorizzazione() {
        return valorizzazione;
    }

    public void setValorizzazione(String valorizzazione) {
        this.valorizzazione = valorizzazione;
    }

    public String getRipartizione() {
        return ripartizione;
    }

    public void setRipartizione(String ripartizione) {
        this.ripartizione = ripartizione;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Date getDataUltimaModifica() {
        return dataUltimaModifica;
    }

    public void setDataUltimaModifica(Date dataUltimaModifica) {
        this.dataUltimaModifica = dataUltimaModifica;
    }

    @Override
    public String toString() {
        return "PerfValorizzazioneDTO{" +
                "ripartizioneOS='" + ripartizioneOS + '\'' +
                ", periodoRipartizione='" + periodoRipartizione + '\'' +
                ", voceIncasso='" + voceIncasso + '\'' +
                ", tipologiaReport='" + tipologiaReport + '\'' +
                ", valorizzazione=" + valorizzazione +
                ", ripartizione='" + ripartizione + '\'' +
                ", user='" + user + '\'' +
                ", dataUltimaModifica='" + dataUltimaModifica + '\'' +
                '}';
    }
}