package com.alkemytech.sophia.codman.dto;

import com.alkemytech.sophia.codman.dto.enumaration.UseStartEndDate;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Data
@XmlRootElement
public class AnagCcidFilenameConfigDTO implements Serializable {

    private Integer idFilename;
    private Integer idCcidConfig;

    @SerializedName("invoiceSender")
    private String invoiceSender;

    @SerializedName("invoiceReceiver")
    private String invoiceReceiver;

    @SerializedName("useType")
    private String useType;

    @SerializedName("useStartEndDate")
    private String useStartEndDate;

    @SerializedName("licenseSD")
    private String licenseSD;

    @SerializedName("typeOfClaim")
    private String typeOfClaim;

}