package com.alkemytech.sophia.codman.rest.performing.serviceimpl;

import java.util.List;

public abstract class Modifier<T> {
	
	public void apply(T type) {}
	
	public abstract List<String> getApplicationFields();

}
