package com.alkemytech.sophia.codman.performing.dto;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;
import com.opencsv.bean.CsvNumber;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;
import java.util.Date;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ScodificaResponseDTO extends ScodificaDTO {
    private Date dataApprovazione;
    private String tipoAzione;
    @CsvBindByPosition(position = 6, locale = "it-IT")
    @CsvBindByName(column = "Importo", locale = "it-IT")
    @CsvNumber("#.###0,00¤")
    private BigDecimal valoreEconomico;

    @CsvBindByPosition(position = 7)
    @CsvBindByName(column = "N. Utilizzazioni")
    private Long nUtilizzazioni;
    private Long idCodifica;
    private Date dataCreazione;
    private Date dataCodifica;
    private String descrizione;
    private String tipoApprovazione;
    private BigDecimal confidenza;
    private String utenteCodifica;

    public Date getDataCreazione() {
        return dataCreazione;
    }

    public void setDataCreazione(Date dataCreazione) {
        this.dataCreazione = dataCreazione;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public String getTipoApprovazione() {
        return tipoApprovazione;
    }

    public void setTipoApprovazione(String tipoApprovazione) {
        this.tipoApprovazione = tipoApprovazione;
    }

    public BigDecimal getConfidenza() {
        return confidenza;
    }

    public void setConfidenza(BigDecimal confidenza) {
        this.confidenza = confidenza;
    }

    public String getUtenteCodifica() {
        return utenteCodifica;
    }

    public void setUtenteCodifica(String utenteCodifica) {
        this.utenteCodifica = utenteCodifica;
    }

    public ScodificaResponseDTO() {
        super();
    }

    public ScodificaResponseDTO(Date dataCreazione,
                                Date dataCodifica,
                                Date dataApprovazione,
                                String descrizione,
                                String tipoApprovazione,
                                String utenteCodifica,
                                String tipoAzione,
                                BigDecimal confidenza) {
        this.dataCreazione = dataCreazione;
        this.dataCodifica = dataCodifica;
        this.descrizione = descrizione;
        this.tipoApprovazione = tipoApprovazione;
        this.utenteCodifica = utenteCodifica;
        this.confidenza = confidenza;
        this.dataApprovazione = dataApprovazione;
        this.tipoAzione = tipoAzione;
    }

    public ScodificaResponseDTO(
            Long idCombana,
            Long idCodifica,
            String codiceCombana,
            String titolo,
            String compositori,
            String autori,
            String interpreti,
            String codiceOperaApprovato,
            BigDecimal valoreEconomico,
            BigDecimal nUtilizzazioni
    ) {
        super(idCombana, codiceCombana, titolo, compositori, autori, interpreti, codiceOperaApprovato);
        this.idCodifica = idCodifica;
        this.valoreEconomico = valoreEconomico;
        this.nUtilizzazioni = nUtilizzazioni != null ? nUtilizzazioni.longValue() : null;
    }

    public BigDecimal getValoreEconomico() {
        return valoreEconomico;
    }

    public void setValoreEconomico(BigDecimal valoreEconomico) {
        this.valoreEconomico = valoreEconomico;
    }

    public Long getnUtilizzazioni() {
        return nUtilizzazioni;
    }

    public void setnUtilizzazioni(Long nUtilizzazioni) {
        this.nUtilizzazioni = nUtilizzazioni;
    }

    public Long getIdCodifica() {
        return idCodifica;
    }

    public void setIdCodifica(Long idCodifica) {
        this.idCodifica = idCodifica;
    }

    public Date getDataCodifica() {
        return dataCodifica;
    }

    public void setDataCodifica(Date dataCodifica) {
        this.dataCodifica = dataCodifica;
    }

    public static Query getListaScodificaNativeQuery(String additionalConditions, EntityManager entityManager, String mapping) {

        return entityManager.createNativeQuery(
                "select combana.ID_COMBANA AS idCombana, " +
                        " codifica.ID_CODIFICA AS idCodifica, " +
                        " combana.CODICE_COMBANA AS codiceCombana, " +
                        " combana.TITOLO AS titolo, " +
                        " combana.COMPOSITORI AS compositori, " +
                        " combana.AUTORI AS autori, " +
                        " combana.INTERPRETI AS interpreti, " +
                        " codifica.CODICE_OPERA_APPROVATO AS codiceOperaApprovato, " +
//                        " codifica.VALORE_ECONOMICO AS valoreEconomico, " +
                        " COALESCE(SUM(( mc.IMPORTO_VALORIZZATO / mc.PUNTI_PROGRAMMA_MUSICALE ) * util.PUNTI_RICALCOLATI), 0) valoreEconomico, " +
                        " COALESCE(COUNT(util.ID_UTILIZZAZIONE),0) as nUtilizzazioni " +
                        "from PERF_COMBANA combana " +
                        " left join PERF_CODIFICA codifica " +
                        " on combana.ID_COMBANA = codifica.ID_COMBANA, " +
                        " PERF_MOVIMENTO_CONTABILE mc, " +
                        " PERF_UTILIZZAZIONE util " +
                        "where UPPER(COALESCE(combana.TITOLO, '')) LIKE UPPER(?1) " +
                        " AND (UPPER(COALESCE(combana.COMPOSITORI, '')) LIKE UPPER(?2) OR " +
                        " UPPER(COALESCE(combana.AUTORI, '')) LIKE UPPER(?2) OR " +
                        " UPPER(COALESCE(combana.INTERPRETI, '')) LIKE UPPER(?2)) " +
                        " AND util.ID_PROGRAMMA_MUSICALE = mc.ID_PROGRAMMA_MUSICALE " +
                        " AND combana.ID_COMBANA = util.ID_COMBANA " +
//                        " AND util.ID_CODIFICA = codifica.ID_CODIFICA " +
                        additionalConditions +
                        " GROUP BY combana.ID_COMBANA "
                        , mapping);
    }

    public static Query getDettaglioScodificaNativeQuery(EntityManager entityManager, String mapping) {

        return entityManager.createNativeQuery(
                "select " +
                        " combana.DATA_CREAZIONE AS dataCreazione, " +
                        " codifica.DATA_CODIFICA AS dataCodifica, " +
                        " codifica.DATA_APPROVAZIONE AS dataApprovazione, " +
                        " fpu.DESCRIZIONE AS descrizione, " +
                        "CASE " +
                        " WHEN (codifica.TIPO_APPROVAZIONE = 'A') THEN 'Automatica' " +
                        " WHEN (codifica.TIPO_APPROVAZIONE = 'M') THEN 'Manuale' " +
                        " ELSE '' END as tipoApprovazione, " +
                        "CASE " +
                        " WHEN COALESCE(util_asse.TIPO_AZIONE, '') IN ('S','') THEN '' " +
                        " ELSE sc.UTENTE END as utenteCodifica, " +
                        " util_asse.TIPO_AZIONE as tipoAzione, " +
                        " codifica.CONFIDENZA AS confidenza " +
                        "FROM PERF_COMBANA combana " +
                        " join PERF_CODIFICA codifica on " +
                        " codifica.ID_COMBANA = combana.ID_COMBANA " +
                        "left join PERF_UTILIZZAZIONI_ASSEGNATE util_asse on " +
                        " util_asse.ID_CODIFICA = codifica.ID_CODIFICA " +
                        "left join PERF_SESSIONE_CODIFICA sc on " +
                        " util_asse.ID_SESSIONE_CODIFICA = sc.ID_SESSIONE_CODIFICA " +
                        "join FONTE_PRIMA_UTILIZZAZIONE fpu on " +
                        " fpu.ID_FONTE_PRIMA_UTILIZZAZIONE = combana.ID_FONTE_PRIMA_UTILIZZAZIONE " +
                        "WHERE " +
                        " combana.id_combana = ?1 " +
                        "order by sc.ID_SESSIONE_CODIFICA desc",
                mapping);
    }

    public Date getDataApprovazione() {
        return dataApprovazione;
    }

    public void setDataApprovazione(Date dataApprovazione) {
        this.dataApprovazione = dataApprovazione;
    }

    public String getTipoAzione() {
        return tipoAzione;
    }

    public void setTipoAzione(String tipoAzione) {
        this.tipoAzione = tipoAzione;
    }

 /*public static String[] getMappingStrategy() {
 return new String[]{
 "Codice Combana",
 "Titolo",
 "Compositori",
 "Autori",
 "Interpreti",
 "Codice Opera",
 "Importo",
 "N. Utilizzazioni"
 };
 }*/
}
