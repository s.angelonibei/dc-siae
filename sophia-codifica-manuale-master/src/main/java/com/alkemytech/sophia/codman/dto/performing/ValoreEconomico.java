package com.alkemytech.sophia.codman.dto.performing;

import java.io.Serializable;
import java.math.BigDecimal;

public class ValoreEconomico implements Serializable {
    private Integer id;
    private BigDecimal inferiore;
    private BigDecimal superiore;
    private String header;

    public ValoreEconomico() {
    }

    public ValoreEconomico(Integer id,BigDecimal inferiore, BigDecimal superiore) {
        this.id = id;
        this.inferiore = inferiore;
        this.superiore = superiore;
        this.header = toString();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getInferiore() {
        return inferiore;
    }

    public void setInferiore(BigDecimal inferiore) {
        this.inferiore = inferiore;
    }

    public BigDecimal getSuperiore() {
        return superiore;
    }

    public void setSuperiore(BigDecimal superiore) {
        this.superiore = superiore;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getHeader() {
        return toString();
    }

    @Override
    public String toString() {
        return inferiore == null ? "€ \u2264 " + superiore :
                superiore == null ? "€ > " + inferiore :
                        inferiore + " < € \u2264 " + superiore;
    }
}
