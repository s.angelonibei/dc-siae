package com.alkemytech.sophia.codman.entity;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


@Entity
@DiscriminatorValue("threshold")
public class Threshold extends KeyValueProperty {

	private static final long serialVersionUID = 1L;

	@Column(name = "RELATION")
	@Convert(converter = RelationJpaConverter.class)
	private Relation relation;

	@Column(name = "CAPTION")
	private String caption;

	public Relation getRelation() {
		return relation;
	}

	public void setRelation(Relation relation) {
		this.relation = relation;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

}
