package com.alkemytech.sophia.codman.dto.performing.csv;

import java.math.BigDecimal;
import java.util.Date;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

public class BonificaEventoPagatoListToCsv {
	@CsvBindByPosition(position = 0)
    @CsvBindByName
	private String origineRecord;
	@CsvBindByPosition(position = 1)
	@CsvBindByName
	private Long idEvento;
	@CsvBindByPosition(position = 2)
	@CsvBindByName
	private String voceIncasso;
	@CsvBindByPosition(position = 3)
	@CsvBindByName
	private String seprag;
	@CsvBindByPosition(position = 4)
	@CsvBindByName
	private String numeroReversale;
	@CsvBindByPosition(position = 5)
	@CsvBindByName
	private Long importoDem;
	@CsvBindByPosition(position = 6)
	@CsvBindByName
	private Long contabilita;
	@CsvBindByPosition(position = 7)
	@CsvBindByName
	private String tipoDocumentoContabile;
	@CsvBindByPosition(position = 8)
	@CsvBindByName
	private String numeroFattura;
	@CsvBindByPosition(position = 9)
	@CsvBindByName
	private String denominazioneLocale;
	@CsvBindByPosition(position = 10)
	@CsvBindByName
	private String codiceBaLocale;
	@CsvBindByPosition(position = 11)
	@CsvBindByName
	private String codiceSpeiLocale;
	@CsvBindByPosition(position = 12)
	@CsvBindByName
	private Date dataInzioEvento;
	@CsvBindByPosition(position = 13)
	@CsvBindByName
	private String oraInizioEvento;
	@CsvBindByPosition(position = 14)
	@CsvBindByName
	private Long numeroPmAttesi;
	@CsvBindByPosition(position = 15)
	@CsvBindByName
	private Long numeroPmAttesiSpalla;
	@CsvBindByPosition(position = 16)
	@CsvBindByName
	private String codiceRaggruppamento;
	@CsvBindByPosition(position = 17)
	@CsvBindByName
	private String sede;
	@CsvBindByPosition(position = 18)
	@CsvBindByName
	private String agenzia;
	@CsvBindByPosition(position = 19)
	@CsvBindByName
	private String permesso;
	@CsvBindByPosition(position = 20)
	@CsvBindByName
	private Date dataReversale;

	public BonificaEventoPagatoListToCsv() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BonificaEventoPagatoListToCsv(Object[] row) {
		super();
		this.origineRecord = (String) row[0];
		this.idEvento = (Long) row[1];
		this.voceIncasso = (String) row[2];
		this.seprag = (String) row[3];
		this.numeroReversale = (String) row[4];
		if ( row[5]!=null) {
			this.importoDem=((BigDecimal) row[5]).longValue();
		}
		this.contabilita = (Long) row[6];
		this.tipoDocumentoContabile = (String) row[7];
		this.numeroFattura = (String) row[8];
		this.denominazioneLocale = (String) row[9];
		this.codiceBaLocale = (String) row[10];
		this.codiceSpeiLocale = (String) row[11];
		this.dataInzioEvento = (Date) row[12];
		this.oraInizioEvento = (String) row[13];
		this.numeroPmAttesi = (Long) row[14];
		this.numeroPmAttesiSpalla = (Long) row[15];
		this.codiceRaggruppamento = (String) row[16];
		this.sede = (String) row[17];
		this.agenzia = (String) row[18];
		this.permesso = (String) row[19];
		this.dataReversale = (Date) row[20];
	}

	public String getOrigineRecord() {
		return origineRecord;
	}

	public void setOrigineRecord(String origineRecord) {
		this.origineRecord = origineRecord;
	}

	public Long getIdEvento() {
		return idEvento;
	}

	public void setIdEvento(Long idEvento) {
		this.idEvento = idEvento;
	}

	public String getVoceIncasso() {
		return voceIncasso;
	}

	public void setVoceIncasso(String voceIncasso) {
		this.voceIncasso = voceIncasso;
	}

	public String getSeprag() {
		return seprag;
	}

	public void setSeprag(String seprag) {
		this.seprag = seprag;
	}

	public String getNumeroReversale() {
		return numeroReversale;
	}

	public void setNumeroReversale(String numeroReversale) {
		this.numeroReversale = numeroReversale;
	}

	public Long getImportoDem() {
		return importoDem;
	}

	public void setImportoDem(Long importoDem) {
		this.importoDem = importoDem;
	}

	public Long getContabilita() {
		return contabilita;
	}

	public void setContabilita(Long contabilita) {
		this.contabilita = contabilita;
	}

	public String getTipoDocumentoContabile() {
		return tipoDocumentoContabile;
	}

	public void setTipoDocumentoContabile(String tipoDocumentoContabile) {
		this.tipoDocumentoContabile = tipoDocumentoContabile;
	}

	public String getNumeroFattura() {
		return numeroFattura;
	}

	public void setNumeroFattura(String numeroFattura) {
		this.numeroFattura = numeroFattura;
	}

	public String getDenominazioneLocale() {
		return denominazioneLocale;
	}

	public void setDenominazioneLocale(String denominazioneLocale) {
		this.denominazioneLocale = denominazioneLocale;
	}

	public String getCodiceBaLocale() {
		return codiceBaLocale;
	}

	public void setCodiceBaLocale(String codiceBaLocale) {
		this.codiceBaLocale = codiceBaLocale;
	}

	public String getCodiceSpeiLocale() {
		return codiceSpeiLocale;
	}

	public void setCodiceSpeiLocale(String codiceSpeiLocale) {
		this.codiceSpeiLocale = codiceSpeiLocale;
	}

	public Date getDataInzioEvento() {
		return dataInzioEvento;
	}

	public void setDataInzioEvento(Date dataInzioEvento) {
		this.dataInzioEvento = dataInzioEvento;
	}

	public String getOraInizioEvento() {
		return oraInizioEvento;
	}

	public void setOraInizioEvento(String oraInizioEvento) {
		this.oraInizioEvento = oraInizioEvento;
	}

	public Long getNumeroPmAttesi() {
		return numeroPmAttesi;
	}

	public void setNumeroPmAttesi(Long numeroPmAttesi) {
		this.numeroPmAttesi = numeroPmAttesi;
	}

	public Long getNumeroPmAttesiSpalla() {
		return numeroPmAttesiSpalla;
	}

	public void setNumeroPmAttesiSpalla(Long numeroPmAttesiSpalla) {
		this.numeroPmAttesiSpalla = numeroPmAttesiSpalla;
	}

	public String getCodiceRaggruppamento() {
		return codiceRaggruppamento;
	}

	public void setCodiceRaggruppamento(String codiceRaggruppamento) {
		this.codiceRaggruppamento = codiceRaggruppamento;
	}

	public String getSede() {
		return sede;
	}

	public void setSede(String sede) {
		this.sede = sede;
	}

	public String getAgenzia() {
		return agenzia;
	}

	public void setAgenzia(String agenzia) {
		this.agenzia = agenzia;
	}

	public String getPermesso() {
		return permesso;
	}

	public void setPermesso(String permesso) {
		this.permesso = permesso;
	}

	public Date getDataReversale() {
		return dataReversale;
	}

	public void setDataReversale(Date dataReversale) {
		this.dataReversale = dataReversale;
	}

	public String[] getMappingStrategy() {
		return new String[] { 
        			"Origine Record",
        			"ID Evento", 
        			"Voce Incasso", 
        			"Seprag", 
        			"Numero Reversabile", 
        			"Importo Dem", 
        			"Contabilita'",
				"Tipo Documento Contabile", 
				"Numero Fattura", 
				"Denominazione Locale", 
				"Codice Ba Locale",
				"Codice Spei Locale", 
				"Data Inizio Evento", 
				"Ora Inizio Evento", 
				"Numero PM Attesi",
				"Numero PM Attesi Spalla", 
				"Codice Raggruppamento", 
				"Sede", 
				"Agenzia", 
				"Permesso", 
				"Data Reversale" };
	}

}
