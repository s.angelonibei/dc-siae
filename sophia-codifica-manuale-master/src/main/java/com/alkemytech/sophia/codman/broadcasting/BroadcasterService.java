package com.alkemytech.sophia.codman.broadcasting;

import com.alkemytech.sophia.broadcasting.dto.BdcAddEmittenteDTO;
import com.alkemytech.sophia.broadcasting.model.BdcBroadcasterConfigTemplate;
import com.alkemytech.sophia.broadcasting.model.TipoBroadcaster;
import com.alkemytech.sophia.broadcasting.service.interfaces.BdcBroadcastersService;
import com.alkemytech.sophia.broadcasting.service.interfaces.BroadcasterConfigService;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import org.codehaus.groovy.tools.shell.ParseCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

/**
 * Created by Alessandro Russo on 27/11/2017.
 */
@Singleton
@Path("broadcaster")
public class BroadcasterService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private BroadcasterConfigService broadcasterConfigService;
	private BdcBroadcastersService service;
	private Gson gson;

	@Inject
	public BroadcasterService(BdcBroadcastersService service,BroadcasterConfigService broadcasterConfigService, Gson gson) {
		this.service = service;
		this.broadcasterConfigService = broadcasterConfigService;
		this.gson = gson;
	}

	@GET
	@Path("{id}")
	@Produces("application/json")
	public Response findById(@PathParam("id") Integer id) {
		try {
			return Response.ok(gson.toJson(service.findById(id))).build();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return Response.status(500).build();
	}

	@POST
	@Path("addEmittente")
	@Produces("application/json")
	public Response addEmittente(BdcAddEmittenteDTO bdcAddCanaleDTO) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		Date inizioValid= null;
		Date fineValid= null;
		try {
			inizioValid = sdf.parse(bdcAddCanaleDTO.getDataInizioValidita());
		} catch (Exception e) {
			inizioValid = new Date();
		}
		
		try {
			fineValid = sdf.parse(bdcAddCanaleDTO.getDataFineValidita());
		} catch (Exception e) {
			
		}
		
		service.addBroadcaster(bdcAddCanaleDTO.getNomeEmittente(),
				TipoBroadcaster.valueOf(bdcAddCanaleDTO.getTipoBroadcaster()),
				bdcAddCanaleDTO.getBdcBroadcasterConfigTemplate(), bdcAddCanaleDTO.getUsername(), inizioValid,fineValid,(bdcAddCanaleDTO.getNuovoTracciatoRai()));
		
		return Response.ok().build();

	}

	@GET
	@Path("getConfigurazioni")
	@Produces("application/json")
	public Response getBdcBroadcastingTemplate(@QueryParam(value = "tipoEmittente") String tipoEmittente) {
		List<BdcBroadcasterConfigTemplate> bdcBroadcasterConfigTemplates = broadcasterConfigService.getBdcBroadcastingTemplate(tipoEmittente);
		return Response.ok(gson.toJson(bdcBroadcasterConfigTemplates)).build();
	}
}
