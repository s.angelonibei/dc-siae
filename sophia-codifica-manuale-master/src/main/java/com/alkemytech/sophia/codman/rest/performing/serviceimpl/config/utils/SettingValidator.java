package com.alkemytech.sophia.codman.rest.performing.serviceimpl.config.utils;

import com.alkemytech.sophia.codman.entity.Setting;

public interface SettingValidator {
	
	public int validate(Setting s);
	
	public boolean supports(Setting s); 

}
