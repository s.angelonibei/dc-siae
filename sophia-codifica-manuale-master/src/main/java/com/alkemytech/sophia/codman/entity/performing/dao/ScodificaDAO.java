package com.alkemytech.sophia.codman.entity.performing.dao;

import com.alkemytech.sophia.codman.dto.PaginationDTO;
import com.alkemytech.sophia.codman.dto.ResponseDTO;
import com.alkemytech.sophia.codman.entity.performing.PerfCodifica;
import com.alkemytech.sophia.codman.entity.performing.PerfSessioneCodifica;
import com.alkemytech.sophia.codman.entity.performing.PerfUtilizzazione;
import com.alkemytech.sophia.codman.entity.performing.PerfUtilizzazioniAssegnate;
import com.alkemytech.sophia.codman.performing.dto.DettaglioCombanaDTO;
import com.alkemytech.sophia.codman.performing.dto.ScodificaRequestDTO;
import com.alkemytech.sophia.codman.performing.dto.ScodificaResponseDTO;
import com.alkemytech.sophia.codman.performing.dto.SessioneCodificaDTO;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

@Repository
public class ScodificaDAO implements IScodificaDAO {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final String QUERY_DETTAGLIO_COMBANA = "SELECT new com.alkemytech.sophia.codman.performing.dto.DettaglioCombanaDTO(util.perfCombana.idCombana, coalesce(pr.codice, 'NA'), pr.competenzeDa, pr.competenzeA, vi.codVoceIncasso, vi.descrizione, COALESCE(SUM(( mc.importoValorizzato / mc.puntiProgrammaMusicale ) * util.puntiRicalcolati), 0), util.titoloComposizione, util.autori, util.compositore, util.interpreti, util.durata, util.codiceOpera, util.numeroProgrammaMusicale )FROM PerfUtilizzazione util, PerfMovimentoContabile mc left join PeriodoRipartizione pr on pr.idPeriodoRipartizione = mc.idPeriodoRipartizione, PerfVoceIncasso vi where util.perfCombana.idCombana = :idCombana AND  util.idProgrammaMusicale = mc.idProgrammaMusicale AND vi.codVoceIncasso = mc.codiceVoceIncasso GROUP BY pr.codice, vi.codVoceIncasso";
    private final String QUERY_LISTA_UTILIZZAZIONI_DETTAGLIO_COMBANA = "SELECT new com.alkemytech.sophia.codman.performing.dto.DettaglioCombanaDTO(util.perfCombana.idCombana, coalesce(pr.codice, 'NA'), pr.competenzeDa, pr.competenzeA, vi.codVoceIncasso, vi.descrizione, coalesce(((mc.importoValorizzato/mc.puntiProgrammaMusicale)*util.puntiRicalcolati),0) , util.titoloComposizione, util.autori, util.compositore, util.interpreti, util.durata, util.codiceOpera, util.numeroProgrammaMusicale) FROM PerfUtilizzazione util, PerfMovimentoContabile mc left join PeriodoRipartizione pr on pr.idPeriodoRipartizione = mc.idPeriodoRipartizione, PerfVoceIncasso vi where util.perfCombana.idCombana = :idCombana AND  util.idProgrammaMusicale = mc.idProgrammaMusicale AND vi.codVoceIncasso = mc.codiceVoceIncasso order by (coalesce((mc.importoValorizzato/mc.puntiProgrammaMusicale)*util.puntiRicalcolati,0)) DESC";
    private Provider<EntityManager> provider;

    private int sessioneScodificaTimeout;

    @Inject
    public ScodificaDAO(Provider<EntityManager> provider, @Named("codifica.scodifica.sessione.timeout") int sessioneScodificaTimeout) {
        this.provider = provider;
        this.sessioneScodificaTimeout = sessioneScodificaTimeout;
    }

    @Override
    public ResponseDTO getListaScodifica(ScodificaRequestDTO scodificaRequestDTO) {
        EntityManager entityManager = provider.get();

//        TypedQuery<ScodificaResponseDTO> typedQuery = getListaScodificaTypedQuery(scodificaRequestDTO, entityManager);

        ResponseDTO responseDTO = new ResponseDTO();
//        responseDTO.setPaginationDTO(new PaginationDTO(entityManager, select + "count(*) " + predicateQuery, valueList, scodificaRequestDTO.getPaginationDTO().getPageSize(), scodificaRequestDTO.getPaginationDTO().getPageNumber()));

        Query q = ScodificaResponseDTO.getListaScodificaNativeQuery(getAdditionalConditions(scodificaRequestDTO), entityManager, "ListaScodificaMapping")
                .setFirstResult((scodificaRequestDTO.getPaginationDTO().getPageNumber() - 1) * scodificaRequestDTO.getPaginationDTO().getPageSize())
                .setMaxResults(scodificaRequestDTO.getPaginationDTO().getPageSize());

        setParameterAndResultingList(scodificaRequestDTO, responseDTO, q);

        responseDTO.setPaginationDTO(new PaginationDTO(scodificaRequestDTO.getPaginationDTO().getPageNumber(), responseDTO.getList().size(), 1, scodificaRequestDTO.getPaginationDTO().getPageSize()));

        return responseDTO;
    }

    @Override
    public ResponseDTO getDettaglioScodifica(Long idCombana) {
        EntityManager entityManager = provider.get();
        ResponseDTO responseDTO = new ResponseDTO();

        responseDTO.setList(ScodificaResponseDTO.getDettaglioScodificaNativeQuery(entityManager, "DetailScodificaMapping")
                .setParameter(1, idCombana).getResultList());

        return responseDTO;
    }

    @Override
    public SessioneCodificaDTO getSessioneScodifica(Long idCombana, Long idCodifica, String utente) {
        EntityManager entityManager = provider.get();
        SessioneCodificaDTO responseDTO = null;

        entityManager.getTransaction().begin();

        String query = "SELECT count(ua) " +
                "FROM PerfUtilizzazioniAssegnate ua " +
                "where ua.perfCodifica.idCodifica = :idCodifica " +
                "and ua.perfSessioneCodifica.dataFineValid > current_timestamp " +
                "and ua.perfSessioneCodifica.utente !=:utente " +
                "order by ua.perfSessioneCodifica.idSessioneCodifica DESC ";

        if (entityManager.createQuery(query, Long.class)
                .setParameter("idCodifica", idCodifica)
                .setParameter("utente", utente)
                .setMaxResults(1)
                .getSingleResult().equals(Long.valueOf(0))) {
            try {
                PerfUtilizzazioniAssegnate perfUtilizzazioniAssegnate = getPerfUtilizzazioniAssegnate(idCombana, idCodifica, utente, entityManager);
                perfUtilizzazioniAssegnate.getPerfSessioneCodifica().setDataFineValid(GregorianCalendar.getInstance().getTime());
                perfUtilizzazioniAssegnate.setTipoAzione("S");
            } catch (NoResultException e) {
                logger.info(" Non ci sono sessioni di codifica per id {} gia' aperte dall'utente {} con la combana {}",idCodifica, utente, idCombana);
            } finally {
                PerfUtilizzazioniAssegnate perfUtilizzazioniAssegnate = new PerfUtilizzazioniAssegnate();
                PerfCodifica perfCodifica = entityManager.find(PerfCodifica.class, idCodifica);
                PerfSessioneCodifica perfSessioneCodifica = new PerfSessioneCodifica();
                Calendar calendar = GregorianCalendar.getInstance();
                calendar.add(Calendar.MINUTE, sessioneScodificaTimeout);
                perfSessioneCodifica.setDataInizioValid(GregorianCalendar.getInstance().getTime());
                perfSessioneCodifica.setDataFineValid(calendar.getTime());
                perfSessioneCodifica.setUtente(utente);
                perfUtilizzazioniAssegnate.setPerfCodifica(perfCodifica);
                perfUtilizzazioniAssegnate.setPerfSessioneCodifica(perfSessioneCodifica);
                entityManager.merge(perfUtilizzazioniAssegnate);

                responseDTO =  new SessioneCodificaDTO(perfUtilizzazioniAssegnate.getPerfSessioneCodifica().getIdSessioneCodifica(),
                                perfUtilizzazioniAssegnate.getPerfSessioneCodifica().getDataFineValid(),
                                perfUtilizzazioniAssegnate.getPerfSessioneCodifica().getDataInizioValid(),
                                perfUtilizzazioniAssegnate.getPerfSessioneCodifica().getUtente()
                                );
            }
        }
        entityManager.getTransaction().commit();
        return responseDTO;
    }

    public PerfUtilizzazioniAssegnate getPerfUtilizzazioniAssegnate(Long idCombana, Long idCodifica, String utente, EntityManager entityManager) {
        String query = "SELECT ua " +
                "FROM PerfUtilizzazioniAssegnate ua " +
                "where ua.perfCodifica.idCodifica = :idCodifica " +
                "and ua.perfCodifica.perfCombana.idCombana = :idCombana " +
                "and ua.perfSessioneCodifica.dataFineValid > current_timestamp " +
                "and ua.perfSessioneCodifica.dataFineValid is not null ";
        if(utente != null){
            query += "and ua.perfSessioneCodifica.utente = :utente ";
        }

        query += "order by ua.perfSessioneCodifica.idSessioneCodifica DESC ";

        TypedQuery<PerfUtilizzazioniAssegnate> q = entityManager.createQuery(
                query
                , PerfUtilizzazioniAssegnate.class)
                .setParameter("idCodifica", idCodifica)
                .setParameter("idCombana", idCombana)
                .setMaxResults(1);

        if(utente!=null){
           q.setParameter("utente", utente);
        }

        return q.getSingleResult();
    }

    public void setParameterAndResultingList(ScodificaRequestDTO scodificaRequestDTO, ResponseDTO responseDTO, Query q) {
        Map<Integer, Object> parameters = setNativeParameters(scodificaRequestDTO);

        for (Integer key : parameters.keySet()) {
            q.setParameter(key, parameters.get(key));
        }

        responseDTO.setList(q.getResultList());
    }

    public ResponseDTO getListaScodificaForExport(ScodificaRequestDTO scodificaRequestDTO) {
        EntityManager entityManager = provider.get();

//        TypedQuery<ScodificaResponseDTO> typedQuery = getListaScodificaTypedQuery(scodificaRequestDTO, entityManager);
        Query q = ScodificaResponseDTO.getListaScodificaNativeQuery(getAdditionalConditions(scodificaRequestDTO), entityManager, "ListaScodificaMapping");
        ResponseDTO responseDTO = new ResponseDTO();

        setParameterAndResultingList(scodificaRequestDTO, responseDTO, q);

        //        responseDTO.setPaginationDTO(new PaginationDTO(entityManager, select + "count(*) " + predicateQuery, valueList, scodificaRequestDTO.getPaginationDTO().getPageSize(), scodificaRequestDTO.getPaginationDTO().getPageNumber()));
        responseDTO.setList(q
                .getResultList());

        //responseDTO.setPaginationDTO(new PaginationDTO(scodificaRequestDTO.getPaginationDTO().getPageNumber(), responseDTO.getList().size(), 1, scodificaRequestDTO.getPaginationDTO().getPageSize()));

        return responseDTO;
    }

    /*public ResponseDTO getListaUtilizzazioniForExport(Long idCombana) {
        EntityManager entityManager = provider.get();

        ResponseDTO responseDTO = new ResponseDTO();

        responseDTO.setList(entityManager.createNamedQuery("PerfUtilizzazione.findAllByIdCombana", PerfUtilizzazione.class)
                .setParameter("idCombana", idCombana)
                .getResultList());

        //responseDTO.setPaginationDTO(new PaginationDTO(scodificaRequestDTO.getPaginationDTO().getPageNumber(), responseDTO.getList().size(), 1, scodificaRequestDTO.getPaginationDTO().getPageSize()));

        return responseDTO;
    }*/

    private String getAdditionalConditions(ScodificaRequestDTO scodificaRequestDTO) {
        String additionalConditions = "";
        additionalConditions = getCommonAdditionalConditions(additionalConditions, scodificaRequestDTO.getIdCombana(), "and combana.ID_COMBANA = ?3 ");
        additionalConditions = getCommonAdditionalConditions(additionalConditions, scodificaRequestDTO.getCodiceOperaApprovato(), "and codifica.CODICE_OPERA_APPROVATO = ?4 ");

        return additionalConditions;
    }

    private Map<String, Object> setParameters(ScodificaRequestDTO scodificaRequestDTO) {
        Map<String, Object> valueList = new HashMap<>();

        valueList.put("titolo", scodificaRequestDTO.getTitolo() != null ? "%" + scodificaRequestDTO.getTitolo() + "%" : "%%");
        valueList.put("nominativo", scodificaRequestDTO.getNominativo() != null ? "%" + scodificaRequestDTO.getNominativo() + "%" : "%%");

        if (StringUtils.isNotEmpty(scodificaRequestDTO.getCodiceCombana())) {
            valueList.put("codiceCombana", scodificaRequestDTO.getCodiceCombana());
        }
        if (StringUtils.isNotEmpty(scodificaRequestDTO.getCodiceOperaApprovato())) {
            valueList.put("codiceOperaApprovato", scodificaRequestDTO.getCodiceOperaApprovato());
        }
        return valueList;
    }

    private Map<Integer, Object> setNativeParameters(ScodificaRequestDTO scodificaRequestDTO) {
        Map<Integer, Object> valueList = new HashMap<>();

        valueList.put(1, scodificaRequestDTO.getTitolo() != null ? "%" + scodificaRequestDTO.getTitolo() + "%" : "%%");
        valueList.put(2, scodificaRequestDTO.getNominativo() != null ? "%" + scodificaRequestDTO.getNominativo() + "%" : "%%");

        if (scodificaRequestDTO.getIdCombana() != null && StringUtils.isNotEmpty(scodificaRequestDTO.getIdCombana().toString())) {
            valueList.put(3, scodificaRequestDTO.getIdCombana());
        }
        if (StringUtils.isNotEmpty(scodificaRequestDTO.getCodiceOperaApprovato())) {
            valueList.put(4, scodificaRequestDTO.getCodiceOperaApprovato());
        }
        return valueList;
    }

    private String getCommonAdditionalConditions(String additionalConditions, Object value, String s) {
        if (value != null && StringUtils.isNotEmpty(value.toString())) {
            additionalConditions += s;
        }
        return additionalConditions;
    }

    private TypedQuery<ScodificaResponseDTO> getListaScodificaTypedQuery(ScodificaRequestDTO scodificaRequestDTO, EntityManager entityManager) {
        String select = "select ";

        String query =
                "utilAsse.perfCodifica.perfCombana.idCombana, " +
                        "utilAsse.perfCodifica.perfCombana.codiceCombana, " +
                        "utilAsse.perfCodifica.perfCombana.titolo, " +
                        "utilAsse.perfCodifica.perfCombana.compositori, " +
                        "utilAsse.perfCodifica.perfCombana.autori, " +
                        "utilAsse.perfCodifica.perfCombana.interpreti, " +
                        "utilAsse.perfCodifica.codiceOperaApprovato, " +
                        "utilAsse.perfCodifica.valoreEconomico, " +
                        "count(utilAsse.perfCodifica.idCodifica), " +
                        "utilAsse.perfCodifica.idCodifica, " +
                        "utilAsse.perfCodifica.perfCombana.dataCreazione, " +
                        "utilAsse.perfCodifica.dataCodifica, " +
                        "utilAsse.perfCodifica.perfCombana.fontePrimaUtilizzazione.descrizione, " +
                        "cast(case when utilAsse.perfCodifica.tipoApprovazione = 'A' then 'Automatica' else 'Manuale' end as char), " +
                        "utilAsse.perfCodifica.confidenza, " +
                        "cast(case when utilAsse.perfCodifica.tipoApprovazione = 'A' then 'Automatica' else utilAsse.perfSessioneCodifica.utente end as char) ";
        String predicateQuery =
                " FROM PerfUtilizzazioniAssegnate utilAsse " +
//                "join utilAsse.perfCodifica.perfCombana " +
//                "utilAsse.perfCodifica.perfUtilizzaziones util " +
//                "join PerfMovimentoContabile mc on mc.idProgrammaMusicale = util.idProgrammaMusicale " +
//                "join PerfEventiPagati ep on ep.voceIncasso = mc.codiceVoceIncasso " +
//                "join ProgrammaMusicale pm on (pm.id = util.idProgrammaMusicale " +
//                "and mc.idEvento = ep.idEvento) " +
//                "join PeriodoRipartizione pr on pr.idPeriodoRipartizione = ep.idPeriodoRipartizione " +
                        "where utilAsse.perfCodifica.codiceOperaApprovato is not null " +
//                "and mc.idCombana is not null " +
                        "and upper(coalesce(utilAsse.perfCodifica.perfCombana.titolo,'')) like upper(:titolo) " +
                        "and (upper(coalesce(utilAsse.perfCodifica.perfCombana.compositori,'')) like upper(:nominativo) " +
                        "or upper(coalesce(utilAsse.perfCodifica.perfCombana.autori, '')) like upper(:nominativo) " +
                        "or upper(coalesce(utilAsse.perfCodifica.perfCombana.interpreti, '')) like upper(:nominativo)) ";

        predicateQuery = getCommonAdditionalConditions(predicateQuery, scodificaRequestDTO.getCodiceCombana(), "and utilAsse.perfCodifica.perfCombana.codiceCombana = :codiceCombana ");
        predicateQuery = getCommonAdditionalConditions(predicateQuery, scodificaRequestDTO.getCodiceOperaApprovato(), "and utilAsse.perfCodifica.codiceOperaApprovato = :codiceOperaApprovato ");
        if (scodificaRequestDTO.getRipartizioniRiferimentoDa() != null) {
            predicateQuery += "and pr.competenzeDa >= :competenzeDa ";
        }
        if (scodificaRequestDTO.getRipartizioniRiferimentoA() != null) {
            predicateQuery += "and pr.competenzeA <= :competenzeA ";
        }
        if (scodificaRequestDTO.getNumeroProgrammaMusicale() != null) {
            predicateQuery += "and pm.numeroProgrammaMusicale = :numeroProgrammaMusicale ";
        }
        predicateQuery += "group by utilAsse.perfCodifica.idCodifica " +
                "order by utilAsse.perfCodifica.idCodifica";

        Map<String, Object> valueList = setParameters(scodificaRequestDTO);

        if (scodificaRequestDTO.getRipartizioniRiferimentoDa() != null) {
            valueList.put("competenzeDa", scodificaRequestDTO.getRipartizioniRiferimentoDa());
        }
        if (scodificaRequestDTO.getRipartizioniRiferimentoA() != null) {
            valueList.put("competenzeA", scodificaRequestDTO.getRipartizioniRiferimentoA());
        }
        if (scodificaRequestDTO.getNumeroProgrammaMusicale() != null) {
            valueList.put("numeroProgrammaMusicale", scodificaRequestDTO.getNumeroProgrammaMusicale());
        }

        TypedQuery<ScodificaResponseDTO> typedQuery =
                entityManager.createQuery(select + "new com.alkemytech.sophia.codman.performing.dto.ScodificaResponseDTO(" + query + ")" + predicateQuery, ScodificaResponseDTO.class);

        for (String key : valueList.keySet()) {
            typedQuery.setParameter(key, valueList.get(key));
        }
        return typedQuery;
    }

    public ResponseDTO getDettaglioCombana(Long idCombana) {
        EntityManager entityManager = provider.get();

        return new ResponseDTO(entityManager.createQuery(this.QUERY_DETTAGLIO_COMBANA, DettaglioCombanaDTO.class)
                .setParameter("idCombana", idCombana).getResultList());
    }

    public ResponseDTO getListaUtilizzazioniForExport(Long idCombana) {
        EntityManager entityManager = provider.get();

        return new ResponseDTO(entityManager.createQuery(this.QUERY_LISTA_UTILIZZAZIONI_DETTAGLIO_COMBANA, DettaglioCombanaDTO.class)
                .setParameter("idCombana", idCombana)
                .getResultList());
    }

    @Override
    public void deleteCodiceOpera(Long idCodifica, Long idCombana, String codiceOpera) {
        EntityManager entityManager = provider.get();
        int batchSize = 25;

        EntityTransaction entityTransaction = entityManager
                .getTransaction();

        try {
            entityTransaction.begin();

            PerfCodifica perfCodifica = entityManager.find(PerfCodifica.class, idCodifica);
            PerfUtilizzazioniAssegnate perfUtilizzazioniAssegnate = getPerfUtilizzazioniAssegnate(idCombana, idCodifica, null, entityManager);

            perfUtilizzazioniAssegnate.setTipoAzione("SC");

            perfUtilizzazioniAssegnate.getPerfSessioneCodifica().setDataFineValid(GregorianCalendar.getInstance().getTime());

            perfCodifica.setCodiceOperaApprovato(null);
            perfCodifica.setDataApprovazione(GregorianCalendar.getInstance().getTime());
            perfCodifica.setStatoApprovazione(null);
            perfCodifica.setTipoApprovazione("M");

            perfUtilizzazioniAssegnate.setPerfCodifica(perfCodifica);

            for (int i = 0; i < perfUtilizzazioniAssegnate.getPerfCodifica().getPerfUtilizzaziones().size(); i++) {
                if (i > 0 && i % batchSize == 0) {
                    entityTransaction.commit();
                    entityTransaction.begin();

                    entityManager.clear();
                }

                PerfUtilizzazione perfUtilizzazione = perfUtilizzazioniAssegnate.getPerfCodifica().getPerfUtilizzaziones().get(i);
                perfUtilizzazione.setCodiceOpera(null);
//                perfUtilizzazione.getPerfCodifica().setIdCodifica(null);
                perfUtilizzazione.setPerfCodifica(null);
                entityManager.merge(perfUtilizzazione);
            }

            entityTransaction.commit();
            entityTransaction.begin();
            entityManager.clear();

            //perfCodifica.setPerfUtilizzaziones(null);

            entityManager.merge(perfUtilizzazioniAssegnate);
            entityManager.flush();

//            List<PerfUtilizzazione> perfUtilizzazioneList =
//                    entityManager.createNamedQuery("PerfUtilizzazione.findAllByCodiceOperaAndIdCombana", PerfUtilizzazione.class)
//                    .setParameter("codiceOpera", codiceOpera)
//                    .setParameter("idCombana", idCombana)
//                    .getResultList();


            entityTransaction.commit();
        } catch (RuntimeException e) {
            if (entityTransaction.isActive()) {
                entityTransaction.rollback();
            }
            logger.error(e.getMessage(), e);
            throw e;
        }
    }
}
