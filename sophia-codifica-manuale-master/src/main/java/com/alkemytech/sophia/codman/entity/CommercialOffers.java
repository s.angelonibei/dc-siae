package com.alkemytech.sophia.codman.entity;

import com.alkemytech.sophia.codman.persistence.AbstractEntity;
import com.google.gson.GsonBuilder;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@Entity(name="CommercialOffers")
@Table(name="COMMERCIAL_OFFERS")
@NamedQueries({
		@NamedQuery(name="CommercialOffers.GetAll", query="SELECT x FROM CommercialOffers x"),
		@NamedQuery(name="CommercialOffers.GetOfferingsByIdDsp", query = "SELECT new com.alkemytech.sophia.codman.dto.DropdownDTO(x.offering, x.offering) FROM CommercialOffers x WHERE x.idDSP IN :idDsps"),
		@NamedQuery(name="CommercialOffers.GetOfferingsByIdDspAndidUtilizationType", query = "SELECT new com.alkemytech.sophia.codman.dto.DropdownDTO(x.offering, x.offering) FROM CommercialOffers x WHERE x.idDSP IN :idDsps and x.idUtilizationType IN :offerings"),
		@NamedQuery(name="CommercialOffers.GetOfferingsByIdUtilizationType", query = "SELECT new com.alkemytech.sophia.codman.dto.DropdownDTO(x.offering, x.offering) FROM CommercialOffers x WHERE x.idUtilizationType IN :offerings")
})
@XmlRootElement
@SuppressWarnings("serial")

public class CommercialOffers extends AbstractEntity<String>{

	@Id
	@Column(name="ID_COMMERCIAL_OFFERS", nullable=false)
	private int idCommercialOffering;

	@Column(name="IDDSP", nullable=false)
	private String idDSP;

	@Column(name="ID_UTIILIZATION_TYPE", nullable=false)
	private String idUtilizationType;

	@Column(name="OFFERING", nullable=false)
	private String offering;
	
	@Column(name="CCID_SERVICE_TYPE_CODE", nullable=false)
	private String ccidServiceTypeCode;
	
	@Column(name="CCID_USE_TYPE_CODE", nullable=false)
	private String ccidUseTypeCode;
	
	@Column(name="CCID_APPLIED_TARIFF_CODE", nullable=false)
	private String ccidAppliedTariffCode;
	
	@Column(name="TRADING_BRAND", nullable=false)
	private String tradingBrand;


	public CommercialOffers(int idCommercialOffering, String offering, String idUtilizationType) {
		this.idCommercialOffering = idCommercialOffering;
		this.offering = offering;
		this.idUtilizationType = idUtilizationType;
	}

	public CommercialOffers() {
	}

	public int getIdCommercialOffering() {
		return idCommercialOffering;
	}

	public void setIdCommercialOffering(int idCommercialOffering) {
		this.idCommercialOffering = idCommercialOffering;
	}

	public String getIdDSP() {
		return idDSP;
	}

	public void setIdDSP(String idDSP) {
		this.idDSP = idDSP;
	}

	public String getIdUtilizationType() {
		return idUtilizationType;
	}

	public void setIdUtilizationType(String idUtilizationType) {
		this.idUtilizationType = idUtilizationType;
	}

	public String getOffering() {
		return offering;
	}

	public void setOffering(String offering) {
		this.offering = offering;
	}


	public String getCcidServiceTypeCode() {
		return ccidServiceTypeCode;
	}

	public void setCcidServiceTypeCode(String ccidServiceTypeCode) {
		this.ccidServiceTypeCode = ccidServiceTypeCode;
	}

	public String getCcidUseTypeCode() {
		return ccidUseTypeCode;
	}

	public void setCcidUseTypeCode(String ccidUseTypeCode) {
		this.ccidUseTypeCode = ccidUseTypeCode;
	}

	public String getCcidAppliedTariffCode() {
		return ccidAppliedTariffCode;
	}

	public void setCcidAppliedTariffCode(String ccidAppliedTariffCode) {
		this.ccidAppliedTariffCode = ccidAppliedTariffCode;
	}

	public String getTradingBrand() {
		return tradingBrand;
	}

	public void setTradingBrand(String tradingBrand) {
		this.tradingBrand = tradingBrand;
	}


	@Override
	public String getId() {
		return String.valueOf(getIdCommercialOffering());
	}

	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}
	
}
