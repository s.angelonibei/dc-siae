package com.alkemytech.sophia.codman.dto.performing;

public class CodificaOpereDTO{

    private Long idCodifica;
    private String statApprov;
    private String codApprov;

    public CodificaOpereDTO() {
        super();
    }

    public CodificaOpereDTO(Long idCodifica, String statApprov, String codApprov) {
        super();
        this.idCodifica = idCodifica;
        this.statApprov = statApprov;
        this.codApprov = codApprov;
    }

    public Long getIdCodifica() {
        return idCodifica;
    }

    public void setIdCodifica(Long idCodifica) {
        this.idCodifica = idCodifica;
    }

    public String getStatApprov() {
        return statApprov;
    }

    public void setStatApprov(String statApprov) {
        this.statApprov = statApprov;
    }

    public String getCodApprov() {
        return codApprov;
    }

    public void setCodApprov(String codApprov) {
        this.codApprov = codApprov;
    }

    @Override
    public String toString() {
        return "CodificaOpereDTO{" +
                "idCodifica=" + idCodifica +
                ", statApprov='" + statApprov + '\'' +
                ", codApprov='" + codApprov + '\'' +
                '}';
    }
}
