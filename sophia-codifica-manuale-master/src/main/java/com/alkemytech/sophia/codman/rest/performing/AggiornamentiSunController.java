package com.alkemytech.sophia.codman.rest.performing;


import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.List;

import javax.persistence.NoResultException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import org.apache.http.HttpStatus;

import com.alkemytech.sophia.codman.dto.performing.csv.BonificaDirettoreListToCsv;
import com.alkemytech.sophia.codman.dto.performing.csv.BonificaEventoPagatoListToCsv;
import com.alkemytech.sophia.codman.dto.performing.csv.BonificaManifestazioneListToCsv;
import com.alkemytech.sophia.codman.dto.performing.csv.BonificaMovimentoContabileListToCsv;
import com.alkemytech.sophia.codman.dto.performing.csv.BonificaProgrammaMusicaleListToCsv;
import com.alkemytech.sophia.codman.rest.performing.service.IAggiornamentoSunService;
import com.alkemytech.sophia.common.beantocsv.CustomMappingStrategy;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

@Singleton
@Path("performing/aggiornamentiSun")
public class AggiornamentiSunController {

	private IAggiornamentoSunService aggiornamentoSunService;
	private Gson gson;

	public static final String UTF8_BOM = "\uFEFF";

	@Inject
	protected AggiornamentiSunController( Gson gson,IAggiornamentoSunService aggiornamentoSunService) {
		super();
		this.gson = gson;
		this.aggiornamentoSunService = aggiornamentoSunService;
		
	
	}

	@GET
	@Path("periodiAggiornamenti")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDateAggiornamento(){
		try {
			return Response.ok(gson.toJson(aggiornamentoSunService.getDateAggiornameto())).build();
		} catch (NoResultException e) {
			return Response.noContent().build();
		} catch (Exception e) {
			return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}

	@GET
	@Path("aggiornamentiSun")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAggiornamenti(@QueryParam(value = "dataAggiornamento") String dataAggiornamento){
		try {
			return Response.ok(gson.toJson(aggiornamentoSunService.getAggiornamenti(dataAggiornamento))).build();
		} catch (NoResultException e) {
			return Response.noContent().build();
		} catch (Exception e) {
			return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).entity(e).build();
		}
	}
	
	
	@GET
	@Path("exportProgrammaMusicale")
	@Produces("application/csv")
	public Response exportPmList(@QueryParam(value = "dataBonifica") String dataBonifica)
			throws IOException {
		try {
			List<BonificaProgrammaMusicaleListToCsv> listToCsv= aggiornamentoSunService.getProgrammiMusicale(dataBonifica);
			StreamingOutput so = null;
			CustomMappingStrategy<BonificaProgrammaMusicaleListToCsv> customMappingStrategy = new CustomMappingStrategy<>(
					new BonificaProgrammaMusicaleListToCsv().getMappingStrategy());
			customMappingStrategy.setType(BonificaProgrammaMusicaleListToCsv.class);
			so = getListStreamingOutput(listToCsv, customMappingStrategy);
			Response response = Response.ok(so, "text/csv")
					.header("Content-Disposition", "attachment; filename=\"" + "Ricerca_PM.csv\"").build();
			return response;

		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(500).entity("Non risulta nessun dato per il periodo selezionato").build();
		}
	}

	@GET
	@Path("exportEventiPagati")
	@Produces("application/csv")
	public Response exportEventiPagati(@QueryParam(value = "dataBonifica") String dataBonifica)
			throws IOException {
		try {
			List<BonificaEventoPagatoListToCsv> listToCsv= aggiornamentoSunService.getEventiPagati(dataBonifica);
			StreamingOutput so = null;
			CustomMappingStrategy<BonificaEventoPagatoListToCsv> customMappingStrategy = new CustomMappingStrategy<>(
					new BonificaEventoPagatoListToCsv().getMappingStrategy());
			customMappingStrategy.setType(BonificaEventoPagatoListToCsv.class);
			so = getListStreamingOutput(listToCsv, customMappingStrategy);
			Response response = Response.ok(so, "text/csv")
					.header("Content-Disposition", "attachment; filename=\"" + "Ricerca_PM.csv\"").build();
			return response;

		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(500).entity("Non risulta nessun dato per il periodo selezionato").build();
		}
	}

	@GET
	@Path("exportMovimentiContabili")
	@Produces("application/csv")
	public Response exportMovimentiContabili(@QueryParam(value = "dataBonifica") String dataBonifica)
			throws IOException {
		try {
			List<BonificaMovimentoContabileListToCsv> listToCsv= aggiornamentoSunService.getMovimentiContabili(dataBonifica);
			StreamingOutput so = null;
			CustomMappingStrategy<BonificaMovimentoContabileListToCsv> customMappingStrategy = new CustomMappingStrategy<>(
					new BonificaMovimentoContabileListToCsv().getMappingStrategy());
			customMappingStrategy.setType(BonificaMovimentoContabileListToCsv.class);
			so = getListStreamingOutput(listToCsv, customMappingStrategy);
			Response response = Response.ok(so, "text/csv")
					.header("Content-Disposition", "attachment; filename=\"" + "Ricerca_PM.csv\"").build();
			return response;

		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(500).entity("Non risulta nessun dato per il periodo selezionato").build();
		}
	}

	@GET
	@Path("exportManifestazioni")
	@Produces("application/csv")
	public Response exportManifestazioni(@QueryParam(value = "dataBonifica") String dataBonifica)
			throws IOException {
		try {
			List<BonificaManifestazioneListToCsv> listToCsv= aggiornamentoSunService.getManifestazioni(dataBonifica);
			StreamingOutput so = null;
			CustomMappingStrategy<BonificaManifestazioneListToCsv> customMappingStrategy = new CustomMappingStrategy<>(
					new BonificaManifestazioneListToCsv().getMappingStrategy());
			customMappingStrategy.setType(BonificaManifestazioneListToCsv.class);
			so = getListStreamingOutput(listToCsv, customMappingStrategy);
			Response response = Response.ok(so, "text/csv")
					.header("Content-Disposition", "attachment; filename=\"" + "Ricerca_PM.csv\"").build();
			return response;

		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(500).entity("Non risulta nessun dato per il periodo selezionato").build();
		}
	}
	
	@GET
	@Path("exportDirettoriEsecuzione")
	@Produces("application/csv")
	public Response exportDirettoriEsecuzione(@QueryParam(value = "dataBonifica") String dataBonifica)
			throws IOException {
		try {
			List<BonificaDirettoreListToCsv> listToCsv= aggiornamentoSunService.getDirettoriEsecuzione(dataBonifica);
			StreamingOutput so = null;
			CustomMappingStrategy<BonificaDirettoreListToCsv> customMappingStrategy = new CustomMappingStrategy<>(
					new BonificaDirettoreListToCsv().getMappingStrategy());
			customMappingStrategy.setType(BonificaDirettoreListToCsv.class);
			so = getListStreamingOutput(listToCsv, customMappingStrategy);
			Response response = Response.ok(so, "text/csv")
					.header("Content-Disposition", "attachment; filename=\"" + "Ricerca_PM.csv\"").build();
			return response;

		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(500).entity("Non risulta nessun dato per il periodo selezionato").build();
		}
	}
	
	private <T> StreamingOutput getListStreamingOutput(final List<T> obj, final CustomMappingStrategy mappingStrategy) {
		StreamingOutput so = new StreamingOutput() {
			@Override
			public void write(OutputStream outputStream) throws IOException, WebApplicationException {
				OutputStreamWriter osw = new OutputStreamWriter(outputStream);

				StatefulBeanToCsv beanToCsv = new StatefulBeanToCsvBuilder(osw).withMappingStrategy(mappingStrategy)
						.withSeparator(';').build();
				try {
					beanToCsv.write(obj);
					osw.flush();
					outputStream.flush();
				} catch (CsvDataTypeMismatchException e) {
					System.out.println(e);
				} catch (CsvRequiredFieldEmptyException e) {
					System.out.println(e);
				} finally {
					if (osw != null)
						osw.close();
				}
			}
		};
		return so;
	}

	
}