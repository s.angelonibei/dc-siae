package com.alkemytech.sophia.codman.rest;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.alkemytech.sophia.codman.dto.*;
import com.alkemytech.sophia.codman.entity.DsrProcessConfig;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.alkemytech.sophia.codman.service.dsrmetadata.period.DsrPeriod;
import com.alkemytech.sophia.codman.utils.Periods;
import com.alkemytech.sophia.codman.utils.QueryUtils;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.google.gson.JsonObject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.config.ResultType;
import org.eclipse.persistence.sessions.Record;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
@Path("dsr-processing")
public class DsrProcessingController {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	
	
	
	private final Gson gson;
	private final SQS sqs;
	private final Properties configuration;
	private final Provider<EntityManager> provider;
	private final DsrMetadataService dsrMetadataService;

	@Inject
	protected DsrProcessingController(@McmdbDataSource Provider<EntityManager> provider, Gson gson, SQS sqs,
									  @Named("configuration") Properties configuration,DsrMetadataService dsrMetadataService ) {
		super();
		this.provider = provider;
		this.gson = gson;
		this.sqs = sqs;
		this.configuration=configuration;
		this.dsrMetadataService=dsrMetadataService;
	}


		static final DsrProcessingStateDTO[] processingStatuses = {
				DsrProcessingStateDTO.statusFromEnum(DsrProcessingStateDTO.STATUS.ARRIVED),
				DsrProcessingStateDTO.statusFromEnum(DsrProcessingStateDTO.STATUS.PROCESSING),
				DsrProcessingStateDTO.statusFromEnum(DsrProcessingStateDTO.STATUS.COMPLETED),
				DsrProcessingStateDTO.statusFromEnum(DsrProcessingStateDTO.STATUS.ERROR)
	};
	
	
	@GET
	@Path("/processing-statuses")
	@Produces(MediaType.APPLICATION_JSON)
	public Response statuses() throws IOException {
		try {
			Map<String,Object> response = new HashMap<String, Object>();
			List<DsrProcessingStateDTO> statuses = new ArrayList<DsrProcessingStateDTO>();
			for(DsrProcessingStateDTO item: processingStatuses) {
				statuses.add(item);
			}
			response.put("statuses", statuses);
			return Response.ok(gson.toJson(response)).build();
		} catch (Exception e) {
			logger.error("Search Request Exception", e);
			return Response.status(500).build();
		}
	}

	@POST
	@Path("/search")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response search(DsrSearchRequestDTO request) throws IOException {
		try {
//			List<DsrProcessingDTO> subList = data.subList(request.getFirst(), Math.min(request.getLast(),data.size()));
			List<DsrProcessingDTO> subList = new ArrayList<>();
			EntityManager em = null;
			List<DsrProcessConfig> result;
			em = provider.get();


			boolean checkPeriod = false;
			Map<Integer, List<Integer>> quarters = new HashMap<>();
			String inCondition = "";

			Integer defaultMonthFrom = 0;
			Integer defaultYearFrom = 2000;
			Date date = new Date();
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(date);
			Integer defaultMonthTo = calendar.get(Calendar.MONTH);
			Integer defaultYearTo = calendar.get(Calendar.YEAR);

			if (request.getMonthFrom() > 0 || request.getMonthTo() > 0) {
				checkPeriod = true;

				if (request.getMonthFrom() > 0) {
					defaultMonthFrom = request.getMonthFrom();
					defaultYearFrom = request.getYearFrom();
				}

				if (request.getMonthTo() > 0) {
					defaultMonthTo = request.getMonthTo();
					defaultYearTo = request.getYearTo();

				}
				quarters = Periods.extractCorrespondingQuarters(defaultMonthFrom, defaultYearFrom, defaultMonthTo, defaultYearTo);

				int y = 0;
				for (int key : quarters.keySet()) {
					if (y++ != 0) {
						inCondition += " or ";
					}
					inCondition += " (M1.PERIOD_TYPE='quarter' and M1.PERIOD in ("
							+ StringUtils.join(quarters.get(key), ',') + ") and M1.YEAR = " + key + " )";

				}
				if (!inCondition.isEmpty()) {
					inCondition = " ( " + inCondition + " )";
				}

			}

			List<String> listaStati = new ArrayList<>();
			for (DsrProcessingStateDTO processingStatus : request.getProcessingStatuses()) {
				if(processingStatus.getId() != null)
					listaStati.add(processingStatus.getId());
			}

			

//			EntityManager entityManager = null;
//			List<Object[]> result;

			String bcLikeCondition = "";
//			if(backclaim ==1 )
//				bcLikeCondition="%_BC_A_%";
//			else if (backclaim == 2)
//				bcLikeCondition="%_BC_B_%";
//			else if (backclaim == 3)
//				bcLikeCondition="%_BC_C1_%";
//			else if (backclaim == 4)
//				bcLikeCondition="%_BC_C2_%";
			

//				entityManager = provider.get();
			
			String selectCountSql="select count(*) TOTAL,\n" +
					"       sum(case when STATUS= 'ERROR' then 1 else 0 end) ERROR,\n" +
					"       sum(case when STATUS= 'COMPLETED' then 1 else 0 end) COMPLETED,\n" +
					"       sum(case when STATUS= 'ARRIVED' then 1 else 0 end) ARRIVED,\n" +
					"       sum(case when STATUS= 'PROCESSING' then 1 else 0 end) PROCESSING ";
			
			String selectSql = ""
					+ "SELECT  " +
					" y.STATUS, "
					+ "y.FILE_PATH, "
					+ "y.INSERT_TIME, "
					+ "y.DSP, "
					+ "y.DSR, "
					+ "y.`DATA RICEZIONE DSR`, "
					+ "y.PERIOD_TYPE, "
					+ "y.PERIOD, "
					+ "y.YEAR, "
					+ "y.PERIODO, "
					+ "y.TERRITORIO, "
					+ "y.`TIPO UTILIZZO`, "
					+ "y.`OFFERTA COMMERCIALE`, "
					+ "y.`TOTALE VALORE (€)`, "
					+ "y.CURRENCY, "
					+ "y.`TOTALE UTILIZZAZIONI`, "
					+ "y.`% IDENTIFICATO UTILIZZAZIONI`, "
					+ "y.BACKCLAIM, "
					+ "y.`DATA PRODUZIONE CCID`, "
					+ "y.EXTENDED_DSR_URL, "
					+ "y.DRILLDOWN, "
					+ "y.DATA_INIZIO, "
					+ "y.IDENTIFY_KB_VERSION, "
					+ "y.ERROR_MESSAGE, "
					+ "y.DATA_FINE, "
					+ "CCID_S3_PATH.ID_CCID_PATH, "
					+ "CCID_S3_PATH.IDDSR, "
					+ "CCID_S3_PATH.S3_PATH, "
					+ "CCID_METADATA.ID_CCID, "
					+ "CCID_METADATA.ID_DSR, "
					+ "CCID_METADATA.TOTAL_VALUE, "
					+ "CCID_METADATA.ID_CCID_METADATA, "
					+ "CCID_METADATA.CURRENCY, "
					+ "CCID_METADATA.CCID_VERSION, "
					+ "CCID_METADATA.CCID_ENCODED, "
					+ "CCID_METADATA.CCID_ENCODED_PROVISIONAL, "
					+ "CCID_METADATA.CCID_NOT_ENCODED, "
					+ "CCID_METADATA.INVOICE_STATUS, "
					+ "CCID_METADATA.INVOICE_ITEM, "
					+ "CCID_METADATA.VALORE_FATTURABILE, "
					+ "CCID_METADATA.SUM_USE_QUANTITY, "
					+ "CCID_METADATA.SUM_USE_QUANTITY_MATCHED, "
					+ "CCID_METADATA.SUM_USE_QUANTITY_UNMATCHED, "
					+ "CCID_METADATA.SUM_AMOUNT_LICENSOR, "
					+ "CCID_METADATA.SUM_AMOUNT_PAI, "
					+ "CCID_METADATA.SUM_AMOUNT_UNMATCHED, "
					+ "CCID_METADATA.SUM_USE_QUANTITY_SIAE, "
					+ "CCID_METADATA.VALORE_RESIDUO, "
					+ "CCID_METADATA.QUOTA_FATTURA, "
					+ "CCID_METADATA.IDENTIFICATO_VALORE_PRICING, "
					+ "CCID_METADATA.TOTAL_VALUE_CCID_CURRENCY, "
					+ "       case when  (y.DSR like '%_BC_A_%' or y.DSR like '%_BC0%') then  'A' " +
					"           when y.DSR like '%_BC_B_%' then 'B' " +
					"           when y.DSR like '%_BC_C1_%' then 'C1' " +
					"           when y.DSR like '%_BC_C2_%' then 'C2' " +
					"           else '' end  as tipoBc ";
			
			
			
				String fromSql =  " from "
						+ "(select "
						+ " case when FILE.LAST_START_TIME > now() - interval 5 minute then 'PROCESSING' " +
						" 			when (EXTRACT_STATUS = 'KO'\n" +
						"                  or CLEAN_STATUS = 'KO'\n" +
						"                  or IDENTIFY_STATUS = 'KO'\n" +
						"                  or CLAIM_STATUS = 'KO') then 'ERROR'\n" +
						"          when (CLAIM_STATUS = 'OK') then 'COMPLETED'\n" +
						"          when (DSR_STEPS_MONITORING.IDDSR is null) then 'ARRIVED'\n" +
						"          else 'PROCESSING' END as STATUS, "
						+ " FILE.FILE_PATH, "
						+ " FILE.INSERT_TIME, "
						+ "IF(ANAG_DSP.NAME IS NULL , '', ANAG_DSP.NAME ) AS DSP, "
						+ "FILE.IDDSR AS DSR, "
						+ "IF (DSR_STEPS_MONITORING.EXTRACT_QUEUED IS NULL,' ',DATE_FORMAT (CONVERT_TZ (DSR_STEPS_MONITORING.EXTRACT_QUEUED,'+00:00','+01:00'),'%d-%m-%Y %H:%i')) AS 'DATA RICEZIONE DSR', "
						+ "M1.PERIOD_TYPE, "
						+ "M1.PERIOD, "
						+ "M1.YEAR, "
						+ "COALESCE(DSR_STEPS_MONITORING.EXTRACT_QUEUED,DSR_STEPS_MONITORING.CLEAN_QUEUED ) DATA_INIZIO, "
						+ "DSR_STEPS_MONITORING.CLAIM_FINISHED DATA_FINE, "
						+ "DSR_STEPS_MONITORING.ERROR_MESSAGE, "
						+ "DSR_STEPS_MONITORING.IDENTIFY_KB_VERSION, "
						+ "(CASE "
						+ "WHEN M1.PERIOD_TYPE = 'month' THEN "
						+ "concat(ELT(M1.PERIOD, 'Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov','Dic') , ' ', M1.YEAR) "
						+ "ELSE concat(ELT(M1.PERIOD, 'Gen - Mar', 'Apr - Giu', 'Lug - Set', 'Ott - Dic') , ' ', M1.YEAR)       END) AS PERIODO, "
						+ "IF(M1.COUNTRY IS NULL , ' ', M1.COUNTRY ) AS 'TERRITORIO', "
						+ "IF(ANAG_UTILIZATION_TYPE.NAME IS NULL , ' ', ANAG_UTILIZATION_TYPE.NAME) AS 'TIPO UTILIZZO', "
						+ "IF(COMMERCIAL_OFFERS.OFFERING IS NULL , ' ', COMMERCIAL_OFFERS.OFFERING) as 'OFFERTA COMMERCIALE', "
						+ "coalesce (BC_STAT.TOTALE_VALORE ,IF(M1.TOTAL_VALUE IS NULL , ' ', ROUND(M1.TOTAL_VALUE , 2) )) as 'TOTALE VALORE (€)', "
						+ "M1.CURRENCY as 'CURRENCY', "
						+ "coalesce (BC_STAT.TOTALE_UTILIZZAZIONI, IF(M1.SALES_LINES_NUM IS NULL , ' ', M1.SALES_LINES_NUM )) as 'TOTALE UTILIZZAZIONI', "
						+ "coalesce ( IF(S1.SALES_LINE_RECOGNIZED IS NULL OR (BC_STAT.TOTALE_UTILIZZAZIONI IS NULL OR BC_STAT.TOTALE_UTILIZZAZIONI = 0), null , round((S1.SALES_LINE_RECOGNIZED * 100) / BC_STAT.TOTALE_UTILIZZAZIONI , 2))," +
						" IF(S1.SALES_LINE_RECOGNIZED IS NULL OR (M1.SALES_LINES_NUM IS NULL OR M1.SALES_LINES_NUM = 0), ' ', round((S1.SALES_LINE_RECOGNIZED * 100) / M1.SALES_LINES_NUM , 2))) as '% IDENTIFICATO UTILIZZAZIONI', "
						+ "M1.BACKCLAIM, "
						+ "IF (DSR_STEPS_MONITORING.CLAIM_FINISHED IS NULL,' ',DATE_FORMAT (CONVERT_TZ (DSR_STEPS_MONITORING.CLAIM_FINISHED,'+00:00','+01:00'),'%d-%m-%Y %H:%i')) AS 'DATA PRODUZIONE CCID', "
						+ "M1.EXTENDED_DSR_URL, "
						+ "IF((SELECT COUNT(ID) FROM CCID_METADATA_DETAILS d WHERE M1.IDDSR = d.ID_DSR) > 0, true,false) AS DRILLDOWN "
//						+ "from "
						+ "from DSR_METADATA_FILE FILE " 
						+ " left join DSR_STEPS_MONITORING on DSR_STEPS_MONITORING.IDDSR=FILE.IDDSR  "
						+ "  left join "
						+ " DSR_METADATA M1 on M1.IDDSR= FILE.IDDSR "
						+ " left join MM_BACKCLAIM_STATISTICS BC_STAT on BC_STAT.ID_DSR = M1.IDDSR "
						+ " left join DSR_STATISTICS S1 on  S1.IDDSR = M1.IDDSR "
						+ " left join COMMERCIAL_OFFERS on  M1.SERVICE_CODE = COMMERCIAL_OFFERS.ID_COMMERCIAL_OFFERS "
						+ " left join ANAG_UTILIZATION_TYPE on  COMMERCIAL_OFFERS.ID_UTIILIZATION_TYPE = ANAG_UTILIZATION_TYPE.ID_UTILIZATION_TYPE "
						+ " left join ANAG_DSP on  COMMERCIAL_OFFERS.IDDSP = ANAG_DSP.IDDSP "
//						+ "where DSR_STEPS_MONITORING.IDDSR=M1.IDDSR and S1.IDDSR = M1.IDDSR "
//						+ "and M1.SERVICE_CODE = COMMERCIAL_OFFERS.ID_COMMERCIAL_OFFERS "
//						+ "and COMMERCIAL_OFFERS.ID_UTIILIZATION_TYPE = ANAG_UTILIZATION_TYPE.ID_UTILIZATION_TYPE "
//						+ "and COMMERCIAL_OFFERS.IDDSP = ANAG_DSP.IDDSP "
						+ " where 1=1 "
						+ " and FILE.IDDSR like '%"+request.getIdDsr()+"%'"
						+ (!request.getDspList().isEmpty() ? "and COMMERCIAL_OFFERS.IDDSP in ( " + QueryUtils.getValuesCollection(request.getDspList()) + " )" : "")
						+ (!request.getCountryList().isEmpty() ? "and M1.COUNTRY in ( " + QueryUtils.getValuesCollection(request.getCountryList()) + " )" : "")
						+ (!request.getUtilizationList().isEmpty() ? "and COMMERCIAL_OFFERS.ID_UTIILIZATION_TYPE in ( " + QueryUtils.getValuesCollection(request.getUtilizationList()) + " )" : "")
						+ (!request.getOfferList().isEmpty() ? "and COMMERCIAL_OFFERS.OFFERING in ( " + QueryUtils.getValuesCollection(request.getOfferList()) + " )" : "")
						+ (!inCondition.isEmpty() ? "and (" + inCondition + " or " : "")
						+ (checkPeriod && inCondition.isEmpty() ? " and " : "")
						+ (checkPeriod ? " (PERIOD_TYPE='month' and  STR_TO_DATE(CONCAT(M1.YEAR,'-', M1.PERIOD , '-01'), '%Y-%m-%d') >= '" + defaultYearFrom + "-" + defaultMonthFrom + "-01' and STR_TO_DATE(CONCAT(M1.YEAR,'-', M1.PERIOD , '-01'), '%Y-%m-%d') <= '" + defaultYearTo + "-" + defaultMonthTo + "-01' ) " : "")
						+ (checkPeriod && !quarters.isEmpty() ? ")" : "")
//						+ (backclaim != -1 ? " and (M1.IDDSR " + (backclaim == 1 ? " LIKE '%_BC0%' or M1.IDDSR LIKE '%_BC_A_%')  " : "LIKE '"+bcLikeCondition+"') ") : " ")
						+ " ) y "
						+ "left join CCID_S3_PATH "
						+ "on y.DSR = CCID_S3_PATH.IDDSR "
						+ "left join CCID_METADATA "
						+ "on y.DSR = CCID_METADATA.ID_DSR "

						+ ((!listaStati.isEmpty()) ? " WHERE STATUS in ( " + QueryUtils.getValuesCollection(listaStati) + " )" : "")		;
				
				
				
						String orderSql =  " ORDER BY "+ request.getDecodedSort();
				
				final Query qCount = em.createNativeQuery(selectCountSql + fromSql);
				Record countRecord = (Record) qCount.setHint(QueryHints.RESULT_TYPE, ResultType.Map).getSingleResult();

				final Query q = em.createNativeQuery(selectSql+fromSql + orderSql).setFirstResult(request.getFirst()) // offset
						.setMaxResults(request.getLast() - request.getFirst());

				List<Record> ccidS3Paths = q.setHint(QueryHints.RESULT_TYPE, ResultType.Map).getResultList();
				List<DspStatiticsDTO> dtoList = new ArrayList<DspStatiticsDTO>();
				final PagedResult pagedResult = new PagedResult();

				
				for (Record r : ccidS3Paths) {
					DsrProcessingDTO data = new DsrProcessingDTO();
					data.setDspName((String) r.get("DSP"));
					data.setDsr((String) r.get("DSR"));
					data.setDsrFilePath((String) r.get("FILE_PATH"));
					DsrPeriod periodo = new DsrPeriod();
					periodo.setPeriod(((Integer) r.get("PERIOD")) != null ? ((Integer) r.get("PERIOD")).toString(): null);
					periodo.setYear(((Integer) r.get("YEAR")) != null ? ((Integer) r.get("YEAR")).toString(): null);
					periodo.setPeriodType((String) r.get("PERIOD_TYPE"));
					data.setDsrPeriod(periodo);
					data.setUtilizationName((String) r.get("TIPO UTILIZZO"));
					data.setCommercialOfferName((String) r.get("OFFERTA COMMERCIALE"));
					data.setCountryName((String)r.get("TERRITORIO"));

					data.setDsrDeliveryDate((Date)r.get("INSERT_TIME"));
					data.setDsrProcessingStart((Date)r.get("DATA_INIZIO"));
					data.setDsrProcessingEnd((Date)r.get("DATA_FINE"));
					
					data.setKbVersion(((String)r.get("IDENTIFY_KB_VERSION")));
					data.setErrorType((String)r.get("ERROR_MESSAGE"));
					
					
					data.setProcessingStatus(DsrProcessingStateDTO.statusFromEnum(DsrProcessingStateDTO.STATUS.valueOf((String)r.get("STATUS"))));

					subList.add(data);
				}


				StatDTO s1 = new StatDTO("Arrivati", ((BigDecimal) countRecord.get("ARRIVED")) != null ? ((BigDecimal) countRecord.get("ARRIVED")).toString(): null);
				StatDTO s3 = new StatDTO("In Lavorazione", ((BigDecimal) countRecord.get("PROCESSING")) != null ? ((BigDecimal) countRecord.get("PROCESSING")).toString(): null);
				StatDTO s2 = new StatDTO("Completati", ((BigDecimal) countRecord.get("COMPLETED")) != null ? ((BigDecimal) countRecord.get("COMPLETED")).toString(): null);
				StatDTO s4 = new StatDTO("In Errore", ((BigDecimal) countRecord.get("ERROR")) != null ? ((BigDecimal) countRecord.get("ERROR")).toString(): null);
				StatDTO s5 = new StatDTO("Totale", ((Long) countRecord.get("TOTAL")) != null ? ((Long) countRecord.get("TOTAL")).toString(): null);
				List<StatDTO> stats = Arrays.asList(s1, s2, s3, s4, s5);
				Map<String, Object> extra = new HashMap<String, Object>();
				extra.put("stats", stats);
				PagedResult page = new PagedResult();
				page.setExtra(extra);
				page.setRows(subList);
				page.setMaxrows(((Long) countRecord.get("TOTAL")).intValue());
				page.setFirst(request.getFirst());
				page.setLast(request.getFirst()+ccidS3Paths.size());
				page.setHasNext(request.getLast() < (Long) countRecord.get("TOTAL"));
				page.setHasPrev(request.getFirst() > 0);
				return Response.ok(gson.toJson(page)).build();
			} catch (Exception e) {
				logger.error("Search Request Exception", e);
				return Response.status(500).build();
			}
		
	}

	private String getDspFromKey(String key, String originalSourcePath){

		String[] split = key.split("/");

		for(int i=0; i<split.length;i++){
			if(split[i].equalsIgnoreCase(originalSourcePath)){
				//dsp
				return split[i+1];
			}
		}
		return null;
	}
	
	private String checkIfRunDSR(String pathIddsr){
		
		

		EntityManager em = provider.get();
		
		try{

			String originalSourceFolder = "original-source-debug";
			
			String idDsr = pathIddsr.substring(0,pathIddsr.lastIndexOf("_V.csv")).substring(pathIddsr.lastIndexOf("/")+1);
//			String path = pathIddsr.substring(0,pathIddsr.lastIndexOf("/")+1);
			//s3://siae-sophia-original-source-debug/spotify/201902/spotify_siae_LT_201902_basic-desktop_dsr_V.csv
			int startDsp= pathIddsr.indexOf("/",5)+1;
			int endDsp = pathIddsr.indexOf("/",startDsp);

			String dsp = pathIddsr.substring(startDsp,endDsp);
		
			DsrMetadataDTO dsrMetadataDTO = dsrMetadataService.extractMetadataQuery(dsp, idDsr, em);

			if(dsrMetadataDTO == null){
				return "Impossibile caricare la configurazione";
			}
			else if(dsrMetadataDTO.getStatus().equalsIgnoreCase("KO")){
				return dsrMetadataDTO.getErrorMessage();
			}
			//get info dsr MONTH and YEAR
			
			String year = dsrMetadataDTO.getPeriodStartDate().substring(0,4);
			String month = dsrMetadataDTO.getPeriodStartDate().substring(4,6);
			
			//check SCHEMI DI RIPARTO
			
			//CHECK KB UPDATE IN PROGRESS

			String sqlCheckQueue="select QUEUE_TYPE\n" +
					"from SQS_SERVICE_BUS\n" +
					"where QUEUE_TYPE in ( 'completed','failed', 'started','to_process')\n" +
					"and SERVICE_NAME= '"+configuration.getProperty("queue_schemi_riparto")+"' \n" +
					"and MONTH= ? \n" +
					"and year= ? \n" +
					"order by INSERT_TIME DESC limit 1;";

			List resultList = em.createNativeQuery(sqlCheckQueue)
					.setParameter(1, month)
					.setParameter(2, year)
					.getResultList();


			if(resultList == null || resultList.size() == 0  
					|| "completed".equalsIgnoreCase((String)resultList.get(0))
					|| "failed".equalsIgnoreCase((String)resultList.get(0)))
				return null;
			else{
				return "schemi di riparto in lavorazione coda ("+configuration.getProperty("queue_schemi_riparto")+")";
			}
		}catch(Exception e){
			logger.error("error in checkDSR", e);
			return e.getMessage();
			
		}
		
		
		
	}


	@POST
	@Path("/submit-dsrs-processing")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response submitProcessing(DsrProcessingSubmitRequestDTO request) throws IOException {
		try {
			DsrProcessingSubmitResponsetDTO response = new DsrProcessingSubmitResponsetDTO();
			for(DsrProcessingDTO dto : request.getDsrs()) {
				
				
				//verifica lo stato effettivo delle lavorazioni
				//verifica lo stato di eventuali schemi di riparto in corso

				
				String error = checkIfRunDSR(dto.getDsrFilePath());
				if(error != null) {
					
					JsonObject responseJs = new JsonObject();
					responseJs.addProperty("error","Errore il DSR "+dto.getDsr() + " non può essere lanciato: "+ error);
					return Response.status(500).entity(responseJs.toString()).build();
				}
				
				
				//if file_path void -> allora invia so extract come coda
				//altrimenti sophia-tools
				
				
				dto.setProcessingStatus(DsrProcessingStateDTO.statusFromEnum(DsrProcessingStateDTO.STATUS.PROCESSING));
				
				//set timeout on table DSR_METDATA_FILE
				
				EntityManager em = provider.get();
				em.getTransaction().begin();
				em.createNativeQuery("UPDATE DSR_METADATA_FILE SET LAST_START_TIME= ? WHERE IDDSR = ?")
				.setParameter(1,new Date())
				.setParameter(2,dto.getDsr())
				.executeUpdate();
				
				em.getTransaction().commit();
				
				
			}

			for(DsrProcessingDTO dto : request.getDsrs()) {
				if(dto.getDsrFilePath().isEmpty()){
					//lancio extract
					JsonObject messageBody = new JsonObject();
					messageBody.addProperty("file_path",dto.getDsrFilePath());
					messageBody.addProperty("idDsr",dto.getDsr());

					SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration, "extract_ccid.sqs");
					sqsMessagePump.sendToProcessMessage(messageBody, false);
				}else{
					//lancio sophia_tools

					JsonObject messageBody = new JsonObject();
					messageBody.addProperty("file_path",dto.getDsrFilePath());
					messageBody.addProperty("idDsr",dto.getDsr());

					SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration, "avvio_ccid.sqs");
					sqsMessagePump.sendToProcessMessage(messageBody, false);
				}
			}



			JsonObject responseJs = new JsonObject();
			responseJs.addProperty("message",String.format("Processo avviato per (%d) DSR", request.getDsrs().size()));
			return Response.ok(responseJs.toString()).build();
		} catch (Exception e) {
			logger.error("Search Request Exception", e);
			return Response.status(500).build();
		}
	}



}
