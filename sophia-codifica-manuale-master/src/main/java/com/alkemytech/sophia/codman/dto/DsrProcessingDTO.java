package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.codman.service.dsrmetadata.period.DsrPeriod;

@XmlRootElement
public class DsrProcessingDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String dsr;
	private String dsp;
	private String dspName;
	private String utilizationType;
	private String utilizationName;
	private String commercialOffer;
	private String commercialOfferName;
	private DsrPeriod dsrPeriod;
	private String country;
	private String countryName;
	private Date dsrDeliveryDate;
	private Date dsrProcessingStart;
	private Date dsrProcessingEnd;
	private DsrProcessingStateDTO processingStatus;
	private String errorType;
	private String kbVersion;
	private String dsrFilePath;


	

	public String getDsrFilePath() {
		return dsrFilePath;
	}

	public void setDsrFilePath(String dsrFilePath) {
		this.dsrFilePath = dsrFilePath;
	}

	public String getDsr() {
		return dsr;
	}

	public void setDsr(String dsr) {
		this.dsr = dsr;
	}

	public String getDsp() {
		return dsp;
	}

	public void setDsp(String dsp) {
		this.dsp = dsp;
	}

	public String getDspName() {
		return dspName;
	}

	public void setDspName(String dspName) {
		this.dspName = dspName;
	}

	public String getUtilizationType() {
		return utilizationType;
	}

	public void setUtilizationType(String utilizationType) {
		this.utilizationType = utilizationType;
	}

	public String getUtilizationName() {
		return utilizationName;
	}

	public void setUtilizationName(String utilizationName) {
		this.utilizationName = utilizationName;
	}

	public String getCommercialOffer() {
		return commercialOffer;
	}

	public void setCommercialOffer(String commercialOffer) {
		this.commercialOffer = commercialOffer;
	}

	public String getCommercialOfferName() {
		return commercialOfferName;
	}

	public void setCommercialOfferName(String commercialOfferName) {
		this.commercialOfferName = commercialOfferName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public Date getDsrDeliveryDate() {
		return dsrDeliveryDate;
	}

	public void setDsrDeliveryDate(Date dsrDeliveryDate) {
		this.dsrDeliveryDate = dsrDeliveryDate;
	}

	public Date getDsrProcessingStart() {
		return dsrProcessingStart;
	}

	public void setDsrProcessingStart(Date dsrProcessingStart) {
		this.dsrProcessingStart = dsrProcessingStart;
	}

	public Date getDsrProcessingEnd() {
		return dsrProcessingEnd;
	}

	public void setDsrProcessingEnd(Date dsrProcessingEnd) {
		this.dsrProcessingEnd = dsrProcessingEnd;
	}

	public DsrProcessingStateDTO getProcessingStatus() {
		return processingStatus;
	}

	public void setProcessingStatus(DsrProcessingStateDTO processingStatus) {
		this.processingStatus = processingStatus;
	}

	public String getKbVersion() {
		return kbVersion;
	}

	public void setKbVersion(String kbVersion) {
		this.kbVersion = kbVersion;
	}

	public String getErrorType() {
		return errorType;
	}

	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}

	public DsrPeriod getDsrPeriod() {
		return dsrPeriod;
	}

	public void setDsrPeriod(DsrPeriod dsrPeriod) {
		this.dsrPeriod = dsrPeriod;
	}
	
	

}
