package com.alkemytech.sophia.codman.invoice.service;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.codman.entity.CCIDMetadata;
import com.alkemytech.sophia.codman.invoice.repository.InvoiceItem;
import com.alkemytech.sophia.codman.invoice.repository.InvoiceLog;
import com.alkemytech.sophia.invoice.integration.service.Tracer;
import com.google.common.primitives.Bytes;
import com.google.inject.Provider;

public class InvoiceTracer implements Tracer {

	private final Provider<EntityManager> provider;
	private final Logger logger = LoggerFactory.getLogger(getClass());

	public InvoiceTracer(Provider<EntityManager> provider) {
		this.provider = provider;
	}

	@Override
	public void trace(String message, String requestId, Integer idInvoice) {
		EntityManager em = null;
		InvoiceLog invoiceLog = null;
		try {
			em = provider.get();
			String queryString = "select x from InvoiceLog x where x.requestId = :requestId ";
			Query query = em.createQuery(queryString);
			query.setParameter("requestId", requestId);

			invoiceLog = (InvoiceLog) query.getSingleResult();

			// esiste tracciamento per il messaggio, quindi bisogna inserire la
			// response
			long millis = new Date().getTime() - invoiceLog.getStartDate().getTime();

			String response = message.contains("\"KO\":false")? "KO: false": message;
			em.getTransaction().begin();
			invoiceLog.setResponseParams(response);
			invoiceLog.setDuration(millis);
			em.getTransaction().commit();

		} catch (NoResultException nRe) {

			try {
				// non esiste un tracciamento per il messaggio, quindi va creato
				invoiceLog = new InvoiceLog();
				invoiceLog.setRequestId(requestId);
				invoiceLog.setRequestParams(message);
				invoiceLog.setStartDate(new Date());
				invoiceLog.setIdInvoice(idInvoice);
				em.getTransaction().begin();
				em.persist(invoiceLog);
				em.getTransaction().commit();

			} catch (Exception e) {
				logger.error("invoice tracer", e);
			}

		} catch (Exception ex) {
			logger.error("invoice tracer", ex);

			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
		}
	}

}
