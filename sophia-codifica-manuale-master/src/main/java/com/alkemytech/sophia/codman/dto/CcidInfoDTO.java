package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;

@Produces("application/json")
@XmlRootElement
public class CcidInfoDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Boolean ccidEncoded;
	private Boolean ccidEncodedProvisional;
	private Boolean ccidNotEncoded;
	private String ccidVersion;
	private String commercialOffer;
	private String country;
	private String currency;
	private String dspName;
	private String idCCID;
	private Integer idCCIDmetadata;
	private Integer idCommercialOffer;
	private String idDsp;
	private String idDsr;
	private String idUtilizationType;
	private String invoiceStatus;
	private Integer period;
	private String periodString;
	private String periodType;
	private BigInteger totalValue;
	private BigInteger totalValueOrigCurrency;
	private String utilizationType;
	private Integer year;
	private BigDecimal invoiceAmount;
	private BigDecimal valoreResiduo;
	
	
	
	public BigDecimal getInvoiceAmount() {
		return invoiceAmount;
	}
	public void setInvoiceAmount(BigDecimal invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	public Boolean getCcidEncoded() {
		return ccidEncoded;
	}
	public void setCcidEncoded(Boolean ccidEncoded) {
		this.ccidEncoded = ccidEncoded;
	}
	public Boolean getCcidEncodedProvisional() {
		return ccidEncodedProvisional;
	}
	public void setCcidEncodedProvisional(Boolean ccidEncodedProvisional) {
		this.ccidEncodedProvisional = ccidEncodedProvisional;
	}
	public Boolean getCcidNotEncoded() {
		return ccidNotEncoded;
	}
	public void setCcidNotEncoded(Boolean ccidNotEncoded) {
		this.ccidNotEncoded = ccidNotEncoded;
	}
	public String getCcidVersion() {
		return ccidVersion;
	}
	public void setCcidVersion(String ccidVersion) {
		this.ccidVersion = ccidVersion;
	}
	public String getCommercialOffer() {
		return commercialOffer;
	}
	public void setCommercialOffer(String commercialOffer) {
		this.commercialOffer = commercialOffer;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getDspName() {
		return dspName;
	}
	public void setDspName(String dspName) {
		this.dspName = dspName;
	}
	public String getIdCCID() {
		return idCCID;
	}
	public void setIdCCID(String idCCID) {
		this.idCCID = idCCID;
	}
	public Integer getIdCCIDmetadata() {
		return idCCIDmetadata;
	}
	public void setIdCCIDmetadata(Integer idCCIDmetadata) {
		this.idCCIDmetadata = idCCIDmetadata;
	}
	public Integer getIdCommercialOffer() {
		return idCommercialOffer;
	}
	public void setIdCommercialOffer(Integer idCommercialOffer) {
		this.idCommercialOffer = idCommercialOffer;
	}
	public String getIdDsp() {
		return idDsp;
	}
	public void setIdDsp(String idDsp) {
		this.idDsp = idDsp;
	}
	public String getIdDsr() {
		return idDsr;
	}
	public void setIdDsr(String idDsr) {
		this.idDsr = idDsr;
	}
	public String getIdUtilizationType() {
		return idUtilizationType;
	}
	public void setIdUtilizationType(String idUtilizationType) {
		this.idUtilizationType = idUtilizationType;
	}
	public String getInvoiceStatus() {
		return invoiceStatus;
	}
	public void setInvoiceStatus(String invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}
	public Integer getPeriod() {
		return period;
	}
	public void setPeriod(Integer period) {
		this.period = period;
	}
	public String getPeriodString() {
		return periodString;
	}
	public void setPeriodString(String periodString) {
		this.periodString = periodString;
	}
	public String getPeriodType() {
		return periodType;
	}
	public void setPeriodType(String periodType) {
		this.periodType = periodType;
	}
	public BigInteger getTotalValue() {
		return totalValue;
	}
	public void setTotalValue(BigInteger totalValue) {
		this.totalValue = totalValue;
	}
	public BigInteger getTotalValueOrigCurrency() {
		return totalValueOrigCurrency;
	}
	public void setTotalValueOrigCurrency(BigInteger totalValueOrigCurrency) {
		this.totalValueOrigCurrency = totalValueOrigCurrency;
	}
	public String getUtilizationType() {
		return utilizationType;
	}
	public void setUtilizationType(String utilizationType) {
		this.utilizationType = utilizationType;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	
	
	
	public BigDecimal getValoreResiduo() {
		return valoreResiduo;
	}
	public void setValoreResiduo(BigDecimal valoreResiduo) {
		this.valoreResiduo = valoreResiduo;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CcidInfoDTO [ccidEncoded=");
		builder.append(ccidEncoded);
		builder.append(", ccidEncodedProvisional=");
		builder.append(ccidEncodedProvisional);
		builder.append(", ccidNotEncoded=");
		builder.append(ccidNotEncoded);
		builder.append(", ccidVersion=");
		builder.append(ccidVersion);
		builder.append(", commercialOffer=");
		builder.append(commercialOffer);
		builder.append(", country=");
		builder.append(country);
		builder.append(", currency=");
		builder.append(currency);
		builder.append(", dspName=");
		builder.append(dspName);
		builder.append(", idCCID=");
		builder.append(idCCID);
		builder.append(", idCCIDmetadata=");
		builder.append(idCCIDmetadata);
		builder.append(", idCommercialOffer=");
		builder.append(idCommercialOffer);
		builder.append(", idDsp=");
		builder.append(idDsp);
		builder.append(", idDsr=");
		builder.append(idDsr);
		builder.append(", idUtilizationType=");
		builder.append(idUtilizationType);
		builder.append(", invoiceStatus=");
		builder.append(invoiceStatus);
		builder.append(", period=");
		builder.append(period);
		builder.append(", periodString=");
		builder.append(periodString);
		builder.append(", periodType=");
		builder.append(periodType);
		builder.append(", totalValue=");
		builder.append(totalValue);
		builder.append(", totalValueOrigCurrency=");
		builder.append(totalValueOrigCurrency);
		builder.append(", utilizationType=");
		builder.append(utilizationType);
		builder.append(", year=");
		builder.append(year);
		builder.append("]");
		return builder.toString();
	}
	
	
	
}
