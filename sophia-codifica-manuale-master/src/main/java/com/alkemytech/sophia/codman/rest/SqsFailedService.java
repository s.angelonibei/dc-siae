package com.alkemytech.sophia.codman.rest;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.codman.entity.SqsFailed;
import com.alkemytech.sophia.codman.entity.SqsFailedStats;
import com.alkemytech.sophia.codman.entity.SqsFailedStatsDate;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.alkemytech.sophia.common.sqs.Iso8601Helper;
import com.alkemytech.sophia.common.sqs.SQSService;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

@Singleton
@Path("sqs-failed")
public class SqsFailedService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Provider<EntityManager> provider;
	private final Gson gson;
	private final SQSService sqsService;
	
	@Inject
	protected SqsFailedService(@McmdbDataSource Provider<EntityManager> provider, Gson gson, SQSService sqsService) {
		super();
		this.provider = provider;
		this.gson = gson;
		this.sqsService = sqsService;
	}

	@GET
	@Produces("application/json")
	public Response findAll() {
		final EntityManager entityManager = provider.get();
		final List<SqsFailed> result = entityManager
				.createQuery("select x from SqsFailed x", SqsFailed.class)
				.setMaxResults(32768)
				.getResultList();
		return Response.ok(gson.toJson(result)).build();
	}
	
	@GET
	@Path("{uuid}")
	@Produces("application/json")
	public Response findByUuid(@PathParam("uuid") String uuid) {
		final EntityManager entityManager = provider.get();
		final SqsFailed result = entityManager
				.find(SqsFailed.class, uuid);
		return Response.ok(gson.toJson(result)).build();
	}

	@GET
	@Path("stats")
	@Produces("application/json")
	public Response getStats() {
		final EntityManager entityManager = provider.get();
		final List<SqsFailedStats> result = entityManager
				.createNamedQuery("SqsFailedStats.FindAll", SqsFailedStats.class)
				.getResultList();
//		@SuppressWarnings("unchecked")
//		final List<SqsFailedStats> result = (List<SqsFailedStats>) entityManager
//				.createNativeQuery("select QUEUE_NAME, count(*) QUEUE_SIZE from SQS_FAILED group by QUEUE_NAME", SqsFailedStats.class)
//				.getResultList();
		return Response.ok(gson.toJson(result)).build();
	}

	@GET
	@Path("stats-date")
	@Produces("application/json")
	public Response getStatsDate() {
		final EntityManager entityManager = provider.get();
		final List<SqsFailedStatsDate> stats = entityManager
				.createNamedQuery("SqsFailedStatsDate.FindAll", SqsFailedStatsDate.class)
				.getResultList();
		final List<Map<String, Object>> result = new ArrayList<Map<String,Object>>();
		Date date = null;
		List<SqsFailedStatsDate> sqsFailedStats = null;
		for (SqsFailedStatsDate sqsFailedStat : stats) {
			if (!sqsFailedStat.getReceiveDate().equals(date)) {
				final Map<String, Object> resultItem = new HashMap<String, Object>();
				resultItem.put("receiveDate", date = sqsFailedStat.getReceiveDate());
				resultItem.put("sqsFailedStats", sqsFailedStats = new ArrayList<SqsFailedStatsDate>());
				result.add(resultItem);
			}
			sqsFailedStats.add(sqsFailedStat);
		}
		return Response.ok(gson.toJson(result)).build();
	}

	@GET
	@Path("queue-names")
	@Produces("application/json")
	public Response getQueueNames() {
		final EntityManager entityManager = provider.get();
		final List<String> result = entityManager
				.createQuery("select distinct x.queueName from SqsFailed x", String.class)
				.getResultList();
		return Response.ok(gson.toJson(result)).build();
	}
	
	@GET
	@Path("search")
	@Produces("application/json")
	public Response search(@QueryParam("receiveDate") String receiveDate,
			@QueryParam("queueName") String queueName,
			@QueryParam("json") String json, 
			@QueryParam("sortType") String sortType,
			@QueryParam("sortReverse") String sortReverse,
			@DefaultValue("0") @QueryParam("first") int first,
			@DefaultValue("50") @QueryParam("last") int last) {
		final EntityManager entityManager = provider.get();
		try {
			String separator = " where ";
			final StringBuffer sql = new StringBuffer()
				.append("select * from SQS_FAILED");
			if (!StringUtils.isEmpty(receiveDate)) {
				sql.append(separator).append("DATE(RECEIVE_TIME) = ?");
				separator = " and ";
			}
			if (!StringUtils.isEmpty(queueName)) {
				sql.append(separator).append("QUEUE_NAME = ?");
				separator = " and ";
			}
			if (!StringUtils.isEmpty(json)) {
				sql.append(separator).append("MESSAGE_JSON like ?");
				separator = " and ";
			}
			final String ascOrDesc = "true".equalsIgnoreCase(sortReverse) ? "DESC" : "ASC";
			if ("receiveTime".equalsIgnoreCase(sortType)) {
				sql.append(" order by RECEIVE_TIME ")
					.append(ascOrDesc);
			} else if ("queueName".equalsIgnoreCase(sortType)) {
				sql.append(" order by QUEUE_NAME ")
					.append(ascOrDesc);
			}
			sql.append(" limit ?, ?");
			logger.debug("search: sql {}", sql);
			int i = 1;
			final Query query = (Query) entityManager
				.createNativeQuery(sql.toString(), SqsFailed.class);
			if (!StringUtils.isEmpty(receiveDate)) {
				query.setParameter(i ++, new SimpleDateFormat("yyyy-MM-dd").parse(receiveDate));
			}
			if (!StringUtils.isEmpty(queueName)) {
				query.setParameter(i ++, queueName);
			}
			if (!StringUtils.isEmpty(json)) {
				query.setParameter(i ++, "%" + json + "%");
			}
			query.setParameter(i ++, first);
			query.setParameter(i ++, 1 + last - first);
			@SuppressWarnings("unchecked")
			final List<SqsFailed> results =
				(List<SqsFailed>) query.getResultList();
//			logger.debug("search: results {}", results);
			final PagedResult result = new PagedResult();
			if (null != results && !results.isEmpty()) {
				final int maxrows = last - first;
				final boolean hasNext = results.size() > maxrows;
				result.setRows(!hasNext ? results : results.subList(0, maxrows))
					.setMaxrows(maxrows)
					.setFirst(first)
					.setLast(hasNext ? last : first + results.size())
					.setHasNext(hasNext)
					.setHasPrev(first > 0);
			} else {
				result.setMaxrows(0)
					.setFirst(0)
					.setLast(0)
					.setHasNext(false)
					.setHasPrev(false);
			}
			return Response.ok(gson.toJson(result)).build();
		} catch (Exception e) {
			logger.error("search", e);
		}
		return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR).build();
	}
	
	@POST
	@Path("send-again")
	@Consumes("application/json")
	@Produces("application/json")
	public Response sendAgain(SqsFailed sqsFailed) {		
		try {
			final JsonObject message = gson
					.fromJson(sqsFailed.getMessageJson(), JsonObject.class);
			final JsonObject input = message
					.getAsJsonObject("input");
			final String queueName = input.getAsJsonObject("header")
					.get("queue").getAsString();
			final String queueUrl = sqsService.getUrl(queueName);
			sqsService.sendMessage(queueUrl, gson.toJson(input));
			return Response.ok().build();
		} catch (Exception e) {
			logger.error("sendAgain", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	@POST
	@Path("force-send")
	@Consumes("application/json")
	@Produces("application/json")
	public Response forceSend(SqsFailed sqsFailed) {		
		try {
			final JsonObject message = gson
					.fromJson(sqsFailed.getMessageJson(), JsonObject.class);
			final JsonObject input = message.getAsJsonObject("input");
			final JsonObject header = input.getAsJsonObject("header");
			header.addProperty("uuid", UUID.randomUUID().toString());
			header.addProperty("timestamp", Iso8601Helper.format(new Date()));
			final String queueName = header.get("queue").getAsString();
			final String queueUrl = sqsService.getUrl(queueName);
			sqsService.sendMessage(queueUrl, gson.toJson(input));
			return Response.ok().build();
		} catch (Exception e) {
			logger.error("forceSend", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
}
