package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;

import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Produces("application/json")
public class SearchImportiAnticipo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer first;
	private Integer last;
	private String dsp;
	private Integer advancePaymentCode;
	private Integer monthFrom;
	private Integer yearFrom;
	private Integer monthTo;
	private Integer yearTo;
	
	public SearchImportiAnticipo(){
		
	}
	
	public Integer getFirst() {
		return first;
	}
	public void setFirst(Integer first) {
		this.first = first;
	}
	public Integer getLast() {
		return last;
	}
	public void setLast(Integer last) {
		this.last = last;
	}
	public String getDsp() {
		return dsp;
	}
	public void setDsp(String dsp) {
		this.dsp = dsp;
	}
	public Integer getAdvancePaymentCode() {
		return advancePaymentCode;
	}
	public void setAdvancePaymentCode(Integer advancePaymentCode) {
		this.advancePaymentCode = advancePaymentCode;
	}
	public Integer getMonthFrom() {
		return monthFrom;
	}
	public void setMonthFrom(Integer monthFrom) {
		this.monthFrom = monthFrom;
	}
	public Integer getYearFrom() {
		return yearFrom;
	}
	public void setYearFrom(Integer yearFrom) {
		this.yearFrom = yearFrom;
	}
	public Integer getMonthTo() {
		return monthTo;
	}
	public void setMonthTo(Integer monthTo) {
		this.monthTo = monthTo;
	}
	public Integer getYearTo() {
		return yearTo;
	}
	public void setYearTo(Integer yearTo) {
		this.yearTo = yearTo;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SearchImportiAnticipo [first=");
		builder.append(first);
		builder.append(", last=");
		builder.append(last);
		builder.append(", dsp=");
		builder.append(dsp);
		builder.append(", advancePaymentCode=");
		builder.append(advancePaymentCode);
		builder.append(", monthFrom=");
		builder.append(monthFrom);
		builder.append(", yearFrom=");
		builder.append(yearFrom);
		builder.append(", monthTo=");
		builder.append(monthTo);
		builder.append(", yearTo=");
		builder.append(yearTo);
		builder.append("]");
		return builder.toString();
	}
	
}
