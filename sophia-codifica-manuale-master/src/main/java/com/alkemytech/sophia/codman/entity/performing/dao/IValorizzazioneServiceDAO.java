package com.alkemytech.sophia.codman.entity.performing.dao;

import java.util.List;

import com.alkemytech.sophia.codman.dto.performing.CarichiRipartizioneResponse;
import com.alkemytech.sophia.codman.dto.performing.CarichiRipartizioniRequest;
import com.alkemytech.sophia.codman.dto.performing.ConfigurazioniPeriodoDTO;
import com.alkemytech.sophia.codman.dto.performing.PerfValorizzazioneDTO;
import com.alkemytech.sophia.codman.entity.PeriodoRipartizione;
import com.alkemytech.sophia.codman.entity.performing.PerfVoceIncasso;


/**
 * Created by idilello on 6/21/16.
 */
public interface IValorizzazioneServiceDAO {


    public Integer addConfigurazione(PerfValorizzazioneDTO perfValorizzazioneJsonDTO);

    public List<ConfigurazioniPeriodoDTO> getConfigurazioni(String periodoRipartizione, String ripartizione, String tipologiaReport, String regola, String voceIncasso);
    
    public List<PerfVoceIncasso> getVociIncasso();
    
    public List<PeriodoRipartizione> getPeriodiRipartizione();

	public Integer updateConfigurazione(PerfValorizzazioneDTO campionamentoConfig);

	public List<ConfigurazioniPeriodoDTO> getStoricoConfigurazione(String periodo, String voceIncasso,
			String tipoReport);

	public CarichiRipartizioneResponse getCarichiRipartizione(String codicePeriodo);

	public Integer approvaRipartizioni(CarichiRipartizioniRequest carichiRipartizioniRequest);

	List<PerfVoceIncasso> getVociIncassoFittizie();


}
