package com.alkemytech.sophia.codman.entity.performing;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "PERF_VALORIZZAZIONE_RUN")
public class ValorizzazioneRun {

	@Id
    @Column(name="ID_VALORIZZAZIONE_RUN", nullable=false)
    private Long idValorizzazioneRun;

    @Column(name="DATA_INIZIO")
    private Date dataInizio;
    
    @Column(name="DATA_FINE")
    private Date dataFine;
    
    @Column(name="PERIODO_RIPARTIZIONE")
    private Long periodoRipartizione;
    
    @Column(name="INFO_RUN")
    private String infoRun;
    
    public ValorizzazioneRun() {
    	super();
    }

	public ValorizzazioneRun(Long idValorizzazioneRun, Date dataInizio, Date dataFine, Long periodoRipartizione,
			String infoRun) {
		super();
		this.idValorizzazioneRun = idValorizzazioneRun;
		this.dataInizio = dataInizio;
		this.dataFine = dataFine;
		this.periodoRipartizione = periodoRipartizione;
		this.infoRun = infoRun;
	}

	public Long getIdValorizzazioneRun() {
		return idValorizzazioneRun;
	}

	public void setIdValorizzazioneRun(Long idValorizzazioneRun) {
		this.idValorizzazioneRun = idValorizzazioneRun;
	}

	public Date getDataInizio() {
		return dataInizio;
	}

	public void setDataInizio(Date dataInizio) {
		this.dataInizio = dataInizio;
	}

	public Date getDataFine() {
		return dataFine;
	}

	public void setDataFine(Date dataFine) {
		this.dataFine = dataFine;
	}

	public Long getPeriodoRipartizione() {
		return periodoRipartizione;
	}

	public void setPeriodoRipartizione(Long periodoRipartizione) {
		this.periodoRipartizione = periodoRipartizione;
	}

	public String getInfoRun() {
		return infoRun;
	}

	public void setInfoRun(String infoRun) {
		this.infoRun = infoRun;
	}
    
    @Override
    public String toString() {
    	return "idValorizzazioneRun: " + this.idValorizzazioneRun + 
    			"dataInizio: " + this.dataInizio +
    			"dataFine: " + this.dataFine + 
    			"periodoRipartizione: " + this.periodoRipartizione +
    			"infoRun: " + this.infoRun;
    }
	
	
}
