package com.alkemytech.sophia.codman.rest.performing.serviceimpl;

import com.alkemytech.sophia.codman.dto.PerfEventiPagatiDTO;
import com.alkemytech.sophia.codman.dto.PerfUtilizzazioniDTO;
import com.alkemytech.sophia.codman.dto.ProgrammaMusicaleDTO;
import com.alkemytech.sophia.codman.dto.performing.AggiMcDTO;
import com.alkemytech.sophia.codman.dto.performing.FatturaDetailDTO;
import com.alkemytech.sophia.codman.dto.performing.PerfCodificaDTO;
import com.alkemytech.sophia.codman.dto.performing.RiconciliazioneImportiDTO;
import com.alkemytech.sophia.codman.entity.performing.*;
import com.alkemytech.sophia.codman.entity.performing.dao.ICampionamentoDAO;
import com.alkemytech.sophia.codman.entity.performing.dao.ICodificaDAO;
import com.alkemytech.sophia.codman.entity.performing.dao.IProgrammaMusicaleDAO;
import com.alkemytech.sophia.codman.entity.performing.security.AggregatoVoce;
import com.alkemytech.sophia.codman.entity.performing.security.ReportPage;
import com.alkemytech.sophia.codman.rest.performing.Constants;
import com.alkemytech.sophia.codman.rest.performing.ReportCache;
import com.alkemytech.sophia.codman.rest.performing.service.IProgrammaMusicaleService;
import com.alkemytech.sophia.codman.rest.performing.service.ITraceService;
import com.alkemytech.sophia.codman.utils.AccountingUtils;
import com.alkemytech.sophia.codman.utils.DateUtils;
import com.alkemytech.sophia.common.megaconcert.MegaConcertUtils;
import com.google.inject.Inject;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ForkJoinPool;

@Service("programmaMusicaleService")
public class ProgrammaMusicaleService implements IProgrammaMusicaleService {

    private IProgrammaMusicaleDAO programmaMusicaleDAO;

    private ICodificaDAO codificaDAO;

    private ICampionamentoDAO campionamentoDAO;

    private ForkJoinPool forkJoinPool;
    
    private ITraceService traceService;

    @Inject
    public ProgrammaMusicaleService(IProgrammaMusicaleDAO programmaMusicaleDAO, ICodificaDAO codificaDAO, ICampionamentoDAO campionamentoDAO,ITraceService traceService) {
        this.programmaMusicaleDAO = programmaMusicaleDAO;
        this.codificaDAO = codificaDAO;
        this.campionamentoDAO = campionamentoDAO;
    }

    public ReportPage getFlussoGuida(Long contabilitaIniziale, Long contabilitaFinale, Integer page) {
        return campionamentoDAO.getFlussoGuida(contabilitaIniziale, contabilitaFinale, page);
    }

    public List<ProgrammaMusicale> getTotFlussoGuida(Long contabilitaIniziale, Long contabilitaFinale) {
        return campionamentoDAO.getTotFlussoGuida(contabilitaIniziale, contabilitaFinale);
    }


    public ReportPage getFlussoRientrati(Long contabilitaIniziale, Long contabilitaFinale, Integer page) {
        return campionamentoDAO.getFlussoRientrati(contabilitaIniziale, contabilitaFinale, page);
    }

    public List<String> getTipoProgrammi() {
        return programmaMusicaleDAO.getTipoProgrammi();
    }
    //TODO to FInish

    public long getCountListaEventiPagati(PerfEventiPagatiDTO perfEventiPagatiDTO, String inizioPeriodoContabile, String finePeriofoContabile,
                                          String organizzatore, String direttoreEsecuzione, String permesso, String numeroPM, String titoloOpera, String codiceSAP,
                                          String dataInizioEvento, String dataFineEvento, String fatturaValidata) {

        return programmaMusicaleDAO.getCountListaEventiPagati(inizioPeriodoContabile, finePeriofoContabile, perfEventiPagatiDTO,
                organizzatore, direttoreEsecuzione, permesso, numeroPM, titoloOpera, codiceSAP, dataInizioEvento, dataFineEvento, fatturaValidata);
    }

    public ReportPage getListaEventiPagati(PerfEventiPagatiDTO perfEventiPagatiDTO, String inizioPeriodoContabile, String finePeriofoContabile,
                                           String organizzatore, String direttoreEsecuzione, String permesso, String numeroPM, String titoloOpera, String codiceSAP,
                                           String dataInizioEvento, String dataFineEvento, String fatturaValidata, String order) {

        ReportPage reportPage;

        List<EventiPagati> eventiPagati = programmaMusicaleDAO.getListaEventiPagati(inizioPeriodoContabile, finePeriofoContabile, perfEventiPagatiDTO,
                organizzatore, direttoreEsecuzione, permesso, numeroPM, titoloOpera, codiceSAP, dataInizioEvento, dataFineEvento, fatturaValidata, order);
        reportPage = new ReportPage(eventiPagati, perfEventiPagatiDTO.getPage());
        return reportPage;

    }


    public int storeFileContant(String filePathName) {

        BufferedReader br = null;
        int cont = 0;

        try {

            String sCurrentLine;

            br = new BufferedReader(new FileReader(filePathName));

            String[] elements;
            Long numeroProgramma;
            Long totaleCedole;
            Double totaleDurata;
            ProgrammaMusicale programmaMusicale;


            while ((sCurrentLine = br.readLine()) != null) {
                System.out.println(sCurrentLine);

                if (sCurrentLine != null) {
                    elements = sCurrentLine.split(",");

                    if (elements[0] != null) {

                        if (AccountingUtils.isNumber(elements[0])) {

                            numeroProgramma = elements[0] != null ? Long.parseLong(elements[0]) : 0L;
                            totaleCedole = elements[1] != null ? Long.parseLong(elements[1]) : 0L;
                            totaleDurata = elements[2] != null ? Double.parseDouble(elements[2]) : 0L;

                            programmaMusicale = new ProgrammaMusicale();

                            programmaMusicale.setNumeroProgrammaMusicale(numeroProgramma);
                            programmaMusicale.setTotaleDurataCedole(totaleDurata);
                            programmaMusicale.setTotaleCedole(totaleCedole);

                            programmaMusicaleDAO.updatePMRientrati(programmaMusicale);
                            cont++;
                        }


                    }

                }

            }

        } catch (IOException e) {
            e.printStackTrace();
            return -1;
        } finally {
            try {
                if (br != null) br.close();
            } catch (IOException ex) {
                ex.printStackTrace();
                return -1;
            }
        }

        return cont;

    }


    public long getCountTracciamentoPM(String dataIniziale, String dataFinale, Long numeroPM, Integer page, String tipoDocumento, String seprag, String idEvento,
                                       String fattura, String reversale, String voceIncasso, String permesso, String locale, String codiceBALocale,
                                       String codiceSAPOrganizzatore, String organizzatore, String titoloOpera, String dataInizioEventoDa, String dataInizioEventoA,
                                       String direttoreEsecuzione, String supporto, String tipoProgrammi, String gruppoPrincipale) {

        return programmaMusicaleDAO.getCountTracciamentoPM(dataIniziale, dataFinale, numeroPM, tipoDocumento, seprag, idEvento,
                fattura, reversale, voceIncasso, permesso, locale, codiceBALocale,
                codiceSAPOrganizzatore, organizzatore, titoloOpera, dataInizioEventoDa, dataInizioEventoA,
                direttoreEsecuzione, supporto, tipoProgrammi, gruppoPrincipale);

    }


    public ReportPage getTracciamentoPM(String dataIniziale, String dataFinale, Long numeroPM, Integer page, String tipoDocumento, String seprag, String idEvento,
                                        String fattura, String reversale, String voceIncasso, String permesso, String locale, String codiceBALocale,
                                        String codiceSAPOrganizzatore, String organizzatore, String titoloOpera, String dataInizioEventoDa, String dataInizioEventoA,
                                        String direttoreEsecuzione, String supporto, String tipoProgrammi, String gruppoPrincipale, String order) {

        ReportPage reportPage;

        List<ProgrammaMusicale> listaPm = programmaMusicaleDAO.getTracciamentoPM(dataIniziale, dataFinale, numeroPM, tipoDocumento, seprag, idEvento,
                fattura, reversale, voceIncasso, permesso, locale, codiceBALocale,
                codiceSAPOrganizzatore, organizzatore, titoloOpera, dataInizioEventoDa, dataInizioEventoA,
                direttoreEsecuzione, supporto, tipoProgrammi, gruppoPrincipale, order, page);

        reportPage = new ReportPage(listaPm, page);
        return reportPage;


    }


    private Map<String, List<ImportoRicalcolato>> normalizeImportiPMAssegnatiPerVoce(List<ImportoRicalcolato> listaImportiPMAssegnati) {

        Map<String, List<ImportoRicalcolato>> mapImporti = new HashMap<String, List<ImportoRicalcolato>>();
        List<ImportoRicalcolato> listaImporti;
        String voceIncasso;
        for (ImportoRicalcolato importo : listaImportiPMAssegnati) {
            voceIncasso = importo.getVoceIncasso();

            if (mapImporti.get(voceIncasso) == null) {
                listaImporti = new ArrayList<ImportoRicalcolato>();
                listaImporti.add(importo);
                mapImporti.put(voceIncasso, listaImporti);
            } else {
                listaImporti = mapImporti.get(voceIncasso);
                listaImporti.add(importo);
                mapImporti.put(voceIncasso, listaImporti);
            }
        }
        return mapImporti;
    }

	private Map<MultiKey, ImportoRicalcolato> normalizeImportiTotaliEventoPerVoce(
			List<ImportoRicalcolato> listaImportiPMAssegnati) {

		Map<MultiKey, ImportoRicalcolato> mapImporti = new HashMap<MultiKey, ImportoRicalcolato>();
		for (ImportoRicalcolato importo : listaImportiPMAssegnati) {
			String voceIncasso = importo.getVoceIncasso();
			Long numeroFattura = importo.getNumeroFattura();
			MultiKey key = new MultiKey(voceIncasso, numeroFattura);

			if (mapImporti.get(key) == null) {
				mapImporti.put(key, importo);
			}
		}
		return mapImporti;
	}

    public List<EventiPagati> getEventiByFattura(String numeroFattura) {

        List<EventiPagati> listaEventiPagati = programmaMusicaleDAO.getEventiByFattura(numeroFattura);

        //TODO:implement service to populate evento with movimenti
        for (EventiPagati evento : listaEventiPagati) {
            evento.setManifestazione(programmaMusicaleDAO.getManifestazione(evento.getIdEvento()));
            evento.setMovimentazioniPM(programmaMusicaleDAO.getMovimentazioniPerEvento(evento.getIdEvento()));
        }
        Map<Long, List<EventiPagati>> eventiPerIdEvento = new HashMap<Long, List<EventiPagati>>();

        Long idEvento;
        for (EventiPagati evento : listaEventiPagati) {
            List<EventiPagati> eventiSelezionati = new ArrayList<EventiPagati>();
            idEvento = evento.getIdEvento();

            if (eventiPerIdEvento.get(idEvento) == null) {
                eventiSelezionati.add(evento);
                eventiPerIdEvento.put(idEvento, eventiSelezionati);
            } else {
                eventiSelezionati = eventiPerIdEvento.get(idEvento);
                eventiSelezionati.add(evento);
                eventiPerIdEvento.put(idEvento, eventiSelezionati);
            }

        }

        List<EventiPagati> listaEventiPopolati = new ArrayList<EventiPagati>();


        Iterator listaEventiPerIdEvento = eventiPerIdEvento.keySet().iterator();
        while (listaEventiPerIdEvento.hasNext()) {
            List<EventiPagati> listaEventiPopolatiByIdEvento = new ArrayList<EventiPagati>();
            idEvento = (Long) listaEventiPerIdEvento.next();
            listaEventiPopolatiByIdEvento = populateEventoPagato(idEvento, eventiPerIdEvento.get(idEvento), true);
            for (EventiPagati evento : listaEventiPopolatiByIdEvento) {
                listaEventiPopolati.add(evento);
            }

        }

        Collections.sort(listaEventiPopolati, new Comparator<EventiPagati>() {
            @Override
            public int compare(EventiPagati s1, EventiPagati s2) {
                return s1.getVoceIncasso().compareToIgnoreCase(s2.getVoceIncasso());
            }
        });

        return listaEventiPopolati;

    }

    public List<EventiPagati> getEvento(Long eventoId) {

        List<EventiPagati> listaEventiPagati = programmaMusicaleDAO.getEvento(eventoId);
        for (EventiPagati evento : listaEventiPagati) {
            if (evento.getImportDem() != null && evento.getImportoAggio() != null) {
                evento.setImportoSophiaNetto();
//            	if(evento.getImportoExArt() != null) {
//            		evento.setImportoSophiaNettoExArt();
//            	}
            }
        }
        populateEventoPagato(eventoId, listaEventiPagati, false);

        return listaEventiPagati;

    }


    //TODO to finish
    private List<MovimentoContabile> getSgancioMovimento(List<MovimentoContabile> movimenti) {

        List<String> vociIncasso = new ArrayList<String>();

        for (MovimentoContabile movimento : movimenti) {

            Map<String, EventiPagati> attesiPerEvento = new HashMap<String, EventiPagati>();
        }


        return movimenti;

    }


    private List<EventiPagati> populateEventoPagato(Long eventoId, List<EventiPagati> listaEventiPagati, boolean fattura) {
        // Caso in cui ci sono movimentazioni su PM, ma non l'evento pagato da SUN
        if (listaEventiPagati == null || listaEventiPagati.size() == 0) {

            EventiPagati eventoPagato = new EventiPagati();

            eventoPagato.setIdEvento(eventoId);

            Manifestazione manifestazione = null;
            MovimentoContabile movimentoContabile = null;
            NDMVoceFattura ndmVoceFattura = null;

            List<MovimentoContabile> movimentazioni = programmaMusicaleDAO.getMovimentazioniPerEvento(eventoId);

            for (MovimentoContabile movimento : movimentazioni) {
                movimento.setSganciabile(programmaMusicaleDAO.getVociIncassoForIdPm(movimento.getIdProgrammaMusicale()));
            }

            List<Manifestazione> manifestazioni = programmaMusicaleDAO.getManifestazioni(movimentazioni);

            if (manifestazioni != null && manifestazioni.size() > 0) {

                manifestazione = manifestazioni.get(0);

                movimentoContabile = movimentazioni.get(0);

                eventoPagato.setManifestazione(manifestazione);

                eventoPagato.setDataInizioEvento(manifestazione.getDataInizioEvento());

                eventoPagato.setDenominazioneLocale(manifestazione.getDenominazioneLocale());

                eventoPagato.setCodiceBaLocale(manifestazione.getCodiceLocale());

                //eventoPagato.setContabilita(movimentoContabile.getContabilita());

            }

            eventoPagato.setMovimentazioniPM(movimentazioni);
            if (movimentazioni.size() > 0) {
                eventoPagato.setVoceIncasso(movimentazioni.get(0).getVoceIncasso());
                eventoPagato.setNumeroFattura(movimentazioni.get(0).getNumeroFattura().toString());
            }
//            Cartella cartella = programmaMusicaleDAO.getCartellaById(movimentazioni.get(0).getIdCartella());
//            eventoPagato.setSeprag(cartella.getSeprag());

            listaEventiPagati.add(eventoPagato);
        }


        if (listaEventiPagati != null && listaEventiPagati.size() > 0) {

            Map<String, EventiPagati> attesiPerEvento = new HashMap<String, EventiPagati>();
//            Map<String, MovimentoContabile> attesiPerMovimento = new HashMap<String, MovimentoContabile>();

            Map<String, List<String>> rientrati = new HashMap<String, List<String>>();
            Map<String, List<String>> rientratiSpalla = new HashMap<String, List<String>>();

            // Lista degli importi ricalcolati
            List<ImportoRicalcolato> listaImportiPMAssegnati = programmaMusicaleDAO.getImportiPMAssegnatiPerEvento(eventoId);

            // Importi presenti in Eventi Pagati
            List<ImportoRicalcolato> listaImportiTotali = programmaMusicaleDAO.getImportiTotaliPerEvento(eventoId);

            Map<String, List<ImportoRicalcolato>> importiPMAssegnati = normalizeImportiPMAssegnatiPerVoce(listaImportiPMAssegnati);
            Map<MultiKey, ImportoRicalcolato> importiTotali = normalizeImportiTotaliEventoPerVoce(listaImportiTotali);

            // Aquisisce l'ultimo movimento contabile dell'Evento/Voce Incasso (EventiPagati più aggiornati)
            String voceIncasso;
            EventiPagati eventoSelezionato;
            MovimentoContabile movimentoSelezionato;
            for (EventiPagati evento : listaEventiPagati) {
                if (evento.getQuadraturaNDM() != null) {
                    evento.setNdmVoceFattura(programmaMusicaleDAO.getNdmVoceFattura(evento.getVoceIncasso(), evento.getNumeroFattura()));

                }
                voceIncasso = evento.getVoceIncasso();

                if (attesiPerEvento.get(voceIncasso) == null) {
                    attesiPerEvento.put(voceIncasso, evento);
                } else {
                    eventoSelezionato = attesiPerEvento.get(voceIncasso);
                    // Entry più recente per cui ha le informazioni degli attesi per Evento/Voce più aggiornate
                    if (evento.getId().longValue() > eventoSelezionato.getId().longValue())
                        attesiPerEvento.put(voceIncasso, evento);
                }
            }

            // Aquisisce l'ultimo movimento contabile dell'Evento/Voce Incasso (movimento158 più aggiornato)

            Long count = 0L;

            List<MovimentoContabile> movimentazioni = listaEventiPagati.get(0).getMovimentazioniPM();
            List<String> pmRientrati = null;
            for (MovimentoContabile movimento : movimentazioni) {

                voceIncasso = movimento.getVoceIncasso();

//                if (attesiPerMovimento.get(voceIncasso)==null)
//                    attesiPerMovimento.put(voceIncasso, movimento);
//                else{
//                    movimentoSelezionato = attesiPerMovimento.get(voceIncasso);
//                    // Entry più recente per cui ha le informazioni degli attesi per Evento/Voce più aggiornate
//                    if (movimento.getId().longValue()>movimentoSelezionato.getId().longValue())
//                        attesiPerMovimento.put(voceIncasso, movimento);
//                }


                // Trattasi di gruppo principale o spalla ()
                if (voceIncasso.equals(Constants.VOCE_2244)) {

                    if (movimento.getFlagGruppoPrincipale() == 'Y' || movimento.getFlagGruppoPrincipale() == '1') {
                        if (rientrati.get(voceIncasso) == null) {
                            pmRientrati = new ArrayList<String>();
                            pmRientrati.add(movimento.getNumProgrammaMusicale());
                            rientrati.put(voceIncasso, pmRientrati);
                        } else {
                            // A parità di evento/voce incasso ci posono essere più fatture sui PM e quindi i PM potrbbero ripetersi nella movimentazione
                            pmRientrati = rientrati.get(voceIncasso);
                            if (!pmRientrati.contains(movimento.getNumProgrammaMusicale())) {
                                pmRientrati.add(movimento.getNumProgrammaMusicale());
                                rientrati.put(voceIncasso, pmRientrati);
                            }
                        }
                    } else {
                        // Trattasi di gruppo Spalla
                        if (rientratiSpalla.get(voceIncasso) == null) {
                            pmRientrati = new ArrayList<String>();
                            pmRientrati.add(movimento.getNumProgrammaMusicale());
                            rientratiSpalla.put(voceIncasso, pmRientrati);
                        } else {
                            pmRientrati = rientratiSpalla.get(voceIncasso);
                            if (!pmRientrati.contains(movimento.getNumProgrammaMusicale())) {
                                pmRientrati.add(movimento.getNumProgrammaMusicale());
                                rientratiSpalla.put(voceIncasso, pmRientrati);
                            }
                        }
                    }
                    /*
                    if (movimento.getFlagGruppoPrincipale()=='Y' || movimento.getFlagGruppoPrincipale()=='1'){
                        if (rientrati.get(voceIncasso+"_"+movimento.getNumProgrammaMusicale())==null)
                            rientrati.put(voceIncasso+"_"+movimento.getNumProgrammaMusicale(), 1L);
                        else{
                            // A parità di evento/voce incasso ci posono essere più fatture sui PM e quindi i PM potrbbero ripetersi nella movimentazione
                            count = rientrati.get(voceIncasso+"_"+movimento.getNumProgrammaMusicale());
                            count++;
                            rientrati.put(voceIncasso+"_"+movimento.getNumProgrammaMusicale(), count);
                        }
                    }else{
                        // Trattasi di gruppo Spalla
                        if (rientratiSpalla.get(voceIncasso+"_"+movimento.getNumProgrammaMusicale())==null)
                            rientratiSpalla.put(voceIncasso+"_"+movimento.getNumProgrammaMusicale(), 1L);
                        else{
                            count = rientratiSpalla.get(voceIncasso+"_"+movimento.getNumProgrammaMusicale());
                            count++;
                            rientratiSpalla.put(voceIncasso+"_"+movimento.getNumProgrammaMusicale(), count);
                        }
                    }
                   */
                } else {

                    if (rientrati.get(voceIncasso) == null) {
                        pmRientrati = new ArrayList<String>();
                        pmRientrati.add(movimento.getNumProgrammaMusicale());
                        rientrati.put(voceIncasso, pmRientrati);
                    } else {
                        pmRientrati = rientrati.get(voceIncasso);
                        if (!pmRientrati.contains(movimento.getNumProgrammaMusicale())) {
                            pmRientrati.add(movimento.getNumProgrammaMusicale());
                            rientrati.put(voceIncasso, pmRientrati);
                        }
                    }

                }

            }


            DettaglioPM dettaglioPM;
            if (fattura == true) {
                for (EventiPagati evento : listaEventiPagati) {
                    List<DettaglioPM> listaPMAttesi = new ArrayList<DettaglioPM>();
                    dettaglioPM = new DettaglioPM();
                    dettaglioPM.setIdEvento(eventoId);
                    dettaglioPM.setNumeroFattura(evento.getNumeroFattura());
                    dettaglioPM.setIdEventoPagato(evento.getId());
                    dettaglioPM.setVoceIncasso(evento.getVoceIncasso());
                    dettaglioPM.setPmPrevisti(evento.getNumeroPmTotaliAttesi().longValue());
                    dettaglioPM.setPmPrevistiSpalla(evento.getNumeroPmTotaliAttesiSpalla().longValue());
                    if (rientrati.get(evento.getVoceIncasso()) != null) {
                        dettaglioPM.setPmRientrati(new Long(((List<String>) rientrati.get(evento.getVoceIncasso())).size()));
                    } else {
                        dettaglioPM.setPmRientrati(new Long(0));
                    }

                    if (rientratiSpalla.get(evento.getVoceIncasso()) != null) {
                        dettaglioPM.setPmRientratiSpalla(new Long(((List<String>) rientratiSpalla.get(evento.getVoceIncasso())).size()));
                    } else {
                        dettaglioPM.setPmRientratiSpalla(new Long(0));
                    }
                    listaPMAttesi.add(dettaglioPM);

                    evento.setDettaglioPM(listaPMAttesi);
                }
            } else {
                List<DettaglioPM> listaPMAttesi = new ArrayList<DettaglioPM>();
                //Iterator listaVociIncasso = attesiPerEvento.keySet().iterator();
                for (EventiPagati evento : listaEventiPagati) {
                //while (listaVociIncasso.hasNext()) {
                    voceIncasso = evento.getVoceIncasso();

                    dettaglioPM = new DettaglioPM();
                    dettaglioPM.setIdEvento(eventoId);
                    // eventoSelezionato = attesiPerEvento.get(voceIncasso);
                    dettaglioPM.setNumeroFattura(evento.getNumeroFattura());
                    dettaglioPM.setIdEventoPagato(evento.getId());                    
                    dettaglioPM.setVoceIncasso(voceIncasso);

                    // I previsti dei Movimenti sono più aggiornati rispetto a quelli degli eventi
                    //                if (attesiPerMovimento.get(voceIncasso)!=null){
                    //                    movimentoSelezionato = attesiPerMovimento.get(voceIncasso);
                    //                    dettaglioPM.setPmPrevisti(movimentoSelezionato.getNumeroPmPrevisti().longValue());
                    //                    dettaglioPM.setPmPrevistiSpalla(movimentoSelezionato.getNumeroPmPrevistiSpalla().longValue());
                    //                }else{
                    dettaglioPM.setPmPrevisti(evento.getNumeroPmTotaliAttesi().longValue());
                    dettaglioPM.setPmPrevistiSpalla(evento.getNumeroPmTotaliAttesiSpalla().longValue());
                    //                }

                    if (rientrati.get(voceIncasso) != null) {
                        dettaglioPM.setPmRientrati(new Long(((List<String>) rientrati.get(voceIncasso)).size()));
                    } else {
                        dettaglioPM.setPmRientrati(new Long(0));
                    }

                    if (rientratiSpalla.get(voceIncasso) != null) {
                        dettaglioPM.setPmRientratiSpalla(new Long(((List<String>) rientratiSpalla.get(voceIncasso)).size()));
                    } else {
                        dettaglioPM.setPmRientratiSpalla(new Long(0));
                    }

                    if (importiPMAssegnati.get(voceIncasso) != null) {
                        dettaglioPM.setImportiRicalcolati(importiPMAssegnati.get(voceIncasso));
                    }
                    MultiKey key = new MultiKey(voceIncasso, Long.parseLong(evento.getNumeroFattura()));
                    ImportoRicalcolato importoRicalcolato = importiTotali.get(key);
					if (importoRicalcolato != null) {
						dettaglioPM.setImportoTotale(importoRicalcolato.getImporto());
						dettaglioPM.setImportoAssegnato(importoRicalcolato.getImportoNetto() != null ? importoRicalcolato.getImportoNetto() : 0D);
						if (importoRicalcolato.getImportoNonRientratoPrincipale() != null) {
							BigDecimal importoSospeso = new BigDecimal(
									importoRicalcolato.getImportoNonRientratoPrincipale());
							if (importoRicalcolato.getImportoNonRientratoSpalla() != null) {
								importoSospeso = importoSospeso
										.add(new BigDecimal(importoRicalcolato.getImportoNonRientratoSpalla()));
							}
							dettaglioPM.setImportoSospeso(importoSospeso.doubleValue());
						}
                    }

                    listaPMAttesi.add(dettaglioPM);
                }

                for (EventiPagati evento : listaEventiPagati) {
                    evento.setDettaglioPM(listaPMAttesi);
                }

            }
        }
        return listaEventiPagati;
    }

    public long getCountListaMovimentazioni(Long inizioPeriodoContabile, Long finePeriodoContabile, Long evento, String fattura, Long reversale, String voceIncasso,
                                            Date dataInizioEvento, Date dataFineEvento, String seprag, String tipoDocumento, String locale, String sapOrganizzatore,
                                            String tipoSupporto, String tipoProgramma, String direttoreEsecuzione, String titoloOpera, String numeroPM, String permesso,
                                            Integer page, String codiceBALocale, String organizzatore) {

        long count = programmaMusicaleDAO.getCountListaMovimentazioni(inizioPeriodoContabile, finePeriodoContabile, evento, fattura, reversale, voceIncasso,
                dataInizioEvento, dataFineEvento, seprag, tipoDocumento, locale, sapOrganizzatore,
                tipoSupporto, tipoProgramma, direttoreEsecuzione, titoloOpera, numeroPM, permesso, page, codiceBALocale, organizzatore);
//
//            ReportCache.putReport(ReportCache.REPORT_LISTA_MOVIMENTI, key, reportPage);
//        }

        return count;


    }

    public ReportPage getListaMovimentazioni(Long inizioPeriodoContabile, Long finePeriodoContabile, Long evento, String fattura, Long reversale, String voceIncasso,
                                             Date dataInizioEvento, Date dataFineEvento, String seprag, String tipoDocumento, String locale, String sapOrganizzatore,
                                             String tipoSupporto, String tipoProgramma, String direttoreEsecuzione, String titoloOpera, String numeroPM, String permesso,
                                             Integer page, String codiceBALocale, String organizzatore, String order) {

        ReportPage reportPage;


        List<MovimentoContabile> listaMovimenti = programmaMusicaleDAO.getListaMovimentazioni(inizioPeriodoContabile, finePeriodoContabile, evento, fattura, reversale, voceIncasso,
                dataInizioEvento, dataFineEvento, seprag, tipoDocumento, locale, sapOrganizzatore,
                tipoSupporto, tipoProgramma, direttoreEsecuzione, titoloOpera, numeroPM, permesso, page, codiceBALocale, organizzatore, order);


        reportPage = new ReportPage(listaMovimenti, page);
        return reportPage;
    }


    public ReportPage getListaAggregatiSiada(Long inizioPeriodoContabile, Long finePeriodoContabile, String voceIncasso,
                                             String tipoSupporto, String tipoProgramma, Long numeroPM, Integer page) {

        List<String> parameters = new ArrayList<String>();
        parameters.add(inizioPeriodoContabile.toString());
        parameters.add(finePeriodoContabile.toString());
        parameters.add(voceIncasso);
        parameters.add(tipoSupporto);
        parameters.add(tipoProgramma);
        if (numeroPM != null)
            parameters.add(numeroPM.toString());
        else
            parameters.add(null);

        String key = ReportCache.generateKey(ReportCache.REPORT_VERIFICA_PM_SIADA, parameters);

        ReportPage reportPage = ReportCache.getReport(key);

        // Verifica presenza del Report in Cache.
        if (reportPage != null) {

            // Se il report è già presente in cache, non esegue la query per prendere la pagina interessata, ma usa i dati
            // già presenti
            reportPage = ReportCache.setCurrentReportPage(reportPage, page);

        } else {

            ReportCache.clearReports();

            // Se il report non è presente in cache, esegue la query per acquisire i dati del report e lo restituisce, dopo
            // aver aggiornato la cache
            reportPage = programmaMusicaleDAO.getListaAggregatiSiada(inizioPeriodoContabile, finePeriodoContabile, voceIncasso, tipoSupporto, tipoProgramma, numeroPM, page);

            ReportCache.putReport(ReportCache.REPORT_VERIFICA_PM_SIADA, key, reportPage);
        }

        return reportPage;


    }

    public ReportPage getListaAggregatiEventi(Long inizioPeriodoContabile, Long finePeriodoContabile, String idPuntoTerritoriale) {

        List<String> parameters = new ArrayList<String>();
        parameters.add(inizioPeriodoContabile.toString());
        parameters.add(finePeriodoContabile.toString());
        if (idPuntoTerritoriale != null)
            parameters.add(idPuntoTerritoriale.toString());
        else
            parameters.add(null);

        String key = ReportCache.generateKey(ReportCache.REPORT_AGGREGATO_INCASSO, parameters);

        ReportPage reportPage = ReportCache.getReport(key);

        // Verifica presenza del Report in Cache.
        if (reportPage != null) {

            int page = -1;

            // Se il report è già presente in cache, non esegue la query per prendere la pagina interessata, ma usa i dati
            // già presenti
            reportPage = ReportCache.setCurrentReportPage(reportPage, page);

        } else {

            ReportCache.clearReports();

            // Se il report non è presente in cache, esegue le query per acquisire i dati del report e lo restituisce, dopo
            // aver aggiornato la cache
            List<AggregatoVoce> listaAggregati = new ArrayList<AggregatoVoce>();

            reportPage = new ReportPage(listaAggregati, listaAggregati.size(), 1);

            List<AggregatoVoce> listaDocumentiEvento = programmaMusicaleDAO.getListaDocumentiEventi(inizioPeriodoContabile, finePeriodoContabile, idPuntoTerritoriale);

            if (listaDocumentiEvento != null && listaDocumentiEvento.size() > 0) {

                List<AggregatoVoce> listaEventi = programmaMusicaleDAO.getListaEventi(inizioPeriodoContabile, finePeriodoContabile, idPuntoTerritoriale);

                AggregatoVoce aggregatoVoce = null;
                HashMap<String, AggregatoVoce> aggregatiEventiMap = new HashMap<String, AggregatoVoce>();

                for (int i = 0; i < listaDocumentiEvento.size(); i++) {

                    if (aggregatiEventiMap.get(listaDocumentiEvento.get(i).getVoceIncasso()) == null) {
                        aggregatoVoce = new AggregatoVoce();
                        aggregatoVoce.setVoceIncasso(listaDocumentiEvento.get(i).getVoceIncasso());

                        for (int j = 0; j < listaEventi.size(); j++) {
                            if (listaEventi.get(j).getVoceIncasso().equalsIgnoreCase(listaDocumentiEvento.get(i).getVoceIncasso())) {
                                aggregatoVoce.setNumeroEventi(listaEventi.get(j).getNumeroEventi());
                                break;
                            }
                        }
                        aggregatiEventiMap.put(listaDocumentiEvento.get(i).getVoceIncasso(), aggregatoVoce);

                    } else {
                        aggregatoVoce = aggregatiEventiMap.get(listaDocumentiEvento.get(i).getVoceIncasso());
                    }

                    if (listaDocumentiEvento.get(i).getTipoDocumento() != null && listaDocumentiEvento.get(i).getTipoDocumento().equalsIgnoreCase(Constants.DOCUMENTO_ENTRATE)) {
                        aggregatoVoce.setNumeroDocumentiEntrata(listaDocumentiEvento.get(i).getNumeroDocumenti());
                        aggregatoVoce.setImportoEntrate(listaDocumentiEvento.get(i).getImporto());
                    } else {
                        aggregatoVoce.setNumeroDocumentiUscita(listaDocumentiEvento.get(i).getNumeroDocumenti());
                        aggregatoVoce.setImportoUscite(listaDocumentiEvento.get(i).getImporto());
                    }

                }

                if (aggregatiEventiMap.size() > 0) {
                    listaAggregati.addAll(aggregatiEventiMap.values());
                }

                reportPage = new ReportPage(listaAggregati, listaAggregati, listaAggregati.size(), 1);

            }


            ReportCache.putReport(ReportCache.REPORT_AGGREGATO_INCASSO, key, reportPage);
        }

        return reportPage;


    }


    private List<ReportPage> getListaAggregatiMovimentiInternal(Long inizioPeriodoContabile, Long finePeriodoContabile, String idPuntoTerritoriale, String correntiArretrati) {

        List<ReportPage> listaReports = new ArrayList<ReportPage>();

        // Inizializza i reports da mostrare in pagina con Report vuoti
        ReportPage emptyReport = new ReportPage(new ArrayList<AggregatoVoce>(), 0, 1);
        listaReports.add(0, emptyReport);
        listaReports.add(1, emptyReport);
        listaReports.add(2, emptyReport);
        listaReports.add(3, emptyReport);

        // ------ Gestione PM Correnti --------
        if (correntiArretrati.equalsIgnoreCase("T") || correntiArretrati.equalsIgnoreCase("C")) {

            // Importo Netto per voce incasso PM Correnti
            List<AggregatoVoce> listaPMImportoNetto = programmaMusicaleDAO.getImportoNettiPMCorrenti(inizioPeriodoContabile, finePeriodoContabile, idPuntoTerritoriale);

            // Importo Programmato effettivo per Voce Incasso
            List<AggregatoVoce> listaPMImportoProgrammato = programmaMusicaleDAO.getImportoProgrammatoCorrenti(inizioPeriodoContabile, finePeriodoContabile, idPuntoTerritoriale);

            if (listaPMImportoNetto != null && listaPMImportoNetto.size() > 0) {

                // Aggiunge all'importo Netto gli altri importi calcolati o presi tramite altre query in modo fafornire un unico oggetto popolato da mostrare in pagina
                AggregatoVoce singolaVoce = null;
                for (int i = 0; i < listaPMImportoNetto.size(); i++) {
                    singolaVoce = listaPMImportoNetto.get(i);
                    for (int j = 0; j < listaPMImportoProgrammato.size(); j++) {
                        if (singolaVoce.getVoceIncasso().equals(listaPMImportoProgrammato.get(j).getVoceIncasso())) {
                            singolaVoce.setImportoProgrammato(listaPMImportoProgrammato.get(j).getImportoProgrammato());
                            // ToDo: inserire con il gestionali gli importi corretti per Sospesi e Annullati
                            singolaVoce.setImporto566(AccountingUtils.round(singolaVoce.getImporto() - singolaVoce.getImportoProgrammato() - singolaVoce.getImportoAnnullati() - singolaVoce.getImportoSospesi(), 7));
                            break;
                        }
                    }
                }

                ReportPage reportImportiPMCorrenti = new ReportPage(listaPMImportoNetto, listaPMImportoNetto.size(), 1);

                listaReports.add(0, reportImportiPMCorrenti);
            }


            // Numero PM e Numero Cedole per Voce Incasso
            List<AggregatoVoce> listaNumeroPMCorrenti = programmaMusicaleDAO.getNumeroPMCorrenti(inizioPeriodoContabile, finePeriodoContabile, idPuntoTerritoriale);

            List<AggregatoVoce> listaNumeroCedoleCorrenti = programmaMusicaleDAO.getNumeroCedoleCorrenti(inizioPeriodoContabile, finePeriodoContabile, idPuntoTerritoriale);

            if (listaNumeroPMCorrenti != null && listaNumeroPMCorrenti.size() > 0) {

                // Aggiunge al numero di PM il numero di Cedole
                AggregatoVoce singolaVoce = null;
                for (int i = 0; i < listaNumeroPMCorrenti.size(); i++) {
                    singolaVoce = listaNumeroPMCorrenti.get(i);
                    for (int j = 0; j < listaNumeroCedoleCorrenti.size(); j++) {
                        if (singolaVoce.getVoceIncasso().equals(listaNumeroCedoleCorrenti.get(j).getVoceIncasso())) {
                            singolaVoce.setNumeroCedole(listaNumeroCedoleCorrenti.get(j).getNumeroCedole());
                            break;
                        }
                    }
                }

                ReportPage reportImportiCedolePM = new ReportPage(listaNumeroPMCorrenti, listaNumeroPMCorrenti.size(), 1);

                listaReports.add(1, reportImportiCedolePM);

            }

        }


        // ------ Gstione PM Arretrati --------
        if (correntiArretrati.equalsIgnoreCase("T") || correntiArretrati.equalsIgnoreCase("A")) {

            List<AggregatoVoce> listaPMImportoNettoArretrati = programmaMusicaleDAO.getImportoNettiPMArretrati(inizioPeriodoContabile, finePeriodoContabile, idPuntoTerritoriale);

            if (listaPMImportoNettoArretrati != null && listaPMImportoNettoArretrati.size() > 0) {

                ReportPage reportImportoNettoArretrati = new ReportPage(listaPMImportoNettoArretrati, listaPMImportoNettoArretrati.size(), 1);

                listaReports.add(2, reportImportoNettoArretrati);
            }

            List<AggregatoVoce> listaNumeroPMArretrati = programmaMusicaleDAO.getNumeroPMArretrati(inizioPeriodoContabile, finePeriodoContabile, idPuntoTerritoriale);

            List<AggregatoVoce> listaNumeroCedoleArretrati = programmaMusicaleDAO.getNumeroCedoleArretrati(inizioPeriodoContabile, finePeriodoContabile, idPuntoTerritoriale);


            if (listaNumeroPMArretrati != null && listaNumeroPMArretrati.size() > 0) {

                // Aggiunge al numero di PM il numero di Cedole
                AggregatoVoce singolaVoce = null;
                for (int i = 0; i < listaNumeroPMArretrati.size(); i++) {
                    singolaVoce = listaNumeroPMArretrati.get(i);
                    for (int j = 0; j < listaNumeroCedoleArretrati.size(); j++) {
                        if (singolaVoce.getVoceIncasso().equals(listaNumeroCedoleArretrati.get(j).getVoceIncasso())) {
                            singolaVoce.setNumeroCedole(listaNumeroCedoleArretrati.get(j).getNumeroCedole());
                            break;
                        }
                    }
                }

                ReportPage reportImportiCedolePMArretrati = new ReportPage(listaNumeroPMArretrati, listaNumeroPMArretrati.size(), 1);

                listaReports.add(3, reportImportiCedolePMArretrati);
            }
        }


        return listaReports;

    }

    public List<ReportPage> getListaAggregatiMovimenti(Long inizioPeriodoContabile, Long finePeriodoContabile, String idPuntoTerritoriale, Integer page, String correntiArretrati) {

        List<String> parameters = new ArrayList<String>();

        parameters.add(inizioPeriodoContabile.toString());
        parameters.add(finePeriodoContabile.toString());
        if (idPuntoTerritoriale != null)
            parameters.add(idPuntoTerritoriale.toString());
        else
            parameters.add(null);
        parameters.add(correntiArretrati);

        String key = ReportCache.generateKey(ReportCache.REPORT_AGGREGATO_MOVIMENTI, parameters);

        List<ReportPage> reportsPage = ReportCache.getReportList(key);

        // Verifica presenza del Report in Cache
        if (reportsPage == null) {

            ReportCache.clearReports();

            reportsPage = getListaAggregatiMovimentiInternal(inizioPeriodoContabile, finePeriodoContabile, idPuntoTerritoriale, correntiArretrati);

            ReportCache.putReportList(ReportCache.REPORT_AGGREGATO_MOVIMENTI, key, reportsPage);

        }

        return reportsPage;

    }


    public List<EventiPagati> getDettagliMovimento(Long movimentoId) {

        MovimentoContabile movimentoContabile = programmaMusicaleDAO.getMovimentoContabile(movimentoId);

        List<EventiPagati> listaEventiPagati = getEvento(movimentoContabile.getIdEvento());

        return listaEventiPagati;
    }

    public ProgrammaMusicale getDettaglioPM(Long idProgrammaMusicale, Long idMovimento) {

        ProgrammaMusicale programmaMusicale = programmaMusicaleDAO.getProgrammaMusicaleById(idProgrammaMusicale);

//        MovimentoContabile movimentoSelezionato = programmaMusicaleDAO.getMovimentoContabile(idMovimento);

//        programmaMusicale.setVoceIncasso(movimentoSelezionato.getVoceIncasso());

        List<Utilizzazione> listaOpere = programmaMusicaleDAO.getUtilizzazioni(idProgrammaMusicale);
        List<MovimentoContabile> listaMovimenti = programmaMusicaleDAO.getMovimentiContabile(idProgrammaMusicale);
        List<Manifestazione> eventi = programmaMusicaleDAO.getManifestazioni(listaMovimenti);

        MovimentoContabile movimento = null;
        for (int i = 0; i < listaMovimenti.size(); i++) {
            movimento = listaMovimenti.get(i);
            for (int j = 0; j < eventi.size(); j++) {
                if (movimento.getIdEvento().longValue() == eventi.get(j).getId().longValue()) {
                    movimento.setEvento(eventi.get(j));
                    break;
                }
            }
        }

        DirettoreEsecuzione direttoreEsecuzione = programmaMusicaleDAO.getDirettoreEsecuzione(idProgrammaMusicale);
//        Cartella cartella = programmaMusicaleDAO.getCartellaById(movimentoSelezionato.getIdCartella());

        programmaMusicale.setOpere(listaOpere);
        programmaMusicale.setMovimenti(listaMovimenti);
        programmaMusicale.setEventi(eventi);
        programmaMusicale.setDirettoreEsecuzione(direttoreEsecuzione);
//        programmaMusicale.setCartella(cartella);
//        programmaMusicale.setDataRientroPM(movimentoSelezionato.getDataRientroPMStr());

        // Inizializzo gli importi aggregati ricalcolati
        ImportoPM importo = new ImportoPM();

        Double importoSUN = programmaMusicaleDAO.getImportoPMSUNAggregato(idProgrammaMusicale);
        importo.setImportoAggregatoSUN(importoSUN);

        Double importoTotaleRicalcolato = programmaMusicaleDAO.getImportoPMSIADAAggregato(idProgrammaMusicale);

        if (importoTotaleRicalcolato == null) {
            List<ImportoRicalcolato> importiRicalcolati = programmaMusicaleDAO.getImportiPMRicalcolati(idProgrammaMusicale);
            importo.setImportiRicalcolati(importiRicalcolati);
        } else {
            importo.setImportoTotaleRicalcolato(importoTotaleRicalcolato);
        }
        programmaMusicale.setImporto(importo);

        return programmaMusicale;
    }

    public void updatePmOnDetail(Long idPM, String flagCampionamento, String flagGruppo, Double durata, String durataMeno30Sec, String flagPD, Long idOpera) {
        if (idPM != null || flagCampionamento != null || flagGruppo != null) {
            programmaMusicaleDAO.updateProgrammaMusicaleOnDetail(idPM, flagCampionamento, flagGruppo);
        } else if (durata != null || durataMeno30Sec != null || flagPD != null) {
            programmaMusicaleDAO.updateOperaOnDetail(durata, durataMeno30Sec, flagPD, idOpera);
        }

        if (flagGruppo != null) {
            List<Long> idMovimentiAssociati = programmaMusicaleDAO.retrieveIdMovimentiForProgrammiMusicali(idPM);

            for (Long idMovimento : idMovimentiAssociati) {
                programmaMusicaleDAO.updateFlagCampionamentoOnMovimentiAssociati(idMovimento, flagGruppo);

            }
        }
    }

    public ProgrammaMusicale getDettaglioPM(Long idProgrammaMusicale) {

        ProgrammaMusicale programmaMusicale = programmaMusicaleDAO.getProgrammaMusicaleById(idProgrammaMusicale);

        List<Utilizzazione> listaOpere = programmaMusicaleDAO.getUtilizzazioni(idProgrammaMusicale);
        List<MovimentoContabile> listaMovimenti = programmaMusicaleDAO.getMovimentiContabile(idProgrammaMusicale);
        List<Manifestazione> eventi = programmaMusicaleDAO.getManifestazioni(listaMovimenti);

        for (int i = 0; i < eventi.size(); i++) {
            List<EventiPagati> eventiPagati = programmaMusicaleDAO.getEvento(eventi.get(i).getId());
            if (eventiPagati.size() > 0) {
                eventi.get(i).setSeprag(eventiPagati.get(0).getSeprag());
            }
        }

        if (listaMovimenti.size() > 0) {
            programmaMusicale.setVoceIncasso(listaMovimenti.get(0).getVoceIncasso());
        }
        TrattamentoPM trattamentoPm;

        MovimentoContabile movimento = null;
        for (int i = 0; i < listaMovimenti.size(); i++) {
            movimento = listaMovimenti.get(i);
            for (int j = 0; j < eventi.size(); j++) {
                if (movimento.getIdEvento() != null) {
                    if (movimento.getIdEvento().longValue() == eventi.get(j).getId().longValue()) {
                        movimento.setEvento(eventi.get(j));
                        break;
                    }
                }
            }
        }

        DirettoreEsecuzione direttoreEsecuzione = programmaMusicaleDAO.getDirettoreEsecuzione(idProgrammaMusicale);
//        Cartella cartella = programmaMusicaleDAO.getCartellaById(listaMovimenti.get(0).getIdCartella());

        programmaMusicale.setOpere(listaOpere);
        programmaMusicale.setMovimenti(listaMovimenti);
        programmaMusicale.setEventi(eventi);
        programmaMusicale.setDirettoreEsecuzione(direttoreEsecuzione);
//        programmaMusicale.setCartella(cartella);
        programmaMusicale.setDataRientroPM(listaMovimenti.get(0).getDataRientroPMStr());

        // Inizializzo gli importi aggregati ricalcolati
        ImportoPM importo = new ImportoPM();

        // Calcolo degli importi delle singole cedole
        if (programmaMusicale.getMovimenti().get(0).getImportiRicalcolati() != null && programmaMusicale.getMovimenti().get(0).getImportiRicalcolati().size() > 0) {

            // Distingue le tipologie di ricalcolo in base al tipo di voce di incasso
            Double importoUnitario = 0.0D;
            trattamentoPm = programmaMusicaleDAO.getTrattamentoPM(programmaMusicale.getVoceIncasso());

            ImportoRicalcolato importoRicalcolato = programmaMusicale.getMovimenti().get(0).getImportiRicalcolati().get(0);

            if (importoRicalcolato.getImportoSingolaCedola() != null)
                importoUnitario = importoRicalcolato.getImportoSingolaCedola();

            if (trattamentoPm.getTipo().equalsIgnoreCase("A") || trattamentoPm.getTipo().equalsIgnoreCase("B") || trattamentoPm.getTipo().equalsIgnoreCase("D")) {
                // Calcolo ProQuota
                for (Utilizzazione cedola : listaOpere) {
                    cedola.setImportoCedola(importoUnitario);
                }
            } else {
                // Ricalcolo Pro Durata
                for (Utilizzazione cedola : listaOpere) {
                    cedola.setImportoCedola(importoUnitario * cedola.getDurata());
                }
            }
        }

        Double importoSUN = programmaMusicaleDAO.getImportoPMSUNAggregato(idProgrammaMusicale);
        importo.setImportoAggregatoSUN(importoSUN);

//        Double importoTotaleRicalcolato = programmaMusicaleDAO.getImportoPMSIADAAggregato(idProgrammaMusicale);

//        if (importoTotaleRicalcolato == null){
        List<ImportoRicalcolato> importiRicalcolati = programmaMusicaleDAO.getImportiPMRicalcolati(idProgrammaMusicale);
        importo.setImportiRicalcolati(importiRicalcolati);
//        }else{
//            importo.setImportoTotaleRicalcolato(importoTotaleRicalcolato);
//        }
        programmaMusicale.setImporto(importo);

        return programmaMusicale;
    }


    public Long getMovimentiContabili(Long periodoContabileInizio, Long periodoContabileFine, String tipologiaPm) {
        return programmaMusicaleDAO.getMovimentiContabili(periodoContabileInizio, periodoContabileFine, tipologiaPm);
    }

    public Integer updateContabilitaOriginale(Integer nuovaContabilita, List<Object> listaMovimenti) {
        return programmaMusicaleDAO.updateContabilitaOriginale(nuovaContabilita, listaMovimenti);
    }

    public void updatePMAttesiInEventiPagati(Long pmPrevisti, Long pmPrevistiSpalla, Long idEvento) {
        programmaMusicaleDAO.updatePMAttesiInEventiPagati(pmPrevisti, pmPrevistiSpalla, idEvento);
    }

    public void updatePMAttesiInMovimenti(Integer pmPrevisti, Integer pmPrevistiSpalla, Long idMovimento) {
        programmaMusicaleDAO.updatePMAttesiInMovimenti(pmPrevisti, pmPrevistiSpalla, idMovimento);
    }

    public long getCountListaFatture(EventiPagati eventoPagato, MovimentoContabile movimentoContabile, String fatturaValidata) {

        long result = programmaMusicaleDAO.getCountListaFatturePerEventiPagati(eventoPagato, fatturaValidata);

        return result;
    }

    public ReportPage getListaFatture(EventiPagati eventoPagato, MovimentoContabile movimentoContabile, String fatturaValidata, Integer page, String order) {

        List<EventiPagati> eventiPagati = programmaMusicaleDAO.getListaFatturePerEventiPagati(eventoPagato, fatturaValidata, order, page);
//    	List<Object> listaFatture =new ArrayList<Object>();


        ReportPage reportPage;

        reportPage = new ReportPage(eventiPagati, page);
        return reportPage;
    }

    public List<EventiPagati> getDettaglioFattureOnEvento(String numeroFattura) {
        return programmaMusicaleDAO.getDettaglioFattureOnEvento(numeroFattura);
    }

    public List<MovimentoContabile> getDettaglioFattureOnMovimento(String numeroFattura) {
        return programmaMusicaleDAO.getDettaglioFattureOnMovimento(numeroFattura);
    }

    public FatturaDetailDTO getFatturaDetail(String numeroFattura) {
        List<EventiPagati> eventiPagati = getEventiByFattura(numeroFattura);
        for (EventiPagati evento : eventiPagati) {
            if (evento.getImportDem() != null && evento.getImportoAggio() != null) {
                evento.setImportoSophiaNetto();
                //            	if(evento.getImportoExArt() != null) {
                //            		evento.setImportoSophiaNettoExArt();
                //            	}
            }
            if (evento.getManifestazione() != null && evento.getManifestazione().getFlagMegaConcert()) {
                AggiMc aggio = programmaMusicaleDAO.getAggioByDate(evento.getDataReversale());
                if (aggio.getId() != null) {
                    evento.setAggioSoprasoglia(aggio.getAggioSoprasoglia());
                    evento.setAggioSottosoglia(aggio.getAggioSottosoglia());
                    //	        		BigDecimal percentualeCalcolata = aggio.getSoglia().compareTo(BigDecimal.valueOf(evento.getImportDem())) < 0 ? aggio.getAggioSottosoglia() : aggio.getAggioSoprasoglia();
                    //	        		NDMVoceFattura ndm = evento.getNdmVoceFattura();
                    //	        		ndm.setPercentualeAggio(percentualeCalcolata);
                }
            }
        }
        //    	List<MovimentoContabile> movimentiContabili = programmaMusicaleDAO.getDettaglioFattureOnMovimento(numeroFattura);
        Manifestazione manifestazione = programmaMusicaleDAO.getManifestazione(eventiPagati.get(0).getIdEvento());

        FatturaDetailDTO fatturaDetail = new FatturaDetailDTO();
        fatturaDetail.setEventiAssociati(eventiPagati);
        //    	fatturaDetail.setMovimentiAssociati(movimentiContabili);
        fatturaDetail.setNumeroFattura(numeroFattura);
        fatturaDetail.setReversale(eventiPagati.get(0).getReversale());
        fatturaDetail.setSeprag(eventiPagati.get(0).getSeprag());
        //    	if(manifestazione != null) {
        fatturaDetail.setManifestazione(manifestazione);
        if (manifestazione != null) {
            fatturaDetail.setNomeOrganizzatore(manifestazione.getNomeOrganizzatore());
            fatturaDetail.setCfOrganizzatore(manifestazione.getCodiceFiscaleOrganizzatore());
            fatturaDetail.setpIvaOrganizzatore(manifestazione.getPartitaIvaOrganizzatore());
            fatturaDetail.setCodiceSap(manifestazione.getCodiceSapOrganizzatore());
        }
        fatturaDetail.setTipoDocumento(eventiPagati.get(0).getTipoDocumentoContabile());
        //  }
        fatturaDetail.setRiconciliazioneFattura(programmaMusicaleDAO.getRiconciliazioneFattura(numeroFattura));
        Double importoTotale = 0D;
        for (EventiPagati evento : eventiPagati) {
            importoTotale = importoTotale + (evento.getImportDem()!=null ? evento.getImportDem() : 0D);
        }

        if (importoTotale != 0)
            fatturaDetail.setImportoDemTotale(importoTotale.toString());


        if (eventiPagati.get(0).getDataReversale() != null)
            fatturaDetail.setDataFattura(DateUtils.dateToString(eventiPagati.get(0).getDataReversale(), DateUtils.DATE_ITA_FORMAT_DASH));


        return fatturaDetail;
    }

    public Double fattureImportoTotByIdEvento(Double idEvento) {
        return programmaMusicaleDAO.getImportoTotaleOfEvento(idEvento);
    }

    public Integer addUtilizzazione(Utilizzazione utilizzazione) {
        return programmaMusicaleDAO.addUtilizzazione(utilizzazione);
    }


    public ReportPage getListaPmDisponibili(String numeroPm, Integer page) {

        ReportPage reportPage;

        int recordCount = 0;

        List<MovimentoContabile> results = programmaMusicaleDAO.getListaPmDisponibili(numeroPm);

        if (results != null && results.size() > 0) {

            recordCount = results.size();

            int startRecord = 0;
            int endRecord = recordCount;

            if (page != -1) {

                startRecord = (page - 1) * ReportPage.NUMBER_RECORD_PER_PAGE;
                endRecord = 0;
                if (recordCount > ReportPage.NUMBER_RECORD_PER_PAGE) {
                    endRecord = startRecord + ReportPage.NUMBER_RECORD_PER_PAGE;
                    if (endRecord > recordCount)
                        endRecord = recordCount - 1;
                } else {
                    endRecord = recordCount;
                }

            }

        }
        reportPage = new ReportPage(results, recordCount, page);
        return reportPage;
    }

    public void sgancioPm(Long idMovimentoContabile) {
        programmaMusicaleDAO.sgancioPm(idMovimentoContabile);
    }
    
    public void sgancioPm(Long idPm, String voceIncasso, Long idEvento) {

        List<MovimentoContabile> movimenti = programmaMusicaleDAO.getMovimentiByPmAndVoceIncasso(idPm, voceIncasso, idEvento);

        programmaMusicaleDAO.sgancioPm(movimenti);
    }
    public void aggancioPm(String numeroPm, String voceIncasso, Long idEvento) {
        List<MovimentoContabile> movimentiAssociati = programmaMusicaleDAO.getListaPmDisponibili(numeroPm);
        EventiPagati eventoSuVoce = programmaMusicaleDAO.getEventoByVoceIncasso(voceIncasso, idEvento);
//		List<Long> idMovimenti = new ArrayList<Long>();
        Long movimentoMaggiore = null;
        for (MovimentoContabile movimento : movimentiAssociati) {
        }
        Long idPm = movimentiAssociati.get(0).getIdProgrammaMusicale();

//		Map<Long, MovimentoContabile> map = new HashMap<Long, MovimentoContabile>();
        Set<Long> idMovimenti = new HashSet<Long>();

        for (int i = 0; i < movimentiAssociati.size(); i++) {
//			idMovimenti.add(movimentiAssociati.get(i).getId());
//			map.put(movimentiAssociati.get(i).getId(), movimentiAssociati.get(i));
            idMovimenti.add(movimentiAssociati.get(i).getId());
            if (movimentoMaggiore == null || movimentoMaggiore <= movimentiAssociati.get(i).getId()) {
                movimentoMaggiore = movimentiAssociati.get(i).getId();
            }
        }
//		map.remove(movimentoMaggiore);
        idMovimenti.remove(movimentoMaggiore);
        for (Long idMovimento : idMovimenti) {
            programmaMusicaleDAO.removeMovimentiAssociationWithPm(idMovimento);
        }

        programmaMusicaleDAO.updateMostRecentMovimentoOnRiaggancio(movimentoMaggiore, idPm, eventoSuVoce);

        if (eventoSuVoce.getVoceIncasso() == "2224" || eventoSuVoce.getVoceIncasso() == "2233" || eventoSuVoce.getVoceIncasso() == "2234"
                || eventoSuVoce.getVoceIncasso() == "2246" || eventoSuVoce.getVoceIncasso() == "2249" || eventoSuVoce.getVoceIncasso() == "2293") {
            List<Utilizzazione> opere = programmaMusicaleDAO.getUtilizzazioni(idPm);
            Double durataDefault = new Double("100");
            for (Utilizzazione opera : opere) {
                if (opera.getDurata() == null || opera.getDurata().equals("")) {
                    programmaMusicaleDAO.updateOperaOnDetail(durataDefault, opera.getDurataInferiore30sec(), opera.getFlagPubblicoDominio(), opera.getId());
                }
            }

        }
    }

    public void removeOpera(Long idUtilizzazione) {
        programmaMusicaleDAO.removeOpera(idUtilizzazione);
    }

    public void addPm(ProgrammaMusicale programmaMusicale) {
        programmaMusicaleDAO.addPm(programmaMusicale);
    }

    public void updatePm(ProgrammaMusicaleDTO programmaMusicaleDto) {
        programmaMusicaleDAO.updatePm(programmaMusicaleDto);
    }

    public void updateOpera(PerfUtilizzazioniDTO perfUtilizzazioniAddDTO) {
        programmaMusicaleDAO.updateOpera(perfUtilizzazioniAddDTO);
    }

    public Integer controlloPmRientrati(Long idEvento) {
        List<EventiPagati> eventi = programmaMusicaleDAO.getEvento(idEvento);
        return null;
    }

    public Utilizzazione getOpera(Long idOpera) {
        return programmaMusicaleDAO.getOperaById(idOpera);
    }

    public MovimentoContabile getMovimento(Long idMovimento) {
        return programmaMusicaleDAO.getMovimentoContabile(idMovimento);
    }

    public List<ProgrammaMusicale> getListaProgrammiMusicali(String dataIniziale, String dataFinale, Long numeroPM, String tipoDocumento, String seprag, String idEvento,
                                                             String fattura, String reversale, String voceIncasso, String permesso, String locale, String codiceBALocale,
                                                             String codiceSAPOrganizzatore, String organizzatore, String titoloOpera, String dataInizioEvento, String dataFineEvento,
                                                             String direttoreEsecuzione, String supporto, String tipoProgrammi, String gruppoPrincipale, String order, boolean allList) {

        return programmaMusicaleDAO.getAllTracciamentoPM(dataIniziale, dataFinale, numeroPM, tipoDocumento, seprag, idEvento,
                fattura, reversale, voceIncasso, permesso, locale, codiceBALocale,
                codiceSAPOrganizzatore, organizzatore, titoloOpera, dataInizioEvento, dataFineEvento,
                direttoreEsecuzione, supporto, tipoProgrammi, gruppoPrincipale, order);
    }

    public List<ProgrammaMusicale> getListaProgrammiMusicali(String dataIniziale, String dataFinale, Long numeroPM, String tipoDocumento, String seprag, String idEvento,
                                                             String fattura, String reversale, String voceIncasso, String permesso, String locale, String codiceBALocale,
                                                             String codiceSAPOrganizzatore, String organizzatore, String titoloOpera, String dataInizioEvento, String dataFineEvento,
                                                             String direttoreEsecuzione, String supporto, String tipoProgrammi, String gruppoPrincipale, String order) {

        return getListaProgrammiMusicali(dataIniziale, dataFinale, numeroPM, tipoDocumento, seprag, idEvento,
                fattura, reversale, voceIncasso, permesso, locale, codiceBALocale,
                codiceSAPOrganizzatore, organizzatore, titoloOpera, dataInizioEvento, dataFineEvento,
                direttoreEsecuzione, supporto, tipoProgrammi, gruppoPrincipale, order, false);
    }

    public List<MovimentoContabile> getListaMovimenti(Long inizioPeriodoContabile, Long finePeriodoContabile, Long evento, String fattura, Long reversale, String voceIncasso,
                                                      Date dataInizioEvento, Date dataFineEvento, String seprag, String tipoDocumento, String locale, String sapOrganizzatore,
                                                      String tipoSupporto, String tipoProgramma, String gruppoPrincipale, String titoloOpera, String numeroPM, String permesso,
                                                      Integer page, String codiceBALocale, String organizzatore, String order) {

        return getListaMovimenti(inizioPeriodoContabile, finePeriodoContabile, evento, fattura, reversale, voceIncasso,
                dataInizioEvento, dataFineEvento, seprag, tipoDocumento, locale, sapOrganizzatore,
                tipoSupporto, tipoProgramma, gruppoPrincipale, titoloOpera, numeroPM, permesso,
                page, codiceBALocale, organizzatore, order, false);
    }


    public List<MovimentoContabile> getListaMovimenti(Long inizioPeriodoContabile, Long finePeriodoContabile, Long evento, String fattura, Long reversale, String voceIncasso,
                                                      Date dataInizioEvento, Date dataFineEvento, String seprag, String tipoDocumento, String locale, String sapOrganizzatore,
                                                      String tipoSupporto, String tipoProgramma, String gruppoPrincipale, String titoloOpera, String numeroPM, String permesso,
                                                      Integer page, String codiceBALocale, String organizzatore, String order, boolean allList) {

        return programmaMusicaleDAO.getListaMovimentazioni(inizioPeriodoContabile, finePeriodoContabile, evento, fattura, reversale, voceIncasso,
                dataInizioEvento, dataFineEvento, seprag, tipoDocumento, locale, sapOrganizzatore,
                tipoSupporto, tipoProgramma, gruppoPrincipale, titoloOpera, numeroPM, permesso, page, codiceBALocale, organizzatore, order, allList);

    }

    public List<EventiPagati> getListaEvPagati(PerfEventiPagatiDTO perfEventiPagatiDTO, String inizioPeriodoContabile, String finePeriofoContabile, String organizzatore,
                                               String direttoreEsecuzione, String permesso, String numeroPM, String titoloOpera, String codiceSAP,
                                               String dataInizioEvento, String dataFineEvento, String fatturaValidata, String order) {
        return programmaMusicaleDAO.getListaEventiPagati(inizioPeriodoContabile, finePeriofoContabile, perfEventiPagatiDTO,
                organizzatore, direttoreEsecuzione, permesso, numeroPM, titoloOpera, codiceSAP, dataInizioEvento, dataFineEvento, fatturaValidata, order, true);
    }

    public void addOpera(Utilizzazione utilizzazione) {
        programmaMusicaleDAO.addOpera(utilizzazione);
    }

    public EventiPagati getEventoPagato(Long idEventoPagato) {
        return programmaMusicaleDAO.getEventoPagato(idEventoPagato);
    }

    public List<String> getVociIncassoPerIdEvento(Long idEvento) {
        return programmaMusicaleDAO.getVociIncassoPerIdEvento(idEvento);
    }

    public List<Long> getIdMovimentiDaSganciare(Long idPm, String voceIncasso, Long idEvento) {
        List<MovimentoContabile> movimenti = programmaMusicaleDAO.getMovimentiByPmAndVoceIncasso(idPm, voceIncasso, idEvento);
        List<Long> idMovimenti = new ArrayList<Long>();
        for (MovimentoContabile movimento : movimenti) {
            idMovimenti.add(movimento.getId());
        }
        return idMovimenti;
    }


    public List<String> addConfigurazioneAggioMc(AggiMcDTO aggio) {
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        f.setTimeZone(TimeZone.getTimeZone("GMT+2"));
//		DATETIME_MYSQL_FORMAT
        List<String> errori = new ArrayList<String>();
        List<String> erroriCodice = getValidazioneCodiceRegola(aggio.getCodiceRegola());
        errori.addAll(getValidazioneDate(aggio.getInizioPeriodoValidazione(), aggio.getFinePeriodoValidazione()));
        errori.addAll(erroriCodice);
        if (!errori.isEmpty()) {
            return errori;
        }

        AggiMc newAggio = new AggiMc();
        newAggio.setInizioPeriodoValidazione(DateUtils.stringToDate(aggio.getInizioPeriodoValidazione(), DateUtils.DATE_ITA_FORMAT_DASH));
        if (aggio.getFinePeriodoValidazione() != null) {
            newAggio.setFinePeriodoValidazione(DateUtils.stringToDate(aggio.getFinePeriodoValidazione(), DateUtils.DATE_ITA_FORMAT_DASH));
        }
        newAggio.setSoglia(new BigDecimal(aggio.getSoglia()));
        newAggio.setAggioSottosoglia(new BigDecimal(aggio.getAggioSottosoglia()));
        newAggio.setAggioSoprasoglia(new BigDecimal(aggio.getAggioSoprasoglia()));
        newAggio.setValoreCap(new BigDecimal(aggio.getValoreCap()));
        newAggio.setFlagCapMultiplo(aggio.getFlagCapMultiplo());
        newAggio.setUtenteCreazione(aggio.getUtenteCreazione());
        newAggio.setCodiceRegola(aggio.getCodiceRegola());
        newAggio.setFlagAttivo(true);
        newAggio.setDataCreazione(DateUtils.stringToDate(f.format(new Date()), DateUtils.DATETIME_MYSQL_FORMAT));
        programmaMusicaleDAO.addConfigurazioneAggioMc(newAggio);
        List<Long> idEventi = getEventiToEditOnAggio(aggio);
        Connection connection = programmaMusicaleDAO.getConfiguration();
        for (Long idEvento : idEventi) {

            try {
                MegaConcertUtils.updateEvento(connection, idEvento);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return errori;
    }

    public List<AggiMc> getListaAggiAttivi(String inizioPeriodoValidazione, String finePeriodoValidazione, String codiceRegola, String order) {
        return programmaMusicaleDAO.getListaAggiAttivi(inizioPeriodoValidazione, finePeriodoValidazione, codiceRegola, order);
    }

    public AggiMc getAggioAttivo(Long id) {
        return programmaMusicaleDAO.getAggioAttivo(id);
    }

    public List<String> getValidazioneDate(String inizioPeriodoValidazione, String finePeriodoValidazione) {
        List<String> errori = new ArrayList<String>();
        List<String> aggiConflitto;
        AggiMc aggioInCorso = programmaMusicaleDAO.aggioIsInCorso();
        if (finePeriodoValidazione == null) {
            if (aggioInCorso != null) {
                errori.add("Periodo di validità del codice regola " + aggioInCorso.getCodiceRegola() + " in corso");
            } else {
                aggiConflitto = programmaMusicaleDAO.getCodiciRegolaAfterDate(inizioPeriodoValidazione);
                if (!aggiConflitto.isEmpty()) {
                    for (String aggioConflitto : aggiConflitto) {
                        errori.add("Conflitto nelle date del codice regola " + aggioConflitto);
                    }
                }
            }
        } else {
            if (aggioInCorso != null && (aggioInCorso.getInizioPeriodoValidazione().before(DateUtils.stringToDate(inizioPeriodoValidazione, DateUtils.DATE_ITA_FORMAT_DASH)) ||
                    aggioInCorso.getInizioPeriodoValidazione().before(DateUtils.stringToDate(finePeriodoValidazione, DateUtils.DATE_ITA_FORMAT_DASH)))) {
                errori.add("Periodo di validità del codice regola " + aggioInCorso.getCodiceRegola() + " in corso");
            } else {
                aggiConflitto = programmaMusicaleDAO.getValidazioneDate(inizioPeriodoValidazione, finePeriodoValidazione);
                if (!aggiConflitto.isEmpty()) {
                    for (String aggioConflitto : aggiConflitto) {
                        errori.add("Conflitto nelle date del codice regola " + aggioConflitto);
                    }
                }
            }
        }
        return errori;
    }

    public List<String> getValidazioneDateOnUpdate(String inizioPeriodoValidazione, String finePeriodoValidazione, String codiceRegola) {
        List<String> errori = new ArrayList<String>();
        List<String> aggiConflitto;
        AggiMc aggioInCorso = programmaMusicaleDAO.aggioIsInCorso();
        if (finePeriodoValidazione == null) {
            if (aggioInCorso != null && !aggioInCorso.getCodiceRegola().equals(codiceRegola)) {
                errori.add("Periodo di validità del codice regola " + aggioInCorso.getCodiceRegola() + " in corso");
            } else {
                aggiConflitto = programmaMusicaleDAO.getCodiciRegolaAfterDate(inizioPeriodoValidazione);
                if (!aggiConflitto.isEmpty()) {
                    for (String aggioConflitto : aggiConflitto) {
                        if (!aggioConflitto.equals(codiceRegola)) {
                            errori.add("Conflitto nelle date del codice regola " + aggioConflitto);
                        }
                    }
                }
            }
        } else {
            if (aggioInCorso != null && !aggioInCorso.getCodiceRegola().equals(codiceRegola) &&
                    (aggioInCorso.getInizioPeriodoValidazione().before(DateUtils.stringToDate(inizioPeriodoValidazione, DateUtils.DATE_ITA_FORMAT_DASH)) ||
                            aggioInCorso.getInizioPeriodoValidazione().before(DateUtils.stringToDate(finePeriodoValidazione, DateUtils.DATE_ITA_FORMAT_DASH)))) {
                errori.add("Periodo di validità del codice regola " + aggioInCorso.getCodiceRegola() + " in corso");
            } else {
                aggiConflitto = programmaMusicaleDAO.getValidazioneDateOnUpdate(inizioPeriodoValidazione, finePeriodoValidazione, codiceRegola);
                if (!aggiConflitto.isEmpty()) {
                    for (String aggioConflitto : aggiConflitto) {
                        if (!aggioConflitto.equals(codiceRegola)) {
                            errori.add("Conflitto nelle date del codice regola " + aggioConflitto);
                        }
                    }
                }
            }
        }
        return errori;
    }

    public List<String> getValidazioneParametri(AggiMcDTO aggio) {
        List<String> errori = new ArrayList<String>();
        if (Double.valueOf(aggio.getAggioSoprasoglia()) > 100) {
            errori.add("percentuale aggio soprasoglia non valido");
        }
        if (Double.valueOf(aggio.getAggioSottosoglia()) > 100) {
            errori.add("percentuale aggio sottosoglia non valido");
        }
        return errori;
    }

    public List<String> getValidazioneCodiceRegola(String codiceRegola) {
        List<String> errori = new ArrayList<String>();
        List<String> aggiConflitto = programmaMusicaleDAO.getValidazioneCodiceRegola(codiceRegola);
        if (!aggiConflitto.isEmpty()) {
            for (String codice : aggiConflitto) {
                errori.add("Codice Regola " + codice + " gia esistente");
            }
        }
        return errori;
    }


    public List<String> getTipoModificaOnAggio(AggiMcDTO aggio, AggiMc currentAggio) {
        List<String> tipoModifica = new ArrayList<String>();

        Date inizioPeriodo = DateUtils.stringToDate(aggio.getInizioPeriodoValidazione(), DateUtils.DATE_ITA_FORMAT_DASH);
        Date finePeriodo = aggio.getFinePeriodoValidazione() != null ? DateUtils.stringToDate(aggio.getFinePeriodoValidazione(), DateUtils.DATE_ITA_FORMAT_DASH) : null;

        if (
                !currentAggio.getInizioPeriodoValidazione().equals(inizioPeriodo) ||
                        (
                                (currentAggio.getFinePeriodoValidazione() != null && finePeriodo != null)
                                        && !currentAggio.getFinePeriodoValidazione().equals(finePeriodo)) ||
                        (currentAggio.getFinePeriodoValidazione() != null && finePeriodo == null) ||
                        (currentAggio.getFinePeriodoValidazione() == null && finePeriodo != null)
        ) {
            tipoModifica.add("Variazione Validita");
        }
        if (currentAggio.getSoglia().compareTo(new BigDecimal(aggio.getSoglia())) != 0 ||
                currentAggio.getAggioSottosoglia().compareTo(new BigDecimal(aggio.getAggioSottosoglia())) != 0 ||
                currentAggio.getAggioSoprasoglia().compareTo(new BigDecimal(aggio.getAggioSoprasoglia())) != 0 ||
                currentAggio.getValoreCap().compareTo(new BigDecimal(aggio.getValoreCap())) != 0 ||
                currentAggio.getFlagCapMultiplo() != aggio.getFlagCapMultiplo()
        ) {
            tipoModifica.add("Modifica Parametri");

        }
        return tipoModifica;
    }

    public List<String> updateAggi(AggiMcDTO aggio) {
        AggiMc currentAggio = programmaMusicaleDAO.getAggioMcById(aggio.getId());

        List<String> errori = new ArrayList<String>();

        List<String> tipoModifica = getTipoModificaOnAggio(aggio, currentAggio);
        if (tipoModifica.isEmpty()) {
            errori.add("Nessun dato da modificare");
            return errori;
        } else if (tipoModifica.size() == 2) {
            aggio.setTipoUltimaModifica("Modifica Parametri e Validita");
        } else {
            aggio.setTipoUltimaModifica(tipoModifica.get(0));

        }
        List<String> erroriDate = getValidazioneDateOnUpdate(aggio.getInizioPeriodoValidazione(), aggio.getFinePeriodoValidazione(), aggio.getCodiceRegola());
        List<String> erroriParametri = getValidazioneParametri(aggio);

        errori.addAll(erroriDate);
        errori.addAll(erroriParametri);
        if (!errori.isEmpty())
            return errori;

        AggiMc aggioOldHistory = new AggiMc();
        aggioOldHistory.setInizioPeriodoValidazione(currentAggio.getInizioPeriodoValidazione());
        if (currentAggio.getFinePeriodoValidazione() != null) {
            aggioOldHistory.setFinePeriodoValidazione(currentAggio.getFinePeriodoValidazione());
        }
        aggioOldHistory.setSoglia(currentAggio.getSoglia());
        aggioOldHistory.setAggioSottosoglia(currentAggio.getAggioSottosoglia());
        aggioOldHistory.setAggioSoprasoglia(currentAggio.getAggioSoprasoglia());
        aggioOldHistory.setValoreCap(currentAggio.getValoreCap());
        aggioOldHistory.setFlagCapMultiplo(currentAggio.getFlagCapMultiplo());
        aggioOldHistory.setDataCreazione(currentAggio.getDataCreazione());
        aggioOldHistory.setUtenteCreazione(currentAggio.getUtenteCreazione());
        aggioOldHistory.setCodiceRegola(currentAggio.getCodiceRegola());
        if (currentAggio.getTipoUltimaModifica() != null)
            aggioOldHistory.setTipoUltimaModifica(currentAggio.getTipoUltimaModifica());
        if (currentAggio.getUtenteUltimaModifica() != null)
            aggioOldHistory.setUtenteUltimaModifica(currentAggio.getUtenteUltimaModifica());
        if (currentAggio.getDataUltimaModifica() != null)
            aggioOldHistory.setDataUltimaModifica(currentAggio.getDataUltimaModifica());
        aggioOldHistory.setFlagAttivo(false);


        programmaMusicaleDAO.updateAggio(aggio);
        programmaMusicaleDAO.addConfigurazioneAggioMc(aggioOldHistory);

        AggiMcDTO aggioOldDTO = new AggiMcDTO();
        aggioOldDTO.setId(currentAggio.getId());
        aggioOldDTO.setInizioPeriodoValidazione(DateUtils.dateToString(currentAggio.getInizioPeriodoValidazione(), DateUtils.DATE_ITA_FORMAT_DASH));
        if (aggioOldHistory.getFinePeriodoValidazione() != null)
            aggioOldDTO.setFinePeriodoValidazione(DateUtils.dateToString(currentAggio.getFinePeriodoValidazione(), DateUtils.DATE_ITA_FORMAT_DASH));

        List<Long> idEventi = getEventiToEditOnAggio(aggioOldDTO);
        Connection connection = programmaMusicaleDAO.getConfiguration();
        for (Long idEvento : idEventi) {

            try {
                MegaConcertUtils.updateEvento(connection, idEvento);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return errori;
    }


    public List<String> updateDataAggi(AggiMcDTO aggio) {
        List<String> errori = getValidazioneDateOnUpdate(aggio.getInizioPeriodoValidazione(), aggio.getFinePeriodoValidazione(), aggio.getCodiceRegola());
        if (!errori.isEmpty())
            return errori;

        AggiMc currentAggio = programmaMusicaleDAO.getAggioMcById(aggio.getId());
        AggiMc aggioOldHistory = new AggiMc();
        aggioOldHistory.setInizioPeriodoValidazione(currentAggio.getInizioPeriodoValidazione());
        if (currentAggio.getFinePeriodoValidazione() != null) {
            aggioOldHistory.setFinePeriodoValidazione(currentAggio.getFinePeriodoValidazione());
        }
        aggioOldHistory.setSoglia(currentAggio.getSoglia());
        aggioOldHistory.setAggioSottosoglia(currentAggio.getAggioSottosoglia());
        aggioOldHistory.setAggioSoprasoglia(currentAggio.getAggioSoprasoglia());
        aggioOldHistory.setValoreCap(currentAggio.getValoreCap());
        aggioOldHistory.setFlagCapMultiplo(currentAggio.getFlagCapMultiplo());
        aggioOldHistory.setDataCreazione(currentAggio.getDataCreazione());
        aggioOldHistory.setUtenteCreazione(currentAggio.getUtenteCreazione());
        aggioOldHistory.setCodiceRegola(currentAggio.getCodiceRegola());
        if (currentAggio.getTipoUltimaModifica() != null)
            aggioOldHistory.setTipoUltimaModifica(currentAggio.getTipoUltimaModifica());
        if (currentAggio.getUtenteUltimaModifica() != null)
            aggioOldHistory.setUtenteUltimaModifica(currentAggio.getUtenteUltimaModifica());
        if (currentAggio.getDataUltimaModifica() != null)
            aggioOldHistory.setDataUltimaModifica(currentAggio.getDataUltimaModifica());
        aggioOldHistory.setFlagAttivo(false);
        Date finePeriodo = null;
        if (aggio.getFinePeriodoValidazione() != null) {
            finePeriodo = DateUtils.stringToDate(aggio.getFinePeriodoValidazione(), DateUtils.DATE_ITA_FORMAT_DASH);
        }
        Date inizioPeriodo = DateUtils.stringToDate(aggio.getInizioPeriodoValidazione(), DateUtils.DATE_ITA_FORMAT_DASH);
//	    if(aggioOldHistory.getInizioPeriodoValidazione().equals(inizioPeriodo) && 
//	    		(
//	    				(aggioOldHistory.getFinePeriodoValidazione()==null && finePeriodo==null) || 
//	    				aggioOldHistory.getFinePeriodoValidazione().equals(finePeriodo))
//	    		) {
//	    	errori.add("Le date inserite sono uguali alla configurazione corrente");
//	    	return errori;
//	    }
        programmaMusicaleDAO.updateDataAggio(inizioPeriodo, finePeriodo, aggio.getId(), aggio.getUtenteUltimaModifica());
        programmaMusicaleDAO.addConfigurazioneAggioMc(aggioOldHistory);

        AggiMcDTO aggioOldDTO = new AggiMcDTO();
        aggioOldDTO.setId(currentAggio.getId());
        aggioOldDTO.setInizioPeriodoValidazione(DateUtils.dateToString(currentAggio.getInizioPeriodoValidazione(), DateUtils.DATE_ITA_FORMAT_DASH));
        if (aggioOldHistory.getFinePeriodoValidazione() != null)
            aggioOldDTO.setFinePeriodoValidazione(DateUtils.dateToString(currentAggio.getFinePeriodoValidazione(), DateUtils.DATE_ITA_FORMAT_DASH));

        List<Long> idEventi = getEventiToEditOnAggio(aggioOldDTO);
        Connection connection = programmaMusicaleDAO.getConfiguration();
        for (Long idEvento : idEventi) {

            try {
                MegaConcertUtils.updateEvento(connection, idEvento);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return errori;
    }

    public List<String> updateParametriAggi(AggiMcDTO aggio) {
        List<String> errori = getValidazioneParametri(aggio);
        if (!errori.isEmpty()) {
            return errori;
        }

        AggiMc currentAggio = programmaMusicaleDAO.getAggioMcById(aggio.getId());
        AggiMc aggioOldHistory = new AggiMc();
        aggioOldHistory.setInizioPeriodoValidazione(currentAggio.getInizioPeriodoValidazione());
        if (currentAggio.getFinePeriodoValidazione() != null) {
            aggioOldHistory.setFinePeriodoValidazione(currentAggio.getFinePeriodoValidazione());
        }
        aggioOldHistory.setSoglia(currentAggio.getSoglia());
        aggioOldHistory.setAggioSottosoglia(currentAggio.getAggioSottosoglia());
        aggioOldHistory.setAggioSoprasoglia(currentAggio.getAggioSoprasoglia());
        aggioOldHistory.setValoreCap(currentAggio.getValoreCap());
        aggioOldHistory.setFlagCapMultiplo(currentAggio.getFlagCapMultiplo());
        aggioOldHistory.setDataCreazione(currentAggio.getDataCreazione());
        aggioOldHistory.setUtenteCreazione(currentAggio.getUtenteCreazione());
        aggioOldHistory.setCodiceRegola(currentAggio.getCodiceRegola());
        if (currentAggio.getTipoUltimaModifica() != null)
            aggioOldHistory.setTipoUltimaModifica(currentAggio.getTipoUltimaModifica());
        if (currentAggio.getUtenteUltimaModifica() != null)
            aggioOldHistory.setUtenteUltimaModifica(currentAggio.getUtenteUltimaModifica());
        if (currentAggio.getDataUltimaModifica() != null)
            aggioOldHistory.setDataUltimaModifica(currentAggio.getDataUltimaModifica());
        aggioOldHistory.setFlagAttivo(false);

        programmaMusicaleDAO.updateParametriAggio(aggio);
        programmaMusicaleDAO.addConfigurazioneAggioMc(aggioOldHistory);
        List<Long> idEventi = getEventiToEditOnAggio(aggio);
        Connection connection = programmaMusicaleDAO.getConfiguration();
        for (Long idEvento : idEventi) {
            try {
                MegaConcertUtils.updateEvento(connection, idEvento);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return errori;
    }

    public List<AggiMc> getStoricoAggio(String codiceRegola) {
        return programmaMusicaleDAO.getStoricoAggio(codiceRegola);
    }

    public ReportPage getRiconciliazioneImportiList(String voceIncasso, String agenzia, String sede, String seprag, String singolaFattura,
                                                    String inizioPeriodoContabile, String finePeriodoContabile, Date inizioPeriodoFattura, Date finePeriodoFattura, Integer page, String order,String fatturaValidata,String megaConcerto) {

//        }
        return programmaMusicaleDAO.getRiconciliazioneImportiList(voceIncasso, agenzia, sede, seprag, singolaFattura,
                inizioPeriodoContabile, finePeriodoContabile, inizioPeriodoFattura, finePeriodoFattura, order, page,fatturaValidata,megaConcerto);
    }

    public TotaleRiconciliazioneImporti getCountRiconciliazioneImportiList(String voceIncasso, String agenzia, String sede, String seprag, String singolaFattura,
                                                   String inizioPeriodoContabile, String finePeriodoContabile, Date inizioPeriodoFattura, Date finePeriodoFattura,String fatturaValidata, String megaConcerto) {

        TotaleRiconciliazioneImporti result = programmaMusicaleDAO.getCountRiconciliazioneImportiList(voceIncasso, agenzia, sede, seprag, singolaFattura,
                inizioPeriodoContabile, finePeriodoContabile, inizioPeriodoFattura, finePeriodoFattura,fatturaValidata,megaConcerto);

        return result;
    }

    public String updateFlagMegaConcert(FatturaDetailDTO fatturaDetailDTO) {
//    	programmaMusicaleDAO.getAggioByDate();
        List<String> regole = getValidazioneDate(fatturaDetailDTO.getDataFattura(), fatturaDetailDTO.getDataFattura());
        if (!regole.isEmpty() || !fatturaDetailDTO.getManifestazione().getFlagMegaConcert()) {
            programmaMusicaleDAO.updateFlagMegaConcert(fatturaDetailDTO);
            Connection connection = programmaMusicaleDAO.getConfiguration();

            try {
                MegaConcertUtils.updateEvento(connection, fatturaDetailDTO.getManifestazione().getId());
                return "Megaconcerto modificato con successo";
            } catch (SQLException e) {
                e.printStackTrace();
                return "Non è stato possibile attivare il Megaconcerto";
            }
        } else {
            return "Non ci sono configurazioni Megaconcerto per l'evento selezionato";
        }

    }


    public List<Long> getEventiToEditOnAggio(AggiMcDTO aggio) {
        if (aggio.getId() != null) {
            AggiMc currentAggio = programmaMusicaleDAO.getAggioMcById(aggio.getId());
            if (currentAggio.getInizioPeriodoValidazione() != null) {
                String dataInizioValidazione = "";
                if (aggio.getInizioPeriodoValidazione() != null) {
                    dataInizioValidazione = (
                            currentAggio.getInizioPeriodoValidazione().before(
                                    DateUtils.stringToDate(aggio.getInizioPeriodoValidazione(), DateUtils.DATE_ITA_FORMAT_DASH)
                            )
                    )
                            ? DateUtils.dateToString(currentAggio.getInizioPeriodoValidazione(), DateUtils.DATE_ITA_FORMAT_DASH) : aggio.getInizioPeriodoValidazione();
                } else {
                    dataInizioValidazione = DateUtils.dateToString(currentAggio.getInizioPeriodoValidazione(), DateUtils.DATE_ITA_FORMAT_DASH);
                }

                aggio.setInizioPeriodoValidazione(dataInizioValidazione);

                if (currentAggio.getFinePeriodoValidazione() != null &&
                        aggio.getFinePeriodoValidazione() != null) {
                    String dataFineValidazione = (
                            currentAggio.getFinePeriodoValidazione().after(
                                    DateUtils.stringToDate(aggio.getFinePeriodoValidazione(), DateUtils.DATE_ITA_FORMAT_DASH)
                            )
                    )
                            ? DateUtils.dateToString(currentAggio.getFinePeriodoValidazione(), DateUtils.DATE_ITA_FORMAT_DASH) : aggio.getFinePeriodoValidazione();
                    aggio.setFinePeriodoValidazione(dataFineValidazione);
                } else {
                    aggio.setFinePeriodoValidazione(null);
                }
            }

        }
        return programmaMusicaleDAO.getEventiToEditOnAggio(aggio);
    }

    @Override
    public PerfCodificaDTO getCodifica(Long idCombana, String user) {
        PerfCodificaDTO codifica = programmaMusicaleDAO.getCodifica(idCombana, user);
        if(codifica!=null) {
            codificaDAO.endSessioneCodifica(user);
            codificaDAO.refreshSessioneCodifica(user, codifica.getIdCodifica());
            return codifica;
        }
        return null;
    }

	@Override
	public FatturaDetailDTO riconciliaImporti(RiconciliazioneImportiDTO riconciliazioneImporti, String username) {
		programmaMusicaleDAO.riconciliaImporti(riconciliazioneImporti, username);
		return getFatturaDetail(riconciliazioneImporti.getNumeroFattura());
	}

	@Override
	public FatturaDetailDTO riconciliaVociIncasso(RiconciliazioneImportiDTO riconciliazioneImporti, String username) {
		programmaMusicaleDAO.riconciliaVociIncasso(riconciliazioneImporti, username);
		return getFatturaDetail(riconciliazioneImporti.getNumeroFattura());
	}

	@Override
	public FatturaDetailDTO ripristinaRiconciliazione(RiconciliazioneImportiDTO riconciliazioneImporti, String username) {
		programmaMusicaleDAO.ripristinaRiconciliazione(riconciliazioneImporti, username);
		return getFatturaDetail(riconciliazioneImporti.getNumeroFattura());
	}

}
