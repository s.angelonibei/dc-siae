package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;

import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;
@Produces("application/json")
@XmlRootElement 
public class CommercialOffersConfigDTO implements Serializable{

	private static final long serialVersionUID = -2297161058485331327L;

	private int idCommercialOffering;
	private String idDSP;
	private String idUtilizationType;
	private String offering;
	private String serviceType;
	private String useType;
	private String appliedTariff;
	private String tradingBrand;

	public CommercialOffersConfigDTO(){
		
	}

	public int getIdCommercialOffering() {
		return idCommercialOffering;
	}

	public void setIdCommercialOffering(int idCommercialOffering) {
		this.idCommercialOffering = idCommercialOffering;
	}

	public String getIdDSP() {
		return idDSP;
	}

	public void setIdDSP(String idDSP) {
		this.idDSP = idDSP;
	}

	public String getIdUtilizationType() {
		return idUtilizationType;
	}

	public void setIdUtilizationType(String idUtilizationType) {
		this.idUtilizationType = idUtilizationType;
	}

	public String getOffering() {
		return offering;
	}

	public void setOffering(String offering) {
		this.offering = offering;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getUseType() {
		return useType;
	}

	public void setUseType(String useType) {
		this.useType = useType;
	}

	public String getAppliedTariff() {
		return appliedTariff;
	}

	public void setAppliedTariff(String appliedTariff) {
		this.appliedTariff = appliedTariff;
	}

	public String getTradingBrand() {
		return tradingBrand;
	}

	public void setTradingBrand(String tradingBrand) {
		this.tradingBrand = tradingBrand;
	}
}
