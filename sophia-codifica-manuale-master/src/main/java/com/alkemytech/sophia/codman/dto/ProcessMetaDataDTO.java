package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ProcessMetaDataDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String processID;

	private String description;

	private List<?> executions;
	
	private String tag;
	
	private ProcessOperationOutcomeDTO outcome;

	public String getProcessID() {
		return processID;
	}

	public void setProcessID(String processID) {
		this.processID = processID;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<?> getExecutions() {
		return executions;
	}

	public void setExecutions(List<?> executions) {
		this.executions = executions;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public ProcessOperationOutcomeDTO getOutcome() {
		return outcome;
	}

	public void setOutcome(ProcessOperationOutcomeDTO outcome) {
		this.outcome = outcome;
	}

}
