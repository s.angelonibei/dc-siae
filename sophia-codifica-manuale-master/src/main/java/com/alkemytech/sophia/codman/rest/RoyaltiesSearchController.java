package com.alkemytech.sophia.codman.rest;

import com.alkemytech.sophia.codman.dto.RoyaltiesSearchRequestDTO;
import com.alkemytech.sophia.codman.dto.RoyaltiesStorageMetadataDTO;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.alkemytech.sophia.common.s3.S3Service;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.google.gson.*;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import net.sf.json.JSONArray;
import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.config.ResultType;
import org.eclipse.persistence.sessions.Record;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Singleton
@Path("royalties-search")
public class RoyaltiesSearchController {

    public static final String READY = "READY";
    public static final String TRANSFERING = "TRANSFERING";

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final Gson gson;
    private final Properties configuration;
    private final S3Service s3Service;
    private final Provider<EntityManager> provider;


    @Inject
    protected RoyaltiesSearchController(Gson gson, @Named("configuration") Properties configuration,
                                        S3Service s3Service, @McmdbDataSource Provider<EntityManager> provider) {
        super();
        this.gson = gson;
        this.configuration = configuration;
        this.s3Service = s3Service;
        this.provider = provider;
    }

    @Path("list-royalties-storages")
    @GET
    @Produces("application/json")
    public Response listRoyaltiesStorages(@QueryParam(value = "dumps") List<String> dumpsRequest) {
        try {
            String s3Bucket = configuration.getProperty("royalties_search.s3.bucket");
            String[] dumps;
            if(dumpsRequest.size()>0){
                dumps= dumpsRequest.toArray(new String[0]);
            }else {
                dumps= configuration.getProperty("royalties_search.s3.dumps").split(",");
            }

            List<S3ObjectSummary> s3ObjectSummaries = new ArrayList<>();
            for (String dump : dumps) {
                s3ObjectSummaries.addAll(s3Service.listObjects(s3Bucket, configuration.getProperty("royalties_search.s3.dump." + dump)));
            }

            Pattern regex = Pattern.compile(configuration.getProperty("royalties_search.s3.file.latest.regexp"));

            Pattern regexLocation = Pattern.compile(configuration.getProperty("royalties_search.s3.file.latest.location.property.regexp"));


            //Map<String, Map<String, JsonArray>> storages = new TreeMap<>();


            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy");

            JsonArray storageList = new JsonArray();

            for (S3ObjectSummary s3ObjectSummary : s3ObjectSummaries) {
                if (regex.matcher(s3ObjectSummary.getKey()).matches()) {
                    InputStream in = s3Service.download(s3ObjectSummary.getBucketName(), s3ObjectSummary.getKey());
                    Properties latest = new Properties();
                    latest.load(in);
                    String location = latest.getProperty("location");
                    Matcher matcher = regexLocation.matcher(location);
                    if (matcher.matches()) {
                        String dump = matcher.group(1);
                        String year = matcher.group(2);
                        String month = matcher.group(3);
                        String updatedDate = matcher.group(4);


                        JsonObject dumpUpdateDate = new JsonObject();
                        dumpUpdateDate.addProperty("year", year);
                        dumpUpdateDate.addProperty("month", month);
                        dumpUpdateDate.addProperty("dump", dump);
                        dumpUpdateDate.addProperty("updatedDate", sdf2.format(sdf.parse(updatedDate)));

                        storageList.add(dumpUpdateDate);

                        /*Map<String, JsonArray> yearStorages;
                        JsonArray monthStorages;
                        if (storages.containsKey(year)) {
                            yearStorages = storages.get(year);
                            if (yearStorages.containsKey(month)) {
                                 monthStorages = yearStorages.get(month);
                            } else {
                                 monthStorages = new JsonArray();
                                 yearStorages.put(month, monthStorages);
                            }
                        } else {
                            yearStorages = new TreeMap<>();
                            monthStorages = new JsonArray();
                            storages.put(year, yearStorages);
                            yearStorages.put(month, monthStorages);
                        }
                        monthStorages.add(dumpUpdateDate);*/

                    }

                }

            }


            JsonObject result = new JsonObject();
            result.addProperty("status", "READY");
            result.add("storages", storageList);


            JsonParser parser = new JsonParser();
            String json = "{\r\n" +
                    "	\"status\": \"READY\",\r\n" +
                    "	\"statusMessage\": \"\",\r\n" +
                    "	\"storages\":\r\n" +
                    "		[\r\n" +
                    "			{\r\n" +
                    "				\"year\": 2016,\r\n" +
                    "				\"month\": 1,\r\n" +
                    "				\"updatedDate\": \"2020-01-25T10:08:26.941Z\",\r\n" +
                    "				\"isActive\": false\r\n" +
                    "			},\r\n" +
                    "			{\r\n" +
                    "				\"year\": 2019,\r\n" +
                    "				\"month\": 2,\r\n" +
                    "				\"updatedDate\": \"2020-01-25T10:08:26.941Z\",\r\n" +
                    "				\"isActive\": false\r\n" +
                    "			},\r\n" +
                    "			{\r\n" +
                    "				\"year\": 2020,\r\n" +
                    "				\"month\": 3,\r\n" +
                    "				\"updatedDate\": \"2020-01-25T10:08:26.941Z\",\r\n" +
                    "				\"isActive\": true\r\n" +
                    "			}\r\n" +
                    "		]\r\n" +
                    "}";
            // Restituire una lisa di RoyaltiesStorageMetadataDTO
            JsonElement jsonTree = parser.parse(json);
            return Response.ok(result.toString()).build();
        } catch (Exception e) {
            logger.error("Errore...", e);
            return Response.status(500).build();
        }
    }

    @GET
    @Path("active-royalties-storage")
    @Produces("application/json")
    @Consumes("application/json")
    public Response activeRoyaltiesStorage() {
        Client client = Client.create();
        WebResource resource = client.resource(configuration.getProperty("royalties_search.ws.view.endpoint"));
        String response = resource.type("application/json").get(String.class);
        return Response.ok(response).build();
    }

    @Path("request-royalties-storage")
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @SuppressWarnings("unchecked")
    public Response requestRoyaltiesStorage(RoyaltiesStorageMetadataDTO request) {
        try {
            String sql = "SELECT bl1.CODICE_SOCIETA_TUTELA, bl1.XLS_S3_URL\n" +
                    " FROM (SELECT bl.*\n" +
                    "       FROM BLACKLIST bl\n" +
                    "       WHERE DATE_SUB(bl.VALID_FROM, INTERVAL 1 DAY) <= STR_TO_DATE(?1,'%Y-%m')) bl1\n" +
                    "          LEFT JOIN (SELECT bl.*\n" +
                    "                     FROM BLACKLIST bl\n" +
                    "                     WHERE DATE_SUB(bl.VALID_FROM, INTERVAL 1 DAY) <= STR_TO_DATE(?1,'%Y-%m')) bl2\n" +
                    "                    ON bl1.CODICE_SOCIETA_TUTELA = bl2.CODICE_SOCIETA_TUTELA\n" +
                    "                        AND bl1.VALID_FROM < bl2.VALID_FROM\n" +
                    " WHERE bl2.VALID_FROM IS NULL";

            EntityManager entityManager = provider.get();
            List<Record> blacklistsResult = entityManager.createNativeQuery(sql)
                    .setParameter(1, request.getYear() + "-" + request.getMonth())
                    .setHint(QueryHints.RESULT_TYPE, ResultType.Map)
                    .getResultList();

            JsonObject requestUpdate = new JsonObject();
            JsonObject blacklists = new JsonObject();
            for (Record record : blacklistsResult) {
                blacklists.addProperty((String) record.get("CODICE_SOCIETA_TUTELA"), (String) record.get("XLS_S3_URL"));
            }
            requestUpdate.addProperty("year", request.getYear());
            requestUpdate.addProperty("month", request.getMonth());
            requestUpdate.add("black_list", blacklists);

            Client client = Client.create();
            WebResource resource = client.resource(configuration.getProperty("royalties_search.ws.update.endpoint"));
            String response = resource.type("application/json").post(String.class, requestUpdate.toString());

            JsonObject responseJson = gson.fromJson(response, JsonObject.class);
            if (responseJson.has("errore")) {
                return Response.status(500).entity(response).build();
            }

            JsonParser parser = new JsonParser();
            String json = "{\r\n" +
                    "	\"status\": \"READY\",\r\n" +
                    "	\"statusMessage\": \"In corso il recupero del periodo "+request.getMonth() + " "+ request.getYear() +"\"\r\n" +
                    "}";
            JsonElement jsonTree = parser.parse(json);
            return Response.ok(gson.toJson(jsonTree)).build();
        } catch (Exception e) {
            logger.error("Errore...", e);
            return Response.status(500).build();
        }
    }

    @POST
    @Produces("application/json")
    @Consumes("application/json")
    public Response search(RoyaltiesSearchRequestDTO request) {
        try {
            boolean twoResult=false;
            JsonObject searchRoyaltiesRequest = new JsonObject();
            searchRoyaltiesRequest.addProperty("dspCode", request.getAnagDsp().getCode());
            searchRoyaltiesRequest.addProperty("territorio", request.getCountry().getCode());
            if (StringUtils.isNotEmpty(request.getUuid())) {
                searchRoyaltiesRequest.addProperty("uuid", request.getUuid());
                twoResult=true;
            } else if (StringUtils.isNotEmpty(request.getSociety()) && StringUtils.isNotEmpty(request.getWorkCode())) {
                searchRoyaltiesRequest.addProperty("codiceOpera", request.getWorkCode());
                searchRoyaltiesRequest.addProperty("societa", request.getSociety());
            } else {
                throw new IllegalStateException("valorizzare uuid o codiceOpera");
            }

            Client client = Client.create();
            WebResource resource = client.resource(configuration.getProperty("royalties_search.ws.search.endpoint"));
            String searchRoyaltiesResponse = resource.type("application/json").post(String.class, searchRoyaltiesRequest.toString());
            JsonObject response = new JsonObject();
            JsonObject searchRoyaltiesResponseJson = gson.fromJson(searchRoyaltiesResponse, JsonObject.class);

            String socName= request.getSociety() != null ? request.getSociety() : "SIAE";


            if (searchRoyaltiesResponseJson.has("errore")) {
                response.addProperty("status", READY);
                String errore= searchRoyaltiesResponseJson.get("errore").getAsString();
                if(errore.indexOf("royalty scheme not found") >= 0)
                    errore="Schemi di riparto non trovati";
                response.addProperty("statusMessage", errore);
                return Response.status(500).entity(response.toString()).build();
            } else if (searchRoyaltiesResponseJson.has("message")) {
                response.addProperty("status", READY);
                String errore= searchRoyaltiesResponseJson.get("message").getAsString();
                if(errore.indexOf("royalty scheme not found") >= 0)
                    errore="Schemi di riparto non trovati";
                response.addProperty("statusMessage", errore);
                return Response.status(500).entity(response.toString()).build();
            } else {
                JsonArray royalties = new JsonArray();
                
                JsonArray lista = new JsonArray();
                JsonArray listSpec = new JsonArray();
                boolean prendiSiae =true;
                boolean isBlacklist = false;
                
                if(socName.equalsIgnoreCase("SIAE") && request.getCountry().getCode().equalsIgnoreCase("IT")){
                    //prendi blacklist
                    isBlacklist=true;
                    lista = searchRoyaltiesResponseJson.getAsJsonArray("blackList");
                    for (JsonElement elem : lista){
                        if(elem.getAsJsonArray().size() > 0 && elem.getAsJsonArray().get(0).getAsJsonObject().get("formatoQualifica").getAsString().equalsIgnoreCase("SIAE")){
                            listSpec=elem.getAsJsonArray();
                            prendiSiae=true;
                            
                            break;
                        }
                    }
                    
                }else if (socName.equalsIgnoreCase("UCMR-ADA") && request.getCountry().getCode().equalsIgnoreCase("RO")) {
                    //prendi blacklist
                    isBlacklist=true;
                    lista = searchRoyaltiesResponseJson.getAsJsonArray("blackList");//.getAsJsonArray();
                    for (JsonElement elem : lista){
                        if(elem.getAsJsonArray().size() > 0 && elem.getAsJsonArray().get(0).getAsJsonObject().get("formatoQualifica").getAsString().equalsIgnoreCase("CISAC")){
                            listSpec=elem.getAsJsonArray();
                            prendiSiae=false;
                            
                            break;
                        }
                    }
                    
                }else{
                    //prendi royality
                    lista = searchRoyaltiesResponseJson.getAsJsonArray("royaltyScheme"); //.getAsJsonArray("royalties").getAsJsonArray();
                    for (JsonElement elem : lista){
                        if(socName.equalsIgnoreCase("SIAE") && elem.getAsJsonObject().get("royalties").getAsJsonArray().get(0).getAsJsonObject().get("formatoQualifica").getAsString().equalsIgnoreCase("SIAE")){
                            listSpec=elem.getAsJsonObject().get("royalties").getAsJsonArray();
                            prendiSiae=true;
                            isBlacklist=false;
                            break;
                        }else if (socName.equalsIgnoreCase("UCMR-ADA") && elem.getAsJsonObject().get("royalties").getAsJsonArray().get(0).getAsJsonObject().get("formatoQualifica").getAsString().equalsIgnoreCase("CISAC")){
                            listSpec=elem.getAsJsonObject().get("royalties").getAsJsonArray();
                            prendiSiae=false;
                            isBlacklist=false;
                            break;
                            
                        }
                    }
                }


                 if (isBlacklist) {
                    //prendo i valori di blacklist
                     for (JsonElement elem : listSpec) {
                         if(elem.getAsJsonObject().get("sospesoPerEditore").getAsString().equals("true"))
                             elem.getAsJsonObject().addProperty("doNotClaim",true);
                         else
                           elem.getAsJsonObject().addProperty("doNotClaim",false);
                           royalties.add(elem);
                    }  
                    
                } else {
                    //prendo i valori in royalityScheme
                    for (JsonElement elem : listSpec) {
                        if(!socName.equalsIgnoreCase(elem.getAsJsonObject().get("socname").getAsString()) || elem.getAsJsonObject().get("sospesoPerEditore").getAsString().equals("true"))
                            elem.getAsJsonObject().addProperty("doNotClaim",true);
//                        else
//                            elem.getAsJsonObject().addProperty("doNotClaim",true);
                        royalties.add(elem);
                        
                        
                    }
                }
                
                //order
                ArrayList<JsonObject> array = new ArrayList<JsonObject>();
                for (int i = 0; i < listSpec.size(); i++) {
                        array.add(listSpec.get(i).getAsJsonObject());
                }

                // Sort the Java Array List
                Collections.sort(array, new Comparator<JsonObject>() {

                    @Override
                    public int compare(JsonObject lhs, JsonObject rhs) {
                        // TODO Auto-generated method stub

                        try {
                            String lhsQ=lhs.get("tipoQualifica") != null && lhs.get("tipoQualifica").getAsString()!=null? lhs.get("tipoQualifica").getAsString() : "";
                            String rhsQ=rhs.get("tipoQualifica") != null && rhs.get("tipoQualitifa").getAsString()!=null? rhs.get("tipoQualifica").getAsString() : "";


                            int i = lhsQ.compareTo(rhsQ);
                            if(i ==0){
                                String lhsI=lhs.get("codiceIpi") != null && lhs.get("codiceIpi").getAsString()!=null? lhs.get("codiceIpi").getAsString() : "";
                                String rhsI=rhs.get("codiceIpi") != null && rhs.get("codiceIpi").getAsString()!=null? rhs.get("codiceIpi").getAsString() : "";

                                int i1 = lhsI.compareTo(rhsI);
                                if(i1==0) {
                                    String lhsT=lhs.get("tipoQuota") != null && lhs.get("tipoQuota").getAsString()!=null? lhs.get("tipoQuota").getAsString() : "";
                                    String rhsT=rhs.get("tipoQuota") != null && rhs.get("tipoQuota").getAsString()!=null? rhs.get("tipoQuota").getAsString() : "";
                                    return lhsT.compareTo(rhsT);
                                }
                                return i1;
                            }
                            return i;
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                            return 0;
                        }
                    }
                });


                // convert Java Array List to JSON Array and then to String representation.
                JsonArray jsonArray = new JsonArray();
                for (JsonObject jsonObject : array) {
                    jsonArray.add(jsonObject);
                }
                

                response.addProperty("status", "READY");
                response.add("royalties", jsonArray);
            }

            return Response.ok(response.toString()).build();
        } catch (Exception e) {
            logger.error("search", e);
            return Response.status(500).build();
        }
    }

}
