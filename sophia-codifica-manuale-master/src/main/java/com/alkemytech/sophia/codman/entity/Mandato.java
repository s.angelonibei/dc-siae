package com.alkemytech.sophia.codman.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="MANDATO")
@NamedQueries({
	@NamedQuery(name="Mandato.GetAll", query="SELECT m FROM Mandato m ORDER BY m.dataInizioMandato DESC")
})
public class Mandato implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	protected Integer id;
	
	@OneToOne
	@JoinColumn(name = "ID_MANDATARIA", referencedColumnName = "ID")
	private SocietaTutela mandataria;
	
	@OneToOne
	@JoinColumn(name = "ID_MANDANTE", referencedColumnName = "ID")
	private SocietaTutela mandante;
		
	@Column(name = "DATA_INIZIO_MANDATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataInizioMandato;
	
	@Column(name = "DATA_FINE_MANDATO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataFineMandato;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public SocietaTutela getMandataria() {
		return mandataria;
	}

	public void setMandataria(SocietaTutela mandataria) {
		this.mandataria = mandataria;
	}

	public SocietaTutela getMandante() {
		return mandante;
	}

	public void setMandante(SocietaTutela mandante) {
		this.mandante = mandante;
	}

	public Date getDataInizioMandato() {
		return dataInizioMandato;
	}

	public void setDataInizioMandato(Date dataInizioMandato) {
		this.dataInizioMandato = dataInizioMandato;
	}

	public Date getDataFineMandato() {
		return dataFineMandato;
	}

	public void setDataFineMandato(Date dataFineMandato) {
		this.dataFineMandato = dataFineMandato;
	}	

}
