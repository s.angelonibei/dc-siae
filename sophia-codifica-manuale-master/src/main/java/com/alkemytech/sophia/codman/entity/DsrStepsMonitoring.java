package com.alkemytech.sophia.codman.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.GsonBuilder;

@XmlRootElement
@Entity(name="DsrStepsMonitoring")
@Table(name="DSR_STEPS_MONITORING")
public class DsrStepsMonitoring {
	
	@Id
	@Column(name="IDDSR", nullable=false)
	private String idDsr;
	
	@Column(name="LAST_UPDATE", nullable=false)
	private Date lastUpdate;
	
	@Column(name="EXTRACT_QUEUED", nullable=true)
	private Date extractQueued;

	@Column(name="EXTRACT_STARTED", nullable=true)
	private Date extractStarted;
	
	@Column(name="EXTRACT_FINISHED", nullable=true)
	private Date extractFinished;

	@Column(name="EXTRACT_STATUS", nullable=true)
	private String extractStatus;

	@Column(name="CLEAN_QUEUED", nullable=true)
	private Date cleanQueued;

	@Column(name="CLEAN_STARTED", nullable=true)
	private Date cleanStarted;
	
	@Column(name="CLEAN_FINISHED", nullable=true)
	private Date cleanFinished;

	@Column(name="CLEAN_STATUS", nullable=true)
	private String cleanStatus;

	@Column(name="PRICE_QUEUED", nullable=true)
	private Date priceQueued;

	@Column(name="PRICE_STARTED", nullable=true)
	private Date priceStarted;
	
	@Column(name="PRICE_FINISHED", nullable=true)
	private Date priceFinished;

	@Column(name="PRICE_STATUS", nullable=true)
	private String priceStatus;
	
	@Column(name="HYPERCUBE_QUEUED", nullable=true)
	private Date hypercubeQueued;

	@Column(name="HYPERCUBE_STARTED", nullable=true)
	private Date hypercubeStarted;
	
	@Column(name="HYPERCUBE_FINISHED", nullable=true)
	private Date hypercubeFinished;

	@Column(name="HYPERCUBE_STATUS", nullable=true)
	private String hypercubeStatus;

	@Column(name="IDENTIFY_QUEUED", nullable=true)
	private Date identifyQueued;

	@Column(name="IDENTIFY_STARTED", nullable=true)
	private Date identifyStarted;
	
	@Column(name="IDENTIFY_FINISHED", nullable=true)
	private Date identifyFinished;

	@Column(name="IDENTIFY_STATUS", nullable=true)
	private String identifyStatus;
	
	@Column(name="UNILOAD_QUEUED", nullable=true)
	private Date uniloadQueued;

	@Column(name="UNILOAD_STARTED", nullable=true)
	private Date uniloadStarted;
	
	@Column(name="UNILOAD_FINISHED", nullable=true)
	private Date uniloadFinished;

	@Column(name="UNILOAD_STATUS", nullable=true)
	private String uniloadStatus;
	
	@Column(name="CLAIM_QUEUED", nullable=true)
	private Date claimQueued;

	@Column(name="CLAIM_STARTED", nullable=true)
	private Date claimStarted;
	
	@Column(name="CLAIM_FINISHED", nullable=true)
	private Date claimFinished;

	@Column(name="CLAIM_STATUS", nullable=true)
	private String claimStatus;

	@Column(name="PRELEARN_QUEUED", nullable=true)
	private Date prelearnQueued;

	@Column(name="PRELEARN_STARTED", nullable=true)
	private Date prelearnStarted;
	
	@Column(name="PRELEARN_FINISHED", nullable=true)
	private Date prelearnFinished;

	@Column(name="PRELEARN_STATUS", nullable=true)
	private String prelearnStatus;
	
	@Column(name="LEARN_QUEUED", nullable=true)
	private Date learnQueued;

	@Column(name="LEARN_STARTED", nullable=true)
	private Date learnStarted;
	
	@Column(name="LEARN_FINISHED", nullable=true)
	private Date learnFinished;

	@Column(name="LEARN_STATUS", nullable=true)
	private String learnStatus;
	
	public String getIdDsr() {
		return idDsr;
	}

	public void setIdDsr(String idDsr) {
		this.idDsr = idDsr;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public Date getExtractQueued() {
		return extractQueued;
	}

	public void setExtractQueued(Date extractQueued) {
		this.extractQueued = extractQueued;
	}

	public Date getExtractStarted() {
		return extractStarted;
	}

	public void setExtractStarted(Date extractStarted) {
		this.extractStarted = extractStarted;
	}

	public Date getExtractFinished() {
		return extractFinished;
	}

	public void setExtractFinished(Date extractFinished) {
		this.extractFinished = extractFinished;
	}

	public String getExtractStatus() {
		return extractStatus;
	}

	public void setExtractStatus(String extractStatus) {
		this.extractStatus = extractStatus;
	}

	public Date getCleanQueued() {
		return cleanQueued;
	}

	public void setCleanQueued(Date cleanQueued) {
		this.cleanQueued = cleanQueued;
	}

	public Date getCleanStarted() {
		return cleanStarted;
	}

	public void setCleanStarted(Date cleanStarted) {
		this.cleanStarted = cleanStarted;
	}

	public Date getCleanFinished() {
		return cleanFinished;
	}

	public void setCleanFinished(Date cleanFinished) {
		this.cleanFinished = cleanFinished;
	}

	public String getCleanStatus() {
		return cleanStatus;
	}

	public void setCleanStatus(String cleanStatus) {
		this.cleanStatus = cleanStatus;
	}

	public Date getPriceQueued() {
		return priceQueued;
	}

	public void setPriceQueued(Date priceQueued) {
		this.priceQueued = priceQueued;
	}

	public Date getPriceStarted() {
		return priceStarted;
	}

	public void setPriceStarted(Date priceStarted) {
		this.priceStarted = priceStarted;
	}

	public Date getPriceFinished() {
		return priceFinished;
	}

	public void setPriceFinished(Date priceFinished) {
		this.priceFinished = priceFinished;
	}

	public String getPriceStatus() {
		return priceStatus;
	}

	public void setPriceStatus(String priceStatus) {
		this.priceStatus = priceStatus;
	}

	public Date getHypercubeQueued() {
		return hypercubeQueued;
	}

	public void setHypercubeQueued(Date hypercubeQueued) {
		this.hypercubeQueued = hypercubeQueued;
	}

	public Date getHypercubeStarted() {
		return hypercubeStarted;
	}

	public void setHypercubeStarted(Date hypercubeStarted) {
		this.hypercubeStarted = hypercubeStarted;
	}

	public Date getHypercubeFinished() {
		return hypercubeFinished;
	}

	public void setHypercubeFinished(Date hypercubeFinished) {
		this.hypercubeFinished = hypercubeFinished;
	}

	public String getHypercubeStatus() {
		return hypercubeStatus;
	}

	public void setHypercubeStatus(String hypercubeStatus) {
		this.hypercubeStatus = hypercubeStatus;
	}

	public Date getIdentifyQueued() {
		return identifyQueued;
	}

	public void setIdentifyQueued(Date identifyQueued) {
		this.identifyQueued = identifyQueued;
	}

	public Date getIdentifyStarted() {
		return identifyStarted;
	}

	public void setIdentifyStarted(Date identifyStarted) {
		this.identifyStarted = identifyStarted;
	}

	public Date getIdentifyFinished() {
		return identifyFinished;
	}

	public void setIdentifyFinished(Date identifyFinished) {
		this.identifyFinished = identifyFinished;
	}

	public String getIdentifyStatus() {
		return identifyStatus;
	}

	public void setIdentifyStatus(String identifyStatus) {
		this.identifyStatus = identifyStatus;
	}

	public Date getUniloadQueued() {
		return uniloadQueued;
	}

	public void setUniloadQueued(Date uniloadQueued) {
		this.uniloadQueued = uniloadQueued;
	}

	public Date getUniloadStarted() {
		return uniloadStarted;
	}

	public void setUniloadStarted(Date uniloadStarted) {
		this.uniloadStarted = uniloadStarted;
	}

	public Date getUniloadFinished() {
		return uniloadFinished;
	}

	public void setUniloadFinished(Date uniloadFinished) {
		this.uniloadFinished = uniloadFinished;
	}

	public String getUniloadStatus() {
		return uniloadStatus;
	}

	public void setUniloadStatus(String uniloadStatus) {
		this.uniloadStatus = uniloadStatus;
	}

	public Date getClaimQueued() {
		return claimQueued;
	}

	public void setClaimQueued(Date claimQueued) {
		this.claimQueued = claimQueued;
	}

	public Date getClaimStarted() {
		return claimStarted;
	}

	public void setClaimStarted(Date claimStarted) {
		this.claimStarted = claimStarted;
	}

	public Date getClaimFinished() {
		return claimFinished;
	}

	public void setClaimFinished(Date claimFinished) {
		this.claimFinished = claimFinished;
	}

	public String getClaimStatus() {
		return claimStatus;
	}

	public void setClaimStatus(String claimStatus) {
		this.claimStatus = claimStatus;
	}

	public Date getPrelearnQueued() {
		return prelearnQueued;
	}

	public void setPrelearnQueued(Date prelearnQueued) {
		this.prelearnQueued = prelearnQueued;
	}

	public Date getPrelearnStarted() {
		return prelearnStarted;
	}

	public void setPrelearnStarted(Date prelearnStarted) {
		this.prelearnStarted = prelearnStarted;
	}

	public Date getPrelearnFinished() {
		return prelearnFinished;
	}

	public void setPrelearnFinished(Date prelearnFinished) {
		this.prelearnFinished = prelearnFinished;
	}

	public String getPrelearnStatus() {
		return prelearnStatus;
	}

	public void setPrelearnStatus(String prelearnStatus) {
		this.prelearnStatus = prelearnStatus;
	}

	public Date getLearnQueued() {
		return learnQueued;
	}

	public void setLearnQueued(Date learnQueued) {
		this.learnQueued = learnQueued;
	}

	public Date getLearnStarted() {
		return learnStarted;
	}

	public void setLearnStarted(Date learnStarted) {
		this.learnStarted = learnStarted;
	}

	public Date getLearnFinished() {
		return learnFinished;
	}

	public void setLearnFinished(Date learnFinished) {
		this.learnFinished = learnFinished;
	}

	public String getLearnStatus() {
		return learnStatus;
	}

	public void setLearnStatus(String learnStatus) {
		this.learnStatus = learnStatus;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}
}
