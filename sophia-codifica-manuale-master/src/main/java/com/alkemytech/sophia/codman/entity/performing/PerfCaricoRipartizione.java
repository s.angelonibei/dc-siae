package com.alkemytech.sophia.codman.entity.performing;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;


/**
 * The persistent class for the PERF_CARICO_RIPARTIZIONE database table.
 * 
 */
@Entity
@Table(name="PERF_CARICO_RIPARTIZIONE")
@NamedQuery(name="PerfCaricoRipartizione.findAll", query="SELECT p FROM PerfCaricoRipartizione p")
public class PerfCaricoRipartizione implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID_PERF_CARICO_RIPARTIZIONE", unique=true, nullable=false)
	private String idPerfCaricoRipartizione;

	@Column(name="FLAG_IN_RIPARTIZIONE", nullable=false)
	private byte flagInRipartizione;

	@Column(name="ID_PERIODO_RIPARTIZIONE", nullable=false)
	private BigInteger idPeriodoRipartizione;

	@Column(name="VALORE_BOLLETTINI_IRREGOLARI", nullable=false, precision=10, scale=8)
	private BigDecimal valoreBollettiniIrregolari;

	@Column(name="VALORE_CARICO_RIPARTIZIONE", nullable=false, precision=10, scale=8)
	private BigDecimal valoreCaricoRipartizione;

	@Column(name="VALORE_INCASSO_NETTO", nullable=false, precision=10, scale=8)
	private BigDecimal valoreIncassoNetto;

	@Column(name="VALORE_NON_CODIFICATO", nullable=false, precision=10, scale=8)
	private BigDecimal valoreNonCodificato;

	@Column(name="VALORE_PM_ANNULLATI", nullable=false, precision=10, scale=8)
	private BigDecimal valorePmAnnullati;

	@Column(name="VALORE_PM_MANCANTI", nullable=false, precision=10, scale=8)
	private BigDecimal valorePmMancanti;

	@Column(name="VALORE_RIPARTIBILE", nullable=false, precision=10, scale=8)
	private BigDecimal valoreRipartibile;

	@Column(name="VALORE_SOSPENSI", nullable=false, precision=10, scale=8)
	private BigDecimal valoreSospensi;

	//bi-directional many-to-one association to PerfValorizzazioneRun
	@JsonBackReference
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ID_RUN_VALORIZZAZIONE", nullable=false)
	private PerfValorizzazioneRun perfValorizzazioneRun;


	//bi-directional many-to-one association to PerfVoceIncasso
	@JsonBackReference
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="COD_VOCE_INCASSO", nullable=false)
	private PerfVoceIncasso perfVoceIncasso;

	public PerfCaricoRipartizione() {
	}

	public String getIdPerfCaricoRipartizione() {
		return this.idPerfCaricoRipartizione;
	}

	public void setIdPerfCaricoRipartizione(String idPerfCaricoRipartizione) {
		this.idPerfCaricoRipartizione = idPerfCaricoRipartizione;
	}

	public byte getFlagInRipartizione() {
		return this.flagInRipartizione;
	}

	public void setFlagInRipartizione(byte flagInRipartizione) {
		this.flagInRipartizione = flagInRipartizione;
	}

	public BigInteger getIdPeriodoRipartizione() {
		return this.idPeriodoRipartizione;
	}

	public void setIdPeriodoRipartizione(BigInteger idPeriodoRipartizione) {
		this.idPeriodoRipartizione = idPeriodoRipartizione;
	}

	public BigDecimal getValoreBollettiniIrregolari() {
		return this.valoreBollettiniIrregolari;
	}

	public void setValoreBollettiniIrregolari(BigDecimal valoreBollettiniIrregolari) {
		this.valoreBollettiniIrregolari = valoreBollettiniIrregolari;
	}

	public BigDecimal getValoreCaricoRipartizione() {
		return this.valoreCaricoRipartizione;
	}

	public void setValoreCaricoRipartizione(BigDecimal valoreCaricoRipartizione) {
		this.valoreCaricoRipartizione = valoreCaricoRipartizione;
	}

	public BigDecimal getValoreIncassoNetto() {
		return this.valoreIncassoNetto;
	}

	public void setValoreIncassoNetto(BigDecimal valoreIncassoNetto) {
		this.valoreIncassoNetto = valoreIncassoNetto;
	}

	public BigDecimal getValoreNonCodificato() {
		return this.valoreNonCodificato;
	}

	public void setValoreNonCodificato(BigDecimal valoreNonCodificato) {
		this.valoreNonCodificato = valoreNonCodificato;
	}

	public BigDecimal getValorePmAnnullati() {
		return this.valorePmAnnullati;
	}

	public void setValorePmAnnullati(BigDecimal valorePmAnnullati) {
		this.valorePmAnnullati = valorePmAnnullati;
	}

	public BigDecimal getValorePmMancanti() {
		return this.valorePmMancanti;
	}

	public void setValorePmMancanti(BigDecimal valorePmMancanti) {
		this.valorePmMancanti = valorePmMancanti;
	}

	public BigDecimal getValoreRipartibile() {
		return this.valoreRipartibile;
	}

	public void setValoreRipartibile(BigDecimal valoreRipartibile) {
		this.valoreRipartibile = valoreRipartibile;
	}

	public BigDecimal getValoreSospensi() {
		return this.valoreSospensi;
	}

	public void setValoreSospensi(BigDecimal valoreSospensi) {
		this.valoreSospensi = valoreSospensi;
	}

	public PerfValorizzazioneRun getPerfValorizzazioneRun() {
		return this.perfValorizzazioneRun;
	}

	public void setPerfValorizzazioneRun1(PerfValorizzazioneRun perfValorizzazioneRun1) {
		this.perfValorizzazioneRun = perfValorizzazioneRun1;
	}

	public PerfVoceIncasso getPerfVoceIncasso() {
		return this.perfVoceIncasso;
	}

	public void setPerfVoceIncasso(PerfVoceIncasso perfVoceIncasso) {
		this.perfVoceIncasso = perfVoceIncasso;
	}

}