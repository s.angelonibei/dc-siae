package com.alkemytech.sophia.codman.entity;

import java.util.List;
import java.util.Set;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.codman.persistence.AbstractEntity;
import com.google.gson.GsonBuilder;


@XmlRootElement
@Entity(name="CCIDConfig")
@Table(name="CCID_CONFIG")
@NamedQueries({@NamedQuery(name="CCIDConfig.GetAll", query="SELECT config FROM CCIDConfig config")})
@SuppressWarnings("serial")
 

public class CCIDConfig extends AbstractEntity<String> {

	@Id
	@Column(name="ID_CCID_CONFIG", nullable=false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idCCIDConfig;
	
	@Column(name="IDDSP", nullable=false)
	private String idDsp;
	
	@Column(name="ID_UTILIZATION_TYPE", nullable=false)
	private String idUtilizationType;
	
	@Column(name="ID_COMMERCIAL_OFFERS")
	private String idCommercialOffers;
	
	@Column(name="ID_COUNTRY")
	private String idCountry;
	
	@Column(name="ENCODED")
	private Boolean encoded;
	
	@Column(name="ENCODED_PROVISIONAL")
	private Boolean encodedProvisional;
	
	@Column(name="NOT_ENCODED")
	private Boolean notEncoded;
	
	@Column(name="CCID_VERSION")
	private Integer ccdiVersion;
	
	@Column(name="RANK")
	private Integer rank;
	
	@Column(name="CURRENCY")
	private String currency;

	@Column(name="EXCLUDE_CLAIM_0")
	private Boolean excludeClaimZero;
	
//	@ManyToMany
//	@JoinTable(name = "ANAG_CCID_CONFIG_PARAM", joinColumns = @JoinColumn(name = "ID_CCID_CONFIG",referencedColumnName = "ID_CCID_CONFIG"),
//			inverseJoinColumns = @JoinColumn(name = "ID_CCID_PARAM",referencedColumnName = "ID_CCID_PARAM"))
//	private
//	Set<AnagCCIDParam> anagCCIDConfigParams;

	@OneToMany(mappedBy = "ccidConfig", cascade=CascadeType.PERSIST)
	private List<AnagCCIDConfigParam> anagCCIDConfigParamList;


	@Override
	public String getId() {
		return String.valueOf(getIdCCIDConfig());
	}

	public Integer getIdCCIDConfig() {
		return idCCIDConfig;
	}

	public void setIdCCIDConfig(Integer idCCIDConfig) {
		this.idCCIDConfig = idCCIDConfig;
	}

	public String getIdDsp() {
		return idDsp;
	}

	public void setIdDsp(String idDsp) {
		this.idDsp = idDsp;
	}

	public String getIdUtilizationType() {
		return idUtilizationType;
	}

	public void setIdUtilizationType(String idUtilizationType) {
		this.idUtilizationType = idUtilizationType;
	}

	public String getIdCommercialOffers() {
		return idCommercialOffers;
	}

	public void setIdCommercialOffers(String idCommercialOffers) {
		this.idCommercialOffers = idCommercialOffers;
	}

	public String getIdCountry() {
		return idCountry;
	}

	public void setIdCountry(String idCountry) {
		this.idCountry = idCountry;
	}

	public Boolean getEncoded() {
		return encoded;
	}

	public void setEncoded(Boolean encoded) {
		this.encoded = encoded;
	}

	public Boolean getEncodedProvisional() {
		return encodedProvisional;
	}

	public void setEncodedProvisional(Boolean encodedProvisional) {
		this.encodedProvisional = encodedProvisional;
	}

	public Boolean getNotEncoded() {
		return notEncoded;
	}

	public void setNotEncoded(Boolean notEncoded) {
		this.notEncoded = notEncoded;
	}

	public Integer getCcdiVersion() {
		return ccdiVersion;
	}

	public void setCcdiVersion(Integer ccdiVersion) {
		this.ccdiVersion = ccdiVersion;
	}

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Boolean getExcludeClaimZero() {
		return excludeClaimZero;
	}

	public void setExcludeClaimZero(Boolean excludeClaimZero) {
		this.excludeClaimZero = excludeClaimZero;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}

	public List<AnagCCIDConfigParam> getAnagCCIDConfigParamList() {
		return anagCCIDConfigParamList;
	}

	public void setAnagCCIDConfigParamList(List<AnagCCIDConfigParam> anagCCIDConfigParamList) {
		this.anagCCIDConfigParamList = anagCCIDConfigParamList;
	}

	//	public Set<AnagCCIDParam> getAnagCCIDConfigParams() {
//		return anagCCIDConfigParams;
//	}
//
//	public void setAnagCCIDConfigParams(Set<AnagCCIDParam> anagCCIDConfigParams) {
//		this.anagCCIDConfigParams = anagCCIDConfigParams;
//	}



}