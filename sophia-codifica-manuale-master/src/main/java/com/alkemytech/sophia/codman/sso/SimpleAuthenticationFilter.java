package com.alkemytech.sophia.codman.sso;

import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.persistence.EntityManager;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.alkemytech.sophia.codman.entity.RolePermission;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.alkemytech.sophia.common.tools.StringTools;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

@Singleton
public class SimpleAuthenticationFilter extends DefaultAuthenticationModule implements Filter {
	
	private final Provider<EntityManager> entityManagerProvider;
	private final String ssoAuthLoginUrl;
	
	@Inject
	protected SimpleAuthenticationFilter(@Named("configuration") Properties configuration,
			@McmdbDataSource Provider<EntityManager> entityManagerProvider,
			@Named("sso.auth.login.url") String ssoAuthLoginUrl) {
		super(configuration);
		this.entityManagerProvider = entityManagerProvider;
		this.ssoAuthLoginUrl = ssoAuthLoginUrl;
	}
	
	@Override
	public void init(FilterConfig config) throws ServletException {

	}

	@Override
	public void destroy() {
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		final HttpServletRequest httpRequest = (HttpServletRequest) request;
		final HttpServletResponse httpResponse = (HttpServletResponse) response;

		HttpSession httpSession = httpRequest.getSession(false);
		if (null == httpSession || // session not found
				StringTools.isNullOrEmpty((String) httpSession.getAttribute(SsoSessionAttrs.USERNAME))) { // username not found

			if (isDefaultIP(httpRequest)) {
				
				// create and initialize session
				httpSession = httpRequest.getSession(true);
				httpSession.setAttribute(SsoSessionAttrs.DENOMINAZIONE, getSsoDefaultUser());
				httpSession.setAttribute(SsoSessionAttrs.EMAIL, getSsoDefaultUser() + "@siae.it");
				httpSession.setAttribute(SsoSessionAttrs.PP_TIPO, "");
				httpSession.setAttribute(SsoSessionAttrs.PP_SEPRAG, "");
				httpSession.setAttribute(SsoSessionAttrs.PP_DESCRIZIONE, "");
				httpSession.setAttribute(SsoSessionAttrs.STATO_PASSWORD, "");
				httpSession.setAttribute(SsoSessionAttrs.TOKEN_ID, "");
				httpSession.setAttribute(SsoSessionAttrs.USERNAME, getSsoDefaultUser());
				httpSession.setAttribute(SsoSessionAttrs.GROUPS, getSsoDefaultGroups());
				httpSession.setAttribute(SsoSessionAttrs.LAST_CHECK, Long.valueOf(System.currentTimeMillis()));
				
				// inject user permission(s) into session
				final EntityManager entityManager = entityManagerProvider.get();
				final List<RolePermission> permissions = (List<RolePermission>) entityManager
					.createQuery("select x from RolePermission x where x.role in :role", RolePermission.class)
					.setParameter("role", getSsoDefaultGroups())
					.getResultList();
				for (RolePermission permission : permissions) {
					httpSession.setAttribute(permission.getPermission(), permission.getPermission());
				}
				
			} else {
				
				// interrupt filter chain and redirect request
				httpResponse.sendRedirect(ssoAuthLoginUrl);
				return;
				
			}

		} else { // session found
			
			final Long lastCheckTokenMillis = (Long) httpSession.getAttribute(SsoSessionAttrs.LAST_CHECK);
			if (null == lastCheckTokenMillis ||
					System.currentTimeMillis() - lastCheckTokenMillis > TimeUnit.MINUTES.toMillis(10)) {

				// update session
				httpSession.setAttribute(SsoSessionAttrs.LAST_CHECK, Long.valueOf(System.currentTimeMillis()));
			}

		}
		
		// pass the request along the filter chain
		chain.doFilter(request, response);
		
	}

}
