package com.alkemytech.sophia.codman.dto.performing;

import com.alkemytech.sophia.codman.entity.performing.PerfValorizzazioneRun;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.text.SimpleDateFormat;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ValorizzazioneRunDTO {

	private Long idValorizzazioneRun;
	private String dataInizio;
	private String dataFine;
	private String periodoRipartizione;
	private String infoRun;

	public ValorizzazioneRunDTO() {
		super();
	}

	public ValorizzazioneRunDTO(Long idValorizzazioneRun, String dataInizio, String dataFine,
			String periodoRipartizione, String infoRun) {
		super();
		this.idValorizzazioneRun = idValorizzazioneRun;
		this.dataInizio = dataInizio;
		this.dataFine = dataFine;
		this.periodoRipartizione = periodoRipartizione;
		this.infoRun = infoRun;
	}

	public ValorizzazioneRunDTO(PerfValorizzazioneRun valorizzazioneRun) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		this.idValorizzazioneRun = valorizzazioneRun.getIdValorizzazioneRun();
		this.dataInizio = dateFormat.format(valorizzazioneRun.getDataInizio());
		this.dataFine = dateFormat.format(valorizzazioneRun.getDataFine());
		this.periodoRipartizione = valorizzazioneRun.getPerfPeriodoRipartizione().getCodice();
		this.infoRun = valorizzazioneRun.getInfoRun();
	}

	public Long getIdValorizzazioneRun() {
		return idValorizzazioneRun;
	}

	public void setIdValorizzazioneRun(Long idValorizzazioneRun) {
		this.idValorizzazioneRun = idValorizzazioneRun;
	}

	public String getDataInizio() {
		return dataInizio;
	}

	public void setDataInizio(String dataInizio) {
		this.dataInizio = dataInizio;
	}

	public String getDataFine() {
		return dataFine;
	}

	public void setDataFine(String dataFine) {
		this.dataFine = dataFine;
	}

	public String getPeriodoRipartizione() {
		return periodoRipartizione;
	}

	public void setPeriodoRipartizione(String periodoRipartizione) {
		this.periodoRipartizione = periodoRipartizione;
	}

	public String getInfoRun() {
		return infoRun;
	}

	public void setInfoRun(String infoRun) {
		this.infoRun = infoRun;
	}

}
