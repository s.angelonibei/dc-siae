package com.alkemytech.sophia.codman.entity.performing;

//import it.siae.valorizzatore.utility.Utility;

import javax.persistence.*;
import javax.swing.text.Utilities;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by idilello on 6/7/16.
 */
@Entity
@Table(name = "PERF_PROGRAMMA_MUSICALE")
public class ProgrammaMusicale implements Serializable {
    private Long id;
    @CsvBindByPosition(position = 0)
    @CsvBindByName
    private Long numeroProgrammaMusicale;
    private Long progressivo;
    private Long totaleCedole;
    private Double totaleDurataCedole;
    private String foglio;
    @CsvBindByPosition(position = 3)
    @CsvBindByName
    private Long codiceCampionamento;
    @CsvBindByPosition(position = 2)
    @CsvBindByName
    private Character flagDaCampionare;
    @CsvBindByPosition(position = 5)
    @CsvBindByName
    private String pmRiferimento;
    @CsvBindByPosition(position = 4)
    @CsvBindByName
    private Long risultatoCalcoloResto;
    private Date dataOraUltimaModifica;
    private String utenteUltimaModifica;
    private Long giornateTrattenimento;
    private Long fogli;
    private Character flagPmCorrente;
    private String supportoPm;
    private String tipoPm;
//    private Long copia;
    @CsvBindByPosition(position = 1)
    @CsvBindByName
    private String voceIncasso;
    private String dataPassaggioStato;
    private String stato;
    private Cartella cartella;
    private DirettoreEsecuzione direttoreEsecuzione;
    private List<Manifestazione> eventi;
    private List<Utilizzazione> opere;
    private List<MovimentoContabile> movimenti;
    @CsvBindByPosition(position = 20)
    @CsvBindByName
    private String dataRientroPM;
    private ImportoPM importo;
    private Character flagGruppoPrincipale;
    private String foglioSegueDi;
    private String agenziaDiCarico;
    private String dataAssegnazione;
    private String dataRestituzione;
    private String dataCompilazione;
    private String statoPm;
    private String statoWeb;
    private String dataAnnullamento;
    private String dataAcquisizioneSophia;
    private String dataRipartizione;
    private String dataInvioDg;
    private String dataAcquisizoneImmagine;
    private String dataRicezioneDig;
    private String causaAnnullamento;
    private String dataAssegnazioneIncarico;
    private String dataRegistazioneRilevazione;
    private String durataRilevazione;
    private String protocolloRegistrazione;
    private String protocolloTrasmissione;
    private String seprag;

    @Id
    @SequenceGenerator(name="PROGRAMMA_MUSICALE",sequenceName = "PROGRAMMA_MUSICALE_SEQ",allocationSize=1)
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="PROGRAMMA_MUSICALE")
    @Column(name = "ID_PROGRAMMA_MUSICALE", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "NUMERO_PROGRAMMA_MUSICALE",nullable = false)
    public Long getNumeroProgrammaMusicale() {
        return numeroProgrammaMusicale;
    }

    public void setNumeroProgrammaMusicale(Long numeroProgrammaMusicale) {
        this.numeroProgrammaMusicale = numeroProgrammaMusicale;
    }

    @Column(name = "PROGRESSIVO")
    public Long getProgressivo() {
        return progressivo;
    }

    public void setProgressivo(Long progressivo) {
        this.progressivo = progressivo;
    }

    @Column(name = "TOTALE_CEDOLE")
    public Long getTotaleCedole() {
        return totaleCedole;
    }

    public void setTotaleCedole(Long totaleCedole) {
        this.totaleCedole = totaleCedole;
    }

    @Column(name = "TOTALE_DURATA_CEDOLE")
    public Double getTotaleDurataCedole() {
        return totaleDurataCedole;
    }

    public void setTotaleDurataCedole(Double totaleDurataCedole) {
        this.totaleDurataCedole = totaleDurataCedole;
    }

    @Column(name = "FOGLIO")
    public String getFoglio() {
        return foglio;
    }

    public void setFoglio(String foglio) {
        this.foglio = foglio;
    }

    @Column(name = "CODICE_CAMPIONAMENTO")
    public Long getCodiceCampionamento() {
        return codiceCampionamento;
    }

    public void setCodiceCampionamento(Long codiceCampionamento) {
        this.codiceCampionamento = codiceCampionamento;
    }


    @Column(name = "FLAG_DA_CAMPIONARE")
    public Character getFlagDaCampionare() {
        return flagDaCampionare;
    }

    public void setFlagDaCampionare(Character flagDaCampionare) {
        this.flagDaCampionare = flagDaCampionare;
    }

    @Column(name = "PM_RIFERIMENTO")
    public String getPmRiferimento() {
        return pmRiferimento;
    }

    public void setPmRiferimento(String pmRiferimento) {
        this.pmRiferimento = pmRiferimento;
    }

    @Column(name = "RISULTATO_CALCOLO_RESTO")
    public Long getRisultatoCalcoloResto() {
        return risultatoCalcoloResto;
    }

    public void setRisultatoCalcoloResto(Long risultatoCalcoloResto) {
        this.risultatoCalcoloResto = risultatoCalcoloResto;
    }


    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_ORA_ULTIMA_MODIFICA")
    public Date getDataOraUltimaModifica() {
        return dataOraUltimaModifica;
    }

    public void setDataOraUltimaModifica(Date dataOraUltimaModifica) {
        this.dataOraUltimaModifica = dataOraUltimaModifica;
    }

    @Column(name = "UTENTE_ULTIMA_MODIFICA")
    public String getUtenteUltimaModifica() {
        return utenteUltimaModifica;
    }

    public void setUtenteUltimaModifica(String utenteUltimaModifica) {
        this.utenteUltimaModifica = utenteUltimaModifica;
    }

    @Column(name = "GIORNATE_TRATTENIMENTO")
    public Long getGiornateTrattenimento() {
        return giornateTrattenimento;
    }

    public void setGiornateTrattenimento(Long giornateTrattenimento) {
        this.giornateTrattenimento = giornateTrattenimento;
    }

    @Column(name = "FOGLI")
    public Long getFogli() {
        return fogli;
    }

    public void setFogli(Long fogli) {
        this.fogli = fogli;
    }

    @Column(name = "FLAG_PM_CORRENTE")
    public Character getFlagPmCorrente() {
        return flagPmCorrente;
    }

    public void setFlagPmCorrente(Character flagPmCorrente) {
        this.flagPmCorrente = flagPmCorrente;
    }

    @Column(name = "SUPPORTO_PM")
    public String getSupportoPm() {
        return supportoPm;
    }

    public void setSupportoPm(String supportoPm) {
        this.supportoPm = supportoPm;
    }

//    @Column(name = "COPIA")
//    public Long getCopia() {
//        return copia;
//    }
//
//    public void setCopia(Long copia) {
//        this.copia = copia;
//    }

    @Column(name = "TIPO_PM")
    public String getTipoPm() {
        return tipoPm;
    }

    public void setTipoPm(String tipoPm) {
        this.tipoPm = tipoPm;
    }

    @Column(name = "FOGLIO_SEGUE_DI")
    public String getFoglioSegueDi() {
		return foglioSegueDi;
	}

	public void setFoglioSegueDi(String foglioSegueDi) {
		this.foglioSegueDi = foglioSegueDi;
	}

    @Column(name = "AGENZIA_DI_CARICO")
	public String getAgenziaDiCarico() {
		return agenziaDiCarico;
	}

	public void setAgenziaDiCarico(String agenziaDiCarico) {
		this.agenziaDiCarico = agenziaDiCarico;
	}

    @Column(name = "DATA_ASSEGNAZIONE")
	public String getDataAssegnazione() {
		return dataAssegnazione;
	}

	public void setDataAssegnazione(String dataAssegnazione) {
		this.dataAssegnazione = dataAssegnazione;
	}

    @Column(name = "DATA_RESTITUZIONE")
	public String getDataRestituzione() {
		return dataRestituzione;
	}

	public void setDataRestituzione(String dataRestituzione) {
		this.dataRestituzione = dataRestituzione;
	}

    @Column(name = "DATA_COMPILAZIONE")
	public String getDataCompilazione() {
		return dataCompilazione;
	}

	public void setDataCompilazione(String dataCompilazione) {
		this.dataCompilazione = dataCompilazione;
	}

    @Column(name = "STATO_PM")
	public String getStatoPm() {
		return statoPm;
	}

	public void setStatoPm(String statoPm) {
		this.statoPm = statoPm;
	}

    @Column(name = "stato_web")
    public String getStatoWeb() {
		return statoWeb;
	}

	public void setStatoWeb(String statoWeb) {
		this.statoWeb = statoWeb;
	}

	@Column(name = "DATA_ANNULLAMENTO")
	public String getDataAnnullamento() {
		return dataAnnullamento;
	}

	public void setDataAnnullamento(String dataAnnullamento) {
		this.dataAnnullamento = dataAnnullamento;
	}

    @Column(name = "DATA_ACQUISIZIONE_SOPHIA")
	public String getDataAcquisizioneSophia() {
		return dataAcquisizioneSophia;
	}

	public void setDataAcquisizioneSophia(String dataAcquisizioneSophia) {
		this.dataAcquisizioneSophia = dataAcquisizioneSophia;
	}

    @Column(name = "DATA_RIPARTIZIONE")
	public String getDataRipartizione() {
		return dataRipartizione;
	}

	public void setDataRipartizione(String dataRipartizione) {
		this.dataRipartizione = dataRipartizione;
	}

    @Column(name = "DATA_INVIO_DG")
	public String getDataInvioDg() {
		return dataInvioDg;
	}

	public void setDataInvioDg(String dataInvioDg) {
		this.dataInvioDg = dataInvioDg;
	}

    @Column(name = "DATA_ACQUISIZIONE_IMMAGINE")
	public String getDataAcquisizoneImmagine() {
		return dataAcquisizoneImmagine;
	}

	public void setDataAcquisizoneImmagine(String dataAcquisizoneImmagine) {
		this.dataAcquisizoneImmagine = dataAcquisizoneImmagine;
	}

    @Column(name = "DATA_RECEZIONE_INFO_DIG")
	public String getDataRicezioneDig() {
		return dataRicezioneDig;
	}

	public void setDataRicezioneDig(String dataRicezioneDig) {
		this.dataRicezioneDig = dataRicezioneDig;
	}

    @Column(name = "CAUSA_ANNULLAMENTO")
	public String getCausaAnnullamento() {
		return causaAnnullamento;
	}

	public void setCausaAnnullamento(String causaAnnullamento) {
		this.causaAnnullamento = causaAnnullamento;
	}

    @Column(name = "DATA_ASSEGNAZIONE_INCARICO")
	public String getDataAssegnazioneIncarico() {
		return dataAssegnazioneIncarico;
	}

	public void setDataAssegnazioneIncarico(String dataAssegnazioneIncarico) {
		this.dataAssegnazioneIncarico = dataAssegnazioneIncarico;
	}

    @Column(name = "DATA_REGISTRAZIONE_RILEVAZIONE")
	public String getDataRegistazioneRilevazione() {
		return dataRegistazioneRilevazione;
	}

	public void setDataRegistazioneRilevazione(String dataRegistazioneRilevazione) {
		this.dataRegistazioneRilevazione = dataRegistazioneRilevazione;
	}

    @Column(name = "DURATA_RILEVAZIONE")
	public String getDurataRilevazione() {
		return durataRilevazione;
	}

	public void setDurataRilevazione(String durataRilevazione) {
		this.durataRilevazione = durataRilevazione;
	}

    @Column(name = "PROTOCOLLO_REGISTRAZIONE")
	public String getProtocolloRegistrazione() {
		return protocolloRegistrazione;
	}

	public void setProtocolloRegistrazione(String protocolloRegistrazione) {
		this.protocolloRegistrazione = protocolloRegistrazione;
	}

    @Column(name = "PROTOCOLLO_TRASMISSIONE")
	public String getProtocolloTrasmissione() {
		return protocolloTrasmissione;
	}

	public void setProtocolloTrasmissione(String protocolloTrasmissione) {
		this.protocolloTrasmissione = protocolloTrasmissione;
	}

	@Transient
    public String getVoceIncasso() {
        return voceIncasso;
    }

    public void setVoceIncasso(String voceIncasso) {
        this.voceIncasso = voceIncasso;
    }

    @Transient
    public String getDataPassaggioStato() {
        return dataPassaggioStato;
    }

    public void setDataPassaggioStato(String dataPassaggioStato) {
        this.dataPassaggioStato = dataPassaggioStato;
    }

    @Transient
    public String getStato() {
        return stato;
    }

    public void setStato(String stato) {
        this.stato = stato;
    }


    @Transient
    public DirettoreEsecuzione getDirettoreEsecuzione() {
        return direttoreEsecuzione;
    }

    public void setDirettoreEsecuzione(DirettoreEsecuzione direttoreEsecuzione) {
        this.direttoreEsecuzione = direttoreEsecuzione;
    }

    @Transient
    public List<Manifestazione> getEventi() {
        return eventi;
    }

    public void setEventi(List<Manifestazione> eventi) {
        this.eventi = eventi;
    }

    @Transient
    public List<Utilizzazione> getOpere() {
        return opere;
    }

    public void setOpere(List<Utilizzazione> opere) {
        this.opere = opere;
    }

    @Transient
    public List<MovimentoContabile> getMovimenti() {
        return movimenti;
    }

    public void setMovimenti(List<MovimentoContabile> movimenti) {
        this.movimenti = movimenti;
    }

    @Transient
    public Cartella getCartella() {
        return cartella;
    }

    public void setCartella(Cartella cartella) {
        this.cartella = cartella;
    }

    @Transient
    public ImportoPM getImporto() {
        return importo;
    }
    
	public void setImporto(ImportoPM importo) {
        this.importo = importo;
    }

    @Transient
    public String getSeprag() {
		return seprag;
	}

	public void setSeprag(String seprag) {
		this.seprag = seprag;
	}

    @Transient
    public String getDataRientroPM() {
        return dataRientroPM;
    }

    public void setDataRientroPM(String dataRientroPM) {
        this.dataRientroPM = dataRientroPM;
    }
    
    @Column(name = "FLAG_GRUPPO_PRINCIPALE") 
    public Character getFlagGruppoPrincipale() {
		return flagGruppoPrincipale;
	}

	public void setFlagGruppoPrincipale(Character flagGruppoPrincipale) {
		this.flagGruppoPrincipale = flagGruppoPrincipale;
	}

	public String[] getMappingStrategy() {
        return new String[]{

        		"Numero programma musicale",
        		"Voce incasso",
        		"Tipo supporto",
    			"Tipo documento",
        		"Flag da campionare",
        		"Codice campionamento",
        		"Risultato calcolo resto",
        		"Totale durata cedole",
        		"Totale cedole",
        		"Foglio segue di",
        		"Data Rientro",
        		"Pm riferimento"
        };
    }

}
