package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement 
public class MandatoDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer id;
	
	private SocietaTutelaDTO mandataria;
	
	private SocietaTutelaDTO mandante;
		
	private Date dataInizioMandato;
	
	private Date dataFineMandato;

	public SocietaTutelaDTO getMandataria() {
		return mandataria;
	}

	public void setMandataria(SocietaTutelaDTO mandataria) {
		this.mandataria = mandataria;
	}

	public SocietaTutelaDTO getMandante() {
		return mandante;
	}

	public void setMandante(SocietaTutelaDTO mandante) {
		this.mandante = mandante;
	}

	public Date getDataInizioMandato() {
		return dataInizioMandato;
	}

	public void setDataInizioMandato(Date dataInizioMandato) {
		this.dataInizioMandato = dataInizioMandato;
	}

	public Date getDataFineMandato() {
		return dataFineMandato;
	}

	public void setDataFineMandato(Date dataFineMandato) {
		this.dataFineMandato = dataFineMandato;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}