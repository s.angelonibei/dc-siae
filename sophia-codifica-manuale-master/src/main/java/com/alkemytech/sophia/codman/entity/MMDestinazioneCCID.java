package com.alkemytech.sophia.codman.entity;

import com.alkemytech.sophia.codman.persistence.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity(name="MMDestinazioneCCID")
@Table(name="MM_DESTINAZIONE_INVIO_CCID")
@SuppressWarnings("serial")
public class MMDestinazioneCCID extends AbstractEntity<String>{

	@Id
	@Column(name="ID", nullable=false)
	private int id;

	@Column(name="IDDSP", nullable=false)
	private String idDSP;

	@Column(name="UTILIZATION_TYPE", nullable=false)
	private String utilizationType;

	@Column(name="COMMERCIAL_OFFERS", nullable=false)
	private String commercialOffers;
	
	@Column(name="PATH", nullable=false)
	private String path;
	
	@Column(name="RANK", nullable=false)
	private String rank;


	public String getIdDSP() {
		return idDSP;
	}

	public void setIdDSP(String idDSP) {
		this.idDSP = idDSP;
	}

	public String getUtilizationType() {
		return utilizationType;
	}

	public void setUtilizationType(String utilizationType) {
		this.utilizationType = utilizationType;
	}

	public String getCommercialOffers() {
		return commercialOffers;
	}

	public void setCommercialOffers(String commercialOffers) {
		this.commercialOffers = commercialOffers;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	@Override
	public String getId() {
		return String.valueOf(id);
	}

}
