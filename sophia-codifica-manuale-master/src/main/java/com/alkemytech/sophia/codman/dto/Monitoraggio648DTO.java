package com.alkemytech.sophia.codman.dto;

import lombok.Data;

import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement
public class Monitoraggio648DTO{

    private String idDsp;
    private String stato;
    private String society;
    private String errorDescription;
    private Long dataInserimento;
    private Long dataAggiornamento;
    private Float valoreDem;
    private Float valoreDrm;

}
