package com.alkemytech.sophia.codman.entity.performing.security;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import com.alkemytech.sophia.codman.entity.performing.InformazioneKPICodifica;

public class ReportMonitoringKPI implements Serializable{

	private List<InformazioneKPICodifica> records;
    private Set<String> periodo;
    
    
    public ReportMonitoringKPI(List<InformazioneKPICodifica> records, Set<String> periodo) {
		super();
		this.records = records;
		this.periodo = periodo;
	}
	
    
	public List<InformazioneKPICodifica> getRecords() {
		return records;
	}
	public void setRecords(List<InformazioneKPICodifica> records) {
		this.records = records;
	}
	public Set<String> getPeriodo() {
		return periodo;
	}
	public void setPeriodo(Set<String> periodo) {
		this.periodo = periodo;
	}
  
    
}
