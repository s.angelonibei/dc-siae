package com.alkemytech.sophia.codman.rest.performing;

public class TimerSessionResponse {
	private Long sessionTimeResidual;

	public Long getSessionTimeResidual() {
		return sessionTimeResidual;
	}

	public void setSessionTimeResidual(Long sessionTimeResidual) {
		this.sessionTimeResidual = sessionTimeResidual;
	}
	
}
