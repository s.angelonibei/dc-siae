package com.alkemytech.sophia.codman.blacklist;

import java.io.File;
import java.util.List;

public interface BlacklistValidator {

	public List<String> validate(File file);
	
}
