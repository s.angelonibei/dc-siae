package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.codman.entity.ImportiAnticipo;

@Produces("application/json")
@XmlRootElement
public class ImportiAnticipoDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer idImportoAnticipo;
	private String periodFrom;
	private String periodTo;
	private String invoiceNumber;
	private BigDecimal originalAmount;
	private BigDecimal amountUsed;
	private String idDSP;
	
	public ImportiAnticipoDTO(){
		
	}
	
	public ImportiAnticipoDTO( Object[] obj ){
		configure(obj);
	}
	
	public ImportiAnticipoDTO( ImportiAnticipo entity ){
		
		init( entity );
		
	}

	public Integer getIdImportoAnticipo() {
		return idImportoAnticipo;
	}

	public void setIdImportoAnticipo(Integer idImportoAnticipo) {
		this.idImportoAnticipo = idImportoAnticipo;
	}

	public String getPeriodTo() {
		return periodTo;
	}

	public void setPeriodTo(String periodTo) {
		this.periodTo = periodTo;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public BigDecimal getOriginalAmount() {
		return originalAmount;
	}

	public void setOriginalAmount(BigDecimal originalAmount) {
		this.originalAmount = originalAmount;
	}

	public BigDecimal getAmountUsed() {
		return amountUsed;
	}

	public void setAmountUsed(BigDecimal amountUsed) {
		this.amountUsed = amountUsed;
	}

	public String getIdDSP() {
		return idDSP;
	}

	public void setIdDSP(String idDSP) {
		this.idDSP = idDSP;
	}

	public String getPeriodFrom() {
		return periodFrom;
	}

	public void setPeriodFrom(String periodFrom) {
		this.periodFrom = periodFrom;
	}

	public void configure( Object[] obj ){
		
	}
	
	public void init(ImportiAnticipo entity ){
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM-yyyy");
		
		this.idImportoAnticipo = entity.getIdImportoAnticipo();
		this.periodFrom = dateFormat.format( entity.getPeriodoPertinenzaInizio() );
		this.periodTo = dateFormat.format( entity.getPeriodoPertinenzaFine() );
		this.invoiceNumber = entity.getNumeroFattura();
		this.originalAmount = entity.getImportoOriginale();
		this.amountUsed = entity.getImportoUtilizzabile();
		this.idDSP = entity.getIdDsp();
	
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ImportiAnticipoDTO [idImportoAnticipo=");
		builder.append(idImportoAnticipo);
		builder.append(", periodFrom=");
		builder.append(periodFrom);
		builder.append(", periodTo=");
		builder.append(periodTo);
		builder.append(", invoiceNumber=");
		builder.append(invoiceNumber);
		builder.append(", originalAmount=");
		builder.append(originalAmount);
		builder.append(", amountUsed=");
		builder.append(amountUsed);
		builder.append(", idDSP=");
		builder.append(idDSP);
		builder.append("]");
		return builder.toString();
	}
	
}