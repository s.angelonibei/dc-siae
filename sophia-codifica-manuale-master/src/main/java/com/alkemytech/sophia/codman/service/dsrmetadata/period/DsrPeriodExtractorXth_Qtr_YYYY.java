package com.alkemytech.sophia.codman.service.dsrmetadata.period;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DsrPeriodExtractorXth_Qtr_YYYY implements DsrPeriodExtractor{
	
	private String regex = "[1-4]{1}(st|nd|rd|th)_Qtr_[0-9]{4}";
	private String code = "Xth_Qtr_YYYY";
	private String description = "Xth_Qtr_YYYY es 2nd_Qtr_2017" ;
	
	@Override
	public DsrPeriod extractDsrPeriod(String periodString) {
				
		try {
			DsrPeriod dsrPeriod = new DsrPeriod();
			dsrPeriod.setPeriodType(DsrPeriod.TRIMESTRALE);
			
			
			String year = periodString.substring(8,12);
			dsrPeriod.setYear(year);
			String quarter = periodString.substring(0,1);
			dsrPeriod.setPeriod(quarter);
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
			
			String startDate = year;
			String endDate = year;
			
			switch (quarter) {
				case "1":{
					startDate += "0101";
					endDate += "0331";
					break;
				}
				case "2":{
					startDate += "0401";
					endDate += "0630";					
					break;
				}	
				case "3":{
					startDate += "0701";
					endDate += "0930";
					break;
				}	
				case "4":{
					startDate += "1001";
					endDate += "1231";
					break;
				}
				default:{
					return null;
				}
			}
			
			Date periodStartDate = dateFormat.parse(startDate);
			Date periodEndDate = dateFormat.parse(endDate);			
			
			dsrPeriod.setPeriodStartDate(periodStartDate);
			dsrPeriod.setPeriodEndDate(periodEndDate);
			
			return dsrPeriod;
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}

	@Override
	public String getRegex() {
		return regex;
	}

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public String getDescription() {
		return description;
	}

}
