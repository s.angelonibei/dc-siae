package com.alkemytech.sophia.codman.dto.performing;

import java.util.HashMap;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class CarichiRipartizioniRequest {

	private List<CarichiRipartizioneDTO> carichiRipartizione;
	private List<CarichiRipartizioneDTO> carryOver;
	private String user;

	public CarichiRipartizioniRequest() {
		super();
	}

	public CarichiRipartizioniRequest(List<CarichiRipartizioneDTO> carichiRipartizione, List<CarichiRipartizioneDTO>  carryOver,String user) {
		super();
		this.carichiRipartizione = carichiRipartizione;
		this.user = user;
		this.carryOver = carryOver;
	}

	public List<CarichiRipartizioneDTO> getCarichiRipartizione() {
		return carichiRipartizione;
	}

	public void setCarichiRipartizione(List<CarichiRipartizioneDTO>  carichiRipartizione) {
		this.carichiRipartizione = carichiRipartizione;
	}

	public List<CarichiRipartizioneDTO>   getCarryOver() {
		return carryOver;
	}

	public void setCarryOver(List<CarichiRipartizioneDTO>  carryOver) {
		this.carryOver = carryOver;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

}
