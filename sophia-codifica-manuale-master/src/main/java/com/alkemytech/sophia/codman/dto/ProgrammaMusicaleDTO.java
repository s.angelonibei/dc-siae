package com.alkemytech.sophia.codman.dto;

import java.util.Date;
import java.util.List;

import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.codman.entity.performing.Cartella;
import com.alkemytech.sophia.codman.entity.performing.DirettoreEsecuzione;
import com.alkemytech.sophia.codman.entity.performing.ImportoPM;
import com.alkemytech.sophia.codman.entity.performing.Manifestazione;
import com.alkemytech.sophia.codman.entity.performing.MovimentoContabile;
import com.alkemytech.sophia.codman.entity.performing.Utilizzazione;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

@Produces("application/json")
@XmlRootElement
public class ProgrammaMusicaleDTO {

    private Long id;
    private Long numeroProgrammaMusicale;
    private String tipoPm;
    private String supportoPm;
    private String agenziaDiCarico;
    private String dataAssegnazione;
    private String dataRestituzione;
    private String dataCompilazione;
    private String stato;
    private String dataAnnullamento;
    private String dataAcquisizioneSophia;
    private String dataRipartizione;
    private String dataInvioDg;
    private String dataAcquisizioneImmagine;
    private String dataRicezioneInfoDig;
    private String causaAnnullamento;
    private Long progressivo;
    private Long totaleCedole;
    private Double totaleDurataCedole;
    private String foglio;
    private String pmRiferimento;
    private Long risultatoCalcoloResto;
    private Date dataOraUltimaModifica;
    private String utenteUltimaModifica;
    private Long giornateTrattenimento;
    private Long fogli;
    private Character flagPmCorrente;
    private String dataAssegnazioneIncarico;
    private String dataRegistrazioneRilevazione;
    private String durataRilevazione;
    private String protocolloRegistrazione;
    private String protocolloTrasmissione;
    private Long codiceCampionamento;
    private Character flagDaCampionare;
    private Character flagGruppoPrincipale;
    private String foglioSegueDi;
    
	public ProgrammaMusicaleDTO(Long id, Long numeroProgrammaMusicale, String tipoPm, String supportoPm,
			String agenziaDiCarico, String dataAssegnazione, String dataRestituzione, String dataCompilazione,
			String stato, String dataAnnullamento, String dataAcquisizioneSophia, String dataRipartizione,
			String dataInvioDg, String dataAcquisizioneImmagine, String dataRicezioneInfoDig, String causaAnnullamento,
			Long progressivo, Long totaleCedole, Double totaleDurataCedole, String foglio, String pmRiferimento,
			Long risultatoCalcoloResto, Date dataOraUltimaModifica, String utenteUltimaModifica,
			Long giornateTrattenimento, Long fogli, Character flagPmCorrente, String dataAssegnazioneIncarico,
			String dataRegistrazioneRilevazione, String durataRilevazione, String protocolloRegistrazione,
			String protocolloTrasmissione, Long codiceCampionamento, Character flagDaCampionare,
			Character flagGruppoPrincipale, String foglioSegueDi) {
		super();
		this.id = id;
		this.numeroProgrammaMusicale = numeroProgrammaMusicale;
		this.tipoPm = tipoPm;
		this.supportoPm = supportoPm;
		this.agenziaDiCarico = agenziaDiCarico;
		this.dataAssegnazione = dataAssegnazione;
		this.dataRestituzione = dataRestituzione;
		this.dataCompilazione = dataCompilazione;
		this.stato = stato;
		this.dataAnnullamento = dataAnnullamento;
		this.dataAcquisizioneSophia = dataAcquisizioneSophia;
		this.dataRipartizione = dataRipartizione;
		this.dataInvioDg = dataInvioDg;
		this.dataAcquisizioneImmagine = dataAcquisizioneImmagine;
		this.dataRicezioneInfoDig = dataRicezioneInfoDig;
		this.causaAnnullamento = causaAnnullamento;
		this.progressivo = progressivo;
		this.totaleCedole = totaleCedole;
		this.totaleDurataCedole = totaleDurataCedole;
		this.foglio = foglio;
		this.pmRiferimento = pmRiferimento;
		this.risultatoCalcoloResto = risultatoCalcoloResto;
		this.dataOraUltimaModifica = dataOraUltimaModifica;
		this.utenteUltimaModifica = utenteUltimaModifica;
		this.giornateTrattenimento = giornateTrattenimento;
		this.fogli = fogli;
		this.flagPmCorrente = flagPmCorrente;
		this.dataAssegnazioneIncarico = dataAssegnazioneIncarico;
		this.dataRegistrazioneRilevazione = dataRegistrazioneRilevazione;
		this.durataRilevazione = durataRilevazione;
		this.protocolloRegistrazione = protocolloRegistrazione;
		this.protocolloTrasmissione = protocolloTrasmissione;
		this.codiceCampionamento = codiceCampionamento;
		this.flagDaCampionare = flagDaCampionare;
		this.flagGruppoPrincipale = flagGruppoPrincipale;
		this.foglioSegueDi = foglioSegueDi;
	}

	public ProgrammaMusicaleDTO() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getNumeroProgrammaMusicale() {
		return numeroProgrammaMusicale;
	}

	public void setNumeroProgrammaMusicale(Long numeroProgrammaMusicale) {
		this.numeroProgrammaMusicale = numeroProgrammaMusicale;
	}

	public String getTipoPm() {
		return tipoPm;
	}

	public void setTipoPm(String tipoPm) {
		this.tipoPm = tipoPm;
	}

	public String getSupportoPm() {
		return supportoPm;
	}

	public void setSupportoPm(String supportoPm) {
		this.supportoPm = supportoPm;
	}

	public String getAgenziaDiCarico() {
		return agenziaDiCarico;
	}

	public void setAgenziaDiCarico(String agenziaDiCarico) {
		this.agenziaDiCarico = agenziaDiCarico;
	}

	public String getDataAssegnazione() {
		return dataAssegnazione;
	}

	public void setDataAssegnazione(String dataAssegnazione) {
		this.dataAssegnazione = dataAssegnazione;
	}

	public String getDataRestituzione() {
		return dataRestituzione;
	}

	public void setDataRestituzione(String dataRestituzione) {
		this.dataRestituzione = dataRestituzione;
	}

	public String getDataCompilazione() {
		return dataCompilazione;
	}

	public void setDataCompilazione(String dataCompilazione) {
		this.dataCompilazione = dataCompilazione;
	}

	public String getStato() {
		return stato;
	}

	public void setStato(String stato) {
		this.stato = stato;
	}

	public String getDataAnnullamento() {
		return dataAnnullamento;
	}

	public void setDataAnnullamento(String dataAnnullamento) {
		this.dataAnnullamento = dataAnnullamento;
	}

	public String getDataAcquisizioneSophia() {
		return dataAcquisizioneSophia;
	}

	public void setDataAcquisizioneSophia(String dataAcquisizioneSophia) {
		this.dataAcquisizioneSophia = dataAcquisizioneSophia;
	}

	public String getDataRipartizione() {
		return dataRipartizione;
	}

	public void setDataRipartizione(String dataRipartizione) {
		this.dataRipartizione = dataRipartizione;
	}

	public String getDataInvioDg() {
		return dataInvioDg;
	}

	public void setDataInvioDg(String dataInvioDg) {
		this.dataInvioDg = dataInvioDg;
	}

	public String getDataAcquisizioneImmagine() {
		return dataAcquisizioneImmagine;
	}

	public void setDataAcquisizioneImmagine(String dataAcquisizioneImmagine) {
		this.dataAcquisizioneImmagine = dataAcquisizioneImmagine;
	}

	public String getDataRicezioneInfoDig() {
		return dataRicezioneInfoDig;
	}

	public void setDataRicezioneInfoDig(String dataRicezioneInfoDig) {
		this.dataRicezioneInfoDig = dataRicezioneInfoDig;
	}

	public String getCausaAnnullamento() {
		return causaAnnullamento;
	}

	public void setCausaAnnullamento(String causaAnnullamento) {
		this.causaAnnullamento = causaAnnullamento;
	}

	public Long getProgressivo() {
		return progressivo;
	}

	public void setProgressivo(Long progressivo) {
		this.progressivo = progressivo;
	}

	public Long getTotaleCedole() {
		return totaleCedole;
	}

	public void setTotaleCedole(Long totaleCedole) {
		this.totaleCedole = totaleCedole;
	}

	public Double getTotaleDurataCedole() {
		return totaleDurataCedole;
	}

	public void setTotaleDurataCedole(Double totaleDurataCedole) {
		this.totaleDurataCedole = totaleDurataCedole;
	}

	public String getFoglio() {
		return foglio;
	}

	public void setFoglio(String foglio) {
		this.foglio = foglio;
	}

	public String getPmRiferimento() {
		return pmRiferimento;
	}

	public void setPmRiferimento(String pmRiferimento) {
		this.pmRiferimento = pmRiferimento;
	}

	public Long getRisultatoCalcoloResto() {
		return risultatoCalcoloResto;
	}

	public void setRisultatoCalcoloResto(Long risultatoCalcoloResto) {
		this.risultatoCalcoloResto = risultatoCalcoloResto;
	}

	public Date getDataOraUltimaModifica() {
		return dataOraUltimaModifica;
	}

	public void setDataOraUltimaModifica(Date dataOraUltimaModifica) {
		this.dataOraUltimaModifica = dataOraUltimaModifica;
	}

	public String getUtenteUltimaModifica() {
		return utenteUltimaModifica;
	}

	public void setUtenteUltimaModifica(String utenteUltimaModifica) {
		this.utenteUltimaModifica = utenteUltimaModifica;
	}

	public Long getGiornateTrattenimento() {
		return giornateTrattenimento;
	}

	public void setGiornateTrattenimento(Long giornateTrattenimento) {
		this.giornateTrattenimento = giornateTrattenimento;
	}

	public Long getFogli() {
		return fogli;
	}

	public void setFogli(Long fogli) {
		this.fogli = fogli;
	}

	public Character getFlagPmCorrente() {
		return flagPmCorrente;
	}

	public void setFlagPmCorrente(Character flagPmCorrente) {
		this.flagPmCorrente = flagPmCorrente;
	}

	public String getDataAssegnazioneIncarico() {
		return dataAssegnazioneIncarico;
	}

	public void setDataAssegnazioneIncarico(String dataAssegnazioneIncarico) {
		this.dataAssegnazioneIncarico = dataAssegnazioneIncarico;
	}

	public String getDataRegistrazioneRilevazione() {
		return dataRegistrazioneRilevazione;
	}

	public void setDataRegistrazioneRilevazione(String dataRegistrazioneRilevazione) {
		this.dataRegistrazioneRilevazione = dataRegistrazioneRilevazione;
	}

	public String getDurataRilevazione() {
		return durataRilevazione;
	}

	public void setDurataRilevazione(String durataRilevazione) {
		this.durataRilevazione = durataRilevazione;
	}

	public String getProtocolloRegistrazione() {
		return protocolloRegistrazione;
	}

	public void setProtocolloRegistrazione(String protocolloRegistrazione) {
		this.protocolloRegistrazione = protocolloRegistrazione;
	}

	public String getProtocolloTrasmissione() {
		return protocolloTrasmissione;
	}

	public void setProtocolloTrasmissione(String protocolloTrasmissione) {
		this.protocolloTrasmissione = protocolloTrasmissione;
	}

	public Long getCodiceCampionamento() {
		return codiceCampionamento;
	}

	public void setCodiceCampionamento(Long codiceCampionamento) {
		this.codiceCampionamento = codiceCampionamento;
	}

	public Character getFlagDaCampionare() {
		return flagDaCampionare;
	}

	public void setFlagDaCampionare(Character flagDaCampionare) {
		this.flagDaCampionare = flagDaCampionare;
	}

	public Character getFlagGruppoPrincipale() {
		return flagGruppoPrincipale;
	}

	public void setFlagGruppoPrincipale(Character flagGruppoPrincipale) {
		this.flagGruppoPrincipale = flagGruppoPrincipale;
	}

	public String getFoglioSegueDi() {
		return foglioSegueDi;
	}

	public void setFoglioSegueDi(String foglioSegueDi) {
		this.foglioSegueDi = foglioSegueDi;
	}

	@Override
	public String toString() {
		return "ProgrammaMusicaleDTO [numeroProgrammaMusicale=" + numeroProgrammaMusicale + ", tipoPm="
				+ tipoPm + ", supportoPm=" + supportoPm + ", agenziaDiCarico=" + agenziaDiCarico + ", dataAssegnazione="
				+ dataAssegnazione + ", dataRestituzione=" + dataRestituzione + ", dataCompilazione=" + dataCompilazione
				+ ", stato=" + stato + ", dataAnnullamento=" + dataAnnullamento + ", dataAcquisizioneSophia="
				+ dataAcquisizioneSophia + ", dataRipartizione=" + dataRipartizione + ", dataInvioDg=" + dataInvioDg
				+ ", dataAcquisizioneImmagine=" + dataAcquisizioneImmagine + ", dataRicezioneInfoDig="
				+ dataRicezioneInfoDig + ", causaAnnullamento=" + causaAnnullamento + ", progressivo=" + progressivo
				+ ", totaleCedole=" + totaleCedole + ", totaleDurataCedole=" + totaleDurataCedole + ", foglio=" + foglio
				+ ", pmRiferimento=" + pmRiferimento + ", risultatoCalcoloResto=" + risultatoCalcoloResto
				+ ", dataOraUltimaModifica=" + dataOraUltimaModifica + ", utenteUltimaModifica=" + utenteUltimaModifica
				+ ", giornateTrattenimento=" + giornateTrattenimento + ", fogli=" + fogli + ", flagPmCorrente="
				+ flagPmCorrente + ", dataAssegnazioneIncarico=" + dataAssegnazioneIncarico
				+ ", dataRegistrazioneRilevazione=" + dataRegistrazioneRilevazione + ", durataRilevazione="
				+ durataRilevazione + ", protocolloRegistrazione=" + protocolloRegistrazione
				+ ", protocolloTrasmissione=" + protocolloTrasmissione + ", codiceCampionamento=" + codiceCampionamento
				+ ", flagDaCampionare=" + flagDaCampionare + ", flagGruppoPrincipale=" + flagGruppoPrincipale
				+ ", foglioSegueDi=" + foglioSegueDi + "]";
	}
    
    
    
    
}
