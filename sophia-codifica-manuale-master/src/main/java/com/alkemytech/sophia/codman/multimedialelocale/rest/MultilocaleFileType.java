package com.alkemytech.sophia.codman.multimedialelocale.rest;

public enum MultilocaleFileType {

	CARICATO("CARICATO"),
    CODIFICATO("CODIFICATO"),
    NORMALIZZATO("NORMALIZZATO");

    String text;

	MultilocaleFileType(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }

    public static MultilocaleFileType valueOfDescription(String description) {

    	MultilocaleFileType multilocaleFileType=null;

        for (MultilocaleFileType v : values()) {

            if (v.toString().equals(description)) {
                multilocaleFileType = v;
            }
        }

        return multilocaleFileType;
    }

}
