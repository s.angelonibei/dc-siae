package com.alkemytech.sophia.codman.dto.performing;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.codman.entity.performing.EventiPagati;
import com.alkemytech.sophia.codman.entity.performing.Manifestazione;
import com.alkemytech.sophia.codman.entity.performing.MovimentoContabile;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class FatturaDetailDTO {

	
	private String numeroFattura;
	private String dataFattura;
	private String reversale;
	private String nomeOrganizzatore;
	private String pIvaOrganizzatore;
	private String cfOrganizzatore;
	private String codiceSap;
	private String tipoDocumento;
	private String importoDemTotale;
	private String seprag;
	private String importoDemVoceIncasso;
	

	private Manifestazione manifestazione;
	private List<MovimentoContabile> movimentiAssociati;
	private List<EventiPagati> eventiAssociati;
	private List<RiconciliazioneImportiDTO> riconciliazioneFattura;

	public FatturaDetailDTO() {
		super();
	}
	
	public FatturaDetailDTO(Manifestazione manifestazione) {
		super();
		this.manifestazione = manifestazione;
	}
	
	public Manifestazione getManifestazione() {
		return manifestazione;
	}
	public void setManifestazione(Manifestazione manifestazione) {
		this.manifestazione = manifestazione;
	}
	public List<MovimentoContabile> getMovimentiAssociati() {
		return movimentiAssociati;
	}
	public void setMovimentiAssociati(List<MovimentoContabile> movimentiAssociati) {
		this.movimentiAssociati = movimentiAssociati;
	}
	public List<EventiPagati> getEventiAssociati() {
		return eventiAssociati;
	}
	public void setEventiAssociati(List<EventiPagati> eventiAssociati) {
		this.eventiAssociati = eventiAssociati;
	}

	public String getNumeroFattura() {
		return numeroFattura;
	}

	public void setNumeroFattura(String numeroFattura) {
		this.numeroFattura = numeroFattura;
	}

	public String getDataFattura() {
		return dataFattura;
	}

	public void setDataFattura(String dataFattura) {
		this.dataFattura = dataFattura;
	}

	public String getReversale() {
		return reversale;
	}

	public void setReversale(String reversale) {
		this.reversale = reversale;
	}

	public String getNomeOrganizzatore() {
		return nomeOrganizzatore;
	}

	public void setNomeOrganizzatore(String nomeOrganizzatore) {
		this.nomeOrganizzatore = nomeOrganizzatore;
	}

	public String getpIvaOrganizzatore() {
		return pIvaOrganizzatore;
	}

	public void setpIvaOrganizzatore(String pIvaOrganizzatore) {
		this.pIvaOrganizzatore = pIvaOrganizzatore;
	}

	public String getCfOrganizzatore() {
		return cfOrganizzatore;
	}

	public void setCfOrganizzatore(String cfOrganizzatore) {
		this.cfOrganizzatore = cfOrganizzatore;
	}

	public String getCodiceSap() {
		return codiceSap;
	}

	public void setCodiceSap(String codiceSap) {
		this.codiceSap = codiceSap;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getImportoDemTotale() {
		return importoDemTotale;
	}

	public void setImportoDemTotale(String importoDemTotale) {
		this.importoDemTotale = importoDemTotale;
	}

	public String getSeprag() {
		return seprag;
	}

	public void setSeprag(String seprag) {
		this.seprag = seprag;
	}

	public List<RiconciliazioneImportiDTO> getRiconciliazioneFattura() {
		return riconciliazioneFattura;
	}

	public void setRiconciliazioneFattura(List<RiconciliazioneImportiDTO> riconciliazioneFattura) {
		this.riconciliazioneFattura = riconciliazioneFattura;
	}

	public String getImportoDemVoceIncasso() {
		return importoDemVoceIncasso;
	}

	public void setImportoDemVoceIncasso(String importoDemVoceIncasso) {
		this.importoDemVoceIncasso = importoDemVoceIncasso;
	}
	
	
}
