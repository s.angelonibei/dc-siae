package com.alkemytech.sophia.codman.rest.performing.serviceimpl;

import com.alkemytech.sophia.performing.model.PerfMusicProvider;
import com.alkemytech.sophia.performing.model.PerfPalinsesto;
import com.alkemytech.sophia.codman.rest.performing.service.FileArmonizzatoService;
import com.alkemytech.sophia.performing.dto.FileArmonizzatoDTO;
import com.google.inject.Provider;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Service("FileArmonizzatoService")
public class FileArmonizzatoServiceImpl implements FileArmonizzatoService {
    private Provider<EntityManager> provider;

    public FileArmonizzatoServiceImpl(Provider<EntityManager> provider) {
        this.provider = provider;
    }

    @Override
    public List<FileArmonizzatoDTO> getFileArmonizzato(PerfMusicProvider musicProvider, List<PerfPalinsesto> palinsesti, Integer anno, List<Integer> mesi) {
        EntityManager entityManager = provider.get();

        List<BigInteger> idPalinseti = new ArrayList<>();
        for (PerfPalinsesto palinsesto: palinsesti) {
            idPalinseti.add(palinsesto.getIdPalinsesto());
        }

        String w = ""
                + "SELECT "
                + "	palinsesto, "
                + "	titoloTrasmissione, "
                + "	dataInizioTrasmissione, "
                + "	orarioInizioTrasmissione, "
                + "	durataTrasmissione, "
                + "	titoloBrano, "
                + "	compositoreAutore, "
                + "	esecutorePerformer, "
                + "	album, "
                + "	durataBrano, "
                + "	codiceISWC, "
                + "	codiceISRC, "
                + "	orarioInizioBrano, "
                + "	categoriaDUso, "
                + "	diffusioneRegionale, "
                + "	stato, "
                + "	errore, "
                + "	altriMotiviDiScarto "
                + "FROM "
                + "	( "
                + "	SELECT "
                + "		palinsesto.nome AS palinsesto, "
                + "		showSchedule.TITLE AS titoloTrasmissione, "
                + "		DATE_FORMAT( showSchedule.BEGIN_TIME, '%d/%m/%Y' ) AS dataInizioTrasmissione, "
                + "		IF( DATE_FORMAT( showSchedule.BEGIN_TIME, '%H:%i:%s' ) = '00:00:00', "
                + "		NULL, "
                + "		DATE_FORMAT( showSchedule.BEGIN_TIME, '%H:%i:%s' ) ) AS orarioInizioTrasmissione, "
                + "		CAST( SEC_TO_TIME(showSchedule.DURATION) AS CHAR(11) ) AS durataTrasmissione, "
                + "		showMusic.TITLE AS titoloBrano, "
                + "		showMusic.COMPOSER AS compositoreAutore, "
                + "		showMusic.PERFORMER AS esecutorePerformer, "
                + "		showMusic.ALBUM AS album, "
                + "		CAST( SEC_TO_TIME(showMusic.DURATION) AS CHAR(11) ) AS durataBrano, "
                + "		showMusic.ISWC_CODE AS codiceISWC, "
                + "		showMusic.ISRC_CODE AS codiceISRC, "
                + "		DATE_FORMAT( showMusic.BEGIN_TIME, '%H:%i:%s' ) AS orarioInizioBrano, "
                + "		showMusic.ID_MUSIC_TYPE AS categoriaDUso, "
                + "		IF( showSchedule.REGIONAL_OFFICE IS NULL, "
                + "		0, "
                + "		1 ) AS diffusioneRegionale, "
                + "		'OK' AS stato, "
                + "		NULL AS errore, "
                + "		NULL AS altriMotiviDiScarto, "
                + "		showMusic.ID_RS_SHOW_MUSIC id, "
                + "		CONCAT( IF( DATE_FORMAT( showSchedule.BEGIN_TIME, '%Y/%m/%d' ) IS NULL, COALESCE( NULL, '' ), DATE_FORMAT( showSchedule.BEGIN_TIME, '%Y/%m/%d' ) ), IF( DATE_FORMAT( showSchedule.BEGIN_TIME, '%H:%i:%s' ) IS NULL, COALESCE( NULL, '' ), DATE_FORMAT( showSchedule.BEGIN_TIME, '%H:%i:%s' ) ) ) AS ordi "
                + "	FROM "
                + "		PERF_RS_SHOW_MUSIC showMusic "
                + "	RIGHT JOIN PERF_RS_SHOW_SCHEDULE showSchedule ON "
                + "		showMusic.ID_RS_SHOW_SCHEDULE = showSchedule.ID_RS_SHOW_SCHEDULE "
                + "	LEFT JOIN PERF_PALINSESTO palinsesto ON "
                + "		showSchedule.ID_PALINSESTO = palinsesto.ID_PALINSESTO "
                + "	WHERE "
                + "		showSchedule.ID_PALINSESTO IN ("+ StringUtils.join(idPalinseti,",") + ") "
                + "		AND showSchedule.SCHEDULE_YEAR = ?1 "
                + "		AND showSchedule.SCHEDULE_MONTH IN ("+ StringUtils.join(mesi,",") +") "
                + "UNION ALL "
                + "	SELECT "
                + "		IF( errShowSchedule.ID_PALINSESTO IS NULL, "
                + "		errShowSchedule.REPORT_PALINSESTO, "
                + "		palinsesto.nome ) AS palinsesto, "
                + "		errShowSchedule.TITLE AS titoloTrasmissione, "
                + "		errShowSchedule.REPORT_BEGIN_DATE AS dataInizioTrasmissione, "
                + "		errShowSchedule.REPORT_BEGIN_TIME AS orarioInizioTrasmissione, "
                + "		IF( errShowSchedule.DURATION IS NULL, "
                + "		errShowSchedule.REPORT_DURATION, "
                + "		CAST( SEC_TO_TIME(errShowSchedule.DURATION) AS CHAR(11) ) ) AS durataTrasmissione, "
                + "		errShowMusic.TITLE AS titoloBrano, "
                + "		errShowMusic.COMPOSER AS compositoreAutore, "
                + "		errShowMusic.PERFORMER AS esecutorePerformer, "
                + "		errShowMusic.ALBUM AS album, "
                + "		IF( errShowMusic.DURATION IS NULL, "
                + "		errShowMusic.REPORT_DURATION, "
                + "		CAST( SEC_TO_TIME(errShowMusic.DURATION) AS CHAR(11) ) ) AS durataBrano, "
                + "		errShowMusic.ISWC_CODE AS codiceISWC, "
                + "		errShowMusic.ISRC_CODE AS codiceISRC, "
                + "		IF( errShowMusic.BEGIN_TIME IS NULL, "
                + "		errShowMusic.REPORT_BEGIN_TIME, "
                + "		DATE_FORMAT( errShowMusic.BEGIN_TIME, '%H:%i:%s' ) ) AS orarioInizioBrano, "
                + "		IF( errShowMusic.ID_MUSIC_TYPE IS NULL, "
                + "		errShowMusic.REPORT_MUSIC_TYPE, "
                + "		errShowMusic.ID_MUSIC_TYPE ) AS categoriaDUso, "
                + "		IF( errShowSchedule.REGIONAL_OFFICE IS NULL, "
                + "		0, "
                + "		1 ) AS diffusioneRegionale, "
                + "		'KO' AS stato, "
                + "		errShowSchedule.ERROR AS errore, "
                + "		errShowSchedule.GLOBAL_ERROR AS altriMotiviDiScarto, "
                + "		errShowMusic.ID_RS_ERR_SHOW_MUSIC id, "
                + "		CONCAT( IF( IF( errShowSchedule.BEGIN_TIME IS NULL, errShowSchedule.REPORT_BEGIN_DATE, DATE_FORMAT( errShowSchedule.BEGIN_TIME, '%Y/%m/%d' ) ) IS NULL, COALESCE( NULL, '' ), IF( errShowSchedule.BEGIN_TIME IS NULL, errShowSchedule.REPORT_BEGIN_DATE, DATE_FORMAT( errShowSchedule.BEGIN_TIME, '%Y/%m/%d' ) ) ), IF( IF( errShowSchedule.BEGIN_TIME IS NULL, errShowSchedule.REPORT_BEGIN_TIME, DATE_FORMAT( errShowSchedule.BEGIN_TIME, '%H:%i:%s' ) ) IS NULL, COALESCE( NULL, '' ), IF( errShowSchedule.BEGIN_TIME IS NULL, errShowSchedule.REPORT_BEGIN_TIME, DATE_FORMAT( errShowSchedule.BEGIN_TIME, '%H:%i:%s' ) ) ) ) AS ordi "
                + "	FROM "
                + "		PERF_RS_ERR_SHOW_MUSIC errShowMusic "
                + "	RIGHT JOIN PERF_RS_ERR_SHOW_SCHEDULE errShowSchedule ON "
                + "		errShowMusic.ID_RS_ERR_SHOW_SCHEDULE = errShowSchedule.ID_RS_ERR_SHOW_SCHEDULE "
                + "	LEFT JOIN PERF_PALINSESTO palinsesto ON "
                + "		errShowSchedule.id_palinsesto = palinsesto.id_palinsesto "
                + "	LEFT JOIN PERF_RS_UTILIZATION_NORMALIZED_FILE utilizationNormalizedFile ON "
                + "		errShowSchedule.ID_NORMALIZED_FILE = utilizationNormalizedFile.ID_NORMALIZED_FILE "
                + "	LEFT JOIN PERF_RS_UTILIZATION_FILE utilizationFile ON "
                + "		utilizationNormalizedFile.ID_UTILIZATION_FILE = utilizationFile.id "
                + "	WHERE "
                + "		( ( errShowSchedule.id_palinsesto IN (" +StringUtils.join(idPalinseti,",")+ ") "
                + "		AND utilizationFile.ID_PALINSESTO IS NULL ) "
                + "		OR utilizationFile.ID_PALINSESTO IN (" + StringUtils.join(idPalinseti,",") + ")) "
                + "		AND ( ( errShowSchedule.SCHEDULE_YEAR = ?1 "
                + "		AND utilizationFile.anno IS NULL ) "
                + "		OR ( utilizationFile.anno = ?1 ) ) "
                + "		AND ( ( errShowSchedule.SCHEDULE_MONTH IN ("+StringUtils.join(mesi,",")+") "
                + "		AND utilizationFile.mese IS NULL ) "
                + "		OR utilizationFile.mese IN (" + StringUtils.join(mesi,",") + ") )) armonizzato "
                + "	GROUP BY "
                + "		id "
                + "	ORDER BY "
                + "		ordi ASC";

        return entityManager.createNativeQuery(w, "FileArmonizzatoMapping")
                .setParameter(1, anno)
                .getResultList();
    }
}
