package com.alkemytech.sophia.codman.entity;

import com.alkemytech.sophia.codman.dto.enumaration.UseStartEndDate;
import com.google.gson.annotations.Expose;
import lombok.Data;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement
@Entity(name = "AnagCCIDFilenameConfig")
@Table(name = "MM_ANAG_CCID_FILENAME_CONFIG")

public class AnagCCIDFilenameConfig {

    @Expose
    @Id
    @Column(name = "ID_FILENAME", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idFilename;

    @Expose
    @Column(name = "ID_CCID_CONFIG", nullable = false)
    private Integer idCCIDConfig;

    @Expose
    @Column(name = "INVOICE_SENDER")
    private String invoiceSender;

    @Expose
    @Column(name = "INVOICE_RECEIVER")
    private String invoiceReceiver;

    @Expose
    @Column(name = "USE_TYPE")
    private String useType;

    @Expose
    @Enumerated(EnumType.STRING)
    @Column(name = "USE_START_END")
    private UseStartEndDate useStartEndDate;

    @Expose
    @Column(name = "LICENSE_SD")
    private String licenseSD;

    @Expose
    @Column(name = "TYPE_OF_CLAIM")
    private String typeOfClaim;

}