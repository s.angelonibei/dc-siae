package com.alkemytech.sophia.codman.utils;

import org.jooq.tools.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RecursiveTask;

public class LongCsvRecursiveTask extends RecursiveTask<List<String>> {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private List<String[]> filterList;
    private List<String[]> sourceList;
    private static final int THRESHOLD = 1000;

    public LongCsvRecursiveTask(List<String[]> filterList, List<String[]> sourceList) {
        this.filterList = filterList;
        this.sourceList = sourceList;
    }

    @Override
    protected List<String> compute() {
        if (this.filterList.size() > THRESHOLD) {
            return createSubtasks();
        } else {
            return processing(filterList, sourceList);
        }
    }

    private List<String> createSubtasks() {

        LongCsvRecursiveTask left = new LongCsvRecursiveTask(
                filterList.subList(0, filterList.size() / 2), sourceList);
        LongCsvRecursiveTask right = new LongCsvRecursiveTask(
                filterList.subList(filterList.size() / 2, filterList.size()), sourceList);
        left.fork();
        List<String> result = right.compute();
        result.addAll(left.join());
        return result;
    }

    private List<String> processing(List<String[]> filterList, List<String[]> sourceList) {
        List<String> result = new ArrayList<>();
        for (String[] filter:filterList) {
            for(String[] source: sourceList){
                if(filter[8].substring(filter[8].lastIndexOf("_")+1).equals(""+(Integer.parseInt(source[2])+1))){
                    result.add(StringUtils.join(source, ';'));
                    break;
                }
            }
        }
        return result;
    }
}
