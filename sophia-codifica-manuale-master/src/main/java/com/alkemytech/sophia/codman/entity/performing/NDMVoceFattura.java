package com.alkemytech.sophia.codman.entity.performing;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

//TODO: See what table will be on database
@Entity
@Table(name = "PERF_NDM_VOCE_FATTURA")
public class NDMVoceFattura  implements Serializable {

	private Long id;
	private Long idNdmFile;
	private Integer annoFattura;
	private Integer meseFattura;
	private Date dataFattura;
	private Integer annoContabileFattura;
	private Integer meseContabileFattura;
	private Date dataContabileFattura;
	private Long idIncasso;
	private Long idQuietanza;
	private String numeroReversale;
	private String numeroFattura;
	private String voceIncasso;
	private BigDecimal importoOriginale;
	private BigDecimal importoAggio;
	private BigDecimal percentualeAggio;

	@Id
    @Column(name = "ID_NDM_VOCE_FATTURA")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
    @Column(name = "ID_NDM_FILE")
	public Long getIdNdmFile() {
		return idNdmFile;
	}
	public void setIdNdmFile(Long idNdmFile) {
		this.idNdmFile = idNdmFile;
	}
    @Column(name = "ANNO_FATTURA")
	public Integer getAnnoFattura() {
		return annoFattura;
	}
	public void setAnnoFattura(Integer annoFattura) {
		this.annoFattura = annoFattura;
	}
    @Column(name = "MESE_FATTURA")
	public Integer getMeseFattura() {
		return meseFattura;
	}
	public void setMeseFattura(Integer meseFattura) {
		this.meseFattura = meseFattura;
	}
    @Column(name = "DATA_FATTURA")
	public Date getDataFattura() {
		return dataFattura;
	}
	public void setDataFattura(Date dataFattura) {
		this.dataFattura = dataFattura;
	}
    @Column(name = "ANNO_CONTABILE_FATTURA")
	public Integer getAnnoContabileFattura() {
		return annoContabileFattura;
	}
	public void setAnnoContabileFattura(Integer annoContabileFattura) {
		this.annoContabileFattura = annoContabileFattura;
	}
    @Column(name = "MESE_CONTABILE_FATTURA")
	public Integer getMeseContabileFattura() {
		return meseContabileFattura;
	}
	public void setMeseContabileFattura(Integer meseContabileFattura) {
		this.meseContabileFattura = meseContabileFattura;
	}
    @Column(name = "DATA_CONTABILE_FATTURA")
	public Date getDataContabileFattura() {
		return dataContabileFattura;
	}
	public void setDataContabileFattura(Date dataContabileFattura) {
		this.dataContabileFattura = dataContabileFattura;
	}
    @Column(name = "ID_INCASSO")
	public Long getIdIncasso() {
		return idIncasso;
	}
	public void setIdIncasso(Long idIncasso) {
		this.idIncasso = idIncasso;
	}
    @Column(name = "ID_QUIETANZA")
	public Long getIdQuietanza() {
		return idQuietanza;
	}
	public void setIdQuietanza(Long idQuietanza) {
		this.idQuietanza = idQuietanza;
	}
    @Column(name = "NUMERO_REVERSALE")
	public String getNumeroReversale() {
		return numeroReversale;
	}
	public void setNumeroReversale(String numeroReversale) {
		this.numeroReversale = numeroReversale;
	}
    @Column(name = "NUMERO_FATTURA")
	public String getNumeroFattura() {
		return numeroFattura;
	}
	public void setNumeroFattura(String numeroFattura) {
		this.numeroFattura = numeroFattura;
	}
    @Column(name = "VOCE")
	public String getVoceIncasso() {
		return voceIncasso;
	}
	public void setVoceIncasso(String voceIncasso) {
		this.voceIncasso = voceIncasso;
	}
    @Column(name = "IMPORTO_ORIGINALE")
	public BigDecimal getImportoOriginale() {
		return importoOriginale;
	}
	public void setImportoOriginale(BigDecimal importoOriginale) {
		this.importoOriginale = importoOriginale;
	}
    @Column(name = "IMPORTO_AGGIO")
	public BigDecimal getImportoAggio() {
		return importoAggio;
	}
	public void setImportoAggio(BigDecimal importoAggio) {
		this.importoAggio = importoAggio;
	}
    @Column(name = "PERCENTUALE_AGGIO")
	public BigDecimal getPercentualeAggio() {
		return percentualeAggio;
	}
	public void setPercentualeAggio(BigDecimal percentualeAggio) {
		this.percentualeAggio = percentualeAggio;
	}
	
	
	
}
