package com.alkemytech.sophia.codman.entity;

import java.io.Serializable;

@SuppressWarnings("serial")
public class CurrencyRateId implements Serializable {

	private String year;
	private String month;
	private String srcCurrency;
	private String dstCurrency;
	
	public CurrencyRateId() {
		super();
	}
	
	public CurrencyRateId(String year, String month, String srcCurrency, String dstCurrency) {
		super();
		this.year = year;
		this.month = month;
		this.srcCurrency = srcCurrency;
		this.dstCurrency = dstCurrency;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getSrcCurrency() {
		return srcCurrency;
	}

	public void setSrcCurrency(String srcCurrency) {
		this.srcCurrency = srcCurrency;
	}

	public String getDstCurrency() {
		return dstCurrency;
	}

	public void setDstCurrency(String dstCurrency) {
		this.dstCurrency = dstCurrency;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dstCurrency == null) ? 0 : dstCurrency.hashCode());
		result = prime * result + ((month == null) ? 0 : month.hashCode());
		result = prime * result
				+ ((srcCurrency == null) ? 0 : srcCurrency.hashCode());
		result = prime * result + ((year == null) ? 0 : year.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CurrencyRateId other = (CurrencyRateId) obj;
		if (dstCurrency == null) {
			if (other.dstCurrency != null)
				return false;
		} else if (!dstCurrency.equals(other.dstCurrency))
			return false;
		if (month == null) {
			if (other.month != null)
				return false;
		} else if (!month.equals(other.month))
			return false;
		if (srcCurrency == null) {
			if (other.srcCurrency != null)
				return false;
		} else if (!srcCurrency.equals(other.srcCurrency))
			return false;
		if (year == null) {
			if (other.year != null)
				return false;
		} else if (!year.equals(other.year))
			return false;
		return true;
	}


}
