package com.alkemytech.sophia.codman.entity.performing.dao;

import com.alkemytech.sophia.broadcasting.model.BdcRuoli;
import com.alkemytech.sophia.codman.dto.performing.MusicProviderDTO;
import com.alkemytech.sophia.codman.dto.performing.PalinsestoDTO;
import com.alkemytech.sophia.codman.dto.performing.RsUtenteDTO;
import com.alkemytech.sophia.performing.model.*;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.http.util.TextUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class RadioInStoreDAO implements IRadioInStoreDAO {

	private Provider<EntityManager> provider;

	@Inject
	public RadioInStoreDAO(Provider<EntityManager> provider) {
		this.provider = provider;
	}

	@Override
	public MusicProviderDTO getMusicProviderById(Long id) {
		EntityManager em = provider.get();
		MusicProviderDTO perfMusicProvider = em
				.createQuery("SELECT new com.alkemytech.sophia.codman.dto.performing.MusicProviderDTO( "
						+ "mp.idMusicProvider, mp.nome, mp.dataCreazione, mp.attivo, mp.dataInizioValidita, "
						+ "(CASE WHEN (SELECT COUNT(p) FROM mp.perfPalinsestos p WHERE(p.dataFineValidita IS NULL OR p.dataFineValidita >= current_timestamp )) = 0 "
						+ "THEN (SELECT MAX(p2.dataFineValidita) FROM mp.perfPalinsestos p2) "
						+ "ELSE null END)) FROM  PerfMusicProvider mp"+
						"  where mp.idMusicProvider=:id ", MusicProviderDTO.class)
				.setParameter("id", id).getSingleResult();
		return perfMusicProvider;
	}

	@Override
	public List<MusicProviderDTO> getMusicProviderListByName(String nome) {
		EntityManager em = provider.get();
		StringBuffer query = new StringBuffer("SELECT new com.alkemytech.sophia.codman.dto.performing.MusicProviderDTO( "
				+ "mp.idMusicProvider, mp.nome, mp.dataCreazione, mp.attivo, mp.dataInizioValidita, "
				+ "(CASE WHEN (SELECT COUNT(p) FROM mp.perfPalinsestos p WHERE(p.dataFineValidita IS NULL OR p.dataFineValidita >= current_timestamp )) = 0 "
				+ "THEN (SELECT MAX(p2.dataFineValidita) FROM mp.perfPalinsestos p2) "
				+ "ELSE null END)) FROM  PerfMusicProvider mp");
		if (!TextUtils.isEmpty(nome)) {
			query.append(" where LOWER(mp.nome) LIKE LOWER(:nome)");
		}
		Query q = em.createQuery(query.toString(), MusicProviderDTO.class);
		if (!TextUtils.isEmpty(nome)) {
			q.setParameter("nome", "%" + nome + "%");
		}
		List<MusicProviderDTO> perfMusicProvider = q.getResultList();
		return perfMusicProvider;
	}

	@Override
	public List<MusicProviderDTO> getMusicProviderList() {
		EntityManager em = provider.get();
		List<MusicProviderDTO> perfMusicProviderList = em
				.createQuery("SELECT new com.alkemytech.sophia.codman.dto.performing.MusicProviderDTO( "
						+ "mp.idMusicProvider, mp.nome, mp.dataCreazione, mp.attivo, mp.dataInizioValidita, "
						+ "(CASE WHEN (SELECT COUNT(p) FROM mp.perfPalinsestos p WHERE(p.dataFineValidita IS NULL OR p.dataFineValidita >= current_timestamp )) = 0 "
						+ "THEN (SELECT MAX(p2.dataFineValidita) FROM mp.perfPalinsestos p2) "
						+ "ELSE null END)) FROM  PerfMusicProvider mp", MusicProviderDTO.class).getResultList();
		return perfMusicProviderList;
	}

	@Override
	public List<PerfPalinsesto> getPalinsestiList() {
		EntityManager em = provider.get();
		List<PerfPalinsesto> perfPalinsestoList = em.createNamedQuery("PerfPalinsesto.findAll", PerfPalinsesto.class)
				.getResultList();
		return perfPalinsestoList;
	}

	@Override
	public List<PerfPalinsesto> getPalinsestiListByMp(Long id, Long idPalinsesto) {
		EntityManager em = provider.get();
		if (idPalinsesto == null) {
			List<PerfPalinsesto> perfPalinsestoList = em.createQuery(
					"Select p From PerfPalinsesto p where p.perfMusicProvider.idMusicProvider = :idMusicProvider",
					PerfPalinsesto.class).setParameter("idMusicProvider", id).getResultList();
			return perfPalinsestoList;
		} else {
			List<PerfPalinsesto> perfPalinsestoList = em.createQuery(
					"Select p From PerfPalinsesto p join p.perfMusicProvider.perfPalinsestos palinsesti where p.perfMusicProvider.idMusicProvider = :idMusicProvider and palinsesti.idPalinsesto = :idPalinsesto",
					PerfPalinsesto.class).setParameter("idMusicProvider", id).setParameter("idPalinsesto", idPalinsesto)
					.getResultList();
			return perfPalinsestoList;
		}
	}

	@Override
	public List<PerfPuntoVendita> getPuntiVenditaList() {
		EntityManager em = provider.get();
		List<PerfPuntoVendita> perfPuntoVenditaList = em
				.createNamedQuery("PerfPuntoVendita.findAll", PerfPuntoVendita.class).getResultList();
		return perfPuntoVenditaList;

	}

	@Override
	public List<PerfPuntoVendita> getPuntiVenditaByPalinsesto(Long id, Long idPalinsesto) {
		EntityManager em = provider.get();
		if (id == null) {
			List<PerfPuntoVendita> perfPuntoVenditaByPalinsesto = em
					.createQuery("select a from PerfPuntoVendita a where a.perfPalinsesto.idPalinsesto = :idPalinsesto",
							PerfPuntoVendita.class)
					.setParameter("idPalinsesto", idPalinsesto).getResultList();
			return perfPuntoVenditaByPalinsesto;

		} else {
			List<PerfPuntoVendita> perfPuntoVenditaByPalinsesto = em.createQuery(
					"select a from PerfPuntoVendita a where a.perfPalinsesto.idPalinsesto = :idPalinsesto and a.idPuntoVendita = :id",
					PerfPuntoVendita.class).setParameter("idPalinsesto", idPalinsesto).setParameter("id", id)
					.getResultList();
			return perfPuntoVenditaByPalinsesto;
		}
	}

	public List<PerfRsUtente> getUsers(Long idMusicProvider) {
		EntityManager em = provider.get();
		if (idMusicProvider != null) {
			List<PerfRsUtente> perfPalinsestoList = em
					.createQuery("Select x From PerfRsUtente x where x.perfMusicProvider.idMusicProvider = :idMusicProvider",
							PerfRsUtente.class)
					.setParameter("idMusicProvider", idMusicProvider).getResultList();
			return perfPalinsestoList;
		} else {
			List<PerfRsUtente> perfPalinsestoList = em.createQuery("Select x From PerfRsUtente x", PerfRsUtente.class)
					.getResultList();
			return perfPalinsestoList;
		}
	}

	@Override
	public Integer insertMusicProvider(MusicProviderDTO musicProvider) {
		EntityManager em = provider.get();
		em.getTransaction().begin();
		try {
			PerfMusicProvider perfMusicProvider = new PerfMusicProvider();
			perfMusicProvider.setAttivo(musicProvider.getDataInizioValidita() == null || musicProvider.getDataInizioValidita().before(GregorianCalendar.getInstance().getTime())
					);
			perfMusicProvider.setDataFineValidita(musicProvider.getDataFineValidita());
			perfMusicProvider.setDataInizioValidita(musicProvider.getDataInizioValidita());
			perfMusicProvider.setNome(musicProvider.getNome());
			PerfRsConfig perfRsConfig = new PerfRsConfig();
			PerfRsConfigTemplate perfRsConfigTemplate = new PerfRsConfigTemplate();
			//TODO in futuro se ci dovessro essere altre configurazioni cambiare il metodo
			perfRsConfigTemplate.setIdPerfRsConfigTemplate(1L);
			perfRsConfig.setPerfRsConfigTemplate(perfRsConfigTemplate);
			perfRsConfig.setPerfMusicProvider(perfMusicProvider);
			em.merge(perfRsConfig);
			em.getTransaction().commit();
			return 200;
		} catch (Exception e) {
			em.getTransaction().rollback();
			return 500;
		}
	}

	@Override
	public Integer updateMusicProvider(MusicProviderDTO musicProvider) {
		EntityManager em = provider.get();
		em.getTransaction().begin();
		try {
			PerfMusicProvider perfMusicProvider = (PerfMusicProvider) em
					.createQuery("Select p from PerfMusicProvider p where p.idMusicProvider = :id")
					.setParameter("id", musicProvider.getIdMusicProvider()).getSingleResult();
			if (!TextUtils.isEmpty(musicProvider.getNome())) {
				perfMusicProvider.setNome(musicProvider.getNome());
			}
			if (null != musicProvider.getAttivo()) {
				perfMusicProvider.setAttivo(musicProvider.getAttivo());
			}
			perfMusicProvider.setDataFineValidita(musicProvider.getDataFineValidita());
			perfMusicProvider.setDataInizioValidita(musicProvider.getDataInizioValidita());
			em.merge(perfMusicProvider);
			em.getTransaction().commit();
			return 200;
		} catch (Exception e) {
			em.getTransaction().rollback();
			return 500;
		}
	}

	@Override
	public Integer updatePalinsesto(PalinsestoDTO palinsesto) {
		EntityManager em = provider.get();
		em.getTransaction().begin();

		try {
			PerfPalinsesto perfPalinsesto = (PerfPalinsesto) em
					.createQuery("Select p from PerfPalinsesto p where p.idPalinsesto = :id")
					.setParameter("id", palinsesto.getIdPalinsesto()).getSingleResult();
			PerfMusicProvider perfMusicProvider = em
					.createQuery("Select x From PerfMusicProvider x where x.idMusicProvider=:id",
							PerfMusicProvider.class)
					.setParameter("id", palinsesto.getIdMusicProvider()).getSingleResult();
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			perfPalinsesto.setAttivo(true);
			perfPalinsesto.setCodiceDitta(palinsesto.getCodiceDitta());
			perfPalinsesto.setDataCreazione(new Date());
			if (!TextUtils.isEmpty(palinsesto.getDataFineValidita())) {
				perfPalinsesto.setDataFineValidita(sdf.parse(palinsesto.getDataFineValidita()));
			} else {
				perfPalinsesto.setDataFineValidita(null);
			}
			if (!TextUtils.isEmpty(palinsesto.getDataInizioValidita())) {
				perfPalinsesto.setDataInizioValidita(sdf.parse(palinsesto.getDataInizioValidita()));
			} else {
				perfPalinsesto.setDataInizioValidita(new Date());
			}
			perfPalinsesto.setDataUltimaModifica(new Date());
			perfPalinsesto.setNome(palinsesto.getNome());
			perfPalinsesto.setUtenteUltimaModifica(palinsesto.getUtenteUltimaModifica());
			perfPalinsesto.setPerfMusicProvider(perfMusicProvider);

			em.merge(perfPalinsesto);
			em.flush();
			PerfPalinsestoStorico storico = new PerfPalinsestoStorico();
			storico.setPerfPalinsesto(perfPalinsesto);
			storico.setAttivo(true);
			storico.setCodiceDitta(palinsesto.getCodiceDitta());
			storico.setDataCreazione(new Date());
			if (!TextUtils.isEmpty(palinsesto.getDataFineValidita())) {
				storico.setDataFineValidita(sdf.parse(palinsesto.getDataFineValidita()));
			} else {
				storico.setDataFineValidita(null);
			}
			if (!TextUtils.isEmpty(palinsesto.getDataInizioValidita())) {
				storico.setDataInizioValidita(sdf.parse(palinsesto.getDataInizioValidita()));
			} else {
				storico.setDataInizioValidita(new Date());
			}
			storico.setDataUltimaModifica(new Date());
			storico.setNome(palinsesto.getNome());
			storico.setUtenteUltimaModifica(palinsesto.getUtenteUltimaModifica());
			storico.setPerfMusicProvider(perfMusicProvider);

			em.persist(storico);
			em.getTransaction().commit();
			return 200;
		} catch (Exception e) {
			em.getTransaction().rollback();
			return 500;
		}
	}

	@Override
	public Integer insertPalinsesto(PalinsestoDTO palinsesto) {
		EntityManager em = provider.get();

		em.getTransaction().begin();
		try {
			PerfMusicProvider perfMusicProvider = em
					.createQuery("Select x From PerfMusicProvider x where x.idMusicProvider=:id",
							PerfMusicProvider.class)
					.setParameter("id", palinsesto.getIdMusicProvider()).getSingleResult();
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			PerfPalinsesto perfPalinsesto = new PerfPalinsesto();
			perfPalinsesto.setAttivo(true);
			perfPalinsesto.setCodiceDitta(palinsesto.getCodiceDitta());
			perfPalinsesto.setDataCreazione(new Date());
			if (!TextUtils.isEmpty(palinsesto.getDataFineValidita())) {
				perfPalinsesto.setDataFineValidita(sdf.parse(palinsesto.getDataFineValidita()));
			} else {
				perfPalinsesto.setDataFineValidita(null);
			}
			if (!TextUtils.isEmpty(palinsesto.getDataFineValidita())) {
				perfPalinsesto.setDataInizioValidita(sdf.parse(palinsesto.getDataInizioValidita()));
			} else {
				perfPalinsesto.setDataInizioValidita(new Date());
			}
			perfPalinsesto.setDataUltimaModifica(new Date());
			perfPalinsesto.setNome(palinsesto.getNome());
			perfPalinsesto.setUtenteUltimaModifica(palinsesto.getUtenteUltimaModifica());
			perfPalinsesto.setPerfMusicProvider(perfMusicProvider);
			em.persist(perfPalinsesto);
			em.flush();
			PerfPalinsestoStorico storico = new PerfPalinsestoStorico();
			storico.setPerfPalinsesto(perfPalinsesto);
			storico.setAttivo(true);
			storico.setCodiceDitta(palinsesto.getCodiceDitta());
			storico.setDataCreazione(new Date());
			if (!TextUtils.isEmpty(palinsesto.getDataFineValidita())) {
				storico.setDataFineValidita(sdf.parse(palinsesto.getDataFineValidita()));
			} else {
				storico.setDataFineValidita(null);
			}
			if (!TextUtils.isEmpty(palinsesto.getDataInizioValidita())) {
				storico.setDataInizioValidita(sdf.parse(palinsesto.getDataInizioValidita()));
			} else {
				storico.setDataInizioValidita(new Date());
			}
			storico.setDataUltimaModifica(new Date());
			storico.setNome(palinsesto.getNome());
			storico.setUtenteUltimaModifica(palinsesto.getUtenteUltimaModifica());
			storico.setPerfMusicProvider(perfMusicProvider);
			em.persist(storico);
			em.getTransaction().commit();
			return 200;
		} catch (Exception e) {
			em.getTransaction().rollback();
			return 500;
		}
	}

	public Integer insertRsUtente(RsUtenteDTO rsUtenteDTO) {
		EntityManager em = provider.get();
		em.getTransaction().begin();
		try {
			BdcRuoli ruoloPm = (BdcRuoli) em.createQuery("SELECT a FROM BdcRuoli a  where a.ruolo='MP'").getSingleResult();
			PerfRsUtente perfRsUtente = new PerfRsUtente();
			perfRsUtente.setDataCreazione(new Date());
			perfRsUtente.setDataUltimaModifica(new Date());
			perfRsUtente.setEmail(rsUtenteDTO.getEmail());
			perfRsUtente.setPerfMusicProvider(new PerfMusicProvider());
			perfRsUtente.getPerfMusicProvider().setIdMusicProvider(rsUtenteDTO.getIdMusicProvider());
			PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			perfRsUtente.setPassword(passwordEncoder.encode(rsUtenteDTO.getPassword()));
			perfRsUtente.setUsername(rsUtenteDTO.getUsername());
			perfRsUtente.setUtenteUltimaModifica(rsUtenteDTO.getUtenteUltimaModifica());
			perfRsUtente.setIdRuolo(ruoloPm.getId());
			em.persist(perfRsUtente);
			em.getTransaction().commit();
			return 200;
		} catch (Exception e) {
			em.getTransaction().rollback();
			return 500;
		}

	}

	public Integer updateRsUtente(RsUtenteDTO rsUtenteDTO) {
		EntityManager em = provider.get();
		em.getTransaction().begin();
		try {
			PerfRsUtente perfRsUtente = em
					.createQuery("Select x from PerfRsUtente x where x.id=:id", PerfRsUtente.class)
					.setParameter("id", rsUtenteDTO.getId()).getSingleResult();
			perfRsUtente.setDataUltimaModifica(new Date());
			perfRsUtente.setEmail(rsUtenteDTO.getEmail());
			perfRsUtente.setUsername(rsUtenteDTO.getUsername());
			perfRsUtente.setUtenteUltimaModifica(rsUtenteDTO.getUtenteUltimaModifica());
			em.merge(perfRsUtente);
			em.getTransaction().commit();
			return 200;
		} catch (Exception e) {
			em.getTransaction().rollback();
			return 500;
		}

	}

	public List<PerfPalinsestoStorico> getStoricoPalinsesto(Long idPalinsesto) {
		EntityManager em = provider.get();
		List<PerfPalinsestoStorico> perfPalinsestoList = em
				.createQuery("Select p From PerfPalinsestoStorico p where p.perfPalinsesto.idPalinsesto = :idPalinsesto",
						PerfPalinsestoStorico.class)
				.setParameter("idPalinsesto", idPalinsesto).getResultList();
		return perfPalinsestoList;
	}

	public List<PerfRsUtilizationFile> getUtilizationFile(Long idMusicProvider, Long idPalinsesto, Long anno,
														  List<Long> mesi) {
		EntityManager em = provider.get();
		StringBuffer query = new StringBuffer(" Select x From PerfRsUtilizationFile x ");
		if (null != idMusicProvider || null != idPalinsesto || null != anno || mesi.size() > 0) {
			query.append(" where ");
			if (idMusicProvider != null) {
				query.append(" x.perfMusicProvider.idMusicProvider = :idMusicProvider ");
			}
			if (idPalinsesto != null) {
				if (idMusicProvider != null) {
					query.append(" AND ");
				}
				query.append(" x.perfPalinsesto.idPalinsesto = :idPalinsesto ");
			}
			if (anno != null) {
				if (idMusicProvider != null || idPalinsesto != null) {
					query.append(" AND ");
				}

				query.append(" x.anno = :anno ");
			}
			if (mesi.size() > 0) {
				if (idMusicProvider != null || idPalinsesto != null || anno != null) {
					query.append(" AND ");
				}
				query.append(" x.mese IN :mesi");
			}
		}
		TypedQuery<PerfRsUtilizationFile> typedQuery = em.createQuery(query.toString(), PerfRsUtilizationFile.class);

			if (idMusicProvider != null) {
				typedQuery.setParameter("idMusicProvider", idMusicProvider);
			}
			if (idPalinsesto != null) {
				typedQuery.setParameter("idPalinsesto", idPalinsesto);
			}
			if (anno != null) {
				typedQuery.setParameter("anno", anno);
			}
			if (mesi.size() > 0) {
				typedQuery.setParameter("mesi", mesi);
			}
		return typedQuery.getResultList();
	}

	@Override
	public PerfRsUtilizationFile save(PerfRsUtilizationFile rsUtilizationFile) {
		EntityManager em = provider.get();
		em.getTransaction().begin();
		try {
			rsUtilizationFile.setDataUpload(new Date());
			em.persist(rsUtilizationFile);
			em.getTransaction().commit();
		} catch (Exception e) {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			throw e;
		}

		final Query q = em.createQuery("SELECT e FROM PerfRsUtilizationFile e WHERE e.id=:id").setParameter("id",
				rsUtilizationFile.getId());
		return (PerfRsUtilizationFile) q.getSingleResult();
	}
}
