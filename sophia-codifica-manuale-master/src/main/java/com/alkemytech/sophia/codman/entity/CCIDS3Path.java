package com.alkemytech.sophia.codman.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.codman.persistence.AbstractEntity;



@SuppressWarnings("serial")
@XmlRootElement
@Entity(name="CCIDS3Path")
@Table(name="CCID_S3_PATH")
public class CCIDS3Path  extends AbstractEntity<String>{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_CCID_PATH", nullable=false)
	private Integer idCCIDPath;

	@Column(name="IDDSR", nullable=false)
	private String idDsr;
	
	@Column(name="S3_PATH", nullable=false)
	private String s3Path;
	
	
	@Override
	public String getId() {
		return String.valueOf(idCCIDPath);
		
		
	}

	public Integer getIdCCIDPath() {
		return idCCIDPath;
	}

	public void setIdCCIDPath(Integer idCCIDPath) {
		this.idCCIDPath = idCCIDPath;
	}

	public String getIdDsr() {
		return idDsr;
	}

	public void setIdDsr(String idDsr) {
		this.idDsr = idDsr;
	}

	public String getS3Path() {
		return s3Path;
	}

	public void setS3Path(String s3Path) {
		this.s3Path = s3Path;
	}

	
}
