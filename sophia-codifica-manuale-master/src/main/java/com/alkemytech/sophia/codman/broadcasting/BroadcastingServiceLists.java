package com.alkemytech.sophia.codman.broadcasting;

import com.alkemytech.sophia.broadcasting.model.BdcBroadcasters;
import com.alkemytech.sophia.broadcasting.model.BdcCanaleStorico;
import com.alkemytech.sophia.broadcasting.model.BdcCanali;
import com.alkemytech.sophia.broadcasting.service.interfaces.BdcBroadcastersService;
import com.alkemytech.sophia.broadcasting.service.interfaces.BdcCanaliService;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.sun.jersey.multipart.FormDataParam;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

@Singleton
@Path("broadcasting")
public class BroadcastingServiceLists {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Gson gson;
	private BdcCanaliService bdcCanaliService;
	private BdcBroadcastersService bdcBroadcastersService;

	@Inject
	protected BroadcastingServiceLists(BdcCanaliService bdcCanaliService, BdcBroadcastersService bdcBroadcastersService,
			Gson gson) {
		super();
		this.bdcCanaliService = bdcCanaliService;
		this.bdcBroadcastersService = bdcBroadcastersService;
		this.gson = gson;
	}

	@GET
	@Path("canali")
	@Produces("application/json")
	public Response getCanali() {
		List<BdcCanali> bdcCanali = bdcCanaliService.findAll();
		return Response.ok(gson.toJson(bdcCanali)).build();
	}

	@GET
	@Path("canaliAttivi")
	@Produces("application/json")
	public Response getCanaliAttivi() {
		List<BdcCanali> bdcCanali = bdcCanaliService.findAllActive();
		return Response.ok(gson.toJson(bdcCanali)).build();
	}
	
	@GET
	@Path("canali/{id}")
	@Produces("application/json")
	public Response getCanali(@PathParam("id") Integer id) {
		List<BdcCanali> bdcCanali = bdcCanaliService.findByBroadcasterId(id);
		Date today = new Date();
		for (BdcCanali bdcCanale : bdcCanali) {
			if (bdcCanale.getDataInizioValid().getTime() < today.getTime() 
					&& (bdcCanale.getDataFineValid() == null|| bdcCanale.getDataFineValid().getTime() > today.getTime())) {
				bdcCanale.setActive(1);
			} else {
				bdcCanale.setActive(0);
			}
		}
		return Response.ok(gson.toJson(bdcCanali)).build();
	}

	@GET
	@Path("allCanali/{id}")
	@Produces("application/json")
	public Response getAllCanali(@PathParam("id") Integer id) {
		List<BdcCanali> bdcCanali = bdcCanaliService.findByBroadcasterId(id);
		Date today = new Date();
		for (BdcCanali bdcCanale : bdcCanali) {
			if (bdcCanale.getDataInizioValid().getTime() < today.getTime() 
					&& (bdcCanale.getDataFineValid() == null|| bdcCanale.getDataFineValid().getTime() > today.getTime())) {
				bdcCanale.setActive(1);
			} else {
				bdcCanale.setActive(0);
			}
		}
		return Response.ok(gson.toJson(bdcCanali)).build();
	}
	
	@GET
	@Path("emittenti")
	@Produces("application/json")
	public Response getEmittenti() {
		List<BdcBroadcasters> bdcCanali = bdcBroadcastersService.findAll();
		return Response.ok(gson.toJson(bdcCanali)).build();
	}

	@GET
	@Path("emittentiFiltrati")
	@Produces("application/json")
	public Response getEmittentiFiltrati(@QueryParam("fiteredName") String fiteredName,
			@QueryParam("fiteredType") String fiteredType) {
		List<BdcBroadcasters> broadcaster = bdcBroadcastersService.findFiltered(fiteredName, fiteredType);
		return Response.ok(gson.toJson(broadcaster)).build();
	}

	@GET
	@Path("storicoCanali/{id}")
	@Produces("application/json")
	public Response findAllRecent(@PathParam("id") Integer id) {
		List<BdcCanaleStorico> canaleStorico = bdcCanaliService.findAllRecent(id);
		return Response.ok(gson.toJson(canaleStorico)).build();
	}

	@POST
	@Path("uploadImage")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces("application/json")
	public Response upload(@FormDataParam("file") InputStream uploadedInputStream,
			@FormDataParam("broadcasterId") Integer broadcasterId) {
		try {
			byte[] bytes = IOUtils.toByteArray(uploadedInputStream);

			boolean esito = bdcBroadcastersService.uploadImage(bytes, broadcasterId);
			if (esito) {

				return Response.ok().build();
			} else {

				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			}
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@GET
	@Path("deleteImage")
	@Produces("application/json")
	public Response deleteImage(@QueryParam("broadcasterId") Integer broadcasterId) {
		bdcBroadcastersService.deleteImage(broadcasterId);
		return Response.ok().build();

	}

}
