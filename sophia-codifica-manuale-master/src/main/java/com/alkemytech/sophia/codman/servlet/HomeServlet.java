package com.alkemytech.sophia.codman.servlet;

import com.google.inject.Singleton;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@Singleton
@SuppressWarnings("serial")
public class HomeServlet extends HttpServlet {

	protected HomeServlet() {
		super();
	}
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		final HttpSession session = request.getSession(false);
		@SuppressWarnings("unchecked")
		final List<String> groups = (List<String>) session.getAttribute("sso.user.groups");

		if ("DEVELOPMENT".equals(groups.get(0))) {
			response.sendRedirect("#!/dspStatistics");
		} else if ("BU-MULTIMEDIALE".equals(groups.get(0))) {
			response.sendRedirect("#!/dspStatistics");
		} else if ("BU-MUSICA".equals(groups.get(0))) {
			response.sendRedirect("#!/evaluate-channel");
		} else if ("IT".equals(groups.get(0))) {
			response.sendRedirect("#!/dspStatistics");
		} else if ("GA".equals(groups.get(0))) {
			response.sendRedirect("#!/dspStatistics");
		}  else if ("DEV".equals(groups.get(0))) {
			response.sendRedirect("#!/dspStatistics");
		} else if ("CODIFICATORESPERTO".equals(groups.get(0))) {
			response.sendRedirect("#!/codificaManualeEsperto");
		} else if ("CODIFICATOREBASE".equals(groups.get(0))) {
            response.sendRedirect("#!/codificaManualeBase");
        }else if ("PERF_OPERATORE".equals(groups.get(0)) || "PERF_COORDINATORE".equals(groups.get(0))
                || "PERF_FUNZIONARIO".equals(groups.get(0)) || "PERF_DIRETTORE".equals(groups.get(0))){
            response.sendRedirect("#!/visualizzazioneEventi");
		} else {
			response.sendError(500);
		}

	}
	
}
