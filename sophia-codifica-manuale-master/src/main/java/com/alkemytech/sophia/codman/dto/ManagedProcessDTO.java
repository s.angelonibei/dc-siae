package com.alkemytech.sophia.codman.dto;

import java.io.UnsupportedEncodingException;

import javax.xml.bind.DatatypeConverter;

import com.alkemytech.sophia.codman.entity.AnagDsrProcess;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class ManagedProcessDTO {

	private String idDsrProcess = "";
	private String name = "";
	private String description="";
	
	// -1 = step non utilizzato
	// 1-4 range precedenza step
	private int identificationByIdSong = -1;
	private int identificationByIswc = -1;
	private int identificationByIsrc = -1;
	private int identificationByAuthorTitle = -1;
	private final static String ISWC = "ISWC";
	private final static String ISRC = "ISRC";
	private final static String IDSONG = "IDSONG";
	private final static String TIT_AUT = "TIT_AUT";
	private static final String[] steps = {ISWC,ISRC,IDSONG,TIT_AUT};
	
	public ManagedProcessDTO(AnagDsrProcess process) {
		this.idDsrProcess = process.getIdDsrProcess();
		this.name = process.getName();
		this.description = process.getDescription();
		JsonParser parser = new JsonParser();
		String configuration = process.getConfiguration();
		
		JsonElement jelement = parser.parse(configuration);		
		if(!jelement.isJsonObject()){
			String convertedBinaryString = "";
			byte[] bytes = DatatypeConverter.parseHexBinary(configuration);
			try {
				convertedBinaryString= new String(bytes, "UTF-8");
				
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 jelement = parser.parse(convertedBinaryString);
		}
		for(String step : steps){
			setProcessStepValue(step, jelement);
		}
	}

	private void setProcessStepValue(String stepName, JsonElement jelement){
		
		JsonObject jobject = jelement.getAsJsonObject();
		jobject = jobject.getAsJsonObject(stepName);
		int weight = -1;
		if(jobject.get("enabled").getAsBoolean()){
			weight = jobject.get("weight").getAsInt();
		}
		
		switch (stepName) {
		case ISWC:
			identificationByIswc = weight;
			break;
		case ISRC:
			identificationByIsrc = weight;
			break;
		case IDSONG:
			identificationByIdSong = weight;
			break;
		case TIT_AUT:
			identificationByAuthorTitle = weight;
			break;	
		default:
			break;
		}
		
	}
	public String getIdDsrProcess() {
		return idDsrProcess;
	}

	public void setIdDsrProcess(String idDsrProcess) {
		this.idDsrProcess = idDsrProcess;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getIdentificationByIdSong() {
		return identificationByIdSong;
	}

	public void setIdentificationByIdSong(int identificationByIdSong) {
		this.identificationByIdSong = identificationByIdSong;
	}

	public int getIdentificationByIswc() {
		return identificationByIswc;
	}

	public void setIdentificationByIswc(int identificationByIswc) {
		this.identificationByIswc = identificationByIswc;
	}

	public int getIdentificationByIsrc() {
		return identificationByIsrc;
	}

	public void setIdentificationByIsrc(int identificationByIsrc) {
		this.identificationByIsrc = identificationByIsrc;
	}

	public int getIdentificationByAuthorTitle() {
		return identificationByAuthorTitle;
	}

	public void setIdentificationByAuthorTitle(int identificationByAuthorTitle) {
		this.identificationByAuthorTitle = identificationByAuthorTitle;
	}

}
