package com.alkemytech.sophia.codman.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="MANDATO_CONFIGURAZIONE")
@NamedQueries({
	
})
public class ConfigurazioneMandato implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	protected Integer id;
		
	@OneToOne
	@JoinColumn(name = "ID_MANDATO", referencedColumnName = "ID")
	private Mandato mandato;
	
	@Column(name = "DSP")
	private String dsp;
	
	@Column(name = "TIPO_UTILIZZO")
	private String tipoUtilizzo;
	
	@Column(name = "OFFERTA_COMMERCIALE")
	private String offertaCommerciale;	
	
	@Column(name = "TERRITORIO")
	private String territorio;
		
	@Column(name = "VALIDA_DA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date validaDa;
	
	@Column(name = "VALIDA_A")
	@Temporal(TemporalType.TIMESTAMP)
	private Date validaA;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Mandato getMandato() {
		return mandato;
	}

	public void setMandato(Mandato mandato) {
		this.mandato = mandato;
	}

	public String getDsp() {
		return dsp;
	}

	public void setDsp(String dsp) {
		this.dsp = dsp;
	}

	public String getTipoUtilizzo() {
		return tipoUtilizzo;
	}

	public void setTipoUtilizzo(String tipoUtilizzo) {
		this.tipoUtilizzo = tipoUtilizzo;
	}

	public String getOffertaCommerciale() {
		return offertaCommerciale;
	}

	public void setOffertaCommerciale(String offertaCommerciale) {
		this.offertaCommerciale = offertaCommerciale;
	}

	public String getTerritorio() {
		return territorio;
	}

	public void setTerritorio(String territorio) {
		this.territorio = territorio;
	}

	public Date getValidaDa() {
		return validaDa;
	}

	public void setValidaDa(Date validaDa) {
		this.validaDa = validaDa;
	}

	public Date getValidaA() {
		return validaA;
	}

	public void setValidaA(Date validaA) {
		this.validaA = validaA;
	}

}
