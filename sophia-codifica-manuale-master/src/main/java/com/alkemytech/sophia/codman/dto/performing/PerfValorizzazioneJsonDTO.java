package com.alkemytech.sophia.codman.dto.performing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PerfValorizzazioneJsonDTO {

    private String regola;
    private Esclusioni esclusioni;

    public PerfValorizzazioneJsonDTO() {
        super();
    }

    public PerfValorizzazioneJsonDTO(String regola, Esclusioni esclusioni) {
        this.regola = regola;
        this.esclusioni = esclusioni;
    }

    public String getRegola() {
        return regola;
    }

    public void setRegola(String regola) {
        this.regola = regola;
    }

    public Esclusioni getEsclusioni() {
        return esclusioni;
    }

    public void setEsclusioni(Esclusioni esclusioni) {
        this.esclusioni = esclusioni;
    }

    private class Esclusioni {

        private boolean bis;
        private Integer durataMin;
        private boolean pubDom;
        private boolean cedoleEsclusePM;
        private boolean aaddEsclusi;

        public Esclusioni() {
            super();
        }

        public Esclusioni(boolean bis, Integer durataMin, boolean pubDom, boolean cedoleEsclusePM, boolean aaddEsclusi) {
            super();
            this.bis = bis;
            this.durataMin = durataMin;
            this.pubDom = pubDom;
            this.cedoleEsclusePM = cedoleEsclusePM;
            this.aaddEsclusi = aaddEsclusi;
        }

        public boolean isBis() {
            return bis;
        }

        public void setBis(boolean bis) {
            this.bis = bis;
        }

        public Integer isDurataMin() {
            return durataMin;
        }

        public void setDurataMin(Integer durataMin) {
            this.durataMin = durataMin;
        }

        public boolean isPubDom() {
            return pubDom;
        }

        public void setPubDom(boolean pubDom) {
            this.pubDom = pubDom;
        }

        public boolean isCedoleEsclusePM() {
            return cedoleEsclusePM;
        }

        public void setCedoleEsclusePM(boolean cedoleEsclusePM) {
            this.cedoleEsclusePM = cedoleEsclusePM;
        }

        public boolean isAaddEsclusi() {
            return aaddEsclusi;
        }

        public void setAaddEsclusi(boolean aaddEsclusi) {
            this.aaddEsclusi = aaddEsclusi;
        }
    }

    @Override
    public String toString() {
        return "PerfValorizzazioneJsonDTO{" +
                "regola='" + regola + '\'' +
                ", esclusioni=" + esclusioni +
                '}';
    }
}
