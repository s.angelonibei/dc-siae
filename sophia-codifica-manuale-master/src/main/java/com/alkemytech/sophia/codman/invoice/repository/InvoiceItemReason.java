package com.alkemytech.sophia.codman.invoice.repository;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.codman.persistence.AbstractEntity;



@XmlRootElement
@Entity(name = "InvoiceItemReason")
@Table(name = "ITEM_INVOICE_REASON")
@NamedQueries({@NamedQuery(name = "InvoiceItemReason.GetAll", query = "SELECT x FROM InvoiceItemReason x")})
@SuppressWarnings("serial")

public class InvoiceItemReason extends AbstractEntity<String> {

    @Id
    @Column(name = "ID_ITEM_INVOICE_REASON", nullable = false)
    private Integer idItemInvoiceReason;

    @Column(name = "CODE", nullable = false)
    private String code;
    

    @Column(name = "DESCRIPTION", nullable = false)
    private String description;

    @Column(name = "CONTO_SAP", nullable = false)
    private String contoSap;
    
	@Override
	public String getId() {
		return idItemInvoiceReason.toString();
	}

	public Integer getIdItemInvoiceReason() {
		return idItemInvoiceReason;
	}

	public void setIdItemInvoiceReason(Integer idItemInvoiceReason) {
		this.idItemInvoiceReason = idItemInvoiceReason;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getContoSap() {
		return contoSap;
	}

	public void setContoSap(String contoSap) {
		this.contoSap = contoSap;
	}

}
