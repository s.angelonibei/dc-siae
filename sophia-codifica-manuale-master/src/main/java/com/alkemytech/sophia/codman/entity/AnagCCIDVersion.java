package com.alkemytech.sophia.codman.entity;

import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.codman.persistence.AbstractEntity;
import com.google.gson.annotations.Expose;

@XmlRootElement
@Entity(name="AnagCCIDVersion")
@Table(name="ANAG_CCID_VERSION")
@NamedQueries({@NamedQuery(name="AnagCCIDVersion.GetAll", query="SELECT x FROM AnagCCIDVersion x")})
@SuppressWarnings("serial")
 

public class AnagCCIDVersion extends AbstractEntity<String> {

	@Expose
	@Id
	@Column(name="ID_CCID_VERSION", nullable=false)
	private String idCCIDVersion;

	@Expose
	@Column(name="NAME", nullable=false)
	private String name;

	@Expose
	@Column(name="DESCRIPTION", nullable=false)
	private String description;

	@Expose
    @OneToMany
	@JoinColumn(name="ID_CCID_VERSION")
	private List<AnagCCIDParam> params;

	@Expose
    @Transient
	private Map<String, List<AnagCCIDParam>> paramsMap;	
	
	@Override
	public String getId() {
		return getIdCCIDVersion();
	}


	public String getIdCCIDVersion() {
		return idCCIDVersion;
	}


	public void setIdCCIDVersion(String idCCIDVersion) {
		this.idCCIDVersion = idCCIDVersion;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public List<AnagCCIDParam> getParams() {
		return params;
	}


	public void setParams(List<AnagCCIDParam> params) {
		this.params = params;
	}


	public Map<String, List<AnagCCIDParam>> getParamsMap() {
		return paramsMap;
	}


	public void setParamsMap(Map<String, List<AnagCCIDParam>> paramsMap) {
		this.paramsMap = paramsMap;
	}

}
	