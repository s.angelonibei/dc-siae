package com.alkemytech.sophia.codman.dto;



import java.math.BigDecimal;
import java.math.BigInteger;

public class InvoiceCCIDDTO {

    private String idCCID;
    private String idDsr;
    private BigDecimal totalValue;
    private BigDecimal totalValueOrigCurrency;
    private Long idCCIDmetadata;
    private String currency;
    private String ccidVersion;
    private Boolean ccidEncoded;
    private Boolean ccidEncodedProvisional;
    private Boolean ccidNotEncoded;
    private String invoiceStatus;
    private String idDsp;
    private String dspName;
    private String idUtilizationType;
    private String utilizationType;
    private Integer idCommercialOffer;
    private String commercialOffer;
    private String country;
    private String periodType;
    private Integer period;
    private String periodString;
    private Integer year;
    private BigDecimal invoiceAmount;
    private BigDecimal valoreResiduo;


    public InvoiceCCIDDTO(Object[] obj) {

        if (null != obj && obj.length == 23) {
            try {

               idCCID = (String) obj[0];
               idDsr = (String) obj[1];
               totalValue = (BigDecimal) obj[2];
               idCCIDmetadata = (Long) obj[3];
               currency = (String) obj[4];
               ccidVersion = (String) obj[5];
               ccidEncoded = (Boolean) obj[6];
               ccidEncodedProvisional = (Boolean) obj[7];
               ccidNotEncoded = (Boolean) obj[8];
               invoiceStatus = (String) obj[9];
               idDsp = (String) obj[10];
               dspName = (String) obj[11];
               idUtilizationType = (String) obj[12];
               utilizationType = (String) obj[13];
               idCommercialOffer = (Integer) obj[14];
               commercialOffer = (String) obj[15];
               country = (String) obj[16];
               periodType = (String) obj[17];
               period = (Integer) obj[18];
               periodString = (String) obj[19];
               year = (Integer) obj[20];
               invoiceAmount = (BigDecimal) obj[21];
               valoreResiduo =(BigDecimal) obj[22];

            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }
    

    public BigDecimal getInvoiceAmount() {
		return invoiceAmount;
	}


	public void setInvoiceAmount(BigDecimal invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}


	public String getIdCCID() {
        return idCCID;
    }

    public void setIdCCID(String idCCID) {
        this.idCCID = idCCID;
    }

    public String getIdDsr() {
        return idDsr;
    }

    public void setIdDsr(String idDsr) {
        this.idDsr = idDsr;
    }

    public BigDecimal getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(BigDecimal totalValue) {
        this.totalValue = totalValue;
    }

    public Long getIdCCIDmetadata() {
		return idCCIDmetadata;
	}

	public void setIdCCIDmetadata(Long idCCIDmetadata) {
		this.idCCIDmetadata = idCCIDmetadata;
	}

	public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCcidVersion() {
        return ccidVersion;
    }

    public void setCcidVersion(String ccidVersion) {
        this.ccidVersion = ccidVersion;
    }

    public Boolean getCcidEncoded() {
        return ccidEncoded;
    }

    public void setCcidEncoded(Boolean ccidEncoded) {
        this.ccidEncoded = ccidEncoded;
    }

    public Boolean getCcidEncodedProvisional() {
        return ccidEncodedProvisional;
    }

    public void setCcidEncodedProvisional(Boolean ccidEncodedProvisional) {
        this.ccidEncodedProvisional = ccidEncodedProvisional;
    }

    public Boolean getCcidNotEncoded() {
        return ccidNotEncoded;
    }

    public void setCcidNotEncoded(Boolean ccidNotEncoded) {
        this.ccidNotEncoded = ccidNotEncoded;
    }

    public String getIdDsp() {
        return idDsp;
    }

    public void setIdDsp(String idDsp) {
        this.idDsp = idDsp;
    }

    public String getDspName() {
        return dspName;
    }

    public void setDspName(String dspName) {
        this.dspName = dspName;
    }

    public String getUtilizationType() {
        return utilizationType;
    }

    public void setUtilizationType(String utilizationType) {
        this.utilizationType = utilizationType;
    }

    public String getIdUtilizationType() {
        return idUtilizationType;
    }

    public void setIdUtilizationType(String idUtilizationType) {
        this.idUtilizationType = idUtilizationType;
    }

    public String getCommercialOffer() {
        return commercialOffer;
    }

    public void setCommercialOffer(String commercialOffer) {
        this.commercialOffer = commercialOffer;
    }

    public Integer getIdCommercialOffer() {
        return idCommercialOffer;
    }

    public void setIdCommercialOffer(Integer idCommercialOffer) {
        this.idCommercialOffer = idCommercialOffer;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPeriodType() {
        return periodType;
    }

    public void setPeriodType(String periodType) {
        this.periodType = periodType;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public String getPeriodString() {
        return periodString;
    }

    public void setPeriodString(String periodString) {
        this.periodString = periodString;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }


	public String getInvoiceStatus() {
		return invoiceStatus;
	}

	public void setInvoiceStatus(String invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}

	public BigDecimal getTotalValueOrigCurrency() {
		return totalValueOrigCurrency;
	}

	public void setTotalValueOrigCurrency(BigDecimal totalValueOrigCurrency) {
		this.totalValueOrigCurrency = totalValueOrigCurrency;
	}


	public BigDecimal getValoreResiduo() {
		return valoreResiduo;
	}


	public void setValoreResiduo(BigDecimal valoreResiduo) {
		this.valoreResiduo = valoreResiduo;
	}
	
	
    
}
