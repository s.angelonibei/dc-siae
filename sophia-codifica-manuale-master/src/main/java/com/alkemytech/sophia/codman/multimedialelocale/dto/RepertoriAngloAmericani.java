
package com.alkemytech.sophia.codman.multimedialelocale.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RepertoriAngloAmericani {

    @SerializedName("tipo")
    @Expose
    private String tipo;
    @SerializedName("puntiDem")
    @Expose
    private Double puntiDem;
    @SerializedName("puntiDrm")
    @Expose
    private Double puntiDrm;
    @SerializedName("repertorio")
    @Expose
    private String repertorio;
    @SerializedName("numeroRecord")
    @Expose
    private Integer numeroRecord;
    @SerializedName("numeroUtilizzazioni")
    @Expose
    private Integer numeroUtilizzazioni;
    @SerializedName("descrizioneRepertorio")
    @Expose
    private String descrizioneRepertorio;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Double getPuntiDem() {
        return puntiDem;
    }

    public void setPuntiDem(Double puntiDem) {
        this.puntiDem = puntiDem;
    }

    public Double getPuntiDrm() {
        return puntiDrm;
    }

    public void setPuntiDrm(Double puntiDrm) {
        this.puntiDrm = puntiDrm;
    }

    public String getRepertorio() {
        return repertorio;
    }

    public void setRepertorio(String repertorio) {
        this.repertorio = repertorio;
    }

    public Integer getNumeroRecord() {
        return numeroRecord;
    }

    public void setNumeroRecord(Integer numeroRecord) {
        this.numeroRecord = numeroRecord;
    }

    public Integer getNumeroUtilizzazioni() {
        return numeroUtilizzazioni;
    }

    public void setNumeroUtilizzazioni(Integer numeroUtilizzazioni) {
        this.numeroUtilizzazioni = numeroUtilizzazioni;
    }

    public String getDescrizioneRepertorio() {
        return descrizioneRepertorio;
    }

    public void setDescrizioneRepertorio(String descrizioneRepertorio) {
        this.descrizioneRepertorio = descrizioneRepertorio;
    }

}
