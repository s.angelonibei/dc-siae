package com.alkemytech.sophia.codman.dto.performing;

import java.math.BigDecimal;
import java.util.Date;

public class BonificaDTO {
	private Date dataBonifica;
	private Long recordTotali;
	private Long recordNonAggiornatiManifestazioni;
	private Long recordNonAggiornatiMovimentoContabile;
	private Long recordNonAggiornatiDirettore;
	private Long recordNonAggiornatiEventiPagati;
	private Long recordNonAggiornatiProgrammaMusicale;
	private Long recordInseriti;
	private Long recordAggiornati;
	private Long recordIdentici;

	public BonificaDTO() {
		super();
	}

	public BonificaDTO(Date dataBonifica, Long recordTotali, Long recordNonAggiornatiManifestazioni,
			Long recordNonAggiornatiMovimentoContabile, Long recordNonAggiornatiDirettore,
			Long recordNonAggiornatiEventiPagati, Long recordNonAggiornatiProgrammaMusicale, Long recordInseriti,
			Long recordAggiornati, Long recordIdentici) {
		super();
		this.dataBonifica = dataBonifica;
		this.recordTotali = recordTotali;
		this.recordNonAggiornatiManifestazioni = recordNonAggiornatiManifestazioni;
		this.recordNonAggiornatiMovimentoContabile = recordNonAggiornatiMovimentoContabile;
		this.recordNonAggiornatiDirettore = recordNonAggiornatiDirettore;
		this.recordNonAggiornatiEventiPagati = recordNonAggiornatiEventiPagati;
		this.recordNonAggiornatiProgrammaMusicale = recordNonAggiornatiProgrammaMusicale;
		this.recordInseriti = recordInseriti;
		this.recordAggiornati = recordAggiornati;
		this.recordIdentici = recordIdentici;
	}

	public BonificaDTO(Object[] row) {
		super();
		this.dataBonifica = (Date) row[0];
		this.recordTotali = (Long) row[1];
		if (row[2] != null) {
			this.recordNonAggiornatiManifestazioni = ((BigDecimal) row[2]).longValue();
		}
		if (row[3] != null) {
			this.recordNonAggiornatiMovimentoContabile = ((BigDecimal) row[3]).longValue();
		}
		if (row[4] != null) {
			this.recordNonAggiornatiDirettore = ((BigDecimal) row[4]).longValue();
		}

		if (row[5] != null) {
			this.recordNonAggiornatiEventiPagati = ((BigDecimal) row[5]).longValue();
		}

		if (row[6] != null) {
			this.recordNonAggiornatiProgrammaMusicale = ((BigDecimal) row[6]).longValue();
		}

		if (row[7] != null) {
			this.recordInseriti = ((BigDecimal) row[7]).longValue();
		}

		if (row[8] != null) {
			this.recordAggiornati = ((BigDecimal) row[8]).longValue();
		}

		if (row[9] != null) {
			this.recordIdentici = ((BigDecimal) row[9]).longValue();
		}
	}

	public Date getDataBonifica() {
		return dataBonifica;
	}

	public void setDataBonifica(Date dataBonifica) {
		this.dataBonifica = dataBonifica;
	}

	public Long getRecordTotali() {
		return recordTotali;
	}

	public void setRecordTotali(Long recordTotali) {
		this.recordTotali = recordTotali;
	}

	public Long getRecordNonAggiornatiManifestazioni() {
		return recordNonAggiornatiManifestazioni;
	}

	public void setRecordNonAggiornatiManifestazioni(Long recordNonAggiornatiManifestazioni) {
		this.recordNonAggiornatiManifestazioni = recordNonAggiornatiManifestazioni;
	}

	public Long getRecordNonAggiornatiMovimentoContabile() {
		return recordNonAggiornatiMovimentoContabile;
	}

	public void setRecordNonAggiornatiMovimentoContabile(Long recordNonAggiornatiMovimentoContabile) {
		this.recordNonAggiornatiMovimentoContabile = recordNonAggiornatiMovimentoContabile;
	}

	public Long getRecordNonAggiornatiDirettore() {
		return recordNonAggiornatiDirettore;
	}

	public void setRecordNonAggiornatiDirettore(Long recordNonAggiornatiDirettore) {
		this.recordNonAggiornatiDirettore = recordNonAggiornatiDirettore;
	}

	public Long getRecordNonAggiornatiEventiPagati() {
		return recordNonAggiornatiEventiPagati;
	}

	public void setRecordNonAggiornatiEventiPagati(Long recordNonAggiornatiEventiPagati) {
		this.recordNonAggiornatiEventiPagati = recordNonAggiornatiEventiPagati;
	}

	public Long getRecordNonAggiornatiProgrammaMusicale() {
		return recordNonAggiornatiProgrammaMusicale;
	}

	public void setRecordNonAggiornatiProgrammaMusicale(Long recordNonAggiornatiProgrammaMusicale) {
		this.recordNonAggiornatiProgrammaMusicale = recordNonAggiornatiProgrammaMusicale;
	}

	public Long getRecordInseriti() {
		return recordInseriti;
	}

	public void setRecordInseriti(Long recordInseriti) {
		this.recordInseriti = recordInseriti;
	}

	public Long getRecordAggiornati() {
		return recordAggiornati;
	}

	public void setRecordAggiornati(Long recordAggiornati) {
		this.recordAggiornati = recordAggiornati;
	}

	public Long getRecordIdentici() {
		return recordIdentici;
	}


	public void setRecordIdentici(Long recordIdentici) {
		this.recordIdentici = recordIdentici;
	}

}
