package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DsrProcessingSubmitRequestDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<DsrProcessingDTO> dsrs;

	public List<DsrProcessingDTO> getDsrs() {
		return dsrs;
	}

	public void setDsrs(List<DsrProcessingDTO> dsrs) {
		this.dsrs = dsrs;
	}

}
