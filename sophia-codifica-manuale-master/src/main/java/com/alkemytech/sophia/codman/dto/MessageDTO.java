package com.alkemytech.sophia.codman.dto;

public class MessageDTO {
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MessageDTO(String message) {
        this.message = message;
    }
}
