package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.codman.service.dsrmetadata.period.DsrPeriod;

@XmlRootElement
public class UnidentifiedLoadingSearchResultDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String dsr;

	private String dsp;
	private String dspName;
	private String dspPath;

	private String utilizationType;
	private String utilizationName;

	private String commercialOffer;
	private String commercialOfferName;

	private DsrPeriod dsrPeriod;

	private String country;
	private String countryName;

	private UnidentifiedLoadingStateDTO status;

	//R1
	private UnidentifiedLoadingStateDTO codificatoStatus;
	//

	public String getDsr() {
		return dsr;
	}

	public void setDsr(String dsr) {
		this.dsr = dsr;
	}

	public String getDsp() {
		return dsp;
	}

	public void setDsp(String dsp) {
		this.dsp = dsp;
	}

	public String getDspName() {
		return dspName;
	}

	public void setDspName(String dspName) {
		this.dspName = dspName;
	}

	public String getUtilizationType() {
		return utilizationType;
	}

	public void setUtilizationType(String utilizationType) {
		this.utilizationType = utilizationType;
	}

	public String getUtilizationName() {
		return utilizationName;
	}

	public void setUtilizationName(String utilizationName) {
		this.utilizationName = utilizationName;
	}

	public String getCommercialOffer() {
		return commercialOffer;
	}

	public void setCommercialOffer(String commercialOffer) {
		this.commercialOffer = commercialOffer;
	}

	public String getCommercialOfferName() {
		return commercialOfferName;
	}

	public void setCommercialOfferName(String commercialOfferName) {
		this.commercialOfferName = commercialOfferName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public DsrPeriod getDsrPeriod() {
		return dsrPeriod;
	}

	public void setDsrPeriod(DsrPeriod dsrPeriod) {
		this.dsrPeriod = dsrPeriod;
	}

	public UnidentifiedLoadingStateDTO getStatus() {
		return status;
	}

	public void setStatus(UnidentifiedLoadingStateDTO status) {
		this.status = status;
	}

	public String getDspPath() {
		return dspPath;
	}

	public void setDspPath(String dspPath) {
		this.dspPath = dspPath;
	}

	//R1
	public void setCodificatoStatus(UnidentifiedLoadingStateDTO codificatoStatus){ this.codificatoStatus = codificatoStatus; }

	public UnidentifiedLoadingStateDTO getCodificatoStatus(){return codificatoStatus;}
	//
}
