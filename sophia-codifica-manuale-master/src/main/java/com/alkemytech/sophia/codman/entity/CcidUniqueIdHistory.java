package com.alkemytech.sophia.codman.entity;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.GsonBuilder;

@XmlRootElement
@Entity(name="CcidUniqueIdHistory")
@Table(name="CCID_UNIQUE_ID_HISTORY")
public class CcidUniqueIdHistory {
	
	@Id
	@Column(name="IDDSR", nullable=false)
	private String idDsr;

	@Column(name="ID", nullable=false)
	private Long id;

	@Column(name="IDCCID", nullable=true)
	private BigInteger idCcid;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIdDsr() {
		return idDsr;
	}

	public void setIdDsr(String idDsr) {
		this.idDsr = idDsr;
	}

	public BigInteger getIdCcid() {
		return idCcid;
	}

	public void setIdCcid(BigInteger idCcid) {
		this.idCcid = idCcid;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}	
}
