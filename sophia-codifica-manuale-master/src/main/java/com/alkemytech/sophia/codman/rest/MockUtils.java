package com.alkemytech.sophia.codman.rest;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import com.alkemytech.sophia.codman.dto.DsrProcessingDTO;
import com.alkemytech.sophia.codman.dto.DsrProcessingStateDTO;
import com.alkemytech.sophia.codman.dto.UnidentifiedLoadingSearchResultDTO;
import com.alkemytech.sophia.codman.dto.UnidentifiedLoadingStateDTO;
import com.alkemytech.sophia.codman.service.dsrmetadata.period.DsrPeriod;

public class MockUtils {
	
	private static final String[] dsrs = {"2016T1_armonia-downloads-gb-ddex", "2016T1_armonia-downloads-it-ddex", "7Digital_PDS_20161001_20161231_AT", "7Digital_PDS_20161001_20161231_DE", "7Digital_PDS_20170701_20170930_BE_SD_BC_A_0001", "7Digital_PDS_20170701_20170930_ES_SD_BC_B_0001", "7Digital_PDS_20170701_20170930_ES_SD_BC_B_0002", "7Digital_PDS_20180701_20180930_IT_SD", "Amazon-CLD-FNONPUR-DDEX-IT-2016-12", "Amazon-CLD-FPLAY-DDEX-ES-2017-01", "Amazon-Sales-DDEX-AT-2016-01", "Amazon-Sales-DDEX-AT-2016-02", "Amazon-Sales-DDEX-AT-2016-10", "Amazon-Sales-DDEX-AT-2017-01", "Amazon-Sales-DDEX-CH-2016-01", "Amazon-Sales-DDEX-CH-2016-02", "Amazon-Sales-DDEX-DE-2016-01", "Amazon-Sales-DDEX-DE-2016-02", "Amazon-Sales-DDEX-ES-2016-01", "Amazon-Sales-DDEX-ES-2016-02", "Amazon-Sales-DDEX-ES-2016-11", "Amazon-Sales-DDEX-ES-2016-12", "Amazon-Sales-DDEX-FR-2016-01", "Amazon-Sales-DDEX-FR-2016-02", "Amazon-Sales-DDEX-GB-2016-01", "Amazon-Sales-DDEX-GB-2016-02", "Amazon-Sales-DDEX-GB-2016-10", "Amazon-Sales-DDEX-IT-2016-01", "Amazon-Sales-DDEX-IT-2016-02", "Amazon-Sales-DDEX-IT-2016-12", "Amazon-Sales-DDEX-IT-2017-07", "ARMONIA_2016_Q4_DE", "AUT_DSR42_MUSIC_201601", "AUT_DSR42_MUSIC_201604", "AUT_DSR42_MUSIC_201607", "AUT_DSR42_MUSIC_201608", "AUT_DSR42_MUSIC_201609", "AUT_DSR42_MUSIC_201611", "AUT_DSR42_MUSIC_201612", "AUT_DSR42_PRECUTRINGTONE_201608", "AUT_DSR42_PRECUTRINGTONE_201612", "BEATPORT_PDS_20150701_20150930_BE", "BEATPORT_PDS_20150701_20150930_BE_BC0001", "BEATPORT_PDS_20161001_20161231_AT", "BEATPORT_PDS_20161001_20161231_DE", "BMATCHER_YouTube_Usage-AdSupport-PremiumMusic-HighRevenue_2017-Q3_FR_1of1_20171129T012700", "BMATCHER_YouTube_Usage-AdSupport-PremiumMusic-HighRevenue_2017-Q3_GB_1of1_20171129T012700", "BMATCHER_YouTube_Usage-AdSupport-PremiumMusic-HighRevenue_2017-Q3_IT_1of1_20171129T012700", "DDEX_SIAE-GB-Premier-Extra_20150701_20150930", "DDEX_SIAE-IT-Premier-Extra_20160101_20160331", "DDEX_SIAE-IT-Radio-Plus_20160101_20160331", "DDEX_SIAE-IT-WIND-Bundle_20160101_20160331", "DeezerWorldwidePremiumPlusStandaloneIOSnewprice_20170101_20170131_IT", "DeezerWorldwidePremiumPlusStandaloneIOSnewprice_20170201_20170228_IT", "DeezerWorldwidePremiumPlusStandaloneIOSnewprice_20170301_20170331_IT", "DeezerWorldwidePremiumPlusStandaloneIOSnewprice_20170401_20170430_IT", "DeezerWorldwidePremiumPlusStandaloneIOSnewprice_20170501_20170531_IT", "DeezerWorldwidePremiumPlusStandaloneIOSnewprice_20170601_20170630_IT", "DeezerWorldwidePremiumPlusStandaloneIOSnewprice_20170701_20170731_IT", "DeezerWorldwidePremiumPlusStandaloneIOSnewprice_20170801_20170831_IT", "DeezerWorldwidePremiumPlusStandaloneIOSnewprice_20170901_20170930_IT", "DeezerWorldwidePremiumPlusStandaloneIOSnewprice_20171001_20171031_IT", "DeezerWorldwidePremiumPlusStandaloneIOSnewprice_20171101_20171130_IT", "DeezerWorldwidePremiumPlusStandaloneIOSnewprice_20171201_20171231_IT", "DeezerWorldwidePremiumPlusStandaloneIOS_20170101_20170131_IT", "DeezerWorldwidePremiumPlusStandaloneIOS_20170201_20170228_IT", "DeezerWorldwidePremiumPlusStandaloneIOS_20170301_20170331_IT", "DeezerWorldwidePremiumPlusStandaloneIOS_20170401_20170430_IT", "DeezerWorldwidePremiumPlusStandaloneIOS_20170501_20170531_IT", "DeezerWorldwidePremiumPlusStandaloneIOS_20170601_20170630_IT", "DeezerWorldwidePremiumPlusStandaloneIOS_20170701_20170731_IT", "DeezerWorldwidePremiumPlusStandaloneIOS_20170801_20170831_IT", "DeezerWorldwidePremiumPlusStandaloneIOS_20170901_20170930_IT", "DeezerWorldwidePremiumPlusStandaloneIOS_20171001_20171031_IT", "DeezerWorldwidePremiumPlusStandaloneIOS_20171101_20171130_IT", "DeezerWorldwidePremiumPlusStandaloneIOS_20171201_20171231_IT", "DeezerWorldwidePremiumPlusStandalone_20170101_20170131_IT", "DeezerWorldwidePremiumPlusStandalone_20170201_20170228_IT", "DeezerWorldwidePremiumPlusStandalone_20170301_20170331_IT", "DeezerWorldwidePremiumPlusStandalone_20170401_20170430_IT", "DeezerWorldwidePremiumPlusStandalone_20170501_20170531_IT", "DeezerWorldwidePremiumPlusStandalone_20170601_20170630_IT", "DeezerWorldwidePremiumPlusStandalone_20170701_20170731_IT", "DeezerWorldwidePremiumPlusStandalone_20170801_20170831_IT", "DeezerWorldwidePremiumPlusStandalone_20170901_20170930_IT", "DeezerWorldwidePremiumPlusStandalone_20171001_20171031_IT", "DeezerWorldwidePremiumPlusStandalone_20171101_20171130_IT", "DeezerWorldwidePremiumPlusStandalone_20171201_20171231_IT", "DEU_DSR42_MUSIC_201607", "DEU_DSR42_MUSIC_201608", "DEU_DSR42_MUSIC_201609", "DEU_DSR42_MUSIC_201611", "DEU_DSR42_MUSIC_201612", "DEU_DSR42_MUSIC_201807", "DEU_DSR42_PRECUTRINGTONE_201608", "DEU_DSR42_PRECUTRINGTONE_201612", "DNK_DSR42_MUSIC_201612", "DSR_BMAT_YouTube_AdSupport-music_2017-Q1_ES_1of1_20170602T183700", "DSR_BMAT_YouTube_AdSupport-music_2017-Q1_IT_1of1_20170602T183700", "DSR_BMAT_YouTube_AdSupport-music_2017-Q2_IT_1of1_20170708T082933"};
	private static final String[] dsps = {"7digital", "amazon", "applemusic", "beatport", "beleive", "deezer", "googleplaypayasyougo", "googleplaysubscriptions", "guvera", "itunes", "reclami", "recisio", "rhapsody", "spotify", "vevo", "xboxmusic", "youtube(trimestrali)", "youtubemasterlist"};
	private static final String[] dspNames = {"7 digital", "amazon", "apple music", "beatport", "beleive", "deezer", "google play pay as you go", "google play subscriptions", "guvera", "itunes", "recisio", "reclami", "rhapsody", "spotify", "vevo", "xbox music", "youtube (trimestrali)", "youtube masterlist"};
	private static final String[] utilizationTypes = {"Cloud", "DWN", "MASTERLIST", "STRM_AD", "STRM_GEN", "STRM_SUB", "YOUTUBE"};
	private static final String[] utilizationNames= {"Cloud", "Download", "Master list", "Streaming AD", "Streaming generico", "Streaming in abbonamento", "Streaming Youtube"};
	private static final String[] commercialOffers = {"50", "59", "60", "61", "164", "173", "174", "175", "215", "225", "51", "52", "53", "54", "55", "56", "57", "58", "62", "63", "64", "65", "66", "67", "68", "69", "70", "71", "72", "73", "75", "76", "77", "78", "79", "80", "81", "82", "84", "85", "86", "87", "88", "89", "90", "91", "92", "93", "94", "97", "98", "99", "100", "102", "103", "105", "106", "107", "108", "109", "110", "112", "113", "114", "115", "116", "117", "118", "119", "120", "121", "122", "123", "124", "125", "126", "127", "128", "129", "130", "131", "132", "133", "135", "136", "137", "138", "139", "140", "141", "142", "144", "145", "146", "148", "149", "150", "151", "152", "153"};
	private static final String[] commercialOfferNames = {"7Digital_PDS", "7Digital_PCS", "Amazon-CLD-FNONPUR-DDEX", "Amazon-CLD-FPLAY-DDEX", "Amazon-CLD-FPREV-DDEX", "Amazon-CLD-FPUR-DDEX", "Amazon-CLD-SPLAY-DDEX", "Amazon-CLD-SPREV-DDEX", "Amazon-CLD-SPUR-DDEX", "Amazon-Sales-DDEX", "Amazon-Prime-DDEX-OnDemandStream", "HOLIDAYPRICEDROP2018", "ALLDEVICESMONTHLY", "Amazon-Prime-DDEX-NonInteractiveStream", "Trial", "Carrier3", "Carrier4", "Family", "Individual", "IndividualAnnual", "Student", "BEATPORT_PDS", "BEATPORT_PDSQ", "MFS", "MOD", "SMR", "3SEPremiumPlusStandalone", "3UKPremiumPlusStandalone", "AuchanExtendedTB3months", "BMWESPremiumPlus12months", "BMWESPremiumPlusTB1month", "BMWFRPremiumPlus12months", "BMWFRPremiumPlusTB1month", "BMWITPremiumPlus12months", "BMWITPremiumPlusTB1month", "BMWNLPremiumPlus12months", "BMWNLPremiumPlusTB1month", "BMWUKPremiumPlus12months", "BMWUKPremiumPlusTB1month", "BOHardBundlePremiumPlus", "BOHardBundlePremiumPlusPayment", "bose-DeezerPremiumPlus", "DeezerPremiumbundletango", "DeezerPremiumplusbundletango", "DeezerPremiumplusupselltango", "DeezerUpsellPremiumPlus", "DeezerWorldwidePremiumPlusStandalone", "DeezerWorldwidePremiumPlusStandaloneIOS", "DeezerWorldwidePremiumPlusStandaloneIOSnewprice", "DeezerWorldwidePremiumStandalone", "DNAFIPremiumPlusStandalone", "DNAFIPremiumPlusStandalonediscountprice", "FastwebITPremiumPlusHardbundlefirstyear", "FastwebITPremiumPlusHardbundlepost12months", "OrangeBundlePremiumPlus", "OrangeESPremiumPlusExtendedTrial6months", "OrangeESPremiumPlusNewStandalone", "OrangeESPremiumPlusStandalone", "OrangeFRBundlePremiumPlus-FamilyOffer", "OrangeFRHardBundlePremiumPlusRetentionOffer", "OrangeFRpremiumPlusdiscount33month-conversion", "OrangeFRPremiumPlusRetentionOffer-Familyoffer", "OrangeFRStandalonediscount33months-newsubs", "OrangeFRStandalonePremiumPlus-Familyoffer", "Orangepremiumplusbundlebelgacom", "OrangeROPremiumPlusBundle", "OrangeROPremiumPlusStandalone", "OrangeSoftBundlePremiumPlus", "Orangestandalonepremium", "Orangestandalonepremiumplus", "OrangeUKBundlePremiumMobile", "OrangeUKStandalonePremiumPlus", "PremiumPlusOrangePLBundle", "PremiumPlusOrangePLStandalone", "PremiumPlusTMobileATBundle", "PremiumPlusTMobileATStandalone", "PremiumPlusTMobileNLSoftBundle", "PremiumPlusTMobileNLStandalone", "Samsung3months", "SKYHardbundlePremiumPlus12months", "Sonos-PremiumPlusXmas", "sonos_biyearly-DeezerElite", "sonos_monthly-DeezerElite", "sonos_yearly-DeezerElite", "StandaloneOrangeFRpremiumPlusDiscount16month", "StandaloneOrangeFRpremiumPlusDiscount52month", "StudentOffer5DD2months", "SummerOffer0993months", "SwisscomCHPremiumPlusDiscountPrice6months", "SwisscomCHPremiumPlusPostpaidStandalone", "T-HrvatskiTelekomPremiumPlusHardBundle", "T-HrvatskiTelekomPremiumPlusStandalone", "T-MobileCZPremiumPlusHardBundle1", "T-MobileCZPremiumPlusHardBundle2", "T-MobileCZPremiumPlusStandalone", "Tele2EEPostpaidHardBundlePremiumPlus", "Tele2EEStandalonePremiumPlus", "Tele2LTPostpaidHardBundlePremiumPlus", "Tele2LTStandalonePremiumPlus", "TelekomSKPremiumDeepHardBundle"};
	
	private static final String[] years = {"2016", "2017", "2018", "2015", "2019", "2013", "2014"};
	private static final String[] periods = {"1", "4", "3", "12", "2", "11", "10", "7", "8", "9", "5", "6"};
	private static final String[] periodTypes = {"quarter", "month"};
	
	private static final String[] countries = {"ABW", "AFG", "AGO", "AIA", "ALA", "ALB", "AND", "ANT", "ARE", "ARG", "ARM", "ASM", "ATA", "ATF", "ATG", "AUS", "AUT", "AZE", "BDI", "BEL", "BEN", "BFA", "BGD", "BGR", "BHR", "BHS", "BIH", "BLM", "BLR", "BLZ", "BMU", "BOL", "BRA", "BRB", "BRN", "BTN", "BVT", "BWA", "CAF", "CAN", "CCK", "CHE", "CHL", "CHN", "CIV", "CMR", "COD", "COG", "COK", "COL", "COM", "CPV", "CRI", "CUB", "CXR", "CYM", "CYP", "CZE", "DEU", "DJI", "DMA", "DNK", "DOM", "DZA", "ECU", "EGY", "ERI", "ESH", "ESP", "EST", "ETH", "FIN", "FJI", "FLK", "FRA", "FRO", "FSM", "GAB", "GBR", "GEO", "GGY", "GHA", "GIB", "GIN", "GLP", "GMB", "GNB", "GNQ", "GRC", "GRD", "GRL", "GTM", "GUF", "GUM", "GUY", "HKG", "HMD", "HND", "HRV", "HTI"};
	private static final String[] countryNames = {"Aruba", "Afghanistan", "Angola", "Anguilla", "Aland Islands", "Albania", "Andorra", "Netherlands Antilles", "United Arab Emirates", "Argentina", "Armenia", "American Samoa", "Antarctica", "French Southern Territories", "Antigua and Barbuda", "Australia", "Austria", "Azerbaijan", "Burundi", "Belgium", "Benin", "Burkina Faso", "Bangladesh", "Bulgaria", "Bahrain", "Bahamas", "Bosnia and Herzegovina", "Saint-Barth?lemy", "Belarus", "Belize", "Bermuda", "Bolivia", "Brazil", "Barbados", "Brunei Darussalam", "Bhutan", "Bouvet Island", "Botswana", "Central African Republic", "Canada", "Cocos (Keeling) Islands", "Switzerland", "Chile", "China", "Costa d'Avorio", "Cameroon", "Democratic Republic of the Congo", "Congo", "Cook Islands", "Colombia", "Comoros", "Cape Verde", "Costa Rica", "Cuba", "Christmas Island", "Cayman Islands", "Cyprus", "Czech Republic", "Germany", "Djibouti", "Dominica", "Denmark", "Dominican Republic", "Algeria", "Ecuador", "Egypt", "Eritrea", "Western Sahara", "Spain", "Estonia", "Ethiopia", "Finland", "Fiji", "Falkland Islands", "France", "Faroe Islands", "Micronesia", "Gabon", "United Kingdom", "Georgia", "Guernsey", "Ghana", "Gibraltar", "Guinea", "Guadeloupe", "Gambia", "Guinea-Bissau", "Equatorial Guinea", "Greece", "Grenada", "Greenland", "Guatemala", "French Guiana", "Guam", "Guyana", "Hong Kong", "Heard Island and Mcdonald Islands", "Honduras", "Croatia", "Haiti"};
	
//	static final DsrProcessingStateDTO[] processingStatuses = {
//			new DsrProcessingStateDTO(1l,"Arrivato", true, true)
//			,new DsrProcessingStateDTO(2l,"In Lavorazione",false, false)
//			,new DsrProcessingStateDTO(3l,"Completato",true, true)
//			,new DsrProcessingStateDTO(4l,"Errore",true, false)
//	};
	

	
	private static final String[] errors = {"Wrong format","Processing Error",""};
	private static final String[] kbVersions = {"1","2","3","4"};
	
	private static int count =0;
	
	static DsrProcessingDTO randomDsrProcessingDTO() {
		DsrProcessingDTO dto = new DsrProcessingDTO();
		Random rnd = new Random();
		String dsr = dsrs[rnd.nextInt(dsrs.length)] + count++;
		int i = rnd.nextInt(dsps.length);
		String dsp = dsps[i];
		String dspName = dspNames[i];

		i = rnd.nextInt(utilizationTypes.length);
		String utilizationType = utilizationTypes[i];
		String utilizationTypeName = utilizationNames[i];

		i = rnd.nextInt(commercialOffers.length);
		String commercialOffer = commercialOffers[i];
		String commercialOfferName = commercialOfferNames[i];

		String year = years[rnd.nextInt(years.length)];
		String period = periods[rnd.nextInt(periods.length)];
		String periodType = periodTypes[1];

		i = rnd.nextInt(countries.length);
		String country = countries[i];
		String countryName = countryNames[i];

//		DsrProcessingStateDTO processingStatus = processingStatuses[rnd.nextInt(processingStatuses.length)];
		String errorType = errors[rnd.nextInt(errors.length)];
		String kbVersion = kbVersions[rnd.nextInt(kbVersions.length)];
		
		dto.setDsr(dsr);
		dto.setDsp(dsp);
		dto.setDspName(dspName);
		dto.setUtilizationType(utilizationType);
		dto.setUtilizationName(utilizationTypeName);
		dto.setCommercialOffer(commercialOffer);
		dto.setCommercialOfferName(commercialOfferName);
		
		DsrPeriod dsrPeriod = new DsrPeriod();
		dsrPeriod.setPeriod(period);
		dsrPeriod.setPeriodType(periodType);
		dsrPeriod.setYear(year);
		dto.setDsrPeriod(dsrPeriod);
		
		dto.setCountry(country);
		dto.setCountryName(countryName);
		
		
		dto.setDsrDeliveryDate(new Date());
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTime(dto.getDsrDeliveryDate());
	    calendar.add(Calendar.HOUR_OF_DAY, rnd.nextInt(5));
	    dto.setDsrProcessingStart(calendar.getTime());
	    calendar.add(Calendar.HOUR_OF_DAY, rnd.nextInt(5));
	    dto.setDsrProcessingEnd(calendar.getTime());
				
//		dto.setProcessingStatus(processingStatus);
		dto.setErrorType(errorType);
		dto.setKbVersion(kbVersion);

		return dto;
		
	}
	
//	static UnidentifiedLoadingSearchResultDTO randomUnidentifiedLoadingSearchResultDTO() {
//		UnidentifiedLoadingSearchResultDTO dto = new UnidentifiedLoadingSearchResultDTO();
//		Random rnd = new Random();
//		String dsr = dsrs[rnd.nextInt(dsrs.length)] + count++;
//		int i = rnd.nextInt(dsps.length);
//		String dsp = dsps[i];
//		String dspName = dspNames[i];
//
//		i = rnd.nextInt(utilizationTypes.length);
//		String utilizationType = utilizationTypes[i];
//		String utilizationTypeName = utilizationNames[i];
//
//		i = rnd.nextInt(commercialOffers.length);
//		String commercialOffer = commercialOffers[i];
//		String commercialOfferName = commercialOfferNames[i];
//
//		String year = years[rnd.nextInt(years.length)];
//		String period = periods[rnd.nextInt(periods.length)];
//		String periodType = periodTypes[1];
//
//		i = rnd.nextInt(countries.length);
//		String country = countries[i];
//		String countryName = countryNames[i];
//
//		
//		dto.setDsr(dsr);
//		dto.setDsp(dsp);
//		dto.setDspName(dspName);
//		dto.setUtilizationType(utilizationType);
//		dto.setUtilizationName(utilizationTypeName);
//		dto.setCommercialOffer(commercialOffer);
//		dto.setCommercialOfferName(commercialOfferName);
//		
//		DsrPeriod dsrPeriod = new DsrPeriod();
//		dsrPeriod.setPeriod(period);
//		dsrPeriod.setPeriodType(periodType);
//		dsrPeriod.setYear(year);
//		dto.setDsrPeriod(dsrPeriod);
//		
////		UnidentifiedLoadingStateDTO status = loadingStatuses[rnd.nextInt(loadingStatuses.length)];
////		dto.setStatus(status);
//		
//		dto.setCountry(country);
//		dto.setCountryName(countryName);
//		
//		return dto;
//		
//	}
	
	

}
