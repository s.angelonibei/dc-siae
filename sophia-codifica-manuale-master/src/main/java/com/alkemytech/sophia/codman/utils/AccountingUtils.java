package com.alkemytech.sophia.codman.utils;

import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;

public class AccountingUtils {
	
    public static String displayFormatted(Double currencyAmount) {

        if (currencyAmount!=null) {
            Locale currentLocale = new Locale("it", "IT");
            Locale.setDefault(currentLocale);
            Currency currentCurrency = Currency.getInstance(currentLocale);
            NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(currentLocale);

            String formattedNumber = currencyFormatter.format(currencyAmount);

            formattedNumber = formattedNumber.replace("€", "&euro;");

            return formattedNumber;

        }else
            return "";
    }    
    
    public static double round(double value, int places){

        if (places < 0) throw new IllegalArgumentException();

        long factor = (long)Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double)tmp / factor;

    }
    
    public static Long normalizePeriodoContabile(String mesePeriodo, String annoPeriodo){

        Long periodoContabile;

        if (mesePeriodo.length()==1)
            return Long.parseLong(annoPeriodo+"0"+mesePeriodo);
        else
            return Long.parseLong(annoPeriodo+mesePeriodo);
    }
    
    public static boolean isNumber(String s){
        try {
            double d = Double.parseDouble(s);
            return true;
        } catch(NumberFormatException nfe) {
            return false;
        }
    }
}
