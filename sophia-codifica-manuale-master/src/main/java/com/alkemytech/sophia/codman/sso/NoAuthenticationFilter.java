package com.alkemytech.sophia.codman.sso;

import com.alkemytech.sophia.codman.entity.RolePermission;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import javax.persistence.EntityManager;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

@Singleton
public class NoAuthenticationFilter extends DefaultAuthenticationModule implements Filter {
	
	private final Provider<EntityManager> entityManagerProvider;
	
	@Inject
	protected NoAuthenticationFilter(@Named("configuration") Properties configuration,
			@McmdbDataSource Provider<EntityManager> entityManagerProvider) {
		super(configuration);
		this.entityManagerProvider = entityManagerProvider;
	}
	
	@Override
	public void init(FilterConfig config) throws ServletException {

	}

	@Override
	public void destroy() {
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		final HttpServletRequest httpRequest = (HttpServletRequest) request;

		HttpSession httpSession = httpRequest.getSession(false);
		if (null == httpSession) { // session not found
			
			// create and initialize session
			httpSession = httpRequest.getSession(true);
			httpSession.setAttribute(SsoSessionAttrs.DENOMINAZIONE, getSsoDefaultUser());
			httpSession.setAttribute(SsoSessionAttrs.EMAIL, getSsoDefaultUser() + "@siae.it");
			httpSession.setAttribute(SsoSessionAttrs.PP_TIPO, "");
			httpSession.setAttribute(SsoSessionAttrs.PP_SEPRAG, "");
			httpSession.setAttribute(SsoSessionAttrs.PP_DESCRIZIONE, "");
			httpSession.setAttribute(SsoSessionAttrs.STATO_PASSWORD, "");
			httpSession.setAttribute(SsoSessionAttrs.TOKEN_ID, "");
			httpSession.setAttribute(SsoSessionAttrs.USERNAME, getSsoDefaultUser());
			httpSession.setAttribute(SsoSessionAttrs.GROUPS, getSsoDefaultGroups());
			httpSession.setAttribute(SsoSessionAttrs.LAST_CHECK, Long.valueOf(System.currentTimeMillis()));
			
			// inject user permission(s) into session
			final EntityManager entityManager = entityManagerProvider.get();
			final List<RolePermission> permissions = (List<RolePermission>) entityManager
				.createQuery("select x from RolePermission x where x.role in :role", RolePermission.class)
				.setParameter("role", getSsoDefaultGroups())
				.getResultList();
			for (RolePermission permission : permissions) {
				httpSession.setAttribute(permission.getPermission(), permission.getPermission());
			}

		} else { // session found
		
			final Long lastCheckTokenMillis = (Long) httpSession.getAttribute(SsoSessionAttrs.LAST_CHECK);
			if (null == lastCheckTokenMillis ||
					System.currentTimeMillis() - lastCheckTokenMillis > TimeUnit.MINUTES.toMillis(10)) {

				// update session
				httpSession.setAttribute(SsoSessionAttrs.LAST_CHECK, Long.valueOf(System.currentTimeMillis()));
			}
			
		}
				
		// pass the request along the filter chain
		chain.doFilter(request, response);
	}

}
