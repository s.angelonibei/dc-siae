package com.alkemytech.sophia.codman.rest.performing.service;

import com.alkemytech.sophia.codman.dto.NdmVoceFatturaDateLimitsDTO;

import java.util.List;

public interface EventiPagatiService {
    List<String> getAgenzie(String agenzie);
    List<String> getVociIncasso(String search);
    NdmVoceFatturaDateLimitsDTO getDateLimits();
    List<String> getSedi(String search);
}
