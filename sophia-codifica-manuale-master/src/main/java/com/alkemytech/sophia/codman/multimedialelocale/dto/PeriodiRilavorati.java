package com.alkemytech.sophia.codman.multimedialelocale.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

public class PeriodiRilavorati {
	private String periodo;
	private Date dataAcquisizioneSchemaRiparto;
	private Long reportDaRilavorare;
	private BigDecimal reportRilavorati;

	
	
	public PeriodiRilavorati() {
		super();
	}

	public PeriodiRilavorati(Object[] record) {
		periodo=(String) record[0];
		dataAcquisizioneSchemaRiparto=(Timestamp) record[1];
		reportDaRilavorare=(Long) record[2];
		reportRilavorati=(BigDecimal) record[3];
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public Date getDataAcquisizioneSchemaRiparto() {
		return dataAcquisizioneSchemaRiparto;
	}

	public void setDataAcquisizioneSchemaRiparto(Date dataAcquisizioneSchemaRiparto) {
		this.dataAcquisizioneSchemaRiparto = dataAcquisizioneSchemaRiparto;
	}

	public Long getReportDaRilavorare() {
		return reportDaRilavorare;
	}

	public void setReportDaRilavorare(Long reportDaRilavorare) {
		this.reportDaRilavorare = reportDaRilavorare;
	}

	public BigDecimal getReportRilavorati() {
		return reportRilavorati;
	}

	public void setReportRilavorati(BigDecimal reportRilavorati) {
		this.reportRilavorati = reportRilavorati;
	}

}
