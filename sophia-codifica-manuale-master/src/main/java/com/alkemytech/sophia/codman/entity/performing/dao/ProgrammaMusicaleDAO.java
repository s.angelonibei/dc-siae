package com.alkemytech.sophia.codman.entity.performing.dao;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.table;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;
import org.jooq.DSLContext;
import org.jooq.Record13;
import org.jooq.SelectQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.alkemytech.sophia.codman.dto.PerfEventiPagatiDTO;
import com.alkemytech.sophia.codman.dto.PerfUtilizzazioniDTO;
import com.alkemytech.sophia.codman.dto.ProgrammaMusicaleDTO;
import com.alkemytech.sophia.codman.dto.performing.AggiMcDTO;
import com.alkemytech.sophia.codman.dto.performing.FatturaDetailDTO;
import com.alkemytech.sophia.codman.dto.performing.PerfCodificaDTO;
import com.alkemytech.sophia.codman.dto.performing.PerfRiconcMvDTO;
import com.alkemytech.sophia.codman.dto.performing.RiconciliazioneImportiDTO;
import com.alkemytech.sophia.codman.entity.performing.AggiMc;
import com.alkemytech.sophia.codman.entity.performing.Cartella;
import com.alkemytech.sophia.codman.entity.performing.DirettoreEsecuzione;
import com.alkemytech.sophia.codman.entity.performing.EventiPagati;
import com.alkemytech.sophia.codman.entity.performing.ImportoRicalcolato;
import com.alkemytech.sophia.codman.entity.performing.Manifestazione;
import com.alkemytech.sophia.codman.entity.performing.MovimentazioneLogisticaPM;
import com.alkemytech.sophia.codman.entity.performing.MovimentoContabile;
import com.alkemytech.sophia.codman.entity.performing.MovimentoSiada;
import com.alkemytech.sophia.codman.entity.performing.NDMVoceFattura;
import com.alkemytech.sophia.codman.entity.performing.PerfMovimentoContabile;
import com.alkemytech.sophia.codman.entity.performing.ProgrammaMusicale;
import com.alkemytech.sophia.codman.entity.performing.TotaleRiconciliazioneImporti;
import com.alkemytech.sophia.codman.entity.performing.TracciamentoApplicativo;
import com.alkemytech.sophia.codman.entity.performing.TrattamentoPM;
import com.alkemytech.sophia.codman.entity.performing.Utilizzazione;
import com.alkemytech.sophia.codman.entity.performing.security.AggregatoVoce;
import com.alkemytech.sophia.codman.entity.performing.security.ReportPage;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.alkemytech.sophia.codman.rest.performing.Constants;
import com.alkemytech.sophia.codman.utils.DateUtils;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.google.inject.Provider;

/*import static com.alkemytech.sophia.codman.jooq.mcmdb_debug.tables.PerfEventiPagati.PERF_EVENTI_PAGATI;
import static com.alkemytech.sophia.codman.jooq.mcmdb_debug.tables.PerfNdmVoceFattura.PERF_NDM_VOCE_FATTURA;*/

@Repository("programmaMusicaleDAO")
public class ProgrammaMusicaleDAO implements IProgrammaMusicaleDAO {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
    private static final String TIPO_RICONCILIAZIONE_IMPORTO = "IMPORTO";
    private static final String TIPO_RICONCILIAZIONE_VOCE_INCASSO = "VOCE_INCASSO";
	private Provider<EntityManager> provider;
    private DSLContext jooq;

    @Inject
    public ProgrammaMusicaleDAO(Provider<EntityManager> provider,
                                @McmdbDataSource DSLContext jooq) {
        this.provider = provider;
        this.jooq = jooq;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void insertMovimentazioneLogisticaPM(MovimentazioneLogisticaPM movimentazioneLogisticaPM) {
        EntityManager entityManager = provider.get();
        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setName("rootTransaction");
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        entityManager.persist(movimentazioneLogisticaPM);
        entityManager.flush();
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void updatePMRientrati(ProgrammaMusicale programmaMusicale) {

        EntityManager entityManager = provider.get();
        List<ProgrammaMusicale> programmiRientrati = getProgrammaMusicale(
                programmaMusicale.getNumeroProgrammaMusicale());
        MovimentazioneLogisticaPM ultimaMovimentazioneLogisticaPM;
        MovimentazioneLogisticaPM nuovaMovimentazioneLogisticaPM;
        for (ProgrammaMusicale programma : programmiRientrati) {

            programma.setTotaleCedole(programmaMusicale.getTotaleCedole());
            programma.setTotaleDurataCedole(programmaMusicale.getTotaleDurataCedole());
            entityManager.merge(programma);

            ultimaMovimentazioneLogisticaPM = getUltimaMovimentazionePM(programma.getId());
            if (ultimaMovimentazioneLogisticaPM != null) {
                ultimaMovimentazioneLogisticaPM.setFlagAttivo('N');
                entityManager.merge(ultimaMovimentazioneLogisticaPM);
            }

            nuovaMovimentazioneLogisticaPM = new MovimentazioneLogisticaPM();
            nuovaMovimentazioneLogisticaPM.setFlagAttivo('Y');
            nuovaMovimentazioneLogisticaPM
                    .setIdProgrammaMusicale(ultimaMovimentazioneLogisticaPM.getIdProgrammaMusicale());
            nuovaMovimentazioneLogisticaPM.setStato(Constants.RITORNATO_DA_NEED);
            nuovaMovimentazioneLogisticaPM
                    .setFlagEsclusoRicalcolo(ultimaMovimentazioneLogisticaPM.getFlagEsclusoRicalcolo());
            nuovaMovimentazioneLogisticaPM
                    .setFlagEsclusoInvioNeed(ultimaMovimentazioneLogisticaPM.getFlagEsclusoInvioNeed());
            entityManager.persist(nuovaMovimentazioneLogisticaPM);

            entityManager.flush();

        }

    }

    public List<ProgrammaMusicale> getProgrammaMusicale(Long numeroProgrammaMusicale) {

        EntityManager entityManager = provider.get();
        List<ProgrammaMusicale> results;

        String query = "select c from ProgrammaMusicale c where c.numeroProgrammaMusicale=:numeroProgrammaMusicale";

        results = (List<ProgrammaMusicale>) entityManager.createQuery(query)
                .setParameter("numeroProgrammaMusicale", numeroProgrammaMusicale).getResultList();

        if (results == null)
            results = new ArrayList<ProgrammaMusicale>();

        return results;

    }

    public MovimentazioneLogisticaPM getUltimaMovimentazionePM(Long idProgrammaMusicale) {

        EntityManager entityManager = provider.get();
        List<MovimentazioneLogisticaPM> results = new ArrayList<MovimentazioneLogisticaPM>();

        String query = "select m from MovimentazioneLogisticaPM m where m.idProgrammaMusicale=:idProgrammaMusicale and m.flagAttivo=:flagAttivo";

        results = (List<MovimentazioneLogisticaPM>) entityManager.createQuery(query)
                .setParameter("idProgrammaMusicale", idProgrammaMusicale).setParameter("flagAttivo", 'Y')
                .getResultList();

        if (results != null && results.size() > 0)
            return results.get(0);
        else
            return null;

    }

    public long getCountTracciamentoPM(String dataIniziale, String dataFinale, Long numeroPM, String tipoDocumento,
                                       String seprag, String idEvento, String fattura, String reversale, String voceIncasso, String permesso,
                                       String locale, String codiceBALocale, String codiceSAPOrganizzatore, String organizzatore,
                                       String titoloOpera, String dataInizioEventoDa, String dataInizioEventoA, String direttoreEsecuzione,
                                       String supporto, String tipoProgrammi, String gruppoPrincipale) {
        EntityManager entityManager = provider.get();
        String query = null;

        List<ProgrammaMusicale> listaAllPM = new ArrayList<ProgrammaMusicale>();

        query = "select count(distinct mc.id) " + "FROM MovimentoContabile mc "
                + "LEFT JOIN ProgrammaMusicale pm on mc.idProgrammaMusicale=pm.id "
                + "LEFT JOIN Manifestazione ma on mc.idEvento=ma.id "
                + "LEFT JOIN EventiPagati e on mc.idEvento=e.idEvento "
                + "LEFT JOIN ImportoRicalcolato ir on mc.idEvento=ir.idEvento "
                + "LEFT JOIN DirettoreEsecuzione d on mc.idProgrammaMusicale=d.idProgrammaMusicale "
                + "where (:inizioPeriodoContabile is null or mc.contabilita>=:inizioPeriodoContabile) "
                + "and (:finePeriodoContabile is null or mc.contabilita<=:finePeriodoContabile) "
                + "and (:tipoDocumento is null or mc.tipoDocumentoContabile=:tipoDocumento) "
                + "and (:idEvento is null or mc.idEvento=:idEvento) "
                + "and (:fattura is null or mc.numeroFattura=:fattura) "
                + "and (:reversale is null or mc.reversale=:reversale) "
                + "and (:voceIncasso is null or mc.voceIncasso=:voceIncasso) "
                + "and (:permesso is null or mc.numeroPermesso=:permesso) "
                + "and (:numeroPM is null or mc.numProgrammaMusicale=:numeroPM) "
                + "and (:locale is null or ma.denominazioneLocale=:locale) "
                + "and (:codiceBALocale is null or ma.codiceBALocale=:codiceBALocale) "
                + "and (:codiceSAPOrganizzatore is null or ma.codiceSapOrganizzatore=:codiceSAPOrganizzatore) "
                + "and (:dataInizioEventoDa is null or ma.dataInizioEvento >= :dataInizioEventoDa) "
                + "and (:dataInizioEventoA is null or ma.dataInizioEvento <= :dataInizioEventoA) "
                + "and (:seprag is null or e.seprag=:seprag) "
                + "and (:titoloOpera is null or upper(ma.titolo) like upper(:titoloOpera)) "
                + "and (:supporto is null or pm.supportoPm=:supporto) "
                + "and (:tipoProgrammi is null or pm.tipoPm=:tipoProgrammi) "
                + "and (:gruppoPrincipale is null or pm.flagGruppoPrincipale=:gruppoPrincipale) "
                + "and (:partitaIva is null or ma.partitaIvaOrganizzatore=:partitaIva) "
                + "and (:codiceFiscale is null or ma.codiceFiscaleOrganizzatore=:codiceFiscale) "
                + "and (:nomeDirettore is null or d.direttoreEsecuzione= upper(:nomeDirettore)) "
                + "and (:cfDirettore is null or d.codiceFiscale= upper(:cfDirettore)) ";

        Query q = entityManager.createQuery(query);

        if (dataIniziale != null)
            q.setParameter("inizioPeriodoContabile", Long.parseLong(dataIniziale));
        else
            q.setParameter("inizioPeriodoContabile", null);

        if (dataFinale != null)
            q.setParameter("finePeriodoContabile", Long.parseLong(dataFinale));
        else
            q.setParameter("finePeriodoContabile", null);

        q.setParameter("tipoDocumento", tipoDocumento);
        q.setParameter("seprag", seprag);

        if (idEvento != null) {
            q.setParameter("idEvento", Long.parseLong(idEvento));
        } else {
            q.setParameter("idEvento", null);
        }

        if (fattura != null) {
            q.setParameter("fattura", Long.parseLong(fattura));
        } else {
            q.setParameter("fattura", null);
        }

        if (reversale != null) {
            q.setParameter("reversale", Long.parseLong(reversale));
        } else {
            q.setParameter("reversale", null);
        }

        q.setParameter("voceIncasso", voceIncasso);
        q.setParameter("permesso", permesso);

        if (numeroPM != null)
            q.setParameter("numeroPM", numeroPM.toString());
        else
            q.setParameter("numeroPM", null);

        q.setParameter("locale", locale);
        q.setParameter("codiceBALocale", codiceBALocale);
        q.setParameter("codiceSAPOrganizzatore", codiceSAPOrganizzatore);

        if (organizzatore != null && Character.isDigit(organizzatore.charAt(0))) {
            q.setParameter("partitaIva", organizzatore);
        } else {
            q.setParameter("partitaIva", null);
        }

        if (organizzatore != null && Character.isLetter(organizzatore.charAt(0))) {
            q.setParameter("codiceFiscale", organizzatore);
        } else {
            q.setParameter("codiceFiscale", null);
        }

        if (dataInizioEventoDa != null) {
            q.setParameter("dataInizioEventoDa",
                    DateUtils.stringToDate(dataInizioEventoDa, DateUtils.DATE_ITA_FORMAT_DASH));
        } else {
            q.setParameter("dataInizioEventoDa", null);
        }

        if (dataInizioEventoA != null) {
            q.setParameter("dataInizioEventoA", DateUtils.stringToDate(dataInizioEventoA, DateUtils.DATE_ITA_FORMAT_DASH));
        } else {
            q.setParameter("dataInizioEventoA", null);
        }
        if (titoloOpera != null)
            q.setParameter("titoloOpera", "%" + titoloOpera + "%");
        else
            q.setParameter("titoloOpera", titoloOpera);
        q.setParameter("tipoProgrammi", tipoProgrammi);
        q.setParameter("gruppoPrincipale", gruppoPrincipale);
        q.setParameter("supporto", supporto);

        if (direttoreEsecuzione != null && Character.isLetter(direttoreEsecuzione.charAt(6))) {
            q.setParameter("nomeDirettore", direttoreEsecuzione);
        } else {
            q.setParameter("nomeDirettore", null);
        }

        if (direttoreEsecuzione != null && Character.isDigit(direttoreEsecuzione.charAt(6))) {
            q.setParameter("cfDirettore", direttoreEsecuzione);
        } else {
            q.setParameter("cfDirettore", null);
        }

        Long result = (Long) q.getSingleResult();

        return result;

    }

    public List<ProgrammaMusicale> getTracciamentoPM(String dataIniziale, String dataFinale, Long numeroPM,
                                                     String tipoDocumento, String seprag, String idEvento, String fattura, String reversale, String voceIncasso,
                                                     String permesso, String locale, String codiceBALocale, String codiceSAPOrganizzatore, String organizzatore,
                                                     String titoloOpera, String dataInizioEventoDa, String dataInizioEventoA, String direttoreEsecuzione,
                                                     String supporto, String tipoProgrammi, String gruppoPrincipale, String order, Integer page) {

        EntityManager entityManager = provider.get();
        String query = null;

        List<ProgrammaMusicale> listaAllPM = new ArrayList<ProgrammaMusicale>();

        query = "select distinct mc.idProgrammaMusicale, mc.contabilita, mc.voceIncasso, mc.idEvento, mc.numeroPermesso, mc.numeroFattura, mc.reversale, mc.tipoDocumentoContabile, mc.numProgrammaMusicale, mc.importoTotDem, mc.importoValorizzato, mc.id "
                + "FROM MovimentoContabile mc " + "LEFT JOIN ProgrammaMusicale pm on mc.idProgrammaMusicale=pm.id "
                + "LEFT JOIN Manifestazione ma on mc.idEvento=ma.id "
                + "LEFT JOIN EventiPagati e on mc.idEvento=e.idEvento "
                + "LEFT JOIN ImportoRicalcolato ir on mc.idEvento=ir.idEvento "
                + "LEFT JOIN DirettoreEsecuzione d on mc.idProgrammaMusicale=d.idProgrammaMusicale "
                + "where (:inizioPeriodoContabile is null or mc.contabilita>=:inizioPeriodoContabile) "
                + "and (:finePeriodoContabile is null or mc.contabilita<=:finePeriodoContabile) "
                + "and (:tipoDocumento is null or mc.tipoDocumentoContabile=:tipoDocumento) "
                + "and (:idEvento is null or mc.idEvento=:idEvento) "
                + "and (:fattura is null or mc.numeroFattura=:fattura) "
                + "and (:reversale is null or mc.reversale=:reversale) "
                + "and (:voceIncasso is null or mc.voceIncasso=:voceIncasso) "
                + "and (:permesso is null or mc.numeroPermesso=:permesso) "
                + "and (:numeroPM is null or mc.numProgrammaMusicale=:numeroPM) "
                + "and (:locale is null or ma.denominazioneLocale=:locale) "
                + "and (:codiceBALocale is null or ma.codiceBALocale=:codiceBALocale) "
                + "and (:codiceSAPOrganizzatore is null or ma.codiceSapOrganizzatore=:codiceSAPOrganizzatore) "
                + "and (:dataInizioEventoDa is null or ma.dataInizioEvento >= :dataInizioEventoDa) "
                + "and (:dataInizioEventoA is null or ma.dataInizioEvento <= :dataInizioEventoA) "
                + "and (:seprag is null or e.seprag=:seprag) "
                + "and (:titoloOpera is null or upper(ma.titolo) like upper(:titoloOpera)) "
                + "and (:supporto is null or pm.supportoPm=:supporto) "
                + "and (:tipoProgrammi is null or pm.tipoPm=:tipoProgrammi) "
                + "and (:gruppoPrincipale is null or pm.flagGruppoPrincipale=:gruppoPrincipale) "
                + "and (:partitaIva is null or ma.partitaIvaOrganizzatore=:partitaIva) "
                + "and (:codiceFiscale is null or ma.codiceFiscaleOrganizzatore=:codiceFiscale) "
                + "and (:nomeDirettore is null or d.direttoreEsecuzione= upper(:nomeDirettore)) "
                + "and (:cfDirettore is null or d.codiceFiscale= upper(:cfDirettore)) ";

        if (order == null) {
            query = query + "order by pm.id asc ";
        } else if (order.startsWith("importo ")) {
            query = query + "order by ir." + order;
        } else {
            query = query + "order by mc." + order;
        }

        Query q = entityManager.createQuery(query);

        if (dataIniziale != null)
            q.setParameter("inizioPeriodoContabile", Long.parseLong(dataIniziale));
        else
            q.setParameter("inizioPeriodoContabile", null);

        if (dataFinale != null)
            q.setParameter("finePeriodoContabile", Long.parseLong(dataFinale));
        else
            q.setParameter("finePeriodoContabile", null);

        q.setParameter("tipoDocumento", tipoDocumento);
        q.setParameter("seprag", seprag);

        if (idEvento != null) {
            q.setParameter("idEvento", Long.parseLong(idEvento));
        } else {
            q.setParameter("idEvento", null);
        }

        if (fattura != null) {
            q.setParameter("fattura", Long.parseLong(fattura));
        } else {
            q.setParameter("fattura", null);
        }

        if (reversale != null) {
            q.setParameter("reversale", Long.parseLong(reversale));
        } else {
            q.setParameter("reversale", null);
        }

        q.setParameter("voceIncasso", voceIncasso);
        q.setParameter("permesso", permesso);

        if (numeroPM != null)
            q.setParameter("numeroPM", numeroPM.toString());
        else
            q.setParameter("numeroPM", null);

        q.setParameter("locale", locale);
        q.setParameter("codiceBALocale", codiceBALocale);
        q.setParameter("codiceSAPOrganizzatore", codiceSAPOrganizzatore);

        if (organizzatore != null && Character.isDigit(organizzatore.charAt(0))) {
            q.setParameter("partitaIva", organizzatore);
        } else {
            q.setParameter("partitaIva", null);
        }

        if (organizzatore != null && Character.isLetter(organizzatore.charAt(0))) {
            q.setParameter("codiceFiscale", organizzatore);
        } else {
            q.setParameter("codiceFiscale", null);
        }

        if (dataInizioEventoDa != null) {
            q.setParameter("dataInizioEventoDa",
                    DateUtils.stringToDate(dataInizioEventoDa, DateUtils.DATE_ITA_FORMAT_DASH));
        } else {
            q.setParameter("dataInizioEventoDa", null);
        }

        if (dataInizioEventoA != null) {
            q.setParameter("dataInizioEventoA", DateUtils.stringToDate(dataInizioEventoA, DateUtils.DATE_ITA_FORMAT_DASH));
        } else {
            q.setParameter("dataInizioEventoA", null);
        }
        if (titoloOpera != null)
            q.setParameter("titoloOpera", "%" + titoloOpera + "%");
        else
            q.setParameter("titoloOpera", titoloOpera);
        q.setParameter("tipoProgrammi", tipoProgrammi);
        q.setParameter("gruppoPrincipale", gruppoPrincipale);
        q.setParameter("supporto", supporto);

        if (direttoreEsecuzione != null && Character.isLetter(direttoreEsecuzione.charAt(6))) {
            q.setParameter("nomeDirettore", direttoreEsecuzione);
        } else {
            q.setParameter("nomeDirettore", null);
        }

        if (direttoreEsecuzione != null && Character.isDigit(direttoreEsecuzione.charAt(6))) {
            q.setParameter("cfDirettore", direttoreEsecuzione);
        } else {
            q.setParameter("cfDirettore", null);
        }

        int recordCount = 0;
        int startRecord = 0;
        int endRecord = recordCount;

        if (page != -1) {

            startRecord = (page) * ReportPage.NUMBER_RECORD_PER_PAGE;
            endRecord = ReportPage.NUMBER_RECORD_PER_PAGE;
        }
        List<Object[]> results = (List<Object[]>) q.setFirstResult(startRecord).setMaxResults(endRecord)
                .getResultList();

        if (results != null && results.size() > 0) {

            recordCount = results.size();
            ProgrammaMusicale programma = null;
            MovimentoContabile movimentoContabile = null;
            Utilizzazione utilizzazione = null;

            for (int i = 0; i < results.size(); i++) {
                List<MovimentoContabile> movimenti = new ArrayList<MovimentoContabile>();
                List<Utilizzazione> utilizzazioni = new ArrayList<Utilizzazione>();

                // utilizzazione = new Utilizzazione();
                programma = new ProgrammaMusicale();

                movimentoContabile = new MovimentoContabile();

                movimentoContabile.setIdProgrammaMusicale((Long) results.get(i)[0]);
                movimentoContabile.setContabilita((Integer) results.get(i)[1]);
                movimentoContabile.setVoceIncasso((String) results.get(i)[2]);
                movimentoContabile.setIdEvento((Long) results.get(i)[3]);
                movimentoContabile.setNumeroPermesso((String) results.get(i)[4]);
                movimentoContabile.setNumeroFattura((Long) results.get(i)[5]);
                movimentoContabile.setReversale((Long) results.get(i)[6]);
                movimentoContabile.setTipoDocumentoContabile((String) results.get(i)[7]);
                movimentoContabile.setNumProgrammaMusicale((String) results.get(i)[8]);
                movimentoContabile.setImportoTotDemLordo((Double) results.get(i)[9]);
                if ((BigDecimal) results.get(i)[10] != null) {
                    movimentoContabile.setImportoRicalcolato(((BigDecimal) results.get(i)[10]).doubleValue());
                }
                movimentoContabile.setId((Long) results.get(i)[11]);
                movimenti.add(movimentoContabile);
                // utilizzazioni.add(utilizzazione);

                // programma.setOpere(utilizzazioni);
                programma.setMovimenti(movimenti);

                listaAllPM.add(programma);
            }
        }

        return listaAllPM;

    }

    public List<ProgrammaMusicale> getAllTracciamentoPM(String dataIniziale, String dataFinale, Long numeroPM,
                                                        String tipoDocumento, String seprag, String idEvento, String fattura, String reversale, String voceIncasso,
                                                        String permesso, String locale, String codiceBALocale, String codiceSAPOrganizzatore, String organizzatore,
                                                        String titoloOpera, String dataInizioEvento, String dataFineEvento, String direttoreEsecuzione,
                                                        String supporto, String tipoProgrammi, String gruppoPrincipale, String order) {

        EntityManager entityManager = provider.get();
        String query = null;

        List<ProgrammaMusicale> listaAllPM = new ArrayList<ProgrammaMusicale>();

        query = "select distinct mc.idProgrammaMusicale, mc.contabilita, mc.voceIncasso, mc.idEvento, mc.numeroPermesso, mc.numeroFattura, mc.reversale, mc.tipoDocumentoContabile, mc.numProgrammaMusicale, mc.importoTotDem, mc.importoValorizzato, mc.id "
                + "FROM MovimentoContabile mc " + "LEFT JOIN ProgrammaMusicale pm on mc.idProgrammaMusicale=pm.id "
                + "LEFT JOIN Manifestazione ma on mc.idEvento=ma.id "
                + "LEFT JOIN EventiPagati e on mc.idEvento=e.idEvento "
                + "LEFT JOIN ImportoRicalcolato ir on mc.idEvento=ir.idEvento "
                + "LEFT JOIN DirettoreEsecuzione d on mc.idProgrammaMusicale=d.idProgrammaMusicale "
                + "where (:inizioPeriodoContabile is null or mc.contabilita>=:inizioPeriodoContabile) "
                + "and (:finePeriodoContabile is null or mc.contabilita<=:finePeriodoContabile) "
                + "and (:tipoDocumento is null or mc.tipoDocumentoContabile=:tipoDocumento) "
                + "and (:idEvento is null or mc.idEvento=:idEvento) "
                + "and (:fattura is null or mc.numeroFattura=:fattura) "
                + "and (:reversale is null or mc.reversale=:reversale) "
                + "and (:voceIncasso is null or mc.voceIncasso=:voceIncasso) "
                + "and (:permesso is null or mc.numeroPermesso=:permesso) "
                + "and (:numeroPM is null or mc.numProgrammaMusicale=:numeroPM) "
                + "and (:locale is null or ma.denominazioneLocale=:locale) "
                + "and (:codiceBALocale is null or ma.codiceBALocale=:codiceBALocale) "
                + "and (:codiceSAPOrganizzatore is null or ma.codiceSapOrganizzatore=:codiceSAPOrganizzatore) "
                + "and (:dataInizioEvento is null or ma.dataInizioEvento=:dataInizioEvento) "
                + "and (:dataFineEvento is null or ma.dataFineEvento=:dataFineEvento) "
                + "and (:seprag is null or e.seprag=:seprag) "
                + "and (:titoloOpera is null or upper(ma.titolo) like upper(:titoloOpera)) "
                + "and (:supporto is null or pm.supportoPm=:supporto) "
                + "and (:tipoProgrammi is null or pm.tipoPm=:tipoProgrammi) "
                + "and (:gruppoPrincipale is null or pm.flagGruppoPrincipale=:gruppoPrincipale) "
                + "and (:partitaIva is null or ma.partitaIvaOrganizzatore=:partitaIva) "
                + "and (:codiceFiscale is null or ma.codiceFiscaleOrganizzatore=:codiceFiscale) "
                + "and (:nomeDirettore is null or d.direttoreEsecuzione= upper(:nomeDirettore)) "
                + "and (:cfDirettore is null or d.codiceFiscale= upper(:cfDirettore)) ";

        if (order == null) {
            query = query + "order by pm.id asc ";
        } else if (order.startsWith("importo")) {
            query = query + "order by ir." + order;
        } else {
            query = query + "order by mc." + order;
        }

        Query q = entityManager.createQuery(query);

        if (dataIniziale != null)
            q.setParameter("inizioPeriodoContabile", Long.parseLong(dataIniziale));
        else
            q.setParameter("inizioPeriodoContabile", null);

        if (dataFinale != null)
            q.setParameter("finePeriodoContabile", Long.parseLong(dataFinale));
        else
            q.setParameter("finePeriodoContabile", null);

        q.setParameter("tipoDocumento", tipoDocumento);
        q.setParameter("seprag", seprag);

        if (idEvento != null) {
            q.setParameter("idEvento", Long.parseLong(idEvento));
        } else {
            q.setParameter("idEvento", null);
        }

        if (fattura != null) {
            q.setParameter("fattura", Long.parseLong(fattura));
        } else {
            q.setParameter("fattura", null);
        }

        if (reversale != null) {
            q.setParameter("reversale", Long.parseLong(reversale));
        } else {
            q.setParameter("reversale", null);
        }

        q.setParameter("voceIncasso", voceIncasso);
        q.setParameter("permesso", permesso);

        if (numeroPM != null)
            q.setParameter("numeroPM", numeroPM.toString());
        else
            q.setParameter("numeroPM", null);

        q.setParameter("locale", locale);
        q.setParameter("codiceBALocale", codiceBALocale);
        q.setParameter("codiceSAPOrganizzatore", codiceSAPOrganizzatore);

        if (organizzatore != null && Character.isDigit(organizzatore.charAt(0))) {
            q.setParameter("partitaIva", organizzatore);
        } else {
            q.setParameter("partitaIva", null);
        }

        if (organizzatore != null && Character.isLetter(organizzatore.charAt(0))) {
            q.setParameter("codiceFiscale", organizzatore);
        } else {
            q.setParameter("codiceFiscale", null);
        }

        if (dataInizioEvento != null) {
            q.setParameter("dataInizioEvento",
                    DateUtils.stringToDate(dataInizioEvento, DateUtils.DATE_ITA_FORMAT_DASH));
        } else {
            q.setParameter("dataInizioEvento", null);
        }

        if (dataFineEvento != null) {
            q.setParameter("dataFineEvento", DateUtils.stringToDate(dataFineEvento, DateUtils.DATE_ITA_FORMAT_DASH));
        } else {
            q.setParameter("dataFineEvento", null);
        }
        if (titoloOpera != null)
            q.setParameter("titoloOpera", "%" + titoloOpera + "%");
        else
            q.setParameter("titoloOpera", titoloOpera);
        q.setParameter("tipoProgrammi", tipoProgrammi);
        q.setParameter("gruppoPrincipale", gruppoPrincipale);
        q.setParameter("supporto", supporto);

        if (direttoreEsecuzione != null && Character.isLetter(direttoreEsecuzione.charAt(6))) {
            q.setParameter("nomeDirettore", direttoreEsecuzione);
        } else {
            q.setParameter("nomeDirettore", null);
        }

        if (direttoreEsecuzione != null && Character.isDigit(direttoreEsecuzione.charAt(6))) {
            q.setParameter("cfDirettore", direttoreEsecuzione);
        } else {
            q.setParameter("cfDirettore", null);
        }

        List<Object[]> results = (List<Object[]>) q.getResultList();

        if (results != null && results.size() > 0) {
            ProgrammaMusicale programma = null;
            MovimentoContabile movimentoContabile = null;
            for (int i = 0; i < results.size(); i++) {
                List<MovimentoContabile> movimenti = new ArrayList<MovimentoContabile>();
                programma = new ProgrammaMusicale();
                movimentoContabile = new MovimentoContabile();
                movimentoContabile.setIdProgrammaMusicale((Long) results.get(i)[0]);
                movimentoContabile.setContabilita((Integer) results.get(i)[1]);
                movimentoContabile.setVoceIncasso((String) results.get(i)[2]);
                movimentoContabile.setIdEvento((Long) results.get(i)[3]);
                movimentoContabile.setNumeroPermesso((String) results.get(i)[4]);
                movimentoContabile.setNumeroFattura((Long) results.get(i)[5]);
                movimentoContabile.setReversale((Long) results.get(i)[6]);
                movimentoContabile.setTipoDocumentoContabile((String) results.get(i)[7]);
                movimentoContabile.setNumProgrammaMusicale((String) results.get(i)[8]);
                movimentoContabile.setImportoTotDemLordo((Double) results.get(i)[9]);
                if ((BigDecimal) results.get(i)[10] != null) {
                    movimentoContabile.setImportoRicalcolato(((BigDecimal) results.get(i)[10]).doubleValue());
                }
                movimentoContabile.setId((Long) results.get(i)[11]);
                movimenti.add(movimentoContabile);
                programma.setMovimenti(movimenti);
                listaAllPM.add(programma);
            }
        }
        return listaAllPM;
    }

    public EventiPagati getEventoPagato(Long idEvento, String voceIncasso, String numeroFattura) {

        EntityManager entityManager = provider.get();
        EventiPagati eventoPagato = null;

        String query = "select e from EventiPagati e " + "where e.idEvento=:idEvento "
                + "and e.numeroFattura=:numeroFattura " + "and e.voceIncasso=:voceIncasso";

        Query q = entityManager.createQuery(query);

        q.setParameter("idEvento", idEvento);
        q.setParameter("numeroFattura", numeroFattura);
        q.setParameter("voceIncasso", voceIncasso);

        List<EventiPagati> results = (List<EventiPagati>) q.getResultList();

        if (results != null && results.size() > 0) {
            eventoPagato = results.get(0);
        }

        return eventoPagato;

    }

    public long getCountListaEventiPagati(String inizioPeriodoContabile, String finePeriodoContabile,
                                          PerfEventiPagatiDTO perfEventiPagatiDTO, String organizzatore, String direttoreEsecuzione, String permesso,
                                          String numeroPM, String titoloOpera, String codiceSAP, String dataInizioEvento, String dataFineEvento,
                                          String fatturaValidata) {
        EntityManager entityManager = provider.get();
        String query = null;

        query = "select COUNT(distinct e.ID_EVENTO_PAGATO) " + "from PERF_EVENTI_PAGATI e "
                + "LEFT JOIN PERF_MOVIMENTO_CONTABILE mc ON e.ID_EVENTO=mc.ID_EVENTO "
                + "LEFT JOIN PERF_MANIFESTAZIONE mf on e.ID_EVENTO=mf.ID_EVENTO "
                + "LEFT JOIN PERF_DIRETTORE_ESECUZIONE d on mc.ID_PROGRAMMA_MUSICALE=d.ID_PROGRAMMA_MUSICALE "
                + "where (?1 is null or e.CONTABILITA>=?1) " + "and (?2 is null or e.CONTABILITA<=?2) "
                + "and (?3 is null or e.ID_EVENTO=?3) " + "and (?4 is null or e.NUMERO_FATTURA=?4) "
                + "and (?5 is null or e.NUMERO_REVERSALE=?5) " + "and (?6 is null or e.VOCE_INCASSO=?6) "
                + "and (?7 is null or mf.DATA_INIZIO_EVENTO >=?7) " + "and (?8 is null or mf.DATA_FINE_EVENTO <=?8) "
                + "and (?9 is null or e.SEPRAG=?9) " + "and (?10 is null or e.CODICE_BA_LOCALE=?10) "
                + "and (?11 is null or e.TIPO_DOCUMENTO_CONTABILE=?11) "
                + "and (?12 is null or d.DIRETTORE_ESECUZIONE=upper(?12)) "
                + "and (?13 is null or d.CODICE_FISCALE= upper(?13)) "
                + "and (?14 is null or e.DENOMINAZIONE_LOCALE=?14) "
                + "and (?15 is null or mf.PARTITA_IVA_ORGANIZZATORE=?15) "
                + "and (?16 is null or mf.CODICE_FISCALE_ORGANIZZATORE= upper(?16)) "
                + "and (?17 is null or mf.CODICE_SAP_ORGANIZZATORE=?17) "
                + "and (?18 is null or mf.TITOLO like '%18%') " + "and (?19 is null or e.PERMESSO=?19) "
                + "and (?20 is null or mc.NUMERO_PM=?20) ";

        switch (fatturaValidata) {
            case "notValidated":
                query = query + "and e.QUADRATURA_NDM is null ";
                break;
            case "validated":
                query = query + "and e.QUADRATURA_NDM is not null ";
                break;
            case "ok":
                query = query + "and e.QUADRATURA_NDM ='1' ";
                break;
            case "ko":
                query = query + "and e.QUADRATURA_NDM = '0' ";
                break;
        }

        Query q = entityManager.createNativeQuery(query);

        if (inizioPeriodoContabile != null)
            q.setParameter(1, new BigInteger(inizioPeriodoContabile));
        else
            q.setParameter(1, null);

        if (finePeriodoContabile != null)
            q.setParameter(2, new BigInteger(finePeriodoContabile));
        else
            q.setParameter(2, null);

        q.setParameter(3, perfEventiPagatiDTO.getEvento());
        q.setParameter(4, perfEventiPagatiDTO.getFattura());
        if (perfEventiPagatiDTO.getReversale() != null) {
            q.setParameter(5, Long.valueOf(perfEventiPagatiDTO.getReversale()));
        } else
            q.setParameter(5, perfEventiPagatiDTO.getReversale());

        q.setParameter(6, perfEventiPagatiDTO.getVoceIncasso());

        if (dataInizioEvento != null) {
            q.setParameter(7, DateUtils.stringToDate(dataInizioEvento, DateUtils.DATE_ITA_FORMAT_DASH));
        } else {
            q.setParameter(7, null);
        }
        if (dataFineEvento != null) {
            q.setParameter(8, DateUtils.stringToDate(dataFineEvento, DateUtils.DATE_ITA_FORMAT_DASH));
        } else {
            q.setParameter(8, null);
        }
        q.setParameter(9, perfEventiPagatiDTO.getSeprag());
        q.setParameter(10, perfEventiPagatiDTO.getCodiceBA());
        q.setParameter(11, perfEventiPagatiDTO.getTipoDocumento());

        if (direttoreEsecuzione != null && Character.isLetter(direttoreEsecuzione.charAt(6))) {
            q.setParameter(12, direttoreEsecuzione);
        } else {
            q.setParameter(12, null);
        }

        if (direttoreEsecuzione != null && Character.isDigit(direttoreEsecuzione.charAt(6))) {
            q.setParameter(13, direttoreEsecuzione);
        } else {
            q.setParameter(13, null);
        }

        q.setParameter(14, perfEventiPagatiDTO.getLocale());

        if (organizzatore != null && Character.isDigit(organizzatore.charAt(0))) {
            q.setParameter(15, organizzatore);
        } else {
            q.setParameter(15, null);
        }

        if (organizzatore != null && Character.isLetter(organizzatore.charAt(0))) {
            q.setParameter(16, organizzatore);
        } else {
            q.setParameter(16, null);
        }
        q.setParameter(17, codiceSAP);
        q.setParameter(18, titoloOpera);
        q.setParameter(19, permesso);
        q.setParameter(20, numeroPM);

        Long result = (Long) q.getSingleResult();

        return result;
    }

    public List<EventiPagati> getListaEventiPagati(String inizioPeriodoContabile, String finePeriodoContabile,
                                                   PerfEventiPagatiDTO perfEventiPagatiDTO, String organizzatore, String direttoreEsecuzione, String permesso,
                                                   String numeroPM, String titoloOpera, String codiceSAP, String dataInizioEvento, String dataFineEvento,
                                                   String fatturaValidata, String order) {

        return getListaEventiPagati(inizioPeriodoContabile, finePeriodoContabile, perfEventiPagatiDTO, organizzatore,
                direttoreEsecuzione, permesso, numeroPM, titoloOpera, codiceSAP, dataInizioEvento, dataFineEvento,
                fatturaValidata, order, false);

    }

    public List<EventiPagati> getListaEventiPagati(String inizioPeriodoContabile, String finePeriodoContabile,
                                                   PerfEventiPagatiDTO perfEventiPagatiDTO, String organizzatore, String direttoreEsecuzione, String permesso,
                                                   String numeroPM, String titoloOpera, String codiceSAP, String dataInizioEvento, String dataFineEvento,
                                                   String fatturaValidata, String order, boolean allList) {

        EntityManager entityManager = provider.get();
        String query = null;

        List<EventiPagati> listaEventi = new ArrayList<EventiPagati>();
        List<EventiPagati> listaAllEventi = new ArrayList<EventiPagati>();

        // ReportPage reportPage = new ReportPage(listaEventi, listaEventi.size(),
        // perfEventiPagatiDTO.getPage());

        int recordCount = 0;

        query = "select * from ( "
                + "select distinct e.ID_EVENTO as idEvento, e.PERMESSO as permesso, e.CONTABILITA as contabilita, e.NUMERO_FATTURA as numeroFattura, e.VOCE_INCASSO as voceIncasso, "
                + "e.DATA_INIZIO_EVENTO as dataInizioEvento, e.ORA_INIZIO_EVENTO as oraInizioEvento, e.DENOMINAZIONE_LOCALE as locale, mf.NOME_ORGANIZZATORE as organizzatore, "
                + "e.TIPO_DOCUMENTO_CONTABILE as tipoDocumento, e.IMPORTO_DEM as importoDem, e.ID_EVENTO_PAGATO as idEventoPagato , e.QUADRATURA_NDM as quadratura, e.IMPORTO_AGGIO as importoAggio, e.IMPORTO_EX_ART_18 as importoExArt "
                + "from PERF_EVENTI_PAGATI e " + "LEFT JOIN PERF_MOVIMENTO_CONTABILE mc ON e.ID_EVENTO=mc.ID_EVENTO "
                + "LEFT JOIN PERF_MANIFESTAZIONE mf on e.ID_EVENTO=mf.ID_EVENTO "
                + "LEFT JOIN PERF_DIRETTORE_ESECUZIONE d on mc.ID_PROGRAMMA_MUSICALE=d.ID_PROGRAMMA_MUSICALE "
                + "where (?1 is null or e.CONTABILITA>=?1) " + "and (?2 is null or e.CONTABILITA<=?2) "
                + "and (?3 is null or e.ID_EVENTO=?3) " + "and (?4 is null or e.NUMERO_FATTURA=?4) "
                + "and (?5 is null or e.NUMERO_REVERSALE=?5) " + "and (?6 is null or e.VOCE_INCASSO=?6) "
                + "and (?7 is null or mf.DATA_INIZIO_EVENTO >=?7) " + "and (?8 is null or mf.DATA_FINE_EVENTO <=?8) "
                + "and (?9 is null or e.SEPRAG=?9) " + "and (?10 is null or e.CODICE_BA_LOCALE=?10) "
                + "and (?11 is null or e.TIPO_DOCUMENTO_CONTABILE=?11) "
                + "and (?12 is null or d.DIRETTORE_ESECUZIONE=upper(?12)) "
                + "and (?13 is null or d.CODICE_FISCALE= upper(?13)) "
                + "and (?14 is null or e.DENOMINAZIONE_LOCALE=?14) "
                + "and (?15 is null or mf.PARTITA_IVA_ORGANIZZATORE=?15) "
                + "and (?16 is null or mf.CODICE_FISCALE_ORGANIZZATORE= upper(?16)) "
                + "and (?17 is null or mf.CODICE_SAP_ORGANIZZATORE=?17) "
                + "and (?18 is null or mf.TITOLO like '%18%') " + "and (?19 is null or e.PERMESSO=?19) "
                + "and (?20 is null or mc.NUMERO_PM=?20) ";

        if (fatturaValidata == null)
            fatturaValidata = "";

        switch (fatturaValidata) {
            case "notValidated":
                query = query + "and e.QUADRATURA_NDM is null ";
                break;
            case "validated":
                query = query + "and e.QUADRATURA_NDM is not null ";
                break;
            case "ok":
                query = query + "and e.QUADRATURA_NDM ='1' ";
                break;
            case "ko":
                query = query + "and e.QUADRATURA_NDM = '0' ";
                break;
        }

        query = query + ") as f ";
        if (order == null) {
            query = query + "order by f.idEvento desc";
        } else {
            query = query + "order by f." + order;
        }

        Query q = entityManager.createNativeQuery(query);

        if (inizioPeriodoContabile != null)
            q.setParameter(1, new BigInteger(inizioPeriodoContabile));
        else
            q.setParameter(1, null);

        if (finePeriodoContabile != null)
            q.setParameter(2, new BigInteger(finePeriodoContabile));
        else
            q.setParameter(2, null);

        q.setParameter(3, perfEventiPagatiDTO.getEvento());
        q.setParameter(4, perfEventiPagatiDTO.getFattura());
        if (perfEventiPagatiDTO.getReversale() != null) {
            q.setParameter(5, Long.valueOf(perfEventiPagatiDTO.getReversale()));
        } else
            q.setParameter(5, perfEventiPagatiDTO.getReversale());

        q.setParameter(6, perfEventiPagatiDTO.getVoceIncasso());

        if (dataInizioEvento != null) {
            q.setParameter(7, DateUtils.stringToDate(dataInizioEvento, DateUtils.DATE_ITA_FORMAT_DASH));
        } else {
            q.setParameter(7, null);
        }
        if (dataFineEvento != null) {
            q.setParameter(8, DateUtils.stringToDate(dataFineEvento, DateUtils.DATE_ITA_FORMAT_DASH));
        } else {
            q.setParameter(8, null);
        }
        q.setParameter(9, perfEventiPagatiDTO.getSeprag());
        q.setParameter(10, perfEventiPagatiDTO.getCodiceBA());
        q.setParameter(11, perfEventiPagatiDTO.getTipoDocumento());

        if (direttoreEsecuzione != null && Character.isLetter(direttoreEsecuzione.charAt(6))) {
            q.setParameter(12, direttoreEsecuzione);
        } else {
            q.setParameter(12, null);
        }

        if (direttoreEsecuzione != null && Character.isDigit(direttoreEsecuzione.charAt(6))) {
            q.setParameter(13, direttoreEsecuzione);
        } else {
            q.setParameter(13, null);
        }

        q.setParameter(14, perfEventiPagatiDTO.getLocale());

        if (organizzatore != null && Character.isDigit(organizzatore.charAt(0))) {
            q.setParameter(15, organizzatore);
        } else {
            q.setParameter(15, null);
        }

        if (organizzatore != null && Character.isLetter(organizzatore.charAt(0))) {
            q.setParameter(16, organizzatore);
        } else {
            q.setParameter(16, null);
        }
        q.setParameter(17, codiceSAP);
        q.setParameter(18, titoloOpera);
        q.setParameter(19, permesso);
        q.setParameter(20, numeroPM);

        int startRecord = 0;
        int endRecord = recordCount;

        if (perfEventiPagatiDTO.getPage() != -1) {

            startRecord = (perfEventiPagatiDTO.getPage()) * ReportPage.NUMBER_RECORD_PER_PAGE;
            endRecord = ReportPage.NUMBER_RECORD_PER_PAGE;
        }

        List<Object[]> results;
        if (allList == true) {
            results = (List<Object[]>) q.getResultList();
        } else {
            results = (List<Object[]>) q.setFirstResult(startRecord).setMaxResults(endRecord).getResultList();
        }

        if (results != null && results.size() > 0) {
            recordCount = results.size();
            EventiPagati eventoPagato = null;
            for (int i = 0; i < results.size(); i++) {
                eventoPagato = new EventiPagati();
                Manifestazione manifestazione = new Manifestazione();
                eventoPagato.setIdEvento(((Long) results.get(i)[0]));
                eventoPagato.setPermesso(((String) results.get(i)[1]));
                eventoPagato.setContabilita(((Long) results.get(i)[2]).intValue());
                eventoPagato.setNumeroFattura(((String) results.get(i)[3]));
                eventoPagato.setVoceIncasso(((String) results.get(i)[4]));
                eventoPagato.setDataInizioEvento(((Date) results.get(i)[5]));
                eventoPagato.setOraInizioEvento(((String) results.get(i)[6]));
                eventoPagato.setDenominazioneLocale(((String) results.get(i)[7]));
                manifestazione.setNomeOrganizzatore((String) results.get(i)[8]);
                eventoPagato.setTipoDocumentoContabile(((String) results.get(i)[9]));
                if (results.get(i)[10] != null)
                    eventoPagato.setImportDem(((BigDecimal) results.get(i)[10]).doubleValue());
                eventoPagato.setId((Long) results.get(i)[11]);
                eventoPagato.setManifestazione(manifestazione);
                eventoPagato.setQuadraturaNDM((String) results.get(i)[12]);
                if ((BigDecimal) results.get(i)[13] != null) {
                    eventoPagato.setImportoAggio(((BigDecimal) results.get(i)[13]).doubleValue());
                }
                if ((BigDecimal) results.get(i)[14] != null) {
                    eventoPagato.setImportoExArt(((BigDecimal) results.get(i)[14]).doubleValue());
                }
                listaAllEventi.add(eventoPagato);
            }
        }

        return listaAllEventi;
    }

    public List<ImportoRicalcolato> getImportiRicalcolatiPerMovimentazione(Long idMovimento158) {

        EntityManager entityManager = provider.get();
        String query = "select i " + "from ImportoRicalcolato i " + "where i.idMovimentoContabile=:idMovimento158 "
                + "order by i.dataOraInserimento desc";

        Query q = entityManager.createQuery(query).setParameter("idMovimento158", idMovimento158);

        List<ImportoRicalcolato> results = (List<ImportoRicalcolato>) q.getResultList();

        if (results != null && results.size() > 0)
            return results;
        else
            results = new ArrayList<ImportoRicalcolato>();

        return results;

    }

    public List<MovimentoContabile> getMovimentazioniPerEvento(Long eventoId) {

        EntityManager entityManager = provider.get();
        String query = "select m " + "from MovimentoContabile m " + "where m.idEvento=:eventoId "
                + "order by m.contabilita, m.voceIncasso, m.numeroFattura desc";

        Query q = entityManager.createQuery(query).setParameter("eventoId", eventoId);

        List<MovimentoContabile> results = (List<MovimentoContabile>) q.getResultList();

        if (results != null && results.size() > 0) {
            List<ImportoRicalcolato> importiRicalcolati;
            for (int i = 0; i < results.size(); i++) {
                importiRicalcolati = getImportiRicalcolatiPerMovimentazione(results.get(i).getId());
                results.get(i).setImportiRicalcolati(importiRicalcolati);
            }
            return results;
        } else
            results = new ArrayList<MovimentoContabile>();

        entityManager.clear();
        return results;

    }

    public List<EventiPagati> getEvento(Long eventoId) {

        EntityManager entityManager = provider.get();
        String query = "select e " + "from EventiPagati e " + "where e.idEvento=:eventoId "
                + "order by e.contabilita, e.voceIncasso, e.numeroFattura desc";

        Query q = entityManager.createQuery(query).setParameter("eventoId", eventoId);

        List<EventiPagati> results = (List<EventiPagati>) q.getResultList();

        Manifestazione manifestazione = entityManager.find(Manifestazione.class, eventoId);

        List<MovimentoContabile> movimentazioniPM = getMovimentazioniPerEvento(eventoId);

        for (MovimentoContabile movimento : movimentazioniPM) {
            movimento.setSganciabile(getVociIncassoForIdPm(movimento.getIdProgrammaMusicale()));
        }
        if (results != null && results.size() > 0 && manifestazione != null) {
            for (int i = 0; i < results.size(); i++) {
                results.get(i).setManifestazione(manifestazione);
                results.get(i).setMovimentazioniPM(movimentazioniPM);
            }
        }

        return results;

    }

    // public List<EventiPagati> getEvento(List<Manifestazione> eventi){
    //// List<Manifestazione> listaEventi=new ArrayList<Manifestazione>();
    //// Map<String,String> eventiKey=new HashMap<String,String>();
    //// Manifestazione evento = null;
    //// if (listaMovimenti!=null) {
    //// for (MovimentoContabile movimento : listaMovimenti) {
    //// evento = getManifestazione(movimento.getIdEvento());
    //// if (eventiKey.get(evento.getId().toString())==null){
    //// listaEventi.add(evento);
    //// eventiKey.put(evento.getId().toString(), evento.getId().toString());
    //// }
    ////
    //// }
    //// }
    ////
    //// return listaEventi;
    //
    // List<EventiPagati> listaEventiPagati=new ArrayList<EventiPagati>();
    // Map<String,String> eventiKey=new HashMap<String,String>();
    // EventiPagati evento = null;
    // if (eventi!=null) {
    // for (Manifestazione movimento : eventi) {
    // evento = getEventoPagato(idEvento, voceIncasso, numeroFattura);
    // if (eventiKey.get(evento.getId().toString())==null){
    // listaEventiPagati.add(evento);
    // eventiKey.put(evento.getId().toString(), evento.getId().toString());
    // }
    //
    // }
    // }
    //
    // return listaEventiPagati;
    //
    // }

    public List<ImportoRicalcolato> getImportiPMAssegnatiPerEvento(Long idEvento) {

        EntityManager entityManager = provider.get();
        List<ImportoRicalcolato> importiPMAssegnati = new ArrayList<ImportoRicalcolato>();
        ImportoRicalcolato importoPMAssegnati;

        // Importo di un evento già assegnato a PM rientrati, suddiviso per Voce Incasso
        String query = "select m.codice_voce_incasso, i.id_esecuzione_ricalcolo, i.data_ora_inserimento, sum(CASE WHEN tipo_documento_contabile='501' THEN i.importo ELSE -i.importo END)"
                + "from PERF_IMPORTO_RICALCOLATO i, PERF_MOVIMENTO_CONTABILE m "
                + "where i.id_movimento_contabile=m.id_movimento_contabile " + "and m.id_evento=? "
                + "group by m.codice_voce_incasso, i.id_esecuzione_ricalcolo, i.data_ora_inserimento "
                + "order by i.id_esecuzione_ricalcolo desc ";

        Query q = entityManager.createNativeQuery(query).setParameter(1, idEvento);

        List<Object[]> results = (List<Object[]>) q.getResultList();

        if (results != null && results.size() > 0) {

            for (int i = 0; i < results.size(); i++) {

                importoPMAssegnati = new ImportoRicalcolato();
                importoPMAssegnati.setVoceIncasso((String) results.get(i)[0]);
                importoPMAssegnati.setIdEsecuzioneRicalcolo((Long) results.get(i)[1]);
                importoPMAssegnati.setDataOraInserimento((Date) results.get(i)[2]);
                importoPMAssegnati.setIdEvento(idEvento);
                importoPMAssegnati.setImporto(((BigDecimal) results.get(i)[3]).doubleValue());

                importiPMAssegnati.add(importoPMAssegnati);
            }
        }

        return importiPMAssegnati;
    }

    public List<ImportoRicalcolato> getImportiTotaliPerEvento(Long idEvento) {

        EntityManager entityManager = provider.get();
        List<ImportoRicalcolato> importiPMAssegnati = new ArrayList<ImportoRicalcolato>();
        ImportoRicalcolato importoPMAssegnati;

        // Importo di un evento totale per Voce Incasso
        String query = "SELECT t1.VOCE_INCASSO " +
                "	,t1.NUMERO_FATTURA " +
                "	,t1.IMPORTO_DEM " +
                "	,t2.IMPORTO_NETTO " +
                "	,t1.IMPORTO_NON_RIENTRATO_PRINCIPALE " +
                "	,t1.IMPORTO_NON_RIENTRATO_SPALLA " +
                "FROM ( " +
                "	SELECT ep.voce_incasso " +
                "		,ep.NUMERO_FATTURA " +
                "		,SUM(CASE " +
                "				WHEN ep.tipo_documento_contabile = '501' " +
                "					THEN importo_dem " +
                "				ELSE - importo_dem " +
                "				END) AS IMPORTO_DEM " +
                "		,SUM(VALORE_NON_RIENTRATO) AS IMPORTO_NON_RIENTRATO_PRINCIPALE " +
                "		,SUM(VALORE_NON_RIENTRATO_SPALLA) AS IMPORTO_NON_RIENTRATO_SPALLA " +
                "	FROM PERF_EVENTI_PAGATI ep " +
                "	WHERE ep.id_evento = ?1 " +
                "	GROUP BY ep.voce_incasso " +
                "		,ep.NUMERO_FATTURA " +
                "	) AS t1 " +
                "LEFT JOIN ( " +
                "	SELECT ep.voce_incasso " +
                "		,ep.NUMERO_FATTURA " +
                "		,SUM(IMPORTO_VALORIZZATO) AS IMPORTO_NETTO " +
                "	FROM PERF_EVENTI_PAGATI ep " +
                "	LEFT JOIN PERF_MOVIMENTO_CONTABILE mc ON mc.ID_EVENTO = ep.ID_EVENTO " +
                "		AND mc.NUMERO_FATTURA = ep.NUMERO_FATTURA " +
                "		AND mc.CODICE_VOCE_INCASSO = ep.voce_incasso " +
                "	WHERE ep.id_evento = ?1 " +
                "	GROUP BY ep.voce_incasso " +
                "		,ep.NUMERO_FATTURA " +
                "	) AS t2 ON t1.voce_incasso = t2.voce_incasso " +
                "	AND t1.numero_fattura = t2.numero_fattura";

        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, idEvento);

        List<Object[]> results = (List<Object[]>) q.getResultList();

        if (results != null && results.size() > 0) {

            for (int i = 0; i < results.size(); i++) {

                importoPMAssegnati = new ImportoRicalcolato();
                importoPMAssegnati.setVoceIncasso((String) results.get(i)[0]);
                importoPMAssegnati.setNumeroFattura(Long.parseLong(((String) results.get(i)[1])));
                importoPMAssegnati.setImporto(results.get(i)[2] != null ? ((BigDecimal) results.get(i)[2]).doubleValue() : null);
                importoPMAssegnati.setImportoNetto(results.get(i)[3] != null ? ((BigDecimal) results.get(i)[3]).doubleValue() : null);
                importoPMAssegnati.setImportoNonRientratoPrincipale(results.get(i)[4] != null ? ((BigDecimal) results.get(i)[4]).doubleValue() : null);
                importoPMAssegnati.setImportoNonRientratoSpalla(results.get(i)[5] != null ? ((BigDecimal) results.get(i)[5]).doubleValue() : null);

                importiPMAssegnati.add(importoPMAssegnati);
            }
        }

        return importiPMAssegnati;
    }

    public List<String> getTipoProgrammi() {

        EntityManager entityManager = provider.get();
        List<String> tipoPMList = null;

        String query = "select distinct(p.tipoPm)" + " from ProgrammaMusicale p "
                + " order by p.tipoPm asc";

        Query q = entityManager.createQuery(query);

        tipoPMList = q.getResultList();

        return tipoPMList;

    }

    // serve per ottenere il seprag per la visualizzazione in dettagli PM
    // public EventiPagati getEventiPagatiByIdEvento(Long idEvento) {
    //
    // EntityManager entityManager= provider.get();
    // EventiPagati eventoPagato = null;
    //
    // String query = "select e from EventiPagati e where idEvento=:idEvento";
    //
    // Query q = entityManager.createQuery(query);
    //
    // q.setParameter("idEvento",idEvento);
    //
    // List<EventiPagati> eventiList = (List <EventiPagati>) q.getResultList();
    //
    // if(eventiList != null && eventiList.size()>0) {
    // eventoPagato = eventiList.get(0);
    // }
    //
    // return eventoPagato;
    // }

    public MovimentoContabile getMovimentoContabile(Long idMovimento) {

        EntityManager entityManager = provider.get();
        return entityManager.find(MovimentoContabile.class, idMovimento);

    }

    public List<MovimentoContabile> getMovimentiContabile(Long idProgrammaMusicale) {

        EntityManager entityManager = provider.get();
        List<MovimentoContabile> listaMovimenti = null;

        String query = "select m " + " from MovimentoContabile m "
                + " where m.idProgrammaMusicale=:idProgrammaMusicale";

        Query q = entityManager.createQuery(query);

        q.setParameter("idProgrammaMusicale", idProgrammaMusicale);

        listaMovimenti = q.getResultList();

        if (listaMovimenti != null && listaMovimenti.size() > 0) {
            List<ImportoRicalcolato> importiRicalcolati;
            for (int i = 0; i < listaMovimenti.size(); i++) {
                importiRicalcolati = getImportiRicalcolatiPerMovimentazione(listaMovimenti.get(i).getId());
                listaMovimenti.get(i).setImportiRicalcolati(importiRicalcolati);
            }
            return listaMovimenti;
        } else {
            return new ArrayList<MovimentoContabile>();
        }

    }

    public Manifestazione getManifestazione(Long idEvento) {
        EntityManager entityManager = provider.get();
        try {
            Manifestazione manifestazione = entityManager.find(Manifestazione.class, idEvento);
            return manifestazione;
        } catch (Exception e) {
            return null;
        }
    }

    public List<Manifestazione> getManifestazioni(List<MovimentoContabile> listaMovimenti) {

        List<Manifestazione> listaEventi = new ArrayList<Manifestazione>();
        Map<String, String> eventiKey = new HashMap<String, String>();
        Manifestazione evento = null;
        if (listaMovimenti != null) {
            for (MovimentoContabile movimento : listaMovimenti) {
                evento = getManifestazione(movimento.getIdEvento());
                if (evento != null) {
                    if (eventiKey.get(evento.getId().toString()) == null) {
                        listaEventi.add(evento);
                        eventiKey.put(evento.getId().toString(), evento.getId().toString());
                    }
                }
            }
        }

        return listaEventi;

    }

    public ProgrammaMusicale getProgrammaMusicaleById(Long idProgrammaMusicale) {

        EntityManager entityManager = provider.get();
        ProgrammaMusicale programmaMusicale = (ProgrammaMusicale) entityManager
                .createQuery("Select x from ProgrammaMusicale x where x.id=:idProgrammaMusicale")
                .setParameter("idProgrammaMusicale", idProgrammaMusicale).getSingleResult();

        return programmaMusicale;

    }

    public Utilizzazione getOperaById(Long idOpera) {

        EntityManager entityManager = provider.get();
        Utilizzazione utilizzazione = entityManager.find(Utilizzazione.class, idOpera);

        entityManager.clear();

        return utilizzazione;

    }

    public void updateProgrammaMusicaleOnDetail(Long idPM, String flagCampionamento, String flagGruppo) {
        EntityManager entityManager = provider.get();
        ProgrammaMusicale programmaMusicaleOld = getProgrammaMusicaleById(idPM);

        entityManager.getTransaction().begin();
        if (flagCampionamento != null)
            programmaMusicaleOld.setFlagDaCampionare(flagCampionamento.charAt(0));
        if (flagGruppo != null)
            programmaMusicaleOld.setFlagGruppoPrincipale(flagGruppo.charAt(0));
        entityManager.persist(programmaMusicaleOld);
        entityManager.getTransaction().commit();
    }

    public void updateOperaOnDetail(Double durata, String durataMeno30Sec, String flagPD, Long idOpera) {

        EntityManager entityManager = provider.get();
        Utilizzazione utilizzazione = getOperaById(idOpera);

        entityManager.getTransaction().begin();
        if (durata != null)
            utilizzazione.setDurata(durata);
        if (durataMeno30Sec != null)
            utilizzazione.setDurataInferiore30sec(durataMeno30Sec);
        if (flagPD != null)
            utilizzazione.setFlagPubblicoDominio(flagPD);
        entityManager.persist(utilizzazione);
        entityManager.getTransaction().commit();
    }

    public List<Utilizzazione> getUtilizzazioni(Long idProgrammaMusicale) {

        EntityManager entityManager = provider.get();
        List<Object[]> result = null;
        List<Utilizzazione> listaOpere = new ArrayList<>();
        String query = "SELECT " + " UT.ID_UTILIZZAZIONE, " + " UT.ID_PROGRAMMA_MUSICALE, "
                + " UT.CODICE_OPERA, " + " UT.TITOLO_COMPOSIZIONE, " + " UT.COMPOSITORE, "
                + " UT.NUMERO_PROGRAMMA_MUSICALE, " + " UT.DURATA, " + " UT.DURATA_INFERIORE_30SEC, "
                + " UT.AUTORI, UT.INTERPRETI, " + " UT.FLAG_CODIFICA, " + " UT.COMBINAZIONE_CODIFICA, "
                + " UT.FLAG_PUBBLICO_DOMINIO, " + " UT.FLAG_MAG_NON_CONCESSA, "
                + " UT.TIPO_ACCERTAMENTO, " + " UT.NETTO_MATURATO, " + " UT.DATA_PRIMA_CODIFICA, "
                + " UT.DATA_INS, UT.PUNTI_ORIGINALI, " + " UT.ID_COMBANA, " + " UT.ID_CODIFICA, "
                + " UT.PUNTI_RICALCOLATI, " + " UT.FLAG_CEDOLA_ESCLUSA, "
                + " SUM(UT.VALORE_ORIGINALE) AS VALORE_ORIGINALE, "
                + " SUM(UT.VALORE_RICALCOLATO) AS VALORE_RICALCOLATO " + "FROM " + " (SELECT "
                + " PERF_UTILIZZAZIONE.*, "
                + "		IF(PERF_MOVIMENTO_CONTABILE.PUNTI_PROGRAMMA_MUSICALE <> 0,(PERF_UTILIZZAZIONE.PUNTI_ORIGINALI / PERF_MOVIMENTO_CONTABILE.PUNTI_PROGRAMMA_MUSICALE) * PERF_MOVIMENTO_CONTABILE.IMPORTO_VALORIZZATO,0) AS VALORE_ORIGINALE, "
                + "		IF(PERF_MOVIMENTO_CONTABILE.PUNTI_PROGRAMMA_MUSICALE <> 0, (PERF_UTILIZZAZIONE.PUNTI_RICALCOLATI / PERF_MOVIMENTO_CONTABILE.PUNTI_PROGRAMMA_MUSICALE) * PERF_MOVIMENTO_CONTABILE.IMPORTO_VALORIZZATO,0) AS VALORE_RICALCOLATO "
                + " FROM " + " PERF_UTILIZZAZIONE "
                + " RIGHT JOIN PERF_MOVIMENTO_CONTABILE ON PERF_UTILIZZAZIONE.ID_PROGRAMMA_MUSICALE = PERF_MOVIMENTO_CONTABILE.ID_PROGRAMMA_MUSICALE "
                + "		WHERE " + " PERF_UTILIZZAZIONE.ID_PROGRAMMA_MUSICALE = " + idProgrammaMusicale + " "
                + " ORDER BY ID_UTILIZZAZIONE) AS UT " + "GROUP BY UT.ID_UTILIZZAZIONE;";

        Query q = entityManager.createNativeQuery(query);
        result = q.getResultList();

        for (Object[] objects : result) {
            Utilizzazione utilizzazione = new Utilizzazione();
            utilizzazione.setId((Long) objects[0]);
            utilizzazione.setIdProgrammaMusicale((Long) objects[1]);
            utilizzazione.setCodiceOpera((String) objects[2]);
            utilizzazione.setTitoloComposizione((String) objects[3]);
            utilizzazione.setCompositore((String) objects[4]);
            utilizzazione.setNumeroProgrammaMusicale((Long) objects[5]);
            if (((Long) objects[6]) != null) {
                utilizzazione.setDurata(((Long) objects[6]).doubleValue());
            }
            utilizzazione.setDurataInferiore30sec((String) objects[7]);
            utilizzazione.setAutori((String) objects[8]);
            utilizzazione.setInterpreti((String) objects[9]);
            utilizzazione.setFlagCodifica((String) objects[10]);
            utilizzazione.setCombinazioneCodifica((String) objects[11]);
            utilizzazione.setFlagPubblicoDominio((String) objects[12]);
            utilizzazione.setFlagMagNonConcessa((String) objects[13]);
            utilizzazione.setTipoAccertamento((String) objects[14]);
            utilizzazione.setNettoMaturato((Double) objects[15]);
            utilizzazione.setDataPrimaCodifica((Date) objects[16]);
            if ((BigDecimal) objects[18] != null) {
                utilizzazione.setPuntiOriginali(((BigDecimal) objects[18]).doubleValue());
            } else {
                utilizzazione.setPuntiOriginali(null);
            }
            utilizzazione.setIdCombana((Long) objects[19]);
            utilizzazione.setIdCodifica((Long) objects[20]);
            if ((BigDecimal) objects[21] != null) {
                utilizzazione.setPuntiRicalcolati(((BigDecimal) objects[21]).doubleValue());
            } else {
                utilizzazione.setPuntiRicalcolati(null);
            }
            utilizzazione.setFlagCedolaEsclusa((String) objects[22]);
            if ((BigDecimal) objects[23] != null) {
                if (utilizzazione.getPuntiOriginali() != null) {
                    utilizzazione.setNettoOriginario(((BigDecimal) objects[23]).doubleValue());
                } else {
                    utilizzazione.setNettoOriginario(null);
                }
            } else {
                utilizzazione.setNettoOriginario(null);
            }
            if ((BigDecimal) objects[24] != null) {
                if (utilizzazione.getPuntiRicalcolati() != null) {
                    utilizzazione.setImportoValorizzato(((BigDecimal) objects[24]).doubleValue());
                } else {
                    utilizzazione.setImportoValorizzato(null);
                }
            } else {
                utilizzazione.setImportoValorizzato(null);
            }try{
                if (utilizzazione.getIdCombana() != null) {
                    String queryApprovazione = "SELECT TIPO_APPROVAZIONE " +
                            "FROM PERF_UTILIZZAZIONE AA JOIN PERF_CODIFICA AB " +
                            "ON AA.ID_COMBANA = AB.ID_COMBANA " +
                            "WHERE ID_UTILIZZAZIONE = " + utilizzazione.getId();

                    Query qApprovazione = entityManager.createNativeQuery(queryApprovazione);
                    String approvazioneResult = (String) qApprovazione.getSingleResult();
                    utilizzazione.setTipoApprovazione(approvazioneResult);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            listaOpere.add(utilizzazione);
        }
        if (listaOpere != null && listaOpere.size() > 0) {
            return listaOpere;
        } else {
            return new ArrayList<>();
        }

    }

    public DirettoreEsecuzione getDirettoreEsecuzione(Long idProgrammaMusicale) {

        EntityManager entityManager = provider.get();
        List<DirettoreEsecuzione> listaDirettoriEsecuzione = null;

        String query = "select d " + " from DirettoreEsecuzione d "
                + " where d.idProgrammaMusicale=:idProgrammaMusicale";

        Query q = entityManager.createQuery(query);

        q.setParameter("idProgrammaMusicale", idProgrammaMusicale);

        listaDirettoriEsecuzione = q.getResultList();

        if (listaDirettoriEsecuzione != null && listaDirettoriEsecuzione.size() > 0) {
            return (DirettoreEsecuzione) listaDirettoriEsecuzione.get(0);
        } else {
            return null;
        }

    }

    public Cartella getCartellaById(Long idCartella) {

        EntityManager entityManager = provider.get();
        Cartella cartella = entityManager.find(Cartella.class, idCartella);

        String seprag = cartella.getSeprag();

        String query = "select denominazione " + "from circoscrizione " + "where cod_seprag=?";

        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, seprag);

        List<Object> results = (List<Object>) q.getResultList();

        if (results != null && results.size() > 0) {

            String denomminazione = (String) results.get(0);

            cartella.setDescrizionePuntoTerritoriale(denomminazione);
        }

        return cartella;

    }

    public long getCountListaMovimentazioni(Long inizioPeriodoContabile, Long finePeriodoContabile, Long evento,
                                            String fattura, Long reversale, String voceIncasso, Date dataInizioEvento, Date dataFineEvento,
                                            String seprag, String tipoDocumento, String locale, String sapOrganizzatore, String tipoSupporto,
                                            String tipoProgramma, String direttoreEsecuzione, String titoloOpera, String numeroPM, String permesso,
                                            Integer page, String codiceBALocale, String organizzatore) {
        EntityManager entityManager = provider.get();
        Character flagGruppoPrincipale = null;
        // Arriva come Y o N
        // if (gruppoPrincipale != null && gruppoPrincipale.equals('Y'))
        // flagGruppoPrincipale = '1'; // Gruppo Principale
        // else
        // if (gruppoPrincipale != null && gruppoPrincipale.equals('N'))
        // flagGruppoPrincipale = '0'; // Gruppo Spalla

        String query = null;

        List<MovimentoContabile> listaEventi = new ArrayList<MovimentoContabile>();
        List<MovimentoContabile> listaAllEventi = new ArrayList<MovimentoContabile>();

        ReportPage reportPage = new ReportPage(listaEventi, listaEventi.size(), page);

        int recordCount = 0;

        query = "select count(*) from("
                + "select distinct(e.ID_MOVIMENTO_CONTABILE), e.CONTABILITA, e.CODICE_VOCE_INCASSO, e.ID_EVENTO, e.NUMERO_PERMESSO, e.NUMERO_FATTURA, e.NUMERO_REVERSALE, "
                + "e.TIPO_DOCUMENTO_CONTABILE, e.NUMERO_PM, e.TOTALE_DEM_LORDO_PM, e.IMPORTO_VALORIZZATO, e.ID_PROGRAMMA_MUSICALE "
                + "from PERF_MOVIMENTO_CONTABILE e "
                + "LEFT JOIN PERF_IMPORTO_RICALCOLATO ir on e.ID_EVENTO=ir.ID_EVENTO "
                + "LEFT JOIN PERF_MANIFESTAZIONE f ON e.ID_EVENTO=f.ID_EVENTO "
                + "LEFT JOIN PERF_EVENTI_PAGATI ep on e.ID_EVENTO=ep.ID_EVENTO " +
                // "LEFT JOIN PERF_UTILIZZAZIONE u on
                // e.ID_PROGRAMMA_MUSICALE=u.ID_PROGRAMMA_MUSICALE " +
                "LEFT JOIN PERF_DIRETTORE_ESECUZIONE d on e.ID_PROGRAMMA_MUSICALE=d.ID_PROGRAMMA_MUSICALE "
                + "where (?1 is null or e.CONTABILITA>=?1) " + "and (?2 is null or e.CONTABILITA<=?2) "
                + "and (?3 is null or e.TIPO_DOCUMENTO_CONTABILE=?3) " + "and (?4 is null or e.ID_EVENTO=?4) "
                + "and (?5 is null or e.NUMERO_FATTURA=?5) " + "and (?6 is null or e.NUMERO_REVERSALE=?6) "
                + "and (?7 is null or e.CODICE_VOCE_INCASSO=?7) " + "and (?8 is null or e.NUMERO_PERMESSO=?8) "
                + "and (?9 is null or e.NUMERO_PM=?9) " + "and (?10 is null or ep.SEPRAG=?10) "
                + "and (?11 is null or upper(f.DENOMINAZIONE_LOCALE) like upper(?11)) "
                + "and (?12 is null or f.CODICE_BA_LOCALE=?12) "
                + "and (?13 is null or f.CODICE_SAP_ORGANIZZATORE=?13) "
                + "and (?14 is null or f.PARTITA_IVA_ORGANIZZATORE=?14) "
                + "and (?15 is null or f.CODICE_FISCALE_ORGANIZZATORE=?15) "
                + "and (?16 is null or f.DATA_INIZIO_EVENTO=?16) " + "and (?17 is null or f.DATA_FINE_EVENTO=?17) "
                + "and (?18 is null or upper(f.TITOLO) like upper(?18)) "
                + "and (?19 is null or d.DIRETTORE_ESECUZIONE= upper(?19)) "
                + "and (?20 is null or d.CODICE_FISCALE= upper(?20)) ) as c";

        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, inizioPeriodoContabile);
        q.setParameter(2, finePeriodoContabile);
        q.setParameter(3, tipoDocumento);

        q.setParameter(4, evento);

        if (fattura != null)
            q.setParameter(5, Long.parseLong(fattura));
        else
            q.setParameter(5, fattura);

        q.setParameter(6, reversale);

        q.setParameter(7, voceIncasso);

        if (permesso != null)
            q.setParameter(8, permesso.toString());
        else
            q.setParameter(8, null);

        q.setParameter(9, numeroPM);

        q.setParameter(10, seprag);

        if (locale != null) {
            q.setParameter(11, "%" + locale + "%");
        } else {
            q.setParameter(11, null);
        }

        q.setParameter(12, codiceBALocale);
        q.setParameter(13, sapOrganizzatore);

        if (organizzatore != null && Character.isDigit(organizzatore.charAt(0))) {
            q.setParameter(14, organizzatore);
        } else {
            q.setParameter(14, null);
        }

        if (organizzatore != null && Character.isLetter(organizzatore.charAt(0))) {
            q.setParameter(15, organizzatore);
        } else {
            q.setParameter(15, null);
        }

        q.setParameter(16, dataInizioEvento);

        q.setParameter(17, dataFineEvento);

        if (titoloOpera != null) {
            q.setParameter(18, "%" + titoloOpera + "%");
        } else {
            q.setParameter(18, null);
        }
        if (direttoreEsecuzione != null && Character.isLetter(direttoreEsecuzione.charAt(6))) {
            q.setParameter(19, direttoreEsecuzione);
        } else {
            q.setParameter(19, null);
        }

        if (direttoreEsecuzione != null && Character.isDigit(direttoreEsecuzione.charAt(6))) {
            q.setParameter(20, direttoreEsecuzione);
        } else {
            q.setParameter(20, null);
        }
        // if (tipoSupporto !=null || tipoProgramma !=null){
        // q.setParameter("tipoSupporto",tipoSupporto );
        // q.setParameter("tipoProgramma",tipoProgramma );
        // }
        //
        //
        // q.setParameter("flagGruppoPrincipale", flagGruppoPrincipale);

        Long result = (Long) q.getSingleResult();

        return result;

    }

    public List<MovimentoContabile> getListaMovimentazioni(Long inizioPeriodoContabile, Long finePeriodoContabile,
                                                           Long evento, String fattura, Long reversale, String voceIncasso, Date dataInizioEvento, Date dataFineEvento,
                                                           String seprag, String tipoDocumento, String locale, String sapOrganizzatore, String tipoSupporto,
                                                           String tipoProgramma, String direttoreEsecuzione, String titoloOpera, String numeroPM, String permesso,
                                                           Integer page, String codiceBALocale, String organizzatore, String order) {
        return getListaMovimentazioni(inizioPeriodoContabile, finePeriodoContabile, evento, fattura, reversale,
                voceIncasso, dataInizioEvento, dataFineEvento, seprag, tipoDocumento, locale, sapOrganizzatore,
                tipoSupporto, tipoProgramma, direttoreEsecuzione, titoloOpera, numeroPM, permesso, page, codiceBALocale,
                organizzatore, order, false);
    }

    public List<MovimentoContabile> getListaMovimentazioni(Long inizioPeriodoContabile, Long finePeriodoContabile,
                                                           Long evento, String fattura, Long reversale, String voceIncasso, Date dataInizioEvento, Date dataFineEvento,
                                                           String seprag, String tipoDocumento, String locale, String sapOrganizzatore, String tipoSupporto,
                                                           String tipoProgramma, String direttoreEsecuzione, String titoloOpera, String numeroPM, String permesso,
                                                           Integer page, String codiceBALocale, String organizzatore, String order, boolean allList) {

        EntityManager entityManager = provider.get();
        Character flagGruppoPrincipale = null;
        // Arriva come Y o N
        // if (gruppoPrincipale != null && gruppoPrincipale.equals('Y'))
        // flagGruppoPrincipale = '1'; // Gruppo Principale
        // else
        // if (gruppoPrincipale != null && gruppoPrincipale.equals('N'))
        // flagGruppoPrincipale = '0'; // Gruppo Spalla

        String query = null;

        List<MovimentoContabile> listaEventi = new ArrayList<MovimentoContabile>();
        List<MovimentoContabile> listaAllEventi = new ArrayList<MovimentoContabile>();

        ReportPage reportPage = new ReportPage(listaEventi, listaEventi.size(), page);

        int recordCount = 0;

        query = "select distinct(e.ID_MOVIMENTO_CONTABILE), e.CONTABILITA, e.CODICE_VOCE_INCASSO, e.ID_EVENTO, e.NUMERO_PERMESSO, e.NUMERO_FATTURA, e.NUMERO_REVERSALE, "
                + "e.TIPO_DOCUMENTO_CONTABILE, e.NUMERO_PM, e.TOTALE_DEM_LORDO_PM, e.IMPORTO_VALORIZZATO, e.ID_PROGRAMMA_MUSICALE "
                + "from PERF_MOVIMENTO_CONTABILE e "
                + "LEFT JOIN PERF_IMPORTO_RICALCOLATO ir on e.ID_EVENTO=ir.ID_EVENTO "
                + "LEFT JOIN PERF_MANIFESTAZIONE f ON e.ID_EVENTO=f.ID_EVENTO "
                + "LEFT JOIN PERF_EVENTI_PAGATI ep on e.ID_EVENTO=ep.ID_EVENTO " +
                // "LEFT JOIN PERF_UTILIZZAZIONE u on
                // e.ID_PROGRAMMA_MUSICALE=u.ID_PROGRAMMA_MUSICALE " +
                "LEFT JOIN PERF_DIRETTORE_ESECUZIONE d on e.ID_PROGRAMMA_MUSICALE=d.ID_PROGRAMMA_MUSICALE "
                + "where (?1 is null or e.CONTABILITA>=?1) " + "and (?2 is null or e.CONTABILITA<=?2) "
                + "and (?3 is null or e.TIPO_DOCUMENTO_CONTABILE=?3) " + "and (?4 is null or e.ID_EVENTO=?4) "
                + "and (?5 is null or e.NUMERO_FATTURA=?5) " + "and (?6 is null or e.NUMERO_REVERSALE=?6) "
                + "and (?7 is null or e.CODICE_VOCE_INCASSO=?7) " + "and (?8 is null or e.NUMERO_PERMESSO=?8) "
                + "and (?9 is null or e.NUMERO_PM=?9) " + "and (?10 is null or ep.SEPRAG=?10) "
                + "and (?11 is null or upper(f.DENOMINAZIONE_LOCALE) like upper(?11)) "
                + "and (?12 is null or f.CODICE_BA_LOCALE=?12) "
                + "and (?13 is null or f.CODICE_SAP_ORGANIZZATORE=?13) "
                + "and (?14 is null or f.PARTITA_IVA_ORGANIZZATORE=?14) "
                + "and (?15 is null or f.CODICE_FISCALE_ORGANIZZATORE=?15) "
                + "and (?16 is null or f.DATA_INIZIO_EVENTO=?16) " + "and (?17 is null or f.DATA_FINE_EVENTO=?17) "
                + "and (?18 is null or upper(f.TITOLO) like upper(?18)) "
                + "and (?19 is null or d.DIRETTORE_ESECUZIONE= upper(?19)) "
                + "and (?20 is null or d.CODICE_FISCALE= upper(?20)) ";

        if (order == null) {
            query = query + "order by e.ID_EVENTO, e.NUMERO_FATTURA, e.CODICE_VOCE_INCASSO desc";

        } else if (order.startsWith("IMPORTO")) {
            query = query + "order by ir." + order;
        } else {
            query = query + "order by e." + order;
        }

        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, inizioPeriodoContabile);
        q.setParameter(2, finePeriodoContabile);
        q.setParameter(3, tipoDocumento);

        q.setParameter(4, evento);

        if (fattura != null)
            q.setParameter(5, Long.parseLong(fattura));
        else
            q.setParameter(5, fattura);

        q.setParameter(6, reversale);

        q.setParameter(7, voceIncasso);

        if (permesso != null)
            q.setParameter(8, permesso.toString());
        else
            q.setParameter(8, null);

        q.setParameter(9, numeroPM);

        q.setParameter(10, seprag);

        if (locale != null) {
            q.setParameter(11, "%" + locale + "%");
        } else {
            q.setParameter(11, null);
        }

        q.setParameter(12, codiceBALocale);
        q.setParameter(13, sapOrganizzatore);

        if (organizzatore != null && Character.isDigit(organizzatore.charAt(0))) {
            q.setParameter(14, organizzatore);
        } else {
            q.setParameter(14, null);
        }

        if (organizzatore != null && Character.isLetter(organizzatore.charAt(0))) {
            q.setParameter(15, organizzatore);
        } else {
            q.setParameter(15, null);
        }

        q.setParameter(16, dataInizioEvento);

        q.setParameter(17, dataFineEvento);

        if (titoloOpera != null) {
            q.setParameter(18, "%" + titoloOpera + "%");
        } else {
            q.setParameter(18, null);
        }
        if (direttoreEsecuzione != null && Character.isLetter(direttoreEsecuzione.charAt(6))) {
            q.setParameter(19, direttoreEsecuzione);
        } else {
            q.setParameter(19, null);
        }

        if (direttoreEsecuzione != null && Character.isDigit(direttoreEsecuzione.charAt(6))) {
            q.setParameter(20, direttoreEsecuzione);
        } else {
            q.setParameter(20, null);
        }
        // if (tipoSupporto !=null || tipoProgramma !=null){
        // q.setParameter("tipoSupporto",tipoSupporto );
        // q.setParameter("tipoProgramma",tipoProgramma );
        // }
        //
        //
        // q.setParameter("flagGruppoPrincipale", flagGruppoPrincipale);

        int startRecord = 0;
        int endRecord = recordCount;

        if (page != -1) {

            startRecord = (page) * ReportPage.NUMBER_RECORD_PER_PAGE;
            endRecord = ReportPage.NUMBER_RECORD_PER_PAGE;
        }
        List<Object[]> results;

        if (allList) {
            results = (List<Object[]>) q.getResultList();
        } else {
            results = (List<Object[]>) q.setFirstResult(startRecord).setMaxResults(endRecord).getResultList();
        }
        for (int i = 0; i < results.size(); i++) {

            MovimentoContabile movimentoContabile = null;
            movimentoContabile = new MovimentoContabile();

            movimentoContabile.setContabilita(((Long) results.get(i)[1]).intValue());
            movimentoContabile.setVoceIncasso(((String) results.get(i)[2]));
            movimentoContabile.setIdEvento((Long) results.get(i)[3]);
            movimentoContabile.setNumeroPermesso((String) results.get(i)[4]);
            movimentoContabile.setNumeroFattura(((Long) results.get(i)[5]));
            movimentoContabile.setReversale(((Long) results.get(i)[6]));
            movimentoContabile.setTipoDocumentoContabile(((String) results.get(i)[7]));
            movimentoContabile.setNumProgrammaMusicale(String.valueOf(results.get(i)[8]));

            if (results.get(i)[9] != null)
                movimentoContabile.setImportoTotDemLordo(((BigDecimal) results.get(i)[9]).doubleValue());
            if (results.get(i)[10] != null) {
                movimentoContabile.setImportoRicalcolato(((BigDecimal) results.get(i)[10]).doubleValue());
            }
            movimentoContabile.setIdProgrammaMusicale((Long) results.get(i)[11]);
            movimentoContabile.setId((Long) results.get(i)[0]);
            // movimentoContabile.setEvento(manifestazione);

            // if (i>=startRecord && i<endRecord)
            // listaEventi.add(movimentoContabile);

            listaAllEventi.add(movimentoContabile);
        }

        return listaAllEventi;
    }

    public ReportPage getListaAggregatiSiada(Long inizioPeriodoContabile, Long finePeriodoContabile, String voceIncasso,
                                             String tipoSupporto, String tipoProgramma, Long numeroPM, Integer page) {

        EntityManager entityManager = provider.get();
        String query = null;

        List<MovimentoSiada> listaAggregati = new ArrayList<MovimentoSiada>();
        List<MovimentoSiada> listaAllAggregati = new ArrayList<MovimentoSiada>();

        ReportPage reportPage = new ReportPage(listaAggregati, listaAggregati.size(), page);

        int recordCount = 0;

        query = "select e.id, e.idEvento, e.idProgrammaMusicale, e.dataInizioEvento, e.dataFineEvento, e.voceIncasso, e.numeroFattura,e.importo, e.meseContabile, e.meseInoltroSiada, p.numeroProgrammaMusicale, e.meseContabileOrig, e.dataElaborazione, e.voceIncassoSiada "
                + "from MovimentoSiada e, ProgrammaMusicale p ";

        query = query + "where e.meseContabile>=:inizioPeriodoContabile and e.meseContabile<=:finePeriodoContabile "
                + "and (:voceIncasso is null or e.voceIncasso=:voceIncasso) " + "and p.id = e.idProgrammaMusicale "
                + "and (:tipoSupporto is null or p.tipologiaPm=:tipoSupporto) "
                + "and (:numProgrammaMusicale is null or p.numeroProgrammaMusicale=:numProgrammaMusicale) "
                + "and (:tipoProgramma is null or p.tipoPm=:tipoProgramma) ";

        query = query + "order by e.idProgrammaMusicale, e.voceIncasso asc ";

        Query q = entityManager.createQuery(query);

        q.setParameter("inizioPeriodoContabile", inizioPeriodoContabile);
        q.setParameter("finePeriodoContabile", finePeriodoContabile);
        q.setParameter("voceIncasso", voceIncasso);
        q.setParameter("tipoSupporto", tipoSupporto);
        q.setParameter("tipoProgramma", tipoProgramma);
        q.setParameter("numProgrammaMusicale", numeroPM);

        List<Object[]> results = (List<Object[]>) q.getResultList();

        if (results != null && results.size() > 0) {

            recordCount = results.size();

            int startRecord = 0;
            int endRecord = recordCount;

            if (page != -1) {

                startRecord = (page - 1) * ReportPage.NUMBER_RECORD_PER_PAGE;
                endRecord = 0;
                if (recordCount > ReportPage.NUMBER_RECORD_PER_PAGE) {
                    endRecord = startRecord + ReportPage.NUMBER_RECORD_PER_PAGE;
                    if (endRecord > recordCount)
                        endRecord = recordCount - 1;
                } else {
                    endRecord = recordCount;
                }

            }

            MovimentoSiada aggregatoSiada = null;

            for (int i = 0; i < recordCount; i++) {

                // e.id, e.idEvento, e.idProgrammaMusicale, e.dataInizioEvento,
                // e.dataFineEvento, e.voceIncasso, e.numeroFattura,e.importo, e.meseContabile,
                // e.meseInoltroSiada, p.numeroProgrammaMusicale
                aggregatoSiada = new MovimentoSiada();
                aggregatoSiada.setId((Long) results.get(i)[0]);
                aggregatoSiada.setIdEvento((Long) results.get(i)[1]);
                aggregatoSiada.setIdProgrammaMusicale((Long) results.get(i)[2]);
                aggregatoSiada.setDataInizioEvento((Date) results.get(i)[3]);
                aggregatoSiada.setDataFineEvento((Date) results.get(i)[4]);
                aggregatoSiada.setVoceIncasso((String) results.get(i)[5]);
                aggregatoSiada.setNumeroFattura((Long) results.get(i)[6]);
                aggregatoSiada.setImporto((Double) results.get(i)[7]);
                aggregatoSiada.setMeseContabile((Integer) results.get(i)[8]);
                aggregatoSiada.setMeseInoltroSiada((Integer) results.get(i)[9]);
                aggregatoSiada.setNumeroProgrammaMusicale((Long) results.get(i)[10]);
                aggregatoSiada.setMeseContabileOrig((Integer) results.get(i)[11]);
                aggregatoSiada.setDataElaborazione((Date) results.get(i)[12]);
                aggregatoSiada.setVoceIncassoSiada((String) results.get(i)[13]);
                // aggregatoSiada = (MovimentoSiada)results.get(i);

                if (i >= startRecord && i < endRecord)
                    listaAggregati.add(aggregatoSiada);

                listaAllAggregati.add(aggregatoSiada);
            }

            // Ordina la lista aggregati per voce di incasso
            reportPage = new ReportPage(listaAllAggregati, listaAggregati, recordCount, page);

        }

        return reportPage;

    }

    public List<AggregatoVoce> getListaDocumentiEventi(Long inizioPeriodoContabile, Long finePeriodoContabile,
                                                       String idPuntoTerritoriale) {

        EntityManager entityManager = provider.get();
        String query = null;

        List<AggregatoVoce> listaAggregati = new ArrayList<AggregatoVoce>();

        query = "select voce_incasso, tipo_documento_contabile AS TIPO_DOCUMENTO, count(tipo_documento_contabile) AS NUMERO_DOCUMENTI, sum(decode(tipo_documento_contabile,'501',importo_dem,-importo_dem)) as IMPORTO, count(distinct(id_evento)) as NUMERO_EVENTI "
                + "from eventi_pagati " + "where CONTABILITA>=? and contabilita<=? ";

        if (idPuntoTerritoriale != null)
            query = query + "and seprag=? ";

        // query = query + "group by voce_incasso, tipo_documento_contabile ";

        query = query + "group by voce_incasso, tipo_documento_contabile " + "order by tipo_documento_contabile desc ";

        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, inizioPeriodoContabile);
        q.setParameter(2, finePeriodoContabile);

        if (idPuntoTerritoriale != null)
            q.setParameter(3, idPuntoTerritoriale);

        List<Object[]> results = (List<Object[]>) q.getResultList();

        if (results != null && results.size() > 0) {

            AggregatoVoce aggregatoVoce = null;

            for (int i = 0; i < results.size(); i++) {

                aggregatoVoce = new AggregatoVoce();
                aggregatoVoce.setVoceIncasso((String) results.get(i)[0]);
                aggregatoVoce.setTipoDocumento((String) results.get(i)[1]);
                aggregatoVoce.setNumeroDocumenti(((BigDecimal) results.get(i)[2]).longValue());
                aggregatoVoce.setImporto(((BigDecimal) results.get(i)[3]).doubleValue());
                aggregatoVoce.setNumeroEventi(((BigDecimal) results.get(i)[4]).longValue());

                listaAggregati.add(aggregatoVoce);
            }

        }

        return listaAggregati;

    }

    public List<AggregatoVoce> getListaEventi(Long inizioPeriodoContabile, Long finePeriodoContabile,
                                              String idPuntoTerritoriale) {

        EntityManager entityManager = provider.get();
        String query = null;

        List<AggregatoVoce> listaAggregati = new ArrayList<AggregatoVoce>();

        query = "select voce_incasso, count(distinct(id_evento)) as NUMERO_EVENTI " + "from eventi_pagati "
                + "where CONTABILITA>=? and contabilita<=? ";

        if (idPuntoTerritoriale != null)
            query = query + "and seprag=? ";

        query = query + "group by voce_incasso";

        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, inizioPeriodoContabile);
        q.setParameter(2, finePeriodoContabile);

        if (idPuntoTerritoriale != null)
            q.setParameter(3, idPuntoTerritoriale);

        List<Object[]> results = (List<Object[]>) q.getResultList();

        if (results != null && results.size() > 0) {

            AggregatoVoce aggregatoVoce = null;

            for (int i = 0; i < results.size(); i++) {

                aggregatoVoce = new AggregatoVoce();
                aggregatoVoce.setVoceIncasso((String) results.get(i)[0]);
                aggregatoVoce.setNumeroEventi(((BigDecimal) results.get(i)[1]).longValue());

                listaAggregati.add(aggregatoVoce);
            }

        }

        return listaAggregati;

    }

    public List<AggregatoVoce> getImportoNettiPMCorrenti(Long inizioPeriodoContabile, Long finePeriodoContabile,
                                                         String idPuntoTerritoriale) {

        EntityManager entityManager = provider.get();
        // PM Correnti: 501-201 (la sottrazione degli assegnati a DG è automatica perchè
        // la join tra le due tabelle per il periodo di riferimento non fornisce nulla)
        // null 501
        // Liquidazione 501
        // Storno 221
        String query = null;

        List<AggregatoVoce> listaAggregati = new ArrayList<AggregatoVoce>();

        if (idPuntoTerritoriale != null) {

            query = "select p.voce_incasso, sum(decode(m.tipo_documento_contabile,'501',p.importo_dem,-p.importo_dem)) as IMPORTO "
                    + "from movimento_contabile_158 m, eventi_pagati p, cartella c "
                    + "where m.codice_voce_incasso=p.voce_incasso " + "and m.id_evento=p.id_evento "
                    + "and m.ID_CARTELLA=c.ID_CARTELLA " + "and m.numero_fattura=p.numero_fattura "
                    + "and m.contabilita>=? and m.contabilita<=? " + "and p.contabilita>=? and p.contabilita<=? "
                    + "and (c.CODICE_SEDE||c.CODICE_PROVINCIA||c.UNITA_TERRITORIALE=?) " + "group by p.voce_incasso";

        } else {

            query = "select p.voce_incasso, sum(decode(m.tipo_documento_contabile,'501',p.importo_dem,-p.importo_dem)) as IMPORTO "
                    + "from movimento_contabile_158 m, eventi_pagati p " + "where m.codice_voce_incasso=p.voce_incasso "
                    + "and m.id_evento=p.id_evento " + "and m.numero_fattura=p.numero_fattura "
                    + "and m.contabilita>=? and m.contabilita<=? " + "and p.contabilita>=? and p.contabilita<=? "
                    + "group by p.voce_incasso";
        }

        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, inizioPeriodoContabile);
        q.setParameter(2, finePeriodoContabile);
        q.setParameter(3, inizioPeriodoContabile);
        q.setParameter(4, finePeriodoContabile);

        if (idPuntoTerritoriale != null)
            q.setParameter(5, idPuntoTerritoriale);

        List<Object[]> results = (List<Object[]>) q.getResultList();

        if (results != null && results.size() > 0) {

            AggregatoVoce aggregatoVoce = null;

            for (int i = 0; i < results.size(); i++) {

                aggregatoVoce = new AggregatoVoce();
                aggregatoVoce.setVoceIncasso((String) results.get(i)[0]);
                aggregatoVoce.setImporto(((BigDecimal) results.get(i)[1]).doubleValue());

                listaAggregati.add(aggregatoVoce);
            }

        }

        return listaAggregati;

    }

    // Importo Programmato effettivo per Voce Incasso
    public List<AggregatoVoce> getImportoProgrammatoCorrenti(Long inizioPeriodoContabile, Long finePeriodoContabile,
                                                             String idPuntoTerritoriale) {

        EntityManager entityManager = provider.get();

        String query = null;

        List<AggregatoVoce> listaAggregati = new ArrayList<AggregatoVoce>();

        if (idPuntoTerritoriale != null) {

            query = "select m.codice_voce_incasso, sum(importo) as IMPORTO "
                    + "from movimenti_siada m, movimento_contabile_158 mo, cartella c "
                    + "where m.id_programma_musicale=mo.id_programma_musicale " + "and mo.ID_CARTELLA=c.ID_CARTELLA "
                    + "and m.mese_contabile>=? and m.mese_contabile<=? "
                    + "and (c.CODICE_SEDE||c.CODICE_PROVINCIA||c.UNITA_TERRITORIALE=?) "
                    + "group by m.codice_voce_incasso";
        } else {

            query = "select m.codice_voce_incasso, sum(importo) as IMPORTO "
                    + "from movimenti_siada m, movimento_contabile_158 mo "
                    + "where m.id_programma_musicale=mo.id_programma_musicale "
                    + "and m.mese_contabile>=? and m.mese_contabile<=? " + "group by m.codice_voce_incasso";
        }

        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, inizioPeriodoContabile);
        q.setParameter(2, finePeriodoContabile);

        if (idPuntoTerritoriale != null)
            q.setParameter(3, idPuntoTerritoriale);

        List<Object[]> results = (List<Object[]>) q.getResultList();

        if (results != null && results.size() > 0) {

            AggregatoVoce aggregatoVoce = null;

            for (int i = 0; i < results.size(); i++) {

                aggregatoVoce = new AggregatoVoce();
                aggregatoVoce.setVoceIncasso((String) results.get(i)[0]);
                aggregatoVoce.setImportoProgrammato(((BigDecimal) results.get(i)[1]).doubleValue());

                listaAggregati.add(aggregatoVoce);
            }

        }

        return listaAggregati;

    }

    // Numero PM e Numero Cedole per Voce Incasso
    public List<AggregatoVoce> getNumeroPMCorrenti(Long inizioPeriodoContabile, Long finePeriodoContabile,
                                                   String idPuntoTerritoriale) {

        EntityManager entityManager = provider.get();
        String query = null;

        List<AggregatoVoce> listaAggregati = new ArrayList<AggregatoVoce>();

        if (idPuntoTerritoriale != null) {

            query = "select m.codice_voce_incasso, count(distinct(p.NUMERO_PROGRAMMA_MUSICALE)), sum(m.importo) "
                    + "from movimenti_siada m, movimento_contabile_158 mo, programma_musicale p, cartella c "
                    + "where m.ID_PROGRAMMA_MUSICALE=p.ID_PROGRAMMA_MUSICALE "
                    + "and m.ID_PROGRAMMA_MUSICALE=mo.ID_PROGRAMMA_MUSICALE " + "and mo.ID_CARTELLA=c.ID_CARTELLA "
                    + "and m.mese_contabile>=? and m.mese_contabile<=? "
                    + "and (c.CODICE_SEDE||c.CODICE_PROVINCIA||c.UNITA_TERRITORIALE=?) "
                    + "group by m.codice_voce_incasso ";

        } else {

            query = "select m.codice_voce_incasso, count(distinct(p.NUMERO_PROGRAMMA_MUSICALE)), sum(m.importo) "
                    + "from movimenti_siada m, programma_musicale p "
                    + "where m.ID_PROGRAMMA_MUSICALE=p.ID_PROGRAMMA_MUSICALE "
                    + "and m.mese_contabile>=? and m.mese_contabile<=? " + "group by m.codice_voce_incasso ";
        }

        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, inizioPeriodoContabile);
        q.setParameter(2, finePeriodoContabile);

        if (idPuntoTerritoriale != null)
            q.setParameter(3, idPuntoTerritoriale);

        List<Object[]> results = (List<Object[]>) q.getResultList();

        if (results != null && results.size() > 0) {

            AggregatoVoce aggregatoVoce = null;

            for (int i = 0; i < results.size(); i++) {

                aggregatoVoce = new AggregatoVoce();
                aggregatoVoce.setVoceIncasso((String) results.get(i)[0]);
                aggregatoVoce.setNumeroPM(((BigDecimal) results.get(i)[1]).longValue());
                aggregatoVoce.setImporto(((BigDecimal) results.get(i)[2]).doubleValue());

                listaAggregati.add(aggregatoVoce);
            }

        }

        return listaAggregati;

    }

    // Numero PM e Numero Cedole per Voce Incasso
    public List<AggregatoVoce> getNumeroCedoleCorrenti(Long inizioPeriodoContabile, Long finePeriodoContabile,
                                                       String idPuntoTerritoriale) {

        EntityManager entityManager = provider.get();
        String query = null;

        List<AggregatoVoce> listaAggregati = new ArrayList<AggregatoVoce>();

        if (idPuntoTerritoriale != null) {

            query = "select m.codice_voce_incasso, count(distinct(p.NUMERO_PROGRAMMA_MUSICALE)), count(u.TITOLO_COMPOSIZIONE) "
                    + "from movimenti_siada m, movimento_contabile_158 mo, programma_musicale p, utilizzazione u, cartella c "
                    + "where m.ID_PROGRAMMA_MUSICALE=p.ID_PROGRAMMA_MUSICALE "
                    + "and m.ID_PROGRAMMA_MUSICALE=mo.ID_PROGRAMMA_MUSICALE "
                    + "and p.ID_PROGRAMMA_MUSICALE=u.ID_PROGRAMMA_MUSICALE " + "and mo.ID_CARTELLA=c.ID_CARTELLA "
                    + "and m.mese_contabile>=? and m.mese_contabile<=? "
                    + "and (c.CODICE_SEDE||c.CODICE_PROVINCIA||c.UNITA_TERRITORIALE=?) "
                    + "group by m.codice_voce_incasso ";

        } else {

            query = "select m.codice_voce_incasso, count(distinct(p.NUMERO_PROGRAMMA_MUSICALE)), count(u.TITOLO_COMPOSIZIONE) "
                    + "from movimenti_siada m, programma_musicale p, utilizzazione u "
                    + "where m.ID_PROGRAMMA_MUSICALE=p.ID_PROGRAMMA_MUSICALE "
                    + "and p.ID_PROGRAMMA_MUSICALE=u.ID_PROGRAMMA_MUSICALE "
                    + "and m.mese_contabile>=? and m.mese_contabile<=? " + "group by m.codice_voce_incasso ";
        }

        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, inizioPeriodoContabile);
        q.setParameter(2, finePeriodoContabile);

        if (idPuntoTerritoriale != null)
            q.setParameter(3, idPuntoTerritoriale);

        List<Object[]> results = (List<Object[]>) q.getResultList();

        if (results != null && results.size() > 0) {

            AggregatoVoce aggregatoVoce = null;

            for (int i = 0; i < results.size(); i++) {

                aggregatoVoce = new AggregatoVoce();
                aggregatoVoce.setVoceIncasso((String) results.get(i)[0]);
                aggregatoVoce.setNumeroPM(((BigDecimal) results.get(i)[1]).longValue());
                aggregatoVoce.setNumeroCedole(((BigDecimal) results.get(i)[2]).longValue());

                listaAggregati.add(aggregatoVoce);
            }

        }

        return listaAggregati;

    }

    public List<AggregatoVoce> getImportoNettiPMArretrati(Long inizioPeriodoContabile, Long finePeriodoContabile,
                                                          String idPuntoTerritoriale) {

        EntityManager entityManager = provider.get();
        String query = null;

        List<AggregatoVoce> listaAggregati = new ArrayList<AggregatoVoce>();

        if (idPuntoTerritoriale != null) {

            query = "select codice_voce_incasso, sum(importo) " + "from movimenti_siada s "
                    + "where id_programma_musicale in ( " + " select m.id_programma_musicale "
                    + " from eventi_pagati p, movimento_contabile_158 m, cartella c "
                    + " where m.codice_voce_incasso=p.voce_incasso " + " and m.id_evento=p.id_evento "
                    + " and m.numero_fattura=p.numero_fattura " + " and m.contabilita>=? and m.contabilita<=? "
                    + " and p.contabilita<? " + " and m.ID_CARTELLA=c.ID_CARTELLA "
                    + " and (c.CODICE_SEDE||c.CODICE_PROVINCIA||c.UNITA_TERRITORIALE=?) " + ") "
                    + "group by codice_voce_incasso ";
        } else {

            query = "select codice_voce_incasso, sum(importo) " + "from movimenti_siada s "
                    + "where id_programma_musicale in ( " + " select m.id_programma_musicale "
                    + " from eventi_pagati p, movimento_contabile_158 m "
                    + " where m.codice_voce_incasso=p.voce_incasso " + " and m.id_evento=p.id_evento "
                    + " and m.numero_fattura=p.numero_fattura " + " and m.contabilita>=? and m.contabilita<=? "
                    + " and p.contabilita<? " + ") " + "group by codice_voce_incasso ";
        }

        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, inizioPeriodoContabile);
        q.setParameter(2, finePeriodoContabile);
        q.setParameter(3, inizioPeriodoContabile);

        if (idPuntoTerritoriale != null)
            q.setParameter(4, idPuntoTerritoriale);

        List<Object[]> results = (List<Object[]>) q.getResultList();

        if (results != null && results.size() > 0) {

            AggregatoVoce aggregatoVoce = null;

            for (int i = 0; i < results.size(); i++) {

                aggregatoVoce = new AggregatoVoce();
                aggregatoVoce.setVoceIncasso((String) results.get(i)[0]);
                aggregatoVoce.setImportoProgrammato(((BigDecimal) results.get(i)[1]).doubleValue());

                listaAggregati.add(aggregatoVoce);
            }

        }

        return listaAggregati;

    }

    public List<AggregatoVoce> getNumeroPMArretrati(Long inizioPeriodoContabile, Long finePeriodoContabile,
                                                    String idPuntoTerritoriale) {

        EntityManager entityManager = provider.get();
        String query = null;

        List<AggregatoVoce> listaAggregati = new ArrayList<AggregatoVoce>();

        if (idPuntoTerritoriale != null) {

            query = "select m.codice_voce_incasso, count(distinct(p.NUMERO_PROGRAMMA_MUSICALE)), sum(m.importo) "
                    + "from movimenti_siada m, movimento_contabile_158 mo, programma_musicale p, cartella c, " + " ( "
                    + " select m.id_programma_musicale "
                    + " from eventi_pagati p, movimento_contabile_158 m "
                    + " where m.codice_voce_incasso=p.voce_incasso " + " and m.id_evento=p.id_evento "
                    + " and m.numero_fattura=p.numero_fattura "
                    + " and m.contabilita>=? and m.contabilita<=? " + " and p.contabilita<? " + ") a "
                    + "where m.ID_PROGRAMMA_MUSICALE=p.ID_PROGRAMMA_MUSICALE "
                    + "and p.ID_PROGRAMMA_MUSICALE=a.ID_PROGRAMMA_MUSICALE "
                    + "and m.ID_PROGRAMMA_MUSICALE=mo.ID_PROGRAMMA_MUSICALE " + "and mo.ID_CARTELLA=c.ID_CARTELLA "
                    + "and m.mese_contabile>=? and m.mese_contabile<=? "
                    + "and (c.CODICE_SEDE||c.CODICE_PROVINCIA||c.UNITA_TERRITORIALE=?) "
                    + "group by m.codice_voce_incasso ";

        } else {

            query = "select m.codice_voce_incasso, count(distinct(p.NUMERO_PROGRAMMA_MUSICALE)), sum(m.importo) "
                    + "from movimenti_siada m, programma_musicale p, " + "( " + " select m.id_programma_musicale "
                    + " from eventi_pagati p, movimento_contabile_158 m "
                    + " where m.codice_voce_incasso=p.voce_incasso " + " and m.id_evento=p.id_evento "
                    + " and m.numero_fattura=p.numero_fattura " + " and m.contabilita>=? and m.contabilita<=? "
                    + " and p.contabilita<? ) a " + "where m.ID_PROGRAMMA_MUSICALE=p.ID_PROGRAMMA_MUSICALE "
                    + "and p.ID_PROGRAMMA_MUSICALE=a.ID_PROGRAMMA_MUSICALE "
                    + "and m.mese_contabile>=? and m.mese_contabile<=? " + "group by m.codice_voce_incasso ";
        }

        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, inizioPeriodoContabile);
        q.setParameter(2, finePeriodoContabile);
        q.setParameter(3, inizioPeriodoContabile);
        q.setParameter(4, inizioPeriodoContabile);
        q.setParameter(5, finePeriodoContabile);

        if (idPuntoTerritoriale != null)
            q.setParameter(6, idPuntoTerritoriale);

        List<Object[]> results = (List<Object[]>) q.getResultList();

        if (results != null && results.size() > 0) {

            AggregatoVoce aggregatoVoce = null;

            for (int i = 0; i < results.size(); i++) {

                aggregatoVoce = new AggregatoVoce();
                aggregatoVoce.setVoceIncasso((String) results.get(i)[0]);
                aggregatoVoce.setNumeroPM(((BigDecimal) results.get(i)[1]).longValue());
                aggregatoVoce.setImporto(((BigDecimal) results.get(i)[2]).doubleValue());

                listaAggregati.add(aggregatoVoce);
            }

        }

        return listaAggregati;

    }

    public List<AggregatoVoce> getNumeroCedoleArretrati(Long inizioPeriodoContabile, Long finePeriodoContabile,
                                                        String idPuntoTerritoriale) {

        EntityManager entityManager = provider.get();
        String query = null;

        List<AggregatoVoce> listaAggregati = new ArrayList<AggregatoVoce>();

        if (idPuntoTerritoriale != null) {

            query = "select m.codice_voce_incasso, count(distinct(p.NUMERO_PROGRAMMA_MUSICALE)), count(u.TITOLO_COMPOSIZIONE) "
                    + "from movimenti_siada m, movimento_contabile_158 mo, programma_musicale p, utilizzazione u, cartella c, "
                    + " ( " + " select m.id_programma_musicale "
                    + " from eventi_pagati p, movimento_contabile_158 m "
                    + " where m.codice_voce_incasso=p.voce_incasso " + " and m.id_evento=p.id_evento "
                    + " and m.numero_fattura=p.numero_fattura "
                    + " and m.contabilita>=? and m.contabilita<=? " + " and p.contabilita<? " + ") a "
                    + "where m.ID_PROGRAMMA_MUSICALE=p.ID_PROGRAMMA_MUSICALE "
                    + "and p.ID_PROGRAMMA_MUSICALE=u.ID_PROGRAMMA_MUSICALE "
                    + "and p.ID_PROGRAMMA_MUSICALE=a.ID_PROGRAMMA_MUSICALE "
                    + "and m.ID_PROGRAMMA_MUSICALE=mo.ID_PROGRAMMA_MUSICALE " + "and mo.ID_CARTELLA=c.ID_CARTELLA "
                    + "and m.mese_contabile>=? and m.mese_contabile<=? "
                    + "and (c.CODICE_SEDE||c.CODICE_PROVINCIA||c.UNITA_TERRITORIALE=?) "
                    + "group by m.codice_voce_incasso ";

        } else {

            query = "select m.codice_voce_incasso, count(distinct(p.NUMERO_PROGRAMMA_MUSICALE)), count(u.TITOLO_COMPOSIZIONE)"
                    + "from movimenti_siada m, programma_musicale p, utilizzazione u, " + "( "
                    + " select m.id_programma_musicale " + " from eventi_pagati p, movimento_contabile_158 m "
                    + " where m.codice_voce_incasso=p.voce_incasso " + " and m.id_evento=p.id_evento "
                    + " and m.numero_fattura=p.numero_fattura " + " and m.contabilita>=? and m.contabilita<=? "
                    + " and p.contabilita<? ) a " + "where m.ID_PROGRAMMA_MUSICALE=p.ID_PROGRAMMA_MUSICALE "
                    + "and p.ID_PROGRAMMA_MUSICALE=u.ID_PROGRAMMA_MUSICALE "
                    + "and p.ID_PROGRAMMA_MUSICALE=a.ID_PROGRAMMA_MUSICALE "
                    + "and m.mese_contabile>=? and m.mese_contabile<=? " + "group by m.codice_voce_incasso ";
        }

        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, inizioPeriodoContabile);
        q.setParameter(2, finePeriodoContabile);
        q.setParameter(3, inizioPeriodoContabile);
        q.setParameter(4, inizioPeriodoContabile);
        q.setParameter(5, finePeriodoContabile);

        if (idPuntoTerritoriale != null)
            q.setParameter(6, idPuntoTerritoriale);

        List<Object[]> results = (List<Object[]>) q.getResultList();

        if (results != null && results.size() > 0) {

            AggregatoVoce aggregatoVoce = null;

            for (int i = 0; i < results.size(); i++) {

                aggregatoVoce = new AggregatoVoce();
                aggregatoVoce.setVoceIncasso((String) results.get(i)[0]);
                aggregatoVoce.setNumeroPM(((BigDecimal) results.get(i)[1]).longValue());
                aggregatoVoce.setNumeroCedole(((BigDecimal) results.get(i)[2]).longValue());

                listaAggregati.add(aggregatoVoce);
            }

        }

        return listaAggregati;

    }

    public Double getImportoPMSUNAggregato(Long idProgrammaMusicale) {

        EntityManager entityManager = provider.get();
        Double importo = 0D;
        String query = null;

        // Importo SUN totale
        query = "select sum(if(m.tipo_documento_contabile='501',m.importo_dem_totale,-m.importo_dem_totale)) as IMPORTO "
                + "from PERF_MOVIMENTO_CONTABILE m " + "where m.ID_PROGRAMMA_MUSICALE=? ";

        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, idProgrammaMusicale);

        List<Object> results = (List<Object>) q.getResultList();

        if (results != null && results.size() > 0 && results.get(0) != null && !results.get(0).equals(0)) {

            importo = ((BigDecimal) results.get(0)).doubleValue();
        }

        return importo;

    }

    public Double getImportoPMSIADAAggregato(Long idProgrammaMusicale) {

        EntityManager entityManager = provider.get();
        Double importo = null;
        String query = null;

        // Importo aggregato e inviato a SIADA oppure l'ultimo ricalcolato
        query = "select IMPORTO " + "from PERF_IMPORTO_RICALCOLATO " + "where ID_EVENTO=( "
                + "SELECT ID_EVENTO from PERF_MOVIMENTO_CONTABILE " + "where ID_PROGRAMMA_MUSICALE = ?)";

        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, idProgrammaMusicale);

        List<Object> results = (List<Object>) q.getResultList();

        if (results != null && results.size() > 0) {

            importo = ((BigDecimal) results.get(0)).doubleValue();
        }

        return importo;

    }

    public List<ImportoRicalcolato> getImportiPMRicalcolati(Long idProgrammaMusicale) {

        EntityManager entityManager = provider.get();
        String query = null;

        List<ImportoRicalcolato> listaImportiRicalcolati = new ArrayList<ImportoRicalcolato>();

        // Importo ricalcolato (da considerare se non esiste ancora l'aggregato)
        query = "select ID_ESECUZIONE_RICALCOLO, ID_PROGRAMMA_MUSICALE, IMPORTO " + "from ( "
                + "select i.ID_ESECUZIONE_RICALCOLO, m.ID_PROGRAMMA_MUSICALE, SUM(i.IMPORTO) as IMPORTO "
                + "from PERF_IMPORTO_RICALCOLATO i, PERF_MOVIMENTO_CONTABILE m "
                + "where i.ID_MOVIMENTO_CONTABILE=m.ID_MOVIMENTO_CONTABILE " + "and m.ID_PROGRAMMA_MUSICALE=? "
                + "group by i.ID_ESECUZIONE_RICALCOLO, m.ID_PROGRAMMA_MUSICALE " + "union "
                + "select i.ID_ESECUZIONE_RICALCOLO, m.ID_PROGRAMMA_MUSICALE, SUM(m.IMPORTO_DEM_TOTALE) as IMPORTO "
                + "from PERF_SOSPESI_RICALCOLO i, PERF_MOVIMENTO_CONTABILE m "
                + "where i.ID_MOVIMENTO_CONTABILE=m.ID_MOVIMENTO_CONTABILE " + "and m.ID_PROGRAMMA_MUSICALE=? "
                + "group by i.ID_ESECUZIONE_RICALCOLO, m.ID_PROGRAMMA_MUSICALE " + ") as selezione "
                + "order by ID_ESECUZIONE_RICALCOLO desc ";

        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, idProgrammaMusicale);
        q.setParameter(2, idProgrammaMusicale);

        List<Object[]> results = (List<Object[]>) q.getResultList();

        if (results != null && results.size() > 0) {

            ImportoRicalcolato importoRicalcolato = null;

            for (int i = 0; i < results.size(); i++) {

                importoRicalcolato = new ImportoRicalcolato();
                importoRicalcolato.setIdEsecuzioneRicalcolo(((Long) results.get(i)[0]));
                importoRicalcolato.setIdProgrammaMusicale(((Long) results.get(i)[1]));
                importoRicalcolato.setImporto(((BigDecimal) results.get(i)[2]).doubleValue());

                listaImportiRicalcolati.add(importoRicalcolato);
            }

        }

        return listaImportiRicalcolati;

    }

    public Long getMovimentiContabili(Long periodoContabileInizio, Long periodoContabileFine, String tipologiaPm) {

        EntityManager entityManager = provider.get();
        String query = "select count(m) from MovimentoContabile m, ProgrammaMusicale p "
                + "where m.idProgrammaMusicale=p.id and m.contabilita>=:inizioPeriodoContabile "
                + "and m.contabilita<=:finePeriodoContabile "
                + "and (:tipologiaPm is null or p.tipologiaPm=:tipologiaPm) "
                + "order by m.idEvento, m.numeroFattura, m.voceIncasso, m.idProgrammaMusicale asc";

        Long results = (Long) entityManager.createQuery(query)
                .setParameter("inizioPeriodoContabile", periodoContabileInizio)
                .setParameter("finePeriodoContabile", periodoContabileFine).setParameter("tipologiaPm", tipologiaPm)
                .getSingleResult();

        return results;

    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Integer updateContabilitaOriginale(Integer nuovaContabilita, List<Object> listaMovimenti) {

        EntityManager entityManager = provider.get();
        Integer result = 0;

        try {
            MovimentoContabile movimentoContabile = null;
            if (nuovaContabilita != null) {
                for (int i = 0; i < listaMovimenti.size(); i++) {
                    movimentoContabile = (MovimentoContabile) listaMovimenti.get(i);

                    // Aggiorna contabilita dei movimenti
                    movimentoContabile = entityManager.find(MovimentoContabile.class, movimentoContabile.getId());
                    // movimentoContabile.setContabilitaOrg(movimentoContabile.getContabilita());
                    movimentoContabile.setContabilita(new Integer(nuovaContabilita));
                    entityManager.merge(movimentoContabile);

                    // Aggiorna contabilita cartella
                    // Cartella cartella = entityManager.find(Cartella.class,
                    // movimentoContabile.getIdCartella());
                    // cartella.setContabilitaOrg(cartella.getContabilita());
                    // cartella.setContabilita(nuovaContabilita);
                    // entityManager.merge(cartella);

                    // Aggiorna contabilita eventi pagati
                    EventiPagati importoPagato = getEventoPagato(movimentoContabile.getIdEvento(),
                            movimentoContabile.getVoceIncasso(), movimentoContabile.getNumeroFattura().toString());
                    if (importoPagato != null) {
                        importoPagato.setContabilitaOrg(importoPagato.getContabilita());
                        importoPagato.setContabilita(nuovaContabilita);
                        entityManager.merge(importoPagato);
                    }

                    entityManager.flush();

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = -1;
        }

        return result;

    }

    public long getCountListaFatturePerEventiPagati(EventiPagati eventoPagato, String fatturaValidata) {

        EntityManager entityManager = provider.get();

        List<EventiPagati> listaEventi = new ArrayList<EventiPagati>();

        String query = "			SELECT count(*) " + "			from " + "				PERF_EVENTI_PAGATI ep "
                + " 			LEFT JOIN PERF_MANIFESTAZIONE ma on ep.ID_EVENTO=ma.ID_EVENTO "
                + " WHERE " + "				(?1 is null or ep.SEDE=?1) "
                + "				AND (?2 is null or ep.SEPRAG=?2) "
                + "				AND (?3 is null or ep.AGENZIA=?3) "
                + "				AND (?4 is null or ep.NUMERO_FATTURA=?4) "
                + "				AND (?5 is null or ep.NUMERO_REVERSALE=?5) "
                + "				AND (?6 is null or ep.VOCE_INCASSO=?6) "
                + "				AND (?10 is null or ep.TIPO_DOCUMENTO_CONTABILE=?10) "
                + "				AND (?11 is null or ep.ID_EVENTO=?11) "
                + "				AND (?12 is null or DATE(ep.DATA_REVERSALE) >= ?12) "
                + "				AND (?13 is null or DATE(ep.DATA_REVERSALE) <= ?13) "
                + "				AND (?16 is null or ep.IMPORTO_DEM=?14) "
                + "	AND (?7 is null or ma.CODICE_FISCALE_ORGANIZZATORE=?7) "
                + "	AND (?8 is null or ma.PARTITA_IVA_ORGANIZZATORE=?8) "
                + "	AND (?9 is null or ma.CODICE_SAP_ORGANIZZATORE=?9) "
                + "	AND (?14 is null or ep.DATA_INIZIO_EVENTO >=?14) "
                + "	AND (?15 is null or ep.DATA_INIZIO_EVENTO <?15) ";

        switch (fatturaValidata) {
            case "notValidated":
                query = query + "and QUADRATURA_NDM is null ";
                break;
            case "validated":
                query = query + "and QUADRATURA_NDM is not null ";
                break;
            case "ok":
                query = query + "and QUADRATURA_NDM ='1' ";
                break;
            case "ko":
                query = query + "and QUADRATURA_NDM = '0' ";
                break;
        }

        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, eventoPagato.getSede());
        q.setParameter(2, eventoPagato.getSeprag());
        q.setParameter(3, eventoPagato.getAgenzia());
        q.setParameter(4, eventoPagato.getNumeroFattura());
        q.setParameter(5, eventoPagato.getReversale());
        q.setParameter(6, eventoPagato.getVoceIncasso());
        q.setParameter(7, eventoPagato.getManifestazione().getCodiceFiscaleOrganizzatore());
        q.setParameter(8, eventoPagato.getManifestazione().getPartitaIvaOrganizzatore());
        q.setParameter(9, eventoPagato.getManifestazione().getCodiceSapOrganizzatore());
        q.setParameter(10, eventoPagato.getTipoDocumentoContabile());
        q.setParameter(11, eventoPagato.getIdEvento());
        q.setParameter(12, eventoPagato.getDataFatturaDa());
        q.setParameter(13, eventoPagato.getDataFatturaA());
        q.setParameter(14, eventoPagato.getDataEventoDa());
        q.setParameter(15, eventoPagato.getDataEventoA());
        q.setParameter(16, eventoPagato.getImportDem());

        Long result = (Long) q.getSingleResult();
        return result;

    }

    public List<EventiPagati> getListaFatturePerEventiPagati(EventiPagati eventoPagato, String fatturaValidata,
                                                             String order, Integer page) {

        EntityManager entityManager = provider.get();

        List<EventiPagati> listaEventi = new ArrayList<EventiPagati>();

        String query = "			SELECT ep.SEDE, ep.SEPRAG, ep.AGENZIA, ep.NUMERO_FATTURA, "
                + "			ep.NUMERO_REVERSALE, ep.VOCE_INCASSO, ma.CODICE_FISCALE_ORGANIZZATORE, "
                + "			ma.PARTITA_IVA_ORGANIZZATORE, ma.CODICE_SAP_ORGANIZZATORE, "
                + "			ep.TIPO_DOCUMENTO_CONTABILE, "
                + "			ep.ID_EVENTO, ep.DATA_REVERSALE, ep.DATA_INIZIO_EVENTO, ep.IMPORTO_DEM "
                + "			from " + "				PERF_EVENTI_PAGATI ep "
                + " 			LEFT JOIN PERF_MANIFESTAZIONE ma on ep.ID_EVENTO=ma.ID_EVENTO "
                + " WHERE " + "				(?1 is null or ep.SEDE=?1) "
                + "				AND (?2 is null or ep.SEPRAG=?2) "
                + "				AND (?3 is null or ep.AGENZIA=?3) "
                + "				AND (?4 is null or ep.NUMERO_FATTURA=?4) "
                + "				AND (?5 is null or ep.NUMERO_REVERSALE=?5) "
                + "				AND (?6 is null or ep.VOCE_INCASSO=?6) "
                + "				AND (?10 is null or ep.TIPO_DOCUMENTO_CONTABILE=?10) "
                + "				AND (?11 is null or ep.ID_EVENTO=?11) "
                + "				AND (?12 is null or DATE(ep.DATA_REVERSALE) >= ?12) "
                + "				AND (?13 is null or DATE(ep.DATA_REVERSALE) <= ?13) "                
                + "				AND (?16 is null or ep.IMPORTO_DEM=?16) "
                + "	AND (?7 is null or ma.CODICE_FISCALE_ORGANIZZATORE=?7) "
                + "	AND (?8 is null or ma.PARTITA_IVA_ORGANIZZATORE=?8) "
                + "	AND (?9 is null or ma.CODICE_SAP_ORGANIZZATORE=?9) "
                + "	AND (?14 is null or ep.DATA_INIZIO_EVENTO >=?14) "
                + "	AND (?15 is null or ep.DATA_INIZIO_EVENTO <?15) ";

        switch (fatturaValidata) {
            case "notValidated":
                query = query + "and QUADRATURA_NDM is null ";
                break;
            case "validated":
                query = query + "and QUADRATURA_NDM is not null ";
                break;
            case "ok":
                query = query + "and QUADRATURA_NDM ='1' ";
                break;
            case "ko":
                query = query + "and QUADRATURA_NDM = '0' ";
                break;
        }

        if (order == null) {
            query = query + "order by 1 asc";
        } else if (order.startsWith("CODICE_FISCALE") || order.startsWith("PARTITA_IVA")
                || order.startsWith("CODICE_SAP")) {
            query = query + "order by ma." + order;

        } else {
            query = query + "order by ep." + order;
        }
        Query q = entityManager.createNativeQuery(query);
        int recordCount = 0;

        int startRecord = 0;
        int endRecord = recordCount;

        if (page != -1) {

            startRecord = (page) * ReportPage.NUMBER_RECORD_PER_PAGE;
            endRecord = ReportPage.NUMBER_RECORD_PER_PAGE;
        }

        q.setParameter(1, eventoPagato.getSede());
        q.setParameter(2, eventoPagato.getSeprag());
        q.setParameter(3, eventoPagato.getAgenzia());
        q.setParameter(4, eventoPagato.getNumeroFattura());
        q.setParameter(5, eventoPagato.getReversale());
        q.setParameter(6, eventoPagato.getVoceIncasso());
        q.setParameter(7, eventoPagato.getManifestazione().getCodiceFiscaleOrganizzatore());
        q.setParameter(8, eventoPagato.getManifestazione().getPartitaIvaOrganizzatore());
        q.setParameter(9, eventoPagato.getManifestazione().getCodiceSapOrganizzatore());
        q.setParameter(10, eventoPagato.getTipoDocumentoContabile());
        q.setParameter(11, eventoPagato.getIdEvento());
        q.setParameter(12, eventoPagato.getDataFatturaDa());
        q.setParameter(13, eventoPagato.getDataFatturaA());
        q.setParameter(14, eventoPagato.getDataEventoDa());
        q.setParameter(15, eventoPagato.getDataEventoA());
        q.setParameter(16, eventoPagato.getImportDem());        

        List<Object[]> results = (List<Object[]>) q.setFirstResult(startRecord).setMaxResults(endRecord)
                .getResultList();

        if (results != null && results.size() > 0) {

            EventiPagati evPagato = null;
            Manifestazione manifestazione = null;

            for (int i = 0; i < results.size(); i++) {

                evPagato = new EventiPagati();
                manifestazione = new Manifestazione();

                evPagato.setSede((String) results.get(i)[0]);
                evPagato.setSeprag((String) results.get(i)[1]);
                evPagato.setAgenzia((String) results.get(i)[2]);
                evPagato.setNumeroFattura((String) results.get(i)[3]);
                evPagato.setReversale((String) results.get(i)[4]);
                evPagato.setVoceIncasso((String) results.get(i)[5]);
                manifestazione.setCodiceFiscaleOrganizzatore((String) results.get(i)[6]);
                manifestazione.setPartitaIvaOrganizzatore((String) results.get(i)[7]);
                manifestazione.setCodiceSapOrganizzatore((String) results.get(i)[8]);
                evPagato.setTipoDocumentoContabile((String) results.get(i)[9]);
                if (results.get(i)[10] != null)
                    evPagato.setIdEvento((Long) results.get(i)[10]);

                evPagato.setDataReversale((Date) results.get(i)[11]);
                if (results.get(i)[12] != null)
                    evPagato.setDataInizioEvento((Date) results.get(i)[12]);

                if (!results.get(i)[13].equals("")) {
                    // if (!union) {
                    evPagato.setImportDem(((BigDecimal) results.get(i)[13]).doubleValue());
                    // }
                    // else {
                    // evPagato.setImportDem(Double.valueOf((String)results.get(i)[13]));
                    // }
                }

                evPagato.setManifestazione(manifestazione);

                listaEventi.add(evPagato);
            }

        }
        return listaEventi;
    }

    public List<MovimentoContabile> getListaFatturePerMovimentiContabili(MovimentoContabile movimentiContabili) {
        EntityManager entityManager = provider.get();

        List<MovimentoContabile> listaMovimenti = new ArrayList<MovimentoContabile>();

        String query = "select e.numeroFattura, e.reversale, e.voceIncasso, "
                + "ma.partitaIvaOrganizzatore, ma.codiceFiscaleOrganizzatore, e.tipoDocumentoContabile, "
                + "e.idEvento, e.importoTotDem, e.id, ma.codiceSapOrganizzatore, e.importoTotDemLordo "
                + "from MovimentoContabile e " + "INNER JOIN Manifestazione ma ON e.idEvento = ma.id "
                + "where (:numeroFattura is null or e.numeroFattura=:numeroFattura) "
                + "and (:reversale is null or e.reversale=:reversale) "
                + "and (:voceIncasso is null or e.voceIncasso=:voceIncasso) "
                + "and (:pIvaOrganizzatore is null or ma.partitaIvaOrganizzatore=:pIvaOrganizzatore) "
                + "and (:cfOrganizzatore is null or ma.codiceFiscaleOrganizzatore=:cfOrganizzatore) "
                + "and (:codiceSAP is null or ma.codiceSapOrganizzatore=:codiceSAP) "
                + "and (:tipoDocumento is null or e.tipoDocumentoContabile=:tipoDocumento) "
                + "and (:idEvento is null or e.idEvento=:idEvento) "
                + "and (:importo is null or e.importoTotDem=:importo) ";

        Query q = entityManager.createQuery(query);

        q.setParameter("numeroFattura", movimentiContabili.getNumeroFattura());
        q.setParameter("reversale", movimentiContabili.getReversale());
        q.setParameter("voceIncasso", movimentiContabili.getVoceIncasso());
        q.setParameter("pIvaOrganizzatore", movimentiContabili.getEvento().getCodiceFiscaleOrganizzatore());
        q.setParameter("cfOrganizzatore", movimentiContabili.getEvento().getPartitaIvaOrganizzatore());
        q.setParameter("codiceSAP", movimentiContabili.getEvento().getCodiceSapOrganizzatore());
        q.setParameter("tipoDocumento", movimentiContabili.getTipoDocumentoContabile());
        q.setParameter("idEvento", movimentiContabili.getIdEvento());
        q.setParameter("importo", movimentiContabili.getImportoTotDem());

        List<Object[]> results = (List<Object[]>) q.getResultList();

        if (results != null && results.size() > 0) {

            MovimentoContabile movimento = null;
            Manifestazione manifestazione = null;

            for (int i = 0; i < results.size(); i++) {

                movimento = new MovimentoContabile();
                manifestazione = new Manifestazione();

                movimento.setNumeroFattura((Long) results.get(i)[0]);
                movimento.setReversale((Long) results.get(i)[1]);
                movimento.setVoceIncasso((String) results.get(i)[2]);
                manifestazione.setPartitaIvaOrganizzatore((String) results.get(i)[3]);
                manifestazione.setCodiceFiscaleOrganizzatore((String) results.get(i)[4]);
                movimento.setTipoDocumentoContabile((String) results.get(i)[5]);
                movimento.setIdEvento((Long) results.get(i)[6]);
                movimento.setImportoTotDem((Double) results.get(i)[7]);
                movimento.setId((Long) results.get(i)[8]);
                manifestazione.setCodiceSapOrganizzatore((String) results.get(i)[9]);
                movimento.setImportoTotDemLordo((Double) results.get(i)[10]);
                movimento.setEvento(manifestazione);

                listaMovimenti.add(movimento);
            }

        }
        return listaMovimenti;
    }

    public List<EventiPagati> getDettaglioFattureOnEvento(String numeroFattura) {
        // TODO cambiare anche interfaccia
        EntityManager entityManager = provider.get();

        List<EventiPagati> eventiPagati = new ArrayList<EventiPagati>();
        String query = "select e.seprag, e.numeroFattura, e.reversale, e.voceIncasso, "
                + "ma.partitaIvaOrganizzatore, ma.codiceFiscaleOrganizzatore, e.tipoDocumentoContabile, "
                + "e.idEvento, e.dataInizioEvento, e.importDem, e.id, ma.codiceSapOrganizzatore, e.numeroPmTotaliAttesi, e.numeroPmTotaliAttesiSpalla "
                + "FROM EventiPagati e " + "LEFT JOIN Manifestazione ma ON e.idEvento = ma.id "
                + "WHERE e.numeroFattura = :numeroFattura";
        Query q = entityManager.createQuery(query);
        q.setParameter("numeroFattura", numeroFattura);

        List<Object[]> results = (List<Object[]>) q.getResultList();

        for (int i = 0; i < results.size(); i++) {
            EventiPagati evPagato = new EventiPagati();
            Manifestazione manifestazione = new Manifestazione();

            evPagato.setSeprag((String) results.get(i)[0]);
            evPagato.setNumeroFattura((String) results.get(i)[1]);
            evPagato.setReversale((String) results.get(i)[2]);
            evPagato.setVoceIncasso((String) results.get(i)[3]);
            manifestazione.setPartitaIvaOrganizzatore((String) results.get(i)[4]);
            manifestazione.setCodiceFiscaleOrganizzatore((String) results.get(i)[5]);
            evPagato.setTipoDocumentoContabile((String) results.get(i)[6]);
            evPagato.setIdEvento((Long) results.get(i)[7]);
            evPagato.setDataInizioEvento((Date) results.get(i)[8]);
            evPagato.setImportDem((Double) results.get(i)[9]);
            evPagato.setId((Long) results.get(i)[10]);
            manifestazione.setCodiceSapOrganizzatore((String) results.get(i)[11]);
            evPagato.setManifestazione(manifestazione);

            eventiPagati.add(evPagato);
        }
        return eventiPagati;
    }

    public List<MovimentoContabile> getDettaglioFattureOnMovimento(String numeroFattura) {

        EntityManager entityManager = provider.get();

        List<MovimentoContabile> listaMovimenti = new ArrayList<MovimentoContabile>();

        String query = "select e.numeroFattura, e.reversale, e.voceIncasso, "
                + "ma.partitaIvaOrganizzatore, ma.codiceFiscaleOrganizzatore, e.tipoDocumentoContabile, "
                + "e.idEvento, e.importoTotDem, e.id, ma.codiceSapOrganizzatore " + "FROM MovimentoContabile e "
                + "LEFT JOIN Manifestazione ma ON e.idEvento = ma.id " + "WHERE e.numeroFattura = :numeroFattura";

        Query q = entityManager.createQuery(query);
        q.setParameter("numeroFattura", Long.parseLong(numeroFattura));

        List<Object[]> results = (List<Object[]>) q.getResultList();

        for (int i = 0; i < results.size(); i++) {

            MovimentoContabile movimento = new MovimentoContabile();
            Manifestazione manifestazione = new Manifestazione();

            movimento.setNumeroFattura((Long) results.get(i)[0]);
            movimento.setReversale((Long) results.get(i)[1]);
            movimento.setVoceIncasso((String) results.get(i)[2]);
            manifestazione.setPartitaIvaOrganizzatore((String) results.get(i)[3]);
            manifestazione.setCodiceFiscaleOrganizzatore((String) results.get(i)[4]);
            movimento.setTipoDocumentoContabile((String) results.get(i)[5]);
            movimento.setIdEvento((Long) results.get(i)[6]);
            movimento.setImportoTotDem((Double) results.get(i)[7]);
            movimento.setId((Long) results.get(0)[8]);
            manifestazione.setCodiceSapOrganizzatore((String) results.get(i)[9]);
            movimento.setEvento(manifestazione);

            listaMovimenti.add(movimento);
        }
        return listaMovimenti;
    }

    public Double getImportoTotaleOfEvento(Double idEvento) {

        EntityManager entityManager = provider.get();

        List<MovimentoContabile> listaMovimenti = new ArrayList<MovimentoContabile>();

        String query = "select sum(mc.importoTotDem + ep.importDem) " + "from MovimentoContabile mc "
                + "JOIN EventiPagati ep on mc.idEvento=ep.idEvento " + "where mc.idEvento=:idEvento ";
        Query q = entityManager.createQuery(query);
        q.setParameter("idEvento", idEvento);

        Double result = (Double) q.getSingleResult();
        return result;
    }

    public Integer addUtilizzazione(Utilizzazione utilizzazione) {
        EntityManager entityManager = provider.get();

        try {
            entityManager.getTransaction().begin();
            entityManager.persist(utilizzazione);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
                return 0;
            }
            return 0;
        }
        return 1;

    }

    ;

    public List<MovimentoContabile> getListaPmDisponibili(String numeroPm) {
        EntityManager entityManager = provider.get();

        String query = "select mc " + "from MovimentoContabile mc " + "WHERE mc.idEvento is null "
                + "AND (mc.idProgrammaMusicale is not null) "
                + "AND (:numeroPm is null or mc.numProgrammaMusicale=:numeroPm) ";

        Query q = entityManager.createQuery(query);

        q.setParameter("numeroPm", numeroPm);

        List<MovimentoContabile> results = (List<MovimentoContabile>) q.getResultList();

        return results;

    }

    public void sgancioPm(List<MovimentoContabile> movimenti) {
        EntityManager entityManager = provider.get();

        for (MovimentoContabile movimento : movimenti) {
            try {
                PerfMovimentoContabile movimentoSelected = entityManager.find(PerfMovimentoContabile.class, new BigDecimal(movimento.getId()));
                entityManager.getTransaction().begin();
                movimentoSelected.setIdEvento(null);
                movimentoSelected.setImportoValorizzato(null);
                entityManager.persist(movimentoSelected);
                entityManager.getTransaction().commit();
                entityManager.clear();
            } catch (Exception ex) {
                if (entityManager.getTransaction().isActive()) {
                    entityManager.getTransaction().rollback();
                }
            }
        }
    }

    public void sgancioPm(Long idMovimentoContabile) {
        EntityManager entityManager = provider.get();

        try {
            PerfMovimentoContabile movimentoSelected = entityManager.find(PerfMovimentoContabile.class,
                    new BigDecimal(idMovimentoContabile));
            entityManager.getTransaction().begin();
            movimentoSelected.setIdEvento(null);
            movimentoSelected.setImportoValorizzato(null);
            movimentoSelected.setPuntiProgrammaMusicale(null);
            entityManager.persist(movimentoSelected);
            entityManager.getTransaction().commit();
            entityManager.clear();
        } catch (Exception ex) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
        }

    }

    public void aggancioPm(String numeroPm, String voceIncasso, Long idEvento) {
        EntityManager entityManager = provider.get();

        List<MovimentoContabile> movimenti = getListaPmDisponibili(numeroPm);
        for (MovimentoContabile movimento : movimenti) {
            try {
                entityManager.getTransaction().begin();
                movimento.setIdEvento(idEvento);
                entityManager.persist(movimento);
                entityManager.getTransaction().commit();
            } catch (Exception ex) {
                if (entityManager.getTransaction().isActive()) {
                    entityManager.getTransaction().rollback();
                }
            }
        }

    }

    public List<MovimentoContabile> getMovimentiByPmAndVoceIncasso(Long idPm, String voceIncasso, Long idEvento) {

        EntityManager entityManager = provider.get();

        int recordCount = 0;

        List<MovimentoContabile> listaMovimenti;
        String query = "select mc from MovimentoContabile mc "
                + "where mc.voceIncasso=:voceIncasso "
                + "and mc.idProgrammaMusicale=:idPm ";
//	 		+ "and mc.idEvento=:idEvento ";

        Query q = entityManager.createQuery(query);

        q.setParameter("voceIncasso", voceIncasso);
        q.setParameter("idPm", idPm);
//	 q.setParameter("idEvento", idEvento);


        listaMovimenti = q.getResultList();

        if (listaMovimenti != null && listaMovimenti.size() > 0) {
            entityManager.clear();
            return listaMovimenti;
        } else {
            return null;
        }
    }

    public Integer getVociIncassoForIdPm(Long idPm) {
        EntityManager entityManager = provider.get();
        List<Object> listaVoci;

        String query = "select distinct mc.voceIncasso from MovimentoContabile mc "
                + "where mc.idProgrammaMusicale=:idPm ";

        Query q = entityManager.createQuery(query);

        q.setParameter("idPm", idPm);
        listaVoci = q.getResultList();
        if (listaVoci.size() > 1)
            return 0;
        else
            return 1;
    }

    public void removeOpera(Long idUtilizzazione) {
        EntityManager entityManager = provider.get();
        Utilizzazione utilizzazione = entityManager.find(Utilizzazione.class, idUtilizzazione);
        entityManager.getTransaction().begin();
        entityManager.remove(utilizzazione);
        entityManager.getTransaction().commit();
    }

    public void addPm(ProgrammaMusicale programmaMusicale) {
        EntityManager entityManager = provider.get();

        try {
            entityManager.getTransaction().begin();
            entityManager.persist(programmaMusicale);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
        }
    }

    public void updatePm(ProgrammaMusicaleDTO programmaMusicaleDto) {
        EntityManager entityManager = provider.get();
        ProgrammaMusicale programmaMusicale = entityManager.find(ProgrammaMusicale.class, programmaMusicaleDto.getId());

        try {
            entityManager.getTransaction().begin();
            programmaMusicale.setNumeroProgrammaMusicale(programmaMusicaleDto.getNumeroProgrammaMusicale());
            programmaMusicale.setTipoPm(programmaMusicaleDto.getTipoPm());
            programmaMusicale.setSupportoPm(programmaMusicaleDto.getSupportoPm());
            programmaMusicale.setAgenziaDiCarico(programmaMusicaleDto.getAgenziaDiCarico());
            programmaMusicale.setDataAssegnazione(programmaMusicaleDto.getDataAssegnazione());
            programmaMusicale.setDataRestituzione(programmaMusicaleDto.getDataRestituzione());
            programmaMusicale.setDataCompilazione(programmaMusicaleDto.getDataCompilazione());
            programmaMusicale.setStatoPm(programmaMusicaleDto.getStato());
            programmaMusicale.setDataAnnullamento(programmaMusicaleDto.getDataAnnullamento());
            programmaMusicale.setDataAcquisizioneSophia(programmaMusicaleDto.getDataAcquisizioneSophia());
            programmaMusicale.setDataRipartizione(programmaMusicaleDto.getDataRipartizione());
            programmaMusicale.setDataInvioDg(programmaMusicaleDto.getDataInvioDg());
            programmaMusicale.setDataAcquisizoneImmagine(programmaMusicaleDto.getDataAcquisizioneImmagine());
            programmaMusicale.setDataRicezioneDig(programmaMusicaleDto.getDataRicezioneInfoDig());
            programmaMusicale.setCausaAnnullamento(programmaMusicaleDto.getCausaAnnullamento());
            programmaMusicale.setProgressivo(programmaMusicaleDto.getProgressivo());
            programmaMusicale.setTotaleCedole(programmaMusicaleDto.getTotaleCedole());
            programmaMusicale.setTotaleDurataCedole(programmaMusicaleDto.getTotaleDurataCedole());
            programmaMusicale.setFoglio(programmaMusicaleDto.getFoglio());
            programmaMusicale.setPmRiferimento(programmaMusicaleDto.getPmRiferimento());
            programmaMusicale.setRisultatoCalcoloResto(programmaMusicaleDto.getRisultatoCalcoloResto());
            programmaMusicale.setDataOraUltimaModifica(programmaMusicaleDto.getDataOraUltimaModifica());
            programmaMusicale.setUtenteUltimaModifica(programmaMusicaleDto.getUtenteUltimaModifica());
            programmaMusicale.setGiornateTrattenimento(programmaMusicaleDto.getGiornateTrattenimento());
            programmaMusicale.setFogli(programmaMusicaleDto.getFogli());
            programmaMusicale.setFlagPmCorrente(programmaMusicaleDto.getFlagPmCorrente());
            programmaMusicale.setDataAssegnazione(programmaMusicaleDto.getDataAssegnazione());
            programmaMusicale.setDataAssegnazioneIncarico(programmaMusicaleDto.getDataAssegnazioneIncarico());
            programmaMusicale.setDataRegistazioneRilevazione(programmaMusicaleDto.getDataRegistrazioneRilevazione());
            programmaMusicale.setDurataRilevazione(programmaMusicaleDto.getDurataRilevazione());
            programmaMusicale.setProtocolloRegistrazione(programmaMusicaleDto.getProtocolloRegistrazione());
            programmaMusicale.setProtocolloTrasmissione(programmaMusicaleDto.getProtocolloTrasmissione());
            programmaMusicale.setCodiceCampionamento(programmaMusicaleDto.getCodiceCampionamento());
            programmaMusicale.setFlagDaCampionare(programmaMusicaleDto.getFlagDaCampionare());
            programmaMusicale.setFlagGruppoPrincipale(programmaMusicaleDto.getFlagGruppoPrincipale());
            programmaMusicale.setFoglioSegueDi(programmaMusicaleDto.getFoglioSegueDi());
            entityManager.persist(programmaMusicale);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
        }
    }

    public void updateOpera(PerfUtilizzazioniDTO perfUtilizzazioniAddDTO) {
        EntityManager entityManager = provider.get();
        Utilizzazione utilizzazione = entityManager.find(Utilizzazione.class, perfUtilizzazioniAddDTO.getId());

        try {
            entityManager.getTransaction().begin();
            // utilizzazione.setIdProgrammaMusicale(perfUtilizzazioniAddDTO.getIdProgrammaMusicale());
            if (perfUtilizzazioniAddDTO.getCodiceOpera() != null)
                utilizzazione.setCodiceOpera(perfUtilizzazioniAddDTO.getCodiceOpera());
            if (perfUtilizzazioniAddDTO.getTitolo() != null)
                utilizzazione.setTitoloComposizione(perfUtilizzazioniAddDTO.getTitolo());
            if (perfUtilizzazioniAddDTO.getCompositore() != null) {
                utilizzazione.setCompositore(perfUtilizzazioniAddDTO.getCompositore());
            } else if (perfUtilizzazioniAddDTO.getCompositori() != null) {
                utilizzazione.setCompositore(perfUtilizzazioniAddDTO.getCompositori());
            }

            // utilizzazione.setNumeroProgrammaMusicale(perfUtilizzazioniAddDTO.getNumeroProgrammaMusicale());
            if (perfUtilizzazioniAddDTO.getDurataEsecuzione() != null)
                utilizzazione.setDurata(perfUtilizzazioniAddDTO.getDurataEsecuzione());

            if (perfUtilizzazioniAddDTO.getMenodi30sec() != null)
                utilizzazione.setDurataInferiore30sec(perfUtilizzazioniAddDTO.getMenodi30sec());

            if (perfUtilizzazioniAddDTO.getAutori() != null)
                utilizzazione.setAutori(perfUtilizzazioniAddDTO.getAutori());

            if (perfUtilizzazioniAddDTO.getInterpreti() != null)
                utilizzazione.setInterpreti(perfUtilizzazioniAddDTO.getInterpreti());

            if (perfUtilizzazioniAddDTO.getFlagCodifica() != null)
                utilizzazione.setFlagCodifica(perfUtilizzazioniAddDTO.getFlagCodifica());

            if (perfUtilizzazioniAddDTO.getCombinazioneCodifica() != null)
                utilizzazione.setCombinazioneCodifica(perfUtilizzazioniAddDTO.getCombinazioneCodifica());

            if (perfUtilizzazioniAddDTO.getElaboratorePubblicoDominio() != null)
                utilizzazione.setFlagPubblicoDominio(perfUtilizzazioniAddDTO.getElaboratorePubblicoDominio());

            if (perfUtilizzazioniAddDTO.getFlagMagNonConcessa() != null)
                utilizzazione.setFlagMagNonConcessa(perfUtilizzazioniAddDTO.getFlagMagNonConcessa());

            if (perfUtilizzazioniAddDTO.getTipoAccertamento() != null)
                utilizzazione.setTipoAccertamento(perfUtilizzazioniAddDTO.getTipoAccertamento());

            if (perfUtilizzazioniAddDTO.getNettomaturato() != null)
                utilizzazione.setNettoMaturato(perfUtilizzazioniAddDTO.getNettomaturato());

            if (perfUtilizzazioniAddDTO.getDataPrimaCodifica() != null)
                utilizzazione.setDataPrimaCodifica(DateUtils
                        .stringToDate(perfUtilizzazioniAddDTO.getDataPrimaCodifica(), DateUtils.DATE_ITA_FORMAT_DASH));

            if (perfUtilizzazioniAddDTO.getFlagCedolaEsclusa() != null)
                utilizzazione.setFlagCedolaEsclusa(perfUtilizzazioniAddDTO.getFlagCedolaEsclusa());

            entityManager.persist(utilizzazione);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
        }

    }

    public TrattamentoPM getTrattamentoPM(String voceIncasso) {
        EntityManager entityManager = provider.get();

        int recordCount = 0;

        List<TrattamentoPM> listaTrattamenti;
        String query = "select tr from TrattamentoPM tr " + "where tr.voceIncasso=:voceIncasso";

        Query q = entityManager.createQuery(query);

        q.setParameter("voceIncasso", voceIncasso);

        listaTrattamenti = q.getResultList();

        if (listaTrattamenti != null && listaTrattamenti.size() > 0) {
            return (TrattamentoPM) listaTrattamenti.get(0);
        } else {
            return null;
        }
    }

    public void updatePMAttesiInEventiPagati(Long pmPrevisti, Long pmPrevistiSpalla, Long idEvento) {
        EntityManager entityManager = provider.get();
        EventiPagati eventiPagati = entityManager.find(EventiPagati.class, idEvento);

        try {
            entityManager.getTransaction().begin();
            eventiPagati.setNumeroPmTotaliAttesi(pmPrevisti);
            eventiPagati.setNumeroPmTotaliAttesiSpalla(pmPrevistiSpalla);
            entityManager.persist(eventiPagati);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
        }
    }

    public void updatePMAttesiInMovimenti(Integer pmPrevisti, Integer pmPrevistiSpalla, Long idMovimento) {
        EntityManager entityManager = provider.get();
        MovimentoContabile movimento = entityManager.find(MovimentoContabile.class, idMovimento);

        try {
            entityManager.getTransaction().begin();
            movimento.setNumeroPmPrevisti(pmPrevisti);
            movimento.setNumeroPmPrevistiSpalla(pmPrevistiSpalla);
            ;
            entityManager.persist(movimento);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
        }
    }

    public Integer controlloPmRientrati(Long idEvento) {

        return null;
    }

    public void addOpera(Utilizzazione utilizzazione) {
        EntityManager entityManager = provider.get();

        try {
            entityManager.getTransaction().begin();
            entityManager.persist(utilizzazione);
            entityManager.getTransaction().commit();
            entityManager.clear();
        } catch (Exception ex) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
        }
    }

    public EventiPagati getEventoPagato(Long idEventoPagato) {

        EntityManager entityManager = provider.get();
        return entityManager.find(EventiPagati.class, idEventoPagato);

    }

    public void updateFlagCampionamentoOnMovimentiAssociati(Long idMovimento, String flagGruppo) {

        EntityManager entityManager = provider.get();
        MovimentoContabile movimento = entityManager.find(MovimentoContabile.class, idMovimento);
        try {
            entityManager.getTransaction().begin();
            movimento.setFlagGruppoPrincipale(flagGruppo.charAt(0));
            entityManager.persist(movimento);
            entityManager.getTransaction().commit();
            entityManager.clear();
        } catch (Exception ex) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
        }
    }

    public List<Long> retrieveIdMovimentiForProgrammiMusicali(Long idPm) {
        EntityManager entityManager = provider.get();

        List<Long> listaIdMovimenti = null;
        String query = "select mc.ID_MOVIMENTO_CONTABILE from PERF_MOVIMENTO_CONTABILE mc "
                + "where mc.ID_PROGRAMMA_MUSICALE=?1";

        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, idPm);

        List<Long> results = (List<Long>) q.getResultList();

        return results;
    }

    public List<EventiPagati> getEventiByFattura(String numeroFattura) {
        EntityManager entityManager = provider.get();

        String query = "select ep from EventiPagati ep where (:numeroFattura is null or ep.numeroFattura = :numeroFattura) "
                + "order by ep.voceIncasso desc";
        Query q = entityManager.createQuery(query);
        q.setParameter("numeroFattura", numeroFattura);

        List<EventiPagati> results = (List<EventiPagati>) q.getResultList();

        return results;
    }

    public List<String> getVociIncassoPerIdEvento(Long idEvento) {
        EntityManager entityManager = provider.get();

        String query = "select ep.voceIncasso from EventiPagati ep where ep.idEvento=:idEvento ";
        Query q = entityManager.createQuery(query);
        q.setParameter("idEvento", idEvento);

        List<String> results = (List<String>) q.getResultList();

        return results;
    }

    public void removeMovimentiAssociationWithPm(Long idMovimento) {

        EntityManager entityManager = provider.get();
        MovimentoContabile movimento = entityManager.find(MovimentoContabile.class, idMovimento);

        try {
            entityManager.getTransaction().begin();
            movimento.setIdProgrammaMusicale(null);
            movimento.setNumeroFattura(null);
            entityManager.persist(movimento);
            entityManager.getTransaction().commit();
            entityManager.clear();
        } catch (Exception ex) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
        }
    }

    public void updateMostRecentMovimentoOnRiaggancio(Long movimentoMaggiore, Long idPm, EventiPagati eventoSuVoce) {

        EntityManager entityManager = provider.get();

        MovimentoContabile movimento = entityManager.find(MovimentoContabile.class, movimentoMaggiore);
        try {
            entityManager.getTransaction().begin();

            movimento.setIdEvento(eventoSuVoce.getIdEvento());
            movimento.setIdProgrammaMusicale(idPm);
            if (eventoSuVoce.getNumeroFattura() != null)
                movimento.setNumeroFattura(Long.valueOf(eventoSuVoce.getNumeroFattura()));
            else
                movimento.setNumeroFattura(null);

            movimento.setDataReversale(eventoSuVoce.getDataReversale());

            if (eventoSuVoce.getReversale() != null)
                movimento.setReversale(Long.valueOf(eventoSuVoce.getReversale()));
            else
                movimento.setReversale(null);

            movimento.setImportoTotDem(eventoSuVoce.getImportDem());
            movimento.setVoceIncasso(eventoSuVoce.getVoceIncasso());
            // TODO scommentare appena la colonna esiste
            // movimento.setImportoManuale(eventoSuVoce.getImportoManuale());
            entityManager.persist(movimento);
            entityManager.getTransaction().commit();
            entityManager.clear();
        } catch (Exception ex) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
        }

    }

    public EventiPagati getEventoByVoceIncasso(String voceIncasso, Long idEvento) {
        EntityManager entityManager = provider.get();

        String query = "select ep from EventiPagati ep where ep.voceIncasso=:voceIncasso and ep.idEvento=:idEvento ";
        Query q = entityManager.createQuery(query);
        q.setParameter("voceIncasso", voceIncasso);
        q.setParameter("idEvento", idEvento);

        List<EventiPagati> result = (List<EventiPagati>) q.getResultList();

        return result.get(0);

    }

    public void addConfigurazioneAggioMc(AggiMc aggio) {

        EntityManager entityManager = provider.get();

        try {
            entityManager.getTransaction().begin();
            entityManager.persist(aggio);
            entityManager.flush();
            entityManager.getTransaction().commit();
            entityManager.clear();
        } catch (Exception ex) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
        }
    }
    // public void updateConfigurazioneAggioMc(AggiMc aggio) {
    //
    // EntityManager entityManager=provider.get();
    // AggiMc aggioInTable = entityManager.find(AggiMc.class, aggio.getId());
    //
    // try {
    // entityManager.getTransaction().begin();
    //// movimento.setIdProgrammaMusicale(null);
    //// movimento.setNumeroFattura(null);
    // entityManager.persist(aggioInTable);
    // entityManager.getTransaction().commit();
    // entityManager.clear();
    // } catch (Exception ex) {
    // if (entityManager.getTransaction().isActive()) {
    // entityManager.getTransaction().rollback();
    // }
    // }
    // }

    public List<AggiMc> getListaAggiAttivi(String inizioPeriodoValidazione, String finePeriodoValidazione,
                                           String codiceRegola, String order) {
        EntityManager entityManager = provider.get();

        String query = "select am from AggiMc am " + "where am.flagAttivo=1 "
                + "and (:codiceRegola is null or am.codiceRegola=:codiceRegola) " + "and (" + "		("
                + "			(:inizioPeriodoValidazione is null or am.finePeriodoValidazione>=:inizioPeriodoValidazione) "
                + "			or " + "			am.finePeriodoValidazione is null) " + "		and"
                + "		(:finePeriodoValidazione is null or am.inizioPeriodoValidazione<=:finePeriodoValidazione) "
                + ")";
        if (order != null) {
            query = query + "order by am." + order;
        }
        Query q = entityManager.createQuery(query);

        if (inizioPeriodoValidazione != null)
            q.setParameter("inizioPeriodoValidazione",
                    DateUtils.stringToDate(inizioPeriodoValidazione, DateUtils.DATE_ITA_FORMAT_DASH));
        else
            q.setParameter("inizioPeriodoValidazione", null);

        if (finePeriodoValidazione != null)
            q.setParameter("finePeriodoValidazione",
                    DateUtils.stringToDate(finePeriodoValidazione, DateUtils.DATE_ITA_FORMAT_DASH));
        else
            q.setParameter("finePeriodoValidazione", null);

        q.setParameter("codiceRegola", codiceRegola);
        List<AggiMc> result = (List<AggiMc>) q.getResultList();

        return result;
    }

    public AggiMc getAggioAttivo(Long id) {
        EntityManager entityManager = provider.get();

        String query = "select ID_AGGIO_MC, INIZIO_PERIODO_VALIDAZIONE, FINE_PERIODO_VALIDAZIONE, SOGLIA, AGGIO_SOTTOSOGLIA, "
                + "AGGIO_SOPRASOGLIA, VALORE_CAP, FLAG_CAP_MULTIPLO, DATA_CREAZIONE, UTENTE_CREAZIONE, CODICE_REGOLA, "
                + "TIPO_ULTIMA_MODIFICA, UTENTE_ULTIMA_MODIFICA, DATA_ULTIMA_MODIFICA from PERF_AGGI_MC am where ID_AGGIO_MC=?1";
        Query q = entityManager.createNativeQuery(query);
        q.setParameter(1, id);

        List<Object[]> result = (List<Object[]>) q.getResultList();
        AggiMc aggioAttivo = new AggiMc();
        aggioAttivo.setId((Long) result.get(0)[0]);
        aggioAttivo.setInizioPeriodoValidazione((Date) result.get(0)[1]);
        aggioAttivo.setFinePeriodoValidazione((Date) result.get(0)[2]);
        aggioAttivo.setSoglia((BigDecimal) result.get(0)[3]);
        aggioAttivo.setAggioSottosoglia((BigDecimal) result.get(0)[4]);
        aggioAttivo.setAggioSoprasoglia((BigDecimal) result.get(0)[5]);
        aggioAttivo.setValoreCap((BigDecimal) result.get(0)[6]);
        if (result.get(0)[7].equals("1"))
            aggioAttivo.setFlagCapMultiplo(true);
        else if (result.get(0)[7].equals("0"))
            aggioAttivo.setFlagCapMultiplo(false);
        aggioAttivo.setDataCreazione((Date) result.get(0)[8]);
        aggioAttivo.setUtenteCreazione((String) result.get(0)[9]);
        aggioAttivo.setCodiceRegola((String) result.get(0)[10]);
        aggioAttivo.setTipoUltimaModifica((String) result.get(0)[11]);
        aggioAttivo.setUtenteUltimaModifica((String) result.get(0)[12]);
        aggioAttivo.setDataUltimaModifica((Date) result.get(0)[13]);

        return aggioAttivo;
    }

    public AggiMc aggioIsInCorso() {
        EntityManager entityManager = provider.get();

        String query = "select am from AggiMc am where am.flagAttivo=1 and am.finePeriodoValidazione is null";
        Query q = entityManager.createQuery(query);

        List<AggiMc> result = (List<AggiMc>) q.getResultList();

        entityManager.clear();

        if (result != null && result.size() > 0) {
            return result.get(0);
        } else {
            return null;
        }
    }

    public List<String> getValidazioneDate(String inizioPeriodoValidazione, String finePeriodoValidazione) {
        EntityManager entityManager = provider.get();

        String query = "select distinct am.codiceRegola from AggiMc am " + "where (( "
                + "		(am.inizioPeriodoValidazione between :inizioPeriodoValidazione and :finePeriodoValidazione) "
                + "		or "
                + "		(am.finePeriodoValidazione between :inizioPeriodoValidazione and :finePeriodoValidazione) "
                + ") " + "or ( " + "		am.inizioPeriodoValidazione<=:inizioPeriodoValidazione " + "		and "
                + "		am.finePeriodoValidazione>=:finePeriodoValidazione " + ")) " + "and am.flagAttivo='1' ";
        Query q = entityManager.createQuery(query);

        q.setParameter("inizioPeriodoValidazione",
                DateUtils.stringToDate(inizioPeriodoValidazione, DateUtils.DATE_ITA_FORMAT_DASH));
        q.setParameter("finePeriodoValidazione",
                DateUtils.stringToDate(finePeriodoValidazione, DateUtils.DATE_ITA_FORMAT_DASH));
        List<String> result = (List<String>) q.getResultList();

        return result;
    }

    public List<String> getValidazioneDateOnUpdate(String inizioPeriodoValidazione, String finePeriodoValidazione,
                                                   String codiceRegola) {
        EntityManager entityManager = provider.get();

        String query = "select distinct am.codiceRegola from AggiMc am " + "where am.flagAttivo='1' " + "and (" + "( "
                + "		(am.inizioPeriodoValidazione between :inizioPeriodoValidazione and :finePeriodoValidazione) "
                + "		or "
                + "		(am.finePeriodoValidazione between :inizioPeriodoValidazione and :finePeriodoValidazione) "
                + ") " + "or ( " + "		am.inizioPeriodoValidazione<=:inizioPeriodoValidazione " + "		and "
                + "		am.finePeriodoValidazione>=:finePeriodoValidazione " + ")" + ") "
                + "and am.codiceRegola != :codiceRegola ";
        Query q = entityManager.createQuery(query);

        q.setParameter("inizioPeriodoValidazione",
                DateUtils.stringToDate(inizioPeriodoValidazione, DateUtils.DATE_ITA_FORMAT_DASH));
        if (finePeriodoValidazione != null || finePeriodoValidazione.equals(""))
            q.setParameter("finePeriodoValidazione",
                    DateUtils.stringToDate(finePeriodoValidazione, DateUtils.DATE_ITA_FORMAT_DASH));
        else
            q.setParameter("finePeriodoValidazione", null);

        q.setParameter("codiceRegola", codiceRegola);
        List<String> result = (List<String>) q.getResultList();
        return result;
    }

    public List<String> getValidazioneCodiceRegola(String codiceRegola) {
        EntityManager entityManager = provider.get();

        String query = "select am.codiceRegola from AggiMc am " + "where am.flagAttivo=1 "
                + "and am.codiceRegola=:codiceRegola";
        Query q = entityManager.createQuery(query);

        q.setParameter("codiceRegola", codiceRegola);
        List<String> result = (List<String>) q.getResultList();

        return result;
    }

    public List<AggiMc> getStoricoAggio(String codiceRegola) {
        EntityManager entityManager = provider.get();

        String query = "select am from AggiMc am " + "where am.flagAttivo=0 " + "and am.codiceRegola=:codiceRegola "
                + "order by am.id desc";
        Query q = entityManager.createQuery(query);

        q.setParameter("codiceRegola", codiceRegola);
        List<AggiMc> result = (List<AggiMc>) q.getResultList();

        return result;
    }

    public void updateAggio(AggiMcDTO aggio) {

        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        f.setTimeZone(TimeZone.getTimeZone("GMT+2"));
        EntityManager entityManager = provider.get();
        AggiMc aggioMc = entityManager.find(AggiMc.class, aggio.getId());
        Date finePeriodo = null;
        if (aggio.getFinePeriodoValidazione() != null) {
            finePeriodo = DateUtils.stringToDate(aggio.getFinePeriodoValidazione(), DateUtils.DATE_ITA_FORMAT_DASH);
        }
        Date inizioPeriodo = DateUtils.stringToDate(aggio.getInizioPeriodoValidazione(),
                DateUtils.DATE_ITA_FORMAT_DASH);

        try {
            entityManager.getTransaction().begin();
            aggioMc.setInizioPeriodoValidazione(inizioPeriodo);
            aggioMc.setFinePeriodoValidazione(finePeriodo);

            aggioMc.setSoglia(new BigDecimal(aggio.getSoglia()));
            aggioMc.setAggioSottosoglia(new BigDecimal(aggio.getAggioSottosoglia()));
            aggioMc.setAggioSoprasoglia(new BigDecimal(aggio.getAggioSoprasoglia()));
            aggioMc.setValoreCap(new BigDecimal(aggio.getValoreCap()));
            aggioMc.setFlagCapMultiplo(aggio.getFlagCapMultiplo());
            aggioMc.setTipoUltimaModifica(aggio.getTipoUltimaModifica());
            aggioMc.setUtenteUltimaModifica(aggio.getUtenteUltimaModifica());
            aggioMc.setDataUltimaModifica(
                    DateUtils.stringToDate(f.format(new Date()), DateUtils.DATETIME_MYSQL_FORMAT));

            entityManager.getTransaction().commit();
            entityManager.clear();
        } catch (Exception ex) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
        }
    }

    public void updateDataAggio(Date inizioPeriodoValidazione, Date finePeriodoValidazione, Long id,
                                String utenteUltimaModifica) {

        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        f.setTimeZone(TimeZone.getTimeZone("GMT+2"));
        EntityManager entityManager = provider.get();
        AggiMc aggioMc = entityManager.find(AggiMc.class, id);

        try {
            entityManager.getTransaction().begin();
            aggioMc.setInizioPeriodoValidazione(inizioPeriodoValidazione);
            aggioMc.setFinePeriodoValidazione(finePeriodoValidazione);
            aggioMc.setTipoUltimaModifica("Variazione Validita");
            aggioMc.setUtenteUltimaModifica(utenteUltimaModifica);
            aggioMc.setDataUltimaModifica(
                    DateUtils.stringToDate(f.format(new Date()), DateUtils.DATETIME_MYSQL_FORMAT));
            entityManager.getTransaction().commit();
            entityManager.clear();
        } catch (Exception ex) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
        }
    }

    public void updateParametriAggio(AggiMcDTO aggio) {
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        f.setTimeZone(TimeZone.getTimeZone("GMT+2"));

        EntityManager entityManager = provider.get();
        AggiMc aggioMc = entityManager.find(AggiMc.class, aggio.getId());

        try {
            entityManager.getTransaction().begin();
            aggioMc.setSoglia(new BigDecimal(aggio.getSoglia()));
            aggioMc.setAggioSottosoglia(new BigDecimal(aggio.getAggioSottosoglia()));
            aggioMc.setAggioSoprasoglia(new BigDecimal(aggio.getAggioSoprasoglia()));
            aggioMc.setValoreCap(new BigDecimal(aggio.getValoreCap()));
            aggioMc.setFlagCapMultiplo(aggio.getFlagCapMultiplo());
            aggioMc.setTipoUltimaModifica("Modifica parametri");
            aggioMc.setUtenteUltimaModifica(aggio.getUtenteUltimaModifica());
            aggioMc.setDataUltimaModifica(
                    DateUtils.stringToDate(f.format(new Date()), DateUtils.DATETIME_MYSQL_FORMAT));
            entityManager.getTransaction().commit();
            entityManager.clear();
        } catch (Exception ex) {
            ex.printStackTrace();
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
        }
    }

    public AggiMc getAggioMcById(Long id) {

        EntityManager entityManager = provider.get();
        AggiMc aggio = entityManager.find(AggiMc.class, id);

        entityManager.clear();

        return aggio;
    }

    public AggiMc getDataInizioConfigurazioneAggioMax() {
        EntityManager entityManager = provider.get();

        String query = "select am from AggiMc am "
                + "where am.inizioPeriodoValidazione = (select max(am.inizioPeriodoValidazione) from AggiMc am "
                + "where am.flagAttivo='1') and " + "am.flagAttivo='1' ";
        Query q = entityManager.createQuery(query);

        List<AggiMc> result = (List<AggiMc>) q.getResultList();

        if (result != null && result.size() > 0) {
            return result.get(0);
        } else {
            return null;
        }

    }

    public List<String> getCodiciRegolaAfterDate(String inizioPeriodoValidazione) {
        EntityManager entityManager = provider.get();

        String query = "select am.codiceRegola from AggiMc am " + "where am.flagAttivo='1' and "
                + "(am.inizioPeriodoValidazione>=:inizioPeriodoValidazione "
                + "or am.finePeriodoValidazione>=:inizioPeriodoValidazione) ";
        Query q = entityManager.createQuery(query);
        q.setParameter("inizioPeriodoValidazione",
                DateUtils.stringToDate(inizioPeriodoValidazione, DateUtils.DATE_ITA_FORMAT_DASH));

        List<String> result = (List<String>) q.getResultList();

        return result;

    }

	/*public List<RiconciliazioneImportiDTO> getRiconciliazioneImportiList(
												String voceIncasso,
												String agenzia,
												String sede,
												String seprag,
												String singolaFattura,
												String inizioPeriodoContabile,
												String finePeriodoContabile,
												String inizioPeriodoFattura,
												String finePeriodoFattura,
												String order,
												Integer page) {
		List<RiconciliazioneImportiDTO> listaRic = new ArrayList<RiconciliazioneImportiDTO>();
		EntityManager entityManager = provider.get();

		String query = "select " + "	ds.CONTABILITA " + "	, ds.VOCE_INCASSO " + "	, ds.quadratura "
				+ "	, ndm.PERCENTUALE_AGGIO " + "	, ds.importoLordoSophia " + "	, ndm.IMPORTO_ORIGINALE "
				+ "	, ds.importoNettoSophia " + " , ndm.IMPORTO_AGGIO " + "	, ds.importoNettoExArt "
				+ " , ds.NUMERO_FATTURA " + " from " + "	(select " + "		ep.CONTABILITA "
				+ "	 , ep.NUMERO_FATTURA " + "	 , ep.VOCE_INCASSO "
				+ "	 , sum(ep.IMPORTO_DEM) as importoLordoSophia "
				+ "		, if(sum(ep.QUADRATURA_NDM) = count(ep.QUADRATURA_NDM), 'OK', 'KO') as quadratura "
				+ "	 , sum(ep.IMPORTO_AGGIO) as importoNettoSophia "
				+ "	 , sum(ep.IMPORTO_EX_ART_18) as importoNettoExArt " + "	 , agenzia " + "	 , sede "
				+ "	 , seprag " + "	from PERF_EVENTI_PAGATI ep " + "	where "
				+ "		ep.QUADRATURA_NDM is not null " + "	 	and (?5 is null or ep.NUMERO_FATTURA = ?5) "
				+ "	 and (?6 is null or ep.CONTABILITA >= ?6) " + "	 and (?7 is null or ep.CONTABILITA <= ?7) "
				+ "		and (?1 is null or VOCE_INCASSO = ?1) "
				+ "	 	and (?2 is null or upper(ep.agenzia) = upper(?2)) "
				+ "	 and (?3 is null or upper(ep.SEDE) = upper(?3)) "
				+ "	 and (?4 is null or upper(ep.seprag) = upper(?4)) " + "	group by ep.CONTABILITA, "
				+ "	 ep.NUMERO_FATTURA, " + "	 ep.VOCE_INCASSO " + "	) as ds " + "	join " + "	( "
				+ "		select * " + "		from PERF_NDM_VOCE_FATTURA " + "	 where "
				+ "	 	(?5 is null or NUMERO_FATTURA = ?5) "
				+ "		and (?6 is null or DATE_FORMAT(DATA_CONTABILE_FATTURA, '%Y%m') >= ?6) "
				+ "	 and (?7 is null or DATE_FORMAT(DATA_CONTABILE_FATTURA, '%Y%m') <= ?7) "
				+ "	 and (?8 is null or DATA_FATTURA >= ?8) " + "		and (?9 is null or DATA_FATTURA <= ?9) "
				+ "	 and (?1 is null or VOCE = ?1) "
				+ "	) as ndm on (ds.NUMERO_FATTURA = ndm.NUMERO_FATTURA and ds.VOCE_INCASSO = ndm.voce) ";

		if (order == null) {
			query = query + "order by " + "	ds.CONTABILITA, " + " ds.NUMERO_FATTURA, " + " ds.VOCE_INCASSO";
		} else if (order.startsWith("IMPORTO_ORIGINALE") || order.startsWith("IMPORTO_AGGIO")
				|| order.startsWith("PERCENTUALE_AGGIO")) {
			query = query + "order by ndm." + order;
		} else {
			query = query + "order by ds." + order;
		}

		Query q = entityManager.createNativeQuery(query);

		int recordCount = 0;

		int startRecord = 0;
		int endRecord = recordCount;

		if (page != -1) {

			startRecord = (page) * ReportPage.NUMBER_RECORD_PER_PAGE;
			endRecord = ReportPage.NUMBER_RECORD_PER_PAGE;
		}

		q.setParameter(1, voceIncasso);
		q.setParameter(2, agenzia);
		q.setParameter(3, sede);
		q.setParameter(4, seprag);
		q.setParameter(5, singolaFattura);
		q.setParameter(6, inizioPeriodoContabile);
		q.setParameter(7, finePeriodoContabile);
		if (inizioPeriodoFattura != null)
			q.setParameter(8, DateUtils.stringToDate(inizioPeriodoFattura, DateUtils.DATE_ITA_FORMAT_DASH));
		else
			q.setParameter(8, null);
		if (finePeriodoFattura != null)
			q.setParameter(9, DateUtils.stringToDate(finePeriodoFattura, DateUtils.DATE_ITA_FORMAT_DASH));
		else
			q.setParameter(9, null);

		List<Object[]> results = (List<Object[]>) q.setFirstResult(startRecord).setMaxResults(endRecord)
				.getResultList();
		for (int i = 0; i < results.size(); i++) {

			RiconciliazioneImportiDTO riconciliazioneImporti = new RiconciliazioneImportiDTO((Long) results.get(i)[0],
					(String) results.get(i)[1], (String) results.get(i)[2], (BigDecimal) results.get(i)[3],
					(BigDecimal) results.get(i)[4], (BigDecimal) results.get(i)[5], (BigDecimal) results.get(i)[6],
					(BigDecimal) results.get(i)[7], (BigDecimal) results.get(i)[8], (String) results.get(i)[9]);
			// riconciliazioneImporti.setContabilita((Long)results.get(i)[0]);
			// riconciliazioneImporti.setVoceIncasso((String)results.get(i)[1]);
			// riconciliazioneImporti.setTotaleLordoSophia((BigDecimal)results.get(i)[2]);
			// riconciliazioneImporti.setTotaleLordoNDM((BigDecimal)results.get(i)[3]);
			// riconciliazioneImporti.setSemaforo((String)results.get(i)[4]);
			// riconciliazioneImporti.setTotaleNettoSophia((BigDecimal)results.get(i)[5]);
			// riconciliazioneImporti.setTotaleNettoNDM((BigDecimal)results.get(i)[6]);
			// riconciliazioneImporti.setPercentualeAggioNDM((BigDecimal)results.get(i)[7]);
			// riconciliazioneImporti.setTotaleSophiaExArt((BigDecimal)results.get(i)[8]);

			listaRic.add(riconciliazioneImporti);
		}
		return listaRic;
	}*/


    public ReportPage getRiconciliazioneImportiList(String voceIncasso,
                                                    String agenzia,
                                                    String sede,
                                                    String seprag,
                                                    String numeroFattura,
                                                    String inizioPeriodoContabile,
                                                    String finePeriodoContabile,
                                                    Date inizioPeriodoFattura,
                                                    Date finePeriodoFattura,
                                                    String order,
                                                    Integer page,
                                                    String fatturaValidata,
                                                    String megaConcerto) {

        DSLContext dslContext = jooq;

        SelectQuery<Record13<Integer, String, String, BigDecimal, BigDecimal, Boolean, BigDecimal, BigDecimal, BigDecimal, BigDecimal,String, String, String>> select = dslContext.select(
        field("PERIODO", Integer.class).as("periodo"),
        field("NUMERO_FATTURA", String.class).as("numeroFattura"),
        field("VOCE_INCASSO", String.class).as("voceIncasso"),
        field("LORDO_SOPHIA", BigDecimal.class).as("lordoSophia"),
        field("LORDO_NDM", BigDecimal.class).as("lordoNdm"),
        field("SEMAFORO", Boolean.class).as("semaforo"),
        field("NETTO_SOPHIA", BigDecimal.class).as("nettoSophia"),
        field("NETTO_NDM", BigDecimal.class).as("nettoNdm"),
        field("PERC_AGGIO_NDM", BigDecimal.class).as("percAggioNdm"),
        field("NETTO_SOPHIA_ART_18", BigDecimal.class).as("nettoSophiaArt"),
        field("SEDE", String.class).as("sede"),
		field("AGENZIA", String.class).as("agenzia"),
		field("SEPRAG", String.class).as("seprag"))
        .from(table("PERF_RICONC_MV"))
        .getQuery();

        addConditions(voceIncasso, agenzia, sede, seprag, numeroFattura, inizioPeriodoContabile, finePeriodoContabile, inizioPeriodoFattura, finePeriodoFattura,fatturaValidata,megaConcerto, select);

//        select.addGroupBy(
//                select.field("PERIODO"),
//                select.field("NUMERO_FATTURA"),
//                select.field("VOCE_INCASSO")
//        );

        if(page!=-1) {
            if (order == null) {
                select.addOrderBy(
                        field("PERIODO"),
                        field("NUMERO_FATTURA"),
                        field("VOCE_INCASSO")
                );
            } else if (order.startsWith("LORDO_NDM") || order.startsWith("NETTO_NDM")
                    || order.startsWith("PERC_AGGIO_NDM")) {
                if (order.split("\\s+")[1].equals("asc"))
                    select.addOrderBy(field(order.split("\\s+")[0]));
                else
                    select.addOrderBy(field(order.split("\\s+")[0]).desc());
            } else {
                if (order.split("\\s+")[1].equals("asc"))
                    select.addOrderBy(field(order.split("\\s+")[0]));
                else
                    select.addOrderBy(field(order.split("\\s+")[0]).desc());
            }
        }

        int startRecord = 0;
        int endRecord = 0;

        if (page != -1) {
            startRecord = (page) * ReportPage.NUMBER_RECORD_PER_PAGE;
            endRecord = ReportPage.NUMBER_RECORD_PER_PAGE;
        }

        if (page != -1) {
            select.addLimit(startRecord, endRecord);
        }

        List<PerfRiconcMvDTO> perfRiconcMvDTOS = select.fetchInto(PerfRiconcMvDTO.class);

        ReportPage reportPage = new ReportPage(perfRiconcMvDTOS, page);

        if (page != -1) {
            reportPage.setFieldNames(RiconciliazioneImportiDTO.fields);
        }
        return reportPage;
    }


    public void addConditions(String voceIncasso, String agenzia, String sede, String seprag, String numeroFattura, String inizioPeriodoContabile, String finePeriodoContabile, Date inizioPeriodoFattura, Date finePeriodoFattura, String fatturaValidata, String megaConcerto, SelectQuery<Record13<Integer, String, String, BigDecimal, BigDecimal, Boolean, BigDecimal, BigDecimal, BigDecimal, BigDecimal, String, String, String>> select) {
        if (StringUtils.isNotEmpty(inizioPeriodoContabile)) {
            select.addConditions(field("PERIODO", String.class).ge(inizioPeriodoContabile));
        }
        if (StringUtils.isNotEmpty(finePeriodoContabile)) {
            select.addConditions(field("PERIODO", String.class).le(finePeriodoContabile));
        }
        if (StringUtils.isNotEmpty(numeroFattura)) {
            select.addConditions(field("NUMERO_FATTURA", String.class).eq(numeroFattura));
        }
        if (StringUtils.isNotEmpty(voceIncasso)) {
            select.addConditions(field("VOCE_INCASSO", String.class).eq(voceIncasso));
        }
        if (StringUtils.isNotEmpty(agenzia)) {
            select.addConditions(field("AGENZIA", String.class).eq(agenzia));
        }
        if (StringUtils.isNotEmpty(sede)) {
            select.addConditions(field("SEDE", String.class).eq(sede));
        }
        if (StringUtils.isNotEmpty(seprag)) {
            select.addConditions(field("SEPRAG", String.class).eq(seprag));
        }
        if (inizioPeriodoFattura!=null) {
            select.addConditions(field("DATA_CONTABILE_FATTURA").greaterThan(inizioPeriodoFattura));
        }
        if (finePeriodoFattura!=null) {
            select.addConditions(field("DATA_CONTABILE_FATTURA").lessThan(finePeriodoFattura));
        }

        if ((fatturaValidata != null) && (!fatturaValidata.equalsIgnoreCase("tutti"))) {
            if (fatturaValidata.equalsIgnoreCase("si")) {
                select.addConditions(field("SEMAFORO").eq(1));
            } else if (fatturaValidata.equalsIgnoreCase("no")) {
                select.addConditions(field("SEMAFORO").eq(0).or(field("SEMAFORO").isNull()));
            }
        }
        
        if ((megaConcerto != null) && (!megaConcerto.equalsIgnoreCase("tutti")))  {
            if (megaConcerto.equalsIgnoreCase("si")) {
                select.addConditions(field("FLAG_MEGACONCERT").eq(1));
            } else if (megaConcerto.equalsIgnoreCase("no")) {
                select.addConditions(field("FLAG_MEGACONCERT").eq(0).or(field("FLAG_MEGACONCERT").isNull()));
            }
        }
        

        
        // if flag_fattura_validata! = null && diversi tutti, -- aggiungi condizione su semaforo: se è si metti true e viceversa
        
    }

    public TotaleRiconciliazioneImporti getCountRiconciliazioneImportiList(String voceIncasso, String agenzia, String sede, String seprag,
                                                   String numeroFattura, String inizioPeriodoContabile, String finePeriodoContabile,
                                                   Date inizioPeriodoFattura, Date finePeriodoFattura, String fatturaValidata, String megaConcerto) {

        TotaleRiconciliazioneImporti totaleRiconciliazioneImporti = new TotaleRiconciliazioneImporti();

             totaleRiconciliazioneImporti.setRecordTotali(Long.valueOf(getRiconciliazioneImportiList(voceIncasso,
                agenzia,
                sede,
                seprag,
                numeroFattura,
                inizioPeriodoContabile,
                finePeriodoContabile,
                inizioPeriodoFattura,
                finePeriodoFattura,
                null, -1,
                fatturaValidata,
                megaConcerto).getRecords().size()));

        for (Object record : getRiconciliazioneImportiList(voceIncasso,
                agenzia,
                sede,
                seprag,
                numeroFattura,
                inizioPeriodoContabile,
                finePeriodoContabile,
                inizioPeriodoFattura,
                finePeriodoFattura,
                null, -1,
                fatturaValidata,
                megaConcerto).getRecords()) {
            if (record instanceof PerfRiconcMvDTO) {

                PerfRiconcMvDTO dto = (PerfRiconcMvDTO)record;
                totaleRiconciliazioneImporti.setTotaleLordoSophia(totaleRiconciliazioneImporti.getTotaleLordoSophia().add(dto.getLordoSophia()!= null ? dto.getLordoSophia().setScale(2, BigDecimal.ROUND_HALF_EVEN): new BigDecimal(0)));
                totaleRiconciliazioneImporti.setTotaleLordoNdm(totaleRiconciliazioneImporti.getTotaleLordoNdm().add(dto.getLordoNdm() != null ? dto.getLordoNdm().setScale(2, BigDecimal.ROUND_HALF_EVEN) : new BigDecimal(0)));
                totaleRiconciliazioneImporti.setTotaleNettoSophiaAggio(totaleRiconciliazioneImporti.getTotaleNettoSophiaAggio().add(dto.getNettoSophia() != null ? dto.getNettoSophia().setScale(2, BigDecimal.ROUND_HALF_EVEN) : new BigDecimal(0)));
                totaleRiconciliazioneImporti.setTotaleNettoNdmAggio(totaleRiconciliazioneImporti.getTotaleNettoNdmAggio().add(dto.getNettoNdm() != null ? dto.getNettoNdm().setScale(2, BigDecimal.ROUND_HALF_EVEN) : new BigDecimal(0)));
                totaleRiconciliazioneImporti.setTotaleNettoSophia(totaleRiconciliazioneImporti.getTotaleNettoSophia().add(dto.getNettoSophiaArt() != null ? dto.getNettoSophiaArt().setScale(2, BigDecimal.ROUND_HALF_EVEN) : new BigDecimal(0)));

            }

        }

             return totaleRiconciliazioneImporti;

    }

 /*public long getCountRiconciliazioneImportiList(String voceIncasso, String agenzia, String sede, String seprag,
 String singolaFattura, String inizioPeriodoContabile, String finePeriodoContabile,
 String inizioPeriodoFattura, String finePeriodoFattura) {

 EntityManager entityManager = provider.get();

 String query = "select count(*) from " + "	(select " + "		ep.CONTABILITA "
 + "	 , ep.NUMERO_FATTURA " + "	 , ep.VOCE_INCASSO " + "	 , sum(ep.IMPORTO_DEM) "
 + "		, if(sum(ep.QUADRATURA_NDM) = count(ep.QUADRATURA_NDM), 'OK', 'KO') as quadratura "
 + "	 , sum(ep.IMPORTO_AGGIO) " + "	 , sum(ep.IMPORTO_EX_ART_18) " + "	 , agenzia "
 + "	 , sede " + "	 , seprag " + "	from PERF_EVENTI_PAGATI ep " + "	where "
 + "		ep.QUADRATURA_NDM is not null " + "	 	and (?5 is null or ep.NUMERO_FATTURA = ?5) "
 + "	 and (?6 is null or ep.CONTABILITA >= ?6) " + "	 and (?7 is null or ep.CONTABILITA <= ?7) "
 + "		and (?1 is null or VOCE_INCASSO = ?1) "
 + "	 	and (?2 is null or upper(ep.agenzia) = upper(?2)) "
 + "	 and (?3 is null or upper(ep.SEDE) = upper(?3)) "
 + "	 and (?4 is null or upper(ep.seprag) = upper(?4)) " + "	group by ep.CONTABILITA, "
 + "	 ep.NUMERO_FATTURA, " + "	 ep.VOCE_INCASSO " + "	) as ds " + "	join " + "	( "
 + "		select * " + "		from PERF_NDM_VOCE_FATTURA " + "	 where "
 + "	 	(?5 is null or NUMERO_FATTURA = ?5) "
 + "		and (?6 is null or DATE_FORMAT(DATA_CONTABILE_FATTURA, '%Y%m') >= ?6) "
 + "	 and (?7 is null or DATE_FORMAT(DATA_CONTABILE_FATTURA, '%Y%m') <= ?7) "
 + "	 and (?8 is null or DATA_FATTURA >= ?8) " + "		and (?9 is null or DATA_FATTURA <= ?9) "
 + "	 and (?1 is null or VOCE = ?1) "
 + "	) as ndm on (ds.NUMERO_FATTURA = ndm.NUMERO_FATTURA and ds.VOCE_INCASSO = ndm.voce) "
 + "order by " + "	ds.CONTABILITA, " + " ds.NUMERO_FATTURA, " + " ds.VOCE_INCASSO";

 Query q = entityManager.createNativeQuery(query);

 q.setParameter(1, voceIncasso);
 q.setParameter(2, agenzia);
 q.setParameter(3, sede);
 q.setParameter(4, seprag);
 q.setParameter(5, singolaFattura);
 q.setParameter(6, inizioPeriodoContabile);
 q.setParameter(7, finePeriodoContabile);
 if (inizioPeriodoFattura != null)
 q.setParameter(8, DateUtils.stringToDate(inizioPeriodoFattura, DateUtils.DATE_ITA_FORMAT_DASH));
 else
 q.setParameter(8, null);
 if (finePeriodoFattura != null)
 q.setParameter(9, DateUtils.stringToDate(finePeriodoFattura, DateUtils.DATE_ITA_FORMAT_DASH));
 else
 q.setParameter(9, null);

 Long result = (Long) q.getSingleResult();
 return result;
 }*/

    public void updateFlagMegaConcert(FatturaDetailDTO fatturaDetailDTO) {
        EntityManager entityManager = provider.get();
        Manifestazione manifestazione = entityManager.find(Manifestazione.class,
                fatturaDetailDTO.getManifestazione().getId());

        try {
            entityManager.getTransaction().begin();
            manifestazione.setFlagMegaConcert(fatturaDetailDTO.getManifestazione().getFlagMegaConcert());
            entityManager.persist(manifestazione);
            entityManager.getTransaction().commit();
            entityManager.clear();
            ;
        } catch (Exception ex) {
            if (entityManager.getTransaction().isActive()) {
                entityManager.getTransaction().rollback();
            }
        }

    }

    public NDMVoceFattura getNdmVoceFattura(String voceIncasso, String numeroFattura) {
        EntityManager entityManager = provider.get();

        String query = "select vf from NDMVoceFattura vf " + "where vf.voceIncasso = :voceIncasso "
                + "and vf.numeroFattura = :numeroFattura";
        Query q = entityManager.createQuery(query);

        q.setParameter("voceIncasso", voceIncasso);
        q.setParameter("numeroFattura", numeroFattura);

        List<NDMVoceFattura> result = (List<NDMVoceFattura>) q.getResultList();

        if (result != null && result.size() > 0) {
            return result.get(0);
        } else {
            return null;
        }

    }

    public List<RiconciliazioneImportiDTO> getRiconciliazioneFattura(String numeroFattura) {
        EntityManager entityManager = provider.get();

        String sql = "SELECT ds.NUMERO_FATTURA, " +
                "       ds.VOCE_INCASSO, " +
                "       ds.IMPORTO_SOPHIA, " +
                "       ds.QUADRATURA, " +
                "       ndm.IMPORTO_NDM, " +
                "       IF (ndm.IMPORTO_NDM IS NOT NULL,ABS(ds.IMPORTO_SOPHIA - ndm.IMPORTO_NDM),NULL) AS DELTA_ASSOLUTO, " +
                "       IF (ndm.IMPORTO_NDM IS NOT NULL AND ndm.IMPORTO_NDM <> 0.0,ABS((ds.IMPORTO_SOPHIA - ndm.IMPORTO_NDM)*100 / ndm.IMPORTO_NDM),NULL) AS DELTA_ASSOLUTO_PERC, " +
                "       ds.FORZATURA_NDM, " +
                "       IF (ds.QUADRATURA = 'KO' AND ndm.IMPORTO_NDM IS NOT NULL AND ds.IMPORTO_SOPHIA <> ndm.IMPORTO_NDM AND ds.VOCE_INCASSO = ndm.voce,TRUE,FALSE) AS RICONCILIABILE_PER_IMPORTO, " +
                "       FALSE AS RICONCILIABILE_PER_VOCE_INCASSO " +                
                "FROM (SELECT ep.NUMERO_FATTURA, " +
                "             ep.VOCE_INCASSO, " +
                "             SUM(ep.IMPORTO_DEM) AS IMPORTO_SOPHIA, " +
                "             IF (SUM(ep.QUADRATURA_NDM) = COUNT(ep.QUADRATURA_NDM),'OK','KO') AS QUADRATURA, " +
                "             MAX(ep.FORZATURA_NDM) AS FORZATURA_NDM " +
                "      FROM PERF_EVENTI_PAGATI ep " +
                "      WHERE ep.NUMERO_FATTURA = ?1 " +
                "      GROUP BY ep.NUMERO_FATTURA, " +
                "               ep.VOCE_INCASSO) AS ds " +
                "  LEFT JOIN (SELECT vf.NUMERO_FATTURA, " +
                "                    MAX(vf.IMPORTO_ORIGINALE) AS IMPORTO_NDM, " +
                "                    vf.voce " +
                "             FROM PERF_NDM_VOCE_FATTURA vf " +
                "             WHERE vf.NUMERO_FATTURA = ?1 " +
                "			  GROUP BY vf.NUMERO_FATTURA,vf.voce ) AS ndm " +
                "         ON (ds.NUMERO_FATTURA = ndm.NUMERO_FATTURA " +
                "        AND ds.VOCE_INCASSO = ndm.voce)";
        
        Query q = entityManager.createNativeQuery(sql);

        q.setParameter(1, numeroFattura);

        List<Object[]> result = (List<Object[]>) q.getResultList();
        List<RiconciliazioneImportiDTO> resultList = new ArrayList<RiconciliazioneImportiDTO>();

        for (int i = 0; i < result.size(); i++) {
            RiconciliazioneImportiDTO riconciliazioneImporti = new RiconciliazioneImportiDTO();
            riconciliazioneImporti.setNumeroFattura((String) result.get(i)[0]);
            riconciliazioneImporti.setVoceIncasso((String) result.get(i)[1]);
            riconciliazioneImporti.setTotaleLordoSophia((BigDecimal) result.get(i)[2]);
            riconciliazioneImporti.setSemaforo((String) result.get(i)[3]);
            riconciliazioneImporti.setTotaleLordoNDM((BigDecimal) result.get(i)[4]);
            riconciliazioneImporti.setDeltaAssoluto((BigDecimal) result.get(i)[5]);
            riconciliazioneImporti.setDeltaPercentuale((BigDecimal) result.get(i)[6]);
            riconciliazioneImporti.setForzaturaNDM((String)result.get(i)[7]);
            riconciliazioneImporti.setRiconciliabilePerImporto((Long)result.get(i)[8] > 0);
            riconciliazioneImporti.setRiconciliabilePerVoceIncasso((Long)result.get(i)[9] > 0);
            resultList.add(riconciliazioneImporti);
        }
        
        fatturaRiconciliabilePerVoceIncasso(resultList);
        
        return resultList;
    }

	protected void fatturaRiconciliabilePerVoceIncasso(List<RiconciliazioneImportiDTO> resultList) {
		if(resultList ==null || resultList.isEmpty()) return;
		EntityManager entityManager = provider.get();
		String fallBackSql = "SELECT ds.NUMERO_FATTURA, " +
                "       ndm.VOCE, " +
                "       ds.IMPORTO_SOPHIA, " +
                "       ds.QUADRATURA, " +
                "       ndm.IMPORTO_NDM, " +
                "       IF (ndm.IMPORTO_NDM IS NOT NULL,ABS(ds.IMPORTO_SOPHIA - ndm.IMPORTO_NDM),NULL) AS DELTA_ASSOLUTO, " +
                "       IF (ndm.IMPORTO_NDM IS NOT NULL AND ndm.IMPORTO_NDM <> 0.0,ABS((ds.IMPORTO_SOPHIA - ndm.IMPORTO_NDM)*100 / ndm.IMPORTO_NDM),NULL) AS DELTA_ASSOLUTO_PERC, " +
                "       ds.FORZATURA_NDM, " +
                "       FALSE AS  RICONCILIABILE_PER_IMPORTO, " +
                "       TRUE AS  RICONCILIABILE_PER_VOCE_INCASSO " +
                "        FROM (SELECT ep.NUMERO_FATTURA,  " +
                "                     ep.VOCE_INCASSO,  " +
                "                     SUM(ep.IMPORTO_DEM) AS IMPORTO_SOPHIA,  " +
                "                      IF (SUM(ep.QUADRATURA_NDM) = COUNT(ep.QUADRATURA_NDM),'OK','KO') AS QUADRATURA, " +
                "                      MAX(ep.FORZATURA_NDM) AS FORZATURA_NDM, " +
                "                     COUNT(*) AS TOT_EVENTI  " +
                "              FROM PERF_EVENTI_PAGATI ep   " +
                "              WHERE ep.NUMERO_FATTURA = ?1 AND ep.QUADRATURA_NDM !='1' " +
                "              GROUP BY ep.NUMERO_FATTURA,  " +
                "                       ep.VOCE_INCASSO) AS ds  " +
                "          LEFT JOIN (SELECT vf.NUMERO_FATTURA,  " +
                "                            vf.voce,  " +
                "                            SUM(vf.IMPORTO_ORIGINALE) AS IMPORTO_NDM,  " +
                "                            COUNT(*) AS TOT_FATTURE  " +
                "                     FROM PERF_NDM_VOCE_FATTURA vf  " +
                "                     WHERE vf.NUMERO_FATTURA = ?1  " +
                "                     GROUP BY vf.NUMERO_FATTURA,  " +
                "                              vf.VOCE) AS ndm ON (ds.NUMERO_FATTURA = ndm.NUMERO_FATTURA)  " +
                "        WHERE ds.IMPORTO_SOPHIA = ndm.IMPORTO_NDM  " +
                "        AND   ds.TOT_EVENTI = ndm.TOT_FATTURE  " +
                "        AND   NOT EXISTS (SELECT 1  " +
                "                          FROM PERF_EVENTI_PAGATI epe  " +
                "                            JOIN PERF_NDM_VOCE_FATTURA vfe  " +
                "                              ON (epe.NUMERO_FATTURA = vfe.NUMERO_FATTURA  " +
                "                             AND epe.VOCE_INCASSO = vfe.VOCE)  " +
                "                          WHERE epe.QUADRATURA_NDM !='1' AND epe.VOCE_INCASSO = ds.VOCE_INCASSO AND epe.NUMERO_FATTURA = ds.NUMERO_FATTURA)";
        
        
        
        for (RiconciliazioneImportiDTO dto : resultList) {
			if (dto.getNumeroFattura() !=null && dto.getVoceIncasso()!=null && "KO".equals(dto.getSemaforo()) && dto.getTotaleLordoNDM() == null && dto.getDeltaAssoluto() == null
					&& "N".equals(dto.getForzaturaNDM()) && !dto.isRiconciliabilePerImporto()) {
				
				Query fallBackQuery = entityManager.createNativeQuery(fallBackSql);
				
				fallBackQuery.setParameter(1, dto.getNumeroFattura());

		        List<Object[]> fallBackResult = (List<Object[]>) fallBackQuery.getResultList();
		        for (int i = 0; i < fallBackResult.size(); i++) {
		        	dto.setNumeroFattura((String) fallBackResult.get(i)[0]);
		        	dto.setVoceIncassoSophia(dto.getVoceIncasso());
		        	dto.setVoceIncasso((String) fallBackResult.get(i)[1]);
		        	dto.setTotaleLordoSophia((BigDecimal) fallBackResult.get(i)[2]);
		        	dto.setSemaforo((String) fallBackResult.get(i)[3]);
		        	dto.setTotaleLordoNDM((BigDecimal) fallBackResult.get(i)[4]);
		        	dto.setDeltaAssoluto((BigDecimal) fallBackResult.get(i)[5]);
		        	dto.setDeltaPercentuale((BigDecimal) fallBackResult.get(i)[6]);
		        	dto.setForzaturaNDM((String)fallBackResult.get(i)[7]);
		        	dto.setRiconciliabilePerImporto((Long)fallBackResult.get(i)[8] > 0);
		        	dto.setRiconciliabilePerVoceIncasso((Long)fallBackResult.get(i)[9] > 0);
		        }				

			}
		}
	}

    public Connection getConfiguration() {
        EntityManager entityManager = provider.get();
        entityManager.getTransaction().begin();
        Connection connection = entityManager.unwrap(Connection.class);
        entityManager.getTransaction().commit();
        entityManager.clear();

        return connection;
    }

    public List<Long> getEventiToEditOnAggio(AggiMcDTO aggio) {
        EntityManager entityManager = provider.get();

        String query = "select distinct ep.ID_EVENTO from PERF_EVENTI_PAGATI ep "
                + "join PERF_MANIFESTAZIONE ma on ep.ID_EVENTO=ma.ID_EVENTO "
                + "where (date(ep.DATA_REVERSALE) >= ?1) " + "and (?2 is null or date(ep.DATA_REVERSALE) <=?2) "
                + "and ep.QUADRATURA_NDM is not null " + "and ma.FLAG_MEGACONCERT=1";
        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, DateUtils.stringToDate(aggio.getInizioPeriodoValidazione(), DateUtils.DATE_ITA_FORMAT_DASH));
        if (aggio.getFinePeriodoValidazione() != null)
            q.setParameter(2,
                    DateUtils.stringToDate(aggio.getFinePeriodoValidazione(), DateUtils.DATE_ITA_FORMAT_DASH));
        else
            q.setParameter(2, null);

        List<Long> result = (List<Long>) q.getResultList();
        return result;

    }

    public AggiMc getAggioByDate(Date dataFattura) {
        EntityManager entityManager = provider.get();

        AggiMc aggio = new AggiMc();
        String query = "select mc from AggiMc mc " + "where (mc.inizioPeriodoValidazione <=:dataFattura) "
                + "and (mc.finePeriodoValidazione is null or mc.finePeriodoValidazione >=:dataFattura) "
                + "and mc.flagAttivo=1 ";
        Query q = entityManager.createQuery(query);

        q.setParameter("dataFattura", dataFattura);
        // if(aggio.getFinePeriodoValidazione()!=null)
        // q.setParameter("finePeriodo",
        // DateUtils.stringToDate(aggio.getFinePeriodoValidazione(),
        // DateUtils.DATE_ITA_FORMAT_DASH));
        // else
        // q.setParameter("finePeriodo", null);

        List<AggiMc> result = (List<AggiMc>) q.getResultList();
        if (result.size() == 0) {
            return aggio;
        } else {
            return result.get(0);
        }

    }

    @Override
    public PerfCodificaDTO getCodifica(Long idCombana, String utente) {
        EntityManager entityManager = provider.get();

        /*
         * String query = "select pua.perfCodifica " +
         * "from PerfUtilizzazioniAssegnate pua " + "join pua.perfSessioneCodifica psc "
         * +
         * "where (CURRENT_TIMESTAMP not between psc.dataInizioValid and psc.dataFineValid "
         * + "or psc.utente = :utente) "+
         * "and pua.perfCodifica.perfCombana.idCombana = :idCombana " +
         * "group by pua.perfCodifica.idCodifica"; TypedQuery<PerfCodificaDTO> q =
         * entityManager.createQuery(query, PerfCodificaDTO.class)
         * .setParameter("idCombana", idCombana) .setParameter("utente", utente);
         */

        String query = "SELECT new com.alkemytech.sophia.codman.dto.performing.PerfCodificaDTO("
                + "cod.idCodifica, cod.candidate, cod.codiceOperaApprovato, cod.codiceOperaSuggerito, cod.confidenza, cod.dataApprovazione, "
                + "cod.dataCodifica, cod.dataFineValidita, cod.distSecCandidata, cod.nUtilizzazioni, cod.preziosa, cod.statoApprovazione, "
                + "cod.tipoApprovazione, cod.valoreEconomico) "
                + "FROM PerfCodifica cod JOIN cod.perfCombana cb WHERE cb.idCombana = :idCombana "
                + "AND cod.dataFineValidita IS NULL AND cod.codiceOperaApprovato IS NULL AND NOT EXISTS(SELECT 1 FROM PerfCodifica cod1 "
                + "WHERE cod1.idCodifica != cod.idCodifica AND cod1.dataCodifica > cod.dataCodifica AND cod1.perfCombana = cod.perfCombana)"
                + "AND NOT EXISTS(SELECT 1 FROM PerfSessioneCodifica sc JOIN sc.perfUtilizzazioniAssegnates ua WHERE( sc.dataFineValid > "
                + "CURRENT_TIMESTAMP OR sc.dataFineValid IS NULL) AND ua.perfCodifica = cod) AND NOT EXISTS(SELECT 1 FROM "
                + "PerfSessioneCodifica sc1 JOIN sc1.perfUtilizzazioniAssegnates ua1 WHERE sc1.utente = :utente AND sc1.dataFineValid > "
                + ":date AND ua1.perfCodifica = cod AND ua1.tipoAzione IS NOT NULL)";

        Calendar cal = java.util.Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, -7);

        TypedQuery<PerfCodificaDTO> q = entityManager.createQuery(query, PerfCodificaDTO.class)
                .setParameter("idCombana", idCombana).setParameter("utente", utente)
                .setParameter("date", new java.sql.Timestamp(cal.getTimeInMillis()));

        return q.getSingleResult();

    }

	@Override
	public void riconciliaImporti(RiconciliazioneImportiDTO riconciliazioneImporti, String username) {
		EntityManager em = provider.get();
		String numeroFattura = riconciliazioneImporti.getNumeroFattura();
		String voceIncasso = riconciliazioneImporti.getVoceIncasso();

		String eventiDaAggiornareSql = "SELECT PERF_EVENTI_PAGATI.ID_EVENTO_PAGATO, " +
	             "       PERF_EVENTI_PAGATI.IMPORTO_DEM, " +
	             "       PERF_EVENTI_PAGATI.IMPORTO_DEM*(PERF_NDM_VOCE_FATTURA.IMPORTO_ORIGINALE / TOTALE_IMPORTI.TOT_DEM) NUOVO_IMPORTO " +
	             "FROM PERF_EVENTI_PAGATI " +
	             "  LEFT JOIN (SELECT NUMERO_FATTURA, " +
	             "                    VOCE_INCASSO, " +
	             "                    SUM(IMPORTO_DEM) AS TOT_DEM " +
	             "             FROM PERF_EVENTI_PAGATI " +
	             "             WHERE PERF_EVENTI_PAGATI.NUMERO_FATTURA = ?1 AND PERF_EVENTI_PAGATI.VOCE_INCASSO = ?2 " +
	             "             GROUP BY NUMERO_FATTURA, " +
	             "                      VOCE_INCASSO) AS TOTALE_IMPORTI " +
	             "         ON (PERF_EVENTI_PAGATI.NUMERO_FATTURA = TOTALE_IMPORTI.NUMERO_FATTURA " +
	             "        AND PERF_EVENTI_PAGATI.VOCE_INCASSO = TOTALE_IMPORTI.VOCE_INCASSO) " +
	             "  LEFT JOIN PERF_NDM_VOCE_FATTURA " +
	             "         ON (PERF_NDM_VOCE_FATTURA.NUMERO_FATTURA = PERF_EVENTI_PAGATI.NUMERO_FATTURA " +
	             "        AND PERF_NDM_VOCE_FATTURA.VOCE = PERF_EVENTI_PAGATI.VOCE_INCASSO) " +
	             "WHERE NOT (PERF_NDM_VOCE_FATTURA.ID_NDM_VOCE_FATTURA IS NULL) " +
	             "AND   PERF_EVENTI_PAGATI.QUADRATURA_NDM != '1' " +
	             "AND   PERF_EVENTI_PAGATI.FORZATURA_NDM = 'N' " +
	             "AND   PERF_EVENTI_PAGATI.NUMERO_FATTURA = ?1 AND PERF_EVENTI_PAGATI.VOCE_INCASSO = ?2 " +
	             "AND   PERF_NDM_VOCE_FATTURA.IMPORTO_ORIGINALE IS NOT NULL";
		
		Query eventiDaAggiornareQuery = em.createNativeQuery(eventiDaAggiornareSql).setParameter(1, numeroFattura).setParameter(2, voceIncasso);

		String riconciliazioneSql = "UPDATE PERF_EVENTI_PAGATI " +
	             "LEFT JOIN (SELECT NUMERO_FATTURA, " +
	             "       VOCE_INCASSO, " +
	             "       SUM(IMPORTO_DEM) AS TOT_DEM " +
	             "FROM PERF_EVENTI_PAGATI " +
	             "WHERE PERF_EVENTI_PAGATI.NUMERO_FATTURA = ?1 AND PERF_EVENTI_PAGATI.VOCE_INCASSO = ?2 " +
	             "GROUP BY NUMERO_FATTURA, " +
	             "         VOCE_INCASSO) AS TOTALE_IMPORTI ON (PERF_EVENTI_PAGATI.NUMERO_FATTURA = TOTALE_IMPORTI.NUMERO_FATTURA AND PERF_EVENTI_PAGATI.VOCE_INCASSO = TOTALE_IMPORTI.VOCE_INCASSO)  " +
	             "LEFT JOIN PERF_NDM_VOCE_FATTURA ON (PERF_NDM_VOCE_FATTURA.NUMERO_FATTURA = PERF_EVENTI_PAGATI.NUMERO_FATTURA AND PERF_NDM_VOCE_FATTURA.VOCE = PERF_EVENTI_PAGATI.VOCE_INCASSO)  " +
	             "SET PERF_EVENTI_PAGATI.QUADRATURA_NDM = '1', " +
	             "         PERF_EVENTI_PAGATI.FORZATURA_NDM = 'M', " +
	             "         PERF_EVENTI_PAGATI.IMPORTO_DEM_NON_FORZATO = PERF_EVENTI_PAGATI.IMPORTO_DEM, " +
	             "         PERF_EVENTI_PAGATI.IMPORTO_DEM = (PERF_EVENTI_PAGATI.IMPORTO_DEM*(PERF_NDM_VOCE_FATTURA.IMPORTO_ORIGINALE / TOTALE_IMPORTI.TOT_DEM)), " +
	             "         PERF_EVENTI_PAGATI.IMPORTO_AGGIO = (PERF_NDM_VOCE_FATTURA.PERCENTUALE_AGGIO*(PERF_EVENTI_PAGATI.IMPORTO_DEM*(PERF_NDM_VOCE_FATTURA.IMPORTO_ORIGINALE / TOTALE_IMPORTI.TOT_DEM))) / 100, " +
	             "         PERF_EVENTI_PAGATI.IMPORTO_EX_ART_18 = 0.05 *((PERF_EVENTI_PAGATI.IMPORTO_DEM*(PERF_NDM_VOCE_FATTURA.IMPORTO_ORIGINALE / TOTALE_IMPORTI.TOT_DEM)) -(PERF_NDM_VOCE_FATTURA.PERCENTUALE_AGGIO*(PERF_EVENTI_PAGATI.IMPORTO_DEM*(PERF_NDM_VOCE_FATTURA.IMPORTO_ORIGINALE / TOTALE_IMPORTI.TOT_DEM))) / 100)  " +
	             "WHERE NOT (PERF_NDM_VOCE_FATTURA.ID_NDM_VOCE_FATTURA IS NULL)  " +
	             "AND PERF_EVENTI_PAGATI.QUADRATURA_NDM != '1'  " +
	             "AND PERF_EVENTI_PAGATI.FORZATURA_NDM = 'N'  " +
	             "AND PERF_EVENTI_PAGATI.NUMERO_FATTURA = ?1 AND PERF_EVENTI_PAGATI.VOCE_INCASSO = ?2 " +
	             "AND PERF_NDM_VOCE_FATTURA.IMPORTO_ORIGINALE IS NOT NULL";
		
		Query riconciliazioneQuery = em.createNativeQuery(riconciliazioneSql).setParameter(1, numeroFattura).setParameter(2, voceIncasso);
		
		TracciamentoApplicativo tracciamentoApplicativo = tracciamentoTemplate(username, "riconciliazione-eventi",numeroFattura + "-" + voceIncasso);
		JsonObject messaggioTracciamento = new JsonObject();
		
		try {
			em.getTransaction().begin();

			List<Object[]> eventi = (List<Object[]>) eventiDaAggiornareQuery.getResultList();
			messaggioTracciamento.addProperty("tipoRiconciliazione", TIPO_RICONCILIAZIONE_IMPORTO);
			JsonArray traceInfoList = new JsonArray();
			for (int i = 0; i < eventi.size(); i++) {
				JsonObject traceInfo = new JsonObject();	
				traceInfo.addProperty("idEventoPagato", (Long) eventi.get(i)[0]);
				traceInfo.addProperty("importoDemOriginale", ((BigDecimal) eventi.get(i)[1]).toString());
				traceInfo.addProperty("importoDemAggiornato", ((BigDecimal) eventi.get(i)[2]).toString());
				traceInfoList.add(traceInfo);
			}
			messaggioTracciamento.add("modificationList", traceInfoList);

			int updatedRecords = riconciliazioneQuery.executeUpdate();
			messaggioTracciamento.addProperty("updatedRecords", updatedRecords);
			tracciamentoApplicativo.setMessage(new Gson().toJson(messaggioTracciamento));
			em.persist(tracciamentoApplicativo);
			em.flush();
			em.getTransaction().commit();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			throw ex;
		}
	}

	@Override
	public void riconciliaVociIncasso(RiconciliazioneImportiDTO riconciliazioneImporti, String username) {
		EntityManager em = provider.get();
		String numeroFattura = riconciliazioneImporti.getNumeroFattura();
		String voceIncasso = riconciliazioneImporti.getVoceIncasso();
		
		String eventiDaAggiornareSql = "SELECT PERF_EVENTI_PAGATI.ID_EVENTO_PAGATO, PERF_EVENTI_PAGATI.VOCE_INCASSO AS VOCE_INCASSO_SOPHIA, ndm.VOCE AS VOCE_INCASSO_NDM " +
	             "        FROM " +
	             "        PERF_EVENTI_PAGATI JOIN " +
	             "        (SELECT ep.NUMERO_FATTURA,  " +
	             "                     ep.VOCE_INCASSO,  " +
	             "                     SUM(ep.IMPORTO_DEM) AS IMPORTO_SOPHIA,  " +
	             "                      IF (SUM(ep.QUADRATURA_NDM) = COUNT(ep.QUADRATURA_NDM),'OK','KO') AS QUADRATURA, " +
	             "                      MAX(ep.FORZATURA_NDM) AS FORZATURA_NDM, " +
	             "                     COUNT(*) AS TOT_EVENTI  " +
	             "              FROM PERF_EVENTI_PAGATI ep   " +
	             "             WHERE ep.NUMERO_FATTURA = ?1  AND ep.QUADRATURA_NDM !='1'  " +
	             "              GROUP BY ep.NUMERO_FATTURA,  " +
	             "                       ep.VOCE_INCASSO) AS ds ON (PERF_EVENTI_PAGATI.NUMERO_FATTURA = ds.NUMERO_FATTURA AND PERF_EVENTI_PAGATI.VOCE_INCASSO=ds.VOCE_INCASSO) " +
	             "          LEFT JOIN (SELECT vf.NUMERO_FATTURA,  " +
	             "                            vf.voce,  " +
	             "                            SUM(vf.IMPORTO_ORIGINALE) AS IMPORTO_NDM,  " +
	             "                            COUNT(*) AS TOT_FATTURE  " +
	             "                     FROM PERF_NDM_VOCE_FATTURA vf  " +
	             "                     WHERE vf.NUMERO_FATTURA = ?1  " +
	             "                     GROUP BY vf.NUMERO_FATTURA,  " +
	             "                              vf.VOCE) AS ndm ON (ds.NUMERO_FATTURA = ndm.NUMERO_FATTURA)  " +
	             "        WHERE ds.IMPORTO_SOPHIA = ndm.IMPORTO_NDM  " +
	             "        AND   ds.TOT_EVENTI = ndm.TOT_FATTURE  " +
	             "        AND   NOT EXISTS (SELECT 1  " +
	             "                          FROM PERF_EVENTI_PAGATI epe  " +
	             "                            JOIN PERF_NDM_VOCE_FATTURA vfe  " +
	             "                              ON (epe.NUMERO_FATTURA = vfe.NUMERO_FATTURA  " +
	             "                             AND epe.VOCE_INCASSO = vfe.VOCE)  " +
	             "                          WHERE epe.QUADRATURA_NDM !='1' AND epe.VOCE_INCASSO = ds.VOCE_INCASSO AND epe.NUMERO_FATTURA = ds.NUMERO_FATTURA)";		
		
		Query eventiDaAggiornareQuery = em.createNativeQuery(eventiDaAggiornareSql).setParameter(1, numeroFattura);

		String riconciliazioneSql = "UPDATE PERF_EVENTI_PAGATI  " +
	             "  LEFT JOIN ( " +
	             "    SELECT NUMERO_FATTURA, VOCE_INCASSO, SUM(IMPORTO_DEM) AS TOT_DEM  " +
	             "    FROM PERF_EVENTI_PAGATI  " +
	             "     WHERE PERF_EVENTI_PAGATI.ID_EVENTO_PAGATO = ?1 " +
	             "     GROUP BY NUMERO_FATTURA, VOCE_INCASSO  " +
	             "     ) AS TOTALE_IMPORTI ON (  " +
	             "	    PERF_EVENTI_PAGATI.NUMERO_FATTURA = TOTALE_IMPORTI.NUMERO_FATTURA  " +
	             "	    AND PERF_EVENTI_PAGATI.VOCE_INCASSO = TOTALE_IMPORTI.VOCE_INCASSO  " +
	             "	    ) LEFT JOIN PERF_NDM_VOCE_FATTURA ON (  " +
	             "	    PERF_NDM_VOCE_FATTURA.NUMERO_FATTURA = PERF_EVENTI_PAGATI.NUMERO_FATTURA   " +
	             "	    AND PERF_NDM_VOCE_FATTURA.VOCE = ?2 " +
	             "	    AND PERF_NDM_VOCE_FATTURA.IMPORTO_ORIGINALE = TOTALE_IMPORTI.TOT_DEM  " +
	             "	    )  " +
	             "	   SET  " +
	             "	   PERF_EVENTI_PAGATI.QUADRATURA_NDM = '1', " +
	             "	   PERF_EVENTI_PAGATI.FORZATURA_NDM = 'M', " +
	             "	   PERF_EVENTI_PAGATI.VOCE_INCASSO = PERF_NDM_VOCE_FATTURA.VOCE, " +
	             "	   PERF_EVENTI_PAGATI.IMPORTO_AGGIO = (PERF_NDM_VOCE_FATTURA.PERCENTUALE_AGGIO * PERF_EVENTI_PAGATI.IMPORTO_DEM) / 100 , " +
	             "	   PERF_EVENTI_PAGATI.IMPORTO_EX_ART_18 = 0.05 * (PERF_EVENTI_PAGATI.IMPORTO_DEM - (PERF_NDM_VOCE_FATTURA.PERCENTUALE_AGGIO * PERF_EVENTI_PAGATI.IMPORTO_DEM) / 100)  " +
	             "	   WHERE NOT (PERF_NDM_VOCE_FATTURA.ID_NDM_VOCE_FATTURA IS NULL)  " +
	             "	   AND PERF_EVENTI_PAGATI.ID_EVENTO_PAGATO = ?1";		
				
		TracciamentoApplicativo tracciamentoApplicativo = tracciamentoTemplate(username, "riconciliazione-eventi",numeroFattura + "-" + voceIncasso);
		JsonObject messaggioTracciamento = new JsonObject();
		
		try {
			em.getTransaction().begin();
			List<Object[]> eventi = (List<Object[]>) eventiDaAggiornareQuery.getResultList();
			messaggioTracciamento.addProperty("tipoRiconciliazione", TIPO_RICONCILIAZIONE_VOCE_INCASSO);
			JsonArray traceInfoList = new JsonArray();
			int updatedRecords = 0;
			for (int i = 0; i < eventi.size(); i++) {
				JsonObject traceInfo = new JsonObject();
				Long idEventoPagato = (Long) eventi.get(i)[0];
				String voceIncassoSophia = (String) eventi.get(i)[1];
				String voceIncassoNdm = (String) eventi.get(i)[2];
				Query riconciliazioneQuery = em.createNativeQuery(riconciliazioneSql).setParameter(1, idEventoPagato)
						.setParameter(2, voceIncassoNdm);
				updatedRecords += riconciliazioneQuery.executeUpdate();
				traceInfo.addProperty("idEventoPagato", idEventoPagato);
				traceInfo.addProperty("voceIncassoOriginale", voceIncassoSophia);
				traceInfo.addProperty("voceIncassoAggiornata", voceIncassoNdm);
				traceInfoList.add(traceInfo);
			}
			messaggioTracciamento.add("modificationList", traceInfoList);
			messaggioTracciamento.addProperty("updatedRecords", updatedRecords);
			tracciamentoApplicativo.setMessage(new Gson().toJson(messaggioTracciamento));
			em.persist(tracciamentoApplicativo);
			em.getTransaction().commit();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			throw ex;
		}
	}

	@Override
	public void ripristinaRiconciliazione(RiconciliazioneImportiDTO riconciliazioneImporti, String username) {
		EntityManager em = provider.get();
		String numeroFattura = riconciliazioneImporti.getNumeroFattura();
		String voceIncasso = riconciliazioneImporti.getVoceIncasso();
		
		String recoverInfoSql = "SELECT MESSAGE " +
	             "FROM PERF_TRACCIAMENTO_LOG_APPLICATIVO " +
	             "WHERE CLASS_NAME = 'com.alkemytech.sophia.codman.entity.performing.dao.ProgrammaMusicaleDAO' " +
	             "AND   SERVICE_NAME = 'riconciliazione-eventi' " +
	             "AND   METHOD_NAME = ?1 " +
	             "ORDER BY TIME_STAMP DESC LIMIT 1";
		Query recoverInfoSqlQuery = em.createNativeQuery(recoverInfoSql).setParameter(1, numeroFattura+"-"+voceIncasso);
		
		String recoverByImportoSql = "UPDATE PERF_EVENTI_PAGATI " +
	             "   SET PERF_EVENTI_PAGATI.QUADRATURA_NDM = '0', " +
	             "       PERF_EVENTI_PAGATI.FORZATURA_NDM = 'N', " +
	             "       PERF_EVENTI_PAGATI.IMPORTO_DEM = ?2, " +
	             "       PERF_EVENTI_PAGATI.IMPORTO_DEM_NON_FORZATO = NULL, " +
	             "       PERF_EVENTI_PAGATI.IMPORTO_AGGIO = NULL, " +
	             "       PERF_EVENTI_PAGATI.IMPORTO_EX_ART_18 = NULL " +
	             "WHERE PERF_EVENTI_PAGATI.ID_EVENTO_PAGATO = ?1";
		
		String recoverByVoceIncassoSql = "UPDATE PERF_EVENTI_PAGATI " +
	             "   SET PERF_EVENTI_PAGATI.QUADRATURA_NDM = '0', " +
	             "       PERF_EVENTI_PAGATI.FORZATURA_NDM = 'N', " +
	             "       PERF_EVENTI_PAGATI.VOCE_INCASSO = ?2, " +
	             "       PERF_EVENTI_PAGATI.IMPORTO_AGGIO = NULL, " +
	             "       PERF_EVENTI_PAGATI.IMPORTO_EX_ART_18 = NULL " +
	             "WHERE PERF_EVENTI_PAGATI.ID_EVENTO_PAGATO = ?1";	
		
		
		
		TracciamentoApplicativo tracciamentoApplicativo = tracciamentoTemplate(username, "riconciliazione-eventi-ripristino",numeroFattura + "-" + voceIncasso);
		JsonObject messaggioTracciamento = new JsonObject();
		
		try {
			em.getTransaction().begin();
			String message = (String) recoverInfoSqlQuery.getSingleResult();
			JsonArray traceInfoList = new JsonArray();
			Gson gson = new Gson();
			JsonObject jsonMessage = gson.fromJson(message, JsonObject.class);
			String tipoRiconciliazione = jsonMessage.get("tipoRiconciliazione").getAsString();
			if(!Arrays.asList(TIPO_RICONCILIAZIONE_IMPORTO,TIPO_RICONCILIAZIONE_VOCE_INCASSO).contains(tipoRiconciliazione)) {
				throw new UnsupportedOperationException(tipoRiconciliazione);
			}
			messaggioTracciamento.addProperty("tipoRipristinoRiconciliazione", tipoRiconciliazione);
			JsonArray modifications = jsonMessage.getAsJsonArray("modificationList");
			int updatedRecords = 0;
			for (JsonElement modification : modifications) {
				JsonObject traceInfo = new JsonObject();
				Long idEventoPagato = modification.getAsJsonObject().get("idEventoPagato").getAsLong();
				traceInfo.addProperty("idEventoPagato", idEventoPagato);
				if(TIPO_RICONCILIAZIONE_IMPORTO.equals(tipoRiconciliazione)) {
					BigDecimal importoDemOriginale = new BigDecimal(modification.getAsJsonObject().get("importoDemOriginale").getAsString());
					Query recoverByImportoQuery = em.createNativeQuery(recoverByImportoSql).setParameter(1, idEventoPagato)
							.setParameter(2, importoDemOriginale);
					updatedRecords += recoverByImportoQuery.executeUpdate();
					traceInfo.addProperty("importoDemOriginale", modification.getAsJsonObject().get("importoDemAggiornato").getAsString());
					traceInfo.addProperty("importoDemAggiornato", modification.getAsJsonObject().get("importoDemOriginale").getAsString());
				}else {
					String voceIncassoOriginale = modification.getAsJsonObject().get("voceIncassoOriginale").getAsString();
					String voceIncassoAggiornata = modification.getAsJsonObject().get("voceIncassoAggiornata").getAsString();
					Query recoverByVoceIncassoQuery = em.createNativeQuery(recoverByVoceIncassoSql).setParameter(1, idEventoPagato)
							.setParameter(2, voceIncassoOriginale);
					updatedRecords += recoverByVoceIncassoQuery.executeUpdate();
					traceInfo.addProperty("voceIncassoOriginale", voceIncassoAggiornata);
					traceInfo.addProperty("voceIncassoAggiornata", voceIncassoOriginale);
				}
				traceInfoList.add(traceInfo);	
			}
			messaggioTracciamento.add("modificationList", traceInfoList);
			messaggioTracciamento.addProperty("updatedRecords", updatedRecords);
			tracciamentoApplicativo.setMessage(gson.toJson(messaggioTracciamento));
			em.persist(tracciamentoApplicativo);
			em.flush();
			em.getTransaction().commit();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			throw ex;
		}
	}
	
	protected TracciamentoApplicativo tracciamentoTemplate(String username, String serviceName, String methodName) {
		TracciamentoApplicativo tracciamentoApplicativo = new TracciamentoApplicativo();
		tracciamentoApplicativo.setApplication("cruscottoUtilizzazioni");

		tracciamentoApplicativo.setDataInserimento(new Date());
		tracciamentoApplicativo.setUserName(username);				
		try {
			tracciamentoApplicativo.setHostName(InetAddress.getLocalHost().getHostName());
		} catch (UnknownHostException e) {
			throw new RuntimeException(e);
		}
		tracciamentoApplicativo.setSessionId(null);

		tracciamentoApplicativo.setLogLevel("INFO");
		tracciamentoApplicativo.setClassName(this.getClass().getName());
		tracciamentoApplicativo.setServiceName(serviceName);
		tracciamentoApplicativo.setMethodName(methodName);

		return tracciamentoApplicativo;
	}	

    /*private List<RiconciliazioneImportiDTO> getRiconciliazioneImportiListTestQuery(String voceIncasso,
                                                        String agenzia,
                                                        String sede,
                                                        String seprag,
                                                        String numeroFattura,
                                                        String inizioPeriodoContabile,
                                                        String finePeriodoContabile){
        DSLContext dsl = jooq;
        SelectQuery<Record3<Object, Object, Object>> selectQuery = dsl.selectDistinct(field("PERF_EVENTI_PAGATI.CONTABILITA"),
                field("PERF_EVENTI_PAGATI.VOCE_INCASSO"),
                field("PERF_EVENTI_PAGATI.NUMERO_FATTURA"))
        .from("PERF_EVENTI_PAGATI").getQuery();

        addConditions(voceIncasso, agenzia, sede, seprag, numeroFattura, inizioPeriodoContabile, finePeriodoContabile, selectQuery);

        return selectQuery.fetchInto(RiconciliazioneImportiDTO.class);

//        select DISTINCT PERF_EVENTI_PAGATI.CONTABILITA as `contabilita`,
//                PERF_EVENTI_PAGATI.VOCE_INCASSO as `voceIncasso`,
//                PERF_EVENTI_PAGATI.NUMERO_FATTURA as `numeroFattura`
//        from
//                PERF_EVENTI_PAGATI
//        where
//                (PERF_EVENTI_PAGATI.CONTABILITA >= '201801'
//                        and PERF_EVENTI_PAGATI.CONTABILITA <= '201802')
    }*/
}
