package com.alkemytech.sophia.codman.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity(name="SqsDeduplication")
@Table(name="SQS_DEDUPLICATION")
public class SqsDeduplication {
	
	@Id
	@Column(name="UUID", nullable=false)
	private String uuid;
	
	@Column(name="QUEUE_NAME", nullable=false)
	private String queueName;
	
	@Column(name="RECEIVE_TIME", nullable=false)
	private Date receiveTime;
	
	@Column(name="MESSAGE_JSON", nullable=true)
	private String messageJson;

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getQueueName() {
		return queueName;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

	public Date getReceiveTime() {
		return receiveTime;
	}

	public void setReceiveTime(Date receiveTime) {
		this.receiveTime = receiveTime;
	}

	public String getMessageJson() {
		return messageJson;
	}

	public void setMessageJson(String messageJson) {
		this.messageJson = messageJson;
	}
	
}
