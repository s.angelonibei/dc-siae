package com.alkemytech.sophia.codman.utils;

import org.jooq.SelectQuery;

import java.util.List;
import java.util.concurrent.RecursiveTask;

public class LongQueryRecursiveTask extends RecursiveTask<List<?>> {
    private SelectQuery<?> resultQuery;
    private Class fetchIntoType;
    private List<?> list;

    private static final int THRESHOLD = 1;

    public LongQueryRecursiveTask(List<?> list, SelectQuery resultQuery, Class fetchIntoType) {
        this.resultQuery = resultQuery;
        this.fetchIntoType = fetchIntoType;
        this.list = list;
    }

    @Override
    protected List<?> compute() {
        if (this.list.size() > THRESHOLD) {
            return createSubtasks();
        } else {
            return processing(list);
        }
    }

    private List<?> createSubtasks() {

        LongQueryRecursiveTask left = new LongQueryRecursiveTask(
                list.subList(0,list.size() / 2),this.resultQuery, fetchIntoType);
        LongQueryRecursiveTask right = new LongQueryRecursiveTask(
                list.subList(list.size() / 2, list.size()),this.resultQuery, fetchIntoType);
        left.fork();
        List result = right.compute();
        result.addAll(left.join());
        return result;
    }

    private List<?> processing(List<?> list) {
        return this.resultQuery.fetchInto(fetchIntoType);
    }
}
