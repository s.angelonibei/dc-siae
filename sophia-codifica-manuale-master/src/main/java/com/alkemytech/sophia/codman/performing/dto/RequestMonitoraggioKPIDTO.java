package com.alkemytech.sophia.codman.performing.dto;

import com.alkemytech.sophia.broadcasting.dto.RequestDTO;
import com.alkemytech.sophia.performing.model.PerfMusicProvider;
import com.alkemytech.sophia.performing.model.PerfPalinsesto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class RequestMonitoraggioKPIDTO extends RequestDTO implements Serializable {
    private List<PerfPalinsesto> palinsesti;
    private List<Short> mesi;
    private Short anno;
    private PerfMusicProvider musicProvider;

    public List<PerfPalinsesto> getPalinsesti() {
        return palinsesti;
    }

    public void setPalinsesti(List<PerfPalinsesto> palinsesti) {
        this.palinsesti = palinsesti;
    }

    public List<Short> getMesi() {
        return mesi;
    }

    public void setMesi(List<Short> mesi) {
        this.mesi = mesi;
    }

    public Short getAnno() {
        return anno;
    }

    public void setAnno(Short anno) {
        this.anno = anno;
    }

    public PerfMusicProvider getMusicProvider() {
        return musicProvider;
    }

    public void setMusicProvider(PerfMusicProvider musicProvider) {
        this.musicProvider = musicProvider;
    }

    public RequestMonitoraggioKPIDTO(List<PerfPalinsesto> palinsesti, List<Short> mesi, Short anno, PerfMusicProvider musicProvider) {
        this.palinsesti = palinsesti;
        this.mesi = mesi;
        this.anno = anno;
        this.musicProvider = musicProvider;
    }

    public RequestMonitoraggioKPIDTO() {
    }
}
