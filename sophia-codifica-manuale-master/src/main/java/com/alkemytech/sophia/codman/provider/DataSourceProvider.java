package com.alkemytech.sophia.codman.provider;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Provider;
import javax.sql.DataSource;

public class DataSourceProvider implements Provider<DataSource> {

    @Inject
    @Named("javax.persistence.jdbc.url")
    private String url;

    @Inject
    @Named("javax.persistence.jdbc.driver")
    private String driver;

    @Inject
    @Named("javax.persistence.jdbc.user")
    private String user;

    @Inject
    @Named("javax.persistence.jdbc.password")
    private String password;

    @Override
    public DataSource get() {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(url);
        config.setDriverClassName(driver);
        config.setUsername(user);
        config.setPassword(password);
        config.addDataSourceProperty("cachePrepStmts", "true");
        config.addDataSourceProperty("prepStmtCacheSize", "250");
        config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
        return new HikariDataSource(config);
    }
}