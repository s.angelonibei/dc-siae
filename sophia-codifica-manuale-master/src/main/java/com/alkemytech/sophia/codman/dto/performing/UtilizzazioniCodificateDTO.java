package com.alkemytech.sophia.codman.dto.performing;

import java.util.Date;

public class UtilizzazioniCodificateDTO {
	private Date dataEventoDa;
	private Date dataEventoA;
	private Date dateFattaruaDa;
	private Date dateFatturaA;
	private String voceIncasso;
	private String Fonte;
	private String titolo;
	private String nominativo;

	public UtilizzazioniCodificateDTO() {
		super();
	}

	public UtilizzazioniCodificateDTO(Date dataEventoDa, Date dataEventoA, Date dateFattaruaDa, Date dateFatturaA,
			String voceIncasso, String fonte, String titolo, String nominativo) {
		super();
		this.dataEventoDa = dataEventoDa;
		this.dataEventoA = dataEventoA;
		this.dateFattaruaDa = dateFattaruaDa;
		this.dateFatturaA = dateFatturaA;
		this.voceIncasso = voceIncasso;
		Fonte = fonte;
		this.titolo = titolo;
		this.nominativo = nominativo;
	}

	public Date getDataEventoDa() {
		return dataEventoDa;
	}

	public void setDataEventoDa(Date dataEventoDa) {
		this.dataEventoDa = dataEventoDa;
	}

	public Date getDataEventoA() {
		return dataEventoA;
	}

	public void setDataEventoA(Date dataEventoA) {
		this.dataEventoA = dataEventoA;
	}

	public Date getDateFattaruaDa() {
		return dateFattaruaDa;
	}

	public void setDateFattaruaDa(Date dateFattaruaDa) {
		this.dateFattaruaDa = dateFattaruaDa;
	}

	public Date getDateFatturaA() {
		return dateFatturaA;
	}

	public void setDateFatturaA(Date dateFatturaA) {
		this.dateFatturaA = dateFatturaA;
	}

	public String getVoceIncasso() {
		return voceIncasso;
	}

	public void setVoceIncasso(String voceIncasso) {
		this.voceIncasso = voceIncasso;
	}

	public String getFonte() {
		return Fonte;
	}

	public void setFonte(String fonte) {
		Fonte = fonte;
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	public String getNominativo() {
		return nominativo;
	}

	public void setNominativo(String nominativo) {
		this.nominativo = nominativo;
	}

}
