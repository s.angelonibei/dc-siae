package com.alkemytech.sophia.codman.entity.performing.dao;

import com.alkemytech.sophia.codman.entity.performing.CampionamentoConfig;
import com.alkemytech.sophia.codman.entity.performing.ProgrammaMusicale;
import com.alkemytech.sophia.codman.entity.performing.TracciamentoApplicativo;
import com.alkemytech.sophia.codman.entity.performing.security.ReportPage;

import java.util.List;

/**
 * Created by idilello on 6/21/16.
 */
public interface ICampionamentoDAO {

    public CampionamentoConfig getCampionamentoConfig(Long contabilita);

    public ReportPage getCampionamenti(Long contabilitaIniziale, Long contabilitaFinale, Integer page);
    
    public ReportPage getCampionamentiEsecuzione(Long contabilitaIniziale, Long contabilitaFinale, Integer page);

    public CampionamentoConfig getCampionamentoConfigById(Long idCampionamentoConfig);

    public ReportPage getFlussoGuida(Long contabilitaIniziale, Long contabilitaFinale, Integer page);

    public ReportPage getFlussoRientrati(Long contabilitaIniziale, Long contabilitaFinale, Integer page);

    public ReportPage getStoricoEsecuzioni(Long periodoIniziale, Long periodoFinale, Integer page);

    public void insertParametriCampionamento(CampionamentoConfig campionamentoConfig);

    public List<TracciamentoApplicativo> getEsecuzioniCampionamentoEngine(Long periodoIniziale, Long periodoFinale);

    public List<TracciamentoApplicativo> getEsecuzioniCampionamentoStart(Long periodoIniziale, Long periodoFinale);

	public List<ProgrammaMusicale>  getTotFlussoGuida(Long contabilitaIniziale, Long contabilitaFinale);

}
