package com.alkemytech.sophia.codman.multimedialelocale.dto;

import com.alkemytech.sophia.codman.multimedialelocale.entity.MultimedialeLocale;
import com.google.gson.Gson;

public class MlUtil {
	public static Double getPercPolverizzazione(MultimedialeLocale multimedialeLocale) {
		// TODO Auto-generated method stub
		Gson gson =  new Gson();
		CodificaStatistiche codificaStatistiche= gson.fromJson(multimedialeLocale.getCodificaStatistiche(), CodificaStatistiche.class);
		Double utilizazionePolverizzate =(double) multimedialeLocale.getCodificaUtilizzazioniCodificate() - ( codificaStatistiche.getUtilizzazioniCodificateRegolari() +codificaStatistiche.getUtilizzazioniCodificateIrregolari());
		return utilizazionePolverizzate / multimedialeLocale.getValidazioneUtilizzazioniValide() * 100.0;
	}

}
