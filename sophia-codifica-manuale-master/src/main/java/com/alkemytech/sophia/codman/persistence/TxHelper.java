package com.alkemytech.sophia.codman.persistence;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

public class TxHelper {

private class PersistOperation implements Runnable {
		
		private Object entity;
		
		public PersistOperation(Object entity) {
			super();
			this.entity = entity;
		}
		
		@Override
		public void run() {
			entityManager.persist(entity);
		}
		
	}
	
	private class MergeOperation implements Runnable {
		
		private Object entity;
		
		public MergeOperation(Object entity) {
			super();
			this.entity = entity;
		}
		
		@Override
		public void run() {
			entityManager.merge(entity);
		}
		
	}

	private class RemoveOperation implements Runnable {
		
		private Object entity;
		
		public RemoveOperation(Object entity) {
			super();
			this.entity = entity;
		}
		
		@Override
		public void run() {
			if (!entityManager.contains(entity)) {
				entity = entityManager.merge(entity);
			}
			entityManager.remove(entity);
		}
		
	}

	private EntityManager entityManager;
	private List<Runnable> operations;

	public TxHelper(EntityManager entityManager) {
		super();
		this.entityManager = entityManager;
		operations = new LinkedList<Runnable>();
	}
	
	public TxHelper persist(Object entity) {
		operations.add(new PersistOperation(entity));
		return this;
	}

	public TxHelper merge(Object entity) {
		operations.add(new MergeOperation(entity));
		return this;
	}

	public TxHelper remove(Object entity) {
		operations.add(new RemoveOperation(entity));
		return this;
	}

	public void execute() {
		try {
			entityManager.getTransaction().begin();
			for (Runnable operation : operations) {
				operation.run();
			}
			entityManager.flush();
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			try {
				entityManager.getTransaction().rollback();
			} catch (Exception ignore) {
				// swallow
			}
			throw e;
		}
	}
	
	public int executeUpdate(Query query) {
		try {
			entityManager.getTransaction().begin();
			final int count = query.executeUpdate();
			entityManager.getTransaction().commit();
			return count;
		} catch (Exception e) {
			try {
				entityManager.getTransaction().rollback();
			} catch (Exception ignore) {
				// swallow
			}
			throw e;
		}
	}
	
}
