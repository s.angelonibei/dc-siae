package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;
import java.util.List;

import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;

@Produces("application/json")
@XmlRootElement 
public class CCIDDTO implements Serializable {

	private static final long serialVersionUID = 7790680520887674977L;

	public CCIDDTO(){
		
	}
	
	private Integer idCCIDConfig;
	private String idDsp;
	private String idUtilizationType;	
	private String idCommercialOffers;	
	private String idCountry;
	private Boolean encoded;	
	private Boolean encodedProvisional;	
	private Boolean notEncoded;	
	private Integer idCCIDVersion;	
	private Integer rank;
	private String currency;
	private Boolean excludeClaimZero;
	private List<AnagCcidConfigDTO> ccidAnagParams;
	
	public Integer getIdCCIDConfig() {
		return idCCIDConfig;
	}
	public void setIdCCIDConfig(Integer idCCIDConfig) {
		this.idCCIDConfig = idCCIDConfig;
	}
	public String getIdDsp() {
		return idDsp;
	}
	public void setIdDsp(String idDsp) {
		this.idDsp = idDsp;
	}
	public String getIdUtilizationType() {
		return idUtilizationType;
	}
	public void setIdUtilizationType(String idUtilizationType) {
		this.idUtilizationType = idUtilizationType;
	}
	public String getIdCommercialOffers() {
		return idCommercialOffers;
	}
	public void setIdCommercialOffers(String idCommercialOffers) {
		this.idCommercialOffers = idCommercialOffers;
	}
	public String getIdCountry() {
		return idCountry;
	}
	public void setIdCountry(String idCountry) {
		this.idCountry = idCountry;
	}
	public Boolean getEncoded() {
		return encoded;
	}
	public void setEncoded(Boolean encoded) {
		this.encoded = encoded;
	}
	public Boolean getEncodedProvisional() {
		return encodedProvisional;
	}
	public void setEncodedProvisional(Boolean encodedProvisional) {
		this.encodedProvisional = encodedProvisional;
	}
	public Boolean getNotEncoded() {
		return notEncoded;
	}
	public void setNotEncoded(Boolean notEncoded) {
		this.notEncoded = notEncoded;
	}
	public Integer getIdCCIDVersion() {
		return idCCIDVersion;
	}
	public void setIdCCIDVersion(Integer idCCIDVersion) {
		this.idCCIDVersion = idCCIDVersion;
	}
	public Integer getRank() {
		return rank;
	}
	public void setRank(Integer rank) {
		this.rank = rank;
	}
	
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public List<AnagCcidConfigDTO> getCcidAnagParams() {
		return ccidAnagParams;
	}
	public void setCcidAnagParams(List<AnagCcidConfigDTO> ccidAnagParams) {
		this.ccidAnagParams = ccidAnagParams;
	}

	public Boolean getExcludeClaimZero() {
		return excludeClaimZero;
	}

	public void setExcludeClaimZero(Boolean excludeClaimZero) {
		this.excludeClaimZero = excludeClaimZero;
	}
}
