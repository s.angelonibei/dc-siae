package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.codman.utils.SearchOpereField;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class RicercaOpereDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@SearchOpereField("title")
	private String titolo;
	@SearchOpereField("title_alternative")
	private String titoloAlternativo;
	@SearchOpereField("artist_composer")
	private String artisti;
	@SearchOpereField("artist_composer")
	private String compositori;
	@SearchOpereField("artist_performer")
	private String interpreti;
	@SearchOpereField("artist_author")
	private String autori;
	
	public String getTitolo() {
		return titolo;
	}
	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}
	public String getTitoloAlternativo() {
		return titoloAlternativo;
	}
	public void setTitoloAlternativo(String titoloAlternativo) {
		this.titoloAlternativo = titoloAlternativo;
	}
	public String getArtisti() {
		return artisti;
	}
	public void setArtisti(String artisti) {
		this.artisti = artisti;
	}
	public String getCompositori() {
		return compositori;
	}
	public void setCompositori(String compositori) {
		this.compositori = compositori;
	}
	public String getInterpreti() {
		return interpreti;
	}
	public void setInterpreti(String interpreti) {
		this.interpreti = interpreti;
	}
	public String getAutori() {
		return autori;
	}
	public void setAutori(String autori) {
		this.autori = autori;
	}

}