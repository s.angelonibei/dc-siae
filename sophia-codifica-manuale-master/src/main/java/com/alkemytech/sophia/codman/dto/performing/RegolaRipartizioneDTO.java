package com.alkemytech.sophia.codman.dto.performing;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.codman.entity.performing.PerfValorizzazione;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class RegolaRipartizioneDTO {

	private String voceIncasso;
	
	/*
	 * digitale, cartaceo o non report
	 */
	private String tipologiaReport;
	
	/*
	 * Ordinaria o Straordinaria
	 */
	private String ripartizioneOS;
	
	/*
	 * Contiene le esclusioni
	 */
	private String valorizzazione;
	
	/*
	 * percentuale di analitico, proporzionale e campionario (incluso campionarioProgrammi e campionarioRilevazioni)
	 */
	private String ripartizione;
	
	private String user;
	
	private Date dataUltimaModifica;
	
	private Date competenzaDa;

	private Date competenzaA;
	
	
	public RegolaRipartizioneDTO() {
		super();
	}


	public RegolaRipartizioneDTO(String voceIncasso, String tipologiaReport, String ripartizioneOS, 
			String valorizzazione, String modalitàRipartizione, String user, Date dataUltimaModifica) {
		super();
		this.voceIncasso = voceIncasso;
		this.tipologiaReport = tipologiaReport;
		this.ripartizioneOS = ripartizioneOS;
		this.valorizzazione = valorizzazione;
		this.user = user;
		this.dataUltimaModifica = dataUltimaModifica;
	}


	public RegolaRipartizioneDTO(PerfValorizzazione perfValorizzazione) {
		this.voceIncasso = perfValorizzazione.getVoceIncasso();
		this.tipologiaReport = perfValorizzazione.getTipologiaReport();
		this.ripartizioneOS = perfValorizzazione.getRipartizioneOS();
		this.valorizzazione = perfValorizzazione.getValorizzazione();
		this.user = perfValorizzazione.getUser();
		this.dataUltimaModifica = perfValorizzazione.getDataUltimaModifica();
		this.ripartizione = perfValorizzazione.getRipartizione();
		this.competenzaDa = perfValorizzazione.getPeriodoRipartizione().getCompetenzeDa();
		this.competenzaA = perfValorizzazione.getPeriodoRipartizione().getCompetenzeA();
	}

	public String getVoceIncasso() {
		return voceIncasso;
	}

	public void setVoceIncasso(String voceIncasso) {
		this.voceIncasso = voceIncasso;
	}

	public String getTipologiaReport() {
		return tipologiaReport;
	}

	public void setTipologiaReport(String tipologiaReport) {
		this.tipologiaReport = tipologiaReport;
	}

	public String getRipartizioneOS() {
		return ripartizioneOS;
	}

	public void setRipartizioneOS(String ripartizioneOS) {
		this.ripartizioneOS = ripartizioneOS;
	}

	public String getValorizzazione() {
		return valorizzazione;
	}


	public void setValorizzazione(String valorizzazione) {
		this.valorizzazione = valorizzazione;
	}
	
	public String getRipartizione() {
		return ripartizione;
	}

	public void setRipartizione(String ripartizione) {
		this.ripartizione = ripartizione;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public Date getDataUltimaModifica() {
		return dataUltimaModifica;
	}

	public void setDataUltimaModifica(Date dataUltimaModifica) {
		this.dataUltimaModifica = dataUltimaModifica;
	}


	public Date getCompetenzaDa() {
		return competenzaDa;
	}


	public void setCompetenzaDa(Date competenzaDa) {
		this.competenzaDa = competenzaDa;
	}


	public Date getCompetenzaA() {
		return competenzaA;
	}


	public void setCompetenzaA(Date competenzaA) {
		this.competenzaA = competenzaA;
	}
	
}
