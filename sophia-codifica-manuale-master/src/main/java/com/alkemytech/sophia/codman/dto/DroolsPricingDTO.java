package com.alkemytech.sophia.codman.dto;

import java.util.Date;

public class DroolsPricingDTO {

    public DroolsPricingDTO() {
    }

    public DroolsPricingDTO(Long id, String idDsp, String idUtilizationType, Date validFrom, Date validTo, String xlsS3Url) {
        this.id = id;
        this.idDsp = idDsp;
        this.idUtilizationType = idUtilizationType;
        this.validFrom = validFrom;
        this.validTo = validTo;
        this.xlsS3Url = xlsS3Url;
    }

    private Long id;

    private String idDsp;

    private String idUtilizationType;

    private Date validFrom;

    private Date validTo;

    private String xlsS3Url;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdDsp() {
        return idDsp;
    }

    public void setIdDsp(String idDsp) {
        this.idDsp = idDsp;
    }

    public String getIdUtilizationType() {
        return idUtilizationType;
    }

    public void setIdUtilizationType(String idUtilizationType) {
        this.idUtilizationType = idUtilizationType;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    public String getXlsS3Url() {
        return xlsS3Url;
    }

    public void setXlsS3Url(String xlsS3Url) {
        this.xlsS3Url = xlsS3Url;
    }

}
