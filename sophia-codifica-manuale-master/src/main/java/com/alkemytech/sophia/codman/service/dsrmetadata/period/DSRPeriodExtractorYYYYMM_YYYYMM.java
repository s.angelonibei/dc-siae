package com.alkemytech.sophia.codman.service.dsrmetadata.period;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DSRPeriodExtractorYYYYMM_YYYYMM implements DsrPeriodExtractor{
	
	private String regex = "[0-9]{6}_[0-9]{6}";
	private String code = "YYYYMM_YYYYMM";
	private String description = "YYYYMM_YYYYMM" ;
	
	@Override
	public DsrPeriod extractDsrPeriod(String periodString) {
				
		try {
			String startDate = periodString.substring(0,6);
			String endDate = periodString.substring(7,13);
			
			String startYear = startDate.substring(0,4);
			String startMonth = startDate.substring(4,6);
			
			String endYear = endDate.substring(0,4);
			String endMonth = endDate.substring(4,6);
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
			
			Date periodStartDate = dateFormat.parse(startDate + "01");
			
			Calendar c = Calendar.getInstance();
			c.setTime(dateFormat.parse(endDate + "01"));
			c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
			
			Date periodEndDate = c.getTime();

			
			
			
			DsrPeriod dsrPeriod = new DsrPeriod();
			dsrPeriod.setYear(startYear);
			if(startYear.equals(endYear) && startMonth.equals(endMonth)){
				dsrPeriod.setPeriodType(DsrPeriod.MENSILE);
				dsrPeriod.setPeriod(startMonth);
			}else{
				dsrPeriod.setPeriodType(DsrPeriod.TRIMESTRALE);
				if("01".equals(startMonth) || "02".equals(startMonth) || "03".equals(startMonth)){
					dsrPeriod.setPeriod("1");
				}else if("04".equals(startMonth) || "05".equals(startMonth) || "06".equals(startMonth)){
					dsrPeriod.setPeriod("2");
				}else if("07".equals(startMonth) || "08".equals(startMonth) || "09".equals(startMonth)){
					dsrPeriod.setPeriod("3");
				}else if("10".equals(startMonth) || "11".equals(startMonth) || "12".equals(startMonth)){
					dsrPeriod.setPeriod("4");
				}
			}
			
			dsrPeriod.setPeriodStartDate(periodStartDate);
			dsrPeriod.setPeriodEndDate(periodEndDate);
			
			return dsrPeriod;
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}

	@Override
	public String getRegex() {
		return regex;
	}

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public String getDescription() {
		return description;
	}

}
