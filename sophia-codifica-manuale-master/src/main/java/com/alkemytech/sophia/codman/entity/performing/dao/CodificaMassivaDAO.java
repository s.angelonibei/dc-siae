package com.alkemytech.sophia.codman.entity.performing.dao;

import static com.alkemytech.sophia.codman.entity.performing.security.ReportPage.NUMBER_RECORD_PER_PAGE;
import static com.alkemytech.sophia.codman.utils.QueryUtils.whiteListOrderBy;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.alkemytech.sophia.codman.entity.performing.PerfCodificaMassiva;
import com.alkemytech.sophia.codman.entity.performing.security.ReportPage;
import com.google.inject.Inject;
import com.google.inject.Provider;

@Repository
public class CodificaMassivaDAO implements ICodificaMassivaDAO {


    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    private Provider<EntityManager> provider;

    @Inject
    public CodificaMassivaDAO(Provider<EntityManager> provider) {
        this.provider = provider;
    }

    @Override
    public PerfCodificaMassiva getPerfCodificaMassiva(Integer uuid){
        String query = "SELECT pcm FROM PerfCodificaMassiva pcm where pcm.uuid = :uuid ";

        EntityManager entityManager = provider.get();
        TypedQuery<PerfCodificaMassiva> perfCodificaMassiva = entityManager.createQuery(
                query
                , PerfCodificaMassiva.class)
                .setParameter("uuid", uuid)
                .setMaxResults(1);

        return perfCodificaMassiva.getSingleResult();
    }

    @Override
    public PerfCodificaMassiva save(PerfCodificaMassiva perfCodificaMassiva) {
        EntityManager em = provider.get();
        em.getTransaction().begin();
        try {
            em.persist(perfCodificaMassiva);
            em.flush();
            em.getTransaction().commit();
        } catch (Exception e) {
            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }
            throw e;
        }
        return perfCodificaMassiva;
    }

	@Override
	public Integer getUploadedFilesCount() {
		String query = "SELECT count(pcm) FROM PerfCodificaMassiva pcm ";
		EntityManager entityManager = provider.get();
        TypedQuery<Long> perfCodificaMassiva = entityManager.createQuery(
                query
                , Long.class);
        return perfCodificaMassiva.getSingleResult().intValue();
	}

	@Override
	public List<PerfCodificaMassiva> getUploadedFiles(String order, Integer page) {
		String orderBy = whiteListOrderBy(order,PerfCodificaMassiva.class,"pcm");
		String query = "SELECT pcm FROM PerfCodificaMassiva pcm" + (StringUtils.isNotEmpty(orderBy) ? orderBy : " ORDER BY pcm.insertTime desc");
        int from = 0;
        int to = NUMBER_RECORD_PER_PAGE;
        if (page != -1) {
            to = (page) * ReportPage.NUMBER_RECORD_PER_PAGE;
        }
		EntityManager entityManager = provider.get();
        TypedQuery<PerfCodificaMassiva> perfCodificaMassivaQuery = entityManager.createQuery(
                query
                , PerfCodificaMassiva.class);
        perfCodificaMassivaQuery.setFirstResult(from);
        perfCodificaMassivaQuery.setMaxResults(to);
        return perfCodificaMassivaQuery.getResultList();
	}
    
}
