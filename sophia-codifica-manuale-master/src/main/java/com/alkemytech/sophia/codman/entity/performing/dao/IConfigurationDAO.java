package com.alkemytech.sophia.codman.entity.performing.dao;

import java.util.List;

import com.alkemytech.sophia.codman.entity.Configuration;
import com.alkemytech.sophia.codman.entity.performing.Circoscrizione;
import com.alkemytech.sophia.codman.entity.performing.Configurazione;

/**
 * Created by idilello on 6/9/16.
 */
public interface IConfigurationDAO {

    public String getParameter(String parameterName);

    public void insertParameter(String parameterName, String parameterValue);

    public List<Circoscrizione> getCircoscrizioni();    
    
    public void insertParameterObject(Configurazione configurazione);
    
    public Configurazione getParameterObject(String parameterName);

    public String getLogicalServerAddress(String phisycalAddress);

    public List<Configuration> getConfiguration(String domain, String key);
    
    public Configuration getConfiguration(long id);

	public Configuration saveConfiguration(Configuration configuration);
	
	public List<Configuration> getConfigurationHistory(String domain, String key);

	public void deleteConfiguration(Configuration configuration);

}
