package com.alkemytech.sophia.codman.multimedialelocale.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MultimedialeStoricoRequestDto {

	private String periodoCompetenza;
	private String licenza;
	private String servizio;
	private String statoLogico;

	public MultimedialeStoricoRequestDto() {
		super();
	}

	public MultimedialeStoricoRequestDto(String periodoCompetenza, String licenza, String servizio,
			String statoLogico) {
		super();
		this.periodoCompetenza = periodoCompetenza;
		this.licenza = licenza;
		this.servizio = servizio;
		this.statoLogico = statoLogico;
	}

	public String getPeriodoCompetenza() {
		return periodoCompetenza;
	}

	public void setPeriodoCompetenza(String periodoCompetenza) {
		this.periodoCompetenza = periodoCompetenza;
	}



	public String getLicenza() {
		return licenza;
	}

	public void setLicenza(String licenza) {
		this.licenza = licenza;
	}

	public String getServizio() {
		return servizio;
	}

	public void setServizio(String servizio) {
		this.servizio = servizio;
	}

	public String getStatoLogico() {
		return statoLogico;
	}

	public void setStatoLogico(String statoLogico) {
		this.statoLogico = statoLogico;
	}


}
