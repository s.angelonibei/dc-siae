package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DsrSearchRequestDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<String> dspList = new ArrayList<>();
	private List<String> countryList= new ArrayList<>();
	private List<String> utilizationList= new ArrayList<>();
	private List<String> offerList= new ArrayList<>();

	private int monthFrom;
	private int yearFrom;
	private int monthTo;
	private int yearTo;
	
	private String idDsr = "";

	private List<DsrProcessingStateDTO> processingStatuses = new ArrayList<>();

	private Integer first;
	private Integer last;
	private String sortBy;
	private String order; // ASC or DESC


	public String getDecodedSort(){
		String sort = "";
		switch (sortBy){
			case "dsr":
				sort =  "DSR";
				break;
			case "dsp":
				sort =  "DSP";
				break;
			case "dspName":
				sort =  "DSP";
				break;
			case "utilizationType":
				sort =  "`TIPO UTILIZZO`";
				break;
			case "utilizationName":
				sort =  "`TIPO UTILIZZO`";
				break;
			case "commercialOffer":
				sort =  "`OFFERTA COMMERCIALE`";
				break;
			case "commercialOfferName":
				sort =  "`OFFERTA COMMERCIALE`";
				break;
			case "period":
				sort =  "YEAR " +  order + ", PERIOD" ;
				break;
			case "country":
				sort =  "TERRITORIO";
				break;
			case "countryName":
				sort =  "TERRITORIO";
				break;
			case "dsrDeliveryDate":
				sort =  "INSERT_TIME";
				break;
			case "spanDsrProcessingStart":
				sort =  "DATA_INIZIO";
				break;
			case "dsrProcessingEnd":
				sort =  "DATA_FINE";
				break;
			case "status":
				sort =  "STATUS";
				break;
			case "errorType":
				sort =  "ERROR_MESSAGE";
				break;
			case "kbVersion":
				sort =  "IDENTIFY_KB_VERSION";
				break;
			default:
				sort =  "INSERT_TIME";
				
		
		}
		sort += " " + order;
		return sort;

	}

	public List<String> getDspList() {
		return dspList;
	}

	public void setDspList(List<String> dspList) {
		this.dspList = dspList;
	}

	public List<String> getCountryList() {
		return countryList;
	}

	public void setCountryList(List<String> countryList) {
		this.countryList = countryList;
	}

	public List<String> getUtilizationList() {
		return utilizationList;
	}

	public void setUtilizationList(List<String> utilizationList) {
		this.utilizationList = utilizationList;
	}

	public List<String> getOfferList() {
		return offerList;
	}

	public void setOfferList(List<String> offerList) {
		this.offerList = offerList;
	}

	public int getMonthFrom() {
		return monthFrom;
	}

	public void setMonthFrom(int monthFrom) {
		this.monthFrom = monthFrom;
	}

	public int getYearFrom() {
		return yearFrom;
	}

	public void setYearFrom(int yearFrom) {
		this.yearFrom = yearFrom;
	}

	public int getMonthTo() {
		return monthTo;
	}

	public void setMonthTo(int monthTo) {
		this.monthTo = monthTo;
	}

	public int getYearTo() {
		return yearTo;
	}

	public void setYearTo(int yearTo) {
		this.yearTo = yearTo;
	}

	public Integer getFirst() {
		return first;
	}

	public void setFirst(Integer first) {
		this.first = first;
	}

	public Integer getLast() {
		return last;
	}

	public void setLast(Integer last) {
		this.last = last;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public List<DsrProcessingStateDTO> getProcessingStatuses() {
		return processingStatuses;
	}

	public void setProcessingStatuses(List<DsrProcessingStateDTO> processingStatuses) {
		this.processingStatuses = processingStatuses;
	}

	public String getIdDsr() {
		return idDsr;
	}

	public void setIdDsr(String idDsr) {
		this.idDsr = idDsr;
	}
}
