package com.alkemytech.sophia.codman.dto.enumaration;

public enum IngestionCodificatoStatus {
    INCORSO("IN CORSO"),
    COMPLETATO("COMPLETATO"),
    ERRORE("ERRORE");


    private String name;

    IngestionCodificatoStatus(String name){
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
