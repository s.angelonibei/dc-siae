package com.alkemytech.sophia.codman.rest.performing.service;

import com.alkemytech.sophia.codman.entity.performing.EsecuzioneRicalcolo;
import com.alkemytech.sophia.codman.entity.performing.InformazioniPM;
import com.alkemytech.sophia.codman.entity.performing.MovimentoContabile;
import com.alkemytech.sophia.codman.entity.performing.security.ReportPage;

import java.util.List;

/**
 * Created by idilello on 6/6/16.
 */
public interface IConsoleRicalcoloService {

    public Long[] getPeriodoAAAAMM(String parameterName);

    public List<EsecuzioneRicalcolo> getEsecuzioni();

    public String checkStatus();

    public String deactivate();

    public String startLocalProcess(int enginePort, int engineIndex);

    public String startLocalProcessFromRemote(int engineIndex);

    public String startProcess();

    public InformazioniPM getAvanzamentoElaborazione();

    public String startRicalcolo();

    public ReportPage getListaPM(Long idEsecuzioneRicalcolo, Long idEvento, String numeroFattura, String voceIncasso, String numeroProgrammaMusicale, String inizioPeriodoContabile, String finePeriodoContabile, Integer page, String tipoSospensione);

    public String startCampionamento();

    public String startAggregazioneSiada(Long elaborazioneSIADAId);

    public InformazioniPM getAvanzamentoAggregazione();

    public InformazioniPM getAvanzamentoElaborazioneCampionamento();

    public int setDataCongelamentoEsecuzione(Long elaborazioneSIADAId);

}
