package com.alkemytech.sophia.codman.entity.performing.dao;

import com.alkemytech.sophia.codman.dto.ResponseDTO;
import com.alkemytech.sophia.codman.performing.dto.SessioneCodificaDTO;
import com.alkemytech.sophia.codman.performing.dto.ScodificaRequestDTO;

public interface IScodificaDAO {
    ResponseDTO getListaScodifica(ScodificaRequestDTO scodificaRequestDTO);
    ResponseDTO getDettaglioCombana(Long idCombana);
    void deleteCodiceOpera(Long idCodifica, Long idCombana, String codiceOpera);
    ResponseDTO getListaScodificaForExport(ScodificaRequestDTO scodificaRequestDTO);
    ResponseDTO getListaUtilizzazioniForExport(Long idCombana);
    ResponseDTO getDettaglioScodifica(Long idCombana);

    SessioneCodificaDTO getSessioneScodifica(Long idCombana, Long idCodifica, String utente);
}
