package com.alkemytech.sophia.codman.rest.performing.serviceimpl.config.utils;

import static org.apache.commons.collections4.CollectionUtils.isEmpty;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.codman.entity.Configuration;
import com.alkemytech.sophia.codman.entity.KeyValueProperty;
import com.alkemytech.sophia.codman.entity.Setting;
import com.alkemytech.sophia.codman.entity.Threshold;
import com.alkemytech.sophia.codman.entity.ThresholdSet;
import com.alkemytech.sophia.codman.entity.performing.dao.IConfigurationDAO;
import com.google.inject.Singleton;

@Singleton
public class ConfigurationValidatorService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	List<SettingValidator> settingValidators = new ArrayList<SettingValidator>();
	List<ConfigurationValidator> configurationValidators = new ArrayList<ConfigurationValidator>();
	IConfigurationDAO configurationDAO;

	@Inject
	public ConfigurationValidatorService(IConfigurationDAO configurationDAO) {
		this.configurationDAO = configurationDAO;
		settingValidators.add(new DecimalPropertyValidator());
		settingValidators.add(new IntegerPropertyValidator());
		settingValidators.add(new DecimalThresholdValidator());
		settingValidators.add(new IntegerThresholdValidator());
		configurationValidators.add(new RankingConfigurationValidator());
		configurationValidators.add(new NDMThresholdConfigurationValidator());
		configurationValidators.add(new AggiConfigurationValidator());

	}

	public int validate(Setting setting) {
		int errors = 0;
		for (SettingValidator validator : settingValidators) {
			if (validator.supports(setting)) {
				errors += validator.validate(setting);
			}
		}
		return errors;
	}

	public int validate(Configuration configuration) {
		int errors = 0;
		for (Setting setting : configuration) {
			errors += validate(setting);
		}
		for (ConfigurationValidator validator : configurationValidators) {
			if (validator.supports(configuration)) {
				errors += validator.validate(configuration);
			}
		}
		return errors;
	}

	class DecimalPropertyValidator implements SettingValidator {
		@Override
		public int validate(Setting setting) {
			try {
				Double.parseDouble(((KeyValueProperty) setting).getValue());
				setting.getErrorMessages().clear();
				return 0;
			} catch (Exception e) {
				logger.error("Errore Validazione",e);
				setting.getErrorMessages().add("Numero non valido!");
				return 1;
			}
		}

		@Override
		public boolean supports(Setting setting) {
			if (setting.getClass() == KeyValueProperty.class && ((KeyValueProperty) setting).getValueType().equals("Decimal")) {
				return true;
			}
			return false;
		}
	}
	
	class DecimalThresholdValidator implements SettingValidator {
		@Override
		public int validate(Setting setting) {
			try {
				Double.parseDouble(((Threshold) setting).getValue());
				setting.getErrorMessages().clear();
				return 0;
			} catch (Exception e) {
				logger.error("Errore Validazione",e);
				setting.getErrorMessages().add("Numero non valido!");
				return 1;
			}
		}

		@Override
		public boolean supports(Setting setting) {
			if (setting.getClass() == Threshold.class && ((Threshold) setting).getValueType().equals("Decimal")) {
				return true;
			}
			return false;
		}
	}
	
	class IntegerThresholdValidator implements SettingValidator {
		@Override
		public int validate(Setting setting) {
			try {
				Integer.parseInt(((Threshold) setting).getValue());
				setting.getErrorMessages().clear();
				return 0;
			} catch (Exception e) {
				logger.error("Errore Validazione",e);
				setting.getErrorMessages().add("Numero non valido!");
				return 1;
			}
		}

		@Override
		public boolean supports(Setting setting) {
			if (setting.getClass() == Threshold.class && ((Threshold) setting).getValueType().equals("Integer")) {
				return true;
			}
			return false;
		}
	}

	class IntegerPropertyValidator implements SettingValidator {
		@Override
		public int validate(Setting setting) {
			try {
				Integer.parseInt(((KeyValueProperty) setting).getValue());
				setting.getErrorMessages().clear();
				return 0;
			} catch (Exception e) {
				logger.error("Errore Validazione",e);
				setting.getErrorMessages().add("Numero non valido!");
				return 1;
			}
		}

		@Override
		public boolean supports(Setting setting) {
			if (setting.getClass() == KeyValueProperty.class && ((KeyValueProperty) setting).getValueType().equals("Integer")) {
				return true;
			}
			return false;
		}
	}
	
	class RankingConfigurationValidator implements ConfigurationValidator {

		@Override
		public int validate(Configuration c) {
			Double totalWeight = 0.0d;
			try {
				for (Setting setting : c) {
					if (Arrays.asList("title_weight", "artist_weight").contains(setting.getKey())) {
						totalWeight += Double.parseDouble(((KeyValueProperty) setting).getValue());
					}
					if ("relevance_weight".equals(setting.getKey()) && c.getSetting("relevance_flag", ThresholdSet.class) != null
							&& (c.getSetting("relevance_flag", ThresholdSet.class)).getValue().equals("1")) {
						totalWeight += Double.parseDouble(((KeyValueProperty) setting).getValue());
					}
					if ("risk_weight".equals(setting.getKey()) && c.getSetting("risk_flag", ThresholdSet.class) != null
							&& (c.getSetting("risk_flag", ThresholdSet.class)).getValue().equals("1")) {
						totalWeight += Double.parseDouble(((KeyValueProperty) setting).getValue());
					}
				}
				int compareTo = totalWeight.compareTo(1.0d);
				if (compareTo != 0) {
					c.getErrorMessages().add(String.format("La somma dei pesi non è 1! (%1$,.2f)",totalWeight));
					return 1;
				}
				c.getErrorMessages().clear();
				return 0;
			} catch (Exception e) {
				logger.error("Errore Validazione",e);
				c.getErrorMessages().add("Configurazione Non valida!");
				return 1;
			}
		}

		@Override
		public boolean supports(Configuration c) {
			if (c.getKey().equals("ranking")) {
				return true;
			}
			return false;
		}
	}
	
	class NDMThresholdConfigurationValidator implements ConfigurationValidator {

		@Override
		public int validate(Configuration c) {
			try {
				List<Configuration> configurations = configurationDAO.getConfiguration(c.getDomain(), c.getKey());
				if (!isEmpty(configurations)) {
					c.getErrorMessages().add(String.format("Esiste già una configurazione per la voce incasso %s", c.getKey()));
					return 1;
				}
				c.getErrorMessages().clear();
				return 0;
			} catch (Exception e) {
				logger.error("Errore Validazione", e);
				c.getErrorMessages().add("Configurazione Non valida!");
				return 1;
			}
		}

		@Override
		public boolean supports(Configuration c) {
			if (c.getId() == null && c.getDomain().equals("performing.soglie-ndm")) {
				return true;
			}
			return false;
		}
	}
	
	class AggiConfigurationValidator implements ConfigurationValidator {

		@Override
		public int validate(Configuration c) {
			try {
				// Range non valido
				if(c.getValidTo()!= null && c.getValidTo().before(c.getValidFrom())) {
					c.getErrorMessages().add(String.format("Date non congruenti %s >= %s", new SimpleDateFormat("dd-MM-yyyy").format(c.getValidTo()), new SimpleDateFormat("dd-MM-yyyy").format(c.getValidFrom())));
					return 1;
				}
				List<Configuration> configurations = configurationDAO.getConfiguration(c.getDomain(), c.getKey());
				List<Configuration> overlappings = getOverlappingConfigs(c, configurations);
				if (overlappings.size() == 0) {
					c.getErrorMessages().clear();
					return 0;
				}
				if (overlappings.size() > 0) {
					Collections.sort(overlappings, new Comparator<Configuration>() {
						@Override
						public int compare(Configuration c1, Configuration c2) {
							return c1.getValidFrom().compareTo(c2.getValidFrom());
						}
					});
					Configuration split = overlappings.get(0);
					if (split.getValidFrom() != null && split.getValidTo() == null) {
						if(c.getValidFrom().compareTo(split.getValidFrom()) <= 0) {
							GregorianCalendar cal = new GregorianCalendar();
							cal.setTime(split.getValidFrom());
							cal.add(Calendar.SECOND, -1);
							c.setValidTo(cal.getTime());
							c.setRelatedConfiguration(split);	
						}else {
							GregorianCalendar cal = new GregorianCalendar();
							cal.setTime(c.getValidFrom());
							cal.add(Calendar.SECOND, -1);
							split.setValidTo(cal.getTime());
							c.setRelatedConfiguration(split);
							setEndTime(split);
						}
						c.getErrorMessages().clear();
						return 0;						
					}					
					if (split.getValidFrom() != null && c.getValidTo() == null) {
						if (c.getValidFrom().compareTo(split.getValidTo()) == 0) {
							GregorianCalendar cal = new GregorianCalendar();
							cal.setTime(c.getValidFrom());
							cal.add(Calendar.SECOND, 1);
							c.setValidFrom(cal.getTime());							
						} else {
							c.setValidTo(split.getValidFrom());
						}
						c.getErrorMessages().clear();
						return 0;
					}				
				}
				c.getErrorMessages().add(String.format("Il periodo inserito si sovrappone al periodo di una configurazione già esistente!"));
				return 1;
				
			} catch (Exception e) {
				logger.error("Errore Validazione", e);
				c.getErrorMessages().add("Configurazione Non valida!");
				return 1;
			}
		}
		
		private List<Configuration> getOverlappingConfigs(Configuration c, List<Configuration> configurations) {
			List<Configuration> overlapping = new ArrayList<Configuration>();
			for (Configuration configuration : configurations) { // (StartA <= EndB) and (EndA >= StartB)
				if ((configuration.getValidTo() == null || c.getValidFrom().compareTo(configuration.getValidTo()) < 0)
						&& (c.getValidTo() == null || c.getValidTo().compareTo(configuration.getValidFrom()) > 0)) {
					overlapping.add(configuration);
				}
			}
			return overlapping;
		}
		
	    private void setEndTime(Configuration configuration) {
	        configuration.setModified(new Date());
	        List<Configuration> configurations = configuration.getConfigurations();
	        if (configurations.isEmpty()) {
	            return;
	        }
	        for (Configuration inner : configurations) {
	            setEndTime(inner);
	        }
	    }		

		@Override
		public boolean supports(Configuration c) {
			if (c.getDomain().equals("multimediale.aggi")) {
				return true;
			}
			return false;
		}
	}

}