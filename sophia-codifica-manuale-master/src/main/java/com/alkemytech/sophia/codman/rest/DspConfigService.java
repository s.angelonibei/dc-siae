package com.alkemytech.sophia.codman.rest;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.codman.dto.DspConfigDTO;
import com.alkemytech.sophia.codman.entity.CommercialOffers;
import com.alkemytech.sophia.codman.entity.DspConfig;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

@Singleton
@Path("dspConfig")
public class DspConfigService {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Provider<EntityManager> provider;
	private final Gson gson;

	@Inject
	protected DspConfigService(@McmdbDataSource Provider<EntityManager> provider, Gson gson) {
		super();
		this.provider = provider;
		this.gson = gson;
	}

	@GET
	@Path("allConfig")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response all(@DefaultValue("0") @QueryParam("first") int first,
			@DefaultValue("50") @QueryParam("last") int last, @DefaultValue("") @QueryParam("dsp") String dsp,
			@DefaultValue("") @QueryParam("utilization") String utilization,
			@DefaultValue("") @QueryParam("offer") String offer) {

		EntityManager entityManager = null;
		List<Object[]> result;
		try {

			entityManager = provider.get();

			String sql = "SELECT * FROM (SELECT "
					+ " DSP_CONFIG.ID_DSP_CONFIG,"
					+ " DSP_CONFIG.IDDSP IDDSP,"
					+ " ANAG_DSP.NAME as DSP_NAME, "
					+ " DSP_CONFIG.ID_UTILIZATION_TYPE as ID_UTILIZATION, " 
					+ " (IF(DSP_CONFIG.ID_UTILIZATION_TYPE='*', '*', (SELECT NAME FROM ANAG_UTILIZATION_TYPE WHERE ID_UTILIZATION_TYPE=DSP_CONFIG.ID_UTILIZATION_TYPE))) as UTILIZATION_NAME,"
					+ " OFFERING as ID_OFFER, "
					+ " OFFERING as OFFER_NAME, "
					+ " DSP_CONFIG.DSR_FREQUENCY, "
					+ " DSP_CONFIG.SLA_RECEIVING, "
					+ " DSP_CONFIG.SLA_SENDING "
					+ " FROM "
					+ " ANAG_DSP, "
					+ " DSP_CONFIG "
					+ " WHERE "
					+ " OFFERING='*' AND DSP_CONFIG.IDDSP = ANAG_DSP.IDDSP "
					+ " UNION "
					+ " SELECT "
					+ " CONFIG.ID_DSP_CONFIG, "
					+ " OFFER.IDDSP as IDDSP, "
					+ " ANAG_DSP.NAME as DSP_NAME, "
					+ " OFFER.ID_UTIILIZATION_TYPE as ID_UTILIZATION, "
					+ " ANAG_UTILIZATION_TYPE.NAME as UTILIZATION_NAME, "
					+ " OFFER.ID_COMMERCIAL_OFFERS as  ID_OFFER, "
					+ " OFFER.OFFERING as OFFER_NAME, "
					+ " CONFIG.DSR_FREQUENCY, "
					+ " CONFIG.SLA_RECEIVING, "
					+ " CONFIG.SLA_SENDING "
					+ " FROM "
					+ " COMMERCIAL_OFFERS OFFER, "
					+ " DSP_CONFIG CONFIG, "
					+ " ANAG_UTILIZATION_TYPE, "
					+ " ANAG_DSP "
					+ " WHERE "
					+ " CONFIG.OFFERING=OFFER.ID_COMMERCIAL_OFFERS "
					+ " AND OFFER.ID_UTIILIZATION_TYPE = ANAG_UTILIZATION_TYPE.ID_UTILIZATION_TYPE "
					+ " AND OFFER.IDDSP = ANAG_DSP.IDDSP) x "
					+ ((!dsp.isEmpty() && !dsp.equals("*")) || (!utilization.isEmpty() && !utilization.equals("*"))
							|| (!offer.isEmpty() && !offer.equals("*")) ? " where " : "")

					+ (!dsp.isEmpty() && !dsp.equals("*") ? " x.IDDSP = ? " : "")
					+ (!dsp.isEmpty() && !dsp.equals("*") && !utilization.isEmpty() && !utilization.equals("*")
							? " and " : "")
					
					+ (!utilization.isEmpty() && !utilization.equals("*")
							? "  ( ID_UTILIZATION = ? or (x.ID_UTILIZATION = '*'  and " +
                         " IDDSP in (select distinct IDDSP from COMMERCIAL_OFFERS where COMMERCIAL_OFFERS.ID_UTIILIZATION_TYPE = ?))) " : "")
					+ (((!utilization.isEmpty() && !utilization.equals("*")) ||(!dsp.isEmpty() && !dsp.equals("*"))) && !offer.isEmpty() && !offer.equals("*")
							? " and " : "")
					+ (!offer.isEmpty() && !offer.equals("*") ? "  ( ID_OFFER = ? or (ID_OFFER='*'  and " +
                         " IDDSP = (select  IDDSP from COMMERCIAL_OFFERS where ID_COMMERCIAL_OFFERS = ?)  and " +
                         " (ID_UTILIZATION = (select  ID_UTIILIZATION_TYPE from COMMERCIAL_OFFERS where ID_COMMERCIAL_OFFERS = ?) or ID_UTILIZATION = '*')))   " : "")
					+ "order by x. DSP_NAME, x.UTILIZATION_NAME, x.OFFER_NAME";


			
			final Query q = entityManager.createNativeQuery(sql).setFirstResult(first) // offset
					.setMaxResults(1 + last - first);
			
			int i = 1;
			if (!dsp.isEmpty() && !dsp.equals("*")) {
				q.setParameter(i++, dsp);
			}
			if (!utilization.isEmpty() && !utilization.equals("*")) {
				q.setParameter(i++, utilization);
				q.setParameter(i++, utilization);
			}
			if (!offer.isEmpty() && !offer.equals("*")) {
				q.setParameter(i++, offer);
				q.setParameter(i++, offer);
				q.setParameter(i++, offer);
			}
			
			result = (List<Object[]>) q.getResultList();
			List<DspConfigDTO> dtoList = new ArrayList<DspConfigDTO>();
			final PagedResult pagedResult = new PagedResult();
			if (null != result) {
				final int maxrows = last - first;
				final boolean hasNext = result.size() > maxrows;
				for (Object[] p : (hasNext ? result.subList(0, maxrows) : result)) {
					dtoList.add(new DspConfigDTO(p));
				}
				pagedResult.setRows(dtoList).setMaxrows(maxrows).setFirst(first)
						.setLast(hasNext ? last : first + result.size()).setHasNext(hasNext).setHasPrev(first > 0);
			} else {
				pagedResult.setMaxrows(0).setFirst(0).setLast(0).setHasNext(false).setHasPrev(false);
			}
			return Response.ok(gson.toJson(pagedResult)).build();
		} catch (Exception e) {
			logger.error("all", e);
		}
		return Response.status(500).build();
	}

	@POST
	@Path("")
	public Response add(DspConfigDTO config) {
		
		if(!config.getIdOffer().equals("*")){
			config.setIdDsp("*");
			config.setIdUtilization("*");
			
		}
		
		DspConfig dspConfig = new DspConfig();
		dspConfig.setIdDSP(config.getIdDsp());
		dspConfig.setIdUtilizationType(config.getIdUtilization());
		dspConfig.setOffering(config.getIdOffer());
		dspConfig.setDsrFrequency(config.getFrequency());
		dspConfig.setSlaSending(String.valueOf(config.getSending()));
		dspConfig.setSlaReceiving(String.valueOf(config.getReceiving()));
		
		Status status = Status.INTERNAL_SERVER_ERROR;
		final EntityManager entityManager = provider.get();
		try {
			entityManager.getTransaction().begin();
			entityManager.persist(dspConfig);

			entityManager.getTransaction().commit();
			return Response.ok().build();
		} catch (Exception e) {
			logger.error("add", e);

			if (e.getCause() != null && e.getCause().getCause() != null
					&& e.getCause().getCause() instanceof MySQLIntegrityConstraintViolationException) {
				status = Status.CONFLICT;
			}

			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}

		}

		return Response.status(status).build();
	}

	@DELETE
	@Path("")
	public Response delete(DspConfigDTO config) {
		if(!config.getIdOffer().equals("*")){
			config.setIdDsp("*");
			config.setIdUtilization("*");
		}
		final EntityManager entityManager = provider.get();
		try {
			entityManager.getTransaction().begin();
			final Query query = entityManager.createQuery("delete from DspConfig x where x.idDSP = :dspId "
					+ "and x.idUtilizationType = :utilizationType  and x.offering = :offering");
			query.setParameter("dspId", config.getIdDsp());
			query.setParameter("utilizationType", config.getIdUtilization());
			query.setParameter("offering", config.getIdOffer());

			query.executeUpdate();
			entityManager.getTransaction().commit();
			return Response.ok().build();
		} catch (Exception e) {
			logger.error("delete", e);
			entityManager.getTransaction().rollback();
		}
		return Response.status(500).build();
	}

	@PUT
	@Path("")
	public Response update(DspConfigDTO config) {
		if(!config.getIdOffer().equals("*")){
			config.setIdDsp("*");
			config.setIdUtilization("*");
		}
		final EntityManager entityManager = provider.get();
		try {
			entityManager.getTransaction().begin();

			final Query query = entityManager.createQuery("update DspConfig x set x.dsrFrequency= :dsrFrequency, "
					+ "x.slaReceiving = :slaReceiving, x.slaSending= :slaSending where x.idDSP = :dspId "
					+ "and x.idUtilizationType = :utilizationType  and x.offering = :offering");
			query.setParameter("dspId", config.getIdDsp());
			query.setParameter("utilizationType", config.getIdUtilization());
			query.setParameter("offering", config.getIdOffer());
			query.setParameter("dsrFrequency", config.getFrequency());
			query.setParameter("slaReceiving", String.valueOf(config.getReceiving()));
			query.setParameter("slaSending", String.valueOf(config.getSending()));

			query.executeUpdate();
			entityManager.getTransaction().commit();
			return Response.ok().build();
		} catch (Exception e) {
			logger.error("update", e);
			entityManager.getTransaction().rollback();
		}
		return Response.status(500).build();
	}
	
	@GET
	@Path("allAvailableOffers")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response allAvailableOffers() {
		EntityManager entityManager = null;
		List<CommercialOffers> result;
		try {
			entityManager = provider.get();
			final Query q = entityManager.createQuery(
					"select x from CommercialOffers x");
			result = (List<CommercialOffers>) q.getResultList();
			return Response.ok(gson.toJson(result)).build();
		} catch (Exception e) {
			logger.error("allAvailableOffers", e);
		}
		return Response.status(500).build();
	}

}
