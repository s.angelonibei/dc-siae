package com.alkemytech.sophia.codman.multimedialelocale.dto;

import com.alkemytech.sophia.codman.multimedialelocale.entity.MultimedialeLocale;
import com.google.gson.Gson;
import com.opencsv.bean.CsvBindByPosition;
import org.apache.http.util.TextUtils;

import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;
import java.text.SimpleDateFormat;

@Produces("application/json")
@XmlRootElement
public class MultimedialeLocaleCsvDto {

	@CsvBindByPosition(position = 0)
	private String identificativoReport;

	@CsvBindByPosition(position = 1)
	private String licenza;

	@CsvBindByPosition(position = 2)
	private String codiceLicenziatario;

	@CsvBindByPosition(position = 3)
	private String nominativoLicenziatario;

	@CsvBindByPosition(position = 4)
	private String servizio;

	@CsvBindByPosition(position = 5)
	private String origPeriodoCompetenza;

	@CsvBindByPosition(position = 6)
	private Integer progressivoReport;

	@CsvBindByPosition(position = 7)
	private String modUtilizzazione;

	@CsvBindByPosition(position = 8)
	private String modFruizione;

	@CsvBindByPosition(position = 9)
	private String inAbbonamento;

	@CsvBindByPosition(position = 10)
	private String statoLogico;

	@CsvBindByPosition(position = 11)
	private String dataUpload;

	@CsvBindByPosition(position = 12)
	private String dataValidazione;

	@CsvBindByPosition(position = 13)
	private String dataCodifica;

	@CsvBindByPosition(position = 14)
	private String validazioneRecordTotali;

	@CsvBindByPosition(position = 15)
	private String validazioneRecordValidi;

	@CsvBindByPosition(position = 16)
	private String validazioneUtilizzazioniValide;

	@CsvBindByPosition(position = 17)
	private String codificaRecordCodificati;

	@CsvBindByPosition(position = 18)
	private String codificaUtilizzazioniCodificate;

	@CsvBindByPosition(position = 19)
	private String scarto;

	@CsvBindByPosition(position = 20)
	private String codifica;

	@CsvBindByPosition(position = 21)
	private String opereCodificate;

	@CsvBindByPosition(position = 22)
	private String recordNonCodificati;

	@CsvBindByPosition(position = 23)
	private String recordCodificatiRegolari;

	@CsvBindByPosition(position = 24)
	private String recordCodificatiIrregolari;

	@CsvBindByPosition(position = 25)
	private String utilizzazioniNonCodificate;

	@CsvBindByPosition(position = 26)
	private String utilizzazioniCodificateRegolari;

	@CsvBindByPosition(position = 27)
	private String utilizzazioniCodificateIrregolari;

	@CsvBindByPosition(position = 28)
	private String percRecordNonCodificati;

	@CsvBindByPosition(position = 29)
	private String percRecordCodificatiRegolari;

	@CsvBindByPosition(position = 30)
	private String percRecordCodificatiIrregolari;

	@CsvBindByPosition(position = 31)
	private String percUtilizzazioniNonCodificate;

	@CsvBindByPosition(position = 32)
	private String percUtilizzazioniCodificateRegolari;

	@CsvBindByPosition(position = 33)
	private String percUtilizzazioniCodificateIrregolari;

	@CsvBindByPosition(position = 34)
	private String percPolverizzazione;

	@CsvBindByPosition(position = 35)
	private String ripartizione;

	@CsvBindByPosition(position = 36)
	private String importo;

	@CsvBindByPosition(position = 37)
	private String ripartito;

	@CsvBindByPosition(position = 38)
	private String dataRipartizione;

	@CsvBindByPosition(position = 39)
	private String importoExArt18;

	public MultimedialeLocaleCsvDto() {
		super();
	}

	public MultimedialeLocaleCsvDto(MultimedialeLocale multimedialeLocale) {
		super();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm");
		this.identificativoReport = multimedialeLocale.getIdentificativoReport();
		this.licenza = multimedialeLocale.getLicenza();
		this.codiceLicenziatario = multimedialeLocale.getCodiceLicenziatario();
		this.nominativoLicenziatario = multimedialeLocale.getNominativoLicenziatario();
		this.servizio = multimedialeLocale.getServizio();
		this.origPeriodoCompetenza = multimedialeLocale.getOrigPeriodoCompetenza();
		this.progressivoReport = multimedialeLocale.getProgressivoReport();
		this.modUtilizzazione = multimedialeLocale.getModUtilizzazione().getModalita();
		this.modFruizione = multimedialeLocale.getModFruizione();
		this.inAbbonamento = multimedialeLocale.getInAbbonamento() ? "SI" : "NO";
		this.statoLogico = multimedialeLocale.getStatoLogico();
		try {
			this.dataUpload = sdf.format(multimedialeLocale.getDataUpload());
		} catch (Exception e) {

		}
		try {
			this.dataValidazione = sdf.format(multimedialeLocale.getDataValidazione());
		} catch (Exception e) {
		}
		if (multimedialeLocale.getValidazioneRecordTotali() != null) {
			this.validazioneRecordTotali = multimedialeLocale.getValidazioneRecordTotali() + "";
		} else {
			this.validazioneRecordTotali = "";
		}
		if (multimedialeLocale.getValidazioneRecordValidi() != null) {
			this.validazioneRecordValidi = multimedialeLocale.getValidazioneRecordValidi() + "";
		} else {
			this.validazioneRecordValidi = "";
		}
		if (multimedialeLocale.getValidazioneUtilizzazioniValide() != null) {
			this.validazioneUtilizzazioniValide = multimedialeLocale.getValidazioneUtilizzazioniValide() + "";
		} else {
			this.validazioneUtilizzazioniValide = "";
		}
		try {
			this.dataCodifica = sdf.format(multimedialeLocale.getDataCodifica());
		} catch (Exception e) {

		}
		if (multimedialeLocale.getCodificaUtilizzazioniCodificate() != null) {
			this.codificaUtilizzazioniCodificate = multimedialeLocale.getCodificaUtilizzazioniCodificate() + "";
		} else {
			this.codificaUtilizzazioniCodificate = "";
		}
		if (multimedialeLocale.getCodificaRecordCodificati() != null) {
			this.codificaRecordCodificati = multimedialeLocale.getCodificaRecordCodificati() + "";
		} else {
			this.codificaRecordCodificati = "";
		}

		if (multimedialeLocale.getValidazioneRecordTotali() != null
				&& multimedialeLocale.getValidazioneRecordValidi() != null) {
			this.scarto = String.format("%.2f",
					((multimedialeLocale.getValidazioneRecordTotali() - multimedialeLocale.getValidazioneRecordValidi())
							* 100.0 / multimedialeLocale.getValidazioneRecordTotali()))
					+ "%";
		} else {
			this.scarto = "";
		}

		if (multimedialeLocale.getCodificaUtilizzazioniCodificate() != null
				&& multimedialeLocale.getValidazioneUtilizzazioniValide() != null) {
			this.codifica = String.format("%.2f", multimedialeLocale.getCodificaUtilizzazioniCodificate() * 100.0
					/ multimedialeLocale.getValidazioneUtilizzazioniValide()) + "%";
		} else {
			this.codifica = "";
		}

		if (multimedialeLocale.getCodificaRecordCodificati() != null
				&& multimedialeLocale.getValidazioneRecordValidi() != null) {
			this.opereCodificate = String.format("%.2f", multimedialeLocale.getCodificaRecordCodificati() * 100.0
					/ multimedialeLocale.getValidazioneRecordValidi()) + "%";
		} else {
			this.opereCodificate = "";
		}

		this.inAbbonamento = multimedialeLocale.getInAbbonamento() ? "SI" : "NO";
		this.statoLogico = multimedialeLocale.getStatoLogico();
		try {
			this.dataUpload = sdf.format(multimedialeLocale.getDataUpload());
		} catch (Exception e) {

		}

		try {
			if (!TextUtils.isEmpty(multimedialeLocale.getCodificaStatistiche())) {
				Gson gson = new Gson();
				CodificaStatistiche jsonObject = gson.fromJson(multimedialeLocale.getCodificaStatistiche(),
						CodificaStatistiche.class);
				this.recordNonCodificati = jsonObject.getRecordNonCodificati() + "";
				this.recordCodificatiRegolari = jsonObject.getRecordCodificatiRegolari() + "";
				this.recordCodificatiIrregolari = jsonObject.getRecordCodificatiIrregolari() + "";
				this.utilizzazioniNonCodificate = jsonObject.getUtilizzazioniNonCodificate() + "";
				this.utilizzazioniCodificateRegolari = jsonObject.getUtilizzazioniCodificateRegolari() + "";
				this.utilizzazioniCodificateIrregolari = jsonObject.getUtilizzazioniCodificateIrregolari() + "";
				if (jsonObject.getRecordCodificatiProvvisori() == null) {
					jsonObject.setRecordCodificatiProvvisori(0);
				}
				if (jsonObject.getUtilizzazioniCodificateProvvisorie() == null) {
					jsonObject.setUtilizzazioniCodificateProvvisorie(0);
				}
				if (multimedialeLocale.getValidazioneRecordValidi() != null) {

					if (jsonObject != null && jsonObject.getRecordNonCodificati() != null) {
						double p1 = jsonObject.getRecordNonCodificati()
								/ multimedialeLocale.getValidazioneRecordValidi().doubleValue();
						this.percRecordNonCodificati = String.format("%.2f", p1 * 100.0) + "%";
					}

					if (jsonObject != null && jsonObject.getRecordCodificatiRegolari() != null
							&& jsonObject.getRecordCodificatiProvvisori() != null) {
						double p2 = (jsonObject.getRecordCodificatiRegolari()
								+ jsonObject.getRecordCodificatiProvvisori())
								/ multimedialeLocale.getValidazioneRecordValidi().doubleValue();
						this.percRecordCodificatiRegolari = String.format("%.2f", p2 * 100.0) + "%";

					}

					if (jsonObject != null && jsonObject.getRecordCodificatiIrregolari() != null) {
						double p3 = jsonObject.getRecordCodificatiIrregolari()
								/ multimedialeLocale.getValidazioneRecordValidi().doubleValue();
						this.percRecordCodificatiIrregolari = String.format("%.2f", p3 * 100.0) + "%";
					}
				}

				if (multimedialeLocale.getValidazioneUtilizzazioniValide() != null) {

					if (jsonObject != null && jsonObject.getUtilizzazioniNonCodificate() != null) {
						double p1 = jsonObject.getUtilizzazioniNonCodificate()
								/ multimedialeLocale.getValidazioneUtilizzazioniValide().doubleValue();
						this.percUtilizzazioniNonCodificate = String.format("%.2f", p1 * 100.0) + "%";
					}

					if (jsonObject != null && jsonObject.getUtilizzazioniCodificateRegolari() != null
							&& jsonObject.getUtilizzazioniCodificateProvvisorie() != null) {
						double p2 = (jsonObject.getUtilizzazioniCodificateRegolari()
								+ jsonObject.getUtilizzazioniCodificateProvvisorie())
								/ multimedialeLocale.getValidazioneUtilizzazioniValide().doubleValue();
						this.percUtilizzazioniCodificateRegolari = String.format("%.2f", p2 * 100.0) + "%";
					}

					if (jsonObject != null && jsonObject.getUtilizzazioniCodificateIrregolari() != null) {
						double p3 = jsonObject.getUtilizzazioniCodificateIrregolari()
								/ multimedialeLocale.getValidazioneUtilizzazioniValide().doubleValue();
						this.percUtilizzazioniCodificateIrregolari = String.format("%.2f", p3 * 100.0) + "%";
					}

				}
				this.percPolverizzazione = String.format("%.2f", MlUtil.getPercPolverizzazione(multimedialeLocale)) + "%";
			}
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.ripartizione=multimedialeLocale.getPeriodoRipartizione();
		this.ripartito= multimedialeLocale.getDataRipartizione()!=null ? "SI" : "NO";
		try {
			this.dataRipartizione = sdf.format(multimedialeLocale.getDataRipartizione());
		} catch (Exception e) {
		}

		if (multimedialeLocale.getImporto() != null) {
			try {
				this.importo = String.format("%.2f", multimedialeLocale.getImporto());
			} catch (Exception e) {
			}
			
		} else {
			this.importo = "";
		}
		
		if (multimedialeLocale.getImportoExArt18() != null) {
			try {
				this.importoExArt18 = String.format("%.2f", multimedialeLocale.getImportoExArt18());
			} catch (Exception e) {
			}
		} else {
			this.importoExArt18 = "";
		}

	}



	public String getLicenza() {
		return licenza;
	}

	public void setLicenza(String licenza) {
		this.licenza = licenza;
	}

	public String getCodiceLicenziatario() {
		return codiceLicenziatario;
	}

	public void setCodiceLicenziatario(String codiceLicenziatario) {
		this.codiceLicenziatario = codiceLicenziatario;
	}

	public String getNominativoLicenziatario() {
		return nominativoLicenziatario;
	}

	public void setNominativoLicenziatario(String nominativoLicenziatario) {
		this.nominativoLicenziatario = nominativoLicenziatario;
	}

	public String getServizio() {
		return servizio;
	}

	public void setServizio(String servizio) {
		this.servizio = servizio;
	}

	public String getOrigPeriodoCompetenza() {
		return origPeriodoCompetenza;
	}

	public void setOrigPeriodoCompetenza(String origPeriodoCompetenza) {
		this.origPeriodoCompetenza = origPeriodoCompetenza;
	}

	public Integer getProgressivoReport() {
		return progressivoReport;
	}

	public void setProgressivoReport(Integer progressivoReport) {
		this.progressivoReport = progressivoReport;
	}

	public String getModUtilizzazione() {
		return modUtilizzazione;
	}

	public void setModUtilizzazione(String modUtilizzazione) {
		this.modUtilizzazione = modUtilizzazione;
	}

	public String getModFruizione() {
		return modFruizione;
	}

	public void setModFruizione(String modFruizione) {
		this.modFruizione = modFruizione;
	}

	public String getInAbbonamento() {
		return inAbbonamento;
	}

	public void setInAbbonamento(String inAbbonamento) {
		this.inAbbonamento = inAbbonamento;
	}

	public String getStatoLogico() {
		return statoLogico;
	}

	public void setStatoLogico(String statoLogico) {
		this.statoLogico = statoLogico;
	}

	public String getDataUpload() {
		return dataUpload;
	}

	public void setDataUpload(String dataUpload) {
		this.dataUpload = dataUpload;
	}

	public String getDataValidazione() {
		return dataValidazione;
	}

	public void setDataValidazione(String dataValidazione) {
		this.dataValidazione = dataValidazione;
	}

	public String getValidazioneRecordTotali() {
		return validazioneRecordTotali;
	}

	public void setValidazioneRecordTotali(String validazioneRecordTotali) {
		this.validazioneRecordTotali = validazioneRecordTotali;
	}

	public String getValidazioneRecordValidi() {
		return validazioneRecordValidi;
	}

	public void setValidazioneRecordValidi(String validazioneRecordValidi) {
		this.validazioneRecordValidi = validazioneRecordValidi;
	}

	public String getValidazioneUtilizzazioniValide() {
		return validazioneUtilizzazioniValide;
	}

	public void setValidazioneUtilizzazioniValide(String validazioneUtilizzazioniValide) {
		this.validazioneUtilizzazioniValide = validazioneUtilizzazioniValide;
	}

	public String getDataCodifica() {
		return dataCodifica;
	}

	public void setDataCodifica(String dataCodifica) {
		this.dataCodifica = dataCodifica;
	}

	public String getCodificaRecordCodificati() {
		return codificaRecordCodificati;
	}

	public void setCodificaRecordCodificati(String codificaRecordCodificati) {
		this.codificaRecordCodificati = codificaRecordCodificati;
	}

	public String getCodificaUtilizzazioniCodificate() {
		return codificaUtilizzazioniCodificate;
	}

	public void setCodificaUtilizzazioniCodificate(String codificaUtilizzazioniCodificate) {
		this.codificaUtilizzazioniCodificate = codificaUtilizzazioniCodificate;
	}

	public String getScarto() {
		return scarto;
	}

	public void setScarto(String scarto) {
		this.scarto = scarto;
	}

	public String getCodifica() {
		return codifica;
	}

	public void setCodifica(String codifica) {
		this.codifica = codifica;
	}

	public String getOpereCodificate() {
		return opereCodificate;
	}

	public void setOpereCodificate(String opereCodificate) {
		this.opereCodificate = opereCodificate;
	}

	public String getIdentificativoReport() {
		return identificativoReport;
	}

	public void setIdentificativoReport(String identificativoReport) {
		this.identificativoReport = identificativoReport;
	}

	public String getRecordNonCodificati() {
		return recordNonCodificati;
	}

	public void setRecordNonCodificati(String recordNonCodificati) {
		this.recordNonCodificati = recordNonCodificati;
	}

	public String getRecordCodificatiRegolari() {
		return recordCodificatiRegolari;
	}

	public void setRecordCodificatiRegolari(String recordCodificatiRegolari) {
		this.recordCodificatiRegolari = recordCodificatiRegolari;
	}

	public String getRecordCodificatiIrregolari() {
		return recordCodificatiIrregolari;
	}

	public void setRecordCodificatiIrregolari(String recordCodificatiIrregolari) {
		this.recordCodificatiIrregolari = recordCodificatiIrregolari;
	}

	public String getUtilizzazioniNonCodificate() {
		return utilizzazioniNonCodificate;
	}

	public void setUtilizzazioniNonCodificate(String utilizzazioniNonCodificate) {
		this.utilizzazioniNonCodificate = utilizzazioniNonCodificate;
	}

	public String getUtilizzazioniCodificateRegolari() {
		return utilizzazioniCodificateRegolari;
	}

	public void setUtilizzazioniCodificateRegolari(String utilizzazioniCodificateRegolari) {
		this.utilizzazioniCodificateRegolari = utilizzazioniCodificateRegolari;
	}

	public String getUtilizzazioniCodificateIrregolari() {
		return utilizzazioniCodificateIrregolari;
	}

	public void setUtilizzazioniCodificateIrregolari(String utilizzazioniCodificateIrregolari) {
		this.utilizzazioniCodificateIrregolari = utilizzazioniCodificateIrregolari;
	}

	public String getPercRecordNonCodificati() {
		return percRecordNonCodificati;
	}

	public void setPercRecordNonCodificati(String percRecordNonCodificati) {
		this.percRecordNonCodificati = percRecordNonCodificati;
	}

	public String getPercRecordCodificatiRegolari() {
		return percRecordCodificatiRegolari;
	}

	public void setPercRecordCodificatiRegolari(String percRecordCodificatiRegolari) {
		this.percRecordCodificatiRegolari = percRecordCodificatiRegolari;
	}

	public String getPercRecordCodificatiIrregolari() {
		return percRecordCodificatiIrregolari;
	}

	public void setPercRecordCodificatiIrregolari(String percRecordCodificatiIrregolari) {
		this.percRecordCodificatiIrregolari = percRecordCodificatiIrregolari;
	}

	public String getPercUtilizzazioniNonCodificate() {
		return percUtilizzazioniNonCodificate;
	}

	public void setPercUtilizzazioniNonCodificate(String percUtilizzazioniNonCodificate) {
		this.percUtilizzazioniNonCodificate = percUtilizzazioniNonCodificate;
	}

	public String getPercUtilizzazioniCodificateIrregolari() {
		return percUtilizzazioniCodificateIrregolari;
	}

	public void setPercUtilizzazioniCodificateIrregolari(String percUtilizzazioniCodificateIrregolari) {
		this.percUtilizzazioniCodificateIrregolari = percUtilizzazioniCodificateIrregolari;
	}

	public String getPercUtilizzazioniCodificateRegolari() {
		return percUtilizzazioniCodificateRegolari;
	}

	public void setPercUtilizzazioniCodificateRegolari(String percUtilizzazioniCodificateRegolari) {
		this.percUtilizzazioniCodificateRegolari = percUtilizzazioniCodificateRegolari;
	}

	public String getPercPolverizzazione() {
		return percPolverizzazione;
	}

	public void setPercPolverizzazione(String percPolverizzazione) {
		this.percPolverizzazione = percPolverizzazione;
	}

	public String getRipartizione() {
		return ripartizione;
	}

	public void setRipartizione(String ripartizione) {
		this.ripartizione = ripartizione;
	}

	public String getRipartito() {
		return ripartito;
	}

	public void setRipartito(String ripartito) {
		this.ripartito = ripartito;
	}

	public String getDataRipartizione() {
		return dataRipartizione;
	}

	public void setDataRipartizione(String dataRipartizione) {
		this.dataRipartizione = dataRipartizione;
	}

	public String getImporto() {
		return importo;
	}

	public void setImporto(String importo) {
		this.importo = importo;
	}

	public String getImportoExArt18() {
		return importoExArt18;
	}

	public void setImportoExArt18(String importoExArt18) {
		this.importoExArt18 = importoExArt18;
	}

	public String[] getMappingStrategy() {

		return new String[] { "Identificativo Report", "Licenza", "Codice Licenziatario", "Nominativo Licenziatario",
				"Servizio", "Periodo Competenza", "Versione Report", "Modalita' Utilizzazione", "Modalita' Fruizione",
				"In Abbonamento", "Stato", "Data Invio", "Data Validazione", "Data Codifica", "Record Totali",
				"Record Validi", "Utilizzazioni Valide", "Record Codificati", "Utilizzazioni Codificate",
				"Percentuale Scarto", "Percentuale Codifica Utilizzazioni", "Percentuale Codifica Report",
				"Record non Codificati", "Record Codificati Regolari", "Record Codificati Irregolari",
				"Utilizzazioni non Codificate", "Utilizzazioni Codificate Regolari",
				"Utilizzazioni Codificate Irregolari", "% Record non Codificati", "% Record Codificati Regolari",
				"% Record Codificati Irregolari", "% Utilizzazioni non Codificate",
				"% Utilizzazioni Codificate Regolari", "% Utilizzazioni Codificate Irregolari", "% Polverizzazione",
				"Ripartizione", "Importo", "Ripartito", "Data Ripartizione", "Importo Ex Articolo 18" };
	}

}
