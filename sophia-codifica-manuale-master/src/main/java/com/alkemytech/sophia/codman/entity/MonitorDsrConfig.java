package com.alkemytech.sophia.codman.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity(name = "MonitorDsrConfig")
@Table(name = "MM_MONITOR_DSR_CONFIG")
@NamedQueries({@NamedQuery(name = "MonitorDsrConfig.all", query = "select x from MonitorDsrConfig x"),
        @NamedQuery(name = "MonitorDsrConfig.searchForDsp", query = "select x from MonitorDsrConfig x where x.idDsp = :idDsp"),
        @NamedQuery(name = "MonitorDspConfig.edit", query = "update MonitorDsrConfig x set x.idDsp = :idDsp, x.territorio = :territorio," +
                " x.tipoUtilizzo = :tipoUtilizzo, x.offertaCommerciale = :offertaCommerciale," +
                " x.frequenzaDsr = :frequenzaDsr, x.slaInvioDsr = :slaInvioDsr, x.slaInvioCcid = :slaInvioCcid, x.dsrAttesi = :dsrAttesi" +
                " where x.idConfig = :idConfig")})

public class MonitorDsrConfig implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_CONFIG", nullable = false)
    private Integer idConfig;

    @Column(name = "ID_DSP", nullable = false)
    private String idDsp;

    @Column(name = "TERRITORIO", nullable = false)
    private String territorio;

    @Column(name = "TIPO_UTILIZZO", nullable = false)
    private String tipoUtilizzo;

    @Column(name = "OFFERTA_COMMERCIALE", nullable = false)
    private String offertaCommerciale;

    @Column(name = "FREQUENZA_DSR", nullable = false)
    private String frequenzaDsr;

    @Column(name = "SLA_INVIO_DSR")
    private Integer slaInvioDsr;

    @Column(name = "SLA_INVIO_CCID")
    private Integer slaInvioCcid;

    @Column(name = "DSR_ATTESI", nullable = false)
    private Integer dsrAttesi = 0;

    
}