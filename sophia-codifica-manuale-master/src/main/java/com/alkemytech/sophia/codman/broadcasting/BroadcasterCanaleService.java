package com.alkemytech.sophia.codman.broadcasting;

import com.alkemytech.sophia.broadcasting.dto.BdcAddCanaleDTO;
import com.alkemytech.sophia.broadcasting.dto.BdcNewsListRequest;
import com.alkemytech.sophia.broadcasting.model.BdcBroadcasters;
import com.alkemytech.sophia.broadcasting.model.BdcCanali;
import com.alkemytech.sophia.broadcasting.service.interfaces.BdcBroadcastersService;
import com.alkemytech.sophia.broadcasting.service.interfaces.BdcCanaliService;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

/**
 * Created by Alessandro Russo on 27/11/2017.
 */
@Singleton
@Path("broadcaster/canale")
public class BroadcasterCanaleService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private BdcBroadcastersService bdcBroadcastersService;
    private BdcCanaliService service;
    private Gson gson;

    @Inject
    public BroadcasterCanaleService(BdcCanaliService service,BdcBroadcastersService bdcBroadcastersService, Gson gson) {
        this.bdcBroadcastersService = bdcBroadcastersService;
        this.service = service;
        this.gson = gson;
    }

    @GET
    @Path("{id}")
    @Produces("application/json")
    public Response findById(@PathParam("id") Integer id) {
        try {
            return Response.ok(gson.toJson(service.findById(id))).build();
        }
        catch (Exception e) {
            logger.error(e.getMessage());
        }
        return Response.status(500).build();
    }
    
  

	@POST
    @Path("addChannel")
    @Produces("application/json")
    public Response addChannel(BdcAddCanaleDTO bdcAddCanaleDTO) {
		if (bdcAddCanaleDTO.getBdcBroadcastersId()==null||bdcAddCanaleDTO.getDataInizioValid()==null||bdcAddCanaleDTO.getNome()==null||bdcAddCanaleDTO.getUtenteUltimaModifica()==null) {
	        return Response.status(Status.BAD_REQUEST).build();
		}
		if (service.findCanaleByName(bdcAddCanaleDTO.getBdcBroadcastersId(),bdcAddCanaleDTO.getNome())) {
			return Response.status(Status.CONFLICT).build();
		}
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		Date date = new Date();
		BdcCanali bdcNewChannel= new BdcCanali();
		BdcBroadcasters bdcBroadcasters=bdcBroadcastersService.findById(bdcAddCanaleDTO.getBdcBroadcastersId());
		bdcNewChannel.setBdcBroadcasters(bdcBroadcasters);
		Date newDateInizioValid=null;
		Date newDateFineValid=null;
		
		try {
			newDateInizioValid=format.parse(bdcAddCanaleDTO.getDataInizioValid());
			bdcNewChannel.setDataInizioValid(newDateInizioValid);
		}catch (Exception e) {
			return Response.status(Status.BAD_REQUEST).build();	
		}
		try {
			newDateFineValid=format.parse(bdcAddCanaleDTO.getDataFineValid());
			bdcNewChannel.setDataFineValid(newDateFineValid);
			if (newDateInizioValid.after(newDateFineValid)) {
				return Response.status(Status.NOT_ACCEPTABLE).build();	
			}	
		}catch (Exception e) {
			bdcNewChannel.setDataFineValid(null);
		}
		bdcNewChannel.setDataCreazione(date);
		bdcNewChannel.setDataUltimaModifica(date);
		bdcNewChannel.setGeneralista(bdcAddCanaleDTO.getGeneralista()? 1:0);
		bdcNewChannel.setNome(bdcAddCanaleDTO.getNome());
		bdcNewChannel.setSpecialRadio(bdcAddCanaleDTO.getSpecialRadio()? 1:0);
		bdcNewChannel.setTipoCanale(bdcBroadcasters.getTipo_broadcaster());
		bdcNewChannel.setUtenteUltimaModifica(bdcAddCanaleDTO.getUtenteUltimaModifica());
		service.addCannel(bdcNewChannel);
        return Response.ok().build();
     
    }
	
	@POST
    @Path("updateChannel")
    @Produces("application/json")
    public Response updateChannel(BdcAddCanaleDTO bdcAddCanaleDTO) {
		if (bdcAddCanaleDTO.getBdcBroadcastersId()==null||bdcAddCanaleDTO.getDataInizioValid()==null||bdcAddCanaleDTO.getNome()==null||bdcAddCanaleDTO.getUtenteUltimaModifica()==null) {
	        return Response.status(Status.BAD_REQUEST).build();
		}
		
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.ITALIAN);
		Date date = new Date();
		BdcCanali bdcChannel= service.findById(bdcAddCanaleDTO.getBdcBroadcastersId());
		
		int specialRadio=bdcAddCanaleDTO.getSpecialRadio()? 1:0;
		int generalista=bdcAddCanaleDTO.getGeneralista()? 1:0;
		String lastDateInizioValidString="";
		String lastDateFineValidString="";
		try {
			lastDateInizioValidString=format.format(bdcChannel.getDataInizioValid());
		}catch (Exception e) {
			
		}
		try {
			lastDateFineValidString=format.format(bdcChannel.getDataFineValid());
		}catch (Exception e) {

		}

		String newDateInizioValidString="";
		String newDateFineValidString="";
		Date newDateInizioValid=null;
		Date newDateFineValid=null;
		
		try {
			newDateInizioValidString=bdcAddCanaleDTO.getDataInizioValid();
			newDateInizioValid=format.parse(bdcAddCanaleDTO.getDataInizioValid());
			bdcChannel.setDataInizioValid(newDateInizioValid);
		}catch (Exception e) {
			return Response.status(Status.BAD_REQUEST).build();	
		}
		try {
			newDateFineValidString=bdcAddCanaleDTO.getDataFineValid();
			newDateFineValid=format.parse(bdcAddCanaleDTO.getDataFineValid());
			bdcChannel.setDataFineValid(newDateFineValid);
			if (newDateInizioValid.after(newDateFineValid)) {
				return Response.status(Status.NOT_ACCEPTABLE).build();	
			}	
		}catch (Exception e) {
			newDateFineValidString="";
			bdcChannel.setDataFineValid(null);
		}
		
		if (bdcChannel.getGeneralista()==generalista&&bdcChannel.getSpecialRadio()==specialRadio
				&&newDateInizioValidString.equals(lastDateInizioValidString)
				&&newDateFineValidString.equals(lastDateFineValidString)
				&&bdcAddCanaleDTO.getNome().equals(bdcChannel.getNome())) {
			return Response.status(Status.CONFLICT).build();
		}
		
		
		bdcChannel.setDataUltimaModifica(date);
		bdcChannel.setGeneralista(bdcAddCanaleDTO.getGeneralista()? 1:0);
		bdcChannel.setNome(bdcAddCanaleDTO.getNome());
		bdcChannel.setSpecialRadio(bdcAddCanaleDTO.getSpecialRadio()? 1:0);
		bdcChannel.setUtenteUltimaModifica(bdcAddCanaleDTO.getUtenteUltimaModifica());
		int esito=service.editCannel(bdcChannel);
		if (esito!=200) {
	        return Response.status(Status.GONE).build();
		}else {
	        return Response.ok().build();
		}
     
    }
    
}
