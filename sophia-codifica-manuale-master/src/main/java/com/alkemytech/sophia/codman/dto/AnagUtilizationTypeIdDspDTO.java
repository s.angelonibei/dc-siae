package com.alkemytech.sophia.codman.dto;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.Objects;

@XmlRootElement
public class AnagUtilizationTypeIdDspDTO {

    private String idUtilizationType;
    private String name;
    private String description;
    private List<String> idDsp;

    public String getIdUtilizationType() {
        return idUtilizationType;
    }

    public AnagUtilizationTypeIdDspDTO setIdUtilizationType(String idUtilizationType) {
        this.idUtilizationType = idUtilizationType;
        return this;
    }

    public String getName() {
        return name;
    }

    public AnagUtilizationTypeIdDspDTO setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public AnagUtilizationTypeIdDspDTO setDescription(String description) {
        this.description = description;
        return this;
    }

    public List<String> getIdDsp() {
        return idDsp;
    }

    public AnagUtilizationTypeIdDspDTO setIdDsp(List<String> idDsp) {
        this.idDsp = idDsp;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AnagUtilizationTypeIdDspDTO that = (AnagUtilizationTypeIdDspDTO) o;
        return Objects.equals(idUtilizationType, that.idUtilizationType) &&
                Objects.equals(name, that.name) &&
                Objects.equals(description, that.description) &&
                Objects.equals(idDsp, that.idDsp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idUtilizationType, name, description, idDsp);
    }

    public AnagUtilizationTypeIdDspDTO(String idUtilizationType, String name, String description) {
        this.idUtilizationType = idUtilizationType;
        this.name = name;
        this.description = description;
    }

    public AnagUtilizationTypeIdDspDTO() {
    }
}
