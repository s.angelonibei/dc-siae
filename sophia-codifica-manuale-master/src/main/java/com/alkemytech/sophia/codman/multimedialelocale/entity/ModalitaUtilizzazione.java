package com.alkemytech.sophia.codman.multimedialelocale.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
@Entity(name = "ModalitaUtilizzazione")
@Table(name = "ML_MODALITA_UTILIZZAZIONE")
public class ModalitaUtilizzazione {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", nullable = false)
	private Integer id;
	@Column(name = "MODALITA")
	private String modalita;
	@Column(name = "FRUIZIONE_GRATUITA")
	private Boolean fruizioneGratuita;
	@Column(name = "FRUIZIONE_PAGAMENTO")
	private Boolean fruizionePagamento;
	@Column(name = "CANALE_UTILIZZAZIONE")
	private String canaleUtilizzazione;
	
	
	public ModalitaUtilizzazione() {
		super();
	}

	public ModalitaUtilizzazione(Integer id, String modalita, Boolean fruizioneGratuita, Boolean fruizionePagamento,
			String canaleUtilizzazione) {
		super();
		this.id = id;
		this.modalita = modalita;
		this.fruizioneGratuita = fruizioneGratuita;
		this.fruizionePagamento = fruizionePagamento;
		this.canaleUtilizzazione = canaleUtilizzazione;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getModalita() {
		return modalita;
	}
	public void setModalita(String modalita) {
		this.modalita = modalita;
	}
	public Boolean getFruizioneGratuita() {
		return fruizioneGratuita;
	}
	public void setFruizioneGratuita(Boolean fruizioneGratuita) {
		this.fruizioneGratuita = fruizioneGratuita;
	}
	public Boolean getFruizionePagamento() {
		return fruizionePagamento;
	}
	public void setFruizionePagamento(Boolean fruizionePagamento) {
		this.fruizionePagamento = fruizionePagamento;
	}
	public String getCanaleUtilizzazione() {
		return canaleUtilizzazione;
	}
	public void setCanaleUtilizzazione(String canaleUtilizzazione) {
		this.canaleUtilizzazione = canaleUtilizzazione;
	}
	
}
