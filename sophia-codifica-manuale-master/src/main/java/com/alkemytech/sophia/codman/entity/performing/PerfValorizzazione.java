package com.alkemytech.sophia.codman.entity.performing;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.codman.entity.PeriodoRipartizione;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Entity(name = "PerfValorizzazione")
@Table(name = "PERF_VALORIZZAZIONE")
public class PerfValorizzazione {

	
	@Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name = "ID_VALORIZZAZIONE", nullable = false)
	private String idValorizzazione;
	
	@Column(name = "TIPO_RIPARTIZIONE", nullable = false)
	private String ripartizioneOS;

	@OneToOne
	@JoinColumn(name = "PERIODO_RIPARTIZIONE")
	private PeriodoRipartizione periodoRipartizione;
	
	@Column(name = "VOCE_INCASSO", nullable = false)
	private String voceIncasso;
	
	@Column(name = "FORMATO_REPORT", nullable = false)
	private String tipologiaReport;

	@Column(name = "VALORIZZAZIONE", nullable = false)
	private String valorizzazione;

	@Column(name = "RIPARTIZIONE", nullable = false)
	private String ripartizione;

	@Column(name = "UTENTE_ULTIMA_MODIFICA")
	private String user;

	@Column(name = "DATA_MODIFICA")
	private Date dataUltimaModifica;

	@Column(name = "DATA_DISATTIVAZIONE")
	private Date dataDisattivazione;
	
	@Transient
	private String codice;

	@Transient
	private String competenzeDa;

	@Transient
	private String competenzeA;

	public PerfValorizzazione() {
		super();
	}

	public PerfValorizzazione(String ripartizioneOS, PeriodoRipartizione periodoRipartizione, String voceIncasso,
			String tipologiaReport, String valorizzazione, String ripartizione, String user, Date dataUltimaModifica) {
		this.ripartizioneOS = ripartizioneOS;
		this.periodoRipartizione = periodoRipartizione;
		this.voceIncasso = voceIncasso;
		this.tipologiaReport = tipologiaReport;
		this.valorizzazione = valorizzazione;
		this.ripartizione = ripartizione;
		this.user = user;
		this.dataUltimaModifica = dataUltimaModifica;
	}

	public PerfValorizzazione(Object[] result) {
    		SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    		  this.ripartizioneOS = (String) result[1]+"";
          this.tipologiaReport = (String) result[4];
          this.valorizzazione = (String) result[5];
          this.ripartizione = (String) result[6];
          this.voceIncasso = (String) result[3];
          this.user = (String) result[7];
          this.dataUltimaModifica =(Timestamp) result[8];
          try {
        	  	  PeriodoRipartizione p= new PeriodoRipartizione();
        	  	  p.setIdPeriodoRipartizione((Long) result[10]);
        	  	  p.setCodice((String) result[11]);
        	  	  p.setRepertorio((String) result[12]);
        	  	  p.setAmbito((String) result[13]);
        	  	  p.setCompetenzeDa((Date) result[14]);
        	  	  p.setCompetenzeA((Date) result[15]);
        	  	  p.setDataApprovazioneRegoleValorizzazione((Date) result[16]);
        	  	  p.setApprovatoreRegoleValorizzazione((String) result[17]);
              this.periodoRipartizione = p;
          } catch (Exception e) {
			// TODO: handle exception
          }
          
	}

	public String getRipartizioneOS() {
		return ripartizioneOS;
	}

	public void setRipartizioneOS(String ripartizioneOS) {
		this.ripartizioneOS = ripartizioneOS;
	}

	public PeriodoRipartizione getPeriodoRipartizione() {
		return periodoRipartizione;
	}

	public void setPeriodoRipartizione(PeriodoRipartizione periodoRipartizione) {
		this.periodoRipartizione = periodoRipartizione;
	}

	public String getVoceIncasso() {
		return voceIncasso;
	}

	public void setVoceIncasso(String voceIncasso) {
		this.voceIncasso = voceIncasso;
	}

	public String getTipologiaReport() {
		return tipologiaReport;
	}

	public void setTipologiaReport(String tipologiaReport) {
		this.tipologiaReport = tipologiaReport;
	}

	public String getValorizzazione() {
		return valorizzazione;
	}

	public void setValorizzazione(String valorizzazione) {
		this.valorizzazione = valorizzazione;
	}

	public String getRipartizione() {
		return ripartizione;
	}

	public void setRipartizione(String ripartizione) {
		this.ripartizione = ripartizione;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public Date getDataUltimaModifica() {
		return dataUltimaModifica;
	}

	public void setDataUltimaModifica(Date dataUltimaModifica) {
		this.dataUltimaModifica = dataUltimaModifica;
	}

	public Date getDataDisattivazione() {
		return dataDisattivazione;
	}

	public void setDataDisattivazione(Date dataDisattivazione) {
		this.dataDisattivazione = dataDisattivazione;
	}

	public String getCodice() {
		return codice;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	public String getCompetenzeDa() {
		return competenzeDa;
	}

	public void setCompetenzeDa(String competenzeDa) {
		this.competenzeDa = competenzeDa;
	}

	public String getCompetenzeA() {
		return competenzeA;
	}

	public void setCompetenzeA(String competenzeA) {
		this.competenzeA = competenzeA;
	}

	@Override
	public String toString() {
		return "PerfValorizzazione{" + "ripartizioneOS='" + ripartizioneOS + '\'' + ", periodoRipartizione='"
				+ periodoRipartizione + '\'' + ", voceIncasso='" + voceIncasso + '\'' + ", tipologiaReport='"
				+ tipologiaReport + '\'' + ", valorizzazione=" + valorizzazione + ", ripartizione='" + ripartizione
				+ '\'' + ", user='" + user + '\'' + ", dataUltimaModifica='" + dataUltimaModifica + '\'' + '}';
	}
}
