package com.alkemytech.sophia.codman.rest.performing.service;

import com.alkemytech.sophia.codman.entity.performing.CampionamentoConfig;
import com.alkemytech.sophia.codman.entity.performing.security.ReportPage;
//import com.alkemytech.sophia.codman.entity.performing.security.User;

import java.util.List;

/**
 * Created by idilello on 6/21/16.
 */
public interface ICampionamentoService {

    public void insertParametriCampionamento(CampionamentoConfig campionamentoConfig);


    public ReportPage getCampionamenti(Long contabilitaIniziale, Long contabilitaFinale, Integer page);
    

    public ReportPage getCampionamentiEsecuzione(Long contabilitaIniziale, Long contabilitaFinale, Integer page);

    public CampionamentoConfig getCampionamentoConfigById(Long idCampionamentoConfig);

    public ReportPage getStoricoEsecuzioni(Long periodoIniziale, Long periodoFinale, Integer page);

}
