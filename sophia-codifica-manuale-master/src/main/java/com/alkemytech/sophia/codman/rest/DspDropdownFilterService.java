package com.alkemytech.sophia.codman.rest;

import com.alkemytech.sophia.codman.dto.DropdownDTO;
import com.alkemytech.sophia.codman.dto.DspDropdownFilterDTO;
import com.alkemytech.sophia.codman.dto.StatisticheDropdownsDTO;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.util.List;

@Singleton
@Path("filter")
public class DspDropdownFilterService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Provider<EntityManager> provider;
	private final Gson gson;

	@Inject
	protected DspDropdownFilterService(@McmdbDataSource Provider<EntityManager> provider, Gson gson) {
		super();
		this.provider = provider;
		this.gson = gson;
	}

	@POST
	@Path("utilizationType")
	@Produces("application/json")
	public Response getUtilizationTypes(DspDropdownFilterDTO dspDropdownFilterDTO) {
		EntityManager entityManager;
		StatisticheDropdownsDTO result;
		try {
			result = new StatisticheDropdownsDTO();
			entityManager = provider.get();
			final Query q = entityManager.createNamedQuery("AnagUtilizationType.GetByIdDsp", DropdownDTO.class);
			result.setUtilizations(q.setParameter("idDsps",dspDropdownFilterDTO.getIdDsp()).getResultList());
			result.setCommercialOffers(_getOffers(dspDropdownFilterDTO));
			return Response.ok(gson.toJson(result)).build();
		} catch (Exception e) {
			logger.error("utilizationType", e);
		}
		return Response.status(500).build();
	}

	@POST
	@Path("offer")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response getOffers(DspDropdownFilterDTO dspDropdownFilterDTO) {
		StatisticheDropdownsDTO result;
		try {
			result = new StatisticheDropdownsDTO();
			result.setCommercialOffers ( _getOffers(dspDropdownFilterDTO));
			return Response.ok(gson.toJson(result)).build();
		} catch (Exception e) {
			logger.error("offer", e);
		}
		return Response.status(500).build();
	}

	private List<DropdownDTO> _getOffers(DspDropdownFilterDTO dspDropdownFilterDTO){
		EntityManager entityManager = provider.get();
		if(dspDropdownFilterDTO.getIdUtilizationType()!=null && dspDropdownFilterDTO.getIdUtilizationType().size()>0){
			if(dspDropdownFilterDTO.getIdDsp()!=null && dspDropdownFilterDTO.getIdDsp().size()>0) {
				Query q = entityManager.createNamedQuery("CommercialOffers.GetOfferingsByIdDspAndidUtilizationType", DropdownDTO.class);
				return q.setParameter("idDsps", dspDropdownFilterDTO.getIdDsp()).setParameter("offerings", dspDropdownFilterDTO.getIdUtilizationType()).getResultList();
			}else{
				Query q = entityManager.createNamedQuery("CommercialOffers.GetOfferingsByIdUtilizationType", DropdownDTO.class);
				return q.setParameter("offerings", dspDropdownFilterDTO.getIdUtilizationType()).getResultList();
			}
		}else{
			Query q = entityManager.createNamedQuery("CommercialOffers.GetOfferingsByIdDsp", DropdownDTO.class);
			return q.setParameter("idDsps", dspDropdownFilterDTO.getIdDsp()).getResultList();
		}

	}
}
