package com.alkemytech.sophia.codman.gestioneperiodi;

import java.util.List;

import com.alkemytech.sophia.broadcasting.dto.Periodo;

public interface GestionePeriodiInterface {
	
	// Input 2016 RIPARTIZIONE 
	// LISTA DI PERIODI RELATIVA A QUEL ANNO PER QUELL'AMBITO
	// Output Periodo (DATA FINE E INIZIO VALORIZZATE)
	public List<Periodo> getPeriodi(Integer anno,String ambito);
	
	// Input 1° TRIM 2016
	// Output Periodo (DATA FINE E INIZIO VALORIZZATE)
	public Periodo beutyfyDate(String periodo) throws Exception;
}
