package com.alkemytech.sophia.codman.service.dsrmetadata.period;

public interface DsrPeriodExtractor {

	public DsrPeriod extractDsrPeriod(String period); 
	
	public String getCode();
	
	public String getDescription();
	
	public String getRegex();
}
