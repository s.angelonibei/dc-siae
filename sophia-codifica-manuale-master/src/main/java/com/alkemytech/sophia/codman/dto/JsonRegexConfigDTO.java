package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;

import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;

@SuppressWarnings("serial")
@Produces("application/json")
@XmlRootElement 
public class JsonRegexConfigDTO implements Serializable {

	
	private int commercialOfferId;
	private String idDsp;
	private String regexString;
	private String regexStringValues;
	
	public JsonRegexConfigDTO(){
		
	}
	
	public int getCommercialOfferId() {
		return commercialOfferId;
	}
	public void setCommercialOfferId(int commercialOfferId) {
		this.commercialOfferId = commercialOfferId;
	}
	
	public String getIdDsp() {
		return idDsp;
	}

	public void setIdDsp(String idDsp) {
		this.idDsp = idDsp;
	}

	public String getRegexString() {
		return regexString;
	}

	public void setRegexString(String regexString) {
		this.regexString = regexString;
	}

	public String getRegexStringValues() {
		return regexStringValues;
	}

	public void setRegexStringValues(String regexStringValues) {
		this.regexStringValues = regexStringValues;
	}


	
}
