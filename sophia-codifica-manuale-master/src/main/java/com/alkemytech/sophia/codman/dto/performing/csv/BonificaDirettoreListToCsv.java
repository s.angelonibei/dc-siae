package com.alkemytech.sophia.codman.dto.performing.csv;

import java.util.Date;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

public class BonificaDirettoreListToCsv {
	
	@CsvBindByPosition(position = 0)
    @CsvBindByName
    String origineRecord;
	@CsvBindByPosition(position = 1)
    @CsvBindByName
	Long idProgrammaMusicale;
	@CsvBindByPosition(position = 2)
    @CsvBindByName
	String siglaProvinciaNascita;
	@CsvBindByPosition(position = 3)
    @CsvBindByName
	String siglaProvincia;
	@CsvBindByPosition(position = 4)
    @CsvBindByName
	String sesso;
	@CsvBindByPosition(position = 5)
    @CsvBindByName
	String posizioneSiae;
	@CsvBindByPosition(position = 6)
    @CsvBindByName
	Long numeroProgramaMusicale;
	@CsvBindByPosition(position = 7)
    @CsvBindByName
	String indirizzo;
	@CsvBindByPosition(position = 8)
    @CsvBindByName
	String direttoreEsecuzione;
	@CsvBindByPosition(position = 9)
    @CsvBindByName
	Date dataNascita;
	@CsvBindByPosition(position = 10)
    @CsvBindByName
	String comuneNascita;
	@CsvBindByPosition(position = 11)
    @CsvBindByName
	String comune;
	@CsvBindByPosition(position = 12)
    @CsvBindByName
	String codiceFiscale;
	@CsvBindByPosition(position = 13)
    @CsvBindByName
	String civicoResidenza;
	@CsvBindByPosition(position = 141)
    @CsvBindByName
	String cap;

	public BonificaDirettoreListToCsv(Object[] row) {
		super();
		this.origineRecord = (String)row[0];
		this.idProgrammaMusicale = (Long)row[1];
		this.siglaProvinciaNascita = (String)row[2];
		this.siglaProvincia = (String)row[3];
		this.sesso = (String)row[4];
		this.posizioneSiae = (String)row[5];
		this.numeroProgramaMusicale = (Long)row[6];
		this.indirizzo = (String)row[7];
		this.direttoreEsecuzione = (String)row[8];
		this.dataNascita = (Date)row[9];
		this.comuneNascita = (String)row[10];
		this.comune = (String)row[11];
		this.codiceFiscale = (String)row[12];
		this.civicoResidenza = (String)row[13];
		this.cap = (String)row[14];
	}

	public BonificaDirettoreListToCsv() {
		super();
	}

	public String getOrigineRecord() {
		return origineRecord;
	}

	public void setOrigineRecord(String origineRecord) {
		this.origineRecord = origineRecord;
	}

	public Long getIdProgrammaMusicale() {
		return idProgrammaMusicale;
	}

	public void setIdProgrammaMusicale(Long idProgrammaMusicale) {
		this.idProgrammaMusicale = idProgrammaMusicale;
	}

	public Long getNumeroProgramaMusicale() {
		return numeroProgramaMusicale;
	}

	public void setNumeroProgramaMusicale(Long numeroProgramaMusicale) {
		this.numeroProgramaMusicale = numeroProgramaMusicale;
	}

	public String getDirettoreEsecuzione() {
		return direttoreEsecuzione;
	}

	public void setDirettoreEsecuzione(String direttoreEsecuzione) {
		this.direttoreEsecuzione = direttoreEsecuzione;
	}

	public String getSesso() {
		return sesso;
	}

	public void setSesso(String sesso) {
		this.sesso = sesso;
	}

	public String getPosizioneSiae() {
		return posizioneSiae;
	}

	public void setPosizioneSiae(String posizioneSiae) {
		this.posizioneSiae = posizioneSiae;
	}

	public String getComuneNascita() {
		return comuneNascita;
	}

	public void setComuneNascita(String comuneNascita) {
		this.comuneNascita = comuneNascita;
	}

	public String getSiglaProvinciaNascita() {
		return siglaProvinciaNascita;
	}

	public void setSiglaProvinciaNascita(String siglaProvinciaNascita) {
		this.siglaProvinciaNascita = siglaProvinciaNascita;
	}

	public Date getDataNascita() {
		return dataNascita;
	}

	public void setDataNascita(Date dataNascita) {
		this.dataNascita = dataNascita;
	}

	public String getCodiceFiscale() {
		return codiceFiscale;
	}

	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}

	public String getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}

	public String getCivicoResidenza() {
		return civicoResidenza;
	}

	public void setCivicoResidenza(String civicoResidenza) {
		this.civicoResidenza = civicoResidenza;
	}

	public String getSiglaProvincia() {
		return siglaProvincia;
	}

	public void setSiglaProvincia(String siglaProvincia) {
		this.siglaProvincia = siglaProvincia;
	}

	public String getCap() {
		return cap;
	}

	public void setCap(String cap) {
		this.cap = cap;
	}



	public String getComune() {
		return comune;
	}

	public void setComune(String comune) {
		this.comune = comune;
	}

	public String[] getMappingStrategy() {
        return new String[]{
        		"Origine Record",
        		"Id programma musicale",
        		"Sigla provincia di nascita",
        		"Sigla provincia",
    			"Sesso",
        		"Posizione siae",
        		"Numero programma musicale",
        		"Indirizzo",
        		"Direttore esecuzione",
        		"Data di nascita",
        		"Comune di nascita",
        		"Comune",
        		"Codice fiscale",
        		"Civico resdenza",
        		"cap"
        };
        
	}

}
