package com.alkemytech.sophia.codman.broadcasting;


import com.alkemytech.sophia.broadcasting.dto.BdcNewsListRequest;
import com.alkemytech.sophia.broadcasting.model.BdcNews;
import com.alkemytech.sophia.broadcasting.service.interfaces.BdcNewsService;
import com.alkemytech.sophia.codman.rest.PagedResult;
import com.alkemytech.sophia.common.beantocsv.CustomMappingStrategy;
import com.amazonaws.util.CollectionUtils;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Date;
import java.util.List;

/**
 * Created by Alessandro Ravà on 12/12/2017.
 */
@Singleton
@Path("news")
public class NewsRestService {

	private final static String NO_RESULTS = "No results found";

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private BdcNewsService service;
	private Gson gson;

	@Inject
	public NewsRestService(BdcNewsService service, Gson gson) {
		this.service = service;
		this.gson = gson;
	}

	@POST
	@Path("allnews")
	@Produces("application/json")
	public Response findAll(BdcNewsListRequest bdcNewsListRequest) {
		try {
			List<BdcNews> result = service.findAllExplose(bdcNewsListRequest,
					bdcNewsListRequest.getFirst(), bdcNewsListRequest.getLast());
			if (!CollectionUtils.isNullOrEmpty(result)) {
				final PagedResult pagedResult = new PagedResult();
				if (null != result && !result.isEmpty()) {
					final int maxrows = bdcNewsListRequest.getLast() - bdcNewsListRequest.getFirst();
					final boolean hasNext = result.size() > maxrows;
					pagedResult.setRows(!hasNext ? result : result.subList(0, maxrows)).setMaxrows(maxrows)
							.setFirst(bdcNewsListRequest.getFirst())
							.setLast(hasNext ? bdcNewsListRequest.getLast()
									: bdcNewsListRequest.getFirst() + result.size())
							.setHasNext(hasNext).setHasPrev(bdcNewsListRequest.getFirst() > 0);
				} else {
					pagedResult.setMaxrows(0).setFirst(0).setLast(0).setHasNext(false).setHasPrev(false);
				}
				return Response.ok(gson.toJson(pagedResult)).build();
			} else {
				return Response.ok(gson.toJson(NO_RESULTS)).build();
			}
		} catch (Exception e) {
			logger.error("findAll", e);
		}
		return Response.status(500).build();
	}

	@POST
	@Path("downloadAllnews")
	@Produces("application/csv")
	public Response downloadAllnews(BdcNewsListRequest bdcNewsListRequest) {
		try {
			List<BdcNews> result = service.findAllExplose(bdcNewsListRequest);
			if (!CollectionUtils.isNullOrEmpty(result)) {
				try {
					if (result.size() != 0) {
						StreamingOutput so = null;
						CustomMappingStrategy<BdcNews> customMappingStrategy = new CustomMappingStrategy<>(
								new BdcNews().getMappingStrategy());
						customMappingStrategy.setType(BdcNews.class);
						so = getStreamingOutput(result, customMappingStrategy);
						Response response = Response.ok(so, "text/csv").header("Content-Disposition", "attachment; filename=\""
								+ "Lista News.csv\"").build();
						return response;
					} else {
						return Response.status(500).entity("Non risulta nessun dato per il periodo selezionato").build();
					}
				} catch (Exception e) {
					logger.error(e.getMessage());
					return Response.status(500).entity("Non risulta nessun dato per il periodo selezionato").build();
				} finally {

				}
			} else {
				return Response.ok(gson.toJson(NO_RESULTS)).build();
			}
		} catch (Exception e) {
			logger.error("findAll", e);
		}
		return Response.status(500).build();
	}

//
//	public static void main(String ...args){
//        DateTime dt = DateTime.now().withMillisOfDay(0);
//        System.out.println(dt.toDate());
//    }

	@POST
	@Path("addNews")
	@Produces("application/json")
	public Response addNews(BdcNews bdcNewsDto) {
		try {

			DateTime dt = DateTime.now().withMillisOfDay(0);
			if (bdcNewsDto.getInsertDate() == null) {
				bdcNewsDto.setInsertDate(new Date());
			}
			if (bdcNewsDto.getValidFrom() == null) {
				bdcNewsDto.setValidFrom(new Date());
			}
			if (bdcNewsDto.getValidTo() == null) {
				Date date = new Date();
				date.setTime(4133890800000l);
				bdcNewsDto.setValidTo(date);
			}
			if(bdcNewsDto.getValidFrom().before(dt.toDate())){
				return Response.status(400).entity(gson.toJson("'Validità da' non può essere inferiore a oggi")).build();
			}
			if(bdcNewsDto.getValidTo().before(dt.toDate())){
				return Response.status(400).entity(gson.toJson("'Validità a' non può essere inferiore a oggi")).build();
			}

			if(bdcNewsDto.getValidFrom().after(bdcNewsDto.getValidTo())){
				return Response.status(400).entity(gson.toJson("'Validità a' non può essere inferiore a 'Validità da'")).build();
			}

			if (bdcNewsDto.getBdcBroadcasters() != null) {
				bdcNewsDto.setBdcBroadcasters(bdcNewsDto.getBdcBroadcasters());
				bdcNewsDto.setFlagForAllBdc(false);
				if (bdcNewsDto.getIdUtente() != null) {
					bdcNewsDto.setIdUtente(bdcNewsDto.getIdUtente());
					bdcNewsDto.setFlagForAllBdcUser(false);
				} else {
					bdcNewsDto.setIdUtente(null);
					bdcNewsDto.setFlagForAllBdcUser(true);
				}
			} else {
				bdcNewsDto.setBdcBroadcasters(null);
				bdcNewsDto.setFlagForAllBdc(true);
				if (bdcNewsDto.getIdUtente() != null) {
					bdcNewsDto.setIdUtente(bdcNewsDto.getIdUtente());
					bdcNewsDto.setFlagForAllBdcUser(false);
				} else {
					bdcNewsDto.setIdUtente(null);
					bdcNewsDto.setFlagForAllBdcUser(true);
				}
			}
			bdcNewsDto.setFlagActive(true);
			service.addNews(bdcNewsDto);
			return Response.ok().build();
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return Response.status(500).build();
		}
	}

	@POST
	@Path("deleteNews")
	@Produces("application/json")
	public Response deleteNews(BdcNews bdcNewsDto) {
		try {
			service.deleteNews(bdcNewsDto);
			return Response.ok().build();
		} catch (Exception e) {
			return Response.status(500).build();
		}

	}

	@POST
	@Path("deactivateNews")
	@Produces("application/json")
	public Response deactivateNews(BdcNews bdcNewsDto) {
		try {
			service.deactivateNews(bdcNewsDto);
			return Response.ok().build();
		} catch (Exception e) {
			return Response.status(500).build();
		}

	}

	@POST
	@Path("activateNews")
	@Produces("application/json")
	public Response activateNews(BdcNews bdcNewsDto) {
		try {
			service.activateNews(bdcNewsDto);
			return Response.ok().build();
		} catch (Exception e) {
			return Response.status(500).build();
		}

	}

	@POST
	@Path("updateNews")
	@Produces("application/json")
	public Response updateNews(BdcNews bdcNewsDto) {
		try {
			DateTime dt = DateTime.now().withMillisOfDay(0);
			if (bdcNewsDto.getInsertDate() == null) {
				bdcNewsDto.setInsertDate(new Date());
			}
			if (bdcNewsDto.getValidFrom() == null) {
				bdcNewsDto.setValidFrom(new Date());
			}
			if (bdcNewsDto.getValidTo() == null) {
				Date date = new Date();
				date.setTime(4133890800000l);
				bdcNewsDto.setValidTo(date);
			}
			if(bdcNewsDto.getValidFrom().before(dt.toDate())){
				return Response.status(400).entity(gson.toJson("'Validità da' non può essere inferiore a oggi")).build();
			}
			if(bdcNewsDto.getValidTo().before(dt.toDate())){
				return Response.status(400).entity(gson.toJson("'Validità a' non può essere inferiore a oggi")).build();
			}

			if(bdcNewsDto.getValidFrom().after(bdcNewsDto.getValidTo())){
				return Response.status(400).entity(gson.toJson("'Validità a' non può essere inferiore a 'Validità da'")).build();
			}

			if (bdcNewsDto.getBdcBroadcasters() != null) {
				bdcNewsDto.setBdcBroadcasters(bdcNewsDto.getBdcBroadcasters());
				bdcNewsDto.setFlagForAllBdc(false);
				if (bdcNewsDto.getIdUtente() != null) {
					bdcNewsDto.setIdUtente(bdcNewsDto.getIdUtente());
					bdcNewsDto.setFlagForAllBdcUser(false);
				} else {
					bdcNewsDto.setIdUtente(null);
					bdcNewsDto.setFlagForAllBdcUser(true);
				}
			} else {
				bdcNewsDto.setBdcBroadcasters(null);
				bdcNewsDto.setFlagForAllBdc(true);
				if (bdcNewsDto.getIdUtente() != null) {
					bdcNewsDto.setIdUtente(bdcNewsDto.getIdUtente());
					bdcNewsDto.setFlagForAllBdcUser(false);
				} else {
					bdcNewsDto.setIdUtente(null);
					bdcNewsDto.setFlagForAllBdcUser(true);
				}
			}
			service.updateNews(bdcNewsDto);
			return Response.ok().build();
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return Response.status(500).build();
		}
	}

	private <T> StreamingOutput getStreamingOutput(final List<T> obj, final CustomMappingStrategy mappingStrategy) {
		StreamingOutput so = new StreamingOutput() {
			@Override
			public void write(OutputStream outputStream) throws IOException, WebApplicationException {
				OutputStreamWriter osw = new OutputStreamWriter(outputStream, "UTF-8");

				StatefulBeanToCsv beanToCsv = new StatefulBeanToCsvBuilder(osw).withMappingStrategy(mappingStrategy)
						.withSeparator(';').build();
				try {
					beanToCsv.write(obj);
					osw.flush();
					outputStream.flush();
				} catch (CsvDataTypeMismatchException e) {
					logger.error(e.getMessage());
				} catch (CsvRequiredFieldEmptyException e) {
					logger.error(e.getMessage());
				} finally {
					if (osw != null)
						osw.close();
				}
			}
		};
		return so;
	}
}
