package com.alkemytech.sophia.codman.dto.performing.csv;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;
import com.opencsv.bean.CsvNumber;

import java.math.BigDecimal;




public class RiconciliazioneImportiCsv {
	
    @CsvBindByPosition(position = 0)
    @CsvBindByName
    private Integer periodo;
    @CsvBindByPosition(position = 1)
    @CsvBindByName
    private String numeroFattura;
    @CsvBindByPosition(position = 2)
    @CsvBindByName
    private String voceIncasso;
    @CsvBindByPosition(position = 3, locale = "it-IT")
    @CsvBindByName(locale = "it-IT")
    @CsvNumber("0,00")
    private BigDecimal lordoSophia;
    @CsvBindByPosition(position = 4, locale = "it-IT")
    @CsvBindByName(locale = "it-IT")
    @CsvNumber("0,00")
    private BigDecimal lordoNdm;
    @CsvBindByPosition(position = 5)
    @CsvBindByName
    private String semaforo;
    @CsvBindByPosition(position = 6, locale = "it-IT")
    @CsvBindByName(locale = "it-IT")
    @CsvNumber("0,00")
    private BigDecimal nettoSophia;
    @CsvBindByPosition(position = 7, locale = "it-IT")
    @CsvBindByName(locale = "it-IT")
    @CsvNumber("0,00")
    private BigDecimal nettoNdm;
    @CsvBindByPosition(position = 8, locale = "it-IT")
    @CsvBindByName(locale = "it-IT")
    @CsvNumber("0,00")
    private BigDecimal percAggioNdm;
    @CsvBindByPosition(position = 9, locale = "it-IT")
    @CsvBindByName(locale = "it-IT")
    @CsvNumber("0,00")
    private BigDecimal nettoSophiaArt;
    @CsvBindByPosition(position = 10)
    @CsvBindByName
    private String sede;
    @CsvBindByPosition(position = 11)
    @CsvBindByName
    private String agenzia;
    @CsvBindByPosition(position = 12)
    @CsvBindByName
    private String seprag;

   /* public RiconciliazioneImportiCsv(Long contabilita, String numeroFattura, String voceIncasso, BigDecimal totaleLordoSophia, BigDecimal totaleLordoNDM, String semaforo, BigDecimal nettoSophia, BigDecimal nettoNDM, BigDecimal percentualeAggioNDM, BigDecimal totaleNettoSophia, String sede, String agenzia, String seprag) {
        this.contabilita = contabilita;
        this.numeroFattura = numeroFattura;
        this.voceIncasso = voceIncasso;
        this.totaleLordoSophia = totaleLordoSophia;
        this.totaleLordoNDM = totaleLordoNDM;
        this.semaforo = semaforo;
        this.nettoSophia = nettoSophia;
        this.nettoNDM = nettoNDM;
        this.percentualeAggioNDM = percentualeAggioNDM;
        this.totaleNettoSophia = totaleNettoSophia;
        this.sede = sede;
        this.agenzia = agenzia;
        this.seprag = seprag;
    }*/

    public Integer getPeriodo() {
        return periodo;
    }

    public void setPeriodo(Integer periodo) {
        this.periodo = periodo;
    }

    public String getNumeroFattura() {
        return numeroFattura;
    }

    public void setNumeroFattura(String numeroFattura) {
        this.numeroFattura = numeroFattura;
    }

    public String getVoceIncasso() {
        return voceIncasso;
    }

    public void setVoceIncasso(String voceIncasso) {
        this.voceIncasso = voceIncasso;
    }

    public BigDecimal getLordoSophia() {
        return lordoSophia;
    }

    public void setLordoSophia(BigDecimal lordoSophia) {
        this.lordoSophia = lordoSophia;
    }

    public BigDecimal getLordoNdm() {
        return lordoNdm;
    }

    public void setLordoNdm(BigDecimal lordoNdm) {
        this.lordoNdm = lordoNdm;
    }

    public String getSemaforo() {
        return semaforo;
    }

    public void setSemaforo(String semaforo) {
        this.semaforo = semaforo;
    }

    public BigDecimal getNettoSophia() {
        return nettoSophia;
    }

    public void setNettoSophia(BigDecimal nettoSophia) {
        this.nettoSophia = nettoSophia;
    }

    public BigDecimal getNettoNdm() {
        return nettoNdm;
    }

    public void setNettoNdm(BigDecimal nettoNdm) {
        this.nettoNdm = nettoNdm;
    }

    public BigDecimal getPercAggioNdm() {
        return percAggioNdm;
    }

    public void setPercAggioNdm(BigDecimal percAggioNdm) {
        this.percAggioNdm = percAggioNdm;
    }

    public BigDecimal getNettoSophiaArt() {
        return nettoSophiaArt;
    }

    public void setNettoSophiaArt(BigDecimal nettoSophiaArt) {
        this.nettoSophiaArt = nettoSophiaArt;
    }

    public String[] getMappingStrategy() {
        return new String[]{

                "Contabilita",
                "Numero Fattura",
                "Voce Incasso",
                "Lordo Sophia",
                "Lordo NDM",
                "Fattura Validata",
                "Sophia Netto Aggio",
                "NDM Netto Aggio",
                "Percentuale Aggio NDM",
                "Netto Sophia",
                "Sede",
                "Agenzia",
                "Seprag"
        };
    }
}
