package com.alkemytech.sophia.codman.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity(name="SqsServiceBus")
@Table(name="SQS_SERVICE_BUS")
public class SqsServiceBus {
	
	@Id
	@GeneratedValue
	@Column(name="ID", nullable=false)
	private Long id;
	
	@Column(name="QUEUE_NAME", nullable=false)
	private String queueName;
	
	@Column(name="INSERT_TIME", nullable=false)
	private Date insertTime;
	
	@Column(name="MESSAGE_TIME", nullable=true)
	private Date messageTime;

	@Column(name="MESSAGE_JSON", nullable=true)
	private String messageJson;
	
	@Column(name="ENVIRONMENT", nullable=true)
	private String environment;

	@Column(name="QUEUE_TYPE", nullable=true)
	private String queueType;
	
	@Column(name="SERVICE_NAME", nullable=true)
	private String serviceName;

	@Column(name="SENDER", nullable=true)
	private String sender;

	@Column(name="HOSTNAME", nullable=true)
	private String hostname;

	@Column(name="IDDSR", nullable=true)
	private String idDsr;
	
	@Column(name="IDDSP", nullable=true)
	private String idDsp;

	@Column(name="YEAR", nullable=true)
	private String year;

	@Column(name="MONTH", nullable=true)
	private String month;

	@Column(name="COUNTRY", nullable=true)
	private String country;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getQueueName() {
		return queueName;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

	public Date getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(Date insertTime) {
		this.insertTime = insertTime;
	}

	public Date getMessageTime() {
		return messageTime;
	}

	public void setMessageTime(Date messageTime) {
		this.messageTime = messageTime;
	}

	public String getMessageJson() {
		return messageJson;
	}

	public void setMessageJson(String messageJson) {
		this.messageJson = messageJson;
	}

	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	public String getQueueType() {
		return queueType;
	}

	public void setQueueType(String queueType) {
		this.queueType = queueType;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String getIdDsr() {
		return idDsr;
	}

	public void setIdDsr(String idDsr) {
		this.idDsr = idDsr;
	}

	public String getIdDsp() {
		return idDsp;
	}

	public void setIdDsp(String idDsp) {
		this.idDsp = idDsp;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
}
