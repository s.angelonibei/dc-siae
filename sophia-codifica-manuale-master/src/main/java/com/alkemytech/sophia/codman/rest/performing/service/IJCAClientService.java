package com.alkemytech.sophia.codman.rest.performing.service;

import java.util.List;

/**
 * Created by idilello on 6/13/16.
 */
public interface IJCAClientService {

    public String sendCommand(String command);

    public List<String> sendMultipleEngineCommand(String command);

}
