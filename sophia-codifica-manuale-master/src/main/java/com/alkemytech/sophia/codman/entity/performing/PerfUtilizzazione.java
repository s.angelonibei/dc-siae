package com.alkemytech.sophia.codman.entity.performing;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;


/**
 * The persistent class for the PERF_UTILIZZAZIONE database table.
 */
@Entity
@Table(name = "PERF_UTILIZZAZIONE")
@NamedQueries({
        @NamedQuery(name = "PerfUtilizzazione.findAll", query = "SELECT p FROM PerfUtilizzazione p"),
        @NamedQuery(name = "PerfUtilizzazione.findAllByIdCodifica", query = "SELECT p FROM PerfUtilizzazione p where p.perfCodifica.idCodifica = :idCodifica"),
        @NamedQuery(name = "PerfUtilizzazione.findAllByIdCombana", query = "SELECT p FROM PerfUtilizzazione p where p.perfCombana.idCombana = :idCombana"),
        @NamedQuery(name = "PerfUtilizzazione.findAllByCodiceOpera", query = "SELECT p FROM PerfUtilizzazione p where p.codiceOpera = :codiceOpera"),
        @NamedQuery(name = "PerfUtilizzazione.findAllByCodiceOperaAndIdCombana", query = "SELECT p FROM PerfUtilizzazione p where p.codiceOpera = :codiceOpera and p.perfCombana.idCombana = :idCombana"),
})
public class PerfUtilizzazione implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID_UTILIZZAZIONE", unique = true, nullable = false)
    private String idUtilizzazione;

    @Column(name = "AUTORI", length = 200)
    private String autori;

    @Column(name = "CODICE_OPERA", length = 20)
    private String codiceOpera;

    @Column(name = "COMBINAZIONE_CODIFICA", length = 200)
    private String combinazioneCodifica;

    @CsvBindByPosition(position = 2)
    @CsvBindByName
    @Column(name = "COMPOSITORE", length = 500)
    private String compositore;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_INS", nullable = false)
    private Date dataIns;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_PRIMA_CODIFICA")
    private Date dataPrimaCodifica;

    @CsvBindByPosition(position = 3)
    @CsvBindByName
    @Column(name = "DURATA")
    private BigInteger durata;

    @Column(name = "DURATA_INFERIORE_30SEC", length = 1)
    @CsvBindByPosition(position = 4)
    @CsvBindByName
    private String durataInferiore30sec;

    @Column(name = "FLAG_CODIFICA", length = 1)
    private String flagCodifica;

    @Column(name = "FLAG_MAG_NON_CONCESSA", length = 1)
    private String flagMagNonConcessa;

    @CsvBindByPosition(position = 7)
    @CsvBindByName
    @Column(name = "FLAG_PUBBLICO_DOMINIO", length = 1)
    private String flagPubblicoDominio;

    @Column(name = "ID_PROGRAMMA_MUSICALE")
    private BigInteger idProgrammaMusicale;

    @CsvBindByPosition(position = 6)
    @CsvBindByName
    @Column(name = "INTERPRETI", length = 500)
    private String interpreti;

    @CsvBindByPosition(position = 8)
    @CsvBindByName
    @Column(name = "NETTO_MATURATO", precision = 10, scale = 7)
    private BigDecimal nettoMaturato;

    @Column(name = "NUMERO_PROGRAMMA_MUSICALE")
    private BigInteger numeroProgrammaMusicale;

    @Column(name = "PUNTI_ORIGINALI", precision = 10, scale = 7)
    private BigDecimal puntiOriginali;

    @Column(name = "PUNTI_RICALCOLATI", precision = 10, scale = 7)
    private BigDecimal puntiRicalcolati;

    @Column(name = "TIPO_ACCERTAMENTO", length = 200)
    private String tipoAccertamento;

    @CsvBindByPosition(position = 1)
    @CsvBindByName
    @Column(name = "TITOLO_COMPOSIZIONE", length = 200)
    private String titoloComposizione;

    //bi-directional many-to-one association to PerfCodificaDTO
    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_CODIFICA")
    private PerfCodifica perfCodifica;

    //bi-directional many-to-one association to PerfCombana
    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_COMBANA")
    private PerfCombana perfCombana;

    @Column(name = "FLAG_CEDOLA_ESCLUSA", length = 1)
    private String flagCedolaEsclusa;


    public PerfUtilizzazione() {
    }

    public String getIdUtilizzazione() {
        return this.idUtilizzazione;
    }

    public void setIdUtilizzazione(String idUtilizzazione) {
        this.idUtilizzazione = idUtilizzazione;
    }

    public String getAutori() {
        return this.autori;
    }

    public void setAutori(String autori) {
        this.autori = autori;
    }

    public String getCodiceOpera() {
        return this.codiceOpera;
    }

    public void setCodiceOpera(String codiceOpera) {
        this.codiceOpera = codiceOpera;
    }

    public String getCombinazioneCodifica() {
        return this.combinazioneCodifica;
    }

    public void setCombinazioneCodifica(String combinazioneCodifica) {
        this.combinazioneCodifica = combinazioneCodifica;
    }

    public String getCompositore() {
        return this.compositore;
    }

    public void setCompositore(String compositore) {
        this.compositore = compositore;
    }

    public Date getDataIns() {
        return this.dataIns;
    }

    public void setDataIns(Date dataIns) {
        this.dataIns = dataIns;
    }

    public Date getDataPrimaCodifica() {
        return this.dataPrimaCodifica;
    }

    public void setDataPrimaCodifica(Date dataPrimaCodifica) {
        this.dataPrimaCodifica = dataPrimaCodifica;
    }

    public BigInteger getDurata() {
        return this.durata;
    }

    public void setDurata(BigInteger durata) {
        this.durata = durata;
    }

    public String getDurataInferiore30sec() {
        return this.durataInferiore30sec;
    }

    public void setDurataInferiore30sec(String durataInferiore30sec) {
        this.durataInferiore30sec = durataInferiore30sec;
    }

    public String getFlagCodifica() {
        return this.flagCodifica;
    }

    public void setFlagCodifica(String flagCodifica) {
        this.flagCodifica = flagCodifica;
    }

    public String getFlagMagNonConcessa() {
        return this.flagMagNonConcessa;
    }

    public void setFlagMagNonConcessa(String flagMagNonConcessa) {
        this.flagMagNonConcessa = flagMagNonConcessa;
    }

    public String getFlagPubblicoDominio() {
        return this.flagPubblicoDominio;
    }

    public void setFlagPubblicoDominio(String flagPubblicoDominio) {
        this.flagPubblicoDominio = flagPubblicoDominio;
    }

    public BigInteger getIdProgrammaMusicale() {
        return this.idProgrammaMusicale;
    }

    public void setIdProgrammaMusicale(BigInteger idProgrammaMusicale) {
        this.idProgrammaMusicale = idProgrammaMusicale;
    }

    public String getInterpreti() {
        return this.interpreti;
    }

    public void setInterpreti(String interpreti) {
        this.interpreti = interpreti;
    }

    public BigDecimal getNettoMaturato() {
        return this.nettoMaturato;
    }

    public void setNettoMaturato(BigDecimal nettoMaturato) {
        this.nettoMaturato = nettoMaturato;
    }

    public BigInteger getNumeroProgrammaMusicale() {
        return this.numeroProgrammaMusicale;
    }

    public void setNumeroProgrammaMusicale(BigInteger numeroProgrammaMusicale) {
        this.numeroProgrammaMusicale = numeroProgrammaMusicale;
    }

    public BigDecimal getPuntiOriginali() {
        return this.puntiOriginali;
    }

    public void setPuntiOriginali(BigDecimal puntiOriginali) {
        this.puntiOriginali = puntiOriginali;
    }

    public BigDecimal getPuntiRicalcolati() {
        return this.puntiRicalcolati;
    }

    public void setPuntiRicalcolati(BigDecimal puntiRicalcolati) {
        this.puntiRicalcolati = puntiRicalcolati;
    }

    public String getTipoAccertamento() {
        return this.tipoAccertamento;
    }

    public void setTipoAccertamento(String tipoAccertamento) {
        this.tipoAccertamento = tipoAccertamento;
    }

    public String getTitoloComposizione() {
        return this.titoloComposizione;
    }

    public void setTitoloComposizione(String titoloComposizione) {
        this.titoloComposizione = titoloComposizione;
    }

    public PerfCodifica getPerfCodifica() {
        return this.perfCodifica;
    }

    public void setPerfCodifica(PerfCodifica perfCodifica) {
        this.perfCodifica = perfCodifica;
    }

    public PerfCombana getPerfCombana() {
        return this.perfCombana;
    }

    public void setPerfCombana(PerfCombana perfCombana) {
        this.perfCombana = perfCombana;
    }

    public String getFlagCedolaEsclusa() {
        return flagCedolaEsclusa;
    }

    public void setFlagCedolaEsclusa(String flagCedolaEsclusa) {
        this.flagCedolaEsclusa = flagCedolaEsclusa;
    }
}