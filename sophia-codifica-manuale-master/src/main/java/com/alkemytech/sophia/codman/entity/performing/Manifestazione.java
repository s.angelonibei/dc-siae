package com.alkemytech.sophia.codman.entity.performing;

import com.alkemytech.sophia.codman.utils.DateUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "PERF_MANIFESTAZIONE")
public class Manifestazione  implements Serializable {

    private Long id;
    private String codiceLocale;
    private String codiceComune;
    private String denominazioneLocale;
    private String denominazioneLocalita;
    private Date dataInizioEvento;
    private Date dataFineEvento;
    private String oraInizioEvento;
    private String oraFineEvento;
    private Long totaleMinuti;
    private String siglaProvinciaLocale;
    private String codiceSapOrganizzatore;
    private Long numeroFatture;
    private String siglaLocale;
    private String partitaIvaOrganizzatore;
    private String codiceFiscaleOrganizzatore;
	private String nomeOrganizzatore;
	private String codiceBALocale;
	private String idProgrammaMusicale;
	private String seprag;
    private String comuneLocale;
    private String titolo;
    private String titoloOriginale;
    private boolean flagMegaConcert;
    
    
	@Id
    @Column(name = "ID_EVENTO")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "CODICE_LOCALE")
    public String getCodiceLocale() {
        return codiceLocale;
    }

    public void setCodiceLocale(String codiceLocale) {
        this.codiceLocale = codiceLocale;
    }

    @Column(name = "CODICE_COMUNE")
    public String getCodiceComune() {
        return codiceComune;
    }

    public void setCodiceComune(String codiceComune) {
        this.codiceComune = codiceComune;
    }


    @Column(name = "DENOMINAZIONE_LOCALE")
    public String getDenominazioneLocale() {
        return denominazioneLocale;
    }

    public void setDenominazioneLocale(String denominazioneLocale) {
        this.denominazioneLocale = denominazioneLocale;
    }

    @Column(name = "DENOMINAZIONE_LOCALITA")
    public String getDenominazioneLocalita() {
        return denominazioneLocalita;
    }

    public void setDenominazioneLocalita(String denominazioneLocalita) {
        this.denominazioneLocalita = denominazioneLocalita;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_INIZIO_EVENTO")
    public Date getDataInizioEvento() {
        return dataInizioEvento;
    }

    public void setDataInizioEvento(Date dataInizioEvento) {
        this.dataInizioEvento = dataInizioEvento;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_FINE_EVENTO")
    public Date getDataFineEvento() {
        return dataFineEvento;
    }

    public void setDataFineEvento(Date dataFineEvento) {
        this.dataFineEvento = dataFineEvento;
    }



    @Column(name = "ORA_INIZIO")
    public String getOraInizioEvento() {
        return oraInizioEvento;
    }

    public void setOraInizioEvento(String oraInizioEvento) {
        this.oraInizioEvento = oraInizioEvento;
    }

    @Column(name = "ORA_FINE")
    public String getOraFineEvento() {
        return oraFineEvento;
    }

    public void setOraFineEvento(String oraFineEvento) {
        this.oraFineEvento = oraFineEvento;
    }

    @Column(name = "TOTALE_MINUTI")
    public Long getTotaleMinuti() {
        return totaleMinuti;
    }

    public void setTotaleMinuti(Long totaleMinuti) {
        this.totaleMinuti = totaleMinuti;
    }


    @Column(name = "SIGLA_PROVINCIA_LOCALE")
    public String getSiglaProvinciaLocale() {
        return siglaProvinciaLocale;
    }

    public void setSiglaProvinciaLocale(String siglaProvinciaLocale) {
        this.siglaProvinciaLocale = siglaProvinciaLocale;
    }

    @Column(name = "CODICE_SAP_ORGANIZZATORE")
    public String getCodiceSapOrganizzatore() {
        return codiceSapOrganizzatore;
    }

    public void setCodiceSapOrganizzatore(String codiceSapOrganizzatore) {
        this.codiceSapOrganizzatore = codiceSapOrganizzatore;
    }

    @Column(name = "NUMERO_FATTURE")
    public Long getNumeroFatture() {
        return numeroFatture;
    }

    public void setNumeroFatture(Long numeroFatture) {
        this.numeroFatture = numeroFatture;
    }

    @Column(name = "SIGLA_LOCALE")
    public String getSiglaLocale() {
		return siglaLocale;
	}

	public void setSiglaLocale(String siglaLocale) {
		this.siglaLocale = siglaLocale;
	}

    @Column(name = "PARTITA_IVA_ORGANIZZATORE")
    public String getPartitaIvaOrganizzatore() {
		return partitaIvaOrganizzatore;
	}

	public void setPartitaIvaOrganizzatore(String partitaIvaOrganizzatore) {
		this.partitaIvaOrganizzatore = partitaIvaOrganizzatore;
	}

    @Column(name = "CODICE_FISCALE_ORGANIZZATORE")
	public String getCodiceFiscaleOrganizzatore() {
		return codiceFiscaleOrganizzatore;
	}

	public void setCodiceFiscaleOrganizzatore(String codiceFiscaleOrganizzatore) {
		this.codiceFiscaleOrganizzatore = codiceFiscaleOrganizzatore;
	}

    @Column(name = "NOME_ORGANIZZATORE")
	public String getNomeOrganizzatore() {
		return nomeOrganizzatore;
	}

	public void setNomeOrganizzatore(String nomeOrganizzatore) {
		this.nomeOrganizzatore = nomeOrganizzatore;
	}
	
	@Column(name = "CODICE_BA_LOCALE")
    public String getCodiceBALocale() {
		return codiceBALocale;
	}

	public void setCodiceBALocale(String codiceBALocale) {
		this.codiceBALocale = codiceBALocale;
	}

	@Column(name = "ID_PROGRAMMA_MUSICALE")
	public String getIdProgrammaMusicale() {
		return idProgrammaMusicale;
	}

	public void setIdProgrammaMusicale(String idProgrammaMusicale) {
		this.idProgrammaMusicale = idProgrammaMusicale;
	}

	@Column(name = "COMUNE_LOCALE")
	public String getComuneLocale() {
		return comuneLocale;
	}

	public void setComuneLocale(String comuneLocale) {
		this.comuneLocale = comuneLocale;
	}

	@Transient
    public String getDataInizioEventoStr() {
//    	return Utility.dateString(dataInizioEvento, Utility.OracleFormat_D);
  		return DateUtils.dateToString(dataInizioEvento,DateUtils.DATE_ITA_FORMAT_SLASH);
    }

    @Transient
    public String getDataFineEventoStr() {
//        return Utility.dateString(dataFineEvento, Utility.OracleFormat_D);
    	return DateUtils.dateToString(dataFineEvento,DateUtils.DATE_ITA_FORMAT_SLASH);
    }

    @Transient
	public String getSeprag() {
		return seprag;
	}

	public void setSeprag(String seprag) {
		this.seprag = seprag;
	}

	@Column(name = "TITOLO")
	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	@Column(name = "TITOLO_ORIGINALE")
	public String getTitoloOriginale() {
		return titoloOriginale;
	}

	public void setTitoloOriginale(String titoloOriginale) {
		this.titoloOriginale = titoloOriginale;
	}
	
	@Column(name = "FLAG_MEGACONCERT")
	public boolean getFlagMegaConcert() {
		return flagMegaConcert;
	}

	public void setFlagMegaConcert(boolean flagMegaConcert) {
		this.flagMegaConcert = flagMegaConcert;
	}
	
	
    
}
