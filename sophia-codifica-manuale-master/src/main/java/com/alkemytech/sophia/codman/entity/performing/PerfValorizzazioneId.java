package com.alkemytech.sophia.codman.entity.performing;

import java.io.Serializable;

import com.alkemytech.sophia.codman.entity.PeriodoRipartizione;

@SuppressWarnings("serial")
public class PerfValorizzazioneId implements Serializable {

    private String voceIncasso;
    private String tipologiaReport;

    public PerfValorizzazioneId() {
        super();
    }

    public PerfValorizzazioneId(PeriodoRipartizione periodoRipartizione, String voceIncasso, String tipologiaReport) {
   
        this.voceIncasso = voceIncasso;
        this.tipologiaReport = tipologiaReport;
    }


    public String getVoceIncasso() {
        return voceIncasso;
    }

    public void setVoceIncasso(String voceIncasso) {
        this.voceIncasso = voceIncasso;
    }

    public String getFormatoReport() {
        return tipologiaReport;
    }

    public void setFormatoReport(String formatoReport) {
        this.tipologiaReport = formatoReport;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PerfValorizzazioneId that = (PerfValorizzazioneId) o;

        if (!voceIncasso.equals(that.voceIncasso)) return false;
        return tipologiaReport.equals(that.tipologiaReport);
    }

    @Override
    public int hashCode() {
        int result = 0;
        result = 31 * result + voceIncasso.hashCode();
        result = 31 * result + tipologiaReport.hashCode();
        return result;
    }
}
