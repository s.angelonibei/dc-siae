package com.alkemytech.sophia.codman.dto.performing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class RiepilogoCodificaManualeResponse implements Serializable {
    private Date periodoRipartizioneDa;
    private Date periodoRipartizioneA;
    private String nomeOperatore;
    private long numeroUtilizzazioniCodificate;
    private BigDecimal valoreEconomicoSbloccato;
    private long numeroUtilizzazioniScodificate;
    private BigDecimal valoreEconomico;

    public RiepilogoCodificaManualeResponse() {
    }

    public RiepilogoCodificaManualeResponse(String nomeOperatore, Long numeroUtilizzazioniCodificate, BigDecimal valoreEconomicoSbloccato) {
        this.nomeOperatore = nomeOperatore;
        this.numeroUtilizzazioniCodificate = numeroUtilizzazioniCodificate;
        this.valoreEconomicoSbloccato = valoreEconomicoSbloccato;
    }

    public Date getPeriodoRipartizioneDa() {
        return periodoRipartizioneDa;
    }

    public void setPeriodoRipartizioneDa(Date periodoRipartizioneDa) {
        this.periodoRipartizioneDa = periodoRipartizioneDa;
    }

    public Date getPeriodoRipartizioneA() {
        return periodoRipartizioneA;
    }

    public void setPeriodoRipartizioneA(Date periodoRipartizioneA) {
        this.periodoRipartizioneA = periodoRipartizioneA;
    }

    public String getNomeOperatore() {
        return nomeOperatore;
    }

    public void setNomeOperatore(String nomeOperatore) {
        this.nomeOperatore = nomeOperatore;
    }

    public long getNumeroUtilizzazioniCodificate() {
        return numeroUtilizzazioniCodificate;
    }

    public void setNumeroUtilizzazioniCodificate(long numeroUtilizzazioniCodificate) {
        this.numeroUtilizzazioniCodificate = numeroUtilizzazioniCodificate;
    }

    public long getNumeroUtilizzazioniScodificate() {
        return numeroUtilizzazioniScodificate;
    }

    public void setNumeroUtilizzazioniScodificate(long numeroUtilizzazioniScodificate) {
        this.numeroUtilizzazioniScodificate = numeroUtilizzazioniScodificate;
    }

    public BigDecimal getValoreEconomicoSbloccato() {
        return valoreEconomicoSbloccato;
    }

    public void setValoreEconomicoSbloccato(BigDecimal valoreEconomicoSbloccato) {
        this.valoreEconomicoSbloccato = valoreEconomicoSbloccato;
    }

    public BigDecimal getValoreEconomico() {
        return valoreEconomico;
    }

    public void setValoreEconomico(BigDecimal valoreEconomico) {
        this.valoreEconomico = valoreEconomico;
    }
}
