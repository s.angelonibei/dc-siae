package com.alkemytech.sophia.codman.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.common.collect.Iterables;

@Entity
@Table(name="PERF_CONFIGURATION")
@NamedQueries({
	@NamedQuery(name = "Configuration.getConfigurations", query = "SELECT c FROM Configuration c WHERE (:key = '' OR c.key = :key) AND c.domain = :domain AND c.topLevel = true AND c.active = true ORDER BY c.validTo DESC"),
	@NamedQuery(name = "Configuration.findConfiguration", query = "SELECT c FROM Configuration c WHERE c.id = :id"),
	@NamedQuery(name = "Configuration.historyConfigurations", query = "SELECT c FROM Configuration c WHERE c.key = :key AND c.domain = :domain AND c.topLevel = true AND c.active = false ORDER BY c.validTo DESC")	
})
@XmlRootElement
public class Configuration implements Serializable, Comparable<Configuration>, Iterable<Setting>{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CONFIGURATION_ID")
	protected Long id;
	
	private String domain;	
	
	@Column(name = "CONF_KEY")
	private String key;
	
	@Column(name = "CONF_LABEL")
	private String label;
	
	@Column(name = "ACTIVE")
	private Boolean active = true;
	
	@Column(name = "TOP_LEVEL")
	private Boolean topLevel = false;
	
	@Column(name = "CREATED_BY")
	private String createdBy;
	
	@Column(name = "EDITED_BY")
	private String editedBy;
	
	@Column(name = "VALID_FROM")
	@Temporal(TemporalType.TIMESTAMP)
	private Date validFrom;
	
	@Column(name = "VALID_TO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date validTo;
	
	@Column(name = "CREATED")
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;
	
	@Column(name = "MODIFIED")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modified;
	
	@ManyToMany(cascade = CascadeType.PERSIST)
	@JoinTable(name = "PERF_CONFIGURATION_SETTINGS", 
	joinColumns = { @JoinColumn(name = "CONFIGURATION_ID") }, 
	inverseJoinColumns = {
			@JoinColumn(name = "SETTINGS_ID") })
	@OrderBy("ordinal ASC")
	private List<Setting> settings = new ArrayList<Setting>();
	
	@ManyToMany(cascade = CascadeType.PERSIST)
	@JoinTable(name = "PERF_CONFIGURATION_CONFIGURATIONS", 
	joinColumns = { @JoinColumn(name = "CONFIGURATION_ID") }, 
	inverseJoinColumns = {
			@JoinColumn(name = "CONFIGURATIONS_ID") })
	@OrderBy("label ASC")
	private List<Configuration> configurations = new ArrayList<Configuration>();
	
	@Transient
	private Configuration relatedConfiguration;
	
	@Transient
	private List<String> errorMessages = new ArrayList<String>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Boolean getTopLevel() {
		return topLevel;
	}

	public void setTopLevel(Boolean topLevel) {
		this.topLevel = topLevel;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getEditedBy() {
		return editedBy;
	}

	public void setEditedBy(String editedBy) {
		this.editedBy = editedBy;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getModified() {
		return modified;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}

	public List<Setting> getSettings() {
		return settings;
	}

	public void setSettings(List<Setting> settings) {
		this.settings = settings;
	}

	public List<Configuration> getConfigurations() {
		return configurations;
	}

	public void setConfigurations(List<Configuration> configurations) {
		this.configurations = configurations;
	}

	@Override
	public int compareTo(Configuration o) {
		return getLabel().compareTo(o.getLabel());
	}
	
	public Configuration getInnerConfiguration(String key) {
		for (Configuration c : configurations) {
			if (c.getKey().equals(key))
				return c;
		}
		return null;
	}

	public<T extends Setting> T getSetting(String key, Class<T> type) {
		for (Setting s : settings)
			if (s.getClass()== type && s.getKey().equals(key)) {
				return (T) s;
			}
		return null;
	}

    @Override
    public Iterator<Setting> iterator() {
    	Iterable<Setting> concat = Iterables.concat(Iterables.concat(configurations),Iterables.concat(settings));
    	return concat.iterator();
    }

	public List<String> getErrorMessages() {
		return errorMessages;
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public Configuration getRelatedConfiguration() {
		return relatedConfiguration;
	}

	public void setRelatedConfiguration(Configuration relatedConfiguration) {
		this.relatedConfiguration = relatedConfiguration;
	}

}
