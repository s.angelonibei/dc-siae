package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class CCIDServiceDTO implements Serializable{
	
	private static final long serialVersionUID = 4568634915111432169L;
	
	private String status;
	private String errorMessage;
	
	private Boolean encoded;
	private Boolean encodedProvisional;
	private Boolean notEncoded;
	private String ccidVersion;
	private BigDecimal conversionRate;
	private String ccidCurrency;
	private String dsrCurrency;
	private BigDecimal drmSplit;
	private BigDecimal demSplit;
	private String tradingBrand;
	private String serviceType;
	private String useType;
	private String appliedTariff;
	private String sender;
	private String ccidId;
	private String workCodeType;
	private String receiver;

	
	
	public CCIDServiceDTO(){
		
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public Boolean getEncoded() {
		return encoded;
	}
	public void setEncoded(Boolean encoded) {
		this.encoded = encoded;
	}
	public Boolean getEncodedProvisional() {
		return encodedProvisional;
	}
	public void setEncodedProvisional(Boolean encodedProvisional) {
		this.encodedProvisional = encodedProvisional;
	}
	public Boolean getNotEncoded() {
		return notEncoded;
	}
	public void setNotEncoded(Boolean notEncoded) {
		this.notEncoded = notEncoded;
	}

	public String getCcidVersion() {
		return ccidVersion;
	}

	public void setCcidVersion(String ccidVersion) {
		this.ccidVersion = ccidVersion;
	}


	public BigDecimal getConversionRate() {
		return conversionRate;
	}

	public void setConversionRate(BigDecimal conversionRate) {
		this.conversionRate = conversionRate;
	}

	public String getCcidCurrency() {
		return ccidCurrency;
	}

	public void setCcidCurrency(String ccidCurrency) {
		this.ccidCurrency = ccidCurrency;
	}

	public String getDsrCurrency() {
		return dsrCurrency;
	}

	public void setDsrCurrency(String dsrCurrency) {
		this.dsrCurrency = dsrCurrency;
	}

	public BigDecimal getDrmSplit() {
		return drmSplit;
	}

	public void setDrmSplit(BigDecimal drmSplit) {
		this.drmSplit = drmSplit;
	}

	public BigDecimal getDemSplit() {
		return demSplit;
	}

	public void setDemSplit(BigDecimal demSplit) {
		this.demSplit = demSplit;
	}

	public String getTradingBrand() {
		return tradingBrand;
	}

	public void setTradingBrand(String tradingBrand) {
		this.tradingBrand = tradingBrand;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getUseType() {
		return useType;
	}

	public void setUseType(String useType) {
		this.useType = useType;
	}

	public String getAppliedTariff() {
		return appliedTariff;
	}

	public void setAppliedTariff(String appliedTariff) {
		this.appliedTariff = appliedTariff;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getCcidId() {
		return ccidId;
	}

	public void setCcidId(String ccidId) {
		this.ccidId = ccidId;
	}

	public String getWorkCodeType() {
		return workCodeType;
	}

	public void setWorkCodeType(String workCodeType) {
		this.workCodeType = workCodeType;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	
}
