package com.alkemytech.sophia.codman.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Table(name = "MM_MONITORAGGIO_648")
@Data
@Entity
@NamedQueries({@NamedQuery(name = "MmMonitoraggio.updateOnFailed", query = "update MmMonitoraggio648 set STATO = :stato" +
        ", dataAggiornamento = :dataAggiornamento, errDescr = :errDescr" +
        " where IDDSR = :IDDSR"),
@NamedQuery(name = "MmMonitoraggio.findByDsrSocieta", query = "select m from MmMonitoraggio648 m where m.IDDSR = :IDDSR")})
public class MmMonitoraggio648 implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "IDDSR", nullable = false)
    private String IDDSR;

    @Id
    @Column(name = "SOCIETA", nullable = false)
    private String SOCIETA;

    @Column(name = "IDDSP")
    private String IDDSP;

    @Column(name = "DATA_INSERIMENTO")
    private Timestamp dataInserimento;

    @Column(name = "DATA_AGGIORNAMENTO")
    private Timestamp dataAggiornamento;

    @Column(name = "STATO")
    private String STATO;

    @Column(name = "ERR_DESCR")
    private String errDescr;

    @Column(name = "VALORE_CLAIM_DEM")
    private Float valoreClaimDem;

    @Column(name = "VALORE_CLAIM_DRM")
    private Float valoreClaimDrm;

    
}