package com.alkemytech.sophia.codman.entity.performing;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PERF_NDM_VOCE_SCOMPOSTA")
public class NDMVoceScomposta implements Serializable{

	private Long id;
	private Long idNdmVoceFattura;
	private Integer sequenza;
	private String tipoPenale;
	private BigDecimal importoOriginale;
	private BigDecimal importoRipartito;
	private BigDecimal percentualeAggio;
	private BigDecimal importoAggio;
	

	@Id
    @Column(name = "ID_NDM_VOCE_SCOMPOSTA")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
    @Column(name = "ID_NDM_VOCE_FATTURA")
	public Long getIdNdmVoceFattura() {
		return idNdmVoceFattura;
	}
	public void setIdNdmVoceFattura(Long idNdmVoceFattura) {
		this.idNdmVoceFattura = idNdmVoceFattura;
	}
    @Column(name = "SEQUENZA")
	public Integer getSequenza() {
		return sequenza;
	}
	public void setSequenza(Integer sequenza) {
		this.sequenza = sequenza;
	}
    @Column(name = "TIPO_PENALE")
	public String getTipoPenale() {
		return tipoPenale;
	}
	public void setTipoPenale(String tipoPenale) {
		this.tipoPenale = tipoPenale;
	}
    @Column(name = "IMPORTO_ORIGINARIO")
	public BigDecimal getImportoOriginale() {
		return importoOriginale;
	}
	public void setImportoOriginale(BigDecimal importoOriginale) {
		this.importoOriginale = importoOriginale;
	}
    @Column(name = "IMPORTO_RIPARTITO")
	public BigDecimal getImportoRipartito() {
		return importoRipartito;
	}
	public void setImportoRipartito(BigDecimal importoRipartito) {
		this.importoRipartito = importoRipartito;
	}
    @Column(name = "PERCENTUALE_AGGIO")
	public BigDecimal getPercentualeAggio() {
		return percentualeAggio;
	}
	public void setPercentualeAggio(BigDecimal percentualeAggio) {
		this.percentualeAggio = percentualeAggio;
	}
    @Column(name = "IMPORTO_AGGIO")
	public BigDecimal getImportoAggio() {
		return importoAggio;
	}
	public void setImportoAggio(BigDecimal importoAggio) {
		this.importoAggio = importoAggio;
	}
	
	
	
}
