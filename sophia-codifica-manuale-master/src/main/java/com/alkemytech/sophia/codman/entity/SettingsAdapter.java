package com.alkemytech.sophia.codman.entity;

import java.util.Collection;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class SettingsAdapter extends XmlAdapter<AdapterSettings, Setting> {

    @Override
    public Setting unmarshal(AdapterSettings as) throws Exception {
    	String valueType = as.getValueType();
		if ("key_value".equals(as.getSettingType())) {
			KeyValueProperty kv = new KeyValueProperty();
			kv.setId(as.getId());
			kv.setKey(as.getKey());
			kv.setSettingType(as.getSettingType());
			kv.setOrdinal(as.getOrdinal());
			kv.setLabel(as.getLabel());
			kv.setValueType(valueType);
			String value = as.getValue();
			value = convertIfBoolean(valueType, value);
			kv.setValue(value);
			return kv;
		} else if ("threshold".equals(as.getSettingType())) {
			Threshold ts = new Threshold();
			ts.setId(as.getId());
			ts.setKey(as.getKey());
			ts.setSettingType(as.getSettingType());
			ts.setOrdinal(as.getOrdinal());
			ts.setLabel(as.getLabel());
			ts.setValueType(valueType);
			String value = as.getValue();
			value = convertIfBoolean(valueType, value);
			ts.setValue(value);			
			ts.setRelation(Relation.valueOf(as.getRelation()));
			ts.setCaption(as.getCaption());
			return ts;
		} else if ("threshold_set".equals(as.getSettingType())) {
			ThresholdSet ts = new ThresholdSet();
			ts.setId(as.getId());
			ts.setKey(as.getKey());
			ts.setSettingType(as.getSettingType());
			ts.setOrdinal(as.getOrdinal());
			ts.setLabel(as.getLabel());
			ts.setValueType(valueType);
			String value = as.getValue();
			value = convertIfBoolean(valueType, value);
			ts.setValue(value);			
			Collection<AdapterSettings> thresholds = as.getThresholds();
			for (AdapterSettings threshold : thresholds) {
				ts.getThresholds().add(unmarshal(threshold));
			}
			return ts;
		} else {
			Setting s = new Setting();
			s.setId(as.getId());
			s.setKey(as.getKey());
			s.setSettingType(as.getSettingType());
			s.setOrdinal(as.getOrdinal());
			return s;
		}
    }

	protected String convertIfBoolean(String valueType, String value) {
		if("Boolean".equals(valueType)) {
			if(Boolean.parseBoolean(value)) {
				value = "1";
			}else {
				value = "0";
			}
		}
		return value;
	}

    @Override
    public AdapterSettings marshal(Setting s) throws Exception {
        AdapterSettings as = new AdapterSettings();
        as.setId(s.getId());
        as.setKey(s.getKey());
        as.setSettingType(s.getSettingType());
        as.setOrdinal(s.getOrdinal());        
        if(KeyValueProperty.class == s.getClass()) {
        	KeyValueProperty kv = (KeyValueProperty) s;
			as.setLabel(kv.getLabel());
			as.setValueType(kv.getValueType());
			as.setValue(kv.getValue());
        } else if (Threshold.class == s.getClass()){
        	Threshold ts = (Threshold) s;
			as.setLabel(ts.getLabel());
			as.setValueType(ts.getValueType());
			as.setValue(ts.getValue());			
			as.setRelation(ts.getRelation().toString());
			as.setCaption(ts.getCaption());
        }else if (ThresholdSet.class == s.getClass()) {
        	ThresholdSet ts = (ThresholdSet) s;
			as.setLabel(ts.getLabel());
			as.setValueType(ts.getValueType());
			as.setValue(ts.getValue());
			Collection<Setting> thresholds = ts.getThresholds();
			for (Setting threshold : thresholds) {
				as.getThresholds().add(marshal(threshold));
			}
        }
        return as;
    }

}