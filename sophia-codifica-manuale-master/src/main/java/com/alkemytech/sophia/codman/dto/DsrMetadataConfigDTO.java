package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;

public class DsrMetadataConfigDTO implements Serializable {

	private static final long serialVersionUID = -1363529334155386059L;

	private int idDsrMetadataConfig;

	private String dsp;
	private String idDsp;

	private String utilizationType;
	private String idUtilizationType;

	private String commercialOffer;
	private int idCommercialOffer;

	private String regEx;
	private String regExRepresentation;
	private String jsonRegExConfig;

	private String countryExtractorCode;
	private String periodTypeExtractorCode;

	private int startIndexCountry;
	private int endIndexCountry;

	private int startIndexPeriod;
	private int endIndexPeriod;
	private String dspCode;

	public DsrMetadataConfigDTO() {

	}

	public DsrMetadataConfigDTO(Object[] obj) {

		if (null != obj && obj.length == 17) {
			try {
				
				 idDsrMetadataConfig = (Integer) obj[0];
				 idCommercialOffer = (Integer) obj[1];
				 idDsp = (String) obj[2];
				 dsp = (String) obj[3];
				 idUtilizationType = (String) obj[4];
				 utilizationType = (String) obj[5];
				 commercialOffer = (String) obj[6];
				 regEx = (String) obj[7];
				 jsonRegExConfig = (String) obj[8];
				 regExRepresentation = (String) obj[9];
				 countryExtractorCode = (String) obj[10];
				 periodTypeExtractorCode = (String) obj[11];
				 startIndexCountry = (Integer) obj[12];
				 endIndexCountry = (Integer) obj[13];
				 startIndexPeriod = (Integer) obj[14] ;
				 endIndexPeriod = (Integer) obj[15];
				 dspCode = (String) obj[16];
				 

			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	public int getIdDsrMetadataConfig() {
		return idDsrMetadataConfig;
	}

	public void setIdDsrMetadataConfig(int idDsrMetadataConfig) {
		this.idDsrMetadataConfig = idDsrMetadataConfig;
	}

	public String getDsp() {
		return dsp;
	}

	public void setDsp(String dsp) {
		this.dsp = dsp;
	}

	public String getIdDsp() {
		return idDsp;
	}

	public void setIdDsp(String idDsp) {
		this.idDsp = idDsp;
	}

	public String getUtilizationType() {
		return utilizationType;
	}

	public void setUtilizationType(String utilizationType) {
		this.utilizationType = utilizationType;
	}

	public String getIdUtilizationType() {
		return idUtilizationType;
	}

	public void setIdUtilizationType(String idUtilizationType) {
		this.idUtilizationType = idUtilizationType;
	}

	public String getCommercialOffer() {
		return commercialOffer;
	}

	public void setCommercialOffer(String commercialOffer) {
		this.commercialOffer = commercialOffer;
	}

	public int getIdCommercialOffer() {
		return idCommercialOffer;
	}

	public void setIdCommercialOffer(int idCommercialOffer) {
		this.idCommercialOffer = idCommercialOffer;
	}

	public String getRegEx() {
		return regEx;
	}

	public void setRegEx(String regEx) {
		this.regEx = regEx;
	}

	public String getRegExRepresentation() {
		return regExRepresentation;
	}

	public void setRegExRepresentation(String regExRepresentation) {
		this.regExRepresentation = regExRepresentation;
	}

	public String getJsonRegExConfig() {
		return jsonRegExConfig;
	}

	public void setJsonRegExConfig(String jsonRegExConfig) {
		this.jsonRegExConfig = jsonRegExConfig;
	}

	public String getCountryExtractorCode() {
		return countryExtractorCode;
	}

	public void setCountryExtractorCode(String countryExtractorCode) {
		this.countryExtractorCode = countryExtractorCode;
	}

	public String getPeriodTypeExtractorCode() {
		return periodTypeExtractorCode;
	}

	public void setPeriodTypeExtractorCode(String periodTypeExtractorCode) {
		this.periodTypeExtractorCode = periodTypeExtractorCode;
	}

	public int getStartIndexCountry() {
		return startIndexCountry;
	}

	public void setStartIndexCountry(int startIndexCountry) {
		this.startIndexCountry = startIndexCountry;
	}

	public int getEndIndexCountry() {
		return endIndexCountry;
	}

	public void setEndIndexCountry(int endIndexCountry) {
		this.endIndexCountry = endIndexCountry;
	}

	public int getStartIndexPeriod() {
		return startIndexPeriod;
	}

	public void setStartIndexPeriod(int startIndexPeriod) {
		this.startIndexPeriod = startIndexPeriod;
	}

	public int getEndIndexPeriod() {
		return endIndexPeriod;
	}

	public void setEndIndexPeriod(int endIndexPeriod) {
		this.endIndexPeriod = endIndexPeriod;
	}

	public String getDspCode() {
		return dspCode;
	}

	public void setDspCode(String dspCode) {
		this.dspCode = dspCode;
	}



}
