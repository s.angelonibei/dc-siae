package com.alkemytech.sophia.codman.entity.performing.dao;

import java.util.List;

import com.alkemytech.sophia.codman.entity.performing.PerfCodificaMassiva;

public interface ICodificaMassivaDAO {

    public PerfCodificaMassiva getPerfCodificaMassiva(Integer uuid);

    public PerfCodificaMassiva save(PerfCodificaMassiva perfCodificaMassiva);

	public Integer getUploadedFilesCount();

	public List<PerfCodificaMassiva> getUploadedFiles(String order, Integer page);

}
