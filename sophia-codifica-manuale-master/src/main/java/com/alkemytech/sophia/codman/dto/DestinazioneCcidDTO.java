package com.alkemytech.sophia.codman.dto;

import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Produces("application/json")
@XmlRootElement
public class DestinazioneCcidDTO implements Serializable {

    private Long id;
    private String idDsp;
    private String host;
    private String user;
    private String dsp;
    private String idUtilizationType;
    private String utilizationType;
    private String idCommercialOffer;
    private String commercialOffer;
    private String url;
    private Integer rank;
    
    public DestinazioneCcidDTO(){}

    public DestinazioneCcidDTO(Object[] a) {
        int i =0;
        this.id = (Long)a[i++];
        this.host = (String)a[i++];
        this.user = (String)a[i++];
        this.idDsp = (String)a[i++];
        this.dsp = (String)a[i++];
        this.idUtilizationType = (String)a[i++];
        this.utilizationType = (String)a[i++];
        this.idCommercialOffer = (String)a[i++];
        this.commercialOffer = (String)a[i++];
        this.url = (String)a[i++];
        this.rank = (Integer)a[i++];
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdDsp() {
        return idDsp;
    }

    public void setIdDsp(String idDsp) {
        this.idDsp = idDsp;
    }

    public String getDsp() {
        return dsp;
    }

    public void setDsp(String dsp) {
        this.dsp = dsp;
    }

    public String getIdUtilizationType() {
        return idUtilizationType;
    }

    public void setIdUtilizationType(String idUtilizationType) {
        this.idUtilizationType = idUtilizationType;
    }

    public String getUtilizationType() {
        return utilizationType;
    }

    public void setUtilizationType(String utilizationType) {
        this.utilizationType = utilizationType;
    }

    public String getIdCommercialOffer() {
        return idCommercialOffer;
    }

    public void setIdCommercialOffer(String idCommercialOffer) {
        this.idCommercialOffer = idCommercialOffer;
    }

    public String getCommercialOffer() {
        return commercialOffer;
    }

    public void setCommercialOffer(String commercialOffer) {
        this.commercialOffer = commercialOffer;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}