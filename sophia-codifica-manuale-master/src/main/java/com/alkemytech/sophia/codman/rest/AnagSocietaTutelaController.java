package com.alkemytech.sophia.codman.rest;

import java.io.ByteArrayOutputStream;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.codman.dto.SocietaTutelaDTO;
import com.alkemytech.sophia.codman.mandato.service.ISocietaTutelaService;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
@Path("societa-tutela")
public class AnagSocietaTutelaController {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private Gson gson;
	
	private ISocietaTutelaService societaTutelaService;

	@Inject
	protected AnagSocietaTutelaController(ISocietaTutelaService societaTutelaService, Gson gson) {
		super();
		this.gson = gson;
		this.societaTutelaService = societaTutelaService;
	}

	@GET
	@Produces("application/json")
	public Response getAll() {
		try {
			List<SocietaTutelaDTO> results = societaTutelaService.getAll();
			return Response.ok(gson.toJson(results)).build();
		} catch (Exception e) {
			logger.error("Errore recupero societa tutela", e);
			return Response.status(500).build();
		}
	}

	@GET
	@Path("getSocietaList")
	@Produces("application/json")
	public Response getAllByTenant0() {
		try {
			List<SocietaTutelaDTO> results = societaTutelaService.getAllByTenant0();
			return Response.ok(gson.toJson(results)).build();
		} catch (Exception e) {
			logger.error("Errore recupero societa tutela", e);
			return Response.status(500).build();
		}
	}

	@GET
	@Path("{id}")
	@Produces("application/json")
	public Response find(@PathParam("id") Integer id) {
		try {
			SocietaTutelaDTO result = societaTutelaService.get(id);
			return Response.ok(gson.toJson(result)).build();
		} catch (Exception e) {
			logger.error(String.format("Errore recupero societa tutela: %d", id), e);
			return Response.status(500).build();
		}
	}

	@POST
	@Produces("application/json")
	public Response save(SocietaTutelaDTO mandato) {
		try {
			SocietaTutelaDTO result = societaTutelaService.save(mandato);
			return Response.ok(gson.toJson(result)).build();
		} 
		catch (IllegalArgumentException e) {
			logger.error(String.format("Errore di Validazione: %s", gson.toJson(mandato)), e);
			return Response.status(400).build();
		}
		catch (Exception e) {
			logger.error(String.format("Errore salvataggio: %s", gson.toJson(mandato)), e);
			return Response.status(500).build();
		}
	}

	@DELETE
	@Produces("application/json")
	public Response delete(SocietaTutelaDTO mandato) {
		try {
			Integer result = societaTutelaService.delete(mandato);
			return Response.ok(gson.toJson(result)).build();
		}
		catch (Exception e) {
			logger.error(String.format("Errore nell'eliminazione del mandato: %s", gson.toJson(mandato)), e);
			return Response.status(500).build();
		}
	}
	
	@GET
	@Path("search")
	@Produces("application/json")
	public Response search(@QueryParam("field") String field, @QueryParam("value") String value) {
		List<SocietaTutelaDTO> results = societaTutelaService.search(field, value);
		return Response.ok(gson.toJson(results)).build();
	}

	@GET
	@Path("search-documentation")
	@Produces("application/json")
	public Response documentation(@QueryParam("codiceSocieta") String codiceSocieta, @QueryParam("pageNumber")Integer pageNumber, @QueryParam("pageSize")Integer pageSize, @QueryParam("order")String order) {
		PagedResult page = societaTutelaService.documentation(codiceSocieta, pageNumber, pageSize, order);
		return Response.ok(gson.toJson(page)).build();
	}
	
	@GET
	@Path("download-documentation/{id}")
	@Produces("application/vnd.ms-excel")
	public Response downloadById(@PathParam("id") Integer id) {
		ByteArrayOutputStream out = societaTutelaService.downloadById(id);
		return Response.ok()
				.type("text/csv")
				.entity(out.toByteArray())
				.build();
	}

}
