package com.alkemytech.sophia.codman.entity;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.codman.persistence.AbstractEntity;

@XmlRootElement
@Entity(name="InfoCcidStatistics")
public class InfoCcidStatistics extends AbstractEntity<String>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private Integer idInfoCcidStatistics;
	private Integer idImportoAnticipo;
	private Date periodoPertinenzaInizio;
	private Date periodoPertinenzaFine;
	private String numeroFattura;
	private BigDecimal importoOriginale;
	private BigDecimal importoUtilizzabile;
	private String idDsp;
	private BigDecimal quotaAnticipo;
    	
	public InfoCcidStatistics() {

	}

	public InfoCcidStatistics(Integer idImportoAnticipo, Date periodoPertinenzaInizio, Date periodoPertinenzaFine,
			String numeroFattura, BigDecimal importoOriginale, BigDecimal importoUtilizzabile, String idDsp,
			BigDecimal quotaAnticipo) {
		super();
		this.idImportoAnticipo = idImportoAnticipo;
		this.periodoPertinenzaInizio = periodoPertinenzaInizio;
		this.periodoPertinenzaFine = periodoPertinenzaFine;
		this.numeroFattura = numeroFattura;
		this.importoOriginale = importoOriginale;
		this.importoUtilizzabile = importoUtilizzabile;
		this.idDsp = idDsp;
		this.quotaAnticipo = quotaAnticipo;
	}



	public Integer getIdInfoCcidStatistics() {
		return idInfoCcidStatistics;
	}



	public void setIdInfoCcidStatistics(Integer idInfoCcidStatistics) {
		this.idInfoCcidStatistics = idInfoCcidStatistics;
	}



	public Integer getIdImportoAnticipo() {
		return idImportoAnticipo;
	}



	public void setIdImportoAnticipo(Integer idImportoAnticipo) {
		this.idImportoAnticipo = idImportoAnticipo;
	}



	public Date getPeriodoPertinenzaInizio() {
		return periodoPertinenzaInizio;
	}



	public void setPeriodoPertinenzaInizio(Date periodoPertinenzaInizio) {
		this.periodoPertinenzaInizio = periodoPertinenzaInizio;
	}



	public Date getPeriodoPertinenzaFine() {
		return periodoPertinenzaFine;
	}



	public void setPeriodoPertinenzaFine(Date periodoPertinenzaFine) {
		this.periodoPertinenzaFine = periodoPertinenzaFine;
	}



	public String getNumeroFattura() {
		return numeroFattura;
	}



	public void setNumeroFattura(String numeroFattura) {
		this.numeroFattura = numeroFattura;
	}



	public BigDecimal getImportoOriginale() {
		return importoOriginale;
	}



	public void setImportoOriginale(BigDecimal importoOriginale) {
		this.importoOriginale = importoOriginale;
	}



	public BigDecimal getImportoUtilizzabile() {
		return importoUtilizzabile;
	}



	public void setImportoUtilizzabile(BigDecimal importoUtilizzabile) {
		this.importoUtilizzabile = importoUtilizzabile;
	}



	public String getIdDsp() {
		return idDsp;
	}



	public void setIdDsp(String idDsp) {
		this.idDsp = idDsp;
	}



	public BigDecimal getQuotaAnticipo() {
		return quotaAnticipo;
	}



	public void setQuotaAnticipo(BigDecimal quotaAnticipo) {
		this.quotaAnticipo = quotaAnticipo;
	}



	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return null;
	}

}
