package com.alkemytech.sophia.codman.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.codman.persistence.AbstractEntity;
import com.google.gson.GsonBuilder;

@XmlRootElement
@Entity(name="IdentifiedSong")
@Table(name="identified_song")
@NamedQueries({@NamedQuery(name="IdentifiedSong.GetAll", query="SELECT x FROM IdentifiedSong x")})
@SuppressWarnings("serial")
public class IdentifiedSong extends AbstractEntity<String> {

	@Id
	@Column(name="hash_id", nullable=false)
	private String hashId;
	
	@Column(name="title", nullable=false)
	private String title;

	@Column(name="artists", nullable=false)
	private String artists;
	
	@Column(name="siada_title")
	private String siadaTitle;

	@Column(name="siada_artists")
	private String siadaArtists;
	
	@Column(name="roles")
	private String roles;
	
	@Column(name="siae_work_code", nullable=false)
	private String siaeWorkCode;
	
	@Column(name="identification_type", nullable=false)
	private String identificationType;

	@Column(name="insert_time", nullable=false)
	private Long insertTime;
	
	@Column(name="sophia_update_time", nullable=false)
	private Long sophiaUpdateTime;
	
	public IdentifiedSong() {
		super();
		sophiaUpdateTime = 0L;
	}

	@Override
	public String getId() {
		return hashId;
	}
	
	public String getHashId() {
		return hashId;
	}
	public void setHashId(String hashId) {
		this.hashId = hashId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getArtists() {
		return artists;
	}
	public void setArtists(String artists) {
		this.artists = artists;
	}
	
	public String getSiadaTitle() {
		return siadaTitle;
	}

	public void setSiadaTitle(String siadaTitle) {
		this.siadaTitle = siadaTitle;
	}

	public String getSiadaArtists() {
		return siadaArtists;
	}

	public void setSiadaArtists(String siadaArtists) {
		this.siadaArtists = siadaArtists;
	}

	public String getRoles() {
		return roles;
	}
	public void setRoles(String roles) {
		this.roles = roles;
	}

	public Long getInsertTime() {
		return insertTime;
	}
	public void setInsertTime(Long insertTime) {
		this.insertTime = insertTime;
	}

	public String getSiaeWorkCode() {
		return siaeWorkCode;
	}

	public void setSiaeWorkCode(String siaeWorkCode) {
		this.siaeWorkCode = siaeWorkCode;
	}

	public String getIdentificationType() {
		return identificationType;
	}

	public void setIdentificationType(String identificationType) {
		this.identificationType = identificationType;
	}

	public Long getSophiaUpdateTime() {
		return sophiaUpdateTime;
	}

	public void setSophiaUpdateTime(Long sophiaUpdateTime) {
		this.sophiaUpdateTime = sophiaUpdateTime;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}
}
