package com.alkemytech.sophia.codman.rest.performing;

import com.alkemytech.sophia.codman.dto.PerfEventiPagatiDTO;
import com.alkemytech.sophia.codman.dto.PerfUtilizzazioniDTO;
import com.alkemytech.sophia.codman.dto.ProgrammaMusicaleDTO;
import com.alkemytech.sophia.codman.dto.performing.AggiMcDTO;
import com.alkemytech.sophia.codman.dto.performing.FatturaDetailDTO;
import com.alkemytech.sophia.codman.dto.performing.RiconciliazioneImportiDTO;
import com.alkemytech.sophia.codman.dto.performing.csv.*;
import com.alkemytech.sophia.codman.entity.performing.*;
import com.alkemytech.sophia.codman.entity.performing.security.ReportPage;
import com.alkemytech.sophia.codman.rest.performing.service.*;
import com.alkemytech.sophia.codman.utils.DateUtils;
import com.alkemytech.sophia.codman.utils.JavaBeanToCsv;
import com.alkemytech.sophia.common.beantocsv.CustomMappingStrategy;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.opencsv.*;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import com.sun.jersey.multipart.FormDataParam;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DurationFormatUtils;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.PrivateKeyDetails;
import org.apache.http.ssl.PrivateKeyStrategy;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.*;

@Singleton
@Path("performing/cruscotti")
public class ConsoleCruscottiController {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private ICampionamentoService campionamentoService;
    private IProgrammaMusicaleService programmaMusicaleService;
    private IConsoleRicalcoloService consoleRicalcoloService;
    private IConfigurationService configurationService;
    private ITraceService traceService;
    private Gson gson;
    private String servizioMulePath;
    private String muleCertificationPath;
    private String mulePwd;
    private String keyPwd;
    private String muleAlias;
    private boolean selfSigned;
    private String keyStoreType;
    private String verifyHostNames;
    private String userAgent;
    private HttpServletRequest request;

    public static final String UTF8_BOM = "\uFEFF";

    @Inject
    protected ConsoleCruscottiController(HttpServletRequest request, Gson gson,
                                         ICampionamentoService campionamentoService, IProgrammaMusicaleService programmaMusicaleService,
                                         IConsoleRicalcoloService consoleRicalcoloService, IConfigurationService configurationService,
                                         ITraceService traceService, @Named("cruscotti.ricercaOpere") String servizioMulePath,
                                         @Named("cruscotti.certificatePath") String muleCertificationPath,
                                         @Named("cruscotti.trustStorePwd") String mulePwd, @Named("cruscotti.key.password") String keyPwd,
                                         @Named("cruscotti.alias") String muleAlias, @Named("cruscotti.self_signed") boolean selfSigned,
                                         @Named("cruscotti.key_store_type") String keyStoreType,
                                         @Named("cruscotti.verify_hostnames") String verifyHostNames,
                                         @Named("cruscotti.user_agent") String userAgent) {
        super();
        this.request = request;
        this.gson = gson;
        this.campionamentoService = campionamentoService;
        this.programmaMusicaleService = programmaMusicaleService;
        this.consoleRicalcoloService = consoleRicalcoloService;
        this.configurationService = configurationService;
        this.traceService = traceService;
        this.servizioMulePath = servizioMulePath;
        this.muleCertificationPath = muleCertificationPath;
        this.mulePwd = mulePwd;
        this.keyPwd = keyPwd;
        this.selfSigned = selfSigned;
        this.keyStoreType = keyStoreType;
        this.verifyHostNames = verifyHostNames;
        this.userAgent = userAgent;
        // this.createExcel = createExcel;
    }


    @GET
    @Path("riconciliazioneImportiList")
    public Response riconciliazioneImportiList(@QueryParam(value = "voceIncasso") String voceIncasso,
                                               @QueryParam(value = "agenzia") String agenzia, @QueryParam(value = "sede") String sede,
                                               @QueryParam(value = "seprag") String seprag, @QueryParam(value = "singolaFattura") String singolaFattura,
                                               @QueryParam(value = "inizioPeriodoContabile") String inizioPeriodoContabile,
                                               @QueryParam(value = "finePeriodoContabile") String finePeriodoContabile,
                                               @QueryParam(value = "inizioPeriodoFattura") String inizioPeriodoFattura,
                                               @QueryParam(value = "finePeriodoFattura") String finePeriodoFattura,
                                               @QueryParam(value="fatturaValidata") String fatturaValidata,
                                               @QueryParam(value="megaConcerto") String megaConcerto,
                                               @QueryParam(value = "order") String order,
                                               @QueryParam(value = "page") int page,
                                               @QueryParam(value = "count") boolean count) {

        Date dateInizioPeriodoFattura = StringUtils.isNotEmpty(inizioPeriodoFattura) ?
                DateUtils.stringToDate(inizioPeriodoFattura, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'") :
                null;
        Date dateFinePeriodoFattura = StringUtils.isNotEmpty(finePeriodoFattura) ?
                DateUtils.stringToDate(finePeriodoFattura, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'") :
                null;

        if (count) {
            TotaleRiconciliazioneImporti result = programmaMusicaleService.getCountRiconciliazioneImportiList(
                    voceIncasso,
                    agenzia,
                    sede,
                    seprag,
                    singolaFattura,
                    inizioPeriodoContabile,
                    finePeriodoContabile,
                    dateInizioPeriodoFattura,
                    dateFinePeriodoFattura,
                    fatturaValidata,
                    megaConcerto);
            return Response.ok(gson.toJson(result)).build();

        } else {
            ReportPage resultPage = programmaMusicaleService.getRiconciliazioneImportiList(
                    voceIncasso,
                    agenzia,
                    sede,
                    seprag,
                    singolaFattura,
                    inizioPeriodoContabile,
                    finePeriodoContabile,
                    dateInizioPeriodoFattura,
                    dateFinePeriodoFattura,
                    page,
                    order,
                    fatturaValidata,
                    megaConcerto);
            return Response.ok(gson.toJson(resultPage)).build();

        }
    }
    
    @GET
    @Path("riconciliazioneImportiListCSV")
    @Produces("application/csv")
    public Response riconciliazioneImportiListCSV(@QueryParam(value = "voceIncasso") String voceIncasso,
                                               @QueryParam(value = "agenzia") String agenzia, @QueryParam(value = "sede") String sede,
                                               @QueryParam(value = "seprag") String seprag, @QueryParam(value = "singolaFattura") String singolaFattura,
                                               @QueryParam(value = "inizioPeriodoContabile") String inizioPeriodoContabile,
                                               @QueryParam(value = "finePeriodoContabile") String finePeriodoContabile,
                                               @QueryParam(value = "inizioPeriodoFattura") String inizioPeriodoFattura,
                                               @QueryParam(value = "finePeriodoFattura") String finePeriodoFattura,
                                               @QueryParam(value="fatturaValidata") String fatturaValidata,
                                               @QueryParam(value="megaConcerto") String megaConcerto,
                                               @QueryParam(value = "order") String order,
                                               @QueryParam(value = "page") int page,
                                               @QueryParam(value = "count") boolean count) {
    	
        Date dateInizioPeriodoFattura = StringUtils.isNotEmpty(inizioPeriodoFattura) ?
                DateUtils.stringToDate(inizioPeriodoFattura, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'") :
                null;
        Date dateFinePeriodoFattura = StringUtils.isNotEmpty(finePeriodoFattura) ?
                DateUtils.stringToDate(finePeriodoFattura, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'") :
                null;


            ReportPage resultPage = programmaMusicaleService.getRiconciliazioneImportiList(
                    voceIncasso,
                    agenzia,
                    sede,
                    seprag,
                    singolaFattura,
                    inizioPeriodoContabile,
                    finePeriodoContabile,
                    dateInizioPeriodoFattura,
                    dateFinePeriodoFattura,
                    -1,
                    order,
                    fatturaValidata,
                    megaConcerto);
            
            try {

                StreamingOutput so = null;
                CustomMappingStrategy<RiconciliazioneImportiCsv> customMappingStrategy = new CustomMappingStrategy<>(
                        new RiconciliazioneImportiCsv().getMappingStrategy());
                customMappingStrategy.setType(RiconciliazioneImportiCsv.class);
                so = getListStreamingOutput(resultPage.getRecords(), customMappingStrategy);
                Response response = Response.ok(so, "text/csv")
                        .header("Content-Disposition", "attachment; filename=\"" + "Ricerca_PM.csv\"").build();
                return response;

            } catch (Exception e) {
                return Response.status(500).entity("Non risulta nessun dato per il periodo selezionato").build();
            }
    
    }

    @POST
    @Path("addConfigurazioneNuovoAggio")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addConfigurazioneNuovoAggio(AggiMcDTO aggio) {
        try {
            List<String> response = programmaMusicaleService.addConfigurazioneAggioMc(aggio);
            if (response.isEmpty()) {
                traceService.trace("Cruscotti", this.getClass().getName(), "/addConfigurazioneNuovoAggio",
                        "Aggiunta una nuova configurazione con id: " + aggio.getId(), Constants.ADD,
                        aggio.getUtenteCreazione());
                return Response.ok().build();
            } else
                return Response.status(500).entity(gson.toJson(response)).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity(gson.toJson(e)).build();
        }
    }

    @GET
    @Path("getEventiToEditOnAggio")
    public Response getEventiToEditOnAggio(
            @QueryParam(value = "inizioPeriodoValidazione") String inizioPeriodoValidazione,
            @QueryParam(value = "finePeriodoValidazione") String finePeriodoValidazione,
            @QueryParam(value = "id") Long id) {
        AggiMcDTO aggio = new AggiMcDTO();
        if (finePeriodoValidazione != null && finePeriodoValidazione.equals(""))
            finePeriodoValidazione = null;
        aggio.setId(id);
        aggio.setInizioPeriodoValidazione(inizioPeriodoValidazione);

        aggio.setFinePeriodoValidazione(finePeriodoValidazione);
        try {
            List<Long> response = programmaMusicaleService.getEventiToEditOnAggio(aggio);
            return Response.ok(gson.toJson(response.size())).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity(gson.toJson(e)).build();
        }
    }

    @PUT
    @Path("updateAggio")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateAggio(AggiMcDTO aggio) {
        try {
            List<String> response = programmaMusicaleService.updateAggi(aggio);

            if (response.isEmpty()) {
                // traceService.trace("Cruscotti", this.getClass().getName(), "/updateDataAggi",
                // "Modificata la data di validazione della configurazione con id: " +
                // aggio.getId() +
                // ", data di validazione precedente: " + ****ottenere la data di validazione
                // precedente********, Constants.UPDATE, aggio.getUtenteUltimaModifica());

                return Response.ok().build();
            } else {
                return Response.status(500).entity(gson.toJson(response)).build();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity(gson.toJson(e)).build();
        }
    }

    @PUT
    @Path("updateDataAggi")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateDataAggi(AggiMcDTO aggio) {
        try {
            List<String> response = programmaMusicaleService.updateDataAggi(aggio);

            if (response.isEmpty()) {
                // traceService.trace("Cruscotti", this.getClass().getName(), "/updateDataAggi",
                // "Modificata la data di validazione della configurazione con id: " +
                // aggio.getId() +
                // ", data di validazione precedente: " + ****ottenere la data di validazione
                // precedente********, Constants.UPDATE, aggio.getUtenteUltimaModifica());

                return Response.ok().build();
            } else {
                return Response.status(500).entity(gson.toJson(response)).build();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity(gson.toJson(e)).build();
        }
    }

    @PUT
    @Path("updateParametriAggi")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateParametriAggi(AggiMcDTO aggio) {
        try {
            // List<String> response =
            List<String> response = programmaMusicaleService.updateParametriAggi(aggio);
            // traceService.trace("Cruscotti", this.getClass().getName(),
            // "/updateParametriAggi", "Modificati i parametri della configurazione con id:
            // " + aggio.getId() +
            // ", parametri precedenti: soglia: " + " " + "Aggio sottosoglia: " + " " + "
            // Aggio soprasoglia: " + " " + " CAP: " + " " + " tipo CAP: " + " " +
            // ****ottenere i parametri precedenti e metterli al posto di " "********,
            // Constants.UPDATE, aggio.getUtenteUltimaModifica());

            if (response.isEmpty()) {
                return Response.ok().build();
            } else {
                return Response.status(500).entity(gson.toJson(response)).build();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity(gson.toJson(e)).build();
        }
    }

    @GET
    @Path("getListaAggiAttivi")
    public Response getListaAggiAttivi(@QueryParam(value = "inizioPeriodoValidazione") String inizioPeriodoValidazione,
                                       @QueryParam(value = "finePeriodoValidazione") String finePeriodoValidazione,
                                       @QueryParam(value = "codiceRegola") String codiceRegola, @QueryParam(value = "order") String order,
                                       @QueryParam(value = "page") Integer page) {
        try {
            if (inizioPeriodoValidazione != null && inizioPeriodoValidazione.equals(""))
                inizioPeriodoValidazione = null;
            if (finePeriodoValidazione != null && finePeriodoValidazione.equals(""))
                finePeriodoValidazione = null;
            if (codiceRegola != null && codiceRegola.equals(""))
                codiceRegola = null;
            if (order != null && order.equals(""))
                order = null;
            if (page == null)
                page = 0;

            List<AggiMc> aggiAttivi = new ArrayList<AggiMc>();

            SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            f.setTimeZone(TimeZone.getTimeZone("GMT+2"));
            aggiAttivi = programmaMusicaleService.getListaAggiAttivi(inizioPeriodoValidazione, finePeriodoValidazione,
                    codiceRegola, order);
            ReportPage reportPage;

            int recordCount = 0;
            if (aggiAttivi != null && aggiAttivi.size() > 0) {

                recordCount = aggiAttivi.size();

                int startRecord = 0;
                int endRecord = recordCount;

                if (page != -1) {

                    startRecord = (page - 1) * ReportPage.NUMBER_RECORD_PER_PAGE;
                    endRecord = 0;
                    if (recordCount > ReportPage.NUMBER_RECORD_PER_PAGE) {
                        endRecord = startRecord + ReportPage.NUMBER_RECORD_PER_PAGE;
                        if (endRecord > recordCount)
                            endRecord = recordCount - 1;
                    } else {
                        endRecord = recordCount;
                    }

                }
            }
            reportPage = new ReportPage(aggiAttivi, recordCount, page);
            return Response.ok(gson.toJson(reportPage)).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity(gson.toJson(e)).build();
        }
    }

    @GET
    @Path("getAggioAttivo")
    public Response getAggioAttivo(@QueryParam(value = "id") Long id) {
        try {
            AggiMc aggiAttivi = new AggiMc();
            aggiAttivi = programmaMusicaleService.getAggioAttivo(id);

            return Response.ok(gson.toJson(aggiAttivi)).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity(gson.toJson(e)).build();
        }
    }

    @GET
    @Path("getStoricoAggio")
    public Response getStoricoAggio(@QueryParam(value = "codiceRegola") String codiceRegola) {
        try {
            List<AggiMc> aggiAttivi = programmaMusicaleService.getStoricoAggio(codiceRegola);

            return Response.ok(gson.toJson(aggiAttivi)).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity(gson.toJson(e)).build();
        }
    }

    // TODO: Useless i think
    @GET
    @Path("getValidazioneDateAggi")
    public Response getValidazioneDateAggi(
            @QueryParam(value = "inizioPeriodoValidazione") String inizioPeriodoValidazione,
            @QueryParam(value = "finePeriodoValidazione") String finePeriodoValidazione) {
        try {
            List<String> response = programmaMusicaleService.getValidazioneDate(inizioPeriodoValidazione,
                    finePeriodoValidazione);
            if (response.isEmpty()) {
                return Response.ok().build();
            } else {
                return Response.status(500).entity(gson.toJson(response)).build();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity(gson.toJson(e)).build();
        }
    }

    /* RELEASE PRECEDENTI */

    /**
     * Ricalcolo Controllo
     *
     * @param model
     * @return
     */
    @GET
    @Path("ricalcoloControllo")
    public Response ricalcoloControllo() {

        // traceService.trace("Cruscotti", this.getClass().getName(),
        // "/ricalcoloControllo", "Accesso verifica di un ricalcolo avvenuto",
        // Constants.CLICK);
        //
        List<EsecuzioneRicalcolo> esecuzioni = consoleRicalcoloService.getEsecuzioni();

        String currentYear = DateUtils.dateToString(new Date(), "yyyy");
        String currentMonth = DateUtils.dateToString(new Date(), "MM");
        String finePeriodoRicalcolo = configurationService.getParameter("FINE_PERIODO_RIPARTIZIONE");

        Long annoFinePeriodoRicalcolo = Long.parseLong(finePeriodoRicalcolo.substring(0, 4));
        Long meseFinePeriodoRicalcolo = Long.parseLong(finePeriodoRicalcolo.substring(4, 6));

        // if (meseFinePeriodoRicalcolo==12) {
        // model.addAttribute("meseInizioPeriodoContabile", "01");
        // model.addAttribute("annoInizioPeriodoContabile", annoFinePeriodoRicalcolo+1);
        // }else {
        // if (meseFinePeriodoRicalcolo<9)
        // model.addAttribute("meseInizioPeriodoContabile",
        // "0"+meseFinePeriodoRicalcolo);
        // else
        // model.addAttribute("meseInizioPeriodoContabile", meseFinePeriodoRicalcolo);
        //
        // model.addAttribute("annoInizioPeriodoContabile", annoFinePeriodoRicalcolo);
        // }
        //
        //
        // if (esecuzioni!=null && esecuzioni.size()>0)
        // model.addAttribute("elabSelected", esecuzioni.get(0).getId());
        // else
        // model.addAttribute("elabSelected", -1);
        //
        // model.addAttribute("meseFinePeriodoContabile", currentMonth);
        // model.addAttribute("annoFinePeriodoContabile", currentYear);
        //
        // model.addAttribute("tipoSospensione", "RICALCOLATI");
        // model.addAttribute("esecuzioni", esecuzioni);
        //
        // ReportCache.clearReports();
        //
        // return "ricalcoloControllo";
        return Response.ok().build();
    }

    @GET
    @Path("ricalcoloControlloView")
    public Response ricalcoloControlloView(@QueryParam(value = "elaborazioneSIADAId") Long elaborazioneSIADAId,
                                           @QueryParam(value = "idEsecuzione") Long idEsecuzione, @QueryParam(value = "evento") Long evento,
                                           @QueryParam(value = "fattura") String fattura, @QueryParam(value = "voceIncasso") String voceIncasso,
                                           @QueryParam(value = "numeroPM") String numeroPM,
                                           @QueryParam(value = "meseInizioPeriodoContabile") String meseInizioPeriodoContabile,
                                           @QueryParam(value = "annoInizioPeriodoContabile") String annoInizioPeriodoContabile,
                                           @QueryParam(value = "meseFinePeriodoContabile") String meseFinePeriodoContabile,
                                           @QueryParam(value = "annoFinePeriodoContabile") String annoFinePeriodoContabile,
                                           @QueryParam(value = "tipoSospensione") String tipoSospensione, @QueryParam(value = "page") Integer page) {

        // traceService.trace("Cruscotti", this.getClass().getName(),
        // "/ricalcoloControlloView", "Visualizzazione report di ricalcolo avvenuto",
        // Constants.CLICK);

        // Setta l'elaborazione arrivata come elaborazione da inviare a SIADA
        if (elaborazioneSIADAId != null) {

            int result = 0;

            String statoEngine = consoleRicalcoloService.checkStatus();

            if (statoEngine != null && statoEngine.equals("ACTIVE")) {

                result = consoleRicalcoloService.setDataCongelamentoEsecuzione(elaborazioneSIADAId);

                // if (result != 0 && result == -2) {
                // model.addAttribute("message", "Periodo contabile selezionato gia' trasmesso a
                // SIADA");
                // } else
                if (result != 0 && result == -1) {
                    // model.addAttribute("message", "Si sono verificati problemi durante
                    // l'operazione");
                    return Response.status(500).build();
                } else {
                    String status = consoleRicalcoloService.startAggregazioneSiada(elaborazioneSIADAId);

                    return Response.ok(gson.toJson(status)).build();
                }

            } else {

                // model.addAttribute("message", "Attenzione. Verificare che il server sia
                // attivo dal menu Ricalcolo - Esecuzioni");
                return Response.status(500).build();
            }

        }

        if (page == null)
            page = 1;

        if (meseInizioPeriodoContabile.length() == 1)
            meseInizioPeriodoContabile = "0" + meseInizioPeriodoContabile;
        if (meseFinePeriodoContabile.length() == 1)
            meseFinePeriodoContabile = "0" + meseFinePeriodoContabile;

        String inizioPeriofoContabile = annoInizioPeriodoContabile + meseInizioPeriodoContabile;
        String finePeriofoContabile = annoFinePeriodoContabile + meseFinePeriodoContabile;

        try {

            ReportPage resultPage = consoleRicalcoloService.getListaPM(idEsecuzione, evento, fattura, voceIncasso,
                    numeroPM, inizioPeriofoContabile, finePeriofoContabile, page, tipoSospensione);

            List<EsecuzioneRicalcolo> esecuzioni = consoleRicalcoloService.getEsecuzioni();

            EsecuzioneRicalcolo esecuzioneCorrente = null;
            for (int i = 0; i < esecuzioni.size(); i++) {
                if (esecuzioni.get(i).getId().longValue() == idEsecuzione.longValue()) {
                    esecuzioneCorrente = esecuzioni.get(i);
                    break;
                }
            }

            return Response.ok(gson.toJson(resultPage)).build();
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: handle exception
        }
        return Response.status(500).build();
    }

    /**
     * Tracciamento PM
     *
     * @param model
     * @return
     */
    @GET
    @Path("tracciamentoPM")
    public Response tracciamentoPM() {

        // traceService.trace("Cruscotti", this.getClass().getName(), "/tracciamentoPM",
        // "Accesso tracciamento PM", Constants.CLICK);
        //
        // session.setAttribute("resultPage", null);
        // model.addAttribute("resultPage");
        //
        // ReportCache.clearReports();

        // return "cruscottiTracciamentoPM";
        return Response.ok().build();
    }

    @GET
    @Path("tracciamentoPMLista")
    public Response tracciamentoPMLista(
            @QueryParam(value = "meseInizioPeriodoContabile") String meseInizioPeriodoContabile,
            @QueryParam(value = "annoInizioPeriodoContabile") String annoInizioPeriodoContabile,
            @QueryParam(value = "meseFinePeriodoContabile") String meseFinePeriodoContabile,
            @QueryParam(value = "annoFinePeriodoContabile") String annoFinePeriodoContabile,
            @QueryParam(value = "tipoDocumento") String tipoDocumento, @QueryParam(value = "seprag") String seprag,
            @QueryParam(value = "evento") String idEvento, @QueryParam(value = "fattura") String fattura,
            @QueryParam(value = "reversale") String reversale, @QueryParam(value = "voceIncasso") String voceIncasso,
            @QueryParam(value = "permesso") String permesso, @QueryParam(value = "numeroPM") String numeroPM,
            @QueryParam(value = "locale") String locale, @QueryParam(value = "codiceBA") String codiceBA,
            @QueryParam(value = "codiceSAPOrganizzatore") String codiceSAPOrganizzatore,
            @QueryParam(value = "organizzatore") String organizzatore,
            @QueryParam(value = "direttoreEsecuzione") String direttoreEsecuzione,
            @QueryParam(value = "dataInizioEventoDa") String dataInizioEventoDa,
            @QueryParam(value = "dataInizioEventoA") String dataInizioEventoA,
            @QueryParam(value = "titoloOpera") String titoloOpera,
            @QueryParam(value = "presenzaMovimenti") String presenzaMovimenti,
            @QueryParam(value = "supporto") String supporto, @QueryParam(value = "tipoProgrammi") String tipoProgrammi,
            @QueryParam(value = "gruppoPrincipale") String gruppoPrincipale, @QueryParam(value = "order") String order,
            @QueryParam(value = "page") Integer page, @QueryParam(value = "count") Boolean count) {
        //
        // traceService.trace("Cruscotti", this.getClass().getName(),
        // "/tracciamentoPMLista", "Lista tracciamento PM", Constants.CLICK);
        if (meseInizioPeriodoContabile != null && meseInizioPeriodoContabile.equals(""))
            meseInizioPeriodoContabile = null;
        if (annoInizioPeriodoContabile != null && annoInizioPeriodoContabile.equals(""))
            annoInizioPeriodoContabile = null;
        if (meseFinePeriodoContabile != null && meseFinePeriodoContabile.equals(""))
            meseFinePeriodoContabile = null;
        if (annoFinePeriodoContabile != null && annoFinePeriodoContabile.equals(""))
            annoFinePeriodoContabile = null;

        if (tipoDocumento != null && tipoDocumento.equals(""))
            tipoDocumento = null;
        if (seprag != null && seprag.equals(""))
            seprag = null;
        if (idEvento != null && idEvento.equals(""))
            idEvento = null;
        if (fattura != null && fattura.equals(""))
            fattura = null;
        if (reversale != null && reversale.equals(""))
            reversale = null;
        if (voceIncasso != null && voceIncasso.equals(""))
            voceIncasso = null;
        if (permesso != null && permesso.equals(""))
            permesso = null;
        if (numeroPM != null && numeroPM.equals(""))
            numeroPM = null;
        if (locale != null && locale.equals(""))
            locale = null;
        if (codiceBA != null && codiceBA.equals(""))
            codiceBA = null;
        if (codiceSAPOrganizzatore != null && codiceSAPOrganizzatore.equals(""))
            codiceSAPOrganizzatore = null;
        if (organizzatore != null && organizzatore.equals(""))
            organizzatore = null;
        if (direttoreEsecuzione != null && direttoreEsecuzione.equals(""))
            direttoreEsecuzione = null;
        if (dataInizioEventoDa != null && dataInizioEventoDa.equals(""))
        	dataInizioEventoDa = null;
        if (dataInizioEventoA != null && dataInizioEventoA.equals(""))
        	dataInizioEventoA = null;
        if (titoloOpera != null && titoloOpera.equals(""))
            titoloOpera = null;
        if (supporto != null && supporto.equals(""))
            supporto = null;
        if (tipoProgrammi != null && tipoProgrammi.equals(""))
            tipoProgrammi = null;
        if (gruppoPrincipale != null && gruppoPrincipale.equals(""))
            gruppoPrincipale = null;

        if (meseInizioPeriodoContabile != null && meseInizioPeriodoContabile.length() == 1)
            meseInizioPeriodoContabile = "0" + meseInizioPeriodoContabile;
        if (meseFinePeriodoContabile != null && meseFinePeriodoContabile.length() == 1)
            meseFinePeriodoContabile = "0" + meseFinePeriodoContabile;

        if (count == null)
            count = false;

        if (page == null)
            page = 0;

        String inizioPeriodoContabile = null;
        String finePeriodoContabile = null;
        if (annoInizioPeriodoContabile != null && meseInizioPeriodoContabile != null) {
            inizioPeriodoContabile = annoInizioPeriodoContabile + meseInizioPeriodoContabile;
        }
        if (annoFinePeriodoContabile != null && meseFinePeriodoContabile != null) {
            finePeriodoContabile = annoFinePeriodoContabile + meseFinePeriodoContabile;
        }
        Long numeroProgrammaMusicale = null;
        if (numeroPM != null && !numeroPM.equalsIgnoreCase(""))
            numeroProgrammaMusicale = Long.parseLong(numeroPM);

        try {
            if (count == true) {
                long totRecords = programmaMusicaleService.getCountTracciamentoPM(inizioPeriodoContabile,
                        finePeriodoContabile, numeroProgrammaMusicale, page, tipoDocumento, seprag, idEvento, fattura,
                        reversale, voceIncasso, permesso, locale, codiceBA, codiceSAPOrganizzatore, organizzatore,
                        titoloOpera, dataInizioEventoDa, dataInizioEventoA, direttoreEsecuzione, supporto, tipoProgrammi,
                        gruppoPrincipale);
                return Response.ok(gson.toJson(totRecords)).build();
            } else {
                ReportPage resultPage = programmaMusicaleService.getTracciamentoPM(inizioPeriodoContabile,
                        finePeriodoContabile, numeroProgrammaMusicale, page, tipoDocumento, seprag, idEvento, fattura,
                        reversale, voceIncasso, permesso, locale, codiceBA, codiceSAPOrganizzatore, organizzatore,
                        titoloOpera, dataInizioEventoDa, dataInizioEventoA, direttoreEsecuzione, supporto, tipoProgrammi,
                        gruppoPrincipale, order);
                return Response.ok(gson.toJson(resultPage)).build();
            }
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: handle exception
        }

        return Response.status(500).build();
    }

    @PUT
    @Path("updatePmAttesiFromEventiPagati")
    public Response updatePmAttesiFromEventiPagati(@QueryParam(value = "pmPrevisti") Long pmPrevisti,
                                                   @QueryParam(value = "pmPrevistiSpalla") Long pmPrevistiSpalla,
                                                   @QueryParam(value = "idEventoPagato") Long idEventoPagato, @QueryParam(value = "userId") String userId) {
        EventiPagati evento = programmaMusicaleService.getEventoPagato(idEventoPagato);
        long numeroPmTotaliAttesiOld = evento.getNumeroPmTotaliAttesi();
        long numeroPmTotaliAttesiSpallaOld = evento.getNumeroPmTotaliAttesiSpalla();

        programmaMusicaleService.updatePMAttesiInEventiPagati(pmPrevisti, pmPrevistiSpalla, idEventoPagato);

        // traceService.trace("Cruscotti", this.getClass().getName(),
        // "/updatePmAttesiFromEventiPagati", "Aggiornato il numero dei pm attesi nell'
        // evento pagato con id: " + evento.getId() +
        // ", parametri precedenti: -pmPrevisti: " + evento.getNumeroPmTotaliAttesi() +
        // " -pmPrevistiSpalla: " + evento.getNumeroPmTotaliAttesiSpalla(),
        // Constants.UPDATE, userId);
        traceService.trace("Cruscotti", this.getClass().getName(), "/updatePmAttesiFromEventiPagati",
                "Aggiornato il numero dei pm attesi nell' evento pagato con id: " + evento.getId()
                        + ", parametri precedenti: -pmPrevisti: " + numeroPmTotaliAttesiOld + " -pmPrevistiSpalla: "
                        + numeroPmTotaliAttesiSpallaOld,
                Constants.UPDATE, userId); // aggiunto ale

        return Response.ok(gson.toJson("ok")).build();
    }

    @PUT
    @Path("updatePmAttesiFromMovimenti")
    public Response updatePmAttesiFromMovimenti(@QueryParam(value = "pmPrevisti") Integer pmPrevisti,
                                                @QueryParam(value = "pmPrevistiSpalla") Integer pmPrevistiSpalla,
                                                @QueryParam(value = "idMovimento") Long idMovimento, @QueryParam(value = "userId") String userId) {
        MovimentoContabile movimento = programmaMusicaleService.getMovimento(idMovimento);
        Integer numeroPmPrevistiOld = movimento.getNumeroPmPrevisti();
        Integer numeroPmPrevistiSpallaOld = movimento.getNumeroPmPrevistiSpalla();

        programmaMusicaleService.updatePMAttesiInMovimenti(pmPrevisti, pmPrevistiSpalla, idMovimento);
        // traceService.trace("Cruscotti", this.getClass().getName(),
        // "/updatePmAttesiFromMovimenti", "Aggiornato il numero dei pm previsti nel
        // movimento contabile con id: "
        // + movimento.getId() + ", parametri precedenti: -pmPrevisti: " +
        // movimento.getNumeroPmPrevisti() + " -pmPrevistiSpalla: " +
        // movimento.getNumeroPmPrevistiSpalla(), Constants.UPDATE, userId);
        traceService.trace("Cruscotti", this.getClass().getName(), "/updatePmAttesiFromMovimenti",
                "Aggiornato il numero dei pm previsti nel movimento contabile con id: " + movimento.getId()
                        + ", parametri precedenti: -pmPrevisti: " + numeroPmPrevistiOld + " -pmPrevistiSpalla: "
                        + numeroPmPrevistiSpallaOld,
                Constants.UPDATE, userId); // aggiunto ale

        return Response.ok(gson.toJson("ok")).build();
    }

    @GET
    @Path("controlloPmRientrati")
    public Response controlloPmRientrati(@QueryParam(value = "idEvento") Long idEvento) {
        List<EventiPagati> risultato = programmaMusicaleService.getEvento(idEvento);

        Map<String, Long> risultatoFinale = new HashMap<String, Long>();

        if (risultato.get(0).getVoceIncasso().equals("2244")) {
            risultatoFinale.put("PmPrevisti", risultato.get(0).getDettaglioPM().get(0).getPmPrevisti());
            risultatoFinale.put("PmPrevistiSpalla", risultato.get(0).getDettaglioPM().get(0).getPmPrevistiSpalla());
            risultatoFinale.put("PmRientrati", risultato.get(0).getDettaglioPM().get(0).getPmRientrati());
            risultatoFinale.put("PmRientratiSpalla", risultato.get(0).getDettaglioPM().get(0).getPmRientratiSpalla());
        } else {
            risultatoFinale.put("PmPrevisti", risultato.get(0).getDettaglioPM().get(0).getPmPrevisti());
            risultatoFinale.put("PmRientrati", risultato.get(0).getDettaglioPM().get(0).getPmRientrati());
        }

        return Response.ok(gson.toJson(risultatoFinale)).build();
    }

    @PUT
    @Path("updatePmOnDetail")
    public Response updatePmOnDetail(@QueryParam(value = "userId") String userId, @QueryParam(value = "idPM") Long idPM,
                                     @QueryParam(value = "flagCampionamento") String flagCampionamento,
                                     @QueryParam(value = "flagGruppo") String flagGruppo, @QueryParam(value = "durata") Double durata,
                                     @QueryParam(value = "durataMeno30Sec") String durataMeno30Sec, @QueryParam(value = "flagPD") String flagPD,
                                     @QueryParam(value = "idOpera") Long idOpera) {

        ProgrammaMusicale progM = new ProgrammaMusicale();
        Utilizzazione opera = new Utilizzazione();
        Character flagGruppoOld = null;
        Character flagCampionamentoOld = null;
        Double durataOld = null;
        String durataMeno30SecOld = null;
        String flagPDOld = null;

        if (idPM != null || flagCampionamento != null || flagGruppo != null) {
            progM = programmaMusicaleService.getDettaglioPM(idPM);
            flagGruppoOld = progM.getFlagGruppoPrincipale(); // aggiunto ale
            flagCampionamentoOld = progM.getFlagDaCampionare();// aggiunto ale
        } else if (idOpera != null && idOpera.equals("") != true) {
            opera = programmaMusicaleService.getOpera(idOpera);
            durataOld = opera.getDurata();// aggiunto ale
            durataMeno30SecOld = opera.getDurataInferiore30sec();// aggiunto ale
            flagPDOld = opera.getFlagPubblicoDominio();// aggiunto ale
        }

        try {
            programmaMusicaleService.updatePmOnDetail(idPM, flagCampionamento, flagGruppo, durata, durataMeno30Sec,
                    flagPD, idOpera);

            if (idPM != null && flagCampionamento != null) {
                // traceService.trace("Cruscotti", this.getClass().getName(),
                // "/updatePmOnDetail", "Modificato il flag campionamento del programma musicale
                // con id: " + progM.getId() + ", parametro precedente: "
                // +progM.getFlagDaCampionare(), Constants.UPDATE, userId);
                traceService.trace("Cruscotti", this.getClass().getName(), "/updatePmOnDetail",
                        "Modificato il flag campionamento del programma musicale con id: " + progM.getId()
                                + ", parametro precedente: " + flagCampionamentoOld,
                        Constants.UPDATE, userId); // aggiunto ale
            } else if (idPM != null && flagGruppo != null) {
                // traceService.trace("Cruscotti", this.getClass().getName(),
                // "/updatePmOnDetail", "Modificato il flag gruppo principale/spalla del
                // programma musicale con id: " + progM.getId() + ", parametro precedente: "
                // +progM.getFlagGruppoPrincipale(), Constants.UPDATE, userId);
                traceService
                        .trace("Cruscotti", this.getClass().getName(), "/updatePmOnDetail",
                                "Modificato il flag gruppo principale/spalla del programma musicale con id: "
                                        + progM.getId() + ", parametro precedente: " + flagGruppoOld,
                                Constants.UPDATE, userId); // aggiunto ale
            }

            if (idOpera != null && idOpera.equals("") != true && durata != null) {
                // traceService.trace("Cruscotti", this.getClass().getName(),
                // "/updatePmOnDetail", "Modificata la durata dell'opera con id: " +
                // opera.getId() + " nel programma musicale con id: " +
                // opera.getIdProgrammaMusicale() + ", parametro precedente: "
                // +opera.getDurata(), Constants.UPDATE, userId);
                traceService.trace("Cruscotti", this.getClass().getName(), "/updatePmOnDetail",
                        "Modificata la durata dell'opera con id: " + opera.getId() + " nel programma musicale con id: "
                                + opera.getIdProgrammaMusicale() + ", parametro precedente: " + durataOld,
                        Constants.UPDATE, userId); // aggiunto ale
            }
            if (idOpera != null && idOpera.equals("") != true && durataMeno30Sec != null) {
                // traceService.trace("Cruscotti", this.getClass().getName(),
                // "/updatePmOnDetail", "Modificato il flag durata meno 30 secondi dell'opera
                // con id: " + opera.getId() + " nel programma musicale con id: " +
                // opera.getIdProgrammaMusicale() + ", parametro precedente: "
                // +opera.getDurataInferiore30sec(), Constants.UPDATE, userId);
                traceService.trace("Cruscotti", this.getClass().getName(), "/updatePmOnDetail",
                        "Modificato il flag durata meno 30 secondi dell'opera con id: " + opera.getId()
                                + " nel programma musicale con id: " + opera.getIdProgrammaMusicale()
                                + ", parametro precedente: " + durataMeno30SecOld,
                        Constants.UPDATE, userId);// aggiunto ale
            }
            if (idOpera != null && idOpera.equals("") != true && flagPD != null) {
                // traceService.trace("Cruscotti", this.getClass().getName(),
                // "/updatePmOnDetail", "Modificato il flag pubblico dominio dell'opera con id:
                // " + opera.getId() + " nel programma musicale con id: " +
                // opera.getIdProgrammaMusicale() + ", parametro precedente: "
                // +opera.getFlagPubblicoDominio(), Constants.UPDATE, userId);
                traceService.trace("Cruscotti", this.getClass().getName(), "/updatePmOnDetail",
                        "Modificato il flag pubblico dominio dell'opera con id: " + opera.getId()
                                + " nel programma musicale con id: " + opera.getIdProgrammaMusicale()
                                + ", parametro precedente: " + flagPDOld,
                        Constants.UPDATE, userId);// aggiunto ale
            }

            return Response.ok(gson.toJson("ok")).build();
        } catch (Exception e) {
            return Response.status(500).build();
        }
    }

    @PUT
    @Path("updatePm")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updatePm(ProgrammaMusicaleDTO programmaMusicaleDTO) {
        try {
            programmaMusicaleService.updatePm(programmaMusicaleDTO);
            return Response.ok(gson.toJson("ok")).build();
        } catch (Exception e) {
            return Response.status(500).build();
        }
    }

    @POST
    @Path("addPm")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addPm(@QueryParam(value = "userId") String userId, ProgrammaMusicaleDTO programmaMusicaleDto) {
        try {
            ProgrammaMusicale programmaMusicale = new ProgrammaMusicale();

            programmaMusicale.setNumeroProgrammaMusicale(programmaMusicaleDto.getNumeroProgrammaMusicale());
            programmaMusicale.setTipoPm(programmaMusicaleDto.getTipoPm());
            programmaMusicale.setSupportoPm(programmaMusicaleDto.getSupportoPm());
            programmaMusicale.setAgenziaDiCarico(programmaMusicaleDto.getAgenziaDiCarico());
            programmaMusicale.setDataAssegnazione(programmaMusicaleDto.getDataAssegnazione());
            programmaMusicale.setDataRestituzione(programmaMusicaleDto.getDataRestituzione());
            programmaMusicale.setDataCompilazione(programmaMusicaleDto.getDataCompilazione());
            programmaMusicale.setStatoPm(programmaMusicaleDto.getStato());
            programmaMusicale.setDataAnnullamento(programmaMusicaleDto.getDataAnnullamento());
            programmaMusicale.setDataAcquisizioneSophia(programmaMusicaleDto.getDataAcquisizioneSophia());
            programmaMusicale.setDataRipartizione(programmaMusicaleDto.getDataRipartizione());
            programmaMusicale.setDataInvioDg(programmaMusicaleDto.getDataInvioDg());
            programmaMusicale.setDataAcquisizoneImmagine(programmaMusicaleDto.getDataAcquisizioneImmagine());
            programmaMusicale.setDataRicezioneDig(programmaMusicaleDto.getDataRicezioneInfoDig());
            programmaMusicale.setCausaAnnullamento(programmaMusicaleDto.getCausaAnnullamento());
            programmaMusicale.setProgressivo(programmaMusicaleDto.getProgressivo());
            programmaMusicale.setTotaleCedole(programmaMusicaleDto.getTotaleCedole());
            programmaMusicale.setTotaleDurataCedole(programmaMusicaleDto.getTotaleDurataCedole());
            programmaMusicale.setFoglio(programmaMusicaleDto.getFoglio());
            programmaMusicale.setPmRiferimento(programmaMusicaleDto.getPmRiferimento());
            programmaMusicale.setRisultatoCalcoloResto(programmaMusicaleDto.getRisultatoCalcoloResto());
            programmaMusicale.setDataOraUltimaModifica(programmaMusicaleDto.getDataOraUltimaModifica());
            programmaMusicale.setUtenteUltimaModifica(programmaMusicaleDto.getUtenteUltimaModifica());
            programmaMusicale.setGiornateTrattenimento(programmaMusicaleDto.getGiornateTrattenimento());
            programmaMusicale.setFogli(programmaMusicaleDto.getFogli());
            programmaMusicale.setFlagPmCorrente(programmaMusicaleDto.getFlagPmCorrente());
            programmaMusicale.setDataAssegnazione(programmaMusicaleDto.getDataAssegnazione());
            programmaMusicale.setDataAssegnazioneIncarico(programmaMusicaleDto.getDataAssegnazioneIncarico());
            programmaMusicale.setDataRegistazioneRilevazione(programmaMusicaleDto.getDataRegistrazioneRilevazione());
            programmaMusicale.setDurataRilevazione(programmaMusicaleDto.getDurataRilevazione());
            programmaMusicale.setProtocolloRegistrazione(programmaMusicaleDto.getProtocolloRegistrazione());
            programmaMusicale.setProtocolloTrasmissione(programmaMusicaleDto.getProtocolloTrasmissione());
            programmaMusicale.setCodiceCampionamento(programmaMusicaleDto.getCodiceCampionamento());
            programmaMusicale.setFlagDaCampionare(programmaMusicaleDto.getFlagDaCampionare());
            programmaMusicale.setFlagGruppoPrincipale(programmaMusicaleDto.getFlagGruppoPrincipale());
            programmaMusicale.setFoglioSegueDi(programmaMusicaleDto.getFoglioSegueDi());

            programmaMusicaleService.addPm(programmaMusicale);

            traceService.trace("Cruscotti", this.getClass().getName(), "/addPm",
                    "PM aggiunto " + programmaMusicaleDto.toString(), Constants.ADD, userId);
            return Response.ok(gson.toJson("ok")).build();
        } catch (Exception e) {
            return Response.status(500).build();
        }
    }

    @PUT
    @Path("sgancioMultiploPm")
    public Response sgancioPm(@QueryParam(value = "userId") String userId,
                              @QueryParam(value = "idEvento") Long idEvento, @QueryParam(value = "voceIncasso") String voceIncasso,
                              @QueryParam(value = "idPm") Long idPm) {

        if (userId != null && userId.equals(""))
            userId = null;
        if (idEvento != null && idEvento.equals(""))
            idEvento = null;
        if (voceIncasso != null && voceIncasso.equals(""))
            voceIncasso = null;
        if (idPm != null && idPm.equals(""))
            idPm = null;

        try {
            List<Long> idMovimenti = new ArrayList<Long>();
            idMovimenti = programmaMusicaleService.getIdMovimentiDaSganciare(idPm, voceIncasso, idEvento);

            programmaMusicaleService.sgancioPm(idPm, voceIncasso, idEvento);

            for (Long id : idMovimenti) {
                traceService.trace("Cruscotti", this.getClass().getName(), "/sgancioPm",
                        "Sganciato l'id evento e settato a null nel movimento contabile con id: " + id,
                        Constants.UPDATE, userId);
            }

            return Response.ok(gson.toJson("ok")).build();
        } catch (Exception e) {
            return Response.status(500).entity(gson.toJson(e)).build();
        }
    }

    @PUT
    @Path("sgancioPm")
    public Response sgancioPm(@QueryParam(value = "userId") String userId,
                              @QueryParam(value = "idMovimentoContabile") Long idMovimentoContabile) {

        if (userId != null && userId.equals(""))
            userId = null;
        if (idMovimentoContabile != null && idMovimentoContabile.equals(""))
            idMovimentoContabile = null;

        try {

            programmaMusicaleService.sgancioPm(idMovimentoContabile);

            traceService.trace("Cruscotti", this.getClass().getName(), "/sgancioPm",
                    "Sganciato l'id evento e settato a null nel movimento contabile con id: " + idMovimentoContabile,
                    Constants.UPDATE, userId);

            return Response.ok(gson.toJson("ok")).build();
        } catch (Exception e) {
            return Response.status(500).entity(gson.toJson(e)).build();
        }
    }

    @GET
    @Path("getVociIncassoPerIdEvento")
    public Response getVociIncassoPerIdEvento(@QueryParam(value = "idEvento") Long idEvento) {
        try {

            List<String> vociIncasso = programmaMusicaleService.getVociIncassoPerIdEvento(idEvento);

            return Response.ok(gson.toJson(vociIncasso)).build();
        } catch (Exception e) {
            return Response.status(500).entity(gson.toJson(e)).build();
        }

    }

    @PUT
    @Path("aggancioPm")
    public Response aggancioPm(@QueryParam(value = "userId") String userId,
                               @QueryParam(value = "idEvento") Long idEvento, @QueryParam(value = "voceIncasso") String voceIncasso,
                               @QueryParam(value = "numeroPm") String numeroPm) {

        try {
            programmaMusicaleService.aggancioPm(numeroPm, voceIncasso, idEvento);

            // TODO correct traceService
            traceService.trace("Cruscotti", this.getClass().getName(), "/aggancioPm",
                    "Agganciato Programma musicale all'evento: " + idEvento, Constants.UPDATE, userId);

            return Response.ok(gson.toJson("ok")).build();
        } catch (Exception e) {
            System.out.println(e);
            return Response.status(500).build();
        }
    }

    /**
     * Lista PM Sospesi
     *
     * @return
     */
    @GET
    @Path("sospesiPM")
    public Response sospesiPM() {

        // traceService.trace("Cruscotti", this.getClass().getName(), "/sospesiPM",
        // "Accesso lista PM sospesi", Constants.CLICK);

        List<EsecuzioneRicalcolo> esecuzioni = consoleRicalcoloService.getEsecuzioni();

        String currentYear = DateUtils.dateToString(new Date(), "yyyy");
        String currentMonth = DateUtils.dateToString(new Date(), "MM");
        String finePeriodoRicalcolo = configurationService.getParameter("FINE_PERIODO_RIPARTIZIONE");

        Long annoFinePeriodoRicalcolo = Long.parseLong(finePeriodoRicalcolo.substring(0, 4));
        Long meseFinePeriodoRicalcolo = Long.parseLong(finePeriodoRicalcolo.substring(4, 6));

        // if (meseFinePeriodoRicalcolo == 12) {
        // model.addAttribute("meseInizioPeriodoContabile", "01");
        // model.addAttribute("annoInizioPeriodoContabile", annoFinePeriodoRicalcolo +
        // 1);
        // } else {
        // if (meseFinePeriodoRicalcolo < 9)
        // model.addAttribute("meseInizioPeriodoContabile", "0" +
        // meseFinePeriodoRicalcolo);
        // else
        // model.addAttribute("meseInizioPeriodoContabile", meseFinePeriodoRicalcolo);
        //
        // model.addAttribute("annoInizioPeriodoContabile", annoFinePeriodoRicalcolo);
        // }
        //
        // if (esecuzioni != null && esecuzioni.size() > 0)
        // model.addAttribute("elabSelected", esecuzioni.get(0).getId());
        // else
        // model.addAttribute("elabSelected", -1);
        //
        // model.addAttribute("meseFinePeriodoContabile", currentMonth);
        // model.addAttribute("annoFinePeriodoContabile", currentYear);
        //
        // model.addAttribute("tipoSospensione", "RICALCOLATI");
        // model.addAttribute("esecuzioni", esecuzioni);
        //
        // return "cruscottiSospesi";
        return Response.ok().build();
    }

    @GET
    @Path("sospesiPMList")
    public Response sospesiPMList(@QueryParam(value = "idEsecuzione") Long idEsecuzione,
                                  @QueryParam(value = "meseInizioPeriodoContabile") String meseInizioPeriodoContabile,
                                  @QueryParam(value = "annoInizioPeriodoContabile") String annoInizioPeriodoContabile,
                                  @QueryParam(value = "meseFinePeriodoContabile") String meseFinePeriodoContabile,
                                  @QueryParam(value = "annoFinePeriodoContabile") String annoFinePeriodoContabile,
                                  @QueryParam(value = "tipoSospensione") String tipoSospensione, @QueryParam(value = "page") Integer page) {

        // traceService.trace("Cruscotti", this.getClass().getName(),
        // "/sospesiPMList", "Visualizzazione lista PM sospesi", Constants.CLICK);

        if (page == null)
            page = 1;

        if (meseInizioPeriodoContabile.length() == 1)
            meseInizioPeriodoContabile = "0" + meseInizioPeriodoContabile;
        if (meseFinePeriodoContabile.length() == 1)
            meseFinePeriodoContabile = "0" + meseFinePeriodoContabile;

        String inizioPeriofoContabile = annoInizioPeriodoContabile + meseInizioPeriodoContabile;
        String finePeriofoContabile = annoFinePeriodoContabile + meseFinePeriodoContabile;

        try {
            ReportPage resultPage = consoleRicalcoloService.getListaPM(idEsecuzione, null, null, null, null,
                    inizioPeriofoContabile, finePeriofoContabile, page, tipoSospensione);

            List<EsecuzioneRicalcolo> esecuzioni = consoleRicalcoloService.getEsecuzioni();

            EsecuzioneRicalcolo esecuzioneCorrente = null;
            for (int i = 0; i < esecuzioni.size(); i++) {
                if (esecuzioni.get(i).getId().longValue() == idEsecuzione.longValue()) {
                    esecuzioneCorrente = esecuzioni.get(i);
                    break;
                }
            }

            return Response.ok(gson.toJson(resultPage)).build();
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: handle exception
        }
        return Response.status(500).build();
    }

    @GET
    @Path("eventiPagati")
    public Response eventiPagati() {

        // traceService.trace("Cruscotti", this.getClass().getName(), "/eventiPagati",
        // "Accesso lista Eventi Pagati", Constants.CLICK);

        String currentYear = DateUtils.dateToString(new Date(), "yyyy");
        String currentMonth = DateUtils.dateToString(new Date(), "MM");
        String finePeriodoRicalcolo = configurationService.getParameter("FINE_PERIODO_RIPARTIZIONE");

        Long annoFinePeriodoRicalcolo = Long.parseLong(finePeriodoRicalcolo.substring(0, 4));
        Long meseFinePeriodoRicalcolo = Long.parseLong(finePeriodoRicalcolo.substring(4, 6));
        //
        // if (meseFinePeriodoRicalcolo==12) {
        // model.addAttribute("meseInizioPeriodoContabile", "01");
        // model.addAttribute("annoInizioPeriodoContabile", annoFinePeriodoRicalcolo+1);
        // }else {
        // if (meseFinePeriodoRicalcolo<9)
        // model.addAttribute("meseInizioPeriodoContabile",
        // "0"+meseFinePeriodoRicalcolo);
        // else
        // model.addAttribute("meseInizioPeriodoContabile", meseFinePeriodoRicalcolo);
        //
        // model.addAttribute("annoInizioPeriodoContabile", annoFinePeriodoRicalcolo);
        // }
        //
        // model.addAttribute("meseFinePeriodoContabile", currentMonth);
        // model.addAttribute("annoFinePeriodoContabile", currentYear);
        //
        // session.setAttribute("resultPage", null);
        // model.addAttribute("resultPage");
        //
        // ReportCache.clearReports();
        //
        // return "cruscottiEventiPagati";
        return Response.ok().build();
    }

    @GET
    @Path("eventiPagatiList")
    public Response eventiPagatiList(
            @QueryParam(value = "meseInizioPeriodoContabile") String meseInizioPeriodoContabile,
            @QueryParam(value = "annoInizioPeriodoContabile") String annoInizioPeriodoContabile,
            @QueryParam(value = "meseFinePeriodoContabile") String meseFinePeriodoContabile,
            @QueryParam(value = "annoFinePeriodoContabile") String annoFinePeriodoContabile,
            @QueryParam(value = "tipoDocumento") String tipoDocumento, @QueryParam(value = "seprag") String seprag,
            @QueryParam(value = "evento") Long evento, @QueryParam(value = "fattura") String fattura,
            @QueryParam(value = "reversale") String reversale, @QueryParam(value = "voceIncasso") String voceIncasso,
            @QueryParam(value = "permesso") String permesso, @QueryParam(value = "numeroPM") String numeroPM,
            @QueryParam(value = "locale") String locale, @QueryParam(value = "codiceBA") String codiceBA,
            @QueryParam(value = "codiceSAP") String codiceSAP,
            @QueryParam(value = "organizzatore") String organizzatore,
            @QueryParam(value = "direttoreEsecuzione") String direttoreEsecuzione,
            @QueryParam(value = "dataInizioEvento") String dataInizioEvento,
            @QueryParam(value = "dataFineEvento") String dataFineEvento,
            @QueryParam(value = "titoloOpera") String titoloOpera,
            @QueryParam(value = "presenzaMovimenti") String presenzaMovimenti,
            @QueryParam(value = "fatturaValidata") String fatturaValidata, @QueryParam(value = "order") String order,
            @QueryParam(value = "page") Integer page, @QueryParam(value = "count") Boolean count) {

        // traceService.trace("Cruscotti", this.getClass().getName(),
        // "/eventiPagatiList", "Visualizzazione lista Eventi Pagati", Constants.CLICK);

        if (page == null)
            page = 0;
        if (meseInizioPeriodoContabile != null && meseInizioPeriodoContabile.equals(""))
            meseInizioPeriodoContabile = null;
        if (meseFinePeriodoContabile != null && meseFinePeriodoContabile.equals(""))
            meseFinePeriodoContabile = null;
        if (annoInizioPeriodoContabile != null && annoInizioPeriodoContabile.equals(""))
            annoInizioPeriodoContabile = null;
        if (annoFinePeriodoContabile != null && annoFinePeriodoContabile.equals(""))
            annoFinePeriodoContabile = null;
        if (tipoDocumento != null && tipoDocumento.equals(""))
            tipoDocumento = null;
        if (seprag != null && seprag.equals(""))
            seprag = null;
        if (evento != null && evento.equals(""))
            evento = null;
        if (fattura != null && fattura.equals(""))
            fattura = null;
        if (reversale != null && reversale.equals(""))
            reversale = null;
        if (voceIncasso != null && voceIncasso.equals(""))
            voceIncasso = null;
        if (permesso != null && permesso.equals(""))
            permesso = null;
        if (numeroPM != null && numeroPM.equals(""))
            numeroPM = null;
        if (locale != null && locale.equals(""))
            locale = null;
        if (codiceBA != null && codiceBA.equals(""))
            codiceBA = null;
        if (codiceSAP != null && codiceSAP.equals(""))
            codiceSAP = null;
        if (organizzatore != null && organizzatore.equals(""))
            organizzatore = null;
        if (direttoreEsecuzione != null && direttoreEsecuzione.equals(""))
            direttoreEsecuzione = null;
        if (dataInizioEvento != null && dataInizioEvento.equals(""))
            dataInizioEvento = null;
        // if (oraInizioEvento != null && oraInizioEvento.equals(""))
        // oraInizioEvento = null;
        if (dataFineEvento != null && dataFineEvento.equals(""))
            dataFineEvento = null;
        if (fatturaValidata == null)
            fatturaValidata = "";
        // if (oraFineEvento != null && oraFineEvento.equals(""))
        // oraFineEvento = null;

        if (titoloOpera != null && titoloOpera.equals(""))
            titoloOpera = null;
        if (meseInizioPeriodoContabile != null && meseInizioPeriodoContabile.length() == 1)
            meseInizioPeriodoContabile = "0" + meseInizioPeriodoContabile;
        if (meseFinePeriodoContabile != null && meseFinePeriodoContabile.length() == 1)
            meseFinePeriodoContabile = "0" + meseFinePeriodoContabile;

        if (count == null)
            count = false;
        String inizioPeriodoContabile = null;
        String finePeriodoContabile = null;

        if (annoInizioPeriodoContabile != null && meseInizioPeriodoContabile != null)
            inizioPeriodoContabile = annoInizioPeriodoContabile + meseInizioPeriodoContabile;

        if (annoFinePeriodoContabile != null && meseFinePeriodoContabile != null)
            finePeriodoContabile = annoFinePeriodoContabile + meseFinePeriodoContabile;

        PerfEventiPagatiDTO perfEventiPagatiDTO = new PerfEventiPagatiDTO(meseInizioPeriodoContabile,
                annoInizioPeriodoContabile, meseFinePeriodoContabile, annoFinePeriodoContabile, evento, fattura,
                reversale, voceIncasso, dataInizioEvento, "0000", seprag, tipoDocumento, locale, codiceBA,
                presenzaMovimenti, page);
        try {
            if (count == true) {
                long totRecords = programmaMusicaleService.getCountListaEventiPagati(perfEventiPagatiDTO,
                        inizioPeriodoContabile, finePeriodoContabile, organizzatore, direttoreEsecuzione, permesso,
                        numeroPM, titoloOpera, codiceSAP, dataInizioEvento, dataFineEvento, fatturaValidata);
                return Response.ok(gson.toJson(totRecords)).build();
            } else {
                ReportPage resultPage = programmaMusicaleService.getListaEventiPagati(perfEventiPagatiDTO,
                        inizioPeriodoContabile, finePeriodoContabile, organizzatore, direttoreEsecuzione, permesso,
                        numeroPM, titoloOpera, codiceSAP, dataInizioEvento, dataFineEvento, fatturaValidata, order);

                return Response.ok(gson.toJson(resultPage)).build();
            }
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: handle exception
        }
        return Response.status(500).build();

    }

    @GET
    @Path("eventoDetail")
    public Response eventoDetail(@QueryParam(value = "evento") Long evento) {

        // traceService.trace("Cruscotti", this.getClass().getName(), "/eventoDetail",
        // "Visualizzazione dettaglio Eventi Pagati", Constants.CLICK);

        // Gestione breadCrumbs in pagina: inizio
        // Gestione breadCrumbs in pagina: fine

        List<EventiPagati> eventoPagato = programmaMusicaleService.getEvento(evento);

        return Response.ok(gson.toJson(eventoPagato)).build();

    }

    @GET
    @Path("movimenti")
    public Response movimenti() {

        // traceService.trace("Cruscotti", this.getClass().getName(), "/movimenti",
        // "Accesso visualizzazione movimenti PM", Constants.CLICK);

        String currentYear = DateUtils.dateToString(new Date(), "yyyy");
        String currentMonth = DateUtils.dateToString(new Date(), "MM");
        String finePeriodoRicalcolo = configurationService.getParameter("FINE_PERIODO_RIPARTIZIONE");
        List<String> tipoProgrammi = programmaMusicaleService.getTipoProgrammi();

        Long annoFinePeriodoRicalcolo = Long.parseLong(finePeriodoRicalcolo.substring(0, 4));
        Long meseFinePeriodoRicalcolo = Long.parseLong(finePeriodoRicalcolo.substring(4, 6));

        // if (meseFinePeriodoRicalcolo==12) {
        // model.addAttribute("meseInizioPeriodoContabile", "01");
        // model.addAttribute("annoInizioPeriodoContabile", annoFinePeriodoRicalcolo+1);
        // }else {
        // if (meseFinePeriodoRicalcolo<9)
        // model.addAttribute("meseInizioPeriodoContabile",
        // "0"+meseFinePeriodoRicalcolo);
        // else
        // model.addAttribute("meseInizioPeriodoContabile", meseFinePeriodoRicalcolo);
        //
        // model.addAttribute("annoInizioPeriodoContabile", annoFinePeriodoRicalcolo);
        // }
        //
        // model.addAttribute("meseFinePeriodoContabile", currentMonth);
        // model.addAttribute("annoFinePeriodoContabile", currentYear);
        //
        // model.addAttribute("tipoProgrammi", tipoProgrammi);
        //
        // session.setAttribute("resultPage", null);
        // model.addAttribute("resultPage");

        // ReportCache.clearReports();

        // return "cruscottiMovimenti";
        return Response.ok().build();
    }

    @GET
    @Path("movimentiList")
    public Response movimentiList(@QueryParam(value = "meseInizioPeriodoContabile") String meseInizioPeriodoContabile,
                                  @QueryParam(value = "annoInizioPeriodoContabile") String annoInizioPeriodoContabile,
                                  @QueryParam(value = "meseFinePeriodoContabile") String meseFinePeriodoContabile,
                                  @QueryParam(value = "annoFinePeriodoContabile") String annoFinePeriodoContabile,
                                  @QueryParam(value = "tipoDocumento") String tipoDocumento, @QueryParam(value = "seprag") String seprag,
                                  @QueryParam(value = "evento") Long evento, @QueryParam(value = "fattura") String fattura,
                                  @QueryParam(value = "reversale") Long reversale, @QueryParam(value = "voceIncasso") String voceIncasso,
                                  @QueryParam(value = "permesso") String permesso, @QueryParam(value = "numeroPM") String numeroPM,
                                  @QueryParam(value = "locale") String locale, @QueryParam(value = "codiceBALocale") String codiceBALocale,
                                  @QueryParam(value = "sapOrganizzatore") String sapOrganizzatore,
                                  @QueryParam(value = "organizzatore") String organizzatore,
                                  @QueryParam(value = "dataInizioEvento") String dataInizioEvento,
                                  @QueryParam(value = "dataFineEvento") String dataFineEvento,
                                  @QueryParam(value = "titoloOpera") String titoloOpera,
                                  @QueryParam(value = "tipoSupporto") String tipoSupporto,
                                  @QueryParam(value = "tipoProgramma") String tipoProgramma,
                                  @QueryParam(value = "gruppoPrincipale") String gruppoPrincipale,
                                  @QueryParam(value = "direttoreEsecuzione") String direttoreEsecuzione,
                                  @QueryParam(value = "page") Integer page, @QueryParam(value = "count") Boolean count,
                                  @QueryParam(value = "order") String order) {

        // traceService.trace("Cruscotti", this.getClass().getName(), "/movimentiList",
        // "Lista movimenti PM", Constants.CLICK);

        if (page == null)
            page = 0;

        if (meseInizioPeriodoContabile != null && meseInizioPeriodoContabile.equals(""))
            meseInizioPeriodoContabile = null;
        if (annoInizioPeriodoContabile != null && annoInizioPeriodoContabile.equals(""))
            annoInizioPeriodoContabile = null;
        if (meseFinePeriodoContabile != null && meseFinePeriodoContabile.equals(""))
            meseFinePeriodoContabile = null;
        if (annoFinePeriodoContabile != null && annoFinePeriodoContabile.equals(""))
            annoFinePeriodoContabile = null;

        if (tipoDocumento != null && tipoDocumento.equals(""))
            tipoDocumento = null;
        if (seprag != null && seprag.equals(""))
            seprag = null;
        if (evento != null && evento.equals(""))
            evento = null;
        if (fattura != null && fattura.equals(""))
            fattura = null;
        if (reversale != null && reversale.equals(""))
            reversale = null;
        if (voceIncasso != null && voceIncasso.equals(""))
            voceIncasso = null;
        if (permesso != null && permesso.equals(""))
            permesso = null;
        if (numeroPM != null && numeroPM.equals(""))
            numeroPM = null;
        if (locale != null && locale.equals(""))
            locale = null;
        if (codiceBALocale != null && codiceBALocale.equals(""))
            codiceBALocale = null;
        if (sapOrganizzatore != null && sapOrganizzatore.equals(""))
            sapOrganizzatore = null;
        if (organizzatore != null && organizzatore.equals(""))
            organizzatore = null;
        if (dataInizioEvento != null && dataInizioEvento.equals(""))
            dataInizioEvento = null;
        if (dataFineEvento != null && dataFineEvento.equals(""))
            dataFineEvento = null;
        if (titoloOpera != null && titoloOpera.equals(""))
            titoloOpera = null;

        if (tipoSupporto != null && tipoSupporto.equals(""))
            tipoSupporto = null;
        if (tipoProgramma != null && tipoProgramma.equals(""))
            tipoProgramma = null;
        if (direttoreEsecuzione != null && direttoreEsecuzione.equals(""))
            direttoreEsecuzione = null;

        if (meseInizioPeriodoContabile != null && meseInizioPeriodoContabile.length() == 1)
            meseInizioPeriodoContabile = "0" + meseInizioPeriodoContabile;
        if (meseFinePeriodoContabile != null && meseFinePeriodoContabile.length() == 1)
            meseFinePeriodoContabile = "0" + meseFinePeriodoContabile;

        if (count == null)
            count = false;

        String inizioPeriodoContabile = null;
        Long concatInizioPeriodo = null;
        String finePeriodoContabile = null;
        Long concatFinePeriodo = null;
        if (annoInizioPeriodoContabile != null && meseInizioPeriodoContabile != null) {
            inizioPeriodoContabile = annoInizioPeriodoContabile + meseInizioPeriodoContabile;
            concatInizioPeriodo = Long.parseLong(inizioPeriodoContabile);
        }
        if (annoFinePeriodoContabile != null && meseFinePeriodoContabile != null) {
            finePeriodoContabile = annoFinePeriodoContabile + meseFinePeriodoContabile;
            concatFinePeriodo = Long.parseLong(finePeriodoContabile);
        }

        Date dataEventoInizio = null;
        if (dataInizioEvento != null)
            dataEventoInizio = DateUtils.stringToDate(dataInizioEvento, DateUtils.DATE_ITA_FORMAT_DASH);
        Date dataEventoFine = null;
        if (dataFineEvento != null)
            dataEventoFine = DateUtils.stringToDate(dataFineEvento, DateUtils.DATE_ITA_FORMAT_DASH);

        try {
            if (count == true) {
                long totRecords = programmaMusicaleService.getCountListaMovimentazioni(concatInizioPeriodo,
                        concatFinePeriodo, evento, fattura, reversale, voceIncasso, dataEventoInizio, dataEventoFine,
                        seprag, tipoDocumento, locale, sapOrganizzatore, tipoSupporto, tipoProgramma,
                        direttoreEsecuzione, titoloOpera, numeroPM, permesso, page, codiceBALocale, organizzatore);
                return Response.ok(gson.toJson(totRecords)).build();
            } else {
                ReportPage resultPage = programmaMusicaleService.getListaMovimentazioni(concatInizioPeriodo,
                        concatFinePeriodo, evento, fattura, reversale, voceIncasso, dataEventoInizio, dataEventoFine,
                        seprag, tipoDocumento, locale, sapOrganizzatore, tipoSupporto, tipoProgramma,
                        direttoreEsecuzione, titoloOpera, numeroPM, permesso, page, codiceBALocale, organizzatore,
                        order);
                return Response.ok(gson.toJson(resultPage)).build();
            }
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: handle exception
        }
        return Response.status(500).build();
    }

    @GET
    @Path("movimentiDetail")
    public Response movimentiDetail(@QueryParam(value = "movimento") Long movimento) {

        // traceService.trace("Cruscotti", this.getClass().getName(),
        // "/movimentiDetail", "Visualizzazione dettaglio movimento", Constants.CLICK);

        List<EventiPagati> eventoPagato = programmaMusicaleService.getDettagliMovimento(movimento);
        // model.addAttribute("evento", eventoPagato);
        // model.addAttribute("movimento", movimento);
        //
        // return "cruscottiMovimentiDettaglio";
        // return Response.ok(eventoPagato).build();
        return Response.ok(gson.toJson(eventoPagato)).build();
    }

    @GET
    @Path("pmDetail")
    public Response pmDetail(@QueryParam(value = "idProgrammaMusicale") Long idProgrammaMusicale,
                             @QueryParam(value = "idMovimento") Long idMovimento) {

        //// traceService.trace("Cruscotti", this.getClass().getName(),
        //// "/pmDetail","Visualizzazione dettaglio PM", Constants.CLICK);

        ProgrammaMusicale programmaMusicale = null;

        try {
            if (idMovimento != null)
                programmaMusicale = programmaMusicaleService.getDettaglioPM(idProgrammaMusicale, idMovimento);
            else
                programmaMusicale = programmaMusicaleService.getDettaglioPM(idProgrammaMusicale);
        } catch (Exception e) {
            return Response.status(404).entity(gson.toJson(e)).build();
        }
        // model.addAttribute("programmaMusicale", programmaMusicale);

        // return "cruscottiProgrammaMusicaleDettaglio";
        return Response.ok(gson.toJson(programmaMusicale)).build();
    }

    @GET
    @Path("listaPmDisponibili")
    public Response listaPmDisponibili(@QueryParam(value = "numeroPm") String numeroPm,
                                       @QueryParam(value = "page") Integer page) {

        if (page == null)
            page = 0;

        try {
            ReportPage result = programmaMusicaleService.getListaPmDisponibili(numeroPm, page);
            return Response.ok(gson.toJson(result)).build();
        } catch (Exception e) {
            System.out.println("ERROR ON PM DISPONIBILI DETAIL: " + e);
        }

        return Response.status(500).build();
    }

    @GET
    @Path("fattureList")
    public Response fattureList(@QueryParam(value = "sede") String sede, @QueryParam(value = "seprag") String seprag,
                                @QueryParam(value = "agenzia") String agenzia, @QueryParam(value = "fattura") String fattura,
                                @QueryParam(value = "reversale") String reversale, @QueryParam(value = "voceIncasso") String voceIncasso,
                                @QueryParam(value = "organizzatore") String organizzatore,
                                @QueryParam(value = "codiceSap") String codiceSap,
                                @QueryParam(value = "tipoDocumento") String tipoDocumento, @QueryParam(value = "idEvento") Long idEvento,
                                @QueryParam(value = "dataFatturaDa") String dataFatturaDa, @QueryParam(value = "dataFatturaA") String dataFatturaA,
                                @QueryParam(value = "dataEventoDa") String dataEventoDa, @QueryParam(value = "dataEventoA") String dataEventoA,
                                @QueryParam(value = "importo") Double importo,
                                @QueryParam(value = "fatturaValidata") String fatturaValidata,
                                @QueryParam(value = "megConcerto" ) String megaConcerto,
                                @QueryParam(value = "page") Integer page,
                                @QueryParam(value = "order") String order,
                                @QueryParam(value = "count") Boolean count) {

        if (page == null)
            page = 0;
        if (count == null || count.equals(""))
            count = false;

        if (sede != null && sede.equals(""))
            sede = null;
        if (seprag != null && seprag.equals(""))
            seprag = null;
        if (agenzia != null && agenzia.equals(""))
            agenzia = null;
        if (fattura != null && fattura.equals(""))
            fattura = null;
        if (reversale != null && reversale.equals(""))
            reversale = null;
        if (voceIncasso != null && voceIncasso.equals(""))
            voceIncasso = null;
        if (organizzatore != null && organizzatore.equals(""))
            organizzatore = null;
        if (codiceSap != null && codiceSap.equals(""))
            codiceSap = null;
        if (tipoDocumento != null && tipoDocumento.equals(""))
            tipoDocumento = null;
        if (idEvento != null && idEvento.equals(""))
            idEvento = null;
        if (dataFatturaDa != null && dataFatturaDa.equals(""))
            dataFatturaDa = null;
        if (dataFatturaA != null && dataFatturaA.equals(""))
            dataFatturaA = null;
        if (dataEventoDa != null && dataEventoDa.equals(""))
            dataEventoDa = null;
        if (dataEventoA != null && dataEventoA.equals(""))
            dataEventoA = null;
        if (importo != null && importo.equals(""))
            importo = null;
        if (fatturaValidata == null)
            fatturaValidata = "";

        Manifestazione manifestazione = new Manifestazione();
        EventiPagati eventoPagato = new EventiPagati();
        MovimentoContabile movimentoContabile = new MovimentoContabile();
        eventoPagato.setSede(sede);// TODO sede
        eventoPagato.setSeprag(seprag);
        eventoPagato.setAgenzia(agenzia);// TODO agenzia
        eventoPagato.setNumeroFattura(fattura);
        eventoPagato.setReversale(reversale);
        eventoPagato.setVoceIncasso(voceIncasso);
        if (organizzatore != null && Character.isDigit(organizzatore.charAt(0))) {
            manifestazione.setPartitaIvaOrganizzatore(organizzatore);
        }
        if (organizzatore != null && Character.isLetter(organizzatore.charAt(0))) {
            manifestazione.setCodiceFiscaleOrganizzatore(organizzatore);
        }
        manifestazione.setCodiceSapOrganizzatore(codiceSap);
        eventoPagato.setTipoDocumentoContabile(tipoDocumento);
        eventoPagato.setIdEvento(idEvento);
        if (dataEventoDa != null) {
            eventoPagato.setDataEventoDa(DateUtils.stringToDate(dataEventoDa, DateUtils.DATE_ITA_FORMAT_DASH));
        }
        if (dataEventoA != null) {
            eventoPagato.setDataEventoA(DateUtils.stringToDate(dataEventoA, DateUtils.DATE_ITA_FORMAT_DASH));
        }


        if (dataFatturaDa != null) {
            eventoPagato.setDataFatturaDa(DateUtils.stringToDate(dataFatturaDa, DateUtils.DATE_ITA_FORMAT_DASH));
        }
        if (dataFatturaA != null) {
            eventoPagato.setDataFatturaA(DateUtils.stringToDate(dataFatturaA, DateUtils.DATE_ITA_FORMAT_DASH));
        }


                eventoPagato.setImportDem(importo);
        eventoPagato.setManifestazione(manifestazione);

        if (fattura != null) {
            movimentoContabile.setNumeroFattura(Long.parseLong(fattura));
        }
        if (reversale != null) {
            movimentoContabile.setReversale(Long.parseLong(reversale));
        }
        movimentoContabile.setVoceIncasso(voceIncasso);
        movimentoContabile.setTipoDocumentoContabile(tipoDocumento);
        movimentoContabile.setIdEvento(idEvento);
        // movimentoContabile.setDataIns(dataFattura);
        movimentoContabile.setImportoTotDem(importo);
        movimentoContabile.setEvento(manifestazione);

        try {
            if (count == true) {
                long totRecords = programmaMusicaleService.getCountListaFatture(eventoPagato, movimentoContabile,
                        fatturaValidata);
                return Response.ok(gson.toJson(totRecords)).build();
            } else {
                ReportPage resultPage = programmaMusicaleService.getListaFatture(eventoPagato, movimentoContabile,
                        fatturaValidata, page, order);
                return Response.ok(gson.toJson(resultPage)).build();
            }
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: handle exception
        }

        return Response.status(500).build();

    }

    @GET
    @Path("fattureDetailEventi")
    public Response fattureDetailEventi(@QueryParam(value = "numeroFattura") String numeroFattura) {
        try {
            List<EventiPagati> eventoPagato = programmaMusicaleService.getDettaglioFattureOnEvento(numeroFattura);
            return Response.ok(gson.toJson(eventoPagato)).build();
        } catch (Exception e) {
            System.out.println("ERROR ON FATTURE DETAIL: " + e);
        }
        return Response.status(500).build();
    }

    @GET
    @Path("fattureDetailMovimenti")
    public Response fattureDetailMovimenti(@QueryParam(value = "numeroFattura") String numeroFattura) {
        try {
            List<MovimentoContabile> movimento = programmaMusicaleService.getDettaglioFattureOnMovimento(numeroFattura);
            return Response.ok(gson.toJson(movimento)).build();
        } catch (Exception e) {
            System.out.println("ERROR ON FATTURE DETAIL: " + e);
        }
        return Response.status(500).build();
    }

    @GET
    @Path("fattureDetail")
    public Response fattureDetail(@QueryParam(value = "numeroFattura") String numeroFattura) {
        try {
            FatturaDetailDTO fattura = programmaMusicaleService.getFatturaDetail(numeroFattura);
            return Response.ok(gson.toJson(fattura)).build();
        } catch (Exception e) {
            return Response.status(500).entity(gson.toJson(e)).build();
        }
    }
    
    @POST
    @Path("eventi/riconcilia-importi")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response riconciliaImporti(RiconciliazioneImportiDTO riconciliazioneImporti, @Context HttpServletRequest request) {
		HttpSession session = request.getSession();
		String username = (String)session.getAttribute("sso.user.userName");
        try {
            FatturaDetailDTO fattura = programmaMusicaleService.riconciliaImporti(riconciliazioneImporti, username);
            return Response.ok(gson.toJson(fattura)).build();
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            return Response.status(500).entity(gson.toJson(e)).build();
        }
    }
    
    @POST
    @Path("eventi/riconcilia-voci-incasso")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response riconciliaVociIncasso(RiconciliazioneImportiDTO riconciliazioneImporti, @Context HttpServletRequest request) {
		HttpSession session = request.getSession();
		String username = (String)session.getAttribute("sso.user.userName");
        try {
            FatturaDetailDTO fattura = programmaMusicaleService.riconciliaVociIncasso(riconciliazioneImporti, username);
            return Response.ok(gson.toJson(fattura)).build();
        } catch (Exception e) {
        	logger.error(e.getMessage(),e);
            return Response.status(500).entity(gson.toJson(e)).build();
        }
    }
    
    @POST
    @Path("eventi/ripristina-riconciliazione")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response ripristinaRiconciliazione(RiconciliazioneImportiDTO riconciliazioneImporti, @Context HttpServletRequest request) {
		HttpSession session = request.getSession();
		String username = (String)session.getAttribute("sso.user.userName");
        try {
            FatturaDetailDTO fattura = programmaMusicaleService.ripristinaRiconciliazione(riconciliazioneImporti, username);
            return Response.ok(gson.toJson(fattura)).build();
        } catch (Exception e) {
        	logger.error(e.getMessage(),e);
            return Response.status(500).entity(gson.toJson(e)).build();
        }
    }    

    @PUT
    @Path("updateFlagMegaConcert")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateFlagMegaConcert(@QueryParam(value = "userId") String userId,
                                          FatturaDetailDTO fatturaDetailDTO) {
        try {
            String message = programmaMusicaleService.updateFlagMegaConcert(fatturaDetailDTO);

            traceService.trace("Cruscotti", this.getClass().getName(), "/updateFlagMegaConcert",
                    "Modificato flag megaconcerto nella manifestazione: " + fatturaDetailDTO.getManifestazione().getId()
                            + ", valore precedente: "
                            + ((fatturaDetailDTO.getManifestazione().getFlagMegaConcert() == true) ? "0" : "1"),
                    Constants.UPDATE, userId);

            if (message.startsWith("Non")) {
                return Response.status(500).entity(gson.toJson(message)).build();
            }
            return Response.ok(gson.toJson(message)).build();

        } catch (Exception e) {
            return Response.status(500).entity(gson.toJson(e)).build();
        }
    }

    @GET
    @Path("fattureImportoTotByIdEvento")
    public Response fattureImportoTotByIdEvento(@QueryParam(value = "idEvento") Double idEvento) {
        try {
            return Response.ok(gson.toJson(programmaMusicaleService.fattureImportoTotByIdEvento(idEvento))).build();
        } catch (Exception e) {
            System.out.println("ERROR ON FATTURE DETAIL: " + e);
        }
        return Response.status(500).build();
    }

    @POST
    @Path("addOpera")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addOpera(@QueryParam(value = "userId") String userId,
                             PerfUtilizzazioniDTO perfUtilizzazioniAddDTO) {

        try {
            Utilizzazione utilizzazione = new Utilizzazione();
            utilizzazione.setIdProgrammaMusicale(perfUtilizzazioniAddDTO.getIdProgrammaMusicale());
            utilizzazione.setCodiceOpera(perfUtilizzazioniAddDTO.getCodiceOpera());
            utilizzazione.setTitoloComposizione(perfUtilizzazioniAddDTO.getTitolo());
            if (perfUtilizzazioniAddDTO.getCompositore() != null) {
                utilizzazione.setCompositore(perfUtilizzazioniAddDTO.getCompositore());
            } else if (perfUtilizzazioniAddDTO.getCompositori() != null) {
                utilizzazione.setCompositore(perfUtilizzazioniAddDTO.getCompositori());
            }
            utilizzazione.setNumeroProgrammaMusicale(perfUtilizzazioniAddDTO.getNumeroProgrammaMusicale());
            utilizzazione.setDurata(perfUtilizzazioniAddDTO.getDurata());
            utilizzazione.setDurataInferiore30sec(perfUtilizzazioniAddDTO.getMenodi30sec());
            utilizzazione.setAutori(perfUtilizzazioniAddDTO.getAutori());
            utilizzazione.setInterpreti(perfUtilizzazioniAddDTO.getInterpreti());
            utilizzazione.setFlagCodifica(perfUtilizzazioniAddDTO.getFlagCodifica());
            utilizzazione.setCombinazioneCodifica(perfUtilizzazioniAddDTO.getCombinazioneCodifica());
            utilizzazione.setFlagPubblicoDominio(perfUtilizzazioniAddDTO.getElaboratorePubblicoDominio());
            utilizzazione.setFlagCedolaEsclusa(perfUtilizzazioniAddDTO.getFlagCedolaEsclusa());
            utilizzazione.setFlagMagNonConcessa(perfUtilizzazioniAddDTO.getFlagMagNonConcessa());
            utilizzazione.setTipoAccertamento(perfUtilizzazioniAddDTO.getTipoAccertamento());
            utilizzazione.setNettoMaturato(perfUtilizzazioniAddDTO.getNettomaturato());
            if (perfUtilizzazioniAddDTO.getDataPrimaCodifica() != null)
                utilizzazione.setDataPrimaCodifica(DateUtils
                        .stringToDate(perfUtilizzazioniAddDTO.getDataPrimaCodifica(), DateUtils.DATE_ITA_FORMAT_DASH));
            utilizzazione.setNettoOriginario(perfUtilizzazioniAddDTO.getNettomaturato());

            int result = programmaMusicaleService.addUtilizzazione(utilizzazione);
            if (result == 1) {
                traceService
                        .trace("Cruscotti", this.getClass().getName(), "/addOpera",
                                "Opera aggiunta: " + perfUtilizzazioniAddDTO.toString()
                                        + " nel programma musicale con id: " + utilizzazione.getIdProgrammaMusicale(),
                                Constants.ADD, userId);
                return Response.ok().build();
            } else {
                return Response.status(500).build();
            }

        } catch (Exception e) {
            return Response.status(500).build();
        }
    }

    @PUT
    @Path("updateOpera")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateOpera(@QueryParam(value = "userId") String userId,
                                PerfUtilizzazioniDTO perfUtilizzazioniAddDTO) {
        Utilizzazione opera;
        try {
            opera = programmaMusicaleService.getOpera(perfUtilizzazioniAddDTO.getId());
            programmaMusicaleService.updateOpera(perfUtilizzazioniAddDTO);

            String updatedValues = "Modificata l'opera con id: " + opera.getId() + " dal programma musicale: "
                    + opera.getIdProgrammaMusicale() + ". Parametri precedenti:";

            if (perfUtilizzazioniAddDTO.getCodiceOpera() != null)
                updatedValues = updatedValues + " codiceOpera = " + opera.getCodiceOpera() + ",";

            if (perfUtilizzazioniAddDTO.getTitolo() != null)
                updatedValues = updatedValues + " titolo composizione = " + opera.getTitoloComposizione() + ",";

            if (perfUtilizzazioniAddDTO.getDurataEsecuzione() != null)
                updatedValues = updatedValues + " durata = " + opera.getDurata() + ",";

            if (perfUtilizzazioniAddDTO.getMenodi30sec() != null)
                updatedValues = updatedValues + " durata meno 30 secondi = " + opera.getDurataInferiore30sec() + ",";

            if (perfUtilizzazioniAddDTO.getCompositori() != null)
                updatedValues = updatedValues + " compositori = " + opera.getCompositore() + ",";

            if (perfUtilizzazioniAddDTO.getAutori() != null)
                updatedValues = updatedValues + " autori = " + opera.getAutori() + ",";

            if (perfUtilizzazioniAddDTO.getInterpreti() != null)
                updatedValues = updatedValues + " interpreti = " + opera.getInterpreti() + ",";

            if (perfUtilizzazioniAddDTO.getFlagCodifica() != null)
                updatedValues = updatedValues + " flag codifica manuale = " + opera.getFlagCodifica() + ",";

            if (perfUtilizzazioniAddDTO.getCombinazioneCodifica() != null)
                updatedValues = updatedValues + " combinazione codifica = " + opera.getCombinazioneCodifica() + ",";

            if (perfUtilizzazioniAddDTO.getElaboratorePubblicoDominio() != null)
                updatedValues = updatedValues + " elaboratore pubblico dominio = " + opera.getFlagPubblicoDominio()
                        + ",";

            if (perfUtilizzazioniAddDTO.getFlagMagNonConcessa() != null)
                updatedValues = updatedValues + " flag mag non concessa = " + opera.getFlagMagNonConcessa() + ",";

            if (perfUtilizzazioniAddDTO.getTipoAccertamento() != null)
                updatedValues = updatedValues + " tipo accertamento = " + opera.getTipoAccertamento() + ",";

            if (perfUtilizzazioniAddDTO.getNettomaturato() != null)
                updatedValues = updatedValues + " netto maturato = " + opera.getNettoMaturato() + ",";

            if (perfUtilizzazioniAddDTO.getDataPrimaCodifica() != null)
                updatedValues = updatedValues + " data prima codifica = " + opera.getDataPrimaCodifica() + ",";

            traceService.trace("Cruscotti", this.getClass().getName(), "/updateOpera", updatedValues, Constants.UPDATE,
                    userId);

            return Response.ok().build();
        } catch (Exception e) {
            return Response.status(500).build();
        }
    }

    @DELETE
    @Path("removeOpera")
    public Response removeOpera(@QueryParam(value = "userId") String userId,
                                @QueryParam(value = "idUtilizzazione") Long idUtilizzazione) {
        try {
            Utilizzazione operaPrec = programmaMusicaleService.getOpera(idUtilizzazione);
            programmaMusicaleService.removeOpera(idUtilizzazione);

            traceService.trace(
                    "Cruscotti", this.getClass().getName(), "/removeOpera", "Rimossa l'opera con id: " + idUtilizzazione
                            + " dal programma musicale con id: " + operaPrec.getIdProgrammaMusicale(),
                    Constants.DELETE, userId);
            return Response.ok().build();
        } catch (Exception e) {
            System.out.println("ERROR " + e);
            return Response.status(500).build();
        }
    }

    @GET
    @Path("aggregazioniIncassi")
    public Response aggregazioniIncassi() {

        // traceService.trace("Cruscotti", this.getClass().getName(),
        // "/aggregazioniIncassi", "Accesso report aggregazioni Incassi",
        // Constants.CLICK);

        List<Circoscrizione> circoscrizioni = configurationService.getCircoscrizioni();

        // model.addAttribute("circoscrizioni", circoscrizioni);
        //
        // session.setAttribute("resultPage", null);
        // model.addAttribute("resultPage");
        //
        // ReportCache.clearReports();
        //
        // return "cruscottoAggregatiIncassi";
        return Response.ok(circoscrizioni).build();
    }

    @GET
    @Path("aggregazioniIncassiList")
    public Response aggregazioniIncassiList(
            @QueryParam(value = "meseInizioPeriodoContabile") String meseInizioPeriodoContabile,
            @QueryParam(value = "annoInizioPeriodoContabile") String annoInizioPeriodoContabile,
            @QueryParam(value = "meseFinePeriodoContabile") String meseFinePeriodoContabile,
            @QueryParam(value = "annoFinePeriodoContabile") String annoFinePeriodoContabile,
            @QueryParam(value = "idPuntoTerritoriale") String idPuntoTerritoriale,
            @QueryParam(value = "page") Integer page) {

        // traceService.trace("Cruscotti",
        // this.getClass().getName(),"/aggregazioniIncassiList", "Aggregazione incassi",
        // Constants.CLICK);

        String seprag = null;

        if (idPuntoTerritoriale != null && "".equals(idPuntoTerritoriale))
            idPuntoTerritoriale = null;

        if (idPuntoTerritoriale != null && idPuntoTerritoriale.length() < 7) {
            seprag = "0" + seprag;
        } else {
            seprag = idPuntoTerritoriale;
        }

        if (meseInizioPeriodoContabile.length() == 1)
            meseInizioPeriodoContabile = "0" + meseInizioPeriodoContabile;
        if (meseFinePeriodoContabile.length() == 1)
            meseFinePeriodoContabile = "0" + meseFinePeriodoContabile;

        String inizioPeriodoContabile = annoInizioPeriodoContabile + meseInizioPeriodoContabile;
        String finePeriodoContabile = annoFinePeriodoContabile + meseFinePeriodoContabile;

        // model.addAttribute("resultPage", resultPage);
        //
        // model.addAttribute("meseInizioPeriodoContabile", meseInizioPeriodoContabile);
        // model.addAttribute("annoInizioPeriodoContabile", annoInizioPeriodoContabile);
        // model.addAttribute("meseFinePeriodoContabile", meseFinePeriodoContabile);
        // model.addAttribute("annoFinePeriodoContabile", annoFinePeriodoContabile);
        // model.addAttribute("elabSelected", idPuntoTerritoriale);
        // model.addAttribute("circoscrizioni", circoscrizioni);
        //
        // return "cruscottoAggregatiIncassi";

        try {
            ReportPage resultPage = programmaMusicaleService.getListaAggregatiEventi(
                    Long.parseLong(inizioPeriodoContabile), Long.parseLong(finePeriodoContabile), seprag);

            List<Circoscrizione> circoscrizioni = configurationService.getCircoscrizioni();
            return Response.ok(gson.toJson(resultPage)).build();
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: handle exception
        }
        return Response.status(500).build();
    }

    @GET
    @Path("aggregazioniMovimenti")
    public Response aggregazioniMovimenti() {

        // traceService.trace("Cruscotti", this.getClass().getName(),
        // "/aggregazioniMovimenti", "Accesso report aggregazioni Movimenti",
        // Constants.CLICK);

        List<Circoscrizione> circoscrizioni = configurationService.getCircoscrizioni();

        // model.addAttribute("circoscrizioni", circoscrizioni);
        //
        // session.setAttribute("resultPage", null);
        // model.addAttribute("resultPage");
        //
        // ReportCache.clearReports();
        //
        // return "cruscottoAggregatiMovimenti";
        return Response.ok(circoscrizioni).build();
    }

    @GET
    @Path("aggregazioniMovimentiList")
    public Response aggregazioniMovimentiList(
            @QueryParam(value = "meseInizioPeriodoContabile") String meseInizioPeriodoContabile,
            @QueryParam(value = "annoInizioPeriodoContabile") String annoInizioPeriodoContabile,
            @QueryParam(value = "meseFinePeriodoContabile") String meseFinePeriodoContabile,
            @QueryParam(value = "annoFinePeriodoContabile") String annoFinePeriodoContabile,
            @QueryParam(value = "idPuntoTerritoriale") String idPuntoTerritoriale,
            @QueryParam(value = "correntiArretrati") String correntiArretrati,
            @QueryParam(value = "page") Integer page) {

        // traceService.trace("Cruscotti", this.getClass().getName(),
        // "/aggregazioniMovimentiList", "Aggregazione incassi", Constants.CLICK);

        String seprag = null;

        if (idPuntoTerritoriale != null && "".equals(idPuntoTerritoriale))
            idPuntoTerritoriale = null;

        if (idPuntoTerritoriale != null && idPuntoTerritoriale.length() < 7) {
            seprag = "0" + seprag;
        } else {
            seprag = idPuntoTerritoriale;
        }

        if (page == null)
            page = 1;

        if (meseInizioPeriodoContabile.length() == 1)
            meseInizioPeriodoContabile = "0" + meseInizioPeriodoContabile;
        if (meseFinePeriodoContabile.length() == 1)
            meseFinePeriodoContabile = "0" + meseFinePeriodoContabile;

        String inizioPeriodoContabile = annoInizioPeriodoContabile + meseInizioPeriodoContabile;
        String finePeriodoContabile = annoFinePeriodoContabile + meseFinePeriodoContabile;

        List<ReportPage> reports = programmaMusicaleService.getListaAggregatiMovimenti(
                Long.parseLong(inizioPeriodoContabile), Long.parseLong(finePeriodoContabile), seprag, page,
                correntiArretrati);

        List<Circoscrizione> circoscrizioni = configurationService.getCircoscrizioni();

        // model.addAttribute("listaImportiCorrenti", reports.get(0));
        // model.addAttribute("listaCedolePMCorrenti", reports.get(1));
        //
        // model.addAttribute("listaImportiArretrati", reports.get(2));
        // model.addAttribute("listaCedolePMArretrati", reports.get(3));
        //
        // model.addAttribute("meseInizioPeriodoContabile", meseInizioPeriodoContabile);
        // model.addAttribute("annoInizioPeriodoContabile", annoInizioPeriodoContabile);
        // model.addAttribute("meseFinePeriodoContabile", meseFinePeriodoContabile);
        // model.addAttribute("annoFinePeriodoContabile", annoFinePeriodoContabile);
        // model.addAttribute("elabSelected", idPuntoTerritoriale);
        // model.addAttribute("circoscrizioni", circoscrizioni);
        // model.addAttribute("correntiArretrati", correntiArretrati);
        //
        // return "cruscottoAggregatiMovimenti";
        return Response.ok(gson.toJson(reports)).build();
    }

    @GET
    @Path("aggregazioniSiada")
    public Response aggregazioniSiada() {

        // traceService.trace("Cruscotti",
        // this.getClass().getName(),"/aggregazioniSiada", "Accesso visualizzazione
        // aggregazioniSiada", Constants.CLICK);

        String currentYear = DateUtils.dateToString(new Date(), "yyyy");
        String currentMonth = DateUtils.dateToString(new Date(), "MM");
        String finePeriodoRicalcolo = configurationService.getParameter("FINE_PERIODO_RIPARTIZIONE");
        List<String> tipoProgrammi = programmaMusicaleService.getTipoProgrammi();

        Long annoFinePeriodoRicalcolo = Long.parseLong(finePeriodoRicalcolo.substring(0, 4));
        Long meseFinePeriodoRicalcolo = Long.parseLong(finePeriodoRicalcolo.substring(4, 6));
        //
        // if (meseFinePeriodoRicalcolo==12) {
        // model.addAttribute("meseInizioPeriodoContabile", "01");
        // model.addAttribute("annoInizioPeriodoContabile", annoFinePeriodoRicalcolo+1);
        // }else {
        // if (meseFinePeriodoRicalcolo<9)
        // model.addAttribute("meseInizioPeriodoContabile",
        // "0"+meseFinePeriodoRicalcolo);
        // else
        // model.addAttribute("meseInizioPeriodoContabile", meseFinePeriodoRicalcolo);
        //
        // model.addAttribute("annoInizioPeriodoContabile", annoFinePeriodoRicalcolo);
        // }
        //
        // model.addAttribute("meseFinePeriodoContabile", currentMonth);
        // model.addAttribute("annoFinePeriodoContabile", currentYear);
        //
        // model.addAttribute("tipoProgrammi", tipoProgrammi);
        // session.setAttribute("resultPage", null);
        // model.addAttribute("resultPage");
        //
        // ReportCache.clearReports();
        //
        // return "cruscottoAggregatiSiada";
        return Response.ok().build();
    }

    @GET
    @Path("aggregatiList")
    public Response aggregatiList(@QueryParam(value = "meseInizioPeriodoContabile") String meseInizioPeriodoContabile,
                                  @QueryParam(value = "annoInizioPeriodoContabile") String annoInizioPeriodoContabile,
                                  @QueryParam(value = "meseFinePeriodoContabile") String meseFinePeriodoContabile,
                                  @QueryParam(value = "annoFinePeriodoContabile") String annoFinePeriodoContabile,
                                  @QueryParam(value = "voceIncasso") String voceIncasso,
                                  @QueryParam(value = "tipoSupporto") String tipoSupporto,
                                  @QueryParam(value = "tipoProgramma") String tipoProgramma, @QueryParam(value = "numeroPM") String numeroPM,
                                  @QueryParam(value = "page") Integer page) {

        // traceService.trace("Cruscotti", this.getClass().getName(), "/aggregatiList",
        // "Lista movimenti PM", Constants.CLICK);

        Long numPM = null;

        if (page == null)
            page = 1;

        if (voceIncasso != null && voceIncasso.equals(""))
            voceIncasso = null;
        if (tipoSupporto != null && tipoSupporto.equals(""))
            tipoSupporto = null;
        if (tipoProgramma != null && tipoProgramma.equals(""))
            tipoProgramma = null;
        if (numeroPM != null && numeroPM.equals(""))
            numeroPM = null;
        if (meseInizioPeriodoContabile.length() == 1)
            meseInizioPeriodoContabile = "0" + meseInizioPeriodoContabile;
        if (meseFinePeriodoContabile.length() == 1)
            meseFinePeriodoContabile = "0" + meseFinePeriodoContabile;

        String inizioPeriodoContabile = annoInizioPeriodoContabile + meseInizioPeriodoContabile;
        String finePeriodoContabile = annoFinePeriodoContabile + meseFinePeriodoContabile;

        if (numeroPM != null)
            numPM = Long.parseLong(numeroPM);

        // model.addAttribute("resultPage", resultPage);

        // model.addAttribute("meseInizioPeriodoContabile", meseInizioPeriodoContabile);
        // model.addAttribute("annoInizioPeriodoContabile", annoInizioPeriodoContabile);
        // model.addAttribute("meseFinePeriodoContabile", meseFinePeriodoContabile);
        // model.addAttribute("annoFinePeriodoContabile", annoFinePeriodoContabile);
        // model.addAttribute("voceIncasso", voceIncasso);
        // model.addAttribute("tipoSupporto", tipoSupporto);
        // model.addAttribute("tipoProgramma", tipoProgramma);
        // model.addAttribute("numeroPM", numeroPM);
        // model.addAttribute("tipoProgrammi", tipoProgrammi);
        //
        // return "cruscottoAggregatiSiada"; try {

        try {
            ReportPage resultPage = programmaMusicaleService.getListaAggregatiSiada(
                    Long.parseLong(inizioPeriodoContabile), Long.parseLong(finePeriodoContabile), voceIncasso,
                    tipoSupporto, tipoProgramma, numPM, page);

            // model.addAttribute("resultPage", resultPage);

            List<String> tipoProgrammi = programmaMusicaleService.getTipoProgrammi();

            // model.addAttribute("tipoProgrammi", tipoProgrammi);
            return Response.ok(gson.toJson(resultPage)).build();
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: handle exception
        }
        return Response.status(500).build();
    }

    @GET
    @Path("ricercaOpere")
    public Response ricercaOpere(@QueryParam(value = "titoloOpera") String titoloOpera,
                                 @QueryParam(value = "autore") String autore, @QueryParam(value = "compositore") String compositore,
                                 @QueryParam(value = "codiceOpera") String codiceOpera) {

        String url = servizioMulePath + "?userName=SOPHIA";
        if (codiceOpera != null) {

            url = url + "&codiceOpera=" + codiceOpera;
        }
        if (titoloOpera != null) {
            url = url + "&titolo=" + titoloOpera;
        }
        if (autore != null) {
            url = url + "&autore=" + autore;
        }
        if (compositore != null) {
            url = url + "&compositore=" + compositore;
        }
        try {

            final HttpClientBuilder httpClientBuilder = HttpClients.custom().setUserAgent(userAgent)
                    .disableCookieManagement();
            if (Strings.isNullOrEmpty(muleCertificationPath)) {
                httpClientBuilder.setSslcontext(SSLContexts.createDefault());
            } else {
                try {
                    final SSLContextBuilder sslContextBuilder = SSLContexts.custom();
                    sslContextBuilder.loadKeyMaterial(new File(muleCertificationPath),
                            Strings.isNullOrEmpty(mulePwd) ? null : mulePwd.toCharArray(),
                            Strings.isNullOrEmpty(keyPwd) ? null : keyPwd.toCharArray(),
                            Strings.isNullOrEmpty(muleAlias) ? null : new PrivateKeyStrategy() {

                                @Override
                                public String chooseAlias(Map<String, PrivateKeyDetails> aliases, Socket socket) {
                                    for (String alias : aliases.keySet()) {
                                        if (alias.equalsIgnoreCase(muleAlias))
                                            return alias;
                                    }
                                    return null;
                                }
                            });
                    sslContextBuilder.loadTrustMaterial(new File(muleCertificationPath),
                            Strings.isNullOrEmpty(mulePwd) ? null : mulePwd.toCharArray(),
                            selfSigned ? new TrustSelfSignedStrategy() : null);
                    httpClientBuilder.setSslcontext(sslContextBuilder.build());
                } catch (Exception e) {
                    System.out.println(e);
                }
                // ssl hostname verifier
                final boolean verifyHostnames = "true".equalsIgnoreCase(verifyHostNames);
                if (!verifyHostnames)
                    httpClientBuilder.setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE);
            }
            CloseableHttpClient httpclient = httpClientBuilder.build();
            HttpGet httpget = new HttpGet(url);
            CloseableHttpResponse response = httpclient.execute(httpget);
            String json = EntityUtils.toString(response.getEntity());
            response.close();
            return Response.ok(json).build();
        } catch (Exception e) {
            System.out.println(e);
        }
        return Response.status(500).build();
    }

    @GET
    @Path("exportPmDetail")
    @Produces("application/csv")
    public Response exportPmDetail(@QueryParam(value = "idProgrammaMusicale") Long idProgrammaMusicale,
                                   @QueryParam(value = "idMovimento") Long idMovimento) {

        try {
            ProgrammaMusicale programmaMusicale = null;

            if (idMovimento != null)
                programmaMusicale = programmaMusicaleService.getDettaglioPM(idProgrammaMusicale, idMovimento);
            else
                programmaMusicale = programmaMusicaleService.getDettaglioPM(idProgrammaMusicale);
            StreamingOutput so = null;
            ProgrammaMusicaleDetailToCsv pmFormattato;
            if (programmaMusicale.getDirettoreEsecuzione() != null) {
                pmFormattato = new ProgrammaMusicaleDetailToCsv(programmaMusicale.getNumeroProgrammaMusicale(),
                        programmaMusicale.getVoceIncasso(), programmaMusicale.getSupportoPm(),
                        programmaMusicale.getTipoPm(), programmaMusicale.getFlagDaCampionare(),
                        programmaMusicale.getCodiceCampionamento(), programmaMusicale.getRisultatoCalcoloResto(),
                        programmaMusicale.getTotaleDurataCedole(), programmaMusicale.getTotaleCedole(),
                        programmaMusicale.getFoglioSegueDi(), programmaMusicale.getDataRientroPM(),
                        programmaMusicale.getDirettoreEsecuzione().getDirettoreEsecuzione(), null,
                        programmaMusicale.getFlagGruppoPrincipale());
            } else {
                pmFormattato = new ProgrammaMusicaleDetailToCsv(programmaMusicale.getNumeroProgrammaMusicale(),
                        programmaMusicale.getVoceIncasso(), programmaMusicale.getSupportoPm(),
                        programmaMusicale.getTipoPm(), programmaMusicale.getFlagDaCampionare(),
                        programmaMusicale.getCodiceCampionamento(), programmaMusicale.getRisultatoCalcoloResto(),
                        programmaMusicale.getTotaleDurataCedole(), programmaMusicale.getTotaleCedole(),
                        programmaMusicale.getFoglioSegueDi(), programmaMusicale.getDataRientroPM(), null, null,
                        programmaMusicale.getFlagGruppoPrincipale());
            }
            CustomMappingStrategy<ProgrammaMusicaleDetailToCsv> customMappingStrategy = new CustomMappingStrategy<>(
                    new ProgrammaMusicaleDetailToCsv().getMappingStrategy());
            customMappingStrategy.setType(ProgrammaMusicaleDetailToCsv.class);
            so = JavaBeanToCsv.getStreamingOutput(pmFormattato, customMappingStrategy);
            Response response = Response.ok(so, "text/csv").header("Content-Disposition",
                    "attachment; filename=\"" + "Programma_Musicale_" + idProgrammaMusicale + ".csv\"").build();
            return response;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Response.status(500).entity("Errore nell'esportazione del dettaglio programma musicale").build();
        }
    }

    @GET
    @Path("exportPmList")
    @Produces("application/csv")
    public Response exportPmList(@QueryParam(value = "meseInizioPeriodoContabile") String meseInizioPeriodoContabile,
                                 @QueryParam(value = "annoInizioPeriodoContabile") String annoInizioPeriodoContabile,
                                 @QueryParam(value = "meseFinePeriodoContabile") String meseFinePeriodoContabile,
                                 @QueryParam(value = "annoFinePeriodoContabile") String annoFinePeriodoContabile,
                                 @QueryParam(value = "tipoDocumento") String tipoDocumento, @QueryParam(value = "seprag") String seprag,
                                 @QueryParam(value = "evento") String idEvento, @QueryParam(value = "fattura") String fattura,
                                 @QueryParam(value = "reversale") String reversale, @QueryParam(value = "voceIncasso") String voceIncasso,
                                 @QueryParam(value = "permesso") String permesso, @QueryParam(value = "numeroPM") String numeroPM,
                                 @QueryParam(value = "locale") String locale, @QueryParam(value = "codiceBA") String codiceBA,
                                 @QueryParam(value = "codiceSAPOrganizzatore") String codiceSAPOrganizzatore,
                                 @QueryParam(value = "organizzatore") String organizzatore,
                                 @QueryParam(value = "direttoreEsecuzione") String direttoreEsecuzione,
                                 @QueryParam(value = "dataInizioEvento") String dataInizioEvento,
                                 @QueryParam(value = "dataFineEvento") String dataFineEvento,
                                 @QueryParam(value = "titoloOpera") String titoloOpera,
                                 @QueryParam(value = "presenzaMovimenti") String presenzaMovimenti,
                                 @QueryParam(value = "supporto") String supporto, @QueryParam(value = "tipoProgrammi") String tipoProgrammi,
                                 @QueryParam(value = "gruppoPrincipale") String gruppoPrincipale, @QueryParam(value = "order") String order)
            throws IOException {

        if (tipoDocumento != null && tipoDocumento.equals(""))
            tipoDocumento = null;
        if (seprag != null && seprag.equals(""))
            seprag = null;
        if (idEvento != null && idEvento.equals(""))
            idEvento = null;
        if (fattura != null && fattura.equals(""))
            fattura = null;
        if (reversale != null && reversale.equals(""))
            reversale = null;
        if (voceIncasso != null && voceIncasso.equals(""))
            voceIncasso = null;
        if (permesso != null && permesso.equals(""))
            permesso = null;
        if (numeroPM != null && numeroPM.equals(""))
            numeroPM = null;
        if (locale != null && locale.equals(""))
            locale = null;
        if (codiceBA != null && codiceBA.equals(""))
            codiceBA = null;
        if (codiceSAPOrganizzatore != null && codiceSAPOrganizzatore.equals(""))
            codiceSAPOrganizzatore = null;
        if (organizzatore != null && organizzatore.equals(""))
            organizzatore = null;
        if (direttoreEsecuzione != null && direttoreEsecuzione.equals(""))
            direttoreEsecuzione = null;
        if (dataInizioEvento != null && dataInizioEvento.equals(""))
            dataInizioEvento = null;
        if (dataFineEvento != null && dataFineEvento.equals(""))
            dataFineEvento = null;
        if (titoloOpera != null && titoloOpera.equals(""))
            titoloOpera = null;
        if (supporto != null && supporto.equals(""))
            supporto = null;
        if (tipoProgrammi != null && tipoProgrammi.equals(""))
            tipoProgrammi = null;
        if (gruppoPrincipale != null && gruppoPrincipale.equals(""))
            gruppoPrincipale = null;

        if (meseInizioPeriodoContabile != null && meseInizioPeriodoContabile.length() == 1)
            meseInizioPeriodoContabile = "0" + meseInizioPeriodoContabile;
        if (meseFinePeriodoContabile != null && meseFinePeriodoContabile.length() == 1)
            meseFinePeriodoContabile = "0" + meseFinePeriodoContabile;

        String inizioPeriodoContabile = null;
        String finePeriodoContabile = null;
        if (annoInizioPeriodoContabile != null && meseInizioPeriodoContabile != null) {
            inizioPeriodoContabile = annoInizioPeriodoContabile + meseInizioPeriodoContabile;
        }
        if (annoFinePeriodoContabile != null && meseFinePeriodoContabile != null) {
            finePeriodoContabile = annoFinePeriodoContabile + meseFinePeriodoContabile;
        }
        Long numeroProgrammaMusicale = null;
        if (numeroPM != null && !numeroPM.equalsIgnoreCase(""))
            numeroProgrammaMusicale = Long.parseLong(numeroPM);
        List<ProgrammaMusicale> programmiMusicali = programmaMusicaleService.getListaProgrammiMusicali(
                inizioPeriodoContabile, finePeriodoContabile, numeroProgrammaMusicale, tipoDocumento, seprag, idEvento,
                fattura, reversale, voceIncasso, permesso, locale, codiceBA, codiceSAPOrganizzatore, organizzatore,
                titoloOpera, dataInizioEvento, dataFineEvento, direttoreEsecuzione, supporto, tipoProgrammi,
                gruppoPrincipale, order, true);
        List<ProgrammaMusicaleListToCsv> pmToCsv = new ArrayList<ProgrammaMusicaleListToCsv>();
        ProgrammaMusicaleListToCsv pmFormatted;
        for (int i = 0; programmiMusicali.size() > i; i++) {

            String permessoToString = "'" + programmiMusicali.get(i).getMovimenti().get(0).getNumeroPermesso() + "'";
            DecimalFormat df = new DecimalFormat("0.00");

            String totDemLordo = null;
            String importoRicalcolato = null;

            if (programmiMusicali.get(i).getMovimenti().get(0).getImportoTotDemLordo() != null)
                totDemLordo = df.format(programmiMusicali.get(i).getMovimenti().get(0).getImportoTotDemLordo());
            else
                totDemLordo = null;
            if (programmiMusicali.get(i).getMovimenti().get(0).getImportoRicalcolato() != null)
                importoRicalcolato = df.format(programmiMusicali.get(i).getMovimenti().get(0).getImportoRicalcolato());
            else
                importoRicalcolato = null;

            pmFormatted = new ProgrammaMusicaleListToCsv(
                    programmiMusicali.get(i).getMovimenti().get(0).getContabilita(),
                    programmiMusicali.get(i).getMovimenti().get(0).getVoceIncasso(),
                    programmiMusicali.get(i).getMovimenti().get(0).getIdEvento(), permessoToString,
                    programmiMusicali.get(i).getMovimenti().get(0).getNumeroFattura(),
                    programmiMusicali.get(i).getMovimenti().get(0).getReversale(),
                    programmiMusicali.get(i).getMovimenti().get(0).getTipoDocumentoContabile(),
                    Long.valueOf(programmiMusicali.get(i).getMovimenti().get(0).getNumProgrammaMusicale()), totDemLordo,
                    importoRicalcolato);
            pmToCsv.add(pmFormatted);
        }

        try {

            StreamingOutput so = null;
            CustomMappingStrategy<ProgrammaMusicaleListToCsv> customMappingStrategy = new CustomMappingStrategy<>(
                    new ProgrammaMusicaleListToCsv().getMappingStrategy());
            customMappingStrategy.setType(ProgrammaMusicaleListToCsv.class);
            so = getListStreamingOutput(pmToCsv, customMappingStrategy);
            Response response = Response.ok(so, "text/csv")
                    .header("Content-Disposition", "attachment; filename=\"" + "Ricerca_PM.csv\"").build();
            return response;

        } catch (Exception e) {
            return Response.status(500).entity("Non risulta nessun dato per il periodo selezionato").build();
        }
    }

    @GET
    @Path("exportMovimentiList")
    @Produces("application/csv")
    public Response exportMovimentiList(
            @QueryParam(value = "meseInizioPeriodoContabile") String meseInizioPeriodoContabile,
            @QueryParam(value = "annoInizioPeriodoContabile") String annoInizioPeriodoContabile,
            @QueryParam(value = "meseFinePeriodoContabile") String meseFinePeriodoContabile,
            @QueryParam(value = "annoFinePeriodoContabile") String annoFinePeriodoContabile,
            @QueryParam(value = "tipoDocumento") String tipoDocumento, @QueryParam(value = "seprag") String seprag,
            @QueryParam(value = "evento") Long evento, @QueryParam(value = "fattura") String fattura,
            @QueryParam(value = "reversale") Long reversale, @QueryParam(value = "voceIncasso") String voceIncasso,
            @QueryParam(value = "permesso") String permesso, @QueryParam(value = "numeroPM") String numeroPM,
            @QueryParam(value = "locale") String locale, @QueryParam(value = "codiceBALocale") String codiceBALocale,
            @QueryParam(value = "sapOrganizzatore") String sapOrganizzatore,
            @QueryParam(value = "organizzatore") String organizzatore,
            @QueryParam(value = "dataInizioEvento") String dataInizioEvento,
            @QueryParam(value = "dataFineEvento") String dataFineEvento,
            @QueryParam(value = "titoloOpera") String titoloOpera,
            @QueryParam(value = "tipoSupporto") String tipoSupporto,
            @QueryParam(value = "tipoProgramma") String tipoProgramma,
            @QueryParam(value = "gruppoPrincipale") String gruppoPrincipale, @QueryParam(value = "page") Integer page,
            @QueryParam(value = "count") Boolean count, @QueryParam(value = "order") String order) {

        // traceService.trace("Cruscotti", this.getClass().getName(), "/movimentiList",
        // "Lista movimenti PM", Constants.CLICK);

        if (page == null)
            page = 0;

        if (meseInizioPeriodoContabile != null && meseInizioPeriodoContabile.equals(""))
            meseInizioPeriodoContabile = null;
        if (annoInizioPeriodoContabile != null && annoInizioPeriodoContabile.equals(""))
            annoInizioPeriodoContabile = null;
        if (meseFinePeriodoContabile != null && meseFinePeriodoContabile.equals(""))
            meseFinePeriodoContabile = null;
        if (annoFinePeriodoContabile != null && annoFinePeriodoContabile.equals(""))
            annoFinePeriodoContabile = null;

        if (tipoDocumento != null && tipoDocumento.equals(""))
            tipoDocumento = null;
        if (seprag != null && seprag.equals(""))
            seprag = null;
        if (evento != null && evento.equals(""))
            evento = null;
        if (fattura != null && fattura.equals(""))
            fattura = null;
        if (reversale != null && reversale.equals(""))
            reversale = null;
        if (voceIncasso != null && voceIncasso.equals(""))
            voceIncasso = null;
        if (permesso != null && permesso.equals(""))
            permesso = null;
        if (numeroPM != null && numeroPM.equals(""))
            numeroPM = null;
        if (locale != null && locale.equals(""))
            locale = null;
        if (codiceBALocale != null && codiceBALocale.equals(""))
            codiceBALocale = null;
        if (sapOrganizzatore != null && sapOrganizzatore.equals(""))
            sapOrganizzatore = null;
        if (organizzatore != null && organizzatore.equals(""))
            organizzatore = null;
        if (dataInizioEvento != null && dataInizioEvento.equals(""))
            dataInizioEvento = null;
        if (dataFineEvento != null && dataFineEvento.equals(""))
            dataFineEvento = null;
        if (titoloOpera != null && titoloOpera.equals(""))
            titoloOpera = null;

        if (tipoSupporto != null && tipoSupporto.equals(""))
            tipoSupporto = null;
        if (tipoProgramma != null && tipoProgramma.equals(""))
            tipoProgramma = null;

        if (meseInizioPeriodoContabile != null && meseInizioPeriodoContabile.length() == 1)
            meseInizioPeriodoContabile = "0" + meseInizioPeriodoContabile;
        if (meseFinePeriodoContabile != null && meseFinePeriodoContabile.length() == 1)
            meseFinePeriodoContabile = "0" + meseFinePeriodoContabile;

        if (count == null)
            count = false;

        String inizioPeriodoContabile = null;
        Long concatInizioPeriodo = null;
        String finePeriodoContabile = null;
        Long concatFinePeriodo = null;
        if (annoInizioPeriodoContabile != null && meseInizioPeriodoContabile != null) {
            inizioPeriodoContabile = annoInizioPeriodoContabile + meseInizioPeriodoContabile;
            concatInizioPeriodo = Long.parseLong(inizioPeriodoContabile);
        }
        if (annoFinePeriodoContabile != null && meseFinePeriodoContabile != null) {
            finePeriodoContabile = annoFinePeriodoContabile + meseFinePeriodoContabile;
            concatFinePeriodo = Long.parseLong(finePeriodoContabile);
        }

        Date dataEventoInizio = null;
        if (dataInizioEvento != null)
            dataEventoInizio = DateUtils.stringToDate(dataInizioEvento, DateUtils.DATE_ITA_FORMAT_DASH);
        Date dataEventoFine = null;
        if (dataFineEvento != null)
            dataEventoFine = DateUtils.stringToDate(dataFineEvento, DateUtils.DATE_ITA_FORMAT_DASH);

        List<MovimentoContabile> movimento = programmaMusicaleService.getListaMovimenti(concatInizioPeriodo,
                concatFinePeriodo, evento, fattura, reversale, voceIncasso, dataEventoInizio, dataEventoFine, seprag,
                tipoDocumento, locale, sapOrganizzatore, tipoSupporto, tipoProgramma, gruppoPrincipale, titoloOpera,
                numeroPM, permesso, page, codiceBALocale, organizzatore, order, true);
        List<ProgrammaMusicaleListToCsv> pmToCsv = new ArrayList<ProgrammaMusicaleListToCsv>();
        ProgrammaMusicaleListToCsv pmFormatted;

        DecimalFormat df = new DecimalFormat("0.00");
        String totDemLordo = null;
        String importoRicalcolato = null;

        for (int i = 0; movimento.size() > i; i++) {

            if (movimento.get(i).getImportoTotDemLordo() != null)
                totDemLordo = df.format(movimento.get(i).getImportoTotDemLordo());
            else
                totDemLordo = null;
            if (movimento.get(i).getImportoRicalcolato() != null)
                importoRicalcolato = df.format(movimento.get(i).getImportoRicalcolato());
            else
                importoRicalcolato = null;
            String permessoToString = "'" + movimento.get(i).getNumeroPermesso() + "'";
            pmFormatted = new ProgrammaMusicaleListToCsv(movimento.get(i).getContabilita(),
                    movimento.get(i).getVoceIncasso(), movimento.get(i).getIdEvento(), permessoToString,
                    movimento.get(i).getNumeroFattura(), movimento.get(i).getReversale(),
                    movimento.get(i).getTipoDocumentoContabile(),
                    Long.valueOf(movimento.get(i).getNumProgrammaMusicale()), totDemLordo, importoRicalcolato);
            pmToCsv.add(pmFormatted);
        }

        try {

            StreamingOutput so = null;
            CustomMappingStrategy<ProgrammaMusicaleListToCsv> customMappingStrategy = new CustomMappingStrategy<>(
                    new ProgrammaMusicaleListToCsv().getMappingStrategy());
            customMappingStrategy.setType(ProgrammaMusicaleListToCsv.class);
            so = getListStreamingOutput(pmToCsv, customMappingStrategy);
            Response response = Response.ok(so, "text/csv")
                    .header("Content-Disposition", "attachment; filename=\"" + "Ricerca_Movimenti.csv\"").build();
            return response;

        } catch (Exception e) {
            return Response.status(500).entity("Non risulta nessun dato per il periodo selezionato").build();
        }
    }

    // R/
    @GET
    @Path("exportEventiList")
    @Produces("application/csv")
    public Response exportEventiList(
            @QueryParam(value = "meseInizioPeriodoContabile") String meseInizioPeriodoContabile,
            @QueryParam(value = "annoInizioPeriodoContabile") String annoInizioPeriodoContabile,
            @QueryParam(value = "meseFinePeriodoContabile") String meseFinePeriodoContabile,
            @QueryParam(value = "annoFinePeriodoContabile") String annoFinePeriodoContabile,
            @QueryParam(value = "tipoDocumento") String tipoDocumento, @QueryParam(value = "seprag") String seprag,
            @QueryParam(value = "evento") Long evento, @QueryParam(value = "fattura") String fattura,
            @QueryParam(value = "reversale") String reversale, @QueryParam(value = "voceIncasso") String voceIncasso,
            @QueryParam(value = "permesso") String permesso, @QueryParam(value = "numeroPM") String numeroPM,
            @QueryParam(value = "locale") String locale, @QueryParam(value = "codiceBA") String codiceBA,
            @QueryParam(value = "codiceSAP") String codiceSAP,
            @QueryParam(value = "organizzatore") String organizzatore,
            @QueryParam(value = "direttoreEsecuzione") String direttoreEsecuzione,
            @QueryParam(value = "dataInizioEvento") String dataInizioEvento,
            @QueryParam(value = "dataFineEvento") String dataFineEvento,
            @QueryParam(value = "titoloOpera") String titoloOpera,
            @QueryParam(value = "presenzaMovimenti") String presenzaMovimenti,
            @QueryParam(value = "fatturaValidata") String fatturaValidata, @QueryParam(value = "order") String order,
            @QueryParam(value = "page") Integer page, @QueryParam(value = "count") Boolean count) {

        // traceService.trace("Cruscotti", this.getClass().getName(),
        // "/eventiPagatiList", "Visualizzazione lista Eventi Pagati", Constants.CLICK);

        if (page == null)
            page = 0;

        if (tipoDocumento != null && tipoDocumento.equals(""))
            tipoDocumento = null;
        if (seprag != null && seprag.equals(""))
            seprag = null;
        if (evento != null && evento.equals(""))
            evento = null;
        if (fattura != null && fattura.equals(""))
            fattura = null;
        if (reversale != null && reversale.equals(""))
            reversale = null;
        if (voceIncasso != null && voceIncasso.equals(""))
            voceIncasso = null;
        if (permesso != null && permesso.equals(""))
            permesso = null;
        if (numeroPM != null && numeroPM.equals(""))
            numeroPM = null;
        if (locale != null && locale.equals(""))
            locale = null;
        if (codiceBA != null && codiceBA.equals(""))
            codiceBA = null;
        if (codiceSAP != null && codiceSAP.equals(""))
            codiceSAP = null;
        if (organizzatore != null && organizzatore.equals(""))
            organizzatore = null;
        if (direttoreEsecuzione != null && direttoreEsecuzione.equals(""))
            direttoreEsecuzione = null;
        if (dataInizioEvento != null && dataInizioEvento.equals(""))
            dataInizioEvento = null;
        // if (oraInizioEvento != null && oraInizioEvento.equals(""))
        // oraInizioEvento = null;
        if (dataFineEvento != null && dataFineEvento.equals(""))
            dataFineEvento = null;
        // if (oraFineEvento != null && oraFineEvento.equals(""))
        // oraFineEvento = null;

        if (titoloOpera != null && titoloOpera.equals(""))
            titoloOpera = null;
        if (meseInizioPeriodoContabile != null && meseInizioPeriodoContabile.length() == 1)
            meseInizioPeriodoContabile = "0" + meseInizioPeriodoContabile;
        if (meseFinePeriodoContabile != null && meseFinePeriodoContabile.length() == 1)
            meseFinePeriodoContabile = "0" + meseFinePeriodoContabile;

        if (count == null)
            count = false;

        String inizioPeriodoContabile = null;
        String finePeriodoContabile = null;

        if (annoInizioPeriodoContabile != null && meseInizioPeriodoContabile != null)
            inizioPeriodoContabile = annoInizioPeriodoContabile + meseInizioPeriodoContabile;

        if (annoFinePeriodoContabile != null && meseFinePeriodoContabile != null)
            finePeriodoContabile = annoFinePeriodoContabile + meseFinePeriodoContabile;

        PerfEventiPagatiDTO perfEventiPagatiDTO = new PerfEventiPagatiDTO(meseInizioPeriodoContabile,
                annoInizioPeriodoContabile, meseFinePeriodoContabile, annoFinePeriodoContabile, evento, fattura,
                reversale, voceIncasso, dataInizioEvento, "0000", seprag, tipoDocumento, locale, codiceBA,
                presenzaMovimenti, page);
        List<EventiPagati> eventiPagati = programmaMusicaleService.getListaEvPagati(perfEventiPagatiDTO,
                inizioPeriodoContabile, finePeriodoContabile, organizzatore, direttoreEsecuzione, permesso, numeroPM,
                titoloOpera, codiceSAP, dataInizioEvento, dataFineEvento, fatturaValidata, order);

        List<EventoPagatoListToCsv> eventoToCsv = new ArrayList<EventoPagatoListToCsv>();
        EventoPagatoListToCsv eventoFormatted;


        for (int i = 0; eventiPagati.size() > i; i++) {

            String permessoToString = "";
            if (eventiPagati.get(i).getPermesso() != null)
                permessoToString = "'" + eventiPagati.get(i).getPermesso() + "'";
            else
                permessoToString = "";

            eventoFormatted = new EventoPagatoListToCsv(eventiPagati.get(i).getIdEvento(), permessoToString,
                    eventiPagati.get(i).getContabilita(), eventiPagati.get(i).getNumeroFattura(),
                    eventiPagati.get(i).getVoceIncasso(), eventiPagati.get(i).getDataInizioEvento(),
                    eventiPagati.get(i).getOraInizioEvento(), eventiPagati.get(i).getDenominazioneLocale(),
                    eventiPagati.get(i).getManifestazione().getNomeOrganizzatore(),
                    eventiPagati.get(i).getTipoDocumentoContabile(), eventiPagati.get(i).getImportDem(), eventiPagati.get(i).getQuadraturaNDM(), eventiPagati.get(i).getImportoAggio(), eventiPagati.get(i).getImportoExArt());
            eventoToCsv.add(eventoFormatted);
        }

        try {

            StreamingOutput so = null;
            CustomMappingStrategy<EventoPagatoListToCsv> customMappingStrategy = new CustomMappingStrategy<>(
                    new EventoPagatoListToCsv().getMappingStrategy());
            customMappingStrategy.setType(EventoPagatoListToCsv.class);
            so = getListStreamingOutput(eventoToCsv, customMappingStrategy);
            Response response = Response.ok(so, "text/csv")
                    .header("Content-Disposition", "attachment; filename=\"" + "Ricerca_Eventi.csv\"").build();
            return response;

        } catch (Exception e) {
            return Response.status(500).entity("Non risulta nessun dato per il periodo selezionato").build();
        }
    }

    @GET
    @Path("exportOpereList")
    @Produces("application/csv")
    public Response exportOpereList(@QueryParam(value = "idPm") Long idPm) {

        ProgrammaMusicale programmaMusicale = null;

        programmaMusicale = programmaMusicaleService.getDettaglioPM(idPm);

        programmaMusicale.getOpere();

        DecimalFormatSymbols dfs = new DecimalFormatSymbols(Locale.ITALIAN);
        dfs.setCurrencySymbol("\u20AC");
        DecimalFormat df = new DecimalFormat("0.00 ¤", dfs);
        String importoCedola;
        List<UtilizzazioniToCsv> opereToCsv = new ArrayList<UtilizzazioniToCsv>();
        UtilizzazioniToCsv utilizzazioniFormatted;

        String durata;

        for (int i = 0; programmaMusicale.getOpere().size() > i; i++) {
            if (programmaMusicale.getOpere().get(i).getImportoCedola() != null)
                importoCedola = df.format(programmaMusicale.getOpere().get(i).getImportoCedola());
            else
                importoCedola = null;

            if (programmaMusicale.getOpere().get(i).getDurata() != null) {
                durata = DurationFormatUtils.formatDuration(programmaMusicale.getOpere().get(i).getDurata().intValue() * 1000, "mm:ss", true);
            } else durata = null;

            utilizzazioniFormatted = new UtilizzazioniToCsv.Builder().codiceOpera((programmaMusicale.getOpere().get(i).getCodiceOpera()))
                    .titoloComposizione(programmaMusicale.getOpere().get(i).getTitoloComposizione())
                    .compositore(programmaMusicale.getOpere().get(i).getCompositore())
                    .durata(durata)
                    .durataInferiore30sec(programmaMusicale.getOpere().get(i).isDurataInferiore30sec() ? "SI" : "NO")
                    .autori(programmaMusicale.getOpere().get(i).getAutori())
                    .interpreti(programmaMusicale.getOpere().get(i).getInterpreti())
                    .flagPubblicoDominio(programmaMusicale.getOpere().get(i).getFlagPubblicoDominio())
                    .importoCedola(importoCedola)
                    .nettoOriginario(programmaMusicale.getOpere().get(i).getNettoOriginario() != null ? df.format(programmaMusicale.getOpere().get(i).getNettoOriginario()) : "")
                    .importoValorizzato(programmaMusicale.getOpere().get(i).getImportoValorizzato() != null ? df.format(programmaMusicale.getOpere().get(i).getImportoValorizzato()) : "")
                    .build();
            opereToCsv.add(utilizzazioniFormatted);
        }

        try {

            StreamingOutput so = null;
            CustomMappingStrategy<UtilizzazioniToCsv> customMappingStrategy = new CustomMappingStrategy<>(
                    new UtilizzazioniToCsv().getMappingStrategy());
            customMappingStrategy.setType(UtilizzazioniToCsv.class);
            so = getListStreamingOutput(opereToCsv, customMappingStrategy);
            Response response = Response.ok(so, "text/csv")
                    .header("Content-Disposition", "attachment; filename=\"" + "Opere.csv\"").build();
            return response;

        } catch (Exception e) {
            return Response.status(500).entity("Non risulta nessun dato per il periodo selezionato").build();
        }
    }

    @GET
    @Path("exportFatturaDetail")
    @Produces("application/csv")
    public Response exportFatturaDetail(@QueryParam(value = "numeroFattura") String numeroFattura)
            throws FileNotFoundException, IOException {

        FatturaDetailDTO fattura = programmaMusicaleService.getFatturaDetail(numeroFattura);
        DecimalFormat df = new DecimalFormat("0.00");

        String[] riepilogoFattura = {"Riepilogo Fattura"};
        String[] riga1 = {"NumeroFattura", fattura.getNumeroFattura(), "", "DataFattura", fattura.getDataFattura()};
        String[] riga2 = {"Numero Reversale", fattura.getReversale(), "", "Seprag", fattura.getSeprag()};
        String[] riga3 = {"Nominativo Organizzatore", fattura.getNomeOrganizzatore(), "", "P.IVA \n C.F",
                fattura.getManifestazione() == null ? ""
                        : fattura.getManifestazione().getPartitaIvaOrganizzatore() + "\n"
                        + fattura.getManifestazione() == null ? ""
                        : fattura.getManifestazione().getCodiceFiscaleOrganizzatore()};
        String[] riga4 = {"Codice SAP",
                fattura.getManifestazione() == null ? "" : fattura.getManifestazione().getCodiceSapOrganizzatore(), "",
                "Tipo Documento", fattura.getTipoDocumento()};
        String[] riga5 = {"Contabilita", fattura.getEventiAssociati().get(0).getContabilita().toString()};
        String[] dettaglioEventi = {"Dettaglio eventi legati a fattura"};

        String[] lineaVuota = {""};

        String[] intestazioneDettaglioEventi = {"Voce Incasso", "Tipo Documento", "Id Evento", "Data Evento",
                "Locale-Spazio", "Comune-Locale", "Importo Evento", "N° PM attesi", "N° PM rientrati",
                "Fattura Validata", "Valore Netto", "Aggio", "Ex Art. 18", "Flag Mega Concerto"};

        String[] intestazioneRiconciliazioneFattura = {"Voci Incasso", "Importo Sophia (SUn)", "Importo NDM",
                "Delta Assoluto", "Delta %"};
        List<String[]> fatturaCsv = new ArrayList<String[]>();

        fatturaCsv.add(riepilogoFattura);
        fatturaCsv.add(lineaVuota);
        fatturaCsv.add(riga1);
        fatturaCsv.add(riga2);
        fatturaCsv.add(riga3);
        fatturaCsv.add(riga4);
        fatturaCsv.add(riga5);
        fatturaCsv.add(lineaVuota);
        fatturaCsv.add(lineaVuota);
        fatturaCsv.add(dettaglioEventi);
        fatturaCsv.add(lineaVuota);
        fatturaCsv.add(intestazioneDettaglioEventi);

        for (EventiPagati evento : fattura.getEventiAssociati()) {
            String quadratura = "Non Validata";
            String flagMegaConcert = "";
            String importoOriginale = "";
            String importoAggio = "";
            String importoDem = "";
            String importoSophiaNetto = "";
            String importoExArt = "";

            if (evento.getQuadraturaNDM() != null && evento.getQuadraturaNDM() == "0") {
                quadratura = "KO";
            }
            if (evento.getQuadraturaNDM() != null && evento.getQuadraturaNDM() == "1") {
                quadratura = "OK";
                if (evento.getManifestazione() != null && evento.getManifestazione().getFlagMegaConcert() == true) {
                    flagMegaConcert = "Si";
                } else if (evento.getManifestazione() != null && evento.getManifestazione().getFlagMegaConcert() == false) {
                    if (evento.getVoceIncasso().equals("2244"))
                        flagMegaConcert = "No";
                }
                importoSophiaNetto = df.format(evento.getImportoSophiaNetto());
            }

            if (evento.getImportoAggio() != null) {
                importoAggio = df.format(evento.getImportoAggio());
            }
            if (evento.getImportoExArt() != null) {
                importoExArt = df.format(evento.getImportoExArt());
            }
            if (evento.getImportDem() != null) {
                importoDem = df.format(evento.getImportDem());
            }
            String[] eventoPagato = {evento.getVoceIncasso(), evento.getTipoDocumentoContabile(),
                    evento.getIdEvento().toString(), evento.getDataInizioEvento().toString(),
                    evento.getManifestazione() == null ? "" : evento.getManifestazione().getDenominazioneLocale(), evento.getManifestazione() == null ? "" : evento.getManifestazione().getComuneLocale(),
                    importoDem, evento.getNumeroPmTotaliAttesi().toString(),
                    evento.getNumeroPmTotaliAttesiSpalla().toString(), quadratura, importoSophiaNetto, importoAggio,
                    importoExArt, flagMegaConcert};
            fatturaCsv.add(eventoPagato);
        }

        fatturaCsv.add(lineaVuota);
        fatturaCsv.add(intestazioneRiconciliazioneFattura);

        for (RiconciliazioneImportiDTO riconciliazioneFattura : fattura.getRiconciliazioneFattura()) {

            String riconciliato[] = {riconciliazioneFattura.getVoceIncasso(),
                    riconciliazioneFattura.getTotaleLordoSophia() == null ? ""
                            : df.format(riconciliazioneFattura.getTotaleLordoSophia()),
                    riconciliazioneFattura.getTotaleLordoNDM() == null ? ""
                            : df.format(riconciliazioneFattura.getTotaleLordoNDM()),
                    riconciliazioneFattura.getDeltaAssoluto() == null ? ""
                            : df.format(riconciliazioneFattura.getDeltaAssoluto()),
                    riconciliazioneFattura.getDeltaPercentuale() == null ? ""
                            : df.format(riconciliazioneFattura.getDeltaPercentuale())};
            fatturaCsv.add(riconciliato);
        }

        try {
            StreamingOutput so = null;
            CustomMappingStrategy<UtilizzazioniToCsv> customMappingStrategy = new CustomMappingStrategy<>(
                    new UtilizzazioniToCsv().getMappingStrategy());
            customMappingStrategy.setType(UtilizzazioniToCsv.class);

            so = getStreamingOutputWithoutStrategy(fatturaCsv);
            Response response = Response.ok(so, "text/csv")
                    .header("Content-Disposition", "attachment; filename=\"" + "FatturaDetail.csv\"").build();
            return response;
        } catch (Exception e) {
            return Response.status(500).entity("Non risulta nessun dato per il periodo selezionato").build();
        }
    }

    @POST
    @Path("importOpere")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response importOpera(@FormDataParam("Opere") InputStream uploadedInputStream,
                                @QueryParam(value = "idPm") Long idPm, @QueryParam(value = "numeroPm") Long numeroPm,
                                @QueryParam(value = "userId") String userId) throws IOException {

        char separator = 'f';
        String result = org.apache.commons.io.IOUtils.toString(uploadedInputStream, StandardCharsets.UTF_8);
        for (int i = 10; i < 15; i++) {
            if (result.charAt(i) == ';') {
                separator = ';';
            } else if (result.charAt(i) == ',') {
                separator = ',';
            }

        }
        if (separator == 'f') {
            return Response.status(500).entity(gson.toJson("Errore nel formato di separazione dei dati")).build();
        }
        InputStream in = org.apache.commons.io.IOUtils.toInputStream(result, "UTF-8");

        final CSVParser parser = new CSVParserBuilder().withSeparator(separator).withIgnoreQuotations(true).build();

        CSVReader reader = new CSVReaderBuilder(new InputStreamReader(in)).withCSVParser(parser).build();

        // CSVReader reader = new CSVReader(new InputStreamReader(in), separator); //
        // cambiato da ; a , siccome windows usa di default come separato (in excel) la
        // ,

        try {
            String[] record = null;
            List<Utilizzazione> utilizzazioni = new ArrayList<Utilizzazione>();
            //
            int countRow = 1;
            boolean error = false;

            // try {
            //// InputStream is = new FileInputStream(uploadedInputStream);
            // byte[] data = IOUtils.toByteArray(uploadedInputStream);
            // CharsetMatch match = new CharsetDetector().setText(data).detect();
            // String charsetName = match.getName();
            // System.out.println("detected charset: "+ charsetName);
            // byte[] encoded = new String(data,charsetName).getBytes("UTF-8");
            // System.out.println(new String(encoded));
            // } catch (FileNotFoundException e) {
            // e.printStackTrace();
            // } catch (IOException e) {
            // e.printStackTrace();
            // }

            // System.setProperty("file.encoding", "UTF-8");
            while ((record = reader.readNext()) != null) {
                Utilizzazione utilizzazione = new Utilizzazione();
                utilizzazione.setIdProgrammaMusicale(idPm);
                utilizzazione.setNumeroProgrammaMusicale(numeroPm);
                if (countRow == 1) {
                    record[0] = removeUTF8BOM(record[0]);
                }
                // if(record[0].matches("[0-9]+") && record[0].length()==11) {
                // utilizzazione.setCodiceOpera(record[0]);
                // }
                if (record[0].matches("[0-9]{11}")) {
                    utilizzazione.setCodiceOpera(record[0]);
                } else {
                    error = true;
                    return Response.status(500).entity(gson.toJson("Errore nell'aggiunta dell'opera sulla riga numero "
                            + countRow + ". Codice Opera non valido")).build();
                }
                if (record[1] != null) {
                    utilizzazione.setTitoloComposizione(record[1]);
                } else {
                    return Response.status(500).entity(gson.toJson(
                            "Errore nell'aggiunta dell'opera sulla riga numero " + countRow + ". Titolo obbligatorio"))
                            .build();
                }

                if (record[2] != null || record[3] != null || record[4] != null) {
                    utilizzazione.setCompositore(record[2]);
                    utilizzazione.setAutori(record[3]);
                    utilizzazione.setInterpreti(record[4]);
                } else {
                    return Response
                            .status(500).entity(gson.toJson("Errore nell'aggiunta dell'opera sulla riga numero "
                                    + countRow + ". Inserire almeno un parametro fra compositore, autore o interprete"))
                            .build();
                }

                if (record[5].equals("0") || record[5].equals("1")) {
                    utilizzazione.setDurataInferiore30sec(record[5]);
                } else {
                    return Response.status(500).entity(gson.toJson("Errore nell'aggiunta dell'opera sulla riga numero "
                            + countRow
                            + " . Colonna 'durata inferiore 30 secondi' può essere popolata solo dai valori 0 o 1"))
                            .build();
                }
                if (record[6] != null && !record[6].equals("")) {
                    utilizzazione.setDurata(Double.valueOf(record[6]));
                }
                utilizzazioni.add(utilizzazione);
                countRow++;
            }
            for (int i = 0; i < utilizzazioni.size(); i++) {

                programmaMusicaleService.addOpera(utilizzazioni.get(i));

                traceService.trace("Cruscotti", this.getClass().getName(), "/importOpere", "Aggiunta opera: "
                                + utilizzazioni.get(i).getCodiceOpera() + " da csv nel programma musicale con id: " + idPm,
                        Constants.ADD, userId);
            }
            return Response.ok().build();
        } catch (IOException e) {
            return Response.status(500).entity(gson.toJson("Errore nell'aggiunta delle opere")).build();
        }

    }

    @GET
    @Path("utilizzazione/{idCombana}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCodifica(@PathParam("idCombana") Long idCombana, @QueryParam("user") String user) {
        try {
            return Response.ok(programmaMusicaleService.getCodifica(idCombana, user)).build();
        } catch (NoResultException e) {
            return Response.noContent().build();
        } catch (Exception e) {
            return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).entity(e).build();
        }
    }

    private <T> StreamingOutput getStreamingOutputWithoutStrategy(final List<String[]> obj) {
        StreamingOutput so = new StreamingOutput() {
            @Override
            public void write(OutputStream outputStream) throws IOException, WebApplicationException {

                BufferedWriter buff = new BufferedWriter(new OutputStreamWriter(outputStream));
                CSVWriter writer = new CSVWriter(buff);
                try {
                    writer.writeAll(obj);
                    outputStream.flush();
                } catch (Exception e) {
                } finally {
                    if (writer != null)
                        writer.close();
                }
            }
        };
        return so;
    }

    private <T> StreamingOutput getListStreamingOutput(final List<T> obj, final CustomMappingStrategy mappingStrategy) {
        StreamingOutput so = new StreamingOutput() {
            @Override
            public void write(OutputStream outputStream) throws IOException, WebApplicationException {
                OutputStreamWriter osw = new OutputStreamWriter(outputStream);

                StatefulBeanToCsv beanToCsv = new StatefulBeanToCsvBuilder(osw).withMappingStrategy(mappingStrategy)
                        .withSeparator(';').build();
                try {
                    beanToCsv.write(obj);
                    osw.flush();
                    outputStream.flush();
                } catch (CsvDataTypeMismatchException e) {
                    System.out.println(e);
                } catch (CsvRequiredFieldEmptyException e) {
                    System.out.println(e);
                } finally {
                    if (osw != null)
                        osw.close();
                }
            }
        };
        return so;
    }

    private String removeUTF8BOM(String s) {
        if (s.startsWith(UTF8_BOM)) {
            s = s.substring(1);
        }
        return s;
    }

}