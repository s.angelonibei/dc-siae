package com.alkemytech.sophia.codman.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity(name="SqsFailedStatsDate")
@Table(name="SQS_FAILED")
@IdClass(SqsFailedStatsDateId.class)
@NamedNativeQuery(
	name="SqsFailedStatsDate.FindAll", 
	query="select DATE(RECEIVE_TIME) RECEIVE_DATE, QUEUE_NAME, count(*) QUEUE_SIZE from SQS_FAILED where RECEIVE_TIME > (CURRENT_TIMESTAMP - INTERVAL 30 DAY) group by DATE(RECEIVE_TIME), QUEUE_NAME order by RECEIVE_DATE DESC, QUEUE_NAME", 
	resultClass=SqsFailedStatsDate.class)
public class SqsFailedStatsDate {

	@Id
	@Column(name="RECEIVE_DATE", nullable=false)
	private Date receiveDate;

	@Id
	@Column(name="QUEUE_NAME", nullable=false)
	private String queueName;
	
	@Column(name="QUEUE_SIZE", nullable=false)
	private Long queueSize;
	
	public Date getReceiveDate() {
		return receiveDate;
	}

	public void setReceiveDate(Date receiveDate) {
		this.receiveDate = receiveDate;
	}

	public String getQueueName() {
		return queueName;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

	public Long getQueueSize() {
		return queueSize;
	}

	public void setQueueSize(Long queueSize) {
		this.queueSize = queueSize;
	}
	
}
