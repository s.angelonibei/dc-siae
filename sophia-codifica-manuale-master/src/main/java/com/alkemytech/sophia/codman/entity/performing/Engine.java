package com.alkemytech.sophia.codman.entity.performing;

import java.io.Serializable;

/**
 * Created by idilello on 11/12/16.
 */
public class Engine implements Serializable {


    String engineAddress;
    int enginePort;

    public Engine(String address){
        String[] hostPort = address.split(":");
        this.engineAddress=hostPort[0];
        this.enginePort=Integer.parseInt(hostPort[1]);
    }

    public String getEngineAddress() {

        // ToDo: Remove
        //engineAddress="127.0.0.1";

        return engineAddress;
    }

    public void setEngineAddress(String engineAddress) {
        this.engineAddress = engineAddress;
    }

    public int getEnginePort() {
        return enginePort;
    }

    public void setEnginePort(int enginePort) {
        this.enginePort = enginePort;
    }
}
