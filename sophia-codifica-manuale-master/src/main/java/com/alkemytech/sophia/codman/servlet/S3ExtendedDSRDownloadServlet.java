package com.alkemytech.sophia.codman.servlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.common.s3.S3Service;
import com.amazonaws.services.s3.AmazonS3URI;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
@SuppressWarnings("serial")
public class S3ExtendedDSRDownloadServlet extends HttpServlet {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final S3Service s3Service;
    
    @Inject
    protected S3ExtendedDSRDownloadServlet(S3Service s3Service) {
        super();
        this.s3Service = s3Service;
    }

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) {

        try {
            String url = request.getParameter("url");
			final AmazonS3URI s3Uri = new AmazonS3URI(url);
            String fileName = url !=  null? url.substring(url.lastIndexOf("/")+1, url.length()) : "extended_dsr";
            response.addHeader(HttpHeaders.CONTENT_TYPE, "text/csv");
            response.addHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
            s3Service.download(s3Uri.getBucket(), s3Uri.getKey(), response.getOutputStream());
        } catch (Exception e) {
            logger.error("S3ExtendedDSRDownloadServlet", e);
        }

    }

}
