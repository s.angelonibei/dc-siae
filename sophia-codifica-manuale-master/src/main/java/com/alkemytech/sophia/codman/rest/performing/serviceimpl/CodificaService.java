package com.alkemytech.sophia.codman.rest.performing.serviceimpl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.TimeZone;

import com.alkemytech.sophia.codman.entity.performing.PerfCodificaMassiva;
import com.alkemytech.sophia.codman.entity.performing.dao.ICodificaMassivaDAO;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.Message;
import org.kie.api.builder.Results;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alkemytech.sophia.codman.dto.performing.CodificaDto;
import com.alkemytech.sophia.codman.dto.performing.CodificaListDTO;
import com.alkemytech.sophia.codman.dto.performing.CodificaOpereRequestDTO;
import com.alkemytech.sophia.codman.dto.performing.CombanaDTO;
import com.alkemytech.sophia.codman.dto.performing.ConfigurazioneSoglieDTO;
import com.alkemytech.sophia.codman.dto.performing.MonitoraggioCodificatoResponse;
import com.alkemytech.sophia.codman.dto.performing.RiepilogoCodificaManualeResponse;
import com.alkemytech.sophia.codman.entity.performing.ConfigurazioniSoglie;
import com.alkemytech.sophia.codman.entity.performing.dao.ICodificaDAO;
import com.alkemytech.sophia.codman.rest.performing.TimerSessionResponse;
import com.alkemytech.sophia.codman.rest.performing.service.ICodificaService;
import com.alkemytech.sophia.codman.utils.DateUtils;
import com.alkemytech.sophia.codman.utils.Response;
import com.alkemytech.sophia.common.s3.S3Service;
import com.alkemytech.sophia.commons.io.CompressionAwareFileOutputStream;
import com.amazonaws.services.s3.AmazonS3URI;
import com.google.common.io.Files;
import com.google.inject.Inject;
import com.google.inject.Provider;

@Service("codificaService")
public class CodificaService implements ICodificaService {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	private ICodificaDAO codificaDAO;
	private ICodificaMassivaDAO codificaMassivaDAO;

	private final Properties configuration;
	private final SequentialAdapter<CombanaDTO> codificaBaseCombanaAdapter = new CodificaBaseCombanaAdapter();
	Provider<SequentialAdapter<CombanaDTO>> codificaEspertoCombanaAdapterProvider;
	
	@Inject
	public CodificaService(ICodificaDAO codificaDAO, ICodificaMassivaDAO codificaMassivaDAO, Properties configuration
			, Provider<SequentialAdapter<CombanaDTO>> combanaAdapterProvider) {
		this.codificaDAO = codificaDAO;
		this.codificaMassivaDAO = codificaMassivaDAO;
		this.configuration = configuration;
		this.codificaEspertoCombanaAdapterProvider = combanaAdapterProvider;
	}

	public List<String> addConfigurazioneSoglie(ConfigurazioneSoglieDTO confSoglia, InputStream is, S3Service s3Service,
			String pathFileS3) {
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		f.setTimeZone(TimeZone.getTimeZone("GMT+2"));
		List<String> errori = new ArrayList<>();
		List<String> erroriCodice = getValidazioneDate(confSoglia.getInizioPeriodoCompetenza(),
				confSoglia.getFinePeriodoCompetenza());
		errori.addAll(erroriCodice);
		if (!errori.isEmpty()) {
			return errori;
		}

		ConfigurazioniSoglie newConfig = new ConfigurazioniSoglie();
		newConfig.setCodiceConfigurazione(confSoglia.getCodiceConfigurazione());
		newConfig.setInizioPeriodoCompetenza(
				DateUtils.stringToDate(confSoglia.getInizioPeriodoCompetenza(), DateUtils.DATE_ITA_FORMAT_DASH));
		if (confSoglia.getFinePeriodoCompetenza() != null) {
			newConfig.setFinePeriodoCompetenza(
					DateUtils.stringToDate(confSoglia.getFinePeriodoCompetenza(), DateUtils.DATE_ITA_FORMAT_DASH));
		}
		newConfig.setUtenteCaricamentoFile(confSoglia.getUtenteCaricamentoFile());
		newConfig.setDataCaricamentoFile(DateUtils.stringToDate(f.format(new Date()), DateUtils.DATETIME_MYSQL_FORMAT));
		newConfig.setDataUltimaModifica(DateUtils.stringToDate(f.format(new Date()), DateUtils.DATETIME_MYSQL_FORMAT));
		String nomeFileOnPath;
		if (Files.getFileExtension(confSoglia.getNomeFile()).equals("xls")
				|| Files.getFileExtension(confSoglia.getNomeFile()).equals("xlsx")) {
			nomeFileOnPath = Files.getNameWithoutExtension(confSoglia.getNomeFile()) + "_" + (new Date().getTime())
					+ "." + Files.getFileExtension(confSoglia.getNomeFile());
		} else {
			errori.add("Estensione del file non supportata");
			return errori;
		}
		newConfig.setUtenteUltimaModifica(confSoglia.getUtenteCaricamentoFile());
		newConfig.setPathFile(pathFileS3 + nomeFileOnPath);
		newConfig.setNomeOriginaleFile(confSoglia.getNomeFile());
		newConfig.setFlagAttivo(true);
		Long codiceConfigurazioneMax = codificaDAO.getMaxCodiceRegolaOnConfigurazioneSoglie();
		if (codiceConfigurazioneMax == null)
			newConfig.setCodiceConfigurazione(1L);
		else
			newConfig.setCodiceConfigurazione(codiceConfigurazioneMax + 1);

		// Controllo la validità del file drools
		final String TEMP_URL = "/tmp";
		final File file = new File(TEMP_URL, nomeFileOnPath);
		try {
			if (saveToFile(is, file)) { // save to temporary local file
				final List<String> errors = getDecisionTableErrors(file); // validate file with drools
				if (null == errors || errors.isEmpty()) {

					final AmazonS3URI s3Uri = new AmazonS3URI(pathFileS3 + file.getName());
					if (s3Service.upload(s3Uri.getBucket(), s3Uri.getKey(), file)) { // upload to s3
						// save to database
						codificaDAO.addConfigurazioneSoglie(newConfig);
					}
				} else {
					errori.add(Response.PERFORMING_CODIFICA_VALIDAZIONE_FILE);
				}
			}
		} catch (Exception e) {
			errori.add(Response.PERFORMING_CODIFICA_ERRORE_CARICAMENTO_FILE_GENERICO);
			return errori;
		} finally {
			file.delete();
		}
		return errori;
	}

	public List<String> updateConfigurazioneSoglie(ConfigurazioneSoglieDTO confSoglia,String user, InputStream is,
			S3Service s3Service, String pathFileS3) {

		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		f.setTimeZone(TimeZone.getTimeZone("GMT+2"));

		ConfigurazioniSoglie currentConfigurazione = codificaDAO.getConfigurazioneSoglieById(confSoglia.getId());

		List<String> errori = new ArrayList<String>();

		// si copiano i dati della configurazione corrente per creare un record nello
		// storico
		ConfigurazioniSoglie configurazioneOldHistory = new ConfigurazioniSoglie();
		configurazioneOldHistory.setCodiceConfigurazione(currentConfigurazione.getCodiceConfigurazione());
		configurazioneOldHistory.setInizioPeriodoCompetenza(currentConfigurazione.getInizioPeriodoCompetenza());
		if (currentConfigurazione.getFinePeriodoCompetenza() != null)
			configurazioneOldHistory.setFinePeriodoCompetenza(currentConfigurazione.getFinePeriodoCompetenza());
		configurazioneOldHistory.setDataCaricamentoFile(currentConfigurazione.getDataCaricamentoFile());
		configurazioneOldHistory.setUtenteCaricamentoFile(currentConfigurazione.getUtenteCaricamentoFile());
		configurazioneOldHistory.setDataUltimaModifica(currentConfigurazione.getDataUltimaModifica());
		configurazioneOldHistory.setUtenteUltimaModifica(currentConfigurazione.getUtenteUltimaModifica());
		configurazioneOldHistory.setPathFile(currentConfigurazione.getPathFile());
		configurazioneOldHistory.setNomeOriginaleFile(currentConfigurazione.getNomeOriginaleFile());
		configurazioneOldHistory.setFlagAttivo(false);

		try {

			List<String> erroriDate = getValidazioneDateOnUpdate(confSoglia.getInizioPeriodoCompetenza(),
					confSoglia.getFinePeriodoCompetenza(), confSoglia.getCodiceConfigurazione());

			if (!erroriDate.isEmpty()) {
				errori.addAll(erroriDate);
				return errori;
			}

			// si modificano i parametri della configurazione corrente per fare l'update
			currentConfigurazione.setInizioPeriodoCompetenza(
					DateUtils.stringToDate(confSoglia.getInizioPeriodoCompetenza(), DateUtils.DATE_ITA_FORMAT_DASH));
			if (confSoglia.getInizioPeriodoCompetenza() != null)
				currentConfigurazione.setFinePeriodoCompetenza(
						DateUtils.stringToDate(confSoglia.getFinePeriodoCompetenza(), DateUtils.DATE_ITA_FORMAT_DASH));
			currentConfigurazione.setDataUltimaModifica(
					DateUtils.stringToDate(f.format(new Date()), DateUtils.DATETIME_MYSQL_FORMAT));
			currentConfigurazione.setUtenteUltimaModifica(user);
			// controlla se il file è vuoto o no
			if (is != null) {
				// se il file c'è, si procede all'update dei parametri sul db ed all'upload del
				// nuovo file
				String nomeFileOnPath;
				if (Files.getFileExtension(confSoglia.getNomeFile()).equals("xls")
						|| Files.getFileExtension(confSoglia.getNomeFile()).equals("xlsx")) {
					nomeFileOnPath = Files.getNameWithoutExtension(confSoglia.getNomeFile()) + "_"
							+ (new Date().getTime()) + "." + Files.getFileExtension(confSoglia.getNomeFile());
				} else {
					errori.add("Estensione del file non supportata");
					return errori;
				}
				currentConfigurazione.setPathFile(pathFileS3 + nomeFileOnPath);
				currentConfigurazione.setNomeOriginaleFile(confSoglia.getNomeFile());

				// Controllo la validità del file drools
				final String TEMP_URL = "/tmp";
				final File file = new File(TEMP_URL, nomeFileOnPath);
				try {
					if (saveToFile(is, file)) { // save to temporary local file
						final List<String> errors = getDecisionTableErrors(file); // validate file with drools
						if (null == errors || errors.isEmpty()) {

							final AmazonS3URI s3Uri = new AmazonS3URI(pathFileS3 + file.getName());
							if (s3Service.upload(s3Uri.getBucket(), s3Uri.getKey(), file)) { // upload to s3
								// save to database
								codificaDAO.updateConfigurazioneSoglie(currentConfigurazione);
								codificaDAO.addConfigurazioneSoglie(configurazioneOldHistory);
							}
						} else {
							errori.add(Response.PERFORMING_CODIFICA_VALIDAZIONE_FILE);
						}
					}
				} catch (Exception e) {
					errori.add(Response.PERFORMING_CODIFICA_ERRORE_CARICAMENTO_FILE_GENERICO);
					return errori;
				} finally {
					file.delete();
				}
			} else {
				codificaDAO.updateConfigurazioneSoglie(currentConfigurazione);
				codificaDAO.addConfigurazioneSoglie(configurazioneOldHistory);
			}

		} catch (Exception e) {
			logger.error(e.toString());
		}

		return errori;

	}

	private boolean saveToFile(InputStream in, File file) {
		OutputStream out = null;
		try {
			int count = 0;
			final byte[] bytes = new byte[1024];
			out = new FileOutputStream(file);
			while (-1 != (count = in.read(bytes))) {
				out.write(bytes, 0, count);
			}
			out.close();
			out = null;
			return true;
		} catch (IOException e) {
			return false;
		} finally {
			if (null != out) {
				try {
					out.close();
				} catch (IOException e) {
				}
			}
		}
	}
	
	private boolean saveToFileCompressed(InputStream in, File file) {
		OutputStream out = null;
		try {
			int count = 0;
			final byte[] bytes = new byte[1024];
			out = new CompressionAwareFileOutputStream(file);
			while (-1 != (count = in.read(bytes))) {
				out.write(bytes, 0, count);
			}
			out.close();
			out = null;
			return true;
		} catch (IOException e) {
			return false;
		} finally {
			if (null != out) {
				try {
					out.close();
				} catch (IOException e) {
				}
			}
		}
	}	

	private List<String> getDecisionTableErrors(File file) throws FileNotFoundException {
		InputStream in = null;
		try {
			in = new FileInputStream(file);
			final KieServices kieServices = KieServices.Factory.get();
			final KieFileSystem kieFileSystem = kieServices.newKieFileSystem();
			kieFileSystem.write("src/main/resources/dtable.xls", kieServices.getResources().newInputStreamResource(in));
			final KieBuilder kieBuilder = kieServices.newKieBuilder(kieFileSystem).buildAll();
			final Results results = kieBuilder.getResults();
			if (!results.hasMessages(Message.Level.ERROR)) {
				return Collections.<String>emptyList();
			} else {
				final List<Message> messages = results.getMessages();
				if (null != messages && !messages.isEmpty()) {
					final List<String> errors = new ArrayList<>(messages.size());
					for (Message message : messages) {
						errors.add(message.getText() + " (line " + message.getLine() + ", column " + message.getColumn()
								+ ")");
					}
					return errors;
				}
			}
		} catch (Exception e) {
			final StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			return Arrays.asList("Bad decision table XLS file", e.getMessage(), out.toString());
		} finally {
			if (null != in) {
				try {
					in.close();
				} catch (IOException e) {
					logger.error(e.getMessage());
				}
			}
		}
		return Arrays.asList("Bad decision table XLS file");
	}

	public List<String> getValidazioneDate(String inizioPeriodoValidazione, String finePeriodoValidazione) {
		List<String> errori = new ArrayList<>();
		List<Long> configurazioniConflitto;
		ConfigurazioniSoglie configurazioneInCorso = codificaDAO.configurazioneIsInCorso();
		if (finePeriodoValidazione == null) {
			if (configurazioneInCorso != null) {
				errori.add("Periodo di validità del codice configurazione "
						+ configurazioneInCorso.getCodiceConfigurazione() + " in corso");
			} else {
				configurazioniConflitto = codificaDAO.getCodiciRegolaAfterDate(inizioPeriodoValidazione);
				if (!configurazioniConflitto.isEmpty()) {
					for (Long configurazioneConflitto : configurazioniConflitto) {
						errori.add("Conflitto nelle date del codice configurazione " + configurazioneConflitto);
					}
				}
			}
		} else {
			if (configurazioneInCorso != null && (configurazioneInCorso.getInizioPeriodoCompetenza()
					.before(DateUtils.stringToDate(inizioPeriodoValidazione, DateUtils.DATE_ITA_FORMAT_DASH))
					|| configurazioneInCorso.getInizioPeriodoCompetenza()
							.before(DateUtils.stringToDate(finePeriodoValidazione, DateUtils.DATE_ITA_FORMAT_DASH)))) {
				errori.add("Periodo di validità del codice configurazione "
						+ configurazioneInCorso.getCodiceConfigurazione() + " in corso");
			} else {
				configurazioniConflitto = codificaDAO.getValidazioneDate(inizioPeriodoValidazione,
						finePeriodoValidazione);
				if (!configurazioniConflitto.isEmpty()) {
					for (Long configurazioneConflitto : configurazioniConflitto) {
						errori.add("Conflitto nelle date del codice configurazione " + configurazioneConflitto);
					}
				}
			}
		}
		return errori;
	}

	public List<String> getValidazioneDateOnUpdate(String inizioPeriodoValidazione, String finePeriodoValidazione,
			Long codiceRegola) {
		List<String> errori = new ArrayList<>();
		List<Long> configurazioniConflitto;
		ConfigurazioniSoglie confInCorso = codificaDAO.configurazioneIsInCorso();
		if (finePeriodoValidazione == null) {
			if (confInCorso != null && !confInCorso.getCodiceConfigurazione().equals(codiceRegola)) {
				errori.add(Response.PERFORMING_CODIFICA_PERIODO_VALIDITA + confInCorso.getCodiceConfigurazione()
						+ Response.PERFORMING_CODIFICA_IN_CORSO);
			} else {
				configurazioniConflitto = codificaDAO.getCodiciRegolaAfterDate(inizioPeriodoValidazione);
				if (!configurazioniConflitto.isEmpty()) {
					for (Long aggioConflitto : configurazioniConflitto) {
						if (!aggioConflitto.equals(codiceRegola)) {
							errori.add(Response.PERFORMING_CODIFICA_CONFLITTO_DATE + aggioConflitto);
						}
					}
				}
			}
		} else {
			if (confInCorso != null && !confInCorso.getCodiceConfigurazione().equals(codiceRegola)
					&& (confInCorso.getInizioPeriodoCompetenza()
							.before(DateUtils.stringToDate(inizioPeriodoValidazione, DateUtils.DATE_ITA_FORMAT_DASH))
							|| confInCorso.getInizioPeriodoCompetenza().before(
									DateUtils.stringToDate(finePeriodoValidazione, DateUtils.DATE_ITA_FORMAT_DASH)))) {
				errori.add(Response.PERFORMING_CODIFICA_PERIODO_VALIDITA + confInCorso.getCodiceConfigurazione()
						+ Response.PERFORMING_CODIFICA_IN_CORSO);
			} else {
				configurazioniConflitto = codificaDAO.getValidazioneDateOnUpdate(inizioPeriodoValidazione,
						finePeriodoValidazione, codiceRegola);
				if (!configurazioniConflitto.isEmpty()) {
					for (Long aggioConflitto : configurazioniConflitto) {
						if (!aggioConflitto.equals(codiceRegola)) {
							errori.add(Response.PERFORMING_CODIFICA_CONFLITTO_DATE + aggioConflitto);
						}
					}
				}
			}
		}
		return errori;
	}

	public List<ConfigurazioniSoglie> getListaConfigurazioniSoglieAttivi(String inizioPeriodoValidazione,
			String finePeriodoValidazione, String order, Integer page) {
		return codificaDAO.getListaConfigurazioniSoglieAttivi(inizioPeriodoValidazione, finePeriodoValidazione, order,
				page);
	}

	public Integer countListaConfigurazioniSoglieAttivi(String inizioPeriodoValidazione,
			String finePeriodoValidazione) {
		return codificaDAO.countListaConfigurazioniSoglieAttivi(inizioPeriodoValidazione, finePeriodoValidazione);
	}

	public List<ConfigurazioniSoglie> getStoricoConfigurazioneSoglie(Long codiceConfigurazione) {

		return codificaDAO.getStoricoConfigurazioneSoglie(codiceConfigurazione);
	}

	public ConfigurazioniSoglie getConfigurazioneSoglie(Long id) {
		return codificaDAO.getConfigurazioneSoglie(id);
	}

	public CodificaListDTO getListCodificaEsperto(String voceIncasso, String fonte, String titolo, String nominativo,
			String username, Integer limitCodifichePerforming) {
		List<CodificaDto> listCodificaEsperto = codificaDAO.getListCodificaEsperto(voceIncasso, fonte, titolo, nominativo, username,
						limitCodifichePerforming);
		SequentialAdapter<CombanaDTO> combanaAdapter = codificaEspertoCombanaAdapterProvider.get();
		if (combanaAdapter != null) {
			for (CodificaDto codificaDto : listCodificaEsperto) {
				List<CombanaDTO> candidate = codificaDto.getCandidate();
				for (CombanaDTO combana : candidate) {
					combanaAdapter.process(combana);
				}
			}
		}
		CodificaListDTO codificaListDTO = new CodificaListDTO(configuration.getProperty("performing.limit.codifiche.esperto"),
				listCodificaEsperto);
		for (Modifier<CombanaDTO> modifier : combanaAdapter.getModifications()) {
			codificaListDTO.getExtraFieldsToShow().addAll(modifier.getApplicationFields());
		}
		return codificaListDTO;
	}

	@Override
	public CodificaListDTO getListCodificaBase(String username, Integer limitCodifichePerforming) {
		List<CodificaDto> listCodificaBase = codificaDAO.getListCodificaBase(username, limitCodifichePerforming);
		if (codificaBaseCombanaAdapter != null) {
			for (CodificaDto codificaDto : listCodificaBase) {
				List<CombanaDTO> candidate = codificaDto.getCandidate();
				for (CombanaDTO combana : candidate) {
					codificaBaseCombanaAdapter.process(combana);
				}
			}
		}
		CodificaListDTO codificaListDTO = new CodificaListDTO(configuration.getProperty("performing.limit.codifiche.base"),
				listCodificaBase);
		return codificaListDTO;
	}

	@Override
	public void getApprovaOpera(CodificaOpereRequestDTO codificaOpereRequestDTO) {
		codificaDAO.approvaOpera(codificaOpereRequestDTO);
	}

	@Override
	public TimerSessionResponse getSessionTimer(String username) {
		return codificaDAO.getSessionTimer(username);
	}

	@Override
	public TimerSessionResponse refreshSession(String username) {
		return codificaDAO.refreshSession(username);
	}

	@Override
	public List<RiepilogoCodificaManualeResponse> getRiepilogoCodificaManualeResponseList(Long idPeriodoRipertizione, int page, String order) {
		return codificaDAO.getRiepilogoCodificaManualeResponseList(idPeriodoRipertizione, page, order);
	}

	@Override
	public MonitoraggioCodificatoResponse getMonitoraggioCodificato(Long idPeriodoRipartizione, int page, String order) {
		return codificaDAO.getMonitoraggioCodificato(idPeriodoRipartizione, page, order);
	}

	@Override
	public PerfCodificaMassiva saveCodificaMassiva(PerfCodificaMassiva perfCodificaMassiva){
		return codificaMassivaDAO.save(perfCodificaMassiva);
	};
	
	@Override
	public String uploadFile(String fileName, InputStream is, S3Service s3Service, String pathFileS3) {
		final File file = new File(fileName);
		try {
			if (saveToFileCompressed(is, file)) {
				final AmazonS3URI s3Uri = new AmazonS3URI(pathFileS3 + file.getName());
				boolean uploadSuccessfull = s3Service.upload(s3Uri.getBucket(), s3Uri.getKey(), file);
				if (uploadSuccessfull) {
					return s3Uri.getURI().getPath();
				}
			}
		} catch (Exception e) {
			logger.error("Errore Upload File CSV to S3", e);
		} finally {
			file.delete();
		}
		return null;
	}

	@Override
	public Integer getUploadedFilesCount() {
		return codificaMassivaDAO.getUploadedFilesCount();
	}

	@Override
	public List<PerfCodificaMassiva> getUploadedFiles(String order, Integer page) {
		return codificaMassivaDAO.getUploadedFiles(order,page);
	}
	
}
