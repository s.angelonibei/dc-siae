package com.alkemytech.sophia.codman.broadcasting;

import java.util.Date;
import java.util.UUID;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.apache.http.util.TextUtils;

import com.alkemytech.sophia.broadcasting.dto.BdcGenerateUserTokenRequestDto;
import com.alkemytech.sophia.broadcasting.dto.TokenResponse;
import com.alkemytech.sophia.broadcasting.service.interfaces.BdcUserTokenService;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

/**
 * Created by Alessandro Ravà on 12/12/2017.
 */
@Singleton
@Path("userToken")
public class UserTokenRestService {
	private String repertori;
	private String repertorioDefault;
	private BdcUserTokenService bdcUserTokenService;
	private Gson gson;

	@Inject
	public UserTokenRestService(@Named("repertori") String repertori,
			@Named("repertorioDefault") String repertorioDefault, BdcUserTokenService bdcUserTokenService, Gson gson) {
		this.repertori = repertori;
		this.repertorioDefault = repertorioDefault;
		this.bdcUserTokenService = bdcUserTokenService;
		this.gson = gson;
	}

	@POST
	@Path("generateUserToken")
	@Produces("application/json")
	public Response deleteUser(BdcGenerateUserTokenRequestDto bdcGenerateUserTokenRequestDto) {
		Date dataCreazione = new Date();
		Date dataFineValidita = new Date(dataCreazione.getTime() + 60000);
		try {
			String token = bdcUserTokenService.createToken(bdcGenerateUserTokenRequestDto.getBdcUserId(), dataCreazione,
					dataFineValidita, null, repertori, repertorioDefault, UUID.randomUUID().toString(), null,
					bdcGenerateUserTokenRequestDto.getUsername());
			TokenResponse response = new TokenResponse();
			response.setToken(token);
			if (!TextUtils.isEmpty(token)) {

				return Response.ok(gson.toJson(response)).build();
			} else {
				return Response.status(500).build();
			}
		} catch (Exception e) {
			return Response.status(500).build();
		}
	}
}
