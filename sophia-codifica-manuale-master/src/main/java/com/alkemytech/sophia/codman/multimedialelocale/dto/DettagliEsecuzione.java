package com.alkemytech.sophia.codman.multimedialelocale.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.alkemytech.sophia.codman.multimedialelocale.entity.MultimedialeLocale;

public class DettagliEsecuzione {
	private Date dataAvvio;
	private Long reportDaRiprocessare;
	private BigDecimal reportRiprocessati;
	private String ripartizione;
	private Date dataAcquisizioneDocumentazione;
	
	private List<PeriodiRilavorati> periodiRilavorati;
	private BigDecimal recordDaRiprocessare;
	private BigDecimal recordRiprocessati;
	private BigDecimal utilizzazioniDaRiprocessare;
	private BigDecimal utilizzazioniRiprocessati;

	private BigDecimal recordCodificati;
	private Double incrementoRecordCodificati;
	private Double recordCodificatiRegolari;
	private Double incrementoRecordCodificatiRegolari;
	private BigDecimal reportAggiornati;
	
	
	private BigDecimal utilizzazioniCodificate;
	private Double incrementoUtilizzazioniCodificate;
	private Double utilizzazioniCodificateRegolari;
	private Double incrementoUtilizzazioniCodificateRegolari;
	

	public Date getDataAvvio() {
		return dataAvvio;
	}

	public void setDataAvvio(Date dataAvvio) {
		this.dataAvvio = dataAvvio;
	}

	public Long getReportDaRiprocessare() {
		return reportDaRiprocessare;
	}

	public void setReportDaRiprocessare(Long reportDaRiprocessare) {
		this.reportDaRiprocessare = reportDaRiprocessare;
	}

	public BigDecimal getReportRiprocessati() {
		return reportRiprocessati;
	}

	public void setReportRiprocessati(BigDecimal reportRiprocessati) {
		this.reportRiprocessati = reportRiprocessati;
	}

	public String getRipartizione() {
		return ripartizione;
	}

	public void setRipartizione(String ripartizione) {
		this.ripartizione = ripartizione;
	}

	public Date getDataAcquisizioneDocumentazione() {
		return dataAcquisizioneDocumentazione;
	}

	public void setDataAcquisizioneDocumentazione(Date dataAcquisizioneDocumentazione) {
		this.dataAcquisizioneDocumentazione = dataAcquisizioneDocumentazione;
	}

	public List<PeriodiRilavorati> getPeriodiRilavorati() {
		return periodiRilavorati;
	}

	public void setPeriodiRilavorati(List<PeriodiRilavorati> periodiRilavorati) {
		this.periodiRilavorati = periodiRilavorati;
	}

	public BigDecimal getRecordDaRiprocessare() {
		return recordDaRiprocessare;
	}

	public void setRecordDaRiprocessare(BigDecimal recordDaRiprocessare) {
		this.recordDaRiprocessare = recordDaRiprocessare;
	}

	public BigDecimal getRecordRiprocessati() {
		return recordRiprocessati;
	}

	public void setRecordRiprocessati(BigDecimal recordRiprocessati) {
		this.recordRiprocessati = recordRiprocessati;
	}

	public BigDecimal getUtilizzazioniDaRiprocessare() {
		return utilizzazioniDaRiprocessare;
	}

	public void setUtilizzazioniDaRiprocessare(BigDecimal utilizzazioniDaRiprocessare) {
		this.utilizzazioniDaRiprocessare = utilizzazioniDaRiprocessare;
	}

	public BigDecimal getUtilizzazioniRiprocessati() {
		return utilizzazioniRiprocessati;
	}

	public void setUtilizzazioniRiprocessati(BigDecimal utilizzazioniRiprocessati) {
		this.utilizzazioniRiprocessati = utilizzazioniRiprocessati;
	}

	public BigDecimal getRecordCodificati() {
		return recordCodificati;
	}

	public void setRecordCodificati(BigDecimal recordCodificati) {
		this.recordCodificati = recordCodificati;
	}

	public Double getIncrementoRecordCodificati() {
		return incrementoRecordCodificati;
	}

	public void setIncrementoRecordCodificati(Double incrementoRecordCodificati) {
		this.incrementoRecordCodificati = incrementoRecordCodificati;
	}

	public Double getRecordCodificatiRegolari() {
		return recordCodificatiRegolari;
	}

	public void setRecordCodificatiRegolari(Double recordCodificatiRegolari) {
		this.recordCodificatiRegolari = recordCodificatiRegolari;
	}

	public Double getIncrementoRecordCodificatiRegolari() {
		return incrementoRecordCodificatiRegolari;
	}

	public void setIncrementoRecordCodificatiRegolari(Double incrementoRecordCodificatiRegolari) {
		this.incrementoRecordCodificatiRegolari = incrementoRecordCodificatiRegolari;
	}

	public BigDecimal getUtilizzazioniCodificate() {
		return utilizzazioniCodificate;
	}

	public void setUtilizzazioniCodificate(BigDecimal utilizzazioniCodificate) {
		this.utilizzazioniCodificate = utilizzazioniCodificate;
	}

	public Double getIncrementoUtilizzazioniCodificate() {
		return incrementoUtilizzazioniCodificate;
	}

	public void setIncrementoUtilizzazioniCodificate(Double incrementoUtilizzazioniCodificate) {
		this.incrementoUtilizzazioniCodificate = incrementoUtilizzazioniCodificate;
	}

	public Double getUtilizzazioniCodificateRegolari() {
		return utilizzazioniCodificateRegolari;
	}

	public void setUtilizzazioniCodificateRegolari(Double utilizzazioniCodificateRegolari) {
		this.utilizzazioniCodificateRegolari = utilizzazioniCodificateRegolari;
	}

	public Double getIncrementoUtilizzazioniCodificateRegolari() {
		return incrementoUtilizzazioniCodificateRegolari;
	}

	public void setIncrementoUtilizzazioniCodificateRegolari(Double incrementoUtilizzazioniCodificateRegolari) {
		this.incrementoUtilizzazioniCodificateRegolari = incrementoUtilizzazioniCodificateRegolari;
	}

	

	public BigDecimal getReportAggiornati() {
		return reportAggiornati;
	}

	public void setReportAggiornati(BigDecimal reportAggiornati) {
		this.reportAggiornati = reportAggiornati;
	}
	
	
}
