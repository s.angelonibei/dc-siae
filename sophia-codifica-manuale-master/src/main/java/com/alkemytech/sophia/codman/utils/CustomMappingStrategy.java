package com.alkemytech.sophia.codman.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.FieldUtils;

import com.opencsv.bean.BeanField;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

import java.lang.annotation.Annotation;

public class CustomMappingStrategy<T> extends ColumnPositionMappingStrategy<T> {
    @Override
    public String[] generateHeader(T bean) throws CsvRequiredFieldEmptyException {

        super.setColumnMapping(new String[FieldUtils.getAllFields(bean.getClass()).length]);
        final int numColumns = findMaxFieldIndex();
        if (!isAnnotationDriven() || numColumns == -1) {
            return super.generateHeader(bean);
        }

        String[] header = new String[numColumns + 1];

        BeanField<T> beanField;
        for (int i = 0; i <= numColumns; i++) {
            beanField = findField(i);
            String columnHeaderName = extractHeaderName(beanField);
            header[i] = columnHeaderName;
        }
        return header;
    }

    private String extractHeaderName(final BeanField<T> beanField) {
        int i = 0;
        if(beanField!=null) {
            for (Annotation a : beanField.getField().getDeclaredAnnotations()) {
                if (a.annotationType().equals(CsvBindByName.class)) {
                    if (beanField == null || beanField.getField() == null) {
                        return StringUtils.EMPTY;
                    }
                    return ((CsvBindByName) beanField.getField().getDeclaredAnnotations()[i]).column();
                }
                i++;
            }
        }
        return StringUtils.EMPTY;
    }
}