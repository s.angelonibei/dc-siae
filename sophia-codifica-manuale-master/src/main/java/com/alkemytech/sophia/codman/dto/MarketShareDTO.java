package com.alkemytech.sophia.codman.dto;

import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Produces("application/json")
@XmlRootElement
public class MarketShareDTO implements Serializable {

	private static final long serialVersionUID = 1911294021205283360L;

	private String dsp;
	private String periodType;
	private Integer periodNumber;
	private Integer year;
	private String period;
	private String country;
	private String utilizationType;
	private String commercialOffer;
	private BigDecimal totUsage;
	private BigDecimal siaeIdentifiedVisualizations;
	private Double marketShare;
	private Date dateFrom;
	private Date dateTo;

	public MarketShareDTO(Object[] obj) {
		if (null != obj && obj.length >= 13) {
				dsp = (String) obj[0];
				periodType = (String) obj[1];
				periodNumber = (Integer) obj[2];
				year = (Integer) obj[3];
				period = (String) obj[4];
				country = (String) obj[5];
				utilizationType = (String) obj[6];
				commercialOffer = (String) obj[7];
				totUsage = obj[8] != null ? (BigDecimal) obj[8] : null;
				siaeIdentifiedVisualizations = obj[9] != null ? (BigDecimal) obj[9] : null;
				marketShare = obj[10] != null ? (Double) obj[10] : null;
				dateFrom = (Date) obj[11];
				dateTo = (Date) obj[12];
		}
	}

	public String getDsp() {
		return dsp;
	}

	public void setDsp(String dsp) {
		this.dsp = dsp;
	}


	public String getPeriodType() {
		return periodType;
	}

	public void setPeriodType(String periodType) {
		this.periodType = periodType;
	}

	public Integer getPeriodNumber() {
		return periodNumber;
	}

	public void setPeriodNumber(Integer periodNumber) {
		this.periodNumber = periodNumber;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getUtilizationType() {
		return utilizationType;
	}

	public void setUtilizationType(String utilizationType) {
		this.utilizationType = utilizationType;
	}

	public String getCommercialOffer() {
		return commercialOffer;
	}

	public void setCommercialOffer(String commercialOffer) {
		this.commercialOffer = commercialOffer;
	}

	public Double getMarketShare() {
		return marketShare;
	}

	public void setMarketShare(Double marketShare) {
		this.marketShare = marketShare;
	}

	public BigDecimal getTotUsage() {
		return totUsage;
	}

	public void setTotUsage(BigDecimal totUsage) {
		this.totUsage = totUsage;
	}



	public BigDecimal getSiaeIdentifiedVisualizations() {
		return siaeIdentifiedVisualizations;
	}

	public void setSiaeIdentifiedVisualizations(BigDecimal siaeIdentifiedVisualizations) {
		this.siaeIdentifiedVisualizations = siaeIdentifiedVisualizations;
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public MarketShareDTO setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
		return this;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public MarketShareDTO setDateTo(Date dateTo) {
		this.dateTo = dateTo;
		return this;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
