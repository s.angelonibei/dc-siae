package com.alkemytech.sophia.codman.service.dsrmetadata.cardinality;

public class BaseCardinality implements Cardinality {

	private String regex = "_[1-9]*of[1-9]*_";
	private String code = "_XofY_";
	private String description = "Campo di posizionamento Es: _1of5_ o _2of5_";

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String getRegex() {
		return regex;
	}

	@Override
	public String getCode() {
		return code;
	}
}
