package com.alkemytech.sophia.codman.rest.performing.serviceimpl;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Provider;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.codman.dto.performing.CombanaDTO;
import com.alkemytech.sophia.codman.entity.Configuration;
import com.alkemytech.sophia.codman.entity.KeyValueProperty;
import com.alkemytech.sophia.codman.entity.Relation;
import com.alkemytech.sophia.codman.entity.Setting;
import com.alkemytech.sophia.codman.entity.Threshold;
import com.alkemytech.sophia.codman.entity.ThresholdSet;
import com.alkemytech.sophia.codman.entity.performing.dao.IConfigurationDAO;

public class CodificaEspertoCombanaAdapterProvider implements Provider<SequentialAdapter<CombanaDTO>> {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
    IConfigurationDAO configurationDAO;

	@Inject
	public CodificaEspertoCombanaAdapterProvider(IConfigurationDAO configurationDAO){
		this.configurationDAO=configurationDAO;
	}	

	@Override
	public SequentialAdapter<CombanaDTO> get() {
		SequentialAdapter<CombanaDTO> result = new SequentialAdapter<CombanaDTO>();
		List<Configuration> configurations = configurationDAO.getConfiguration("codifica", "view-info");
		if (!configurations.isEmpty()) {
			Configuration configuration = configurations.get(0);
			Configuration maturatoConf = configuration.getInnerConfiguration("maturato");
			Configuration rischioConf = configuration.getInnerConfiguration("rischio");
			Configuration statoOperaConf = configuration.getInnerConfiguration("stato-opera");
			if (maturatoConf != null) {
				result.addModifier(decimalFieldModifier(maturatoConf, "maturato", "maturato.show", "maturato.thresholdset"));
			}
			if (rischioConf != null) {
				result.addModifier(decimalFieldModifier(rischioConf, "rischio", "rischio.show", "rischio.thresholdset"));
			}
			if(statoOperaConf!= null) {
				result.addModifier(onOffFieldModifier(statoOperaConf, "statoOpera", "stato-opera.show", null));
			}
		}
		return result;
	}
	
	protected Modifier<CombanaDTO> decimalFieldModifier(Configuration configuration, final String field, String mainSettingKey, String secondarySettingKey) {
		KeyValueProperty mainSetting = configuration.getSetting(mainSettingKey, KeyValueProperty.class);
		final ThresholdSet secondarySetting = configuration.getSetting(secondarySettingKey, ThresholdSet.class);
		if (mainSetting!=null && mainSetting.isTrue()) {
			if (secondarySetting!=null && secondarySetting.isTrue()) {
				return new Modifier<CombanaDTO>() {
					@Override
					public void apply(CombanaDTO combana) {
						try {
							String property = BeanUtils.getProperty(combana, field);
							if (property != null) {
								double propertyValue = Double.parseDouble(property);
								List<Setting> settings = secondarySetting.getThresholds();
								for (Setting setting : settings) {
									if (setting.getClass() == Threshold.class) {
										Threshold threshold = (Threshold) setting;
										double tvalue = Double.parseDouble(threshold.getValue());
										if (propertyValue < tvalue && threshold.getRelation() == Relation.LT) {
											BeanUtils.setProperty(combana, field, threshold.getCaption());
											break;
										}
										if (propertyValue >= tvalue && (threshold.getRelation() == Relation.GT || threshold.getRelation() == Relation.GTE)) {
											BeanUtils.setProperty(combana, field, threshold.getCaption());
										}
									}
								}
							}
						} catch (Exception e) {
							logger.warn(e.getMessage(), e);
						}
					}

					@Override
					public List<String> getApplicationFields() {
						return Arrays.asList(field);
					}
				};				
				
			}
			return new Modifier<CombanaDTO>() {

				@Override
				public List<String> getApplicationFields() {
					return Arrays.asList(field);
				}
			};
		}
		return new Modifier<CombanaDTO>() {

			@Override
			public void apply(CombanaDTO combana) {
				try {
					BeanUtils.setProperty(combana, field, null);
				} catch (Exception e) {
					logger.warn(e.getMessage(), e);
				}
			}

			@Override
			public List<String> getApplicationFields() {
				return Arrays.asList();
			}
		};
	}
	
	protected Modifier<CombanaDTO> onOffFieldModifier(Configuration configuration, final String field, String mainSettingKey, String secondarySettingKey) {
		KeyValueProperty mainSetting = configuration.getSetting(mainSettingKey, KeyValueProperty.class);
		if (mainSetting!=null && mainSetting.isTrue()) {
			return new Modifier<CombanaDTO>() {
				@Override
				public List<String> getApplicationFields() {
					return Arrays.asList(field);
				}
			};			
		}
		return new Modifier<CombanaDTO>() {

			@Override
			public void apply(CombanaDTO combana) {
				try {
					BeanUtils.setProperty(combana, field, null);
				} catch (Exception e) {
					logger.warn(e.getMessage(), e);
				}
			}

			@Override
			public List<String> getApplicationFields() {
				return Arrays.asList();
			}
		};
	}	

}