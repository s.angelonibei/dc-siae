package com.alkemytech.sophia.codman.dto;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

@XmlRootElement
public class DspDropdownFilterDTO implements Serializable {
    private List<String> idDsp;
    private List<String> idUtilizationType;

    public List<String> getIdDsp() {
        return idDsp;
    }

    public void setIdDsp(List<String> idDsp) {
        this.idDsp = idDsp;
    }

    public List<String> getIdUtilizationType() {
        return idUtilizationType;
    }

    public void setIdUtilizationType(List<String> idUtilizationType) {
        this.idUtilizationType = idUtilizationType;
    }
}
