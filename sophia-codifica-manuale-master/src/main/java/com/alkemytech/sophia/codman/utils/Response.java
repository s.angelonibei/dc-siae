package com.alkemytech.sophia.codman.utils;

public interface Response {
    String MULTIMEDIALE_LOCALE_LICENZA_MANDATORY   = "Licenza obbligatoria";
    String MULTIMEDIALE_LOCALE_CODICE_LICENZIATARIO_MANDATORY   = "Licenziatario obbligatorio";
    String MULTIMEDIALE_LOCALE_SERVIZIO_MANDATORY   = "Servizio obbligatorio";
    String MULTIMEDIALE_LOCALE_PERIODO_COMPETENZA_MANDATORY   = "Periodo competenza obbligatorio";
    String MULTIMEDIALE_LOCALE_MOD_UTILIZZAZIONE_MANDATORY   = "Modalita' utilizzazione obbligatoria";
    String MULTIMEDIALE_LOCALE_MOD_FRUIZIONE_MANDATORY   = "Modalita' fruizione obbligatoria";
    String MULTIMEDIALE_LOCALE_MOD_FRUIZIONE_NOT_VALID   = "Modalita' fruizione non valida";
    String MULTIMEDIALE_LOCALE_ABBONAMENTO_MANDATORY   = "Flag abbonamento obbligatorio";
    String MULTIMEDIALE_LOCALE_POSIZIONE_REPORT_MANDATORY   = "Posizione report obbligatoria";
    String MULTIMEDIALE_LOCALE_IDENTIFICATIVO_REPORT_MANDATORY   = "Identificativo report obbligatorio";
    String MULTIMEDIALE_LOCALE_PROGRESSIVO_REPORT_MANDATORY   = "Progressivo report obbligatorio";
    String MULTIMEDIALE_LOCALE_STATO_MANDATORY   = "Stato obbligatorio";
    String MULTIMEDIALE_LOCALE_FORMATO_PERIODO_COMPETENZA_NOT_VALID   = "Formato periodo non valido";
    String MULTIMEDIALE_LOCALE_IDENTIFICATIVO_REPORT_ALREADY_EXIST   = "Identificativo report gia' presente";
    String MULTIMEDIALE_LOCALE_REPORT_ALREADY_EXIST   = "Report gia' presente";
    String MULTIMEDIALE_LOCALE_STATO_CARICAMENTO = "CARICATO";
    String MULTIMEDIALE_LOCALE_SERVIZIO_NOT_EXIST = "Servizio non esistente";
    String MULTIMEDIALE_LOCALE_MOD_UTILIZZAZIONE_NOT_EXIST = "Modalita' utilizzazione non esistente";
    String MULTIMEDIALE_LOCALE_REPORT_NOT_EXIST   = "Report non esistente";

    String MULTILMEDIALE_STATISTCHE_STORICO_NOT_EXIST = "Non esiste uno storico per il DSR scelto";

    String PERFORMING_CODIFICA_PERIODO_VALIDITA = "Periodo di validità del codice configurazione ";
    String PERFORMING_CODIFICA_IN_CORSO = " in corso";
    String PERFORMING_CODIFICA_CONFLITTO_DATE = "Conflitto nelle date del codice configurazione ";
    String PERFORMING_CODIFICA_VALIDAZIONE_FILE = "Problemi nella validazione del file";
    String PERFORMING_CODIFICA_ERRORE_CARICAMENTO_FILE_GENERICO = "Non è stato possibile caricare il file";
}
