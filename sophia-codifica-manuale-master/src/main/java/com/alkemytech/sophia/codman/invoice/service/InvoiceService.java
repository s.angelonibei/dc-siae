package com.alkemytech.sophia.codman.invoice.service;

import com.alkemytech.sophia.codman.dto.AnagClientDTO;
import com.alkemytech.sophia.codman.dto.InvoiceCCIDDTO;
import com.alkemytech.sophia.codman.dto.InvoiceDTO;
import com.alkemytech.sophia.codman.dto.InvoiceDraftDto;
import com.alkemytech.sophia.codman.dto.InvoiceItemDetailDTO;
import com.alkemytech.sophia.codman.dto.InvoiceItemDto;
import com.alkemytech.sophia.codman.dto.InvoiceItemReasonDTO;
import com.alkemytech.sophia.codman.entity.AnagClient;
import com.alkemytech.sophia.codman.entity.AnagDsp;
import com.alkemytech.sophia.codman.entity.CCIDMetadata;
import com.alkemytech.sophia.codman.entity.CurrencyRate;
import com.alkemytech.sophia.codman.entity.performing.security.ReportPage;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.alkemytech.sophia.codman.invoice.repository.AvailableCCIDSearchParameters;
import com.alkemytech.sophia.codman.invoice.repository.Invoice;
import com.alkemytech.sophia.codman.invoice.repository.InvoiceConfiguration;
import com.alkemytech.sophia.codman.invoice.repository.InvoiceItem;
import com.alkemytech.sophia.codman.invoice.repository.InvoiceItemReason;
import com.alkemytech.sophia.codman.invoice.repository.InvoiceItemToCcid;
import com.alkemytech.sophia.codman.invoice.repository.InvoiceSearchParameters;
import com.alkemytech.sophia.codman.invoice.repository.UpdateCcidParameter;
import com.alkemytech.sophia.codman.utils.Periods;
import com.alkemytech.sophia.codman.utils.QueryUtils;
import com.google.common.primitives.Bytes;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import groovy.json.StringEscapeUtils;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URI;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.alkemytech.sophia.invoice.integration.model.Configuration;
import com.alkemytech.sophia.invoice.integration.model.DatiFattura;
import com.alkemytech.sophia.invoice.integration.model.DatiStampa;
import com.alkemytech.sophia.invoice.integration.model.InvoiceOrder;
import com.alkemytech.sophia.invoice.integration.model.InvoiceResponse;
import com.alkemytech.sophia.invoice.integration.model.Posizione;
import com.alkemytech.sophia.invoice.integration.service.InvoiceServiceClientInterface;
import com.alkemytech.sophia.invoice.integration.service.SapPiInvoiceServiceClientImpl;

@Singleton
public class InvoiceService implements InvoiceServiceInterface {

	private final Provider<EntityManager> provider;
	private InvoiceConfiguration invoiceConfig;
	private InvoiceServiceClientInterface sapPiInvoiceServiceClientImpl;
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final Properties configuration;
	
	@Inject
	protected InvoiceService(@McmdbDataSource Provider<EntityManager> provider, @Named("configuration") Properties configuration) {
		super();
		this.provider = provider;
		sapPiInvoiceServiceClientImpl = new SapPiInvoiceServiceClientImpl();
//		sapPiInvoiceServiceClientImpl = new SapPiInvoiceClientStub();
		this.configuration = configuration;

		Yaml yaml = new Yaml();
		try {
			InputStream in = getClass().getResourceAsStream("/yaml/invoiceConfig.yml");
			invoiceConfig = yaml.loadAs(in, InvoiceConfiguration.class);

		} catch (Exception e) {
			logger.error("invoice yaml configuration ", e);
		}
		Configuration invoiceServiceConfiguration = new Configuration();
		invoiceServiceConfiguration.setEsbPath(configuration.getProperty("invoice.esbPath"));
		invoiceServiceConfiguration.setHttps(Boolean.parseBoolean(configuration.getProperty("invoice.https")));
		invoiceServiceConfiguration.setCertificatePath(configuration.getProperty("invoice.certificatePath"));
		invoiceServiceConfiguration.setTrustStorePwd(configuration.getProperty("invoice.trustStorePwd"));
		sapPiInvoiceServiceClientImpl.init(invoiceServiceConfiguration, new InvoiceTracer(provider));
	}

	@Override
	public InvoiceDTO newInvoice(InvoiceDraftDto dto) {

		EntityManager entityManager = null;
		List<Object[]> result;
		List<InvoiceCCIDDTO> dtoList = new ArrayList<>();
		InvoiceDTO invoiceDto = null;
		try {

			entityManager = provider.get();
			entityManager.getTransaction().begin();
			Invoice invoice = new Invoice();
			Date date = new Date();
			invoice.setCreationDate(date);
			invoice.setClientId(dto.getClientId());
			invoice.setDateOfPertinence(dto.getDateOfpertinence());
			invoice.setLastUpdateDate(date);
			invoice.setIdDsp(dto.getIdDsp());
			invoice.setNote(dto.getAnnotation());
			invoice.setTotal(dto.getTotal());
			invoice.setStatus("BOZZA");
			invoice.setInvoiceType("AUTOMATICA");
			int position = 1;

			for (InvoiceItemDto itemDao : dto.getItemList()) {
				InvoiceItem itemEntity = new InvoiceItem();
				InvoiceItemReason description=(InvoiceItemReason) entityManager.createQuery("SELECT x FROM InvoiceItemReason x WHERE x.idItemInvoiceReason =:id").setParameter("id", itemDao.getIdDescription()).getSingleResult();
				itemEntity.setDescription(description);

				itemEntity.setNote(itemDao.getNote());
				itemEntity.setInvoicePosition(position);

				itemEntity.setTotal(itemDao.getTotalValue());
				itemEntity.setTotalOrigCurrency(itemDao.getTotalValueOrig());
				itemEntity.setOrigCurrency(itemDao.getCurrency());
				itemEntity.setInvoice(invoice);
				invoice.addInvoiceItem(itemEntity);
				entityManager.persist(invoice);
				entityManager.persist(itemEntity);
				entityManager.flush();

				List<CCIDMetadata> ccids= entityManager.createQuery("SELECT x FROM CCIDMetadata x WHERE x.idCcid IN :idCcids").setParameter("idCcids", itemDao.getCcidList()).getResultList();
				for (CCIDMetadata ccidMedata: ccids) {
					InvoiceItemToCcid invoiceItemToCcid= new InvoiceItemToCcid();
					invoiceItemToCcid.setCcidMedata(ccidMedata);
					invoiceItemToCcid.setInvoiceItem(itemEntity);
					invoiceItemToCcid.setValore(ccidMedata.getValoreResiduo());
					entityManager.persist(invoiceItemToCcid);
					ccidMedata.setQuotaFattura(new BigDecimal("0"));
					ccidMedata.setValoreResiduo(new BigDecimal(0));
					entityManager.merge(ccidMedata);
					entityManager.flush();
				}
				position++;

			}


			entityManager.getTransaction().commit();

			invoiceDto = new InvoiceDTO();
			invoiceDto.setIdInvoice(invoice.getIdInvoice());

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("create invoice", e);
			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
		}

		return invoiceDto;
	}

	@Override
	public List<InvoiceDTO> searchInvoice(InvoiceSearchParameters searchParameters) {
		EntityManager entityManager = null;
		List<InvoiceDTO> resultSet = new ArrayList<>();
		boolean checkDsp = searchParameters.getDspList() != null && !searchParameters.getDspList().isEmpty();
		boolean checkStatus = searchParameters.getStatusList() != null && !searchParameters.getStatusList().isEmpty();
		boolean checkInvoiceCode = searchParameters.getInvoiceCode() != null && !searchParameters.getInvoiceCode().isEmpty();
		boolean checkDateFrom = searchParameters.getDateFrom() != null; 
		boolean checkDateTo = searchParameters.getDateTo() != null;

		try {
			entityManager = provider.get();

			String queryString0 = "select x from InvoiceItemReason x ";
			Query query0 = entityManager.createQuery(queryString0);
			List<InvoiceItemReason> reasonResultList = (List<InvoiceItemReason>) query0.getResultList();

			Map<Integer, InvoiceItemReason> invoiceReasonMap = new HashMap<>();
			for (InvoiceItemReason reason : reasonResultList) {
				invoiceReasonMap.put(reason.getIdItemInvoiceReason(), reason);
			}

			String queryString = "" + "select x, d,c from Invoice x, AnagDsp d, AnagClient c "
					+ "where x.idDsp = d.idDsp " + "and x.clientId = c.idAnagClient and x.invoiceType<>'ANTICIPO'"
					+ (checkDsp
							? " and x.idDsp in (" + QueryUtils.getValuesCollection(searchParameters.getDspList()) + ") "
							: "")
					+ (checkStatus
							? " and x.status in (" + QueryUtils.getValuesCollection(searchParameters.getStatusList()) + ")"
							: "")
					+ (checkInvoiceCode
							? " and x.invoiceCode = " + searchParameters.getInvoiceCode() + " "
							: "")

					+ (checkDateFrom ? " and x.creationDate >= :dateFrom " : "")
					+ (checkDateTo ? " and x.creationDate <= :dateTo" : "");
			

			int first = (searchParameters.getFirst() > 0 ? searchParameters.getFirst()-1 : 0);
			int last = (searchParameters.getLast() > 0 ? searchParameters.getLast() : 50);
			
			final Query query = entityManager.createQuery(queryString).setFirstResult(first)
					.setMaxResults(ReportPage.NUMBER_RECORD_PER_PAGE);

			if (checkDateFrom) {
				query.setParameter("dateFrom", searchParameters.getDateFrom());
			}
			if (checkDateTo) {
				query.setParameter("dateTo", searchParameters.getDateTo());
			}
			
			List<Object[]> result = query.getResultList();
			for (Object[] o : result) {
				Invoice invoice = (Invoice) o[0];
				AnagDsp dsp = (AnagDsp) o[1];
				AnagClient clientData = (AnagClient) o[2];
				resultSet.add(new InvoiceDTO(invoice, dsp, clientData, invoiceReasonMap));
			}

		} catch (Exception e) {
			logger.error("search invoice", e);
		}
		return resultSet;
	}

	@Override
	public InvoiceDTO confirmInvoice(InvoiceDTO dto) {
		return null;
	}
	
	@Override
	public void removeInvoice(Integer idInvoice) {
		EntityManager entityManager = null;
		try {
			entityManager = provider.get();
			
			entityManager.getTransaction().begin();
			Invoice invoice = (Invoice) entityManager.find(Invoice.class, idInvoice);

			for (InvoiceItem invoiceItem : invoice.getInvoiceItems()) {
				List<InvoiceItemToCcid> invoiceItemToCcids=entityManager.createQuery("Select x from InvoiceItemToCcid x where x.invoiceItem.idItem = :invoiceItem").setParameter("invoiceItem", invoiceItem.getIdItem()).getResultList();
				for(InvoiceItemToCcid invoiceItemToCcid:invoiceItemToCcids) {
					CCIDMetadata metadata =invoiceItemToCcid.getIdCcidMedata();
					metadata.setInvoiceStatus("DA_FATTURARE");
					metadata.setInvoiceItem(null);
					metadata.setValoreResiduo(invoiceItemToCcid.getValore());
					metadata.setQuotaFattura(new BigDecimal("0"));
					entityManager.merge(metadata);
				}
				entityManager.createQuery("delete from InvoiceItemToCcid x where x.invoiceItem.idItem = :invoiceItem").setParameter("invoiceItem", invoiceItem.getIdItem()).executeUpdate();
				entityManager.remove(invoiceItem);
			}
			
			entityManager.createQuery("delete from InvoiceLog x where x.idInvoice = :idInvoice").setParameter("idInvoice", idInvoice).executeUpdate();
			entityManager.remove(invoice);

			entityManager.getTransaction().commit();

		} catch (Exception e) {
			logger.error("delete", e);
			throw (e);
		}
		
	}

	@Override
	public void removeInvoiceBozza(Integer idInvoice) {

		EntityManager entityManager = null;
		try {
			entityManager = provider.get();
			
			entityManager.getTransaction().begin();
			Invoice invoice = (Invoice) entityManager.find(Invoice.class, idInvoice);
			for (InvoiceItem invoiceItem : invoice.getInvoiceItems()) {
				List<InvoiceItemToCcid> invoiceItemToCcids=entityManager.createQuery("Select x from InvoiceItemToCcid x where x.invoiceItem.idItem = :invoiceItem").setParameter("invoiceItem", invoiceItem.getIdItem()).getResultList();
				for(InvoiceItemToCcid invoiceItemToCcid:invoiceItemToCcids) {
					CCIDMetadata metadata =invoiceItemToCcid.getIdCcidMedata();
					metadata.setInvoiceStatus("DA_FATTURARE");
					metadata.setInvoiceItem(null);
					metadata.setValoreResiduo(invoiceItemToCcid.getValore());
					metadata.setQuotaFattura(new BigDecimal("0"));
					entityManager.merge(metadata);
				}
				entityManager.createQuery("delete from InvoiceItemToCcid x where x.invoiceItem.idItem = :invoiceItem").setParameter("invoiceItem", invoiceItem.getIdItem()).executeUpdate();
				entityManager.remove(invoiceItem);
			}
			
			entityManager.createQuery("delete from InvoiceLog x where x.idInvoice = :idInvoice").setParameter("idInvoice", idInvoice).executeUpdate();
			entityManager.remove(invoice);

			entityManager.getTransaction().commit();

		} catch (Exception e) {
			logger.error("delete", e);
			throw (e);
		}

	}

	@Override
	public InvoiceDTO updateInvoice(InvoiceDTO dto) {
		return null;
	}

	@Override
	public List<InvoiceCCIDDTO> availableCCID(AvailableCCIDSearchParameters dto) {
		boolean checkPeriod = false;
		Map<Integer, List<Integer>> quarters = new HashMap<>();
		String inCondition = "";
		Integer monthFrom = 0;
		Integer yearFrom = 2000;
		Date date = new Date();
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		Integer monthTo = calendar.get(Calendar.MONTH);;
		Integer yearTo = calendar.get(Calendar.YEAR);;
		
		if (dto.getMonthFrom() > 0 || dto.getMonthTo() > 0) {
			checkPeriod = true;
			
			if(dto.getMonthFrom() > 0){
				monthFrom = dto.getMonthFrom();  
				yearFrom = dto.getYearFrom();
			}
			
			if(dto.getMonthTo() > 0){
				monthTo = dto.getMonthTo(); 
				yearTo = dto.getYearTo();
				
			}
			quarters = Periods.extractCorrespondingQuarters(monthFrom, yearFrom, monthTo, yearTo);

			int y = 0;
			for (int key : quarters.keySet()) {
				if (y++ != 0) {
					inCondition += " or ";
				}
				inCondition += " (M1.PERIOD_TYPE='quarter' and M1.PERIOD in ("
						+ StringUtils.join(quarters.get(key), ',') + ") and M1.YEAR = " + key + " )";

			}
			if (!inCondition.isEmpty()) {
				inCondition = " ( " + inCondition + " )";
			}

		}

		EntityManager entityManager = null;
		List<Object[]> result;
		List<InvoiceCCIDDTO> dtoList = new ArrayList<>();
		try {

			entityManager = provider.get();

			String sql = "select " + "MD.ID_CCID , MD.ID_DSR, MD.TOTAL_VALUE, MD.ID_CCID_METADATA, MD.CURRENCY, "
					+ "MD.CCID_VERSION , MD.CCID_ENCODED, MD.CCID_ENCODED_PROVISIONAL, MD.CCID_NOT_ENCODED, MD.INVOICE_STATUS, "
					+ "D.IDDSP , D.NAME, U.ID_UTILIZATION_TYPE, U.NAME, C.ID_COMMERCIAL_OFFERS, C.OFFERING, "
					+ "M1.COUNTRY, M1.PERIOD_TYPE, M1.PERIOD, " + "(CASE " + "WHEN M1.PERIOD_TYPE = 'month' THEN "
					+ "concat(ELT(M1.PERIOD, 'Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov','Dic') , ' ', M1.YEAR) "
					+ "ELSE concat(ELT(M1.PERIOD, 'Gen - Mar', 'Apr - Giu', 'Lug - Set', 'Ott - Dic') , ' ', M1.YEAR)       END) AS PERIOD_STRING, "
					+ "M1.YEAR, MD.VALORE_FATTURABILE " + ",MD.VALORE_RESIDUO "  + "from " + "DSR_METADATA M1," + "CCID_METADATA MD, " + "COMMERCIAL_OFFERS C, "
					+ "ANAG_UTILIZATION_TYPE U, " + "ANAG_DSP D " + "where MD.INVOICE_STATUS='DA_FATTURARE' "
					+ "and MD.ID_DSR = M1.IDDSR " + "and M1.SERVICE_CODE = C.ID_COMMERCIAL_OFFERS "
					+ "and C.ID_UTIILIZATION_TYPE = U.ID_UTILIZATION_TYPE " + "and C.IDDSP = D.IDDSP "
					+ (!dto.getDsp().isEmpty() ? "and C.IDDSP = '" + dto.getDsp() + "'" : "")
					+ (dto.getCountryList() != null && !dto.getCountryList().isEmpty()
							? "and M1.COUNTRY in ( " + QueryUtils.getValuesCollection(dto.getCountryList()) + " )" : "")
					+ (dto.getUtilizationList() != null && !dto.getUtilizationList().isEmpty()
							? "and C.ID_UTIILIZATION_TYPE in ( "
									+ QueryUtils.getValuesCollection(dto.getUtilizationList()) + " )"
							: "")
					+ (dto.getOfferList() != null && !dto.getOfferList().isEmpty()
							? "and C.OFFERING in ( " + QueryUtils.getValuesCollection(dto.getOfferList()) + " )" : "")
					+ (!inCondition.isEmpty() ? "and (" + inCondition + " or " : "")
					+ (checkPeriod && inCondition.isEmpty() ? " and " : "")
					+ (checkPeriod
							? " (PERIOD_TYPE='month' and  STR_TO_DATE(CONCAT(M1.YEAR,'-', M1.PERIOD , '-01'), '%Y-%m-%d') >= '"
									+ yearFrom + "-" + monthFrom
									+ "-01' and STR_TO_DATE(CONCAT(M1.YEAR,'-', M1.PERIOD , '-01'), '%Y-%m-%d') <= '"
									+ yearTo + "-" + monthTo + "-01' ) "
							: "")
					+ (checkPeriod && !quarters.isEmpty() ? ")" : "");


			int first = (dto.getFirst() > 0 ? dto.getFirst()-1 : 0);
			int last = (dto.getLast() > 0 ? dto.getLast() : 50);

			final Query q = entityManager.createNativeQuery(sql).setFirstResult(first) // offset
					.setMaxResults(ReportPage.NUMBER_RECORD_PER_PAGE);

			result = (List<Object[]>) q.getResultList();
			if (null != result) {
//				final int maxrows = last - first;
//				final boolean hasNext = result.size() > maxrows;
//				for (Object[] p : (hasNext ? result.subList(0, maxrows) : result)) {
//					dtoList.add(new InvoiceCCIDDTO(p));
//				}

					for (Object[] p : result) {
					dtoList.add(new InvoiceCCIDDTO(p));
				}
			}

			Map<String, BigDecimal> currencyRateMap = new HashMap<>();

			for (InvoiceCCIDDTO item : dtoList) {
				if (item.getCurrency().equals("EUR")) {
					item.setTotalValueOrigCurrency(item.getTotalValue());
				} else {
					final Calendar now = GregorianCalendar.getInstance();
					BigDecimal originalValue = item.getTotalValue();
					String month = item.getPeriodType() != null && item.getPeriod() != null
							? getPeriodRepresentation(item.getPeriodType(), item.getPeriod())
							: String.format("%02d", 1 + now.get(Calendar.MONTH));
					String year = item.getYear() != null ? item.getYear().toString()
							: String.format("%04d", now.get(Calendar.YEAR));
					String srcCurrency = item.getCurrency();
					String dstCurrency = "EUR";
					BigDecimal rate;
					String key = year + month + srcCurrency + dstCurrency;
					if (currencyRateMap.containsKey(key)) {

						rate = currencyRateMap.get(key);
					} else {

						final CurrencyRate currencyRate = entityManager
								.createQuery(
										new StringBuffer().append("select x from CurrencyRate x")
												.append(" where x.year = :year").append(" and x.month = :month")
												.append(" and x.srcCurrency = :srcCurrency")
												.append(" and x.dstCurrency = :dstCurrency").toString(),
										CurrencyRate.class)
								.setParameter("year", year).setParameter("month", month)
								.setParameter("srcCurrency", srcCurrency).setParameter("dstCurrency", dstCurrency)
								.getSingleResult();

						if (null != currencyRate) {
							rate = currencyRate.getRate();

						} else {
							rate = new BigDecimal(0);
						}
						currencyRateMap.put(key, currencyRate.getRate());
					}

					BigDecimal eurValue = originalValue.multiply(rate);
					item.setTotalValue(eurValue);
					item.setTotalValueOrigCurrency(originalValue);
				}

			}

		} catch (Exception e) {
			// TODO: handle exception
			logger.error("invoice available dsr", e);
		}
		return dtoList;
	}
	
	@Override
	public List<InvoiceCCIDDTO> payableCCID(AvailableCCIDSearchParameters dto) {
		boolean checkPeriod = false;
		Map<Integer, List<Integer>> quarters = new HashMap<>();
		String inCondition = "";
		Integer monthFrom = 0;
		Integer yearFrom = 2000;
		Date date = new Date();
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		Integer monthTo = calendar.get(Calendar.MONTH);;
		Integer yearTo = calendar.get(Calendar.YEAR);;
		
		if (dto.getMonthFrom() > 0 || dto.getMonthTo() > 0) {
			checkPeriod = true;
			
			if(dto.getMonthFrom() > 0){
				monthFrom = dto.getMonthFrom();  
				yearFrom = dto.getYearFrom();
			}
			
			if(dto.getMonthTo() > 0){
				monthTo = dto.getMonthTo(); 
				yearTo = dto.getYearTo();
				
			}
			quarters = Periods.extractCorrespondingQuarters(monthFrom, yearFrom, monthTo, yearTo);

			int y = 0;
			for (int key : quarters.keySet()) {
				if (y++ != 0) {
					inCondition += " or ";
				}
				inCondition += " (M1.PERIOD_TYPE='quarter' and M1.PERIOD in ("
						+ StringUtils.join(quarters.get(key), ',') + ") and M1.YEAR = " + key + " )";

			}
			if (!inCondition.isEmpty()) {
				inCondition = " ( " + inCondition + " )";
			}

		}

		EntityManager entityManager = null;
		List<Object[]> result;
		List<InvoiceCCIDDTO> dtoList = new ArrayList<>();
		try {

			entityManager = provider.get();

			String sql = "select " + "MD.ID_CCID , MD.ID_DSR, MD.TOTAL_VALUE, MD.ID_CCID_METADATA, MD.CURRENCY, "
					+ "MD.CCID_VERSION , MD.CCID_ENCODED, MD.CCID_ENCODED_PROVISIONAL, MD.CCID_NOT_ENCODED, MD.INVOICE_STATUS, "
					+ "D.IDDSP , D.NAME, U.ID_UTILIZATION_TYPE, U.NAME, C.ID_COMMERCIAL_OFFERS, C.OFFERING, "
					+ "M1.COUNTRY, M1.PERIOD_TYPE, M1.PERIOD, " + "(CASE " + "WHEN M1.PERIOD_TYPE = 'month' THEN "
					+ "concat(ELT(M1.PERIOD, 'Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov','Dic') , ' ', M1.YEAR) "
					+ "ELSE concat(ELT(M1.PERIOD, 'Gen - Mar', 'Apr - Giu', 'Lug - Set', 'Ott - Dic') , ' ', M1.YEAR)       END) AS PERIOD_STRING, "
					+ "M1.YEAR, MD.VALORE_FATTURABILE " + ",MD.VALORE_RESIDUO "  + "from " + "DSR_METADATA M1," + "CCID_METADATA MD, " + "COMMERCIAL_OFFERS C, "
					+ "ANAG_UTILIZATION_TYPE U, " + "ANAG_DSP D " + "where MD.INVOICE_STATUS='DA_FATTURARE' "
					+ "and MD.VALORE_RESIDUO > 0 " + "and MD.ID_DSR = M1.IDDSR " + "and M1.SERVICE_CODE = C.ID_COMMERCIAL_OFFERS "
					+ "and C.ID_UTIILIZATION_TYPE = U.ID_UTILIZATION_TYPE " + "and C.IDDSP = D.IDDSP "
					+ (!dto.getDsp().isEmpty() ? "and C.IDDSP = '" + dto.getDsp() + "'" : "")
					+ (dto.getCountryList() != null && !dto.getCountryList().isEmpty()
							? "and M1.COUNTRY in ( " + QueryUtils.getValuesCollection(dto.getCountryList()) + " )" : "")
					+ (dto.getUtilizationList() != null && !dto.getUtilizationList().isEmpty()
							? "and C.ID_UTIILIZATION_TYPE in ( "
									+ QueryUtils.getValuesCollection(dto.getUtilizationList()) + " )"
							: "")
					+ (dto.getOfferList() != null && !dto.getOfferList().isEmpty()
							? "and C.OFFERING in ( " + QueryUtils.getValuesCollection(dto.getOfferList()) + " )" : "")
					+ (!inCondition.isEmpty() ? "and (" + inCondition + " or " : "")
					+ (checkPeriod && inCondition.isEmpty() ? " and " : "")
					+ (checkPeriod
							? " (PERIOD_TYPE='month' and  STR_TO_DATE(CONCAT(M1.YEAR,'-', M1.PERIOD , '-01'), '%Y-%m-%d') >= '"
									+ yearFrom + "-" + monthFrom
									+ "-01' and STR_TO_DATE(CONCAT(M1.YEAR,'-', M1.PERIOD , '-01'), '%Y-%m-%d') <= '"
									+ yearTo + "-" + monthTo + "-01' ) "
							: "")
					+ (checkPeriod && !quarters.isEmpty() ? ")" : "");

			int first = (dto.getFirst() > 0 ? dto.getFirst()-1 : 0);
			int last = (dto.getLast() > 0 ? dto.getLast() : 50);
			final Query q = entityManager.createNativeQuery(sql).setFirstResult(first) // offset
					.setMaxResults(ReportPage.NUMBER_RECORD_PER_PAGE);


			result = (List<Object[]>) q.getResultList();
			if (null != result) {
//				final int maxrows = last - first;
//				final boolean hasNext = result.size() > maxrows;
//				for (Object[] p : (hasNext ? result.subList(0, maxrows) : result)) {
//					dtoList.add(new InvoiceCCIDDTO(p));
//				}

					for (Object[] p : result) {
					dtoList.add(new InvoiceCCIDDTO(p));
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			logger.error("invoice available dsr", e);
		}
		return dtoList;
	}

	private String getPeriodRepresentation(String periodType, Integer period) {
		if (periodType.equals("quarter")) {
			return "Q" + period.toString();
		}
		return String.format("%02d", period);
	}

	public InvoiceConfiguration getConfiguration() {
		return invoiceConfig;
	}

	@Override
	public List<InvoiceItemReasonDTO> getInvoiceItemReasons() {
		EntityManager entityManager = null;
		List<InvoiceItemReason> result;
		List<InvoiceItemReasonDTO> dtoList = new ArrayList<>();
		try {
			entityManager = provider.get();
			final Query q = entityManager.createQuery("select x from InvoiceItemReason x");
			result = (List<InvoiceItemReason>) q.getResultList();
			if (result != null && !result.isEmpty()) {
				for (InvoiceItemReason e : result) {
					dtoList.add(new InvoiceItemReasonDTO(e));
				}
			}

		} catch (Exception e) {
			logger.error("list invoice item reason", e);
		}
		return dtoList;
	}

	@Override
	public InvoiceResponse callInvoiceService(int invoiceId) {

		EntityManager entityManager = null;
		List<InvoiceItemReason> resultList;
		InvoiceResponse invoiceResponse = null;

		try {
			entityManager = provider.get();
			String queryString = "select x from InvoiceItemReason x ";
			Query query = entityManager.createQuery(queryString);

			resultList = (List<InvoiceItemReason>) query.getResultList();

			String sqlStringUniqueCodeUpdate = "UPDATE INVOICE_REQUEST_ID SET REQUEST_ID = LAST_INSERT_ID(REQUEST_ID + 1)";
			String sqlStringGetUniqueCode = "SELECT LAST_INSERT_ID()";

			BigInteger requestId;
			entityManager.getTransaction().begin();
			Query queryUpdate = entityManager.createNativeQuery(sqlStringUniqueCodeUpdate);
			queryUpdate.executeUpdate();
			Query queryGetUniqueCode = entityManager.createNativeQuery(sqlStringGetUniqueCode);
			requestId = (BigInteger) queryGetUniqueCode.getSingleResult();
			entityManager.getTransaction().commit();

			Map<Integer, InvoiceItemReason> invoiceReasonMap = new HashMap<>();
			for (InvoiceItemReason reason : resultList) {
				invoiceReasonMap.put(reason.getIdItemInvoiceReason(), reason);
			}

			Invoice invoice = (Invoice) entityManager.find(Invoice.class, invoiceId);

			List<InvoiceItem> items = invoice.getInvoiceItems();

			AnagClient anagClient = (AnagClient) entityManager.find(AnagClient.class, invoice.getClientId());

			if (resultList != null) {

				SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
				Date date = new Date();
				DatiFattura datiFattura = new DatiFattura();
				datiFattura.setIdRichiesta(requestId.toString());
				datiFattura.setCodiceCliente(anagClient.getCode());
				
				datiFattura.setDataCompetenza(DATE_FORMAT.format(invoice.getDateOfPertinence()));
				datiFattura.setDataDocumento(DATE_FORMAT.format(date));
				datiFattura.setDataAccredito(DATE_FORMAT.format(date));
				datiFattura.setDataPagamento(DATE_FORMAT.format(date));
				datiFattura.setDivisione("DGEN");
		        datiFattura.setTipoDocumento("3"); // Il flusso di fatturazione sarà previsto solo per le fatturazione anticipata e fatturazione 
		        								//con acconto per soggetti estero (va gestito come input o dipende unicamente dal cliente estero?)
		        datiFattura.setAttribuzione("MULCL n. ord. d\'acquisto");
		        datiFattura.setIncoterms2("C001");
//		        datiFattura.setTestoTestata(invoice.getNote());

				List<Posizione> positions = new ArrayList<>();
				for (InvoiceItem item : items) {

					Posizione position = new Posizione();
					position.setCodiceMateriale(item.getDescription().getCode());
					position.setCodiceIVASAP(anagClient.getVatCode()); 

					
					
					position.setNumeroPosizione(item.getInvoicePosition().toString());
					position.setQuantita("1");
					position.setPrezzoUnitario(item.getTotal().toString());
					position.setDescrizioneMateriale(item.getDescription().getDescription());
					positions.add(position);

				}

				datiFattura.setPosizioni(positions);

				
				InvoiceOrder order = new InvoiceOrder();
				DatiStampa datiStampa = new DatiStampa();
			
				datiStampa.setAnnotazioni(StringEscapeUtils.escapeJava(invoice.getNote()));
				
				order.setDatiFattura(datiFattura);
				order.setDatiStampa(datiStampa);
				
				
				invoiceResponse = sapPiInvoiceServiceClientImpl.sendInvoiceOrder(order, requestId.toString(),
						invoiceId);


				if (invoiceResponse != null) {

					CriteriaBuilder cb = entityManager.getCriteriaBuilder();
					entityManager.getTransaction().begin();
					invoice.setStatus("RICHIESTA_FATTURA");
					invoice.setPdfPath(invoiceResponse.getPathFilePdf());
					invoice.setInvoiceCode(invoiceResponse.getNumeroDocumento());
					invoice.setLastUpdateDate(new Date());


					for (InvoiceItem i : invoice.getInvoiceItems()) {
						
							// create update
							CriteriaUpdate<CCIDMetadata> update = cb.createCriteriaUpdate(CCIDMetadata.class);

							// set the root class
							Root e = update.from(CCIDMetadata.class);

							// set update and where clause
							update.set("invoiceStatus", "RICHIESTA_FATTURA");
							Expression<String> idItem = e.get("invoiceItem");
							update.where(cb.equal(idItem, i.getIdItem()));
							entityManager.createQuery(update).executeUpdate();

						

					}
					entityManager.getTransaction().commit();
				}

			}

		} catch (Exception e) {
			logger.error("call invoice service", e);
		}

		return invoiceResponse;

	}

	@Override
	public List<InvoiceItemDto> getInvoiceItems(Integer invoiceId) {
		EntityManager entityManager = null;
		List<Object[]> result;
		List<InvoiceItemDto> dtoList = new ArrayList<>();
		try {
			entityManager = provider.get();

			String sqlString = "SELECT \n" +
					"INVOICE_ITEM.ID_ITEM, \n" +
					"ITEM_INVOICE_REASON.DESCRIPTION, \n" +
					"INVOICE_ITEM.TOTAL, \n" +
					"INVOICE_ITEM_TO_CCID.VALORE,\n" +
					"INVOICE_ITEM.TOTAL_ORIG_CURRENCY, \n" +
					"INVOICE_ITEM.ORIG_CURRENCY, \n" +
					"DSR_METADATA.PERIOD, \n" +
					"DSR_METADATA.PERIOD_TYPE, \n" +
					"DSR_METADATA.YEAR, \n" +
					"ANAG_DSP.NAME, \n" +
					"(CASE   WHEN DSR_METADATA.PERIOD_TYPE = 'month' THEN concat(ELT(DSR_METADATA.PERIOD, 'Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov','Dic') , ' ', DSR_METADATA.YEAR) \n" +
					"ELSE concat(ELT(DSR_METADATA.PERIOD, 'Gen - Mar', 'Apr - Giu', 'Lug - Set', 'Ott - Dic') , ' ', DSR_METADATA.YEAR) END) AS PERIOD_STRING, \n" +
					"ANAG_COUNTRY.NAME, \n" +
					"ANAG_UTILIZATION_TYPE.NAME, \n" +
					"COMMERCIAL_OFFERS.OFFERING, \n" +
					"CCID_METADATA.VALORE_RESIDUO , \n" +
					"CCID_METADATA.CURRENCY\n" +
					"FROM INVOICE_ITEM_TO_CCID\n" +
					"\n" +
					"LEFT JOIN INVOICE_ITEM\n" +
					"ON INVOICE_ITEM_TO_CCID.ID_INVOICE_ITEM = INVOICE_ITEM.ID_ITEM\n" +
					"\n" +
					"LEFT JOIN ITEM_INVOICE_REASON\n" +
					"ON INVOICE_ITEM.DESCRIPTION = ITEM_INVOICE_REASON.ID_ITEM_INVOICE_REASON\n" +
					"\n" +
					"LEFT JOIN CCID_METADATA\n" +
					"ON INVOICE_ITEM_TO_CCID.ID_CCID_METADATA = CCID_METADATA.ID_CCID_METADATA\n" +
					"\n" +
					"LEFT JOIN DSR_METADATA\n" +
					"ON CCID_METADATA.ID_DSR = DSR_METADATA.IDDSR\n" +
					"\n" +
					"LEFT JOIN COMMERCIAL_OFFERS\n" +
					"ON DSR_METADATA.SERVICE_CODE = COMMERCIAL_OFFERS.ID_COMMERCIAL_OFFERS\n" +
					"\n" +
					"LEFT JOIN ANAG_DSP\n" +
					"ON DSR_METADATA.IDDSP = ANAG_DSP.IDDSP\n" +
					"\n" +
					"LEFT JOIN ANAG_COUNTRY\n" +
					"ON DSR_METADATA.COUNTRY = ANAG_COUNTRY.ID_COUNTRY\n" +
					"\n" +
					"LEFT JOIN ANAG_UTILIZATION_TYPE\n" +
					"ON COMMERCIAL_OFFERS.ID_UTIILIZATION_TYPE = ANAG_UTILIZATION_TYPE.ID_UTILIZATION_TYPE\n" +
					"\n" +
					"WHERE INVOICE_ITEM.ID_INVOICE = " + invoiceId.toString();

			final Query q = entityManager.createNativeQuery(sqlString);
			result = (List<Object[]>) q.getResultList();
			if (result != null && !result.isEmpty()) {
				Map<Integer, InvoiceItemDto> itemMap = new HashMap<>();
				for (Object[] element : result) {
					InvoiceItemDetailDTO itemDetail = new InvoiceItemDetailDTO(element);
					Integer itemId = (Integer) element[0];
					Integer period = (Integer) element[6];
					String periodType = (String) element[7];
					Integer year = (Integer) element[8];
					Map<String, BigDecimal> currencyRateMap = new HashMap<>();

					if (itemDetail.getCurrency().equals("EUR")) {
						itemDetail.setTotalValueOrigCurrency(itemDetail.getTotalValue());
					} else {
						BigDecimal originalValue = itemDetail.getTotalValue();

						Map<String, String> currencyKeys = getCurrencyKeyMap(itemDetail.getCurrency(), periodType,
								period, year);
						updateCurrencyMap(currencyRateMap, currencyKeys, entityManager);

						BigDecimal eurValue = originalValue.multiply(currencyRateMap.get(currencyKeys.get("key")));
						itemDetail.setTotalValue(eurValue);
						itemDetail.setTotalValueOrigCurrency(originalValue);
					}

					if (!itemMap.containsKey(itemId)) {
						InvoiceItemDto invoiceItem = new InvoiceItemDto();
						invoiceItem.setDescriptionText((String) element[1]);
						invoiceItem.setTotalValue((BigDecimal) element[14]);
						invoiceItem.setTotalValueOrig((BigDecimal) element[3]);
						invoiceItem.setCurrency((String) element[5]);
						List<InvoiceItemDetailDTO> invoiceDetailList = new ArrayList<>();
						invoiceDetailList.add(itemDetail);
						invoiceItem.setInvoiceDetailList(invoiceDetailList);
						itemMap.put(itemId, invoiceItem);
						dtoList.add(invoiceItem);
					} else {
						itemMap.get(itemId).getInvoiceDetailList().add(itemDetail);
						itemMap.get(itemId).setTotalValue(itemMap.get(itemId).getTotalValue().add((BigDecimal) element[14]));
						itemMap.get(itemId).setTotalValueOrig(itemMap.get(itemId).getTotalValueOrig().add((BigDecimal) element[3]));
					}

				}
			}

		} catch (Exception e) {
			logger.error("list invoice item", e);
		}
		return dtoList;
	}

	private void updateCurrencyMap(Map<String, BigDecimal> currencyRateMap, Map<String, String> currencyKeys,
			EntityManager em) {
		try {

			BigDecimal rate;

			if (currencyRateMap.containsKey(currencyKeys.get("key"))) {

				rate = currencyRateMap.get(currencyKeys.get("key"));
			} else {

				final CurrencyRate currencyRate = em
						.createQuery(new StringBuffer().append("select x from CurrencyRate x")
								.append(" where x.year = :year").append(" and x.month = :month")
								.append(" and x.srcCurrency = :srcCurrency").append(" and x.dstCurrency = :dstCurrency")
								.toString(), CurrencyRate.class)
						.setParameter("year", currencyKeys.get("yearString"))
						.setParameter("month", currencyKeys.get("month"))
						.setParameter("srcCurrency", currencyKeys.get("srcCurrency"))
						.setParameter("dstCurrency", currencyKeys.get("dstCurrency")).getSingleResult();

				if (null != currencyRate) {
					rate = currencyRate.getRate();

				} else {
					rate = new BigDecimal(0);
				}
				currencyRateMap.put(currencyKeys.get("key"), currencyRate.getRate());
			}
		} catch (Exception e) {
			logger.error("invoice currency map", e);
		}
	}

	private Map<String, String> getCurrencyKeyMap(String currency, String periodType, Integer period, Integer year) {

		final Calendar now = GregorianCalendar.getInstance();
		String month = periodType != null && period != null ? getPeriodRepresentation(periodType, period)
				: String.format("%02d", 1 + now.get(Calendar.MONTH));
		String yearString = year != null ? year.toString() : String.format("%04d", now.get(Calendar.YEAR));
		String srcCurrency = currency;
		String dstCurrency = "EUR";
		String key = yearString + month + srcCurrency + dstCurrency;
		HashMap<String, String> values = new HashMap<>();
		values.put("month", month);
		values.put("yearString", yearString);
		values.put("srcCurrency", srcCurrency);
		values.put("dstCurrency", dstCurrency);
		values.put("key", key);
		return values;
	}

	@Override
	public AnagClientDTO getAnagClient(String dsp) {

		AnagClientDTO clientData = null;
		EntityManager entityManager = null;
		AnagClient result;
		try {
			entityManager = provider.get();
			String queryString = "" + "select x from AnagClient x " + "where x.idDsp = :dsp and x.endDate is null";
			final Query q = entityManager.createQuery(queryString);
			q.setParameter("dsp", dsp);
			result = (AnagClient) q.getSingleResult();
			if (result != null) {
				clientData = new AnagClientDTO(result);

			}

		} catch (Exception e) {
			logger.error("client data", e);
		}

		return clientData;
	}

	@Override
	public InvoiceDTO getInvoice(Integer invoiceId) {
		EntityManager entityManager = null;
		InvoiceDTO result = new InvoiceDTO();
		try {
			entityManager = provider.get();
			Invoice invoice = (Invoice) entityManager.find(Invoice.class, invoiceId);
			AnagDsp anagDsp = (AnagDsp) entityManager.find(AnagDsp.class, invoice.getIdDsp());
			AnagClient anagClient = (AnagClient) entityManager.find(AnagClient.class, invoice.getClientId());

			String queryString = "select x from InvoiceItemReason x ";
			Query query = entityManager.createQuery(queryString);

			List<InvoiceItemReason> resultList = (List<InvoiceItemReason>) query.getResultList();

			Map<Integer, InvoiceItemReason> invoiceReasonMap = new HashMap<>();
			for (InvoiceItemReason reason : resultList) {
				invoiceReasonMap.put(reason.getIdItemInvoiceReason(), reason);
			}

			if (invoice != null && anagDsp != null && anagClient != null && !invoiceReasonMap.isEmpty()) {
				result = new InvoiceDTO(invoice, anagDsp, anagClient, invoiceReasonMap);

			}

		} catch (Exception e) {
			logger.error("client data", e);
		}

		return result;
	}

	@Override
	public InvoiceDTO checkInvoice(Integer invoiceId) {
		EntityManager entityManager = null;
		InvoiceDTO result = new InvoiceDTO();
		try {
			entityManager = provider.get();
			Invoice invoice = (Invoice) entityManager.find(Invoice.class, invoiceId);

			String sqlStringUniqueCodeUpdate = "UPDATE INVOICE_REQUEST_ID SET REQUEST_ID = LAST_INSERT_ID(REQUEST_ID + 1)";
			String sqlStringGetUniqueCode = "SELECT LAST_INSERT_ID()";

			BigInteger requestId;
			entityManager.getTransaction().begin();
			Query queryUpdate = entityManager.createNativeQuery(sqlStringUniqueCodeUpdate);
			queryUpdate.executeUpdate();
			Query queryGetUniqueCode = entityManager.createNativeQuery(sqlStringGetUniqueCode);
			requestId = (BigInteger) queryGetUniqueCode.getSingleResult();
			entityManager.getTransaction().commit();
			
			String pdfPath = invoice.getPdfPath();
			String pdf = pdfPath !=  null? pdfPath.substring(pdfPath.lastIndexOf("\\")+1, pdfPath.length()) : null;
			List<Byte> pdfByteString = sapPiInvoiceServiceClientImpl.getInvoice(pdf,
					requestId.toString(), invoiceId);
			if (pdfByteString != null && !pdfByteString.isEmpty()) {

				CriteriaBuilder cb = entityManager.getCriteriaBuilder();
				entityManager.getTransaction().begin();
				invoice.setStatus("FATTURATO");
				invoice.setPdf(Bytes.toArray(pdfByteString));
				invoice.setLastUpdateDate(new Date());
				for (InvoiceItem i : invoice.getInvoiceItems()) {
					
						// create update
						CriteriaUpdate<CCIDMetadata> update = cb.createCriteriaUpdate(CCIDMetadata.class);

						// set the root class
						Root e = update.from(CCIDMetadata.class);

						// set update and where clause
						update.set("invoiceStatus", "FATTURATO");
						Expression<String> itemId = e.get("invoiceItem");
						update.where(cb.equal(itemId, i.getIdItem().toString()));
						entityManager.createQuery(update).executeUpdate();

					

				}
				entityManager.getTransaction().commit();
			}

			AnagDsp anagDsp = (AnagDsp) entityManager.find(AnagDsp.class, invoice.getIdDsp());
			AnagClient anagClient = (AnagClient) entityManager.find(AnagClient.class, invoice.getClientId());

			String queryString = "select x from InvoiceItemReason x ";
			Query query = entityManager.createQuery(queryString);

			List<InvoiceItemReason> resultList = (List<InvoiceItemReason>) query.getResultList();

			Map<Integer, InvoiceItemReason> invoiceReasonMap = new HashMap<>();
			for (InvoiceItemReason reason : resultList) {
				invoiceReasonMap.put(reason.getIdItemInvoiceReason(), reason);
			}

			if (invoice != null && anagDsp != null && anagClient != null && !invoiceReasonMap.isEmpty()) {
				result = new InvoiceDTO(invoice, anagDsp, anagClient, invoiceReasonMap);

			}

		} catch (Exception e) {
			logger.error("client data", e);
		}

		return result;
	}

	@Override
	public byte[] downloadInvoice(Integer invoiceId) {
		final EntityManager entityManager = provider.get();
		Invoice invoice = (Invoice) entityManager.find(Invoice.class, invoiceId);
		if (invoice != null) {
			return invoice.getPdf();
		}
		return null;
	}


	@Override
	public boolean updateCCid(UpdateCcidParameter dto) {
	
		EntityManager entityManager = null;
		entityManager = provider.get();
		
		try {
			CCIDMetadata entity = entityManager.getReference(CCIDMetadata.class, new BigInteger(dto.getIdCCIDMetadata()));
			entity.setValoreFatturabile(new BigDecimal(dto.getInvoiceAmaunt()));
			entity.setValoreResiduo(new BigDecimal( dto.getInvoiceAmaunt()));
			entityManager.getTransaction().begin();
			entityManager.merge( entity );
			entityManager.getTransaction().commit();
			//return Response.ok( "").build();
			return true;
		}catch(Exception e) {
			return false;
		}
	}

	@Override
	public CCIDMetadata getCcidMetadata(UpdateCcidParameter dto) {
		EntityManager entityManager = provider.get();
		return entityManager.find(CCIDMetadata.class,new BigInteger( dto.getIdCCIDMetadata() ) );
	}

	@Override
	public List<InvoiceItemToCcid> getInvoiceItemToCcid(CCIDMetadata ccidMetadata) {
		EntityManager entityManager = provider.get();
		List<InvoiceItemToCcid> ccids= entityManager.createQuery("Select x from InvoiceItemToCcid x where x.ccidMedata.idCCIDMetadata=:idCCIDMetadata").setParameter("idCCIDMetadata", ccidMetadata.getIdCCIDMetadata()).getResultList();
		return ccids;

	}




}
