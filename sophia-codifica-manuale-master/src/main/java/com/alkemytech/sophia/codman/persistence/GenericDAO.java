package com.alkemytech.sophia.codman.persistence;

import java.util.List;

import javax.persistence.EntityManager;

public interface GenericDAO<E extends AbstractEntity<?>, PK> {

	 List<E> findAll();
	 PK insert(E entity);
	 E find(PK id);
	 void update(E entity);
	 void delete(E entity);
	 
	 Class<E> getEntityClass();
	 EntityManager getEntityManager();

}
