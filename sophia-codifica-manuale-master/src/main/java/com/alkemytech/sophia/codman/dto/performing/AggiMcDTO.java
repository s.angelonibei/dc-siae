package com.alkemytech.sophia.codman.dto.performing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class AggiMcDTO {
	
	private Long id;
	private String inizioPeriodoValidazione;
	private String finePeriodoValidazione;
	private String soglia;
	private String aggioSottosoglia;
	private String aggioSoprasoglia;
	private String valoreCap;
	private boolean flagCapMultiplo;
	private String utenteCreazione;
	private String codiceRegola;
	private String tipoUltimaModifica;
	private String utenteUltimaModifica;
	private boolean flagAttivo;
	
	public AggiMcDTO() {
		super();
	}

	public AggiMcDTO(String inizioPeriodoValidazione, String finePeriodoValidazione, String soglia,
			String aggioSottosoglia, String aggioSoprasoglia, String valoreCap, boolean flagCapMultiplo,
			String utenteCreazione) {
		super();
		this.inizioPeriodoValidazione = inizioPeriodoValidazione;
		this.finePeriodoValidazione = finePeriodoValidazione;
		this.soglia = soglia;
		this.aggioSottosoglia = aggioSottosoglia;
		this.aggioSoprasoglia = aggioSoprasoglia;
		this.valoreCap = valoreCap;
		this.flagCapMultiplo = flagCapMultiplo;
		this.utenteCreazione = utenteCreazione;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getInizioPeriodoValidazione() {
		return inizioPeriodoValidazione;
	}
	public void setInizioPeriodoValidazione(String inizioPeriodoValidazione) {
		this.inizioPeriodoValidazione = inizioPeriodoValidazione;
	}
	public String getFinePeriodoValidazione() {
		return finePeriodoValidazione;
	}
	public void setFinePeriodoValidazione(String finePeriodoValidazione) {
		this.finePeriodoValidazione = finePeriodoValidazione;
	}
	public String getSoglia() {
		return soglia;
	}

	public void setSoglia(String soglia) {
		this.soglia = soglia;
	}

	public String getAggioSottosoglia() {
		return aggioSottosoglia;
	}

	public void setAggioSottosoglia(String aggioSottosoglia) {
		this.aggioSottosoglia = aggioSottosoglia;
	}

	public String getAggioSoprasoglia() {
		return aggioSoprasoglia;
	}

	public void setAggioSoprasoglia(String aggioSoprasoglia) {
		this.aggioSoprasoglia = aggioSoprasoglia;
	}

	public String getValoreCap() {
		return valoreCap;
	}

	public void setValoreCap(String valoreCap) {
		this.valoreCap = valoreCap;
	}

	public boolean getFlagCapMultiplo() {
		return flagCapMultiplo;
	}

	public void setFlagCapMultiplo(boolean flagCapMultiplo) {
		this.flagCapMultiplo = flagCapMultiplo;
	}

	public String getUtenteCreazione() {
		return utenteCreazione;
	}

	public void setUtenteCreazione(String utenteCreazione) {
		this.utenteCreazione = utenteCreazione;
	}

	public String getCodiceRegola() {
		return codiceRegola;
	}

	public void setCodiceRegola(String codiceRegola) {
		this.codiceRegola = codiceRegola;
	}

	public String getTipoUltimaModifica() {
		return tipoUltimaModifica;
	}

	public void setTipoUltimaModifica(String tipoUltimaModifica) {
		this.tipoUltimaModifica = tipoUltimaModifica;
	}

	public String getUtenteUltimaModifica() {
		return utenteUltimaModifica;
	}

	public void setUtenteUltimaModifica(String utenteUltimaModifica) {
		this.utenteUltimaModifica = utenteUltimaModifica;
	}

	public boolean getFlagAttivo() {
		return flagAttivo;
	}

	public void setFlagAttivo(boolean flagAttivo) {
		this.flagAttivo = flagAttivo;
	}

	@Override
	public String toString() {
		return "AggiMcDTO [inizioPeriodoValidazione=" + inizioPeriodoValidazione + ", finePeriodoValidazione="
				+ finePeriodoValidazione + ", soglia=" + soglia + ", aggioSottosoglia=" + aggioSottosoglia
				+ ", aggioSoprasoglia=" + aggioSoprasoglia + ", valoreCap=" + valoreCap + ", flagCapMultiplo="
				+ flagCapMultiplo + ", utenteCreazione=" + utenteCreazione + ", codiceRegola=" + codiceRegola
				+ ", tipoUltimaModifica=" + tipoUltimaModifica + ", utenteUltimaModifica=" + utenteUltimaModifica
				+ ", flagAttivo=" + flagAttivo + "]";
	}

	
}
