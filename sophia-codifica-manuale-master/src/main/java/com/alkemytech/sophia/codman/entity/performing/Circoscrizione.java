package com.alkemytech.sophia.codman.entity.performing;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by idilello on 10/17/16.
 */
@Entity
@Table(name = "PERF_CIRCOSCRIZIONE")
public class Circoscrizione implements Serializable {

    private Long id;
    private String denominazione;
    private String localita;

    @Id
    @Column(name = "COD_SEPRAG", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "DENOMINAZIONE")
    public String getDenominazione() {
        return denominazione;
    }

    public void setDenominazione(String denominazione) {
        this.denominazione = denominazione;
    }

    @Column(name = "LOCALITA")
    public String getLocalita() {
        return localita;
    }

    public void setLocalita(String localita) {
        this.localita = localita;
    }

}
