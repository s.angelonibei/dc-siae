package com.alkemytech.sophia.codman.dto;

import com.alkemytech.sophia.codman.invoice.repository.InvoiceItemReason;

public class InvoiceItemReasonDTO {

	private Integer idItemInvoiceReason;
	private String code;
	private String description;
	private String contoSap;
	
	public Integer getIdItemInvoiceReason() {
		return idItemInvoiceReason;
	}
	public InvoiceItemReasonDTO(){
		
	}
	
public InvoiceItemReasonDTO(InvoiceItemReason entity){
		this.idItemInvoiceReason = new Integer(entity.getIdItemInvoiceReason().intValue());
		this.code = entity.getCode();
		this.description = entity.getDescription();
		this.contoSap = entity.getContoSap();
}
	public void setIdItemInvoiceReason(Integer idItemInvoiceReason) {
		this.idItemInvoiceReason = idItemInvoiceReason;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getContoSap() {
		return contoSap;
	}
	public void setContoSap(String contoSap) {
		this.contoSap = contoSap;
	}
}
