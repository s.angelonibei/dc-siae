package com.alkemytech.sophia.codman.dto;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Data
@XmlRootElement
public class MonitorDsrDTO implements Serializable {

    private Integer idConfig;

    @SerializedName("idDsp")
    private String idDsp;

    @SerializedName("territorio")
    private String territorio;

    @SerializedName("tipoUtilizzo")
    private String tipoUtilizzo;

    @SerializedName("offertaCommerciale")
    private String offertaCommerciale;

    @SerializedName("frequenzaDsr")
    private String frequenzaDsr;

    @SerializedName("slaInvioDsr")
    private Integer slaInvioDsr;

    @SerializedName("slaInvioCcid")
    private Integer slaInvioCcid;

    @SerializedName("dsrAttesi")
    private Integer dsrAttesi;
}
