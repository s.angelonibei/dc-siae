package com.alkemytech.sophia.codman.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PerfEventiPagatiDTO {
	private String meseInizioPeriodoContabile;
	private String annoInizioPeriodoContabile;
	private String meseFinePeriodoContabile;
	private String annoFinePeriodoContabile;
	private Long evento;
	private String fattura;
	private String reversale;
	private String voceIncasso;
	private String dataInizioEvento;
	private String oraInizioEvento;
	private String seprag;
	private String tipoDocumento;
	private String locale;
	private String codiceBA;
	private String presenzaMovimenti;
	private Integer page;
	
	public PerfEventiPagatiDTO() {
		super();
	}
	
	public PerfEventiPagatiDTO(String meseInizioPeriodoContabile, String annoInizioPeriodoContabile,
			String meseFinePeriodoContabile, String annoFinePeriodoContabile, Long evento, String fattura,
			String reversale, String voceIncasso, String dataInizioEvento, String oraInizioEvento, String seprag,
			String tipoDocumento, String locale, String codiceBA, String presenzaMovimenti, Integer page) {
		super();
		this.meseInizioPeriodoContabile = meseInizioPeriodoContabile;
		this.annoInizioPeriodoContabile = annoInizioPeriodoContabile;
		this.meseFinePeriodoContabile = meseFinePeriodoContabile;
		this.annoFinePeriodoContabile = annoFinePeriodoContabile;
		this.evento = evento;
		this.fattura = fattura;
		this.reversale = reversale;
		this.voceIncasso = voceIncasso;
		this.dataInizioEvento = dataInizioEvento;
		this.oraInizioEvento = oraInizioEvento;
		this.seprag = seprag;
		this.tipoDocumento = tipoDocumento;
		this.locale = locale;
		this.codiceBA = codiceBA;
		this.presenzaMovimenti = presenzaMovimenti;
		this.page = page;
	}

	public String getMeseInizioPeriodoContabile() {
		return meseInizioPeriodoContabile;
	}

	public void setMeseInizioPeriodoContabile(String meseInizioPeriodoContabile) {
		this.meseInizioPeriodoContabile = meseInizioPeriodoContabile;
	}

	public String getAnnoInizioPeriodoContabile() {
		return annoInizioPeriodoContabile;
	}

	public void setAnnoInizioPeriodoContabile(String annoInizioPeriodoContabile) {
		this.annoInizioPeriodoContabile = annoInizioPeriodoContabile;
	}

	public String getMeseFinePeriodoContabile() {
		return meseFinePeriodoContabile;
	}

	public void setMeseFinePeriodoContabile(String meseFinePeriodoContabile) {
		this.meseFinePeriodoContabile = meseFinePeriodoContabile;
	}

	public String getAnnoFinePeriodoContabile() {
		return annoFinePeriodoContabile;
	}

	public void setAnnoFinePeriodoContabile(String annoFinePeriodoContabile) {
		this.annoFinePeriodoContabile = annoFinePeriodoContabile;
	}

	public Long getEvento() {
		return evento;
	}

	public void setEvento(Long evento) {
		this.evento = evento;
	}

	public String getFattura() {
		return fattura;
	}

	public void setFattura(String fattura) {
		this.fattura = fattura;
	}

	public String getReversale() {
		return reversale;
	}

	public void setReversale(String reversale) {
		this.reversale = reversale;
	}

	public String getVoceIncasso() {
		return voceIncasso;
	}

	public void setVoceIncasso(String voceIncasso) {
		this.voceIncasso = voceIncasso;
	}

	public String getDataInizioEvento() {
		return dataInizioEvento;
	}

	public void setDataInizioEvento(String dataInizioEvento) {
		this.dataInizioEvento = dataInizioEvento;
	}

	public String getOraInizioEvento() {
		return oraInizioEvento;
	}

	public void setOraInizioEvento(String oraInizioEvento) {
		this.oraInizioEvento = oraInizioEvento;
	}

	public String getSeprag() {
		return seprag;
	}

	public void setSeprag(String seprag) {
		this.seprag = seprag;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getCodiceBA() {
		return codiceBA;
	}

	public void setCodiceBA(String codiceBA) {
		this.codiceBA = codiceBA;
	}

	public String getPresenzaMovimenti() {
		return presenzaMovimenti;
	}

	public void setPresenzaMovimenti(String presenzaMovimenti) {
		this.presenzaMovimenti = presenzaMovimenti;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}
	
}
