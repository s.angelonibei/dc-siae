package com.alkemytech.sophia.codman.entity;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.codman.persistence.AbstractEntity;

@XmlRootElement
@Entity(name="ImportiAnticipo")
@Table(name="IMPORTI_ANTICIPO")
public class ImportiAnticipo extends AbstractEntity<String>{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
    @Id
    @Column(name = "ID_IMPORTO_ANTICIPO", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idImportoAnticipo;
    
    @Column(name = "PERIODO_PERTINENZA_INIZIO", nullable = false)
	private Date periodoPertinenzaInizio;
    
    @Column(name = "PERIODO_PERTINENZA_FINE", nullable = false)
	private Date periodoPertinenzaFine;
    
    @Column(name = "NUMERO_FATTURA", nullable = false)
	private String numeroFattura;
   
    @Column(name = "ID_INVOICE", nullable = false)
	private Integer idInvoice;
    
    @Column(name = "IMPORTO_ORIGINALE", nullable = false)
	private BigDecimal importoOriginale;
    
    @Column(name = "IMPORTO_UTILIZZABILE", nullable = false)
	private BigDecimal importoUtilizzabile;
    
    @Column(name = "IDDSP", nullable = false)
	private String idDsp;

	public Integer getIdImportoAnticipo() {
		return idImportoAnticipo;
	}

	public void setIdImportoAnticipo(Integer idImportoAnticipo) {
		this.idImportoAnticipo = idImportoAnticipo;
	}

	public Date getPeriodoPertinenzaInizio() {
		return periodoPertinenzaInizio;
	}

	public void setPeriodoPertinenzaInizio(Date periodoPertinenzaInizio) {
		this.periodoPertinenzaInizio = periodoPertinenzaInizio;
	}

	public Date getPeriodoPertinenzaFine() {
		return periodoPertinenzaFine;
	}

	public void setPeriodoPertinenzaFine(Date periodoPertinenzaFine) {
		this.periodoPertinenzaFine = periodoPertinenzaFine;
	}

	public String getNumeroFattura() {
		return numeroFattura;
	}

	public void setNumeroFattura(String numeroFattura) {
		this.numeroFattura = numeroFattura;
	}

	public BigDecimal getImportoOriginale() {
		return importoOriginale;
	}

	public void setImportoOriginale(BigDecimal importoOriginale) {
		this.importoOriginale = importoOriginale;
	}

	public BigDecimal getImportoUtilizzabile() {
		return importoUtilizzabile;
	}

	public void setImportoUtilizzabile(BigDecimal importoUtilizzabile) {
		this.importoUtilizzabile = importoUtilizzabile;
	}

	public String getIdDsp() {
		return idDsp;
	}

	public void setIdDsp(String idDsp) {
		this.idDsp = idDsp;
	}

	public Integer getIdInvoice() {
		return idInvoice;
	}

	public void setIdInvoice(Integer idInvoice) {
		this.idInvoice = idInvoice;
	}

	@Override
	public String getId() {
		return idImportoAnticipo.toString();
	}
    
    
    

}
