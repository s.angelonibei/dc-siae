package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;

@Produces("application/json")
@XmlRootElement
public class DettaglioDspStatisticsDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String invoiceStatus;
	private BigDecimal totaleQuoteAnticipo;
	private List<InfoCcidStatisticsDTO> listAnticipo;
	private String invoiceCode;
	private Boolean foundAnticipi;
	private BigDecimal totaleFatturato;
	

	public String getInvoiceStatus() {
		return invoiceStatus;
	}
	public void setInvoiceStatus(String invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}
	public BigDecimal getTotaleQuoteAnticipo() {
		return totaleQuoteAnticipo;
	}
	public void setTotaleQuoteAnticipo(BigDecimal totaleQuoteAnticipo) {
		this.totaleQuoteAnticipo = totaleQuoteAnticipo;
	}
	public List<InfoCcidStatisticsDTO> getListAnticipo() {
		return listAnticipo;
	}
	public void setListAnticipo(List<InfoCcidStatisticsDTO> listAnticipo) {
		this.listAnticipo = listAnticipo;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getInvoiceCode() {
		return invoiceCode;
	}
	public void setInvoiceCode(String invoiceCode) {
		this.invoiceCode = invoiceCode;
	}
	public Boolean getFoundAnticipi() {
		return foundAnticipi;
	}
	public void setFoundAnticipi(Boolean foundAnticipi) {
		this.foundAnticipi = foundAnticipi;
	}
	public BigDecimal getTotaleFatturato() {
		return totaleFatturato;
	}
	public void setTotaleFatturato(BigDecimal totaleFatturato) {
		this.totaleFatturato = totaleFatturato;
	}
	
	

}
