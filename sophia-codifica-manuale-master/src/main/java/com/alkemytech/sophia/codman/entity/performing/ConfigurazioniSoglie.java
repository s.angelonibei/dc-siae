package com.alkemytech.sophia.codman.entity.performing;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "PERF_CONFIGURAZIONI_SOGLIE")
public class ConfigurazioniSoglie implements Serializable{

	private Long id;
	private Long codiceConfigurazione;
	private Date inizioPeriodoCompetenza;
	private Date finePeriodoCompetenza;
	private String utenteCaricamentoFile;
	private Date dataCaricamentoFile;
	private String utenteUltimaModifica;
	private Date dataUltimaModifica;
	private String pathFile;
	private String nomeOriginaleFile;
	private boolean flagAttivo;
	
	
	@Id
    @Column(name = "ID_CONFIGURAZIONE_SOGLIE", unique = true, nullable = false)
	@SequenceGenerator(name="PERF_CONFIGURAZIONI_SOGLIE",sequenceName = "PERF_CONFIGURAZIONI_SOGLIE_SEQ",allocationSize=1)
	@GeneratedValue(strategy= GenerationType.SEQUENCE, generator="PERF_CONFIGURAZIONI_SOGLIE")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "CODICE_CONFIGURAZIONE")
	public Long getCodiceConfigurazione() {
		return codiceConfigurazione;
	}
	public void setCodiceConfigurazione(Long codiceConfigurazione) {
		this.codiceConfigurazione = codiceConfigurazione;
	}
	
	@Column(name = "INIZIO_PERIODO_COMPETENZA")
	public Date getInizioPeriodoCompetenza() {
		return inizioPeriodoCompetenza;
	}
	public void setInizioPeriodoCompetenza(Date inizioPeriodoCompetenza) {
		this.inizioPeriodoCompetenza = inizioPeriodoCompetenza;
	}
	
	@Column(name = "FINE_PERIODO_COMPETENZA")
	public Date getFinePeriodoCompetenza() {
		return finePeriodoCompetenza;
	}
	public void setFinePeriodoCompetenza(Date finePeriodoCompetenza) {
		this.finePeriodoCompetenza = finePeriodoCompetenza;
	}
	
	@Column(name = "DATA_CARICAMENTO_FILE")
	public Date getDataCaricamentoFile() {
		return dataCaricamentoFile;
	}
	public void setDataCaricamentoFile(Date dataCaricamentoFile) {
		this.dataCaricamentoFile = dataCaricamentoFile;
	}
	
	@Column(name = "DATA_ULTIMA_MODIFICA")
	public Date getDataUltimaModifica() {
		return dataUltimaModifica;
	}
	public void setDataUltimaModifica(Date dataUltimaModifica) {
		this.dataUltimaModifica = dataUltimaModifica;
	}
	
	@Column(name = "UTENTE_CARICAMENTO_FILE")
	public String getUtenteCaricamentoFile() {
		return utenteCaricamentoFile;
	}
	public void setUtenteCaricamentoFile(String utenteCaricamentoFile) {
		this.utenteCaricamentoFile = utenteCaricamentoFile;
	}
	
	@Column(name = "UTENTE_ULTIMA_MODIFICA")
	public String getUtenteUltimaModifica() {
		return utenteUltimaModifica;
	}
	public void setUtenteUltimaModifica(String utenteUltimaModifica) {
		this.utenteUltimaModifica = utenteUltimaModifica;
	}
	
	@Column(name = "PATH_FILE")
	public String getPathFile() {
		return pathFile;
	}
	public void setPathFile(String pathFile) {
		this.pathFile = pathFile;
	}
	
	@Column(name = "NOME_ORIGINALE_FILE")
	public String getNomeOriginaleFile() {
		return nomeOriginaleFile;
	}
	public void setNomeOriginaleFile(String nomeOriginaleFile) {
		this.nomeOriginaleFile = nomeOriginaleFile;
	}
	
	@Column(name = "FLAG_ATTIVO")
	public boolean getFlagAttivo() {
		return flagAttivo;
	}
	public void setFlagAttivo(boolean flagAttivo) {
		this.flagAttivo = flagAttivo;
	}
	
}
