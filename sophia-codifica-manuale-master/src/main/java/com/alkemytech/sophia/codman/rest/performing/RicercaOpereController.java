package com.alkemytech.sophia.codman.rest.performing;

import static com.alkemytech.sophia.codman.utils.ConfigurationsUtils.getOverrides;
import static com.alkemytech.sophia.commons.guice.ConfigurationLoader.expandVariables;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.codman.dto.OperaDTO;
import com.alkemytech.sophia.codman.dto.RicercaOpereDTO;
import com.alkemytech.sophia.codman.entity.Configuration;
import com.alkemytech.sophia.codman.entity.ThresholdSet;
import com.alkemytech.sophia.codman.rest.performing.service.IConfigurationService;
import com.alkemytech.sophia.codman.utils.SearchOpereField;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

@Singleton
@Path("ricercaOpere")
public class RicercaOpereController {

	private static Logger LOGGER = LoggerFactory.getLogger(RicercaOpereController.class.getClass());

	private final static String PREFIX = "ricerca.opere.service.prefix";
	private final static String REQUEST_TEMPLATE = "ricerca.opere.service.request.template";
	private final static String SEARCH_URL = "ricerca.opere.service.url";
	private final static String RESULT_SIZE = "ricerca.opere.service.result.sieve.size";

	private Properties configuration;
	private Gson gson;
	private IConfigurationService configurationService;

	@Inject
	public RicercaOpereController(@Named("configuration") Properties configuration, IConfigurationService configurationService, Gson gson) {
		super();
		String prefix = (String) configuration.get(PREFIX);
		this.configuration = new Properties();
		for (Enumeration<?> e = configuration.propertyNames(); e.hasMoreElements();) {
			String key = (String) e.nextElement();
			String value = configuration.getProperty(key);
			if (key.startsWith(prefix)) {
				this.configuration.put(key, value);
			}
		}
		expandVariables(this.configuration);
		this.gson = gson;
		this.configurationService = configurationService;
	}

	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response ricercaOpere(RicercaOpereDTO request) throws IOException {

		List<Configuration> configurations = configurationService.getConfiguration("codifica", "ranking");
		if (CollectionUtils.isEmpty(configurations)) {
			return Response.status(500).entity("Configurazione Ranking Non Trovata!").build();
		}
		Configuration ranking = configurations.get(0);
		Map<String, Object> overrides = new HashMap<String, Object>();
		getOverrides(ranking.getSettings(), overrides);
		
		List<Configuration> configurationsViewInfo = configurationService.getConfiguration("codifica", "view-info");
		if (CollectionUtils.isEmpty(configurationsViewInfo)) {
			return Response.status(500).entity("Configurazione Informazioni Visualizzate Non Trovata!").build();
		}
		Configuration maturato = configurationsViewInfo.get(0).getInnerConfiguration("maturato");
		Configuration rischio = configurationsViewInfo.get(0).getInnerConfiguration("rischio");
		
		getOverrides(maturato.getSetting("maturato.thresholdset", ThresholdSet.class).getThresholds(), overrides);
		getOverrides(rischio.getSetting("rischio.thresholdset", ThresholdSet.class).getThresholds(), overrides);
		
		JSONObject searchOpereRequest;
		try {
			searchOpereRequest = buildRequest(request, overrides);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return Response.status(500).entity(e.getMessage()).build();
		}

		Client client = Client.create();
		String searchUrl = configuration.getProperty(SEARCH_URL);
		LOGGER.info(String.format("Calling to %s", searchUrl));
		LOGGER.info(String.format("Request is: %s", searchOpereRequest.toString()));
		WebResource webResource = client.resource(searchUrl);
		ClientResponse response = webResource.type("application/json").post(ClientResponse.class, searchOpereRequest);
		if (response.getStatus() != 200) {
			LOGGER.error("Service Opere not Available");
			return Response.status(500).build();
		}

		Reader responseReader;
		try {
			responseReader = new InputStreamReader(response.getEntityInputStream(), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			LOGGER.error(e.getMessage(), e);
			return Response.status(500).entity(e.getMessage()).build();
		}
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		IOUtils.copy(responseReader, baos);
		byte[] bytes = baos.toByteArray();
		Type operaListType = new TypeToken<ArrayList<OperaDTO>>() {
		}.getType();
		LOGGER.debug("Response is: {}", new String(bytes));
		ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
		InputStreamReader isr = new InputStreamReader(bais, "UTF-8");
		List<OperaDTO> rawResult = new Gson().fromJson(isr, operaListType);
		Collection<OperaDTO> results = filterResults(rawResult);
		return Response.ok(gson.toJson(results)).build();
	}
	
	protected Collection<OperaDTO> filterResults(List<OperaDTO> rawResult) {
		Integer maxSize = Integer.parseInt(configuration.getProperty(RESULT_SIZE));
		Map<String, OperaDTO> mappedResults = new LinkedHashMap<String, OperaDTO>(maxSize);
		for (Iterator<OperaDTO> iterator = rawResult.iterator(); iterator.hasNext() && mappedResults.size() < maxSize;) {
			OperaDTO opera = (OperaDTO) iterator.next();
			if (!mappedResults.containsKey(opera.getCodiceOpera())) {
				mappedResults.put(opera.getCodiceOpera(), opera);
			}
		}
		Collection<OperaDTO> results = mappedResults.values();
		return results;
	}

	protected JSONObject buildRequest(RicercaOpereDTO request, Map<String, Object> overrides) throws JSONException, IllegalAccessException {
		JSONObject searchOpereRequest = new JSONObject(configuration.getProperty(REQUEST_TEMPLATE));
		Field[] fields = RicercaOpereDTO.class.getDeclaredFields();
		for (Field field : fields) {
			field.setAccessible(true);
			Object value = field.get(request);
			Class<?> type = field.getType();
			SearchOpereField searchOpereField = field.getAnnotation(SearchOpereField.class);
			if (value != null && type == String.class && !((String) value).isEmpty() && searchOpereField != null) {
				String strValue = (String) value;
				String requestField = searchOpereField.value();
				JSONObject texts = searchOpereRequest.getJSONObject("query").getJSONObject("texts");
				texts.put(requestField, Arrays.asList(strValue.split("\\s*,\\s*")));
			}
		}
		JSONObject config = searchOpereRequest.getJSONObject("config");
		JSONObject properties = config.getJSONObject("properties");
		for (Map.Entry<String, Object> entry : overrides.entrySet()) {
			properties.put(entry.getKey(), entry.getValue());
		}
		return searchOpereRequest;
	}

}
