package com.alkemytech.sophia.codman.rest.performing.service;

import com.alkemytech.sophia.codman.dto.performing.MusicProviderDTO;
import com.alkemytech.sophia.codman.dto.performing.PalinsestoDTO;
import com.alkemytech.sophia.codman.dto.performing.RsUtenteDTO;
import com.alkemytech.sophia.performing.model.*;

import java.util.List;

public interface IRadioInStoreService {

	MusicProviderDTO getMusicProviderById(Long id);

	List<MusicProviderDTO> getMusicProviderList();

	List<MusicProviderDTO> getMusicProviderListByName(String nome);

	List<PerfPalinsesto> getPalinsestiList();

	List<PerfPalinsesto> getPalinsestiListByMp(Long id, Long idPalinsesto);

	List<PerfPuntoVendita> getPuntiVenditaByPalinsesto(Long id, Long idPalinsesto);

	List<PerfRsUtente> getUsers(Long idMusicProvider);

	Integer insertMusicProvider(MusicProviderDTO musicProviderDTO);

	Integer insertRsUtente(RsUtenteDTO musicProviderDTO);

	Integer updateRsUtente(RsUtenteDTO musicProviderDTO);

	Integer insertPalinsesto(PalinsestoDTO palinsestoDTO);

	Integer updateMusicProvider(MusicProviderDTO musicProviderDTO);

	Integer updatePalinsesto(PalinsestoDTO palinsestoDTO);

	List<PerfPalinsestoStorico> getStoricoPalinsesto(Long idPalinsesto);

	List<PerfRsUtilizationFile> getUtilizationFile(Long idMusicProvider, Long idPalinsesto, Long anno, List<Long> mesi);

   PerfRsUtilizationFile save(PerfRsUtilizationFile rsUtilizationFile);
}
