package com.alkemytech.sophia.codman.performing.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigInteger;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class FontePrimaUtilizzazioneDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer idFontePrimaUtilizzazione;
	private String descrizione;
	private BigInteger idUtilizzazione;

	public FontePrimaUtilizzazioneDTO() {
	}

	public Integer getIdFontePrimaUtilizzazione() {
		return this.idFontePrimaUtilizzazione;
	}

	public void setIdFontePrimaUtilizzazione(Integer idFontePrimaUtilizzazione) {
		this.idFontePrimaUtilizzazione = idFontePrimaUtilizzazione;
	}

	public String getDescrizione() {
		return this.descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public BigInteger getIdUtilizzazione() {
		return this.idUtilizzazione;
	}

	public void setIdUtilizzazione(BigInteger idUtilizzazione) {
		this.idUtilizzazione = idUtilizzazione;
	}
}