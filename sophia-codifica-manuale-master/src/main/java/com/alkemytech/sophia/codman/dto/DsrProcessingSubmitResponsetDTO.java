package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DsrProcessingSubmitResponsetDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String status;
	
	private String message;

	private List<DsrProcessingDTO> dsrs;

	public List<DsrProcessingDTO> getDsrs() {
		return dsrs;
	}

	public void setDsrs(List<DsrProcessingDTO> dsrs) {
		this.dsrs = dsrs;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
