package com.alkemytech.sophia.codman.dto.performing;

public class CombanaDTO {

    private String autore;
    private String titolo;
    private String interprete;
    private String codiceOpera;
    private String compositore;
    private String gradoDiConf;
    private String confTitolo;
    private String confAutore;
    private String confCompositore;
    private String confInterprete;
    
    private String titoloOriginale;
    private String maturato;
    private String rischio;
    private String statoOpera;
    private String flagPD;
    private String flagEL;
    private String flagDoppioDeposito;

    public CombanaDTO() {
        super();
    }

    public CombanaDTO(String autore, String titolo, String interprete, String codiceOpera, String compositore, String gradoDiConf, String confTitolo, String confAutore, String confCompositore, String confInterprete) {
        this.autore = autore;
        this.titolo = titolo;
        this.interprete = interprete;
        this.codiceOpera = codiceOpera;
        this.compositore = compositore;
        this.gradoDiConf = gradoDiConf;
        this.confTitolo = confTitolo;
        this.confAutore = confAutore;
        this.confCompositore = confCompositore;
        this.confInterprete = confInterprete;
    }

    
    public String getTitoloOriginale() {
		return titoloOriginale;
	}

	public void setTitoloOriginale(String titoloOriginale) {
		this.titoloOriginale = titoloOriginale;
	}
	
    public String getAutore() {
        return autore;
    }

    public void setAutore(String autore) {
        this.autore = autore;
    }

    public String getTitolo() {
        return titolo;
    }

    public void setTitolo(String titolo) {
        this.titolo = titolo;
    }

    public String getInterprete() {
        return interprete;
    }

    public void setInterprete(String interprete) {
        this.interprete = interprete;
    }

    public String getCodiceOpera() {
        return codiceOpera;
    }

    public void setCodiceOpera(String codiceOpera) {
        this.codiceOpera = codiceOpera;
    }

    public String getCompositore() {
        return compositore;
    }

    public void setCompositore(String compositore) {
        this.compositore = compositore;
    }

    public String getGradoDiConf() {
        return gradoDiConf;
    }

    public void setGradoDiConf(String gradoDiConf) {
        this.gradoDiConf = gradoDiConf;
    }
    
    

    public String getConfTitolo() {
		return confTitolo;
	}

	public void setConfTitolo(String confTitolo) {
		this.confTitolo = confTitolo;
	}

	public String getConfAutore() {
		return confAutore;
	}

	public void setConfAutore(String confAutore) {
		this.confAutore = confAutore;
	}

	public String getConfCompositore() {
		return confCompositore;
	}

	public void setConfCompositore(String confCompositore) {
		this.confCompositore = confCompositore;
	}

	public String getConfInterprete() {
		return confInterprete;
	}

	public void setConfInterprete(String confInterprete) {
		this.confInterprete = confInterprete;
	}

	@Override
    public String toString() {
        return "CombanaDTO{" +
                "autore='" + autore + '\'' +
                ", titolo='" + titolo + '\'' +
                ", interprete='" + interprete + '\'' +
                ", codiceOpera='" + codiceOpera + '\'' +
                ", compositore='" + compositore + '\'' +
                ", gradoDiConf='" + gradoDiConf + '\'' +
                ", confTitolo='" + confTitolo + '\'' +
                ", confAutore='" + confAutore + '\'' +
                ", confCompositore='" + confCompositore + '\'' +
                ", confInterprete='" + confInterprete + '\'' +
                ", maturato='" + maturato + '\'' +
                ", rischio='" + rischio + '\'' +
                ", titoloOriginale='" + titoloOriginale + '\'' +
                ", flagPD='" + flagPD + '\'' +
                ", flagEL='" + flagEL + '\'' +
                ", flagDoppioDeposito='" + flagDoppioDeposito + '\'' +
                ", statoOpera='" + statoOpera + '\'' +
                '}';
    }

	public String getMaturato() {
		return maturato;
	}

	public void setMaturato(String maturato) {
		this.maturato = maturato;
	}

	public String getStatoOpera() {
		return statoOpera;
	}

	public void setStatoOpera(String statoOpera) {
		this.statoOpera = statoOpera;
	}


	public String getRischio() {
		return rischio;
	}

	public void setRischio(String rischio) {
		this.rischio = rischio;
	}

	public String getFlagPD() {
		return flagPD;
	}

	public void setFlagPD(String flagPD) {
		this.flagPD = flagPD;
	}

	public String getFlagEL() {
		return flagEL;
	}

	public void setFlagEL(String flagEL) {
		this.flagEL = flagEL;
	}

	public String getFlagDoppioDeposito() {
		return flagDoppioDeposito;
	}

	public void setFlagDoppioDeposito(String flagDoppioDeposito) {
		this.flagDoppioDeposito = flagDoppioDeposito;
	}
}
