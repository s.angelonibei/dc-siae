package com.alkemytech.sophia.codman.dto.performing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;
import java.math.BigInteger;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class EventiPagatiConciliazioneImportiDTO {
    private BigInteger contabilita;
    private String voceIncasso;
    private BigDecimal importoLordoSophia;
    private String quadratura;
    private BigDecimal importoNettoSophia;
    private BigDecimal importoNettoExArt;


    public EventiPagatiConciliazioneImportiDTO() {
    }

    public EventiPagatiConciliazioneImportiDTO(BigInteger contabilita, String voceIncasso, BigDecimal importoLordoSophia, String quadratura, BigDecimal importoNettoSophia, BigDecimal importoNettoExArt) {
        this.contabilita = contabilita;
        this.voceIncasso = voceIncasso;
        this.importoLordoSophia = importoLordoSophia;
        this.quadratura = quadratura;
        this.importoNettoSophia = importoNettoSophia;
        this.importoNettoExArt = importoNettoExArt;
    }

    public BigInteger getContabilita() {
        return contabilita;
    }

    public void setContabilita(BigInteger contabilita) {
        this.contabilita = contabilita;
    }

    public String getVoceIncasso() {
        return voceIncasso;
    }

    public void setVoceIncasso(String voceIncasso) {
        this.voceIncasso = voceIncasso;
    }

    public BigDecimal getImportoLordoSophia() {
        return importoLordoSophia;
    }

    public void setImportoLordoSophia(BigDecimal importoLordoSophia) {
        this.importoLordoSophia = importoLordoSophia;
    }

    public String getQuadratura() {
        return quadratura;
    }

    public void setQuadratura(String quadratura) {
        this.quadratura = quadratura;
    }

    public BigDecimal getImportoNettoSophia() {
        return importoNettoSophia;
    }

    public void setImportoNettoSophia(BigDecimal importoNettoSophia) {
        this.importoNettoSophia = importoNettoSophia;
    }

    public BigDecimal getImportoNettoExArt() {
        return importoNettoExArt;
    }

    public void setImportoNettoExArt(BigDecimal importoNettoExArt) {
        this.importoNettoExArt = importoNettoExArt;
    }
}