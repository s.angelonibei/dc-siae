package com.alkemytech.sophia.codman.dto;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@SuppressWarnings("serial")
@Data
public class ClaimConfigDTO implements Serializable {
    private Long id;
    private String codiceUUID;
    private String dspId;
    private String dspName;
    private String countryId;
    private String countryName;
    private String utilizationTypeId;
    private String utilizationTypeName;
    private String offerId;
    private String offerName;
    private Double percDem;
    private Double percDrm;
    private String validFrom;
    private String validTo;
    private String lastUpdate;
}
