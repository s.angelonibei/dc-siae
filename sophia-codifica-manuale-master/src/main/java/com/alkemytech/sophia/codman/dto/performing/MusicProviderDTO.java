package com.alkemytech.sophia.codman.dto.performing;

import java.math.BigInteger;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class MusicProviderDTO {

	private BigInteger idMusicProvider;
	private Boolean attivo;
	private Date dataCreazione;
	private Date dataInizioValidita;
	private Date dataFineValidita;
	private String nome;

	public MusicProviderDTO() {
		super();
	}


	public MusicProviderDTO(BigInteger idMusicProvider, String nome, Date dataCreazione, Boolean attivo, Date dataInizioValidita,
			Date dataFineValidita) {
		super();
		this.idMusicProvider = idMusicProvider;
		this.attivo = attivo;
		this.dataCreazione = dataCreazione;
		this.dataInizioValidita = dataInizioValidita;
		this.dataFineValidita = dataFineValidita;
		this.nome = nome;
	}


	public BigInteger getIdMusicProvider() {
		return idMusicProvider;
	}


	public void setIdMusicProvider(BigInteger idMusicProvider) {
		this.idMusicProvider = idMusicProvider;
	}


	public Boolean getAttivo() {
		return attivo;
	}


	public void setAttivo(Boolean attivo) {
		this.attivo = attivo;
	}


	public Date getDataCreazione() {
		return dataCreazione;
	}


	public void setDataCreazione(Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}


	public Date getDataInizioValidita() {
		return dataInizioValidita;
	}


	public void setDataInizioValidita(Date dataInizioValidita) {
		this.dataInizioValidita = dataInizioValidita;
	}


	public Date getDataFineValidita() {
		return dataFineValidita;
	}


	public void setDataFineValidita(Date dataFineValidita) {
		this.dataFineValidita = dataFineValidita;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}

	
}
