package com.alkemytech.sophia.codman.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.alkemytech.sophia.codman.entity.AnagCountry;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

@Singleton
@Path("anagCountry")
public class AnagCountryService {

	private final Provider<EntityManager> provider;
	private final Gson gson;
	
	@Inject
	protected AnagCountryService(@McmdbDataSource Provider<EntityManager> provider, Gson gson) {
		super();
		this.provider = provider;
		this.gson = gson;
	}

	@GET
	@Produces("application/json")
	public Response getCountries() {
		final EntityManager entityManager = provider.get();
		final List<AnagCountry> anagCountries = entityManager
				.createQuery("select x from AnagCountry x order by x.name", AnagCountry.class)
				.getResultList();
		final Map<String, String> result = new HashMap<String, String>(anagCountries.size());
		for (AnagCountry anagCountry : anagCountries) {
			result.put(anagCountry.getIdCountry(), anagCountry.getName());
		}
		return Response.ok(gson.toJson(result)).build();
	}
	
	@GET
	@Path("search")
	@Produces("application/json")
	public Response searchCountries(@QueryParam("name") String name) {
		final EntityManager entityManager = provider.get();
		final List<AnagCountry> result = entityManager
				.createQuery("select x from AnagCountry x where lower(x.name) like :name order by x.name", AnagCountry.class)
				.setParameter("name", "%" + name.toLowerCase() + "%")
				.getResultList();
		return Response.ok(gson.toJson(result)).build();
	}
	
}
