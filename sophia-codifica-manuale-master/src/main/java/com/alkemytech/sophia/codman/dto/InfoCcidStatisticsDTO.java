package com.alkemytech.sophia.codman.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.alkemytech.sophia.codman.entity.InfoCcidStatistics;

public class InfoCcidStatisticsDTO {
	
	private Integer idInfoCcidStatistics;
	private Integer idImportoAnticipo;
	private Date periodoPertinenzaInizio;
	private Date periodoPertinenzaFine;
	private String numeroFattura;
	private BigDecimal importoOriginale;
	private BigDecimal importoUtilizzabile;
	private String idDsp;
	private BigDecimal quotaAnticipo;
	
	
	
	public InfoCcidStatisticsDTO( InfoCcidStatistics entity ) {
		init(entity);
	}

	public Integer getIdInfoCcidStatistics() {
		return idInfoCcidStatistics;
	}
	public void setIdInfoCcidStatistics(Integer idInfoCcidStatistics) {
		this.idInfoCcidStatistics = idInfoCcidStatistics;
	}
	public Integer getIdImportoAnticipo() {
		return idImportoAnticipo;
	}
	public void setIdImportoAnticipo(Integer idImportoAnticipo) {
		this.idImportoAnticipo = idImportoAnticipo;
	}
	public Date getPeriodoPertinenzaInizio() {
		return periodoPertinenzaInizio;
	}
	public void setPeriodoPertinenzaInizio(Date periodoPertinenzaInizio) {
		this.periodoPertinenzaInizio = periodoPertinenzaInizio;
	}
	public Date getPeriodoPertinenzaFine() {
		return periodoPertinenzaFine;
	}
	public void setPeriodoPertinenzaFine(Date periodoPertinenzaFine) {
		this.periodoPertinenzaFine = periodoPertinenzaFine;
	}
	public String getNumeroFattura() {
		return numeroFattura;
	}
	public void setNumeroFattura(String numeroFattura) {
		this.numeroFattura = numeroFattura;
	}
	public BigDecimal getImportoOriginale() {
		return importoOriginale;
	}
	public void setImportoOriginale(BigDecimal importoOriginale) {
		this.importoOriginale = importoOriginale;
	}
	public BigDecimal getImportoUtilizzabile() {
		return importoUtilizzabile;
	}
	public void setImportoUtilizzabile(BigDecimal importoUtilizzabile) {
		this.importoUtilizzabile = importoUtilizzabile;
	}
	public String getIdDsp() {
		return idDsp;
	}
	public void setIdDsp(String idDsp) {
		this.idDsp = idDsp;
	}
	public BigDecimal getQuotaAnticipo() {
		return quotaAnticipo;
	}
	public void setQuotaAnticipo(BigDecimal quotaAnticipo) {
		this.quotaAnticipo = quotaAnticipo;
	}
	
	private void init(InfoCcidStatistics entity) {
		
	 this.idImportoAnticipo = entity.getIdImportoAnticipo();
	 this.periodoPertinenzaInizio = entity.getPeriodoPertinenzaInizio();
	 this.periodoPertinenzaFine = entity.getPeriodoPertinenzaFine();
	 this.numeroFattura = entity.getNumeroFattura();
	 this.importoOriginale = entity.getImportoOriginale();
	 this.importoUtilizzabile = entity.getImportoUtilizzabile();
	 this.idDsp = entity.getIdDsp();
	 this.quotaAnticipo = entity.getQuotaAnticipo();
	 
	}
}
