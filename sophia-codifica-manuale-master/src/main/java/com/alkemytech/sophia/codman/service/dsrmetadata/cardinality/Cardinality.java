package com.alkemytech.sophia.codman.service.dsrmetadata.cardinality;

public interface Cardinality {

	public String getCode();
	
	public String getDescription();
	
	public String getRegex();
}
