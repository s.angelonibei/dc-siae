package com.alkemytech.sophia.codman.entity;

import com.alkemytech.sophia.codman.persistence.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity(name="DsrMetadataConfig")
@Table(name="DSR_METADATA_CONFIG")
@XmlRootElement
@SuppressWarnings("serial")

public class DsrMetadataConfig extends AbstractEntity<String>{

	@Id
	@Column(name="ID_DSR_METADATA_CONFIG", nullable=false)
	private int idDsrMetadataConfig;

	@Column(name="ID_COMMERCIAL_OFFER", nullable=false)
	private int idCommercialOffer;
	
	@Column(name="REGEX", nullable=false)
	private String regex;
	
	@Column(name="REGEX_CONFIG", nullable=false)
	private String regexConfig;
	
	@Column(name="REGEX_REPRESENTATION", nullable=false)
	private String regexRepresentation;
	
	@Column(name="COUNTRY_CODE", nullable=false)
	private String countryCode;
	
	@Column(name="PERIOD_CODE", nullable=false)
	private String periodCode;

	@Column(name="START_INDEX_COUNTRY", nullable=false)
	private int startIndexCountry;

	@Column(name="END_INDEX_COUNTRY", nullable=false)
	private int endIndexCountry;

	@Column(name="START_INDEX_PERIOD", nullable=false)
	private int startIndexPeriod;

	@Column(name="END_INDEX_PERIOD", nullable=false)
	private int endIndexPeriod;

	public int getIdDsrMetadataConfig() {
		return idDsrMetadataConfig;
	}

	public void setIdDsrMetadataConfig(int idDsrMetadataConfig) {
		this.idDsrMetadataConfig = idDsrMetadataConfig;
	}

	public int getIdCommercialOffer() {
		return idCommercialOffer;
	}

	public void setIdCommercialOffer(int idCommercialOffer) {
		this.idCommercialOffer = idCommercialOffer;
	}

	public String getRegex() {
		return regex;
	}

	public void setRegex(String regex) {
		this.regex = regex;
	}

	public String getRegexConfig() {
		return regexConfig;
	}

	public void setRegexConfig(String regexConfig) {
		this.regexConfig = regexConfig;
	}



	public String getRegexRepresentation() {
		return regexRepresentation;
	}



	public void setRegexRepresentation(String regexRepresentation) {
		this.regexRepresentation = regexRepresentation;
	}



	public String getCountryCode() {
		return countryCode;
	}



	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}



	public String getPeriodCode() {
		return periodCode;
	}



	public void setPeriodCode(String periodCode) {
		this.periodCode = periodCode;
	}



	public int getStartIndexCountry() {
		return startIndexCountry;
	}



	public void setStartIndexCountry(int startIndexCountry) {
		this.startIndexCountry = startIndexCountry;
	}



	public int getEndIndexCountry() {
		return endIndexCountry;
	}



	public void setEndIndexCountry(int endIndexCountry) {
		this.endIndexCountry = endIndexCountry;
	}



	public int getStartIndexPeriod() {
		return startIndexPeriod;
	}



	public void setStartIndexPeriod(int startIndexPeriod) {
		this.startIndexPeriod = startIndexPeriod;
	}



	public int getEndIndexPeriod() {
		return endIndexPeriod;
	}



	public void setEndIndexPeriod(int endIndexPeriod) {
		this.endIndexPeriod = endIndexPeriod;
	}



	@Override
	public String getId() {
		return String.valueOf(idDsrMetadataConfig);
	}

}
