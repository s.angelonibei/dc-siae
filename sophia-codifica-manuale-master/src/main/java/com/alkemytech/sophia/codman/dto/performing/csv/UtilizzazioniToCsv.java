package com.alkemytech.sophia.codman.dto.performing.csv;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

public class UtilizzazioniToCsv {
	

    @CsvBindByPosition(position = 0)
    @CsvBindByName
    private String codiceOpera;

    @CsvBindByPosition(position = 1)
    @CsvBindByName
    private String titoloComposizione;

    @CsvBindByPosition(position = 2)
    @CsvBindByName
    private String compositore;

    @CsvBindByPosition(position = 3)
    @CsvBindByName
    private String durata;

    @CsvBindByPosition(position = 4)
    @CsvBindByName
    private String durataInferiore30sec;

    @CsvBindByPosition(position = 5)
    @CsvBindByName
    private String autori;

    @CsvBindByPosition(position = 6)
    @CsvBindByName
    private String interpreti;

    @CsvBindByPosition(position = 7)
    @CsvBindByName
    private String flagPubblicoDominio;

    //@CsvBindByPosition(position = 8)
    //@CsvBindByName
    private String importoCedola;
    
    @CsvBindByPosition(position = 8)
    @CsvBindByName
    private String nettoOriginario;
    
    @CsvBindByPosition(position = 9)
    @CsvBindByName
    private String importoValorizzato;    
    
    
	public UtilizzazioniToCsv(String codiceOpera, String titoloComposizione, String compositore, String durata,
			String durataInferiore30sec, String autori, String interpreti, String flagPubblicoDominio,
			String importoCedola) {
		super();
		this.codiceOpera = codiceOpera;
		this.titoloComposizione = titoloComposizione;
		this.compositore = compositore;
		this.durata = durata;
		this.durataInferiore30sec = durataInferiore30sec;
		this.autori = autori;
		this.interpreti = interpreti;
		this.flagPubblicoDominio = flagPubblicoDominio;
		this.importoCedola = importoCedola;
	}
	

	public UtilizzazioniToCsv() {
		super();
	}
	
	public String getCodiceOpera() {
		return codiceOpera;
	}
	public void setCodiceOpera(String codiceOpera) {
		this.codiceOpera = codiceOpera;
	}
	public String getTitoloComposizione() {
		return titoloComposizione;
	}
	public void setTitoloComposizione(String titoloComposizione) {
		this.titoloComposizione = titoloComposizione;
	}
	public String getCompositore() {
		return compositore;
	}
	public void setCompositore(String compositore) {
		this.compositore = compositore;
	}
	public String getDurata() {
		return durata;
	}
	public void setDurata(String durata) {
		this.durata = durata;
	}
	public String getDurataInferiore30sec() {
		return durataInferiore30sec;
	}
	public void setDurataInferiore30sec(String durataInferiore30sec) {
		this.durataInferiore30sec = durataInferiore30sec;
	}
	public String getAutori() {
		return autori;
	}
	public void setAutori(String autori) {
		this.autori = autori;
	}
	public String getInterpreti() {
		return interpreti;
	}
	public void setInterpreti(String interpreti) {
		this.interpreti = interpreti;
	}
	public String getFlagPubblicoDominio() {
		return flagPubblicoDominio;
	}
	public void setFlagPubblicoDominio(String flagPubblicoDominio) {
		this.flagPubblicoDominio = flagPubblicoDominio;
	}
	public String getImportoCedola() {
		return importoCedola;
	}
	public void setImportoCedola(String importoCedola) {
		this.importoCedola = importoCedola;
	}
    
	public String[] getMappingStrategy() {
        return new String[]{
        		"Codice Opera",
        		"Titolo Composizione",
        		"Compositore",
    			"Durata in secondi",
        		"Durata Inferiore 30 sec",
        		"Autori",
        		"Interpreti",
        		"Flag Pubblico Dominio",
        		"Importo Netto Cedola Valorizzata",
        		"Importo Netto In Ripartizione"
        };
	}

	public static class Builder {
		private String codiceOpera;
		private String titoloComposizione;
		private String compositore;
		private String durata;
		private String durataInferiore30sec;
		private String autori;
		private String interpreti;
		private String flagPubblicoDominio;
		private String importoCedola;
		private String nettoOriginario;
		private String importoValorizzato;

		public Builder codiceOpera(String codiceOpera) {
			this.codiceOpera = codiceOpera;
			return this;
		}

		public Builder titoloComposizione(String titoloComposizione) {
			this.titoloComposizione = titoloComposizione;
			return this;
		}

		public Builder compositore(String compositore) {
			this.compositore = compositore;
			return this;
		}

		public Builder durata(String durata) {
			this.durata = durata;
			return this;
		}

		public Builder durataInferiore30sec(String durataInferiore30sec) {
			this.durataInferiore30sec = durataInferiore30sec;
			return this;
		}

		public Builder autori(String autori) {
			this.autori = autori;
			return this;
		}

		public Builder interpreti(String interpreti) {
			this.interpreti = interpreti;
			return this;
		}

		public Builder flagPubblicoDominio(String flagPubblicoDominio) {
			this.flagPubblicoDominio = flagPubblicoDominio;
			return this;
		}

		public Builder importoCedola(String importoCedola) {
			this.importoCedola = importoCedola;
			return this;
		}

		public Builder nettoOriginario(String nettoOriginario) {
			this.nettoOriginario = nettoOriginario;
			return this;
		}

		public Builder importoValorizzato(String importoValorizzato) {
			this.importoValorizzato = importoValorizzato;
			return this;
		}

		public UtilizzazioniToCsv build() {
			return new UtilizzazioniToCsv(this);
		}
	}

	private UtilizzazioniToCsv(Builder builder) {
		this.codiceOpera = builder.codiceOpera;
		this.titoloComposizione = builder.titoloComposizione;
		this.compositore = builder.compositore;
		this.durata = builder.durata;
		this.durataInferiore30sec = builder.durataInferiore30sec;
		this.autori = builder.autori;
		this.interpreti = builder.interpreti;
		this.flagPubblicoDominio = builder.flagPubblicoDominio;
		this.importoCedola = builder.importoCedola;
		this.setNettoOriginario(builder.nettoOriginario);
		this.setImportoValorizzato(builder.importoValorizzato);
	}


	public String getNettoOriginario() {
		return nettoOriginario;
	}


	public void setNettoOriginario(String nettoOriginario) {
		this.nettoOriginario = nettoOriginario;
	}


	public String getImportoValorizzato() {
		return importoValorizzato;
	}


	public void setImportoValorizzato(String importoValorizzato) {
		this.importoValorizzato = importoValorizzato;
	}
}
