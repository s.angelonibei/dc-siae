package com.alkemytech.sophia.codman.guice;

import com.alkemytech.sophia.broadcasting.service.*;
import com.alkemytech.sophia.broadcasting.service.interfaces.*;
import com.alkemytech.sophia.broadcasting.service.performing.*;
import com.alkemytech.sophia.codman.dto.performing.CombanaDTO;
import com.alkemytech.sophia.codman.entity.performing.dao.*;
import com.alkemytech.sophia.codman.gestioneperiodi.GestionePeriodiImpl;
import com.alkemytech.sophia.codman.gestioneperiodi.GestionePeriodiInterface;
import com.alkemytech.sophia.codman.mandato.service.IConfigurazioneMandatoService;
import com.alkemytech.sophia.codman.mandato.service.IMandatoService;
import com.alkemytech.sophia.codman.mandato.service.ISocietaTutelaService;
import com.alkemytech.sophia.codman.mandato.service.impl.ConfigurazioneMandatoService;
import com.alkemytech.sophia.codman.mandato.service.impl.MandatoService;
import com.alkemytech.sophia.codman.mandato.service.impl.SocietaTutelaService;
import com.alkemytech.sophia.codman.multimedialelocale.repository.DsrMetadataRepository;
import com.alkemytech.sophia.codman.multimedialelocale.repository.IDsrMetadataRepository;
import com.alkemytech.sophia.codman.provider.JooqProvider;
import com.alkemytech.sophia.codman.rest.performing.service.*;
import com.alkemytech.sophia.codman.rest.performing.serviceimpl.*;
import com.alkemytech.sophia.common.s3.S3Service;
import com.google.inject.*;
import com.google.inject.persist.PersistFilter;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.util.Properties;

public class McmdbJpaModule extends AbstractJpaModule {

    public static final String UNIT_NAME = "mcmdb";
    public static final Key<PersistFilter> FILTER_KEY = Key.get(PersistFilter.class, McmdbDataSource.class);

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public McmdbJpaModule(Properties configuration) {
        super(configuration, UNIT_NAME);
    }

    @Override
    protected void configure() {
        // install jpa module
        super.configure();
        final Provider<EntityManager> entityManagerProvider = binder().getProvider(EntityManager.class);
        bind(EntityManager.class).annotatedWith(McmdbDataSource.class).toProvider(entityManagerProvider);
        expose(EntityManager.class).annotatedWith(McmdbDataSource.class);
        bind(FILTER_KEY).to(PersistFilter.class);
        expose(FILTER_KEY);



        bind(DSLContext.class).annotatedWith(McmdbDataSource.class).toProvider(JooqProvider.class).in(Singleton.class);
        expose(DSLContext.class)
                .annotatedWith(McmdbDataSource.class);

        bind(BroadcasterConfigService.class).toInstance(new BroadcasterConfigServiceImpl(entityManagerProvider));
        expose(BroadcasterConfigService.class);

        bind(InformationFileEntityService.class)
                .toInstance(new InformationFileEntityServiceImpl(entityManagerProvider));
        expose(InformationFileEntityService.class);

        bind(BdcBroadcastersService.class).toInstance(new BdcBroadcastersServiceImpl(entityManagerProvider));
        expose(BdcBroadcastersService.class);

        bind(BdcUtentiService.class).toInstance(new BdcUtentiServiceImpl(entityManagerProvider));
        expose(BdcUtentiService.class);

        bind(BdcCanaliService.class).toInstance(new BdcCanaliServiceImpl(entityManagerProvider));
        expose(BdcCanaliService.class);

        final BdcIndicatoreDTOServiceImpl bdcIndicatoreDTOServiceImpl = new BdcIndicatoreDTOServiceImpl(entityManagerProvider,
                new BdcCanaliServiceImpl(entityManagerProvider));

        bind(BdcIndicatoreDTOService.class).toInstance(bdcIndicatoreDTOServiceImpl);
        expose(BdcIndicatoreDTOService.class);

        KpiServiceImpl impl = new KpiServiceImpl(bdcIndicatoreDTOServiceImpl);
        bind(KpiService.class).toInstance(impl);
        expose(KpiService.class);

        bind(BdcInformationFileDTOService.class)
                .toInstance(new BdcInformationFileDTOServiceImpl(entityManagerProvider));
        expose(BdcInformationFileDTOService.class);

        bind(BdcFileArmonizzatoService.class).toInstance(new BdcFileArmonizzatoServiceImpl(entityManagerProvider));
        expose(BdcFileArmonizzatoService.class);

        bind(BdcNewsService.class).toInstance(new BdcNewsServiceImpl(entityManagerProvider));
        expose(BdcNewsService.class);

        bind(BdcCaricoRipartizioneService.class).toInstance(new BdcCaricoRipartizioneServiceImpl(entityManagerProvider, impl));
        expose(BdcCaricoRipartizioneService.class);


        bind(BdcRuoliService.class).toInstance(new BdcRuoliServiceImpl(entityManagerProvider));
        expose(BdcRuoliService.class);

        bind(ICampionamentoDAO.class).to(CampionamentoDAO.class);
        expose(ICampionamentoDAO.class);

        bind(IDsrMetadataRepository.class).to(DsrMetadataRepository.class);
        expose(IDsrMetadataRepository.class);

        bind(IJCAClientService.class).to(JCAClientService.class);
        expose(IJCAClientService.class);

        bind(ICampionamentoService.class)
                .toInstance(new CampionamentoService(new CampionamentoDAO(entityManagerProvider)));
        expose(ICampionamentoService.class);

        bind(IConfigurationService.class)
                .toInstance(new ConfigurationService(new ConfigurationDAO(entityManagerProvider)));
        expose(IConfigurationService.class);

        bind(IConsoleRicalcoloService.class).to(ConsoleRicalcoloService.class);
        expose(IConsoleRicalcoloService.class);

        // TODO see how to finish
        bind(IProgrammaMusicaleService.class).to(ProgrammaMusicaleService.class);
        expose(IProgrammaMusicaleService.class);

        bind(ITraceService.class).toInstance(new TraceService(new EsecuzioneDAO(entityManagerProvider)));
        expose(ITraceService.class);

        bind(IConfigurationDAO.class).toInstance(new ConfigurationDAO(entityManagerProvider));
        expose(IConfigurationDAO.class);

        bind(IProgrammaMusicaleDAO.class).to(ProgrammaMusicaleDAO.class);
        expose(IProgrammaMusicaleDAO.class);

        bind(IEsecuzioneDAO.class).toInstance(new EsecuzioneDAO(entityManagerProvider));
        expose(IEsecuzioneDAO.class);

        bind(GestionePeriodiInterface.class).toInstance(new GestionePeriodiImpl());
        expose(GestionePeriodiInterface.class);

        bind(BdcPreallocazioneIncassoService.class).toInstance(new BdcPreallocazioneIncassoServiceImpl(entityManagerProvider));
        expose(BdcPreallocazioneIncassoService.class);

        bind(BdcUserTokenService.class).toInstance(new BdcUserTokenServiceImpl(entityManagerProvider));
        expose(BdcUserTokenService.class);

        bind(new TypeLiteral<SequentialAdapter<CombanaDTO>>() { }).toProvider(new CodificaEspertoCombanaAdapterProvider(new ConfigurationDAO(entityManagerProvider)));//toProvider(CodificaEspertoCombanaAdapterProvider.class);
        final Provider<SequentialAdapter<CombanaDTO>> combanaAdapterProvider = binder().getProvider(Key.get(new TypeLiteral<SequentialAdapter<CombanaDTO>>(){}));


        bind(ICodificaService.class).toInstance(new CodificaService(new CodificaDAO(entityManagerProvider), new CodificaMassivaDAO(entityManagerProvider), configuration, combanaAdapterProvider));
        expose(ICodificaService.class);

        bind(ICodificaMassivaDAO.class).toInstance(new CodificaMassivaDAO(entityManagerProvider));
        expose(ICodificaMassivaDAO.class);

        bind(ICodificaDAO.class).toInstance(new CodificaDAO(entityManagerProvider));
        expose(ICodificaDAO.class);

        bind(IScodificaService.class).to(ScodificaService.class);
        expose(IScodificaService.class);

        bind(IScodificaDAO.class).to(ScodificaDAO.class);
        expose(IScodificaDAO.class);

        bind(ValorizzazioneService.class).toInstance(new ValorizzazioneServiceImpl(new ValorizzazioneServiceDAO(entityManagerProvider)));
        expose(ValorizzazioneService.class);

        bind(IValorizzazioneServiceDAO.class).toInstance(new ValorizzazioneServiceDAO(entityManagerProvider));
        expose(IValorizzazioneServiceDAO.class);

        bind(ImonitoringKPIService.class).toInstance(new MonitoringKPIService(new MonitoringKPIDAO(entityManagerProvider)));
        expose(ImonitoringKPIService.class);

        bind(IMonitoringKPIDAO.class).toInstance(new MonitoringKPIDAO(entityManagerProvider));
        expose(ImonitoringKPIService.class);

        bind(IAggiornamentoSunService.class).toInstance(new AggiornamentoSunService(new AggiornamentoSunDAO(entityManagerProvider), configuration));
        expose(IAggiornamentoSunService.class);

        bind(IAggiornamentoSunDAO.class).toInstance(new AggiornamentoSunDAO(entityManagerProvider));
        expose(IAggiornamentoSunDAO.class);

        bind(IRadioInStoreService.class).toInstance(new RadioInStoreService(new RadioInStoreDAO(entityManagerProvider)));
        expose(IRadioInStoreService.class);

        bind(IRadioInStoreDAO.class).toInstance(new RadioInStoreDAO(entityManagerProvider));
        expose(IRadioInStoreDAO.class);

        bind(FileArmonizzatoService.class).toInstance(new FileArmonizzatoServiceImpl(entityManagerProvider));
        expose(FileArmonizzatoService.class);

        bind(RsIndicatoreService.class).toInstance(new RsIndicatoreServiceImpl(entityManagerProvider));
        expose(RsIndicatoreService.class);

        bind(RsInformationFileService.class).toInstance(new RsInformationFileServiceImpl(entityManagerProvider));
        expose(RsInformationFileService.class);

        RsKpiService impl2 = new RsKpiServiceImpl(new RsIndicatoreServiceImpl(entityManagerProvider));
        bind(RsKpiService.class).toInstance(impl2);
        expose(RsKpiService.class);

        bind(EventiPagatiService.class).to(EventiPagatiServiceImpl.class);
        expose(EventiPagatiService.class);

        bind(IEventiPagatiDAO.class).to(EventiPagatiDAO.class);
        expose(IEventiPagatiDAO.class);
        
		bind(ISocietaTutelaService.class).toInstance(new SocietaTutelaService(entityManagerProvider,getProvider(S3Service.class)));
		expose(ISocietaTutelaService.class);
		
		bind(IMandatoService.class).toInstance(new MandatoService(entityManagerProvider));
		expose(IMandatoService.class);
		
		bind(IConfigurazioneMandatoService.class).toInstance(new ConfigurazioneMandatoService(entityManagerProvider));
		expose(IConfigurazioneMandatoService.class);
    }

    @Provides
    public SQLDialect dialect() {
        //TODO read from DB configuration
        return SQLDialect.MYSQL;
    }
}
