package com.alkemytech.sophia.codman.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.GsonBuilder;

@XmlRootElement
@Entity(name="MMPercClaimResult")
@Table(name="MM_PERC_CLAIM_RESULT")
public class MMPercClaimResult {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "ID_DSR", nullable = false)
    private String dsr;

    @Column(name = "ID_CONFIG", nullable = false)
    private Long idConfig;

    @Column(name = "PERC_DEM_RES")
    private Float percDemRes;

    @Column(name = "PERC_DRM_RES")
    private Float percDrmRes;

    @Column(name = "PERC_DEM_CONF")
    private Float percDemConf;

    @Column(name = "PERC_DRM_CONF")
    private Float percDrmConf;

    @Column(name = "ESITO", nullable = false)
    private String esito;

    @Column(name = "SALES_TRANSACTION_ID")
    private String salesTransactionId;

    //@ManyToOne(fetch = FetchType.EAGER)
    //@JoinColumn(name = "ID_CONFIG", referencedColumnName = "ID")
    //private SocietaTutela societaTutela;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDsr() {
        return dsr;
    }

    public void setDsr(String dsr) {
        this.dsr = dsr;
    }

    public Long getIdConfig() {
        return idConfig;
    }

    public void setIdConfig(Long idConfig) {
        this.idConfig = idConfig;
    }

    public Float getPercDemRes() {
        return percDemRes;
    }

    public void setPercDemRes(Float percDemRes) {
        this.percDemRes = percDemRes;
    }

    public Float getPercDrmRes() {
        return percDrmRes;
    }

    public void setPercDrmRes(Float percDrmRes) {
        this.percDrmRes = percDrmRes;
    }

    public Float getPercDemConf() {
        return percDemConf;
    }

    public void setPercDemConf(Float percDemConf) {
        this.percDemConf = percDemConf;
    }

    public Float getPercDrmConf() {
        return percDrmConf;
    }

    public void setPercDrmConf(Float percDrmConf) {
        this.percDrmConf = percDrmConf;
    }

    public String getEsito() {
        return esito;
    }

    public void setEsito(String esito) {
        this.esito = esito;
    }

    public String getSalesTransactionId() {
        return salesTransactionId;
    }

    public void setSalesTransactionId(String salesTransactionId) {
        this.salesTransactionId = salesTransactionId;
    }

    @Override
    public String toString() {
        return new GsonBuilder()
                .setPrettyPrinting()
                .create()
                .toJson(this);
    }

}
