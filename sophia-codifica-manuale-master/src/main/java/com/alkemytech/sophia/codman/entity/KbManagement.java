package com.alkemytech.sophia.codman.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.GsonBuilder;

@XmlRootElement
@Entity(name="KbManagement")
@Table(name="kb_management")
public class KbManagement {

	@Id
	@Column(name="kb_active_version", nullable=false)
	private Integer kbActiveVersion;

	@Column(name="kb_learning_in_progress", nullable=false)
	private Boolean kbLearningInProgress;
	
	public Integer getKbActiveVersion() {
		return kbActiveVersion;
	}

	public void setKbActiveVersion(Integer kbActiveVersion) {
		this.kbActiveVersion = kbActiveVersion;
	}

	public Boolean getKbLearningInProgress() {
		return kbLearningInProgress;
	}

	public void setKbLearningInProgress(Boolean kbLearningInProgress) {
		this.kbLearningInProgress = kbLearningInProgress;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}
}
