package com.alkemytech.sophia.codman.multimedialelocale.dto;

import java.io.Serializable;

import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;

@Produces("application/json")
@XmlRootElement
public class MultimedialeLocaleIngestionRequestDto implements Serializable {

	private Integer idMlReport;

	private String licenza;

	private String codiceLicenziatario;

	private String nominativoLicenziatario;

	private String servizio;

	private String periodoCompetenza;

	private Integer modUtilizzazione;

	private String modFruizione;

	private Boolean inAbbonamento;

	private String posizioneReport;

	private String identificativoReport;
	
	private Integer progressivoReport;

	private boolean usaCodiceOpera;

	public MultimedialeLocaleIngestionRequestDto() {
		super();
	}

	public MultimedialeLocaleIngestionRequestDto(Integer idMlReport, String licenza, String codiceLicenziatario,
			String nominativoLicenziatario, String servizio, String periodoCompetenza, Integer modUtilizzazione,
			String modFruizione, Boolean inAbbonamento, String posizioneReport, String identificativoReport,
			Integer progressivoReport, boolean usaCodiceOpera) {
		super();
		this.idMlReport = idMlReport;
		this.licenza = licenza;
		this.codiceLicenziatario = codiceLicenziatario;
		this.nominativoLicenziatario = nominativoLicenziatario;
		this.servizio = servizio;
		this.periodoCompetenza = periodoCompetenza;
		this.modUtilizzazione = modUtilizzazione;
		this.modFruizione = modFruizione;
		this.inAbbonamento = inAbbonamento;
		this.posizioneReport = posizioneReport;
		this.identificativoReport = identificativoReport;
		this.progressivoReport = progressivoReport;
		this.usaCodiceOpera = usaCodiceOpera;
	}


	public boolean isUsaCodiceOpera() {
		return usaCodiceOpera;
	}


	public void setUsaCodiceOpera(boolean usaCodiceOpera) {
		this.usaCodiceOpera = usaCodiceOpera;
	}


	public Integer getIdMlReport() {
		return idMlReport;
	}

	public void setIdMlReport(Integer idMlReport) {
		this.idMlReport = idMlReport;
	}

	public String getLicenza() {
		return licenza;
	}

	public void setLicenza(String licenza) {
		this.licenza = licenza;
	}

	public String getCodiceLicenziatario() {
		return codiceLicenziatario;
	}

	public void setCodiceLicenziatario(String codiceLicenziatario) {
		this.codiceLicenziatario = codiceLicenziatario;
	}

	public String getNominativoLicenziatario() {
		return nominativoLicenziatario;
	}

	public void setNominativoLicenziatario(String nominativoLicenziatario) {
		this.nominativoLicenziatario = nominativoLicenziatario;
	}

	public String getServizio() {
		return servizio;
	}

	public void setServizio(String servizio) {
		this.servizio = servizio;
	}

	public String getPeriodoCompetenza() {
		return periodoCompetenza;
	}

	public void setPeriodoCompetenza(String periodoCompetenza) {
		this.periodoCompetenza = periodoCompetenza;
	}

	public Integer getModUtilizzazione() {
		return modUtilizzazione;
	}

	public void setModUtilizzazione(Integer modUtilizzazione) {
		this.modUtilizzazione = modUtilizzazione;
	}

	public String getModFruizione() {
		return modFruizione;
	}

	public void setModFruizione(String modFruizione) {
		this.modFruizione = modFruizione;
	}

	public Boolean getInAbbonamento() {
		return inAbbonamento;
	}

	public void setInAbbonamento(Boolean inAbbonamento) {
		this.inAbbonamento = inAbbonamento;
	}

	public String getPosizioneReport() {
		return posizioneReport;
	}

	public void setPosizioneReport(String posizioneReport) {
		this.posizioneReport = posizioneReport;
	}


	public String getIdentificativoReport() {
		return identificativoReport;
	}


	public void setIdentificativoReport(String identificativoReport) {
		this.identificativoReport = identificativoReport;
	}


	public Integer getProgressivoReport() {
		return progressivoReport;
	}


	public void setProgressivoReport(Integer progressivoReport) {
		this.progressivoReport = progressivoReport;
	}

}
