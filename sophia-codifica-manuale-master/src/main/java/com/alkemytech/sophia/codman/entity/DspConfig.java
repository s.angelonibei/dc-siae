package com.alkemytech.sophia.codman.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.codman.persistence.AbstractEntity;
@XmlRootElement
@Entity(name="DspConfig")
@Table(name="DSP_CONFIG")
@SuppressWarnings("serial")
public class DspConfig extends AbstractEntity<String>{

	@Id
	@Column(name="ID_DSP_CONFIG", nullable=false)
	private int idDspConfig;

	@Column(name="IDDSP", nullable=false)
	private String idDSP;

	@Column(name="ID_UTILIZATION_TYPE", nullable=false)
	private String idUtilizationType;

	@Column(name="OFFERING", nullable=false)
	private String offering;
	
	@Column(name="DSR_FREQUENCY", nullable=false)
	private String dsrFrequency;
	
	@Column(name="SLA_RECEIVING", nullable=false)
	private String slaReceiving;
	
	@Column(name="SLA_SENDING", nullable=false)
	private String slaSending;
	
	public int getIdDspConfig() {
		return idDspConfig;
	}

	public void setIdDspConfig(int idDspConfig) {
		this.idDspConfig = idDspConfig;
	}

	public String getIdDSP() {
		return idDSP;
	}

	public void setIdDSP(String idDSP) {
		this.idDSP = idDSP;
	}

	public String getIdUtilizationType() {
		return idUtilizationType;
	}

	public void setIdUtilizationType(String idUtilizationType) {
		this.idUtilizationType = idUtilizationType;
	}

	public String getOffering() {
		return offering;
	}

	public void setOffering(String offering) {
		this.offering = offering;
	}

	public String getDsrFrequency() {
		return dsrFrequency;
	}

	public void setDsrFrequency(String dsrFrequency) {
		this.dsrFrequency = dsrFrequency;
	}

	public String getSlaReceiving() {
		return slaReceiving;
	}

	public void setSlaReceiving(String slaReceiving) {
		this.slaReceiving = slaReceiving;
	}

	public String getSlaSending() {
		return slaSending;
	}

	public void setSlaSending(String slaSending) {
		this.slaSending = slaSending;
	}

	@Override
	public String getId() {
		return String.valueOf(getIdDspConfig());
	}

}
