package com.alkemytech.sophia.codman.broadcasting;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import javax.ws.rs.core.Response.Status;

import com.alkemytech.sophia.broadcasting.dto.BdcCarichiRipartoCsv;
import com.alkemytech.sophia.broadcasting.dto.CarichiRipartizioneRequestDto;
import com.alkemytech.sophia.broadcasting.dto.KPI;
import com.alkemytech.sophia.broadcasting.dto.UpdateCarichiRipartizioneRequestDto;
import com.alkemytech.sophia.broadcasting.model.BdcCaricoRipartizione;
import com.alkemytech.sophia.broadcasting.model.BdcCaricoRipartizioneStoricoModifica;
import com.alkemytech.sophia.broadcasting.model.TipoBroadcaster;
import com.alkemytech.sophia.broadcasting.service.interfaces.BdcCaricoRipartizioneService;
import com.alkemytech.sophia.broadcasting.service.interfaces.KpiService;
import com.alkemytech.sophia.codman.rest.PagedResult;
import com.alkemytech.sophia.common.beantocsv.CustomMappingStrategy;
import com.amazonaws.util.CollectionUtils;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

@Singleton
@Path("gestioneRipartizione")
public class GestioneRipartizioneRestService {
	private Gson gson;
	private BdcCaricoRipartizioneService bdcCaricoRipartizioneService;
	private final static String NO_RESULTS = "{\"message\":\"No results found\"}";

	@Inject
	public GestioneRipartizioneRestService(BdcCaricoRipartizioneService bdcCaricoRipartizioneService,
			KpiService kpiService, Gson gson) {
		this.gson = gson;
		this.bdcCaricoRipartizioneService = bdcCaricoRipartizioneService;
	}

	@POST
	@Path("getRipartizioni")
	@Produces("application/json")
	public Response getRipartizioni(CarichiRipartizioneRequestDto carichiRipartizioneRequestDto) {
		try {
			if (carichiRipartizioneRequestDto.getDateFrom()==null||carichiRipartizioneRequestDto.getDateTo()==null) {
				return Response.status(410).build();
			}
			SimpleDateFormat sdf= new SimpleDateFormat("dd-MM-yyyy");
			Date dataInizio =sdf.parse(carichiRipartizioneRequestDto.getDateFrom());
			Date dataFine =sdf.parse(carichiRipartizioneRequestDto.getDateTo());
			if (dataInizio.compareTo(dataFine)==1) {
				return Response.status(Status.NOT_ACCEPTABLE).build();	
			}	
			
			List<BdcCaricoRipartizione> result = bdcCaricoRipartizioneService.findAll(
					dataInizio,dataFine, carichiRipartizioneRequestDto.getTipoEmittente(),
					carichiRipartizioneRequestDto.getIdEmittente(), carichiRipartizioneRequestDto.getIdCanale(),
					carichiRipartizioneRequestDto.getFirst(), carichiRipartizioneRequestDto.getLast(),
					carichiRipartizioneRequestDto.getMaxrow());
			if (!CollectionUtils.isNullOrEmpty(result)) {
				final PagedResult pagedResult = new PagedResult();
				if (null != result && !result.isEmpty()) {
					final int maxrows = carichiRipartizioneRequestDto.getLast()
							- carichiRipartizioneRequestDto.getFirst();
					final boolean hasNext = result.size() > maxrows;
					pagedResult.setRows(!hasNext ? result : result.subList(0, maxrows)).setMaxrows(maxrows)
							.setFirst(carichiRipartizioneRequestDto.getFirst())
							.setLast(hasNext ? carichiRipartizioneRequestDto.getLast()
									: carichiRipartizioneRequestDto.getFirst() + result.size())
							.setHasNext(hasNext).setHasPrev(carichiRipartizioneRequestDto.getFirst() > 0);
				} else {
					pagedResult.setMaxrows(0).setFirst(0).setLast(0).setHasNext(false).setHasPrev(false);
				}
				return Response.ok(gson.toJson(pagedResult)).build();
			} else {
				return Response.ok(gson.toJson(NO_RESULTS)).build();
			}
		} catch (Exception e) {
			return Response.status(500).build();
		}
	}

	@POST
	@Path("downloadRipartizioni")
	@Produces("application/json")
	public Response downloadUtilizzazioni(CarichiRipartizioneRequestDto carichiRipartizioneRequestDto) {
		try {
			SimpleDateFormat sdf= new SimpleDateFormat("dd-MM-yyyy");
			Date dataInizio =sdf.parse(carichiRipartizioneRequestDto.getDateFrom());
			Date dataFine =sdf.parse(carichiRipartizioneRequestDto.getDateTo());
			if (dataInizio.after(dataFine)) {
				return Response.status(Status.NOT_ACCEPTABLE).build();	
			}	
			
			List<BdcCaricoRipartizione> result = bdcCaricoRipartizioneService.findAll(
					dataInizio,dataFine, carichiRipartizioneRequestDto.getTipoEmittente(),
					carichiRipartizioneRequestDto.getIdEmittente(), carichiRipartizioneRequestDto.getIdCanale(),
					carichiRipartizioneRequestDto.getFirst(), carichiRipartizioneRequestDto.getLast(),
					carichiRipartizioneRequestDto.getMaxrow());
			

			List<BdcCarichiRipartoCsv> csvList = new ArrayList<>();
			
			for (BdcCaricoRipartizione bdcCaricoRipartizione : result) {
				
				String kpi = "";
					
				BdcCarichiRipartoCsv bdcCarichiRipartoCsv = new BdcCarichiRipartoCsv(
						bdcCaricoRipartizione.getCanale().getNome(),
						bdcCaricoRipartizione.getCanale().getBdcBroadcasters().getNome(),
						bdcCaricoRipartizione.getCanale().getTipoCanale(),
						bdcCaricoRipartizione.getIncassoNetto(),
						bdcCaricoRipartizione.getIncasso(), 
						bdcCaricoRipartizione.isSospesoPerContenzioso(),
						bdcCaricoRipartizione.getTipoDiritto(),
						bdcCaricoRipartizione.isRepoDisponibile(),
						bdcCaricoRipartizione.isImportoRipartibile(),
						bdcCaricoRipartizione.getInizioPeriodoCompetenza(),
						bdcCaricoRipartizione.getFinePeriodoCompetenza(),
						bdcCaricoRipartizione.getNotaCommentoUffEmittenti(),
						bdcCaricoRipartizione.getNotaCommentoDirRipartizione());
				
			 
				if (bdcCaricoRipartizione.getKpi() != null) {
					DecimalFormat df = new DecimalFormat("0.00##");
					for (KPI iterable_element : bdcCaricoRipartizione.getKpi()) {
						if (KPI.KPI_COPERTURA.equals(iterable_element.getKpi())) {
							bdcCarichiRipartoCsv.setCompletezzaPalinsesto(df.format(iterable_element.getValore().doubleValue())+" %");
						} else if (KPI.KPI_DURATA_MUSICA_DICHIARATA.equals(iterable_element.getKpi())) {
							bdcCarichiRipartoCsv.setMusicaDichiarata(df.format(iterable_element.getValore().doubleValue())+" %");						
						}else if (KPI.KPI_BRANI_TITOLI_ERRONEI.equals(iterable_element.getKpi())) {
							bdcCarichiRipartoCsv.setOpereMusicaliTitoloErroneo(df.format(iterable_element.getValore().doubleValue())+" %");
						}else if (KPI.KPI_FILM_TITOLI_ERRONEI.equals(iterable_element.getKpi())) {
							bdcCarichiRipartoCsv.setOpereFilmicheTitoloErroneo(df.format(iterable_element.getValore().doubleValue())+" %");
						}else if (KPI.KPI_AUTORI_TITOLI_ERRONEI.equals(iterable_element.getKpi())) {
							bdcCarichiRipartoCsv.setAutoriTitoloErroneo(df.format(iterable_element.getValore().doubleValue())+" %");
						}else if (KPI.KPI_DURATA_BRANI_DICHIARATI.equals(iterable_element.getKpi())) {
							bdcCarichiRipartoCsv.setDurataBraniDichiarati(df.format(iterable_element.getValore().doubleValue())+" %");
						}else if (KPI.KPI_BRANI_RD_TITOLI_ERRONEI.equals(iterable_element.getKpi())) {
							bdcCarichiRipartoCsv.setBraniRadioTitoloErroneo(df.format(iterable_element.getValore().doubleValue())+" %");
						}else if (KPI.KPI_AUTORI_RD_TITOLI_ERRONEI.equals(iterable_element.getKpi())) {
							bdcCarichiRipartoCsv.setAutoriRadioTitoloErroneo(df.format(iterable_element.getValore().doubleValue())+" %");
						}
					}
				}		
				
				csvList.add(bdcCarichiRipartoCsv);
			}
			try {
				if (csvList.size() != 0) {
					StreamingOutput so = null;
					CustomMappingStrategy<BdcCarichiRipartoCsv> customMappingStrategy = new CustomMappingStrategy<>(
							new BdcCarichiRipartoCsv().getMappingStrategy());
					customMappingStrategy.setType(BdcCarichiRipartoCsv.class);
					so = getStreamingOutput(csvList, customMappingStrategy);
					Response response = Response.ok(so, "text/csv")
							.header("Content-Disposition", "attachment; filename=\"" + "Lista Kpi.csv\"").build();
					return response;
				} else {
					return Response.status(500).entity("Non risulta nessun dato per il periodo selezionato").build();
				}
			} catch (Exception e) {
				return Response.status(500).entity("Non risulta nessun dato per il periodo selezionato").build();
			} finally {

			}

		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(500).build();
		}
	}

	private <BdcCaricoRipartizione> StreamingOutput getStreamingOutput(final List<BdcCaricoRipartizione> obj,
			final CustomMappingStrategy mappingStrategy) {
		StreamingOutput so = new StreamingOutput() {
			@Override
			public void write(OutputStream outputStream) throws IOException, WebApplicationException {
				OutputStreamWriter osw = new OutputStreamWriter(outputStream);

				StatefulBeanToCsv<BdcCaricoRipartizione> beanToCsv = new StatefulBeanToCsvBuilder<BdcCaricoRipartizione>(
						osw).withMappingStrategy(mappingStrategy).withSeparator(';').build();
				try {
					beanToCsv.write(obj);
					osw.flush();
					outputStream.flush();
				} catch (CsvDataTypeMismatchException e) {
				} catch (CsvRequiredFieldEmptyException e) {
				} finally {
					if (osw != null)
						osw.close();
				}
			}
		};
		return so;
	}

	@POST
	@Path("updateRipartizioni")
	@Produces("application/json")
	public Response updateRipartizioni(UpdateCarichiRipartizioneRequestDto carichiRipartizioneRequestDto) {
		bdcCaricoRipartizioneService.updateCaricoRipartizione(carichiRipartizioneRequestDto.getUsername(),
				carichiRipartizioneRequestDto.getCaricoRipartizione());
		return Response.ok().build();
	}

	@GET
	@Path("getStorico")
	@Produces("application/json")
	public Response getPeriodiRipartizione(@QueryParam(value = "idCaricoRipartizione") Integer idCaricoRipartizione) {
		List<BdcCaricoRipartizioneStoricoModifica> storico = bdcCaricoRipartizioneService
				.findAllStorico(idCaricoRipartizione);
		return Response.ok(gson.toJson(storico)).build();
	}

}
