package com.alkemytech.sophia.codman.rest;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.codman.dto.IdentifiedSongDTO;
import com.alkemytech.sophia.codman.dto.IdentifiedSongDsrDTO;
import com.alkemytech.sophia.codman.entity.KbManagement;
import com.alkemytech.sophia.codman.entity.SophiaCaOpera;
import com.alkemytech.sophia.codman.entity.SophiaIdsongOpera;
import com.alkemytech.sophia.codman.entity.SophiaIsrcOpera;
import com.alkemytech.sophia.codman.entity.SophiaIswcOpera;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.alkemytech.sophia.codman.guice.SophiaKbDataSource;
import com.alkemytech.sophia.common.kb.ConflictingCa;
import com.alkemytech.sophia.common.kb.ConflictingIdsong;
import com.alkemytech.sophia.common.kb.ConflictingIsrc;
import com.alkemytech.sophia.common.kb.ConflictingIswc;
import com.alkemytech.sophia.common.kb.KbException;
import com.alkemytech.sophia.common.kb.Learning;
import com.alkemytech.sophia.common.kb.LessonCa;
import com.alkemytech.sophia.common.kb.LessonIdsong;
import com.alkemytech.sophia.common.kb.LessonIsrc;
import com.alkemytech.sophia.common.kb.LessonIswc;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

@Singleton
@Path("kb")
public class KnowledgeBaseService {
	
	private static final String ERROR_NONE = "00";
	private static final String ERROR_MISSING_PARAMS = "11";
	private static final String ERROR_KB_MANAGEMENT = "12";
	private static final String ERROR_LEARNING = "13";
	private static final String ERROR_HOMONYMY = "14";
	
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Provider<EntityManager> mcmdbProvider;
	private final Provider<EntityManager> sophiaKbProvider;
	private final Gson gson;

	@Inject
	protected KnowledgeBaseService(@McmdbDataSource Provider<EntityManager> mcmdbProvider,
			@SophiaKbDataSource Provider<EntityManager> sophiaKbProvider,
			Gson gson) {
		super();
		this.mcmdbProvider = mcmdbProvider;
		this.sophiaKbProvider = sophiaKbProvider;
		this.gson = gson;
	}

	@GET
	@Path("searchCombana")
	@Produces("application/json")
	public Response search(@QueryParam("title") String title,
			@QueryParam("artists") String artists, 
			@QueryParam("codiceOpera") String codiceOpera,
			@DefaultValue("0") @QueryParam("first") int first,
			@DefaultValue("50") @QueryParam("last") int last) {
		final EntityManager entityManager = sophiaKbProvider.get();
		try {
			String separator = " where ";
			final StringBuffer sql = new StringBuffer()
				.append("select * from sophia_ca_opera");
			if (!StringUtils.isEmpty(title)) {
				sql.append(separator).append("title like ?");
				separator = " and ";
			}
			if (!StringUtils.isEmpty(artists)) {
				sql.append(separator).append("artists_names like ?");
				separator = " and ";
			}
			if (!StringUtils.isEmpty(codiceOpera)) {
				sql.append(separator).append("codice_opera = ?");
				separator = " and ";
			}
			sql.append(" limit ?, ?");
			int i = 1;
			final Query query = (Query) entityManager
				.createNativeQuery(sql.toString(), SophiaCaOpera.class);
			if (!StringUtils.isEmpty(title)) {
				query.setParameter(i ++, "%" + title + "%");
			}
			if (!StringUtils.isEmpty(artists)) {
				query.setParameter(i ++, "%" + artists + "%");
			}
			if (!StringUtils.isEmpty(codiceOpera)) {
				query.setParameter(i ++, codiceOpera);
			}
			query.setParameter(i ++, first);
			query.setParameter(i ++, 1 + last - first);
			@SuppressWarnings("unchecked")
			final List<SophiaCaOpera> results =
				(List<SophiaCaOpera>) query.getResultList();
			final PagedResult result = new PagedResult();
			if (null != results && !results.isEmpty()) {
				final int maxrows = last - first;
				final boolean hasNext = results.size() > maxrows;
				result.setRows(!hasNext ? results : results.subList(0, maxrows))
					.setMaxrows(maxrows)
					.setFirst(first)
					.setLast(hasNext ? last : first + results.size())
					.setHasNext(hasNext)
					.setHasPrev(first > 0);
			} else {
				result.setMaxrows(0)
					.setFirst(0)
					.setLast(0)
					.setHasNext(false)
					.setHasPrev(false);
			}
			return Response.ok(gson.toJson(result)).build();
		} catch (Exception e) {
			logger.error("search", e);
		}
		return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR).build();
	}
	
//	POST /cruscottoUtilizzazioni/rest/kb/identify HTTP/1.1
//	Host: localhost:8080
//	Content-Type: application/json
//	Cache-Control: no-cache
//	
//	{
//		"hashId":"00001527bc7c2d7da4f404bb4f3115b14339d662",
//		"title":"Bald Headed Teacher",
//		"artists":"Max Romeo|Max Romeo",
//		"siadaTitle":"BALD HEADED TEACHER",
//		"siadaArtists":"MAX ROMEO|MAX ROMEO",
//		"roles":"Composer|Artist",
//		"siaeWorkCode":"03303023300",
//		"identificationType": "automatic",
//		"dsrs":[
//			{
//				"idUtil":"GooglePlay_SUB_20160801_20160831_FR_1455796",
//				"hashId":"00001527bc7c2d7da4f404bb4f3115b14339d662",
//				"idDsr":"GooglePlay_SUB_20160801_20160831_FR",
//				"albumTitle":"Bald Headed Teacher",
//				"proprietaryId":"09ce75078cdef9a27e5036ba5eb9cf03",
//				"iswc":"",
//				"isrc":"",
//				"dsp":"google_play"
//			},
//			{
//				"idUtil":"GooglePlay_SUB_20160701_20160731_GB_1115046",
//				"hashId":"00001527bc7c2d7da4f404bb4f3115b14339d662",
//				"idDsr":"GooglePlay_SUB_20160701_20160731_GB",
//				"albumTitle":"Bald Headed Teacher",
//				"proprietaryId":"09ce75078cdef9a27e5036ba5eb9cf03",
//				"iswc":"",
//				"isrc":"",
//				"dsp":"google_play"
//			}
//		]
//	}
	
	@POST
	@Path("identify-deprecated")
	@Deprecated
	public Response identifyDeprecated(IdentifiedSongDTO identifiedSong) {
		logger.debug("identifyDeprecated: identifiedSong {}", identifiedSong);
		if (null == identifiedSong ||
				Strings.isNullOrEmpty(identifiedSong.getHashId()) ||
				Strings.isNullOrEmpty(identifiedSong.getTitle()) ||
				Strings.isNullOrEmpty(identifiedSong.getArtists()) ||
				Strings.isNullOrEmpty(identifiedSong.getSiadaTitle()) ||
				Strings.isNullOrEmpty(identifiedSong.getSiadaArtists()) ||
				Strings.isNullOrEmpty(identifiedSong.getSiaeWorkCode()) ||
				Strings.isNullOrEmpty(identifiedSong.getIdentificationType()) ||
				null == identifiedSong.getDsrs() || identifiedSong.getDsrs().isEmpty()) {
			logger.debug("identifyDeprecated: missing mandatory parameter(s)");
			return Response.status(HttpServletResponse.SC_BAD_REQUEST)
					.entity(gson.toJson(RestUtils.asMap("status", "KO",
							"errorCode", ERROR_MISSING_PARAMS,
							"errorMessage", "missing mandatory parameter(s)"))).build();
		}
		final EntityManager mcmdbEM = mcmdbProvider.get();
		final EntityManager sophiaKbEM = sophiaKbProvider.get();
		try {			
			// load knowledge base version
			final List<KbManagement> kbManagements = mcmdbEM
					.createQuery("select x from KbManagement x", 
							KbManagement.class)
					.getResultList();
			if (null == kbManagements || kbManagements.isEmpty()) {
				logger.debug("identifyDeprecated: KB_MANAGEMENT empty");
				return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR)
						.entity(gson.toJson(RestUtils.asMap("status", "KO",
								"errorCode", ERROR_KB_MANAGEMENT,
								"errorMessage", "table kb_management is empty"))).build();
			}
			final KbManagement kbManagement = kbManagements.get(0);
			if (null == kbManagement || null == kbManagement.getKbActiveVersion()) {
				logger.debug("identifyDeprecated: KB_MANAGEMENT empty");
				return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR)
						.entity(gson.toJson(RestUtils.asMap("status", "KO",
								"errorCode", ERROR_KB_MANAGEMENT,
								"errorMessage", "table kb_management is empty"))).build();
			}
			final int kbVersion = kbManagement.getKbActiveVersion();
			logger.debug("identifyDeprecated: kbVersion {}", kbVersion);

			// check learning in progress
			if (Boolean.TRUE.equals(kbManagement.getKbLearningInProgress())) {
				logger.debug("identifyDeprecated: learning in progress");
				return Response.status(HttpServletResponse.SC_FORBIDDEN)
						.entity(gson.toJson(RestUtils.asMap("status", "KO",
								"errorCode", ERROR_LEARNING,
								"errorMessage", "learning in progress"))).build();
			}
			
			// search sophia_ca_opera
			final List<String> artists = pipesToList(identifiedSong.getArtists());
			logger.debug("identifyDeprecated: artists {}", artists);
			final String hashTitle = getTitleHash(identifiedSong.getTitle());
			logger.debug("identifyDeprecated: hashTitle [" + hashTitle + "]");
			SophiaCaOpera sophiaCaOpera = null;
			final List<SophiaCaOpera> titoliUguali = sophiaKbEM
					.createQuery("select x from SophiaCaOpera x where x.hashTitle = :hashTitle",
							SophiaCaOpera.class)
					.setParameter("hashTitle", hashTitle)
					.getResultList();
			logger.debug("identifyDeprecated: titoliUguali {}", titoliUguali);
			if (null != titoliUguali) for (SophiaCaOpera titoloUguale : titoliUguali) {
				if (titoloUguale.getCodiceOpera().equalsIgnoreCase(identifiedSong.getSiaeWorkCode())) {
					// codice opera già esistente, non fare insert ma solo update
					sophiaCaOpera = titoloUguale;
				} else {
					// codice opera diverso, controlla presenza di autori omonimi
					final List<String> artistsNames = pipesToList(titoloUguale.getArtistsNames());
					for (String artist : artists) {
						if (artistsNames.contains(artist)) {	    		               						
							logger.debug("identifyDeprecated: presenza di autori omonimi in [" + identifiedSong.getTitle() + "]");
							
							// TODO artista presente in opera con codice diverso, cosa fare?
							
							return Response.status(HttpServletResponse.SC_BAD_REQUEST)
									.entity(gson.toJson(RestUtils.asMap("status", "KO",
											"errorCode", ERROR_HOMONYMY,
											"errorMessage", "same artist in multiple works with same title: {}", artist))).build();
						}
					}
				}
			}
			logger.debug("identifyDeprecated: sophiaCaOpera {}", sophiaCaOpera);

			// insert or update sophia_ca_opera
			final String origin = identifiedSong
					.getIdentificationType().replace("automatic", "fuzzy");
			logger.debug("identifyDeprecated: origin [" + origin + "]");
			final StringBuffer idutils = new StringBuffer();
			final StringBuffer iddsrs = new StringBuffer();
//			for (IdentifiedSongDsrDTO dsrRef : identifiedSong.getDsrs()) {
//				idutils.append('|').append(dsrRef.getIdUtil()).append('|').append(kbVersion);
//				iddsrs.append('|').append(dsrRef.getIdDsr()).append('|').append(kbVersion);
//			}
//			idutils.delete(0, 1);
//			iddsrs.delete(0, 1);
			idutils.append(identifiedSong.getDsrs().get(0).getIdUtil()).append('|').append(kbVersion);
			iddsrs.append(identifiedSong.getDsrs().get(0).getIdDsr()).append('|').append(kbVersion);		               				
			int artistsCount = 1 + StringUtils.countMatches(identifiedSong.getArtists(), "|");
			String originVector = StringUtils.repeat("|" + origin, artistsCount).substring(1);
			String idutilsMatrix = StringUtils.repeat("||" + idutils, artistsCount).substring(2);
			String iddsrsMatrix = StringUtils.repeat("||" + iddsrs, artistsCount).substring(2);
			logger.debug("identifyDeprecated: originVector [" + originVector + "]");
			logger.debug("identifyDeprecated: idutilsMatrix [" + idutilsMatrix + "]");
			logger.debug("identifyDeprecated: iddsrsMatrix [" + iddsrsMatrix + "]");

			sophiaKbEM.getTransaction().begin();
			
			if (null == sophiaCaOpera) { // not found --> insert
				
				sophiaCaOpera = new SophiaCaOpera();
				sophiaCaOpera.setCodiceOpera(identifiedSong.getSiaeWorkCode());
				sophiaCaOpera.setTitle(identifiedSong.getSiadaTitle());
				sophiaCaOpera.setHashTitle(hashTitle);
				sophiaCaOpera.setArtistsNames(identifiedSong.getSiadaArtists());
//   			sophiaCaOpera.setArtistsIpi(StringUtils.repeat("|0", artistsCount).substring(1));
				sophiaCaOpera.setAnOriginVector(originVector);
//   			sophiaCaOpera.setAiOriginVector(originVector);
				sophiaCaOpera.setAnIdutilSrcMatrix(idutilsMatrix);
//   			sophiaCaOpera.setAiIdutilSourceMatrix(idutilsMatrix);
				sophiaCaOpera.setAnIddsrSourceMatrix(iddsrsMatrix);
//   			sophiaCaOpera.setAiIddsrSourceMatrix(iddsrsMatrix);
				sophiaCaOpera.setKbVersion(kbVersion);
				sophiaCaOpera.setValid(1);
   				logger.debug("identifyDeprecated: inserting sophiaCaOpera {}", sophiaCaOpera);
   				
   				try {
   	   				sophiaKbEM.persist(sophiaCaOpera);
				} catch (Exception e) {
   					logger.debug("identifyDeprecated: impossibile fare insert nella knowledge base CA_OPERA per [" +
   							identifiedSong.getSiaeWorkCode() + "][" + identifiedSong.getTitle() + "]");
					logger.error("identifyDeprecated", e);					
					return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR)
							.entity(gson.toJson(RestUtils.asMap("status", "KO",
									"errorCode", KbException.Error.INSERT_CA.getCode(),
									"errorMessage", KbException.Error.INSERT_CA.getMessage(),
									"stackTrace", RestUtils.asString(e)))).build();
				}

			} else { // found (duplicate entry) --> update
				
				// merge artists
   				final List<String> oldArtistsNames = pipesToList(sophiaCaOpera.getArtistsNames());
   				final List<String> newArtistsNames = new ArrayList<String>();
   				for (String newArtistName : pipesToList(identifiedSong.getSiadaArtists())) {
   					if (!oldArtistsNames.contains(newArtistName)) { // new name 
   						newArtistsNames.add(newArtistName);
   					}
   				}
   				logger.debug("identifyDeprecated: newArtistsNames {}", newArtistsNames);
   				if (!newArtistsNames.isEmpty()) { // some artists missing
   					
   					artistsCount = newArtistsNames.size();
       				originVector = StringUtils.repeat("|" + origin, artistsCount);
       				idutilsMatrix = StringUtils.repeat("||" + idutils, artistsCount);
       				iddsrsMatrix = StringUtils.repeat("||" + iddsrs, artistsCount);
       				sophiaCaOpera.setArtistsNames(sophiaCaOpera.getArtistsNames() + "|" + listToPipes(newArtistsNames));
       				sophiaCaOpera.setAnOriginVector(sophiaCaOpera.getAnOriginVector() + originVector);
       				sophiaCaOpera.setAnIdutilSrcMatrix(sophiaCaOpera.getAnIdutilSrcMatrix() + idutilsMatrix);
       				sophiaCaOpera.setAnIddsrSourceMatrix(sophiaCaOpera.getAnIddsrSourceMatrix() + iddsrsMatrix);
       				sophiaCaOpera.setKbVersion(kbVersion);
       				sophiaCaOpera.setValid(1);
       				if (!StringUtils.isEmpty(sophiaCaOpera.getArtistsIpi())) {
           				sophiaCaOpera.setArtistsIpi(sophiaCaOpera.getArtistsIpi() + StringUtils.repeat("|0", artistsCount));
           				sophiaCaOpera.setAiOriginVector(sophiaCaOpera.getAiOriginVector() + originVector);
       					sophiaCaOpera.setAiIdutilSrcMatrix(sophiaCaOpera.getAiIdutilSrcMatrix() + idutilsMatrix);
       					sophiaCaOpera.setAiIddsrSourceMatrix(sophiaCaOpera.getAiIddsrSourceMatrix() + iddsrsMatrix);
       				}
       				logger.debug("identifyDeprecated: updating sophiaCaOpera {}", sophiaCaOpera);
       				
       				try {
           				sophiaKbEM.merge(sophiaCaOpera);
					} catch (Exception e) {
       					logger.debug("identifyDeprecated: impossibile fare update nella knowledge base CA_OPERA per [" +
       							identifiedSong.getSiaeWorkCode() + "][" + identifiedSong.getTitle() + "]");
						logger.error("identifyDeprecated", e);
						return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR)
								.entity(gson.toJson(RestUtils.asMap("status", "KO",
										"errorCode", KbException.Error.UPDATE_CA.getCode(),
										"errorMessage", KbException.Error.UPDATE_CA.getMessage(),
										"stackTrace", RestUtils.asString(e)))).build();
					}
       			}
			}
						
			// insert idsong, iswc & isrc
			final Set<String> uniqueIdsongs = new HashSet<String>();
			final Set<String> uniqueIswc = new HashSet<String>();
			final Set<String> uniqueIsrc = new HashSet<String>();
			for (IdentifiedSongDsrDTO dsrRef : identifiedSong.getDsrs()) {

				// insert idsong_sophia
   				final String idsongSophia = getIdSong(identifiedSong, dsrRef);
   				if (uniqueIdsongs.add(idsongSophia)) { // the set did not already contain the specified idsong
   					
   					SophiaIdsongOpera sophiaIdsongOpera = sophiaKbEM
   							.find(SophiaIdsongOpera.class, idsongSophia);
   	   				logger.debug("identifyDeprecated: sophiaIdsongOpera {}", sophiaIdsongOpera);
   					if (null == sophiaIdsongOpera) {
   	   					sophiaIdsongOpera = new SophiaIdsongOpera();
   	   					sophiaIdsongOpera.setIdsongSophia(idsongSophia);
   	   					sophiaIdsongOpera.setCodiceOpera(identifiedSong.getSiaeWorkCode());
   	   					sophiaIdsongOpera.setOrigin(origin);
   	   					sophiaIdsongOpera.setIdutilSource(dsrRef.getIdUtil());
   	   					sophiaIdsongOpera.setIddsrSource(dsrRef.getIdDsr());
   	   					sophiaIdsongOpera.setKbVersion(kbVersion);
   	   					sophiaIdsongOpera.setValid(1);
   	   	   				logger.debug("identifyDeprecated: inserting sophiaIdsongOpera {}", sophiaIdsongOpera);

   	   	   				try {
   	   	   	   				sophiaKbEM.persist(sophiaIdsongOpera);
   						} catch (Exception e) {
   	       					logger.debug("identifyDeprecated: impossibile fare insert nella knowledge base IDSONG per [" +
   	       							identifiedSong.getSiaeWorkCode() + "][" + idsongSophia + "]");
   							logger.error("identifyDeprecated", e);
   							return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR)
   									.entity(gson.toJson(RestUtils.asMap("status", "KO",
											"errorCode", KbException.Error.INSERT_IDSONG.getCode(),
											"errorMessage", KbException.Error.INSERT_IDSONG.getMessage(),
   											"stackTrace", RestUtils.asString(e)))).build();
   						}
   					}
   				}
   				
   				// insert iswc
   				if (!StringUtils.isEmpty(dsrRef.getIswc())) {
       				if (uniqueIswc.add(dsrRef.getIswc())) { // the set did not already contain the specified iswc
       					
       					SophiaIswcOpera sophiaIswcOpera = sophiaKbEM
       							.find(SophiaIswcOpera.class, dsrRef.getIswc());
       	   				logger.debug("identifyDeprecated: sophiaIswcOpera {}", sophiaIswcOpera);
       					if (null == sophiaIswcOpera) {
       						sophiaIswcOpera = new SophiaIswcOpera();
           					sophiaIswcOpera.setIswc(dsrRef.getIswc());
           					sophiaIswcOpera.setCodiceOpera(identifiedSong.getSiaeWorkCode());
           					sophiaIswcOpera.setOrigin(origin);
           					sophiaIswcOpera.setIdutilSource(dsrRef.getIdUtil());
           					sophiaIswcOpera.setIddsrSource(dsrRef.getIdDsr());
           					sophiaIswcOpera.setKbVersion(kbVersion);
           					sophiaIswcOpera.setValid(1);
           	   				logger.debug("identifyDeprecated: inserting sophiaIswcOpera {}", sophiaIswcOpera);

           	   				try {
               	   				sophiaKbEM.persist(sophiaIswcOpera);
    						} catch (Exception e) {
               					logger.debug("identifyDeprecated: impossibile fare insert nella knowledge base ISWC per [" +
               							identifiedSong.getSiaeWorkCode() + "][" + dsrRef.getIswc() + "]");
    							logger.error("identifyDeprecated", e);
    							return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR)
    									.entity(gson.toJson(RestUtils.asMap("status", "KO",
    											"errorCode", KbException.Error.INSERT_ISWC.getCode(),
    											"errorMessage", KbException.Error.INSERT_ISWC.getMessage(),
    											"stackTrace", RestUtils.asString(e)))).build();
    						}
       					}
       				}
   				}
   				
   				// insert isrc
   				if (!StringUtils.isEmpty(dsrRef.getIsrc())) {
       				if (uniqueIsrc.add(dsrRef.getIsrc())) { // the set did not already contain the specified isrc
       					
       					SophiaIsrcOpera sophiaIsrcOpera = sophiaKbEM
       							.find(SophiaIsrcOpera.class, dsrRef.getIsrc());
       	   				logger.debug("identifyDeprecated: sophiaIsrcOpera {}", sophiaIsrcOpera);
       					if (null == sophiaIsrcOpera) {
       						sophiaIsrcOpera = new SophiaIsrcOpera();
           					sophiaIsrcOpera.setIsrc(dsrRef.getIsrc());
           					sophiaIsrcOpera.setCodiceOpera(identifiedSong.getSiaeWorkCode());
           					sophiaIsrcOpera.setOrigin(origin);
           					sophiaIsrcOpera.setIdutilSource(dsrRef.getIdUtil());
           					sophiaIsrcOpera.setIddsrSource(dsrRef.getIdDsr());
           					sophiaIsrcOpera.setKbVersion(kbVersion);
           					sophiaIsrcOpera.setValid(1);
           	   				logger.debug("identifyDeprecated: inserting sophiaIsrcOpera {}", sophiaIsrcOpera);

           	   				try {
    							sophiaKbEM.persist(sophiaIsrcOpera);
    						} catch (Exception e) {
               					logger.debug("identifyDeprecated: impossibile fare insert nella knowledge base ISRC per [" +
               							identifiedSong.getSiaeWorkCode() + "][" + dsrRef.getIsrc() + "]");
    							logger.error("", e);
    							return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR)
    									.entity(gson.toJson(RestUtils.asMap("status", "KO",
    											"errorCode", KbException.Error.INSERT_ISRC.getCode(),
    											"errorMessage", KbException.Error.INSERT_ISRC.getMessage(),
    											"stackTrace", RestUtils.asString(e)))).build();
    						}
       					}
       				}
   				}
   				
			}

			sophiaKbEM.getTransaction().commit();
						
			logger.debug("identifyDeprecated: knowledge base aggiornata per [" + identifiedSong.getSiaeWorkCode() + "][" + identifiedSong.getTitle() + "]");					

			return Response.ok(gson.toJson(RestUtils
					.asMap("status", "OK", "errorCode", ERROR_NONE))).build();
		} catch (Exception e) {
			if (sophiaKbEM.getTransaction().isActive()) {
				sophiaKbEM.getTransaction().rollback();
			}
			logger.debug("identifyDeprecated: impossibile aggiornare la knowledge base per [" + identifiedSong.getSiaeWorkCode() + "][" + identifiedSong.getTitle() + "]");
			logger.error("identifyDeprecated", e);
			return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR)
					.entity(gson.toJson(RestUtils.asMap("status", "KO",
							"errorCode", KbException.Error.GENERIC.getCode(),
							"errorMessage", KbException.Error.GENERIC.getMessage(),
							"stackTrace", RestUtils.asString(e)))).build();
		}
	}
	
	@POST
	@Path("identify")
	public Response identify(IdentifiedSongDTO identifiedSong) {
		logger.debug("identify: identifiedSong {}", identifiedSong);
		if (null == identifiedSong ||
				Strings.isNullOrEmpty(identifiedSong.getHashId()) ||
				Strings.isNullOrEmpty(identifiedSong.getTitle()) ||
				Strings.isNullOrEmpty(identifiedSong.getArtists()) ||
				Strings.isNullOrEmpty(identifiedSong.getSiadaTitle()) ||
				Strings.isNullOrEmpty(identifiedSong.getSiadaArtists()) ||
				Strings.isNullOrEmpty(identifiedSong.getSiaeWorkCode()) ||
				Strings.isNullOrEmpty(identifiedSong.getIdentificationType()) ||
				null == identifiedSong.getDsrs() || identifiedSong.getDsrs().isEmpty()) {
			logger.debug("identify: missing mandatory parameter(s)");
			return Response.status(HttpServletResponse.SC_BAD_REQUEST)
					.entity(gson.toJson(RestUtils.asMap("status", "KO",
							"errorCode", ERROR_MISSING_PARAMS,
							"errorMessage", "missing mandatory parameter(s)"))).build();
		}
		final EntityManager mcmdbEM = mcmdbProvider.get();
		final EntityManager sophiaKbEM = sophiaKbProvider.get();
		try {
		
			// load knowledge base version
			final List<KbManagement> kbManagements = mcmdbEM
					.createQuery("select x from KbManagement x", 
							KbManagement.class)
					.getResultList();
			final KbManagement kbManagement = null == kbManagements ||
					kbManagements.isEmpty() ? null : kbManagements.get(0);
			if (null == kbManagement || null == kbManagement.getKbActiveVersion()) {
				logger.debug("identify: KB_MANAGEMENT empty");
				return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR)
						.entity(gson.toJson(RestUtils.asMap("status", "KO",
								"errorCode", ERROR_KB_MANAGEMENT,
								"errorMessage", "table kb_management is empty"))).build();
			}
			final int kbVersion = 1 + kbManagement.getKbActiveVersion();
			logger.debug("identify: kbVersion {}", kbVersion);

			// check learning in progress
			if (Boolean.TRUE.equals(kbManagement.getKbLearningInProgress())) {
				logger.debug("identify: learning in progress");
				return Response.status(HttpServletResponse.SC_FORBIDDEN)
						.entity(gson.toJson(RestUtils.asMap("status", "KO",
								"errorCode", ERROR_LEARNING,
								"errorMessage", "learning in progress"))).build();
			}
			
			// begin MCMDB transaction
			sophiaKbEM.getTransaction().begin();
			
			// load learning library
			final Learning learning = new Learning()
					.setConnection(sophiaKbEM.unwrap(Connection.class))
					.setKbVersion(kbVersion);

			// combana
			LessonCa lessonCa = new LessonCa();
			lessonCa.setCodiceOpera(identifiedSong.getSiaeWorkCode());
			lessonCa.setTitle(identifiedSong.getSiadaTitle());
//			lessonCa.setHashTitle(Learning.getHashTitle(identifiedSong.getTitle()));
			lessonCa.setArtistsNames(identifiedSong.getSiadaArtists());
//			lessonCa.setArtistsIpi(null);
			lessonCa.setOrigin(identifiedSong.getIdentificationType().replace("automatic", "fuzzy"));
			lessonCa.setIdutilSource(identifiedSong.getDsrs().get(0).getIdUtil());
			lessonCa.setIddsrSource(identifiedSong.getDsrs().get(0).getIdDsr());
			final List<ConflictingCa> conflictingCas = learning.learn(lessonCa);
			logger.debug("identify: conflictingCas {}", conflictingCas);
			
			// idsong, iswc & isrc
			final Set<String> uniqueIdsongs = new HashSet<String>();
			final Set<String> uniqueIswc = new HashSet<String>();
			final Set<String> uniqueIsrc = new HashSet<String>();
			for (IdentifiedSongDsrDTO dsrRef : identifiedSong.getDsrs()) {

				// insert idsong_sophia
				// WARNING: maybe sisdaTilte and siadaArtists should be used instead
				final String idsongSophia = Learning.getIdSong(dsrRef.getAlbumTitle(),
						dsrRef.getProprietaryId(), identifiedSong.getTitle(), identifiedSong.getArtists(),
						identifiedSong.getRoles(), dsrRef.getIswc(), dsrRef.getIsrc(), dsrRef.getDsp());
   				if (uniqueIdsongs.add(idsongSophia)) { // the set did not already contain the specified idsong
   					
   					final LessonIdsong lessonIdsong = new LessonIdsong();
   					lessonIdsong.setIdsongSophia(idsongSophia);
   					lessonIdsong.setCodiceOpera(identifiedSong.getSiaeWorkCode());
   					lessonIdsong.setOrigin(identifiedSong.getIdentificationType().replace("automatic", "fuzzy"));
   					lessonIdsong.setIdutilSource(dsrRef.getIdUtil());
   					lessonIdsong.setIddsrSource(dsrRef.getIdDsr());
   					lessonIdsong.setIddspSource(dsrRef.getDsp());
   					final ConflictingIdsong conflictingIdsong = learning.learn(lessonIdsong);
   					logger.debug("identify: conflictingIdsong {}", conflictingIdsong);

   				}
   				
   				// insert iswc
   				if (!StringUtils.isEmpty(dsrRef.getIswc())) {
       				if (uniqueIswc.add(dsrRef.getIswc())) { // the set did not already contain the specified iswc
       					
       					final LessonIswc lessonIswc = new LessonIswc();
       					lessonIswc.setIswc(dsrRef.getIswc());
       					lessonIswc.setCodiceOpera(identifiedSong.getSiaeWorkCode());
       					lessonIswc.setOrigin(identifiedSong.getIdentificationType().replace("automatic", "fuzzy"));
       					lessonIswc.setIdutilSource(dsrRef.getIdUtil());
       					lessonIswc.setIddsrSource(dsrRef.getIdDsr());
       					final ConflictingIswc conflictingIswc = learning.learn(lessonIswc);
       					logger.debug("identify: conflictingIswc {}", conflictingIswc);

       				}
   				}
   
   				// insert isrc
   				if (!StringUtils.isEmpty(dsrRef.getIsrc())) {
       				if (uniqueIsrc.add(dsrRef.getIsrc())) { // the set did not already contain the specified isrc
       					
       					final LessonIsrc lessonIsrc = new LessonIsrc();
       					lessonIsrc.setIsrc(dsrRef.getIsrc());
       					lessonIsrc.setCodiceOpera(identifiedSong.getSiaeWorkCode());
       					lessonIsrc.setOrigin(identifiedSong.getIdentificationType().replace("automatic", "fuzzy"));
       					lessonIsrc.setIdutilSource(dsrRef.getIdUtil());
       					lessonIsrc.setIddsrSource(dsrRef.getIdDsr());
       					final ConflictingIsrc conflictingIsrc = learning.learn(lessonIsrc);
       					logger.debug("identify: conflictingIsrc {}", conflictingIsrc);

       				}
   				}
   				
			}
			
			// commit MCMDB transaction
			sophiaKbEM.getTransaction().commit();

			logger.debug("identify: knowledge base aggiornata per [{}][{}]", identifiedSong.getSiaeWorkCode(), identifiedSong.getTitle());					

			return Response.ok(gson.toJson(RestUtils
					.asMap("status", "OK", "errorCode", ERROR_NONE))).build();
			
		} catch (KbException e) {
			// rollback MCMDB transaction
			if (sophiaKbEM.getTransaction().isActive()) {
				sophiaKbEM.getTransaction().rollback();
			}
			logger.debug("identify: impossibile aggiornare la knowledge base per [{}][{}]", identifiedSong.getSiaeWorkCode(), identifiedSong.getTitle());
			logger.error("identify", e);
			return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR)
					.entity(gson.toJson(RestUtils.asMap("status", "KO",
							"errorCode", e.getError().getCode(),
							"errorMessage", e.getError().getMessage(),
							"stackTrace", RestUtils.asString(e)))).build();
		} catch (Exception e) {
			// rollback MCMDB transaction
			if (sophiaKbEM.getTransaction().isActive()) {
				sophiaKbEM.getTransaction().rollback();
			}
			logger.debug("identify: impossibile aggiornare la knowledge base per [{}][{}]", identifiedSong.getSiaeWorkCode(), identifiedSong.getTitle());
			logger.error("identify", e);
			return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR)
					.entity(gson.toJson(RestUtils.asMap("status", "KO",
							"errorCode", KbException.Error.GENERIC.getCode(),
							"errorMessage", KbException.Error.GENERIC.getMessage(),
							"stackTrace", RestUtils.asString(e)))).build();
		}
	}
	
	private List<String> pipesToList(String pipes) {
		return StringUtils.isEmpty(pipes) ? new ArrayList<String>() :
			Arrays.asList(pipes.split("\\|"));
	}

	private String listToPipes(List<String> list) {
		final StringBuffer result = new StringBuffer();
		for (String elem : list) {
			result.append('|').append(elem);
		}
		return result.substring(1);
	}
	
	private String getTitleHash(String title) {
		try {
			return Hex.encodeHexString(MessageDigest.getInstance("MD5")
					.digest(title.getBytes("UTF-8")));
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			// never happens
			return null;
		}
	}
	
	private String getIdSong(IdentifiedSongDTO identifiedSong, IdentifiedSongDsrDTO identifiedSongDsr) {
		try {
			// WARNING: maybe sisdaTilte and siadaArtists should be used instead
			final String data = identifiedSongDsr.getAlbumTitle() +
					identifiedSongDsr.getProprietaryId() + 
					identifiedSong.getTitle() +
					identifiedSong.getArtists() +
					identifiedSong.getRoles() +
					identifiedSongDsr.getIswc() +
					identifiedSongDsr.getIsrc() +
					identifiedSongDsr.getDsp();
			return Hex.encodeHexString(MessageDigest.getInstance("MD5")
					.digest(data.getBytes("UTF-8")));
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			// never happens
			return null;
		}
	}
	
}
