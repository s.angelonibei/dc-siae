package com.alkemytech.sophia.codman.broadcasting;

import com.alkemytech.sophia.broadcasting.consts.Consts;
import com.alkemytech.sophia.broadcasting.dto.*;
import com.alkemytech.sophia.broadcasting.model.BdcKpiEntity;
import com.alkemytech.sophia.broadcasting.service.interfaces.BdcIndicatoreDTOService;
import com.alkemytech.sophia.broadcasting.service.interfaces.BdcInformationFileDTOService;
import com.alkemytech.sophia.broadcasting.service.interfaces.KpiService;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import java.math.BigDecimal;
import java.util.*;

@Singleton
@Path("broadcastingKPI")
public class BroadcastingKPIService {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final KpiService kpiService;
    private final Gson gson;
    private final BdcIndicatoreDTOService bdcIndicatoreDTOService;
    private final BdcInformationFileDTOService bdcInformationFileDTOService;

    @Inject
    protected BroadcastingKPIService(
            BdcIndicatoreDTOService bdcIndicatoreDTOService,
            BdcInformationFileDTOService bdcInformationFileDTOService,
            Gson gson, KpiService kpiService) {
        super();
        this.gson = gson;
        this.kpiService = kpiService;
        this.bdcIndicatoreDTOService = bdcIndicatoreDTOService;
        this.bdcInformationFileDTOService = bdcInformationFileDTOService;
    }

    @POST
    @Path("fe/monitoraggio/kpi")
    @Produces("application/json")
    public Response getKPI(RequestMonitoraggioKPIDTO monitoraggioKPI) {
        if (monitoraggioKPI != null &&
                monitoraggioKPI.getRequestData() != null &&
                monitoraggioKPI.getRequestData().getCanali() != null &&
                monitoraggioKPI.getRequestData().getCanali().size() > 0 &&
                monitoraggioKPI.getRequestData().getAnno() != 0 &&
                monitoraggioKPI.getRequestData().getMesi() != null &&
                monitoraggioKPI.getRequestData().getMesi().size() > 0 &&
                StringUtils.isNotEmpty(monitoraggioKPI.getRequestData().getTipoCanali())
                ) {
            ResponseMonitoraggioKPIDTO response = new ResponseMonitoraggioKPIDTO();
            response.setHeader(monitoraggioKPI.getHeader());
            ResponseMonitoraggioKPIDataDTO responseMonitoraggioKPIDataDTO = new ResponseMonitoraggioKPIDataDTO();
            responseMonitoraggioKPIDataDTO.setInput(monitoraggioKPI.getRequestData());
            response.setResponseData(responseMonitoraggioKPIDataDTO);
            try {
                Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
                Set<ConstraintViolation<RequestMonitoraggioKPIDTO>> violations = validator.validate(monitoraggioKPI);
                if (!violations.isEmpty()) {
                    List<String> errorLog = new ArrayList<>();
                    for (ConstraintViolation<RequestMonitoraggioKPIDTO> violation : violations) {
                        errorLog.add(violation.getPropertyPath() + " " + violation.getMessage());
                    }
                    EsitoDTO esito = new EsitoDTO();
                    esito.setCodice("10");
                    esito.setDescrizioneEsito("Input error");
                    esito.setLog(errorLog);
                    response.setEsito(esito);
                    String responseString = gson.toJson(response);
                    logger.error(responseString);
                    return Response.ok(responseString).build();
                }

                Date lastKpiUpdate = null;
                HashMap<String, IndicatoreDTO> mapIndicatori = new HashMap<>();
                for (IndicatoreDTO indicatore : bdcIndicatoreDTOService.listaInd(monitoraggioKPI)) {
                    mapIndicatori.put(indicatore.getIndicatore(), indicatore);
                    if (lastKpiUpdate == null || indicatore.getLastUpdate().after(lastKpiUpdate)) {
                        lastKpiUpdate = indicatore.getLastUpdate();
                    }
                }

                monitoraggioKPI.getRequestData().getCanali().indexOf("TELEVISIONE");

                Integer anno = monitoraggioKPI.getRequestData().getAnno();
                List<Short> mesi = monitoraggioKPI.getRequestData().getMesi();

                if (monitoraggioKPI.getRequestData().getTipoCanali().contains("TELEVISIONE")) {
                    MonitoraggioKPIOutputDTO monitoraggioKPIOutputDTO = new MonitoraggioKPIOutputDTO();
                    monitoraggioKPIOutputDTO.setKpiAttesi(Consts.KPIATTESITV);
                    List<KPI> listaKPI = new ArrayList<>();

                    long durataFilm = 0;
                    long durataPubblicita = 0;
                    long durataTrasmissioni = 0;
                    long durataGeneriVuoti = 0;
                    if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_FILM)) {
                        durataFilm = mapIndicatori.get(BdcKpiEntity.DURATA_FILM).getValore();
                    }
                    if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_GENERI_VUOTI)) {
                        durataGeneriVuoti = mapIndicatori.get(BdcKpiEntity.DURATA_GENERI_VUOTI).getValore();
                    }
                    if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_PUBBLICITA)) {
                        durataPubblicita = mapIndicatori.get(BdcKpiEntity.DURATA_PUBBLICITA).getValore();
                    }
                    if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_TRASMISSIONE)) {
                        durataTrasmissioni = mapIndicatori.get(BdcKpiEntity.DURATA_TRASMISSIONE).getValore();
                    }
                    if (monitoraggioKPI.getRequestData().getPlatform() != null && monitoraggioKPI.getRequestData().getPlatform().equals("RIP")) {

                        listaKPI.add(kpiService.calcolaKpiCopertura(durataFilm, durataGeneriVuoti, durataPubblicita, durataTrasmissioni, anno, mesi));
                        listaKPI.add(kpiService.calcolaKpiDurataMusicaDichiarata(mapIndicatori));
                        listaKPI.add(kpiService.calcolaKpiBraniTitoliErronei(mapIndicatori));
                        listaKPI.add(kpiService.calcolaKpiFilmTitoliErronei(mapIndicatori));
                        listaKPI.add(kpiService.calcolaKpiAutoriTitoliErronei(mapIndicatori));
                        monitoraggioKPIOutputDTO.setKpiAttesi(5);
                    } else {
                        listaKPI.add(kpiService.calcolaKpiCopertura(durataFilm, durataGeneriVuoti, durataPubblicita, durataTrasmissioni, anno, mesi));
                        listaKPI.add(kpiService.calcolaKpiRecordScartati(mapIndicatori));
                        listaKPI.add(kpiService.calcolaKpiRecordInRitardo(mapIndicatori));
                        if (monitoraggioKPI.getRequestData().getPlatform() != null) {
                            listaKPI.add(kpiService.calcolaKpiDurataMusicaDichiarata(mapIndicatori));
                            listaKPI.add(kpiService.calcolaKpiBraniTitoliErronei(mapIndicatori));
                            listaKPI.add(kpiService.calcolaKpiAutoriTitoliErronei(mapIndicatori));
                            listaKPI.add(kpiService.calcolaKpiFilmTitoliErronei(mapIndicatori));
                            listaKPI.add(kpiService.calcolaKpiProgrammiTitoliErronei(mapIndicatori));
                            listaKPI.add(kpiService.calcolaKpiMusicaEccedenteTrasmissione(mapIndicatori));
                            monitoraggioKPIOutputDTO.setKpiAttesi(Consts.KPIATTESITVBKO);
                        }
                    }
                    monitoraggioKPIOutputDTO.setLastUpdate(lastKpiUpdate);
                    monitoraggioKPIOutputDTO.setListaKPI(listaKPI);
                    monitoraggioKPIOutputDTO.setFileDaElaborare(bdcInformationFileDTOService.fileDTOS(monitoraggioKPI));
                    responseMonitoraggioKPIDataDTO.setOutput(monitoraggioKPIOutputDTO);
                } else if (monitoraggioKPI.getRequestData().getTipoCanali().contains("RADIO")) {
                    MonitoraggioKPIOutputDTO monitoraggioKPIOutputDTO = new MonitoraggioKPIOutputDTO();
                    monitoraggioKPIOutputDTO.setKpiAttesi(Consts.KPIATTESIRADIO);
                    List<KPI> listaKPI = new ArrayList<KPI>();

                    long durataMusiche = 0;
                    if (mapIndicatori.containsKey(BdcKpiEntity.DURATA_BRANI)) {
                        durataMusiche = mapIndicatori.get(BdcKpiEntity.DURATA_BRANI).getValore();
                    }

                    if (monitoraggioKPI.getRequestData().getPlatform() != null && monitoraggioKPI.getRequestData().getPlatform().equals("RIP")) {
                        listaKPI.add(kpiService.calcolaKpiDurataBraniDichiaratiRD(durataMusiche, anno, mesi));
                        listaKPI.add(kpiService.calcolaKpiBraniTitoliErroneiRD(mapIndicatori));
                        listaKPI.add(kpiService.calcolaKpiAutoriTitoliErroneiRD(mapIndicatori));
                        monitoraggioKPIOutputDTO.setKpiAttesi(3);
                    } else {
                        listaKPI.add(kpiService.calcolaKpiRecordScartati(mapIndicatori));
                        listaKPI.add(kpiService.calcolaKpiRecordInRitardo(mapIndicatori));
                        if (monitoraggioKPI.getRequestData().getPlatform() != null) {
                            listaKPI.add(kpiService.calcolaKpiDurataBraniDichiaratiRD(durataMusiche, anno, mesi));
                            listaKPI.add(kpiService.calcolaKpiBraniTitoliErroneiRD(mapIndicatori));
                            listaKPI.add(kpiService.calcolaKpiAutoriTitoliErroneiRD(mapIndicatori));
                            monitoraggioKPIOutputDTO.setKpiAttesi(Consts.KPIATTESIRADIOBKO);
                        }
                    }
                    monitoraggioKPIOutputDTO.setLastUpdate(lastKpiUpdate);
                    monitoraggioKPIOutputDTO.setListaKPI(listaKPI);
                    monitoraggioKPIOutputDTO.setFileDaElaborare(bdcInformationFileDTOService.fileDTOS(monitoraggioKPI));
                    responseMonitoraggioKPIDataDTO.setOutput(monitoraggioKPIOutputDTO);
                }
                EsitoDTO esito = new EsitoDTO();
                esito.setCodice("00");
                esito.setDescrizioneEsito("OK");
                response.setEsito(esito);
                
                return Response.ok(gson.toJson(response)).build();
            } catch (Throwable e) {
                logger.error(e.getMessage());
                EsitoDTO esitoDTO = new EsitoDTO();
                esitoDTO.setCodice("500");
                esitoDTO.setDescrizioneEsito("KO");
                List<String> log = new ArrayList<>();
                for (StackTraceElement ste : e.getStackTrace()) {
                    log.add(ste.toString());
                    logger.error(ste.toString());
                }
                esitoDTO.setLog(log);
                response.setEsito(esitoDTO);
                return Response.status(500).entity(response).build();
            }
        } else {
        	
            ResponseMonitoraggioKPIDTO response = new ResponseMonitoraggioKPIDTO();
            response.setHeader(monitoraggioKPI.getHeader());
            ResponseMonitoraggioKPIDataDTO responseMonitoraggioKPIDataDTO = new ResponseMonitoraggioKPIDataDTO();
            responseMonitoraggioKPIDataDTO.setInput(monitoraggioKPI.getRequestData());
            response.setResponseData(responseMonitoraggioKPIDataDTO);
            List<KPI> listaKPI = new ArrayList<KPI>();
            if (monitoraggioKPI.getRequestData().getTipoCanali().contains("TELEVISIONE")) {
                MonitoraggioKPIOutputDTO monitoraggioKPIOutputDTO = new MonitoraggioKPIOutputDTO();
                if (monitoraggioKPI.getRequestData().getPlatform() != null && monitoraggioKPI.getRequestData().getPlatform().equals("RIP")) {
                    listaKPI.add(new KPI(true, KPI.KPI_COPERTURA, KPI.DESC_KPI_COPERTURA, new BigDecimal(0)));
                    listaKPI.add(new KPI(true, KPI.KPI_DURATA_MUSICA_DICHIARATA, KPI.DESC_KPI_DURATA_MUSICA_DICHIARATA, new BigDecimal(0)));
                    listaKPI.add(new KPI(true, KPI.KPI_BRANI_TITOLI_ERRONEI, KPI.DESC_KPI_BRANI_TITOLI_ERRONEI, new BigDecimal(0)));
                    listaKPI.add(new KPI(true, KPI.KPI_FILM_TITOLI_ERRONEI, KPI.DESC_KPI_FILM_TITOLI_ERRONEI, new BigDecimal(0)));
                    listaKPI.add(new KPI(true, KPI.KPI_AUTORI_TITOLI_ERRONEI, KPI.DESC_KPI_AUTORI_TITOLI_ERRONEI, new BigDecimal(0)));
                    monitoraggioKPIOutputDTO.setKpiAttesi(5);
                } else {
                    monitoraggioKPIOutputDTO.setKpiAttesi(Consts.KPIATTESITV);
                    listaKPI.add(new KPI(true, KPI.KPI_COPERTURA, KPI.DESC_KPI_COPERTURA, new BigDecimal(0)));
                    listaKPI.add(new KPI(true, KPI.KPI_RECORD_ACCETTATI, KPI.DESC_KPI_RECORD_ACCETTATI, new BigDecimal(0)));
                    listaKPI.add(new KPI(true, KPI.KPI_RECORD_IN_TEMPO, KPI.DESC_KPI_RECORD_IN_TEMPO, new BigDecimal(0)));
                    if (monitoraggioKPI.getRequestData().getPlatform() != null) {
                        listaKPI.add(new KPI(true, KPI.KPI_DURATA_MUSICA_DICHIARATA, KPI.DESC_KPI_DURATA_MUSICA_DICHIARATA, new BigDecimal(0)));
                        listaKPI.add(new KPI(true, KPI.KPI_BRANI_TITOLI_ERRONEI, KPI.DESC_KPI_BRANI_TITOLI_ERRONEI, new BigDecimal(0)));
                        listaKPI.add(new KPI(true, KPI.KPI_AUTORI_TITOLI_ERRONEI, KPI.DESC_KPI_AUTORI_TITOLI_ERRONEI, new BigDecimal(0)));
                        listaKPI.add(new KPI(true, KPI.KPI_FILM_TITOLI_ERRONEI, KPI.DESC_KPI_FILM_TITOLI_ERRONEI, new BigDecimal(0)));
                        listaKPI.add(new KPI(true, KPI.KPI_PROGRAMMI_TITOLI_ERRONEI, KPI.DESC_KPI_PROGRAMMI_TITOLI_ERRONEI, new BigDecimal(0)));
                        listaKPI.add(new KPI(true, KPI.KPI_TRASMISSIONI_MUSICHE_ECCEDENTI, KPI.DESC_KPI_TRASMISSIONI_MUSICHE_ECCEDENTI, new BigDecimal(0)));
                        monitoraggioKPIOutputDTO.setKpiAttesi(Consts.KPIATTESITVBKO);
                    }
                }
                monitoraggioKPIOutputDTO.setLastUpdate(new Date());
                monitoraggioKPIOutputDTO.setListaKPI(listaKPI);
                monitoraggioKPIOutputDTO.setFileDaElaborare(bdcInformationFileDTOService.fileDTOS(monitoraggioKPI));
                responseMonitoraggioKPIDataDTO.setOutput(monitoraggioKPIOutputDTO);
            } else if (monitoraggioKPI.getRequestData().getTipoCanali().contains("RADIO")) {
                MonitoraggioKPIOutputDTO monitoraggioKPIOutputDTO = new MonitoraggioKPIOutputDTO();
                if (monitoraggioKPI.getRequestData().getPlatform() != null && monitoraggioKPI.getRequestData().getPlatform().equals("RIP")) {
                    listaKPI.add(new KPI(true, KPI.KPI_DURATA_BRANI_DICHIARATI, KPI.DESC_KPI_DURATA_BRANI_DICHIARATI, new BigDecimal(0)));
                    listaKPI.add(new KPI(true, KPI.KPI_BRANI_RD_TITOLI_ERRONEI, KPI.DESC_KPI_BRANI_RD_TITOLI_ERRONEI, new BigDecimal(0)));
                    listaKPI.add(new KPI(true, KPI.KPI_AUTORI_RD_TITOLI_ERRONEI, KPI.DESC_KPI_AUTORI_RD_TITOLI_ERRONEI, new BigDecimal(0)));
                } else {
                    monitoraggioKPIOutputDTO.setKpiAttesi(Consts.KPIATTESIRADIO);
                    listaKPI.add(new KPI(true, KPI.KPI_RECORD_ACCETTATI, KPI.DESC_KPI_RECORD_ACCETTATI, new BigDecimal(0)));
                    listaKPI.add(new KPI(true, KPI.KPI_RECORD_IN_TEMPO, KPI.DESC_KPI_RECORD_IN_TEMPO, new BigDecimal(0)));
                    if (monitoraggioKPI.getRequestData().getPlatform() != null) {
                        monitoraggioKPIOutputDTO.setKpiAttesi(Consts.KPIATTESIRADIOBKO);
                        listaKPI.add(new KPI(true, KPI.KPI_DURATA_BRANI_DICHIARATI, KPI.DESC_KPI_DURATA_BRANI_DICHIARATI, new BigDecimal(0)));
                        listaKPI.add(new KPI(true, KPI.KPI_BRANI_RD_TITOLI_ERRONEI, KPI.DESC_KPI_BRANI_RD_TITOLI_ERRONEI, new BigDecimal(0)));
                        listaKPI.add(new KPI(true, KPI.KPI_AUTORI_RD_TITOLI_ERRONEI, KPI.DESC_KPI_AUTORI_RD_TITOLI_ERRONEI, new BigDecimal(0)));
                    }
                }
                monitoraggioKPIOutputDTO.setLastUpdate(new Date());
                monitoraggioKPIOutputDTO.setListaKPI(listaKPI);
                monitoraggioKPIOutputDTO.setFileDaElaborare(bdcInformationFileDTOService.fileDTOS(monitoraggioKPI));
                responseMonitoraggioKPIDataDTO.setOutput(monitoraggioKPIOutputDTO);
            }
            EsitoDTO esito = new EsitoDTO();
            esito.setCodice("00");
            esito.setDescrizioneEsito("OK");
            response.setEsito(esito);
            return Response.ok(gson.toJson(response)).build();
        }
    }
}
