package com.alkemytech.sophia.codman.dto;

public class AnagUtilizationTypeIdDspResultSet {

    private String idUtilizationType;
    private String name;
    private String description;
    private String idDsp;

    public String getIdUtilizationType() {
        return idUtilizationType;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getIdDsp() {
        return idDsp;
    }

    public AnagUtilizationTypeIdDspResultSet setIdUtilizationType(String idUtilizationType) {
        this.idUtilizationType = idUtilizationType;
        return this;
    }

    public AnagUtilizationTypeIdDspResultSet setName(String name) {
        this.name = name;
        return this;
    }

    public AnagUtilizationTypeIdDspResultSet setDescription(String description) {
        this.description = description;
        return this;
    }

    public AnagUtilizationTypeIdDspResultSet setIdDsp(String idDsp) {
        this.idDsp = idDsp;
        return this;
    }

    public AnagUtilizationTypeIdDspResultSet(String idUtilizationType, String name, String description, String idDsp) {
        this.idUtilizationType = idUtilizationType;
        this.name = name;
        this.description = description;
        this.idDsp = idDsp;
    }

    @Override
    public String toString() {
        return "AnagUtilizationTypeIdDspResultSet{" +
                "idUtilizationType='" + idUtilizationType + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", idDsp='" + idDsp + '\'' +
                '}';
    }
}