package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.codman.entity.CCIDMetadata;

@Produces("application/json")
@XmlRootElement 
public class CCIDMetadataDTO implements Serializable {

	private static final long serialVersionUID = 8253395841146510114L;
	private BigInteger idCCIDMetadata;
    private String idCcid;
    private String idDsr;
    private BigDecimal totalValue;
    private String currency;
    private String ccidVersion;
    private Boolean ccidEncoded;
    private Boolean ccidEncodedProvisional;
    private Boolean ccidNotEncoded;
    private Integer invoiceItem;
    private BigDecimal valoreFatturabile;
    private BigDecimal valoreResiduo;
    private BigDecimal quotaFattura;
    private BigDecimal identificatoValorePricing;

    public CCIDMetadataDTO(){
    	
    }
    
public CCIDMetadataDTO(CCIDMetadata ccidMetadata){
	this.idCCIDMetadata = ccidMetadata.getIdCCIDMetadata();
	this.idCcid = ccidMetadata.getIdCcid();
	this.idDsr = ccidMetadata.getIdDsr();
	this.totalValue = ccidMetadata.getTotalValue();
	this.currency = ccidMetadata.getCurrency();
	this.ccidVersion = ccidMetadata.getCcidVersion();
	this.ccidEncoded = ccidMetadata.getCcidEncoded();
	this.ccidEncodedProvisional = ccidMetadata.getCcidEncodedProvisional();
	this.ccidNotEncoded = ccidMetadata.getCcidNotEncoded() ;
	this.invoiceItem = ccidMetadata.getInvoiceItem();
	this.valoreFatturabile = ccidMetadata.getValoreFatturabile();
	this.valoreResiduo = ccidMetadata.getValoreResiduo();
	this.quotaFattura = ccidMetadata.getQuotaFattura();
	this.identificatoValorePricing = ccidMetadata.getIdentificatoValorePricing();
    }
    
    public BigInteger getIdCCIDMetadata() {
		return idCCIDMetadata;
	}
	public void setIdCCIDMetadata(BigInteger idCCIDMetadata) {
		this.idCCIDMetadata = idCCIDMetadata;
	}
	public String getIdCcid() {
		return idCcid;
	}
	public void setIdCcid(String idCcid) {
		this.idCcid = idCcid;
	}
	public String getIdDsr() {
		return idDsr;
	}
	public void setIdDsr(String idDsr) {
		this.idDsr = idDsr;
	}
	public BigDecimal getTotalValue() {
		return totalValue;
	}
	public void setTotalValue(BigDecimal totalValue) {
		this.totalValue = totalValue;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getCcidVersion() {
		return ccidVersion;
	}
	public void setCcidVersion(String ccidVersion) {
		this.ccidVersion = ccidVersion;
	}
	public Boolean getCcidEncoded() {
		return ccidEncoded;
	}
	public void setCcidEncoded(Boolean ccidEncoded) {
		this.ccidEncoded = ccidEncoded;
	}
	public Boolean getCcidEncodedProvisional() {
		return ccidEncodedProvisional;
	}
	public void setCcidEncodedProvisional(Boolean ccidEncodedProvisional) {
		this.ccidEncodedProvisional = ccidEncodedProvisional;
	}
	public Boolean getCcidNotEncoded() {
		return ccidNotEncoded;
	}
	public void setCcidNotEncoded(Boolean ccidNotEncoded) {
		this.ccidNotEncoded = ccidNotEncoded;
	}
	public Integer getInvoiceItem() {
		return invoiceItem;
	}
	public void setInvoiceItem(Integer invoiceItem) {
		this.invoiceItem = invoiceItem;
	}
	public BigDecimal getValoreFatturabile() {
		return valoreFatturabile;
	}
	public void setValoreFatturabile(BigDecimal valoreFatturabile) {
		this.valoreFatturabile = valoreFatturabile;
	}
	public BigDecimal getValoreResiduo() {
		return valoreResiduo;
	}
	public void setValoreResiduo(BigDecimal valoreResiduo) {
		this.valoreResiduo = valoreResiduo;
	}
	public BigDecimal getQuotaFattura() {
		return quotaFattura;
	}
	public void setQuotaFattura(BigDecimal quotaFattura) {
		this.quotaFattura = quotaFattura;
	}

	public BigDecimal getIdentificatoValorePricing() {
		return identificatoValorePricing;
	}

	public void setIdentificatoValorePricing(BigDecimal identificatoValorePricing) {
		this.identificatoValorePricing = identificatoValorePricing;
	}
	
	
}
