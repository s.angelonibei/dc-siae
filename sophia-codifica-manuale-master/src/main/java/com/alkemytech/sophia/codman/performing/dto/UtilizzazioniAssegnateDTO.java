package com.alkemytech.sophia.codman.performing.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class UtilizzazioniAssegnateDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private UtilizzazioniAssegnatePKDTO id;
	private String tipoAzione;
	private CodificaDTO codificaDTO;

	public UtilizzazioniAssegnateDTO() {
	}

	public UtilizzazioniAssegnatePKDTO getId() {
		return this.id;
	}

	public void setId(UtilizzazioniAssegnatePKDTO id) {
		this.id = id;
	}

	public String getTipoAzione() {
		return this.tipoAzione;
	}

	public void setTipoAzione(String tipoAzione) {
		this.tipoAzione = tipoAzione;
	}

	public CodificaDTO getCodificaDTO() {
		return this.codificaDTO;
	}

	public void setCodificaDTO(CodificaDTO codificaDTO) {
		this.codificaDTO = codificaDTO;
	}
}