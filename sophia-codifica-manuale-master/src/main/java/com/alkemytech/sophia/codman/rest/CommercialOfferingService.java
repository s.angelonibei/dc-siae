package com.alkemytech.sophia.codman.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.codman.dto.CommercialOfferDTO;
import com.alkemytech.sophia.codman.dto.CommercialOffersConfigDTO;
import com.alkemytech.sophia.codman.dto.ConfDspUtilizationDTO;
import com.alkemytech.sophia.codman.dto.ConfUtilizationDTO;
import com.alkemytech.sophia.codman.dto.DspDTO;
import com.alkemytech.sophia.codman.dto.UtilizationTypeDTO;
import com.alkemytech.sophia.codman.entity.CommercialOffers;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

@Singleton
@Path("commercialOffers")
public class CommercialOfferingService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Provider<EntityManager> provider;
	private final Gson gson;

	@Inject
	protected CommercialOfferingService(@McmdbDataSource Provider<EntityManager> provider, Gson gson) {
		super();
		this.provider = provider;
		this.gson = gson;
	}

	@GET
	@Path("all")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response all(@DefaultValue("0") @QueryParam("first") int first,
			@DefaultValue("50") @QueryParam("last") int last,
			@DefaultValue("") @QueryParam("dsp") String dsp,
			@DefaultValue("") @QueryParam("utilization") String utilization,
			@DefaultValue("") @QueryParam("offer") String offer) {
		EntityManager entityManager = null;
		List<Object[]> result;
		try {
			entityManager = provider.get();


			
			String sql =
					"select x.ID_COMMERCIAL_OFFERS, x.IDDSP, x.DSP_NAME, x.ID_UTILIZATION, x.UTILIZATION_NAME, x.OFFER,"
					+ " x.SERVICE_TYPE_CODE, x.SERVICE_TYPE_DESCRIPTION, x.USE_TYPE_CODE, "
					+ " x.USE_TYPE_DESCRIPTION, x.APPLIED_TARIFF_CODE, x.APPLIED_TARIFF_DESCRIPTION, x.TRADING_BRAND "
					+ " from "
					+ " (select "
					+ " c.ID_COMMERCIAL_OFFERS, "
					+ " c.IDDSP, "
					+ " d.NAME as DSP_NAME, "
					+ " c.ID_UTIILIZATION_TYPE as ID_UTILIZATION, "
					+ " u.NAME as UTILIZATION_NAME, "
					+ " c.OFFERING as OFFER, "
					+ " c.CCID_SERVICE_TYPE_CODE as SERVICE_TYPE_CODE, "
					+ " s.DESCRIPTION as SERVICE_TYPE_DESCRIPTION, "
					+ " c.CCID_USE_TYPE_CODE as USE_TYPE_CODE, "
					+ " t.description as USE_TYPE_DESCRIPTION, "
					+ " c.CCID_APPLIED_TARIFF_CODE as APPLIED_TARIFF_CODE, "
					+ " a.description as APPLIED_TARIFF_DESCRIPTION, "
					+ " c.TRADING_BRAND as TRADING_BRAND "
					+ " from"
					+ " (select * from COMMERCIAL_OFFERS) c "
					+ " left join "
					+ "(select * from ANAG_DSP) d "
					+ " on c.IDDSP = d.IDDSP "
					+ " left join "
					+ " (select * from CCID_SERVICE_TYPE) s "
					+ " on c.CCID_SERVICE_TYPE_CODE = s.CODE "
					+ " left join "
					+ " (select * from CCID_USE_TYPE) t "
					+ " on c.CCID_USE_TYPE_CODE = t.CODE "
					+ " left join "
					+ " (select * from CCID_APPLIED_TARIFF) a "
					+ " on c.CCID_APPLIED_TARIFF_CODE = a.CODE "
					+ " left join "
					+ " (select * from ANAG_UTILIZATION_TYPE) u "
					+ " on c.ID_UTIILIZATION_TYPE = u.ID_UTILIZATION_TYPE ) x "

					+ ((!dsp.isEmpty() && !dsp.equals("*")) || (!utilization.isEmpty() && !utilization.equals("*")) 
							|| (!offer.isEmpty() && !offer.equals("*")) ? " where ": "" )
					 
					+((!dsp.isEmpty() && !dsp.equals("*")) ? " IDDSP = ?  " : "")
					+((!dsp.isEmpty() && !dsp.equals("*")) && (!utilization.isEmpty() && !utilization.equals("*"))?  " and ": "")
					+ ((!utilization.isEmpty() && !utilization.equals("*")) ? " ID_UTILIZATION = ? " : ""  )
					+ (((!dsp.isEmpty() && !dsp.equals("*")) || (!utilization.isEmpty() && !utilization.equals("*")))
							 && (!offer.isEmpty() && !offer.equals("*")) ? " and " : "")
					+(!offer.isEmpty() && !offer.equals("*") ? " ID_COMMERCIAL_OFFERS = ? " : "")
					+ "order by x.DSP_NAME, x.UTILIZATION_NAME, x.OFFER";
			
			final Query q = entityManager.createNativeQuery(sql).setFirstResult(first) 
					.setMaxResults(1 + last - first);
			int i = 1;
			if (!dsp.isEmpty() && !dsp.equals("*")) {
				q.setParameter(i++, dsp);
			}
			if (!utilization.isEmpty() && !utilization.equals("*")) {
				q.setParameter(i++, utilization);
			}
			if (!offer.isEmpty() && !offer.equals("*")) {
				q.setParameter(i++, offer);
			}
			
			result = (List<Object[]>) q.getResultList();
			
			List<CommercialOfferDTO> dtoList = new ArrayList<CommercialOfferDTO>();
			final PagedResult pagedResult = new PagedResult();
			if (null != result) {
				final int maxrows = last - first;
				final boolean hasNext = result.size() > maxrows;
				for (Object[] o : (hasNext ? result.subList(0, maxrows) : result)) {
					dtoList.add(new CommercialOfferDTO(o));
				}
				
				pagedResult.setRows(dtoList).setMaxrows(maxrows).setFirst(first)
				.setLast(hasNext ? last : first + result.size()).setHasNext(hasNext).setHasPrev(first > 0);
				
			} else {
				pagedResult.setMaxrows(0).setFirst(0).setLast(0).setHasNext(false).setHasPrev(false);
			}
			return Response.ok(gson.toJson(pagedResult)).build();
			
		} catch (Exception e) {
			logger.error("all", e);
		}
		return Response.status(500).build();
	}
	

	@GET
	@Path("allAvailable")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response allAvailable() {
		EntityManager entityManager = null;
		List<CommercialOffers> result;
		try {
			entityManager = provider.get();
			final Query q = entityManager.createQuery(
					"select x from CommercialOffers x");
			result = (List<CommercialOffers>) q.getResultList();
			return Response.ok(gson.toJson(result)).build();
		} catch (Exception e) {
			logger.error("allAvailable", e);
		}
		return Response.status(500).build();
	}

		
	@GET
	@Path("all/{dspId}")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response byDSP(@PathParam("dspId") String dspId) {
		EntityManager entityManager = null;
		List<CommercialOffers> result;
		try {
			entityManager = provider.get();
			final Query q = entityManager.createQuery(
					"select x from CommercialOffers x where x.idDSP = :dspId order by x.idUtilizationType");
			q.setParameter("dspId", dspId);
			result = (List<CommercialOffers>) q.getResultList();
			return Response.ok(gson.toJson(result)).build();
		} catch (Exception e) {
			logger.error("byDSP", e);
		}
		return Response.status(500).build();
	}

	@GET
	@Path("all/{dspId}/utilizations")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response utilizationsByDSP(@PathParam("dspId") String dspId) {
		EntityManager entityManager = null;
		List<String> result;
		try {
			entityManager = provider.get();
			final Query q = entityManager.createQuery(
					"select distinct x.idUtilizationType from CommercialOffers x where x.idDSP = :dspId order by x.idUtilizationType");
			q.setParameter("dspId", dspId);
			result = (List<String>) q.getResultList();
			result.add(0, "*");
			return Response.ok(gson.toJson(result)).build();
		} catch (Exception e) {
			logger.error("utilizationsByDSP", e);
		}
		return Response.status(500).build();
	}

	@GET
	@Path("all/{dspId}/utilizations/{utilizationType}")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response offersByDSPAndUtilizations(@PathParam("dspId") String dspId,
			@PathParam("utilizationType") String utilizationType) {
		EntityManager entityManager = null;
		List<String> result;
		try {
			entityManager = provider.get();
			final Query q = entityManager
					.createQuery("select distinct x.offering from CommercialOffers x where x.idDSP = :dspId "
							+ "AND x.idUtilizationType = :utilizationType order by x.offering");
			q.setParameter("dspId", dspId);
			q.setParameter("utilizationType", utilizationType);
			result = (List<String>) q.getResultList();
			result.add(0, "*");
			return Response.ok(gson.toJson(result)).build();
		} catch (Exception e) {
			logger.error("offersByDSPAndUtilizations", e);
		}
		return Response.status(500).build();
	}

	@GET
	@Path("dspCollection")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response dspCollection() {
		EntityManager entityManager = null;
		List<String> result;
		try {
			entityManager = provider.get();
			final Query q = entityManager
					.createQuery("select distinct x.idDSP from CommercialOffers x  order by x.idDSP");
			result = (List<String>) q.getResultList();
			return Response.ok(gson.toJson(result)).build();
		} catch (Exception e) {
			logger.error("dspCollection", e);
		}
		return Response.status(500).build();
	}
	
	

	@GET
	@Path("allConfiguredCommercialOffers")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response allConfiguredCommercialOffers() {
		EntityManager entityManager = null;
		List<ConfDspUtilizationDTO> result = new ArrayList<ConfDspUtilizationDTO>();
		try {
			entityManager = provider.get();
			final Query q = entityManager.createQuery("select x from CommercialOffers x  order by x.idDSP");
			List<CommercialOffers> resList = (List<CommercialOffers>) q.getResultList();
			if (resList != null) {
				Map<String, Map<String, List<String>>> mapResult = new LinkedHashMap<String, Map<String, List<String>>>();
				for (CommercialOffers oc : resList) {
					if (!mapResult.containsKey(oc.getIdDSP())) {
						mapResult.put(oc.getIdDSP(), new LinkedHashMap<String, List<String>>());
						mapResult.get(oc.getIdDSP()).put("*", new ArrayList<String>());
						mapResult.get(oc.getIdDSP()).get("*").add("*");

					}
					Map<String, List<String>> utilizationMap = mapResult.get(oc.getIdDSP());
					if (!utilizationMap.containsKey(oc.getIdUtilizationType())) {
						utilizationMap.put(oc.getIdUtilizationType(), new ArrayList<String>());
						
					}
					
						utilizationMap.get(oc.getIdUtilizationType()).add(oc.getOffering());
						if(!oc.getOffering().equals("*")){
							
							utilizationMap.get("*").add(oc.getOffering());				
						}

				}

				for (String dsp : mapResult.keySet()) {
					Map<String, List<String>> utilizationMap = mapResult.get(dsp);
					List<ConfUtilizationDTO> confUtilizationDTOList = new ArrayList<ConfUtilizationDTO>();
					ConfDspUtilizationDTO confDspUtilizationDTO = new ConfDspUtilizationDTO(dsp,
							confUtilizationDTOList);
					result.add(confDspUtilizationDTO);
					for (String utilization : utilizationMap.keySet()) {

						Set<String> set = new HashSet<String>(utilizationMap.get(utilization));

						if (!utilizationMap.get(utilization).contains("*")
								&& utilizationMap.get(utilization).size() > 1) {
							utilizationMap.get(utilization).add(0, "*");
						}
						if (set.size() < utilizationMap.get(utilization).size()) {
							/* There are duplicates */
							List<String> blackList = new ArrayList<String>();
							blackList.add("*");
							utilizationMap.get(utilization).removeAll(blackList);
							utilizationMap.get(utilization).add(0, "*");
						}
						ConfUtilizationDTO confUtilizationDTO = new ConfUtilizationDTO(utilization,
								new ArrayList<>(utilizationMap.get(utilization)));
						confUtilizationDTOList.add(confUtilizationDTO);
					}
				}

			}
			return Response.ok(gson.toJson(result)).build();
		} catch (Exception e) {
			logger.error("allConfiguredCommercialOffers", e);
		}
		return Response.status(500).build();
	}
	
	
	@GET
	@Path("allCommercialOffers")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response allCommercialOffers() {
		Map<String, List<Object>> result = new HashMap<>();

		EntityManager entityManager = null;
		List<Object> resultUtilizations;
		List<Object> resultDsp;
		List<Object> resultOffers;

		try {
			entityManager = provider.get();
			final Query q1 = entityManager.createQuery("select distinct a from  AnagDsp a, CommercialOffers c where a.idDsp = c.idDSP  order by a.idDsp");
			resultDsp = (List<Object>) q1.getResultList();
			DspDTO wildcardDsp = new DspDTO();
			wildcardDsp.setIdDsp("*");
			wildcardDsp.setName("*");
			resultDsp.add(0, wildcardDsp);
			
			result.put("dsp", resultDsp);

			final Query q2 = entityManager.createQuery("select distinct u from AnagUtilizationType u, CommercialOffers c  where u.idUtilizationType = c.idUtilizationType  order by u.idUtilizationType");
			resultUtilizations = (List<Object>) q2.getResultList();
			UtilizationTypeDTO wildcardUtilization = new UtilizationTypeDTO();
			wildcardUtilization.setIdUtilizationType("*");
			wildcardUtilization.setName("*");
			resultUtilizations.add(0, wildcardUtilization);
			result.put("utilizations", resultUtilizations);

			final Query q3 = entityManager.createQuery("select distinct x.offering from CommercialOffers x  order by x.offering");
			resultOffers = (List<Object>) q3.getResultList();
			if(!resultOffers.contains("*")){
				resultOffers.add(0, "*");
			}
			
			result.put("offers", resultOffers);
			
		} catch (Exception e) {
			logger.error("allCommercialOffers", e);
			return Response.status(500).build();
		}
		
		return Response.ok(gson.toJson(result)).build();

	}
	
	@POST
	@Path("")
	public Response put(CommercialOffersConfigDTO config) {
		Status status = Status.INTERNAL_SERVER_ERROR;
		final EntityManager entityManager = provider.get();
		CommercialOffers commercialOffer = new CommercialOffers();
		commercialOffer.setIdDSP(config.getIdDSP());
		commercialOffer.setIdUtilizationType(config.getIdUtilizationType());
		commercialOffer.setOffering(config.getOffering());
		commercialOffer.setCcidServiceTypeCode(config.getServiceType());
		commercialOffer.setCcidUseTypeCode(config.getUseType());
		commercialOffer.setCcidAppliedTariffCode(config.getAppliedTariff());
		commercialOffer.setTradingBrand(config.getTradingBrand());

		try {
			entityManager.getTransaction().begin();
			entityManager.persist(commercialOffer);

			// TODO log user activity

			entityManager.getTransaction().commit();
			return Response.ok().build();
		} catch (Exception e) {
			logger.error("put", e);

			if (e.getCause() != null && e.getCause().getCause() != null
					&& e.getCause().getCause() instanceof MySQLIntegrityConstraintViolationException) {
				status = Status.CONFLICT;
			}

			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}

		}

		return Response.status(status).build();
	}

	@DELETE
	@Path("")
	public Response delete(CommercialOffersConfigDTO config) {

		Status status = Status.INTERNAL_SERVER_ERROR;
		final EntityManager entityManager = provider.get();
		try {
			entityManager.getTransaction().begin();
			
			final Query query = entityManager.createQuery("delete from CommercialOffers x where x.idDSP = :dspId "
					+ "and x.idUtilizationType = :utilizationType  and x.offering = :offering");
			query.setParameter("dspId", config.getIdDSP());
			query.setParameter("utilizationType", config.getIdUtilizationType());
			query.setParameter("offering", config.getOffering());
			query.executeUpdate();

			final Query query2 = entityManager.createQuery("delete from DspConfig x where x.offering = :offering ");
			query2.setParameter("offering", Integer.toString(config.getIdCommercialOffering()));
			query2.executeUpdate();

			entityManager.getTransaction().commit();

					
			return Response.ok().build();
		} catch (Exception e) {
			logger.error("delete", e);
			if (e.getCause() != null && e.getCause().getCause() != null
					&& e.getCause().getCause() instanceof MySQLIntegrityConstraintViolationException) {
				status = Status.CONFLICT;
			}

			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
		}
		return Response.status(status).build();
	}
	
//	private List<String> filter(List<String> list, String expression){
//		List<String> filteredList = new ArrayList<>();
//		Iterator<String> it = list.iterator();
//		while(it.hasNext()){
//			String ith = it.next();
//			if(!ith.equals(expression)){
//				filteredList.add(ith);
//			}
//		}
//		return filteredList;
//	}
	
	@PUT
	@Path("")
	public Response update(
			@DefaultValue("") @QueryParam("previdDSP") String previdDSP,
			@DefaultValue("") @QueryParam("previdUtilizationType") String previdUtilizationType,
			@DefaultValue("") @QueryParam("prevoffering") String prevOffering,
			@DefaultValue("") @QueryParam("prevCcidServiceTypeCode") String prevCcidServiceTypeCode,
			@DefaultValue("") @QueryParam("prevCcidUseTypeCode") String	prevCcidUseTypeCode,
			@DefaultValue("") @QueryParam("prevAppliedTariffCode") String prevAppliedTariffCode,
			@DefaultValue("") @QueryParam("prevTradingBrand") String prevTradingBrand,
			@DefaultValue("") @QueryParam("newidUtilizationType") String 	newidUtilizationType,
			@DefaultValue("") @QueryParam("newoffering") String newOffering,
			@DefaultValue("") @QueryParam("newserviceType") String newserviceType,
			@DefaultValue("") @QueryParam("newuseType")	String newuseType,
			@DefaultValue("") @QueryParam("newappliedTariff") String newappliedTariff,
			@DefaultValue("") @QueryParam("newtradingBrand") String newtradingBrand ) {




		Status status = Status.INTERNAL_SERVER_ERROR;
//		if(
//				&& ){
//			return Response.status(Status.CONFLICT).build();
//		}
//
		final EntityManager entityManager = provider.get();
		try {
			entityManager.getTransaction().begin();

			boolean mustChangeUtilization = !previdUtilizationType.equals(newidUtilizationType);
			boolean mustChangeOffering = !prevOffering.equals(newOffering);
			boolean mustChangeServiceType = !prevCcidServiceTypeCode.equals(newserviceType) ;
			boolean mustChangeUseType = !prevCcidUseTypeCode.equals(newuseType);
			boolean mustChangeAppliedTariff = !prevAppliedTariffCode.equals(newappliedTariff);
			boolean mustChangeTradingBrand = !prevTradingBrand.equals(newtradingBrand);
  			String changeString = "";
  			
			if(mustChangeUtilization){
				changeString += "x.idUtilizationType = :newUtilizationType , ";
			}
			if(mustChangeOffering){
				changeString += "x.offering = :newOffering , ";
			}
			if(mustChangeServiceType){
				changeString += "x.ccidServiceTypeCode = :service , ";
			}
			if(mustChangeUseType){
				changeString += " x.ccidUseTypeCode = :use , ";
			}
			if(mustChangeAppliedTariff){
				changeString += "x.ccidAppliedTariffCode = :tariff , ";
			}
			if(mustChangeTradingBrand){
				changeString += "x.tradingBrand = :brand , ";
			}
			
			if(!changeString.isEmpty()){				
				changeString = changeString.substring(0,changeString.lastIndexOf(",") -1);
			}

			if( mustChangeUtilization || mustChangeOffering ||
					mustChangeServiceType || mustChangeUseType || mustChangeAppliedTariff ||
			mustChangeTradingBrand){
				final Query query = entityManager.createQuery("update CommercialOffers x set "
						+ changeString
						+ " where x.idDSP = :prevDspId "
						+ " and x.idUtilizationType = :prevUtilizationType  and x.offering = :prevOffering");

				query.setParameter("prevDspId", previdDSP);
				query.setParameter("prevUtilizationType", previdUtilizationType);
				query.setParameter("prevOffering", prevOffering);
				if(mustChangeUtilization){
					query.setParameter("newUtilizationType", newidUtilizationType);
				}
				if(mustChangeOffering){					
					query.setParameter("newOffering", newOffering);
				}
				if(mustChangeServiceType){
					query.setParameter("service", newserviceType);
				}
				if(mustChangeUseType){
					query.setParameter("use", newuseType);
				}
				if(mustChangeAppliedTariff){
					query.setParameter("tariff", newappliedTariff);
				}
				if(mustChangeTradingBrand){
					query.setParameter("brand", newtradingBrand);
				}
				query.executeUpdate();
				entityManager.getTransaction().commit();
			}
			return Response.ok().build();

		} catch (Exception e) {
			logger.error("update", e);
			if (e.getCause() != null && e.getCause().getCause() != null
					&& e.getCause().getCause() instanceof MySQLIntegrityConstraintViolationException) {
				status = Status.CONFLICT;
			}

			if (entityManager.getTransaction().isActive()) {
				entityManager.getTransaction().rollback();
			}
		}
		return Response.status(status).build();
	}
	

}
