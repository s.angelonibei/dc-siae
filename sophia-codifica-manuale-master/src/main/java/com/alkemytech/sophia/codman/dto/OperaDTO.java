package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class OperaDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String codiceOpera;
	private String gradoDiConfidenza;
	private String titoloOriginale;
	private String confidenzaTitoloOriginale;
	private String titoloAlternativo;
	private String confidenzaTitoloAlternativo;
	private String autore;
	private String confidenzaAutore;
	private String compositore;
	private String confCompositore;
	private String interprete;
	private String confInterprete;
	private String tipoCodifica;
	private String codiceSiae;
	private String codiceAda;
	private String iswc;
	private List<UtilizzazioneOperaDTO> utilizzazioni;
	
	

	public String getConfCompositore() {
		return confCompositore;
	}

	public void setConfCompositore(String confCompositore) {
		this.confCompositore = confCompositore;
	}

	public String getConfInterprete() {
		return confInterprete;
	}

	public void setConfInterprete(String confInterprete) {
		this.confInterprete = confInterprete;
	}

	public String getCodiceSiae() {
		return codiceSiae;
	}

	public void setCodiceSiae(String codiceSiae) {
		this.codiceSiae = codiceSiae;
	}

	public String getCodiceAda() {
		return codiceAda;
	}

	public void setCodiceAda(String codiceAda) {
		this.codiceAda = codiceAda;
	}

	public String getIswc() {
		return iswc;
	}

	public void setIswc(String iswc) {
		this.iswc = iswc;
	}

	public String getCodiceOpera() {
		return codiceOpera;
	}

	public void setCodiceOpera(String codiceOpera) {
		this.codiceOpera = codiceOpera;
	}

	public String getGradoDiConfidenza() {
		return gradoDiConfidenza;
	}

	public void setGradoDiConfidenza(String gradoDiConfidenza) {
		this.gradoDiConfidenza = gradoDiConfidenza;
	}

	public String getTitoloOriginale() {
		return titoloOriginale;
	}

	public void setTitoloOriginale(String titoloOriginale) {
		this.titoloOriginale = titoloOriginale;
	}

	public String getConfidenzaTitoloOriginale() {
		return confidenzaTitoloOriginale;
	}

	public void setConfidenzaTitoloOriginale(String confidenzaTitoloOriginale) {
		this.confidenzaTitoloOriginale = confidenzaTitoloOriginale;
	}

	public String getTitoloAlternativo() {
		return titoloAlternativo;
	}

	public void setTitoloAlternativo(String titoloAlternativo) {
		this.titoloAlternativo = titoloAlternativo;
	}

	public String getConfidenzaTitoloAlternativo() {
		return confidenzaTitoloAlternativo;
	}

	public void setConfidenzaTitoloAlternativo(String confidenzaTitoloAlternativo) {
		this.confidenzaTitoloAlternativo = confidenzaTitoloAlternativo;
	}

	public String getAutore() {
		return autore;
	}

	public void setAutore(String autore) {
		this.autore = autore;
	}

	public String getConfidenzaAutore() {
		return confidenzaAutore;
	}

	public void setConfidenzaAutore(String confidenzaAutore) {
		this.confidenzaAutore = confidenzaAutore;
	}

	public String getCompositore() {
		return compositore;
	}

	public void setCompositore(String compositore) {
		this.compositore = compositore;
	}


	public String getInterprete() {
		return interprete;
	}

	public void setInterprete(String interprete) {
		this.interprete = interprete;
	}


	public String getTipoCodifica() {
		return tipoCodifica;
	}

	public void setTipoCodifica(String tipoCodifica) {
		this.tipoCodifica = tipoCodifica;
	}

	public List<UtilizzazioneOperaDTO> getUtilizzazioni() {
		return utilizzazioni;
	}

	public void setUtilizzazioni(List<UtilizzazioneOperaDTO> utilizzazioni) {
		this.utilizzazioni = utilizzazioni;
	}

}
