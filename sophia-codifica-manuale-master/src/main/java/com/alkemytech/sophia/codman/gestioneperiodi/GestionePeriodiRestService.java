package com.alkemytech.sophia.codman.gestioneperiodi;


import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.alkemytech.sophia.broadcasting.dto.Periodo;
import com.alkemytech.sophia.codman.gestioneperiodi.GestionePeriodiInterface;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
@Path("gestionePeriodi")
public class GestionePeriodiRestService {
	private Gson gson;
	private GestionePeriodiInterface gestionePeriodiInterface;
	
	@Inject
	public GestionePeriodiRestService(GestionePeriodiInterface gestionePeriodiInterface, Gson gson) {
		this.gson = gson;
		this.gestionePeriodiInterface = gestionePeriodiInterface;
	}
	
	@GET
	@Path("getPeriodi")
	@Produces("application/json")
	public Response getPeriodiRipartizione(@QueryParam(value = "anno") Integer anno,@QueryParam(value = "ambito") String ambito) {
		List<Periodo> periodi = gestionePeriodiInterface.getPeriodi(anno, ambito);
		return Response.ok(gson.toJson(periodi)).build();
	}
}