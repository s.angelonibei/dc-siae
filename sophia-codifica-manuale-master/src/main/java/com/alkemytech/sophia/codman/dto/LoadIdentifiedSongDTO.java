package com.alkemytech.sophia.codman.dto;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class LoadIdentifiedSongDTO implements Serializable {

    private String hashId;
    private String title;
    private String artists;
    private String roles;
    private String salesCount;
    private String insertTime;
    private String lastAutoTime;
    private String lastManualTime;
    private String siadaTitle;
    private String siadaArtists;
    private String uuid;
    private String username;
    private String identificationType;
}
