package com.alkemytech.sophia.codman.entity.performing;

import java.io.Serializable;
import java.util.List;

//TODO:see if this import needs migration
//import it.siae.valorizzatore.utility.Utility;

import com.alkemytech.sophia.codman.utils.AccountingUtils;

/**
 * Created by idilello on 7/15/16.
 */
public class DettaglioPM implements Serializable{

	private static final long serialVersionUID = 8053008325223255763L;
	
	private Long idEvento;
	private String numeroFattura;
	private Long idEventoPagato;
    private String voceIncasso;
    private Long pmPrevisti=1L;
    private Long pmPrevistiSpalla=1L;
    private Long pmRientrati=0L;
    private Long pmRientratiSpalla=0L;
    private List<ImportoRicalcolato> importiRicalcolati;
    private Double importoTotale=0D;;

    private Double importoAssegnato;
    private Double importoDisponibile;
	private Double importoSospeso;

    public Long getIdEvento() {
        return idEvento;
    }

    public void setIdEvento(Long idEvento) {
        this.idEvento = idEvento;
    }

    public String getVoceIncasso() {
        return voceIncasso;
    }

    public void setVoceIncasso(String voceIncasso) {
        this.voceIncasso = voceIncasso;
    }

    public Long getPmPrevisti() {
        return pmPrevisti;
    }

    public void setPmPrevisti(Long pmPrevisti) {
        this.pmPrevisti = pmPrevisti;
    }

    public Long getPmPrevistiSpalla() {
        return pmPrevistiSpalla;
    }

    public void setPmPrevistiSpalla(Long pmPrevistiSpalla) {
        this.pmPrevistiSpalla = pmPrevistiSpalla;
    }

    public Long getPmRientrati() {
        return pmRientrati;
    }

    public void setPmRientrati(Long pmRientrati) {
        this.pmRientrati = pmRientrati;
    }

    public Long getPmRientratiSpalla() {
        return pmRientratiSpalla;
    }

    public void setPmRientratiSpalla(Long pmRientratiSpalla) {
        this.pmRientratiSpalla = pmRientratiSpalla;
    }

    public List<ImportoRicalcolato> getImportiRicalcolati() {
        return importiRicalcolati;
    }

    public void setImportiRicalcolati(List<ImportoRicalcolato> importiRicalcolati) {
        this.importiRicalcolati = importiRicalcolati;
    }

    public Double getImportoTotale() {
        if (importoTotale==null)
            importoTotale=0D;

        return importoTotale;
    }

    public void setImportoTotale(Double importoTotale) {
        this.importoTotale = importoTotale;
    }

    public Double getImportoAssegnato() {
        return importoAssegnato;
    }

    public void setImportoAssegnato(Double importoAssegnato) {
        this.importoAssegnato = importoAssegnato;
    }

    public Double getImportoDisponibile() {
        importoDisponibile = getImportoTotale() - getImportoAssegnato();
        return importoDisponibile;
    }

    public void setImportoDisponibile(Double importoDisponibile) {

        this.importoDisponibile = importoDisponibile;
    }

    //TODO: can I create a class in utils package?
    public String getImportoTotaleFormatted() {
        return AccountingUtils.displayFormatted(importoTotale);
    }

    public String getImportoAssegnatoFormatted() {
        return AccountingUtils.displayFormatted(importoAssegnato);
    }

    public String getImportoDisponibileFormatted() {
        return AccountingUtils.displayFormatted(importoDisponibile);
    }

	public String getNumeroFattura() {
		return numeroFattura;
	}

	public void setNumeroFattura(String numeroFattura) {
		this.numeroFattura = numeroFattura;
	}

	public Long getIdEventoPagato() {
		return idEventoPagato;
	}

	public void setIdEventoPagato(Long idEventoPagato) {
		this.idEventoPagato = idEventoPagato;
	}

	public Double getImportoSospeso() {
		return importoSospeso;
	}

	public void setImportoSospeso(Double importoSospeso) {
		this.importoSospeso = importoSospeso;
	}

}
