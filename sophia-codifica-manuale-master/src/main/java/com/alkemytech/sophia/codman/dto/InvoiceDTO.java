package com.alkemytech.sophia.codman.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.codman.entity.AnagClient;
import com.alkemytech.sophia.codman.entity.AnagDsp;
import com.alkemytech.sophia.codman.invoice.repository.Invoice;
import com.alkemytech.sophia.codman.invoice.repository.InvoiceItem;
import com.alkemytech.sophia.codman.invoice.repository.InvoiceItemReason;

@Produces("application/json")
@XmlRootElement 
public class InvoiceDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6852388758418063179L;
	private Integer idInvoice;
	private String invoiceCode;
	private Date creationDate;
	private Date lastUpdateDate;
	private String userID;
	private String idDsp;
	private BigDecimal total;
	private Float vat;
	private String note;
	private String status;
	private String pdfPath;
	private List<InvoiceItemDto> invoiceItems = new ArrayList<InvoiceItemDto>();
	private DspDTO dsp;
	private AnagClientDTO clientData;
	
	public InvoiceDTO() {

	}

	public InvoiceDTO(Invoice invoice, AnagDsp anagDsp, AnagClient anagClient, Map<Integer,InvoiceItemReason> reasonMap) {

		this.idInvoice = invoice.getIdInvoice();
		this.invoiceCode = invoice.getInvoiceCode();
		this.idDsp = invoice.getIdDsp();
		for(InvoiceItem item : invoice.getInvoiceItems()){
			this.invoiceItems.add(new InvoiceItemDto(item));	
		}
		
		this.note = invoice.getNote();
		this.status = invoice.getStatus();
		this.total = invoice.getTotal();
		this.userID = invoice.getUserID();
		this.vat = invoice.getVat();
		this.creationDate = invoice.getCreationDate();
		this.lastUpdateDate = invoice.getLastUpdateDate();
		this.dsp = new DspDTO(anagDsp);
		this.clientData = new AnagClientDTO(anagClient);
	}

	public Integer getIdInvoice() {
		return idInvoice;
	}

	public void setIdInvoice(Integer idInvoice) {
		this.idInvoice = idInvoice;
	}

	public String getInvoiceCode() {
		return invoiceCode;
	}

	public void setInvoiceCode(String invoiceCode) {
		this.invoiceCode = invoiceCode;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getIdDsp() {
		return idDsp;
	}

	public void setIdDsp(String idDsp) {
		this.idDsp = idDsp;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public Float getVat() {
		return vat;
	}

	public void setVat(Float vat) {
		this.vat = vat;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPdfPath() {
		return pdfPath;
	}

	public void setPdfPath(String pdfPath) {
		this.pdfPath = pdfPath;
	}


	public List<InvoiceItemDto> getInvoiceItems() {
		return invoiceItems;
	}

	public void setInvoiceItems(List<InvoiceItemDto> invoiceItems) {
		this.invoiceItems = invoiceItems;
	}

	public AnagClientDTO getClientData() {
		return clientData;
	}

	public void setClientData(AnagClientDTO clientData) {
		this.clientData = clientData;
	}

	public DspDTO getDsp() {
		return dsp;
	}

	public void setDsp(DspDTO dsp) {
		this.dsp = dsp;
	}

	
}
