package com.alkemytech.sophia.codman.rest;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.alkemytech.sophia.codman.entity.UnidentifiedSongDsr;
import com.alkemytech.sophia.codman.guice.UnidentifiedDataSource;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

@Singleton
@Path("unidentifiedSongDsr")
public class UnidentifiedSongDsrService {

	private final Provider<EntityManager> provider;
	private final Gson gson;
	
	@Inject
	protected UnidentifiedSongDsrService(@UnidentifiedDataSource Provider<EntityManager> provider, Gson gson) {
		super();
		this.provider = provider;
		this.gson = gson;
	}

	@GET
	@Path("byHashId/{hashId}")
	@Produces("application/json")
	@SuppressWarnings("unchecked")
	public Response byHash(@PathParam("hashId") String hashId) {
		EntityManager entityManager = null;
		List<UnidentifiedSongDsr> result;
		try {
			entityManager = provider.get();
			entityManager.getTransaction().begin();
			final Query q = entityManager.createQuery("select x from UnidentifiedSongDsr x where x.hashId = :hashId order by x.salesCount desc");
			q.setParameter("hashId", hashId);
			result = (List<UnidentifiedSongDsr>) q.getResultList();
			return Response.ok(gson.toJson(result)).build();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Response.status(500).build();
	}

}
