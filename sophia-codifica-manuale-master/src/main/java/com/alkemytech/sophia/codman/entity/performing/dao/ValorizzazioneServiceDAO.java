package com.alkemytech.sophia.codman.entity.performing.dao;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.http.util.TextUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.codman.dto.performing.CarichiRipartizioneDTO;
import com.alkemytech.sophia.codman.dto.performing.CarichiRipartizioneResponse;
import com.alkemytech.sophia.codman.dto.performing.CarichiRipartizioniRequest;
import com.alkemytech.sophia.codman.dto.performing.ConfigurazioniPeriodoDTO;
import com.alkemytech.sophia.codman.dto.performing.PerfValorizzazioneDTO;
import com.alkemytech.sophia.codman.dto.performing.RegolaRipartizioneDTO;
import com.alkemytech.sophia.codman.dto.performing.ValorizzazioneRunDTO;
import com.alkemytech.sophia.codman.entity.CarichiRipartizione;
import com.alkemytech.sophia.codman.entity.PeriodoRipartizione;
import com.alkemytech.sophia.codman.entity.performing.PerfValorizzazione;
import com.alkemytech.sophia.codman.entity.performing.PerfValorizzazioneRun;
import com.alkemytech.sophia.codman.entity.performing.PerfVoceIncasso;
import com.alkemytech.sophia.codman.utils.DateUtils;
import com.google.inject.Inject;
import com.google.inject.Provider;

/**
 * Created by idilello on 6/9/16.
 */
public class ValorizzazioneServiceDAO implements IValorizzazioneServiceDAO {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Inject
	private Provider<EntityManager> provider;

	// @Inject
	public ValorizzazioneServiceDAO(Provider<EntityManager> provider) {
		this.provider = provider;
	}

	@Override
	public Integer addConfigurazione(PerfValorizzazioneDTO perfValorizzazioneDTO) {
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		f.setTimeZone(TimeZone.getTimeZone("GMT+0"));
		perfValorizzazioneDTO
				.setDataUltimaModifica(DateUtils.stringToDate(f.format(new Date()), DateUtils.DATETIME_MYSQL_FORMAT));

		EntityManager em = provider.get();
		PeriodoRipartizione p = (PeriodoRipartizione) em
				.createQuery("SELECT x from PeriodoRipartizione x where x.idPeriodoRipartizione=:id")
				.setParameter("id", Long.parseLong(perfValorizzazioneDTO.getPeriodoRipartizione())).getSingleResult();
		try {
			em.createQuery("SELECT x from PerfValorizzazione x where "
					+ " x.periodoRipartizione.idPeriodoRipartizione=:id " + " AND x.voceIncasso=:voceIncasso "
					+ " AND x.dataDisattivazione IS NULL " + " AND x.tipologiaReport=:tipologiaReport")
					.setParameter("id", Long.parseLong(perfValorizzazioneDTO.getPeriodoRipartizione()))
					.setParameter("voceIncasso", perfValorizzazioneDTO.getVoceIncasso())
					.setParameter("tipologiaReport", perfValorizzazioneDTO.getTipologiaReport()).getSingleResult();
			return 501;

		} catch (Exception e) {
		}

		try {
			em.getTransaction().begin();
			PerfValorizzazione perfValorizzazione = new PerfValorizzazione();
			perfValorizzazione.setRipartizioneOS(perfValorizzazioneDTO.getRipartizioneOS());
			perfValorizzazione.setPeriodoRipartizione(p);
			perfValorizzazione.setVoceIncasso(perfValorizzazioneDTO.getVoceIncasso());
			perfValorizzazione.setTipologiaReport(perfValorizzazioneDTO.getTipologiaReport());
			perfValorizzazione.setValorizzazione(perfValorizzazioneDTO.getValorizzazione());
			perfValorizzazione.setRipartizione(perfValorizzazioneDTO.getRipartizione());
			perfValorizzazione.setUser(perfValorizzazioneDTO.getUser());
			perfValorizzazione.setDataUltimaModifica(perfValorizzazioneDTO.getDataUltimaModifica());

			em.persist(perfValorizzazione);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
		}
		return 200;
	}

	@Override
	public List<ConfigurazioniPeriodoDTO> getConfigurazioni(String periodoRipartizione, String ripartizione,
			String tipologiaReport, String regola, String voceIncasso) {

		EntityManager em = provider.get();
		StringBuffer buffer = new StringBuffer("SELECT \n" + "    *\n" + "FROM\n" + "    PERF_VALORIZZAZIONE v\n"
				+ "    LEFT JOIN  PERIODO_RIPARTIZIONE p on v.PERIODO_RIPARTIZIONE = p.ID_PERIODO_RIPARTIZIONE\n"
				+ "WHERE\n" + "   p.REPERTORIO = 'MUSICA'\n" + "        AND v.DATA_DISATTIVAZIONE IS NULL\n"
				+ "        AND p.AMBITO = 'PERFORMING'\n");

		if (!TextUtils.isEmpty(periodoRipartizione)) {
			buffer.append(" AND p.CODICE = '" + periodoRipartizione + "'\n");
		}
		if (!TextUtils.isEmpty(ripartizione)) {

			if ("analitico".equals(ripartizione)) {
				buffer.append(" AND JSON_EXTRACT(v.RIPARTIZIONE,'$.analitico') > " + 1 + "\n");
			}
			if ("campionario".equals(ripartizione)) {
				buffer.append(" AND JSON_EXTRACT(v.RIPARTIZIONE,'$.campionario') > " + 1 + "\n");
			}
			if ("proporzionale".equals(ripartizione)) {
				buffer.append(" AND JSON_EXTRACT(v.RIPARTIZIONE,'$.proporzionale') > " + 1 + "\n");
			}
		}
		if (!TextUtils.isEmpty(tipologiaReport)) {
			buffer.append(" AND v.FORMATO_REPORT = '" + tipologiaReport + "'\n");
		}
		if (!TextUtils.isEmpty(regola)) {
			buffer.append(" AND JSON_EXTRACT(v.VALORIZZAZIONE,'$.regola') = '" + regola + "'\n");
		}
		if (!TextUtils.isEmpty(voceIncasso)) {
			buffer.append(" AND v.VOCE_INCASSO ='" + voceIncasso + "'\n");
		}
		buffer.append("ORDER BY v.VOCE_INCASSO ASC");
		Query q = em.createNativeQuery(buffer.toString());
		List<ConfigurazioniPeriodoDTO> configurazioniPeriodoDTO = new ArrayList<>();

		LinkedHashMap<String, List<RegolaRipartizioneDTO>> map = new LinkedHashMap<>();

		List<Object[]> result = q.getResultList();
		for (Object[] row : result) {
			PerfValorizzazione perfValorizzazione = new PerfValorizzazione(row);
			List<RegolaRipartizioneDTO> regole = map.get(perfValorizzazione.getPeriodoRipartizione().getCodice());
			if (map.get(perfValorizzazione.getPeriodoRipartizione().getCodice()) == null) {
				regole = new ArrayList<>();
				regole.add(new RegolaRipartizioneDTO(perfValorizzazione));
				map.put(perfValorizzazione.getPeriodoRipartizione().getCodice(), regole);
			} else {
				regole.add(new RegolaRipartizioneDTO(perfValorizzazione));
			}

		}
		for (Entry<String, List<RegolaRipartizioneDTO>> regolaRipartizioneDTO : map.entrySet()) {
			Date competenzaDa = null;
			Date competenzaA = null;
			try {
				competenzaDa = regolaRipartizioneDTO.getValue().get(0).getCompetenzaDa();
				competenzaA = regolaRipartizioneDTO.getValue().get(0).getCompetenzaA();
				configurazioniPeriodoDTO.add(new ConfigurazioniPeriodoDTO(regolaRipartizioneDTO.getKey(), competenzaDa,
						competenzaA, regolaRipartizioneDTO.getKey(), regolaRipartizioneDTO.getValue()));
			} catch (Exception e) {
				return null;
			}

		}
		return configurazioniPeriodoDTO;
	}

	@Override
	public List<PerfVoceIncasso> getVociIncasso() {
		EntityManager em = provider.get();
		Query q = em.createQuery("Select x from PerfVoceIncasso x where x.flagVoceRipartizioneFittizia=false");

		List<PerfVoceIncasso> result = (List<PerfVoceIncasso>) q.getResultList();
		return result;
	}

	@Override
	public List<PerfVoceIncasso> getVociIncassoFittizie() {
		EntityManager em = provider.get();
		Query q = em.createQuery("Select x from PerfVoceIncasso x where x.flagVoceRipartizioneFittizia=true");

		List<PerfVoceIncasso> result = (List<PerfVoceIncasso>) q.getResultList();
		return result;
	}

	@Override
	public List<PeriodoRipartizione> getPeriodiRipartizione() {
		EntityManager em = provider.get();
		Query q = em.createQuery(
				"Select x from PeriodoRipartizione x where x.ambito = 'PERFORMING' and x.repertorio = 'MUSICA' order by x.codice");

		List<PeriodoRipartizione> result = (List<PeriodoRipartizione>) q.getResultList();
		return result;
	}

	@Override
	public Integer updateConfigurazione(PerfValorizzazioneDTO perfValorizzazioneDTO) {

		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		f.setTimeZone(TimeZone.getTimeZone("GMT+0"));
		perfValorizzazioneDTO
				.setDataUltimaModifica(DateUtils.stringToDate(f.format(new Date()), DateUtils.DATETIME_MYSQL_FORMAT));

		EntityManager em = provider.get();
		PeriodoRipartizione p = (PeriodoRipartizione) em.createQuery(
				"SELECT x from PeriodoRipartizione x where x.codice=:codice and x.ambito = 'PERFORMING' and x.repertorio = 'MUSICA'")
				.setParameter("codice", perfValorizzazioneDTO.getPeriodoRipartizione()).getSingleResult();
		try {
			em.getTransaction().begin();
			PerfValorizzazione prPerfValorizzazione = (PerfValorizzazione) em
					.createQuery("SELECT x from PerfValorizzazione x where " 
							+ " x.periodoRipartizione.codice=:id "
							+ " AND x.voceIncasso=:voceIncasso " 
							+ " AND x.tipologiaReport=:tipologiaReport "
							+ " AND x.dataDisattivazione IS NULL " 
							+ " AND x.periodoRipartizione.ambito='PERFORMING' "
							+ " AND x.periodoRipartizione.repertorio='MUSICA'")
					.setParameter("id", perfValorizzazioneDTO.getPeriodoRipartizione())
					.setParameter("voceIncasso", perfValorizzazioneDTO.getVoceIncasso())
					.setParameter("tipologiaReport", perfValorizzazioneDTO.getTipologiaReport()).getSingleResult();
			prPerfValorizzazione.setDataDisattivazione(new Date());
			em.merge(prPerfValorizzazione);
			em.flush();
			PerfValorizzazione perfValorizzazioneNew = new PerfValorizzazione();
			perfValorizzazioneNew.setRipartizioneOS(perfValorizzazioneDTO.getRipartizioneOS());
			perfValorizzazioneNew.setPeriodoRipartizione(p);
			perfValorizzazioneNew.setVoceIncasso(perfValorizzazioneDTO.getVoceIncasso());
			perfValorizzazioneNew.setTipologiaReport(perfValorizzazioneDTO.getTipologiaReport());
			perfValorizzazioneNew.setValorizzazione(perfValorizzazioneDTO.getValorizzazione());
			perfValorizzazioneNew.setRipartizione(perfValorizzazioneDTO.getRipartizione());
			perfValorizzazioneNew.setUser(perfValorizzazioneDTO.getUser());
			perfValorizzazioneNew.setDataUltimaModifica(perfValorizzazioneDTO.getDataUltimaModifica());

			em.persist(perfValorizzazioneNew);
			em.flush();
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
			return 501;
		}
		return 200;
	}

	public List<ConfigurazioniPeriodoDTO> getStoricoConfigurazione(String periodo, String voceIncasso,
			String tipoReport) {
		EntityManager em = provider.get();
		String s = "SELECT \n" + "    *\n" + "FROM\n" + "    PERF_VALORIZZAZIONE v\n"
				+ "    LEFT JOIN  PERIODO_RIPARTIZIONE p on v.PERIODO_RIPARTIZIONE = p.ID_PERIODO_RIPARTIZIONE\n"
				+ "WHERE\n" + " 	 p.REPERTORIO = 'MUSICA'\n" + "    AND v.DATA_DISATTIVAZIONE IS NOT NULL\n"
				+ "    AND p.AMBITO = 'PERFORMING'\n" + "    AND v.VOCE_INCASSO ='" + voceIncasso + "'\n"
				+ "    AND v.FORMATO_REPORT = '" + tipoReport + "'\n" + "    AND p.CODICE = '" + periodo + "'\n";

		Query q = em.createNativeQuery(s);
		List<ConfigurazioniPeriodoDTO> configurazioniPeriodoDTO = new ArrayList<>();

		LinkedHashMap<String, List<RegolaRipartizioneDTO>> map = new LinkedHashMap<>();

		List<Object[]> result = q.getResultList();
		for (Object[] row : result) {
			PerfValorizzazione perfValorizzazione = new PerfValorizzazione(row);
			List<RegolaRipartizioneDTO> regole = map.get(perfValorizzazione.getPeriodoRipartizione().getCodice());
			if (map.get(perfValorizzazione.getPeriodoRipartizione().getCodice()) == null) {
				regole = new ArrayList<>();
				regole.add(new RegolaRipartizioneDTO(perfValorizzazione));
				map.put(perfValorizzazione.getPeriodoRipartizione().getCodice(), regole);
			} else {
				regole.add(new RegolaRipartizioneDTO(perfValorizzazione));
			}

		}
		for (Entry<String, List<RegolaRipartizioneDTO>> regolaRipartizioneDTO : map.entrySet()) {
			Date competenzaDa = null;
			Date competenzaA = null;
			try {
				competenzaDa = regolaRipartizioneDTO.getValue().get(0).getCompetenzaDa();
				competenzaA = regolaRipartizioneDTO.getValue().get(0).getCompetenzaA();
				configurazioniPeriodoDTO.add(new ConfigurazioniPeriodoDTO(regolaRipartizioneDTO.getKey(), competenzaDa,
						competenzaA, regolaRipartizioneDTO.getKey(), regolaRipartizioneDTO.getValue()));
			} catch (Exception e) {
				return null;
			}

		}
		return configurazioniPeriodoDTO;

	}

	public void deleteValorizzazioneConfigurazione(String periodoRipartizione, String voceIncasso,
			String tipologiaReport, String user) {

		EntityManager em = provider.get();
		try {
			em.getTransaction().begin();
			PerfValorizzazione prPerfValorizzazione = (PerfValorizzazione) em
					.createQuery("SELECT x from PerfValorizzazione x where " 
							+ " x.periodoRipartizione.codice=:id "
							+ " AND x.voceIncasso=:voceIncasso " 
							+ " AND x.tipologiaReport=:tipologiaReport "
							+ " AND x.dataDisattivazione IS NULL " 
							+ " AND x.periodoRipartizione.ambito='PERFORMING' "
							+ " AND x.periodoRipartizione.repertorio='MUSICA'")
					.setParameter("id", periodoRipartizione).setParameter("voceIncasso", voceIncasso)
					.setParameter("tipologiaReport", tipologiaReport).getSingleResult();
			prPerfValorizzazione.setDataDisattivazione(new Date());
			prPerfValorizzazione.setUser(user);
			em.merge(prPerfValorizzazione);
			em.flush();
			em.persist(prPerfValorizzazione);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			System.out.println("ERROR deleteValorizzazioneConfigurazione: " + e);
			logger.error("deleteValorizzazioneConfigurazione: ", e);
			throw (e);
		}
	}

	@Override
	public CarichiRipartizioneResponse getCarichiRipartizione(String codicePeriodo) {
		EntityManager em = provider.get();
		PeriodoRipartizione p = null;
		boolean periodiRipartiti = false;
		if (TextUtils.isEmpty(codicePeriodo)) {
			try {
				p = (PeriodoRipartizione) em.createQuery("SELECT x from PeriodoRipartizione x"
						+ " where x.repertorio='MUSICA' " + " and x.ambito='PERFORMING' "
						+ " and x.dataApprovazioneCarichiRiparto IS NULL " + " order by x.competenzeDa asc ")
						.setMaxResults(1).getSingleResult();
			} catch (Exception e) {
				periodiRipartiti = true;
				p = (PeriodoRipartizione) em
						.createQuery("SELECT x from PeriodoRipartizione x" + " where x.repertorio='MUSICA' "
								+ " and x.ambito='PERFORMING' " + " order by x.competenzeDa desc ")
						.setMaxResults(1).getSingleResult();
			}

		} else {
			p = (PeriodoRipartizione) em
					.createQuery("SELECT x from PeriodoRipartizione x" + " where x.repertorio='MUSICA' "
							+ " and x.ambito='PERFORMING' " + " and x.codice=:codice "
							+ " order by x.competenzeDa asc ")
					.setParameter("codice", codicePeriodo).setMaxResults(1).getSingleResult();
		}

		Query q = em.createQuery("Select x from CarichiRipartizione x " + " where "
				+ " x.idPeriodoRipartizione =:id and " + " x.idPeriodoRipartizioneCompetenza= :id and "
				+ " x.idRunValorizzazione=(Select max(x.idRunValorizzazione) "
				+ " from CarichiRipartizione x where x.idPeriodoRipartizione =:id " + " ) order by x.voceIncasso asc")
				.setParameter("id", p.getIdPeriodoRipartizione());
		List<CarichiRipartizione> result = q.getResultList();

		Query q1 = em.createQuery("Select x from CarichiRipartizione x " + " where "
				+ " x.idPeriodoRipartizione =:id and " + " x.idPeriodoRipartizioneCompetenza<>:id and "
				+ " x.idRunValorizzazione=(Select max(x.idRunValorizzazione) "
				+ " from CarichiRipartizione x where x.idPeriodoRipartizione =:id ) " + " order by x.voceIncasso asc ")
				.setParameter("id", p.getIdPeriodoRipartizione());
		List<CarichiRipartizione> result1 = q1.getResultList();

		List<CarichiRipartizioneDTO> carichiRipartizioneDTOs = new ArrayList<>();
		for (CarichiRipartizione row : result) {
			CarichiRipartizioneDTO carichiRipartizioneDTO = new CarichiRipartizioneDTO(row, p.getCodice());
			carichiRipartizioneDTOs.add(carichiRipartizioneDTO);
		}

		HashMap<Long, List<CarichiRipartizioneDTO>> carichiRipartizioneMap = new HashMap<>();
		for (CarichiRipartizione row : result1) {
			CarichiRipartizioneDTO carichiRipartizioneDTO = new CarichiRipartizioneDTO(row, p.getCodice());
			if (carichiRipartizioneMap.containsKey(row.getIdPeriodoRipartizioneCompetenza())) {
				carichiRipartizioneMap.get(row.getIdPeriodoRipartizioneCompetenza()).add(carichiRipartizioneDTO);
			} else {
				List<CarichiRipartizioneDTO> list = new ArrayList<>();
				list.add(carichiRipartizioneDTO);
				carichiRipartizioneMap.put(row.getIdPeriodoRipartizioneCompetenza(), list);
			}

		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm");
		CarichiRipartizioneResponse response = null;

		response = new CarichiRipartizioneResponse(
				sdf.format(p.getCompetenzeDa()) + " - " + sdf.format(p.getCompetenzeA()), p.getCodice(),
				carichiRipartizioneDTOs, carichiRipartizioneMap, periodiRipartiti);

		return response;
	}

	@Override
	public Integer approvaRipartizioni(CarichiRipartizioniRequest carichiRipartizioniRequest) {
		EntityManager em = provider.get();
		try {
			em.getTransaction().begin();
			String joinedVociIncasso="";
			if (carichiRipartizioniRequest.getCarichiRipartizione()!=null) {
				 joinedVociIncasso = getConcactVoceIncasso(carichiRipartizioniRequest.getCarichiRipartizione());
			}

			// PRENDE PERIODO CORRENTE
			PeriodoRipartizione p = (PeriodoRipartizione) em.createQuery("SELECT x from PeriodoRipartizione x "
					+ "	where x.repertorio='MUSICA' " + " and x.ambito='PERFORMING' "
					+ " and x.dataApprovazioneCarichiRiparto IS NULL " + " order by x.competenzeDa asc ")
					.setMaxResults(1).getSingleResult();
			if (!TextUtils.isEmpty(joinedVociIncasso)) {
				em.createNativeQuery("UPDATE PERF_EVENTI_PAGATI SET ID_PERIODO_RIPARTIZIONE=" + p.getIdPeriodoRipartizione()
				+ " WHERE VOCE_INCASSO IN (" + joinedVociIncasso
				+ ") AND ID_PERIODO_RIPARTIZIONE IS NULL AND CONTABILITA >="
				+ getContabilitaFromDate(p.getCompetenzeDa()) + " AND CONTABILITA <= "
				+ getContabilitaFromDate(p.getCompetenzeA())).executeUpdate();
			}
			
			if (carichiRipartizioniRequest.getCarryOver()!=null) {
				for (CarichiRipartizioneDTO caricoPeriodo : carichiRipartizioniRequest.getCarryOver()) {

					PeriodoRipartizione p1 = (PeriodoRipartizione) em
							.createQuery("SELECT x from PeriodoRipartizione x " + "	where x.idPeriodoRipartizione=:id")
							.setParameter("id", caricoPeriodo.getPeriodoRipartizioneCompetenza()).setMaxResults(1)
							.getSingleResult();

					em.createNativeQuery("UPDATE PERF_EVENTI_PAGATI SET ID_PERIODO_RIPARTIZIONE="
							+ p.getIdPeriodoRipartizione() + " WHERE VOCE_INCASSO = " + caricoPeriodo.getVoceIncasso()
							+ " AND ID_PERIODO_RIPARTIZIONE IS NULL AND CONTABILITA < "
							+ getContabilitaFromDate(p1.getCompetenzeDa())).executeUpdate();
				}
			}
			
			em.flush();

			em.createNativeQuery("UPDATE PERF_MOVIMENTO_CONTABILE mc\n" + "        JOIN\n"
					+ "    PERF_EVENTI_PAGATI ep ON (ep.VOCE_INCASSO = mc.CODICE_VOCE_INCASSO\n"
					+ "        AND ep.ID_EVENTO = mc.ID_EVENTO\n"
					+ "        AND ep.NUMERO_FATTURA = mc.NUMERO_FATTURA) \n" + "SET \n"
					+ "    mc.ID_PERIODO_RIPARTIZIONE = ?1\n"
					+ "WHERE ep.ID_PERIODO_RIPARTIZIONE = ?1 AND mc.ID_PERIODO_RIPARTIZIONE IS NULL;")
					.setParameter(1, p.getIdPeriodoRipartizione()).executeUpdate();

		
			
			//TODO GESTIRE LA RIPARTIZIONE PER VOCI INCASSO FITTIZIE
			Query q = em.createQuery("Select x from PerfVoceIncasso x where x.flagVoceRipartizioneFittizia=true");
			List<PerfVoceIncasso> result = (List<PerfVoceIncasso>) q.getResultList();
			for (PerfVoceIncasso voceIncassoFittizia : result) {
					em.createNativeQuery("UPDATE PERF_MOVIMENTO_CONTABILE SET ID_PERIODO_RIPARTIZIONE="
							+ p.getIdPeriodoRipartizione() + " WHERE CODICE_VOCE_INCASSO = "
							+ voceIncassoFittizia.getCodVoceIncasso() + " AND ID_PERIODO_RIPARTIZIONE IS NULL")
							.executeUpdate();
			}

				em.createNativeQuery(
						"UPDATE PERF_CARICO_RIPARTIZIONE SET FLAG_IN_RIPARTIZIONE=1 WHERE ID_PERF_CARICO_RIPARTIZIONE IN ("
								+ getConcactId(carichiRipartizioniRequest.getCarichiRipartizione(),
										carichiRipartizioniRequest.getCarryOver())
								+ ")")
						.executeUpdate();
			
			p.setStatoApprovazioneCarichiRiparto(true);
			p.setDataApprovazioneCarichiRiparto(new Date());
			p.setApprovatoreCarichiRiparto(carichiRipartizioniRequest.getUser());
			em.merge(p);
			em.getTransaction().commit();
			return 200;
		} catch (Exception e) {
			em.getTransaction().rollback();
			return 500;
		}
	}

	private String getConcactVoceIncasso(List<CarichiRipartizioneDTO> carichiRipartizioni) {
		String separator = ", ";
		String joinedVociIncasso = "";
		StringBuffer bufferVociIncasso = new StringBuffer();
		for (CarichiRipartizioneDTO row : carichiRipartizioni) {
			if (row.getRipartizione()) {
				bufferVociIncasso.append("'" + row.getVoceIncasso() + "'");
				bufferVociIncasso.append(separator);
			}
		}
		joinedVociIncasso = bufferVociIncasso.toString();
		if (!TextUtils.isEmpty(joinedVociIncasso)) {
			joinedVociIncasso = joinedVociIncasso.substring(0, joinedVociIncasso.length() - separator.length());
		}
		return joinedVociIncasso.toString();
	}

	private String getConcactId(List<CarichiRipartizioneDTO> carichiRipartizioni,
			List<CarichiRipartizioneDTO> carryOver) {
		String separator = ", ";
		String joined = "";
		StringBuffer buffer = new StringBuffer();

		if (carichiRipartizioni!=null) {
			for (CarichiRipartizioneDTO row : carichiRipartizioni) {
				if (row.getRipartizione()) {
					buffer.append(row.getId() + "");
					buffer.append(separator);
				}
			}
		}
		if (carryOver!=null) {
			for (CarichiRipartizioneDTO carico : carryOver) {
				if (carico.getRipartizione()) {
					buffer.append(carico.getId() + "");
					buffer.append(separator);
				}
			}
		}

		joined = buffer.toString();
		joined = joined.substring(0, joined.length() - separator.length());

		return joined.toString();
	}

	private String getContabilitaFromDate(Date contabilita) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(contabilita);
		return calendar.get(Calendar.YEAR) + ""
				+ ((calendar.get(Calendar.MONTH) + 1) < 10 ? "0" + (calendar.get(Calendar.MONTH) + 1)
						: "" + (calendar.get(Calendar.MONTH) + 1));
	}



	public ValorizzazioneRunDTO getValorizzazioneInfo() {
		EntityManager em = provider.get();
		// PRENDE PERIODO CORRENTE
		PeriodoRipartizione p = (PeriodoRipartizione) em.createQuery("SELECT x from PeriodoRipartizione x "
				+ "	where x.repertorio='MUSICA' " + " and x.ambito='PERFORMING' "
				+ " and x.dataApprovazioneCarichiRiparto IS NULL " + " order by x.competenzeDa asc ").setMaxResults(1)
				.getSingleResult();

		String result = "%OK%";
		PerfValorizzazioneRun perfValorizzazioneRun = em.createQuery(
				"Select x from PerfValorizzazioneRun x where x.infoRun like :result order by x.idValorizzazioneRun desc",PerfValorizzazioneRun.class).setMaxResults(1).setParameter("result", result).getSingleResult();
		ValorizzazioneRunDTO valorizzazioneRunDTO= new ValorizzazioneRunDTO(perfValorizzazioneRun);
		return valorizzazioneRunDTO;
	}

}
