package com.alkemytech.sophia.codman.rest;

import com.alkemytech.sophia.codman.dto.*;
import com.alkemytech.sophia.codman.guice.McmdbDataSource;
import com.alkemytech.sophia.common.s3.S3Service;
import com.alkemytech.sophia.commons.aws.SQS;
import com.alkemytech.sophia.commons.sqs.SqsMessagePump;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.config.ResultType;
import org.eclipse.persistence.sessions.Record;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

@Singleton
@Path("preparatory-activities")
public class PreparatoryActivitiesController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final Gson gson;
    private final Provider<EntityManager> entityManagerProvider;
    private final S3Service s3Service;
    private final SQS sqs;
    private final Properties configuration;
    
    @Inject
    protected PreparatoryActivitiesController(Gson gson, @McmdbDataSource Provider<EntityManager> entityManagerProvider,
                                              S3Service s3Service, SQS sqs, @Named("configuration") Properties configuration) {
        super();
        this.gson = gson;
        this.entityManagerProvider = entityManagerProvider;
        this.s3Service = s3Service;
        this.sqs = sqs;
        this.configuration = configuration;
    }

    @Path("search")
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    public Response searchProcess(ProcessSearchDTO payload) {
        try {
            ProcessMetaDataDTO p1 = new ProcessMetaDataDTO();
            p1.setProcessID("UPDATE_DOCS");
            p1.setDescription("Aggiornamento documentazione");
            p1.setTag("preparatoryActities");
            p1.setExecutions(new ArrayList<ProcessExecutionDTO>());

            ProcessMetaDataDTO p2 = new ProcessMetaDataDTO();
            p2.setProcessID("UPDATE_RAYALTIES");
            p2.setDescription("Aggiornamento Schemi di Riparto");
            p2.setTag("preparatoryActities");
            p2.setExecutions(new ArrayList<ProcessExecutionDTO>());

            ProcessMetaDataDTO p3 = new ProcessMetaDataDTO();
            p3.setProcessID("UPDATE_KB");
            p3.setDescription("Aggiornamento KB");
            p3.setTag("preparatoryActities");
            p3.setExecutions(new ArrayList<ProcessExecutionDTO>());

            return Response.ok(gson.toJson(Arrays.asList(p1, p2, p3))).build();
        } catch (Exception e) {
            logger.error(String.format("Errore..."), e);
            return Response.status(500).build();
        }
    }

    @Path("start-processes")
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    public Response startProcesses(ProcessesStartRequestDTO request) {
        try {
            for (ProcessMetaDataDTO process : request.getProcesses()) {
                ProcessOperationOutcomeDTO outcome = new ProcessOperationOutcomeDTO();
                outcome.setStatus("OK");
                outcome.setMessage("Successful Operation!!");
                process.setOutcome(outcome);
            }
            return Response.ok(gson.toJson(request.getProcesses())).build();
        } catch (Exception e) {
            logger.error(String.format("Errore..."), e);
            return Response.status(500).build();
        }
    }

    @Path("start-update-royalties")
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    public Response startUpdateRoyalties(UpdateRoyaltiesRequestDTO request) {
        
        
        //verifica sovrapposizioni
        EntityManager em = null;
        em = entityManagerProvider.get();
        
        String sql="select * from(\n" +
                "select DSR_METADATA.IDDSR,DSR_METADATA.YEAR,PERIOD,PERIOD_TYPE,\n" +
                "       case when (PERIOD_TYPE='month') then PERIOD ELSE (PERIOD*3)-2 END MESE,\n" +
                "case\n" +
                "\t\twhen (EXTRACT_STATUS = 'KO'\n" +
                "               or CLEAN_STATUS = 'KO'\n" +
                "               or IDENTIFY_STATUS = 'KO'\n" +
                "               or CLAIM_STATUS = 'KO') then 'ERROR'\n" +
                "       when (CLAIM_STATUS = 'OK') then 'COMPLETED'\n" +
                "       when (DSR_STEPS_MONITORING.IDDSR is null) then 'ARRIVED'\n" +
                "       else 'PROCESSING' END as STATUS\n" +
                "from DSR_STEPS_MONITORING\n" +
                "join DSR_METADATA on DSR_METADATA.IDDSR=DSR_STEPS_MONITORING.IDDSR\n" +
                "where case\n" +
                "\t\twhen (EXTRACT_STATUS = 'KO'\n" +
                "               or CLEAN_STATUS = 'KO'\n" +
                "               or IDENTIFY_STATUS = 'KO'\n" +
                "               or CLAIM_STATUS = 'KO') then 'ERROR'\n" +
                "       when (CLAIM_STATUS = 'OK') then 'COMPLETED'\n" +
                "       when (DSR_STEPS_MONITORING.IDDSR is null) then 'ARRIVED'\n" +
                "       else 'PROCESSING' END ='PROCESSING') a\n" +
                "where a.MESE=? and a.year=?";

        List resultList = em.createNativeQuery(sql)
                .setParameter(1, request.getMonth())
                .setParameter(2, request.getYear())
                .getResultList();

        if(resultList == null || resultList.size() == 0) {
            
            //check double invio
            String queryControl="select CASE WHEN max(INSERT_TIME)+interval 2 hour > now() then 'IN CORSO' ELSE null END data_insert\n" +
                    " from MM_PROCESS_EXECUTION\n" +
                    "  where PROCESS='start-update-royalties' and MESE=? and ANNO=? ";

            List resultList2 = em.createNativeQuery(queryControl)
                    .setParameter(1, request.getMonth())
                    .setParameter(2, request.getYear())
                    .getResultList();

            if(resultList2 == null || resultList2.size() == 0 || resultList2.get(0) == null) {

                String s = "INSERT INTO MM_PROCESS_EXECUTION(PROCESS,MESE,ANNO) values('start-update-royalties',?,?)";
                em.getTransaction().begin();
                em.createNativeQuery(s)
                        .setParameter(1, request.getMonth())
                        .setParameter(2, request.getYear())
                        .executeUpdate();
                em.getTransaction().commit();


                SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration, "update_royalties.sqs");
                JsonObject message = new JsonObject();
                message.addProperty("month", request.getMonth());
                message.addProperty("year", request.getYear());
                sqsMessagePump.sendToProcessMessage(message, false);
                return Response.ok().build();
            }else{
                return Response.status(500).entity(gson.toJson("Errore: Periodo in lavorazione")).build();
            }
        }else{
            return Response.status(500).entity(gson.toJson("Errore: esistono DSR in esecuzione per il periodo selezionato")).build();
        }
    }

    @Path("start-update-siae-documentation")
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    public Response startUpdateSiaeDocumentation() {


        EntityManager em = null;
        em = entityManagerProvider.get();
        //check double invio
        String queryControl="select CASE WHEN max(INSERT_TIME)+interval 1 hour > now() then 'IN CORSO' ELSE null END data_insert\n" +
                "from MM_PROCESS_EXECUTION\n" +
                "  where PROCESS='start-update-siae-documentation' ";

        String queryControl2="select * from (\n" +
                "select QUEUE_TYPE from SQS_SERVICE_BUS where SERVICE_NAME = 'royalties' order by MESSAGE_TIME desc limit 1) a\n" +
                "where QUEUE_TYPE in ('started','to_process');";



        List resultList2 = em.createNativeQuery(queryControl)
                .getResultList();

        List resultList3 = em.createNativeQuery(queryControl2)
                .getResultList();

        if((resultList2 == null || resultList2.size() == 0 || resultList2.get(0) == null) &&
                (resultList3 == null || resultList3.size() == 0 || resultList3.get(0) == null)){

            String s = "INSERT INTO MM_PROCESS_EXECUTION(PROCESS) values('start-update-siae-documentation')";
            em.getTransaction().begin();
            em.createNativeQuery(s)
                    .executeUpdate();
            em.getTransaction().commit();


            SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration, "update_siae_documentation.sqs");
            JsonObject message = new JsonObject();
            message.addProperty("force", "true");
            sqsMessagePump.sendToProcessMessage(message, false);
            return Response.ok().build();
        }else{
            return Response.status(500).entity(gson.toJson("Errore: Aggiornamento documentazione SIAE in corso")).build();
        }
    }

    @Path("start-update-ucmr-ada-documentation")
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    public Response startUpdateUcmrAdaDocumentation() {

        EntityManager em = null;
        em = entityManagerProvider.get();
        String queryControl="select CASE WHEN max(INSERT_TIME)+interval 1 hour > now() then 'IN CORSO' ELSE null END data_insert\n" +
                "from MM_PROCESS_EXECUTION\n" +
                "  where PROCESS='start-update-ucmr-ada-documentation' ";

        String queryControl2="select * from (\n" +
                "select QUEUE_TYPE from SQS_SERVICE_BUS where SERVICE_NAME = 'ucmr_ada_royalties' order by MESSAGE_TIME desc limit 1) a\n" +
                "where QUEUE_TYPE in ('started','to_process');";



        List resultList2 = em.createNativeQuery(queryControl)
                .getResultList();

        List resultList3 = em.createNativeQuery(queryControl2)
                .getResultList();

        if((resultList2 == null || resultList2.size() == 0 || resultList2.get(0) == null) &&
                (resultList3 == null || resultList3.size() == 0 || resultList3.get(0) == null)){
            
            String s = "INSERT INTO MM_PROCESS_EXECUTION(PROCESS) values('start-update-ucmr-ada-documentation')";
            em.getTransaction().begin();
            em.createNativeQuery(s)
                    .executeUpdate();
            em.getTransaction().commit();

            SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration, "update_ucmr_ada_documentation.sqs");
            JsonObject message = new JsonObject();
            message.addProperty("force", "true");
            sqsMessagePump.sendToProcessMessage(message, false);
            return Response.ok().build();
        }else{
            return Response.status(500).entity(gson.toJson("Errore: Aggiornamento documentazione UCMR-ADA in corso")).build();
        }
    }


    @Path("start-update-kb")
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    public Response startUpdateKB() {

        EntityManager em = null;
        em = entityManagerProvider.get();
        String queryControl="select CASE WHEN max(INSERT_TIME)+interval 1 hour > now() then 'IN CORSO' ELSE null END data_insert\n" +
                "from MM_PROCESS_EXECUTION\n" +
                "  where PROCESS='start-update-kb' ";

        String queryControl2="select * from (\n" +
                "select QUEUE_TYPE from SQS_SERVICE_BUS where SERVICE_NAME = 'update_kb' order by MESSAGE_TIME desc limit 1) a\n" +
                "where QUEUE_TYPE in ('started','to_process');";
        
        

        List resultList2 = em.createNativeQuery(queryControl)
                .getResultList();

        //List resultList3 = em.createNativeQuery(queryControl2)
        //          .getResultList();

        if (resultList2 == null || resultList2.size() == 0 || resultList2.get(0) == null){
        //if((resultList2 == null || resultList2.size() == 0 || resultList2.get(0) == null) &&
        //(resultList3 == null || resultList3.size() == 0 || resultList3.get(0) == null)){

            //List statusLastUpdate =  em.createNativeQuery(queryControl2).getResultList();
            
            String s = "INSERT INTO MM_PROCESS_EXECUTION(PROCESS) values('start-update-kb')";
            em.getTransaction().begin();
            em.createNativeQuery(s)
                    .executeUpdate();
            em.getTransaction().commit();


            SqsMessagePump sqsMessagePump = new SqsMessagePump(sqs, configuration, "update_kb.sqs");
            sqsMessagePump.sendToProcessMessage(new JsonObject(), false);
            return Response.ok().build();
        }else{
            return Response.status(500).entity(gson.toJson("Errore: Aggiorno KB in corso")).build();
        }
    }

    @Path("process-executions")
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    public Response executions(ProcessMetaDataDTO process) {
        Gson gson = new GsonBuilder().serializeNulls().create();

        try {
            if ("UPDATE_RAYALTIES".equals(process.getProcessID())) {
                EntityManager entityManager = entityManagerProvider.get();
                entityManager.getTransaction().begin();
                entityManager.createNativeQuery("set session lc_time_names = 'it_IT'").executeUpdate();
                List resultList = entityManager.createNativeQuery(
                        "select update_id                                            id_lancio,\n" +
                                "       SERVICE_NAME as                                      service,\n" +
                                "       periodo,\n" +
                                "       date_format(max(b.data_inizio), '%d %b %Y %H:%i:%s') data_inizio,\n" +
                                "       date_format(max(b.data_fine), '%d %b %Y %H:%i:%s')   data_fine,\n" +
                                "       coalesce(max(esito_1), max(esito_2), max(esito_3))   esito\n" +
                                "from (select case when QUEUE_TYPE = 'started' then MESSAGE_TIME end                data_inizio,\n" +
                                "             case when QUEUE_TYPE in ('completed', 'failed') then MESSAGE_TIME end data_fine,\n" +
                                "             case when QUEUE_TYPE = 'failed' then 'Fallito' end                    esito_1,\n" +
                                "             case when QUEUE_TYPE = 'completed' then 'Completato' end              esito_2,\n" +
                                "             case when QUEUE_TYPE = 'started' then 'In corso' end                  esito_3,\n" +
                                "             concat(MONTH, '-', YEAR)                                              periodo,\n" +
                                "             update_id,\n" +
                                "             SERVICE_NAME\n" +
                                "      from (select QUEUE_TYPE,\n" +
                                "                   SERVICE_NAME,\n" +
                                "                   MESSAGE_TIME,\n" +
                                "                   MONTH,\n" +
                                "                   YEAR,\n" +
                                "                   json_extract(MESSAGE_JSON, '$.input.body.updateId') update_id\n" +
                                "            from SQS_SERVICE_BUS\n" +
                                "            where SERVICE_NAME like '%dump_v2'\n" +
                                "              and json_extract(MESSAGE_JSON, '$.input.body.updateId') is not null\n" +
                                "           ) a\n" +
                                "      order by data_fine desc, data_inizio desc\n" +
                                "     ) b\n" +
                                "         join (select json_extract(MESSAGE_JSON, '$.input.body.updateId') updateId,\n" +
                                "                      max(MESSAGE_TIME) min_date\n" +
                                "               from SQS_SERVICE_BUS\n" +
                                "               where SERVICE_NAME like '%dump_v2'\n" +
                                "                 and json_extract(MESSAGE_JSON, '$.input.body.updateId') is not null\n" +
                                "               group by updateId) c on b.update_id = c.updateId\n" +
                                "group by update_id, SERVICE_NAME, periodo\n" +
                                "order by c.min_date desc, data_inizio desc")
                        .setHint(QueryHints.RESULT_TYPE, ResultType.Map)
                        .getResultList();
                entityManager.getTransaction().commit();
                process.setExecutions(resultList);
            } else if ("UPDATE_DOCS".equals(process.getProcessID())) {
                /*Set<JsonObject> docs = new TreeSet<>(new Comparator<JsonObject>() {
                    @Override
                    public int compare(JsonObject o1, JsonObject o2) {
                        return o2.getAsJsonPrimitive("data")
                                .getAsString()
                                .compareTo(o1
                                        .getAsJsonPrimitive("data")
                                        .getAsString());
                    }
                });*/
                List<Record> mandatorsDocs = entityManagerProvider.get()
                        .createNativeQuery("select launch_id id_lancio,\n" +
                                "       case when(SERVICE_NAME= 'royalties') then 'Documentazione SIAE' else 'Documentazione UCMR-ADA' END  as                                      service,\n" +
                                "       date_format(max(b.data_inizio), '%d %b %Y %H:%i:%s') data_inizio,\n" +
                                "       date_format(max(b.data_fine), '%d %b %Y %H:%i:%s')   data_fine,\n" +
                                "       coalesce(max(esito_1), max(esito_2), max(esito_3))   esito\n" +
                                "from (select case when (QUEUE_TYPE = 'started'or QUEUE_TYPE = 'to_process') then MESSAGE_TIME end                data_inizio,\n" +
                                "             case when QUEUE_TYPE in ('completed', 'failed') then MESSAGE_TIME end data_fine,\n" +
                                "             case when QUEUE_TYPE = 'failed' then 'Fallito' end                    esito_1,\n" +
                                "             case when QUEUE_TYPE = 'completed' then 'Completato' end              esito_2,\n" +
                                "             case when (QUEUE_TYPE = 'started' or QUEUE_TYPE = 'to_process') then 'In corso' end                  esito_3,\n" +
                                "             launch_id,\n" +
                                "             SERVICE_NAME\n" +
                                "      from (select QUEUE_TYPE,\n" +
                                "                   SERVICE_NAME,\n" +
                                "                   MESSAGE_TIME,\n" +
                                "                   coalesce( json_extract(MESSAGE_JSON, '$.input.header.uuid'),json_extract(MESSAGE_JSON, '$.header.uuid')  )launch_id\n" +
                                "            from SQS_SERVICE_BUS\n" +
                                "            where SERVICE_NAME like '%royalties%' and (json_extract(MESSAGE_JSON, '$.input.header.uuid') is not null " +
                                "   or (json_extract(MESSAGE_JSON, '$.header.uuid') is not null)) and QUEUE_TYPE != 'STARTED' \n" +
                                "           ) a\n" +
                                "      order by data_fine desc, data_inizio desc\n" +
                                "     ) b\n" +
                                "group by launch_id, SERVICE_NAME\n" +
                                "order by data_inizio desc")
                        .setHint(QueryHints.RESULT_TYPE, ResultType.Map)
                        .getResultList();

                /*for (Record mandatorsDoc : mandatorsDocs) {
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("societa", (String) mandatorsDoc.get("SOCIETA"));
                    jsonObject.addProperty("data", (String) mandatorsDoc.get("DATA_INSERIMENTO"));
                    docs.add(jsonObject);
                }

                Pattern regex = Pattern.compile("elastic-dump/([0-9]{4})_([0-9]{2})_([0-9]{2})_export_ulisse_ipi_preprod/.+");
                List<S3ObjectSummary> objects = s3Service.listObjects("siae-sophia-datalake", "elastic-dump/");
                for (S3ObjectSummary object : objects) {
                    Matcher matcher = regex.matcher(object.getKey());
                    if (matcher.matches()) {
                        JsonObject jsonObject = new JsonObject();
                        jsonObject.addProperty("societa", "SIAE");
                        jsonObject.addProperty("data", matcher.group(3) + "/" + matcher.group(2) + "/" + matcher.group(1));
                        docs.add(jsonObject);
                    }
                }*/
                process.setExecutions(mandatorsDocs);
            } else if ("UPDATE_KB".equals(process.getProcessID())) {
                EntityManager entityManager = entityManagerProvider.get();
                entityManager.getTransaction().begin();
                entityManager.createNativeQuery("set session lc_time_names = 'it_IT'").executeUpdate();
                List resultList = entityManager.createNativeQuery(
                        "select MM_KB_UPDATE.LAUNCH_ID id_lancio,\n" +
                                "SERVICE_NAME service,\n" +
                                "STARTED data_inizio,\n" +
                                "FINISHED data_fine,\n" +
                                "STATUS esito\n" +
                                "from MM_KB_UPDATE join (select min(ID) data, LAUNCH_ID from MM_KB_UPDATE group by LAUNCH_ID) b on b.LAUNCH_ID=MM_KB_UPDATE.LAUNCH_ID\n" +
                                "order by b.data desc, MM_KB_UPDATE.LAUNCH_ID;")
                        .setHint(QueryHints.RESULT_TYPE, ResultType.Map)
                        .getResultList();
                entityManager.getTransaction().commit();
                process.setExecutions(resultList);
            }
            return Response.ok(gson.toJson(process)).build();
        } catch (Exception e) {
            logger.error(String.format("Errore..."), e);
            return Response.status(500).build();
        }
    }

}
