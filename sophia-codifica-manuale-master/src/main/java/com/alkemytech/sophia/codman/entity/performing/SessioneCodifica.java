package com.alkemytech.sophia.codman.entity.performing;

import java.util.Date;

import javax.persistence.*;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(name = "PERF_SESSIONE_CODIFICA")
public class SessioneCodifica {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name = "ID_SESSIONE_CODIFICA")
	private Long idSessione;
	@Column(name = "DATA_INIZIO_VALID")
	private Date dataInizioValidita;
	@Column(name = "DATA_FINE_VALID")
	private Date dataFineValidita;
	@Column(name = "UTENTE")
	private String utente;

	public SessioneCodifica() {
		super();
	}

	public SessioneCodifica(Long idSessione, Date dataInizioValidita, Date dataFineValidita, String utente) {
		super();
		this.idSessione = idSessione;
		this.dataInizioValidita = dataInizioValidita;
		this.dataFineValidita = dataFineValidita;
		this.utente = utente;
	}

	public Long getIdSessione() {
		return idSessione;
	}

	public void setIdSessione(Long idSessione) {
		this.idSessione = idSessione;
	}

	public Date getDataInizioValidita() {
		return dataInizioValidita;
	}

	public void setDataInizioValidita(Date dataInizioValidita) {
		this.dataInizioValidita = dataInizioValidita;
	}

	public Date getDataFineValidita() {
		return dataFineValidita;
	}

	public void setDataFineValidita(Date dataFineValidita) {
		this.dataFineValidita = dataFineValidita;
	}

	public String getUtente() {
		return utente;
	}

	public void setUtente(String utente) {
		this.utente = utente;
	}
}
