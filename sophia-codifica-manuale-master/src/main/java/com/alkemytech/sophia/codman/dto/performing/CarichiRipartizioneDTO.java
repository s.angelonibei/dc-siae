package com.alkemytech.sophia.codman.dto.performing;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.alkemytech.sophia.codman.entity.CarichiRipartizione;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class CarichiRipartizioneDTO {
	private Long id;
	private String voceIncasso;
	private String periodoRipartizione;
	private Double incassoNetto;
	private Double pmMancanti;
	private Double pmAnnullati;
	private Double caricoRipartizione;
	private Double nonCodificato;
	private Double bollettiniIrregolari;
	private Double causeSospensione;
	private Double caricoRipartibile;
	private Boolean ripartizione;
	private Long periodoRipartizioneCompetenza;

	public CarichiRipartizioneDTO() {
		super();
	}

	public CarichiRipartizioneDTO(String voceIncasso, String periodoRipartizione, Double incassoNetto,
			Double pmMancanti, Double pmAnnullati, Double caricoRipartizione, Double nonCodificato,
			Double bollettiniIrregolari, Double causeSospensione, Double caricoRipartibile, Boolean ripartizione, Long periodoRipartizioneCompetenza) {
		super();
		this.voceIncasso = voceIncasso;
		this.periodoRipartizione = periodoRipartizione;
		this.incassoNetto = incassoNetto;
		this.pmMancanti = pmMancanti;
		this.pmAnnullati = pmAnnullati;
		this.caricoRipartizione = caricoRipartizione;
		this.bollettiniIrregolari = bollettiniIrregolari;
		this.causeSospensione = causeSospensione;
		this.caricoRipartibile = caricoRipartibile;
		this.ripartizione = ripartizione;
		this.nonCodificato = nonCodificato;
		this.periodoRipartizioneCompetenza = periodoRipartizioneCompetenza;
	}

	public CarichiRipartizioneDTO(CarichiRipartizione row, String periodo) {
		this.id=row.getIdPerfCaricoRipartizione();
		this.voceIncasso = row.getVoceIncasso();
		this.periodoRipartizione = periodo;
		this.incassoNetto = row.getValoreIncassoNetto();
		this.pmMancanti = row.getValorePmMancanti();
		this.pmAnnullati = row.getValorePmAnnullati();
		this.caricoRipartizione = row.getValoreCaricoRipartizione();
		this.bollettiniIrregolari = getBollettiniIrregolari();
		this.causeSospensione = row.getValoreSospesi();
		this.caricoRipartibile = row.getValoreRipartibile();
		this.ripartizione = row.isFlagInRipartizione();
		this.nonCodificato = row.getValoreNonCodificato();
		this.periodoRipartizioneCompetenza = row.getIdPeriodoRipartizioneCompetenza();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPeriodoRipartizione() {
		return periodoRipartizione;
	}

	public void setPeriodoRipartizione(String periodoRipartizione) {
		this.periodoRipartizione = periodoRipartizione;
	}

	public Double getIncassoNetto() {
		return incassoNetto;
	}

	public void setIncassoNetto(Double incassoNetto) {
		this.incassoNetto = incassoNetto;
	}

	public Double getPmMancanti() {
		return pmMancanti;
	}

	public void setPmMancanti(Double pmMancanti) {
		this.pmMancanti = pmMancanti;
	}

	public Double getPmAnnullati() {
		return pmAnnullati;
	}

	public void setPmAnnullati(Double pmAnnullati) {
		this.pmAnnullati = pmAnnullati;
	}

	public Double getCaricoRipartizione() {
		return caricoRipartizione;
	}

	public void setCaricoRipartizione(Double caricoRipartizione) {
		this.caricoRipartizione = caricoRipartizione;
	}

	public Double getBollettiniIrregolari() {
		return bollettiniIrregolari;
	}

	public void setBollettiniIrregolari(Double bollettiniIrregolari) {
		this.bollettiniIrregolari = bollettiniIrregolari;
	}

	public Double getCauseSospensione() {
		return causeSospensione;
	}

	public void setCauseSospensione(Double causeSospensione) {
		this.causeSospensione = causeSospensione;
	}

	public Double getCaricoRipartibile() {
		return caricoRipartibile;
	}

	public void setCaricoRipartibile(Double caricoRipartibile) {
		this.caricoRipartibile = caricoRipartibile;
	}

	public Boolean getRipartizione() {
		return ripartizione;
	}

	public void setRipartizione(Boolean ripartizione) {
		this.ripartizione = ripartizione;
	}

	public String getVoceIncasso() {
		return voceIncasso;
	}

	public void setVoceIncasso(String voceIncasso) {
		this.voceIncasso = voceIncasso;
	}

	public Double getNonCodificato() {
		return nonCodificato;
	}

	public void setNonCodificato(Double nonCodificato) {
		this.nonCodificato = nonCodificato;
	}

	public Long getPeriodoRipartizioneCompetenza() {
		return periodoRipartizioneCompetenza;
	}

	public void setPeriodoRipartizioneCompetenza(Long periodoRipartizioneCompetenza) {
		this.periodoRipartizioneCompetenza = periodoRipartizioneCompetenza;
	}

}
