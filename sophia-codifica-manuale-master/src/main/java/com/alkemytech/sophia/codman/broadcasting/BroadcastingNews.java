package com.alkemytech.sophia.codman.broadcasting;

import com.alkemytech.sophia.broadcasting.dto.BdcNewsDTO;
import com.alkemytech.sophia.broadcasting.service.interfaces.BdcNewsService;
import com.alkemytech.sophia.broadcasting.dto.BdcNewsRequestDTO;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.util.List;

@Singleton
@Path("broadcastingNews")
public class BroadcastingNews {

    private final BdcNewsService bdcNewsService;
    private final Gson gson;

    @Inject
    public BroadcastingNews(BdcNewsService bdcNewsService, Gson gson){
        super();
        this.bdcNewsService=bdcNewsService;
        this.gson=gson;
    }

    @POST
    @Path("listNews")
    @Produces("application/json")
    public Response listNews(BdcNewsRequestDTO bdcNewsRequestDTO) {

        List<BdcNewsDTO> news = bdcNewsService.findNewsForBdc(bdcNewsRequestDTO.getIdUser(), bdcNewsRequestDTO.getIdBroadcaster());

        return Response.ok(gson.toJson(news)).build();
    }
}
