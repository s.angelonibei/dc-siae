package com.alkemytech.sophia.codman.entity;

import com.alkemytech.sophia.codman.invoice.repository.InvoiceItemToCcid;
import lombok.Data;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;

@XmlRootElement
@Entity(name = "ClaimConfig")
@Table(name = "MM_PERC_CLAIM_CONFIG")
@Data
public class ClaimConfig {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "CODICE_UUID", nullable = false)
    private String codiceUUID;

    @Column(name = "ID_DSP", nullable = false)
    private String dspId;

    @Column(name = "TERRITORIO", nullable = false)
    private String countryId;

    @Column(name = "TIPO_UTILIZZO", nullable = false)
    private String utilizationTypeId;

    @Column(name = "OFFERTA_COMMERCIALE", nullable = false)
    private String offerId;

    @Column(name = "PERC_DEM", nullable = false)
    private Double percDem;

    @Column(name = "PERC_DRM")
    private Double percDrm;

    @Column(name = "VALID_FROM")
    private Date validFrom;

    @Column(name = "VALID_TO")
    private Date validTo;

    @Column(name = "LAST_UPD")
    private Date lastUpdate;

    /* @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_DSP", referencedColumnName = "IDDSP", insertable = false, updatable = false)
    private AnagDsp dsp;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "TERRITORIO", referencedColumnName = "ID_COUNTRY", insertable = false, updatable = false)
    private AnagCountry country;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "TIPO_UTILIZZO", referencedColumnName = "ID_UTILIZATION_TYPE", insertable = false, updatable = false)
    private AnagUtilizationType utilizationType; */

    // @OneToOne(fetch = FetchType.LAZY)
    // @JoinColumn(name = "OFFERTA_COMMERCIALE", referencedColumnName = "ID_COMMERCIAL_OFFERS", insertable = false, updatable = false)
    // private CommercialOffers offer;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_CONFIG", referencedColumnName = "ID")
    private List<MMPercClaimResult> result;

    @PrePersist
    protected void onCreate() {
        lastUpdate = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        lastUpdate = new Date();
    }
}
