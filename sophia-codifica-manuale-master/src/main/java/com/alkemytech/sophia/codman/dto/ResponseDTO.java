package com.alkemytech.sophia.codman.dto;

import com.google.gson.annotations.SerializedName;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ResponseDTO {
    @SerializedName("pagination")
    PaginationDTO paginationDTO;
    List<?> list = new ArrayList<>();

    public ResponseDTO() {
    }

    public ResponseDTO(List<?> list) {
        this.list = list;
    }

    public ResponseDTO(PaginationDTO paginationDTO, List<?> list) {
        this.paginationDTO = paginationDTO;
        this.list = list;
    }

    public PaginationDTO getPaginationDTO() {
        return paginationDTO;
    }

    public void setPaginationDTO(PaginationDTO paginationDTO) {
        this.paginationDTO = paginationDTO;
    }

    public List<?> getList() {
        return list;
    }

    public void setList(List<?> list) {
        this.list = list;
    }
}
