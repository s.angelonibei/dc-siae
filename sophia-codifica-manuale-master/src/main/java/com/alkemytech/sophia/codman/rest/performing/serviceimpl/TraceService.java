package com.alkemytech.sophia.codman.rest.performing.serviceimpl;

import com.alkemytech.sophia.codman.entity.performing.TracciamentoApplicativo;
import com.alkemytech.sophia.codman.entity.performing.dao.IEsecuzioneDAO;
import com.alkemytech.sophia.codman.multimedialelocale.entity.UserRole;
import com.alkemytech.sophia.codman.rest.performing.Constants;
import com.alkemytech.sophia.codman.rest.performing.service.ITraceService;
import com.google.inject.Inject;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;

//import com.alkemytech.sophia.codman.entity.performing.security.User;

public class TraceService implements ITraceService {

	IEsecuzioneDAO esecuzioneDAO;

	@Inject
	public TraceService(IEsecuzioneDAO esecuzioneDAO) {
		this.esecuzioneDAO = esecuzioneDAO;
	}

	public void trace(String serviceName, String className, String methodName, String message, String logLevel,
			String username) {

		// TODO: change spring classes with guice classes
		// ServletRequestAttributes attr = (ServletRequestAttributes)
		// RequestContextHolder.currentRequestAttributes();
		// HttpServletRequest currentRequest = attr.getRequest();
		// UserRole user = (UserRole)currentRequest.getSession().getAttribute("user");

		// Utente Guest
		// if (user==null){
		UserRole user = new UserRole();

		// user.setUsername("GUEST");
		// }

		// final HttpSession session = request.getSession(false);
		// session.getAttribute("username");
		// request.getParameter("username");
		// UserRole user=new UserRole();

		if (username == null || username.equals("")) {
			user = new UserRole();

			user.setUsername("GUEST");
		} else {
			user.setUsername(username);
		}

		String hostName = " ";

		try {

			hostName = InetAddress.getLocalHost().getHostName();

		} catch (UnknownHostException e) {
			e.printStackTrace();
		}

		TracciamentoApplicativo tracciamentoApplicativo = new TracciamentoApplicativo();

		tracciamentoApplicativo.setApplication(Constants.APPID);

		tracciamentoApplicativo.setDataInserimento(new Date());
		tracciamentoApplicativo.setUserName(user.getUsername());
		tracciamentoApplicativo.setHostName(hostName);
		tracciamentoApplicativo.setSessionId(null);

		tracciamentoApplicativo.setLogLevel(logLevel);
		tracciamentoApplicativo.setServiceName(serviceName);
		tracciamentoApplicativo.setClassName(className);
		tracciamentoApplicativo.setMethodName(methodName);
		tracciamentoApplicativo.setMessage(message);

		esecuzioneDAO.trace(tracciamentoApplicativo);

	}

}
