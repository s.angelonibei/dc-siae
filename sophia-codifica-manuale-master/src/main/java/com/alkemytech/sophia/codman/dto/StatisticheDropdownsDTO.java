package com.alkemytech.sophia.codman.dto;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class StatisticheDropdownsDTO {
    private List<DropdownDTO> utilizations;
    private List<DropdownDTO> commercialOffers;

    public List<DropdownDTO> getUtilizations() {
        return utilizations;
    }

    public void setUtilizations(List<DropdownDTO> utilizations) {
        this.utilizations = utilizations;
    }

    public List<DropdownDTO> getCommercialOffers() {
        return commercialOffers;
    }

    public void setCommercialOffers(List<DropdownDTO> commercialOffers) {
        this.commercialOffers = commercialOffers;
    }
}
