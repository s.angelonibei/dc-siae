package com.alkemytech.sophia.codman.dto.performing;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class RsUtenteDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Boolean sendMail;
	
	private BigInteger id;

	private Date dataCreazione;

	private Date dataUltimaModifica;

	private String email;

	private BigInteger idMusicProvider;

	private String password;

	private String username;

	private String utenteUltimaModifica;

	public RsUtenteDTO() {
	}

	public BigInteger getId() {
		return this.id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public Date getDataCreazione() {
		return this.dataCreazione;
	}

	public void setDataCreazione(Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}

	public Date getDataUltimaModifica() {
		return this.dataUltimaModifica;
	}

	public void setDataUltimaModifica(Date dataUltimaModifica) {
		this.dataUltimaModifica = dataUltimaModifica;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public BigInteger getIdMusicProvider() {
		return this.idMusicProvider;
	}

	public void setIdMusicProvider(BigInteger idMusicProvider) {
		this.idMusicProvider = idMusicProvider;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUtenteUltimaModifica() {
		return this.utenteUltimaModifica;
	}

	public void setUtenteUltimaModifica(String utenteUltimaModifica) {
		this.utenteUltimaModifica = utenteUltimaModifica;
	}

	public Boolean getSendMail() {
		return sendMail;
	}

	public void setSendMail(Boolean sendMail) {
		this.sendMail = sendMail;
	}


}