package com.alkemytech.sophia.performing.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Entity
@Table(name = "PERF_NDM_VOCE_FATTURA")
public class PerfNdmVoceFattura {
    private long idNdmVoceFattura;
    private long idNdmFile;
    private int annoFattura;
    private int meseFattura;
    private Timestamp dataFattura;
    private int annoContabileFattura;
    private int meseContabileFattura;
    private Timestamp dataContabileFattura;
    private Long idIncasso;
    private Long idQuietanza;
    private String numeroReversale;
    private String numeroFattura;
    private String voce;
    private BigDecimal importoOriginale;
    private BigDecimal importoAggio;
    private BigDecimal percentualeAggio;

    @Id
    @Column(name = "ID_NDM_VOCE_FATTURA")
    public long getIdNdmVoceFattura() {
        return idNdmVoceFattura;
    }

    public void setIdNdmVoceFattura(long idNdmVoceFattura) {
        this.idNdmVoceFattura = idNdmVoceFattura;
    }

    @Basic
    @Column(name = "ID_NDM_FILE")
    public long getIdNdmFile() {
        return idNdmFile;
    }

    public void setIdNdmFile(long idNdmFile) {
        this.idNdmFile = idNdmFile;
    }

    @Basic
    @Column(name = "ANNO_FATTURA")
    public int getAnnoFattura() {
        return annoFattura;
    }

    public void setAnnoFattura(int annoFattura) {
        this.annoFattura = annoFattura;
    }

    @Basic
    @Column(name = "MESE_FATTURA")
    public int getMeseFattura() {
        return meseFattura;
    }

    public void setMeseFattura(int meseFattura) {
        this.meseFattura = meseFattura;
    }

    @Basic
    @Column(name = "DATA_FATTURA")
    public Timestamp getDataFattura() {
        return dataFattura;
    }

    public void setDataFattura(Timestamp dataFattura) {
        this.dataFattura = dataFattura;
    }

    @Basic
    @Column(name = "ANNO_CONTABILE_FATTURA")
    public int getAnnoContabileFattura() {
        return annoContabileFattura;
    }

    public void setAnnoContabileFattura(int annoContabileFattura) {
        this.annoContabileFattura = annoContabileFattura;
    }

    @Basic
    @Column(name = "MESE_CONTABILE_FATTURA")
    public int getMeseContabileFattura() {
        return meseContabileFattura;
    }

    public void setMeseContabileFattura(int meseContabileFattura) {
        this.meseContabileFattura = meseContabileFattura;
    }

    @Basic
    @Column(name = "DATA_CONTABILE_FATTURA")
    public Timestamp getDataContabileFattura() {
        return dataContabileFattura;
    }

    public void setDataContabileFattura(Timestamp dataContabileFattura) {
        this.dataContabileFattura = dataContabileFattura;
    }

    @Basic
    @Column(name = "ID_INCASSO")
    public Long getIdIncasso() {
        return idIncasso;
    }

    public void setIdIncasso(Long idIncasso) {
        this.idIncasso = idIncasso;
    }

    @Basic
    @Column(name = "ID_QUIETANZA")
    public Long getIdQuietanza() {
        return idQuietanza;
    }

    public void setIdQuietanza(Long idQuietanza) {
        this.idQuietanza = idQuietanza;
    }

    @Basic
    @Column(name = "NUMERO_REVERSALE")
    public String getNumeroReversale() {
        return numeroReversale;
    }

    public void setNumeroReversale(String numeroReversale) {
        this.numeroReversale = numeroReversale;
    }

    @Basic
    @Column(name = "NUMERO_FATTURA")
    public String getNumeroFattura() {
        return numeroFattura;
    }

    public void setNumeroFattura(String numeroFattura) {
        this.numeroFattura = numeroFattura;
    }

    @Basic
    @Column(name = "VOCE")
    public String getVoce() {
        return voce;
    }

    public void setVoce(String voce) {
        this.voce = voce;
    }

    @Basic
    @Column(name = "IMPORTO_ORIGINALE")
    public BigDecimal getImportoOriginale() {
        return importoOriginale;
    }

    public void setImportoOriginale(BigDecimal importoOriginale) {
        this.importoOriginale = importoOriginale;
    }

    @Basic
    @Column(name = "IMPORTO_AGGIO")
    public BigDecimal getImportoAggio() {
        return importoAggio;
    }

    public void setImportoAggio(BigDecimal importoAggio) {
        this.importoAggio = importoAggio;
    }

    @Basic
    @Column(name = "PERCENTUALE_AGGIO")
    public BigDecimal getPercentualeAggio() {
        return percentualeAggio;
    }

    public void setPercentualeAggio(BigDecimal percentualeAggio) {
        this.percentualeAggio = percentualeAggio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PerfNdmVoceFattura that = (PerfNdmVoceFattura) o;

        if (idNdmVoceFattura != that.idNdmVoceFattura) return false;
        if (idNdmFile != that.idNdmFile) return false;
        if (annoFattura != that.annoFattura) return false;
        if (meseFattura != that.meseFattura) return false;
        if (annoContabileFattura != that.annoContabileFattura) return false;
        if (meseContabileFattura != that.meseContabileFattura) return false;
        if (dataFattura != null ? !dataFattura.equals(that.dataFattura) : that.dataFattura != null)
            return false;
        if (dataContabileFattura != null ? !dataContabileFattura.equals(that.dataContabileFattura) : that.dataContabileFattura != null)
            return false;
        if (idIncasso != null ? !idIncasso.equals(that.idIncasso) : that.idIncasso != null)
            return false;
        if (idQuietanza != null ? !idQuietanza.equals(that.idQuietanza) : that.idQuietanza != null)
            return false;
        if (numeroReversale != null ? !numeroReversale.equals(that.numeroReversale) : that.numeroReversale != null)
            return false;
        if (numeroFattura != null ? !numeroFattura.equals(that.numeroFattura) : that.numeroFattura != null)
            return false;
        if (voce != null ? !voce.equals(that.voce) : that.voce != null)
            return false;
        if (importoOriginale != null ? !importoOriginale.equals(that.importoOriginale) : that.importoOriginale != null)
            return false;
        if (importoAggio != null ? !importoAggio.equals(that.importoAggio) : that.importoAggio != null)
            return false;
        if (percentualeAggio != null ? !percentualeAggio.equals(that.percentualeAggio) : that.percentualeAggio != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (idNdmVoceFattura ^ (idNdmVoceFattura >>> 32));
        result = 31 * result + (int) (idNdmFile ^ (idNdmFile >>> 32));
        result = 31 * result + annoFattura;
        result = 31 * result + meseFattura;
        result = 31 * result + (dataFattura != null ? dataFattura.hashCode() : 0);
        result = 31 * result + annoContabileFattura;
        result = 31 * result + meseContabileFattura;
        result = 31 * result + (dataContabileFattura != null ? dataContabileFattura.hashCode() : 0);
        result = 31 * result + (idIncasso != null ? idIncasso.hashCode() : 0);
        result = 31 * result + (idQuietanza != null ? idQuietanza.hashCode() : 0);
        result = 31 * result + (numeroReversale != null ? numeroReversale.hashCode() : 0);
        result = 31 * result + (numeroFattura != null ? numeroFattura.hashCode() : 0);
        result = 31 * result + (voce != null ? voce.hashCode() : 0);
        result = 31 * result + (importoOriginale != null ? importoOriginale.hashCode() : 0);
        result = 31 * result + (importoAggio != null ? importoAggio.hashCode() : 0);
        result = 31 * result + (percentualeAggio != null ? percentualeAggio.hashCode() : 0);
        return result;
    }
}
