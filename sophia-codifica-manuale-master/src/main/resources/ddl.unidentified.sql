

DROP TABLE IF EXISTS identified_song;
CREATE TABLE identified_song
(
   hash_id              CHAR(40)        NOT NULL,
   title                TEXT            NOT NULL,
   artists              TEXT            NOT NULL,
   roles                TEXT,
   siae_work_code       CHAR(11),
   identification_type  VARCHAR(20),
   insert_time          BIGINT          NOT NULL,
   sophia_update_time   BIGINT          DEFAULT 0 NOT NULL,
   PRIMARY KEY (hash_id)
);
ALTER TABLE identified_song PARTITION BY RANGE COLUMNS (hash_id)
( PARTITION p0 VALUES LESS THAN ('0fffffffffffffffffffffffffffffffffffffff')
, PARTITION p1 VALUES LESS THAN ('1fffffffffffffffffffffffffffffffffffffff')
, PARTITION p2 VALUES LESS THAN ('2fffffffffffffffffffffffffffffffffffffff')
, PARTITION p3 VALUES LESS THAN ('3fffffffffffffffffffffffffffffffffffffff')
, PARTITION p4 VALUES LESS THAN ('4fffffffffffffffffffffffffffffffffffffff')
, PARTITION p5 VALUES LESS THAN ('5fffffffffffffffffffffffffffffffffffffff')
, PARTITION p6 VALUES LESS THAN ('6fffffffffffffffffffffffffffffffffffffff')
, PARTITION p7 VALUES LESS THAN ('7fffffffffffffffffffffffffffffffffffffff')
, PARTITION p8 VALUES LESS THAN ('8fffffffffffffffffffffffffffffffffffffff')
, PARTITION p9 VALUES LESS THAN ('9fffffffffffffffffffffffffffffffffffffff')
, PARTITION pa VALUES LESS THAN ('afffffffffffffffffffffffffffffffffffffff')
, PARTITION pb VALUES LESS THAN ('bfffffffffffffffffffffffffffffffffffffff')
, PARTITION pc VALUES LESS THAN ('cfffffffffffffffffffffffffffffffffffffff')
, PARTITION pd VALUES LESS THAN ('dfffffffffffffffffffffffffffffffffffffff')
, PARTITION pe VALUES LESS THAN ('efffffffffffffffffffffffffffffffffffffff')
, PARTITION pf VALUES LESS THAN ('ffffffffffffffffffffffffffffffffffffffff')
);
CREATE INDEX identified_song_index_01 ON identified_song (hash_id);
CREATE INDEX identified_song_index_02 ON identified_song (sophia_update_time);



DROP TABLE IF EXISTS unidentified_song;
CREATE TABLE unidentified_song
(
   hash_id           CHAR(40)        NOT NULL,
   title             TEXT            NOT NULL,
   artists           TEXT            NOT NULL,
   roles             TEXT,
   priority          BIGINT          NOT NULL,
   insert_time       BIGINT          NOT NULL,
   last_auto_time    BIGINT          DEFAULT 0 NOT NULL,
   last_manual_time  BIGINT          DEFAULT 0 NOT NULL,
   PRIMARY KEY (hash_id)
);
ALTER TABLE unidentified_song PARTITION BY RANGE COLUMNS (hash_id)
( PARTITION p0 VALUES LESS THAN ('0fffffffffffffffffffffffffffffffffffffff')
, PARTITION p1 VALUES LESS THAN ('1fffffffffffffffffffffffffffffffffffffff')
, PARTITION p2 VALUES LESS THAN ('2fffffffffffffffffffffffffffffffffffffff')
, PARTITION p3 VALUES LESS THAN ('3fffffffffffffffffffffffffffffffffffffff')
, PARTITION p4 VALUES LESS THAN ('4fffffffffffffffffffffffffffffffffffffff')
, PARTITION p5 VALUES LESS THAN ('5fffffffffffffffffffffffffffffffffffffff')
, PARTITION p6 VALUES LESS THAN ('6fffffffffffffffffffffffffffffffffffffff')
, PARTITION p7 VALUES LESS THAN ('7fffffffffffffffffffffffffffffffffffffff')
, PARTITION p8 VALUES LESS THAN ('8fffffffffffffffffffffffffffffffffffffff')
, PARTITION p9 VALUES LESS THAN ('9fffffffffffffffffffffffffffffffffffffff')
, PARTITION pa VALUES LESS THAN ('afffffffffffffffffffffffffffffffffffffff')
, PARTITION pb VALUES LESS THAN ('bfffffffffffffffffffffffffffffffffffffff')
, PARTITION pc VALUES LESS THAN ('cfffffffffffffffffffffffffffffffffffffff')
, PARTITION pd VALUES LESS THAN ('dfffffffffffffffffffffffffffffffffffffff')
, PARTITION pe VALUES LESS THAN ('efffffffffffffffffffffffffffffffffffffff')
, PARTITION pf VALUES LESS THAN ('ffffffffffffffffffffffffffffffffffffffff')
);
CREATE INDEX unidentified_song_index_01 ON unidentified_song (priority);
CREATE INDEX unidentified_song_index_02 ON unidentified_song (last_auto_time);
CREATE INDEX unidentified_song_index_03 ON unidentified_song (last_manual_time);
CREATE INDEX unidentified_song_index_04 ON unidentified_song (insert_time);



DROP TABLE IF EXISTS unidentified_song_dsr;
CREATE TABLE unidentified_song_dsr
(
   id_util         VARCHAR(255)    NOT NULL,
   hash_id         CHAR(40)        NOT NULL,
   id_dsr          VARCHAR(100)    NOT NULL,
   album_title     TEXT,
   proprietary_id  VARCHAR(255),
   isrc            CHAR(12),
   iswc            CHAR(11),
   sales_count     BIGINT          DEFAULT 0 NOT NULL,
   dsp             VARCHAR(100),
   insert_time     BIGINT          NOT NULL,
   PRIMARY KEY (id_util)
);
ALTER TABLE unidentified_song_dsr PARTITION BY KEY() PARTITIONS 16;
CREATE INDEX unidentified_song_dsr_index_01 ON unidentified_song_dsr (hash_id);
CREATE INDEX unidentified_song_dsr_index_02 ON unidentified_song_dsr (insert_time);


