

DROP TABLE IF EXISTS sophia_ca_opera;
CREATE TABLE sophia_ca_opera
(
   codice_opera            VARCHAR(30)    NOT NULL,
   title                   VARCHAR(255),
   hash_title              VARCHAR(100)   NOT NULL,
   artists_names           TEXT,
   artists_ipi             VARCHAR(255),
   black_list_artists      TEXT,
   an_origin_vector        TEXT,
   ai_origin_vector        TEXT,
   an_idutil_src_matrix    TEXT,
   ai_idutil_src_matrix    TEXT,
   an_iddsr_source_matrix  TEXT,
   ai_iddsr_source_matrix  TEXT,
   kb_version              INT,
   valid                   TINYINT,
   PRIMARY KEY (codice_opera, hash_title)
);
CREATE INDEX sophia_ca_opera_index_01 on sophia_ca_opera (codice_opera);
CREATE INDEX sophia_ca_opera_index_02 on sophia_ca_opera (hash_title);


DROP TABLE IF EXISTS sophia_idsong_opera;
CREATE TABLE sophia_idsong_opera
(
   idsong_sophia  VARCHAR(255)   NOT NULL,
   codice_opera   VARCHAR(100)   NOT NULL,
   origin         VARCHAR(100),
   idutil_source  VARCHAR(100),
   iddsr_source   VARCHAR(255),
   iddsp_source   VARCHAR(255),
   kb_version     INT,
   valid          TINYINT,
   PRIMARY KEY (idsong_sophia, codice_opera)
);
CREATE INDEX sophia_idsong_opera_index_01 on sophia_idsong_opera (idsong_sophia);
CREATE INDEX sophia_idsong_opera_index_02 on sophia_idsong_opera (codice_opera);


DROP TABLE IF EXISTS sophia_isrc_opera;
CREATE TABLE sophia_isrc_opera
(
   isrc           VARCHAR(20)    NOT NULL,
   codice_opera   VARCHAR(30)    NOT NULL,
   origin         VARCHAR(30),
   idutil_source  VARCHAR(100),
   iddsr_source   VARCHAR(100),
   kb_version     INT,
   valid          TINYINT,
   PRIMARY KEY (isrc, codice_opera)
);
CREATE INDEX sophia_isrc_opera_index_01 on sophia_isrc_opera (isrc);
CREATE INDEX sophia_isrc_opera_index_02 on sophia_isrc_opera (codice_opera);


DROP TABLE IF EXISTS sophia_iswc_opera;
CREATE TABLE sophia_iswc_opera
(
   iswc           VARCHAR(20)    NOT NULL,
   codice_opera   VARCHAR(30)    NOT NULL,
   origin         VARCHAR(30),
   idutil_source  VARCHAR(100),
   iddsr_source   VARCHAR(100),
   kb_version     INT,
   valid          TINYINT,
   PRIMARY KEY (iswc, codice_opera)
);
CREATE INDEX sophia_iswc_opera_index_01 on sophia_iswc_opera (iswc);
CREATE INDEX sophia_iswc_opera_index_02 on sophia_iswc_opera (codice_opera);

