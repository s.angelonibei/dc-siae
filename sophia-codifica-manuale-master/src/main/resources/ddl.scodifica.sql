CREATE TABLE FONTE_PRIMA_UTILIZZAZIONE
(
  ID_FONTE_PRIMA_UTILIZZAZIONE int(11) NOT NULL AUTO_INCREMENT,
  DESCRIZIONE                  varchar(100) NOT NULL,
  PRIMARY KEY (ID_FONTE_PRIMA_UTILIZZAZIONE)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

ALTER TABLE PERF_COMBANA
  ADD CONSTRAINT PERF_COMBANA_FONTE_PRIMA_UTILIZZAZIONE_FK FOREIGN KEY (ID_FONTE_PRIMA_UTILIZZAZIONE) REFERENCES FONTE_PRIMA_UTILIZZAZIONE (ID_FONTE_PRIMA_UTILIZZAZIONE),
  ADD INDEX PERF_COMBANA_FONTE_PRIMA_UTILIZZAZIONE_FK (ID_FONTE_PRIMA_UTILIZZAZIONE),
  ADD COLUMN ID_FONTE_PRIMA_UTILIZZAZIONE int (11) NULL,
  ADD COLUMN DATA_CREAZIONE date NULL AFTER LAVORABILE,
  AUTO_INCREMENT=143508;

-- ALTER TABLE PERF_MOVIMENTO_CONTABILE ADD CONSTRAINT PERF_MOVIMENTO_CONTABILE_PERIODO_RIPARTIZIONE_FK FOREIGN KEY (ID_PERIODO_RIPARTIZIONE) REFERENCES PERIODO_RIPARTIZIONE(ID_PERIODO_RIPARTIZIONE) ;
-- ALTER TABLE PERF_EVENTI_PAGATI ADD CONSTRAINT PERF_EVENTI_PAGATI_PERIODO_RIPARTIZIONE_FK FOREIGN KEY (ID_PERIODO_RIPARTIZIONE) REFERENCES PERIODO_RIPARTIZIONE(ID_PERIODO_RIPARTIZIONE) ;

ALTER TABLE PERF_UTILIZZAZIONI_ASSEGNATE MODIFY COLUMN TIPO_AZIONE enum('P','A','M','S','SC') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL COMMENT 'S = SALTATA, SC = SCODIFICATA, P = PRIMA, A = ALTRA';
-- ALTER TABLE PERF_SESSIONE_CODIFICA MODIFY COLUMN DATA_FINE_VALID datetime NULL;
-- ALTER TABLE PERF_SESSIONE_CODIFICA MODIFY COLUMN DATA_INIZIO_VALID datetime DEFAULT CURRENT_TIMESTAMP NOT NULL;
