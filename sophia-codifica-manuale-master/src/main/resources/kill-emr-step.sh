#!/bin/sh

# Usage: kill-emr-step.sh $1 $2
#  $1: EMR master node DNS name
#  $2: YARN application ID

HOME=/var/local/sophia/dev/webapp
#HOME=/var/local/sophia/debug/webapp
#HOME=/home/orchestrator/sophia/webapp

echo "$(date '+%Y-%m-%d %H:%M:%S')" >> $HOME/kill-emr-step.log
echo "/usr/bin/ssh -o StrictHostKeyChecking=no -i $HOME/sophiaIrlanda.pem hadoop@$1 \"yarn application -kill $2\"" >> $HOME/kill-emr-step.log

/usr/bin/ssh -o StrictHostKeyChecking=no -i $HOME/sophiaIrlanda.pem hadoop@$1 "yarn application -kill $2"
