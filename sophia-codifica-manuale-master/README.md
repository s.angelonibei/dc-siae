Cruscotto Amministrazione Sistema Sophia

Funzionalità:
- riconoscimento manuale delle opere non riconosciute
- ricerca/inserimento knowledge base
- configurazione processi
- configurazione pricing
- configurazione claiming
- monitoring service bus SQS
- restful web service(s)