package it.siae.valorizzatore.model.domain;

import it.siae.valorizzatore.model.domain.ChiaveElaborazione;
import it.siae.valorizzatore.model.domain.ProgrammaRientrato;
import it.siae.valorizzatore.utility.Constants;
import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by idilello on 6/8/16.
 */
public class RigaElaborata implements Serializable{

    private Long idEvento;
    private Long numeroFattura;
    private String voceIncasso;
    private Integer contabilitaPagamentoEvento;

    private Double importoTotEventoFatturaVoce;
    //private Double importoTotEventoVoce;
    private Long numeroPmAttesiEventoVoce;
    private Long numeroPmRientratiEventoVoce;

    // Ricalcolo Tipo 1
    private Long numeroCedolePmAttesi;
    private Double valoreProQuota;

    // Ricalcolo Tipo 2
    private Double durataPmAttesi;
    private Double valoreAlSecondo;

    // Voce 2244
    private Long numeroPmAttesiEventoVoceSpalla;
    private Double valoreProQuotaSpalla;
    private Long numeroCedolePmAttesiSpalla;
    private Double importoTotPrincipale;
    private Double importoTotSpalla;

    private Map<Long, ProgrammaRientrato> listaPmRientratiEventoFatturaVoce=new HashMap<Long, ProgrammaRientrato>();

    private Map<Long, ProgrammaRientrato> listaPmRientratiEventoVoce=new HashMap<Long, ProgrammaRientrato>();

    private ChiaveElaborazione chiaveRigaElaborata;
    private String rigaSospesa;

    protected Logger log = Logger.getLogger(this.getClass());

    //public void addPmRientratoEventoFatturaVoce(ProgrammaRientrato programmaRientrato){
    //    if (listaPmRientratiEventoFatturaVoce.get(programmaRientrato.getId())!=null)
    //      listaPmRientratiEventoFatturaVoce.put(programmaRientrato.getId(), programmaRientrato);
    //}

    public ProgrammaRientrato getPmRientratoEventoVoce(Long idProgrammaMusicale){


        ProgrammaRientrato programmaRientrato = listaPmRientratiEventoVoce.get(idProgrammaMusicale);
                                                //listaPmRientratiEventoFatturaVoce
        log.debug("Programma rientrato di getPmRientratoEventoVoce: " + programmaRientrato.toString());
        if (voceIncasso!=null && (voceIncasso.equalsIgnoreCase(Constants.VOCE_2243) || voceIncasso.equalsIgnoreCase(Constants.VOCE_2244))){

            if (programmaRientrato.getFlagPMPrincipale()!=null && programmaRientrato.getFlagPMPrincipale()=='Y')
               programmaRientrato.setImportoSingolaCedola(valoreProQuota);
            else
               programmaRientrato.setImportoSingolaCedola(valoreProQuotaSpalla);

        }else {

            if (valoreProQuota != null)
                programmaRientrato.setImportoSingolaCedola(valoreProQuota);
            else if (valoreProQuotaSpalla != null)
                programmaRientrato.setImportoSingolaCedola(valoreProQuotaSpalla);
            else if (valoreAlSecondo != null)
                programmaRientrato.setImportoSingolaCedola(valoreAlSecondo);
        }

          return programmaRientrato;
    }

    //public ProgrammaRientrato getPmRientratoEventoVoce(Long id){
    //    return listaPmRientratiEventoVoce.get(id);
    //}

    public void addPmRientratoEventoVoce(ProgrammaRientrato programmaRientrato){
        if (listaPmRientratiEventoVoce.get(programmaRientrato.getId())!=null)
            listaPmRientratiEventoVoce.put(programmaRientrato.getId(), programmaRientrato);
    }

    public Long getIdEvento() {
        return idEvento;
    }

    public void setIdEvento(Long idEvento) {
        this.idEvento = idEvento;
    }

    public Long getNumeroFattura() {
        return numeroFattura;
    }

    public void setNumeroFattura(Long numeroFattura) {
        this.numeroFattura = numeroFattura;
    }

    public String getVoceIncasso() {
        return voceIncasso;
    }

    public void setVoceIncasso(String voceIncasso) {
        this.voceIncasso = voceIncasso;
    }

    public Double getImportoTotEventoFatturaVoce() {
        return importoTotEventoFatturaVoce;
    }

    public void setImportoTotEventoFatturaVoce(Double importoTotEventoFatturaVoce) {
        this.importoTotEventoFatturaVoce = importoTotEventoFatturaVoce;
    }

    //public Double getImportoTotEventoVoce() {
    //    return importoTotEventoVoce;
    //}

    //public void setImportoTotEventoVoce(Double importoTotEventoVoce) {
    //    this.importoTotEventoVoce = importoTotEventoVoce;
    //}

    public Long getNumeroPmAttesiEventoVoce() {
        if (numeroPmAttesiEventoVoce==null)
            numeroPmAttesiEventoVoce=0L;

        return numeroPmAttesiEventoVoce;
    }

    public void setNumeroPmAttesiEventoVoce(Long numeroPmAttesiEventoVoce) {
        this.numeroPmAttesiEventoVoce = numeroPmAttesiEventoVoce;
    }

    public Long getNumeroPmRientratiEventoVoce() {
        if (numeroPmRientratiEventoVoce==null)
            numeroPmRientratiEventoVoce=0L;

        return numeroPmRientratiEventoVoce;
    }

    public void setNumeroPmRientratiEventoVoce(Long numeroPmRientratiEventoVoce) {
        this.numeroPmRientratiEventoVoce = numeroPmRientratiEventoVoce;
    }

    public Long getNumeroCedolePmAttesi() {

        if (numeroCedolePmAttesi==null)
            numeroCedolePmAttesi=0L;

        return numeroCedolePmAttesi;
    }

    public void setNumeroCedolePmAttesi(Long numeroCedolePmAttesi) {
        this.numeroCedolePmAttesi = numeroCedolePmAttesi;
    }

    public Double getValoreProQuota() {
       if (valoreProQuota==null)
           valoreProQuota=0D;
        return valoreProQuota;
    }

    public void setValoreProQuota(Double valoreProQuota) {
        this.valoreProQuota = valoreProQuota;
    }

    public Double getDurataPmAttesi() {
        if (durataPmAttesi==null)
            durataPmAttesi=0D;

        return durataPmAttesi;
    }

    public void setDurataPmAttesi(Double durataPmAttesi) {
        this.durataPmAttesi = durataPmAttesi;
    }

    public Double getValoreAlSecondo() {
        if (valoreAlSecondo==null)
            valoreAlSecondo=0D;

        return valoreAlSecondo;
    }

    public void setValoreAlSecondo(Double valoreAlSecondo) {
        this.valoreAlSecondo = valoreAlSecondo;
    }

    public Long getNumeroPmAttesiEventoVoceSpalla() {
        if (numeroPmAttesiEventoVoceSpalla==null)
            numeroPmAttesiEventoVoceSpalla=0L;

        return numeroPmAttesiEventoVoceSpalla;
    }

    public void setNumeroPmAttesiEventoVoceSpalla(Long numeroPmAttesiEventoVoceSpalla) {
        this.numeroPmAttesiEventoVoceSpalla = numeroPmAttesiEventoVoceSpalla;
    }

    public Double getValoreProQuotaSpalla() {
        if (valoreProQuotaSpalla==null)
            valoreProQuotaSpalla=0D;

        return valoreProQuotaSpalla;
    }

    public void setValoreProQuotaSpalla(Double valoreProQuotaSpalla) {
        this.valoreProQuotaSpalla = valoreProQuotaSpalla;
    }

    public Long getNumeroCedolePmAttesiSpalla() {
         if (numeroCedolePmAttesiSpalla==null)
             numeroCedolePmAttesiSpalla=0L;

        return numeroCedolePmAttesiSpalla;
    }

    public void setNumeroCedolePmAttesiSpalla(Long numeroCedolePmAttesiSpalla) {
        this.numeroCedolePmAttesiSpalla = numeroCedolePmAttesiSpalla;
    }

    public Double getImportoTotPrincipale() {
        if (importoTotPrincipale==null)
            importoTotPrincipale=0D;

        return importoTotPrincipale;
    }

    public void setImportoTotPrincipale(Double importoTotPrincipale) {
        this.importoTotPrincipale = importoTotPrincipale;
    }

    public Double getImportoTotSpalla() {
        if (importoTotSpalla==null)
            importoTotSpalla=0D;

        return importoTotSpalla;
    }

    public void setImportoTotSpalla(Double importoTotSpalla) {
        this.importoTotSpalla = importoTotSpalla;
    }

    public Map<Long, ProgrammaRientrato> getListaPmRientratiEventoFatturaVoce() {
        return listaPmRientratiEventoFatturaVoce;
    }

    public void setListaPmRientratiEventoFatturaVoce(Map<Long, ProgrammaRientrato> listaPmRientratiEventoFatturaVoce) {
        this.listaPmRientratiEventoFatturaVoce = listaPmRientratiEventoFatturaVoce;
    }

    public Map<Long, ProgrammaRientrato> getListaPmRientratiEventoVoce() {
        return listaPmRientratiEventoVoce;
    }

    public void setListaPmRientratiEventoVoce(Map<Long, ProgrammaRientrato> listaPmRientratiEventoVoce) {
        this.listaPmRientratiEventoVoce = listaPmRientratiEventoVoce;
    }

    public ChiaveElaborazione getChiaveRigaElaborata() {
        return chiaveRigaElaborata;
    }

    public void setChiaveRigaElaborata(ChiaveElaborazione chiaveRigaElaborata) {
        this.chiaveRigaElaborata = chiaveRigaElaborata;
    }

    public String getRigaSospesa() {
        return rigaSospesa;
    }

    public void setRigaSospesa(String rigaSospesa) {
        this.rigaSospesa = rigaSospesa;
    }

    public Integer getContabilitaPagamentoEvento() {
        return contabilitaPagamentoEvento;
    }

    public void setContabilitaPagamentoEvento(Integer contabilitaPagamentoEvento) {
        this.contabilitaPagamentoEvento = contabilitaPagamentoEvento;
    }
}
