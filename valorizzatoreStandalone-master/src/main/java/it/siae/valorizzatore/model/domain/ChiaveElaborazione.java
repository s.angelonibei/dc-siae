package it.siae.valorizzatore.model.domain;

/**
 * Created by idilello on 6/8/16.
 */
public class ChiaveElaborazione {

    private Long idEvento;
    private Long numeroFattura;
    private String voceIncasso;

    public ChiaveElaborazione(Long idEvento, Long numeroFattura, String voceIncasso){
        this.idEvento =  idEvento;
        this.numeroFattura = numeroFattura;
        this.voceIncasso =  voceIncasso;
    }

    public Long getIdEvento() {
        return idEvento;
    }

    public void setIdEvento(Long idEvento) {
        this.idEvento = idEvento;
    }

    public Long getNumeroFattura() {
        return numeroFattura;
    }

    public void setNumeroFattura(Long numeroFattura) {
        this.numeroFattura = numeroFattura;
    }

    public String getVoceIncasso() {
        return voceIncasso;
    }

    public void setVoceIncasso(String voceIncasso) {
        this.voceIncasso = voceIncasso;
    }

    public String toString(){
        return idEvento+"_"+numeroFattura+"_"+voceIncasso;
    }
}
