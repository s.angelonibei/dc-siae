package it.siae.valorizzatore.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by idilello on 6/8/16.
 */
@Entity
@Table(name = "ESECUZIONE_RICALCOLO")
public class EsecuzioneRicalcolo implements Serializable {

    private Long id;
    private Date dataOraInizioEsecuzione;
    private Date dataOraFineEsecuzione;
    private String esitoEsecuzione;
    private String descrizioneEsito;
    private Date dataOraTrasmissioneSIADA;
    private Date dataOraUltimaModifica;
    private String utenteUltimaModifica;
    private Long contabilitaInizio;
    private Long contabilitaFine;
    private Date dataOraCongelamento;
    private Long contabilitaSiada;
    private String voceIncasso;


    @Id
    @SequenceGenerator(name="ESEC_RICALCOLO",sequenceName = "ESEC_RICALCOLO_SEQ",allocationSize=1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ESEC_RICALCOLO")
    @Column(name = "ID_ESECUZIONE_RICALCOLO", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_ORA_INIZIO_ESECUZIONE")
    public Date getDataOraInizioEsecuzione() {
        return dataOraInizioEsecuzione;
    }

    public void setDataOraInizioEsecuzione(Date dataOraInizioEsecuzione) {
        this.dataOraInizioEsecuzione = dataOraInizioEsecuzione;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_ORA_FINE_ESECUZIONE")
    public Date getDataOraFineEsecuzione() {
        return dataOraFineEsecuzione;
    }

    public void setDataOraFineEsecuzione(Date dataOraFineEsecuzione) {
        this.dataOraFineEsecuzione = dataOraFineEsecuzione;
    }

    @Column(name = "ESITO_ESECUZIONE")
    public String getEsitoEsecuzione() {
        return esitoEsecuzione;
    }

    public void setEsitoEsecuzione(String esitoEsecuzione) {
        this.esitoEsecuzione = esitoEsecuzione;
    }

    @Column(name = "DESCRIZIONE_ESITO")
    public String getDescrizioneEsito() {
        return descrizioneEsito;
    }

    public void setDescrizioneEsito(String descrizioneEsito) {
        this.descrizioneEsito = descrizioneEsito;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_ORA_TRASMISSIONE_SIADA")
    public Date getDataOraTrasmissioneSIADA() {
        return dataOraTrasmissioneSIADA;
    }

    public void setDataOraTrasmissioneSIADA(Date dataOraTrasmissioneSIADA) {
        this.dataOraTrasmissioneSIADA = dataOraTrasmissioneSIADA;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_ORA_ULTIMA_MODIFICA")
    public Date getDataOraUltimaModifica() {
        return dataOraUltimaModifica;
    }

    public void setDataOraUltimaModifica(Date dataOraUltimaModifica) {
        this.dataOraUltimaModifica = dataOraUltimaModifica;
    }

    @Column(name = "UTENTE_ULTIMA_MODIFICA")
    public String getUtenteUltimaModifica() {
        return utenteUltimaModifica;
    }

    public void setUtenteUltimaModifica(String utenteUltimaModifica) {
        this.utenteUltimaModifica = utenteUltimaModifica;
    }


    @Column(name = "CONTABILITA_INIZIO")
    public Long getContabilitaInizio() {
        return contabilitaInizio;
    }

    public void setContabilitaInizio(Long contabilitaInizio) {
        this.contabilitaInizio = contabilitaInizio;
    }

    @Column(name = "CONTABILITA_FINE")
    public Long getContabilitaFine() {
        return contabilitaFine;
    }

    public void setContabilitaFine(Long contabilitaFine) {
        this.contabilitaFine = contabilitaFine;
    }


    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_CONGELAMENTO")
    public Date getDataOraCongelamento() {
        return dataOraCongelamento;
    }

    public void setDataOraCongelamento(Date dataOraCongelamento) {
        this.dataOraCongelamento = dataOraCongelamento;
    }


    @Column(name = "CONTABILITA_SIADA")
    public Long getContabilitaSiada() {
        return contabilitaSiada;
    }

    public void setContabilitaSiada(Long contabilitaSiada) {
        this.contabilitaSiada = contabilitaSiada;
    }


    @Column(name = "VOCE_INCASSO")
    public String getVoceIncasso() {
        return voceIncasso;
    }

    public void setVoceIncasso(String voceIncasso) {
        this.voceIncasso = voceIncasso;
    }
}