package it.siae.valorizzatore.model;

/**
 * Created by idilello on 6/8/16.
 */

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "CAMPIONAMENTO_CONFIG")
public class CampionamentoConfig implements Serializable {

    private Long id;
    private Long contabilita;
    private Date dataInizioLavorazione;
    private Date dataEstrazioniLotto;
    private String estrazioniRoma;
    private String estrazioniMilano;
    private Long restoRoma1;
    private Long restoRoma2;
    private Long restoMilano1;
    private Long restoMilano2;

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="CAMPIONAMENTO_CONFIG_SEQ")
    @Column(name = "ID_CAMPIONAMENTO_CONFIG", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    @Column(name = "CONTABILITA")
    public Long getContabilita() {
        return contabilita;
    }

    public void setContabilita(Long contabilita) {
        this.contabilita = contabilita;
    }


    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "INIZIO_LAVORAZIONE")
    public Date getDataInizioLavorazione() {
        return dataInizioLavorazione;
    }

    public void setDataInizioLavorazione(Date dataInizioLavorazione) {
        this.dataInizioLavorazione = dataInizioLavorazione;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_ESTRAZIONE_LOTTO")
    public Date getDataEstrazioniLotto() {
        return dataEstrazioniLotto;
    }

    public void setDataEstrazioniLotto(Date dataEstrazioniLotto) {
        this.dataEstrazioniLotto = dataEstrazioniLotto;
    }


    @Column(name = "ESTRAZIONI_ROMA")
    public String getEstrazioniRoma() {
        return estrazioniRoma;
    }

    public void setEstrazioniRoma(String estrazioniRoma) {
        this.estrazioniRoma = estrazioniRoma;
    }

    @Column(name = "ESTRAZIONI_MILANO")
    public String getEstrazioniMilano() {
        return estrazioniMilano;
    }

    public void setEstrazioniMilano(String estrazioniMilano) {
        this.estrazioniMilano = estrazioniMilano;
    }



    @Column(name = "RESTO1_ROMA")
    public Long getRestoRoma1() {
        return restoRoma1;
    }

    public void setRestoRoma1(Long restoRoma1) {
        this.restoRoma1 = restoRoma1;
    }

    @Column(name = "RESTO2_ROMA")
    public Long getRestoRoma2() {
        return restoRoma2;
    }

    public void setRestoRoma2(Long restoRoma2) {
        this.restoRoma2 = restoRoma2;
    }

    @Column(name = "RESTO1_MILANO")
    public Long getRestoMilano1() {
        return restoMilano1;
    }

    public void setRestoMilano1(Long restoMilano1) {
        this.restoMilano1 = restoMilano1;
    }

    @Column(name = "RESTO2_MILANO")
    public Long getRestoMilano2() {
        return restoMilano2;
    }

    public void setRestoMilano2(Long restoMilano2) {
        this.restoMilano2 = restoMilano2;
    }
}
