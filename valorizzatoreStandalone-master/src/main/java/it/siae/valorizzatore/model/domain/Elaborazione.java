package it.siae.valorizzatore.model.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by idilello on 6/8/16.
 */
public class Elaborazione {

    private Map<String, RigaElaborata> datiElaborati = new HashMap<String, RigaElaborata>();

    private Map<String, List<RigaElaborata>> eventoVoceElaborati = new HashMap<String, List<RigaElaborata>>();

    public void addRigaElaborata(RigaElaborata rigaElaborata){

        ChiaveElaborazione chiaveRigaElaborata = rigaElaborata.getChiaveRigaElaborata();

        datiElaborati.put(chiaveRigaElaborata.toString(), rigaElaborata);

        String chiaveEventoVoce = chiaveRigaElaborata.getIdEvento()+"_"+chiaveRigaElaborata.getVoceIncasso();

        if (eventoVoceElaborati.get(chiaveEventoVoce)==null){
            ArrayList<RigaElaborata> eventoVoceElaboratiList = new ArrayList<RigaElaborata>();
            eventoVoceElaboratiList.add(rigaElaborata);
            eventoVoceElaborati.put(chiaveEventoVoce, eventoVoceElaboratiList);
        }else{
            eventoVoceElaborati.get(chiaveEventoVoce).add(rigaElaborata);
        }

    }

    public List<RigaElaborata> getListaRigaElaborataEventoVoce(Long idEvento, String voceIncasso){
        String chiave = idEvento+"_"+voceIncasso;
        return eventoVoceElaborati.get(chiave);
    }

    public RigaElaborata getRigaElaborata(ChiaveElaborazione chiaveElaborazione){
        return datiElaborati.get(chiaveElaborazione.toString());
    }

    public int size(){
        return datiElaborati.size();
    }

}
