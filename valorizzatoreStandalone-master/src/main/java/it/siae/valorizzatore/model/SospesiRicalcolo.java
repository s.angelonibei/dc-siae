package it.siae.valorizzatore.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by idilello on 6/8/16.
 */
@Entity
@Table(name = "SOSPESI_RICALCOLO")
public class SospesiRicalcolo implements Serializable {

    private Long id;
    private Long idEvento;
    private Long numeroFattura;
    private String voceIncasso;
    private Long idProgrammaMusicale;
    private String motivazioneSospensione;
    private Long idEsecuzioneRicalcolo;
    private Long idMovimentoContabile;
    private Double importoSingolaCedola;

    @Id
    @SequenceGenerator(name="SOSPESI_RICALCOLO",sequenceName = "SOSPESI_RICALCOLO_SEQ",allocationSize=1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SOSPESI_RICALCOLO")
    @Column(name = "ID_SOSPESI_RICALCOLO", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    @Column(name = "ID_EVENTO")
    public Long getIdEvento() {
        return idEvento;
    }

    public void setIdEvento(Long idEvento) {
        this.idEvento = idEvento;
    }

    @Column(name = "NUMERO_FATTURA")
    public Long getNumeroFattura() {
        return numeroFattura;
    }

    public void setNumeroFattura(Long numeroFattura) {
        this.numeroFattura = numeroFattura;
    }


    @Column(name = "VOCE_INCASSO")
    public String getVoceIncasso() {
        return voceIncasso;
    }

    public void setVoceIncasso(String voceIncasso) {
        this.voceIncasso = voceIncasso;
    }


    @Column(name = "ID_PROGRAMMA_MUSICALE")
    public Long getIdProgrammaMusicale() {
        return idProgrammaMusicale;
    }

    public void setIdProgrammaMusicale(Long idProgrammaMusicale) {
        this.idProgrammaMusicale = idProgrammaMusicale;
    }

    @Column(name = "MOTIVAZIONE_SOSPENSIONE")
    public String getMotivazioneSospensione() {
        return motivazioneSospensione;
    }

    public void setMotivazioneSospensione(String motivazioneSospensione) {
        this.motivazioneSospensione = motivazioneSospensione;
    }

    @Column(name = "ID_ESECUZIONE_RICALCOLO")
    public Long getIdEsecuzioneRicalcolo() {
        return idEsecuzioneRicalcolo;
    }

    public void setIdEsecuzioneRicalcolo(Long idEsecuzioneRicalcolo) {
        this.idEsecuzioneRicalcolo = idEsecuzioneRicalcolo;
    }

    @Column(name = "ID_MOVIMENTO_CONTABILE")
    public Long getIdMovimentoContabile() {
        return idMovimentoContabile;
    }

    public void setIdMovimentoContabile(Long idMovimentoContabile) {
        this.idMovimentoContabile = idMovimentoContabile;
    }

    @Column(name = "IMPORTO_SINGOLA_CEDOLA")
    public Double getImportoSingolaCedola() {
        return importoSingolaCedola;
    }

    public void setImportoSingolaCedola(Double importoSingolaCedola) {
        this.importoSingolaCedola = importoSingolaCedola;
    }
}
