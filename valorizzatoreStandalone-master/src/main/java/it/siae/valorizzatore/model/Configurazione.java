package it.siae.valorizzatore.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by idilello on 6/8/16.
 */
@Entity
@Table(name = "CONFIGURAZIONE")
public class Configurazione implements Serializable {

    private Long id;
    private String parameterName;
    private String parameterValue;
    private Character flagAttivo;
    private Date dataOraUltimaModifica;
    private String utenteUltimaModifica;


    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="CONFIGURAZIONE_SEQ")
    @Column(name = "ID_CONFIGURAZIONE", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    @Column(name = "PARAMETER_NAME")
    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    @Column(name = "PARAMETER_VALUE")
    public String getParameterValue() {
        return parameterValue;
    }

    public void setParameterValue(String parameterValue) {
        this.parameterValue = parameterValue;
    }

    @Column(name = "FLAG_ATTIVO")
    public Character getFlagAttivo() {
        return flagAttivo;
    }

    public void setFlagAttivo(Character flagAttivo) {
        this.flagAttivo = flagAttivo;
    }


    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_ORA_ULTIMA_MODIFICA")
    public Date getDataOraUltimaModifica() {
        return dataOraUltimaModifica;
    }

    public void setDataOraUltimaModifica(Date dataOraUltimaModifica) {
        this.dataOraUltimaModifica = dataOraUltimaModifica;
    }


    @Column(name = "UTENTE_ULTIMA_MODIFICA")
    public String getUtenteUltimaModifica() {
        return utenteUltimaModifica;
    }

    public void setUtenteUltimaModifica(String utenteUltimaModifica) {
        this.utenteUltimaModifica = utenteUltimaModifica;
    }
}
