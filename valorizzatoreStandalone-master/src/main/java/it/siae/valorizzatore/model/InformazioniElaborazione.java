package it.siae.valorizzatore.model;

import it.siae.valorizzatore.utility.Constants;

import java.io.Serializable;

/**
 * Created by idilello on 6/14/16.
 */
public class InformazioniElaborazione implements Serializable{

    private Long totalePM=0L;
    private Long totaleElaborati=0L;
    private Long totaleSospesi=0L;
    private Long totaleErrori=0L;

    private boolean suspende=false;
    private boolean terminate=true;

    private String error = Constants.SUCCESS_ERROR_CODE;
    private String description = Constants.SUCCESS_ERROR_DESCR;

    public Long getTotalePM() {
        return totalePM;
    }

    public void setTotalePM(Long totalePM) {
        this.totalePM = totalePM;
    }

    public Long getTotaleElaborati() {
        return totaleElaborati;
    }

    public void setTotaleElaborati(Long totaleElaborati) {
        this.totaleElaborati = totaleElaborati;
    }

    public Long getTotaleSospesi() {
        return totaleSospesi;
    }

    public void setTotaleSospesi(Long totaleSospesi) {
        this.totaleSospesi = totaleSospesi;
    }

    public Long getTotaleErrori() {
        return totaleErrori;
    }

    public void setTotaleErrori(Long totaleErrori) {
        this.totaleErrori = totaleErrori;
    }

    public boolean isSuspende() {
        return suspende;
    }

    public void setSuspende(boolean suspende) {
        this.suspende = suspende;
    }

    public boolean isTerminate() {
        return terminate;
    }

    public void setTerminate(boolean terminate) {
        this.terminate = terminate;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void reset(){
        totalePM=0L;
        totaleElaborati=0L;
        totaleSospesi=0L;
        totaleErrori=0L;

        suspende=false;
        terminate=true;

        error = Constants.SUCCESS_ERROR_CODE;
        description = Constants.SUCCESS_ERROR_DESCR;
    }

    public String toString(){

        //  1300-600-100-0-0 (Totali-Elaborati-Sospesi-Errori-Terminato[1]/Attivo [0])
        if (terminate)
            return totalePM+"-"+totaleElaborati+"-"+totaleSospesi+"-"+totaleErrori+"-1";
        else
            return totalePM+"-"+totaleElaborati+"-"+totaleSospesi+"-"+totaleErrori+"-0";

    }

}