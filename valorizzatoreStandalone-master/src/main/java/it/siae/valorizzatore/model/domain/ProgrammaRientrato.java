package it.siae.valorizzatore.model.domain;

import it.siae.valorizzatore.utility.Utility;

import java.io.Serializable;

/**
 * Created by idilello on 6/8/16.
 */
public class ProgrammaRientrato implements Serializable {

    private Long id;
    //private Double importoDemManuale;
    //private Double importoDem;
    private Double importoRicalcolato;
    private Double importoSingolaCedola;
    private Character flagPMPrincipale;
    private Long cedole;
    private Double durata;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    //public Double getImportoDemManuale() {
    //    return importoDemManuale;
    //}

    //public void setImportoDemManuale(Double importoDemManuale) {
    //    this.importoDemManuale = importoDemManuale;
    //}

    //public Double getImportoDem() {
    //    return importoDem;
    //}

    //public void setImportoDem(Double importoDem) {
    //    this.importoDem = importoDem;
    //}

    public Double getImportoRicalcolato() {
        return importoRicalcolato;
    }

    public void setImportoRicalcolato(Double importoRicalcolato) {

        this.importoRicalcolato = Utility.round(importoRicalcolato,7);
    }

    public Double getImportoSingolaCedola() {
        return importoSingolaCedola;
    }

    public void setImportoSingolaCedola(Double importoSingolaCedola) {
        this.importoSingolaCedola = importoSingolaCedola;
    }

    public Character getFlagPMPrincipale() {
        return flagPMPrincipale;
    }

    public void setFlagPMPrincipale(Character flagPMPrincipale) {
        this.flagPMPrincipale = flagPMPrincipale;
    }

    public Long getCedole() {
        if (cedole==null)
            cedole=0L;

        return cedole;
    }

    public void setCedole(Long cedole) {
        this.cedole = cedole;
    }

    public Double getDurata() {
        if (durata==null)
            durata=0D;


        return durata;
    }

    public void setDurata(Double durata) {
        this.durata = durata;
    }

    @Override
    public String toString() {
        return "ProgrammaRientrato{" +
                "id=" + id +
                ", importoRicalcolato=" + importoRicalcolato +
                ", importoSingolaCedola=" + importoSingolaCedola +
                ", flagPMPrincipale=" + flagPMPrincipale +
                ", cedole=" + cedole +
                ", durata=" + durata +
                '}';
    }
}
