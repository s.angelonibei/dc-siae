package it.siae.valorizzatore.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;


/**
 * Created by idilello on 6/6/16.
 */
@Entity
@Table(name = "TRATTAMENTO_PM")
public class TrattamentoPM implements Serializable{

    private Long id;
    private String voceIncasso;
    private String tipo;
    private String descrizione;
    private String mod107;
    private String valorizzazionePm;
    private String acquisizioneCedole;

    @Id
    //@GeneratedValue(strategy= GenerationType.SEQUENCE, generator="ADM_USR_ADMIN_SEQ_GENERATOR")
    @Column(name = "ID_TRATTAMENTO_PM", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "VOCE_INCASSO",nullable = false)
    public String getVoceIncasso() {
        return voceIncasso;
    }

    public void setVoceIncasso(String voceIncasso) {
        this.voceIncasso = voceIncasso;
    }

    @Column(name = "TIPO",nullable = true)
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Column(name = "DESCRIZIONE",nullable = false)
    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    @Column(name = "MOD107",nullable = false)
    public String getMod107() {
        return mod107;
    }

    public void setMod107(String mod107) {
        this.mod107 = mod107;
    }

    @Column(name = "VALORIZZAZIONE_PM",nullable = false)
    public String getValorizzazionePm() {
        return valorizzazionePm;
    }

    public void setValorizzazionePm(String valorizzazionePm) {
        this.valorizzazionePm = valorizzazionePm;
    }

    @Column(name = "ACQUISIZIONE_CEDOLE",nullable = false)
    public String getAcquisizioneCedole() {
        return acquisizioneCedole;
    }

    public void setAcquisizioneCedole(String acquisizioneCedole) {
        this.acquisizioneCedole = acquisizioneCedole;
    }
}
