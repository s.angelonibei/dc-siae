package it.siae.valorizzatore.model.dwh;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by idilello on 6/8/16.
 */
@Entity
@Table(name = "EDW_ADM.EDW_D_RIPARTIZIONE")
//@Table(name = "EDW_D_RIPARTIZIONE")
public class EDWRipartizione implements Serializable {

    private Long id;
    private Long ripartizione;
    private String tipoPeriodo;
    private Long dataInizio;
    private Long dataFine;
    private String flagRolling;
    private Long distanzaDaAnnoCorrente;
    private Long semestre;


    @Id
    @Column(name = "SGK_RIPARTIZIONE", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "NTK_RIPARTIZIONE")
    public Long getRipartizione() {
        return ripartizione;
    }

    public void setRipartizione(Long ripartizione) {
        this.ripartizione = ripartizione;
    }

    @Column(name = "TIPO_PERIODO")
    public String getTipoPeriodo() {
        return tipoPeriodo;
    }

    public void setTipoPeriodo(String tipoPeriodo) {
        this.tipoPeriodo = tipoPeriodo;
    }

    @Column(name = "DATA_INIZIO")
    public Long getDataInizio() {
        return dataInizio;
    }

    public void setDataInizio(Long dataInizio) {
        this.dataInizio = dataInizio;
    }

    @Column(name = "DATA_FINE")
    public Long getDataFine() {
        return dataFine;
    }

    public void setDataFine(Long dataFine) {
        this.dataFine = dataFine;
    }


    @Column(name = "FLAG_ROLLING")
    public String getFlagRolling() {
        return flagRolling;
    }

    public void setFlagRolling(String flagRolling) {
        this.flagRolling = flagRolling;
    }

    @Column(name = "DISTANZA_DA_ANNO_CORRENTE")
    public Long getDistanzaDaAnnoCorrente() {
        return distanzaDaAnnoCorrente;
    }

    public void setDistanzaDaAnnoCorrente(Long distanzaDaAnnoCorrente) {
        this.distanzaDaAnnoCorrente = distanzaDaAnnoCorrente;
    }

    @Column(name = "SEMESTRE")
    public Long getSemestre() {
        return semestre;
    }

    public void setSemestre(Long semestre) {
        this.semestre = semestre;
    }
}
