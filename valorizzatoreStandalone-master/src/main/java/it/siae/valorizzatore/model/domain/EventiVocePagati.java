package it.siae.valorizzatore.model.domain;

import java.io.Serializable;

/**
 * Created by idilello on 6/9/16.
 */
public class EventiVocePagati implements Serializable {

    private Long idEvento;
    private String voceIncasso;
    private String numeroFattura;
    private Double importDem;
    private Long numeroPmTotaliAttesi;
    private Long numeroPmTotaliAttesiSpalla;

    public Long getIdEvento() {
        return idEvento;
    }

    public void setIdEvento(Long idEvento) {
        this.idEvento = idEvento;
    }

    public String getVoceIncasso() {
        return voceIncasso;
    }

    public void setVoceIncasso(String voceIncasso) {
        this.voceIncasso = voceIncasso;
    }

    public Double getImportDem() {
        return importDem;
    }

    public void setImportDem(Double importDem) {
        this.importDem = importDem;
    }

    public Long getNumeroPmTotaliAttesi() {
        return numeroPmTotaliAttesi;
    }

    public void setNumeroPmTotaliAttesi(Long numeroPmTotaliAttesi) {
        this.numeroPmTotaliAttesi = numeroPmTotaliAttesi;
    }

    public Long getNumeroPmTotaliAttesiSpalla() {
        return numeroPmTotaliAttesiSpalla;
    }

    public void setNumeroPmTotaliAttesiSpalla(Long numeroPmTotaliAttesiSpalla) {
        this.numeroPmTotaliAttesiSpalla = numeroPmTotaliAttesiSpalla;
    }

    public String getNumeroFattura() {
        return numeroFattura;
    }

    public void setNumeroFattura(String numeroFattura) {
        this.numeroFattura = numeroFattura;
    }
}
