package it.siae.valorizzatore.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by idilello on 6/7/16.
 */
@Entity
@Table(name = "MOVIMENTO_CONTABILE_158")
public class MovimentoContabile  implements Serializable {

    private Long id;
    private Long idCartella;
    private Long idEvento;
    private Long idProgrammaMusicale;
    private String tipologiaMovimento;
    private Double importoManuale;
    private Double importoTotDemLordo;
    private Double importoTotDem;
    private Character flagRipartitoSemestrePrecedente;
    private Date dataOraUltimaModifica;
    private String voceIncasso;
    private String numProgrammaMusicale;
    private Long reversale;
    private Integer contabilita;
    private Integer contabilitaOrg;
    private String flagSospeso;
    private String flagNoMaggiorazione;
    private Long numeroFattura;
    private Long numeroPmPrevisti;
    private Long numeroPmPrevistiSpalla;
    private String tipoDocumentoContabile;
    private String utenteUltimaModifica;
    private Date dataIns;
    private Character flagGruppoPrincipale;
    private String numeroPermesso;

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="MOVIMENTO_CONTABILE_SEQ")
    @Column(name = "ID_MOVIMENTO_CONTABILE_158", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    @Column(name = "ID_CARTELLA",nullable = false)
    public Long getIdCartella() {
        return idCartella;
    }

    public void setIdCartella(Long idCartella) {
        this.idCartella = idCartella;
    }

    @Column(name = "ID_EVENTO",nullable = false)
    public Long getIdEvento() {
        return idEvento;
    }

    public void setIdEvento(Long idEvento) {
        this.idEvento = idEvento;
    }

    @Column(name = "ID_PROGRAMMA_MUSICALE",nullable = false)
    public Long getIdProgrammaMusicale() {
        return idProgrammaMusicale;
    }

    public void setIdProgrammaMusicale(Long idProgrammaMusicale) {
        this.idProgrammaMusicale = idProgrammaMusicale;
    }


    @Column(name = "TIPOLOGIA_MOVIMENTO")
    public String getTipologiaMovimento() {
        return tipologiaMovimento;
    }

    public void setTipologiaMovimento(String tipologiaMovimento) {
        this.tipologiaMovimento = tipologiaMovimento;
    }

    @Column(name = "IMPORTO_MANUALE")
    public Double getImportoManuale() {
        return importoManuale;
    }

    public void setImportoManuale(Double importoManuale) {
        this.importoManuale = importoManuale;
    }

    @Column(name = "TOTALE_DEM_LORDO_PM")
    public Double getImportoTotDemLordo() {
        return importoTotDemLordo;
    }

    public void setImportoTotDemLordo(Double importoTotDemLordo) {
        this.importoTotDemLordo = importoTotDemLordo;
    }


    @Column(name = "IMPORTO_DEM_TOTALE")
    public Double getImportoTotDem() {
        return importoTotDem;
    }

    public void setImportoTotDem(Double importoTotDem) {
        this.importoTotDem = importoTotDem;
    }


    @Column(name = "PM_RIPARTITO_SEMESTRE_PREC")
    public Character getFlagRipartitoSemestrePrecedente() {
        return flagRipartitoSemestrePrecedente;
    }

    public void setFlagRipartitoSemestrePrecedente(Character flagRipartitoSemestrePrecedente) {
        this.flagRipartitoSemestrePrecedente = flagRipartitoSemestrePrecedente;
    }


    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_ORA_ULTIMA_MODIFICA")
    public Date getDataOraUltimaModifica() {
        return dataOraUltimaModifica;
    }

    public void setDataOraUltimaModifica(Date dataOraUltimaModifica) {
        this.dataOraUltimaModifica = dataOraUltimaModifica;
    }

    @Column(name = "CODICE_VOCE_INCASSO")
    public String getVoceIncasso() {
        return voceIncasso;
    }

    public void setVoceIncasso(String voceIncasso) {
        this.voceIncasso = voceIncasso;
    }


    @Column(name = "NUMERO_PM")
    public String getNumProgrammaMusicale() {
        return numProgrammaMusicale;
    }

    public void setNumProgrammaMusicale(String numProgrammaMusicale) {
        this.numProgrammaMusicale = numProgrammaMusicale;
    }

    @Column(name = "NUMERO_REVERSALE")
    public Long getReversale() {
        return reversale;
    }

    public void setReversale(Long reversale) {
        this.reversale = reversale;
    }

    @Column(name = "CONTABILITA")
    public Integer getContabilita() {
        return contabilita;
    }

    public void setContabilita(Integer contabilita) {
        this.contabilita = contabilita;
    }

    @Column(name = "CONTABILITA_ORIG")
    public Integer getContabilitaOrg() {
        return contabilitaOrg;
    }

    public void setContabilitaOrg(Integer contabilitaOrg) {
        this.contabilitaOrg = contabilitaOrg;
    }

    @Column(name = "FLAG_SOSPESO")
    public String getFlagSospeso() {
        return flagSospeso;
    }

    public void setFlagSospeso(String flagSospeso) {
        this.flagSospeso = flagSospeso;
    }

    @Column(name = "FLAG_NO_MAGGIORAZIONE")
    public String getFlagNoMaggiorazione() {
        return flagNoMaggiorazione;
    }

    public void setFlagNoMaggiorazione(String flagNoMaggiorazione) {
        this.flagNoMaggiorazione = flagNoMaggiorazione;
    }

    @Column(name = "NUMERO_FATTURA")
    public Long getNumeroFattura() {
        return numeroFattura;
    }

    public void setNumeroFattura(Long numeroFattura) {
        this.numeroFattura = numeroFattura;
    }

    @Column(name = "NUMERO_PM_PREVISTI")
    public Long getNumeroPmPrevisti() {
        return numeroPmPrevisti;
    }


    public void setNumeroPmPrevisti(Long numeroPmPrevisti) {
        this.numeroPmPrevisti = numeroPmPrevisti;
    }

    @Column(name = "NUMERO_PM_PREVISTI_SPALLA")
    public Long getNumeroPmPrevistiSpalla() {
        return numeroPmPrevistiSpalla;
    }

    public void setNumeroPmPrevistiSpalla(Long numeroPmPrevistiSpalla) {
        this.numeroPmPrevistiSpalla = numeroPmPrevistiSpalla;
    }

    @Column(name = "TIPO_DOCUMENTO_CONTABILE")
    public String getTipoDocumentoContabile() {
        return tipoDocumentoContabile;
    }

    public void setTipoDocumentoContabile(String tipoDocumentoContabile) {
        this.tipoDocumentoContabile = tipoDocumentoContabile;
    }

    @Column(name = "UTENTE_ULTIMA_MODIFICA")
    public String getUtenteUltimaModifica() {
        return utenteUltimaModifica;
    }

    public void setUtenteUltimaModifica(String utenteUltimaModifica) {
        this.utenteUltimaModifica = utenteUltimaModifica;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_INS")
    public Date getDataIns() {
        return dataIns;
    }

    public void setDataIns(Date dataIns) {
        this.dataIns = dataIns;
    }


    @Column(name = "FLAG_GRUPPO_PRINCIPALE")
    public Character getFlagGruppoPrincipale() {

        if (flagGruppoPrincipale!=null && flagGruppoPrincipale=='1')
            flagGruppoPrincipale = 'Y';
        else
        if (flagGruppoPrincipale!=null && flagGruppoPrincipale=='0')
            flagGruppoPrincipale = 'N';

        return flagGruppoPrincipale;
    }

    public void setFlagGruppoPrincipale(Character flagGruppoPrincipale) {
        this.flagGruppoPrincipale = flagGruppoPrincipale;
    }

    @Column(name = "NUMERO_PERMESSO")
    public String getNumeroPermesso() {
        return numeroPermesso;
    }

    public void setNumeroPermesso(String numeroPermesso) {
        this.numeroPermesso = numeroPermesso;
    }
}
