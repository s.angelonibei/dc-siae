package it.siae.valorizzatore.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by idilello on 6/7/16.
 */
@Entity
@Table(name = "PROGRAMMA_MUSICALE")
public class ProgrammaMusicale implements Serializable {


    private Long id;
    private Long numeroProgrammaMusicale;
    private Long progressivo;
    private Long totaleCedole;
    private Double totaleDurataCedole;
    private String foglio;
    private Long codiceCampionamento;
    private Character flagDaCampionare;
    private String pmRiferimento;
    private Long risultatoCalcoloResto;
    private Date dataOraUltimaModifica;
    private String utenteUltimaModifica;
    private Long giornateTrattenimento;
    private Long fogli;
    private Character flagPmCorrente;
    private String tipologiaPm;
    private Long copia;

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="PROGRAMMA_MUSICALE_SEQ")
    @Column(name = "ID_PROGRAMMA_MUSICALE", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    @Column(name = "NUMERO_PROGRAMMA_MUSICALE",nullable = false)
    public Long getNumeroProgrammaMusicale() {
        return numeroProgrammaMusicale;
    }

    public void setNumeroProgrammaMusicale(Long numeroProgrammaMusicale) {
        this.numeroProgrammaMusicale = numeroProgrammaMusicale;
    }

    @Column(name = "PROGRESSIVO")
    public Long getProgressivo() {
        return progressivo;
    }

    public void setProgressivo(Long progressivo) {
        this.progressivo = progressivo;
    }

    @Column(name = "TOTALE_CEDOLE")
    public Long getTotaleCedole() {
        return totaleCedole;
    }

    public void setTotaleCedole(Long totaleCedole) {
        this.totaleCedole = totaleCedole;
    }



    @Column(name = "TOTALE_DURATA_CEDOLE")
    public Double getTotaleDurataCedole() {
        return totaleDurataCedole;
    }

    public void setTotaleDurataCedole(Double totaleDurataCedole) {
        this.totaleDurataCedole = totaleDurataCedole;
    }

    @Column(name = "FOGLIO")
    public String getFoglio() {
        return foglio;
    }

    public void setFoglio(String foglio) {
        this.foglio = foglio;
    }

    @Column(name = "CODICE_CAMPIONAMENTO")
    public Long getCodiceCampionamento() {
        return codiceCampionamento;
    }

    public void setCodiceCampionamento(Long codiceCampionamento) {
        this.codiceCampionamento = codiceCampionamento;
    }


    @Column(name = "FLAG_DA_CAMPIONARE")
    public Character getFlagDaCampionare() {
        return flagDaCampionare;
    }

    public void setFlagDaCampionare(Character flagDaCampionare) {
        this.flagDaCampionare = flagDaCampionare;
    }

    @Column(name = "PM_RIFERIMENTO")
    public String getPmRiferimento() {
        return pmRiferimento;
    }

    public void setPmRiferimento(String pmRiferimento) {
        this.pmRiferimento = pmRiferimento;
    }

    @Column(name = "RISULTATO_CALCOLO_RESTO")
    public Long getRisultatoCalcoloResto() {
        return risultatoCalcoloResto;
    }

    public void setRisultatoCalcoloResto(Long risultatoCalcoloResto) {
        this.risultatoCalcoloResto = risultatoCalcoloResto;
    }


    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_ORA_ULTIMA_MODIFICA")
    public Date getDataOraUltimaModifica() {
        return dataOraUltimaModifica;
    }

    public void setDataOraUltimaModifica(Date dataOraUltimaModifica) {
        this.dataOraUltimaModifica = dataOraUltimaModifica;
    }

    @Column(name = "UTENTE_ULTIMA_MODIFICA")
    public String getUtenteUltimaModifica() {
        return utenteUltimaModifica;
    }

    public void setUtenteUltimaModifica(String utenteUltimaModifica) {
        this.utenteUltimaModifica = utenteUltimaModifica;
    }

    @Column(name = "GIONATE_TRATTENIMENTO")
    public Long getGiornateTrattenimento() {
        return giornateTrattenimento;
    }

    public void setGiornateTrattenimento(Long giornateTrattenimento) {
        this.giornateTrattenimento = giornateTrattenimento;
    }

    @Column(name = "FOGLI")
    public Long getFogli() {
        return fogli;
    }

    public void setFogli(Long fogli) {
        this.fogli = fogli;
    }

    @Column(name = "FLAG_PM_CORRENTE")
    public Character getFlagPmCorrente() {
        return flagPmCorrente;
    }

    public void setFlagPmCorrente(Character flagPmCorrente) {
        this.flagPmCorrente = flagPmCorrente;
    }

    @Column(name = "TIPOLOGIA_PM")
    public String getTipologiaPm() {
        return tipologiaPm;
    }

    public void setTipologiaPm(String tipologiaPm) {
        this.tipologiaPm = tipologiaPm;
    }

    @Column(name = "COPIA")
    public Long getCopia() {
        return copia;
    }

    public void setCopia(Long copia) {
        this.copia = copia;
    }
}
