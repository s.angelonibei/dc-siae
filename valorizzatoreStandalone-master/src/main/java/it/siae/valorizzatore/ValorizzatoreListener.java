package it.siae.valorizzatore;

/**
 * Created by idilello on 6/13/16.
 */
import it.siae.valorizzatore.model.InformazioniElaborazione;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import it.siae.valorizzatore.service.ITraceService;
import it.siae.valorizzatore.service.TraceService;
import it.siae.valorizzatore.utility.Constants;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;

/**
 * A TCP server that runs on port 9090.  When a client connects, it
 * sends the client the current date and time, then closes the
 * connection with that client.  Arguably just about the simplest
 * server you can write.
 */
@Component
public class ValorizzatoreListener {

    private int port;
    private int engineIndex;

    private static ValorizzatoreInstance valorizzatoreInstance;
    private static CampionamentoInstance campionamentoInstance;
    private static AggregazioneSiadaInstance aggregazioneSiadaInstance;

    protected Logger log = Logger.getLogger(this.getClass());

    //private static Thread vThread = null;

    public ValorizzatoreListener(int port, int engineIndex){
        this.port = port;
        this.engineIndex = engineIndex;
    }

    /**
     * Runs the server.
     */
    public String makeConnection() throws IOException {

        ServerSocket listener = new ServerSocket(port);

        log.info("Engine "+engineIndex+" in attesa sulla porta "+port+" ...");

        try {

            while (true) {

                Socket socket = listener.accept();

                try {

                    String risposta = "EMPTY";

                    // Riceve il comando dal client
                    BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    String command = input.readLine();

                    //log.info("Arrivato: "+command);

                    if (command.equalsIgnoreCase(Constants.STATUS)){
                        risposta = "ACTIVE";
                    }else
                    if (command.equalsIgnoreCase(Constants.DEACTIVATE)){
                        risposta = "INACTIVE";

                        // Invia la risposta del server
                        PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                        out.println(risposta);

                        System.exit(0);
                    }else
                    if (command.startsWith(Constants.START_RICALCOLO)){

                        log.info("Arrivato comando: "+Constants.START_RICALCOLO);

                        String[] parameters = command.split(" ");

                        Long elaborationId = Long.parseLong(parameters[1]);

                        log.info("Istanziato Valorizzatore ");
                        valorizzatoreInstance = new ValorizzatoreInstance(engineIndex, elaborationId);
                        valorizzatoreInstance.start();

                        risposta = "OK";

                    } else
                    if (command.equalsIgnoreCase(Constants.INFORMAZIONI_ELABORAZIONE)){
                        risposta = "0-0-0-0";
                        if (valorizzatoreInstance!=null){
                            InformazioniElaborazione info = valorizzatoreInstance.getInformazioniElaborazione();
                            risposta = info.toString();
                        }
                    } else
                    if (command.equalsIgnoreCase(Constants.SUSPEND_RICALCOLO)){
                        if (valorizzatoreInstance!=null){
                            valorizzatoreInstance.suspendElaborazione();
                            risposta = "SUSPENDED";
                        }
                    } else
                    if (command.equalsIgnoreCase(Constants.RESUME_RICALCOLO)){
                        if (valorizzatoreInstance!=null){
                            valorizzatoreInstance.resumeElaborazione();
                            risposta = "RESUMED";
                        }
                    } else
                    if (command.equalsIgnoreCase(Constants.START_CAMPIONAMENTO)){

                        log.info("Arrivato comando: "+Constants.START_CAMPIONAMENTO);

                        log.info("Istanziato Valorizzatore ");
                        campionamentoInstance = new CampionamentoInstance();
                        campionamentoInstance.start();

                        risposta = "OK";
                    }else
                    if (command.equalsIgnoreCase(Constants.INFORMAZIONI_ELABORAZIONE_CAMPIONAMENTO)){
                        risposta = "0-0-0-0";
                        if (campionamentoInstance!=null){
                            InformazioniElaborazione info = campionamentoInstance.getInformazioniElaborazione();
                            risposta = info.toString();
                        }
                    }  else
                    if (command.startsWith(Constants.START_AGGREGAZIONE)){

                        log.info("Arrivato comando: "+Constants.START_AGGREGAZIONE);

                        String[] parameters = command.split(" ");

                        Long elaborationId = Long.parseLong(parameters[1]);

                        log.info("Istanziato Aggregatore ");
                        aggregazioneSiadaInstance = new AggregazioneSiadaInstance(engineIndex, elaborationId);
                        aggregazioneSiadaInstance.start();

                        risposta = "OK";

                    }else
                    if (command.equalsIgnoreCase(Constants.INFORMAZIONI_ELABORAZIONE_AGGREGAZIONE)){
                        risposta = "0-0-0-0";
                        if (aggregazioneSiadaInstance!=null){
                            InformazioniElaborazione info = aggregazioneSiadaInstance.getInformazioniElaborazione();
                            risposta = info.toString();
                        }
                    }



                    // -----------------------------------------------------------------
                    // ----------- Comandi gestionali per Valorizzatore 2243 -----------
                    // -----------------------------------------------------------------
                    if (command.equalsIgnoreCase(Constants.STATUS_2243)){
                        risposta = "ACTIVE";
                    }else
                    if (command.equalsIgnoreCase(Constants.DEACTIVATE_2243)){
                        risposta = "INACTIVE";

                        // Invia la risposta del server
                        PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                        out.println(risposta);

                        System.exit(0);
                    }else
                    if (command.startsWith(Constants.START_RICALCOLO_2243)){

                        log.info("Arrivato comando: "+Constants.START_RICALCOLO_2243);

                        String[] parameters = command.split(" ");

                        Long elaborationId = Long.parseLong(parameters[1]);

                        log.info("Istanziato Valorizzatore 2243");
                        valorizzatoreInstance = new ValorizzatoreInstance(engineIndex, elaborationId, Constants.VOCE_2243);
                        valorizzatoreInstance.start();

                        risposta = "OK";

                    } else
                    if (command.equalsIgnoreCase(Constants.INFORMAZIONI_ELABORAZIONE_2243)){
                        risposta = "0-0-0-0";
                        if (valorizzatoreInstance!=null){
                            InformazioniElaborazione info = valorizzatoreInstance.getInformazioniElaborazione();
                            risposta = info.toString();
                        }
                    } else
                    if (command.startsWith(Constants.START_AGGREGAZIONE_2243)){

                        log.info("Arrivato comando: "+Constants.START_AGGREGAZIONE_2243);

                        String[] parameters = command.split(" ");

                        Long elaborationId = Long.parseLong(parameters[1]);

                        log.info("Istanziato Aggregatore ");
                        aggregazioneSiadaInstance = new AggregazioneSiadaInstance(engineIndex, elaborationId, Constants.VOCE_2243);
                        aggregazioneSiadaInstance.start();

                        risposta = "OK";

                    }else
                    if (command.equalsIgnoreCase(Constants.INFORMAZIONI_ELABORAZIONE_AGGREGAZIONE_2243)){
                        risposta = "0-0-0-0";
                        if (aggregazioneSiadaInstance!=null){
                            InformazioniElaborazione info = aggregazioneSiadaInstance.getInformazioniElaborazione();
                            risposta = info.toString();
                        }
                    }


                    // Invia la risposta del server
                    PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                    out.println(risposta);

                } finally {
                    socket.close();
                }
            }
        }
        finally {
            listener.close();
        }
    }


    public static void main(String[] args) {

        try {


            if (args==null || args.length<2) {
                System.out.println("Specificare la porta di ascolto del servizio e l'indice univoco identificativo dell'engine");
                return;
            }

            String port = args[0];
            String engineIndex = args[1];

            // ToDo: Scommentare
            ValorizzatoreListener server = new ValorizzatoreListener(Integer.parseInt(port), Integer.parseInt(engineIndex));
            server.makeConnection();

            // ToDo: Commentare
            //Long elaborationId = 181L;
            //valorizzatoreInstance = new ValorizzatoreInstance(1, elaborationId, Constants.VOCE_2243);
            //valorizzatoreInstance = new ValorizzatoreInstance(1, elaborationId);
            //valorizzatoreInstance.start();
            // ------

        }catch(Exception e){
            e.printStackTrace();
        }
    }

}