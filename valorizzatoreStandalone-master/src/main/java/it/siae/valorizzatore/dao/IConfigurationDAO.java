package it.siae.valorizzatore.dao;

import it.siae.valorizzatore.model.CampionamentoConfig;

/**
 * Created by idilello on 6/9/16.
 */
public interface IConfigurationDAO {

    public String getParameter(String parameterName);

    public CampionamentoConfig getCampionamentoConfig(Long periodoContabilita);
}
