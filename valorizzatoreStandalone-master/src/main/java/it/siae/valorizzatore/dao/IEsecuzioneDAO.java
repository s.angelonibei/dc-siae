package it.siae.valorizzatore.dao;

import it.siae.valorizzatore.model.TracciamentoApplicativo;

/**
 * Created by idilello on 6/9/16.
 */
public interface IEsecuzioneDAO {


    public Long startEsecuzioneRicalcolo(Long idEsecuzioneRicalcolo, Long contabilitaInizio, Long contabilitaFine, String voceIncasso);

    public Long endEsecuzioneRicalcolo(Long idEsecuzioneRicalcolo, String esitoEsecuzione, String descrizioneEsitoEsecuzione);

    //public Long insertImportoRicalcolato(Long idMovimentoContabile, Long idEvento, Long numeroFattura, String voceIncasso, Double importo, Long idEsecuzioneRicalcolo);

    //public Long insertSospesi(Long idEvento, Long numeroFattura, String voceIncasso, Long idProgrammaMusicale, String motivazioneSospensione, Long idEsecuzioneRicalcolo);

    public void trace(TracciamentoApplicativo tracciamentoApplicativo);

}
