package it.siae.valorizzatore.dao;

import it.siae.valorizzatore.model.dwh.EDWRipartizione;

/**
 * Created by idilello on 6/8/16.
 */
public interface IEdwDAO {

    public EDWRipartizione getUltimaRipartizione();
}
