package it.siae.valorizzatore.dao;

import it.siae.valorizzatore.model.EsecuzioneRicalcolo;
import it.siae.valorizzatore.model.MovimentoSiada;
import it.siae.valorizzatore.model.MovimentoSiada2243;
import it.siae.valorizzatore.utility.Constants;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by idilello on 8/4/16.
 */
@Repository("gestioneSiadaDAO")
public class GestioneSiadaDAO implements IGestioneSiadaDAO{

    @PersistenceContext(unitName = "VALORIZZATORE")
    private EntityManager entityManager;


    public List<EsecuzioneRicalcolo> getContabilitaScaricate() {

        String query = "select c from EsecuzioneRicalcolo c where c.contabilitaSiada is not null";

        List<EsecuzioneRicalcolo> listaEsecuzioni = (List<EsecuzioneRicalcolo>) entityManager.createQuery(query)
                                                            .getResultList();


        if (listaEsecuzioni == null){
            listaEsecuzioni = new ArrayList<EsecuzioneRicalcolo>();
        }

        return listaEsecuzioni;

    }

    public EsecuzioneRicalcolo getEsecuzioneDaAggregare(Long esecuzioneId){

        String query = "select c from EsecuzioneRicalcolo c where c.id=:esecuzioneId";

        List<EsecuzioneRicalcolo> listaEsecuzioni = (List<EsecuzioneRicalcolo>) entityManager.createQuery(query)
                .setParameter("esecuzioneId",esecuzioneId)
                .getResultList();

        if (listaEsecuzioni != null && listaEsecuzioni.size()>0){
            return listaEsecuzioni.get(0);
        }else{
             return null;
        }

    }


    public int verificaContabilitaScaricata(Long periodoContabile) {

        String query =  "select count(m) " +
                        "from MovimentoSiada m " +
                        "where m.meseInoltroSiada=:meseInoltroSiada";

        Query q = entityManager.createQuery(query);

        q.setParameter("meseInoltroSiada", periodoContabile.intValue());

        List<Long> results = (List<Long>) q.getResultList();

        if (results != null && results.size() > 0) {
            results.get(0).intValue();
        }

          return 0;
    }

    private MovimentoSiada getMovimentoSiada(MovimentoSiada movimentoSiada){

        String query =  "select m " +
                        "from MovimentoSiada m " +
                        "where m.idEvento=:idEvento and m.idProgrammaMusicale=:idProgrammaMusicale and m.voceIncasso=:voceIncasso and m.numeroFattura=:numeroFattura ";

        Query q = entityManager.createQuery(query);

        q.setParameter("idEvento", movimentoSiada.getIdEvento());
        q.setParameter("idProgrammaMusicale", movimentoSiada.getIdProgrammaMusicale());
        q.setParameter("voceIncasso", movimentoSiada.getVoceIncasso());
        q.setParameter("numeroFattura", movimentoSiada.getNumeroFattura());

        List<MovimentoSiada> results = (List<MovimentoSiada>) q.getResultList();

        if (results != null && results.size() > 0) {
            return results.get(0);
        }

        return null;

    }


    private MovimentoSiada2243 getMovimentoSiada2243(MovimentoSiada2243 movimentoSiada){

        String query =  "select m " +
                "from MovimentoSiada2243 m " +
                "where m.idEvento=:idEvento and m.idProgrammaMusicale=:idProgrammaMusicale and m.voceIncasso=:voceIncasso and m.numeroFattura=:numeroFattura ";

        Query q = entityManager.createQuery(query);

        q.setParameter("idEvento", movimentoSiada.getIdEvento());
        q.setParameter("idProgrammaMusicale", movimentoSiada.getIdProgrammaMusicale());
        q.setParameter("voceIncasso", movimentoSiada.getVoceIncasso());
        q.setParameter("numeroFattura", movimentoSiada.getNumeroFattura());

        List<MovimentoSiada2243> results = (List<MovimentoSiada2243>) q.getResultList();

        if (results != null && results.size() > 0) {
            return results.get(0);
        }

        return null;

    }



    @Transactional(propagation= Propagation.REQUIRES_NEW)
    public List<MovimentoSiada> getMovimentoSiada(Long idEvento, String voceIncasso, Integer fineUltimaRipartizione){

        String query =  "select m " +
                "from MovimentoSiada m " +
                "where m.idEvento=:idEvento and m.voceIncasso=:voceIncasso and m.meseInoltroSiada<=:fineUltimaRipartizione ";

        Query q = entityManager.createQuery(query);

        q.setParameter("idEvento", idEvento);
        q.setParameter("voceIncasso", voceIncasso);
        q.setParameter("fineUltimaRipartizione", fineUltimaRipartizione);

        List<MovimentoSiada> results = (List<MovimentoSiada>) q.getResultList();

        return results;

    }


    @Transactional(propagation= Propagation.REQUIRES_NEW)
    public void deleteMovimentoSiada(){

        String query = "delete from movimenti_siada where data_elaborazione is null";

        Query q = entityManager.createNativeQuery(query);

        int result = q.executeUpdate();

        entityManager.flush();

    }


    @Transactional(propagation= Propagation.REQUIRES_NEW)
    public void insertMovimentoSiada(MovimentoSiada movimentoSiada){

        //MovimentoSiada movimento = getMovimentoSiada(movimentoSiada);
        // Inserisce il nuovo movimento
        //if (movimento == null){
        //    entityManager.persist(movimentoSiada);
        //    entityManager.flush();
        //} else{
            // Aggiorna il movimento esistente
        //    movimento.setDataInizioEvento(movimentoSiada.getDataInizioEvento());
        //    movimento.setDataFineEvento(movimentoSiada.getDataFineEvento());
        //    movimento.setImporto(movimentoSiada.getImporto());
        //    movimento.setImportoSingolaCedola(movimentoSiada.getImportoSingolaCedola());
        //    movimento.setMeseContabile(movimentoSiada.getMeseContabile());
        //    movimento.setMeseInoltroSiada(movimentoSiada.getMeseInoltroSiada());
        //    movimento.setDataIns(new Date());

        //    entityManager.merge(movimento);
        //    entityManager.flush();
        //}

        if (movimentoSiada.getDataFineEvento()==null) {
            movimentoSiada.setDataFineEvento(movimentoSiada.getDataInizioEvento());
        }

        entityManager.persist(movimentoSiada);
        entityManager.flush();

    }


    @Transactional(propagation= Propagation.REQUIRES_NEW)
    public void insertMovimentoSiada2243(MovimentoSiada2243 movimentoSiada){

        //MovimentoSiada2243 movimento = getMovimentoSiada2243(movimentoSiada);
        // Inserisce il nuovo movimento
        //if (movimento == null){
        //    entityManager.persist(movimentoSiada);
        //    entityManager.flush();
        //} else{
            // Aggiorna il movimento esistente
        //    movimento.setDataInizioEvento(movimentoSiada.getDataInizioEvento());
        //    movimento.setDataFineEvento(movimentoSiada.getDataFineEvento());
        //    movimento.setImporto(movimentoSiada.getImporto());
        //    movimento.setImportoSingolaCedola(movimentoSiada.getImportoSingolaCedola());
        //    movimento.setMeseContabile(movimentoSiada.getMeseContabile());
        //    movimento.setMeseInoltroSiada(movimentoSiada.getMeseInoltroSiada());
        //    movimento.setDataIns(new Date());
        //    entityManager.merge(movimento);
        //    entityManager.flush();
        //}

        if (movimentoSiada.getDataFineEvento()==null) {
            movimentoSiada.setDataFineEvento(movimentoSiada.getDataInizioEvento());
        }

        entityManager.persist(movimentoSiada);
        entityManager.flush();

    }



    @Transactional
    public List<MovimentoSiada> getMovimentazioniAggregate(EsecuzioneRicalcolo esecuzione, int minInterval, int maxInterval){

        List<MovimentoSiada> movimentiSiada = new ArrayList<MovimentoSiada>();

        String query=
        "select ID_PROGRAMMA_MUSICALE, NUMERO_PM, ID_EVENTO, DATA_INIZIO_EVENTO, DATA_FINE_EVENTO,CODICE_VOCE_INCASSO, NUMERO_FATTURA, IMPORTO, CONTABILITA "+
        "from ( "+
        //"-- Use case 1 PM (totale 15 Use Case) "+
        "select m.ID_PROGRAMMA_MUSICALE, m.NUMERO_PM, max(m.CONTABILITA) as CONTABILITA, min(e.ID_EVENTO) as ID_EVENTO, min(e.DATA_INIZIO_EVENTO) as DATA_INIZIO_EVENTO, max(e.DATA_FINE_EVENTO) as DATA_FINE_EVENTO, "+
        "m.CODICE_VOCE_INCASSO, max(m.NUMERO_FATTURA) as NUMERO_FATTURA, "+
        "sum(decode(m.TIPO_DOCUMENTO_CONTABILE,'501', m.IMPORTO_DEM_TOTALE, '221',-m.IMPORTO_DEM_TOTALE)) as IMPORTO  "+
        "from movimento_contabile_158 m, MANIFESTAZIONE e, esecuzione_ricalcolo r, sospesi_ricalcolo s "+
        "where m.ID_EVENTO=e.ID_EVENTO "+
        "and s.id_esecuzione_ricalcolo=r.id_esecuzione_ricalcolo "+
        "and s.motivazione_sospensione='EVENTO_VOCE_UNICO_PM_ATTESO' "+
        "and s.id_movimento_contabile=m.id_movimento_contabile_158  "+
        "and m.CONTABILITA>=? and m.CONTABILITA<=? "+
        "and r.id_esecuzione_ricalcolo=? "+
        "and m.CODICE_VOCE_INCASSO<>? "+
        "group by m.ID_PROGRAMMA_MUSICALE, m.NUMERO_PM, m.CODICE_VOCE_INCASSO "+
        "union  "+
        //"-- Use case q PM (totale 16 Use Case) "+
        "select m.ID_PROGRAMMA_MUSICALE, m.NUMERO_PM, max(m.CONTABILITA) as CONTABILITA, min(m.ID_EVENTO) as ID_EVENTO, min(e.DATA_INIZIO_EVENTO) as DATA_INIZIO_EVENTO, max(e.DATA_FINE_EVENTO) as DATA_FINE_EVENTO, "+
        "m.CODICE_VOCE_INCASSO, max(m.NUMERO_FATTURA) as NUMERO_FATTURA,  "+
        "sum(decode(m.TIPO_DOCUMENTO_CONTABILE,'501', i.IMPORTO, '221',-i.IMPORTO)) as IMPORTO "+
        "from movimento_contabile_158 m, MANIFESTAZIONE e, esecuzione_ricalcolo r, importo_ricalcolato i "+
        "where m.ID_EVENTO=e.ID_EVENTO "+
        "and i.id_esecuzione_ricalcolo=r.id_esecuzione_ricalcolo "+
        "and i.id_movimento_contabile=m.id_movimento_contabile_158 "+
        "and m.CONTABILITA>=? and m.CONTABILITA<=? "+
        "and r.id_esecuzione_ricalcolo=? "+
        "and m.CODICE_VOCE_INCASSO<>? "+
        "group by m.CODICE_VOCE_INCASSO, m.NUMERO_PM, m.ID_PROGRAMMA_MUSICALE "+
        ") "+
        "order by ID_PROGRAMMA_MUSICALE, NUMERO_PM, CODICE_VOCE_INCASSO asc ";

        String optimizedQuery = "select ID_PROGRAMMA_MUSICALE, NUMERO_PM, ID_EVENTO, DATA_INIZIO_EVENTO, DATA_FINE_EVENTO,CODICE_VOCE_INCASSO, NUMERO_FATTURA, IMPORTO, CONTABILITA "+
                                    "from ( "+
                                    "       select /*+ FIRST_ROWS(n) */ a.*, ROWNUM rnum "+
                                    "       from ( "+
                                    query +
                                    "             ) a "+
                                    "where ROWNUM <=? "+
                                    ") "+
                                    "where rnum  >=? ";

        Query q = entityManager.createNativeQuery(optimizedQuery);

        q.setParameter(1, esecuzione.getContabilitaInizio());
        q.setParameter(2, esecuzione.getContabilitaFine());
        q.setParameter(3, esecuzione.getId());
        q.setParameter(4, Constants.VOCE_2243);

        q.setParameter(5, esecuzione.getContabilitaInizio());
        q.setParameter(6, esecuzione.getContabilitaFine());
        q.setParameter(7, esecuzione.getId());
        q.setParameter(8, Constants.VOCE_2243);

        q.setParameter(9, maxInterval);
        q.setParameter(10, minInterval);

        List<Object[]> results = (List<Object[]>)q.getResultList();
        Integer contabilitaSiada=null;

        if (results != null && results.size() > 0) {
            MovimentoSiada movimentoSiada=null;
            for (int i=0; i<results.size(); i++){

                movimentoSiada=new MovimentoSiada();

                movimentoSiada.setIdProgrammaMusicale(((BigDecimal)results.get(i)[0]).longValue());

                movimentoSiada.setIdEvento(((BigDecimal)results.get(i)[2]).longValue());
                movimentoSiada.setDataInizioEvento((Date)results.get(i)[3]);
                movimentoSiada.setDataFineEvento((Date)results.get(i)[4]);
                movimentoSiada.setVoceIncasso((String)results.get(i)[5]);
                movimentoSiada.setNumeroFattura(((BigDecimal)results.get(i)[6]).longValue());
                movimentoSiada.setImporto(((BigDecimal)results.get(i)[7]).doubleValue());
                movimentoSiada.setMeseContabile(((BigDecimal)results.get(i)[8]).intValue());

                // Si tratta di un PM Digitale
//                if (results.get(i)[1]!=null && ((String)results.get(i)[1]).startsWith("9")){
//                    movimentoSiada.setVoceIncassoSiada("DI"+movimentoSiada.getVoceIncasso());
//                }

                if (esecuzione.getContabilitaSiada()!=null){
                    contabilitaSiada = esecuzione.getContabilitaSiada().intValue();
                }else{
                    contabilitaSiada = esecuzione.getContabilitaFine().intValue();
                }
                movimentoSiada.setMeseInoltroSiada(contabilitaSiada);

                movimentoSiada.setDataIns(new Date());

                movimentiSiada.add(movimentoSiada);
            }

        }

        return movimentiSiada;
    }


    @Transactional
    public List<MovimentoSiada2243> getMovimentazioniAggregate2243(EsecuzioneRicalcolo esecuzione, int minInterval, int maxInterval){

        List<MovimentoSiada2243> movimentiSiada = new ArrayList<MovimentoSiada2243>();

        String query=
                "select ID_PROGRAMMA_MUSICALE, NUMERO_PM, ID_EVENTO, DATA_INIZIO_EVENTO, DATA_FINE_EVENTO,CODICE_VOCE_INCASSO, NUMERO_FATTURA, IMPORTO, CONTABILITA "+
                        "from ( "+
                        //"-- Use case 1 PM (totale 15 Use Case) "+
                        "select m.ID_PROGRAMMA_MUSICALE, m.NUMERO_PM, max(m.CONTABILITA) as CONTABILITA, min(e.ID_EVENTO) as ID_EVENTO, min(e.DATA_INIZIO_EVENTO) as DATA_INIZIO_EVENTO, max(e.DATA_FINE_EVENTO) as DATA_FINE_EVENTO, "+
                        "m.CODICE_VOCE_INCASSO, max(m.NUMERO_FATTURA) as NUMERO_FATTURA, "+
                        "sum(decode(m.TIPO_DOCUMENTO_CONTABILE,'501', m.IMPORTO_DEM_TOTALE, '221',-m.IMPORTO_DEM_TOTALE)) as IMPORTO  "+
                        "from movimento_contabile_158 m, MANIFESTAZIONE e, esecuzione_ricalcolo r, sospesi_ricalcolo s "+
                        "where m.ID_EVENTO=e.ID_EVENTO "+
                        "and s.id_esecuzione_ricalcolo=r.id_esecuzione_ricalcolo "+
                        "and s.motivazione_sospensione='EVENTO_VOCE_UNICO_PM_ATTESO' "+
                        "and s.id_movimento_contabile=m.id_movimento_contabile_158  "+
                        "and m.CONTABILITA>=? and m.CONTABILITA<=? "+
                        "and r.id_esecuzione_ricalcolo=? "+
                        "and m.CODICE_VOCE_INCASSO=? "+
                        "group by m.ID_PROGRAMMA_MUSICALE, m.NUMERO_PM, m.CODICE_VOCE_INCASSO "+
                        "union  "+
                        //"-- Use case q PM (totale 16 Use Case) "+
                        "select m.ID_PROGRAMMA_MUSICALE, m.NUMERO_PM, max(m.CONTABILITA) as CONTABILITA, min(m.ID_EVENTO) as ID_EVENTO, min(e.DATA_INIZIO_EVENTO) as DATA_INIZIO_EVENTO, max(e.DATA_FINE_EVENTO) as DATA_FINE_EVENTO, "+
                        "m.CODICE_VOCE_INCASSO, max(m.NUMERO_FATTURA) as NUMERO_FATTURA,  "+
                        "sum(decode(m.TIPO_DOCUMENTO_CONTABILE,'501', i.IMPORTO, '221',-i.IMPORTO)) as IMPORTO "+
                        "from movimento_contabile_158 m, MANIFESTAZIONE e, esecuzione_ricalcolo r, importo_ricalcolato i "+
                        "where m.ID_EVENTO=e.ID_EVENTO "+
                        "and i.id_esecuzione_ricalcolo=r.id_esecuzione_ricalcolo "+
                        "and i.id_movimento_contabile=m.id_movimento_contabile_158 "+
                        "and m.CONTABILITA>=? and m.CONTABILITA<=? "+
                        "and r.id_esecuzione_ricalcolo=? "+
                        "and m.CODICE_VOCE_INCASSO=? "+
                        "group by m.CODICE_VOCE_INCASSO, m.NUMERO_PM, m.ID_PROGRAMMA_MUSICALE "+
                        ") "+
                        "order by ID_PROGRAMMA_MUSICALE, NUMERO_PM, CODICE_VOCE_INCASSO asc ";

        String optimizedQuery = "select ID_PROGRAMMA_MUSICALE, NUMERO_PM, ID_EVENTO, DATA_INIZIO_EVENTO, DATA_FINE_EVENTO,CODICE_VOCE_INCASSO, NUMERO_FATTURA, IMPORTO, CONTABILITA "+
                "from ( "+
                "       select /*+ FIRST_ROWS(n) */ a.*, ROWNUM rnum "+
                "       from ( "+
                query +
                "             ) a "+
                "where ROWNUM <=? "+
                ") "+
                "where rnum  >=? ";

        Query q = entityManager.createNativeQuery(optimizedQuery);

        q.setParameter(1, esecuzione.getContabilitaInizio());
        q.setParameter(2, esecuzione.getContabilitaFine());
        q.setParameter(3, esecuzione.getId());
        q.setParameter(4, Constants.VOCE_2243);

        q.setParameter(5, esecuzione.getContabilitaInizio());
        q.setParameter(6, esecuzione.getContabilitaFine());
        q.setParameter(7, esecuzione.getId());
        q.setParameter(8, Constants.VOCE_2243);

        q.setParameter(9, maxInterval);
        q.setParameter(10, minInterval);

        List<Object[]> results = (List<Object[]>)q.getResultList();
        Integer contabilitaSiada=null;

        if (results != null && results.size() > 0) {
            MovimentoSiada2243 movimentoSiada2243=null;
            for (int i=0; i<results.size(); i++){

                movimentoSiada2243=new MovimentoSiada2243();

                movimentoSiada2243.setIdProgrammaMusicale(((BigDecimal)results.get(i)[0]).longValue());

                movimentoSiada2243.setIdEvento(((BigDecimal)results.get(i)[2]).longValue());
                movimentoSiada2243.setDataInizioEvento((Date)results.get(i)[3]);
                movimentoSiada2243.setDataFineEvento((Date)results.get(i)[4]);
                movimentoSiada2243.setVoceIncasso((String)results.get(i)[5]);
                movimentoSiada2243.setNumeroFattura(((BigDecimal)results.get(i)[6]).longValue());
                movimentoSiada2243.setImporto(((BigDecimal)results.get(i)[7]).doubleValue());
                movimentoSiada2243.setMeseContabile(((BigDecimal)results.get(i)[8]).intValue());

                // Si tratta di un PM Digitale
//                if (results.get(i)[1]!=null && ((String)results.get(i)[1]).startsWith("9")){
//                    movimentoSiada.setVoceIncassoSiada("DI"+movimentoSiada.getVoceIncasso());
//                }

                if (esecuzione.getContabilitaSiada()!=null){
                    contabilitaSiada = esecuzione.getContabilitaSiada().intValue();
                }else{
                    contabilitaSiada = esecuzione.getContabilitaFine().intValue();
                }
                movimentoSiada2243.setMeseInoltroSiada(contabilitaSiada);

                movimentoSiada2243.setDataIns(new Date());

                movimentiSiada.add(movimentoSiada2243);
            }

        }

        return movimentiSiada;
    }

}
