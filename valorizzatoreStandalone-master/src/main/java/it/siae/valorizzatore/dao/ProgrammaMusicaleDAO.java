package it.siae.valorizzatore.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import it.siae.valorizzatore.model.*;
import it.siae.valorizzatore.model.domain.ProgrammaRientrato;
import it.siae.valorizzatore.utility.Constants;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by idilello on 6/6/16.
 */
@Repository("programmaMusicaleDAO")
public class ProgrammaMusicaleDAO implements IProgrammaMusicaleDAO{

    @PersistenceContext(unitName = "VALORIZZATORE")
    private EntityManager entityManager;

    public List<TrattamentoPM> getTrattamentoPM(){

        String query = "select t from TrattamentoPM t where t.tipo is not null";

        List<TrattamentoPM> results = (List<TrattamentoPM>)entityManager.createQuery(query)
                                                                .getResultList();
        return results;

    }


    public List<MovimentoContabile> getMovimentiContabili(Long periodoContabileInizio, Long periodoContabileFine, String tipologiaPm, int minInterval, int maxInterval) throws Exception {

        List<MovimentoContabile> listaMovimenti = new ArrayList<MovimentoContabile>();

        String query = "select m.ID_MOVIMENTO_CONTABILE_158, m.ID_CARTELLA, m.ID_EVENTO, m.ID_PROGRAMMA_MUSICALE, m.TIPOLOGIA_MOVIMENTO, m.IMPORTO_MANUALE, m.TOTALE_DEM_LORDO_PM, m.IMPORTO_DEM_TOTALE, m.PM_RIPARTITO_SEMESTRE_PREC, "+
                       "m.DATA_ORA_ULTIMA_MODIFICA, m.CODICE_VOCE_INCASSO, m.NUMERO_PM, m.NUMERO_REVERSALE, m.CONTABILITA, m.FLAG_SOSPESO, m.FLAG_NO_MAGGIORAZIONE, m.NUMERO_FATTURA, m.NUMERO_PM_PREVISTI, "+
                       "m.NUMERO_PM_PREVISTI_SPALLA, m.TIPO_DOCUMENTO_CONTABILE, m.UTENTE_ULTIMA_MODIFICA, m.DATA_INS, m.FLAG_GRUPPO_PRINCIPALE, m.NUMERO_PERMESSO "+
                       "from movimento_contabile_158 m, Programma_Musicale p "+
                       "where m.id_Programma_Musicale=p.id_Programma_Musicale "+
                       //"and id_evento=76494623 and codice_voce_incasso=2244 "+
                        "and id_evento=90306011 " +
                       "and m.contabilita>=? and m.contabilita<=? "+
                       "and codice_voce_incasso<>? ";


        if (tipologiaPm!=null){
          query = query + "and p.tipologia_Pm=? ";
        }

        query = query + "order by m.id_Evento, m.numero_Fattura, m.codice_voce_Incasso, m.id_Programma_Musicale asc";


        String optimizedQuery = "select ID_MOVIMENTO_CONTABILE_158, ID_CARTELLA, ID_EVENTO, ID_PROGRAMMA_MUSICALE, TIPOLOGIA_MOVIMENTO, IMPORTO_MANUALE, TOTALE_DEM_LORDO_PM, IMPORTO_DEM_TOTALE, PM_RIPARTITO_SEMESTRE_PREC, "+
                                "DATA_ORA_ULTIMA_MODIFICA, CODICE_VOCE_INCASSO, NUMERO_PM, NUMERO_REVERSALE, CONTABILITA, FLAG_SOSPESO, FLAG_NO_MAGGIORAZIONE, NUMERO_FATTURA, NUMERO_PM_PREVISTI, "+
                                "NUMERO_PM_PREVISTI_SPALLA, TIPO_DOCUMENTO_CONTABILE, UTENTE_ULTIMA_MODIFICA, DATA_INS, FLAG_GRUPPO_PRINCIPALE, NUMERO_PERMESSO "+
                                "from ( "+
                                "       select /*+ FIRST_ROWS(n) */ a.*, ROWNUM rnum "+
                                "       from ( "+
                                        query +
                                "             ) a "+
                                "where ROWNUM <=? "+
                                ") "+
                                "where rnum  >=? ";

        Query q = entityManager.createNativeQuery(optimizedQuery);


          q.setParameter(1, periodoContabileInizio);
          q.setParameter(2, periodoContabileFine);
          q.setParameter(3, Constants.VOCE_2243);

          if (tipologiaPm!=null){
            q.setParameter(4, tipologiaPm);
            q.setParameter(5, maxInterval);
            q.setParameter(6, minInterval);
          }else{
            q.setParameter(4, maxInterval);
            q.setParameter(5, minInterval);
          }

        q.setParameter(3, "2244");


        /* Per query custom
        maxInterval=6000;
        q.setParameter(1, maxInterval);
        q.setParameter(2, minInterval);
         --------------- */

        List<Object[]> results = (List<Object[]>)q.getResultList();

        if (results != null && results.size() > 0) {

            MovimentoContabile movimentoContabile = null;

            for (int i = 0; i < results.size(); i++) {

                movimentoContabile = new MovimentoContabile();

                /*
                m.ID_MOVIMENTO_CONTABILE_158 -long
                m.ID_CARTELLA - long
                m.ID_EVENTO -long
                m.ID_PROGRAMMA_MUSICALE - long
                m.TIPOLOGIA_MOVIMENTO - string
                m.IMPORTO_MANUALE - Double
                m.TOTALE_DEM_LORDO_PM - Double
                m.IMPORTO_DEM_TOTALE - Double
                m.PM_RIPARTITO_SEMESTRE_PREC - Character
                m.DATA_ORA_ULTIMA_MODIFICA - Date
                m.CODICE_VOCE_INCASSO - String
                m.NUMERO_PM - String
                m.NUMERO_REVERSALE - long
                m.CONTABILITA - integer
                m.FLAG_SOSPESO - character
                m.FLAG_NO_MAGGIORAZIONE - character
                m.NUMERO_FATTURA - long
                m.NUMERO_PM_PREVISTI - long
                m.NUMERO_PM_PREVISTI_SPALLA - long
                m.TIPO_DOCUMENTO_CONTABILE - string
                m.UTENTE_ULTIMA_MODIFICA - string
                m.DATA_INS - date
                m.FLAG_GRUPPO_PRINCIPALE - character
                m.NUMERO_PERMESSO - string
                */

                movimentoContabile.setId(((BigDecimal)results.get(i)[0]).longValue());
                movimentoContabile.setIdCartella(((BigDecimal)results.get(i)[1]).longValue());
                movimentoContabile.setIdEvento(((BigDecimal)results.get(i)[2]).longValue());
                movimentoContabile.setIdProgrammaMusicale(((BigDecimal)results.get(i)[3]).longValue());

                if (results.get(i)[4]!=null)
                 movimentoContabile.setTipologiaMovimento(((String)results.get(i)[4]));
                if (results.get(i)[5]!=null)
                 movimentoContabile.setImportoManuale((((BigDecimal)results.get(i)[5])).doubleValue());
                if (results.get(i)[6]!=null)
                 movimentoContabile.setImportoTotDemLordo((((BigDecimal)results.get(i)[6])).doubleValue());
                if (results.get(i)[7]!=null) {
                    double importo = (((BigDecimal) results.get(i)[7])).doubleValue();
                    if (importo < 0)
                        importo = -importo;

                    movimentoContabile.setImportoTotDem(importo);
                }                
                if (results.get(i)[8]!=null)
                 movimentoContabile.setFlagRipartitoSemestrePrecedente(((Character)results.get(i)[8]));
                if (results.get(i)[9]!=null)
                 movimentoContabile.setDataOraUltimaModifica(((Date) results.get(i)[9]));

                movimentoContabile.setVoceIncasso(((String)results.get(i)[10]));
                movimentoContabile.setNumProgrammaMusicale((String)results.get(i)[11]);

                if (results.get(i)[12]!=null)
                 movimentoContabile.setReversale((((BigDecimal)results.get(i)[12])).longValue());
                if (results.get(i)[13]!=null)
                 movimentoContabile.setContabilita((((BigDecimal)results.get(i)[13])).intValue());
                if (results.get(i)[14]!=null)
                 movimentoContabile.setFlagSospeso(((String) results.get(i)[14]));
                if (results.get(i)[15]!=null)
                 movimentoContabile.setFlagNoMaggiorazione(((String) results.get(i)[15]));
                if (results.get(i)[16]!=null)
                 movimentoContabile.setNumeroFattura((((BigDecimal)results.get(i)[16])).longValue());
                if (results.get(i)[17]!=null)
                 movimentoContabile.setNumeroPmPrevisti((((BigDecimal)results.get(i)[17])).longValue());
                if (results.get(i)[18]!=null)
                 movimentoContabile.setNumeroPmPrevistiSpalla((((BigDecimal)results.get(i)[18])).longValue());

                movimentoContabile.setTipoDocumentoContabile(((String)results.get(i)[19]));
                movimentoContabile.setUtenteUltimaModifica(((String)results.get(i)[20]));
                movimentoContabile.setDataIns(((Date)results.get(i)[21]));

                if (results.get(i)[22]!=null)
                 movimentoContabile.setFlagGruppoPrincipale(((String)results.get(i)[22]).charAt(0));
                if (results.get(i)[23]!=null)
                 movimentoContabile.setNumeroPermesso(((String)results.get(i)[23]));

                listaMovimenti.add(movimentoContabile);
            }
        }

        return listaMovimenti;
    }


    public List<MovimentoContabile> getMovimentiContabili2243(Long periodoContabileInizio, Long periodoContabileFine, String tipologiaPm, int minInterval, int maxInterval) throws Exception {

        List<MovimentoContabile> listaMovimenti = new ArrayList<MovimentoContabile>();

        String query = "select m.ID_MOVIMENTO_CONTABILE_158, m.ID_CARTELLA, m.ID_EVENTO, m.ID_PROGRAMMA_MUSICALE, m.TIPOLOGIA_MOVIMENTO, m.IMPORTO_MANUALE, m.TOTALE_DEM_LORDO_PM, m.IMPORTO_DEM_TOTALE, m.PM_RIPARTITO_SEMESTRE_PREC, "+
                "m.DATA_ORA_ULTIMA_MODIFICA, m.CODICE_VOCE_INCASSO, m.NUMERO_PM, m.NUMERO_REVERSALE, m.CONTABILITA, m.FLAG_SOSPESO, m.FLAG_NO_MAGGIORAZIONE, m.NUMERO_FATTURA, m.NUMERO_PM_PREVISTI, "+
                "m.NUMERO_PM_PREVISTI_SPALLA, m.TIPO_DOCUMENTO_CONTABILE, m.UTENTE_ULTIMA_MODIFICA, m.DATA_INS, m.FLAG_GRUPPO_PRINCIPALE, m.NUMERO_PERMESSO "+
                "from movimento_contabile_158 m, Programma_Musicale p "+
                "where m.id_Programma_Musicale=p.id_Programma_Musicale "+
                //"and id_evento=76505510 and codice_voce_incasso=2243 "+
                "and m.contabilita>=? and m.contabilita<=? "+
                "and codice_voce_incasso=? ";;


        if (tipologiaPm!=null){
            query = query + "and p.tipologia_Pm=? ";
        }

        query = query + "order by m.id_Evento, m.numero_Fattura, m.codice_voce_Incasso, m.id_Programma_Musicale asc";


        String optimizedQuery = "select ID_MOVIMENTO_CONTABILE_158, ID_CARTELLA, ID_EVENTO, ID_PROGRAMMA_MUSICALE, TIPOLOGIA_MOVIMENTO, IMPORTO_MANUALE, TOTALE_DEM_LORDO_PM, IMPORTO_DEM_TOTALE, PM_RIPARTITO_SEMESTRE_PREC, "+
                "DATA_ORA_ULTIMA_MODIFICA, CODICE_VOCE_INCASSO, NUMERO_PM, NUMERO_REVERSALE, CONTABILITA, FLAG_SOSPESO, FLAG_NO_MAGGIORAZIONE, NUMERO_FATTURA, NUMERO_PM_PREVISTI, "+
                "NUMERO_PM_PREVISTI_SPALLA, TIPO_DOCUMENTO_CONTABILE, UTENTE_ULTIMA_MODIFICA, DATA_INS, FLAG_GRUPPO_PRINCIPALE, NUMERO_PERMESSO "+
                "from ( "+
                "       select /*+ FIRST_ROWS(n) */ a.*, ROWNUM rnum "+
                "       from ( "+
                query +
                "             ) a "+
                "where ROWNUM <=? "+
                ") "+
                "where rnum  >=? ";

        Query q = entityManager.createNativeQuery(optimizedQuery);


        q.setParameter(1, periodoContabileInizio);
        q.setParameter(2, periodoContabileFine);
        q.setParameter(3, Constants.VOCE_2243);

        if (tipologiaPm!=null){
            q.setParameter(4, tipologiaPm);
            q.setParameter(5, maxInterval);
            q.setParameter(6, minInterval);
        }else{
            q.setParameter(4, maxInterval);
            q.setParameter(5, minInterval);
        }


        /* Per query custom
        maxInterval=6000;
        q.setParameter(1, maxInterval);
        q.setParameter(2, minInterval);
         --------------- */

        List<Object[]> results = (List<Object[]>)q.getResultList();

        if (results != null && results.size() > 0) {

            MovimentoContabile movimentoContabile = null;

            for (int i = 0; i < results.size(); i++) {

                movimentoContabile = new MovimentoContabile();

                /*
                m.ID_MOVIMENTO_CONTABILE_158 -long
                m.ID_CARTELLA - long
                m.ID_EVENTO -long
                m.ID_PROGRAMMA_MUSICALE - long
                m.TIPOLOGIA_MOVIMENTO - string
                m.IMPORTO_MANUALE - Double
                m.TOTALE_DEM_LORDO_PM - Double
                m.IMPORTO_DEM_TOTALE - Double
                m.PM_RIPARTITO_SEMESTRE_PREC - Character
                m.DATA_ORA_ULTIMA_MODIFICA - Date
                m.CODICE_VOCE_INCASSO - String
                m.NUMERO_PM - String
                m.NUMERO_REVERSALE - long
                m.CONTABILITA - integer
                m.FLAG_SOSPESO - character
                m.FLAG_NO_MAGGIORAZIONE - character
                m.NUMERO_FATTURA - long
                m.NUMERO_PM_PREVISTI - long
                m.NUMERO_PM_PREVISTI_SPALLA - long
                m.TIPO_DOCUMENTO_CONTABILE - string
                m.UTENTE_ULTIMA_MODIFICA - string
                m.DATA_INS - date
                m.FLAG_GRUPPO_PRINCIPALE - character
                m.NUMERO_PERMESSO - string
                */

                movimentoContabile.setId(((BigDecimal)results.get(i)[0]).longValue());
                movimentoContabile.setIdCartella(((BigDecimal)results.get(i)[1]).longValue());
                movimentoContabile.setIdEvento(((BigDecimal)results.get(i)[2]).longValue());
                movimentoContabile.setIdProgrammaMusicale(((BigDecimal)results.get(i)[3]).longValue());

                if (results.get(i)[4]!=null)
                    movimentoContabile.setTipologiaMovimento(((String)results.get(i)[4]));
                if (results.get(i)[5]!=null)
                    movimentoContabile.setImportoManuale((((BigDecimal)results.get(i)[5])).doubleValue());
                if (results.get(i)[6]!=null)
                    movimentoContabile.setImportoTotDemLordo((((BigDecimal)results.get(i)[6])).doubleValue());

                if (results.get(i)[7]!=null) {
                    double importo = (((BigDecimal) results.get(i)[7])).doubleValue();
                    if (importo < 0)
                        importo = -importo;

                    movimentoContabile.setImportoTotDem(importo);
                }
                    if (results.get(i)[8]!=null)
                    movimentoContabile.setFlagRipartitoSemestrePrecedente(((Character)results.get(i)[8]));
                if (results.get(i)[9]!=null)
                    movimentoContabile.setDataOraUltimaModifica(((Date) results.get(i)[9]));

                movimentoContabile.setVoceIncasso(((String)results.get(i)[10]));
                movimentoContabile.setNumProgrammaMusicale((String)results.get(i)[11]);

                if (results.get(i)[12]!=null)
                    movimentoContabile.setReversale((((BigDecimal)results.get(i)[12])).longValue());
                if (results.get(i)[13]!=null)
                    movimentoContabile.setContabilita((((BigDecimal)results.get(i)[13])).intValue());
                if (results.get(i)[14]!=null)
                    movimentoContabile.setFlagSospeso(((String) results.get(i)[14]));
                if (results.get(i)[15]!=null)
                    movimentoContabile.setFlagNoMaggiorazione(((String) results.get(i)[15]));
                if (results.get(i)[16]!=null)
                    movimentoContabile.setNumeroFattura((((BigDecimal)results.get(i)[16])).longValue());
                if (results.get(i)[17]!=null)
                    movimentoContabile.setNumeroPmPrevisti((((BigDecimal)results.get(i)[17])).longValue());
                if (results.get(i)[18]!=null)
                    movimentoContabile.setNumeroPmPrevistiSpalla((((BigDecimal)results.get(i)[18])).longValue());

                movimentoContabile.setTipoDocumentoContabile(((String)results.get(i)[19]));
                movimentoContabile.setUtenteUltimaModifica(((String)results.get(i)[20]));
                movimentoContabile.setDataIns(((Date)results.get(i)[21]));

                if (results.get(i)[22]!=null)
                    movimentoContabile.setFlagGruppoPrincipale(((String)results.get(i)[22]).charAt(0));
                if (results.get(i)[23]!=null)
                    movimentoContabile.setNumeroPermesso(((String)results.get(i)[23]));

                listaMovimenti.add(movimentoContabile);
            }
        }

        return listaMovimenti;
    }



    public List<MovimentoContabile> getProgrammiMusicaliBSM(Long periodoContabileInizio, Long periodoContabileFine, String tipologiaPm){

        String query = "select m from MovimentoContabile m, ProgrammaMusicale p "+
                "where m.idProgrammaMusicale=p.id and m.contabilita>=:inizioPeriodoContabile "+
                "and m.contabilita<=:finePeriodoContabile "+
                //"and p.tipologiaPm=:tipologiaPm "+
                "and p.pmRiferimento is null and m.voceIncasso=:voceIncassoBSM "+
                " order by m.idEvento, m.numeroFattura, m.voceIncasso, m.idProgrammaMusicale asc";

        List<MovimentoContabile> results = (List<MovimentoContabile>)entityManager.createQuery(query)
                .setParameter("inizioPeriodoContabile",periodoContabileInizio)
                .setParameter("finePeriodoContabile",periodoContabileFine)
                //.setParameter("tipologiaPm",tipologiaPm)
                .setParameter("voceIncassoBSM",Constants.VOCE_BSM)
                .getResultList();

        if (results==null)
            results=new ArrayList<MovimentoContabile>();

        return results;

    }



    public List<MovimentoContabile> getProgrammiMusicaliConcertino(Long periodoContabileInizio, Long periodoContabileFine, String tipologiaPm){

        String query = "select m from MovimentoContabile m, ProgrammaMusicale p, TrattamentoPM t "+
                "where m.idProgrammaMusicale=p.id and m.contabilita>=:inizioPeriodoContabile "+
                "and m.contabilita<=:finePeriodoContabile "+
                //"and p.tipologiaPm=:tipologiaPm "+
                "and p.pmRiferimento is null and "+
                "m.voceIncasso=t.voceIncasso and t.tipo=:tipo and m.voceIncasso<>:voceIncassoBSM "+
                " order by m.idEvento, m.numeroFattura, m.voceIncasso, m.idProgrammaMusicale asc";

        List<MovimentoContabile> results = (List<MovimentoContabile>)entityManager.createQuery(query)
                .setParameter("inizioPeriodoContabile",periodoContabileInizio)
                .setParameter("finePeriodoContabile",periodoContabileFine)
                //.setParameter("tipologiaPm",tipologiaPm)
                .setParameter("tipo",Constants.TIPO_VOCE_CONCERTINO)
                .setParameter("voceIncassoBSM",Constants.VOCE_BSM)
                .getResultList();

        if (results==null)
            results=new ArrayList<MovimentoContabile>();

        return results;

    }


    public EventiPagati getInfoEventiPagatiEventoFatturaVoce(Long idEvento, Long numeroFattura, String voceIncasso){

        String query = "select e from EventiPagati e "+
                       "where e.idEvento=:idEvento and e.numeroFattura=:numeroFattura and e.voceIncasso=:voceIncasso ";

        List<EventiPagati> results = (List<EventiPagati>)entityManager.createQuery(query)
                .setParameter("idEvento",idEvento)
                .setParameter("numeroFattura",numeroFattura.toString())
                .setParameter("voceIncasso",voceIncasso)
                .getResultList();

        if (results!=null && results.size()>0){
            return results.get(0);
        }

        return null;

    }


    public List<EventiPagati> getListaPagamentiEventoVoce(Long idEvento, String voceIncasso){

        String query = "select e from EventiPagati e "+
                       "where e.idEvento=:idEvento and e.voceIncasso=:voceIncasso ";

        List<EventiPagati> results = (List<EventiPagati>)entityManager.createQuery(query)
                .setParameter("idEvento",idEvento)
                .setParameter("voceIncasso",voceIncasso)
                .getResultList();

            return results;
    }

    public List<EventiPagati> getListaPagamentiEventoVoceFattura(Long idEvento, String voceIncasso, Long numeroFattura){

        String query = "select e from EventiPagati e "+
                "where e.idEvento=:idEvento and e.voceIncasso=:voceIncasso and e.numeroFattura=:numeroFattura";

        List<EventiPagati> results = (List<EventiPagati>)entityManager.createQuery(query)
                .setParameter("idEvento", idEvento)
                .setParameter("voceIncasso", voceIncasso)
                .setParameter("numeroFattura", numeroFattura.toString())
                .getResultList();

        return results;
    }

    // Lista dei PM rientrati per Evento/Fattura/Voce
    public Map<Long, ProgrammaRientrato> getProgrammiRientratiEventoFatturaVoce(Long idEvento, Long numeroFattura, String voceIncasso, Long periodoContabileInizio, Long periodoContabileFine, String tipologiaPm){

        Map<Long, ProgrammaRientrato> programmiRientrati = new HashMap<Long, ProgrammaRientrato>();
        ProgrammaRientrato programmaRientrato;

        String query = "select p.id, m.flagGruppoPrincipale, x.totaleCedole , x.totaleDurataCedole " +
                       "from MovimentoContabile m, ProgrammaMusicale p , " +
                " (SELECT u.idProgrammaMusicale, count(u.id) as totaleCedole , sum(u.durata) as totaleDurataCedole " +
                " FROM Utilizzazione u group by (u.idProgrammaMusicale ) ) x "+
                       "where m.idProgrammaMusicale=p.id and m.idEvento=:idEvento and m.numeroFattura=:numeroFattura and x.idProgrammaMusicale=p.id "+
                       "and m.voceIncasso=:voceIncasso "+
                       "and m.contabilita>=:inizioPeriodoContabile "+
                       "and m.contabilita<=:finePeriodoContabile ";

                    if (tipologiaPm!=null) {
                        query = query + "and p.tipologiaPm=:tipologia_Pm";
                    }

                  Query q = entityManager.createQuery(query)
                    .setParameter("idEvento", idEvento)
                    .setParameter("numeroFattura", numeroFattura)
                    .setParameter("voceIncasso", voceIncasso)
                    .setParameter("inizioPeriodoContabile", periodoContabileInizio)
                    .setParameter("finePeriodoContabile", periodoContabileFine);

                    if (tipologiaPm!=null) {
                        q.setParameter("tipologia_Pm", tipologiaPm);
                    }

                  List<Object[]> results = (List<Object[]>)q.getResultList();

                if (results!=null && results.size()>0){
                  for (Object[] campi: results){
                      programmaRientrato = new ProgrammaRientrato();
                      programmaRientrato.setId((Long)campi[0]);

                      programmaRientrato.setFlagPMPrincipale('Y'); // Default

                      if (campi[1]!=null)
                          if ((Character)campi[1]=='1' || (Character)campi[1]=='Y')
                             programmaRientrato.setFlagPMPrincipale('Y');
                      else
                      if (campi[1]!=null)
                          if ((Character)campi[1]=='0' || (Character)campi[1]=='N')
                          programmaRientrato.setFlagPMPrincipale('N');

                      programmaRientrato.setCedole(((BigDecimal)campi[2]).longValue());
                      if((BigDecimal)campi[3]!=null)
                          programmaRientrato.setDurata(((BigDecimal)campi[3]).doubleValue());
                      else{
                          programmaRientrato.setDurata(0d);
                      }
                      programmiRientrati.put(programmaRientrato.getId(),programmaRientrato);
                  }
                }

        return programmiRientrati;

    }

    public Map<Long, ProgrammaRientrato> getProgrammiRientratiEventoVoce(Long idEvento, String voceIncasso, Long dataInizioRicalcolo, Long dataFineRicalcolo, String tipologiaPm){

        Map<Long, ProgrammaRientrato> programmiRientrati = new HashMap<Long, ProgrammaRientrato>();
        ProgrammaRientrato programmaRientrato;

        String query =
                    "select p.ID_PROGRAMMA_MUSICALE, m.FLAG_GRUPPO_PRINCIPALE, (select count(*) from VAL_ADMIN.UTILIZZAZIONE where id_programma_musicale=p.ID_PROGRAMMA_MUSICALE) as CEDOLE, (select nvl(sum(DURATA),0) from VAL_ADMIN.UTILIZZAZIONE where id_programma_musicale=p.ID_PROGRAMMA_MUSICALE)  as TOTALE_DURATA_CEDOLE "+
                    "from Movimento_Contabile_158 m, Programma_Musicale p "+
                    "where m.id_Programma_Musicale=p.ID_PROGRAMMA_MUSICALE "+
                    "and m.id_Evento=? "+
                    "and m.CODICE_VOCE_INCASSO=? "+
                    "and m.contabilita>=? "+
                    "and m.contabilita<=? ";

        if (tipologiaPm!=null){
            query = query + "and p.tipologia_Pm=? ";
        }

        Query q = entityManager.createNativeQuery(query);

        q.setParameter(1, idEvento);
        q.setParameter(2, voceIncasso);
        q.setParameter(3, dataInizioRicalcolo);
        q.setParameter(4, dataFineRicalcolo);

        if (tipologiaPm!=null) {
          q.setParameter(5, tipologiaPm);
        }

         List<Object[]> results = (List<Object[]>)q.getResultList();


        if (results!=null && results.size()>0){

            for (Object[] campi: results){

                programmaRientrato = new ProgrammaRientrato();
                programmaRientrato.setId(((BigDecimal)campi[0]).longValue());

                if (campi[1]!=null)
                    if (((String)campi[1]).equalsIgnoreCase("1") ||((String)campi[1]).equalsIgnoreCase("Y"))
                        programmaRientrato.setFlagPMPrincipale('Y');

                if (campi[1]!=null)
                    if (((String)campi[1]).equalsIgnoreCase("0") ||((String)campi[1]).equalsIgnoreCase("N"))
                        programmaRientrato.setFlagPMPrincipale('N');

                if (campi[2]!=null)
                    programmaRientrato.setCedole(((BigDecimal)campi[2]).longValue());

                if (campi[3]!=null)
                    programmaRientrato.setDurata(((BigDecimal)campi[3]).doubleValue());

                programmiRientrati.put(programmaRientrato.getId(),programmaRientrato);
            }
        }

        return programmiRientrati;

    }


    // Lista dei PM rientrati per Evento/Voce
    public Map<Long, ProgrammaRientrato> getProgrammiRientratiEventoVoceOLD(Long idEvento, String voceIncasso, Long dataInizioRicalcolo, Long dataFineRicalcolo){

        Map<Long, ProgrammaRientrato> programmiRientrati = new HashMap<Long, ProgrammaRientrato>();
        ProgrammaRientrato programmaRientrato;

        String query = "select p.id, m.flagGruppoPrincipale, p.totaleCedole, p.totaleDurataCedole " +
                "from MovimentoContabile m, ProgrammaMusicale p "+
                "where m.idProgrammaMusicale=p.id and m.idEvento=:idEvento and m.voceIncasso=:voceIncasso "+
                "and m.contabilita>=:dataInizioRicalcolo and m.contabilita<=:dataFineRicalcolo";

        List<Object[]> results = (List<Object[]>) entityManager.createQuery(query)
                .setParameter("idEvento", idEvento)
                .setParameter("voceIncasso", voceIncasso)
                .setParameter("dataInizioRicalcolo", dataInizioRicalcolo)
                .setParameter("dataFineRicalcolo", dataFineRicalcolo)
                .getResultList();

        if (results!=null && results.size()>0){

            for (Object[] campi: results){

                programmaRientrato = new ProgrammaRientrato();
                programmaRientrato.setId(((Long)campi[0]).longValue());

                if (campi[1]!=null)
                if ((Character)campi[1]=='1' || (Character)campi[1]=='Y')
                  programmaRientrato.setFlagPMPrincipale('Y');
                else
                if (campi[1]!=null)
                if ((Character)campi[1]=='0' || (Character)campi[1]=='N')
                  programmaRientrato.setFlagPMPrincipale('N');

                if (campi[2]!=null)
                 programmaRientrato.setCedole(((Long)campi[2]).longValue());

                if (campi[3]!=null)
                 programmaRientrato.setDurata(((Double)campi[3]).doubleValue());

                programmiRientrati.put(programmaRientrato.getId(),programmaRientrato);
            }
        }

        return programmiRientrati;

    }


    public ProgrammaMusicale getProgrammaMusicale(Long idProgrammaMusicale){
        return entityManager.find(ProgrammaMusicale.class, idProgrammaMusicale);
    }


    //@Transactional(propagation= Propagation.REQUIRED)
    public ProgrammaMusicale updateProgrammaMusicale(ProgrammaMusicale programmaMusicale){
        entityManager.persist(programmaMusicale);
        return programmaMusicale;
    }

    @Transactional(propagation=Propagation.REQUIRES_NEW)
    public Long insertSospesiRicalcolo(Long idEvento, Long numeroFattura, String voceIncasso, Long idEsecuzioneRicalcolo, Long idProgrammaMusicale, Long idMovimentoContabile, String motivazioneSospensione){

        SospesiRicalcolo sospesiRicalcolo = new SospesiRicalcolo();

        sospesiRicalcolo.setIdEvento(idEvento);
        sospesiRicalcolo.setIdEsecuzioneRicalcolo(idEsecuzioneRicalcolo);
        sospesiRicalcolo.setIdProgrammaMusicale(idProgrammaMusicale);
        sospesiRicalcolo.setVoceIncasso(voceIncasso);
        sospesiRicalcolo.setNumeroFattura(numeroFattura);
        sospesiRicalcolo.setMotivazioneSospensione(motivazioneSospensione);
        sospesiRicalcolo.setIdMovimentoContabile(idMovimentoContabile);

        entityManager.persist(sospesiRicalcolo);

        return sospesiRicalcolo.getId();
    }

    @Transactional(propagation= Propagation.REQUIRES_NEW)
    public Long insertImportoRicalcolato(Long idEvento, Long numeroFattura, String voceIncasso, Long idEsecuzioneRicalcolo, Long idMovimentoContabile, Double importo, Double importoSingolaCedola){

        ImportoRicalcolato importoRicalcolato = new ImportoRicalcolato();

        importoRicalcolato.setDataOraInserimento(new Date());
        importoRicalcolato.setIdEsecuzioneRicalcolo(idEsecuzioneRicalcolo);
        importoRicalcolato.setNumeroFattura(numeroFattura);
        importoRicalcolato.setIdEvento(idEvento);
        importoRicalcolato.setImporto(importo);
        importoRicalcolato.setVoceIncasso(voceIncasso);
        importoRicalcolato.setIdMovimentoContabile(idMovimentoContabile);
        importoRicalcolato.setImportoSingolaCedola(importoSingolaCedola);

        entityManager.persist(importoRicalcolato);

        return importoRicalcolato.getId();
    }

    // 1. Acquisire tutte le entry del periodo di ricalcolo che hanno PM_RIPARTITO_SEMESTRE_PREC a null
    // 2. Per ognuno di questi:
    //      - prende ID_EVENTO e VOCE_INCASSO
    //      - prende il periodo ultimo di ripartizione
    //      - Verifiche pregresse su VALORIZZATORE:
    //         x verifica se esiste sul DB del valorizzatore un qualche PM rientrato prima della data di ultima ripartizione
    //         x se esiste allora setta il flag a Y altrimenti a N
    //      - Verifiche pregresse su DWH
    //         x verifica su SUN se esistono altri PM colegati all'evento/voce
    //         x acquisisce tutti gli eventuali PM collegati
    //         x verifica se esiste sul DB del DWH un qualche PM rientrato prima della data di ultima ripartizione
    //         x se esiste allora setta il flag a Y altrimenti a N
    public void checkPMRipartitiPregressi(Long inizioPeriodoContabile, Long finePeriodoContabile, Long finePeriodoRipartizione){

        String query = "select m from MovimentoContabile m "+
                       "where m.contabilita>=:inizioPeriodoContabile "+
                       "and m.contabilita<=:finePeriodoContabile "+
                       "and m.flagRipartitoSemestrePrecedente is null";

        List<MovimentoContabile> listaMovimenti = (List<MovimentoContabile>)entityManager.createQuery(query)
                                            .setParameter("inizioPeriodoContabile",inizioPeriodoContabile)
                                            .setParameter("finePeriodoContabile",finePeriodoContabile)
                                            .getResultList();

         if (listaMovimenti!=null && listaMovimenti.size()>0){

             List<MovimentoContabile> listaMovimentiPMRipartiti;

              for (MovimentoContabile movimento: listaMovimenti){

                        query = "select m from MovimentoContabile m "+
                                "where m.contabilita<=:finePeriodoRipartizione "+
                                "and m.idEvento=:idEvento and m.voceIncasso=:voceIncasso ";

                        listaMovimentiPMRipartiti = (List<MovimentoContabile>)entityManager.createQuery(query)
                                  .setParameter("finePeriodoRipartizione",finePeriodoRipartizione)
                                  .setParameter("idEvento",movimento.getIdEvento())
                                  .setParameter("voceIncasso",movimento.getVoceIncasso())
                                  .getResultList();

                        if (listaMovimentiPMRipartiti!=null && listaMovimentiPMRipartiti.size()>0){
                            movimento.setFlagRipartitoSemestrePrecedente('Y');
                        }else{
                            movimento.setFlagRipartitoSemestrePrecedente('N');
                        }
                            entityManager.merge(movimento);
              }
         }
    }



    // Importo tot Evento/Voce (Può essere una lista di 221 e 501)
    // PM Attesi Evento/Voce, PM Attesi Evento/Voce Spalla
    /*
    public EventiVocePagati getInfoEventiPagatiEventoVoce(Long idEvento, String voceIncasso, Long periodoContabileInizio, Long periodoContabileFine) {

        EventiVocePagati eventiVocePagati = new EventiVocePagati();

        String query = "select e from EventiPagati e " +
                       "where e.idEvento=:idEvento and e.voceIncasso=:voceIncasso "+
                       "and e.contabilita>=:inizioPeriodoContabile "+
                       "and e.contabilita<=:finePeriodoContabile ";

        List<EventiPagati> results = (List<EventiPagati>) entityManager.createQuery(query)
                                                            .setParameter("idEvento", idEvento)
                                                            .setParameter("voceIncasso", voceIncasso)
                                                            .setParameter("inizioPeriodoContabile", periodoContabileInizio)
                                                            .setParameter("finePeriodoContabile", periodoContabileFine)
                                                            .getResultList();

        if (results != null && results.size() > 0) {

            for (EventiPagati eventoPagato: results){

                 if (eventiVocePagati.getImportDem()==null){

                     if (eventoPagato.getTipoDocumentoContabile().equalsIgnoreCase(Constants.DOCUMENTO_USCITE))
                         eventiVocePagati.setImportDem(-eventoPagato.getImportDem());
                      else
                         eventiVocePagati.setImportDem(eventoPagato.getImportDem());

                     eventiVocePagati.setIdEvento(eventoPagato.getIdEvento());
                     eventiVocePagati.setVoceIncasso(eventoPagato.getVoceIncasso());
                     eventiVocePagati.setNumeroPmTotaliAttesi(eventoPagato.getNumeroPmTotaliAttesi());
                     eventiVocePagati.setNumeroPmTotaliAttesiSpalla(eventoPagato.getNumeroPmTotaliAttesiSpalla());

                 }else{
                     if (eventoPagato.getTipoDocumentoContabile().equalsIgnoreCase(Constants.DOCUMENTO_USCITE))
                         eventiVocePagati.setImportDem(eventiVocePagati.getImportDem()-eventoPagato.getImportDem());
                     else
                         eventiVocePagati.setImportDem(eventiVocePagati.getImportDem()+eventoPagato.getImportDem());
                 }
            }

        }

        return eventiVocePagati;
    }
    */

     /*
    public EventiVocePagati getInfoEventiPagatiEventoVoce(Long idEvento, String fattura, String voceIncasso) {

        EventiVocePagati eventiVocePagati = new EventiVocePagati();

        String query = "select e from EventiPagati e " +
                "where e.idEvento=:idEvento and e.voceIncasso=:voceIncasso and e.numeroFattura=:numeroFattura ";

        List<EventiPagati> results = (List<EventiPagati>) entityManager.createQuery(query)
                .setParameter("idEvento", idEvento)
                .setParameter("voceIncasso", voceIncasso)
                .setParameter("numeroFattura", fattura)
                .getResultList();

        if (results != null && results.size() > 0) {

            for (EventiPagati eventoPagato: results){

                if (eventiVocePagati.getImportDem()==null){

                    if (eventoPagato.getTipoDocumentoContabile().equalsIgnoreCase(Constants.DOCUMENTO_USCITE))
                        eventiVocePagati.setImportDem(-eventoPagato.getImportDem());
                    else
                        eventiVocePagati.setImportDem(eventoPagato.getImportDem());

                    eventiVocePagati.setIdEvento(eventoPagato.getIdEvento());
                    eventiVocePagati.setVoceIncasso(eventoPagato.getVoceIncasso());
                    eventiVocePagati.setNumeroPmTotaliAttesi(eventoPagato.getNumeroPmTotaliAttesi());
                    eventiVocePagati.setNumeroPmTotaliAttesiSpalla(eventoPagato.getNumeroPmTotaliAttesiSpalla());
                    eventiVocePagati.setNumeroFattura(eventoPagato.getNumeroFattura());
                }else{
                    if (eventoPagato.getTipoDocumentoContabile().equalsIgnoreCase(Constants.DOCUMENTO_USCITE))
                        eventiVocePagati.setImportDem(eventiVocePagati.getImportDem()-eventoPagato.getImportDem());
                    else
                        eventiVocePagati.setImportDem(eventiVocePagati.getImportDem()+eventoPagato.getImportDem());
                }
            }

        }

        return eventiVocePagati;
    }
     */

       /*
    public EventiVocePagati getInfoEventiPagatiEventoVoceFattura(Long idEvento, Long fattura, String voceIncasso, Long periodoContabileInizio, Long periodoContabileFine ){

        EventiVocePagati eventiVocePagati = new EventiVocePagati();

        String query = "select e from EventiPagati e " +
                "where e.idEvento=:idEvento and e.voceIncasso=:voceIncasso and e.numeroFattura=:fattura "+
                "and e.contabilita>=:inizioPeriodoContabile "+
                "and e.contabilita<=:finePeriodoContabile ";

        List<EventiPagati> results = (List<EventiPagati>) entityManager.createQuery(query)
                .setParameter("idEvento", idEvento)
                .setParameter("voceIncasso", voceIncasso)
                .setParameter("fattura", fattura.toString())
                .setParameter("inizioPeriodoContabile", periodoContabileInizio)
                .setParameter("finePeriodoContabile", periodoContabileFine)
                .getResultList();

        if (results != null && results.size() > 0) {

            for (EventiPagati eventoPagato: results){

                if (eventiVocePagati.getImportDem()==null){

                    if (eventoPagato.getTipoDocumentoContabile().equalsIgnoreCase(Constants.DOCUMENTO_USCITE))
                        eventiVocePagati.setImportDem(-eventoPagato.getImportDem());
                    else
                        eventiVocePagati.setImportDem(eventoPagato.getImportDem());

                    eventiVocePagati.setIdEvento(eventoPagato.getIdEvento());
                    eventiVocePagati.setVoceIncasso(eventoPagato.getVoceIncasso());
                    eventiVocePagati.setNumeroPmTotaliAttesi(eventoPagato.getNumeroPmTotaliAttesi());
                    eventiVocePagati.setNumeroPmTotaliAttesiSpalla(eventoPagato.getNumeroPmTotaliAttesiSpalla());
                    eventiVocePagati.setNumeroFattura(eventoPagato.getNumeroFattura());

                }else{
                    if (eventoPagato.getTipoDocumentoContabile().equalsIgnoreCase(Constants.DOCUMENTO_USCITE))
                        eventiVocePagati.setImportDem(eventiVocePagati.getImportDem()-eventoPagato.getImportDem());
                    else
                        eventiVocePagati.setImportDem(eventiVocePagati.getImportDem()+eventoPagato.getImportDem());
                }
            }

        }

        return eventiVocePagati;

    }
    */


}
