package it.siae.valorizzatore.dao;

import it.siae.valorizzatore.model.EventiPagati;
import it.siae.valorizzatore.model.MovimentoContabile;
import it.siae.valorizzatore.model.ProgrammaMusicale;
import it.siae.valorizzatore.model.TrattamentoPM;
import it.siae.valorizzatore.model.domain.EventiVocePagati;
import it.siae.valorizzatore.model.domain.ProgrammaRientrato;

import java.util.List;
import java.util.Map;

/**
 * Created by idilello on 6/6/16.
 */
public interface IProgrammaMusicaleDAO {

    public List<TrattamentoPM> getTrattamentoPM();

    public List<MovimentoContabile> getMovimentiContabili(Long periodoContabileInizio, Long periodoContabileFine, String tipologiaPm, int minInterval, int maxInterval) throws Exception;

    public List<MovimentoContabile> getMovimentiContabili2243(Long periodoContabileInizio, Long periodoContabileFine, String tipologiaPm, int minInterval, int maxInterval) throws Exception;

    public EventiPagati getInfoEventiPagatiEventoFatturaVoce(Long idEvento, Long numeroFattura, String voceIncasso);

    public Map<Long, ProgrammaRientrato> getProgrammiRientratiEventoFatturaVoce(Long idEvento, Long numeroFattura, String voceIncasso, Long periodoContabileInizio, Long periodoContabileFine, String tipologiaPm);

    public Map<Long, ProgrammaRientrato> getProgrammiRientratiEventoVoce(Long idEvento, String voceIncasso, Long periodoContabileInizio, Long periodoContabileFine, String tipologiaPm);

    public Long insertSospesiRicalcolo(Long idEvento, Long numeroFattura, String voceIncasso, Long idEsecuzioneRicalcolo, Long idProgrammaMusicale, Long idMovimentoContabile, String motivazioneSospensione);

    public Long insertImportoRicalcolato(Long idEvento, Long numeroFattura, String voceIncasso, Long idEsecuzioneRicalcolo, Long idMovimentoContabile, Double importo, Double importoSingolaCedola);

    public List<MovimentoContabile> getProgrammiMusicaliBSM(Long periodoContabileInizio, Long periodoContabileFine, String tipologiaPm);

    public List<MovimentoContabile> getProgrammiMusicaliConcertino(Long periodoContabileInizio, Long periodoContabileFine, String tipologiaPm);

    public ProgrammaMusicale getProgrammaMusicale(Long idProgrammaMusicale);

    public ProgrammaMusicale updateProgrammaMusicale(ProgrammaMusicale programmaMusicale);

    public void checkPMRipartitiPregressi(Long inizioPeriodoContabile, Long finePeriodoContabile, Long dataFineUltimaRipartizione);

    public List<EventiPagati> getListaPagamentiEventoVoce(Long idEvento, String voceIncasso);

    public List<EventiPagati> getListaPagamentiEventoVoceFattura(Long idEvento, String voceIncasso, Long numeroFattura);

    //public EventiVocePagati getInfoEventiPagatiEventoVoce(Long idEvento, String voceIncasso, Long periodoContabileInizio, Long periodoContabileFine);

    //public EventiVocePagati getInfoEventiPagatiEventoVoceFattura(Long idEvento, Long fattura, String voceIncasso, Long periodoContabileInizio,Long periodoContabileFine);


}