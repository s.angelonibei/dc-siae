package it.siae.valorizzatore.dao;

import it.siae.valorizzatore.model.CampionamentoConfig;
import it.siae.valorizzatore.model.Configurazione;
import it.siae.valorizzatore.model.MovimentoContabile;
import it.siae.valorizzatore.model.TrattamentoPM;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by idilello on 6/9/16.
 */
@Repository("configurationDAO")
public class ConfigurationDAO implements IConfigurationDAO {

    @PersistenceContext(unitName = "VALORIZZATORE")
    private EntityManager entityManager;

    public String getParameter(String parameterName) {

        String query = "select c from Configurazione c where c.parameterName=:parameterName and c.flagAttivo='Y'";

        List<Configurazione> results = (List<Configurazione>) entityManager.createQuery(query)
                .setParameter("parameterName", parameterName)
                .getResultList();

        if (results != null && results.size() > 0)
            return results.get(0).getParameterValue();
        else
            return null;

    }


    public CampionamentoConfig getCampionamentoConfig(Long periodoContabilita) {

        String query = null;

        query = "select c from CampionamentoConfig c where c.contabilita=:periodoContabilite";

        Query q = entityManager.createQuery(query);

        List<CampionamentoConfig> results = (List<CampionamentoConfig>) q
                .setParameter("periodoContabilite", periodoContabilita)
                .getResultList();

        if (results != null && results.size() > 0)
            return results.get(0);
        else
            return null;

    }

}