package it.siae.valorizzatore.dao;

import it.siae.valorizzatore.model.dwh.EDWRipartizione;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by idilello on 6/8/16.
 */
@Repository("edwDAO")
public class EdwDAO implements IEdwDAO{

    @PersistenceContext(unitName = "DWH")
    private EntityManager entityManagerDwh;

    public EDWRipartizione getUltimaRipartizione(){

        String query = "select t from EDWRipartizione t where t.dataFine is not null order by t.dataFine desc";

        List<EDWRipartizione> results = (List<EDWRipartizione>)entityManagerDwh.createQuery(query)
                                            .getResultList();

        if (results!=null && results.size()>0)
            return results.get(0);

        return null;
    }

}
