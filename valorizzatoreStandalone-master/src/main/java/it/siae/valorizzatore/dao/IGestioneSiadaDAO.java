package it.siae.valorizzatore.dao;

import it.siae.valorizzatore.model.EsecuzioneRicalcolo;
import it.siae.valorizzatore.model.MovimentoSiada;
import it.siae.valorizzatore.model.MovimentoSiada2243;

import java.util.Date;
import java.util.List;

/**
 * Created by idilello on 8/4/16.
 */
public interface IGestioneSiadaDAO {

    public List<EsecuzioneRicalcolo> getContabilitaScaricate();

    public EsecuzioneRicalcolo getEsecuzioneDaAggregare(Long esecuzioneId);

    public List<MovimentoSiada> getMovimentoSiada(Long idEvento, String voceIncasso, Integer fineUltimaRipartizione);

    public void insertMovimentoSiada(MovimentoSiada movimentoSiada);

    public void insertMovimentoSiada2243(MovimentoSiada2243 movimentoSiada);

    public int verificaContabilitaScaricata(Long periodoContabile);

    public List<MovimentoSiada> getMovimentazioniAggregate(EsecuzioneRicalcolo esecuzione, int minInterval, int maxInterval);

    public List<MovimentoSiada2243> getMovimentazioniAggregate2243(EsecuzioneRicalcolo esecuzione, int minInterval, int maxInterval);

    public void deleteMovimentoSiada();
}
