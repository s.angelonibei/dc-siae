package it.siae.valorizzatore.dao;

import it.siae.valorizzatore.model.EsecuzioneRicalcolo;
import it.siae.valorizzatore.model.ImportoRicalcolato;
import it.siae.valorizzatore.model.SospesiRicalcolo;
import it.siae.valorizzatore.model.TracciamentoApplicativo;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;

/**
 * Created by idilello on 6/9/16.
 */
@Repository("esecuzioneDAO")
public class EsecuzioneDAO implements IEsecuzioneDAO{

    @PersistenceContext(unitName = "VALORIZZATORE")
    private EntityManager entityManager;

    @Transactional(propagation=Propagation.REQUIRES_NEW)
    public Long startEsecuzioneRicalcolo(Long idEsecuzioneRicalcolo, Long contabilitaInizio, Long contabilitaFine, String voceIncasso) {

        EsecuzioneRicalcolo esecuzioneRicalcolo = new EsecuzioneRicalcolo();

        Date now = new Date();

        esecuzioneRicalcolo.setId(idEsecuzioneRicalcolo);
        esecuzioneRicalcolo.setDataOraInizioEsecuzione(now);
        esecuzioneRicalcolo.setDataOraUltimaModifica(now);
        esecuzioneRicalcolo.setContabilitaInizio(contabilitaInizio);
        esecuzioneRicalcolo.setContabilitaFine(contabilitaFine);
        esecuzioneRicalcolo.setVoceIncasso(voceIncasso);

        entityManager.merge(esecuzioneRicalcolo);
        entityManager.flush();

        return esecuzioneRicalcolo.getId();

    }



    @Transactional(propagation=Propagation.REQUIRES_NEW)
    public Long endEsecuzioneRicalcolo(Long idEsecuzioneRicalcolo, String esitoEsecuzione, String descrizioneEsitoEsecuzione) {

        EsecuzioneRicalcolo esecuzioneRicalcolo = (EsecuzioneRicalcolo)entityManager.find(EsecuzioneRicalcolo.class, idEsecuzioneRicalcolo);

        Date now = new Date();

        esecuzioneRicalcolo.setDataOraFineEsecuzione(now);
        esecuzioneRicalcolo.setDataOraUltimaModifica(now);
        esecuzioneRicalcolo.setEsitoEsecuzione(esitoEsecuzione);
        esecuzioneRicalcolo.setDescrizioneEsito(descrizioneEsitoEsecuzione);

        entityManager.merge(esecuzioneRicalcolo);
        entityManager.flush();

        return esecuzioneRicalcolo.getId();

    }

    @Transactional(propagation=Propagation.REQUIRES_NEW)
    public void trace(TracciamentoApplicativo tracciamentoApplicativo){
        entityManager.persist(tracciamentoApplicativo);
        entityManager.flush();
    }

    /*
    public Long insertSospesi(Long idEvento, Long numeroFattura, String voceIncasso, Long idProgrammaMusicale, String motivazioneSospensione, Long idEsecuzioneRicalcolo){

        SospesiRicalcolo sospesiRicalcolo = new SospesiRicalcolo();
        sospesiRicalcolo.setIdEvento(idEvento);
        sospesiRicalcolo.setIdEsecuzioneRicalcolo(idEsecuzioneRicalcolo);
        sospesiRicalcolo.setNumeroFattura(numeroFattura);
        sospesiRicalcolo.setVoceIncasso(voceIncasso);
        sospesiRicalcolo.setIdProgrammaMusicale(idProgrammaMusicale);

        entityManager.persist(sospesiRicalcolo);

        return sospesiRicalcolo.getId();
    }

    @Transactional(propagation=Propagation.REQUIRED)
    public Long insertImportoRicalcolato(Long idMovimentoContabile, Long idEvento, Long numeroFattura, String voceIncasso, Double importo, Long idEsecuzioneRicalcolo){

        ImportoRicalcolato importoRicalcolato = new ImportoRicalcolato();

        Date now = new Date();

        importoRicalcolato.setIdMovimentoContabile(idMovimentoContabile);
        importoRicalcolato.setIdEvento(idEvento);
        importoRicalcolato.setNumeroFattura(numeroFattura);
        importoRicalcolato.setVoceIncasso(voceIncasso);
        importoRicalcolato.setImporto(importo);
        importoRicalcolato.setIdEsecuzioneRicalcolo(idEsecuzioneRicalcolo);
        importoRicalcolato.setDataOraInserimento(now);

        entityManager.persist(importoRicalcolato);

        return importoRicalcolato.getId();
    }
    */

}
