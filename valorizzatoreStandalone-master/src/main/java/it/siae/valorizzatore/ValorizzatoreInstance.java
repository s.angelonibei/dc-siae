package it.siae.valorizzatore;

import it.siae.valorizzatore.model.InformazioniElaborazione;
import it.siae.valorizzatore.service.ITraceService;
import it.siae.valorizzatore.service.IValorizzatoreService;
import it.siae.valorizzatore.utility.Constants;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;


/**
 * Created by idilello on 6/6/16.
 */
@Component
public class ValorizzatoreInstance extends Thread{

    @Autowired
    @Qualifier("valorizzatoreService")
    private IValorizzatoreService valorizzatoreService;

    @Autowired
    @Qualifier("traceService")
    private ITraceService traceService;

    protected Logger log = Logger.getLogger(this.getClass());

    private int engineIndex;
    private Long elaborationId;
    private String voceIncasso=null;

    public ValorizzatoreInstance(int engineIndex, Long elaborationId){
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        AutowireCapableBeanFactory acbFactory = context.getAutowireCapableBeanFactory();
        acbFactory.autowireBean(this);

        this.engineIndex=engineIndex;
        this.elaborationId=elaborationId;
        this.voceIncasso=null;
    }

    public ValorizzatoreInstance(int engineIndex, Long elaborationId, String voceIncasso){
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        AutowireCapableBeanFactory acbFactory = context.getAutowireCapableBeanFactory();
        acbFactory.autowireBean(this);

        this.engineIndex=engineIndex;
        this.elaborationId=elaborationId;
        this.voceIncasso=voceIncasso;
    }

    public void run() {

        InformazioniElaborazione esito = null;

                log.info("ValorizzatoreInstance.run(). valorizzatoreService: "+valorizzatoreService);

        if (this.voceIncasso==null) {

            traceService.trace("ValorizzatoreInstance" + engineIndex, this.getClass().getName(), "run", "Attivato motore di ricalcolo", Constants.INFO);

            esito = valorizzatoreService.startElaborazioneRicalcolo(engineIndex, elaborationId);

        }else
        if (this.voceIncasso!=null && this.voceIncasso.equalsIgnoreCase(Constants.VOCE_2243)){

            traceService.trace("ValorizzatoreInstance2243" + engineIndex, this.getClass().getName(), "run", "Attivato motore di ricalcolo per voce 2243", Constants.INFO);

            esito = valorizzatoreService.startElaborazioneRicalcolo2243(engineIndex, elaborationId);

        }

        log.info("Esito: "+esito);
    }

    public InformazioniElaborazione getInformazioniElaborazione(){
        return valorizzatoreService.getInformazioniElaborazione();
    }


    public void suspendElaborazione(){
        valorizzatoreService.suspend();
    }

    public void resumeElaborazione(){
        valorizzatoreService.resume();
    }

    public void stopElaborazione(){
        valorizzatoreService.getInformazioniElaborazione().setTerminate(true);
    }

}
