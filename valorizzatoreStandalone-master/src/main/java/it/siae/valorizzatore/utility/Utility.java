package it.siae.valorizzatore.utility;

/**
 * Created by idilello on 7/20/16.
 */
public class Utility {

    public static double round(double value, int places){

        if (places < 0) throw new IllegalArgumentException();

        long factor = (long)Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double)tmp / factor;

    }

}
