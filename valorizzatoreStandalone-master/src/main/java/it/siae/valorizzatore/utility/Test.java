package it.siae.valorizzatore.utility;

/**
 * Created by idilello on 7/6/16.
 */
public class Test {

    public static void main(String[] args){

        //Long r = 14L % 5;

        //Double r = 12.463784632443443;

        //System.out.println(r);

        //System.out.println(Utility.round(r,7));


        int numeroEngine = 4;
        Long numeroMovimenti = 3L;

        String[] intervalliEngine = new String[numeroEngine];
        long min=0;
        long max=0;
        long r = numeroMovimenti / numeroEngine;

        for (int i=0; i<numeroEngine; i++){
             min = (r*i)+1;
             max = (r*(i+1));
             // Assegna eventuali movimenti residue all'ultimo engine
             if (i==(numeroEngine-1)){
                 max=numeroMovimenti;
             }
             intervalliEngine[i]=min+"-"+max;

        }

        System.out.println("r="+r);
        for (int i=0; i<intervalliEngine.length; i++){
            System.out.println("Engine("+(i+1)+")="+intervalliEngine[i]);
        }

    }

}
