package it.siae.valorizzatore.utility;

/**
 * Created by idilello on 6/13/16.
 */
import java.io.*;
import java.net.Socket;
import java.util.Date;


/**
 * Trivial client for the date server.
 */
public class DateClient {

    /**
     * Runs the client as an application.  First it displays a dialog
     * box asking for the IP address or hostname of a host running
     * the date server, then connects to it and displays the date that
     * it serves.
     */
    public static void main(String[] args) throws IOException {

        String serverAddress = "127.0.0.1";

        try{

            Socket s = new Socket(serverAddress, 9090);

            PrintWriter out = new PrintWriter(s.getOutputStream(), true);
            out.println("STATUS");

            BufferedReader input = new BufferedReader(new InputStreamReader(s.getInputStream()));
            String answer = input.readLine();

            System.out.println("R: "+answer);

        }catch(java.net.ConnectException cr){

        }

    }
}