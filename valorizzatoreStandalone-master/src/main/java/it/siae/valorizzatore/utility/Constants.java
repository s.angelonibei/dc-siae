package it.siae.valorizzatore.utility;

/**
 * Created by idilello on 6/9/16.
 */
public class Constants {

    public static final String VOCE_2267 = "2267";
    public static final String VOCE_2244 = "2244";
    public static final String VOCE_2243 = "2243";

    // Voce Incasso BSM
    public static final String VOCE_BSM = "2223";
    // Voce incasso concertini
    public static final String TIPO_VOCE_CONCERTINO="B";

    // Quote Gruppo Principale e Gruppo Spalla (Eventi fino a 01/07/2016)
    public static final Double QUOTA_GRUPPO_PRINCIPALE1 = 0.85D;
    public static final Double QUOTA_GRUPPO_SPALLA1 = 0.15D;

    public static Integer DATA1_2=201607;

    // Quote Gruppo Principale e Gruppo Spalla (Eventi dopo 01/07/2016)
    public static final Double QUOTA_GRUPPO_PRINCIPALE2 = 0.95D;
    public static final Double QUOTA_GRUPPO_SPALLA2 = 0.05D;
    public static final Double QUOTA_CAP = 1000D;


    // Motivazioni di sospensione (non ricalcolo importo) per PM
    public static final String RIPARTITO_PERIODO_PRECEDENTE = "RIPARTITO_PERIODO_PRECEDENTE";
    public static final String IMPORTO_MODIFICATO_MANUALMENTE = "IMPORTO_MODIFICATO_MANUALMENTE";
    public static final String VOCE_INCASSO_2267 = "VOCE_INCASSO_2267";
    public static final String EVENTO_IMPORTO_INESISTENTE = "EVENTO_IMPORTO_INESISTENTE";
    public static final String EVENTO_VOCE_UNICO_PM_ATTESO = "EVENTO_VOCE_UNICO_PM_ATTESO";
    public static final String ERRORE= "ERRORE";
    public static final String AGGREGATE_INTERVAL="AGGREGATE_INTERVAL";
    public static final String AGGREGATE_INTERVAL_2243="AGGREGATE_INTERVAL_2243";

    // Errori durante l'esecuzione
    // Esecuzione corretta
    public static final String SUCCESS_ERROR_CODE="0";
    public static final String SUCCESS_ERROR_DESCR="Success";
    // Periodo di ricalcolo errato
    public static final String PERIODO_RICALCOLO_ERRATO_ERROR_CODE="-1";
    public static final String PERIODO_RICALCOLO_ERRATO_ERROR_DESCR="L'inizio del periodo di ricalcolo deve essere successivo alla fine dell'ultimo periodo di ripartizione";
    // Errore generico
    public static final String ECCEZIONE_ERROR_CODE="-100";

    public static final String ALL_TIPOLOGIE_PM="Tutte";

    // Comandi interrogazione processi di backend di calcolo
    public static final String STATUS="STATUS";
    public static final String DEACTIVATE="DEACTIVATE";
    public static final String ACTIVATE="ACTIVATE";
    public static final String STATUS_2243="STATUS_2243";
    public static final String DEACTIVATE_2243="DEACTIVATE_2243";
    public static final String ACTIVATE_2243="ACTIVATE_2243";

    // Comandi per Valorizzazione
    public static final String START_RICALCOLO="START_RICALCOLO";
    public static final String INFORMAZIONI_ELABORAZIONE="INFORMAZIONI_ELABORAZIONE";

    public static final String START_RICALCOLO_2243="RICALCOLO_START_2243";
    public static final String INFORMAZIONI_ELABORAZIONE_2243="INFORMAZIONI_ELABORAZIONE_2243";


    public static final String INFORMAZIONI_ELABORAZIONE_CAMPIONAMENTO="INFORMAZIONI_ELABORAZIONE_CAMPIONAMENTO";

    public static final String SUSPEND_RICALCOLO="SUSPEND_RICALCOLO";
    public static final String RESUME_RICALCOLO="RESUME_RICALCOLO";

    // Comandi per Campionamento
    public static final String START_CAMPIONAMENTO="START_CAMPIONAMENTO";
    public static final String SUSPEND_CAMPIONAMENTO="SUSPEND_CAMPIONAMENTO";
    public static final String RESUME_CAMPIONAMENTO="RESUME_CAMPIONAMENTO";

    public static final String START_AGGREGAZIONE="START_AGGREGAZIONE";
    public static final String INFORMAZIONI_ELABORAZIONE_AGGREGAZIONE="INFORMAZIONI_ELABORAZIONE_AGGREGAZIONE";

    public static final String START_AGGREGAZIONE_2243="AGGREGAZIONE_START_2243";
    public static final String INFORMAZIONI_ELABORAZIONE_AGGREGAZIONE_2243="INFORMAZIONI_ELABORAZIONE_AGGREGAZIONE_2243";

    // Parametri per il trace applicativo
    public static final String APPID="VALORIZZATORE_ENGINE";
    public static final String ERROR="ERROR";
    public static final String INFO="INFO";
    public static final String WARNING="WARNING";

}
