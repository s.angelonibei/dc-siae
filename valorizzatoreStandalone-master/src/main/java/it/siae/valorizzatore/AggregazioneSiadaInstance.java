
package it.siae.valorizzatore;

import it.siae.valorizzatore.model.InformazioniElaborazione;
import it.siae.valorizzatore.service.IGestioneSiadaService;
import it.siae.valorizzatore.utility.Constants;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;


/**
 * Created by idilello on 6/6/16.
 */
@Component
public class AggregazioneSiadaInstance extends Thread{

    @Autowired
    @Qualifier("gestioneSiadaService")
    private IGestioneSiadaService gestioneSiadaService;

    protected Logger log = Logger.getLogger(this.getClass());

    private int engineIndex;
    private Long elaborationId;
    private String voceIncasso=null;

    public AggregazioneSiadaInstance(int engineIndex, Long elaborationId){
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        AutowireCapableBeanFactory acbFactory = context.getAutowireCapableBeanFactory();
        acbFactory.autowireBean(this);

        this.engineIndex=engineIndex;
        this.elaborationId=elaborationId;
        this.voceIncasso=null;
    }

    public AggregazioneSiadaInstance(int engineIndex, Long elaborationId, String voceIncasso){
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        AutowireCapableBeanFactory acbFactory = context.getAutowireCapableBeanFactory();
        acbFactory.autowireBean(this);

        this.engineIndex=engineIndex;
        this.elaborationId=elaborationId;
        this.voceIncasso=voceIncasso;
    }

    public AggregazioneSiadaInstance(){
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        AutowireCapableBeanFactory acbFactory = context.getAutowireCapableBeanFactory();
        acbFactory.autowireBean(this);
    }

    public void run() {
        if (this.voceIncasso==null) {
            log.info("AggregazioneSiadaInstance.run(). gestioneSiadaService: " + gestioneSiadaService);
            InformazioniElaborazione esito = gestioneSiadaService.startAggregazione(engineIndex, elaborationId);
            log.info("Esito: " + esito);
        }else
        if (this.voceIncasso!=null && this.voceIncasso.equalsIgnoreCase(Constants.VOCE_2243)){
            log.info("AggregazioneSiadaInstance.run(). gestioneSiadaService: " + gestioneSiadaService);
            InformazioniElaborazione esito = gestioneSiadaService.startAggregazione2243(engineIndex, elaborationId);
            log.info("Esito: " + esito);
        }
    }

    public InformazioniElaborazione getInformazioniElaborazione(){
        return gestioneSiadaService.getInformazioniElaborazione();
    }

}
