
package it.siae.valorizzatore;

import it.siae.valorizzatore.model.InformazioniElaborazione;
import it.siae.valorizzatore.service.ICampionamentoService;
import it.siae.valorizzatore.service.ITraceService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;


/**
 * Created by idilello on 6/6/16.
 */
@Component
public class CampionamentoInstance extends Thread{

    @Autowired
    @Qualifier("campionamentoService")
    private ICampionamentoService campionamentoService;


    @Autowired
    private ITraceService traceService;

    protected Logger log = Logger.getLogger(this.getClass());

    public CampionamentoInstance(){
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        AutowireCapableBeanFactory acbFactory = context.getAutowireCapableBeanFactory();
        acbFactory.autowireBean(this);
    }

    public void run() {
        log.info("CampionamentoInstance.run(). campionamentoService: "+campionamentoService);
        InformazioniElaborazione esito = campionamentoService.startCampionamento();
        log.info("Esito: "+esito);
    }

    public InformazioniElaborazione getInformazioniElaborazione(){
        return campionamentoService.getInformazioniElaborazione();
    }

}
