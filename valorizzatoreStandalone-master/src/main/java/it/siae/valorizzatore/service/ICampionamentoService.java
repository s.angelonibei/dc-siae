package it.siae.valorizzatore.service;

import it.siae.valorizzatore.model.InformazioniElaborazione;

/**
 * Created by idilello on 7/5/16.
 */
public interface ICampionamentoService {

    public InformazioniElaborazione startCampionamento();

    public InformazioniElaborazione getInformazioniElaborazione();
}
