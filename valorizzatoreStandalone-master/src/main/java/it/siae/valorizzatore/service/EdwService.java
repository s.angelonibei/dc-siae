package it.siae.valorizzatore.service;

import it.siae.valorizzatore.dao.IEdwDAO;
import it.siae.valorizzatore.dao.IProgrammaMusicaleDAO;
import it.siae.valorizzatore.model.TrattamentoPM;
import it.siae.valorizzatore.model.dwh.EDWRipartizione;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * Created by idilello on 6/8/16.
 */
@Service("edwService")
public class EdwService implements IEdwService{

    @Autowired
    @Qualifier("edwDAO")
    IEdwDAO edwDAO;

    public EDWRipartizione getUltimaRipartizione(){

        return edwDAO.getUltimaRipartizione();
    }

}
