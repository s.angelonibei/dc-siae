package it.siae.valorizzatore.service;

import it.siae.valorizzatore.dao.IConfigurationDAO;
import it.siae.valorizzatore.dao.IProgrammaMusicaleDAO;
import it.siae.valorizzatore.model.CampionamentoConfig;
import it.siae.valorizzatore.model.InformazioniElaborazione;
import it.siae.valorizzatore.model.MovimentoContabile;
import it.siae.valorizzatore.model.ProgrammaMusicale;
import it.siae.valorizzatore.utility.Constants;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

/**
 * Created by idilello on 7/5/16.
 */
@Service("campionamentoService")
public class CampionamentoService implements ICampionamentoService{

    @Autowired
    @Qualifier("configurationDAO")
    IConfigurationDAO configurationDAO;

    @Autowired
    @Qualifier("programmaMusicaleDAO")
    IProgrammaMusicaleDAO programmaMusicaleDAO;


    @Autowired
    @Qualifier("traceService")
    private ITraceService traceService;


    public InformazioniElaborazione informazioniElaborazione;

    protected Logger log = Logger.getLogger(this.getClass());

    @Transactional
    public InformazioniElaborazione startCampionamento(){

        InformazioniElaborazione informazioniElaborazione = new InformazioniElaborazione();

        String periodoContabileInizio = configurationDAO.getParameter("INIZIO_PERIODO_CAMPIONAMENTO");
        String periodoContabileFine = configurationDAO.getParameter("FINE_PERIODO_CAMPIONAMENTO");

        String campionaDigitali = configurationDAO.getParameter("CAMPIONAMENTO_DIGITALI");

        CampionamentoConfig parametriCampionamento = configurationDAO.getCampionamentoConfig(Long.parseLong(periodoContabileInizio));

        if (parametriCampionamento!=null) {

         try{

            Long restoMod5Bsm = parametriCampionamento.getRestoRoma1();
            Long restoMod61Bsm = parametriCampionamento.getRestoRoma2();

            Long restoMod5Concertini = parametriCampionamento.getRestoMilano1();
            Long restoMod61Concertini = parametriCampionamento.getRestoMilano2();

            List<MovimentoContabile> listaProgrammiMusicaliBSM = programmaMusicaleDAO.getProgrammiMusicaliBSM(Long.parseLong(periodoContabileInizio), Long.parseLong(periodoContabileFine), null);

            List<MovimentoContabile> listaProgrammiMusicaliConcertini = programmaMusicaleDAO.getProgrammiMusicaliConcertino(Long.parseLong(periodoContabileInizio), Long.parseLong(periodoContabileFine), null);

            getInformazioniElaborazione().setTotalePM(new Long(listaProgrammiMusicaliBSM.size() + listaProgrammiMusicaliConcertini.size()));

            traceService.trace("CampionamentoInstance", this.getClass().getName(), "startCampionamento", "Periodo contabile da: " + periodoContabileInizio + " a: " + periodoContabileFine + " per PM-BSM: " + listaProgrammiMusicaliBSM.size() + " PM Concertini: " + listaProgrammiMusicaliConcertini.size(), Constants.INFO);

            ProgrammaMusicale programmaMusicale = null;
            Long codiceCampionamento;
            Long r;
            int cont = 0;

            // Campiona i PM BSM
            for (MovimentoContabile movimento : listaProgrammiMusicaliBSM) {
                programmaMusicale = programmaMusicaleDAO.getProgrammaMusicale(movimento.getIdProgrammaMusicale());
                codiceCampionamento = programmaMusicale.getCodiceCampionamento();

                cont++;
                getInformazioniElaborazione().setTotaleElaborati(new Long(cont));

                if ((!programmaMusicale.getFoglio().equalsIgnoreCase("DI")) ||  // PM Cartacei
                        (programmaMusicale.getFoglio().equalsIgnoreCase("DI") && campionaDigitali.equalsIgnoreCase("Y")) &&   // PM Digitali da campionare
                                (programmaMusicale.getPmRiferimento()==null) &&  // Il PM non è un foglio segue di ...
                                (programmaMusicale.getFlagDaCampionare() == null)) {   // PM ancora non analizzato per campionamento
                    r = codiceCampionamento % 5;
                    if (r == restoMod5Bsm) {
                        programmaMusicale.setRisultatoCalcoloResto(r);
                        programmaMusicale.setFlagDaCampionare('Y');
                    } else {
                        r = codiceCampionamento % 61;
                        if (r == restoMod61Bsm) {
                            programmaMusicale.setRisultatoCalcoloResto(r);
                            programmaMusicale.setFlagDaCampionare('Y');
                        } else {
                            programmaMusicale.setFlagDaCampionare('N');
                        }
                    }
                    programmaMusicaleDAO.updateProgrammaMusicale(programmaMusicale);
                } else
                if (programmaMusicale.getFoglio().equalsIgnoreCase("DI") && campionaDigitali.equalsIgnoreCase("N")){
                    programmaMusicale.setFlagDaCampionare('N');
                    programmaMusicaleDAO.updateProgrammaMusicale(programmaMusicale);
                }
            }

            traceService.trace("CampionamentoInstance", this.getClass().getName(), "startCampionamento", "Elaborati "+listaProgrammiMusicaliBSM.size() + " PM BSM ", Constants.INFO);

            // Campiona i PM Concertini
            for (MovimentoContabile movimento : listaProgrammiMusicaliConcertini) {
                programmaMusicale = programmaMusicaleDAO.getProgrammaMusicale(movimento.getIdProgrammaMusicale());
                codiceCampionamento = programmaMusicale.getCodiceCampionamento();

                cont++;
                getInformazioniElaborazione().setTotaleElaborati(new Long(cont));

                if ((!programmaMusicale.getFoglio().equalsIgnoreCase("DI")) ||  // PM Cartacei
                        (programmaMusicale.getFoglio().equalsIgnoreCase("DI") && campionaDigitali.equalsIgnoreCase("Y")) &&   // PM Digitali da campionare
                                (programmaMusicale.getPmRiferimento()==null) &&  // Il PM non è un foglio segue di ...
                                (programmaMusicale.getFlagDaCampionare() == null)) {   // PM ancora non analizzato per campionamento
                    r = codiceCampionamento % 5;
                    if (r == restoMod5Concertini) {
                        programmaMusicale.setRisultatoCalcoloResto(r);
                        programmaMusicale.setFlagDaCampionare('Y');
                    } else {
                        r = codiceCampionamento % 61;
                        if (r == restoMod61Concertini) {
                            programmaMusicale.setRisultatoCalcoloResto(r);
                            programmaMusicale.setFlagDaCampionare('Y');
                        } else {
                            programmaMusicale.setFlagDaCampionare('N');
                        }
                    }

                    programmaMusicaleDAO.updateProgrammaMusicale(programmaMusicale);
                }else
                if (programmaMusicale.getFoglio().equalsIgnoreCase("DI") && campionaDigitali.equalsIgnoreCase("N")){
                    programmaMusicale.setFlagDaCampionare('N');
                    programmaMusicaleDAO.updateProgrammaMusicale(programmaMusicale);
                }
            }

            traceService.trace("CampionamentoInstance", this.getClass().getName(), "startCampionamento", "Elaborati "+listaProgrammiMusicaliConcertini.size() + " PM Concertini ", Constants.INFO);

            informazioniElaborazione.setTotalePM(new Long(cont));

            getInformazioniElaborazione().setTerminate(true);

         }catch(Exception e){

             StringWriter sw = new StringWriter();
             PrintWriter pw = new PrintWriter(sw);
             e.printStackTrace(pw);

             traceService.trace("CampionamentoInstance", this.getClass().getName(), "startCampionamento", sw.toString(), Constants.ERROR);

         }

        }else{
            traceService.trace("CampionamentoInstance", this.getClass().getName(), "startCampionamento", "Parameti per il periodo contabile: " + periodoContabileInizio + " a: " + periodoContabileFine + " non configurati", Constants.INFO);
        }

        log.info("Campionamento terminato. Elaborati "+getInformazioniElaborazione().getTotaleElaborati()+" su "+getInformazioniElaborazione().getTotalePM());

        traceService.trace("CampionamentoInstance", this.getClass().getName(), "startCampionamento", "Campionamento terminato", Constants.INFO);

        return informazioniElaborazione;
    }

    public InformazioniElaborazione getInformazioniElaborazione(){
        if(informazioniElaborazione == null) {
            informazioniElaborazione = new InformazioniElaborazione();
        }
        return informazioniElaborazione;
    }


}
