package it.siae.valorizzatore.service;

/**
 * Created by idilello on 8/1/16.
 */
public interface ITraceService {

    public void trace(String serviceName, String className, String methodName, String message, String logLevel);

}
