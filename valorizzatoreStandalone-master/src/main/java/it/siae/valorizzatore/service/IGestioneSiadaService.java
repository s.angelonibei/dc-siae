package it.siae.valorizzatore.service;

import it.siae.valorizzatore.model.InformazioniElaborazione;
import it.siae.valorizzatore.model.MovimentoSiada;

import java.util.Date;
import java.util.List;

/**
 * Created by idilello on 8/4/16.
 */
public interface IGestioneSiadaService {

    public List<MovimentoSiada> getMovimentoSiada(Long idEvento, String voceIncasso, Integer fineUltimaRipartizione);

    public InformazioniElaborazione startAggregazione(int engineIndex, Long elaborationId);

    public InformazioniElaborazione startAggregazione2243(int engineIndex, Long elaborationId);

    public InformazioniElaborazione getInformazioniElaborazione();

}
