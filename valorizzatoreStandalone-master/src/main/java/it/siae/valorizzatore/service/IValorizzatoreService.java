package it.siae.valorizzatore.service;

import it.siae.valorizzatore.model.InformazioniElaborazione;
import it.siae.valorizzatore.model.MovimentoContabile;
import it.siae.valorizzatore.model.TrattamentoPM;

import java.util.List;

/**
 * Created by idilello on 6/6/16.
 */
public interface IValorizzatoreService {

    //public List<MovimentoContabile> getMovimentiContabili(Long periodoContabileInizio, Long periodoContabileFine, String tipologiaPm);

    public InformazioniElaborazione startElaborazioneRicalcolo(int engineIndex, Long idElebaorazione);

    public InformazioniElaborazione startElaborazioneRicalcolo2243(int engineIndex, Long idElebaorazione);

    public InformazioniElaborazione getInformazioniElaborazione();

    public void suspend();

    public void resume();

}

