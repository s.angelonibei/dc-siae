package it.siae.valorizzatore.service;

import it.siae.valorizzatore.model.dwh.EDWRipartizione;

/**
 * Created by idilello on 6/8/16.
 */
public interface IEdwService {

    public EDWRipartizione getUltimaRipartizione();
}
