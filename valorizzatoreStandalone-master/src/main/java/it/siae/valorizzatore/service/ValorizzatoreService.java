//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package it.siae.valorizzatore.service;

import it.siae.valorizzatore.dao.IConfigurationDAO;
import it.siae.valorizzatore.dao.IEdwDAO;
import it.siae.valorizzatore.dao.IEsecuzioneDAO;
import it.siae.valorizzatore.dao.IGestioneSiadaDAO;
import it.siae.valorizzatore.dao.IProgrammaMusicaleDAO;
import it.siae.valorizzatore.model.EventiPagati;
import it.siae.valorizzatore.model.InformazioniElaborazione;
import it.siae.valorizzatore.model.MovimentoContabile;
import it.siae.valorizzatore.model.MovimentoSiada;
import it.siae.valorizzatore.model.TrattamentoPM;
import it.siae.valorizzatore.model.domain.ChiaveElaborazione;
import it.siae.valorizzatore.model.domain.Elaborazione;
import it.siae.valorizzatore.model.domain.ProgrammaRientrato;
import it.siae.valorizzatore.model.domain.RigaElaborata;
import it.siae.valorizzatore.utility.Constants;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("valorizzatoreService")
public class ValorizzatoreService implements IValorizzatoreService {
    @Autowired
    @Qualifier("programmaMusicaleDAO")
    IProgrammaMusicaleDAO programmaMusicaleDAO;
    @Autowired
    @Qualifier("edwDAO")
    IEdwDAO edwDAO;
    @Autowired
    @Qualifier("configurationDAO")
    IConfigurationDAO configurationDAO;
    @Autowired
    @Qualifier("esecuzioneDAO")
    IEsecuzioneDAO esecuzioneDAO;
    @Autowired
    @Qualifier("gestioneSiadaDAO")
    IGestioneSiadaDAO gestioneSiadaDAO;
    @Autowired
    @Qualifier("traceService")
    private ITraceService traceService;
    public InformazioniElaborazione informazioniElaborazione;
    protected Logger log = Logger.getLogger(this.getClass());

    public ValorizzatoreService() {
    }

    private List<TrattamentoPM> getTrattamentoPM() {
        return this.programmaMusicaleDAO.getTrattamentoPM();
    }

    private List<MovimentoContabile> getMovimentiContabili(Long periodoContabileInizio, Long periodoContabileFine, String tipologiaPm, int minInterval, int maxInterval) throws Exception {
        return this.programmaMusicaleDAO.getMovimentiContabili(periodoContabileInizio, periodoContabileFine, tipologiaPm, minInterval, maxInterval);
    }

    private List<MovimentoContabile> getMovimentiContabili2243(Long periodoContabileInizio, Long periodoContabileFine, String tipologiaPm, int minInterval, int maxInterval) throws Exception {
        return this.programmaMusicaleDAO.getMovimentiContabili2243(periodoContabileInizio, periodoContabileFine, tipologiaPm, minInterval, maxInterval);
    }

    private void eseguiRicalcoloProQuota(RigaElaborata rigaElaborata) {
        if (rigaElaborata.getVoceIncasso().equalsIgnoreCase("2244")) {
           // int numeroFatture = false;
            int rientratiPrincipale = 0;
            int rientratiSpalla = 0;
            Long numeroCedoleTotaliPrincipale = 0L;
            Long numeroCedoleTotaliSpalla = 0L;
            Long maxCedolePrincipale = 0L;
            Long maxCedoleSpalla = 0L;
            boolean eccezioneRegolaAA_AB = false;
            Collection<ProgrammaRientrato> programmiRientrati = rigaElaborata.getListaPmRientratiEventoVoce().values();
            this.log.debug("Collection Programmi rientrati: " + Arrays.toString(programmiRientrati.toArray()));
            Iterator<ProgrammaRientrato> programmiRientratiList = programmiRientrati.iterator();
            Long numeroPMAttesiPrincipale = rigaElaborata.getNumeroPmAttesiEventoVoce();
            Long numeroPMAttesiSpalla = rigaElaborata.getNumeroPmAttesiEventoVoceSpalla();
            this.log.debug("Numero pm attesi principali: " + numeroPMAttesiPrincipale);
            this.log.debug("Numero pm attesi spalla: " + numeroPMAttesiSpalla);

            while(true) {
                ProgrammaRientrato programmaRientrato;
                while(programmiRientratiList.hasNext()) {
                    this.log.debug("Entrato nel while programmiRientratiList");
                    programmaRientrato = (ProgrammaRientrato)programmiRientratiList.next();
                    if (programmaRientrato.getFlagPMPrincipale() != null && programmaRientrato.getFlagPMPrincipale() == 'Y') {
                        ++rientratiPrincipale;
                        numeroCedoleTotaliPrincipale = numeroCedoleTotaliPrincipale + programmaRientrato.getCedole();
                        if (programmaRientrato.getCedole() > maxCedolePrincipale) {
                            maxCedolePrincipale = programmaRientrato.getCedole();
                        }

                        this.log.debug("Entrato if principale. Importo: " + rigaElaborata.getValoreProQuota() + " " + programmaRientrato.getCedole());
                    } else {
                        ++rientratiSpalla;
                        numeroCedoleTotaliSpalla = numeroCedoleTotaliSpalla + programmaRientrato.getCedole();
                        if (programmaRientrato.getCedole() > maxCedoleSpalla) {
                            maxCedoleSpalla = programmaRientrato.getCedole();
                        }

                        this.log.debug("Entrato if spalla. Importo: " + rigaElaborata.getValoreProQuota() + " " + programmaRientrato.getCedole());
                    }
                }

                if (numeroPMAttesiPrincipale != 0L && numeroPMAttesiSpalla != 0L) {
                    if (numeroPMAttesiPrincipale != 0L && numeroPMAttesiSpalla != 0L) {
                        if ((long)rientratiPrincipale > numeroPMAttesiPrincipale) {
                            rigaElaborata.setNumeroPmAttesiEventoVoce(new Long((long)rientratiPrincipale));
                            if (numeroPMAttesiSpalla - (long)rientratiPrincipale > 0L) {
                                rigaElaborata.setNumeroPmAttesiEventoVoceSpalla(new Long(numeroPMAttesiSpalla - (long)rientratiPrincipale));
                            } else {
                                rigaElaborata.setNumeroPmAttesiEventoVoceSpalla(new Long(0L));
                            }
                        }

                        if ((long)rientratiSpalla > numeroPMAttesiSpalla) {
                            rigaElaborata.setNumeroPmAttesiEventoVoceSpalla(new Long((long)rientratiSpalla));
                            if (numeroPMAttesiPrincipale - (long)rientratiSpalla > 0L) {
                                rigaElaborata.setNumeroPmAttesiEventoVoce(new Long(numeroPMAttesiPrincipale - (long)rientratiSpalla));
                            } else {
                                rigaElaborata.setNumeroPmAttesiEventoVoce(new Long(0L));
                            }
                        }
                    }
                } else if (numeroPMAttesiPrincipale == 0L && rientratiPrincipale > 0) {
                    rigaElaborata.setNumeroPmAttesiEventoVoce(new Long((long)rientratiPrincipale));
                    if (numeroPMAttesiSpalla - (long)rientratiPrincipale > 0L) {
                        rigaElaborata.setNumeroPmAttesiEventoVoceSpalla(new Long(numeroPMAttesiSpalla - (long)rientratiPrincipale));
                    } else {
                        rigaElaborata.setNumeroPmAttesiEventoVoceSpalla(new Long(0L));
                    }
                } else if (numeroPMAttesiSpalla == 0L && rientratiSpalla > 0) {
                    rigaElaborata.setNumeroPmAttesiEventoVoceSpalla(new Long((long)rientratiSpalla));
                    if (numeroPMAttesiPrincipale - (long)rientratiSpalla > 0L) {
                        rigaElaborata.setNumeroPmAttesiEventoVoce(new Long(numeroPMAttesiPrincipale - (long)rientratiSpalla));
                    } else {
                        rigaElaborata.setNumeroPmAttesiEventoVoce(new Long(0L));
                    }
                }

                this.log.debug("rigaElaborata.getListaPmRientratiEventoVoce().size(): " + rigaElaborata.getListaPmRientratiEventoVoce().size());
                this.log.debug("rigaElaborata.rigaElaborata.getNumeroPmAttesiEventoVoce(): " + rigaElaborata.getNumeroPmAttesiEventoVoce());
                this.log.debug("rigaElaborata.getNumeroPmAttesiEventoVoceSpalla(): " + rigaElaborata.getNumeroPmAttesiEventoVoceSpalla());
                if ((long)rigaElaborata.getListaPmRientratiEventoVoce().size() == rigaElaborata.getNumeroPmAttesiEventoVoce() + rigaElaborata.getNumeroPmAttesiEventoVoceSpalla()) {
                    this.log.debug("rigaElaborata.getNumeroPmAttesiEventoVoce() " + rigaElaborata.getNumeroPmAttesiEventoVoce());
                    this.log.debug("rientratiPrincipale " + rientratiPrincipale);
                    this.log.debug("rigaElaborata.getNumeroPmAttesiEventoVoceSpalla() " + rigaElaborata.getNumeroPmAttesiEventoVoceSpalla());
                    this.log.debug("rientratiSpalla " + rientratiSpalla);
                    if (rigaElaborata.getNumeroPmAttesiEventoVoce() != (long)rientratiPrincipale || rigaElaborata.getNumeroPmAttesiEventoVoceSpalla() != (long)rientratiSpalla) {
                        rigaElaborata.setNumeroPmAttesiEventoVoce(new Long((long)rientratiPrincipale));
                        rigaElaborata.setNumeroPmAttesiEventoVoceSpalla(new Long((long)rientratiSpalla));
                    }
                }

                Double importoTotaleEventoVoce = rigaElaborata.getImportoTotEventoFatturaVoce();
                Double importoPrincipaleEventoVoce = 0.0;
                Double importoSpallaEventoVoce = 0.0;
                if (rigaElaborata.getContabilitaPagamentoEvento() < Constants.DATA1_2) {
                    if (rigaElaborata.getNumeroPmAttesiEventoVoce() > 0L && rigaElaborata.getNumeroPmAttesiEventoVoceSpalla() > 0L) {
                        importoPrincipaleEventoVoce = importoTotaleEventoVoce * Constants.QUOTA_GRUPPO_PRINCIPALE1;
                        importoSpallaEventoVoce = importoTotaleEventoVoce * Constants.QUOTA_GRUPPO_SPALLA1;
                    } else if (rigaElaborata.getNumeroPmAttesiEventoVoce() > 0L && rigaElaborata.getNumeroPmAttesiEventoVoceSpalla() == 0L) {
                        importoPrincipaleEventoVoce = importoTotaleEventoVoce;
                    } else if (rigaElaborata.getNumeroPmAttesiEventoVoce() == 0L && rigaElaborata.getNumeroPmAttesiEventoVoceSpalla() > 0L) {
                        importoSpallaEventoVoce = importoTotaleEventoVoce;
                    }
                } else if (rigaElaborata.getNumeroPmAttesiEventoVoce() > 0L && rigaElaborata.getNumeroPmAttesiEventoVoceSpalla() > 0L || rientratiPrincipale > 0 && rientratiSpalla > 0) {
                    importoPrincipaleEventoVoce = importoTotaleEventoVoce * Constants.QUOTA_GRUPPO_PRINCIPALE2;
                    this.log.debug("Importo principale evento voce: " + importoPrincipaleEventoVoce);
                    importoSpallaEventoVoce = importoTotaleEventoVoce * Constants.QUOTA_GRUPPO_SPALLA2;
                    this.log.debug("Importo spalla evento voce: " + importoSpallaEventoVoce);
                    if (importoSpallaEventoVoce > Constants.QUOTA_CAP) {
                        importoSpallaEventoVoce = Constants.QUOTA_CAP;
                        importoPrincipaleEventoVoce = importoTotaleEventoVoce - Constants.QUOTA_CAP;
                    }
                } else if (rigaElaborata.getNumeroPmAttesiEventoVoce() > 0L && rigaElaborata.getNumeroPmAttesiEventoVoceSpalla() == 0L) {
                    importoPrincipaleEventoVoce = importoTotaleEventoVoce;
                } else if (rigaElaborata.getNumeroPmAttesiEventoVoce() == 0L && rigaElaborata.getNumeroPmAttesiEventoVoceSpalla() > 0L) {
                    importoSpallaEventoVoce = importoTotaleEventoVoce;
                    this.log.debug("Sono nella regola AA o AB dove il principale atteso = 1 e spalla atteso = 0 e rientrati solo 1-n caso spalla");
                    eccezioneRegolaAA_AB = true;
                }

                if (numeroCedoleTotaliSpalla == 0L) {
                    importoPrincipaleEventoVoce = importoPrincipaleEventoVoce + importoSpallaEventoVoce;
                }

                rigaElaborata.setImportoTotPrincipale(importoPrincipaleEventoVoce);
                rigaElaborata.setImportoTotSpalla(importoSpallaEventoVoce);
                this.log.debug("setImportoTotPrincipale: " + importoPrincipaleEventoVoce);
                this.log.debug("setImportoTotSpalla: " + importoSpallaEventoVoce);
                if (programmiRientrati.size() > 0) {
                    Double valoreProQuotaPrincipale = 0.0;
                    Double valoreProQuotaSpalla = 0.0;
                    numeroCedoleTotaliPrincipale = numeroCedoleTotaliPrincipale + (rigaElaborata.getNumeroPmAttesiEventoVoce() - (long)rientratiPrincipale) * maxCedolePrincipale;
                    numeroCedoleTotaliSpalla = numeroCedoleTotaliSpalla + (rigaElaborata.getNumeroPmAttesiEventoVoceSpalla() - (long)rientratiSpalla) * maxCedoleSpalla;
                    if (rigaElaborata.getImportoTotPrincipale() != 0.0 && numeroCedoleTotaliPrincipale != 0L) {
                        valoreProQuotaPrincipale = rigaElaborata.getImportoTotPrincipale() / (double)numeroCedoleTotaliPrincipale;
                        rigaElaborata.setValoreProQuota(valoreProQuotaPrincipale);
                        rigaElaborata.setNumeroCedolePmAttesi(numeroCedoleTotaliPrincipale);
                    } else {
                        rigaElaborata.setValoreProQuota(0.0);
                        rigaElaborata.setNumeroCedolePmAttesi(0L);
                    }

                    if (rigaElaborata.getImportoTotSpalla() != 0.0 && numeroCedoleTotaliSpalla != 0L) {
                        valoreProQuotaSpalla = rigaElaborata.getImportoTotSpalla() / (double)numeroCedoleTotaliSpalla;
                        rigaElaborata.setValoreProQuotaSpalla(valoreProQuotaSpalla);
                        rigaElaborata.setNumeroCedolePmAttesiSpalla(numeroCedoleTotaliSpalla);
                    } else {
                        rigaElaborata.setValoreProQuotaSpalla(0.0);
                        rigaElaborata.setNumeroCedolePmAttesiSpalla(0L);
                    }

                    if ((long)rigaElaborata.getListaPmRientratiEventoVoce().size() == rigaElaborata.getNumeroPmAttesiEventoVoce() + rigaElaborata.getNumeroPmAttesiEventoVoceSpalla()) {
                        this.log.debug("Valore proquota principale double: " + valoreProQuotaPrincipale);
                        this.log.debug("Valore proquota spalla double: " + valoreProQuotaSpalla);
                        this.log.debug("eccezioneRegolaAA_AB: " + eccezioneRegolaAA_AB);
                        if (valoreProQuotaPrincipale > valoreProQuotaSpalla || eccezioneRegolaAA_AB && valoreProQuotaSpalla > 0.0) {
                            programmiRientratiList = programmiRientrati.iterator();

                            while(true) {
                                while(programmiRientratiList.hasNext()) {
                                    programmaRientrato = (ProgrammaRientrato)programmiRientratiList.next();
                                    if (programmaRientrato.getFlagPMPrincipale() != null && programmaRientrato.getFlagPMPrincipale() == 'Y') {
                                        programmaRientrato.setImportoRicalcolato(rigaElaborata.getValoreProQuota() * (double)programmaRientrato.getCedole());
                                        this.log.debug("Entrato if principale: " + rigaElaborata.getValoreProQuota() + " " + programmaRientrato.getCedole());
                                    } else {
                                        programmaRientrato.setImportoRicalcolato(rigaElaborata.getValoreProQuotaSpalla() * (double)programmaRientrato.getCedole());
                                        this.log.debug("Entrato if else. Importo: " + rigaElaborata.getValoreProQuota() + " " + programmaRientrato.getCedole());
                                    }
                                }

                                return;
                            }
                        } else {
                            rigaElaborata.setImportoTotPrincipale((Double)null);
                            rigaElaborata.setImportoTotSpalla((Double)null);
                            this.log.debug("Entrato in ricalcolo proQuota standard in suddivisione 95% 5%");
                            this.eseguiRicalcoloProQuotaStandard(rigaElaborata);
                        }
                    } else {
                        programmiRientrati = rigaElaborata.getListaPmRientratiEventoVoce().values();
                        programmiRientratiList = programmiRientrati.iterator();

                        while(true) {
                            while(programmiRientratiList.hasNext()) {
                                programmaRientrato = (ProgrammaRientrato)programmiRientratiList.next();
                                if (programmaRientrato.getFlagPMPrincipale() != null && programmaRientrato.getFlagPMPrincipale() == 'Y') {
                                    programmaRientrato.setImportoRicalcolato(rigaElaborata.getValoreProQuota() * (double)programmaRientrato.getCedole());
                                    this.log.debug("Entrato if principale. Importo: " + rigaElaborata.getValoreProQuota() + " " + programmaRientrato.getCedole());
                                } else {
                                    programmaRientrato.setImportoRicalcolato(rigaElaborata.getValoreProQuotaSpalla() * (double)programmaRientrato.getCedole());
                                    this.log.debug("Entrato if else. Importo: " + rigaElaborata.getValoreProQuota() + " " + programmaRientrato.getCedole());
                                }
                            }

                            return;
                        }
                    }
                }
                break;
            }
        } else {
            this.log.debug("Entrato in ricalcolo quota standard della fine gestione 2244 per voce incasso diverso da 2244");
            this.eseguiRicalcoloProQuotaStandard(rigaElaborata);
        }

    }

    private void eseguiRicalcoloProQuota2243(RigaElaborata rigaElaborata) {
        if (rigaElaborata.getVoceIncasso().equalsIgnoreCase("2243")) {
            //int numeroFatture = false;
            int rientratiPrincipale = 0;
            int rientratiSpalla = 0;
            Long numeroCedoleTotaliPrincipale = 0L;
            Long numeroCedoleTotaliSpalla = 0L;
            Long maxCedolePrincipale = 0L;
            Long maxCedoleSpalla = 0L;
            Collection<ProgrammaRientrato> programmiRientrati = rigaElaborata.getListaPmRientratiEventoVoce().values();
            Iterator<ProgrammaRientrato> programmiRientratiList = programmiRientrati.iterator();
            Long numeroPMAttesiPrincipale = rigaElaborata.getNumeroPmAttesiEventoVoce();
            Long numeroPMAttesiSpalla = rigaElaborata.getNumeroPmAttesiEventoVoceSpalla();

            while(true) {
                ProgrammaRientrato programmaRientrato;
                while(programmiRientratiList.hasNext()) {
                    programmaRientrato = (ProgrammaRientrato)programmiRientratiList.next();
                    if (programmaRientrato.getFlagPMPrincipale() != null && programmaRientrato.getFlagPMPrincipale() == 'Y') {
                        ++rientratiPrincipale;
                        numeroCedoleTotaliPrincipale = numeroCedoleTotaliPrincipale + programmaRientrato.getCedole();
                        if (programmaRientrato.getCedole() > maxCedolePrincipale) {
                            maxCedolePrincipale = programmaRientrato.getCedole();
                        }
                    } else {
                        ++rientratiSpalla;
                        numeroCedoleTotaliSpalla = numeroCedoleTotaliSpalla + programmaRientrato.getCedole();
                        if (programmaRientrato.getCedole() > maxCedoleSpalla) {
                            maxCedoleSpalla = programmaRientrato.getCedole();
                        }
                    }
                }

                if (numeroPMAttesiPrincipale != 0L && numeroPMAttesiSpalla != 0L) {
                    if (numeroPMAttesiPrincipale != 0L && numeroPMAttesiSpalla != 0L) {
                        if ((long)rientratiPrincipale > numeroPMAttesiPrincipale) {
                            rigaElaborata.setNumeroPmAttesiEventoVoce(new Long((long)rientratiPrincipale));
                            if (numeroPMAttesiSpalla - (long)rientratiPrincipale > 0L) {
                                rigaElaborata.setNumeroPmAttesiEventoVoceSpalla(new Long(numeroPMAttesiSpalla - (long)rientratiPrincipale));
                            } else {
                                rigaElaborata.setNumeroPmAttesiEventoVoceSpalla(new Long(0L));
                            }
                        }

                        if ((long)rientratiSpalla > numeroPMAttesiSpalla) {
                            rigaElaborata.setNumeroPmAttesiEventoVoceSpalla(new Long((long)rientratiSpalla));
                            if (numeroPMAttesiPrincipale - (long)rientratiSpalla > 0L) {
                                rigaElaborata.setNumeroPmAttesiEventoVoce(new Long(numeroPMAttesiPrincipale - (long)rientratiSpalla));
                            } else {
                                rigaElaborata.setNumeroPmAttesiEventoVoce(new Long(0L));
                            }
                        }
                    }
                } else if (numeroPMAttesiPrincipale == 0L && rientratiPrincipale > 0) {
                    rigaElaborata.setNumeroPmAttesiEventoVoce(new Long((long)rientratiPrincipale));
                    if (numeroPMAttesiSpalla - (long)rientratiPrincipale > 0L) {
                        rigaElaborata.setNumeroPmAttesiEventoVoceSpalla(new Long(numeroPMAttesiSpalla - (long)rientratiPrincipale));
                    } else {
                        rigaElaborata.setNumeroPmAttesiEventoVoceSpalla(new Long(0L));
                    }
                } else if (numeroPMAttesiSpalla == 0L && rientratiSpalla > 0) {
                    rigaElaborata.setNumeroPmAttesiEventoVoceSpalla(new Long((long)rientratiSpalla));
                    if (numeroPMAttesiPrincipale - (long)rientratiSpalla > 0L) {
                        rigaElaborata.setNumeroPmAttesiEventoVoce(new Long(numeroPMAttesiPrincipale - (long)rientratiSpalla));
                    } else {
                        rigaElaborata.setNumeroPmAttesiEventoVoce(new Long(0L));
                    }
                }

                if ((long)rigaElaborata.getListaPmRientratiEventoVoce().size() == rigaElaborata.getNumeroPmAttesiEventoVoce() + rigaElaborata.getNumeroPmAttesiEventoVoceSpalla() && (rigaElaborata.getNumeroPmAttesiEventoVoce() != (long)rientratiPrincipale || rigaElaborata.getNumeroPmAttesiEventoVoceSpalla() != (long)rientratiSpalla)) {
                    rigaElaborata.setNumeroPmAttesiEventoVoce(new Long((long)rientratiPrincipale));
                    rigaElaborata.setNumeroPmAttesiEventoVoceSpalla(new Long((long)rientratiSpalla));
                }

                Double importoTotaleEventoVoce = rigaElaborata.getImportoTotEventoFatturaVoce();
                Double importoPrincipaleEventoVoce = 0.0;
                Double importoSpallaEventoVoce = 0.0;
                if (rigaElaborata.getContabilitaPagamentoEvento() < Constants.DATA1_2) {
                    if (rigaElaborata.getNumeroPmAttesiEventoVoce() > 0L && rigaElaborata.getNumeroPmAttesiEventoVoceSpalla() > 0L) {
                        importoPrincipaleEventoVoce = importoTotaleEventoVoce * Constants.QUOTA_GRUPPO_PRINCIPALE1;
                        importoSpallaEventoVoce = importoTotaleEventoVoce * Constants.QUOTA_GRUPPO_SPALLA1;
                    } else if (rigaElaborata.getNumeroPmAttesiEventoVoce() > 0L && rigaElaborata.getNumeroPmAttesiEventoVoceSpalla() == 0L) {
                        importoPrincipaleEventoVoce = importoTotaleEventoVoce;
                    } else if (rigaElaborata.getNumeroPmAttesiEventoVoce() == 0L && rigaElaborata.getNumeroPmAttesiEventoVoceSpalla() > 0L) {
                        importoSpallaEventoVoce = importoTotaleEventoVoce;
                    }
                } else if ((rigaElaborata.getNumeroPmAttesiEventoVoce() <= 0L || rigaElaborata.getNumeroPmAttesiEventoVoceSpalla() <= 0L) && (rientratiPrincipale <= 0 || rientratiSpalla <= 0)) {
                    if (rigaElaborata.getNumeroPmAttesiEventoVoce() > 0L && rigaElaborata.getNumeroPmAttesiEventoVoceSpalla() == 0L) {
                        importoPrincipaleEventoVoce = importoTotaleEventoVoce;
                    } else if (rigaElaborata.getNumeroPmAttesiEventoVoce() == 0L && rigaElaborata.getNumeroPmAttesiEventoVoceSpalla() > 0L) {
                        importoSpallaEventoVoce = importoTotaleEventoVoce;
                    }
                } else {
                    importoPrincipaleEventoVoce = importoTotaleEventoVoce * Constants.QUOTA_GRUPPO_PRINCIPALE2;
                    importoSpallaEventoVoce = importoTotaleEventoVoce * Constants.QUOTA_GRUPPO_SPALLA2;
                    if (importoSpallaEventoVoce > Constants.QUOTA_CAP) {
                        importoSpallaEventoVoce = Constants.QUOTA_CAP;
                        importoPrincipaleEventoVoce = importoTotaleEventoVoce - Constants.QUOTA_CAP;
                    }
                }

                rigaElaborata.setImportoTotPrincipale(importoPrincipaleEventoVoce);
                rigaElaborata.setImportoTotSpalla(importoSpallaEventoVoce);
                if (programmiRientrati.size() > 0) {
                    Double valoreProQuotaPrincipale = 0.0;
                    Double valoreProQuotaSpalla = 0.0;
                    numeroCedoleTotaliPrincipale = numeroCedoleTotaliPrincipale + (rigaElaborata.getNumeroPmAttesiEventoVoce() - (long)rientratiPrincipale) * maxCedolePrincipale;
                    numeroCedoleTotaliSpalla = numeroCedoleTotaliSpalla + (rigaElaborata.getNumeroPmAttesiEventoVoceSpalla() - (long)rientratiSpalla) * maxCedoleSpalla;
                    if (rigaElaborata.getImportoTotPrincipale() != 0.0 && numeroCedoleTotaliPrincipale != 0L) {
                        valoreProQuotaPrincipale = rigaElaborata.getImportoTotPrincipale() / (double)numeroCedoleTotaliPrincipale;
                        rigaElaborata.setValoreProQuota(valoreProQuotaPrincipale);
                        rigaElaborata.setNumeroCedolePmAttesi(numeroCedoleTotaliPrincipale);
                    } else {
                        rigaElaborata.setValoreProQuota(0.0);
                        rigaElaborata.setNumeroCedolePmAttesi(0L);
                    }

                    if (rigaElaborata.getImportoTotSpalla() != 0.0 && numeroCedoleTotaliSpalla != 0L) {
                        valoreProQuotaSpalla = rigaElaborata.getImportoTotSpalla() / (double)numeroCedoleTotaliSpalla;
                        rigaElaborata.setValoreProQuotaSpalla(valoreProQuotaSpalla);
                        rigaElaborata.setNumeroCedolePmAttesiSpalla(numeroCedoleTotaliSpalla);
                    } else {
                        rigaElaborata.setValoreProQuotaSpalla(0.0);
                        rigaElaborata.setNumeroCedolePmAttesiSpalla(0L);
                    }

                    if ((long)rigaElaborata.getListaPmRientratiEventoVoce().size() == rigaElaborata.getNumeroPmAttesiEventoVoce() + rigaElaborata.getNumeroPmAttesiEventoVoceSpalla()) {
                        if (valoreProQuotaPrincipale > valoreProQuotaSpalla) {
                            programmiRientratiList = programmiRientrati.iterator();

                            while(true) {
                                while(programmiRientratiList.hasNext()) {
                                    programmaRientrato = (ProgrammaRientrato)programmiRientratiList.next();
                                    if (programmaRientrato.getFlagPMPrincipale() != null && programmaRientrato.getFlagPMPrincipale() == 'Y') {
                                        programmaRientrato.setImportoRicalcolato(rigaElaborata.getValoreProQuota() * (double)programmaRientrato.getCedole());
                                    } else {
                                        programmaRientrato.setImportoRicalcolato(rigaElaborata.getValoreProQuotaSpalla() * (double)programmaRientrato.getCedole());
                                    }
                                }

                                return;
                            }
                        } else {
                            rigaElaborata.setImportoTotPrincipale((Double)null);
                            rigaElaborata.setImportoTotSpalla((Double)null);
                            this.eseguiRicalcoloProQuotaStandard(rigaElaborata);
                        }
                    } else {
                        programmiRientrati = rigaElaborata.getListaPmRientratiEventoVoce().values();
                        programmiRientratiList = programmiRientrati.iterator();

                        while(true) {
                            while(programmiRientratiList.hasNext()) {
                                programmaRientrato = (ProgrammaRientrato)programmiRientratiList.next();
                                if (programmaRientrato.getFlagPMPrincipale() != null && programmaRientrato.getFlagPMPrincipale() == 'Y') {
                                    programmaRientrato.setImportoRicalcolato(rigaElaborata.getValoreProQuota() * (double)programmaRientrato.getCedole());
                                } else {
                                    programmaRientrato.setImportoRicalcolato(rigaElaborata.getValoreProQuotaSpalla() * (double)programmaRientrato.getCedole());
                                }
                            }

                            return;
                        }
                    }
                }
                break;
            }
        }

    }

    private void eseguiRicalcoloProQuotaStandard(RigaElaborata rigaElaborata) {
        this.log.debug("Sono entrato in Quota Standard");
        Long numeroCedoleTotali = 0L;
        Long maxCedole = 1L;
        Double valoreProQuota = 0.0;
        int rientrati = 0;
        Collection<ProgrammaRientrato> programmiRientrati = rigaElaborata.getListaPmRientratiEventoVoce().values();
        Iterator<ProgrammaRientrato> programmiRientratiList = programmiRientrati.iterator();
        ProgrammaRientrato programmaRientrato;
        if (rigaElaborata.getVoceIncasso().equalsIgnoreCase("2244")) {
            programmiRientratiList = programmiRientrati.iterator();

            while(true) {
                while(programmiRientratiList.hasNext()) {
                    programmaRientrato = (ProgrammaRientrato)programmiRientratiList.next();
                    if (programmaRientrato.getFlagPMPrincipale() != null && programmaRientrato.getFlagPMPrincipale() == 'Y') {
                        programmaRientrato.setImportoRicalcolato(rigaElaborata.getValoreProQuota() * (double)programmaRientrato.getCedole());
                        this.log.debug("Entrato if principale: " + rigaElaborata.getValoreProQuota() + " " + programmaRientrato.getCedole());
                    } else {
                        programmaRientrato.setImportoRicalcolato(rigaElaborata.getValoreProQuotaSpalla() * (double)programmaRientrato.getCedole());
                        this.log.debug("Entrato if else. Importo: " + rigaElaborata.getValoreProQuota() + " " + programmaRientrato.getCedole());
                    }
                }

                return;
            }
        } else {
            while(programmiRientratiList.hasNext()) {
                programmaRientrato = (ProgrammaRientrato)programmiRientratiList.next();
                ++rientrati;
                if (programmaRientrato.getCedole() == 0L) {
                    this.traceService.trace("ValorizzatoreInstance", this.getClass().getName(), "eseguiRicalcoloProQuotaStandard", "idProgramma " + programmaRientrato.getId() + " senza numero cedole", "WARNING");
                }

                this.log.debug("While: Cedole totali" + programmaRientrato.getCedole());
                numeroCedoleTotali = numeroCedoleTotali + programmaRientrato.getCedole();
                if (programmaRientrato.getCedole() > maxCedole) {
                    maxCedole = programmaRientrato.getCedole();
                }
            }

            this.log.debug("Numero iterazioni: " + rientrati);
            this.log.debug("Numero cedole totali: " + numeroCedoleTotali);
            numeroCedoleTotali = numeroCedoleTotali + (rigaElaborata.getNumeroPmAttesiEventoVoce() - (long)rientrati) * maxCedole;
            this.log.debug("PM Attesi evento voce: " + rigaElaborata.getNumeroPmAttesiEventoVoce());
            this.log.debug("rientrati: " + rientrati);
            this.log.debug("max cedole: " + maxCedole);
            if (numeroCedoleTotali != 0L) {
                valoreProQuota = rigaElaborata.getImportoTotEventoFatturaVoce() / (double)numeroCedoleTotali;
                this.log.debug("Importo totale evento fattura voce: " + rigaElaborata.getImportoTotEventoFatturaVoce() + " " + numeroCedoleTotali);
            } else {
                valoreProQuota = 0.0;
            }

            rigaElaborata.setValoreProQuota(valoreProQuota);
            rigaElaborata.setNumeroCedolePmAttesi(numeroCedoleTotali);
            programmiRientratiList = programmiRientrati.iterator();

            while(programmiRientratiList.hasNext()) {
                programmaRientrato = (ProgrammaRientrato)programmiRientratiList.next();
                programmaRientrato.setImportoRicalcolato(rigaElaborata.getValoreProQuota() * (double)programmaRientrato.getCedole());
                this.log.debug("Quota standard calcolo importo: " + rigaElaborata.getValoreProQuota() + " " + programmaRientrato.getCedole());
            }

        }
    }

    private void eseguiRicalcoloProDurata(RigaElaborata rigaElaborata) {
        new ProgrammaRientrato();
        Double durataTotale = 0.0;
        Double maxDurata = 1.0;
        Double valoreAlSecondo = 0.0;
        int rientrati = 0;
        Collection<ProgrammaRientrato> programmiRientrati = rigaElaborata.getListaPmRientratiEventoVoce().values();
        Iterator<ProgrammaRientrato> programmiRientratiList = programmiRientrati.iterator();
        this.log.debug("Programmi rientrati size: " + programmiRientrati.size());

        ProgrammaRientrato programmaRientrato;
        for(; programmiRientratiList.hasNext(); rigaElaborata.getListaPmRientratiEventoFatturaVoce().put(programmaRientrato.getId(), programmaRientrato)) {
            programmaRientrato = (ProgrammaRientrato)programmiRientratiList.next();
            this.log.debug("Programma in corso: " + programmaRientrato.getId());
            ++rientrati;
            this.log.debug("Entrato in programma rientrati list");
            if (programmaRientrato.getCedole() == 0L) {
                this.traceService.trace("ValorizzatoreInstance", this.getClass().getName(), "eseguiRicalcoloProDurata", "idProgramma " + programmaRientrato.getId() + " senza durata cedole", "WARNING");
            }

            if (programmaRientrato.getDurata() == null) {
                programmaRientrato.setDurata(0.0);
            }

            durataTotale = durataTotale + programmaRientrato.getDurata();
            if (programmaRientrato.getDurata() > maxDurata) {
                maxDurata = programmaRientrato.getDurata();
            }
        }

        durataTotale = durataTotale + (double)(rigaElaborata.getNumeroPmAttesiEventoVoce() - (long)rientrati) * maxDurata;
        this.log.debug("Durata totale: " + durataTotale);
        if (durataTotale != 0.0 && rigaElaborata.getImportoTotEventoFatturaVoce() != 0.0) {
            valoreAlSecondo = rigaElaborata.getImportoTotEventoFatturaVoce() / durataTotale;
            rigaElaborata.setValoreAlSecondo(valoreAlSecondo);
            rigaElaborata.setDurataPmAttesi(durataTotale);
        } else {
            rigaElaborata.setValoreAlSecondo(0.0);
            rigaElaborata.setDurataPmAttesi(0.0);
        }

        programmiRientratiList = programmiRientrati.iterator();

        while(programmiRientratiList.hasNext()) {
            programmaRientrato = (ProgrammaRientrato)programmiRientratiList.next();
            this.log.debug("Calcolo Pro Durata: " + rigaElaborata.getValoreAlSecondo() * programmaRientrato.getDurata());
            programmaRientrato.setImportoRicalcolato(rigaElaborata.getValoreAlSecondo() * programmaRientrato.getDurata());
            this.log.debug("Programma rientrato getImporto - Riga 692: " + programmaRientrato.getImportoRicalcolato());
            rigaElaborata.addPmRientratoEventoVoce(programmaRientrato);
        }

    }

    private Double getImporto(MovimentoContabile movimento, Long dataFineUltimaRipartizione, Double importoDEM) {
        List<MovimentoSiada> listaPMInviatiSIADA = this.gestioneSiadaDAO.getMovimentoSiada(movimento.getIdEvento(), movimento.getVoceIncasso(), dataFineUltimaRipartizione.intValue());
        Double importoResiduoPM = 0.0;
        Double importoRipartito = 0.0;
        Double importoTotale = 0.0;
        List listaPagamentiRicevuti;
        int w;
        if (movimento.getVoceIncasso().equalsIgnoreCase("2244")) {
            listaPagamentiRicevuti = this.programmaMusicaleDAO.getListaPagamentiEventoVoceFattura(movimento.getIdEvento(), movimento.getVoceIncasso(), movimento.getNumeroFattura());

            for(w = 0; w < listaPagamentiRicevuti.size(); ++w) {
                importoTotale = importoTotale + ((EventiPagati)listaPagamentiRicevuti.get(w)).getImportDem();
            }

            return importoTotale;
        } else if (listaPMInviatiSIADA != null && listaPMInviatiSIADA.size() > 0) {
            listaPagamentiRicevuti = this.programmaMusicaleDAO.getListaPagamentiEventoVoce(movimento.getIdEvento(), movimento.getVoceIncasso());
            if (listaPagamentiRicevuti != null && listaPagamentiRicevuti.size() > 0) {
                for(w = 0; w < listaPagamentiRicevuti.size(); ++w) {
                    importoTotale = importoTotale + ((EventiPagati)listaPagamentiRicevuti.get(w)).getImportDem();
                }

                for(w = 0; w < listaPMInviatiSIADA.size(); ++w) {
                    importoRipartito = importoRipartito + ((MovimentoSiada)listaPMInviatiSIADA.get(w)).getImporto();
                }

                if (importoTotale > importoRipartito) {
                    importoResiduoPM = importoTotale - importoRipartito;
                }
            }

            return importoResiduoPM;
        } else {
            return importoDEM;
        }
    }

    @Transactional
    public InformazioniElaborazione startElaborazioneRicalcolo(int engineIndex, Long elaborationId) {
        this.log.info("valorizzatoreService.startElaborazioneRicalcolo");
        InformazioniElaborazione esitoRicalcolo = new InformazioniElaborazione();
        this.traceService.trace("ValorizzatoreInstance" + engineIndex, this.getClass().getName(), "startElaborazioneRicalcolo", "Id elaborazione: " + elaborationId + ". Started ValorizzatoreInstance ricalcolo", "INFO");
        RigaElaborata rigaElaborata = null;
        this.log.info("valorizzatoreService.startElaborazioneRicalcolo " + this.esecuzioneDAO);
        Map<String, TrattamentoPM> mappaVociIncasso = new HashMap();
        Long dataFineUltimaRipartizione = null;
        dataFineUltimaRipartizione = Long.parseLong(this.configurationDAO.getParameter("FINE_PERIODO_RIPARTIZIONE"));
        dataFineUltimaRipartizione = Long.parseLong(Long.toString(dataFineUltimaRipartizione).substring(0, 6));
        Long dataInizioRicalcolo = Long.parseLong(this.configurationDAO.getParameter("INIZIO_PERIODO_RICALCOLO"));
        Long dataFineRicalcolo = Long.parseLong(this.configurationDAO.getParameter("FINE_PERIODO_RICALCOLO"));
        String tipologiaPM = this.configurationDAO.getParameter("TIPOLOGIA_PM");
        String intervalloMovimenti = this.configurationDAO.getParameter("ENGINE_INTERVAL_" + engineIndex);
        String[] minMaxIntervel = intervalloMovimenti.split("-");
        int minInterval = Integer.parseInt(minMaxIntervel[0]);
        int maxInterval = Integer.parseInt(minMaxIntervel[1]);
        boolean isPMGiaRipartito = false;
        Double importoResiduoPM = 0.0;
        if (tipologiaPM != null && tipologiaPM.equals("Tutte")) {
            tipologiaPM = null;
        }

        List<TrattamentoPM> listaVociIncasso = this.programmaMusicaleDAO.getTrattamentoPM();
        Iterator var17 = listaVociIncasso.iterator();

        while(var17.hasNext()) {
            TrattamentoPM voceIncasso = (TrattamentoPM)var17.next();
            mappaVociIncasso.put(voceIncasso.getVoceIncasso(), voceIncasso);
        }

        this.traceService.trace("ValorizzatoreInstance" + engineIndex, this.getClass().getName(), "startElaborazioneRicalcolo", "Intervallo movimenti da acquisire: " + minInterval + "-" + maxInterval, "INFO");
        Long idEsecuzioneRicalcolo = this.esecuzioneDAO.startEsecuzioneRicalcolo(elaborationId, dataInizioRicalcolo, dataFineRicalcolo, "");
        this.log.info(new Date() + " - id elaborazione: " + idEsecuzioneRicalcolo + ". Started ValorizzatoreInstance ricalcolo");
        if (dataInizioRicalcolo <= dataFineUltimaRipartizione) {
            esitoRicalcolo.setError("-1");
            esitoRicalcolo.setDescription("L'inizio del periodo di ricalcolo deve essere successivo alla fine dell'ultimo periodo di ripartizione");
            this.traceService.trace("ValorizzatoreInstance" + engineIndex, this.getClass().getName(), "startElaborazioneRicalcolo", "Codice Errore: " + esitoRicalcolo.getError() + " Descrizione: " + esitoRicalcolo.getDescription(), "ERROR");
        }

        if (esitoRicalcolo.getError().equalsIgnoreCase("0")) {
            Elaborazione elaborazione = new Elaborazione();
            this.log.info(new Date() + " - id elaborazione: " + idEsecuzioneRicalcolo + ". Start acquisizione movimenti contabili  da: " + dataInizioRicalcolo + " a: " + dataFineRicalcolo + " per PM " + tipologiaPM);
            this.traceService.trace("ValorizzatoreInstance" + engineIndex, this.getClass().getName(), "startElaborazioneRicalcolo", "Id elaborazione: " + idEsecuzioneRicalcolo + ". Start acquisizione movimenti contabili  da: " + dataInizioRicalcolo + " a: " + dataFineRicalcolo + " per PM " + tipologiaPM, "INFO");
            List<MovimentoContabile> movimenti = new ArrayList();

            try {
                movimenti = this.getMovimentiContabili(dataInizioRicalcolo, dataFineRicalcolo, tipologiaPM, minInterval, maxInterval);
            } catch (Exception var41) {
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                var41.printStackTrace(pw);

                try {
                    this.traceService.trace("ValorizzatoreInstance" + engineIndex, this.getClass().getName(), "startElaborazioneRicalcolo", sw.toString(), "ERROR");
                } catch (Exception var40) {
                    var40.printStackTrace();
                }
            }

            this.log.info(new Date() + " - id elaborazione: " + idEsecuzioneRicalcolo + ". End acquisizione movimenti contabili da: " + dataInizioRicalcolo + " a: " + dataFineRicalcolo + " per PM " + tipologiaPM + ". Trovati: " + ((List)movimenti).size());
            this.traceService.trace("ValorizzatoreInstance" + engineIndex, this.getClass().getName(), "startElaborazioneRicalcolo", "Id elaborazione: " + idEsecuzioneRicalcolo + ". End acquisizione movimenti contabili da: " + dataInizioRicalcolo + " a: " + dataFineRicalcolo + " per PM " + tipologiaPM + ". Trovati: " + ((List)movimenti).size(), "INFO");
            this.getInformazioniElaborazione().setTotalePM(new Long((long)((List)movimenti).size()));
            int i = 1;
            int j = 1;
            int k = 1;
            String sospeso = null;

            for(Iterator var26 = ((List)movimenti).iterator(); var26.hasNext(); ++i) {
                MovimentoContabile movimento = (MovimentoContabile)var26.next();
                isPMGiaRipartito = false;
                importoResiduoPM = 0.0;

                try {
                    this.log.info(new Date() + " - id elaborazione: " + idEsecuzioneRicalcolo + ". [" + i + "] (" + movimento.getIdEvento() + "|" + movimento.getNumeroFattura() + "|" + movimento.getVoceIncasso() + ") PM: " + movimento.getIdProgrammaMusicale() + " PM Attesi: " + movimento.getNumeroPmPrevisti());
                    ChiaveElaborazione chiaveElaborazione = new ChiaveElaborazione(movimento.getIdEvento(), movimento.getNumeroFattura(), movimento.getVoceIncasso());
                    rigaElaborata = new RigaElaborata();
                    rigaElaborata.setChiaveRigaElaborata(chiaveElaborazione);
                    rigaElaborata.setIdEvento(movimento.getIdEvento());
                    rigaElaborata.setNumeroFattura(movimento.getNumeroFattura());
                    rigaElaborata.setVoceIncasso(movimento.getVoceIncasso());
                    sospeso = null;
                    if (movimento.getImportoManuale() != null) {
                        sospeso = "IMPORTO_MODIFICATO_MANUALMENTE";
                    } else if (movimento.getVoceIncasso().equalsIgnoreCase("2267")) {
                        sospeso = "VOCE_INCASSO_2267";
                    }

                    rigaElaborata.setRigaSospesa(sospeso);
                    if (sospeso == null) {
                        EventiPagati eventiPagati = this.programmaMusicaleDAO.getInfoEventiPagatiEventoFatturaVoce(movimento.getIdEvento(), movimento.getNumeroFattura(), movimento.getVoceIncasso());
                        if (eventiPagati != null) {
                            rigaElaborata.setContabilitaPagamentoEvento(eventiPagati.getContabilita());
                            Map<Long, ProgrammaRientrato> programmiRientrati = this.programmaMusicaleDAO.getProgrammiRientratiEventoFatturaVoce(movimento.getIdEvento(), movimento.getNumeroFattura(), movimento.getVoceIncasso(), dataInizioRicalcolo, dataFineRicalcolo, tipologiaPM);
                            rigaElaborata.setListaPmRientratiEventoFatturaVoce(programmiRientrati);
                            this.log.debug("Lista pm rientrati size - Riga 923" + rigaElaborata.getListaPmRientratiEventoFatturaVoce().size());
                            Double importo = this.getImporto(movimento, dataFineUltimaRipartizione, eventiPagati.getImportDem());
                            this.log.debug("Importo - Riga 925: " + importo);
                            rigaElaborata.setImportoTotEventoFatturaVoce(importo);
                            this.log.debug("Riga elaborata - Riga 927: " + rigaElaborata.getImportoTotEventoFatturaVoce());
                            rigaElaborata.setNumeroPmAttesiEventoVoce(movimento.getNumeroPmPrevisti());
                            rigaElaborata.setNumeroPmAttesiEventoVoceSpalla(movimento.getNumeroPmPrevistiSpalla());
                            if (rigaElaborata.getNumeroPmAttesiEventoVoce() == null || rigaElaborata.getNumeroPmAttesiEventoVoce() != null && rigaElaborata.getNumeroPmAttesiEventoVoce() == 0L && !movimento.getVoceIncasso().equalsIgnoreCase("2244")) {
                                programmiRientrati = this.programmaMusicaleDAO.getProgrammiRientratiEventoVoce(movimento.getIdEvento(), movimento.getVoceIncasso(), dataInizioRicalcolo, dataFineRicalcolo, tipologiaPM);
                                Iterator<ProgrammaRientrato> programmiAbbinati = programmiRientrati.values().iterator();
                                long count = 0L;
                                long countSpalla = 0L;

                                label195:
                                while(true) {
                                    ProgrammaRientrato programmaAbbinato;
                                    label193:
                                    do {
                                        while(programmiAbbinati.hasNext()) {
                                            programmaAbbinato = (ProgrammaRientrato)programmiAbbinati.next();
                                            if (programmaAbbinato.getFlagPMPrincipale() != null && programmaAbbinato.getFlagPMPrincipale() != 'Y' && programmaAbbinato.getFlagPMPrincipale() != '1') {
                                                continue label193;
                                            }

                                            ++count;
                                        }

                                        rigaElaborata.setNumeroPmAttesiEventoVoce(count);
                                        rigaElaborata.setNumeroPmAttesiEventoVoceSpalla(countSpalla);
                                        break label195;
                                    } while(programmaAbbinato.getFlagPMPrincipale() != 'N' && programmaAbbinato.getFlagPMPrincipale() != '0');

                                    ++countSpalla;
                                }
                            }

                            List<RigaElaborata> elaborazioneVoceIncasso = elaborazione.getListaRigaElaborataEventoVoce(movimento.getIdEvento(), movimento.getVoceIncasso());
                            if (elaborazioneVoceIncasso != null && elaborazioneVoceIncasso.size() > 0) {
                                RigaElaborata eventoVocePagato = (RigaElaborata)elaborazioneVoceIncasso.get(0);
                                rigaElaborata.setNumeroPmRientratiEventoVoce(eventoVocePagato.getNumeroPmRientratiEventoVoce());
                                rigaElaborata.setListaPmRientratiEventoVoce(eventoVocePagato.getListaPmRientratiEventoVoce());
                                this.log.debug("RIGA ELABORATA - Riga 974:" + ((ProgrammaRientrato)rigaElaborata.getListaPmRientratiEventoVoce().get(movimento.getIdProgrammaMusicale())).toString());
                                if (eventoVocePagato.getNumeroPmRientratiEventoVoce() != null && eventoVocePagato.getNumeroPmRientratiEventoVoce() == 0L) {
                                    programmiRientrati = this.programmaMusicaleDAO.getProgrammiRientratiEventoVoce(movimento.getIdEvento(), movimento.getVoceIncasso(), dataInizioRicalcolo, dataFineRicalcolo, tipologiaPM);
                                    rigaElaborata.setNumeroPmRientratiEventoVoce(new Long((long)programmiRientrati.size()));
                                    rigaElaborata.setListaPmRientratiEventoVoce(programmiRientrati);
                                    this.log.debug("RIGA ELABORATA - Riga 983:" + ((ProgrammaRientrato)rigaElaborata.getListaPmRientratiEventoVoce().get(movimento.getIdProgrammaMusicale())).toString());
                                }
                            } else {
                                programmiRientrati = this.programmaMusicaleDAO.getProgrammiRientratiEventoVoce(movimento.getIdEvento(), movimento.getVoceIncasso(), dataInizioRicalcolo, dataFineRicalcolo, tipologiaPM);
                                rigaElaborata.setNumeroPmRientratiEventoVoce(new Long((long)programmiRientrati.size()));
                                rigaElaborata.setListaPmRientratiEventoVoce(programmiRientrati);
                                this.log.debug("RIGA ELABORATA - Riga 992:" + ((ProgrammaRientrato)rigaElaborata.getListaPmRientratiEventoVoce().get(movimento.getIdProgrammaMusicale())).toString());
                            }

                            TrattamentoPM tipoVoceIncasso = (TrattamentoPM)mappaVociIncasso.get(movimento.getVoceIncasso());
                            if (!tipoVoceIncasso.getTipo().equalsIgnoreCase("A") && !tipoVoceIncasso.getTipo().equalsIgnoreCase("B") && !tipoVoceIncasso.getTipo().equalsIgnoreCase("D")) {
                                this.eseguiRicalcoloProDurata(rigaElaborata);
                                this.log.debug("Ricalcolo Pro durata - Riga 1005" + rigaElaborata.getListaPmRientratiEventoFatturaVoce().values().toString());
                            } else {
                                this.eseguiRicalcoloProQuota(rigaElaborata);
                                this.log.debug("Ricalcolo Pro quota");
                            }
                        } else {
                            List<MovimentoSiada> listaPMInviatiSIADA = this.gestioneSiadaDAO.getMovimentoSiada(movimento.getIdEvento(), movimento.getVoceIncasso(), dataFineUltimaRipartizione.intValue());
                            if (listaPMInviatiSIADA != null && listaPMInviatiSIADA.size() > 0) {
                                Double importoRipartito = 0.0;
                                Double importoTotale = 0.0;
                                List<EventiPagati> listaPagamentiRicevuti = this.programmaMusicaleDAO.getListaPagamentiEventoVoce(movimento.getIdEvento(), movimento.getVoceIncasso());
                                if (listaPagamentiRicevuti != null && listaPagamentiRicevuti.size() > 0) {
                                    int w;
                                    for(w = 0; w < listaPagamentiRicevuti.size(); ++w) {
                                        importoTotale = importoTotale + ((EventiPagati)listaPagamentiRicevuti.get(w)).getImportDem();
                                    }

                                    for(w = 0; w < listaPMInviatiSIADA.size(); ++w) {
                                        importoRipartito = importoRipartito + ((MovimentoSiada)listaPMInviatiSIADA.get(w)).getImporto();
                                    }

                                    if (importoTotale > importoRipartito) {
                                        importoResiduoPM = importoTotale - importoRipartito;
                                        isPMGiaRipartito = true;
                                    }
                                }
                            } else {
                                rigaElaborata.setRigaSospesa("EVENTO_IMPORTO_INESISTENTE");
                            }
                        }
                    }

                    if (rigaElaborata.getRigaSospesa() != null) {
                        this.log.debug("Riga sospesa");
                        this.log.info(new Date() + " - id elaborazione: " + idEsecuzioneRicalcolo + ". [" + i + "] (" + movimento.getIdEvento() + "|" + movimento.getNumeroFattura() + "|" + movimento.getVoceIncasso() + ") PM: " + movimento.getIdProgrammaMusicale() + " SOSPESO1");
                        this.programmaMusicaleDAO.insertSospesiRicalcolo(movimento.getIdEvento(), movimento.getNumeroFattura(), movimento.getVoceIncasso(), idEsecuzioneRicalcolo, movimento.getIdProgrammaMusicale(), movimento.getId(), rigaElaborata.getRigaSospesa());
                        this.getInformazioniElaborazione().setTotaleSospesi(new Long((long)j));
                        ++j;
                    } else if (isPMGiaRipartito) {
                        this.log.info(new Date() + " - id elaborazione: " + idEsecuzioneRicalcolo + ". [" + i + "] (" + movimento.getIdEvento() + "|" + movimento.getNumeroFattura() + "|" + movimento.getVoceIncasso() + ") Movimento: " + movimento.getId() + " PM: " + movimento.getIdProgrammaMusicale() + " Importo SUN: " + movimento.getImportoTotDemLordo() + " Importo Ricalcolato: " + importoResiduoPM);
                        this.log.debug("isPmGiàRipartito importo di Pm già elaborato: " + importoResiduoPM);
                        this.programmaMusicaleDAO.insertImportoRicalcolato(movimento.getIdEvento(), movimento.getNumeroFattura(), movimento.getVoceIncasso(), idEsecuzioneRicalcolo, movimento.getId(), importoResiduoPM, (Double)null);
                    } else {
                        this.log.debug("Riga evento fattura voce prima" + rigaElaborata.getListaPmRientratiEventoFatturaVoce().values().toString());
                        ProgrammaRientrato programmaRielaborato = rigaElaborata.getPmRientratoEventoVoce(movimento.getIdProgrammaMusicale());
                        this.log.debug("Riga elaborata valore al secondo" + rigaElaborata.getValoreAlSecondo());
                        this.log.debug("Riga elaborata programma rientrato durata" + programmaRielaborato.getDurata());
                        this.log.debug("Riga evento fattura voce dopo" + rigaElaborata.getListaPmRientratiEventoFatturaVoce().values().toString());
                        this.log.debug("Programma Rielaborato ID: " + movimento.getIdProgrammaMusicale());
                        this.log.debug("Programma rielaborato - Riga 1080: " + programmaRielaborato.toString());
                        this.log.info(new Date() + " - id elaborazione: " + idEsecuzioneRicalcolo + ". [" + i + "] (" + movimento.getIdEvento() + "|" + movimento.getNumeroFattura() + "|" + movimento.getVoceIncasso() + ") Movimento: " + movimento.getId() + " PM: " + movimento.getIdProgrammaMusicale() + " Importo SUN: " + movimento.getImportoTotDemLordo() + " Importo Ricalcolato: " + programmaRielaborato.getImportoRicalcolato());
                        if (programmaRielaborato.getImportoRicalcolato() == null) {
                            programmaRielaborato.setImportoRicalcolato(0.0);
                        }

                        this.programmaMusicaleDAO.insertImportoRicalcolato(movimento.getIdEvento(), movimento.getNumeroFattura(), movimento.getVoceIncasso(), idEsecuzioneRicalcolo, movimento.getId(), programmaRielaborato.getImportoRicalcolato(), programmaRielaborato.getImportoSingolaCedola());
                    }
                } catch (Exception var42) {
                    String description = null;
                    StringWriter sw;
                    PrintWriter pw;
                    if (rigaElaborata != null) {
                        description = "Error - id elaborazione: " + idEsecuzioneRicalcolo + " Key: (Evento=" + rigaElaborata.getIdEvento() + "|Fattura=" + rigaElaborata.getNumeroFattura() + "|Voce=" + rigaElaborata.getVoceIncasso() + "): " + var42.getMessage();
                        this.log.info(description);

                        try {
                            this.programmaMusicaleDAO.insertSospesiRicalcolo(movimento.getIdEvento(), movimento.getNumeroFattura(), movimento.getVoceIncasso(), idEsecuzioneRicalcolo, movimento.getIdProgrammaMusicale(), movimento.getId(), "ERRORE_ELABORAZIONE: " + description);
                        } catch (Exception var39) {
                            var39.printStackTrace();
                        }

                        rigaElaborata.setRigaSospesa("ERRORE");
                        this.getInformazioniElaborazione().setTotaleErrori(new Long((long)k));
                        ++k;
                        sw = new StringWriter();
                        pw = new PrintWriter(sw);
                        var42.printStackTrace(pw);

                        try {
                            this.traceService.trace("ValorizzatoreInstance" + engineIndex, this.getClass().getName(), "startElaborazioneRicalcolo", description + " \n " + sw.toString(), "ERROR");
                        } catch (Exception var38) {
                            var38.printStackTrace();
                        }
                    } else {
                        description = "Error - id elaborazione: " + idEsecuzioneRicalcolo + ". Eccezione: " + var42.getMessage();
                        this.log.info(description);

                        try {
                            this.programmaMusicaleDAO.insertSospesiRicalcolo(movimento.getIdEvento(), movimento.getNumeroFattura(), movimento.getVoceIncasso(), idEsecuzioneRicalcolo, movimento.getIdProgrammaMusicale(), movimento.getId(), "ERRORE_ELABORAZIONE: " + description);
                        } catch (Exception var37) {
                            var37.printStackTrace();
                        }

                        this.getInformazioniElaborazione().setTotaleErrori(new Long((long)k));
                        ++k;
                        sw = new StringWriter();
                        pw = new PrintWriter(sw);
                        var42.printStackTrace(pw);

                        try {
                            this.traceService.trace("ValorizzatoreInstance" + engineIndex, this.getClass().getName(), "startElaborazioneRicalcolo", description + " \n " + sw.toString(), "ERROR");
                        } catch (Exception var36) {
                            var36.printStackTrace();
                        }
                    }
                }

                elaborazione.addRigaElaborata(rigaElaborata);
                this.getInformazioniElaborazione().setTotaleElaborati(new Long((long)i));
            }

            if (((List)movimenti).size() == 0) {
                this.getInformazioniElaborazione().setTotaleElaborati(new Long(-1L));
            }
        }

        this.esecuzioneDAO.endEsecuzioneRicalcolo(idEsecuzioneRicalcolo, esitoRicalcolo.getError(), esitoRicalcolo.getDescription());
        this.getInformazioniElaborazione().setTerminate(true);
        this.log.info(new Date() + " - id elaborazione: " + idEsecuzioneRicalcolo + ". Ended ValorizzatoreInstance ricalcolo");
        this.traceService.trace("ValorizzatoreInstance" + engineIndex, this.getClass().getName(), "startElaborazioneRicalcolo", "Id elaborazione: " + idEsecuzioneRicalcolo + ". Ended ValorizzatoreInstance ricalcolo", "INFO");
        this.log.info("ValorizzatoreService: ## FINE ELABORAZIONE ##");
        return esitoRicalcolo;
    }

    @Transactional
    public InformazioniElaborazione startElaborazioneRicalcolo2243(int engineIndex, Long elaborationId) {
        this.log.info("valorizzatoreService.startElaborazioneRicalcolo2243");
        InformazioniElaborazione esitoRicalcolo = new InformazioniElaborazione();
        this.traceService.trace("ValorizzatoreInstance2243_" + engineIndex, this.getClass().getName(), "startElaborazioneRicalcolo2243", "Id elaborazione: " + elaborationId + ". Started ValorizzatoreInstance ricalcolo", "INFO");
        RigaElaborata rigaElaborata = null;
        this.log.info("valorizzatoreService.startElaborazioneRicalcolo2243 " + this.esecuzioneDAO);
        Map<String, TrattamentoPM> mappaVociIncasso = new HashMap();
        Long dataFineUltimaRipartizione = null;
        dataFineUltimaRipartizione = Long.parseLong(this.configurationDAO.getParameter("FINE_PERIODO_RIPARTIZIONE_2243"));
        dataFineUltimaRipartizione = Long.parseLong(Long.toString(dataFineUltimaRipartizione).substring(0, 6));
        Long dataInizioRicalcolo = Long.parseLong(this.configurationDAO.getParameter("INIZIO_PERIODO_RICALCOLO_2243"));
        Long dataFineRicalcolo = Long.parseLong(this.configurationDAO.getParameter("FINE_PERIODO_RICALCOLO_2243"));
        String tipologiaPM = this.configurationDAO.getParameter("TIPOLOGIA_PM_2243");
        String intervalloMovimenti = this.configurationDAO.getParameter("ENGINE_INTERVAL_2243_" + engineIndex);
        String[] minMaxIntervel = intervalloMovimenti.split("-");
        int minInterval = Integer.parseInt(minMaxIntervel[0]);
        int maxInterval = Integer.parseInt(minMaxIntervel[1]);
        boolean isPMGiaRipartito = false;
        Double importoResiduoPM = 0.0;
        if (tipologiaPM != null && tipologiaPM.equals("Tutte")) {
            tipologiaPM = null;
        }

        List<TrattamentoPM> listaVociIncasso = this.programmaMusicaleDAO.getTrattamentoPM();
        Iterator var17 = listaVociIncasso.iterator();

        while(var17.hasNext()) {
            TrattamentoPM voceIncasso = (TrattamentoPM)var17.next();
            mappaVociIncasso.put(voceIncasso.getVoceIncasso(), voceIncasso);
        }

        this.traceService.trace("ValorizzatoreInstance2243_" + engineIndex, this.getClass().getName(), "startElaborazioneRicalcolo2243", "Intervallo movimenti da acquisire: " + minInterval + "-" + maxInterval, "INFO");
        Long idEsecuzioneRicalcolo = this.esecuzioneDAO.startEsecuzioneRicalcolo(elaborationId, dataInizioRicalcolo, dataFineRicalcolo, "2243");
        this.log.info(new Date() + " - id elaborazione: " + idEsecuzioneRicalcolo + ". Started ValorizzatoreInstance ricalcolo 2243");
        if (dataInizioRicalcolo <= dataFineUltimaRipartizione) {
            esitoRicalcolo.setError("-1");
            esitoRicalcolo.setDescription("L'inizio del periodo di ricalcolo deve essere successivo alla fine dell'ultimo periodo di ripartizione");
            this.traceService.trace("ValorizzatoreInstance2243_" + engineIndex, this.getClass().getName(), "startElaborazioneRicalcolo2243", "Codice Errore: " + esitoRicalcolo.getError() + " Descrizione: " + esitoRicalcolo.getDescription(), "ERROR");
        }

        if (esitoRicalcolo.getError().equalsIgnoreCase("0")) {
            Elaborazione elaborazione = new Elaborazione();
            this.log.info(new Date() + " - id elaborazione: " + idEsecuzioneRicalcolo + ". Start acquisizione movimenti contabili per voce 2243 da: " + dataInizioRicalcolo + " a: " + dataFineRicalcolo + " per PM " + tipologiaPM);
            this.traceService.trace("ValorizzatoreInstance2243_" + engineIndex, this.getClass().getName(), "startElaborazioneRicalcolo2243", "Id elaborazione: " + idEsecuzioneRicalcolo + ". Start acquisizione movimenti contabili per voce 2243 da: " + dataInizioRicalcolo + " a: " + dataFineRicalcolo + " per PM " + tipologiaPM, "INFO");
            List<MovimentoContabile> movimenti = new ArrayList();

            try {
                movimenti = this.getMovimentiContabili2243(dataInizioRicalcolo, dataFineRicalcolo, tipologiaPM, minInterval, maxInterval);
            } catch (Exception var40) {
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                var40.printStackTrace(pw);

                try {
                    this.traceService.trace("ValorizzatoreInstance2243_" + engineIndex, this.getClass().getName(), "startElaborazioneRicalcolo2243", sw.toString(), "ERROR");
                } catch (Exception var39) {
                    var39.printStackTrace();
                }
            }

            this.log.info(new Date() + " - id elaborazione: " + idEsecuzioneRicalcolo + ". End acquisizione movimenti contabili per voce 2243 da: " + dataInizioRicalcolo + " a: " + dataFineRicalcolo + " per PM " + tipologiaPM + ". Trovati: " + ((List)movimenti).size());
            this.traceService.trace("ValorizzatoreInstance2243_" + engineIndex, this.getClass().getName(), "startElaborazioneRicalcolo2243", "Id elaborazione: " + idEsecuzioneRicalcolo + ". End acquisizione movimenti contabili per voce 2243 da: " + dataInizioRicalcolo + " a: " + dataFineRicalcolo + " per PM " + tipologiaPM + ". Trovati: " + ((List)movimenti).size(), "INFO");
            this.getInformazioniElaborazione().setTotalePM(new Long((long)((List)movimenti).size()));
            int i = 1;
            int j = 1;
            int k = 1;
            String sospeso = null;

            for(Iterator var26 = ((List)movimenti).iterator(); var26.hasNext(); ++i) {
                MovimentoContabile movimento = (MovimentoContabile)var26.next();
                isPMGiaRipartito = false;
                importoResiduoPM = 0.0;

                try {
                    this.log.info(new Date() + " - id elaborazione: " + idEsecuzioneRicalcolo + ". [" + i + "] (" + movimento.getIdEvento() + "|" + movimento.getNumeroFattura() + "|" + movimento.getVoceIncasso() + ") PM: " + movimento.getIdProgrammaMusicale() + " PM Attesi: " + movimento.getNumeroPmPrevisti());
                    ChiaveElaborazione chiaveElaborazione = new ChiaveElaborazione(movimento.getIdEvento(), movimento.getNumeroFattura(), movimento.getVoceIncasso());
                    rigaElaborata = new RigaElaborata();
                    rigaElaborata.setChiaveRigaElaborata(chiaveElaborazione);
                    rigaElaborata.setIdEvento(movimento.getIdEvento());
                    rigaElaborata.setNumeroFattura(movimento.getNumeroFattura());
                    rigaElaborata.setVoceIncasso(movimento.getVoceIncasso());
                    sospeso = null;
                    if (movimento.getImportoManuale() != null) {
                        sospeso = "IMPORTO_MODIFICATO_MANUALMENTE";
                    }

                    rigaElaborata.setRigaSospesa(sospeso);
                    if (sospeso == null) {
                        EventiPagati eventiPagati = this.programmaMusicaleDAO.getInfoEventiPagatiEventoFatturaVoce(movimento.getIdEvento(), movimento.getNumeroFattura(), movimento.getVoceIncasso());
                        List listaPMInviatiSIADA;
                        if (eventiPagati != null) {
                            rigaElaborata.setContabilitaPagamentoEvento(eventiPagati.getContabilita());
                            Map<Long, ProgrammaRientrato> programmiRientrati = this.programmaMusicaleDAO.getProgrammiRientratiEventoFatturaVoce(movimento.getIdEvento(), movimento.getNumeroFattura(), movimento.getVoceIncasso(), dataInizioRicalcolo, dataFineRicalcolo, tipologiaPM);
                            rigaElaborata.setListaPmRientratiEventoFatturaVoce(programmiRientrati);
                            rigaElaborata.setImportoTotEventoFatturaVoce(this.getImporto(movimento, dataFineUltimaRipartizione, eventiPagati.getImportDem()));
                            rigaElaborata.setNumeroPmAttesiEventoVoce(movimento.getNumeroPmPrevisti());
                            rigaElaborata.setNumeroPmAttesiEventoVoceSpalla(movimento.getNumeroPmPrevistiSpalla());
                            if (rigaElaborata.getNumeroPmAttesiEventoVoce() == null) {
                                programmiRientrati = this.programmaMusicaleDAO.getProgrammiRientratiEventoVoce(movimento.getIdEvento(), movimento.getVoceIncasso(), dataInizioRicalcolo, dataFineRicalcolo, tipologiaPM);
                                Iterator<ProgrammaRientrato> programmiAbbinati = programmiRientrati.values().iterator();
                                long count = 0L;
                                long countSpalla = 0L;

                                label170:
                                while(true) {
                                    ProgrammaRientrato programmaAbbinato;
                                    label168:
                                    do {
                                        while(programmiAbbinati.hasNext()) {
                                            programmaAbbinato = (ProgrammaRientrato)programmiAbbinati.next();
                                            if (programmaAbbinato.getFlagPMPrincipale() != null && programmaAbbinato.getFlagPMPrincipale() != 'Y' && programmaAbbinato.getFlagPMPrincipale() != '1') {
                                                continue label168;
                                            }

                                            ++count;
                                        }

                                        rigaElaborata.setNumeroPmAttesiEventoVoce(count);
                                        rigaElaborata.setNumeroPmAttesiEventoVoceSpalla(countSpalla);
                                        break label170;
                                    } while(programmaAbbinato.getFlagPMPrincipale() != 'N' && programmaAbbinato.getFlagPMPrincipale() != '0');

                                    ++countSpalla;
                                }
                            }

                            listaPMInviatiSIADA = elaborazione.getListaRigaElaborataEventoVoce(movimento.getIdEvento(), movimento.getVoceIncasso());
                            if (listaPMInviatiSIADA != null && listaPMInviatiSIADA.size() > 0) {
                                RigaElaborata eventoVocePagato = (RigaElaborata)listaPMInviatiSIADA.get(0);
                                rigaElaborata.setNumeroPmRientratiEventoVoce(eventoVocePagato.getNumeroPmRientratiEventoVoce());
                                rigaElaborata.setListaPmRientratiEventoVoce(eventoVocePagato.getListaPmRientratiEventoVoce());
                                if (eventoVocePagato.getNumeroPmRientratiEventoVoce() != null && eventoVocePagato.getNumeroPmRientratiEventoVoce() == 0L) {
                                    programmiRientrati = this.programmaMusicaleDAO.getProgrammiRientratiEventoVoce(movimento.getIdEvento(), movimento.getVoceIncasso(), dataInizioRicalcolo, dataFineRicalcolo, tipologiaPM);
                                    rigaElaborata.setNumeroPmRientratiEventoVoce(new Long((long)programmiRientrati.size()));
                                    rigaElaborata.setListaPmRientratiEventoVoce(programmiRientrati);
                                }
                            } else {
                                programmiRientrati = this.programmaMusicaleDAO.getProgrammiRientratiEventoVoce(movimento.getIdEvento(), movimento.getVoceIncasso(), dataInizioRicalcolo, dataFineRicalcolo, tipologiaPM);
                                rigaElaborata.setNumeroPmRientratiEventoVoce(new Long((long)programmiRientrati.size()));
                                rigaElaborata.setListaPmRientratiEventoVoce(programmiRientrati);
                            }

                            TrattamentoPM tipoVoceIncasso = (TrattamentoPM)mappaVociIncasso.get(movimento.getVoceIncasso());
                            this.eseguiRicalcoloProQuota2243(rigaElaborata);
                        } else {
                            listaPMInviatiSIADA = this.gestioneSiadaDAO.getMovimentoSiada(movimento.getIdEvento(), movimento.getVoceIncasso(), dataFineUltimaRipartizione.intValue());
                            if (listaPMInviatiSIADA != null && listaPMInviatiSIADA.size() > 0) {
                                Double importoRipartito = 0.0;
                                Double importoTotale = 0.0;
                                List<EventiPagati> listaPagamentiRicevuti = this.programmaMusicaleDAO.getListaPagamentiEventoVoce(movimento.getIdEvento(), movimento.getVoceIncasso());
                                if (listaPagamentiRicevuti != null && listaPagamentiRicevuti.size() > 0) {
                                    int w;
                                    for(w = 0; w < listaPagamentiRicevuti.size(); ++w) {
                                        importoTotale = importoTotale + ((EventiPagati)listaPagamentiRicevuti.get(w)).getImportDem();
                                    }

                                    for(w = 0; w < listaPMInviatiSIADA.size(); ++w) {
                                        importoRipartito = importoRipartito + ((MovimentoSiada)listaPMInviatiSIADA.get(w)).getImporto();
                                    }

                                    if (importoTotale > importoRipartito) {
                                        importoResiduoPM = importoTotale - importoRipartito;
                                        isPMGiaRipartito = true;
                                    }
                                }
                            } else {
                                rigaElaborata.setRigaSospesa("EVENTO_IMPORTO_INESISTENTE");
                            }
                        }
                    }

                    if (rigaElaborata.getRigaSospesa() != null) {
                        this.log.info(new Date() + " - id elaborazione: " + idEsecuzioneRicalcolo + ". [" + i + "] (" + movimento.getIdEvento() + "|" + movimento.getNumeroFattura() + "|" + movimento.getVoceIncasso() + ") PM: " + movimento.getIdProgrammaMusicale() + " SOSPESO1");
                        this.programmaMusicaleDAO.insertSospesiRicalcolo(movimento.getIdEvento(), movimento.getNumeroFattura(), movimento.getVoceIncasso(), idEsecuzioneRicalcolo, movimento.getIdProgrammaMusicale(), movimento.getId(), rigaElaborata.getRigaSospesa());
                        this.getInformazioniElaborazione().setTotaleSospesi(new Long((long)j));
                        ++j;
                    } else if (isPMGiaRipartito) {
                        this.log.info(new Date() + " - id elaborazione: " + idEsecuzioneRicalcolo + ". [" + i + "] (" + movimento.getIdEvento() + "|" + movimento.getNumeroFattura() + "|" + movimento.getVoceIncasso() + ") Movimento: " + movimento.getId() + " PM: " + movimento.getIdProgrammaMusicale() + " Importo SUN: " + movimento.getImportoTotDemLordo() + " Importo Ricalcolato: " + importoResiduoPM);
                        this.programmaMusicaleDAO.insertImportoRicalcolato(movimento.getIdEvento(), movimento.getNumeroFattura(), movimento.getVoceIncasso(), idEsecuzioneRicalcolo, movimento.getId(), importoResiduoPM, (Double)null);
                    } else {
                        ProgrammaRientrato programmaRielaborato = rigaElaborata.getPmRientratoEventoVoce(movimento.getIdProgrammaMusicale());
                        this.log.info(new Date() + " - id elaborazione: " + idEsecuzioneRicalcolo + ". [" + i + "] (" + movimento.getIdEvento() + "|" + movimento.getNumeroFattura() + "|" + movimento.getVoceIncasso() + ") Movimento: " + movimento.getId() + " PM: " + movimento.getIdProgrammaMusicale() + " Importo SUN: " + movimento.getImportoTotDemLordo() + " Importo Ricalcolato: " + programmaRielaborato.getImportoRicalcolato());
                        this.programmaMusicaleDAO.insertImportoRicalcolato(movimento.getIdEvento(), movimento.getNumeroFattura(), movimento.getVoceIncasso(), idEsecuzioneRicalcolo, movimento.getId(), programmaRielaborato.getImportoRicalcolato(), programmaRielaborato.getImportoSingolaCedola());
                    }
                } catch (Exception var41) {
                    String description = null;
                    StringWriter sw;
                    PrintWriter pw;
                    if (rigaElaborata != null) {
                        description = "Error - id elaborazione: " + idEsecuzioneRicalcolo + " Key: (Evento=" + rigaElaborata.getIdEvento() + "|Fattura=" + rigaElaborata.getNumeroFattura() + "|Voce=" + rigaElaborata.getVoceIncasso() + "): " + var41.getMessage();
                        this.log.info(description);

                        try {
                            this.programmaMusicaleDAO.insertSospesiRicalcolo(movimento.getIdEvento(), movimento.getNumeroFattura(), movimento.getVoceIncasso(), idEsecuzioneRicalcolo, movimento.getIdProgrammaMusicale(), movimento.getId(), "ERRORE_ELABORAZIONE: " + description);
                        } catch (Exception var38) {
                            var38.printStackTrace();
                        }

                        rigaElaborata.setRigaSospesa("ERRORE");
                        this.getInformazioniElaborazione().setTotaleErrori(new Long((long)k));
                        ++k;
                        sw = new StringWriter();
                        pw = new PrintWriter(sw);
                        var41.printStackTrace(pw);

                        try {
                            this.traceService.trace("ValorizzatoreInstance2243_" + engineIndex, this.getClass().getName(), "startElaborazioneRicalcolo2243", description + " \n " + sw.toString(), "ERROR");
                        } catch (Exception var37) {
                            var37.printStackTrace();
                        }
                    } else {
                        description = "Error - id elaborazione: " + idEsecuzioneRicalcolo + ". Eccezione: " + var41.getMessage();
                        this.log.info(description);

                        try {
                            this.programmaMusicaleDAO.insertSospesiRicalcolo(movimento.getIdEvento(), movimento.getNumeroFattura(), movimento.getVoceIncasso(), idEsecuzioneRicalcolo, movimento.getIdProgrammaMusicale(), movimento.getId(), "ERRORE_ELABORAZIONE: " + description);
                        } catch (Exception var36) {
                            var36.printStackTrace();
                        }

                        this.getInformazioniElaborazione().setTotaleErrori(new Long((long)k));
                        ++k;
                        sw = new StringWriter();
                        pw = new PrintWriter(sw);
                        var41.printStackTrace(pw);

                        try {
                            this.traceService.trace("ValorizzatoreInstance2243_" + engineIndex, this.getClass().getName(), "startElaborazioneRicalcolo2243", description + " \n " + sw.toString(), "ERROR");
                        } catch (Exception var35) {
                            var35.printStackTrace();
                        }
                    }
                }

                elaborazione.addRigaElaborata(rigaElaborata);
                this.getInformazioniElaborazione().setTotaleElaborati(new Long((long)i));
            }

            if (((List)movimenti).size() == 0) {
                this.getInformazioniElaborazione().setTotaleElaborati(new Long(-1L));
            }
        }

        this.esecuzioneDAO.endEsecuzioneRicalcolo(idEsecuzioneRicalcolo, esitoRicalcolo.getError(), esitoRicalcolo.getDescription());
        this.getInformazioniElaborazione().setTerminate(true);
        this.log.info(new Date() + " - id elaborazione: " + idEsecuzioneRicalcolo + ". Ended ValorizzatoreInstance ricalcolo");
        this.traceService.trace("ValorizzatoreInstance" + engineIndex, this.getClass().getName(), "startElaborazioneRicalcolo2243", "Id elaborazione: " + idEsecuzioneRicalcolo + ". Ended ValorizzatoreInstance ricalcolo", "INFO");
        return esitoRicalcolo;
    }

    public InformazioniElaborazione getInformazioniElaborazione() {
        if (this.informazioniElaborazione == null) {
            this.informazioniElaborazione = new InformazioniElaborazione();
        }

        return this.informazioniElaborazione;
    }

    public void suspend() {
        this.getInformazioniElaborazione().setSuspende(true);
    }

    @Transactional
    public void resume() {
        this.getInformazioniElaborazione().setSuspende(false);
        this.notify();
    }
}
