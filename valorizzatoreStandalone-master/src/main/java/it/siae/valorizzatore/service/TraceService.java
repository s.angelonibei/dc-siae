package it.siae.valorizzatore.service;

import it.siae.valorizzatore.dao.IEsecuzioneDAO;
import it.siae.valorizzatore.model.TracciamentoApplicativo;
import it.siae.valorizzatore.utility.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;

@Service("traceService")
public class TraceService implements ITraceService {

    @Autowired
    @Qualifier("esecuzioneDAO")
    IEsecuzioneDAO esecuzioneDAO;

    public void trace(String serviceName, String className, String methodName, String message, String logLevel){

        String hostName = " ";

        try {

            hostName = InetAddress.getLocalHost().getHostName();

        }catch(UnknownHostException e){
            e.printStackTrace();
        }

        TracciamentoApplicativo tracciamentoApplicativo = new TracciamentoApplicativo();

        tracciamentoApplicativo.setApplication(Constants.APPID);


        tracciamentoApplicativo.setDataInserimento(new Date());
        tracciamentoApplicativo.setUserName(null);
        tracciamentoApplicativo.setHostName(hostName);
        tracciamentoApplicativo.setSessionId(null);

        tracciamentoApplicativo.setLogLevel(logLevel);
        tracciamentoApplicativo.setServiceName(serviceName);
        tracciamentoApplicativo.setClassName(className);
        tracciamentoApplicativo.setMethodName(methodName);
        tracciamentoApplicativo.setMessage(message);

        esecuzioneDAO.trace(tracciamentoApplicativo);

    }

}
