//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package it.siae.valorizzatore.service;

import it.siae.valorizzatore.dao.IConfigurationDAO;
import it.siae.valorizzatore.dao.IGestioneSiadaDAO;
import it.siae.valorizzatore.model.EsecuzioneRicalcolo;
import it.siae.valorizzatore.model.InformazioniElaborazione;
import it.siae.valorizzatore.model.MovimentoSiada;
import it.siae.valorizzatore.model.MovimentoSiada2243;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("gestioneSiadaService")
public class GestioneSiadaService implements IGestioneSiadaService {
    @Autowired
    @Qualifier("configurationDAO")
    IConfigurationDAO configurationDAO;
    @Autowired
    @Qualifier("gestioneSiadaDAO")
    IGestioneSiadaDAO gestioneSiadaDAO;
    @Autowired
    @Qualifier("traceService")
    private ITraceService traceService;
    public InformazioniElaborazione informazioniElaborazione;
    protected Logger log = Logger.getLogger(this.getClass());

    public GestioneSiadaService() {
    }

    @Transactional
    public InformazioniElaborazione startAggregazione(int engineIndex, Long elaborationId) {
        String intervalloMovimenti = this.configurationDAO.getParameter("AGGREGATE_INTERVAL_" + engineIndex);
        String[] minMaxIntervel = intervalloMovimenti.split("-");
        int minInterval = Integer.parseInt(minMaxIntervel[0]);
        int maxInterval = Integer.parseInt(minMaxIntervel[1]);

        try {
            Thread.currentThread();
            Thread.sleep(2000L);
        } catch (Exception var12) {
        }

        this.traceService.trace("GestioneSiadaService", this.getClass().getName(), "startAggregazione", "Esecuzione da aggregare (Engine " + engineIndex + "): " + elaborationId, "INFO");
        EsecuzioneRicalcolo esecuzione = this.gestioneSiadaDAO.getEsecuzioneDaAggregare(elaborationId);
        if (esecuzione != null) {
            //int i = false;

            try {
                int i = 1;
                List<MovimentoSiada> movimentazioniAggregate = this.gestioneSiadaDAO.getMovimentazioniAggregate(esecuzione, minInterval, maxInterval);
                this.getInformazioniElaborazione().setTotalePM(new Long((long)movimentazioniAggregate.size()));
                this.traceService.trace("GestioneSiadaService", this.getClass().getName(), "startAggregazione", "Totale movimentazioni da aggregare (Engine " + engineIndex + "): " + movimentazioniAggregate.size(), "INFO");

                for(Iterator var15 = movimentazioniAggregate.iterator(); var15.hasNext(); ++i) {
                    MovimentoSiada movimentoAggregato = (MovimentoSiada)var15.next();
                    this.gestioneSiadaDAO.insertMovimentoSiada(movimentoAggregato);
                    this.getInformazioniElaborazione().setTotaleElaborati(new Long((long)i));
                }

                if (i % 100 == 0) {
                    this.traceService.trace("GestioneSiadaService", this.getClass().getName(), "startAggregazione", "Movimenti aggregati (Engine " + engineIndex + "): " + i, "INFO");
                }
            } catch (Exception var13) {
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                var13.printStackTrace(pw);
                this.traceService.trace("GestioneSiadaService", this.getClass().getName(), "startAggregazione (Engine " + engineIndex + ")", sw.toString(), "ERROR");
            }
        }

        this.log.info("Aggregazione terminata. Elaborati " + this.getInformazioniElaborazione().getTotaleElaborati() + " su " + this.getInformazioniElaborazione().getTotalePM());
        this.traceService.trace("GestioneSiadaService", this.getClass().getName(), "startAggregazione", "Aggregazione terminata (Engine " + engineIndex + "). Elaborati " + this.getInformazioniElaborazione().getTotaleElaborati() + " su " + this.getInformazioniElaborazione().getTotalePM(), "INFO");
        return this.informazioniElaborazione;
    }

    @Transactional
    public InformazioniElaborazione startAggregazione2243(int engineIndex, Long elaborationId) {
        String intervalloMovimenti = this.configurationDAO.getParameter("AGGREGATE_INTERVAL_2243_" + engineIndex);
        String[] minMaxIntervel = intervalloMovimenti.split("-");
        int minInterval = Integer.parseInt(minMaxIntervel[0]);
        int maxInterval = Integer.parseInt(minMaxIntervel[1]);

        try {
            Thread.currentThread();
            Thread.sleep(2000L);
        } catch (Exception var12) {
        }

        this.traceService.trace("GestioneSiadaService2243", this.getClass().getName(), "startAggregazione2243", "Esecuzione da aggregare (Engine " + engineIndex + "): " + elaborationId, "INFO");
        EsecuzioneRicalcolo esecuzione = this.gestioneSiadaDAO.getEsecuzioneDaAggregare(elaborationId);
        if (esecuzione != null) {
           // int i = false;

            try {
                int i = 1;
                List<MovimentoSiada2243> movimentazioniAggregate = this.gestioneSiadaDAO.getMovimentazioniAggregate2243(esecuzione, minInterval, maxInterval);
                this.getInformazioniElaborazione().setTotalePM(new Long((long)movimentazioniAggregate.size()));
                this.traceService.trace("GestioneSiadaService2243", this.getClass().getName(), "startAggregazione2243", "Totale movimentazioni da aggregare (Engine " + engineIndex + "): " + movimentazioniAggregate.size(), "INFO");

                for(Iterator var15 = movimentazioniAggregate.iterator(); var15.hasNext(); ++i) {
                    MovimentoSiada2243 movimentoAggregato2243 = (MovimentoSiada2243)var15.next();
                    this.gestioneSiadaDAO.insertMovimentoSiada2243(movimentoAggregato2243);
                    this.getInformazioniElaborazione().setTotaleElaborati(new Long((long)i));
                }

                if (i % 100 == 0) {
                    this.traceService.trace("GestioneSiadaService2243", this.getClass().getName(), "startAggregazione2243", "Movimenti aggregati (Engine " + engineIndex + "): " + i, "INFO");
                }
            } catch (Exception var13) {
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                var13.printStackTrace(pw);
                this.traceService.trace("GestioneSiadaService2243", this.getClass().getName(), "startAggregazione2243 (Engine " + engineIndex + ")", sw.toString(), "ERROR");
            }
        }

        this.log.info("Aggregazione terminata. Elaborati " + this.getInformazioniElaborazione().getTotaleElaborati() + " su " + this.getInformazioniElaborazione().getTotalePM());
        this.traceService.trace("GestioneSiadaService", this.getClass().getName(), "startAggregazione2243", "Aggregazione terminata (Engine " + engineIndex + "). Elaborati " + this.getInformazioniElaborazione().getTotaleElaborati() + " su " + this.getInformazioniElaborazione().getTotalePM(), "INFO");
        return this.informazioniElaborazione;
    }

    public List<MovimentoSiada> getMovimentoSiada(Long idEvento, String voceIncasso, Integer fineUltimaRipartizione) {
        return this.gestioneSiadaDAO.getMovimentoSiada(idEvento, voceIncasso, fineUltimaRipartizione);
    }

    public InformazioniElaborazione getInformazioniElaborazione() {
        if (this.informazioniElaborazione == null) {
            this.informazioniElaborazione = new InformazioniElaborazione();
        }

        return this.informazioniElaborazione;
    }
}
