package com.alkemytech.sophia.uniload;

import java.io.IOException;
import java.util.Properties;

import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class GenericSaleLine implements SaleLine {

	private final String idUtil;			// id utilizzazione sophia
	private final String proprietaryId;		// 0rojbLrI4NCPE3rWUmCG8O
	private final String title;				// Up Around The Bend
	private final String artists;			// John Fogerty|creedence clearwater revival
	private final String roles;				// Composer|MainArtist
	private final String albumTitle;		// Cosmo's Factory
	private final String iswc;
	private final String isrc;
	private final String salesCount;		// 2

	// fix per codifica manuale 14.02.2019
	private final String siadaTitle;		// UP AROUND THE BEND
	private final String siadaArtists;	 	// JOHN FOGERTY|CREEDENCE CLEARWATER REVIVAL

	private final CSVRecord record;
	
	public GenericSaleLine(Properties properties, CSVRecord record) {
		super();
		this.record = record;
		this.idUtil = safeGet(record, Integer.parseInt(properties.getProperty("input.column.idUtil", "8")), "");
		this.title = safeGet(record, Integer.parseInt(properties.getProperty("input.column.title", "3")), "");
		this.artists = safeGet(record, Integer.parseInt(properties.getProperty("input.column.artists", "5")), "");
		this.roles = safeGet(record, Integer.parseInt(properties.getProperty("input.column.roles", "9")), "");
		this.albumTitle = safeGet(record, Integer.parseInt(properties.getProperty("input.column.albumTitle", "10")), "");
		this.proprietaryId = safeGet(record, Integer.parseInt(properties.getProperty("input.column.proprietaryId", "0")), "");
		this.iswc = safeGet(record, Integer.parseInt(properties.getProperty("input.column.iswc", "1")), "").replaceAll("[^A-Za-z0-9]", "");
		this.isrc = safeGet(record, Integer.parseInt(properties.getProperty("input.column.isrc", "2")), "").replaceAll("[^A-Za-z0-9]", "");
		this.salesCount = safeGet(record, Integer.parseInt(properties.getProperty("input.column.salesCount", "11")), "0");
		this.siadaTitle = safeGet(record, Integer.parseInt(properties.getProperty("input.column.siadaTitle", "4")), "");
		this.siadaArtists = safeGet(record, Integer.parseInt(properties.getProperty("input.column.siadaArtists", "6")), "");
	}

	private String safeGet(CSVRecord record, int index, String defaultValue) {
		try {
			return record.get(index);
		} catch (IndexOutOfBoundsException e) {
			return defaultValue;
		}
	}
	
	public CSVRecord getRecord() {
		return record;
	}
	
	@Override
	public String getProprietaryId() {
		return proprietaryId;
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public String getArtists() {
		return artists;
	}

	@Override
	public String getRoles() {
		return roles;
	}

	@Override
	public String getIdUtil() {
		return idUtil;
	}

	@Override
	public String getAlbumTitle() {
		return albumTitle;
	}

	@Override
	public String getIswc() {
		return iswc;
	}

	@Override
	public String getIsrc() {
		return isrc;
	}

	@Override
	public String getSalesCount() {
		return salesCount;
	}

	@Override
	public String getSiadaTitle() {
		return siadaTitle;
	}

	@Override
	public String getSiadaArtists() {
		return siadaArtists;
	}

	@Override
	public void print(CSVPrinter printer) throws IOException {
		for (String field : record) {
			printer.print(field);
		}
	}

	@Override
	public String toString() {
		return "GenericSaleLine [idUtil=" + idUtil + ", proprietaryId="
				+ proprietaryId + ", title=" + title + ", artists=" + artists
				+ ", roles=" + roles + ", albumTitle=" + albumTitle + ", iswc="
				+ iswc + ", isrc=" + isrc + ", salesCount=" + salesCount
				+ ", siadaTitle=" + siadaTitle + ", siadaArtists=" + siadaArtists
				+ ", record=" + record + "]";
	}
	
}
