package com.alkemytech.sophia.uniload.tools;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.knallgrau.utils.textcat.TextCategorizer;

import com.carrotsearch.labs.langid.DetectedLanguage;
import com.carrotsearch.labs.langid.LangIdV3;

public class Debug {

	public static void main(String[] args) throws Exception {
		
		LangIdV3 langIdV3 = new LangIdV3();
		
		final String[] titles = {
//				"Mike McCandless rocks the boat.",
//				"W Szczebrzeszynie chrząszcz brzmi w trzcinie",
//				"Piano italiano per la crescita: negoziato in Europa sugli investimenti «virtuosi»",
//				"Bella ciao",
				
				"Hello, Hello",
				"Llámame Fino",
				"Tu verano mi invierno",
				"Busco una chica",
				"Ni un Segundo",
				"",
				"",
				
//				"Boom (feat. MOTi, TY Dolla $ign, Wizkid & Kranium)",
//				"Uno X Uno",
//				"Dessert",
//				"Paper Planes",
//				"Bailando - Spanish Version",
//				"Hypnotize",
//				"My Head Is A Jungle - MK Remix / Radio Edit",
//				"Arabella",
//				"Hollaback Girl",
		};
		
		
		

		
//		//load all languages:
//		List<LanguageProfile> languageProfiles = new LanguageProfileReader().readAllBuiltIn();
//		//build language detector:
//		LanguageDetector languageDetector = LanguageDetectorBuilder.create(NgramExtractors.standard())
//		        .withProfiles(languageProfiles)
//		        .build();
//		//create a text object factory
//		TextObjectFactory textObjectFactory = CommonTextObjectFactories.forDetectingShortCleanText();
//		//query
//		for (String title : titles) {
//			TextObject textObject = textObjectFactory.forText(title);
//			Optional<LdLocale> lang = languageDetector.detect(textObject);
//			System.out.println(title);
//			System.out.println("\t" + (lang.isPresent() ? lang.get() : null));
//			
//		}
//
//		System.out.println();
//		System.out.println();
//		System.out.println();

		
		
		
		TextCategorizer textCategorizer = new TextCategorizer();
		for (String title : titles) {
			final String result = textCategorizer.categorize(title);
			System.out.println(title);
			System.out.println("\t" + result);
			
		}
		
		System.out.println();
		System.out.println();
		System.out.println();
		
		
		for (String title : titles) {
			langIdV3.reset();
			DetectedLanguage language = langIdV3.classify(title, true);
			System.out.println(title);
			System.out.println("\t" + language.getLangCode() + " - " + language.getConfidence());
			
			List<DetectedLanguage> languages = new ArrayList<DetectedLanguage>(langIdV3.rank(true));
			Collections.sort(languages, new Comparator<DetectedLanguage>() {
				@Override
				public int compare(DetectedLanguage o1, DetectedLanguage o2) {
					return -Double.compare(o1.getConfidence(), o2.getConfidence());
				}
			});
			System.out.println("\t" + languages);
		}
		
	}
	
}
