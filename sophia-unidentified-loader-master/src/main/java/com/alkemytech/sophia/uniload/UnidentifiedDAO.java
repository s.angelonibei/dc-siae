package com.alkemytech.sophia.uniload;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.common.tools.StringTools;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class UnidentifiedDAO {

	private final Logger logger = LoggerFactory.getLogger(UnidentifiedDAO.class);

	private final Connection jdbc;
	
	public UnidentifiedDAO(Connection jdbc) {
		super();
		this.jdbc = jdbc;
	}
	
	public String hashId(SaleLine line) {
		try {
			return Hex.encodeHexString(MessageDigest.getInstance("SHA1")
					.digest((line.getTitle() + line.getArtists()).getBytes(StandardCharsets.UTF_8)));
		} catch (NoSuchAlgorithmException e) {
			return null;
		}
	}
	
	public int updateUnidentified(SaleLine line) throws Exception {
		
		if (StringTools.isNullOrEmpty(line.getSalesCount())) {
			return 1;
		}
		
		jdbc.setAutoCommit(true);
		final String sql = new StringBuffer()
			.append("update unidentified_song")
			.append(" set priority = priority + ?")
			.append(" where hash_id = ?")
			.toString();
		final PreparedStatement stmt = jdbc.prepareStatement(sql);
		int i = 1;
		stmt.setLong(i++, Math.round(Double.parseDouble(line.getSalesCount())));
		stmt.setString(i++, hashId(line));
		int count = 0;
		try {
			count = stmt.executeUpdate();
		} finally {
			stmt.close();
		}
		return count;
		
	}
	
	public int insertUnidentified(SaleLine line, long priority) throws Exception {
		
		jdbc.setAutoCommit(true);
		final String hashId = hashId(line);
		final String sql = new StringBuffer()
			.append("insert into unidentified_song ")
			.append("( hash_id")
			.append(", title")
			.append(", artists")
			.append(", siada_title")
			.append(", siada_artists")
			.append(", roles")
			.append(", priority")
			.append(", insert_time")
			.append(", last_auto_time")
			.append(", last_manual_time")
			.append(") values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
			.toString();
		final PreparedStatement stmt = jdbc.prepareStatement(sql);
		int i = 1;
		stmt.setString(i++, hashId);
		stmt.setString(i++, line.getTitle());
		stmt.setString(i++, line.getArtists());
		stmt.setString(i++, line.getSiadaTitle());
		stmt.setString(i++, line.getSiadaArtists());
		stmt.setString(i++, line.getRoles());
		stmt.setLong(i++, priority);
		stmt.setLong(i++, System.currentTimeMillis());
		stmt.setLong(i++, 0L);
		stmt.setLong(i++, 0L);
		int count = -1;
		try {
			count = stmt.executeUpdate();
		} catch (SQLException e) {
			if (1062 == e.getErrorCode()) {
				count = 0;
				logger.debug("insertUnidentified: duplicate entry {}", hashId);				
			} else {
				logger.debug("insertUnidentified: sql exception {}", e.getErrorCode());				
				logger.error("insertUnidentified", e);
			}
		} finally {
			stmt.close();
		}
		return count;
		
	}
	
	public int insertUnidentifiedDsr(String iddsr, String dsp, SaleLine line) throws Exception {
					
		jdbc.setAutoCommit(true);
		final String sql = new StringBuffer()
			.append("insert into unidentified_song_dsr ")
			.append("( id_util")
			.append(", hash_id")
			.append(", id_dsr")
			.append(", album_title")
			.append(", proprietary_id")
			.append(", isrc")
			.append(", iswc")
			.append(", sales_count")
			.append(", dsp")
			.append(", insert_time")
			.append(") values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
			.toString();
		final PreparedStatement stmt = jdbc.prepareStatement(sql);
		int i = 1;
		stmt.setString(i++, line.getIdUtil());
		stmt.setString(i++, hashId(line));
		stmt.setString(i++, iddsr);
		stmt.setString(i++, line.getAlbumTitle());
		stmt.setString(i++, line.getProprietaryId());
		stmt.setString(i++, line.getIsrc());
		stmt.setString(i++, line.getIswc());
		if (StringTools.isNullOrEmpty(line.getSalesCount())) {
			stmt.setNull(i++, Types.BIGINT);
		} else {
			stmt.setLong(i++, Math.round(Double.parseDouble(line.getSalesCount())));
		}
		stmt.setString(i++, dsp);
		stmt.setLong(i++, System.currentTimeMillis());
		int count = -1;
		try {
			count = stmt.executeUpdate();
		} catch (SQLException e) {
			if (1062 == e.getErrorCode()) {
				count = 0;
				logger.debug("insertUnidentifiedDsr: duplicate entry {}", line.getIdUtil());				
			} else {
				logger.debug("insertUnidentifiedDsr: sql exception {}", e.getErrorCode());				
				logger.debug("insertUnidentifiedDsr: line: {}", line.toString());
				logger.error("insertUnidentifiedDsr", e);
			}
		} finally {
			stmt.close();
		}
		return count;
		
	}
	
	public int deleteUnidentifiedDsr(SaleLine line) throws Exception {
		
		jdbc.setAutoCommit(true);
		final String sql = new StringBuffer()
			.append("delete from unidentified_song_dsr ")
			.append(" where id_util = ?")
			.toString();
		final PreparedStatement stmt = jdbc.prepareStatement(sql);
		int i = 1;
		stmt.setString(i++, line.getIdUtil());
		int count = 0;
		try {
			count = stmt.executeUpdate();
		} finally {
			stmt.close();
		}
		return count;
		
	}
	
}
