package com.alkemytech.sophia.uniload;

import java.io.IOException;

import org.apache.commons.csv.CSVPrinter;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public interface SaleLine {

	public String getProprietaryId();
	public String getTitle();
	public String getArtists();
	public String getRoles();
	public String getIdUtil();
	public String getAlbumTitle();
	public String getIswc();
	public String getIsrc();
	public String getSalesCount();
	public String getSiadaTitle();
	public String getSiadaArtists();
	public void print(CSVPrinter printer) throws IOException ;
	
}
