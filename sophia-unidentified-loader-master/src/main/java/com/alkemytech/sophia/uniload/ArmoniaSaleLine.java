package com.alkemytech.sophia.uniload;

import java.io.IOException;

import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

/**
 * Warning: il numero tracce album si calcola sommando le righe che hanno uguale albumProprietaryId e transactionId
 * 
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class ArmoniaSaleLine implements SaleLine {

	// Armonia DSR columns
	// 0-9
	private String dsrId;
	private String territory;				// IT
	private String transactionId;			// 1
	private String albumTitle;				// Cosmo's Factory
	private String albumProprietaryId;		// 0rojbLrI4NCPE3rWUmCG8O
	private String proprietaryId;			// 0rojbLrI4NCPE3rWUmCG8O
	private String title;					// Up Around The Bend
	private String artists;					// John Fogerty|creedence clearwater revival
	private String ipnNameNumbers;
	private String roles;					// Composer|MainArtist
	// 10-19
	private String iswc;
	private String label;
	private String isrc;
	private String currency;				// EUR
	private String commercialModel;			// SubscriptionModel
	private String unitaryPrice;			// 0.00
	private String salesCount;				// 2
	private String societyCode;
	private String societyWorkCode;
	private String demRights;
	// 20-29
	private String drmRights;
	private String demRightsPublic;
	private String drmRightsPublic;
	private String amount;
	private String panEuropeanAgreement;
	private String societyReferenceCode;
	private String societyLicenseId;
	private String albumArtist;
	private String transactionType;			// OnDemandStream
	private String releaseType;				// SINGLE
	// 30-39
	private String upgradeFlag;				// 0
	private String drmFlag;
	private String upc;						// 00888072387652
	private String grid;
	private String trackMediaType;
	private String trackDuration;			// 161
	private String conflictType;
	private String waiveLength;
	private String wpbFlag;					// 1
	private String originalTitle;			// Up Around The Bend - Live In Amsterdam 9-10-71
	// 40-43
	private String originalArtists;			// John Fogerty|Creedence Clearwater Revival
	private String originalRoles;			// Composer|NotProvided
	private String originalIswc;
	private String originalIsrc;			// USC4R0817638
	
	public ArmoniaSaleLine() {
		super();
	}
	
	public ArmoniaSaleLine(CSVRecord record) {
		super();
		int i = 0;
		this.dsrId = record.get(i++);
		this.territory = record.get(i++);
		this.transactionId = record.get(i++);
		this.albumTitle = record.get(i++);
		this.albumProprietaryId = record.get(i++);
		this.proprietaryId = record.get(i++);
		this.title = record.get(i++);
		this.artists = record.get(i++);
		this.ipnNameNumbers = record.get(i++);
		this.roles = record.get(i++);
		this.iswc = record.get(i++);
		this.label = record.get(i++);
		this.isrc = record.get(i++);
		this.currency = record.get(i++);
		this.commercialModel = record.get(i++);
		this.unitaryPrice = record.get(i++);
		this.salesCount = record.get(i++);
		this.societyCode = record.get(i++);
		this.societyWorkCode = record.get(i++);
		this.demRights = record.get(i++);
		this.drmRights = record.get(i++);
		this.demRightsPublic = record.get(i++);
		this.drmRightsPublic = record.get(i++);
		this.amount = record.get(i++);
		this.panEuropeanAgreement = record.get(i++);
		this.societyReferenceCode = record.get(i++);
		this.societyLicenseId = record.get(i++);
		this.albumArtist = record.get(i++);
		this.transactionType = record.get(i++);
		this.releaseType = record.get(i++);
		this.upgradeFlag = record.get(i++);
		this.drmFlag = record.get(i++);
		this.upc = record.get(i++);
		this.grid = record.get(i++);
		this.trackMediaType = record.get(i++);
		this.trackDuration = record.get(i++);
		this.conflictType = record.get(i++);
		this.waiveLength = record.get(i++);
		this.wpbFlag = record.get(i++);
		this.originalTitle = record.get(i++);
		this.originalArtists = record.get(i++);
		this.originalRoles = record.get(i++);
		this.originalIswc = record.get(i++);
		this.originalIsrc = record.get(i++);
	}
	
	@Override
	public String getIdUtil() {
		throw new IllegalStateException();
	}

	public String getDsrId() {
		return dsrId;
	}
	public void setDsrId(String dsrId) {
		this.dsrId = dsrId;
	}
	public String getTerritory() {
		return territory;
	}
	public void setTerritory(String territory) {
		this.territory = territory;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getAlbumTitle() {
		return albumTitle;
	}
	public void setAlbumTitle(String albumTitle) {
		this.albumTitle = albumTitle;
	}
	public String getAlbumProprietaryId() {
		return albumProprietaryId;
	}
	public void setAlbumProprietaryId(String albumProprietaryId) {
		this.albumProprietaryId = albumProprietaryId;
	}
	@Override
	public String getProprietaryId() {
		return proprietaryId;
	}
	public void setProprietaryId(String proprietaryId) {
		this.proprietaryId = proprietaryId;
	}
	@Override
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	@Override
	public String getArtists() {
		return artists;
	}
	public void setArtists(String artists) {
		this.artists = artists;
	}
	public String getIpnNameNumbers() {
		return ipnNameNumbers;
	}
	public void setIpnNameNumbers(String ipnNameNumbers) {
		this.ipnNameNumbers = ipnNameNumbers;
	}
	@Override
	public String getRoles() {
		return roles;
	}
	public void setRoles(String roles) {
		this.roles = roles;
	}
	public String getIswc() {
		return iswc;
	}
	public void setIswc(String iswc) {
		this.iswc = iswc;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getIsrc() {
		return isrc;
	}
	public void setIsrc(String isrc) {
		this.isrc = isrc;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getCommercialModel() {
		return commercialModel;
	}
	public void setCommercialModel(String commercialModel) {
		this.commercialModel = commercialModel;
	}
	public String getUnitaryPrice() {
		return unitaryPrice;
	}
	public void setUnitaryPrice(String unitaryPrice) {
		this.unitaryPrice = unitaryPrice;
	}
	public String getSalesCount() {
		return salesCount;
	}
	public void setSalesCount(String salesCount) {
		this.salesCount = salesCount;
	}
	public String getSocietyCode() {
		return societyCode;
	}
	public void setSocietyCode(String societyCode) {
		this.societyCode = societyCode;
	}
	public String getSocietyWorkCode() {
		return societyWorkCode;
	}
	public void setSocietyWorkCode(String societyWorkCode) {
		this.societyWorkCode = societyWorkCode;
	}
	public String getDemRights() {
		return demRights;
	}
	public void setDemRights(String demRights) {
		this.demRights = demRights;
	}
	public String getDrmRights() {
		return drmRights;
	}
	public void setDrmRights(String drmRights) {
		this.drmRights = drmRights;
	}
	public String getDemRightsPublic() {
		return demRightsPublic;
	}
	public void setDemRightsPublic(String demRightsPublic) {
		this.demRightsPublic = demRightsPublic;
	}
	public String getDrmRightsPublic() {
		return drmRightsPublic;
	}
	public void setDrmRightsPublic(String drmRightsPublic) {
		this.drmRightsPublic = drmRightsPublic;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getPanEuropeanAgreement() {
		return panEuropeanAgreement;
	}
	public void setPanEuropeanAgreement(String panEuropeanAgreement) {
		this.panEuropeanAgreement = panEuropeanAgreement;
	}
	public String getSocietyReferenceCode() {
		return societyReferenceCode;
	}
	public void setSocietyReferenceCode(String societyReferenceCode) {
		this.societyReferenceCode = societyReferenceCode;
	}
	public String getSocietyLicenseId() {
		return societyLicenseId;
	}
	public void setSocietyLicenseId(String societyLicenseId) {
		this.societyLicenseId = societyLicenseId;
	}
	public String getAlbumArtist() {
		return albumArtist;
	}
	public void setAlbumArtist(String albumArtist) {
		this.albumArtist = albumArtist;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getReleaseType() {
		return releaseType;
	}
	public void setReleaseType(String releaseType) {
		this.releaseType = releaseType;
	}
	public String getUpgradeFlag() {
		return upgradeFlag;
	}
	public void setUpgradeFlag(String upgradeFlag) {
		this.upgradeFlag = upgradeFlag;
	}
	public String getDrmFlag() {
		return drmFlag;
	}
	public void setDrmFlag(String drmFlag) {
		this.drmFlag = drmFlag;
	}
	public String getUpc() {
		return upc;
	}
	public void setUpc(String upc) {
		this.upc = upc;
	}
	public String getGrid() {
		return grid;
	}
	public void setGrid(String grid) {
		this.grid = grid;
	}
	public String getTrackMediaType() {
		return trackMediaType;
	}
	public void setTrackMediaType(String trackMediaType) {
		this.trackMediaType = trackMediaType;
	}
	public String getTrackDuration() {
		return trackDuration;
	}
	public void setTrackDuration(String trackDuration) {
		this.trackDuration = trackDuration;
	}
	public String getConflictType() {
		return conflictType;
	}
	public void setConflictType(String conflictType) {
		this.conflictType = conflictType;
	}
	public String getWaiveLength() {
		return waiveLength;
	}
	public void setWaiveLength(String waiveLength) {
		this.waiveLength = waiveLength;
	}
	public String getWpbFlag() {
		return wpbFlag;
	}
	public void setWpbFlag(String wpbFlag) {
		this.wpbFlag = wpbFlag;
	}
	public String getOriginalTitle() {
		return originalTitle;
	}
	public void setOriginalTitle(String originalTitle) {
		this.originalTitle = originalTitle;
	}
	public String getOriginalArtists() {
		return originalArtists;
	}
	public void setOriginalArtists(String originalArtists) {
		this.originalArtists = originalArtists;
	}
	public String getOriginalRoles() {
		return originalRoles;
	}
	public void setOriginalRoles(String originalRoles) {
		this.originalRoles = originalRoles;
	}
	public String getOriginalIswc() {
		return originalIswc;
	}
	public void setOriginalIswc(String originalIswc) {
		this.originalIswc = originalIswc;
	}
	public String getOriginalIsrc() {
		return originalIsrc;
	}
	public void setOriginalIsrc(String originalIsrc) {
		this.originalIsrc = originalIsrc;
	}
	@Override
	public String getSiadaTitle() {
		return title;
	}
	@Override
	public String getSiadaArtists() {
		return artists;
	}
	@Override
	public void print(CSVPrinter printer) throws IOException {
		printer.print(dsrId);
		printer.print(territory);
		printer.print(transactionId);
		printer.print(albumTitle);
		printer.print(albumProprietaryId);
		printer.print(proprietaryId);
		printer.print(title);
		printer.print(artists);
		printer.print(ipnNameNumbers);
		printer.print(roles);
		printer.print(iswc);
		printer.print(label);
		printer.print(isrc);
		printer.print(currency);
		printer.print(commercialModel);
		printer.print(unitaryPrice);
		printer.print(salesCount);
		printer.print(societyCode);
		printer.print(societyWorkCode);
		printer.print(demRights);
		printer.print(drmRights);
		printer.print(demRightsPublic);
		printer.print(drmRightsPublic);
		printer.print(amount);
		printer.print(panEuropeanAgreement);
		printer.print(societyReferenceCode);
		printer.print(societyLicenseId);
		printer.print(albumArtist);
		printer.print(transactionType);
		printer.print(releaseType);
		printer.print(upgradeFlag);
		printer.print(drmFlag);
		printer.print(upc);
		printer.print(grid);
		printer.print(trackMediaType);
		printer.print(trackDuration);
		printer.print(conflictType);
		printer.print(waiveLength);
		printer.print(wpbFlag);
		printer.print(originalTitle);
		printer.print(originalArtists);
		printer.print(originalRoles);
		printer.print(originalIswc);
		printer.print(originalIsrc);
	}
	
}
