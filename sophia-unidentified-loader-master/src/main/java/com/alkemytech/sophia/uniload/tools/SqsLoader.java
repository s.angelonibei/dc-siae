package com.alkemytech.sophia.uniload.tools;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.util.Date;
import java.util.Properties;
import java.util.UUID;

import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.common.dao.DsrInfo;
import com.alkemytech.sophia.common.dao.McmdbDAO;
import com.alkemytech.sophia.common.sqs.Iso8601Helper;
import com.alkemytech.sophia.common.sqs.SQSService;
import com.alkemytech.sophia.uniload.GuiceModule;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.name.Named;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class SqsLoader {

	private static final Logger logger = LoggerFactory.getLogger(SqsLoader.class);

	private final Properties configuration;
	private final SQSService sqsService;
	private final DataSource mcmdbDS;

	@Inject
	protected SqsLoader(@Named("configuration") Properties configuration,
			SQSService sqsService,
			@Named("MCMDB") DataSource mcmdbDS) {
		super();
		this.configuration = configuration;
		this.sqsService = sqsService;
		this.mcmdbDS = mcmdbDS;
	}
	
	private String getHostName() {
		try {
			return InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			return "localhost";
		}
	}
	
	public void publishToSqs(String idDsr) throws Exception {
			
		try (final Connection mcmdb = mcmdbDS.getConnection()) {
			logger.debug("publishToSqs: connected to {} {}", mcmdb.getMetaData()
					.getDatabaseProductName(), mcmdb.getMetaData().getURL());

			// data access object
			final McmdbDAO dao = new McmdbDAO(mcmdb);

			// load dsr metadata
			final DsrInfo meta = dao.getDsrInfo(idDsr);
			logger.debug("publishToSqs: meta {}", meta);

			if (!StringUtils.isEmpty(meta.getFtpSourcePath()) &&
					!StringUtils.isEmpty(meta.getYear()) &&
					!StringUtils.isEmpty(meta.getMonth())) {

				// queue
				final String pendingQueue = configuration.getProperty("sqs.queue.pending");
				final String serviceBusQueueUrl = sqsService.getOrCreateUrl(configuration.getProperty("sqs.queue.service_bus"));
				
				logger.debug("publishToSqs: pendingQueue {}", pendingQueue);
				logger.debug("publishToSqs: serviceBusQueueUrl {}", serviceBusQueueUrl);

				// gson
				final Gson gson = new GsonBuilder()
					.disableHtmlEscaping()
					.setPrettyPrinting()
					.create();

				final JsonObject header = new JsonObject();
				header.addProperty("uuid", UUID.randomUUID().toString());				
				header.addProperty("timestamp", Iso8601Helper.format(new Date()));
				header.addProperty("sender", "orchestrator-dispatcher");
				header.addProperty("queue", pendingQueue);
				header.addProperty("hostname", getHostName());
				final JsonObject body = new JsonObject();
				body.addProperty("idDsr", idDsr);
				body.addProperty("dspCode", meta.getDspCode());
				body.addProperty("dsp", meta.getFtpSourcePath());
				body.addProperty("year", meta.getYear());
				body.addProperty("month", meta.getMonth());
				body.addProperty("country", meta.getCountry());
				body.addProperty("idUtilizationType", meta.getIdUtilizationType());
				final JsonObject message = new JsonObject();
				message.add("header", header);
				message.add("body", body);
				
				logger.debug("publishToSqs: message {}", gson.toJson(message));
				
				sqsService.sendMessage(serviceBusQueueUrl, gson.toJson(message));

			}
			
			logger.debug("publishToSqs: finished");
			
		}
	}

	public static void main(String[] args) throws Exception {
		try {	
			final Injector injector = Guice.createInjector(new GuiceModule(args));
			injector.getInstance(SQSService.class).startup();
			//injector.getInstance(S3Service.class).startup();
			try {
				injector.getInstance(SqsLoader.class)
					.publishToSqs(args[1]); // args[0] properties file, args[1] id dsr
			} finally {
				injector.getInstance(SQSService.class).shutdown();
				//injector.getInstance(S3Service.class).shutdown();
			}
		} catch (Exception e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}
	
}
