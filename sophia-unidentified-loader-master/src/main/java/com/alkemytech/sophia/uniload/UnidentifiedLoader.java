package com.alkemytech.sophia.uniload;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.SocketTimeoutException;
import java.net.URLDecoder;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

import javax.sql.DataSource;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.common.dao.DsrInfo;
import com.alkemytech.sophia.common.dao.McmdbDAO;
import com.alkemytech.sophia.common.rest.RestClient;
import com.alkemytech.sophia.common.s3.S3Service;
import com.alkemytech.sophia.common.sqs.Iso8601Helper;
import com.alkemytech.sophia.common.sqs.SQSMessage;
import com.alkemytech.sophia.common.sqs.SQSService;
import com.alkemytech.sophia.common.tools.StringTools;
import com.amazonaws.services.s3.AmazonS3URI;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Provider;
import com.google.inject.name.Named;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class UnidentifiedLoader {
	
	private static final Logger logger = LoggerFactory.getLogger(UnidentifiedLoader.class);

	private final Properties configuration;
	private final SQSService sqsService;
	private final S3Service s3Service;
	private final DataSource mcmdbDS;
	private final DataSource unidentifiedDS;
	private final Provider<LanguageGuess> languageGuessProvider;
	
	@Inject
	public UnidentifiedLoader(@Named("configuration") Properties configuration,
			SQSService sqsService,
			S3Service s3Service,
			Provider<LanguageGuess> languageGuessProvider,
			@Named("MCMDB") DataSource mcmdbDS,
			@Named("unidentified") DataSource unidentifiedDS) {
		super();
		this.configuration = configuration;
		this.sqsService = sqsService;
		this.s3Service = s3Service;
		this.languageGuessProvider = languageGuessProvider;
		this.mcmdbDS = mcmdbDS;
		this.unidentifiedDS = unidentifiedDS;
	}
	
	private String convert(Throwable e) {
		StringWriter sw = new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		return sw.toString();
	}

	private String getAsString(JsonObject json, String key) {
		final JsonElement element = null == json ? null : json.get(key);
		return null == element ? null : element.getAsString();
	}
	
	static class Stats {
		
		final AtomicLong rows = new AtomicLong(0);
		final AtomicLong italianLanguage = new AtomicLong(0);
		final AtomicLong italianIsrc = new AtomicLong(0);
		final AtomicLong skipped = new AtomicLong(0);
		final AtomicLong inserted = new AtomicLong(0);
		final AtomicLong updated = new AtomicLong(0);
		final AtomicLong boosted = new AtomicLong(0);
		final AtomicLong duplicated = new AtomicLong(0);
		
		Stats update (Stats other) {
			rows.addAndGet(other.rows.get());
			italianLanguage.addAndGet(other.italianLanguage.get());
			italianIsrc.addAndGet(other.italianIsrc.get());
			skipped.addAndGet(other.skipped.get());
			inserted.addAndGet(other.inserted.get());
			updated.addAndGet(other.updated.get());
			boosted.addAndGet(other.boosted.get());
			duplicated.addAndGet(other.duplicated.get());
			return this;
		}
		
		void addProperties(JsonObject output) {
			output.addProperty("totalRows", rows.get());
			output.addProperty("skippedRows", skipped.get());
			output.addProperty("boostedRows", boosted.get());
			output.addProperty("insertedSongs", inserted.get());
			output.addProperty("updatedSongs", updated.get());
			output.addProperty("duplicatedSongs", duplicated.get());
			output.addProperty("italianISRCs", italianIsrc.get());
			output.addProperty("italianTitles", italianLanguage.get());			
		}

	}
	
	private String getHostName() {
		try {
			return InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			return "localhost";
		}
	}
	
	private JsonObject formatFailedJson(JsonObject input, String code, String description, Exception e) {
		// send output message to failed queue
		final JsonObject header = new JsonObject();
		header.addProperty("uuid", UUID.randomUUID().toString());
		header.addProperty("timestamp", Iso8601Helper.format(new Date()));
		header.addProperty("queue", configuration.getProperty("sqs.queue.failed"));
		header.addProperty("sender", "unidentified-loader");
		header.addProperty("hostname", getHostName());
		final JsonObject error = new JsonObject();
		error.addProperty("code", code);
		error.addProperty("description", description);
		if (null != e) {
			error.addProperty("stackTrace", convert(e));
		}
		final JsonObject message = new JsonObject();
		message.add("header", header);
		message.add("input", input);
		message.add("error", error);
		return message;
	}
	
	private JsonObject formatStartedJson(JsonObject input) {
		final JsonObject header = new JsonObject();
		header.addProperty("uuid", UUID.randomUUID().toString());
		header.addProperty("timestamp", Iso8601Helper.format(new Date()));
		header.addProperty("queue", configuration.getProperty("sqs.queue.started"));
		header.addProperty("sender", "unidentified-loader");
		header.addProperty("hostname", getHostName());
		final JsonObject message = new JsonObject();
		message.add("header", header);
		message.add("input", input);
		return message;
	}
	
	private JsonObject formatCompletedJson(JsonObject input, JsonObject output) {
		final JsonObject header = new JsonObject();
		header.addProperty("uuid", UUID.randomUUID().toString());
		header.addProperty("timestamp", Iso8601Helper.format(new Date()));
		header.addProperty("queue", configuration.getProperty("sqs.queue.completed"));
		header.addProperty("sender", "unidentified-loader");
		header.addProperty("hostname", getHostName());
		final JsonObject message = new JsonObject();
		message.add("header", header);
		message.add("input", input);
		message.add("output", output);
		return message;
	}

	private boolean isInvoiced(String idDsr) {
		try {
			final RestClient.Result result = RestClient
					.get(configuration.getProperty("invoiced.restUrl").replace("{idDsr}", idDsr));
			if (200 == result.getStatusCode()) {
				final JsonObject resultJson = result.getJsonElement().getAsJsonObject();
				logger.debug("isInvoiced: resultJson {}", resultJson);
				return "true".equalsIgnoreCase(getAsString(resultJson, "invoiced"));
			}
		} catch (Exception e) {
			logger.error("isInvoiced", e);
		}
		return false;
	}
	
	/**
	 * Process all queued DSRs
	 */
	public void loadUnidentified() throws Exception {

		// connect to db
		try (final ServerSocket socket = new ServerSocket(Integer.parseInt(configuration.getProperty("listenPort", "0")));
				final Connection mcmdb = mcmdbDS.getConnection()) {
			logger.info("loadUnidentified: listening on {}", socket.getLocalSocketAddress());				
			logger.info("loadUnidentified: connected to {} {}", mcmdb.getMetaData()
					.getDatabaseProductName(), mcmdb.getMetaData().getURL());
			
			// data access object
			final McmdbDAO mcmdbDAO = new McmdbDAO(mcmdb);
			
			// GSON
			final Gson gson = new GsonBuilder()
				.disableHtmlEscaping()
				.setPrettyPrinting()
				.create();
			
			// create or open queues
			final String pendingQueueName = configuration.getProperty("sqs.queue.pending");
			final String pendingQueueUrl = sqsService.getOrCreateUrl(pendingQueueName);
			logger.info("loadUnidentified: pendingQueueUrl {}", pendingQueueUrl);
			final String serviceBusQueueUrl = sqsService.getOrCreateUrl(configuration.getProperty("sqs.queue.service_bus"));
			logger.info("loadUnidentified: serviceBusQueueUrl {}", serviceBusQueueUrl);
			
			// main loop
			final int sqsMessagesPerRun = Integer.parseInt(configuration.getProperty("sqs.messagesPerRun", "1")); // default to 1 message per run
			final long runTimeoutSeconds = Long.parseLong(configuration.getProperty("runTimeoutSeconds", "3600")); // default to 1 hour run timeout
			final long startTimeMillis = System.currentTimeMillis();
			for (int nreceivedMessages = 0; nreceivedMessages < sqsMessagesPerRun &&
					System.currentTimeMillis() - startTimeMillis < TimeUnit.SECONDS.toMillis(runTimeoutSeconds); ) {
				final List<SQSMessage> sqsMessages = sqsService.receiveMessages(pendingQueueUrl);
				logger.info("loadUnidentified: read {} messages", sqsMessages.size());
				if (null == sqsMessages || sqsMessages.isEmpty()) {
					Thread.sleep(5000L);
					continue;
				}
				for (SQSMessage sqsMessage : sqsMessages) {
					nreceivedMessages ++;
					try {
						// parse input message
						final String text = sqsMessage.getText();
						logger.info("loadUnidentified: received sqs message {}", text);
						final JsonObject input = gson.fromJson(text, JsonObject.class);
						try {
							// de-duplicate message by uuid
							final String uuid = getAsString(input.getAsJsonObject("header"), "uuid");
							if (1 != mcmdbDAO.insertSqsDeduplication(uuid, pendingQueueName, text)) {
								logger.info("loadUnidentified: duplicate message {}", uuid);
								// WARNING: do not send message to failed queue
//								final JsonObject message = formatFailedJson(input, "1",
//										"duplicate message: " + uuid, null);
//								sqsService.sendMessage(serviceBusQueueUrl, gson.toJson(message));
							} else {
								// send message to started queue
								sqsService.sendMessage(serviceBusQueueUrl,
										gson.toJson(formatStartedJson(input)));

								// process message
								logger.info("loadUnidentified: processing message...");
								final JsonObject output = new JsonObject();
								output.addProperty("beginTimeMillis", System.currentTimeMillis());
								processMessage(input, output);
								output.addProperty("endTimeMillis", System.currentTimeMillis());
								logger.info("loadUnidentified: message successfully processed");
									
								// send message to completed queue
								sqsService.sendMessage(serviceBusQueueUrl, 
										gson.toJson(formatCompletedJson(input, output)));									
							}
						} catch (Exception e) {
							logger.error("loadUnidentified", e);
							// send message to failed queue
							final JsonObject message = formatFailedJson(input, "0",
									"error processing message: " + e.getMessage(), e);
							sqsService.sendMessage(serviceBusQueueUrl, gson.toJson(message));
						}
						// delete from pending queue
						sqsService.deleteMessage(pendingQueueUrl, sqsMessage.getReceiptHandle());
					} catch (Exception e) {
						logger.error("loadUnidentified", e);
					}
				}
			}

			logger.info("updateKb: exiting after {}ms", System.currentTimeMillis() - startTimeMillis);			
		}
	}

	private void processMessage(JsonObject input, JsonObject output) throws Exception {
		
//		{
//			"idDsr":"spotify_siae_IT_201604_familyplan2_dsr",
//			"dsp":"spotify",
//			"year":"2016",
//			"month":"04",
//			"country":"ITA",
//			"idUtilizationType":"DWN"
//		}
		
		final JsonObject body = input.getAsJsonObject("body");
		final String idDsr = getAsString(body, "idDsr");
		final String dsp = getAsString(body, "dsp");
		final String year = getAsString(body, "year");
		final String month = getAsString(body, "month");
		final String idUtilizationType = getAsString(body, "idUtilizationType");

		if (StringUtils.isEmpty(idDsr) ||
				StringUtils.isEmpty(dsp) ||
				StringUtils.isEmpty(year) ||
				StringUtils.isEmpty(month)) {
			throw new IllegalArgumentException("missing mandatory parameter(s)");
		}

		// check invoice status
//		if (isInvoiced(idDsr)) {
//			throw new IllegalArgumentException("dsr already invoiced");
//		}
		
		// stats
		final Stats stats = new Stats();

		// DSR info
		final DsrInfo dsrInfo;
		try (final Connection mcmdb = mcmdbDS.getConnection()) {
			final McmdbDAO mcmdbDAO = new McmdbDAO(mcmdb);
			// load DSR info
			if (StringTools.isNullOrEmpty(idUtilizationType)) {
				dsrInfo = mcmdbDAO.getDsrInfo(idDsr);
				if (null == dsrInfo) {
					throw new IllegalArgumentException("unknown dsr " + idDsr);
				}
			} else {
				dsrInfo = new DsrInfo()
					.setIdDsr(idDsr)
					.setFtpSourcePath(dsp)
					.setYear(year)
					.setMonth(month)
					.setIdUtilizationType(idUtilizationType);
			}
			// check if DSR_UNIDENTIFIED already exists
			if ("true".equalsIgnoreCase(configuration.getProperty("checkAlreadyProcessed")) &&
					mcmdbDAO.existsDsrUnidentified(idDsr)) {
				logger.info("processMessage: dsr already processed {}", idDsr);
				stats.addProperties(output);
				output.addProperty("alreadyProcessed", true);
				output.addProperty("additionalInfo", "unidentified sale lines already loaded");
				return;
			}
		}
		logger.info("processMessage: dsrInfo {}", dsrInfo);
		
		// parse unidentified partition
		parse(dsrInfo, stats, new AmazonS3URI(configuration.getProperty("s3uri.input", "")
				.replace("{dsr}", idDsr).replace("{dsp}", dsp)
				.replace("{year}", year).replace("{month}", month)));

		// insert into DSR_UNIDENTIFIED
		try (final Connection mcmdb = mcmdbDS.getConnection()) {
			final int count = new McmdbDAO(mcmdb)
					.insertDsrUnidentified(idDsr, stats.rows.get());
			logger.info("processMessage: insertDsrUnidentified {}", count);
		}
				
		// output values
		stats.addProperties(output);
		
	}	

	private void parse(final DsrInfo dsr, final Stats stats, AmazonS3URI s3Uri) throws Exception {
		logger.info("parse: s3Uri {}", s3Uri);
		final int threadsCount = Integer.parseInt(configuration.getProperty("threadsCount", "1"));
		final String inputS3uriPartsExtension = configuration
				.getProperty("input.s3uri.parts.extension", ".csv").toLowerCase();
		final String inputS3uriPartsPrefix = String.format("%s/%s", s3Uri.getKey(),
				configuration.getProperty("input.s3uri.parts.prefix", "part-")).toLowerCase();
		final int inputS3uriPartsMaxNumber = Integer.parseInt(configuration
				.getProperty("input.s3uri.parts.maxNumber", "1000"));
		logger.debug("parse: threadsCount {}", threadsCount);
		logger.debug("parse: inputS3uriPartsExtension {}", inputS3uriPartsExtension);
		logger.debug("parse: inputS3uriPartsPrefix {}", inputS3uriPartsPrefix);
		logger.debug("parse: inputS3uriPartsMaxNumber {}", inputS3uriPartsMaxNumber);
		final Semaphore nextPartSemaphore = new Semaphore(threadsCount);
		final ExecutorService executor = Executors.newCachedThreadPool();
		final AtomicInteger runningTasks = new AtomicInteger(0);
		final AtomicReference<Exception> exception = new AtomicReference<Exception>();
		final long startTimeMillis = System.currentTimeMillis();
		// list directory
		final List<S3ObjectSummary> s3Listing = s3Service.listObjects(s3Uri.getBucket(), s3Uri.getKey());		
		if (null != s3Listing) {
			logger.info("parse: s3 parts number {}", s3Listing.size());
			if (s3Listing.size() > inputS3uriPartsMaxNumber) {
				throw new IllegalArgumentException("too many s3 parts " + s3Listing.size());
			}
			// loop on part(s)
			for (final S3ObjectSummary s3Summary : s3Listing) {
				if (null != exception.get()) {
					break;
				}
				// parse s3 part
				final String s3Key = URLDecoder.decode(s3Summary.getKey(), "UTF-8").toLowerCase();
				logger.debug("parse: s3Key {}", s3Key);
				if (s3Key.startsWith(inputS3uriPartsPrefix) && s3Key.endsWith(inputS3uriPartsExtension)) {
					// parse each s3 part in a separate thread 
					nextPartSemaphore.acquire();
					runningTasks.incrementAndGet();
					executor.execute(new Runnable() {

						@Override
						public void run() {
							try {
								for (int retries = Integer.parseInt(configuration.getProperty("socketTimeoutRetries", "1"));
										retries > 0; retries --) {
									logger.info("run: {} parsing {}", retries, s3Summary);
									try {
										final S3Object s3Object = s3Service.getObject(s3Summary.getBucketName(), s3Summary.getKey());
										if (null == s3Object) {
											throw new IOException("unable to download s3://" + s3Summary.getBucketName()
													+ "/" + s3Summary.getKey() + " (" + s3Summary.getSize() + ")");
										}
										try (final InputStream in = s3Object.getObjectContent();
												final InputStreamReader reader = new InputStreamReader(in, StandardCharsets.UTF_8);
												final CSVParser parser = new CSVParser(reader, CSVFormat.newFormat(';'));
												final Connection unidentified = unidentifiedDS.getConnection()) {											
											// loop on csv row(s)
											final Stats partialStats = new Stats();
											consumeCsv(parser, unidentified, languageGuessProvider.get(), dsr, partialStats);
											stats.update(partialStats);
										}
										return;
									} catch (SocketTimeoutException e) {
										if (1 == retries) { // this was last try
											exception.compareAndSet(null, e); // record exception
										}
										logger.error("run: {} {}", e.getClass().getName(), e.getMessage());
									}
								}
							} catch (Exception e) {
								exception.compareAndSet(null, e); // record exception
								logger.error("run: {} {}", e.getClass().getName(), e.getMessage());
							} finally {
								runningTasks.decrementAndGet();
								nextPartSemaphore.release();
							}						
						}

					});
				}
			}
			// join threads
			while (null == exception.get() && runningTasks.get() > 0) {
				logger.info("parse: waiting executor shutdown (still running {})", runningTasks.get());
				Thread.sleep(1000L);
			}
		}
		// throw exception (if any)
		if (null != exception.get()) {
			executor.shutdownNow();
			throw exception.get();
		}
		// gracefully shutdown executor
		executor.shutdown();
		executor.awaitTermination(5, TimeUnit.MINUTES);
		logger.info("parse: elapsed {}ms", System.currentTimeMillis() - startTimeMillis);
	}
	
	private void consumeCsv(CSVParser parser, Connection unidentified, LanguageGuess languageGuess, DsrInfo dsr, Stats stats) throws Exception {

		// WARNING: copy configuration because of heavy multi-threaded access
		final Properties properties = new Properties();
		properties.putAll(configuration);
		
		final double priorityIsrcBoostFixed = Double.parseDouble(properties
				.getProperty("priority.boost.isrc.fixed", "0"));
		final double priorityIsrcBoostMultiplier = Double.parseDouble(properties
				.getProperty("priority.boost.isrc.multiplier", "1.0"));
		final double priorityLangBoostFixed = Double.parseDouble(properties
				.getProperty("priority.boost.language.fixed", "0"));
		final double priorityLangBoostMultiplier = Double.parseDouble(properties
				.getProperty("priority.boost.language.multiplier", "1.0"));

		final double priorityTypeBoostFixed = Double.parseDouble(properties
				.getProperty("priority.boost.type.fixed." + dsr.getIdUtilizationType(), "0"));
		final double priorityTypeBoostMultiplier = Double.parseDouble(properties
				.getProperty("priority.boost.type.multiplier." + dsr.getIdUtilizationType(), "1.0"));
		logger.debug("consumeCsv: using type boost ({} + N * {})", priorityTypeBoostFixed, priorityTypeBoostMultiplier);
		
		int prioritySkipIfLessThan = Integer.parseInt(properties
				.getProperty("priority.skipIfLessThan." + dsr.getIdUtilizationType(), "0"));
		if (0 == prioritySkipIfLessThan) {
			prioritySkipIfLessThan = Integer.parseInt(properties
					.getProperty("priority.skipIfLessThan", "0"));
		}
		logger.debug("consumeCsv: using priority threshold {}", prioritySkipIfLessThan);
		
		// unidentified DAO
		final UnidentifiedDAO unidentifiedDAO = new UnidentifiedDAO(unidentified);   

		// loop on csv row(s)
		for (CSVRecord record : parser) {

			final GenericSaleLine line = new GenericSaleLine(properties, record);
			
			boolean boosted = false;
			final long originalPriority = StringTools.isNullOrEmpty(line.getSalesCount()) ?
					1L : Math.round(Double.parseDouble(line.getSalesCount()));
			long priority = originalPriority;
			
			// italian ISRC
			if (!StringUtils.isEmpty(line.getIsrc()) && line.getIsrc().startsWith("IT")) {
				boosted = true;
				stats.italianIsrc.incrementAndGet();
				priority = (long) (priorityIsrcBoostFixed + priority * priorityIsrcBoostMultiplier);
			}
			
			// italian title
			if (!boosted) {
				if (languageGuess.isItalianText(line.getTitle())) {
					boosted = true;
					stats.italianLanguage.incrementAndGet();
					priority = (long) (priorityLangBoostFixed + priority * priorityLangBoostMultiplier);
				}
			}
			
			// download
			if (!boosted) { 
				boosted = true;
				priority = (long) (priorityTypeBoostFixed + priority * priorityTypeBoostMultiplier);
			}
			stats.rows.incrementAndGet();
			if (originalPriority != priority) {
				stats.boosted.incrementAndGet();
			}
			
			// skip if priority less than...
			if (priority < prioritySkipIfLessThan) {
				stats.skipped.incrementAndGet();
				continue;
			}
			
			// insert into unidentified_song_dsr
			int count = unidentifiedDAO.insertUnidentifiedDsr(dsr.getIdDsr(), dsr.getFtpSourcePath(), line);
			boolean inserted = (1 == count);
			boolean duplicate = (0 == count);
			if (!inserted || duplicate) {
				stats.duplicated.incrementAndGet();
				continue;
			}
			
			// insert into unidentified_song
			count = unidentifiedDAO.insertUnidentified(line, priority);
			inserted = (1 == count);
			duplicate = (0 == count);

			// update existing line in unidentified_song
			boolean updated = false;
			if (duplicate) {
				count = unidentifiedDAO.updateUnidentified(line);
				updated = (1 == count);
			}
			
			// delete from unidentified_song_dsr
			if (!inserted && !updated) {
				count = unidentifiedDAO.deleteUnidentifiedDsr(line);
			}

			if (inserted) {
				stats.inserted.incrementAndGet();
			}
			if (updated) {
				stats.updated.incrementAndGet();
			}
			
		}
		
	}

	public static void main(String[] args) throws Exception {
		try {	
			final Injector injector = Guice.createInjector(new GuiceModule(args));
			injector.getInstance(SQSService.class).startup();
			injector.getInstance(S3Service.class).startup();
			try {
				injector.getInstance(UnidentifiedLoader.class)
					.loadUnidentified();
			} finally {
				injector.getInstance(SQSService.class).shutdown();
				injector.getInstance(S3Service.class).shutdown();
			}
		} catch (Exception e) {
			logger.error("main", e);
		} finally {
			System.exit(0);
		}
	}

}
