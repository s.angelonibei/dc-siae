#!/bin/sh

ENVNAME=dev
APPNAME=dsr-metadata-csv
VERSION=1.6.3

JARNAME=unidentified-loader-$VERSION-jar-with-dependencies.jar
CFGNAME=$APPNAME.properties
#HOME=/var/local/sophia/$ENVNAME
HOME=/home/orchestrator/sophia/unidentified
#JVMOPTS=-Dlog4j.configurationFile=$HOME/log4j2.xml

java -cp $HOME/$JARNAME $JVMOPTS com.alkemytech.sophia.uniload.tools.DsrMetadataCsv $HOME/$CFGNAME $1


