#!/bin/sh

# Usage: codifica-automatica.sh <ID_DSR>

ENVNAME=dev
APPNAME=codifica-automatica
VERSION=1.6.3
JARNAME=unidentified-loader-$VERSION-jar-with-dependencies.jar
CFGNAME=unidentified-loader.properties
RUNNAME=codifica-automatica.log
HOME=/var/local/sophia/$ENVNAME
#OUTPUT=/dev/null
OUTPUT=$HOME/logs/$APPNAME-$(date +%Y%m%d%H%M%S).log

#cd $HOME

nohup java -cp $HOME/$JARNAME com.alkemytech.sophia.uniload.tools.SqsLoader $HOME/$CFGNAME $1 2>&1 > $OUTPUT &

echo "$(date +%Y%m%d%H%M%S) $ENVNAME $APPNAME $1" >> $HOME/$RUNNAME

