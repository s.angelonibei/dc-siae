#!/bin/sh

ENVNAME=dev
APPNAME=unidentified-loader
VERSION=1.6.3

JARNAME=$APPNAME-$VERSION-jar-with-dependencies.jar
CFGNAME=$APPNAME.properties
HOME=/var/local/sophia/$ENVNAME
#HOME=/home/orchestrator/sophia/unidentified
#OUTPUT=/dev/null
OUTPUT=$HOME/logs/$APPNAME.out

echo "$(date +%Y%m%d%H%M%S) $ENVNAME $APPNAME" >> $OUTPUT

nohup java -jar $HOME/$JARNAME $HOME/$CFGNAME 2>&1 >> $OUTPUT &

echo "$(date +%Y%m%d%H%M%S) $ENVNAME $APPNAME" >> $HOME/logs/crontab.log
