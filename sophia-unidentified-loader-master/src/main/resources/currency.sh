#!/bin/sh

ENVNAME=dev
APPNAME=currency
HOME=/var/local/sophia/$ENVNAME
#OUTPUT=/dev/null
OUTPUT=$HOME/logs/$APPNAME-$(date +%Y%m%d%H%M%S).log

#cd $HOME

curl -X GET -H "Cache-Control: no-cache" "http://gestioneutilizzazioni-sviluppo.alkemytech.it/cruscottoUtilizzazioni/rest/currency/refresh" 2>&1 > $OUTPUT &

echo "$(date +%Y%m%d%H%M%S) $ENVNAME $APPNAME" >> $HOME/crontab.log

