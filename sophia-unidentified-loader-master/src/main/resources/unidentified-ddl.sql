

### unidentified_song

DROP TABLE IF EXISTS unidentified_song;

CREATE TABLE unidentified_song
(
   hash_id           CHAR(40)        NOT NULL,
   title             VARCHAR(4095)   NOT NULL,
   artists           VARCHAR(4095)   NOT NULL,
   siada_title       VARCHAR(4095)   NOT NULL,
   siada_artists     VARCHAR(4095)   NOT NULL,
   roles             VARCHAR(4095),
   priority          BIGINT          NOT NULL,
   insert_time       BIGINT          NOT NULL,
   last_auto_time    BIGINT          DEFAULT 0 NOT NULL,
   last_manual_time  BIGINT          DEFAULT 0 NOT NULL,
   PRIMARY KEY (hash_id)
)
ENGINE=InnoDB;


--alter table unidentified_song add siada_title VARCHAR(4095);
--alter table unidentified_song add siada_artists VARCHAR(4095);


CREATE INDEX unidentified_song_idx_02
   ON unidentified_song (last_auto_time ASC);

CREATE INDEX unidentified_song_idx_01
   ON unidentified_song (priority ASC);

CREATE INDEX unidentified_song_idx_04
   ON unidentified_song (insert_time ASC);

CREATE INDEX unidentified_song_idx_03
   ON unidentified_song (last_manual_time ASC);




### unidentified_song_dsr

DROP TABLE IF EXISTS unidentified_song_dsr;

CREATE TABLE unidentified_song_dsr
(
   id_util         VARCHAR(255)    NOT NULL,
   hash_id         CHAR(40)        NOT NULL,
   id_dsr          VARCHAR(255)    NOT NULL,
   album_title     VARCHAR(4095),
   proprietary_id  VARCHAR(255),
   isrc            CHAR(12),
   iswc            CHAR(11),
   sales_count     BIGINT          DEFAULT 0 NOT NULL,
   dsp             VARCHAR(255),
   insert_time     BIGINT          NOT NULL,
   PRIMARY KEY (id_util)
)
ENGINE=InnoDB;

CREATE INDEX unidentified_song_dsr_idx_01
   ON unidentified_song_dsr (hash_id ASC);

CREATE INDEX unidentified_song_dsr_idx_02
   ON unidentified_song_dsr (insert_time ASC);




### identified_song

DROP TABLE IF EXISTS identified_song;

CREATE TABLE identified_song
(
   hash_id              CHAR(40)        NOT NULL,
   title                VARCHAR(4095)   NOT NULL,
   artists              VARCHAR(4095)   NOT NULL,
   siada_title          VARCHAR(4095)   NOT NULL,
   siada_artists        VARCHAR(4095)   NOT NULL,
   roles                VARCHAR(4095),
   siae_work_code       CHAR(11),
   identification_type  VARCHAR(20),
   insert_time          BIGINT          NOT NULL,
   sophia_update_time   BIGINT          DEFAULT 0 NOT NULL,
   PRIMARY KEY (hash_id)
)
ENGINE=InnoDB;

--alter table identified_song add siada_title VARCHAR(4095);
--alter table identified_song add siada_artists VARCHAR(4095);

CREATE INDEX identified_song_idx_01
   ON identified_song (sophia_update_time ASC);


