package com.alkemytech.sophia.harmonizer.goal.utils;

/**
 * Created by Alessandro Russo on 07/03/2018.
 */
public class HarmonizerConstants {

    //TO DO inserire costanti utilizzate nel batch (stati, variabili globali, ecc.)
    public static final String TITOLO_OPERA = "titoloOpera";
    public static final String AUTORE = "autore";
    public static final String EDITORE = "editore";
    public static final String INTERPRETE = "interprete";
    public static final String UTILIZZAZIONI = "utilizzazioni";
    public static final String PREZZO_VENDITA = "prezzoVendita";
    public static final String CODICE_OPERA = "codiceOpera";
    public static final String CODICE_ISWC = "codiceISWC";
    public static final String CODICE_ISRC = "codiceISRC";
    public static final String STATO = "stato";
    public static final String MOTIVI_SCARTO = "motiviScarto";
    public static final String STATO_CARICATO = "CARICATO";
    public static final String STATO_IN_VALIDAZIONE = "IN_VALIDAZIONE";
    public static final String STATO_VALIDATO = "VALIDATO";
    public static final String STATO_ERRORE_VALIDAZIONE = "ERRORE_VALIDAZIONE";
    public static final String STATO_LOGICO_LAVORABILE = "LAVORABILE";
    public static final String STATO_LOGICO_SCARTATO = "SCARTATO";
    public static final Integer ATTIVO = 1;
    public static final String BUCKET_MULTIMEDIALE_LOCALE = "s3://siae-sophia-multimediale-locale/";
    public static final String TOTAL = "total";
    public static final String SUCCESS = "success";
    public static final String FAIL = "fail";
    public static final String UTILIZATIONS = "utilizations";
    public static final String CODICE_ML_VAL_ACC = "ML_VAL_ACC";
}