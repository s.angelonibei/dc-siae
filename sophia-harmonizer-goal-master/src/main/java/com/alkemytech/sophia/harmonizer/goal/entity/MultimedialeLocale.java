package com.alkemytech.sophia.harmonizer.goal.entity;

import javax.persistence.*;
import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@Produces("application/json")
@XmlRootElement
@Entity(name = "MultimedialeLocale")
@Table(name = "ML_REPORT")
@NamedQueries({ @NamedQuery(name = "MultimedialeLocale.GetAll", query = "SELECT x FROM MultimedialeLocale x") })
public class MultimedialeLocale {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_ML_REPORT", nullable = false)
    private Integer idMlReport;

    @Column(name = "LICENZA")
    private String licenza;

    @Column(name = "CODICE_LICENZIATARIO")
    private String codiceLicenziatario;

    @Column(name = "NOMINATIVO_LICENZIATARIO")
    private String nominativoLicenziatario;

    @Column(name = "SERVIZIO")
    private String servizio;

    @Column(name = "ORIG_PERIODO_COMPETENZA")
    private String origPeriodoCompetenza;

    @Column(name = "PERIODO_COMPETENZA_DATA_DA")
    private Date periodoCompetenzaDataDa;

    @Column(name = "PERIODO_COMPETENZA_DATA_A")
    private Date periodoCompetenzaDataA;

    @Column(name = "MOD_UTILIZZAZIONE")
    private Integer modUtilizzazione;

    @Column(name = "MOD_FRUIZIONE")
    private String modFruizione;

    @Column(name = "IN_ABBONAMENTO")
    private Boolean inAbbonamento;

    @Column(name = "POSIZIONE_REPORT")
    private String posizioneReport;

    @Column(name = "IDENTIFICATIVO_REPORT")
    private String identificativoReport;

    @Column(name = "PROGRESSIVO_REPORT")
    private Integer progressivoReport;

    @Column(name = "STATO")
    private String stato;

    @Column(name = "STATO_LOGICO")
    private String statoLogico;

    @Column(name = "USA_CODICE_OPERA")
    private Boolean usaCodiceOpera;

    @Column(name = "DATA_UPLOAD")
    private Date dataUpload;

    @Column(name = "POSIZIONE_REPORT_NORMALIZZATO")
    private String posizioneReportNormalizzato;

    @Column(name = "DATA_VALIDAZIONE")
    private Date dataValidazione;

    @Column(name = "VALIDAZIONE_RECORD_TOTALI")
    private Integer validazioneRecordTotali;

    @Column(name = "VALIDAZIONE_RECORD_VALIDI")
    private Integer validazioneRecordValidi;

    @Column(name = "VALIDAZIONE_UTILIZZAZIONI_VALIDE")
    private Integer validazioneUtilizzazioniValide;

    @Column(name = "DATA_CODIFICA")
    private Date dataCodifica;

    @Column(name = "POSIZIONE_REPORT_CODIFICATO")
    private String posizioneReportCodificato;

    @Column(name = "CODIFICA_RECORD_CODIFICATI")
    private Integer codificaRecordCodificati;

    @Column(name = "CODIFICA_UTILIZZAZIONI_CODIFICATE")
    private Integer codificaUtilizzazioniCodificate;

    @Column(name = "ATTIVO")
    private boolean attivo;

    @Column(name = "CODIFICA_INFO")
    private String codificaInfo;

    @Column(name = "SOGLIA_VALIDAZIONE")
    private Double sogliaValidazione;

    @Column(name = "SOGLIA_CODIFICA")
    private Double sogliaCodifica;


    @Column(name = "DATA_NOTIFICA_VALIDAZIONE")
    private Date dataNotificaValidazione;

    @Column(name = "INFO_NOTIFICA_VALIDAZIONE")
    private String infoNotificaValidazione;

    public MultimedialeLocale() {
    }

    public MultimedialeLocale(String licenza, String codiceLicenziatario, String nominativoLicenziatario, String servizio, String origPeriodoCompetenza, Date periodoCompetenzaDataDa, Date periodoCompetenzaDataA, Integer modUtilizzazione, String modFruizione, Boolean inAbbonamento, String posizioneReport, String identificativoReport, Integer progressivoReport, String stato, String statoLogico, Boolean usaCodiceOpera, Date dataUpload, String posizioneReportNormalizzato, Date dataValidazione, Integer validazioneRecordTotali, Integer validazioneRecordValidi, Integer validazioneUtilizzazioniValide, Date dataCodifica, String posizioneReportCodificato, Integer codificaRecordCodificati, Integer codificaUtilizzazioniCodificate, boolean attivo, String codificaInfo, Double sogliaValidazione, Double sogliaCodifica) {
        this.licenza = licenza;
        this.codiceLicenziatario = codiceLicenziatario;
        this.nominativoLicenziatario = nominativoLicenziatario;
        this.servizio = servizio;
        this.origPeriodoCompetenza = origPeriodoCompetenza;
        this.periodoCompetenzaDataDa = periodoCompetenzaDataDa;
        this.periodoCompetenzaDataA = periodoCompetenzaDataA;
        this.modUtilizzazione = modUtilizzazione;
        this.modFruizione = modFruizione;
        this.inAbbonamento = inAbbonamento;
        this.posizioneReport = posizioneReport;
        this.identificativoReport = identificativoReport;
        this.progressivoReport = progressivoReport;
        this.stato = stato;
        this.statoLogico = statoLogico;
        this.usaCodiceOpera = usaCodiceOpera;
        this.dataUpload = dataUpload;
        this.posizioneReportNormalizzato = posizioneReportNormalizzato;
        this.dataValidazione = dataValidazione;
        this.validazioneRecordTotali = validazioneRecordTotali;
        this.validazioneRecordValidi = validazioneRecordValidi;
        this.validazioneUtilizzazioniValide = validazioneUtilizzazioniValide;
        this.dataCodifica = dataCodifica;
        this.posizioneReportCodificato = posizioneReportCodificato;
        this.codificaRecordCodificati = codificaRecordCodificati;
        this.codificaUtilizzazioniCodificate = codificaUtilizzazioniCodificate;
        this.attivo = attivo;
        this.codificaInfo = codificaInfo;
        this.sogliaValidazione = sogliaValidazione;
        this.sogliaCodifica = sogliaCodifica;
    }

    public Integer getIdMlReport() {
        return idMlReport;
    }

    public void setIdMlReport(Integer idMlReport) {
        this.idMlReport = idMlReport;
    }

    public String getLicenza() {
        return licenza;
    }

    public void setLicenza(String licenza) {
        this.licenza = licenza;
    }

    public String getCodiceLicenziatario() {
        return codiceLicenziatario;
    }

    public void setCodiceLicenziatario(String codiceLicenziatario) {
        this.codiceLicenziatario = codiceLicenziatario;
    }

    public String getNominativoLicenziatario() {
        return nominativoLicenziatario;
    }

    public void setNominativoLicenziatario(String nominativoLicenziatario) {
        this.nominativoLicenziatario = nominativoLicenziatario;
    }

    public String getServizio() {
        return servizio;
    }

    public void setServizio(String servizio) {
        this.servizio = servizio;
    }

    public String getOrigPeriodoCompetenza() {
        return origPeriodoCompetenza;
    }

    public void setOrigPeriodoCompetenza(String origPeriodoCompetenza) {
        this.origPeriodoCompetenza = origPeriodoCompetenza;
    }

    public Date getPeriodoCompetenzaDataDa() {
        return periodoCompetenzaDataDa;
    }

    public void setPeriodoCompetenzaDataDa(Date periodoCompetenzaDataDa) {
        this.periodoCompetenzaDataDa = periodoCompetenzaDataDa;
    }

    public Date getPeriodoCompetenzaDataA() {
        return periodoCompetenzaDataA;
    }

    public void setPeriodoCompetenzaDataA(Date periodoCompetenzaDataA) {
        this.periodoCompetenzaDataA = periodoCompetenzaDataA;
    }

    public Integer getModUtilizzazione() {
        return modUtilizzazione;
    }

    public void setModUtilizzazione(Integer modUtilizzazione) {
        this.modUtilizzazione = modUtilizzazione;
    }

    public String getModFruizione() {
        return modFruizione;
    }

    public void setModFruizione(String modFruizione) {
        this.modFruizione = modFruizione;
    }

    public Boolean getInAbbonamento() {
        return inAbbonamento;
    }

    public void setInAbbonamento(Boolean inAbbonamento) {
        this.inAbbonamento = inAbbonamento;
    }

    public String getPosizioneReport() {
        return posizioneReport;
    }

    public void setPosizioneReport(String posizioneReport) {
        this.posizioneReport = posizioneReport;
    }

    public String getIdentificativoReport() {
        return identificativoReport;
    }

    public void setIdentificativoReport(String identificativoReport) {
        this.identificativoReport = identificativoReport;
    }

    public Integer getProgressivoReport() {
        return progressivoReport;
    }

    public void setProgressivoReport(Integer progressivoReport) {
        this.progressivoReport = progressivoReport;
    }

    public String getStato() {
        return stato;
    }

    public void setStato(String stato) {
        this.stato = stato;
    }

    public String getStatoLogico() {
        return statoLogico;
    }

    public void setStatoLogico(String statoLogico) {
        this.statoLogico = statoLogico;
    }

    public Boolean getUsaCodiceOpera() {
        return usaCodiceOpera;
    }

    public void setUsaCodiceOpera(Boolean usaCodiceOpera) {
        this.usaCodiceOpera = usaCodiceOpera;
    }

    public Date getDataUpload() {
        return dataUpload;
    }

    public void setDataUpload(Date dataUpload) {
        this.dataUpload = dataUpload;
    }

    public String getPosizioneReportNormalizzato() {
        return posizioneReportNormalizzato;
    }

    public void setPosizioneReportNormalizzato(String posizioneReportNormalizzato) {
        this.posizioneReportNormalizzato = posizioneReportNormalizzato;
    }

    public Date getDataValidazione() {
        return dataValidazione;
    }

    public void setDataValidazione(Date dataValidazione) {
        this.dataValidazione = dataValidazione;
    }

    public Integer getValidazioneRecordTotali() {
        return validazioneRecordTotali;
    }

    public void setValidazioneRecordTotali(Integer validazioneRecordTotali) {
        this.validazioneRecordTotali = validazioneRecordTotali;
    }

    public Integer getValidazioneRecordValidi() {
        return validazioneRecordValidi;
    }

    public void setValidazioneRecordValidi(Integer validazioneRecordValidi) {
        this.validazioneRecordValidi = validazioneRecordValidi;
    }

    public Integer getValidazioneUtilizzazioniValide() {
        return validazioneUtilizzazioniValide;
    }

    public void setValidazioneUtilizzazioniValide(Integer validazioneUtilizzazioniValide) {
        this.validazioneUtilizzazioniValide = validazioneUtilizzazioniValide;
    }

    public Date getDataCodifica() {
        return dataCodifica;
    }

    public void setDataCodifica(Date dataCodifica) {
        this.dataCodifica = dataCodifica;
    }

    public String getPosizioneReportCodificato() {
        return posizioneReportCodificato;
    }

    public void setPosizioneReportCodificato(String posizioneReportCodificato) {
        this.posizioneReportCodificato = posizioneReportCodificato;
    }

    public Integer getCodificaRecordCodificati() {
        return codificaRecordCodificati;
    }

    public void setCodificaRecordCodificati(Integer codificaRecordCodificati) {
        this.codificaRecordCodificati = codificaRecordCodificati;
    }

    public Integer getCodificaUtilizzazioniCodificate() {
        return codificaUtilizzazioniCodificate;
    }

    public void setCodificaUtilizzazioniCodificate(Integer codificaUtilizzazioniCodificate) {
        this.codificaUtilizzazioniCodificate = codificaUtilizzazioniCodificate;
    }

    public boolean isAttivo() {
        return attivo;
    }

    public void setAttivo(boolean attivo) {
        this.attivo = attivo;
    }

    public String getCodificaInfo() {
        return codificaInfo;
    }

    public void setCodificaInfo(String codificaInfo) {
        this.codificaInfo = codificaInfo;
    }

    public Double getSogliaValidazione() {
        return sogliaValidazione;
    }

    public void setSogliaValidazione(Double sogliaValidazione) {
        this.sogliaValidazione = sogliaValidazione;
    }

    public Double getSogliaCodifica() {
        return sogliaCodifica;
    }

    public void setSogliaCodifica(Double sogliaCodifica) {
        this.sogliaCodifica = sogliaCodifica;
    }

    public Date getDataNotificaValidazione() {
        return dataNotificaValidazione;
    }

    public void setDataNotificaValidazione(Date dataNotificaValidazione) {
        this.dataNotificaValidazione = dataNotificaValidazione;
    }

    public String getInfoNotificaValidazione() {
        return infoNotificaValidazione;
    }

    public void setInfoNotificaValidazione(String infoNotificaValidazione) {
        this.infoNotificaValidazione = infoNotificaValidazione;
    }
}