package com.alkemytech.sophia.harmonizer.goal.rest;

import com.alkemytech.sophia.common.tools.DateTools;
import com.alkemytech.sophia.harmonizer.goal.entity.MultimedialeLocale;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Alessandro Russo on 12/04/2018.
 */
public class ValidationResult implements Serializable {

    private String id;
    private String esitoValidazione;
    private Double sogliaValidazione;
    private Integer numRigheValide;
    private Integer numTotRighe;
    private Integer numTotUtilizzazioniValide;
    private String dataLavorazioneValidazione;

    public ValidationResult(MultimedialeLocale file){
        this.id = file.getIdentificativoReport();
        this.esitoValidazione = file.getStatoLogico();
        this.sogliaValidazione = file.getSogliaValidazione();
        this.numRigheValide = file.getValidazioneRecordValidi();
        this.numTotRighe = file.getValidazioneRecordTotali();
        this.numTotUtilizzazioniValide = file.getValidazioneUtilizzazioniValide();
        this.dataLavorazioneValidazione = DateTools.dateToString(file.getDataValidazione(), DateTools.DATETIME_ITA_FORMAT_SLASH);
    }

    @Override
    public String toString() {
        return "ValidationResult{" +
                "identificativoReport='" + id + '\'' +
                ", esitoValidazione='" + esitoValidazione + '\'' +
                ", sogliaValidazione='" + sogliaValidazione + '\'' +
                ", numRigheValide='" + numRigheValide + '\'' +
                ", numTotRighe='" + numTotRighe + '\'' +
                ", numTotUtilizzazioniValide='" + numTotUtilizzazioniValide + '\'' +
                ", dataLavorazioneValidazione='" + dataLavorazioneValidazione + '\'' +
                '}';
    }
}
