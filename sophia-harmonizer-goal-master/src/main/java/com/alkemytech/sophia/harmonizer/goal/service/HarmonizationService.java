package com.alkemytech.sophia.harmonizer.goal.service;

import com.alkemytech.sophia.common.mapper.TemplateMapper;
import com.alkemytech.sophia.common.s3.S3Service;
import com.alkemytech.sophia.harmonizer.goal.entity.MultimedialeLocale;
import com.alkemytech.sophia.harmonizer.goal.utils.HarmonizerConstants;
import com.amazonaws.services.s3.AmazonS3URI;
import com.amazonaws.util.IOUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.ibm.icu.text.CharsetDetector;
import com.ibm.icu.text.CharsetMatch;
import org.apache.commons.lang3.StringUtils;
import org.drools.runtime.StatefulKnowledgeSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.util.*;

/**
 * Created by Alessandro Russo on 01/12/2017.
 */
@Singleton
public class HarmonizationService {

    private static final String ENVIRONMENT = "ENVNAME";
    private static final String UTF8_BOM = "\uFEFF";

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private S3Service s3Service;
    private DbService dbService;

    private String parserConfig;
    private String parserStream;
    private String writerBucket;
    private String writerNormalizedDir;
    private String writerNormalizedConfig;
    private String writerNormalizedStream;
    private String writerNormalizedHeader;
    private String writerOutDir;
    private String writerOutConfig;
    private String writerOutStream;
    private String writerOutHeader;
    private String writerOutSuffix;
    private String validationConfig;
    private String timezone;

    @Inject
    public HarmonizationService(S3Service s3Service,
                                DbService dbService,
                                @Named("parser.config") String parserConfig,
                                @Named("parser.stream")String parserStream,
                                @Named("writer.bucket")String writerBucket,
                                @Named("writer.normalized.dir")String writerNormalizedDir,
                                @Named("writer.normalized.config")String writerNormalizedConfig,
                                @Named("writer.normalized.stream")String writerNormalizedStream,
                                @Named("writer.normalized.header")String writerNormalizedHeader,
                                @Named("writer.out.dir")String writerOutDir,
                                @Named("writer.out.config")String writerOutConfig,
                                @Named("writer.out.stream")String writerOutStream,
                                @Named("writer.out.header")String writerOutHeader,
                                @Named("writer.out.suffix")String writerOutSuffix,
                                @Named("validation.config")String validationConfig,
                                @Named("default.timezone")String timezone) {
        this.s3Service = s3Service;
        this.dbService = dbService;
        this.parserConfig = parserConfig;
        this.parserStream = parserStream;
        this.writerBucket = writerBucket;
        this.writerNormalizedDir = writerNormalizedDir;
        this.writerNormalizedConfig = writerNormalizedConfig;
        this.writerNormalizedStream = writerNormalizedStream;
        this.writerNormalizedHeader = writerNormalizedHeader;
        this.writerOutDir = writerOutDir;
        this.writerOutConfig = writerOutConfig;
        this.writerOutStream = writerOutStream;
        this.writerOutHeader = writerOutHeader;
        this.writerOutSuffix = writerOutSuffix;
        this.validationConfig = validationConfig;
        this.timezone = timezone;
    }

    public List<Object> parseFile(MultimedialeLocale fileToParse) throws IOException {
        List<Object> records = null;
        if(StringUtils.isNotEmpty( parserConfig ) || StringUtils.isNotEmpty( parserStream ) ){
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            TemplateMapper mapper = new TemplateMapper();
            try {
                logger.debug("default charset: {}", Charset.defaultCharset());

                logger.debug("report to parse -> {}", fileToParse.getPosizioneReport() );
                AmazonS3URI amazonS3URI = new AmazonS3URI(fileToParse.getPosizioneReport());
                InputStream is = s3Service.download( amazonS3URI.getBucket(), amazonS3URI.getKey() );
                logger.debug("Retrieved file from Amazon S3");
                byte[] data = IOUtils.toByteArray( is );
                CharsetMatch match = new CharsetDetector().setText(data).detect();
                logger.debug("detected charset: {}", match.getName());
                String encodedData = new String(data , match.getName() );
                encodedData = encodedData.replace(UTF8_BOM, "");
                byte[] newBytes = encodedData.getBytes();
                bos = new ByteArrayOutputStream(newBytes.length);
                bos.write(newBytes, 0, newBytes.length);
                records = mapper.parseFile(parserConfig, parserStream, bos);
            }
            finally {
                bos.flush();
                bos.close();
            }
        }
        else{
            logger.debug("Configurazione Parser assente");
        }
        return records;
    }

    public Map<String, Object> validateFile(List<Object> records) throws Exception {
        Map<String, Object> validationResult = new HashMap<>();
        if( StringUtils.isNotEmpty(validationConfig)){
            StatefulKnowledgeSession session = new RulesEngineService().createKnowledgeBaseFromSpreadsheet(validationConfig).newStatefulKnowledgeSession();
            List<Map<String, Object>> total = new ArrayList<>();
            List<Map<String, Object>> fail = new ArrayList<>();
            List<Integer> utilizations = new ArrayList<>();
            for(Object currentRecord : records){
                Map<String, Object> castRecord = (Map<String, Object>) currentRecord;
                setupDroolsSession(session, total, fail, utilizations);
                session.insert(castRecord);
                session.fireAllRules();
            }
            logger.info("normalized file content validated successfully!");
            logger.debug("total records -> {}", total );
            logger.debug("fail records -> {}", fail );
            logger.debug("some record has utilization field present -> {}", utilizations.size() > 0 ? true : false  );
            validationResult.put(HarmonizerConstants.TOTAL, total);
            validationResult.put(HarmonizerConstants.FAIL, fail);
            validationResult.put(HarmonizerConstants.UTILIZATIONS, utilizations);
        }
        else{
            logger.error("Validation rules file not found in config!");
        }
        return validationResult;
    }

    public String createNormalizedFile(Map<String, Object> validatedContent, MultimedialeLocale currentFile) throws Exception {
        if( validatedContent.containsKey( HarmonizerConstants.TOTAL ) ){
            List<Map<String, Object> > total = (List<Map<String, Object>>) validatedContent.get(HarmonizerConstants.TOTAL);
            List<Map<String, Object> > fail = (List<Map<String, Object>>) validatedContent.get(HarmonizerConstants.FAIL);
            List<Map<String, Object> > success = new ArrayList<Map<String, Object>>(total.size());
            success.addAll(total);
            success.removeAll(fail);
            if( success.size() > 0 ){
                fillUtilizationField( success, (List<Integer>) validatedContent.get(HarmonizerConstants.UTILIZATIONS));
                validatedContent.put(HarmonizerConstants.SUCCESS, success);
                String posizioneReportNormalizzato = writeNormalizedFile(success, currentFile.getPosizioneReport(), null, writerNormalizedDir, writerNormalizedConfig, writerNormalizedStream, writerNormalizedHeader);
                try{
                    writeNormalizedFile(addRecordStatus(total), currentFile.getPosizioneReport(), writerOutSuffix, writerOutDir, writerOutConfig, writerOutStream, writerOutHeader);
                }
                catch(Exception e){
                    logger.error("Error writing OUT normalized File");
                }
                return posizioneReportNormalizzato;
            }
            throw new Exception("No successfully validated records found!");
        }
        throw new Exception("Error writing normalized File");
    }

    private List<Map<String, Object>> addRecordStatus(List<Map<String, Object>> totale) {
        for (Map<String, Object> current : totale) {
            if(current.containsKey(HarmonizerConstants.MOTIVI_SCARTO)){
                current.put(HarmonizerConstants.STATO, "KO");
            } else{
                current.put(HarmonizerConstants.STATO, "OK");
            }
        }
        return totale;
    }

    public boolean evaluateKPI(Map<String, Object> validatedContent,
                               MultimedialeLocale currentFile,
                               String posizioneReportNormalizzato) {

        Double validationThreshold = dbService.getValidationThreshold();
        logger.debug("Current validation threshold is {}", validationThreshold);
        Integer koRecords = ((List<Map<String, Object>>)validatedContent.get(HarmonizerConstants.FAIL)).size();
        Integer totalRecords = ((List<Map<String, Object>>)validatedContent.get(HarmonizerConstants.TOTAL)).size();
        Integer okRecords = validatedContent.containsKey(HarmonizerConstants.SUCCESS) ? ((List<Map<String, Object>>)validatedContent.get(HarmonizerConstants.SUCCESS)).size() : totalRecords - koRecords;
        double sogliaValidazione = ((okRecords.doubleValue()/totalRecords.doubleValue())*100);
        boolean isOK = sogliaValidazione > validationThreshold;
        String stato = HarmonizerConstants.STATO_VALIDATO;
        String statoLogico = isOK ? HarmonizerConstants.STATO_LOGICO_LAVORABILE : HarmonizerConstants.STATO_LOGICO_SCARTATO;
        TimeZone.setDefault(TimeZone.getTimeZone(timezone));

        currentFile.setStato(stato);
        currentFile.setStatoLogico(statoLogico);
        currentFile.setPosizioneReportNormalizzato(posizioneReportNormalizzato);
        currentFile.setDataValidazione(new Date());
        currentFile.setSogliaValidazione(validationThreshold);
        currentFile.setValidazioneRecordTotali(totalRecords);
        currentFile.setValidazioneRecordValidi(okRecords);
        currentFile.setValidazioneUtilizzazioniValide( countUtilizations((List<Map<String, Object>>)validatedContent.get(HarmonizerConstants.SUCCESS), (List<Integer>) validatedContent.get(HarmonizerConstants.UTILIZATIONS) ));
        dbService.updateReport(currentFile);
        return true;
    }

    private void fillUtilizationField ( List<Map<String, Object>> success, List<Integer> utilizations ){
        boolean fillWithZero = utilizations.size() > 0 ? true : false;
        for( Map<String, Object> record : success){
            if(record.get(HarmonizerConstants.UTILIZZAZIONI) != null &&
                record.get(HarmonizerConstants.UTILIZZAZIONI) instanceof String &&
                    StringUtils.isEmpty((String) record.get(HarmonizerConstants.UTILIZZAZIONI))){
                record.put(HarmonizerConstants.UTILIZZAZIONI, fillWithZero ? "0" : "1" );
            }
        }
    }

    private Integer countUtilizations(List<Map<String, Object>> success, List<Integer> utilizations) {
        if( utilizations.size()>0){
            Integer count = 0;
            for( Map<String, Object> record : success){
                if(record.get(HarmonizerConstants.UTILIZZAZIONI)!= null ){
                    count = count + Integer.parseInt((String)record.get(HarmonizerConstants.UTILIZZAZIONI));
                }
            }
            return count;
        }
        return success.size();
    }

    private String writeNormalizedFile(List<Map<String, Object>> normalizedContent, String filePath,
                                       String fileSuffix, String writerDir, String writerConfig, String writerStream, String header ) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        File fileToWrite = null;
        try{
            StringBuffer fileName = new StringBuffer(filePath.substring(filePath.lastIndexOf("/")+1, filePath.lastIndexOf(".")));
            if(StringUtils.isNotEmpty(fileSuffix)){
                fileName.append(fileSuffix);
            }
            fileName.append(filePath.substring(filePath.lastIndexOf(".")));

            fileToWrite = new File(fileName.toString());
            logger.debug("create temp file {}", fileToWrite.getAbsolutePath());
            Map<String, Object> headerMap = !StringUtils.isEmpty(header) ? new ObjectMapper().readValue(header, Map.class) : null;
            TemplateMapper mapper = new TemplateMapper();
            mapper.writeFile(headerMap, normalizedContent, writerConfig, writerStream, fileToWrite);

            StringBuffer bucketPath = new StringBuffer(StringUtils.isNotEmpty(writerBucket) ? writerBucket : HarmonizerConstants.BUCKET_MULTIMEDIALE_LOCALE);
            logger.debug("Current Environment is {}", System.getenv(ENVIRONMENT));
            bucketPath.append(System.getenv(ENVIRONMENT)).append(writerDir).append(fileName);
            AmazonS3URI amazonS3URI = new AmazonS3URI(bucketPath.toString());
            logger.info("Load normalized file on Amazon S3 at {}", bucketPath);
            if (s3Service.upload(amazonS3URI.getBucket(), amazonS3URI.getKey(), fileToWrite)){
                fileToWrite.delete();
                return bucketPath.toString();
            }
        }
        catch (Exception e){
            logger.error(e.getMessage());
        }
        finally {
            if(fileToWrite != null){
                fileToWrite.delete();
            }
            bos.flush();
            bos.close();
        }
        return null;
    }

    private void setupDroolsSession(StatefulKnowledgeSession session, List<Map<String, Object>> total,
                                    List<Map<String, Object>> fail, List<Integer> utilizations
    ) {
        session.setGlobal(HarmonizerConstants.TOTAL, total);
        session.setGlobal(HarmonizerConstants.FAIL, fail);
        session.setGlobal(HarmonizerConstants.UTILIZATIONS, utilizations);
    }
}