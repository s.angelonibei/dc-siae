package com.alkemytech.sophia.harmonizer.goal.entity;

import javax.persistence.*;
import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigInteger;
import java.util.Date;

    @Produces("application/json")
    @XmlRootElement
    @Entity(name = "Soglie")
    @Table(name = "SOGLIE")
    @NamedQueries({ @NamedQuery(name = "Soglie.GetAll", query = "SELECT x FROM Soglie x") })
    public class Soglie {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private BigInteger id;

        @Column(name = "CODICE")
        private String codice;

        @Column(name = "REPERTORIO")
        private String repertorio;

        @Column(name = "AMBITO")
        private String ambito;

        @Column(name = "VALORE")
        private Double valore;

        @Column(name = "OPERATORE")
        private String operatore;

        @Column(name = "DESCRIZIONE")
        private String descrizione;

        @Column(name = "VALIDO_DA")
        private Date validoDa;

        @Column(name = "VALIDO_A")
        private Date validoA;

        public Soglie() {
        }

        public Soglie(String codice, String repertorio, String ambito, Double valore, String operatore, String descrizione, Date validoDa, Date validoA) {
            this.codice = codice;
            this.repertorio = repertorio;
            this.ambito = ambito;
            this.valore = valore;
            this.operatore = operatore;
            this.descrizione = descrizione;
            this.validoDa = validoDa;
            this.validoA = validoA;
        }

        public BigInteger getId() {
            return id;
        }

        public void setId(BigInteger id) {
            this.id = id;
        }

        public String getCodice() {
            return codice;
        }

        public void setCodice(String codice) {
            this.codice = codice;
        }

        public String getRepertorio() {
            return repertorio;
        }

        public void setRepertorio(String repertorio) {
            this.repertorio = repertorio;
        }

        public String getAmbito() {
            return ambito;
        }

        public void setAmbito(String ambito) {
            this.ambito = ambito;
        }

        public Double getValore() {
            return valore;
        }

        public void setValore(Double valore) {
            this.valore = valore;
        }

        public String getOperatore() {
            return operatore;
        }

        public void setOperatore(String operatore) {
            this.operatore = operatore;
        }

        public String getDescrizione() {
            return descrizione;
        }

        public void setDescrizione(String descrizione) {
            this.descrizione = descrizione;
        }

        public Date getValidoDa() {
            return validoDa;
        }

        public void setValidoDa(Date validoDa) {
            this.validoDa = validoDa;
        }

        public Date getValidoA() {
            return validoA;
        }

        public void setValidoA(Date validoA) {
            this.validoA = validoA;
        }
    }
