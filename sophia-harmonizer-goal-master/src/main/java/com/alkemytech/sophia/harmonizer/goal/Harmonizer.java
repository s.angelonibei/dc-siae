
package com.alkemytech.sophia.harmonizer.goal;

import com.alkemytech.sophia.common.s3.S3Service;
import com.alkemytech.sophia.harmonizer.goal.entity.MultimedialeLocale;
import com.alkemytech.sophia.harmonizer.goal.guice.McmdbJpaModule;
import com.alkemytech.sophia.harmonizer.goal.rest.ValidationClient;
import com.alkemytech.sophia.harmonizer.goal.service.DbService;
import com.alkemytech.sophia.harmonizer.goal.service.HarmonizationService;
import com.alkemytech.sophia.harmonizer.goal.utils.ExceptionHandler;
import com.alkemytech.sophia.harmonizer.goal.utils.HarmonizerConstants;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.name.Named;
import com.google.inject.persist.PersistService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;
import java.net.ServerSocket;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * Created by Alessandro Russo on 01/12/2017.
 */

public class Harmonizer {

    private static final Logger logger = LoggerFactory.getLogger(Harmonizer.class);

    private final Integer listenPort;
    private DbService dbService;
    private HarmonizationService harmonizationService;
    private ValidationClient validationClient;

    @Inject
    public Harmonizer( @Named("listenPort") Integer listenPort,
                       DbService dbService,
                       HarmonizationService harmonizationService,
                       ValidationClient validationClient
                   ) {
        this.listenPort = listenPort;
        this.dbService = dbService;
        this.harmonizationService = harmonizationService;
        this.validationClient = validationClient;
    }

    public static void main(String[] args) throws Exception {
        try {
            final Injector injector = Guice.createInjector( new McmdbJpaModule(args) );
            try{
                //Setup services
                injector.getInstance(PersistService.class).start();
                injector.getInstance(S3Service.class).startup();
                Harmonizer harmonizer = injector.getInstance(Harmonizer.class);

                //Run business logic
                harmonizer.process();
            }
            finally {
                //Stop services
                injector.getInstance(PersistService.class).stop();
                injector.getInstance(S3Service.class).shutdown();
            }
        } catch (Throwable e) {
            logger.error("main", e);
        } finally {
            System.exit(0);
        }
    }

    /**
     * Process Original Report in state "CARICATO"
     * @throws Exception
     */
    private void process() throws Exception {

        try(final ServerSocket socket = new ServerSocket( listenPort )){
            logger.info("HARMONIZATION TASK START: listening on {}", socket.getLocalSocketAddress());
            //seleziono i file da elaborare (stato ATTIVO)
            List<MultimedialeLocale> fileToProcessList = dbService.fileToParse();
            if(CollectionUtils.isNotEmpty(fileToProcessList)){
                logger.debug("Retrieved {} files to process ", fileToProcessList.size());
                for(MultimedialeLocale currentFile : fileToProcessList){
                    logger.info("----------------------------------------");
                    logger.info("START working {}", currentFile.getPosizioneReport());
                    logger.info("----------------------------------------");
                    try{
                        //leggo il contenuto (BEAN IO)
                        List<Object> records = harmonizationService.parseFile(currentFile);
                        //valido il contenuto (DROOLS)
                        Map<String, Object> validatedContent = harmonizationService.validateFile(records);
                        //genero un file con i soli record in OK (ne genero anche uno complessivo)
                        String posizioneReportNormalizzato = harmonizationService.createNormalizedFile(validatedContent, currentFile);
                        //aggiorno lo stato del file in (VALIDATO)
                        if(StringUtils.isNotEmpty(posizioneReportNormalizzato)){
                            if( harmonizationService.evaluateKPI(validatedContent, currentFile, posizioneReportNormalizzato) ){
                                logger.info("----------------------------------------");
                                logger.info("SUCCESS - stato {} - stato logico {} ", currentFile.getStato(), currentFile.getStatoLogico());
                                logger.info("----------------------------------------");
                            }
                        }
                    }
                    catch (Exception e){
                        logger.error(ExceptionHandler.logStackTrace(e));
                        currentFile.setStato(HarmonizerConstants.STATO_ERRORE_VALIDAZIONE);
                        currentFile.setStatoLogico(HarmonizerConstants.STATO_LOGICO_SCARTATO);
                        currentFile.setDataValidazione(new Date());
                        dbService.updateReport(currentFile);
                        logger.error("----------------------------------------");
                        logger.error("ERROR - stato {} - stato logico {} ", currentFile.getStato(), currentFile.getStatoLogico());
                        logger.error("----------------------------------------");
                    }
                    finally {
                        validationClient.sendValidationData(currentFile);
                    }
                }
            }
            else {
                logger.info("No Report with status {} found!", HarmonizerConstants.STATO_CARICATO);
            }
            logger.info("-- HARMONIZATION TASK END --");
        }
        catch (Exception e){
            logger.error("----------------------------------------");
            logger.error("-- HARMONIZATION TASK ERROR {}", ExceptionHandler.logStackTrace(e) );
            logger.error("----------------------------------------");
        }
    }
}