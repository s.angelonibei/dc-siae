package com.alkemytech.sophia.harmonizer.goal.service;

import com.alkemytech.sophia.harmonizer.goal.entity.MultimedialeLocale;
import com.alkemytech.sophia.harmonizer.goal.utils.HarmonizerConstants;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public class DbService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final Provider<EntityManager> provider;
    private String queryRetrieveValidationThreshold;
    private String queryRetrieveReportToNormalize;

    @Inject
    public DbService(Provider<EntityManager> provider,
                     @Named("query.retrieve.validation.threshold") String queryRetrieveValidationThreshold,
                     @Named("query.retrieve.report.to.normalize")String queryRetrieveReportToNormalize) {

        this.provider = provider;
        this.queryRetrieveValidationThreshold = queryRetrieveValidationThreshold;
        this.queryRetrieveReportToNormalize = queryRetrieveReportToNormalize;
    }

    public double getValidationThreshold() {
        EntityManager entityManager = provider.get();
        Query query = entityManager.createNativeQuery(queryRetrieveValidationThreshold);
        BigDecimal ret = (BigDecimal) query.setParameter(1, HarmonizerConstants.CODICE_ML_VAL_ACC).getSingleResult();
        return ret.doubleValue();
    }

    public List<MultimedialeLocale> getReportToNormalize(){
        EntityManager entityManager = provider.get();
        Query queryListFiles = entityManager.createNativeQuery(queryRetrieveReportToNormalize, MultimedialeLocale.class);
        queryListFiles.setParameter(1, HarmonizerConstants.STATO_CARICATO);
        queryListFiles.setParameter(2, HarmonizerConstants.STATO_CARICATO);
        List<MultimedialeLocale> fileList = queryListFiles.getResultList();
        return fileList;
    }

    public List<MultimedialeLocale> fileToParse() {
        EntityManager entityManager = provider.get();
        List<MultimedialeLocale> fileList = getReportToNormalize();
        if(CollectionUtils.isNotEmpty(fileList)){
            try {
                entityManager.getTransaction().begin();
                for (MultimedialeLocale current : fileList) {
                    current.setStato(HarmonizerConstants.STATO_IN_VALIDAZIONE);
                    current.setDataValidazione(new Timestamp(new Date().getTime()));
                    entityManager.persist(current);
                    entityManager.flush();
                }
                entityManager.getTransaction().commit();
            }
            catch (Exception e){
                logger.error(e.getMessage());
                if(entityManager.getTransaction().isActive()){
                    entityManager.getTransaction().rollback();
                }
            }
        }
        return fileList;
    }


    public MultimedialeLocale updateReport(MultimedialeLocale multimedialeLocale){
        EntityManager em = provider.get();
        em.getTransaction().begin();
        try {
            em.merge(multimedialeLocale);
            em.flush();
            em.getTransaction().commit();
            return multimedialeLocale;
        }
        catch (Exception e){
            logger.error(e.getMessage());
            if(em.getTransaction().isActive()){
                em.getTransaction().rollback();
            }
            return null;
        }
    }
}
