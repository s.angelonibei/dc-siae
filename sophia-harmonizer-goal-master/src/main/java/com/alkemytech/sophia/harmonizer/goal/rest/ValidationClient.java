package com.alkemytech.sophia.harmonizer.goal.rest;

import com.alkemytech.sophia.harmonizer.goal.entity.MultimedialeLocale;
import com.alkemytech.sophia.harmonizer.goal.service.DbService;
import com.alkemytech.sophia.harmonizer.goal.utils.ExceptionHandler;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.*;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import java.io.*;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Alessandro Russo on 12/04/2018.
 */

public class ValidationClient {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private DbService dbService;
    private String serviceEndopoint;

    @Inject
    public ValidationClient(DbService dbService,
                            @Named("mule.goal.validation.service") String serviceEndopoint) {
        this.dbService = dbService;
        this.serviceEndopoint = serviceEndopoint;
    }

    public void sendValidationData(MultimedialeLocale report) {
        String payload = null;
        try {
            //build Request
            payload = new Gson().toJson(new ValidationResult(report));
            logger.info("Request Payload: {}", payload);
            report.setInfoNotificaValidazione(payload);

            //setup ssl contetxt
            SSLContext ctx = SSLContext.getInstance("TLS");
            ctx.init(new KeyManager[0], new TrustManager[] {new DefaultTrustManager()}, new SecureRandom());
            SSLContext.setDefault(ctx);
            //open connection
            URL url = new URL(serviceEndopoint);
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.setDoOutput (true);
            conn.setHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String arg0, SSLSession arg1) {
                    return true;
                }
            });
            conn.setRequestMethod(HttpMethod.POST);
            conn.setRequestProperty(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON);
            conn.connect();
            //send request
            DataOutputStream outputStream = new DataOutputStream(conn.getOutputStream());
            outputStream.writeBytes(payload);
            outputStream.flush();
            outputStream.close();
            //evaluate response
            logger.info( "Response Status {}",conn.getResponseCode());
            if(conn.getResponseCode() == HttpStatus.SC_OK){
                JsonObject response = (JsonObject) new JsonParser().parse(new InputStreamReader(conn.getInputStream()));
                logger.info("Response Body {}", response.toString());
                Boolean isKO = response.get("KO").getAsBoolean();
                if( !isKO ){
                    report.setDataNotificaValidazione(new Date());
                }
            }
            conn.disconnect();
        }
        catch ( Exception e) {
            e.printStackTrace();
            logger.error(ExceptionHandler.logStackTrace(e));
        }
        finally {
            dbService.updateReport(report);
        }
    }

    private Map<String, String> buildResponse(InputStream inputStream) {
        StringBuffer response = new StringBuffer();
        String currentLine = null;
        try(BufferedReader reader = new BufferedReader(new InputStreamReader( inputStream ) ) ){
            while (null != ((currentLine = reader.readLine()))) {
                response.append(currentLine);
            }
            reader.close();
            return new Gson().fromJson(response.toString(), Map.class);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static class DefaultTrustManager implements X509TrustManager {

        @Override
        public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {}

        @Override
        public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {}

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }
    }
}