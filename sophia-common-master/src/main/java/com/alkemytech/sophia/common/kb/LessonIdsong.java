package com.alkemytech.sophia.common.kb;

import com.google.gson.GsonBuilder;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class LessonIdsong {

	private String idsongSophia;
	private String codiceOpera;
	private String origin;
	private String idutilSource;
	private String iddsrSource;
	private String iddspSource;

	public String getIdsongSophia() {
		return idsongSophia;
	}
	public LessonIdsong setIdsongSophia(String idsongSophia) {
		this.idsongSophia = idsongSophia;
		return this;
	}
	public String getCodiceOpera() {
		return codiceOpera;
	}
	public LessonIdsong setCodiceOpera(String codiceOpera) {
		this.codiceOpera = codiceOpera;
		return this;
	}
	public String getOrigin() {
		return origin;
	}
	public LessonIdsong setOrigin(String origin) {
		this.origin = origin;
		return this;
	}
	public String getIdutilSource() {
		return idutilSource;
	}
	public LessonIdsong setIdutilSource(String idutilSource) {
		this.idutilSource = idutilSource;
		return this;
	}
	public String getIddsrSource() {
		return iddsrSource;
	}
	public LessonIdsong setIddsrSource(String iddsrSource) {
		this.iddsrSource = iddsrSource;
		return this;
	}
	public String getIddspSource() {
		return iddspSource;
	}
	public LessonIdsong setIddspSource(String iddspSource) {
		this.iddspSource = iddspSource;
		return this;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}

}
