package com.alkemytech.sophia.common;

public interface Constants {
	//GENERAL
	String SCOPE_TELE					="T";
	String SCOPE_RADIO					="R";
	String FLAG_NO 						="0";
	String FLAG_YES 					="1";

	//HARMONIZATION UTILS
	String HARMONIZED_FILE_PREFIX 		="HARM_";
	String LOCK_SUFFIX 					=".lock";
    String PRELOCK_SUFFIX 				=".prelock";
	
	//DROOLS
	String GLOBAL_RECORD	 			="record";
	String GLOBAL_ERROR 				="errors";
		 
	//BEANIO
	String RECORD_HEADER 				="header";
	String RECORD_BODY 					="record";
	String RECORD_FOOTER 				="footer";

	//NORMALIZED DEFAULT
	String DEFAULT_WRITE_EXT			=".csv";
	String DEFAULT_WRITE_BUCKET			="s3://siae-sophia-broadcasting/";

	String IDENTIFICATIVO_RAI ="";

	//NORMALIZED FILE PROPERTY
	// -- comparison fields
	String ID_BROADCASTER 				="idBroadcaster";
	String COMPARE_BROADCASTER			="compareBroadcaster";
	String COMPARE_CHANNEL				="compareChannel";
	String COMPARE_DATE					="compareDate";
	String COMPARE_YEAR					="compareYear";
	String BEGIN_MONTH					="beginMonth";
	String END_MONTH					="endMonth";
	String CURRENT_DATE					="currentDate";
	String CURRENT_YEAR					="currentYear";

	// -- original fields
	// -- RADIO
	String NOME_EMITTENTE				="nomeEmittente";
	String DATA_DIFFUSIONE_TX			="dataDiffusioneTx";
	String COMPOSITORE_OP				="compositoreOp";
	String ESECUTORE_OP					="esecutoreOp";
	String ALBUM_OP						="albumOp";
	String CODICE_ISWC_OP				="codiceIswcOp";
	String CODICE_ISRC_OP				="codiceIsrcOp";
	String ORARIO_INIZIO_OP				="orarioInizioOp";

	// -- TV
	String GENERE_TX					="genereTx";
	String TITOLO_ORIGINALE_TX			="titoloOriginaleTx";
	String DATA_INIZIO_TX				="dataInizioTx";
	String ORARIO_FINE_TX				="orarioFineTx";
	String REPLICA_TX					="replicaTx";
	String PAESE_PRODUZIONE_TX			="paeseProduzioneTx";
	String ANNO_PRODUZIONE_TX			="annoProduzioneTx";
	String PRODUTTORE_TX				="produttoreTx";
	String REGISTA_TX					="registaTx";
	String TITOLO_EPISODIO_TX			="titoloEpisodioTx";
	String TITOLO_ORIGINALE_EPISODIO_TX	="titoloOriginaleEpisodioTx";
	String NUM_PROGRESSIVO_EPISODIO_TX	="numProgressivoEpisodioTx";
	String COMPOSITORE1_OP				="compositore1Op";
	String COMPOSITORE2_OP				="compositore2Op";
	String DURATA_EFFETTIVA_OP			="durataEffettivaOp";

	// -- COMMON
	String PALINSESTO					="palinsesto";
	String CANALE						="canale";
	String TITOLO_TX					="titoloTx";
	String ORARIO_INIZIO_TX				="orarioInizioTx";
	String DURATA_TX					="durataTx";
	String TITOLO_OP					="titoloOp";
	String DURATA_OP					="durataOp";
	String CATEGORIA_USO_OP				="categoriaUsoOp";
	String DIFFUSIONE_REGIONALE			="diffusioneRegionale";

	// -- decoded object
	String DECODE_SERVICE 				="decodeService";

	// -- decoded fields
	String DECODE_ID_BROADCASTER        ="decodeIdBroadcaster";
	String DECODE_ID_CANALE				="decodeIdCanale";
	String DECODE_ID_GENERE_TX			="decodiIdGenereTx";
	String DECODE_DESC_GENERE_TX		="decodeGenereTx";
	String DECODE_DATA_INIZIO_TX		="decodeDataInizioTx";
	String DECODE_ORARIO_INIZIO_TX		="decodeOrarioInizioTx";
	String DECODE_ORARIO_FINE_TX		="decodeOrarioFineTx";
	String DECODE_DURATA_TX				="decodeDurataTx";
	String DECODE_ANNO_PRODUZIONE_TX	="decodeAnnoProduzioneTx";
	String DECODE_DURATA_OP				="decodeDurataOp";
	String DECODE_DURATA_EFFETTIVA_OP	="decodeDurataEffettivaOp";
	String DECODE_ORARIO_INIZIO_OP		="decodeOrarioInizioOp";
	String DECODE_ID_CATEGORIA_USO_OP	="decodeIdCategoriaUsoOp";
	String DECODE_DESC_CATEGORIA_USO_OP	="decodeCategoriaUsoOp";

	// -- error field
	String ERRORS						="Errors";

	// -- rule fields
	String REGOLA1						="regola1";
	String REGOLA2						="regola2";
	String REGOLA3						="regola3";
}
