package com.alkemytech.sophia.common.kb;

import com.google.gson.GsonBuilder;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class ConflictingIdsong {

	private SophiaIdsongOpera sophiaIdsongOpera;
	private LessonIdsong lessonIdsong;
	
	public SophiaIdsongOpera getSophiaIdsongOpera() {
		return sophiaIdsongOpera;
	}
	public ConflictingIdsong setSophiaIdsongOpera(SophiaIdsongOpera sophiaIdsongOpera) {
		this.sophiaIdsongOpera = sophiaIdsongOpera;
		return this;
	}

	public LessonIdsong getLessonIdsong() {
		return lessonIdsong;
	}
	public ConflictingIdsong setLessonIdsong(LessonIdsong lessonIdsong) {
		this.lessonIdsong = lessonIdsong;
		return this;
	}
	
	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}

}
