package com.alkemytech.sophia.common.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.alkemytech.sophia.common.tools.StringTools;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class ConfigurationLoader {

	private String filePath;
	private String[] args;
	private String resourceName;
	
	public ConfigurationLoader withFilePath(String filePath) {
		this.filePath = filePath;
		return this;
	}
	
	public ConfigurationLoader withCommandLineArgs(String[] args) {
		this.args = args;
		return this;
	}

	public ConfigurationLoader withResourceName(String resourceName) {
		this.resourceName = resourceName;
		return this;
	}

	public Properties load() {

		final Properties properties = new Properties();
		properties.putAll(System.getenv()); // os environment
		properties.putAll(System.getProperties()); // java system properties
		boolean loaded = false;
		String message = "no configuration";
		if (null != args && args.length > 0) {
			try (final InputStream in = new FileInputStream(args[0])) {
				properties.load(in);
				loaded = true;
				message = "configuration from command line " + args[0];
			} catch (IOException | NullPointerException e) {
				// swallow
			}
		}
		if (!loaded && !StringTools.isNullOrEmpty(filePath)) {
			try {
				final File file = new File(filePath);
				try (final InputStream in = new FileInputStream(file)) {
					properties.load(in);
					loaded = true;
					message = "configuration from file path " + file.getAbsolutePath();
				}
			} catch (IOException | NullPointerException e) {
				// swallow
			}
		}
		if (!loaded && !StringTools.isNullOrEmpty(resourceName)) {
			try (final InputStream in = ConfigurationLoader.class.getResourceAsStream(resourceName)) {
				properties.load(in);
				loaded = true;
				message = "configuration from resource name";
			} catch (IOException | NullPointerException e) {
				// swallow
			}
		}
		if (!loaded && !StringTools.isNullOrEmpty(properties.getProperty("configuration"))) {
			try (final InputStream in = new FileInputStream(properties.getProperty("configuration"))) {
				properties.load(in);
				loaded = true;
				message = "configuration from java system property " +
						properties.getProperty("configuration");
			} catch (IOException | NullPointerException e) {
				// swallow
			}
		}
		if (!loaded && !StringTools.isNullOrEmpty(System.getenv("CONFIGURATION"))) {
			try (final InputStream in = new FileInputStream(System.getenv("CONFIGURATION"))) {
				properties.load(in);
				loaded = true;
				message = "configuration from os environment " + System.getenv("CONFIGURATION");
			} catch (IOException | NullPointerException e) {
				// swallow
			}
		}
		if (!loaded) {
			try (final InputStream in = ConfigurationLoader.class.getResourceAsStream("/configuration.properties")) {
				properties.load(in);
				loaded = true;
				message = "configuration from classpath";
			} catch (IOException | NullPointerException e) {
				// swallow
			}
		}
		if ("true".equals(properties.getProperty("debug"))) {
			System.out.println(message);
			properties.list(System.out);
		}
		return properties;
		
	}
	
}
