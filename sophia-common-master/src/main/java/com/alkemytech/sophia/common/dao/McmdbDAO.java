 package com.alkemytech.sophia.common.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class McmdbDAO {

	private final Connection connection;
	
	private boolean ignoreDuplicateKeyViolation;
	
	public McmdbDAO(Connection connection) {
		super();
		this.connection = connection;
		this.ignoreDuplicateKeyViolation = true;
	}

	public boolean isIgnoreDuplicateKeyViolation() {
		return ignoreDuplicateKeyViolation;
	}

	public McmdbDAO setIgnoreDuplicateKeyViolation(boolean ignoreDuplicateKeyViolation) {
		this.ignoreDuplicateKeyViolation = ignoreDuplicateKeyViolation;
		return this;
	}

	public DsrInfo getDsrInfo(String idDsr) throws SQLException {
		final String sql = new StringBuilder()
			.append("select A.YEAR YEAR")
			.append(", A.PERIOD PERIOD")
			.append(", A.PERIOD_TYPE PERIOD_TYPE")
			.append(", A.COUNTRY COUNTRY")
			.append(", B.IDDSP IDDSP")
			.append(", B.ID_UTIILIZATION_TYPE ID_UTIILIZATION_TYPE")
			.append(", B.OFFERING OFFERING")
			.append(", C.FTP_SOURCE_PATH FTP_SOURCE_PATH")
			.append(", C.CODE CODE")
			.append(" from DSR_METADATA A")
			.append(", COMMERCIAL_OFFERS B")
			.append(", ANAG_DSP C")
			.append(" where A.IDDSR = '").append(idDsr)
			.append("' and A.SERVICE_CODE = B.ID_COMMERCIAL_OFFERS")
			.append(" and B.IDDSP = C.IDDSP")
			.toString();
		try (final Statement statement = connection.createStatement()) {
			try (final ResultSet resultSet = statement.executeQuery(sql)) {
				if (resultSet.next()) {
					return new DsrInfo()
						.setIdDsr(idDsr)
						.setYear(resultSet.getString("YEAR"))
						.setMonth("quarter".equalsIgnoreCase(resultSet.getString("PERIOD_TYPE")) ?
								String.format("Q%d", resultSet.getInt("PERIOD")) : 
									String.format("%02d", resultSet.getInt("PERIOD")))
						.setIdDsp(resultSet.getString("IDDSP"))
						.setFtpSourcePath(resultSet.getString("FTP_SOURCE_PATH"))
						.setDspCode(resultSet.getString("CODE"))
						.setCountry(resultSet.getString("COUNTRY"))
						.setIdUtilizationType(resultSet.getString("ID_UTIILIZATION_TYPE"))
						.setOffering(resultSet.getString("OFFERING"));
				}
			}
		}
		return null;
	}
	
	public int insertSqsDeduplication(String uuid, String queueName, String messageJson) throws SQLException {
		final String sql = new StringBuilder()
			.append("insert into SQS_DEDUPLICATION (UUID, QUEUE_NAME) values ('")
			.append(uuid).append("', '").append(queueName).append("')")
			.toString();
		// WARNING: discard JSON to minimize disk use
		try (final Statement statement = connection.createStatement()) {
			try {
				return statement.executeUpdate(sql);
			} catch (SQLException e) {
				if (ignoreDuplicateKeyViolation && 1062 == e.getErrorCode()) { // duplicate key violation
					return 0;
				}
				throw e;
			}
		}
	}

//	public int insertSqsDeduplication(String uuid, String queueName, String messageJson) throws SQLException {
//		final String sql = "insert into SQS_DEDUPLICATION (UUID, QUEUE_NAME, MESSAGE_JSON) values (?, ?, ?)";
//		try (final PreparedStatement statement = connection.prepareStatement(sql)) {
//			int i = 1;
//			statement.setString(i++, uuid);
//			statement.setString(i++, queueName);
//			statement.setString(i++, messageJson);
//			try {
//				return statement.executeUpdate();
//			} catch (SQLException e) {
//				if (ignoreDuplicateKeyViolation && 1062 == e.getErrorCode()) { // duplicate key violation
//					return 0;
//				}
//				throw e;
//			}
//		}
//	}
	
	public boolean existsDsrLearning(String idDsr, int kbVersion) throws SQLException {
		try (final Statement statement = connection.createStatement()) {
			final String sql = new StringBuilder()
				.append("select INSERT_TIME")
				.append(" from DSR_LEARNING")
				.append(" where IDDSR = '").append(idDsr)
				.append("' and KB_VERSION = ").append(kbVersion)
				.toString();
			try (final ResultSet resultSet = statement.executeQuery(sql)) {
				if (resultSet.next()) {
					resultSet.getTimestamp(1);
					return !resultSet.wasNull();
				}
			}
			return false;
		}
	}
	
	public int insertDsrLearning(String idDsr, int kbVersion) throws SQLException {
		final String sql = new StringBuffer()
			.append("insert into DSR_LEARNING ")
			.append("( IDDSR")
			.append(", KB_VERSION")
			.append(") values (?, ?)")
			.toString();
		try (final PreparedStatement statement = connection.prepareStatement(sql)) {
			int i = 1;
			statement.setString(i++, idDsr);
			statement.setInt(i++, kbVersion);
			try {
				return statement.executeUpdate();
			} catch (SQLException e) {
				if (ignoreDuplicateKeyViolation && 1062 == e.getErrorCode()) { // duplicate key violation
					return 0;
				}
				throw e;
			}
		}
	}
	
	public boolean existsDsrUnidentified(String idDsr) throws SQLException {
		try (final Statement statement = connection.createStatement()) {
			final String sql = new StringBuilder()
				.append("select INSERT_TIME")
				.append(" from DSR_UNIDENTIFIED")
				.append(" where IDDSR = '").append(idDsr).append("'")
				.toString();
			try (final ResultSet resultSet = statement.executeQuery(sql)) {
				if (resultSet.next()) {
					resultSet.getTimestamp(1);
					return !resultSet.wasNull();
				}
			}
			return false;
		}
	} 
	
	public int insertDsrUnidentified(String idDsr, long totalRows) throws SQLException {
		final String sql = new StringBuffer()
			.append("insert into DSR_UNIDENTIFIED ")
			.append("( IDDSR")
			.append(", TOTAL_ROWS")
			.append(") values (?, ?)")
			.toString();
		try (final PreparedStatement statement = connection.prepareStatement(sql)) {
			int i = 1;
			statement.setString(i++, idDsr);
			statement.setLong(i++, totalRows);
			try {
				return statement.executeUpdate();
			} catch (SQLException e) {
				if (ignoreDuplicateKeyViolation && 1062 == e.getErrorCode()) { // duplicate key violation
					return 0;
				}
				throw e;
			}
		}
	}
	
	public Map<String, BigDecimal> getCurrencyRates(String year, String month) throws SQLException {
		try (Statement statement = connection.createStatement()) {
			final String sql = new StringBuilder()
				.append("select src_currency")
				.append(", dst_currency")
				.append(", rate")
				.append(" from currency_rate")
				.append(" where year = '").append(year)
				.append("' and month = '").append(month)
				.append("'")
				.toString();
			try (final ResultSet resultSet = statement.executeQuery(sql)) {
				final Map<String, BigDecimal> result = new HashMap<String, BigDecimal>();
				while (resultSet.next()) {
					result.put(resultSet.getString("src_currency") + resultSet.getString("dst_currency"), 
							new BigDecimal(resultSet.getString("rate")));				
				}
				return result;
			}			
		}
	}
	
}


