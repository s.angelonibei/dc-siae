package com.alkemytech.sophia.common.beantocsv;

import com.opencsv.bean.ColumnPositionMappingStrategy;

public class CustomMappingStrategy<T> extends ColumnPositionMappingStrategy<T> {

    private String[] header;

    public CustomMappingStrategy(final String[] stringHeadings) {
        this.header = stringHeadings;
    }
    
    @Override
	public String[] generateHeader(T bean) {
		return this.generateHeader();
	}

	private String[] generateHeader() {
        return header.clone();
    }
	
	@Override
	public int findMaxFieldIndex() {
		return header.length -1;
	}
}