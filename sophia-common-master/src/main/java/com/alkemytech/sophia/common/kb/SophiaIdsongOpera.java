package com.alkemytech.sophia.common.kb;

import com.google.gson.GsonBuilder;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class SophiaIdsongOpera {

	private String idsongSophia;
	private String codiceOpera;
	private String origin;
	private String idutilSource;
	private String iddsrSource;
	private String iddspSource;
	private int kbVersion;
	private int valid;
	
	public String getIdsongSophia() {
		return idsongSophia;
	}
	public void setIdsongSophia(String idsongSophia) {
		this.idsongSophia = idsongSophia;
	}
	public String getCodiceOpera() {
		return codiceOpera;
	}
	public void setCodiceOpera(String codiceOpera) {
		this.codiceOpera = codiceOpera;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getIdutilSource() {
		return idutilSource;
	}
	public void setIdutilSource(String idutilSource) {
		this.idutilSource = idutilSource;
	}
	public String getIddsrSource() {
		return iddsrSource;
	}
	public void setIddsrSource(String iddsrSource) {
		this.iddsrSource = iddsrSource;
	}
	public String getIddspSource() {
		return iddspSource;
	}
	public void setIddspSource(String iddspSource) {
		this.iddspSource = iddspSource;
	}
	public int getKbVersion() {
		return kbVersion;
	}
	public void setKbVersion(int kbVersion) {
		this.kbVersion = kbVersion;
	}
	public int getValid() {
		return valid;
	}
	public void setValid(int valid) {
		this.valid = valid;
	}
	
	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}

}
