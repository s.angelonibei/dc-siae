package com.alkemytech.sophia.common.kb;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class Management {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Connection connection;

	public Management(Connection connection) {
		super();
		this.connection = connection;
	}

	public int getKbActiveVersion() throws KbException {
		try (final Statement statement = connection.createStatement()) {
			final StringBuilder sql = new StringBuilder()
				.append("select kb_active_version")
				.append(" from kb_management");
			logger.debug("getKbActiveVersion: executing sql {}", sql);
			try (final ResultSet resultSet = statement.executeQuery(sql.toString())) {
				resultSet.next();
				return resultSet.getInt(1);
			}
		} catch (Exception e) {
			throw new KbException(KbException.Error.GENERIC, e);
		}
	}

}
