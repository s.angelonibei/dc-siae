package com.alkemytech.sophia.common.emr;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.common.tools.StringTools;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.auth.PropertiesFileCredentialsProvider;
import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduce;
import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduceClientBuilder;
import com.amazonaws.services.elasticmapreduce.model.DescribeClusterRequest;
import com.amazonaws.services.elasticmapreduce.model.DescribeClusterResult;
import com.amazonaws.util.StringUtils;
import com.google.inject.Inject;
import com.google.inject.name.Named;

public class EMRService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Properties configuration;

	private AmazonElasticMapReduce emrClient;
	
	@Inject
	protected EMRService(@Named("configuration") Properties configuration) {
		super();
		this.configuration = configuration;
	}

	public EMRService startup() {
		shutdown();
		final String proxyHost = configuration.getProperty("proxy.host");
		final String proxyPort = configuration.getProperty("proxy.port");
	    final ClientConfiguration clientConfiguration = new ClientConfiguration();
	    if (!StringUtils.isNullOrEmpty(proxyHost) && !StringUtils.isNullOrEmpty(proxyPort)) {
	    	clientConfiguration.setProxyHost(proxyHost);
	    	clientConfiguration.setProxyPort(Integer.parseInt(proxyPort));
	    }
		final String region = configuration.getProperty("aws.region", "eu-west-1");
	    logger.debug("startup: region {}", region);
	    final String credentialsFilePath = configuration.getProperty("aws.credentials");
	    logger.debug("startup: credentialsFilePath {}", credentialsFilePath);
	    final AWSCredentialsProvider credentials = StringTools.isNullOrEmpty(credentialsFilePath) ?
	    		new ClasspathPropertiesFileCredentialsProvider() :
	    			new PropertiesFileCredentialsProvider(credentialsFilePath);
	    emrClient = AmazonElasticMapReduceClientBuilder.standard()
	    	.withCredentials(credentials)
	    	.withClientConfiguration(clientConfiguration)
	    	.withRegion(region)
	    	.build();
		return this;
	}

	public EMRService shutdown() {
		if (null != emrClient) {
			emrClient.shutdown();
		}
		return this;
	}
	
	public String getDnsName(String clusterId) {
		final DescribeClusterResult result = emrClient
				.describeCluster(new DescribeClusterRequest().withClusterId(clusterId));
		return null == result ? null :
			result.getCluster().getMasterPublicDnsName();
	}
	

}
