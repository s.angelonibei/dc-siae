package com.alkemytech.sophia.common.net;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class IPv4Mask {
	
	private Inet4Address i4addr;
	private byte maskCtr;
	private int addrInt;
	private int maskInt;

	public IPv4Mask(Inet4Address i4addr, byte mask) {
		this.i4addr = i4addr;
		this.maskCtr = mask;
		this.addrInt = addrToInt(i4addr);
		this.maskInt = ~((1 << (32 - maskCtr)) - 1);
	}

	/** IPv4Mask factory method. 
	 * 
	 * @param addrSlashMask IP/Mask String in format "nnn.nnn.nnn.nnn/mask". If 
	 *		the "/mask" is omitted, "/32" (just the single address) is assumed.
	 * @return a new IPv4Mask
	 * @throws UnknownHostException if address part cannot be parsed by 
	 *		InetAddress
	 */
	public static IPv4Mask getIPv4Mask(String addrSlashMask) throws UnknownHostException {
		int pos = addrSlashMask.indexOf('/');
		String addr;
		byte maskCtr;
		if (-1 == pos) {
			addr = addrSlashMask;
			maskCtr = 32;
		} else { 
			addr = addrSlashMask.substring(0, pos);
			maskCtr = Byte.parseByte(addrSlashMask.substring(pos + 1));
		}
		return new IPv4Mask((Inet4Address) InetAddress.getByName(addr), maskCtr);
	}

	/** Test given IPv4 address against this IPv4Mask object.
	 * 
	 * @param testAddr address to check.
	 * @return true if address is in the IP Mask range, false if not.
	 */	
	public boolean matches(Inet4Address testAddr) {
		final int testAddrInt = addrToInt(testAddr);	 
		return ((addrInt & maskInt) == (testAddrInt & maskInt));
	}

	/** Convenience method that converts String host to IPv4 address.
	 * 
	 * @param addr IP address to match in nnn.nnn.nnn.nnn format or hostname.
	 * @return true if address is in the IP Mask range, false if not.
	 * @throws UnknownHostException if the string cannot be decoded.
	 */
	public boolean matches(String addr) throws UnknownHostException {
		return matches((Inet4Address) InetAddress.getByName(addr));
	}

	/** Converts IPv4 address to integer representation.
	 */
	private static int addrToInt(Inet4Address i4addr) {
		final byte[] ba = i4addr.getAddress();	
		return	((ba[0] & 0xFF) << 24) |
				((ba[1] & 0xFF) << 16) |
				((ba[2] & 0xFF) << 8 ) |
				((ba[3] & 0xFF)      );
	}

	@Override
	public String toString() {
		return i4addr.getHostAddress() + "/" + maskCtr;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final IPv4Mask that = (IPv4Mask) obj;		
		return (this.addrInt == that.addrInt && this.maskInt == that.maskInt);
	}

	@Override
	public int hashCode() {
		return this.maskInt + this.addrInt;
	}

//	public static void test(IPv4Mask ipmask, String addr, boolean expect) throws UnknownHostException {
//		final boolean got = ipmask.matches(addr);
//		System.out.println(addr + "\t(" + expect + ") ?\t "+ got + "\t" + (got == expect ? "" : "ERROR"));
//	}

//	public static void main(String args[]) throws Exception {
//		IPv4Mask ipmask;
//
//		ipmask = IPv4Mask.getIPv4Mask("10.0.0.1/16");
//		System.out.println("Checking " + ipmask + "...");
//
//		test(ipmask, "10.0.0.1", true);
//		test(ipmask, "10.1.0.1", false);
//
//		ipmask = IPv4Mask.getIPv4Mask("192.168.20.32/24");
//		System.out.println("Checking " + ipmask + "...");
//
//		test(ipmask, "192.168.20.31", true);
//		test(ipmask, "192.168.20.32", true);
//		test(ipmask, "192.168.20.33", true);
//		test(ipmask, "192.168.20.34", true);
//		test(ipmask, "192.168.20.35", true);
//		test(ipmask, "192.168.20.36", true);
//		test(ipmask, "192.168.20.254", true);
//		test(ipmask, "192.168.20.157", true);
//		test(ipmask, "192.168.21.1", false);
//		test(ipmask, "192.168.19.255", false);
//		test(ipmask, "192.168.24.1", false);
//
//		ipmask = IPv4Mask.getIPv4Mask("192.168.20.32/31");
//		System.out.println("Checking " + ipmask + "...");
//
//		test(ipmask, "192.168.20.31", false);
//		test(ipmask, "192.168.20.32", true);
//		test(ipmask, "192.168.20.33", true);
//		test(ipmask, "192.168.20.34", false);
//		test(ipmask, "192.168.20.35", false);
//		test(ipmask, "192.168.20.36", false);
//		test(ipmask, "192.168.20.254", false);
//		test(ipmask, "192.168.20.157", false);
//		test(ipmask, "192.168.21.1", false);
//		test(ipmask, "192.168.19.255", false);
//		test(ipmask, "192.168.24.1", false);
//
//		ipmask = IPv4Mask.getIPv4Mask("192.168.20.32/23");
//		System.out.println("Checking " + ipmask + "...");
//
//		test(ipmask, "192.168.20.31", true);
//		test(ipmask, "192.168.20.32", true);
//		test(ipmask, "192.168.20.33", true);
//		test(ipmask, "192.168.20.254", true);
//		test(ipmask, "192.168.21.254", true);
//		test(ipmask, "192.168.19.255", false);
//		test(ipmask, "192.168.24.1", false);
//	}
	
}