package com.alkemytech.sophia.common.tools;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.lang.StringUtils;

/**
 * Created by Alessandro Russo on 28/11/2017.
 */
public class DateTools {
    //DATE FORMAT
    public static final String DATE_ENG_FORMAT_SLASH        = "MM/dd/yyyy";
    public static final String DATE_ENG_FORMAT_DASH         = "MM-dd-yyyy";
    public static final String DATE_ITA_FORMAT_SLASH        = "dd/MM/yyyy";
    public static final String DATE_ITA_FORMAT_DASH         = "dd-MM-yyyy";

    //DATETIME FORMAT
    public static final String DATETIME_ENG_FORMAT_SLASH    = "MM/dd/yyyy HH:mm:ss";
    public static final String DATETIME_ENG_FORMAT_DASH     = "MM-dd-yyyy HH:mm:ss";
    public static final String DATETIME_ITA_FORMAT_SLASH    = "dd/MM/yyyy HH:mm:ss";
    public static final String DATETIME_ITA_FORMAT_DASH     = "dd-MM-yyyy HH:mm:ss";
    public static final String DATETIME_MYSQL_FORMAT        = "yyyy-MM-dd HH:mm:ss";

    //TIME FORMAT
    public static final String TIME_SIMPLE_FORMAT           = "HH:mm";
    public static final String TIME_COMPLETE_FORMAT         = "HH:mm:ss";

    public static final String DATE_ITA_SLASH_REGEX         = "^([0-2][0-9]||3[0-1])/(0[0-9]||1[0-2])/([0-9][0-9])?[0-9][0-9]$";

    public static String dateToString (Date date, String format) {
        if( date != null ){
            DateFormat df = new SimpleDateFormat(format);
            try{
                return df.format(date);
            }catch (Exception e){
                return null;
            }
        }
        return null;
    }

    public static Date stringToDate (String dateString, String format) {
        if(StringUtils.isNotEmpty(dateString)){
            DateFormat df = new SimpleDateFormat(format);
            boolean matched = true;
            if( DATE_ITA_FORMAT_SLASH.equals(format)){
                matched = dateString.matches(DATE_ITA_SLASH_REGEX);
            }
            if(matched){
                try {
                    return df.parse(dateString);
                } catch (ParseException e) {
                    return null;
                }
            }
        }
        return null;
    }

    public static Date manageDate (Date inputDate, int quantity, int timeUnit){
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(inputDate);
        switch (timeUnit){
            case Calendar.SECOND:
                calendar.add(Calendar.SECOND, quantity);
                break;
            case Calendar.MINUTE:
                calendar.add(Calendar.MINUTE, quantity);
                break;
            case Calendar.HOUR_OF_DAY:
                calendar.add(Calendar.HOUR_OF_DAY, quantity);
                break;
            case Calendar.DAY_OF_MONTH:
                calendar.add(Calendar.DAY_OF_MONTH, quantity);
                break;
        }
        return calendar.getTime();
    }

    public static void main (String[] args) throws Exception{
//        Date test = new Date();
//        System.out.println("INPUT DATE -> "+dateToString(test, DATETIME_ITA_FORMAT_SLASH));

        String input = "1800";
        while (input.length() < 6) {
            input = "0" + input;
        }
        String output = DateTools.dateToString(DateTools.stringToDate(input, "HHmmss"), DateTools.TIME_COMPLETE_FORMAT);



        System.out.println("OUTPUT DATE -> "+output);


//        int quantity = -1;
//        int timeUnit = Calendar.DAY_OF_MONTH;
//        Date ret = manageDate(test, quantity, timeUnit);
//        System.out.println("OUTPUT DATE -> "+dateToString(ret, DATETIME_ITA_FORMAT_SLASH));
//        System.out.println("OUTPUT DATE -> "+dateToString(test, TIME_SIMPLE_FORMAT));

    }

}
