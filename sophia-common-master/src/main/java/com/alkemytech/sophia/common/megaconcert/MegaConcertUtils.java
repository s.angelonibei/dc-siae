package com.alkemytech.sophia.common.megaconcert;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public abstract class MegaConcertUtils {

	static class EventoPagato {
		public long idEventoPagato;
		public long idEvento;
		public String numeroFattura;
		public String voceIncasso;
		public BigDecimal importoDem;
		public BigDecimal importoAggio;
		public Boolean quadraturaNdm;
		public BigDecimal importoExArt18;
		public int numeroPmAttesiSpalla; 
		public BigDecimal totaleDem;
	}
	
	static class NdmVoceFattura {
		public String voceIncasso;
		public BigDecimal importoOriginale;
		public BigDecimal percentualeAggio;
	}
	
	static class AggioMc {
		public BigDecimal soglia;
		public BigDecimal aggioSottoSoglia;
		public BigDecimal aggioSopraSoglia;
		public BigDecimal valoreCap;
		public boolean flagCapMultiplo;
	}

	private static boolean isMegaconcert(Connection connection, long idEvento) throws SQLException {
		
		final String sql = "SELECT FLAG_MEGACONCERT FROM PERF_MANIFESTAZIONE WHERE ID_EVENTO = " + idEvento;
		
		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(sql)) {
			if (resultSet.next()) {
				boolean quadraturaNdm = resultSet.getBoolean(1);
				return resultSet.wasNull() ? false : quadraturaNdm;
			}
		}
		
		return false;
		
	}
	
	private static List<EventoPagato> getEventiPagati(Connection connection, long idEvento) throws SQLException {
		
		final List<EventoPagato> eventiPagati = new ArrayList<>();
		
		try (Statement statement = connection.createStatement()) {
			
			// estrae tutti i numeri fattura e voci incasso per l'evento
			final String sql  = new StringBuilder()
				.append("SELECT PERF_EVENTI_PAGATI.ID_EVENTO_PAGATO")
				.append(", PERF_EVENTI_PAGATI.ID_EVENTO")
				.append(", PERF_EVENTI_PAGATI.NUMERO_FATTURA")
				.append(", PERF_EVENTI_PAGATI.VOCE_INCASSO")
				.append(", PERF_EVENTI_PAGATI.IMPORTO_DEM")
				.append(", PERF_EVENTI_PAGATI.IMPORTO_AGGIO")
				.append(", PERF_EVENTI_PAGATI.QUADRATURA_NDM")
				.append(", PERF_EVENTI_PAGATI.IMPORTO_EX_ART_18")
				.append(", PERF_EVENTI_PAGATI.NUMERO_PM_ATTESI_SPALLA")
				.append(", TOTALE_IMPORTI.TOT_DEM")
				.append(" FROM PERF_EVENTI_PAGATI")
				.append("  INNER JOIN (SELECT NUMERO_FATTURA, VOCE_INCASSO FROM PERF_EVENTI_PAGATI WHERE ID_EVENTO = ").append(idEvento).append(") AS VOCI_INCASSO")			
				.append("    ON (PERF_EVENTI_PAGATI.NUMERO_FATTURA = VOCI_INCASSO.NUMERO_FATTURA ")
				.append("        AND PERF_EVENTI_PAGATI.VOCE_INCASSO = VOCI_INCASSO.VOCE_INCASSO)")
				.append(" LEFT JOIN (")
				.append("   SELECT NUMERO_FATTURA, VOCE_INCASSO, SUM(IMPORTO_DEM) AS TOT_DEM")
				.append("     FROM PERF_EVENTI_PAGATI")
				.append("    GROUP BY NUMERO_FATTURA, VOCE_INCASSO")
				.append("  ) AS TOTALE_IMPORTI")
				.append("     ON (PERF_EVENTI_PAGATI.NUMERO_FATTURA = TOTALE_IMPORTI.NUMERO_FATTURA ")
				.append("         AND PERF_EVENTI_PAGATI.VOCE_INCASSO = TOTALE_IMPORTI.VOCE_INCASSO)")
				.toString();
			try (ResultSet resultSet = statement.executeQuery(sql)) {
				while (resultSet.next()) {
				
					final EventoPagato eventoPagato = new EventoPagato();
					int i = 1;
					eventoPagato.idEventoPagato = resultSet.getLong(i++);
					eventoPagato.idEvento = resultSet.getLong(i++);
					eventoPagato.numeroFattura = resultSet.getString(i++);
					eventoPagato.voceIncasso = resultSet.getString(i++);
					eventoPagato.importoDem = resultSet.getBigDecimal(i++);
					eventoPagato.importoAggio = resultSet.getBigDecimal(i++);
					final boolean quadraturaNdm = resultSet.getBoolean(i++);
					eventoPagato.quadraturaNdm = resultSet.wasNull() ? null : quadraturaNdm;
					eventoPagato.importoExArt18 = resultSet.getBigDecimal(i++);
					final int numeroPmAttesiSpalla = resultSet.getInt(i++);
					eventoPagato.numeroPmAttesiSpalla = resultSet.wasNull() ? 0 : numeroPmAttesiSpalla;
					eventoPagato.totaleDem = resultSet.getBigDecimal(i++);
					eventiPagati.add(eventoPagato);
		
				}
			}
			
		}

		return eventiPagati;
		
	}
	
	private static Map<String, NdmVoceFattura> getNdmVociFattura(Connection connection, String numeroFattura) throws SQLException {
		
		final Map<String, NdmVoceFattura> ndmVociFattura = new HashMap<>();
		
		final String sql = new StringBuilder()
			.append("SELECT VOCE")
			.append(", IMPORTO_ORIGINALE")
			.append(", PERCENTUALE_AGGIO")
			.append(" FROM PERF_NDM_VOCE_FATTURA")
			.append(" WHERE NUMERO_FATTURA = '").append(numeroFattura).append("'")			
			.toString();

		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(sql)) {
			while (resultSet.next()) {
			
				final NdmVoceFattura ndmVoceFattura = new NdmVoceFattura();
				int i = 1;
				ndmVoceFattura.voceIncasso = resultSet.getString(i++);
				ndmVoceFattura.importoOriginale = resultSet.getBigDecimal(i++);
				ndmVoceFattura.percentualeAggio = resultSet.getBigDecimal(i++);
				ndmVociFattura.put(ndmVoceFattura.voceIncasso, ndmVoceFattura);

			}
		}
		return ndmVociFattura;
		
	}
	
	private static void setNdmVoceFatturaNonTrovata(Connection connection, long idEventoPagato) throws SQLException {
		
		final String sql = new StringBuilder()
			.append("UPDATE PERF_EVENTI_PAGATI")
			.append(" SET IMPORTO_AGGIO = NULL")
			.append(", QUADRATURA_NDM = NULL")
			.append(", IMPORTO_EX_ART_18 = NULL")
			.append(" WHERE ID_EVENTO_PAGATO = ").append(idEventoPagato)			
			.toString();
		
		try (Statement statement = connection.createStatement()) {
			statement.executeUpdate(sql);
		}
		
	}
	
	private static void setMancataQuadraturaNdm(Connection connection, long idEventoPagato) throws SQLException {
		
		final String sql = new StringBuilder()
			.append("UPDATE PERF_EVENTI_PAGATI")
			.append(" SET IMPORTO_AGGIO = NULL")
			.append(", QUADRATURA_NDM = 0")
			.append(", IMPORTO_EX_ART_18 = NULL")
			.append(" WHERE ID_EVENTO_PAGATO = ").append(idEventoPagato)			
			.toString();
		
		try (Statement statement = connection.createStatement()) {
			statement.executeUpdate(sql);
		}
		
	}
	
	private static void setAggioNdm(Connection connection, long idEventoPagato, BigDecimal importoAggio, BigDecimal importoExArt18) throws SQLException {
		
		final String sql = new StringBuilder()
			.append("UPDATE PERF_EVENTI_PAGATI")
			.append(" SET IMPORTO_AGGIO = ").append(importoAggio.toPlainString())
			.append(", QUADRATURA_NDM = 1")
			.append(", IMPORTO_EX_ART_18 = ").append(importoExArt18.toPlainString())
			.append(" WHERE ID_EVENTO_PAGATO = ").append(idEventoPagato)			
			.toString();
		
		try (Statement statement = connection.createStatement()) {
			statement.executeUpdate(sql);
		}
		
	}
	
	private static AggioMc getAggioMc(Connection connection, long idEventoPagato) throws SQLException {
		
		final String sql = new StringBuilder()
			.append("SELECT A.SOGLIA")
			.append(", A.AGGIO_SOTTOSOGLIA")
			.append(", A.AGGIO_SOPRASOGLIA")
			.append(", A.VALORE_CAP")
			.append(", A.FLAG_CAP_MULTIPLO")
			.append(" FROM PERF_AGGI_MC A, PERF_EVENTI_PAGATI B")
			.append(" WHERE A.FLAG_ATTIVO = 1")	
			.append(" AND DATE(B.DATA_REVERSALE) >= DATE(A.INIZIO_PERIODO_VALIDAZIONE)")	
			.append(" AND (A.FINE_PERIODO_VALIDAZIONE is null or DATE(B.DATA_REVERSALE) <= DATE(A.FINE_PERIODO_VALIDAZIONE))")
			.append(" AND B.ID_EVENTO_PAGATO = ").append(idEventoPagato)	
			.toString();
		
		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(sql)) {
			if (resultSet.next()) {
				final AggioMc aggioMc = new AggioMc();
				int i = 1;
				aggioMc.soglia = resultSet.getBigDecimal(i++);
				aggioMc.aggioSottoSoglia = resultSet.getBigDecimal(i++);
				aggioMc.aggioSopraSoglia = resultSet.getBigDecimal(i++);
				aggioMc.valoreCap = resultSet.getBigDecimal(i++);
				aggioMc.flagCapMultiplo = resultSet.getBoolean(i++);
				return aggioMc;
			}
		}
		
		return null;
		
	}

	public static void updateEvento(Connection connection, long idEvento) throws SQLException {
		updateEvento(connection, idEvento, new HashSet<String>(Arrays.asList(new String[] { "2244" })), new BigDecimal(0.05), BigDecimal.ZERO);
	}

	public static void updateEvento(Connection connection, long idEvento, Set<String> vociIncassoMegaConcert) throws SQLException {
		updateEvento(connection, idEvento, vociIncassoMegaConcert, new BigDecimal(0.05), BigDecimal.ZERO);
	}

	public static void updateEvento(Connection connection, long idEvento, Set<String> vociIncassoMegaConcert, BigDecimal percentualeTrattenutaExArt18, BigDecimal percentualeTrattenutaExArt18MegaConcert) throws SQLException {
		
		// flag mega concert
		final boolean megaconcert = isMegaconcert(connection, idEvento);
		
		// eventi pagati
		final List<EventoPagato> eventiPagati = getEventiPagati(connection, idEvento);
		
		// ndm voci fatture
		final Map<String, Map<String, NdmVoceFattura>> ndmVociFatture = new HashMap<>();
		for (EventoPagato eventoPagato : eventiPagati) {
			if (!ndmVociFatture.containsKey(eventoPagato.numeroFattura))
				ndmVociFatture.put(eventoPagato.numeroFattura,
						getNdmVociFattura(connection, eventoPagato.numeroFattura));			
		}

		// processa eventi pagati
		for (EventoPagato eventoPagato : eventiPagati) {
			
			// cerca voci fattura
			final Map<String, NdmVoceFattura> ndmVociFattura = ndmVociFatture.get(eventoPagato.numeroFattura);
			// cerca voce fattura
			final NdmVoceFattura ndmVoceFattura = null == ndmVociFattura ? null : ndmVociFattura.get(eventoPagato.voceIncasso);
			
			// voce fattura non trovata
			if (null == ndmVoceFattura) {
				// update evento pagato
				setNdmVoceFatturaNonTrovata(connection, eventoPagato.idEventoPagato);
				continue;
			}
			
			// quadratura ndm
			final boolean quadraturaNdm = null == eventoPagato.totaleDem || null == ndmVoceFattura.importoOriginale ?
					false : (eventoPagato.totaleDem.compareTo(ndmVoceFattura.importoOriginale) == 0 ? true : false);
			// mancata quadratura ndm
			if (!quadraturaNdm) {
				// update evento pagato
				setMancataQuadraturaNdm(connection, eventoPagato.idEventoPagato);
				continue;
			}
			
			if(eventoPagato.idEvento != idEvento){
				continue;
			}
			
			// regole aggio megaconcert
			final AggioMc aggioMc = getAggioMc(connection, eventoPagato.idEventoPagato);

			// quadratura ma voce diversa da 2244 oppure manifestazione non megaconcert
			if (null == aggioMc || !vociIncassoMegaConcert.contains(eventoPagato.voceIncasso) || !megaconcert) {
				
				// calcolo aggio
				final BigDecimal importoAggio = eventoPagato.importoDem
						.multiply(ndmVoceFattura.percentualeAggio.multiply(new BigDecimal(0.01)));
				
				// calcolo trattenuta ex.art.18
				final BigDecimal importoAlNettoDiAgio = eventoPagato.importoDem.subtract(importoAggio);
				final BigDecimal importoExArt18 = importoAlNettoDiAgio.multiply(percentualeTrattenutaExArt18);
				
				// update evento pagato
				setAggioNdm(connection, eventoPagato.idEventoPagato, importoAggio, importoExArt18);
				continue;
			}

			// calcolo soglia effettiva
			final BigDecimal soglia;
			if (0 == eventoPagato.numeroPmAttesiSpalla) {
				soglia = aggioMc.soglia;
			} else if (aggioMc.flagCapMultiplo) {
				soglia = aggioMc.soglia.add(aggioMc.valoreCap.multiply(new BigDecimal(eventoPagato.numeroPmAttesiSpalla)));
			} else {
				soglia = aggioMc.soglia.add(aggioMc.valoreCap);
			}
			
			// calcolo importo aggio
			
			final BigDecimal cento = new BigDecimal(100);
			final BigDecimal importoAggio;
			if (eventoPagato.importoDem.compareTo(soglia) <= 0) {
				importoAggio = eventoPagato.importoDem.multiply(aggioMc.aggioSottoSoglia).divide(cento);
			} else {
				final BigDecimal importoAggioSottoSoglia = soglia.multiply(aggioMc.aggioSottoSoglia).divide(cento);
				final BigDecimal importoSopraSoglia = eventoPagato.importoDem.subtract(soglia);
				final BigDecimal importoAggioSopraSoglia = importoSopraSoglia.multiply(aggioMc.aggioSopraSoglia).divide(cento);
				importoAggio = importoAggioSottoSoglia.add(importoAggioSopraSoglia);		
			}
			
			// calcolo trattenuta ex.art.18
			final BigDecimal importoAlNettoDiAgio = eventoPagato.importoDem.subtract(importoAggio);
			
			final BigDecimal importoExArt18 = importoAlNettoDiAgio.multiply(percentualeTrattenutaExArt18MegaConcert);

			// update evento pagato
			setAggioNdm(connection, eventoPagato.idEventoPagato, importoAggio, importoExArt18);
			
		}
	
	}	

}
