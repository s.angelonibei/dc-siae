package com.alkemytech.sophia.common.s3;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.common.sqs.SQSMessage;
import com.amazonaws.services.s3.AmazonS3URI;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.google.inject.Inject;

public class S3Events {

	private static final Logger logger = LoggerFactory.getLogger(S3Events.class);

	// ObjectCreated
	public static final Pattern OBJECT_CRATED = Pattern
			.compile("(?:s3\\:)?ObjectCreated\\:.*");
	public static final Pattern OBJECT_CRATED_PUT = Pattern
			.compile("(?:s3\\:)?ObjectCreated\\:Put");
	public static final Pattern OBJECT_CRATED_POST = Pattern
			.compile("(?:s3\\:)?ObjectCreated\\:Post");
	public static final Pattern OBJECT_CRATED_COPY = Pattern
			.compile("(?:s3\\:)?ObjectCreated\\:Copy");
	public static final Pattern OBJECT_CRATED_COMPLETED_MULTIPART_UPLOAD = Pattern
			.compile("(?:s3\\:)?ObjectCreated\\:CompleteMultipartUpload");
	
	// ObjectRemoved
	public static final Pattern OBJECT_REMOVED = Pattern
			.compile("(?:s3\\:)?ObjectRemoved\\:.*");
	public static final Pattern OBJECT_REMOVED_DELETE = Pattern
			.compile("(?:s3\\:)?ObjectRemoved\\:Delete");
	public static final Pattern OBJECT_REMOVED_DELETE_MARKER_CREATED = Pattern
			.compile("(?:s3\\:)?ObjectRemoved\\:DeleteMarkerCreated");

	// ReducedRedundancyLostObject
	public static final Pattern REDUCED_REDUNDANCY_LOST_OBJECT = Pattern
			.compile("(?:s3\\:)?ReducedRedundancyLostObject");

	private final Gson gson;

	@Inject
	protected S3Events() {
		super();
		this.gson = new GsonBuilder()
			.disableHtmlEscaping()
			.create();
	}
	
	public List<AmazonS3URI> parseMessage(SQSMessage message, Pattern...eventNameFilters) {
		final List<AmazonS3URI> uris = new ArrayList<>();
		try {
			final JsonObject rootJsonObject = gson.fromJson(message.getText(), JsonObject.class);
			final JsonArray jsonArray = rootJsonObject.getAsJsonArray("Records");
			if (null != jsonArray) {
				for (JsonElement element : jsonArray) {
					final JsonObject record = element.getAsJsonObject();
					logger.debug("parseMessage: record {}", record);
					final JsonElement eventName = record.get("eventName");
					final JsonObject s3 = record.getAsJsonObject("s3");
					final JsonElement bucketName = s3.getAsJsonObject("bucket").get("name");
					final JsonElement objectKey = s3.getAsJsonObject("object").get("key");
					if (null != bucketName && !Strings.isNullOrEmpty(bucketName.getAsString()) &&
							null != objectKey && !Strings.isNullOrEmpty(objectKey.getAsString()) &&
							null != eventName && !Strings.isNullOrEmpty(eventName.getAsString())) {
						for (Pattern eventNameFilter : eventNameFilters) {
							if (eventNameFilter.matcher(eventName.getAsString()).matches()) {
								uris.add(new AmazonS3URI("s3://" + bucketName.getAsString() +
										"/" + objectKey.getAsString()));
							}
						}
					}
				}
			}
		} catch (JsonSyntaxException e) {
			logger.warn("parseMessage", e);
		}
		return uris;
	}
	
}
