package com.alkemytech.sophia.common.rest;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.TrustStrategy;

import javax.net.ssl.SSLContext;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class RestClient {

	public static class Result {
		
		private int statusCode;
		private JsonElement jsonElement;
		
		public int getStatusCode() {
			return statusCode;
		}
		public Result setStatusCode(int statusCode) {
			this.statusCode = statusCode;
			return this;
		}
		public JsonElement getJsonElement() {
			return jsonElement;
		}
		public Result setJsonElement(JsonElement jsonElement) {
			this.jsonElement = jsonElement;
			return this;
		}
		
		@Override
		public String toString() {
			return new GsonBuilder()
				.setPrettyPrinting().create().toJson(this);
		}
	}
	
	private static Result fromResponse(CloseableHttpResponse response) throws IOException {
		Result result = new Result()
			.setStatusCode(response.getStatusLine().getStatusCode());
	    HttpEntity responseEntity = response.getEntity();
	    if (null != responseEntity) {
		    try (InputStream in = responseEntity.getContent()) {
		    	byte[] buffer = new byte[1024];
		    	ByteArrayOutputStream out = new ByteArrayOutputStream();
		    	for (int count; -1 != (count = in.read(buffer)); ) {
		    		out.write(buffer, 0, count);
		    	}
		    	result.setJsonElement(new GsonBuilder().create()
			    		.fromJson(out.toString("UTF-8"), JsonElement.class));
		    }					
	    }
	    return result;
	}
	
	public static Result get(String url) throws IOException {
		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
			HttpGet httpGet = new HttpGet(url);
			try (CloseableHttpResponse response = httpClient.execute(httpGet)) {
			    return fromResponse(response);
			}
		}
	}
	
	public static Result post(String url, JsonObject requestJson) throws IOException {
		return post(url, requestJson.toString());
	}
	
	public static Result post(String url, String requestJson) throws IOException {
		StringEntity requestEntity = new StringEntity(requestJson,
				ContentType.create("application/json", "UTF-8"));
		return post(url, requestEntity, false);
	}

	public static Result post(String url, Map<String, String> urlEncodedParams, boolean insecure) throws IOException {
		List<NameValuePair> nvps = new ArrayList<>();
		for (Map.Entry<String, String> param : urlEncodedParams.entrySet()) {
			nvps.add(new BasicNameValuePair(param.getKey(), param.getValue()));
		}
		return post(url, new UrlEncodedFormEntity(nvps), insecure);
	}

	private static Result post(String url, HttpEntity httpEntity, boolean insecure) throws IOException {
		try (CloseableHttpClient httpClient = getCloseableHttpClient(insecure)) {
			HttpPost httpPost = new HttpPost(url);
			httpPost.setEntity(httpEntity);
			try (CloseableHttpResponse response = httpClient.execute(httpPost)) {
				return fromResponse(response);
			}
		} catch (NoSuchAlgorithmException | KeyStoreException | KeyManagementException e) {
			throw new RuntimeException(e);
		}
	}

	private static CloseableHttpClient getCloseableHttpClient(boolean insecure)
			throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
		if (insecure) {
			SSLContextBuilder builder = new SSLContextBuilder();
			builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
					builder.build(), NoopHostnameVerifier.INSTANCE);

			return HttpClients.custom().setSSLSocketFactory(sslsf).build();
		} else {
			return HttpClients.createDefault();
		}
	}

	public static Result put(String url, JsonObject requestJson) throws IOException {
		return put(url, requestJson.toString());
	}
	
	public static Result put(String url, String requestJson) throws IOException {
		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
			HttpPut httpPut = new HttpPut(url);
			StringEntity requestEntity = new StringEntity(requestJson.toString(), 
					   ContentType.create("application/json", "UTF-8"));
			httpPut.setEntity(requestEntity);
			try (CloseableHttpResponse response = httpClient.execute(httpPut)) {
			    return fromResponse(response);
			}
		}
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("main");
		Map<String, String> params = new HashMap<>();
		params.put("codiceAbbonamento", "12345678");
		params.put("isHtml", "0");
		params.put("mailSender", "noreply@siae.it");
		params.put("messageBody", "test prova test");
		params.put("subject", "test di prova");
		params.put("toList", "[{\"email\":\"roberto.yacoub@alkemy.com\"}]");


		/*System.setProperty("socksProxyHost", "127.0.0.1");
		System.setProperty("socksProxyPort", "8888");*/


		System.out.println(post("https://muleprd.ws.siae.it/mdaServices/mule/sendMail",
				params, true));

	}
	
}
