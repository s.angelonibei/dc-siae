package com.alkemytech.sophia.common.dao;

import com.google.gson.GsonBuilder;

/**
 * @author roberto.cerfogli <r.cerfogli@alkemytech.it>
 */
public class DsrInfo {

	private String idDsr;
	private String year;
	private String month;
	private String idDsp;
	private String ftpSourcePath;
	private String dspCode;
	private String country;
	private String idUtilizationType;
	private String offering;
	
	public String getIdDsr() {
		return idDsr;
	}
	public DsrInfo setIdDsr(String idDsr) {
		this.idDsr = idDsr;
		return this;
	}
	public String getYear() {
		return year;
	}
	public DsrInfo setYear(String year) {
		this.year = year;
		return this;
	}
	public String getMonth() {
		return month;
	}
	public DsrInfo setMonth(String month) {
		this.month = month;
		return this;
	}
	public String getIdDsp() {
		return idDsp;
	}
	public DsrInfo setIdDsp(String idDsp) {
		this.idDsp = idDsp;
		return this;
	}
	public String getFtpSourcePath() {
		return ftpSourcePath;
	}
	public DsrInfo setFtpSourcePath(String ftpSourcePath) {
		this.ftpSourcePath = ftpSourcePath;
		return this;
	}
	public String getDspCode() {
		return dspCode;
	}
	public DsrInfo setDspCode(String dspCode) {
		this.dspCode = dspCode;
		return this;
	}
	public String getCountry() {
		return country;
	}
	public DsrInfo setCountry(String country) {
		this.country = country;
		return this;
	}
	public String getIdUtilizationType() {
		return idUtilizationType;
	}
	public DsrInfo setIdUtilizationType(String idUtilizationType) {
		this.idUtilizationType = idUtilizationType;
		return this;
	}
	public String getOffering() {
		return offering;
	}
	public DsrInfo setOffering(String offering) {
		this.offering = offering;
		return this;
	}

	@Override
	public String toString() {
		return new GsonBuilder()
			.setPrettyPrinting()
			.create()
			.toJson(this);
	}
	
}
