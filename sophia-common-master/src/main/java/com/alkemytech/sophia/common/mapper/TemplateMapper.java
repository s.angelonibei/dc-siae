package com.alkemytech.sophia.common.mapper;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.beanio.BeanReader;
import org.beanio.BeanWriter;
import org.beanio.StreamFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alkemytech.sophia.common.Constants;
import com.alkemytech.sophia.common.s3.S3Service;
import com.alkemytech.sophia.common.tools.DateTools;
import com.amazonaws.services.s3.AmazonS3URI;
import com.google.inject.Singleton;

/**
 * Created by Alessandro Russo on 09/12/2017.
 */
@Singleton
public class TemplateMapper implements TemplateMapperInterface{

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public TemplateMapper(){
    }

    public StreamFactory loadStreamFactory( String configFilePath ){
        StreamFactory factory = StreamFactory.newInstance();
        factory.load( configFilePath );
        logger.debug( "Loading BEANIO config from {}", configFilePath );
        return factory;
    }

    public BeanReader createBeanReader(String configPath, String configStream, ByteArrayOutputStream bos){
        InputStreamReader fileStreamReader = new InputStreamReader( new ByteArrayInputStream(bos.toByteArray()) );
        BeanReader reader = loadStreamFactory(configPath).createReader( configStream, fileStreamReader );
        return reader;
    }

    public BeanWriter createBeanWriter(String configPath, String configStream, File fileToWrite){
        BeanWriter writer = loadStreamFactory(configPath).createWriter( configStream, fileToWrite);
        return writer;
    }

    public List<Object> parseFile(String configPath, String configStream, ByteArrayOutputStream bos ){
        BeanReader reader = createBeanReader(configPath, configStream, bos);
        List<Object> ret = new ArrayList<>();
        Object current = null;
        while ( ( current = reader.read() ) != null ){
            if( Constants.RECORD_BODY.equals(reader.getRecordName() ) ){
//                logger.debug("loaded object {}", current.toString());
                Object currentDto  = current;
                ret.add(currentDto);
            }
        }
        logger.debug("FILE parsed. Loaded {} rows", ret.size());
        return ret;
    }

    public Map<String, Object> parseEmpyTemplate(String configPath, String configStream, ByteArrayOutputStream bos ){
        BeanReader reader = createBeanReader(configPath, configStream, bos);
        Map<String,Object> ret = new HashMap<>();
        Object current = null;
        while ( ( current = reader.read() ) != null ){
            if( Constants.RECORD_HEADER.equals(reader.getRecordName() ) ){
                logger.debug("loaded object {}", current.toString());
                ret = (Map<String, Object>) current;
            }
        }
        return ret;
    }

    public void writeFile(Map<String, Object>header, List<Map<String,Object>> recordList, String configPath, String configStream, File fileToWrite ) throws Exception{
        BeanWriter writer = createBeanWriter(configPath, configStream, fileToWrite);
        if(header!=null){
            writer.write(Constants.RECORD_HEADER, header);
        }
        for(Map<String, Object> record : recordList){
            writer.write(Constants.RECORD_BODY, record);
        }
        writer.flush();
        writer.close();
    }

    public StreamFactory loadStreamFactory( String configFilePath, S3Service s3Service) throws IOException {
        StreamFactory factory = StreamFactory.newInstance();

        AmazonS3URI s3Uri = new AmazonS3URI(configFilePath);
        InputStream is =s3Service.download(s3Uri.getBucket(), s3Uri.getKey());
        factory.load(is);
        logger.debug( "Loading BEANIO config from {}", configFilePath );
        return factory;
    }

    public BeanReader createBeanReader(String configPath, String configStream, ByteArrayOutputStream bos, S3Service s3Service,Charset charset) throws IOException {
        InputStreamReader fileStreamReader = new InputStreamReader( new ByteArrayInputStream(bos.toByteArray()),charset );
        BeanReader reader = loadStreamFactory(configPath, s3Service).createReader( configStream, fileStreamReader);
        return reader;
    }

    public BeanWriter createBeanWriter(String configPath, String configStream, File fileToWrite, S3Service s3Service)throws IOException{
        BeanWriter writer = loadStreamFactory(configPath, s3Service).createWriter( configStream, fileToWrite);
        return writer;
    }

    public List<Object> parseFile(String configPath, String configStream, ByteArrayOutputStream bos, S3Service s3Service,Charset charset) throws IOException{
        BeanReader reader = createBeanReader(configPath, configStream, bos, s3Service,charset);
        List<Object> ret = new ArrayList<>();
        Object current = null;
        while ( ( current = reader.read() ) != null ){
            if( Constants.RECORD_BODY.equals(reader.getRecordName() ) ){
//                logger.debug("loaded object {}", current.toString());
                Object currentDto  = current;
                ret.add(currentDto);
            }
        }
        logger.debug("FILE parsed. Loaded {} rows", ret.size());
        return ret;
    }

    public Map<String, Object> parseEmpyTemplate(String configPath, String configStream, ByteArrayOutputStream bos, S3Service s3Service)throws IOException{
        BeanReader reader = createBeanReader(configPath, configStream, bos, s3Service, Charset.defaultCharset());
        Map<String,Object> ret = new HashMap<>();
        Object current = null;
        while ( ( current = reader.read() ) != null ){
            if( Constants.RECORD_HEADER.equals(reader.getRecordName() ) ){
                logger.debug("loaded object {}", current.toString());
                ret = (Map<String, Object>) current;
            }
        }
        return ret;
    }

    public void writeFile(Map<String, Object>header, List<Map<String,Object>> recordList, String configPath, String configStream, File fileToWrite, S3Service s3Service) throws Exception{
        BeanWriter writer = createBeanWriter(configPath, configStream, fileToWrite, s3Service);
        if(header!=null){
            writer.write(Constants.RECORD_HEADER, header);
        }
        for(Map<String, Object> record : recordList){
            writer.write(Constants.RECORD_BODY, record);
        }
        writer.flush();
        writer.close();
    }

    public List<Object> parseXlsFile(String configFilePath, ByteArrayOutputStream bos) throws Exception {
        XlsMapperConfiguration configuration = loadXlsConfiguration(configFilePath);
        InputStream is = new ByteArrayInputStream(bos.toByteArray());
        Workbook workbook = WorkbookFactory.create(is);
        Sheet sheet = workbook.getSheetAt(0);
        List<Object> ret = new ArrayList<>();
        for (Row row: sheet) {
            boolean isEvaluateHeader = configuration.isEvaluateHeader();
            boolean isHeader = (row.getRowNum() < configuration.getHeaderRow());
            if(isHeader && ! isEvaluateHeader){
                continue;
            }
            else{
                Map<String, Object> parsedRow = new HashMap<>();
                for(Cell cell: row) {
                    if( isHeader ){
                        evaluateHeaderCell(cell, configuration);
                    }
                    else{
                        parseCell(cell, configuration, parsedRow);
                    }
                }
                if(!isHeader){
                    ret.add(parsedRow);
                }
            }
        }
        logger.debug("FILE parsed. Loaded {} rows", ret.size());
        return ret;
    }

    public List<Object> parseXlsFile(String configFilePath, ByteArrayOutputStream bos, S3Service s3Service) throws Exception {
        XlsMapperConfiguration configuration = loadXlsConfiguration(configFilePath, s3Service);
        InputStream is = new ByteArrayInputStream(bos.toByteArray());
        Workbook workbook = WorkbookFactory.create(is);
        Sheet sheet = workbook.getSheetAt(0);
        List<Object> ret = new ArrayList<>();
        for (Row row: sheet) {
            boolean isEvaluateHeader = configuration.isEvaluateHeader();
            boolean isHeader = (row.getRowNum() < configuration.getHeaderRow());
            if(isHeader && ! isEvaluateHeader){
                continue;
            }
            else{
                Map<String, Object> parsedRow = new HashMap<>();
                for(Cell cell: row) {
                    if( isHeader ){
                        evaluateHeaderCell(cell, configuration);
                    }
                    else{
                        parseCell(cell, configuration, parsedRow);
                    }
                }
                if(!isHeader){
                    ret.add(parsedRow);
                }
            }
        }
        logger.debug("FILE parsed. Loaded {} rows", ret.size());
        return ret;
    }

    public XlsMapperConfiguration loadXlsConfiguration(String configFilePath) throws JAXBException {
        JAXBContext jc = JAXBContext.newInstance(XlsMapperConfiguration.class);
        Unmarshaller unmarshaller = jc.createUnmarshaller();
        XlsMapperConfiguration configuration = (XlsMapperConfiguration) unmarshaller.unmarshal(new File(configFilePath) );
        logger.debug( "Loading XLS MAPPER CONFIGURATION config from {}", configFilePath );
        return configuration;
    }

    public XlsMapperConfiguration loadXlsConfiguration(String configFilePath, S3Service s3Service) throws JAXBException {
        JAXBContext jc = JAXBContext.newInstance(XlsMapperConfiguration.class);
        Unmarshaller unmarshaller = jc.createUnmarshaller();
        AmazonS3URI amazonS3URI = new AmazonS3URI(configFilePath);
        XlsMapperConfiguration configuration = (XlsMapperConfiguration) unmarshaller.unmarshal(s3Service.download(amazonS3URI.getBucket(), amazonS3URI.getKey()));
        logger.debug( "Loading XLS MAPPER CONFIGURATION config from {}", configFilePath );
        return configuration;
    }

    public void parseCell(Cell cell, XlsMapperConfiguration configParser, Map<String, Object> parsedRow) {
        DataFormatter dataFormatter = new DataFormatter();
        XlsMapperConfiguration.Field field = configParser.getFields().get(cell.getColumnIndex());
        String cellValue = dataFormatter.formatCellValue(cell);
        if(CellType.NUMERIC.equals(cell.getCellTypeEnum()) &&  HSSFDateUtil.isCellDateFormatted(cell) && StringUtils.isNotEmpty(field.getFormat()) ){
            String dateFormat = StringUtils.isNotEmpty(field.getFormat()) ? field.getFormat() : DateTools.DATE_ITA_FORMAT_SLASH;
            Date dateCell = cell.getDateCellValue();
            cellValue = DateTools.dateToString(dateCell, dateFormat);
        }
        String treatedValue = cellValue.replaceAll("[\n\r]", "");
        parsedRow.put(field.getName(), treatedValue);
    }

    public void evaluateHeaderCell(Cell cell, XlsMapperConfiguration configParser) throws Exception {
        DataFormatter dataFormatter = new DataFormatter();
        XlsMapperConfiguration.Field field = configParser.getFields().get(cell.getColumnIndex());
        if( StringUtils.isNotEmpty(field.getLiteral())){
            String cellValue = dataFormatter.formatCellValue(cell);
            boolean isOk = field.isTrim() ? field.getLiteral().equals(cellValue.trim()) : field.getLiteral().equals(cellValue);
            if(!isOk){
                throw new Exception("Expected header value: "+ field.getLiteral() + " => header valued found: "+ cellValue );
            }
        }
        return;
    }
}
