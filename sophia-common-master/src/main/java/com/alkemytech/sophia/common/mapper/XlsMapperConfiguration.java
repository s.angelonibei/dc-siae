package com.alkemytech.sophia.common.mapper;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Alessandro Russo on 26/01/2018.
 */
@XmlRootElement(name = "configuration")
public class XlsMapperConfiguration {

    @XmlAttribute(name = "headerRow")
    private int headerRow;

    @XmlAttribute(name = "literal")
    private boolean literal;

    @XmlAttribute(name = "evaluateHeader")
    private boolean evaluateHeader;

    @XmlElementWrapper(name = "fields")
    @XmlElement(name = "field")
    private List<Field> fields;

    public static class Field{
        @XmlAttribute(name = "name")
        private String name;

        @XmlAttribute(name = "literal")
        private String literal;

        @XmlAttribute(name = "required")
        private boolean required;

        @XmlAttribute(name = "trim")
        private boolean trim;

        @XmlAttribute(name = "format")
        private String format;

        public boolean isTrim() {
            return trim;
        }

        public String getName() {
            return name;
        }

        public String getLiteral() {
            return literal;
        }

        public boolean isRequired() {
            return required;
        }

        public String getFormat() {
            return format;
        }

        @Override
        public String toString() {
            return "Field{" +
                    "name='" + name + '\'' +
                    ", literal='" + literal + '\'' +
                    ", required=" + required +
                    ", trim=" + trim +
                    ", format=" + format +
                    '}';
        }
    }

    public int getHeaderRow() {
        return headerRow;
    }

    public boolean isLiteral() {
        return literal;
    }

    public boolean isEvaluateHeader() {
        return evaluateHeader;
    }

    public List<Field> getFields() {
        return fields;
    }

    @Override
    public String toString() {
        return "XlsMapperConfiguration{" +
                "headerRow=" + headerRow +
                ", literal=" + literal +
                ", evaluateHeader=" + evaluateHeader +
                ", fields=" + fields +
                '}';
    }
}
